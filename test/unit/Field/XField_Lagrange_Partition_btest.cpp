// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <cmath>
#include <ostream>
#include <fstream>
#include <string>
#include <set>
#include <cstdio>
#include <algorithm>    // std::remove

#ifdef SANS_MPI
#include <boost/serialization/map.hpp>
#include <boost/mpi/collectives/broadcast.hpp>
#include <boost/mpi/collectives/gather.hpp>
#endif

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/linspace.h"

#include "tools/output_std_vector.h"

#include "Field/XFieldArea.h" //Needed because we are using area XField
#include "Field/XFieldVolume.h"
#include "Field/Partition/XField_Lagrange.h"

#include "Field/output_grm.h"
#include "Field/output_Tecplot.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include "unit/Field/XField2D_CheckTraceCoord2D_btest.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField3D_Box_Hex_X1.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( XField_Lagrange_Partition_test_suite )
#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DOFs_Even_test )
{
  XField_Lagrange<PhysD2>::VectorX X;

  // global communicator
  mpi::communicator world;

  // construct communicators for each available size
  for (int size = 1; size <= world.size(); size++)
  {
    int color = world.rank() < size ? 0 : 1;

    // split the world into two groups
    mpi::communicator comm = world.split(color);

    // only use group 0 for testing
    if (color == 1) continue;

    XField_Lagrange<PhysD2> xfldin(comm);

    int ii = 4;
    int jj = size;
    int nDOF_global = ii*jj;

    xfldin.sizeDOF(nDOF_global);

    // Add coordinates to rank 0
    if (comm.rank() == 0)
      for (int j = 0; j < jj; j++)
        for (int i = 0; i < ii; i++)
        {
          X = {i, j};
          xfldin.addDOF(X);
        }

    // DOFs should be equally distributed
    BOOST_CHECK_EQUAL( ii, xfldin.nDOF() );

    // Check that the DOFs were distributed to ranks
    for (int i = 0; i < ii; i++)
    {
      BOOST_CHECK_EQUAL( comm.rank()*ii + i, xfldin.DOF(i).idof);
      BOOST_CHECK_EQUAL(                  i, xfldin.DOF(i).X[0]);
      BOOST_CHECK_EQUAL(        comm.rank(), xfldin.DOF(i).X[1]);
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DOFs_Odd_test )
{
  XField_Lagrange<PhysD2>::VectorX X;

  // global communicator
  mpi::communicator world;

  // construct communicators for each available size
  for (int size = 1; size <= world.size(); size++)
  {
    int color = world.rank() < size ? 0 : 1;

    // split the world into two groups
    mpi::communicator comm = world.split(color);

    // only use group 0 for testing
    if (color == 1) continue;

    XField_Lagrange<PhysD2> xfldin(comm);

    int ii = 4;
    int jj = size;
    int nDOF_global = ii*jj + 1; // one more odd DOF

    xfldin.sizeDOF(nDOF_global);

    // Add coordinates to rank 0
    if (comm.rank() == 0)
    {
      for (int j = 0; j < jj; j++)
        for (int i = 0; i < ii; i++)
        {
          X = {i, j};
          xfldin.addDOF(X);
        }

      // Add the last odd DOF
      X = {100, 200};
      xfldin.addDOF(X);
    }

    // DOFs should be equally distributed
    if (comm.rank() == comm.size()-1)
      BOOST_CHECK_EQUAL( ii+1, xfldin.nDOF() );
    else
      BOOST_CHECK_EQUAL(   ii, xfldin.nDOF() );

    // Check that the DOFs were distributed to ranks
    for (int i = 0; i < ii; i++)
    {
      BOOST_CHECK_EQUAL( comm.rank()*ii + i, xfldin.DOF(i).idof);
      BOOST_CHECK_EQUAL(                  i, xfldin.DOF(i).X[0]);
      BOOST_CHECK_EQUAL(        comm.rank(), xfldin.DOF(i).X[1]);
    }

    // check the last odd DOF
    if (comm.rank() == comm.size()-1)
    {
      int i = ii;
      BOOST_CHECK_EQUAL( nDOF_global-1, xfldin.DOF(i).idof);
      BOOST_CHECK_EQUAL(           100, xfldin.DOF(i).X[0]);
      BOOST_CHECK_EQUAL(           200, xfldin.DOF(i).X[1]);
    }
  }
}
#endif
//----------------------------------------------------------------------------//
// create triangle grid in unit box with ii x jj x 2 (triangle) elements
class XField2D_Box_Triangle_Lagrange_2Group_Q1 : public XField<PhysD2,TopoD2>
{
public:

  XField2D_Box_Triangle_Lagrange_2Group_Q1(XField<PhysD2,TopoD2>& xfld, XFieldBalance graph,
                                           int ii, int jj, Real xmin, Real xmax, Real ymin, Real ymax )
   : XField<PhysD2,TopoD2>(xfld.comm())
  {
    XField_Lagrange<PhysD2> xfldin(xfld, graph);

    init(xfldin, ii, jj, xmin, xmax, ymin, ymax);
  }

  XField2D_Box_Triangle_Lagrange_2Group_Q1(mpi::communicator comm,
                                           int ii, int jj, Real xmin, Real xmax, Real ymin, Real ymax )
   : XField<PhysD2,TopoD2>(comm)
  {
    XField_Lagrange<PhysD2> xfldin(comm);

    init(xfldin, ii, jj, xmin, xmax, ymin, ymax);
  }

protected:
  void init(XField_Lagrange<PhysD2>& xfldin, int ii, int jj, Real xmin, Real xmax, Real ymin, Real ymax)
  {
    mpi::communicator comm = *xfldin.comm();

    XField_Lagrange<PhysD2>::VectorX X;

    int order = 1;
    int nnode = (ii*order + 1)*(jj*order + 1);
    int nCell = ii*jj*2;

    Real xdiv = 0*(xmax - xmin)/Real(ii)/10;
    Real ydiv = 0*(ymax - ymin)/Real(ii)/10;

    // create the wavy grid-coordinate DOF arrays
    xfldin.sizeDOF( nnode );

    if (comm.rank() == 0)
      for (int j = 0; j < (jj*order + 1); j++)
      {
        for (int i = 0; i < (ii*order + 1); i++)
        {
          X[0] = xmin + (xmax - xmin)*i/Real(ii*order) + xdiv*sin(2*PI*j/Real(jj*order));
          X[1] = ymin + (ymax - ymin)*j/Real(jj*order) + ydiv*sin(2*PI*i/Real(ii*order));
          xfldin.addDOF(X);
        }
      }


    // Start the process of adding cells
    xfldin.sizeCells(nCell);

    // Add elements to rank 0
    if (comm.rank() == 0)
    {
      // split the elements into two groups
      for (int group = 0; group < 2; group++)
      {
        for (int j = 0; j < jj; j++)
        {
          for (int i = ii/2*group; i < ii/2*(group+1); i++)
          {
            /* Node indexes in the grid

                2-----------3
                | \         |
                |   \       |
                |     \     |
                |       \   |
                |         \ |
                0-----------1
            */

            /* SANS canonical node indexes for an element

              2
              | \
              |   \
              |     \
              |       \
              |         \
              0-----------1
            */

            int n0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
            int n1 = n0 + 1;
            int n2 = (j*order)*(ii*order + 1) + i*order + 1*(ii*order + 1);
            int n3 = n2 + 1;

            std::vector<int> triL = {n0, n1, n2}; // nodes
            std::vector<int> triR = {n3, n2, n1}; // nodes

            // add the indices to the group
            xfldin.addCell(group, eTriangle, order, triL);
            xfldin.addCell(group, eTriangle, order, triR);
          }
        }
      }
    }


    // Start the process of adding boundary trace elements
    xfldin.sizeBoundaryTrace(2*ii + 2*jj);


    // Add boundary elements to rank 0
    if (comm.rank() == 0)
    {
      // lower boundary
      int bcgroup = -1;
      {
        int j = 0;
        for (int cellgroup = 0; cellgroup < 2; cellgroup++)
        {
          bcgroup++;
          for (int i = ii/2*cellgroup; i < ii/2*(cellgroup+1); i++)
          {
            int n0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
            int n1 = n0 + 1;

            xfldin.addBoundaryTrace( bcgroup, eLine, {n0, n1} );
          }
        }
      }

      // right boundary
      {
        bcgroup++;

        int i = ii-1;
        for (int j = 0; j < jj; j++)
        {
          int n0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
          int n1 = n0 + 1;
          int n2 = (j*order)*(ii*order + 1) + i*order + 1*(ii*order + 1);
          int n3 = n2 + 1;

          xfldin.addBoundaryTrace( bcgroup, eLine, {n1, n3} );
        }
      }

      // upper boundary
      {
        int j = jj - 1;

        for (int cellgroup = 1; cellgroup >= 0; cellgroup--)
        {
          bcgroup++;
          for (int i = ii/2*(cellgroup+1) - 1; i >= ii/2*cellgroup; i--)
          {
            int n2 = (j*order)*(ii*order + 1) + i*order + 1*(ii*order + 1);
            int n3 = n2 + 1;

            xfldin.addBoundaryTrace( bcgroup, eLine, {n3, n2} );
          }
        }
      }

      // left boundary
      {
        bcgroup++;

        int i = 0;
        for (int j = jj-1; j >= 0; j--)
        {
          int n0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
          int n2 = (j*order)*(ii*order + 1) + i*order + 1*(ii*order + 1);

          xfldin.addBoundaryTrace( bcgroup, eLine, {n2, n0} );
        }
      }
    } // rank

    this->buildFrom( xfldin );
  }
};
#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField_Q1_test )
{
  XField_Lagrange<PhysD2>::VectorX X;

  int ii = 4;
  int jj = 5;
  int order = 1;
  int nNode_global = (ii + 1)*(jj + 1);
  int nDOF_global;
  int nElem;
  int nBoundaryElem;

  std::map<int,int> globalNodeMap;

  // construct maps that map the nodes in the global numbering to a continuous numbering
  for (int j = 0; j < jj; j++)
    for (int i = 0; i < ii; i++)
    {
      int n0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
      int n1 = n0 + order;
      int n2 = (j*order)*(ii*order + 1) + i*order + 1*(ii*order + 1);
      int n3 = n2 + order;

      globalNodeMap[n0] = j*(ii + 1) + i + 0*(ii + 1);
      globalNodeMap[n1] = globalNodeMap.at(n0) + 1;
      globalNodeMap[n2] = j*(ii + 1) + i + 1*(ii + 1);
      globalNodeMap[n3] = globalNodeMap.at(n2) + 1;
    }

  // global communicator
  mpi::communicator world;

  // Generate a global grid for testing
  XField2D_Box_Triangle_Lagrange_2Group_Q1 xfld_global(world.split(world.rank()), ii, jj, 0, ii*order, 0, jj*order);

  typedef typename XField2D_Box_Triangle_X1::template FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef typename XField2D_Box_Triangle_X1::template FieldTraceGroupType<Line>    XFieldTraceGroupType;
  typedef typename XFieldCellGroupType::template ElementType<>  ElementXFieldCell;
  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTrace;

  ElementXFieldCell xfldCellElem(xfld_global.getCellGroup<Triangle>(0).basis());

  // construct communicators for each available size
  for (int size = 1; size <= world.size(); size++)
  {
    int color = world.rank() < size ? 0 : 1;

    // split the world into two groups
    mpi::communicator comm = world.split(color);

    // only use group 0 for testing
    if (color == 1) continue;

    XField_Lagrange<PhysD2> xfldin(comm);

    if (comm.rank() == 0)
    {
      nDOF_global = (ii*order + 1)*(jj*order + 1);
      nElem = 2*ii*jj;
    }

    xfldin.sizeDOF(nDOF_global);

    // Add coordinates to rank 0
    if (comm.rank() == 0)
    {
      for (int j = 0; j < jj*order + 1; j++)
        for (int i = 0; i < ii*order + 1; i++)
        {
          X = {i, j};
          xfldin.addDOF(X);
        }
    }

    // Start the process of adding cells
    xfldin.sizeCells(nElem);

    std::map<int, std::pair<int,int>> mapCellID;

    // Add elements to rank 0
    if (comm.rank() == 0)
    {
      int cellID = 0;

      // split the elements into two groups
      for (int group = 0; group < 2; group++)
      {
        int elem = 0;
        for (int j = 0; j < jj; j++)
          for (int i = ii/2*group; i < ii/2*(group+1); i++)
          {
            /* Node indexes in the grid

              2-----------3
              | \         |
              |   \       |
              |     \     |
              |       \   |
              |         \ |
              0-----------1
            */

            /* SANS canonical node indexes for an element

              2
              | \
              |   \
              |     \
              |       \
              |         \
              0-----------1
            */

            int n0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
            int n1 = n0 + 1;
            int n2 = (j*order)*(ii*order + 1) + i*order + 1*(ii*order + 1);
            int n3 = n2 + 1;

            std::vector<int> triL = {n0, n1, n2}; // nodes
            std::vector<int> triR = {n3, n2, n1}; // nodes

            xfldin.addCell(group, eTriangle, order, triL);
            mapCellID[cellID] = {group, elem}; cellID++; elem++;

            xfldin.addCell(group, eTriangle, order, triR);
            mapCellID[cellID] = {group, elem}; cellID++; elem++;
          }
      }
    }

    if (comm.rank() == 0)
      nBoundaryElem = 2*ii + 2*jj;

    // Start the process of adding boundary trace elements
    xfldin.sizeBoundaryTrace(nBoundaryElem);

    std::map< int, std::pair<int,int> > mapEdgeID;

    // Add boundary elements to rank 0
    if (comm.rank() == 0)
    {
      // lower boundary
      int bcgroup = -1;
      int edgeID = 0;
      {
        int j = 0;
        for (int cellgroup = 0; cellgroup < 2; cellgroup++)
        {
          bcgroup++;
          for (int i = ii/2*cellgroup; i < ii/2*(cellgroup+1); i++)
          {
            int n0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
            int n1 = n0 + 1;
            int edge = i - ii/2*cellgroup;

            mapEdgeID[edgeID] = {bcgroup, edge};

            xfldin.addBoundaryTrace( bcgroup, eLine, {n0, n1} );
            edgeID++;
          }
        }
      }

      // right boundary
      {
        bcgroup++;

        int i = ii-1;
        for (int j = 0; j < jj; j++)
        {
          int n0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
          int n1 = n0 + 1;
          int n2 = (j*order)*(ii*order + 1) + i*order + 1*(ii*order + 1);
          int n3 = n2 + 1;

          int edge = j;
          mapEdgeID[edgeID] = {bcgroup, edge};

          xfldin.addBoundaryTrace( bcgroup, eLine, {n1, n3} );
          edgeID++;
        }
      }

      // upper boundary
      {
        int j = jj - 1;

        for (int cellgroup = 1; cellgroup >= 0; cellgroup--)
        {
          bcgroup++;
          for (int i = ii/2*(cellgroup+1) - 1; i >= ii/2*cellgroup; i--)
          {
            int n2 = (j*order)*(ii*order + 1) + i*order + 1*(ii*order + 1);
            int n3 = n2 + 1;

            int edge = ii/2*(cellgroup+1)-1 - i;
            mapEdgeID[edgeID] = {bcgroup, edge};

            xfldin.addBoundaryTrace( bcgroup, eLine, {n3, n2} );
            edgeID++;
          }
        }
      }

      // left boundary
      {
        bcgroup++;

        int i = 0;
        for (int j = jj-1; j >= 0; j--)
        {
          int n0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
          int n2 = (j*order)*(ii*order + 1) + i*order + 1*(ii*order + 1);

          int edge = jj-1 - j;
          mapEdgeID[edgeID] = {bcgroup, edge};

          xfldin.addBoundaryTrace( bcgroup, eLine, {n2, n0} );
          edgeID++;
        }
      }
    } // rank

    xfldin.balance();

    // make sure the node count works correctly
    BOOST_REQUIRE_EQUAL( nNode_global, xfldin.nNode_global() );

    std::map<int,int> nodeRank = xfldin.nodeRank();

#ifdef SANS_MPI
    // send the maps to the other ranks, as it was only constructed on rank 0
    boost::mpi::broadcast(comm, mapCellID, 0);
    boost::mpi::broadcast(comm, mapEdgeID, 0);

    std::vector<std::map<int,int>> nodeRankAll;
    boost::mpi::gather(comm, xfldin.nodeRank(), nodeRankAll, 0);

    std::map<int,int> nodeRankGlobal;
    if ( comm.rank() == 0 )
      for (const std::map<int,int>& nodeRanks : nodeRankAll)
        nodeRankGlobal.insert(nodeRanks.begin(), nodeRanks.end());

    boost::mpi::broadcast(comm, nodeRankGlobal, 0);
#else
    std::map<int,int> nodeRankGlobal = nodeRank;
#endif

    const Real small_tol = 1e-11;
    const Real close_tol = 1e-11;

    std::set<int> elemOnRank;

    for (int rank = 0; rank < comm.size(); rank++)
    {
      // only let one processor print at a time in order
      comm.barrier();
      if (rank != comm.rank()) continue;

      for (int group = 0; group < 2; group++)
      {
        const XFieldCellGroupType& xfldCellGroup = xfld_global.getCellGroup<Triangle>(group);

        std::vector<int> globalElemMap(xfldCellGroup.basis()->nBasis());

        for (int elem = 0; elem < xfldin.cellGroup(group)->nElem(); elem++)
        {
          const int elemID = xfldin.cellGroup(group)->elem(elem).elemID;
          const std::vector<int>& elemMap = xfldin.cellGroup(group)->elem(elem).elemMap;

          // The element should only occur once on any given rank
          BOOST_CHECK( elemOnRank.find(elemID) == elemOnRank.end() );
          elemOnRank.insert(elemID);

          std::pair<int,int> cellgroup_cell = mapCellID.at(elemID);
          int cellgroup = cellgroup_cell.first;
          int cell = cellgroup_cell.second;

          BOOST_CHECK_EQUAL(group, cellgroup);

          // get the element from the global grid
          xfldCellGroup.getElement(xfldCellElem, cell);

          for (std::size_t n = 0; n < elemMap.size(); n++)
          {
            X = xfldin.DOF(xfldin.global2localDOFmap(elemMap[n])).X;

            xfldCellGroup.associativity(cell).getGlobalMapping(globalElemMap.data(), globalElemMap.size());

            // check that the parallel DOF map matches the global map
            BOOST_CHECK_EQUAL(globalElemMap[n], elemMap[n]);

            // check that the node ranks are consistent on all partitions
            BOOST_CHECK_EQUAL(nodeRankGlobal.at(elemMap[n]), nodeRank.at(elemMap[n]));

            // check that the DOF coordinates match
            SANS_CHECK_CLOSE(xfldCellElem.DOF(n)[0], X[0], small_tol, close_tol);
            SANS_CHECK_CLOSE(xfldCellElem.DOF(n)[1], X[1], small_tol, close_tol);
          }
        } // elem
      } // group


      // loop over all boundary groups
      for (int group = 0; group < xfld_global.nBoundaryTraceGroups(); group++)
      {
        const XFieldTraceGroupType& xfldBtrace = xfld_global.getBoundaryTraceGroup<Line>(group);
        ElementXFieldTrace xfldTraceElem(xfldBtrace.basis());

        std::vector<int> globalElemMap(xfldBtrace.basis()->nBasis());
        elemOnRank.clear();

        for (int elem = 0; elem < xfldin.getBoundaryTraceGroup(group)->nElem(); elem++)
        {
          const int elemID = xfldin.getBoundaryTraceGroup(group)->elem(elem).elemID;
          const std::vector<int>& elemMap = xfldin.getBoundaryTraceGroup(group)->elem(elem).elemMap;

          std::pair<int,int> bcgroup_edge =  mapEdgeID.at(elemID);
          int bcgroup = bcgroup_edge.first;
          int edge = bcgroup_edge.second;

          BOOST_CHECK_EQUAL( bcgroup, group );

          // The boundary element should only occur once on any given rank
          BOOST_CHECK( elemOnRank.find(elemID) == elemOnRank.end() );
          elemOnRank.insert(elemID);

          // get the boundary element from the global grid
          xfldBtrace.getElement(xfldTraceElem, edge);

          for (std::size_t n = 0; n < elemMap.size(); n++)
          {
            X = xfldin.DOF(xfldin.global2localDOFmap(elemMap[n])).X;

            xfldBtrace.associativity(edge).getGlobalMapping(globalElemMap.data(), globalElemMap.size());

            // check that the parallel DOF map matches the global map
            BOOST_CHECK_EQUAL(globalElemMap[n], elemMap[n]);

            // check that the node ranks are consistent on all partitions
            BOOST_CHECK_EQUAL(nodeRankGlobal.at(elemMap[n]), nodeRank.at(elemMap[n]));

            // check that the DOF coordinates match
            SANS_CHECK_CLOSE(xfldTraceElem.DOF(n)[0], X[0], small_tol, close_tol);
            SANS_CHECK_CLOSE(xfldTraceElem.DOF(n)[1], X[1], small_tol, close_tol);
          }
        } // elem
      } // group
    } // rank

  } // comm size
}
#endif
#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField_Q1_Serialize_test )
{
  typedef typename XField<PhysD2,TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef typename XField<PhysD2,TopoD2>::template FieldTraceGroupType<Line>    XFieldTraceGroupType;
  typedef typename XFieldCellGroupType::template ElementType<>  ElementXFieldCell;
  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTrace;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  XField_Lagrange<PhysD2>::VectorX X;

  int ii = 4;
  int jj = 5;
  int order = 1;

  // global communicator
  mpi::communicator world;

  // Generate a global grid for testing (identical on all processors)
  XField2D_Box_Triangle_Lagrange_2Group_Q1 xfld_global(world.split(world.rank()), ii, jj, 0, ii*order, 0, jj*order);

  world.barrier();

  // construct communicators for each available size
  for (int size = 1; size <= world.size(); size++)
  {
    int color = world.rank() < size ? 0 : 1;

    // split the world into two groups
    mpi::communicator comm = world.split(color);

    // only use group 0 for testing
    if (color == 1) continue;

    // Generate a partitioned grid
    XField2D_Box_Triangle_Lagrange_2Group_Q1 xfld_part(comm, ii, jj, 0, ii*order, 0, jj*order);

    // serialize the grid
    XField<PhysD2,TopoD2> xfld_serial(xfld_part, XFieldBalance::Serial);

    if (comm.rank() == 0)
    {
      BOOST_REQUIRE_EQUAL(xfld_global.nDOF(), xfld_serial.nDOF());

      for (int n = 0; n < xfld_global.nDOF(); n++)
      {
        // check that the DOF coordinates match
        SANS_CHECK_CLOSE(xfld_global.DOF(n)[0], xfld_serial.DOF(n)[0], small_tol, close_tol);
        SANS_CHECK_CLOSE(xfld_global.DOF(n)[1], xfld_serial.DOF(n)[1], small_tol, close_tol);
      }

      BOOST_REQUIRE_EQUAL(xfld_global.nCellGroups(), xfld_serial.nCellGroups());

      for (int group = 0; group < 2; group++)
      {
        const XFieldCellGroupType& xfldCellGroup_global = xfld_global.getCellGroup<Triangle>(group);
        const XFieldCellGroupType& xfldCellGroup_serial = xfld_serial.getCellGroup<Triangle>(group);

        BOOST_REQUIRE_EQUAL(xfldCellGroup_global.nElem(), xfldCellGroup_serial.nElem());

        std::vector<int> globalElemMap_global(xfldCellGroup_global.nBasis());
        std::vector<int> globalElemMap_serial(xfldCellGroup_serial.nBasis());

        ElementXFieldCell xfldCellElem_global(xfldCellGroup_global.basis());
        ElementXFieldCell xfldCellElem_serial(xfldCellGroup_serial.basis());

        for (int elem = 0; elem < xfldCellGroup_global.nElem(); elem++)
        {
          // get the element from the global grid
          xfldCellGroup_global.getElement(xfldCellElem_global, elem);
          xfldCellGroup_serial.getElement(xfldCellElem_serial, elem);

          xfldCellGroup_global.associativity(elem).getGlobalMapping(globalElemMap_global.data(), globalElemMap_global.size());
          xfldCellGroup_serial.associativity(elem).getGlobalMapping(globalElemMap_serial.data(), globalElemMap_serial.size());

          for (int n = 0; n < xfldCellElem_global.nDOF(); n++)
          {
            // check that the parallel DOF map matches the global map
            BOOST_CHECK_EQUAL(globalElemMap_global[n], globalElemMap_serial[n]);

            // check that the DOF coordinates match
            SANS_CHECK_CLOSE(xfldCellElem_global.DOF(n)[0], xfldCellElem_serial.DOF(n)[0], small_tol, close_tol);
            SANS_CHECK_CLOSE(xfldCellElem_global.DOF(n)[1], xfldCellElem_serial.DOF(n)[1], small_tol, close_tol);
          }
        } // elem
      } // group

      // loop over all boundary groups
      for (int group = 0; group < xfld_global.nBoundaryTraceGroups(); group++)
      {
        const XFieldTraceGroupType& xfldBtrace_global = xfld_global.getBoundaryTraceGroup<Line>(group);
        const XFieldTraceGroupType& xfldBtrace_serial = xfld_serial.getBoundaryTraceGroup<Line>(group);

        ElementXFieldTrace xfldTraceElem_global(xfldBtrace_global.basis());
        ElementXFieldTrace xfldTraceElem_serial(xfldBtrace_serial.basis());

        std::vector<int> globalElemMap_global(xfldBtrace_global.basis()->nBasis());
        std::vector<int> globalElemMap_serial(xfldBtrace_serial.basis()->nBasis());

        for (int elem = 0; elem < xfldBtrace_global.nElem(); elem++)
        {
          // get the boundary element from the global grid
          xfldBtrace_global.getElement(xfldTraceElem_global, elem);
          xfldBtrace_serial.getElement(xfldTraceElem_serial, elem);

          xfldBtrace_global.associativity(elem).getGlobalMapping(globalElemMap_global.data(), globalElemMap_global.size());
          xfldBtrace_serial.associativity(elem).getGlobalMapping(globalElemMap_serial.data(), globalElemMap_serial.size());

          for (std::size_t n = 0; n < globalElemMap_global.size(); n++)
          {
            // check that the DOF map matches the global map
            BOOST_CHECK_EQUAL(globalElemMap_global[n], globalElemMap_serial[n]);

            // check that the DOF coordinates match
            SANS_CHECK_CLOSE(xfldTraceElem_global.DOF(n)[0], xfldTraceElem_serial.DOF(n)[0], small_tol, close_tol);
            SANS_CHECK_CLOSE(xfldTraceElem_global.DOF(n)[1], xfldTraceElem_serial.DOF(n)[1], small_tol, close_tol);
          }
        } // elem
      } // group
    }
    else
    {
      // all othe processors should have an empty xfield
      BOOST_CHECK_EQUAL(0, xfld_serial.nDOF());
      BOOST_CHECK_EQUAL(2, xfld_serial.nCellGroups());
      BOOST_CHECK_EQUAL(6, xfld_serial.nBoundaryTraceGroups());
      BOOST_CHECK_EQUAL(0, xfld_serial.nInteriorTraceGroups());

      for (int group = 0; group < xfld_serial.nCellGroups(); group++)
        BOOST_CHECK_EQUAL(0, xfld_serial.getCellGroupBase(group).nElem() );

      for (int group = 0; group < xfld_serial.nBoundaryTraceGroups(); group++)
        BOOST_CHECK_EQUAL(0, xfld_serial.getBoundaryTraceGroupBase(group).nElem() );

      for (int group = 0; group < xfld_serial.nInteriorTraceGroups(); group++)
        BOOST_CHECK_EQUAL(0, xfld_serial.getInteriorTraceGroupBase(group).nElem() );
    }

  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField_Q1_CopyPartition_test )
{
  typedef typename XField<PhysD2,TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef typename XField<PhysD2,TopoD2>::template FieldTraceGroupType<Line>    XFieldTraceGroupType;
  typedef typename XFieldCellGroupType::template ElementType<>  ElementXFieldCell;
  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTrace;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  int ii = 4;
  int jj = 5;
  int order = 1;

  // global communicator
  mpi::communicator world;

  // construct communicators for each available size
  for (int size = 1; size <= world.size(); size++)
  {
    int color = world.rank() < size ? 0 : 1;

    // split the world into two groups
    mpi::communicator comm = world.split(color);

    // only use group 0 for testing
    if (color == 1) continue;

    // Generate a partitioned grid
    XField2D_Box_Triangle_Lagrange_2Group_Q1 xfld_part(comm, ii, jj, 0, ii*order, 0, jj*order);

    // crete a grid with identical partition
    XField2D_Box_Triangle_Lagrange_2Group_Q1 xfld_copy(xfld_part, XFieldBalance::CellPartitionCopy, ii, jj, 0, ii*order, 0, jj*order);

    BOOST_REQUIRE_EQUAL(xfld_part.nDOF(), xfld_copy.nDOF());

    for (int n = 0; n < xfld_part.nDOF(); n++)
    {
      // check that the DOF coordinates match
      SANS_CHECK_CLOSE(xfld_part.DOF(n)[0], xfld_copy.DOF(n)[0], small_tol, close_tol);
      SANS_CHECK_CLOSE(xfld_part.DOF(n)[1], xfld_copy.DOF(n)[1], small_tol, close_tol);
    }

    BOOST_REQUIRE_EQUAL(xfld_part.nCellGroups(), xfld_copy.nCellGroups());

    for (int group = 0; group < 2; group++)
    {
      const XFieldCellGroupType& xfldCellGroup_part = xfld_part.getCellGroup<Triangle>(group);
      const XFieldCellGroupType& xfldCellGroup_copy = xfld_copy.getCellGroup<Triangle>(group);

      BOOST_REQUIRE_EQUAL(xfldCellGroup_part.nElem(), xfldCellGroup_copy.nElem());

      std::vector<int> globalElemMap_part(xfldCellGroup_part.nBasis());
      std::vector<int> globalElemMap_copy(xfldCellGroup_copy.nBasis());

      ElementXFieldCell xfldCellElem_part(xfldCellGroup_part.basis());
      ElementXFieldCell xfldCellElem_copy(xfldCellGroup_copy.basis());

      const std::vector<int>& cellIDs_part = xfld_part.cellIDs(group);
      const std::vector<int>& cellIDs_copy = xfld_copy.cellIDs(group);


      for (int elem = 0; elem < xfldCellGroup_part.nElem(); elem++)
      {
        // get the element from the global grid
        xfldCellGroup_part.getElement(xfldCellElem_part, elem);
        xfldCellGroup_copy.getElement(xfldCellElem_copy, elem);

        xfldCellGroup_part.associativity(elem).getGlobalMapping(globalElemMap_part.data(), globalElemMap_part.size());
        xfldCellGroup_copy.associativity(elem).getGlobalMapping(globalElemMap_copy.data(), globalElemMap_copy.size());

        // check the element ranks match
        BOOST_CHECK_EQUAL(xfldCellElem_part.rank(), xfldCellElem_copy.rank());

        // check that the global IDs match
        BOOST_CHECK_EQUAL(cellIDs_part[elem], cellIDs_copy[elem]);

        for (int n = 0; n < xfldCellElem_part.nDOF(); n++)
        {
          // check that the parallel DOF map matches the global map
          BOOST_CHECK_EQUAL(globalElemMap_part[n], globalElemMap_copy[n]);

          // check that the DOF coordinates match
          SANS_CHECK_CLOSE(xfldCellElem_part.DOF(n)[0], xfldCellElem_copy.DOF(n)[0], small_tol, close_tol);
          SANS_CHECK_CLOSE(xfldCellElem_part.DOF(n)[1], xfldCellElem_copy.DOF(n)[1], small_tol, close_tol);
        }
      } // elem
    } // group

    // loop over all boundary groups
    for (int group = 0; group < xfld_part.nBoundaryTraceGroups(); group++)
    {
      const XFieldTraceGroupType& xfldBtrace_part = xfld_part.getBoundaryTraceGroup<Line>(group);
      const XFieldTraceGroupType& xfldBtrace_copy = xfld_copy.getBoundaryTraceGroup<Line>(group);

      ElementXFieldTrace xfldTraceElem_part(xfldBtrace_part.basis());
      ElementXFieldTrace xfldTraceElem_copy(xfldBtrace_copy.basis());

      std::vector<int> globalElemMap_part(xfldBtrace_part.basis()->nBasis());
      std::vector<int> globalElemMap_copy(xfldBtrace_copy.basis()->nBasis());

      const std::vector<int>& boundaryTraceIDs_part = xfld_part.boundaryTraceIDs(group);
      const std::vector<int>& boundaryTraceIDs_copy = xfld_copy.boundaryTraceIDs(group);

      for (int elem = 0; elem < xfldBtrace_part.nElem(); elem++)
      {
        // get the boundary element from the global grid
        xfldBtrace_part.getElement(xfldTraceElem_part, elem);
        xfldBtrace_copy.getElement(xfldTraceElem_copy, elem);

        xfldBtrace_part.associativity(elem).getGlobalMapping(globalElemMap_part.data(), globalElemMap_part.size());
        xfldBtrace_copy.associativity(elem).getGlobalMapping(globalElemMap_copy.data(), globalElemMap_copy.size());

        // check the element ranks match
        BOOST_CHECK_EQUAL(xfldTraceElem_part.rank(), xfldTraceElem_copy.rank());

        // check that the global IDs match
        BOOST_CHECK_EQUAL(boundaryTraceIDs_part[elem], boundaryTraceIDs_copy[elem]);

        for (std::size_t n = 0; n < globalElemMap_part.size(); n++)
        {
          // check that the DOF map matches the global map
          BOOST_CHECK_EQUAL(globalElemMap_part[n], globalElemMap_copy[n]);

          // check that the DOF coordinates match
          SANS_CHECK_CLOSE(xfldTraceElem_part.DOF(n)[0], xfldTraceElem_copy.DOF(n)[0], small_tol, close_tol);
          SANS_CHECK_CLOSE(xfldTraceElem_part.DOF(n)[1], xfldTraceElem_copy.DOF(n)[1], small_tol, close_tol);
        }
      } // elem
    } // group
  }
}

//----------------------------------------------------------------------------//
// create triangle grid in unit box with ii x jj x 2 (triangle) elements
class XField2D_Box_Triangle_Lagrange_2Group_Q2 : public XField<PhysD2,TopoD2>
{
public:

  XField2D_Box_Triangle_Lagrange_2Group_Q2(XField<PhysD2,TopoD2>& xfld, XFieldBalance graph,
                                           int ii, int jj, Real xmin, Real xmax, Real ymin, Real ymax )
   : XField<PhysD2,TopoD2>(xfld.comm())
  {
    XField_Lagrange<PhysD2> xfldin(xfld, graph);

    init(xfldin, ii, jj, xmin, xmax, ymin, ymax);
  }

  XField2D_Box_Triangle_Lagrange_2Group_Q2(mpi::communicator comm,
                                           int ii, int jj, Real xmin, Real xmax, Real ymin, Real ymax )
   : XField<PhysD2,TopoD2>(comm)
  {
    XField_Lagrange<PhysD2> xfldin(comm);

    init(xfldin, ii, jj, xmin, xmax, ymin, ymax);
  }

protected:
  void init(XField_Lagrange<PhysD2>& xfldin, int ii, int jj, Real xmin, Real xmax, Real ymin, Real ymax)
  {
    mpi::communicator comm = *xfldin.comm();

    XField_Lagrange<PhysD2>::VectorX X;

    int order = 2;
    int nnode = (ii*order + 1)*(jj*order + 1);
    int nCell = ii*jj*2;

    // initialize adding DOF
    xfldin.sizeDOF( nnode );

    if (comm.rank() == 0)
    {
      for (int j = 0; j < (jj*order + 1); j++)
      {
        for (int i = 0; i < (ii*order + 1); i++)
        {
          X[0] = xmin + (xmax - xmin)*i/Real(ii*order);
          X[1] = ymin + (ymax - ymin)*j/Real(jj*order);
          xfldin.addDOF(X);
        }
      }
    }

    // Start the process of adding cells
    xfldin.sizeCells(nCell);

    // Add elements to rank 0
    if (comm.rank() == 0)
    {
      // split the elements into two groups
      for (int group = 0; group < 2; group++)
      {
        for (int j = 0; j < jj; j++)
        {
          for (int i = ii/2*group; i < ii/2*(group+1); i++)
          {
            /* Node indexes in the grid

                6-----7-----8
                | \         |
                |   \       |
                3     4     5
                |       \   |
                |         \ |
                0-----1-----2
            */

            /* SANS canonical node indexes for an element

              2
              | \
              |   \
              4     3
              |       \
              |         \
              0-----5-----1
            */


            int m0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);

            int n0 = m0 + 0;
            int n1 = m0 + 1;
            int n2 = m0 + 2;

            int m1 = (j*order)*(ii*order + 1) + i*order + 1*(ii*order + 1);

            int n3 = m1 + 0;
            int n4 = m1 + 1;
            int n5 = m1 + 2;

            int m2 = (j*order)*(ii*order + 1) + i*order + 2*(ii*order + 1);

            int n6  = m2 + 0;
            int n7  = m2 + 1;
            int n8  = m2 + 2;

            std::vector<int> triL = {n0, n2, n6,  // nodes
                                     n4,          // edge 1
                                     n3,          // edge 2
                                     n1};         // edge 3

            std::vector<int> triR = {n8, n6, n2,  // nodes
                                     n4,          // edge 1
                                     n5,          // edge 2
                                     n7};         // edge 3

            // add the indices to the group
            xfldin.addCell(group, eTriangle, order, triL);
            xfldin.addCell(group, eTriangle, order, triR);
          }
        }
      }
    }

    // Start the process of adding boundary trace elements
    xfldin.sizeBoundaryTrace(2*ii + 2*jj);

    // Add boundary elements to rank 0
    if (comm.rank() == 0)
    {
      // lower boundary
      int bcgroup = -1;
      {
        int j = 0;
        for (int cellgroup = 0; cellgroup < 2; cellgroup++)
        {
          bcgroup++;
          for (int i = ii/2*cellgroup; i < ii/2*(cellgroup+1); i++)
          {
            int m0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);

            int n0 = m0 + 0;
            int n2 = m0 + 2;

            xfldin.addBoundaryTrace( bcgroup, eLine, {n0, n2} );
          }
        }
      }

      // right boundary
      {
        bcgroup++;

        int i = ii-1;
        for (int j = 0; j < jj; j++)
        {
          int m0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);

          int n2 = m0 + 2;

          int m2 = (j*order)*(ii*order + 1) + i*order + 2*(ii*order + 1);

          int n8  = m2 + 2;

          xfldin.addBoundaryTrace( bcgroup, eLine, {n2, n8} );
        }
      }

      // upper boundary
      {
        int j = jj - 1;

        for (int cellgroup = 1; cellgroup >= 0; cellgroup--)
        {
          bcgroup++;
          for (int i = ii/2*(cellgroup+1) - 1; i >= ii/2*cellgroup; i--)
          {
            int m2 = (j*order)*(ii*order + 1) + i*order + 2*(ii*order + 1);

            int n6  = m2 + 0;
            int n8  = m2 + 2;

            xfldin.addBoundaryTrace( bcgroup, eLine, {n8, n6} );
          }
        }
      }

      // left boundary
      {
        bcgroup++;

        int i = 0;
        for (int j = jj-1; j >= 0; j--)
        {
          int m0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);

          int n0 = m0 + 0;

          int m2 = (j*order)*(ii*order + 1) + i*order + 2*(ii*order + 1);

          int n6  = m2 + 0;

          xfldin.addBoundaryTrace( bcgroup, eLine, {n6, n0} );
        }
      }
    } // rank


    this->buildFrom( xfldin );
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Cells_Q2_test )
{
  XField_Lagrange<PhysD2>::VectorX X;

  int ii = 4;
  int jj = 5;
  int order = 2;
  int nNode_global = (ii + 1)*(jj + 1);
  int nDOF_global;
  int nElem;
  int nBoundaryElem;

  std::map<int,int> globalNodeMap;

  // construct maps that map the nodes in the global numbering to a continuous numbering
  for (int j = 0; j < jj; j++)
    for (int i = 0; i < ii; i++)
    {
      int n0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
      int n2 = n0 + order;
      int n6 = (j*order)*(ii*order + 1) + i*order + 2*(ii*order + 1);
      int n8 = n6 + order;

      globalNodeMap[n0] = j*(ii + 1) + i + 0*(ii + 1);
      globalNodeMap[n2] = globalNodeMap.at(n0) + 1;
      globalNodeMap[n6] = j*(ii + 1) + i + 1*(ii + 1);
      globalNodeMap[n8] = globalNodeMap.at(n6) + 1;
    }

  // global communicator
  mpi::communicator world;

  // Generate a grid global on each rank for testing
  XField2D_Box_Triangle_Lagrange_2Group_Q2 xfld_global(world.split(world.rank()), ii, jj, 0, ii*order, 0, jj*order);

  typedef typename XField<PhysD2,TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef typename XField<PhysD2,TopoD2>::template FieldTraceGroupType<Line>    XFieldTraceGroupType;
  typedef typename XFieldCellGroupType::template ElementType<>  ElementXFieldCell;
  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTrace;

  ElementXFieldCell xfldCellElem(xfld_global.getCellGroup<Triangle>(0).basis());


  // construct communicators for each available size
  for (int size = 1; size <= world.size(); size++)
  {
    int color = world.rank() < size ? 0 : 1;

    // split the world into two groups
    mpi::communicator comm = world.split(color);

    // only use group 0 for testing
    if (color == 1) continue;

    XField_Lagrange<PhysD2> xfldin(comm);

    if (comm.rank() == 0)
    {
      nDOF_global = (ii*order + 1)*(jj*order + 1);
      nElem = 2*ii*jj;
    }

    xfldin.sizeDOF(nDOF_global);

    // Add coordinates to rank 0
    if (comm.rank() == 0)
    {
      for (int j = 0; j < jj*order + 1; j++)
        for (int i = 0; i < ii*order + 1; i++)
        {
          X = {i, j};
          xfldin.addDOF(X);
        }
    }

    // Start the process of adding cells
    xfldin.sizeCells(nElem);

    std::map<int, std::pair<int,int>> mapCellID;

    // Add elements to rank 0
    if (comm.rank() == 0)
    {
      int cellID = 0;

      // split the elements into two groups
      for (int group = 0; group < 2; group++)
      {
        int elem = 0;

        for (int j = 0; j < jj; j++)
          for (int i = ii/2*group; i < ii/2*(group+1); i++)
          {
            /* Node indexes in the grid

                6-----7-----8
                | \         |
                |   \       |
                3     4     5
                |       \   |
                |         \ |
                0-----1-----2
            */

            /* SANS canonical node indexes for an element

              2
              | \
              |   \
              4     3
              |       \
              |         \
              0-----5-----1
            */


            int m0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);

            int n0 = m0 + 0;
            int n1 = m0 + 1;
            int n2 = m0 + 2;

            int m1 = (j*order)*(ii*order + 1) + i*order + 1*(ii*order + 1);

            int n3 = m1 + 0;
            int n4 = m1 + 1;
            int n5 = m1 + 2;

            int m2 = (j*order)*(ii*order + 1) + i*order + 2*(ii*order + 1);

            int n6  = m2 + 0;
            int n7  = m2 + 1;
            int n8  = m2 + 2;

            std::vector<int> triL = {n0, n2, n6,  // nodes
                                     n4,          // edge 1
                                     n3,          // edge 2
                                     n1};         // edge 3

            std::vector<int> triR = {n8, n6, n2,  // nodes
                                     n4,          // edge 1
                                     n5,          // edge 2
                                     n7};         // edge 3

            xfldin.addCell(group, eTriangle, order, triL);
            mapCellID[cellID] = {group, elem}; cellID++; elem++;

            xfldin.addCell(group, eTriangle, order, triR);
            mapCellID[cellID] = {group, elem}; cellID++; elem++;
          }
      }
    }

    if ( comm.rank() == 0 )
      nBoundaryElem = 2*ii + 2*jj;

    // Start the process of adding boundary trace elements
    xfldin.sizeBoundaryTrace(nBoundaryElem);

    std::map< int, std::pair<int,int> > mapEdgeID;

    // Add boundary elements to rank 0
    if (comm.rank() == 0)
    {
      // lower boundary
      int bcgroup = -1;
      int edgeID = 0;
      {
        int j = 0;
        for (int cellgroup = 0; cellgroup < 2; cellgroup++)
        {
          bcgroup++;
          for (int i = ii/2*cellgroup; i < ii/2*(cellgroup+1); i++)
          {
            int m0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
            int n0 = m0 + 0;
            int n2 = m0 + 2;
            int edge = i - ii/2*cellgroup;

            mapEdgeID[edgeID] = {bcgroup, edge};

            xfldin.addBoundaryTrace( bcgroup, eLine, {n0, n2} );
            edgeID++;
          }
        }
      }

      // right boundary
      {
        bcgroup++;

        int i = ii-1;
        for (int j = 0; j < jj; j++)
        {
          int m0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
          int n2 = m0 + 2;
          int m2 = (j*order)*(ii*order + 1) + i*order + 2*(ii*order + 1);
          int n8  = m2 + 2;

          int edge = j;
          mapEdgeID[edgeID] = {bcgroup, edge};

          xfldin.addBoundaryTrace( bcgroup, eLine, {n2, n8} );
          edgeID++;
        }
      }

      // upper boundary
      {
        int j = jj - 1;

        for (int cellgroup = 1; cellgroup >= 0; cellgroup--)
        {
          bcgroup++;
          for (int i = ii/2*(cellgroup+1) - 1; i >= ii/2*cellgroup; i--)
          {
            int m2 = (j*order)*(ii*order + 1) + i*order + 2*(ii*order + 1);
            int n6  = m2 + 0;
            int n8  = m2 + 2;

            int edge = ii/2*(cellgroup+1)-1 - i;
            mapEdgeID[edgeID] = {bcgroup, edge};

            xfldin.addBoundaryTrace( bcgroup, eLine, {n8, n6} );
            edgeID++;
          }
        }
      }

      // left boundary
      {
        bcgroup++;

        int i = 0;
        for (int j = jj-1; j >= 0; j--)
        {
          int m0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
          int n0 = m0 + 0;
          int m2 = (j*order)*(ii*order + 1) + i*order + 2*(ii*order + 1);
          int n6  = m2 + 0;

          int edge = jj-1 - j;
          mapEdgeID[edgeID] = {bcgroup, edge};

          xfldin.addBoundaryTrace( bcgroup, eLine, {n6, n0} );
          edgeID++;
        }
      }
    } // rank

#ifdef SANS_MPI
    // send the maps to the other ranks, as it was only constructed on rank 0
    boost::mpi::broadcast(comm, mapCellID, 0);
    boost::mpi::broadcast(comm, mapEdgeID, 0);
#endif

    xfldin.balance();

    // make sure the node count works correctly
    BOOST_REQUIRE_EQUAL( nNode_global, xfldin.nNode_global() );

    const Real small_tol = 1e-11;
    const Real close_tol = 1e-11;

    std::set<int> elemOnRank;

    for (int rank = 0; rank < comm.size(); rank++)
    {
      // only let one processor print at a time in order
      comm.barrier();
      if (rank != comm.rank()) continue;

      for (int group = 0; group < 2; group++)
      {
        const XFieldCellGroupType& xfldCellGroup = xfld_global.getCellGroup<Triangle>(group);

        std::vector<int> globalElemMap(xfldCellGroup.basis()->nBasis());

        for (int elem = 0; elem < xfldin.cellGroup(group)->nElem(); elem++)
        {
          const int elemID = xfldin.cellGroup(group)->elem(elem).elemID;
          const std::vector<int>& elemMap = xfldin.cellGroup(group)->elem(elem).elemMap;

          // The element should only occur once on any given rank
          BOOST_CHECK( elemOnRank.find(elemID) == elemOnRank.end() );
          elemOnRank.insert(elemID);

          std::pair<int,int> cellgroup_cell = mapCellID.at(elemID);
          int cellgroup = cellgroup_cell.first;
          int cell = cellgroup_cell.second;

          BOOST_CHECK_EQUAL(group, cellgroup);

          // get the element from the global grid
          xfldCellGroup.getElement(xfldCellElem, cell);

          //std::cout << comm.rank() << " group " << group << " elemID " << elemID << std::endl;

          for (std::size_t n = 0; n < elemMap.size(); n++)
          {
            X = xfldin.DOF(xfldin.global2localDOFmap(elemMap[n])).X;

            xfldCellGroup.associativity(cell).getGlobalMapping(globalElemMap.data(), globalElemMap.size());

            // check that the parallel DOF map matches the global map
            BOOST_CHECK_EQUAL(globalElemMap[n], elemMap[n]);

            // check that the DOF coordinates match
            SANS_CHECK_CLOSE(xfldCellElem.DOF(n)[0], X[0], small_tol, close_tol);
            SANS_CHECK_CLOSE(xfldCellElem.DOF(n)[1], X[1], small_tol, close_tol);
          }
        } // elem
      } // group


      // loop over all boundary groups
      for (int group = 0; group < xfld_global.nBoundaryTraceGroups(); group++)
      {
        const XFieldTraceGroupType& xfldBtrace = xfld_global.getBoundaryTraceGroup<Line>(group);
        ElementXFieldTrace xfldTraceElem(xfldBtrace.basis());

        std::vector<int> globalElemMap(xfldBtrace.basis()->nBasis());
        elemOnRank.clear();

        for (int elem = 0; elem < xfldin.getBoundaryTraceGroup(group)->nElem(); elem++)
        {
          const int elemID = xfldin.getBoundaryTraceGroup(group)->elem(elem).elemID;
          const std::vector<int>& elemMap = xfldin.getBoundaryTraceGroup(group)->elem(elem).elemMap;

          // The boundary element should only occur once on any given rank
          BOOST_CHECK( elemOnRank.find(elemID) == elemOnRank.end() );
          elemOnRank.insert(elemID);

          std::pair<int,int> bcgroup_edge =  mapEdgeID.at(elemID);
          int bcgroup = bcgroup_edge.first;
          int edge = bcgroup_edge.second;

          BOOST_CHECK_EQUAL( bcgroup, group );

          // get the boundary element from the global grid
          xfldBtrace.getElement(xfldTraceElem, edge);

          for (std::size_t n = 0; n < elemMap.size(); n++)
          {
            X = xfldin.DOF(xfldin.global2localDOFmap(elemMap[n])).X;

            xfldBtrace.associativity(edge).getGlobalMapping(globalElemMap.data(), globalElemMap.size());

            // check that the parallel DOF map matches the global map
            BOOST_CHECK_EQUAL(globalElemMap[n], elemMap[n]);

            // check that the DOF coordinates match
            SANS_CHECK_CLOSE(xfldTraceElem.DOF(n)[0], X[0], small_tol, close_tol);
            SANS_CHECK_CLOSE(xfldTraceElem.DOF(n)[1], X[1], small_tol, close_tol);
          }
        } // elem
      } // group
    } // rank
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField_Q2_Serialize_test )
{
  typedef typename XField<PhysD2,TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef typename XField<PhysD2,TopoD2>::template FieldTraceGroupType<Line>    XFieldTraceGroupType;
  typedef typename XFieldCellGroupType::template ElementType<>  ElementXFieldCell;
  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTrace;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  XField_Lagrange<PhysD2>::VectorX X;

  int ii = 4;
  int jj = 5;
  int order = 2;

  // global communicator
  mpi::communicator world;

  // Generate a global grid for testing (identical on all processors)
  XField2D_Box_Triangle_Lagrange_2Group_Q2 xfld_global(world.split(world.rank()), ii, jj, 0, ii*order, 0, jj*order);

  // construct communicators for each available size
  for (int size = 1; size <= world.size(); size++)
  {
    int color = world.rank() < size ? 0 : 1;

    // split the world into two groups
    mpi::communicator comm = world.split(color);

    // only use group 0 for testing
    if (color == 1) continue;

    // Generate a partitioned grid
    XField2D_Box_Triangle_Lagrange_2Group_Q2 xfld_part(comm, ii, jj, 0, ii*order, 0, jj*order);

    // check that the native DOF count is the same
    BOOST_CHECK_EQUAL(xfld_global.nDOFnative(), xfld_part.nDOFnative());

    // serialize the grid
    XField<PhysD2,TopoD2> xfld_serial(xfld_part, XFieldBalance::Serial);

    if (comm.rank() == 0)
    {
      BOOST_REQUIRE_EQUAL(xfld_global.nDOF(), xfld_serial.nDOF());

      for (int n = 0; n < xfld_global.nDOF(); n++)
      {
        // check that the DOF coordinates match
        SANS_CHECK_CLOSE(xfld_global.DOF(n)[0], xfld_serial.DOF(n)[0], small_tol, close_tol);
        SANS_CHECK_CLOSE(xfld_global.DOF(n)[1], xfld_serial.DOF(n)[1], small_tol, close_tol);
      }

      BOOST_REQUIRE_EQUAL(xfld_global.nCellGroups(), xfld_serial.nCellGroups());

      for (int group = 0; group < 2; group++)
      {
        const XFieldCellGroupType& xfldCellGroup_global = xfld_global.getCellGroup<Triangle>(group);
        const XFieldCellGroupType& xfldCellGroup_serial = xfld_serial.getCellGroup<Triangle>(group);

        BOOST_REQUIRE_EQUAL(xfldCellGroup_global.nElem(), xfldCellGroup_serial.nElem());

        std::vector<int> globalElemMap_global(xfldCellGroup_global.nBasis());
        std::vector<int> globalElemMap_serial(xfldCellGroup_serial.nBasis());

        ElementXFieldCell xfldCellElem_global(xfldCellGroup_global.basis());
        ElementXFieldCell xfldCellElem_serial(xfldCellGroup_serial.basis());

        for (int elem = 0; elem < xfldCellGroup_global.nElem(); elem++)
        {
          // get the element from the global grid
          xfldCellGroup_global.getElement(xfldCellElem_global, elem);
          xfldCellGroup_serial.getElement(xfldCellElem_serial, elem);

          xfldCellGroup_global.associativity(elem).getGlobalMapping(globalElemMap_global.data(), globalElemMap_global.size());
          xfldCellGroup_serial.associativity(elem).getGlobalMapping(globalElemMap_serial.data(), globalElemMap_serial.size());

          for (int n = 0; n < xfldCellElem_global.nDOF(); n++)
          {
            // check that the parallel DOF map matches the global map
            BOOST_CHECK_EQUAL(globalElemMap_global[n], globalElemMap_serial[n]);

            // check that the DOF coordinates match
            SANS_CHECK_CLOSE(xfldCellElem_global.DOF(n)[0], xfldCellElem_serial.DOF(n)[0], small_tol, close_tol);
            SANS_CHECK_CLOSE(xfldCellElem_global.DOF(n)[1], xfldCellElem_serial.DOF(n)[1], small_tol, close_tol);
          }
        } // elem
      } // group

      // loop over all boundary groups
      for (int group = 0; group < xfld_global.nBoundaryTraceGroups(); group++)
      {
        const XFieldTraceGroupType& xfldBtrace_global = xfld_global.getBoundaryTraceGroup<Line>(group);
        const XFieldTraceGroupType& xfldBtrace_serial = xfld_serial.getBoundaryTraceGroup<Line>(group);

        ElementXFieldTrace xfldTraceElem_global(xfldBtrace_global.basis());
        ElementXFieldTrace xfldTraceElem_serial(xfldBtrace_serial.basis());

        std::vector<int> globalElemMap_global(xfldBtrace_global.basis()->nBasis());
        std::vector<int> globalElemMap_serial(xfldBtrace_serial.basis()->nBasis());

        for (int elem = 0; elem < xfldBtrace_global.nElem(); elem++)
        {
          // get the boundary element from the global grid
          xfldBtrace_global.getElement(xfldTraceElem_global, elem);
          xfldBtrace_serial.getElement(xfldTraceElem_serial, elem);

          xfldBtrace_global.associativity(elem).getGlobalMapping(globalElemMap_global.data(), globalElemMap_global.size());
          xfldBtrace_serial.associativity(elem).getGlobalMapping(globalElemMap_serial.data(), globalElemMap_serial.size());

          for (std::size_t n = 0; n < globalElemMap_global.size(); n++)
          {
            // check that the DOF map matches the global map
            BOOST_CHECK_EQUAL(globalElemMap_global[n], globalElemMap_serial[n]);

            // check that the DOF coordinates match
            SANS_CHECK_CLOSE(xfldTraceElem_global.DOF(n)[0], xfldTraceElem_serial.DOF(n)[0], small_tol, close_tol);
            SANS_CHECK_CLOSE(xfldTraceElem_global.DOF(n)[1], xfldTraceElem_serial.DOF(n)[1], small_tol, close_tol);
          }
        } // elem
      } // group
    }
    else
    {
      // all othe processors should have an empty xfield
      BOOST_CHECK_EQUAL(0, xfld_serial.nDOF());
      BOOST_CHECK_EQUAL(2, xfld_serial.nCellGroups());
      BOOST_CHECK_EQUAL(6, xfld_serial.nBoundaryTraceGroups());
      BOOST_CHECK_EQUAL(0, xfld_serial.nInteriorTraceGroups());

      for (int group = 0; group < xfld_serial.nCellGroups(); group++)
        BOOST_CHECK_EQUAL(0, xfld_serial.getCellGroupBase(group).nElem() );

      for (int group = 0; group < xfld_serial.nBoundaryTraceGroups(); group++)
        BOOST_CHECK_EQUAL(0, xfld_serial.getBoundaryTraceGroupBase(group).nElem() );

      for (int group = 0; group < xfld_serial.nInteriorTraceGroups(); group++)
        BOOST_CHECK_EQUAL(0, xfld_serial.getInteriorTraceGroupBase(group).nElem() );
    }

  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField_Q2_CopyPartition_test )
{
  typedef typename XField<PhysD2,TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef typename XField<PhysD2,TopoD2>::template FieldTraceGroupType<Line>    XFieldTraceGroupType;
  typedef typename XFieldCellGroupType::template ElementType<>  ElementXFieldCell;
  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTrace;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  int ii = 4;
  int jj = 5;
  int order = 1;

  // global communicator
  mpi::communicator world;

  // construct communicators for each available size
  for (int size = 1; size <= world.size(); size++)
  {
    int color = world.rank() < size ? 0 : 1;

    // split the world into two groups
    mpi::communicator comm = world.split(color);

    // only use group 0 for testing
    if (color == 1) continue;

    // Generate a partitioned grid
    XField2D_Box_Triangle_Lagrange_2Group_Q2 xfld_part(comm, ii, jj, 0, ii*order, 0, jj*order);

    // crete a grid with identical partition
    XField2D_Box_Triangle_Lagrange_2Group_Q2 xfld_copy(xfld_part, XFieldBalance::CellPartitionCopy, ii, jj, 0, ii*order, 0, jj*order);

    BOOST_REQUIRE_EQUAL(xfld_part.nDOF(), xfld_copy.nDOF());

    for (int n = 0; n < xfld_part.nDOF(); n++)
    {
      // check that the DOF coordinates match
      SANS_CHECK_CLOSE(xfld_part.DOF(n)[0], xfld_copy.DOF(n)[0], small_tol, close_tol);
      SANS_CHECK_CLOSE(xfld_part.DOF(n)[1], xfld_copy.DOF(n)[1], small_tol, close_tol);
    }

    BOOST_REQUIRE_EQUAL(xfld_part.nCellGroups(), xfld_copy.nCellGroups());

    for (int group = 0; group < 2; group++)
    {
      const XFieldCellGroupType& xfldCellGroup_part = xfld_part.getCellGroup<Triangle>(group);
      const XFieldCellGroupType& xfldCellGroup_copy = xfld_copy.getCellGroup<Triangle>(group);

      BOOST_REQUIRE_EQUAL(xfldCellGroup_part.nElem(), xfldCellGroup_copy.nElem());

      std::vector<int> globalElemMap_part(xfldCellGroup_part.nBasis());
      std::vector<int> globalElemMap_copy(xfldCellGroup_copy.nBasis());

      ElementXFieldCell xfldCellElem_part(xfldCellGroup_part.basis());
      ElementXFieldCell xfldCellElem_copy(xfldCellGroup_copy.basis());

      const std::vector<int>& cellIDs_part = xfld_part.cellIDs(group);
      const std::vector<int>& cellIDs_copy = xfld_copy.cellIDs(group);


      for (int elem = 0; elem < xfldCellGroup_part.nElem(); elem++)
      {
        // get the element from the global grid
        xfldCellGroup_part.getElement(xfldCellElem_part, elem);
        xfldCellGroup_copy.getElement(xfldCellElem_copy, elem);

        xfldCellGroup_part.associativity(elem).getGlobalMapping(globalElemMap_part.data(), globalElemMap_part.size());
        xfldCellGroup_copy.associativity(elem).getGlobalMapping(globalElemMap_copy.data(), globalElemMap_copy.size());

        // check the element ranks match
        BOOST_CHECK_EQUAL(xfldCellElem_part.rank(), xfldCellElem_copy.rank());

        // check that the global IDs match
        BOOST_CHECK_EQUAL(cellIDs_part[elem], cellIDs_copy[elem]);

        for (int n = 0; n < xfldCellElem_part.nDOF(); n++)
        {
          // check that the parallel DOF map matches the global map
          BOOST_CHECK_EQUAL(globalElemMap_part[n], globalElemMap_copy[n]);

          // check that the DOF coordinates match
          SANS_CHECK_CLOSE(xfldCellElem_part.DOF(n)[0], xfldCellElem_copy.DOF(n)[0], small_tol, close_tol);
          SANS_CHECK_CLOSE(xfldCellElem_part.DOF(n)[1], xfldCellElem_copy.DOF(n)[1], small_tol, close_tol);
        }
      } // elem
    } // group

    // loop over all boundary groups
    for (int group = 0; group < xfld_part.nBoundaryTraceGroups(); group++)
    {
      const XFieldTraceGroupType& xfldBtrace_part = xfld_part.getBoundaryTraceGroup<Line>(group);
      const XFieldTraceGroupType& xfldBtrace_copy = xfld_copy.getBoundaryTraceGroup<Line>(group);

      ElementXFieldTrace xfldTraceElem_part(xfldBtrace_part.basis());
      ElementXFieldTrace xfldTraceElem_copy(xfldBtrace_copy.basis());

      std::vector<int> globalElemMap_part(xfldBtrace_part.basis()->nBasis());
      std::vector<int> globalElemMap_copy(xfldBtrace_copy.basis()->nBasis());

      const std::vector<int>& boundaryTraceIDs_part = xfld_part.boundaryTraceIDs(group);
      const std::vector<int>& boundaryTraceIDs_copy = xfld_copy.boundaryTraceIDs(group);

      for (int elem = 0; elem < xfldBtrace_part.nElem(); elem++)
      {
        // get the boundary element from the global grid
        xfldBtrace_part.getElement(xfldTraceElem_part, elem);
        xfldBtrace_copy.getElement(xfldTraceElem_copy, elem);

        xfldBtrace_part.associativity(elem).getGlobalMapping(globalElemMap_part.data(), globalElemMap_part.size());
        xfldBtrace_copy.associativity(elem).getGlobalMapping(globalElemMap_copy.data(), globalElemMap_copy.size());

        // check the element ranks match
        BOOST_CHECK_EQUAL(xfldTraceElem_part.rank(), xfldTraceElem_copy.rank());

        // check that the global IDs match
        BOOST_CHECK_EQUAL(boundaryTraceIDs_part[elem], boundaryTraceIDs_copy[elem]);

        for (std::size_t n = 0; n < globalElemMap_part.size(); n++)
        {
          // check that the DOF map matches the global map
          BOOST_CHECK_EQUAL(globalElemMap_part[n], globalElemMap_copy[n]);

          // check that the DOF coordinates match
          SANS_CHECK_CLOSE(xfldTraceElem_part.DOF(n)[0], xfldTraceElem_copy.DOF(n)[0], small_tol, close_tol);
          SANS_CHECK_CLOSE(xfldTraceElem_part.DOF(n)[1], xfldTraceElem_copy.DOF(n)[1], small_tol, close_tol);
        }
      } // elem
    } // group
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField_Q1_Q2_CopyPartition_test )
{
  typedef typename XField<PhysD2,TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef typename XField<PhysD2,TopoD2>::template FieldTraceGroupType<Line>    XFieldTraceGroupType;
  typedef typename XFieldCellGroupType::template ElementType<>  ElementXFieldCell;
  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTrace;

  int ii = 4;
  int jj = 5;
  int order = 1;

  // global communicator
  mpi::communicator world;

  // construct communicators for each available size
  for (int size = 1; size <= world.size(); size++)
  {
    int color = world.rank() < size ? 0 : 1;

    // split the world into two groups
    mpi::communicator comm = world.split(color);

    // only use group 0 for testing
    if (color == 1) continue;

    // Generate a partitioned grid
    XField2D_Box_Triangle_Lagrange_2Group_Q1 xfld_part(comm, ii, jj, 0, ii*order, 0, jj*order);

    // crete a grid with identical partition (but different order)
    XField2D_Box_Triangle_Lagrange_2Group_Q2 xfld_copy(xfld_part, XFieldBalance::CellPartitionCopy, ii, jj, 0, ii*order, 0, jj*order);

    // can't compare DOFs, but elemnt numbering and IDs should be identical

    BOOST_REQUIRE_EQUAL(xfld_part.nCellGroups(), xfld_copy.nCellGroups());

    for (int group = 0; group < 2; group++)
    {
      const XFieldCellGroupType& xfldCellGroup_part = xfld_part.getCellGroup<Triangle>(group);
      const XFieldCellGroupType& xfldCellGroup_copy = xfld_copy.getCellGroup<Triangle>(group);

      BOOST_REQUIRE_EQUAL(xfldCellGroup_part.nElem(), xfldCellGroup_copy.nElem());

      ElementXFieldCell xfldCellElem_part(xfldCellGroup_part.basis());
      ElementXFieldCell xfldCellElem_copy(xfldCellGroup_copy.basis());

      const std::vector<int>& cellIDs_part = xfld_part.cellIDs(group);
      const std::vector<int>& cellIDs_copy = xfld_copy.cellIDs(group);

      for (int elem = 0; elem < xfldCellGroup_part.nElem(); elem++)
      {
        // get the element from the global grid
        xfldCellGroup_part.getElement(xfldCellElem_part, elem);
        xfldCellGroup_copy.getElement(xfldCellElem_copy, elem);

        // check the element ranks match
        BOOST_CHECK_EQUAL(xfldCellElem_part.rank(), xfldCellElem_copy.rank());

        // check that the global IDs match
        BOOST_CHECK_EQUAL(cellIDs_part[elem], cellIDs_copy[elem]);
      } // elem
    } // group

    // loop over all boundary groups
    for (int group = 0; group < xfld_part.nBoundaryTraceGroups(); group++)
    {
      const XFieldTraceGroupType& xfldBtrace_part = xfld_part.getBoundaryTraceGroup<Line>(group);
      const XFieldTraceGroupType& xfldBtrace_copy = xfld_copy.getBoundaryTraceGroup<Line>(group);

      ElementXFieldTrace xfldTraceElem_part(xfldBtrace_part.basis());
      ElementXFieldTrace xfldTraceElem_copy(xfldBtrace_copy.basis());

      const std::vector<int>& boundaryTraceIDs_part = xfld_part.boundaryTraceIDs(group);
      const std::vector<int>& boundaryTraceIDs_copy = xfld_copy.boundaryTraceIDs(group);

      for (int elem = 0; elem < xfldBtrace_part.nElem(); elem++)
      {
        // get the boundary element from the global grid
        xfldBtrace_part.getElement(xfldTraceElem_part, elem);
        xfldBtrace_copy.getElement(xfldTraceElem_copy, elem);

        // check the element ranks match
        BOOST_CHECK_EQUAL(xfldTraceElem_part.rank(), xfldTraceElem_copy.rank());

        // check that the global IDs match
        BOOST_CHECK_EQUAL(boundaryTraceIDs_part[elem], boundaryTraceIDs_copy[elem]);
      } // elem
    } // group
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField_Q1_PartitionRead_test )
{
  typedef typename XField<PhysD3,TopoD3>::template FieldCellGroupType<Hex> XFieldCellGroupType;
  typedef typename XField<PhysD3,TopoD3>::template FieldTraceGroupType<Quad> XFieldTraceGroupType;
  typedef typename XFieldCellGroupType::template ElementType<>  ElementXFieldCell;
  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTrace;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  int ii = 3;
  int jj = 3;
  int kk = 3;


  // global communicator
  mpi::communicator world;

  // construct communicators for each available size
  for (int size = 1; size <= world.size(); size++)
  {
    int color = world.rank() < size ? 0 : 1;

    // split the world into two groups
    mpi::communicator comm = world.split(color);

    // only use group 0 for testing
    if (color == 1) continue;

    std::string partfile = "tmp/Test_Part" + to_string( comm.rank() ) + ".prt";

    // Generate a partitioned grid
    XField3D_Box_Hex_X1 xfld_part(comm, ii, jj, kk);
    XField_Lagrange<PhysD3>::outputPartition(xfld_part, partfile );

    // crete a grid with identical partition
    XField3D_Box_Hex_X1 xfld_copy(comm, partfile, ii, jj, kk);

    std::remove(partfile.c_str()); // cleanup

    BOOST_REQUIRE_EQUAL(xfld_part.nDOF(), xfld_copy.nDOF());

    for (int n = 0; n < xfld_part.nDOF(); n++)
    {
      // check that the DOF coordinates match
      SANS_CHECK_CLOSE(xfld_part.DOF(n)[0], xfld_copy.DOF(n)[0], small_tol, close_tol);
      SANS_CHECK_CLOSE(xfld_part.DOF(n)[1], xfld_copy.DOF(n)[1], small_tol, close_tol);
    }

    BOOST_REQUIRE_EQUAL(xfld_part.nCellGroups(), xfld_copy.nCellGroups());

    for (int group = 0; group < 1; group++)
    {
      const XFieldCellGroupType& xfldCellGroup_part = xfld_part.getCellGroup<Hex>(group);
      const XFieldCellGroupType& xfldCellGroup_copy = xfld_copy.getCellGroup<Hex>(group);

      BOOST_REQUIRE_EQUAL(xfldCellGroup_part.nElem(), xfldCellGroup_copy.nElem());

      std::vector<int> globalElemMap_part(xfldCellGroup_part.nBasis());
      std::vector<int> globalElemMap_copy(xfldCellGroup_copy.nBasis());

      ElementXFieldCell xfldCellElem_part(xfldCellGroup_part.basis());
      ElementXFieldCell xfldCellElem_copy(xfldCellGroup_copy.basis());

      const std::vector<int>& cellIDs_part = xfld_part.cellIDs(group);
      const std::vector<int>& cellIDs_copy = xfld_copy.cellIDs(group);


      for (int elem = 0; elem < xfldCellGroup_part.nElem(); elem++)
      {
        // get the element from the global grid
        xfldCellGroup_part.getElement(xfldCellElem_part, elem);
        xfldCellGroup_copy.getElement(xfldCellElem_copy, elem);

        xfldCellGroup_part.associativity(elem).getGlobalMapping(globalElemMap_part.data(), globalElemMap_part.size());
        xfldCellGroup_copy.associativity(elem).getGlobalMapping(globalElemMap_copy.data(), globalElemMap_copy.size());

        // check the element ranks match
        BOOST_CHECK_EQUAL(xfldCellElem_part.rank(), xfldCellElem_copy.rank());

        // check that the global IDs match
        BOOST_CHECK_EQUAL(cellIDs_part[elem], cellIDs_copy[elem]);

        for (int n = 0; n < xfldCellElem_part.nDOF(); n++)
        {
          // check that the parallel DOF map matches the global map
          BOOST_CHECK_EQUAL(globalElemMap_part[n], globalElemMap_copy[n]);

          // check that the DOF coordinates match
          SANS_CHECK_CLOSE(xfldCellElem_part.DOF(n)[0], xfldCellElem_copy.DOF(n)[0], small_tol, close_tol);
          SANS_CHECK_CLOSE(xfldCellElem_part.DOF(n)[1], xfldCellElem_copy.DOF(n)[1], small_tol, close_tol);
        }
      } // elem
    } // group

    // loop over all boundary groups
    for (int group = 0; group < xfld_part.nBoundaryTraceGroups(); group++)
    {
      const XFieldTraceGroupType& xfldBtrace_part = xfld_part.getBoundaryTraceGroup<Quad>(group);
      const XFieldTraceGroupType& xfldBtrace_copy = xfld_copy.getBoundaryTraceGroup<Quad>(group);
      BOOST_REQUIRE_EQUAL(xfld_part.nDOF(), xfld_copy.nDOF());
      ElementXFieldTrace xfldTraceElem_part(xfldBtrace_part.basis());
      ElementXFieldTrace xfldTraceElem_copy(xfldBtrace_copy.basis());

      std::vector<int> globalElemMap_part(xfldBtrace_part.basis()->nBasis());
      std::vector<int> globalElemMap_copy(xfldBtrace_copy.basis()->nBasis());

      const std::vector<int>& boundaryTraceIDs_part = xfld_part.boundaryTraceIDs(group);
      const std::vector<int>& boundaryTraceIDs_copy = xfld_copy.boundaryTraceIDs(group);

      for (int elem = 0; elem < xfldBtrace_part.nElem(); elem++)
      {
        // get the boundary element from the global grid
        xfldBtrace_part.getElement(xfldTraceElem_part, elem);
        xfldBtrace_copy.getElement(xfldTraceElem_copy, elem);

        xfldBtrace_part.associativity(elem).getGlobalMapping(globalElemMap_part.data(), globalElemMap_part.size());
        xfldBtrace_copy.associativity(elem).getGlobalMapping(globalElemMap_copy.data(), globalElemMap_copy.size());

        // check the element ranks match
        BOOST_CHECK_EQUAL(xfldTraceElem_part.rank(), xfldTraceElem_copy.rank());

        // check that the global IDs match
        BOOST_CHECK_EQUAL(boundaryTraceIDs_part[elem], boundaryTraceIDs_copy[elem]);

        for (std::size_t n = 0; n < globalElemMap_part.size(); n++)
        {
          // check that the DOF map matches the global map
          BOOST_CHECK_EQUAL(globalElemMap_part[n], globalElemMap_copy[n]);

          // check that the DOF coordinates match
          SANS_CHECK_CLOSE(xfldTraceElem_part.DOF(n)[0], xfldTraceElem_copy.DOF(n)[0], small_tol, close_tol);
          SANS_CHECK_CLOSE(xfldTraceElem_part.DOF(n)[1], xfldTraceElem_copy.DOF(n)[1], small_tol, close_tol);
        }
      } // elem
    } // group
  }
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
