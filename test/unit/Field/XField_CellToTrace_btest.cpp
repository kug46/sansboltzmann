// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField2D_btest
// testing of XField2D
//
// Note: unit grids tested in "unit/UnitGrids/UnitGrids_btest.cpp"

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Field/XField_CellToTrace.h"
#include "Field/XFieldLine.h"
#include "Field/FieldArea_CG_Cell.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_BoxPeriodic_Triangle_X1.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( XField_CellToTrace_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField_CellToTrace_1D_1Group_test )
{
  XField1D xfld(4);
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  BOOST_CHECK_EQUAL( &xfld, &connectivity.getXField() );

  TraceInfo traceinfo;

  //Cellgroup 0, Cell 0
  traceinfo = connectivity.getTrace(0,0,0);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 0 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Interior );

  traceinfo = connectivity.getTrace(0,0,1);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 0 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Boundary );

  //Cellgroup 0, Cell 1
  traceinfo = connectivity.getTrace(0,1,0);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 1 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Interior );

  traceinfo = connectivity.getTrace(0,1,1);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 0 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Interior );

  //Cellgroup 0, Cell 2
  traceinfo = connectivity.getTrace(0,2,0);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 2 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Interior );

  traceinfo = connectivity.getTrace(0,2,1);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 1 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Interior );

  //Cellgroup 0, Cell 3
  traceinfo = connectivity.getTrace(0,3,0);
  BOOST_CHECK_EQUAL( traceinfo.group, 1 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 0 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Boundary );

  traceinfo = connectivity.getTrace(0,3,1);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 2 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Interior );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField_CellToTrace_1D_1Line_X1_1Group_test )
{
  XField1D_1Line_X1_1Group xfld;
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  BOOST_CHECK_EQUAL( &xfld, &connectivity.getXField() );

  TraceInfo traceinfo;

  //Cellgroup 0, Cell 0
  traceinfo = connectivity.getTrace(0,0,0);
  BOOST_CHECK_EQUAL( traceinfo.group, 1 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 0 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Boundary );

  traceinfo = connectivity.getTrace(0,0,1);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 0 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Boundary );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField_CellToTrace_1D_2Line_X1_1Group_test )
{
  XField1D_2Line_X1_1Group xfld;
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  BOOST_CHECK_EQUAL( &xfld, &connectivity.getXField() );

  TraceInfo traceinfo;

  //Cellgroup 0, Cell 0
  traceinfo = connectivity.getTrace(0,0,0);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 0 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Interior );

  traceinfo = connectivity.getTrace(0,0,1);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 0 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Boundary );

  //Cellgroup 0, Cell 1
  traceinfo = connectivity.getTrace(0,1,0);
  BOOST_CHECK_EQUAL( traceinfo.group, 1 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 0 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Boundary );

  traceinfo = connectivity.getTrace(0,1,1);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 0 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Interior );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField_CellToTrace_2D_1Triangle_X1_1Group_test )
{
  XField2D_1Triangle_X1_1Group xfld;
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  BOOST_CHECK_EQUAL( &xfld, &connectivity.getXField() );

  TraceInfo traceinfo;

  //Cellgroup 0, Cell 0
  traceinfo = connectivity.getTrace(0,0,0);
  BOOST_CHECK_EQUAL( traceinfo.group, 1 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 0 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Boundary );

  traceinfo = connectivity.getTrace(0,0,1);
  BOOST_CHECK_EQUAL( traceinfo.group, 2 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 0 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Boundary );

  traceinfo = connectivity.getTrace(0,0,2);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 0 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Boundary );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField_CellToTrace_2D_2Triangle_X1_1Group_test )
{
  XField2D_2Triangle_X1_1Group xfld;
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  BOOST_CHECK_EQUAL( &xfld, &connectivity.getXField() );

  TraceInfo traceinfo;

  //Cellgroup 0, Cell 0
  traceinfo = connectivity.getTrace(0,0,0);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 0 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Interior );

  traceinfo = connectivity.getTrace(0,0,1);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 3 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Boundary );

  traceinfo = connectivity.getTrace(0,0,2);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 0 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Boundary );

  //Cellgroup 0, Cell 1
  traceinfo = connectivity.getTrace(0,1,0);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 0 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Interior );

  traceinfo = connectivity.getTrace(0,1,1);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 1 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Boundary );

  traceinfo = connectivity.getTrace(0,1,2);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 2 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Boundary );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField_CellToTrace_2D_4Triangle_X1_1Group_test )
{
  XField2D_4Triangle_X1_1Group xfld;
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  BOOST_CHECK_EQUAL( &xfld, &connectivity.getXField() );

  TraceInfo traceinfo;

  //Cellgroup 0, Cell 0
  traceinfo = connectivity.getTrace(0,0,0);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 0 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Interior );

  traceinfo = connectivity.getTrace(0,0,1);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 1 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Interior );

  traceinfo = connectivity.getTrace(0,0,2);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 2 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Interior );

  //Cellgroup 0, Cell 1
  traceinfo = connectivity.getTrace(0,1,0);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 0 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Interior );

  traceinfo = connectivity.getTrace(0,1,1);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 2 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Boundary );

  traceinfo = connectivity.getTrace(0,1,2);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 3 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Boundary );

  //Cellgroup 0, Cell 2
  traceinfo = connectivity.getTrace(0,2,0);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 5 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Boundary );

  traceinfo = connectivity.getTrace(0,2,1);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 1 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Interior );

  traceinfo = connectivity.getTrace(0,2,2);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 4 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Boundary );

  //Cellgroup 0, Cell 3
  traceinfo = connectivity.getTrace(0,3,0);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 0 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Boundary );

  traceinfo = connectivity.getTrace(0,3,1);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 1 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Boundary );

  traceinfo = connectivity.getTrace(0,3,2);
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 2 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Interior );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField_CellToTrace_2D_BoxPeriodic_Triangle_X1_test )
{
  XField2D_BoxPeriodic_Triangle_X1 xfld( 1, 1 );
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  BOOST_CHECK_EQUAL( &xfld, &connectivity.getXField() );

  TraceInfo traceinfo;

  //Cellgroup 0, Cell 0
  traceinfo = connectivity.getTrace(0,0,0); //interior trace group - diagonal
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 2 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Interior );

  traceinfo = connectivity.getTrace(0,0,1); //interior trace group - left
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 1 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Interior );

  traceinfo = connectivity.getTrace(0,0,2); //interior trace group - bottom
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 0 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Interior );

  //Cellgroup 0, Cell 1
  traceinfo = connectivity.getTrace(0,1,0); //interior trace group - diagonal
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 2 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Interior );

  traceinfo = connectivity.getTrace(0,1,1); //interior trace group - right
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 1 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Interior );

  traceinfo = connectivity.getTrace(0,1,2); //interior trace group - top
  BOOST_CHECK_EQUAL( traceinfo.group, 0 );
  BOOST_CHECK_EQUAL( traceinfo.elem , 0 );
  BOOST_CHECK_EQUAL( traceinfo.type , TraceInfo::Interior );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
