// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Field/XFieldLine_BoundaryTrace.h"   //Needed because we are using line XField
#include "Field/XFieldArea_BoundaryTrace.h"   //Needed because we are using area XField
#include "Field/XFieldVolume_BoundaryTrace.h" //Needed because we are using volume XField

#include "Field/output_grm.h"
#include "Field/output_Tecplot.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( XField_BoundaryTrace_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XFieldArea_BoundaryTrace_test )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef Line TopologyTrace;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  mpi::communicator world;
  int ii = 3, jj = 4;

  XField2D_Box_Triangle_Lagrange_X1 xfld_global(world.split(world.rank()), ii, jj);
  XField2D_Box_Triangle_Lagrange_X1 xfld_local(world, ii, jj);

  std::vector<int> bonudaryGroups = {0,1};

  XField_BoundaryTrace<PhysDim, TopoDim> xfld_bnd(xfld_local, bonudaryGroups);

  for (std::size_t i = 0; i < bonudaryGroups.size(); i++)
  {
    typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XField_BoundaryTrace<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldBndGroupType;

    int boundaryTraceGroup = bonudaryGroups[i];

    const XFieldTraceGroupType& xfldTraceGroup_global = xfld_global.template getBoundaryTraceGroupGlobal<TopologyTrace>(boundaryTraceGroup);

    const XFieldBndGroupType& xfldTraceGroup_bnd = xfld_bnd.template getBoundaryTraceGroupGlobal<TopologyTrace>(i);

    BOOST_REQUIRE_EQUAL(xfldTraceGroup_global.nElem(), xfldTraceGroup_bnd.nElem());

    XFieldTraceGroupType::ElementType<> xfldElem_global(xfldTraceGroup_global.basis());
    XFieldBndGroupType::ElementType<> xfldElem_bnd(xfldTraceGroup_bnd.basis());

    const int nElem = xfldTraceGroup_global.nElem();
    for (int elem = 0; elem < nElem; elem++)
    {
      xfldTraceGroup_global.getElement(xfldElem_global, elem);
      xfldTraceGroup_bnd.getElement(xfldElem_bnd, elem);

      for (int j = 0; j < xfldElem_global.nDOF(); j++)
        for (int d = 0; d < PhysDim::D; d++)
          SANS_CHECK_CLOSE(xfldElem_global.DOF(j)[d], xfldElem_bnd.DOF(j)[d], small_tol, close_tol);
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XFieldVolume_BoundaryTrace_test )
{
  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;
  typedef Triangle TopologyTrace;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  mpi::communicator world;
  int ii = 3, jj = 4, kk = 5;

  XField3D_Box_Tet_X1 xfld_global(world.split(world.rank()), ii, jj, kk);
  XField3D_Box_Tet_X1 xfld_local(world, ii, jj, kk);

  std::vector<int> bonudaryGroups = {0,1,3};

  XField_BoundaryTrace<PhysDim, TopoDim> xfld_bnd(xfld_local, bonudaryGroups);

  for (std::size_t i = 0; i < bonudaryGroups.size(); i++)
  {
    typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XField_BoundaryTrace<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldBndGroupType;

    int boundaryTraceGroup = bonudaryGroups[i];

    const XFieldTraceGroupType& xfldTraceGroup_global = xfld_global.template getBoundaryTraceGroupGlobal<TopologyTrace>(boundaryTraceGroup);

    const XFieldBndGroupType& xfldTraceGroup_bnd = xfld_bnd.template getBoundaryTraceGroupGlobal<TopologyTrace>(i);

    BOOST_REQUIRE_EQUAL(xfldTraceGroup_global.nElem(), xfldTraceGroup_bnd.nElem());

    XFieldTraceGroupType::ElementType<> xfldElem_global(xfldTraceGroup_global.basis());
    XFieldBndGroupType::ElementType<> xfldElem_bnd(xfldTraceGroup_bnd.basis());

    const int nElem = xfldTraceGroup_global.nElem();
    for (int elem = 0; elem < nElem; elem++)
    {
      xfldTraceGroup_global.getElement(xfldElem_global, elem);
      xfldTraceGroup_bnd.getElement(xfldElem_bnd, elem);

      for (int j = 0; j < xfldElem_global.nDOF(); j++)
        for (int d = 0; d < PhysDim::D; d++)
          SANS_CHECK_CLOSE(xfldElem_global.DOF(j)[d], xfldElem_bnd.DOF(j)[d], small_tol, close_tol);
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
