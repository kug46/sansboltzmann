// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementalMassMatrix_Cell_btest
// testing of FieldDataMassMatrix_Cell
//

#include <iomanip>
#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Quad_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Quad_X1_1Group.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/Element/ElementalMassMatrix.h"

#include "Quadrature/Quadrature.h"

#include "LinearAlgebra/DenseLinAlg/InverseLU.h"

using namespace std;
using namespace SANS;


//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementalMassMatrix_Cell_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_1D_Legendre_test )
{
  typedef PhysD1 PhysDim;
  typedef TopoD1 TopoDim;
  typedef Real ArrayQ;
  const int M = 2;
  typedef DLA::MatrixS<M,M,Real> MatrixQ;
  typedef typename XField<PhysDim, TopoDim>::FieldCellGroupType<Line> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::ElementType<> XElementType;

  typedef Field_DG_Cell<PhysDim, TopoDim, ArrayQ> QField_DG_1D;
  typedef QField_DG_1D::FieldCellGroupType<Line> FieldCellGroupType;
  typedef FieldCellGroupType::ElementType<> ElementType;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  for (int order = 0; order <= 2; order++) // Legendre line basis supported for order 0 to 7
  {
    XField1D xfld(1, 0, 1.6);
    QField_DG_1D qfld(xfld, order, BasisFunctionCategory_Legendre);

    XFieldCellGroupType& xfldCell = xfld.getCellGroup<Line>(0);
    FieldCellGroupType& qfldCell = qfld.getCellGroupGlobal<Line>(0);

    XElementType xfldElem( xfldCell.basis() );
    ElementType qfldElem( qfldCell.basis() );

    const int nDOF = qfldCell.basis()->nBasis();

    ElementalMassMatrix<TopoDim,Line> massMtx(xfldElem, qfldElem);

    DLA::MatrixD<Real> mtx( nDOF, nDOF );
    DLA::MatrixD<DLA::MatrixS<PhysD1::D,PhysD1::D,Real>> mtxD( nDOF, nDOF );
    DLA::MatrixD<DLA::MatrixS<PhysD1::D,PhysD1::D,MatrixQ>> mtxDM( nDOF, nDOF );

    for (int elem = 0; elem < qfldCell.nElem(); elem++)
    {
      // copy global grid DOFs to element
      xfldCell.getElement( xfldElem, elem );

      // Compute the elemental mass matrix
      massMtx(xfldElem, mtx);
      massMtx(xfldElem, mtxD);
      massMtx(xfldElem, mtxDM);

      // Compute the elemental mass matrix analytically
      DLA::MatrixD<Real> mtxTrue( nDOF, nDOF );
      DLA::MatrixD<DLA::MatrixS<PhysD1::D,PhysD1::D,Real>> mtxTrueD( nDOF, nDOF );
      DLA::MatrixD<DLA::MatrixS<PhysD1::D,PhysD1::D,MatrixQ>> mtxTrueDM( nDOF, nDOF );
      mtxTrue = 0;
      mtxTrueD = 0;
      mtxTrueDM = 0;

      for (int i = 0; i < nDOF; i++)
        for (int j = 0; j < nDOF; j++)
        {
          mtxTrue(i,j) = (i == j) * xfldElem.length();
          for (int d = 0; d < PhysD1::D; d++)
          {
            mtxTrueD(i,j)(d,d) = mtxTrue(i,j);
            for (int m = 0; m < M; m++)
              mtxTrueDM(i,j)(d,d)(m,m) = mtxTrue(i,j);
          }
        }

      for (int i = 0; i < nDOF; i++)
        for (int j = 0; j < nDOF; j++)
        {
          SANS_CHECK_CLOSE(mtxTrue(i,j), mtx(i,j), small_tol, close_tol );
          for (int d0 = 0; d0 < PhysD1::D; d0++)
            for (int d1 = 0; d1 < PhysD1::D; d1++)
            {
              SANS_CHECK_CLOSE(mtxTrueD(i,j)(d0,d1), mtxD(i,j)(d0,d1), small_tol, close_tol );
              for (int m0 = 0; m0 < M; m0++)
                for (int m1 = 0; m1 < M; m1++)
                  SANS_CHECK_CLOSE(mtxTrueDM(i,j)(d0,d1)(m0,m1), mtxDM(i,j)(d0,d1)(m0,m1), small_tol, close_tol );
            }
        }

#if 0
      cout << "// ---------------------------------------- //" << endl;
      cout << "// order = " << order << ", elem = " << elem << endl;
      cout << "// ---------------------------------------- //" << endl;

      cout << "mtx    |    mtxTrue" << endl;
      for (int i = 0; i < nDOF; i++)
      {
        for (int j = 0; j < mtx.n(); j++)
        {
          cout << setprecision(3) << mtx(i,j) << ",    ";
        }
        cout << "|";
        for (int j = 0; j < mtxTrue.n(); j++)
        {
          cout << "    " << setprecision(3) << mtxTrue(i,j) << ",";
        }
        cout << endl;
      }
#endif
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Area_Triangle_2D_Legendre )
{
  typedef Real ArrayQ;
  const int M = 2;
  typedef DLA::MatrixS<M,M,Real> MatrixQ;

  typedef XField< PhysD2, TopoD2> XField_2D;
  typedef XField_2D::FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef XFieldCellGroupType::ElementType<> XElementType;

  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayQ > QField_DG_2D;
  typedef QField_DG_2D::FieldCellGroupType<Triangle> FieldCellGroupType;
  typedef FieldCellGroupType::ElementType<> ElementType;

  const Real small_tol = 1e-12;
  const Real close_tol = 6e-11;

  for (int order = 0; order <= 7; order++) // Legendre area basis supported for order 0 to 7
  {
    XField2D_4Triangle_X1_1Group xfld;
    QField_DG_2D qfld(xfld, order, BasisFunctionCategory_Legendre);

    XFieldCellGroupType& xfldCell = xfld.getCellGroup<Triangle>(0);
    FieldCellGroupType& qfldCell = qfld.getCellGroupGlobal<Triangle>(0);

    XElementType xfldElem( xfldCell.basis() );
    ElementType qfldElem( qfldCell.basis() );

    const int nDOF = qfldCell.basis()->nBasis();

    ElementalMassMatrix<TopoD2,Triangle> massMtx(xfldElem, qfldElem);
    DLA::MatrixD<Real> mtx( nDOF, nDOF );
    DLA::MatrixD<DLA::MatrixS<PhysD2::D,PhysD2::D,Real>> mtxD( nDOF, nDOF );
    DLA::MatrixD<DLA::MatrixS<PhysD2::D,PhysD2::D,MatrixQ>> mtxDM( nDOF, nDOF );

    for (int elem = 0; elem < qfldCell.nElem(); elem++)
    {
      // copy global grid DOFs to element
      xfldCell.getElement( xfldElem, elem );

      // Compute the elemental mass matrix
      massMtx(xfldElem, mtx);
      massMtx(xfldElem, mtxD);
      massMtx(xfldElem, mtxDM);

      // Compute the true mass matrix (which should be exact if quadrature order is sufficiently large)
      DLA::MatrixD<Real> mtxTrue( nDOF, nDOF );
      DLA::MatrixD<DLA::MatrixS<PhysD2::D,PhysD2::D,Real>> mtxTrueD( nDOF, nDOF );
      DLA::MatrixD<DLA::MatrixS<PhysD2::D,PhysD2::D,MatrixQ>> mtxTrueDM( nDOF, nDOF );
      mtxTrue = 0;
      mtxTrueD = 0;
      mtxTrueDM = 0;

      for (int i = 0; i < nDOF; i++)
        for (int j = 0; j < nDOF; j++)
        {
          mtxTrue(i,j) = (i == j) * xfldElem.area();
          for (int d = 0; d < PhysD2::D; d++)
          {
            mtxTrueD(i,j)(d,d) = mtxTrue(i,j);
            for (int m = 0; m < M; m++)
              mtxTrueDM(i,j)(d,d)(m,m) = mtxTrue(i,j);
          }
        }

      // check the elemental mass matrix
      for (int i = 0; i < nDOF; i++)
        for (int j = 0; j < nDOF; j++)
        {
          SANS_CHECK_CLOSE(mtxTrue(i,j), mtx(i,j), small_tol, close_tol );
          for (int d0 = 0; d0 < PhysD2::D; d0++)
            for (int d1 = 0; d1 < PhysD2::D; d1++)
            {
              SANS_CHECK_CLOSE(mtxTrueD(i,j)(d0,d1), mtxD(i,j)(d0,d1), small_tol, close_tol );
              for (int m0 = 0; m0 < M; m0++)
                for (int m1 = 0; m1 < M; m1++)
                  SANS_CHECK_CLOSE(mtxTrueDM(i,j)(d0,d1)(m0,m1), mtxDM(i,j)(d0,d1)(m0,m1), small_tol, close_tol );
            }
        }
    }
  }
}

#if 0 // Disabled until we get quad legendre polynomial orders increased
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Area_Quad_2D_Legendre )
{
  typedef Real ArrayQ;
  typedef XField< PhysD2, TopoD2> XField_2D;
  typedef XField_2D::FieldCellGroupType<Quad> XFieldCellGroupType;
  typedef XFieldCellGroupType::ElementType<> XElementType;

  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayQ > QField_DG_2D;
  typedef QField_DG_2D::FieldCellGroupType<Quad> FieldCellGroupType;
  typedef FieldCellGroupType::ElementType<> ElementType;
  typedef ElementType::RefCoordType RefCoordType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real weight;
  RefCoordType sRef;

  for (int order=0; order<=3; order++)
  {
    Quadrature<TopoD2, Triangle> quadrature( 2*order + 1 );

    XField2D_4Quad_X1_1Group xfld;

    XFieldCellGroupType& xfldCell = xfld.getCellGroup<Quad>(0);


    QField_DG_2D qfld(xfld, order, BasisFunctionCategory_Legendre);

    FieldCellGroupType& qfldCell = qfld.getCellGroupGlobal<Quad>(0);


    // Compute the field of inverse mass matrices
    FieldDataInvMassMatrix_Cell mmfld(qfld);

    DLA::MatrixDView_Array<Real>& mmfldCell = mmfld.getCellGroupGlobal(0);


    XElementType xfldElem( xfldCell.basis() );
    ElementType qfldElem( qfldCell.basis() );
    const int nDOF = qfldCell.basis()->nBasis();
    std::vector<Real> phi(nDOF);

    for (int elem = 0; elem < qfldCell.nElem(); elem++)
    {
      // copy global grid DOFs to element
      xfldCell.getElement( xfldElem, elem );

      DLA::MatrixD<Real> mtx( nDOF, nDOF );
      mtx = 0;

      const int nquad = quadrature.nQuadrature();
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        quadrature.weight( iquad, weight );
        quadrature.coordinates( iquad, sRef );

        Real dJ = weight * xfldElem.jacobianDeterminant( sRef );
        qfldElem.evalBasis( sRef, phi.data(), phi.size() );

        for (int i = 0; i < nDOF; i++)
          for (int j = 0; j < nDOF; j++)
            mtx(i,j) += dJ*phi[i]*phi[j];
      }

      mtx = DLA::InverseLU::Inverse(mtx);


      DLA::MatrixD<Real> Minv = mmfldCell[elem];
      for (int i = 0; i < Minv.m(); i++)
        for (int j = 0; j < Minv.n(); j++)
          SANS_CHECK_CLOSE(mtx(i,j), Minv(i,j), small_tol, close_tol );
    }
  }
}
#endif
#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_DG_Area;
  typedef Field_DG_InteriorTrace< PhysD2, TopoD2, ArrayQ > QField2D_DG_InteriorEdge;
  typedef Field_DG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > QField2D_DG_BoundaryEdge;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/Field2D_DG_pattern.txt", true );

  XField2D_2Triangle_X1_1Group xfld1;

  QField2D_DG_Area qfld1(xfld1, 0, BasisFunctionCategory_Legendre);
  qfld1.dump( 2, output );

  QField2D_DG_InteriorEdge qfld2(xfld1, 2, BasisFunctionCategory_Legendre);
  qfld2.dump( 2, output );

  QField2D_DG_BoundaryEdge qfld3(xfld1, 2, BasisFunctionCategory_Legendre);
  qfld3.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
