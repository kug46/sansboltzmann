// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// FieldAssociativityLine_btest
// testing of FieldAssociativity for a field of Line elements

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real

#include "Field/FieldGroupLine_Traits.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class FieldAssociativity< FieldGroupLineTraits<Real> >;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( FieldAssociativityLine_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  typedef FieldAssociativity< FieldGroupLineTraits<Real> > FieldLineClass;

  static_assert( std::is_same<typename FieldLineClass::FieldBase, FieldAssociativityBase<Real> >::value, "Incorrect Base type" );
  static_assert( std::is_same<typename FieldLineClass::BasisType, BasisFunctionLineBase >::value, "Incorrect Basis type" );
  static_assert( std::is_same<typename FieldLineClass::TopologyType, Line>::value, "Incorrect topology type" );
  static_assert( std::is_same<typename FieldLineClass::FieldAssociativityConstructorType,
                              FieldAssociativityConstructor<ElementAssociativityConstructor<TopoD1, Line>> >::value,
                 "Incorrect Associativity Constructor type" );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef FieldAssociativity< FieldGroupLineTraits<Real> > FieldLineClass;
  typedef typename FieldLineClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;
  typedef typename FieldLineClass::FieldBase FieldLineBase;

  int order = 1;
  ElementAssociativityConstructor<TopoD1, Line> assoc1(order);

  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( 2, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );

  assoc1.setRank( 2 );

  int node[2] = {3, 4};

  assoc1.setNodeGlobalMapping( node, 2 );

  int nElem = 1;
  FieldAssociativityConstructorType fldassoc1(BasisFunctionLineBase::HierarchicalP1, nElem);

  fldassoc1.setAssociativity(assoc1, 0);

  // constructor class
  FieldLineClass fld1(fldassoc1);

  BOOST_CHECK_EQUAL( 1, fld1.nElem() );
  BOOST_CHECK_EQUAL( 0, fld1.nDOF() );

  BOOST_CHECK( fld1.topoTypeID() == typeid(Line) );

  // default ctor and operator=
  FieldLineClass fld2;

  BOOST_CHECK_EQUAL( 0, fld2.nElem() );
  BOOST_CHECK_EQUAL( 0, fld2.nDOF() );

  fld2 = fld1;

  BOOST_CHECK_EQUAL( 1, fld2.nElem() );
  BOOST_CHECK_EQUAL( 0, fld2.nDOF() );

  BOOST_CHECK( fld2.topoTypeID() == typeid(Line) );

  // copy ctor
  FieldLineClass fld3(fld1);

  BOOST_CHECK_EQUAL( 1, fld3.nElem() );
  BOOST_CHECK_EQUAL( 0, fld3.nDOF() );

  BOOST_CHECK( fld3.topoTypeID() == typeid(Line) );

  // resize needed for new []
  FieldLineClass fld4;

  fld4.resize(fldassoc1);

  BOOST_CHECK_EQUAL( 1, fld4.nElem() );
  BOOST_CHECK_EQUAL( 0, fld4.nDOF() );

  BOOST_CHECK( fld4.topoTypeID() == typeid(Line) );

  // clone
  FieldLineBase* fld5 = fld1.clone();

  BOOST_CHECK_EQUAL( 1, fld5->nElem() );
  BOOST_CHECK_EQUAL( 0, fld5->nDOF() );

  BOOST_CHECK( fld5->topoTypeID() == typeid(Line) );
  delete fld5;
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis )
{
  typedef FieldAssociativity< FieldGroupLineTraits<Real> > FieldLineClass;
  typedef typename FieldLineClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;

  int order = 1;
  ElementAssociativityConstructor<TopoD1, Line> assoc1(order);

  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( 2, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );

  assoc1.setRank( 2 );

  int node[2] = {3, 4};

  assoc1.setNodeGlobalMapping( node, 2 );

  int nElem = 1;
  FieldAssociativityConstructorType fldassoc1(BasisFunctionLineBase::HierarchicalP1, nElem);

  fldassoc1.setAssociativity(assoc1, 0);

  FieldLineClass fld(fldassoc1);

  BOOST_CHECK_EQUAL( 1, fld.nElem() );
  BOOST_CHECK_EQUAL( 0, fld.nDOF() );

  const BasisFunctionLineBase* basis2 = fld.basis();

  BOOST_CHECK_EQUAL( 1, basis2->order() );
  BOOST_CHECK_EQUAL( 2, basis2->nBasis() );
  BOOST_CHECK_EQUAL( 2, basis2->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis2->nBasisEdge() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DOF_associativity )
{
  typedef FieldAssociativity< FieldGroupLineTraits<Real> > FieldLineClass;
  typedef typename FieldLineClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;

  ElementAssociativityConstructor<TopoD1, Line> assoc1(BasisFunctionLineBase::HierarchicalP1);

  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( 2, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );

  assoc1.setRank( 2 );

  int nodeTrue1[2] = {3, 4};
  assoc1.setNodeGlobalMapping( nodeTrue1, 2 );

  int nElem = 1;
  FieldAssociativityConstructorType fldassoc1(BasisFunctionLineBase::HierarchicalP1, nElem);

  fldassoc1.setAssociativity(assoc1, 0);

  FieldLineClass fld1(fldassoc1);

  BOOST_CHECK_EQUAL( 1, fld1.nElem() );
  BOOST_CHECK_EQUAL( 0, fld1.nDOF() );

  int node1[2];
  fld1.associativity( 0 ).getNodeGlobalMapping( node1, 2 );
  BOOST_CHECK_EQUAL( nodeTrue1[0], node1[0] );
  BOOST_CHECK_EQUAL( nodeTrue1[1], node1[1] );



  ElementAssociativityConstructor<TopoD1, Line> assoc2(BasisFunctionLineBase::HierarchicalP2);
  ElementAssociativityConstructor<TopoD1, Line> assoc2b;

  BOOST_CHECK_EQUAL( 2, assoc2.order() );
  BOOST_CHECK_EQUAL( 2, assoc2.nNode() );
  BOOST_CHECK_EQUAL( 1, assoc2.nEdge() );

  assoc2.setRank( 2 );

  int nodeTrue2[2] = {3, 4};
  int edgeTrue2[1] = {8};
  assoc2.setNodeGlobalMapping( nodeTrue2, 2 );
  assoc2.setEdgeGlobalMapping( edgeTrue2, 1 );

  FieldAssociativityConstructorType fldassoc2(BasisFunctionLineBase::HierarchicalP2, nElem);

  fldassoc2.setAssociativity(assoc2, 0);

  FieldLineClass fld2(fldassoc2);

  BOOST_CHECK_EQUAL( 1, fld2.nElem() );
  BOOST_CHECK_EQUAL( 0, fld2.nDOF() );


  int node2[2], edge2[1];
  fld2.associativity( 0 ).getNodeGlobalMapping( node2, 2 );
  BOOST_CHECK_EQUAL( 3, node2[0] );
  BOOST_CHECK_EQUAL( 4, node2[1] );

  fld2.associativity( 0 ).getEdgeGlobalMapping( edge2, 1 );
  BOOST_CHECK_EQUAL( 8, edge2[0] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( accessors )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef FieldAssociativity< FieldGroupLineTraits<ArrayQ> > FieldLineClass;
  typedef typename FieldLineClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;

  ElementAssociativityConstructor<TopoD1, Line> assoc1(BasisFunctionLineBase::HierarchicalP1);

  assoc1.setRank( 2 );
  int nodeTrue1[2] = {3, 4};
  assoc1.setNodeGlobalMapping( nodeTrue1, 2 );

  int nElem = 1;
  FieldAssociativityConstructorType fldassoc1(BasisFunctionLineBase::HierarchicalP1, nElem);

  fldassoc1.setAssociativity(assoc1, 0);

  FieldLineClass qfld(fldassoc1);

  BOOST_CHECK_EQUAL( 1, qfld.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld.nDOF() );

  ArrayQ* qDOF = new ArrayQ[2];

  qfld.setDOF( qDOF, 2 );
  BOOST_CHECK_EQUAL( 2, qfld.nDOF() );

  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};

  qfld.DOF(0) = q1;
  qfld.DOF(1) = q2;

  const Real tol = 1e-13;
  ArrayQ q;
  for (int k = 0; k < 2; k++)
  {
    q = qfld.DOF(0);
    BOOST_CHECK_CLOSE( q1[k]     , q[k], tol );
    BOOST_CHECK_CLOSE( qDOF[0][k], q[k], tol );
    q = qfld.DOF(1);
    BOOST_CHECK_CLOSE( q2[k]     , q[k], tol );
    BOOST_CHECK_CLOSE( qDOF[1][k], q[k], tol );
  }

  delete [] qDOF;
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( elementfield )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef FieldAssociativity< FieldGroupLineTraits<ArrayQ> > FieldLineClass;
  typedef typename FieldLineClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;

  const Real tol = 1e-13;

  ElementAssociativityConstructor<TopoD1, Line> assoc1(BasisFunctionLineBase::HierarchicalP1);

  assoc1.setRank( 2 );
  int node[2] = {3, 4};
  assoc1.setNodeGlobalMapping( node, 2 );

  int nElem = 1;
  FieldAssociativityConstructorType fldassoc1(BasisFunctionLineBase::HierarchicalP1, nElem);

  fldassoc1.setAssociativity(assoc1, 0);

  FieldLineClass qfld(fldassoc1);

  // solution DOFs
  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};

  ArrayQ* qDOF = new ArrayQ[5];

  qfld.setDOF( qDOF, 5 );
  BOOST_CHECK_EQUAL( 5, qfld.nDOF() );

  qfld.DOF(node[0]) = q1;
  qfld.DOF(node[1]) = q2;

  // get element DOFs

  typename FieldLineClass::template ElementType<> qfldElem( qfld.basis() );

  qfld.getElement( qfldElem, 0 );

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  ArrayQ q;
  for (int k = 0; k < 2; k++)
  {
    q = qfldElem.DOF(0);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );
    q = qfldElem.DOF(1);
    BOOST_CHECK_CLOSE( q2[k], q[k], tol );
  }

  delete [] qDOF;
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( projection_Hierarchical )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef FieldAssociativity< FieldGroupLineTraits<ArrayQ> > FieldLineClass;
  typedef typename FieldLineClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;
  typedef typename FieldLineClass::template ElementType<> ElementLineClass;

  const Real tol = 1e-15;

  for (int order = 1; order < BasisFunctionLine_HierarchicalPMax; order++)
  {
    for (int inc = 1; inc <= BasisFunctionLine_HierarchicalPMax-order; inc++)
    {
      const BasisFunctionLineBase* basis0 =
            BasisFunctionLineBase::getBasisFunction( order      , BasisFunctionCategory_Hierarchical );
      const BasisFunctionLineBase* basis1 =
            BasisFunctionLineBase::getBasisFunction( order + inc, BasisFunctionCategory_Hierarchical );

      const int nDOF0 = basis0->nBasis();
      const int nDOF1 = basis1->nBasis();

      // DOF associativity
      ElementAssociativityConstructor<TopoD1, Line> assoc0( basis0 );
      ElementAssociativityConstructor<TopoD1, Line> assoc1( basis1 );

      int* map0 = new int[ nDOF0 ];
      int* map1 = new int[ nDOF1 ];
      for (int k = 0; k < nDOF0; k++)
        map0[k] = k;
      for (int k = 0; k < nDOF1; k++)
        map1[k] = nDOF1 - 1 - k;

      assoc0.setRank( 2 );
      assoc1.setRank( 2 );

      assoc0.setGlobalMapping( map0, nDOF0 );
      assoc1.setGlobalMapping( map1, nDOF1 );

      int nElem = 1;
      FieldAssociativityConstructorType fldassoc0(basis0, nElem);
      FieldAssociativityConstructorType fldassoc1(basis1, nElem);

      fldassoc0.setAssociativity(assoc0, 0);
      fldassoc1.setAssociativity(assoc1, 0);

      FieldLineClass qfld0( fldassoc0 );
      FieldLineClass qfld1( fldassoc1 );

      // solution DOFs
      ArrayQ* qDOF0 = new ArrayQ[ nDOF0 ];
      ArrayQ* qDOF1 = new ArrayQ[ nDOF1 ];
      qfld0.setDOF( qDOF0, nDOF0 );
      qfld1.setDOF( qDOF1, nDOF1 );

      for (int k = 0; k < nDOF0; k++)
      {
        ArrayQ q = { sqrt(k+7), 1./(k+4) };
        qfld0.DOF(k) = q;
      }

      qfld0.projectTo( qfld1 );

      ElementLineClass qfldElem0( basis0 );
      ElementLineClass qfldElem1( basis1 );
      qfld0.getElement( qfldElem0, 0 );
      qfld1.getElement( qfldElem1, 0 );

      ArrayQ q0, q1;
      Real s;

      for (int is = 0; is < 3; is++)
      {
        s = 0.11 + is*0.3357;

        qfldElem0.eval( s, q0 );
        qfldElem1.eval( s, q1 );

        for (int n = 0; n < ArrayQ::N; n++)
          BOOST_CHECK_CLOSE( q0[n], q1[n], tol );
      }

      delete [] map0;
      delete [] map1;
      delete [] qDOF0;
      delete [] qDOF1;
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( projection_Legendre )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef FieldAssociativity< FieldGroupLineTraits<ArrayQ> > FieldLineClass;
  typedef typename FieldLineClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;
  typedef typename FieldLineClass::template ElementType<> ElementLineClass;

  const Real tol = 1e-15;

  for (int order = 1; order < BasisFunctionLine_LegendrePMax; order++)
  {
    for (int inc = 1; inc <= BasisFunctionLine_LegendrePMax-order; inc++)
    {
      const BasisFunctionLineBase* basis0 =
            BasisFunctionLineBase::getBasisFunction( order      , BasisFunctionCategory_Legendre );
      const BasisFunctionLineBase* basis1 =
            BasisFunctionLineBase::getBasisFunction( order + inc, BasisFunctionCategory_Legendre );

      const int nDOF0 = basis0->nBasis();
      const int nDOF1 = basis1->nBasis();

      // DOF associativity
      ElementAssociativityConstructor<TopoD1, Line> assoc0( basis0 );
      ElementAssociativityConstructor<TopoD1, Line> assoc1( basis1 );

      int* map0 = new int[ nDOF0 ];
      int* map1 = new int[ nDOF1 ];
      for (int k = 0; k < nDOF0; k++)
        map0[k] = k;
      for (int k = 0; k < nDOF1; k++)
        map1[k] = nDOF1 - 1 - k;

      assoc0.setRank( 2 );
      assoc1.setRank( 2 );

      assoc0.setGlobalMapping( map0, nDOF0 );
      assoc1.setGlobalMapping( map1, nDOF1 );

      int nElem = 1;
      FieldAssociativityConstructorType fldassoc0(basis0, nElem);
      FieldAssociativityConstructorType fldassoc1(basis1, nElem);

      fldassoc0.setAssociativity(assoc0, 0);
      fldassoc1.setAssociativity(assoc1, 0);

      FieldLineClass qfld0( fldassoc0 );
      FieldLineClass qfld1( fldassoc1 );

      // solution DOFs
      ArrayQ* qDOF0 = new ArrayQ[ nDOF0 ];
      ArrayQ* qDOF1 = new ArrayQ[ nDOF1 ];
      qfld0.setDOF( qDOF0, nDOF0 );
      qfld1.setDOF( qDOF1, nDOF1 );

      for (int k = 0; k < nDOF0; k++)
      {
        ArrayQ q = { sqrt(k+7), 1./(k+4) };
        qfld0.DOF(k) = q;
      }

      qfld0.projectTo( qfld1 );

      ElementLineClass qfldElem0( basis0 );
      ElementLineClass qfldElem1( basis1 );
      qfld0.getElement( qfldElem0, 0 );
      qfld1.getElement( qfldElem1, 0 );

      ArrayQ q0, q1;
      Real s;

      for (int is = 0; is < 3; is++)
      {
        s = 0.11 + is*0.3357;

        qfldElem0.eval( s, q0 );
        qfldElem1.eval( s, q1 );

        for (int n = 0; n < ArrayQ::N; n++)
          BOOST_CHECK_CLOSE( q0[n], q1[n], tol );
      }

      delete [] map0;
      delete [] map1;
      delete [] qDOF0;
      delete [] qDOF1;
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/FieldAssociativityLine_pattern.txt", true );

  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef FieldAssociativity< FieldGroupLineTraits<ArrayQ> > FieldLineClass;
  typedef typename FieldLineClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;

  ElementAssociativityConstructor<TopoD1, Line> assoc1(BasisFunctionLineBase::HierarchicalP1);

  assoc1.setRank( 2 );
  int node[2] = {0, 1};
  assoc1.setNodeGlobalMapping( node, 2 );

  int nElem = 1;
  FieldAssociativityConstructorType fldassoc1(BasisFunctionLineBase::HierarchicalP1, nElem);

  fldassoc1.setAssociativity(assoc1, 0);

  FieldLineClass qfld(fldassoc1);

  // solution DOFs
  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};

  ArrayQ* qDOF = new ArrayQ[2];

  qfld.setDOF( qDOF, 2 );
  BOOST_CHECK_EQUAL( 2, qfld.nDOF() );

  qfld.DOF(node[0]) = q1;
  qfld.DOF(node[1]) = q2;

  qfld.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  delete [] qDOF;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
