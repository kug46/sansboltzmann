// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// HField_CG_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Field/HField/HFieldLine_CG.h"
#include "Field/HField/HFieldArea_CG.h"
#include "Field/HField/HFieldVolume_CG.h"

#include "Field/HField/HFieldLine_DG.h"
#include "Field/HField/HFieldArea_DG.h"
#include "Field/HField/HFieldVolume_DG.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( HField_CG_test_suite )

BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK( (HField_CG<PhysD1,TopoD1>::D == 1) );
  BOOST_CHECK( (HField_CG<PhysD2,TopoD2>::D == 2) );
  BOOST_CHECK( (HField_CG<PhysD3,TopoD3>::D == 3) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_grid_equal_test )
{
  // grid
  XField1D xfld( 8 );

  typedef HField_CG<PhysD1,TopoD1>::FieldCellGroupType<Line> HFieldLineClass;
  typedef HFieldLineClass::ElementType<> ElementHFieldLineClass;

  HField_CG<PhysD1,TopoD1> hfld( xfld );

  Real perimeter = 1. + 1.;
  Real length = 1.0/8.0;
  Real exactSize = length/perimeter;

  Real h;
  HFieldLineClass& hfld1Line = hfld.getCellGroup<Line>(0);
  ElementHFieldLineClass hfldElem(hfld1Line.basis());

  for (int elem = 0; elem < hfld1Line.nElem(); elem++)
  {
    hfld1Line.getElement(hfldElem, elem);

    hfldElem.eval(0.25, h);
    BOOST_CHECK_CLOSE(h, exactSize, 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_grid_varying_test )
{
  // grid
  XField1D xfld( 4 );
  xfld.DOF(0) = -0.1;
  xfld.DOF(1) =  0.0;
  xfld.DOF(2) =  0.2;
  xfld.DOF(3) =  0.5;
  xfld.DOF(4) =  0.9;

  HField_CG<PhysD1,TopoD1> hfld( xfld );

  HField_DG<PhysD1,TopoD1> hfldDG( xfld );

  const Real close_tol = 1e-13;

  //Check if the h-value at each node is the average size of the cells around that node
  BOOST_CHECK_CLOSE(hfld.DOF(0),  hfldDG.DOF(0)                    , close_tol);
  BOOST_CHECK_CLOSE(hfld.DOF(1), (hfldDG.DOF(0) + hfldDG.DOF(1))/2., close_tol);
  BOOST_CHECK_CLOSE(hfld.DOF(2), (hfldDG.DOF(1) + hfldDG.DOF(2))/2., close_tol);
  BOOST_CHECK_CLOSE(hfld.DOF(3), (hfldDG.DOF(2) + hfldDG.DOF(3))/2., close_tol);
  BOOST_CHECK_CLOSE(hfld.DOF(4),  hfldDG.DOF(3)                    , close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Area_grid_equal_test )
{
  // grid
  XField2D_4Triangle_X1_1Group xfld;

  typedef HField_CG<PhysD2,TopoD2>::FieldCellGroupType<Triangle> HFieldTriangleClass;
  typedef HFieldTriangleClass::ElementType<> ElementHFieldTriangleClass;

  HField_CG<PhysD2,TopoD2> hfld( xfld );

  Real perimeter = 1. + 1. + sqrt(2.);
  Real area = 0.5;
  Real exactSize = area/perimeter;

  Real h;
  HFieldTriangleClass& hfld1Triangle = hfld.getCellGroup<Triangle>(0);
  ElementHFieldTriangleClass hfldElem(hfld1Triangle.basis());

  for (int elem = 0; elem < hfld1Triangle.nElem(); elem++)
  {
    hfld1Triangle.getElement(hfldElem, elem);

    hfldElem.eval(1./3., 1./3., h);
    BOOST_CHECK_CLOSE(h, exactSize, 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Area_grid_varying_test )
{
  // grid
  XField2D_4Triangle_X1_1Group xfld;

  xfld.DOF(0) = { 0.0, 0.0};
  xfld.DOF(1) = { 1.0, 0.0};
  xfld.DOF(2) = { 0.0, 1.0};
  xfld.DOF(3) = { 2.0, 1.0};
  xfld.DOF(4) = {-0.5, 1.0};
  xfld.DOF(5) = { 1.0,-3.0};

  HField_CG<PhysD2,TopoD2> hfld( xfld );

  HField_DG<PhysD2,TopoD2> hfldDG( xfld );

  const Real close_tol = 1e-13;

  //Check if the h-value at each node is the average size of the cells around that node
  BOOST_CHECK_CLOSE(hfld.DOF(0), (hfldDG.DOF(0) + hfldDG.DOF(2) + hfldDG.DOF(3))/3.0, close_tol);
  BOOST_CHECK_CLOSE(hfld.DOF(1), (hfldDG.DOF(0) + hfldDG.DOF(1) + hfldDG.DOF(3))/3.0, close_tol);
  BOOST_CHECK_CLOSE(hfld.DOF(2), (hfldDG.DOF(0) + hfldDG.DOF(1) + hfldDG.DOF(2))/3.0, close_tol);
  BOOST_CHECK_CLOSE(hfld.DOF(3), hfldDG.DOF(1), close_tol);
  BOOST_CHECK_CLOSE(hfld.DOF(4), hfldDG.DOF(2), close_tol);
  BOOST_CHECK_CLOSE(hfld.DOF(5), hfldDG.DOF(3), close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Volume_grid_equal_test )
{
  // grid
  XField3D_6Tet_X1_1Group xfld;

  HField_CG<PhysD3,TopoD3> hfld( xfld );

  HField_DG<PhysD3,TopoD3> hfldDG( xfld );

  const Real close_tol = 1e-13;

  //Check if the h-value at each node is the average size of the cells around that node
  BOOST_CHECK_CLOSE(hfld.DOF(0), hfldDG.DOF(0), close_tol);
  BOOST_CHECK_CLOSE(hfld.DOF(1), (hfldDG.DOF(0) + hfldDG.DOF(1) + hfldDG.DOF(3))/3.0, close_tol);
  BOOST_CHECK_CLOSE(hfld.DOF(2), (hfldDG.DOF(0) + hfldDG.DOF(1) + hfldDG.DOF(2))/3.0, close_tol);
  BOOST_CHECK_CLOSE(hfld.DOF(3), (hfldDG.DOF(1) + hfldDG.DOF(2) + hfldDG.DOF(3) + hfldDG.DOF(4) + hfldDG.DOF(5))/5.0, close_tol);
  BOOST_CHECK_CLOSE(hfld.DOF(4), (hfldDG.DOF(0) + hfldDG.DOF(1) + hfldDG.DOF(2) + hfldDG.DOF(3) + hfldDG.DOF(4))/5.0, close_tol);
  BOOST_CHECK_CLOSE(hfld.DOF(5), (hfldDG.DOF(3) + hfldDG.DOF(4) + hfldDG.DOF(5))/3.0, close_tol);
  BOOST_CHECK_CLOSE(hfld.DOF(6), (hfldDG.DOF(2) + hfldDG.DOF(4) + hfldDG.DOF(5))/3.0, close_tol);
  BOOST_CHECK_CLOSE(hfld.DOF(7), hfldDG.DOF(5), close_tol);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
