// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField3D_Curved_Hex_btest
// testing of higher order XField3D tet grids
//
// Note: unit grids tested in "unit/UnitGrids/UnitGrids_btest.cpp"

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/linspace.h"

#include "BasisFunction/BasisFunctionArea_Quad.h"
#include "BasisFunction/BasisFunctionArea_Quad.h"
#include "BasisFunction/BasisFunctionVolume_Hexahedron.h"
#include "BasisFunction/TraceToCellRefCoord.h"
#include "BasisFunction/ElementEdges.h"

#include "Field/XFieldVolume.h"
#include "Field/Field_CG/Field_CG_Topology.h"

#include "unit/UnitGrids/XField3D_1Hex_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Hex_X1_1Group.h"
//#include "unit/UnitGrids/XField3D_5Hex_X1_1Group_AllOrientations.h"
#include "unit/UnitGrids/XField3D_Box_Hex_X1.h"

#include "unit/Field/XField3D_CheckTraceCoord3D_btest.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( XField3D_Curved_Hex_test_suite )

//--------------------CHECKING HIGHER ORDER GRIDS-----------------------------//

std::vector<int> getEdgeOrdering_1Hex(const int order)
{
  std::set<std::pair<int,int>> edges;
  const int (*EdgeNodes)[ Line::NNode ] = ElementEdges<Hex>::EdgeNodes;

  std::map<std::pair<int,int>,bool> edgeReverse;

  for (int iedge = 0; iedge < Hex::NEdge; iedge++)
  {
    // Use the fact that nodeMap is identity for a single Tet
    int node0 = EdgeNodes[iedge][0];
    int node1 = EdgeNodes[iedge][1];

    if (node0 < node1) //key is {node0, node1}
    {
      edges.insert({node0, node1});
      edgeReverse[{node0, node1}] = false;
    }
    else //key is {node1, node0}
    {
      edges.insert({node1, node0});
      edgeReverse[{node1, node0}] = true;
    }
  }

  //go through all edges and construct the DOF indexing
  const int nDOFperEdge = TopologyDOF_CG<Line>::count(order); //(order - 1) for lines
  int edgeDOF_index = 0;
  std::map<std::pair<int,int>,std::vector<int>> edgeDOFsMap;
  for ( const std::pair<int,int>& edge : edges )
  {
    // create the indexes for the edge DOFs
    for (int i = 0; i < nDOFperEdge; i++)
      edgeDOFsMap[edge].push_back(edgeDOF_index++);

    if (edgeReverse[edge])
      std::reverse(edgeDOFsMap[edge].begin(), edgeDOFsMap[edge].end());
  }

  std::vector<int> edgeDOForder;
  for (int iedge = 0; iedge < Hex::NEdge; iedge++)
  {
    // Use the fact that nodeMap is identity for a single Tet
    int node0 = EdgeNodes[iedge][0];
    int node1 = EdgeNodes[iedge][1];

    std::pair<int,int> key;
    if (node0 < node1) //key is {node0, node1}
    {
      key = {node0, node1};
    }
    else //key is {node1, node0}
    {
      key = {node1, node0};
    }

    // flatten the edge DOFs
    for (int i = 0; i < nDOFperEdge; i++)
      edgeDOForder.push_back(edgeDOFsMap.at(key)[i]);
  }

  return edgeDOForder;
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_1Hex_X2_Lagrange_test )
{
  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  typedef std::array<int,Hex::NTrace> Int6;

  XField3D_1Hex_X1_1Group xfld_X1; //Linear mesh

  const int order = 2;

  XField<PhysD3,TopoD3> xfld(xfld_X1,order,BasisFunctionCategory_Lagrange); //Construct Q2 mesh from linear mesh

//  xfld.dump(3,std::cout);

  BOOST_CHECK_EQUAL( xfld.nDOF(), (order+1)*(order+1)*(order+1) );

  // Get the Lagrange nodes to compare with the curved grid
  std::vector<Real> coord_s, coord_t, coord_u;
  BasisFunctionVolumeBase<Hex>::LagrangeP2->coordinates( coord_s, coord_t, coord_u );

  std::vector<int> edgeDOForder = getEdgeOrdering_1Hex(order);

  const int nCellDOF =    (order - 1)*(order - 1)*(order - 1);
  const int nFaceDOF =  6*(order - 1)*(order - 1);
  const int nEdgeDOF = 12*(order - 1);
  const int nNodeDOF = 8;

  const int offset_cellDOF = 0;
  const int offset_faceDOF = nCellDOF;
  const int offset_edgeDOF = nCellDOF + nFaceDOF;
  const int offset_nodeDOF = nCellDOF + nFaceDOF + nEdgeDOF;

  int cellMap[nCellDOF];
  int faceMap[nFaceDOF];
  int edgeMap[nEdgeDOF];
  int nodeMap[nNodeDOF];

  // Note that the reference element is sorted
  // Nodes, Edge, Face, Cell
  // The grid DOFs are sorted
  // Cell, Face, Edge, Node

  // volume field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Hex) );


  const XField<PhysD3,TopoD3>::FieldCellGroupType<Hex>& xfldVol = xfld.getCellGroup<Hex>(0);


  //Cell DOFs
  xfldVol.associativity(0).getCellGlobalMapping( cellMap, nCellDOF );
  for (int n = 0; n < nCellDOF; n++)
    BOOST_CHECK_EQUAL( cellMap[n], n );

  //Face DOFs
  xfldVol.associativity(0).getFaceGlobalMapping( faceMap, nFaceDOF );
  BOOST_CHECK_EQUAL( faceMap[0], 0 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[1], 1 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[2], 3 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[3], 4 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[4], 2 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[5], 5 + nCellDOF );

  //Edge DOFs
  xfldVol.associativity(0).getEdgeGlobalMapping( edgeMap, nEdgeDOF );
  for (int n = 0; n < nEdgeDOF; n++)
    BOOST_CHECK_EQUAL( edgeMap[n], edgeDOForder[n] + nCellDOF + nFaceDOF );

  //Node DOFs
  xfldVol.associativity(0).getNodeGlobalMapping( nodeMap, nNodeDOF );
  for (int n = 0; n < nNodeDOF; n++)
    BOOST_CHECK_EQUAL( nodeMap[n], n + nCellDOF + nFaceDOF + nEdgeDOF );


  // Check cell DOFs
  for (int i = 0; i < nCellDOF; i++)
  {
    SANS_CHECK_CLOSE( coord_s[i+nNodeDOF+nEdgeDOF+nFaceDOF], xfld.DOF(i+offset_cellDOF)[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_t[i+nNodeDOF+nEdgeDOF+nFaceDOF], xfld.DOF(i+offset_cellDOF)[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_u[i+nNodeDOF+nEdgeDOF+nFaceDOF], xfld.DOF(i+offset_cellDOF)[2], small_tol, close_tol );
  }

  // Check face DOFs
  for (int i = 0; i < nFaceDOF; i++)
  {
    SANS_CHECK_CLOSE( coord_s[i+nNodeDOF+nEdgeDOF], xfld.DOF(faceMap[i])[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_t[i+nNodeDOF+nEdgeDOF], xfld.DOF(faceMap[i])[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_u[i+nNodeDOF+nEdgeDOF], xfld.DOF(faceMap[i])[2], small_tol, close_tol );
  }

  // Check edge DOFs
  for (int i = 0; i < nEdgeDOF; i++)
  {
    SANS_CHECK_CLOSE( coord_s[i+nNodeDOF], xfld.DOF(edgeDOForder[i]+offset_edgeDOF)[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_t[i+nNodeDOF], xfld.DOF(edgeDOForder[i]+offset_edgeDOF)[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_u[i+nNodeDOF], xfld.DOF(edgeDOForder[i]+offset_edgeDOF)[2], small_tol, close_tol );
  }

  //Check the node DOFs
  for (int i = 0; i < nNodeDOF; i++)
  {
    SANS_CHECK_CLOSE( coord_s[i], xfld.DOF(i+offset_nodeDOF)[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_t[i], xfld.DOF(i+offset_nodeDOF)[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_u[i], xfld.DOF(i+offset_nodeDOF)[2], small_tol, close_tol );
  }


  Int6 faceSign;

  faceSign = xfldVol.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );
  BOOST_CHECK_EQUAL( faceSign[4], +1 );
  BOOST_CHECK_EQUAL( faceSign[5], +1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 0 );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 6 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Quad>(0).topoTypeID() == typeid(Quad) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Quad>(1).topoTypeID() == typeid(Quad) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Quad>(2).topoTypeID() == typeid(Quad) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Quad>(3).topoTypeID() == typeid(Quad) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Quad>(4).topoTypeID() == typeid(Quad) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Quad>(5).topoTypeID() == typeid(Quad) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Quad>& xfldBTraceGroup0 = xfld.getBoundaryTraceGroup<Quad>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Quad>& xfldBTraceGroup1 = xfld.getBoundaryTraceGroup<Quad>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Quad>& xfldBTraceGroup2 = xfld.getBoundaryTraceGroup<Quad>(2);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Quad>& xfldBTraceGroup3 = xfld.getBoundaryTraceGroup<Quad>(3);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Quad>& xfldBTraceGroup4 = xfld.getBoundaryTraceGroup<Quad>(4);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Quad>& xfldBTraceGroup5 = xfld.getBoundaryTraceGroup<Quad>(5);

  //Node DOFs
  xfldBTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[3], 1 + nCellDOF + nFaceDOF + nEdgeDOF );

  xfldBTraceGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[3], 4 + nCellDOF + nFaceDOF + nEdgeDOF );

  xfldBTraceGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 6 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[3], 5 + nCellDOF + nFaceDOF + nEdgeDOF );

  xfldBTraceGroup3.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 7 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 6 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[3], 2 + nCellDOF + nFaceDOF + nEdgeDOF );

  xfldBTraceGroup4.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 7 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[3], 3 + nCellDOF + nFaceDOF + nEdgeDOF );

  xfldBTraceGroup5.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 6 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[3], 7 + nCellDOF + nFaceDOF + nEdgeDOF );


  //Edge DOFs
  xfldBTraceGroup0.associativity(0).getEdgeGlobalMapping( edgeMap, 4 );
  BOOST_CHECK_EQUAL( edgeMap[0],  1 + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1],  5 + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2],  3 + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[3],  0 + offset_edgeDOF );

  xfldBTraceGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 4 );
  BOOST_CHECK_EQUAL( edgeMap[0],  0 + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1],  4 + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2],  8 + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[3],  2 + offset_edgeDOF );

  xfldBTraceGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 4 );
  BOOST_CHECK_EQUAL( edgeMap[0],  3 + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1],  6 + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2], 10 + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[3],  4 + offset_edgeDOF );

  xfldBTraceGroup3.associativity(0).getEdgeGlobalMapping( edgeMap, 4 );
  BOOST_CHECK_EQUAL( edgeMap[0],  7 + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1], 11 + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2],  6 + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[3],  5 + offset_edgeDOF );

  xfldBTraceGroup4.associativity(0).getEdgeGlobalMapping( edgeMap, 4 );
  BOOST_CHECK_EQUAL( edgeMap[0],  2 + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1],  9 + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2],  7 + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[3],  1 + offset_edgeDOF );

  xfldBTraceGroup5.associativity(0).getEdgeGlobalMapping( edgeMap, 4 );
  BOOST_CHECK_EQUAL( edgeMap[0],  8 + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1], 10 + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2], 11 + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[3],  9 + offset_edgeDOF );


  //Face DOFs
  xfldBTraceGroup0.associativity(0).getFaceGlobalMapping( faceMap, 1 );
  BOOST_CHECK_EQUAL( faceMap[0], 0 + offset_faceDOF );

  xfldBTraceGroup1.associativity(0).getFaceGlobalMapping( faceMap, 1 );
  BOOST_CHECK_EQUAL( faceMap[0], 1 + offset_faceDOF );

  xfldBTraceGroup2.associativity(0).getFaceGlobalMapping( faceMap, 1 );
  BOOST_CHECK_EQUAL( faceMap[0], 3 + offset_faceDOF );

  xfldBTraceGroup3.associativity(0).getFaceGlobalMapping( faceMap, 1 );
  BOOST_CHECK_EQUAL( faceMap[0], 4 + offset_faceDOF );

  xfldBTraceGroup4.associativity(0).getFaceGlobalMapping( faceMap, 1 );
  BOOST_CHECK_EQUAL( faceMap[0], 2 + offset_faceDOF );

  xfldBTraceGroup5.associativity(0).getFaceGlobalMapping( faceMap, 1 );
  BOOST_CHECK_EQUAL( faceMap[0], 5 + offset_faceDOF );


  // boundary face-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup4.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup5.getGroupLeft(), 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup4.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup5.getElementLeft(0), 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(0).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getCanonicalTraceLeft(0).trace, 3 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup4.getCanonicalTraceLeft(0).trace, 4 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup5.getCanonicalTraceLeft(0).trace, 5 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup4.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup5.getCanonicalTraceLeft(0).orientation, 1 );

  // Check that boundary trace coordinates match
  for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
      CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_1Hex_X3_Lagrange_test )
{
  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  typedef std::array<int,Hex::NTrace> Int6;

  XField3D_1Hex_X1_1Group xfld_X1; //Linear mesh

  const int order = 3;

  XField<PhysD3,TopoD3> xfld(xfld_X1,order,BasisFunctionCategory_Lagrange); //Construct Q2 mesh from linear mesh

//  xfld.dump(3,std::cout);

  BOOST_CHECK_EQUAL( xfld.nDOF(), (order+1)*(order+1)*(order+1) );

  // Get the Lagrange nodes to compare with the curved grid
  std::vector<Real> coord_s, coord_t, coord_u;
  BasisFunctionVolumeBase<Hex>::LagrangeP3->coordinates( coord_s, coord_t, coord_u );

  std::vector<int> edgeDOForder = getEdgeOrdering_1Hex(order);

  const int nCellDOF =    (order - 1)*(order - 1)*(order - 1);
  const int nFaceDOF =  6*(order - 1)*(order - 1);
  const int nEdgeDOF = 12*(order - 1);
  const int nNodeDOF = 8;

  const int offset_cellDOF = 0;
  const int offset_faceDOF = nCellDOF;
  const int offset_edgeDOF = nCellDOF + nFaceDOF;
  const int offset_nodeDOF = nCellDOF + nFaceDOF + nEdgeDOF;

  int cellMap[nCellDOF];
  int faceMap[nFaceDOF];
  int edgeMap[nEdgeDOF];
  int nodeMap[nNodeDOF];

  // Note that the reference element is sorted
  // Nodes, Edge, Face, Cell
  // The grid DOFs are sorted
  // Cell, Face, Edge, Node

  // volume field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Hex) );


  const XField<PhysD3,TopoD3>::FieldCellGroupType<Hex>& xfldVol = xfld.getCellGroup<Hex>(0);

  //Cell DOFs
  xfldVol.associativity(0).getCellGlobalMapping( cellMap, nCellDOF );
  for (int n = 0; n < nCellDOF; n++)
    BOOST_CHECK_EQUAL( cellMap[n], n );

  //Face DOFs
  xfldVol.associativity(0).getFaceGlobalMapping( faceMap, nFaceDOF );
  BOOST_CHECK_EQUAL( faceMap[ 0],  0 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[ 1],  3 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[ 2],  2 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[ 3],  1 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[ 4],  4 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[ 5],  5 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[ 6],  6 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[ 7],  7 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[ 8], 12 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[ 9], 13 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[10], 14 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[11], 15 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[12], 17 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[13], 18 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[14], 19 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[15], 16 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[16],  8 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[17], 11 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[18], 10 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[19],  9 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[20], 20 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[21], 21 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[22], 22 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[23], 23 + nCellDOF );

  //Edge DOFs
  xfldVol.associativity(0).getEdgeGlobalMapping( edgeMap, nEdgeDOF );
  for (int n = 0; n < nEdgeDOF; n++)
    BOOST_CHECK_EQUAL( edgeMap[n], edgeDOForder[n] + nCellDOF + nFaceDOF );

  //Node DOFs
  xfldVol.associativity(0).getNodeGlobalMapping( nodeMap, nNodeDOF );
  for (int n = 0; n < nNodeDOF; n++)
    BOOST_CHECK_EQUAL( nodeMap[n], n + nCellDOF + nFaceDOF + nEdgeDOF );


  // Check cell DOFs
  for (int i = 0; i < nCellDOF; i++)
  {
    SANS_CHECK_CLOSE( coord_s[i+nNodeDOF+nEdgeDOF+nFaceDOF], xfld.DOF(i+offset_cellDOF)[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_t[i+nNodeDOF+nEdgeDOF+nFaceDOF], xfld.DOF(i+offset_cellDOF)[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_u[i+nNodeDOF+nEdgeDOF+nFaceDOF], xfld.DOF(i+offset_cellDOF)[2], small_tol, close_tol );
  }

  // Check face DOFs
  for (int i = 0; i < nFaceDOF; i++)
  {
    SANS_CHECK_CLOSE( coord_s[i+nNodeDOF+nEdgeDOF], xfld.DOF(faceMap[i])[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_t[i+nNodeDOF+nEdgeDOF], xfld.DOF(faceMap[i])[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_u[i+nNodeDOF+nEdgeDOF], xfld.DOF(faceMap[i])[2], small_tol, close_tol );
  }

  // Check edge DOFs
  for (int i = 0; i < nEdgeDOF; i++)
  {
    SANS_CHECK_CLOSE( coord_s[i+nNodeDOF], xfld.DOF(edgeDOForder[i]+offset_edgeDOF)[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_t[i+nNodeDOF], xfld.DOF(edgeDOForder[i]+offset_edgeDOF)[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_u[i+nNodeDOF], xfld.DOF(edgeDOForder[i]+offset_edgeDOF)[2], small_tol, close_tol );
  }

  //Check the node DOFs
  for (int i = 0; i < nNodeDOF; i++)
  {
    SANS_CHECK_CLOSE( coord_s[i], xfld.DOF(i+offset_nodeDOF)[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_t[i], xfld.DOF(i+offset_nodeDOF)[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_u[i], xfld.DOF(i+offset_nodeDOF)[2], small_tol, close_tol );
  }


  Int6 faceSign;

  faceSign = xfldVol.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );
  BOOST_CHECK_EQUAL( faceSign[4], +1 );
  BOOST_CHECK_EQUAL( faceSign[5], +1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 0 );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 6 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Quad>(0).topoTypeID() == typeid(Quad) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Quad>(1).topoTypeID() == typeid(Quad) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Quad>(2).topoTypeID() == typeid(Quad) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Quad>(3).topoTypeID() == typeid(Quad) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Quad>(4).topoTypeID() == typeid(Quad) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Quad>(5).topoTypeID() == typeid(Quad) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Quad>& xfldBTraceGroup0 = xfld.getBoundaryTraceGroup<Quad>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Quad>& xfldBTraceGroup1 = xfld.getBoundaryTraceGroup<Quad>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Quad>& xfldBTraceGroup2 = xfld.getBoundaryTraceGroup<Quad>(2);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Quad>& xfldBTraceGroup3 = xfld.getBoundaryTraceGroup<Quad>(3);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Quad>& xfldBTraceGroup4 = xfld.getBoundaryTraceGroup<Quad>(4);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Quad>& xfldBTraceGroup5 = xfld.getBoundaryTraceGroup<Quad>(5);

  //Node DOFs
  xfldBTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[3], 1 + nCellDOF + nFaceDOF + nEdgeDOF );

  xfldBTraceGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[3], 4 + nCellDOF + nFaceDOF + nEdgeDOF );

  xfldBTraceGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 6 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[3], 5 + nCellDOF + nFaceDOF + nEdgeDOF );

  xfldBTraceGroup3.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 7 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 6 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[3], 2 + nCellDOF + nFaceDOF + nEdgeDOF );

  xfldBTraceGroup4.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 7 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[3], 3 + nCellDOF + nFaceDOF + nEdgeDOF );

  xfldBTraceGroup5.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 6 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[3], 7 + nCellDOF + nFaceDOF + nEdgeDOF );


  //Edge DOFs
  xfldBTraceGroup0.associativity(0).getEdgeGlobalMapping( edgeMap, 8 );
  BOOST_CHECK_EQUAL( edgeMap[0], 10 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1], 11 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2], 19 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[3], 18 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[4], 15 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[5], 14 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[6],  9 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[7],  8 - nNodeDOF + offset_edgeDOF );

  xfldBTraceGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 8 );
  BOOST_CHECK_EQUAL( edgeMap[0],  8 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1],  9 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2], 16 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[3], 17 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[4], 25 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[5], 24 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[6], 13 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[7], 12 - nNodeDOF + offset_edgeDOF );

  xfldBTraceGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 8 );
  BOOST_CHECK_EQUAL( edgeMap[0], 14 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1], 15 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2], 20 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[3], 21 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[4], 29 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[5], 28 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[6], 17 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[7], 16 - nNodeDOF + offset_edgeDOF );

  xfldBTraceGroup3.associativity(0).getEdgeGlobalMapping( edgeMap, 8 );
  BOOST_CHECK_EQUAL( edgeMap[0], 22 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1], 23 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2], 31 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[3], 30 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[4], 21 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[5], 20 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[6], 18 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[7], 19 - nNodeDOF + offset_edgeDOF );

  xfldBTraceGroup4.associativity(0).getEdgeGlobalMapping( edgeMap, 8 );
  BOOST_CHECK_EQUAL( edgeMap[0], 12 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1], 13 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2], 26 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[3], 27 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[4], 23 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[5], 22 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[6], 11 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[7], 10 - nNodeDOF + offset_edgeDOF );

  xfldBTraceGroup5.associativity(0).getEdgeGlobalMapping( edgeMap, 8 );
  BOOST_CHECK_EQUAL( edgeMap[0], 24 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1], 25 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2], 28 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[3], 29 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[4], 30 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[5], 31 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[6], 27 - nNodeDOF + offset_edgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[7], 26 - nNodeDOF + offset_edgeDOF );

  //Face DOFs
  xfldBTraceGroup0.associativity(0).getFaceGlobalMapping( faceMap, 4 );
  BOOST_CHECK_EQUAL( faceMap[0], 32 - nNodeDOF - nEdgeDOF + offset_faceDOF );
  BOOST_CHECK_EQUAL( faceMap[1], 35 - nNodeDOF - nEdgeDOF + offset_faceDOF );
  BOOST_CHECK_EQUAL( faceMap[2], 34 - nNodeDOF - nEdgeDOF + offset_faceDOF );
  BOOST_CHECK_EQUAL( faceMap[3], 33 - nNodeDOF - nEdgeDOF + offset_faceDOF );

  xfldBTraceGroup1.associativity(0).getFaceGlobalMapping( faceMap, 4 );
  BOOST_CHECK_EQUAL( faceMap[0], 36 - nNodeDOF - nEdgeDOF + offset_faceDOF );
  BOOST_CHECK_EQUAL( faceMap[1], 37 - nNodeDOF - nEdgeDOF + offset_faceDOF );
  BOOST_CHECK_EQUAL( faceMap[2], 38 - nNodeDOF - nEdgeDOF + offset_faceDOF );
  BOOST_CHECK_EQUAL( faceMap[3], 39 - nNodeDOF - nEdgeDOF + offset_faceDOF );

  xfldBTraceGroup2.associativity(0).getFaceGlobalMapping( faceMap, 4 );
  BOOST_CHECK_EQUAL( faceMap[0], 44 - nNodeDOF - nEdgeDOF + offset_faceDOF );
  BOOST_CHECK_EQUAL( faceMap[1], 45 - nNodeDOF - nEdgeDOF + offset_faceDOF );
  BOOST_CHECK_EQUAL( faceMap[2], 46 - nNodeDOF - nEdgeDOF + offset_faceDOF );
  BOOST_CHECK_EQUAL( faceMap[3], 47 - nNodeDOF - nEdgeDOF + offset_faceDOF );

  xfldBTraceGroup3.associativity(0).getFaceGlobalMapping( faceMap, 4 );
  BOOST_CHECK_EQUAL( faceMap[0], 49 - nNodeDOF - nEdgeDOF + offset_faceDOF );
  BOOST_CHECK_EQUAL( faceMap[1], 50 - nNodeDOF - nEdgeDOF + offset_faceDOF );
  BOOST_CHECK_EQUAL( faceMap[2], 51 - nNodeDOF - nEdgeDOF + offset_faceDOF );
  BOOST_CHECK_EQUAL( faceMap[3], 48 - nNodeDOF - nEdgeDOF + offset_faceDOF );

  xfldBTraceGroup4.associativity(0).getFaceGlobalMapping( faceMap, 4 );
  BOOST_CHECK_EQUAL( faceMap[0], 40 - nNodeDOF - nEdgeDOF + offset_faceDOF );
  BOOST_CHECK_EQUAL( faceMap[1], 43 - nNodeDOF - nEdgeDOF + offset_faceDOF );
  BOOST_CHECK_EQUAL( faceMap[2], 42 - nNodeDOF - nEdgeDOF + offset_faceDOF );
  BOOST_CHECK_EQUAL( faceMap[3], 41 - nNodeDOF - nEdgeDOF + offset_faceDOF );

  xfldBTraceGroup5.associativity(0).getFaceGlobalMapping( faceMap, 4 );
  BOOST_CHECK_EQUAL( faceMap[0], 52 - nNodeDOF - nEdgeDOF + offset_faceDOF );
  BOOST_CHECK_EQUAL( faceMap[1], 53 - nNodeDOF - nEdgeDOF + offset_faceDOF );
  BOOST_CHECK_EQUAL( faceMap[2], 54 - nNodeDOF - nEdgeDOF + offset_faceDOF );
  BOOST_CHECK_EQUAL( faceMap[3], 55 - nNodeDOF - nEdgeDOF + offset_faceDOF );

  // boundary face-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup4.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup5.getGroupLeft(), 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup4.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup5.getElementLeft(0), 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(0).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getCanonicalTraceLeft(0).trace, 3 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup4.getCanonicalTraceLeft(0).trace, 4 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup5.getCanonicalTraceLeft(0).trace, 5 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup4.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup5.getCanonicalTraceLeft(0).orientation, 1 );

  // Check that boundary trace coordinates match
  for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
      CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_CheckTrace_Lagrange_test )
{
  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  //mpi::communicator comm = world.split(world.rank());

  const Real small_tol = 1e-12;
  const Real close_tol = 5e-11;

  for ( int order = 1; order <= BasisFunctionVolume_Hex_LagrangePMax; order++)
  {
#if 0
    for (int orientation : {-3,-2,-1, 1, 2, 3})
    {
      XField3D_5Hex_X1_1Group_AllOrientations xfld_X1(orientation); //Linear mesh

      //Construct curved mesh from linear mesh
      XField<PhysD3,TopoD3> xfld(xfld_X1, order, BasisFunctionCategory_Lagrange);

      // Check that interior and boundary trace coordinates match
      for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply(
          CheckInteriorTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

      for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
          CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
    }
#endif
    XField3D_Box_Hex_X1 xfld_X1(world, 3,3,3); //Linear mesh

    //Construct curved mesh from linear mesh
    XField<PhysD3,TopoD3> xfld(xfld_X1, order, BasisFunctionCategory_Lagrange);

    // Check that interior and boundary trace coordinates match
    for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply(
        CheckInteriorTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

    for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
        CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );


    // check the cellID matches
    BOOST_REQUIRE_EQUAL(xfld_X1.nCellGroups(), xfld.nCellGroups());

    for (int group = 0; group < xfld_X1.nCellGroups(); group++)
    {
      const std::vector<int>& cellIDs_X1 = xfld_X1.cellIDs(group);
      const std::vector<int>& cellIDs    = xfld.cellIDs(group);

      BOOST_REQUIRE_EQUAL(cellIDs_X1.size(), cellIDs.size());

      for (std::size_t ielem = 0; ielem < cellIDs.size(); ielem++)
        BOOST_CHECK_EQUAL(cellIDs_X1[ielem], cellIDs[ielem]);
    }

    // check the BoundaryTraceID matches
    BOOST_REQUIRE_EQUAL(xfld_X1.nBoundaryTraceGroups(), xfld.nBoundaryTraceGroups());

    for (int group = 0; group < xfld_X1.nBoundaryTraceGroups(); group++)
    {
      const std::vector<int>& boundaryIDs_X1 = xfld_X1.boundaryTraceIDs(group);
      const std::vector<int>& boundaryIDs    = xfld.boundaryTraceIDs(group);

      BOOST_REQUIRE_EQUAL(boundaryIDs_X1.size(), boundaryIDs.size());

      for (std::size_t ielem = 0; ielem < boundaryIDs.size(); ielem++)
        BOOST_CHECK_EQUAL(boundaryIDs_X1[ielem], boundaryIDs[ielem]);
    }

  }
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
