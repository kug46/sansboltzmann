// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ProjectSolnCell_Galerkin_btest
// testing of cell solution projection for Galerkin

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldVolume_DG_Cell.h"

#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/tools/for_each_CellGroup.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Quad_X1_1Group.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Hex_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( ProjectSolnCell_Discontinuous_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ProjectSolnCell_Discontinuous_1D_Linear )
{

  typedef SolnNDConvertSpace<PhysD1, ScalarFunction1D_Linear> NDSolutionExact;
  typedef Real ArrayQ;

  Real a0 = 1;
  Real ax = 2;
  NDSolutionExact solnExact(a0, ax);

  // grid: three lines, P1 (aka X1)
  XField1D xfld(3);

  // solution: P1 (aka Q1), which spans the space of the Linear solution
  // the check below assumes Hierarchical basis
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );


  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  const XField1D              ::template FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);
  const Field<PhysD1, TopoD1, ArrayQ>::template FieldCellGroupType<Line>& qfldCell = qfld.getCellGroup<Line>(0);

  typedef XField1D              ::template FieldCellGroupType<Line>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysD1, TopoD1, ArrayQ>::template FieldCellGroupType<Line>::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass qfldElem( qfldCell.basis() );

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    qfldCell.getElement( qfldElem, elem );

    BOOST_REQUIRE_EQUAL(xfldElem.nDOF(), qfldElem.nDOF());
    for (int n = 0; n < qfldElem.nDOF(); n++)
    {
      Real exact = solnExact(xfldElem.DOF(n));
      SANS_CHECK_CLOSE( exact, qfldElem.DOF(n), small_tol, close_tol );
    }
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ProjectSolnCell_Discontinuous_2D_Triangle )
{

  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Linear> NDSolutionExact;
  typedef Real ArrayQ;

  Real a0 = 1;
  Real ax = 2;
  Real ay = 3;
  NDSolutionExact solnExact(a0, ax, ay);

  // grid: four triangles, P1 (aka X1)
  XField2D_4Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1), which spans the space of the Linear solution
  // the check below assumes Hierarchical basis
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  const XField2D_4Triangle_X1_1Group ::template FieldCellGroupType<Triangle>& xfldCell = xfld.getCellGroup<Triangle>(0);
  const Field<PhysD2, TopoD2, ArrayQ>::template FieldCellGroupType<Triangle>& qfldCell = qfld.getCellGroup<Triangle>(0);

  typedef XField2D_4Triangle_X1_1Group ::template FieldCellGroupType<Triangle>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysD2, TopoD2, ArrayQ>::template FieldCellGroupType<Triangle>::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass qfldElem( qfldCell.basis() );

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    qfldCell.getElement( qfldElem, elem );

    BOOST_REQUIRE_EQUAL(xfldElem.nDOF(), qfldElem.nDOF());
    for (int n = 0; n < qfldElem.nDOF(); n++)
    {
      Real exact = solnExact(xfldElem.DOF(n));
      SANS_CHECK_CLOSE( exact, qfldElem.DOF(n), small_tol, close_tol );
    }
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ProjectSolnCell_Discontinuous_2D_Quad )
{

  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Linear> NDSolutionExact;
  typedef Real ArrayQ;

  Real a0 = 1;
  Real ax = 2;
  Real ay = 3;
  NDSolutionExact solnExact(a0, ax, ay);

  // grid: four quads, P1 (aka X1)
  XField2D_4Quad_X1_1Group xfld;

  // solution: P1 (aka Q1), which spans the space of the Linear solution
  // the check below assumes Hierarchical basis
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );


  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  const XField2D_4Quad_X1_1Group     ::template FieldCellGroupType<Quad>& xfldCell = xfld.getCellGroup<Quad>(0);
  const Field<PhysD2, TopoD2, ArrayQ>::template FieldCellGroupType<Quad>& qfldCell = qfld.getCellGroup<Quad>(0);

  typedef XField2D_4Quad_X1_1Group     ::template FieldCellGroupType<Quad>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysD2, TopoD2, ArrayQ>::template FieldCellGroupType<Quad>::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass qfldElem( qfldCell.basis() );

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    qfldCell.getElement( qfldElem, elem );

    BOOST_REQUIRE_EQUAL(xfldElem.nDOF(), qfldElem.nDOF());
    for (int n = 0; n < qfldElem.nDOF(); n++)
    {
      Real exact = solnExact(xfldElem.DOF(n));
      SANS_CHECK_CLOSE( exact, qfldElem.DOF(n), small_tol, close_tol );
    }
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ProjectSolnCell_Discontinuous_3D_Tet )
{

  typedef SolnNDConvertSpace<PhysD3, ScalarFunction3D_Linear> NDSolutionExact;
  typedef Real ArrayQ;

  Real a0 = 1;
  Real ax = 2;
  Real ay = 3;
  Real az = 4;
  NDSolutionExact solnExact(a0, ax, ay, az);

  // grid: six tets, P1 (aka X1)
  XField3D_6Tet_X1_1Group xfld;

  // solution: P1 (aka Q1), which spans the space of the Linear solution
  // the check below assumes Hierarchical basis
  int qorder = 1;
  Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  for_each_CellGroup<TopoD3>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );


  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  const XField3D_6Tet_X1_1Group      ::template FieldCellGroupType<Tet>& xfldCell = xfld.getCellGroup<Tet>(0);
  const Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<Tet>& qfldCell = qfld.getCellGroup<Tet>(0);

  typedef XField3D_6Tet_X1_1Group      ::template FieldCellGroupType<Tet>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<Tet>::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass qfldElem( qfldCell.basis() );

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    qfldCell.getElement( qfldElem, elem );

    BOOST_REQUIRE_EQUAL(xfldElem.nDOF(), qfldElem.nDOF());
    for (int n = 0; n < qfldElem.nDOF(); n++)
    {
      Real exact = solnExact(xfldElem.DOF(n));
      SANS_CHECK_CLOSE( exact, qfldElem.DOF(n), small_tol, close_tol );
    }
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ProjectSolnCell_Discontinuous_3D_Hex )
{

  typedef SolnNDConvertSpace<PhysD3, ScalarFunction3D_Linear> NDSolutionExact;
  typedef Real ArrayQ;

  Real a0 = 1;
  Real ax = 2;
  Real ay = 3;
  Real az = 4;
  NDSolutionExact solnExact(a0, ax, ay, az);

  // grid: two hex, P1 (aka X1)
  XField3D_2Hex_X1_1Group xfld;

  // solution: P1 (aka Q1), which spans the space of the Linear solution
  // the check below assumes Hierarchical basis
  int qorder = 1;
  Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  for_each_CellGroup<TopoD3>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );


  const Real small_tol = 5e-11;
  const Real close_tol = 5e-11;

  const XField3D_2Hex_X1_1Group      ::template FieldCellGroupType<Hex>& xfldCell = xfld.getCellGroup<Hex>(0);
  const Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<Hex>& qfldCell = qfld.getCellGroup<Hex>(0);

  typedef XField3D_2Hex_X1_1Group      ::template FieldCellGroupType<Hex>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<Hex>::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass qfldElem( qfldCell.basis() );

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    qfldCell.getElement( qfldElem, elem );

    BOOST_REQUIRE_EQUAL(xfldElem.nDOF(), qfldElem.nDOF());
    for (int n = 0; n < qfldElem.nDOF(); n++)
    {
      Real exact = solnExact(xfldElem.DOF(n));
      SANS_CHECK_CLOSE( exact, qfldElem.DOF(n), small_tol, close_tol );
    }
  }

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
