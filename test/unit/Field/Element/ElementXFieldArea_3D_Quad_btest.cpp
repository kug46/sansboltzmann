// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementXFieldArea_3D_Quad_btest
// testing of ElementXFieldArea class w/ quad

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <memory>

#include "tools/SANSnumerics.h"     // Real
#include "tools/stringify.h"     // Real
#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "BasisFunction/BasisFunctionArea_Quad.h"
#include "BasisFunction/TraceToCellRefCoord.h"

using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class ElementXField<PhysD3,TopoD2,Quad>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementXFieldArea_3D_Quad_test_suite )

//TODO: missing manifold test

////----------------------------------------------------------------------------//
//BOOST_AUTO_TEST_CASE( evalBasisGradient_PhysD3_test )

////----------------------------------------------------------------------------//
//// check that the trace element coordinates evaluate to the volume element coordinates on the trace
//BOOST_AUTO_TEST_CASE( Trace_RefCoord_P1_test )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( unitNormal_test )
{
  const Real small_tol = 1e-13;
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysD3, TopoD2, Quad> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 4, xElem.nDOF() );

  ElementXField<PhysD3, TopoD2, Quad>::RefCoordType sRef;
  ElementXField<PhysD3, TopoD2, Quad>::VectorX N;
  Real nxTrue, nyTrue, nzTrue;

  sRef = 1./2.;

  xElem.DOF(0) = {0, 0, 0};
  xElem.DOF(1) = {1, 0, 0};
  xElem.DOF(2) = {1, 1, 0};
  xElem.DOF(3) = {0, 1, 0};

  nxTrue =  0;
  nyTrue =  0;
  nzTrue =  1;

  xElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );

  xElem.DOF(0) = {0, 0, 0};
  xElem.DOF(1) = {0, 1, 0};
  xElem.DOF(2) = {1, 1, 0};
  xElem.DOF(3) = {1, 0, 0};

  nxTrue =  0;
  nyTrue =  0;
  nzTrue = -1;

  xElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );

  xElem.DOF(0) = {0, 0, 0};
  xElem.DOF(1) = {0, 0, 1};
  xElem.DOF(2) = {0, 1, 1};
  xElem.DOF(3) = {0, 1, 0};

  nxTrue = -1;
  nyTrue =  0;
  nzTrue =  0;

  xElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );

  xElem.DOF(0) = {0, 0, 0};
  xElem.DOF(1) = {1, 0, 0};
  xElem.DOF(2) = {1, 0, 1};
  xElem.DOF(3) = {0, 0, 1};

  nxTrue =  0;
  nyTrue = -1;
  nzTrue =  0;

  xElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );

  xElem.DOF(0) = {1, 0, 0};
  xElem.DOF(1) = {0, 1, 0};
  xElem.DOF(2) = {0, 0, 1};
  xElem.DOF(3) = {1, -1, 1};

  nxTrue =  1./sqrt(3);
  nyTrue =  1./sqrt(3);
  nzTrue =  1./sqrt(3);

  sRef = {1, 0};
  xElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );

  sRef = {0, 1};
  xElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );

  xElem.DOF(0) = {0, 0, 0};
  xElem.DOF(1) = {1, 0, 0};
  xElem.DOF(2) = {1, 1, 1};
  xElem.DOF(3) = {0, 1, 0};

  sRef = 0;
  nxTrue =  0;
  nyTrue =  0;
  nzTrue =  1;

  xElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
