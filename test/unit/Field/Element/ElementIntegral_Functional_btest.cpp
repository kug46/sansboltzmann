// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementIntegral_Functional_btest
// testing of element integral for functional

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Field/Element/ElementIntegral.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"

using namespace std;
using namespace SANS;


namespace           // local definition
{


//----------------------------------------------------------------------------//
// Element integrand: solution squared

template <class TopoDim, class Topology, class ArrayQ>
class IntegrandFunctor_Square
{
public:
  typedef typename Element<ArrayQ, TopoDim, Topology>::RefCoordType RefCoordType;

  // cppcheck-suppress noExplicitConstructor
  IntegrandFunctor_Square(
    const Element<ArrayQ, TopoDim, Topology>& qfldElem ) :
    qfldElem_(qfldElem) {}

  // Element integrand
  void operator()( const QuadraturePoint<TopoDim>& ref, Real& integrand ) const
  {
    ArrayQ q;
    qfldElem_.eval( ref, q );
    integrand = q*q;
  }

private:
  const Element<ArrayQ, TopoDim, Topology>& qfldElem_;
};

template <class TopoDim, class Topology, class ArrayQ>
class IntegrandFunctor_Square2
{
public:
  typedef typename Element<ArrayQ, TopoDim, Topology>::RefCoordType RefCoordType;

  IntegrandFunctor_Square2(
    const Element<ArrayQ, TopoDim, Topology>& qfldElemL,
    const Element<ArrayQ, TopoDim, Topology>& qfldElemR ) :
    qfldElemL_(qfldElemL), qfldElemR_(qfldElemR) {}

  // Element integrand
  void operator()( const QuadraturePoint<TopoDim>& ref, Real& integrandL, Real& integrandR ) const
  {
    ArrayQ qL, qR;
    qfldElemL_.eval( ref, qL );
    qfldElemR_.eval( ref, qR );
    integrandL = qL*qL;
    integrandR = qR*qR;
  }

private:
  const Element<ArrayQ, TopoDim, Topology>& qfldElemL_;
  const Element<ArrayQ, TopoDim, Topology>& qfldElemR_;
};

template <class TopoDim, class Topology, class ArrayQ>
class IntegrandFunctor_Square3
{
public:
  typedef typename Element<ArrayQ, TopoDim, Topology>::RefCoordType RefCoordType;

  IntegrandFunctor_Square3(
    const Element<ArrayQ, TopoDim, Topology>& qfldElemL,
    const Element<ArrayQ, TopoDim, Topology>& qfldElemR,
    const Element<ArrayQ, TopoDim, Topology>& qfldElemTrace) :
    qfldElemL_(qfldElemL), qfldElemR_(qfldElemR), qfldElemTrace_(qfldElemTrace) {}

  // Element integrand
  void operator()( const QuadraturePoint<TopoDim>& ref, Real& integrandL, Real& integrandR, Real& integrandTrace ) const
  {
    ArrayQ qL, qR, qT;
    qfldElemL_.eval( ref, qL );
    qfldElemR_.eval( ref, qR );
    qfldElemTrace_.eval( ref, qT );
    integrandL = qL*qL;
    integrandR = qR*qR;
    integrandTrace = qT*qT;
  }

private:
  const Element<ArrayQ, TopoDim, Topology>& qfldElemL_;
  const Element<ArrayQ, TopoDim, Topology>& qfldElemR_;
  const Element<ArrayQ, TopoDim, Topology>& qfldElemTrace_;
};

}                   // local definition


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementIntegral_Functional_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementIntegral2DArea_Functional_Squared )
{
  typedef DLA::VectorS<1,Real> ArrayQ;

  typedef ElementXField<PhysD2, TopoD2, Triangle> ElementXFieldClass;
  typedef Element<ArrayQ, TopoD2, Triangle>       ElementQFieldClass;

  typedef IntegrandFunctor_Square<TopoD2, Triangle, ArrayQ> FunctorClass;

  // grid

  int order = 1;
  ElementXFieldClass xfld(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfld.order() );
  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );

  // triangle grid
  xfld.DOF(0) = {0, 0};
  xfld.DOF(1) = {1, 0};
  xfld.DOF(2) = {0, 1};

  // solution

  ElementQFieldClass qfld(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfld.order() );
  BOOST_CHECK_EQUAL( 3, qfld.nDOF() );

  // triangle solution
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  // integrand functor
  FunctorClass fcn( qfld );

  // quadrature rule: quadratic
  int quadorder = 2;

  Real functional = 0;

  ElementIntegral<TopoD2, Triangle, Real> ElemIntegral(quadorder);

  ElemIntegral( fcn, xfld, functional );

  const Real tol = 1e-13;
  BOOST_CHECK_CLOSE( 15./4., functional, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementIntegral2DBoundaryEdge_Functional_Squared )
{
  typedef DLA::VectorS<1,Real> ArrayQ;

  typedef ElementXField<PhysD2, TopoD1, Line> ElementXFieldClass;
  typedef Element<ArrayQ, TopoD1, Line>       ElementQFieldClass;

  typedef IntegrandFunctor_Square<TopoD1, Line, ArrayQ> FunctorClass;

  // grid

  int order = 1;
  ElementXFieldClass xfld(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfld.order() );
  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );

  xfld.DOF(0) = {0,  1};
  xfld.DOF(1) = {1, -2};

  // solution

  ElementQFieldClass qfld(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfld.order() );
  BOOST_CHECK_EQUAL( 2, qfld.nDOF() );

  // triangle solution
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;

  // integrand functor
  FunctorClass fcn( qfld );

  // quadrature rule: quadratic
  int quadorder = 2;

  Real functional = 0;

  ElementIntegral<TopoD1, Line, Real> ElemIntegral(quadorder);

  ElemIntegral( fcn, xfld, functional );

  const Real tol = 1e-13;
  BOOST_CHECK_CLOSE( 13*sqrt(10)/3., functional, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementIntegral2DArea_Functional_Squared2 )
{
  typedef DLA::VectorS<1,Real> ArrayQ;

  typedef ElementXField<PhysD2, TopoD2, Triangle> ElementXFieldClass;
  typedef Element<ArrayQ, TopoD2, Triangle>       ElementQFieldClass;

  typedef IntegrandFunctor_Square2<TopoD2, Triangle, ArrayQ> FunctorClass;

  // grid

  int order = 1;
  ElementXFieldClass xfld(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfld.order() );
  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );

  // triangle grid
  xfld.DOF(0) = {0, 0};
  xfld.DOF(1) = {1, 0};
  xfld.DOF(2) = {0, 1};

  // solution

  ElementQFieldClass qfldL(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldL.order() );
  BOOST_CHECK_EQUAL( 3, qfldL.nDOF() );

  // triangle solution
  qfldL.DOF(0) = 1;
  qfldL.DOF(1) = 3;
  qfldL.DOF(2) = 4;

  ElementQFieldClass qfldR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldR.order() );
  BOOST_CHECK_EQUAL( 3, qfldR.nDOF() );

  // triangle solution
  qfldR.DOF(0) = 5;
  qfldR.DOF(1) = 2;
  qfldR.DOF(2) = 7;

  // integrand functor
  FunctorClass fcn( qfldL, qfldR );

  // quadrature rule: quadratic
  int quadorder = 2;

  Real functionalL = 0;
  Real functionalR = 0;

  ElementIntegral<TopoD2, Triangle, Real, Real> ElemIntegral(quadorder);

  ElemIntegral( fcn, xfld, functionalL, functionalR );

  const Real tol = 1e-13;
  BOOST_CHECK_CLOSE( 15./4., functionalL, tol );
  BOOST_CHECK_CLOSE( 137./12., functionalR, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementIntegral2DArea_Functional_Squared3 )
{
  typedef DLA::VectorS<1,Real> ArrayQ;

  typedef ElementXField<PhysD1, TopoD1, Line> ElementXFieldClass;
  typedef Element<ArrayQ, TopoD1, Line>       ElementQFieldClass;

  typedef IntegrandFunctor_Square3<TopoD1, Line, ArrayQ> FunctorClass;

  // grid

  int order = 1;
  ElementXFieldClass xfld(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfld.order() );
  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );

  xfld.DOF(0) = 0;
  xfld.DOF(1) = 1;

  // solution

  ElementQFieldClass qfldL(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldL.order() );
  BOOST_CHECK_EQUAL( 2, qfldL.nDOF() );

  // Line solution
  qfldL.DOF(0) = 1;
  qfldL.DOF(1) = 3;

  ElementQFieldClass qfldR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldR.order() );
  BOOST_CHECK_EQUAL( 2, qfldR.nDOF() );

  // Line solution
  qfldR.DOF(0) = 5;
  qfldR.DOF(1) = 2;

  ElementQFieldClass qfldTrace(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldTrace.order() );
  BOOST_CHECK_EQUAL( 2, qfldTrace.nDOF() );

  // Line solution
  qfldTrace.DOF(0) = 6;
  qfldTrace.DOF(1) = 4;

  // integrand functor
  FunctorClass fcn( qfldL, qfldR, qfldTrace );

  // quadrature rule: quadratic
  int quadorder = 2;

  Real functionalL = 0;
  Real functionalR = 0;
  Real functionalTrace = 0;

  ElementIntegral<TopoD1, Line, Real, Real, Real> ElemIntegral(quadorder);

  ElemIntegral( fcn, xfld, functionalL, functionalR, functionalTrace );

  const Real tol = 1e-13;
  BOOST_CHECK_CLOSE( 13./3., functionalL, tol );
  BOOST_CHECK_CLOSE( 13.   , functionalR, tol );
  BOOST_CHECK_CLOSE( 76./3., functionalTrace, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
