// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementXFieldArea_2D_Quad_btest
// testing of ElementXFieldArea class w/ quad

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <memory>

#include <boost/mpl/list.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "tools/stringify.h"     // Real
#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "BasisFunction/BasisFunctionArea_Quad.h"
#include "BasisFunction/TraceToCellRefCoord.h"

using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class ElementXField<PhysD2,TopoD2,Quad>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementXFieldArea_2D_Quad_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BoundingBox_Q1_test )
{
  int order = 1;
  ElementXField<PhysD2,TopoD2,Quad> xElem(order, BasisFunctionCategory_Lagrange);

  xElem.DOF(0) = {0,0};
  xElem.DOF(1) = {1,0};
  xElem.DOF(2) = {1,1};
  xElem.DOF(3) = {0,1};

  BoundingBox<PhysD2> bbox = xElem.boundingBox();

  BOOST_CHECK_EQUAL(0, bbox.low_bounds()[0]);
  BOOST_CHECK_EQUAL(0, bbox.low_bounds()[1]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[0]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[1]);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BoundingBox_Q2_test )
{
  int order = 2;
  ElementXField<PhysD2,TopoD2,Quad> xElem(order, BasisFunctionCategory_Lagrange);

  // node DOFs
  xElem.DOF(0) = {0,0};
  xElem.DOF(1) = {1,0};
  xElem.DOF(2) = {1,1};
  xElem.DOF(3) = {0,1};

  // edge DOFs
  xElem.DOF(4) = {0.5,-0.1};
  xElem.DOF(5) = {1.2, 0.5};
  xElem.DOF(6) = {0.5, 1.3};
  xElem.DOF(7) = {-0.4, 0.5};

  // face DOF
  xElem.DOF(8) = {0.45, 0.55};

  BoundingBox<PhysD2> bbox = xElem.boundingBox();

  BOOST_CHECK_EQUAL(-0.4, bbox.low_bounds()[0]);
  BOOST_CHECK_EQUAL(-0.1, bbox.low_bounds()[1]);
  BOOST_CHECK_EQUAL(1.2, bbox.high_bounds()[0]);
  BOOST_CHECK_EQUAL(1.3, bbox.high_bounds()[1]);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( evalBasisGradient_Hessian_Q1_test )
{
  typedef BasisFunctionArea<Quad,Hierarchical,1> BFieldClass;

  int order = 1;
  ElementXField<PhysD2,TopoD2,Quad> xElem(order, BasisFunctionCategory_Hierarchical);

  static const int NNode = Quad::NNode;

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( NNode, xElem.nDOF() );

  const BFieldClass* basis = BFieldClass::self();
  Element<Real,TopoD2,Quad> qfldElem(basis);

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( NNode, basis->nBasis() );

  const Real tol = 1e-13;
  typename ElementXField<PhysD2,TopoD2,Quad>::RefCoordType sRef;
  typename ElementXField<PhysD2,TopoD2,Quad>::VectorX gradphi[4];
  Real phixTrue[4], phiyTrue[4];
  int k;

  xElem.DOF(0) = {0, 0};
  xElem.DOF(1) = {1, 0};
  xElem.DOF(2) = {1, 1};
  xElem.DOF(3) = {0, 1};


  sRef[0] = 0;  sRef[1] = 0;

  phixTrue[0] = -1;  phixTrue[1] = 1;  phixTrue[2] = 0;  phixTrue[3] = 0;
  phiyTrue[0] = -1;  phiyTrue[1] = 0;  phiyTrue[2] = 0;  phiyTrue[3] = 1;

  xElem.evalBasisGradient( sRef, qfldElem, gradphi, 4 );

  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phiyTrue[k], gradphi[k][1], tol );
  }


  sRef[0] = 1;  sRef[1] = 0;

  phixTrue[0] = -1;  phixTrue[1] =  1;  phixTrue[2] = 0;  phixTrue[3] = 0;
  phiyTrue[0] =  0;  phiyTrue[1] = -1;  phiyTrue[2] = 1;  phiyTrue[3] = 0;

  xElem.evalBasisGradient( sRef, qfldElem, gradphi, 4 );

  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phiyTrue[k], gradphi[k][1], tol );
  }


  sRef[0] = 1;  sRef[1] = 1;

  phixTrue[0] =  0;  phixTrue[1] =  0;  phixTrue[2] = 1;  phixTrue[3] = -1;
  phiyTrue[0] =  0;  phiyTrue[1] = -1;  phiyTrue[2] = 1;  phiyTrue[3] =  0;

  xElem.evalBasisGradient( sRef, qfldElem, gradphi, 4 );

  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phiyTrue[k], gradphi[k][1], tol );
  }


  sRef[0] = 0;  sRef[1] = 1;

  phixTrue[0] =  0;  phixTrue[1] =  0;  phixTrue[2] = 1;  phixTrue[3] = -1;
  phiyTrue[0] = -1;  phiyTrue[1] =  0;  phiyTrue[2] = 0;  phiyTrue[3] =  1;

  xElem.evalBasisGradient( sRef, qfldElem, gradphi, 4 );

  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phiyTrue[k], gradphi[k][1], tol );
  }


  sRef[0] = 1./2.;  sRef[1] = 1./2.;

  phixTrue[0] = -1./2.;  phixTrue[1] =  1./2.;  phixTrue[2] = 1./2.;  phixTrue[3] = -1./2.;
  phiyTrue[0] = -1./2.;  phiyTrue[1] = -1./2.;  phiyTrue[2] = 1./2.;  phiyTrue[3] =  1./2.;

  xElem.evalBasisGradient( sRef, qfldElem, gradphi, 4 );

  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phiyTrue[k], gradphi[k][1], tol );
  }
}


//----------------------------------------------------------------------------//
// check that the trace element coordinates evaluate to the volume element coordinates on the trace
BOOST_AUTO_TEST_CASE( Trace_RefCoord_P1_test )
{
  typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldAreaClass;
  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldLineClass;
  typedef ElementXFieldAreaClass::VectorX VectorX;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  // element field variables
  ElementXFieldLineClass xfldElemFaceP( BasisFunctionLineBase::HierarchicalP1 );
  ElementXFieldLineClass xfldElemFaceN( BasisFunctionLineBase::HierarchicalP1 );
  ElementXFieldAreaClass xfldElem( BasisFunctionAreaBase<Quad>::HierarchicalP1 );

  xfldElem.DOF(0) = {0, 0};
  xfldElem.DOF(1) = {1, 0};
  xfldElem.DOF(2) = {1, 1};
  xfldElem.DOF(3) = {0, 1};

  const int quad[4] = {0, 1, 2, 3};

  const int (*TraceNodes)[ Line::NTrace ] = TraceToCellRefCoord<Line, TopoD2, Quad>::TraceNodes;

  const int *OrientPos = TraceToCellRefCoord<Line, TopoD2, Quad>::OrientPos;
  const int *OrientNeg = TraceToCellRefCoord<Line, TopoD2, Quad>::OrientNeg;


  for (int edge = 0; edge < Quad::NTrace; edge++)
  {
    // Get the re-oriented lines
    const int lineP[2] = {quad[TraceNodes[edge][OrientPos[0]]],
                          quad[TraceNodes[edge][OrientPos[1]]]};

    const int lineN[2] = {quad[TraceNodes[edge][OrientNeg[0]]],
                          quad[TraceNodes[edge][OrientNeg[1]]]};

    const CanonicalTraceToCell canonicalEdgeP = TraceToCellRefCoord<Line, TopoD2, Quad>::getCanonicalTrace(lineP, 2, quad, 4);
    const CanonicalTraceToCell canonicalEdgeN = TraceToCellRefCoord<Line, TopoD2, Quad>::getCanonicalTrace(lineN, 2, quad, 4);

    BOOST_REQUIRE_EQUAL(canonicalEdgeP.trace, edge);
    BOOST_REQUIRE_EQUAL(canonicalEdgeP.orientation, +1);

    BOOST_REQUIRE_EQUAL(canonicalEdgeN.trace, edge);
    BOOST_REQUIRE_EQUAL(canonicalEdgeN.orientation, -1);

    // Set the DOF's on the trace
    for (int n = 0; n < Line::NNode; n++)
    {
      xfldElemFaceP.DOF(n) = xfldElem.DOF(lineP[n]);
      xfldElemFaceN.DOF(n) = xfldElem.DOF(lineN[n]);
    }

    int kmax = 5;
    for (int k = 0; k < kmax; k++)
    {
      DLA::VectorS<1,Real> sRefTrace;
      DLA::VectorS<2,Real> sRefCell;
      VectorX xTrace, xCell;
      Real s;

      s = k/static_cast<Real>(kmax-1);

      sRefTrace = {s};

      // volume reference-element coords
      TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalEdgeP, sRefTrace, sRefCell );

      // element coordinates from trace and cell
      xfldElemFaceP.eval( sRefTrace, xTrace );
      xfldElem.eval( sRefCell, xCell );

      SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
      SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );

      // volume reference-element coords
      TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalEdgeN, sRefTrace, sRefCell );

      // element coordinates from trace and cell
      xfldElemFaceN.eval( sRefTrace, xTrace );
      xfldElem.eval( sRefCell, xCell );

      SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
      SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( impliedMetric_test )
{
  const Real small_tol = 1e-13;
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysD2, TopoD2, Quad> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 4, xElem.nDOF() );

  ElementXField<PhysD2, TopoD2, Quad>::RefCoordType sRef = {0.0, 0.0};

  DLA::MatrixSymS<PhysD2::D, Real> M;

  //Unit quad
  xElem.DOF(0) = {0.0, 0.0};
  xElem.DOF(1) = {1.0, 0.0};
  xElem.DOF(2) = {1.0, 1.0};
  xElem.DOF(3) = {0.0, 1.0};

  xElem.impliedMetric(sRef, M);

  SANS_CHECK_CLOSE( M(0,0), 1.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,1), 1.0, small_tol, tol );

  M = 0;
  xElem.impliedMetric(M); //evaluate at centerRef

  SANS_CHECK_CLOSE( M(0,0), 1.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,1), 1.0, small_tol, tol );

  //Unit quad - scaled by 2.5, translated by (0.6,1.3)
  Real scale = 2.5;
  Real xshift[2] = {0.6, 1.3};
  for (int i=0; i<4; i++)
    for (int d=0; d<2; d++)
      xElem.DOF(i)[d] = xElem.DOF(i)[d]*scale + xshift[d];

  xElem.impliedMetric(sRef, M);

  SANS_CHECK_CLOSE( M(0,0), 1.0/pow(scale,2.0), small_tol, tol );
  SANS_CHECK_CLOSE( M(0,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,1), 1.0/pow(scale,2.0), small_tol, tol );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
