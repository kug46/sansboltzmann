// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementAssociativityLine_btest
// testing of ElementAssociativityLine classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "Topology/ElementTopology.h"
#include "Field/Element/ElementAssociativityLine.h"
#include "BasisFunction/BasisFunctionLine.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementAssociativityLine_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctorsLine )
{
  int order;

  order = 1;
  ElementAssociativity<TopoD1,Line> assoc1(order);

  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( 2, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );

  order = 2;
  ElementAssociativity<TopoD1,Line> assoc2(order);

  BOOST_CHECK_EQUAL( 2, assoc2.order() );
  BOOST_CHECK_EQUAL( 2, assoc2.nNode() );
  BOOST_CHECK_EQUAL( 1, assoc2.nEdge() );

  ElementAssociativity<TopoD1,Line> assoc3( BasisFunctionLineBase::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, assoc3.order() );
  BOOST_CHECK_EQUAL( 2, assoc3.nNode() );
  BOOST_CHECK_EQUAL( 1, assoc3.nEdge() );

  ElementAssociativity<TopoD1,Line> assoc3b( BasisFunctionLineBase::LegendreP2 );

  BOOST_CHECK_EQUAL( 2, assoc3b.order() );
  BOOST_CHECK_EQUAL( 0, assoc3b.nNode() );
  BOOST_CHECK_EQUAL( 3, assoc3b.nEdge() );

  ElementAssociativity<TopoD1,Line> assoc4(assoc3);

  BOOST_CHECK_EQUAL( 2, assoc4.order() );
  BOOST_CHECK_EQUAL( 2, assoc4.nNode() );
  BOOST_CHECK_EQUAL( 1, assoc4.nEdge() );

  ElementAssociativity<TopoD1,Line> assoc4b(assoc3b);

  BOOST_CHECK_EQUAL( 2, assoc4b.order() );
  BOOST_CHECK_EQUAL( 0, assoc4b.nNode() );
  BOOST_CHECK_EQUAL( 3, assoc4b.nEdge() );

  // default ctor and operator=
  ElementAssociativity<TopoD1,Line> assoc5;
  assoc5 = assoc4;

  BOOST_CHECK_EQUAL( 2, assoc5.order() );
  BOOST_CHECK_EQUAL( 2, assoc5.nNode() );
  BOOST_CHECK_EQUAL( 1, assoc5.nEdge() );

  ElementAssociativity<TopoD1,Line> assoc5b;
  assoc5b = assoc4b;

  BOOST_CHECK_EQUAL( 2, assoc5b.order() );
  BOOST_CHECK_EQUAL( 0, assoc5b.nNode() );
  BOOST_CHECK_EQUAL( 3, assoc5b.nEdge() );

  //Check if association order is too low or too high
  typedef ElementAssociativity<TopoD1,Line> ElementAssociativityLine;
  BOOST_CHECK_THROW( ElementAssociativityLine assoc6(0), DeveloperException );
  BOOST_CHECK_THROW( ElementAssociativityLine assoc7(5), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctorsLineConstructor )
{
  int order;
  typedef ElementAssociativityConstructor<TopoD1,Line> Constructor;
  order = 1;
  Constructor assoc1(order);

  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( 2, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );

  order = 2;
  Constructor assoc2(order);

  BOOST_CHECK_EQUAL( 2, assoc2.order() );
  BOOST_CHECK_EQUAL( 2, assoc2.nNode() );
  BOOST_CHECK_EQUAL( 1, assoc2.nEdge() );

  Constructor assoc3( BasisFunctionLineBase::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, assoc3.order() );
  BOOST_CHECK_EQUAL( 2, assoc3.nNode() );
  BOOST_CHECK_EQUAL( 1, assoc3.nEdge() );

  Constructor assoc3b( BasisFunctionLineBase::LegendreP2 );

  BOOST_CHECK_EQUAL( 2, assoc3b.order() );
  BOOST_CHECK_EQUAL( 0, assoc3b.nNode() );
  BOOST_CHECK_EQUAL( 3, assoc3b.nEdge() );

  // default ctor and operator=
  Constructor assoc5;
  assoc5 = assoc3;

  BOOST_CHECK_EQUAL( 2, assoc5.order() );
  BOOST_CHECK_EQUAL( 2, assoc5.nNode() );
  BOOST_CHECK_EQUAL( 1, assoc5.nEdge() );

  Constructor assoc5b;
  assoc5b = assoc3b;

  BOOST_CHECK_EQUAL( 2, assoc5b.order() );
  BOOST_CHECK_EQUAL( 0, assoc5b.nNode() );
  BOOST_CHECK_EQUAL( 3, assoc5b.nEdge() );

  //Check if association order is too low or too high
  BOOST_CHECK_THROW( Constructor assoc6(0), DeveloperException );
  BOOST_CHECK_THROW( Constructor assoc7(5), DeveloperException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fcnsLine )
{
  int order = 1;
  typedef ElementAssociativityConstructor<TopoD1,Line> Constructor;
  Constructor assoc1ctor(order);

  BOOST_CHECK_EQUAL( 1, assoc1ctor.order() );
  BOOST_CHECK_EQUAL( 2, assoc1ctor.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1ctor.nEdge() );

  int nodeTrue[2] = {3, 4};
  int node[2];

  assoc1ctor.setRank( 2 );
  BOOST_CHECK_EQUAL( 2, assoc1ctor.rank() );

  assoc1ctor.setNodeGlobalMapping( nodeTrue, 2 );
  node[0] = -1;
  node[1] = -1;
  assoc1ctor.getNodeGlobalMapping( node, 2 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );

  assoc1ctor.setNodeGlobalMapping( {nodeTrue[0], nodeTrue[1]} );
  node[0] = -1;
  node[1] = -1;
  assoc1ctor.getNodeGlobalMapping( node, 2 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );

  int node0, node1;

  node0 = assoc1ctor.nodeGlobal(0);
  node1 = assoc1ctor.nodeGlobal(1);
  BOOST_CHECK_EQUAL( nodeTrue[0], node0 );
  BOOST_CHECK_EQUAL( nodeTrue[1], node1 );

  assoc1ctor.getGlobalMapping( node, 2 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );


  ElementAssociativity<TopoD1,Line> assoc1(assoc1ctor);
  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( 2, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );

  BOOST_CHECK_EQUAL( 2, assoc1.rank() );

  node[0] = -1;
  node[1] = -1;
  assoc1.getNodeGlobalMapping( node, 2 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );

  node[0] = -1;
  node[1] = -1;
  assoc1.getGlobalMapping( node, 2 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );


  order = 2;
  Constructor assoc2ctor(order);

  BOOST_CHECK_EQUAL( 2, assoc2ctor.order() );
  BOOST_CHECK_EQUAL( 2, assoc2ctor.nNode() );
  BOOST_CHECK_EQUAL( 1, assoc2ctor.nEdge() );

  assoc2ctor.setRank( 2 );

  node[0] = nodeTrue[0];
  node[1] = nodeTrue[1];
  assoc2ctor.setNodeGlobalMapping( node, 2 );
  assoc2ctor.getNodeGlobalMapping( node, 2 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );

  int edgeTrue[1] = {9};
  int edge[1];

  assoc2ctor.setEdgeGlobalMapping( edgeTrue, 1 );
  assoc2ctor.getEdgeGlobalMapping( edge, 1 );

  BOOST_CHECK_EQUAL( edgeTrue[0], edge[0] );

  edge[0] = -1;
  assoc2ctor.setEdgeGlobalMapping( {edgeTrue[0]} );
  assoc2ctor.getEdgeGlobalMapping( edge, 1 );

  BOOST_CHECK_EQUAL( edgeTrue[0], edge[0] );

  node0 = assoc2ctor.nodeGlobal(0);
  node1 = assoc2ctor.nodeGlobal(1);
  BOOST_CHECK_EQUAL( nodeTrue[0], node0 );
  BOOST_CHECK_EQUAL( nodeTrue[1], node1 );

  edge[0] = assoc2ctor.edgeGlobal(0);
  BOOST_CHECK_EQUAL( edgeTrue[0], edge[0] );

  int map[3];
  assoc2ctor.getGlobalMapping( map, 3 );

  BOOST_CHECK_EQUAL( nodeTrue[0], map[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], map[1] );
  BOOST_CHECK_EQUAL( edgeTrue[0], map[2] );


  ElementAssociativity<TopoD1,Line> assoc2(order);
  assoc2 = assoc2ctor;

  BOOST_CHECK_EQUAL( 2, assoc2.order() );
  BOOST_CHECK_EQUAL( 2, assoc2.nNode() );
  BOOST_CHECK_EQUAL( 1, assoc2.nEdge() );

  BOOST_CHECK_EQUAL( 2, assoc2.rank() );

  node[0] = -1;
  node[1] = -1;
  assoc2.getNodeGlobalMapping( node, 2 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );

  edge[0] = -1;
  assoc2.getEdgeGlobalMapping( edge, 1 );

  BOOST_CHECK_EQUAL( edgeTrue[0], edge[0] );

  map[0] = map[1] = map[2] = -1;
  assoc2.getGlobalMapping( map, 3 );

  BOOST_CHECK_EQUAL( nodeTrue[0], map[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], map[1] );
  BOOST_CHECK_EQUAL( edgeTrue[0], map[2] );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/ElementAssociativityLine_pattern.txt", true );

  //Line dump
  typedef ElementAssociativityConstructor<TopoD1,Line> Constructor;
  Constructor assocEdgector(2);

  assocEdgector.setRank( 2 );
  assocEdgector.setNodeGlobalMapping( {3, 4} );
  assocEdgector.setEdgeGlobalMapping( {5} );

  ElementAssociativity<TopoD1,Line> assocEdge(assocEdgector);

  assocEdgector.dump( 2, output );
  assocEdge.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
