// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementProjection_L2_Line_AD_btest
// testing of 2-D element L2 projection operator with Advection-Diffusion

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "Field/Element/ElementProjection_L2.h"
#include "Field/Element/ElementNode.h"
#include "Field/Element/ElementLine.h"
#include "Field/Element/ElementXFieldLine.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementProjection_L2_Line_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementProjection_Const_L2_Legendre )
{
  typedef Real ArrayQ;
  typedef Element<ArrayQ, TopoD1, Line> ElementQFieldClass;

  int order = 0;
  ElementQFieldClass qfld0(order, BasisFunctionCategory_Legendre);

  ElementProjectionConst_L2<TopoD1, Line> projector0(qfld0.basis());

  BOOST_CHECK_EQUAL( 0, qfld0.order() );
  BOOST_CHECK_EQUAL( 1, qfld0.nDOF() );

  ArrayQ aconst = 2;

  projector0.project( qfld0, aconst );

  Real tol = 1e-13;
  BOOST_CHECK_CLOSE( aconst, qfld0.eval({0.5}), tol );
  BOOST_CHECK_CLOSE( aconst, qfld0.DOF(0), tol );

  order = 1;
  ElementQFieldClass qfld1(order, BasisFunctionCategory_Legendre);

  ElementProjectionConst_L2<TopoD1, Line> projector1(qfld1.basis());

  BOOST_CHECK_EQUAL( 1, qfld1.order() );
  BOOST_CHECK_EQUAL( 2, qfld1.nDOF() );

  projector1.project( qfld1, aconst );

  BOOST_CHECK_CLOSE( aconst, qfld1.eval({0.5}), tol );
  BOOST_CHECK_CLOSE( aconst, qfld1.DOF(0), tol );
  BOOST_CHECK_SMALL(         qfld1.DOF(1), tol );

  order = 2;
  ElementQFieldClass qfld2(order, BasisFunctionCategory_Legendre);

  ElementProjectionConst_L2<TopoD1, Line> projector2(qfld2.basis());

  BOOST_CHECK_EQUAL( 2, qfld2.order() );
  BOOST_CHECK_EQUAL( 3, qfld2.nDOF() );

  projector2.project( qfld2, aconst );

  BOOST_CHECK_CLOSE( aconst, qfld2.eval({0.5}), tol );
  BOOST_CHECK_CLOSE( aconst, qfld2.DOF(0), tol );
  BOOST_CHECK_SMALL(         qfld2.DOF(1), tol );
  BOOST_CHECK_SMALL(         qfld2.DOF(2), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementProjection_Const_L2_Hierarchical )
{
  typedef Real ArrayQ;
  typedef Element<ArrayQ, TopoD1, Line> ElementQFieldClass;

  ArrayQ aconst = 2;

  int order = 1;
  ElementQFieldClass qfld1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfld1.order() );
  BOOST_CHECK_EQUAL( 2, qfld1.nDOF() );

  ElementProjectionConst_L2<TopoD1, Line> projector1(qfld1.basis());

  projector1.project( qfld1, aconst );

  Real tol = 1e-12;
  BOOST_CHECK_CLOSE( aconst, qfld1.eval({0.5}), tol );
  BOOST_CHECK_CLOSE( aconst, qfld1.DOF(0), tol );
  BOOST_CHECK_CLOSE( aconst, qfld1.DOF(1), tol );

  order = 2;
  ElementQFieldClass qfld2(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qfld2.order() );
  BOOST_CHECK_EQUAL( 3, qfld2.nDOF() );

  ElementProjectionConst_L2<TopoD1, Line> projector2(qfld2.basis());

  projector2.project( qfld2, aconst );

  BOOST_CHECK_CLOSE( aconst, qfld2.eval({0.5}), tol );
  BOOST_CHECK_CLOSE( aconst, qfld2.DOF(0), tol );
  BOOST_CHECK_CLOSE( aconst, qfld2.DOF(1), tol );
  BOOST_CHECK_SMALL(         qfld2.DOF(2), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementProjection_L2_LegendreP0 )
{
  typedef ScalarFunction2D_Const::ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2, TopoD1, Line> ElementXFieldClass;
  typedef Element<ArrayQ, TopoD1, Line>       ElementQFieldClass;

  typedef ScalarFunction2D_Const    ScalarFunctionConst;
  typedef ScalarFunction2D_Linear   ScalarFunctionLinear;
  typedef ScalarFunction2D_SineSine ScalarFunctionSine;

  // grid

  int order = 1;
  ElementXFieldClass xfld(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfld.order() );
  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );

  Real x1 = 0;  Real y1 =  0;
  Real x2 = 1;  Real y2 = -2;

  xfld.DOF(0) = { x1, y1 };
  xfld.DOF(1) = { x2, y2 };

  // solution

  order = 0;
  ElementQFieldClass qfld(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, qfld.order() );
  BOOST_CHECK_EQUAL( 1, qfld.nDOF() );

  int aconst = 1;

  // exact solution: const = 1
  ScalarFunctionConst solnExact0( aconst );
  SolnNDConvertSpace<PhysD2, ScalarFunctionConst> solnExact0PlusTime(solnExact0);

  ElementProjectionSolution_L2<TopoD1, Line> projector(qfld.basis());

  projector.project( xfld, qfld, solnExact0PlusTime );

  Real tol = 1e-13;
  BOOST_CHECK_CLOSE( 1, qfld.DOF(0), tol );

  // exact solution: linear = a0 + ax*x + ay*y
  Real a0 =  1;
  Real ax = -3;
  Real ay =  4;
  ScalarFunctionLinear solnExact1( a0, ax, ay );
  SolnNDConvertSpace<PhysD2, ScalarFunctionLinear> solnExact1PlusTime(solnExact1);

  projector.project( xfld, qfld, solnExact1PlusTime );

  tol = 1e-12;
  BOOST_CHECK_CLOSE( a0 + 0.5*ax - ay, qfld.DOF(0), tol );

  // exact solution: sin(2 PI x)*sin(2 PI y)
  ScalarFunctionSine solnExact2;
  SolnNDConvertSpace<PhysD2, ScalarFunctionSine> solnExact2PlusTime(solnExact2);

  projector.project( xfld, qfld, solnExact2PlusTime );

  tol = 1e-14;
  BOOST_CHECK_SMALL( qfld.DOF(0), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementProjection_L2_HierarchicalP1_test )
{
  typedef ScalarFunction2D_Const::ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2, TopoD1, Line> ElementXFieldClass;
  typedef Element<ArrayQ, TopoD1, Line>       ElementQFieldClass;

  typedef ScalarFunction2D_Const    ScalarFunctionConst;
  typedef ScalarFunction2D_Linear   ScalarFunctionLinear;
  typedef ScalarFunction2D_SineSine ScalarFunctionSine;

  // grid

  int order = 1;
  ElementXFieldClass xfld(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfld.order() );
  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );

  xfld.DOF(0) = { 0,  0 };
  xfld.DOF(1) = { 1, -2 };

  // solution

  order = 1;
  ElementQFieldClass qfld(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfld.order() );
  BOOST_CHECK_EQUAL( 2, qfld.nDOF() );

  ElementProjectionSolution_L2<TopoD1, Line> projector(qfld.basis());

  // exact solution: const = 1
  int aconst = 1;
  SolnNDConvertSpace<PhysD2, ScalarFunctionConst> solnExact0(aconst);

  projector.project( xfld, qfld, solnExact0 );

  Real tol = 1e-13;
  BOOST_CHECK_CLOSE( 1, qfld.DOF(0), tol );
  BOOST_CHECK_CLOSE( 1, qfld.DOF(1), tol );

  // exact solution: linear = a0 + ax*x + ay*y
  Real a0 =  1;
  Real ax = -3;
  Real ay =  4;
  SolnNDConvertSpace<PhysD2, ScalarFunctionLinear> solnExact1(a0, ax, ay);

  projector.project( xfld, qfld, solnExact1 );

  tol = 1e-12;
  BOOST_CHECK_CLOSE( a0,             qfld.DOF(0), tol );
  BOOST_CHECK_CLOSE( a0 + ax - 2*ay, qfld.DOF(1), tol );

  // exact solution: sin(2 PI x)*sin(2 PI y)
  SolnNDConvertSpace<PhysD2, ScalarFunctionSine> solnExact2;

  projector.project( xfld, qfld, solnExact2 );

  tol = 1e-15;
  BOOST_CHECK_SMALL( qfld.DOF(0), tol );
  BOOST_CHECK_SMALL( qfld.DOF(1), tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementProjection_L2_LagrangeBC_LegendreP0 )
{
  typedef ScalarFunction2D_Const::ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2, TopoD1, Line> ElementXFieldClass;
  typedef Element<ArrayQ, TopoD1, Line>       ElementQFieldClass;

  typedef ScalarFunction2D_Const    ScalarFunctionConst;
  typedef ScalarFunction2D_Linear   ScalarFunctionLinear;
  typedef ScalarFunction2D_SineSine ScalarFunctionSine;

  // grid

  int order = 1;
  ElementXFieldClass xfld(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfld.order() );
  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );

  Real x1 = 0;  Real y1 =  0;
  Real x2 = 1;  Real y2 = -2;

  xfld.DOF(0) = { x1, y1 };
  xfld.DOF(1) = { x2, y2 };

  Real nx =   y2 - y1;
  Real ny = -(x2 - x1);
  Real ds = sqrt(nx*nx + ny*ny);
  nx = nx / ds;
  ny = ny / ds;

  // Lagrange multiplier

  order = 0;
  ElementQFieldClass lgfld(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, lgfld.order() );
  BOOST_CHECK_EQUAL( 1, lgfld.nDOF() );

  // exact solution: const = 1
  int aconst = 1;
  SolnNDConvertSpace<PhysD2, ScalarFunctionConst> solnExact0(aconst);

  ElementProjection_NormalGradient_L2( xfld, lgfld, solnExact0 );

  Real tol = 1e-13;
  BOOST_CHECK_SMALL( lgfld.DOF(0), tol );

  // exact solution: linear = a0 + ax*x + ay*y
  Real a0 =  1;
  Real ax = -3;
  Real ay =  4;
  SolnNDConvertSpace<PhysD2, ScalarFunctionLinear> solnExact1(a0, ax, ay);

  ElementProjection_NormalGradient_L2( xfld, lgfld, solnExact1 );

  tol = 1e-12;
  BOOST_CHECK_CLOSE( nx*ax + ny*ay, lgfld.DOF(0), tol );

  // exact solution: sin(2 PI x)*sin(2 PI y)
  SolnNDConvertSpace<PhysD2, ScalarFunctionSine> solnExact2;

  ElementProjection_NormalGradient_L2( xfld, lgfld, solnExact2 );

  tol = 1e-14;
  BOOST_CHECK_SMALL( lgfld.DOF(0), tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Element_Subdivision_Projector_P1 )
{
  typedef ElementXField<PhysD1, TopoD1, Line> ElementXFieldClass_1D;
  typedef ElementXField<PhysD2, TopoD1, Line> ElementXFieldClass_2D;
  typedef ElementXField<PhysD3, TopoD1, Line> ElementXFieldClass_3D;

  Real tol = 1e-12;

  int order = 1;
  int split_edge_index = -1;

  //--------------1D lines-----------------

  ElementXFieldClass_1D xfldElem1D_main(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldClass_1D xfldElem1D_sub(order, BasisFunctionCategory_Hierarchical);

  Element_Subdivision_Projector<TopoD1, Line> projector(xfldElem1D_sub.basis());

  BOOST_CHECK_EQUAL( 1, xfldElem1D_main.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem1D_main.nDOF() );

  xfldElem1D_main.DOF(0) = 0.25;
  xfldElem1D_main.DOF(1) = 0.75;

  projector.project( xfldElem1D_main, xfldElem1D_sub, ElementSplitType::Isotropic, split_edge_index, 0 );
  BOOST_CHECK_CLOSE( xfldElem1D_sub.DOF(0)[0], 0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_sub.DOF(1)[0], 0.50, tol );

  projector.project( xfldElem1D_main, xfldElem1D_sub, ElementSplitType::Isotropic, split_edge_index, 1 );
  BOOST_CHECK_CLOSE( xfldElem1D_sub.DOF(0)[0], 0.50, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_sub.DOF(1)[0], 0.75, tol );

  //--------------2D lines-----------------

  ElementXFieldClass_2D xfldElem2D_main(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldClass_2D xfldElem2D_sub(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem2D_main.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem2D_main.nDOF() );

  xfldElem2D_main.DOF(0) = {0.25,-0.40};
  xfldElem2D_main.DOF(1) = {0.75,-0.80};

  projector.project( xfldElem2D_main, xfldElem2D_sub, ElementSplitType::Isotropic, split_edge_index, 0 );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[0], 0.25, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[0], 0.50, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[1], -0.60, tol );

  projector.project( xfldElem2D_main, xfldElem2D_sub, ElementSplitType::Isotropic, split_edge_index, 1 );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[0], 0.50, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[1], -0.60, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[0], 0.75, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[1], -0.80, tol );

  //--------------3D lines-----------------

  ElementXFieldClass_3D xfldElem3D_main(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldClass_3D xfldElem3D_sub(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem3D_main.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem3D_main.nDOF() );

  xfldElem3D_main.DOF(0) = {0.25,-0.40, 2.0};
  xfldElem3D_main.DOF(1) = {0.75,-0.80,-1.0};

  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Isotropic, split_edge_index, 0 );
  BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[0],  0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[2],  2.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[0],  0.50, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[1], -0.60, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[2],  0.50, tol );

  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Isotropic, split_edge_index, 1 );
  BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[0],  0.50, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[1], -0.60, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[2],  0.50, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[0],  0.75, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[2], -1.00, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Element_Subdivision_Projector_P2 )
{
  typedef ElementXField<PhysD1, TopoD1, Line> ElementXFieldClass_1D;
  typedef ElementXField<PhysD2, TopoD1, Line> ElementXFieldClass_2D;
  typedef ElementXField<PhysD3, TopoD1, Line> ElementXFieldClass_3D;

  Real tol = 1e-12;

  int order = 2;
  int split_edge_index = -1;

  //--------------1D lines-----------------

  ElementXFieldClass_1D xfldElem1D_main(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldClass_1D xfldElem1D_sub(order, BasisFunctionCategory_Hierarchical);

  Element_Subdivision_Projector<TopoD1, Line> projector(xfldElem1D_sub.basis());

  BOOST_CHECK_EQUAL( 2, xfldElem1D_main.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem1D_main.nDOF() );

  xfldElem1D_main.DOF(0) = 0.25;
  xfldElem1D_main.DOF(1) = 0.75;
  xfldElem1D_main.DOF(2) = 0.00;

  projector.project( xfldElem1D_main, xfldElem1D_sub, ElementSplitType::Isotropic, split_edge_index, 0 );
  SANS_CHECK_CLOSE( xfldElem1D_sub.DOF(0)[0], 0.25, tol, tol );
  SANS_CHECK_CLOSE( xfldElem1D_sub.DOF(1)[0], 0.50, tol, tol );
  SANS_CHECK_CLOSE( xfldElem1D_sub.DOF(2)[0], 0.00, tol, tol );

  projector.project( xfldElem1D_main, xfldElem1D_sub, ElementSplitType::Isotropic, split_edge_index, 1 );
  SANS_CHECK_CLOSE( xfldElem1D_sub.DOF(0)[0], 0.50, tol, tol );
  SANS_CHECK_CLOSE( xfldElem1D_sub.DOF(1)[0], 0.75, tol, tol );
  SANS_CHECK_CLOSE( xfldElem1D_sub.DOF(2)[0], 0.00, tol, tol );

  //--------------2D lines-----------------

  ElementXFieldClass_2D xfldElem2D_main(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldClass_2D xfldElem2D_sub(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, xfldElem2D_main.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem2D_main.nDOF() );

  xfldElem2D_main.DOF(0) = {0.25,-0.40};
  xfldElem2D_main.DOF(1) = {0.75,-0.80};
  xfldElem2D_main.DOF(2) = {0.00, 0.00};

  projector.project( xfldElem2D_main, xfldElem2D_sub, ElementSplitType::Isotropic, split_edge_index, 0 );
  SANS_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[0], 0.25, tol, tol ); SANS_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[1], -0.40, tol, tol );
  SANS_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[0], 0.50, tol, tol ); SANS_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[1], -0.60, tol, tol );
  SANS_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[0], 0.00, tol, tol ); SANS_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[1],  0.00, tol, tol );

  projector.project( xfldElem2D_main, xfldElem2D_sub, ElementSplitType::Isotropic, split_edge_index, 1 );
  SANS_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[0], 0.50, tol, tol ); SANS_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[1], -0.60, tol, tol );
  SANS_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[0], 0.75, tol, tol ); SANS_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[1], -0.80, tol, tol );
  SANS_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[0], 0.00, tol, tol ); SANS_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[1],  0.00, tol, tol );

  //--------------3D lines-----------------

  ElementXFieldClass_3D xfldElem3D_main(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldClass_3D xfldElem3D_sub(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, xfldElem3D_main.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem3D_main.nDOF() );

  xfldElem3D_main.DOF(0) = {0.25,-0.40, 2.0};
  xfldElem3D_main.DOF(1) = {0.75,-0.80,-1.0};
  xfldElem3D_main.DOF(2) = {0.00, 0.00, 0.0};

  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Isotropic, split_edge_index, 0 );
  SANS_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[0],  0.25, tol, tol );
  SANS_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[1], -0.40, tol, tol );
  SANS_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[2],  2.00, tol, tol );

  SANS_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[0],  0.50, tol, tol );
  SANS_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[1], -0.60, tol, tol );
  SANS_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[2],  0.50, tol, tol );

  SANS_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[0],  0.00, tol, tol );
  SANS_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[1],  0.00, tol, tol );
  SANS_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[2],  0.00, tol, tol );

  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Isotropic, split_edge_index, 1 );
  SANS_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[0],  0.50, tol, tol );
  SANS_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[1], -0.60, tol, tol );
  SANS_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[2],  0.50, tol, tol );

  SANS_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[0],  0.75, tol, tol );
  SANS_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[1], -0.80, tol, tol );
  SANS_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[2], -1.00, tol, tol );

  SANS_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[0],  0.00, tol, tol );
  SANS_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[1],  0.00, tol, tol );
  SANS_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[2],  0.00, tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Element_Subdivision_CellToTrace_Projector_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Element<ArrayQ, TopoD0, Node> ElementClassTrace;
  typedef Element<ArrayQ, TopoD1, Line> ElementClassCell;

  Real tol = 1e-12;

  int order = 1;
  int split_edge_index = -1;

  ElementClassTrace fldElem_trace(0, BasisFunctionCategory_Legendre);
  ElementClassCell fldElem_cell(order, BasisFunctionCategory_Hierarchical);

  Element_Subdivision_CellToTrace_Projector<TopoD0, Node> projector(fldElem_trace.basis());

  BOOST_CHECK_EQUAL( 0, fldElem_trace.order() );
  BOOST_CHECK_EQUAL( 1, fldElem_trace.nDOF() );

  BOOST_CHECK_EQUAL( 1, fldElem_cell.order() );
  BOOST_CHECK_EQUAL( 2, fldElem_cell.nDOF() );

  fldElem_cell.DOF(0) = {0.25,-0.40};
  fldElem_cell.DOF(1) = {0.75,-0.80};

  projector.project( fldElem_cell, fldElem_trace, ElementSplitType::Isotropic, split_edge_index, 0 );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(0)[0], 0.50, tol );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(0)[1], -0.60, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Element_Subdivision_CellToTrace_Projector_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Element<ArrayQ, TopoD0, Node> ElementClassTrace;
  typedef Element<ArrayQ, TopoD1, Line> ElementClassCell;

  Real tol = 1e-12;

  int order = 2;
  int split_edge_index = -1;

  ElementClassTrace fldElem_trace(0, BasisFunctionCategory_Legendre);
  ElementClassCell fldElem_cell(order, BasisFunctionCategory_Hierarchical);

  Element_Subdivision_CellToTrace_Projector<TopoD0, Node> projector(fldElem_trace.basis());

  BOOST_CHECK_EQUAL( 0, fldElem_trace.order() );
  BOOST_CHECK_EQUAL( 1, fldElem_trace.nDOF() );

  BOOST_CHECK_EQUAL( 2, fldElem_cell.order() );
  BOOST_CHECK_EQUAL( 3, fldElem_cell.nDOF() );

  fldElem_cell.DOF(0) = {0.25,-0.40};
  fldElem_cell.DOF(1) = {0.75,-0.80};
  fldElem_cell.DOF(2) = {0.40, 0.50};

  projector.project( fldElem_cell, fldElem_trace, ElementSplitType::Isotropic, split_edge_index, 0 );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(0)[0],  0.9, tol );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(0)[1], -0.1, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Element_Trace_Projection_L2_Hierarchical_P1 )
{
  typedef ElementXField<PhysD1, TopoD1, Line> ElementXFieldClass_1D;
  typedef ElementXField<PhysD2, TopoD1, Line> ElementXFieldClass_2D;
  typedef ElementXField<PhysD3, TopoD1, Line> ElementXFieldClass_3D;

  Real tol = 1e-12;

  int order = 1;

  //--------------1D lines-----------------

  ElementXFieldClass_1D xfldElem1D_0(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldClass_1D xfldElem1D_1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem1D_0.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem1D_0.nDOF() );

  xfldElem1D_0.DOF(0) = 0.25;
  xfldElem1D_0.DOF(1) = 0.60;

  Element_Trace_Projection_L2( xfldElem1D_0, 1, xfldElem1D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(0)[0], 0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(1)[0], 0.60, tol );

  Element_Trace_Projection_L2( xfldElem1D_0,-1, xfldElem1D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(0)[0], 0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(1)[0], 0.60, tol );

  Element_Trace_Projection_L2( xfldElem1D_0, 1, xfldElem1D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(0)[0], 0.60, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(1)[0], 0.25, tol );

  Element_Trace_Projection_L2( xfldElem1D_0,-1, xfldElem1D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(0)[0], 0.60, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(1)[0], 0.25, tol );

  BOOST_CHECK_THROW( Element_Trace_Projection_L2( xfldElem1D_0, 0, xfldElem1D_1, 0 ), DeveloperException );


  //--------------2D lines-----------------

  ElementXFieldClass_2D xfldElem2D_0(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldClass_2D xfldElem2D_1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem2D_0.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem2D_0.nDOF() );

  xfldElem2D_0.DOF(0) = {0.25,-0.40};
  xfldElem2D_0.DOF(1) = {0.75,-0.80};

  Element_Trace_Projection_L2( xfldElem2D_0, 1, xfldElem2D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[0], 0.25, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[0], 0.75, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[1], -0.80, tol );

  Element_Trace_Projection_L2( xfldElem2D_0,-1, xfldElem2D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[0], 0.25, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[0], 0.75, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[1], -0.80, tol );

  Element_Trace_Projection_L2( xfldElem2D_0, 1, xfldElem2D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[0], 0.75, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[0], 0.25, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[1], -0.40, tol );

  Element_Trace_Projection_L2( xfldElem2D_0,-1, xfldElem2D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[0], 0.75, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[0], 0.25, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[1], -0.40, tol );

  BOOST_CHECK_THROW( Element_Trace_Projection_L2( xfldElem2D_0, 0, xfldElem2D_1, 0 ), DeveloperException );


  //--------------3D lines-----------------

  ElementXFieldClass_3D xfldElem3D_0(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldClass_3D xfldElem3D_1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem3D_0.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem3D_0.nDOF() );

  xfldElem3D_0.DOF(0) = {0.25,-0.40, 2.0};
  xfldElem3D_0.DOF(1) = {0.75,-0.80,-1.0};

  Element_Trace_Projection_L2( xfldElem3D_0, 1, xfldElem3D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[0],  0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[2],  2.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[0],  0.75, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[2], -1.00, tol );

  Element_Trace_Projection_L2( xfldElem3D_0,-1, xfldElem3D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[0],  0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[2],  2.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[0],  0.75, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[2], -1.00, tol );

  Element_Trace_Projection_L2( xfldElem3D_0, 1, xfldElem3D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[0],  0.75, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[2], -1.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[0],  0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[2],  2.00, tol );

  Element_Trace_Projection_L2( xfldElem3D_0,-1, xfldElem3D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[0],  0.75, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[2], -1.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[0],  0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[2],  2.00, tol );

  BOOST_CHECK_THROW( Element_Trace_Projection_L2( xfldElem3D_0, 0, xfldElem3D_1, 0 ), DeveloperException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Element_Trace_Projection_L2_Legendre_P1 )
{
  typedef ElementXField<PhysD1, TopoD1, Line> ElementXFieldClass_1D;
  typedef ElementXField<PhysD2, TopoD1, Line> ElementXFieldClass_2D;
  typedef ElementXField<PhysD3, TopoD1, Line> ElementXFieldClass_3D;

  Real tol = 1e-12;

  int order = 1;

  //--------------1D lines-----------------

  ElementXFieldClass_1D xfldElem1D_0(order, BasisFunctionCategory_Legendre);
  ElementXFieldClass_1D xfldElem1D_1(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 1, xfldElem1D_0.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem1D_0.nDOF() );

  xfldElem1D_0.DOF(0) = 0.25;
  xfldElem1D_0.DOF(1) = 0.60;

  Element_Trace_Projection_L2( xfldElem1D_0, 1, xfldElem1D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(0)[0], 0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(1)[0], 0.60, tol );

  Element_Trace_Projection_L2( xfldElem1D_0,-1, xfldElem1D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(0)[0], 0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(1)[0], 0.60, tol );

  Element_Trace_Projection_L2( xfldElem1D_0, 1, xfldElem1D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(0)[0], 0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(1)[0],-0.60, tol );

  Element_Trace_Projection_L2( xfldElem1D_0,-1, xfldElem1D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(0)[0], 0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(1)[0],-0.60, tol );

  BOOST_CHECK_THROW( Element_Trace_Projection_L2( xfldElem1D_0, 0, xfldElem1D_1, 0 ), DeveloperException );


  //--------------2D lines-----------------

  ElementXFieldClass_2D xfldElem2D_0(order, BasisFunctionCategory_Legendre);
  ElementXFieldClass_2D xfldElem2D_1(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 1, xfldElem2D_0.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem2D_0.nDOF() );

  xfldElem2D_0.DOF(0) = {0.25,-0.40};
  xfldElem2D_0.DOF(1) = {0.75,-0.80};

  Element_Trace_Projection_L2( xfldElem2D_0, 1, xfldElem2D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[0], 0.25, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[0], 0.75, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[1], -0.80, tol );

  Element_Trace_Projection_L2( xfldElem2D_0,-1, xfldElem2D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[0], 0.25, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[0], 0.75, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[1], -0.80, tol );

  Element_Trace_Projection_L2( xfldElem2D_0, 1, xfldElem2D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[0], 0.25, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[0],-0.75, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[1],  0.80, tol );

  Element_Trace_Projection_L2( xfldElem2D_0,-1, xfldElem2D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[0], 0.25, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[0],-0.75, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[1],  0.80, tol );

  BOOST_CHECK_THROW( Element_Trace_Projection_L2( xfldElem2D_0, 0, xfldElem2D_1, 0 ), DeveloperException );


  //--------------3D lines-----------------

  ElementXFieldClass_3D xfldElem3D_0(order, BasisFunctionCategory_Legendre);
  ElementXFieldClass_3D xfldElem3D_1(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 1, xfldElem3D_0.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem3D_0.nDOF() );

  xfldElem3D_0.DOF(0) = {0.25,-0.40, 2.0};
  xfldElem3D_0.DOF(1) = {0.75,-0.80,-1.0};

  Element_Trace_Projection_L2( xfldElem3D_0, 1, xfldElem3D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[0],  0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[2],  2.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[0],  0.75, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[2], -1.00, tol );

  Element_Trace_Projection_L2( xfldElem3D_0,-1, xfldElem3D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[0],  0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[2],  2.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[0],  0.75, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[2], -1.00, tol );

  Element_Trace_Projection_L2( xfldElem3D_0, 1, xfldElem3D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[0],  0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[2],  2.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[0], -0.75, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[1],  0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[2],  1.00, tol );

  Element_Trace_Projection_L2( xfldElem3D_0,-1, xfldElem3D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[0],  0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[2],  2.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[0], -0.75, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[1],  0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[2],  1.00, tol );

  BOOST_CHECK_THROW( Element_Trace_Projection_L2( xfldElem3D_0, 0, xfldElem3D_1, 0 ), DeveloperException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Element_Trace_Projection_L2_Hierarchical_P2 )
{
  typedef ElementXField<PhysD1, TopoD1, Line> ElementXFieldClass_1D;
  typedef ElementXField<PhysD2, TopoD1, Line> ElementXFieldClass_2D;
  typedef ElementXField<PhysD3, TopoD1, Line> ElementXFieldClass_3D;

  Real tol = 1e-12;

  int order = 2;

  //--------------1D lines-----------------

  ElementXFieldClass_1D xfldElem1D_0(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldClass_1D xfldElem1D_1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, xfldElem1D_0.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem1D_0.nDOF() );

  xfldElem1D_0.DOF(0) = 0.25;
  xfldElem1D_0.DOF(1) = 0.60;
  xfldElem1D_0.DOF(2) = 0.37;

  Element_Trace_Projection_L2( xfldElem1D_0, 1, xfldElem1D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(0)[0], 0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(1)[0], 0.60, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(2)[0], 0.37, tol );

  Element_Trace_Projection_L2( xfldElem1D_0,-1, xfldElem1D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(0)[0], 0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(1)[0], 0.60, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(2)[0], 0.37, tol );

  Element_Trace_Projection_L2( xfldElem1D_0, 1, xfldElem1D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(0)[0], 0.60, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(1)[0], 0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(2)[0], 0.37, tol );

  Element_Trace_Projection_L2( xfldElem1D_0,-1, xfldElem1D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(0)[0], 0.60, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(1)[0], 0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(2)[0], 0.37, tol );

  BOOST_CHECK_THROW( Element_Trace_Projection_L2( xfldElem1D_0, 0, xfldElem1D_1, 0 ), DeveloperException );


  //--------------2D lines-----------------

  ElementXFieldClass_2D xfldElem2D_0(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldClass_2D xfldElem2D_1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, xfldElem2D_0.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem2D_0.nDOF() );

  xfldElem2D_0.DOF(0) = {0.25,-0.40};
  xfldElem2D_0.DOF(1) = {0.75,-0.80};
  xfldElem2D_0.DOF(2) = {0.37, 0.68};

  Element_Trace_Projection_L2( xfldElem2D_0, 1, xfldElem2D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[0], 0.25, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[0], 0.75, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[0], 0.37, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[1],  0.68, tol );

  Element_Trace_Projection_L2( xfldElem2D_0,-1, xfldElem2D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[0], 0.25, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[0], 0.75, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[0], 0.37, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[1],  0.68, tol );

  Element_Trace_Projection_L2( xfldElem2D_0, 1, xfldElem2D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[0], 0.75, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[0], 0.25, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[0], 0.37, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[1],  0.68, tol );

  Element_Trace_Projection_L2( xfldElem2D_0,-1, xfldElem2D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[0], 0.75, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[0], 0.25, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[0], 0.37, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[1],  0.68, tol );

  BOOST_CHECK_THROW( Element_Trace_Projection_L2( xfldElem2D_0, 0, xfldElem2D_1, 0 ), DeveloperException );


  //--------------3D lines-----------------

  ElementXFieldClass_3D xfldElem3D_0(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldClass_3D xfldElem3D_1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, xfldElem3D_0.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem3D_0.nDOF() );

  xfldElem3D_0.DOF(0) = {0.25,-0.40, 2.00};
  xfldElem3D_0.DOF(1) = {0.75,-0.80,-1.00};
  xfldElem3D_0.DOF(2) = {0.37, 0.68, 4.03};

  Element_Trace_Projection_L2( xfldElem3D_0, 1, xfldElem3D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[0],  0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[2],  2.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[0],  0.75, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[2], -1.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[0],  0.37, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[1],  0.68, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[2],  4.03, tol );

  Element_Trace_Projection_L2( xfldElem3D_0,-1, xfldElem3D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[0],  0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[2],  2.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[0],  0.75, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[2], -1.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[0],  0.37, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[1],  0.68, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[2],  4.03, tol );

  Element_Trace_Projection_L2( xfldElem3D_0, 1, xfldElem3D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[0],  0.75, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[2], -1.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[0],  0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[2],  2.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[0],  0.37, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[1],  0.68, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[2],  4.03, tol );

  Element_Trace_Projection_L2( xfldElem3D_0,-1, xfldElem3D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[0],  0.75, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[2], -1.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[0],  0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[2],  2.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[0],  0.37, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[1],  0.68, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[2],  4.03, tol );

  BOOST_CHECK_THROW( Element_Trace_Projection_L2( xfldElem3D_0, 0, xfldElem3D_1, 0 ), DeveloperException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Element_Trace_Projection_L2_Legendre_P2 )
{
  typedef ElementXField<PhysD1, TopoD1, Line> ElementXFieldClass_1D;
  typedef ElementXField<PhysD2, TopoD1, Line> ElementXFieldClass_2D;
  typedef ElementXField<PhysD3, TopoD1, Line> ElementXFieldClass_3D;

  Real tol = 1e-12;

  int order = 2;

  //--------------1D lines-----------------

  ElementXFieldClass_1D xfldElem1D_0(order, BasisFunctionCategory_Legendre);
  ElementXFieldClass_1D xfldElem1D_1(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, xfldElem1D_0.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem1D_0.nDOF() );

  xfldElem1D_0.DOF(0) = 0.25;
  xfldElem1D_0.DOF(1) = 0.60;
  xfldElem1D_0.DOF(2) = 0.37;

  Element_Trace_Projection_L2( xfldElem1D_0, 1, xfldElem1D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(0)[0], 0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(1)[0], 0.60, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(2)[0], 0.37, tol );

  Element_Trace_Projection_L2( xfldElem1D_0,-1, xfldElem1D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(0)[0], 0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(1)[0], 0.60, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(2)[0], 0.37, tol );

  Element_Trace_Projection_L2( xfldElem1D_0, 1, xfldElem1D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(0)[0], 0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(1)[0],-0.60, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(2)[0], 0.37, tol );

  Element_Trace_Projection_L2( xfldElem1D_0,-1, xfldElem1D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(0)[0], 0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(1)[0],-0.60, tol );
  BOOST_CHECK_CLOSE( xfldElem1D_1.DOF(2)[0], 0.37, tol );

  BOOST_CHECK_THROW( Element_Trace_Projection_L2( xfldElem1D_0, 0, xfldElem1D_1, 0 ), DeveloperException );


  //--------------2D lines-----------------

  ElementXFieldClass_2D xfldElem2D_0(order, BasisFunctionCategory_Legendre);
  ElementXFieldClass_2D xfldElem2D_1(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, xfldElem2D_0.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem2D_0.nDOF() );

  xfldElem2D_0.DOF(0) = {0.25,-0.40};
  xfldElem2D_0.DOF(1) = {0.75,-0.80};
  xfldElem2D_0.DOF(2) = {0.37, 0.68};

  Element_Trace_Projection_L2( xfldElem2D_0, 1, xfldElem2D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[0], 0.25, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[0], 0.75, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[0], 0.37, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[1],  0.68, tol );

  Element_Trace_Projection_L2( xfldElem2D_0,-1, xfldElem2D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[0], 0.25, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[0], 0.75, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[0], 0.37, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[1],  0.68, tol );

  Element_Trace_Projection_L2( xfldElem2D_0, 1, xfldElem2D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[0], 0.25, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[0],-0.75, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[1],  0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[0], 0.37, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[1],  0.68, tol );

  Element_Trace_Projection_L2( xfldElem2D_0,-1, xfldElem2D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[0], 0.25, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[0],-0.75, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[1],  0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[0], 0.37, tol ); BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[1],  0.68, tol );

  BOOST_CHECK_THROW( Element_Trace_Projection_L2( xfldElem2D_0, 0, xfldElem2D_1, 0 ), DeveloperException );


  //--------------3D lines-----------------

  ElementXFieldClass_3D xfldElem3D_0(order, BasisFunctionCategory_Legendre);
  ElementXFieldClass_3D xfldElem3D_1(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, xfldElem3D_0.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem3D_0.nDOF() );

  xfldElem3D_0.DOF(0) = {0.25,-0.40, 2.00};
  xfldElem3D_0.DOF(1) = {0.75,-0.80,-1.00};
  xfldElem3D_0.DOF(2) = {0.37, 0.68, 4.03};

  Element_Trace_Projection_L2( xfldElem3D_0, 1, xfldElem3D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[0],  0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[2],  2.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[0],  0.75, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[2], -1.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[0],  0.37, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[1],  0.68, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[2],  4.03, tol );

  Element_Trace_Projection_L2( xfldElem3D_0,-1, xfldElem3D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[0],  0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[2],  2.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[0],  0.75, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[2], -1.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[0],  0.37, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[1],  0.68, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[2],  4.03, tol );

  Element_Trace_Projection_L2( xfldElem3D_0, 1, xfldElem3D_1,-1 );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[0],  0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[2],  2.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[0], -0.75, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[1],  0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[2],  1.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[0],  0.37, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[1],  0.68, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[2],  4.03, tol );

  Element_Trace_Projection_L2( xfldElem3D_0,-1, xfldElem3D_1, 1 );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[0],  0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(0)[2],  2.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[0], -0.75, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[1],  0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(1)[2],  1.00, tol );

  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[0],  0.37, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[1],  0.68, tol );
  BOOST_CHECK_CLOSE( xfldElem3D_1.DOF(2)[2],  4.03, tol );

  BOOST_CHECK_THROW( Element_Trace_Projection_L2( xfldElem3D_0, 0, xfldElem3D_1, 0 ), DeveloperException );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
