// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementXFieldVolume_Tetrahedron_btest
// testing of ElementXFieldVolume class w/ tetrahedron

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "tools/stringify.h"     // Real

#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"

#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"

#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/TraceToCellRefCoord.h"

using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class ElementXField<PhysD3,TopoD3,Tet>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementXFieldVolume_Tetrahedron_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK( (ElementXField<PhysD3,TopoD3,Tet>::D == 3) );
}

//Not sure how to test a 4D tetrahedron yet
typedef boost::mpl::list< PhysD3 > Dimensions;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctors, PhysDim, Dimensions )
{
  int order;

  order = 1;
  ElementXField<PhysDim,TopoD3,Tet> xElem_P1_1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem_P1_1.order() );
  BOOST_CHECK_EQUAL( 4, xElem_P1_1.nDOF() );

  order = 2;
  ElementXField<PhysDim,TopoD3,Tet> xElem_P2_1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, xElem_P2_1.order() );
  BOOST_CHECK_EQUAL( 10, xElem_P2_1.nDOF() );

  ElementXField<PhysDim,TopoD3,Tet> xElem_P2_2( BasisFunctionVolumeBase<Tet>::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, xElem_P2_2.order() );
  BOOST_CHECK_EQUAL( 10, xElem_P2_2.nDOF() );

  ElementXField<PhysDim,TopoD3,Tet> xElem_P2_3(xElem_P1_1);

  BOOST_CHECK_EQUAL( 1, xElem_P2_3.order() );
  BOOST_CHECK_EQUAL( 4, xElem_P2_3.nDOF() );

  xElem_P2_3 = xElem_P2_1;

  BOOST_CHECK_EQUAL( 2, xElem_P2_3.order() );
  BOOST_CHECK_EQUAL( 10, xElem_P2_3.nDOF() );

  typedef ElementXField<PhysDim,TopoD3,Tet> ElementType;
  BOOST_CHECK_THROW( ElementType xElem_P0(0, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( ElementType xElem_pmax(BasisFunctionVolume_Tet_HierarchicalPMax+1,
                                       BasisFunctionCategory_Hierarchical), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( accessors, PhysDim, Dimensions )
{
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysDim,TopoD3,Tet> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 4, xElem.nDOF() );

  typename ElementXField<PhysDim,TopoD3,Tet>::VectorX X1, X2, X3, X4;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    X1[i] = i+1;
    X2[i] = 2*i;
    X3[i] = 2*(i+1);
    X4[i] = 2*(i+2);
  }

  xElem.DOF(0) = X1;
  xElem.DOF(1) = X2;
  xElem.DOF(2) = X3;
  xElem.DOF(3) = X4;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    BOOST_CHECK_CLOSE( X1[i], xElem.DOF(0)[i], tol );
    BOOST_CHECK_CLOSE( X2[i], xElem.DOF(1)[i], tol );
    BOOST_CHECK_CLOSE( X3[i], xElem.DOF(2)[i], tol );
    BOOST_CHECK_CLOSE( X4[i], xElem.DOF(3)[i], tol );
  }

  ElementXField<PhysDim,TopoD3,Tet> xElem2(xElem);

  BOOST_CHECK_EQUAL( 1, xElem2.order() );
  BOOST_CHECK_EQUAL( 4, xElem2.nDOF() );

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    BOOST_CHECK_CLOSE( X1[i], xElem2.DOF(0)[i], tol );
    BOOST_CHECK_CLOSE( X2[i], xElem2.DOF(1)[i], tol );
    BOOST_CHECK_CLOSE( X3[i], xElem2.DOF(2)[i], tol );
    BOOST_CHECK_CLOSE( X4[i], xElem2.DOF(3)[i], tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( volume_coordinates, PhysDim, Dimensions )
{
  const Real small_tol = 1e-13;
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysDim,TopoD3,Tet> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 4, xElem.nDOF() );

  Real sRef, tRef, uRef;
  typename ElementXField<PhysDim,TopoD3,Tet>::VectorX X1, X2, X3, X4;
  typename ElementXField<PhysDim,TopoD3,Tet>::VectorX X, XTrue;
  Real volume, volumeTrue;

  X1 = X2 = X3 = X4 = 0;

  X1[0] = 0;  X1[1] = 0;  X1[2] = 0;
  X2[0] = 1;  X2[1] = 0;  X2[2] = 0;
  X3[0] = 0;  X3[1] = 1;  X3[2] = 0;
  X4[0] = 0;  X4[1] = 0;  X4[2] = 1;
  volumeTrue = 1./6.;

  xElem.DOF(0) = X1;
  xElem.DOF(1) = X2;
  xElem.DOF(2) = X3;
  xElem.DOF(3) = X4;

  X = xElem.DOF(0);
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( X1[i], X[i], small_tol, tol );

  X = xElem.DOF(1);
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( X2[i], X[i], small_tol, tol );

  X = xElem.DOF(2);
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( X3[i], X[i], small_tol, tol );

  X = xElem.DOF(3);
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( X4[i], X[i], small_tol, tol );

  volume = xElem.volume();
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );

  //------
  sRef = 0;  tRef = 0;  uRef = 0;
  XTrue = X1;
  xElem.coordinates( sRef, tRef, uRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  volume = xElem.volume( sRef, tRef, uRef );
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );

  //------
  sRef = 1;  tRef = 0;  uRef = 0;
  XTrue = X2;
  xElem.coordinates( sRef, tRef, uRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  volume = xElem.volume( sRef, tRef, uRef );
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );

  //------
  sRef = 0;  tRef = 1;  uRef = 0;
  XTrue = X3;
  xElem.coordinates( sRef, tRef, uRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  volume = xElem.volume( sRef, tRef, uRef );
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );

  //------
  sRef = 0;  tRef = 0;  uRef = 1;
  XTrue = X4;
  xElem.coordinates( sRef, tRef, uRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  volume = xElem.volume( sRef, tRef, uRef );
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );

  //------
  sRef = 1./4.;  tRef = 1./4.;  uRef = 1./4.;
  XTrue = (X1 + X2 + X3 + X4)/4.;
  xElem.coordinates( sRef, tRef, uRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  volume = xElem.volume( sRef, tRef, uRef );
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( eval_coordinate, PhysDim, Dimensions )
{
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysDim,TopoD3,Tet> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 4, xElem.nDOF() );

  Real sRef, tRef, uRef;
  typename ElementXField<PhysDim,TopoD3,Tet>::VectorX X1, X2, X3, X4;
  Real phi[4], phis[4], phit[4], phiu[4];
  Real phiTrue[4], phisTrue[4], phitTrue[4], phiuTrue[4];
  typename ElementXField<PhysDim,TopoD3,Tet>::VectorX sgrad, tgrad, ugrad;
  typename ElementXField<PhysDim,TopoD3,Tet>::VectorX sgradTrue, tgradTrue, ugradTrue;
  int k;

  X1 = X2 = X3 = X4 = 0;

  X1[0] = 0;  X1[1] = 0;  X1[2] = 0;
  X2[0] = 1;  X2[1] = 0;  X2[2] = 0;
  X3[0] = 0;  X3[1] = 1;  X3[2] = 0;
  X4[0] = 0;  X4[1] = 0;  X4[2] = 1;

  xElem.DOF(0) = X1;
  xElem.DOF(1) = X2;
  xElem.DOF(2) = X3;
  xElem.DOF(3) = X4;

  //Basis function truths
  phisTrue[0] = -1;  phisTrue[1] = 1;  phisTrue[2] = 0;  phisTrue[3] = 0;
  phitTrue[0] = -1;  phitTrue[1] = 0;  phitTrue[2] = 1;  phitTrue[3] = 0;
  phiuTrue[0] = -1;  phiuTrue[1] = 0;  phiuTrue[2] = 0;  phiuTrue[3] = 1;
  sgradTrue = X2;
  tgradTrue = X3;
  ugradTrue = X4;

  //------
  sRef = 0;  tRef = 0;  uRef = 0;
  phiTrue[0] = 1;  phiTrue[1] = 0;  phiTrue[2] = 0;  phiTrue[3] = 0;

  xElem.evalBasis( sRef, tRef, uRef, phi, 4 );
  xElem.evalBasisDerivative( sRef, tRef, uRef, phis, phit, phiu, 4 );
  for (k = 0; k < 4; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
  }

  xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, sgrad, tgrad, ugrad );
  for ( int i = 0; i < PhysDim::D; i++ )
  {
    BOOST_CHECK_CLOSE( sgradTrue[i], sgrad[i], tol );
    BOOST_CHECK_CLOSE( tgradTrue[i], tgrad[i], tol );
    BOOST_CHECK_CLOSE( ugradTrue[i], ugrad[i], tol );
  }

  //------
  sRef = 1;  tRef = 0;  uRef = 0;
  phiTrue[0] = 0;  phiTrue[1] = 1;  phiTrue[2] = 0;  phiTrue[3] = 0;

  xElem.evalBasis( sRef, tRef, uRef, phi, 4 );
  xElem.evalBasisDerivative( sRef, tRef, uRef, phis, phit, phiu, 4 );
  for (k = 0; k < 4; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
  }

  xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, sgrad, tgrad, ugrad );
  for ( int i = 0; i < PhysDim::D; i++ )
  {
    BOOST_CHECK_CLOSE( sgradTrue[i], sgrad[i], tol );
    BOOST_CHECK_CLOSE( tgradTrue[i], tgrad[i], tol );
    BOOST_CHECK_CLOSE( ugradTrue[i], ugrad[i], tol );
  }

  //------
  sRef = 0;  tRef = 1;  uRef = 0;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 1;  phiTrue[3] = 0;

  xElem.evalBasis( sRef, tRef, uRef, phi, 4 );
  xElem.evalBasisDerivative( sRef, tRef, uRef, phis, phit, phiu, 4 );
  for (k = 0; k < 4; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
  }

  xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, sgrad, tgrad, ugrad );
  for ( int i = 0; i < PhysDim::D; i++ )
  {
    BOOST_CHECK_CLOSE( sgradTrue[i], sgrad[i], tol );
    BOOST_CHECK_CLOSE( tgradTrue[i], tgrad[i], tol );
    BOOST_CHECK_CLOSE( ugradTrue[i], ugrad[i], tol );
  }

  //------
  sRef = 0;  tRef = 0;  uRef = 1;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 0;  phiTrue[3] = 1;

  xElem.evalBasis( sRef, tRef, uRef, phi, 4 );
  xElem.evalBasisDerivative( sRef, tRef, uRef, phis, phit, phiu, 4 );
  for (k = 0; k < 4; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
  }

  xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, sgrad, tgrad, ugrad );
  for ( int i = 0; i < PhysDim::D; i++ )
  {
    BOOST_CHECK_CLOSE( sgradTrue[i], sgrad[i], tol );
    BOOST_CHECK_CLOSE( tgradTrue[i], tgrad[i], tol );
    BOOST_CHECK_CLOSE( ugradTrue[i], ugrad[i], tol );
  }

  //------
  sRef = 1./4.;  tRef = 1./4.;  uRef = 1./4.;
  phiTrue[0] = 1./4.;  phiTrue[1] = 1./4.;  phiTrue[2] = 1./4.;  phiTrue[3] = 1./4.;

  xElem.evalBasis( sRef, tRef, uRef, phi, 4 );
  xElem.evalBasisDerivative( sRef, tRef, uRef, phis, phit, phiu, 4 );
  for (k = 0; k < 4; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
  }

  xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, sgrad, tgrad, ugrad );
  for ( int i = 0; i < PhysDim::D; i++ )
  {
    BOOST_CHECK_CLOSE( sgradTrue[i], sgrad[i], tol );
    BOOST_CHECK_CLOSE( tgradTrue[i], tgrad[i], tol );
    BOOST_CHECK_CLOSE( ugradTrue[i], ugrad[i], tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( evalBasisGradient )
{
  typedef BasisFunctionVolume<Tet,Hierarchical,1> BasisClass;

  int order = 1;
  ElementXField<PhysD3,TopoD3,Tet> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 4, xElem.nDOF() );

  const BasisClass* basis = BasisClass::self();
  Element<Real,TopoD3,Tet> qfldElem(basis);

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( 4, basis->nBasis() );

  const Real tol = 1e-13;
  typename ElementXField<PhysD3,TopoD3,Tet>::RefCoordType sRef;
  typename ElementXField<PhysD3,TopoD3,Tet>::VectorX gradphi[4];
  Real phixTrue[4], phiyTrue[4], phizTrue[4];
  int k;

  xElem.DOF(0) = {0, 0, 0};
  xElem.DOF(1) = {1, 0, 0};
  xElem.DOF(2) = {0, 1, 0};
  xElem.DOF(3) = {0, 0, 1};

  // basis gradient independent of location for Q=1, P=1
  phixTrue[0] = -1;  phixTrue[1] = 1;  phixTrue[2] = 0;  phixTrue[3] = 0;
  phiyTrue[0] = -1;  phiyTrue[1] = 0;  phiyTrue[2] = 1;  phiyTrue[3] = 0;
  phizTrue[0] = -1;  phizTrue[1] = 0;  phizTrue[2] = 0;  phizTrue[3] = 1;

  //------
  sRef[0] = 0;  sRef[1] = 0;  sRef[2] = 0;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, 4 );

  for (k = 0; k < 4; k++)
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phiyTrue[k], gradphi[k][1], tol );
    BOOST_CHECK_CLOSE( phizTrue[k], gradphi[k][2], tol );
  }

  //------
  sRef[0] = 1;  sRef[1] = 0;  sRef[2] = 0;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, 4 );

  for (k = 0; k < 4; k++)
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phiyTrue[k], gradphi[k][1], tol );
    BOOST_CHECK_CLOSE( phizTrue[k], gradphi[k][2], tol );
  }

  //------
  sRef[0] = 0;  sRef[1] = 1;  sRef[2] = 0;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, 4 );

  for (k = 0; k < 4; k++)
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phiyTrue[k], gradphi[k][1], tol );
    BOOST_CHECK_CLOSE( phizTrue[k], gradphi[k][2], tol );
  }

  //------
  sRef[0] = 0;  sRef[1] = 0;  sRef[2] = 1;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, 4 );

  for (k = 0; k < 4; k++)
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phiyTrue[k], gradphi[k][1], tol );
    BOOST_CHECK_CLOSE( phizTrue[k], gradphi[k][2], tol );
  }

  //------
  sRef[0] = 1./4.;  sRef[1] = 1./4.;  sRef[2] = 1./4.;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, 4 );

  for (k = 0; k < 4; k++)
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phiyTrue[k], gradphi[k][1], tol );
    BOOST_CHECK_CLOSE( phizTrue[k], gradphi[k][2], tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( evalReferenceCoordinateHessian )
{
  const Real tol = 1e-11;

  typename ElementXField<PhysD3,TopoD3,Tet>::VectorX sgrad, tgrad, ugrad;
  typename ElementXField<PhysD3,TopoD3,Tet>::Matrix invJ;

  typename ElementXField<PhysD3,TopoD3,Tet>::TensorSymX shess, thess, uhess;
  typename ElementXField<PhysD3,TopoD3,Tet>::TensorSymX shessTrue, thessTrue, uhessTrue;
  typename ElementXField<PhysD3,TopoD3,Tet>::TensorSymHessian H;
  typename ElementXField<PhysD3,TopoD3,Tet>::TensorSymX hesstmp0, hesstmp1, hesstmp2;

  Real sRef = 0.3, tRef=0.4, uRef = 0.25;

  int order = 2;
  ElementXField<PhysD3,TopoD3,Tet> xElem(order, BasisFunctionCategory_Lagrange);


  // Node DOFs
  xElem.DOF(0) = {0, 0, 0};
  xElem.DOF(1) = {1, 0, 0};
  xElem.DOF(2) = {0, 1, 0};
  xElem.DOF(3) = {0, 0, 1};

  // Edge DOFs
  xElem.DOF(4) = {-0.1,  0.6,  0.5};
  xElem.DOF(5) = { 0.6, -0.1,  0.5};
  xElem.DOF(6) = { 0.5,  0.6,  0.0};
  xElem.DOF(7) = {-0.1,  0.5, -0.1};
  xElem.DOF(8) = { 0.0, -0.1,  0.5};
  xElem.DOF(9) = { 0.5, -0.1, -0.1};

  xElem.hessian( sRef, tRef, uRef, H );
  xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, sgrad, tgrad, ugrad );
  xElem.evalReferenceCoordinateHessian( sRef, tRef, uRef, shess, thess, uhess );

  invJ(0,0) = sgrad[0];
  invJ(0,1) = sgrad[1];
  invJ(0,2) = sgrad[2];

  invJ(1,0) = tgrad[0];
  invJ(1,1) = tgrad[1];
  invJ(1,2) = tgrad[2];

  invJ(2,0) = ugrad[0];
  invJ(2,1) = ugrad[1];
  invJ(2,2) = ugrad[2];

  hesstmp0 = Transpose(invJ)*H[0]*invJ;
  hesstmp1 = Transpose(invJ)*H[1]*invJ;
  hesstmp2 = Transpose(invJ)*H[2]*invJ;

  shessTrue = -(invJ(0,0)*hesstmp0 + invJ(0,1)*hesstmp1 + invJ(0,2)*hesstmp2);
  thessTrue = -(invJ(1,0)*hesstmp0 + invJ(1,1)*hesstmp1 + invJ(1,2)*hesstmp2);
  uhessTrue = -(invJ(2,0)*hesstmp0 + invJ(2,1)*hesstmp1 + invJ(2,2)*hesstmp2);

  for (int i = 0; i < PhysD3::D; i++)
    for (int j = 0; j <= i; j++)
    {
      BOOST_CHECK_CLOSE( shessTrue(i,j), shess(i,j), tol );
      BOOST_CHECK_CLOSE( thessTrue(i,j), thess(i,j), tol );
      BOOST_CHECK_CLOSE( uhessTrue(i,j), uhess(i,j), tol );
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( extract_basis, PhysDim, Dimensions )
{
  int order = 1;
  ElementXField<PhysDim,TopoD3,Tet> xElem_P1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem_P1.order() );
  BOOST_CHECK_EQUAL( 4, xElem_P1.nDOF() );

  const BasisFunctionVolumeBase<Tet>* basis_P1 = xElem_P1.basis();

  BOOST_CHECK_EQUAL( 1, basis_P1->order() );
  BOOST_CHECK_EQUAL( 4, basis_P1->nBasis() );

  order = 2;
  ElementXField<PhysDim,TopoD3,Tet> xElem_P2(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, xElem_P2.order() );
  BOOST_CHECK_EQUAL( 10, xElem_P2.nDOF() );

  const BasisFunctionVolumeBase<Tet>* basis_P2 = xElem_P2.basis();

  BOOST_CHECK_EQUAL( 2, basis_P2->order() );
  BOOST_CHECK_EQUAL( 10, basis_P2->nBasis() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( face_sign, PhysDim, Dimensions )
{
  typedef std::array<int,4> Int4;

  int order = 1;
  ElementXField<PhysDim,TopoD3,Tet> xElem(order, BasisFunctionCategory_Hierarchical);

  Int4 faceSign = xElem.faceSign();
  BOOST_CHECK_EQUAL( +1, faceSign[0] );
  BOOST_CHECK_EQUAL( +1, faceSign[1] );
  BOOST_CHECK_EQUAL( +1, faceSign[2] );
  BOOST_CHECK_EQUAL( +1, faceSign[3] );

  faceSign[1] = -1;
  xElem.setFaceSign(faceSign);

  Int4 faceSign2 = xElem.faceSign();
  BOOST_CHECK_EQUAL( +1, faceSign2[0] );
  BOOST_CHECK_EQUAL( -1, faceSign2[1] );
  BOOST_CHECK_EQUAL( +1, faceSign2[2] );
  BOOST_CHECK_EQUAL( +1, faceSign2[3] );
}

//----------------------------------------------------------------------------//
// check that the trace element coordinates evaluate to the volume element coordinates on the trace
BOOST_AUTO_TEST_CASE( Trace_RefCoord_P1 )
{
  typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldVolumeClass;
  typedef ElementXField<PhysD3,TopoD2,Triangle> ElementXFieldAreaClass;
  typedef ElementXFieldVolumeClass::VectorX VectorX;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  // element field variables
  ElementXFieldAreaClass xfldElemFaceP( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  ElementXFieldAreaClass xfldElemFaceN( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  ElementXFieldVolumeClass xfldElemCell( BasisFunctionVolumeBase<Tet>::HierarchicalP1 );

  xfldElemCell.DOF(0) = {0, 0, 0};
  xfldElemCell.DOF(1) = {1, 0, 0};
  xfldElemCell.DOF(2) = {0, 1, 0};
  xfldElemCell.DOF(3) = {0, 0, 1};

  const int tet[4] = {0, 1, 2, 3};

  const int (*TraceNodes)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes;

  const int (*OrientPos)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::OrientPos;
  const int (*OrientNeg)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::OrientNeg;


  for (int face = 0; face < Tet::NTrace; face++)
  {
    for (int orient = 0; orient < Triangle::NTrace; orient++)
    {
      // Get the re-oriented triangles
      const int triP[3] = {tet[TraceNodes[face][OrientPos[orient][0]]],
                           tet[TraceNodes[face][OrientPos[orient][1]]],
                           tet[TraceNodes[face][OrientPos[orient][2]]]};

      const int triN[3] = {tet[TraceNodes[face][OrientNeg[orient][0]]],
                           tet[TraceNodes[face][OrientNeg[orient][1]]],
                           tet[TraceNodes[face][OrientNeg[orient][2]]]};

      const CanonicalTraceToCell canonicalFaceP = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(triP, 3, tet, 4);
      const CanonicalTraceToCell canonicalFaceN = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(triN, 3, tet, 4);

      BOOST_REQUIRE_EQUAL(canonicalFaceP.trace, face);
      BOOST_REQUIRE_EQUAL(canonicalFaceP.orientation, orient+1);

      BOOST_REQUIRE_EQUAL(canonicalFaceN.trace, face);
      BOOST_REQUIRE_EQUAL(canonicalFaceN.orientation, -orient-1);

      // Set the DOF's on the trace
      for (int n = 0; n < Triangle::NNode; n++)
      {
        xfldElemFaceP.DOF(n) = xfldElemCell.DOF(triP[n]);
        xfldElemFaceN.DOF(n) = xfldElemCell.DOF(triN[n]);
      }

      int kmax = 5;
      for (int k = 0; k < kmax; k++)
      {
        DLA::VectorS<2,Real> sRefTrace;
        DLA::VectorS<3,Real> sRefCell;
        VectorX xTrace, xCell;
        Real s, t;

        // Edge 0
        s = k/static_cast<Real>(kmax-1);
        t = 1-s;

        sRefTrace = {s,t};

        // volume reference-element coords
        TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFaceP, sRefTrace, sRefCell );

        // element coordinates from trace and cell
        xfldElemFaceP.eval( sRefTrace, xTrace );
        xfldElemCell.eval( sRefCell, xCell );

        SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );

        // volume reference-element coords
        TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFaceN, sRefTrace, sRefCell );

        // element coordinates from trace and cell
        xfldElemFaceN.eval( sRefTrace, xTrace );
        xfldElemCell.eval( sRefCell, xCell );

        SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );


        // Edge 1
        s = 0;
        t = k/static_cast<Real>(kmax-1);

        sRefTrace = {s,t};

        // left/right reference-element coords
        TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFaceP, sRefTrace, sRefCell );

        // element coordinates from trace and cell
        xfldElemFaceP.eval( sRefTrace, xTrace );
        xfldElemCell.eval( sRefCell, xCell );

        SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );

        // volume reference-element coords
        TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFaceN, sRefTrace, sRefCell );

        // element coordinates from trace and cell
        xfldElemFaceN.eval( sRefTrace, xTrace );
        xfldElemCell.eval( sRefCell, xCell );

        SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );


        // Edge 2
        s = k/static_cast<Real>(kmax-1);
        t = 0;

        sRefTrace = {s,t};

        // left/right reference-element coords
        TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFaceP, sRefTrace, sRefCell );

        // element coordinates from trace and cell
        xfldElemFaceP.eval( sRefTrace, xTrace );
        xfldElemCell.eval( sRefCell, xCell );

        SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );

        // volume reference-element coords
        TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFaceN, sRefTrace, sRefCell );

        // element coordinates from trace and cell
        xfldElemFaceN.eval( sRefTrace, xTrace );
        xfldElemCell.eval( sRefCell, xCell );

        SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );
      }
    }
  }
}


//----------------------------------------------------------------------------//
// check that the trace element coordinates evaluate to the volume element coordinates on the trace
BOOST_AUTO_TEST_CASE( Frame_RefCoord_P1 )
{
  typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldVolumeClass;
  typedef ElementXField<PhysD3,TopoD2,Triangle> ElementXFieldAreaClass;
  typedef ElementXField<PhysD3,TopoD1,Line> ElementXFieldLineClass;
  typedef ElementXFieldVolumeClass::VectorX VectorX;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  // element field variables
  ElementXFieldLineClass xfldElemEdgeP( BasisFunctionLineBase::HierarchicalP1 );
  ElementXFieldLineClass xfldElemEdgeN( BasisFunctionLineBase::HierarchicalP1 );
  ElementXFieldAreaClass xfldElemFaceP( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  ElementXFieldAreaClass xfldElemFaceN( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  ElementXFieldVolumeClass xfldElemCell( BasisFunctionVolumeBase<Tet>::HierarchicalP1 );

  xfldElemCell.DOF(0) = {0, 0, 0};
  xfldElemCell.DOF(1) = {1, 0, 0};
  xfldElemCell.DOF(2) = {0, 1, 0};
  xfldElemCell.DOF(3) = {0, 0, 1};

  const int tet[4] = {0, 1, 2, 3};

  const int (*TraceNodes)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes;

  const int (*OrientTPos)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::OrientPos;
  const int (*OrientTNeg)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::OrientNeg;

  const int (*FrameNodes)[ Line::NTrace ] = TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes;

  const int *OrientFPos = TraceToCellRefCoord<Line, TopoD2, Triangle>::OrientPos;
  const int *OrientFNeg = TraceToCellRefCoord<Line, TopoD2, Triangle>::OrientNeg;

  for (int face = 0; face < Tet::NTrace; face++)
  {
    for (int orient = 0; orient < Triangle::NTrace; orient++)
    {
      // Get the re-oriented triangles
      const int triP[3] = {tet[TraceNodes[face][OrientTPos[orient][0]]],
                           tet[TraceNodes[face][OrientTPos[orient][1]]],
                           tet[TraceNodes[face][OrientTPos[orient][2]]]};

      const int triN[3] = {tet[TraceNodes[face][OrientTNeg[orient][0]]],
                           tet[TraceNodes[face][OrientTNeg[orient][1]]],
                           tet[TraceNodes[face][OrientTNeg[orient][2]]]};

      const CanonicalTraceToCell canonicalFaceP = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(triP, 3, tet, 4);
      const CanonicalTraceToCell canonicalFaceN = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(triN, 3, tet, 4);

      BOOST_REQUIRE_EQUAL(canonicalFaceP.trace, face);
      BOOST_REQUIRE_EQUAL(canonicalFaceP.orientation, orient+1);

      BOOST_REQUIRE_EQUAL(canonicalFaceN.trace, face);
      BOOST_REQUIRE_EQUAL(canonicalFaceN.orientation, -orient-1);

      // Set the DOF's on the trace
      for (int n = 0; n < Triangle::NNode; n++)
      {
        xfldElemFaceP.DOF(n) = xfldElemCell.DOF(triP[n]);
        xfldElemFaceN.DOF(n) = xfldElemCell.DOF(triN[n]);
      }

      // Test both positive and negative triangles
      for (int n = 0; n < 2; n++ )
      {
        const int* tri = NULL;
        CanonicalTraceToCell canonicalFace;
        ElementXFieldAreaClass* xfldElemFace = NULL;
        if (n == 0)
        {
          tri = triP;
          canonicalFace = canonicalFaceP;
          xfldElemFace = &xfldElemFaceP;
        }
        else if (n == 1)
        {
          tri = triN;
          canonicalFace = canonicalFaceN;
          xfldElemFace = &xfldElemFaceN;
        }
        else
          BOOST_REQUIRE( false ); // suppress compiler warning

        for (int edge = 0; edge < Triangle::NTrace; edge++)
        {
          // Get the re-oriented lines
          const int lineP[2] = {tri[FrameNodes[edge][OrientFPos[0]]],
                                tri[FrameNodes[edge][OrientFPos[1]]]};

          const int lineN[2] = {tri[FrameNodes[edge][OrientFNeg[0]]],
                                tri[FrameNodes[edge][OrientFNeg[1]]]};

          const CanonicalTraceToCell canonicalEdgeP = TraceToCellRefCoord<Line, TopoD2, Triangle>::getCanonicalTrace(lineP, 2, tri, 3);
          const CanonicalTraceToCell canonicalEdgeN = TraceToCellRefCoord<Line, TopoD2, Triangle>::getCanonicalTrace(lineN, 2, tri, 3);

          BOOST_REQUIRE_EQUAL(canonicalEdgeP.trace, edge);
          BOOST_REQUIRE_EQUAL(canonicalEdgeP.orientation, +1);

          BOOST_REQUIRE_EQUAL(canonicalEdgeN.trace, edge);
          BOOST_REQUIRE_EQUAL(canonicalEdgeN.orientation, -1);

          // Set the DOF's on the trace
          for (int n = 0; n < Line::NNode; n++)
          {
            xfldElemEdgeP.DOF(n) = xfldElemCell.DOF(lineP[n]);
            xfldElemEdgeN.DOF(n) = xfldElemCell.DOF(lineN[n]);
          }

          int kmax = 2;
          for (int k = 0; k < kmax; k++)
          {
            DLA::VectorS<1,Real> sRefFrame;
            DLA::VectorS<2,Real> sRefTrace;
            DLA::VectorS<3,Real> sRefCell;
            VectorX xFrame, xTrace, xCell;
            Real s;

            s = k/static_cast<Real>(kmax-1);

            sRefFrame = {s};

            // volume reference-element coords
            TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalEdgeP, sRefFrame, sRefTrace );
            TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefTrace, sRefCell );

            // element coordinates from trace and cell
            xfldElemEdgeP.eval( sRefFrame, xFrame );
            xfldElemFace->eval( sRefTrace, xTrace );
            xfldElemCell.eval( sRefCell, xCell );

            SANS_CHECK_CLOSE( xFrame[0], xTrace[0], small_tol, close_tol );
            SANS_CHECK_CLOSE( xFrame[1], xTrace[1], small_tol, close_tol );
            SANS_CHECK_CLOSE( xFrame[2], xTrace[2], small_tol, close_tol );

            SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
            SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
            SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );

            // volume reference-element coords
            TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalEdgeN, sRefFrame, sRefTrace );
            TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefTrace, sRefCell );

            // element coordinates from trace and cell
            xfldElemEdgeN.eval( sRefFrame, xFrame );
            xfldElemFace->eval( sRefTrace, xTrace );
            xfldElemCell.eval( sRefCell, xCell );

            SANS_CHECK_CLOSE( xFrame[0], xTrace[0], small_tol, close_tol );
            SANS_CHECK_CLOSE( xFrame[1], xTrace[1], small_tol, close_tol );
            SANS_CHECK_CLOSE( xFrame[2], xTrace[2], small_tol, close_tol );

            SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
            SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
            SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );
          }
        }
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BoundingBox_Q1_test )
{
  int order = 1;
  ElementXField<PhysD3,TopoD3,Tet> xElem(order, BasisFunctionCategory_Lagrange);

  xElem.DOF(0) = {0,0,0};
  xElem.DOF(1) = {1,0,0};
  xElem.DOF(2) = {0,1,0};
  xElem.DOF(3) = {0,0,1};

  BoundingBox<PhysD3> bbox = xElem.boundingBox();

  BOOST_CHECK_EQUAL(0, bbox.low_bounds()[0]);
  BOOST_CHECK_EQUAL(0, bbox.low_bounds()[1]);
  BOOST_CHECK_EQUAL(0, bbox.low_bounds()[2]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[0]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[1]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[2]);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BoundingBox_Q2_test )
{
  int order = 2;
  ElementXField<PhysD3,TopoD3,Tet> xElem(order, BasisFunctionCategory_Lagrange);

  // node DOFs
  xElem.DOF(0) = {0,0,0};
  xElem.DOF(1) = {1,0,0};
  xElem.DOF(2) = {0,1,0};
  xElem.DOF(3) = {0,0,1};

  // edge DOFs
  xElem.DOF(4) = { 0.0,  0.5,  0.5};
  xElem.DOF(5) = { 0.5, -0.1,  0.5};
  xElem.DOF(6) = { 0.5,  0.5,  0.0};
  xElem.DOF(7) = {-0.1,  0.5,  0.0};
  xElem.DOF(8) = {-0.1,  0.0,  0.5};
  xElem.DOF(9) = { 0.5,  0.0, -0.1};

  BoundingBox<PhysD3> bbox = xElem.boundingBox();

  BOOST_CHECK_EQUAL(-0.1, bbox.low_bounds()[0]);
  BOOST_CHECK_EQUAL(-0.1, bbox.low_bounds()[1]);
  BOOST_CHECK_EQUAL(-0.1, bbox.low_bounds()[2]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[0]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[1]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[2]);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( impliedMetric )
{
  const Real small_tol = 1e-13;
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysD3, TopoD3, Tet> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 4, xElem.nDOF() );

  ElementXField<PhysD3, TopoD3, Tet>::RefCoordType sRef = {0.0, 0.0, 0.0};

  DLA::MatrixSymS<PhysD3::D, Real> M;

  //Unit equilateral tet
  xElem.DOF(0) = {0.0, 0.0, 0.0};
  xElem.DOF(1) = {1.0, 0.0, 0.0};
  xElem.DOF(2) = {0.5, sqrt(3.0)/2.0, 0.0};
  xElem.DOF(3) = {0.5, sqrt(3.0)/6.0, sqrt(2.0/3.0)};

  xElem.impliedMetric(sRef, M);

  SANS_CHECK_CLOSE( M(0,0), 1.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,2), 0.0, small_tol, tol );

  SANS_CHECK_CLOSE( M(1,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,1), 1.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,2), 0.0, small_tol, tol );

  SANS_CHECK_CLOSE( M(2,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(2,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(2,2), 1.0, small_tol, tol );

  M = 0;
  xElem.impliedMetric(M); //evaluate at centerRef

  SANS_CHECK_CLOSE( M(0,0), 1.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,2), 0.0, small_tol, tol );

  SANS_CHECK_CLOSE( M(1,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,1), 1.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,2), 0.0, small_tol, tol );

  SANS_CHECK_CLOSE( M(2,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(2,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(2,2), 1.0, small_tol, tol );

  //Unit tet - scaled by 2.5, translated by (0.6, 1.3, 3.2)
  Real scale = 2.5;
  Real xshift[3] = {0.6, 1.3, 3.2};
  for (int i=0; i<4; i++)
    for (int d=0; d<3; d++)
      xElem.DOF(i)[d] = xElem.DOF(i)[d]*scale + xshift[d];

  xElem.impliedMetric(sRef, M);

  SANS_CHECK_CLOSE( M(0,0), 1.0/pow(scale,2.0), small_tol, tol );
  SANS_CHECK_CLOSE( M(0,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,2), 0.0, small_tol, tol );

  SANS_CHECK_CLOSE( M(1,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,1), 1.0/pow(scale,2.0), small_tol, tol );
  SANS_CHECK_CLOSE( M(1,2), 0.0, small_tol, tol );

  SANS_CHECK_CLOSE( M(2,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(2,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(2,2), 1.0/pow(scale,2.0), small_tol, tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( IO, PhysDim, Dimensions )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/ElementXField" + stringify((int)PhysDim::D) + "DVolume_Tetrahedron_pattern.txt", true );

  ElementXField<PhysDim,TopoD3,Tet> xElem(1, BasisFunctionCategory_Hierarchical);

  typename ElementXField<PhysDim,TopoD3,Tet>::VectorX X1, X2, X3, X4;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    X1[i] = i+1;
    X2[i] = 2*i;
    X3[i] = 2*(i+1);
    X4[i] = 2*(i+2);
  }

  xElem.DOF(0) = X1;
  xElem.DOF(1) = X2;
  xElem.DOF(2) = X3;
  xElem.DOF(3) = X4;

  xElem.dump( 2, output );
  xElem.dumpTecplot( output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
