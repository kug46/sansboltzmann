// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementVolume_Tetrahedron_btest
// testing of Element< T, TopoD3, Tet >

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <ostream>
#include <memory>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"
#include "Field/Element/ElementSpacetime.h"
#include "BasisFunction/BasisFunctionSpacetime_Pentatope.h"
#include "BasisFunction/TraceToCellRefCoord.h"
#include "Quadrature/QuadratureVolume.h"
#include "Quadrature/QuadratureSpacetime.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class Element<Real, TopoD4,Pentatope>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementSpacetime_Pentatope_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  typedef Element< Real, TopoD4, Pentatope > ElementClass;

  static_assert( std::is_same<typename ElementClass::BasisType, BasisFunctionSpacetimeBase<Pentatope> >::value, "Incorrect Basis type" );
  static_assert( std::is_same<typename ElementClass::TopologyType, Pentatope>::value, "Incorrect topology type" );
  static_assert( std::is_same<typename ElementClass::RefCoordType, DLA::VectorS<4,Real> >::value, "Incorrect reference coordinate type" );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef Element< Real, TopoD4, Pentatope > ElementClass;

  int order;

  order = 1;
  ElementClass qElem_P1_1(order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL( 1, qElem_P1_1.order() );
  BOOST_CHECK_EQUAL( 5, qElem_P1_1.nDOF() );


  order = 2;
  ElementClass qElem_P2_1(order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL( 2, qElem_P2_1.order() );
  BOOST_CHECK_EQUAL( 15, qElem_P2_1.nDOF() );

  ElementClass qElem_P2_2( BasisFunctionSpacetimeBase<Pentatope>::LagrangeP2 );

  BOOST_CHECK_EQUAL( 2, qElem_P2_2.order() );
  BOOST_CHECK_EQUAL( 15, qElem_P2_2.nDOF() );

  ElementClass qElem_P2_3(qElem_P1_1);

  BOOST_CHECK_EQUAL( 1, qElem_P2_3.order() );
  BOOST_CHECK_EQUAL( 5, qElem_P2_3.nDOF() );

  qElem_P2_3 = qElem_P2_1;

  BOOST_CHECK_EQUAL( 2, qElem_P2_3.order() );
  BOOST_CHECK_EQUAL( 15, qElem_P2_3.nDOF() );

  ElementClass qElem_P2_4;

  qElem_P2_4.setBasis( BasisFunctionSpacetimeBase<Pentatope>::LagrangeP2 );

  BOOST_CHECK_EQUAL( 2, qElem_P2_4.order() );
  BOOST_CHECK_EQUAL( 15, qElem_P2_4.nDOF() );


  BOOST_CHECK_THROW( ElementClass qElem_P0(0, BasisFunctionCategory_Lagrange), DeveloperException );
  BOOST_CHECK_THROW( ElementClass qElem_pmax(BasisFunctionSpacetime_Pentatope_LagrangePMax+1, BasisFunctionCategory_Lagrange), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( accessors )
{
  typedef DLA::VectorS< 3, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD4, Pentatope > ElementClass;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( 5, qElem.nDOF() );

  const Real tol = 1e-13;

  ArrayQ q1 = {1, 2, 3};
  ArrayQ q2 = {4, 5, 6};
  ArrayQ q3 = {7, 8, 9};
  ArrayQ q4 = {10, 11, 12};
  ArrayQ q5 = {13, 14, 15};

  qElem.DOF(0) = q1;
  qElem.DOF(1) = q2;
  qElem.DOF(2) = q3;
  qElem.DOF(3) = q4;
  qElem.DOF(4) = q5;

  ArrayQ q;
  for (int k = 0; k < 3; k++)
  {
    q = qElem.DOF(0);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );
    q = qElem.DOF(1);
    BOOST_CHECK_CLOSE( q2[k], q[k], tol );
    q = qElem.DOF(2);
    BOOST_CHECK_CLOSE( q3[k], q[k], tol );
    q = qElem.DOF(3);
    BOOST_CHECK_CLOSE( q4[k], q[k], tol );
    q = qElem.DOF(4);
    BOOST_CHECK_CLOSE( q5[k], q[k], tol );
  }


  // Check the vector view version of the DOF
  DLA::VectorDView<ArrayQ> vectorDOF = qElem.vectorViewDOF();

  BOOST_CHECK_EQUAL( qElem.nDOF(), vectorDOF.m() );

  vectorDOF[0] = q5;
  vectorDOF[1] = q4;
  vectorDOF[2] = q3;
  vectorDOF[3] = q2;
  vectorDOF[4] = q1;

  for (int k = 0; k < 3; k++)
  {
    q = qElem.DOF(0);
    BOOST_CHECK_CLOSE( q5[k], q[k], tol );
    q = qElem.DOF(1);
    BOOST_CHECK_CLOSE( q4[k], q[k], tol );
    q = qElem.DOF(2);
    BOOST_CHECK_CLOSE( q3[k], q[k], tol );
    q = qElem.DOF(3);
    BOOST_CHECK_CLOSE( q2[k], q[k], tol );
    q = qElem.DOF(4);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( eval_basis_P1 )
{
  typedef DLA::VectorS< 3, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD4, Pentatope > ElementClass;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Lagrange);

  static const int NNode = Pentatope::NNode;

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( NNode, qElem.nDOF() );

  const Real tol = 1e-13;
  Real sRef, tRef, uRef, vRef;
  Real phi[NNode], phi1[NNode], phis[NNode], phit[NNode], phiu[NNode], phiv[NNode];
  Real phiss[NNode], phist[NNode], phitt[NNode],
       phisu[NNode], phitu[NNode], phiuu[NNode],
       phisv[NNode], phitv[NNode], phiuv[NNode], phivv[NNode];
  const Real *phi4, *phis4, *phit4, *phiu4, *phiv4;
  const Real *phiss4, *phist4, *phitt4,
             *phisu4, *phitu4, *phiuu4,
             *phisv4, *phitv4, *phiuv4, *phivv4;
  Real phiTrue[NNode], phisTrue[NNode], phitTrue[NNode], phiuTrue[NNode], phivTrue[NNode];
  Real phissTrue[NNode], phistTrue[NNode], phittTrue[NNode],
       phisuTrue[NNode], phituTrue[NNode], phiuuTrue[NNode],
       phisvTrue[NNode], phitvTrue[NNode], phiuvTrue[NNode], phivvTrue[NNode];
  int k;

  for (k=0;k<NNode;k++)
  {
    phissTrue[k]=0; phistTrue[k]=0; phittTrue[k]=0;
    phisuTrue[k]=0; phituTrue[k]=0; phiuuTrue[k]=0;
    phisvTrue[k]=0; phitvTrue[k]=0; phiuvTrue[k]=0,phivvTrue[k]=0;
  }

  sRef = 0;  tRef = 0;  uRef = 0; vRef = 0;
  phiTrue[0]  =  1;  phiTrue[1]  = 0;  phiTrue[2]  = 0;  phiTrue[3]  = 0; phiTrue[4] = 0;

  qElem.evalBasis( sRef, tRef, uRef, vRef, phi, NNode );
  qElem.evalBasis( sRef, tRef, uRef, vRef, &phi4 );
  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phiTrue[k], phi4[k], tol );
  }

  // Compute true derivatives using finite difference (which is exact for linear)
  // Only compute them once as they are constant across a tetrahedra
  sRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, vRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phisTrue[k] = phi1[k] - phi[k];
  sRef -= 1;

  tRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, vRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phitTrue[k] = phi1[k] - phi[k];
  tRef -= 1;

  uRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, vRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phiuTrue[k] = phi1[k] - phi[k];
  uRef -= 1;

  vRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, vRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phivTrue[k] = phi1[k] - phi[k];
  vRef -= 1;

  qElem.evalBasisDerivative( sRef, tRef, uRef, vRef, phis, phit, phiu, phiv, NNode );
  qElem.evalBasisHessianDerivative( sRef, tRef, uRef, vRef,
      phiss, phist, phitt, phisu, phitu, phiuu, phisv, phitv, phiuv, phivv, NNode);
  qElem.evalBasisDerivative( sRef, tRef, uRef, vRef, &phis4, &phit4, &phiu4 , &phiv4 );
  qElem.evalBasisHessianDerivative( sRef, tRef, uRef, vRef,
      &phiss4, &phist4, &phitt4, &phisu4, &phitu4, &phiuu4, &phisv4, &phitv4, &phiuv4, &phivv4 );
  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
    BOOST_CHECK_CLOSE( phivTrue[k], phiv[k], tol );
    BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );
    BOOST_CHECK_CLOSE( phistTrue[k], phist[k], tol );
    BOOST_CHECK_CLOSE( phittTrue[k], phitt[k], tol );
    BOOST_CHECK_CLOSE( phisuTrue[k], phisu[k], tol );
    BOOST_CHECK_CLOSE( phituTrue[k], phitu[k], tol );
    BOOST_CHECK_CLOSE( phiuuTrue[k], phiuu[k], tol );
    BOOST_CHECK_CLOSE( phisvTrue[k], phisv[k], tol );
    BOOST_CHECK_CLOSE( phitvTrue[k], phitv[k], tol );
    BOOST_CHECK_CLOSE( phiuvTrue[k], phiuv[k], tol );
    BOOST_CHECK_CLOSE( phivvTrue[k], phivv[k], tol );

    BOOST_CHECK_CLOSE( phisTrue[k], phis4[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit4[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu4[k], tol );
    BOOST_CHECK_CLOSE( phivTrue[k], phiv4[k], tol );
    BOOST_CHECK_CLOSE( phissTrue[k], phiss4[k], tol );
    BOOST_CHECK_CLOSE( phistTrue[k], phist4[k], tol );
    BOOST_CHECK_CLOSE( phittTrue[k], phitt4[k], tol );
    BOOST_CHECK_CLOSE( phisuTrue[k], phisu4[k], tol );
    BOOST_CHECK_CLOSE( phituTrue[k], phitu4[k], tol );
    BOOST_CHECK_CLOSE( phiuuTrue[k], phiuu4[k], tol );
    BOOST_CHECK_CLOSE( phisvTrue[k], phisv4[k], tol );
    BOOST_CHECK_CLOSE( phitvTrue[k], phitv4[k], tol );
    BOOST_CHECK_CLOSE( phiuvTrue[k], phiuv4[k], tol );
    BOOST_CHECK_CLOSE( phivvTrue[k], phivv4[k], tol );
  }

  sRef = 1;  tRef = 0;  uRef = 0, vRef = 0;
  phiTrue[0] = 0;  phiTrue[1] = 1;  phiTrue[2] = 0;  phiTrue[3] = 0; phiTrue[4] = 0;

  qElem.evalBasis( sRef, tRef, uRef, vRef, phi, NNode );
  qElem.evalBasisDerivative( sRef, tRef, uRef, vRef, phis, phit, phiu, phiv, NNode );
  qElem.evalBasisHessianDerivative( sRef, tRef, uRef, vRef,
      phiss, phist, phitt, phisu, phitu, phiuu, phisv, phitv, phiuv, phivv, NNode);
  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
    BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );
    BOOST_CHECK_CLOSE( phistTrue[k], phist[k], tol );
    BOOST_CHECK_CLOSE( phittTrue[k], phitt[k], tol );
    BOOST_CHECK_CLOSE( phisuTrue[k], phisu[k], tol );
    BOOST_CHECK_CLOSE( phituTrue[k], phitu[k], tol );
    BOOST_CHECK_CLOSE( phiuuTrue[k], phiuu[k], tol );
  }


  sRef = 0;  tRef = 1;  uRef = 0, vRef = 0;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 1;  phiTrue[3] = 0; phiTrue[4] = 0;

  qElem.evalBasis( sRef, tRef, uRef, vRef, phi, NNode );
  qElem.evalBasisDerivative( sRef, tRef, uRef, vRef, phis, phit, phiu, phiv, NNode );
  qElem.evalBasisHessianDerivative( sRef, tRef, uRef, vRef,
      phiss, phist, phitt, phisu, phitu, phiuu, phisv, phitv, phiuv, phivv, NNode);
  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
    BOOST_CHECK_CLOSE( phivTrue[k], phiv[k], tol );
    BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );
    BOOST_CHECK_CLOSE( phistTrue[k], phist[k], tol );
    BOOST_CHECK_CLOSE( phittTrue[k], phitt[k], tol );
    BOOST_CHECK_CLOSE( phisuTrue[k], phisu[k], tol );
    BOOST_CHECK_CLOSE( phituTrue[k], phitu[k], tol );
    BOOST_CHECK_CLOSE( phiuuTrue[k], phiuu[k], tol );
    BOOST_CHECK_CLOSE( phisvTrue[k], phisv[k], tol );
    BOOST_CHECK_CLOSE( phitvTrue[k], phitv[k], tol );
    BOOST_CHECK_CLOSE( phiuvTrue[k], phiuv[k], tol );
    BOOST_CHECK_CLOSE( phivvTrue[k], phivv[k], tol );
  }

  sRef = 0;  tRef = 0;  uRef = 1, vRef = 0;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 0;  phiTrue[3] = 1; phiTrue[4] = 0;

  qElem.evalBasis( sRef, tRef, uRef, vRef, phi, NNode );
  qElem.evalBasisDerivative( sRef, tRef, uRef, vRef, phis, phit, phiu, phiv, NNode );
  qElem.evalBasisHessianDerivative( sRef, tRef, uRef, vRef,
      phiss, phist, phitt, phisu, phitu, phiuu, phisv, phitv, phiuv, phivv, NNode);
  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
    BOOST_CHECK_CLOSE( phivTrue[k], phiv[k], tol );
    BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );
    BOOST_CHECK_CLOSE( phistTrue[k], phist[k], tol );
    BOOST_CHECK_CLOSE( phittTrue[k], phitt[k], tol );
    BOOST_CHECK_CLOSE( phisuTrue[k], phisu[k], tol );
    BOOST_CHECK_CLOSE( phituTrue[k], phitu[k], tol );
    BOOST_CHECK_CLOSE( phiuuTrue[k], phiuu[k], tol );
    BOOST_CHECK_CLOSE( phisvTrue[k], phisv[k], tol );
    BOOST_CHECK_CLOSE( phitvTrue[k], phitv[k], tol );
    BOOST_CHECK_CLOSE( phiuvTrue[k], phiuv[k], tol );
    BOOST_CHECK_CLOSE( phivvTrue[k], phivv[k], tol );
  }

  sRef = 0;  tRef = 0;  uRef = 0, vRef = 1;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 0;  phiTrue[3] = 0; phiTrue[4] = 1;

  qElem.evalBasis( sRef, tRef, uRef, vRef, phi, NNode );
  qElem.evalBasisDerivative( sRef, tRef, uRef, vRef, phis, phit, phiu, phiv, NNode );
  qElem.evalBasisHessianDerivative( sRef, tRef, uRef, vRef,
      phiss, phist, phitt, phisu, phitu, phiuu, phisv, phitv, phiuv, phivv, NNode);
  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
    BOOST_CHECK_CLOSE( phivTrue[k], phiv[k], tol );
    BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );
    BOOST_CHECK_CLOSE( phistTrue[k], phist[k], tol );
    BOOST_CHECK_CLOSE( phittTrue[k], phitt[k], tol );
    BOOST_CHECK_CLOSE( phisuTrue[k], phisu[k], tol );
    BOOST_CHECK_CLOSE( phituTrue[k], phitu[k], tol );
    BOOST_CHECK_CLOSE( phiuuTrue[k], phiuu[k], tol );
    BOOST_CHECK_CLOSE( phisvTrue[k], phisv[k], tol );
    BOOST_CHECK_CLOSE( phitvTrue[k], phitv[k], tol );
    BOOST_CHECK_CLOSE( phiuvTrue[k], phiuv[k], tol );
    BOOST_CHECK_CLOSE( phivvTrue[k], phivv[k], tol );
  }

  sRef = 1./5.;  tRef = 1./5.;  uRef = 1./5., vRef = 1./5.;
  phiTrue[0] = 1./5.;  phiTrue[1] = 1./5.;  phiTrue[2] = 1./5.;  phiTrue[3] = 1./5.; phiTrue[4] = 1./5.;

  qElem.evalBasis( sRef, tRef, uRef, vRef, phi, NNode );
  qElem.evalBasisDerivative( sRef, tRef, uRef, vRef, phis, phit, phiu, phiv, NNode );
  qElem.evalBasisHessianDerivative( sRef, tRef, uRef, vRef,
      phiss, phist, phitt, phisu, phitu, phiuu, phisv, phitv, phiuv, phivv, NNode);
  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
    BOOST_CHECK_CLOSE( phivTrue[k], phiv[k], tol );
    BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );
    BOOST_CHECK_CLOSE( phistTrue[k], phist[k], tol );
    BOOST_CHECK_CLOSE( phittTrue[k], phitt[k], tol );
    BOOST_CHECK_CLOSE( phisuTrue[k], phisu[k], tol );
    BOOST_CHECK_CLOSE( phituTrue[k], phitu[k], tol );
    BOOST_CHECK_CLOSE( phiuuTrue[k], phiuu[k], tol );
    BOOST_CHECK_CLOSE( phisvTrue[k], phisv[k], tol );
    BOOST_CHECK_CLOSE( phitvTrue[k], phitv[k], tol );
    BOOST_CHECK_CLOSE( phiuvTrue[k], phiuv[k], tol );
    BOOST_CHECK_CLOSE( phivvTrue[k], phivv[k], tol );
  }
}


#if 0

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( eval_basis_P2 )
{
  typedef DLA::VectorS< 3, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD4, Pentatope > ElementClass;

  int order = 2;
  ElementClass qElem(order, BasisFunctionCategory_Lagrange);

  static const int nDOF = 15;

  BOOST_CHECK_EQUAL( order, qElem.order() );
  BOOST_CHECK_EQUAL( nDOF, qElem.nDOF() );

  const Real tol = 1e-13;
  Real sRef, tRef, uRef, vRef;
  Real phi[nDOF], phis[nDOF], phit[nDOF], phiu[nDOF];
  Real phiTrue[nDOF], phisTrue[nDOF], phitTrue[nDOF], phiuTrue[nDOF];
  int k;

  sRef = 0;  tRef = 0;  uRef = 0; vRef = 0;
  phiTrue[0] = 1;   phiTrue[1] = 0;   phiTrue[2] = 0;   phiTrue[3] = 0;   phiTrue[4] = 0;
  phiTrue[5] = 0;   phiTrue[6] = 0;   phiTrue[7] = 0;   phiTrue[8] = 0;   phiTrue[9] = 0;
  phiTrue[10] = 0;  phiTrue[11] = 0;  phiTrue[12] = 0;  phiTrue[13] = 0;  phiTrue[14] = 0;

  phisTrue[0] =-1;  phisTrue[1] = 1;  phisTrue[2] = 0;  phisTrue[3] = 0;  phisTrue[4] = 0;
  phisTrue[5] = 0;  phisTrue[6] = 0;  phisTrue[7] = 0;  phisTrue[8] = 0;  phisTrue[9] = 4;

  phitTrue[0] =-1;  phitTrue[1] = 0;  phitTrue[2] = 1;  phitTrue[3] = 0;  phitTrue[4] = 0;
  phitTrue[5] = 0;  phitTrue[6] = 0;  phitTrue[7] = 4;  phitTrue[8] = 0;  phitTrue[9] = 0;

  phiuTrue[0] =-1;  phiuTrue[1] = 0;  phiuTrue[2] = 0;  phiuTrue[3] = 1;  phiuTrue[4] = 0;
  phiuTrue[5] = 0;  phiuTrue[6] = 0;  phiuTrue[7] = 0;  phiuTrue[8] = 4;  phiuTrue[9] = 0;

  qElem.evalBasis( sRef, tRef, uRef, phi, nDOF );
  for (k = 0; k < nDOF; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  qElem.evalBasisDerivative( sRef, tRef, uRef, phis, phit, phiu, nDOF );
  for (k = 0; k < nDOF; k++)
  {
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
  }


  sRef = 1;  tRef = 0;  uRef = 0;
  phiTrue[0] = 0;  phiTrue[1] = 1;  phiTrue[2] = 0;  phiTrue[3] = 0;  phiTrue[4] = 0;
  phiTrue[5] = 0;  phiTrue[6] = 0;  phiTrue[7] = 0;  phiTrue[8] = 0;  phiTrue[9] = 0;

  phisTrue[0] =-1;  phisTrue[1] = 1;  phisTrue[2] = 0;  phisTrue[3] = 0;  phisTrue[4] = 0;
  phisTrue[5] = 0;  phisTrue[6] = 0;  phisTrue[7] = 0;  phisTrue[8] = 0;  phisTrue[9] =-4;

  phitTrue[0] =-1;  phitTrue[1] = 0;  phitTrue[2] = 1;  phitTrue[3] = 0;  phitTrue[4] = 0;
  phitTrue[5] = 0;  phitTrue[6] = 4;  phitTrue[7] = 0;  phitTrue[8] = 0;  phitTrue[9] =-4;

  phiuTrue[0] =-1;  phiuTrue[1] = 0;  phiuTrue[2] = 0;  phiuTrue[3] = 1;  phiuTrue[4] = 0;
  phiuTrue[5] = 4;  phiuTrue[6] = 0;  phiuTrue[7] = 0;  phiuTrue[8] = 0;  phiuTrue[9] =-4;

  qElem.evalBasis( sRef, tRef, uRef, phi, nDOF );
  qElem.evalBasisDerivative( sRef, tRef, uRef, phis, phit, phiu, nDOF );
  for (k = 0; k < nDOF; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
  }

  sRef = 0;  tRef = 1;  uRef = 0;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 1;  phiTrue[3] = 0;  phiTrue[4] = 0;
  phiTrue[5] = 0;  phiTrue[6] = 0;  phiTrue[7] = 0;  phiTrue[8] = 0;  phiTrue[9] = 0;

  phisTrue[0] =-1;  phisTrue[1] = 1;  phisTrue[2] = 0;  phisTrue[3] = 0;  phisTrue[4] = 0;
  phisTrue[5] = 0;  phisTrue[6] = 4;  phisTrue[7] =-4;  phisTrue[8] = 0;  phisTrue[9] = 0;

  phitTrue[0] =-1;  phitTrue[1] = 0;  phitTrue[2] = 1;  phitTrue[3] = 0;  phitTrue[4] = 0;
  phitTrue[5] = 0;  phitTrue[6] = 0;  phitTrue[7] =-4;  phitTrue[8] = 0;  phitTrue[9] = 0;

  phiuTrue[0] =-1;  phiuTrue[1] = 0;  phiuTrue[2] = 0;  phiuTrue[3] = 1;  phiuTrue[4] = 4;
  phiuTrue[5] = 0;  phiuTrue[6] = 0;  phiuTrue[7] =-4;  phiuTrue[8] = 0;  phiuTrue[9] = 0;

  qElem.evalBasis( sRef, tRef, uRef, phi, nDOF );
  qElem.evalBasisDerivative( sRef, tRef, uRef, phis, phit, phiu, nDOF );
  for (k = 0; k < nDOF; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
  }

  sRef = 0;  tRef = 0;  uRef = 1;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 0;  phiTrue[3] = 1;  phiTrue[4] = 0;
  phiTrue[5] = 0;  phiTrue[6] = 0;  phiTrue[7] = 0;  phiTrue[8] = 0;  phiTrue[9] = 0;

  phisTrue[0] =-1;  phisTrue[1] = 1;  phisTrue[2] = 0;  phisTrue[3] = 0;  phisTrue[4] = 0;
  phisTrue[5] = 4;  phisTrue[6] = 0;  phisTrue[7] = 0;  phisTrue[8] =-4;  phisTrue[9] = 0;

  phitTrue[0] =-1;  phitTrue[1] = 0;  phitTrue[2] = 1;  phitTrue[3] = 0;  phitTrue[4] = 4;
  phitTrue[5] = 0;  phitTrue[6] = 0;  phitTrue[7] = 0;  phitTrue[8] =-4;  phitTrue[9] = 0;

  phiuTrue[0] =-1;  phiuTrue[1] = 0;  phiuTrue[2] = 0;  phiuTrue[3] = 1;  phiuTrue[4] = 0;
  phiuTrue[5] = 0;  phiuTrue[6] = 0;  phiuTrue[7] = 0;  phiuTrue[8] =-4;  phiuTrue[9] = 0;

  qElem.evalBasis( sRef, tRef, uRef, phi, nDOF );
  qElem.evalBasisDerivative( sRef, tRef, uRef, phis, phit, phiu, nDOF );
  for (k = 0; k < nDOF; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
  }

  sRef = 1./4.;  tRef = 1./4.;  uRef = 1./4.;
  phiTrue[0] = 0.25;  phiTrue[1] = 0.25;  phiTrue[2] = 0.25;  phiTrue[3] = 0.25;  phiTrue[4] = 0.25;
  phiTrue[5] = 0.25;  phiTrue[6] = 0.25;  phiTrue[7] = 0.25;  phiTrue[8] = 0.25;  phiTrue[9] = 0.25;

  phisTrue[0] =-1;  phisTrue[1] = 1;  phisTrue[2] = 0;  phisTrue[3] = 0;  phisTrue[4] = 0;
  phisTrue[5] = 1;  phisTrue[6] = 1;  phisTrue[7] =-1;  phisTrue[8] =-1;  phisTrue[9] = 0;

  phitTrue[0] =-1;  phitTrue[1] = 0;  phitTrue[2] = 1;  phitTrue[3] = 0;  phitTrue[4] = 1;
  phitTrue[5] = 0;  phitTrue[6] = 1;  phitTrue[7] = 0;  phitTrue[8] =-1;  phitTrue[9] =-1;

  phiuTrue[0] =-1;  phiuTrue[1] = 0;  phiuTrue[2] = 0;  phiuTrue[3] = 1;  phiuTrue[4] = 1;
  phiuTrue[5] = 1;  phiuTrue[6] = 0;  phiuTrue[7] =-1;  phiuTrue[8] = 0;  phiuTrue[9] =-1;

  qElem.evalBasis( sRef, tRef, uRef, phi, nDOF );
  qElem.evalBasisDerivative( sRef, tRef, uRef, phis, phit, phiu, nDOF );
  for (k = 0; k < nDOF; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
  }
}

#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( eval_variable )
{
  typedef DLA::VectorS< 3, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD4, Pentatope > ElementClass;

  static const int NNode = Pentatope::NNode;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( 5, qElem.nDOF() );

  const Real tol = 1e-13;
  Real sRef, tRef, uRef, vRef;
  Real phi[NNode];
  Real phiTrue[NNode];
  ArrayQ q, qTrue;                            // variable
  ArrayQ qs, qt, qu, qv, qsTrue, qtTrue, quTrue, qvTrue;  // variable derivatives wrt sRef
  int k;

  ArrayQ q1 = {1, 2, 3};
  ArrayQ q2 = {4, 5, 6};
  ArrayQ q3 = {7, 8, 9};
  ArrayQ q4 = {10, 11, 12};
  ArrayQ q5 = {13, 14, 15};

  qElem.DOF(0) = q1;
  qElem.DOF(1) = q2;
  qElem.DOF(2) = q3;
  qElem.DOF(3) = q4;
  qElem.DOF(4) = q5;

  // test @ (s,t,u) = (0,0,0)
  sRef = 0;  tRef = 0;  uRef = 0; vRef = 0;
  phiTrue[0] = 1;  phiTrue[1] = 0;  phiTrue[2] = 0;  phiTrue[3] = 0; phiTrue[4] = 0;
  qTrue = q1;
  qsTrue = q2 - q1;
  qtTrue = q3 - q1;
  quTrue = q4 - q1;
  qvTrue = q5 - q1;

  qElem.evalBasis( sRef, tRef, uRef, vRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  qElem.evalDerivative( sRef, tRef, uRef, vRef, qs, qt, qu, qv );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  for (k = 0; k < 3; k++)
  {
    BOOST_CHECK_CLOSE( qTrue[k],  q[k],  tol );
    BOOST_CHECK_CLOSE( qsTrue[k], qs[k], tol );
    BOOST_CHECK_CLOSE( qtTrue[k], qt[k], tol );
    BOOST_CHECK_CLOSE( quTrue[k], qu[k], tol );
    BOOST_CHECK_CLOSE( qvTrue[k], qv[k], tol );
  }

  q = 0; // cppcheck-suppress redundantAssignment
  q = qElem.eval( sRef, tRef, uRef, vRef );
  for (k = 0; k < 3; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );

  // test @ (s,t,u,v) = (1,0,0,0)
  sRef = 1;  tRef = 0;  uRef = 0; vRef = 0;
  phiTrue[0] = 0;  phiTrue[1] = 1;  phiTrue[2] = 0;  phiTrue[3] = 0; phiTrue[4] = 0;
  qTrue = q2;
  qsTrue = q2 - q1;
  qtTrue = q3 - q1;
  quTrue = q4 - q1;
  qvTrue = q5 - q1;

  qElem.evalBasis( sRef, tRef, uRef, vRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  qElem.evalDerivative( sRef, tRef, uRef, vRef, qs, qt, qu, qv );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  for (k = 0; k < 3; k++)
  {
    BOOST_CHECK_CLOSE( qTrue[k],  q[k],  tol );
    BOOST_CHECK_CLOSE( qsTrue[k], qs[k], tol );
    BOOST_CHECK_CLOSE( qtTrue[k], qt[k], tol );
    BOOST_CHECK_CLOSE( quTrue[k], qu[k], tol );
    BOOST_CHECK_CLOSE( qvTrue[k], qv[k], tol );
  }

  q = 0; // cppcheck-suppress redundantAssignment
  q = qElem.eval( sRef, tRef, uRef, vRef );
  for (k = 0; k < 3; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );

  // test @ (s,t,u,v) = (0,1,0,0)
  sRef = 0;  tRef = 1;  uRef = 0; vRef = 0;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 1;  phiTrue[3] = 0; phiTrue[4] = 0;
  qTrue = q3;
  qsTrue = q2 - q1;
  qtTrue = q3 - q1;
  quTrue = q4 - q1;
  qvTrue = q5 - q1;

  qElem.evalBasis( sRef, tRef, uRef, vRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  qElem.evalDerivative( sRef, tRef, uRef, vRef, qs, qt, qu, qv );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  for (k = 0; k < 3; k++)
  {
    BOOST_CHECK_CLOSE( qTrue[k],  q[k],  tol );
    BOOST_CHECK_CLOSE( qsTrue[k], qs[k], tol );
    BOOST_CHECK_CLOSE( qtTrue[k], qt[k], tol );
    BOOST_CHECK_CLOSE( quTrue[k], qu[k], tol );
    BOOST_CHECK_CLOSE( qvTrue[k], qv[k], tol );
  }

  q = 0; // cppcheck-suppress redundantAssignment
  q = qElem.eval( sRef, tRef, uRef, vRef );
  for (k = 0; k < 3; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );


  // test @ (s,t,u,v) = (0,0,1,0)
  sRef = 0;  tRef = 0;  uRef = 1; vRef = 0;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 0;  phiTrue[3] = 1; phiTrue[4] = 0;
  qTrue = q4;
  qsTrue = q2 - q1;
  qtTrue = q3 - q1;
  quTrue = q4 - q1;
  qvTrue = q5 - q1;

  qElem.evalBasis( sRef, tRef, uRef, vRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  qElem.evalDerivative( sRef, tRef, uRef, vRef, qs, qt, qu, qv );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  for (k = 0; k < 3; k++)
  {
    BOOST_CHECK_CLOSE( qTrue[k],  q[k],  tol );
    BOOST_CHECK_CLOSE( qsTrue[k], qs[k], tol );
    BOOST_CHECK_CLOSE( qtTrue[k], qt[k], tol );
    BOOST_CHECK_CLOSE( quTrue[k], qu[k], tol );
    BOOST_CHECK_CLOSE( qvTrue[k], qv[k], tol );
  }

  // test @ (s,t,u,v) = (0,0,0,1)
  sRef = 0;  tRef = 0;  uRef = 0; vRef = 1;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 0;  phiTrue[3] = 0; phiTrue[4] = 1;
  qTrue = q5;
  qsTrue = q2 - q1;
  qtTrue = q3 - q1;
  quTrue = q4 - q1;
  qvTrue = q5 - q1;

  qElem.evalBasis( sRef, tRef, uRef, vRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  qElem.evalDerivative( sRef, tRef, uRef, vRef, qs, qt, qu, qv );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  for (k = 0; k < 3; k++)
  {
    BOOST_CHECK_CLOSE( qTrue[k],  q[k],  tol );
    BOOST_CHECK_CLOSE( qsTrue[k], qs[k], tol );
    BOOST_CHECK_CLOSE( qtTrue[k], qt[k], tol );
    BOOST_CHECK_CLOSE( quTrue[k], qu[k], tol );
    BOOST_CHECK_CLOSE( qvTrue[k], qv[k], tol );
  }

  q = 0; // cppcheck-suppress redundantAssignment
  q = qElem.eval( sRef, tRef, uRef, vRef );
  for (k = 0; k < 3; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );


  // test @ (s,t,u,v) = (1/5,1/5,1/5,1/5)
  sRef = 1./5.;  tRef = 1./5.;  uRef = 1./5.; vRef = 1./5.;
  phiTrue[0] = 1./5.;  phiTrue[1] = 1./5.;  phiTrue[2] = 1./5.;  phiTrue[3] = 1./5.; phiTrue[4] = 1./5.;
  qTrue = (q1 + q2 + q3 + q4 +q5)/5.;
  qsTrue = q2 - q1;
  qtTrue = q3 - q1;
  quTrue = q4 - q1;
  qvTrue = q5 - q1;

  qElem.evalBasis( sRef, tRef, uRef, vRef , phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  qElem.evalDerivative( sRef, tRef, uRef, vRef , qs, qt, qu, qv );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  for (k = 0; k < 3; k++)
  {
    BOOST_CHECK_CLOSE( qTrue[k],  q[k],  tol );
    BOOST_CHECK_CLOSE( qsTrue[k], qs[k], tol );
    BOOST_CHECK_CLOSE( qtTrue[k], qt[k], tol );
    BOOST_CHECK_CLOSE( quTrue[k], qu[k], tol );
    BOOST_CHECK_CLOSE( qvTrue[k], qv[k], tol );
  }

  q = 0; // cppcheck-suppress redundantAssignment
  q = qElem.eval( sRef, tRef, uRef, vRef );
  for (k = 0; k < 3; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( cached_basis )
{
  typedef DLA::VectorS< 3, Real > ArrayQ;
  typedef DLA::VectorS< TopoD4::D, ArrayQ > VectorArrayQ;
  typedef DLA::VectorS< TopoD4::D, Real > RefCoordCell;
  typedef DLA::VectorS< TopoD3::D, Real > RefCoordTrace;
  typedef Element< ArrayQ, TopoD4, Pentatope > ElementClass;
  typedef std::array<int,Pentatope::NTrace> IntTrace;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-13;

  RefCoordCell ref;
  ArrayQ q, qTrue;
  VectorArrayQ derivq, derivqTrue;

  const int nFaceSign = 1;

  // TODO: Not sure how the face sign work out in 4D, so this is just dummy now.
  IntTrace faceSigns[nFaceSign] = { {{ 1, 1, 1, 1, 1}} };

  for (int isgn = 0; isgn < nFaceSign; isgn++)
  {
    IntTrace sgn = faceSigns[isgn];

    // up to 2 because this is really slow
    for (int order = 1; order <= 2; order++)
    {
      ElementClass qElem(order, BasisFunctionCategory_Lagrange);
      qElem.setFaceSign(sgn);

      int nDOF = qElem.nDOF();

      // initalize some arbitrary values
      for (int n = 0; n < nDOF; n++)
        qElem.DOF(n) = pow(-1, n) / sqrt(n+1);

      std::vector<Real> phiTrue(nDOF), phisTrue(nDOF), phitTrue(nDOF), phiuTrue(nDOF), phivTrue(nDOF);
      std::vector<Real> phissTrue(nDOF),
                        phistTrue(nDOF), phittTrue(nDOF),
                        phisuTrue(nDOF), phituTrue(nDOF), phiuuTrue(nDOF),
                        phisvTrue(nDOF), phitvTrue(nDOF), phiuvTrue(nDOF), phivvTrue(nDOF);
      std::vector<Real> phi(nDOF), phis(nDOF), phit(nDOF), phiu(nDOF), phiv(nDOF);
      std::vector<Real> phiss(nDOF),
                        phist(nDOF), phitt(nDOF),
                        phisu(nDOF), phitu(nDOF), phiuu(nDOF),
                        phisv(nDOF), phitv(nDOF), phiuv(nDOF), phivv(nDOF);

      // check that the cached basis functions work
      for (int iorderidx = 0; iorderidx < QuadratureSpacetime<Pentatope>::nOrderIdx; iorderidx++)
      {
        QuadratureSpacetime<Pentatope> quadrature( QuadratureSpacetime<Pentatope>::getOrderFromIndex(iorderidx) );
        for (int n = 0; n < quadrature.nQuadrature(); n++)
        {
          quadrature.coordinates(n, ref);
          QuadraturePoint<TopoD4> point = quadrature.coordinates_cache(n);

          //--------------
          qElem.evalBasis( ref, phiTrue.data(), phiTrue.size() );
          qElem.evalBasis( point, phi.data(), phi.size() );

          for (int k = 0; k < nDOF; k++)
          {
            BOOST_CHECK_CLOSE( phiTrue[k], phi[k], close_tol );
            phi[k] = 0;
          }

          // 'non-quadrature' coordinate (mainly used with testing of integrands)
          qElem.evalBasis( QuadraturePoint<TopoD4>(ref), phi.data(), phi.size() );

          for (int k = 0; k < nDOF; k++)
            BOOST_CHECK_CLOSE( phiTrue[k], phi[k], close_tol );


          //--------------
          qElem.evalBasisDerivative( ref[0], ref[1], ref[2], ref[3],
                                     phisTrue.data(), phitTrue.data(), phiuTrue.data(), phivTrue.data(), phisTrue.size() );
          qElem.evalBasisDerivative( point, phis.data(), phit.data(), phiu.data(), phiv.data(), phis.size() );

          for (int k = 0; k < nDOF; k++)
          {
            BOOST_CHECK_CLOSE( phisTrue[k], phis[k], close_tol );
            BOOST_CHECK_CLOSE( phitTrue[k], phit[k], close_tol );
            BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], close_tol );
            BOOST_CHECK_CLOSE( phivTrue[k], phiv[k], close_tol );
            phis[k] = 0;
            phit[k] = 0;
            phiu[k] = 0;
            phiv[k] = 0;
          }

          // 'non-quadrature' coordinate (mainly used with testing of integrands)
          qElem.evalBasisDerivative( QuadraturePoint<TopoD4>(ref), phis.data(), phit.data(), phiu.data(), phiv.data(), phis.size() );

          for (int k = 0; k < nDOF; k++)
          {
            BOOST_CHECK_CLOSE( phisTrue[k], phis[k], close_tol );
            BOOST_CHECK_CLOSE( phitTrue[k], phit[k], close_tol );
            BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], close_tol );
            BOOST_CHECK_CLOSE( phivTrue[k], phiv[k], close_tol );
          }


          //--------------
          qElem.evalBasisHessianDerivative( ref[0], ref[1], ref[2], ref[3],
                                            phissTrue.data(),
                                            phistTrue.data(), phittTrue.data(),
                                            phisuTrue.data(), phituTrue.data(), phiuuTrue.data(),
                                            phisvTrue.data(), phitvTrue.data(), phiuvTrue.data(), phivvTrue.data(), phissTrue.size() );
          qElem.evalBasisHessianDerivative( point,
                                            phiss.data(),
                                            phist.data(), phitt.data(),
                                            phisu.data(), phitu.data(), phiuu.data(),
                                            phisv.data(), phitv.data(), phiuv.data(), phivv.data(), phiss.size() );

          for (int k = 0; k < nDOF; k++)
          {
            SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phisuTrue[k], phisu[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phituTrue[k], phitu[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phiuuTrue[k], phiuu[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phisvTrue[k], phisv[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phitvTrue[k], phitv[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phiuvTrue[k], phiuv[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phivvTrue[k], phivv[k], small_tol, close_tol );
            phiss[k] = 0;
            phist[k] = 0; phitt[k] = 0;
            phisu[k] = 0; phitu[k] = 0; phiuu[k] = 0;
            phisv[k] = 0; phitv[k] = 0; phiuv[k] = 0; phivv[k] = 0;
          }

          // 'non-quadrature' coordinate (mainly used with testing of integrands)
          qElem.evalBasisHessianDerivative( QuadraturePoint<TopoD4>(ref),
                                            phiss.data(),
                                            phist.data(), phitt.data(),
                                            phisu.data(), phitu.data(), phiuu.data(),
                                            phisv.data(), phitv.data(), phiuv.data(), phivv.data(), phiss.size() );

          for (int k = 0; k < nDOF; k++)
          {
            SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phisuTrue[k], phisu[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phituTrue[k], phitu[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phiuuTrue[k], phiuu[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phisvTrue[k], phisv[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phitvTrue[k], phitv[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phiuvTrue[k], phiuv[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phivvTrue[k], phivv[k], small_tol, close_tol );
          }


          //--------------
          qElem.eval( ref, qTrue );
          qElem.eval( point, q );

          for (int k = 0; k < ArrayQ::M; k++)
            BOOST_CHECK_CLOSE( qTrue[k], q[k], close_tol );

          // 'non-quadrature' coordinate (mainly used with testing of integrands)
          qElem.eval( QuadraturePoint<TopoD4>(ref), q );

          for (int k = 0; k < ArrayQ::M; k++)
            BOOST_CHECK_CLOSE( qTrue[k], q[k], close_tol );


          //--------------
          qElem.evalDerivative( ref, derivqTrue );
          qElem.evalDerivative( point, derivq );

          for (int k = 0; k < ArrayQ::M; k++)
          {
            BOOST_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], close_tol );
            BOOST_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], close_tol );
            BOOST_CHECK_CLOSE( derivqTrue[2][k], derivq[2][k], close_tol );
            BOOST_CHECK_CLOSE( derivqTrue[3][k], derivq[3][k], close_tol );
          }

          // 'non-quadrature' coordinate (mainly used with testing of integrands)
          qElem.evalDerivative( QuadraturePoint<TopoD4>(ref), derivq );

          for (int k = 0; k < ArrayQ::M; k++)
          {
            BOOST_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], close_tol );
            BOOST_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], close_tol );
            BOOST_CHECK_CLOSE( derivqTrue[2][k], derivq[2][k], close_tol );
            BOOST_CHECK_CLOSE( derivqTrue[3][k], derivq[3][k], close_tol );
          }

        } // iquad
      } // orderidx


      for (int itrace = 0; itrace < Pentatope::NTrace; itrace++)
      {
        //for (int orientation : {-12,-11,-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,1})
        for (int orientation : {-1,1})
        {
          CanonicalTraceToCell canonicalTrace(itrace, orientation);

          // relax tolerances for non-canonical traces
          Real small_tol = 1e-12;
          Real close_tol = 1e-13;
          if (orientation != 1)
          {
            small_tol = 1e-11;
            close_tol = 2e-8;
          }

          for (int orderidx = 0; orderidx < QuadratureVolume<Tet>::nOrderIdx; orderidx++)
          {
            QuadratureVolume<Tet> quadrature( QuadratureVolume<Tet>::getOrderFromIndex(orderidx) );

            RefCoordTrace sTrace;
            RefCoordCell refCell;
            QuadratureCellTracePoint<TopoD4> pointCell, pointCell2;
            for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
            {
              quadrature.coordinates(iquad, sTrace);
              QuadraturePoint<TopoD3> pointTrace = quadrature.coordinates_cache(iquad);

              TraceToCellRefCoord<Tet, TopoD4, Pentatope>::eval( canonicalTrace, sTrace, refCell );
              TraceToCellRefCoord<Tet, TopoD4, Pentatope>::eval( canonicalTrace, pointTrace, pointCell );
              TraceToCellRefCoord<Tet, TopoD4, Pentatope>::eval( canonicalTrace, QuadraturePoint<TopoD3>(sTrace), pointCell2 );

              SANS_CHECK_CLOSE( refCell[0], pointCell.ref[0], small_tol, close_tol );
              SANS_CHECK_CLOSE( refCell[1], pointCell.ref[1], small_tol, close_tol );
              SANS_CHECK_CLOSE( refCell[2], pointCell.ref[2], small_tol, close_tol );
              SANS_CHECK_CLOSE( refCell[3], pointCell.ref[3], small_tol, close_tol );

              //--------------
              qElem.evalBasis( refCell, phiTrue.data(), phiTrue.size() );
              qElem.evalBasis( pointCell, phi.data(), phi.size() );

              for (int k = 0; k < nDOF; k++)
              {
                SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
                phi[k] = 0;
              }


              // 'non-quadrature' coordinate (mainly used with testing of integrands)
              qElem.evalBasis( pointCell2, phi.data(), phi.size() );

              for (int k = 0; k < nDOF; k++)
                SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );

              //--------------
              qElem.evalBasisDerivative( refCell[0], refCell[1], refCell[2], refCell[3] ,
                                         phisTrue.data(), phitTrue.data(), phiuTrue.data(), phivTrue.data(), phisTrue.size() );
              qElem.evalBasisDerivative( pointCell, phis.data(), phit.data(), phiu.data(), phiv.data(), phis.size() );

              for (int k = 0; k < nDOF; k++)
              {
                SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
                SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
                SANS_CHECK_CLOSE( phiuTrue[k], phiu[k], small_tol, close_tol );
                SANS_CHECK_CLOSE( phivTrue[k], phiv[k], small_tol, close_tol );
                phis[k] = 0;
                phit[k] = 0;
                phiu[k] = 0;
                phiv[k] = 0;
              }

              // 'non-quadrature' coordinate (mainly used with testing of integrands)
              qElem.evalBasisDerivative( pointCell2, phis.data(), phit.data(), phiu.data(), phiv.data(), phis.size() );

              for (int k = 0; k < nDOF; k++)
              {
                SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
                SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
                SANS_CHECK_CLOSE( phiuTrue[k], phiu[k], small_tol, close_tol );
                SANS_CHECK_CLOSE( phivTrue[k], phiv[k], small_tol, close_tol );
              }

              //--------------
              qElem.eval( refCell, qTrue );
              qElem.eval( pointCell, q );

              for (int k = 0; k < ArrayQ::M; k++)
                SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );

              // 'non-quadrature' coordinate (mainly used with testing of integrands)
              qElem.eval( pointCell2, q );

              for (int k = 0; k < ArrayQ::M; k++)
                SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );


              //--------------
              qElem.evalDerivative( refCell, derivqTrue );
              qElem.evalDerivative( pointCell, derivq );

              for (int k = 0; k < ArrayQ::M; k++)
              {
                SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
                SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
                SANS_CHECK_CLOSE( derivqTrue[2][k], derivq[2][k], small_tol, close_tol );
                SANS_CHECK_CLOSE( derivqTrue[3][k], derivq[3][k], small_tol, close_tol );
              }

              // 'non-quadrature' coordinate (mainly used with testing of integrands)
              qElem.evalDerivative( pointCell2, derivq );

              for (int k = 0; k < ArrayQ::M; k++)
              {
                SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
                SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
                SANS_CHECK_CLOSE( derivqTrue[2][k], derivq[2][k], small_tol, close_tol );
                SANS_CHECK_CLOSE( derivqTrue[3][k], derivq[3][k], small_tol, close_tol );
              }

            } //iquad
          } // orderidx
        } // orientation
      } // itrace

    } // order
  } // isgn
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( extract_basis )
{
  typedef DLA::VectorS< 3, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD4, Pentatope > ElementClass;

  int order = 1;
  ElementClass qElem_P1(order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL( 1, qElem_P1.order() );
  BOOST_CHECK_EQUAL( 5, qElem_P1.nDOF() );

  const BasisFunctionSpacetimeBase<Pentatope>* basis_P1 = qElem_P1.basis();

  BOOST_CHECK_EQUAL( 1, basis_P1->order() );
  BOOST_CHECK_EQUAL( 5, basis_P1->nBasis() );

  order = 2;
  ElementClass qElem_P2(order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL( 2, qElem_P2.order() );
  BOOST_CHECK_EQUAL( 15, qElem_P2.nDOF() );

  const BasisFunctionSpacetimeBase<Pentatope>* basis_P2 = qElem_P2.basis();

  BOOST_CHECK_EQUAL( 2, basis_P2->order() );
  BOOST_CHECK_EQUAL( 15, basis_P2->nBasis() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( edge_sign )
{
  typedef DLA::VectorS< 3, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD4, Pentatope > ElementClass;
  typedef std::array<int,5> Int5;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Lagrange);

  Int5 faceSign = qElem.faceSign();
  BOOST_CHECK_EQUAL( +1, faceSign[0] );
  BOOST_CHECK_EQUAL( +1, faceSign[1] );
  BOOST_CHECK_EQUAL( +1, faceSign[2] );
  BOOST_CHECK_EQUAL( +1, faceSign[3] );
  BOOST_CHECK_EQUAL( +1, faceSign[4] );

  faceSign[1] = -1;
  qElem.setFaceSign(faceSign);

  Int5 faceSign2 = qElem.faceSign();
  BOOST_CHECK_EQUAL( +1, faceSign2[0] );
  BOOST_CHECK_EQUAL( -1, faceSign2[1] );
  BOOST_CHECK_EQUAL( +1, faceSign2[2] );
  BOOST_CHECK_EQUAL( +1, faceSign2[3] );
  BOOST_CHECK_EQUAL( +1, faceSign2[4] );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( projection )
{
  typedef DLA::VectorS< 3, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD4, Pentatope > ElementClass;

  // small tolerance here because of numerical error
  const Real tol = 5e-8;

  for (int order = 1; order < BasisFunctionSpacetime_Pentatope_LagrangePMax; order++)
  {
    for (int inc = 1; inc <= BasisFunctionSpacetime_Pentatope_LagrangePMax-order; inc++)
    {
      ElementClass qElem0( order,       BasisFunctionCategory_Lagrange );
      ElementClass qElem1( order + inc, BasisFunctionCategory_Lagrange );

      for (int k = 0; k < qElem0.nDOF(); k++)
      {
        ArrayQ q = {sqrt(k+7), 1./(k+3),2./(1.+0.2*k)};
        qElem0.DOF(k) = q;
      }

      qElem0.projectTo( qElem1 );

      ArrayQ q0, q1;
      Real s, t, u, v;

      for (int is = 0; is < 4; is++)
      {
        for (int it = 0; it < 4; it++)
        {
          for (int iu = 0; iu < 4; iu++)
          {
            for (int iv = 0; iv < 4; iv++)
            {
              s = 0.11 + is*0.3357;
              t = 0.13 + it*0.2986;
              u = 0.15 + iu*0.2629;
              v = 0.17 + iv*0.2314;

              qElem0.eval( s, t, u, v, q0 );
              qElem1.eval( s, t, u, v, q1 );

              for (int n = 0; n < ArrayQ::M; n++)
                BOOST_CHECK_CLOSE( q0[n], q1[n], tol );
            }
          }
        }
      }
    }
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  typedef DLA::VectorS< 3, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD4, Pentatope > ElementClass;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/ElementSpacetime_Pentatope_pattern.txt", true );

  ElementClass qElem(1, BasisFunctionCategory_Lagrange);

  ArrayQ q1 = {1, 2, 3};
  ArrayQ q2 = {4, 5, 6};
  ArrayQ q3 = {7, 8, 9};
  ArrayQ q4 = {10, 11, 12};
  ArrayQ q5 = {13, 14, 15};

  qElem.DOF(0) = q1;
  qElem.DOF(1) = q2;
  qElem.DOF(2) = q3;
  qElem.DOF(3) = q4;
  qElem.DOF(4) = q5;

  qElem.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
