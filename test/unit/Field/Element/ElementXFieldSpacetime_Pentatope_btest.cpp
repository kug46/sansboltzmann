// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementXFieldVolume_Tetrahedron_btest
// testing of ElementXFieldVolume class w/ tetrahedron

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "tools/stringify.h"     // Real

#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"

#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"
#include "Field/Element/ElementXFieldSpacetime.h"

#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/BasisFunctionSpacetime_Pentatope.h"
#include "BasisFunction/ElementEdges.h"
#include "BasisFunction/TraceToCellRefCoord.h"

using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class ElementXField<PhysD4,TopoD4,Pentatope>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementXFieldSpacetime_Pentatope_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK( (ElementXField<PhysD4,TopoD4,Pentatope>::D == 4) );
}

//Not sure how to test a 4D tetrahedron yet
typedef boost::mpl::list< PhysD4 > Dimensions;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctors, PhysDim, Dimensions )
{
  int order;

  order = 1;
  ElementXField<PhysDim,TopoD4,Pentatope> xElem_P1_1(order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL( 1, xElem_P1_1.order() );
  BOOST_CHECK_EQUAL( 5, xElem_P1_1.nDOF() );

  order = 2;
  ElementXField<PhysDim,TopoD4,Pentatope> xElem_P2_1(order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL( 2, xElem_P2_1.order() );
  BOOST_CHECK_EQUAL( 15, xElem_P2_1.nDOF() );

  ElementXField<PhysDim,TopoD4,Pentatope> xElem_P2_2( BasisFunctionSpacetimeBase<Pentatope>::LagrangeP2 );

  BOOST_CHECK_EQUAL( 2, xElem_P2_2.order() );
  BOOST_CHECK_EQUAL( 15, xElem_P2_2.nDOF() );

  ElementXField<PhysDim,TopoD4,Pentatope> xElem_P2_3(xElem_P1_1);

  BOOST_CHECK_EQUAL( 1, xElem_P2_3.order() );
  BOOST_CHECK_EQUAL( 5, xElem_P2_3.nDOF() );

  xElem_P2_3 = xElem_P2_1;

  BOOST_CHECK_EQUAL( 2, xElem_P2_3.order() );
  BOOST_CHECK_EQUAL( 15, xElem_P2_3.nDOF() );

  typedef ElementXField<PhysDim,TopoD4,Pentatope> ElementType;
  BOOST_CHECK_THROW( ElementType xElem_P0(0, BasisFunctionCategory_Lagrange), DeveloperException );
  BOOST_CHECK_THROW( ElementType xElem_pmax(BasisFunctionSpacetime_Pentatope_LagrangePMax+1,
                                       BasisFunctionCategory_Lagrange), DeveloperException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( accessors, PhysDim, Dimensions )
{
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysDim,TopoD4,Pentatope> xElem(order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 5, xElem.nDOF() );

  typename ElementXField<PhysDim,TopoD4,Pentatope>::VectorX X1, X2, X3, X4, X5;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    X1[i] = i+1;
    X2[i] = 2*i;
    X3[i] = 2*(i+1);
    X4[i] = 2*(i+2);
    X5[i] = 2*(i+3);
  }

  xElem.DOF(0) = X1;
  xElem.DOF(1) = X2;
  xElem.DOF(2) = X3;
  xElem.DOF(3) = X4;
  xElem.DOF(4) = X5;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    BOOST_CHECK_CLOSE( X1[i], xElem.DOF(0)[i], tol );
    BOOST_CHECK_CLOSE( X2[i], xElem.DOF(1)[i], tol );
    BOOST_CHECK_CLOSE( X3[i], xElem.DOF(2)[i], tol );
    BOOST_CHECK_CLOSE( X4[i], xElem.DOF(3)[i], tol );
    BOOST_CHECK_CLOSE( X5[i], xElem.DOF(4)[i], tol );
  }

  ElementXField<PhysDim,TopoD4,Pentatope> xElem2(xElem);

  BOOST_CHECK_EQUAL( 1, xElem2.order() );
  BOOST_CHECK_EQUAL( 5, xElem2.nDOF() );

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    BOOST_CHECK_CLOSE( X1[i], xElem2.DOF(0)[i], tol );
    BOOST_CHECK_CLOSE( X2[i], xElem2.DOF(1)[i], tol );
    BOOST_CHECK_CLOSE( X3[i], xElem2.DOF(2)[i], tol );
    BOOST_CHECK_CLOSE( X4[i], xElem2.DOF(3)[i], tol );
    BOOST_CHECK_CLOSE( X5[i], xElem2.DOF(4)[i], tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( volume_coordinates, PhysDim, Dimensions )
{
  const Real small_tol = 1e-13;
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysDim,TopoD4,Pentatope> xElem(order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 5, xElem.nDOF() );

  Real sRef, tRef, uRef, vRef;
  typename ElementXField<PhysDim,TopoD3,Tet>::VectorX X1, X2, X3, X4, X5;
  typename ElementXField<PhysDim,TopoD3,Tet>::VectorX X, XTrue;
  Real volume, volumeTrue;

  X1 = X2 = X3 = X4 = X5 = 0;

  X1[0] = 0;  X1[1] = 0;  X1[2] = 0; X1[3] = 0;
  X2[0] = 1;  X2[1] = 0;  X2[2] = 0; X2[3] = 0;
  X3[0] = 0;  X3[1] = 1;  X3[2] = 0; X3[3] = 0;
  X4[0] = 0;  X4[1] = 0;  X4[2] = 1; X4[3] = 0;
  X5[0] = 0;  X5[1] = 0;  X5[2] = 0; X5[3] = 1;
  volumeTrue = 1./24.; // 1/factorial(4)

  xElem.DOF(0) = X1;
  xElem.DOF(1) = X2;
  xElem.DOF(2) = X3;
  xElem.DOF(3) = X4;
  xElem.DOF(4) = X5;

  X = xElem.DOF(0);
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( X1[i], X[i], small_tol, tol );

  X = xElem.DOF(1);
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( X2[i], X[i], small_tol, tol );

  X = xElem.DOF(2);
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( X3[i], X[i], small_tol, tol );

  X = xElem.DOF(3);
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( X4[i], X[i], small_tol, tol );

  X = xElem.DOF(4);
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( X5[i], X[i], small_tol, tol );

  volume = xElem.volume();
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );

  //------
  sRef = 0;  tRef = 0;  uRef = 0; vRef = 0;
  XTrue = X1;
  xElem.coordinates( sRef, tRef, uRef, vRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  volume = xElem.volume( sRef, tRef, uRef, vRef );
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );

  //------
  sRef = 1;  tRef = 0;  uRef = 0; vRef = 0;
  XTrue = X2;
  xElem.coordinates( sRef, tRef, uRef, vRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  volume = xElem.volume( sRef, tRef, uRef, vRef );
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );

  //------
  sRef = 0;  tRef = 1;  uRef = 0; vRef = 0;
  XTrue = X3;
  xElem.coordinates( sRef, tRef, uRef, vRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  volume = xElem.volume( sRef, tRef, uRef, vRef );
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );

  //------
  sRef = 0;  tRef = 0;  uRef = 1; vRef = 0;
  XTrue = X4;
  xElem.coordinates( sRef, tRef, uRef, vRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  volume = xElem.volume( sRef, tRef, uRef, vRef );
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );

  //------
  sRef = 0;  tRef = 0;  uRef = 0; vRef = 1;
  XTrue = X5;
  xElem.coordinates( sRef, tRef, uRef, vRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  volume = xElem.volume( sRef, tRef, uRef, vRef );
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );

  //------
  sRef = 1./5.;  tRef = 1./5.;  uRef = 1./5.; vRef = 1./5.;
  XTrue = (X1 + X2 + X3 + X4 + X5)/5.;
  xElem.coordinates( sRef, tRef, uRef, vRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  volume = xElem.volume( sRef, tRef, uRef, vRef );
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( impliedMetric )
{
  const Real small_tol = 1e-13;
  const Real tol = 1e-11;

  int order = 1;
  ElementXField<PhysD4, TopoD4, Pentatope> xElem(order, BasisFunctionCategory_Lagrange);
  const int (*Edges)[Line::NNode] = ElementEdges<Pentatope>::EdgeNodes;

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 5, xElem.nDOF() );

  ElementXField<PhysD4, TopoD4, Pentatope>::RefCoordType sRef = {0.0, 0.0, 0.0, 0.0};

  DLA::MatrixSymS<PhysD4::D, Real> M;

  // unit equilateral pentatope (not exactly but that's why we multiply by the factor below)
  xElem.DOF(0) = {1.0, 0.0, 0.0, 0};
  xElem.DOF(1) = {-0.25, sqrt(15.)/4., 0.0, 0.0};
  xElem.DOF(2) = {-0.25, -sqrt(5./48.), sqrt(5./6.), 0.0};
  xElem.DOF(3) = {-0.25,-sqrt(5/48.), -sqrt(5./24.) ,sqrt(5./8.)};
  xElem.DOF(4) = {-0.25,-sqrt(5./48.),-sqrt(5./24.),-sqrt(5./8.) };

  // multiply by a factor to actually make unit
  Real fac = sqrt(0.4);
  for (int i=0;i<5;i++)
  for (int d=0;d<4;d++)
    xElem.DOF(i)[d] *= fac;

  // what are the lengths of the  edges?
  // compute the edges of the pentatope
  for (int i=0;i<Pentatope::NEdge;i++)
  {
    DLA::VectorS<PhysD4::D,Real> X = xElem.DOF( Edges[i][1] ) -xElem.DOF( Edges[i][0] );
    Real length = sqrt( Transpose(X)*X );
    SANS_CHECK_CLOSE( 1.0 , length , small_tol , tol );
  }

  xElem.impliedMetric(sRef, M);

  SANS_CHECK_CLOSE( M(0,0), 1.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,2), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,3), 0.0, small_tol, tol );

  SANS_CHECK_CLOSE( M(1,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,1), 1.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,2), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,3), 0.0, small_tol, tol );

  SANS_CHECK_CLOSE( M(2,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(2,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(2,2), 1.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(2,3), 0.0, small_tol, tol );

  SANS_CHECK_CLOSE( M(3,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(3,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(3,2), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(3,3), 1.0, small_tol, tol );

  M = 0;
  xElem.impliedMetric(M); //evaluate at centerRef

  SANS_CHECK_CLOSE( M(0,0), 1.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,2), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,3), 0.0, small_tol, tol );

  SANS_CHECK_CLOSE( M(1,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,1), 1.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,2), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,3), 0.0, small_tol, tol );

  SANS_CHECK_CLOSE( M(2,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(2,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(2,2), 1.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(2,3), 0.0, small_tol, tol );

  SANS_CHECK_CLOSE( M(3,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(3,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(3,2), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(3,3), 1.0, small_tol, tol );

  // unit pentatope - scaled by 2.5, translated by (0.6, 1.3, 3.2, 2.4)
  Real scale = 2.5;
  Real xshift[4] = {0.6, 1.3, 3.2, 2.4};
  for (int i=0; i<5; i++)
    for (int d=0; d<4; d++)
      xElem.DOF(i)[d] = xElem.DOF(i)[d]*scale + xshift[d];

  xElem.impliedMetric(sRef, M);

  SANS_CHECK_CLOSE( M(0,0), 1.0/pow(scale,2.0), small_tol, tol );
  SANS_CHECK_CLOSE( M(0,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,2), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,3), 0.0, small_tol, tol );


  SANS_CHECK_CLOSE( M(1,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,1), 1.0/pow(scale,2.0), small_tol, tol );
  SANS_CHECK_CLOSE( M(1,2), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,3), 0.0, small_tol, tol );

  SANS_CHECK_CLOSE( M(2,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(2,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(2,2), 1.0/pow(scale,2.0), small_tol, tol );
  SANS_CHECK_CLOSE( M(2,3), 0.0, small_tol, tol );

  SANS_CHECK_CLOSE( M(3,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(3,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(3,2), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(3,3), 1.0/pow(scale,2.0), small_tol, tol );

  // some random pentatope, let's hope it's not degenerate ;)
  xElem.DOF(0) = { 0.23420 , 0.391391 , 1.9183 , 0.20381};
  xElem.DOF(1) = { 18.38381 , 2.110 , 0.01 , 2.9 };
  xElem.DOF(2) = { 1.234 , 13.13 , 0.8, 0.19 };
  xElem.DOF(3) = { 0.19819 , 1.2334 , 0.19338 , .1999 };
  xElem.DOF(4) = { .018381 , 0.001 , 18.02 , 8.1 };

  xElem.impliedMetric(sRef,M);

  // check the edges are unit under this metric
  for (int i=0;i<Pentatope::NEdge;i++)
  {
    DLA::VectorS<PhysD4::D,Real> X = xElem.DOF( Edges[i][1] ) -xElem.DOF( Edges[i][0] );
    Real length = sqrt( Transpose(X)*M*X );
    SANS_CHECK_CLOSE( 1.0 , length , small_tol , tol );
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( eval_coordinate, PhysDim, Dimensions )
{
  // tolerance
  const Real tol= 1e-13;

  // linear element
  int order= 1;
  ElementXField<PhysDim,TopoD4,Pentatope> xElem(order, BasisFunctionCategory_Lagrange);

  // constructor checks
  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 5, xElem.nDOF() );

  // variables and typedefs
  Real sRef, tRef, uRef, vRef;
  typename ElementXField<PhysDim,TopoD4,Pentatope>::RefCoordType Ref= {0, 0, 0, 0};
  typename ElementXField<PhysDim,TopoD4,Pentatope>::VectorX X1, X2, X3, X4, X5;
  Real phi[5], phis[5], phit[5], phiu[5], phiv[5];
  Real phi2[5], phis2[5], phit2[5], phiu2[5], phiv2[5];
  Real phi3[5], phis3[5], phit3[5], phiu3[5], phiv3[5];
  Real phiTrue[5], phisTrue[5], phitTrue[5], phiuTrue[5], phivTrue[5];
  typename ElementXField<PhysDim,TopoD4,Pentatope>::VectorX sgrad, tgrad, ugrad, vgrad;
  typename ElementXField<PhysDim,TopoD4,Pentatope>::VectorX sgrad2, tgrad2, ugrad2, vgrad2;
  typename ElementXField<PhysDim,TopoD4,Pentatope>::VectorX sgrad3, tgrad3, ugrad3, vgrad3;
  typename ElementXField<PhysDim,TopoD4,Pentatope>::VectorX sgrad4, tgrad4, ugrad4, vgrad4;
  typename ElementXField<PhysDim,TopoD4,Pentatope>::VectorX sgradTrue, tgradTrue, ugradTrue, vgradTrue;
  int k;

  // zero initializer
  X1= X2= X3= X4= X5= 0;

  // set in the degrees of freedom as origin, followed by sequential
  // ones on each axis, in order. (same as reference element)
  X1[0]= 0; X1[1]= 0; X1[2]= 0; X1[3]= 0;
  X2[0]= 1; X2[1]= 0; X2[2]= 0; X2[3]= 0;
  X3[0]= 0; X3[1]= 1; X3[2]= 0; X3[3]= 0;
  X4[0]= 0; X4[1]= 0; X4[2]= 1; X4[3]= 0;
  X5[0]= 0; X5[1]= 0; X5[2]= 0; X5[3]= 1;

  // set in degrees of freedom
  xElem.DOF(0) = X1;
  xElem.DOF(1) = X2;
  xElem.DOF(2) = X3;
  xElem.DOF(3) = X4;
  xElem.DOF(4) = X5;

  // basis function definitions

  // phi0= 1 - s - t - u - v
  // phi1= s
  // phi2= t
  // phi3= u
  // phi4= v

  // basis function gradients: phi_j -> d(phi_j)/d(x_i) (TRUTH)
  phisTrue[0]= -1; phisTrue[1]= 1; phisTrue[2]= 0; phisTrue[3]= 0; phisTrue[4]= 0;
  phitTrue[0]= -1; phitTrue[1]= 0; phitTrue[2]= 1; phitTrue[3]= 0; phitTrue[4]= 0;
  phiuTrue[0]= -1; phiuTrue[1]= 0; phiuTrue[2]= 0; phiuTrue[3]= 1; phiuTrue[4]= 0;
  phivTrue[0]= -1; phivTrue[1]= 0; phivTrue[2]= 0; phivTrue[3]= 0; phivTrue[4]= 1;

  // d(xi)/d(x) -> for a linear element
  sgradTrue= X2 - X1;
  tgradTrue= X3 - X1;
  ugradTrue= X4 - X1;
  vgradTrue= X5 - X1;

  //------
  {
    sRef= 0; tRef= 0; uRef= 0; vRef= 0;

    Ref= {sRef, tRef, uRef, vRef};
    QuadraturePoint<TopoD4> Quad= QuadraturePoint<TopoD4>(Ref);
    QuadratureCellTracePoint<TopoD4> QCTP= QuadratureCellTracePoint<TopoD4>(Ref);

    phiTrue[0]= 1;    // phi0= 1 - s - t - u - v= 1
    phiTrue[1]= 0;    // phi1= s= 0
    phiTrue[2]= 0;    // phi2= t= 0
    phiTrue[3]= 0;    // phi3= u= 0
    phiTrue[4]= 0;    // phi4= v= 0

    xElem.evalBasis( sRef, tRef, uRef, vRef, phi, 5 );
    xElem.evalBasis( Ref, phi2, 5 );
    xElem.evalBasis( Quad, phi3, 5 );
    xElem.evalBasisDerivative( sRef, tRef, uRef, vRef, phis, phit, phiu, phiv, 5 );
    xElem.evalBasisDerivative( Ref, phis2, phit2, phiu2, phiv2, 5 );
    xElem.evalBasisDerivative( Quad, phis3, phit3, phiu3, phiv3, 5 );
    for (k = 0; k < 5; k++)
    {
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
      BOOST_CHECK_CLOSE( phivTrue[k], phiv[k], tol );
      BOOST_CHECK_CLOSE( phiTrue[k], phi2[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis2[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit2[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu2[k], tol );
      BOOST_CHECK_CLOSE( phivTrue[k], phiv2[k], tol );
      BOOST_CHECK_CLOSE( phiTrue[k], phi3[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis3[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit3[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu3[k], tol );
      BOOST_CHECK_CLOSE( phivTrue[k], phiv3[k], tol );
    }

    xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, vRef, sgrad, tgrad, ugrad, vgrad );
    xElem.evalReferenceCoordinateGradient( Ref, sgrad2, tgrad2, ugrad2, vgrad2 );
    xElem.evalReferenceCoordinateGradient( Quad, sgrad3, tgrad3, ugrad3, vgrad3 );
    xElem.evalReferenceCoordinateGradient( QCTP, sgrad4, tgrad4, ugrad4, vgrad4 );

    for ( int i = 0; i < PhysDim::D; i++ )
    {
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad[i], tol );
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad2[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad2[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad2[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad2[i], tol );
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad3[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad3[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad3[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad3[i], tol );
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad4[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad4[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad4[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad4[i], tol );
    }
  }

  //------
  {
    sRef= 1; tRef= 0; uRef= 0; vRef= 0;

    Ref= {sRef, tRef, uRef, vRef};
    QuadraturePoint<TopoD4> Quad= QuadraturePoint<TopoD4>(Ref);
    QuadratureCellTracePoint<TopoD4> QCTP= QuadratureCellTracePoint<TopoD4>(Ref);

    phiTrue[0]= 0;    // phi0= 1 - s - t - u - v= 0
    phiTrue[1]= 1;    // phi1= s= 1
    phiTrue[2]= 0;    // phi2= t= 0
    phiTrue[3]= 0;    // phi3= u= 0
    phiTrue[4]= 0;    // phi4= v= 0

    xElem.evalBasis( sRef, tRef, uRef, vRef, phi, 5 );
    xElem.evalBasis( Ref, phi2, 5 );
    xElem.evalBasis( Quad, phi3, 5 );
    xElem.evalBasisDerivative( sRef, tRef, uRef, vRef, phis, phit, phiu, phiv, 5 );
    xElem.evalBasisDerivative( Ref, phis2, phit2, phiu2, phiv2, 5 );
    xElem.evalBasisDerivative( Quad, phis3, phit3, phiu3, phiv3, 5 );
    for (k = 0; k < 5; k++)
    {
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
      BOOST_CHECK_CLOSE( phivTrue[k], phiv[k], tol );
      BOOST_CHECK_CLOSE( phiTrue[k], phi2[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis2[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit2[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu2[k], tol );
      BOOST_CHECK_CLOSE( phivTrue[k], phiv2[k], tol );
      BOOST_CHECK_CLOSE( phiTrue[k], phi3[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis3[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit3[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu3[k], tol );
      BOOST_CHECK_CLOSE( phivTrue[k], phiv3[k], tol );
    }

    xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, vRef, sgrad, tgrad, ugrad, vgrad );
    xElem.evalReferenceCoordinateGradient( Ref, sgrad2, tgrad2, ugrad2, vgrad2 );
    xElem.evalReferenceCoordinateGradient( Quad, sgrad3, tgrad3, ugrad3, vgrad3 );
    xElem.evalReferenceCoordinateGradient( QCTP, sgrad4, tgrad4, ugrad4, vgrad4 );

    for ( int i = 0; i < PhysDim::D; i++ )
    {
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad[i], tol );
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad2[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad2[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad2[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad2[i], tol );
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad3[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad3[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad3[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad3[i], tol );
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad4[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad4[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad4[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad4[i], tol );
    }
  }

  //------
  {
    sRef= 0; tRef= 1; uRef= 0; vRef= 0;

    Ref= {sRef, tRef, uRef, vRef};
    QuadraturePoint<TopoD4> Quad= QuadraturePoint<TopoD4>(Ref);
    QuadratureCellTracePoint<TopoD4> QCTP= QuadratureCellTracePoint<TopoD4>(Ref);

    phiTrue[0]= 0;    // phi0= 1 - s - t - u - v= 0
    phiTrue[1]= 0;    // phi1= s= 0
    phiTrue[2]= 1;    // phi2= t= 1
    phiTrue[3]= 0;    // phi3= u= 0
    phiTrue[4]= 0;    // phi4= v= 0

    xElem.evalBasis( sRef, tRef, uRef, vRef, phi, 5 );
    xElem.evalBasis( Ref, phi2, 5 );
    xElem.evalBasis( Quad, phi3, 5 );
    xElem.evalBasisDerivative( sRef, tRef, uRef, vRef, phis, phit, phiu, phiv, 5 );
    xElem.evalBasisDerivative( Ref, phis2, phit2, phiu2, phiv2, 5 );
    xElem.evalBasisDerivative( Quad, phis3, phit3, phiu3, phiv3, 5 );
    for (k = 0; k < 5; k++)
    {
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
      BOOST_CHECK_CLOSE( phivTrue[k], phiv[k], tol );
      BOOST_CHECK_CLOSE( phiTrue[k], phi2[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis2[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit2[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu2[k], tol );
      BOOST_CHECK_CLOSE( phivTrue[k], phiv2[k], tol );
      BOOST_CHECK_CLOSE( phiTrue[k], phi3[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis3[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit3[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu3[k], tol );
      BOOST_CHECK_CLOSE( phivTrue[k], phiv3[k], tol );
    }

    xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, vRef, sgrad, tgrad, ugrad, vgrad );
    xElem.evalReferenceCoordinateGradient( Ref, sgrad2, tgrad2, ugrad2, vgrad2 );
    xElem.evalReferenceCoordinateGradient( Quad, sgrad3, tgrad3, ugrad3, vgrad3 );
    xElem.evalReferenceCoordinateGradient( QCTP, sgrad4, tgrad4, ugrad4, vgrad4 );

    for ( int i = 0; i < PhysDim::D; i++ )
    {
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad[i], tol );
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad2[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad2[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad2[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad2[i], tol );
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad3[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad3[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad3[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad3[i], tol );
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad4[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad4[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad4[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad4[i], tol );
    }
  }

  //------
  {
    sRef= 0; tRef= 0; uRef= 1; vRef= 0;

    Ref= {sRef, tRef, uRef, vRef};
    QuadraturePoint<TopoD4> Quad= QuadraturePoint<TopoD4>(Ref);
    QuadratureCellTracePoint<TopoD4> QCTP= QuadratureCellTracePoint<TopoD4>(Ref);

    phiTrue[0]= 0;    // phi0= 1 - s - t - u - v= 0
    phiTrue[1]= 0;    // phi1= s= 0
    phiTrue[2]= 0;    // phi2= t= 0
    phiTrue[3]= 1;    // phi3= u= 1
    phiTrue[4]= 0;    // phi4= v= 0

    xElem.evalBasis( sRef, tRef, uRef, vRef, phi, 5 );
    xElem.evalBasis( Ref, phi2, 5 );
    xElem.evalBasis( Quad, phi3, 5 );
    xElem.evalBasisDerivative( sRef, tRef, uRef, vRef, phis, phit, phiu, phiv, 5 );
    xElem.evalBasisDerivative( Ref, phis2, phit2, phiu2, phiv2, 5 );
    xElem.evalBasisDerivative( Quad, phis3, phit3, phiu3, phiv3, 5 );
    for (k = 0; k < 5; k++)
    {
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
      BOOST_CHECK_CLOSE( phivTrue[k], phiv[k], tol );
      BOOST_CHECK_CLOSE( phiTrue[k], phi2[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis2[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit2[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu2[k], tol );
      BOOST_CHECK_CLOSE( phivTrue[k], phiv2[k], tol );
      BOOST_CHECK_CLOSE( phiTrue[k], phi3[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis3[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit3[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu3[k], tol );
      BOOST_CHECK_CLOSE( phivTrue[k], phiv3[k], tol );
    }

    xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, vRef, sgrad, tgrad, ugrad, vgrad );
    xElem.evalReferenceCoordinateGradient( Ref, sgrad2, tgrad2, ugrad2, vgrad2 );
    xElem.evalReferenceCoordinateGradient( Quad, sgrad3, tgrad3, ugrad3, vgrad3 );
    xElem.evalReferenceCoordinateGradient( QCTP, sgrad4, tgrad4, ugrad4, vgrad4 );

    for ( int i = 0; i < PhysDim::D; i++ )
    {
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad[i], tol );
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad2[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad2[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad2[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad2[i], tol );
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad3[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad3[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad3[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad3[i], tol );
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad4[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad4[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad4[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad4[i], tol );
    }
  }

  //------
  {
    sRef= 0; tRef= 0; uRef= 0; vRef= 1;

    Ref= {sRef, tRef, uRef, vRef};
    QuadraturePoint<TopoD4> Quad= QuadraturePoint<TopoD4>(Ref);
    QuadratureCellTracePoint<TopoD4> QCTP= QuadratureCellTracePoint<TopoD4>(Ref);

    phiTrue[0]= 0;    // phi0= 1 - s - t - u - v= 0
    phiTrue[1]= 0;    // phi1= s= 0
    phiTrue[2]= 0;    // phi2= t= 0
    phiTrue[3]= 0;    // phi3= u= 0
    phiTrue[4]= 1;    // phi4= v= 1

    xElem.evalBasis( sRef, tRef, uRef, vRef, phi, 5 );
    xElem.evalBasis( Ref, phi2, 5 );
    xElem.evalBasis( Quad, phi3, 5 );
    xElem.evalBasisDerivative( sRef, tRef, uRef, vRef, phis, phit, phiu, phiv, 5 );
    xElem.evalBasisDerivative( Ref, phis2, phit2, phiu2, phiv2, 5 );
    xElem.evalBasisDerivative( Quad, phis3, phit3, phiu3, phiv3, 5 );
    for (k = 0; k < 5; k++)
    {
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
      BOOST_CHECK_CLOSE( phivTrue[k], phiv[k], tol );
      BOOST_CHECK_CLOSE( phiTrue[k], phi2[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis2[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit2[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu2[k], tol );
      BOOST_CHECK_CLOSE( phivTrue[k], phiv2[k], tol );
      BOOST_CHECK_CLOSE( phiTrue[k], phi3[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis3[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit3[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu3[k], tol );
      BOOST_CHECK_CLOSE( phivTrue[k], phiv3[k], tol );
    }

    xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, vRef, sgrad, tgrad, ugrad, vgrad );
    xElem.evalReferenceCoordinateGradient( Ref, sgrad2, tgrad2, ugrad2, vgrad2 );
    xElem.evalReferenceCoordinateGradient( Quad, sgrad3, tgrad3, ugrad3, vgrad3 );
    xElem.evalReferenceCoordinateGradient( QCTP, sgrad4, tgrad4, ugrad4, vgrad4 );

    for ( int i = 0; i < PhysDim::D; i++ )
    {
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad[i], tol );
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad2[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad2[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad2[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad2[i], tol );
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad3[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad3[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad3[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad3[i], tol );
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad4[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad4[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad4[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad4[i], tol );
    }
  }

  //------
  {
    sRef = 1./5.;  tRef = 1./5.;  uRef = 1./5.; vRef = 1./5.;

    Ref= {sRef, tRef, uRef, vRef};
    QuadraturePoint<TopoD4> Quad= QuadraturePoint<TopoD4>(Ref);
    QuadratureCellTracePoint<TopoD4> QCTP= QuadratureCellTracePoint<TopoD4>(Ref);

    phiTrue[0]= 1./5.;    // phi0= 1 - s - t - u - v= 1/5
    phiTrue[1]= 1./5.;    // phi1= s= 1/5
    phiTrue[2]= 1./5.;    // phi2= t= 1/5
    phiTrue[3]= 1./5.;    // phi3= u= 1/5
    phiTrue[4]= 1./5.;    // phi4= v= 1/5

    xElem.evalBasis( sRef, tRef, uRef, vRef, phi, 5 );
    xElem.evalBasis( Ref, phi2, 5 );
    xElem.evalBasis( Quad, phi3, 5 );
    xElem.evalBasisDerivative( sRef, tRef, uRef, vRef, phis, phit, phiu, phiv, 5 );
    xElem.evalBasisDerivative( Ref, phis2, phit2, phiu2, phiv2, 5 );
    xElem.evalBasisDerivative( Quad, phis3, phit3, phiu3, phiv3, 5 );
    for (k = 0; k < 5; k++)
    {
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
      BOOST_CHECK_CLOSE( phivTrue[k], phiv[k], tol );
      BOOST_CHECK_CLOSE( phiTrue[k], phi2[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis2[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit2[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu2[k], tol );
      BOOST_CHECK_CLOSE( phivTrue[k], phiv2[k], tol );
      BOOST_CHECK_CLOSE( phiTrue[k], phi3[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis3[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit3[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu3[k], tol );
      BOOST_CHECK_CLOSE( phivTrue[k], phiv3[k], tol );
    }

    xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, vRef, sgrad, tgrad, ugrad, vgrad );
    xElem.evalReferenceCoordinateGradient( Ref, sgrad2, tgrad2, ugrad2, vgrad2 );
    xElem.evalReferenceCoordinateGradient( Quad, sgrad3, tgrad3, ugrad3, vgrad3 );
    xElem.evalReferenceCoordinateGradient( QCTP, sgrad4, tgrad4, ugrad4, vgrad4 );

    for ( int i = 0; i < PhysDim::D; i++ )
    {
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad[i], tol );
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad2[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad2[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad2[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad2[i], tol );
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad3[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad3[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad3[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad3[i], tol );
      BOOST_CHECK_CLOSE( sgradTrue[i], sgrad4[i], tol );
      BOOST_CHECK_CLOSE( tgradTrue[i], tgrad4[i], tol );
      BOOST_CHECK_CLOSE( ugradTrue[i], ugrad4[i], tol );
      BOOST_CHECK_CLOSE( vgradTrue[i], vgrad4[i], tol );
    }
  }


}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( evalBasisGradient )
{

  // set up first order lagrange basis
  typedef BasisFunctionSpacetime<Pentatope, Lagrange, 1> BasisClass;

  int order= 1;
  ElementXField<PhysD4, TopoD4, Pentatope> xElem(order, BasisFunctionCategory_Lagrange);

  // check vitals of element
  BOOST_CHECK_EQUAL(1, xElem.order());
  BOOST_CHECK_EQUAL(5, xElem.nDOF());

  // create basis and qfld
  const BasisClass* basis= BasisClass::self();
  Element<Real, TopoD4, Pentatope> qfldElem(basis);

  // check vitals of basis and field
  BOOST_CHECK_EQUAL(1, basis->order());
  BOOST_CHECK_EQUAL(5, basis->nBasis());

  // set up stuff
  const Real tol= 1e-13;
  typename ElementXField<PhysD4, TopoD4, Pentatope>::RefCoordType sRef;
  typename ElementXField<PhysD4, TopoD4, Pentatope>::VectorX gradphi[5];
  typename ElementXField<PhysD4, TopoD4, Pentatope>::VectorX gradphi2[5];
  typename ElementXField<PhysD4, TopoD4, Pentatope>::VectorX gradphi3[5];
  Real phixTrue[5], phiyTrue[5], phizTrue[5], phitTrue[5];
  int k;

  xElem.DOF(0)= {0, 0, 0, 0};
  xElem.DOF(1)= {1, 0, 0, 0};
  xElem.DOF(2)= {0, 1, 0, 0};
  xElem.DOF(3)= {0, 0, 1, 0};
  xElem.DOF(4)= {0, 0, 0, 1};

  // basis gradient is constant because element is fully linear
  phixTrue[0]= -1; phixTrue[1]= 1; phixTrue[2]= 0; phixTrue[3]= 0; phixTrue[4]= 0;
  phiyTrue[0]= -1; phiyTrue[1]= 0; phiyTrue[2]= 1; phiyTrue[3]= 0; phiyTrue[4]= 0;
  phizTrue[0]= -1; phizTrue[1]= 0; phizTrue[2]= 0; phizTrue[3]= 1; phizTrue[4]= 0;
  phitTrue[0]= -1; phitTrue[1]= 0; phitTrue[2]= 0; phitTrue[3]= 0; phitTrue[4]= 1;

  //------
  {
    sRef[0]= 0; sRef[1]= 0; sRef[2]= 0; sRef[3]= 0;
    QuadraturePoint<TopoD4> Quad= QuadraturePoint<TopoD4>(sRef);
    QuadratureCellTracePoint<TopoD4> QCTP= QuadratureCellTracePoint<TopoD4>(sRef);

    xElem.evalBasisGradient(sRef, qfldElem, gradphi, 5);
    xElem.evalBasisGradient(Quad, qfldElem, gradphi2, 5);
    xElem.evalBasisGradient(QCTP, qfldElem, gradphi3, 5);

    for (k = 0; k < 5; k++)
    {
      BOOST_CHECK_CLOSE(phixTrue[k], gradphi[k][0], tol);
      BOOST_CHECK_CLOSE(phiyTrue[k], gradphi[k][1], tol);
      BOOST_CHECK_CLOSE(phizTrue[k], gradphi[k][2], tol);
      BOOST_CHECK_CLOSE(phitTrue[k], gradphi[k][3], tol);
      BOOST_CHECK_CLOSE(phixTrue[k], gradphi2[k][0], tol);
      BOOST_CHECK_CLOSE(phiyTrue[k], gradphi2[k][1], tol);
      BOOST_CHECK_CLOSE(phizTrue[k], gradphi2[k][2], tol);
      BOOST_CHECK_CLOSE(phitTrue[k], gradphi2[k][3], tol);
      BOOST_CHECK_CLOSE(phixTrue[k], gradphi3[k][0], tol);
      BOOST_CHECK_CLOSE(phiyTrue[k], gradphi3[k][1], tol);
      BOOST_CHECK_CLOSE(phizTrue[k], gradphi3[k][2], tol);
      BOOST_CHECK_CLOSE(phitTrue[k], gradphi3[k][3], tol);
    }
  }

  //------
  {
    sRef[0]= 1; sRef[1]= 0; sRef[2]= 0; sRef[3]= 0;
    QuadraturePoint<TopoD4> Quad= QuadraturePoint<TopoD4>(sRef);
    QuadratureCellTracePoint<TopoD4> QCTP= QuadratureCellTracePoint<TopoD4>(sRef);

    xElem.evalBasisGradient(sRef, qfldElem, gradphi, 5);
    xElem.evalBasisGradient(Quad, qfldElem, gradphi2, 5);
    xElem.evalBasisGradient(QCTP, qfldElem, gradphi3, 5);

    for (k = 0; k < 5; k++)
    {
      BOOST_CHECK_CLOSE(phixTrue[k], gradphi[k][0], tol);
      BOOST_CHECK_CLOSE(phiyTrue[k], gradphi[k][1], tol);
      BOOST_CHECK_CLOSE(phizTrue[k], gradphi[k][2], tol);
      BOOST_CHECK_CLOSE(phitTrue[k], gradphi[k][3], tol);
      BOOST_CHECK_CLOSE(phixTrue[k], gradphi2[k][0], tol);
      BOOST_CHECK_CLOSE(phiyTrue[k], gradphi2[k][1], tol);
      BOOST_CHECK_CLOSE(phizTrue[k], gradphi2[k][2], tol);
      BOOST_CHECK_CLOSE(phitTrue[k], gradphi2[k][3], tol);
      BOOST_CHECK_CLOSE(phixTrue[k], gradphi3[k][0], tol);
      BOOST_CHECK_CLOSE(phiyTrue[k], gradphi3[k][1], tol);
      BOOST_CHECK_CLOSE(phizTrue[k], gradphi3[k][2], tol);
      BOOST_CHECK_CLOSE(phitTrue[k], gradphi3[k][3], tol);
    }
  }

  //------
  {
    sRef[0]= 0; sRef[1]= 1; sRef[2]= 0; sRef[3]= 0;
    QuadraturePoint<TopoD4> Quad= QuadraturePoint<TopoD4>(sRef);
    QuadratureCellTracePoint<TopoD4> QCTP= QuadratureCellTracePoint<TopoD4>(sRef);

    xElem.evalBasisGradient(sRef, qfldElem, gradphi, 5);
    xElem.evalBasisGradient(Quad, qfldElem, gradphi2, 5);
    xElem.evalBasisGradient(QCTP, qfldElem, gradphi3, 5);

    for (k = 0; k < 5; k++)
    {
      BOOST_CHECK_CLOSE(phixTrue[k], gradphi[k][0], tol);
      BOOST_CHECK_CLOSE(phiyTrue[k], gradphi[k][1], tol);
      BOOST_CHECK_CLOSE(phizTrue[k], gradphi[k][2], tol);
      BOOST_CHECK_CLOSE(phitTrue[k], gradphi[k][3], tol);
      BOOST_CHECK_CLOSE(phixTrue[k], gradphi2[k][0], tol);
      BOOST_CHECK_CLOSE(phiyTrue[k], gradphi2[k][1], tol);
      BOOST_CHECK_CLOSE(phizTrue[k], gradphi2[k][2], tol);
      BOOST_CHECK_CLOSE(phitTrue[k], gradphi2[k][3], tol);
      BOOST_CHECK_CLOSE(phixTrue[k], gradphi3[k][0], tol);
      BOOST_CHECK_CLOSE(phiyTrue[k], gradphi3[k][1], tol);
      BOOST_CHECK_CLOSE(phizTrue[k], gradphi3[k][2], tol);
      BOOST_CHECK_CLOSE(phitTrue[k], gradphi3[k][3], tol);
    }
  }

  //------
  {
    sRef[0]= 0; sRef[1]= 0; sRef[2]= 1; sRef[3]= 0;
    QuadraturePoint<TopoD4> Quad= QuadraturePoint<TopoD4>(sRef);
    QuadratureCellTracePoint<TopoD4> QCTP= QuadratureCellTracePoint<TopoD4>(sRef);

    xElem.evalBasisGradient(sRef, qfldElem, gradphi, 5);
    xElem.evalBasisGradient(Quad, qfldElem, gradphi2, 5);
    xElem.evalBasisGradient(QCTP, qfldElem, gradphi3, 5);

    for (k = 0; k < 5; k++)
    {
      BOOST_CHECK_CLOSE(phixTrue[k], gradphi[k][0], tol);
      BOOST_CHECK_CLOSE(phiyTrue[k], gradphi[k][1], tol);
      BOOST_CHECK_CLOSE(phizTrue[k], gradphi[k][2], tol);
      BOOST_CHECK_CLOSE(phitTrue[k], gradphi[k][3], tol);
      BOOST_CHECK_CLOSE(phixTrue[k], gradphi2[k][0], tol);
      BOOST_CHECK_CLOSE(phiyTrue[k], gradphi2[k][1], tol);
      BOOST_CHECK_CLOSE(phizTrue[k], gradphi2[k][2], tol);
      BOOST_CHECK_CLOSE(phitTrue[k], gradphi2[k][3], tol);
      BOOST_CHECK_CLOSE(phixTrue[k], gradphi3[k][0], tol);
      BOOST_CHECK_CLOSE(phiyTrue[k], gradphi3[k][1], tol);
      BOOST_CHECK_CLOSE(phizTrue[k], gradphi3[k][2], tol);
      BOOST_CHECK_CLOSE(phitTrue[k], gradphi3[k][3], tol);
    }
  }

  //------
  {
    sRef[0]= 0; sRef[1]= 0; sRef[2]= 0; sRef[3]= 1;
    QuadraturePoint<TopoD4> Quad= QuadraturePoint<TopoD4>(sRef);
    QuadratureCellTracePoint<TopoD4> QCTP= QuadratureCellTracePoint<TopoD4>(sRef);

    xElem.evalBasisGradient(sRef, qfldElem, gradphi, 5);
    xElem.evalBasisGradient(Quad, qfldElem, gradphi2, 5);
    xElem.evalBasisGradient(QCTP, qfldElem, gradphi3, 5);

    for (k = 0; k < 5; k++)
    {
      BOOST_CHECK_CLOSE(phixTrue[k], gradphi[k][0], tol);
      BOOST_CHECK_CLOSE(phiyTrue[k], gradphi[k][1], tol);
      BOOST_CHECK_CLOSE(phizTrue[k], gradphi[k][2], tol);
      BOOST_CHECK_CLOSE(phitTrue[k], gradphi[k][3], tol);
      BOOST_CHECK_CLOSE(phixTrue[k], gradphi2[k][0], tol);
      BOOST_CHECK_CLOSE(phiyTrue[k], gradphi2[k][1], tol);
      BOOST_CHECK_CLOSE(phizTrue[k], gradphi2[k][2], tol);
      BOOST_CHECK_CLOSE(phitTrue[k], gradphi2[k][3], tol);
      BOOST_CHECK_CLOSE(phixTrue[k], gradphi3[k][0], tol);
      BOOST_CHECK_CLOSE(phiyTrue[k], gradphi3[k][1], tol);
      BOOST_CHECK_CLOSE(phizTrue[k], gradphi3[k][2], tol);
      BOOST_CHECK_CLOSE(phitTrue[k], gradphi3[k][3], tol);
    }
  }

  //------
  {
    sRef[0]= 1./5.; sRef[1]= 1./5.; sRef[2]= 1./5.; sRef[3]= 1./5.;
    QuadraturePoint<TopoD4> Quad= QuadraturePoint<TopoD4>(sRef);
    QuadratureCellTracePoint<TopoD4> QCTP= QuadratureCellTracePoint<TopoD4>(sRef);

    xElem.evalBasisGradient(sRef, qfldElem, gradphi, 5);
    xElem.evalBasisGradient(Quad, qfldElem, gradphi2, 5);
    xElem.evalBasisGradient(QCTP, qfldElem, gradphi3, 5);

    for (k = 0; k < 5; k++)
    {
      BOOST_CHECK_CLOSE(phixTrue[k], gradphi[k][0], tol);
      BOOST_CHECK_CLOSE(phiyTrue[k], gradphi[k][1], tol);
      BOOST_CHECK_CLOSE(phizTrue[k], gradphi[k][2], tol);
      BOOST_CHECK_CLOSE(phitTrue[k], gradphi[k][3], tol);
      BOOST_CHECK_CLOSE(phixTrue[k], gradphi2[k][0], tol);
      BOOST_CHECK_CLOSE(phiyTrue[k], gradphi2[k][1], tol);
      BOOST_CHECK_CLOSE(phizTrue[k], gradphi2[k][2], tol);
      BOOST_CHECK_CLOSE(phitTrue[k], gradphi2[k][3], tol);
      BOOST_CHECK_CLOSE(phixTrue[k], gradphi3[k][0], tol);
      BOOST_CHECK_CLOSE(phiyTrue[k], gradphi3[k][1], tol);
      BOOST_CHECK_CLOSE(phizTrue[k], gradphi3[k][2], tol);
      BOOST_CHECK_CLOSE(phitTrue[k], gradphi3[k][3], tol);
    }
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( evalReferenceCoordinateHessian )
{

  const Real tol= 1e-11;

  typename ElementXField<PhysD4, TopoD4, Pentatope>::VectorX sgrad, tgrad, ugrad, vgrad;
  typename ElementXField<PhysD4, TopoD4, Pentatope>::Matrix invJ;

  typename ElementXField<PhysD4, TopoD4, Pentatope>::TensorSymX shess, thess, uhess, vhess;
  typename ElementXField<PhysD4, TopoD4, Pentatope>::TensorSymX shessTrue, thessTrue, uhessTrue, vhessTrue;
  typename ElementXField<PhysD4, TopoD4, Pentatope>::TensorSymHessian H;
  typename ElementXField<PhysD4, TopoD4, Pentatope>::TensorSymX hesstmp0, hesstmp1, hesstmp2, hesstmp3;

  // a seemingly arbitrary point
  Real sRef= 0.3, tRef=0.4, uRef= 0.25, vRef= 0.15;

//  // second order elements with lagrange basis functions TODO needs edge dofs and all that.
//  int order= 2;
//  ElementXField<PhysD4, TopoD4, Pentatope> xElem(order, BasisFunctionCategory_Lagrange);
//
//  // node DOFs
//  xElem.DOF(0)= {0, 0, 0, 0};
//  xElem.DOF(1)= {1, 0, 0, 0};
//  xElem.DOF(2)= {0, 1, 0, 0};
//  xElem.DOF(3)= {0, 0, 1, 0};
//  xElem.DOF(4)= {0, 0, 0, 1};
//
//  // edge DOFs
//  xElem.DOF(4)= {-0.1,  0.6,  0.5};
//  xElem.DOF(5)= { 0.6, -0.1,  0.5};
//  xElem.DOF(6)= { 0.5,  0.6,  0.0};
//  xElem.DOF(7)= {-0.1,  0.5, -0.1};
//  xElem.DOF(8)= { 0.0, -0.1,  0.5};
//  xElem.DOF(9)= { 0.5, -0.1, -0.1};

  // first order elements with lagrange basis functions for now
  int order= 1;
  ElementXField<PhysD4, TopoD4, Pentatope> xElem(order, BasisFunctionCategory_Lagrange);

  // node DOFs
  xElem.DOF(0)= {0, 0, 0, 0};
  xElem.DOF(1)= {1, 0, 0, 0};
  xElem.DOF(2)= {0, 1, 0, 0};
  xElem.DOF(3)= {0, 0, 1, 0};
  xElem.DOF(4)= {0, 0, 0, 1};

  xElem.hessian(sRef, tRef, uRef, vRef, H);
  xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, vRef, sgrad, tgrad, ugrad, vgrad);
  xElem.evalReferenceCoordinateHessian( sRef, tRef, uRef, vRef, shess, thess, uhess, vhess);

  invJ(0, 0)= sgrad[0];
  invJ(0, 1)= sgrad[1];
  invJ(0, 2)= sgrad[2];
  invJ(0, 3)= sgrad[3];

  invJ(1, 0)= tgrad[0];
  invJ(1, 1)= tgrad[1];
  invJ(1, 2)= tgrad[2];
  invJ(1, 3)= tgrad[3];

  invJ(2, 0)= ugrad[0];
  invJ(2, 1)= ugrad[1];
  invJ(2, 2)= ugrad[2];
  invJ(2, 3)= ugrad[3];

  invJ(3, 0)= vgrad[0];
  invJ(3, 1)= vgrad[1];
  invJ(3, 2)= vgrad[2];
  invJ(3, 3)= vgrad[3];

  hesstmp0= Transpose(invJ)*H[0]*invJ;
  hesstmp1= Transpose(invJ)*H[1]*invJ;
  hesstmp2= Transpose(invJ)*H[2]*invJ;
  hesstmp3= Transpose(invJ)*H[3]*invJ;

  shessTrue= -(invJ(0,0)*hesstmp0 + invJ(0,1)*hesstmp1 + invJ(0,2)*hesstmp2 + invJ(0,3)*hesstmp3);
  thessTrue= -(invJ(1,0)*hesstmp0 + invJ(1,1)*hesstmp1 + invJ(1,2)*hesstmp2 + invJ(1,3)*hesstmp3);
  uhessTrue= -(invJ(2,0)*hesstmp0 + invJ(2,1)*hesstmp1 + invJ(2,2)*hesstmp2 + invJ(2,3)*hesstmp3);
  vhessTrue= -(invJ(3,0)*hesstmp0 + invJ(3,1)*hesstmp1 + invJ(3,2)*hesstmp2 + invJ(3,3)*hesstmp3);

  for (int i= 0; i < PhysD3::D; i++)
    for (int j= 0; j <= i; j++)
    {
      BOOST_CHECK_CLOSE(shessTrue(i, j), shess(i, j), tol);
      BOOST_CHECK_CLOSE(thessTrue(i, j), thess(i, j), tol);
      BOOST_CHECK_CLOSE(uhessTrue(i, j), uhess(i, j), tol);
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BoundingBox_Q1_test )
{

  int order= 1;
  ElementXField<PhysD4, TopoD4, Pentatope> xElem(order, BasisFunctionCategory_Lagrange);

  xElem.DOF(0)= {0, 0, 0, 0};
  xElem.DOF(1)= {1, 0, 0, 0};
  xElem.DOF(2)= {0, 1, 0, 0};
  xElem.DOF(3)= {0, 0, 1, 0};
  xElem.DOF(4)= {0, 0, 0, 1};

  BoundingBox<PhysD4> bbox= xElem.boundingBox();

  BOOST_CHECK_EQUAL(0, bbox.low_bounds()[0]);
  BOOST_CHECK_EQUAL(0, bbox.low_bounds()[1]);
  BOOST_CHECK_EQUAL(0, bbox.low_bounds()[2]);
  BOOST_CHECK_EQUAL(0, bbox.low_bounds()[3]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[0]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[1]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[2]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[3]);

}

////----------------------------------------------------------------------------//
//BOOST_AUTO_TEST_CASE( BoundingBox_Q2_test )
//{
//  int order = 2;
//  ElementXField<PhysD3,TopoD3,Tet> xElem(order, BasisFunctionCategory_Lagrange);
//
//  // node DOFs
//  xElem.DOF(0) = {0,0,0};
//  xElem.DOF(1) = {1,0,0};
//  xElem.DOF(2) = {0,1,0};
//  xElem.DOF(3) = {0,0,1};
//
//  // edge DOFs
//  xElem.DOF(4) = { 0.0,  0.5,  0.5};
//  xElem.DOF(5) = { 0.5, -0.1,  0.5};
//  xElem.DOF(6) = { 0.5,  0.5,  0.0};
//  xElem.DOF(7) = {-0.1,  0.5,  0.0};
//  xElem.DOF(8) = {-0.1,  0.0,  0.5};
//  xElem.DOF(9) = { 0.5,  0.0, -0.1};
//
//  BoundingBox<PhysD3> bbox = xElem.boundingBox();
//
//  BOOST_CHECK_EQUAL(-0.1, bbox.low_bounds()[0]);
//  BOOST_CHECK_EQUAL(-0.1, bbox.low_bounds()[1]);
//  BOOST_CHECK_EQUAL(-0.1, bbox.low_bounds()[2]);
//  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[0]);
//  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[1]);
//  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[2]);
//}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( extract_basis, PhysDim, Dimensions )
{
  int order = 1;
  ElementXField<PhysDim,TopoD3,Tet> xElem_P1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem_P1.order() );
  BOOST_CHECK_EQUAL( 4, xElem_P1.nDOF() );

  const BasisFunctionVolumeBase<Tet>* basis_P1 = xElem_P1.basis();

  BOOST_CHECK_EQUAL( 1, basis_P1->order() );
  BOOST_CHECK_EQUAL( 4, basis_P1->nBasis() );

  order = 2;
  ElementXField<PhysDim,TopoD3,Tet> xElem_P2(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, xElem_P2.order() );
  BOOST_CHECK_EQUAL( 10, xElem_P2.nDOF() );

  const BasisFunctionVolumeBase<Tet>* basis_P2 = xElem_P2.basis();

  BOOST_CHECK_EQUAL( 2, basis_P2->order() );
  BOOST_CHECK_EQUAL( 10, basis_P2->nBasis() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( face_sign, PhysDim, Dimensions )
{
  typedef std::array<int,4> Int4;

  int order = 1;
  ElementXField<PhysDim,TopoD3,Tet> xElem(order, BasisFunctionCategory_Hierarchical);

  Int4 faceSign = xElem.faceSign();
  BOOST_CHECK_EQUAL( +1, faceSign[0] );
  BOOST_CHECK_EQUAL( +1, faceSign[1] );
  BOOST_CHECK_EQUAL( +1, faceSign[2] );
  BOOST_CHECK_EQUAL( +1, faceSign[3] );

  faceSign[1] = -1;
  xElem.setFaceSign(faceSign);

  Int4 faceSign2 = xElem.faceSign();
  BOOST_CHECK_EQUAL( +1, faceSign2[0] );
  BOOST_CHECK_EQUAL( -1, faceSign2[1] );
  BOOST_CHECK_EQUAL( +1, faceSign2[2] );
  BOOST_CHECK_EQUAL( +1, faceSign2[3] );
}

//----------------------------------------------------------------------------//
// check that the trace element coordinates evaluate to the volume element coordinates on the trace
BOOST_AUTO_TEST_CASE( Trace_RefCoord_P1 )
{
  typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldVolumeClass;
  typedef ElementXField<PhysD3,TopoD2,Triangle> ElementXFieldAreaClass;
  typedef ElementXFieldVolumeClass::VectorX VectorX;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  // element field variables
  ElementXFieldAreaClass xfldElemFaceP( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  ElementXFieldAreaClass xfldElemFaceN( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  ElementXFieldVolumeClass xfldElemCell( BasisFunctionVolumeBase<Tet>::HierarchicalP1 );

  xfldElemCell.DOF(0) = {0, 0, 0};
  xfldElemCell.DOF(1) = {1, 0, 0};
  xfldElemCell.DOF(2) = {0, 1, 0};
  xfldElemCell.DOF(3) = {0, 0, 1};

  const int tet[4] = {0, 1, 2, 3};

  const int (*TraceNodes)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes;

  const int (*OrientPos)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::OrientPos;
  const int (*OrientNeg)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::OrientNeg;


  for (int face = 0; face < Tet::NTrace; face++)
  {
    for (int orient = 0; orient < Triangle::NTrace; orient++)
    {
      // Get the re-oriented triangles
      const int triP[3] = {tet[TraceNodes[face][OrientPos[orient][0]]],
                           tet[TraceNodes[face][OrientPos[orient][1]]],
                           tet[TraceNodes[face][OrientPos[orient][2]]]};

      const int triN[3] = {tet[TraceNodes[face][OrientNeg[orient][0]]],
                           tet[TraceNodes[face][OrientNeg[orient][1]]],
                           tet[TraceNodes[face][OrientNeg[orient][2]]]};

      const CanonicalTraceToCell canonicalFaceP = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(triP, 3, tet, 4);
      const CanonicalTraceToCell canonicalFaceN = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(triN, 3, tet, 4);

      BOOST_REQUIRE_EQUAL(canonicalFaceP.trace, face);
      BOOST_REQUIRE_EQUAL(canonicalFaceP.orientation, orient+1);

      BOOST_REQUIRE_EQUAL(canonicalFaceN.trace, face);
      BOOST_REQUIRE_EQUAL(canonicalFaceN.orientation, -orient-1);

      // Set the DOF's on the trace
      for (int n = 0; n < Triangle::NNode; n++)
      {
        xfldElemFaceP.DOF(n) = xfldElemCell.DOF(triP[n]);
        xfldElemFaceN.DOF(n) = xfldElemCell.DOF(triN[n]);
      }

      int kmax = 5;
      for (int k = 0; k < kmax; k++)
      {
        DLA::VectorS<2,Real> sRefTrace;
        DLA::VectorS<3,Real> sRefCell;
        VectorX xTrace, xCell;
        Real s, t;

        // Edge 0
        s = k/static_cast<Real>(kmax-1);
        t = 1-s;

        sRefTrace = {s,t};

        // volume reference-element coords
        TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFaceP, sRefTrace, sRefCell );

        // element coordinates from trace and cell
        xfldElemFaceP.eval( sRefTrace, xTrace );
        xfldElemCell.eval( sRefCell, xCell );

        SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );

        // volume reference-element coords
        TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFaceN, sRefTrace, sRefCell );

        // element coordinates from trace and cell
        xfldElemFaceN.eval( sRefTrace, xTrace );
        xfldElemCell.eval( sRefCell, xCell );

        SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );


        // Edge 1
        s = 0;
        t = k/static_cast<Real>(kmax-1);

        sRefTrace = {s,t};

        // left/right reference-element coords
        TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFaceP, sRefTrace, sRefCell );

        // element coordinates from trace and cell
        xfldElemFaceP.eval( sRefTrace, xTrace );
        xfldElemCell.eval( sRefCell, xCell );

        SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );

        // volume reference-element coords
        TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFaceN, sRefTrace, sRefCell );

        // element coordinates from trace and cell
        xfldElemFaceN.eval( sRefTrace, xTrace );
        xfldElemCell.eval( sRefCell, xCell );

        SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );


        // Edge 2
        s = k/static_cast<Real>(kmax-1);
        t = 0;

        sRefTrace = {s,t};

        // left/right reference-element coords
        TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFaceP, sRefTrace, sRefCell );

        // element coordinates from trace and cell
        xfldElemFaceP.eval( sRefTrace, xTrace );
        xfldElemCell.eval( sRefCell, xCell );

        SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );

        // volume reference-element coords
        TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFaceN, sRefTrace, sRefCell );

        // element coordinates from trace and cell
        xfldElemFaceN.eval( sRefTrace, xTrace );
        xfldElemCell.eval( sRefCell, xCell );

        SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );
      }
    }
  }
}


//----------------------------------------------------------------------------//
// check that the trace element coordinates evaluate to the volume element coordinates on the trace
BOOST_AUTO_TEST_CASE( Frame_RefCoord_P1 )
{
  typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldVolumeClass;
  typedef ElementXField<PhysD3,TopoD2,Triangle> ElementXFieldAreaClass;
  typedef ElementXField<PhysD3,TopoD1,Line> ElementXFieldLineClass;
  typedef ElementXFieldVolumeClass::VectorX VectorX;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  // element field variables
  ElementXFieldLineClass xfldElemEdgeP( BasisFunctionLineBase::HierarchicalP1 );
  ElementXFieldLineClass xfldElemEdgeN( BasisFunctionLineBase::HierarchicalP1 );
  ElementXFieldAreaClass xfldElemFaceP( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  ElementXFieldAreaClass xfldElemFaceN( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  ElementXFieldVolumeClass xfldElemCell( BasisFunctionVolumeBase<Tet>::HierarchicalP1 );

  xfldElemCell.DOF(0) = {0, 0, 0};
  xfldElemCell.DOF(1) = {1, 0, 0};
  xfldElemCell.DOF(2) = {0, 1, 0};
  xfldElemCell.DOF(3) = {0, 0, 1};

  const int tet[4] = {0, 1, 2, 3};

  const int (*TraceNodes)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes;

  const int (*OrientTPos)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::OrientPos;
  const int (*OrientTNeg)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::OrientNeg;

  const int (*FrameNodes)[ Line::NTrace ] = TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes;

  const int *OrientFPos = TraceToCellRefCoord<Line, TopoD2, Triangle>::OrientPos;
  const int *OrientFNeg = TraceToCellRefCoord<Line, TopoD2, Triangle>::OrientNeg;

  for (int face = 0; face < Tet::NTrace; face++)
  {
    for (int orient = 0; orient < Triangle::NTrace; orient++)
    {
      // Get the re-oriented triangles
      const int triP[3] = {tet[TraceNodes[face][OrientTPos[orient][0]]],
                           tet[TraceNodes[face][OrientTPos[orient][1]]],
                           tet[TraceNodes[face][OrientTPos[orient][2]]]};

      const int triN[3] = {tet[TraceNodes[face][OrientTNeg[orient][0]]],
                           tet[TraceNodes[face][OrientTNeg[orient][1]]],
                           tet[TraceNodes[face][OrientTNeg[orient][2]]]};

      const CanonicalTraceToCell canonicalFaceP = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(triP, 3, tet, 4);
      const CanonicalTraceToCell canonicalFaceN = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(triN, 3, tet, 4);

      BOOST_REQUIRE_EQUAL(canonicalFaceP.trace, face);
      BOOST_REQUIRE_EQUAL(canonicalFaceP.orientation, orient+1);

      BOOST_REQUIRE_EQUAL(canonicalFaceN.trace, face);
      BOOST_REQUIRE_EQUAL(canonicalFaceN.orientation, -orient-1);

      // Set the DOF's on the trace
      for (int n = 0; n < Triangle::NNode; n++)
      {
        xfldElemFaceP.DOF(n) = xfldElemCell.DOF(triP[n]);
        xfldElemFaceN.DOF(n) = xfldElemCell.DOF(triN[n]);
      }

      // Test both positive and negative triangles
      for (int n = 0; n < 2; n++ )
      {
        const int* tri = NULL;
        CanonicalTraceToCell canonicalFace;
        ElementXFieldAreaClass* xfldElemFace = NULL;
        if (n == 0)
        {
          tri = triP;
          canonicalFace = canonicalFaceP;
          xfldElemFace = &xfldElemFaceP;
        }
        else if (n == 1)
        {
          tri = triN;
          canonicalFace = canonicalFaceN;
          xfldElemFace = &xfldElemFaceN;
        }
        else
          BOOST_REQUIRE( false ); // suppress compiler warning

        for (int edge = 0; edge < Triangle::NTrace; edge++)
        {
          // Get the re-oriented lines
          const int lineP[2] = {tri[FrameNodes[edge][OrientFPos[0]]],
                                tri[FrameNodes[edge][OrientFPos[1]]]};

          const int lineN[2] = {tri[FrameNodes[edge][OrientFNeg[0]]],
                                tri[FrameNodes[edge][OrientFNeg[1]]]};

          const CanonicalTraceToCell canonicalEdgeP = TraceToCellRefCoord<Line, TopoD2, Triangle>::getCanonicalTrace(lineP, 2, tri, 3);
          const CanonicalTraceToCell canonicalEdgeN = TraceToCellRefCoord<Line, TopoD2, Triangle>::getCanonicalTrace(lineN, 2, tri, 3);

          BOOST_REQUIRE_EQUAL(canonicalEdgeP.trace, edge);
          BOOST_REQUIRE_EQUAL(canonicalEdgeP.orientation, +1);

          BOOST_REQUIRE_EQUAL(canonicalEdgeN.trace, edge);
          BOOST_REQUIRE_EQUAL(canonicalEdgeN.orientation, -1);

          // Set the DOF's on the trace
          for (int n = 0; n < Line::NNode; n++)
          {
            xfldElemEdgeP.DOF(n) = xfldElemCell.DOF(lineP[n]);
            xfldElemEdgeN.DOF(n) = xfldElemCell.DOF(lineN[n]);
          }

          int kmax = 2;
          for (int k = 0; k < kmax; k++)
          {
            DLA::VectorS<1,Real> sRefFrame;
            DLA::VectorS<2,Real> sRefTrace;
            DLA::VectorS<3,Real> sRefCell;
            VectorX xFrame, xTrace, xCell;
            Real s;

            s = k/static_cast<Real>(kmax-1);

            sRefFrame = {s};

            // volume reference-element coords
            TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalEdgeP, sRefFrame, sRefTrace );
            TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefTrace, sRefCell );

            // element coordinates from trace and cell
            xfldElemEdgeP.eval( sRefFrame, xFrame );
            xfldElemFace->eval( sRefTrace, xTrace );
            xfldElemCell.eval( sRefCell, xCell );

            SANS_CHECK_CLOSE( xFrame[0], xTrace[0], small_tol, close_tol );
            SANS_CHECK_CLOSE( xFrame[1], xTrace[1], small_tol, close_tol );
            SANS_CHECK_CLOSE( xFrame[2], xTrace[2], small_tol, close_tol );

            SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
            SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
            SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );

            // volume reference-element coords
            TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalEdgeN, sRefFrame, sRefTrace );
            TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefTrace, sRefCell );

            // element coordinates from trace and cell
            xfldElemEdgeN.eval( sRefFrame, xFrame );
            xfldElemFace->eval( sRefTrace, xTrace );
            xfldElemCell.eval( sRefCell, xCell );

            SANS_CHECK_CLOSE( xFrame[0], xTrace[0], small_tol, close_tol );
            SANS_CHECK_CLOSE( xFrame[1], xTrace[1], small_tol, close_tol );
            SANS_CHECK_CLOSE( xFrame[2], xTrace[2], small_tol, close_tol );

            SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
            SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
            SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );
          }
        }
      }
    }
  }
}

////----------------------------------------------------------------------------//
//BOOST_AUTO_TEST_CASE_TEMPLATE( IO, PhysDim, Dimensions )
//{
//  //Set the 2nd argument to false to regenerate the pattern file
//  output_test_stream output( "IO/Field/ElementXField" + stringify((int)PhysDim::D) + "DVolume_Tetrahedron_pattern.txt", true );
//
//  ElementXField<PhysDim,TopoD3,Tet> xElem(1, BasisFunctionCategory_Hierarchical);
//
//  typename ElementXField<PhysDim,TopoD3,Tet>::VectorX X1, X2, X3, X4;
//
//  for ( int i = 0; i < PhysDim::D; i++ )
//  {
//    X1[i] = i+1;
//    X2[i] = 2*i;
//    X3[i] = 2*(i+1);
//    X4[i] = 2*(i+2);
//  }
//
//  xElem.DOF(0) = X1;
//  xElem.DOF(1) = X2;
//  xElem.DOF(2) = X3;
//  xElem.DOF(3) = X4;
//
//  xElem.dump( 2, output );
//  xElem.dumpTecplot( output );
//  BOOST_CHECK( output.match_pattern() );
//}

#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
