// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementProjection2D_L2_Triangle_AD_btest
// testing of 2-D element L2 projection operator

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "Field/Element/ElementProjection_L2.h"
#include "Field/Element/ElementLine.h"
#include "Field/Element/ElementArea.h"
#include "Field/Element/ElementXFieldArea.h"


using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementProjection_L2_Triangle_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementProjection_Const_L2_Legendre )
{
  typedef Real ArrayQ;
  typedef Element<ArrayQ, TopoD2, Triangle> ElementQFieldClass;

  int order = 0;
  ElementQFieldClass qfld0(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, qfld0.order() );
  BOOST_CHECK_EQUAL( 1, qfld0.nDOF() );

  ArrayQ aconst = 2;

  ElementProjectionConst_L2<TopoD2, Triangle> projector0(qfld0.basis());

  projector0.project( qfld0, aconst );

  Real tol = 1e-13;
  BOOST_CHECK_CLOSE( aconst, qfld0.eval({0.3, 0.3}), tol );
  BOOST_CHECK_CLOSE( aconst, qfld0.DOF(0), tol );

  order = 1;
  ElementQFieldClass qfld1(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 1, qfld1.order() );
  BOOST_CHECK_EQUAL( 3, qfld1.nDOF() );

  ElementProjectionConst_L2<TopoD2, Triangle> projector1(qfld1.basis());

  projector1.project( qfld1, aconst );

  BOOST_CHECK_CLOSE( aconst, qfld1.eval({0.3, 0.3}), tol );
  BOOST_CHECK_CLOSE( aconst, qfld1.DOF(0), tol );
  BOOST_CHECK_SMALL(         qfld1.DOF(1), tol );
  BOOST_CHECK_SMALL(         qfld1.DOF(2), tol );

  order = 2;
  ElementQFieldClass qfld2(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, qfld2.order() );
  BOOST_CHECK_EQUAL( 6, qfld2.nDOF() );

  ElementProjectionConst_L2<TopoD2, Triangle> projector2(qfld2.basis());

  projector2.project( qfld2, aconst );

  BOOST_CHECK_CLOSE( aconst, qfld2.eval({0.3, 0.3}), tol );
  BOOST_CHECK_CLOSE( aconst, qfld2.DOF(0), tol );
  BOOST_CHECK_SMALL(         qfld2.DOF(1), tol );
  BOOST_CHECK_SMALL(         qfld2.DOF(2), tol );
  BOOST_CHECK_SMALL(         qfld2.DOF(3), tol );
  BOOST_CHECK_SMALL(         qfld2.DOF(4), tol );
  BOOST_CHECK_SMALL(         qfld2.DOF(5), tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementProjection_Const_L2_Hierarchical )
{
  typedef Real ArrayQ;
  typedef Element<ArrayQ, TopoD2, Triangle> ElementQFieldClass;

  ArrayQ aconst = 2;

  int order = 1;
  ElementQFieldClass qfld1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfld1.order() );
  BOOST_CHECK_EQUAL( 3, qfld1.nDOF() );

  ElementProjectionConst_L2<TopoD2, Triangle> projector1(qfld1.basis());

  projector1.project( qfld1, aconst );

  Real tol = 1e-12;
  BOOST_CHECK_CLOSE( aconst, qfld1.eval({0.3, 0.3}), tol );
  BOOST_CHECK_CLOSE( aconst, qfld1.DOF(0), tol );
  BOOST_CHECK_CLOSE( aconst, qfld1.DOF(1), tol );
  BOOST_CHECK_CLOSE( aconst, qfld1.DOF(2), tol );

  order = 2;
  ElementQFieldClass qfld2(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qfld2.order() );
  BOOST_CHECK_EQUAL( 6, qfld2.nDOF() );

  ElementProjectionConst_L2<TopoD2, Triangle> projector2(qfld2.basis());

  projector2.project( qfld2, aconst );

  BOOST_CHECK_CLOSE( aconst, qfld2.eval({0.3, 0.3}), tol );
  BOOST_CHECK_CLOSE( aconst, qfld2.DOF(0), tol );
  BOOST_CHECK_CLOSE( aconst, qfld2.DOF(1), tol );
  BOOST_CHECK_CLOSE( aconst, qfld2.DOF(2), tol );
  BOOST_CHECK_SMALL(         qfld2.DOF(3), tol );
  BOOST_CHECK_SMALL(         qfld2.DOF(4), tol );
  BOOST_CHECK_SMALL(         qfld2.DOF(5), tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementProjection_Exact_L2_Exact )
{
  typedef Real ArrayQ;
  typedef Element<ArrayQ, TopoD2, Triangle> ElementQFieldClass;

  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;

  int order = 1;
  ElementQFieldClass qfld1(order, BasisFunctionCategory_Hierarchical);

  order = 2;
  ElementQFieldClass qfld2(order, BasisFunctionCategory_Lagrange);

  order = 3;
  ElementQFieldClass qfld1to3(order, BasisFunctionCategory_Legendre);
  ElementQFieldClass qfld2to3(order, BasisFunctionCategory_Hierarchical);

  for ( int i = 0; i < qfld1.nDOF(); i++)
    qfld1.DOF(i) = -i;

  BOOST_CHECK_EQUAL( 1, qfld1.order() );
  BOOST_CHECK_EQUAL( 3, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld2.order() );
  BOOST_CHECK_EQUAL( 6, qfld2.nDOF() );

  Real tol = 1e-11;

  Element_Projector_L2<TopoD2,Triangle> projector1(qfld2.basis());

  Element_Projector_L2<TopoD2,Triangle> projector1to3(qfld1to3.basis());
  Element_Projector_L2<TopoD2,Triangle> projector2to3(qfld2to3.basis());

  projector1.project(qfld1,qfld2);
  projector1to3.project(qfld2,qfld1to3);
  projector2to3.project(qfld2,qfld2to3);

  Quadrature<TopoD2, Triangle> quadrature(3); // capturing a linear solution
  const int nquad = quadrature.nQuadrature();

  RefCoordType RefCoord;  // reference-element coordinates

  ArrayQ qfld1_eval, qfld2_eval, qfld1to3_eval, qfld2to3_eval;

  //Check if the solutions are equal at a few quadrature points
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.coordinates( iquad, RefCoord );

    qfld1.eval(RefCoord, qfld1_eval);
    qfld2.eval( RefCoord, qfld2_eval);
    qfld1to3.eval( RefCoord, qfld1to3_eval);
    qfld2to3.eval( RefCoord, qfld2to3_eval);

    SANS_CHECK_CLOSE(qfld1_eval, qfld2_eval, tol, tol);
    SANS_CHECK_CLOSE(qfld1_eval, qfld1to3_eval, tol, tol);
    SANS_CHECK_CLOSE(qfld2_eval, qfld2to3_eval, tol, tol);
    SANS_CHECK_CLOSE(qfld1to3_eval, qfld2to3_eval, tol, tol);
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementProjection_Exact_L2_Hierarchical_P1_To_P3 )
{
  typedef Real ArrayQ;
  typedef Element<ArrayQ, TopoD2, Triangle> ElementQFieldClass;

  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;

  int order = 1;
  ElementQFieldClass qfld_p1(order, BasisFunctionCategory_Hierarchical);

  order = 3;
  ElementQFieldClass qfld_p3(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldClass qfld_p3_edgeflip(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfld_p1.order() );
  BOOST_CHECK_EQUAL( 3, qfld_p1.nDOF() );
  BOOST_CHECK_EQUAL( 3,  qfld_p3.order() );
  BOOST_CHECK_EQUAL( 10, qfld_p3.nDOF() );
  BOOST_CHECK_EQUAL( 3,  qfld_p3_edgeflip.order() );
  BOOST_CHECK_EQUAL( 10, qfld_p3_edgeflip.nDOF() );

  std::vector<std::array<int,Triangle::NEdge>> edgeSigns;

  // populate a vector of all possible edgeSign permutations
  for (int i = 0; i <= 1; i++)
    for (int j = 0; j <= 1; j++)
      for (int k = 0; k <= 1; k++)
      {
        std::array<int,Triangle::NEdge> edgeSign;
        edgeSign[0] = pow(-1,i);
        edgeSign[1] = pow(-1,j);
        edgeSign[2] = pow(-1,k);

        edgeSigns.push_back(edgeSign);
      }

  qfld_p1.setEdgeSign(edgeSigns.at(0));

  for ( int i = 0; i < qfld_p1.nDOF(); i++)
    qfld_p1.DOF(i) = -(i+1);


  RefCoordType RefCoord;  // reference-element coordinates
  Quadrature<TopoD2, Triangle> quadrature(3); // capturing a linear solution
  const int nquad = quadrature.nQuadrature();

  Quadrature<TopoD2, Triangle> highQuadrature(5); // capturing a linear solution
  const int nHquad = highQuadrature.nQuadrature();

  Real tol = 1e-11;

  ArrayQ qfld_p1_eval, qfld_p3_eval, qfld_p3_edgeflip_eval;

  Element_Projector_L2<TopoD2,Triangle> projector(qfld_p3.basis());
  Element_Projector_L2<TopoD2,Triangle> projector2(qfld_p3_edgeflip.basis());

  // P1 to P3
  for (std::size_t i = 0; i < edgeSigns.size(); i++)
  {
    qfld_p3.setEdgeSign( edgeSigns.at(i) ); // set a new set of edgeSigns

    projector.project( qfld_p1, qfld_p3 ); // P1 to P3

    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature.coordinates( iquad, RefCoord );

      qfld_p1.eval( RefCoord, qfld_p1_eval );
      qfld_p3.eval( RefCoord, qfld_p3_eval );
      SANS_CHECK_CLOSE( qfld_p1_eval, qfld_p3_eval, tol, tol );
    }

    // write the p3 with noise
    for (int dof = 0; dof < qfld_p3.nDOF(); dof++)
      qfld_p3.DOF(dof) = -(i+3);

    // P3 to P3, edgeSign combinatorics
    for (std::size_t j = 0; j < edgeSigns.size(); j++)
    {
      qfld_p3_edgeflip.setEdgeSign( edgeSigns.at(j) ); // change the p3 edge sign

      projector2.project( qfld_p3, qfld_p3_edgeflip );

      for (int iquad = 0; iquad < nHquad; iquad++)
      {
        highQuadrature.coordinates( iquad, RefCoord);

        qfld_p3.eval( RefCoord, qfld_p3_eval );
        qfld_p3_edgeflip.eval( RefCoord, qfld_p3_edgeflip_eval );

        SANS_CHECK_CLOSE( qfld_p3_eval, qfld_p3_edgeflip_eval, tol, tol );
      }
    }
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementProjection_Exact_L2_Hierarchical_P3_To_P3 )
{
  typedef Real ArrayQ;
  typedef Element<ArrayQ, TopoD2, Triangle> ElementQFieldClass;

  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;

  int order = 3;
  ElementQFieldClass qfldFrom_p3(order, BasisFunctionCategory_Hierarchical);

  order = 3;
  ElementQFieldClass qfldTo_p3(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldClass qfldTo_p3_edgeflip(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 3, qfldFrom_p3.order() );
  BOOST_CHECK_EQUAL( 10, qfldFrom_p3.nDOF() );
  BOOST_CHECK_EQUAL( 3,  qfldTo_p3.order() );
  BOOST_CHECK_EQUAL( 10, qfldTo_p3.nDOF() );
  BOOST_CHECK_EQUAL( 3,  qfldTo_p3_edgeflip.order() );
  BOOST_CHECK_EQUAL( 10, qfldTo_p3_edgeflip.nDOF() );

  std::vector<std::array<int,Triangle::NEdge>> edgeSigns;

  // populate a vector of all possible edgeSign permutations
  for (int i = 0; i <= 1; i++)
    for (int j = 0; j <= 1; j++)
      for (int k = 0; k <= 1; k++)
      {
        std::array<int,Triangle::NEdge> edgeSign;
        edgeSign[0] = pow(-1,i);
        edgeSign[1] = pow(-1,j);
        edgeSign[2] = pow(-1,k);

        edgeSigns.push_back(edgeSign);
      }

  qfldFrom_p3.setEdgeSign(edgeSigns.at(0));

  for ( int i = 0; i < qfldFrom_p3.nDOF(); i++)
    qfldFrom_p3.DOF(i) = -(i+1)%5;


  RefCoordType RefCoord;  // reference-element coordinates
  Quadrature<TopoD2, Triangle> quadrature(5); // capturing a linear solution
  const int nquad = quadrature.nQuadrature();

  Quadrature<TopoD2, Triangle> highQuadrature(5); // capturing a linear solution
  const int nHquad = highQuadrature.nQuadrature();

  Real tol = 1e-11;

  ArrayQ qfldFrom_p3_eval, qfldTo_p3_eval, qfldTo_p3_edgeflip_eval;

  Element_Projector_L2<TopoD2,Triangle> projector(qfldTo_p3.basis());
  Element_Projector_L2<TopoD2,Triangle> projector2(qfldTo_p3_edgeflip.basis());

  // P1 to P3
  for (std::size_t i = 0; i < edgeSigns.size(); i++)
  {
    qfldTo_p3.setEdgeSign( edgeSigns.at(i) ); // set a new set of edgeSigns

    projector.project( qfldFrom_p3, qfldTo_p3 ); // P1 to P3

    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature.coordinates( iquad, RefCoord );

      qfldFrom_p3.eval( RefCoord, qfldFrom_p3_eval );
      qfldTo_p3.eval( RefCoord, qfldTo_p3_eval );
      SANS_CHECK_CLOSE( qfldFrom_p3_eval, qfldTo_p3_eval, tol, tol );
    }

    // write the p3 with noise
    for (int dof = 0; dof < qfldTo_p3.nDOF(); dof++)
      qfldTo_p3.DOF(dof) = -(i+3)%5;

    // P3 to P3, edgeSign combinatorics
    for (std::size_t j = 0; j < edgeSigns.size(); j++)
    {
      qfldTo_p3_edgeflip.setEdgeSign( edgeSigns.at(j) ); // change the p3 edge sign

      projector2.project( qfldTo_p3, qfldTo_p3_edgeflip );

      for (int iquad = 0; iquad < nHquad; iquad++)
      {
        highQuadrature.coordinates( iquad, RefCoord);

        qfldTo_p3.eval( RefCoord, qfldTo_p3_eval );
        qfldTo_p3_edgeflip.eval( RefCoord, qfldTo_p3_edgeflip_eval );

        SANS_CHECK_CLOSE( qfldTo_p3_eval, qfldTo_p3_edgeflip_eval, tol, tol );
      }
    }
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementProjection_L2_LegendreP0 )
{
  typedef ScalarFunction2D_Const::ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2, TopoD2, Triangle> ElementXFieldClass;
  typedef Element<ArrayQ, TopoD2, Triangle>       ElementQFieldClass;

  typedef ScalarFunction2D_Const    ScalarFunctionConst;
  typedef ScalarFunction2D_Linear   ScalarFunctionLinear;
  typedef ScalarFunction2D_SineSine ScalarFunctionSine;

  // grid

  int order = 1;
  ElementXFieldClass xfld(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfld.order() );
  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfld.DOF(0) = {x1, y1};
  xfld.DOF(1) = {x2, y2};
  xfld.DOF(2) = {x3, y3};

  // solution

  order = 0;
  ElementQFieldClass qfld(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, qfld.order() );
  BOOST_CHECK_EQUAL( 1, qfld.nDOF() );

  // exact solution: const = 1
  Real aconst = 1;
  SolnNDConvertSpace<PhysD2, ScalarFunctionConst> solnExact0(aconst);

  ElementProjectionSolution_L2<TopoD2, Triangle> projector(qfld.basis());

  projector.project( xfld, qfld, solnExact0 );

  Real tol = 1e-13;
  BOOST_CHECK_CLOSE( 1, qfld.DOF(0), tol );

  // exact solution: linear = a0 + ax*x + ay*y
  Real a0 =  1;
  Real ax = -3;
  Real ay =  4;
  SolnNDConvertSpace<PhysD2, ScalarFunctionLinear> solnExact1(a0, ax, ay);

  projector.project( xfld, qfld, solnExact1 );

  tol = 1e-12;
  BOOST_CHECK_CLOSE( a0 + (ax + ay)/3., qfld.DOF(0), tol );

  // exact solution: sin(2 PI x)*sin(2 PI y)
  SolnNDConvertSpace<PhysD2, ScalarFunctionSine> solnExact2;

  projector.project( xfld, qfld, solnExact2 );

  tol = 1e-14;
  BOOST_CHECK_SMALL( qfld.DOF(0), tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementProjection_L2_HierarchicalP1_test )
{
  typedef ScalarFunction2D_Const::ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2, TopoD2, Triangle> ElementXFieldClass;
  typedef Element<ArrayQ, TopoD2, Triangle>       ElementQFieldClass;

  typedef ScalarFunction2D_Const    ScalarFunctionConst;
  typedef ScalarFunction2D_Linear   ScalarFunctionLinear;
  typedef ScalarFunction2D_SineSine ScalarFunctionSine;

  // grid

  int order = 1;
  ElementXFieldClass xfld(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfld.order() );
  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfld.DOF(0) = {x1, y1};
  xfld.DOF(1) = {x2, y2};
  xfld.DOF(2) = {x3, y3};

  // solution

  order = 1;
  ElementQFieldClass qfld(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfld.order() );
  BOOST_CHECK_EQUAL( 3, qfld.nDOF() );

  // exact solution: const = 1
  Real aconst = 1;
  SolnNDConvertSpace<PhysD2, ScalarFunctionConst> solnExact0(aconst);

  ElementProjectionSolution_L2<TopoD2, Triangle> projector(qfld.basis());

  projector.project( xfld, qfld, solnExact0 );

  Real tol = 1e-13;
  BOOST_CHECK_CLOSE( 1, qfld.DOF(0), tol );
  BOOST_CHECK_CLOSE( 1, qfld.DOF(1), tol );
  BOOST_CHECK_CLOSE( 1, qfld.DOF(2), tol );

  // exact solution: linear = a0 + ax*x + ay*y
  Real a0 =  1;
  Real ax = -3;
  Real ay =  4;
  SolnNDConvertSpace<PhysD2, ScalarFunctionLinear> solnExact1(a0, ax, ay);

  projector.project( xfld, qfld, solnExact1 );

  tol = 1e-12;
  BOOST_CHECK_CLOSE( a0,      qfld.DOF(0), tol );
  BOOST_CHECK_CLOSE( a0 + ax, qfld.DOF(1), tol );
  BOOST_CHECK_CLOSE( a0 + ay, qfld.DOF(2), tol );

  // exact solution: sin(2 PI x)*sin(2 PI y)
  SolnNDConvertSpace<PhysD2, ScalarFunctionSine> solnExact2;

  projector.project( xfld, qfld, solnExact2 );

  Real q1 =  9./(PI*PI);
  Real q2 = -9./(2*PI*PI);
  Real q3 = -9./(2*PI*PI);

  tol = 1e-11;
  BOOST_CHECK_CLOSE( q1, qfld.DOF(0), tol );
  BOOST_CHECK_CLOSE( q2, qfld.DOF(1), tol );
  BOOST_CHECK_CLOSE( q3, qfld.DOF(2), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementProjectionSolutionGradient_L2_LegendreP1_test )
{
  typedef ScalarFunction2D_Linear::ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2, TopoD2, Triangle>           ElementXFieldClass;
  typedef Element<DLA::VectorS<2,ArrayQ>, TopoD2, Triangle> ElementQFieldClass;

  typedef ScalarFunction2D_Linear   ScalarFunctionLinear;

  // grid
  int order = 1;
  ElementXFieldClass xfld(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfld.order() );
  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfld.DOF(0) = {x1, y1};
  xfld.DOF(1) = {x2, y2};
  xfld.DOF(2) = {x3, y3};

  // solution

  order = 1;
  ElementQFieldClass qfld(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfld.order() );
  BOOST_CHECK_EQUAL( 3, qfld.nDOF() );

  // exact solution: linear = a0 + ax*x + ay*y
  Real a0 =  1;
  Real ax = -3;
  Real ay =  4;
  SolnNDConvertSpace<PhysD2, ScalarFunctionLinear> solnExact1(a0, ax, ay);

  ElementProjectionSolutionGradient_L2<TopoD2, Triangle> projector( qfld.basis() );

  projector.project( xfld, qfld, solnExact1 );

  Real tol = 1e-13;
  BOOST_CHECK_CLOSE( ax, qfld.DOF(0)[0], tol );
  BOOST_CHECK_CLOSE( ax, qfld.DOF(1)[0], tol );
  BOOST_CHECK_CLOSE( ax, qfld.DOF(2)[0], tol );

  BOOST_CHECK_CLOSE( ay, qfld.DOF(0)[1], tol );
  BOOST_CHECK_CLOSE( ay, qfld.DOF(1)[1], tol );
  BOOST_CHECK_CLOSE( ay, qfld.DOF(2)[1], tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Element_Subdivision_Projector_P1 )
{

  typedef ElementXField<PhysD2, TopoD2, Triangle> ElementXFieldClass_2D;

  Real tol = 1e-12;

  int order = 1;
  int split_edge_index = -1;

  //--------------2D triangles-----------------

  ElementXFieldClass_2D xfldElem2D_main(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldClass_2D xfldElem2D_sub(order, BasisFunctionCategory_Hierarchical);

  Element_Subdivision_Projector<TopoD2, Triangle> projector(xfldElem2D_sub.basis());

  BOOST_CHECK_EQUAL( 1, xfldElem2D_main.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem2D_main.nDOF() );

  xfldElem2D_main.DOF(0) = {0.20,-0.40};
  xfldElem2D_main.DOF(1) = {0.70,-0.80};
  xfldElem2D_main.DOF(2) = {0.50, 0.30};

  // cppcheck-suppress redundantAssignment
  split_edge_index = 0;
  projector.project( xfldElem2D_main, xfldElem2D_sub, ElementSplitType::Edge, split_edge_index, 0 );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[0], 0.20, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[0], 0.70, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[0], 0.60, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[1], -0.25, tol );

  projector.project( xfldElem2D_main, xfldElem2D_sub, ElementSplitType::Edge, split_edge_index, 1 );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[0], 0.20, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[0], 0.60, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[1], -0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[0], 0.50, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[1],  0.30, tol );

  split_edge_index = 1;
  projector.project( xfldElem2D_main, xfldElem2D_sub, ElementSplitType::Edge, split_edge_index, 0 );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[0], 0.35, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[1], -0.05, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[0], 0.70, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[0], 0.50, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[1],  0.30, tol );

  projector.project( xfldElem2D_main, xfldElem2D_sub, ElementSplitType::Edge, split_edge_index, 1 );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[0], 0.20, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[0], 0.70, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[0], 0.35, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[1], -0.05, tol );

  split_edge_index = 2;
  projector.project( xfldElem2D_main, xfldElem2D_sub, ElementSplitType::Edge, split_edge_index, 0 );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[0], 0.20, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[0], 0.45, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[1], -0.60, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[0], 0.50, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[1],  0.30, tol );

  projector.project( xfldElem2D_main, xfldElem2D_sub, ElementSplitType::Edge, split_edge_index, 1 );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[0], 0.45, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[1], -0.60, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[0], 0.70, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[0], 0.50, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[1],  0.30, tol );

  split_edge_index = -1;
  projector.project( xfldElem2D_main, xfldElem2D_sub, ElementSplitType::Isotropic, split_edge_index, 0 );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[0], 0.45, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[1], -0.60, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[0], 0.70, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[0], 0.60, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[1], -0.25, tol );

  projector.project( xfldElem2D_main, xfldElem2D_sub, ElementSplitType::Isotropic, split_edge_index, 1 );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[0], 0.35, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[1], -0.05, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[0], 0.60, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[1], -0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[0], 0.50, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[1],  0.30, tol );

  projector.project( xfldElem2D_main, xfldElem2D_sub, ElementSplitType::Isotropic, split_edge_index, 2 );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[0], 0.20, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[1], -0.40, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[0], 0.45, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[1], -0.60, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[0], 0.35, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[1], -0.05, tol );

  projector.project( xfldElem2D_main, xfldElem2D_sub, ElementSplitType::Isotropic, split_edge_index, 3 );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[0], 0.60, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(0)[1], -0.25, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[0], 0.35, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(1)[1], -0.05, tol );
  BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[0], 0.45, tol ); BOOST_CHECK_CLOSE( xfldElem2D_sub.DOF(2)[1], -0.60, tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Element_Subdivision_Projector_Hierarchical_P1_to_P3 )
{
  typedef Real ArrayQ;
  typedef Element<ArrayQ, TopoD2, Triangle> ElementQFieldClass;

  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;

  int order = 1;
  ElementQFieldClass qfld_p1(order, BasisFunctionCategory_Hierarchical);

  order = 3;
  ElementQFieldClass qfld_p3(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldClass qfld_p3_edgeflip(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfld_p1.order() );
  BOOST_CHECK_EQUAL( 3, qfld_p1.nDOF() );
  BOOST_CHECK_EQUAL( 3,  qfld_p3.order() );
  BOOST_CHECK_EQUAL( 10, qfld_p3.nDOF() );
  BOOST_CHECK_EQUAL( 3,  qfld_p3_edgeflip.order() );
  BOOST_CHECK_EQUAL( 10, qfld_p3_edgeflip.nDOF() );

  std::vector<std::array<int,Triangle::NEdge>> edgeSigns;

  // populate a vector of all possible edgeSign permutations
  for (int i = 0; i <= 1; i++)
    for (int j = 0; j <= 1; j++)
      for (int k = 0; k <= 1; k++)
      {
        std::array<int,Triangle::NEdge> edgeSign;
        edgeSign[0] = pow(-1,i);
        edgeSign[1] = pow(-1,j);
        edgeSign[2] = pow(-1,k);

        edgeSigns.push_back(edgeSign);
      }

  qfld_p1.setEdgeSign(edgeSigns.at(0));

  for ( int i = 0; i < qfld_p1.nDOF(); i++)
    qfld_p1.DOF(i) = -(i+1);


  RefCoordType RefCoord_main, RefCoord_sub;  // reference-element coordinates
  Quadrature<TopoD2, Triangle> quadrature(3); // capturing a linear solution
  const int nquad = quadrature.nQuadrature();

  Quadrature<TopoD2, Triangle> highQuadrature(5); // capturing a p3 solution
  const int nHquad = highQuadrature.nQuadrature();

  Real tol = 1e-11;

  ArrayQ qfld_p1_eval, qfld_p3_eval, qfld_p3_edgeflip_eval;

  Element_Subdivision_Projector<TopoD2, Triangle> projector(qfld_p3.basis());

  for (int edge = 0; edge < 3; edge++) // loop around canonical edges
    for (int subEdge = 0; subEdge < 2; subEdge++) // loop over pair of sub elements in split
      // P1 to P3
      for (std::size_t i = 0; i < edgeSigns.size(); i++)
      {
        qfld_p3.setEdgeSign( edgeSigns.at(i) ); // set a new set of edgeSigns

        // P1 to P3
        projector.project( qfld_p1, qfld_p3, ElementSplitType::Edge, edge, subEdge );

        for (int iquad = 0; iquad < nquad; iquad++)
        {
          quadrature.coordinates( iquad, RefCoord_sub );

          BasisFunction_RefElement_Split<TopoD2,Triangle>::
            transform( RefCoord_sub, ElementSplitType::Edge, edge, subEdge, RefCoord_main);

          qfld_p1.eval( RefCoord_main, qfld_p1_eval );
          qfld_p3.eval( RefCoord_sub, qfld_p3_eval );
          SANS_CHECK_CLOSE( qfld_p1_eval, qfld_p3_eval, tol, tol );
        }

        // write the p3 with noise
        for (int dof = 0; dof < qfld_p3.nDOF(); dof++)
          qfld_p3.DOF(dof) = -(i+3);

        // P3 to P3, edgeSign combinatorics
        for (std::size_t j = 0; j < edgeSigns.size(); j++)
        {
          qfld_p3_edgeflip.setEdgeSign( edgeSigns.at(j) ); // change the p3 edge sign

          projector.project( qfld_p3, qfld_p3_edgeflip, ElementSplitType::Edge, edge, subEdge );

          for (int iquad = 0; iquad < nHquad; iquad++)
          {
            highQuadrature.coordinates( iquad, RefCoord_sub);

            BasisFunction_RefElement_Split<TopoD2,Triangle>::
              transform( RefCoord_sub, ElementSplitType::Edge, edge, subEdge, RefCoord_main);

            qfld_p3.eval( RefCoord_main, qfld_p3_eval );
            qfld_p3_edgeflip.eval( RefCoord_sub, qfld_p3_edgeflip_eval );

            SANS_CHECK_CLOSE( qfld_p3_eval, qfld_p3_edgeflip_eval, tol, tol );
          }
        }
      }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Element_Subdivision_Projector_Hierarchical_P3_to_P3 )
{
  typedef Real ArrayQ;
  typedef Element<ArrayQ, TopoD2, Triangle> ElementQFieldClass;

  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;

  int order = 3;
  ElementQFieldClass qfldFrom_p3(order, BasisFunctionCategory_Hierarchical);

  order = 3;
  ElementQFieldClass qfldTo_p3(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldClass qfldTo_p3_edgeflip(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 3, qfldFrom_p3.order() );
  BOOST_CHECK_EQUAL( 10, qfldFrom_p3.nDOF() );
  BOOST_CHECK_EQUAL( 3,  qfldTo_p3.order() );
  BOOST_CHECK_EQUAL( 10, qfldTo_p3.nDOF() );
  BOOST_CHECK_EQUAL( 3,  qfldTo_p3_edgeflip.order() );
  BOOST_CHECK_EQUAL( 10, qfldTo_p3_edgeflip.nDOF() );

  std::vector<std::array<int,Triangle::NEdge>> edgeSigns;

  // populate a vector of all possible edgeSign permutations
  for (int i = 0; i <= 1; i++)
    for (int j = 0; j <= 1; j++)
      for (int k = 0; k <= 1; k++)
      {
        std::array<int,Triangle::NEdge> edgeSign;
        edgeSign[0] = pow(-1,i);
        edgeSign[1] = pow(-1,j);
        edgeSign[2] = pow(-1,k);

        edgeSigns.push_back(edgeSign);
      }

  qfldFrom_p3.setEdgeSign(edgeSigns.at(0));

  for ( int i = 0; i < qfldFrom_p3.nDOF(); i++)
    qfldFrom_p3.DOF(i) = -(i+1)%5;


  RefCoordType RefCoord_main, RefCoord_sub;  // reference-element coordinates
  Quadrature<TopoD2, Triangle> quadrature(5); // capturing a linear solution
  const int nquad = quadrature.nQuadrature();

  Quadrature<TopoD2, Triangle> highQuadrature(5); // capturing a p3 solution
  const int nHquad = highQuadrature.nQuadrature();

  Real tol = 1e-11;

  ArrayQ qfldFrom_p3_eval, qfldTo_p3_eval, qfldTo_p3_edgeflip_eval;

  Element_Subdivision_Projector<TopoD2, Triangle> projector(qfldTo_p3.basis());

  for (int edge = 0; edge < 3; edge++) // loop around canonical edges
    for (int subEdge = 0; subEdge < 2; subEdge++) // loop over pair of sub elements in split
      // P1 to P3
      for (std::size_t i = 0; i < edgeSigns.size(); i++)
      {
        qfldTo_p3.setEdgeSign( edgeSigns.at(i) ); // set a new set of edgeSigns

        // P1 to P3
        projector.project( qfldFrom_p3, qfldTo_p3, ElementSplitType::Edge, edge, subEdge );

        for (int iquad = 0; iquad < nquad; iquad++)
        {
          quadrature.coordinates( iquad, RefCoord_sub );

          BasisFunction_RefElement_Split<TopoD2,Triangle>::
            transform( RefCoord_sub, ElementSplitType::Edge, edge, subEdge, RefCoord_main);

          qfldFrom_p3.eval( RefCoord_main, qfldFrom_p3_eval );
          qfldTo_p3.eval( RefCoord_sub, qfldTo_p3_eval );
          SANS_CHECK_CLOSE( qfldFrom_p3_eval, qfldTo_p3_eval, tol, tol );
        }

        // write the p3 with noise
        for (int dof = 0; dof < qfldTo_p3.nDOF(); dof++)
          qfldTo_p3.DOF(dof) = -(i+3)%5;

        // P3 to P3, edgeSign combinatorics
        for (std::size_t j = 0; j < edgeSigns.size(); j++)
        {
          qfldTo_p3_edgeflip.setEdgeSign( edgeSigns.at(j) ); // change the p3 edge sign

          projector.project( qfldTo_p3, qfldTo_p3_edgeflip, ElementSplitType::Edge, edge, subEdge );

          for (int iquad = 0; iquad < nHquad; iquad++)
          {
            highQuadrature.coordinates( iquad, RefCoord_sub);

            BasisFunction_RefElement_Split<TopoD2,Triangle>::
              transform( RefCoord_sub, ElementSplitType::Edge, edge, subEdge, RefCoord_main);

            qfldTo_p3.eval( RefCoord_main, qfldTo_p3_eval );
            qfldTo_p3_edgeflip.eval( RefCoord_sub, qfldTo_p3_edgeflip_eval );

            SANS_CHECK_CLOSE( qfldTo_p3_eval, qfldTo_p3_edgeflip_eval, tol, tol );
          }
        }
      }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Element_Subdivision_CellToTrace_Projector_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Element<ArrayQ, TopoD1, Line> ElementClassTrace;
  typedef Element<ArrayQ, TopoD2, Triangle> ElementClassCell;

  Real tol = 1e-12;

  int order = 1;

  ElementClassTrace fldElem_trace(order, BasisFunctionCategory_Hierarchical);
  ElementClassCell fldElem_cell(order, BasisFunctionCategory_Hierarchical);

  Element_Subdivision_CellToTrace_Projector<TopoD1, Line> projector(fldElem_trace.basis());

  BOOST_CHECK_EQUAL( 1, fldElem_trace.order() );
  BOOST_CHECK_EQUAL( 2, fldElem_trace.nDOF() );

  BOOST_CHECK_EQUAL( 1, fldElem_cell.order() );
  BOOST_CHECK_EQUAL( 3, fldElem_cell.nDOF() );

  fldElem_cell.DOF(0) = {0.25,-0.40};
  fldElem_cell.DOF(1) = {0.75,-0.80};
  fldElem_cell.DOF(2) = {1.25,-1.20};

  int split_edge_index = 0;

  projector.project( fldElem_cell, fldElem_trace, ElementSplitType::Edge, split_edge_index, 0 );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(0)[0],  1.00, tol );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(0)[1], -1.00, tol );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(1)[0],  0.25, tol );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(1)[1], -0.40, tol );

  split_edge_index = 1;
  projector.project( fldElem_cell, fldElem_trace, ElementSplitType::Edge, split_edge_index, 0 );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(0)[0],  0.75, tol );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(0)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(1)[0],  0.75, tol );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(1)[1], -0.80, tol );

  split_edge_index = 2;
  projector.project( fldElem_cell, fldElem_trace, ElementSplitType::Edge, split_edge_index, 0 );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(0)[0],  0.50, tol );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(0)[1], -0.60, tol );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(1)[0],  1.25, tol );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(1)[1], -1.20, tol );

  split_edge_index = -1;
  projector.project( fldElem_cell, fldElem_trace, ElementSplitType::Isotropic, split_edge_index, 0 );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(0)[0],  1.00, tol );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(0)[1], -1.00, tol );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(1)[0],  0.50, tol );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(1)[1], -0.60, tol );

  projector.project( fldElem_cell, fldElem_trace, ElementSplitType::Isotropic, split_edge_index, 1 );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(0)[0],  0.75, tol );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(0)[1], -0.80, tol );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(1)[0],  1.00, tol );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(1)[1], -1.00, tol );

  projector.project( fldElem_cell, fldElem_trace, ElementSplitType::Isotropic, split_edge_index, 2 );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(0)[0],  0.50, tol );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(0)[1], -0.60, tol );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(1)[0],  0.75, tol );
  BOOST_CHECK_CLOSE( fldElem_trace.DOF(1)[1], -0.80, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Element_Trace_Projection_L2_Hierarchical_P1 )
{
  static const int D = PhysD2::D;
  typedef ElementXField<PhysD2, TopoD2, Triangle> ElementXFieldClass_2D;

  Real tol = 1e-12;

  int order = 1;

  //--------------2D triangles-----------------

  ElementXFieldClass_2D xfldElem2D_0(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldClass_2D xfldElem2D_1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem2D_0.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem2D_0.nDOF() );

  DLA::VectorS<D,Real> v0 = {0.25,-0.40};
  DLA::VectorS<D,Real> v1 = {0.75,-0.80};
  DLA::VectorS<D,Real> v2 = {0.37, 0.68};

  xfldElem2D_0.DOF(0) = v0;
  xfldElem2D_0.DOF(1) = v1;
  xfldElem2D_0.DOF(2) = v2;

  Element_Trace_Projection_L2( xfldElem2D_0, 1, xfldElem2D_1, 1 );
  for (int i = 0; i < D; i++)
  {
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[i], v0[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[i], v1[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[i], v2[i], tol );
  }

  Element_Trace_Projection_L2( xfldElem2D_0, 1, xfldElem2D_1, 2 );
  for (int i = 0; i < D; i++)
  {
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[i], v1[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[i], v2[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[i], v0[i], tol );
  }

  Element_Trace_Projection_L2( xfldElem2D_0, 1, xfldElem2D_1, 3 );
  for (int i = 0; i < D; i++)
  {
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[i], v2[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[i], v0[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[i], v1[i], tol );
  }

  Element_Trace_Projection_L2( xfldElem2D_0, 2, xfldElem2D_1, 1 );
  for (int i = 0; i < D; i++)
  {
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[i], v2[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[i], v0[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[i], v1[i], tol );
  }

  Element_Trace_Projection_L2( xfldElem2D_0, 2, xfldElem2D_1, 2 );
  for (int i = 0; i < D; i++)
  {
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[i], v0[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[i], v1[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[i], v2[i], tol );
  }

  Element_Trace_Projection_L2( xfldElem2D_0, 2, xfldElem2D_1, 3 );
  for (int i = 0; i < D; i++)
  {
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[i], v1[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[i], v2[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[i], v0[i], tol );
  }

  Element_Trace_Projection_L2( xfldElem2D_0, 3, xfldElem2D_1, 1 );
  for (int i = 0; i < D; i++)
  {
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[i], v1[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[i], v2[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[i], v0[i], tol );
  }

  Element_Trace_Projection_L2( xfldElem2D_0, 3, xfldElem2D_1, 2 );
  for (int i = 0; i < D; i++)
  {
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[i], v2[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[i], v0[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[i], v1[i], tol );
  }

  Element_Trace_Projection_L2( xfldElem2D_0, 3, xfldElem2D_1, 3 );
  for (int i = 0; i < D; i++)
  {
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[i], v0[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[i], v1[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[i], v2[i], tol );
  }

  Element_Trace_Projection_L2( xfldElem2D_0, 1, xfldElem2D_1, -1 );
  for (int i = 0; i < D; i++)
  {
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[i], v0[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[i], v2[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[i], v1[i], tol );
  }

  Element_Trace_Projection_L2( xfldElem2D_0, 1, xfldElem2D_1, -2 );
  for (int i = 0; i < D; i++)
  {
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[i], v1[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[i], v0[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[i], v2[i], tol );
  }

  Element_Trace_Projection_L2( xfldElem2D_0, 1, xfldElem2D_1, -3 );
  for (int i = 0; i < D; i++)
  {
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[i], v2[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[i], v1[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[i], v0[i], tol );
  }

  Element_Trace_Projection_L2( xfldElem2D_0, 2, xfldElem2D_1, -1 );
  for (int i = 0; i < D; i++)
  {
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[i], v2[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[i], v1[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[i], v0[i], tol );
  }

  Element_Trace_Projection_L2( xfldElem2D_0, 2, xfldElem2D_1, -2 );
  for (int i = 0; i < D; i++)
  {
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[i], v0[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[i], v2[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[i], v1[i], tol );
  }

  Element_Trace_Projection_L2( xfldElem2D_0, 2, xfldElem2D_1, -3 );
  for (int i = 0; i < D; i++)
  {
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[i], v1[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[i], v0[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[i], v2[i], tol );
  }

  Element_Trace_Projection_L2( xfldElem2D_0, 3, xfldElem2D_1, -1 );
  for (int i = 0; i < D; i++)
  {
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[i], v1[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[i], v0[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[i], v2[i], tol );
  }

  Element_Trace_Projection_L2( xfldElem2D_0, 3, xfldElem2D_1, -2 );
  for (int i = 0; i < D; i++)
  {
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[i], v2[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[i], v1[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[i], v0[i], tol );
  }

  Element_Trace_Projection_L2( xfldElem2D_0, 3, xfldElem2D_1, -3 );
  for (int i = 0; i < D; i++)
  {
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(0)[i], v0[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(1)[i], v2[i], tol );
    BOOST_CHECK_CLOSE( xfldElem2D_1.DOF(2)[i], v1[i], tol );
  }

  BOOST_CHECK_THROW( Element_Trace_Projection_L2( xfldElem2D_0, 0, xfldElem2D_1, 0 ), DeveloperException );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
