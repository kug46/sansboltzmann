// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementXFieldVolume_4D_Tetrahedron_btest
// testing of ElementXFieldVolume class w/ Tetrahedron in 4D

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/stringify.h"     // Real
#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"
#include "Field/Element/ElementXFieldSpacetime.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class ElementXField<PhysD4,TopoD3,Tet>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementXFieldVolume_4D_Tetrahedron_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( unitNormal_test )
{

  const Real small_tol = 1e-13;
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysD4, TopoD3, Tet> xElem(order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 4, xElem.nDOF() );

  ElementXField<PhysD4, TopoD3, Tet>::RefCoordType sRef;
  ElementXField<PhysD4, TopoD3, Tet>::VectorX N;
  ElementXField<PhysD4, TopoD3, Tet>::VectorX N2;
  ElementXField<PhysD4, TopoD3, Tet>::VectorX N3;
  ElementXField<PhysD4, TopoD3, Tet>::VectorX N4;
  Real nxTrue, nyTrue, nzTrue, ntTrue;

  sRef = 1./4.;

  {
    xElem.DOF(0) = {0, 0, 0, 0};
    xElem.DOF(1) = {1, 0, 0, 0};
    xElem.DOF(2) = {0, 1, 0, 0};
    xElem.DOF(3) = {0, 0, 1, 0};

    nxTrue =  0;
    nyTrue =  0;
    nzTrue =  0;
    ntTrue =  1;

    xElem.unitNormal( sRef, N );
    xElem.unitNormal( QuadraturePoint<TopoD3>(sRef), N2 );
    xElem.unitNormal( QuadraturePoint<TopoD3>(QuadratureRule::eGauss, 1, 1, sRef), N3 );

    BOOST_CHECK_EQUAL(N[0], N2[0]);
    BOOST_CHECK_EQUAL(N[1], N2[1]);
    BOOST_CHECK_EQUAL(N[2], N2[2]);
    BOOST_CHECK_EQUAL(N[3], N2[3]);

    BOOST_CHECK_EQUAL(N[0], N3[0]);
    BOOST_CHECK_EQUAL(N[1], N3[1]);
    BOOST_CHECK_EQUAL(N[2], N3[2]);
    BOOST_CHECK_EQUAL(N[3], N3[3]);

    SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
    SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
    SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );
    SANS_CHECK_CLOSE( ntTrue, N[3], small_tol, tol );
  }

  {
    xElem.DOF(0) = {0, 0, 0, 0};
    xElem.DOF(1) = {0, 1, 0, 0};
    xElem.DOF(2) = {1, 0, 0, 0};
    xElem.DOF(3) = {0, 0, 1, 0};

    nxTrue =  0;
    nyTrue =  0;
    nzTrue =  0;
    ntTrue = -1;

    xElem.unitNormal( sRef, N );
    xElem.unitNormal( QuadraturePoint<TopoD3>(sRef), N2 );
    xElem.unitNormal( QuadraturePoint<TopoD3>(QuadratureRule::eGauss, 1, 1, sRef), N3 );

    BOOST_CHECK_EQUAL(N[0], N2[0]);
    BOOST_CHECK_EQUAL(N[1], N2[1]);
    BOOST_CHECK_EQUAL(N[2], N2[2]);
    BOOST_CHECK_EQUAL(N[3], N2[3]);

    BOOST_CHECK_EQUAL(N[0], N3[0]);
    BOOST_CHECK_EQUAL(N[1], N3[1]);
    BOOST_CHECK_EQUAL(N[2], N3[2]);
    BOOST_CHECK_EQUAL(N[3], N3[3]);

    SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
    SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
    SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );
    SANS_CHECK_CLOSE( ntTrue, N[3], small_tol, tol );
  }

  {
    xElem.DOF(0) = {0, 0, 0, 0};
    xElem.DOF(1) = {0, 0, 1, 0};
    xElem.DOF(2) = {0, 1, 0, 0};
    xElem.DOF(3) = {1, 0, 0, 0};

    nxTrue =  0;
    nyTrue =  0;
    nzTrue =  0;
    ntTrue = -1;

    xElem.unitNormal( sRef, N );
    xElem.unitNormal( QuadraturePoint<TopoD3>(sRef), N2 );
    xElem.unitNormal( QuadraturePoint<TopoD3>(QuadratureRule::eGauss, 1, 1, sRef), N3 );

    BOOST_CHECK_EQUAL(N[0], N2[0]);
    BOOST_CHECK_EQUAL(N[1], N2[1]);
    BOOST_CHECK_EQUAL(N[2], N2[2]);
    BOOST_CHECK_EQUAL(N[3], N2[3]);

    BOOST_CHECK_EQUAL(N[0], N3[0]);
    BOOST_CHECK_EQUAL(N[1], N3[1]);
    BOOST_CHECK_EQUAL(N[2], N3[2]);
    BOOST_CHECK_EQUAL(N[3], N3[3]);

    SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
    SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
    SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );
    SANS_CHECK_CLOSE( ntTrue, N[3], small_tol, tol );
  }

  {
    xElem.DOF(0) = {0, 0, 0, 0};
    xElem.DOF(1) = {1, 0, 0, 0};
    xElem.DOF(2) = {0, 0, 1, 0};
    xElem.DOF(3) = {0, 0, 0, 1};

    nxTrue =  0;
    nyTrue = 1;
    nzTrue =  0;
    ntTrue =  0;

    ElementXField<PhysD4, TopoD3, Tet>::VectorX d1,d2,d3;

    xElem.unitNormal( sRef, N );
    xElem.unitNormal( QuadraturePoint<TopoD3>(sRef), N2 );
    xElem.unitNormal( QuadraturePoint<TopoD3>(QuadratureRule::eGauss, 1, 1, sRef), N3 );

    BOOST_CHECK_EQUAL(N[0], N2[0]);
    BOOST_CHECK_EQUAL(N[1], N2[1]);
    BOOST_CHECK_EQUAL(N[2], N2[2]);
    BOOST_CHECK_EQUAL(N[3], N2[3]);

    BOOST_CHECK_EQUAL(N[0], N3[0]);
    BOOST_CHECK_EQUAL(N[1], N3[1]);
    BOOST_CHECK_EQUAL(N[2], N3[2]);
    BOOST_CHECK_EQUAL(N[3], N3[3]);
    d1= xElem.DOF(1) - xElem.DOF(0);
    d2= xElem.DOF(2) - xElem.DOF(0);
    d3= xElem.DOF(3) - xElem.DOF(0);
    SANS_CHECK_CLOSE( 0.0 , dot(d1,N) , small_tol , tol );
    SANS_CHECK_CLOSE( 0.0 , dot(d2,N) , small_tol , tol );
    SANS_CHECK_CLOSE( 0.0 , dot(d3,N) , small_tol , tol );

    SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
    SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
    SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );
    SANS_CHECK_CLOSE( ntTrue, N[3], small_tol, tol );
  }

  {
    xElem.DOF(0) = {1, 0, 0, 0};
    xElem.DOF(1) = {0, 1, 0, 0};
    xElem.DOF(2) = {0, 0, 1, 0};
    xElem.DOF(3) = {0, 0, 0, 0};

    nxTrue =  0;
    nyTrue =  0;
    nzTrue =  0;
    ntTrue =  -1;

    xElem.unitNormal( sRef, N );
    xElem.unitNormal( QuadraturePoint<TopoD3>(sRef), N2 );
    xElem.unitNormal( QuadraturePoint<TopoD3>(QuadratureRule::eGauss, 1, 1, sRef), N3 );

    BOOST_CHECK_EQUAL(N[0], N2[0]);
    BOOST_CHECK_EQUAL(N[1], N2[1]);
    BOOST_CHECK_EQUAL(N[2], N2[2]);
    BOOST_CHECK_EQUAL(N[3], N2[3]);

    BOOST_CHECK_EQUAL(N[0], N3[0]);
    BOOST_CHECK_EQUAL(N[1], N3[1]);
    BOOST_CHECK_EQUAL(N[2], N3[2]);
    BOOST_CHECK_EQUAL(N[3], N3[3]);

    SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
    SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
    SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );
    SANS_CHECK_CLOSE( ntTrue, N[3], small_tol, tol );

    xElem.DOF(0) = {1,  0, 0, 0};
    xElem.DOF(1) = {0,  0, 1, 0};
    xElem.DOF(2) = {1, -1, 1, 0};
    xElem.DOF(3) = {0, 0, 0, 0};

    xElem.unitNormal( sRef, N );
    xElem.unitNormal( QuadraturePoint<TopoD3>(sRef), N2 );
    xElem.unitNormal( QuadraturePoint<TopoD3>(QuadratureRule::eGauss, 1, 1, sRef), N3 );

    BOOST_CHECK_EQUAL(N[0], N2[0]);
    BOOST_CHECK_EQUAL(N[1], N2[1]);
    BOOST_CHECK_EQUAL(N[2], N2[2]);
    BOOST_CHECK_EQUAL(N[3], N2[3]);

    BOOST_CHECK_EQUAL(N[0], N3[0]);
    BOOST_CHECK_EQUAL(N[1], N3[1]);
    BOOST_CHECK_EQUAL(N[2], N3[2]);
    BOOST_CHECK_EQUAL(N[3], N3[3]);

    SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
    SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
    SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );
    SANS_CHECK_CLOSE( ntTrue, N[3], small_tol, tol );
  }

  // loop through all the positive orientations
  ElementXField<PhysD4,TopoD4,Pentatope> pentatope( order,BasisFunctionCategory_Lagrange );
  pentatope.DOF(0) = {0,0,0,0};
  pentatope.DOF(1) = {1,0,0,0};
  pentatope.DOF(2) = {0,1,0,0};
  pentatope.DOF(3) = {0,0,1,0};
  pentatope.DOF(4) = {0,0,0,1};
  const int (*OrientPos)[Tet::NTrace] = TraceToCellRefCoord<Tet,TopoD4,Pentatope>::OrientPos;
  const int (*OrientNeg)[Tet::NTrace] = TraceToCellRefCoord<Tet,TopoD4,Pentatope>::OrientNeg;

  // the canonical traces are oriented outwards
  const int (*TraceNodes)[Tet::NNode] = TraceToCellRefCoord<Tet,TopoD4,Pentatope>::TraceNodes;

  for (int trace=0;trace<Pentatope::NTrace;trace++)
  for (int orient=0;orient<Tet::NPermutation;orient++)
  {
    std::vector<int> used(Pentatope::NNode,-1);

    // check the positive orientation of the canonical trace which creates an inward pointing normal
    {
      for (int vertex=0;vertex<Tet::NNode;vertex++)
      {
        int v = TraceNodes[trace][ OrientPos[orient][vertex] ];
        xElem.DOF(vertex) = pentatope.DOF(v);
        used[v] = 1;
      }

      // which vertex is not used? this determines the normal coordinates
      std::vector<int>::iterator it = std::find(used.begin(),used.end(),-1);
      BOOST_REQUIRE( it!=used.end() );
      int d = std::distance(used.begin(),it);

      // assign the true normal coordinates
      std::vector<Real> normal( PhysD4::D , 0. );
      if (d==0)
      {
        // any positive permutation of {1,2,3,4} points inwards
        std::fill( normal.begin() , normal.end() , 0.5 );
      }
      else
      {
        // it's really the d-1 coordinate we didn't use since the first vertex of pentatope is (0,0,0,0)
        // this is the positive trace orientation, aka outward pointing normal
        normal[d-1] = -1.;
      }

      // compute the normal
      xElem.unitNormal( sRef , N );

      SANS_CHECK_CLOSE( N[0] , normal[0] , small_tol , tol );
      SANS_CHECK_CLOSE( N[1] , normal[1] , small_tol , tol );
      SANS_CHECK_CLOSE( N[2] , normal[2] , small_tol , tol );
      SANS_CHECK_CLOSE( N[3] , normal[3] , small_tol , tol );
    }

    // check the negative permutation of the canonical trace
    {
      for (int vertex=0;vertex<Tet::NNode;vertex++)
      {
        int v = TraceNodes[trace][ OrientNeg[orient][vertex] ];
        xElem.DOF(vertex) = pentatope.DOF(v);
        used[v] = 1;
      }

      // which vertex is not used? this determines the normal coordinates
      std::vector<int>::iterator it = std::find(used.begin(),used.end(),-1);
      BOOST_REQUIRE( it!=used.end() );
      int d = std::distance(used.begin(),it);

      // assign the true normal coordinates
      std::vector<Real> normal( PhysD4::D , 0. );
      if (d==0)
      {
        // any negative permutation of {1,2,3,4} points outward
        std::fill( normal.begin() , normal.end() , -0.5 );
      }
      else
      {
        // it's really the d-1 coordinate we didn't use since the first vertex of pentatope is (0,0,0,0)
        // this is the negative trace orientation, aka inward pointing normal
        normal[d-1] = 1.;
      }

      // compute the normal
      xElem.unitNormal( sRef, N );
      xElem.unitNormal( QuadraturePoint<TopoD3>(sRef), N2 );
      xElem.unitNormal( QuadraturePoint<TopoD3>(QuadratureRule::eGauss, 1, 1, sRef), N3 );
      xElem.unitNormal( QuadratureCellTracePoint<TopoD3>(sRef), N4 );

      BOOST_CHECK_EQUAL(N[0], N2[0]);
      BOOST_CHECK_EQUAL(N[1], N2[1]);
      BOOST_CHECK_EQUAL(N[2], N2[2]);
      BOOST_CHECK_EQUAL(N[3], N2[3]);

      BOOST_CHECK_EQUAL(N[0], N3[0]);
      BOOST_CHECK_EQUAL(N[1], N3[1]);
      BOOST_CHECK_EQUAL(N[2], N3[2]);
      BOOST_CHECK_EQUAL(N[3], N3[3]);

      BOOST_CHECK_EQUAL(N[0], N4[0]);
      BOOST_CHECK_EQUAL(N[1], N4[1]);
      BOOST_CHECK_EQUAL(N[2], N4[2]);
      BOOST_CHECK_EQUAL(N[3], N4[3]);

      SANS_CHECK_CLOSE( N[0] , normal[0] , small_tol , tol );
      SANS_CHECK_CLOSE( N[1] , normal[1] , small_tol , tol );
      SANS_CHECK_CLOSE( N[2] , normal[2] , small_tol , tol );
      SANS_CHECK_CLOSE( N[3] , normal[3] , small_tol , tol );
    }
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BoundingBox_test )
{
  BoundingBox<PhysD4> bbox;

  ElementXField<PhysD4, TopoD3, Tet> xElemQ1(1, BasisFunctionCategory_Lagrange);

  BOOST_REQUIRE_EQUAL( 1, xElemQ1.order() );
  BOOST_REQUIRE_EQUAL( 4, xElemQ1.nDOF() );

  xElemQ1.DOF(0) = {0, 0, 0, 0}; // Node 0
  xElemQ1.DOF(1) = {1, 0, 0, 0}; // Node 1
  xElemQ1.DOF(2) = {0, 1, 0, 0}; // Node 2
  xElemQ1.DOF(3) = {0, 0, 1, 0}; // Node 3

  bbox = xElemQ1.boundingBox();

  BOOST_CHECK_EQUAL( 0, bbox.low_bounds()[0] );
  BOOST_CHECK_EQUAL( 0, bbox.low_bounds()[1] );
  BOOST_CHECK_EQUAL( 0, bbox.low_bounds()[2] );
  BOOST_CHECK_EQUAL( 1, bbox.high_bounds()[0] );
  BOOST_CHECK_EQUAL( 1, bbox.high_bounds()[1] );
  BOOST_CHECK_EQUAL( 1, bbox.high_bounds()[2] );
}

BOOST_AUTO_TEST_CASE(Volume_test)
{
  ElementXField<PhysD4, TopoD3, Tet> xElemQ1(1, BasisFunctionCategory_Lagrange);

  BOOST_REQUIRE_EQUAL( 1, xElemQ1.order() );
  BOOST_REQUIRE_EQUAL( 4, xElemQ1.nDOF() );

  xElemQ1.DOF(0) = {0, 0, 0, 0}; // Node 0
  xElemQ1.DOF(1) = {1, 0, 0, 0}; // Node 1
  xElemQ1.DOF(2) = {0, 1, 0, 0}; // Node 2
  xElemQ1.DOF(3) = {0, 0, 1, 0}; // Node 3

  Real volume= xElemQ1.volume();

  BOOST_CHECK_EQUAL(volume, 1.0/6.0);

//  typename ElementXField<PhysD4, TopoD3, Tet>::RefCoordType Ref;
//  Ref[0]= Tet::centerRef[0];
//  Ref[1]= Tet::centerRef[1];
//  Ref[2]= Tet::centerRef[2];

  // check alternate constructor
  QuadraturePoint<TopoD3> Quad= QuadraturePoint<TopoD3>(Tet::centerRef);

  Real volume2= xElemQ1.volume(Quad);

  BOOST_CHECK_EQUAL(volume2, volume);

}

// BOOST_AUTO_TEST_CASE(unitNormalPointsInside)
// {
//
//   const Real small_tol= 1e-13;
//   const Real tol= 1e-13;
//
//   int order= 1;
//   ElementXField<PhysD4, TopoD3, Tet> xElem(order, BasisFunctionCategory_Lagrange);
//
//   // loop through all the positive orientations
//   ElementXField<PhysD4, TopoD4, Pentatope> pentatope(order, BasisFunctionCategory_Lagrange);
//   pentatope.DOF(0)= {0,0,0,0};
//   pentatope.DOF(1)= {1,0,0,0};
//   pentatope.DOF(2)= {0,1,0,0};
//   pentatope.DOF(3)= {0,0,1,0};
//   pentatope.DOF(4)= {0,0,0,1};
//   const int (*OrientPos)[Tet::NTrace]= TraceToCellRefCoord<Tet, TopoD4, Pentatope>::OrientPos;
//   const int (*OrientNeg)[Tet::NTrace]= TraceToCellRefCoord<Tet, TopoD4, Pentatope>::OrientNeg;
//
//   // the canonical traces are oriented outwards??
//   const int (*TraceNodes)[Tet::NNode]= TraceToCellRefCoord<Tet, TopoD4, Pentatope>::TraceNodes;
//
//
//
//
//   for (int trace= 0; trace < Pentatope::NTrace; trace++)
//   for (int orient= 0; orient < Tet::NPermutation; orient++)
//   {
//     // vector to see if a node is used on a given vertex
//     std::vector<int> used(Pentatope::NNode, -1);
//
//     // check the positive orientations of the canonical trace which should
//     // all create inward pointing normals!
//     {
//       for (int vertex= 0; vertex < Tet::NNode; vertex++)
//       {
//         int v= TraceNodes[trace][OrientPos[orient][vertex]];
//         xElen.DOF(vertex)= pentatope.DOF(v);
//         used[v]= 1;
//       }
//
//       //
//
//
//     }
//
//
//   }
//
// }

BOOST_AUTO_TEST_CASE(CurvedBoxPoints)
{

  ElementXField<PhysD4, TopoD3, Tet> xElem(2, BasisFunctionCategory_Lagrange);

  BOOST_REQUIRE_EQUAL(10, xElem.nDOF());

  xElem.DOF(0)= {0, 0, 0, 0};
  xElem.DOF(1)= {1, 0, 0, 0};
  xElem.DOF(2)= {0, 1, 0, 0};
  xElem.DOF(3)= {0, 0, 1, 0};
  xElem.DOF(4)= {0.5, 0, 0, 0};
  xElem.DOF(5)= {0, 0.5, 0, 0};
  xElem.DOF(6)= {0, 0, 0.5, 0};
  xElem.DOF(7)= {0.5, 0.5, 0, 0};
  xElem.DOF(8)= {0.5, 0, 0.5, 0};
  xElem.DOF(9)= {0, 0.5, 0.5, 0};


  BOOST_CHECK_THROW(xElem.boundingBox(), DeveloperException);

}

BOOST_AUTO_TEST_CASE(ImpliedMetric)
{

  DLA::MatrixSymS<PhysD4::D, Real> M;
  ElementXField<PhysD4, TopoD3, Tet> xElem(1, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_THROW(xElem.impliedMetric(Tet::centerRef[0], Tet::centerRef[1], Tet::centerRef[2], M), DeveloperException);

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
