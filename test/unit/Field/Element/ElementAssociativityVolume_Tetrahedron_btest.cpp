// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementAssociativityVolume_Tetrahedron_btest
// testing of ElementAssociativityVolume classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "Topology/ElementTopology.h"
#include "Field/Element/ElementAssociativityVolume.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementAssociativityVolume_Tetrahedron_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctorsVolume )
{
  ElementAssociativity<TopoD3,Tet> assoc1( BasisFunctionVolumeBase<Tet>::HierarchicalP1 );
  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( 4, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc1.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc1.nCell() );
  BOOST_CHECK_EQUAL( 4, assoc1.nDOF() );

  ElementAssociativity<TopoD3,Tet> assoc2( BasisFunctionVolumeBase<Tet>::HierarchicalP2 );
  BOOST_CHECK_EQUAL( 2, assoc2.order() );
  BOOST_CHECK_EQUAL( 4, assoc2.nNode() );
  BOOST_CHECK_EQUAL( 6, assoc2.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc1.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc2.nCell() );
  BOOST_CHECK_EQUAL( 10, assoc2.nDOF() );

#if 0
  ElementAssociativity<TopoD3,Tet> assoc2b( BasisFunctionVolumeBase<Tet>::LegendreP2 );

  BOOST_CHECK_EQUAL( 2, assoc2b.order() );
  BOOST_CHECK_EQUAL( 0, assoc2b.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc2b.nEdge() );
  BOOST_CHECK_EQUAL( 6, assoc2b.nCell() );
  BOOST_CHECK_EQUAL( 6, assoc2b.nDOF() );

  ElementAssociativity<TopoD3,Tet> assoc3(assoc2);

  BOOST_CHECK_EQUAL( 2, assoc3.order() );
  BOOST_CHECK_EQUAL( 3, assoc3.nNode() );
  BOOST_CHECK_EQUAL( 3, assoc3.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc3.nCell() );
  BOOST_CHECK_EQUAL( 6, assoc3.nDOF() );

  ElementAssociativity<TopoD3,Tet> assoc3b(assoc2b);

  BOOST_CHECK_EQUAL( 2, assoc3b.order() );
  BOOST_CHECK_EQUAL( 0, assoc3b.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc3b.nEdge() );
  BOOST_CHECK_EQUAL( 6, assoc3b.nCell() );
  BOOST_CHECK_EQUAL( 6, assoc3b.nDOF() );

  // default ctor and operator=
  ElementAssociativity<TopoD3,Tet> assoc4;
  assoc4 = assoc3;

  BOOST_CHECK_EQUAL( 2, assoc4.order() );
  BOOST_CHECK_EQUAL( 3, assoc4.nNode() );
  BOOST_CHECK_EQUAL( 3, assoc4.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc4.nCell() );
  BOOST_CHECK_EQUAL( 6, assoc4.nDOF() );

  ElementAssociativity<TopoD3,Tet> assoc4b;
  assoc4b = assoc3b;

  BOOST_CHECK_EQUAL( 2, assoc4b.order() );
  BOOST_CHECK_EQUAL( 0, assoc4b.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc4b.nEdge() );
  BOOST_CHECK_EQUAL( 6, assoc4b.nCell() );
  BOOST_CHECK_EQUAL( 6, assoc4b.nDOF() );
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctorsVolumeConstructor )
{
  typedef ElementAssociativityConstructor<TopoD3,Tet> Constructor;

  Constructor assoc1( BasisFunctionVolumeBase<Tet>::HierarchicalP1 );
  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( 4, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc1.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc1.nCell() );
  BOOST_CHECK_EQUAL( 4, assoc1.nDOF() );

  Constructor assoc2( BasisFunctionVolumeBase<Tet>::HierarchicalP2 );
  BOOST_CHECK_EQUAL( 2, assoc2.order() );
  BOOST_CHECK_EQUAL( 4, assoc2.nNode() );
  BOOST_CHECK_EQUAL( 6, assoc2.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc1.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc2.nCell() );
  BOOST_CHECK_EQUAL( 10, assoc2.nDOF() );

#if 0
  ElementAssociativityConstructor<TopoD3, Tet> assoc2b( BasisFunctionVolumeBase<Tet>::LegendreP2 );

  BOOST_CHECK_EQUAL( 2, assoc2b.order() );
  BOOST_CHECK_EQUAL( 0, assoc2b.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc2b.nEdge() );
  BOOST_CHECK_EQUAL( 6, assoc2b.nCell() );
  BOOST_CHECK_EQUAL( 6, assoc2b.nDOF() );

  // default ctor and operator=
  ElementAssociativityConstructor<TopoD3, Tet> assoc4;
  assoc4 = assoc2;

  BOOST_CHECK_EQUAL( 2, assoc4.order() );
  BOOST_CHECK_EQUAL( 3, assoc4.nNode() );
  BOOST_CHECK_EQUAL( 3, assoc4.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc4.nCell() );
  BOOST_CHECK_EQUAL( 6, assoc4.nDOF() );

  ElementAssociativityConstructor<TopoD3, Tet> assoc4b;
  assoc4b = assoc2b;

  BOOST_CHECK_EQUAL( 2, assoc4b.order() );
  BOOST_CHECK_EQUAL( 0, assoc4b.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc4b.nEdge() );
  BOOST_CHECK_EQUAL( 6, assoc4b.nCell() );
  BOOST_CHECK_EQUAL( 6, assoc4b.nDOF() );
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fcnsVolume )
{
  //Linear element

  typedef ElementAssociativityConstructor<TopoD3,Tet> Constructor;
  Constructor assoc1ctor( BasisFunctionVolumeBase<Tet>::HierarchicalP1 );

  BOOST_CHECK_EQUAL( 1, assoc1ctor.order() );
  BOOST_CHECK_EQUAL( 4, assoc1ctor.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1ctor.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc1ctor.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc1ctor.nCell() );

  assoc1ctor.setRank( 2 );
  BOOST_CHECK_EQUAL( 2, assoc1ctor.rank() );

  int nodeTrue[4] = {3, 4, 5, 6};
  int node[4];

  node[0] = nodeTrue[0];
  node[1] = nodeTrue[1];
  node[2] = nodeTrue[2];
  node[3] = nodeTrue[3];
  assoc1ctor.setNodeGlobalMapping( node, 4 );
  assoc1ctor.getNodeGlobalMapping( node, 4 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );

  node[0] = assoc1ctor.nodeGlobal(0);
  node[1] = assoc1ctor.nodeGlobal(1);
  node[2] = assoc1ctor.nodeGlobal(2);
  node[3] = assoc1ctor.nodeGlobal(3);
  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );

  assoc1ctor.getGlobalMapping( node, 4 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );


  ElementAssociativity<TopoD3,Tet> assoc1( assoc1ctor );

  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( 4, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc1.nCell() );

  BOOST_CHECK_EQUAL( 2, assoc1.rank() );

  node[0] = node[1] = node[2] = node[3] = -1;
  assoc1.getNodeGlobalMapping( node, 4 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );

  node[0] = assoc1.nodeGlobal(0);
  node[1] = assoc1.nodeGlobal(1);
  node[2] = assoc1.nodeGlobal(2);
  node[3] = assoc1.nodeGlobal(3);
  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );

  node[0] = node[1] = node[2] = node[3] = -1;
  assoc1.getGlobalMapping( node, 4 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );


  //Quadratic element

  Constructor assoc2ctor( BasisFunctionVolumeBase<Tet>::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, assoc2ctor.order() );
  BOOST_CHECK_EQUAL( 4, assoc2ctor.nNode() );
  BOOST_CHECK_EQUAL( 6, assoc2ctor.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc2ctor.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc2ctor.nCell() );

  assoc2ctor.setRank( 2 );
  BOOST_CHECK_EQUAL( 2, assoc2ctor.rank() );

  node[0] = nodeTrue[0];
  node[1] = nodeTrue[1];
  node[2] = nodeTrue[2];
  node[3] = nodeTrue[3];
  assoc2ctor.setNodeGlobalMapping( node, 4 );
  assoc2ctor.getNodeGlobalMapping( node, 4 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );

  int edgeTrue_P2[6] = {10,11,12,13,14,15};
  int edge_P2[6];

  edge_P2[0] = edgeTrue_P2[0];
  edge_P2[1] = edgeTrue_P2[1];
  edge_P2[2] = edgeTrue_P2[2];
  edge_P2[3] = edgeTrue_P2[3];
  edge_P2[4] = edgeTrue_P2[4];
  edge_P2[5] = edgeTrue_P2[5];

  //setting edgeDOF vector one by one through face edges (checking all positive orientations)
  const int edge_index[3] = {0,1,2};

  for (int orient=0; orient<3; orient++) //loop across all positive orientations
  {
    //working out the edge index in the possibly rotated triangle face, which gives the same edgeTrue in the tet
    int edge_ind0 = edge_index[(3-orient)%3];
    int edge_ind1 = edge_index[(4-orient)%3];
    int edge_ind2 = edge_index[(5-orient)%3];

    assoc2ctor.setEdgeGlobalMapping({edge_P2[0]}, CanonicalTraceToCell(0,orient+1), edge_ind0); //face 0, edge 0
    assoc2ctor.setEdgeGlobalMapping({edge_P2[1]}, CanonicalTraceToCell(0,orient+1), edge_ind1); //face 0, edge 1
    assoc2ctor.setEdgeGlobalMapping({edge_P2[2]}, CanonicalTraceToCell(0,orient+1), edge_ind2); //face 0, edge 2
    BOOST_CHECK_EQUAL( edgeTrue_P2[0], assoc2ctor.edgeGlobal(0) );
    BOOST_CHECK_EQUAL( edgeTrue_P2[1], assoc2ctor.edgeGlobal(1) );
    BOOST_CHECK_EQUAL( edgeTrue_P2[2], assoc2ctor.edgeGlobal(2) );

    assoc2ctor.setEdgeGlobalMapping({edge_P2[0]}, CanonicalTraceToCell(1,orient+1), edge_ind0); //face 1, edge 0
    assoc2ctor.setEdgeGlobalMapping({edge_P2[3]}, CanonicalTraceToCell(1,orient+1), edge_ind1); //face 1, edge 1
    assoc2ctor.setEdgeGlobalMapping({edge_P2[4]}, CanonicalTraceToCell(1,orient+1), edge_ind2); //face 1, edge 2
    BOOST_CHECK_EQUAL( edgeTrue_P2[0], assoc2ctor.edgeGlobal(0) );
    BOOST_CHECK_EQUAL( edgeTrue_P2[3], assoc2ctor.edgeGlobal(3) );
    BOOST_CHECK_EQUAL( edgeTrue_P2[4], assoc2ctor.edgeGlobal(4) );

    assoc2ctor.setEdgeGlobalMapping({edge_P2[1]}, CanonicalTraceToCell(2,orient+1), edge_ind0); //face 2, edge 0
    assoc2ctor.setEdgeGlobalMapping({edge_P2[4]}, CanonicalTraceToCell(2,orient+1), edge_ind1); //face 2, edge 1
    assoc2ctor.setEdgeGlobalMapping({edge_P2[5]}, CanonicalTraceToCell(2,orient+1), edge_ind2); //face 2, edge 2
    BOOST_CHECK_EQUAL( edgeTrue_P2[1], assoc2ctor.edgeGlobal(1) );
    BOOST_CHECK_EQUAL( edgeTrue_P2[4], assoc2ctor.edgeGlobal(4) );
    BOOST_CHECK_EQUAL( edgeTrue_P2[5], assoc2ctor.edgeGlobal(5) );

    assoc2ctor.setEdgeGlobalMapping({edge_P2[2]}, CanonicalTraceToCell(3,orient+1), edge_ind0); //face 3, edge 0
    assoc2ctor.setEdgeGlobalMapping({edge_P2[5]}, CanonicalTraceToCell(3,orient+1), edge_ind1); //face 3, edge 1
    assoc2ctor.setEdgeGlobalMapping({edge_P2[3]}, CanonicalTraceToCell(3,orient+1), edge_ind2); //face 3, edge 2
    BOOST_CHECK_EQUAL( edgeTrue_P2[2], assoc2ctor.edgeGlobal(2) );
    BOOST_CHECK_EQUAL( edgeTrue_P2[5], assoc2ctor.edgeGlobal(5) );
    BOOST_CHECK_EQUAL( edgeTrue_P2[3], assoc2ctor.edgeGlobal(3) );
  }

  //setting full edgeDOF vector at once
  assoc2ctor.setEdgeGlobalMapping( edge_P2, 6 );
  assoc2ctor.getEdgeGlobalMapping( edge_P2, 6 );

  BOOST_CHECK_EQUAL( edgeTrue_P2[0], edge_P2[0] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[1], edge_P2[1] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[2], edge_P2[2] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[3], edge_P2[3] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[4], edge_P2[4] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[5], edge_P2[5] );

  node[0] = assoc2ctor.nodeGlobal(0);
  node[1] = assoc2ctor.nodeGlobal(1);
  node[2] = assoc2ctor.nodeGlobal(2);
  node[3] = assoc2ctor.nodeGlobal(3);

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );

  edge_P2[0] = assoc2ctor.edgeGlobal(0);
  edge_P2[1] = assoc2ctor.edgeGlobal(1);
  edge_P2[2] = assoc2ctor.edgeGlobal(2);
  edge_P2[3] = assoc2ctor.edgeGlobal(3);
  edge_P2[4] = assoc2ctor.edgeGlobal(4);
  edge_P2[5] = assoc2ctor.edgeGlobal(5);

  BOOST_CHECK_EQUAL( edgeTrue_P2[0], edge_P2[0] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[1], edge_P2[1] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[2], edge_P2[2] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[3], edge_P2[3] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[4], edge_P2[4] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[5], edge_P2[5] );

  int mapDOF[10];
  assoc2ctor.getGlobalMapping( mapDOF, 10 );

  BOOST_CHECK_EQUAL( nodeTrue[0], mapDOF[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], mapDOF[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], mapDOF[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], mapDOF[3] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[0], mapDOF[4] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[1], mapDOF[5] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[2], mapDOF[6] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[3], mapDOF[7] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[4], mapDOF[8] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[5], mapDOF[9] );


  ElementAssociativity<TopoD3,Tet> assoc2( assoc2ctor );

  BOOST_CHECK_EQUAL( 2, assoc2.order() );
  BOOST_CHECK_EQUAL( 4, assoc2.nNode() );
  BOOST_CHECK_EQUAL( 6, assoc2.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc2.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc2.nCell() );

  BOOST_CHECK_EQUAL( 2, assoc2.rank() );

  node[0] = node[1] = node[2] = node[3] = -1;
  assoc2.getNodeGlobalMapping( node, 4 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );

  node[0] = assoc2.nodeGlobal(0);
  node[1] = assoc2.nodeGlobal(1);
  node[2] = assoc2.nodeGlobal(2);
  node[3] = assoc2.nodeGlobal(3);
  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );

  edge_P2[0] = edge_P2[1] = edge_P2[2] = edge_P2[3] = edge_P2[4] = edge_P2[5] = -1;
  assoc2.getEdgeGlobalMapping( edge_P2, 6 );

  BOOST_CHECK_EQUAL( edgeTrue_P2[0], edge_P2[0] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[1], edge_P2[1] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[2], edge_P2[2] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[3], edge_P2[3] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[4], edge_P2[4] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[5], edge_P2[5] );

  edge_P2[0] = assoc2.edgeGlobal(0);
  edge_P2[1] = assoc2.edgeGlobal(1);
  edge_P2[2] = assoc2.edgeGlobal(2);
  edge_P2[3] = assoc2.edgeGlobal(3);
  edge_P2[4] = assoc2.edgeGlobal(4);
  edge_P2[5] = assoc2.edgeGlobal(5);

  BOOST_CHECK_EQUAL( edgeTrue_P2[0], edge_P2[0] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[1], edge_P2[1] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[2], edge_P2[2] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[3], edge_P2[3] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[4], edge_P2[4] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[5], edge_P2[5] );

  for (int i=0; i<10; i++)
    mapDOF[i] = -1;

  assoc2.getGlobalMapping( mapDOF, 10 );

  BOOST_CHECK_EQUAL( nodeTrue[0], mapDOF[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], mapDOF[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], mapDOF[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], mapDOF[3] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[0], mapDOF[4] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[1], mapDOF[5] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[2], mapDOF[6] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[3], mapDOF[7] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[4], mapDOF[8] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[5], mapDOF[9] );

#if 0

  // Cubic element
  ElementAssociativityConstructor<TopoD3, Tet> assoc4ctor( BasisFunctionVolumeBase<Tet>::HierarchicalP3 );

  BOOST_CHECK_EQUAL( 3, assoc4ctor.order() );
  BOOST_CHECK_EQUAL( 3, assoc4ctor.nNode() );
  BOOST_CHECK_EQUAL( 6, assoc4ctor.nEdge() );
  BOOST_CHECK_EQUAL( 1, assoc4ctor.nCell() );

  assoc4ctor.setNodeGlobalMapping( {3, 4, 5} );
  assoc4ctor.getNodeGlobalMapping( node, 3 );

  BOOST_CHECK_EQUAL( 3, node[0] );
  BOOST_CHECK_EQUAL( 4, node[1] );
  BOOST_CHECK_EQUAL( 5, node[2] );

  int edgeCub[6];
  int cellCub[1];

  assoc4ctor.setEdgeGlobalMapping( {9, 3, 7, 2, 5, 1} );
  assoc4ctor.getEdgeGlobalMapping( edgeCub, 6 );

  BOOST_CHECK_EQUAL( 9, edgeCub[0] );
  BOOST_CHECK_EQUAL( 3, edgeCub[1] );
  BOOST_CHECK_EQUAL( 7, edgeCub[2] );
  BOOST_CHECK_EQUAL( 2, edgeCub[3] );
  BOOST_CHECK_EQUAL( 5, edgeCub[4] );
  BOOST_CHECK_EQUAL( 1, edgeCub[5] );

  assoc4ctor.setCellGlobalMapping( {4} );
  assoc4ctor.getCellGlobalMapping( cellCub, 1 );

  BOOST_CHECK_EQUAL( 4, cellCub[0] );

  node[0] = assoc4ctor.nodeGlobal(0);
  node[1] = assoc4ctor.nodeGlobal(1);
  node[2] = assoc4ctor.nodeGlobal(2);
  BOOST_CHECK_EQUAL( 3, node[0] );
  BOOST_CHECK_EQUAL( 4, node[1] );
  BOOST_CHECK_EQUAL( 5, node[2] );

  edgeCub[0] = assoc4ctor.edgeGlobal(0);
  edgeCub[1] = assoc4ctor.edgeGlobal(1);
  edgeCub[2] = assoc4ctor.edgeGlobal(2);
  edgeCub[3] = assoc4ctor.edgeGlobal(3);
  edgeCub[4] = assoc4ctor.edgeGlobal(4);
  edgeCub[5] = assoc4ctor.edgeGlobal(5);
  BOOST_CHECK_EQUAL( 9, edgeCub[0] );
  BOOST_CHECK_EQUAL( 3, edgeCub[1] );
  BOOST_CHECK_EQUAL( 7, edgeCub[2] );
  BOOST_CHECK_EQUAL( 2, edgeCub[3] );
  BOOST_CHECK_EQUAL( 5, edgeCub[4] );
  BOOST_CHECK_EQUAL( 1, edgeCub[5] );

  cellCub[0] = assoc4ctor.cellGlobal(0);
  BOOST_CHECK_EQUAL( 4, cellCub[0] );

  int mapDOF3[10] = {-1};
  int globalP3True[10] = {4, 8, 2, 3, 6, 1, 7, 5, 9, 4};
  assoc4ctor.setGlobalMapping( globalP3True, 10 );
  assoc4ctor.getGlobalMapping( mapDOF3, 10 );

  BOOST_CHECK_EQUAL( globalP3True[0], mapDOF3[0] );
  BOOST_CHECK_EQUAL( globalP3True[1], mapDOF3[1] );
  BOOST_CHECK_EQUAL( globalP3True[2], mapDOF3[2] );
  BOOST_CHECK_EQUAL( globalP3True[3], mapDOF3[3] );
  BOOST_CHECK_EQUAL( globalP3True[4], mapDOF3[4] );
  BOOST_CHECK_EQUAL( globalP3True[5], mapDOF3[5] );
  BOOST_CHECK_EQUAL( globalP3True[6], mapDOF3[6] );
  BOOST_CHECK_EQUAL( globalP3True[7], mapDOF3[7] );
  BOOST_CHECK_EQUAL( globalP3True[8], mapDOF3[8] );
  BOOST_CHECK_EQUAL( globalP3True[9], mapDOF3[9] );

  int globalP3True2[10] = {5, 9, 3, 1, 7, 2, 4, 8, 6, 3};
  assoc4ctor.setGlobalMapping( {5, 9, 3, 1, 7, 2, 4, 8, 6, 3} );
  assoc4ctor.getGlobalMapping( mapDOF3, 10 );

  BOOST_CHECK_EQUAL( globalP3True2[0], mapDOF3[0] );
  BOOST_CHECK_EQUAL( globalP3True2[1], mapDOF3[1] );
  BOOST_CHECK_EQUAL( globalP3True2[2], mapDOF3[2] );
  BOOST_CHECK_EQUAL( globalP3True2[3], mapDOF3[3] );
  BOOST_CHECK_EQUAL( globalP3True2[4], mapDOF3[4] );
  BOOST_CHECK_EQUAL( globalP3True2[5], mapDOF3[5] );
  BOOST_CHECK_EQUAL( globalP3True2[6], mapDOF3[6] );
  BOOST_CHECK_EQUAL( globalP3True2[7], mapDOF3[7] );
  BOOST_CHECK_EQUAL( globalP3True2[8], mapDOF3[8] );
  BOOST_CHECK_EQUAL( globalP3True2[9], mapDOF3[9] );


  ElementAssociativityConstructor<TopoD3, Tet> assoc4( assoc4ctor );

  BOOST_CHECK_EQUAL( 3, assoc4.order() );
  BOOST_CHECK_EQUAL( 3, assoc4.nNode() );
  BOOST_CHECK_EQUAL( 6, assoc4.nEdge() );
  BOOST_CHECK_EQUAL( 1, assoc4.nCell() );

  node[0] = node[1] = node[2] = -1;
  assoc4.getNodeGlobalMapping( node, 3 );

  BOOST_CHECK_EQUAL( globalP3True2[0], node[0] );
  BOOST_CHECK_EQUAL( globalP3True2[1], node[1] );
  BOOST_CHECK_EQUAL( globalP3True2[2], node[2] );

  for ( int i=0; i<6; i++) edgeCub[i] = -1;
  assoc4.getEdgeGlobalMapping( edgeCub, 6 );

  BOOST_CHECK_EQUAL( globalP3True2[3], edgeCub[0] );
  BOOST_CHECK_EQUAL( globalP3True2[4], edgeCub[1] );
  BOOST_CHECK_EQUAL( globalP3True2[5], edgeCub[2] );
  BOOST_CHECK_EQUAL( globalP3True2[6], edgeCub[3] );
  BOOST_CHECK_EQUAL( globalP3True2[7], edgeCub[4] );
  BOOST_CHECK_EQUAL( globalP3True2[8], edgeCub[5] );

  cellCub[0] = -1;
  assoc4.getCellGlobalMapping( cellCub, 1 );

  BOOST_CHECK_EQUAL( globalP3True2[9], cellCub[0] );

  node[0] = assoc4.nodeGlobal(0);
  node[1] = assoc4.nodeGlobal(1);
  node[2] = assoc4.nodeGlobal(2);
  BOOST_CHECK_EQUAL( globalP3True2[0], node[0] );
  BOOST_CHECK_EQUAL( globalP3True2[1], node[1] );
  BOOST_CHECK_EQUAL( globalP3True2[2], node[2] );

  edgeCub[0] = assoc4.edgeGlobal(0);
  edgeCub[1] = assoc4.edgeGlobal(1);
  edgeCub[2] = assoc4.edgeGlobal(2);
  edgeCub[3] = assoc4.edgeGlobal(3);
  edgeCub[4] = assoc4.edgeGlobal(4);
  edgeCub[5] = assoc4.edgeGlobal(5);
  BOOST_CHECK_EQUAL( globalP3True2[3], edgeCub[0] );
  BOOST_CHECK_EQUAL( globalP3True2[4], edgeCub[1] );
  BOOST_CHECK_EQUAL( globalP3True2[5], edgeCub[2] );
  BOOST_CHECK_EQUAL( globalP3True2[6], edgeCub[3] );
  BOOST_CHECK_EQUAL( globalP3True2[7], edgeCub[4] );
  BOOST_CHECK_EQUAL( globalP3True2[8], edgeCub[5] );

  cellCub[0] = assoc4.cellGlobal(0);
  BOOST_CHECK_EQUAL( globalP3True2[9], cellCub[0] );

  for ( int i=0; i<10; i++) mapDOF3[i] = -1;
  assoc4.getGlobalMapping( mapDOF3, 10 );

  BOOST_CHECK_EQUAL( globalP3True2[0], mapDOF3[0] );
  BOOST_CHECK_EQUAL( globalP3True2[1], mapDOF3[1] );
  BOOST_CHECK_EQUAL( globalP3True2[2], mapDOF3[2] );
  BOOST_CHECK_EQUAL( globalP3True2[3], mapDOF3[3] );
  BOOST_CHECK_EQUAL( globalP3True2[4], mapDOF3[4] );
  BOOST_CHECK_EQUAL( globalP3True2[5], mapDOF3[5] );
  BOOST_CHECK_EQUAL( globalP3True2[6], mapDOF3[6] );
  BOOST_CHECK_EQUAL( globalP3True2[7], mapDOF3[7] );
  BOOST_CHECK_EQUAL( globalP3True2[8], mapDOF3[8] );
  BOOST_CHECK_EQUAL( globalP3True2[9], mapDOF3[9] );
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( facesignVolume )
{
  typedef std::array<int,4> Int4;
  typedef ElementAssociativityConstructor<TopoD3,Tet> Constructor;

  Constructor assocCtor( BasisFunctionVolumeBase<Tet>::HierarchicalP1 );

  assocCtor.setRank( 2 );
  assocCtor.setNodeGlobalMapping( {3, 4, 5, 6} );

  Int4 faceSign = assocCtor.faceSign();
  BOOST_CHECK_EQUAL( +1, faceSign[0] );
  BOOST_CHECK_EQUAL( +1, faceSign[1] );
  BOOST_CHECK_EQUAL( +1, faceSign[2] );
  BOOST_CHECK_EQUAL( +1, faceSign[3] );

  faceSign[1] = -1;
  assocCtor.faceSign() = faceSign;

  Int4 faceSign2 = assocCtor.faceSign();
  BOOST_CHECK_EQUAL( +1, faceSign2[0] );
  BOOST_CHECK_EQUAL( -1, faceSign2[1] );
  BOOST_CHECK_EQUAL( +1, faceSign2[2] );
  BOOST_CHECK_EQUAL( +1, faceSign2[3] );

  assocCtor.setFaceSign( -1, 2 );
  Int4 faceSign3 = assocCtor.faceSign();
  BOOST_CHECK_EQUAL( +1, faceSign3[0] );
  BOOST_CHECK_EQUAL( -1, faceSign3[1] );
  BOOST_CHECK_EQUAL( -1, faceSign3[2] );
  BOOST_CHECK_EQUAL( +1, faceSign3[3] );

  ElementAssociativity<TopoD3,Tet> assoc( assocCtor );

  faceSign = assoc.faceSign();
  BOOST_CHECK_EQUAL( +1, faceSign[0] );
  BOOST_CHECK_EQUAL( -1, faceSign[1] );
  BOOST_CHECK_EQUAL( -1, faceSign[2] );
  BOOST_CHECK_EQUAL( +1, faceSign[3] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/ElementAssociativityVolume_Tetrahedron_pattern.txt", true );

  //Volume dumps
  typedef ElementAssociativityConstructor<TopoD3,Tet> Constructor;

  Constructor assoc1ctor( BasisFunctionVolumeBase<Tet>::HierarchicalP1 );

  assoc1ctor.setRank( 2 );
  assoc1ctor.setNodeGlobalMapping( {1, 2, 3, 4} );

  ElementAssociativity<TopoD3,Tet> assoc1( assoc1ctor );

  assoc1ctor.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  assoc1.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
#if 0
  ElementAssociativityConstructor<TopoD3, Tet> assoc2ctor( BasisFunctionVolumeBase<Tet>::HierarchicalP3 );

  assoc2ctor.setNodeGlobalMapping( {3, 4, 5} );
  assoc2ctor.setEdgeGlobalMapping( {9, 3, 7, 12, 14, 15} );
  assoc2ctor.setCellGlobalMapping( {1} );

  ElementAssociativity<TopoD3,Tet> assoc2( assoc2ctor );

  assoc2ctor.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  assoc2.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
