// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementXFieldArea_2D_Triangle_btest
// testing of ElementXFieldArea class w/ Triangle

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <memory>

#include "tools/SANSnumerics.h"     // Real
#include "tools/stringify.h"     // Real
#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/TraceToCellRefCoord.h"

using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class ElementXField<PhysD2,TopoD2,Triangle>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementXFieldArea_2D_Triangle_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BoundingBox_Q1_test )
{
  int order = 1;
  ElementXField<PhysD2,TopoD2,Triangle> xElem(order, BasisFunctionCategory_Lagrange);

  xElem.DOF(0) = {0,0};
  xElem.DOF(1) = {1,0};
  xElem.DOF(2) = {0,1};

  BoundingBox<PhysD2> bbox = xElem.boundingBox();

  BOOST_CHECK_EQUAL(0, bbox.low_bounds()[0]);
  BOOST_CHECK_EQUAL(0, bbox.low_bounds()[1]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[0]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[1]);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BoundingBox_Q2_test )
{
  int order = 2;
  ElementXField<PhysD2,TopoD2,Triangle> xElem(order, BasisFunctionCategory_Lagrange);

  // node DOFs
  xElem.DOF(0) = {0,0};
  xElem.DOF(1) = {1,0};
  xElem.DOF(2) = {0,1};

  // edge DOFs
  xElem.DOF(3) = {0.6,0.6};
  xElem.DOF(4) = {-0.1,0.5};
  xElem.DOF(5) = {0.5,-0.2};

  BoundingBox<PhysD2> bbox = xElem.boundingBox();

  BOOST_CHECK_EQUAL(-0.1, bbox.low_bounds()[0]);
  BOOST_CHECK_EQUAL(-0.2, bbox.low_bounds()[1]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[0]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[1]);
}

//----------------------------------------------------------------------------//
// Checking being forwarded correctly and constructed into correct classes,
// the values aren't very informative
//
BOOST_AUTO_TEST_CASE( evalBasisGradient_Hessian_Q1_P1_test )
{
  typedef BasisFunctionArea<Triangle,Hierarchical,1> BasisClass;

  int order = 1;
  ElementXField<PhysD2,TopoD2,Triangle> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 3, xElem.nDOF() );

  const BasisClass* basis = BasisClass::self();

  Element<Real,TopoD2,Triangle> qfldElem( basis );

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( 3, basis->nBasis() );

  const Real tol = 1e-13;
  typename ElementXField<PhysD2,TopoD2,Triangle>::RefCoordType sRef;
  typename ElementXField<PhysD2,TopoD2,Triangle>::VectorX gradphi[3];
  typename ElementXField<PhysD2,TopoD2,Triangle>::TensorSymX hessphi[3];
  Real phixTrue[3], phiyTrue[3], phixxTrue[3], phixyTrue[3], phiyyTrue[3];
  int k;

  xElem.DOF(0) = {0, 0};
  xElem.DOF(1) = {1, 0};
  xElem.DOF(2) = {0, 1};

  // basis gradient independent of location for Q=1, P=1
  phixTrue[0]  = -1; phixTrue[1]  =  1; phixTrue[2]  =  0;
  phiyTrue[0]  = -1; phiyTrue[1]  =  0; phiyTrue[2]  =  1;
  phixxTrue[0] =  0; phixxTrue[1] =  0; phixxTrue[2] =  0;
  phixyTrue[0] =  0; phixyTrue[1] =  0; phixyTrue[2] =  0;
  phiyyTrue[0] =  0; phiyyTrue[1] =  0; phiyyTrue[2] =  0;

  sRef[0] = 0;  sRef[1] = 0;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, 3 );
  xElem.evalBasisHessian( sRef, qfldElem, hessphi, 3 );
  for (k = 0; k < 3; k++)
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phiyTrue[k], gradphi[k][1], tol );
    BOOST_CHECK_CLOSE( phixxTrue[k], hessphi[k](0,0), tol );
    BOOST_CHECK_CLOSE( phixyTrue[k], hessphi[k](1,0), tol );
    BOOST_CHECK_CLOSE( phiyyTrue[k], hessphi[k](1,1), tol );
  }

  sRef[0] = 1;  sRef[1] = 0;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, 3 );
  xElem.evalBasisHessian( sRef, qfldElem, hessphi, 3 );
  for (k = 0; k < 3; k++)
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phiyTrue[k], gradphi[k][1], tol );
    BOOST_CHECK_CLOSE( phixxTrue[k], hessphi[k](0,0), tol );
    BOOST_CHECK_CLOSE( phixyTrue[k], hessphi[k](1,0), tol );
    BOOST_CHECK_CLOSE( phiyyTrue[k], hessphi[k](1,1), tol );
  }

  sRef[0] = 0;  sRef[1] = 1;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, 3 );
  xElem.evalBasisHessian( sRef, qfldElem, hessphi, 3 );
  for (k = 0; k < 3; k++)
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phiyTrue[k], gradphi[k][1], tol );
    BOOST_CHECK_CLOSE( phixxTrue[k], hessphi[k](0,0), tol );
    BOOST_CHECK_CLOSE( phixyTrue[k], hessphi[k](1,0), tol );
    BOOST_CHECK_CLOSE( phiyyTrue[k], hessphi[k](1,1), tol );
  }

  sRef[0] = 1./3.;  sRef[1] = 1./3.;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, 3 );
  xElem.evalBasisHessian( sRef, qfldElem, hessphi, 3 );
  for (k = 0; k < 3; k++)
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phiyTrue[k], gradphi[k][1], tol );
    BOOST_CHECK_CLOSE( phixxTrue[k], hessphi[k](0,0), tol );
    BOOST_CHECK_CLOSE( phixyTrue[k], hessphi[k](1,0), tol );
    BOOST_CHECK_CLOSE( phiyyTrue[k], hessphi[k](1,1), tol );
  }
}

//----------------------------------------------------------------------------//
// Checking that strange coordinates are returning correct values
//
BOOST_AUTO_TEST_CASE( evalBasisGradient_Hessian_Q1_P1_funky_test )
{
  typedef BasisFunctionArea<Triangle,Hierarchical,1> BasisClass;

  int order = 1;
  ElementXField<PhysD2,TopoD2,Triangle> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 3, xElem.nDOF() );

  const BasisClass* basis = BasisClass::self();
  Element<Real,TopoD2,Triangle> qfldElem(basis);

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( 3, basis->nBasis() );

  const Real tol = 1e-13;
  typename ElementXField<PhysD2,TopoD2,Triangle>::RefCoordType sRef;
  typename ElementXField<PhysD2,TopoD2,Triangle>::VectorX gradphi[3];
  typename ElementXField<PhysD2,TopoD2,Triangle>::TensorSymX hessphi[3];
  Real phixTrue[3], phiyTrue[3], phixxTrue[3], phixyTrue[3], phiyyTrue[3];
  int k;

  // Original Master element coords
  xElem.DOF(0) = {0, 0};
  xElem.DOF(1) = {1, 0};
  xElem.DOF(2) = {0, 1};

#if 0
  std::cout<< "pre Transform:" <<std::endl;
  std::cout<< "xDOF(0):"<< xElem.DOF(0) << std::endl;
  std::cout<< "xDOF(1):"<< xElem.DOF(1) << std::endl;
  std::cout<< "xDOF(2):"<< xElem.DOF(2) << std::endl;
#endif

  // Stretch - relative to DOF(0)
  DLA::VectorS<2,Real> tempV1, tempV2;
  DLA::MatrixS<2,2,Real> stretch = {{2.,0},{0,1./2}};
  for (k=1;k<3;k++)
  {
    tempV1 = xElem.DOF(k)-xElem.DOF(0);
    tempV2 = stretch*tempV1;
#if 0
    std::cout<< "xElem.DOF(0):"<<xElem.DOF(0)<<std::endl;
    std::cout<< "xElem.DOF("<<k<<"):"<<xElem.DOF(k)<<std::endl;
    std::cout<< "tempV1:"<< tempV1 << std::endl;
    std::cout<< "tempV2:"<< tempV2 << std::endl;
#endif
    for (int ki=0;ki<2;ki++)
    xElem.DOF(k)(ki) = xElem.DOF(0)(ki) + tempV2[ki];
  }

  // Rotate
  Real rot= PI/6.;
  DLA::MatrixS<2,2,Real> rotMat= {{cos(rot), -sin(rot)}, {sin(rot), cos(rot) }};
  for (k=0;k<3;k++)
  {
    tempV1 = xElem.DOF(k);
    tempV2 = rotMat*tempV1;
    for (int ki=0;ki<2;ki++)
    xElem.DOF(k)(ki)=tempV2[ki];
  }

  // Translate - Shouldn't effect derivative transformations, but making certain
  Real tran[2]={2,1};
  for (k=0;k<3;k++)
  {
    for (int ki=0;ki<2;ki++)
    xElem.DOF(k)(ki)= xElem.DOF(k)(ki) + tran[ki];
  }

#if 0
  std::cout<< "post Transform:" <<std::endl;
  std::cout<< "xDOF(0):"<< xElem.DOF(0) << std::endl;
  std::cout<< "xDOF(1):"<< xElem.DOF(1) << std::endl;
  std::cout<< "xDOF(2):"<< xElem.DOF(2) << std::endl;
#else
  //Checking the coordinate transform
  SANS_CHECK_CLOSE( 2, xElem.DOF(0)(0), tol, tol);
  SANS_CHECK_CLOSE( 1, xElem.DOF(0)(1), tol, tol);
  SANS_CHECK_CLOSE( 2. +2.*cos(PI/6.), xElem.DOF(1)(0), tol, tol);
  SANS_CHECK_CLOSE( 1. +2.*sin(PI/6.), xElem.DOF(1)(1), tol, tol);
  SANS_CHECK_CLOSE( 2.-0.5*sin(PI/6.), xElem.DOF(2)(0), tol, tol);
  SANS_CHECK_CLOSE( 1.+0.5*cos(PI/6.), xElem.DOF(2)(1), tol, tol);
#endif

  // basis gradient independent of location for Q=1, P=1
  phixTrue[0]  = 1 - sqrt(3)/4; phixTrue[1]  =  sqrt(3)/4; phixTrue[2]  =  -1;
  phiyTrue[0]  = -1./4 -sqrt(3); phiyTrue[1]  =  1./4; phiyTrue[2]  = sqrt(3);
  phixxTrue[0] =  0; phixxTrue[1] =  0; phixxTrue[2] =  0;
  phixyTrue[0] =  0; phixyTrue[1] =  0; phixyTrue[2] =  0;
  phiyyTrue[0] =  0; phiyyTrue[1] =  0; phiyyTrue[2] =  0;

  sRef[0] = 0;  sRef[1] = 0;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, 3 );
  xElem.evalBasisHessian( sRef, qfldElem, hessphi, 3 );
  for (k = 0; k < 3; k++)
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phiyTrue[k], gradphi[k][1], tol );
    BOOST_CHECK_CLOSE( phixxTrue[k], hessphi[k](0,0), tol );
    BOOST_CHECK_CLOSE( phixyTrue[k], hessphi[k](1,0), tol );
    BOOST_CHECK_CLOSE( phiyyTrue[k], hessphi[k](1,1), tol );

  }

  sRef[0] = 1;  sRef[1] = 0;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, 3 );
  xElem.evalBasisHessian( sRef, qfldElem, hessphi, 3 );
  for (k = 0; k < 3; k++)
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phiyTrue[k], gradphi[k][1], tol );
    BOOST_CHECK_CLOSE( phixxTrue[k], hessphi[k](0,0), tol );
    BOOST_CHECK_CLOSE( phixyTrue[k], hessphi[k](1,0), tol );
    BOOST_CHECK_CLOSE( phiyyTrue[k], hessphi[k](1,1), tol );
  }

  sRef[0] = 0;  sRef[1] = 1;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, 3 );
  xElem.evalBasisHessian( sRef, qfldElem, hessphi, 3 );
  for (k = 0; k < 3; k++)
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phiyTrue[k], gradphi[k][1], tol );
    BOOST_CHECK_CLOSE( phixxTrue[k], hessphi[k](0,0), tol );
    BOOST_CHECK_CLOSE( phixyTrue[k], hessphi[k](1,0), tol );
    BOOST_CHECK_CLOSE( phiyyTrue[k], hessphi[k](1,1), tol );
  }

  sRef[0] = 1./3.;  sRef[1] = 1./3.;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, 3 );
  xElem.evalBasisHessian( sRef, qfldElem, hessphi, 3 );
  for (k = 0; k < 3; k++)
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phiyTrue[k], gradphi[k][1], tol );
    BOOST_CHECK_CLOSE( phixxTrue[k], hessphi[k](0,0), tol );
    BOOST_CHECK_CLOSE( phixyTrue[k], hessphi[k](1,0), tol );
    BOOST_CHECK_CLOSE( phiyyTrue[k], hessphi[k](1,1), tol );
  }
}

//----------------------------------------------------------------------------//
// Checking being forwarded correctly and constructed into correct classes,
// the values aren't very informative
//
BOOST_AUTO_TEST_CASE( evalBasisGradient_Hessian_Q2_P2_test )
{
  typedef BasisFunctionArea<Triangle,Hierarchical,2> BasisClass;

  int order = 2;
  ElementXField<PhysD2,TopoD2,Triangle> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, xElem.order() );
  BOOST_CHECK_EQUAL( 6, xElem.nDOF() );

  const BasisClass* basis = BasisClass::self();
  Element<Real,TopoD2,Triangle> qfldElem( basis );

  BOOST_CHECK_EQUAL( 2, basis->order() );
  BOOST_CHECK_EQUAL( 6, basis->nBasis() );

  const Real tol = 2e-13;
  typename ElementXField<PhysD2,TopoD2,Triangle>::RefCoordType sRef;
  typename ElementXField<PhysD2,TopoD2,Triangle>::VectorX gradphi[6];
  typename ElementXField<PhysD2,TopoD2,Triangle>::TensorSymX hessphi[6];
  Real phixTrue[6], phiyTrue[6], phixxTrue[6], phixyTrue[6], phiyyTrue[6];
  int k;

  xElem.DOF(0) = {0,    0};
  xElem.DOF(1) = {0.7,  0.1};
  xElem.DOF(2) = {0.05, 1.2};
  xElem.DOF(3) = {0.5,  0.8};
  xElem.DOF(4) = {0.1,  0.3};
  xElem.DOF(5) = {0.1, -0.1};

  // basis gradient independent of location for Q=1, P=1
  phixTrue[0]  = -0.9729729729729728;
  phixTrue[1]  =  0.8648648648648647;
  phixTrue[2]  =  0.1081081081081081;
  phixTrue[3]  =  0.;
  phixTrue[4]  =  0.4324324324324324;
  phixTrue[5]  =  3.459459459459459;

  phiyTrue[0]  = -0.2342342342342342;
  phiyTrue[1]  = -0.1621621621621621;
  phiyTrue[2]  =  0.3963963963963963;
  phiyTrue[3]  =  0.;
  phiyTrue[4]  =  1.585585585585585;
  phiyTrue[5]  = -0.6486486486486486;

  phixxTrue[0] = -0.1342677959186357;
  phixxTrue[1] =  0.4968076915483777;
  phixxTrue[2] = -0.3625398956297420;
  phixxTrue[3] =  0.7479912344777208;
  phixxTrue[4] = -2.2916497213064040;
  phixxTrue[5] = -4.7446903441059760;

  phixyTrue[0] =  0.5881165314328204;
  phixyTrue[1] = -0.3132634460611085;
  phixyTrue[2] = -0.2748530853717119;
  phixyTrue[3] =  1.3011930849768680;
  phixyTrue[4] = -2.7434347422660050;
  phixyTrue[5] = -1.4322600175047210;

  phiyyTrue[0] = -0.4485560798197757;
  phiyyTrue[1] =  0.1525604494194705;
  phiyyTrue[2] =  0.2959956304003052;
  phiyyTrue[3] = -0.5142439737034330;
  phiyyTrue[4] =  0.4411856706962619;
  phiyyTrue[5] =  0.9141132366844562;

  sRef[0] = 0;  sRef[1] = 0;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, 6 );
  xElem.evalBasisHessian( sRef, qfldElem, hessphi, 6 );
  // TODO: to complete
  for (k = 0; k < 6; k++)
  {
    SANS_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol, tol );
    SANS_CHECK_CLOSE( phiyTrue[k], gradphi[k][1], tol, tol );
    SANS_CHECK_CLOSE( phixxTrue[k], hessphi[k](0,0), tol, tol );
    SANS_CHECK_CLOSE( phixyTrue[k], hessphi[k](1,0), tol, tol );
    SANS_CHECK_CLOSE( phiyyTrue[k], hessphi[k](1,1), tol, tol );
  }
}

//----------------------------------------------------------------------------//
// check that the trace element coordinates evaluate to the cell element coordinates on the trace
BOOST_AUTO_TEST_CASE( Trace_RefCoord_P1_test )
{
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldAreaClass;
  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldLineClass;
  typedef ElementXFieldAreaClass::VectorX VectorX;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  // element field variables
  ElementXFieldLineClass xfldElemEdgeP( BasisFunctionLineBase::HierarchicalP1 );
  ElementXFieldLineClass xfldElemEdgeN( BasisFunctionLineBase::HierarchicalP1 );
  ElementXFieldAreaClass xfldElemCell( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );

  xfldElemCell.DOF(0) = {0, 0};
  xfldElemCell.DOF(1) = {1, 0};
  xfldElemCell.DOF(2) = {0, 1};

  const int tri[3] = {0, 1, 2};

  const int (*TraceNodes)[ Line::NTrace ] = TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes;

  const int *OrientPos = TraceToCellRefCoord<Line, TopoD2, Triangle>::OrientPos;
  const int *OrientNeg = TraceToCellRefCoord<Line, TopoD2, Triangle>::OrientNeg;


  for (int edge = 0; edge < Triangle::NTrace; edge++)
  {
    // Get the re-oriented lines
    const int lineP[2] = {tri[TraceNodes[edge][OrientPos[0]]],
                          tri[TraceNodes[edge][OrientPos[1]]]};

    const int lineN[2] = {tri[TraceNodes[edge][OrientNeg[0]]],
                          tri[TraceNodes[edge][OrientNeg[1]]]};

    const CanonicalTraceToCell canonicalEdgeP = TraceToCellRefCoord<Line, TopoD2, Triangle>::getCanonicalTrace(lineP, 2, tri, 3);
    const CanonicalTraceToCell canonicalEdgeN = TraceToCellRefCoord<Line, TopoD2, Triangle>::getCanonicalTrace(lineN, 2, tri, 3);

    BOOST_REQUIRE_EQUAL(canonicalEdgeP.trace, edge);
    BOOST_REQUIRE_EQUAL(canonicalEdgeP.orientation, +1);

    BOOST_REQUIRE_EQUAL(canonicalEdgeN.trace, edge);
    BOOST_REQUIRE_EQUAL(canonicalEdgeN.orientation, -1);

    // Set the DOF's on the trace
    for (int n = 0; n < Line::NNode; n++)
    {
      xfldElemEdgeP.DOF(n) = xfldElemCell.DOF(lineP[n]);
      xfldElemEdgeN.DOF(n) = xfldElemCell.DOF(lineN[n]);
    }

    int kmax = 5;
    for (int k = 0; k < kmax; k++)
    {
      DLA::VectorS<1,Real> sRefTrace;
      DLA::VectorS<2,Real> sRefCell;
      VectorX xTrace, xCell;
      Real s;

      s = k/static_cast<Real>(kmax-1);

      sRefTrace = {s};

      // cell reference-element coords
      TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalEdgeP, sRefTrace, sRefCell );

      // element coordinates from trace and cell
      xfldElemEdgeP.eval( sRefTrace, xTrace );
      xfldElemCell.eval( sRefCell, xCell );

      SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
      SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );

      // cell reference-element coords
      TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalEdgeN, sRefTrace, sRefCell );

      // element coordinates from trace and cell
      xfldElemEdgeN.eval( sRefTrace, xTrace );
      xfldElemCell.eval( sRefCell, xCell );

      SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
      SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( impliedMetric_test )
{
  const Real small_tol = 1e-13;
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysD2, TopoD2, Triangle> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 3, xElem.nDOF() );

  ElementXField<PhysD2, TopoD2, Triangle>::RefCoordType sRef = {0.0, 0.0};

  DLA::MatrixSymS<PhysD2::D, Real> M;

  //Unit equilateral triangle
  xElem.DOF(0) = {0.0, 0.0};
  xElem.DOF(1) = {1.0, 0.0};
  xElem.DOF(2) = {0.5, 0.5*sqrt(3.0)};

  xElem.impliedMetric(sRef, M);

  SANS_CHECK_CLOSE( M(0,0), 1.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,1), 1.0, small_tol, tol );

  M = 0;
  xElem.impliedMetric(M); //evaluate at centerRef

  SANS_CHECK_CLOSE( M(0,0), 1.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,1), 1.0, small_tol, tol );

  //Unit equilateral triangle - scaled by 2.5, translated by (0.6,1.3)
  Real scale = 2.5;
  Real xshift[2] = {0.6, 1.3};
  for (int i=0; i<3; i++)
    for (int d=0; d<2; d++)
      xElem.DOF(i)[d] = xElem.DOF(i)[d]*scale + xshift[d];

  xElem.impliedMetric(sRef, M);

  SANS_CHECK_CLOSE( M(0,0), 1.0/pow(scale,2.0), small_tol, tol );
  SANS_CHECK_CLOSE( M(0,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,1), 1.0/pow(scale,2.0), small_tol, tol );


  // TestPXGetImpliedMetricFromCoordinates_1
  xElem.DOF(0) = {-1. , -.5 };
  xElem.DOF(1) = { 1. , 0.  };
  xElem.DOF(2) = { 0. , 1.  };
  xElem.impliedMetric(sRef, M);

  SANS_CHECK_CLOSE( M(0,0) , .28 , small_tol , tol );
  SANS_CHECK_CLOSE( M(0,1) ,-.12 , small_tol , tol );
  SANS_CHECK_CLOSE( M(1,0) ,-.12 , small_tol , tol );
  SANS_CHECK_CLOSE( M(1,1) , .48 , small_tol , tol );
}


//----------------------------------------------------------------------------//
// Checking being forwarded correctly and constructed into correct classes,
// the values aren't very informative
//
BOOST_AUTO_TEST_CASE( evalGradient_Q1_P1_test )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  typedef BasisFunctionArea<Triangle,Hierarchical,1> BasisClass;

  int order = 1;
  ElementXField<PhysD2,TopoD2,Triangle> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 3, xElem.nDOF() );

  xElem.DOF(0) = {0, 0};
  xElem.DOF(1) = {1, 0};
  xElem.DOF(2) = {0, 1};

  const BasisClass* basis = BasisClass::self();

  Element<Real,TopoD2,Triangle> q1fldElem( basis );

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( 3, basis->nBasis() );

  q1fldElem.DOF(0) =  3;
  q1fldElem.DOF(1) =  7;
  q1fldElem.DOF(2) = -2;

  Real q1xTrue =  4;
  Real q1yTrue = -5;

  typename ElementXField<PhysD2,TopoD2,Triangle>::RefCoordType sRef = {0.5, 0.5};

  DLA::VectorS<PhysD2::D, Real> gradq1;
  xElem.evalGradient( sRef, q1fldElem, gradq1 );

  SANS_CHECK_CLOSE( q1xTrue, gradq1[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( q1yTrue, gradq1[1], small_tol, close_tol );

  typedef DLA::VectorS<2, Real> ArrayQ;
  Element<ArrayQ,TopoD2,Triangle> q2fldElem( basis );

  q2fldElem.DOF(0) = { 3, -4};
  q2fldElem.DOF(1) = { 7, -1};
  q2fldElem.DOF(2) = {-2,  9};

  ArrayQ q2xTrue = { 4,  3};
  ArrayQ q2yTrue = {-5, 13};

  DLA::VectorS<PhysD2::D, ArrayQ> gradq2;
  xElem.evalGradient( sRef, q2fldElem, gradq2 );

  SANS_CHECK_CLOSE( q2xTrue(0), gradq2[0](0), small_tol, close_tol );
  SANS_CHECK_CLOSE( q2xTrue(1), gradq2[0](1), small_tol, close_tol );
  SANS_CHECK_CLOSE( q2yTrue(0), gradq2[1](0), small_tol, close_tol );
  SANS_CHECK_CLOSE( q2yTrue(1), gradq2[1](1), small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
