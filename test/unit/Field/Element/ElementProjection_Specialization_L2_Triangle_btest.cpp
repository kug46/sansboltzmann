// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementProjection2D_L2_Triangle_AD_btest
// testing of 2-D element L2 projection operator with Advection-Diffusion

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/ForcingFunction2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "Field/Element/ElementProjection_Specialization_L2.h"
#include "Field/Element/ElementArea.h"
#include "Field/Element/ElementXFieldArea.h"


using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementProjectionDiffusiveFlux_L2_Triangle_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementProjection_L2_LegendreP0 )
{
  typedef ScalarFunction2D_Linear   ScalarFunctionLinear;
  typedef ScalarFunction2D_Const    ScalarFunctionConst;
  typedef SolnNDConvertSpace<PhysD2, ScalarFunctionLinear> SolutionLinearND;
  typedef SolnNDConvertSpace<PhysD2, ScalarFunctionConst>  SolutionConstND;

  typedef PDEAdvectionDiffusion<PhysD2,
      AdvectiveFlux2D_Uniform,
      ViscousFlux2D_Uniform,
      Source2D_None > PDEAdvectionDiffusion2DLinear;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2DLinear> NDPDEClassLinear;

  typedef PDEAdvectionDiffusion<PhysD2,
      AdvectiveFlux2D_Uniform,
      ViscousFlux2D_Uniform,
      Source2D_None > PDEAdvectionDiffusion2DConst;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2DConst> NDPDEClassConst;

  typedef NDPDEClassConst::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD2, TopoD2, Triangle> ElementXFieldClass;
  typedef Element<VectorArrayQ, TopoD2, Triangle> ElementQFieldClass;

  Real u = 2;
  Real v = 3;
  AdvectiveFlux2D_Uniform adv( u, v );

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc( kxx, kxy, kxy, kyy );

  Source2D_None source;

  // grid

  int order = 1;
  ElementXFieldClass xfld(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfld.order() );
  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfld.DOF(0) = {x1, y1};
  xfld.DOF(1) = {x2, y2};
  xfld.DOF(2) = {x3, y3};

  // solution

  order = 0;
  ElementQFieldClass qfld(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, qfld.order() );
  BOOST_CHECK_EQUAL( 1, qfld.nDOF() );

  Real tol = 1e-13;

  // exact solution: constant = aconst
  Real aconst = 3.2;
  SolutionConstND solnExact0(aconst);

  typedef ForcingFunction2D_MMS<PDEAdvectionDiffusion2DConst> ForcingType0;
  std::shared_ptr<ForcingType0> forcingptr0( new ForcingType0(solnExact0));

  NDPDEClassConst pde0( adv, visc, source, forcingptr0 );

  ElementProjectionDiffusiveFlux_L2( xfld, qfld, pde0, solnExact0 );

  Real diffx = 0;
  Real diffy = 0;
  BOOST_CHECK_CLOSE( diffx, qfld.DOF(0)[0], tol );
  BOOST_CHECK_CLOSE( diffy, qfld.DOF(0)[1], tol );


  // exact solution: linear = a0 + ax*x + ay*y
  Real a0 =  1;
  Real ax = -3;
  Real ay =  4;
  SolutionLinearND solnExact1(a0, ax, ay);

  typedef ForcingFunction2D_MMS<PDEAdvectionDiffusion2DLinear> ForcingType1;

  std::shared_ptr<ForcingType1> forcingptr1( new ForcingType1(solnExact1));


  NDPDEClassLinear pde1( adv, visc, source, forcingptr1 );

  ElementProjectionDiffusiveFlux_L2( xfld, qfld, pde1, solnExact1 );

  tol = 1e-12;
  diffx = -ax*kxx - ay*kxy;
  diffy = -ax*kxy - ay*kyy;
  BOOST_CHECK_CLOSE( diffx, qfld.DOF(0)[0], tol );
  BOOST_CHECK_CLOSE( diffy, qfld.DOF(0)[1], tol );
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementProjectionLinearScalarLG_L2_LegendreP0 )
{
  typedef ScalarFunction2D_Linear   ScalarFunctionLinear;
  typedef ScalarFunction2D_Const    ScalarFunctionConst;
  typedef SolnNDConvertSpace<PhysD2, ScalarFunctionLinear> SolutionLinearND;
  typedef SolnNDConvertSpace<PhysD2, ScalarFunctionConst>  SolutionConstND;

  typedef PDEAdvectionDiffusion<PhysD2,
      AdvectiveFlux2D_Uniform,
      ViscousFlux2D_Uniform,
      Source2D_None,
      ForcingFunction2D_MMS<SolnNDConvertSpace<PhysD2, ScalarFunctionLinear> > > PDEAdvectionDiffusion2DLinear;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2DLinear> NDPDEClassLinear;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;

  typedef PDEAdvectionDiffusion<PhysD2,
      AdvectiveFlux2D_Uniform,
      ViscousFlux2D_Uniform,
      Source2D_None,
      ForcingFunction2D_MMS<SolnNDConvertSpace<PhysD2, ScalarFunctionConst> > > PDEAdvectionDiffusion2DConst;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2DConst> NDPDEClassConst;

  typedef BCTypeFunction<SolutionConstND> BCTypeConst;
  typedef BCAdvectionDiffusion<PhysD2,BCTypeConst> BCClassRawConst;
  typedef BCNDConvertSpace<PhysD2, BCClassRawConst> BCClassConst;

  typedef NDPDEClassConst::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClassConst::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename Field<PhysD2, TopoD2, ArrayQ>::FieldTraceGroupType<Line> QFieldTraceGroupType;

  typedef ElementXField<PhysD2, TopoD2, Triangle> ElementXFieldClass;
  typedef typename QFieldTraceGroupType::ElementType<> ElementQFieldTraceClass;

  Real A = 1.0,B = 2.0,bcdata = 3.0; // Robin
  BCClass bc(A, B, bcdata);

  Real u = 2;
  Real v = 2;
  AdvectiveFlux2D_Uniform adv( u, v );

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc( kxx, kxy, kxy, kyy );

  Source2D_None source;

  // grid

  int order = 1;
  ElementXFieldClass xfld(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfld.order() );
  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfld.DOF(0) = {x1, y1};
  xfld.DOF(1) = {x2, y2};
  xfld.DOF(2) = {x3, y3};

  // solution

  order = 0;
  ElementQFieldTraceClass qfld(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, qfld.order() );
  BOOST_CHECK_EQUAL( 1, qfld.nDOF() );

  Real tol = 1e-13;

  // exact solution: constant = aconst
  Real aconst = 3.2;
  SolutionConstND solnExact0(aconst);
  ForcingFunction2D_MMS<SolutionConstND> forcing0(solnExact0);
  NDPDEClassConst pde0( adv, visc, source, forcing0 );


  ElementProjectionLinearScalarLG_L2( xfld, qfld, pde0, bc, solnExact0 );

// mu = ((B - A*an)*qExact - A*dot(Kn,gradQExact))/(A*A + B*B);


  Real mu;
  BOOST_CHECK_CLOSE( diffx, qfld.DOF(0), tol );

  // exact solution: linear = a0 + ax*x + ay*y
  Real a0 =  1;
  Real ax = -3;
  Real ay =  4;
  SolutionLinearND solnExact1(a0, ax, ay);
  ForcingFunction2D_MMS<SolutionLinearND> forcing1(solnExact1);
  NDPDEClassLinear pde1( adv, visc, source, forcing1 );

  ElementProjectionDiffusiveFlux_L2( xfld, qfld, pde1, solnExact1 );

  tol = 1e-12;
  diffx = -ax*kxx - ay*kxy;
  diffy = -ax*kxy - ay*kyy;
  BOOST_CHECK_CLOSE( diffx, qfld.DOF(0)[0], tol );
  BOOST_CHECK_CLOSE( diffy, qfld.DOF(0)[1], tol );
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
