// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// RefCoordSolver_btest
// testing of reference coordinate solver

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Field/Element/RefCoordSolver.h"

#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class RefCoordSolver<PhysD1, TopoD0, Node>;
template class RefCoordSolver<PhysD1, TopoD1, Line>;
template class RefCoordSolver<PhysD2, TopoD1, Line>;
template class RefCoordSolver<PhysD2, TopoD2, Triangle>;
template class RefCoordSolver<PhysD3, TopoD2, Triangle>;
template class RefCoordSolver<PhysD3, TopoD3, Tet>;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( RefCoordSolver_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( RefCoordSolver_1D_Node_X1 )
{
  typedef PhysD1 PhysDim;
  typedef TopoD0 TopoDim;
  typedef Node Topology;
  typedef ElementXField<PhysDim, TopoDim, Topology>::VectorX VectorX;
  typedef ElementXField<PhysDim, TopoDim, Topology>::RefCoordType RefCoordType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  int order = 0;
  ElementXField<PhysDim, TopoDim, Topology> xElem(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xElem.order() );
  BOOST_CHECK_EQUAL( 1, xElem.nDOF() );

  xElem.DOF(0) = {0.75};

  VectorX X0 = {0.75};
  VectorX X1 = {1.40};

  RefCoordSolver<PhysDim, TopoDim, Topology> solver(xElem);
  RefCoordType sRef = Node::centerRef;
  bool converged = solver.solve(X0, sRef);

  BOOST_REQUIRE(converged);
  SANS_CHECK_CLOSE( 0.0, sRef[0], small_tol, close_tol );

  converged = solver.solve(X1, sRef);
  BOOST_REQUIRE(!converged); //Check that it did not converge - because target coordinate is outside element
  SANS_CHECK_CLOSE( 0.0, sRef[0], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( RefCoordSolver_1D_Line_X1 )
{
  typedef PhysD1 PhysDim;
  typedef TopoD1 TopoDim;
  typedef Line Topology;
  typedef ElementXField<PhysDim, TopoDim, Topology>::VectorX VectorX;
  typedef ElementXField<PhysDim, TopoDim, Topology>::RefCoordType RefCoordType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  int order = 1;
  ElementXField<PhysDim, TopoDim, Topology> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 2, xElem.nDOF() );

  xElem.DOF(0) = {0.0};
  xElem.DOF(1) = {1.5};

  VectorX X0 = {0.75};
  VectorX X1 = {1.40};
  VectorX X2 = {-0.23};

  RefCoordSolver<PhysDim, TopoDim, Topology> solver(xElem);
  RefCoordType sRef = Line::centerRef;
  bool converged = solver.solve(X0, sRef);

  BOOST_REQUIRE(converged);
  SANS_CHECK_CLOSE( 0.5, sRef[0], small_tol, close_tol );

  converged = solver.solve(X1, sRef);
  BOOST_REQUIRE(converged);
  SANS_CHECK_CLOSE( 1.4/1.5, sRef[0], small_tol, close_tol );

  converged = solver.solve(X2, sRef);
  BOOST_REQUIRE(converged);
  SANS_CHECK_CLOSE( 0.0, sRef[0], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( RefCoordSolver_2D_Line_X1 )
{
  typedef PhysD2 PhysDim;
  typedef TopoD1 TopoDim;
  typedef Line Topology;
  typedef ElementXField<PhysDim, TopoDim, Topology>::VectorX VectorX;
  typedef ElementXField<PhysDim, TopoDim, Topology>::RefCoordType RefCoordType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  int order = 1;
  ElementXField<PhysDim, TopoDim, Topology> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 2, xElem.nDOF() );

  xElem.DOF(0) = {0.6, 0.1};
  xElem.DOF(1) = {0.6, 0.9};

  VectorX X0 = {0.6, 0.66};
  VectorX X1 = {0.6, 1.0};  //outside element
  VectorX X2 = {0.8, 0.34}; //outside element

  RefCoordSolver<PhysDim, TopoDim, Topology> solver(xElem);
  RefCoordType sRef = Line::centerRef;
  bool converged = solver.solve(X0, sRef);

  BOOST_REQUIRE(converged);
  SANS_CHECK_CLOSE( 0.7, sRef[0], small_tol, close_tol );

  converged = solver.solve(X1, sRef);
  BOOST_REQUIRE(converged);
  SANS_CHECK_CLOSE( 1.0, sRef[0], small_tol, close_tol );

  converged = solver.solve(X2, sRef);
  BOOST_REQUIRE(converged);
  SANS_CHECK_CLOSE( 0.3, sRef[0], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( RefCoordSolver_2D_Triangle_X1 )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef Triangle Topology;
  typedef ElementXField<PhysDim, TopoDim, Topology>::VectorX VectorX;
  typedef ElementXField<PhysDim, TopoDim, Topology>::RefCoordType RefCoordType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  int order = 1;
  ElementXField<PhysDim, TopoDim, Topology> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 3, xElem.nDOF() );

  xElem.DOF(0) = {0.0, 0.0};
  xElem.DOF(1) = {2.0, 0.0};
  xElem.DOF(2) = {1.0, 0.5};

  VectorX X0 = {2.0/3.0, 1.0/3.0};
  VectorX X1 = {1.5, 0.25};
  VectorX X2 = {1.0, -0.1}; // outside the element
  VectorX X;

  RefCoordSolver<PhysDim, TopoDim, Topology> solver(xElem);
  RefCoordType sRef = Triangle::centerRef;
  bool converged = solver.solve(X0, sRef);

  BOOST_CHECK(converged);
  xElem.coordinates(sRef, X);
  SANS_CHECK_CLOSE( X0[0], X[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( X0[1], X[1], small_tol, close_tol );

  converged = solver.solve(X1, sRef);
  BOOST_CHECK(converged);
  xElem.coordinates(sRef, X);
  SANS_CHECK_CLOSE( X1[0], X[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( X1[1], X[1], small_tol, close_tol );

  converged = solver.solve(X2, sRef);
  BOOST_CHECK(converged);
  xElem.coordinates(sRef, X);

  SANS_CHECK_CLOSE( X2[0], X[0], small_tol, close_tol );
  SANS_CHECK_CLOSE(   0.0, X[1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( RefCoordSolver_3D_Triangle_X1 )
{
  typedef PhysD3 PhysDim;
  typedef TopoD2 TopoDim;
  typedef Triangle Topology;
  typedef ElementXField<PhysDim, TopoDim, Topology>::VectorX VectorX;
  typedef ElementXField<PhysDim, TopoDim, Topology>::RefCoordType RefCoordType;

  const Real small_tol = 1e-12;
  const Real close_tol = 5e-4;

  int order = 1;
  ElementXField<PhysDim, TopoDim, Topology> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 3, xElem.nDOF() );

  xElem.DOF(0) = {0.0, 0.0, 0.0};
  xElem.DOF(1) = {2.0, 0.0, 0.0};
  xElem.DOF(2) = {1.0, 0.5, 0.0};

  VectorX X0 = {2.0/3.0, 1.0/3.0, 0.0};
  VectorX X1 = {1.5, 0.25, 0.2}; //outside element
  VectorX X2 = {1.0, -0.1, 0.0}; //outside the element
  VectorX X;

  RefCoordSolver<PhysDim, TopoDim, Topology> solver(xElem);
  RefCoordType sRef = Triangle::centerRef;
  bool converged = solver.solve(X0, sRef);

  BOOST_CHECK(converged);
  xElem.coordinates(sRef, X);
  SANS_CHECK_CLOSE( X0[0], X[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( X0[1], X[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( X0[2], X[2], small_tol, close_tol );

  converged = solver.solve(X1, sRef);
  BOOST_CHECK(converged);
  xElem.coordinates(sRef, X);
  SANS_CHECK_CLOSE( X1[0], X[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( X1[1], X[1], small_tol, close_tol );
  SANS_CHECK_CLOSE(   0.0, X[2], small_tol, close_tol );

  sRef = {0.1, 0.1};

  converged = solver.solve(X2, sRef);
  BOOST_CHECK(converged);
  xElem.coordinates(sRef, X);
  SANS_CHECK_CLOSE( X2[0], X[0], small_tol, close_tol );
  SANS_CHECK_CLOSE(   0.0, X[1], small_tol, close_tol );
  SANS_CHECK_CLOSE(   0.0, X[2], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( RefCoordSolver_3D_Tet_X1 )
{
  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;
  typedef Tet Topology;
  typedef ElementXField<PhysDim, TopoDim, Topology>::VectorX VectorX;
  typedef ElementXField<PhysDim, TopoDim, Topology>::RefCoordType RefCoordType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  int order = 1;
  ElementXField<PhysDim, TopoDim, Topology> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 4, xElem.nDOF() );

  xElem.DOF(0) = {0.0, 0.0, 0.0};
  xElem.DOF(1) = {2.0, 0.0, 0.0};
  xElem.DOF(2) = {0.0, 0.5, 0.0};
  xElem.DOF(3) = {0.0, 0.0, 1.0};

  VectorX X0 = {0.4, 0.1, 0.2};
  VectorX X1 = {0.0, 0.25, 0.5};
  VectorX X2 = {0.25, 0.1, -0.1};
  VectorX X;

  RefCoordSolver<PhysDim, TopoDim, Topology> solver(xElem);
  RefCoordType sRef = Tet::centerRef;
  bool converged = solver.solve(X0, sRef);

  BOOST_CHECK(converged);
  xElem.coordinates(sRef, X);
  SANS_CHECK_CLOSE( X0[0], X[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( X0[1], X[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( X0[2], X[2], small_tol, close_tol );

  converged = solver.solve(X1, sRef);
  BOOST_CHECK(converged);
  xElem.coordinates(sRef, X);
  SANS_CHECK_CLOSE( X1[0], X[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( X1[1], X[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( X1[2], X[2], small_tol, close_tol );

  converged = solver.solve(X2, sRef);
  BOOST_CHECK(converged);
  xElem.coordinates(sRef, X);
  SANS_CHECK_CLOSE( X2[0], X[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( X2[1], X[1], small_tol, close_tol );
  SANS_CHECK_CLOSE(     0, X[2], small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
