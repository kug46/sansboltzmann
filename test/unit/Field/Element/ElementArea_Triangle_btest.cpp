// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementArea_Triangle_btest
// testing of Element< T, TopoD2, Triangle >

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <ostream>
#include <memory>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"
#include "Field/Element/ElementArea.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "Quadrature/QuadratureArea.h"
#include "Quadrature/QuadratureLine.h"
#include "BasisFunction/TraceToCellRefCoord.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class Element<Real, TopoD2, Triangle>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementArea_Triangle_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  typedef Element< Real, TopoD2, Triangle > ElementClass;

  static_assert( std::is_same<typename ElementClass::BasisType, BasisFunctionAreaBase<Triangle> >::value, "Incorrect Basis type" );
  static_assert( std::is_same<typename ElementClass::TopologyType, Triangle>::value, "Incorrect topology type" );
  static_assert( std::is_same<typename ElementClass::RefCoordType, DLA::VectorS<2,Real> >::value, "Incorrect reference coordinate type" );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef Element< Real, TopoD2, Triangle > ElementClass;

  int order;

  order = 1;
  ElementClass qElem_p1_1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem_p1_1.order() );
  BOOST_CHECK_EQUAL( 3, qElem_p1_1.nDOF() );

  ElementClass qElem_p1_2( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );

  BOOST_CHECK_EQUAL( 1, qElem_p1_2.order() );
  BOOST_CHECK_EQUAL( 3, qElem_p1_2.nDOF() );

  ElementClass qElem_p1_3(qElem_p1_1);

  BOOST_CHECK_EQUAL( 1, qElem_p1_3.order() );
  BOOST_CHECK_EQUAL( 3, qElem_p1_3.nDOF() );


  order = 2;
  ElementClass qElem_p2_1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qElem_p2_1.order() );
  BOOST_CHECK_EQUAL( 6, qElem_p2_1.nDOF() );

  ElementClass qElem_p2_2( BasisFunctionAreaBase<Triangle>::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, qElem_p2_2.order() );
  BOOST_CHECK_EQUAL( 6, qElem_p2_2.nDOF() );

  ElementClass qElem_p2_3(qElem_p1_1);

  BOOST_CHECK_EQUAL( 1, qElem_p2_3.order() );
  BOOST_CHECK_EQUAL( 3, qElem_p2_3.nDOF() );

  qElem_p2_3 = qElem_p2_2;

  BOOST_CHECK_EQUAL( 2, qElem_p2_3.order() );
  BOOST_CHECK_EQUAL( 6, qElem_p2_3.nDOF() );

  ElementClass qElem_p2_4;

  qElem_p2_4.setBasis( BasisFunctionAreaBase<Triangle>::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, qElem_p2_4.order() );
  BOOST_CHECK_EQUAL( 6, qElem_p2_4.nDOF() );


  //Exceptions
  BOOST_CHECK_THROW( ElementClass qElem_p0(0, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( ElementClass qElem_pmax(BasisFunctionArea_Triangle_HierarchicalPMax+1, BasisFunctionCategory_Hierarchical), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( accessors )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD2, Triangle > ElementClass;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( 3, qElem.nDOF() );

  const Real tol = 1e-13;

  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};
  ArrayQ q3 = {5, 6};

  qElem.DOF(0) = q1;
  qElem.DOF(1) = q2;
  qElem.DOF(2) = q3;

  ArrayQ q;
  for (int k = 0; k < 2; k++)
  {
    q = qElem.DOF(0);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );
    q = qElem.DOF(1);
    BOOST_CHECK_CLOSE( q2[k], q[k], tol );
    q = qElem.DOF(2);
    BOOST_CHECK_CLOSE( q3[k], q[k], tol );
  }


  // Check the vector view version of the DOF
  DLA::VectorDView<ArrayQ> vectorDOF = qElem.vectorViewDOF();

  BOOST_CHECK_EQUAL( qElem.nDOF(), vectorDOF.m() );

  vectorDOF[0] = q3;
  vectorDOF[1] = q2;
  vectorDOF[2] = q1;

  for (int k = 0; k < 2; k++)
  {
    q = qElem.DOF(0);
    BOOST_CHECK_CLOSE( q3[k], q[k], tol );
    q = qElem.DOF(1);
    BOOST_CHECK_CLOSE( q2[k], q[k], tol );
    q = qElem.DOF(2);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( eval_basis_P1 )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD2, Triangle > ElementClass;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( 3, qElem.nDOF() );

  const Real tol = 1e-13;
  Real sRef, tRef;
  Real phi[3], phis[3], phit[3];
  Real phiss[3], phist[3], phitt[3];
  const Real *phi3, *phis3, *phit3;
  const Real *phiss3, *phist3, *phitt3;

  Real phiTrue[3], phisTrue[3], phitTrue[3];
  Real phissTrue[3],phistTrue[3],phittTrue[3];
  int k;

  sRef = 0;  tRef = 0;
  phiTrue[0] = 1;  phiTrue[1] = 0;  phiTrue[2] = 0;
  phisTrue[0] = -1;  phisTrue[1] = 1;  phisTrue[2] = 0;
  phitTrue[0] = -1;  phitTrue[1] = 0;  phitTrue[2] = 1;
  phissTrue[0]=  0;  phissTrue[1]= 0;  phissTrue[2]= 0;
  phistTrue[0]=  0;  phistTrue[1]= 0;  phistTrue[2]= 0;
  phittTrue[0]=  0;  phittTrue[1]= 0;  phittTrue[2]= 0;

  qElem.evalBasis( sRef, tRef, phi, 3 );
  qElem.evalBasisDerivative( sRef, tRef, phis, phit, 3 );
  qElem.evalBasisHessianDerivative( sRef, tRef, phiss, phist, phitt, 3 );
  qElem.evalBasis( sRef, tRef, &phi3 );
  qElem.evalBasisDerivative( sRef, tRef, &phis3, &phit3 );
  qElem.evalBasisHessianDerivative( sRef, tRef, &phiss3, &phist3, &phitt3 );
  for (k = 0; k < 3; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], tol, tol );
    SANS_CHECK_CLOSE( phistTrue[k], phist[k], tol, tol );
    SANS_CHECK_CLOSE( phittTrue[k], phitt[k], tol, tol );

    BOOST_CHECK_CLOSE( phiTrue[k], phi3[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis3[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit3[k], tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss3[k], tol, tol );
    SANS_CHECK_CLOSE( phistTrue[k], phist3[k], tol, tol );
    SANS_CHECK_CLOSE( phittTrue[k], phitt3[k], tol, tol );
  }

  sRef = 1;  tRef = 0;
  phiTrue[0] = 0;  phiTrue[1] = 1;  phiTrue[2] = 0;

  qElem.evalBasis( sRef, tRef, phi, 3 );
  qElem.evalBasisDerivative( sRef, tRef, phis, phit, 3 );
  for (k = 0; k < 3; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
  }

  sRef = 0;  tRef = 1;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 1;

  qElem.evalBasis( sRef, tRef, phi, 3 );
  qElem.evalBasisDerivative( sRef, tRef, phis, phit, 3 );
  for (k = 0; k < 3; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
  }

  sRef = 1./3.;  tRef = 1./3.;
  phiTrue[0] = 1./3.;  phiTrue[1] = 1./3.;  phiTrue[2] = 1./3.;

  qElem.evalBasis( sRef, tRef, phi, 3 );
  qElem.evalBasisDerivative( sRef, tRef, phis, phit, 3 );
  for (k = 0; k < 3; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( eval_basis_P2 )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD2, Triangle > ElementClass;

  int order = 2;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  static const int nDOF = 6;

  BOOST_CHECK_EQUAL( order, qElem.order() );
  BOOST_CHECK_EQUAL( nDOF, qElem.nDOF() );

  const Real tol = 1e-13;
  Real sRef, tRef;
  Real phi[nDOF], phis[nDOF], phit[nDOF];
  Real phiss[nDOF], phist[nDOF], phitt[nDOF];
  Real phiTrue[nDOF], phisTrue[nDOF], phitTrue[nDOF];
  Real phissTrue[nDOF], phistTrue[nDOF], phittTrue[nDOF];
  int k;

  sRef = 0;  tRef = 0;
  phiTrue[0]   =  1;  phiTrue[1]   =  0;  phiTrue[2]   =  0;  phiTrue[3]   =  0;  phiTrue[4]   =  0;  phiTrue[5]   =  0;
  phisTrue[0]  = -1;  phisTrue[1]  =  1;  phisTrue[2]  =  0;  phisTrue[3]  =  0;  phisTrue[4]  =  0;  phisTrue[5]  =  4;
  phitTrue[0]  = -1;  phitTrue[1]  =  0;  phitTrue[2]  =  1;  phitTrue[3]  =  0;  phitTrue[4]  =  4;  phitTrue[5]  =  0;

  phissTrue[0] =  0;  phissTrue[1] =  0;  phissTrue[2] =  0;  phissTrue[3] =  0;  phissTrue[4] =  0;  phissTrue[5] = -8;
  phistTrue[0] =  0;  phistTrue[1] =  0;  phistTrue[2] =  0;  phistTrue[3] =  4;  phistTrue[4] = -4;  phistTrue[5] = -4;
  phittTrue[0] =  0;  phittTrue[1] =  0;  phittTrue[2] =  0;  phittTrue[3] =  0;  phittTrue[4] = -8;  phittTrue[5] =  0;

  qElem.evalBasis( sRef, tRef, phi, nDOF );
  qElem.evalBasisDerivative( sRef, tRef, phis, phit, nDOF );
  qElem.evalBasisHessianDerivative( sRef, tRef, phiss, phist, phitt, nDOF );
  for (k = 0; k < nDOF; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );
    BOOST_CHECK_CLOSE( phistTrue[k], phist[k], tol );
    BOOST_CHECK_CLOSE( phittTrue[k], phitt[k], tol );
  }

  sRef = 1;  tRef = 0;
  phiTrue[0]  =  0;  phiTrue[1]  =  1;  phiTrue[2]  =  0;  phiTrue[3]  =  0;  phiTrue[4]  =  0;  phiTrue[5]  =  0;
  phisTrue[0] = -1;  phisTrue[1] =  1;  phisTrue[2] =  0;  phisTrue[3] =  0;  phisTrue[4] =  0;  phisTrue[5] = -4;
  phitTrue[0] = -1;  phitTrue[1] =  0;  phitTrue[2] =  1;  phitTrue[3] =  4;  phitTrue[4] =  0;  phitTrue[5] = -4;

  qElem.evalBasis( sRef, tRef, phi, nDOF );
  qElem.evalBasisDerivative( sRef, tRef, phis, phit, nDOF );
  qElem.evalBasisHessianDerivative( sRef, tRef, phiss, phist, phitt, nDOF );
  for (k = 0; k < nDOF; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );
    BOOST_CHECK_CLOSE( phistTrue[k], phist[k], tol );
    BOOST_CHECK_CLOSE( phittTrue[k], phitt[k], tol );
  }

  sRef = 0;  tRef = 1;
  phiTrue[0]  =  0;  phiTrue[1]  =  0;  phiTrue[2]  =  1;  phiTrue[3]  =  0;  phiTrue[4]  =  0;  phiTrue[5]  =  0;
  phisTrue[0] = -1;  phisTrue[1] =  1;  phisTrue[2] =  0;  phisTrue[3] =  4;  phisTrue[4] = -4;  phisTrue[5] =  0;
  phitTrue[0] = -1;  phitTrue[1] =  0;  phitTrue[2] =  1;  phitTrue[3] =  0;  phitTrue[4] = -4;  phitTrue[5] =  0;

  qElem.evalBasis( sRef, tRef, phi, nDOF );
  qElem.evalBasisDerivative( sRef, tRef, phis, phit, nDOF );
  qElem.evalBasisHessianDerivative( sRef, tRef, phiss, phist, phitt, nDOF );
  for (k = 0; k < nDOF; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );
    BOOST_CHECK_CLOSE( phistTrue[k], phist[k], tol );
    BOOST_CHECK_CLOSE( phittTrue[k], phitt[k], tol );
  }

  sRef = 1./3.;  tRef = 1./3.;
  phiTrue[0]  = 1./3; phiTrue[1]  = 1./3; phiTrue[2]  = 1./3; phiTrue[3]  = 4./9; phiTrue[4]  = 4./9;  phiTrue[5]  = 4./9;
  phisTrue[0] = -1;   phisTrue[1] =  1;   phisTrue[2] = 0;    phisTrue[3] = 4./3; phisTrue[4] = -4./3; phisTrue[5] = 0.0;
  phitTrue[0] = -1;   phitTrue[1] =  0;   phitTrue[2] = 1;    phitTrue[3] = 4./3; phitTrue[4] = 0.0;   phitTrue[5] = -4./3;

  qElem.evalBasis( sRef, tRef, phi, nDOF );
  qElem.evalBasisDerivative( sRef, tRef, phis, phit, nDOF );
  qElem.evalBasisHessianDerivative( sRef, tRef, phiss, phist, phitt, nDOF );
  for (k = 0; k < nDOF; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], tol, tol);
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], tol, tol);
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], tol, tol);
    BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );
    BOOST_CHECK_CLOSE( phistTrue[k], phist[k], tol );
    BOOST_CHECK_CLOSE( phittTrue[k], phitt[k], tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( eval_variable )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD2, Triangle > ElementClass;

  static const int NNode = Triangle::NNode;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( 3, qElem.nDOF() );

  const Real tol = 1e-13;
  Real sRef, tRef;
  Real phi[NNode];
  Real phiTrue[NNode];
  ArrayQ q, qTrue;                // variable
  ArrayQ qs, qt, qsTrue, qtTrue;  // variable derivatives wrt sRef
  int k;

  Real q1Data[2] = {1, 2};
  Real q2Data[2] = {3, 4};
  Real q3Data[2] = {5, 6};
  ArrayQ q1(q1Data, 2);
  ArrayQ q2(q2Data, 2);
  ArrayQ q3(q3Data, 2);

  qElem.DOF(0) = q1;
  qElem.DOF(1) = q2;
  qElem.DOF(2) = q3;

  // test @ (s,t) = (0,0)
  sRef = 0;  tRef = 0;
  phiTrue[0] = 1;  phiTrue[1] = 0;  phiTrue[2] = 0;
  qTrue = q1;
  qsTrue = q2 - q1;  // compute true derivatives using finite difference (which is exact for linear)
  qtTrue = q3 - q1;  // compute true derivatives using finite difference (which is exact for linear)

  qElem.evalBasis( sRef, tRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  qElem.evalDerivative( sRef, tRef, qs, qt );

  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  for (k = 0; k < 2; k++)
  {
    BOOST_CHECK_CLOSE( qTrue[k],  q[k],  tol );
    BOOST_CHECK_CLOSE( qsTrue[k], qs[k], tol );
    BOOST_CHECK_CLOSE( qtTrue[k], qt[k], tol );
  }


  // test @ (s,t) = (1,0)
  sRef = 1;  tRef = 0;
  phiTrue[0] = 0;  phiTrue[1] = 1;  phiTrue[2] = 0;
  qTrue = q2;
  qsTrue = q2 - q1;  // compute true derivatives using finite difference (which is exact for linear)
  qtTrue = q3 - q1;  // compute true derivatives using finite difference (which is exact for linear)

  qElem.evalBasis( sRef, tRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  qElem.evalDerivative( sRef, tRef, qs, qt );

  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  for (k = 0; k < 2; k++)
  {
    BOOST_CHECK_CLOSE( qTrue[k],  q[k],  tol );
    BOOST_CHECK_CLOSE( qsTrue[k], qs[k], tol );
    BOOST_CHECK_CLOSE( qtTrue[k], qt[k], tol );
  }

  q = 0; // cppcheck-suppress redundantAssignment
  q = qElem.eval( sRef, tRef );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );


  // test @ (s,t) = (0,1)
  sRef = 0;  tRef = 1;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 1;
  qTrue = q3;
  qsTrue = q2 - q1;  // compute true derivatives using finite difference (which is exact for linear)
  qtTrue = q3 - q1;  // compute true derivatives using finite difference (which is exact for linear)

  qElem.evalBasis( sRef, tRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  qElem.evalDerivative( sRef, tRef, qs, qt );

  for (k = 0; k < NNode; k++)
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  for (k = 0; k < 2; k++)
  {
    BOOST_CHECK_CLOSE( qTrue[k],  q[k],  tol );
    BOOST_CHECK_CLOSE( qsTrue[k], qs[k], tol );
    BOOST_CHECK_CLOSE( qtTrue[k], qt[k], tol );
  }

  q = 0; // cppcheck-suppress redundantAssignment
  qElem.eval( sRef, tRef, q );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );


  // test @ (s,t) = (1/3,1/3)
  sRef = 1./3.;  tRef = 1./3.;
  phiTrue[0] = 1./3.;  phiTrue[1] = 1./3.;  phiTrue[2] = 1./3.;
  qTrue = (q1 + q2 + q3)/3.;
  qsTrue = q2 - q1;  // compute true derivatives using finite difference (which is exact for linear)
  qtTrue = q3 - q1;  // compute true derivatives using finite difference (which is exact for linear)

  qElem.evalBasis( sRef, tRef, phi, 3 );
  qElem.evalFromBasis( phi, NNode, q );
  qElem.evalDerivative( sRef, tRef, qs, qt );

  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  for (k = 0; k < 2; k++)
  {
    BOOST_CHECK_CLOSE( qTrue[k],  q[k],  tol );
    BOOST_CHECK_CLOSE( qsTrue[k], qs[k], tol );
    BOOST_CHECK_CLOSE( qtTrue[k], qt[k], tol );
  }

  q = 0;
  qElem.eval( sRef, tRef, q );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( cached_basis )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef DLA::VectorS< TopoD2::D, ArrayQ > VectorArrayQ;
  typedef DLA::VectorS< TopoD2::D, Real > RefCoordCell;
  typedef DLA::VectorS< TopoD1::D, Real > RefCoordTrace;
  typedef Element< ArrayQ, TopoD2, Triangle > ElementClass;
  typedef std::array<int,Triangle::NTrace> IntTrace;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-13;

  RefCoordCell ref;
  ArrayQ q, qTrue;
  VectorArrayQ derivq, derivqTrue;

  const int nEdgeSign = 8;

  IntTrace edgeSigns[nEdgeSign] = { {{ 1, 1, 1}},
                                    {{-1, 1, 1}},
                                    {{ 1,-1, 1}},
                                    {{ 1, 1,-1}},
                                    {{-1,-1, 1}},
                                    {{ 1,-1,-1}},
                                    {{-1, 1,-1}},
                                    {{-1,-1,-1}} };

  for (int isgn = 0; isgn < nEdgeSign; isgn++)
  {
    IntTrace sgn = edgeSigns[isgn];

    for (int order = 1; order < BasisFunctionArea_Triangle_HierarchicalPMax+1; order++)
    {
      ElementClass qElem(order, BasisFunctionCategory_Hierarchical);
      qElem.setEdgeSign(sgn);

      int nDOF = qElem.nDOF();

      // initalize some arbitrary values
      for (int n = 0; n < nDOF; n++)
        qElem.DOF(n) = pow(-1, n) / sqrt(n+1);

      std::vector<Real> phiTrue(nDOF), phisTrue(nDOF), phitTrue(nDOF);
      std::vector<Real> phissTrue(nDOF), phistTrue(nDOF), phittTrue(nDOF);
      std::vector<Real> phi(nDOF), phis(nDOF), phit(nDOF);
      std::vector<Real> phiss(nDOF), phist(nDOF), phitt(nDOF);
      std::vector<RefCoordCell> dphi(nDOF);

      // check that the cached basis functions work
      for (int iorderidx = 0; iorderidx < QuadratureArea<Triangle>::nOrderIdx; iorderidx++)
      {
        QuadratureArea<Triangle> quadrature( QuadratureArea<Triangle>::getOrderFromIndex(iorderidx) );
        for (int n = 0; n < quadrature.nQuadrature(); n++)
        {
          quadrature.coordinates(n, ref);
          QuadraturePoint<TopoD2> point = quadrature.coordinates_cache(n);

          //--------------
          qElem.evalBasis( ref, phiTrue.data(), phiTrue.size() );
          qElem.evalBasis( point, phi.data(), phi.size() );

          for (int k = 0; k < nDOF; k++)
          {
            SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
            phi[k] = 0;
          }

          // 'non-quadrature' coordinate (mainly used with testing of integrands)
          qElem.evalBasis( QuadraturePoint<TopoD2>(ref), phi.data(), phi.size() );

          for (int k = 0; k < nDOF; k++)
            SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );


          //--------------
          qElem.evalBasisDerivative( ref[0], ref[1], phisTrue.data(), phitTrue.data(), phisTrue.size() );
          qElem.evalBasisDerivative( point, phis.data(), phit.data(), phis.size() );
          qElem.evalBasisDerivative( point, dphi.data(), dphi.size() );

          for (int k = 0; k < nDOF; k++)
          {
            SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phisTrue[k], dphi[k][0], small_tol, close_tol );
            SANS_CHECK_CLOSE( phitTrue[k], dphi[k][1], small_tol, close_tol );
            phis[k] = 0; phit[k] = 0; dphi[k] = 0;
          }

          // 'non-quadrature' coordinate (mainly used with testing of integrands)
          qElem.evalBasisDerivative( QuadraturePoint<TopoD2>(ref), phis.data(), phit.data(), phis.size() );

          for (int k = 0; k < nDOF; k++)
          {
            SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
          }


          //--------------
          qElem.evalBasisHessianDerivative( ref[0], ref[1], phissTrue.data(), phistTrue.data(), phittTrue.data(), phissTrue.size() );
          qElem.evalBasisHessianDerivative( point, phiss.data(), phist.data(), phitt.data(), phiss.size() );

          for (int k = 0; k < nDOF; k++)
          {
            SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
            phiss[k] = 0; phist[k] = 0; phitt[k] = 0;
          }

          // 'non-quadrature' coordinate (mainly used with testing of integrands)
          qElem.evalBasisHessianDerivative( QuadraturePoint<TopoD2>(ref), phiss.data(), phist.data(), phitt.data(), phiss.size() );

          for (int k = 0; k < nDOF; k++)
          {
            SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
          }


          //--------------
          qElem.eval( ref, qTrue );
          qElem.eval( point, q );

          for (int k = 0; k < ArrayQ::M; k++)
            SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );

          // 'non-quadrature' coordinate (mainly used with testing of integrands)
          qElem.eval( QuadraturePoint<TopoD2>(ref), q );

          for (int k = 0; k < ArrayQ::M; k++)
            SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );


          //--------------
          qElem.evalDerivative( ref, derivqTrue );
          qElem.evalDerivative( point, derivq );

          for (int k = 0; k < ArrayQ::M; k++)
          {
            SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
            SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
          }

          // 'non-quadrature' coordinate (mainly used with testing of integrands)
          qElem.evalDerivative( QuadraturePoint<TopoD2>(ref), derivq );

          for (int k = 0; k < ArrayQ::M; k++)
          {
            SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
            SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
          }

        } // iquad
      } // orderidx


      for (int itrace = 0; itrace < Triangle::NTrace; itrace++)
      {
        for (int orientation : {-1, 1})
        {
          CanonicalTraceToCell canonicalTrace(itrace, orientation);

          // relax tolerances for non-canonical traces
          Real small_tol = 1e-12;
          Real close_tol = 1e-13;
          if (orientation == -1)
          {
            small_tol = 1e-11;
            close_tol = 2e-8;
          }

          for (int orderidx = 0; orderidx < QuadratureLine::nOrderIdx; orderidx++)
          {
            QuadratureLine quadrature( QuadratureLine::getOrderFromIndex(orderidx) );

            RefCoordTrace sTrace;
            RefCoordCell refCell;
            QuadratureCellTracePoint<TopoD2> pointCell, pointCell2;
            for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
            {
              quadrature.coordinates(iquad, sTrace);
              QuadraturePoint<TopoD1> pointTrace = quadrature.coordinates_cache(iquad);

              TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalTrace, sTrace, refCell );
              TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalTrace, pointTrace, pointCell );
              TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalTrace, QuadraturePoint<TopoD1>(sTrace), pointCell2 );

              SANS_CHECK_CLOSE( refCell[0], pointCell.ref[0], small_tol, close_tol );
              SANS_CHECK_CLOSE( refCell[1], pointCell.ref[1], small_tol, close_tol );

              //--------------
              qElem.evalBasis( refCell, phiTrue.data(), phiTrue.size() );
              qElem.evalBasis( pointCell, phi.data(), phi.size() );

              for (int k = 0; k < nDOF; k++)
              {
                SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
                phi[k] = 0;
              }

              // 'non-quadrature' coordinate (mainly used with testing of integrands)
              qElem.evalBasis( pointCell2, phi.data(), phi.size() );

              for (int k = 0; k < nDOF; k++)
                SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );

              //--------------
              qElem.evalBasisDerivative( refCell[0], refCell[1], phisTrue.data(), phitTrue.data(), phisTrue.size() );
              qElem.evalBasisDerivative( pointCell, phis.data(), phit.data(), phis.size() );
              qElem.evalBasisDerivative( pointCell, dphi.data(), dphi.size() );

              for (int k = 0; k < nDOF; k++)
              {
                SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
                SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
                SANS_CHECK_CLOSE( phisTrue[k], dphi[k][0], small_tol, close_tol );
                SANS_CHECK_CLOSE( phitTrue[k], dphi[k][1], small_tol, close_tol );
                phis[k] = 0; phit[k] = 0; dphi[k] = 0;
              }

              // 'non-quadrature' coordinate (mainly used with testing of integrands)
              qElem.evalBasisDerivative( pointCell2, phis.data(), phit.data(), phis.size() );

              for (int k = 0; k < nDOF; k++)
              {
                SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
                SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
              }

              //--------------
              qElem.eval( refCell, qTrue );
              qElem.eval( pointCell, q );

              for (int k = 0; k < ArrayQ::M; k++)
                SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );

              // 'non-quadrature' coordinate (mainly used with testing of integrands)
              qElem.eval( pointCell2, q );

              for (int k = 0; k < ArrayQ::M; k++)
                SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );


              //--------------
              qElem.evalDerivative( refCell, derivqTrue );
              qElem.evalDerivative( pointCell, derivq );

              for (int k = 0; k < ArrayQ::M; k++)
              {
                SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
                SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
              }

              // 'non-quadrature' coordinate (mainly used with testing of integrands)
              qElem.evalDerivative( pointCell2, derivq );

              for (int k = 0; k < ArrayQ::M; k++)
              {
                SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
                SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
              }

            } //iquad
          } // orderidx
        } // orientation
      } // itrace

    } // order
  } // isgn


  for (int order = 0; order < BasisFunctionArea_Triangle_LegendrePMax+1; order++)
  {
    ElementClass qElem(order, BasisFunctionCategory_Legendre);

    int nDOF = qElem.nDOF();

    // initalize some arbitrary values
    for (int n = 0; n < nDOF; n++)
      qElem.DOF(n) = pow(-1, n) / sqrt(n+1);

    std::vector<Real> phiTrue(nDOF), phisTrue(nDOF), phitTrue(nDOF);
    std::vector<Real> phissTrue(nDOF), phistTrue(nDOF), phittTrue(nDOF);
    std::vector<Real> phi(nDOF), phis(nDOF), phit(nDOF);
    std::vector<Real> phiss(nDOF), phist(nDOF), phitt(nDOF);
    std::vector<RefCoordCell> dphi(nDOF);

    // check that the cached basis functions work
    for (int iorderidx = 0; iorderidx < QuadratureArea<Triangle>::nOrderIdx; iorderidx++)
    {
      QuadratureArea<Triangle> quadrature( QuadratureArea<Triangle>::getOrderFromIndex(iorderidx) );
      for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
      {
        quadrature.coordinates(iquad, ref);
        QuadraturePoint<TopoD2> point = quadrature.coordinates_cache(iquad);

        //--------------
        qElem.evalBasis( ref, phiTrue.data(), phiTrue.size() );
        qElem.evalBasis( point, phi.data(), phi.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
          phi[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasis( QuadraturePoint<TopoD2>(ref), phi.data(), phi.size() );

        for (int k = 0; k < nDOF; k++)
          SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );


        //--------------
        qElem.evalBasisDerivative( ref[0], ref[1], phisTrue.data(), phitTrue.data(), phisTrue.size() );
        qElem.evalBasisDerivative( point, phis.data(), phit.data(), phis.size() );
        qElem.evalBasisDerivative( point, dphi.data(), dphi.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phisTrue[k], dphi[k][0], small_tol, close_tol );
          SANS_CHECK_CLOSE( phitTrue[k], dphi[k][1], small_tol, close_tol );
          phis[k] = 0; phit[k] = 0; dphi[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasisDerivative( QuadraturePoint<TopoD2>(ref), phis.data(), phit.data(), phis.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
        }

        if (order <= 1) // until more hessians are implemented...
        {
        //--------------
        qElem.evalBasisHessianDerivative( ref[0], ref[1], phissTrue.data(), phistTrue.data(), phittTrue.data(), phissTrue.size() );
        qElem.evalBasisHessianDerivative( point, phiss.data(), phist.data(), phitt.data(), phiss.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
          phiss[k] = 0; phist[k] = 0; phitt[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasisHessianDerivative( QuadraturePoint<TopoD2>(ref), phiss.data(), phist.data(), phitt.data(), phiss.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
        }
        }
        else
        BOOST_CHECK_THROW(qElem.evalBasisHessianDerivative( ref[0], ref[1], phissTrue.data(), phistTrue.data(), phittTrue.data(), phissTrue.size() ),
                          DeveloperException);

        //--------------
        qElem.eval( ref, qTrue );
        qElem.eval( point, q );

        for (int k = 0; k < ArrayQ::M; k++)
          SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.eval( QuadraturePoint<TopoD2>(ref), q );

        for (int k = 0; k < ArrayQ::M; k++)
          SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );


        //--------------
        qElem.evalDerivative( ref, derivqTrue );
        qElem.evalDerivative( point, derivq );

        for (int k = 0; k < ArrayQ::M; k++)
        {
          SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
          SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalDerivative( QuadraturePoint<TopoD2>(ref), derivq );

        for (int k = 0; k < ArrayQ::M; k++)
        {
          SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
          SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
        }

      } // iquad
    } // orderidx


    for (int itrace = 0; itrace < Triangle::NTrace; itrace++)
    {
      for (int orientation : {-1, 1})
      {
        CanonicalTraceToCell canonicalTrace(itrace, orientation);

        // relax tolerances for non-canonical traces
        Real small_tol = 1e-12;
        Real close_tol = 1e-13;
        if (orientation == -1)
        {
          small_tol = 1e-11;
          close_tol = 4e-8;
        }

        for (int orderidx = 0; orderidx < QuadratureLine::nOrderIdx; orderidx++)
        {
          QuadratureLine quadrature( QuadratureLine::getOrderFromIndex(orderidx) );

          RefCoordTrace sTrace;
          RefCoordCell refCell;
          QuadratureCellTracePoint<TopoD2> pointCell, pointCell2;
          for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
          {
            quadrature.coordinates(iquad, sTrace);
            QuadraturePoint<TopoD1> pointTrace = quadrature.coordinates_cache(iquad);

            TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalTrace, sTrace, refCell );
            TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalTrace, pointTrace, pointCell );
            TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalTrace, QuadraturePoint<TopoD1>(sTrace), pointCell2 );

            SANS_CHECK_CLOSE( refCell[0], pointCell.ref[0], small_tol, close_tol );
            SANS_CHECK_CLOSE( refCell[1], pointCell.ref[1], small_tol, close_tol );

            //--------------
            qElem.evalBasis( refCell, phiTrue.data(), phiTrue.size() );
            qElem.evalBasis( pointCell, phi.data(), phi.size() );

            for (int k = 0; k < nDOF; k++)
            {
              SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
              phi[k] = 0;
            }

            // 'non-quadrature' coordinate (mainly used with testing of integrands)
            qElem.evalBasis( pointCell2, phi.data(), phi.size() );

            for (int k = 0; k < nDOF; k++)
              SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );

            //--------------
            qElem.evalBasisDerivative( refCell[0], refCell[1], phisTrue.data(), phitTrue.data(), phisTrue.size() );
            qElem.evalBasisDerivative( pointCell, phis.data(), phit.data(), phis.size() );
            qElem.evalBasisDerivative( pointCell, dphi.data(), dphi.size() );

            for (int k = 0; k < nDOF; k++)
            {
              SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
              SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
              SANS_CHECK_CLOSE( phisTrue[k], dphi[k][0], small_tol, close_tol );
              SANS_CHECK_CLOSE( phitTrue[k], dphi[k][1], small_tol, close_tol );
              phis[k] = 0; phit[k] = 0; dphi[k] = 0;
            }

            // 'non-quadrature' coordinate (mainly used with testing of integrands)
            qElem.evalBasisDerivative( pointCell2, phis.data(), phit.data(), phis.size() );

            for (int k = 0; k < nDOF; k++)
            {
              SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
              SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
            }

            //--------------
            qElem.eval( refCell, qTrue );
            qElem.eval( pointCell, q );

            for (int k = 0; k < ArrayQ::M; k++)
              SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );

            // 'non-quadrature' coordinate (mainly used with testing of integrands)
            qElem.eval( pointCell2, q );

            for (int k = 0; k < ArrayQ::M; k++)
              SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );


            //--------------
            qElem.evalDerivative( refCell, derivqTrue );
            qElem.evalDerivative( pointCell, derivq );

            for (int k = 0; k < ArrayQ::M; k++)
            {
              SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
              SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
            }

            // 'non-quadrature' coordinate (mainly used with testing of integrands)
            qElem.evalDerivative( pointCell2, derivq );

            for (int k = 0; k < ArrayQ::M; k++)
            {
              SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
              SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
            }

          } //iquad
        } // orderidx
      } // orientation
    } // itrace

  } // order


  for (int order = 1; order < BasisFunctionArea_Triangle_LagrangePMax+1; order++)
  {
    ElementClass qElem(order, BasisFunctionCategory_Lagrange);

    int nDOF = qElem.nDOF();

    // initalize some arbitrary values
    for (int n = 0; n < nDOF; n++)
      qElem.DOF(n) = pow(-1, n) / sqrt(n+1);

    std::vector<Real> phiTrue(nDOF), phisTrue(nDOF), phitTrue(nDOF);
    std::vector<Real> phissTrue(nDOF), phistTrue(nDOF), phittTrue(nDOF);
    std::vector<Real> phi(nDOF), phis(nDOF), phit(nDOF);
    std::vector<Real> phiss(nDOF), phist(nDOF), phitt(nDOF);
    std::vector<RefCoordCell> dphi(nDOF);

    // check that the cached basis functions work
    for (int iorderidx = 0; iorderidx < QuadratureArea<Triangle>::nOrderIdx; iorderidx++)
    {
      QuadratureArea<Triangle> quadrature( QuadratureArea<Triangle>::getOrderFromIndex(iorderidx) );
      for (int n = 0; n < quadrature.nQuadrature(); n++)
      {
        quadrature.coordinates(n, ref);
        QuadraturePoint<TopoD2> point = quadrature.coordinates_cache(n);

        //--------------
        qElem.evalBasis( ref, phiTrue.data(), phiTrue.size() );
        qElem.evalBasis( point, phi.data(), phi.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
          phi[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasis( QuadraturePoint<TopoD2>(ref), phi.data(), phi.size() );

        for (int k = 0; k < nDOF; k++)
          SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );


        //--------------
        qElem.evalBasisDerivative( ref[0], ref[1], phisTrue.data(), phitTrue.data(), phisTrue.size() );
        qElem.evalBasisDerivative( point, phis.data(), phit.data(), phis.size() );
        qElem.evalBasisDerivative( point, dphi.data(), dphi.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phisTrue[k], dphi[k][0], small_tol, close_tol );
          SANS_CHECK_CLOSE( phitTrue[k], dphi[k][1], small_tol, close_tol );
          phis[k] = 0; phit[k] = 0; dphi[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasisDerivative( QuadraturePoint<TopoD2>(ref), phis.data(), phit.data(), phis.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
        }


        //--------------
        qElem.evalBasisHessianDerivative( ref[0], ref[1], phissTrue.data(), phistTrue.data(), phittTrue.data(), phissTrue.size() );
        qElem.evalBasisHessianDerivative( point, phiss.data(), phist.data(), phitt.data(), phiss.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
          phiss[k] = 0; phist[k] = 0; phitt[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasisHessianDerivative( QuadraturePoint<TopoD2>(ref), phiss.data(), phist.data(), phitt.data(), phiss.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
        }


        //--------------
        qElem.eval( ref, qTrue );
        qElem.eval( point, q );

        for (int k = 0; k < ArrayQ::M; k++)
          SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.eval( QuadraturePoint<TopoD2>(ref), q );

        for (int k = 0; k < ArrayQ::M; k++)
          SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );


        //--------------
        qElem.evalDerivative( ref, derivqTrue );
        qElem.evalDerivative( point, derivq );

        for (int k = 0; k < ArrayQ::M; k++)
        {
          SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
          SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalDerivative( QuadraturePoint<TopoD2>(ref), derivq );

        for (int k = 0; k < ArrayQ::M; k++)
        {
          SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
          SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
        }

      } // iquad
    } // orderidx


    for (int itrace = 0; itrace < Triangle::NTrace; itrace++)
    {
      for (int orientation : {-1, 1})
      {
        CanonicalTraceToCell canonicalTrace(itrace, orientation);

        // relax tolerances for non-canonical traces
        Real small_tol = 1e-12;
        Real close_tol = 1e-13;
        if (orientation == -1)
        {
          small_tol = 1e-11;
          close_tol = 2e-8;
        }

        for (int orderidx = 0; orderidx < QuadratureLine::nOrderIdx; orderidx++)
        {
          QuadratureLine quadrature( QuadratureLine::getOrderFromIndex(orderidx) );

          RefCoordTrace sTrace;
          RefCoordCell refCell;
          QuadratureCellTracePoint<TopoD2> pointCell, pointCell2;
          for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
          {
            quadrature.coordinates(iquad, sTrace);
            QuadraturePoint<TopoD1> pointTrace = quadrature.coordinates_cache(iquad);

            TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalTrace, sTrace, refCell );
            TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalTrace, pointTrace, pointCell );
            TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalTrace, QuadraturePoint<TopoD1>(sTrace), pointCell2 );

            SANS_CHECK_CLOSE( refCell[0], pointCell.ref[0], small_tol, close_tol );
            SANS_CHECK_CLOSE( refCell[1], pointCell.ref[1], small_tol, close_tol );

            //--------------
            qElem.evalBasis( refCell, phiTrue.data(), phiTrue.size() );
            qElem.evalBasis( pointCell, phi.data(), phi.size() );

            for (int k = 0; k < nDOF; k++)
            {
              SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
              phi[k] = 0;
            }

            // 'non-quadrature' coordinate (mainly used with testing of integrands)
            qElem.evalBasis( pointCell2, phi.data(), phi.size() );

            for (int k = 0; k < nDOF; k++)
              SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );

            //--------------
            qElem.evalBasisDerivative( refCell[0], refCell[1], phisTrue.data(), phitTrue.data(), phisTrue.size() );
            qElem.evalBasisDerivative( pointCell, phis.data(), phit.data(), phis.size() );
            qElem.evalBasisDerivative( pointCell, dphi.data(), dphi.size() );

            for (int k = 0; k < nDOF; k++)
            {
              SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
              SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
              SANS_CHECK_CLOSE( phisTrue[k], dphi[k][0], small_tol, close_tol );
              SANS_CHECK_CLOSE( phitTrue[k], dphi[k][1], small_tol, close_tol );
              phis[k] = 0; phit[k] = 0; dphi[k] = 0;
            }

            // 'non-quadrature' coordinate (mainly used with testing of integrands)
            qElem.evalBasisDerivative( pointCell2, phis.data(), phit.data(), phis.size() );

            for (int k = 0; k < nDOF; k++)
            {
              SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
              SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
            }

            //--------------
            qElem.eval( refCell, qTrue );
            qElem.eval( pointCell, q );

            for (int k = 0; k < ArrayQ::M; k++)
              SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );

            // 'non-quadrature' coordinate (mainly used with testing of integrands)
            qElem.eval( pointCell2, q );

            for (int k = 0; k < ArrayQ::M; k++)
              SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );


            //--------------
            qElem.evalDerivative( refCell, derivqTrue );
            qElem.evalDerivative( pointCell, derivq );

            for (int k = 0; k < ArrayQ::M; k++)
            {
              SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
              SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
            }

            // 'non-quadrature' coordinate (mainly used with testing of integrands)
            qElem.evalDerivative( pointCell2, derivq );

            for (int k = 0; k < ArrayQ::M; k++)
            {
              SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
              SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
            }

          } //iquad
        } // orderidx
      } // orientation
    } // itrace

  } // order
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( extract_basis )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD2, Triangle > ElementClass;

  int order = 1;
  ElementClass qElem_P1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem_P1.order() );
  BOOST_CHECK_EQUAL( 3, qElem_P1.nDOF() );

  const BasisFunctionAreaBase<Triangle>* basis_P1 = qElem_P1.basis();

  BOOST_CHECK_EQUAL( 1, basis_P1->order() );
  BOOST_CHECK_EQUAL( 3, basis_P1->nBasis() );


  order = 2;
  ElementClass qElem_P2(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qElem_P2.order() );
  BOOST_CHECK_EQUAL( 6, qElem_P2.nDOF() );

  const BasisFunctionAreaBase<Triangle>* basis_P2 = qElem_P2.basis();

  BOOST_CHECK_EQUAL( 2, basis_P2->order() );
  BOOST_CHECK_EQUAL( 6, basis_P2->nBasis() );


  order = 3;
  ElementClass qElem_P3(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 3, qElem_P3.order() );
  BOOST_CHECK_EQUAL( 10, qElem_P3.nDOF() );

  const BasisFunctionAreaBase<Triangle>* basis_P3 = qElem_P3.basis();

  BOOST_CHECK_EQUAL( 3, basis_P3->order() );
  BOOST_CHECK_EQUAL( 10, basis_P3->nBasis() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( edge_sign )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD2, Triangle > ElementClass;
  typedef std::array<int,3> Int3;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  Int3 edgeSign = qElem.edgeSign();
  BOOST_CHECK_EQUAL( +1, edgeSign[0] );
  BOOST_CHECK_EQUAL( +1, edgeSign[1] );
  BOOST_CHECK_EQUAL( +1, edgeSign[2] );

  edgeSign[1] = -1;
  qElem.setEdgeSign(edgeSign);

  Int3 edgeSign2 = qElem.edgeSign();
  BOOST_CHECK_EQUAL( +1, edgeSign2[0] );
  BOOST_CHECK_EQUAL( -1, edgeSign2[1] );
  BOOST_CHECK_EQUAL( +1, edgeSign2[2] );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( projection )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD2, Triangle > ElementClass;

  Real small_tol = 5e-13;
  Real close_tol = 5e-12;

  for (int order = 1; order < BasisFunctionArea_Triangle_HierarchicalPMax; order++)
  {
    for (int inc = 1; inc <= BasisFunctionArea_Triangle_HierarchicalPMax-order; inc++)
    {
      ElementClass qElem0( order,       BasisFunctionCategory_Hierarchical );
      ElementClass qElem1( order + inc, BasisFunctionCategory_Hierarchical );

      for (int k = 0; k < qElem0.nDOF(); k++)
      {
        ArrayQ q = {sqrt(k+7), 1./(k+3)};
        qElem0.DOF(k) = q;
      }

      qElem0.projectTo( qElem1 );

      ArrayQ q0, q1;
      Real s, t;

      for (int is = 0; is < 3; is++)
      {
        for (int it = 0; it < 3; it++)
        {
          s = 0.11 + is*0.3357;
          t = 0.13 + it*0.2986;

          qElem0.eval( s, t, q0 );
          qElem1.eval( s, t, q1 );

          for (int n = 0; n < ArrayQ::N; n++)
            SANS_CHECK_CLOSE( q0[n], q1[n], small_tol, close_tol );
        }
      }
    }
  }

  for (int order = 0; order < BasisFunctionArea_Triangle_LegendrePMax; order++)
  {
    for (int inc = 1; inc <= BasisFunctionArea_Triangle_LegendrePMax-order; inc++)
    {
      ElementClass qElem0( order,       BasisFunctionCategory_Legendre );
      ElementClass qElem1( order + inc, BasisFunctionCategory_Legendre );

      for (int k = 0; k < qElem0.nDOF(); k++)
      {
        ArrayQ q = {sqrt(k+7), 1./(k+3)};
        qElem0.DOF(k) = q;
      }

      qElem0.projectTo( qElem1 );

      ArrayQ q0, q1;
      Real s, t;

      for (int is = 0; is < 3; is++)
      {
        for (int it = 0; it < 3; it++)
        {
          s = 0.11 + is*0.3357;
          t = 0.13 + it*0.2986;

          qElem0.eval( s, t, q0 );
          qElem1.eval( s, t, q1 );

          for (int n = 0; n < ArrayQ::N; n++)
            SANS_CHECK_CLOSE( q0[n], q1[n], small_tol, close_tol );
        }
      }
    }
  }

  //Relax tolerances a bit for Lagrange
  small_tol = 5e-10;
  close_tol = 5e-10;

  for (int order = 1; order < BasisFunctionArea_Triangle_LagrangePMax; order++)
  {
    for (int inc = 1; inc <= BasisFunctionArea_Triangle_LagrangePMax-order; inc++)
    {
      ElementClass qElem0( order,       BasisFunctionCategory_Lagrange );
      ElementClass qElem1( order + inc, BasisFunctionCategory_Lagrange );

      for (int k = 0; k < qElem0.nDOF(); k++)
      {
        ArrayQ q = {sqrt(k+7), 1./(k+3)};
        qElem0.DOF(k) = q;
      }

      qElem0.projectTo( qElem1 );

      ArrayQ q0, q1;
      Real s, t;

      for (int is = 0; is < 3; is++)
      {
        for (int it = 0; it < 3; it++)
        {
          s = 0.11 + is*0.3357;
          t = 0.13 + it*0.2986;

          qElem0.eval( s, t, q0 );
          qElem1.eval( s, t, q1 );

          for (int n = 0; n < ArrayQ::N; n++)
            SANS_CHECK_CLOSE( q0[n], q1[n], small_tol, close_tol );
        }
      }
    }
  }


  {
    // P=1 Lagrange can be projected to any hierarchical P
    int order = 1;
    ElementClass qElem0( order,       BasisFunctionCategory_Lagrange );
    for (int inc = 1; inc <= BasisFunctionArea_Triangle_HierarchicalPMax-order; inc++)
    {
      ElementClass qElem1( order + inc, BasisFunctionCategory_Hierarchical );

      for (int k = 0; k < qElem0.nDOF(); k++)
      {
        ArrayQ q = {sqrt(k+7), 1./(k+3)};
        qElem0.DOF(k) = q;
      }

      qElem0.projectTo( qElem1 );

      ArrayQ q0, q1;
      Real s, t;

      for (int is = 0; is < 3; is++)
      {
        for (int it = 0; it < 3; it++)
        {
          s = 0.11 + is*0.3357;
          t = 0.13 + it*0.2986;

          qElem0.eval( s, t, q0 );
          qElem1.eval( s, t, q1 );

          for (int n = 0; n < ArrayQ::N; n++)
            SANS_CHECK_CLOSE( q0[n], q1[n], small_tol, close_tol );
        }
      }
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD2, Triangle > ElementClass;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/ElementArea_Triangle_pattern.txt", true );

  ElementClass qElem(1, BasisFunctionCategory_Hierarchical);

  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};
  ArrayQ q3 = {5, 6};

  qElem.DOF(0) = q1;
  qElem.DOF(1) = q2;
  qElem.DOF(2) = q3;

  qElem.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
