// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field_Tuple_btest
// testing of FieldTuple classes
//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/Tuple.h"

#include "Surreal/SurrealS.h"

#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "Field/Element/ElementArea.h"
#include "Field/Element/ElementXFieldArea.h"

#include "BasisFunction/BasisFunctionLine.h"
#include "Field/Element/ElementLine.h"
#include "Field/Element/ElementXFieldLine.h"

#include "Field/Tuple/ElementTuple.h"

using namespace std;
using namespace SANS;


//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
template class ElementTuple< Element< Real, TopoD2, Triangle >, Element< Real, TopoD2, Triangle >, TupleClass<> >;
template class ElementTuple< Element< Real, TopoD2, Triangle >, ElementXField< PhysD2, TopoD2, Triangle >, TupleClass<> >;
template class ElementTuple< ElementXField< PhysD2, TopoD2, Triangle >, Element< Real, TopoD2, Triangle >, TupleClass<> >;
template class ElementTuple< ElementXField< PhysD2, TopoD2, Triangle >, ElementXField< PhysD2, TopoD2, Triangle >, TupleClass<> >;


template class BasisTuple< BasisFunctionLineBase, BasisFunctionLineBase, TupleClass<> >;
template class BasisPointTuple< BasisFunctionLineBase, BasisFunctionLineBase, TupleClass<> >;
template class ParamTuple< Real, Real, TupleClass<> >;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementTuple_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementTuple_create_append_test )
{
  // element types
  typedef Element<DLA::VectorS<1,Real> ,TopoD2, Triangle> ElementV1FieldClass;
  typedef Element<DLA::VectorS<2,Real> ,TopoD2, Triangle> ElementV2FieldClass;
  typedef Element<DLA::VectorS<3,Real> ,TopoD2, Triangle> ElementV3FieldClass;
  typedef ElementXField<PhysD2, TopoD2, Triangle> ElementXFieldClass;

  // tuple types
  typedef MakeTuple<ElementTuple, ElementV1FieldClass, ElementV2FieldClass>::type ElementParamV1V2;
  typedef MakeTuple<ElementTuple, ElementV3FieldClass, ElementXFieldClass>::type  ElementParamV3X;
  typedef MakeTuple<ElementTuple, ElementXFieldClass, ElementV3FieldClass>::type  ElementParamXV3;
  typedef MakeTuple<ElementTuple, ElementXFieldClass, ElementXFieldClass>::type   ElementParamXX;
  typedef MakeTuple<ElementTuple, ElementV1FieldClass, ElementV2FieldClass, ElementV3FieldClass>::type ElementParam3;
  typedef MakeTuple<ElementTuple, ElementV1FieldClass, ElementV2FieldClass, ElementV3FieldClass, ElementXFieldClass>::type ElementParam4;

  typedef ElementTuple<ElementParamV1V2, ElementParamV3X, TupleClass<1>> ElementParamV1V2ParamV3X;                   // level-1 tuple
  typedef ElementTuple<ElementParamV1V2ParamV3X, ElementParamV1V2, TupleClass<1>> ElementParamV1V2ParamV3XParamV1V2; // level-1 tuple

  int order = 1;
  ElementV1FieldClass v1fldElem(order, BasisFunctionCategory_Hierarchical);

  v1fldElem.DOF(0) = 1;
  v1fldElem.DOF(1) = 1;
  v1fldElem.DOF(2) = 1;

  ElementV2FieldClass v2fldElem(order, BasisFunctionCategory_Hierarchical);

  v2fldElem.DOF(0) = 2;
  v2fldElem.DOF(1) = 2;
  v2fldElem.DOF(2) = 2;

  ElementV3FieldClass v3fldElem(order, BasisFunctionCategory_Hierarchical);

  v3fldElem.DOF(0) = 3;
  v3fldElem.DOF(1) = 3;
  v3fldElem.DOF(2) = 3;

  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  xfldElem.DOF(0) = {0, 0};
  xfldElem.DOF(1) = {1, 0};
  xfldElem.DOF(2) = {0, 1};


  // create a tuple from two field elements
  ElementParamV1V2 params2 = (v1fldElem, v2fldElem);

  const ElementV1FieldClass& v1fldElemRef2 = get<0>(params2);
  const ElementV2FieldClass& v2fldElemRef2 = get<1>(params2);

  for (int n = 0; n < 3; n++)
  {
    BOOST_CHECK_EQUAL( v1fldElem.DOF(n)[0], v1fldElemRef2.DOF(n)[0] );

    BOOST_CHECK_EQUAL( v2fldElem.DOF(n)[0], v2fldElemRef2.DOF(n)[0] );
    BOOST_CHECK_EQUAL( v2fldElem.DOF(n)[1], v2fldElemRef2.DOF(n)[1] );
  }

  // create a tuple from field, Xfield elements
  ElementParamXV3 paramsXV3 = (xfldElem, v3fldElem);

  // create a tuple from Xfield, field elements
  ElementParamXX paramsXX = (xfldElem, xfldElem);

  // append a field element to a tuple
  ElementParam3 params3 = (v1fldElem, v2fldElem, v3fldElem);

  const ElementV1FieldClass& v1fldElemRef3 = get<0>(params3);
  const ElementV2FieldClass& v2fldElemRef3 = get<1>(params3);
  const ElementV3FieldClass& v3fldElemRef3 = get<2>(params3);

  for (int n = 0; n < 3; n++)
  {
    BOOST_CHECK_EQUAL( v1fldElem.DOF(n)[0], v1fldElemRef3.DOF(n)[0] );

    BOOST_CHECK_EQUAL( v2fldElem.DOF(n)[0], v2fldElemRef3.DOF(n)[0] );
    BOOST_CHECK_EQUAL( v2fldElem.DOF(n)[1], v2fldElemRef3.DOF(n)[1] );

    BOOST_CHECK_EQUAL( v3fldElem.DOF(n)[0], v3fldElemRef3.DOF(n)[0] );
    BOOST_CHECK_EQUAL( v3fldElem.DOF(n)[1], v3fldElemRef3.DOF(n)[1] );
    BOOST_CHECK_EQUAL( v3fldElem.DOF(n)[2], v3fldElemRef3.DOF(n)[2] );
  }


  // append a Xfield element to a tuple
  ElementParam4 params4 = (v1fldElem, v2fldElem, v3fldElem, xfldElem);

  const ElementV1FieldClass& v1fldElemRef4 = get<0>(params4);
  const ElementV2FieldClass& v2fldElemRef4 = get<1>(params4);
  const ElementV3FieldClass& v3fldElemRef4 = get<2>(params4);
  const ElementXFieldClass&  xfldElemRef4 = get<3>(params4);

  for (int n = 0; n < 3; n++)
  {
    BOOST_CHECK_EQUAL( v1fldElem.DOF(n)[0], v1fldElemRef4.DOF(n)[0] );

    BOOST_CHECK_EQUAL( v2fldElem.DOF(n)[0], v2fldElemRef4.DOF(n)[0] );
    BOOST_CHECK_EQUAL( v2fldElem.DOF(n)[1], v2fldElemRef4.DOF(n)[1] );

    BOOST_CHECK_EQUAL( v3fldElem.DOF(n)[0], v3fldElemRef4.DOF(n)[0] );
    BOOST_CHECK_EQUAL( v3fldElem.DOF(n)[1], v3fldElemRef4.DOF(n)[1] );
    BOOST_CHECK_EQUAL( v3fldElem.DOF(n)[2], v3fldElemRef4.DOF(n)[2] );

    BOOST_CHECK_EQUAL( xfldElem.DOF(n)[0], xfldElemRef4.DOF(n)[0] );
    BOOST_CHECK_EQUAL( xfldElem.DOF(n)[1], xfldElemRef4.DOF(n)[1] );
  }


  // append a tuple to a tuple of the same level
  ElementParamV1V2ParamV3X paramsLv1_1 = ( params2, (v3fldElem, xfldElem) );

  const ElementParamV1V2& paramsLv1_1L = get<0>(paramsLv1_1);
  const ElementParamV3X& paramsLv1_1R = get<1>(paramsLv1_1);

  const ElementV1FieldClass& v1fldElemRef5 = get<0>(paramsLv1_1L);
  const ElementV2FieldClass& v2fldElemRef5 = get<1>(paramsLv1_1L);
  const ElementV3FieldClass& v3fldElemRef5 = get<0>(paramsLv1_1R);
  const ElementXFieldClass&  xfldElemRef5  = get<1>(paramsLv1_1R);

  for (int n = 0; n < 3; n++)
  {
    BOOST_CHECK_EQUAL( v1fldElem.DOF(n)[0], v1fldElemRef5.DOF(n)[0] );

    BOOST_CHECK_EQUAL( v2fldElem.DOF(n)[0], v2fldElemRef5.DOF(n)[0] );
    BOOST_CHECK_EQUAL( v2fldElem.DOF(n)[1], v2fldElemRef5.DOF(n)[1] );

    BOOST_CHECK_EQUAL( v3fldElem.DOF(n)[0], v3fldElemRef5.DOF(n)[0] );
    BOOST_CHECK_EQUAL( v3fldElem.DOF(n)[1], v3fldElemRef5.DOF(n)[1] );
    BOOST_CHECK_EQUAL( v3fldElem.DOF(n)[2], v3fldElemRef5.DOF(n)[2] );

    BOOST_CHECK_EQUAL( xfldElem.DOF(n)[0], xfldElemRef5.DOF(n)[0] );
    BOOST_CHECK_EQUAL( xfldElem.DOF(n)[1], xfldElemRef5.DOF(n)[1] );
  }


  // append a tuple to a tuple of one level lower
  ElementParamV1V2ParamV3XParamV1V2 paramsLv1_2 = ( paramsLv1_1, params2 );

  const ElementParamV1V2ParamV3X& paramsLv1_2L = paramsLv1_2.left();
  const ElementParamV1V2& paramsLv1_2R = paramsLv1_2.right();

  const ElementParamV1V2& paramsLv1_2L_L = paramsLv1_2L.left();
  const ElementParamV3X&  paramsLv1_2L_R = paramsLv1_2L.right();

  const ElementV1FieldClass& v1fldElemRef6 = get<0>(paramsLv1_2L_L);
  const ElementV2FieldClass& v2fldElemRef6 = get<1>(paramsLv1_2L_L);
  const ElementV3FieldClass& v3fldElemRef6 = get<0>(paramsLv1_2L_R);
  const ElementXFieldClass& xfldElemRef6  = get<1>(paramsLv1_2L_R);
  const ElementV1FieldClass& v1fldElemRef6_2 = get<0>(paramsLv1_2R);
  const ElementV2FieldClass& v2fldElemRef6_2 = get<1>(paramsLv1_2R);

  for (int n = 0; n < 3; n++)
  {
    BOOST_CHECK_EQUAL( v1fldElem.DOF(n)[0], v1fldElemRef6.DOF(n)[0] );

    BOOST_CHECK_EQUAL( v2fldElem.DOF(n)[0], v2fldElemRef6.DOF(n)[0] );
    BOOST_CHECK_EQUAL( v2fldElem.DOF(n)[1], v2fldElemRef6.DOF(n)[1] );

    BOOST_CHECK_EQUAL( v3fldElem.DOF(n)[0], v3fldElemRef6.DOF(n)[0] );
    BOOST_CHECK_EQUAL( v3fldElem.DOF(n)[1], v3fldElemRef6.DOF(n)[1] );
    BOOST_CHECK_EQUAL( v3fldElem.DOF(n)[2], v3fldElemRef6.DOF(n)[2] );

    BOOST_CHECK_EQUAL( xfldElem.DOF(n)[0], xfldElemRef6.DOF(n)[0] );
    BOOST_CHECK_EQUAL( xfldElem.DOF(n)[1], xfldElemRef6.DOF(n)[1] );

    BOOST_CHECK_EQUAL( v1fldElem.DOF(n)[0], v1fldElemRef6_2.DOF(n)[0] );

    BOOST_CHECK_EQUAL( v2fldElem.DOF(n)[0], v2fldElemRef6_2.DOF(n)[0] );
    BOOST_CHECK_EQUAL( v2fldElem.DOF(n)[1], v2fldElemRef6_2.DOF(n)[1] );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementTuple_eval_accessor_test )
{
  const Real tol = 1e-15;

  typedef DLA::VectorS<1,Real> TL;
  typedef DLA::VectorS<2,Real> TR;

  typedef Element<TL ,TopoD1, Line>           ElementV1FieldClass;
  typedef ElementXField<PhysD2, TopoD1, Line> ElementXFieldClass;

  typedef MakeTuple<ElementTuple, ElementV1FieldClass, ElementXFieldClass>::type  ElementParamV1X;
  const int levelTuple = 0;

  typedef ParamTuple<TL, TR, TupleClass<levelTuple>> ParamType;

  typedef BasisTuple<ElementV1FieldClass::BasisType, ElementXFieldClass::BasisType, TupleClass<levelTuple>> BasisType;
  typedef BasisType::BasisPointType BasisPointType;
  typedef BasisType::BasisPointDerivativeType BasisPointDerivativeType;

  // set up elements
  const int order = 1;

  ElementV1FieldClass v1fldElem(order, BasisFunctionCategory_Hierarchical);

  v1fldElem.DOF(0) = 1.;
  v1fldElem.DOF(1) = 2.;

  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  xfldElem.DOF(0) = {0, 0};
  xfldElem.DOF(1) = {1, 0};

  // ctor & basis

  ElementParamV1X paramElem = (v1fldElem, xfldElem);
  const BasisType paramBasis = paramElem.basis();

  ElementParamV1X paramElem2(paramBasis);

  const Real sRef = 0.5;

  // DOF accessor

  ParamType params0;
  params0 = paramElem.DOF(0);

  SANS_CHECK_CLOSE(v1fldElem.DOF(0)[0], params0.left()[0] , tol, tol);
  SANS_CHECK_CLOSE( xfldElem.DOF(0)[0], params0.right()[0], tol, tol);

  params0 = paramElem.DOF(1);

  SANS_CHECK_CLOSE(v1fldElem.DOF(1)[0], params0.left()[0] , tol, tol);
  SANS_CHECK_CLOSE( xfldElem.DOF(1)[0], params0.right()[0], tol, tol);

  // evaluation routines

  ParamType params1;
  paramElem.eval(sRef,params1);

  SANS_CHECK_CLOSE(0.5*(v1fldElem.DOF(0)[0] + v1fldElem.DOF(1)[0]),
                   params1.left()[0], tol, tol);

  SANS_CHECK_CLOSE(0.5*(xfldElem.DOF(0)[0] + xfldElem.DOF(1)[0]),
                   params1.right()[0], tol, tol);


  BasisPointType phi(paramBasis);
  BasisPointDerivativeType dphi(paramBasis);

  paramElem.evalBasis(sRef,phi);
  paramElem.evalBasisDerivative(sRef,dphi);

  SANS_CHECK_CLOSE(-1, dphi.dphiL().deriv(0)[0], tol, tol);
  SANS_CHECK_CLOSE( 1, dphi.dphiL().deriv(0)[1], tol, tol);

  SANS_CHECK_CLOSE(-1, dphi.dphiR().deriv(0)[0], tol, tol);
  SANS_CHECK_CLOSE( 1, dphi.dphiR().deriv(0)[1], tol, tol);

  SANS_CHECK_CLOSE(0.5*(xfldElem.DOF(0)[0] + xfldElem.DOF(1)[0]),
                   params1.right()[0], tol, tol);

  ParamType params2;
  paramElem.evalFromBasis(phi,phi.size(),params2);

  SANS_CHECK_CLOSE(0.5*(v1fldElem.DOF(0)[0] + v1fldElem.DOF(1)[0]),
                   params2.left()[0], tol, tol);

  SANS_CHECK_CLOSE(0.5*(xfldElem.DOF(0)[0] + xfldElem.DOF(1)[0]),
                   params2.right()[0], tol, tol);

  // accessors

  // not const-qualified
  BOOST_CHECK_EQUAL( v1fldElem.DOF(0)[0], paramElem.elemL().DOF(0)[0] );
  BOOST_CHECK_EQUAL( v1fldElem.DOF(1)[0], paramElem.elemL().DOF(1)[0] );

  BOOST_CHECK_EQUAL( xfldElem.DOF(0)[0], paramElem.elemR().DOF(0)[0] );
  BOOST_CHECK_EQUAL( xfldElem.DOF(0)[1], paramElem.elemR().DOF(0)[1] );

  BOOST_CHECK_EQUAL( xfldElem.DOF(1)[0], paramElem.elemR().DOF(1)[0] );
  BOOST_CHECK_EQUAL( xfldElem.DOF(1)[1], paramElem.elemR().DOF(1)[1] );

  BOOST_CHECK_EQUAL( &paramElem.left(), &paramElem.elemL() );
  BOOST_CHECK_EQUAL( &paramElem.right(), &paramElem.elemR() );

  // const-qualified
  const ElementParamV1X paramElemConst(paramElem);

  BOOST_CHECK_EQUAL( paramElemConst.elemL().DOF(0)[0], paramElem.elemL().DOF(0)[0] );
  BOOST_CHECK_EQUAL( paramElemConst.elemL().DOF(1)[0], paramElem.elemL().DOF(1)[0] );

  BOOST_CHECK_EQUAL( paramElemConst.elemR().DOF(0)[0], paramElem.elemR().DOF(0)[0] );
  BOOST_CHECK_EQUAL( paramElemConst.elemR().DOF(0)[1], paramElem.elemR().DOF(0)[1] );

  BOOST_CHECK_EQUAL( paramElemConst.elemR().DOF(1)[0], paramElem.elemR().DOF(1)[0] );
  BOOST_CHECK_EQUAL( paramElemConst.elemR().DOF(1)[1], paramElem.elemR().DOF(1)[1] );

  BOOST_CHECK_EQUAL( &paramElemConst.left(),  &paramElemConst.elemL() );
  BOOST_CHECK_EQUAL( &paramElemConst.right(), &paramElemConst.elemR() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ParamTuple_test )
{
  const Real tol = 1e-15;

  typedef MakeTuple<ParamTuple,int,double,long,bool>::type ParamType;

  // variadic template constructor; set

  ParamType param0(1, 2., 3, true);

  BOOST_CHECK_EQUAL( 1, get<0>(param0) );
  BOOST_CHECK_EQUAL( 2., get<1>(param0) );
  BOOST_CHECK_EQUAL( 3, get<2>(param0) );
  BOOST_CHECK_EQUAL( true, get<3>(param0) );

  ParamType param1;
  param1.set(1, 2., 3, true);

  BOOST_CHECK_EQUAL( 1, get<0>(param1) );
  BOOST_CHECK_EQUAL( 2., get<1>(param1) );
  BOOST_CHECK_EQUAL( 3, get<2>(param1) );
  BOOST_CHECK_EQUAL( true, get<3>(param1) );

  // L/R param constructor; accessors

  typedef DLA::VectorS<1,Real> V1Type;
  typedef DLA::VectorS<2,Real> V2Type;

  typedef MakeTuple<ParamTuple,V1Type,V2Type>::type ParamType2;

  V1Type v1 = {0};
  V2Type v2 = {1., 2.};

  ParamType2 param2(v1, v2);
  const ParamType2 param2const(param2.paramL(), param2.paramR());

  V1Type param2_L = param2.paramL();
  V2Type param2_R = param2.paramR();

  BOOST_CHECK_EQUAL( 0,  param2_L[0] );

  BOOST_CHECK_EQUAL( 1., param2_R[0] );
  BOOST_CHECK_EQUAL( 2., param2_R[1] );

  BOOST_CHECK_EQUAL( param2_L[0], param2const.paramL()[0] );

  BOOST_CHECK_EQUAL( param2_R[0], param2const.paramR()[0] );
  BOOST_CHECK_EQUAL( param2_R[1], param2const.paramR()[1] );

  // overloaded = operator
  param2 = 0; // reset to zero

  SANS_CHECK_CLOSE( 0, param2.paramL()[0], tol, tol );

  SANS_CHECK_CLOSE( 0, param2.paramR()[0], tol, tol );
  SANS_CHECK_CLOSE( 0, param2.paramR()[1], tol, tol );

  {
    typedef MakeTuple<ParamTuple,int,double>::type ParamType;

    typedef promote_Surreal<ParamType, SurrealS<1>>::type SurrealType;

    BOOST_CHECK(( std::is_same<SurrealS<1>, SurrealType>::value ));
  }

  {
    typedef MakeTuple<ParamTuple,int,double,int>::type ParamType;

    typedef promote_Surreal<ParamType, SurrealS<1>>::type SurrealType;

    BOOST_CHECK(( std::is_same<SurrealS<1>, SurrealType>::value ));
  }

  {
    typedef MakeTuple<ParamTuple,SurrealS<1>,double,int>::type ParamType;

    typedef promote_Surreal<ParamType, double>::type SurrealType;

    BOOST_CHECK(( std::is_same<SurrealS<1>, SurrealType>::value ));
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
