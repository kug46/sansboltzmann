// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementAssociativityVolume_Hexahedron_btest
// testing of ElementAssociativityVolume classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "Topology/ElementTopology.h"
#include "Field/Element/ElementAssociativityVolume.h"
#include "BasisFunction/BasisFunctionVolume_Hexahedron.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementAssociativityVolume_Hexahedron_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctorsVolume )
{
  ElementAssociativity<TopoD3,Hex> assoc1( BasisFunctionVolumeBase<Hex>::HierarchicalP1 );

  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( 8, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc1.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc1.nCell() );
  BOOST_CHECK_EQUAL( 8, assoc1.nDOF() );
#if 0
  ElementAssociativity<TopoD3,Hex> assoc2( BasisFunctionVolumeBase<Hex>::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, assoc2.order() );
  BOOST_CHECK_EQUAL( 3, assoc2.nNode() );
  BOOST_CHECK_EQUAL( 3, assoc2.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc2.nCell() );
  BOOST_CHECK_EQUAL( 6, assoc2.nDOF() );

  ElementAssociativity<TopoD3,Hex> assoc2b( BasisFunctionVolumeBase<Hex>::LegendreP2 );

  BOOST_CHECK_EQUAL( 2, assoc2b.order() );
  BOOST_CHECK_EQUAL( 0, assoc2b.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc2b.nEdge() );
  BOOST_CHECK_EQUAL( 6, assoc2b.nCell() );
  BOOST_CHECK_EQUAL( 6, assoc2b.nDOF() );

  ElementAssociativity<TopoD3,Hex> assoc3(assoc2);

  BOOST_CHECK_EQUAL( 2, assoc3.order() );
  BOOST_CHECK_EQUAL( 3, assoc3.nNode() );
  BOOST_CHECK_EQUAL( 3, assoc3.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc3.nCell() );
  BOOST_CHECK_EQUAL( 6, assoc3.nDOF() );

  ElementAssociativity<TopoD3,Hex> assoc3b(assoc2b);

  BOOST_CHECK_EQUAL( 2, assoc3b.order() );
  BOOST_CHECK_EQUAL( 0, assoc3b.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc3b.nEdge() );
  BOOST_CHECK_EQUAL( 6, assoc3b.nCell() );
  BOOST_CHECK_EQUAL( 6, assoc3b.nDOF() );

  // default ctor and operator=
  ElementAssociativity<TopoD3,Hex> assoc4;
  assoc4 = assoc3;

  BOOST_CHECK_EQUAL( 2, assoc4.order() );
  BOOST_CHECK_EQUAL( 3, assoc4.nNode() );
  BOOST_CHECK_EQUAL( 3, assoc4.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc4.nCell() );
  BOOST_CHECK_EQUAL( 6, assoc4.nDOF() );

  ElementAssociativity<TopoD3,Hex> assoc4b;
  assoc4b = assoc3b;

  BOOST_CHECK_EQUAL( 2, assoc4b.order() );
  BOOST_CHECK_EQUAL( 0, assoc4b.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc4b.nEdge() );
  BOOST_CHECK_EQUAL( 6, assoc4b.nCell() );
  BOOST_CHECK_EQUAL( 6, assoc4b.nDOF() );
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctorsVolumeConstructor )
{
  typedef ElementAssociativityConstructor<TopoD3,Hex> Constructor;
  Constructor assoc1( BasisFunctionVolumeBase<Hex>::HierarchicalP1 );

  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( 8, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc1.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc1.nCell() );
  BOOST_CHECK_EQUAL( 8, assoc1.nDOF() );

#if 0
  ElementAssociativityConstructor<TopoD3, Hex> assoc2( BasisFunctionVolumeBase<Hex>::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, assoc2.order() );
  BOOST_CHECK_EQUAL( 3, assoc2.nNode() );
  BOOST_CHECK_EQUAL( 3, assoc2.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc2.nCell() );
  BOOST_CHECK_EQUAL( 6, assoc2.nDOF() );

  ElementAssociativityConstructor<TopoD3, Hex> assoc2b( BasisFunctionVolumeBase<Hex>::LegendreP2 );

  BOOST_CHECK_EQUAL( 2, assoc2b.order() );
  BOOST_CHECK_EQUAL( 0, assoc2b.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc2b.nEdge() );
  BOOST_CHECK_EQUAL( 6, assoc2b.nCell() );
  BOOST_CHECK_EQUAL( 6, assoc2b.nDOF() );

  // default ctor and operator=
  ElementAssociativityConstructor<TopoD3, Hex> assoc4;
  assoc4 = assoc2;

  BOOST_CHECK_EQUAL( 2, assoc4.order() );
  BOOST_CHECK_EQUAL( 3, assoc4.nNode() );
  BOOST_CHECK_EQUAL( 3, assoc4.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc4.nCell() );
  BOOST_CHECK_EQUAL( 6, assoc4.nDOF() );

  ElementAssociativityConstructor<TopoD3, Hex> assoc4b;
  assoc4b = assoc2b;

  BOOST_CHECK_EQUAL( 2, assoc4b.order() );
  BOOST_CHECK_EQUAL( 0, assoc4b.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc4b.nEdge() );
  BOOST_CHECK_EQUAL( 6, assoc4b.nCell() );
  BOOST_CHECK_EQUAL( 6, assoc4b.nDOF() );
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fcnsVolume )
{
  typedef ElementAssociativityConstructor<TopoD3,Hex> Constructor;
  Constructor assoc1ctor( BasisFunctionVolumeBase<Hex>::HierarchicalP1 );

  static const int NNode = Hex::NNode;

  BOOST_CHECK_EQUAL( 1, assoc1ctor.order() );
  BOOST_CHECK_EQUAL( NNode, assoc1ctor.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1ctor.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc1ctor.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc1ctor.nCell() );

  assoc1ctor.setRank( 2 );
  BOOST_CHECK_EQUAL( 2, assoc1ctor.rank() );

  int nodeTrue[NNode] = {3, 4, 5, 6,
                         7, 8, 9, 10};
  int node[NNode];

  for (int i = 0; i < NNode; i++) node[i] = nodeTrue[i];

  assoc1ctor.setNodeGlobalMapping( node, NNode );
  assoc1ctor.getNodeGlobalMapping( node, NNode );

  for (int i = 0; i < NNode; i++)
    BOOST_CHECK_EQUAL( nodeTrue[i], node[i] );


  for (int i = 0; i < NNode; i++)
    node[i] = assoc1ctor.nodeGlobal(i);

  for (int i = 0; i < NNode; i++)
    BOOST_CHECK_EQUAL( nodeTrue[i], node[i] );

  assoc1ctor.getGlobalMapping( node, NNode );

  for (int i = 0; i < NNode; i++)
    BOOST_CHECK_EQUAL( nodeTrue[i], node[i] );



  ElementAssociativity<TopoD3,Hex> assoc1( assoc1ctor );

  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( NNode, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc1.nCell() );

  BOOST_CHECK_EQUAL( 2, assoc1.rank() );

  for (int i = 0; i < NNode; i++) node[i] = -1;
  assoc1.getNodeGlobalMapping( node, NNode );

  for (int i = 0; i < NNode; i++)
    BOOST_CHECK_EQUAL( nodeTrue[i], node[i] );

  for (int i = 0; i < NNode; i++)
    node[i] = assoc1.nodeGlobal(i);

  for (int i = 0; i < NNode; i++)
    BOOST_CHECK_EQUAL( nodeTrue[i], node[i] );

  for (int i = 0; i < NNode; i++) node[i] = -1;
  assoc1.getGlobalMapping( node, NNode );

  for (int i = 0; i < NNode; i++)
    BOOST_CHECK_EQUAL( nodeTrue[i], node[i] );



#if 0
  ElementAssociativityConstructor<TopoD3, Hex> assoc2ctor( BasisFunctionVolumeBase<Hex>::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, assoc2ctor.order() );
  BOOST_CHECK_EQUAL( 3, assoc2ctor.nNode() );
  BOOST_CHECK_EQUAL( 3, assoc2ctor.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc2ctor.nCell() );

  node[0] = nodeTrue[0];
  node[1] = nodeTrue[1];
  node[2] = nodeTrue[2];
  assoc2ctor.setNodeGlobalMapping( node, 3 );
  assoc2ctor.getNodeGlobalMapping( node, 3 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );

  int edgeTrue[3] = {9, 3, 7};
  int edge[3];

  edge[0] = edgeTrue[0];
  edge[1] = edgeTrue[1];
  edge[2] = edgeTrue[2];
  assoc2ctor.setEdgeGlobalMapping( edge, 3 );
  assoc2ctor.getEdgeGlobalMapping( edge, 3 );

  BOOST_CHECK_EQUAL( edgeTrue[0], edge[0] );
  BOOST_CHECK_EQUAL( edgeTrue[1], edge[1] );
  BOOST_CHECK_EQUAL( edgeTrue[2], edge[2] );

  node[0] = assoc2ctor.nodeGlobal(0);
  node[1] = assoc2ctor.nodeGlobal(1);
  node[2] = assoc2ctor.nodeGlobal(2);
  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );

  edge[0] = assoc2ctor.edgeGlobal(0);
  edge[1] = assoc2ctor.edgeGlobal(1);
  edge[2] = assoc2ctor.edgeGlobal(2);
  BOOST_CHECK_EQUAL( edgeTrue[0], edge[0] );
  BOOST_CHECK_EQUAL( edgeTrue[1], edge[1] );
  BOOST_CHECK_EQUAL( edgeTrue[2], edge[2] );

  int mapDOF[6];
  assoc2ctor.getGlobalMapping( mapDOF, 6 );

  BOOST_CHECK_EQUAL( nodeTrue[0], mapDOF[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], mapDOF[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], mapDOF[2] );
  BOOST_CHECK_EQUAL( edgeTrue[0], mapDOF[3] );
  BOOST_CHECK_EQUAL( edgeTrue[1], mapDOF[4] );
  BOOST_CHECK_EQUAL( edgeTrue[2], mapDOF[5] );

  int globalP2True[6] = {4, 8, 2, 2, 6, 1};
  assoc2ctor.setGlobalMapping( globalP2True, 6 );
  assoc2ctor.getGlobalMapping( mapDOF, 6 );

  BOOST_CHECK_EQUAL( globalP2True[0], mapDOF[0] );
  BOOST_CHECK_EQUAL( globalP2True[1], mapDOF[1] );
  BOOST_CHECK_EQUAL( globalP2True[2], mapDOF[2] );
  BOOST_CHECK_EQUAL( globalP2True[3], mapDOF[3] );
  BOOST_CHECK_EQUAL( globalP2True[4], mapDOF[4] );
  BOOST_CHECK_EQUAL( globalP2True[5], mapDOF[5] );

  int globalP2True2[6] = {5, 9, 3, 3, 7, 2};
  assoc2ctor.setGlobalMapping( {5, 9, 3, 3, 7, 2} );
  assoc2ctor.getGlobalMapping( mapDOF, 6 );

  BOOST_CHECK_EQUAL( globalP2True2[0], mapDOF[0] );
  BOOST_CHECK_EQUAL( globalP2True2[1], mapDOF[1] );
  BOOST_CHECK_EQUAL( globalP2True2[2], mapDOF[2] );
  BOOST_CHECK_EQUAL( globalP2True2[3], mapDOF[3] );
  BOOST_CHECK_EQUAL( globalP2True2[4], mapDOF[4] );
  BOOST_CHECK_EQUAL( globalP2True2[5], mapDOF[5] );


  ElementAssociativity<TopoD3,Hex> assoc2( BasisFunctionVolumeBase<Hex>::HierarchicalP2 );
  assoc2 = assoc2ctor;

  BOOST_CHECK_EQUAL( 2, assoc2.order() );
  BOOST_CHECK_EQUAL( 3, assoc2.nNode() );
  BOOST_CHECK_EQUAL( 3, assoc2.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc2.nCell() );

  node[0] = node[1] = node[2] = -1;
  assoc2.getNodeGlobalMapping( node, 3 );

  BOOST_CHECK_EQUAL( globalP2True2[0], node[0] );
  BOOST_CHECK_EQUAL( globalP2True2[1], node[1] );
  BOOST_CHECK_EQUAL( globalP2True2[2], node[2] );

  edge[0] = edge[1] = edge[2] = -1;
  assoc2.getEdgeGlobalMapping( edge, 3 );

  BOOST_CHECK_EQUAL( globalP2True2[3], edge[0] );
  BOOST_CHECK_EQUAL( globalP2True2[4], edge[1] );
  BOOST_CHECK_EQUAL( globalP2True2[5], edge[2] );

  node[0] = assoc2.nodeGlobal(0);
  node[1] = assoc2.nodeGlobal(1);
  node[2] = assoc2.nodeGlobal(2);
  BOOST_CHECK_EQUAL( globalP2True2[0], node[0] );
  BOOST_CHECK_EQUAL( globalP2True2[1], node[1] );
  BOOST_CHECK_EQUAL( globalP2True2[2], node[2] );

  edge[0] = assoc2.edgeGlobal(0);
  edge[1] = assoc2.edgeGlobal(1);
  edge[2] = assoc2.edgeGlobal(2);
  BOOST_CHECK_EQUAL( globalP2True2[3], edge[0] );
  BOOST_CHECK_EQUAL( globalP2True2[4], edge[1] );
  BOOST_CHECK_EQUAL( globalP2True2[5], edge[2] );

  for ( int i=0; i<6; i++) mapDOF[i] = -1;
  assoc2.getGlobalMapping( mapDOF, 6 );

  BOOST_CHECK_EQUAL( globalP2True2[0], mapDOF[0] );
  BOOST_CHECK_EQUAL( globalP2True2[1], mapDOF[1] );
  BOOST_CHECK_EQUAL( globalP2True2[2], mapDOF[2] );
  BOOST_CHECK_EQUAL( globalP2True2[3], mapDOF[3] );
  BOOST_CHECK_EQUAL( globalP2True2[4], mapDOF[4] );
  BOOST_CHECK_EQUAL( globalP2True2[5], mapDOF[5] );



  ElementAssociativityConstructor<TopoD3, Hex> assoc3( BasisFunctionVolumeBase<Hex>::HierarchicalP2 );

  assoc3.setNodeGlobalMapping( {3, 4, 5} );
  assoc3.getNodeGlobalMapping( node, 3 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );

  assoc3.setEdgeGlobalMapping( {9, 3, 7} );
  assoc3.getEdgeGlobalMapping( edge, 3 );

  BOOST_CHECK_EQUAL( edgeTrue[0], edge[0] );
  BOOST_CHECK_EQUAL( edgeTrue[1], edge[1] );
  BOOST_CHECK_EQUAL( edgeTrue[2], edge[2] );

  node[0] = assoc3.nodeGlobal(0);
  node[1] = assoc3.nodeGlobal(1);
  node[2] = assoc3.nodeGlobal(2);
  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );

  edge[0] = assoc3.edgeGlobal(0);
  edge[1] = assoc3.edgeGlobal(1);
  edge[2] = assoc3.edgeGlobal(2);
  BOOST_CHECK_EQUAL( edgeTrue[0], edge[0] );
  BOOST_CHECK_EQUAL( edgeTrue[1], edge[1] );
  BOOST_CHECK_EQUAL( edgeTrue[2], edge[2] );


  // cubic element
  ElementAssociativityConstructor<TopoD3, Hex> assoc4ctor( BasisFunctionVolumeBase<Hex>::HierarchicalP3 );

  BOOST_CHECK_EQUAL( 3, assoc4ctor.order() );
  BOOST_CHECK_EQUAL( 3, assoc4ctor.nNode() );
  BOOST_CHECK_EQUAL( 6, assoc4ctor.nEdge() );
  BOOST_CHECK_EQUAL( 1, assoc4ctor.nCell() );

  assoc4ctor.setNodeGlobalMapping( {3, 4, 5} );
  assoc4ctor.getNodeGlobalMapping( node, 3 );

  BOOST_CHECK_EQUAL( 3, node[0] );
  BOOST_CHECK_EQUAL( 4, node[1] );
  BOOST_CHECK_EQUAL( 5, node[2] );

  int edgeCub[6];
  int cellCub[1];

  assoc4ctor.setEdgeGlobalMapping( {9, 3, 7, 2, 5, 1} );
  assoc4ctor.getEdgeGlobalMapping( edgeCub, 6 );

  BOOST_CHECK_EQUAL( 9, edgeCub[0] );
  BOOST_CHECK_EQUAL( 3, edgeCub[1] );
  BOOST_CHECK_EQUAL( 7, edgeCub[2] );
  BOOST_CHECK_EQUAL( 2, edgeCub[3] );
  BOOST_CHECK_EQUAL( 5, edgeCub[4] );
  BOOST_CHECK_EQUAL( 1, edgeCub[5] );

  assoc4ctor.setCellGlobalMapping( {4} );
  assoc4ctor.getCellGlobalMapping( cellCub, 1 );

  BOOST_CHECK_EQUAL( 4, cellCub[0] );

  node[0] = assoc4ctor.nodeGlobal(0);
  node[1] = assoc4ctor.nodeGlobal(1);
  node[2] = assoc4ctor.nodeGlobal(2);
  BOOST_CHECK_EQUAL( 3, node[0] );
  BOOST_CHECK_EQUAL( 4, node[1] );
  BOOST_CHECK_EQUAL( 5, node[2] );

  edgeCub[0] = assoc4ctor.edgeGlobal(0);
  edgeCub[1] = assoc4ctor.edgeGlobal(1);
  edgeCub[2] = assoc4ctor.edgeGlobal(2);
  edgeCub[3] = assoc4ctor.edgeGlobal(3);
  edgeCub[4] = assoc4ctor.edgeGlobal(4);
  edgeCub[5] = assoc4ctor.edgeGlobal(5);
  BOOST_CHECK_EQUAL( 9, edgeCub[0] );
  BOOST_CHECK_EQUAL( 3, edgeCub[1] );
  BOOST_CHECK_EQUAL( 7, edgeCub[2] );
  BOOST_CHECK_EQUAL( 2, edgeCub[3] );
  BOOST_CHECK_EQUAL( 5, edgeCub[4] );
  BOOST_CHECK_EQUAL( 1, edgeCub[5] );

  cellCub[0] = assoc4ctor.cellGlobal(0);
  BOOST_CHECK_EQUAL( 4, cellCub[0] );

  int mapDOF3[10] = {-1};
  int globalP3True[10] = {4, 8, 2, 3, 6, 1, 7, 5, 9, 4};
  assoc4ctor.setGlobalMapping( globalP3True, 10 );
  assoc4ctor.getGlobalMapping( mapDOF3, 10 );

  BOOST_CHECK_EQUAL( globalP3True[0], mapDOF3[0] );
  BOOST_CHECK_EQUAL( globalP3True[1], mapDOF3[1] );
  BOOST_CHECK_EQUAL( globalP3True[2], mapDOF3[2] );
  BOOST_CHECK_EQUAL( globalP3True[3], mapDOF3[3] );
  BOOST_CHECK_EQUAL( globalP3True[4], mapDOF3[4] );
  BOOST_CHECK_EQUAL( globalP3True[5], mapDOF3[5] );
  BOOST_CHECK_EQUAL( globalP3True[6], mapDOF3[6] );
  BOOST_CHECK_EQUAL( globalP3True[7], mapDOF3[7] );
  BOOST_CHECK_EQUAL( globalP3True[8], mapDOF3[8] );
  BOOST_CHECK_EQUAL( globalP3True[9], mapDOF3[9] );

  int globalP3True2[10] = {5, 9, 3, 1, 7, 2, 4, 8, 6, 3};
  assoc4ctor.setGlobalMapping( {5, 9, 3, 1, 7, 2, 4, 8, 6, 3} );
  assoc4ctor.getGlobalMapping( mapDOF3, 10 );

  BOOST_CHECK_EQUAL( globalP3True2[0], mapDOF3[0] );
  BOOST_CHECK_EQUAL( globalP3True2[1], mapDOF3[1] );
  BOOST_CHECK_EQUAL( globalP3True2[2], mapDOF3[2] );
  BOOST_CHECK_EQUAL( globalP3True2[3], mapDOF3[3] );
  BOOST_CHECK_EQUAL( globalP3True2[4], mapDOF3[4] );
  BOOST_CHECK_EQUAL( globalP3True2[5], mapDOF3[5] );
  BOOST_CHECK_EQUAL( globalP3True2[6], mapDOF3[6] );
  BOOST_CHECK_EQUAL( globalP3True2[7], mapDOF3[7] );
  BOOST_CHECK_EQUAL( globalP3True2[8], mapDOF3[8] );
  BOOST_CHECK_EQUAL( globalP3True2[9], mapDOF3[9] );


  ElementAssociativityConstructor<TopoD3, Hex> assoc4( assoc4ctor );

  BOOST_CHECK_EQUAL( 3, assoc4.order() );
  BOOST_CHECK_EQUAL( 3, assoc4.nNode() );
  BOOST_CHECK_EQUAL( 6, assoc4.nEdge() );
  BOOST_CHECK_EQUAL( 1, assoc4.nCell() );

  node[0] = node[1] = node[2] = -1;
  assoc4.getNodeGlobalMapping( node, 3 );

  BOOST_CHECK_EQUAL( globalP3True2[0], node[0] );
  BOOST_CHECK_EQUAL( globalP3True2[1], node[1] );
  BOOST_CHECK_EQUAL( globalP3True2[2], node[2] );

  for ( int i=0; i<6; i++) edgeCub[i] = -1;
  assoc4.getEdgeGlobalMapping( edgeCub, 6 );

  BOOST_CHECK_EQUAL( globalP3True2[3], edgeCub[0] );
  BOOST_CHECK_EQUAL( globalP3True2[4], edgeCub[1] );
  BOOST_CHECK_EQUAL( globalP3True2[5], edgeCub[2] );
  BOOST_CHECK_EQUAL( globalP3True2[6], edgeCub[3] );
  BOOST_CHECK_EQUAL( globalP3True2[7], edgeCub[4] );
  BOOST_CHECK_EQUAL( globalP3True2[8], edgeCub[5] );

  cellCub[0] = -1;
  assoc4.getCellGlobalMapping( cellCub, 1 );

  BOOST_CHECK_EQUAL( globalP3True2[9], cellCub[0] );

  node[0] = assoc4.nodeGlobal(0);
  node[1] = assoc4.nodeGlobal(1);
  node[2] = assoc4.nodeGlobal(2);
  BOOST_CHECK_EQUAL( globalP3True2[0], node[0] );
  BOOST_CHECK_EQUAL( globalP3True2[1], node[1] );
  BOOST_CHECK_EQUAL( globalP3True2[2], node[2] );

  edgeCub[0] = assoc4.edgeGlobal(0);
  edgeCub[1] = assoc4.edgeGlobal(1);
  edgeCub[2] = assoc4.edgeGlobal(2);
  edgeCub[3] = assoc4.edgeGlobal(3);
  edgeCub[4] = assoc4.edgeGlobal(4);
  edgeCub[5] = assoc4.edgeGlobal(5);
  BOOST_CHECK_EQUAL( globalP3True2[3], edgeCub[0] );
  BOOST_CHECK_EQUAL( globalP3True2[4], edgeCub[1] );
  BOOST_CHECK_EQUAL( globalP3True2[5], edgeCub[2] );
  BOOST_CHECK_EQUAL( globalP3True2[6], edgeCub[3] );
  BOOST_CHECK_EQUAL( globalP3True2[7], edgeCub[4] );
  BOOST_CHECK_EQUAL( globalP3True2[8], edgeCub[5] );

  cellCub[0] = assoc4.cellGlobal(0);
  BOOST_CHECK_EQUAL( globalP3True2[9], cellCub[0] );

  for ( int i=0; i<10; i++) mapDOF3[i] = -1;
  assoc4.getGlobalMapping( mapDOF3, 10 );

  BOOST_CHECK_EQUAL( globalP3True2[0], mapDOF3[0] );
  BOOST_CHECK_EQUAL( globalP3True2[1], mapDOF3[1] );
  BOOST_CHECK_EQUAL( globalP3True2[2], mapDOF3[2] );
  BOOST_CHECK_EQUAL( globalP3True2[3], mapDOF3[3] );
  BOOST_CHECK_EQUAL( globalP3True2[4], mapDOF3[4] );
  BOOST_CHECK_EQUAL( globalP3True2[5], mapDOF3[5] );
  BOOST_CHECK_EQUAL( globalP3True2[6], mapDOF3[6] );
  BOOST_CHECK_EQUAL( globalP3True2[7], mapDOF3[7] );
  BOOST_CHECK_EQUAL( globalP3True2[8], mapDOF3[8] );
  BOOST_CHECK_EQUAL( globalP3True2[9], mapDOF3[9] );
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( facesignVolume )
{
  typedef std::array<int,6> Int6;

  typedef ElementAssociativityConstructor<TopoD3,Hex> Constructor;
  Constructor assocCtor( BasisFunctionVolumeBase<Hex>::HierarchicalP1 );

  assocCtor.setRank( 2 );
  assocCtor.setNodeGlobalMapping( {3, 4, 5, 6,
                                   7, 8, 9, 10} );

  Int6 faceSign = assocCtor.faceSign();
  BOOST_CHECK_EQUAL( +1, faceSign[0] );
  BOOST_CHECK_EQUAL( +1, faceSign[1] );
  BOOST_CHECK_EQUAL( +1, faceSign[2] );
  BOOST_CHECK_EQUAL( +1, faceSign[3] );
  BOOST_CHECK_EQUAL( +1, faceSign[4] );
  BOOST_CHECK_EQUAL( +1, faceSign[5] );

  faceSign[1] = -1;
  assocCtor.faceSign() = faceSign;

  Int6 faceSign2 = assocCtor.faceSign();
  BOOST_CHECK_EQUAL( +1, faceSign2[0] );
  BOOST_CHECK_EQUAL( -1, faceSign2[1] );
  BOOST_CHECK_EQUAL( +1, faceSign2[2] );
  BOOST_CHECK_EQUAL( +1, faceSign2[3] );
  BOOST_CHECK_EQUAL( +1, faceSign2[4] );
  BOOST_CHECK_EQUAL( +1, faceSign2[5] );

  assocCtor.setFaceSign( -1, 2 );
  Int6 faceSign3 = assocCtor.faceSign();
  BOOST_CHECK_EQUAL( +1, faceSign3[0] );
  BOOST_CHECK_EQUAL( -1, faceSign3[1] );
  BOOST_CHECK_EQUAL( -1, faceSign3[2] );
  BOOST_CHECK_EQUAL( +1, faceSign3[3] );
  BOOST_CHECK_EQUAL( +1, faceSign3[4] );
  BOOST_CHECK_EQUAL( +1, faceSign3[5] );

  ElementAssociativity<TopoD3,Hex> assoc( assocCtor );

  faceSign = assoc.faceSign();
  BOOST_CHECK_EQUAL( +1, faceSign[0] );
  BOOST_CHECK_EQUAL( -1, faceSign[1] );
  BOOST_CHECK_EQUAL( -1, faceSign[2] );
  BOOST_CHECK_EQUAL( +1, faceSign[3] );
  BOOST_CHECK_EQUAL( +1, faceSign[4] );
  BOOST_CHECK_EQUAL( +1, faceSign[5] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/ElementAssociativityVolume_Hexahedron_pattern.txt", true );

  //Volume dumps
  typedef ElementAssociativityConstructor<TopoD3,Hex> Constructor;
  Constructor assoc1ctor( BasisFunctionVolumeBase<Hex>::HierarchicalP1 );

  assoc1ctor.setRank( 2 );
  assoc1ctor.setNodeGlobalMapping( {3, 4, 5, 6,
                                    7, 8, 9, 10} );

  ElementAssociativity<TopoD3,Hex> assoc1( assoc1ctor );

  assoc1ctor.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  assoc1.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
#if 0
  ElementAssociativityConstructor<TopoD3, Hex> assoc2ctor( BasisFunctionVolumeBase<Hex>::HierarchicalP3 );

  assoc2ctor.setNodeGlobalMapping( {3, 4, 5} );
  assoc2ctor.setEdgeGlobalMapping( {9, 3, 7, 12, 14, 15} );
  assoc2ctor.setCellGlobalMapping( {1} );

  ElementAssociativity<TopoD3,Hex> assoc2( assoc2ctor );

  assoc2ctor.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  assoc2.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
