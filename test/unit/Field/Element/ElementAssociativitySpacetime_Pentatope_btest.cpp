// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementAssociativityVolume_Tetrahedron_btest
// testing of ElementAssociativityVolume classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "Topology/ElementTopology.h"
#include "Field/Element/ElementAssociativitySpacetime.h"
#include "BasisFunction/BasisFunctionSpacetime_Pentatope.h"

using namespace SANS;

#define CHECK_EQUAL_5(X,Y) \
  BOOST_CHECK_EQUAL( X[0] , Y[0] ); \
  BOOST_CHECK_EQUAL( X[1] , Y[1] ); \
  BOOST_CHECK_EQUAL( X[2] , Y[2] ); \
  BOOST_CHECK_EQUAL( X[3] , Y[3] ); \
  BOOST_CHECK_EQUAL( X[4] , Y[4] ); \

#define CHECK_EQUAL_10(X,Y) \
  BOOST_CHECK_EQUAL( X[0] , Y[0] ); \
  BOOST_CHECK_EQUAL( X[1] , Y[1] ); \
  BOOST_CHECK_EQUAL( X[2] , Y[2] ); \
  BOOST_CHECK_EQUAL( X[3] , Y[3] ); \
  BOOST_CHECK_EQUAL( X[4] , Y[4] ); \
  BOOST_CHECK_EQUAL( X[5] , Y[5] ); \
  BOOST_CHECK_EQUAL( X[6] , Y[6] ); \
  BOOST_CHECK_EQUAL( X[7] , Y[7] ); \
  BOOST_CHECK_EQUAL( X[8] , Y[8] ); \
  BOOST_CHECK_EQUAL( X[9] , Y[9] );

#define CHECK_EQUAL_20(X,Y) \
  BOOST_CHECK_EQUAL( X[0] , Y[0] ); \
  BOOST_CHECK_EQUAL( X[1] , Y[1] ); \
  BOOST_CHECK_EQUAL( X[2] , Y[2] ); \
  BOOST_CHECK_EQUAL( X[3] , Y[3] ); \
  BOOST_CHECK_EQUAL( X[4] , Y[4] ); \
  BOOST_CHECK_EQUAL( X[5] , Y[5] ); \
  BOOST_CHECK_EQUAL( X[6] , Y[6] ); \
  BOOST_CHECK_EQUAL( X[7] , Y[7] ); \
  BOOST_CHECK_EQUAL( X[8] , Y[8] ); \
  BOOST_CHECK_EQUAL( X[9] , Y[9] ); \
  BOOST_CHECK_EQUAL( X[10] , Y[10] ); \
  BOOST_CHECK_EQUAL( X[11] , Y[11] ); \
  BOOST_CHECK_EQUAL( X[12] , Y[12] ); \
  BOOST_CHECK_EQUAL( X[13] , Y[13] ); \
  BOOST_CHECK_EQUAL( X[14] , Y[14] ); \
  BOOST_CHECK_EQUAL( X[15] , Y[15] ); \
  BOOST_CHECK_EQUAL( X[16] , Y[16] ); \
  BOOST_CHECK_EQUAL( X[17] , Y[17] ); \
  BOOST_CHECK_EQUAL( X[18] , Y[18] ); \
  BOOST_CHECK_EQUAL( X[19] , Y[19] );

#define CHECK_EQUAL_30(X,Y) \
  BOOST_CHECK_EQUAL( X[0] , Y[0] ); \
  BOOST_CHECK_EQUAL( X[1] , Y[1] ); \
  BOOST_CHECK_EQUAL( X[2] , Y[2] ); \
  BOOST_CHECK_EQUAL( X[3] , Y[3] ); \
  BOOST_CHECK_EQUAL( X[4] , Y[4] ); \
  BOOST_CHECK_EQUAL( X[5] , Y[5] ); \
  BOOST_CHECK_EQUAL( X[6] , Y[6] ); \
  BOOST_CHECK_EQUAL( X[7] , Y[7] ); \
  BOOST_CHECK_EQUAL( X[8] , Y[8] ); \
  BOOST_CHECK_EQUAL( X[9] , Y[9] ); \
  BOOST_CHECK_EQUAL( X[10] , Y[10] ); \
  BOOST_CHECK_EQUAL( X[11] , Y[11] ); \
  BOOST_CHECK_EQUAL( X[12] , Y[12] ); \
  BOOST_CHECK_EQUAL( X[13] , Y[13] ); \
  BOOST_CHECK_EQUAL( X[14] , Y[14] ); \
  BOOST_CHECK_EQUAL( X[15] , Y[15] ); \
  BOOST_CHECK_EQUAL( X[16] , Y[16] ); \
  BOOST_CHECK_EQUAL( X[17] , Y[17] ); \
  BOOST_CHECK_EQUAL( X[18] , Y[18] ); \
  BOOST_CHECK_EQUAL( X[19] , Y[19] ); \
  BOOST_CHECK_EQUAL( X[20] , Y[20] ); \
  BOOST_CHECK_EQUAL( X[21] , Y[21] ); \
  BOOST_CHECK_EQUAL( X[22] , Y[22] ); \
  BOOST_CHECK_EQUAL( X[23] , Y[23] ); \
  BOOST_CHECK_EQUAL( X[24] , Y[24] ); \
  BOOST_CHECK_EQUAL( X[25] , Y[25] ); \
  BOOST_CHECK_EQUAL( X[26] , Y[26] ); \
  BOOST_CHECK_EQUAL( X[27] , Y[27] ); \
  BOOST_CHECK_EQUAL( X[28] , Y[28] ); \
  BOOST_CHECK_EQUAL( X[29] , Y[29] );

#define CHECK_EQUAL_35(X,Y) \
  BOOST_CHECK_EQUAL( X[0] , Y[0] ); \
  BOOST_CHECK_EQUAL( X[1] , Y[1] ); \
  BOOST_CHECK_EQUAL( X[2] , Y[2] ); \
  BOOST_CHECK_EQUAL( X[3] , Y[3] ); \
  BOOST_CHECK_EQUAL( X[4] , Y[4] ); \
  BOOST_CHECK_EQUAL( X[5] , Y[5] ); \
  BOOST_CHECK_EQUAL( X[6] , Y[6] ); \
  BOOST_CHECK_EQUAL( X[7] , Y[7] ); \
  BOOST_CHECK_EQUAL( X[8] , Y[8] ); \
  BOOST_CHECK_EQUAL( X[9] , Y[9] ); \
  BOOST_CHECK_EQUAL( X[10] , Y[10] ); \
  BOOST_CHECK_EQUAL( X[11] , Y[11] ); \
  BOOST_CHECK_EQUAL( X[12] , Y[12] ); \
  BOOST_CHECK_EQUAL( X[13] , Y[13] ); \
  BOOST_CHECK_EQUAL( X[14] , Y[14] ); \
  BOOST_CHECK_EQUAL( X[15] , Y[15] ); \
  BOOST_CHECK_EQUAL( X[16] , Y[16] ); \
  BOOST_CHECK_EQUAL( X[17] , Y[17] ); \
  BOOST_CHECK_EQUAL( X[18] , Y[18] ); \
  BOOST_CHECK_EQUAL( X[19] , Y[19] ); \
  BOOST_CHECK_EQUAL( X[20] , Y[20] ); \
  BOOST_CHECK_EQUAL( X[21] , Y[21] ); \
  BOOST_CHECK_EQUAL( X[22] , Y[22] ); \
  BOOST_CHECK_EQUAL( X[23] , Y[23] ); \
  BOOST_CHECK_EQUAL( X[24] , Y[24] ); \
  BOOST_CHECK_EQUAL( X[25] , Y[25] ); \
  BOOST_CHECK_EQUAL( X[26] , Y[26] ); \
  BOOST_CHECK_EQUAL( X[27] , Y[27] ); \
  BOOST_CHECK_EQUAL( X[28] , Y[28] ); \
  BOOST_CHECK_EQUAL( X[29] , Y[29] ); \
  BOOST_CHECK_EQUAL( X[30] , Y[30] ); \
  BOOST_CHECK_EQUAL( X[31] , Y[31] ); \
  BOOST_CHECK_EQUAL( X[32] , Y[32] ); \
  BOOST_CHECK_EQUAL( X[33] , Y[33] ); \
  BOOST_CHECK_EQUAL( X[34] , Y[34] );

//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementAssociativitySpacetime_Pentatope_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctorsSpacetime )
{
  ElementAssociativity<TopoD4,Pentatope> assoc1( BasisFunctionSpacetimeBase<Pentatope>::LagrangeP1 );
  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( 5, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc1.nArea() );
  BOOST_CHECK_EQUAL( 0, assoc1.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc1.nCell() );
  BOOST_CHECK_EQUAL( 5, assoc1.nDOF() );

  ElementAssociativity<TopoD4,Pentatope> assoc2( BasisFunctionSpacetimeBase<Pentatope>::LagrangeP2 );
  BOOST_CHECK_EQUAL( 2, assoc2.order() );
  BOOST_CHECK_EQUAL( 5, assoc2.nNode() );
  BOOST_CHECK_EQUAL( 10, assoc2.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc2.nArea() );
  BOOST_CHECK_EQUAL( 0, assoc2.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc2.nCell() );
  BOOST_CHECK_EQUAL( 15, assoc2.nDOF() );

  ElementAssociativity<TopoD4,Pentatope> assoc3( BasisFunctionSpacetimeBase<Pentatope>::LagrangeP3 );
  BOOST_CHECK_EQUAL( 3, assoc3.order() );
  BOOST_CHECK_EQUAL( 5, assoc3.nNode() );
  BOOST_CHECK_EQUAL( 20, assoc3.nEdge() );
  BOOST_CHECK_EQUAL( 10, assoc3.nArea() );
  BOOST_CHECK_EQUAL( 0, assoc3.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc3.nCell() );
  BOOST_CHECK_EQUAL( 35, assoc3.nDOF() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctorsSpacetimeConstructor )
{
  typedef ElementAssociativityConstructor<TopoD4,Pentatope> Constructor;

  Constructor assoc1( BasisFunctionSpacetimeBase<Pentatope>::LagrangeP1 );
  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( 5, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc1.nArea() );
  BOOST_CHECK_EQUAL( 0, assoc1.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc1.nCell() );
  BOOST_CHECK_EQUAL( 5, assoc1.nDOF() );

  Constructor assoc2( BasisFunctionSpacetimeBase<Pentatope>::LagrangeP2 );
  BOOST_CHECK_EQUAL( 2, assoc2.order() );
  BOOST_CHECK_EQUAL( 5, assoc2.nNode() );
  BOOST_CHECK_EQUAL( 10, assoc2.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc2.nArea() );
  BOOST_CHECK_EQUAL( 0, assoc2.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc2.nCell() );
  BOOST_CHECK_EQUAL( 15, assoc2.nDOF() );

  Constructor assoc3( BasisFunctionSpacetimeBase<Pentatope>::LagrangeP3 );
  BOOST_CHECK_EQUAL( 3, assoc3.order() );
  BOOST_CHECK_EQUAL( 5, assoc3.nNode() );
  BOOST_CHECK_EQUAL( 20, assoc3.nEdge() );
  BOOST_CHECK_EQUAL( 10, assoc3.nArea() );
  BOOST_CHECK_EQUAL( 0, assoc3.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc3.nCell() );
  BOOST_CHECK_EQUAL( 35, assoc3.nDOF() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fcnsSpacetime_linear_and_quadratic )
{
  //Linear element

  typedef ElementAssociativityConstructor<TopoD4,Pentatope> Constructor;
  Constructor assoc1ctor( BasisFunctionSpacetimeBase<Pentatope>::LagrangeP1 );

  BOOST_CHECK_EQUAL( 1, assoc1ctor.order() );
  BOOST_CHECK_EQUAL( 5, assoc1ctor.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1ctor.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc1ctor.nArea() );
  BOOST_CHECK_EQUAL( 0, assoc1ctor.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc1ctor.nCell() );

  assoc1ctor.setRank( 2 );
  BOOST_CHECK_EQUAL( 2, assoc1ctor.rank() );

  int nodeTrue[5] = {3, 4, 5, 6, 7};
  int node[5];

  node[0] = nodeTrue[0];
  node[1] = nodeTrue[1];
  node[2] = nodeTrue[2];
  node[3] = nodeTrue[3];
  node[4] = nodeTrue[4];
  assoc1ctor.setNodeGlobalMapping( node, 5 );
  assoc1ctor.getNodeGlobalMapping( node, 5 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );
  BOOST_CHECK_EQUAL( nodeTrue[4], node[4] );

  node[0] = assoc1ctor.nodeGlobal(0);
  node[1] = assoc1ctor.nodeGlobal(1);
  node[2] = assoc1ctor.nodeGlobal(2);
  node[3] = assoc1ctor.nodeGlobal(3);
  node[4] = assoc1ctor.nodeGlobal(4);
  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );
  BOOST_CHECK_EQUAL( nodeTrue[4], node[4] );

  assoc1ctor.getGlobalMapping( node, 5 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );
  BOOST_CHECK_EQUAL( nodeTrue[4], node[4] );

  ElementAssociativity<TopoD4,Pentatope> assoc1( assoc1ctor );

  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( 5, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc1.nArea() );
  BOOST_CHECK_EQUAL( 0, assoc1.nCell() );

  BOOST_CHECK_EQUAL( 2, assoc1.rank() );

  node[0] = node[1] = node[2] = node[3] = node[4] = -1;
  assoc1.getNodeGlobalMapping( node, 5 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );
  BOOST_CHECK_EQUAL( nodeTrue[4], node[4] );

  node[0] = assoc1.nodeGlobal(0);
  node[1] = assoc1.nodeGlobal(1);
  node[2] = assoc1.nodeGlobal(2);
  node[3] = assoc1.nodeGlobal(3);
  node[4] = assoc1.nodeGlobal(4);
  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );
  BOOST_CHECK_EQUAL( nodeTrue[4], node[4] );

  node[0] = node[1] = node[2] = node[3] = node[4] = -1;
  assoc1.getGlobalMapping( node, 5 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );
  BOOST_CHECK_EQUAL( nodeTrue[4], node[4] );

  // quadratic element

  Constructor assoc2ctor( BasisFunctionSpacetimeBase<Pentatope>::LagrangeP2 );

  BOOST_CHECK_EQUAL( 2, assoc2ctor.order() );
  BOOST_CHECK_EQUAL( 5, assoc2ctor.nNode() );
  BOOST_CHECK_EQUAL( 10, assoc2ctor.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc2ctor.nArea() );
  BOOST_CHECK_EQUAL( 0, assoc2ctor.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc2ctor.nCell() );

  assoc2ctor.setRank( 2 );
  BOOST_CHECK_EQUAL( 2, assoc2ctor.rank() );

  node[0] = nodeTrue[0];
  node[1] = nodeTrue[1];
  node[2] = nodeTrue[2];
  node[3] = nodeTrue[3];
  node[4] = nodeTrue[4];
  assoc2ctor.setNodeGlobalMapping( node, 5 );
  assoc2ctor.getNodeGlobalMapping( node, 5 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );
  BOOST_CHECK_EQUAL( nodeTrue[4], node[4] );

  int edgeTrue_P2[10] = {10,11,12,13,14,15,16,17,18,19};
  int edge_P2[10];

  for (int i=0;i<10;i++)
    edge_P2[i] = edgeTrue_P2[i];

  // ensure the set(Edge/Area/Face)GlobalMapping throws an exception until it is implemented + tested
  BOOST_CHECK_THROW( assoc2ctor.setEdgeGlobalMapping( {0} , CanonicalTraceToCell(0,1) , 0 ) , DeveloperException );
  BOOST_CHECK_THROW( assoc2ctor.setAreaGlobalMapping( {0} , CanonicalTraceToCell(0,1) , 0 ) , DeveloperException );
  //BOOST_CHECK_THROW( assoc2ctor.setFaceGlobalMapping( 0 , 1 , CanonicalTraceToCell(0,1) ) , DeveloperException );

  //setting full edgeDOF vector at once
  assoc2ctor.setEdgeGlobalMapping( edge_P2, 10 );
  assoc2ctor.getEdgeGlobalMapping( edge_P2, 10 );

  BOOST_CHECK_EQUAL( edgeTrue_P2[0], edge_P2[0] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[1], edge_P2[1] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[2], edge_P2[2] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[3], edge_P2[3] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[4], edge_P2[4] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[5], edge_P2[5] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[6], edge_P2[6] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[7], edge_P2[7] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[8], edge_P2[8] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[9], edge_P2[9] );

  node[0] = assoc2ctor.nodeGlobal(0);
  node[1] = assoc2ctor.nodeGlobal(1);
  node[2] = assoc2ctor.nodeGlobal(2);
  node[3] = assoc2ctor.nodeGlobal(3);
  node[4] = assoc2ctor.nodeGlobal(4);

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );
  BOOST_CHECK_EQUAL( nodeTrue[4], node[4] );

  edge_P2[0] = assoc2ctor.edgeGlobal(0);
  edge_P2[1] = assoc2ctor.edgeGlobal(1);
  edge_P2[2] = assoc2ctor.edgeGlobal(2);
  edge_P2[3] = assoc2ctor.edgeGlobal(3);
  edge_P2[4] = assoc2ctor.edgeGlobal(4);
  edge_P2[5] = assoc2ctor.edgeGlobal(5);
  edge_P2[6] = assoc2ctor.edgeGlobal(6);
  edge_P2[7] = assoc2ctor.edgeGlobal(7);
  edge_P2[8] = assoc2ctor.edgeGlobal(8);
  edge_P2[9] = assoc2ctor.edgeGlobal(9);

  BOOST_CHECK_EQUAL( edgeTrue_P2[0], edge_P2[0] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[1], edge_P2[1] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[2], edge_P2[2] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[3], edge_P2[3] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[4], edge_P2[4] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[5], edge_P2[5] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[6], edge_P2[6] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[7], edge_P2[7] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[8], edge_P2[8] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[9], edge_P2[9] );

  int mapDOF[15];
  assoc2ctor.getGlobalMapping( mapDOF, 15 );

  BOOST_CHECK_EQUAL( nodeTrue[0], mapDOF[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], mapDOF[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], mapDOF[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], mapDOF[3] );
  BOOST_CHECK_EQUAL( nodeTrue[4], mapDOF[4] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[0], mapDOF[5] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[1], mapDOF[6] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[2], mapDOF[7] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[3], mapDOF[8] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[4], mapDOF[9] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[5], mapDOF[10] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[6], mapDOF[11] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[7], mapDOF[12] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[8], mapDOF[13] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[9], mapDOF[14] );


  ElementAssociativity<TopoD4,Pentatope> assoc2( assoc2ctor );

  BOOST_CHECK_EQUAL( 2, assoc2.order() );
  BOOST_CHECK_EQUAL( 5, assoc2.nNode() );
  BOOST_CHECK_EQUAL( 10, assoc2.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc2.nArea() );
  BOOST_CHECK_EQUAL( 0, assoc2.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc2.nCell() );

  BOOST_CHECK_EQUAL( 2, assoc2.rank() );

  node[0] = node[1] = node[2] = node[3] = -1;
  assoc2.getNodeGlobalMapping( node, 5 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );
  BOOST_CHECK_EQUAL( nodeTrue[4], node[4] );
//herr
  node[0] = assoc2.nodeGlobal(0);
  node[1] = assoc2.nodeGlobal(1);
  node[2] = assoc2.nodeGlobal(2);
  node[3] = assoc2.nodeGlobal(3);
  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );

  edge_P2[0] = edge_P2[1] = edge_P2[2] = edge_P2[3] = edge_P2[4] = edge_P2[5] = -1;
  edge_P2[6] = edge_P2[7] = edge_P2[8] = edge_P2[9] = -1;
  assoc2.getEdgeGlobalMapping( edge_P2, 10 );

  BOOST_CHECK_EQUAL( edgeTrue_P2[0], edge_P2[0] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[1], edge_P2[1] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[2], edge_P2[2] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[3], edge_P2[3] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[4], edge_P2[4] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[5], edge_P2[5] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[6], edge_P2[6] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[7], edge_P2[7] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[8], edge_P2[8] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[9], edge_P2[9] );

  edge_P2[0] = assoc2.edgeGlobal(0);
  edge_P2[1] = assoc2.edgeGlobal(1);
  edge_P2[2] = assoc2.edgeGlobal(2);
  edge_P2[3] = assoc2.edgeGlobal(3);
  edge_P2[4] = assoc2.edgeGlobal(4);
  edge_P2[5] = assoc2.edgeGlobal(5);
  edge_P2[6] = assoc2.edgeGlobal(6);
  edge_P2[7] = assoc2.edgeGlobal(7);
  edge_P2[8] = assoc2.edgeGlobal(8);
  edge_P2[9] = assoc2.edgeGlobal(9);

  BOOST_CHECK_EQUAL( edgeTrue_P2[0], edge_P2[0] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[1], edge_P2[1] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[2], edge_P2[2] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[3], edge_P2[3] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[4], edge_P2[4] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[5], edge_P2[5] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[6], edge_P2[6] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[7], edge_P2[7] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[8], edge_P2[8] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[9], edge_P2[9] );

  for (int i=0; i<15; i++)
    mapDOF[i] = -1;

  assoc2.getGlobalMapping( mapDOF, 15 );

  BOOST_CHECK_EQUAL( nodeTrue[0], mapDOF[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], mapDOF[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], mapDOF[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], mapDOF[3] );
  BOOST_CHECK_EQUAL( nodeTrue[4], mapDOF[4] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[0], mapDOF[5] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[1], mapDOF[6] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[2], mapDOF[7] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[3], mapDOF[8] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[4], mapDOF[9] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[5], mapDOF[10] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[6], mapDOF[11] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[7], mapDOF[12] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[8], mapDOF[13] );
  BOOST_CHECK_EQUAL( edgeTrue_P2[9], mapDOF[14] );

}

BOOST_AUTO_TEST_CASE( fcnsSpacetime_cubic )
{
  // cubic element
  typedef ElementAssociativityConstructor<TopoD4,Pentatope> Constructor;
  Constructor assoc1ctor( BasisFunctionSpacetimeBase<Pentatope>::LagrangeP3 );

  BOOST_CHECK_EQUAL( 3, assoc1ctor.order() );
  BOOST_CHECK_EQUAL( 5, assoc1ctor.nNode() );
  BOOST_CHECK_EQUAL( 20, assoc1ctor.nEdge() );
  BOOST_CHECK_EQUAL( 10, assoc1ctor.nArea() );
  BOOST_CHECK_EQUAL( 0, assoc1ctor.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc1ctor.nCell() );

  assoc1ctor.setRank( 10 );
  BOOST_CHECK_EQUAL( 10, assoc1ctor.rank() );

  // set/get the node DOF
  int nodeTrue[5] = {3, 4, 5, 6, 7};
  int node[5];

  node[0] = nodeTrue[0];
  node[1] = nodeTrue[1];
  node[2] = nodeTrue[2];
  node[3] = nodeTrue[3];
  node[4] = nodeTrue[4];
  assoc1ctor.setNodeGlobalMapping( node, 5 );
  assoc1ctor.getNodeGlobalMapping( node, 5 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );
  BOOST_CHECK_EQUAL( nodeTrue[4], node[4] );

  node[0] = assoc1ctor.nodeGlobal(0);
  node[1] = assoc1ctor.nodeGlobal(1);
  node[2] = assoc1ctor.nodeGlobal(2);
  node[3] = assoc1ctor.nodeGlobal(3);
  node[4] = assoc1ctor.nodeGlobal(4);
  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );
  BOOST_CHECK_EQUAL( nodeTrue[4], node[4] );

  // set/get the edge DOF
  int edgeTrue[20] = {8,10,38,90,13,15,9,11,45,30,12,19,22,23,24,25,30,31,33,32};
  int edge[20];

  // set the edge DOF
  for (int j=0;j<20;j++)
    edge[j] = edgeTrue[j];
  assoc1ctor.setEdgeGlobalMapping( edge , 20 );

  // check the getEdgeGlobalMapping function
  assoc1ctor.getEdgeGlobalMapping( edge , 20 );
  CHECK_EQUAL_20( edgeTrue , edge );

  // check the edgeGlobal function
  for (int j=0;j<20;j++)
    edge[j] = assoc1ctor.edgeGlobal(j);
  CHECK_EQUAL_20( edgeTrue , edge );

  // set/get the area DOF
  int areaTrue[10] = {101,104,102,100,99,98,113,112,121,198};
  int area[10];

  // set the area DOF
  for (int j=0;j<10;j++)
   area[j] = areaTrue[j];
  assoc1ctor.setAreaGlobalMapping( area , 10 );

  // check the getAreaGlobalMapping function
  assoc1ctor.getAreaGlobalMapping( area , 10 );
  CHECK_EQUAL_10( areaTrue , area );

  // check the areaGlobal function
  for (int j=0;j<10;j++)
    area[j] = assoc1ctor.areaGlobal(j);
  CHECK_EQUAL_10( areaTrue , area );

  // (order+1)*(order+2)*(order+3)*(order+4) / 24 = 35
  int dofTrue[35],dof[35];
  for (int j=0;j<5;j++)
    dofTrue[j] = nodeTrue[j];
  for (int j=0;j<20;j++)
    dofTrue[j+5] = edgeTrue[j];
  for (int j=0;j<10;j++)
    dofTrue[j+25] = areaTrue[j];

  // check all the DOF
  assoc1ctor.getGlobalMapping( dof , 35 );
  CHECK_EQUAL_35( dofTrue , dof );

  ElementAssociativity<TopoD4,Pentatope> assoc1( assoc1ctor );

  BOOST_CHECK_EQUAL( 3, assoc1.order() );
  BOOST_CHECK_EQUAL( 5, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 20, assoc1.nEdge() );
  BOOST_CHECK_EQUAL( 10, assoc1.nArea() );
  BOOST_CHECK_EQUAL( 0 , assoc1.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc1.nCell() );

  BOOST_CHECK_EQUAL( 10, assoc1.rank() );

  node[0] = node[1] = node[2] = node[3] = node[4] = -1;
  assoc1.getNodeGlobalMapping( node, 5 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );
  BOOST_CHECK_EQUAL( nodeTrue[4], node[4] );

  node[0] = assoc1.nodeGlobal(0);
  node[1] = assoc1.nodeGlobal(1);
  node[2] = assoc1.nodeGlobal(2);
  node[3] = assoc1.nodeGlobal(3);
  node[4] = assoc1.nodeGlobal(4);
  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );
  BOOST_CHECK_EQUAL( nodeTrue[4], node[4] );

  // check the getEdgeGlobalMapping function
  assoc1.getEdgeGlobalMapping( edge , 20 );
  CHECK_EQUAL_20( edgeTrue , edge );

  // check the edgeGlobal function
  for (int j=0;j<20;j++)
    edge[j] = assoc1.edgeGlobal(j);
  CHECK_EQUAL_20( edgeTrue , edge );

  // check the getAreaGlobalMapping function
  assoc1.getAreaGlobalMapping( area , 10 );
  CHECK_EQUAL_10( areaTrue , area );

  // check the areaGlobal function
  for (int j=0;j<10;j++)
    area[j] = assoc1.areaGlobal(j);
  CHECK_EQUAL_10( areaTrue , area );

  for (int j=0;j<35;j++)
    dof[j] = -1;
  assoc1.getGlobalMapping( dof, 35 );

  CHECK_EQUAL_35( dofTrue , dof );

}

BOOST_AUTO_TEST_CASE( fcnsSpacetime_quartic )
{
  // quartic element
  typedef ElementAssociativityConstructor<TopoD4,Pentatope> Constructor;
  Constructor assoc1ctor( BasisFunctionSpacetimeBase<Pentatope>::LagrangeP4 );

  BOOST_CHECK_EQUAL( 4, assoc1ctor.order() );
  BOOST_CHECK_EQUAL( 5, assoc1ctor.nNode() );
  BOOST_CHECK_EQUAL( 30, assoc1ctor.nEdge() );
  BOOST_CHECK_EQUAL( 30, assoc1ctor.nArea() );
  BOOST_CHECK_EQUAL( 5, assoc1ctor.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc1ctor.nCell() );

  assoc1ctor.setRank( 10 );
  BOOST_CHECK_EQUAL( 10, assoc1ctor.rank() );

  // set/get the node DOF
  int nodeTrue[5] = {3, 4, 5, 6, 7};
  int node[5];

  node[0] = nodeTrue[0];
  node[1] = nodeTrue[1];
  node[2] = nodeTrue[2];
  node[3] = nodeTrue[3];
  node[4] = nodeTrue[4];
  assoc1ctor.setNodeGlobalMapping( node, 5 );
  assoc1ctor.getNodeGlobalMapping( node, 5 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );
  BOOST_CHECK_EQUAL( nodeTrue[4], node[4] );

  node[0] = assoc1ctor.nodeGlobal(0);
  node[1] = assoc1ctor.nodeGlobal(1);
  node[2] = assoc1ctor.nodeGlobal(2);
  node[3] = assoc1ctor.nodeGlobal(3);
  node[4] = assoc1ctor.nodeGlobal(4);
  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );
  BOOST_CHECK_EQUAL( nodeTrue[4], node[4] );

  // set/get the edge DOF
  int edgeTrue[30] = {8,10,38,90,13,15,9,11,45,30,12,19,22,23,24,25,30,31,33,32,201,202,203,205,204,207,206,209,210,211};
  int edge[30];

  // set the edge DOF
  for (int j=0;j<30;j++)
    edge[j] = edgeTrue[j];
  assoc1ctor.setEdgeGlobalMapping( edge , 30 );

  // check the getEdgeGlobalMapping function
  assoc1ctor.getEdgeGlobalMapping( edge , 30 );
  CHECK_EQUAL_20( edgeTrue , edge );

  // check the edgeGlobal function
  for (int j=0;j<30;j++)
    edge[j] = assoc1ctor.edgeGlobal(j);
  CHECK_EQUAL_20( edgeTrue , edge );

  // set/get the area DOF
  int areaTrue[30] = {101,104,102,100,99,98,113,112,121,198,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320};
  int area[30];

  // set the area DOF
  for (int j=0;j<30;j++)
   area[j] = areaTrue[j];
  assoc1ctor.setAreaGlobalMapping( area , 30 );

  // check the getAreaGlobalMapping function
  assoc1ctor.getAreaGlobalMapping( area , 30 );
  CHECK_EQUAL_30( areaTrue , area );

  // check the areaGlobal function
  for (int j=0;j<30;j++)
    area[j] = assoc1ctor.areaGlobal(j);
  CHECK_EQUAL_30( areaTrue , area );

  int faceTrue[5] = {401,432,420,100001,2345};
  int face[5];

  // set the face DOF
  for (int j=0;j<5;j++)
   face[j] = faceTrue[j];
  assoc1ctor.setFaceGlobalMapping( face , 5 );

  // check the getAreaGlobalMapping function
  assoc1ctor.getFaceGlobalMapping( face , 5 );
  CHECK_EQUAL_5( faceTrue , face );

  // check the faceGlobal function
  for (int j=0;j<5;j++)
    face[j] = assoc1ctor.faceGlobal(j);
  CHECK_EQUAL_5( faceTrue , face );

  assoc1ctor.setFaceGlobalMapping( {faceTrue[0],faceTrue[1],faceTrue[2],faceTrue[3],faceTrue[4]} );
  assoc1ctor.getFaceGlobalMapping( face , 5 );
  CHECK_EQUAL_5( faceTrue , face );

  // (order+1)*(order+2)*(order+3)*(order+4) / 24 = 35
  int dofTrue[70],dof[70];
  for (int j=0;j<5;j++)
    dofTrue[j] = nodeTrue[j];
  for (int j=0;j<30;j++)
    dofTrue[j+5] = edgeTrue[j];
  for (int j=0;j<30;j++)
    dofTrue[j+35] = areaTrue[j];
  for (int j=0;j<5;j++)
    dofTrue[j+65] = faceTrue[j];

  // check all the DOF
  assoc1ctor.getGlobalMapping( dof , 70 );
  for (int j=0;j<70;j++)
    BOOST_CHECK_EQUAL( dofTrue[j] , dof[j] );

  ElementAssociativity<TopoD4,Pentatope> assoc1( assoc1ctor );

  BOOST_CHECK_EQUAL( 4, assoc1.order() );
  BOOST_CHECK_EQUAL( 5, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 30, assoc1.nEdge() );
  BOOST_CHECK_EQUAL( 30, assoc1.nArea() );
  BOOST_CHECK_EQUAL( 5, assoc1.nFace() );
  BOOST_CHECK_EQUAL( 0, assoc1.nCell() );

  BOOST_CHECK_EQUAL( 10, assoc1.rank() );

  node[0] = node[1] = node[2] = node[3] = node[4] = -1;
  assoc1.getNodeGlobalMapping( node, 5 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );
  BOOST_CHECK_EQUAL( nodeTrue[4], node[4] );

  node[0] = assoc1.nodeGlobal(0);
  node[1] = assoc1.nodeGlobal(1);
  node[2] = assoc1.nodeGlobal(2);
  node[3] = assoc1.nodeGlobal(3);
  node[4] = assoc1.nodeGlobal(4);
  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );
  BOOST_CHECK_EQUAL( nodeTrue[1], node[1] );
  BOOST_CHECK_EQUAL( nodeTrue[2], node[2] );
  BOOST_CHECK_EQUAL( nodeTrue[3], node[3] );
  BOOST_CHECK_EQUAL( nodeTrue[4], node[4] );

  // check the getEdgeGlobalMapping function
  assoc1.getEdgeGlobalMapping( edge , 30 );
  CHECK_EQUAL_30( edgeTrue , edge );

  // check the edgeGlobal function
  for (int j=0;j<30;j++)
    edge[j] = assoc1.edgeGlobal(j);
  CHECK_EQUAL_30( edgeTrue , edge );

  // check the getAreaGlobalMapping function
  assoc1.getAreaGlobalMapping( area , 30 );
  CHECK_EQUAL_30( areaTrue , area );

  // check the areaGlobal function
  for (int j=0;j<30;j++)
    area[j] = assoc1.areaGlobal(j);
  CHECK_EQUAL_30( areaTrue , area );

  for (int j=0;j<70;j++)
    dof[j] = -1;
  assoc1.getGlobalMapping( dof, 70 );
  for (int j=0;j<70;j++)
    BOOST_CHECK_EQUAL( dofTrue[j] , dof[j] );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( facesignSpacetime )
{
  typedef std::array<int,5> Int5;
  typedef ElementAssociativityConstructor<TopoD4,Pentatope> Constructor;

  Constructor assocCtor( BasisFunctionSpacetimeBase<Pentatope>::LagrangeP1 );

  assocCtor.setRank( 2 );
  assocCtor.setNodeGlobalMapping( {3, 4, 5, 6, 7} );

  Int5 faceSign = assocCtor.faceSign();
  BOOST_CHECK_EQUAL( +1, faceSign[0] );
  BOOST_CHECK_EQUAL( +1, faceSign[1] );
  BOOST_CHECK_EQUAL( +1, faceSign[2] );
  BOOST_CHECK_EQUAL( +1, faceSign[3] );
  BOOST_CHECK_EQUAL( +1, faceSign[4] );

  faceSign[1] = -1;
  assocCtor.faceSign() = faceSign;

  Int5 faceSign2 = assocCtor.faceSign();
  BOOST_CHECK_EQUAL( +1, faceSign2[0] );
  BOOST_CHECK_EQUAL( -1, faceSign2[1] );
  BOOST_CHECK_EQUAL( +1, faceSign2[2] );
  BOOST_CHECK_EQUAL( +1, faceSign2[3] );
  BOOST_CHECK_EQUAL( +1, faceSign2[4] );

  assocCtor.setFaceSign( -1, 2 );
  Int5 faceSign3 = assocCtor.faceSign();
  BOOST_CHECK_EQUAL( +1, faceSign3[0] );
  BOOST_CHECK_EQUAL( -1, faceSign3[1] );
  BOOST_CHECK_EQUAL( -1, faceSign3[2] );
  BOOST_CHECK_EQUAL( +1, faceSign3[3] );
  BOOST_CHECK_EQUAL( +1, faceSign3[4] );

  ElementAssociativity<TopoD4,Pentatope> assoc( assocCtor );

  faceSign = assoc.faceSign();
  BOOST_CHECK_EQUAL( +1, faceSign[0] );
  BOOST_CHECK_EQUAL( -1, faceSign[1] );
  BOOST_CHECK_EQUAL( -1, faceSign[2] );
  BOOST_CHECK_EQUAL( +1, faceSign[3] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/ElementAssociativitySpacetime_Pentatope_pattern.txt", true );

  //Volume dumps
  typedef ElementAssociativityConstructor<TopoD4,Pentatope> Constructor;

  Constructor assoc1ctor( BasisFunctionSpacetimeBase<Pentatope>::LagrangeP1 );

  assoc1ctor.setRank( 2 );
  assoc1ctor.setNodeGlobalMapping( {1, 2, 3, 4, 5} );

  ElementAssociativity<TopoD4,Pentatope> assoc1( assoc1ctor );

  assoc1ctor.dump( 2 , output );
  BOOST_CHECK( output.match_pattern() );

  assoc1.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
