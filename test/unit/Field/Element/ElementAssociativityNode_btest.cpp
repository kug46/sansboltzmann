// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementAssociativityNode_btest
// testing of ElementAssociativityNode classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "Topology/ElementTopology.h"
#include "Field/Element/ElementAssociativityNode.h"
#include "BasisFunction/BasisFunctionNode.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementAssociativityNode_test_suite )

//----------------------------------------------------------------------------//
// Tested class(es): ElementAssociativityNode
BOOST_AUTO_TEST_CASE( ctorsNode )
{
  int order;

  order = 0;
  ElementAssociativity<TopoD0,Node> assoc1(order);

  BOOST_CHECK_EQUAL( 0, assoc1.order() );
  BOOST_CHECK_EQUAL( 1, assoc1.nNode() );

  ElementAssociativity<TopoD0,Node> assoc3( BasisFunctionNodeBase::P0 );

  BOOST_CHECK_EQUAL( 0, assoc3.order() );
  BOOST_CHECK_EQUAL( 1, assoc3.nNode() );

  ElementAssociativity<TopoD0,Node> assoc4(assoc3);

  BOOST_CHECK_EQUAL( 0, assoc4.order() );
  BOOST_CHECK_EQUAL( 1, assoc4.nNode() );

  //Check if association order is too low or too high
  typedef ElementAssociativity<TopoD0,Node> ElementAssociativityLine;
  BOOST_CHECK_THROW( ElementAssociativityLine assoc6(1), AssertionException );
}


//----------------------------------------------------------------------------//
// Tested class(es): ElementAssociativityConstructor<TopoD0, Node>
BOOST_AUTO_TEST_CASE( ctorsNodeConstructor )
{
  int order;

  order = 0;
  typedef ElementAssociativityConstructor<TopoD0,Node> Constructor;
  Constructor assoc1(order);

  BOOST_CHECK_EQUAL( 0, assoc1.order() );
  BOOST_CHECK_EQUAL( 1, assoc1.nNode() );

  Constructor assoc3( BasisFunctionNodeBase::P0 );

  BOOST_CHECK_EQUAL( 0, assoc3.order() );
  BOOST_CHECK_EQUAL( 1, assoc3.nNode() );

  // default ctor and operator=
  Constructor assoc5;
  assoc5 = assoc3;

  BOOST_CHECK_EQUAL( 0, assoc5.order() );
  BOOST_CHECK_EQUAL( 1, assoc5.nNode() );

  //Check if association order is too low or too high
  typedef ElementAssociativityConstructor<TopoD0, Node> ElementAssociativityNodeConstructor;
  BOOST_CHECK_THROW( ElementAssociativityNodeConstructor assoc6(-1), AssertionException );
  BOOST_CHECK_THROW( ElementAssociativityNodeConstructor assoc7(BasisFunctionNode_PMax+1), AssertionException );
}

//----------------------------------------------------------------------------//
// Tested class(es): ElementAssociativityConstructor<TopoD0, Node>, ElementAssociativityNode
BOOST_AUTO_TEST_CASE( fcnsNode )
{
  int order = 0;
  typedef ElementAssociativityConstructor<TopoD0,Node> Constructor;
  Constructor assoc1ctor(order);

  BOOST_CHECK_EQUAL( 0, assoc1ctor.order() );
  BOOST_CHECK_EQUAL( 1, assoc1ctor.nNode() );

  int nodeTrue[1] = {3};
  int node[1];

  assoc1ctor.setRank( 2 );
  BOOST_CHECK_EQUAL( 2, assoc1ctor.rank() );

  assoc1ctor.setNodeGlobalMapping( nodeTrue, 1 );
  node[0] = -1;
  assoc1ctor.getNodeGlobalMapping( node, 1 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );

  assoc1ctor.setNodeGlobalMapping( {nodeTrue[0]} );
  node[0] = -1;
  assoc1ctor.getNodeGlobalMapping( node, 1 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );

  assoc1ctor.setNormalSignL(1);
  assoc1ctor.setNormalSignR(-1);
  BOOST_CHECK_EQUAL(  1, assoc1ctor.normalSignL() );
  BOOST_CHECK_EQUAL( -1, assoc1ctor.normalSignR() );

  int node0;

  node0 = assoc1ctor.nodeGlobal(0);
  BOOST_CHECK_EQUAL( nodeTrue[0], node0 );

  assoc1ctor.getGlobalMapping( node, 1 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );


  ElementAssociativity<TopoD0,Node> assoc1(assoc1ctor);
  BOOST_CHECK_EQUAL( 0, assoc1.order() );
  BOOST_CHECK_EQUAL( 1, assoc1.nNode() );

  BOOST_CHECK_EQUAL( 2, assoc1.rank() );

  node[0] = -1;
  assoc1.getNodeGlobalMapping( node, 1 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );

  node[0] = -1;
  assoc1.getGlobalMapping( node, 1 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );

  int map[1];
  assoc1ctor.getGlobalMapping( map, 1 );

  BOOST_CHECK_EQUAL( nodeTrue[0], map[0] );

  BOOST_CHECK_EQUAL(  1, assoc1.normalSignL() );
  BOOST_CHECK_EQUAL( -1, assoc1.normalSignR() );


  ElementAssociativity<TopoD0,Node> assoc2(order);

  BOOST_CHECK_EQUAL( 0, assoc2.normalSignL() ); // By default there should not be a normal sign
  BOOST_CHECK_EQUAL( 0, assoc2.normalSignR() ); // By default there should not be a normal sign

  assoc2 = assoc1ctor;

  BOOST_CHECK_EQUAL( 0, assoc2.order() );
  BOOST_CHECK_EQUAL( 1, assoc2.nNode() );

  BOOST_CHECK_EQUAL( 2, assoc2.rank() );

  node[0] = -1;
  assoc2.getNodeGlobalMapping( node, 1 );

  BOOST_CHECK_EQUAL( nodeTrue[0], node[0] );

  map[0] = -1;
  assoc2.getGlobalMapping( map, 1 );

  BOOST_CHECK_EQUAL( nodeTrue[0], map[0] );

  BOOST_CHECK_EQUAL(  1, assoc2.normalSignL() );
  BOOST_CHECK_EQUAL( -1, assoc2.normalSignR() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/ElementAssociativityNode_pattern.txt", true );

  //Node dump
  typedef ElementAssociativityConstructor<TopoD0,Node> Constructor;
  Constructor assocNodector(0);

  assocNodector.setRank( 1 );
  assocNodector.setNodeGlobalMapping( {3} );

  ElementAssociativity<TopoD0,Node> assocNode(assocNodector);

  assocNodector.dump( 2, output );
  assocNode.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
