// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementArea_Quad_btest
// testing of Element< T, TopoD2, Quad >

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <ostream>
#include <memory>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"
#include "Field/Element/ElementArea.h"
#include "BasisFunction/BasisFunctionArea_Quad.h"
#include "Quadrature/QuadratureArea.h"
#include "Quadrature/QuadratureLine.h"
#include "BasisFunction/TraceToCellRefCoord.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class Element<Real, TopoD2, Quad>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementArea_Quad_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  typedef Element< Real, TopoD2, Quad > ElementClass;

  static_assert( std::is_same<typename ElementClass::BasisType, BasisFunctionAreaBase<Quad> >::value, "Incorrect Basis type" );
  static_assert( std::is_same<typename ElementClass::TopologyType, Quad>::value, "Incorrect topology type" );
  static_assert( std::is_same<typename ElementClass::RefCoordType, DLA::VectorS<2,Real> >::value, "Incorrect reference coordinate type" );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef Element< Real, TopoD2, Quad > ElementClass;

  int order;

  order = 1;
  ElementClass qElem1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem1.order() );
  BOOST_CHECK_EQUAL( 4, qElem1.nDOF() );
#if 0
  order = 2;
  ElementClass qElem2(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qElem2.order() );
  BOOST_CHECK_EQUAL( 6, qElem2.nDOF() );

  ElementClass qElem3( BasisFunctionAreaBase<Quad>::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, qElem3.order() );
  BOOST_CHECK_EQUAL( 6, qElem3.nDOF() );

  ElementClass qElem4(qElem1);

  BOOST_CHECK_EQUAL( 1, qElem4.order() );
  BOOST_CHECK_EQUAL( 3, qElem4.nDOF() );

  qElem4 = qElem2;

  BOOST_CHECK_EQUAL( 2, qElem4.order() );
  BOOST_CHECK_EQUAL( 6, qElem4.nDOF() );

  ElementClass qElem5;

  qElem5.setBasis( BasisFunctionAreaBase<Quad>::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, qElem5.order() );
  BOOST_CHECK_EQUAL( 6, qElem5.nDOF() );
#endif
  BOOST_CHECK_THROW( ElementClass qElem5(0, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( ElementClass qElem6(BasisFunctionArea_Quad_HierarchicalPMax+1, BasisFunctionCategory_Hierarchical), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( accessors )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD2, Quad > ElementClass;

  static const int NNode = Quad::NNode;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( NNode, qElem.nDOF() );

  const Real tol = 1e-13;

  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};
  ArrayQ q3 = {5, 6};
  ArrayQ q4 = {7, 8};

  qElem.DOF(0) = q1;
  qElem.DOF(1) = q2;
  qElem.DOF(2) = q3;
  qElem.DOF(3) = q4;

  ArrayQ q;
  for (int k = 0; k < 2; k++)
  {
    q = qElem.DOF(0);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );
    q = qElem.DOF(1);
    BOOST_CHECK_CLOSE( q2[k], q[k], tol );
    q = qElem.DOF(2);
    BOOST_CHECK_CLOSE( q3[k], q[k], tol );
    q = qElem.DOF(3);
    BOOST_CHECK_CLOSE( q4[k], q[k], tol );
  }


  // Check the vector view version of the DOF
  DLA::VectorDView<ArrayQ> vectorDOF = qElem.vectorViewDOF();

  BOOST_CHECK_EQUAL( qElem.nDOF(), vectorDOF.m() );

  vectorDOF[0] = q4;
  vectorDOF[1] = q3;
  vectorDOF[2] = q2;
  vectorDOF[3] = q1;

  for (int k = 0; k < 2; k++)
  {
    q = qElem.DOF(0);
    BOOST_CHECK_CLOSE( q4[k], q[k], tol );
    q = qElem.DOF(1);
    BOOST_CHECK_CLOSE( q3[k], q[k], tol );
    q = qElem.DOF(2);
    BOOST_CHECK_CLOSE( q2[k], q[k], tol );
    q = qElem.DOF(3);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( eval_basis )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD2, Quad > ElementClass;

  static const int NNode = Quad::NNode;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( NNode, qElem.nDOF() );

  const Real tol = 1e-13;
  Real sRef, tRef;
  Real phi[NNode], phi1[NNode], phis[NNode], phit[NNode];
  Real phiss[NNode], phist[NNode], phitt[NNode];
  Real phiTrue[NNode], phisTrue[NNode], phitTrue[NNode];
  Real phissTrue[NNode], phistTrue[NNode], phittTrue[NNode];
  int k;

  for (k=0; k<NNode;k++)
  {
    phiss[k] = 0; phist[k] = 0; phitt[k] = 0;
    phissTrue[k] = 0; phittTrue[k] = 0;
    phistTrue[k] = pow(-1.,k);
  }

  sRef = 0;  tRef = 0;
  phiTrue[0]  =  1;  phiTrue[1]  = 0;  phiTrue[2]  = 0;  phiTrue[3] = 0;

  qElem.evalBasis( sRef, tRef, phi, NNode );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  // Compute true derivatives using finite difference (which is exact for linear)
  sRef += 1;
  qElem.evalBasis( sRef, tRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phisTrue[k] = phi1[k] - phi[k];
  sRef -= 1;

  tRef += 1;
  qElem.evalBasis( sRef, tRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phitTrue[k] = phi1[k] - phi[k];
  tRef -= 1;

  qElem.evalBasisDerivative( sRef, tRef, phis, phit, NNode );
  qElem.evalBasisHessianDerivative( sRef, tRef, phiss, phist, phitt, NNode );
  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], tol, tol);
    SANS_CHECK_CLOSE( phistTrue[k], phist[k], tol, tol );
    SANS_CHECK_CLOSE( phittTrue[k], phitt[k], tol, tol );
  }


  sRef = 1;  tRef = 0;
  phiTrue[0]  =  0;  phiTrue[1]  =  1;  phiTrue[2]  = 0;  phiTrue[3]  = 0;

  qElem.evalBasis( sRef, tRef, phi, NNode );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  // Compute true derivatives using finite difference (which is exact for linear)
  sRef += 1;
  qElem.evalBasis( sRef, tRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phisTrue[k] = phi1[k] - phi[k];
  sRef -= 1;

  tRef += 1;
  qElem.evalBasis( sRef, tRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phitTrue[k] = phi1[k] - phi[k];
  tRef -= 1;

  qElem.evalBasisDerivative( sRef, tRef, phis, phit, NNode );
  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
  }


  sRef = 1;  tRef = 1;
  phiTrue[0]  =  0;  phiTrue[1]  =  0;  phiTrue[2]  = 1;  phiTrue[3]  =  0;

  qElem.evalBasis( sRef, tRef, phi, NNode );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  // Compute true derivatives using finite difference (which is exact for linear)
  sRef += 1;
  qElem.evalBasis( sRef, tRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phisTrue[k] = phi1[k] - phi[k];
  sRef -= 1;

  tRef += 1;
  qElem.evalBasis( sRef, tRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phitTrue[k] = phi1[k] - phi[k];
  tRef -= 1;

  qElem.evalBasisDerivative( sRef, tRef, phis, phit, NNode );
  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
  }


  sRef = 1./2.;  tRef = 1./2.;
  phiTrue[0]  =  1./4.;  phiTrue[1]  =  1./4.;  phiTrue[2]  = 1./4.;  phiTrue[3]  =  1./4.;

  qElem.evalBasis( sRef, tRef, phi, NNode );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  // Compute true derivatives using finite difference (which is exact for linear)
  sRef += 1;
  qElem.evalBasis( sRef, tRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phisTrue[k] = phi1[k] - phi[k];
  sRef -= 1;

  tRef += 1;
  qElem.evalBasis( sRef, tRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phitTrue[k] = phi1[k] - phi[k];
  tRef -= 1;

  qElem.evalBasisDerivative( sRef, tRef, phis, phit, NNode );
  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( eval_variable )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD2, Quad > ElementClass;

  static const int NNode = Quad::NNode;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( NNode, qElem.nDOF() );

  const Real tol = 1e-13;
  Real sRef, tRef;
  Real phi[NNode];
  Real phiTrue[NNode];
  ArrayQ q, qTrue;                // variable
  ArrayQ qs, qt, qsTrue, qtTrue;  // variable derivatives wrt sRef
  int k;

  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};
  ArrayQ q3 = {5, 6};
  ArrayQ q4 = {6, 7};

  qElem.DOF(0) = q1;
  qElem.DOF(1) = q2;
  qElem.DOF(2) = q3;
  qElem.DOF(3) = q4;

  // test @ (s,t) = (0,0)
  sRef = 0;  tRef = 0;
  phiTrue[0] = 1;  phiTrue[1] = 0;  phiTrue[2] = 0;  phiTrue[3] = 0;
  qTrue = q1;
  qsTrue = q2 - q1;  // compute true derivatives using finite difference (which is exact for linear)
  qtTrue = q4 - q1;  // compute true derivatives using finite difference (which is exact for linear)

  qElem.evalBasis( sRef, tRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  qElem.evalDerivative( sRef, tRef, qs, qt );

  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  for (k = 0; k < 2; k++)
  {
    BOOST_CHECK_CLOSE( qTrue[k],  q[k],  tol );
    BOOST_CHECK_CLOSE( qsTrue[k], qs[k], tol );
    BOOST_CHECK_CLOSE( qtTrue[k], qt[k], tol );
  }

  q = 0; // cppcheck-suppress redundantAssignment
  q = qElem.eval( sRef, tRef );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );

  // test @ (s,t) = (1,0)
  sRef = 1;  tRef = 0;
  phiTrue[0] = 0;  phiTrue[1] = 1;  phiTrue[2] = 0;  phiTrue[3] = 0;
  qTrue = q2;
  qsTrue = q2 - q1;  // compute true derivatives using finite difference (which is exact for linear)
  qtTrue = q3 - q2;  // compute true derivatives using finite difference (which is exact for linear)

  qElem.evalBasis( sRef, tRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  qElem.evalDerivative( sRef, tRef, qs, qt );

  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  for (k = 0; k < 2; k++)
  {
    BOOST_CHECK_CLOSE( qTrue[k],  q[k],  tol );
    BOOST_CHECK_CLOSE( qsTrue[k], qs[k], tol );
    BOOST_CHECK_CLOSE( qtTrue[k], qt[k], tol );
  }

  q = 0; // cppcheck-suppress redundantAssignment
  q = qElem.eval( sRef, tRef );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );


  // test @ (s,t) = (1,1)
  sRef = 1;  tRef = 1;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 1;  phiTrue[3] = 0;
  qTrue = q3;
  qsTrue = q3 - q4;  // compute true derivatives using finite difference (which is exact for linear)
  qtTrue = q3 - q2;  // compute true derivatives using finite difference (which is exact for linear)

  qElem.evalBasis( sRef, tRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  qElem.evalDerivative( sRef, tRef, qs, qt );

  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  for (k = 0; k < 2; k++)
  {
    BOOST_CHECK_CLOSE( qTrue[k],  q[k],  tol );
    BOOST_CHECK_CLOSE( qsTrue[k], qs[k], tol );
    BOOST_CHECK_CLOSE( qtTrue[k], qt[k], tol );
  }

  q = 0; // cppcheck-suppress redundantAssignment
  qElem.eval( sRef, tRef, q );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );


  // test @ (s,t) = (0,1)
  sRef = 0;  tRef = 1;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 0;  phiTrue[3] = 1;
  qTrue = q4;
  qsTrue = q3 - q4;  // compute true derivatives using finite difference (which is exact for linear)
  qtTrue = q4 - q1;  // compute true derivatives using finite difference (which is exact for linear)

  qElem.evalBasis( sRef, tRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  qElem.evalDerivative( sRef, tRef, qs, qt );

  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  for (k = 0; k < 2; k++)
  {
    BOOST_CHECK_CLOSE( qTrue[k],  q[k],  tol );
    BOOST_CHECK_CLOSE( qsTrue[k], qs[k], tol );
    BOOST_CHECK_CLOSE( qtTrue[k], qt[k], tol );
  }

  q = 0; // cppcheck-suppress redundantAssignment
  qElem.eval( sRef, tRef, q );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );


  // test @ (s,t) = (0.5,0.5)
  sRef = 1./2.;  tRef = 1./2.;
  phiTrue[0] = 1./4.;  phiTrue[1] = 1./4.;  phiTrue[2] = 1./4.;  phiTrue[3] = 1./4.;
  qTrue  = (q1 + q2 + q3 + q4)/4.;
  qsTrue = (-q1 + q2 + q3 - q4)/2.;  // compute true derivatives using finite difference (which is exact for linear)
  qtTrue = (-q1 - q2 + q3 + q4)/2.;  // compute true derivatives using finite difference (which is exact for linear)

  qElem.evalBasis( sRef, tRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  qElem.evalDerivative( sRef, tRef, qs, qt );

  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  for (k = 0; k < 2; k++)
  {
    BOOST_CHECK_CLOSE( qTrue[k],  q[k],  tol );
    BOOST_CHECK_CLOSE( qsTrue[k], qs[k], tol );
    BOOST_CHECK_CLOSE( qtTrue[k], qt[k], tol );
  }
  q = 0; // cppcheck-suppress redundantAssignment
  qElem.eval( sRef, tRef, q );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( cached_basis )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef DLA::VectorS< TopoD2::D, ArrayQ > VectorArrayQ;
  typedef DLA::VectorS< TopoD2::D, Real > RefCoordCell;
  typedef DLA::VectorS< TopoD1::D, Real > RefCoordTrace;
  typedef Element< ArrayQ, TopoD2, Quad > ElementClass;
  typedef std::array<int,Quad::NTrace> IntTrace;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-13;

  RefCoordCell ref;
  ArrayQ q, qTrue;
  VectorArrayQ derivq, derivqTrue;

  const int nEdgeSign = 15;

  IntTrace edgeSigns[nEdgeSign] = { {{ 1, 1, 1, 1}},

                                    {{-1, 1, 1, 1}},
                                    {{ 1,-1, 1, 1}},
                                    {{ 1, 1,-1, 1}},
                                    {{ 1, 1, 1,-1}},

                                    {{-1,-1, 1, 1}},
                                    {{ 1,-1,-1, 1}},
                                    {{ 1, 1,-1,-1}},

                                    {{-1, 1,-1, 1}},
                                    {{ 1,-1, 1,-1}},
                                    {{-1, 1, 1,-1}},

                                    {{-1,-1,-1, 1}},
                                    {{ 1,-1,-1,-1}},
                                    {{-1, 1,-1,-1}},

                                    {{-1,-1,-1,-1}} };

  for (int isgn = 0; isgn < nEdgeSign; isgn++)
  {
    IntTrace sgn = edgeSigns[isgn];

    for (int order = 1; order < BasisFunctionArea_Quad_HierarchicalPMax+1; order++)
    {
      ElementClass qElem(order, BasisFunctionCategory_Hierarchical);
      qElem.setEdgeSign(sgn);

      int nDOF = qElem.nDOF();

      // initalize some arbitrary values
      for (int n = 0; n < nDOF; n++)
        qElem.DOF(n) = pow(-1, n) / sqrt(n+1);

      std::vector<Real> phiTrue(nDOF), phisTrue(nDOF), phitTrue(nDOF);
      std::vector<Real> phissTrue(nDOF), phistTrue(nDOF), phittTrue(nDOF);
      std::vector<Real> phi(nDOF), phis(nDOF), phit(nDOF);
      std::vector<Real> phiss(nDOF), phist(nDOF), phitt(nDOF);
      std::vector<RefCoordCell> dphi(nDOF);

      // check that the cached basis functions work
      for (int iorderidx = 0; iorderidx < QuadratureArea<Quad>::nOrderIdx; iorderidx++)
      {
        QuadratureArea<Quad> quadrature( QuadratureArea<Quad>::getOrderFromIndex(iorderidx) );
        for (int n = 0; n < quadrature.nQuadrature(); n++)
        {
          quadrature.coordinates(n, ref);
          QuadraturePoint<TopoD2> point = quadrature.coordinates_cache(n);

          //--------------
          qElem.evalBasis( ref, phiTrue.data(), phiTrue.size() );
          qElem.evalBasis( point, phi.data(), phi.size() );

          for (int k = 0; k < nDOF; k++)
          {
            SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
            phi[k] = 0;
          }

          // 'non-quadrature' coordinate (mainly used with testing of integrands)
          qElem.evalBasis( QuadraturePoint<TopoD2>(ref), phi.data(), phi.size() );

          for (int k = 0; k < nDOF; k++)
            SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );


          //--------------
          qElem.evalBasisDerivative( ref[0], ref[1], phisTrue.data(), phitTrue.data(), phisTrue.size() );
          qElem.evalBasisDerivative( point, phis.data(), phit.data(), phis.size() );
          qElem.evalBasisDerivative( point, dphi.data(), dphi.size() );

          for (int k = 0; k < nDOF; k++)
          {
            SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phisTrue[k], dphi[k][0], small_tol, close_tol );
            SANS_CHECK_CLOSE( phitTrue[k], dphi[k][1], small_tol, close_tol );
            phis[k] = 0; phit[k] = 0; dphi[k] = 0;
          }

          // 'non-quadrature' coordinate (mainly used with testing of integrands)
          qElem.evalBasisDerivative( QuadraturePoint<TopoD2>(ref), phis.data(), phit.data(), phis.size() );

          for (int k = 0; k < nDOF; k++)
          {
            SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
          }


          //--------------
          qElem.evalBasisHessianDerivative( ref[0], ref[1], phissTrue.data(), phistTrue.data(), phittTrue.data(), phissTrue.size() );
          qElem.evalBasisHessianDerivative( point, phiss.data(), phist.data(), phitt.data(), phiss.size() );

          for (int k = 0; k < nDOF; k++)
          {
            SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
            phiss[k] = 0; phist[k] = 0; phitt[k] = 0;
          }

          // 'non-quadrature' coordinate (mainly used with testing of integrands)
          qElem.evalBasisHessianDerivative( QuadraturePoint<TopoD2>(ref), phiss.data(), phist.data(), phitt.data(), phiss.size() );

          for (int k = 0; k < nDOF; k++)
          {
            SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
            SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
          }


          //--------------
          qElem.eval( ref, qTrue );
          qElem.eval( point, q );

          for (int k = 0; k < ArrayQ::M; k++)
            SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );

          // 'non-quadrature' coordinate (mainly used with testing of integrands)
          qElem.eval( QuadraturePoint<TopoD2>(ref), q );

          for (int k = 0; k < ArrayQ::M; k++)
            SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );


          //--------------
          qElem.evalDerivative( ref, derivqTrue );
          qElem.evalDerivative( point, derivq );

          for (int k = 0; k < ArrayQ::M; k++)
          {
            SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
            SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
          }

          // 'non-quadrature' coordinate (mainly used with testing of integrands)
          qElem.evalDerivative( QuadraturePoint<TopoD2>(ref), derivq );

          for (int k = 0; k < ArrayQ::M; k++)
          {
            SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
            SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
          }

        } // iquad
      } // orderidx


      for (int itrace = 0; itrace < Quad::NTrace; itrace++)
      {
        for (int orientation : {-1, 1})
        {
          CanonicalTraceToCell canonicalTrace(itrace, orientation);

          // relax tolerances for non-canonical traces
          Real small_tol = 1e-12;
          Real close_tol = 1e-13;
          if (orientation == -1)
          {
            small_tol = 1e-11;
            close_tol = 2e-8;
          }

          for (int orderidx = 0; orderidx < QuadratureLine::nOrderIdx; orderidx++)
          {
            QuadratureLine quadrature( QuadratureLine::getOrderFromIndex(orderidx) );

            RefCoordTrace sTrace;
            RefCoordCell refCell;
            QuadratureCellTracePoint<TopoD2> pointCell, pointCell2;
            for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
            {
              quadrature.coordinates(iquad, sTrace);
              QuadraturePoint<TopoD1> pointTrace = quadrature.coordinates_cache(iquad);

              TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalTrace, sTrace, refCell );
              TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalTrace, pointTrace, pointCell );
              TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalTrace, QuadraturePoint<TopoD1>(sTrace), pointCell2 );

              SANS_CHECK_CLOSE( refCell[0], pointCell.ref[0], small_tol, close_tol );
              SANS_CHECK_CLOSE( refCell[1], pointCell.ref[1], small_tol, close_tol );

              //--------------
              qElem.evalBasis( refCell, phiTrue.data(), phiTrue.size() );
              qElem.evalBasis( pointCell, phi.data(), phi.size() );

              for (int k = 0; k < nDOF; k++)
              {
                SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
                phi[k] = 0;
              }

              // 'non-quadrature' coordinate (mainly used with testing of integrands)
              qElem.evalBasis( pointCell2, phi.data(), phi.size() );

              for (int k = 0; k < nDOF; k++)
                SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );

              //--------------
              qElem.evalBasisDerivative( refCell[0], refCell[1], phisTrue.data(), phitTrue.data(), phisTrue.size() );
              qElem.evalBasisDerivative( pointCell, phis.data(), phit.data(), phis.size() );
              qElem.evalBasisDerivative( pointCell, dphi.data(), dphi.size() );

              for (int k = 0; k < nDOF; k++)
              {
                SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
                SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
                SANS_CHECK_CLOSE( phisTrue[k], dphi[k][0], small_tol, close_tol );
                SANS_CHECK_CLOSE( phitTrue[k], dphi[k][1], small_tol, close_tol );
                phis[k] = 0; phit[k] = 0; dphi[k] = 0;
              }

              // 'non-quadrature' coordinate (mainly used with testing of integrands)
              qElem.evalBasisDerivative( pointCell2, phis.data(), phit.data(), phis.size() );

              for (int k = 0; k < nDOF; k++)
              {
                SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
                SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
              }

              //--------------
              qElem.eval( refCell, qTrue );
              qElem.eval( pointCell, q );

              for (int k = 0; k < ArrayQ::M; k++)
                SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );

              // 'non-quadrature' coordinate (mainly used with testing of integrands)
              qElem.eval( pointCell2, q );

              for (int k = 0; k < ArrayQ::M; k++)
                SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );


              //--------------
              qElem.evalDerivative( refCell, derivqTrue );
              qElem.evalDerivative( pointCell, derivq );

              for (int k = 0; k < ArrayQ::M; k++)
              {
                SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
                SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
              }

              // 'non-quadrature' coordinate (mainly used with testing of integrands)
              qElem.evalDerivative( pointCell2, derivq );

              for (int k = 0; k < ArrayQ::M; k++)
              {
                SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
                SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
              }

            } //iquad
          } // orderidx
        } // orientation
      } // itrace

    } // order
  } // isgn


  for (int order = 0; order < BasisFunctionArea_Quad_LegendrePMax+1; order++)
  {
    ElementClass qElem(order, BasisFunctionCategory_Legendre);

    int nDOF = qElem.nDOF();

    // initalize some arbitrary values
    for (int n = 0; n < nDOF; n++)
      qElem.DOF(n) = pow(-1, n) / sqrt(n+1);

    std::vector<Real> phiTrue(nDOF), phisTrue(nDOF), phitTrue(nDOF);
    std::vector<Real> phissTrue(nDOF), phistTrue(nDOF), phittTrue(nDOF);
    std::vector<Real> phi(nDOF), phis(nDOF), phit(nDOF);
    std::vector<Real> phiss(nDOF), phist(nDOF), phitt(nDOF);
    std::vector<RefCoordCell> dphi(nDOF);

    // check that the cached basis functions work
    for (int iorderidx = 0; iorderidx < QuadratureArea<Quad>::nOrderIdx; iorderidx++)
    {
      QuadratureArea<Quad> quadrature( QuadratureArea<Quad>::getOrderFromIndex(iorderidx) );
      for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
      {
        quadrature.coordinates(iquad, ref);
        QuadraturePoint<TopoD2> point = quadrature.coordinates_cache(iquad);

        //--------------
        qElem.evalBasis( ref, phiTrue.data(), phiTrue.size() );
        qElem.evalBasis( point, phi.data(), phi.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
          phi[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasis( QuadraturePoint<TopoD2>(ref), phi.data(), phi.size() );

        for (int k = 0; k < nDOF; k++)
          SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );


        //--------------
        qElem.evalBasisDerivative( ref[0], ref[1], phisTrue.data(), phitTrue.data(), phisTrue.size() );
        qElem.evalBasisDerivative( point, phis.data(), phit.data(), phis.size() );
        qElem.evalBasisDerivative( point, dphi.data(), dphi.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phisTrue[k], dphi[k][0], small_tol, close_tol );
          SANS_CHECK_CLOSE( phitTrue[k], dphi[k][1], small_tol, close_tol );
          phis[k] = 0; phit[k] = 0; dphi[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasisDerivative( QuadraturePoint<TopoD2>(ref), phis.data(), phit.data(), phis.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
        }


        //--------------
        qElem.evalBasisHessianDerivative( ref[0], ref[1], phissTrue.data(), phistTrue.data(), phittTrue.data(), phissTrue.size() );
        qElem.evalBasisHessianDerivative( point, phiss.data(), phist.data(), phitt.data(), phiss.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
          phiss[k] = 0; phist[k] = 0; phitt[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasisHessianDerivative( QuadraturePoint<TopoD2>(ref), phiss.data(), phist.data(), phitt.data(), phiss.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
        }


        //--------------
        qElem.eval( ref, qTrue );
        qElem.eval( point, q );

        for (int k = 0; k < ArrayQ::M; k++)
          SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.eval( QuadraturePoint<TopoD2>(ref), q );

        for (int k = 0; k < ArrayQ::M; k++)
          SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );


        //--------------
        qElem.evalDerivative( ref, derivqTrue );
        qElem.evalDerivative( point, derivq );

        for (int k = 0; k < ArrayQ::M; k++)
        {
          SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
          SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalDerivative( QuadraturePoint<TopoD2>(ref), derivq );

        for (int k = 0; k < ArrayQ::M; k++)
        {
          SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
          SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
        }

      } // iquad
    } // orderidx


    for (int itrace = 0; itrace < Quad::NTrace; itrace++)
    {
      for (int orientation : {-1, 1})
      {
        CanonicalTraceToCell canonicalTrace(itrace, orientation);

        // relax tolerances for non-canonical traces
        Real small_tol = 1e-12;
        Real close_tol = 1e-13;
        if (orientation == -1)
        {
          small_tol = 1e-11;
          close_tol = 2e-8;
        }

        for (int orderidx = 0; orderidx < QuadratureLine::nOrderIdx; orderidx++)
        {
          QuadratureLine quadrature( QuadratureLine::getOrderFromIndex(orderidx) );

          RefCoordTrace sTrace;
          RefCoordCell refCell;
          QuadratureCellTracePoint<TopoD2> pointCell, pointCell2;
          for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
          {
            quadrature.coordinates(iquad, sTrace);
            QuadraturePoint<TopoD1> pointTrace = quadrature.coordinates_cache(iquad);

            TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalTrace, sTrace, refCell );
            TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalTrace, pointTrace, pointCell );
            TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalTrace, QuadraturePoint<TopoD1>(sTrace), pointCell2 );

            SANS_CHECK_CLOSE( refCell[0], pointCell.ref[0], small_tol, close_tol );
            SANS_CHECK_CLOSE( refCell[1], pointCell.ref[1], small_tol, close_tol );

            //--------------
            qElem.evalBasis( refCell, phiTrue.data(), phiTrue.size() );
            qElem.evalBasis( pointCell, phi.data(), phi.size() );

            for (int k = 0; k < nDOF; k++)
            {
              SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
              phi[k] = 0;
            }

            // 'non-quadrature' coordinate (mainly used with testing of integrands)
            qElem.evalBasis( pointCell2, phi.data(), phi.size() );

            for (int k = 0; k < nDOF; k++)
              SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );

            //--------------
            qElem.evalBasisDerivative( refCell[0], refCell[1], phisTrue.data(), phitTrue.data(), phisTrue.size() );
            qElem.evalBasisDerivative( pointCell, phis.data(), phit.data(), phis.size() );
            qElem.evalBasisDerivative( pointCell, dphi.data(), dphi.size() );

            for (int k = 0; k < nDOF; k++)
            {
              SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
              SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
              SANS_CHECK_CLOSE( phisTrue[k], dphi[k][0], small_tol, close_tol );
              SANS_CHECK_CLOSE( phitTrue[k], dphi[k][1], small_tol, close_tol );
              phis[k] = 0; phit[k] = 0; dphi[k] = 0;
            }

            // 'non-quadrature' coordinate (mainly used with testing of integrands)
            qElem.evalBasisDerivative( pointCell2, phis.data(), phit.data(), phis.size() );

            for (int k = 0; k < nDOF; k++)
            {
              SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
              SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
            }

            //--------------
            qElem.eval( refCell, qTrue );
            qElem.eval( pointCell, q );

            for (int k = 0; k < ArrayQ::M; k++)
              SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );

            // 'non-quadrature' coordinate (mainly used with testing of integrands)
            qElem.eval( pointCell2, q );

            for (int k = 0; k < ArrayQ::M; k++)
              SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );


            //--------------
            qElem.evalDerivative( refCell, derivqTrue );
            qElem.evalDerivative( pointCell, derivq );

            for (int k = 0; k < ArrayQ::M; k++)
            {
              SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
              SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
            }

            // 'non-quadrature' coordinate (mainly used with testing of integrands)
            qElem.evalDerivative( pointCell2, derivq );

            for (int k = 0; k < ArrayQ::M; k++)
            {
              SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
              SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
            }

          } //iquad
        } // orderidx
      } // orientation
    } // itrace

  } // order

  for (int order = 1; order < BasisFunctionArea_Quad_LagrangePMax+1; order++)
  {
    ElementClass qElem(order, BasisFunctionCategory_Lagrange);

    int nDOF = qElem.nDOF();

    // initalize some arbitrary values
    for (int n = 0; n < nDOF; n++)
      qElem.DOF(n) = pow(-1, n) / sqrt(n+1);

    std::vector<Real> phiTrue(nDOF), phisTrue(nDOF), phitTrue(nDOF);
    std::vector<Real> phissTrue(nDOF), phistTrue(nDOF), phittTrue(nDOF);
    std::vector<Real> phi(nDOF), phis(nDOF), phit(nDOF);
    std::vector<Real> phiss(nDOF), phist(nDOF), phitt(nDOF);
    std::vector<RefCoordCell> dphi(nDOF);

    // check that the cached basis functions work
    for (int iorderidx = 0; iorderidx < QuadratureArea<Quad>::nOrderIdx; iorderidx++)
    {
      QuadratureArea<Quad> quadrature( QuadratureArea<Quad>::getOrderFromIndex(iorderidx) );
      for (int n = 0; n < quadrature.nQuadrature(); n++)
      {
        quadrature.coordinates(n, ref);
        QuadraturePoint<TopoD2> point = quadrature.coordinates_cache(n);

        //--------------
        qElem.evalBasis( ref, phiTrue.data(), phiTrue.size() );
        qElem.evalBasis( point, phi.data(), phi.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
          phi[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasis( QuadraturePoint<TopoD2>(ref), phi.data(), phi.size() );

        for (int k = 0; k < nDOF; k++)
          SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );


        //--------------
        qElem.evalBasisDerivative( ref[0], ref[1], phisTrue.data(), phitTrue.data(), phisTrue.size() );
        qElem.evalBasisDerivative( point, phis.data(), phit.data(), phis.size() );
        qElem.evalBasisDerivative( point, dphi.data(), dphi.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phisTrue[k], dphi[k][0], small_tol, close_tol );
          SANS_CHECK_CLOSE( phitTrue[k], dphi[k][1], small_tol, close_tol );
          phis[k] = 0; phit[k] = 0; dphi[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasisDerivative( QuadraturePoint<TopoD2>(ref), phis.data(), phit.data(), phis.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
        }


        //--------------
        qElem.evalBasisHessianDerivative( ref[0], ref[1], phissTrue.data(), phistTrue.data(), phittTrue.data(), phissTrue.size() );
        qElem.evalBasisHessianDerivative( point, phiss.data(), phist.data(), phitt.data(), phiss.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
          phiss[k] = 0; phist[k] = 0; phitt[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasisHessianDerivative( QuadraturePoint<TopoD2>(ref), phiss.data(), phist.data(), phitt.data(), phiss.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
        }


        //--------------
        qElem.eval( ref, qTrue );
        qElem.eval( point, q );

        for (int k = 0; k < ArrayQ::M; k++)
          SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.eval( QuadraturePoint<TopoD2>(ref), q );

        for (int k = 0; k < ArrayQ::M; k++)
          SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );


        //--------------
        qElem.evalDerivative( ref, derivqTrue );
        qElem.evalDerivative( point, derivq );

        for (int k = 0; k < ArrayQ::M; k++)
        {
          SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
          SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalDerivative( QuadraturePoint<TopoD2>(ref), derivq );

        for (int k = 0; k < ArrayQ::M; k++)
        {
          SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
          SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
        }

      } // iquad
    } // orderidx


    for (int itrace = 0; itrace < Quad::NTrace; itrace++)
    {
      for (int orientation : {-1, 1})
      {
        CanonicalTraceToCell canonicalTrace(itrace, orientation);

        // relax tolerances for non-canonical traces
        Real small_tol = 1e-12;
        Real close_tol = 1e-13;
        if (orientation == -1)
        {
          small_tol = 1e-11;
          close_tol = 2e-8;
        }

        for (int orderidx = 0; orderidx < QuadratureLine::nOrderIdx; orderidx++)
        {
          QuadratureLine quadrature( QuadratureLine::getOrderFromIndex(orderidx) );

          RefCoordTrace sTrace;
          RefCoordCell refCell;
          QuadratureCellTracePoint<TopoD2> pointCell, pointCell2;
          for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
          {
            quadrature.coordinates(iquad, sTrace);
            QuadraturePoint<TopoD1> pointTrace = quadrature.coordinates_cache(iquad);

            TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalTrace, sTrace, refCell );
            TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalTrace, pointTrace, pointCell );
            TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalTrace, QuadraturePoint<TopoD1>(sTrace), pointCell2 );

            SANS_CHECK_CLOSE( refCell[0], pointCell.ref[0], small_tol, close_tol );
            SANS_CHECK_CLOSE( refCell[1], pointCell.ref[1], small_tol, close_tol );

            //--------------
            qElem.evalBasis( refCell, phiTrue.data(), phiTrue.size() );
            qElem.evalBasis( pointCell, phi.data(), phi.size() );

            for (int k = 0; k < nDOF; k++)
            {
              SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
              phi[k] = 0;
            }

            // 'non-quadrature' coordinate (mainly used with testing of integrands)
            qElem.evalBasis( pointCell2, phi.data(), phi.size() );

            for (int k = 0; k < nDOF; k++)
              SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );

            //--------------
            qElem.evalBasisDerivative( refCell[0], refCell[1], phisTrue.data(), phitTrue.data(), phisTrue.size() );
            qElem.evalBasisDerivative( pointCell, phis.data(), phit.data(), phis.size() );
            qElem.evalBasisDerivative( pointCell, dphi.data(), dphi.size() );

            for (int k = 0; k < nDOF; k++)
            {
              SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
              SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
              SANS_CHECK_CLOSE( phisTrue[k], dphi[k][0], small_tol, close_tol );
              SANS_CHECK_CLOSE( phitTrue[k], dphi[k][1], small_tol, close_tol );
              phis[k] = 0; phit[k] = 0; dphi[k] = 0;
            }

            // 'non-quadrature' coordinate (mainly used with testing of integrands)
            qElem.evalBasisDerivative( pointCell2, phis.data(), phit.data(), phis.size() );

            for (int k = 0; k < nDOF; k++)
            {
              SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
              SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
            }

            //--------------
            qElem.eval( refCell, qTrue );
            qElem.eval( pointCell, q );

            for (int k = 0; k < ArrayQ::M; k++)
              SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );

            // 'non-quadrature' coordinate (mainly used with testing of integrands)
            qElem.eval( pointCell2, q );

            for (int k = 0; k < ArrayQ::M; k++)
              SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );


            //--------------
            qElem.evalDerivative( refCell, derivqTrue );
            qElem.evalDerivative( pointCell, derivq );

            for (int k = 0; k < ArrayQ::M; k++)
            {
              SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
              SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
            }

            // 'non-quadrature' coordinate (mainly used with testing of integrands)
            qElem.evalDerivative( pointCell2, derivq );

            for (int k = 0; k < ArrayQ::M; k++)
            {
              SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
              SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
            }

          } //iquad
        } // orderidx
      } // orientation
    } // itrace

  } // order
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( extract_basis )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD2, Quad > ElementClass;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( 4, qElem.nDOF() );

  const BasisFunctionAreaBase<Quad>* basis = qElem.basis();

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( 4, basis->nBasis() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( edge_sign )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD2, Quad > ElementClass;
  typedef std::array<int,4> Int4;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  Int4 edgeSign = qElem.edgeSign();
  BOOST_CHECK_EQUAL( +1, edgeSign[0] );
  BOOST_CHECK_EQUAL( +1, edgeSign[1] );
  BOOST_CHECK_EQUAL( +1, edgeSign[2] );
  BOOST_CHECK_EQUAL( +1, edgeSign[3] );

  edgeSign[1] = -1;
  qElem.setEdgeSign( edgeSign );

  Int4 edgeSign2 = qElem.edgeSign();
  BOOST_CHECK_EQUAL( +1, edgeSign2[0] );
  BOOST_CHECK_EQUAL( -1, edgeSign2[1] );
  BOOST_CHECK_EQUAL( +1, edgeSign2[2] );
  BOOST_CHECK_EQUAL( +1, edgeSign2[3] );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( projection )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD2, Quad > ElementClass;

  const Real tol = 5e-13;

  for (int order = 1; order < BasisFunctionArea_Quad_HierarchicalPMax; order++)
  {
    for (int inc = 1; inc <= BasisFunctionArea_Quad_HierarchicalPMax-order; inc++)
    {
      ElementClass qElem0( order,       BasisFunctionCategory_Hierarchical );
      ElementClass qElem1( order + inc, BasisFunctionCategory_Hierarchical );

      for (int k = 0; k < qElem0.nDOF(); k++)
      {
        ArrayQ q = {sqrt(k+7), 1./(k+3)};
        qElem0.DOF(k) = q;
      }

      qElem0.projectTo( qElem1 );

      ArrayQ q0, q1;
      Real s, t;

      for (int is = 0; is < 3; is++)
      {
        for (int it = 0; it < 3; it++)
        {
          s = 0.11 + is*0.3357;
          t = 0.13 + it*0.2986;

          qElem0.eval( s, t, q0 );
          qElem1.eval( s, t, q1 );

          for (int n = 0; n < ArrayQ::N; n++)
            BOOST_CHECK_CLOSE( q0[n], q1[n], tol );
        }
      }
    }
  }
#if 0
  for (int order = 1; order < BasisFunctionArea_Quad_LegendrePMax; order++)
  {
    for (int inc = 1; inc <= BasisFunctionArea_Quad_LegendrePMax-order; inc++)
    {
      ElementClass qElem0( order,       BasisFunctionCategory_Legendre );
      ElementClass qElem1( order + inc, BasisFunctionCategory_Legendre );

      for (int k = 0; k < qElem0.nDOF(); k++)
      {
        ArrayQ q = {sqrt(k+7), 1./(k+3)};
        qElem0.DOF(k) = q;
      }

      qElem0.projectTo( qElem1 );

      ArrayQ q0, q1;
      Real s, t;

      for (int is = 0; is < 3; is++)
      {
        for (int it = 0; it < 3; it++)
        {
          s = 0.11 + is*0.3357;
          t = 0.13 + it*0.2986;

          qElem0.eval( s, t, q0 );
          qElem1.eval( s, t, q1 );

          for (int n = 0; n < ArrayQ::N; n++)
            BOOST_CHECK_CLOSE( q0[n], q1[n], tol );
        }
      }
    }
  }
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD2, Quad > ElementClass;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/ElementArea_Quad_pattern.txt", true );

  ElementClass qElem(1, BasisFunctionCategory_Hierarchical);

  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};
  ArrayQ q3 = {5, 6};
  ArrayQ q4 = {7, 8};

  qElem.DOF(0) = q1;
  qElem.DOF(1) = q2;
  qElem.DOF(2) = q3;
  qElem.DOF(3) = q4;

  qElem.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
