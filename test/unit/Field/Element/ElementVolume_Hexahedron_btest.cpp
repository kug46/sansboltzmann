// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementVolume_Hexahedron_btest
// testing of Element< T, TopoD3, Hex >

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <ostream>
#include <memory>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"
#include "Field/Element/ElementVolume.h"
#include "BasisFunction/BasisFunctionVolume_Hexahedron.h"
#include "BasisFunction/TraceToCellRefCoord.h"
#include "Topology/ElementTopology.h"
#include "Quadrature/QuadratureVolume.h"
#include "Quadrature/QuadratureArea.h"


using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class Element<Real, TopoD3, Hex>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementVolume_Hexahedron_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  typedef Element< Real, TopoD3, Hex > ElementClass;

  static_assert( std::is_same<typename ElementClass::BasisType, BasisFunctionVolumeBase<Hex> >::value, "Incorrect Basis type" );
  static_assert( std::is_same<typename ElementClass::TopologyType, Hex>::value, "Incorrect topology type" );
  static_assert( std::is_same<typename ElementClass::RefCoordType, DLA::VectorS<3,Real> >::value, "Incorrect reference coordinate type" );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef Element< Real, TopoD3, Hex > ElementClass;

  int order;

  order = 1;
  ElementClass qElem1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem1.order() );
  BOOST_CHECK_EQUAL( 8, qElem1.nDOF() );
#if 0
  order = 2;
  ElementClass qElem2(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qElem2.order() );
  BOOST_CHECK_EQUAL( 6, qElem2.nDOF() );

  ElementClass qElem3( BasisFunctionAreaBase<Quad>::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, qElem3.order() );
  BOOST_CHECK_EQUAL( 6, qElem3.nDOF() );

  ElementClass qElem4(qElem1);

  BOOST_CHECK_EQUAL( 1, qElem4.order() );
  BOOST_CHECK_EQUAL( 3, qElem4.nDOF() );

  qElem4 = qElem2;

  BOOST_CHECK_EQUAL( 2, qElem4.order() );
  BOOST_CHECK_EQUAL( 6, qElem4.nDOF() );

  ElementClass qElem5;

  qElem5.setBasis( BasisFunctionAreaBase<Quad>::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, qElem5.order() );
  BOOST_CHECK_EQUAL( 6, qElem5.nDOF() );
#endif
  BOOST_CHECK_THROW( ElementClass qElem5(0, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( ElementClass qElem6(BasisFunctionVolume_Hex_HierarchicalPMax+1, BasisFunctionCategory_Hierarchical), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( accessors )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD3, Hex > ElementClass;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( 8, qElem.nDOF() );

  const Real tol = 1e-13;

  ArrayQ qTrue[8] = { {1, 2}, {3, 4}, {5, 6}, {7, 8}, {9, 10}, {11, 12}, {13, 14}, {15, 16} };

  for (int n = 0; n < Hex::NNode; n++)
    qElem.DOF(n) = qTrue[n];

  ArrayQ q;
  for (int n = 0; n < Hex::NNode; n++)
  {
    q = qElem.DOF(n);
    for (int k = 0; k < 2; k++)
      BOOST_CHECK_CLOSE( qTrue[n][k], q[k], tol );
  }


  // Check the vector view version of the DOF
  DLA::VectorDView<ArrayQ> vectorDOF = qElem.vectorViewDOF();

  BOOST_CHECK_EQUAL( qElem.nDOF(), vectorDOF.m() );

  for (int n = 0; n < Hex::NNode; n++)
    vectorDOF[n] = qTrue[Hex::NNode-1-n];

  for (int n = 0; n < Hex::NNode; n++)
  {
    q = qElem.DOF(n);
    for (int k = 0; k < 2; k++)
      BOOST_CHECK_CLOSE( qTrue[Hex::NNode-1-n][k], q[k], tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( eval_basis )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD3, Hex > ElementClass;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  static const int NNode = Hex::NNode;

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( NNode, qElem.nDOF() );

  const Real tol = 1e-13;
  Real sRef, tRef, uRef;
  Real phi[NNode], phi1[NNode], phis[NNode], phit[NNode], phiu[NNode];
  Real phiTrue[NNode], phisTrue[NNode], phitTrue[NNode], phiuTrue[NNode];
  int k;


  sRef = 0;  tRef = 0;  uRef = 0;
  phiTrue[0] = 1;  phiTrue[1] = 0;  phiTrue[2] = 0;  phiTrue[3] = 0;
  phiTrue[4] = 0;  phiTrue[5] = 0;  phiTrue[6] = 0;  phiTrue[7] = 0;

  qElem.evalBasis( sRef, tRef, uRef, phi, NNode );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  // Compute true derivatives using finite difference (which is exact for linear)
  sRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phisTrue[k] = phi1[k] - phi[k];
  sRef -= 1;

  tRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phitTrue[k] = phi1[k] - phi[k];
  tRef -= 1;

  uRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phiuTrue[k] = phi1[k] - phi[k];
  uRef -= 1;

  qElem.evalBasisDerivative( sRef, tRef, uRef, phis, phit, phiu, NNode );
  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
  }


  sRef = 1;  tRef = 0;  uRef = 0;
  phiTrue[0] = 0;  phiTrue[1] = 1;  phiTrue[2] = 0;  phiTrue[3] = 0;
  phiTrue[4] = 0;  phiTrue[5] = 0;  phiTrue[6] = 0;  phiTrue[7] = 0;

  qElem.evalBasis( sRef, tRef, uRef, phi, NNode );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  // Compute true derivatives using finite difference (which is exact for linear)
  sRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phisTrue[k] = phi1[k] - phi[k];
  sRef -= 1;

  tRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phitTrue[k] = phi1[k] - phi[k];
  tRef -= 1;

  uRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phiuTrue[k] = phi1[k] - phi[k];
  uRef -= 1;

  qElem.evalBasisDerivative( sRef, tRef, uRef, phis, phit, phiu, NNode );
  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
  }


  sRef = 1;  tRef = 1;  uRef = 0;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 1;  phiTrue[3] = 0;
  phiTrue[4] = 0;  phiTrue[5] = 0;  phiTrue[6] = 0;  phiTrue[7] = 0;

  qElem.evalBasis( sRef, tRef, uRef, phi, NNode );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  // Compute true derivatives using finite difference (which is exact for linear)
  sRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phisTrue[k] = phi1[k] - phi[k];
  sRef -= 1;

  tRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phitTrue[k] = phi1[k] - phi[k];
  tRef -= 1;

  uRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phiuTrue[k] = phi1[k] - phi[k];
  uRef -= 1;

  qElem.evalBasisDerivative( sRef, tRef, uRef, phis, phit, phiu, NNode );
  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
  }


  sRef = 0;  tRef = 1;  uRef = 0;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 0;  phiTrue[3] = 1;
  phiTrue[4] = 0;  phiTrue[5] = 0;  phiTrue[6] = 0;  phiTrue[7] = 0;

  qElem.evalBasis( sRef, tRef, uRef, phi, NNode );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  // Compute true derivatives using finite difference (which is exact for linear)
  sRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phisTrue[k] = phi1[k] - phi[k];
  sRef -= 1;

  tRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phitTrue[k] = phi1[k] - phi[k];
  tRef -= 1;

  uRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phiuTrue[k] = phi1[k] - phi[k];
  uRef -= 1;

  qElem.evalBasisDerivative( sRef, tRef, uRef, phis, phit, phiu, NNode );
  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
  }


  sRef = 0;  tRef = 0;  uRef = 1;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 0;  phiTrue[3] = 0;
  phiTrue[4] = 1;  phiTrue[5] = 0;  phiTrue[6] = 0;  phiTrue[7] = 0;

  qElem.evalBasis( sRef, tRef, uRef, phi, NNode );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  // Compute true derivatives using finite difference (which is exact for linear)
  sRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phisTrue[k] = phi1[k] - phi[k];
  sRef -= 1;

  tRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phitTrue[k] = phi1[k] - phi[k];
  tRef -= 1;

  uRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phiuTrue[k] = phi1[k] - phi[k];
  uRef -= 1;

  qElem.evalBasisDerivative( sRef, tRef, uRef, phis, phit, phiu, NNode );
  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
  }


  sRef = 1;  tRef = 0;  uRef = 1;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 0;  phiTrue[3] = 0;
  phiTrue[4] = 0;  phiTrue[5] = 1;  phiTrue[6] = 0;  phiTrue[7] = 0;

  qElem.evalBasis( sRef, tRef, uRef, phi, NNode );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  // Compute true derivatives using finite difference (which is exact for linear)
  sRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phisTrue[k] = phi1[k] - phi[k];
  sRef -= 1;

  tRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phitTrue[k] = phi1[k] - phi[k];
  tRef -= 1;

  uRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phiuTrue[k] = phi1[k] - phi[k];
  uRef -= 1;

  qElem.evalBasisDerivative( sRef, tRef, uRef, phis, phit, phiu, NNode );
  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
  }


  sRef = 1;  tRef = 1;  uRef = 1;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 0;  phiTrue[3] = 0;
  phiTrue[4] = 0;  phiTrue[5] = 0;  phiTrue[6] = 1;  phiTrue[7] = 0;

  qElem.evalBasis( sRef, tRef, uRef, phi, NNode );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  // Compute true derivatives using finite difference (which is exact for linear)
  sRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phisTrue[k] = phi1[k] - phi[k];
  sRef -= 1;

  tRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phitTrue[k] = phi1[k] - phi[k];
  tRef -= 1;

  uRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phiuTrue[k] = phi1[k] - phi[k];
  uRef -= 1;

  qElem.evalBasisDerivative( sRef, tRef, uRef, phis, phit, phiu, NNode );
  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
  }


  sRef = 0;  tRef = 1;  uRef = 1;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 0;  phiTrue[3] = 0;
  phiTrue[4] = 0;  phiTrue[5] = 0;  phiTrue[6] = 0;  phiTrue[7] = 1;

  qElem.evalBasis( sRef, tRef, uRef, phi, NNode );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  // Compute true derivatives using finite difference (which is exact for linear)
  sRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phisTrue[k] = phi1[k] - phi[k];
  sRef -= 1;

  tRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phitTrue[k] = phi1[k] - phi[k];
  tRef -= 1;

  uRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phiuTrue[k] = phi1[k] - phi[k];
  uRef -= 1;

  qElem.evalBasisDerivative( sRef, tRef, uRef, phis, phit, phiu, NNode );
  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
  }


  sRef = 1./2.;  tRef = 1./2.;  uRef = 1./2.;
  phiTrue[0] = 1./8.;  phiTrue[1] = 1./8.;  phiTrue[2] = 1./8.;  phiTrue[3] = 1./8.;
  phiTrue[4] = 1./8.;  phiTrue[5] = 1./8.;  phiTrue[6] = 1./8.;  phiTrue[7] = 1./8.;

  qElem.evalBasis( sRef, tRef, uRef, phi, NNode );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  // Compute true derivatives using finite difference (which is exact for linear)
  sRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phisTrue[k] = phi1[k] - phi[k];
  sRef -= 1;

  tRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phitTrue[k] = phi1[k] - phi[k];
  tRef -= 1;

  uRef += 1;
  qElem.evalBasis( sRef, tRef, uRef, phi1, NNode );
  for (k = 0; k < NNode; k++) phiuTrue[k] = phi1[k] - phi[k];
  uRef -= 1;

  qElem.evalBasisDerivative( sRef, tRef, uRef, phis, phit, phiu, NNode );
  for (k = 0; k < NNode; k++)
  {
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( eval_variable )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD3, Hex > ElementClass;

  static const int NNode = Hex::NNode;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( NNode, qElem.nDOF() );

  const Real tol = 1e-13;
  Real sRef, tRef, uRef;
  Real phi[NNode];
  Real phiTrue[NNode];
  ArrayQ q, qTrue;
  int k;

  ArrayQ qs[8] = { {1, 2}, {3, 4}, {5, 6}, {7, 8}, {9, 10}, {11, 12}, {13, 14}, {15, 16} };

  for (int n = 0; n < Hex::NNode; n++)
    qElem.DOF(n) = qs[n];

  sRef = 0;  tRef = 0;  uRef = 0;
  phiTrue[0] = 1;  phiTrue[1] = 0;  phiTrue[2] = 0;  phiTrue[3] = 0;
  phiTrue[4] = 0;  phiTrue[5] = 0;  phiTrue[6] = 0;  phiTrue[7] = 0;
  qTrue = qs[0];

  qElem.evalBasis( sRef, tRef, uRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );


  sRef = 1;  tRef = 0;  uRef = 0;
  phiTrue[0] = 0;  phiTrue[1] = 1;  phiTrue[2] = 0;  phiTrue[3] = 0;
  phiTrue[4] = 0;  phiTrue[5] = 0;  phiTrue[6] = 0;  phiTrue[7] = 0;
  qTrue = qs[1];

  qElem.evalBasis( sRef, tRef, uRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );

  q = 0;
  qElem.eval( sRef, tRef, uRef, q );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );


  q = 0; // cppcheck-suppress redundantAssignment
  q = qElem.eval( sRef, tRef, uRef );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );


  sRef = 1;  tRef = 1;  uRef = 0;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 1;  phiTrue[3] = 0;
  phiTrue[4] = 0;  phiTrue[5] = 0;  phiTrue[6] = 0;  phiTrue[7] = 0;
  qTrue = qs[2];

  qElem.evalBasis( sRef, tRef, uRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );

  q = 0;
  qElem.eval( sRef, tRef, uRef, q );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );


  sRef = 0;  tRef = 1;  uRef = 0;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 0;  phiTrue[3] = 1;
  phiTrue[4] = 0;  phiTrue[5] = 0;  phiTrue[6] = 0;  phiTrue[7] = 0;
  qTrue = qs[3];

  qElem.evalBasis( sRef, tRef, uRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );

  q = 0;
  qElem.eval( sRef, tRef, uRef, q );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );


  sRef = 0;  tRef = 0;  uRef = 1;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 0;  phiTrue[3] = 0;
  phiTrue[4] = 1;  phiTrue[5] = 0;  phiTrue[6] = 0;  phiTrue[7] = 0;
  qTrue = qs[4];

  qElem.evalBasis( sRef, tRef, uRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );

  q = 0;
  qElem.eval( sRef, tRef, uRef, q );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );


  sRef = 1;  tRef = 0;  uRef = 1;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 0;  phiTrue[3] = 0;
  phiTrue[4] = 0;  phiTrue[5] = 1;  phiTrue[6] = 0;  phiTrue[7] = 0;
  qTrue = qs[5];

  qElem.evalBasis( sRef, tRef, uRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );

  q = 0;
  qElem.eval( sRef, tRef, uRef, q );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );


  sRef = 1;  tRef = 1;  uRef = 1;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 0;  phiTrue[3] = 0;
  phiTrue[4] = 0;  phiTrue[5] = 0;  phiTrue[6] = 1;  phiTrue[7] = 0;
  qTrue = qs[6];

  qElem.evalBasis( sRef, tRef, uRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );

  q = 0;
  qElem.eval( sRef, tRef, uRef, q );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );


  sRef = 0;  tRef = 1;  uRef = 1;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 0;  phiTrue[3] = 0;
  phiTrue[4] = 0;  phiTrue[5] = 0;  phiTrue[6] = 0;  phiTrue[7] = 1;
  qTrue = qs[7];

  qElem.evalBasis( sRef, tRef, uRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );

  q = 0;
  qElem.eval( sRef, tRef, uRef, q );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );



  sRef = 1./2.;  tRef = 1./2.;  uRef = 1./2.;
  phiTrue[0] = 1./8.;  phiTrue[1] = 1./8.;  phiTrue[2] = 1./8.;  phiTrue[3] = 1./8.;
  phiTrue[4] = 1./8.;  phiTrue[5] = 1./8.;  phiTrue[6] = 1./8.;  phiTrue[7] = 1./8.;
  qTrue = 0;
  for (k = 0; k < NNode; k++)
    qTrue += qs[k]/8.;

  qElem.evalBasis( sRef, tRef, uRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );

  q = 0;
  qElem.eval( sRef, tRef, uRef, q );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( extract_basis )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD3, Hex > ElementClass;

  static const int NNode = Hex::NNode;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( NNode, qElem.nDOF() );

  const BasisFunctionVolumeBase<Hex>* basis = qElem.basis();

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( NNode, basis->nBasis() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( edge_sign )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD3, Hex > ElementClass;
  typedef std::array<int,6> Int6;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  Int6 faceSign = qElem.faceSign();
  BOOST_CHECK_EQUAL( +1, faceSign[0] );
  BOOST_CHECK_EQUAL( +1, faceSign[1] );
  BOOST_CHECK_EQUAL( +1, faceSign[2] );
  BOOST_CHECK_EQUAL( +1, faceSign[3] );
  BOOST_CHECK_EQUAL( +1, faceSign[4] );
  BOOST_CHECK_EQUAL( +1, faceSign[5] );

  faceSign[1] = -1;
  qElem.setFaceSign( faceSign );

  Int6 faceSign2 = qElem.faceSign();
  BOOST_CHECK_EQUAL( +1, faceSign2[0] );
  BOOST_CHECK_EQUAL( -1, faceSign2[1] );
  BOOST_CHECK_EQUAL( +1, faceSign2[2] );
  BOOST_CHECK_EQUAL( +1, faceSign2[3] );
  BOOST_CHECK_EQUAL( +1, faceSign2[4] );
  BOOST_CHECK_EQUAL( +1, faceSign2[5] );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( projection )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD3, Hex > ElementClass;

  const Real tol = 5e-13;

  for (int order = 1; order < BasisFunctionVolume_Hex_HierarchicalPMax; order++)
  {
    for (int inc = 1; inc <= BasisFunctionVolume_Hex_HierarchicalPMax-order; inc++)
    {
      ElementClass qElem0( order,       BasisFunctionCategory_Hierarchical );
      ElementClass qElem1( order + inc, BasisFunctionCategory_Hierarchical );

      for (int k = 0; k < qElem0.nDOF(); k++)
      {
        ArrayQ q = {sqrt(k+7), 1./(k+3)};
        qElem0.DOF(k) = q;
      }

      qElem0.projectTo( qElem1 );

      ArrayQ q0, q1;
      Real s, t, u;

      for (int is = 0; is < 3; is++)
      {
        for (int it = 0; it < 3; it++)
        {
          for (int iu = 0; iu < 3; iu++)
          {
            s = 0.11 + is*0.3357;
            t = 0.13 + it*0.2986;
            u = 0.15 + iu*0.2629;

            qElem0.eval( s, t, u, q0 );
            qElem1.eval( s, t, u, q1 );

            for (int n = 0; n < ArrayQ::N; n++)
              BOOST_CHECK_CLOSE( q0[n], q1[n], tol );
          }
        }
      }
    }
  }

  for (int order = 1; order < BasisFunctionVolume_Hex_LegendrePMax; order++)
  {
    for (int inc = 1; inc <= BasisFunctionVolume_Hex_LegendrePMax-order; inc++)
    {
      ElementClass qElem0( order,       BasisFunctionCategory_Legendre );
      ElementClass qElem1( order + inc, BasisFunctionCategory_Legendre );

      for (int k = 0; k < qElem0.nDOF(); k++)
      {
        ArrayQ q = {sqrt(k+7), 1./(k+3)};
        qElem0.DOF(k) = q;
      }

      qElem0.projectTo( qElem1 );

      ArrayQ q0, q1;
      Real s, t, u;

      for (int is = 0; is < 3; is++)
      {
        for (int it = 0; it < 3; it++)
        {
          for (int iu = 0; iu < 3; iu++)
          {
            s = 0.11 + is*0.3357;
            t = 0.13 + it*0.2986;
            u = 0.15 + iu*0.2629;

            qElem0.eval( s, t, u, q0 );
            qElem1.eval( s, t, u, q1 );

            for (int n = 0; n < ArrayQ::N; n++)
              BOOST_CHECK_CLOSE( q0[n], q1[n], tol );
          }
        }
      }
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD3, Hex > ElementClass;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/ElementVolume_Hexahedron_pattern.txt", true );

  ElementClass qElem(1, BasisFunctionCategory_Hierarchical);

  ArrayQ q[8] = { {1, 2}, {3, 4}, {5, 6}, {7, 8}, {9, 10}, {11, 12}, {13, 14}, {15, 16} };

  for (int n = 0; n < Hex::NNode; n++)
    qElem.DOF(n) = q[n];

  qElem.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
