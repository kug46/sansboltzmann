// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementXFieldLine_btest
//   testing of ElementXFieldLineBase class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "tools/stringify.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "Field/Element/ElementXFieldLine.h"

using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class ElementXField<PhysD1, TopoD1, Line>;
//template class ElementXField<PhysD2, TopoD1, Line>;
template class ElementXField<PhysD3, TopoD1, Line>;
template class ElementXField<PhysD4, TopoD1, Line>;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementXFieldLineBase_test_suite )

typedef boost::mpl::list< PhysD1, PhysD2, PhysD3, PhysD4 > Dimensions;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( static_test, PhysDim, Dimensions )
{
  typedef ElementXField<PhysDim, TopoD1, Line> ElementXFieldLine;
  BOOST_CHECK( ElementXFieldLine::D == PhysDim::D );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctors_test, PhysDim, Dimensions )
{
  int order;

  order = 1;
  ElementXField<PhysDim, TopoD1, Line> xElem1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem1.order() );
  BOOST_CHECK_EQUAL( 2, xElem1.nDOF() );

  order = 2;
  ElementXField<PhysDim, TopoD1, Line> xElem2(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, xElem2.order() );
  BOOST_CHECK_EQUAL( 3, xElem2.nDOF() );

  ElementXField<PhysDim, TopoD1, Line> xElem3( BasisFunctionLineBase::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, xElem3.order() );
  BOOST_CHECK_EQUAL( 3, xElem3.nDOF() );

  ElementXField<PhysDim, TopoD1, Line> xElem4(xElem1);

  BOOST_CHECK_EQUAL( 1, xElem4.order() );
  BOOST_CHECK_EQUAL( 2, xElem4.nDOF() );

  xElem4 = xElem2;

  BOOST_CHECK_EQUAL( 2, xElem4.order() );
  BOOST_CHECK_EQUAL( 3, xElem4.nDOF() );

  typedef ElementXField<PhysDim, TopoD1, Line> ElementXFieldLine;
  BOOST_CHECK_THROW( ElementXFieldLine xElem5(0, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( ElementXFieldLine xElem6(8, BasisFunctionCategory_Hierarchical), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( accessors_test, PhysDim, Dimensions )
{
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysDim, TopoD1, Line> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 2, xElem.nDOF() );

  typename ElementXField<PhysDim, TopoD1, Line>::VectorX X1, X2;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    X1[i] = i+1;
    X2[i] = 2*i;
  }

  xElem.DOF(0) = X1;
  xElem.DOF(1) = X2;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    BOOST_CHECK_CLOSE( X1[i], xElem.DOF(0)[i], tol );
    BOOST_CHECK_CLOSE( X2[i], xElem.DOF(1)[i], tol );
  }

  ElementXField<PhysDim, TopoD1, Line> xElem2(xElem);

  BOOST_CHECK_EQUAL( 1, xElem2.order() );
  BOOST_CHECK_EQUAL( 2, xElem2.nDOF() );

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    BOOST_CHECK_CLOSE( X1[i], xElem2.DOF(0)[i], tol );
    BOOST_CHECK_CLOSE( X2[i], xElem2.DOF(1)[i], tol );
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fcns_test, PhysDim, Dimensions )
{
  const Real small_tol = 1e-13;
  const Real tol = 1e-13;

  typedef typename ElementXField<PhysDim, TopoD1, Line>::VectorX VectorX;

  int order = 1;
  ElementXField<PhysDim, TopoD1, Line> xElem(order, BasisFunctionCategory_Hierarchical);

  const ElementXField<PhysDim, TopoD1, Line>& cxElem = xElem;

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 2, xElem.nDOF() );

  Real sRef;
  VectorX X1, X2;
  VectorX X, XTrue;
  Real length, lengthTrue, jaceval;

  X1 = 0;
  X2 = 1;
  lengthTrue = sqrt(Real(PhysDim::D));

  xElem.DOF(0) = X1;
  xElem.DOF(1) = X2;

  //Check the non-const accessor versions
  X = xElem.DOF(0);
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( X1[i], X[i], small_tol, tol );

  X = xElem.DOF(1);
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( X2[i], X[i], small_tol, tol );

  length = xElem.length();
  BOOST_CHECK_CLOSE( lengthTrue, length, tol );
  jaceval= xElem.jacobianDeterminant();
  BOOST_CHECK_CLOSE( lengthTrue, jaceval,tol );

  //Check the const accessor versions
  X = cxElem.DOF(0);
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( X1[i], X[i], small_tol, tol );

  X = cxElem.DOF(1);
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( X2[i], X[i], small_tol, tol );


  sRef = 0;
  XTrue = X1;
  xElem.coordinates( sRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( XTrue[i], X[i], small_tol, tol );

  length = xElem.length( sRef );
  BOOST_CHECK_CLOSE( lengthTrue, length, tol );

  sRef = 1;
  XTrue = X2;
  xElem.coordinates( sRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( XTrue[i], X[i], small_tol, tol );

  X = xElem.coordinates( sRef );
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( XTrue[i], X[i], small_tol, tol );

  length = xElem.length( sRef );
  BOOST_CHECK_CLOSE( lengthTrue, length, tol );

  sRef = 1./3.;
  XTrue = (2*X1 + X2)/3.;
  xElem.coordinates( sRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( XTrue[i], X[i], small_tol, tol );

  X = xElem.coordinates( sRef );
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( XTrue[i], X[i], small_tol, tol );

  length = xElem.length( sRef );
  BOOST_CHECK_CLOSE( lengthTrue, length, tol );

  typename ElementXField<PhysDim, TopoD1, Line>::RefCoordType Ref = sRef;
  VectorX Xs;
  VectorX XsTrue = X2 - X1;

  xElem.tangent( sRef, Xs );
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( XsTrue[i], Xs[i], small_tol, tol );

  Xs = -1;
  xElem.tangent( Ref, Xs );
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( XsTrue[i], Xs[i], small_tol, tol );


  XsTrue /= sqrt(dot(XsTrue,XsTrue));

  xElem.unitTangent( sRef, Xs );
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( XsTrue[i], Xs[i], small_tol, tol );

  Xs = -1;
  xElem.unitTangent( Ref, Xs );
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( XsTrue[i], Xs[i], small_tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( eval_coordinate_test, PhysDim, Dimensions )
{
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysDim,TopoD1,Line> xElem(order, BasisFunctionCategory_Hierarchical);

  static const int NNode = Line::NNode;

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( NNode, xElem.nDOF() );

  Real sRef;
  typename ElementXField<PhysDim,TopoD1,Line>::VectorX X1, X2;
  typename ElementXField<PhysDim,TopoD1,Line>::VectorX X, Xd, sgrad;
  typename ElementXField<PhysDim,TopoD1,Line>::VectorX XTrue, sgradTrue;

  //Create a quad in the x-y plane for 2D, and rotated about the x-axis 45 degrees for 3D
  X1 = 0;
  X2 = 2.1;

  xElem.DOF(0) = X1;
  xElem.DOF(1) = X2;

  //------
  sRef = 0;
  XTrue = X1;

  xElem.eval( sRef, X );
  xElem.evalReferenceCoordinateGradient( sRef, sgrad );

  // Compute true derivatives using finite difference (which is exact for linear)
  sRef += 1;
  xElem.eval( sRef, Xd );
  for ( int i = 0; i < PhysDim::D; i++ )
    sgradTrue[i] = (1./PhysDim::D)/(Xd[i] - X[i]);
  sRef -= 1;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    SANS_CHECK_CLOSE( XTrue[i], X[i], tol, tol );
    SANS_CHECK_CLOSE( sgradTrue[i], sgrad[i], tol, tol );
  }

  //------
  sRef = 1;
  XTrue = X2;

  xElem.eval( sRef, X );
  xElem.evalReferenceCoordinateGradient( sRef, sgrad );

  // Compute true derivatives using finite difference (which is exact for linear)
  sRef += 1;
  xElem.eval( sRef, Xd );
  for ( int i = 0; i < PhysDim::D; i++ )
    sgradTrue[i] = (1./PhysDim::D)/(Xd[i] - X[i]);
  sRef -= 1;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    SANS_CHECK_CLOSE( XTrue[i], X[i], tol, tol );
    SANS_CHECK_CLOSE( sgradTrue[i], sgrad[i], tol, tol );
  }

  //------
  sRef = 1./2.;
  XTrue = (X1 + X2)/2.;

  xElem.eval( sRef, X );
  xElem.evalReferenceCoordinateGradient( sRef, sgrad );

  // Compute true derivatives using finite difference (which is exact for linear)
  sRef += 1;
  xElem.eval( sRef, Xd );
  for ( int i = 0; i < PhysDim::D; i++ )
    sgradTrue[i] = (1./PhysDim::D)/(Xd[i] - X[i]);
  sRef -= 1;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    SANS_CHECK_CLOSE( XTrue[i], X[i], tol, tol );
    SANS_CHECK_CLOSE( sgradTrue[i], sgrad[i], tol, tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( extract_basis_test, PhysDim, Dimensions )
{
  int order = 1;
  ElementXField<PhysDim, TopoD1, Line> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 2, xElem.nDOF() );

  const BasisFunctionLineBase* basis = xElem.basis();

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( 2, basis->nBasis() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( IO_test, PhysDim, Dimensions )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/ElementXField" + stringify((int)PhysDim::D) + "DLine_pattern.txt", true );

  ElementXField<PhysDim, TopoD1, Line> xElem(1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 2, xElem.nDOF() );

  typename ElementXField<PhysDim, TopoD1, Line>::VectorX X1, X2;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    X1[i] = i+1;
    X2[i] = 2*i;
  }

  xElem.DOF(0) = X1;
  xElem.DOF(1) = X2;

  xElem.dump( 2, output );
  xElem.dumpTecplot( output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
