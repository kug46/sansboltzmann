// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementXFieldLine_2D_btest
//   testing of ElementXFieldLine class 2D specialization

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/stringify.h"
#include "Field/Element/ElementXFieldLine.h"

using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementXFieldLine_2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BoundingBox_Q2_test )
{
  int order = 2;
  ElementXField<PhysD2,TopoD1,Line> xElem(order, BasisFunctionCategory_Lagrange);

  // node DOFs
  xElem.DOF(0) = {0,0};
  xElem.DOF(1) = {1,0};

  // edge DOF
  xElem.DOF(2) = {0.5,0.5};

  BoundingBox<PhysD2> bbox = xElem.boundingBox();

  BOOST_CHECK_EQUAL(0, bbox.low_bounds()[0]);
  BOOST_CHECK_EQUAL(0, bbox.low_bounds()[1]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[0]);
  BOOST_CHECK_EQUAL(0.5, bbox.high_bounds()[1]);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( unitNormal_Hierarchical_Q1_test )
{
  const Real small_tol = 1e-13;
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysD2, TopoD1, Line> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_REQUIRE_EQUAL( 2, xElem.nDOF() );

  Real sRef;
  Real nx, ny;
  Real nxTrue, nyTrue;

  xElem.DOF(0) = {0, 0};
  xElem.DOF(1) = {1, 0};

  nxTrue =  0;
  nyTrue = -1;

  sRef = 0;
  xElem.unitNormal( sRef, nx, ny );
  SANS_CHECK_CLOSE( nxTrue, nx, small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, ny, small_tol, tol );

  sRef = 1;
  xElem.unitNormal( sRef, nx, ny );
  SANS_CHECK_CLOSE( nxTrue, nx, small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, ny, small_tol, tol );

  xElem.DOF(0) = {1, 0};
  xElem.DOF(1) = {0, 1};

  nxTrue = 1./sqrt(2);
  nyTrue = 1./sqrt(2);

  sRef = 0;
  xElem.unitNormal( sRef, nx, ny );
  BOOST_CHECK_CLOSE( nxTrue, nx, tol );
  BOOST_CHECK_CLOSE( nyTrue, ny, tol );

  sRef = 1;
  xElem.unitNormal( sRef, nx, ny );
  BOOST_CHECK_CLOSE( nxTrue, nx, tol );
  BOOST_CHECK_CLOSE( nyTrue, ny, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( unitNormal_Lagrange_Q1_test )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  int order = 1;
  ElementXField<PhysD2, TopoD1, Line> xElem(order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_REQUIRE_EQUAL( 2, xElem.nDOF() );

  Real sRef;
  Real nx, ny;
  Real nxTrue, nyTrue;

  xElem.DOF(0) = {0, 0};
  xElem.DOF(1) = {1, 0};

  nxTrue =  0;
  nyTrue = -1;

  sRef = 0;
  xElem.unitNormal( sRef, nx, ny );
  SANS_CHECK_CLOSE( nxTrue, nx, small_tol, close_tol );
  SANS_CHECK_CLOSE( nyTrue, ny, small_tol, close_tol );

  sRef = 1;
  xElem.unitNormal( sRef, nx, ny );
  SANS_CHECK_CLOSE( nxTrue, nx, small_tol, close_tol );
  SANS_CHECK_CLOSE( nyTrue, ny, small_tol, close_tol );

  xElem.DOF(0) = {1, 0};
  xElem.DOF(1) = {0, 1};

  nxTrue = 1./sqrt(2);
  nyTrue = 1./sqrt(2);

  sRef = 0;
  xElem.unitNormal( sRef, nx, ny );
  SANS_CHECK_CLOSE( nxTrue, nx, small_tol, close_tol );
  SANS_CHECK_CLOSE( nyTrue, ny, small_tol, close_tol );

  sRef = 1;
  xElem.unitNormal( sRef, nx, ny );
  SANS_CHECK_CLOSE( nxTrue, nx, small_tol, close_tol );
  SANS_CHECK_CLOSE( nyTrue, ny, small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( unitNormal_Lagrange_Q2_test )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  int order = 2;
  ElementXField<PhysD2, TopoD1, Line> xElem(order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL( 2, xElem.order() );
  BOOST_REQUIRE_EQUAL( 3, xElem.nDOF() );

  Real sRef;
  Real nx, ny;
  Real nxTrue, nyTrue;

  xElem.DOF(0) = {0, 0};
  xElem.DOF(1) = {1, 0};
  xElem.DOF(2) = {1./2., 0};

  nxTrue =  0;
  nyTrue = -1;

  for (int i = 0; i < 5; i++)
  {
    sRef = i/Real(4);
    xElem.unitNormal( sRef, nx, ny );
    SANS_CHECK_CLOSE( nxTrue, nx, small_tol, close_tol );
    SANS_CHECK_CLOSE( nyTrue, ny, small_tol, close_tol );
  }

  xElem.DOF(0) = {1, 0};
  xElem.DOF(1) = {0, 1};
  xElem.DOF(2) = {1./2., 1./2.};

  nxTrue = 1./sqrt(2);
  nyTrue = 1./sqrt(2);

  for (int i = 0; i < 5; i++)
  {
    sRef = i/Real(4);
    xElem.unitNormal( sRef, nx, ny );
    SANS_CHECK_CLOSE( nxTrue, nx, small_tol, close_tol );
    SANS_CHECK_CLOSE( nyTrue, ny, small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( unitNormal_Lagrange_Q3_test )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  int order = 3;
  ElementXField<PhysD2, TopoD1, Line> xElem(order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL( 3, xElem.order() );
  BOOST_REQUIRE_EQUAL( 4, xElem.nDOF() );

  Real sRef;
  Real nx, ny;
  Real nxTrue, nyTrue;

  xElem.DOF(0) = {0, 0};
  xElem.DOF(1) = {1, 0};
  xElem.DOF(2) = {1./3., 0};
  xElem.DOF(3) = {2./3., 0};

  nxTrue =  0;
  nyTrue = -1;

  for (int i = 0; i < 5; i++)
  {
    sRef = i/Real(4);
    xElem.unitNormal( sRef, nx, ny );
    SANS_CHECK_CLOSE( nxTrue, nx, small_tol, close_tol );
    SANS_CHECK_CLOSE( nyTrue, ny, small_tol, close_tol );
  }

  xElem.DOF(0) = {1, 0};
  xElem.DOF(1) = {0, 1};
  xElem.DOF(2) = {2./3., 1./3.};
  xElem.DOF(3) = {1./3., 2./3.};

  nxTrue = 1./sqrt(2);
  nyTrue = 1./sqrt(2);

  for (int i = 0; i < 5; i++)
  {
    sRef = i/Real(4);
    xElem.unitNormal( sRef, nx, ny );
    SANS_CHECK_CLOSE( nxTrue, nx, small_tol, close_tol );
    SANS_CHECK_CLOSE( nyTrue, ny, small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( evalUnitTangentGradient_Q1_test )
{
  typedef ElementXField<PhysD2, TopoD1, Line> ElemXFieldType;
  typedef typename ElemXFieldType::VectorX VectorX;

  const Real small_tol = 1e-13;
  const Real tol = 1e-13;

  int order = 1; // linear element
  ElemXFieldType xElem(order, BasisFunctionCategory_Hierarchical);
  int nDOF = order + 1;

  BOOST_CHECK_EQUAL( order, xElem.order() );
  BOOST_CHECK_EQUAL( nDOF, xElem.nDOF() );

  Real sRef;
  VectorX exgrad, eygrad;
  VectorX exgradTrue, eygradTrue;

  BOOST_REQUIRE_EQUAL( 2, xElem.nDOF() );
  xElem.DOF(0) = {0, 0};
  xElem.DOF(1) = {2, 1};

  // check
  sRef = 0.;
  xElem.evalUnitTangentGradient( sRef, exgrad, eygrad );

  exgradTrue = { 0., 0. };
  SANS_CHECK_CLOSE( exgrad[0], exgradTrue[0], small_tol, tol );
  SANS_CHECK_CLOSE( exgrad[1], exgradTrue[1], small_tol, tol );

  eygradTrue = { 0., 0. };
  SANS_CHECK_CLOSE( eygrad[0], eygradTrue[0], small_tol, tol );
  SANS_CHECK_CLOSE( eygrad[1], eygradTrue[1], small_tol, tol );


  // check
  sRef = 0.7;
  xElem.evalUnitTangentGradient( sRef, exgrad, eygrad );

  exgradTrue = { 0., 0. };
  SANS_CHECK_CLOSE( exgrad[0], exgradTrue[0], small_tol, tol );
  SANS_CHECK_CLOSE( exgrad[1], exgradTrue[1], small_tol, tol );

  eygradTrue = { 0., 0. };
  SANS_CHECK_CLOSE( eygrad[0], eygradTrue[0], small_tol, tol );
  SANS_CHECK_CLOSE( eygrad[1], eygradTrue[1], small_tol, tol );

  // check
  sRef = 1.;
  xElem.evalUnitTangentGradient( sRef, exgrad, eygrad );

  exgradTrue = { 0., 0. };
  SANS_CHECK_CLOSE( exgrad[0], exgradTrue[0], small_tol, tol );
  SANS_CHECK_CLOSE( exgrad[1], exgradTrue[1], small_tol, tol );

  eygradTrue = { 0., 0. };
  SANS_CHECK_CLOSE( eygrad[0], eygradTrue[0], small_tol, tol );
  SANS_CHECK_CLOSE( eygrad[1], eygradTrue[1], small_tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( evalUnitTangentGradient_Q2_test )
{
  typedef ElementXField<PhysD2, TopoD1, Line> ElemXFieldType;
  typedef typename ElemXFieldType::VectorX VectorX;

  const Real small_tol = 1e-13;
  const Real tol = 1e-12;

  int order = 2; // linear element
  ElemXFieldType xElem(order, BasisFunctionCategory_Hierarchical);
  int nDOF = order + 1;

  BOOST_CHECK_EQUAL( order, xElem.order() );
  BOOST_CHECK_EQUAL( nDOF, xElem.nDOF() );

  Real sRef;
  VectorX exgrad, eygrad;
  VectorX exgradTrue, eygradTrue;

  BOOST_REQUIRE_EQUAL( 3, xElem.nDOF() );
  xElem.DOF(0) = {0, 0};
  xElem.DOF(1) = {1, 1};
  xElem.DOF(2) = {2, 3};

  // check
  sRef = 0.;
  xElem.evalUnitTangentGradient( sRef, exgrad, eygrad );

  exgradTrue = { (117.*sqrt(2./5.))/78125.,
                 (-81.*sqrt(2./5.))/78125.};
  SANS_CHECK_CLOSE( exgrad[0], exgradTrue[0], small_tol, tol );
  SANS_CHECK_CLOSE( exgrad[1], exgradTrue[1], small_tol, tol );

  eygradTrue = { ( 169.*sqrt(2./5.))/78125.,
                 (-117.*sqrt(2./5.))/78125. };
  SANS_CHECK_CLOSE( eygrad[0], eygradTrue[0], small_tol, tol );
  SANS_CHECK_CLOSE( eygrad[1], eygradTrue[1], small_tol, tol );

  // check
  sRef = 0.7;
  xElem.evalUnitTangentGradient( sRef, exgrad, eygrad );

  exgradTrue = {  0.04097591785735607,
                 -0.0237228998121535 };
  SANS_CHECK_CLOSE( exgrad[0], exgradTrue[0], small_tol, tol );
  SANS_CHECK_CLOSE( exgrad[1], exgradTrue[1], small_tol, tol );

  eygradTrue = {  0.07077658538997865,
                 -0.04097591785735602 };
  SANS_CHECK_CLOSE( eygrad[0], eygradTrue[0], small_tol, tol );
  SANS_CHECK_CLOSE( eygrad[1], eygradTrue[1], small_tol, tol );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
