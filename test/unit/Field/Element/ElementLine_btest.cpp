// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementLine_btest
// testing of Element class with Line specializiation

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "Field/Element/ElementLine.h"
#include "Quadrature/QuadratureLine.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class Element< Real, TopoD1, Line >;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementLine_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef Element< Real, TopoD1, Line > ElementClass;

  static_assert( std::is_same<typename ElementClass::BasisType, BasisFunctionLineBase >::value, "Incorrect Basis type" );
  static_assert( std::is_same<typename ElementClass::TopologyType, Line>::value, "Incorrect topology type" );
  static_assert( std::is_same<typename ElementClass::RefCoordType, DLA::VectorS<1,Real> >::value, "Incorrect reference coordinate type" );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef Element< Real, TopoD1, Line > ElementClass;

  int order;

  order = 1;
  ElementClass elem1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, elem1.order() );
  BOOST_CHECK_EQUAL( 2, elem1.nDOF() );

  order = 2;
  ElementClass elem2(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, elem2.order() );
  BOOST_CHECK_EQUAL( 3, elem2.nDOF() );

  ElementClass elem3( BasisFunctionLineBase::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, elem3.order() );
  BOOST_CHECK_EQUAL( 3, elem3.nDOF() );

  ElementClass elem4(elem1);

  BOOST_CHECK_EQUAL( 1, elem4.order() );
  BOOST_CHECK_EQUAL( 2, elem4.nDOF() );

  elem4 = elem2;

  BOOST_CHECK_EQUAL( 2, elem4.order() );
  BOOST_CHECK_EQUAL( 3, elem4.nDOF() );

  BOOST_CHECK_THROW( ElementClass elem5(0, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( ElementClass elem6(BasisFunctionLine_HierarchicalPMax+1, BasisFunctionCategory_Hierarchical), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( accessors )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD1, Line > ElementClass;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( 2, qElem.nDOF() );

  const Real tol = 1e-12;

  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};

  qElem.DOF(0) = q1;
  qElem.DOF(1) = q2;

  ArrayQ q;
  for (int k = 0; k < 2; k++)
  {
    q = qElem.DOF(0);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );
    q = qElem.DOF(1);
    BOOST_CHECK_CLOSE( q2[k], q[k], tol );
  }

  // Check the vector view version of the DOF
  DLA::VectorDView<ArrayQ> vectorDOF = qElem.vectorViewDOF();

  BOOST_CHECK_EQUAL( qElem.nDOF(), vectorDOF.m() );

  vectorDOF[0] = q2;
  vectorDOF[1] = q1;

  for (int k = 0; k < 2; k++)
  {
    q = qElem.DOF(0);
    BOOST_CHECK_CLOSE( q2[k], q[k], tol );
    q = qElem.DOF(1);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( eval_basis )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD1, Line > ElementClass;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( 2, qElem.nDOF() );

  const Real tol = 1e-13;
  Real sRef;
  Real phi[2], phis[2], phiss[2];
  const Real *phi2, *phis2, *phiss2;
  Real phiTrue[2], phisTrue[2], phissTrue[2];
  int k;

  sRef = 0;
  phiTrue[0] = 1;  phiTrue[1] = 0;
  phisTrue[0] = -1;  phisTrue[1] = 1;
  phissTrue[0] = 0; phissTrue[1] = 0; // This isn't very good TODO: add a p2 test

  qElem.evalBasis( sRef, phi, 2 );
  qElem.evalBasisDerivative( sRef, phis, 2 );
  qElem.evalBasisHessianDerivative( sRef, phiss, 2 );
  qElem.evalBasis( sRef, &phi2 );
  qElem.evalBasisDerivative( sRef, &phis2 );
  qElem.evalBasisHessianDerivative( sRef, &phiss2 );
  for (k = 0; k < 2; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );

    BOOST_CHECK_CLOSE( phiTrue[k], phi2[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis2[k], tol );
    BOOST_CHECK_CLOSE( phissTrue[k], phiss2[k], tol );
  }

  sRef = 1;
  phiTrue[0] = 0;  phiTrue[1] = 1;
  phisTrue[0] = -1;  phisTrue[1] = 1;

  qElem.evalBasis( sRef, phi, 2 );
  qElem.evalBasisDerivative( sRef, phis, 2 );
  qElem.evalBasisHessianDerivative( sRef, phiss, 2 );
  qElem.evalBasis( sRef, &phi2 );
  qElem.evalBasisDerivative( sRef, &phis2 );
  qElem.evalBasisHessianDerivative( sRef, &phiss2 );
  for (k = 0; k < 2; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );

    BOOST_CHECK_CLOSE( phiTrue[k], phi2[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis2[k], tol );
    BOOST_CHECK_CLOSE( phissTrue[k], phiss2[k], tol );
  }

  sRef = 0.5;
  phiTrue[0] = 0.5;  phiTrue[1] = 0.5;
  phisTrue[0] = -1;  phisTrue[1] = 1;

  qElem.evalBasis( sRef, phi, 2 );
  qElem.evalBasisDerivative( sRef, phis, 2 );
  qElem.evalBasisHessianDerivative( sRef, phiss, 2 );
  qElem.evalBasis( sRef, &phi2 );
  qElem.evalBasisDerivative( sRef, &phis2 );
  qElem.evalBasisHessianDerivative( sRef, &phiss2 );
  for (k = 0; k < 2; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );

    BOOST_CHECK_CLOSE( phiTrue[k], phi2[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis2[k], tol );
    BOOST_CHECK_CLOSE( phissTrue[k], phiss2[k], tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( eval_basis_hessian )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD1, Line > ElementClass;

  int order = 3;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 3, qElem.order() );
  BOOST_CHECK_EQUAL( 4, qElem.nDOF() );

  const Real tol = 1e-13;
  Real sRef;
  Real phiss[4];
  Real phissTrue[4];
  int k;

  sRef = 0;
  phissTrue[0] = 0; phissTrue[1] = 0; phissTrue[2] = -8; phissTrue[3] = 36*sqrt(3)*(2*sRef-1); // phissTrue[3] = 36*sqrt(3)*(2*s-1);

  qElem.evalBasisHessianDerivative( sRef, phiss, 4 );
  for (k = 0; k < 4; k++)
  {
    BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );
  }

  sRef = 1;
  phissTrue[3] = 36*sqrt(3)*(2*sRef-1);

  qElem.evalBasisHessianDerivative( sRef, phiss, 4 );
  for (k = 0; k < 4; k++)
  {
    BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );
  }

  sRef = 0.5;
  phissTrue[3] = 36*sqrt(3)*(2*sRef-1);

  qElem.evalBasisHessianDerivative( sRef, phiss, 4 );
  for (k = 0; k < 4; k++)
  {
    BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( eval_variable )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD1, Line > ElementClass;

  static const int NNode = Line::NNode;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( 2, qElem.nDOF() );

  const Real tol = 1e-12;
  Real sRef;
  Real phi[NNode];
  Real phiTrue[NNode];
  ArrayQ q, qTrue;      // variable
  ArrayQ qs, qsTrue;    // variable derivatives wrt sRef
  int k;

  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};

  qElem.DOF(0) = q1;
  qElem.DOF(1) = q2;

  // test s = 0
  sRef = 0;
  phiTrue[0] = 1;  phiTrue[1] = 0;
  qTrue  = q1;
  qsTrue = q2 - q1;  // compute true derivatives using finite difference (which is exact for linear)

  qElem.evalBasis( sRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  qElem.evalDerivative( sRef, qs );

  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  for (k = 0; k < 2; k++)
  {
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );
    BOOST_CHECK_CLOSE( qsTrue[k], qs[k], tol );
  }

  q = 0;
  qElem.eval( sRef, q );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );

  // test s = 1
  sRef = 1;
  phiTrue[0] = 0;  phiTrue[1] = 1;
  qTrue  = q2;
  qsTrue = q2 - q1;  // compute true derivatives using finite difference (which is exact for linear)

  qElem.evalBasis( sRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  qElem.evalDerivative( sRef, qs );

  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  for (k = 0; k < 2; k++)
  {
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );
    BOOST_CHECK_CLOSE( qsTrue[k], qs[k], tol );
  }

  q = 0;
  qElem.eval( sRef, q );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );

  // test s = 0.5
  sRef = 0.5;
  phiTrue[0] = 0.5;  phiTrue[1] = 0.5;
  qTrue = 0.5*(q1 + q2);
  qsTrue = q2 - q1;  // compute true derivatives using finite difference (which is exact for linear)

  qElem.evalBasis( sRef, phi, NNode );
  qElem.evalFromBasis( phi, NNode, q );
  qElem.evalDerivative( sRef, qs );

  for (k = 0; k < NNode; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );

  for (k = 0; k < 2; k++)
  {
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );
    BOOST_CHECK_CLOSE( qsTrue[k], qs[k], tol );
  }

  q = 0;
  qElem.eval( sRef, q );
  for (k = 0; k < 2; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( cached_basis )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef DLA::VectorS< TopoD1::D, ArrayQ > VectorArrayQ;
  typedef Element< ArrayQ, TopoD1, Line > ElementClass;

  static const int TopoD = TopoD1::D;
  const Real small_tol = 1e-12;
  const Real close_tol = 1e-13;

  DLA::VectorS< TopoD, Real > ref;
  ArrayQ q, qTrue;
  VectorArrayQ derivq, derivqTrue;

  for (int order = 1; order < BasisFunctionLine_HierarchicalPMax+1; order++)
  {
    ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

    int nDOF = qElem.nDOF();

    // initalize some arbitrary values
    for (int n = 0; n < nDOF; n++)
      qElem.DOF(n) = pow(-1, n) / sqrt(n+1);

    std::vector<Real> phiTrue(nDOF), phisTrue(nDOF), phissTrue(nDOF);
    std::vector<Real> phi(nDOF), phis(nDOF), phiss(nDOF);

    // check that the cached basis functions work
    for (int iorderidx = 0; iorderidx < QuadratureLine::nOrderIdx; iorderidx++)
    {
      QuadratureLine quadrature( QuadratureLine::getOrderFromIndex(iorderidx) );
      for (int n = 0; n < quadrature.nQuadrature(); n++)
      {
        quadrature.coordinates(n, ref);
        QuadraturePoint<TopoD1> point = quadrature.coordinates_cache(n);

        //--------------
        qElem.evalBasis( ref, phiTrue.data(), phiTrue.size() );
        qElem.evalBasis( point, phi.data(), phi.size() );

        for (int k = 0; k < nDOF; k++)
        {
          BOOST_CHECK_CLOSE( phiTrue[k], phi[k], close_tol );
          phi[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasis( QuadraturePoint<TopoD1>(ref), phi.data(), phi.size() );

        for (int k = 0; k < nDOF; k++)
          BOOST_CHECK_CLOSE( phiTrue[k], phi[k], close_tol );


        //--------------
        qElem.evalBasisDerivative( ref[0], phisTrue.data(), phisTrue.size() );
        qElem.evalBasisDerivative( point, phis.data(), phis.size() );

        for (int k = 0; k < nDOF; k++)
        {
          BOOST_CHECK_CLOSE( phisTrue[k], phis[k], close_tol );
          phis[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasisDerivative( QuadraturePoint<TopoD1>(ref), phis.data(), phis.size() );

        for (int k = 0; k < nDOF; k++)
        {
          BOOST_CHECK_CLOSE( phisTrue[k], phis[k], close_tol );
        }


        //--------------
        qElem.evalBasisHessianDerivative( ref[0], phissTrue.data(), phissTrue.size() );
        qElem.evalBasisHessianDerivative( point, phiss.data(), phiss.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
          phiss[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasisHessianDerivative( QuadraturePoint<TopoD1>(ref), phiss.data(), phiss.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
        }


        //--------------
        qElem.eval( ref, qTrue );
        qElem.eval( point, q );

        for (int k = 0; k < ArrayQ::M; k++)
          BOOST_CHECK_CLOSE( qTrue[k], q[k], close_tol );

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.eval( QuadraturePoint<TopoD1>(ref), q );

        for (int k = 0; k < ArrayQ::M; k++)
          BOOST_CHECK_CLOSE( qTrue[k], q[k], close_tol );


        //--------------
        qElem.evalDerivative( ref, derivqTrue );
        qElem.evalDerivative( point, derivq );

        for (int k = 0; k < ArrayQ::M; k++)
        {
          BOOST_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], close_tol );
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalDerivative( QuadraturePoint<TopoD1>(ref), derivq );

        for (int k = 0; k < ArrayQ::M; k++)
        {
          BOOST_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], close_tol );
        }

      }
    }
  }


  for (int order = 0; order < BasisFunctionLine_LegendrePMax+1; order++)
  {
    ElementClass qElem(order, BasisFunctionCategory_Legendre);

    int nDOF = qElem.nDOF();

    // initalize some arbitrary values
    for (int n = 0; n < nDOF; n++)
      qElem.DOF(n) = pow(-1, n) / sqrt(n+1);

    std::vector<Real> phiTrue(nDOF), phisTrue(nDOF), phissTrue(nDOF);
    std::vector<Real> phi(nDOF), phis(nDOF), phiss(nDOF);

    // check that the cached basis functions work
    for (int iorderidx = 0; iorderidx < QuadratureLine::nOrderIdx; iorderidx++)
    {
      QuadratureLine quadrature( QuadratureLine::getOrderFromIndex(iorderidx) );
      for (int n = 0; n < quadrature.nQuadrature(); n++)
      {
        quadrature.coordinates(n, ref);
        QuadraturePoint<TopoD1> point = quadrature.coordinates_cache(n);

        //--------------
        qElem.evalBasis( ref, phiTrue.data(), phiTrue.size() );
        qElem.evalBasis( point, phi.data(), phi.size() );

        for (int k = 0; k < nDOF; k++)
        {
          BOOST_CHECK_CLOSE( phiTrue[k], phi[k], close_tol );
          phi[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasis( QuadraturePoint<TopoD1>(ref), phi.data(), phi.size() );

        for (int k = 0; k < nDOF; k++)
          BOOST_CHECK_CLOSE( phiTrue[k], phi[k], close_tol );


        //--------------
        qElem.evalBasisDerivative( ref[0], phisTrue.data(), phisTrue.size() );
        qElem.evalBasisDerivative( point, phis.data(), phis.size() );

        for (int k = 0; k < nDOF; k++)
        {
          BOOST_CHECK_CLOSE( phisTrue[k], phis[k], close_tol );
          phis[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasisDerivative( QuadraturePoint<TopoD1>(ref), phis.data(), phis.size() );

        for (int k = 0; k < nDOF; k++)
        {
          BOOST_CHECK_CLOSE( phisTrue[k], phis[k], close_tol );
        }


        //--------------
        qElem.evalBasisHessianDerivative( ref[0], phissTrue.data(), phissTrue.size() );
        qElem.evalBasisHessianDerivative( point, phiss.data(), phiss.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
          phiss[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasisHessianDerivative( QuadraturePoint<TopoD1>(ref), phiss.data(), phiss.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
        }


        //--------------
        qElem.eval( ref, qTrue );
        qElem.eval( point, q );

        for (int k = 0; k < ArrayQ::M; k++)
          BOOST_CHECK_CLOSE( qTrue[k], q[k], close_tol );

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.eval( QuadraturePoint<TopoD1>(ref), q );

        for (int k = 0; k < ArrayQ::M; k++)
          BOOST_CHECK_CLOSE( qTrue[k], q[k], close_tol );


        //--------------
        qElem.evalDerivative( ref, derivqTrue );
        qElem.evalDerivative( point, derivq );

        for (int k = 0; k < ArrayQ::M; k++)
        {
          BOOST_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], close_tol );
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalDerivative( QuadraturePoint<TopoD1>(ref), derivq );

        for (int k = 0; k < ArrayQ::M; k++)
        {
          BOOST_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], close_tol );
        }

      }
    }
  }


  for (int order = 1; order < BasisFunctionLine_LagrangePMax+1; order++)
  {
    ElementClass qElem(order, BasisFunctionCategory_Lagrange);

    int nDOF = qElem.nDOF();

    // initalize some arbitrary values
    for (int n = 0; n < nDOF; n++)
      qElem.DOF(n) = pow(-1, n) / sqrt(n+1);

    std::vector<Real> phiTrue(nDOF), phisTrue(nDOF), phissTrue(nDOF);
    std::vector<Real> phi(nDOF), phis(nDOF), phiss(nDOF);

    // check that the cached basis functions work
    for (int iorderidx = 0; iorderidx < QuadratureLine::nOrderIdx; iorderidx++)
    {
      QuadratureLine quadrature( QuadratureLine::getOrderFromIndex(iorderidx) );
      for (int n = 0; n < quadrature.nQuadrature(); n++)
      {
        quadrature.coordinates(n, ref);
        QuadraturePoint<TopoD1> point = quadrature.coordinates_cache(n);

        //--------------
        qElem.evalBasis( ref, phiTrue.data(), phiTrue.size() );
        qElem.evalBasis( point, phi.data(), phi.size() );

        for (int k = 0; k < nDOF; k++)
        {
          BOOST_CHECK_CLOSE( phiTrue[k], phi[k], close_tol );
          phi[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasis( QuadraturePoint<TopoD1>(ref), phi.data(), phi.size() );

        for (int k = 0; k < nDOF; k++)
          BOOST_CHECK_CLOSE( phiTrue[k], phi[k], close_tol );


        //--------------
        qElem.evalBasisDerivative( ref[0], phisTrue.data(), phisTrue.size() );
        qElem.evalBasisDerivative( point, phis.data(), phis.size() );

        for (int k = 0; k < nDOF; k++)
        {
          BOOST_CHECK_CLOSE( phisTrue[k], phis[k], close_tol );
          phis[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasisDerivative( QuadraturePoint<TopoD1>(ref), phis.data(), phis.size() );

        for (int k = 0; k < nDOF; k++)
        {
          BOOST_CHECK_CLOSE( phisTrue[k], phis[k], close_tol );
        }


        //--------------
        qElem.evalBasisHessianDerivative( ref[0], phissTrue.data(), phissTrue.size() );
        qElem.evalBasisHessianDerivative( point, phiss.data(), phiss.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
          phiss[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasisHessianDerivative( QuadraturePoint<TopoD1>(ref), phiss.data(), phiss.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
        }


        //--------------
        qElem.eval( ref, qTrue );
        qElem.eval( point, q );

        for (int k = 0; k < ArrayQ::M; k++)
          BOOST_CHECK_CLOSE( qTrue[k], q[k], close_tol );

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.eval( QuadraturePoint<TopoD1>(ref), q );

        for (int k = 0; k < ArrayQ::M; k++)
          BOOST_CHECK_CLOSE( qTrue[k], q[k], close_tol );


        //--------------
        qElem.evalDerivative( ref, derivqTrue );
        qElem.evalDerivative( point, derivq );

        for (int k = 0; k < ArrayQ::M; k++)
        {
          BOOST_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], close_tol );
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalDerivative( QuadraturePoint<TopoD1>(ref), derivq );

        for (int k = 0; k < ArrayQ::M; k++)
        {
          BOOST_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], close_tol );
        }

      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( extract_basis )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD1, Line > ElementClass;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( 2, qElem.nDOF() );

  const BasisFunctionLineBase* basis = qElem.basis();

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( 2, basis->nBasis() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( projection )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD1, Line > ElementClass;

  const Real tol = 5e-12;

  for (int order = 1; order < BasisFunctionLine_HierarchicalPMax; order++)
  {
    for (int inc = 1; inc <= BasisFunctionLine_HierarchicalPMax-order; inc++)
    {
      ElementClass qElem0( order,       BasisFunctionCategory_Hierarchical );
      ElementClass qElem1( order + inc, BasisFunctionCategory_Hierarchical );

      for (int k = 0; k < qElem0.nDOF(); k++)
      {
        ArrayQ q = {sqrt(k+7), 1./(k+3)};
        qElem0.DOF(k) = q;
      }

      qElem0.projectTo( qElem1 );

      ArrayQ q0, q1;
      Real s;

      for (int is = 0; is < 3; is++)
      {
        s = 0.11 + is*0.3357;

        qElem0.eval( s, q0 );
        qElem1.eval( s, q1 );

        for (int n = 0; n < ArrayQ::N; n++)
          BOOST_CHECK_CLOSE( q0[n], q1[n], tol );
      }
    }
  }

  for (int order = 0; order < BasisFunctionLine_LegendrePMax; order++)
  {
    for (int inc = 1; inc <= BasisFunctionLine_LegendrePMax-order; inc++)
    {
      ElementClass qElem0( order,       BasisFunctionCategory_Legendre );
      ElementClass qElem1( order + inc, BasisFunctionCategory_Legendre );

      for (int k = 0; k < qElem0.nDOF(); k++)
      {
        ArrayQ q = {sqrt(k+7), 1./(k+3)};
        qElem0.DOF(k) = q;
      }

      qElem0.projectTo( qElem1 );

      ArrayQ q0, q1;
      Real s;

      for (int is = 0; is < 3; is++)
      {
        s = 0.11 + is*0.3357;

        qElem0.eval( s, q0 );
        qElem1.eval( s, q1 );

        for (int n = 0; n < ArrayQ::N; n++)
          BOOST_CHECK_CLOSE( q0[n], q1[n], tol );
      }
    }
  }

  for (int order = 1; order < BasisFunctionLine_LagrangePMax; order++)
  {
    for (int inc = 1; inc <= BasisFunctionLine_LagrangePMax-order; inc++)
    {
      ElementClass qElem0( order,       BasisFunctionCategory_Lagrange );
      ElementClass qElem1( order + inc, BasisFunctionCategory_Lagrange );

      for (int k = 0; k < qElem0.nDOF(); k++)
      {
        ArrayQ q = {sqrt(k+7), 1./(k+3)};
        qElem0.DOF(k) = q;
      }

      qElem0.projectTo( qElem1 );

      ArrayQ q0, q1;
      Real s;

      for (int is = 0; is < 3; is++)
      {
        s = 0.11 + is*0.3357;

        qElem0.eval( s, q0 );
        qElem1.eval( s, q1 );

        for (int n = 0; n < ArrayQ::N; n++)
          BOOST_CHECK_CLOSE( q0[n], q1[n], tol );
      }
    }
  }


  {
    // P=1 Lagrange can be projected to any hierarchical P
    int order = 1;
    ElementClass qElem0( order, BasisFunctionCategory_Lagrange );
    for (int inc = 1; inc <= BasisFunctionLine_HierarchicalPMax-order; inc++)
    {
      ElementClass qElem1( order + inc, BasisFunctionCategory_Hierarchical );

      for (int k = 0; k < qElem0.nDOF(); k++)
      {
        ArrayQ q = {sqrt(k+7), 1./(k+3)};
        qElem0.DOF(k) = q;
      }

      qElem0.projectTo( qElem1 );

      ArrayQ q0, q1;
      Real s;

      for (int is = 0; is < 3; is++)
      {
        s = 0.11 + is*0.3357;

        qElem0.eval( s, q0 );
        qElem1.eval( s, q1 );

        for (int n = 0; n < ArrayQ::N; n++)
          BOOST_CHECK_CLOSE( q0[n], q1[n], tol );
      }
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD1, Line > ElementClass;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/ElementLine_pattern.txt", true );

  ElementClass qElem(1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( 2, qElem.nDOF() );

  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};

  qElem.DOF(0) = q1;
  qElem.DOF(1) = q2;

  qElem.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
