// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementXFieldArea_Triangle_btest
// testing of ElementXFieldArea class w/ triangle

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "tools/stringify.h"     // Real
#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/TraceToCellRefCoord.h"

using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class ElementXField<PhysD2,TopoD2,Triangle>;
template class ElementXField<PhysD3,TopoD2,Triangle>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementXFieldArea_Triangle_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics_test )
{
  BOOST_CHECK( (ElementXField<PhysD2,TopoD2,Triangle>::D == 2) );
  BOOST_CHECK( (ElementXField<PhysD3,TopoD2,Triangle>::D == 3) );
}

//Not sure how to test a 4D triangle yet
typedef boost::mpl::list< PhysD2, PhysD3 > Dimensions;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctors_test, PhysDim, Dimensions )
{
  int order;

  order = 1;
  ElementXField<PhysDim,TopoD2,Triangle> xElem1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem1.order() );
  BOOST_CHECK_EQUAL( 3, xElem1.nDOF() );

  order = 2;
  ElementXField<PhysDim,TopoD2,Triangle> xElem2(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, xElem2.order() );
  BOOST_CHECK_EQUAL( 6, xElem2.nDOF() );

  ElementXField<PhysDim,TopoD2,Triangle> xElem3( BasisFunctionAreaBase<Triangle>::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, xElem3.order() );
  BOOST_CHECK_EQUAL( 6, xElem3.nDOF() );

  ElementXField<PhysDim,TopoD2,Triangle> xElem4(xElem1);

  BOOST_CHECK_EQUAL( 1, xElem4.order() );
  BOOST_CHECK_EQUAL( 3, xElem4.nDOF() );

  xElem4 = xElem2;

  BOOST_CHECK_EQUAL( 2, xElem4.order() );
  BOOST_CHECK_EQUAL( 6, xElem4.nDOF() );

  typedef ElementXField<PhysDim,TopoD2,Triangle> ElementType;
  BOOST_CHECK_THROW( ElementType xElem5(0, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( ElementType xElem6(8, BasisFunctionCategory_Hierarchical), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( accessors_test, PhysDim, Dimensions )
{
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysDim,TopoD2,Triangle> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 3, xElem.nDOF() );

  typename ElementXField<PhysDim,TopoD2,Triangle>::VectorX X1, X2, X3;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    X1[i] = i+1;
    X2[i] = 2*i;
    X3[i] = 2*(i+1);
  }

  xElem.DOF(0) = X1;
  xElem.DOF(1) = X2;
  xElem.DOF(2) = X3;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    BOOST_CHECK_CLOSE( X1[i], xElem.DOF(0)[i], tol );
    BOOST_CHECK_CLOSE( X2[i], xElem.DOF(1)[i], tol );
    BOOST_CHECK_CLOSE( X3[i], xElem.DOF(2)[i], tol );
  }

  ElementXField<PhysDim,TopoD2,Triangle> xElem2(xElem);

  BOOST_CHECK_EQUAL( 1, xElem2.order() );
  BOOST_CHECK_EQUAL( 3, xElem2.nDOF() );

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    BOOST_CHECK_CLOSE( X1[i], xElem2.DOF(0)[i], tol );
    BOOST_CHECK_CLOSE( X2[i], xElem2.DOF(1)[i], tol );
    BOOST_CHECK_CLOSE( X3[i], xElem2.DOF(2)[i], tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( area_coordinates_test, PhysDim, Dimensions )
{
  const Real small_tol = 1e-13;
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysDim,TopoD2,Triangle> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 3, xElem.nDOF() );

  Real sRef, tRef;
  typename ElementXField<PhysDim,TopoD2,Triangle>::VectorX X1, X2, X3;
  typename ElementXField<PhysDim,TopoD2,Triangle>::VectorX X, XTrue;
  Real area, areaTrue, jaceval;

  //Create a triangle in the x-y plane for 2D, and rotated about the x-axis 45 degrees for 3D
  X1 = X2 = 0;
  X3 = cos(PI/4.);

  X1[0] = 0;  X1[1] = 0;
  X2[0] = 1;  X2[1] = 0;
  X3[0] = 0;  X3[1] = 1;
  areaTrue = 0.5;

  if ( PhysDim::D == 3 ) X3[1] = sin(PI/4.);

  xElem.DOF(0) = X1;
  xElem.DOF(1) = X2;
  xElem.DOF(2) = X3;

  X = xElem.DOF(0);
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( X1[i], X[i], small_tol, tol );

  X = xElem.DOF(1);
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( X2[i], X[i], small_tol, tol );

  X = xElem.DOF(2);
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( X3[i], X[i], small_tol, tol );

  area = xElem.area();
  BOOST_CHECK_CLOSE( areaTrue, area, tol );
  jaceval = xElem.jacobianDeterminant();
  BOOST_CHECK_CLOSE( areaTrue, jaceval, tol );

  sRef = 0;  tRef = 0;
  XTrue = X1;
  xElem.coordinates( sRef, tRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  area = xElem.area( sRef, tRef );
  BOOST_CHECK_CLOSE( areaTrue, area, tol );

  sRef = 1;  tRef = 0;
  XTrue = X2;
  xElem.coordinates( sRef, tRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  area = xElem.area( sRef, tRef );
  BOOST_CHECK_CLOSE( areaTrue, area, tol );

  sRef = 0;  tRef = 1;
  XTrue = X3;
  xElem.coordinates( sRef, tRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  area = xElem.area( sRef, tRef );
  BOOST_CHECK_CLOSE( areaTrue, area, tol );

  sRef = 1./3.;  tRef = 1./3.;
  XTrue = (X1 + X2 + X3)/3.;
  xElem.coordinates( sRef, tRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  area = xElem.area( sRef, tRef );
  BOOST_CHECK_CLOSE( areaTrue, area, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( eval_coordinate_test, PhysDim, Dimensions )
{
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysDim,TopoD2,Triangle> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 3, xElem.nDOF() );

  Real sRef, tRef;
  typename ElementXField<PhysDim,TopoD2,Triangle>::VectorX X1, X2, X3;
  Real phi[3], phis[3], phit[3];
  Real phiTrue[3], phisTrue[3], phitTrue[3];
  typename ElementXField<PhysDim,TopoD2,Triangle>::VectorX sxyz, txyz;
  typename ElementXField<PhysDim,TopoD2,Triangle>::VectorX sxyzTrue, txyzTrue;
  typename ElementXField<PhysDim,TopoD2,Triangle>::Matrix invJ;

  int k;

  //Create a triangle in the x-y plane for 2D, and rotated about the x-axis 45 degrees for 3D
  X1 = X2 = 0;
  X3 = cos(PI/4.);

  X1[0] = 0;  X1[1] = 0;
  X2[0] = 1;  X2[1] = 0;
  X3[0] = 0;  X3[1] = 1;

  if ( PhysDim::D == 3 ) X3[1] = sin(PI/4.);

  xElem.DOF(0) = X1;
  xElem.DOF(1) = X2;
  xElem.DOF(2) = X3;

  //Basis function truths
  phisTrue[0] = -1;  phisTrue[1] = 1;  phisTrue[2] = 0;
  phitTrue[0] = -1;  phitTrue[1] = 0;  phitTrue[2] = 1;
  sxyzTrue = X2;
  txyzTrue = X3;

  sRef = 0;  tRef = 0;
  phiTrue[0] = 1;  phiTrue[1] = 0;  phiTrue[2] = 0;

  xElem.evalBasis( sRef, tRef, phi, 3 );
  xElem.evalBasisDerivative( sRef, tRef, phis, phit, 3 );
  for (k = 0; k < 3; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
  }

  xElem.evalReferenceCoordinateGradient( sRef, tRef, sxyz, txyz );
  for ( int i = 0; i < PhysDim::D; i++ )
  {
    BOOST_CHECK_CLOSE( sxyzTrue[i], sxyz[i], tol );
    BOOST_CHECK_CLOSE( txyzTrue[i], txyz[i], tol );
  }

  sRef = 1;  tRef = 0;
  phiTrue[0] = 0;  phiTrue[1] = 1;  phiTrue[2] = 0;

  xElem.evalBasis( sRef, tRef, phi, 3 );
  xElem.evalBasisDerivative( sRef, tRef, phis, phit, 3 );
  for (k = 0; k < 3; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
  }

  xElem.evalReferenceCoordinateGradient( sRef, tRef, sxyz, txyz );
  for ( int i = 0; i < PhysDim::D; i++ )
  {
    BOOST_CHECK_CLOSE( sxyzTrue[i], sxyz[i], tol );
    BOOST_CHECK_CLOSE( txyzTrue[i], txyz[i], tol );
  }

  sRef = 0;  tRef = 1;
  phiTrue[0] = 0;  phiTrue[1] = 0;  phiTrue[2] = 1;

  xElem.evalBasis( sRef, tRef, phi, 3 );
  xElem.evalBasisDerivative( sRef, tRef, phis, phit, 3 );
  for (k = 0; k < 3; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
  }

  xElem.evalReferenceCoordinateGradient( sRef, tRef, sxyz, txyz );
  for ( int i = 0; i < PhysDim::D; i++ )
  {
    BOOST_CHECK_CLOSE( sxyzTrue[i], sxyz[i], tol );
    BOOST_CHECK_CLOSE( txyzTrue[i], txyz[i], tol );
  }

  sRef = 1./3.;  tRef = 1./3.;
  phiTrue[0] = 1./3.;  phiTrue[1] = 1./3.;  phiTrue[2] = 1./3.;

  xElem.evalBasis( sRef, tRef, phi, 3 );
  xElem.evalBasisDerivative( sRef, tRef, phis, phit, 3 );
  for (k = 0; k < 3; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
  }

  xElem.evalReferenceCoordinateGradient( sRef, tRef, sxyz, txyz );
  for ( int i = 0; i < PhysDim::D; i++ )
  {
    BOOST_CHECK_CLOSE( sxyzTrue[i], sxyz[i], tol );
    BOOST_CHECK_CLOSE( txyzTrue[i], txyz[i], tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( evalReferenceCoordinateHessian )
{
  const Real tol = 1e-11;

  typename ElementXField<PhysD2,TopoD2,Triangle>::VectorX sgrad, tgrad;
  typename ElementXField<PhysD2,TopoD2,Triangle>::Matrix invJ;

  typename ElementXField<PhysD2,TopoD2,Triangle>::TensorSymX shess, thess;
  typename ElementXField<PhysD2,TopoD2,Triangle>::TensorSymX shessTrue, thessTrue;
  typename ElementXField<PhysD2,TopoD2,Triangle>::TensorSymHessian H;
  typename ElementXField<PhysD2,TopoD2,Triangle>::TensorSymX hesstmp0, hesstmp1;

  Real sRef = 0.3, tRef=0.4;

  int order = 2;
  ElementXField<PhysD2,TopoD2,Triangle> xElem(order, BasisFunctionCategory_Lagrange);

  // Node DOFs
  xElem.DOF(0) = {0, 0};
  xElem.DOF(1) = {1, 0};
  xElem.DOF(2) = {0, 1};

  // Edge DOFs
  xElem.DOF(3) = { 0.5, -0.1};
  xElem.DOF(4) = { 0.5,  0.6};
  xElem.DOF(5) = {-0.1,  0.5};

  xElem.hessian( sRef, tRef, H );
  xElem.evalReferenceCoordinateGradient( sRef, tRef, sgrad, tgrad );
  xElem.evalReferenceCoordinateHessian( sRef, tRef, shess, thess );

  invJ(0,0) = sgrad[0];
  invJ(0,1) = sgrad[1];

  invJ(1,0) = tgrad[0];
  invJ(1,1) = tgrad[1];

  hesstmp0 = Transpose(invJ)*H[0]*invJ;
  hesstmp1 = Transpose(invJ)*H[1]*invJ;

  shessTrue = -(invJ(0,0)*hesstmp0 + invJ(0,1)*hesstmp1);
  thessTrue = -(invJ(1,0)*hesstmp0 + invJ(1,1)*hesstmp1);

  for (int i = 0; i < PhysD2::D; i++)
    for (int j = 0; j <= i; j++)
    {
      //std::cout << "shessTrue(" << i << "," << j << ") " << shessTrue(i,j) << std::endl;
      //std::cout << "thessTrue(" << i << "," << j << ") " << thessTrue(i,j) << std::endl;
      BOOST_CHECK_CLOSE( shessTrue(i,j), shess(i,j), tol );
      BOOST_CHECK_CLOSE( thessTrue(i,j), thess(i,j), tol );
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( extract_basis_test, PhysDim, Dimensions )
{
  int order = 1;
  ElementXField<PhysDim,TopoD2,Triangle> xElem_P1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem_P1.order() );
  BOOST_CHECK_EQUAL( 3, xElem_P1.nDOF() );

  const BasisFunctionAreaBase<Triangle>* basis_P1 = xElem_P1.basis();

  BOOST_CHECK_EQUAL( 1, basis_P1->order() );
  BOOST_CHECK_EQUAL( 3, basis_P1->nBasis() );

  order = 2;
  ElementXField<PhysDim,TopoD2,Triangle> xElem_P2(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, xElem_P2.order() );
  BOOST_CHECK_EQUAL( 6, xElem_P2.nDOF() );

  const BasisFunctionAreaBase<Triangle>* basis_P2 = xElem_P2.basis();

  BOOST_CHECK_EQUAL( 2, basis_P2->order() );
  BOOST_CHECK_EQUAL( 6, basis_P2->nBasis() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( edge_sign_test, PhysDim, Dimensions )
{
  typedef std::array<int,3> Int3;

  int order = 1;
  ElementXField<PhysDim,TopoD2,Triangle> xElem(order, BasisFunctionCategory_Hierarchical);

  Int3 edgeSign = xElem.edgeSign();
  BOOST_CHECK_EQUAL( +1, edgeSign[0] );
  BOOST_CHECK_EQUAL( +1, edgeSign[1] );
  BOOST_CHECK_EQUAL( +1, edgeSign[2] );

  edgeSign[1] = -1;
  xElem.setEdgeSign(edgeSign);

  Int3 edgeSign2 = xElem.edgeSign();
  BOOST_CHECK_EQUAL( +1, edgeSign2[0] );
  BOOST_CHECK_EQUAL( -1, edgeSign2[1] );
  BOOST_CHECK_EQUAL( +1, edgeSign2[2] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( IO_test, PhysDim, Dimensions )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/ElementXField" + stringify((int)PhysDim::D) + "DArea_Triangle_pattern.txt", true );

  ElementXField<PhysDim,TopoD2,Triangle> xElem(1, BasisFunctionCategory_Hierarchical);

  typename ElementXField<PhysDim,TopoD2,Triangle>::VectorX X1, X2, X3;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    X1[i] = i+1;
    X2[i] = 2*i;
    X3[i] = 2*(i+1);
  }

  xElem.DOF(0) = X1;
  xElem.DOF(1) = X2;
  xElem.DOF(2) = X3;

  xElem.dump( 2, output );
  xElem.dumpTecplot( output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
