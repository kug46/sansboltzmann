// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BoundingBox_btest
// testing of BoundingBox class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "Field/Element/BoundingBox.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( BoundingBox_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PhysD1_test )
{
  typedef BoundingBox<PhysD1>::VectorX VectorX;

  BoundingBox< PhysD1 > bbox0;

  VectorX low_bounds = {0};
  VectorX high_bounds = {1};

  bbox0.setBound(low_bounds);
  bbox0.setBound(high_bounds);

  BOOST_CHECK_EQUAL( 0, bbox0.low_bounds()[0] );
  BOOST_CHECK_EQUAL( 1, bbox0.high_bounds()[0] );

  BoundingBox< PhysD1 > bbox1(bbox0);

  BOOST_CHECK_EQUAL( 0, bbox1.low_bounds()[0] );
  BOOST_CHECK_EQUAL( 1, bbox1.high_bounds()[0] );

  BoundingBox< PhysD1 > bbox2;
  bbox2 = bbox0;

  BOOST_CHECK_EQUAL( 0, bbox2.low_bounds()[0] );
  BOOST_CHECK_EQUAL( 1, bbox2.high_bounds()[0] );

  BoundingBox< PhysD1 > bbox3;
  bbox3.setBound(bbox0);

  BOOST_CHECK_EQUAL( 0, bbox3.low_bounds()[0] );
  BOOST_CHECK_EQUAL( 1, bbox3.high_bounds()[0] );

  BOOST_CHECK( bbox0.intersects_with(bbox1) );

  BoundingBox< PhysD1 > bbox4;
  bbox4.setBound({2});
  bbox4.setBound({3});

  BOOST_CHECK( !bbox0.intersects_with(bbox4) );

  BOOST_CHECK(  bbox0.encloses({0.5}) );
  BOOST_CHECK( !bbox0.encloses({1.5}) );

  const Real close_tol = 1e-11;

  BOOST_CHECK_CLOSE( 0.5, bbox0.distance_to({-0.5}), close_tol );
  BOOST_CHECK_CLOSE( 1.0, bbox0.distance_to({ 2.0}), close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PhysD2_test )
{
  typedef BoundingBox<PhysD2>::VectorX VectorX;

  BoundingBox< PhysD2 > bbox0;

  VectorX low_bounds = {0,0};
  VectorX high_bounds = {1,1};

  bbox0.setBound(low_bounds);
  bbox0.setBound(high_bounds);

  BOOST_CHECK_EQUAL( 0, bbox0.low_bounds()[0] );
  BOOST_CHECK_EQUAL( 0, bbox0.low_bounds()[1] );
  BOOST_CHECK_EQUAL( 1, bbox0.high_bounds()[0] );
  BOOST_CHECK_EQUAL( 1, bbox0.high_bounds()[1] );

  BoundingBox< PhysD2 > bbox1(bbox0);

  BOOST_CHECK_EQUAL( 0, bbox1.low_bounds()[0] );
  BOOST_CHECK_EQUAL( 0, bbox1.low_bounds()[1] );
  BOOST_CHECK_EQUAL( 1, bbox1.high_bounds()[0] );
  BOOST_CHECK_EQUAL( 1, bbox1.high_bounds()[1] );

  BoundingBox< PhysD2 > bbox2;
  bbox2 = bbox0;

  BOOST_CHECK_EQUAL( 0, bbox2.low_bounds()[0] );
  BOOST_CHECK_EQUAL( 0, bbox2.low_bounds()[1] );
  BOOST_CHECK_EQUAL( 1, bbox2.high_bounds()[0] );
  BOOST_CHECK_EQUAL( 1, bbox2.high_bounds()[1] );

  BoundingBox< PhysD2 > bbox3;
  bbox3.setBound(bbox0);

  BOOST_CHECK_EQUAL( 0, bbox3.low_bounds()[0] );
  BOOST_CHECK_EQUAL( 0, bbox3.low_bounds()[1] );
  BOOST_CHECK_EQUAL( 1, bbox3.high_bounds()[0] );
  BOOST_CHECK_EQUAL( 1, bbox3.high_bounds()[1] );

  BOOST_CHECK( bbox0.intersects_with(bbox1) );

  BoundingBox< PhysD2 > bbox4;
  bbox4.setBound({2,2});
  bbox4.setBound({3,3});

  BOOST_CHECK( !bbox0.intersects_with(bbox4) );

  BoundingBox< PhysD2 > bbox5;
  bbox5.setBound({-2,-2});
  bbox5.setBound({-1,-1});

  BOOST_CHECK( !bbox0.intersects_with(bbox5) );

  BoundingBox< PhysD2 > bbox6;
  bbox6.setBound({-1,-1});
  bbox6.setBound({ 2, 2});

  BOOST_CHECK( bbox0.intersects_with(bbox6) );

  BOOST_CHECK(  bbox0.encloses({0.5,0.5}) );
  BOOST_CHECK( !bbox0.encloses({1.5,1.5}) );

  /*
      1          2          3
          +-------------+
          |             |
      8   |             |   4
          |             |
          +-------------+
      7          6          5
   */

  const Real close_tol = 1e-11;

  BOOST_CHECK_CLOSE( sqrt(pow(0.25,2) + pow(0.50,2)), bbox0.distance_to({-0.25, 1.50}), close_tol );
  BOOST_CHECK_CLOSE( sqrt(          0 + pow(0.25,2)), bbox0.distance_to({ 0.50, 1.25}), close_tol );
  BOOST_CHECK_CLOSE( sqrt(pow(0.50,2) + pow(0.30,2)), bbox0.distance_to({ 1.50, 1.30}), close_tol );
  BOOST_CHECK_CLOSE( sqrt(pow(0.25,2) + 0          ), bbox0.distance_to({ 1.25, 0.50}), close_tol );
  BOOST_CHECK_CLOSE( sqrt(pow(0.25,2) + pow(0.25,2)), bbox0.distance_to({ 1.25,-0.25}), close_tol );
  BOOST_CHECK_CLOSE( sqrt(          0 + pow(0.25,2)), bbox0.distance_to({ 0.50,-0.25}), close_tol );
  BOOST_CHECK_CLOSE( sqrt(pow(0.50,2) + pow(0.25,2)), bbox0.distance_to({-0.50,-0.25}), close_tol );
  BOOST_CHECK_CLOSE( sqrt(pow(0.25,2) + 0          ), bbox0.distance_to({-0.25, 0.50}), close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PhysD3_test )
{
  typedef BoundingBox<PhysD3>::VectorX VectorX;

  BoundingBox< PhysD3 > bbox0;

  VectorX low_bounds = {0,0,0};
  VectorX high_bounds = {1,1,1};

  bbox0.setBound(low_bounds);
  bbox0.setBound(high_bounds);

  BOOST_CHECK_EQUAL( 0, bbox0.low_bounds()[0] );
  BOOST_CHECK_EQUAL( 0, bbox0.low_bounds()[1] );
  BOOST_CHECK_EQUAL( 0, bbox0.low_bounds()[2] );
  BOOST_CHECK_EQUAL( 1, bbox0.high_bounds()[0] );
  BOOST_CHECK_EQUAL( 1, bbox0.high_bounds()[1] );
  BOOST_CHECK_EQUAL( 1, bbox0.high_bounds()[2] );

  BoundingBox< PhysD3 > bbox1(bbox0);

  BOOST_CHECK_EQUAL( 0, bbox1.low_bounds()[0] );
  BOOST_CHECK_EQUAL( 0, bbox1.low_bounds()[1] );
  BOOST_CHECK_EQUAL( 0, bbox1.low_bounds()[2] );
  BOOST_CHECK_EQUAL( 1, bbox1.high_bounds()[0] );
  BOOST_CHECK_EQUAL( 1, bbox1.high_bounds()[1] );
  BOOST_CHECK_EQUAL( 1, bbox1.high_bounds()[2] );

  BoundingBox< PhysD3 > bbox2;
  bbox2 = bbox0;

  BOOST_CHECK_EQUAL( 0, bbox2.low_bounds()[0] );
  BOOST_CHECK_EQUAL( 0, bbox2.low_bounds()[1] );
  BOOST_CHECK_EQUAL( 0, bbox2.low_bounds()[2] );
  BOOST_CHECK_EQUAL( 1, bbox2.high_bounds()[0] );
  BOOST_CHECK_EQUAL( 1, bbox2.high_bounds()[1] );
  BOOST_CHECK_EQUAL( 1, bbox2.high_bounds()[2] );

  BoundingBox< PhysD3 > bbox3;
  bbox3.setBound(bbox0);

  BOOST_CHECK_EQUAL( 0, bbox3.low_bounds()[0] );
  BOOST_CHECK_EQUAL( 0, bbox3.low_bounds()[1] );
  BOOST_CHECK_EQUAL( 0, bbox3.low_bounds()[2] );
  BOOST_CHECK_EQUAL( 1, bbox3.high_bounds()[0] );
  BOOST_CHECK_EQUAL( 1, bbox3.high_bounds()[1] );
  BOOST_CHECK_EQUAL( 1, bbox3.high_bounds()[2] );

  BOOST_CHECK( bbox0.intersects_with(bbox1) );

  BoundingBox< PhysD3 > bbox4;
  bbox4.setBound({2,2,2});
  bbox4.setBound({3,3,3});

  BOOST_CHECK( !bbox0.intersects_with(bbox4) );

  BoundingBox< PhysD3 > bbox5;
  bbox5.setBound({-2,-2,-2});
  bbox5.setBound({-1,-1,-1});

  BOOST_CHECK( !bbox0.intersects_with(bbox5) );

  BoundingBox< PhysD3 > bbox6;
  bbox6.setBound({-1,-1,-1});
  bbox6.setBound({ 2, 2, 2});

  BOOST_CHECK( bbox0.intersects_with(bbox6) );

  BOOST_CHECK(  bbox0.encloses({0.5,0.5,0.5}) );
  BOOST_CHECK( !bbox0.encloses({1.5,1.5,1.5}) );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
