// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementXFieldArea_Quad_btest
// testing of ElementXFieldArea class w/ quad

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "tools/stringify.h"     // Real
#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "BasisFunction/BasisFunctionArea_Quad.h"
#include "BasisFunction/TraceToCellRefCoord.h"

using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class ElementXField<PhysD2,TopoD2,Quad>;
template class ElementXField<PhysD3,TopoD2,Quad>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementXFieldAreaBase_Quad_test_suite )

//TODO: Not sure how to test a 4D quad yet
typedef boost::mpl::list< PhysD2, PhysD3 > Dimensions;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics_test )
{
  BOOST_CHECK( (ElementXField<PhysD2,TopoD2,Quad>::D == 2) );
  BOOST_CHECK( (ElementXField<PhysD3,TopoD2,Quad>::D == 3) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctors_test, PhysDim, Dimensions )
{
  int order;

  order = 1;
  ElementXField<PhysDim,TopoD2,Quad> xElem1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem1.order() );
  BOOST_CHECK_EQUAL( 4, xElem1.nDOF() );
#if 0
  order = 2;
  ElementXField<PhysDim,TopoD2,Quad> xElem2(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, xElem2.order() );
  BOOST_CHECK_EQUAL( 6, xElem2.nDOF() );

  ElementXField<PhysDim,TopoD2,Quad> xElem3( BasisFunctionAreaBase<Quad>::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, xElem3.order() );
  BOOST_CHECK_EQUAL( 6, xElem3.nDOF() );

  ElementXField<PhysDim,TopoD2,Quad> xElem4(xElem1);

  BOOST_CHECK_EQUAL( 1, xElem4.order() );
  BOOST_CHECK_EQUAL( 3, xElem4.nDOF() );

  xElem4 = xElem2;

  BOOST_CHECK_EQUAL( 2, xElem4.order() );
  BOOST_CHECK_EQUAL( 6, xElem4.nDOF() );
#endif
  typedef ElementXField<PhysDim,TopoD2,Quad> ElementType;
  BOOST_CHECK_THROW( ElementType xElem5(0, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( ElementType xElem6(BasisFunctionArea_Quad_HierarchicalPMax+1, BasisFunctionCategory_Hierarchical), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( accessors_test, PhysDim, Dimensions )
{
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysDim,TopoD2,Quad> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 4, xElem.nDOF() );

  typename ElementXField<PhysDim,TopoD2,Quad>::VectorX X1, X2, X3, X4;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    X1[i] = i+1;
    X2[i] = 2*i;
    X3[i] = 2*(i+1);
    X4[i] = 3*(i+1);
  }

  xElem.DOF(0) = X1;
  xElem.DOF(1) = X2;
  xElem.DOF(2) = X3;
  xElem.DOF(3) = X4;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    BOOST_CHECK_CLOSE( X1[i], xElem.DOF(0)[i], tol );
    BOOST_CHECK_CLOSE( X2[i], xElem.DOF(1)[i], tol );
    BOOST_CHECK_CLOSE( X3[i], xElem.DOF(2)[i], tol );
    BOOST_CHECK_CLOSE( X4[i], xElem.DOF(3)[i], tol );
  }

  ElementXField<PhysDim,TopoD2,Quad> xElem2(xElem);

  BOOST_CHECK_EQUAL( 1, xElem2.order() );
  BOOST_CHECK_EQUAL( 4, xElem2.nDOF() );

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    BOOST_CHECK_CLOSE( X1[i], xElem2.DOF(0)[i], tol );
    BOOST_CHECK_CLOSE( X2[i], xElem2.DOF(1)[i], tol );
    BOOST_CHECK_CLOSE( X3[i], xElem2.DOF(2)[i], tol );
    BOOST_CHECK_CLOSE( X4[i], xElem2.DOF(3)[i], tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( area_coordinates_test, PhysDim, Dimensions )
{
  const Real small_tol = 1e-13;
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysDim,TopoD2,Quad> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 4, xElem.nDOF() );

  Real sRef, tRef;
  typename ElementXField<PhysDim,TopoD2,Quad>::VectorX X1, X2, X3, X4;
  typename ElementXField<PhysDim,TopoD2,Quad>::VectorX X, XTrue;
  Real area, areaTrue, jaceval;

  //Create a quad in the x-y plane for 2D, and rotated about the x-axis 45 degrees for 3D
  X1 = X2 = 0;
  X3 = X4 = cos(PI/4.);

  X1[0] = 0;  X1[1] = 0;
  X2[0] = 1;  X2[1] = 0;
  X3[0] = 1;  X3[1] = 1;
  X4[0] = 0;  X4[1] = 1;
  areaTrue = 1;

  if ( PhysDim::D == 3 )
    X3[1] = X4[1] = sin(PI/4.);

  xElem.DOF(0) = X1;
  xElem.DOF(1) = X2;
  xElem.DOF(2) = X3;
  xElem.DOF(3) = X4;

  X = xElem.DOF(0);
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( X1[i], X[i], small_tol, tol );

  X = xElem.DOF(1);
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( X2[i], X[i], small_tol, tol );

  X = xElem.DOF(2);
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( X3[i], X[i], small_tol, tol );

  X = xElem.DOF(3);
  for ( int i = 0; i < PhysDim::D; i++ )
    SANS_CHECK_CLOSE( X4[i], X[i], small_tol, tol );

  area = xElem.area();
  BOOST_CHECK_CLOSE( areaTrue, area, tol );
  jaceval = xElem.jacobianDeterminant();
  BOOST_CHECK_CLOSE( areaTrue, jaceval, tol );


  sRef = 0;  tRef = 0;
  XTrue = X1;
  xElem.coordinates( sRef, tRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  area = xElem.area( sRef, tRef );
  BOOST_CHECK_CLOSE( areaTrue, area, tol );


  sRef = 1;  tRef = 0;
  XTrue = X2;
  xElem.coordinates( sRef, tRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  area = xElem.area( sRef, tRef );
  BOOST_CHECK_CLOSE( areaTrue, area, tol );


  sRef = 1;  tRef = 1;
  XTrue = X3;
  xElem.coordinates( sRef, tRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  area = xElem.area( sRef, tRef );
  BOOST_CHECK_CLOSE( areaTrue, area, tol );


  sRef = 0;  tRef = 1;
  XTrue = X4;
  xElem.coordinates( sRef, tRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  area = xElem.area( sRef, tRef );
  BOOST_CHECK_CLOSE( areaTrue, area, tol );


  sRef = 1./2.;  tRef = 1./2.;
  XTrue = (X1 + X2 + X3 + X4)/4.;
  xElem.coordinates( sRef, tRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  area = xElem.area( sRef, tRef );
  BOOST_CHECK_CLOSE( areaTrue, area, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( eval_coordinate_test, PhysDim, Dimensions )
{
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysDim,TopoD2,Quad> xElem(order, BasisFunctionCategory_Hierarchical);

  static const int NNode = Quad::NNode;

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( NNode, xElem.nDOF() );

  Real sRef, tRef;
  typename ElementXField<PhysDim,TopoD2,Quad>::VectorX X1, X2, X3, X4;
  typename ElementXField<PhysDim,TopoD2,Quad>::VectorX X, Xd, Xs, Xt;
  typename ElementXField<PhysDim,TopoD2,Quad>::VectorX XTrue, XsTrue, XtTrue;
  typename ElementXField<PhysDim,TopoD2,Quad>::Matrix invJ;

  //Create a quad in the x-y plane for 2D, and rotated about the x-axis 45 degrees for 3D
  X1 = X2 = 0;
  X3 = X4 = cos(PI/4.);

  X1[0] = 0;  X1[1] = 0;
  X2[0] = 1;  X2[1] = 0;
  X3[0] = 1;  X3[1] = 1;
  X4[0] = 0;  X4[1] = 1;

  if ( PhysDim::D == 3 ) X3[1] = X4[1] = sin(PI/4.);

  xElem.DOF(0) = X1;
  xElem.DOF(1) = X2;
  xElem.DOF(2) = X3;
  xElem.DOF(3) = X4;

  //------
  sRef = 0;  tRef = 0;
  XTrue = X1;

  xElem.eval( sRef, tRef, X );
  xElem.evalReferenceCoordinateGradient( sRef, tRef, Xs, Xt );

  // Compute true derivatives using finite difference (which is exact for linear and the reference coordinates match the physical coordinates)
  sRef += 1;
  xElem.eval( sRef, tRef, Xd );
  XsTrue = Xd - X;
  sRef -= 1;

  tRef += 1;
  xElem.eval( sRef, tRef, Xd );
  XtTrue = Xd - X;
  tRef -= 1;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    SANS_CHECK_CLOSE( XTrue[i], X[i], tol, tol );
    SANS_CHECK_CLOSE( XsTrue[i], Xs[i], tol, tol );
    SANS_CHECK_CLOSE( XtTrue[i], Xt[i], tol, tol );
  }

  //------
  sRef = 1;  tRef = 0;
  XTrue = X2;

  xElem.eval( sRef, tRef, X );
  xElem.evalReferenceCoordinateGradient( sRef, tRef, Xs, Xt );

  // Compute true derivatives using finite difference (which is exact for linear and the reference coordinates match the physical coordinates)
  sRef += 1;
  xElem.eval( sRef, tRef, Xd );
  XsTrue = Xd - X;
  sRef -= 1;

  tRef += 1;
  xElem.eval( sRef, tRef, Xd );
  XtTrue = Xd - X;
  tRef -= 1;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    SANS_CHECK_CLOSE( XTrue[i], X[i], tol, tol );
    SANS_CHECK_CLOSE( XsTrue[i], Xs[i], tol, tol );
    SANS_CHECK_CLOSE( XtTrue[i], Xt[i], tol, tol );
  }

  //------
  sRef = 1;  tRef = 1;
  XTrue = X3;

  xElem.eval( sRef, tRef, X );
  xElem.evalReferenceCoordinateGradient( sRef, tRef, Xs, Xt );

  // Compute true derivatives using finite difference (which is exact for linear and the reference coordinates match the physical coordinates)
  sRef += 1;
  xElem.eval( sRef, tRef, Xd );
  XsTrue = Xd - X;
  sRef -= 1;

  tRef += 1;
  xElem.eval( sRef, tRef, Xd );
  XtTrue = Xd - X;
  tRef -= 1;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    SANS_CHECK_CLOSE( XTrue[i], X[i], tol, tol );
    SANS_CHECK_CLOSE( XsTrue[i], Xs[i], tol, tol );
    SANS_CHECK_CLOSE( XtTrue[i], Xt[i], tol, tol );
  }

  //------
  sRef = 0;  tRef = 1;
  XTrue = X4;

  xElem.eval( sRef, tRef, X );
  xElem.evalReferenceCoordinateGradient( sRef, tRef, Xs, Xt );

  // Compute true derivatives using finite difference (which is exact for linear and the reference coordinates match the physical coordinates)
  sRef += 1;
  xElem.eval( sRef, tRef, Xd );
  XsTrue = Xd - X;
  sRef -= 1;

  tRef += 1;
  xElem.eval( sRef, tRef, Xd );
  XtTrue = Xd - X;
  tRef -= 1;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    SANS_CHECK_CLOSE( XTrue[i], X[i], tol, tol );
    SANS_CHECK_CLOSE( XsTrue[i], Xs[i], tol, tol );
    SANS_CHECK_CLOSE( XtTrue[i], Xt[i], tol, tol );
  }

  //------
  sRef = 1./2.;  tRef = 1./2.;
  XTrue = (X1 + X2 + X3 + X4)/4.;

  xElem.eval( sRef, tRef, X );
  xElem.evalReferenceCoordinateGradient( sRef, tRef, Xs, Xt );

  // Compute true derivatives using finite difference (which is exact for linear and the reference coordinates match the physical coordinates)
  sRef += 1;
  xElem.eval( sRef, tRef, Xd );
  XsTrue = Xd - X;
  sRef -= 1;

  tRef += 1;
  xElem.eval( sRef, tRef, Xd );
  XtTrue = Xd - X;
  tRef -= 1;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    SANS_CHECK_CLOSE( XTrue[i], X[i], tol, tol );
    SANS_CHECK_CLOSE( XsTrue[i], Xs[i], tol, tol );
    SANS_CHECK_CLOSE( XtTrue[i], Xt[i], tol, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( evalReferenceCoordinateHessian )
{
  const Real tol = 1e-11;

  typename ElementXField<PhysD2,TopoD2,Quad>::VectorX sgrad, tgrad;
  typename ElementXField<PhysD2,TopoD2,Quad>::Matrix invJ;

  typename ElementXField<PhysD2,TopoD2,Quad>::TensorSymX shess, thess;
  typename ElementXField<PhysD2,TopoD2,Quad>::TensorSymX shessTrue, thessTrue;
  typename ElementXField<PhysD2,TopoD2,Quad>::TensorSymHessian H;
  typename ElementXField<PhysD2,TopoD2,Quad>::TensorSymX hesstmp0, hesstmp1;

  Real sRef = 0.3, tRef=0.4;

  int order = 2;
  ElementXField<PhysD2,TopoD2,Quad> xElem(order, BasisFunctionCategory_Lagrange);

  // Node DOFs
  xElem.DOF(0) = {0, 0};
  xElem.DOF(1) = {1, 0};
  xElem.DOF(2) = {1, 1};
  xElem.DOF(3) = {0, 1};

  // Edge DOFs
  xElem.DOF(4) = { 0.5, -0.1};
  xElem.DOF(5) = { 1.1,  0.5};
  xElem.DOF(6) = { 0.5,  1.1};
  xElem.DOF(7) = {-0.1,  0.5};

  // Cell DOF
  xElem.DOF(8) = { 0.5,  0.5};

  xElem.hessian( sRef, tRef, H );
  xElem.evalReferenceCoordinateGradient( sRef, tRef, sgrad, tgrad );
  xElem.evalReferenceCoordinateHessian( sRef, tRef, shess, thess );

  invJ(0,0) = sgrad[0];
  invJ(0,1) = sgrad[1];

  invJ(1,0) = tgrad[0];
  invJ(1,1) = tgrad[1];

  hesstmp0 = Transpose(invJ)*H[0]*invJ;
  hesstmp1 = Transpose(invJ)*H[1]*invJ;

  shessTrue = -(invJ(0,0)*hesstmp0 + invJ(0,1)*hesstmp1);
  thessTrue = -(invJ(1,0)*hesstmp0 + invJ(1,1)*hesstmp1);

  for (int i = 0; i < PhysD2::D; i++)
    for (int j = 0; j <= i; j++)
    {
      //std::cout << "shessTrue(" << i << "," << j << ") " << shessTrue(i,j) << std::endl;
      //std::cout << "thessTrue(" << i << "," << j << ") " << thessTrue(i,j) << std::endl;
      BOOST_CHECK_CLOSE( shessTrue(i,j), shess(i,j), tol );
      BOOST_CHECK_CLOSE( thessTrue(i,j), thess(i,j), tol );
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( extract_basis_test, PhysDim, Dimensions )
{
  int order = 1;
  ElementXField<PhysDim,TopoD2,Quad> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 4, xElem.nDOF() );

  const BasisFunctionAreaBase<Quad>* basis = xElem.basis();

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( 4, basis->nBasis() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( edge_sign_test, PhysDim, Dimensions )
{
  typedef std::array<int,4> Int4;

  int order = 1;
  ElementXField<PhysDim,TopoD2,Quad> xElem(order, BasisFunctionCategory_Hierarchical);

  Int4 edgeSign = xElem.edgeSign();
  BOOST_CHECK_EQUAL( +1, edgeSign[0] );
  BOOST_CHECK_EQUAL( +1, edgeSign[1] );
  BOOST_CHECK_EQUAL( +1, edgeSign[2] );
  BOOST_CHECK_EQUAL( +1, edgeSign[3] );

  edgeSign[1] = -1;
  xElem.setEdgeSign(edgeSign);

  Int4 edgeSign2 = xElem.edgeSign();
  BOOST_CHECK_EQUAL( +1, edgeSign2[0] );
  BOOST_CHECK_EQUAL( -1, edgeSign2[1] );
  BOOST_CHECK_EQUAL( +1, edgeSign2[2] );
  BOOST_CHECK_EQUAL( +1, edgeSign2[3] );
}


//----------------------------------------------------------------------------//
// check that the trace element coordinates evaluate to the volume element coordinates on the trace
BOOST_AUTO_TEST_CASE( Trace_RefCoord_P1_test )
{
  typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldAreaClass;
  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldLineClass;
  typedef ElementXFieldAreaClass::VectorX VectorX;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  // element field variables
  ElementXFieldLineClass xfldElemFaceP( BasisFunctionLineBase::HierarchicalP1 );
  ElementXFieldLineClass xfldElemFaceN( BasisFunctionLineBase::HierarchicalP1 );
  ElementXFieldAreaClass xfldElem( BasisFunctionAreaBase<Quad>::HierarchicalP1 );

  xfldElem.DOF(0) = {0, 0};
  xfldElem.DOF(1) = {1, 0};
  xfldElem.DOF(2) = {1, 1};
  xfldElem.DOF(3) = {0, 1};

  const int quad[4] = {0, 1, 2, 3};

  const int (*TraceNodes)[ Line::NTrace ] = TraceToCellRefCoord<Line, TopoD2, Quad>::TraceNodes;

  const int *OrientPos = TraceToCellRefCoord<Line, TopoD2, Quad>::OrientPos;
  const int *OrientNeg = TraceToCellRefCoord<Line, TopoD2, Quad>::OrientNeg;


  for (int edge = 0; edge < Quad::NTrace; edge++)
  {
    // Get the re-oriented lines
    const int lineP[2] = {quad[TraceNodes[edge][OrientPos[0]]],
                          quad[TraceNodes[edge][OrientPos[1]]]};

    const int lineN[2] = {quad[TraceNodes[edge][OrientNeg[0]]],
                          quad[TraceNodes[edge][OrientNeg[1]]]};

    const CanonicalTraceToCell canonicalEdgeP = TraceToCellRefCoord<Line, TopoD2, Quad>::getCanonicalTrace(lineP, 2, quad, 4);
    const CanonicalTraceToCell canonicalEdgeN = TraceToCellRefCoord<Line, TopoD2, Quad>::getCanonicalTrace(lineN, 2, quad, 4);

    BOOST_REQUIRE_EQUAL(canonicalEdgeP.trace, edge);
    BOOST_REQUIRE_EQUAL(canonicalEdgeP.orientation, +1);

    BOOST_REQUIRE_EQUAL(canonicalEdgeN.trace, edge);
    BOOST_REQUIRE_EQUAL(canonicalEdgeN.orientation, -1);

    // Set the DOF's on the trace
    for (int n = 0; n < Line::NNode; n++)
    {
      xfldElemFaceP.DOF(n) = xfldElem.DOF(lineP[n]);
      xfldElemFaceN.DOF(n) = xfldElem.DOF(lineN[n]);
    }

    int kmax = 5;
    for (int k = 0; k < kmax; k++)
    {
      DLA::VectorS<1,Real> sRefTrace;
      DLA::VectorS<2,Real> sRefCell;
      VectorX xTrace, xCell;
      Real s;

      s = k/static_cast<Real>(kmax-1);

      sRefTrace = {s};

      // volume reference-element coords
      TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalEdgeP, sRefTrace, sRefCell );

      // element coordinates from trace and cell
      xfldElemFaceP.eval( sRefTrace, xTrace );
      xfldElem.eval( sRefCell, xCell );

      SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
      SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );

      // volume reference-element coords
      TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalEdgeN, sRefTrace, sRefCell );

      // element coordinates from trace and cell
      xfldElemFaceN.eval( sRefTrace, xTrace );
      xfldElem.eval( sRefCell, xCell );

      SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
      SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( impliedMetric_test )
{
  const Real small_tol = 1e-13;
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysD2, TopoD2, Quad> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 4, xElem.nDOF() );

  ElementXField<PhysD2, TopoD2, Quad>::RefCoordType sRef = {0.0, 0.0};

  DLA::MatrixSymS<PhysD2::D, Real> M;

  //Unit quad
  xElem.DOF(0) = {0.0, 0.0};
  xElem.DOF(1) = {1.0, 0.0};
  xElem.DOF(2) = {1.0, 1.0};
  xElem.DOF(3) = {0.0, 1.0};

  xElem.impliedMetric(sRef, M);

  SANS_CHECK_CLOSE( M(0,0), 1.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,1), 1.0, small_tol, tol );

  M = 0;
  xElem.impliedMetric(M); //evaluate at centerRef

  SANS_CHECK_CLOSE( M(0,0), 1.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,1), 1.0, small_tol, tol );

  //Unit quad - scaled by 2.5, translated by (0.6,1.3)
  Real scale = 2.5;
  Real xshift[2] = {0.6, 1.3};
  for (int i=0; i<4; i++)
    for (int d=0; d<2; d++)
      xElem.DOF(i)[d] = xElem.DOF(i)[d]*scale + xshift[d];

  xElem.impliedMetric(sRef, M);

  SANS_CHECK_CLOSE( M(0,0), 1.0/pow(scale,2.0), small_tol, tol );
  SANS_CHECK_CLOSE( M(0,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,1), 1.0/pow(scale,2.0), small_tol, tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( IO, PhysDim, Dimensions )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/ElementXField" + stringify((int)PhysDim::D) + "DArea_Quad_pattern.txt", true );

  ElementXField<PhysDim,TopoD2,Quad> xElem(1, BasisFunctionCategory_Hierarchical);

  typename ElementXField<PhysDim,TopoD2,Quad>::VectorX X1, X2, X3, X4;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    X1[i] = i+1;
    X2[i] = 2*i;
    X3[i] = 2*(i+1);
    X4[i] = 3*(i+1);
  }

  xElem.DOF(0) = X1;
  xElem.DOF(1) = X2;
  xElem.DOF(2) = X3;
  xElem.DOF(3) = X4;

  xElem.dump( 2, output );
  xElem.dumpTecplot( output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
