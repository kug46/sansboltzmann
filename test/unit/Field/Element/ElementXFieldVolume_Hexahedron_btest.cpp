// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementXFieldVolume_Hexahedron_btest
// testing of ElementXFieldVolume class w/ Hexahedron

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "tools/stringify.h"     // Real
#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"
#include "BasisFunction/BasisFunctionArea_Quad.h"
#include "BasisFunction/BasisFunctionVolume_Hexahedron.h"
#include "BasisFunction/TraceToCellRefCoord.h"
#include "BasisFunction/ElementEdges.h"

using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class ElementXField<PhysD3,TopoD3,Hex>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementXFieldVolume_Hexahedron_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK( (ElementXField<PhysD3,TopoD3,Hex>::D == 3) );
}

//Not sure how to test a 4D Hexahedron yet
typedef boost::mpl::list< PhysD3 > Dimensions;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctors, PhysDim, Dimensions )
{
  int order;

  order = 1;
  ElementXField<PhysDim,TopoD3,Hex> xElem1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem1.order() );
  BOOST_CHECK_EQUAL( 8, xElem1.nDOF() );
#if 0
  order = 2;
  ElementXField<PhysDim,TopoD3,Hex> xElem2(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, xElem2.order() );
  BOOST_CHECK_EQUAL( 6, xElem2.nDOF() );

  ElementXField<PhysDim,TopoD3,Hex> xElem3( BasisFunctionVolumeBase<Hex>::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, xElem3.order() );
  BOOST_CHECK_EQUAL( 6, xElem3.nDOF() );

  ElementXField<PhysDim,TopoD3,Hex> xElem4(xElem1);

  BOOST_CHECK_EQUAL( 1, xElem4.order() );
  BOOST_CHECK_EQUAL( 3, xElem4.nDOF() );

  xElem4 = xElem2;

  BOOST_CHECK_EQUAL( 2, xElem4.order() );
  BOOST_CHECK_EQUAL( 6, xElem4.nDOF() );
#endif
  typedef ElementXField<PhysDim,TopoD3,Hex> ElementType;
  BOOST_CHECK_THROW( ElementType xElem5(0, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( ElementType xElem6(BasisFunctionVolume_Hex_HierarchicalPMax+1,
                                       BasisFunctionCategory_Hierarchical), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( accessors, PhysDim, Dimensions )
{
  const Real tol = 1e-13;

  static const int NNode = Hex::NNode;

  int order = 1;
  ElementXField<PhysDim,TopoD3,Hex> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( NNode, xElem.nDOF() );

  typename ElementXField<PhysDim,TopoD3,Hex>::VectorX X[NNode];

  for ( int k = 0; k < NNode; k++ )
    for ( int i = 0; i < PhysDim::D; i++ )
      X[k][i] = k*i+1;

  for ( int k = 0; k < NNode; k++ )
    xElem.DOF(k) = X[k];

  for ( int k = 0; k < NNode; k++ )
    for ( int i = 0; i < PhysDim::D; i++ )
      BOOST_CHECK_CLOSE( X[k][i], xElem.DOF(k)[i], tol );

  ElementXField<PhysDim,TopoD3,Hex> xElem2(xElem);

  BOOST_CHECK_EQUAL( 1, xElem2.order() );
  BOOST_CHECK_EQUAL( NNode, xElem2.nDOF() );

  for ( int k = 0; k < NNode; k++ )
    for ( int i = 0; i < PhysDim::D; i++ )
      BOOST_CHECK_CLOSE( X[k][i], xElem2.DOF(k)[i], tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( volume_coordinates, PhysDim, Dimensions )
{
  const Real small_tol = 1e-13;
  const Real tol = 1e-13;

  static const int NNode = Hex::NNode;

  int order = 1;
  ElementXField<PhysDim,TopoD3,Hex> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( NNode, xElem.nDOF() );

  Real sRef, tRef, uRef;
  typename ElementXField<PhysDim,TopoD3,Hex>::VectorX XN[NNode];
  typename ElementXField<PhysDim,TopoD3,Hex>::VectorX X, XTrue;
  Real volume, volumeTrue;

  XN[0] = XN[1] = XN[2] = XN[3] = XN[4] = XN[5] = XN[6] = XN[7] = 0;

  XN[0][0] = 0;  XN[0][1] = 0;  XN[0][2] = 0;
  XN[1][0] = 1;  XN[1][1] = 0;  XN[1][2] = 0;
  XN[2][0] = 1;  XN[2][1] = 1;  XN[2][2] = 0;
  XN[3][0] = 0;  XN[3][1] = 1;  XN[3][2] = 0;

  XN[4][0] = 0;  XN[4][1] = 0;  XN[4][2] = 1;
  XN[5][0] = 1;  XN[5][1] = 0;  XN[5][2] = 1;
  XN[6][0] = 1;  XN[6][1] = 1;  XN[6][2] = 1;
  XN[7][0] = 0;  XN[7][1] = 1;  XN[7][2] = 1;

  volumeTrue = 1.;

  for ( int k = 0; k < NNode; k++ )
    xElem.DOF(k) = XN[k];

  for ( int k = 0; k < NNode; k++ )
  {
    X = xElem.DOF(k);
    for ( int i = 0; i < PhysDim::D; i++ )
      SANS_CHECK_CLOSE( XN[k][i], X[i], small_tol, tol );
  }

  volume = xElem.volume();
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );

  //------
  sRef = 0;  tRef = 0;  uRef = 0;
  XTrue = XN[0];
  xElem.coordinates( sRef, tRef, uRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  volume = xElem.volume( sRef, tRef, uRef );
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );

  //------
  sRef = 1;  tRef = 0;  uRef = 0;
  XTrue = XN[1];
  xElem.coordinates( sRef, tRef, uRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  volume = xElem.volume( sRef, tRef, uRef );
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );

  //------
  sRef = 1;  tRef = 1;  uRef = 0;
  XTrue = XN[2];
  xElem.coordinates( sRef, tRef, uRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  volume = xElem.volume( sRef, tRef, uRef );
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );

  //------
  sRef = 0;  tRef = 1;  uRef = 0;
  XTrue = XN[3];
  xElem.coordinates( sRef, tRef, uRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  volume = xElem.volume( sRef, tRef, uRef );
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );

  //------
  sRef = 0;  tRef = 0;  uRef = 1;
  XTrue = XN[4];
  xElem.coordinates( sRef, tRef, uRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  volume = xElem.volume( sRef, tRef, uRef );
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );

  //------
  sRef = 1;  tRef = 0;  uRef = 1;
  XTrue = XN[5];
  xElem.coordinates( sRef, tRef, uRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  volume = xElem.volume( sRef, tRef, uRef );
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );

  //------
  sRef = 1;  tRef = 1;  uRef = 1;
  XTrue = XN[6];
  xElem.coordinates( sRef, tRef, uRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  volume = xElem.volume( sRef, tRef, uRef );
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );

  //------
  sRef = 0;  tRef = 1;  uRef = 1;
  XTrue = XN[7];
  xElem.coordinates( sRef, tRef, uRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  volume = xElem.volume( sRef, tRef, uRef );
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );

  //------
  sRef = 1./2.;  tRef = 1./2.;  uRef = 1./2.;
  XTrue = 0;
  for ( int k = 0; k < NNode; k++ )
    XTrue += XN[k]/8.;

  xElem.coordinates( sRef, tRef, uRef, X );
  for ( int i = 0; i < PhysDim::D; i++ )
    BOOST_CHECK_CLOSE( XTrue[i], X[i], tol );

  volume = xElem.volume( sRef, tRef, uRef );
  BOOST_CHECK_CLOSE( volumeTrue, volume, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( eval_coordinate, PhysDim, Dimensions )
{
  const Real tol = 1e-13;

  static const int NNode = Hex::NNode;

  int order = 1;
  ElementXField<PhysDim,TopoD3,Hex> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( NNode, xElem.nDOF() );

  Real sRef, tRef, uRef;
  typename ElementXField<PhysDim,TopoD3,Hex>::VectorX XN[NNode];
  typename ElementXField<PhysDim,TopoD3,Hex>::VectorX X, Xd, Xs, Xt, Xu;
  typename ElementXField<PhysDim,TopoD3,Hex>::VectorX XTrue, XsTrue, XtTrue, XuTrue;

  XN[0] = XN[1] = XN[2] = XN[3] = XN[4] = XN[5] = XN[6] = XN[7] = 0;

  XN[0][0] = 0;  XN[0][1] = 0;  XN[0][2] = 0;
  XN[1][0] = 1;  XN[1][1] = 0;  XN[1][2] = 0;
  XN[2][0] = 1;  XN[2][1] = 1;  XN[2][2] = 0;
  XN[3][0] = 0;  XN[3][1] = 1;  XN[3][2] = 0;

  XN[4][0] = 0;  XN[4][1] = 0;  XN[4][2] = 1;
  XN[5][0] = 1;  XN[5][1] = 0;  XN[5][2] = 1;
  XN[6][0] = 1;  XN[6][1] = 1;  XN[6][2] = 1;
  XN[7][0] = 0;  XN[7][1] = 1;  XN[7][2] = 1;

  for ( int k = 0; k < NNode; k++ )
    xElem.DOF(k) = XN[k];

  //------
  sRef = 0;  tRef = 0;  uRef = 0;
  XTrue = XN[0];

  xElem.eval( sRef, tRef, uRef, X );
  xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, Xs, Xt, Xu );

  // Compute true derivatives using finite difference (which is exact for linear and the reference coordinates match the physical coordinates)
  sRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XsTrue = Xd - X;
  sRef -= 1;

  tRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XtTrue = Xd - X;
  tRef -= 1;

  uRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XuTrue = Xd - X;
  uRef -= 1;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    SANS_CHECK_CLOSE( XTrue[i], X[i], tol, tol );
    SANS_CHECK_CLOSE( XsTrue[i], Xs[i], tol, tol );
    SANS_CHECK_CLOSE( XtTrue[i], Xt[i], tol, tol );
    SANS_CHECK_CLOSE( XuTrue[i], Xu[i], tol, tol );
  }

  //------
  sRef = 1;  tRef = 0;  uRef = 0;
  XTrue = XN[1];

  xElem.eval( sRef, tRef, uRef, X );
  xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, Xs, Xt, Xu );

  // Compute true derivatives using finite difference (which is exact for linear and the reference coordinates match the physical coordinates)
  sRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XsTrue = Xd - X;
  sRef -= 1;

  tRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XtTrue = Xd - X;
  tRef -= 1;

  uRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XuTrue = Xd - X;
  uRef -= 1;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    SANS_CHECK_CLOSE( XTrue[i], X[i], tol, tol );
    SANS_CHECK_CLOSE( XsTrue[i], Xs[i], tol, tol );
    SANS_CHECK_CLOSE( XtTrue[i], Xt[i], tol, tol );
    SANS_CHECK_CLOSE( XuTrue[i], Xu[i], tol, tol );
  }

  //------
  sRef = 1;  tRef = 1;  uRef = 0;
  XTrue = XN[2];

  xElem.eval( sRef, tRef, uRef, X );
  xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, Xs, Xt, Xu );

  // Compute true derivatives using finite difference (which is exact for linear and the reference coordinates match the physical coordinates)
  sRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XsTrue = Xd - X;
  sRef -= 1;

  tRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XtTrue = Xd - X;
  tRef -= 1;

  uRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XuTrue = Xd - X;
  uRef -= 1;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    SANS_CHECK_CLOSE( XTrue[i], X[i], tol, tol );
    SANS_CHECK_CLOSE( XsTrue[i], Xs[i], tol, tol );
    SANS_CHECK_CLOSE( XtTrue[i], Xt[i], tol, tol );
    SANS_CHECK_CLOSE( XuTrue[i], Xu[i], tol, tol );
  }

  //------
  sRef = 0;  tRef = 1;  uRef = 0;
  XTrue = XN[3];

  xElem.eval( sRef, tRef, uRef, X );
  xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, Xs, Xt, Xu );

  // Compute true derivatives using finite difference (which is exact for linear and the reference coordinates match the physical coordinates)
  sRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XsTrue = Xd - X;
  sRef -= 1;

  tRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XtTrue = Xd - X;
  tRef -= 1;

  uRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XuTrue = Xd - X;
  uRef -= 1;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    SANS_CHECK_CLOSE( XTrue[i], X[i], tol, tol );
    SANS_CHECK_CLOSE( XsTrue[i], Xs[i], tol, tol );
    SANS_CHECK_CLOSE( XtTrue[i], Xt[i], tol, tol );
    SANS_CHECK_CLOSE( XuTrue[i], Xu[i], tol, tol );
  }

  //------
  sRef = 0;  tRef = 0;  uRef = 1;
  XTrue = XN[4];

  xElem.eval( sRef, tRef, uRef, X );
  xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, Xs, Xt, Xu );

  // Compute true derivatives using finite difference (which is exact for linear and the reference coordinates match the physical coordinates)
  sRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XsTrue = Xd - X;
  sRef -= 1;

  tRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XtTrue = Xd - X;
  tRef -= 1;

  uRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XuTrue = Xd - X;
  uRef -= 1;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    SANS_CHECK_CLOSE( XTrue[i], X[i], tol, tol );
    SANS_CHECK_CLOSE( XsTrue[i], Xs[i], tol, tol );
    SANS_CHECK_CLOSE( XtTrue[i], Xt[i], tol, tol );
    SANS_CHECK_CLOSE( XuTrue[i], Xu[i], tol, tol );
  }

  //------
  sRef = 1;  tRef = 0;  uRef = 1;
  XTrue = XN[5];

  xElem.eval( sRef, tRef, uRef, X );
  xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, Xs, Xt, Xu );

  // Compute true derivatives using finite difference (which is exact for linear and the reference coordinates match the physical coordinates)
  sRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XsTrue = Xd - X;
  sRef -= 1;

  tRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XtTrue = Xd - X;
  tRef -= 1;

  uRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XuTrue = Xd - X;
  uRef -= 1;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    SANS_CHECK_CLOSE( XTrue[i], X[i], tol, tol );
    SANS_CHECK_CLOSE( XsTrue[i], Xs[i], tol, tol );
    SANS_CHECK_CLOSE( XtTrue[i], Xt[i], tol, tol );
    SANS_CHECK_CLOSE( XuTrue[i], Xu[i], tol, tol );
  }

  //------
  sRef = 1;  tRef = 1;  uRef = 1;
  XTrue = XN[6];

  xElem.eval( sRef, tRef, uRef, X );
  xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, Xs, Xt, Xu );

  // Compute true derivatives using finite difference (which is exact for linear and the reference coordinates match the physical coordinates)
  sRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XsTrue = Xd - X;
  sRef -= 1;

  tRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XtTrue = Xd - X;
  tRef -= 1;

  uRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XuTrue = Xd - X;
  uRef -= 1;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    SANS_CHECK_CLOSE( XTrue[i], X[i], tol, tol );
    SANS_CHECK_CLOSE( XsTrue[i], Xs[i], tol, tol );
    SANS_CHECK_CLOSE( XtTrue[i], Xt[i], tol, tol );
    SANS_CHECK_CLOSE( XuTrue[i], Xu[i], tol, tol );
  }

  //------
  sRef = 0;  tRef = 1;  uRef = 1;
  XTrue = XN[7];

  xElem.eval( sRef, tRef, uRef, X );
  xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, Xs, Xt, Xu );

  // Compute true derivatives using finite difference (which is exact for linear and the reference coordinates match the physical coordinates)
  sRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XsTrue = Xd - X;
  sRef -= 1;

  tRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XtTrue = Xd - X;
  tRef -= 1;

  uRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XuTrue = Xd - X;
  uRef -= 1;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    SANS_CHECK_CLOSE( XTrue[i], X[i], tol, tol );
    SANS_CHECK_CLOSE( XsTrue[i], Xs[i], tol, tol );
    SANS_CHECK_CLOSE( XtTrue[i], Xt[i], tol, tol );
    SANS_CHECK_CLOSE( XuTrue[i], Xu[i], tol, tol );
  }

  //------
  sRef = 1./2.;  tRef = 1./2.;  uRef = 1./2.;
  XTrue = 0;
  for ( int k = 0; k < NNode; k++ )
    XTrue += XN[k]/8.;

  xElem.eval( sRef, tRef, uRef, X );
  xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, Xs, Xt, Xu );

  // Compute true derivatives using finite difference (which is exact for linear and the reference coordinates match the physical coordinates)
  sRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XsTrue = Xd - X;
  sRef -= 1;

  tRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XtTrue = Xd - X;
  tRef -= 1;

  uRef += 1;
  xElem.eval( sRef, tRef, uRef, Xd );
  XuTrue = Xd - X;
  uRef -= 1;

  for ( int i = 0; i < PhysDim::D; i++ )
  {
    SANS_CHECK_CLOSE( XTrue[i], X[i], tol, tol );
    SANS_CHECK_CLOSE( XsTrue[i], Xs[i], tol, tol );
    SANS_CHECK_CLOSE( XtTrue[i], Xt[i], tol, tol );
    SANS_CHECK_CLOSE( XuTrue[i], Xu[i], tol, tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( evalBasisGradient )
{
  typedef BasisFunctionVolume<Hex,Hierarchical,1> BasisClass;

  typedef PhysD3 PhysDim;
  static const int NNode = Hex::NNode;

  int order = 1;
  ElementXField<PhysD3,TopoD3,Hex> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( NNode, xElem.nDOF() );

  const BasisClass* basis = BasisClass::self();
  Element<Real,TopoD3,Hex> qfldElem(basis);

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( NNode, basis->nBasis() );

  const Real tol = 1e-13;
  typename ElementXField<PhysDim,TopoD3,Hex>::VectorX XN[NNode];
  typename ElementXField<PhysD3,TopoD3,Hex>::RefCoordType sRef;
  typename ElementXField<PhysD3,TopoD3,Hex>::VectorX gradphi[NNode], gradphiTrue[NNode];
  Real phi[NNode], phi1[NNode];

  XN[0] = XN[1] = XN[2] = XN[3] = XN[4] = XN[5] = XN[6] = XN[7] = 0;

  XN[0][0] = 0;  XN[0][1] = 0;  XN[0][2] = 0;
  XN[1][0] = 1;  XN[1][1] = 0;  XN[1][2] = 0;
  XN[2][0] = 1;  XN[2][1] = 1;  XN[2][2] = 0;
  XN[3][0] = 0;  XN[3][1] = 1;  XN[3][2] = 0;

  XN[4][0] = 0;  XN[4][1] = 0;  XN[4][2] = 1;
  XN[5][0] = 1;  XN[5][1] = 0;  XN[5][2] = 1;
  XN[6][0] = 1;  XN[6][1] = 1;  XN[6][2] = 1;
  XN[7][0] = 0;  XN[7][1] = 1;  XN[7][2] = 1;

  for ( int k = 0; k < NNode; k++ )
    xElem.DOF(k) = XN[k];

  //------
  sRef[0] = 0;  sRef[1] = 0;  sRef[2] = 0;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, NNode );

  // Compute true derivatives using finite difference (which is exact for linear)
  // This is value because the hex is a unit hex
  xElem.evalBasis( sRef, phi, NNode );
  for ( int d = 0; d < PhysDim::D; d++ )
  {
    sRef[d] += 1;
    xElem.evalBasis( sRef, phi1, NNode );
    for (int k = 0; k < NNode; k++) gradphiTrue[k][d] = phi1[k] - phi[k];
    sRef[d] -= 1;
  }

  for ( int k = 0; k < NNode; k++)
    for ( int d = 0; d < PhysDim::D; d++ )
      BOOST_CHECK_CLOSE( gradphiTrue[k][d], gradphi[k][d], tol );

  //------
  sRef[0] = 1;  sRef[1] = 0;  sRef[2] = 0;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, NNode );

  // Compute true derivatives using finite difference (which is exact for linear)
  // This is value because the hex is a unit hex
  xElem.evalBasis( sRef, phi, NNode );
  for ( int d = 0; d < PhysDim::D; d++ )
  {
    sRef[d] += 1;
    xElem.evalBasis( sRef, phi1, NNode );
    for (int k = 0; k < NNode; k++) gradphiTrue[k][d] = phi1[k] - phi[k];
    sRef[d] -= 1;
  }

  for ( int k = 0; k < NNode; k++)
    for ( int d = 0; d < PhysDim::D; d++ )
      BOOST_CHECK_CLOSE( gradphiTrue[k][d], gradphi[k][d], tol );

  //------
  sRef[0] = 1;  sRef[1] = 1;  sRef[2] = 0;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, NNode );

  // Compute true derivatives using finite difference (which is exact for linear)
  // This is value because the hex is a unit hex
  xElem.evalBasis( sRef, phi, NNode );
  for ( int d = 0; d < PhysDim::D; d++ )
  {
    sRef[d] += 1;
    xElem.evalBasis( sRef, phi1, NNode );
    for (int k = 0; k < NNode; k++) gradphiTrue[k][d] = phi1[k] - phi[k];
    sRef[d] -= 1;
  }

  for ( int k = 0; k < NNode; k++)
    for ( int d = 0; d < PhysDim::D; d++ )
      BOOST_CHECK_CLOSE( gradphiTrue[k][d], gradphi[k][d], tol );

  //------
  sRef[0] = 0;  sRef[1] = 1;  sRef[2] = 0;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, NNode );

  // Compute true derivatives using finite difference (which is exact for linear)
  // This is value because the hex is a unit hex
  xElem.evalBasis( sRef, phi, NNode );
  for ( int d = 0; d < PhysDim::D; d++ )
  {
    sRef[d] += 1;
    xElem.evalBasis( sRef, phi1, NNode );
    for (int k = 0; k < NNode; k++) gradphiTrue[k][d] = phi1[k] - phi[k];
    sRef[d] -= 1;
  }

  for ( int k = 0; k < NNode; k++)
    for ( int d = 0; d < PhysDim::D; d++ )
      BOOST_CHECK_CLOSE( gradphiTrue[k][d], gradphi[k][d], tol );

  //------
  sRef[0] = 0;  sRef[1] = 0;  sRef[2] = 1;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, NNode );

  // Compute true derivatives using finite difference (which is exact for linear)
  // This is value because the hex is a unit hex
  xElem.evalBasis( sRef, phi, NNode );
  for ( int d = 0; d < PhysDim::D; d++ )
  {
    sRef[d] += 1;
    xElem.evalBasis( sRef, phi1, NNode );
    for (int k = 0; k < NNode; k++) gradphiTrue[k][d] = phi1[k] - phi[k];
    sRef[d] -= 1;
  }

  for ( int k = 0; k < NNode; k++)
    for ( int d = 0; d < PhysDim::D; d++ )
      BOOST_CHECK_CLOSE( gradphiTrue[k][d], gradphi[k][d], tol );

  //------
  sRef[0] = 1;  sRef[1] = 0;  sRef[2] = 1;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, NNode );

  // Compute true derivatives using finite difference (which is exact for linear)
  // This is value because the hex is a unit hex
  xElem.evalBasis( sRef, phi, NNode );
  for ( int d = 0; d < PhysDim::D; d++ )
  {
    sRef[d] += 1;
    xElem.evalBasis( sRef, phi1, NNode );
    for (int k = 0; k < NNode; k++) gradphiTrue[k][d] = phi1[k] - phi[k];
    sRef[d] -= 1;
  }

  for ( int k = 0; k < NNode; k++)
    for ( int d = 0; d < PhysDim::D; d++ )
      BOOST_CHECK_CLOSE( gradphiTrue[k][d], gradphi[k][d], tol );

  //------
  sRef[0] = 1;  sRef[1] = 1;  sRef[2] = 1;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, NNode );

  // Compute true derivatives using finite difference (which is exact for linear)
  // This is value because the hex is a unit hex
  xElem.evalBasis( sRef, phi, NNode );
  for ( int d = 0; d < PhysDim::D; d++ )
  {
    sRef[d] += 1;
    xElem.evalBasis( sRef, phi1, NNode );
    for (int k = 0; k < NNode; k++) gradphiTrue[k][d] = phi1[k] - phi[k];
    sRef[d] -= 1;
  }

  for ( int k = 0; k < NNode; k++)
    for ( int d = 0; d < PhysDim::D; d++ )
      BOOST_CHECK_CLOSE( gradphiTrue[k][d], gradphi[k][d], tol );

  //------
  sRef[0] = 0;  sRef[1] = 1;  sRef[2] = 1;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, NNode );

  // Compute true derivatives using finite difference (which is exact for linear)
  // This is value because the hex is a unit hex
  xElem.evalBasis( sRef, phi, NNode );
  for ( int d = 0; d < PhysDim::D; d++ )
  {
    sRef[d] += 1;
    xElem.evalBasis( sRef, phi1, NNode );
    for (int k = 0; k < NNode; k++) gradphiTrue[k][d] = phi1[k] - phi[k];
    sRef[d] -= 1;
  }

  for ( int k = 0; k < NNode; k++)
    for ( int d = 0; d < PhysDim::D; d++ )
      BOOST_CHECK_CLOSE( gradphiTrue[k][d], gradphi[k][d], tol );

  //------
  sRef[0] = 1./2.;  sRef[1] = 1./2.;  sRef[2] = 1./2.;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, NNode );

  // Compute true derivatives using finite difference (which is exact for linear)
  // This is value because the hex is a unit hex
  xElem.evalBasis( sRef, phi, NNode );
  for ( int d = 0; d < PhysDim::D; d++ )
  {
    sRef[d] += 1;
    xElem.evalBasis( sRef, phi1, NNode );
    for (int k = 0; k < NNode; k++) gradphiTrue[k][d] = phi1[k] - phi[k];
    sRef[d] -= 1;
  }

  for ( int k = 0; k < NNode; k++)
    for ( int d = 0; d < PhysDim::D; d++ )
      BOOST_CHECK_CLOSE( gradphiTrue[k][d], gradphi[k][d], tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( evalReferenceCoordinateHessian )
{
  const Real tol = 1e-11;

  const int (*EdgeNodes)[ Line::NNode ] = ElementEdges<Hex>::EdgeNodes;

  typedef typename ElementXField<PhysD3,TopoD3,Hex>::VectorX VectorX;
  VectorX dX;
  typename ElementXField<PhysD3,TopoD3,Hex>::VectorX sgrad, tgrad, ugrad;
  typename ElementXField<PhysD3,TopoD3,Hex>::Matrix invJ;

  typename ElementXField<PhysD3,TopoD3,Hex>::TensorSymX shess, thess, uhess;
  typename ElementXField<PhysD3,TopoD3,Hex>::TensorSymX shessTrue, thessTrue, uhessTrue;
  typename ElementXField<PhysD3,TopoD3,Hex>::TensorSymHessian H;
  typename ElementXField<PhysD3,TopoD3,Hex>::TensorSymX hesstmp0, hesstmp1, hesstmp2;

  Real sRef = 0.3, tRef=0.4, uRef = 0.25;

  int order = 2;
  ElementXField<PhysD3,TopoD3,Hex> xElem(order, BasisFunctionCategory_Lagrange);


  // Node DOFs
  xElem.DOF(0) = {0, 0, 0};
  xElem.DOF(1) = {1, 0, 0};
  xElem.DOF(2) = {1, 1, 0};
  xElem.DOF(3) = {0, 1, 0};

  xElem.DOF(4) = {0, 0, 1};
  xElem.DOF(5) = {1, 0, 1};
  xElem.DOF(6) = {1, 1, 1};
  xElem.DOF(7) = {0, 1, 1};

  // Edge DOFs
  for (int n = 0; n < Hex::NEdge; n++)
  {
    xElem.DOF(8+n) = (xElem.DOF(EdgeNodes[n][0]) + xElem.DOF(EdgeNodes[n][1]))/2;
    //dX = {0.1 * (n % 2), 0.1 * (n % 3), -0.1 * (n % 2)};
    //xElem.DOF(8+n) += dX;
  }

  // Face DOFs
  xElem.DOF(20) = {  0.5,  0.5, -0.1 };
  xElem.DOF(21) = {  0.5, -0.1,  0.5 };
  xElem.DOF(22) = {  1.1,  0.5,  0.5 };
  xElem.DOF(23) = {  0.5,  1.1,  0.5 };
  xElem.DOF(24) = { -0.1,  0.5,  0.5 };
  xElem.DOF(25) = {  0.5,  0.5,  1.1 };

  // Cell DOFs
  xElem.DOF(26) = { 0.5, 0.5, 0.5 };

  xElem.hessian( sRef, tRef, uRef, H );
  xElem.evalReferenceCoordinateGradient( sRef, tRef, uRef, sgrad, tgrad, ugrad );
  xElem.evalReferenceCoordinateHessian( sRef, tRef, uRef, shess, thess, uhess );

  invJ(0,0) = sgrad[0];
  invJ(0,1) = sgrad[1];
  invJ(0,2) = sgrad[2];

  invJ(1,0) = tgrad[0];
  invJ(1,1) = tgrad[1];
  invJ(1,2) = tgrad[2];

  invJ(2,0) = ugrad[0];
  invJ(2,1) = ugrad[1];
  invJ(2,2) = ugrad[2];

  hesstmp0 = Transpose(invJ)*H[0]*invJ;
  hesstmp1 = Transpose(invJ)*H[1]*invJ;
  hesstmp2 = Transpose(invJ)*H[2]*invJ;

  shessTrue = -(invJ(0,0)*hesstmp0 + invJ(0,1)*hesstmp1 + invJ(0,2)*hesstmp2);
  thessTrue = -(invJ(1,0)*hesstmp0 + invJ(1,1)*hesstmp1 + invJ(1,2)*hesstmp2);
  uhessTrue = -(invJ(2,0)*hesstmp0 + invJ(2,1)*hesstmp1 + invJ(2,2)*hesstmp2);

  for (int i = 0; i < PhysD3::D; i++)
    for (int j = 0; j <= i; j++)
    {
      BOOST_CHECK_CLOSE( shessTrue(i,j), shess(i,j), tol );
      BOOST_CHECK_CLOSE( thessTrue(i,j), thess(i,j), tol );
      BOOST_CHECK_CLOSE( uhessTrue(i,j), uhess(i,j), tol );
    }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( extract_basis, PhysDim, Dimensions )
{
  int order = 1;
  ElementXField<PhysDim,TopoD3,Hex> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 8, xElem.nDOF() );

  const BasisFunctionVolumeBase<Hex>* basis = xElem.basis();

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( 8, basis->nBasis() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( face_sign, PhysDim, Dimensions )
{
  typedef std::array<int,6> Int6;

  int order = 1;
  ElementXField<PhysDim,TopoD3,Hex> xElem(order, BasisFunctionCategory_Hierarchical);

  Int6 faceSign = xElem.faceSign();
  BOOST_CHECK_EQUAL( +1, faceSign[0] );
  BOOST_CHECK_EQUAL( +1, faceSign[1] );
  BOOST_CHECK_EQUAL( +1, faceSign[2] );
  BOOST_CHECK_EQUAL( +1, faceSign[3] );
  BOOST_CHECK_EQUAL( +1, faceSign[4] );
  BOOST_CHECK_EQUAL( +1, faceSign[5] );

  faceSign[1] = -1;
  xElem.setFaceSign( faceSign );

  Int6 faceSign2 = xElem.faceSign();
  BOOST_CHECK_EQUAL( +1, faceSign2[0] );
  BOOST_CHECK_EQUAL( -1, faceSign2[1] );
  BOOST_CHECK_EQUAL( +1, faceSign2[2] );
  BOOST_CHECK_EQUAL( +1, faceSign2[3] );
  BOOST_CHECK_EQUAL( +1, faceSign2[4] );
  BOOST_CHECK_EQUAL( +1, faceSign2[5] );
}


//----------------------------------------------------------------------------//
// check that the trace element coordinates evaluate to the volume element coordinates on the trace
BOOST_AUTO_TEST_CASE( Trace_RefCoord_P1 )
{
  typedef ElementXField<PhysD3,TopoD3,Hex> ElementXFieldVolumeClass;
  typedef ElementXField<PhysD3,TopoD2,Quad> ElementXFieldAreaClass;
  typedef ElementXFieldVolumeClass::VectorX VectorX;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  // element field variables
  ElementXFieldAreaClass xfldElemFaceP( BasisFunctionAreaBase<Quad>::HierarchicalP1 );
  ElementXFieldAreaClass xfldElemFaceN( BasisFunctionAreaBase<Quad>::HierarchicalP1 );
  ElementXFieldVolumeClass xfldElem( BasisFunctionVolumeBase<Hex>::HierarchicalP1 );

  xfldElem.DOF(0) = {0, 0, 0};
  xfldElem.DOF(1) = {1, 0, 0};
  xfldElem.DOF(2) = {1, 1, 0};
  xfldElem.DOF(3) = {0, 1, 0};

  xfldElem.DOF(4) = {0, 0, 1};
  xfldElem.DOF(5) = {1, 0, 1};
  xfldElem.DOF(6) = {1, 1, 1};
  xfldElem.DOF(7) = {0, 1, 1};

  const int hex[8] = {0, 1, 2, 3, 4, 5, 6, 7};

  const int (*TraceNodes)[ Quad::NTrace ] = TraceToCellRefCoord<Quad, TopoD3, Hex>::TraceNodes;

  const int (*OrientPos)[ Quad::NTrace ] = TraceToCellRefCoord<Quad, TopoD3, Hex>::OrientPos;
  const int (*OrientNeg)[ Quad::NTrace ] = TraceToCellRefCoord<Quad, TopoD3, Hex>::OrientNeg;


  for (int face = 0; face < Hex::NTrace; face++)
  {
    for (int orient = 0; orient < Quad::NTrace; orient++)
    {
      // Get the re-oriented triangles
      const int quadP[4] = {hex[TraceNodes[face][OrientPos[orient][0]]],
                            hex[TraceNodes[face][OrientPos[orient][1]]],
                            hex[TraceNodes[face][OrientPos[orient][2]]],
                            hex[TraceNodes[face][OrientPos[orient][3]]]};

      const int quadN[4] = {hex[TraceNodes[face][OrientNeg[orient][0]]],
                            hex[TraceNodes[face][OrientNeg[orient][1]]],
                            hex[TraceNodes[face][OrientNeg[orient][2]]],
                            hex[TraceNodes[face][OrientNeg[orient][3]]]};

      const CanonicalTraceToCell canonicalFaceP = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTrace(quadP, 4, hex, 8);
      const CanonicalTraceToCell canonicalFaceN = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTrace(quadN, 4, hex, 8);

      BOOST_REQUIRE_EQUAL(canonicalFaceP.trace, face);
      BOOST_REQUIRE_EQUAL(canonicalFaceP.orientation, orient+1);

      BOOST_REQUIRE_EQUAL(canonicalFaceN.trace, face);
      BOOST_REQUIRE_EQUAL(canonicalFaceN.orientation, -orient-1);

      // Set the DOF's on the trace
      for (int n = 0; n < Quad::NNode; n++)
      {
        xfldElemFaceP.DOF(n) = xfldElem.DOF(quadP[n]);
        xfldElemFaceN.DOF(n) = xfldElem.DOF(quadN[n]);
      }

      int kmax = 5;
      for (int k = 0; k < kmax; k++)
      {
        DLA::VectorS<2,Real> sRefTrace;
        DLA::VectorS<3,Real> sRefCell;
        VectorX xTrace, xCell;
        Real s, t;

        // Edge 0
        s = k/static_cast<Real>(kmax-1);
        t = 0;

        sRefTrace = {s,t};

        // volume reference-element coords
        TraceToCellRefCoord<Quad, TopoD3, Hex>::eval( canonicalFaceP, sRefTrace, sRefCell );

        // element coordinates from trace and cell
        xfldElemFaceP.eval( sRefTrace, xTrace );
        xfldElem.eval( sRefCell, xCell );

        SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );

        // volume reference-element coords
        TraceToCellRefCoord<Quad, TopoD3, Hex>::eval( canonicalFaceN, sRefTrace, sRefCell );

        // element coordinates from trace and cell
        xfldElemFaceN.eval( sRefTrace, xTrace );
        xfldElem.eval( sRefCell, xCell );

        SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );


        // Edge 1
        s = 1;
        t = k/static_cast<Real>(kmax-1);

        sRefTrace = {s,t};

        // left/right reference-element coords
        TraceToCellRefCoord<Quad, TopoD3, Hex>::eval( canonicalFaceP, sRefTrace, sRefCell );

        // element coordinates from trace and cell
        xfldElemFaceP.eval( sRefTrace, xTrace );
        xfldElem.eval( sRefCell, xCell );

        SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );

        // volume reference-element coords
        TraceToCellRefCoord<Quad, TopoD3, Hex>::eval( canonicalFaceN, sRefTrace, sRefCell );

        // element coordinates from trace and cell
        xfldElemFaceN.eval( sRefTrace, xTrace );
        xfldElem.eval( sRefCell, xCell );

        SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );


        // Edge 2
        s = 1-k/static_cast<Real>(kmax-1);
        t = 1;

        sRefTrace = {s,t};

        // left/right reference-element coords
        TraceToCellRefCoord<Quad, TopoD3, Hex>::eval( canonicalFaceP, sRefTrace, sRefCell );

        // element coordinates from trace and cell
        xfldElemFaceP.eval( sRefTrace, xTrace );
        xfldElem.eval( sRefCell, xCell );

        SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );

        // volume reference-element coords
        TraceToCellRefCoord<Quad, TopoD3, Hex>::eval( canonicalFaceN, sRefTrace, sRefCell );

        // element coordinates from trace and cell
        xfldElemFaceN.eval( sRefTrace, xTrace );
        xfldElem.eval( sRefCell, xCell );

        SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );


        // Edge 3
        s = 0;
        t = 1-k/static_cast<Real>(kmax-1);

        sRefTrace = {s,t};

        // left/right reference-element coords
        TraceToCellRefCoord<Quad, TopoD3, Hex>::eval( canonicalFaceP, sRefTrace, sRefCell );

        // element coordinates from trace and cell
        xfldElemFaceP.eval( sRefTrace, xTrace );
        xfldElem.eval( sRefCell, xCell );

        SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );

        // volume reference-element coords
        TraceToCellRefCoord<Quad, TopoD3, Hex>::eval( canonicalFaceN, sRefTrace, sRefCell );

        // element coordinates from trace and cell
        xfldElemFaceN.eval( sRefTrace, xTrace );
        xfldElem.eval( sRefCell, xCell );

        SANS_CHECK_CLOSE( xTrace[0], xCell[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[1], xCell[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( xTrace[2], xCell[2], small_tol, close_tol );
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( impliedMetric )
{
  const Real small_tol = 1e-13;
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysD3, TopoD3, Hex> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 8, xElem.nDOF() );

  ElementXField<PhysD3, TopoD3, Hex>::RefCoordType sRef = {0.0, 0.0, 0.0};

  DLA::MatrixSymS<PhysD3::D, Real> M;

  //Unit hex
  xElem.DOF(0) = {0.0, 0.0, 0.0};
  xElem.DOF(1) = {1.0, 0.0, 0.0};
  xElem.DOF(2) = {1.0, 1.0, 0.0};
  xElem.DOF(3) = {0.0, 1.0, 0.0};
  xElem.DOF(4) = {0.0, 0.0, 1.0};
  xElem.DOF(5) = {1.0, 0.0, 1.0};
  xElem.DOF(6) = {1.0, 1.0, 1.0};
  xElem.DOF(7) = {0.0, 1.0, 1.0};

  xElem.impliedMetric(sRef, M);

  SANS_CHECK_CLOSE( M(0,0), 1.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,2), 0.0, small_tol, tol );

  SANS_CHECK_CLOSE( M(1,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,1), 1.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,2), 0.0, small_tol, tol );

  SANS_CHECK_CLOSE( M(2,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(2,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(2,2), 1.0, small_tol, tol );

  M = 0;
  xElem.impliedMetric(M); //evaluate at centerRef

  SANS_CHECK_CLOSE( M(0,0), 1.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,2), 0.0, small_tol, tol );

  SANS_CHECK_CLOSE( M(1,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,1), 1.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,2), 0.0, small_tol, tol );

  SANS_CHECK_CLOSE( M(2,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(2,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(2,2), 1.0, small_tol, tol );

  //Unit tet - scaled by 2.5, translated by (0.6, 1.3, 3.2)
  Real scale = 2.5;
  Real xshift[3] = {0.6, 1.3, 3.2};
  for (int i=0; i<8; i++)
    for (int d=0; d<3; d++)
      xElem.DOF(i)[d] = xElem.DOF(i)[d]*scale + xshift[d];

  xElem.impliedMetric(sRef, M);

  SANS_CHECK_CLOSE( M(0,0), 1.0/pow(scale,2.0), small_tol, tol );
  SANS_CHECK_CLOSE( M(0,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(0,2), 0.0, small_tol, tol );

  SANS_CHECK_CLOSE( M(1,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(1,1), 1.0/pow(scale,2.0), small_tol, tol );
  SANS_CHECK_CLOSE( M(1,2), 0.0, small_tol, tol );

  SANS_CHECK_CLOSE( M(2,0), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(2,1), 0.0, small_tol, tol );
  SANS_CHECK_CLOSE( M(2,2), 1.0/pow(scale,2.0), small_tol, tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BoundingBox_Q1_test )
{
  int order = 1;
  ElementXField<PhysD3,TopoD3,Hex> xElem(order, BasisFunctionCategory_Hierarchical);

  xElem.DOF(0) = {0, 0, 0};
  xElem.DOF(1) = {1, 0, 0};
  xElem.DOF(2) = {1, 1, 0};
  xElem.DOF(3) = {0, 1, 0};

  xElem.DOF(4) = {0, 0, 1};
  xElem.DOF(5) = {1, 0, 1};
  xElem.DOF(6) = {1, 1, 1};
  xElem.DOF(7) = {0, 1, 1};

  BoundingBox<PhysD3> bbox = xElem.boundingBox();

  BOOST_CHECK_EQUAL(0, bbox.low_bounds()[0]);
  BOOST_CHECK_EQUAL(0, bbox.low_bounds()[1]);
  BOOST_CHECK_EQUAL(0, bbox.low_bounds()[2]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[0]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[1]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[2]);
}

#if 0 // Don't have Lagrange Hex yet...
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BoundingBox_Q2_test )
{
  int order = 2;
  ElementXField<PhysD3,TopoD3,Hex> xElem(order, BasisFunctionCategory_Lagrange);

  // node DOFs
  xElem.DOF(0) = {0, 0, 0};
  xElem.DOF(1) = {1, 0, 0};
  xElem.DOF(2) = {1, 1, 0};
  xElem.DOF(3) = {0, 1, 0};

  xElem.DOF(4) = {0, 0, 1};
  xElem.DOF(5) = {1, 0, 1};
  xElem.DOF(6) = {1, 1, 1};
  xElem.DOF(7) = {0, 1, 1};

  BoundingBox<PhysD2> bbox = xElem.boundingBox();

  BOOST_CHECK_EQUAL(0, bbox.low_bounds()[0]);
  BOOST_CHECK_EQUAL(0, bbox.low_bounds()[1]);
  BOOST_CHECK_EQUAL(0, bbox.low_bounds()[2]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[0]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[1]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[2]);
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( IO, PhysDim, Dimensions )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/ElementXField" + stringify((int)PhysDim::D) + "DVolume_Hexahedron_pattern.txt", true );

  ElementXField<PhysDim,TopoD3,Hex> xElem(1, BasisFunctionCategory_Hierarchical);

  static const int NNode = Hex::NNode;

  typename ElementXField<PhysDim,TopoD3,Hex>::VectorX XN[NNode];

  XN[0] = XN[1] = XN[2] = XN[3] = XN[4] = XN[5] = XN[6] = XN[7] = 0;

  XN[0][0] = 0;  XN[0][1] = 0;  XN[0][2] = 0;
  XN[1][0] = 1;  XN[1][1] = 0;  XN[1][2] = 0;
  XN[2][0] = 1;  XN[2][1] = 1;  XN[2][2] = 0;
  XN[3][0] = 0;  XN[3][1] = 1;  XN[3][2] = 0;

  XN[4][0] = 0;  XN[4][1] = 0;  XN[4][2] = 1;
  XN[5][0] = 1;  XN[5][1] = 0;  XN[5][2] = 1;
  XN[6][0] = 1;  XN[6][1] = 1;  XN[6][2] = 1;
  XN[7][0] = 0;  XN[7][1] = 1;  XN[7][2] = 1;

  for ( int k = 0; k < NNode; k++ )
    xElem.DOF(k) = XN[k];

  xElem.dump( 2, output );
  xElem.dumpTecplot( output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
