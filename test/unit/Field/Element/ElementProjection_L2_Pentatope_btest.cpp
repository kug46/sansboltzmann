// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementProjection_L2_Tet_btest

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Field/Element/ElementProjection_L2.h"
#include "Field/Element/ElementVolume.h"
#include "Field/Element/ElementSpacetime.h"
#include "Field/Element/ElementXFieldSpacetime.h"

// Included to get BasisFunctionSpacetime_Pentatope_LegendrePMax
// Remove this when BasisFunctionSpacetime_Pentatope_LegendrePMax is no longer needed
#include "BasisFunction/BasisFunctionSpacetime_Pentatope_Legendre.h"
#include "BasisFunction/BasisFunctionSpacetime_Pentatope_Lagrange.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE(ElementProjection_L2_Pentatope_test_suite)

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(ElementProjection_Const_L2_Legendre)
{
  typedef Real ArrayQ;
  typedef Element<ArrayQ, TopoD4, Pentatope> ElementQFieldClass;

  int order= 0;
  ElementQFieldClass qfld0(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL(0, qfld0.order());
  BOOST_CHECK_EQUAL(1, qfld0.nDOF());

  ArrayQ aconst= 2;

  ElementProjectionConst_L2<TopoD4, Pentatope> projector0(qfld0.basis());

  projector0.project(qfld0, aconst);

  Real tol= 1e-13;
  BOOST_CHECK_CLOSE(aconst, qfld0.eval({0.3, 0.3, 0.3, 0.3}), tol);
  BOOST_CHECK_CLOSE(aconst, qfld0.DOF(0), tol);

  order= 1;
  ElementQFieldClass qfld1(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL(1, qfld1.order());
  BOOST_CHECK_EQUAL(5, qfld1.nDOF());

  ElementProjectionConst_L2<TopoD4, Pentatope> projector1(qfld1.basis());

  projector1.project(qfld1, aconst);

  BOOST_CHECK_CLOSE(aconst, qfld1.eval({0.3, 0.3, 0.3, 0.3}), tol);
  BOOST_CHECK_CLOSE(aconst, qfld1.DOF(0), tol);
  BOOST_CHECK_SMALL(qfld1.DOF(1), tol);
  BOOST_CHECK_SMALL(qfld1.DOF(2), tol);
  BOOST_CHECK_SMALL(qfld1.DOF(3), tol);
  BOOST_CHECK_SMALL(qfld1.DOF(4), tol);

  order= 2;
  ElementQFieldClass qfld2(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL(2, qfld2.order());
  BOOST_CHECK_EQUAL(15, qfld2.nDOF());

  ElementProjectionConst_L2<TopoD4, Pentatope> projector2(qfld2.basis());

  projector2.project(qfld2, aconst);

  BOOST_CHECK_CLOSE(aconst, qfld2.eval({0.3, 0.3, 0.3, 0.3}), tol);
  BOOST_CHECK_CLOSE(aconst, qfld2.DOF(0), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(1), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(2), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(3), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(4), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(5), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(6), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(7), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(8), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(9), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(10), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(11), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(12), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(13), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(14), tol);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementProjection_Const_L2_Hierarchical )
{
  typedef Real ArrayQ;
  typedef Element<ArrayQ, TopoD4, Pentatope> ElementQFieldClass;

#if 0
  ArrayQ aconst= 2;

  int order= 1;
  ElementQFieldClass qfld1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL(1, qfld1.order());
  BOOST_CHECK_EQUAL(4, qfld1.nDOF());

  ElementProjectionConst_L2<TopoD4, Pentatope> projector1(qfld1.basis());

  projector1.project(qfld1, aconst);

  Real tol= 5e-12;
  BOOST_CHECK_CLOSE(aconst, qfld1.eval({0.3, 0.3, 0.3}), tol);
  BOOST_CHECK_CLOSE(aconst, qfld1.DOF(0), tol);
  BOOST_CHECK_CLOSE(aconst, qfld1.DOF(1), tol);
  BOOST_CHECK_CLOSE(aconst, qfld1.DOF(2), tol);
  BOOST_CHECK_CLOSE(aconst, qfld1.DOF(3), tol);
  BOOST_CHECK_CLOSE(aconst, qfld1.DOF(4), tol);

  order= 2;
  ElementQFieldClass qfld2(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL(2, qfld2.order());
  BOOST_CHECK_EQUAL(10, qfld2.nDOF());

  ElementProjectionConst_L2<TopoD4, Pentatope> projector2(qfld2.basis());

  projector2.project(qfld2, aconst);

  BOOST_CHECK_CLOSE(aconst, qfld2.eval({0.3, 0.3, 0.3}), tol);
  BOOST_CHECK_CLOSE(aconst, qfld2.DOF(0), tol);
  BOOST_CHECK_CLOSE(aconst, qfld2.DOF(1), tol);
  BOOST_CHECK_CLOSE(aconst, qfld2.DOF(2), tol);
  BOOST_CHECK_CLOSE(aconst, qfld2.DOF(3), tol);
  BOOST_CHECK_CLOSE(aconst, qfld2.DOF(4), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(5), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(6), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(7), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(8), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(9), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(10), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(11), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(12), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(13), tol);
  BOOST_CHECK_SMALL(qfld2.DOF(14), tol);
#else
  int order= 1;
  BOOST_CHECK_THROW(ElementQFieldClass qfld1(order, BasisFunctionCategory_Hierarchical), DeveloperException);
#endif
}


////----------------------------------------------------------------------------//
//BOOST_AUTO_TEST_CASE( Element_Subdivision_Projector_P1 )
//{
//
//  typedef ElementXField<PhysD3, TopoD3, Tet> ElementXFieldClass_3D;
//
//  Real tol = 5e-12;
//
//  Real v0[3] = { 0.2, -0.4,  0.9};
//  Real v1[3] = {-0.3,  0.6,  0.2};
//  Real v2[3] = { 0.5,  1.2, -0.8};
//  Real v3[3] = { 0.7,  0.3,  0.1};
//
//  Real v4[3] = { 0.60, 0.75,-0.35};
//  Real v5[3] = { 0.20, 0.45, 0.15};
//  Real v6[3] = { 0.10, 0.90,-0.30};
//  Real v7[3] = { 0.35, 0.40, 0.05};
//  Real v8[3] = { 0.45,-0.05, 0.50};
//  Real v9[3] = {-0.05, 0.10, 0.55};
//
//  int order = 1;
//  int split_edge_index = -1;
//
//  //--------------3D tets-----------------
//
//  ElementXFieldClass_3D xfldElem3D_main(order, BasisFunctionCategory_Hierarchical);
//  ElementXFieldClass_3D xfldElem3D_sub(order, BasisFunctionCategory_Hierarchical);
//
//  Element_Subdivision_Projector<TopoD3, Tet> projector(xfldElem3D_sub.basis());
//
//  BOOST_CHECK_EQUAL( 1, xfldElem3D_main.order() );
//  BOOST_CHECK_EQUAL( 4, xfldElem3D_main.nDOF() );
//
//  for (int d=0; d<3; d++)
//  {
//    xfldElem3D_main.DOF(0)[d] = v0[d];
//    xfldElem3D_main.DOF(1)[d] = v1[d];
//    xfldElem3D_main.DOF(2)[d] = v2[d];
//    xfldElem3D_main.DOF(3)[d] = v3[d];
//  }
//
//  /*-------Edge splits------------*/
//
//  split_edge_index = 0;
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Edge, split_edge_index, 0 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v0[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v1[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v2[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v4[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Edge, split_edge_index, 1 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v0[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v1[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v4[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v3[d], tol );
//  }
//
//  split_edge_index = 1;
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Edge, split_edge_index, 0 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v0[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v5[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v2[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v3[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Edge, split_edge_index, 1 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v0[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v1[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v2[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v5[d], tol );
//  }
//
//  split_edge_index = 2;
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Edge, split_edge_index, 0 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v0[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v1[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v6[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v3[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Edge, split_edge_index, 1 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v0[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v6[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v2[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v3[d], tol );
//  }
//
//  split_edge_index = 3;
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Edge, split_edge_index, 0 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v7[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v1[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v2[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v3[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Edge, split_edge_index, 1 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v0[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v1[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v7[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v3[d], tol );
//  }
//
//  split_edge_index = 4;
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Edge, split_edge_index, 0 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v0[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v1[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v2[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v8[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Edge, split_edge_index, 1 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v8[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v1[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v2[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v3[d], tol );
//  }
//
//  split_edge_index = 5;
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Edge, split_edge_index, 0 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v0[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v9[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v2[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v3[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Edge, split_edge_index, 1 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v9[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v1[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v2[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v3[d], tol );
//  }
//
//  /*-------Isotropic splits------------*/
//
//  split_edge_index = -1;
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Isotropic, split_edge_index, 0 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v7[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v6[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v2[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v4[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Isotropic, split_edge_index, 1 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v8[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v5[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v4[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v3[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Isotropic, split_edge_index, 2 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v9[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v1[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v6[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v5[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Isotropic, split_edge_index, 3 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v0[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v9[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v7[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v8[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Isotropic, split_edge_index, 4 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v7[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v6[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v4[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v5[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Isotropic, split_edge_index, 5 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v8[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v4[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v5[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v7[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Isotropic, split_edge_index, 6 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v9[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v5[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v6[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v7[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::Isotropic, split_edge_index, 7 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v9[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v7[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v8[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v5[d], tol );
//  }
//
//  /*-------Isotropic-face splits------------*/
//
//  int split_face_index = 0;
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::IsotropicFace, split_face_index, 0 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v0[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v6[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v2[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v4[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::IsotropicFace, split_face_index, 1 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v0[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v5[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v4[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v3[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::IsotropicFace, split_face_index, 2 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v0[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v1[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v6[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v5[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::IsotropicFace, split_face_index, 3 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v0[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v4[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v5[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v6[d], tol );
//  }
//
//  split_face_index = 1;
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::IsotropicFace, split_face_index, 0 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v8[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v1[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v4[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v3[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::IsotropicFace, split_face_index, 1 );
//  for (int d=0; d<3; d++)
//  {
//   BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v7[d], tol );
//   BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v1[d], tol );
//   BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v2[d], tol );
//   BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v4[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::IsotropicFace, split_face_index, 2 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v0[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v1[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v7[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v8[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::IsotropicFace, split_face_index, 3 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v4[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v1[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v8[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v7[d], tol );
//  }
//
//  split_face_index = 2;
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::IsotropicFace, split_face_index, 0 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v9[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v1[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v2[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v5[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::IsotropicFace, split_face_index, 1 );
//  for (int d=0; d<3; d++)
//  {
//   BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v8[d], tol );
//   BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v5[d], tol );
//   BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v2[d], tol );
//   BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v3[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::IsotropicFace, split_face_index, 2 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v0[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v9[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v2[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v8[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::IsotropicFace, split_face_index, 3 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v5[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v8[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v2[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v9[d], tol );
//  }
//
//  split_face_index = 3;
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::IsotropicFace, split_face_index, 0 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v7[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v6[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v2[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v3[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::IsotropicFace, split_face_index, 1 );
//  for (int d=0; d<3; d++)
//  {
//   BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v9[d], tol );
//   BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v1[d], tol );
//   BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v6[d], tol );
//   BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v3[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::IsotropicFace, split_face_index, 2 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v0[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v9[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v7[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v3[d], tol );
//  }
//
//  projector.project( xfldElem3D_main, xfldElem3D_sub, ElementSplitType::IsotropicFace, split_face_index, 3 );
//  for (int d=0; d<3; d++)
//  {
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(0)[d], v6[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(1)[d], v7[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(2)[d], v9[d], tol );
//    BOOST_CHECK_CLOSE( xfldElem3D_sub.DOF(3)[d], v3[d], tol );
//  }
//}

////----------------------------------------------------------------------------//
//BOOST_AUTO_TEST_CASE( Element_Subdivision_CellToTrace_Projector_P1 )
//{
//  typedef DLA::VectorS<2, Real> ArrayQ;
//  typedef Element<ArrayQ, TopoD2, Triangle> ElementClassTrace;
//  typedef Element<ArrayQ, TopoD3, Tet> ElementClassCell;
//
//  Real small_tol = 1e-12;
//  Real close_tol = 1e-12;
//
//  int order = 1;
//
//  ElementClassTrace fldElem_trace(order, BasisFunctionCategory_Hierarchical);
//  ElementClassCell fldElem_cell(order, BasisFunctionCategory_Hierarchical);
//
//  Element_Subdivision_CellToTrace_Projector<TopoD2, Triangle> projector(fldElem_trace.basis());
//
//  BOOST_CHECK_EQUAL( 1, fldElem_trace.order() );
//  BOOST_CHECK_EQUAL( 3, fldElem_trace.nDOF() );
//
//  BOOST_CHECK_EQUAL( 1, fldElem_cell.order() );
//  BOOST_CHECK_EQUAL( 4, fldElem_cell.nDOF() );
//
//  fldElem_cell.DOF(0) = { 0.25,-0.40};
//  fldElem_cell.DOF(1) = { 0.75,-0.80};
//  fldElem_cell.DOF(2) = { 1.25,-1.20};
//  fldElem_cell.DOF(3) = {-0.25, 0.20};
//
//  int split_edge_index = 0;
//
//  projector.project( fldElem_cell, fldElem_trace, ElementSplitType::Edge, split_edge_index, 0 );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(0)[0],  0.25, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(0)[1], -0.40, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(1)[0],  0.75, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(1)[1], -0.80, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(2)[0],  0.50, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(2)[1], -0.50, small_tol, close_tol );
//
//  split_edge_index = 1;
//  projector.project( fldElem_cell, fldElem_trace, ElementSplitType::Edge, split_edge_index, 0 );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(0)[0],  0.25, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(0)[1], -0.40, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(1)[0],  1.25, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(1)[1], -1.20, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(2)[0],  0.25, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(2)[1], -0.30, small_tol, close_tol );
//
//  split_edge_index = 2;
//  projector.project( fldElem_cell, fldElem_trace, ElementSplitType::Edge, split_edge_index, 0 );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(0)[0],  0.25, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(0)[1], -0.40, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(1)[0], -0.25, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(1)[1],  0.20, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(2)[0],  1.00, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(2)[1], -1.00, small_tol, close_tol );
//
//  split_edge_index = 3;
//  projector.project( fldElem_cell, fldElem_trace, ElementSplitType::Edge, split_edge_index, 0 );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(0)[0],  0.75, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(0)[1], -0.80, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(1)[0],  0.75, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(1)[1], -0.80, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(2)[0], -0.25, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(2)[1],  0.20, small_tol, close_tol );
//
//  split_edge_index = 4;
//  projector.project( fldElem_cell, fldElem_trace, ElementSplitType::Edge, split_edge_index, 0 );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(0)[0],  0.75, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(0)[1], -0.80, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(1)[0],  1.25, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(1)[1], -1.20, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(2)[0],  0.00, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(2)[1], -0.10, small_tol, close_tol );
//
//  split_edge_index = 5;
//  projector.project( fldElem_cell, fldElem_trace, ElementSplitType::Edge, split_edge_index, 0 );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(0)[0],  0.50, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(0)[1], -0.60, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(1)[0],  1.25, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(1)[1], -1.20, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(2)[0], -0.25, small_tol, close_tol );
//  SANS_CHECK_CLOSE( fldElem_trace.DOF(2)[1],  0.20, small_tol, close_tol );
//}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
