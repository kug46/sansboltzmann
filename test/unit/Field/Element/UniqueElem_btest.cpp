// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "Field/Element/UniqueElem.h"
#include "Field/Element/UniqueElemHash.h"

#include <map>
#include <unordered_set>

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{

}


//############################################################################//
BOOST_AUTO_TEST_SUITE( UniqueElem_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_test )
{
  UniqueElem elem0(0, 1);
  BOOST_CHECK_EQUAL(Line::Topology, elem0.topo);
  BOOST_REQUIRE_EQUAL(2, elem0.sortedNodes().size());
  BOOST_CHECK_EQUAL(0, elem0.sortedNodes()[0]);
  BOOST_CHECK_EQUAL(1, elem0.sortedNodes()[1]);

  UniqueElem elem1(1, 0);
  BOOST_CHECK_EQUAL(Line::Topology, elem1.topo);
  BOOST_REQUIRE_EQUAL(2, elem1.sortedNodes().size());
  BOOST_CHECK_EQUAL(0, elem1.sortedNodes()[0]);
  BOOST_CHECK_EQUAL(1, elem1.sortedNodes()[1]);

  UniqueElem elem2(1, 2);
  BOOST_CHECK_EQUAL(Line::Topology, elem2.topo);
  BOOST_REQUIRE_EQUAL(2, elem2.sortedNodes().size());
  BOOST_CHECK_EQUAL(1, elem2.sortedNodes()[0]);
  BOOST_CHECK_EQUAL(2, elem2.sortedNodes()[1]);

  BOOST_CHECK_EQUAL(elem0, elem1);
  BOOST_CHECK_LT(elem0, elem2);

  std::map<UniqueElem, int> elemMap;

  elemMap[elem0] = 42;
  elemMap[elem2] = 314;

  BOOST_CHECK_EQUAL( 42, elemMap.at(elem1));
  BOOST_CHECK_EQUAL(314, elemMap.at(elem2));

  std::unordered_set<UniqueElem> elemSet;

  elemSet.insert(elem0);
  elemSet.insert(elem1);
  elemSet.insert(elem2);

  BOOST_CHECK_EQUAL(2, elemSet.size());

  BOOST_CHECK_EQUAL(elem0, *elemSet.find(elem0));
  BOOST_CHECK_EQUAL(elem0, *elemSet.find(elem1));
  BOOST_CHECK_EQUAL(elem2, *elemSet.find(elem2));
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_test )
{
  UniqueElem elem0(Triangle::Topology, std::vector<int>({0, 1, 2}));
  BOOST_CHECK_EQUAL(Triangle::Topology, elem0.topo);
  BOOST_REQUIRE_EQUAL(3, elem0.sortedNodes().size());
  BOOST_CHECK_EQUAL(0, elem0.sortedNodes()[0]);
  BOOST_CHECK_EQUAL(1, elem0.sortedNodes()[1]);
  BOOST_CHECK_EQUAL(2, elem0.sortedNodes()[2]);

  UniqueElem elem1(Triangle::Topology, std::vector<int>({1, 2, 0}));
  BOOST_CHECK_EQUAL(Triangle::Topology, elem1.topo);
  BOOST_REQUIRE_EQUAL(3, elem1.sortedNodes().size());
  BOOST_CHECK_EQUAL(0, elem1.sortedNodes()[0]);
  BOOST_CHECK_EQUAL(1, elem1.sortedNodes()[1]);
  BOOST_CHECK_EQUAL(2, elem1.sortedNodes()[2]);

  UniqueElem elem2(Triangle::Topology, std::vector<int>({3, 1, 0}));
  BOOST_CHECK_EQUAL(Triangle::Topology, elem2.topo);
  BOOST_REQUIRE_EQUAL(3, elem2.sortedNodes().size());
  BOOST_CHECK_EQUAL(0, elem2.sortedNodes()[0]);
  BOOST_CHECK_EQUAL(1, elem2.sortedNodes()[1]);
  BOOST_CHECK_EQUAL(3, elem2.sortedNodes()[2]);

  BOOST_CHECK_EQUAL(elem0, elem1);
  BOOST_CHECK_LT(elem0, elem2);

  std::map<UniqueElem, int> elemMap;

  elemMap[elem0] = 42;
  elemMap[elem2] = 314;

  BOOST_CHECK_EQUAL( 42, elemMap.at(elem1));
  BOOST_CHECK_EQUAL(314, elemMap.at(elem2));

  std::unordered_set<UniqueElem> elemSet;

  elemSet.insert(elem0);
  elemSet.insert(elem1);
  elemSet.insert(elem2);

  BOOST_CHECK_EQUAL(2, elemSet.size());

  BOOST_CHECK_EQUAL(elem0, *elemSet.find(elem0));
  BOOST_CHECK_EQUAL(elem0, *elemSet.find(elem1));
  BOOST_CHECK_EQUAL(elem2, *elemSet.find(elem2));
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Tet_test )
{
  UniqueElem elem0(Tet::Topology, std::vector<int>({0, 1, 2, 3}));
  BOOST_CHECK_EQUAL(Tet::Topology, elem0.topo);
  BOOST_REQUIRE_EQUAL(4, elem0.sortedNodes().size());
  BOOST_CHECK_EQUAL(0, elem0.sortedNodes()[0]);
  BOOST_CHECK_EQUAL(1, elem0.sortedNodes()[1]);
  BOOST_CHECK_EQUAL(2, elem0.sortedNodes()[2]);
  BOOST_CHECK_EQUAL(3, elem0.sortedNodes()[3]);

  UniqueElem elem1(Tet::Topology, std::vector<int>({1, 3, 0, 2}));
  BOOST_CHECK_EQUAL(Tet::Topology, elem1.topo);
  BOOST_REQUIRE_EQUAL(4, elem1.sortedNodes().size());
  BOOST_CHECK_EQUAL(0, elem1.sortedNodes()[0]);
  BOOST_CHECK_EQUAL(1, elem1.sortedNodes()[1]);
  BOOST_CHECK_EQUAL(2, elem1.sortedNodes()[2]);
  BOOST_CHECK_EQUAL(3, elem1.sortedNodes()[3]);

  UniqueElem elem2(Tet::Topology, std::vector<int>({4, 1, 0, 2}));
  BOOST_CHECK_EQUAL(Tet::Topology, elem2.topo);
  BOOST_REQUIRE_EQUAL(4, elem2.sortedNodes().size());
  BOOST_CHECK_EQUAL(0, elem2.sortedNodes()[0]);
  BOOST_CHECK_EQUAL(1, elem2.sortedNodes()[1]);
  BOOST_CHECK_EQUAL(2, elem2.sortedNodes()[2]);
  BOOST_CHECK_EQUAL(4, elem2.sortedNodes()[3]);

  BOOST_CHECK_EQUAL(elem0, elem1);
  BOOST_CHECK_LT(elem0, elem2);

  std::map<UniqueElem, int> elemMap;

  elemMap[elem0] = 42;
  elemMap[elem2] = 314;

  BOOST_CHECK_EQUAL( 42, elemMap.at(elem1));
  BOOST_CHECK_EQUAL(314, elemMap.at(elem2));

  std::unordered_set<UniqueElem> elemSet;

  elemSet.insert(elem0);
  elemSet.insert(elem1);
  elemSet.insert(elem2);

  BOOST_CHECK_EQUAL(2, elemSet.size());

  BOOST_CHECK_EQUAL(elem0, *elemSet.find(elem0));
  BOOST_CHECK_EQUAL(elem0, *elemSet.find(elem1));
  BOOST_CHECK_EQUAL(elem2, *elemSet.find(elem2));
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Ptope_test )
{
  UniqueElem elem0(Pentatope::Topology, std::vector<int>({0, 1, 2, 3, 4}));
  BOOST_CHECK_EQUAL(Pentatope::Topology, elem0.topo);
  BOOST_REQUIRE_EQUAL(5, elem0.sortedNodes().size());
  BOOST_CHECK_EQUAL(0, elem0.sortedNodes()[0]);
  BOOST_CHECK_EQUAL(1, elem0.sortedNodes()[1]);
  BOOST_CHECK_EQUAL(2, elem0.sortedNodes()[2]);
  BOOST_CHECK_EQUAL(3, elem0.sortedNodes()[3]);
  BOOST_CHECK_EQUAL(4, elem0.sortedNodes()[4]);

  UniqueElem elem1(Pentatope::Topology, std::vector<int>({0, 3, 4, 1, 2}));
  BOOST_CHECK_EQUAL(Pentatope::Topology, elem1.topo);
  BOOST_REQUIRE_EQUAL(5, elem1.sortedNodes().size());
  BOOST_CHECK_EQUAL(0, elem1.sortedNodes()[0]);
  BOOST_CHECK_EQUAL(1, elem1.sortedNodes()[1]);
  BOOST_CHECK_EQUAL(2, elem1.sortedNodes()[2]);
  BOOST_CHECK_EQUAL(3, elem1.sortedNodes()[3]);
  BOOST_CHECK_EQUAL(4, elem1.sortedNodes()[4]);

  UniqueElem elem2(Pentatope::Topology, std::vector<int>({3, 4, 1, 2, 0}));
  BOOST_CHECK_EQUAL(Pentatope::Topology, elem2.topo);
  BOOST_REQUIRE_EQUAL(5, elem2.sortedNodes().size());
  BOOST_CHECK_EQUAL(0, elem2.sortedNodes()[0]);
  BOOST_CHECK_EQUAL(1, elem2.sortedNodes()[1]);
  BOOST_CHECK_EQUAL(2, elem2.sortedNodes()[2]);
  BOOST_CHECK_EQUAL(3, elem2.sortedNodes()[3]);
  BOOST_CHECK_EQUAL(4, elem2.sortedNodes()[4]);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Hierarchy_test )
{
  UniqueElem elemLine(25, 24);
  UniqueElem elemTri(Triangle::Topology, std::vector<int>({23, 22, 21}));
  UniqueElem elemQuad(Quad::Topology, std::vector<int>({20, 19, 18, 17}));
  UniqueElem elemTet(Tet::Topology, std::vector<int>({16, 15, 14, 13}));
  UniqueElem elemHex(Hex::Topology, std::vector<int>({12, 11, 10, 9, 8, 7, 6, 5}));
  UniqueElem elemPtope(Pentatope::Topology, std::vector<int>({4, 3, 2, 1, 0}));

  // check the hierarchy
  BOOST_CHECK_LT(elemLine, elemTri);
  BOOST_CHECK_LT(elemLine, elemQuad);
  BOOST_CHECK_LT(elemLine, elemTet);
  BOOST_CHECK_LT(elemLine, elemHex);
  BOOST_CHECK_LT(elemLine, elemPtope);

  BOOST_CHECK_LT(elemTri, elemQuad);
  BOOST_CHECK_LT(elemTri, elemTet);
  BOOST_CHECK_LT(elemTri, elemHex);
  BOOST_CHECK_LT(elemTri, elemPtope);

  BOOST_CHECK_LT(elemQuad, elemTet);
  BOOST_CHECK_LT(elemQuad, elemHex);
  BOOST_CHECK_LT(elemQuad, elemPtope);

  BOOST_CHECK_LT(elemTet, elemHex);
  BOOST_CHECK_LT(elemTet, elemPtope);

  BOOST_CHECK_LT(elemHex, elemPtope);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
