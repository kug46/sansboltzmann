// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementXFieldArea_3D_Triangle_btest
// testing of ElementXFieldArea class w/ Triangle

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/stringify.h"     // Real
#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/TraceToCellRefCoord.h"

using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class ElementXField<PhysD3,TopoD2,Triangle>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementXFieldArea_3D_Triangle_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( unitNormal_test )
{
  const Real small_tol = 1e-13;
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysD3, TopoD2, Triangle> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 3, xElem.nDOF() );

  ElementXField<PhysD3, TopoD2, Triangle>::RefCoordType sRef;
  ElementXField<PhysD3, TopoD2, Triangle>::VectorX N;
  Real nxTrue, nyTrue, nzTrue;

  sRef = 1./3.;

  xElem.DOF(0) = {0, 0, 0};
  xElem.DOF(1) = {1, 0, 0};
  xElem.DOF(2) = {0, 1, 0};

  nxTrue =  0;
  nyTrue =  0;
  nzTrue =  1;

  xElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );

  xElem.DOF(0) = {0, 0, 0};
  xElem.DOF(1) = {0, 1, 0};
  xElem.DOF(2) = {1, 0, 0};

  nxTrue =  0;
  nyTrue =  0;
  nzTrue = -1;

  xElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );

  xElem.DOF(0) = {0, 0, 0};
  xElem.DOF(1) = {0, 0, 1};
  xElem.DOF(2) = {0, 1, 0};

  nxTrue = -1;
  nyTrue =  0;
  nzTrue =  0;

  xElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );

  xElem.DOF(0) = {0, 0, 0};
  xElem.DOF(1) = {1, 0, 0};
  xElem.DOF(2) = {0, 0, 1};

  nxTrue =  0;
  nyTrue = -1;
  nzTrue =  0;

  xElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );

  xElem.DOF(0) = {1, 0, 0};
  xElem.DOF(1) = {0, 1, 0};
  xElem.DOF(2) = {0, 0, 1};

  nxTrue =  1./sqrt(3);
  nyTrue =  1./sqrt(3);
  nzTrue =  1./sqrt(3);

  xElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );

  xElem.DOF(0) = {1,  0, 0};
  xElem.DOF(1) = {0,  0, 1};
  xElem.DOF(2) = {1, -1, 1};

  xElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BoundingBox_test )
{
  BoundingBox<PhysD3> bbox;

  ElementXField<PhysD3, TopoD2, Triangle> xElemQ1(1, BasisFunctionCategory_Lagrange);

  BOOST_REQUIRE_EQUAL( 1, xElemQ1.order() );
  BOOST_REQUIRE_EQUAL( 3, xElemQ1.nDOF() );

  xElemQ1.DOF(0) = {0, 0, 0}; // Node 0
  xElemQ1.DOF(1) = {1, 0, 0}; // Node 1
  xElemQ1.DOF(2) = {0, 1, 1}; // Node 2

  bbox = xElemQ1.boundingBox();

  BOOST_CHECK_EQUAL( 0, bbox.low_bounds()[0] );
  BOOST_CHECK_EQUAL( 0, bbox.low_bounds()[1] );
  BOOST_CHECK_EQUAL( 0, bbox.low_bounds()[2] );
  BOOST_CHECK_EQUAL( 1, bbox.high_bounds()[0] );
  BOOST_CHECK_EQUAL( 1, bbox.high_bounds()[1] );
  BOOST_CHECK_EQUAL( 1, bbox.high_bounds()[2] );

  ElementXField<PhysD3, TopoD2, Triangle> xElemQ2(2, BasisFunctionCategory_Lagrange);

  BOOST_REQUIRE_EQUAL( 2, xElemQ2.order() );
  BOOST_REQUIRE_EQUAL( 6, xElemQ2.nDOF() );

  xElemQ2.DOF(0) = {0, 0, 0}; // Node 0
  xElemQ2.DOF(1) = {1, 0, 0}; // Node 1
  xElemQ2.DOF(2) = {0, 1, 0}; // Node 2

  xElemQ2.DOF(3) = {0.5, 0.5, 0.1}; // Edge 0
  xElemQ2.DOF(4) = {0.0, 0.5, 0.0}; // Edge 1
  xElemQ2.DOF(5) = {0.5, 0.0, 0.0}; // Edge 2

  bbox = xElemQ2.boundingBox();

  BOOST_CHECK_EQUAL(   0, bbox.low_bounds()[0] );
  BOOST_CHECK_EQUAL(   0, bbox.low_bounds()[1] );
  BOOST_CHECK_EQUAL(   0, bbox.low_bounds()[2] );
  BOOST_CHECK_EQUAL(   1, bbox.high_bounds()[0] );
  BOOST_CHECK_EQUAL(   1, bbox.high_bounds()[1] );
  BOOST_CHECK_EQUAL( 0.1, bbox.high_bounds()[2] );

  xElemQ2.DOF(4) = {0.0, 0.5, 0.2}; // Edge 1

  bbox = xElemQ2.boundingBox();

  BOOST_CHECK_EQUAL(   0, bbox.low_bounds()[0] );
  BOOST_CHECK_EQUAL(   0, bbox.low_bounds()[1] );
  BOOST_CHECK_EQUAL(   0, bbox.low_bounds()[2] );
  BOOST_CHECK_EQUAL(   1, bbox.high_bounds()[0] );
  BOOST_CHECK_EQUAL(   1, bbox.high_bounds()[1] );
  BOOST_CHECK_EQUAL( 0.2, bbox.high_bounds()[2] );

  xElemQ2.DOF(5) = {0.5, 0.0, 0.3}; // Edge 2

  bbox = xElemQ2.boundingBox();

  BOOST_CHECK_EQUAL(   0, bbox.low_bounds()[0] );
  BOOST_CHECK_EQUAL(   0, bbox.low_bounds()[1] );
  BOOST_CHECK_EQUAL(   0, bbox.low_bounds()[2] );
  BOOST_CHECK_EQUAL(   1, bbox.high_bounds()[0] );
  BOOST_CHECK_EQUAL(   1, bbox.high_bounds()[1] );
  BOOST_CHECK_EQUAL( 0.3, bbox.high_bounds()[2] );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
