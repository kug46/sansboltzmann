// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementPointData_btest
// testing of Point Data Element class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "Field/Element/ElementPointData.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class ElementPointData< Real, TopoD0, Node >;
template class ElementPointData< Real, TopoD1, Line >;
template class ElementPointData< Real, TopoD2, Triangle >;
template class ElementPointData< Real, TopoD2, Quad >;
template class ElementPointData< Real, TopoD3, Tet >;
template class ElementPointData< Real, TopoD3, Hex >;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementPointData_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  {
  typedef ElementPointData< Real, TopoD0, Node > ElementClass;

  static_assert( std::is_same<typename ElementClass::BasisType, BasisFunctionNodeBase >::value, "Incorrect Basis type" );
  static_assert( std::is_same<typename ElementClass::TopologyType, Node>::value, "Incorrect topology type" );
  }

  {
  typedef ElementPointData< Real, TopoD1, Line > ElementClass;

  static_assert( std::is_same<typename ElementClass::BasisType, BasisFunctionLineBase >::value, "Incorrect Basis type" );
  static_assert( std::is_same<typename ElementClass::TopologyType, Line>::value, "Incorrect topology type" );
  }

  {
  typedef ElementPointData< Real, TopoD2, Triangle > ElementClass;

  static_assert( std::is_same<typename ElementClass::BasisType, BasisFunctionAreaBase<Triangle> >::value, "Incorrect Basis type" );
  static_assert( std::is_same<typename ElementClass::TopologyType, Triangle>::value, "Incorrect topology type" );
  }

  {
  typedef ElementPointData< Real, TopoD2, Quad > ElementClass;

  static_assert( std::is_same<typename ElementClass::BasisType, BasisFunctionAreaBase<Quad> >::value, "Incorrect Basis type" );
  static_assert( std::is_same<typename ElementClass::TopologyType, Quad>::value, "Incorrect topology type" );
  }

  {
  typedef ElementPointData< Real, TopoD3, Tet > ElementClass;

  static_assert( std::is_same<typename ElementClass::BasisType, BasisFunctionVolumeBase<Tet> >::value, "Incorrect Basis type" );
  static_assert( std::is_same<typename ElementClass::TopologyType, Tet>::value, "Incorrect topology type" );
  }

  {
  typedef ElementPointData< Real, TopoD3, Hex > ElementClass;

  static_assert( std::is_same<typename ElementClass::BasisType, BasisFunctionVolumeBase<Hex> >::value, "Incorrect Basis type" );
  static_assert( std::is_same<typename ElementClass::TopologyType, Hex>::value, "Incorrect topology type" );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors_Node )
{
  typedef ElementPointData< Real, TopoD0, Node > ElementClass;

  int order;

  order = 0;
  ElementClass elem1(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, elem1.order() );
  BOOST_CHECK_EQUAL( 1, elem1.nDOF() );

  ElementClass elem2(elem1);

  BOOST_CHECK_EQUAL( 0, elem2.order() );
  BOOST_CHECK_EQUAL( 1, elem2.nDOF() );

  ElementClass elem3(order, BasisFunctionCategory_Legendre);

  elem3 = elem2;

  BOOST_CHECK_EQUAL( 0, elem3.order() );
  BOOST_CHECK_EQUAL( 1, elem3.nDOF() );

  BOOST_CHECK_THROW( ElementClass elem4(BasisFunctionNode_PMax+2, BasisFunctionCategory_Hierarchical), DeveloperException ); // p2 is an exception
  BOOST_CHECK_THROW( ElementClass elem5(BasisFunctionNode_PMax+1, BasisFunctionCategory_Legendre), AssertionException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( accessors_Node )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef ElementPointData< ArrayQ, TopoD0, Node > ElementClass;

  int order = 0;
  ElementClass qElem(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, qElem.order() );
  BOOST_CHECK_EQUAL( 1, qElem.nDOF() );

  const Real tol = 1e-12;

  ArrayQ q1 = {1, 2};

  qElem.DOF(0) = q1;

  ArrayQ q;
  for (int k = 0; k < 2; k++)
  {
    q = qElem.DOF(0);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );

    q = const_cast<const ElementClass&>(qElem).DOF(0);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( extract_basis_Node )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef ElementPointData< ArrayQ, TopoD0, Node > ElementClass;

  int order = 0;
  ElementClass qElem(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, qElem.order() );
  BOOST_CHECK_EQUAL( 1, qElem.nDOF() );

  const BasisFunctionNodeBase* basis = qElem.basis();

  BOOST_CHECK_EQUAL( 0, basis->order() );
  BOOST_CHECK_EQUAL( 1, basis->nBasis() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors_Line )
{
  typedef ElementPointData< Real, TopoD1, Line > ElementClass;

  int order;

  order = 1;
  ElementClass elem1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, elem1.order() );
  BOOST_CHECK_EQUAL( 2, elem1.nDOF() );

  order = 2;
  ElementClass elem2(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, elem2.order() );
  BOOST_CHECK_EQUAL( 3, elem2.nDOF() );

  ElementClass elem3( BasisFunctionLineBase::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, elem3.order() );
  BOOST_CHECK_EQUAL( 3, elem3.nDOF() );

  ElementClass elem4(elem1);

  BOOST_CHECK_EQUAL( 1, elem4.order() );
  BOOST_CHECK_EQUAL( 2, elem4.nDOF() );

  elem4 = elem2;

  BOOST_CHECK_EQUAL( 2, elem4.order() );
  BOOST_CHECK_EQUAL( 3, elem4.nDOF() );

  BOOST_CHECK_THROW( ElementClass elem5(0, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( ElementClass elem6(BasisFunctionLine_HierarchicalPMax+1, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( ElementClass elem7(1, BasisFunctionCategory_Legendre), AssertionException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( accessors_Line )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef ElementPointData< ArrayQ, TopoD1, Line > ElementClass;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( 2, qElem.nDOF() );

  const Real tol = 1e-12;

  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};

  qElem.DOF(0) = q1;
  qElem.DOF(1) = q2;

  ArrayQ q;
  for (int k = 0; k < 2; k++)
  {
    q = qElem.DOF(0);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );
    q = qElem.DOF(1);
    BOOST_CHECK_CLOSE( q2[k], q[k], tol );

    q = const_cast<const ElementClass&>(qElem).DOF(0);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );
    q = const_cast<const ElementClass&>(qElem).DOF(1);
    BOOST_CHECK_CLOSE( q2[k], q[k], tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( extract_basis_Line )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef ElementPointData< ArrayQ, TopoD1, Line > ElementClass;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( 2, qElem.nDOF() );

  const BasisFunctionLineBase* basis = qElem.basis();

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( 2, basis->nBasis() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors_Area_Triangle )
{
  typedef ElementPointData< Real, TopoD2, Triangle > ElementClass;

  int order;

  order = 1;
  ElementClass qElem_p1_1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem_p1_1.order() );
  BOOST_CHECK_EQUAL( 3, qElem_p1_1.nDOF() );

  ElementClass qElem_p1_2( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );

  BOOST_CHECK_EQUAL( 1, qElem_p1_2.order() );
  BOOST_CHECK_EQUAL( 3, qElem_p1_2.nDOF() );

  ElementClass qElem_p1_3(qElem_p1_1);

  BOOST_CHECK_EQUAL( 1, qElem_p1_3.order() );
  BOOST_CHECK_EQUAL( 3, qElem_p1_3.nDOF() );


  order = 2;
  ElementClass qElem_p2_1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qElem_p2_1.order() );
  BOOST_CHECK_EQUAL( 6, qElem_p2_1.nDOF() );

  ElementClass qElem_p2_2( BasisFunctionAreaBase<Triangle>::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, qElem_p2_2.order() );
  BOOST_CHECK_EQUAL( 6, qElem_p2_2.nDOF() );

  ElementClass qElem_p2_3(qElem_p1_1);

  BOOST_CHECK_EQUAL( 1, qElem_p2_3.order() );
  BOOST_CHECK_EQUAL( 3, qElem_p2_3.nDOF() );

  qElem_p2_3 = qElem_p2_2;

  BOOST_CHECK_EQUAL( 2, qElem_p2_3.order() );
  BOOST_CHECK_EQUAL( 6, qElem_p2_3.nDOF() );

  //Exceptions
  BOOST_CHECK_THROW( ElementClass qElem_p0(0, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( ElementClass qElem_pmax(BasisFunctionArea_Triangle_HierarchicalPMax+1, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( ElementClass elem7(1, BasisFunctionCategory_Legendre), AssertionException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( accessors_Area_Triangle )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef ElementPointData< ArrayQ, TopoD2, Triangle > ElementClass;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( 3, qElem.nDOF() );

  const Real tol = 1e-13;

  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};
  ArrayQ q3 = {5, 6};

  qElem.DOF(0) = q1;
  qElem.DOF(1) = q2;
  qElem.DOF(2) = q3;

  ArrayQ q;
  for (int k = 0; k < 2; k++)
  {
    q = qElem.DOF(0);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );
    q = qElem.DOF(1);
    BOOST_CHECK_CLOSE( q2[k], q[k], tol );
    q = qElem.DOF(2);
    BOOST_CHECK_CLOSE( q3[k], q[k], tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( extract_basis_Area_Triangle )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef ElementPointData< ArrayQ, TopoD2, Triangle > ElementClass;

  int order = 1;
  ElementClass qElem_P1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem_P1.order() );
  BOOST_CHECK_EQUAL( 3, qElem_P1.nDOF() );

  const BasisFunctionAreaBase<Triangle>* basis_P1 = qElem_P1.basis();

  BOOST_CHECK_EQUAL( 1, basis_P1->order() );
  BOOST_CHECK_EQUAL( 3, basis_P1->nBasis() );


  order = 2;
  ElementClass qElem_P2(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qElem_P2.order() );
  BOOST_CHECK_EQUAL( 6, qElem_P2.nDOF() );

  const BasisFunctionAreaBase<Triangle>* basis_P2 = qElem_P2.basis();

  BOOST_CHECK_EQUAL( 2, basis_P2->order() );
  BOOST_CHECK_EQUAL( 6, basis_P2->nBasis() );


  order = 3;
  ElementClass qElem_P3(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 3, qElem_P3.order() );
  BOOST_CHECK_EQUAL( 10, qElem_P3.nDOF() );

  const BasisFunctionAreaBase<Triangle>* basis_P3 = qElem_P3.basis();

  BOOST_CHECK_EQUAL( 3, basis_P3->order() );
  BOOST_CHECK_EQUAL( 10, basis_P3->nBasis() );
}



//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors_Volume_Tet )
{
  typedef ElementPointData< Real, TopoD3, Tet > ElementClass;

  int order;

  order = 1;
  ElementClass qElem_P1_1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem_P1_1.order() );
  BOOST_CHECK_EQUAL( 4, qElem_P1_1.nDOF() );


  order = 2;
  ElementClass qElem_P2_1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qElem_P2_1.order() );
  BOOST_CHECK_EQUAL( 10, qElem_P2_1.nDOF() );

  ElementClass qElem_P2_2( BasisFunctionVolumeBase<Tet>::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, qElem_P2_2.order() );
  BOOST_CHECK_EQUAL( 10, qElem_P2_2.nDOF() );

  ElementClass qElem_P2_3(qElem_P1_1);

  BOOST_CHECK_EQUAL( 1, qElem_P2_3.order() );
  BOOST_CHECK_EQUAL( 4, qElem_P2_3.nDOF() );

  qElem_P2_3 = qElem_P2_1;

  BOOST_CHECK_EQUAL( 2, qElem_P2_3.order() );
  BOOST_CHECK_EQUAL( 10, qElem_P2_3.nDOF() );

  BOOST_CHECK_THROW( ElementClass qElem_P0(0, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( ElementClass qElem_pmax(BasisFunctionVolume_Tet_HierarchicalPMax+1, BasisFunctionCategory_Hierarchical), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( accessors_Volume_Tet )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef ElementPointData< ArrayQ, TopoD3, Tet > ElementClass;

  int order = 1;
  ElementClass qElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem.order() );
  BOOST_CHECK_EQUAL( 4, qElem.nDOF() );

  const Real tol = 1e-13;

  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};
  ArrayQ q3 = {5, 6};
  ArrayQ q4 = {7, 8};

  qElem.DOF(0) = q1;
  qElem.DOF(1) = q2;
  qElem.DOF(2) = q3;
  qElem.DOF(3) = q4;

  ArrayQ q;
  for (int k = 0; k < 2; k++)
  {
    q = qElem.DOF(0);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );
    q = qElem.DOF(1);
    BOOST_CHECK_CLOSE( q2[k], q[k], tol );
    q = qElem.DOF(2);
    BOOST_CHECK_CLOSE( q3[k], q[k], tol );
    q = qElem.DOF(3);
    BOOST_CHECK_CLOSE( q4[k], q[k], tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( extract_basis_Volume_Tet )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef ElementPointData< ArrayQ, TopoD3, Tet > ElementClass;

  int order = 1;
  ElementClass qElem_P1(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qElem_P1.order() );
  BOOST_CHECK_EQUAL( 4, qElem_P1.nDOF() );

  const BasisFunctionVolumeBase<Tet>* basis_P1 = qElem_P1.basis();

  BOOST_CHECK_EQUAL( 1, basis_P1->order() );
  BOOST_CHECK_EQUAL( 4, basis_P1->nBasis() );

  order = 2;
  ElementClass qElem_P2(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qElem_P2.order() );
  BOOST_CHECK_EQUAL( 10, qElem_P2.nDOF() );

  const BasisFunctionVolumeBase<Tet>* basis_P2 = qElem_P2.basis();

  BOOST_CHECK_EQUAL( 2, basis_P2->order() );
  BOOST_CHECK_EQUAL( 10, basis_P2->nBasis() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/ElementPointData_pattern.txt", true );

  {
  typedef ElementPointData< ArrayQ, TopoD0, Node > ElementClass;
  ElementClass qElem(0, BasisFunctionCategory_Legendre);

  ArrayQ q1 = {1, 2};

  qElem.DOF(0) = q1;

  qElem.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
  }

  {
  typedef ElementPointData< ArrayQ, TopoD1, Line > ElementClass;
  ElementClass qElem(1, BasisFunctionCategory_Hierarchical);

  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};

  qElem.DOF(0) = q1;
  qElem.DOF(1) = q2;

  qElem.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
  }

  {
  typedef ElementPointData< ArrayQ, TopoD2, Triangle > ElementClass;
  ElementClass qElem(1, BasisFunctionCategory_Hierarchical);

  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};
  ArrayQ q3 = {5, 6};

  qElem.DOF(0) = q1;
  qElem.DOF(1) = q2;
  qElem.DOF(2) = q3;

  qElem.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
  }

  {
  typedef ElementPointData< ArrayQ, TopoD3, Tet > ElementClass;
  ElementClass qElem(1, BasisFunctionCategory_Hierarchical);

  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};
  ArrayQ q3 = {5, 6};
  ArrayQ q4 = {7, 8};

  qElem.DOF(0) = q1;
  qElem.DOF(1) = q2;
  qElem.DOF(2) = q3;
  qElem.DOF(3) = q4;

  qElem.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
