// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BoundingBox_btest
// testing of BoundingBox class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "Field/Element/ReferenceElementMesh.h"

#include <map>

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ReferenceElementMesh_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_test )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  for (int nRefine = 0; nRefine < 5; nRefine++)
  {
    ReferenceElementMesh<Line> refMesh(nRefine);

    int order = pow(2,nRefine);
    int nelem = order;
    int npts = nelem + 1;

    Real ds = 1./(Real)order;

    BOOST_REQUIRE_EQUAL(npts, refMesh.nodes.size());
    BOOST_REQUIRE_EQUAL(nelem, refMesh.elems.size());

    for ( int i = 0; i < npts; i++ )
      SANS_CHECK_CLOSE( i*ds, refMesh.nodes[i][0], small_tol, close_tol );

    for ( int i = 0; i < nelem; i++ )
    {
      BOOST_CHECK_EQUAL( i  , refMesh.elems[i][0] );
      BOOST_CHECK_EQUAL( i+1, refMesh.elems[i][1] );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_test )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  for (int nRefine = 0; nRefine < 5; nRefine++)
  {
    ReferenceElementMesh<Triangle> refMesh(nRefine);

    int nelem = pow(4,nRefine);

    int order = pow(2,nRefine);
    int npts = (order+1)*(order+2)/2;

    Real ds = 1./(Real)order;

    BOOST_REQUIRE_EQUAL(npts, refMesh.nodes.size());
    BOOST_REQUIRE_EQUAL(nelem, refMesh.elems.size());

    int n = 0;
    for (int j = 0; j < order+1; j++)
    {
      for (int i = 0; i < order+1-j; i++)
      {
        SANS_CHECK_CLOSE( i*ds, refMesh.nodes[n][0], small_tol, close_tol );
        SANS_CHECK_CLOSE( j*ds, refMesh.nodes[n][1], small_tol, close_tol );
        n++;
      }
    }

    int n0, n1, n2, n3;
    int elemL, elemR;
    int nodeoffset = 0;
    int elemoffset = 0;
    for (int j = 0; j < order; j++)
    {
      for (int i = 0; i < order-j; i++)
      {
        n0 = nodeoffset + i;         // n3--n2
        n1 = n0 + 1;                 //  |\R|
        n2 = n1 + (order+1-j);       //  |L\|
        n3 = n0 + (order+1-j);       // n0--n1

        elemL = elemoffset + 2*i;
        elemR = elemoffset + 2*i + 1;

        BOOST_CHECK_EQUAL( n0, refMesh.elems[elemL][0] );
        BOOST_CHECK_EQUAL( n1, refMesh.elems[elemL][1] );
        BOOST_CHECK_EQUAL( n3, refMesh.elems[elemL][2] );
        if ( i < order-j-1)
        {
          BOOST_CHECK_EQUAL( n2, refMesh.elems[elemR][0] );
          BOOST_CHECK_EQUAL( n3, refMesh.elems[elemR][1] );
          BOOST_CHECK_EQUAL( n1, refMesh.elems[elemR][2] );
        }
      }

      nodeoffset += order+1-j;
      elemoffset += 2*(order-j-1)+1;
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Quad_test )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  for (int nRefine = 0; nRefine < 5; nRefine++)
  {
    ReferenceElementMesh<Quad> refMesh(nRefine);

    int nelem = pow(4,nRefine);

    int order = pow(2,nRefine);
    int npts = (order+1)*(order+1);

    Real ds = 1./(Real)order;

    BOOST_REQUIRE_EQUAL(npts, refMesh.nodes.size());
    BOOST_REQUIRE_EQUAL(nelem, refMesh.elems.size());

    int n = 0;
    for (int j = 0; j < order+1; j++)
    {
      for (int i = 0; i < order+1; i++)
      {
        SANS_CHECK_CLOSE( i*ds, refMesh.nodes[n][0], small_tol, close_tol );
        SANS_CHECK_CLOSE( j*ds, refMesh.nodes[n][1], small_tol, close_tol );
        n++;
      }
    }

    int n0, n1, n2, n3;
    int elem;
    for (int j = 0; j < order; j++)
    {
      for (int i = 0; i < order; i++)
      {
        n0 =  i + j*(order+1);       // n3--n2
        n1 = n0 + 1;                 //  |  |
        n2 = n1 + (order+1);         //  |  |
        n3 = n0 + (order+1);         // n0--n1

        elem = j*order + i;

        BOOST_CHECK_EQUAL( n0, refMesh.elems[elem][0] );
        BOOST_CHECK_EQUAL( n1, refMesh.elems[elem][1] );
        BOOST_CHECK_EQUAL( n2, refMesh.elems[elem][2] );
        BOOST_CHECK_EQUAL( n3, refMesh.elems[elem][3] );
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Tet_test )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  // Index table for each tet in the hex
  const int hextets[6][4] = { {0, 1, 2, 4},
                              {1, 4, 3, 2},
                              {6, 2, 3, 4},

                              {1, 4, 5, 3},
                              {4, 6, 5, 3},
                              {7, 6, 3, 5} };

  for (int nRefine = 0; nRefine < 5; nRefine++)
  {
    ReferenceElementMesh<Tet> refMesh(nRefine);

    int nelem = pow(8,nRefine);

    int order = pow(2,nRefine);
    int npts = (order+1)*(order+2)*(order+3)/6;

    BOOST_REQUIRE_EQUAL(npts, refMesh.nodes.size());
    BOOST_REQUIRE_EQUAL(nelem, refMesh.elems.size());

    std::map<int,int> nodemap;
    Real ds = 1./(Real)order;

    int joffset = (order+1);
    int koffset = (order+1)*(order+1);

    int n = 0;
    for (int k = 0; k < order+1; k++)
      for (int j = 0; j < order+1-k; j++)
        for (int i = 0; i < order+1-j-k; i++)
        {
          SANS_CHECK_CLOSE( i*ds, refMesh.nodes[n][0], small_tol, close_tol );
          SANS_CHECK_CLOSE( j*ds, refMesh.nodes[n][1], small_tol, close_tol );
          SANS_CHECK_CLOSE( k*ds, refMesh.nodes[n][2], small_tol, close_tol );
          nodemap[i + j*joffset + k*koffset] = n;
          n++;
        }

    int elem = 0;
    for (int k = 0; k < order; k++)
    {
      for (int j = 0; j < order-k; j++)
      {
        for (int i = 0; i < order-j-k; i++)
        {
          const int n0 = i + j*joffset + k*koffset;

          //All the nodes that make up an individual hex
          const int hexnodes[8] = { n0 + 0,
                                    n0 + 1,
                                    n0 + joffset + 0,
                                    n0 + joffset + 1,

                                    n0 + koffset + 0,
                                    n0 + koffset + 1,
                                    n0 + koffset + joffset + 0,
                                    n0 + koffset + joffset + 1 };

          //Loop over all tets that make up a hex
          for (int tet = 0; tet < 6; tet++)
          {
            //Get the nodes from the hex for each tet (if the nodes exist for the tet)
            try
            {
              // first check that no exceptions are thrown
              for (int n = 0; n < 4; n++)
                nodemap.at(hexnodes[hextets[tet][n]]);

              // now check the node values
              for (int n = 0; n < 4; n++)
                BOOST_CHECK_EQUAL( nodemap.at(hexnodes[hextets[tet][n]]), refMesh.elems[elem][n] );

              elem++;
              if (elem == nelem) break;
            }
            catch (std::out_of_range&)
            {
              // ignore elements without nodes
            }
          } // tet

        } // i
      } // j
    } // k

    BOOST_CHECK_EQUAL( elem, nelem );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Hex_test )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  for (int nRefine = 0; nRefine < 5; nRefine++)
  {
    ReferenceElementMesh<Hex> refMesh(nRefine);

    int ii = pow(2,nRefine);
    int jj = ii;
    int kk = ii;
    int nelem = ii*jj*kk;

    int order = pow(2,nRefine);
    int npts = (order+1)*(order+1)*(order+1);

    Real ds = 1./(Real)order;

    BOOST_REQUIRE_EQUAL(npts, refMesh.nodes.size());
    BOOST_REQUIRE_EQUAL(nelem, refMesh.elems.size());

    int n = 0;
    for (int k = 0; k < order+1; k++)
    {
      for (int j = 0; j < order+1; j++)
      {
        for (int i = 0; i < order+1; i++)
        {
          SANS_CHECK_CLOSE( i*ds, refMesh.nodes[n][0], small_tol, close_tol );
          SANS_CHECK_CLOSE( j*ds, refMesh.nodes[n][1], small_tol, close_tol );
          SANS_CHECK_CLOSE( k*ds, refMesh.nodes[n][2], small_tol, close_tol );
          n++;
        }
      }
    }

    const int joffset = (ii + 1);
    const int koffset = (ii + 1) * (jj + 1);

    int elem;
    for (int k = 0; k < kk; k++)
    {
      for (int j = 0; j < order; j++)
      {
        for (int i = 0; i < order; i++)
        {
          const int n0 = k * koffset + j * joffset + i;

          elem = k*order*order + j*order + i;

          BOOST_CHECK_EQUAL( n0 + 0                    , refMesh.elems[elem][0] );
          BOOST_CHECK_EQUAL( n0 + 1                    , refMesh.elems[elem][1] );
          BOOST_CHECK_EQUAL( n0 + joffset + 1          , refMesh.elems[elem][2] );
          BOOST_CHECK_EQUAL( n0 + joffset + 0          , refMesh.elems[elem][3] );

          BOOST_CHECK_EQUAL( n0 + koffset + 0          , refMesh.elems[elem][4] );
          BOOST_CHECK_EQUAL( n0 + koffset + 1          , refMesh.elems[elem][5] );
          BOOST_CHECK_EQUAL( n0 + koffset + joffset + 1, refMesh.elems[elem][6] );
          BOOST_CHECK_EQUAL( n0 + koffset + joffset + 0, refMesh.elems[elem][7] );
        }
      }
    }
  }
}

////----------------------------------------------------------------------------//
//BOOST_AUTO_TEST_CASE(Ptope_test)
//{
//
//  const Real small_tol= 1e-13;
//  const Real close_tol= 1e-12;
//
//  const int nSimplexTesseract= 24;
//  const int nNodesSimplex= 5;
//  const int nNodeTesseract= 16;
//
//  // index table for each pentatope in the tesseract
//  const int tesseractpentatopes[nSimplexTesseract][nNodesSimplex]=
//      {{0, 8, 10, 15, 14}, {0, 8, 10, 11, 15}, {0, 8, 9, 13, 15},
//       {0, 8, 9, 15, 11}, {0, 2, 10, 14, 15}, {0, 2, 10, 15, 11},
//       {0, 8, 12, 14, 15}, {0, 8, 12, 15, 13}, {0, 4, 12, 15, 14},
//       {0, 4, 12, 13, 15}, {0, 4, 6, 14, 15}, {0, 4, 6, 15, 7},
//       {0, 4, 5, 15, 13}, {0, 4, 5, 7, 15}, {0, 2, 6, 15, 14},
//       {0, 2, 6, 7, 15}, {0, 2, 3, 11, 15}, {0, 2, 3, 15, 7},
//       {0, 1, 9, 15, 13}, {0, 1, 9, 11, 15}, {0, 1, 5, 13, 15},
//       {0, 1, 5, 15, 7}, {0, 1, 3, 15, 11}, {0, 1, 3, 7, 15}};
//
//  for (int nRefine= 0; nRefine < 5; nRefine++)
//  {
//
//    ReferenceElementMesh<Pentatope> refMesh(nRefine);
//
//  }
//
//}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
