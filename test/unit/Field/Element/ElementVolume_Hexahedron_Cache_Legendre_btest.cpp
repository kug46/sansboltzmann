// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementVolume_Hexahedron_btest
// testing of Element< T, TopoD3, Hex >

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <ostream>
#include <memory>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"
#include "Field/Element/ElementVolume.h"
#include "BasisFunction/BasisFunctionVolume_Hexahedron.h"
#include "BasisFunction/TraceToCellRefCoord.h"
#include "Topology/ElementTopology.h"
#include "Quadrature/QuadratureVolume.h"
#include "Quadrature/QuadratureArea.h"


using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class Element<Real, TopoD3, Hex>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementVolume_Hexahedron_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( cached_Legendre_basis )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef DLA::VectorS< TopoD3::D, ArrayQ > VectorArrayQ;
  typedef DLA::VectorS< TopoD3::D, Real > RefCoordCell;
  typedef DLA::VectorS< TopoD2::D, Real > RefCoordTrace;
  typedef Element< ArrayQ, TopoD3, Hex > ElementClass;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-13;

  RefCoordCell ref;
  ArrayQ q, qTrue;
  VectorArrayQ derivq, derivqTrue;

  for (int order = 0; order < BasisFunctionVolume_Hex_LegendrePMax+1; order++)
  {
    ElementClass qElem(order, BasisFunctionCategory_Legendre);

    int nDOF = qElem.nDOF();

    // initalize some arbitrary values
    for (int n = 0; n < nDOF; n++)
      qElem.DOF(n) = pow(-1, n) / sqrt(n+1);

    std::vector<Real> phiTrue(nDOF), phisTrue(nDOF), phitTrue(nDOF), phiuTrue(nDOF);
    std::vector<Real> phissTrue(nDOF), phistTrue(nDOF), phittTrue(nDOF),
                      phisuTrue(nDOF), phituTrue(nDOF), phiuuTrue(nDOF);
    std::vector<Real> phi(nDOF), phis(nDOF), phit(nDOF), phiu(nDOF);
    std::vector<Real> phiss(nDOF), phist(nDOF), phitt(nDOF),
                      phisu(nDOF), phitu(nDOF), phiuu(nDOF);

    // check that the cached basis functions work
    for (int iorderidx = 0; iorderidx < QuadratureVolume<Hex>::nOrderIdx; iorderidx++)
    {
      QuadratureVolume<Hex> quadrature( QuadratureVolume<Hex>::getOrderFromIndex(iorderidx) );
      for (int n = 0; n < quadrature.nQuadrature(); n++)
      {
        quadrature.coordinates(n, ref);
        QuadraturePoint<TopoD3> point = quadrature.coordinates_cache(n);

        //--------------
        qElem.evalBasis( ref, phiTrue.data(), phiTrue.size() );
        qElem.evalBasis( point, phi.data(), phi.size() );

        for (int k = 0; k < nDOF; k++)
        {
          BOOST_CHECK_CLOSE( phiTrue[k], phi[k], close_tol );
          phi[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasis( QuadraturePoint<TopoD3>(ref), phi.data(), phi.size() );

        for (int k = 0; k < nDOF; k++)
          BOOST_CHECK_CLOSE( phiTrue[k], phi[k], close_tol );


        //--------------
        qElem.evalBasisDerivative( ref[0], ref[1], ref[2], phisTrue.data(), phitTrue.data(), phiuTrue.data(), phisTrue.size() );
        qElem.evalBasisDerivative( point, phis.data(), phit.data(), phiu.data(), phis.size() );

        for (int k = 0; k < nDOF; k++)
        {
          BOOST_CHECK_CLOSE( phisTrue[k], phis[k], close_tol );
          BOOST_CHECK_CLOSE( phitTrue[k], phit[k], close_tol );
          BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], close_tol );
          phis[k] = 0;
          phit[k] = 0;
          phiu[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasisDerivative( QuadraturePoint<TopoD3>(ref), phis.data(), phit.data(), phiu.data(), phis.size() );

        for (int k = 0; k < nDOF; k++)
        {
          BOOST_CHECK_CLOSE( phisTrue[k], phis[k], close_tol );
          BOOST_CHECK_CLOSE( phitTrue[k], phit[k], close_tol );
          BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], close_tol );
        }


        //--------------
        qElem.evalBasisHessianDerivative( ref[0], ref[1], ref[2],
                                          phissTrue.data(),
                                          phistTrue.data(), phittTrue.data(),
                                          phisuTrue.data(), phituTrue.data(), phiuuTrue.data(), phissTrue.size() );
        qElem.evalBasisHessianDerivative( point,
                                          phiss.data(),
                                          phist.data(), phitt.data(),
                                          phisu.data(), phitu.data(), phiuu.data(), phiss.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phisuTrue[k], phisu[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phituTrue[k], phitu[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phiuuTrue[k], phiuu[k], small_tol, close_tol );
          phiss[k] = 0; phist[k] = 0; phitt[k] = 0;
          phisu[k] = 0; phitu[k] = 0; phiuu[k] = 0;
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalBasisHessianDerivative( QuadraturePoint<TopoD3>(ref),
                                          phiss.data(),
                                          phist.data(), phitt.data(),
                                          phisu.data(), phitu.data(), phiuu.data(), phiss.size() );

        for (int k = 0; k < nDOF; k++)
        {
          SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phisuTrue[k], phisu[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phituTrue[k], phitu[k], small_tol, close_tol );
          SANS_CHECK_CLOSE( phiuuTrue[k], phiuu[k], small_tol, close_tol );
        }


        //--------------
        qElem.eval( ref, qTrue );
        qElem.eval( point, q );

        for (int k = 0; k < ArrayQ::M; k++)
          BOOST_CHECK_CLOSE( qTrue[k], q[k], close_tol );

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.eval( QuadraturePoint<TopoD3>(ref), q );

        for (int k = 0; k < ArrayQ::M; k++)
          BOOST_CHECK_CLOSE( qTrue[k], q[k], close_tol );


        //--------------
        qElem.evalDerivative( ref, derivqTrue );
        qElem.evalDerivative( point, derivq );

        for (int k = 0; k < ArrayQ::M; k++)
        {
          BOOST_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], close_tol );
          BOOST_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], close_tol );
          BOOST_CHECK_CLOSE( derivqTrue[2][k], derivq[2][k], close_tol );
        }

        // 'non-quadrature' coordinate (mainly used with testing of integrands)
        qElem.evalDerivative( QuadraturePoint<TopoD3>(ref), derivq );

        for (int k = 0; k < ArrayQ::M; k++)
        {
          BOOST_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], close_tol );
          BOOST_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], close_tol );
          BOOST_CHECK_CLOSE( derivqTrue[2][k], derivq[2][k], close_tol );
        }

      } // iquad
    } // orderidx


    for (int itrace = 0; itrace < Hex::NTrace; itrace++)
    {
      for (int orientation : {-4, -3, -2, -1, 1})
      {
        CanonicalTraceToCell canonicalTrace(itrace, orientation);

        // relax tolerances for non-canonical traces
        Real small_tol = 1e-12;
        Real close_tol = 1e-13;
        if (orientation != 1)
        {
          small_tol = 1e-11;
          close_tol = 2e-8;
        }

        for (int orderidx = 0; orderidx < QuadratureArea<Quad>::nOrderIdx; orderidx++)
        {
          QuadratureArea<Quad> quadrature( QuadratureArea<Quad>::getOrderFromIndex(orderidx) );

          RefCoordTrace sTrace;
          RefCoordCell refCell;
          QuadratureCellTracePoint<TopoD3> pointCell, pointCell2;
          for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
          {
            quadrature.coordinates(iquad, sTrace);
            QuadraturePoint<TopoD2> pointTrace = quadrature.coordinates_cache(iquad);

            TraceToCellRefCoord<Quad, TopoD3, Hex>::eval( canonicalTrace, sTrace, refCell );
            TraceToCellRefCoord<Quad, TopoD3, Hex>::eval( canonicalTrace, pointTrace, pointCell );
            TraceToCellRefCoord<Quad, TopoD3, Hex>::eval( canonicalTrace, QuadraturePoint<TopoD2>(sTrace), pointCell2 );

            SANS_CHECK_CLOSE( refCell[0], pointCell.ref[0], small_tol, close_tol );
            SANS_CHECK_CLOSE( refCell[1], pointCell.ref[1], small_tol, close_tol );
            SANS_CHECK_CLOSE( refCell[2], pointCell.ref[2], small_tol, close_tol );

            //--------------
            qElem.evalBasis( refCell, phiTrue.data(), phiTrue.size() );
            qElem.evalBasis( pointCell, phi.data(), phi.size() );

            for (int k = 0; k < nDOF; k++)
            {
              SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
              phi[k] = 0;
            }


            // 'non-quadrature' coordinate (mainly used with testing of integrands)
            qElem.evalBasis( pointCell2, phi.data(), phi.size() );

            for (int k = 0; k < nDOF; k++)
              SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );

            //--------------
            qElem.evalBasisDerivative( refCell[0], refCell[1], refCell[2], phisTrue.data(), phitTrue.data(), phiuTrue.data(), phisTrue.size() );
            qElem.evalBasisDerivative( pointCell, phis.data(), phit.data(), phiu.data(), phis.size() );

            for (int k = 0; k < nDOF; k++)
            {
              SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
              SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
              SANS_CHECK_CLOSE( phiuTrue[k], phiu[k], small_tol, close_tol );
              phis[k] = 0;
              phit[k] = 0;
              phiu[k] = 0;
            }

            // 'non-quadrature' coordinate (mainly used with testing of integrands)
            qElem.evalBasisDerivative( pointCell2, phis.data(), phit.data(), phiu.data(), phis.size() );

            for (int k = 0; k < nDOF; k++)
            {
              SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
              SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
              SANS_CHECK_CLOSE( phiuTrue[k], phiu[k], small_tol, close_tol );
            }

            //--------------
            qElem.eval( refCell, qTrue );
            qElem.eval( pointCell, q );

            for (int k = 0; k < ArrayQ::M; k++)
              SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );

            // 'non-quadrature' coordinate (mainly used with testing of integrands)
            qElem.eval( pointCell2, q );

            for (int k = 0; k < ArrayQ::M; k++)
              SANS_CHECK_CLOSE( qTrue[k], q[k], small_tol, close_tol );


            //--------------
            qElem.evalDerivative( refCell, derivqTrue );
            qElem.evalDerivative( pointCell, derivq );

            for (int k = 0; k < ArrayQ::M; k++)
            {
              SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
              SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
              SANS_CHECK_CLOSE( derivqTrue[2][k], derivq[2][k], small_tol, close_tol );
            }

            // 'non-quadrature' coordinate (mainly used with testing of integrands)
            qElem.evalDerivative( pointCell2, derivq );

            for (int k = 0; k < ArrayQ::M; k++)
            {
              SANS_CHECK_CLOSE( derivqTrue[0][k], derivq[0][k], small_tol, close_tol );
              SANS_CHECK_CLOSE( derivqTrue[1][k], derivq[1][k], small_tol, close_tol );
              SANS_CHECK_CLOSE( derivqTrue[2][k], derivq[2][k], small_tol, close_tol );
            }

          } //iquad
        } // orderidx
      } // orientation
    } // itrace

  } // order
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
