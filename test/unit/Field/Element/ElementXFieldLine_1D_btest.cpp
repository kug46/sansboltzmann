// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementXFieldLine_1D_btest
//   testing of ElementXFieldLine class 1D specialization

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/stringify.h"
#include "Field/Element/ElementXFieldLine.h"

using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class ElementXField<PhysD1, TopoD1, Line>;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementXFieldLine_1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BoundingBox_Q1_test )
{
  int order = 1;
  ElementXField<PhysD1,TopoD1,Line> xElem(order, BasisFunctionCategory_Lagrange);

  xElem.DOF(0) = {0};
  xElem.DOF(1) = {1};

  BoundingBox<PhysD1> bbox = xElem.boundingBox();

  BOOST_CHECK_EQUAL(0, bbox.low_bounds()[0]);
  BOOST_CHECK_EQUAL(1, bbox.high_bounds()[0]);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( impliedMetric_test )
{
  const Real small_tol = 1e-13;
  const Real tol = 1e-13;

  int order = 1;
  ElementXField<PhysD1, TopoD1, Line> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 2, xElem.nDOF() );

  ElementXField<PhysD1, TopoD1, Line>::RefCoordType sRef = {0.0};

  DLA::MatrixSymS<PhysD1::D, Real> M;

  //Unit line
  xElem.DOF(0) = {0.0};
  xElem.DOF(1) = {1.0};

  xElem.impliedMetric(sRef, M);

  SANS_CHECK_CLOSE( M(0,0), 1.0, small_tol, tol );

  M = 0;
  xElem.impliedMetric(M); //evaluate at centerRef

  SANS_CHECK_CLOSE( M(0,0), 1.0, small_tol, tol );

  //Unit line scaled by 2.5, translated by (3.0)
  Real scale = 2.5;
  Real xs = 3.0;
  xElem.DOF(0) = scale*0.0 + xs;
  xElem.DOF(1) = scale*1.0 + xs;

  xElem.impliedMetric(sRef, M);

  SANS_CHECK_CLOSE( M(0,0), 1.0/pow(scale,2.0), small_tol, tol );
}

//----------------------------------------------------------------------------//
// Checking being forwarded correctly and constructed into correct classes,
// the values aren't very informative
//
BOOST_AUTO_TEST_CASE( evalBasisGradient_Hessian_Q1_test )
{
  typedef BasisFunctionLine<Hierarchical,1> BasisClass;

  int order = 1;
  ElementXField<PhysD1,TopoD1,Line> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xElem.order() );
  BOOST_CHECK_EQUAL( 2, xElem.nDOF() );

  const BasisClass* basis = BasisClass::self();
  Element<Real,TopoD1,Line> qfldElem(basis);

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( 2, basis->nBasis() );

  const Real tol = 1e-13;
  typename ElementXField<PhysD1,TopoD1,Line>::RefCoordType sRef;
  typename ElementXField<PhysD1,TopoD1,Line>::VectorX gradphi[2];
  typename ElementXField<PhysD1,TopoD1,Line>::TensorSymX hessphi[2];
  Real phixTrue[2], phixxTrue[2];
  int k;

  xElem.DOF(0) = {0};
  xElem.DOF(1) = {1};

  // basis gradient independent of location for Q=1, P=1
  phixTrue[0]  = -1; phixTrue[1]  =  1;
  phixxTrue[0] =  0; phixxTrue[1] =  0;

  sRef[0] = 0;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, 2 );
  xElem.evalBasisHessian( sRef, qfldElem, hessphi, 2 );
  for (k = 0; k < 2; k++)
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phixxTrue[k], hessphi[k](0,0), tol );
  }

  sRef[0] = 1;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, 2 );
  xElem.evalBasisHessian( sRef, qfldElem, hessphi, 2 );
  for (k = 0; k < 2; k++)
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phixxTrue[k], hessphi[k](0,0), tol );
  }

  sRef[0] = 1./2.;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, 2 );
  xElem.evalBasisHessian( sRef, qfldElem, hessphi, 2 );
  for (k = 0; k < 2; k++)
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phixxTrue[k], hessphi[k](0,0), tol );
  }
}

//----------------------------------------------------------------------------//
// Checking being forwarded correctly and constructed into correct classes,
// the values aren't very informative
//
BOOST_AUTO_TEST_CASE( evalBasisGradient_Hessian_Q2_test )
{
  typedef BasisFunctionLine<Hierarchical,2> BasisClass;

  int order = 2;
  ElementXField<PhysD1,TopoD1,Line> xElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, xElem.order() );
  BOOST_CHECK_EQUAL( 3, xElem.nDOF() );

  const BasisClass* basis = BasisClass::self();
  Element<Real,TopoD1,Line> qfldElem(basis);

  BOOST_CHECK_EQUAL( 2, basis->order() );
  BOOST_CHECK_EQUAL( 3, basis->nBasis() );

  const Real tol = 1e-13;
  const int nDOF = 3;
  typename ElementXField<PhysD1,TopoD1,Line>::RefCoordType sRef;
  typename ElementXField<PhysD1,TopoD1,Line>::VectorX gradphi[nDOF];
  typename ElementXField<PhysD1,TopoD1,Line>::TensorSymX hessphi[nDOF];
  Real phixTrue[nDOF], phixxTrue[nDOF];
  int k;

  xElem.DOF(0) = {0};
  xElem.DOF(1) = {0.7};
  xElem.DOF(2) = {1};

  sRef[0] = 0;
  phixTrue[0]  = -0.2127659574468085;
  phixTrue[1]  =  0.2127659574468085;
  phixTrue[2]  =  0.8510638297872340;

  phixxTrue[0] = -0.07705421727362914;
  phixxTrue[1] =  0.07705421727362914;
  phixxTrue[2] = -0.05393795209154040;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, nDOF );
  xElem.evalBasisHessian( sRef, qfldElem, hessphi, nDOF );
  for ( k = 0; k < nDOF; k++ )
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phixxTrue[k], hessphi[k](0,0), tol );
  }

  sRef[0] = 1;
  phixTrue[0]  =  0.3030303030303030;
  phixTrue[1]  = -0.3030303030303030;
  phixTrue[2]  =  1.212121212121212;

  phixxTrue[0] =  0.2226117928597268;
  phixxTrue[1] = -0.2226117928597268;
  phixxTrue[2] =  0.1558282550018087;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, nDOF );
  xElem.evalBasisHessian( sRef, qfldElem, hessphi, nDOF );
  for ( k = 0; k < nDOF; k++ )
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phixxTrue[k], hessphi[k](0,0), tol );
  }

  sRef[0] = 0.3;
  phixTrue[0]  = -0.4347826086956521;
  phixTrue[1]  =  0.4347826086956521;
  phixTrue[2]  =  0.6956521739130433;

  phixxTrue[0] = -0.6575162324319880;
  phixxTrue[1] =  0.6575162324319880;
  phixxTrue[2] = -0.4602613627023919;
  xElem.evalBasisGradient( sRef, qfldElem, gradphi, nDOF );
  xElem.evalBasisHessian( sRef, qfldElem, hessphi, nDOF );
  for ( k = 0; k < nDOF; k++ )
  {
    BOOST_CHECK_CLOSE( phixTrue[k], gradphi[k][0], tol );
    BOOST_CHECK_CLOSE( phixxTrue[k], hessphi[k](0,0), tol );
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
