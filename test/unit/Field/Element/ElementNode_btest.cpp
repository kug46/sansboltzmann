// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementNode_btest
// testing of Element class with Node specializiation

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "Field/Element/ElementNode.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class Element< Real, TopoD0, Node >;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementNode_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef Element< Real, TopoD0, Node > ElementClass;

  static_assert( std::is_same<typename ElementClass::BasisType, BasisFunctionNodeBase >::value, "Incorrect Basis type" );
  static_assert( std::is_same<typename ElementClass::TopologyType, Node>::value, "Incorrect topology type" );
  static_assert( std::is_same<typename ElementClass::RefCoordType, DLA::VectorS<1,Real> >::value, "Incorrect reference coordinate type" );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef Element< Real, TopoD0, Node > ElementClass;

  int order;

  order = 0;
  ElementClass elem1(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, elem1.order() );
  BOOST_CHECK_EQUAL( 1, elem1.nDOF() );

  ElementClass elem2(elem1);

  BOOST_CHECK_EQUAL( 0, elem2.order() );
  BOOST_CHECK_EQUAL( 1, elem2.nDOF() );

  ElementClass elem3(order, BasisFunctionCategory_Legendre);

  elem3 = elem2;

  BOOST_CHECK_EQUAL( 0, elem3.order() );
  BOOST_CHECK_EQUAL( 1, elem3.nDOF() );

  BOOST_CHECK_THROW( ElementClass elem4(BasisFunctionNode_PMax+2, BasisFunctionCategory_Hierarchical), DeveloperException ); // p2 is an exception
  BOOST_CHECK_THROW( ElementClass elem5(BasisFunctionNode_PMax+1, BasisFunctionCategory_Legendre), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( accessors )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD0, Node > ElementClass;

  int order = 0;
  ElementClass qElem(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, qElem.order() );
  BOOST_CHECK_EQUAL( 1, qElem.nDOF() );

  const Real tol = 1e-12;

  ArrayQ q1 = {1, 2};

  qElem.DOF(0) = q1;

  ArrayQ q;
  for (int k = 0; k < 2; k++)
  {
    q = qElem.DOF(0);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( eval_basis )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD0, Node > ElementClass;

  int order = 0;
  ElementClass qElem(order, BasisFunctionCategory_Legendre);
  ElementClass::RefCoordType sRef = 0;

  BOOST_CHECK_EQUAL( 0, qElem.order() );
  BOOST_CHECK_EQUAL( 1, qElem.nDOF() );

  const Real tol = 1e-13;
  Real phi[1], phis[1], phiss[1];
  Real phiTrue[1], phisTrue[1], phissTrue[1];
  int k;

  phiTrue[0] = 1;
  phisTrue[0] = 0;
  phissTrue[0] = 0;

  qElem.evalBasis( phi, 1 );
  qElem.evalBasisDerivative( phis, 1 );
  qElem.evalBasisHessianDerivative( phiss, 1 );
  for (k = 0; k < 1; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );
  }

  phi[0] = phis[0] = phiss[0] = -1;

  qElem.evalBasis( sRef, phi, 1 );
  qElem.evalBasisDerivative( sRef, phis, 1 );
  qElem.evalBasisHessianDerivative( sRef, phiss, 1 );
  for (k = 0; k < 1; k++)
  {
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
    BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
    BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( eval_variable )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD0, Node > ElementClass;

  int order = 0;
  ElementClass qElem(order, BasisFunctionCategory_Legendre);
  ElementClass::RefCoordType sRef = 0;

  BOOST_CHECK_EQUAL( 0, qElem.order() );
  BOOST_CHECK_EQUAL( 1, qElem.nDOF() );

  const Real tol = 1e-12;
  Real phi[1];
  Real phiTrue[1];
  ArrayQ q, qTrue;
  int k;

  ArrayQ q1 = {1, 2};

  qElem.DOF(0) = q1;

  phiTrue[0] = 1;
  qTrue = q1;

  qElem.evalBasis( phi, 1 );
  qElem.evalFromBasis( phi, 1, q );
  for (k = 0; k < 1; k++)
    BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
  for (k = 0; k < 1; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );

  q = 0;
  qElem.eval( q );
  for (k = 0; k < 1; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );

  q = 0;
  q = qElem.eval( sRef );
  for (k = 0; k < 1; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );

  q = 0;
  qElem.eval( sRef, q );
  for (k = 0; k < 1; k++)
    BOOST_CHECK_CLOSE( qTrue[k], q[k], tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( extract_basis )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD0, Node > ElementClass;

  int order = 0;
  ElementClass qElem(order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, qElem.order() );
  BOOST_CHECK_EQUAL( 1, qElem.nDOF() );

  const BasisFunctionNodeBase* basis = qElem.basis();

  BOOST_CHECK_EQUAL( 0, basis->order() );
  BOOST_CHECK_EQUAL( 1, basis->nBasis() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( projection )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD0, Node > ElementClass;

  const Real tol = 1e-13;

  ElementClass qElem0( 0, BasisFunctionCategory_Legendre );
  ElementClass qElem1( 0, BasisFunctionCategory_Legendre );

  for (int k = 0; k < qElem0.nDOF(); k++)
  {
    ArrayQ q = {sqrt(k+7), 1./(k+3)};
    qElem0.DOF(k) = q;
  }

  qElem0.projectTo( qElem1 );

  ArrayQ q0, q1;

  qElem0.eval( q0 );
  qElem1.eval( q1 );

  for (int n = 0; n < ArrayQ::N; n++)
    BOOST_CHECK_CLOSE( q0[n], q1[n], tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( normalSign )
{
  typedef Element< Real, TopoD0, Node > ElementClass;

  ElementClass qElem( 0, BasisFunctionCategory_Legendre );

  BOOST_CHECK_EQUAL( 0, qElem.normalSignL() );  // zero by default
  BOOST_CHECK_EQUAL( 0, qElem.normalSignR() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef Element< ArrayQ, TopoD0, Node > ElementClass;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/ElementNode_pattern.txt", true );

  ElementClass qElem(0, BasisFunctionCategory_Legendre);

  qElem.DOF(0) = {1, 2};

  qElem.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
