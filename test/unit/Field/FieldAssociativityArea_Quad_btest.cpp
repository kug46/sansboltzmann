// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// FieldAssociativityArea_Quad_btest
// testing of FieldAssociativity class w/ Quad

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionArea_Quad.h"

#include "Field/FieldGroupArea_Traits.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class FieldAssociativity< FieldGroupAreaTraits<Quad,Real> >;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( FieldAssociativityArea_Quad_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  typedef FieldAssociativity< FieldGroupAreaTraits<Quad, Real> > FieldAreaClass;

  static_assert( std::is_same<typename FieldAreaClass::FieldBase, FieldAssociativityBase<Real> >::value, "Incorrect Base type" );
  static_assert( std::is_same<typename FieldAreaClass::BasisType, BasisFunctionAreaBase<Quad> >::value, "Incorrect Basis type" );
  static_assert( std::is_same<typename FieldAreaClass::TopologyType, Quad>::value, "Incorrect topology type" );
  static_assert( std::is_same<typename FieldAreaClass::FieldAssociativityConstructorType,
                              FieldAssociativityConstructor<ElementAssociativityConstructor<TopoD2, Quad> > >::value,
                 "Incorrect Associativity Constructor type" );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef FieldAssociativity< FieldGroupAreaTraits<Quad, Real> > FieldAreaClass;
  typedef typename FieldAreaClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;
  typedef typename FieldAreaClass::FieldBase FieldAreaBase;

  const BasisFunctionAreaBase<Quad>* basis = BasisFunctionAreaBase<Quad>::HierarchicalP1;

  ElementAssociativityConstructor<TopoD2, Quad> assoc1( basis );

  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( 4, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc1.nCell() );

  assoc1.setRank( 2 );
  assoc1.setNodeGlobalMapping( {3, 4, 5, 6} );

  int nElem = 1;
  FieldAssociativityConstructorType fldassoc1( basis, nElem);
  fldassoc1.setAssociativity(assoc1, 0);

  // constructor class
  FieldAreaClass fld1(fldassoc1);

  BOOST_CHECK_EQUAL( 1, fld1.nElem() );
  BOOST_CHECK_EQUAL( 4, fld1.nElemTrace() );
  BOOST_CHECK_EQUAL( basis->nBasis(), fld1.nBasis() );
  BOOST_CHECK_EQUAL( basis->order(), fld1.order() );
  BOOST_CHECK_EQUAL( 0, fld1.nDOF() );

  BOOST_CHECK( fld1.topoTypeID() == typeid(Quad) );

  // default ctor and operator=
  FieldAreaClass fld2;

  BOOST_CHECK_EQUAL( 0, fld2.nElem() );
  BOOST_CHECK_EQUAL( 4, fld2.nElemTrace() );
  BOOST_CHECK_EQUAL( 0, fld2.nDOF() );

  fld2 = fld1;

  BOOST_CHECK_EQUAL( 1, fld2.nElem() );
  BOOST_CHECK_EQUAL( 4, fld2.nElemTrace() );
  BOOST_CHECK_EQUAL( basis->nBasis(), fld2.nBasis() );
  BOOST_CHECK_EQUAL( basis->order(), fld2.order() );
  BOOST_CHECK_EQUAL( 0, fld2.nDOF() );

  BOOST_CHECK( fld2.topoTypeID() == typeid(Quad) );

  // copy ctor
  FieldAreaClass fld3(fld1);

  BOOST_CHECK_EQUAL( 1, fld3.nElem() );
  BOOST_CHECK_EQUAL( 4, fld3.nElemTrace() );
  BOOST_CHECK_EQUAL( basis->nBasis(), fld3.nBasis() );
  BOOST_CHECK_EQUAL( basis->order(), fld3.order() );
  BOOST_CHECK_EQUAL( 0, fld3.nDOF() );

  BOOST_CHECK( fld3.topoTypeID() == typeid(Quad) );

  // resize needed for new []
  FieldAreaClass fld4;

  fld4.resize(fldassoc1);

  BOOST_CHECK_EQUAL( 1, fld4.nElem() );
  BOOST_CHECK_EQUAL( 4, fld4.nElemTrace() );
  BOOST_CHECK_EQUAL( basis->nBasis(), fld4.nBasis() );
  BOOST_CHECK_EQUAL( basis->order(), fld4.order() );
  BOOST_CHECK_EQUAL( 0, fld4.nDOF() );

  BOOST_CHECK( fld4.topoTypeID() == typeid(Quad) );

  // clone
  FieldAreaBase* fld5 = fld1.clone();

  BOOST_CHECK_EQUAL( 1, fld5->nElem() );
  BOOST_CHECK_EQUAL( 4, fld5->nElemTrace() );
  BOOST_CHECK_EQUAL( basis->nBasis(), fld5->nBasis() );
  BOOST_CHECK_EQUAL( basis->order(), fld5->order() );
  BOOST_CHECK_EQUAL( 0, fld5->nDOF() );

  BOOST_CHECK( fld5->topoTypeID() == typeid(Quad) );
  delete fld5;

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis )
{
  typedef FieldAssociativity< FieldGroupAreaTraits<Quad, Real> > FieldAreaClass;
  typedef typename FieldAreaClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;

  ElementAssociativityConstructor<TopoD2, Quad> assoc1( BasisFunctionAreaBase<Quad>::HierarchicalP1 );
  assoc1.setRank( 2 );
  assoc1.setNodeGlobalMapping( {3, 4, 5, 6} );

  int nElem = 1;
  FieldAssociativityConstructorType fldassoc1( BasisFunctionAreaBase<Quad>::HierarchicalP1, nElem);
  fldassoc1.setAssociativity(assoc1, 0);

  FieldAreaClass fld(fldassoc1);

  BOOST_CHECK_EQUAL( 1, fld.nElem() );
  BOOST_CHECK_EQUAL( 0, fld.nDOF() );

  const BasisFunctionAreaBase<Quad>* basis2 = fld.basis();

  BOOST_CHECK_EQUAL( 1, basis2->order() );
  BOOST_CHECK_EQUAL( 4, basis2->nBasis() );
  BOOST_CHECK_EQUAL( 4, basis2->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis2->nBasisEdge() );
  BOOST_CHECK_EQUAL( 0, basis2->nBasisCell() );
  BOOST_CHECK_EQUAL( basis2, BasisFunctionAreaBase<Quad>::HierarchicalP1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DOF_associativity )
{
  typedef FieldAssociativity< FieldGroupAreaTraits<Quad, Real> > FieldAreaClass;
  typedef typename FieldAreaClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;

  ElementAssociativityConstructor<TopoD2, Quad> assoc1( BasisFunctionAreaBase<Quad>::HierarchicalP1 );

  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( 4, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc1.nCell() );

  assoc1.setRank( 2 );
  assoc1.setNodeGlobalMapping( {3, 4, 5, 6} );

  int nElem = 1;
  FieldAssociativityConstructorType fldassoc1( BasisFunctionAreaBase<Quad>::HierarchicalP1, nElem);
  fldassoc1.setAssociativity(assoc1, 0);

  FieldAreaClass fld1(fldassoc1);

  BOOST_CHECK_EQUAL( 1, fld1.nElem() );
  BOOST_CHECK_EQUAL( 0, fld1.nDOF() );

  int node1[4];
  fld1.associativity(0).getNodeGlobalMapping( node1, 4 );
  BOOST_CHECK_EQUAL( 3, node1[0] );
  BOOST_CHECK_EQUAL( 4, node1[1] );
  BOOST_CHECK_EQUAL( 5, node1[2] );
  BOOST_CHECK_EQUAL( 6, node1[3] );
#if 0
  ElementAssociativityConstructor<TopoD2, Quad> assoc2( BasisFunctionAreaBase<Quad>::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, assoc2.order() );
  BOOST_CHECK_EQUAL( 3, assoc2.nNode() );
  BOOST_CHECK_EQUAL( 3, assoc2.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc2.nCell() );

  assoc2.setNodeGlobalMapping( {3, 4, 5} );
  assoc2.setEdgeGlobalMapping( {8, 2, 7} );

  FieldAssociativityConstructorType fldassoc2( BasisFunctionAreaBase<Quad>::HierarchicalP2, nElem);
  fldassoc2.setAssociativity(assoc2, 0);

  FieldAreaClass fld2( fldassoc2 );

  BOOST_CHECK_EQUAL( 1, fld2.nElem() );
  BOOST_CHECK_EQUAL( 0, fld2.nDOF() );


  int node2[3];
  int edge2[3];

  fld2.associativity(0).getNodeGlobalMapping( node2, 3 );
  BOOST_CHECK_EQUAL( 3, node2[0] );
  BOOST_CHECK_EQUAL( 4, node2[1] );
  BOOST_CHECK_EQUAL( 5, node2[2] );

  fld2.associativity(0).getEdgeGlobalMapping( edge2, 3 );
  BOOST_CHECK_EQUAL( 8, edge2[0] );
  BOOST_CHECK_EQUAL( 2, edge2[1] );
  BOOST_CHECK_EQUAL( 7, edge2[2] );
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( accessors )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef FieldAssociativity< FieldGroupAreaTraits<Quad, ArrayQ> > FieldAreaClass;
  typedef typename FieldAreaClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;

  ElementAssociativityConstructor<TopoD2, Quad> assoc1( BasisFunctionAreaBase<Quad>::HierarchicalP1 );
  assoc1.setRank( 2 );
  assoc1.setNodeGlobalMapping( {0, 1, 2, 3} );

  int nElem = 1;
  FieldAssociativityConstructorType fldassoc1( BasisFunctionAreaBase<Quad>::HierarchicalP1, nElem);
  fldassoc1.setAssociativity(assoc1, 0);

  FieldAreaClass qfld( fldassoc1 );

  BOOST_CHECK_EQUAL( 1, qfld.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld.nDOF() );

  ArrayQ* qDOF = new ArrayQ[4];

  qfld.setDOF( qDOF, 4 );
  BOOST_CHECK_EQUAL( 4, qfld.nDOF() );

  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};
  ArrayQ q3 = {5, 6};
  ArrayQ q4 = {7, 8};

  qfld.DOF(0) = q1;
  qfld.DOF(1) = q2;
  qfld.DOF(2) = q3;
  qfld.DOF(3) = q4;

  const Real tol = 1e-13;
  ArrayQ q;
  for (int k = 0; k < 2; k++)
  {
    q = qfld.DOF(0);
    BOOST_CHECK_CLOSE( q1[k]     , q[k], tol );
    BOOST_CHECK_CLOSE( qDOF[0][k], q[k], tol );
    q = qfld.DOF(1);
    BOOST_CHECK_CLOSE( q2[k]     , q[k], tol );
    BOOST_CHECK_CLOSE( qDOF[1][k], q[k], tol );
    q = qfld.DOF(2);
    BOOST_CHECK_CLOSE( q3[k]     , q[k], tol );
    BOOST_CHECK_CLOSE( qDOF[2][k], q[k], tol );
    q = qfld.DOF(3);
    BOOST_CHECK_CLOSE( q4[k]     , q[k], tol );
    BOOST_CHECK_CLOSE( qDOF[3][k], q[k], tol );
  }

  // const accessors
  const FieldAreaClass qfld2 = qfld;

  for (int k = 0; k < 2; k++)
  {
    q = qfld2.DOF(0);
    BOOST_CHECK_CLOSE( q1[k]     , q[k], tol );
    BOOST_CHECK_CLOSE( qDOF[0][k], q[k], tol );
    q = qfld2.DOF(1);
    BOOST_CHECK_CLOSE( q2[k]     , q[k], tol );
    BOOST_CHECK_CLOSE( qDOF[1][k], q[k], tol );
    q = qfld2.DOF(2);
    BOOST_CHECK_CLOSE( q3[k]     , q[k], tol );
    BOOST_CHECK_CLOSE( qDOF[2][k], q[k], tol );
    q = qfld.DOF(3);
    BOOST_CHECK_CLOSE( q4[k]     , q[k], tol );
    BOOST_CHECK_CLOSE( qDOF[3][k], q[k], tol );
  }

  delete [] qDOF;
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( elementfieldP1 )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef FieldAssociativity< FieldGroupAreaTraits<Quad, ArrayQ> > FieldAreaClass;
  typedef typename FieldAreaClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;
  typedef typename FieldAreaClass::template ElementType<> ElementAreaClass;
  typedef std::array<int,4> Int4;

  const Real tol = 1e-13;

  // DOF associativity
  ElementAssociativityConstructor<TopoD2, Quad> assoc( BasisFunctionAreaBase<Quad>::HierarchicalP1 );

  BOOST_CHECK_EQUAL( 1, assoc.order() );
  BOOST_CHECK_EQUAL( 4, assoc.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc.nCell() );

  assoc.setRank( 2 );

  int node[4] = {3, 4, 5, 6};
  assoc.setNodeGlobalMapping( node, 4 );

  Int4 edgeSign = {{+1, -1, +1, +1}};
  assoc.edgeSign() = edgeSign;

  int nElem = 1;
  FieldAssociativityConstructorType fldassoc( BasisFunctionAreaBase<Quad>::HierarchicalP1, nElem);
  fldassoc.setAssociativity(assoc, 0);

  FieldAreaClass qfld( fldassoc );

  BOOST_CHECK_EQUAL( 1, qfld.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld.nDOF() );

  // solution DOFs
  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};
  ArrayQ q3 = {5, 6};
  ArrayQ q4 = {7, 8};

  ArrayQ* qDOF = new ArrayQ[7];

  qfld.setDOF( qDOF, 7 );
  BOOST_CHECK_EQUAL( 7, qfld.nDOF() );

  qfld.DOF(node[0]) = q1;
  qfld.DOF(node[1]) = q2;
  qfld.DOF(node[2]) = q3;
  qfld.DOF(node[3]) = q4;

  // get element DOFs

  ElementAreaClass qfldElem( qfld.basis() );

  qfld.getElement( qfldElem, 0 );

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 4, qfldElem.nDOF() );

  ArrayQ q;
  for (int k = 0; k < 2; k++)
  {
    q = qfldElem.DOF(0);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );
    q = qfldElem.DOF(1);
    BOOST_CHECK_CLOSE( q2[k], q[k], tol );
    q = qfldElem.DOF(2);
    BOOST_CHECK_CLOSE( q3[k], q[k], tol );
    q = qfldElem.DOF(3);
    BOOST_CHECK_CLOSE( q4[k], q[k], tol );
  }

  // set element DOFs

  ArrayQ q1b = {7, 9};
  ArrayQ q2b = {4, 3};
  ArrayQ q3b = {2, 5};
  ArrayQ q4b = {1, 4};

  qfldElem.DOF(0) = q1b;
  qfldElem.DOF(1) = q2b;
  qfldElem.DOF(2) = q3b;
  qfldElem.DOF(3) = q4b;

  qfld.setElement( qfldElem, 0 );

  for (int k = 0; k < 2; k++)
  {
    q = qfld.DOF(node[0]);
    BOOST_CHECK_CLOSE( q1b[k], q[k], tol );
    q = qfld.DOF(node[1]);
    BOOST_CHECK_CLOSE( q2b[k], q[k], tol );
    q = qfld.DOF(node[2]);
    BOOST_CHECK_CLOSE( q3b[k], q[k], tol );
    q = qfld.DOF(node[3]);
    BOOST_CHECK_CLOSE( q4b[k], q[k], tol );
  }

  // edge signs

  Int4 edgeSign2 = qfldElem.edgeSign();

  BOOST_CHECK_EQUAL( +1, edgeSign2[0] );
  BOOST_CHECK_EQUAL( -1, edgeSign2[1] );
  BOOST_CHECK_EQUAL( +1, edgeSign2[2] );
  BOOST_CHECK_EQUAL( +1, edgeSign2[3] );

  delete [] qDOF;
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( elementfieldP3 )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef FieldAssociativity< FieldGroupAreaTraits<Quad, ArrayQ> > FieldAreaClass;
  typedef typename FieldAreaClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;
  typedef typename FieldAreaClass::template ElementType<> ElementAreaClass;
  typedef std::array<int,3> Int3;

  const Real tol = 1e-13;

  // DOF associativity
  ElementAssociativityConstructor<TopoD2, Quad> assoc( BasisFunctionAreaBase<Quad>::HierarchicalP3 );

  BOOST_CHECK_EQUAL( 3, assoc.order() );
  BOOST_CHECK_EQUAL( 3, assoc.nNode() );
  BOOST_CHECK_EQUAL( 6, assoc.nEdge() );
  BOOST_CHECK_EQUAL( 1, assoc.nCell() );

  int node[3] = {3, 4, 5};
  int edge[6] = {9, 2, 1, 6, 0, 8};
  int cell[1] = {7};
  assoc.setNodeGlobalMapping( node, 3 );
  assoc.setEdgeGlobalMapping( edge, 6 );
  assoc.setCellGlobalMapping( cell, 1 );

  Int3 edgeSign = {{+1, -1, +1}};
  assoc.edgeSign() = edgeSign;

  int nElem = 1;
  FieldAssociativityConstructorType fldassoc( BasisFunctionAreaBase<Quad>::HierarchicalP3, nElem);
  fldassoc.setAssociativity(assoc, 0);

  FieldAreaClass qfld( fldassoc );

  BOOST_CHECK_EQUAL( 1, qfld.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld.nDOF() );

  // solution DOFs
  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};
  ArrayQ q3 = {5, 6};
  ArrayQ q4 = {7, 1};
  ArrayQ q5 = {4, 8};
  ArrayQ q6 = {2, 9};
  ArrayQ q7 = {3, 2};
  ArrayQ q8 = {6, 7};
  ArrayQ q9 = {9, 3};
  ArrayQ q0 = {7, 6};

  ArrayQ* qDOF = new ArrayQ[10];

  qfld.setDOF( qDOF, 10 );
  BOOST_CHECK_EQUAL( 10, qfld.nDOF() );

  qfld.DOF(node[0]) = q1;
  qfld.DOF(node[1]) = q2;
  qfld.DOF(node[2]) = q3;
  qfld.DOF(edge[0]) = q4;
  qfld.DOF(edge[1]) = q5;
  qfld.DOF(edge[2]) = q6;
  qfld.DOF(edge[3]) = q7;
  qfld.DOF(edge[4]) = q8;
  qfld.DOF(edge[5]) = q9;
  qfld.DOF(cell[0]) = q0;

  // get element DOFs

  ElementAreaClass qfldElem( qfld.basis() );

  qfld.getElement( qfldElem, 0 );

  BOOST_CHECK_EQUAL(  3, qfldElem.order() );
  BOOST_CHECK_EQUAL( 10, qfldElem.nDOF() );

  ArrayQ q;
  for (int k = 0; k < 2; k++)
  {
    q = qfldElem.DOF(0);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );
    q = qfldElem.DOF(1);
    BOOST_CHECK_CLOSE( q2[k], q[k], tol );
    q = qfldElem.DOF(2);
    BOOST_CHECK_CLOSE( q3[k], q[k], tol );
    q = qfldElem.DOF(3);
    BOOST_CHECK_CLOSE( q4[k], q[k], tol );
    q = qfldElem.DOF(4);
    BOOST_CHECK_CLOSE( q5[k], q[k], tol );
    q = qfldElem.DOF(5);
    BOOST_CHECK_CLOSE( q6[k], q[k], tol );
    q = qfldElem.DOF(6);
    BOOST_CHECK_CLOSE( q7[k], q[k], tol );
    q = qfldElem.DOF(7);
    BOOST_CHECK_CLOSE( q8[k], q[k], tol );
    q = qfldElem.DOF(8);
    BOOST_CHECK_CLOSE( q9[k], q[k], tol );
    q = qfldElem.DOF(9);
    BOOST_CHECK_CLOSE( q0[k], q[k], tol );
  }

  // set element DOFs

  for (int n = 0; n < 10; n++)
  {
    qfldElem.DOF(n) = 0;

    q = qfldElem.DOF(n);
    BOOST_CHECK_SMALL( q[0], tol );
    BOOST_CHECK_SMALL( q[1], tol );
  }

  qfldElem.DOF(0) = q1;
  qfldElem.DOF(1) = q2;
  qfldElem.DOF(2) = q3;
  qfldElem.DOF(3) = q4;
  qfldElem.DOF(4) = q5;
  qfldElem.DOF(5) = q6;
  qfldElem.DOF(6) = q7;
  qfldElem.DOF(7) = q8;
  qfldElem.DOF(8) = q9;
  qfldElem.DOF(9) = q0;

  qfld.setElement( qfldElem, 0 );

  for (int k = 0; k < 2; k++)
  {
    q = qfld.DOF(0);
    BOOST_CHECK_CLOSE( q8[k], q[k], tol );
    q = qfld.DOF(1);
    BOOST_CHECK_CLOSE( q6[k], q[k], tol );
    q = qfld.DOF(2);
    BOOST_CHECK_CLOSE( q5[k], q[k], tol );
    q = qfld.DOF(3);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );
    q = qfld.DOF(4);
    BOOST_CHECK_CLOSE( q2[k], q[k], tol );
    q = qfld.DOF(5);
    BOOST_CHECK_CLOSE( q3[k], q[k], tol );
    q = qfld.DOF(6);
    BOOST_CHECK_CLOSE( q7[k], q[k], tol );
    q = qfld.DOF(7);
    BOOST_CHECK_CLOSE( q0[k], q[k], tol );
    q = qfld.DOF(8);
    BOOST_CHECK_CLOSE( q9[k], q[k], tol );
    q = qfld.DOF(9);
    BOOST_CHECK_CLOSE( q4[k], q[k], tol );
  }

  delete [] qDOF;
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( projection_Hierarchical )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef FieldAssociativity< FieldGroupAreaTraits<Quad, ArrayQ> > FieldAreaClass;
  typedef typename FieldAreaClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;
  typedef typename FieldAreaClass::template ElementType<> ElementAreaClass;
  typedef std::array<int,4> Int4;

  const Real tol = 1e-15;

  for (int order = 1; order < BasisFunctionArea_Quad_HierarchicalPMax; order++)
  {
    for (int inc = 1; inc <= BasisFunctionArea_Quad_HierarchicalPMax-order; inc++)
    {
      const BasisFunctionAreaBase<Quad>* basis0 =
            BasisFunctionAreaBase<Quad>::getBasisFunction( order      , BasisFunctionCategory_Hierarchical );
      const BasisFunctionAreaBase<Quad>* basis1 =
            BasisFunctionAreaBase<Quad>::getBasisFunction( order + inc, BasisFunctionCategory_Hierarchical );

      const int nDOF0 = basis0->nBasis();
      const int nDOF1 = basis1->nBasis();

      // DOF associativity
      ElementAssociativityConstructor<TopoD2, Quad> assoc0( basis0 );
      ElementAssociativityConstructor<TopoD2, Quad> assoc1( basis1 );

      assoc0.setRank( 2 );
      assoc1.setRank( 2 );

      int* map0 = new int[ nDOF0 ];
      int* map1 = new int[ nDOF1 ];
      for (int k = 0; k < nDOF0; k++)
        map0[k] = k;
      for (int k = 0; k < nDOF1; k++)
        map1[k] = nDOF1 - 1 - k;

      assoc0.setGlobalMapping( map0, nDOF0 );
      assoc1.setGlobalMapping( map1, nDOF1 );

      Int4 edgeSign = {{+1, +1, +1, +1}};
      assoc0.edgeSign() = edgeSign;
      assoc1.edgeSign() = edgeSign;

      int nElem = 1;
      FieldAssociativityConstructorType fldassoc0( basis0, nElem);
      FieldAssociativityConstructorType fldassoc1( basis1, nElem);
      fldassoc0.setAssociativity(assoc0, 0);
      fldassoc1.setAssociativity(assoc1, 0);

      FieldAreaClass qfld0( fldassoc0 );
      FieldAreaClass qfld1( fldassoc1 );

      // solution DOFs
      ArrayQ* qDOF0 = new ArrayQ[ nDOF0 ];
      ArrayQ* qDOF1 = new ArrayQ[ nDOF1 ];
      qfld0.setDOF( qDOF0, nDOF0 );
      qfld1.setDOF( qDOF1, nDOF1 );

      for (int k = 0; k < nDOF0; k++)
      {
        ArrayQ q = { sqrt(k+7), 1./(k+4) };
        qfld0.DOF(k) = q;
      }

      qfld0.projectTo( qfld1 );

      ElementAreaClass qfldElem0( basis0 );
      ElementAreaClass qfldElem1( basis1 );
      qfld0.getElement( qfldElem0, 0 );
      qfld1.getElement( qfldElem1, 0 );

      ArrayQ q0, q1;
      Real s, t;

      for (int is = 0; is < 3; is++)
      {
        for (int it = 0; it < 3; it++)
        {
          s = 0.11 + is*0.3357;
          t = 0.13 + it*0.2986;

          qfldElem0.eval( s, t, q0 );
          qfldElem1.eval( s, t, q1 );

          for (int n = 0; n < ArrayQ::N; n++)
            BOOST_CHECK_CLOSE( q0[n], q1[n], tol );
        }
      }

      delete [] map0;
      delete [] map1;
      delete [] qDOF0;
      delete [] qDOF1;
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/FieldAssociativityArea_Quad_pattern.txt", true );

  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef FieldAssociativity< FieldGroupAreaTraits<Quad, ArrayQ> > FieldAreaClass;
  typedef typename FieldAreaClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;
  typedef std::array<int,4> Int4;

  // DOF associativity
  ElementAssociativityConstructor<TopoD2, Quad> assoc( BasisFunctionAreaBase<Quad>::HierarchicalP1 );

  assoc.setRank( 2 );

  int node[4] = {0, 1, 2, 3};
  assoc.setNodeGlobalMapping( node, 4 );

  Int4 edgeSign = {{+1, -1, +1, +1}};
  assoc.edgeSign() = edgeSign;

  int nElem = 1;
  FieldAssociativityConstructorType fldassoc( BasisFunctionAreaBase<Quad>::HierarchicalP1, nElem);
  fldassoc.setAssociativity(assoc, 0);

  FieldAreaClass qfld( fldassoc );

  // solution DOFs
  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};
  ArrayQ q3 = {5, 6};
  ArrayQ q4 = {7, 8};

  ArrayQ* qDOF = new ArrayQ[4];

  qfld.setDOF( qDOF, 4 );
  BOOST_CHECK_EQUAL( 4, qfld.nDOF() );

  qfld.DOF(node[0]) = q1;
  qfld.DOF(node[1]) = q2;
  qfld.DOF(node[2]) = q3;
  qfld.DOF(node[3]) = q4;

  qfld.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  delete [] qDOF;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
