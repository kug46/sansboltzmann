// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field4D_DG_Pentatope_btest
// testing of Field4D_DG_* classes
//

//#define SLEEP_FOR_OUTPUT
#ifdef SLEEP_FOR_OUTPUT
#define SLEEP_MILLISECONDS 500
#include <chrono>
#include <thread>
#endif

#include <set>
#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;


#include "unit/UnitGrids/XField_KuhnFreudenthal.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionSpacetime_Pentatope_Lagrange.h"
#include "Field/FieldSpacetime_DG_Cell.h"
#include "Field/FieldSpacetime_DG_InteriorTrace.h"
#include "Field/FieldSpacetime_DG_BoundaryTrace.h"

#include "Field/output_Tecplot.h"
#include "tools/output_std_vector.h"

#ifdef SANS_MPI
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/mpi/collectives/all_gather.hpp>
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
#ifdef SANS_AVRO
template class Field_DG_Cell< PhysD4, TopoD4, Real >;
template class Field_DG_InteriorTrace< PhysD4, TopoD4, Real >;
template class Field_DG_BoundaryTrace< PhysD4, TopoD4, Real >;
#endif
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( Field_DG_Pentatope_test_suite )

#ifdef SANS_AVRO

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Spacetime_LagrangeP1 )
{
  typedef DLA::VectorS<4, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD4, TopoD4, ArrayQ > QField4D_DG_Spacetime;
  typedef QField4D_DG_Spacetime::FieldCellGroupType<Pentatope> QFieldSpacetimeClass;
  typedef QFieldSpacetimeClass::ElementType<> ElementQFieldClass;

  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  // loop over various mesh sizes
  for (int N=2;N<5;N++)
  {
    XField_KuhnFreudenthal<PhysD4,TopoD4> xfld1( comm , {N,N,N,N} );

    int order = 1;
    QField4D_DG_Spacetime qfld1(xfld1, order, BasisFunctionCategory_Lagrange);

    BOOST_CHECK( qfld1.spaceType() == SpaceType::Discontinuous );

    BOOST_CHECK_EQUAL( 24*pow(N-1,4)*5 , qfld1.nDOF() );
    BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
    BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
    BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
    BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

    BOOST_CHECK_EQUAL( 24*pow(N-1,4)*5 , qfld1.nDOFCellGroup(0) );
    BOOST_CHECK_THROW( qfld1.nDOFInteriorTraceGroup(0), SANSException);
    BOOST_CHECK_THROW( qfld1.nDOFBoundaryTraceGroup(0), SANSException);

    const int nDOFPDE = qfld1.nDOF();
    for (int n = 0; n < nDOFPDE; n++)
      qfld1.DOF(n) = n;

    Field< PhysD4, TopoD4, ArrayQ > qfld2(qfld1, FieldCopy());

    BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld2.nDOF() );
    BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld2.nDOFpossessed() );
    BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld2.nDOFghost() );
    BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld2.nInteriorTraceGroups() );
    BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld2.nBoundaryTraceGroups() );
    BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld2.nCellGroups() );
    BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld2.getXField() );

    BOOST_CHECK_THROW( qfld2.nDOFCellGroup(0), SANSException);
    BOOST_CHECK_THROW( qfld2.nDOFInteriorTraceGroup(0), SANSException);
    BOOST_CHECK_THROW( qfld2.nDOFBoundaryTraceGroup(0), SANSException);

    const QFieldSpacetimeClass& qfld1Spacetime = qfld1.getCellGroup<Pentatope>(0);
    const QFieldSpacetimeClass& qfld2Spacetime = qfld2.getCellGroup<Pentatope>(0);

    for (int n = 0; n < nDOFPDE; n++)
    {
      BOOST_CHECK_EQUAL( qfld1.DOF(n)[0], n );
      BOOST_CHECK_EQUAL( qfld1.DOF(n)[0], qfld1Spacetime.DOF(n)[0] );
      BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], n );
      BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], qfld2Spacetime.DOF(n)[0] );
    }

    for (int n = 0; n < nDOFPDE; n++)
      qfld1.DOF(n) = 0;

    for (int n = 0; n < nDOFPDE; n++)
    {
      BOOST_CHECK_EQUAL( qfld1.DOF(n)[0], 0 );
      BOOST_CHECK_EQUAL( qfld1.DOF(n)[0], qfld1Spacetime.DOF(n)[0] );
      BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], n );
      BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], qfld2Spacetime.DOF(n)[0] );
    }

    // Test the constant assignment operator
    ElementQFieldClass qfldElem(qfld1Spacetime.basis());

    qfld1 = 1.23;
    ArrayQ q1;

    for (int elem = 0; elem < qfld1Spacetime.nElem(); elem++)
    {
      qfld1Spacetime.getElement(qfldElem, elem);

      qfldElem.eval(0.25, 0.25 , 0.25, 0.25, q1);
      BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
      BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
    }

    ArrayQ q0 = {4.56, 7.89, 2.34, 9.1781};
    qfld1 = q0;

    for (int elem = 0; elem < qfld1Spacetime.nElem(); elem++)
    {
      qfld1Spacetime.getElement(qfldElem, elem);

      qfldElem.eval(0.25, 0.25, 0.25, 0.25, q1);
      BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
    }

  } // loop over mesh size

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Spacetime_LagrangeP2 )
{
  typedef DLA::VectorS<4, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD4, TopoD4, ArrayQ > QField4D_DG_Spacetime;
  typedef QField4D_DG_Spacetime::FieldCellGroupType<Pentatope> QFieldSpacetimeClass;
  typedef QFieldSpacetimeClass::ElementType<> ElementQFieldClass;

  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  int order = 2;
  int NDOF_PER_PENTATOPE = (order+1)*(order+2)*(order+3)*(order+4)/24;

  // loop over various mesh sizes
  for (int N=2;N<5;N++)
  {
    XField_KuhnFreudenthal<PhysD4,TopoD4> xfld1( comm , {N,N,N,N} );

    QField4D_DG_Spacetime qfld1(xfld1, order, BasisFunctionCategory_Lagrange);

    BOOST_CHECK( qfld1.spaceType() == SpaceType::Discontinuous );

    BOOST_CHECK_EQUAL( 24*pow(N-1,4)*NDOF_PER_PENTATOPE , qfld1.nDOF() );
    BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
    BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
    BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
    BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

    BOOST_CHECK_EQUAL( 24*pow(N-1,4)*NDOF_PER_PENTATOPE , qfld1.nDOFCellGroup(0) );
    BOOST_CHECK_THROW( qfld1.nDOFInteriorTraceGroup(0), SANSException);
    BOOST_CHECK_THROW( qfld1.nDOFBoundaryTraceGroup(0), SANSException);

    const int nDOFPDE = qfld1.nDOF();
    for (int n = 0; n < nDOFPDE; n++)
      qfld1.DOF(n) = n;

    Field< PhysD4, TopoD4, ArrayQ > qfld2(qfld1, FieldCopy());

    BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld2.nDOF() );
    BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld2.nDOFpossessed() );
    BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld2.nDOFghost() );
    BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld2.nInteriorTraceGroups() );
    BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld2.nBoundaryTraceGroups() );
    BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld2.nCellGroups() );
    BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld2.getXField() );

    BOOST_CHECK_THROW( qfld2.nDOFCellGroup(0), SANSException);
    BOOST_CHECK_THROW( qfld2.nDOFInteriorTraceGroup(0), SANSException);
    BOOST_CHECK_THROW( qfld2.nDOFBoundaryTraceGroup(0), SANSException);

    const QFieldSpacetimeClass& qfld1Spacetime = qfld1.getCellGroup<Pentatope>(0);
    const QFieldSpacetimeClass& qfld2Spacetime = qfld2.getCellGroup<Pentatope>(0);

    for (int n = 0; n < nDOFPDE; n++)
    {
      BOOST_CHECK_EQUAL( qfld1.DOF(n)[0], n );
      BOOST_CHECK_EQUAL( qfld1.DOF(n)[0], qfld1Spacetime.DOF(n)[0] );
      BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], n );
      BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], qfld2Spacetime.DOF(n)[0] );
    }

    for (int n = 0; n < nDOFPDE; n++)
      qfld1.DOF(n) = 0;

    for (int n = 0; n < nDOFPDE; n++)
    {
      BOOST_CHECK_EQUAL( qfld1.DOF(n)[0], 0 );
      BOOST_CHECK_EQUAL( qfld1.DOF(n)[0], qfld1Spacetime.DOF(n)[0] );
      BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], n );
      BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], qfld2Spacetime.DOF(n)[0] );
    }

    // Test the constant assignment operator
    ElementQFieldClass qfldElem(qfld1Spacetime.basis());

    qfld1 = 1.23;
    ArrayQ q1;

    for (int elem = 0; elem < qfld1Spacetime.nElem(); elem++)
    {
      qfld1Spacetime.getElement(qfldElem, elem);

      qfldElem.eval(0.25, 0.25 , 0.25, 0.25, q1);
      BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
      BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
    }

    ArrayQ q0 = {4.56, 7.89, 2.34, 9.1781};
    qfld1 = q0;

    for (int elem = 0; elem < qfld1Spacetime.nElem(); elem++)
    {
      qfld1Spacetime.getElement(qfldElem, elem);

      qfldElem.eval(0.25, 0.25, 0.25, 0.25, q1);
      BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
    }

  } // loop over mesh size
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Spacetime_LagrangeP3 )
{
  typedef DLA::VectorS<4, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD4, TopoD4, ArrayQ > QField4D_DG_Spacetime;
  typedef QField4D_DG_Spacetime::FieldCellGroupType<Pentatope> QFieldSpacetimeClass;
  typedef QFieldSpacetimeClass::ElementType<> ElementQFieldClass;

  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  int order = 3;
  int NDOF_PER_PENTATOPE = (order+1)*(order+2)*(order+3)*(order+4)/24;

  // loop over various mesh sizes
  for (int N=2;N<5;N++)
  {
    XField_KuhnFreudenthal<PhysD4,TopoD4> xfld1( comm , {N,N,N,N} );

    QField4D_DG_Spacetime qfld1(xfld1, order, BasisFunctionCategory_Lagrange);

    BOOST_CHECK( qfld1.spaceType() == SpaceType::Discontinuous );

    BOOST_CHECK_EQUAL( 24*pow(N-1,4)*NDOF_PER_PENTATOPE , qfld1.nDOF() );
    BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
    BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
    BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
    BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

    BOOST_CHECK_EQUAL( 24*pow(N-1,4)*NDOF_PER_PENTATOPE , qfld1.nDOFCellGroup(0) );
    BOOST_CHECK_THROW( qfld1.nDOFInteriorTraceGroup(0), SANSException);
    BOOST_CHECK_THROW( qfld1.nDOFBoundaryTraceGroup(0), SANSException);

    const int nDOFPDE = qfld1.nDOF();
    for (int n = 0; n < nDOFPDE; n++)
      qfld1.DOF(n) = n;

    Field< PhysD4, TopoD4, ArrayQ > qfld2(qfld1, FieldCopy());

    BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld2.nDOF() );
    BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld2.nDOFpossessed() );
    BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld2.nDOFghost() );
    BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld2.nInteriorTraceGroups() );
    BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld2.nBoundaryTraceGroups() );
    BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld2.nCellGroups() );
    BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld2.getXField() );

    BOOST_CHECK_THROW( qfld2.nDOFCellGroup(0), SANSException);
    BOOST_CHECK_THROW( qfld2.nDOFInteriorTraceGroup(0), SANSException);
    BOOST_CHECK_THROW( qfld2.nDOFBoundaryTraceGroup(0), SANSException);

    const QFieldSpacetimeClass& qfld1Spacetime = qfld1.getCellGroup<Pentatope>(0);
    const QFieldSpacetimeClass& qfld2Spacetime = qfld2.getCellGroup<Pentatope>(0);

    for (int n = 0; n < nDOFPDE; n++)
    {
      BOOST_CHECK_EQUAL( qfld1.DOF(n)[0], n );
      BOOST_CHECK_EQUAL( qfld1.DOF(n)[0], qfld1Spacetime.DOF(n)[0] );
      BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], n );
      BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], qfld2Spacetime.DOF(n)[0] );
    }

    for (int n = 0; n < nDOFPDE; n++)
      qfld1.DOF(n) = 0;

    for (int n = 0; n < nDOFPDE; n++)
    {
      BOOST_CHECK_EQUAL( qfld1.DOF(n)[0], 0 );
      BOOST_CHECK_EQUAL( qfld1.DOF(n)[0], qfld1Spacetime.DOF(n)[0] );
      BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], n );
      BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], qfld2Spacetime.DOF(n)[0] );
    }

    // Test the constant assignment operator
    ElementQFieldClass qfldElem(qfld1Spacetime.basis());

    qfld1 = 1.23;
    ArrayQ q1;

    for (int elem = 0; elem < qfld1Spacetime.nElem(); elem++)
    {
      qfld1Spacetime.getElement(qfldElem, elem);

      qfldElem.eval(0.25, 0.25 , 0.25, 0.25, q1);
      BOOST_CHECK_CLOSE(1.23, q1[0], 1e-11);
      BOOST_CHECK_CLOSE(1.23, q1[1], 1e-11);
    }

    ArrayQ q0 = {4.56, 7.89, 2.34, 9.1781};
    qfld1 = q0;

    for (int elem = 0; elem < qfld1Spacetime.nElem(); elem++)
    {
      qfld1Spacetime.getElement(qfldElem, elem);

      qfldElem.eval(0.25, 0.25, 0.25, 0.25, q1);
      BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-11);
      BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-11);
    }

  } // loop over mesh size
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Spacetime_LagrangeP4 )
{
  typedef DLA::VectorS<4, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD4, TopoD4, ArrayQ > QField4D_DG_Spacetime;
  typedef QField4D_DG_Spacetime::FieldCellGroupType<Pentatope> QFieldSpacetimeClass;
  typedef QFieldSpacetimeClass::ElementType<> ElementQFieldClass;

  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  int order = 4;
  int NDOF_PER_PENTATOPE = (order+1)*(order+2)*(order+3)*(order+4)/24;

  // loop over various mesh sizes
  for (int N=2;N<5;N++)
  {
    XField_KuhnFreudenthal<PhysD4,TopoD4> xfld1( comm , {N,N,N,N} );

    QField4D_DG_Spacetime qfld1(xfld1, order, BasisFunctionCategory_Lagrange);

    BOOST_CHECK( qfld1.spaceType() == SpaceType::Discontinuous );

    BOOST_CHECK_EQUAL( 24*pow(N-1,4)*NDOF_PER_PENTATOPE , qfld1.nDOF() );
    BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
    BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
    BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
    BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

    BOOST_CHECK_EQUAL( 24*pow(N-1,4)*NDOF_PER_PENTATOPE , qfld1.nDOFCellGroup(0) );
    BOOST_CHECK_THROW( qfld1.nDOFInteriorTraceGroup(0), SANSException);
    BOOST_CHECK_THROW( qfld1.nDOFBoundaryTraceGroup(0), SANSException);

    const int nDOFPDE = qfld1.nDOF();
    for (int n = 0; n < nDOFPDE; n++)
      qfld1.DOF(n) = n;

    Field< PhysD4, TopoD4, ArrayQ > qfld2(qfld1, FieldCopy());

    BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld2.nDOF() );
    BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld2.nDOFpossessed() );
    BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld2.nDOFghost() );
    BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld2.nInteriorTraceGroups() );
    BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld2.nBoundaryTraceGroups() );
    BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld2.nCellGroups() );
    BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld2.getXField() );

    BOOST_CHECK_THROW( qfld2.nDOFCellGroup(0), SANSException);
    BOOST_CHECK_THROW( qfld2.nDOFInteriorTraceGroup(0), SANSException);
    BOOST_CHECK_THROW( qfld2.nDOFBoundaryTraceGroup(0), SANSException);

    const QFieldSpacetimeClass& qfld1Spacetime = qfld1.getCellGroup<Pentatope>(0);
    const QFieldSpacetimeClass& qfld2Spacetime = qfld2.getCellGroup<Pentatope>(0);

    for (int n = 0; n < nDOFPDE; n++)
    {
      BOOST_CHECK_EQUAL( qfld1.DOF(n)[0], n );
      BOOST_CHECK_EQUAL( qfld1.DOF(n)[0], qfld1Spacetime.DOF(n)[0] );
      BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], n );
      BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], qfld2Spacetime.DOF(n)[0] );
    }

    for (int n = 0; n < nDOFPDE; n++)
      qfld1.DOF(n) = 0;

    for (int n = 0; n < nDOFPDE; n++)
    {
      BOOST_CHECK_EQUAL( qfld1.DOF(n)[0], 0 );
      BOOST_CHECK_EQUAL( qfld1.DOF(n)[0], qfld1Spacetime.DOF(n)[0] );
      BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], n );
      BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], qfld2Spacetime.DOF(n)[0] );
    }

    // Test the constant assignment operator
    ElementQFieldClass qfldElem(qfld1Spacetime.basis());

    qfld1 = 1.23;
    ArrayQ q1;

    for (int elem = 0; elem < qfld1Spacetime.nElem(); elem++)
    {
      qfld1Spacetime.getElement(qfldElem, elem);

      qfldElem.eval(0.25, 0.25 , 0.25, 0.25, q1);
      BOOST_CHECK_CLOSE(1.23, q1[0], 1e-10);
      BOOST_CHECK_CLOSE(1.23, q1[1], 1e-10);
    }

    ArrayQ q0 = {4.56, 7.89, 2.34, 9.1781};
    qfld1 = q0;

    for (int elem = 0; elem < qfld1Spacetime.nElem(); elem++)
    {
      qfld1Spacetime.getElement(qfldElem, elem);

      qfldElem.eval(0.25, 0.25, 0.25, 0.25, q1);
      BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-10);
      BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-10);
    }

  } // loop over mesh size
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Spacetime_ProjectPtoPp1 )
{
  typedef DLA::VectorS<4, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD4, TopoD4, ArrayQ > QField4D_DG_Spacetime;
  typedef QField4D_DG_Spacetime::FieldCellGroupType<Pentatope> QFieldSpacetimeClass;
  typedef QFieldSpacetimeClass::ElementType<> ElementQFieldClass;

  ArrayQ q0, q1;

  mpi::communicator comm;
  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld1( comm , {2,2,2,2} );

  for (int order = 1; order < BasisFunctionSpacetime_Pentatope_LagrangePMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionSpacetime_Pentatope_LagrangePMax-order; orderinc++)
    {
      printf("testing projection from p = %d to p = %d\n",order,orderinc);

      QField4D_DG_Spacetime qfldP  (xfld1, order         , BasisFunctionCategory_Lagrange);
      QField4D_DG_Spacetime qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Lagrange);

      typedef QField4D_DG_Spacetime::FieldCellGroupType<Pentatope> QFieldSpacetimeClass;

      QFieldSpacetimeClass& qfldSpacetimeP   = qfldP.getCellGroup<Pentatope>(0);
      QFieldSpacetimeClass& qfldSpacetimePp1 = qfldPp1.getCellGroup<Pentatope>(0);

      ElementQFieldClass qfldElemP(qfldSpacetimeP.basis());
      ElementQFieldClass qfldElemPp1(qfldSpacetimePp1.basis());

      // give some non-zero initial condition
      for (int n = 0; n < qfldP.nDOF(); n++)
        qfldP.DOF(n) = n+1;

      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = 0;

      // use spacetime function projectTo
      qfldSpacetimeP.projectTo(qfldSpacetimePp1);

      for (int elem = 0; elem < qfldSpacetimeP.nElem(); elem++)
      {
        qfldSpacetimeP.getElement(qfldElemP, elem);
        qfldSpacetimePp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, 0.25, 0.25, 0.25, q0);
        qfldElemPp1.eval(0.25, 0.25, 0.25, 0.25, q1);
        for (int i= 0; i < ArrayQ::M; i++)
          BOOST_CHECK_CLOSE(q0[i], q1[i], 1e-12);
      }

      //Wipe out DOF's for P1
      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = -1;

      //Use base function projectTo
      qfldP.projectTo(qfldPp1);

      for (int elem = 0; elem < qfldSpacetimeP.nElem(); elem++)
      {
        qfldSpacetimeP.getElement(qfldElemP, elem);
        qfldSpacetimePp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, 0.25, 0.25, 0.25, q0);
        qfldElemPp1.eval(0.25, 0.25, 0.25, 0.25, q1);
        for (int i= 0; i < ArrayQ::M; i++)
          BOOST_CHECK_CLOSE(q0[i], q1[i], 1e-12);
      }
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_InteriorTet_LagrangeP1 )
{
  typedef DLA::VectorS<4, Real> ArrayQ;
  typedef Field_DG_InteriorTrace< PhysD4, TopoD4, ArrayQ > QField4D_DG_InteriorTet;
  typedef QField4D_DG_InteriorTet::FieldTraceGroupType<Tet> QFieldTetClass;
  typedef QFieldTetClass::ElementType<> ElementQFieldTetClass;

  mpi::communicator comm;
  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld1( comm , {2,2,2,2} );

  // there are 36 interior facets and 48 boundary facets
  // see avro sandbox_xfield_TraceToCellRefCoord_ut
  int nInterior = 36;

  for (int order=1;order<=BasisFunctionSpacetime_Pentatope_LagrangePMax;order++)
  {
    int NDOF_PER_TET = (order+1)*(order+2)*(order+3)/6;

    int tetMap[4];

    QField4D_DG_InteriorTet qfld1(xfld1, order, BasisFunctionCategory_Lagrange);

    BOOST_CHECK_EQUAL( nInterior*NDOF_PER_TET , qfld1.nDOF() );
    BOOST_CHECK_EQUAL( 1, qfld1.nInteriorTraceGroups() );
    BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
    BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
    BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

    BOOST_CHECK_EQUAL( nInterior*NDOF_PER_TET , qfld1.nDOFInteriorTraceGroup(0) );
    BOOST_CHECK_THROW( qfld1.nDOFCellGroup(0), SANSException);
    BOOST_CHECK_THROW( qfld1.nDOFBoundaryTraceGroup(0), SANSException);

    const QFieldTetClass& qfldGroup1 = qfld1.getInteriorTraceGroup<Tet>(0);

    int count = 0;
    for (int elem=0;elem<qfldGroup1.nElem();elem++)
    {
      qfldGroup1.associativity(elem).getNodeGlobalMapping( tetMap , 4 );
      BOOST_CHECK_EQUAL( tetMap[0] , count   );
      BOOST_CHECK_EQUAL( tetMap[1] , count+1 );
      BOOST_CHECK_EQUAL( tetMap[2] , count+2 );
      BOOST_CHECK_EQUAL( tetMap[3] , count+3 );
      count += NDOF_PER_TET;
    }

    // check the copy
    Field< PhysD4, TopoD4, ArrayQ > qfld2(qfld1, FieldCopy());

    BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld2.nDOF() );
    BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld2.nDOFpossessed() );
    BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld2.nDOFghost() );
    BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld2.nInteriorTraceGroups() );
    BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld2.nBoundaryTraceGroups() );
    BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld2.nCellGroups() );
    BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld2.getXField() );

    BOOST_CHECK_THROW( qfld2.nDOFCellGroup(0), SANSException);
    BOOST_CHECK_THROW( qfld2.nDOFInteriorTraceGroup(0), SANSException);
    BOOST_CHECK_THROW( qfld2.nDOFBoundaryTraceGroup(0), SANSException);

    // Test the constant assignment operator
    ElementQFieldTetClass qfldElem(qfldGroup1.basis());

    qfld1 = 1.23;
    ArrayQ q1;

    for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
    {
      qfldGroup1.getElement(qfldElem, elem);

      qfldElem.eval(0.25,0.25,0.25, q1);
      BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
      BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
    }

    ArrayQ q0 = {4.56, 7.89, 2.34, 9.1781};
    qfld1 = q0;

    for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
    {
      qfldGroup1.getElement(qfldElem, elem);

      qfldElem.eval(0.25,0.25,0.25, q1);
      BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_InteriorTet_Lagrange_ProjectPtoPp1 )
{
  typedef DLA::VectorS<4, Real> ArrayQ;
  typedef Field_DG_InteriorTrace< PhysD4, TopoD4, ArrayQ > QField4D_DG_InteriorTet;
  typedef QField4D_DG_InteriorTet::FieldTraceGroupType<Tet> QFieldTetClass;
  typedef QFieldTetClass::ElementType<> ElementQFieldTetClass;

  ArrayQ q1, q2;

  mpi::communicator comm;
  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld1(comm,{2,2,2,2});

  for (int order = 1; order < BasisFunctionSpacetime_Pentatope_LagrangePMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionSpacetime_Pentatope_LagrangePMax-order; orderinc++)
    {
      QField4D_DG_InteriorTet qfldP(xfld1, order  , BasisFunctionCategory_Lagrange);
      QField4D_DG_InteriorTet qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Lagrange);

      for ( int group = 0; group < qfldP.nInteriorTraceGroups(); group++ )
      {
        QFieldTetClass& qfldTetGroupP   = qfldP.getInteriorTraceGroup<Tet>(group);
        QFieldTetClass& qfldTetGroupPp1 = qfldPp1.getInteriorTraceGroup<Tet>(group);

        ElementQFieldTetClass qfldElemP(qfldTetGroupP.basis());
        ElementQFieldTetClass qfldElemPp1(qfldTetGroupPp1.basis());

        //Give some non-zero initial condition
        for (int n = 0; n < qfldP.nDOF(); n++)
          qfldP.DOF(n) = n+1;

        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = 0;

        //Use line function projectTo
        qfldTetGroupP.projectTo(qfldTetGroupPp1);

        for (int elem = 0; elem < qfldTetGroupP.nElem(); elem++)
        {
          qfldTetGroupP.getElement(qfldElemP, elem);
          qfldTetGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.25, 0.25, 0.25, q1);
          qfldElemPp1.eval(0.25, 0.25, 0.25, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-10);
        }

        //Wipe out DOF's for P1
        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = -1;

        //Use base function projectTo
        qfldP.projectTo(qfldPp1);

        for (int elem = 0; elem < qfldTetGroupP.nElem(); elem++)
        {
          qfldTetGroupP.getElement(qfldElemP, elem);
          qfldTetGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.75,  0.75, 0.75, q1);
          qfldElemPp1.eval(0.75, 0.75, 0.75, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 5e-10);
        }
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_BoundaryTet_LagrangeP0 )
{
  typedef DLA::VectorS<4, Real> ArrayQ;
  typedef Field_DG_BoundaryTrace< PhysD4, TopoD4, ArrayQ > QField4D_DG_BoundaryTet;
  typedef QField4D_DG_BoundaryTet::FieldTraceGroupType<Tet> QFieldTetClass;
  typedef QFieldTetClass::ElementType<> ElementQFieldTetClass;

  mpi::communicator comm;
  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld1( comm , {2,2,2,2} );

  // there are 36 interior facets and 48 boundary facets
  // however the 48 boundary facets are split into 8 (for each bounding cube)
  // see avro sandbox_xfield_TraceToCellRefCoord_ut
  int nBoundary = 6; // number of bounding tetrahedra in each cube

  for (int order=1;order<=BasisFunctionSpacetime_Pentatope_LagrangePMax;order++)
  {
    int NDOF_PER_TET = (order+1)*(order+2)*(order+3)/6;

    int tetMap[4];

    QField4D_DG_BoundaryTet qfld1(xfld1, order, BasisFunctionCategory_Lagrange);

    // the total number of dof should be the number of bounding cubes (8) times the
    // number of tetrahedra it takes to fill each cube (6) times the number
    // of DOF per tetrahedra
    BOOST_CHECK_EQUAL( 8*nBoundary*NDOF_PER_TET , qfld1.nDOF() );
    BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
    BOOST_CHECK_EQUAL( 8, qfld1.nBoundaryTraceGroups() ); // there are eight bounding cubes
    BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
    BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

    int count = 0;
    for (int bnd=0;bnd<8;bnd++)
    {
      BOOST_CHECK_EQUAL( nBoundary*NDOF_PER_TET , qfld1.nDOFBoundaryTraceGroup(bnd) );
      BOOST_CHECK_THROW( qfld1.nDOFCellGroup(0), SANSException);
      BOOST_CHECK_THROW( qfld1.nDOFInteriorTraceGroup(0), SANSException);

      const QFieldTetClass& qfldGroup1 = qfld1.getBoundaryTraceGroup<Tet>(bnd);

      for (int elem=0;elem<qfldGroup1.nElem();elem++)
      {
        qfldGroup1.associativity(elem).getNodeGlobalMapping( tetMap , 4 );
        BOOST_CHECK_EQUAL( tetMap[0] , count   );
        BOOST_CHECK_EQUAL( tetMap[1] , count+1 );
        BOOST_CHECK_EQUAL( tetMap[2] , count+2 );
        BOOST_CHECK_EQUAL( tetMap[3] , count+3 );
        count += NDOF_PER_TET;
      }

      // check the copy
      Field< PhysD4, TopoD4, ArrayQ > qfld2(qfld1, FieldCopy());

      BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld2.nDOF() );
      BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld2.nDOFpossessed() );
      BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld2.nDOFghost() );
      BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld2.nInteriorTraceGroups() );
      BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld2.nBoundaryTraceGroups() );
      BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld2.nCellGroups() );
      BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld2.getXField() );

      BOOST_CHECK_THROW( qfld2.nDOFCellGroup(0), SANSException);
      BOOST_CHECK_THROW( qfld2.nDOFInteriorTraceGroup(0), SANSException);
      BOOST_CHECK_THROW( qfld2.nDOFBoundaryTraceGroup(0), SANSException);

      // Test the constant assignment operator
      ElementQFieldTetClass qfldElem(qfldGroup1.basis());

      qfld1 = 1.23;
      ArrayQ q1;

      for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
      {
        qfldGroup1.getElement(qfldElem, elem);

        qfldElem.eval(0.25,0.25,0.25, q1);
        BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
        BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
      }

      ArrayQ q0 = {4.56, 7.89, 2.34, 9.1781};
      qfld1 = q0;

      for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
      {
        qfldGroup1.getElement(qfldElem, elem);

        qfldElem.eval(0.25,0.25,0.25, q1);
        BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
        BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
      }
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_BoundaryTet_Lagrange_ProjectPtoPp1 )
{
  typedef DLA::VectorS<4, Real> ArrayQ;
  typedef Field_DG_BoundaryTrace< PhysD4, TopoD4, ArrayQ > QField4D_DG_BoundaryTet;
  typedef QField4D_DG_BoundaryTet::FieldTraceGroupType<Tet> QFieldTetClass;
  typedef QFieldTetClass::ElementType<> ElementQFieldTetClass;

  ArrayQ q1, q2;

  mpi::communicator comm;
  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld1(comm,{2,2,2,2});

  for (int order = 1; order < BasisFunctionSpacetime_Pentatope_LagrangePMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionSpacetime_Pentatope_LagrangePMax-order; orderinc++)
    {
      QField4D_DG_BoundaryTet qfldP(xfld1, order  , BasisFunctionCategory_Lagrange);
      QField4D_DG_BoundaryTet qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Lagrange);

      for ( int group = 0; group < qfldP.nBoundaryTraceGroups(); group++ )
      {
        QFieldTetClass& qfldTetGroupP   = qfldP.getBoundaryTraceGroup<Tet>(group);
        QFieldTetClass& qfldTetGroupPp1 = qfldPp1.getBoundaryTraceGroup<Tet>(group);

        ElementQFieldTetClass qfldElemP(qfldTetGroupP.basis());
        ElementQFieldTetClass qfldElemPp1(qfldTetGroupPp1.basis());

        //Give some non-zero initial condition
        for (int n = 0; n < qfldP.nDOF(); n++)
          qfldP.DOF(n) = n+1;

        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = 0;

        //Use line function projectTo
        qfldTetGroupP.projectTo(qfldTetGroupPp1);

        for (int elem = 0; elem < qfldTetGroupP.nElem(); elem++)
        {
          qfldTetGroupP.getElement(qfldElemP, elem);
          qfldTetGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.25, 0.25, 0.25, q1);
          qfldElemPp1.eval(0.25, 0.25, 0.25, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-10);
        }

        //Wipe out DOF's for P1
        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = -1;

        //Use base function projectTo
        qfldP.projectTo(qfldPp1);

        for (int elem = 0; elem < qfldTetGroupP.nElem(); elem++)
        {
          qfldTetGroupP.getElement(qfldElemP, elem);
          qfldTetGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.75,  0.75, 0.75, q1);
          qfldElemPp1.eval(0.75, 0.75, 0.75, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 5e-10);
        }
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedNativeIndexing )
{
  typedef Real ArrayQ;
  typedef Field_DG_Cell< PhysD4, TopoD4, ArrayQ > QField4D_DG_Spacetime;
  typedef QField4D_DG_Spacetime::FieldCellGroupType<Pentatope> QFieldSpacetimeClass;

  // global communicator
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  // Generate a global grid (identical on all processors)
  int N = 5;
  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld_global(comm, {N,N,N,N});

  // Generate a partitioned grid
  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld_local(world, {N,N,N,N});

  // local and global fields
  int order = 1;
  QField4D_DG_Spacetime qfld_local(xfld_local, order, BasisFunctionCategory_Lagrange);
  QField4D_DG_Spacetime qfld_global(xfld_global, order, BasisFunctionCategory_Lagrange);

  // gather the local native DOFs and make sure they are unique for each processor
  std::vector<int> nativeDOF(qfld_local.nDOFpossessed());
  for (int idof = 0; idof < qfld_local.nDOFpossessed(); idof++)
    nativeDOF[idof] = qfld_local.local2nativeDOFmap(idof);

  // send the native DOF to all other ranks
  std::vector<std::vector<int>> globalnativeDOF(world.size());
#ifdef SANS_MPI
  boost::mpi::all_gather(world, nativeDOF, globalnativeDOF);
#else
  globalnativeDOF[0] = nativeDOF;
#endif

  // check that the possessed nativeDOF is unieque to each processor
  std::set<int> uniqueNativeDOF;
  for (std::size_t rank = 0; rank < globalnativeDOF.size(); rank++)
  {
    for ( const int nativeDOF : globalnativeDOF[rank])
    {
      BOOST_CHECK(uniqueNativeDOF.find(nativeDOF) == uniqueNativeDOF.end());
      uniqueNativeDOF.insert(nativeDOF);
    }
  }

  // check that the unique count adds up to the total DOF count
  printf("global number of dof = %d, unique = %lu\n",qfld_global.nDOF(),uniqueNativeDOF.size());
  BOOST_CHECK_EQUAL(qfld_global.nDOF(), uniqueNativeDOF.size());


  // check that local2nativeDOFmap is identity on a single processor
  if (world.size() == 1)
    for (int idof = 0; idof < qfld_local.nDOF(); idof++)
      BOOST_CHECK_EQUAL(idof, qfld_local.local2nativeDOFmap(idof));

  for (int idof = 0; idof < qfld_global.nDOF(); idof++)
    BOOST_CHECK_EQUAL(idof, qfld_global.local2nativeDOFmap(idof));


  int group = 0;

  // local and global cell groups
  const QFieldSpacetimeClass& qfldCellGroup_local = qfld_local.getCellGroup<Pentatope>(group);
  const QFieldSpacetimeClass& qfldCellGroup_global = qfld_global.getCellGroup<Pentatope>(group);

  int nBasis = qfldCellGroup_local.basis()->nBasis();
  std::vector<int> map_local(nBasis);
  std::vector<int> map_global(nBasis);

  const std::vector<int>& groupCellID = xfld_local.cellIDs(group);

  world.barrier();
  for (int irank = 0; irank < world.size(); irank++)
  {
    std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
    world.barrier();
    if (irank != world.rank() ) continue;

    // collect the DOF indexing on the current rank in order to construct
    // the global continuous mapping
    for (int elem_local = 0; elem_local < qfldCellGroup_local.nElem(); elem_local++)
    {
      int elem_global = groupCellID[elem_local];
      qfldCellGroup_local.associativity(elem_local).getGlobalMapping(map_local.data(), map_local.size());
      qfldCellGroup_global.associativity(elem_global).getGlobalMapping(map_global.data(), map_global.size());

#if 0
      int elemRank = qfldCellGroup_local.associativity(elem_local).rank();
      std::cout << "rank " << world.rank() << " elemRank = " << elemRank
                << " elem_global = " << elem_global << " map_global = " << map_global << " l2n = ";
      for (int n = 0; n < nBasis; n++)
        std::cout << qfld_local.local2nativeDOFmap(map_local[n]) << " ";
      std::cout << std::endl;
#endif
      for (int n = 0; n < nBasis; n++)
        BOOST_CHECK_EQUAL(map_global[n], qfld_local.local2nativeDOFmap(map_local[n]));
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedContinuousIndexing )
{
  typedef Real ArrayQ;
  typedef Field_DG_Cell< PhysD4, TopoD4, ArrayQ > QField4D_DG_Spacetime;
  typedef QField4D_DG_Spacetime::FieldCellGroupType<Pentatope> QFieldSpacetimeClass;

  // global communicator
  mpi::communicator world;

  int N = 5;

  // Generate a partitioned grid
  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld(world, {N,N,N,N});

  int order = 1;
  QField4D_DG_Spacetime qfld(xfld, order, BasisFunctionCategory_Lagrange);

  int group = 0;

  // local cell group
  const QFieldSpacetimeClass& qfldCellGroup = qfld.getCellGroup<Pentatope>(group);
  int nBasis = qfldCellGroup.basis()->nBasis();
  std::vector<int> map(nBasis);

  const std::vector<int>& groupCellID = xfld.cellIDs(group);

  const GlobalContinuousMap& continuousGlobalMap = qfld.continuousGlobalMap();

  std::map<int,int> cellID2local;
  int nElemPossessed = 0;

  world.barrier();
  for (int irank = 0; irank < world.size(); irank++)
  {
    std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
    world.barrier();
    if (irank != world.rank() ) continue;

    // collect the DOF indexing on the current rank in order to construct
    // the global continuous mapping
    for (int elem = 0; elem < qfldCellGroup.nElem(); elem++)
    {
      if (qfldCellGroup.associativity(elem).rank() == world.rank())
      {
        //std::cout << irank << " : ID " << cellID2local[groupCellID[elem]] << " : " << " elem " << elem << " = " << map << std::endl;
        cellID2local[groupCellID[elem]] = elem;
        nElemPossessed++;
      }
    }
  }
  world.barrier();


  // send the DOF index to all other ranks
  std::vector<int> nElemOnRank(world.size());
  std::vector<std::map<int,int>> globalCellID2local(world.size());
#ifdef SANS_MPI
  boost::mpi::all_gather(world, nElemPossessed, nElemOnRank);
  boost::mpi::all_gather(world, cellID2local, globalCellID2local);
#else
  nElemOnRank[0] = nElemPossessed;
  globalCellID2local[0] = cellID2local;
#endif

  // compute the DOF rank offset on all ranks
  std::vector<int> nDOF_rank_offset(world.size(), 0);
  for (int rank = 1; rank < world.size(); rank++)
    nDOF_rank_offset[rank] = nDOF_rank_offset[rank-1] + nBasis*nElemOnRank[rank-1];

  // construct a continuous indexing across all processors
  std::vector<std::vector<std::vector<int>>> continuousDOFindx(world.size());
  int continuousIdx = 0;
  for (int rank = 0; rank < world.size(); rank++)
  {
    continuousDOFindx[rank].resize(nElemOnRank[rank]);
    for (std::size_t elem = 0; elem < continuousDOFindx[rank].size(); elem++)
    {
      continuousDOFindx[rank][elem].resize(nBasis);
      for (int n = 0; n < nBasis; n++)
        continuousDOFindx[rank][elem][n] = continuousIdx++;
    }
  }

  // check that the nDOF rank offset is correct
  BOOST_CHECK_EQUAL( nDOF_rank_offset[world.rank()], continuousGlobalMap.nDOF_rank_offset );

  int nElemZombie = 0;
  world.barrier();
  for (int irank = 0; irank < world.size(); irank++)
  {
    std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
    world.barrier();
    if (irank != world.rank() ) continue;

    for (int elem = 0; elem < qfldCellGroup.nElem(); elem++)
    {
      qfldCellGroup.associativity(elem).getGlobalMapping(map.data(), map.size());

      int rank   = qfldCellGroup.associativity(elem).rank();
      int cellID = groupCellID[elem];

      // Zombies are not included as part of the continuous map
      if (map[0] >= qfld.nDOFpossessed() + qfld.nDOFghost())
      {
        nElemZombie++;
        continue;
      }

      // the local DOF indexing should stride with the nBasis and local element number (skipping over Zombie elements)
      for (int i = 0; i < nBasis; i++)
        BOOST_CHECK_EQUAL( (elem-nElemZombie)*nBasis + i, map[i] );

      if (rank == world.rank())
      {
        // check that the global continuous indexing is correct for elements possessed by this processor
        for (int i = 0; i < nBasis; i++)
          BOOST_CHECK_EQUAL( continuousDOFindx[rank][elem][i], map[i] + continuousGlobalMap.nDOF_rank_offset );
      }
      else
      {
        int remoteElem = globalCellID2local[rank].at(cellID);

#if 0
        std::cout << world.rank() << " " << rank << " : cellID " << cellID << " remoteElem " << remoteElem
                  << " = " << continuousDOFindx[rank][remoteElem] <<  " : ";
        for (int i = 0; i < nBasis; i++)
        {
          int localGhostIndex = map[i] - continuousGlobalMap.nDOFpossessed;
          int globalIndex = continuousGlobalMap.remoteGhostIndex[localGhostIndex];
          std::cout << globalIndex << " ";
        }
        std::cout << " : ";
#endif

        for (int i = 0; i < nBasis; i++)
        {
          int localGhostIndex = map[i] - continuousGlobalMap.nDOFpossessed;
          int nativeIndex = continuousGlobalMap.remoteGhostIndex[localGhostIndex];

          BOOST_CHECK_EQUAL( continuousDOFindx[rank][remoteElem][i], nativeIndex );
        }
      }
      std::cout << std::flush;
    }
  }
  world.barrier();

  //output_Tecplot(xfld, "tmp/test" + std::to_string(world.rank()) + ".dat");
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedSyncDOF )
{
  typedef Real ArrayQ;
  typedef Field_DG_Cell< PhysD4, TopoD4, ArrayQ > QField4D_DG_Spacetime;
  typedef QField4D_DG_Spacetime::FieldCellGroupType<Pentatope> QFieldSpacetimeClass;

  // global communicator
  mpi::communicator world;

  int N = 5;

  // Generate a partitioned grid
  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld(world, {N,N,N,N});

  int order = 1;
  QField4D_DG_Spacetime qfld(xfld, order, BasisFunctionCategory_Lagrange);

  int group = 0;

  // local cell group
  const QFieldSpacetimeClass& qfldCellGroup = qfld.getCellGroup<Pentatope>(group);
  int nBasis = qfldCellGroup.basis()->nBasis();
  std::vector<int> map(nBasis);

  const std::vector<int>& groupCellID = xfld.cellIDs(group);


  std::map<int,int> cellID2local;
  int nElemPossessed = 0;

  world.barrier();
  for (int irank = 0; irank < world.size(); irank++)
  {
    std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
    world.barrier();
    if (irank != world.rank() ) continue;

    // collect the DOF indexing on the current rank in order to construct
    // the global continuous mapping
    for (int elem = 0; elem < qfldCellGroup.nElem(); elem++)
    {
      if (qfldCellGroup.associativity(elem).rank() == world.rank())
      {
        //std::cout << irank << " : ID " << cellID2local[groupCellID[elem]] << " : " << " elem " << elem << " = " << map << std::endl;
        cellID2local[groupCellID[elem]] = elem;
        nElemPossessed++;
      }
    }
  }
  world.barrier();

  // send the DOF index to all other ranks
  std::vector<int> nElemOnRank(world.size());
  std::vector<std::map<int,int>> globalCellID2local(world.size());
#ifdef SANS_MPI
  boost::mpi::all_gather(world, nElemPossessed , nElemOnRank);
  boost::mpi::all_gather(world, cellID2local, globalCellID2local);
#else
  nElemOnRank[0]  = nElemPossessed;
  globalCellID2local[0] = cellID2local;
#endif

  // construct a continuous indexing across all processors
  std::vector<std::vector<std::vector<int>>> continuousDOFindx(world.size());
  int continuousIdx = 0;
  for (int rank = 0; rank < world.size(); rank++)
  {
    continuousDOFindx[rank].resize(nElemOnRank[rank]);
    for (std::size_t elem = 0; elem < continuousDOFindx[rank].size(); elem++)
    {
      continuousDOFindx[rank][elem].resize(nBasis);
      for (int n = 0; n < nBasis; n++)
        continuousDOFindx[rank][elem][n] = continuousIdx++;
    }
  }

  // set all DOFs to -1
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = -1;

  for (int elem = 0; elem < qfldCellGroup.nElem(); elem++)
  {
    qfldCellGroup.associativity(elem).getGlobalMapping(map.data(), map.size());

    int rank = qfldCellGroup.associativity(elem).rank();

    if (rank == world.rank())
    {
      // assign the continuous index to possessed DOFs
      for (int i = 0; i < nBasis; i++)
        qfld.DOF(map[i]) = continuousDOFindx[rank][elem][i];
    }
  }

  // synchronize the DOFs. all ghosts and zombies should now get the continuous index
  qfld.syncDOFs_MPI_noCache();

  world.barrier();
  for (int irank = 0; irank < world.size(); irank++)
  {
    std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
    world.barrier();
    if (irank != world.rank() ) continue;

    for (int elem = 0; elem < qfldCellGroup.nElem(); elem++)
    {
      qfldCellGroup.associativity(elem).getGlobalMapping(map.data(), map.size());

      int rank = qfldCellGroup.associativity(elem).rank();

      if (rank == world.rank())
      {
        // check that the global indexing has not been modified
        for (int i = 0; i < nBasis; i++)
          BOOST_CHECK_EQUAL( qfld.DOF(map[i]), continuousDOFindx[rank][elem][i] );
      }
      else
      {
        int cellID = groupCellID[elem];
        int remoteElem = globalCellID2local[rank].at(cellID);

        for (int i = 0; i < nBasis; i++)
          BOOST_CHECK_EQUAL( qfld.DOF(map[i]), continuousDOFindx[rank][remoteElem][i] );
      }
      std::cout << std::flush;
    }
  }
  world.barrier();

  //output_Tecplot(xfld, "tmp/test" + std::to_string(world.rank()) + ".dat");
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Exception )
{
  typedef DLA::VectorS<4, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD4, TopoD4, ArrayQ > QField4D_DG_Spacetime;
  typedef Field_DG_InteriorTrace< PhysD4, TopoD4, ArrayQ > QField4D_DG_InteriorTet;
  typedef Field_DG_BoundaryTrace< PhysD4, TopoD4, ArrayQ > QField4D_DG_BoundaryTet;

  mpi::communicator comm;
  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld1(comm,{2,2,2,2});

  int order = 999; //This should always be an order that is not available

  BOOST_CHECK_THROW( QField4D_DG_Spacetime qfld1(xfld1, order, BasisFunctionCategory_Lagrange), DeveloperException );
  BOOST_CHECK_THROW( QField4D_DG_Spacetime qfld1(xfld1, order, BasisFunctionCategory_Lagrange), DeveloperException );
  BOOST_CHECK_THROW( QField4D_DG_InteriorTet qfld1(xfld1, order, BasisFunctionCategory_Lagrange), DeveloperException );
  BOOST_CHECK_THROW( QField4D_DG_BoundaryTet qfld1(xfld1, order, BasisFunctionCategory_Lagrange), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  typedef DLA::VectorS<4, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD4, TopoD4, ArrayQ > QField4D_DG_Spacetime;
  typedef Field_DG_InteriorTrace< PhysD4, TopoD4, ArrayQ > QField4D_DG_InteriorTet;
  typedef Field_DG_BoundaryTrace< PhysD4, TopoD4, ArrayQ > QField4D_DG_BoundaryTet;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/Field4D_DG_Pentatope_pattern.txt", true );

  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());
  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld1(comm,{2,2,2,2});

  QField4D_DG_Spacetime qfld1(xfld1, 1, BasisFunctionCategory_Lagrange);
  qfld1 = 0;
  qfld1.dump( 2, output );

  QField4D_DG_InteriorTet qfld2(xfld1, 2, BasisFunctionCategory_Lagrange);
  qfld2 = 0;
  qfld2.dump( 2, output );

  QField4D_DG_BoundaryTet qfld3(xfld1, 2, BasisFunctionCategory_Lagrange);
  qfld3 = 0;
  qfld3.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
