// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField2D_Triangle_PhysD3_btest
// testing of XField2D with triangles with PhysD3
//
// Note: unit grids tested in "UnitGrids"

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real

#include "Field/XFieldVolume.h"
#include "unit/UnitGrids/XField3D_Sphere_Triangle_X1.h"

#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( XField2D_Triangle_PhysD3_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK( (XField<PhysD3,TopoD2>::D == 3) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  XField<PhysD3,TopoD2> xfld1;

  BOOST_CHECK_EQUAL( 0, xfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, xfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, xfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, xfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, xfld1.nCellGroups() );
}

// TODO:: Not sure how to compute a signed jacobian on a triangle in 3D...
#if 0
class XField2D_NegativeJacobian : public XField<PhysD3,TopoD2>
{
public:
  XField2D_NegativeJacobian()
  {
    //Create the DOF arrays
    resizeDOF(3);

    //Create the element groups
    resizeInteriorTraceGroups(0);
    resizeBoundaryTraceGroups(3);
    resizeCellGroups(1);

    // nodal coordinates for the triangle.
    DOF(0) = {0, 0, 0};
    DOF(1) = {1, 0, 1};
    DOF(2) = {0, -1, 0.5};  //<---- Should be {0, 1, 0.5} for positive jacobian

    // area field variable
    FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 1 );

    //set processor rank
    fldAssocCell.setAssociativity( 0 ).setRank( 0 );

    //element area associativity
    fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1, 2} );

    // edge signs for elements (L is +, R is -)
    fldAssocCell.setAssociativity( 0 ).setEdgeSign( +1, 0 );


    FieldCellGroupType<Triangle>* xfldElementGroup = NULL;
    cellGroups_[0] = xfldElementGroup = new FieldCellGroupType<Triangle>( fldAssocCell );

    xfldElementGroup->setDOF(DOF_, nDOF_);

    nElem_ = 1;

    // interior-edge field variable

    // none

    // boundary-edge field variable

    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge0( BasisFunctionLineBase::HierarchicalP1, 1 );
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge1( BasisFunctionLineBase::HierarchicalP1, 1 );
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge2( BasisFunctionLineBase::HierarchicalP1, 1 );

    //set processor rank
    fldAssocBedge0.setAssociativity( 0 ).setRank( 0 );
    fldAssocBedge1.setAssociativity( 0 ).setRank( 0 );
    fldAssocBedge2.setAssociativity( 0 ).setRank( 0 );

    // edge-element associativity
    fldAssocBedge0.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );
    fldAssocBedge1.setAssociativity( 0 ).setNodeGlobalMapping( {1, 2} );
    fldAssocBedge2.setAssociativity( 0 ).setNodeGlobalMapping( {2, 0} );

    // edge-to-cell connectivity
    fldAssocBedge0.setGroupLeft( 0 );
    fldAssocBedge1.setGroupLeft( 0 );
    fldAssocBedge2.setGroupLeft( 0 );
    fldAssocBedge0.setElementLeft( 0, 0 );
    fldAssocBedge1.setElementLeft( 0, 0 );
    fldAssocBedge2.setElementLeft( 0, 0 );
    fldAssocBedge0.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 0 );
    fldAssocBedge1.setCanonicalTraceLeft( CanonicalTraceToCell(0, 1), 0 );
    fldAssocBedge2.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 0 );

    boundaryTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocBedge0 );
    boundaryTraceGroups_[1] = new FieldTraceGroupType<Line>( fldAssocBedge1 );
    boundaryTraceGroups_[2] = new FieldTraceGroupType<Line>( fldAssocBedge2 );

    boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
    boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);
    boundaryTraceGroups_[2]->setDOF(DOF_, nDOF_);

    checkGrid();
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( NegativeJacobian )
{
  BOOST_CHECK_THROW( XField2D_NegativeJacobian xfld, XFieldException );
}
#endif

class XField2D_WrongBoundaryNodeMap : public XField<PhysD3,TopoD2>
{
public:
  XField2D_WrongBoundaryNodeMap()
  {
    //Create the DOF arrays
    resizeDOF(3);

    //Create the element groups
    resizeInteriorTraceGroups(0);
    resizeBoundaryTraceGroups(3);
    resizeCellGroups(1);

    // nodal coordinates for the triangle.
    DOF(0) = {0, 0, 0};
    DOF(1) = {1, 0, 1};
    DOF(2) = {0, 1, 0.5};

    // area field variable
    FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 1 );

    //set processor rank
    fldAssocCell.setAssociativity( 0 ).setRank( 0 );

    //element area associativity
    fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1, 2} );

    // edge signs for elements (L is +, R is -)
    fldAssocCell.setAssociativity( 0 ).setEdgeSign( +1, 0 );


    FieldCellGroupType<Triangle>* xfldElementGroup = NULL;
    cellGroups_[0] = xfldElementGroup = new FieldCellGroupType<Triangle>( fldAssocCell );

    xfldElementGroup->setDOF(DOF_, nDOF_);

    nElem_ = 1;

    // interior-edge field variable

    // none

    // boundary-edge field variable

    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge0( BasisFunctionLineBase::HierarchicalP1, 1 );
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge1( BasisFunctionLineBase::HierarchicalP1, 1 );
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge2( BasisFunctionLineBase::HierarchicalP1, 1 );

    //set processor rank
    fldAssocBedge0.setAssociativity( 0 ).setRank( 0 );
    fldAssocBedge1.setAssociativity( 0 ).setRank( 0 );
    fldAssocBedge2.setAssociativity( 0 ).setRank( 0 );

    // edge-element associativity
    fldAssocBedge0.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );
    fldAssocBedge1.setAssociativity( 0 ).setNodeGlobalMapping( {1, 2} );
    fldAssocBedge2.setAssociativity( 0 ).setNodeGlobalMapping( {0, 2} ); //<---- Should be {2, 0}

    // edge-to-cell connectivity
    fldAssocBedge0.setGroupLeft( 0 );
    fldAssocBedge1.setGroupLeft( 0 );
    fldAssocBedge2.setGroupLeft( 0 );
    fldAssocBedge0.setElementLeft( 0, 0 );
    fldAssocBedge1.setElementLeft( 0, 0 );
    fldAssocBedge2.setElementLeft( 0, 0 );
    fldAssocBedge0.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 0 );
    fldAssocBedge1.setCanonicalTraceLeft( CanonicalTraceToCell(0, 1), 0 );
    fldAssocBedge2.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 0 );

    boundaryTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocBedge0 );
    boundaryTraceGroups_[1] = new FieldTraceGroupType<Line>( fldAssocBedge1 );
    boundaryTraceGroups_[2] = new FieldTraceGroupType<Line>( fldAssocBedge2 );

    boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
    boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);
    boundaryTraceGroups_[2]->setDOF(DOF_, nDOF_);

    checkGrid();
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WrongBoundaryNodeMap )
{
  BOOST_CHECK_THROW( XField2D_WrongBoundaryNodeMap xfld;, XFieldException );
}

class XField2D_WrongBoundaryCanonicalEdge : public XField<PhysD3,TopoD2>
{
public:
  XField2D_WrongBoundaryCanonicalEdge()
  {
    //Create the DOF arrays
    resizeDOF(3);

    //Create the element groups
    resizeInteriorTraceGroups(0);
    resizeBoundaryTraceGroups(3);
    resizeCellGroups(1);

    // nodal coordinates for the triangle.
    DOF(0) = {0, 0, 0};
    DOF(1) = {1, 0, 1};
    DOF(2) = {0, 1, 0.5};

    // area field variable
    FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 1 );

    //set processor rank
    fldAssocCell.setAssociativity( 0 ).setRank( 0 );

    //element area associativity
    fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1, 2} );

    // edge signs for elements (L is +, R is -)
    fldAssocCell.setAssociativity( 0 ).setEdgeSign( +1, 0 );


    FieldCellGroupType<Triangle>* xfldElementGroup = NULL;
    cellGroups_[0] = xfldElementGroup = new FieldCellGroupType<Triangle>( fldAssocCell );

    xfldElementGroup->setDOF(DOF_, nDOF_);

    nElem_ = 1;

    // interior-edge field variable

    // none

    // boundary-edge field variable

    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge0( BasisFunctionLineBase::HierarchicalP1, 1 );
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge1( BasisFunctionLineBase::HierarchicalP1, 1 );
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge2( BasisFunctionLineBase::HierarchicalP1, 1 );

    //set processor rank
    fldAssocBedge0.setAssociativity( 0 ).setRank( 0 );
    fldAssocBedge1.setAssociativity( 0 ).setRank( 0 );
    fldAssocBedge2.setAssociativity( 0 ).setRank( 0 );

    // edge-element associativity
    fldAssocBedge0.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );
    fldAssocBedge1.setAssociativity( 0 ).setNodeGlobalMapping( {1, 2} );
    fldAssocBedge2.setAssociativity( 0 ).setNodeGlobalMapping( {2, 0} );

    // edge-to-cell connectivity
    fldAssocBedge0.setGroupLeft( 0 );
    fldAssocBedge1.setGroupLeft( 0 );
    fldAssocBedge2.setGroupLeft( 0 );
    fldAssocBedge0.setElementLeft( 0, 0 );
    fldAssocBedge1.setElementLeft( 0, 0 );
    fldAssocBedge2.setElementLeft( 0, 0 );
    fldAssocBedge0.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 0 );
    fldAssocBedge1.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 0 ); //<---- Should be (0, 1), 0 );
    fldAssocBedge2.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 0 );

    boundaryTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocBedge0 );
    boundaryTraceGroups_[1] = new FieldTraceGroupType<Line>( fldAssocBedge1 );
    boundaryTraceGroups_[2] = new FieldTraceGroupType<Line>( fldAssocBedge2 );

    boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
    boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);
    boundaryTraceGroups_[2]->setDOF(DOF_, nDOF_);

    checkGrid();
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WrongBoundaryCanonicalEdge )
{
  BOOST_CHECK_THROW( XField2D_WrongBoundaryCanonicalEdge xfld;, XFieldException );
}

class XField2D_WrongInteriorNodeMap : public XField<PhysD3,TopoD2>
{
public:
  XField2D_WrongInteriorNodeMap()
  {
    //Create the DOF arrays
    resizeDOF(4);

    //Create the element groups
    resizeInteriorTraceGroups(1);
    resizeBoundaryTraceGroups(1);
    resizeCellGroups(1);

    // nodal coordinates for the triangle.
    DOF(0) = {0, 0, 0};
    DOF(1) = {1, 0, 1};
    DOF(2) = {0, 1, 0.5};
    DOF(3) = {1, 1, -0.5};

    // area field variable
    FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 2 );

    //set processor rank
    fldAssocCell.setAssociativity( 0 ).setRank( 0 );
    fldAssocCell.setAssociativity( 1 ).setRank( 0 );

    //element area associativity
    fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1, 2} );
    fldAssocCell.setAssociativity( 1 ).setNodeGlobalMapping( {3, 2, 1} );

    // edge signs for elements (L is +, R is -)
    fldAssocCell.setAssociativity( 0 ).setEdgeSign( +1, 0 );
    fldAssocCell.setAssociativity( 1 ).setEdgeSign( -1, 0 );

    FieldCellGroupType<Triangle>* xfldElementGroup = NULL;
    cellGroups_[0] = xfldElementGroup = new FieldCellGroupType<Triangle>( fldAssocCell );

    xfldElementGroup->setDOF(DOF_, nDOF_);

    nElem_ = 2;

    // interior-edge field variable

    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, 1 );

    //set processor rank
    fldAssocIedge.setAssociativity( 0 ).setRank( 0 );

    // edge-element associativity
    fldAssocIedge.setAssociativity( 0 ).setNodeGlobalMapping( {2, 1} ); //<---- Should be {1, 2}

    // edge-to-cell connectivity
    fldAssocIedge.setGroupLeft( 0 );
    fldAssocIedge.setGroupRight( 0 );
    fldAssocIedge.setElementLeft( 0, 0 );
    fldAssocIedge.setElementRight( 1, 0 );
    fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), 0 );
    fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), 0 );

    FieldTraceGroupType<Line>* xfldIedge = NULL;
    interiorTraceGroups_[0] = xfldIedge = new FieldTraceGroupType<Line>( fldAssocIedge );

    xfldIedge->setDOF(DOF_, nDOF_);

    // boundary-edge field variable

    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, 4 );

    //set processor rank
    fldAssocBedge.setAssociativity( 0 ).setRank( 0 );
    fldAssocBedge.setAssociativity( 1 ).setRank( 0 );
    fldAssocBedge.setAssociativity( 2 ).setRank( 0 );
    fldAssocBedge.setAssociativity( 3 ).setRank( 0 );

    // edge-element associativity
    fldAssocBedge.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );
    fldAssocBedge.setAssociativity( 1 ).setNodeGlobalMapping( {1, 3} );
    fldAssocBedge.setAssociativity( 2 ).setNodeGlobalMapping( {3, 2} );
    fldAssocBedge.setAssociativity( 3 ).setNodeGlobalMapping( {2, 0} );

    // edge-to-cell connectivity
    fldAssocBedge.setGroupLeft( 0 );
    fldAssocBedge.setElementLeft( 0, 0 );
    fldAssocBedge.setElementLeft( 1, 1 );
    fldAssocBedge.setElementLeft( 1, 2 );
    fldAssocBedge.setElementLeft( 0, 3 );
    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 0 );
    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 1 );
    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 2 );
    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 3 );

    FieldTraceGroupType<Line>* xfldBedge = NULL;
    boundaryTraceGroups_[0] = xfldBedge = new FieldTraceGroupType<Line>( fldAssocBedge );

    xfldBedge->setDOF(DOF_, nDOF_);

    checkGrid();
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WrongInteriorNodeMap )
{
  BOOST_CHECK_THROW( XField2D_WrongInteriorNodeMap xfld;, XFieldException );
}

//  2 ----- 3
//  |\      |
//  | \ (1) |
//  |  \    |
//  |   \   |
//  |    \  |
//  | (0) \ |
//  |      \|
//  0 ----- 1

class XField2D_WrongInteriorCanonicalEdge : public XField<PhysD3,TopoD2>
{
public:
  XField2D_WrongInteriorCanonicalEdge()
  {
    //Create the DOF arrays
    resizeDOF(4);

    //Create the element groups
    resizeInteriorTraceGroups(1);
    resizeBoundaryTraceGroups(1);
    resizeCellGroups(1);

    // nodal coordinates for the two triangles.
    DOF(0) = {0, 0, 0};
    DOF(1) = {1, 0, 1};
    DOF(2) = {0, 1, 0.5};
    DOF(3) = {1, 1, -0.5};

    // area field variable
    FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 2 );

    //set processor rank
    fldAssocCell.setAssociativity( 0 ).setRank( 0 );
    fldAssocCell.setAssociativity( 1 ).setRank( 0 );

    //element area associativity
    fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1, 2} );
    fldAssocCell.setAssociativity( 1 ).setNodeGlobalMapping( {3, 2, 1} );

    // edge signs for elements (L is +, R is -)
    fldAssocCell.setAssociativity( 0 ).setEdgeSign( +1, 0 );
    fldAssocCell.setAssociativity( 1 ).setEdgeSign( -1, 0 );

    FieldCellGroupType<Triangle>* xfldElementGroup = NULL;
    cellGroups_[0] = xfldElementGroup = new FieldCellGroupType<Triangle>( fldAssocCell );

    xfldElementGroup->setDOF(DOF_, nDOF_);

    nElem_ = 2;

    // interior-edge field variable

    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, 1 );

    //set processor rank
    fldAssocIedge.setAssociativity( 0 ).setRank( 0 );

    // edge-element associativity
    fldAssocIedge.setAssociativity( 0 ).setNodeGlobalMapping( {1, 2} );

    // edge-to-cell connectivity
    fldAssocIedge.setGroupLeft( 0 );
    fldAssocIedge.setGroupRight( 0 );
    fldAssocIedge.setElementLeft( 0, 0 );
    fldAssocIedge.setElementRight( 1, 0 );
    fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), 0 );
    fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), 0 ); //<---- Should be (0, -1), 0)

    FieldTraceGroupType<Line>* xfldIedge = NULL;
    interiorTraceGroups_[0] = xfldIedge = new FieldTraceGroupType<Line>( fldAssocIedge );

    xfldIedge->setDOF(DOF_, nDOF_);

    // boundary-edge field variable

    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, 4 );

    //set processor rank
    fldAssocBedge.setAssociativity( 0 ).setRank( 0 );
    fldAssocBedge.setAssociativity( 1 ).setRank( 0 );
    fldAssocBedge.setAssociativity( 2 ).setRank( 0 );
    fldAssocBedge.setAssociativity( 3 ).setRank( 0 );

    // edge-element associativity
    fldAssocBedge.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );
    fldAssocBedge.setAssociativity( 1 ).setNodeGlobalMapping( {1, 3} );
    fldAssocBedge.setAssociativity( 2 ).setNodeGlobalMapping( {3, 2} );
    fldAssocBedge.setAssociativity( 3 ).setNodeGlobalMapping( {2, 0} );

    // edge-to-cell connectivity
    fldAssocBedge.setGroupLeft( 0 );
    fldAssocBedge.setElementLeft( 0, 0 );
    fldAssocBedge.setElementLeft( 1, 1 );
    fldAssocBedge.setElementLeft( 1, 2 );
    fldAssocBedge.setElementLeft( 0, 3 );
    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 0 );
    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 1 );
    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 2 );
    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 3 );

    FieldTraceGroupType<Line>* xfldBedge = NULL;
    boundaryTraceGroups_[0] = xfldBedge = new FieldTraceGroupType<Line>( fldAssocBedge );

    xfldBedge->setDOF(DOF_, nDOF_);

    checkGrid();
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WrongInteriorCanonicalEdge )
{
  BOOST_CHECK_THROW( XField2D_WrongInteriorCanonicalEdge xfld;, XFieldException );
}

//  2 ----- 3
//  |\      |
//  | \ (1) |
//  |  \    |
//  |   \   |
//  |    \  |
//  | (0) \ |
//  |      \|
//  0 ----- 1

class XField2D_WrongBoundaryTraceConnectedNodeMap : public XField<PhysD3,TopoD2>
{
public:
  XField2D_WrongBoundaryTraceConnectedNodeMap()
  {
    //Create the DOF arrays
    resizeDOF(4);

    //Create the element groups
    resizeInteriorTraceGroups(0); // No interior group
    resizeBoundaryTraceGroups(2);
    resizeCellGroups(1);

    // nodal coordinates for the two triangles.
    DOF(0) = {0, 0, 0};
    DOF(1) = {1, 0, 1};
    DOF(2) = {0, 1, 0.5};
    DOF(3) = {1, 1, -0.5};

    // area field variable
    FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 2 );

    //set processor rank
    fldAssocCell.setAssociativity( 0 ).setRank( 0 );
    fldAssocCell.setAssociativity( 1 ).setRank( 0 );

    //element area associativity
    fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1, 2} );
    fldAssocCell.setAssociativity( 1 ).setNodeGlobalMapping( {3, 2, 1} );

    // edge signs for elements (L is +, R is -)
    fldAssocCell.setAssociativity( 0 ).setEdgeSign( +1, 0 );
    fldAssocCell.setAssociativity( 1 ).setEdgeSign( -1, 0 );

    FieldCellGroupType<Triangle>* xfldElementGroup = NULL;
    cellGroups_[0] = xfldElementGroup = new FieldCellGroupType<Triangle>( fldAssocCell );

    xfldElementGroup->setDOF(DOF_, nDOF_);

    nElem_ = 2;

    // interior-edge field variable

    // boundary-edge field variable

    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, 4 );

    //set processor rank
    fldAssocBedge.setAssociativity( 0 ).setRank( 0 );
    fldAssocBedge.setAssociativity( 1 ).setRank( 0 );
    fldAssocBedge.setAssociativity( 2 ).setRank( 0 );
    fldAssocBedge.setAssociativity( 3 ).setRank( 0 );

    // edge-element associativity
    fldAssocBedge.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );
    fldAssocBedge.setAssociativity( 1 ).setNodeGlobalMapping( {1, 3} );
    fldAssocBedge.setAssociativity( 2 ).setNodeGlobalMapping( {3, 2} );
    fldAssocBedge.setAssociativity( 3 ).setNodeGlobalMapping( {2, 0} );

    // edge-to-cell connectivity
    fldAssocBedge.setGroupLeft( 0 );
    fldAssocBedge.setElementLeft( 0, 0 );
    fldAssocBedge.setElementLeft( 1, 1 );
    fldAssocBedge.setElementLeft( 1, 2 );
    fldAssocBedge.setElementLeft( 0, 3 );
    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 0 );
    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 1 );
    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 2 );
    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 3 );

    boundaryTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocBedge );
    boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);

    // connected boundary trace

    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCBedge( BasisFunctionLineBase::HierarchicalP1, 1 );

    fldAssocCBedge.setAssociativity( 0 ).setRank( 0 );

    // edge-element associativity
    fldAssocCBedge.setAssociativity( 0 ).setNodeGlobalMapping( {2, 1} ); //<---- Should be {1, 2}

    // edge-to-cell connectivity
    fldAssocCBedge.setGroupRight( 0 );
    fldAssocCBedge.setGroupLeft( 0 );
    fldAssocCBedge.setElementLeft( 0, 0 );
    fldAssocCBedge.setElementRight( 1, 0 );
    fldAssocCBedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), 0 );
    fldAssocCBedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), 0 );

    boundaryTraceGroups_[1] = new FieldTraceGroupType<Line>( fldAssocCBedge );
    boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);

    checkGrid();
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WrongBoundaryTraceConnectedNodeMap )
{
  BOOST_CHECK_THROW( XField2D_WrongBoundaryTraceConnectedNodeMap xfld;, XFieldException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/XField3D_Sphere_Triangle_pattern.txt", true );

  XField3D_Sphere_Triangle_X1 xfld(3, 3, 13., 167., 11., 2*PI-11.);

  xfld.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
