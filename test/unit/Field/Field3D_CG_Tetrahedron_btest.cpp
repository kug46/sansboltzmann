// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field3D_CG_btest
// testing of Field3D_CG_* classes
//

//#define SLEEP_FOR_OUTPUT
#ifdef SLEEP_FOR_OUTPUT
#define SLEEP_MILLISECONDS 500
#include <chrono>
#include <thread>
#endif

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;


#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"
#include "unit/UnitGrids/XField3D_5Tet_X1_1Group_AllOrientations.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/output_std_vector.h"

#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/TraceToCellRefCoord.h"
#include "Field/FieldVolume_CG_Cell.h"
//#include "Field/Field_CG_InteriorTrace.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/Field_CG/Field_CG_Topology.h"

#ifdef SANS_MPI
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/mpi/collectives/all_gather.hpp>
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
template class Field_CG_Cell< PhysD3, TopoD3, Real >;
//template class Field_CG_InteriorTrace< PhysD3, TopoD3, Real >;
template class Field_CG_BoundaryTrace< PhysD3, TopoD3, Real >;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( Field3D_CG_Tetrahedron_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_Volume_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_CG_Volume;
  typedef XField3D_2Tet_X1_1Group::FieldCellGroupType<Tet> XFieldVolumeClass;
  typedef QField3D_CG_Volume::FieldCellGroupType<Tet> QFieldVolumeClass;
  typedef QFieldVolumeClass::ElementType<> ElementQFieldClass;
  typedef std::array<int,4> Int4;

  int nodeMap[4];

  XField3D_2Tet_X1_1Group xfld1;

  const XFieldVolumeClass& xfldGroup1 = xfld1.getCellGroup<Tet>(0);

  int order = 1;
  QField3D_CG_Volume qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 5, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 6, qfld1.nBoundaryTraceGroups() ); // These are just views of the cell dofs TODO: check they are
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldVolumeClass& qfldGroup1 = qfld1.getCellGroup<Tet>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup1.nElem() );

  BOOST_CHECK_EQUAL( qfldGroup1.associativity(0).nNode(), 4 );
  BOOST_CHECK_EQUAL( qfldGroup1.associativity(0).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldGroup1.associativity(0).nFace(), 0 );
  BOOST_CHECK_EQUAL( qfldGroup1.associativity(0).nCell(), 0 );

  BOOST_CHECK_EQUAL( qfldGroup1.associativity(1).nNode(), 4 );
  BOOST_CHECK_EQUAL( qfldGroup1.associativity(1).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldGroup1.associativity(1).nFace(), 0 );
  BOOST_CHECK_EQUAL( qfldGroup1.associativity(1).nCell(), 0 );

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );
  BOOST_CHECK_EQUAL( 2, nodeMap[2] );
  BOOST_CHECK_EQUAL( 3, nodeMap[3] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( 4, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0, nodeMap[1] );
  BOOST_CHECK_EQUAL( 2, nodeMap[2] );
  BOOST_CHECK_EQUAL( 3, nodeMap[3] );

  Int4 xfaceSign, qfaceSign;

  xfaceSign = xfldGroup1.associativity(0).faceSign();
  qfaceSign = qfldGroup1.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( xfaceSign[0], qfaceSign[0] );
  BOOST_CHECK_EQUAL( xfaceSign[1], qfaceSign[1] );
  BOOST_CHECK_EQUAL( xfaceSign[2], qfaceSign[2] );
  BOOST_CHECK_EQUAL( xfaceSign[3], qfaceSign[3] );

  xfaceSign = xfldGroup1.associativity(1).faceSign();
  qfaceSign = qfldGroup1.associativity(1).faceSign();
  BOOST_CHECK_EQUAL( xfaceSign[0], qfaceSign[0] );
  BOOST_CHECK_EQUAL( xfaceSign[1], qfaceSign[1] );
  BOOST_CHECK_EQUAL( xfaceSign[2], qfaceSign[2] );
  BOOST_CHECK_EQUAL( xfaceSign[3], qfaceSign[3] );


  XField3D_6Tet_X1_1Group xfld2;

  const XFieldVolumeClass& xfldGroup2 = xfld2.getCellGroup<Tet>(0);

  QField3D_CG_Volume qfld2(xfld2, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 8, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 6, qfld2.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nBoundaryTraceGroups() ); // These are just views of the cell dofs TODO: check they are
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  QField3D_CG_Volume qfld3(qfld2, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld2.nElem()               ,  qfld3.nElem() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  const QFieldVolumeClass& qfldGroup2 = qfld2.getCellGroup<Tet>(0);

  BOOST_CHECK_EQUAL( 6, qfldGroup2.nElem() );
#if 0
  qfldGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );
  BOOST_CHECK_EQUAL( 2, nodeMap[2] );

  qfldGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );
  BOOST_CHECK_EQUAL( 1, nodeMap[2] );

  qfldGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4, nodeMap[1] );
  BOOST_CHECK_EQUAL( 0, nodeMap[2] );

  qfldGroup2.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0, nodeMap[1] );
  BOOST_CHECK_EQUAL( 5, nodeMap[2] );
#endif
  for (int elem = 0; elem < qfldGroup2.nElem(); elem++)
  {
    xfaceSign = xfldGroup2.associativity(elem).faceSign();
    qfaceSign = qfldGroup2.associativity(elem).faceSign();
    BOOST_CHECK_EQUAL( xfaceSign[0], qfaceSign[0] );
    BOOST_CHECK_EQUAL( xfaceSign[1], qfaceSign[1] );
    BOOST_CHECK_EQUAL( xfaceSign[2], qfaceSign[2] );
    BOOST_CHECK_EQUAL( xfaceSign[3], qfaceSign[3] );
  }

  // Test the constant assignment operator
  ElementQFieldClass qfldElem(qfldGroup1.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_Volume_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_CG_Volume;
  typedef XField3D_2Tet_X1_1Group::FieldCellGroupType<Tet> XFieldVolumeClass;
  typedef QField3D_CG_Volume::FieldCellGroupType<Tet> QFieldVolumeClass;
  typedef QFieldVolumeClass::ElementType<> ElementQFieldClass;
  typedef std::array<int,4> Int4;

  int nodeMap[4];
  int edgeMap[6];

  XField3D_2Tet_X1_1Group xfld1;
//  xfld1.getInteriorTraceGroup<Triangle>(0).associativity(0).
  const XFieldVolumeClass& xfldGroup1 = xfld1.getCellGroup<Tet>(0);

  int order = 2;
  QField3D_CG_Volume qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( qfld1.nDOF(), 14 );
  BOOST_CHECK_EQUAL( qfld1.nElem(), 2 );
  BOOST_CHECK_EQUAL( qfld1.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld1.nBoundaryTraceGroups(), 6 );// These are just views of the cell dofs TODO: check they are
  BOOST_CHECK_EQUAL( qfld1.nCellGroups(), 1 );
  BOOST_CHECK_EQUAL( &qfld1.getXField(), &xfld1 );

  const QFieldVolumeClass& qfldGroup1 = qfld1.getCellGroup<Tet>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup1.nElem() );

  BOOST_CHECK_EQUAL( qfldGroup1.associativity(0).nNode(), 4 );
  BOOST_CHECK_EQUAL( qfldGroup1.associativity(0).nEdge(), 6 );
  BOOST_CHECK_EQUAL( qfldGroup1.associativity(0).nFace(), 0 );
  BOOST_CHECK_EQUAL( qfldGroup1.associativity(0).nCell(), 0 );

  BOOST_CHECK_EQUAL( qfldGroup1.associativity(1).nNode(), 4 );
  BOOST_CHECK_EQUAL( qfldGroup1.associativity(1).nEdge(), 6 );
  BOOST_CHECK_EQUAL( qfldGroup1.associativity(1).nFace(), 0 );
  BOOST_CHECK_EQUAL( qfldGroup1.associativity(1).nCell(), 0 );

  //Edge DOFs
  qfldGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( edgeMap[0], 6 );
  BOOST_CHECK_EQUAL( edgeMap[1], 5 );
  BOOST_CHECK_EQUAL( edgeMap[2], 4 );
  BOOST_CHECK_EQUAL( edgeMap[3], 1 );
  BOOST_CHECK_EQUAL( edgeMap[4], 2 );
  BOOST_CHECK_EQUAL( edgeMap[5], 0 );

  qfldGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( edgeMap[0], 6 );
  BOOST_CHECK_EQUAL( edgeMap[1], 2 );
  BOOST_CHECK_EQUAL( edgeMap[2], 1 );
  BOOST_CHECK_EQUAL( edgeMap[3], 7 );
  BOOST_CHECK_EQUAL( edgeMap[4], 8 );
  BOOST_CHECK_EQUAL( edgeMap[5], 3 );

  //Node DOFs
  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 10 );
  BOOST_CHECK_EQUAL( nodeMap[2], 11 );
  BOOST_CHECK_EQUAL( nodeMap[3], 12 );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 13 );
  BOOST_CHECK_EQUAL( nodeMap[1], 9 );
  BOOST_CHECK_EQUAL( nodeMap[2], 11 );
  BOOST_CHECK_EQUAL( nodeMap[3], 12 );

  Int4 xfaceSign, qfaceSign;

  xfaceSign = xfldGroup1.associativity(0).faceSign();
  qfaceSign = qfldGroup1.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( xfaceSign[0], qfaceSign[0] );
  BOOST_CHECK_EQUAL( xfaceSign[1], qfaceSign[1] );
  BOOST_CHECK_EQUAL( xfaceSign[2], qfaceSign[2] );
  BOOST_CHECK_EQUAL( xfaceSign[3], qfaceSign[3] );

  xfaceSign = xfldGroup1.associativity(1).faceSign();
  qfaceSign = qfldGroup1.associativity(1).faceSign();
  BOOST_CHECK_EQUAL( xfaceSign[0], qfaceSign[0] );
  BOOST_CHECK_EQUAL( xfaceSign[1], qfaceSign[1] );
  BOOST_CHECK_EQUAL( xfaceSign[2], qfaceSign[2] );
  BOOST_CHECK_EQUAL( xfaceSign[3], qfaceSign[3] );

#if 0
  XField3D_4Tet_X1_1Group xfld2;

  const XFieldVolumeClass& xfldGroup2 = xfld2.getCellGroup<Tet>(0);

  QField3D_CG_Volume qfld2(xfld2, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 6, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 4, qfld2.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  QField3D_CG_Volume qfld3(qfld2);

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nElem()               ,  qfld3.nElem() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  const QFieldVolumeClass& qfldGroup2 = qfld2.getCellGroup<Tet>(0);

  BOOST_CHECK_EQUAL( 4, qfldGroup2.nElem() );

  qfldGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );
  BOOST_CHECK_EQUAL( 2, nodeMap[2] );

  qfldGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );
  BOOST_CHECK_EQUAL( 1, nodeMap[2] );

  qfldGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4, nodeMap[1] );
  BOOST_CHECK_EQUAL( 0, nodeMap[2] );

  qfldGroup2.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0, nodeMap[1] );
  BOOST_CHECK_EQUAL( 5, nodeMap[2] );

  xfaceSign = xfldGroup2.associativity(0).faceSign();
  qfaceSign = qfldGroup2.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( xfaceSign[0], qfaceSign[0] );
  BOOST_CHECK_EQUAL( xfaceSign[1], qfaceSign[1] );
  BOOST_CHECK_EQUAL( xfaceSign[2], qfaceSign[2] );

  xfaceSign = xfldGroup2.associativity(1).faceSign();
  qfaceSign = qfldGroup2.associativity(1).faceSign();
  BOOST_CHECK_EQUAL( xfaceSign[0], qfaceSign[0] );
  BOOST_CHECK_EQUAL( xfaceSign[1], qfaceSign[1] );
  BOOST_CHECK_EQUAL( xfaceSign[2], qfaceSign[2] );

  xfaceSign = xfldGroup2.associativity(2).faceSign();
  qfaceSign = qfldGroup2.associativity(2).faceSign();
  BOOST_CHECK_EQUAL( xfaceSign[0], qfaceSign[0] );
  BOOST_CHECK_EQUAL( xfaceSign[1], qfaceSign[1] );
  BOOST_CHECK_EQUAL( xfaceSign[2], qfaceSign[2] );

  xfaceSign = xfldGroup2.associativity(3).faceSign();
  qfaceSign = qfldGroup2.associativity(3).faceSign();
  BOOST_CHECK_EQUAL( xfaceSign[0], qfaceSign[0] );
  BOOST_CHECK_EQUAL( xfaceSign[1], qfaceSign[1] );
  BOOST_CHECK_EQUAL( xfaceSign[2], qfaceSign[2] );
#endif

  // Test the constant assignment operator
  ElementQFieldClass qfldElem(qfldGroup1.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}


#if 0
//----------------------------------------------------------------------------//
// NOTE: DOF ordering for P3 is cells, interior edges, then boundary edges, then nodes
BOOST_AUTO_TEST_CASE( CG_Volume_P3 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_CG_Volume;
  typedef QField3D_CG_Volume::FieldCellGroupType<Tet> QFieldVolumeClass;
  typedef QField3D_CG_Volume::FieldTraceGroupType<Tet> QFieldLineClass;

  int nodeMap[3];
  int edgeMap[6];
  int cellMap[1];

  XField3D_2Tet_X1_1Group xfld1;

  int order = 3;
  QField3D_CG_Volume qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2 + 2*5 + 4, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldVolumeClass& qfldAreaGroup1 = qfld1.getCellGroup<Tet>(0);
  //cout << "btest: qfldAreaGroup1 =" << endl; qfldAreaGroup1.dump(2);

  qfldAreaGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 12, nodeMap[0] );
  BOOST_CHECK_EQUAL( 13, nodeMap[1] );
  BOOST_CHECK_EQUAL( 14, nodeMap[2] );

  qfldAreaGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 15, nodeMap[0] );
  BOOST_CHECK_EQUAL( 14, nodeMap[1] );
  BOOST_CHECK_EQUAL( 13, nodeMap[2] );

  qfldAreaGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL(  2, edgeMap[0] );
  BOOST_CHECK_EQUAL(  3, edgeMap[1] );
  BOOST_CHECK_EQUAL( 10, edgeMap[2] );
  BOOST_CHECK_EQUAL( 11, edgeMap[3] );
  BOOST_CHECK_EQUAL(  4, edgeMap[4] );
  BOOST_CHECK_EQUAL(  5, edgeMap[5] );

  qfldAreaGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL(  2, edgeMap[0] );
  BOOST_CHECK_EQUAL(  3, edgeMap[1] );
  BOOST_CHECK_EQUAL(  6, edgeMap[2] );
  BOOST_CHECK_EQUAL(  7, edgeMap[3] );
  BOOST_CHECK_EQUAL(  8, edgeMap[4] );
  BOOST_CHECK_EQUAL(  9, edgeMap[5] );

  qfldAreaGroup1.associativity(0).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 0, cellMap[0] );

  qfldAreaGroup1.associativity(1).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 1, cellMap[0] );

  const QFieldLineClass& qfldInteriorGroup1 = qfld1.getInteriorTraceGroup<Line>(0);

  qfldInteriorGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 13, nodeMap[0] );
  BOOST_CHECK_EQUAL( 14, nodeMap[1] );

  qfldInteriorGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );
  BOOST_CHECK_EQUAL( 3, edgeMap[1] );

  const QFieldLineClass& qfldBoundaryGroup1 = qfld1.getBoundaryTraceGroup<Line>(0);

  qfldBoundaryGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 12, nodeMap[0] );
  BOOST_CHECK_EQUAL( 13, nodeMap[1] );

  qfldBoundaryGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 13, nodeMap[0] );
  BOOST_CHECK_EQUAL( 15, nodeMap[1] );

  qfldBoundaryGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 15, nodeMap[0] );
  BOOST_CHECK_EQUAL( 14, nodeMap[1] );

  qfldBoundaryGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 14, nodeMap[0] );
  BOOST_CHECK_EQUAL( 12, nodeMap[1] );

  qfldBoundaryGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 4, edgeMap[0] );
  BOOST_CHECK_EQUAL( 5, edgeMap[1] );

  qfldBoundaryGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 6, edgeMap[0] );
  BOOST_CHECK_EQUAL( 7, edgeMap[1] );

  qfldBoundaryGroup1.associativity(2).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 8, edgeMap[0] );
  BOOST_CHECK_EQUAL( 9, edgeMap[1] );

  qfldBoundaryGroup1.associativity(3).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 10, edgeMap[0] );
  BOOST_CHECK_EQUAL( 11, edgeMap[1] );


  XField3D_4Tet_X1_1Group xfld2;

  QField3D_CG_Volume qfld2(xfld2, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 4 + 2*3 + 6 + 2*6, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  QField3D_CG_Volume qfld3(qfld2);

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  const QFieldVolumeClass& qfldAreaGroup2 = qfld2.getCellGroup<Tet>(0);
  //cout << "btest: qfldAreaGroup2 =" << endl; qfldAreaGroup2.dump(2);

  qfldAreaGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 22, nodeMap[0] );
  BOOST_CHECK_EQUAL( 23, nodeMap[1] );
  BOOST_CHECK_EQUAL( 24, nodeMap[2] );

  qfldAreaGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 25, nodeMap[0] );
  BOOST_CHECK_EQUAL( 24, nodeMap[1] );
  BOOST_CHECK_EQUAL( 23, nodeMap[2] );

  qfldAreaGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 24, nodeMap[0] );
  BOOST_CHECK_EQUAL( 26, nodeMap[1] );
  BOOST_CHECK_EQUAL( 22, nodeMap[2] );

  qfldAreaGroup2.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 23, nodeMap[0] );
  BOOST_CHECK_EQUAL( 22, nodeMap[1] );
  BOOST_CHECK_EQUAL( 27, nodeMap[2] );

  qfldAreaGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( 4, edgeMap[0] );
  BOOST_CHECK_EQUAL( 5, edgeMap[1] );
  BOOST_CHECK_EQUAL( 6, edgeMap[2] );
  BOOST_CHECK_EQUAL( 7, edgeMap[3] );
  BOOST_CHECK_EQUAL( 8, edgeMap[4] );
  BOOST_CHECK_EQUAL( 9, edgeMap[5] );

  qfldAreaGroup2.associativity(1).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL(  4, edgeMap[0] );
  BOOST_CHECK_EQUAL(  5, edgeMap[1] );
  BOOST_CHECK_EQUAL( 14, edgeMap[2] );
  BOOST_CHECK_EQUAL( 15, edgeMap[3] );
  BOOST_CHECK_EQUAL( 16, edgeMap[4] );
  BOOST_CHECK_EQUAL( 17, edgeMap[5] );

  qfldAreaGroup2.associativity(2).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( 20, edgeMap[0] );
  BOOST_CHECK_EQUAL( 21, edgeMap[1] );
  BOOST_CHECK_EQUAL(  6, edgeMap[2] );
  BOOST_CHECK_EQUAL(  7, edgeMap[3] );
  BOOST_CHECK_EQUAL( 18, edgeMap[4] );
  BOOST_CHECK_EQUAL( 19, edgeMap[5] );

  qfldAreaGroup2.associativity(3).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( 10, edgeMap[0] );
  BOOST_CHECK_EQUAL( 11, edgeMap[1] );
  BOOST_CHECK_EQUAL( 12, edgeMap[2] );
  BOOST_CHECK_EQUAL( 13, edgeMap[3] );
  BOOST_CHECK_EQUAL(  8, edgeMap[4] );
  BOOST_CHECK_EQUAL(  9, edgeMap[5] );

  qfldAreaGroup2.associativity(0).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 0, cellMap[0] );

  qfldAreaGroup2.associativity(1).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 1, cellMap[0] );

  qfldAreaGroup2.associativity(2).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 2, cellMap[0] );

  qfldAreaGroup2.associativity(3).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 3, cellMap[0] );

  const QFieldLineClass& qfldInteriorGroup2 = qfld2.getInteriorTraceGroup<Line>(0);

  qfldInteriorGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 23, nodeMap[0] );
  BOOST_CHECK_EQUAL( 24, nodeMap[1] );

  qfldInteriorGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 24, nodeMap[0] );
  BOOST_CHECK_EQUAL( 22, nodeMap[1] );

  qfldInteriorGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 22, nodeMap[0] );
  BOOST_CHECK_EQUAL( 23, nodeMap[1] );

  qfldInteriorGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 4, edgeMap[0] );
  BOOST_CHECK_EQUAL( 5, edgeMap[1] );

  qfldInteriorGroup2.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 6, edgeMap[0] );
  BOOST_CHECK_EQUAL( 7, edgeMap[1] );

  qfldInteriorGroup2.associativity(2).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 8, edgeMap[0] );
  BOOST_CHECK_EQUAL( 9, edgeMap[1] );

  const QFieldLineClass& qfldBoundaryGroup2 = qfld2.getBoundaryTraceGroup<Line>(0);

  qfldBoundaryGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 22, nodeMap[0] );
  BOOST_CHECK_EQUAL( 27, nodeMap[1] );

  qfldBoundaryGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 27, nodeMap[0] );
  BOOST_CHECK_EQUAL( 23, nodeMap[1] );

  qfldBoundaryGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 23, nodeMap[0] );
  BOOST_CHECK_EQUAL( 25, nodeMap[1] );

  qfldBoundaryGroup2.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 25, nodeMap[0] );
  BOOST_CHECK_EQUAL( 24, nodeMap[1] );

  qfldBoundaryGroup2.associativity(4).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 24, nodeMap[0] );
  BOOST_CHECK_EQUAL( 26, nodeMap[1] );

  qfldBoundaryGroup2.associativity(5).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 26, nodeMap[0] );
  BOOST_CHECK_EQUAL( 22, nodeMap[1] );

  qfldBoundaryGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 10, edgeMap[0] );
  BOOST_CHECK_EQUAL( 11, edgeMap[1] );

  qfldBoundaryGroup2.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 12, edgeMap[0] );
  BOOST_CHECK_EQUAL( 13, edgeMap[1] );

  qfldBoundaryGroup2.associativity(2).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 14, edgeMap[0] );
  BOOST_CHECK_EQUAL( 15, edgeMap[1] );

  qfldBoundaryGroup2.associativity(3).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 16, edgeMap[0] );
  BOOST_CHECK_EQUAL( 17, edgeMap[1] );

  qfldBoundaryGroup2.associativity(4).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 18, edgeMap[0] );
  BOOST_CHECK_EQUAL( 19, edgeMap[1] );

  qfldBoundaryGroup2.associativity(5).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 20, edgeMap[0] );
  BOOST_CHECK_EQUAL( 21, edgeMap[1] );
}
#endif

//----------------------------------------------------------------------------//
// check that solution trace is identical for adjacent elements
BOOST_AUTO_TEST_CASE( CG_Volume_CheckTrace_BoxGrid )
{
  typedef DLA::VectorS<3, Real> ArrayQ; //Same type as VectorX because we will copy xfld DOFs to the qfld later

  typedef XField3D_2Tet_X1_1Group::FieldCellGroupType<Tet> XFieldVolumeClass;
  typedef XField3D_2Tet_X1_1Group::FieldTraceGroupType<Triangle> XFieldAreaClass;

  typedef XFieldVolumeClass::ElementType<> ElementXFieldVolumeClass;
  typedef XFieldAreaClass::ElementType<> ElementXFieldAreaClass;
  typedef ElementXFieldVolumeClass::VectorX VectorX;

  typedef Field_CG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_CG_Volume;
  typedef QField3D_CG_Volume::FieldCellGroupType<Tet> QFieldVolumeClass;

  typedef QFieldVolumeClass::ElementType<> ElementQFieldVolumeClass;

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-10;

  int Nx = 3;
  int Ny = 3;
  int Nz = 3;
  XField3D_Box_Tet_X1 xfld_X1(comm, Nx,Ny,Nz);

  for ( int order = 1; order <= BasisFunctionVolume_Tet_HierarchicalPMax; order++)
  {
    // curve the grid to the same order as the solution so the DOFs match
    // (only true if both grid and solution curved inside SANS)
    XField<PhysD3,TopoD3> xfld(xfld_X1, order, BasisFunctionCategory_Hierarchical);

    const XFieldVolumeClass& xfldVolume = xfld.getCellGroup<Tet>(0);
    const XFieldAreaClass& xfldFace = xfld.getInteriorTraceGroup<Triangle>(0);

    QField3D_CG_Volume qfld(xfld, order, BasisFunctionCategory_Hierarchical);

    const QFieldVolumeClass& qfldVolume = qfld.getCellGroup<Tet>(0);

    int nCells = Nx*Ny*Nz;
    int nFaces = xfldFace.nElem(); // Only need to modify interior face DOFs
    int nEdges = 279; //for 3x3x3 (couldn't figure out a general formula)

    BOOST_REQUIRE_EQUAL( Nx, 3 );
    BOOST_REQUIRE_EQUAL( Ny, 3 );
    BOOST_REQUIRE_EQUAL( Nz, 3 );
    BOOST_REQUIRE_EQUAL( nEdges, 279 );

    const int nCellDOF = nCells*(order - 1)*(order - 2)*(order - 3)/6;
    const int nFaceDOF = nFaces*(order - 1)*(order - 2)/2;
    const int nEdgeDOF = nEdges*(order - 1);

    const int offset_cellDOF = 0;
    const int offset_faceDOF = nCellDOF;
    const int offset_edgeDOF = nCellDOF + nFaceDOF;

    //Curve the xfld by setting high-oder DOFs to some random nonzero value
    for (int k = 0; k < nCellDOF; k++)
      xfld.DOF(k + offset_cellDOF) = 0.02*(k+1);

    for (int k = 0; k < nFaceDOF; k++)
      xfld.DOF(k + offset_faceDOF) = 0.01*(k+1);

    for (int k = 0; k < nEdgeDOF; k++)
      xfld.DOF(k + offset_edgeDOF) = 0.1*(k+1);

    // initialize qfld DOFs to xfld DOFs (so the solution field essentially has XField data)
    for (int k = 0; k < xfld.nDOF(); k++)
      qfld.DOF(k) = xfld.DOF(k);

    // element field variables
    ElementXFieldAreaClass xfldElemFace( xfldFace.basis() );
    ElementXFieldVolumeClass xfldElemL( xfldVolume.basis() );
    ElementXFieldVolumeClass xfldElemR( xfldVolume.basis() );

    ElementQFieldVolumeClass qfldElemL( qfldVolume.basis() );
    ElementQFieldVolumeClass qfldElemR( qfldVolume.basis() );

    const int nface = xfldFace.nElem();
    for (int face = 0; face < nface; face++)
    {
      const int elemL = xfldFace.getElementLeft( face );
      const int elemR = xfldFace.getElementRight( face );
      const CanonicalTraceToCell& canonicalFaceL = xfldFace.getCanonicalTraceLeft( face );
      const CanonicalTraceToCell& canonicalFaceR = xfldFace.getCanonicalTraceRight( face );

      // copy global grid/solution DOFs to element
      xfldFace.getElement( xfldElemFace, face );
      xfldVolume.getElement( xfldElemL, elemL );
      xfldVolume.getElement( xfldElemR, elemR );

      qfldVolume.getElement( qfldElemL, elemL );
      qfldVolume.getElement( qfldElemR, elemR );

      Real sRefL, tRefL, uRefL, sRefR, tRefR, uRefR;
      Real sRef=0, tRef=0;
      ArrayQ qL, qR;
      VectorX xL, xR, xF;

      int kmax = 5;
      Real dRef = 1./static_cast<Real>(kmax);
      for (int ki = 0; ki < kmax; ki++)
      {
        sRef += dRef;
        tRef = 0;
        for (int kj = ki; kj < kmax; kj++)
        {
          tRef += dRef;

          // left/right reference-element coords
          TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFaceL, sRef, tRef, sRefL, tRefL, uRefL );
          TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFaceR, sRef, tRef, sRefR, tRefR, uRefR );

          // solution trace from L/R elements
          qfldElemL.eval( sRefL, tRefL, uRefL, qL );
          qfldElemR.eval( sRefR, tRefR, uRefR, qR );

          // check if the left and right solutions match
          SANS_CHECK_CLOSE( qL(0), qR(0), small_tol, close_tol );
          SANS_CHECK_CLOSE( qL(1), qR(1), small_tol, close_tol );
          SANS_CHECK_CLOSE( qL(2), qR(2), small_tol, close_tol );

          // element coordinates from L/R elements
          xfldElemFace.eval( sRef, tRef, xF );
          xfldElemL.eval( sRefL, tRefL, uRefL, xL );
          xfldElemR.eval( sRefR, tRefR, uRefR, xR );

          // check if the face coordinates match the cell coordinates
          SANS_CHECK_CLOSE( xF[0], xL[0], small_tol, close_tol );
          SANS_CHECK_CLOSE( xF[1], xL[1], small_tol, close_tol );
          SANS_CHECK_CLOSE( xF[2], xL[2], small_tol, close_tol );

          // check if the left and right coordinates match
          SANS_CHECK_CLOSE( xL[0], xR[0], small_tol, close_tol );
          SANS_CHECK_CLOSE( xL[1], xR[1], small_tol, close_tol );
          SANS_CHECK_CLOSE( xL[2], xR[2], small_tol, close_tol );

          // check if the qfld and xfld solutions match (since both have the same data)
          SANS_CHECK_CLOSE( qL(0), xL(0), small_tol, close_tol );
          SANS_CHECK_CLOSE( qL(1), xL(1), small_tol, close_tol );
          SANS_CHECK_CLOSE( qL(2), xL(2), small_tol, close_tol );
        } // kj
      } // ki
    } //loop over interior faces
  } //loop over order
}

//----------------------------------------------------------------------------//
// check that solution trace is identical for adjacent elements
BOOST_AUTO_TEST_CASE( CG_Volume_CheckTrace_Hierarchical_AllOrientationsGrid )
{
  typedef DLA::VectorS<3, Real> ArrayQ; //Same type as VectorX because we will copy xfld DOFs to the qfld later

  typedef XField3D_2Tet_X1_1Group::FieldCellGroupType<Tet> XFieldVolumeClass;
  typedef XField3D_2Tet_X1_1Group::FieldTraceGroupType<Triangle> XFieldAreaClass;

  typedef XFieldVolumeClass::ElementType<> ElementXFieldVolumeClass;
  typedef XFieldAreaClass::ElementType<> ElementXFieldAreaClass;
  typedef ElementXFieldVolumeClass::VectorX VectorX;

  typedef Field_CG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_CG_Volume;
  typedef QField3D_CG_Volume::FieldCellGroupType<Tet> QFieldVolumeClass;

  typedef QFieldVolumeClass::ElementType<> ElementQFieldVolumeClass;

  const Real small_tol = 1e-12;
  const Real close_tol = 5e-12;

  for ( int order = 1; order <= BasisFunctionVolume_Tet_HierarchicalPMax; order++)
  {
    for (int orientation : {-3, -2, -1, 1, 2, 3})
    {
      XField3D_5Tet_X1_1Group_AllOrientations xfld_X1(orientation);

      // curve the grid to the same order as the solution so the DOFs match
      // (only true if both grid and solution curved inside SANS)
      XField<PhysD3,TopoD3> xfld(xfld_X1,order, BasisFunctionCategory_Hierarchical);

      const XFieldVolumeClass& xfldVolume = xfld.getCellGroup<Tet>(0);
      const XFieldAreaClass& xfldFace = xfld.getInteriorTraceGroup<Triangle>(0);

      QField3D_CG_Volume qfld(xfld, order, BasisFunctionCategory_Hierarchical);

      const QFieldVolumeClass& qfldVolume = qfld.getCellGroup<Tet>(0);

      int nCells = 5;
      int nFaces = xfldFace.nElem(); // Only need to modify interior face DOFs
      int nEdges = 18;

      const int nCellDOF = nCells*(order - 1)*(order - 2)*(order - 3)/6;
      const int nFaceDOF = nFaces*(order - 1)*(order - 2)/2;
      const int nEdgeDOF = nEdges*(order - 1);

      const int offset_cellDOF = 0;
      const int offset_faceDOF = nCellDOF;
      const int offset_edgeDOF = nCellDOF + nFaceDOF;

      //Curve the xfld by setting high-oder DOFs to some random nonzero value
      for (int k = 0; k < nCellDOF; k++)
        xfld.DOF(k + offset_cellDOF) = 0.02*(k+1);

      for (int k = 0; k < nFaceDOF; k++)
        xfld.DOF(k + offset_faceDOF) = 0.01*(k+1);

      for (int k = 0; k < nEdgeDOF; k++)
        xfld.DOF(k + offset_edgeDOF) = 0.1*(k+1);

      // initialize qfld DOFs to xfld DOFs (so the solution field essentially has XField data)
      for (int k = 0; k < xfld.nDOF(); k++)
        qfld.DOF(k) = xfld.DOF(k);

      // element field variables
      ElementXFieldAreaClass xfldElemFace( xfldFace.basis() );
      ElementXFieldVolumeClass xfldElemL( xfldVolume.basis() );
      ElementXFieldVolumeClass xfldElemR( xfldVolume.basis() );

      ElementQFieldVolumeClass qfldElemL( qfldVolume.basis() );
      ElementQFieldVolumeClass qfldElemR( qfldVolume.basis() );

      const int nface = xfldFace.nElem();
      for (int face = 0; face < nface; face++)
      {
        const int elemL = xfldFace.getElementLeft( face );
        const int elemR = xfldFace.getElementRight( face );
        const CanonicalTraceToCell& canonicalFaceL = xfldFace.getCanonicalTraceLeft( face );
        const CanonicalTraceToCell& canonicalFaceR = xfldFace.getCanonicalTraceRight( face );

        // copy global grid/solution DOFs to element
        xfldFace.getElement( xfldElemFace, face );
        xfldVolume.getElement( xfldElemL, elemL );
        xfldVolume.getElement( xfldElemR, elemR );

        qfldVolume.getElement( qfldElemL, elemL );
        qfldVolume.getElement( qfldElemR, elemR );

        Real sRefL, tRefL, uRefL, sRefR, tRefR, uRefR;
        Real sRef=0, tRef=0;
        ArrayQ qL, qR;
        VectorX xL, xR, xF;

        int kmax = 5;
        Real dRef = 1./static_cast<Real>(kmax);
        for (int ki = 0; ki < kmax; ki++)
        {
          sRef += dRef;
          tRef = 0;
          for (int kj = ki; kj < kmax; kj++)
          {
            tRef += dRef;

            // left/right reference-element coords
            TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFaceL, sRef, tRef, sRefL, tRefL, uRefL );
            TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFaceR, sRef, tRef, sRefR, tRefR, uRefR );

            // solution trace from L/R elements
            qfldElemL.eval( sRefL, tRefL, uRefL, qL );
            qfldElemR.eval( sRefR, tRefR, uRefR, qR );

            // check if the left and right solutions match
            SANS_CHECK_CLOSE( qL(0), qR(0), small_tol, close_tol );
            SANS_CHECK_CLOSE( qL(1), qR(1), small_tol, close_tol );
            SANS_CHECK_CLOSE( qL(2), qR(2), small_tol, close_tol );

            // element coordinates from L/R elements
            xfldElemFace.eval( sRef, tRef, xF );
            xfldElemL.eval( sRefL, tRefL, uRefL, xL );
            xfldElemR.eval( sRefR, tRefR, uRefR, xR );

            // check if the face coordinates match the cell coordinates
            SANS_CHECK_CLOSE( xF[0], xL[0], small_tol, close_tol );
            SANS_CHECK_CLOSE( xF[1], xL[1], small_tol, close_tol );
            SANS_CHECK_CLOSE( xF[2], xL[2], small_tol, close_tol );

            // check if the left and right coordinates match
            SANS_CHECK_CLOSE( xL[0], xR[0], small_tol, close_tol );
            SANS_CHECK_CLOSE( xL[1], xR[1], small_tol, close_tol );
            SANS_CHECK_CLOSE( xL[2], xR[2], small_tol, close_tol );

            // check if the qfld and xfld solutions match (since both have the same data)
            SANS_CHECK_CLOSE( qL(0), xL(0), small_tol, close_tol );
            SANS_CHECK_CLOSE( qL(1), xL(1), small_tol, close_tol );
            SANS_CHECK_CLOSE( qL(2), xL(2), small_tol, close_tol );
          } // kj
        } // ki
      } //loop over interior faces
    } //loop over orientations
  } //loop over order
}

//----------------------------------------------------------------------------//
// check that solution trace is identical for adjacent elements
BOOST_AUTO_TEST_CASE( CG_Volume_CheckTrace_Lagrange_AllOrientationsGrid )
{
  typedef DLA::VectorS<3, Real> ArrayQ; //Same type as VectorX because we will copy xfld DOFs to the qfld later

  typedef XField3D_2Tet_X1_1Group::FieldCellGroupType<Tet> XFieldVolumeClass;
  typedef XField3D_2Tet_X1_1Group::FieldTraceGroupType<Triangle> XFieldAreaClass;

  typedef XFieldVolumeClass::ElementType<> ElementXFieldVolumeClass;
  typedef XFieldAreaClass::ElementType<> ElementXFieldAreaClass;
  typedef ElementXFieldVolumeClass::VectorX VectorX;

  typedef Field_CG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_CG_Volume;
  typedef QField3D_CG_Volume::FieldCellGroupType<Tet> QFieldVolumeClass;

  typedef QFieldVolumeClass::ElementType<> ElementQFieldVolumeClass;

  const Real small_tol = 1e-11;
  const Real close_tol = 5e-11;

  for ( int order = 1; order <= BasisFunctionVolume_Tet_LagrangePMax; order++)
  {
    for (int orientation : {-3, -2, -1, 1, 2, 3})
    {
      // create the grid
      XField3D_5Tet_X1_1Group_AllOrientations xfld_X1(orientation);

      // curve the grid to the same order as the solution so the DOFs match
      // (only true if both grid and solution curved inside SANS)
      XField<PhysD3,TopoD3> xfld(xfld_X1, order, BasisFunctionCategory_Lagrange);

      const XFieldVolumeClass& xfldVolume = xfld.getCellGroup<Tet>(0);
      const XFieldAreaClass& xfldFace = xfld.getInteriorTraceGroup<Triangle>(0);

      QField3D_CG_Volume qfld(xfld, order, BasisFunctionCategory_Lagrange);

      const QFieldVolumeClass& qfldVolume = qfld.getCellGroup<Tet>(0);

      // Don't modify DOFs for Lagrange polynomials, they are already non-zero values

      // initialize qfld DOFs to xfld DOFs (so the solution field essentially has XField data)
      for (int k = 0; k < xfld.nDOF(); k++)
        qfld.DOF(k) = xfld.DOF(k);

      // element field variables
      ElementXFieldAreaClass xfldElemFace( xfldFace.basis() );
      ElementXFieldVolumeClass xfldElemL( xfldVolume.basis() );
      ElementXFieldVolumeClass xfldElemR( xfldVolume.basis() );

      ElementQFieldVolumeClass qfldElemL( qfldVolume.basis() );
      ElementQFieldVolumeClass qfldElemR( qfldVolume.basis() );

      const int nface = xfldFace.nElem();
      for (int face = 0; face < nface; face++)
      {
        const int elemL = xfldFace.getElementLeft( face );
        const int elemR = xfldFace.getElementRight( face );
        const CanonicalTraceToCell& canonicalFaceL = xfldFace.getCanonicalTraceLeft( face );
        const CanonicalTraceToCell& canonicalFaceR = xfldFace.getCanonicalTraceRight( face );

        // copy global grid/solution DOFs to element
        xfldFace.getElement( xfldElemFace, face );
        xfldVolume.getElement( xfldElemL, elemL );
        xfldVolume.getElement( xfldElemR, elemR );

        qfldVolume.getElement( qfldElemL, elemL );
        qfldVolume.getElement( qfldElemR, elemR );

        Real sRefL, tRefL, uRefL, sRefR, tRefR, uRefR;
        Real sRef=0, tRef=0;
        ArrayQ qL, qR;
        VectorX xL, xR, xF;

        int kmax = 5;
        Real dRef = 1./static_cast<Real>(kmax);
        for (int ki = 0; ki < kmax; ki++)
        {
          sRef += dRef;
          tRef = 0;
          for (int kj = ki; kj < kmax; kj++)
          {
            tRef += dRef;

            // left/right reference-element coords
            TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFaceL, sRef, tRef, sRefL, tRefL, uRefL );
            TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFaceR, sRef, tRef, sRefR, tRefR, uRefR );

            // solution trace from L/R elements
            qfldElemL.eval( sRefL, tRefL, uRefL, qL );
            qfldElemR.eval( sRefR, tRefR, uRefR, qR );

            // check if the left and right solutions match
            SANS_CHECK_CLOSE( qL(0), qR(0), small_tol, close_tol );
            SANS_CHECK_CLOSE( qL(1), qR(1), small_tol, close_tol );
            SANS_CHECK_CLOSE( qL(2), qR(2), small_tol, close_tol );

            // element coordinates from L/R elements
            xfldElemFace.eval( sRef, tRef, xF );
            xfldElemL.eval( sRefL, tRefL, uRefL, xL );
            xfldElemR.eval( sRefR, tRefR, uRefR, xR );

            // check if the face coordinates match the cell coordinates
            SANS_CHECK_CLOSE( xF[0], xL[0], small_tol, close_tol );
            SANS_CHECK_CLOSE( xF[1], xL[1], small_tol, close_tol );
            SANS_CHECK_CLOSE( xF[2], xL[2], small_tol, close_tol );

            // check if the left and right coordinates match
            SANS_CHECK_CLOSE( xL[0], xR[0], small_tol, close_tol );
            SANS_CHECK_CLOSE( xL[1], xR[1], small_tol, close_tol );
            SANS_CHECK_CLOSE( xL[2], xR[2], small_tol, close_tol );

            // check if the qfld and xfld solutions match (since both have the same data)
            SANS_CHECK_CLOSE( qL(0), xL(0), small_tol, close_tol );
            SANS_CHECK_CLOSE( qL(1), xL(1), small_tol, close_tol );
            SANS_CHECK_CLOSE( qL(2), xL(2), small_tol, close_tol );
          } // kj
        } // ki
      } //loop over interior faces
    } //loop over orientations
  } //loop over order
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_Vol_ProjectPtoPp1_Hierarchical )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_CG_Volume;
  typedef QField3D_CG_Volume::FieldCellGroupType<Tet> QFieldVolumeClass;
  typedef QFieldVolumeClass::ElementType<> ElementQFieldClass;

  ArrayQ q0, q1;

  XField3D_2Tet_X1_1Group xfld1;

  for (int order = 1; order < BasisFunctionVolume_Tet_HierarchicalPMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionVolume_Tet_HierarchicalPMax-order; orderinc++)
    {
      QField3D_CG_Volume qfldP  (xfld1, order, BasisFunctionCategory_Hierarchical);
      QField3D_CG_Volume qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Hierarchical);

      QFieldVolumeClass& qfldVolP   = qfldP.getCellGroup<Tet>(0);
      QFieldVolumeClass& qfldVolPp1 = qfldPp1.getCellGroup<Tet>(0);

      ElementQFieldClass qfldElemP(qfldVolP.basis());
      ElementQFieldClass qfldElemPp1(qfldVolPp1.basis());

      //Give some non-zero initial condition
      for (int n = 0; n < qfldP.nDOF(); n++)
        qfldP.DOF(n) = n+1;

      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = 0;

      //Use area function projectTo
      qfldVolP.projectTo(qfldVolPp1);

      for (int elem = 0; elem < qfldVolP.nElem(); elem++)
      {
        qfldVolP.getElement(qfldElemP, elem);
        qfldVolPp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, 0.25, 0.25, q0);
        qfldElemPp1.eval(0.25, 0.25, 0.25, q1);
        BOOST_CHECK_GT(q0[0], 1);
        BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      }

      //Wipe out DOF's for P1
      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = -1;

      //Use base function projectTo
      qfldP.projectTo(qfldPp1);

      for (int elem = 0; elem < qfldVolP.nElem(); elem++)
      {
        qfldVolP.getElement(qfldElemP, elem);
        qfldVolPp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, 0.25, 0.25, q0);
        qfldElemPp1.eval(0.25, 0.25, 0.25, q1);
        BOOST_CHECK_GT(q0[0], 1);
        BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      }

      const int nDOFperEdge = TopologyDOF_CG<Line>::count(order+orderinc);

      //Check whether all the edge DOFs are equal to zero (because of hierarchical basis)
      for (int k = 0; k < 9*nDOFperEdge; k++)
      {
        BOOST_CHECK_CLOSE(qfldPp1.DOF(k)[0], 0.0, 1e-12);
        BOOST_CHECK_CLOSE(qfldPp1.DOF(k)[1], 0.0, 1e-12);
      }
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_Vol_ProjectPtoPp1_Lagrange )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_CG_Volume;
  typedef QField3D_CG_Volume::FieldCellGroupType<Tet> QFieldVolumeClass;
  typedef QFieldVolumeClass::ElementType<> ElementQFieldClass;

  ArrayQ q0, q1;

  XField3D_2Tet_X1_1Group xfld1;

  for (int order = 1; order < BasisFunctionVolume_Tet_LagrangePMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionVolume_Tet_LagrangePMax-order; orderinc++)
    {
      QField3D_CG_Volume qfldP  (xfld1, order, BasisFunctionCategory_Lagrange);
      QField3D_CG_Volume qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Lagrange);

      QFieldVolumeClass& qfldVolP   = qfldP.getCellGroup<Tet>(0);
      QFieldVolumeClass& qfldVolPp1 = qfldPp1.getCellGroup<Tet>(0);

      ElementQFieldClass qfldElemP(qfldVolP.basis());
      ElementQFieldClass qfldElemPp1(qfldVolPp1.basis());

      //Give some non-zero initial condition
      for (int n = 0; n < qfldP.nDOF(); n++)
        qfldP.DOF(n) = 0.1*n+2;

      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = 0;

      //Use area function projectTo
      qfldVolP.projectTo(qfldVolPp1);

      for (int elem = 0; elem < qfldVolP.nElem(); elem++)
      {
        qfldVolP.getElement(qfldElemP, elem);
        qfldVolPp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, 0.25, 0.25, q0);
        qfldElemPp1.eval(0.25, 0.25, 0.25, q1);
        BOOST_CHECK_GT(q0[0], 1);
        BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      }

      //Wipe out DOF's for P1
      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = -1;

      //Use base function projectTo
      qfldP.projectTo(qfldPp1);

      for (int elem = 0; elem < qfldVolP.nElem(); elem++)
      {
        qfldVolP.getElement(qfldElemP, elem);
        qfldVolPp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, 0.25, 0.25, q0);
        qfldElemPp1.eval(0.25, 0.25, 0.25, q1);
        BOOST_CHECK_GT(q0[0], 1);
        BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      }
    }
  }
}

//----------------------------------------------------------------------------//
void checkPartitionedNativeIndexing(const int order)
{
  typedef Real ArrayQ;
  typedef Field_CG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_CG_Volume;
  typedef QField3D_CG_Volume::FieldCellGroupType<Tet> QFieldVolumeClass;

  // global communicator
  mpi::communicator world;

  int ii = 2;
  int jj = 3;
  int kk = 4;

  // Generate a global grid (identical on all processors)
  XField3D_Box_Tet_X1 xfld_global(world.split(world.rank()), ii, jj, kk);

  // Generate a partitioned grid
  XField3D_Box_Tet_X1 xfld_local(world, ii, jj, kk);

  // local and global fields
  QField3D_CG_Volume qfld_local(xfld_local, order, BasisFunctionCategory_Lagrange);
  QField3D_CG_Volume qfld_global(xfld_global, order, BasisFunctionCategory_Lagrange);

  // gather the local native DOFs and make sure they are unique for each processor
  std::vector<int> nativeDOF(qfld_local.nDOFpossessed());
  for (int idof = 0; idof < qfld_local.nDOFpossessed(); idof++)
    nativeDOF[idof] = qfld_local.local2nativeDOFmap(idof);

  // send the native DOF to all other ranks
  std::vector<std::vector<int>> globalnativeDOF(world.size());
#ifdef SANS_MPI
  boost::mpi::all_gather(world, nativeDOF, globalnativeDOF);
#else
  globalnativeDOF[0] = nativeDOF;
#endif

  // check that the possessed nativeDOF is unieque to each processor
  std::set<int> uniqueNativeDOF;
  for (std::size_t rank = 0; rank < globalnativeDOF.size(); rank++)
  {
    for ( const int nativeDOF : globalnativeDOF[rank])
    {
      BOOST_CHECK(uniqueNativeDOF.find(nativeDOF) == uniqueNativeDOF.end());
      uniqueNativeDOF.insert(nativeDOF);
    }
  }

  // check that the unique count adds up to the total DOF count
  BOOST_CHECK_EQUAL(qfld_global.nDOF(), uniqueNativeDOF.size());


  // check that local2nativeDOFmap is identity on a single processor
  if (world.size() == 1)
    for (int idof = 0; idof < qfld_local.nDOF(); idof++)
      BOOST_CHECK_EQUAL(idof, qfld_local.local2nativeDOFmap(idof));

  for (int idof = 0; idof < qfld_global.nDOF(); idof++)
    BOOST_CHECK_EQUAL(idof, qfld_global.local2nativeDOFmap(idof));


  int group = 0;

  // local and global cell groups
  const QFieldVolumeClass& qfldCellGroup_local = qfld_local.getCellGroup<Tet>(group);
  const QFieldVolumeClass& qfldCellGroup_global = qfld_global.getCellGroup<Tet>(group);

  int nBasis = qfldCellGroup_local.basis()->nBasis();
  std::vector<int> map_local(nBasis);
  std::vector<int> map_global(nBasis);

  const std::vector<int>& groupCellID = xfld_local.cellIDs(group);


  world.barrier();
  for (int irank = 0; irank < world.size(); irank++)
  {
    std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
    world.barrier();
    if (irank != world.rank() ) continue;

    // collect the DOF indexing on the current rank in order to construct
    // the global continuous mapping
    for (int elem_local = 0; elem_local < qfldCellGroup_local.nElem(); elem_local++)
    {
      int elem_global = groupCellID[elem_local];
      qfldCellGroup_local.associativity(elem_local).getGlobalMapping(map_local.data(), map_local.size());
      qfldCellGroup_global.associativity(elem_global).getGlobalMapping(map_global.data(), map_global.size());

#if 0
      int elemRank = qfldCellGroup_local.associativity(elem_local).rank();
      std::cout << "rank " << world.rank() << " elemRank = " << elemRank
                << " elem_global = " << elem_global << " map_global = " << map_global << " l2n = ";
      for (int n = 0; n < nBasis; n++)
        std::cout << qfld_local.local2nativeDOFmap(map_local[n]) << " ";
      std::cout << std::endl;
#endif
      for (int n = 0; n < nBasis; n++)
        BOOST_CHECK_EQUAL(map_global[n], qfld_local.local2nativeDOFmap(map_local[n]));
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedNativeIndexing_P1 )
{
  checkPartitionedNativeIndexing(1);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedNativeIndexing_P2 )
{
  checkPartitionedNativeIndexing(2);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedNativeIndexing_P3 )
{
  checkPartitionedNativeIndexing(3);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedNativeIndexing_P4 )
{
  checkPartitionedNativeIndexing(4);
}

//----------------------------------------------------------------------------//
void checkPartitionedContinuousIndexing(const int order)
{
  typedef Real ArrayQ;
  typedef Field_CG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_CG_Volume;

  // global communicator
  mpi::communicator world;

  const int comm_size = world.size();
  const int comm_rank = world.rank();

  int ii = 2;
  int jj = 3;
  int kk = 4;

  // Generate a partitioned grid
  XField3D_Box_Tet_X1 xfld(world, ii, jj, kk);

  QField3D_CG_Volume qfld(xfld, order, BasisFunctionCategory_Lagrange);

  const GlobalContinuousMap& continuousGlobalMap = qfld.continuousGlobalMap();

  // collect the DOF indexing on the current rank in order to construct
  // the global continuous mapping
  std::map<int,int> native2localDOFmap;
  for (int idof = 0; idof < qfld.nDOFpossessed(); idof++)
    native2localDOFmap[qfld.local2nativeDOFmap(idof)] = idof;

  // send the DOF index to all other ranks
  std::vector<int> nDOFonRank(world.size());
  std::vector<std::map<int,int>> globalnative2localDOFmap(world.size());
#ifdef SANS_MPI
  boost::mpi::all_gather(world, qfld.nDOFpossessed(), nDOFonRank);
  boost::mpi::all_gather(world, native2localDOFmap, globalnative2localDOFmap);
#else
  nDOFonRank[0] = qfld.nDOFpossessed();
  globalnative2localDOFmap[0] = native2localDOFmap;
#endif

  // construct a continuous indexing across all processors
  std::vector<std::vector<int>> continuousDOFindx(world.size());
  int idxDOF = 0;
  for (int rank = 0; rank < world.size(); rank++)
  {
    continuousDOFindx[rank].resize(nDOFonRank[rank]);
    for (std::size_t i = 0; i < continuousDOFindx[rank].size(); i++)
      continuousDOFindx[rank][i] = idxDOF++;
  }

  // compute the DOF rank offset on all ranks
  std::vector<int> nDOF_rank_offset(comm_size, 0);
  for (int rank = 1; rank < comm_size; rank++)
    nDOF_rank_offset[rank] = nDOF_rank_offset[rank-1] + nDOFonRank[rank-1];

  // check that the nDOF rank offset is correct
  BOOST_CHECK_EQUAL( nDOF_rank_offset[comm_rank], continuousGlobalMap.nDOF_rank_offset );

  world.barrier();
  for (int irank = 0; irank < comm_size; irank++)
  {
    std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
    world.barrier();
    if (irank != comm_rank ) continue;

    // check possessed DOFs
    for (int idof = 0; idof < qfld.nDOFpossessed(); idof++)
      BOOST_CHECK_EQUAL( continuousDOFindx[comm_rank][idof], idof + continuousGlobalMap.nDOF_rank_offset );

    // only ghost DOFs are included in the continuousMap
    for (int ighost = 0; ighost < qfld.nDOFghost() ; ighost++)
    {
      int remoteRank = qfld.DOFghost_rank(ighost);
      int nativeDOF = qfld.local2nativeDOFmap(ighost + qfld.nDOFpossessed());
      int remoteDOF = globalnative2localDOFmap[remoteRank].at(nativeDOF);

      BOOST_CHECK_EQUAL( continuousDOFindx[remoteRank][remoteDOF], continuousGlobalMap.remoteGhostIndex[ighost] );
    }

    std::cout << std::flush;
  }
  world.barrier();

  //output_Tecplot(xfld, "tmp/test" + std::to_string(world.rank()) + ".dat");
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedContinuousIndexing_P1 )
{
  checkPartitionedContinuousIndexing(1);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedContinuousIndexing_P2 )
{
  checkPartitionedContinuousIndexing(2);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedContinuousIndexing_P3 )
{
  checkPartitionedContinuousIndexing(3);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedContinuousIndexing_P4 )
{
  checkPartitionedContinuousIndexing(4);
}

//----------------------------------------------------------------------------//
void checkPartitionedSyncDOF(const int order)
{
  typedef Real ArrayQ;
  typedef Field_CG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_CG_Volume;

  // global communicator
  mpi::communicator world;

  const int comm_rank = world.rank();

  int ii = 2;
  int jj = 3;
  int kk = 4;

  // Generate a partitioned grid
  XField3D_Box_Tet_X1 xfld(world, ii, jj, kk);

  QField3D_CG_Volume qfld(xfld, order, BasisFunctionCategory_Lagrange);

  // collect the DOF indexing on the current rank in order to construct
  // the global continuous mapping
  std::map<int,int> native2localDOFmap;
  for (int idof = 0; idof < qfld.nDOFpossessed(); idof++)
    native2localDOFmap[qfld.local2nativeDOFmap(idof)] = idof;

  // send the DOF index to all other ranks
  std::vector<int> nDOFonRank(world.size());;
  std::vector<std::map<int,int>> globalnative2localDOFmap(world.size());
#ifdef SANS_MPI
  boost::mpi::all_gather(world, qfld.nDOFpossessed(), nDOFonRank);
  boost::mpi::all_gather(world, native2localDOFmap, globalnative2localDOFmap);
#else
  nDOFonRank[0] = qfld.nDOFpossessed();
  globalnative2localDOFmap[0] = native2localDOFmap;
#endif

  // construct a continuous indexing across all processors
  std::vector<std::vector<int>> continuousDOFindx(world.size());
  int idxDOF = 0;
  for (int rank = 0; rank < world.size(); rank++)
  {
    continuousDOFindx[rank].resize(nDOFonRank[rank]);
    for (std::size_t i = 0; i < continuousDOFindx[rank].size(); i++)
      continuousDOFindx[rank][i] = idxDOF++;
  }

  // set all DOFs to -1
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = -1;

  BOOST_REQUIRE_EQUAL(qfld.nDOFpossessed(), continuousDOFindx[comm_rank].size());

  // assign the continuous index to possessed DOFs
  for (int idof = 0; idof < qfld.nDOFpossessed(); idof++)
    qfld.DOF(idof) = continuousDOFindx[comm_rank][idof];

  // synchronize the DOFs. all ghosts and zombies should now get the continuous index
  qfld.syncDOFs_MPI_noCache();

  world.barrier();
  for (int irank = 0; irank < world.size(); irank++)
  {
    std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
    world.barrier();
    if (irank != world.rank() ) continue;

    // check that possessed DOFs have not changed
    for (int idof = 0; idof < qfld.nDOFpossessed(); idof++)
      BOOST_CHECK_EQUAL( continuousDOFindx[comm_rank][idof], qfld.DOF(idof) );

    // both ghost and zombie DOFs are synchronized
    for (int ighost = qfld.nDOFpossessed(); ighost < qfld.nDOF(); ighost++)
    {
      int remoteRank = qfld.DOFghost_rank(ighost - qfld.nDOFpossessed());
      int nativeDOF = qfld.local2nativeDOFmap(ighost);
      int remoteDOF = globalnative2localDOFmap[remoteRank].at(nativeDOF);

      BOOST_CHECK_EQUAL( continuousDOFindx[remoteRank][remoteDOF], qfld.DOF(ighost) );
    }

    std::cout << std::flush;
  }
  world.barrier();

  //output_Tecplot(xfld, "tmp/test" + std::to_string(world.rank()) + ".dat");
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedSyncDOF_P1 )
{
  checkPartitionedSyncDOF(1);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedSyncDOF_P2 )
{
  checkPartitionedSyncDOF(2);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedSyncDOF_P3 )
{
  checkPartitionedSyncDOF(3);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedSyncDOF_P4 )
{
  checkPartitionedSyncDOF(4);
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_InteriorEdge_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_CG_Volume;
  typedef QField3D_CG_Volume::FieldTraceGroupType<Tet> QFieldLineClass;

  int nodeMap[2];

  XField3D_4Tet_X1_1Group xfld1;

  int order = 1;
  Field_CG_InteriorTrace< PhysD3, TopoD3, ArrayQ > qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 3, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldGroup1 = qfld1.getInteriorTraceGroup<Line>(0);

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0, nodeMap[1] );

  qfldGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );

  Field< PhysD3, TopoD3, ArrayQ > qfld3(qfld1);

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_InteriorEdge_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_CG_Volume;
  typedef QField3D_CG_Volume::FieldTraceGroupType<Tet> QFieldLineClass;

  int nodeMap[2];
  int edgeMap[1];

  XField3D_4Tet_X1_1Group xfld1;

  int order = 2;
  Field_CG_InteriorTrace<PhysD3, TopoD3, ArrayQ> qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 6, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldGroup1 = qfld1.getInteriorTraceGroup<Line>(0);

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0, nodeMap[1] );

  qfldGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );

  qfldGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 3, edgeMap[0] );

  qfldGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 4, edgeMap[0] );

  qfldGroup1.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 5, edgeMap[0] );

  Field< PhysD3, TopoD3, ArrayQ > qfld3(qfld1);

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_InteriorEdge_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_CG_Volume;
  typedef QField3D_CG_Volume::FieldTraceGroupType<Tet> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  ArrayQ q1, q2;

  XField3D_4Tet_X1_1Group xfld1;

  for (int order = 1; order < BasisFunctionLine_HierarchicalPMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionLine_HierarchicalPMax-order; orderinc++)
    {
      Field_CG_InteriorTrace<PhysD3, TopoD3, ArrayQ> qfldP(xfld1, order, BasisFunctionCategory_Hierarchical);
      Field_CG_InteriorTrace<PhysD3, TopoD3, ArrayQ> qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Hierarchical);

      for ( int group = 0; group < qfldP.nInteriorTraceGroups(); group++ )
      {
        QFieldLineClass& qfldLineGroupP   = qfldP.getInteriorTraceGroup<Line>(group);
        QFieldLineClass& qfldLineGroupPp1 = qfldPp1.getInteriorTraceGroup<Line>(group);

        ElementQFieldLineClass qfldElemP(qfldLineGroupP.basis());
        ElementQFieldLineClass qfldElemPp1(qfldLineGroupPp1.basis());

        //Give some non-zero initial condition
        for (int n = 0; n < qfldP.nDOF(); n++)
          qfldP.DOF(n) = n+1;

        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = 0;

        //Use line function projectTo
        qfldLineGroupP.projectTo(qfldLineGroupPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.25, q1);
          qfldElemPp1.eval(0.25, q2);
          BOOST_CHECK_GT(q1[0], 1);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }

        //Wipe out DOF's for P1
        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = -1;

        //Use base function projectTo
        qfldP.projectTo(qfldPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.75, q1);
          qfldElemPp1.eval(0.75, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }
      }
    }
  }
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryTrace_P1_Empty )
{
  typedef DLA::VectorS<2, Real> ArrayQ;

  XField3D_2Tet_X1_1Group xfld1;

  int order = 1;
  Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> qfld1(xfld1, order, BasisFunctionCategory_Hierarchical, {}); // Empty with no groups

  BOOST_CHECK_EQUAL( 0, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryTrace_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;

  int nodeMap[3];

  XField3D_2Tet_X1_1Group xfld1;

  int order = 1;
  Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 5, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 6, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 6, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  qfld1.getBoundaryTraceGroup<Triangle>(0).associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );
  BOOST_CHECK_EQUAL( 3, nodeMap[2] );

  qfld1.getBoundaryTraceGroup<Triangle>(1).associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );
  BOOST_CHECK_EQUAL( 3, nodeMap[2] );

  qfld1.getBoundaryTraceGroup<Triangle>(2).associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );
  BOOST_CHECK_EQUAL( 1, nodeMap[2] );

  qfld1.getBoundaryTraceGroup<Triangle>(3).associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 4, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3, nodeMap[1] );
  BOOST_CHECK_EQUAL( 2, nodeMap[2] );

  qfld1.getBoundaryTraceGroup<Triangle>(4).associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 4, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0, nodeMap[1] );
  BOOST_CHECK_EQUAL( 3, nodeMap[2] );

  qfld1.getBoundaryTraceGroup<Triangle>(5).associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 4, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );
  BOOST_CHECK_EQUAL( 0, nodeMap[2] );

  Field< PhysD3, TopoD3, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryTrace_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_CG_Volume;
  typedef QField3D_CG_Volume::FieldTraceGroupType<Triangle> QFieldTraceClass;
//  typedef QFieldTraceClass::ElementType<> ElementQFieldTraceClass;

  int nodeMap[3];
  int edgeMap[3];

  XField3D_2Tet_X1_1Group xfld1;

  int order = 2;
  Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 14, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 6, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 6, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldTraceClass& qfldGroup0 = qfld1.getBoundaryTraceGroup<Triangle>(0);
  const QFieldTraceClass& qfldGroup1 = qfld1.getBoundaryTraceGroup<Triangle>(1);
  const QFieldTraceClass& qfldGroup2 = qfld1.getBoundaryTraceGroup<Triangle>(2);
  const QFieldTraceClass& qfldGroup3 = qfld1.getBoundaryTraceGroup<Triangle>(3);
  const QFieldTraceClass& qfldGroup4 = qfld1.getBoundaryTraceGroup<Triangle>(4);
  const QFieldTraceClass& qfldGroup5 = qfld1.getBoundaryTraceGroup<Triangle>(5);

  BOOST_CHECK_EQUAL( qfldGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( qfldGroup1.nElem(), 1 );
  BOOST_CHECK_EQUAL( qfldGroup2.nElem(), 1 );
  BOOST_CHECK_EQUAL( qfldGroup3.nElem(), 1 );
  BOOST_CHECK_EQUAL( qfldGroup4.nElem(), 1 );
  BOOST_CHECK_EQUAL( qfldGroup5.nElem(), 1 );

  //Node DOFs
  qfldGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 10 );
  BOOST_CHECK_EQUAL( nodeMap[1], 11 );
  BOOST_CHECK_EQUAL( nodeMap[2], 12 );

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0],  9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 10 );
  BOOST_CHECK_EQUAL( nodeMap[2], 12 );

  qfldGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0],  9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 11 );
  BOOST_CHECK_EQUAL( nodeMap[2], 10 );

  qfldGroup3.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 13 );
  BOOST_CHECK_EQUAL( nodeMap[1], 12 );
  BOOST_CHECK_EQUAL( nodeMap[2], 11 );

  qfldGroup4.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 13 );
  BOOST_CHECK_EQUAL( nodeMap[1],  9 );
  BOOST_CHECK_EQUAL( nodeMap[2], 12 );

  qfldGroup5.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 13 );
  BOOST_CHECK_EQUAL( nodeMap[1], 11 );
  BOOST_CHECK_EQUAL( nodeMap[2],  9 );

  //Edge DOFs
  qfldGroup0.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 6 );
  BOOST_CHECK_EQUAL( edgeMap[1], 5 );
  BOOST_CHECK_EQUAL( edgeMap[2], 4 );

  qfldGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 5 );
  BOOST_CHECK_EQUAL( edgeMap[1], 2 );
  BOOST_CHECK_EQUAL( edgeMap[2], 0 );

  qfldGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 4 );
  BOOST_CHECK_EQUAL( edgeMap[1], 0 );
  BOOST_CHECK_EQUAL( edgeMap[2], 1 );

  qfldGroup3.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 6 );
  BOOST_CHECK_EQUAL( edgeMap[1], 7 );
  BOOST_CHECK_EQUAL( edgeMap[2], 8 );

  qfldGroup4.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 2 );
  BOOST_CHECK_EQUAL( edgeMap[1], 8 );
  BOOST_CHECK_EQUAL( edgeMap[2], 3 );

  qfldGroup5.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 1 );
  BOOST_CHECK_EQUAL( edgeMap[1], 3 );
  BOOST_CHECK_EQUAL( edgeMap[2], 7 );


  Field< PhysD3, TopoD3, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryEdge_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_CG_Volume;
  typedef QField3D_CG_Volume::FieldTraceGroupType<Triangle> QFieldTraceClass;
  typedef QFieldTraceClass::ElementType<> ElementQFieldTraceClass;

  ArrayQ q1, q2;

  XField3D_2Tet_X1_1Group xfld1;

  for (int order = 1; order < BasisFunctionVolume_Tet_HierarchicalPMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionVolume_Tet_HierarchicalPMax-order; orderinc++)
    {
      Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> qfldP(xfld1, order, BasisFunctionCategory_Hierarchical);
      Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Hierarchical);

      for ( int group = 0; group < qfldP.nBoundaryTraceGroups(); group++ )
      {
        QFieldTraceClass& qfldTraceGroupP   = qfldP.getBoundaryTraceGroup<Triangle>(group);
        QFieldTraceClass& qfldTraceGroupPp1 = qfldPp1.getBoundaryTraceGroup<Triangle>(group);

        ElementQFieldTraceClass qfldElemP(qfldTraceGroupP.basis());
        ElementQFieldTraceClass qfldElemPp1(qfldTraceGroupPp1.basis());

        //Give some non-zero initial condition
        for (int n = 0; n < qfldP.nDOF(); n++)
          qfldP.DOF(n) = n+1;

        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = 0;

        //Use line function projectTo
        qfldTraceGroupP.projectTo(qfldTraceGroupPp1);

        for (int elem = 0; elem < qfldTraceGroupP.nElem(); elem++)
        {
          qfldTraceGroupP.getElement(qfldElemP, elem);
          qfldTraceGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.25, 0.25, q1);
          qfldElemPp1.eval(0.25, 0.25, q2);
          BOOST_CHECK_GT( abs(q1[0]), 1e-12 );
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }
      }

      //Wipe out DOF's for P1
      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = -1;

      //Use base function projectTo
      qfldP.projectTo(qfldPp1);

      for ( int group = 0; group < qfldP.nBoundaryTraceGroups(); group++ )
      {
        QFieldTraceClass& qfldTraceGroupP   = qfldP.getBoundaryTraceGroup<Triangle>(group);
        QFieldTraceClass& qfldTraceGroupPp1 = qfldPp1.getBoundaryTraceGroup<Triangle>(group);

        ElementQFieldTraceClass qfldElemP(qfldTraceGroupP.basis());
        ElementQFieldTraceClass qfldElemPp1(qfldTraceGroupPp1.basis());

        for (int elem = 0; elem < qfldTraceGroupP.nElem(); elem++)
        {
          qfldTraceGroupP.getElement(qfldElemP, elem);
          qfldTraceGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.25, 0.25, q1);
          qfldElemPp1.eval(0.25, 0.25, q2);
          BOOST_CHECK_GT( abs(q1[0]), 1e-12 );
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }
      }

      const int nDOFperEdge = TopologyDOF_CG<Line>::count(order+orderinc);

      //Check whether all the edge DOFs are equal to zero (because of hierarchical basis)
      for (int k = 0; k < 9*nDOFperEdge; k++)
      {
        BOOST_CHECK_CLOSE(qfldPp1.DOF(k)[0], 0.0, 1e-12);
        BOOST_CHECK_CLOSE(qfldPp1.DOF(k)[1], 0.0, 1e-12);
      }

    }
  }
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryEdge_Independent_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_CG_Volume;
  typedef QField3D_CG_Volume::FieldTraceGroupType<Tet> QFieldLineClass;

  int nodeMap[2];

  XField3D_2Tet_X1_1Group xfld1;

  int order = 1;
  std::vector<std::vector<int>> boundaryGroupSets = {{0}};
  Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> qfld1(boundaryGroupSets, xfld1, order);

  BOOST_CHECK_EQUAL( 4, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldGroup1 = qfld1.getBoundaryTraceGroup<Line>(0);

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3, nodeMap[1] );

  qfldGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );

  qfldGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0, nodeMap[1] );

  Field< PhysD3, TopoD3, ArrayQ > qfld3(qfld1);

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryEdge_Independent_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_CG_Volume;
  typedef QField3D_CG_Volume::FieldTraceGroupType<Tet> QFieldLineClass;

  int nodeMap[2];
  int edgeMap[1];

  XField3D_2Tet_X1_1Group xfld1;
  std::vector<std::vector<int>> boundaryGroupSets = {{0}};

  int order = 2;
  Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> qfld1(boundaryGroupSets, xfld1, order);

  BOOST_CHECK_EQUAL( 8, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldGroup1 = qfld1.getBoundaryTraceGroup<Line>(0);

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3, nodeMap[1] );

  qfldGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );

  qfldGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0, nodeMap[1] );

  qfldGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 4, edgeMap[0] );

  qfldGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 5, edgeMap[0] );

  qfldGroup1.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 6, edgeMap[0] );

  qfldGroup1.associativity(3).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 7, edgeMap[0] );

  Field< PhysD3, TopoD3, ArrayQ > qfld3(qfld1);

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryEdge_Independent_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_BoundaryTrace< PhysD3, TopoD3, ArrayQ > QField3D_CG_BoundaryTrace_Independent;
  typedef QField3D_CG_BoundaryTrace_Independent::FieldTraceGroupType<Tet> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  ArrayQ q1, q2;

  XField3D_4Tet_X1_1Group xfld1;

  std::vector<std::vector<int>> boundaryGroupSets = {{0}};

  for (int order = 1; order < BasisFunctionLine_HierarchicalPMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionLine_HierarchicalPMax-order; orderinc++)
    {
      QField3D_CG_BoundaryTrace_Independent qfldP(boundaryGroupSets, xfld1, order);
      QField3D_CG_BoundaryTrace_Independent qfldPp1(boundaryGroupSets, xfld1, order+orderinc);

      for ( int group = 0; group < qfldP.nBoundaryTraceGroups(); group++ )
      {
        QFieldLineClass& qfldLineGroupP   = qfldP.getBoundaryTraceGroup<Line>(group);
        QFieldLineClass& qfldLineGroupPp1 = qfldPp1.getBoundaryTraceGroup<Line>(group);

        ElementQFieldLineClass qfldElemP(qfldLineGroupP.basis());
        ElementQFieldLineClass qfldElemPp1(qfldLineGroupPp1.basis());

        //Give some non-zero initial condition
        for (int n = 0; n < qfldP.nDOF(); n++)
          qfldP.DOF(n) = n+1;

        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = 0;

        //Use line function projectTo
        qfldLineGroupP.projectTo(qfldLineGroupPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.25, q1);
          qfldElemPp1.eval(0.25, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }

        //Wipe out DOF's for P1
        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = -1;

        //Use base function projectTo
        qfldP.projectTo(qfldPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.75, q1);
          qfldElemPp1.eval(0.75, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }
      }
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_Exception )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_CG_Volume;
  typedef Field_CG_InteriorTrace< PhysD3, TopoD3, ArrayQ > QField3D_CG_InteriorTrace;
  typedef Field_CG_BoundaryTrace< PhysD3, TopoD3, ArrayQ > QField3D_CG_BoundaryTrace;


  XField3D_2Tet_X1_1Group xfld1;

  int order = 999; //This should always higher than the maximum available order
  BOOST_CHECK_THROW( QField3D_CG_Volume qfld1(xfld1, order, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( QField3D_CG_InteriorTrace qfld1(xfld1, order, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( QField3D_CG_BoundaryTrace qfld1(xfld1, order, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( QField3D_CG_BoundaryTrace_Independent qfld1(xfld1, order, BasisFunctionCategory_Hierarchical), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_CG_Volume;
  typedef Field_CG_InteriorTrace< PhysD3, TopoD3, ArrayQ > QField3D_CG_InteriorTrace;
  typedef Field_CG_BoundaryTrace< PhysD3, TopoD3, ArrayQ > QField3D_CG_BoundaryTrace;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/Field3D_CG_pattern.txt", true );

  XField3D_2Tet_X1_1Group xfld;

  QField3D_CG_Volume qfld1(xfld, 2);
  qfld1 = 0;
  qfld1.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QField3D_CG_InteriorTrace qfld2(xfld, 2);
  qfld2 = 0;
  qfld2.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QField3D_CG_BoundaryTrace qfld3(xfld, 2);
  qfld3 = 0;
  qfld3.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
