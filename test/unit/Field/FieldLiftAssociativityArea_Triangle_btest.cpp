// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// FieldLiftAssociativityArea_Triangle_btest
// testing of FieldLiftAssociativity class w/ triangle

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"
#include "Field/FieldLift.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "Field/FieldLiftAssociativity.h"
#include "Field/FieldLiftGroupArea_Traits.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class FieldLiftAssociativity< FieldLiftGroupAreaTraits<Triangle,Real> >;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( FieldLiftAssociativityArea_Triangle_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  typedef FieldLiftAssociativity< FieldLiftGroupAreaTraits<Triangle, Real> > FieldAreaClass;

  static_assert( std::is_same<typename FieldAreaClass::FieldBase, FieldLiftAssociativityBase<Real> >::value, "Incorrect Base type" );
  static_assert( std::is_same<typename FieldAreaClass::BasisType, BasisFunctionAreaBase<Triangle> >::value, "Incorrect Basis type" );
  static_assert( std::is_same<typename FieldAreaClass::TopologyType, Triangle>::value, "Incorrect topology type" );
  static_assert( std::is_same<typename FieldAreaClass::FieldLiftAssociativityConstructorType,
                              FieldLiftAssociativityConstructor<ElementAssociativityConstructor<TopoD2, Triangle> > >::value,
                 "Incorrect Associativity Constructor type" );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef FieldLiftAssociativity< FieldLiftGroupAreaTraits<Triangle, Real> > FieldAreaClass;
  typedef typename FieldAreaClass::FieldLiftAssociativityConstructorType FieldLiftAssociativityConstructorType;
  typedef typename FieldAreaClass::FieldBase FieldAreaBase;

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::HierarchicalP1;

  ElementAssociativityConstructor<TopoD2, Triangle> assoc1( basis );
  ElementAssociativityConstructor<TopoD2, Triangle> assoc2( basis );
  ElementAssociativityConstructor<TopoD2, Triangle> assoc3( basis );

  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( 3, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc1.nCell() );

  assoc1.setRank( 0 );
  assoc2.setRank( 0 );
  assoc3.setRank( 0 );

  assoc1.setNodeGlobalMapping( {3, 4, 5} );
  assoc2.setNodeGlobalMapping( {6, 7, 8} );
  assoc3.setNodeGlobalMapping( {9,10,11} );

  int nElem = 1;
  FieldLiftAssociativityConstructorType fldassoc1( basis, nElem);

  fldassoc1.setAssociativity(assoc1, 0, 0);
  fldassoc1.setAssociativity(assoc2, 0, 1);
  fldassoc1.setAssociativity(assoc3, 0, 2);

  // constructor class
  FieldAreaClass fld1(fldassoc1);

  BOOST_CHECK_EQUAL( 1, fld1.nElem() );
  BOOST_CHECK_EQUAL( 3, fld1.nElemTrace() );
  BOOST_CHECK_EQUAL( basis->nBasis(), fld1.nBasis() );
  BOOST_CHECK_EQUAL( basis->order(), fld1.order() );
  BOOST_CHECK_EQUAL( 0, fld1.nDOF() );

  BOOST_CHECK( fld1.topoTypeID() == typeid(Triangle) );

  // default ctor and operator=
  FieldAreaClass fld2;

  BOOST_CHECK_EQUAL( 0, fld2.nElem() );
  BOOST_CHECK_EQUAL( 0, fld2.nDOF() );

  fld2 = fld1;

  BOOST_CHECK_EQUAL( 1, fld2.nElem() );
  BOOST_CHECK_EQUAL( 3, fld2.nElemTrace() );
  BOOST_CHECK_EQUAL( basis->nBasis(), fld2.nBasis() );
  BOOST_CHECK_EQUAL( basis->order(), fld2.order() );
  BOOST_CHECK_EQUAL( 0, fld2.nDOF() );

  BOOST_CHECK( fld2.topoTypeID() == typeid(Triangle) );

  // copy ctor
  FieldAreaClass fld3(fld1);

  BOOST_CHECK_EQUAL( 1, fld3.nElem() );
  BOOST_CHECK_EQUAL( 3, fld3.nElemTrace() );
  BOOST_CHECK_EQUAL( basis->nBasis(), fld3.nBasis() );
  BOOST_CHECK_EQUAL( basis->order(), fld3.order() );
  BOOST_CHECK_EQUAL( 0, fld3.nDOF() );

  BOOST_CHECK( fld3.topoTypeID() == typeid(Triangle) );

  // resize needed for new []
  FieldAreaClass fld4;

  fld4.resize(fldassoc1);

  BOOST_CHECK_EQUAL( 1, fld4.nElem() );
  BOOST_CHECK_EQUAL( 3, fld4.nElemTrace() );
  BOOST_CHECK_EQUAL( basis->nBasis(), fld4.nBasis() );
  BOOST_CHECK_EQUAL( basis->order(), fld4.order() );
  BOOST_CHECK_EQUAL( 0, fld4.nDOF() );

  BOOST_CHECK( fld4.topoTypeID() == typeid(Triangle) );

  // clone
  FieldAreaBase* fld5 = fld1.clone();

  BOOST_CHECK_EQUAL( 1, fld5->nElem() );
  BOOST_CHECK_EQUAL( 3, fld5->nElemTrace() );
  BOOST_CHECK_EQUAL( basis->nBasis(), fld5->nBasis() );
  BOOST_CHECK_EQUAL( basis->order(), fld5->order() );
  BOOST_CHECK_EQUAL( 0, fld5->nDOF() );

  BOOST_CHECK( fld5->topoTypeID() == typeid(Triangle) );
  delete fld5;

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis )
{
  typedef FieldLiftAssociativity< FieldLiftGroupAreaTraits<Triangle, Real> > FieldAreaClass;
  typedef typename FieldAreaClass::FieldLiftAssociativityConstructorType FieldLiftAssociativityConstructorType;

  ElementAssociativityConstructor<TopoD2, Triangle> assoc1( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  ElementAssociativityConstructor<TopoD2, Triangle> assoc2( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  ElementAssociativityConstructor<TopoD2, Triangle> assoc3( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );

  assoc1.setRank( 0 );
  assoc2.setRank( 0 );
  assoc3.setRank( 0 );

  assoc1.setNodeGlobalMapping( {3, 4, 5} );
  assoc2.setNodeGlobalMapping( {6, 7, 8} );
  assoc3.setNodeGlobalMapping( {9,10,11} );

  int nElem = 1;
  FieldLiftAssociativityConstructorType fldassoc1( BasisFunctionAreaBase<Triangle>::HierarchicalP1, nElem);

  fldassoc1.setAssociativity(assoc1, 0, 0);
  fldassoc1.setAssociativity(assoc2, 0, 1);
  fldassoc1.setAssociativity(assoc3, 0, 2);

  FieldAreaClass fld(fldassoc1);

  BOOST_CHECK_EQUAL( 1, fld.nElem() );
  BOOST_CHECK_EQUAL( 0, fld.nDOF() );

  const BasisFunctionAreaBase<Triangle>* basis2 = fld.basis();

  BOOST_CHECK_EQUAL( 1, basis2->order() );
  BOOST_CHECK_EQUAL( 3, basis2->nBasis() );
  BOOST_CHECK_EQUAL( 3, basis2->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis2->nBasisEdge() );
  BOOST_CHECK_EQUAL( 0, basis2->nBasisCell() );
  BOOST_CHECK_EQUAL( basis2, BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DOF_associativity_P1 )
{
  typedef FieldLiftAssociativity< FieldLiftGroupAreaTraits<Triangle, Real> > FieldAreaClass;
  typedef typename FieldAreaClass::FieldLiftAssociativityConstructorType FieldLiftAssociativityConstructorType;

  ElementAssociativityConstructor<TopoD2, Triangle> assoc1( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  ElementAssociativityConstructor<TopoD2, Triangle> assoc2( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  ElementAssociativityConstructor<TopoD2, Triangle> assoc3( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );

  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( 3, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc1.nCell() );

  assoc1.setRank( 0 );
  assoc2.setRank( 0 );
  assoc3.setRank( 0 );

  assoc1.setNodeGlobalMapping( {3, 4, 5} );
  assoc2.setNodeGlobalMapping( {6, 7, 8} );
  assoc3.setNodeGlobalMapping( {9,10,11} );

  int nElem = 1;
  FieldLiftAssociativityConstructorType fldassoc1( BasisFunctionAreaBase<Triangle>::HierarchicalP1, nElem);

  fldassoc1.setAssociativity(assoc1, 0, 0);
  fldassoc1.setAssociativity(assoc2, 0, 1);
  fldassoc1.setAssociativity(assoc3, 0, 2);

  FieldAreaClass fld1(fldassoc1);

  BOOST_CHECK_EQUAL( 1, fld1.nElem() );
  BOOST_CHECK_EQUAL( 0, fld1.nDOF() );

  int node1[3];
  fld1.associativity(0, 0).getNodeGlobalMapping( node1, 3 );
  BOOST_CHECK_EQUAL( 3, node1[0] );
  BOOST_CHECK_EQUAL( 4, node1[1] );
  BOOST_CHECK_EQUAL( 5, node1[2] );

  int node2[3];
  fld1.associativity(0, 1).getNodeGlobalMapping( node2, 3 );
  BOOST_CHECK_EQUAL( 6, node2[0] );
  BOOST_CHECK_EQUAL( 7, node2[1] );
  BOOST_CHECK_EQUAL( 8, node2[2] );

  int node3[3];
  fld1.associativity(0, 2).getNodeGlobalMapping( node3, 3 );
  BOOST_CHECK_EQUAL( 9, node3[0] );
  BOOST_CHECK_EQUAL(10, node3[1] );
  BOOST_CHECK_EQUAL(11, node3[2] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DOF_associativity_P2 )
{
  typedef FieldLiftAssociativity< FieldLiftGroupAreaTraits<Triangle, Real> > FieldAreaClass;
  typedef typename FieldAreaClass::FieldLiftAssociativityConstructorType FieldLiftAssociativityConstructorType;

  ElementAssociativityConstructor<TopoD2, Triangle> assoc1( BasisFunctionAreaBase<Triangle>::HierarchicalP2 );
  ElementAssociativityConstructor<TopoD2, Triangle> assoc2( BasisFunctionAreaBase<Triangle>::HierarchicalP2 );
  ElementAssociativityConstructor<TopoD2, Triangle> assoc3( BasisFunctionAreaBase<Triangle>::HierarchicalP2 );

  BOOST_CHECK_EQUAL( 2, assoc1.order() );
  BOOST_CHECK_EQUAL( 3, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 3, assoc1.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc1.nCell() );

  assoc1.setRank( 0 );
  assoc2.setRank( 0 );
  assoc3.setRank( 0 );

  assoc1.setNodeGlobalMapping( {3, 4, 5} );
  assoc2.setNodeGlobalMapping( {6, 7, 8} );
  assoc3.setNodeGlobalMapping( {9,10,11} );

  assoc1.setEdgeGlobalMapping( {13, 14, 15} );
  assoc2.setEdgeGlobalMapping( {16, 17, 18} );
  assoc3.setEdgeGlobalMapping( {19, 20, 21} );

  int nElem = 1;
  FieldLiftAssociativityConstructorType fldassoc1( BasisFunctionAreaBase<Triangle>::HierarchicalP2, nElem);

  fldassoc1.setAssociativity(assoc1, 0, 0);
  fldassoc1.setAssociativity(assoc2, 0, 1);
  fldassoc1.setAssociativity(assoc3, 0, 2);

  FieldAreaClass fld1( fldassoc1 );

  BOOST_CHECK_EQUAL( 1, fld1.nElem() );
  BOOST_CHECK_EQUAL( 0, fld1.nDOF() );


  int node1[3];
  int edge1[3];

  fld1.associativity(0, 0).getNodeGlobalMapping( node1, 3 );
  BOOST_CHECK_EQUAL( 3, node1[0] );
  BOOST_CHECK_EQUAL( 4, node1[1] );
  BOOST_CHECK_EQUAL( 5, node1[2] );

  fld1.associativity(0, 0).getEdgeGlobalMapping( edge1, 3 );
  BOOST_CHECK_EQUAL(13, edge1[0] );
  BOOST_CHECK_EQUAL(14, edge1[1] );
  BOOST_CHECK_EQUAL(15, edge1[2] );

  int node2[3];
  int edge2[3];

  fld1.associativity(0, 1).getNodeGlobalMapping( node2, 3 );
  BOOST_CHECK_EQUAL( 6, node2[0] );
  BOOST_CHECK_EQUAL( 7, node2[1] );
  BOOST_CHECK_EQUAL( 8, node2[2] );

  fld1.associativity(0, 1).getEdgeGlobalMapping( edge2, 3 );
  BOOST_CHECK_EQUAL(16, edge2[0] );
  BOOST_CHECK_EQUAL(17, edge2[1] );
  BOOST_CHECK_EQUAL(18, edge2[2] );

  int node3[3];
  int edge3[3];

  fld1.associativity(0, 2).getNodeGlobalMapping( node3, 3 );
  BOOST_CHECK_EQUAL( 9, node3[0] );
  BOOST_CHECK_EQUAL(10, node3[1] );
  BOOST_CHECK_EQUAL(11, node3[2] );

  fld1.associativity(0, 2).getEdgeGlobalMapping( edge3, 3 );
  BOOST_CHECK_EQUAL(19, edge3[0] );
  BOOST_CHECK_EQUAL(20, edge3[1] );
  BOOST_CHECK_EQUAL(21, edge3[2] );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( accessors )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef FieldLiftAssociativity< FieldLiftGroupAreaTraits<Triangle, ArrayQ> > FieldAreaClass;
  typedef typename FieldAreaClass::FieldLiftAssociativityConstructorType FieldLiftAssociativityConstructorType;

  ElementAssociativityConstructor<TopoD2, Triangle> assoc1( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  ElementAssociativityConstructor<TopoD2, Triangle> assoc2( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  ElementAssociativityConstructor<TopoD2, Triangle> assoc3( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );

  assoc1.setRank( 0 );
  assoc2.setRank( 0 );
  assoc3.setRank( 0 );

  assoc1.setNodeGlobalMapping( {0, 1, 2} );
  assoc2.setNodeGlobalMapping( {3, 4, 5} );
  assoc3.setNodeGlobalMapping( {6, 7, 8} );

  int nElem = 1;
  FieldLiftAssociativityConstructorType fldassoc1( BasisFunctionAreaBase<Triangle>::HierarchicalP1, nElem);

  fldassoc1.setAssociativity(assoc1, 0, 0);
  fldassoc1.setAssociativity(assoc2, 0, 1);
  fldassoc1.setAssociativity(assoc3, 0, 2);

  FieldAreaClass qfld( fldassoc1 );

  BOOST_CHECK_EQUAL( 1, qfld.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld.nDOF() );

  ArrayQ* qDOF = new ArrayQ[9];

  for (int n = 0; n < 9; n++ ) qDOF[n] = 0;

  qfld.setDOF( qDOF, 9 );
  BOOST_CHECK_EQUAL( 9, qfld.nDOF() );

  for ( int cononicalTrace = 0; cononicalTrace < 2; cononicalTrace++ )
  {
    ArrayQ q1 = {(cononicalTrace+1)*1., (cononicalTrace+1)*2.};
    ArrayQ q2 = {(cononicalTrace+1)*3., (cononicalTrace+1)*4.};
    ArrayQ q3 = {(cononicalTrace+1)*5., (cononicalTrace+1)*6.};

    qfld.DOF(3*cononicalTrace+0) = q1;
    qfld.DOF(3*cononicalTrace+1) = q2;
    qfld.DOF(3*cononicalTrace+2) = q3;

    const Real tol = 1e-13;
    ArrayQ q;
    for (int k = 0; k < 2; k++)
    {
      q = qfld.DOF(3*cononicalTrace+0);
      BOOST_CHECK_CLOSE( q1[k]                          , q[k], tol );
      BOOST_CHECK_CLOSE( qDOF[3*cononicalTrace+0][k], q[k], tol );
      q = qfld.DOF(3*cononicalTrace+1);
      BOOST_CHECK_CLOSE( q2[k]                          , q[k], tol );
      BOOST_CHECK_CLOSE( qDOF[3*cononicalTrace+1][k], q[k], tol );
      q = qfld.DOF(3*cononicalTrace+2);
      BOOST_CHECK_CLOSE( q3[k]                          , q[k], tol );
      BOOST_CHECK_CLOSE( qDOF[3*cononicalTrace+2][k], q[k], tol );
    }

    // const accessors
    const FieldAreaClass qfld2 = qfld;

    for (int k = 0; k < 2; k++)
    {
      q = qfld2.DOF(3*cononicalTrace+0);
      BOOST_CHECK_CLOSE( q1[k]                          , q[k], tol );
      BOOST_CHECK_CLOSE( qDOF[3*cononicalTrace+0][k], q[k], tol );
      q = qfld2.DOF(3*cononicalTrace+1);
      BOOST_CHECK_CLOSE( q2[k]                          , q[k], tol );
      BOOST_CHECK_CLOSE( qDOF[3*cononicalTrace+1][k], q[k], tol );
      q = qfld2.DOF(3*cononicalTrace+2);
      BOOST_CHECK_CLOSE( q3[k]                          , q[k], tol );
      BOOST_CHECK_CLOSE( qDOF[3*cononicalTrace+2][k], q[k], tol );
    }
  }

  delete [] qDOF;
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( elementfieldP1 )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef FieldLiftAssociativity< FieldLiftGroupAreaTraits<Triangle, ArrayQ> > FieldAreaClass;
  typedef typename FieldAreaClass::FieldLiftAssociativityConstructorType FieldLiftAssociativityConstructorType;
  typedef typename FieldAreaClass::template ElementType<> ElementAreaClass;
  typedef std::array<int,3> Int3;

  const Real tol = 1e-13;

  // DOF associativity
  ElementAssociativityConstructor<TopoD2, Triangle> assoc1( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  ElementAssociativityConstructor<TopoD2, Triangle> assoc2( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  ElementAssociativityConstructor<TopoD2, Triangle> assoc3( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );

  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( 3, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );
  BOOST_CHECK_EQUAL( 0, assoc1.nCell() );

  assoc1.setRank( 0 );
  assoc2.setRank( 0 );
  assoc3.setRank( 0 );

  int node1[3] = {3, 4, 5};
  int node2[3] = {6, 7, 8};
  int node3[3] = {9,10,11};
  assoc1.setNodeGlobalMapping( node1, 3 );
  assoc2.setNodeGlobalMapping( node2, 3 );
  assoc3.setNodeGlobalMapping( node3, 3 );

  Int3 edgeSign = {{+1, -1, +1}};
  assoc1.edgeSign() = edgeSign;
  assoc2.edgeSign() = edgeSign;
  assoc3.edgeSign() = edgeSign;

  int nElem = 1;
  FieldLiftAssociativityConstructorType fldassoc( BasisFunctionAreaBase<Triangle>::HierarchicalP1, nElem);

  fldassoc.setAssociativity(assoc1, 0, 0);
  fldassoc.setAssociativity(assoc2, 0, 1);
  fldassoc.setAssociativity(assoc3, 0, 2);

  FieldAreaClass qfld( fldassoc );

  BOOST_CHECK_EQUAL( 1, qfld.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld.nDOF() );

  // solution DOFs
  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};
  ArrayQ q3 = {5, 6};

  ArrayQ q4 = {1*2, 2*2};
  ArrayQ q5 = {3*2, 4*2};
  ArrayQ q6 = {5*2, 6*2};

  ArrayQ q7 = {1*3, 2*3};
  ArrayQ q8 = {3*3, 4*3};
  ArrayQ q9 = {5*3, 6*3};

  ArrayQ* qDOF = new ArrayQ[12];

  qfld.setDOF( qDOF, 12 );
  BOOST_CHECK_EQUAL( 12, qfld.nDOF() );

  qfld.DOF(node1[0]) = q1;
  qfld.DOF(node1[1]) = q2;
  qfld.DOF(node1[2]) = q3;

  qfld.DOF(node2[0]) = q4;
  qfld.DOF(node2[1]) = q5;
  qfld.DOF(node2[2]) = q6;

  qfld.DOF(node3[0]) = q7;
  qfld.DOF(node3[1]) = q8;
  qfld.DOF(node3[2]) = q9;

  // get element DOFs

  ElementAreaClass qfldElem( qfld.basis() );

  qfld.getElement( qfldElem, 0, 0 );

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  ArrayQ q;
  for (int k = 0; k < 2; k++)
  {
    q = qfldElem.DOF(0);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );
    q = qfldElem.DOF(1);
    BOOST_CHECK_CLOSE( q2[k], q[k], tol );
    q = qfldElem.DOF(2);
    BOOST_CHECK_CLOSE( q3[k], q[k], tol );
  }

  qfld.getElement( qfldElem, 0, 1 );

  for (int k = 0; k < 2; k++)
  {
    q = qfldElem.DOF(0);
    BOOST_CHECK_CLOSE( q4[k], q[k], tol );
    q = qfldElem.DOF(1);
    BOOST_CHECK_CLOSE( q5[k], q[k], tol );
    q = qfldElem.DOF(2);
    BOOST_CHECK_CLOSE( q6[k], q[k], tol );
  }

  qfld.getElement( qfldElem, 0, 2 );

  for (int k = 0; k < 2; k++)
  {
    q = qfldElem.DOF(0);
    BOOST_CHECK_CLOSE( q7[k], q[k], tol );
    q = qfldElem.DOF(1);
    BOOST_CHECK_CLOSE( q8[k], q[k], tol );
    q = qfldElem.DOF(2);
    BOOST_CHECK_CLOSE( q9[k], q[k], tol );
  }

  // set element DOFs

  ArrayQ q1b = {7, 9};
  ArrayQ q2b = {4, 3};
  ArrayQ q3b = {2, 5};

  qfldElem.DOF(0) = q1b;
  qfldElem.DOF(1) = q2b;
  qfldElem.DOF(2) = q3b;

  qfld.setElement( qfldElem, 0, 1 );

  for (int k = 0; k < 2; k++)
  {
    q = qfld.DOF(node2[0]);
    BOOST_CHECK_CLOSE( q1b[k], q[k], tol );
    q = qfld.DOF(node2[1]);
    BOOST_CHECK_CLOSE( q2b[k], q[k], tol );
    q = qfld.DOF(node2[2]);
    BOOST_CHECK_CLOSE( q3b[k], q[k], tol );
  }

  // edge signs

  Int3 edgeSign2 = qfldElem.edgeSign();

  BOOST_CHECK_EQUAL( +1, edgeSign2[0] );
  BOOST_CHECK_EQUAL( -1, edgeSign2[1] );
  BOOST_CHECK_EQUAL( +1, edgeSign2[2] );

  delete [] qDOF;
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( elementfieldP3 )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef FieldLiftAssociativity< FieldLiftGroupAreaTraits<Triangle, ArrayQ> > FieldAreaClass;
  typedef typename FieldAreaClass::FieldLiftAssociativityConstructorType FieldLiftAssociativityConstructorType;
  typedef typename FieldAreaClass::template ElementType<> ElementAreaClass;
  typedef std::array<int,3> Int3;

  const Real tol = 1e-13;

  // DOF associativity
  ElementAssociativityConstructor<TopoD2, Triangle> assoc( BasisFunctionAreaBase<Triangle>::HierarchicalP3 );

  BOOST_CHECK_EQUAL( 3, assoc.order() );
  BOOST_CHECK_EQUAL( 3, assoc.nNode() );
  BOOST_CHECK_EQUAL( 6, assoc.nEdge() );
  BOOST_CHECK_EQUAL( 1, assoc.nCell() );

  assoc.setRank( 0 );

  int node[3] = {3, 4, 5};
  int edge[6] = {9, 2, 1, 6, 0, 8};
  int cell[1] = {7};
  assoc.setNodeGlobalMapping( node, 3 );
  assoc.setEdgeGlobalMapping( edge, 6 );
  assoc.setCellGlobalMapping( cell, 1 );

  Int3 edgeSign = {{+1, -1, +1}};
  assoc.edgeSign() = edgeSign;

  int nElem = 1;
  FieldLiftAssociativityConstructorType fldassoc( BasisFunctionAreaBase<Triangle>::HierarchicalP3, nElem);
  fldassoc.setAssociativity(assoc, 0, 0);
  fldassoc.setAssociativity(assoc, 0, 1);
  fldassoc.setAssociativity(assoc, 0, 2);

  FieldAreaClass qfld( fldassoc );

  BOOST_CHECK_EQUAL( 1, qfld.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld.nDOF() );

  // solution DOFs
  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};
  ArrayQ q3 = {5, 6};
  ArrayQ q4 = {7, 1};
  ArrayQ q5 = {4, 8};
  ArrayQ q6 = {2, 9};
  ArrayQ q7 = {3, 2};
  ArrayQ q8 = {6, 7};
  ArrayQ q9 = {9, 3};
  ArrayQ q0 = {7, 6};

  ArrayQ* qDOF = new ArrayQ[10];

  qfld.setDOF( qDOF, 10 );
  BOOST_CHECK_EQUAL( 10, qfld.nDOF() );

  qfld.DOF(node[0]) = q1;
  qfld.DOF(node[1]) = q2;
  qfld.DOF(node[2]) = q3;
  qfld.DOF(edge[0]) = q4;
  qfld.DOF(edge[1]) = q5;
  qfld.DOF(edge[2]) = q6;
  qfld.DOF(edge[3]) = q7;
  qfld.DOF(edge[4]) = q8;
  qfld.DOF(edge[5]) = q9;
  qfld.DOF(cell[0]) = q0;

  // get element DOFs

  ElementAreaClass qfldElem( qfld.basis() );

  qfld.getElement( qfldElem, 0, 0 );

  BOOST_CHECK_EQUAL(  3, qfldElem.order() );
  BOOST_CHECK_EQUAL( 10, qfldElem.nDOF() );

  ArrayQ q;
  for (int k = 0; k < 2; k++)
  {
    q = qfldElem.DOF(0);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );
    q = qfldElem.DOF(1);
    BOOST_CHECK_CLOSE( q2[k], q[k], tol );
    q = qfldElem.DOF(2);
    BOOST_CHECK_CLOSE( q3[k], q[k], tol );
    q = qfldElem.DOF(3);
    BOOST_CHECK_CLOSE( q4[k], q[k], tol );
    q = qfldElem.DOF(4);
    BOOST_CHECK_CLOSE( q5[k], q[k], tol );
    q = qfldElem.DOF(5);
    BOOST_CHECK_CLOSE( q6[k], q[k], tol );
    q = qfldElem.DOF(6);
    BOOST_CHECK_CLOSE( q7[k], q[k], tol );
    q = qfldElem.DOF(7);
    BOOST_CHECK_CLOSE( q8[k], q[k], tol );
    q = qfldElem.DOF(8);
    BOOST_CHECK_CLOSE( q9[k], q[k], tol );
    q = qfldElem.DOF(9);
    BOOST_CHECK_CLOSE( q0[k], q[k], tol );
  }

  // set element DOFs

  for (int n = 0; n < 10; n++)
  {
    qfldElem.DOF(n) = 0;

    q = qfldElem.DOF(n);
    BOOST_CHECK_SMALL( q[0], tol );
    BOOST_CHECK_SMALL( q[1], tol );
  }

  qfldElem.DOF(0) = q1;
  qfldElem.DOF(1) = q2;
  qfldElem.DOF(2) = q3;
  qfldElem.DOF(3) = q4;
  qfldElem.DOF(4) = q5;
  qfldElem.DOF(5) = q6;
  qfldElem.DOF(6) = q7;
  qfldElem.DOF(7) = q8;
  qfldElem.DOF(8) = q9;
  qfldElem.DOF(9) = q0;

  qfld.setElement( qfldElem, 0, 0 );

  for (int k = 0; k < 2; k++)
  {
    q = qfld.DOF(0);
    BOOST_CHECK_CLOSE( q8[k], q[k], tol );
    q = qfld.DOF(1);
    BOOST_CHECK_CLOSE( q6[k], q[k], tol );
    q = qfld.DOF(2);
    BOOST_CHECK_CLOSE( q5[k], q[k], tol );
    q = qfld.DOF(3);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );
    q = qfld.DOF(4);
    BOOST_CHECK_CLOSE( q2[k], q[k], tol );
    q = qfld.DOF(5);
    BOOST_CHECK_CLOSE( q3[k], q[k], tol );
    q = qfld.DOF(6);
    BOOST_CHECK_CLOSE( q7[k], q[k], tol );
    q = qfld.DOF(7);
    BOOST_CHECK_CLOSE( q0[k], q[k], tol );
    q = qfld.DOF(8);
    BOOST_CHECK_CLOSE( q9[k], q[k], tol );
    q = qfld.DOF(9);
    BOOST_CHECK_CLOSE( q4[k], q[k], tol );
  }

  delete [] qDOF;
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( projection_Hierarchical )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef FieldLiftAssociativity< FieldLiftGroupAreaTraits<Triangle, ArrayQ> > FieldAreaClass;
  typedef typename FieldAreaClass::FieldLiftAssociativityConstructorType FieldLiftAssociativityConstructorType;
  typedef typename FieldAreaClass::template ElementType<> ElementAreaClass;
  typedef std::array<int,3> Int3;

  const Real tol = 1e-15;

  for (int order = 1; order < BasisFunctionArea_Triangle_HierarchicalPMax; order++)
  {
    for (int inc = 1; inc <= BasisFunctionArea_Triangle_HierarchicalPMax-order; inc++)
    {
      const BasisFunctionAreaBase<Triangle>* basis0 =
            BasisFunctionAreaBase<Triangle>::getBasisFunction( order      , BasisFunctionCategory_Hierarchical );
      const BasisFunctionAreaBase<Triangle>* basis1 =
            BasisFunctionAreaBase<Triangle>::getBasisFunction( order + inc, BasisFunctionCategory_Hierarchical );

      const int nDOF0 = basis0->nBasis();
      const int nDOF1 = basis1->nBasis();

      // DOF associativity
      ElementAssociativityConstructor<TopoD2, Triangle> assoc0( basis0 );
      ElementAssociativityConstructor<TopoD2, Triangle> assoc1( basis1 );

      assoc0.setRank( 0 );
      assoc1.setRank( 0 );

      // solution DOFs
      ArrayQ* qDOF0 = new ArrayQ[ nDOF0*3 ];
      ArrayQ* qDOF1 = new ArrayQ[ nDOF1*3 ];

      int nElem = 1;
      FieldLiftAssociativityConstructorType fldassoc0( basis0, nElem);
      FieldLiftAssociativityConstructorType fldassoc1( basis1, nElem);

      for ( int cononicalTrace = 0; cononicalTrace < 3; cononicalTrace++ )
      {
        int* map0 = new int[ nDOF0 ];
        int* map1 = new int[ nDOF1 ];
        for (int k = 0; k < nDOF0; k++)
          map0[k] = k + nDOF0*cononicalTrace;
        for (int k = 0; k < nDOF1; k++)
          map1[k] = nDOF1 - 1 - k + nDOF1*cononicalTrace;

        assoc0.setGlobalMapping( map0, nDOF0 );
        assoc1.setGlobalMapping( map1, nDOF1 );

        Int3 edgeSign = {{+1, -1, +1}};
        assoc0.edgeSign() = edgeSign;
        assoc1.edgeSign() = edgeSign;

        fldassoc0.setAssociativity(assoc0, 0, cononicalTrace);
        fldassoc1.setAssociativity(assoc1, 0, cononicalTrace);

        delete [] map0;
        delete [] map1;
      }

      FieldAreaClass qfld0( fldassoc0 );
      FieldAreaClass qfld1( fldassoc1 );

      qfld0.setDOF( qDOF0, 3*nDOF0 );
      qfld1.setDOF( qDOF1, 3*nDOF1 );

      for (int k = 0; k < 3*nDOF0; k++)
      {
        ArrayQ q = { sqrt(k+7), 1./(k+4) };
        qfld0.DOF(k) = q;
      }

      qfld0.projectTo( qfld1 );

      ElementAreaClass qfldElem0( basis0 );
      ElementAreaClass qfldElem1( basis1 );

      for ( int cononicalTrace = 0; cononicalTrace < 3; cononicalTrace++ )
      {
        qfld0.getElement( qfldElem0, 0, cononicalTrace );
        qfld1.getElement( qfldElem1, 0, cononicalTrace );

        ArrayQ q0, q1;
        Real s, t;

        for (int is = 0; is < 3; is++)
        {
          for (int it = 0; it < 3; it++)
          {
            s = 0.11 + is*0.3357;
            t = 0.13 + it*0.2986;

            qfldElem0.eval( s, t, q0 );
            qfldElem1.eval( s, t, q1 );

            for (int n = 0; n < ArrayQ::N; n++)
              BOOST_CHECK_CLOSE( q0[n], q1[n], tol );
          }
        }
      }
      delete [] qDOF0;
      delete [] qDOF1;
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/FieldLiftAssociativityArea_Triangle_pattern.txt", true );

  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef FieldLiftAssociativity< FieldLiftGroupAreaTraits<Triangle, ArrayQ> > FieldAreaClass;
  typedef typename FieldAreaClass::FieldLiftAssociativityConstructorType FieldLiftAssociativityConstructorType;
  typedef std::array<int,3> Int3;

  // DOF associativity
  ElementAssociativityConstructor<TopoD2, Triangle> assoc1( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  ElementAssociativityConstructor<TopoD2, Triangle> assoc2( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  ElementAssociativityConstructor<TopoD2, Triangle> assoc3( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );

  assoc1.setRank( 0 );
  assoc2.setRank( 0 );
  assoc3.setRank( 0 );

  int node1[3] = {0, 1, 2};
  int node2[3] = {3, 4, 5};
  int node3[3] = {6, 7, 8};
  assoc1.setNodeGlobalMapping( node1, 3 );
  assoc2.setNodeGlobalMapping( node2, 3 );
  assoc3.setNodeGlobalMapping( node3, 3 );

  Int3 edgeSign = {{+1, -1, +1}};
  assoc1.edgeSign() = edgeSign;
  assoc2.edgeSign() = edgeSign;
  assoc3.edgeSign() = edgeSign;

  int nElem = 1;
  FieldLiftAssociativityConstructorType fldassoc( BasisFunctionAreaBase<Triangle>::HierarchicalP1, nElem);
  fldassoc.setAssociativity(assoc1, 0, 0);
  fldassoc.setAssociativity(assoc2, 0, 1);
  fldassoc.setAssociativity(assoc3, 0, 2);

  FieldAreaClass qfld( fldassoc );

  // solution DOFs
  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};
  ArrayQ q3 = {5, 6};

  ArrayQ qDOF[9];

  qfld.setDOF( qDOF, 9 );
  BOOST_CHECK_EQUAL( 9, qfld.nDOF() );

  qfld.DOF(node1[0]) = q1;
  qfld.DOF(node1[1]) = q2;
  qfld.DOF(node1[2]) = q3;

  qfld.DOF(node2[0]) = q1;
  qfld.DOF(node2[1]) = q2;
  qfld.DOF(node2[2]) = q3;

  qfld.DOF(node3[0]) = q1;
  qfld.DOF(node3[1]) = q2;
  qfld.DOF(node3[2]) = q3;

  qfld.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
