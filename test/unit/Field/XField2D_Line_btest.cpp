// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField2D_Line_btest
// testing of XField2D with Lines (topo1D)
//
// Note: unit grids tested in "UnitGrids"

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "Field/XFieldLine.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( XField2D_Line_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK( (XField<PhysD2,TopoD1>::D == 2) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  XField<PhysD2,TopoD1> xfld1;

  BOOST_CHECK_EQUAL( 0, xfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, xfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, xfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, xfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, xfld1.nCellGroups() );
}

// This should be a correct version that incurs no exception message.
class XField2D_Corrrect : public XField<PhysD2,TopoD1>
{
public:
  XField2D_Corrrect()
  {
    //Create the DOF arrays
    resizeDOF(2);

    //Create the element/trace groups
    resizeCellGroups(1);
    resizeInteriorTraceGroups(0);
    resizeBoundaryTraceGroups(2);

    // nodal coordinates for the line.
    DOF(0) = {0, 0};
    DOF(1) = {1, 0};

    // cell field variable
    FieldCellGroupType<Line>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionLineBase::HierarchicalP1, 1 );

    // set the processor rank
    fldAssocCell.setAssociativity( 0 ).setRank( 0 );

    //element (line) associativity
    fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );

    cellGroups_[0] = new FieldCellGroupType<Line>( fldAssocCell );

    cellGroups_[0]->setDOF(DOF_, nDOF_);

    nElem_ = 1;

    // interior-node field variable

    // none

    // boundary-node field variable

    FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnode0( BasisFunctionNodeBase::P0, 1 );
    FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnode1( BasisFunctionNodeBase::P0, 1 );

    // edge-element associativity
    fldAssocBnode0.setAssociativity( 0 ).setRank( 0 );
    fldAssocBnode1.setAssociativity( 0 ).setRank( 0 );

    // edge-element associativity
    fldAssocBnode0.setAssociativity( 0 ).setNodeGlobalMapping( {0} );
    fldAssocBnode1.setAssociativity( 0 ).setNodeGlobalMapping( {1} );

    fldAssocBnode0.setAssociativity( 0 ).setNormalSignL( -1 );
    fldAssocBnode1.setAssociativity( 0 ).setNormalSignL(  1 );

    // edge-to-cell connectivity
    fldAssocBnode0.setGroupLeft( 0 );
    fldAssocBnode0.setElementLeft( 0, 0 );
    fldAssocBnode0.setCanonicalTraceLeft( CanonicalTraceToCell(1, 0), 0 );

    fldAssocBnode1.setGroupLeft( 0 );
    fldAssocBnode1.setElementLeft( 0, 0 );
    fldAssocBnode1.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), 0 );

    boundaryTraceGroups_[0] = new FieldTraceGroupType<Node>( fldAssocBnode0 );
    boundaryTraceGroups_[1] = new FieldTraceGroupType<Node>( fldAssocBnode1 );

    boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
    boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);

    checkGrid();
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CorrectXField )
{
  XField2D_Corrrect xfld;
}

// TODO:: Not sure how to compute a negative jacobian for a line in 2D
#if 0
class XField2D_NegativeJacobian : public XField<PhysD2,TopoD1>
{
public:
  XField2D_NegativeJacobian()
  {
    //Create the DOF arrays
    resizeDOF(2);

    //Create the element groups
    resizeInteriorTraceGroups(0);
    resizeBoundaryTraceGroups(2);
    resizeCellGroups(1);

    // nodal coordinates for the line.
    DOF(0) = {0, 0};
    DOF(1) = {-1, 0};  //<---- Should be {1, 0} for positive Jacobian

    // cell field variable
    FieldCellGroupType<Line>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionLineBase::HierarchicalP1, 1 );

    // set the processor rank
    fldAssocCell.setAssociativity( 0 ).setRank( 0 );

    //element (line) associativity
    fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );

    cellGroups_[0] = new FieldCellGroupType<Line>( fldAssocCell );

    cellGroups_[0]->setDOF(DOF_, nDOF_);

    nElem_ = 1;

    // interior-node field variable

    // none

    // boundary-node field variable

    FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnode0( BasisFunctionNodeBase::P0, 1 );
    FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnode1( BasisFunctionNodeBase::P0, 1 );

    // set processor rank
    fldAssocBnode0.setAssociativity( 0 ).setRank( 0 );
    fldAssocBnode1.setAssociativity( 0 ).setRank( 0 );

    // edge-element associativity
    fldAssocBnode0.setAssociativity( 0 ).setNodeGlobalMapping( {0} );
    fldAssocBnode1.setAssociativity( 0 ).setNodeGlobalMapping( {1} );

    fldAssocBnode0.setAssociativity( 0 ).setNormalSignL( -1 );
    fldAssocBnode1.setAssociativity( 0 ).setNormalSignL(  1 );

    // edge-to-cell connectivity
    fldAssocBnode0.setGroupLeft( 0 );
    fldAssocBnode0.setElementLeft( 0, 0 );
    fldAssocBnode0.setCanonicalTraceLeft( CanonicalTraceToCell(1, 0), 0 );

    fldAssocBnode1.setGroupLeft( 0 );
    fldAssocBnode1.setElementLeft( 0, 0 );
    fldAssocBnode1.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), 0 );

    boundaryTraceGroups_[0] = new FieldTraceGroupType<Node>( fldAssocBnode0 );
    boundaryTraceGroups_[1] = new FieldTraceGroupType<Node>( fldAssocBnode1 );

    boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
    boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);

    checkGrid();
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( NegativeJacobian )
{
  BOOST_CHECK_THROW( XField2D_NegativeJacobian xfld, XFieldException );
}
#endif


class XField2D_WrongBoundaryNodeMap : public XField<PhysD2,TopoD1>
{
public:
  XField2D_WrongBoundaryNodeMap()
  {
    //Create the DOF arrays
    resizeDOF(2);

    //Create the element groups
    resizeInteriorTraceGroups(0);
    resizeBoundaryTraceGroups(2);
    resizeCellGroups(1);

    // nodal coordinates for the line.
    DOF(0) = {0, 0};
    DOF(1) = {1, 0};

    // cell field variable
    FieldCellGroupType<Line>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionLineBase::HierarchicalP1, 1 );

    // set the processor rank
    fldAssocCell.setAssociativity( 0 ).setRank( 0 );

    //element (line) associativity
    fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );

    cellGroups_[0] = new FieldCellGroupType<Line>( fldAssocCell );

    cellGroups_[0]->setDOF(DOF_, nDOF_);

    nElem_ = 1;

    // interior-node field variable

    // none

    // boundary-node field variable

    FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnode0( BasisFunctionNodeBase::P0, 1 );
    FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnode1( BasisFunctionNodeBase::P0, 1 );

    // set processor rank
    fldAssocBnode0.setAssociativity( 0 ).setRank( 0 );
    fldAssocBnode1.setAssociativity( 0 ).setRank( 0 );

    // edge-element associativity
    fldAssocBnode0.setAssociativity( 0 ).setNodeGlobalMapping( {0} );
    fldAssocBnode1.setAssociativity( 0 ).setNodeGlobalMapping( {0} ); //<---- Should be {1} for correct right boundary trace

    fldAssocBnode0.setAssociativity( 0 ).setNormalSignL( -1 );
    fldAssocBnode1.setAssociativity( 0 ).setNormalSignL(  1 );

    // edge-to-cell connectivity
    fldAssocBnode0.setGroupLeft( 0 );
    fldAssocBnode0.setElementLeft( 0, 0 );
    fldAssocBnode0.setCanonicalTraceLeft( CanonicalTraceToCell(1, 0), 0 );

    fldAssocBnode1.setGroupLeft( 0 );
    fldAssocBnode1.setElementLeft( 0, 0 );
    fldAssocBnode1.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), 0 );

    boundaryTraceGroups_[0] = new FieldTraceGroupType<Node>( fldAssocBnode0 );
    boundaryTraceGroups_[1] = new FieldTraceGroupType<Node>( fldAssocBnode1 );

    boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
    boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);

    checkGrid();
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WrongBoundaryNodeMap )
{
  BOOST_CHECK_THROW( XField2D_WrongBoundaryNodeMap xfld;, XFieldException );
}


class XField2D_WrongBoundaryCanonicalEdge : public XField<PhysD2,TopoD1>
{
public:
  XField2D_WrongBoundaryCanonicalEdge()
  {
    //Create the DOF arrays
    resizeDOF(2);

    //Create the element groups
    resizeInteriorTraceGroups(0);
    resizeBoundaryTraceGroups(2);
    resizeCellGroups(1);

    // nodal coordinates for the triangle.
    DOF(0) = {0, 0};
    DOF(1) = {1, 0};

    // area field variable
    FieldCellGroupType<Line>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionLineBase::HierarchicalP1, 1 );

    // set the processor rank
    fldAssocCell.setAssociativity( 0 ).setRank( 0 );

    //element area associativity
    fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );

    cellGroups_[0] = new FieldCellGroupType<Line>( fldAssocCell );

    cellGroups_[0]->setDOF(DOF_, nDOF_);

    nElem_ = 1;

    // interior-edge field variable

    // none

    // boundary-node field variable

    FieldTraceGroupType<Node>::FieldAssociativityConstructorType
        fldAssocBnode0( BasisFunctionNodeBase::getBasisFunction( 0, BasisFunctionCategory_Legendre ), 1 );
    FieldTraceGroupType<Node>::FieldAssociativityConstructorType
        fldAssocBnode1( BasisFunctionNodeBase::getBasisFunction( 0, BasisFunctionCategory_Legendre ), 1 );

    // set processor rank
    fldAssocBnode0.setAssociativity( 0 ).setRank( 0 );
    fldAssocBnode1.setAssociativity( 0 ).setRank( 0 );

    // node-element associativity
    fldAssocBnode0.setAssociativity( 0 ).setNodeGlobalMapping( {0} );
    fldAssocBnode1.setAssociativity( 0 ).setNodeGlobalMapping( {1} );

    // node-to-cell connectivity
    fldAssocBnode0.setGroupLeft( 0 );
    fldAssocBnode1.setGroupLeft( 0 );
    fldAssocBnode0.setElementLeft( 0, 0 );
    fldAssocBnode1.setElementLeft( 0, 0 );
    fldAssocBnode0.setCanonicalTraceLeft( CanonicalTraceToCell(1, 0), 0 );
    fldAssocBnode1.setCanonicalTraceLeft( CanonicalTraceToCell(1, 0), 0 ); //<---- Should be (0, 0), 0 );

    boundaryTraceGroups_[0] = new FieldTraceGroupType<Node>( fldAssocBnode0 );
    boundaryTraceGroups_[1] = new FieldTraceGroupType<Node>( fldAssocBnode1 );

    boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
    boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);

    checkGrid();
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WrongBoundaryCanonicalEdge )
{
  BOOST_CHECK_THROW( XField2D_WrongBoundaryCanonicalEdge xfld;, XFieldException );
}


class XField2D_Correct2Cellgroup : public XField<PhysD2,TopoD1>
{
public:
  XField2D_Correct2Cellgroup()
  {
    //Create the DOF arrays
    resizeDOF(3);

    //Create the element groups
    resizeInteriorTraceGroups(1);
    resizeBoundaryTraceGroups(2);
    resizeCellGroups(1);

    // nodal coordinates for the triangle.
    DOF(0) = {0, 0};
    DOF(1) = {1, 0};
    DOF(2) = {1, 1};

    // line field variable
    FieldCellGroupType<Line>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionLineBase::HierarchicalP1, 2 );

    // set the processor rank
    fldAssocCell.setAssociativity( 0 ).setRank( 0 );
    fldAssocCell.setAssociativity( 1 ).setRank( 0 );

    //element area associativity
    fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );
    fldAssocCell.setAssociativity( 1 ).setNodeGlobalMapping( {1, 2} );

    cellGroups_[0] = new FieldCellGroupType<Line>( fldAssocCell );

    cellGroups_[0]->setDOF(DOF_, nDOF_);

    nElem_ = 2;

    // interior-node field variable

    FieldTraceGroupType<Node>::FieldAssociativityConstructorType
        fldAssocInode( BasisFunctionNodeBase::getBasisFunction( 0, BasisFunctionCategory_Legendre ), 1 );

    // set processor rank
    fldAssocInode.setAssociativity( 0 ).setRank( 0 );

    // node-element associativity
    fldAssocInode.setAssociativity( 0 ).setNodeGlobalMapping( {1} );
    fldAssocInode.setAssociativity( 0 ).setNormalSignL(  1 );
    fldAssocInode.setAssociativity( 0 ).setNormalSignR( -1 );

    // edge-to-cell connectivity
    fldAssocInode.setGroupLeft( 0 );
    fldAssocInode.setGroupRight( 0 );
    fldAssocInode.setElementLeft( 0, 0 );
    fldAssocInode.setElementRight( 1, 0 );
    fldAssocInode.setCanonicalTraceLeft(  CanonicalTraceToCell(0, 0), 0 );
    fldAssocInode.setCanonicalTraceRight( CanonicalTraceToCell(1, 0), 0 );

    interiorTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocInode );

    interiorTraceGroups_[0]->setDOF(DOF_, nDOF_);

    // boundary-node field variable

    FieldTraceGroupType<Node>::FieldAssociativityConstructorType
        fldAssocBnode0( BasisFunctionNodeBase::getBasisFunction( 0, BasisFunctionCategory_Legendre ), 1 );
    FieldTraceGroupType<Node>::FieldAssociativityConstructorType
        fldAssocBnode1( BasisFunctionNodeBase::getBasisFunction( 0, BasisFunctionCategory_Legendre ), 1 );

    // set processor rank
    fldAssocBnode0.setAssociativity( 0 ).setRank( 0 );
    fldAssocBnode1.setAssociativity( 0 ).setRank( 0 );

    // node-element associativity
    fldAssocBnode0.setAssociativity( 0 ).setNodeGlobalMapping( {0} );
    fldAssocBnode1.setAssociativity( 0 ).setNodeGlobalMapping( {2} );

    fldAssocBnode0.setAssociativity( 0 ).setNormalSignL( -1 );
    fldAssocBnode1.setAssociativity( 0 ).setNormalSignL(  1 );

    // node-to-cell connectivity
    fldAssocBnode0.setGroupLeft( 0 );
    fldAssocBnode1.setGroupLeft( 0 );
    fldAssocBnode0.setElementLeft( 0, 0 );
    fldAssocBnode1.setElementLeft( 1, 0 );
    fldAssocBnode0.setCanonicalTraceLeft( CanonicalTraceToCell(1, 0), 0 );
    fldAssocBnode1.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), 0 );

    boundaryTraceGroups_[0] = new FieldTraceGroupType<Node>( fldAssocBnode0 );
    boundaryTraceGroups_[1] = new FieldTraceGroupType<Node>( fldAssocBnode1 );

    boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
    boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);

    checkGrid();
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Correct2Cellgroup )
{
  XField2D_Correct2Cellgroup xfld;
}


class XField2D_WrongInteriorNodeMap : public XField<PhysD2,TopoD1>
{
public:
  XField2D_WrongInteriorNodeMap()
  {
    //Create the DOF arrays
    resizeDOF(3);

    //Create the element groups
    resizeInteriorTraceGroups(1);
    resizeBoundaryTraceGroups(2);
    resizeCellGroups(1);

    // nodal coordinates for the triangle.
    DOF(0) = {0, 0};
    DOF(1) = {1, 0};
    DOF(2) = {1, 1};

    // line field variable
    FieldCellGroupType<Line>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionLineBase::HierarchicalP1, 2 );

    // set the processor rank
    fldAssocCell.setAssociativity( 0 ).setRank( 0 );
    fldAssocCell.setAssociativity( 1 ).setRank( 0 );

    //element area associativity
    fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );
    fldAssocCell.setAssociativity( 1 ).setNodeGlobalMapping( {1, 2} );

    cellGroups_[0] = new FieldCellGroupType<Line>( fldAssocCell );

    cellGroups_[0]->setDOF(DOF_, nDOF_);

    nElem_ = 2;

    // interior-node field variable

    FieldTraceGroupType<Node>::FieldAssociativityConstructorType
        fldAssocInode( BasisFunctionNodeBase::getBasisFunction( 0, BasisFunctionCategory_Legendre ), 1 );

    // set processor rank
    fldAssocInode.setAssociativity( 0 ).setRank( 0 );

    // node-element associativity
    fldAssocInode.setAssociativity( 0 ).setNodeGlobalMapping( {0} ); //<---- Should be {1}
    fldAssocInode.setAssociativity( 0 ).setNormalSignL(  1 );
    fldAssocInode.setAssociativity( 0 ).setNormalSignR( -1 );

    // edge-to-cell connectivity
    fldAssocInode.setGroupLeft( 0 );
    fldAssocInode.setGroupRight( 0 );
    fldAssocInode.setElementLeft( 0, 0 );
    fldAssocInode.setElementRight( 1, 0 );
    fldAssocInode.setCanonicalTraceLeft(  CanonicalTraceToCell(0, 0), 0 );
    fldAssocInode.setCanonicalTraceRight( CanonicalTraceToCell(1, 0), 0 );

    interiorTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocInode );

    interiorTraceGroups_[0]->setDOF(DOF_, nDOF_);

    // boundary-node field variable

    FieldTraceGroupType<Node>::FieldAssociativityConstructorType
        fldAssocBnode0( BasisFunctionNodeBase::getBasisFunction( 0, BasisFunctionCategory_Legendre ), 1 );
    FieldTraceGroupType<Node>::FieldAssociativityConstructorType
        fldAssocBnode1( BasisFunctionNodeBase::getBasisFunction( 0, BasisFunctionCategory_Legendre ), 1 );

    // set processor rank
    fldAssocBnode0.setAssociativity( 0 ).setRank( 0 );
    fldAssocBnode1.setAssociativity( 0 ).setRank( 0 );

    // node-element associativity
    fldAssocBnode0.setAssociativity( 0 ).setNodeGlobalMapping( {0} );
    fldAssocBnode1.setAssociativity( 0 ).setNodeGlobalMapping( {2} );

    fldAssocBnode0.setAssociativity( 0 ).setNormalSignL( -1 );
    fldAssocBnode1.setAssociativity( 0 ).setNormalSignL(  1 );

    // node-to-cell connectivity
    fldAssocBnode0.setGroupLeft( 0 );
    fldAssocBnode1.setGroupLeft( 0 );
    fldAssocBnode0.setElementLeft( 0, 0 );
    fldAssocBnode1.setElementLeft( 1, 0 );
    fldAssocBnode0.setCanonicalTraceLeft( CanonicalTraceToCell(1, 0), 0 );
    fldAssocBnode1.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), 0 );

    boundaryTraceGroups_[0] = new FieldTraceGroupType<Node>( fldAssocBnode0 );
    boundaryTraceGroups_[1] = new FieldTraceGroupType<Node>( fldAssocBnode1 );

    boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
    boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);

    checkGrid();
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WrongInteriorNodeMap )
{
  BOOST_CHECK_THROW( XField2D_WrongInteriorNodeMap xfld;, XFieldException );
}

//               2
//               |
//               |
//            (1)|
//               |
//        (0)    |
//  0 ---------- 1


class XField2D_WrongInteriorCanonicalEdge : public XField<PhysD2,TopoD1>
{
public:
  XField2D_WrongInteriorCanonicalEdge()
  {
    //Create the DOF arrays
    resizeDOF(3);

    //Create the element groups
    resizeInteriorTraceGroups(1);
    resizeBoundaryTraceGroups(2);
    resizeCellGroups(1);

    // nodal coordinates for the triangle.
    DOF(0) = {0, 0};
    DOF(1) = {1, 0};
    DOF(2) = {1, 1};

    // line field variable
    FieldCellGroupType<Line>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionLineBase::HierarchicalP1, 2 );

    // set the processor rank
    fldAssocCell.setAssociativity( 0 ).setRank( 0 );
    fldAssocCell.setAssociativity( 1 ).setRank( 0 );

    //element area associativity
    fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );
    fldAssocCell.setAssociativity( 1 ).setNodeGlobalMapping( {1, 2} );

    cellGroups_[0] = new FieldCellGroupType<Line>( fldAssocCell );

    cellGroups_[0]->setDOF(DOF_, nDOF_);

    nElem_ = 2;

    // interior-node field variable

    FieldTraceGroupType<Node>::FieldAssociativityConstructorType
        fldAssocInode( BasisFunctionNodeBase::getBasisFunction( 0, BasisFunctionCategory_Legendre ), 1 );

    // set processor rank
    fldAssocInode.setAssociativity( 0 ).setRank( 0 );

    // node-element associativity
    fldAssocInode.setAssociativity( 0 ).setNodeGlobalMapping( {1} );
    fldAssocInode.setAssociativity( 0 ).setNormalSignL(  1 );
    fldAssocInode.setAssociativity( 0 ).setNormalSignR( -1 );

    // edge-to-cell connectivity
    fldAssocInode.setGroupLeft( 0 );
    fldAssocInode.setGroupRight( 0 );
    fldAssocInode.setElementLeft( 0, 0 );
    fldAssocInode.setElementRight( 1, 0 );
    fldAssocInode.setCanonicalTraceLeft(  CanonicalTraceToCell(0, 0), 0 );
    fldAssocInode.setCanonicalTraceRight( CanonicalTraceToCell(1, 0), 0 );

    interiorTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocInode );

    interiorTraceGroups_[0]->setDOF(DOF_, nDOF_);

    // boundary-node field variable

    FieldTraceGroupType<Node>::FieldAssociativityConstructorType
        fldAssocBnode0( BasisFunctionNodeBase::getBasisFunction( 0, BasisFunctionCategory_Legendre ), 1 );
    FieldTraceGroupType<Node>::FieldAssociativityConstructorType
        fldAssocBnode1( BasisFunctionNodeBase::getBasisFunction( 0, BasisFunctionCategory_Legendre ), 1 );

    // set processor rank
    fldAssocBnode0.setAssociativity( 0 ).setRank( 0 );
    fldAssocBnode1.setAssociativity( 0 ).setRank( 0 );

    // node-element associativity
    fldAssocBnode0.setAssociativity( 0 ).setNodeGlobalMapping( {0} );
    fldAssocBnode1.setAssociativity( 0 ).setNodeGlobalMapping( {2} );

    fldAssocBnode0.setAssociativity( 0 ).setNormalSignL( -1 );
    fldAssocBnode1.setAssociativity( 0 ).setNormalSignL(  1 );

    // node-to-cell connectivity
    fldAssocBnode0.setGroupLeft( 0 );
    fldAssocBnode1.setGroupLeft( 0 );
    fldAssocBnode0.setElementLeft( 0, 0 );
    fldAssocBnode1.setElementLeft( 1, 0 );
    fldAssocBnode0.setCanonicalTraceLeft( CanonicalTraceToCell(1, 0), 0 );
    fldAssocBnode1.setCanonicalTraceLeft( CanonicalTraceToCell(1, 0), 0 ); //<---- Should be (0, 0), 0)

    boundaryTraceGroups_[0] = new FieldTraceGroupType<Node>( fldAssocBnode0 );
    boundaryTraceGroups_[1] = new FieldTraceGroupType<Node>( fldAssocBnode1 );

    boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
    boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);

    checkGrid();
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WrongInteriorCanonicalEdge )
{
  BOOST_CHECK_THROW( XField2D_WrongInteriorCanonicalEdge xfld;, XFieldException );
}

//               2
//               |
//               |
//            (1)|
//               |
//        (0)    |
//  0 ---------- 1


#if 0
class XField2D_WrongBoundaryTraceConnectedNodeMap : public XField<PhysD2,TopoD1>
{
public:
  XField2D_WrongBoundaryTraceConnectedNodeMap()
  {
    //Create the DOF arrays
    resizeDOF(4);

    //Create the element groups
    resizeInteriorTraceGroups(0); // No interior group
    resizeBoundaryTraceGroups(2);
    resizeCellGroups(1);

    // nodal coordinates for the two triangles.
    DOF(0) = {0, 0, 0};
    DOF(1) = {1, 0, 1};
    DOF(2) = {0, 1, 0.5};
    DOF(3) = {1, 1, -0.5};

    // area field variable
    FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 2 );

    //element area associativity
    fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1, 2} );
    fldAssocCell.setAssociativity( 1 ).setNodeGlobalMapping( {3, 2, 1} );

    // edge signs for elements (L is +, R is -)
    fldAssocCell.setAssociativity( 0 ).setEdgeSign( +1, 0 );
    fldAssocCell.setAssociativity( 1 ).setEdgeSign( -1, 0 );

    FieldCellGroupType<Triangle>* xfldElementGroup = NULL;
    cellGroups_[0] = xfldElementGroup = new FieldCellGroupType<Triangle>( fldAssocCell );

    xfldElementGroup->setDOF(DOF_, nDOF_);

    nElem_ = 2;

    // interior-edge field variable

    // boundary-edge field variable

    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, 4 );

    // edge-element associativity
    fldAssocBedge.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );
    fldAssocBedge.setAssociativity( 1 ).setNodeGlobalMapping( {1, 3} );
    fldAssocBedge.setAssociativity( 2 ).setNodeGlobalMapping( {3, 2} );
    fldAssocBedge.setAssociativity( 3 ).setNodeGlobalMapping( {2, 0} );

    // edge-to-cell connectivity
    fldAssocBedge.setGroupLeft( 0 );
    fldAssocBedge.setElementLeft( 0, 0 );
    fldAssocBedge.setElementLeft( 1, 1 );
    fldAssocBedge.setElementLeft( 1, 2 );
    fldAssocBedge.setElementLeft( 0, 3 );
    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 0 );
    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 1 );
    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 2 );
    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 3 );

    boundaryTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocBedge );
    boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);

    // connected boundary trace

    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCBedge( BasisFunctionLineBase::HierarchicalP1, 1 );

    // edge-element associativity
    fldAssocCBedge.setAssociativity( 0 ).setNodeGlobalMapping( {2, 1} ); //<---- Should be {1, 2}

    // edge-to-cell connectivity
    fldAssocCBedge.setGroupRight( 0 );
    fldAssocCBedge.setGroupLeft( 0 );
    fldAssocCBedge.setElementLeft( 0, 0 );
    fldAssocCBedge.setElementRight( 1, 0 );
    fldAssocCBedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), 0 );
    fldAssocCBedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), 0 );

    boundaryTraceGroups_[1] = new FieldTraceGroupType<Line>( fldAssocCBedge );
    boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);

    checkGrid();
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WrongBoundaryTraceConnectedNodeMap )
{
  BOOST_CHECK_THROW( XField2D_WrongBoundaryTraceConnectedNodeMap xfld;, XFieldException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/XField3D_Triangle_pattern.txt", true );

  XField3D_Sphere_Triangle_X1 xfld(2, 2, 10., 170.);

  xfld.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
