// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field2D_CG_Quad_btest
// testing of Field2D_CG_* classes
//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;


#include "unit/UnitGrids/XField2D_2Quad_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Quad_X1_1Group.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionArea_Quad.h"
#include "BasisFunction/TraceToCellRefCoord.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_Trace.h"
#include "Field/FieldArea_CG_InteriorTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"

using namespace std;
using namespace SANS;


//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
template class Field_CG_Cell< PhysD2, TopoD2, Real >;
template class Field_CG_Trace< PhysD2, TopoD2, Real >;
template class Field_CG_InteriorTrace< PhysD2, TopoD2, Real >;
template class Field_CG_BoundaryTrace< PhysD2, TopoD2, Real >;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( Field2D_CG_Quad_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_Area_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef XField2D_2Quad_X1_1Group::FieldCellGroupType<Quad> XFieldAreaClass;
  typedef QField2D_CG_Area::FieldCellGroupType<Quad> QFieldAreaClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldClass;
  typedef std::array<int,4> Int4;

  int nodeMap[4];

  XField2D_2Quad_X1_1Group xfld1;


  const XFieldAreaClass& xfldGroup1 = xfld1.getCellGroup<Quad>(0);

  int order = 1;
  QField2D_CG_Area qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 6, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() ); // These are just views of the cell dofs TODO: check they are
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldAreaClass& qfldGroup1 = qfld1.getCellGroup<Quad>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup1.nElem() );

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );
  BOOST_CHECK_EQUAL( 2, nodeMap[2] );
  BOOST_CHECK_EQUAL( 3, nodeMap[3] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4, nodeMap[1] );
  BOOST_CHECK_EQUAL( 5, nodeMap[2] );
  BOOST_CHECK_EQUAL( 2, nodeMap[3] );

  Int4 xedgeSign, qedgeSign;

  xedgeSign = xfldGroup1.associativity(0).edgeSign();
  qedgeSign = qfldGroup1.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup1.associativity(1).edgeSign();
  qedgeSign = qfldGroup1.associativity(1).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );


  XField2D_4Quad_X1_1Group xfld2;

  const XFieldAreaClass& xfldGroup2 = xfld2.getCellGroup<Quad>(0);

  QField2D_CG_Area qfld2(xfld2, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 9, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 4, qfld2.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nBoundaryTraceGroups() ); // These are just views of the cell dofs TODO: check they are
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  QField2D_CG_Area qfld3(qfld2, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld2.nElem()               ,  qfld3.nElem() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  const QFieldAreaClass& qfldGroup2 = qfld2.getCellGroup<Quad>(0);

  BOOST_CHECK_EQUAL( 4, qfldGroup2.nElem() );

  qfldGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );
  BOOST_CHECK_EQUAL( 4, nodeMap[2] );
  BOOST_CHECK_EQUAL( 3, nodeMap[3] );

  qfldGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );
  BOOST_CHECK_EQUAL( 5, nodeMap[2] );
  BOOST_CHECK_EQUAL( 4, nodeMap[3] );

  qfldGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4, nodeMap[1] );
  BOOST_CHECK_EQUAL( 7, nodeMap[2] );
  BOOST_CHECK_EQUAL( 6, nodeMap[3] );

  qfldGroup2.associativity(3).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( 4, nodeMap[0] );
  BOOST_CHECK_EQUAL( 5, nodeMap[1] );
  BOOST_CHECK_EQUAL( 8, nodeMap[2] );
  BOOST_CHECK_EQUAL( 7, nodeMap[3] );

  xedgeSign = xfldGroup2.associativity(0).edgeSign();
  qedgeSign = qfldGroup2.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );
  BOOST_CHECK_EQUAL( xedgeSign[3], qedgeSign[3] );

  xedgeSign = xfldGroup2.associativity(1).edgeSign();
  qedgeSign = qfldGroup2.associativity(1).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );
  BOOST_CHECK_EQUAL( xedgeSign[3], qedgeSign[3] );

  xedgeSign = xfldGroup2.associativity(2).edgeSign();
  qedgeSign = qfldGroup2.associativity(2).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );
  BOOST_CHECK_EQUAL( xedgeSign[3], qedgeSign[3] );

  xedgeSign = xfldGroup2.associativity(3).edgeSign();
  qedgeSign = qfldGroup2.associativity(3).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );
  BOOST_CHECK_EQUAL( xedgeSign[3], qedgeSign[3] );


  // Test the constant assignment operator
  const QFieldAreaClass& qfld1Area = qfld1.getCellGroup<Quad>(0);
  ElementQFieldClass qfldElem(qfld1Area.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfld1Area.nElem(); elem++)
  {
    qfld1Area.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfld1Area.nElem(); elem++)
  {
    qfld1Area.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryTrace_P1_Empty )
{
  typedef DLA::VectorS<2, Real> ArrayQ;

  XField2D_2Quad_X1_1Group xfld1;

  int order = 1;
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> qfld1(xfld1, order, BasisFunctionCategory_Hierarchical, {}); // Empty with no groups

  BOOST_CHECK_EQUAL( 0, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );
}


#if 0
//----------------------------------------------------------------------------//
// NOTE: DOF ordering for P2 is interior edges, then boundary edges, then nodes
BOOST_AUTO_TEST_CASE( CG_Area_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldCellGroupType<Quad> QFieldAreaClass;
  typedef QField2D_CG_Area::FieldTraceGroupType<Line> QFieldInteriorEdgeClass;
  typedef QField2D_CG_Area::FieldTraceGroupType<Line> QFieldBoundaryEdgeClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldClass;

  int nodeMap[3];
  int edgeMap[3];

  XField2D_2Quad_X1_1Group xfld1;

  int order = 2;
  QField2D_CG_Area qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 4+5, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldAreaClass& qfldAreaGroup1 = qfld1.getCellGroup<Quad>(0);

  qfldAreaGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 5, nodeMap[0] );
  BOOST_CHECK_EQUAL( 6, nodeMap[1] );
  BOOST_CHECK_EQUAL( 7, nodeMap[2] );

  qfldAreaGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 8, nodeMap[0] );
  BOOST_CHECK_EQUAL( 7, nodeMap[1] );
  BOOST_CHECK_EQUAL( 6, nodeMap[2] );

  qfldAreaGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );
  BOOST_CHECK_EQUAL( 4, edgeMap[1] );
  BOOST_CHECK_EQUAL( 1, edgeMap[2] );

  qfldAreaGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );
  BOOST_CHECK_EQUAL( 2, edgeMap[1] );
  BOOST_CHECK_EQUAL( 3, edgeMap[2] );

  const QFieldInteriorEdgeClass& qfldInteriorGroup1 = qfld1.getInteriorTraceGroup<Line>(0);

  qfldInteriorGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 6, nodeMap[0] );
  BOOST_CHECK_EQUAL( 7, nodeMap[1] );

  qfldInteriorGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );

  const QFieldBoundaryEdgeClass& qfldBoundaryGroup1 = qfld1.getBoundaryTraceGroup<Line>(0);

  qfldBoundaryGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 5, nodeMap[0] );
  BOOST_CHECK_EQUAL( 6, nodeMap[1] );

  qfldBoundaryGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 6, nodeMap[0] );
  BOOST_CHECK_EQUAL( 8, nodeMap[1] );

  qfldBoundaryGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 8, nodeMap[0] );
  BOOST_CHECK_EQUAL( 7, nodeMap[1] );

  qfldBoundaryGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 7, nodeMap[0] );
  BOOST_CHECK_EQUAL( 5, nodeMap[1] );

  qfldBoundaryGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 1, edgeMap[0] );

  qfldBoundaryGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );

  qfldBoundaryGroup1.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 3, edgeMap[0] );

  qfldBoundaryGroup1.associativity(3).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 4, edgeMap[0] );


  XField2D_4Quad_X1_1Group xfld2;

  QField2D_CG_Area qfld2(xfld2, order);

  BOOST_CHECK_EQUAL( 6+3+6, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  QField2D_CG_Area qfld3(qfld2);

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  const QFieldAreaClass& qfldAreaGroup2 = qfld2.getCellGroup<Quad>(0);

  qfldAreaGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL(  9, nodeMap[0] );
  BOOST_CHECK_EQUAL( 10, nodeMap[1] );
  BOOST_CHECK_EQUAL( 11, nodeMap[2] );

  qfldAreaGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 12, nodeMap[0] );
  BOOST_CHECK_EQUAL( 11, nodeMap[1] );
  BOOST_CHECK_EQUAL( 10, nodeMap[2] );

  qfldAreaGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 11, nodeMap[0] );
  BOOST_CHECK_EQUAL( 13, nodeMap[1] );
  BOOST_CHECK_EQUAL(  9, nodeMap[2] );

  qfldAreaGroup2.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 10, nodeMap[0] );
  BOOST_CHECK_EQUAL(  9, nodeMap[1] );
  BOOST_CHECK_EQUAL( 14, nodeMap[2] );

  const QFieldInteriorEdgeClass& qfldInteriorGroup2 = qfld2.getInteriorTraceGroup<Line>(0);

  qfldInteriorGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 10, nodeMap[0] );
  BOOST_CHECK_EQUAL( 11, nodeMap[1] );

  qfldInteriorGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 11, nodeMap[0] );
  BOOST_CHECK_EQUAL(  9, nodeMap[1] );

  qfldInteriorGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL(  9, nodeMap[0] );
  BOOST_CHECK_EQUAL( 10, nodeMap[1] );

  qfldInteriorGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );

  qfldInteriorGroup2.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 1, edgeMap[0] );

  qfldInteriorGroup2.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );


  const QFieldBoundaryEdgeClass& qfldBoundaryGroup2 = qfld2.getBoundaryTraceGroup<Line>(0);

  qfldBoundaryGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL(  9, nodeMap[0] );
  BOOST_CHECK_EQUAL( 14, nodeMap[1] );

  qfldBoundaryGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 14, nodeMap[0] );
  BOOST_CHECK_EQUAL( 10, nodeMap[1] );

  qfldBoundaryGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 10, nodeMap[0] );
  BOOST_CHECK_EQUAL( 12, nodeMap[1] );

  qfldBoundaryGroup2.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 12, nodeMap[0] );
  BOOST_CHECK_EQUAL( 11, nodeMap[1] );

  qfldBoundaryGroup2.associativity(4).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 11, nodeMap[0] );
  BOOST_CHECK_EQUAL( 13, nodeMap[1] );

  qfldBoundaryGroup2.associativity(5).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 13, nodeMap[0] );
  BOOST_CHECK_EQUAL(  9, nodeMap[1] );

  qfldBoundaryGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 3, edgeMap[0] );

  qfldBoundaryGroup2.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 4, edgeMap[0] );

  qfldBoundaryGroup2.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 5, edgeMap[0] );

  qfldBoundaryGroup2.associativity(3).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 6, edgeMap[0] );

  qfldBoundaryGroup2.associativity(4).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 7, edgeMap[0] );

  qfldBoundaryGroup2.associativity(5).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 8, edgeMap[0] );


  // Test the constant assignment operator
  const QFieldAreaClass& qfld1Area = qfld1.getCellGroup<Quad>(0);
  ElementQFieldClass qfldElem(qfld1Area.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfld1Area.nElem(); elem++)
  {
    qfld1Area.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfld1Area.nElem(); elem++)
  {
    qfld1Area.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}


//----------------------------------------------------------------------------//
// NOTE: DOF ordering for P3 is cells, interior edges, then boundary edges, then nodes
BOOST_AUTO_TEST_CASE( CG_Area_P3 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldCellGroupType<Quad> QFieldAreaClass;
  typedef QField2D_CG_Area::FieldTraceGroupType<Line> QFieldLineClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldClass;

  int nodeMap[3];
  int edgeMap[6];
  int cellMap[1];

  XField2D_2Quad_X1_1Group xfld1;

  int order = 3;
  QField2D_CG_Area qfld1(xfld1, order);

  BOOST_CHECK_EQUAL( 2 + 2*5 + 4, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldAreaClass& qfldAreaGroup1 = qfld1.getCellGroup<Quad>(0);
  //cout << "btest: qfldAreaGroup1 =" << endl; qfldAreaGroup1.dump(2);

  qfldAreaGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 12, nodeMap[0] );
  BOOST_CHECK_EQUAL( 13, nodeMap[1] );
  BOOST_CHECK_EQUAL( 14, nodeMap[2] );

  qfldAreaGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 15, nodeMap[0] );
  BOOST_CHECK_EQUAL( 14, nodeMap[1] );
  BOOST_CHECK_EQUAL( 13, nodeMap[2] );

  qfldAreaGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL(  2, edgeMap[0] );
  BOOST_CHECK_EQUAL(  3, edgeMap[1] );
  BOOST_CHECK_EQUAL( 10, edgeMap[2] );
  BOOST_CHECK_EQUAL( 11, edgeMap[3] );
  BOOST_CHECK_EQUAL(  4, edgeMap[4] );
  BOOST_CHECK_EQUAL(  5, edgeMap[5] );

  qfldAreaGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL(  2, edgeMap[0] );
  BOOST_CHECK_EQUAL(  3, edgeMap[1] );
  BOOST_CHECK_EQUAL(  6, edgeMap[2] );
  BOOST_CHECK_EQUAL(  7, edgeMap[3] );
  BOOST_CHECK_EQUAL(  8, edgeMap[4] );
  BOOST_CHECK_EQUAL(  9, edgeMap[5] );

  qfldAreaGroup1.associativity(0).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 0, cellMap[0] );

  qfldAreaGroup1.associativity(1).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 1, cellMap[0] );

  const QFieldLineClass& qfldInteriorGroup1 = qfld1.getInteriorTraceGroup<Line>(0);

  qfldInteriorGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 13, nodeMap[0] );
  BOOST_CHECK_EQUAL( 14, nodeMap[1] );

  qfldInteriorGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );
  BOOST_CHECK_EQUAL( 3, edgeMap[1] );

  const QFieldLineClass& qfldBoundaryGroup1 = qfld1.getBoundaryTraceGroup<Line>(0);

  qfldBoundaryGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 12, nodeMap[0] );
  BOOST_CHECK_EQUAL( 13, nodeMap[1] );

  qfldBoundaryGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 13, nodeMap[0] );
  BOOST_CHECK_EQUAL( 15, nodeMap[1] );

  qfldBoundaryGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 15, nodeMap[0] );
  BOOST_CHECK_EQUAL( 14, nodeMap[1] );

  qfldBoundaryGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 14, nodeMap[0] );
  BOOST_CHECK_EQUAL( 12, nodeMap[1] );

  qfldBoundaryGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 4, edgeMap[0] );
  BOOST_CHECK_EQUAL( 5, edgeMap[1] );

  qfldBoundaryGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 6, edgeMap[0] );
  BOOST_CHECK_EQUAL( 7, edgeMap[1] );

  qfldBoundaryGroup1.associativity(2).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 8, edgeMap[0] );
  BOOST_CHECK_EQUAL( 9, edgeMap[1] );

  qfldBoundaryGroup1.associativity(3).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 10, edgeMap[0] );
  BOOST_CHECK_EQUAL( 11, edgeMap[1] );


  XField2D_4Quad_X1_1Group xfld2;

  QField2D_CG_Area qfld2(xfld2, order);

  BOOST_CHECK_EQUAL( 4 + 2*3 + 6 + 2*6, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  QField2D_CG_Area qfld3(qfld2);

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  const QFieldAreaClass& qfldAreaGroup2 = qfld2.getCellGroup<Quad>(0);
  //cout << "btest: qfldAreaGroup2 =" << endl; qfldAreaGroup2.dump(2);

  qfldAreaGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 22, nodeMap[0] );
  BOOST_CHECK_EQUAL( 23, nodeMap[1] );
  BOOST_CHECK_EQUAL( 24, nodeMap[2] );

  qfldAreaGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 25, nodeMap[0] );
  BOOST_CHECK_EQUAL( 24, nodeMap[1] );
  BOOST_CHECK_EQUAL( 23, nodeMap[2] );

  qfldAreaGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 24, nodeMap[0] );
  BOOST_CHECK_EQUAL( 26, nodeMap[1] );
  BOOST_CHECK_EQUAL( 22, nodeMap[2] );

  qfldAreaGroup2.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 23, nodeMap[0] );
  BOOST_CHECK_EQUAL( 22, nodeMap[1] );
  BOOST_CHECK_EQUAL( 27, nodeMap[2] );

  qfldAreaGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( 4, edgeMap[0] );
  BOOST_CHECK_EQUAL( 5, edgeMap[1] );
  BOOST_CHECK_EQUAL( 6, edgeMap[2] );
  BOOST_CHECK_EQUAL( 7, edgeMap[3] );
  BOOST_CHECK_EQUAL( 8, edgeMap[4] );
  BOOST_CHECK_EQUAL( 9, edgeMap[5] );

  qfldAreaGroup2.associativity(1).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL(  4, edgeMap[0] );
  BOOST_CHECK_EQUAL(  5, edgeMap[1] );
  BOOST_CHECK_EQUAL( 14, edgeMap[2] );
  BOOST_CHECK_EQUAL( 15, edgeMap[3] );
  BOOST_CHECK_EQUAL( 16, edgeMap[4] );
  BOOST_CHECK_EQUAL( 17, edgeMap[5] );

  qfldAreaGroup2.associativity(2).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( 20, edgeMap[0] );
  BOOST_CHECK_EQUAL( 21, edgeMap[1] );
  BOOST_CHECK_EQUAL(  6, edgeMap[2] );
  BOOST_CHECK_EQUAL(  7, edgeMap[3] );
  BOOST_CHECK_EQUAL( 18, edgeMap[4] );
  BOOST_CHECK_EQUAL( 19, edgeMap[5] );

  qfldAreaGroup2.associativity(3).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( 10, edgeMap[0] );
  BOOST_CHECK_EQUAL( 11, edgeMap[1] );
  BOOST_CHECK_EQUAL( 12, edgeMap[2] );
  BOOST_CHECK_EQUAL( 13, edgeMap[3] );
  BOOST_CHECK_EQUAL(  8, edgeMap[4] );
  BOOST_CHECK_EQUAL(  9, edgeMap[5] );

  qfldAreaGroup2.associativity(0).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 0, cellMap[0] );

  qfldAreaGroup2.associativity(1).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 1, cellMap[0] );

  qfldAreaGroup2.associativity(2).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 2, cellMap[0] );

  qfldAreaGroup2.associativity(3).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 3, cellMap[0] );

  const QFieldLineClass& qfldInteriorGroup2 = qfld2.getInteriorTraceGroup<Line>(0);

  qfldInteriorGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 23, nodeMap[0] );
  BOOST_CHECK_EQUAL( 24, nodeMap[1] );

  qfldInteriorGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 24, nodeMap[0] );
  BOOST_CHECK_EQUAL( 22, nodeMap[1] );

  qfldInteriorGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 22, nodeMap[0] );
  BOOST_CHECK_EQUAL( 23, nodeMap[1] );

  qfldInteriorGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 4, edgeMap[0] );
  BOOST_CHECK_EQUAL( 5, edgeMap[1] );

  qfldInteriorGroup2.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 6, edgeMap[0] );
  BOOST_CHECK_EQUAL( 7, edgeMap[1] );

  qfldInteriorGroup2.associativity(2).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 8, edgeMap[0] );
  BOOST_CHECK_EQUAL( 9, edgeMap[1] );

  const QFieldLineClass& qfldBoundaryGroup2 = qfld2.getBoundaryTraceGroup<Line>(0);

  qfldBoundaryGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 22, nodeMap[0] );
  BOOST_CHECK_EQUAL( 27, nodeMap[1] );

  qfldBoundaryGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 27, nodeMap[0] );
  BOOST_CHECK_EQUAL( 23, nodeMap[1] );

  qfldBoundaryGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 23, nodeMap[0] );
  BOOST_CHECK_EQUAL( 25, nodeMap[1] );

  qfldBoundaryGroup2.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 25, nodeMap[0] );
  BOOST_CHECK_EQUAL( 24, nodeMap[1] );

  qfldBoundaryGroup2.associativity(4).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 24, nodeMap[0] );
  BOOST_CHECK_EQUAL( 26, nodeMap[1] );

  qfldBoundaryGroup2.associativity(5).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 26, nodeMap[0] );
  BOOST_CHECK_EQUAL( 22, nodeMap[1] );

  qfldBoundaryGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 10, edgeMap[0] );
  BOOST_CHECK_EQUAL( 11, edgeMap[1] );

  qfldBoundaryGroup2.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 12, edgeMap[0] );
  BOOST_CHECK_EQUAL( 13, edgeMap[1] );

  qfldBoundaryGroup2.associativity(2).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 14, edgeMap[0] );
  BOOST_CHECK_EQUAL( 15, edgeMap[1] );

  qfldBoundaryGroup2.associativity(3).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 16, edgeMap[0] );
  BOOST_CHECK_EQUAL( 17, edgeMap[1] );

  qfldBoundaryGroup2.associativity(4).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 18, edgeMap[0] );
  BOOST_CHECK_EQUAL( 19, edgeMap[1] );

  qfldBoundaryGroup2.associativity(5).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 20, edgeMap[0] );
  BOOST_CHECK_EQUAL( 21, edgeMap[1] );


  // Test the constant assignment operator
  const QFieldAreaClass& qfld1Area = qfld1.getCellGroup<Quad>(0);
  ElementQFieldClass qfldElem(qfld1Area.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfld1Area.nElem(); elem++)
  {
    qfld1Area.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfld1Area.nElem(); elem++)
  {
    qfld1Area.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}
#endif

//----------------------------------------------------------------------------//
// check that solution trace is identical for adjacent elements
void CG_Area_CheckTrace(const int order, const BasisFunctionCategory category)
{
  typedef DLA::VectorS<2, Real> ArrayQ;

  typedef XField2D_2Quad_X1_1Group::FieldCellGroupType<Quad> XFieldAreaClass;
  typedef XField2D_2Quad_X1_1Group::FieldTraceGroupType<Line> XFieldLineClass;

  typedef XFieldAreaClass::ElementType<> ElementXFieldAreaClass;
  typedef XFieldLineClass::ElementType<> ElementXFieldLineClass;

  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldCellGroupType<Quad> QFieldAreaClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldAreaClass;


  const Real small_tol = 5e-12;
  const Real close_tol = 5e-10;

  XField2D_2Quad_X1_1Group xfld;

  const XFieldAreaClass& xfldArea = xfld.getCellGroup<Quad>(0);
  const XFieldLineClass& xfldEdge = xfld.getInteriorTraceGroup<Line>(0);

  QField2D_CG_Area qfld(xfld, order, category);

  const QFieldAreaClass& qfldArea = qfld.getCellGroup<Quad>(0);

  // initialize solution DOFs
  for (int k = 0; k < qfld.nDOF(); k++)
    qfld.DOF(k) = pow(-1, k) / sqrt(k+1);

  // element field variables
  ElementXFieldLineClass xfldElemEdge( xfldEdge.basis() );
  ElementXFieldAreaClass xfldElemL( xfldArea.basis() );
  ElementXFieldAreaClass xfldElemR( xfldArea.basis() );
  ElementQFieldAreaClass qfldElemL( qfldArea.basis() );
  ElementQFieldAreaClass qfldElemR( qfldArea.basis() );

  int nedge = xfldEdge.nElem();
  for (int edge = 0; edge < nedge; edge++)
  {
    int elemL = xfldEdge.getElementLeft( edge );
    int elemR = xfldEdge.getElementRight( edge );
    CanonicalTraceToCell canonicalEdgeL = xfldEdge.getCanonicalTraceLeft( edge );
    CanonicalTraceToCell canonicalEdgeR = xfldEdge.getCanonicalTraceRight( edge );

    // copy global grid/solution DOFs to element
    xfldEdge.getElement( xfldElemEdge, edge );
    xfldArea.getElement( xfldElemL, elemL );
    xfldArea.getElement( xfldElemR, elemR );
    qfldArea.getElement( qfldElemL, elemL );
    qfldArea.getElement( qfldElemR, elemR );

    int kmax = 5;
    for (int k = 0; k < kmax; k++)
    {
      Real sRefL, tRefL, sRefR, tRefR;
      Real sRef;
      ArrayQ qL, qR;

      sRef = k/static_cast<Real>(kmax-1);

      // left/right reference-element coords
      TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalEdgeL, sRef, sRefL, tRefL );
      TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalEdgeR, sRef, sRefR, tRefR );

      // solution trace from L/R elements
      qfldElemL.eval( sRefL, tRefL, qL );
      qfldElemR.eval( sRefR, tRefR, qR );

      SANS_CHECK_CLOSE( qL(0), qR(0), small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
// check that solution trace is identical for adjacent elements
BOOST_AUTO_TEST_CASE( CG_Area_CheckTrace_Hierarchical )
{
  for (int order = 1; order <= BasisFunctionArea_Quad_HierarchicalPMax; order++)
  {
    CG_Area_CheckTrace(order, BasisFunctionCategory_Hierarchical);
  }
}

//----------------------------------------------------------------------------//
// check that solution trace is identical for adjacent elements
BOOST_AUTO_TEST_CASE( CG_Area_CheckTrace_Lagrange )
{
  for (int order = 1; order <= BasisFunctionArea_Quad_LagrangePMax; order++)
  {
    CG_Area_CheckTrace(order, BasisFunctionCategory_Lagrange);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_Area_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldCellGroupType<Quad> QFieldAreaClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldClass;

  ArrayQ q0, q1;

  XField2D_4Quad_X1_1Group xfld1;

  for (int order = 1; order < BasisFunctionArea_Quad_HierarchicalPMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionArea_Quad_HierarchicalPMax-order; orderinc++)
    {
      QField2D_CG_Area qfldP  (xfld1, order, BasisFunctionCategory_Hierarchical);
      QField2D_CG_Area qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Hierarchical);

      QFieldAreaClass& qfldAeraP   = qfldP.getCellGroup<Quad>(0);
      QFieldAreaClass& qfldAeraPp1 = qfldPp1.getCellGroup<Quad>(0);

      ElementQFieldClass qfldElemP(qfldAeraP.basis());
      ElementQFieldClass qfldElemPp1(qfldAeraPp1.basis());

      //Give some non-zero initial condition
      for (int n = 0; n < qfldP.nDOF(); n++)
        qfldP.DOF(n) = n+1;

      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = 0;

      //Use area function projectTo
      qfldAeraP.projectTo(qfldAeraPp1);

      for (int elem = 0; elem < qfldAeraP.nElem(); elem++)
      {
        qfldAeraP.getElement(qfldElemP, elem);
        qfldAeraPp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, 0.25, q0);
        qfldElemPp1.eval(0.25, 0.25, q1);
        BOOST_CHECK_GT(q0[0], 1);
        BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      }

      //Wipe out DOF's for P1
      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = -1;

      //Use base function projectTo
      qfldP.projectTo(qfldPp1);

      for (int elem = 0; elem < qfldAeraP.nElem(); elem++)
      {
        qfldAeraP.getElement(qfldElemP, elem);
        qfldAeraPp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, 0.25, q0);
        qfldElemPp1.eval(0.25, 0.25, q1);
        BOOST_CHECK_GT(q0[0], 1);
        BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      }
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_InteriorEdge_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Quad> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int nodeMap[2];

  XField2D_4Quad_X1_1Group xfld1;

  int order = 1;
  Field_CG_InteriorTrace< PhysD2, TopoD2, ArrayQ > qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 5, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldGroup1 = qfld1.getInteriorTraceGroup<Line>(0);

  BOOST_REQUIRE_EQUAL( 4, qfldGroup1.nElem() );

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4, nodeMap[1] );

  qfldGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );

  qfldGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );


  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldGroup1.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_InteriorEdge_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Quad> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int nodeMap[2];
  int edgeMap[1];

  XField2D_4Quad_X1_1Group xfld1;

  int nEdgeDOF = 4;

  int order = 2;
  Field_CG_InteriorTrace<PhysD2, TopoD2, ArrayQ> qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 9, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldGroup1 = qfld1.getInteriorTraceGroup<Line>(0);

  BOOST_REQUIRE_EQUAL( 4, qfldGroup1.nElem() );

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2 + nEdgeDOF, nodeMap[1] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4 + nEdgeDOF, nodeMap[1] );

  qfldGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2 + nEdgeDOF, nodeMap[1] );

  qfldGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1 + nEdgeDOF, nodeMap[1] );

  qfldGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );

  qfldGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 3, edgeMap[0] );

  qfldGroup1.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );

  qfldGroup1.associativity(3).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 1, edgeMap[0] );

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );


  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldGroup1.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_InteriorEdge_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Quad> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  ArrayQ q1, q2;

  XField2D_4Quad_X1_1Group xfld1;

  for (int order = 1; order < BasisFunctionLine_HierarchicalPMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionLine_HierarchicalPMax-order; orderinc++)
    {
      Field_CG_InteriorTrace<PhysD2, TopoD2, ArrayQ> qfldP(xfld1, order, BasisFunctionCategory_Hierarchical);
      Field_CG_InteriorTrace<PhysD2, TopoD2, ArrayQ> qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Hierarchical);

      for ( int group = 0; group < qfldP.nInteriorTraceGroups(); group++ )
      {
        QFieldLineClass& qfldLineGroupP   = qfldP.getInteriorTraceGroup<Line>(group);
        QFieldLineClass& qfldLineGroupPp1 = qfldPp1.getInteriorTraceGroup<Line>(group);

        ElementQFieldLineClass qfldElemP(qfldLineGroupP.basis());
        ElementQFieldLineClass qfldElemPp1(qfldLineGroupPp1.basis());

        //Give some non-zero initial condition
        for (int n = 0; n < qfldP.nDOF(); n++)
          qfldP.DOF(n) = n+1;

        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = 0;

        //Use line function projectTo
        qfldLineGroupP.projectTo(qfldLineGroupPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.25, q1);
          qfldElemPp1.eval(0.25, q2);
          BOOST_CHECK_GT(q1[0], 1);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }

        //Wipe out DOF's for P1
        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = -1;

        //Use base function projectTo
        qfldP.projectTo(qfldPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.75, q1);
          qfldElemPp1.eval(0.75, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }
      }
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryEdge_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Quad> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int nodeMap[2];

  XField2D_2Quad_X1_1Group xfld1;

  int order = 1;
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 6, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldGroup1 = qfld1.getBoundaryTraceGroup<Line>(0);

  BOOST_REQUIRE_EQUAL( 6, qfldGroup1.nElem() );

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4, nodeMap[1] );

  qfldGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 4, nodeMap[0] );
  BOOST_CHECK_EQUAL( 5, nodeMap[1] );

  qfldGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 5, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );

  qfldGroup1.associativity(4).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3, nodeMap[1] );

  qfldGroup1.associativity(5).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0, nodeMap[1] );

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );


  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldGroup1.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryEdge_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Quad> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int nodeMap[2];
  int edgeMap[1];

  XField2D_2Quad_X1_1Group xfld1;

  int nEdgeDOF = 6;

  int order = 2;
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 12, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldGroup1 = qfld1.getBoundaryTraceGroup<Line>(0);

  BOOST_REQUIRE_EQUAL( 6, qfldGroup1.nElem() );

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1 + nEdgeDOF, nodeMap[1] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4 + nEdgeDOF, nodeMap[1] );

  qfldGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 4 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 5 + nEdgeDOF, nodeMap[1] );

  qfldGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 5 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2 + nEdgeDOF, nodeMap[1] );

  qfldGroup1.associativity(4).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3 + nEdgeDOF, nodeMap[1] );

  qfldGroup1.associativity(5).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0 + nEdgeDOF, nodeMap[1] );

  qfldGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );

  qfldGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );

  qfldGroup1.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 5, edgeMap[0] );

  qfldGroup1.associativity(3).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 4, edgeMap[0] );

  qfldGroup1.associativity(4).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 3, edgeMap[0] );

  qfldGroup1.associativity(5).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 1, edgeMap[0] );

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );


  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldGroup1.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryEdge_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Quad> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  ArrayQ q1, q2;

  XField2D_4Quad_X1_1Group xfld1;

  for (int order = 1; order < BasisFunctionLine_HierarchicalPMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionLine_HierarchicalPMax-order; orderinc++)
    {
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> qfldP(xfld1, order, BasisFunctionCategory_Hierarchical);
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Hierarchical);

      for ( int group = 0; group < qfldP.nBoundaryTraceGroups(); group++ )
      {
        QFieldLineClass& qfldLineGroupP   = qfldP.getBoundaryTraceGroup<Line>(group);
        QFieldLineClass& qfldLineGroupPp1 = qfldPp1.getBoundaryTraceGroup<Line>(group);

        ElementQFieldLineClass qfldElemP(qfldLineGroupP.basis());
        ElementQFieldLineClass qfldElemPp1(qfldLineGroupPp1.basis());

        //Give some non-zero initial condition
        for (int n = 0; n < qfldP.nDOF(); n++)
          qfldP.DOF(n) = n+1;

        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = 0;

        //Use line function projectTo
        qfldLineGroupP.projectTo(qfldLineGroupPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.25, q1);
          qfldElemPp1.eval(0.25, q2);
          BOOST_CHECK_GT( abs(q1[0]), 1e-12 );
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }

        //Wipe out DOF's for P1
        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = -1;

        //Use base function projectTo
        qfldP.projectTo(qfldPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.75, q1);
          qfldElemPp1.eval(0.75, q2);
          BOOST_CHECK_GT( abs(q1[0]), 1e-12 );
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }
      }
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryEdge_Independent_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Quad> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int nodeMap[2];

  XField2D_2Quad_X1_1Group xfld1;

  int order = 1;
  std::vector<std::vector<int>> boundaryGroupSets = {{0}};
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> qfld1(boundaryGroupSets, xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 6, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldGroup1 = qfld1.getBoundaryTraceGroup<Line>(0);

  BOOST_REQUIRE_EQUAL( 6, qfldGroup1.nElem() );

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4, nodeMap[1] );

  qfldGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 4, nodeMap[0] );
  BOOST_CHECK_EQUAL( 5, nodeMap[1] );

  qfldGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 5, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );

  qfldGroup1.associativity(4).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3, nodeMap[1] );

  qfldGroup1.associativity(5).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0, nodeMap[1] );

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );


  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldGroup1.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryEdge_Independent_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Quad> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int nodeMap[2];
  int edgeMap[1];

  XField2D_2Quad_X1_1Group xfld1;

  int order = 2;
  std::vector<std::vector<int>> boundaryGroupSets = {{0}};
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> qfld1(boundaryGroupSets, xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 12, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldGroup1 = qfld1.getBoundaryTraceGroup<Line>(0);

  BOOST_REQUIRE_EQUAL( 6, qfldGroup1.nElem() );

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL(  6, nodeMap[0] );
  BOOST_CHECK_EQUAL(  7, nodeMap[1] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL(  7, nodeMap[0] );
  BOOST_CHECK_EQUAL( 10, nodeMap[1] );

  qfldGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 10, nodeMap[0] );
  BOOST_CHECK_EQUAL( 11, nodeMap[1] );

  qfldGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 11, nodeMap[0] );
  BOOST_CHECK_EQUAL(  8, nodeMap[1] );

  qfldGroup1.associativity(4).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL(  8, nodeMap[0] );
  BOOST_CHECK_EQUAL(  9, nodeMap[1] );

  qfldGroup1.associativity(5).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL(  9, nodeMap[0] );
  BOOST_CHECK_EQUAL(  6, nodeMap[1] );

  qfldGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL(  0, edgeMap[0] );

  qfldGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL(  2, edgeMap[0] );

  qfldGroup1.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL(  5, edgeMap[0] );

  qfldGroup1.associativity(3).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL(  4, edgeMap[0] );

  qfldGroup1.associativity(4).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL(  3, edgeMap[0] );

  qfldGroup1.associativity(5).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL(  1, edgeMap[0] );

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );


  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldGroup1.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryEdge_Independent_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > QField2D_CG_BoundaryTrace_Independent;
  typedef QField2D_CG_BoundaryTrace_Independent::FieldTraceGroupType<Quad> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  ArrayQ q1, q2;

  std::vector<std::vector<int>> boundaryGroupSets = {{0}};

  XField2D_4Quad_X1_1Group xfld1;

  for (int order = 1; order < BasisFunctionLine_HierarchicalPMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionLine_HierarchicalPMax-order; orderinc++)
    {
      QField2D_CG_BoundaryTrace_Independent qfldP(boundaryGroupSets, xfld1, order, BasisFunctionCategory_Hierarchical);
      QField2D_CG_BoundaryTrace_Independent qfldPp1(boundaryGroupSets, xfld1, order+orderinc, BasisFunctionCategory_Hierarchical);

      for ( int group = 0; group < qfldP.nBoundaryTraceGroups(); group++ )
      {
        QFieldLineClass& qfldLineGroupP   = qfldP.getBoundaryTraceGroup<Line>(group);
        QFieldLineClass& qfldLineGroupPp1 = qfldPp1.getBoundaryTraceGroup<Line>(group);

        ElementQFieldLineClass qfldElemP(qfldLineGroupP.basis());
        ElementQFieldLineClass qfldElemPp1(qfldLineGroupPp1.basis());

        //Give some non-zero initial condition
        for (int n = 0; n < qfldP.nDOF(); n++)
          qfldP.DOF(n) = n+1;

        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = 0;

        //Use line function projectTo
        qfldLineGroupP.projectTo(qfldLineGroupPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.25, q1);
          qfldElemPp1.eval(0.25, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }

        //Wipe out DOF's for P1
        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = -1;

        //Use base function projectTo
        qfldP.projectTo(qfldPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.75, q1);
          qfldElemPp1.eval(0.75, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }
      }
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_TraceEdge_Hierarchical_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Quad> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int nodeMap[2];

  XField2D_4Quad_X1_1Group xfld1;

  int order = 1;
  Field_CG_Trace< PhysD2, TopoD2, ArrayQ > qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( 9, qfld1.nDOF() );
  BOOST_REQUIRE_EQUAL( 1, qfld1.nInteriorTraceGroups() );
  BOOST_REQUIRE_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  // check interior node maps
  const QFieldLineClass& qfldGroupI = qfld1.getInteriorTraceGroup<Line>(0);

  BOOST_REQUIRE_EQUAL( 4, qfldGroupI.nElem() );

  qfldGroupI.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4, nodeMap[1] );

  qfldGroupI.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 4, nodeMap[0] );
  BOOST_CHECK_EQUAL( 7, nodeMap[1] );

  qfldGroupI.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 5, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4, nodeMap[1] );

  qfldGroupI.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 4, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3, nodeMap[1] );

  // check boundary node maps
  const QFieldLineClass& qfldGroupB = qfld1.getBoundaryTraceGroup<Line>(0);

  BOOST_REQUIRE_EQUAL( 8, qfldGroupB.nElem() );

  qfldGroupB.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );

  qfldGroupB.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );

  qfldGroupB.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 5, nodeMap[1] );

  qfldGroupB.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 5, nodeMap[0] );
  BOOST_CHECK_EQUAL( 8, nodeMap[1] );

  qfldGroupB.associativity(4).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 8, nodeMap[0] );
  BOOST_CHECK_EQUAL( 7, nodeMap[1] );

  qfldGroupB.associativity(5).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 7, nodeMap[0] );
  BOOST_CHECK_EQUAL( 6, nodeMap[1] );

  qfldGroupB.associativity(6).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 6, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3, nodeMap[1] );

  qfldGroupB.associativity(7).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0, nodeMap[1] );


  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );


  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldGroupI.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroupI.nElem(); elem++)
  {
    qfldGroupI.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  for (int elem = 0; elem < qfldGroupB.nElem(); elem++)
  {
    qfldGroupB.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroupI.nElem(); elem++)
  {
    qfldGroupI.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }

  for (int elem = 0; elem < qfldGroupB.nElem(); elem++)
  {
    qfldGroupB.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_TraceEdge_Hierarchical_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Quad> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int nodeMap[2];
  int edgeMap[1];

  XField2D_4Quad_X1_1Group xfld1;

  int nEdgeDOF = 12;

  int order = 2;
  Field_CG_Trace< PhysD2, TopoD2, ArrayQ > qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( 21, qfld1.nDOF() );
  BOOST_REQUIRE_EQUAL( 1, qfld1.nInteriorTraceGroups() );
  BOOST_REQUIRE_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  // check interior node maps
  const QFieldLineClass& qfldGroupI = qfld1.getInteriorTraceGroup<Line>(0);

  BOOST_REQUIRE_EQUAL( 4, qfldGroupI.nElem() );

  qfldGroupI.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4 + nEdgeDOF, nodeMap[1] );

  qfldGroupI.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 4 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 7 + nEdgeDOF, nodeMap[1] );

  qfldGroupI.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 5 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4 + nEdgeDOF, nodeMap[1] );

  qfldGroupI.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 4 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3 + nEdgeDOF, nodeMap[1] );

  // check boundary node maps
  const QFieldLineClass& qfldGroupB = qfld1.getBoundaryTraceGroup<Line>(0);

  BOOST_REQUIRE_EQUAL( 8, qfldGroupB.nElem() );

  qfldGroupB.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 5 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 5 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 8 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(4).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 8 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 7 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(5).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 7 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 6 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(6).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 6 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(7).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0 + nEdgeDOF, nodeMap[1] );

  // check interior edge maps

  qfldGroupI.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL(  3, edgeMap[0] );

  qfldGroupI.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL(  8, edgeMap[0] );

  qfldGroupI.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL(  7, edgeMap[0] );

  qfldGroupI.associativity(3).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL(  5, edgeMap[0] );

  // check boundary edge maps

  qfldGroupB.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL(  0, edgeMap[0] );

  qfldGroupB.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL(  2, edgeMap[0] );

  qfldGroupB.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL(  4, edgeMap[0] );

  qfldGroupB.associativity(3).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL(  9, edgeMap[0] );

  qfldGroupB.associativity(4).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 11, edgeMap[0] );

  qfldGroupB.associativity(5).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 10, edgeMap[0] );

  qfldGroupB.associativity(6).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL(  6, edgeMap[0] );

  qfldGroupB.associativity(7).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL(  1, edgeMap[0] );


  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );


  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldGroupI.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroupI.nElem(); elem++)
  {
    qfldGroupI.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  for (int elem = 0; elem < qfldGroupB.nElem(); elem++)
  {
    qfldGroupB.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroupI.nElem(); elem++)
  {
    qfldGroupI.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }

  for (int elem = 0; elem < qfldGroupB.nElem(); elem++)
  {
    qfldGroupB.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_TraceEdge_Hierarchical_P3 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Quad> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int nodeMap[2];
  int edgeMap[2];

  XField2D_4Quad_X1_1Group xfld1;

  int nEdgeDOF = 12*2;

  int order = 3;
  Field_CG_Trace< PhysD2, TopoD2, ArrayQ > qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( 33, qfld1.nDOF() );
  BOOST_REQUIRE_EQUAL( 1, qfld1.nInteriorTraceGroups() );
  BOOST_REQUIRE_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  // check interior node maps
  const QFieldLineClass& qfldGroupI = qfld1.getInteriorTraceGroup<Line>(0);

  BOOST_REQUIRE_EQUAL( 4, qfldGroupI.nElem() );

  qfldGroupI.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4 + nEdgeDOF, nodeMap[1] );

  qfldGroupI.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 4 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 7 + nEdgeDOF, nodeMap[1] );

  qfldGroupI.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 5 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4 + nEdgeDOF, nodeMap[1] );

  qfldGroupI.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 4 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3 + nEdgeDOF, nodeMap[1] );

  // check boundary node maps
  const QFieldLineClass& qfldGroupB = qfld1.getBoundaryTraceGroup<Line>(0);

  BOOST_REQUIRE_EQUAL( 8, qfldGroupB.nElem() );

  qfldGroupB.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 5 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 5 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 8 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(4).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 8 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 7 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(5).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 7 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 6 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(6).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 6 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(7).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0 + nEdgeDOF, nodeMap[1] );

  // check interior edge maps

  qfldGroupI.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL(  6, edgeMap[0] );
  BOOST_CHECK_EQUAL(  7, edgeMap[1] );

  qfldGroupI.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 16, edgeMap[0] );
  BOOST_CHECK_EQUAL( 17, edgeMap[1] );

  qfldGroupI.associativity(2).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 14, edgeMap[0] );
  BOOST_CHECK_EQUAL( 15, edgeMap[1] );

  qfldGroupI.associativity(3).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 10, edgeMap[0] );
  BOOST_CHECK_EQUAL( 11, edgeMap[1] );

  // check boundary edge maps
  qfldGroupB.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );
  BOOST_CHECK_EQUAL( 1, edgeMap[1] );

  qfldGroupB.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 4, edgeMap[0] );
  BOOST_CHECK_EQUAL( 5, edgeMap[1] );

  qfldGroupB.associativity(2).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 8, edgeMap[0] );
  BOOST_CHECK_EQUAL( 9, edgeMap[1] );

  qfldGroupB.associativity(3).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 18, edgeMap[0] );
  BOOST_CHECK_EQUAL( 19, edgeMap[1] );

  qfldGroupB.associativity(4).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 22, edgeMap[0] );
  BOOST_CHECK_EQUAL( 23, edgeMap[1] );

  qfldGroupB.associativity(5).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 20, edgeMap[0] );
  BOOST_CHECK_EQUAL( 21, edgeMap[1] );

  qfldGroupB.associativity(6).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 12, edgeMap[0] );
  BOOST_CHECK_EQUAL( 13, edgeMap[1] );

  qfldGroupB.associativity(7).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL(  2, edgeMap[0] );
  BOOST_CHECK_EQUAL(  3, edgeMap[1] );


  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );


  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldGroupI.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroupI.nElem(); elem++)
  {
    qfldGroupI.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  for (int elem = 0; elem < qfldGroupB.nElem(); elem++)
  {
    qfldGroupB.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroupI.nElem(); elem++)
  {
    qfldGroupI.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }

  for (int elem = 0; elem < qfldGroupB.nElem(); elem++)
  {
    qfldGroupB.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_TraceEdge_Hierarchical_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Quad> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  ArrayQ q1, q2;

  XField2D_4Quad_X1_1Group xfld1;

  for (int order = 1; order < BasisFunctionLine_HierarchicalPMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionLine_HierarchicalPMax-order; orderinc++)
    {
      Field_CG_Trace<PhysD2, TopoD2, ArrayQ> qfldP(xfld1, order, BasisFunctionCategory_Hierarchical);
      Field_CG_Trace<PhysD2, TopoD2, ArrayQ> qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Hierarchical);

      //Give some non-zero initial condition
      for (int n = 0; n < qfldP.nDOF(); n++)
        qfldP.DOF(n) = n+1;

      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = 0;

      //Use base function projectTo
      qfldP.projectTo(qfldPp1);

      for ( int group = 0; group < qfldP.nInteriorTraceGroups(); group++ )
      {
        QFieldLineClass& qfldLineGroupP   = qfldP.getInteriorTraceGroup<Line>(group);
        QFieldLineClass& qfldLineGroupPp1 = qfldPp1.getInteriorTraceGroup<Line>(group);

        ElementQFieldLineClass qfldElemP(qfldLineGroupP.basis());
        ElementQFieldLineClass qfldElemPp1(qfldLineGroupPp1.basis());

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.75, q1);
          qfldElemPp1.eval(0.75, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }
      }

      for ( int group = 0; group < qfldP.nBoundaryTraceGroups(); group++ )
      {
        QFieldLineClass& qfldLineGroupP   = qfldP.getBoundaryTraceGroup<Line>(group);
        QFieldLineClass& qfldLineGroupPp1 = qfldPp1.getBoundaryTraceGroup<Line>(group);

        ElementQFieldLineClass qfldElemP(qfldLineGroupP.basis());
        ElementQFieldLineClass qfldElemPp1(qfldLineGroupPp1.basis());

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.75, q1);
          qfldElemPp1.eval(0.75, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_Exception )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef Field_CG_Trace< PhysD2, TopoD2, ArrayQ > QField2D_CG_Edge;
  typedef Field_CG_InteriorTrace< PhysD2, TopoD2, ArrayQ > QField2D_CG_InteriorTrace;
  typedef Field_CG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > QField2D_CG_BoundaryTrace;


  XField2D_2Quad_X1_1Group xfld1;

  int orderCell = BasisFunctionArea_Quad_HierarchicalPMax+1; //This should always higher than the maximum available order
  int orderEdge = BasisFunctionLine_HierarchicalPMax+1; //This should always higher than the maximum available order
  BOOST_CHECK_THROW( QField2D_CG_Area qfld1(xfld1, orderCell, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( QField2D_CG_Edge qfld1(xfld1, orderEdge, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( QField2D_CG_InteriorTrace qfld1(xfld1, orderEdge, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( QField2D_CG_BoundaryTrace qfld1(xfld1, orderEdge, BasisFunctionCategory_Hierarchical), DeveloperException );

  // Can't use Legendre basis functions
  orderCell = 1;
  orderEdge = 1;
  BOOST_CHECK_THROW( QField2D_CG_Area qfld1(xfld1, orderCell, BasisFunctionCategory_Legendre), AssertionException );
  BOOST_CHECK_THROW( QField2D_CG_Edge qfld1(xfld1, orderEdge, BasisFunctionCategory_Legendre), AssertionException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef Field_CG_Trace< PhysD2, TopoD2, ArrayQ > QField2D_CG_Edge;
  typedef Field_CG_InteriorTrace< PhysD2, TopoD2, ArrayQ > QField2D_CG_InteriorTrace;
  typedef Field_CG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > QField2D_CG_BoundaryTrace;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/Field2D_CG_Quad_pattern.txt", true );

  XField2D_2Quad_X1_1Group xfld;

  QField2D_CG_Area qfld1(xfld, 1, BasisFunctionCategory_Hierarchical);
  qfld1 = 0;
  qfld1.dump( 2, output );

  QField2D_CG_Edge qfld2(xfld, 1, BasisFunctionCategory_Hierarchical);
  qfld2 = 0;
  qfld2.dump( 2, output );

  QField2D_CG_InteriorTrace qfld3(xfld, 2, BasisFunctionCategory_Hierarchical);
  qfld3 = 0;
  qfld3.dump( 2, output );

  QField2D_CG_BoundaryTrace qfld4(xfld, 2, BasisFunctionCategory_Hierarchical);
  qfld4 = 0;
  qfld4.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
