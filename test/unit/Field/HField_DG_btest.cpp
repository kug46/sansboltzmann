// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// HField_1DLine_btest
// testing of 1-D HDG with Burgers

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Field/HField/HFieldLine_DG.h"
#include "Field/HField/HFieldArea_DG.h"
#include "Field/HField/HFieldVolume_DG.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( HField_DG_test_suite )

BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK( (HField_DG<PhysD1,TopoD1>::D == 1) );
  BOOST_CHECK( (HField_DG<PhysD2,TopoD2>::D == 2) );
  BOOST_CHECK( (HField_DG<PhysD3,TopoD3>::D == 3) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_grid_test )
{
  // grid
  XField1D xfld( 64 );

  typedef HField_DG<PhysD1,TopoD1>::FieldCellGroupType<Line> HFieldLineClass;
  typedef HFieldLineClass::ElementType<> ElementHFieldLineClass;

  HField_DG<PhysD1,TopoD1> hfld( xfld );

  Real perimeter = 1. + 1.;
  Real length = 1.0/64.0;
  Real exactSize = length/perimeter;

  Real h;
  HFieldLineClass& hfld1Line = hfld.getCellGroup<Line>(0);
  ElementHFieldLineClass hfldElem(hfld1Line.basis());

  for (int elem = 0; elem < hfld1Line.nElem(); elem++)
  {
    hfld1Line.getElement(hfldElem, elem);

    hfldElem.eval(0.25, h);
    BOOST_CHECK_CLOSE(h, exactSize, 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Area_grid_test )
{
  // grid
  XField2D_4Triangle_X1_1Group xfld;

  typedef HField_DG<PhysD2,TopoD2>::FieldCellGroupType<Triangle> HFieldTriangleClass;
  typedef HFieldTriangleClass::ElementType<> ElementHFieldTriangleClass;

  HField_DG<PhysD2,TopoD2> hfld( xfld );

  Real perimeter = 1. + 1. + sqrt(2.);
  Real area = 0.5;
  Real exactSize = area/perimeter;

  Real h;
  HFieldTriangleClass& hfld1Triangle = hfld.getCellGroup<Triangle>(0);
  ElementHFieldTriangleClass hfldElem(hfld1Triangle.basis());

  for (int elem = 0; elem < hfld1Triangle.nElem(); elem++)
  {
    hfld1Triangle.getElement(hfldElem, elem);

    hfldElem.eval(1./3., 1./3., h);
    BOOST_CHECK_CLOSE(h, exactSize, 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Volume_grid_test )
{
  // grid
  XField3D_6Tet_X1_1Group xfld;

  typedef HField_DG<PhysD3,TopoD3>::FieldCellGroupType<Tet> HFieldTetClass;
  typedef HFieldTetClass::ElementType<> ElementHFieldTetClass;

  HField_DG<PhysD3,TopoD3> hfld( xfld );

  Real perimeter0 = 3*0.5 + sqrt(2.)*sqrt(3./2.)/2.;
  Real perimeter1 = 0.5 + sqrt(2.)*sqrt(3./2.)/2. + sqrt(2.)/2. + sqrt(2.)/2.;
  Real perimeter2 = 0.5 + 0.5 + sqrt(2.)/2. + sqrt(2.)/2.;

  Real volume = 1./6.;

  Real exactSize0 = volume/perimeter0;
  Real exactSize1 = volume/perimeter1;
  Real exactSize2 = volume/perimeter2;

  Real h;
  HFieldTetClass& hfld1Tet = hfld.getCellGroup<Tet>(0);
  ElementHFieldTetClass hfldElem(hfld1Tet.basis());

  hfld1Tet.getElement(hfldElem, 0);
  hfldElem.eval(1./3., 1./3., 1./3., h);
  BOOST_CHECK_CLOSE(h, exactSize0, 1e-12);

  hfld1Tet.getElement(hfldElem, 1);
  hfldElem.eval(1./3., 1./3., 1./3., h);
  BOOST_CHECK_CLOSE(h, exactSize1, 1e-12);

  hfld1Tet.getElement(hfldElem, 2);
  hfldElem.eval(1./3., 1./3., 1./3., h);
  BOOST_CHECK_CLOSE(h, exactSize2, 1e-12);


  hfld1Tet.getElement(hfldElem, 3);
  hfldElem.eval(1./3., 1./3., 1./3., h);
  BOOST_CHECK_CLOSE(h, exactSize2, 1e-12);

  hfld1Tet.getElement(hfldElem, 4);
  hfldElem.eval(1./3., 1./3., 1./3., h);
  BOOST_CHECK_CLOSE(h, exactSize1, 1e-12);

  hfld1Tet.getElement(hfldElem, 5);
  hfldElem.eval(1./3., 1./3., 1./3., h);
  BOOST_CHECK_CLOSE(h, exactSize0, 1e-12);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
