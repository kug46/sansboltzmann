// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ProjectSolnCell_Galerkin_btest
// testing of cell solution projection for Galerkin

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real


#include "Field/Tuple/FieldTuple.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_InteriorTrace.h"

#include "Field/tools/for_each_InteriorFieldTraceGroup_Cell.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Quad_X1_1Group.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Hex_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( for_each_InteriorFieldTraceGroup_Cell_test_suite )

// Class for testing field distribution, taken from ErrorEstimate_HDG
  template<class PhysDim_>
  class DistributeTraceField:
      public GroupFunctorInteriorTraceType<DistributeTraceField<PhysDim_>>
  {
  public:
    typedef PhysDim_ PhysDim;

    explicit DistributeTraceField( const std::vector<int>& interiorTraceGroups ) :
    interiorTraceGroups_(interiorTraceGroups) {}

    std::size_t nInteriorTraceGroups() const { return interiorTraceGroups_.size(); }
    std::size_t interiorTraceGroup(const int n) const { return interiorTraceGroups_[n]; }

    //----------------------------------------------------------------------------//
    // Distribution function that redistributes the error in each cell group
    template< class TopologyTrace, class TopologyL, class TopologyR>
    void
    apply( const typename Field<PhysDim, typename TopologyL::TopoDim, Real>::template FieldCellGroupType<TopologyL>& efldL,
           const typename Field<PhysDim, typename TopologyR::TopoDim, Real>::template FieldCellGroupType<TopologyR>& efldR,
           const typename Field<PhysDim, typename TopologyTrace::CellTopoDim, Real>::template FieldTraceGroupType<TopologyTrace>& eIfld,
           const typename XField<PhysDim, typename TopologyTrace::CellTopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
           const int traceGroupGlobal)
    {
      // Cell Group Types
      typedef typename Field< PhysDim,typename TopologyL::TopoDim, Real>::template FieldCellGroupType<TopologyL> ELFieldCellGroupType;
      typedef typename Field< PhysDim,typename TopologyR::TopoDim, Real>::template FieldCellGroupType<TopologyR> ERFieldCellGroupType;
      typedef typename Field< PhysDim,typename TopologyTrace::CellTopoDim, Real>::template FieldTraceGroupType<TopologyTrace> EIFieldCellGroupType;

      typedef typename ELFieldCellGroupType::template ElementType<> ElementELFieldClass;
      typedef typename ERFieldCellGroupType::template ElementType<> ElementERFieldClass;
      typedef typename EIFieldCellGroupType::template ElementType<> ElementEIFieldClass;

      // Apportioning the eIfld to efld
      ElementELFieldClass efldElemL(efldL.basis() );
      ElementERFieldClass efldElemR(efldR.basis() );

      ElementEIFieldClass eIfldElem(eIfld.basis() );

      // loop over elements
      for (int elem = 0; elem<eIfld.nElem();elem++)
      {
        int elemL = xfldTrace.getElementLeft( elem );
        int elemR = xfldTrace.getElementRight( elem );

        eIfld.getElement(eIfldElem, elem );

        efldL.getElement( efldElemL, elemL );
        efldElemL.DOF(0) += 0.5*eIfldElem.DOF(0);
        const_cast<ELFieldCellGroupType&>(efldL).setElement( efldElemL, elemL );

        efldR.getElement( efldElemR, elemR );
        efldElemR.DOF(0) += 0.5*eIfldElem.DOF(0);
        const_cast<ERFieldCellGroupType&>(efldR).setElement( efldElemR, elemR );
      }
    }
  protected:
    const std::vector<int> interiorTraceGroups_;
  };

// Class for testing field distribution, taken from ErrorEstimate_HDG, with added Tupling
  template<class PhysDim_>
  class DistributeTraceField_Tuple:
      public GroupFunctorInteriorTraceType<DistributeTraceField_Tuple<PhysDim_>>
  {
  public:
    typedef PhysDim_ PhysDim;

    explicit DistributeTraceField_Tuple( const std::vector<int>& interiorTraceGroups ) :
    interiorTraceGroups_(interiorTraceGroups) {}

    std::size_t nInteriorTraceGroups() const { return interiorTraceGroups_.size(); }
    std::size_t interiorTraceGroup(const int n) const { return interiorTraceGroups_[n]; }

    //----------------------------------------------------------------------------//
    // Distribution function that redistributes the error in each cell group
    template< class TopologyTrace, class TopologyL, class TopologyR>
    void
    apply( const typename FieldTuple<XField<PhysDim, typename TopologyL::TopoDim>,
                               Field<PhysDim, typename TopologyL::TopoDim, Real>, TupleClass<>>::template FieldCellGroupType<TopologyL>& fldsL,
           const typename FieldTuple<XField<PhysDim, typename TopologyR::TopoDim>,
                               Field<PhysDim, typename TopologyR::TopoDim, Real>, TupleClass<>>::template FieldCellGroupType<TopologyR>& fldsR,
           const typename Field<PhysDim, typename TopologyTrace::CellTopoDim, Real>::template FieldTraceGroupType<TopologyTrace>& eIfld,
           const typename XField<PhysDim, typename TopologyTrace::CellTopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
           const int traceGroupGlobal)
    {
      // Cell Group Types
      typedef typename Field< PhysDim,typename TopologyL::TopoDim, Real>::template FieldCellGroupType<TopologyL> ELFieldCellGroupType;
      typedef typename Field< PhysDim,typename TopologyR::TopoDim, Real>::template FieldCellGroupType<TopologyR> ERFieldCellGroupType;
      typedef typename Field< PhysDim,typename TopologyTrace::CellTopoDim, Real>::template FieldTraceGroupType<TopologyTrace> EIFieldCellGroupType;

      typedef typename ELFieldCellGroupType::template ElementType<> ElementELFieldClass;
      typedef typename ERFieldCellGroupType::template ElementType<> ElementERFieldClass;
      typedef typename EIFieldCellGroupType::template ElementType<> ElementEIFieldClass;

      ELFieldCellGroupType efldL = get<-1>(fldsL);
      ERFieldCellGroupType efldR = get<-1>(fldsR);

      // Apportioning the eIfld to efld
      ElementELFieldClass efldElemL(efldL.basis() );
      ElementERFieldClass efldElemR(efldR.basis() );

      ElementEIFieldClass eIfldElem(eIfld.basis() );

      // loop over elements
      for (int elem = 0; elem<eIfld.nElem();elem++)
      {
        int elemL = xfldTrace.getElementLeft( elem );
        int elemR = xfldTrace.getElementRight( elem );

        eIfld.getElement(eIfldElem, elem );

        efldL.getElement( efldElemL, elemL );
        efldElemL.DOF(0) += 0.5*eIfldElem.DOF(0);
        const_cast<ELFieldCellGroupType&>(efldL).setElement( efldElemL, elemL );

        efldR.getElement( efldElemR, elemR );
        efldElemR.DOF(0) += 0.5*eIfldElem.DOF(0);
        const_cast<ERFieldCellGroupType&>(efldR).setElement( efldElemR, elemR );
      }
    }
  protected:
    const std::vector<int> interiorTraceGroups_;
  };




//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistributeTraceField_1D_Line )
{
  // grid: three lines, P1 (aka X1)
  XField1D xfld(3);

  // solution: P1 (aka Q1), which spans the space of the Linear solution

  Field_DG_Cell<PhysD1, TopoD1, Real> efld( xfld, 0, BasisFunctionCategory_Legendre );
  Field_DG_InteriorTrace<PhysD1, TopoD1, Real> eIfld( xfld, 0, BasisFunctionCategory_Legendre );

  efld = 0;
  eIfld = 1;

  for_each_InteriorFieldTraceGroup_Cell<TopoD1>::apply( DistributeTraceField<PhysD1>({0}), efld, eIfld );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Field<PhysD1, TopoD1, Real>::template FieldCellGroupType<Line>& efldCell = efld.getCellGroup<Line>(0);
  typedef Field<PhysD1, TopoD1, Real>::template FieldCellGroupType<Line>::template ElementType<> ElementEFieldClass;

  // element field variables
  ElementEFieldClass efldElem( efldCell.basis() );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );

    if ( ( elem == 0) || (elem == 2) )
    {
      SANS_CHECK_CLOSE( 0.5, efldElem.DOF(0), small_tol, close_tol )
    }
    else
    {
      SANS_CHECK_CLOSE(   1, efldElem.DOF(0), small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistributeTraceField_1D_Line_Tuple )
{
  // grid: three lines, P1 (aka X1)
  XField1D xfld(3);

  // solution: P1 (aka Q1), which spans the space of the Linear solution

  Field_DG_Cell<PhysD1, TopoD1, Real> efld( xfld, 0, BasisFunctionCategory_Legendre );
  Field_DG_InteriorTrace<PhysD1, TopoD1, Real> eIfld( xfld, 0, BasisFunctionCategory_Legendre );

  efld = 0;
  eIfld = 1;

  for_each_InteriorFieldTraceGroup_Cell<TopoD1>::apply( DistributeTraceField_Tuple<PhysD1>({0}), (xfld, efld), eIfld );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Field<PhysD1, TopoD1, Real>::template FieldCellGroupType<Line>& efldCell = efld.getCellGroup<Line>(0);
  typedef Field<PhysD1, TopoD1, Real>::template FieldCellGroupType<Line>::template ElementType<> ElementEFieldClass;

  // element field variables
  ElementEFieldClass efldElem( efldCell.basis() );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );

    if ( ( elem == 0) || (elem == 2) )
    {
      SANS_CHECK_CLOSE( 0.5, efldElem.DOF(0), small_tol, close_tol )
    }
    else
    {
      SANS_CHECK_CLOSE( 1.0, efldElem.DOF(0), small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistributeTraceField_2D_Triangle )
{
  // grid: four triangles, P1 (aka X1)
  XField2D_4Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1), which spans the space of the Linear solution

  Field_DG_Cell<PhysD2, TopoD2, Real> efld( xfld, 0, BasisFunctionCategory_Legendre );
  Field_DG_InteriorTrace<PhysD2, TopoD2, Real> eIfld( xfld, 0, BasisFunctionCategory_Legendre );

  efld = 0;
  eIfld = 1;

  for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply( DistributeTraceField<PhysD2>({0}), efld, eIfld );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Field<PhysD2, TopoD2, Real>::template FieldCellGroupType<Triangle>& efldCell = efld.getCellGroup<Triangle>(0);
  typedef Field<PhysD2, TopoD2, Real>::template FieldCellGroupType<Triangle>::template ElementType<> ElementEFieldClass;

  // element field variables
  ElementEFieldClass efldElem( efldCell.basis() );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );

    if ( elem == 0 )
    {
      SANS_CHECK_CLOSE( 1.5, efldElem.DOF(0), small_tol, close_tol );
    }
    else
    {
      SANS_CHECK_CLOSE( 0.5, efldElem.DOF(0), small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistributeTraceField_2D_Triangle_Tuple )
{
  // grid: four triangles, P1 (aka X1)
  XField2D_4Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1), which spans the space of the Linear solution

  Field_DG_Cell<PhysD2, TopoD2, Real> efld( xfld, 0, BasisFunctionCategory_Legendre );
  Field_DG_InteriorTrace<PhysD2, TopoD2, Real> eIfld( xfld, 0, BasisFunctionCategory_Legendre );

  efld = 0;
  eIfld = 1;

  for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply( DistributeTraceField_Tuple<PhysD2>({0}), (xfld, efld), eIfld );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Field<PhysD2, TopoD2, Real>::template FieldCellGroupType<Triangle>& efldCell = efld.getCellGroup<Triangle>(0);
  typedef Field<PhysD2, TopoD2, Real>::template FieldCellGroupType<Triangle>::template ElementType<> ElementEFieldClass;

  // element field variables
  ElementEFieldClass efldElem( efldCell.basis() );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );

    if ( elem == 0 )
    {
      SANS_CHECK_CLOSE( 1.5, efldElem.DOF(0), small_tol, close_tol );
    }
    else
    {
      SANS_CHECK_CLOSE( 0.5, efldElem.DOF(0), small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistributeTraceField_2D_Quad )
{
  // grid: four triangles, P1 (aka X1)
  XField2D_4Quad_X1_1Group xfld;

  // solution: P1 (aka Q1), which spans the space of the Linear solution

  Field_DG_Cell<PhysD2, TopoD2, Real> efld( xfld, 0, BasisFunctionCategory_Legendre );
  Field_DG_InteriorTrace<PhysD2, TopoD2, Real> eIfld( xfld, 0, BasisFunctionCategory_Legendre );

  efld = 0;
  eIfld = 1;

  for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply( DistributeTraceField<PhysD2>({0}), efld, eIfld );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Field<PhysD2, TopoD2, Real>::template FieldCellGroupType<Quad>& efldCell = efld.getCellGroup<Quad>(0);
  typedef Field<PhysD2, TopoD2, Real>::template FieldCellGroupType<Quad>::template ElementType<> ElementEFieldClass;

  // element field variables
  ElementEFieldClass efldElem( efldCell.basis() );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );

    SANS_CHECK_CLOSE( 1, efldElem.DOF(0), small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistributeTraceField_2D_Quad_Tuple )
{
  // grid: four triangles, P1 (aka X1)
  XField2D_4Quad_X1_1Group xfld;

  // solution: P1 (aka Q1), which spans the space of the Linear solution

  Field_DG_Cell<PhysD2, TopoD2, Real> efld( xfld, 0, BasisFunctionCategory_Legendre );
  Field_DG_InteriorTrace<PhysD2, TopoD2, Real> eIfld( xfld, 0, BasisFunctionCategory_Legendre );

  efld = 0;
  eIfld = 1;

  for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply( DistributeTraceField_Tuple<PhysD2>({0}), (xfld, efld), eIfld );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Field<PhysD2, TopoD2, Real>::template FieldCellGroupType<Quad>& efldCell = efld.getCellGroup<Quad>(0);
  typedef Field<PhysD2, TopoD2, Real>::template FieldCellGroupType<Quad>::template ElementType<> ElementEFieldClass;

  // element field variables
  ElementEFieldClass efldElem( efldCell.basis() );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );

    SANS_CHECK_CLOSE( 1, efldElem.DOF(0), small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistributeTraceField_3D_Tet )
{
  // grid: four triangles, P1 (aka X1)
  XField3D_6Tet_X1_1Group xfld;

  // solution: P1 (aka Q1), which spans the space of the Linear solution

  Field_DG_Cell<PhysD3, TopoD3, Real> efld( xfld, 0, BasisFunctionCategory_Legendre );
  Field_DG_InteriorTrace<PhysD3, TopoD3, Real> eIfld( xfld, 0, BasisFunctionCategory_Legendre );

  efld = 0;
  eIfld = 1;

  for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply( DistributeTraceField<PhysD3>({0}), efld, eIfld );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Field<PhysD3, TopoD3, Real>::template FieldCellGroupType<Tet>& efldCell = efld.getCellGroup<Tet>(0);
  typedef Field<PhysD3, TopoD3, Real>::template FieldCellGroupType<Tet>::template ElementType<> ElementEFieldClass;

  // element field variables
  ElementEFieldClass efldElem( efldCell.basis() );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );
    if ((elem == 0) || (elem == 5))
      {SANS_CHECK_CLOSE( 0.5, efldElem.DOF(0), small_tol, close_tol );}
    if ( (elem == 1) || (elem == 4) )
      {SANS_CHECK_CLOSE( 1.5, efldElem.DOF(0), small_tol, close_tol );}
    if ( (elem == 3) || (elem == 2) )
      {SANS_CHECK_CLOSE( 1.0, efldElem.DOF(0), small_tol, close_tol );}
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistributeTraceField_3D_Tet_Tuple )
{
  // grid: four triangles, P1 (aka X1)
  XField3D_6Tet_X1_1Group xfld;

  // solution: P1 (aka Q1), which spans the space of the Linear solution

  Field_DG_Cell<PhysD3, TopoD3, Real> efld( xfld, 0, BasisFunctionCategory_Legendre );
  Field_DG_InteriorTrace<PhysD3, TopoD3, Real> eIfld( xfld, 0, BasisFunctionCategory_Legendre );

  efld = 0;
  eIfld = 1;

  for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply( DistributeTraceField_Tuple<PhysD3>({0}), (xfld, efld) , eIfld );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Field<PhysD3, TopoD3, Real>::template FieldCellGroupType<Tet>& efldCell = efld.getCellGroup<Tet>(0);
  typedef Field<PhysD3, TopoD3, Real>::template FieldCellGroupType<Tet>::template ElementType<> ElementEFieldClass;

  // element field variables
  ElementEFieldClass efldElem( efldCell.basis() );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );
    if ((elem == 0) || (elem == 5))
      {SANS_CHECK_CLOSE( 0.5, efldElem.DOF(0), small_tol, close_tol );}
    if ( (elem == 1) || (elem == 4) )
      {SANS_CHECK_CLOSE( 1.5, efldElem.DOF(0), small_tol, close_tol );}
    if ( (elem == 3) || (elem == 2) )
      {SANS_CHECK_CLOSE( 1.0, efldElem.DOF(0), small_tol, close_tol );}
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistributeTraceField_3D_Hex )
{
  // grid: four triangles, P1 (aka X1)
  XField3D_2Hex_X1_1Group xfld;

  // solution: P1 (aka Q1), which spans the space of the Linear solution

  Field_DG_Cell<PhysD3, TopoD3, Real> efld( xfld, 0, BasisFunctionCategory_Legendre );
  Field_DG_InteriorTrace<PhysD3, TopoD3, Real> eIfld( xfld, 0, BasisFunctionCategory_Legendre );

  efld = 0;
  eIfld = 1;

  for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply( DistributeTraceField<PhysD3>({0}), efld, eIfld );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Field<PhysD3, TopoD3, Real>::template FieldCellGroupType<Hex>& efldCell = efld.getCellGroup<Hex>(0);
  typedef Field<PhysD3, TopoD3, Real>::template FieldCellGroupType<Hex>::template ElementType<> ElementEFieldClass;

  // element field variables
  ElementEFieldClass efldElem( efldCell.basis() );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );
    SANS_CHECK_CLOSE( 0.5, efldElem.DOF(0), small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistributeTraceField_3D_Hex_Tuple )
{
  // grid: four triangles, P1 (aka X1)
  XField3D_2Hex_X1_1Group xfld;

  // solution: P1 (aka Q1), which spans the space of the Linear solution

  Field_DG_Cell<PhysD3, TopoD3, Real> efld( xfld, 0, BasisFunctionCategory_Legendre );
  Field_DG_InteriorTrace<PhysD3, TopoD3, Real> eIfld( xfld, 0, BasisFunctionCategory_Legendre );

  efld = 0;
  eIfld = 1;

  for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply( DistributeTraceField_Tuple<PhysD3>({0}), (xfld, efld) , eIfld );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Field<PhysD3, TopoD3, Real>::template FieldCellGroupType<Hex>& efldCell = efld.getCellGroup<Hex>(0);
  typedef Field<PhysD3, TopoD3, Real>::template FieldCellGroupType<Hex>::template ElementType<> ElementEFieldClass;

  // element field variables
  ElementEFieldClass efldElem( efldCell.basis() );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );
    SANS_CHECK_CLOSE( 0.5, efldElem.DOF(0), small_tol, close_tol );
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
