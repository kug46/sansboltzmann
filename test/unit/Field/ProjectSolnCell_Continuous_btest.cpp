// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ProjectSolnCell_Galerkin_btest
// testing of cell solution projection for Galerkin

#include <cmath>
#include <ostream>

#include <boost/test/unit_test.hpp>

#include "Field/ProjectSoln/InterpolateFunctionCell_Continuous.h"
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldLine_CG_Cell.h"

#include "Quadrature/QuadratureArea.h"

#include "Field/tools/for_each_CellGroup.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Quad_X1_1Group.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Hex_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( InterpolateFunctionCell_Continuous_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( InterpolateFunctionCell_Continuous_1D_Linear )
{

  typedef SolnNDConvertSpace<PhysD1, ScalarFunction1D_Linear> NDSolutionExact;
  typedef Real ArrayQ;

  Real a0 = 1;
  Real ax = 2;
  NDSolutionExact solnExact(a0, ax);

  // grid: three lines, P1 (aka X1)
  XField1D xfld(3);

  // solution: P1 (aka Q1), which spans the space of the Linear solution
  // the check below assumes Hierarchical basis
  int qorder = 1;
  Field_CG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  for_each_CellGroup<TopoD1>::apply( InterpolateFunctionCell_Continuous(solnExact, {0}), (xfld, qfld) );


  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  const XField1D              ::template FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);
  const Field<PhysD1, TopoD1, ArrayQ>::template FieldCellGroupType<Line>& qfldCell = qfld.getCellGroup<Line>(0);

  typedef XField1D              ::template FieldCellGroupType<Line>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysD1, TopoD1, ArrayQ>::template FieldCellGroupType<Line>::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass qfldElem( qfldCell.basis() );

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    qfldCell.getElement( qfldElem, elem );

    BOOST_REQUIRE_EQUAL(xfldElem.nDOF(), qfldElem.nDOF());
    for (int n = 0; n < qfldElem.nDOF(); n++)
    {
      Real exact = solnExact(xfldElem.DOF(n));
      SANS_CHECK_CLOSE( exact, qfldElem.DOF(n), small_tol, close_tol );
    }
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( InterpolateFunctionCell_Continuous_2D_Triangle )
{

  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Linear> NDSolutionExact;
  typedef Real ArrayQ;

  Real a0 = 1;
  Real ax = 2;
  Real ay = 3;
  NDSolutionExact solnExact(a0, ax, ay);

  // grid: four triangles, P1 (aka X1)
  XField2D_4Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1), which spans the space of the Linear solution
  // the check below assumes Hierarchical basis
  int qorder = 1;
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  for_each_CellGroup<TopoD2>::apply( InterpolateFunctionCell_Continuous(solnExact, {0}), (xfld, qfld) );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  const XField2D_4Triangle_X1_1Group ::template FieldCellGroupType<Triangle>& xfldCell = xfld.getCellGroup<Triangle>(0);
  const Field<PhysD2, TopoD2, ArrayQ>::template FieldCellGroupType<Triangle>& qfldCell = qfld.getCellGroup<Triangle>(0);

  typedef XField2D_4Triangle_X1_1Group ::template FieldCellGroupType<Triangle>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysD2, TopoD2, ArrayQ>::template FieldCellGroupType<Triangle>::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass qfldElem( qfldCell.basis() );

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    qfldCell.getElement( qfldElem, elem );

    BOOST_REQUIRE_EQUAL(xfldElem.nDOF(), qfldElem.nDOF());
    for (int n = 0; n < qfldElem.nDOF(); n++)
    {
      Real exact = solnExact(xfldElem.DOF(n));
      SANS_CHECK_CLOSE( exact, qfldElem.DOF(n), small_tol, close_tol );
    }
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ProjectSolnCell_Discontinuous_from_Continuous_2D_Triangle )
{

  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_SineSine> NDSolutionExact;
  typedef Real ArrayQ;

  NDSolutionExact solnExact;

  // grid: four triangles, P1 (aka X1)
  // XField2D_4Triangle_X1_1Group xfld;
  XField2D_Box_UnionJack_Triangle_X1 xfld(4,4);


  // Create a p3 solution field and project
  int qorder = 3;
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> cfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  for_each_CellGroup<TopoD2>::apply( InterpolateFunctionCell_Continuous(solnExact, {0}), (xfld, cfld) );

  // A P3 DG field in the same basis
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> dfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  const Field<PhysD2, TopoD2, ArrayQ>::template FieldCellGroupType<Triangle>& cfldCell = cfld.getCellGroup<Triangle>(0);
        Field<PhysD2, TopoD2, ArrayQ>::template FieldCellGroupType<Triangle>& dfldCell = dfld.getCellGroup<Triangle>(0);

  typedef Field<PhysD2, TopoD2, ArrayQ>::template FieldCellGroupType<Triangle>::template ElementType<> ElementQFieldClass;

  QuadratureArea< Triangle> cellQuadrature(qorder+2);
  const int nCellquad = cellQuadrature.nQuadrature();

  // element field variables
  ElementQFieldClass cfldElem( cfldCell.basis() ), dfldElem( dfldCell.basis() );

  ArrayQ qC, qD;

  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;
  RefCoordType RefCoord; // reference-element coordinates in cell

  // set the DG solution using get/set
  for (int elem = 0; elem < cfldCell.nElem(); elem++)
  {
    cfldCell.getElement( cfldElem, elem );
    dfldCell.setElement( cfldElem, elem );
    dfldCell.getElement( dfldElem, elem );

    for (int iquad = 0; iquad < nCellquad; iquad++)
    {
      cellQuadrature.coordinates( iquad, RefCoord );
      cfldElem.eval( RefCoord, qC);
      dfldElem.eval( RefCoord, qD);

      SANS_CHECK_CLOSE( qC, qD, small_tol, close_tol );
    }
  }

}



//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
