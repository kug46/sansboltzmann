// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Quad_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Quad_X1_1Group.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/FieldLiftArea_DG_BoundaryTrace.h"

using namespace std;
using namespace SANS;


//Explicitly instantiate classes to get proper coverage information
//namespace SANS
//{
////template class Field_DG_Cell< PhysD2, TopoD2, Real >;
//template class FieldLift_DG_Cell< PhysD2, TopoD2, Real >;
//}


//############################################################################//
BOOST_AUTO_TEST_SUITE( FieldLift_DG_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldLift_DG_1D_Line_Legendre )
{
  typedef DLA::VectorS<1, Real> ArrayQ;
  typedef DLA::VectorS<PhysD1::D, ArrayQ> VectorArrayQ;
  typedef FieldLift_DG_Cell< PhysD1, TopoD1, VectorArrayQ > RField_DG_1D;

  for (int order=0; order<=3; order++)
  {

    int DOF_per_elem = order+1;

    XField1D_1Line_X1_1Group xfld1;
    RField_DG_1D rfld1(xfld1, order, BasisFunctionCategory_Legendre);

    BOOST_CHECK_EQUAL( DOF_per_elem*2*1, rfld1.nDOF() );
    BOOST_CHECK_EQUAL( 0, rfld1.nInteriorTraceGroups() );
    BOOST_CHECK_EQUAL( 0, rfld1.nBoundaryTraceGroups() );
    BOOST_CHECK_EQUAL( 1, rfld1.nCellGroups() );
    BOOST_CHECK_EQUAL( &xfld1, &rfld1.getXField() );
    BOOST_CHECK_EQUAL( 1, rfld1.nElem() );

    BOOST_CHECK_THROW( rfld1.nDOFCellGroup(0), DeveloperException);
    BOOST_CHECK_THROW( rfld1.nDOFInteriorTraceGroup(0), DeveloperException);
    BOOST_CHECK_THROW( rfld1.nDOFBoundaryTraceGroup(0), DeveloperException);

    rfld1 = 1;
    for ( int n = 0; n < rfld1.nDOF(); n++ )
      BOOST_CHECK_EQUAL( 1, rfld1.DOF(n)[0][0] );

    XField1D_2Line_X1_1Group xfld2;
    RField_DG_1D rfld2(xfld2, order, BasisFunctionCategory_Legendre);

    BOOST_CHECK_EQUAL( DOF_per_elem*2*2, rfld2.nDOF() );
    BOOST_CHECK_EQUAL( 0, rfld2.nInteriorTraceGroups() );
    BOOST_CHECK_EQUAL( 0, rfld2.nBoundaryTraceGroups() );
    BOOST_CHECK_EQUAL( 1, rfld2.nCellGroups() );
    BOOST_CHECK_EQUAL( &xfld2, &rfld2.getXField() );
    BOOST_CHECK_EQUAL( 2, rfld2.nElem() );

    BOOST_CHECK_THROW( rfld2.nDOFCellGroup(0), DeveloperException);
    BOOST_CHECK_THROW( rfld2.nDOFInteriorTraceGroup(0), DeveloperException);
    BOOST_CHECK_THROW( rfld2.nDOFBoundaryTraceGroup(0), DeveloperException);

    RField_DG_1D rfld3( rfld1, FieldCopy() );

    BOOST_CHECK_EQUAL(  rfld1.nDOF()         ,  rfld3.nDOF() );

    for ( int n = 0; n < rfld1.nDOF(); n++ )
      BOOST_CHECK_EQUAL( rfld3.DOF(n)[0][0], rfld1.DOF(n)[0][0] );

  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldLift_DG_2D_Area_Legendre )
{
  typedef DLA::VectorS<1, Real> ArrayQ;
  typedef DLA::VectorS<PhysD2::D, ArrayQ> VectorArrayQ;
  typedef FieldLift_DG_Cell< PhysD2, TopoD2, VectorArrayQ > RField_DG_2D;

  // ---------- Triangles ---------- //
  for (int order=0; order<=3; order++)
  {
    int DOF_per_elem = (order+1)*(order+2)/2;

    XField2D_2Triangle_X1_1Group xfld1;
    RField_DG_2D rfld1(xfld1, order, BasisFunctionCategory_Legendre);

    BOOST_CHECK_EQUAL( DOF_per_elem*3*2, rfld1.nDOF() );
    BOOST_CHECK_EQUAL( 0, rfld1.nInteriorTraceGroups() );
    BOOST_CHECK_EQUAL( 0, rfld1.nBoundaryTraceGroups() );
    BOOST_CHECK_EQUAL( 1, rfld1.nCellGroups() );
    BOOST_CHECK_EQUAL( &xfld1, &rfld1.getXField() );
    BOOST_CHECK_EQUAL( 2, rfld1.nElem() );

    BOOST_CHECK_THROW( rfld1.nDOFCellGroup(0), DeveloperException);
    BOOST_CHECK_THROW( rfld1.nDOFInteriorTraceGroup(0), DeveloperException);
    BOOST_CHECK_THROW( rfld1.nDOFBoundaryTraceGroup(0), DeveloperException);

    rfld1 = 1;
    for (int i = 0; i < rfld1.nDOF(); i++)
      for (int j = 0; j < PhysD2::D; j++)
        rfld1.DOF(i)(j) = pow(-1,j)*(i+1);

    XField2D_4Triangle_X1_1Group xfld2;
    RField_DG_2D rfld2(xfld2, order, BasisFunctionCategory_Legendre);

    BOOST_CHECK_EQUAL( DOF_per_elem*3*4, rfld2.nDOF() );
    BOOST_CHECK_EQUAL( 0, rfld2.nInteriorTraceGroups() );
    BOOST_CHECK_EQUAL( 0, rfld2.nBoundaryTraceGroups() );
    BOOST_CHECK_EQUAL( 1, rfld2.nCellGroups() );
    BOOST_CHECK_EQUAL( &xfld2, &rfld2.getXField() );
    BOOST_CHECK_EQUAL( 4, rfld2.nElem() );

    BOOST_CHECK_THROW( rfld2.nDOFCellGroup(0), DeveloperException);
    BOOST_CHECK_THROW( rfld2.nDOFInteriorTraceGroup(0), DeveloperException);
    BOOST_CHECK_THROW( rfld2.nDOFBoundaryTraceGroup(0), DeveloperException);

    RField_DG_2D rfld3( rfld1, FieldCopy() );

    BOOST_CHECK_EQUAL(  rfld1.nDOF()         ,  rfld3.nDOF() );

    for (int i = 0; i < rfld3.nDOF(); i++)
      for (int j = 0; j < PhysD2::D; j++)
        BOOST_CHECK_EQUAL( rfld1.DOF(i)(j)(0), rfld3.DOF(i)(j)(0) ) ;

  }

  // ---------- Quads ---------- //
  const int ordermax_quad = 0;  //TODO: higher than p0 has yet been implemented for quads
  for (int order=0; order<=ordermax_quad; order++)
  {
    int DOF_per_elem = (order+1)*(order+1);

    XField2D_2Quad_X1_1Group xfld3;
    RField_DG_2D rfld3(xfld3, order, BasisFunctionCategory_Legendre);

    BOOST_CHECK_EQUAL( DOF_per_elem*4*2, rfld3.nDOF() );
    BOOST_CHECK_EQUAL( 0, rfld3.nInteriorTraceGroups() );
    BOOST_CHECK_EQUAL( 0, rfld3.nBoundaryTraceGroups() );
    BOOST_CHECK_EQUAL( 1, rfld3.nCellGroups() );
    BOOST_CHECK_EQUAL( &xfld3, &rfld3.getXField() );
    BOOST_CHECK_EQUAL( 2, rfld3.nElem() );

    BOOST_CHECK_THROW( rfld3.nDOFCellGroup(0), DeveloperException);
    BOOST_CHECK_THROW( rfld3.nDOFInteriorTraceGroup(0), DeveloperException);
    BOOST_CHECK_THROW( rfld3.nDOFBoundaryTraceGroup(0), DeveloperException);

    XField2D_4Quad_X1_1Group xfld4;
    RField_DG_2D rfld4(xfld4, order, BasisFunctionCategory_Legendre);

    BOOST_CHECK_EQUAL( DOF_per_elem*4*4, rfld4.nDOF() );
    BOOST_CHECK_EQUAL( 0, rfld4.nInteriorTraceGroups() );
    BOOST_CHECK_EQUAL( 0, rfld4.nBoundaryTraceGroups() );
    BOOST_CHECK_EQUAL( 1, rfld4.nCellGroups() );
    BOOST_CHECK_EQUAL( &xfld4, &rfld4.getXField() );
    BOOST_CHECK_EQUAL( 4, rfld4.nElem() );

    BOOST_CHECK_THROW( rfld4.nDOFCellGroup(0), DeveloperException);
    BOOST_CHECK_THROW( rfld4.nDOFInteriorTraceGroup(0), DeveloperException);
    BOOST_CHECK_THROW( rfld4.nDOFBoundaryTraceGroup(0), DeveloperException);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldLift_DG_2D_Area_BoundaryTrace_Legendre )
{
  typedef Real ArrayQ;
  typedef DLA::VectorS<PhysD2::D, ArrayQ> VectorArrayQ;
  typedef FieldLift_DG_BoundaryTrace< PhysD2, TopoD2, VectorArrayQ > RField_DG_Boundary;


  XField2D_2Triangle_X1_1Group xfld1;

  int order = 0;
  RField_DG_Boundary rfld1(xfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 4, rfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, rfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, rfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, rfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &rfld1.getXField() );


  order = 1;
  RField_DG_Boundary rfld2(xfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 3*4, rfld2.nDOF() );
  BOOST_CHECK_EQUAL(   0, rfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(   0, rfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(   1, rfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &rfld2.getXField() );

  // assign a constant value
  rfld1 = 0;
  for (int i = 0; i < rfld1.nDOF(); i++)
  {
    BOOST_CHECK_EQUAL( 0.0, rfld1.DOF(i)[0] );
    BOOST_CHECK_EQUAL( 0.0, rfld1.DOF(i)[1] );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Exception_1D_Line )
{
  typedef DLA::VectorS<1, Real> ArrayQ;
  typedef DLA::VectorS<PhysD1::D, ArrayQ> VectorArrayQ;
  typedef FieldLift_DG_Cell< PhysD1, TopoD1, VectorArrayQ > RField_DG_1D;

  XField1D_1Line_X1_1Group xfld1;

  int order = 999; //This should always be an order that is not available

  BOOST_CHECK_THROW( RField_DG_1D rfld1(xfld1, order, BasisFunctionCategory_Legendre), DeveloperException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Exception_2D_Area )
{
  typedef DLA::VectorS<1, Real> ArrayQ;
  typedef DLA::VectorS<PhysD2::D, ArrayQ> VectorArrayQ;
  typedef FieldLift_DG_Cell< PhysD2, TopoD2, VectorArrayQ > RField_DG_2D;

  XField2D_2Triangle_X1_1Group xfld1;

  int order = 999; //This should always be an order that is not available

  BOOST_CHECK_THROW( RField_DG_2D rfld1(xfld1, order, BasisFunctionCategory_Legendre), DeveloperException );
}

#if 0

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Area_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_DG_Area;
  typedef QField2D_DG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldClass;

  ArrayQ q0, q1;

  XField2D_4Triangle_X1_1Group xfld1;

  for (int order = 0; order < BasisFunctionArea_Triangle_LegendrePMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionArea_Triangle_LegendrePMax-order; orderinc++)
    {
      QField2D_DG_Area qfldP  (xfld1, order         , BasisFunctionCategory_Legendre);
      QField2D_DG_Area qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Legendre);

      typedef QField2D_DG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;

      QFieldAreaClass& qfldAeraP   = qfldP.getCellGroup<Triangle>(0);
      QFieldAreaClass& qfldAeraPp1 = qfldPp1.getCellGroup<Triangle>(0);

      ElementQFieldClass qfldElemP(qfldAeraP.basis());
      ElementQFieldClass qfldElemPp1(qfldAeraPp1.basis());

      //Give some non-zero initial condition
      for (int n = 0; n < qfldP.nDOF(); n++)
        qfldP.DOF(n) = n+1;

      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = 0;

      //Use area function projectTo
      qfldAeraP.projectTo(qfldAeraPp1);

      for (int elem = 0; elem < qfldAeraP.nElem(); elem++)
      {
        qfldAeraP.getElement(qfldElemP, elem);
        qfldAeraPp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, 0.25, q0);
        qfldElemPp1.eval(0.25, 0.25, q1);
        BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      }

      //Wipe out DOF's for P1
      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = -1;

      //Use base function projectTo
      qfldP.projectTo(qfldPp1);

      for (int elem = 0; elem < qfldAeraP.nElem(); elem++)
      {
        qfldAeraP.getElement(qfldElemP, elem);
        qfldAeraPp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, 0.25, q0);
        qfldElemPp1.eval(0.25, 0.25, q1);
        BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      }
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_DG_Area;
  typedef Field_DG_InteriorTrace< PhysD2, TopoD2, ArrayQ > QField2D_DG_InteriorEdge;
  typedef Field_DG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > QField2D_DG_BoundaryEdge;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/Field2D_DG_pattern.txt", true );

  XField2D_2Triangle_X1_1Group xfld1;

  QField2D_DG_Area qfld1(xfld1, 0, BasisFunctionCategory_Legendre);
  qfld1.dump( 2, output );

  QField2D_DG_InteriorEdge qfld2(xfld1, 2, BasisFunctionCategory_Legendre);
  qfld2.dump( 2, output );

  QField2D_DG_BoundaryEdge qfld3(xfld1, 2, BasisFunctionCategory_Legendre);
  qfld3.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
