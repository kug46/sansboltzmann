// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field_NodalView_btest
// testing of Field_NodalView
//
// Note: unit grids tested in "unit/UnitGrids/UnitGrids_btest.cpp"

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Field/Field_NodalView.h"

#include "Field/XField_CellToTrace.h"
#include "Field/Local/XField_LocalPatch.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"
#include "unit/UnitGrids/XField_KuhnFreudenthal.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( Field_NodalView_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field_NodalView_1D_1Group_test )
{
  XField1D xfld(4);
  Field_NodalView nodalview(xfld,{0});

  std::vector<int> nodelist;
  std::vector<int> invnodelist;
  Field_NodalView::IndexVector cellset;
  std::vector<std::pair<int,int>> edgeset;

  //Check nodeDOF list
  nodelist = nodalview.getNodeDOFList();
  BOOST_REQUIRE_EQUAL( (int) nodelist.size(), 5 );
  BOOST_CHECK_EQUAL( nodelist[0], 0 );
  BOOST_CHECK_EQUAL( nodelist[1], 1 );
  BOOST_CHECK_EQUAL( nodelist[2], 2 );
  BOOST_CHECK_EQUAL( nodelist[3], 3 );
  BOOST_CHECK_EQUAL( nodelist[4], 4 );

  //Check inverse nodeDOF list
  invnodelist = nodalview.getInverseNodeDOFList();
  BOOST_REQUIRE_EQUAL( (int) invnodelist.size(), xfld.nDOF() );
  BOOST_CHECK_EQUAL( invnodelist[0], 0 );
  BOOST_CHECK_EQUAL( invnodelist[1], 1 );
  BOOST_CHECK_EQUAL( invnodelist[2], 2 );
  BOOST_CHECK_EQUAL( invnodelist[3], 3 );
  BOOST_CHECK_EQUAL( invnodelist[4], 4 );

  //Cells around nodeDOF 0
  cellset = nodalview.getCellList(0);
  BOOST_CHECK_EQUAL( cellset[0].group, 0 );  BOOST_CHECK_EQUAL( cellset[0].elem, 0 );

  //Cells around nodeDOF 1
  cellset = nodalview.getCellList(1);
  BOOST_CHECK_EQUAL( cellset[0].group, 0 );  BOOST_CHECK_EQUAL( cellset[0].elem, 0 );
  BOOST_CHECK_EQUAL( cellset[1].group, 0 );  BOOST_CHECK_EQUAL( cellset[1].elem, 1 );

  //Cells around nodeDOF 2
  cellset = nodalview.getCellList(2);
  BOOST_CHECK_EQUAL( cellset[0].group, 0 );  BOOST_CHECK_EQUAL( cellset[0].elem, 1 );
  BOOST_CHECK_EQUAL( cellset[1].group, 0 );  BOOST_CHECK_EQUAL( cellset[1].elem, 2 );

  //Cells around nodeDOF 3
  cellset = nodalview.getCellList(3);
  BOOST_CHECK_EQUAL( cellset[0].group, 0 );  BOOST_CHECK_EQUAL( cellset[0].elem, 2 );
  BOOST_CHECK_EQUAL( cellset[1].group, 0 );  BOOST_CHECK_EQUAL( cellset[1].elem, 3 );

  //Cells around nodeDOF 4
  cellset = nodalview.getCellList(4);
  BOOST_CHECK_EQUAL( cellset[0].group, 0 );  BOOST_CHECK_EQUAL( cellset[0].elem, 3 );

  //Edges around nodeDOF 0
  edgeset = nodalview.getEdgeList(0);
  BOOST_CHECK_EQUAL( edgeset[0].first, 0 );  BOOST_CHECK_EQUAL( edgeset[0].second, 1 );

  //Edges around nodeDOF 1
  edgeset = nodalview.getEdgeList(1);
  BOOST_CHECK_EQUAL( edgeset[0].first, 0 );  BOOST_CHECK_EQUAL( edgeset[0].second, 1 );
  BOOST_CHECK_EQUAL( edgeset[1].first, 1 );  BOOST_CHECK_EQUAL( edgeset[1].second, 2 );

  //Edges around nodeDOF 2
  edgeset = nodalview.getEdgeList(2);
  BOOST_CHECK_EQUAL( edgeset[0].first, 1 );  BOOST_CHECK_EQUAL( edgeset[0].second, 2 );
  BOOST_CHECK_EQUAL( edgeset[1].first, 2 );  BOOST_CHECK_EQUAL( edgeset[1].second, 3 );

  //Edges around nodeDOF 3
  edgeset = nodalview.getEdgeList(3);
  BOOST_CHECK_EQUAL( edgeset[0].first, 2 );  BOOST_CHECK_EQUAL( edgeset[0].second, 3 );
  BOOST_CHECK_EQUAL( edgeset[1].first, 3 );  BOOST_CHECK_EQUAL( edgeset[1].second, 4 );

  //Edges around nodeDOF 4
  edgeset = nodalview.getEdgeList(4);
  BOOST_CHECK_EQUAL( edgeset[0].first, 3 );  BOOST_CHECK_EQUAL( edgeset[0].second, 4 );

  // checking the count of cells common to nodes
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({}), 0 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({0}), 1 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({1}), 2 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({0,1}), 1 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({0,1,2}), 0 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field_NodalView_2D_4Triangle_X1_1Group_test )
{
  XField2D_4Triangle_X1_1Group xfld;
  Field_NodalView nodalview(xfld,{0});

  std::vector<int> nodelist;
  std::vector<int> invnodelist;
  Field_NodalView::IndexVector cellset;
  std::vector<std::pair<int,int>> edgeset;

  //Check nodeDOF list
  nodelist = nodalview.getNodeDOFList();
  BOOST_REQUIRE_EQUAL( (int) nodelist.size(), 6 );
  BOOST_CHECK_EQUAL( nodelist[0], 0 );
  BOOST_CHECK_EQUAL( nodelist[1], 1 );
  BOOST_CHECK_EQUAL( nodelist[2], 2 );
  BOOST_CHECK_EQUAL( nodelist[3], 3 );
  BOOST_CHECK_EQUAL( nodelist[4], 4 );
  BOOST_CHECK_EQUAL( nodelist[5], 5 );

  //Check inverse nodeDOF list
  invnodelist = nodalview.getInverseNodeDOFList();
  BOOST_REQUIRE_EQUAL( (int) invnodelist.size(), xfld.nDOF() );
  BOOST_CHECK_EQUAL( invnodelist[0], 0 );
  BOOST_CHECK_EQUAL( invnodelist[1], 1 );
  BOOST_CHECK_EQUAL( invnodelist[2], 2 );
  BOOST_CHECK_EQUAL( invnodelist[3], 3 );
  BOOST_CHECK_EQUAL( invnodelist[4], 4 );
  BOOST_CHECK_EQUAL( invnodelist[5], 5 );

  //Cells around nodeDOF 0
  cellset = nodalview.getCellList(0);
  BOOST_REQUIRE_EQUAL( (int) cellset.size(), 3 );
  BOOST_CHECK_EQUAL( cellset[0].group, 0 );  BOOST_CHECK_EQUAL( cellset[0].elem, 0 );
  BOOST_CHECK_EQUAL( cellset[1].group, 0 );  BOOST_CHECK_EQUAL( cellset[1].elem, 2 );
  BOOST_CHECK_EQUAL( cellset[2].group, 0 );  BOOST_CHECK_EQUAL( cellset[2].elem, 3 );

  //Cells around nodeDOF 1
  cellset = nodalview.getCellList(1);
  BOOST_REQUIRE_EQUAL( (int) cellset.size(), 3 );
  BOOST_CHECK_EQUAL( cellset[0].group, 0 );  BOOST_CHECK_EQUAL( cellset[0].elem, 0 );
  BOOST_CHECK_EQUAL( cellset[1].group, 0 );  BOOST_CHECK_EQUAL( cellset[1].elem, 1 );
  BOOST_CHECK_EQUAL( cellset[2].group, 0 );  BOOST_CHECK_EQUAL( cellset[2].elem, 3 );

  //Cells around nodeDOF 2
  cellset = nodalview.getCellList(2);
  BOOST_REQUIRE_EQUAL( (int) cellset.size(), 3 );
  BOOST_CHECK_EQUAL( cellset[0].group, 0 );  BOOST_CHECK_EQUAL( cellset[0].elem, 0 );
  BOOST_CHECK_EQUAL( cellset[1].group, 0 );  BOOST_CHECK_EQUAL( cellset[1].elem, 1 );
  BOOST_CHECK_EQUAL( cellset[2].group, 0 );  BOOST_CHECK_EQUAL( cellset[2].elem, 2 );

  //Cells around nodeDOF 3
  cellset = nodalview.getCellList(3);
  BOOST_REQUIRE_EQUAL( (int) cellset.size(), 1 );
  BOOST_CHECK_EQUAL( cellset[0].group, 0 );  BOOST_CHECK_EQUAL( cellset[0].elem, 1 );

  //Cells around nodeDOF 4
  cellset = nodalview.getCellList(4);
  BOOST_REQUIRE_EQUAL( (int) cellset.size(), 1 );
  BOOST_CHECK_EQUAL( cellset[0].group, 0 );  BOOST_CHECK_EQUAL( cellset[0].elem, 2 );

  //Cells around nodeDOF 5
  cellset = nodalview.getCellList(5);
  BOOST_REQUIRE_EQUAL( (int) cellset.size(), 1 );
  BOOST_CHECK_EQUAL( cellset[0].group, 0 );  BOOST_CHECK_EQUAL( cellset[0].elem, 3 );

  //Edges around nodeDOF 0
  edgeset = nodalview.getEdgeList(0);
  BOOST_REQUIRE_EQUAL( (int) edgeset.size(), 4 );
  BOOST_CHECK_EQUAL( edgeset[0].first, 0 );  BOOST_CHECK_EQUAL( edgeset[0].second, 1 );
  BOOST_CHECK_EQUAL( edgeset[1].first, 0 );  BOOST_CHECK_EQUAL( edgeset[1].second, 2 );
  BOOST_CHECK_EQUAL( edgeset[2].first, 0 );  BOOST_CHECK_EQUAL( edgeset[2].second, 4 );
  BOOST_CHECK_EQUAL( edgeset[3].first, 0 );  BOOST_CHECK_EQUAL( edgeset[3].second, 5 );

  //Edges around nodeDOF 1
  edgeset = nodalview.getEdgeList(1);
  BOOST_REQUIRE_EQUAL( (int) edgeset.size(), 4 );
  BOOST_CHECK_EQUAL( edgeset[0].first, 0 );  BOOST_CHECK_EQUAL( edgeset[0].second, 1 );
  BOOST_CHECK_EQUAL( edgeset[1].first, 1 );  BOOST_CHECK_EQUAL( edgeset[1].second, 2 );
  BOOST_CHECK_EQUAL( edgeset[2].first, 1 );  BOOST_CHECK_EQUAL( edgeset[2].second, 3 );
  BOOST_CHECK_EQUAL( edgeset[3].first, 1 );  BOOST_CHECK_EQUAL( edgeset[3].second, 5 );

  //Edges around nodeDOF 2
  edgeset = nodalview.getEdgeList(2);
  BOOST_REQUIRE_EQUAL( (int) edgeset.size(), 4 );
  BOOST_CHECK_EQUAL( edgeset[0].first, 0 );  BOOST_CHECK_EQUAL( edgeset[0].second, 2 );
  BOOST_CHECK_EQUAL( edgeset[1].first, 1 );  BOOST_CHECK_EQUAL( edgeset[1].second, 2 );
  BOOST_CHECK_EQUAL( edgeset[2].first, 2 );  BOOST_CHECK_EQUAL( edgeset[2].second, 3 );
  BOOST_CHECK_EQUAL( edgeset[3].first, 2 );  BOOST_CHECK_EQUAL( edgeset[3].second, 4 );

  //Edges around nodeDOF 3
  edgeset = nodalview.getEdgeList(3);
  BOOST_REQUIRE_EQUAL( (int) edgeset.size(), 2 );
  BOOST_CHECK_EQUAL( edgeset[0].first, 1 );  BOOST_CHECK_EQUAL( edgeset[0].second, 3 );
  BOOST_CHECK_EQUAL( edgeset[1].first, 2 );  BOOST_CHECK_EQUAL( edgeset[1].second, 3 );

  //Edges around nodeDOF 4
  edgeset = nodalview.getEdgeList(4);
  BOOST_REQUIRE_EQUAL( (int) edgeset.size(), 2 );
  BOOST_CHECK_EQUAL( edgeset[0].first, 0 );  BOOST_CHECK_EQUAL( edgeset[0].second, 4 );
  BOOST_CHECK_EQUAL( edgeset[1].first, 2 );  BOOST_CHECK_EQUAL( edgeset[1].second, 4 );

  //Edges around nodeDOF 5
  edgeset = nodalview.getEdgeList(5);
  BOOST_REQUIRE_EQUAL( (int) edgeset.size(), 2 );
  BOOST_CHECK_EQUAL( edgeset[0].first, 0 );  BOOST_CHECK_EQUAL( edgeset[0].second, 5 );
  BOOST_CHECK_EQUAL( edgeset[1].first, 1 );  BOOST_CHECK_EQUAL( edgeset[1].second, 5 );

  // checking the count of cells common to nodes
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({}), 0 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({0}), 3 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({1}), 3 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({3}), 1 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({0,1}), 2 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({0,1,2}), 1 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({0,1,2,3}), 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field_NodalView_2D_4Triangle_X1_2Group_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_4Triangle_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the central triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local, connectivity, 0, 0);
  XField_LocalPatch<PhysD2, Triangle> xfld_local(xfld_construct);

  //xfld_local has two cell groups, extract nodal view only for cell group 1
  Field_NodalView nodalview(xfld_local,{1});

  std::vector<int> nodelist;
  std::vector<int> invnodelist;
  Field_NodalView::IndexVector cellset;
  std::vector<std::pair<int,int>> edgeset;

  //Check nodeDOF list
  nodelist = nodalview.getNodeDOFList();
  BOOST_REQUIRE_EQUAL( (int) nodelist.size(), 6 );
  BOOST_CHECK_EQUAL( nodelist[0], 0 );
  BOOST_CHECK_EQUAL( nodelist[1], 1 );
  BOOST_CHECK_EQUAL( nodelist[2], 2 );
  BOOST_CHECK_EQUAL( nodelist[3], 3 );
  BOOST_CHECK_EQUAL( nodelist[4], 4 );
  BOOST_CHECK_EQUAL( nodelist[5], 5 );

  //Check inverse nodeDOF list
  invnodelist = nodalview.getInverseNodeDOFList();
  BOOST_REQUIRE_EQUAL( (int) invnodelist.size(), xfld.nDOF() );
  BOOST_CHECK_EQUAL( invnodelist[0], 0 );
  BOOST_CHECK_EQUAL( invnodelist[1], 1 );
  BOOST_CHECK_EQUAL( invnodelist[2], 2 );
  BOOST_CHECK_EQUAL( invnodelist[3], 3 );
  BOOST_CHECK_EQUAL( invnodelist[4], 4 );
  BOOST_CHECK_EQUAL( invnodelist[5], 5 );

  //Cells around nodeDOF 0
  cellset = nodalview.getCellList(0);
  BOOST_REQUIRE_EQUAL( (int) cellset.size(), 2 );
  BOOST_CHECK_EQUAL( cellset[0].group, 1 );  BOOST_CHECK_EQUAL( cellset[0].elem, 1 );
  BOOST_CHECK_EQUAL( cellset[1].group, 1 );  BOOST_CHECK_EQUAL( cellset[1].elem, 2 );

  //Cells around nodeDOF 1
  cellset = nodalview.getCellList(1);
  BOOST_REQUIRE_EQUAL( (int) cellset.size(), 2 );
  BOOST_CHECK_EQUAL( cellset[0].group, 1 );  BOOST_CHECK_EQUAL( cellset[0].elem, 0 );
  BOOST_CHECK_EQUAL( cellset[1].group, 1 );  BOOST_CHECK_EQUAL( cellset[1].elem, 2 );

  //Cells around nodeDOF 2
  cellset = nodalview.getCellList(2);
  BOOST_REQUIRE_EQUAL( (int) cellset.size(), 2 );
  BOOST_CHECK_EQUAL( cellset[0].group, 1 );  BOOST_CHECK_EQUAL( cellset[0].elem, 0 );
  BOOST_CHECK_EQUAL( cellset[1].group, 1 );  BOOST_CHECK_EQUAL( cellset[1].elem, 1 );

  //Cells around nodeDOF 3
  cellset = nodalview.getCellList(3);
  BOOST_REQUIRE_EQUAL( (int) cellset.size(), 1 );
  BOOST_CHECK_EQUAL( cellset[0].group, 1 );  BOOST_CHECK_EQUAL( cellset[0].elem, 0 );

  //Cells around nodeDOF 4
  cellset = nodalview.getCellList(4);
  BOOST_REQUIRE_EQUAL( (int) cellset.size(), 1 );
  BOOST_CHECK_EQUAL( cellset[0].group, 1 );  BOOST_CHECK_EQUAL( cellset[0].elem, 1 );

  //Cells around nodeDOF 5
  cellset = nodalview.getCellList(5);
  BOOST_REQUIRE_EQUAL( (int) cellset.size(), 1 );
  BOOST_CHECK_EQUAL( cellset[0].group, 1 );  BOOST_CHECK_EQUAL( cellset[0].elem, 2 );

  //Edges around nodeDOF 0
  edgeset = nodalview.getEdgeList(0);
  BOOST_REQUIRE_EQUAL( (int) edgeset.size(), 4 );
  BOOST_CHECK_EQUAL( edgeset[0].first, 0 );  BOOST_CHECK_EQUAL( edgeset[0].second, 1 );
  BOOST_CHECK_EQUAL( edgeset[1].first, 0 );  BOOST_CHECK_EQUAL( edgeset[1].second, 2 );
  BOOST_CHECK_EQUAL( edgeset[2].first, 0 );  BOOST_CHECK_EQUAL( edgeset[2].second, 4 );
  BOOST_CHECK_EQUAL( edgeset[3].first, 0 );  BOOST_CHECK_EQUAL( edgeset[3].second, 5 );

  //Edges around nodeDOF 1
  edgeset = nodalview.getEdgeList(1);
  BOOST_REQUIRE_EQUAL( (int) edgeset.size(), 4 );
  BOOST_CHECK_EQUAL( edgeset[0].first, 0 );  BOOST_CHECK_EQUAL( edgeset[0].second, 1 );
  BOOST_CHECK_EQUAL( edgeset[1].first, 1 );  BOOST_CHECK_EQUAL( edgeset[1].second, 2 );
  BOOST_CHECK_EQUAL( edgeset[2].first, 1 );  BOOST_CHECK_EQUAL( edgeset[2].second, 3 );
  BOOST_CHECK_EQUAL( edgeset[3].first, 1 );  BOOST_CHECK_EQUAL( edgeset[3].second, 5 );

  //Edges around nodeDOF 2
  edgeset = nodalview.getEdgeList(2);
  BOOST_REQUIRE_EQUAL( (int) edgeset.size(), 4 );
  BOOST_CHECK_EQUAL( edgeset[0].first, 0 );  BOOST_CHECK_EQUAL( edgeset[0].second, 2 );
  BOOST_CHECK_EQUAL( edgeset[1].first, 1 );  BOOST_CHECK_EQUAL( edgeset[1].second, 2 );
  BOOST_CHECK_EQUAL( edgeset[2].first, 2 );  BOOST_CHECK_EQUAL( edgeset[2].second, 3 );
  BOOST_CHECK_EQUAL( edgeset[3].first, 2 );  BOOST_CHECK_EQUAL( edgeset[3].second, 4 );

  //Edges around nodeDOF 3
  edgeset = nodalview.getEdgeList(3);
  BOOST_REQUIRE_EQUAL( (int) edgeset.size(), 2 );
  BOOST_CHECK_EQUAL( edgeset[0].first, 1 );  BOOST_CHECK_EQUAL( edgeset[0].second, 3 );
  BOOST_CHECK_EQUAL( edgeset[1].first, 2 );  BOOST_CHECK_EQUAL( edgeset[1].second, 3 );

  //Edges around nodeDOF 4
  edgeset = nodalview.getEdgeList(4);
  BOOST_REQUIRE_EQUAL( (int) edgeset.size(), 2 );
  BOOST_CHECK_EQUAL( edgeset[0].first, 0 );  BOOST_CHECK_EQUAL( edgeset[0].second, 4 );
  BOOST_CHECK_EQUAL( edgeset[1].first, 2 );  BOOST_CHECK_EQUAL( edgeset[1].second, 4 );

  //Edges around nodeDOF 5
  edgeset = nodalview.getEdgeList(5);
  BOOST_REQUIRE_EQUAL( (int) edgeset.size(), 2 );
  BOOST_CHECK_EQUAL( edgeset[0].first, 0 );  BOOST_CHECK_EQUAL( edgeset[0].second, 5 );
  BOOST_CHECK_EQUAL( edgeset[1].first, 1 );  BOOST_CHECK_EQUAL( edgeset[1].second, 5 );


  // This is a little dubious, it's only looking in the nodalview, in actuality, they share the interior
  // checking the count of cells common to nodes
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({}), 0 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({0}), 2 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({1}), 2 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({3}), 1 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({0,1}), 1 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({0,1,2}), 0 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({0,1,2,3}), 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field_NodalView_3D_2Tet_X1_1Group_test )
{
  XField3D_2Tet_X1_1Group xfld;
  Field_NodalView nodalview(xfld,{0});

  std::vector<int> nodelist;
  std::vector<int> invnodelist;
  Field_NodalView::IndexVector cellset;
  std::vector<std::pair<int,int>> edgeset;

  //Check nodeDOF list
  nodelist = nodalview.getNodeDOFList();
  BOOST_REQUIRE_EQUAL( (int) nodelist.size(), 5 );
  BOOST_CHECK_EQUAL( nodelist[0], 0 );
  BOOST_CHECK_EQUAL( nodelist[1], 1 );
  BOOST_CHECK_EQUAL( nodelist[2], 2 );
  BOOST_CHECK_EQUAL( nodelist[3], 3 );
  BOOST_CHECK_EQUAL( nodelist[4], 4 );

  //Check inverse nodeDOF list
  invnodelist = nodalview.getInverseNodeDOFList();
  BOOST_REQUIRE_EQUAL( (int) invnodelist.size(), xfld.nDOF() );
  BOOST_CHECK_EQUAL( invnodelist[0], 0 );
  BOOST_CHECK_EQUAL( invnodelist[1], 1 );
  BOOST_CHECK_EQUAL( invnodelist[2], 2 );
  BOOST_CHECK_EQUAL( invnodelist[3], 3 );
  BOOST_CHECK_EQUAL( invnodelist[4], 4 );

  //Cells around nodeDOF 0
  cellset = nodalview.getCellList(0);
  BOOST_REQUIRE_EQUAL( (int) cellset.size(), 2 );
  BOOST_CHECK_EQUAL( cellset[0].group, 0 );  BOOST_CHECK_EQUAL( cellset[0].elem, 0 );
  BOOST_CHECK_EQUAL( cellset[1].group, 0 );  BOOST_CHECK_EQUAL( cellset[1].elem, 1 );

  //Cells around nodeDOF 1
  cellset = nodalview.getCellList(1);
  BOOST_REQUIRE_EQUAL( (int) cellset.size(), 1 );
  BOOST_CHECK_EQUAL( cellset[0].group, 0 );  BOOST_CHECK_EQUAL( cellset[0].elem, 0 );

  //Cells around nodeDOF 2
  cellset = nodalview.getCellList(2);
  BOOST_REQUIRE_EQUAL( (int) cellset.size(), 2 );
  BOOST_CHECK_EQUAL( cellset[0].group, 0 );  BOOST_CHECK_EQUAL( cellset[0].elem, 0 );
  BOOST_CHECK_EQUAL( cellset[1].group, 0 );  BOOST_CHECK_EQUAL( cellset[1].elem, 1 );

  //Cells around nodeDOF 3
  cellset = nodalview.getCellList(3);
  BOOST_REQUIRE_EQUAL( (int) cellset.size(), 2 );
  BOOST_CHECK_EQUAL( cellset[0].group, 0 );  BOOST_CHECK_EQUAL( cellset[0].elem, 0 );
  BOOST_CHECK_EQUAL( cellset[1].group, 0 );  BOOST_CHECK_EQUAL( cellset[1].elem, 1 );

  //Cells around nodeDOF 4
  cellset = nodalview.getCellList(4);
  BOOST_REQUIRE_EQUAL( (int) cellset.size(), 1 );
  BOOST_CHECK_EQUAL( cellset[0].group, 0 );  BOOST_CHECK_EQUAL( cellset[0].elem, 1 );

  // checking the count of cells common to nodes
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({}), 0 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({0}), 2 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({1}), 1 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({3}), 2 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({0,1}), 1 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({0,1,2}), 1 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({0,1,2,3}), 1 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({0,1,2,3,4}), 0 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field_NodalView_4D_KuhnFreudenthal_test )
{
  #ifdef SANS_AVRO
  mpi::communicator comm;
  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld0(comm,{2,2,2,2});
  XField<PhysD4,TopoD4> xfld(xfld0, XFieldBalance::Serial);
  if (comm.rank()!=0) return;

  Field_NodalView nodalview(xfld,{0});

  std::vector<int> nodelist;
  std::vector<int> invnodelist;
  Field_NodalView::IndexVector cellset;
  std::vector<std::pair<int,int>> edgeset;

  //Check nodeDOF list
  nodelist = nodalview.getNodeDOFList();
  BOOST_REQUIRE_EQUAL( (int) nodelist.size(), 16 );
  std::sort( nodelist.begin() , nodelist.end() );
  for (size_t i=0;i<nodelist.size();i++)
    BOOST_CHECK_EQUAL( i , nodelist[i] );

  //Check inverse nodeDOF list
  invnodelist = nodalview.getInverseNodeDOFList();
  BOOST_REQUIRE_EQUAL( (int) invnodelist.size(), xfld.nDOF() );
  std::sort(invnodelist.begin(),invnodelist.end());
  for (size_t i=0;i<nodelist.size();i++)
    BOOST_CHECK_EQUAL( i , invnodelist[i] );

  //Cells around nodeDOF 0
  cellset = nodalview.getCellList(0);
  BOOST_REQUIRE_EQUAL( (int) cellset.size(), 24 );
  for (size_t i=0;i<cellset.size();i++)
    BOOST_CHECK_EQUAL( i , cellset[i].elem );

  // TODO finish impelmenting
  #if 0

  //Cells around nodeDOF 1
  cellset = nodalview.getCellList(1);
  BOOST_REQUIRE_EQUAL( (int) cellset.size(), 1 );
  BOOST_CHECK_EQUAL( cellset[0].group, 0 );  BOOST_CHECK_EQUAL( cellset[0].elem, 0 );

  //Cells around nodeDOF 2
  cellset = nodalview.getCellList(2);
  BOOST_REQUIRE_EQUAL( (int) cellset.size(), 2 );
  BOOST_CHECK_EQUAL( cellset[0].group, 0 );  BOOST_CHECK_EQUAL( cellset[0].elem, 0 );
  BOOST_CHECK_EQUAL( cellset[1].group, 0 );  BOOST_CHECK_EQUAL( cellset[1].elem, 1 );

  //Cells around nodeDOF 3
  cellset = nodalview.getCellList(3);
  BOOST_REQUIRE_EQUAL( (int) cellset.size(), 2 );
  BOOST_CHECK_EQUAL( cellset[0].group, 0 );  BOOST_CHECK_EQUAL( cellset[0].elem, 0 );
  BOOST_CHECK_EQUAL( cellset[1].group, 0 );  BOOST_CHECK_EQUAL( cellset[1].elem, 1 );

  //Cells around nodeDOF 4
  cellset = nodalview.getCellList(4);
  BOOST_REQUIRE_EQUAL( (int) cellset.size(), 1 );
  BOOST_CHECK_EQUAL( cellset[0].group, 0 );  BOOST_CHECK_EQUAL( cellset[0].elem, 1 );

  // checking the count of cells common to nodes
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({}), 0 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({0}), 2 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({1}), 1 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({3}), 2 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({0,1}), 1 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({0,1,2}), 1 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({0,1,2,3}), 1 );
  BOOST_CHECK_EQUAL( nodalview.getTopologicalShare({0,1,2,3,4}), 0 );
  #endif

  #endif

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
