// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// FieldDataMatrixD_TraceLift_btest
// testing of FieldDataMatrixD_TraceLift
//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Quad_X1_1Group.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionArea_Quad.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldData/FieldDataMatrixD_TraceLift.h"

using namespace std;
using namespace SANS;


//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( FieldDataMatrixD_TraceLift_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_1D )
{
  typedef DLA::MatrixS<1,1,Real> MatrixQ;
  typedef DLA::VectorS<PhysD1::D,Real> VectorArrayQ;
  typedef DLA::VectorS<PhysD1::D,MatrixQ> VectorMatrixQ;
  typedef FieldLift_DG_Cell< PhysD1, TopoD1, VectorArrayQ > RField_DG_1D;
  typedef RField_DG_1D::FieldCellGroupType<Line> FieldCellGroupType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  for (int order=0; order<=3; order++)
  {
    XField1D_2Line_X1_1Group xfld;

    RField_DG_1D rfld(xfld, order, BasisFunctionCategory_Legendre);

    FieldCellGroupType& rfldCell = rfld.getCellGroupGlobal<Line>(0);

    // Setup the field for the lifting operator jacobians
    FieldDataMatrixD_TraceLift<VectorMatrixQ> jacLOfld(rfld);

    DLA::MatrixDView_Array2D<VectorMatrixQ>& jacLOfldCell = jacLOfld.getCellGroupGlobal(0);

    const int nDOF = rfldCell.basis()->nBasis();

    DLA::MatrixD<VectorMatrixQ> mtx( nDOF, nDOF );
    for (int i = 0; i < nDOF; i++)
      for (int j = 0; j < nDOF; j++)
        mtx(i,j) = i*nDOF + j + 1;

    // Set all the matrices
    Real k = 1;
    for (int elem = 0; elem < rfldCell.nElem(); elem++)
      for (int trace = 0; trace < Line::NTrace; trace++)
      {
        jacLOfldCell(elem, trace) = mtx*k;
        k++;
      }

    // Check that all matrices are correct
    k = 1;
    for (int elem = 0; elem < rfldCell.nElem(); elem++)
      for (int trace = 0; trace < Line::NTrace; trace++)
      {
        DLA::MatrixDView<VectorMatrixQ> A = jacLOfldCell(elem, trace);
        BOOST_REQUIRE_EQUAL(nDOF, A.m());
        BOOST_REQUIRE_EQUAL(nDOF, A.n());

        for (int i = 0; i < nDOF; i++)
          for (int j = 0; j < nDOF; j++)
            for (int d = 0; d < PhysD1::D; d++)
              SANS_CHECK_CLOSE(mtx(i,j)[d](0,0)*k, A(i,j)[d](0,0), small_tol, close_tol );
        k++;
      }
  } // order
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Area_Triangle_2D )
{
  typedef DLA::MatrixS<1,1,Real> MatrixQ;
  typedef DLA::VectorS<PhysD2::D,Real> VectorArrayQ;
  typedef DLA::VectorS<PhysD2::D,MatrixQ> VectorMatrixQ;
  typedef FieldLift_DG_Cell< PhysD2, TopoD2, VectorArrayQ > RField_DG_2D;
  typedef RField_DG_2D::FieldCellGroupType<Triangle> FieldCellGroupType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  XField2D_4Triangle_X1_1Group xfld;

  for (int order=0; order<=3; order++)
  {
    RField_DG_2D rfld(xfld, order, BasisFunctionCategory_Legendre);

    FieldCellGroupType& rfldCell = rfld.getCellGroupGlobal<Triangle>(0);

    // Setup the field for the lifting operator jacobians
    FieldDataMatrixD_TraceLift<VectorMatrixQ> jacLOfld(rfld);

    DLA::MatrixDView_Array2D<VectorMatrixQ>& jacLOfldCell = jacLOfld.getCellGroupGlobal(0);

    const int nDOF = rfldCell.basis()->nBasis();

    DLA::MatrixD<VectorMatrixQ> mtx( nDOF, nDOF );
    for (int i = 0; i < nDOF; i++)
      for (int j = 0; j < nDOF; j++)
        mtx(i,j) = i*nDOF + j + 1;

    // Set all the matrices
    Real k = 1;
    for (int elem = 0; elem < rfldCell.nElem(); elem++)
      for (int trace = 0; trace < Triangle::NTrace; trace++)
      {
        jacLOfldCell(elem, trace) = mtx*k;
        k++;
      }

    // Check that all matrices are correct
    k = 1;
    for (int elem = 0; elem < rfldCell.nElem(); elem++)
      for (int trace = 0; trace < Triangle::NTrace; trace++)
      {
        DLA::MatrixDView<VectorMatrixQ> A = jacLOfldCell(elem, trace);
        BOOST_REQUIRE_EQUAL(nDOF, A.m());
        BOOST_REQUIRE_EQUAL(nDOF, A.n());

        for (int i = 0; i < nDOF; i++)
          for (int j = 0; j < nDOF; j++)
            for (int d = 0; d < PhysD2::D; d++)
              SANS_CHECK_CLOSE(mtx(i,j)[d](0,0)*k, A(i,j)[d](0,0), small_tol, close_tol );
        k++;
      }
  } // order
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Area_Quad_2D )
{
  typedef DLA::MatrixS<1,1,Real> MatrixQ;
  typedef DLA::VectorS<PhysD2::D,Real> VectorArrayQ;
  typedef DLA::VectorS<PhysD2::D,MatrixQ> VectorMatrixQ;
  typedef FieldLift_DG_Cell< PhysD2, TopoD2, VectorArrayQ > RField_DG_2D;
  typedef RField_DG_2D::FieldCellGroupType<Quad> FieldCellGroupType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  XField2D_4Quad_X1_1Group xfld;

  // The MIN can be removed when we have at last order 3 legendre basis functions
  for (int order=0; order<=MIN(BasisFunctionArea_Quad_LegendrePMax,3); order++)
  {
    RField_DG_2D rfld(xfld, order, BasisFunctionCategory_Legendre);

    FieldCellGroupType& rfldCell = rfld.getCellGroupGlobal<Quad>(0);

    // Setup the field for the lifting operator jacobians
    FieldDataMatrixD_TraceLift<VectorMatrixQ> jacLOfld(rfld);

    DLA::MatrixDView_Array2D<VectorMatrixQ>& jacLOfldCell = jacLOfld.getCellGroupGlobal(0);

    const int nDOF = rfldCell.basis()->nBasis();

    DLA::MatrixD<VectorMatrixQ> mtx( nDOF, nDOF );
    for (int i = 0; i < nDOF; i++)
      for (int j = 0; j < nDOF; j++)
        mtx(i,j) = i*nDOF + j + 1;

    // Set all the matrices
    Real k = 1;
    for (int elem = 0; elem < rfldCell.nElem(); elem++)
      for (int trace = 0; trace < Quad::NTrace; trace++)
      {
        jacLOfldCell(elem, trace) = mtx*k;
        k++;
      }

    // Check that all matrices are correct
    k = 1;
    for (int elem = 0; elem < rfldCell.nElem(); elem++)
      for (int trace = 0; trace < Quad::NTrace; trace++)
      {
        DLA::MatrixDView<VectorMatrixQ> A = jacLOfldCell(elem, trace);
        BOOST_REQUIRE_EQUAL(nDOF, A.m());
        BOOST_REQUIRE_EQUAL(nDOF, A.n());

        for (int i = 0; i < nDOF; i++)
          for (int j = 0; j < nDOF; j++)
            for (int d = 0; d < PhysD2::D; d++)
              SANS_CHECK_CLOSE(mtx(i,j)[d](0,0)*k, A(i,j)[d](0,0), small_tol, close_tol );
        k++;
      }
  } // order
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
