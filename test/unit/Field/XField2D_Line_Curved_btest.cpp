// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField2D_Line_Curved_btest
// testing of higher order XField2D grid field with line elements
// Q denotes the geometric order of the mesh. E.g. Q = 1 is a linear mesh, i.e. faceted line grids
//
// Note: unit grids tested in "unit/UnitGrids/"

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Field/XFieldLine.h"

#include "unit/UnitGrids/XField2D_Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Line_X1_1Group.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( XField2D_Line_Curved_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK( (XField<PhysD2,TopoD1>::D == 2) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_1Line_X2_1Group_test )
{
  XField2D_1Line_X1_1Group xfld_X1;      // Construct linear (Q1) mesh

  XField<PhysD2,TopoD1> xfld(xfld_X1,2); // Construct Q2 mesh from linear mesh

  BOOST_CHECK_EQUAL( xfld.nDOF(), 3 );

  // ---------- Check node values ----------
  // 1st node
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(0)[1], 0 );
  // 2nd node: added DOF is zero by default
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[1], 0 );
  // 3rd node
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[1], 0 );

  // ---------- Check line field variable ----------
  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XField2D_1Line_X1_1Group::FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);

  int nodeMap[2];

  xfldCell.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  // Edge DOFs
  xfldCell.associativity(0).getEdgeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );

  // ---------- Check interior-trace field variable ----------
  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 0 );

  // ---------- Check boundary-trace field variable ----------
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 2 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Node) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(1).topoTypeID() == typeid(Node) );


  const XField<PhysD2,TopoD1>::FieldTraceGroupType<Node>& xfldBnode0 = xfld.getBoundaryTraceGroup<Node>(0);
  const XField<PhysD2,TopoD1>::FieldTraceGroupType<Node>& xfldBnode1 = xfld.getBoundaryTraceGroup<Node>(1);

  // boundary trace 1
  // node map
  xfldBnode0.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );

  // normals
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignL(), -1 );
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignR(),  0 );

  // boundary trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getCanonicalTraceLeft(0).trace, 1 );

  // boundary trace 2
  // node map
  xfldBnode1.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );

  // normals
  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignL(), 1 );
  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignR(), 0 );

  // boundary trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode1.getCanonicalTraceLeft(0).trace, 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_1Line_X3_1Group_test )
{
  XField2D_1Line_X1_1Group xfld_X1;      // Construct linear (Q1) mesh

  XField<PhysD2,TopoD1> xfld(xfld_X1,3); // Construct Q3 mesh from linear mesh

  BOOST_CHECK_EQUAL( xfld.nDOF(), 4 );

  // ---------- Check node values ----------
  // 1st node
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(0)[1], 0 );
  // 2nd node: added DOF is zero by default
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[1], 0 );
  // 3rd node: added DOF is zero by default
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[1], 0 );
  // 4th node
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[1], 0 );

  // ---------- Check line field variable ----------
  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XField2D_1Line_X1_1Group::FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);

  int nodeMap[2];

  xfldCell.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  // Edge DOFs
  xfldCell.associativity(0).getEdgeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  // ---------- Check interior-trace field variable ----------
  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 0 );

  // ---------- Check boundary-trace field variable ----------
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 2 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Node) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(1).topoTypeID() == typeid(Node) );


  const XField<PhysD2,TopoD1>::FieldTraceGroupType<Node>& xfldBnode0 = xfld.getBoundaryTraceGroup<Node>(0);
  const XField<PhysD2,TopoD1>::FieldTraceGroupType<Node>& xfldBnode1 = xfld.getBoundaryTraceGroup<Node>(1);

  // boundary trace 1
  // node map
  xfldBnode0.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );

  // normals
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignL(), -1 );
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignR(),  0 );

  // boundary trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getCanonicalTraceLeft(0).trace, 1 );

  // boundary trace 2
  // node map
  xfldBnode1.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );

  // normals
  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignL(), 1 );
  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignR(), 0 );

  // boundary trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode1.getCanonicalTraceLeft(0).trace, 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField1D_X3_1Group_test )
{
  // ---------- Set up a 3-element line grid ----------
  const int ii = 3;  // number of elements
  std::vector<DLA::VectorS<2,Real>> coordinates(ii+1);
  coordinates[0] = {0, 0};
  coordinates[1] = {1, 0};
  coordinates[2] = {2, 0.5};
  coordinates[3] = {3, 1.5};

  XField2D_Line_X1_1Group xfld_X1(coordinates); //Linear mesh with 3 lines

  XField<PhysD2,TopoD1> xfld(xfld_X1,3);        //Construct Q3 mesh from linear mesh

  BOOST_CHECK_EQUAL( xfld.nDOF(), 10 );

  //Check the edge node values: added DOF is zero by default
  for ( int i = 0; i < 6; i++)
  {
    BOOST_CHECK_CLOSE( xfld.DOF(i)[0], 0, 1.e-12 );
    BOOST_CHECK_CLOSE( xfld.DOF(i)[1], 0, 1.e-12 );
  }

  //Check the vertex node values
  for ( int i = 0; i < ii+1; i++)
  {
    BOOST_CHECK_CLOSE( xfld.DOF(i+6)[0], coordinates[i][0], 1.e-12 );
    BOOST_CHECK_CLOSE( xfld.DOF(i+6)[1], coordinates[i][1], 1.e-12 );
  }

  // ---------- Check line field variable ----------
  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD1>::FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);

  int nodeMap[2];

  // element 1
  xfldCell.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 6 );
  BOOST_CHECK_EQUAL( nodeMap[1], 7 );

  xfldCell.associativity(0).getEdgeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  // element 2
  xfldCell.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );
  BOOST_CHECK_EQUAL( nodeMap[1], 8 );

  xfldCell.associativity(1).getEdgeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  // element 3
  xfldCell.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 8 );
  BOOST_CHECK_EQUAL( nodeMap[1], 9 );

  xfldCell.associativity(2).getEdgeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );

  BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Node) );

  const XField<PhysD2,TopoD1>::FieldTraceGroupType<Node>& xfldInode = xfld.getInteriorTraceGroup<Node>(0);

  xfldInode.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );

  xfldInode.associativity(1).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 8 );

  // interior edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldInode.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldInode.getGroupRight(), 0 );

  BOOST_CHECK_EQUAL( xfldInode.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldInode.getElementRight(0), 1 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceRight(0).trace, 1 );

  BOOST_CHECK_EQUAL( xfldInode.getElementLeft(1), 1 );
  BOOST_CHECK_EQUAL( xfldInode.getElementRight(1), 2 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceLeft(1).trace, 0 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceRight(1).trace, 1 );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 2 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Node) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(1).topoTypeID() == typeid(Node) );

  const XField<PhysD2,TopoD1>::FieldTraceGroupType<Node>& xfldBnode0 = xfld.getBoundaryTraceGroup<Node>(0);
  const XField<PhysD2,TopoD1>::FieldTraceGroupType<Node>& xfldBnode1 = xfld.getBoundaryTraceGroup<Node>(1);

  xfldBnode0.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 6 );

  xfldBnode1.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );

  // boundary edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getCanonicalTraceLeft(0).trace, 1 );

  BOOST_CHECK_EQUAL( xfldBnode1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode1.getElementLeft(0), 2 );
  BOOST_CHECK_EQUAL( xfldBnode1.getCanonicalTraceLeft(0).trace, 0 );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
