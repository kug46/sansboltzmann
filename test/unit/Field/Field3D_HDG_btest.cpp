// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field3D_HDG_btest
// Tests Field_DG_Cell< PhysD3, TopoD3, DLA::VectorS<3, ArrayQ> >
//

// This is nearly identical to Field3D_DG_Tetrahedron, but is meant to test
// a field of VectorArrayQ (i.e. DLA::VectorS<2, ArrayQ>) used for
// the HDG gradient variable

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;


#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_InteriorTrace.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldVolume_DG_Trace.h"

using namespace std;
using namespace SANS;


//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
template class Field_DG_Cell< PhysD3, TopoD3, Real >;
template class Field_DG_InteriorTrace< PhysD3, TopoD3, Real >;
template class Field_DG_Trace< PhysD3, TopoD3, Real >;
template class Field_DG_BoundaryTrace< PhysD3, TopoD3, Real >;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( Field3D_HDG_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Volume_LegendreP0 )
{
  typedef Real ArrayQ;
  typedef Field_DG_Cell< PhysD3, TopoD3, DLA::VectorS<3, ArrayQ> > QField3D_DG_Volume;

  XField3D_2Tet_X1_1Group xfld1;

  int order = 0;
  QField3D_DG_Volume qfld1(xfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  XField3D_6Tet_X1_1Group xflD3;

  QField3D_DG_Volume qflD3(xflD3, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 6, qflD3.nDOF() );
  BOOST_CHECK_EQUAL( 0, qflD3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflD3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qflD3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xflD3, &qflD3.getXField() );

  const int nDOFPDE = qflD3.nDOF();
  for (int n = 0; n < nDOFPDE; n++)
    qflD3.DOF(n) = n;

  Field< PhysD3, TopoD3, DLA::VectorS<3, ArrayQ> > qfld3(qflD3, FieldCopy());

  BOOST_CHECK_EQUAL(  qflD3.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qflD3.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qflD3.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qflD3.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qflD3.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qflD3.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qflD3.getXField()           , &qfld3.getXField() );

  typedef QField3D_DG_Volume::FieldCellGroupType<Tet> QFieldVolumeClass;

  const QFieldVolumeClass& qflD3Volume = qflD3.getCellGroup<Tet>(0);
  const QFieldVolumeClass& qfld3Volume = qfld3.getCellGroup<Tet>(0);

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qflD3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qflD3.DOF(n)[0], qflD3Volume.DOF(n)[0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], qfld3Volume.DOF(n)[0] );
  }

  for (int n = 0; n < nDOFPDE; n++)
    qflD3.DOF(n) = 0;

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qflD3.DOF(n)[0], 0 );
    BOOST_CHECK_EQUAL( qflD3.DOF(n)[0], qflD3Volume.DOF(n)[0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], qfld3Volume.DOF(n)[0] );
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Volume_HierarchicalP1 )
{
  typedef Real ArrayQ;
  typedef Field_DG_Cell< PhysD3, TopoD3, DLA::VectorS<3, ArrayQ> > QField3D_DG_Volume;

  XField3D_2Tet_X1_1Group xfld1;

  int order = 1;
  QField3D_DG_Volume qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2*4, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  XField3D_6Tet_X1_1Group xflD3;

  QField3D_DG_Volume qflD3(xflD3, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 6*4, qflD3.nDOF() );
  BOOST_CHECK_EQUAL( 0, qflD3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflD3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qflD3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xflD3, &qflD3.getXField() );

  const int nDOFPDE = qflD3.nDOF();
  for (int n = 0; n < nDOFPDE; n++)
    qflD3.DOF(n) = n;

  Field< PhysD3, TopoD3, DLA::VectorS<3, ArrayQ> > qfld3(qflD3, FieldCopy());

  BOOST_CHECK_EQUAL(  qflD3.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qflD3.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qflD3.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qflD3.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qflD3.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qflD3.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qflD3.getXField()           , &qfld3.getXField() );

  typedef QField3D_DG_Volume::FieldCellGroupType<Tet> QFieldVolumeClass;

        QFieldVolumeClass& qflD3Volume = qflD3.getCellGroup<Tet>(0);
  const QFieldVolumeClass& qfld3Volume = qfld3.getCellGroup<Tet>(0);

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qflD3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qflD3.DOF(n)[0], qflD3Volume.DOF(n)[0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], qfld3Volume.DOF(n)[0] );
  }

  for (int n = 0; n < nDOFPDE; n++)
    qflD3.DOF(n) = 0;

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qflD3.DOF(n)[0], 0 );
    BOOST_CHECK_EQUAL( qflD3.DOF(n)[0], qflD3Volume.DOF(n)[0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], qfld3Volume.DOF(n)[0] );
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Volume_HierarchicalP2 )
{
  typedef Real ArrayQ;
  typedef Field_DG_Cell< PhysD3, TopoD3, DLA::VectorS<3, ArrayQ> > QField3D_DG_Volume;

  XField3D_2Tet_X1_1Group xfld1;

  int order = 2;
  QField3D_DG_Volume qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2*10, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  XField3D_6Tet_X1_1Group xflD3;

  QField3D_DG_Volume qflD3(xflD3, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 6*10, qflD3.nDOF() );
  BOOST_CHECK_EQUAL( 0, qflD3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflD3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qflD3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xflD3, &qflD3.getXField() );

  const int nDOFPDE = qflD3.nDOF();
  for (int n = 0; n < nDOFPDE; n++)
    qflD3.DOF(n) = n;

  Field< PhysD3, TopoD3, DLA::VectorS<3, ArrayQ> > qfld3(qflD3, FieldCopy());

  BOOST_CHECK_EQUAL(  qflD3.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qflD3.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qflD3.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qflD3.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qflD3.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qflD3.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qflD3.getXField()           , &qfld3.getXField() );

  typedef QField3D_DG_Volume::FieldCellGroupType<Tet> QFieldVolumeClass;

        QFieldVolumeClass& qflD3Volume = qflD3.getCellGroup<Tet>(0);
  const QFieldVolumeClass& qfld3Volume = qfld3.getCellGroup<Tet>(0);

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qflD3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qflD3.DOF(n)[0], qflD3Volume.DOF(n)[0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], qfld3Volume.DOF(n)[0] );
  }

  for (int n = 0; n < nDOFPDE; n++)
    qflD3.DOF(n) = 0;

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qflD3.DOF(n)[0], 0 );
    BOOST_CHECK_EQUAL( qflD3.DOF(n)[0], qflD3Volume.DOF(n)[0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], qfld3Volume.DOF(n)[0] );
  }

}

#if 0 // NOTE: We only have up to P2 for 3D
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Volume_HierarchicalP3 )
{
  typedef Real ArrayQ;
  typedef Field_DG_Cell< PhysD3, TopoD3, DLA::VectorS<3, ArrayQ> > QField3D_DG_Volume;

  XField3D_2Tet_X1_1Group xfld1;

  int order = 3;
  QField3D_DG_Volume qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2*15, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  XField3D_6Tet_X1_1Group xflD3;

  QField3D_DG_Volume qflD3(xflD3, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 6*15, qflD3.nDOF() );
  BOOST_CHECK_EQUAL( 0, qflD3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflD3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qflD3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xflD3, &qflD3.getXField() );

  const int nDOFPDE = qflD3.nDOF();
  for (int n = 0; n < nDOFPDE; n++)
    qflD3.DOF(n) = n;

  Field< PhysD3, TopoD3, DLA::VectorS<3, ArrayQ> > qfld3(qflD3, FieldCopy());

  BOOST_CHECK_EQUAL(  qflD3.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qflD3.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qflD3.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qflD3.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qflD3.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qflD3.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qflD3.getXField()           , &qfld3.getXField() );

  typedef QField3D_DG_Volume::FieldCellGroupType<Tet> QFieldVolumeClass;

        QFieldVolumeClass& qflD3Volume = qflD3.getCellGroup<Tet>(0);
  const QFieldVolumeClass& qfld3Volume = qfld3.getCellGroup<Tet>(0);

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qflD3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qflD3.DOF(n)[0], qflD3Volume.DOF(n)[0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], qfld3Volume.DOF(n)[0] );
  }

  for (int n = 0; n < nDOFPDE; n++)
    qflD3.DOF(n) = 0;

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qflD3.DOF(n)[0], 0 );
    BOOST_CHECK_EQUAL( qflD3.DOF(n)[0], qflD3Volume.DOF(n)[0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], qfld3Volume.DOF(n)[0] );
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Volume_LegendreP4 )
{
  typedef Real ArrayQ;
  typedef Field_DG_Cell< PhysD3, TopoD3, DLA::VectorS<3, ArrayQ> > QField3D_DG_Volume;

  XField3D_2Tet_X1_1Group xfld1;

  int order = 4;
  QField3D_DG_Volume qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2*21, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  XField3D_6Tet_X1_1Group xflD3;

  QField3D_DG_Volume qflD3(xflD3, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 6*21, qflD3.nDOF() );
  BOOST_CHECK_EQUAL( 0, qflD3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflD3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qflD3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xflD3, &qflD3.getXField() );

  const int nDOFPDE = qflD3.nDOF();
  for (int n = 0; n < nDOFPDE; n++)
    qflD3.DOF(n) = n;

  Field< PhysD3, TopoD3, DLA::VectorS<3, ArrayQ> > qfld3(qflD3, FieldCopy());

  BOOST_CHECK_EQUAL(  qflD3.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qflD3.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qflD3.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qflD3.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qflD3.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qflD3.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qflD3.getXField()           , &qfld3.getXField() );

  typedef QField3D_DG_Volume::FieldCellGroupType<Tet> QFieldVolumeClass;

        QFieldVolumeClass& qflD3Volume = qflD3.getCellGroup<Tet>(0);
  const QFieldVolumeClass& qfld3Volume = qfld3.getCellGroup<Tet>(0);

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qflD3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qflD3.DOF(n)[0], qflD3Volume.DOF(n)[0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], qfld3Volume.DOF(n)[0] );
  }

  for (int n = 0; n < nDOFPDE; n++)
    qflD3.DOF(n) = 0;

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qflD3.DOF(n)[0], 0 );
    BOOST_CHECK_EQUAL( qflD3.DOF(n)[0], qflD3Volume.DOF(n)[0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], qfld3Volume.DOF(n)[0] );
  }

}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Volume_ProjectPtoPp1 )
{
  typedef Real ArrayQ;
  typedef Field_DG_Cell< PhysD3, TopoD3, DLA::VectorS<3, ArrayQ> > QField3D_DG_Volume;
  typedef QField3D_DG_Volume::FieldCellGroupType<Tet> QFieldVolumeClass;
  typedef QFieldVolumeClass::ElementType<> ElementQFieldClass;

  DLA::VectorS<3, ArrayQ> q0, q1;

  XField3D_6Tet_X1_1Group xfld1;

  for (int order = 1; order < BasisFunctionVolume_Tet_HierarchicalPMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionVolume_Tet_HierarchicalPMax-order; orderinc++)
    {
      QField3D_DG_Volume qfldP  (xfld1, order         , BasisFunctionCategory_Hierarchical);
      QField3D_DG_Volume qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Hierarchical);

      typedef QField3D_DG_Volume::FieldCellGroupType<Tet> QFieldVolumeClass;

      QFieldVolumeClass& qfldVolP   = qfldP.getCellGroup<Tet>(0);
      QFieldVolumeClass& qfldVolPp1 = qfldPp1.getCellGroup<Tet>(0);

      ElementQFieldClass qfldElemP(qfldVolP.basis());
      ElementQFieldClass qfldElemPp1(qfldVolPp1.basis());

      //Give some non-zero initial condition
      for (int n = 0; n < qfldP.nDOF(); n++)
        qfldP.DOF(n) = n+1;

      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = 0;

      //Use Volume function projectTo
      qfldVolP.projectTo(qfldVolPp1);

      for (int elem = 0; elem < qfldVolP.nElem(); elem++)
      {
        qfldVolP.getElement(qfldElemP, elem);
        qfldVolPp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, 0.25, 0.25, q0);
        qfldElemPp1.eval(0.25, 0.25, 0.25, q1);
        BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      }

      //Wipe out DOF's for P1
      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = -1;

      //Use base function projectTo
      qfldP.projectTo(qfldPp1);

      for (int elem = 0; elem < qfldVolP.nElem(); elem++)
      {
        qfldVolP.getElement(qfldElemP, elem);
        qfldVolPp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, 0.25, 0.25, q0);
        qfldElemPp1.eval(0.25, 0.25, 0.25, q1);
        BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      }
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_InteriorFace_LegendreP0 )
{
  typedef Real ArrayQ;
  typedef Field_DG_InteriorTrace< PhysD3, TopoD3, ArrayQ > QField3D_DG_InteriorFace;
  typedef QField3D_DG_InteriorFace::FieldTraceGroupType<Triangle> QFieldAreaClass;

  XField3D_6Tet_X1_1Group xfld1;

  int faceMap[1];

  int order = 0;
  QField3D_DG_InteriorFace qfld1(xfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 6, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  BOOST_CHECK_THROW( qfld1.nDOFCellGroup(0), SANSException);
  BOOST_CHECK_EQUAL( 6, qfld1.nDOFInteriorTraceGroup(0) );
  BOOST_CHECK_THROW( qfld1.nDOFBoundaryTraceGroup(0), SANSException);

  const QFieldAreaClass& qfldGroup1 = qfld1.getInteriorTraceGroup<Triangle>(0);

  for (int i = 0; i < qfld1.nDOFInteriorTraceGroup(0); i++)
  {
    qfldGroup1.associativity(0).getCellGlobalMapping( faceMap, 1 );
    BOOST_CHECK_EQUAL( 0, faceMap[0] );
  }

  Field< PhysD3, TopoD3, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Face_LegendreP0 )
{
  typedef Real ArrayQ;
  typedef Field_DG_Trace< PhysD3, TopoD3, ArrayQ > QField3D_DG_Face;
  typedef QField3D_DG_Face::FieldTraceGroupType<Triangle> QFieldLineClass;

  XField3D_6Tet_X1_1Group xfld1;

  int faceMap[1];

  int order = 0;
  QField3D_DG_Face qfld1(xfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL(18, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  BOOST_CHECK_THROW( qfld1.nDOFCellGroup(0), SANSException);
  BOOST_CHECK_EQUAL( 6, qfld1.nDOFInteriorTraceGroup(0) );
  BOOST_CHECK_EQUAL(12, qfld1.nDOFBoundaryTraceGroup(0) );

  const QFieldLineClass& qfldGroup1 = qfld1.getInteriorTraceGroup<Triangle>(0);

  for (int i = 0; i < qfld1.nDOFInteriorTraceGroup(0); i++)
  {
    qfldGroup1.associativity(0).getCellGlobalMapping( faceMap, 1 );
    BOOST_CHECK_EQUAL( 0, faceMap[0] );
  }

  Field< PhysD3, TopoD3, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );

  BOOST_CHECK_THROW( qfld3.nDOFCellGroup(0), DeveloperException);
  BOOST_CHECK_THROW( qfld3.nDOFInteriorTraceGroup(0), DeveloperException);
  BOOST_CHECK_THROW( qfld3.nDOFBoundaryTraceGroup(0), DeveloperException);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_InteriorFace_Legendre_ProjectPtoPp1 )
{
  typedef Real ArrayQ;
  typedef Field_DG_InteriorTrace< PhysD3, TopoD3, ArrayQ > QField3D_DG_InteriorFace;
  typedef QField3D_DG_InteriorFace::FieldTraceGroupType<Triangle> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

//  ArrayQ q1, q2;

  XField3D_6Tet_X1_1Group xfld1;

  for (int order = 0; order < BasisFunctionArea_Triangle_LegendrePMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionArea_Triangle_LegendrePMax-order; orderinc++)
    {
      QField3D_DG_InteriorFace qfldP(xfld1, order  , BasisFunctionCategory_Legendre);
      QField3D_DG_InteriorFace qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Legendre);

      for ( int group = 0; group < qfldP.nInteriorTraceGroups(); group++ )
      {
        QFieldLineClass& qfldLineGroupP   = qfldP.getInteriorTraceGroup<Triangle>(group);
        QFieldLineClass& qfldLineGroupPp1 = qfldPp1.getInteriorTraceGroup<Triangle>(group);

        ElementQFieldLineClass qfldElemP(qfldLineGroupP.basis());
        ElementQFieldLineClass qfldElemPp1(qfldLineGroupPp1.basis());

        //Give some non-zero initial condition
        for (int n = 0; n < qfldP.nDOF(); n++)
          qfldP.DOF(n) = n+1;

        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = 0;

        //Use line function projectTo
        qfldLineGroupP.projectTo(qfldLineGroupPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          // Why? Because the call is ambiguous otherwise!
//          qfldElemP.eval(0.25, q1);
//          qfldElemPp1.eval(0.25, q2);
//          BOOST_CHECK_CLOSE(q1, q2, 1e-12);
        }

        //Wipe out DOF's for P1
        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = -1;

        //Use base function projectTo
        qfldP.projectTo(qfldPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          // Why? Because the call is ambiguous otherwise!
//          qfldElemP.eval(0.75, q1);
//          qfldElemPp1.eval(0.75, q2);
//          BOOST_CHECK_CLOSE(q1, q2, 1e-12);
        }
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Face_Legendre_ProjectPtoPp1 )
{
  typedef Real ArrayQ;
  typedef Field_DG_Trace< PhysD3, TopoD3, ArrayQ > QField3D_DG_Face;
  typedef QField3D_DG_Face::FieldTraceGroupType<Triangle> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

//  ArrayQ q1, q2;

  XField3D_6Tet_X1_1Group xfld1;

  for (int order = 0; order < BasisFunctionArea_Triangle_LegendrePMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionArea_Triangle_LegendrePMax-order; orderinc++)
    {
      QField3D_DG_Face qfldP(xfld1, order  , BasisFunctionCategory_Legendre);
      QField3D_DG_Face qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Legendre);

      for ( int group = 0; group < qfldP.nInteriorTraceGroups(); group++ )
      {
        QFieldLineClass& qfldLineGroupP   = qfldP.getInteriorTraceGroup<Triangle>(group);
        QFieldLineClass& qfldLineGroupPp1 = qfldPp1.getInteriorTraceGroup<Triangle>(group);

        ElementQFieldLineClass qfldElemP(qfldLineGroupP.basis());
        ElementQFieldLineClass qfldElemPp1(qfldLineGroupPp1.basis());

        //Give some non-zero initial condition
        for (int n = 0; n < qfldP.nDOF(); n++)
          qfldP.DOF(n) = n+1;

        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = 0;

        //Use line function projectTo
        qfldLineGroupP.projectTo(qfldLineGroupPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          // Why? Because the call is ambiguous otherwise!
//          qfldElemP.eval(0.25, q1);
//          qfldElemPp1.eval(0.25, q2);
//          BOOST_CHECK_CLOSE(q1, q2, 1e-12);
        }

        //Wipe out DOF's for P1
        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = -1;

        //Use base function projectTo
        qfldP.projectTo(qfldPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          // Why? Because the call is ambiguous otherwise!
//          qfldElemP.eval(0.75, q1);
//          qfldElemPp1.eval(0.75, q2);
//          BOOST_CHECK_CLOSE(q1, q2, 1e-12);
        }
      }

      for ( int group = 0; group < qfldP.nBoundaryTraceGroups(); group++ )
      {
        QFieldLineClass& qfldLineGroupP   = qfldP.getBoundaryTraceGroup<Triangle>(group);
        QFieldLineClass& qfldLineGroupPp1 = qfldPp1.getBoundaryTraceGroup<Triangle>(group);

        ElementQFieldLineClass qfldElemP(qfldLineGroupP.basis());
        ElementQFieldLineClass qfldElemPp1(qfldLineGroupPp1.basis());

        //Give some non-zero initial condition
        for (int n = 0; n < qfldP.nDOF(); n++)
          qfldP.DOF(n) = n+1;

        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = 0;

        //Use line function projectTo
        qfldLineGroupP.projectTo(qfldLineGroupPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          // Why? Because the call is ambiguous otherwise!
//          qfldElemP.eval(0.25, q1);
//          qfldElemPp1.eval(0.25, q2);
//          BOOST_CHECK_CLOSE(q1, q2, 1e-12);
        }

        //Wipe out DOF's for P1
        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = -1;

        //Use base function projectTo
        qfldP.projectTo(qfldPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          // Why? Because the call is ambiguous otherwise!
//          qfldElemP.eval(0.75, q1);
//          qfldElemPp1.eval(0.75, q2);
//          BOOST_CHECK_CLOSE(q1, q2, 1e-12);
        }
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_BoundaryFace_LegendreP0 )
{
  typedef DLA::VectorS<3, Real> ArrayQ;
  typedef Field_DG_BoundaryTrace< PhysD3, TopoD3, ArrayQ > QField3D_DG_BoundaryFace;

  XField3D_2Tet_X1_1Group xfld1;

  int order = 0;
  QField3D_DG_BoundaryFace qfld1(xfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 6, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 6, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  BOOST_CHECK_THROW( qfld1.nDOFCellGroup(0), SANSException);
  BOOST_CHECK_THROW( qfld1.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_EQUAL( 1, qfld1.nDOFBoundaryTraceGroup(0) );

  Field< PhysD3, TopoD3, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );

  BOOST_CHECK_THROW( qfld3.nDOFCellGroup(0), DeveloperException);
  BOOST_CHECK_THROW( qfld3.nDOFInteriorTraceGroup(0), DeveloperException);
  BOOST_CHECK_THROW( qfld3.nDOFBoundaryTraceGroup(0), DeveloperException);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_BoundaryFace_Legendre_ProjectPtoPp1 )
{
  typedef DLA::VectorS<3, Real> ArrayQ;
  typedef Field_DG_BoundaryTrace< PhysD3, TopoD3, ArrayQ > QField3D_DG_BoundaryFace;
  typedef QField3D_DG_BoundaryFace::FieldTraceGroupType<Triangle> QFieldAreaClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldAreaClass;

  ArrayQ q1, q2;

  XField3D_6Tet_X1_1Group xfld1;

  for (int order = 0; order < BasisFunctionArea_Triangle_LegendrePMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionArea_Triangle_LegendrePMax-order; orderinc++)
    {
      QField3D_DG_BoundaryFace qfldP(xfld1, order  , BasisFunctionCategory_Legendre);
      QField3D_DG_BoundaryFace qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Legendre);

      for ( int group = 0; group < qfldP.nBoundaryTraceGroups(); group++ )
      {
        QFieldAreaClass& qfldAreaGroupP   = qfldP.getBoundaryTraceGroup<Triangle>(group);
        QFieldAreaClass& qfldAreaGroupPp1 = qfldPp1.getBoundaryTraceGroup<Triangle>(group);

        ElementQFieldAreaClass qfldElemP(qfldAreaGroupP.basis());
        ElementQFieldAreaClass qfldElemPp1(qfldAreaGroupPp1.basis());

        //Give some non-zero initial condition
        for (int n = 0; n < qfldP.nDOF(); n++)
          qfldP.DOF(n) = n+1;

        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = 0;

        //Use line function projectTo
        qfldAreaGroupP.projectTo(qfldAreaGroupPp1);

        for (int elem = 0; elem < qfldAreaGroupP.nElem(); elem++)
        {
          qfldAreaGroupP.getElement(qfldElemP, elem);
          qfldAreaGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.25, 0.25, q1);
          qfldElemPp1.eval(0.25, 0.25, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }

        //Wipe out DOF's for P1
        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = -1;

        //Use base function projectTo
        qfldP.projectTo(qfldPp1);

        for (int elem = 0; elem < qfldAreaGroupP.nElem(); elem++)
        {
          qfldAreaGroupP.getElement(qfldElemP, elem);
          qfldAreaGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.75, 0.75, q1);
          qfldElemPp1.eval(0.75, 0.75, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Exception )
{
  typedef Real ArrayQ;
  typedef Field_DG_Cell< PhysD3, TopoD3, DLA::VectorS<3, ArrayQ> > QField3D_DG_Volume;
  typedef Field_DG_InteriorTrace< PhysD3, TopoD3, ArrayQ > QField3D_DG_InteriorFace;
  typedef Field_DG_BoundaryTrace< PhysD3, TopoD3, ArrayQ > QField3D_DG_BoundaryFace;
  typedef Field_DG_Trace< PhysD3, TopoD3, ArrayQ > QField3D_DG_Face;

  XField3D_2Tet_X1_1Group xfld1;

  int order = 999; //This should always be an order that is not available

  BOOST_CHECK_THROW( QField3D_DG_Volume qfld1(xfld1, order, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( QField3D_DG_Volume qfld1(xfld1, order, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( QField3D_DG_InteriorFace qfld1(xfld1, order, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( QField3D_DG_BoundaryFace qfld1(xfld1, order, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( QField3D_DG_Face qfld1(xfld1, order, BasisFunctionCategory_Hierarchical), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  typedef Real ArrayQ;
  typedef Field_DG_Cell< PhysD3, TopoD3, DLA::VectorS<3, ArrayQ> > QField3D_DG_Volume;
  typedef Field_DG_InteriorTrace< PhysD3, TopoD3, ArrayQ > QField3D_DG_InteriorFace;
  typedef Field_DG_Trace< PhysD3, TopoD3, ArrayQ > QField3D_DG_Face;
  typedef Field_DG_BoundaryTrace< PhysD3, TopoD3, ArrayQ > QField3D_DG_BoundaryFace;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/Field3D_HDG_pattern.txt", true );

  XField3D_2Tet_X1_1Group xfld1;

  QField3D_DG_Volume qfld1(xfld1, 1, BasisFunctionCategory_Hierarchical);
  qfld1 = 0;
  qfld1.dump( 2, output );

  QField3D_DG_InteriorFace qfld2(xfld1, 2, BasisFunctionCategory_Hierarchical);
  qfld2 = 0;
  qfld2.dump( 2, output );

  QField3D_DG_BoundaryFace qfld3(xfld1, 2, BasisFunctionCategory_Hierarchical);
  qfld3 = 0;
  qfld3.dump( 2, output );

  QField3D_DG_Face qfld4(xfld1, 2, BasisFunctionCategory_Hierarchical);
  qfld4 = 0;
  qfld4.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
