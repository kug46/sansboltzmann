// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// FieldLiftAssociativityLine_btest
// testing of FieldLiftAssociativity for a field of Line elements

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "Field/FieldLift.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "Field/FieldLiftAssociativity.h"
#include "Field/FieldLiftGroupLine_Traits.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class FieldLiftAssociativity< FieldLiftGroupLineTraits<Real> >;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( FieldLiftAssociativityLine_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  typedef FieldLiftAssociativity< FieldLiftGroupLineTraits<Real> > FieldLineClass;

  static_assert( std::is_same<typename FieldLineClass::FieldBase, FieldLiftAssociativityBase<Real> >::value, "Incorrect Base type" );
  static_assert( std::is_same<typename FieldLineClass::BasisType, BasisFunctionLineBase >::value, "Incorrect Basis type" );
  static_assert( std::is_same<typename FieldLineClass::TopologyType, Line>::value, "Incorrect topology type" );
  static_assert( std::is_same<typename FieldLineClass::FieldLiftAssociativityConstructorType,
                              FieldLiftAssociativityConstructor<ElementAssociativityConstructor<TopoD1, Line>> >::value,
                 "Incorrect Associativity Constructor type" );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef FieldLiftAssociativity< FieldLiftGroupLineTraits<Real> > FieldLineClass;
  typedef typename FieldLineClass::FieldLiftAssociativityConstructorType FieldLiftAssociativityConstructorType;
  typedef typename FieldLineClass::FieldBase FieldLineBase;

  int order = 1;
  ElementAssociativityConstructor<TopoD1, Line> assoc1(order);
  ElementAssociativityConstructor<TopoD1, Line> assoc2(order);

  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( 2, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );

  assoc1.setRank( 0 );
  assoc2.setRank( 0 );

  int node1[2] = {3, 4};
  int node2[2] = {5, 6};

  assoc1.setNodeGlobalMapping( node1, 2 );
  assoc2.setNodeGlobalMapping( node2, 2 );

  int nElem = 1;
  FieldLiftAssociativityConstructorType fldassoc1(BasisFunctionLineBase::HierarchicalP1, nElem);

  fldassoc1.setAssociativity(assoc1, 0, 0);
  fldassoc1.setAssociativity(assoc2, 0, 1);

  // constructor class
  FieldLineClass fld1(fldassoc1);

  BOOST_CHECK_EQUAL( 1, fld1.nElem() );
  BOOST_CHECK_EQUAL( 0, fld1.nDOF() );

  BOOST_CHECK( fld1.topoTypeID() == typeid(Line) );

  // default ctor and operator=
  FieldLineClass fld2;

  BOOST_CHECK_EQUAL( 0, fld2.nElem() );
  BOOST_CHECK_EQUAL( 0, fld2.nDOF() );

  fld2 = fld1;

  BOOST_CHECK_EQUAL( 1, fld2.nElem() );
  BOOST_CHECK_EQUAL( 0, fld2.nDOF() );

  BOOST_CHECK( fld2.topoTypeID() == typeid(Line) );

  // copy ctor
  FieldLineClass fld3(fld1);

  BOOST_CHECK_EQUAL( 1, fld3.nElem() );
  BOOST_CHECK_EQUAL( 0, fld3.nDOF() );

  BOOST_CHECK( fld3.topoTypeID() == typeid(Line) );

  // resize needed for new []
  FieldLineClass fld4;

  fld4.resize(fldassoc1);

  BOOST_CHECK_EQUAL( 1, fld4.nElem() );
  BOOST_CHECK_EQUAL( 0, fld4.nDOF() );

  BOOST_CHECK( fld4.topoTypeID() == typeid(Line) );

  // clone
  FieldLineBase* fld5 = fld1.clone();

  BOOST_CHECK_EQUAL( 1, fld5->nElem() );
  BOOST_CHECK_EQUAL( 0, fld5->nDOF() );

  BOOST_CHECK( fld5->topoTypeID() == typeid(Line) );
  delete fld5;
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis )
{
  typedef FieldLiftAssociativity< FieldLiftGroupLineTraits<Real> > FieldLineClass;
  typedef typename FieldLineClass::FieldLiftAssociativityConstructorType FieldLiftAssociativityConstructorType;

  int order = 1;
  ElementAssociativityConstructor<TopoD1, Line> assoc1(order);
  ElementAssociativityConstructor<TopoD1, Line> assoc2(order);

  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( 2, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );

  assoc1.setRank( 0 );
  assoc2.setRank( 0 );

  int node1[2] = {3, 4};
  int node2[2] = {5, 6};

  assoc1.setNodeGlobalMapping( node1, 2 );
  assoc2.setNodeGlobalMapping( node2, 2 );

  int nElem = 1;
  FieldLiftAssociativityConstructorType fldassoc1(BasisFunctionLineBase::HierarchicalP1, nElem);

  fldassoc1.setAssociativity(assoc1, 0, 0);
  fldassoc1.setAssociativity(assoc2, 0, 1);

  FieldLineClass fld(fldassoc1);

  BOOST_CHECK_EQUAL( 1, fld.nElem() );
  BOOST_CHECK_EQUAL( 0, fld.nDOF() );

  const BasisFunctionLineBase* basis2 = fld.basis();

  BOOST_CHECK_EQUAL( 1, basis2->order() );
  BOOST_CHECK_EQUAL( 2, basis2->nBasis() );
  BOOST_CHECK_EQUAL( 2, basis2->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis2->nBasisEdge() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DOF_associativity_P1 )
{
  typedef FieldLiftAssociativity< FieldLiftGroupLineTraits<Real> > FieldLineClass;
  typedef typename FieldLineClass::FieldLiftAssociativityConstructorType FieldLiftAssociativityConstructorType;

  ElementAssociativityConstructor<TopoD1, Line> assoc1(BasisFunctionLineBase::HierarchicalP1);
  ElementAssociativityConstructor<TopoD1, Line> assoc2(BasisFunctionLineBase::HierarchicalP1);

  BOOST_CHECK_EQUAL( 1, assoc1.order() );
  BOOST_CHECK_EQUAL( 2, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 0, assoc1.nEdge() );

  assoc1.setRank( 0 );
  assoc2.setRank( 0 );

  int nodeTrue1[2] = {3, 4};
  int nodeTrue2[2] = {5, 6};
  assoc1.setNodeGlobalMapping( nodeTrue1, 2 );
  assoc2.setNodeGlobalMapping( nodeTrue2, 2 );

  int nElem = 1;
  FieldLiftAssociativityConstructorType fldassoc1(BasisFunctionLineBase::HierarchicalP1, nElem);

  fldassoc1.setAssociativity(assoc1, 0, 0);
  fldassoc1.setAssociativity(assoc2, 0, 1);

  FieldLineClass fld1(fldassoc1);

  BOOST_CHECK_EQUAL( 1, fld1.nElem() );
  BOOST_CHECK_EQUAL( 0, fld1.nDOF() );

  int node1[2];
  fld1.associativity( 0, 0 ).getNodeGlobalMapping( node1, 2 );
  BOOST_CHECK_EQUAL( nodeTrue1[0], node1[0] );
  BOOST_CHECK_EQUAL( nodeTrue1[1], node1[1] );

  int node2[2];
  fld1.associativity( 0, 1 ).getNodeGlobalMapping( node2, 2 );
  BOOST_CHECK_EQUAL( nodeTrue2[0], node2[0] );
  BOOST_CHECK_EQUAL( nodeTrue2[1], node2[1] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DOF_associativity_P2 )
{
  typedef FieldLiftAssociativity< FieldLiftGroupLineTraits<Real> > FieldLineClass;
  typedef typename FieldLineClass::FieldLiftAssociativityConstructorType FieldLiftAssociativityConstructorType;

  ElementAssociativityConstructor<TopoD1, Line> assoc1(BasisFunctionLineBase::HierarchicalP2);
  ElementAssociativityConstructor<TopoD1, Line> assoc2(BasisFunctionLineBase::HierarchicalP2);

  BOOST_CHECK_EQUAL( 2, assoc1.order() );
  BOOST_CHECK_EQUAL( 2, assoc1.nNode() );
  BOOST_CHECK_EQUAL( 1, assoc1.nEdge() );

  assoc1.setRank( 0 );
  assoc2.setRank( 0 );

  int nodeTrue1[2] = {3, 4};
  int edgeTrue1[1] = {8};
  assoc1.setNodeGlobalMapping( nodeTrue1, 2 );
  assoc1.setEdgeGlobalMapping( edgeTrue1, 1 );

  int nodeTrue2[2] = {5, 6};
  int edgeTrue2[1] = {9};
  assoc2.setNodeGlobalMapping( nodeTrue2, 2 );
  assoc2.setEdgeGlobalMapping( edgeTrue2, 1 );

  int nElem = 1;
  FieldLiftAssociativityConstructorType fldassoc1(BasisFunctionLineBase::HierarchicalP2, nElem);

  fldassoc1.setAssociativity(assoc1, 0, 0);
  fldassoc1.setAssociativity(assoc2, 0, 1);

  FieldLineClass fld1(fldassoc1);

  BOOST_CHECK_EQUAL( 1, fld1.nElem() );
  BOOST_CHECK_EQUAL( 0, fld1.nDOF() );


  int node1[2], edge1[1];
  fld1.associativity( 0, 0 ).getNodeGlobalMapping( node1, 2 );
  BOOST_CHECK_EQUAL( 3, node1[0] );
  BOOST_CHECK_EQUAL( 4, node1[1] );

  fld1.associativity( 0, 0 ).getEdgeGlobalMapping( edge1, 1 );
  BOOST_CHECK_EQUAL( 8, edge1[0] );

  int node2[2], edge2[1];
  fld1.associativity( 0, 1 ).getNodeGlobalMapping( node2, 2 );
  BOOST_CHECK_EQUAL( 5, node2[0] );
  BOOST_CHECK_EQUAL( 6, node2[1] );

  fld1.associativity( 0, 1 ).getEdgeGlobalMapping( edge2, 1 );
  BOOST_CHECK_EQUAL( 9, edge2[0] );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( accessors )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef FieldLiftAssociativity< FieldLiftGroupLineTraits<ArrayQ> > FieldLineClass;
  typedef typename FieldLineClass::FieldLiftAssociativityConstructorType FieldLiftAssociativityConstructorType;

  ElementAssociativityConstructor<TopoD1, Line> assoc1(BasisFunctionLineBase::HierarchicalP1);
  ElementAssociativityConstructor<TopoD1, Line> assoc2(BasisFunctionLineBase::HierarchicalP1);

  assoc1.setRank( 0 );
  assoc2.setRank( 0 );

  int nodeTrue1[2] = {3, 4};
  int nodeTrue2[2] = {5, 6};
  assoc1.setNodeGlobalMapping( nodeTrue1, 2 );
  assoc2.setNodeGlobalMapping( nodeTrue2, 2 );

  int nElem = 1;
  FieldLiftAssociativityConstructorType fldassoc1(BasisFunctionLineBase::HierarchicalP1, nElem);

  fldassoc1.setAssociativity(assoc1, 0, 0);
  fldassoc1.setAssociativity(assoc2, 0, 1);

  FieldLineClass qfld(fldassoc1);

  BOOST_CHECK_EQUAL( 1, qfld.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld.nDOF() );

  ArrayQ* qDOF = new ArrayQ[2];

  qfld.setDOF( qDOF, 2 );
  BOOST_CHECK_EQUAL( 2, qfld.nDOF() );

  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};

  qfld.DOF(0) = q1;
  qfld.DOF(1) = q2;

  const Real tol = 1e-13;
  ArrayQ q;
  for (int k = 0; k < 2; k++)
  {
    q = qfld.DOF(0);
    BOOST_CHECK_CLOSE( q1[k]     , q[k], tol );
    BOOST_CHECK_CLOSE( qDOF[0][k], q[k], tol );
    q = qfld.DOF(1);
    BOOST_CHECK_CLOSE( q2[k]     , q[k], tol );
    BOOST_CHECK_CLOSE( qDOF[1][k], q[k], tol );
  }

  delete [] qDOF;
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( elementfield )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef FieldLiftAssociativity< FieldLiftGroupLineTraits<ArrayQ> > FieldLineClass;
  typedef typename FieldLineClass::FieldLiftAssociativityConstructorType FieldLiftAssociativityConstructorType;

  const Real tol = 1e-13;

  ElementAssociativityConstructor<TopoD1, Line> assoc1(BasisFunctionLineBase::HierarchicalP1);
  ElementAssociativityConstructor<TopoD1, Line> assoc2(BasisFunctionLineBase::HierarchicalP1);

  assoc1.setRank( 0 );
  assoc2.setRank( 0 );

  int node1[2] = {3, 4};
  int node2[2] = {5, 6};
  assoc1.setNodeGlobalMapping( node1, 2 );
  assoc2.setNodeGlobalMapping( node2, 2 );

  int nElem = 1;
  FieldLiftAssociativityConstructorType fldassoc1(BasisFunctionLineBase::HierarchicalP1, nElem);

  fldassoc1.setAssociativity(assoc1, 0, 0);
  fldassoc1.setAssociativity(assoc2, 0, 1);

  FieldLineClass qfld(fldassoc1);

  // solution DOFs
  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};
  ArrayQ q3 = {5, 6};
  ArrayQ q4 = {7, 8};

  ArrayQ* qDOF = new ArrayQ[7];

  qfld.setDOF( qDOF, 5 );
  BOOST_CHECK_EQUAL( 5, qfld.nDOF() );

  qfld.DOF(node1[0]) = q1;
  qfld.DOF(node1[1]) = q2;
  qfld.DOF(node2[0]) = q3;
  qfld.DOF(node2[1]) = q4;

  // get element DOFs

  typename FieldLineClass::template ElementType<> qfldElem( qfld.basis() );

  qfld.getElement( qfldElem, 0, 0 );

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  ArrayQ q;
  for (int k = 0; k < 2; k++)
  {
    q = qfldElem.DOF(0);
    BOOST_CHECK_CLOSE( q1[k], q[k], tol );
    q = qfldElem.DOF(1);
    BOOST_CHECK_CLOSE( q2[k], q[k], tol );
  }

  qfld.getElement( qfldElem, 0, 1 );

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  for (int k = 0; k < 2; k++)
  {
    q = qfldElem.DOF(0);
    BOOST_CHECK_CLOSE( q3[k], q[k], tol );
    q = qfldElem.DOF(1);
    BOOST_CHECK_CLOSE( q4[k], q[k], tol );
  }

  delete [] qDOF;
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( projection_Hierarchical )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef FieldLiftAssociativity< FieldLiftGroupLineTraits<ArrayQ> > FieldLineClass;
  typedef typename FieldLineClass::FieldLiftAssociativityConstructorType FieldLiftAssociativityConstructorType;
  typedef typename FieldLineClass::template ElementType<> ElementLineClass;

  const Real tol = 1e-15;

  for (int order = 1; order < BasisFunctionLine_HierarchicalPMax; order++)
  {
    for (int inc = 1; inc <= BasisFunctionLine_HierarchicalPMax-order; inc++)
    {
      const BasisFunctionLineBase* basis0 =
            BasisFunctionLineBase::getBasisFunction( order      , BasisFunctionCategory_Hierarchical );
      const BasisFunctionLineBase* basis1 =
            BasisFunctionLineBase::getBasisFunction( order + inc, BasisFunctionCategory_Hierarchical );

      const int nDOF0 = basis0->nBasis();
      const int nDOF1 = basis1->nBasis();

      // DOF associativity
      ElementAssociativityConstructor<TopoD1, Line> assoc01( basis0 );
      ElementAssociativityConstructor<TopoD1, Line> assoc02( basis0 );
      ElementAssociativityConstructor<TopoD1, Line> assoc11( basis1 );
      ElementAssociativityConstructor<TopoD1, Line> assoc12( basis1 );

      assoc01.setRank( 0 );
      assoc02.setRank( 0 );
      assoc11.setRank( 0 );
      assoc12.setRank( 0 );

      int* map01 = new int[ nDOF0 ];
      int* map02 = new int[ nDOF0 ];
      int* map11 = new int[ nDOF1 ];
      int* map12 = new int[ nDOF1 ];
      for (int k = 0; k < nDOF0; k++)
      {
        map01[k] = k;
        map02[k] = k+nDOF0;
      }
      for (int k = 0; k < nDOF1; k++)
      {
        map11[k] = nDOF1 - 1 - k;
        map12[k] = nDOF1 - 1 - k + nDOF1;
      }

      assoc01.setGlobalMapping( map01, nDOF0 );
      assoc02.setGlobalMapping( map02, nDOF0 );
      assoc11.setGlobalMapping( map11, nDOF1 );
      assoc12.setGlobalMapping( map12, nDOF1 );

      int nElem = 1;
      FieldLiftAssociativityConstructorType fldassoc0(basis0, nElem);
      FieldLiftAssociativityConstructorType fldassoc1(basis1, nElem);

      fldassoc0.setAssociativity(assoc01, 0, 0);
      fldassoc0.setAssociativity(assoc02, 0, 1);
      fldassoc1.setAssociativity(assoc11, 0, 0);
      fldassoc1.setAssociativity(assoc12, 0, 1);

      FieldLineClass qfld0( fldassoc0 );
      FieldLineClass qfld1( fldassoc1 );

      // solution DOFs
      ArrayQ* qDOF0 = new ArrayQ[ 2*nDOF0 ];
      ArrayQ* qDOF1 = new ArrayQ[ 2*nDOF1 ];
      qfld0.setDOF( qDOF0, 2*nDOF0 );
      qfld1.setDOF( qDOF1, 2*nDOF1 );

      for (int k = 0; k < 2*nDOF0; k++)
      {
        ArrayQ q = { sqrt(k+7), 1./(k+4) };
        qfld0.DOF(k) = q;
      }

      qfld0.projectTo( qfld1 );

      ElementLineClass qfldElem0( basis0 );
      ElementLineClass qfldElem1( basis1 );
      for ( int cononicalTrace = 0; cononicalTrace < 2; cononicalTrace++ )
      {
        qfld0.getElement( qfldElem0, 0, cononicalTrace );
        qfld1.getElement( qfldElem1, 0, cononicalTrace );

        ArrayQ q0, q1;
        Real s;

        for (int is = 0; is < 3; is++)
        {
          s = 0.11 + is*0.3357;

          qfldElem0.eval( s, q0 );
          qfldElem1.eval( s, q1 );

          for (int n = 0; n < ArrayQ::N; n++)
            BOOST_CHECK_CLOSE( q0[n], q1[n], tol );
        }
      }

      delete [] map01;
      delete [] map02;
      delete [] map11;
      delete [] map12;
      delete [] qDOF0;
      delete [] qDOF1;
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( projection_Legendre )
{
  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef FieldLiftAssociativity< FieldLiftGroupLineTraits<ArrayQ> > FieldLineClass;
  typedef typename FieldLineClass::FieldLiftAssociativityConstructorType FieldLiftAssociativityConstructorType;
  typedef typename FieldLineClass::template ElementType<> ElementLineClass;

  const Real tol = 1e-15;

  for (int order = 1; order < BasisFunctionLine_LegendrePMax; order++)
  {
    for (int inc = 1; inc <= BasisFunctionLine_LegendrePMax-order; inc++)
    {
      const BasisFunctionLineBase* basis0 =
            BasisFunctionLineBase::getBasisFunction( order      , BasisFunctionCategory_Legendre );
      const BasisFunctionLineBase* basis1 =
            BasisFunctionLineBase::getBasisFunction( order + inc, BasisFunctionCategory_Legendre );

      const int nDOF0 = basis0->nBasis();
      const int nDOF1 = basis1->nBasis();

      // DOF associativity
      ElementAssociativityConstructor<TopoD1, Line> assoc01( basis0 );
      ElementAssociativityConstructor<TopoD1, Line> assoc02( basis0 );
      ElementAssociativityConstructor<TopoD1, Line> assoc11( basis1 );
      ElementAssociativityConstructor<TopoD1, Line> assoc12( basis1 );

      assoc01.setRank( 0 );
      assoc02.setRank( 0 );
      assoc11.setRank( 0 );
      assoc12.setRank( 0 );

      int* map01 = new int[ nDOF0 ];
      int* map02 = new int[ nDOF0 ];
      int* map11 = new int[ nDOF1 ];
      int* map12 = new int[ nDOF1 ];
      for (int k = 0; k < nDOF0; k++)
      {
        map01[k] = k;
        map02[k] = k+nDOF0;
      }
      for (int k = 0; k < nDOF1; k++)
      {
        map11[k] = nDOF1 - 1 - k;
        map12[k] = nDOF1 - 1 - k + nDOF1;
      }

      assoc01.setGlobalMapping( map01, nDOF0 );
      assoc02.setGlobalMapping( map02, nDOF0 );
      assoc11.setGlobalMapping( map11, nDOF1 );
      assoc12.setGlobalMapping( map12, nDOF1 );

      int nElem = 1;
      FieldLiftAssociativityConstructorType fldassoc0(basis0, nElem);
      FieldLiftAssociativityConstructorType fldassoc1(basis1, nElem);

      fldassoc0.setAssociativity(assoc01, 0, 0);
      fldassoc0.setAssociativity(assoc02, 0, 1);
      fldassoc1.setAssociativity(assoc11, 0, 0);
      fldassoc1.setAssociativity(assoc12, 0, 1);

      FieldLineClass qfld0( fldassoc0 );
      FieldLineClass qfld1( fldassoc1 );

      // solution DOFs
      ArrayQ* qDOF0 = new ArrayQ[ 2*nDOF0 ];
      ArrayQ* qDOF1 = new ArrayQ[ 2*nDOF1 ];
      qfld0.setDOF( qDOF0, 2*nDOF0 );
      qfld1.setDOF( qDOF1, 2*nDOF1 );

      for (int k = 0; k < 2*nDOF0; k++)
      {
        ArrayQ q = { sqrt(k+7), 1./(k+4) };
        qfld0.DOF(k) = q;
      }

      qfld0.projectTo( qfld1 );

      ElementLineClass qfldElem0( basis0 );
      ElementLineClass qfldElem1( basis1 );
      for ( int cononicalTrace = 0; cononicalTrace < 2; cononicalTrace++ )
      {
        qfld0.getElement( qfldElem0, 0, cononicalTrace );
        qfld1.getElement( qfldElem1, 0, cononicalTrace );

        ArrayQ q0, q1;
        Real s;

        for (int is = 0; is < 3; is++)
        {
          s = 0.11 + is*0.3357;

          qfldElem0.eval( s, q0 );
          qfldElem1.eval( s, q1 );

          for (int n = 0; n < ArrayQ::N; n++)
            BOOST_CHECK_CLOSE( q0[n], q1[n], tol );
        }
      }

      delete [] map01;
      delete [] map02;
      delete [] map11;
      delete [] map12;
      delete [] qDOF0;
      delete [] qDOF1;
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/FieldLiftAssociativityLine_pattern.txt", true );

  typedef DLA::VectorS< 2, Real > ArrayQ;
  typedef FieldLiftAssociativity< FieldLiftGroupLineTraits<ArrayQ> > FieldLineClass;
  typedef typename FieldLineClass::FieldLiftAssociativityConstructorType FieldLiftAssociativityConstructorType;

  ElementAssociativityConstructor<TopoD1, Line> assoc1(BasisFunctionLineBase::HierarchicalP1);
  ElementAssociativityConstructor<TopoD1, Line> assoc2(BasisFunctionLineBase::HierarchicalP1);

  assoc1.setRank( 0 );
  assoc2.setRank( 0 );

  int node1[2] = {0, 1};
  int node2[2] = {2, 3};
  assoc1.setNodeGlobalMapping( node1, 2 );
  assoc2.setNodeGlobalMapping( node2, 2 );

  int nElem = 1;
  FieldLiftAssociativityConstructorType fldassoc1(BasisFunctionLineBase::HierarchicalP1, nElem);

  fldassoc1.setAssociativity(assoc1, 0, 0);
  fldassoc1.setAssociativity(assoc2, 0, 1);

  FieldLineClass qfld(fldassoc1);

  // solution DOFs
  ArrayQ q1 = {1, 2};
  ArrayQ q2 = {3, 4};
  ArrayQ q3 = {5, 6};
  ArrayQ q4 = {6, 7};

  ArrayQ* qDOF = new ArrayQ[4];

  qfld.setDOF( qDOF, 4 );
  BOOST_CHECK_EQUAL( 4, qfld.nDOF() );

  qfld.DOF(node1[0]) = q1;
  qfld.DOF(node1[1]) = q2;
  qfld.DOF(node2[0]) = q3;
  qfld.DOF(node2[1]) = q4;

  qfld.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  delete [] qDOF;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
