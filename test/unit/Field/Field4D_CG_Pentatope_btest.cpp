// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field3D_CG_btest
// testing of Field3D_CG_* classes
//

//#define SLEEP_FOR_OUTPUT
#ifdef SLEEP_FOR_OUTPUT
#define SLEEP_MILLISECONDS 500
#include <chrono>
#include <thread>
#endif

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;


#include "unit/UnitGrids/XField_KuhnFreudenthal.h"
#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"
#include "unit/UnitGrids/XField4D_1Ptope_X1_1Group.h"
#include "unit/UnitGrids/XField4D_2Ptope_X1_1Group.h"
#include "unit/UnitGrids/XField4D_24Ptope_X1_1Group.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/output_std_vector.h"

#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/BasisFunctionSpacetime_Pentatope.h"

#include "BasisFunction/TraceToCellRefCoord.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldSpacetime_CG_Cell.h"
#include "Field/FieldSpacetime_CG_BoundaryTrace.h"
#include "Field/Field_CG/Field_CG_Topology.h"

#ifdef SANS_MPI
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/mpi/collectives/all_gather.hpp>
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#define LIMITP 1

using namespace std;
using namespace SANS;


// explicitly instantiate classes to get proper coverage information
namespace SANS
{
template class Field_CG_Cell< PhysD4, TopoD4, Real >;
template class Field_CG_BoundaryTrace< PhysD4, TopoD4, Real >;
}

static inline int factorial(int n)
{
  int m;
  if (n <= 1)
    m= 1;
  else
    m= n*factorial(n - 1);

  return m;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( Field4D_CG_Pentatope_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_Spacetime_P1 )
{
  typedef DLA::VectorS<3, Real> ArrayQ;
  typedef Field_CG_Cell<PhysD4, TopoD4, ArrayQ> QField4D_CG_Spacetime;
  typedef XField4D_2Ptope_X1_1Group::FieldCellGroupType<Pentatope> XFieldSpacetimeClass;
  typedef QField4D_CG_Spacetime::FieldCellGroupType<Pentatope> QFieldSpacetimeClass;
  typedef QFieldSpacetimeClass::ElementType<> ElementQFieldClass;
  typedef std::array<int, 5> Int5;

  int nodeMap[5];

  int order= 1;
  int dim= PhysD4::D;

  XField4D_1Ptope_X1_1Group xfld0;

  const XFieldSpacetimeClass &xfldGroup0= xfld0.getCellGroup<Pentatope>(0);

  QField4D_CG_Spacetime qfld0(xfld0, order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL(qfld0.nDOF(), dim + 1);
  BOOST_CHECK_EQUAL(qfld0.nElem(), 1);
  BOOST_CHECK_EQUAL(qfld0.nInteriorTraceGroups(), 0);
  BOOST_CHECK_EQUAL(qfld0.nBoundaryTraceGroups(), dim + 1);
  BOOST_CHECK_EQUAL(qfld0.nCellGroups(), 1);
  BOOST_CHECK_EQUAL(&qfld0.getXField(), &xfld0);

  const QFieldSpacetimeClass &qfldGroup0= qfld0.getCellGroup<Pentatope>(0);

  BOOST_CHECK_EQUAL(qfldGroup0.nElem(), 1);

  BOOST_CHECK_EQUAL(qfldGroup0.associativity(0).nNode(), dim + 1);  // order 1 has just DOFs on nodes
  BOOST_CHECK_EQUAL(qfldGroup0.associativity(0).nEdge(), 0);
  BOOST_CHECK_EQUAL(qfldGroup0.associativity(0).nFace(), 0);
  BOOST_CHECK_EQUAL(qfldGroup0.associativity(0).nCell(), 0);

  qfldGroup0.associativity(0).getNodeGlobalMapping(nodeMap, 5);
  BOOST_CHECK_EQUAL(nodeMap[0], 0);
  BOOST_CHECK_EQUAL(nodeMap[1], 1);
  BOOST_CHECK_EQUAL(nodeMap[2], 2);
  BOOST_CHECK_EQUAL(nodeMap[3], 3);
  BOOST_CHECK_EQUAL(nodeMap[4], 4);

  Int5 xfaceSign;
  Int5 qfaceSign;

  xfaceSign= xfldGroup0.associativity(0).faceSign();
  qfaceSign= qfldGroup0.associativity(0).faceSign();
  BOOST_CHECK_EQUAL(xfaceSign[0], qfaceSign[0]);
  BOOST_CHECK_EQUAL(xfaceSign[1], qfaceSign[1]);
  BOOST_CHECK_EQUAL(xfaceSign[2], qfaceSign[2]);
  BOOST_CHECK_EQUAL(xfaceSign[3], qfaceSign[3]);
  BOOST_CHECK_EQUAL(xfaceSign[4], qfaceSign[4]);

  XField4D_2Ptope_X1_1Group xfld1;

  const XFieldSpacetimeClass &xfldGroup1= xfld1.getCellGroup<Pentatope>(0);

  QField4D_CG_Spacetime qfld1(xfld1, order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL(qfld1.nDOF(), (dim + 1) + 1);
  BOOST_CHECK_EQUAL(qfld1.nElem(), 2);
  BOOST_CHECK_EQUAL(qfld1.nInteriorTraceGroups(), 0);
  BOOST_CHECK_EQUAL(qfld1.nBoundaryTraceGroups(), 2*dim);
  BOOST_CHECK_EQUAL(qfld1.nCellGroups(), 1);
  BOOST_CHECK_EQUAL(&qfld1.getXField(), &xfld1);

  const QFieldSpacetimeClass &qfldGroup1= qfld1.getCellGroup<Pentatope>(0);

  BOOST_CHECK_EQUAL(qfldGroup1.nElem(), 2);

  BOOST_CHECK_EQUAL(qfldGroup1.associativity(0).nNode(), dim + 1);  // order 1 has just DOFs on nodes
  BOOST_CHECK_EQUAL(qfldGroup1.associativity(0).nEdge(), 0);
  BOOST_CHECK_EQUAL(qfldGroup1.associativity(0).nFace(), 0);
  BOOST_CHECK_EQUAL(qfldGroup1.associativity(0).nCell(), 0);


  BOOST_CHECK_EQUAL(qfldGroup1.associativity(1).nNode(), dim + 1);  // order 1 has just DOFs on nodes
  BOOST_CHECK_EQUAL(qfldGroup1.associativity(1).nEdge(), 0);
  BOOST_CHECK_EQUAL(qfldGroup1.associativity(1).nFace(), 0);
  BOOST_CHECK_EQUAL(qfldGroup1.associativity(1).nCell(), 0);

  qfldGroup1.associativity(0).getNodeGlobalMapping(nodeMap, 5);
  BOOST_CHECK_EQUAL(nodeMap[0], 0);
  BOOST_CHECK_EQUAL(nodeMap[1], 1);
  BOOST_CHECK_EQUAL(nodeMap[2], 2);
  BOOST_CHECK_EQUAL(nodeMap[3], 3);
  BOOST_CHECK_EQUAL(nodeMap[4], 4);

  qfldGroup1.associativity(1).getNodeGlobalMapping(nodeMap, 5);
  BOOST_CHECK_EQUAL(nodeMap[0], 0);
  BOOST_CHECK_EQUAL(nodeMap[1], 5);
  BOOST_CHECK_EQUAL(nodeMap[2], 2);
  BOOST_CHECK_EQUAL(nodeMap[3], 4);
  BOOST_CHECK_EQUAL(nodeMap[4], 3);

  xfaceSign= xfldGroup1.associativity(0).faceSign();
  qfaceSign= qfldGroup1.associativity(0).faceSign();
  BOOST_CHECK_EQUAL(xfaceSign[0], qfaceSign[0]);
  BOOST_CHECK_EQUAL(xfaceSign[1], qfaceSign[1]);
  BOOST_CHECK_EQUAL(xfaceSign[2], qfaceSign[2]);
  BOOST_CHECK_EQUAL(xfaceSign[3], qfaceSign[3]);
  BOOST_CHECK_EQUAL(xfaceSign[4], qfaceSign[4]);

  xfaceSign= xfldGroup1.associativity(1).faceSign();
  qfaceSign= qfldGroup1.associativity(1).faceSign();
  BOOST_CHECK_EQUAL(xfaceSign[0], qfaceSign[0]);
  BOOST_CHECK_EQUAL(xfaceSign[1], qfaceSign[1]);
  BOOST_CHECK_EQUAL(xfaceSign[2], qfaceSign[2]);
  BOOST_CHECK_EQUAL(xfaceSign[3], qfaceSign[3]);
  BOOST_CHECK_EQUAL(xfaceSign[4], qfaceSign[4]);

  XField4D_24Ptope_X1_1Group xfld2;

  const int nDim= 4;
  const int nPentatope= 24;
//  const int nVertex= 16;
//  const int nBoundaries= 8;
//  const int nBoundTet= 6;

//  // these were the vertices that were
//  // dumped out of XField_KuhnFreudenthal
//  const int DOFarray[nVertex][nDim]=
//    {{0, 0, 0, 0}, {0, 0, 0, 1}, {0, 0, 1, 0}, {0, 0, 1, 1},
//     {0, 1, 0, 0}, {0, 1, 0, 1}, {0, 1, 1, 0}, {0, 1, 1, 1},
//     {1, 0, 0, 0}, {1, 0, 0, 1}, {1, 0, 1, 0}, {1, 0, 1, 1},
//     {1, 1, 0, 0}, {1, 1, 0, 1}, {1, 1, 1, 0}, {1, 1, 1, 1}};

  // these were the vertices that were
  // dumped out of XField_KuhnFreudenthal
  const int simplexVertices[nPentatope][nDim + 1]=
    {{0, 8, 10, 15, 14}, {0, 8, 10, 11, 15}, {0, 8, 9, 13, 15},
     {0, 8, 9, 15, 11}, {0, 2, 10, 14, 15}, {0, 2, 10, 15, 11},
     {0, 8, 12, 14, 15}, {0, 8, 12, 15, 13}, {0, 4, 12, 15, 14},
     {0, 4, 12, 13, 15}, {0, 4, 6, 14, 15}, {0, 4, 6, 15, 7},
     {0, 4, 5, 15, 13}, {0, 4, 5, 7, 15}, {0, 2, 6, 15, 14},
     {0, 2, 6, 7, 15}, {0, 2, 3, 11, 15}, {0, 2, 3, 15, 7},
     {0, 1, 9, 15, 13}, {0, 1, 9, 11, 15}, {0, 1, 5, 13, 15},
     {0, 1, 5, 15, 7}, {0, 1, 3, 15, 11}, {0, 1, 3, 7, 15}};

//  // these were the boundary trace groups that were
//  // dumped out of XField_KuhnFreudenthal
//  const int boundaryTraceGroups[nBoundaries][nBoundTet][nDim]=
//  {{{0, 4, 6, 7}, {0, 4, 7, 5}, {0, 2, 7, 6},
//    {0, 2, 3, 7}, {0, 1, 5, 7}, {0, 1, 7, 3}},
//   {{8, 10, 14, 15}, {8, 10, 15, 11}, {8, 9, 15, 13},
//    {8, 9, 11, 15}, {8, 12, 15, 14}, {8, 12, 13, 15}},
//   {{0, 8, 11, 10}, {0, 8, 9, 11}, {0, 2, 10, 11},
//    {0, 2, 11, 3}, {0, 1, 11, 9}, {0, 1, 3, 11}},
//   {{4, 12, 14, 15}, {4, 12, 15, 13}, {4, 6, 15, 14},
//    {4, 6, 7, 15}, {4, 5, 13, 15}, {4, 5, 15, 7}},
//   {{0, 8, 13, 9}, {0, 8, 12, 13}, {0, 4, 13, 12},
//    {0, 4, 5, 13}, {0, 1, 9, 13}, {0, 1, 13, 5}},
//   {{2, 10, 15, 14}, {2, 10, 11, 15}, {2, 6, 14, 15},
//    {2, 6, 15, 7}, {2, 3, 15, 11}, {2, 3, 7, 15}},
//   {{0, 8, 10, 14}, {0, 2, 14, 10}, {0, 8, 14, 12},
//    {0, 4, 12, 14}, {0, 4, 14, 6}, {0, 2, 6, 14}},
//   {{1, 9, 13, 15}, {1, 9, 15, 11}, {1, 5, 15, 13},
//    {1, 5, 7, 15}, {1, 3, 11, 15}, {1, 3, 15, 7}}};

  const XFieldSpacetimeClass &xfldGroup2= xfld2.getCellGroup<Pentatope>(0);

  QField4D_CG_Spacetime qfld2(xfld2, order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL(qfld2.nDOF(), 16);
  BOOST_CHECK_EQUAL(qfld2.nElem(), 24);
  BOOST_CHECK_EQUAL(qfld2.nInteriorTraceGroups(), 0);
  BOOST_CHECK_EQUAL(qfld2.nBoundaryTraceGroups(), 1);
  BOOST_CHECK_EQUAL(qfld2.nCellGroups(), 1);
  BOOST_CHECK_EQUAL(&xfld2, &qfld2.getXField());

  const QFieldSpacetimeClass &qfldGroup2= qfld2.getCellGroup<Pentatope>(0);

  BOOST_CHECK_EQUAL(qfldGroup2.nElem(), nPentatope);

  for (int k= 0; k < nPentatope; k++)
  {
    qfldGroup2.associativity(k).getNodeGlobalMapping(nodeMap, 5);
    BOOST_CHECK_EQUAL(nodeMap[0], simplexVertices[k][0]);
    BOOST_CHECK_EQUAL(nodeMap[1], simplexVertices[k][1]);
    BOOST_CHECK_EQUAL(nodeMap[2], simplexVertices[k][2]);
    BOOST_CHECK_EQUAL(nodeMap[3], simplexVertices[k][3]);
    BOOST_CHECK_EQUAL(nodeMap[4], simplexVertices[k][4]);

    xfaceSign= xfldGroup2.associativity(k).faceSign();
    qfaceSign= qfldGroup2.associativity(k).faceSign();
    BOOST_CHECK_EQUAL(xfaceSign[0], qfaceSign[0]);
    BOOST_CHECK_EQUAL(xfaceSign[1], qfaceSign[1]);
    BOOST_CHECK_EQUAL(xfaceSign[2], qfaceSign[2]);
    BOOST_CHECK_EQUAL(xfaceSign[3], qfaceSign[3]);
    BOOST_CHECK_EQUAL(xfaceSign[4], qfaceSign[4]);

  }

  // test the constant assignment operator
  ElementQFieldClass qfldElem(qfldGroup1.basis());

  qfld1= 1.23;

  ArrayQ q1;

  for (int k= 0; k < qfldGroup1.nElem(); k++)
  {
    qfldGroup1.getElement(qfldElem, k);

    qfldElem.eval(0.2, 0.2, 0.2, 0.2, q1);

    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[2], 1e-12);
  }

  ArrayQ q0= {4.56, 7.89, 12.34};
  qfld1= q0;

  for (int k= 0; k < qfldGroup1.nElem(); k++)
  {
    qfldGroup1.getElement(qfldElem, k);

    qfldElem.eval(0.2, 0.2, 0.2, 0.2, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
    BOOST_CHECK_CLOSE(q0[2], q1[2], 1e-12);
  }

  // test copy constructor
  QField4D_CG_Spacetime qfld3(qfld2, FieldCopy());

  BOOST_CHECK_EQUAL(qfld3.nDOF(), qfld2.nDOF());
  BOOST_CHECK_EQUAL(qfld3.nElem(), qfld2.nElem());
  BOOST_CHECK_EQUAL(qfld3.nInteriorTraceGroups(), qfld2.nInteriorTraceGroups());
  BOOST_CHECK_EQUAL(qfld3.nBoundaryTraceGroups(), qfld2.nBoundaryTraceGroups());
  BOOST_CHECK_EQUAL(qfld3.nCellGroups(), qfld2.nCellGroups());
  BOOST_CHECK_EQUAL(&qfld3.getXField(), &qfld2.getXField());

}

//----------------------------------------------------------------------------//
//BOOST_AUTO_TEST_CASE( CG_Volume_P2 )
//{
//  typedef DLA::VectorS<3, Real> ArrayQ;
//  typedef Field_CG_Cell<PhysD3, TopoD3, ArrayQ> QField3D_CG_Volume;
//  typedef XField3D_2Tet_X1_1Group::FieldCellGroupType<Tet> XFieldVolumeClass;
//  typedef QField3D_CG_Volume::FieldCellGroupType<Tet> QFieldVolumeClass;
//  typedef QFieldVolumeClass::ElementType<> ElementQFieldClass;
//  typedef std::array<int, 4> Int5;
//
//  int nodeMap[4];
//  int edgeMap[6];
//
//  int order= 2;
//  int dim= PhysD3::D;
//
//  int nDOFelem= factorial(order + dim)/factorial(order)/factorial(dim);
//  int nDOFtrace= factorial(order + dim - 1)/factorial(order)/factorial(dim - 1);
//
//  XField3D_1Tet_X1_1Group xfld0;
//
//  const XFieldVolumeClass &xfldGroup0= xfld0.getCellGroup<Tet>(0);
//
//  QField3D_CG_Volume qfld0(xfld0, order, BasisFunctionCategory_Lagrange);
//
//  BOOST_CHECK_EQUAL(qfld0.nDOF(), nDOFelem);
//  BOOST_CHECK_EQUAL(qfld0.nElem(), 1);
//  BOOST_CHECK_EQUAL(qfld0.nInteriorTraceGroups(), 0);
//  BOOST_CHECK_EQUAL(qfld0.nBoundaryTraceGroups(), dim + 1);
//  BOOST_CHECK_EQUAL(qfld0.nCellGroups(), 1);
//  BOOST_CHECK_EQUAL(&qfld0.getXField(), &xfld0);
//
//  const QFieldVolumeClass &qfldGroup0= qfld0.getCellGroup<Tet>(0);
//
//  BOOST_CHECK_EQUAL(qfldGroup0.nElem(), 1);
//
//  BOOST_CHECK_EQUAL(qfldGroup0.associativity(0).nNode(), dim + 1);  // order 2 has DOFs on nodes
//  BOOST_CHECK_EQUAL(qfldGroup0.associativity(0).nEdge(), 6);       // and on edges. thats it
//  BOOST_CHECK_EQUAL(qfldGroup0.associativity(0).nFace(), 0);
//  BOOST_CHECK_EQUAL(qfldGroup0.associativity(0).nCell(), 0);
//
//}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_Spacetime_P2 )
{
  typedef DLA::VectorS<3, Real> ArrayQ;
  typedef Field_CG_Cell<PhysD4, TopoD4, ArrayQ> QField4D_CG_Spacetime;
  typedef XField4D_2Ptope_X1_1Group::FieldCellGroupType<Pentatope> XFieldSpacetimeClass;
  typedef QField4D_CG_Spacetime::FieldCellGroupType<Pentatope> QFieldSpacetimeClass;
//  typedef QFieldSpacetimeClass::ElementType<> ElementQFieldClass;
  typedef std::array<int, 5> Int5;

  int nodeMap[5];
  int edgeMap[10];

  int order= 2;
  int dim= PhysD4::D;

  int nDOFelem= factorial(order + dim)/factorial(order)/factorial(dim);
  int nDOFtrace= factorial(order + dim - 1)/factorial(order)/factorial(dim - 1);

  XField4D_1Ptope_X1_1Group xfld0;

  const XFieldSpacetimeClass &xfldGroup0= xfld0.getCellGroup<Pentatope>(0);

  QField4D_CG_Spacetime qfld0(xfld0, order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL(qfld0.nDOF(), nDOFelem);
  BOOST_CHECK_EQUAL(qfld0.nElem(), 1);
  BOOST_CHECK_EQUAL(qfld0.nInteriorTraceGroups(), 0);
  BOOST_CHECK_EQUAL(qfld0.nBoundaryTraceGroups(), dim + 1);
  BOOST_CHECK_EQUAL(qfld0.nCellGroups(), 1);
  BOOST_CHECK_EQUAL(&qfld0.getXField(), &xfld0);

  const QFieldSpacetimeClass &qfldGroup0= qfld0.getCellGroup<Pentatope>(0);

  BOOST_CHECK_EQUAL(qfldGroup0.nElem(), 1);

  BOOST_CHECK_EQUAL(qfldGroup0.associativity(0).nNode(), dim + 1);  // order 2 has DOFs on nodes
  BOOST_CHECK_EQUAL(qfldGroup0.associativity(0).nEdge(), 10);       // and on edges. thats it
  BOOST_CHECK_EQUAL(qfldGroup0.associativity(0).nArea(), 0);
  BOOST_CHECK_EQUAL(qfldGroup0.associativity(0).nFace(), 0);
  BOOST_CHECK_EQUAL(qfldGroup0.associativity(0).nCell(), 0);

  qfldGroup0.associativity(0).getNodeGlobalMapping(nodeMap, 5);
//  printf("DEBUG!!!!!:\n\tnodeMap=\t/\t%d\n\t\t\t\b{\t%d\n\t\t\t|\t%d\n\t\t\t|\t%d\n\t\t\t\\\t%d\n\n",
//         nodeMap[0], nodeMap[1], nodeMap[2], nodeMap[3], nodeMap[4]);
  // node DOFs should be on the back end, in trivial order for singular element
  BOOST_CHECK_EQUAL(nodeMap[0], 10 + 0);
  BOOST_CHECK_EQUAL(nodeMap[1], 10 + 1);
  BOOST_CHECK_EQUAL(nodeMap[2], 10 + 2);
  BOOST_CHECK_EQUAL(nodeMap[3], 10 + 3);
  BOOST_CHECK_EQUAL(nodeMap[4], 10 + 4);

  qfldGroup0.associativity(0).getEdgeGlobalMapping(edgeMap, 10);
  // edge DOFs lead for p= 2, in trivial order for single element
  BOOST_CHECK_EQUAL(edgeMap[0], 0);
  BOOST_CHECK_EQUAL(edgeMap[1], 1);
  BOOST_CHECK_EQUAL(edgeMap[2], 2);
  BOOST_CHECK_EQUAL(edgeMap[3], 3);
  BOOST_CHECK_EQUAL(edgeMap[4], 4);
  BOOST_CHECK_EQUAL(edgeMap[5], 5);
  BOOST_CHECK_EQUAL(edgeMap[6], 6);
  BOOST_CHECK_EQUAL(edgeMap[7], 7);
  BOOST_CHECK_EQUAL(edgeMap[8], 8);
  BOOST_CHECK_EQUAL(edgeMap[9], 9);

  Int5 xfaceSign;
  Int5 qfaceSign;

  xfaceSign= xfldGroup0.associativity(0).faceSign();
  qfaceSign= qfldGroup0.associativity(0).faceSign();
  BOOST_CHECK_EQUAL(xfaceSign[0], qfaceSign[0]);
  BOOST_CHECK_EQUAL(xfaceSign[1], qfaceSign[1]);
  BOOST_CHECK_EQUAL(xfaceSign[2], qfaceSign[2]);
  BOOST_CHECK_EQUAL(xfaceSign[3], qfaceSign[3]);
  BOOST_CHECK_EQUAL(xfaceSign[4], qfaceSign[4]);

  XField4D_2Ptope_X1_1Group xfld1;

//  const XFieldSpacetimeClass &xfldGroup1= xfld1.getCellGroup<Pentatope>(0);

  QField4D_CG_Spacetime qfld1(xfld1, order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL(qfld1.nDOF(), 2*nDOFelem - nDOFtrace);
  BOOST_CHECK_EQUAL(qfld1.nElem(), 2);
  BOOST_CHECK_EQUAL(qfld1.nInteriorTraceGroups(), 0);
  BOOST_CHECK_EQUAL(qfld1.nBoundaryTraceGroups(), 2*dim);
  BOOST_CHECK_EQUAL(qfld1.nCellGroups(), 1);
  BOOST_CHECK_EQUAL(&qfld1.getXField(), &xfld1);

  const QFieldSpacetimeClass &qfldGroup1= qfld1.getCellGroup<Pentatope>(0);

  BOOST_CHECK_EQUAL(2, qfldGroup1.nElem());

  BOOST_CHECK_EQUAL(qfldGroup1.associativity(0).nNode(), dim + 1);  // order 2 has DOFs on nodes
  BOOST_CHECK_EQUAL(qfldGroup1.associativity(0).nEdge(), 10);       // and on edges. thats it
  BOOST_CHECK_EQUAL(qfldGroup1.associativity(0).nArea(), 0);
  BOOST_CHECK_EQUAL(qfldGroup1.associativity(0).nFace(), 0);
  BOOST_CHECK_EQUAL(qfldGroup1.associativity(0).nCell(), 0);

  BOOST_CHECK_EQUAL(qfldGroup1.associativity(1).nNode(), dim + 1);  // order 2 has DOFs on nodes
  BOOST_CHECK_EQUAL(qfldGroup1.associativity(1).nEdge(), 10);       // and on edges. thats it
  BOOST_CHECK_EQUAL(qfldGroup1.associativity(1).nFace(), 0);
  BOOST_CHECK_EQUAL(qfldGroup1.associativity(1).nArea(), 0);
  BOOST_CHECK_EQUAL(qfldGroup1.associativity(1).nCell(), 0);

//  // edge DOFs
//  qfldGroup1.associativity(0).getEdgeGlobalMapping(edgeMap, 10);
//  for (int i= 0; i < 10; i++)
//  {
//    printf("edgemap[%d]: %d\n", i, edgeMap[i]);
//  }

}

#ifdef SANS_AVRO

BOOST_AUTO_TEST_CASE(CG_Spacetime_CheckTrace_BoxGrid)
{
  // same type as VectorX because we copy xfld DOFs to the qfld ;)
  typedef DLA::VectorS<4, Real> ArrayQ;

  typedef XField4D_2Ptope_X1_1Group::FieldCellGroupType<Pentatope> XFieldSpacetimeClass;
  typedef XField4D_2Ptope_X1_1Group::FieldTraceGroupType<Tet> XFieldVolumeClass;

  typedef XFieldSpacetimeClass::ElementType<> ElementXFieldSpacetimeClass;
  typedef XFieldVolumeClass::ElementType<> ElementXFieldVolumeClass;
  typedef ElementXFieldSpacetimeClass::VectorX VectorX;

  typedef Field_CG_Cell<PhysD4, TopoD4, ArrayQ> QField4D_CG_Spacetime;
  typedef QField4D_CG_Spacetime::FieldCellGroupType<Pentatope> QFieldSpacetimeClass;

  typedef QFieldSpacetimeClass::ElementType<> ElementQFieldSpacetimeClass;

  // global communicator
  mpi::communicator world;

  // split the communicator across all processors
  mpi::communicator comm= world.split(world.rank());

  const Real small_tol= 1e-12;
  const Real close_tol= 1e-10;

  // XField4D_2Ptope_X1_1Group xfld_X1;

  int Nx= 2; // number of elements rows not number of nodes
  int Ny= 2;
  int Nz= 2;
  int Nt= 2;

  XField_KuhnFreudenthal<PhysD4, TopoD4> xfld_X1(comm, {Nx + 1, Ny + 1, Nz + 1, Nt + 1});
  // printf("%d\t%d\n", xfld_X1.nElem(), xfld_X1.nDOF());

#if LIMITP
  int pLimit= 3;  // DEBUG!!!!!
#else
  int pLimit= BasisFunctionSpacetime_Pentatope_LagrangePMax;
#endif

  for (int order= 1; order <= pLimit; order++)
  {
    XField<PhysD4, TopoD4> xfld(xfld_X1, order, BasisFunctionCategory_Lagrange);

    const XFieldSpacetimeClass &xfldSpacetime= xfld.getCellGroup<Pentatope>(0);
    const XFieldVolumeClass &xfldFace= xfld.getInteriorTraceGroup<Tet>(0);

    QField4D_CG_Spacetime qfld(xfld, order, BasisFunctionCategory_Lagrange);

    BOOST_REQUIRE(qfld.nDOF() == xfld.nDOF());
    for (int i= 0; i < qfld.nDOF(); i++)
      qfld.DOF(i)= xfld.DOF(i);

    const QFieldSpacetimeClass &qfldSpacetime= qfld.getCellGroup<Pentatope>(0);

    ElementXFieldVolumeClass xfldElemFace(xfldFace.basis());
    ElementXFieldSpacetimeClass xfldElemL(xfldSpacetime.basis());
    ElementXFieldSpacetimeClass xfldElemR(xfldSpacetime.basis());

    ElementQFieldSpacetimeClass qfldElemL(qfldSpacetime.basis());
    ElementQFieldSpacetimeClass qfldElemR(qfldSpacetime.basis());

    const int nface= xfldFace.nElem();
    BOOST_CHECK_GT(nface, 1);

    for (int face= 0; face < nface; face++)
    {
      const int elemL= xfldFace.getElementLeft(face);
      const int elemR= xfldFace.getElementRight(face);

      const CanonicalTraceToCell &canonicalFaceL= xfldFace.getCanonicalTraceLeft(face);
      const CanonicalTraceToCell &canonicalFaceR= xfldFace.getCanonicalTraceRight(face);

      xfldFace.getElement(xfldElemFace, face);
      xfldSpacetime.getElement(xfldElemL, elemL);
      xfldSpacetime.getElement(xfldElemR, elemR);

      qfldSpacetime.getElement(qfldElemL, elemL);
      qfldSpacetime.getElement(qfldElemR, elemR);

      Real sRefL;
      Real sRefR;
      Real tRefL;
      Real tRefR;
      Real uRefL;
      Real uRefR;
      Real vRefL;
      Real vRefR;

      Real sRef= 0;
      Real tRef= 0;
      Real uRef= 0;

      ArrayQ qL;
      ArrayQ qR;
      VectorX xL;
      VectorX xR;
      VectorX xF;

      int kmax= 5;
      Real dRef= 1./static_cast<Real>(kmax);
      for (int ki= 0; ki < kmax; ki++)
      {
        sRef += dRef;
        tRef= 0;
        for (int kj= ki; kj < kmax; kj++)
        {
          tRef += dRef;
          uRef= 0;

          for (int kk= ki + kj; kk < kmax; kk++)
          {
            uRef += dRef;

            TraceToCellRefCoord<Tet, TopoD4, Pentatope>::eval(canonicalFaceL, sRef, tRef, uRef, sRefL, tRefL, uRefL, vRefL);
            TraceToCellRefCoord<Tet, TopoD4, Pentatope>::eval(canonicalFaceR, sRef, tRef, uRef, sRefR, tRefR, uRefR, vRefR);

            // solution trace from L/R elements
            qfldElemL.eval(sRefL, tRefL, uRefL, vRefL, qL);
            qfldElemR.eval(sRefR, tRefR, uRefR, vRefR, qR);

            // check if the left and right solutions match
            SANS_CHECK_CLOSE(qL(0), qR(0), small_tol, close_tol);
            SANS_CHECK_CLOSE(qL(1), qR(1), small_tol, close_tol);
            SANS_CHECK_CLOSE(qL(2), qR(2), small_tol, close_tol);
            SANS_CHECK_CLOSE(qL(3), qR(3), small_tol, close_tol);

            // element coordinates from L/R elements
            xfldElemFace.eval(sRef, tRef, uRef, xF);
            xfldElemL.eval(sRefL, tRefL, uRefL, vRefL, xL);
            xfldElemR.eval(sRefR, tRefR, uRefR, vRefR, xR);

            // check if the face coordinates match the cell coordinates
            SANS_CHECK_CLOSE(xF[0], xL[0], small_tol, close_tol);
            SANS_CHECK_CLOSE(xF[1], xL[1], small_tol, close_tol);
            SANS_CHECK_CLOSE(xF[2], xL[2], small_tol, close_tol);
            SANS_CHECK_CLOSE(xF[3], xL[3], small_tol, close_tol);

            // check if the left and right coordinates match
            SANS_CHECK_CLOSE(xL[0], xR[0], small_tol, close_tol);
            SANS_CHECK_CLOSE(xL[1], xR[1], small_tol, close_tol);
            SANS_CHECK_CLOSE(xL[2], xR[2], small_tol, close_tol);
            SANS_CHECK_CLOSE(xL[3], xR[3], small_tol, close_tol);

            // check if the qfld and xfld solutions match (both should have the same data)
            SANS_CHECK_CLOSE(qL[0], xL[0], small_tol, close_tol);
            SANS_CHECK_CLOSE(qL[1], xL[1], small_tol, close_tol);
            SANS_CHECK_CLOSE(qL[2], xL[2], small_tol, close_tol);
            SANS_CHECK_CLOSE(qL[3], xL[3], small_tol, close_tol);

          }
        }
      }

    }

  }

}

BOOST_AUTO_TEST_CASE(CG_ST_ProjectPtoPp1_Lagrange_1Elem)
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell<PhysD4, TopoD4, ArrayQ> QField4D_CG_Spacetime;
  typedef QField4D_CG_Spacetime::FieldCellGroupType<Pentatope> QFieldSpacetimeClass;
  typedef QFieldSpacetimeClass::ElementType<> ElementQFieldClass;

  // solution arrays
  ArrayQ q0;
  ArrayQ q1;

  // do the two-element thing
  XField4D_1Ptope_X1_1Group xfld1;

  // limit of order that we want to do
#if LIMITP
  int pLimit= 3;
#else
  int pLimit= BasisFunctionSpacetime_Pentatope_LagrangePMax;
#endif
  // loop over orders up to the limit
  for (int order= 1; order < pLimit; order++)
  {
    // check projections onto higher orders
    for (int orderinc= 1; orderinc <= pLimit - order; orderinc++)
    {
      // solution fields
      QField4D_CG_Spacetime qfldP(xfld1, order, BasisFunctionCategory_Lagrange);
      QField4D_CG_Spacetime qfldPp1(xfld1, order + orderinc, BasisFunctionCategory_Lagrange);

      // solution cellgroups
      QFieldSpacetimeClass &qfldSpacetimeP= qfldP.getCellGroup<Pentatope>(0);
      QFieldSpacetimeClass &qfldSpacetimePp1= qfldPp1.getCellGroup<Pentatope>(0);

      // solution elements
      ElementQFieldClass qfldElemP(qfldSpacetimeP.basis());
      ElementQFieldClass qfldElemPp1(qfldSpacetimePp1.basis());

      // give some non-zero initial condition
      for (int n= 0; n < qfldP.nDOF(); n++)
        qfldP.DOF(n)= 0.1*n + 2;

      // zero lower order thing
      for (int n= 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n)= 0;

      // use spacetime function projectTo to project from highest order to lower order
      qfldSpacetimeP.projectTo(qfldSpacetimePp1);

      // on each element order p+ solution should match order p solution
      for (int elem= 0; elem < qfldSpacetimeP.nElem(); elem++)
      {
        qfldSpacetimeP.getElement(qfldElemP, elem);
        qfldSpacetimePp1.getElement(qfldElemPp1, elem);

        const int kmax= 5;
        const Real dRef= 1./static_cast<Real>(kmax);
        Real sRef= 0;
        Real tRef= 0;
        Real uRef= 0;
        Real vRef= 0;

        for (int i= 0; i <= kmax; i++)
        {
          tRef= 0;
          for (int j= 0; j + i <= kmax; j++)
          {
            uRef= 0;
            for (int k= 0; i + j + k <= kmax; k++)
            {
              vRef= 0;
              for (int m= 0; i + j + k + m <= kmax; m++)
              {
                qfldElemP.eval(sRef, tRef, uRef, vRef, q0);
                qfldElemPp1.eval(sRef, tRef, uRef, vRef, q1);
                BOOST_CHECK_GT(q0[0], 1);
                BOOST_CHECK_GT(q1[0], 1);
                BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
                BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);

                vRef += dRef;
              }
              uRef += dRef;
            }
            tRef += dRef;
          }
          sRef += dRef;
        }

      }

//      for (int face= 0; face < qfldVolumeIP.nElem(); face++)
//      {
//
//      }

    }
  }
}

BOOST_AUTO_TEST_CASE(CG_ST_ProjectPtoPp1_Lagrange)
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell<PhysD4, TopoD4, ArrayQ> QField4D_CG_Spacetime;
  typedef QField4D_CG_Spacetime::FieldCellGroupType<Pentatope> QFieldSpacetimeClass;
  typedef QFieldSpacetimeClass::ElementType<> ElementQFieldClass;

  // solution arrays
  ArrayQ q0;
  ArrayQ q1;

  // do the two-element thing
  XField4D_2Ptope_X1_1Group xfld1;

  // limit of order that we want to do
#if LIMITP
  int pLimit= 3;
#else
  int pLimit= BasisFunctionSpacetime_Pentatope_LagrangePMax;
#endif

  // loop over orders up to the limit
  for (int order= 1; order < pLimit; order++)
  {
    // check projections onto higher orders
    for (int orderinc= 1; orderinc <= pLimit - order; orderinc++)
    {
      // solution fields
      QField4D_CG_Spacetime qfldP(xfld1, order, BasisFunctionCategory_Lagrange);
      QField4D_CG_Spacetime qfldPp1(xfld1, order + orderinc, BasisFunctionCategory_Lagrange);

      // solution cellgroups
      QFieldSpacetimeClass &qfldSpacetimeP= qfldP.getCellGroup<Pentatope>(0);
      QFieldSpacetimeClass &qfldSpacetimePp1= qfldPp1.getCellGroup<Pentatope>(0);

      // solution elements
      ElementQFieldClass qfldElemP(qfldSpacetimeP.basis());
      ElementQFieldClass qfldElemPp1(qfldSpacetimePp1.basis());

      // give some non-zero initial condition
      for (int n= 0; n < qfldP.nDOF(); n++)
        qfldP.DOF(n)= 0.1*n + 2;

      // zero highter order thing
      for (int n= 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n)= 0;

      // use spacetime function projectTo to project from highest order to lower order
      qfldSpacetimeP.projectTo(qfldSpacetimePp1);

      // on each element order p+ solution should match order p solution
      for (int elem= 0; elem < qfldSpacetimeP.nElem(); elem++)
      {
        qfldSpacetimeP.getElement(qfldElemP, elem);
        qfldSpacetimePp1.getElement(qfldElemPp1, elem);

        const int kmax= 5;
        const Real dRef= 1./static_cast<Real>(kmax);
        Real sRef= 0;
        Real tRef= 0;
        Real uRef= 0;
        Real vRef= 0;

        for (int i= 0; i <= kmax; i++)
        {
          tRef= 0;
          for (int j= 0; j + i <= kmax; j++)
          {
            uRef= 0;
            for (int k= 0; i + j + k <= kmax; k++)
            {
              vRef= 0;
              for (int m= 0; i + j + k + m <= kmax; m++)
              {
                qfldElemP.eval(sRef, tRef, uRef, vRef, q0);
                qfldElemPp1.eval(sRef, tRef, uRef, vRef, q1);

                for (int i= 0; i < ArrayQ::M; i++)
                {
                  BOOST_CHECK_GT(q0[i], 1);
                  BOOST_CHECK_GT(q1[i], 1);
                  BOOST_CHECK_CLOSE(q0[i], q1[i], 1e-12);
                  BOOST_CHECK_CLOSE(q0[i], q1[i], 1e-12);
                }

                vRef += dRef;
              }
              uRef += dRef;
            }
            tRef += dRef;
          }
          sRef += dRef;
        }

      }

    }
  }
}

#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
