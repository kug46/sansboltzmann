// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField1D_Curved_btest
// testing of higher order XField1D grids
//
// Note: unit grids tested in "unit/UnitGrids/"

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Field/XFieldLine.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( XField1D_Curved_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK( (XField<PhysD1,TopoD1>::D == 1) );
}

//--------------------CHECKING HIGHER ORDER GRIDS-----------------------------//

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField1D_1Line_X2_1Group_test )
{
  XField1D_1Line_X1_1Group xfld_X1; //Linear mesh

  XField<PhysD1,TopoD1> xfld(xfld_X1,2); //Construct Q2 mesh from linear mesh

  BOOST_CHECK_EQUAL( xfld.nDOF(), 3 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 1 );

  // line field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XField<PhysD1,TopoD1>::FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);

  int nodeMap[2];

  //Node DOFs
  xfldCell.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  //Edge DOFs
  xfldCell.associativity(0).getEdgeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 0 );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 2 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Node) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(1).topoTypeID() == typeid(Node) );

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldBnode0 = xfld.getBoundaryTraceGroup<Node>(0);
  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldBnode1 = xfld.getBoundaryTraceGroup<Node>(1);

  xfldBnode0.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );

  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignL(), -1 );
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignR(),  0 );

  xfldBnode1.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );

  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignL(),  1 );
  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignR(),  0 );

  // boundary edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getCanonicalTraceLeft(0).trace, 1 );

  BOOST_CHECK_EQUAL( xfldBnode1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode1.getCanonicalTraceLeft(0).trace, 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField1D_1Line_X3_1Group_test )
{
  XField1D_1Line_X1_1Group xfld_X1; //Linear mesh

  XField<PhysD1,TopoD1> xfld(xfld_X1,3); //Construct Q2 mesh from linear mesh

  BOOST_CHECK_EQUAL( xfld.nDOF(), 4 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0], 1 );

  // line field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XField<PhysD1,TopoD1>::FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);

  int nodeMap[2];

  //Node DOFs
  xfldCell.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  //Edge DOFs
  xfldCell.associativity(0).getEdgeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 0 );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 2 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Node) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(1).topoTypeID() == typeid(Node) );

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldBnode0 = xfld.getBoundaryTraceGroup<Node>(0);
  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldBnode1 = xfld.getBoundaryTraceGroup<Node>(1);

  xfldBnode0.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );

  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignL(), -1 );
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignR(),  0 );

  xfldBnode1.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );

  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignL(),  1 );
  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignR(),  0 );

  // boundary edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getCanonicalTraceLeft(0).trace, 1 );

  BOOST_CHECK_EQUAL( xfldBnode1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode1.getCanonicalTraceLeft(0).trace, 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField1D_X2_test )
{
  XField1D xfld_X1(2); //Linear mesh with 2 lines

  XField<PhysD1,TopoD1> xfld(xfld_X1,2); //Construct Q2 mesh from linear mesh

  BOOST_CHECK_EQUAL( xfld.nDOF(), 5 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0], 0.5 );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[0], 1.0 );

  // line field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XField<PhysD1,TopoD1>::FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);

  int nodeMap[2];

  //Node DOFs
  xfldCell.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  xfldCell.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );

  //Edge DOFs
  xfldCell.associativity(0).getEdgeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );

  xfldCell.associativity(1).getEdgeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );

  BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Node) );

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldInode = xfld.getInteriorTraceGroup<Node>(0);

  xfldInode.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );

  BOOST_CHECK_EQUAL( xfldInode.associativity(0).normalSignL(),  1 );
  BOOST_CHECK_EQUAL( xfldInode.associativity(0).normalSignR(), -1 );

  // interior edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldInode.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldInode.getGroupRight(), 0 );
  BOOST_CHECK_EQUAL( xfldInode.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldInode.getElementRight(0), 1 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceRight(0).trace, 1 );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 2 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Node) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(1).topoTypeID() == typeid(Node) );

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldBnode0 = xfld.getBoundaryTraceGroup<Node>(0);
  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldBnode1 = xfld.getBoundaryTraceGroup<Node>(1);

  xfldBnode0.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );

  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignL(), -1 );
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignR(),  0 );

  xfldBnode1.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );

  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignL(),  1 );
  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignR(),  0 );

  // boundary edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getCanonicalTraceLeft(0).trace, 1 );

  BOOST_CHECK_EQUAL( xfldBnode1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode1.getElementLeft(0), 1 );
  BOOST_CHECK_EQUAL( xfldBnode1.getCanonicalTraceLeft(0).trace, 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField1D_X3_test )
{
  XField1D xfld_X1(3); //Linear mesh with 3 lines

  XField<PhysD1,TopoD1> xfld(xfld_X1,3); //Construct Q3 mesh from linear mesh

  BOOST_CHECK_EQUAL( xfld.nDOF(), 10 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(5)[0], 0 );

  BOOST_CHECK_EQUAL( xfld.DOF(6)[0], 0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(7)[0], 1.0/3.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(8)[0], 2.0/3.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(9)[0], 1.0 );

  // line field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XField<PhysD1,TopoD1>::FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);

  int nodeMap[2];

  //Node DOFs
  xfldCell.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 6 );
  BOOST_CHECK_EQUAL( nodeMap[1], 7 );

  xfldCell.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );
  BOOST_CHECK_EQUAL( nodeMap[1], 8 );

  xfldCell.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 8 );
  BOOST_CHECK_EQUAL( nodeMap[1], 9 );

  //Edge DOFs
  xfldCell.associativity(0).getEdgeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  xfldCell.associativity(1).getEdgeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  xfldCell.associativity(2).getEdgeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );

  BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Node) );

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldInode = xfld.getInteriorTraceGroup<Node>(0);

  xfldInode.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );

  BOOST_CHECK_EQUAL( xfldInode.associativity(0).normalSignL(),  1 );
  BOOST_CHECK_EQUAL( xfldInode.associativity(0).normalSignR(), -1 );

  xfldInode.associativity(1).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 8 );

  BOOST_CHECK_EQUAL( xfldInode.associativity(1).normalSignL(),  1 );
  BOOST_CHECK_EQUAL( xfldInode.associativity(1).normalSignR(), -1 );

  // interior edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldInode.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldInode.getGroupRight(), 0 );

  BOOST_CHECK_EQUAL( xfldInode.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldInode.getElementRight(0), 1 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceRight(0).trace, 1 );

  BOOST_CHECK_EQUAL( xfldInode.getElementLeft(1), 1 );
  BOOST_CHECK_EQUAL( xfldInode.getElementRight(1), 2 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceLeft(1).trace, 0 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceRight(1).trace, 1 );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 2 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Node) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(1).topoTypeID() == typeid(Node) );

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldBnode0 = xfld.getBoundaryTraceGroup<Node>(0);
  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldBnode1 = xfld.getBoundaryTraceGroup<Node>(1);

  xfldBnode0.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 6 );

  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignL(), -1 );
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignR(),  0 );

  xfldBnode1.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );

  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignL(),  1 );
  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignR(),  0 );

  // boundary edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getCanonicalTraceLeft(0).trace, 1 );

  BOOST_CHECK_EQUAL( xfldBnode1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode1.getElementLeft(0), 2 );
  BOOST_CHECK_EQUAL( xfldBnode1.getCanonicalTraceLeft(0).trace, 0 );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
