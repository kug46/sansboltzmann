// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// NewtonSolver_Parameters_btest
// testing of NewtonSolver_Parameters_btest classes

#include "Python/PyDict.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "NonLinearSolver/NewtonSolver.h"
#include "LinearAlgebra/SparseLinAlg/tools/norm.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "Surreal/SurrealS.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( NewtonSolver_Parameters_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( UMFPACK_Dict )
{
  PyDict NewtonSolverDict, LinearSolverDict, NewtonLineUpdateDict;

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinearSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FGMRES_Dict )
{
  PyDict NewtonSolverDict, LinearSolverDict, PreconditionerDict, NewtonLineUpdateDict;

  PreconditionerDict[SLA::FGMRESParam::params.Preconditioner.Name] = SLA::FGMRESParam::params.Preconditioner.Identity;
  LinearSolverDict[SLA::FGMRESParam::params.Preconditioner] = PreconditionerDict;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.FGMRES;

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinearSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

}

#ifdef SANS_PETSC
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PETSc_Dict )
{
  PyDict NewtonSolverDict, LinearSolverDict, NewtonLineUpdateDict;

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinearSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

}
#endif


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
