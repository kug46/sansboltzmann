// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// NewtonSolver_btest
// testing of NewtonSolver_btest classes

#include "Python/PyDict.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "NonLinearSolver/NewtonSolver.h"
#include "LinearAlgebra/SparseLinAlg/tools/norm.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#ifdef SANS_PETSC
#include "LinearAlgebra/SparseLinAlg/PETSc/PETScSolver.h"
#endif

//
#include <boost/iostreams/device/mapped_file.hpp>
using boost::iostreams::mapped_file_source;

#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
//

#include "Surreal/SurrealS.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace SANS;

namespace
{

enum EquationSet
{
  SetEq1,
  SetEq2,
};

template <class SystemMatrix>
class DummyNonLinearEquation : public AlgebraicEquationSetBase<SystemMatrix>
{
public:
  typedef AlgebraicEquationSetBase<SystemMatrix> BaseType;

  typedef typename BaseType::SystemVector SystemVector;
  typedef typename BaseType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename BaseType::LinesearchData LinesearchData;

  typedef typename MatrixSizeType<SystemMatrix>::type MatrixSizeClass;
  typedef typename VectorSizeType<SystemVector>::type VectorSizeClass;

  explicit DummyNonLinearEquation(const EquationSet& Eq) : Eq_(Eq) {}
  virtual ~DummyNonLinearEquation() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  virtual void residual(SystemVector& rsd) override
  {
    Real x = qfld_[0];
    Real y = qfld_[1];

    residual(x,y,rsd);
  }

  virtual void jacobian(SystemMatrix& mtx       ) override { this->template jacobian<SystemMatrix>(mtx);        }
  virtual void jacobian(SystemNonZeroPattern& nz) override { this->template jacobian<SystemNonZeroPattern>(nz); }

  template<class SystemMatrix_type>
  void jacobian(SystemMatrix_type& mtx)
  {
    SurrealS<2> x = qfld_[0];
    SurrealS<2> y = qfld_[1];

    // Set derivatives
    x.deriv(0) = 1;
    y.deriv(1) = 1;

    DLA::VectorS<2, SurrealS<2> > rsd;

    residual(x,y,rsd);

    DLA::MatrixD<Real> M(2,2);

    for (int i = 0; i < 2; i++)
      for (int j = 0; j < 2; j++)
        M(i,j) = rsd[i].deriv(j);

    int Map[] = {0,1};

    mtx.scatterAdd(M,Map,2);
  }
  virtual void jacobianTranspose(SystemMatrix& mtx) override {}
  virtual void jacobianTranspose(SystemNonZeroPattern& nz) override {}

  virtual std::vector<std::vector<Real>> residualNorm(const SystemVector& rsd) const override
  {
    std::vector<std::vector<Real>> rsdNorm(1, std::vector<Real>(2, 0));

    rsdNorm[0][0] = fabs(rsd[0]);
    rsdNorm[0][1] = fabs(rsd[1]);

    return rsdNorm;

  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm) const override
  {

    //converged?
    return rsdNorm[0][0] < 1e-12 && rsdNorm[0][1] < 1e-12;
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm, int iEq, int iMon) const override
  {
    BOOST_REQUIRE( iEq == 0 );
    BOOST_REQUIRE( iMon < 2 );

    return rsdNorm[iEq][iMon] < 1e-10;
  }

  // TODO:implement!
  virtual bool decreasedResidual(const std::vector<std::vector<Real>>& rsdNormOld,
                                 const std::vector<std::vector<Real>>& rsdNormNew) const override
  {
    //is the residual actually decreased or converged?
    int t = 0;

    if (rsdNormOld[0][0] < rsdNormNew[0][0] && rsdNormOld[0][0] > 1e-12)
      t++;
    if (rsdNormOld[0][1] < rsdNormNew[0][1] && rsdNormOld[0][1] > 1e-12)
      t++;

    return (t == 0);
  }

  //prints out a residual that could not be decreased and the convergence tolerances
  virtual void printDecreaseResidualFailure(const std::vector<std::vector<Real>>& rsdNorm, std::ostream& os = std::cout) const override {}

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVector& q) override
  {
    qfld_[0] = q[0];
    qfld_[1] = q[1];
  }

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVector& q) const override
  {
    q[0] = qfld_[0];
    q[1] = qfld_[1];
  }

  virtual VectorSizeClass vectorEqSize() const override    // vector for equations (rows in matrixSize)
  {
    return VectorSizeClass(2);
  }

  virtual VectorSizeClass vectorStateSize() const override // vector for state DOFs (columns in matrixSize)
  {
    return VectorSizeClass(2);
  }

  virtual MatrixSizeClass matrixSize() const override
  {
    // Create the size that represents the size of a sparse linear algebra matrix
    return { 2, 2 };
  }

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { return 0; }
  virtual int indexQ() const override { return 0; }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVector& q) override
  {
    return true;
  }

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override
  {
    return 2;
  }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override
  {
    mpi::communicator world;
    return std::make_shared<mpi::communicator>(world.split(world.rank()));
  }
  virtual void syncDOFs_MPI() override {}

  virtual bool updateLinesearchDebugInfo(const Real& s, const SystemVector& rsd,
                                         LinesearchData& pResData,
                                         LinesearchData& pStepData) const override { return true; };

  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override {};


  // Are we having machine precision issues - assume no issues
  virtual bool atMachinePrecision(const SystemVector& q, const std::vector<std::vector<Real>>& R0norm) override
  {
    return false;
  }

protected:
  const EquationSet Eq_;
  DLA::VectorS<2,Real> qfld_;

  template<class T, class Vector>
  void residual(const T& x, const T& y, Vector& rsd) const
  {
    switch (Eq_)
    {
    case SetEq1:
      //rsd[0] = x*x +   y - 5;
      //rsd[1] = x*x + y*y - 7;
      rsd[0] = x*y + x - 4*y - 11;
      rsd[1] = x*y - x - 4*y - 4;
      break;
    case SetEq2:
      rsd[0] = x*x - 7 + y;
      rsd[1] = y*(x - sqrt(7));
      break;
    default:
      SANS_DEVELOPER_EXCEPTION("Unknown Equation set");
    }
  }
};

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( NewtonSolver_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DummyNonLinearResidual_Solve_Sparse_Eq1 )
{
  typedef SLA::SparseVector<Real> SystemVector;
  typedef SLA::SparseMatrix_CRS<Real> SparseMatrix;

  typedef DummyNonLinearEquation<SparseMatrix> DummyEquation;

  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  //std::cout << (std::string)boost::python::extract<std::string>(boost::python::str(NewtonSolverDict)) << std::endl;

  DummyEquation Equation(SetEq1);

  NewtonSolver<SparseMatrix> Solver( Equation, NewtonSolverDict );

  SystemVector X(2), Sln(2);

  // This has an initial residual of 0 for the 2nd equation, but the residual grows on the first iteration
  X[0] = 0;
  X[1] = -1;

//  std::cout << "Using full Newton update" << std::endl;
  SolveStatus status = Solver.solve(X,Sln);
  BOOST_CHECK( status.converged );

  SystemVector rsd(2);

  //std::cout << "x = " << Sln[0] << " y = " << Sln[1] << std::endl;

  BOOST_CHECK_CLOSE(  3.5, Sln[0], 1e-12 );
  BOOST_CHECK_CLOSE( -15., Sln[1], 1e-12 );

  Equation.residual(Sln,rsd);

  //std::cout << "rsd[0] = " << rsd[0] << " rsd[1] = " << rsd[1] << std::endl;

  BOOST_CHECK_SMALL( rsd[0], 1e-12 );
  BOOST_CHECK_SMALL( rsd[1], 1e-12 );

}

#if 0 //Commented out until the dense Newton solver is implemented
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DummyNonLinearResidual_Solve_Dense_Eq1 )
{
  typedef DLA::VectorD<Real> SystemVector;
  typedef DLA::MatrixD<Real> SparseMatrix;
  typedef DLA::DenseNonZeroPattern<Real> NonZeroPattern;

  typedef DummyNonLinearEquation<SystemVector, SparseMatrix, NonZeroPattern> DummyEquation;

  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  //std::cout << (std::string)boost::python::extract<std::string>(boost::python::str(NewtonSolverDict)) << std::endl;

  DummyEquation Equation(SetEq1);

  NewtonSolver<SparseMatrix,NonZeroPattern> Solver( Equation, NewtonSolverDict );

  SystemVector X(2), Sln(2);

  // This has an initial residual of 0 for the 2nd equation, but the residual grows on the first iteration
  X[0] = 0;
  X[1] = -1;

//  std::cout << "Using full Newton update" << std::endl;
  BOOST_CHECK( Solver.solve(X,Sln) );

  SystemVector rsd(2);

  //std::cout << "x = " << Sln[0] << " y = " << Sln[1] << std::endl;

  BOOST_CHECK_CLOSE(  3.5, Sln[0], 1e-12 );
  BOOST_CHECK_CLOSE( -15., Sln[1], 1e-12 );

  Equation.residual(Sln,rsd);

  //std::cout << "rsd[0] = " << rsd[0] << " rsd[1] = " << rsd[1] << std::endl;

  BOOST_CHECK_SMALL( rsd[0], 1e-12 );
  BOOST_CHECK_SMALL( rsd[1], 1e-12 );

}
#endif


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DummyNonLinearResidual_Solve_Sparse_ScalarGoldenSearch_Eq1 )
{
  typedef SLA::SparseVector<Real> SystemVector;
  typedef SLA::SparseMatrix_CRS<Real> SparseMatrix;

  typedef DummyNonLinearEquation<SparseMatrix> DummyEquation;

  PyDict NewtonSolverDict, UMFPACKDict, ScalarGoldenSearchLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  ScalarGoldenSearchLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] =
      LineUpdateParam::params.LineUpdate.ScalarGoldenSearch;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = ScalarGoldenSearchLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  //std::cout << (std::string)boost::python::extract<std::string>(boost::python::str(NewtonSolverDict)) << std::endl;

  DummyEquation Equation(SetEq1);

  NewtonSolver<SparseMatrix> Solver( Equation, NewtonSolverDict );

  SystemVector X(2), Sln(2);

  // This has an initial residual of 0 for the 2nd equation, but the residual grows on the first iteration
  X[0] = 0;
  X[1] = -1;

//  std::cout << "Using Golden search update" << std::endl;
  SolveStatus status = Solver.solve(X,Sln);
  BOOST_CHECK( status.converged );

  SystemVector rsd(2);

  //std::cout << "x = " << Sln[0] << " y = " << Sln[1] << std::endl;

  BOOST_CHECK_CLOSE(  3.5, Sln[0], 5e-12 );
  BOOST_CHECK_CLOSE( -15., Sln[1], 5e-11 );

  Equation.residual(Sln,rsd);

  //std::cout << "rsd[0] = " << rsd[0] << " rsd[1] = " << rsd[1] << std::endl;

  BOOST_CHECK_SMALL( rsd[0], 1e-12 );
  BOOST_CHECK_SMALL( rsd[1], 1e-12 );

}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DummyNonLinearResidual_Solve_Sparse_HalvingSearch_Eq1 )
{
  typedef SLA::SparseVector<Real> SystemVector;
  typedef SLA::SparseMatrix_CRS<Real> SparseMatrix;

  typedef DummyNonLinearEquation<SparseMatrix> DummyEquation;

  PyDict NewtonSolverDict, UMFPACKDict, HalvingSearchLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  HalvingSearchLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] =
      LineUpdateParam::params.LineUpdate.HalvingSearch;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = HalvingSearchLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  //std::cout << (std::string)boost::python::extract<std::string>(boost::python::str(NewtonSolverDict)) << std::endl;

  DummyEquation Equation(SetEq1);

  NewtonSolver<SparseMatrix> Solver( Equation, NewtonSolverDict );

  SystemVector X(2), Sln(2);

  // This has an initial residual of 0 for the 2nd equation, but the residual grows on the first iteration
  X[0] = 0;
  X[1] = -1;

//  std::cout << "Using halving search update" << std::endl;
  SolveStatus status = Solver.solve(X,Sln);
  BOOST_CHECK( status.converged );

  SystemVector rsd(2);

  //std::cout << "x = " << Sln[0] << " y = " << Sln[1] << std::endl;

  BOOST_CHECK_CLOSE(  3.5, Sln[0], 5e-12 );
  BOOST_CHECK_CLOSE( -15., Sln[1], 5e-12 );

  Equation.residual(Sln,rsd);

  //std::cout << "rsd[0] = " << rsd[0] << " rsd[1] = " << rsd[1] << std::endl;

  BOOST_CHECK_SMALL( rsd[0], 1e-12 );
  BOOST_CHECK_SMALL( rsd[1], 1e-12 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DummyNonLinearResidual_Solve_Sparse_Eq2 )
{
  typedef SLA::SparseVector<Real> SystemVector;
  typedef SLA::SparseMatrix_CRS<Real> SparseMatrix;

  typedef DummyNonLinearEquation<SparseMatrix> DummyEquation;

  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  //std::cout << (std::string)boost::python::extract<std::string>(boost::python::str(NewtonSolverDict)) << std::endl;

  DummyEquation Equation(SetEq2);

  NewtonSolver<SparseMatrix> Solver( Equation, NewtonSolverDict );

  SystemVector X(2), Sln(2);

  X[0] = 1;
  X[1] = 0;

//  std::cout << "Using full Newton update" << std::endl;
  SolveStatus status = Solver.solve(X,Sln);
  BOOST_CHECK( status.converged );

  SystemVector rsd(2);

  //std::cout << "x = " << Sln[0] << " y = " << Sln[1] << std::endl;

  BOOST_CHECK_CLOSE(  sqrt(7.), Sln[0], 1e-12 );
  BOOST_CHECK_SMALL(            Sln[1], 1e-12 );

  Equation.residual(Sln,rsd);

  //std::cout << "rsd[0] = " << rsd[0] << " rsd[1] = " << rsd[1] << std::endl;

  BOOST_CHECK_SMALL( rsd[0], 1e-12 );
  BOOST_CHECK_SMALL( rsd[1], 1e-12 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DummyNonLinearResidual_Solve_Sparse_ScalarGoldenSearch_Eq2 )
{
  typedef SLA::SparseVector<Real> SystemVector;
  typedef SLA::SparseMatrix_CRS<Real> SparseMatrix;

  typedef DummyNonLinearEquation<SparseMatrix> DummyEquation;

  PyDict NewtonSolverDict, UMFPACKDict, ScalarGoldenSearchLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  ScalarGoldenSearchLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] =
      LineUpdateParam::params.LineUpdate.ScalarGoldenSearch;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = ScalarGoldenSearchLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  //std::cout << (std::string)boost::python::extract<std::string>(boost::python::str(NewtonSolverDict)) << std::endl;

  DummyEquation Equation(SetEq2);

  NewtonSolver<SparseMatrix> Solver( Equation, NewtonSolverDict );

  SystemVector X(2), Sln(2);

  X[0] = 1;
  X[1] = 0;

//  std::cout << "Using Golden search update" << std::endl;
  SolveStatus status = Solver.solve(X,Sln);
  BOOST_CHECK( status.converged );

  SystemVector rsd(2);

  //std::cout << "x = " << Sln[0] << " y = " << Sln[1] << std::endl;

  BOOST_CHECK_CLOSE(  sqrt(7.), Sln[0], 1e-12 );
  BOOST_CHECK_SMALL(            Sln[1], 1e-12 );

  Equation.residual(Sln,rsd);

  //std::cout << "rsd[0] = " << rsd[0] << " rsd[1] = " << rsd[1] << std::endl;

  BOOST_CHECK_SMALL( rsd[0], 1e-12 );
  BOOST_CHECK_SMALL( rsd[1], 1e-12 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DummyNonLinearResidual_Solve_Sparse_HalvingSearch_Eq2 )
{
  typedef SLA::SparseVector<Real> SystemVector;
  typedef SLA::SparseMatrix_CRS<Real> SparseMatrix;

  typedef DummyNonLinearEquation<SparseMatrix> DummyEquation;

  PyDict NewtonSolverDict, UMFPACKDict, HalvingSearchLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  HalvingSearchLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] =
      LineUpdateParam::params.LineUpdate.HalvingSearch;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = HalvingSearchLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  //std::cout << (std::string)boost::python::extract<std::string>(boost::python::str(NewtonSolverDict)) << std::endl;

  DummyEquation Equation(SetEq2);

  NewtonSolver<SparseMatrix> Solver( Equation, NewtonSolverDict );

  SystemVector X(2), Sln(2);

  X[0] = 1;
  X[1] = 0;

//  std::cout << "Using halving search update" << std::endl;
  SolveStatus status = Solver.solve(X,Sln);
  BOOST_CHECK( status.converged );

  SystemVector rsd(2);

  //std::cout << "x = " << Sln[0] << " y = " << Sln[1] << std::endl;

  BOOST_CHECK_CLOSE(  sqrt(7.), Sln[0], 1e-12 );
  BOOST_CHECK_SMALL(            Sln[1], 1e-12 );

  Equation.residual(Sln,rsd);

  //std::cout << "rsd[0] = " << rsd[0] << " rsd[1] = " << rsd[1] << std::endl;

  BOOST_CHECK_SMALL( rsd[0], 1e-12 );
  BOOST_CHECK_SMALL( rsd[1], 1e-12 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DummyNonLinearResidual_Solve_Sparse_ScalarGoldenSearch_tol )
{
  typedef SLA::SparseVector<Real> SystemVector;
  typedef SLA::SparseMatrix_CRS<Real> SparseMatrix;

  typedef DummyNonLinearEquation<SparseMatrix> DummyEquation;

  PyDict NewtonSolverDict, UMFPACKDict, ScalarGoldenSearchDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  ScalarGoldenSearchDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.ScalarGoldenSearch;
  ScalarGoldenSearchDict[ScalarGoldenSearchLineUpdateParam::params.tol] = 1e-8;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = ScalarGoldenSearchDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  //std::cout << (std::string)boost::python::extract<std::string>(boost::python::str(NewtonSolverDict)) << std::endl;

  DummyEquation Equation(SetEq1);

  NewtonSolver<SparseMatrix> Solver( Equation, NewtonSolverDict );

  SystemVector X(2), Sln(2);

  X = 1;

//  std::cout << "Using Golden search update" << std::endl;
  SolveStatus status = Solver.solve(X,Sln);
  BOOST_CHECK( status.converged );

  SystemVector rsd(2);

  //std::cout << "x = " << Sln[0] << " y = " << Sln[1] << std::endl;

  BOOST_CHECK_CLOSE(  3.5, Sln[0], 5e-12 );
  BOOST_CHECK_CLOSE( -15., Sln[1], 5e-12 );

  Equation.residual(Sln,rsd);

  //std::cout << "rsd[0] = " << rsd[0] << " rsd[1] = " << rsd[1] << std::endl;

  BOOST_CHECK_SMALL( rsd[0], 1e-12 );
  BOOST_CHECK_SMALL( rsd[1], 1e-12 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DummyNonLinearResidual_Solve_Sparse_HalvingSearch_tol )
{
  typedef SLA::SparseVector<Real> SystemVector;
  typedef SLA::SparseMatrix_CRS<Real> SparseMatrix;

  typedef DummyNonLinearEquation<SparseMatrix> DummyEquation;

  PyDict NewtonSolverDict, UMFPACKDict, HalvingLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  HalvingLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  HalvingLineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = HalvingLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  //std::cout << (std::string)boost::python::extract<std::string>(boost::python::str(NewtonSolverDict)) << std::endl;

  DummyEquation Equation(SetEq1);

  NewtonSolver<SparseMatrix> Solver( Equation, NewtonSolverDict );

  SystemVector X(2), Sln(2);

  X = 1;

//  std::cout << "Using halving search update" << std::endl;
  SolveStatus status = Solver.solve(X,Sln);
  BOOST_CHECK( status.converged );

  SystemVector rsd(2);

//  std::cout << "x = " << Sln[0] << " y = " << Sln[1] << std::endl;

  BOOST_CHECK_CLOSE(  3.5, Sln[0], 5e-12 );
  BOOST_CHECK_CLOSE( -15., Sln[1], 5e-12 );

  Equation.residual(Sln,rsd);

  //std::cout << "rsd[0] = " << rsd[0] << " rsd[1] = " << rsd[1] << std::endl;

  BOOST_CHECK_SMALL( rsd[0], 1e-12 );
  BOOST_CHECK_SMALL( rsd[1], 1e-12 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGAdvectiveNonLinearResidual_Solve_ResidualHistoryFile )
// Needs to be run once without file checking and have file moved to "IO/Adaptation/residual.txt" to work
{
  typedef ScalarFunction2D_SineSine SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2, AdvectiveFlux2D_Uniform, ViscousFlux2D_None,Source2D_None >
                                PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;
  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_None> BCVector;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2>>
                                           PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;


  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  ViscousFlux2D_None visc;

  NDSolutionExact solnExact;

  Source2D_None source;

  typedef ForcingFunction2D_MMS<PDEAdvectionDiffusion2D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict SineSine;
  SineSine[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.SineSine;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = SineSine;

  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCNameSolution"] = BCSoln;
  PyBCList["BCNone"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNameSolution"] = {XField2D_Box_Triangle_Lagrange_X1::iBottom,
                                        XField2D_Box_Triangle_Lagrange_X1::iLeft};
  BCBoundaryGroups["BCNone"] = {XField2D_Box_Triangle_Lagrange_X1::iTop,
                                XField2D_Box_Triangle_Lagrange_X1::iRight};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);


  // grid: HierarchicalP1 (aka X1)
  mpi::communicator world;

  const int Nx = 5;
  const int Ny = Nx;

  XField2D_Box_Triangle_Lagrange_X1 xfld( world, Nx, Ny);

  // solution: P1 (aka Q1)
  int order = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0;

  // Lagrange multiplier: Legendre P1
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre,
                                                        BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

  lgfld = 0;

  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {1e-11, 1e-11};

  PrimalEquationSetClass PrimalEqSet(xfld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_Default, tol,
                                     {0}, {0}, PyBCList, BCBoundaryGroups);


  // Get residuals after a few iterations
  PyDict NewtonSolverDict, HalvingLineUpdateDict;

#ifdef SANS_PETSC
  PyDict PETScDict;
  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-16;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-12;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  HalvingLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  HalvingLineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;

  std::string filename_test = "tmp/residual.dat";

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = HalvingLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.ResidualHistoryFile] = filename_test;

  NewtonSolverParam::checkInputs(NewtonSolverDict);


  NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

  SystemVectorClass X(PrimalEqSet.vectorStateSize()), Sln(PrimalEqSet.vectorStateSize());

  X = 1;

  // One iteration does not solve the system, but generates a reliable residual history file
  Solver.solve(X,Sln);

  // only rank 0 does file IO
  if (world.rank() == 0)
  {
    //Check if the file was correctly generated
    std::string filename_true = "IO/NonLinearSolver/residual.dat";

    mapped_file_source f1b(filename_test);
    mapped_file_source f2b(filename_true);

    //Check if the contents of the new script and the example are equal
    BOOST_CHECK( f1b.size() == f2b.size() && std::equal(f1b.data(), f1b.data() + f1b.size(), f2b.data()) );

    //Delete the written files
    std::remove(filename_test.c_str());
  }

}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
