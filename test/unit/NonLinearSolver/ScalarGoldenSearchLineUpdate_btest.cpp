// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ScalarGoldenSearchLineUpdate_btest
// testing of ScalarGoldenSearchLineUpdate class

#include "Python/PyDict.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Surreal/SurrealS.h"

#include "NonLinearSolver/LineUpdate/LineUpdateBase.h"
#include "NonLinearSolver/LineUpdate/ScalarGoldenSearchLineUpdate.h"

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/SparseLinAlg/tools/norm.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ScalarGoldenSearchLineUpdate_test_suite )

typedef SLA::SparseVector<Real> SystemVector;
typedef SLA::SparseMatrix_CRS<Real> SparseMatrix;
typedef typename SparseMatrix::NonZeroPattern NonZeroPattern;
typedef MatrixSizeType<SparseMatrix>::type MatrixSizeClass;
typedef VectorSizeType<SystemVector>::type VectorSizeClass;

class DummyNonLinearEquation : public AlgebraicEquationSetBase<SparseMatrix>
{
public:
  typedef AlgebraicEquationSetBase<SparseMatrix> BaseType;
  typedef typename BaseType::LinesearchData LinesearchData;

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  virtual void residual(SystemVector& rsd) override
  {
    Real x = qfld_[0];
    Real y = qfld_[1];

    residual(x,y,rsd);
  }

  virtual void jacobian(SparseMatrix& mtx) override
  {
    jacobian<SparseMatrix>(mtx);
  }

  virtual void jacobian(NonZeroPattern& nz) override
  {
    jacobian<NonZeroPattern>(nz);
  }

  template<class SparseMatrix_type>
  void jacobian(SparseMatrix_type& mtx)
  {
    SurrealS<2> x = qfld_[0];
    SurrealS<2> y = qfld_[1];

    // Set derivatives
    x.deriv(0) = 1;
    y.deriv(1) = 1;

    DLA::VectorS<2, SurrealS<2> > rsd;

    residual(x,y,rsd);

    DLA::MatrixD<Real> M(2,2);

    for (int i = 0; i < 2; i++)
      for (int j = 0; j < 2; j++)
        M(i,j) = rsd[i].deriv(j);

    int Map[] = {0,1};

    mtx.scatterAdd(M,Map,2);
  }
  virtual void jacobianTranspose(SparseMatrix& mtx) override {}
  virtual void jacobianTranspose(NonZeroPattern& nz) override {}

  virtual std::vector<std::vector<Real>> residualNorm(const SystemVector& rsd) const override
  {
    std::vector<std::vector<Real>> rsdNorm(1, std::vector<Real>(1, 0));

    rsdNorm[0][0] = norm(rsd,2);

    return rsdNorm;

  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm) const override
  {
    return rsdNorm[0][0] < 1e-10;
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm, int iEq, int iMon) const override
  {
    BOOST_REQUIRE( iEq == 0 );
    BOOST_REQUIRE( iMon == 0 );

    return rsdNorm[iEq][iMon] < 1e-10;
  }

  // TODO:implement!
  virtual bool decreasedResidual(const std::vector<std::vector<Real>>& rsdNormOld,
                                 const std::vector<std::vector<Real>>& rsdNormNew) const override
  {
    return (rsdNormNew[0][0] < rsdNormOld[0][0] || rsdNormNew[0][0] < 1e-10);
  }

  //prints out a residual that could not be decreased and the convergence tolerances
  virtual void printDecreaseResidualFailure(const std::vector<std::vector<Real>>& rsdNorm, std::ostream& os = std::cout) const override {}

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVector& q) override
  {
    qfld_[0] = q[0];
    qfld_[1] = q[1];
  }

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVector& q) const override
  {
    q[0] = qfld_[0];
    q[1] = qfld_[1];
  }

  virtual VectorSizeClass vectorEqSize() const override    // vector for equations (rows in matrixSize)
  {
    return VectorSizeClass(2);
  }

  virtual VectorSizeClass vectorStateSize() const override // vector for state DOFs (columns in matrixSize)
  {
    return VectorSizeClass(2);
  }

  virtual MatrixSizeClass matrixSize() const override
  {
    // Create the size that represents the size of a sparse linear algebra matrix
    return { 2, 2 };
  }

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { return 0; }
  virtual int indexQ() const override { return 0; }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVector& q) override
  {
    return true;
  }

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override
  {
    return 2;
  }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override
  {
    mpi::communicator world;
    return std::make_shared<mpi::communicator>(world.split(world.rank()));
  }
  virtual void syncDOFs_MPI() override {}

  virtual bool updateLinesearchDebugInfo(const Real& s, const SystemVector& rsd,
                                         LinesearchData& pResData,
                                         LinesearchData& pStepData) const override { return true; };

  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override {};


  // Are we having machine precision issues - assume no issues
  virtual bool atMachinePrecision(const SystemVector& q, const std::vector<std::vector<Real>>& R0norm) override
  {
    return false;
  }

protected:
  DLA::VectorS<2,Real> qfld_;

  template<class T, class Vector>
  void residual(const T& x, const T& y, Vector& rsd) const
  {
    //rsd[0] = x*x +   y - 5;
    //rsd[1] = x*x + y*y - 7;
    rsd[0] = x*y + x - 4*y - 11;
    rsd[1] = x*y - x - 4*y - 4;
  }
};


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarGoldenSearchLineUpdate_constructor )
{
  // No PyDict
  {
    DummyNonLinearEquation Equation;
    ScalarGoldenSearchLineUpdate<SparseMatrix> lineSearch(Equation);
  }

  // With PyDict
  {
    PyDict LineUpdateDict;
    DummyNonLinearEquation Equation;

    // Set up PyDict
    LineUpdateDict[ScalarGoldenSearchLineUpdateParam::params.tol] = 1.0e-15;
    LineUpdateDict[ScalarGoldenSearchLineUpdateParam::params.Verbose] = true;

    ScalarGoldenSearchLineUpdate<SparseMatrix> lineSearch(Equation, LineUpdateDict);
  }
}
#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarGoldenSearchLineUpdate_linesearch )
{
  PyDict LineUpdateDict;
  DummyNonLinearEquation Equation;

  // Set up PyDict
  LineUpdateDict[ScalarGoldenSearchLineUpdateParam::params.tol] = 1.0e-15;

  ScalarGoldenSearchLineUpdate<SystemVector,SparseMatrix,SparseMatrix::NonZeroPattern> lineSearch(Equation,
                                                                                                  LineUpdateDict);


  SystemVector X(2), Sln(2);
  NonZeroPattern nz(Equation.matrixSize());

  // Set X explicitly
  X(0) = 3.5;
  X(1) = 0.0;

  // Non-zero pattern
  Equation.jacobian(X, nz);

  // Set up Jacobian
  SparseMatrix dX(nz);

  // We only want to search along the y-direction to find the true minima
  dX = 0;
  dX(0,1) = -15.0;
  dX(1,1) = -15.0;

  // Do the line search
  lineSearch(X, dX, Sln);

  BOOST_CHECK_CLOSE(  3.5, Sln[0], 5e-12 );
  BOOST_CHECK_CLOSE( -15., Sln[1], 5e-12 );

}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
