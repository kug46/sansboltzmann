// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEEuler3D_btest
//
// test of 3-D compressible Euler PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
//#include "pde/NS/Q3DPrimitiveSurrogate.h"
#include "pde/NS/Q3DConservative.h"
//#include "pde/NS/Q3DEntropy.h"
#include "pde/NS/PDEEuler3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"

namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
class QTypeEntropy {};

// Explicitly instantiate the class so coverage information is correct
template class TraitsSizeEuler<PhysD3>;
template class TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>;
//template class TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>;
template class TraitsModelEuler<QTypeConservative, GasModel>;
//template class TraitsModelEuler<QTypeEntropy, GasModel>;
template class PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>>;
//template class PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>>;
template class PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypeConservative, GasModel>>;
//template class PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypeEntropy, GasModel>>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEEuler3D_test_suite )

typedef boost::mpl::list< QTypePrimitiveRhoPressure
                        , QTypeConservative
                      //, QTypePrimitiveSurrogate
                      //, QTypeEntropy
                        > QTypes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( static_test, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  BOOST_REQUIRE( PDEClass::D == 3 );
  BOOST_REQUIRE( PDEClass::N == 5 );
  BOOST_REQUIRE( ArrayQ::M == 5 );
  BOOST_REQUIRE( MatrixQ::M == 5 );
  BOOST_REQUIRE( MatrixQ::N == 5 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctor, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( pde.D == 3 );
  BOOST_REQUIRE( pde.N == 5 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setDOFFrom )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  Real rho = 1.137;
  Real u = 0.784;
  Real v = -0.231;
  Real w = 0.456;
  Real t = 0.987;
  Real p = R*rho*t;
  Real E = gas.energy(rho,t) + 0.5*(u*u + v*v + w*w);

  PyDict d;

  // set
  Real qDataPrim[5] = {rho, u, v, w, t};
  string qNamePrim[5] = {"Density", "VelocityX", "VelocityY", "VelocityZ", "Temperature"};
  ArrayQ qTrue = {rho, u, v, w, p};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

  DensityVelocityPressure3D<Real> qdata1;
  qdata1.Density = rho; qdata1.VelocityX = u; qdata1.VelocityY = v; qdata1.VelocityZ = w; qdata1.Pressure = p;
  q = 0;
  pde.setDOFFrom( q, qdata1 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

  PyDict rhoVP;
  rhoVP[NSVariableType3DParams::params.StateVector.Variables] = NSVariableType3DParams::params.StateVector.PrimitivePressure;
  rhoVP[DensityVelocityPressure3DParams::params.rho] = rho;
  rhoVP[DensityVelocityPressure3DParams::params.u] = u;
  rhoVP[DensityVelocityPressure3DParams::params.v] = v;
  rhoVP[DensityVelocityPressure3DParams::params.w] = w;
  rhoVP[DensityVelocityPressure3DParams::params.p] = p;

  d[NSVariableType3DParams::params.StateVector] = rhoVP;
  NSVariableType3DParams::checkInputs(d);
  q = pde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );


  DensityVelocityTemperature3D<Real> qdata2(rho, u, v, w, t);
  q = 0;
  pde.setDOFFrom( q, qdata2 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

  PyDict rhoVT;
  rhoVT[NSVariableType3DParams::params.StateVector.Variables] = NSVariableType3DParams::params.StateVector.PrimitiveTemperature;
  rhoVT[DensityVelocityTemperature3DParams::params.rho] = rho;
  rhoVT[DensityVelocityTemperature3DParams::params.u] = u;
  rhoVT[DensityVelocityTemperature3DParams::params.v] = v;
  rhoVT[DensityVelocityTemperature3DParams::params.w] = w;
  rhoVT[DensityVelocityTemperature3DParams::params.t] = t;

  d[NSVariableType3DParams::params.StateVector] = rhoVT;
  NSVariableType3DParams::checkInputs(d);
  q = pde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );


  Conservative3D<Real> qdata3(rho, rho*u, rho*v, rho*w, rho*E);
  q = 0;
  pde.setDOFFrom( q, qdata3 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

  PyDict Conservative;
  Conservative[NSVariableType3DParams::params.StateVector.Variables] = NSVariableType3DParams::params.StateVector.Conservative;
  Conservative[Conservative3DParams::params.rho] = rho;
  Conservative[Conservative3DParams::params.rhou] = rho*u;
  Conservative[Conservative3DParams::params.rhov] = rho*v;
  Conservative[Conservative3DParams::params.rhow] = rho*w;
  Conservative[Conservative3DParams::params.rhoE] = rho*E;

  d[NSVariableType3DParams::params.StateVector] = Conservative;
  NSVariableType3DParams::checkInputs(d);
  q = pde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  // function tests

  Real x, y, z, time;
  Real rho, u, v, w, t, p, e0, h0;
  Real Cv, Cp;

  x = y = z = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = -0.231; w = 0.456; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u + v*v + w*w);
  h0 = Cp*t + 0.5*(u*u + v*v + w*w);

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );

  // conservative flux
  ArrayQ uTrue = {rho, rho*u, rho*v, rho*w, rho*e0};
  ArrayQ uCons = 0;
  pde.masterState( x, y, z, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );
  BOOST_CHECK_CLOSE( uTrue(3), uCons(3), tol );

  // Should not accumulate
  pde.masterState( x, y, z, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );
  BOOST_CHECK_CLOSE( uTrue(3), uCons(3), tol );

  // conservative flux
  ArrayQ ftTrue = {rho, rho*u, rho*v, rho*w, rho*e0};
  ArrayQ ft = 0;
  pde.fluxAdvectiveTime( x, y, z, time, q, ft );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( ftTrue(3), ft(3), tol );

  // Flux accumulation
  pde.fluxAdvectiveTime( x, y, z, time, q, ft );
  BOOST_CHECK_CLOSE( 2*ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(3), ft(3), tol );

  // jacobianMasterState
  MatrixQ dudq = 0;
  MatrixQ dudqTrue = 0;

  ArrayQ rho_q, u_q, v_q, w_q, t_q;
  Real e_rho, e_t = 0;
  pde.variableInterpreter().evalJacobian( q, rho_q, u_q, v_q, w_q, t_q );
  Real e = pde.gasModel().energy(rho, t);
  pde.gasModel().energyJacobian(rho, t, e_rho, e_t);
  Real E = e + 0.5*(u*u + v*v + w*w);
  ArrayQ E_q = e_rho*rho_q + e_t*t_q + u*u_q + v*v_q + w*w_q;

  dudqTrue(0,0) = rho_q[0];
  dudqTrue(0,1) = rho_q[1];
  dudqTrue(0,2) = rho_q[2];
  dudqTrue(0,3) = rho_q[3];
  dudqTrue(0,4) = rho_q[4];

  dudqTrue(1,0) = rho_q[0]*u + rho*u_q[0];
  dudqTrue(1,1) = rho_q[1]*u + rho*u_q[1];
  dudqTrue(1,2) = rho_q[2]*u + rho*u_q[2];
  dudqTrue(1,3) = rho_q[3]*u + rho*u_q[3];
  dudqTrue(1,4) = rho_q[4]*u + rho*u_q[4];

  dudqTrue(2,0) = rho_q[0]*v + rho*v_q[0];
  dudqTrue(2,1) = rho_q[1]*v + rho*v_q[1];
  dudqTrue(2,2) = rho_q[2]*v + rho*v_q[2];
  dudqTrue(2,3) = rho_q[3]*v + rho*v_q[3];
  dudqTrue(2,4) = rho_q[4]*v + rho*v_q[4];

  dudqTrue(3,0) = rho_q[0]*w + rho*w_q[0];
  dudqTrue(3,1) = rho_q[1]*w + rho*w_q[1];
  dudqTrue(3,2) = rho_q[2]*w + rho*w_q[2];
  dudqTrue(3,3) = rho_q[3]*w + rho*w_q[3];
  dudqTrue(3,4) = rho_q[4]*w + rho*w_q[4];

  dudqTrue(4,0) = rho_q[0]*E + rho*E_q[0];
  dudqTrue(4,1) = rho_q[1]*E + rho*E_q[1];
  dudqTrue(4,2) = rho_q[2]*E + rho*E_q[2];
  dudqTrue(4,3) = rho_q[3]*E + rho*E_q[3];
  dudqTrue(4,4) = rho_q[4]*E + rho*E_q[4];

  pde.jacobianMasterState(x, y, z, time, q, dudq);
  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
      BOOST_CHECK_CLOSE( dudqTrue(i,j), dudq(i,j), tol );

  // Should not accumulate
  pde.jacobianMasterState(x, y, z, time, q, dudq);
  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
      BOOST_CHECK_CLOSE( dudqTrue(i,j), dudq(i,j), tol );

  // advective flux
  ArrayQ fxTrue = {rho*u, rho*u*u + p, rho*u*v    , rho*u*w    , rho*u*h0};
  ArrayQ fyTrue = {rho*v, rho*v*u    , rho*v*v + p, rho*v*w    , rho*v*h0};
  ArrayQ fzTrue = {rho*w, rho*w*u    , rho*w*v    , rho*w*w + p, rho*w*h0};
  ArrayQ fx = 0, fy = 0, fz = 0;

  pde.fluxAdvective( x, y, z, time, q, fx, fy, fz );
  BOOST_CHECK_CLOSE( fxTrue(0), fx(0), tol );
  BOOST_CHECK_CLOSE( fxTrue(1), fx(1), tol );
  BOOST_CHECK_CLOSE( fxTrue(2), fx(2), tol );
  BOOST_CHECK_CLOSE( fxTrue(3), fx(3), tol );
  BOOST_CHECK_CLOSE( fxTrue(4), fx(4), tol );

  BOOST_CHECK_CLOSE( fyTrue(0), fy(0), tol );
  BOOST_CHECK_CLOSE( fyTrue(1), fy(1), tol );
  BOOST_CHECK_CLOSE( fyTrue(2), fy(2), tol );
  BOOST_CHECK_CLOSE( fyTrue(3), fy(3), tol );
  BOOST_CHECK_CLOSE( fyTrue(4), fy(4), tol );

  BOOST_CHECK_CLOSE( fzTrue(0), fz(0), tol );
  BOOST_CHECK_CLOSE( fzTrue(1), fz(1), tol );
  BOOST_CHECK_CLOSE( fzTrue(2), fz(2), tol );
  BOOST_CHECK_CLOSE( fzTrue(3), fz(3), tol );
  BOOST_CHECK_CLOSE( fzTrue(4), fz(4), tol );

  // Flux accumulation
  pde.fluxAdvective( x, y, z, time, q, fx, fy, fz );
  BOOST_CHECK_CLOSE( 2*fxTrue(0), fx(0), tol );
  BOOST_CHECK_CLOSE( 2*fxTrue(1), fx(1), tol );
  BOOST_CHECK_CLOSE( 2*fxTrue(2), fx(2), tol );
  BOOST_CHECK_CLOSE( 2*fxTrue(3), fx(3), tol );
  BOOST_CHECK_CLOSE( 2*fxTrue(4), fx(4), tol );

  BOOST_CHECK_CLOSE( 2*fyTrue(0), fy(0), tol );
  BOOST_CHECK_CLOSE( 2*fyTrue(1), fy(1), tol );
  BOOST_CHECK_CLOSE( 2*fyTrue(2), fy(2), tol );
  BOOST_CHECK_CLOSE( 2*fyTrue(3), fy(3), tol );
  BOOST_CHECK_CLOSE( 2*fyTrue(4), fy(4), tol );

  BOOST_CHECK_CLOSE( 2*fzTrue(0), fz(0), tol );
  BOOST_CHECK_CLOSE( 2*fzTrue(1), fz(1), tol );
  BOOST_CHECK_CLOSE( 2*fzTrue(2), fz(2), tol );
  BOOST_CHECK_CLOSE( 2*fzTrue(3), fz(3), tol );
  BOOST_CHECK_CLOSE( 2*fzTrue(4), fz(4), tol );


  // Call these simply to make sure coverage is high
  ArrayQ qL = q, qR = q, qx = 0, qy = 0, qz = 0, qxL = 0, qxR = 0, qyL = 0, qyR = 0, qzL = 0, qzR = 0, source = 0;
  ArrayQ qp = 0, qpx = 0, qpy = 0, qpz = 0;
  Real dummy = 0;
  MatrixQ kxx, kxy, kxz;
  MatrixQ kyx, kyy, kyz;
  MatrixQ kzx, kzy, kzz;
  pde.fluxViscous( dummy, dummy, dummy, dummy, q, qx, qy, qz, fx, fy, fz );
  pde.diffusionViscous( dummy, dummy, dummy, dummy, q, qx, qy, qz, kxx, kxy, kxz, kyx, kyy, kyz, kzx, kzy, kzz );
  pde.source( dummy, dummy, dummy, dummy, q, qx, qy, qz, source );
  pde.source( dummy, dummy, dummy, dummy, q, qp, qx, qy, qz, qpx, qpy, qpz, source );
  pde.forcingFunction( x, y, z, dummy, source );

  ArrayQ fn = 0, sourceL = 0 , sourceR = 0;
  Real nx=0, ny=0, nz=0;
  pde.fluxViscous( dummy, dummy, dummy, dummy, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, nx, ny, nz, fn );
  pde.sourceTrace( dummy, dummy, dummy, dummy, dummy, dummy, dummy, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, sourceL, sourceR );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( derivedQuantites, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  typedef Q3D<QType, TraitsSizeEuler> QInterpret;

  const Real tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  QInterpret qInterpret(gas);

  // function tests

  Real x, y, z, time;
  Real rho, u, v, w, t;
  Real rho2, u2, v2, w2, t2;

  x = y = z = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = -0.231; w = 0.456; t = 0.987;
  rho2 = rho+0.05;
  u2 = u-0.07;
  v2 = v+0.32;
  w2 = w-0.11;
  t2 = t+0.02;

  // set
  ArrayQ q = 0, q2 = 0, qx = 0, qy = 0, qz = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );
  pde.setDOFFrom( q2, DensityVelocityTemperature3D<Real>(rho2, u2, v2, w2, t2) );

  // make up some gradients
  Real delta_x = 0.1, delta_y = 0.05, delta_z = 0.03;
  qx = (q2-q)/delta_x;
  qy = (q2-q)/delta_y;
  qz = (q2-q)/delta_z;

  std::vector<std::string> namesTrue = {"mach","streamwiseVorticity"};

  std::vector<std::string> names = pde.derivedQuantityNames();

  BOOST_CHECK_EQUAL( names.size(), namesTrue.size() );

  for (int i = 0; i < (int) namesTrue.size(); i++)
    BOOST_CHECK_EQUAL( namesTrue[i], names[i] );

  std::vector<Real> output(names.size(), 0.0);

  for (int i = 0; i < (int)names.size(); i++)
    pde.derivedQuantity(i, x, y, z, time, q, qx, qy, qz, output[i]);

  Real trueMach = sqrt( u*u + v*v + w*w )/gas.speedofSound(t);
  SANS_CHECK_CLOSE( output[0], trueMach, tol, tol );

  Real rhox, ux, vx, wx, tx;
  Real rhoy, uy, vy, wy, ty;
  Real rhoz, uz, vz, wz, tz;
  qInterpret.evalGradient( q, qx, rhox, ux, vx, wx, tx);
  qInterpret.evalGradient( q, qy, rhoy, uy, vy, wy, ty);
  qInterpret.evalGradient( q, qz, rhoz, uz, vz, wz, tz);

  Real vortx = (wy-vz);
  Real vorty = (uz-wx);
  Real vortz = (vx-uy);
  Real trueStreamVort = u*vortx + v*vorty + z*vortz;

  SANS_CHECK_CLOSE( output[1], trueStreamVort, tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Conservative_Jacobians )
{
  // Uses automatic differentiation to verify Jacobian functions d()/du
  typedef QTypeConservative QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template ArrayQ< SurrealS<PDEClass::N> > ArrayQSurreal;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  // function tests

  Real x, y, z, time;
  Real rho, u, v, w, t;

  x = y = z = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = -0.231; w = 0.456; t = 0.987;

  ArrayQ q = pde.setDOFFrom( DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );
  ArrayQSurreal qSurreal = q;
  ArrayQSurreal fx = 0, fy = 0, fz = 0;

  qSurreal[0].deriv(0) = 1;
  qSurreal[1].deriv(1) = 1;
  qSurreal[2].deriv(2) = 1;
  qSurreal[3].deriv(3) = 1;
  qSurreal[4].deriv(4) = 1;

  pde.fluxAdvective( x, y, z, time, qSurreal, fx, fy, fz );

  MatrixQ dfxdu=0, dfydu=0, dfzdu=0;

  pde.jacobianFluxAdvective( x, y, z, time, q, dfxdu, dfydu, dfzdu );
  for (int i = 0; i < PDEClass::N; i++)
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( fx[i].deriv(j), dfxdu(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( fy[i].deriv(j), dfydu(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( fz[i].deriv(j), dfzdu(i,j), small_tol, close_tol );
    }

  // Flux accumulation
  pde.jacobianFluxAdvective( x, y, z, time, q, dfxdu, dfydu, dfzdu );
  for (int i = 0; i < PDEClass::N; i++)
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( 2*fx[i].deriv(j), dfxdu(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( 2*fy[i].deriv(j), dfydu(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( 2*fz[i].deriv(j), dfzdu(i,j), small_tol, close_tol );
    }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PrimitiveRhoPressure_StrongFlux )
{
  // Separate test for Primitive here to bypass generalization of QInterpret.
  // This ensures that the strong flux works barring any bugs in QInterpret
  // Next test uses QInterpret
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  // function tests

  Real x, y, z, time;
  Real rho, u, v, w, t, p, e0;
  Real Cv, Cp;

  x = y = z = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = -0.231; w = 0.456; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u + v*v + w*w);

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );

  // Strong Conservative Flux (PRIMITIVE SPECIFIC)
  Real rhot = -0.9;
  Real ut = -0.25;
  Real vt = 0.14;
  Real wt = 0.32;
  Real pt = 0.23;
  Real tt = (t/p) * pt - (t/rho)*rhot;

  Real e0t = Cv*tt + u*ut + v*vt + w*wt;

  ArrayQ qt = {rhot, ut, vt, wt, pt};
  ArrayQ utCons, utConsTrue;
  utCons = 0;
  utConsTrue[0] = rhot;
  utConsTrue[1] = rho*ut + rhot*u;
  utConsTrue[2] = rho*vt + rhot*v;
  utConsTrue[3] = rho*wt + rhot*w;
  utConsTrue[4] = rho*e0t + rhot*e0;
  pde.strongFluxAdvectiveTime( x, y, z, time, q, qt, utCons );
  BOOST_CHECK_CLOSE( utConsTrue(0), utCons(0), tol );
  BOOST_CHECK_CLOSE( utConsTrue(1), utCons(1), tol );
  BOOST_CHECK_CLOSE( utConsTrue(2), utCons(2), tol );
  BOOST_CHECK_CLOSE( utConsTrue(3), utCons(3), tol );
  BOOST_CHECK_CLOSE( utConsTrue(4), utCons(4), tol );

  // Flux accumulate
  pde.strongFluxAdvectiveTime( x, y, z, time, q, qt, utCons );
  BOOST_CHECK_CLOSE( 2*utConsTrue(0), utCons(0), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(1), utCons(1), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(2), utCons(2), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(3), utCons(3), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(4), utCons(4), tol );

  // Strong Advective Flux
  Real rhox = -0.02;
  Real rhoy = 0.05;
  Real rhoz = 0.03;
  Real ux = 0.072;
  Real uy = -0.036;
  Real uz = 0.125;
  Real vx = -0.0234;
  Real vy = 0.0898;
  Real vz = 0.0456;
  Real wx = -0.12;
  Real wy = 0.024;
  Real wz = 0.026;
  Real px = 10.2;
  Real py = 0.05;
  Real pz = 0.16;

  //PRIMITIVE SPECIFIC
  ArrayQ qx = {rhox, ux, vx, wx, px};
  ArrayQ qy = {rhoy, uy, vy, wy, py};
  ArrayQ qz = {rhoz, uz, vz, wz, pz};

  Real tx = (t/p) * px - (t/rho)*rhox;
  Real ty = (t/p) * py - (t/rho)*rhoy;
  Real tz = (t/p) * pz - (t/rho)*rhoz;

  Real H = Cp*t + 0.5*(u*u + v*v + w*w);
  Real Hx = Cp*tx + (ux*u + vx*v + wx*w);
  Real Hy = Cp*ty + (uy*u + vy*v + wy*w);
  Real Hz = Cp*tz + (uz*u + vz*v + wz*w);

  ArrayQ strongAdv = 0;
  ArrayQ strongAdvTrue = 0;
  strongAdvTrue(0) += rhox*u   +   rho*ux;
  strongAdvTrue(1) += rhox*u*u + 2*rho*ux*u + px;
  strongAdvTrue(2) += rhox*u*v +   rho*ux*v + rho*u*vx;
  strongAdvTrue(3) += rhox*u*w +   rho*ux*w + rho*u*wx;
  strongAdvTrue(4) += rhox*u*H +   rho*ux*H + rho*u*Hx;

  strongAdvTrue(0) += rhoy*v   +   rho*vy;
  strongAdvTrue(1) += rhoy*v*u +   rho*vy*u + rho*v*uy;
  strongAdvTrue(2) += rhoy*v*v + 2*rho*vy*v + py;
  strongAdvTrue(3) += rhoy*v*w +   rho*vy*w + rho*v*wy;
  strongAdvTrue(4) += rhoy*v*H +   rho*vy*H + rho*v*Hy;

  strongAdvTrue(0) += rhoz*w   +   rho*wz;
  strongAdvTrue(1) += rhoz*w*u +   rho*wz*u + rho*w*uz;
  strongAdvTrue(2) += rhoz*w*v +   rho*wz*v + rho*w*vz;
  strongAdvTrue(3) += rhoz*w*w + 2*rho*wz*w + pz;
  strongAdvTrue(4) += rhoz*w*H +   rho*wz*H + rho*w*Hz;

  pde.strongFluxAdvective( x, y, z, time, q, qx, qy, qz, strongAdv );
  BOOST_CHECK_CLOSE( strongAdvTrue(0), strongAdv(0), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(1), strongAdv(1), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(2), strongAdv(2), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(3), strongAdv(3), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(4), strongAdv(4), tol );

  // Flux accumulation
  pde.strongFluxAdvective( x, y, z, time, q, qx, qy, qz, strongAdv );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(0), strongAdv(0), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(1), strongAdv(1), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(2), strongAdv(2), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(3), strongAdv(3), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(4), strongAdv(4), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( strongFlux, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  typedef Q3D<QType, TraitsSizeEuler> QInterpret;

  const Real tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  QInterpret qInterpret(gas);

  // function tests

  Real x, y, z, time;
  Real rho, u, v, w, t, e0;
  Real Cv, Cp;

  x = y = z = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = -0.231; w = 0.456; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  e0 = Cv*t + 0.5*(u*u + v*v + w*w);

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );

  // Strong Conservative Flux
  ArrayQ qt = {-0.9, -0.25, 0.14, 0.78, 0.23};

  Real rhot, ut, vt, wt, et;
  Real tt; //d(temperature)/d(time)
  qInterpret.evalGradient( q, qt, rhot, ut, vt, wt, tt);
  gas.energyGradient(rho, t, rhot, tt, et);
  Real e0t = et + u*ut + v*vt + w*wt;

  ArrayQ utCons, utConsTrue;

  utConsTrue[0] = rhot;
  utConsTrue[1] = rho*ut + rhot*u;
  utConsTrue[2] = rho*vt + rhot*v;
  utConsTrue[3] = rho*wt + rhot*w;
  utConsTrue[4] = rho*e0t + rhot*e0;

  utCons = 0;
  pde.strongFluxAdvectiveTime( x, y, z, time, q, qt, utCons );
  BOOST_CHECK_CLOSE( utConsTrue(0), utCons(0), tol );
  BOOST_CHECK_CLOSE( utConsTrue(1), utCons(1), tol );
  BOOST_CHECK_CLOSE( utConsTrue(2), utCons(2), tol );
  BOOST_CHECK_CLOSE( utConsTrue(3), utCons(3), tol );
  BOOST_CHECK_CLOSE( utConsTrue(4), utCons(4), tol );

  // Flux accumulate
  pde.strongFluxAdvectiveTime( x, y, z, time, q, qt, utCons );
  BOOST_CHECK_CLOSE( 2*utConsTrue(0), utCons(0), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(1), utCons(1), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(2), utCons(2), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(3), utCons(3), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(4), utCons(4), tol );


  // Strong Advective Flux
  Real rhox, ux, vx, wx, tx, px, hx;
  Real rhoy, uy, vy, wy, ty, py, hy;
  Real rhoz, uz, vz, wz, tz, pz, hz;

  ArrayQ qx = {-0.02,  0.072, -0.0234, -0.12, 10.2};
  ArrayQ qy = { 0.05, -0.036,  0.0898, 0.024, 0.05};
  ArrayQ qz = { 0.03,  0.125,  0.0456, 0.026, 0.16};

  qInterpret.eval( q, rho, u, v, w, t );
  qInterpret.evalGradient( q, qx, rhox, ux, vx, wx, tx);
  qInterpret.evalGradient( q, qy, rhoy, uy, vy, wy, ty);
  qInterpret.evalGradient( q, qz, rhoz, uz, vz, wz, tz);

  gas.pressureGradient(rho, t, rhox, tx, px);
  gas.pressureGradient(rho, t, rhoy, ty, py);
  gas.pressureGradient(rho, t, rhoz, tz, pz);

  gas.enthalpyGradient(rho, t, rhox, tx, hx);
  gas.enthalpyGradient(rho, t, rhoy, ty, hy);
  gas.enthalpyGradient(rho, t, rhoz, tz, hz);

  Real H = Cp*t + 0.5*(u*u + v*v + w*w);
  Real Hx = Cp*tx + (ux*u + vx*v + wx*w);
  Real Hy = Cp*ty + (uy*u + vy*v + wy*w);
  Real Hz = Cp*tz + (uz*u + vz*v + wz*w);

  ArrayQ strongAdv = 0;
  ArrayQ strongAdvTrue = 0;
  strongAdvTrue(0) += rhox*u   +   rho*ux;
  strongAdvTrue(1) += rhox*u*u + 2*rho*ux*u + px;
  strongAdvTrue(2) += rhox*u*v +   rho*ux*v + rho*u*vx;
  strongAdvTrue(3) += rhox*u*w +   rho*ux*w + rho*u*wx;
  strongAdvTrue(4) += rhox*u*H +   rho*ux*H + rho*u*Hx;

  strongAdvTrue(0) += rhoy*v   +   rho*vy;
  strongAdvTrue(1) += rhoy*v*u +   rho*vy*u + rho*v*uy;
  strongAdvTrue(2) += rhoy*v*v + 2*rho*vy*v + py;
  strongAdvTrue(3) += rhoy*v*w +   rho*vy*w + rho*v*wy;
  strongAdvTrue(4) += rhoy*v*H +   rho*vy*H + rho*v*Hy;

  strongAdvTrue(0) += rhoz*w   +   rho*wz;
  strongAdvTrue(1) += rhoz*w*u +   rho*wz*u + rho*w*uz;
  strongAdvTrue(2) += rhoz*w*v +   rho*wz*v + rho*w*vz;
  strongAdvTrue(3) += rhoz*w*w + 2*rho*wz*w + pz;
  strongAdvTrue(4) += rhoz*w*H +   rho*wz*H + rho*w*Hz;

  pde.strongFluxAdvective( x, y, z, time, q, qx, qy, qz, strongAdv );
  BOOST_CHECK_CLOSE( strongAdvTrue(0), strongAdv(0), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(1), strongAdv(1), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(2), strongAdv(2), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(3), strongAdv(3), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(4), strongAdv(4), tol );

  // Flux accumulate
  pde.strongFluxAdvective( x, y, z, time, q, qx, qy, qz, strongAdv );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(0), strongAdv(0), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(1), strongAdv(1), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(2), strongAdv(2), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(3), strongAdv(3), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(4), strongAdv(4), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( jacobianMasterState, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename PDEClass::template ArrayQ<SurrealS<PDEClass::N>> ArrayQSurreal;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( pde.N == 5 );

  // function tests

  Real rho, u, v, w, t;

  rho = 1.137; u = 0.784; v = -0.231; w = 0.456; t = 0.987;

  Real x = 0, y = 0, z = 0, time = 0;

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );

  if ( std::is_same<QType,QTypeEntropy>::value )
  {
    // Entropy variables seem to have numerical problems when comparing Surreal and
    // hand differentiated code...
    q[0] = -10;
    q[1] = 2;
    q[2] = 4;
    q[3] = -3;
    q[4] = 3;
  }

  ArrayQSurreal qSurreal = q;

  qSurreal[0].deriv(0) = 1;
  qSurreal[1].deriv(1) = 1;
  qSurreal[2].deriv(2) = 1;
  qSurreal[3].deriv(3) = 1;
  qSurreal[4].deriv(4) = 1;

  // conservative flux
  ArrayQSurreal uConsSurreal = 0;
  pde.masterState( x, y, z, time, qSurreal, uConsSurreal );


  const Real small_tol = 5e-10;
  const Real close_tol = 1e-12;

  MatrixQ dudq = 0;
  pde.jacobianMasterState( x, y, z, time, q, dudq );
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(0), dudq(0,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(1), dudq(0,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(2), dudq(0,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(3), dudq(0,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(4), dudq(0,4), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(0), dudq(1,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(1), dudq(1,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(2), dudq(1,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(3), dudq(1,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(4), dudq(1,4), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(0), dudq(2,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(1), dudq(2,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(2), dudq(2,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(3), dudq(2,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(4), dudq(2,4), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(0), dudq(3,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(1), dudq(3,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(2), dudq(3,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(3), dudq(3,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(4), dudq(3,4), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[4].deriv(0), dudq(4,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[4].deriv(1), dudq(4,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[4].deriv(2), dudq(4,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[4].deriv(3), dudq(4,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[4].deriv(4), dudq(4,4), small_tol, close_tol )

  // Should not accumulate
  pde.jacobianMasterState( x, y, z, time, q, dudq );
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(0), dudq(0,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(1), dudq(0,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(2), dudq(0,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(3), dudq(0,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(4), dudq(0,4), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(0), dudq(1,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(1), dudq(1,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(2), dudq(1,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(3), dudq(1,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(4), dudq(1,4), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(0), dudq(2,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(1), dudq(2,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(2), dudq(2,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(3), dudq(2,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(4), dudq(2,4), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(0), dudq(3,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(1), dudq(3,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(2), dudq(3,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(3), dudq(3,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(4), dudq(3,4), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[4].deriv(0), dudq(4,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[4].deriv(1), dudq(4,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[4].deriv(2), dudq(4,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[4].deriv(3), dudq(4,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[4].deriv(4), dudq(4,4), small_tol, close_tol )
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( jacobianFluxAdvectiveAbsoluteValue_test, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  const Real small_tol = 1.e-14;
  const Real close_tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;

  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  Real x, y, z, time;
  Real nx, ny, nz;
  Real rho, u, v, w, t;

  x = y = z = time = 0;   // not actually used in functions
  rho = 1.034; u = 1.26; v = -2.17; w = 0.56; t = 5.78;

  ArrayQ q = pde.setDOFFrom( DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );

  MatrixQ mtx, mtxTrue;

  // {qn - c, qn, qn + c} = {neg, neg, neg}

  nx = -0.856;  ny = 0.48;  nz = 0.192;   // Note: magnitude = 1

  mtxTrue =
    {{0., 0.856, -0.48, -0.192, 0.},
     {-1.40427728, 2.659776, 0.138208, -0.433664, 0.3424},
     {3.7328592, -1.6156, 2.6376, 0.52416, -0.192},
     {-1.38090624, 0.576128, -0.435456, 1.948128, -0.0768},
     {-20.2774083792, 8.74150424, -3.72361248, -2.63906496, 2.817696}};

  mtx = 0;
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, nx, ny, nz, mtx );
  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
      SANS_CHECK_CLOSE( mtxTrue(i,j), mtx(i,j), small_tol, close_tol );

  // Flux accumulate
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, nx, ny, nz, mtx );
  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
      SANS_CHECK_CLOSE( 2*mtxTrue(i,j), mtx(i,j), small_tol, close_tol );

  // {qn - c, qn, qn + c} = {neg, neg, pos}

  nx = -0.192;  ny = 0.48;  nz = -0.856;   // Note: magnitude = 1

  mtxTrue =
    {{0.05029920307361089, 0.1824919733978881, -0.4606177577277211, 0.8362543552680354, 0.004477371666327435},
     {-1.921398782191514, 1.899336237370957, -0.4204175689463774, 1.017493211701233, 0.08089487297855683},
     {3.125167774490924, -0.1622984594974613, 2.362518519792777, -1.724203761090408, -0.1978493582133912},
     {0.09514114318471302, -0.3145857638476372, 0.4552126481553504, 2.069847500399908, 0.3380120014936149},
     {-17.34733614186995, 1.221534440499240, -3.780976817210479, 9.198573194484009, 2.504860322410591}};

  mtx = 0;
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, nx, ny, nz, mtx );
  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
      SANS_CHECK_CLOSE( mtxTrue(i,j), mtx(i,j), small_tol, close_tol );

  // Flux accumulate
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, nx, ny, nz, mtx );
  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
      SANS_CHECK_CLOSE( 2*mtxTrue(i,j), mtx(i,j), small_tol, close_tol );

  // {qn - c, qn, qn + c} = {neg, pos, pos}

  nx = -0.48;  ny = -0.856;  nz = -0.192;   // Note: magnitude = 1

  mtxTrue =
    {{0.6833178495735146, -0.4073576217113168, -0.3695186359806880, -0.1674683835739656, 0.08080955159712328},
     {-0.6264461084779726, 0.9365814097471189, -0.4621211994809992, -0.08230531611978917, -0.02039499966720123},
     {0.9229712316480684, 1.427262129147915, 1.953247676052250, 0.5929300364992487, -0.3933068721443357},
     {-0.2764438438150722, -0.1062594629169862, -0.2055415234073182, 1.102899644071942, -0.003632664977441590},
     {-5.157984981879059, -5.369524110454105, -4.219706655995295, -2.215713855583411, 2.357775203603017}};

  mtx = 0;
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, nx, ny, nz, mtx );
  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
      SANS_CHECK_CLOSE( mtxTrue(i,j), mtx(i,j), small_tol, close_tol );

  // Flux accumulate
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, nx, ny, nz, mtx );
  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
      SANS_CHECK_CLOSE( 2*mtxTrue(i,j), mtx(i,j), small_tol, close_tol );

  // {qn - c, qn, qn + c} = {pos, pos, pos}

  nx = -0.192;  ny = -0.856;  nz = 0.48;   // Note: magnitude = 1

  mtxTrue =
    {{0, -0.192, -0.856, 0.48, 0},
     {-2.62817184, 1.739248, -1.245216, 0.647808, -0.0768},
     {2.95749888, 0.848064, 2.998912, -0.849856, -0.3424},
     {-0.4206944, -0.34944, -0.06272, 2.04568, 0.192},
     {-18.985386532, -3.1379712, -8.1202156, 5.0484784, 2.63816}};

  mtx = 0;
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, nx, ny, nz, mtx );
  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
      SANS_CHECK_CLOSE( mtxTrue(i,j), mtx(i,j), small_tol, close_tol );

  // Flux accumulate
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, nx, ny, nz, mtx );
  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
      SANS_CHECK_CLOSE( 2*mtxTrue(i,j), mtx(i,j), small_tol, close_tol );

  // {qn - c, qn, qn + c} = {pos, pos, pos}

  nx = u;  ny = v;  nz = w;   // Note: magnitude > 1

  mtxTrue =
  {{0, 1.26, -2.17, 0.56, 0},
   {-6.6629808, 7.56266, -1.64052, 0.42336, 0.504},
   {11.4751336, -1.64052, 9.43544, -0.72912, -0.868},
   {-2.9613248, 0.42336, -0.72912, 6.79826, 0.224},
   {-66.596955803, 11.0287926, -18.9940317, 4.9016856, 9.25414}};

  mtx = 0;
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, nx, ny, nz, mtx );
  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
      SANS_CHECK_CLOSE( mtxTrue(i,j), mtx(i,j), small_tol, close_tol );

  // Flux accumulate
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, nx, ny, nz, mtx );
  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
      SANS_CHECK_CLOSE( 2*mtxTrue(i,j), mtx(i,j), small_tol, close_tol );

  // {nx, ny, nz} = {0, 0, 0}

  nx = 0;  ny = 0;  nz = 0;   // Note: magnitude == 0

  mtxTrue = 0;

  const Real small_tol2 = 1.e-12;
  mtx = 0;
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, nx, ny, nz, mtx );
  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
      SANS_CHECK_CLOSE( mtxTrue(i,j), mtx(i,j), small_tol2, close_tol );


#if 0
  printf( "btest: mtx = \n" );
  for (int i=0; i<5; i++)
  {
    for (int j = 0; j < 5; j++)
      printf( " %13.5e", mtx(i,j) );
    printf( "\n" );
  }
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux_NDSteady, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> PDENDClass;
  typedef typename PDENDClass::VectorX VectorX;
  typedef typename PDENDClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDENDClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename PDENDClass::template TensorMatrixQ<Real> TensorMatrixQ;

  const Real tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDENDClass pdeND(gas, Euler_ResidInterp_Raw);

  // function tests

  VectorX X;
  Real rho, u, v, w, t, p, e0, h0;
  Real Cv, Cp;

  X = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = -0.231; w = 0.456; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u + v*v + w*w);
  h0 = Cp*t + 0.5*(u*u + v*v + w*w);

  // set
  ArrayQ q;
  pdeND.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );

  // conservative flux
  ArrayQ uTrue = {rho, rho*u, rho*v, rho*w, rho*e0};
  ArrayQ uCons = 0;
  pdeND.masterState( X, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );
  BOOST_CHECK_CLOSE( uTrue(3), uCons(3), tol );
  BOOST_CHECK_CLOSE( uTrue(4), uCons(4), tol );

  // Should not accumulate
  pdeND.masterState( X, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );
  BOOST_CHECK_CLOSE( uTrue(3), uCons(3), tol );
  BOOST_CHECK_CLOSE( uTrue(4), uCons(4), tol );

  // advective flux
  VectorArrayQ F = 0;
  ArrayQ fxTrue = {rho*u, rho*u*u + p, rho*u*v    , rho*u*w    , rho*u*h0};
  ArrayQ fyTrue = {rho*v, rho*v*u    , rho*v*v + p, rho*v*w    , rho*v*h0};
  ArrayQ fzTrue = {rho*w, rho*w*u    , rho*w*v    , rho*w*w + p, rho*w*h0};
  ArrayQ fx = 0, fy = 0, fz = 0;

  pdeND.fluxAdvective( X, q, F );
  BOOST_CHECK_CLOSE( fxTrue(0), F[0](0), tol );
  BOOST_CHECK_CLOSE( fxTrue(1), F[0](1), tol );
  BOOST_CHECK_CLOSE( fxTrue(2), F[0](2), tol );
  BOOST_CHECK_CLOSE( fxTrue(3), F[0](3), tol );
  BOOST_CHECK_CLOSE( fxTrue(4), F[0](4), tol );

  BOOST_CHECK_CLOSE( fyTrue(0), F[1](0), tol );
  BOOST_CHECK_CLOSE( fyTrue(1), F[1](1), tol );
  BOOST_CHECK_CLOSE( fyTrue(2), F[1](2), tol );
  BOOST_CHECK_CLOSE( fyTrue(3), F[1](3), tol );
  BOOST_CHECK_CLOSE( fyTrue(4), F[1](4), tol );

  BOOST_CHECK_CLOSE( fzTrue(0), F[2](0), tol );
  BOOST_CHECK_CLOSE( fzTrue(1), F[2](1), tol );
  BOOST_CHECK_CLOSE( fzTrue(2), F[2](2), tol );
  BOOST_CHECK_CLOSE( fzTrue(3), F[2](3), tol );
  BOOST_CHECK_CLOSE( fzTrue(4), F[2](4), tol );

  // Flux accumulate
  pdeND.fluxAdvective( X, q, F );
  BOOST_CHECK_CLOSE( 2*fxTrue(0), F[0](0), tol );
  BOOST_CHECK_CLOSE( 2*fxTrue(1), F[0](1), tol );
  BOOST_CHECK_CLOSE( 2*fxTrue(2), F[0](2), tol );
  BOOST_CHECK_CLOSE( 2*fxTrue(3), F[0](3), tol );
  BOOST_CHECK_CLOSE( 2*fxTrue(4), F[0](4), tol );

  BOOST_CHECK_CLOSE( 2*fyTrue(0), F[1](0), tol );
  BOOST_CHECK_CLOSE( 2*fyTrue(1), F[1](1), tol );
  BOOST_CHECK_CLOSE( 2*fyTrue(2), F[1](2), tol );
  BOOST_CHECK_CLOSE( 2*fyTrue(3), F[1](3), tol );
  BOOST_CHECK_CLOSE( 2*fyTrue(4), F[1](4), tol );

  BOOST_CHECK_CLOSE( 2*fzTrue(0), F[2](0), tol );
  BOOST_CHECK_CLOSE( 2*fzTrue(1), F[2](1), tol );
  BOOST_CHECK_CLOSE( 2*fzTrue(2), F[2](2), tol );
  BOOST_CHECK_CLOSE( 2*fzTrue(3), F[2](3), tol );
  BOOST_CHECK_CLOSE( 2*fzTrue(4), F[2](4), tol );

  //Call these simply to make sure coverage is high
  ArrayQ qL, qR, qx, qy, qxL, qxR, qyL, qyR, source;
  VectorArrayQ gradq = 0;
  TensorMatrixQ K;
  pdeND.fluxViscous( X, q, gradq, F );
  pdeND.diffusionViscous( X, q, gradq, K );
  pdeND.source( X, q, gradq, source );
  pdeND.forcingFunction( X, source );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxRoe, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 2.e-11;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  // Roe flux function test

  Real x, y, z, time;
  Real nx, ny, nz;
  Real rhoL, uL, vL, wL, tL, pL, h0L;
  Real rhoR, uR, vR, wR, tR, pR, h0R;
  Real Cp;

  x = y = z = time = 0;   // not actually used in functions
  nx = 2.0/7.0; ny = 1.0/7.0; nz = sqrt(1.0 - nx*nx - ny*ny);
  rhoL = 1.034; uL = 3.26; vL = -2.17; wL = 3.56; tL = 5.78;
  rhoR = 0.973; uR = 1.79; vR =  0.93; wR = 5.23; tR = 6.13;
  Cp  = R*gamma/(gamma - 1);

  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL + vL*vL + wL*wL);

  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR + vR*vR + wR*wR);

  // set
  ArrayQ qL = pde.setDOFFrom( DensityVelocityTemperature3D<Real>(rhoL, uL, vL, wL, tL) );
  ArrayQ qR = pde.setDOFFrom( DensityVelocityTemperature3D<Real>(rhoR, uR, vR, wR, tR) );

  // advective normal flux (average)
  ArrayQ fxL = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*vL     , rhoL*uL*wL     , rhoL*uL*h0L};
  ArrayQ fyL = {rhoL*vL, rhoL*vL*uL     , rhoL*vL*vL + pL, rhoL*vL*wL     , rhoL*vL*h0L};
  ArrayQ fzL = {rhoL*wL, rhoL*wL*uL     , rhoL*wL*vL     , rhoL*wL*wL + pL, rhoL*wL*h0L};

  ArrayQ fxR = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*vR     , rhoR*uR*wR     , rhoR*uR*h0R};
  ArrayQ fyR = {rhoR*vR, rhoR*vR*uR     , rhoR*vR*vR + pR, rhoR*vR*wR     , rhoR*vR*h0R};
  ArrayQ fzR = {rhoR*wR, rhoR*wR*uR     , rhoR*wR*vR     , rhoR*wR*wR + pR, rhoR*wR*h0R};

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < PDEClass::N; k++)
    fnTrue[k] = 0.5*(nx*(fxL[k] + fxR[k]) + ny*(fyL[k] + fyR[k]) + nz*(fzL[k] + fzR[k]));

#if 1
  // Exact values for Roe scheme from Roe.nb
  fnTrue[0] -= 0.65558457182422097471369683881548;
  fnTrue[1] -= -1.8638923372862592525182437515145;
  fnTrue[2] -=  7.0186535948930936601201334787757;
  fnTrue[3] -=  6.8854604538052794714508533474917;
  fnTrue[4] -= 20.499927239215613126430884098724;
#endif

  ArrayQ fn = 0;
  pde.fluxAdvectiveUpwind( x, y, z, time, qL, qR, nx, ny, nz, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( fnTrue(3), fn(3), tol );
  BOOST_CHECK_CLOSE( fnTrue(4), fn(4), tol );

  // Flux accumulate
  pde.fluxAdvectiveUpwind( x, y, z, time, qL, qR, nx, ny, nz, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(3), fn(3), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(4), fn(4), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( speedCharacteristic, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  Real x, y, z, time, dx, dy, dz;
  Real rho, u, v, w, t, c;
  Real speed, speedTrue;

  x = y = z = time = 0;   // not actually used in functions
  dx = 0.57; dy = -0.43; dz = 0.76;
  rho = 1.137; u = 0.784; v = -0.231; w = 0.456; t = 0.987;
  c = sqrt(gamma*R*t);
  speedTrue = fabs(dx*u + dy*v + dz*w)/sqrt(dx*dx + dy*dy + dz*dz) + c;

  ArrayQ q;
  pde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );

  pde.speedCharacteristic( x, y, z, time, dx, dy, dz, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  pde.speedCharacteristic( x, y, z, time, q, speed );
  BOOST_CHECK_CLOSE( sqrt(u*u + v*v + w*w) + c, speed, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( isValidState, QType, QTypes )
{
  // Surrogates are special
  if ( std::is_same<QType,QTypePrimitiveSurrogate>::value ) return;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  ArrayQ q;
  Real rho, u = 0, v = 0, w = 0, t;

  rho = 1;
  t = 1;
  pde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );
  BOOST_CHECK( pde.isValidState(q) == true );

  rho = 1;
  t = -1;
  pde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );
  BOOST_CHECK( pde.isValidState(q) == false );

  rho = -1;
  t = 1;
  pde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );
  BOOST_CHECK( pde.isValidState(q) == false );

  rho = -1;
  t = -1;
  pde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );
  BOOST_CHECK( pde.isValidState(q) == false );

  rho = 1;
  t = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );
  BOOST_CHECK( pde.isValidState(q) == false );

  rho = 0;
  t = 1;
  pde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );
  BOOST_CHECK( pde.isValidState(q) == false );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( interpResidBC )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde1(gas, Euler_ResidInterp_Raw);
  Real tol = 1.0e-12;

  ArrayQ q;
  Real rho = 1.137, u = 0.784, v = -0.231, w = 0.456, t = 0.987;

  pde1.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );

  BOOST_CHECK_EQUAL(pde1.category(), 0);

  BOOST_CHECK_EQUAL(pde1.nMonitor(), 5);
  BOOST_CHECK( pde1.isValidState(q) == true );
  DLA::VectorD<Real> vecout1(5);
  pde1.interpResidVariable(q, vecout1);
  for (int i = 0; i < PDEClass::N; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );
  vecout1 = 0;
  pde1.interpResidGradient(q, vecout1);
  for (int i = 0; i < PDEClass::N; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );
  vecout1 = 0;
  pde1.interpResidBC(q, vecout1);
  for (int i = 0; i < PDEClass::N; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );

  PDEClass pde2(gas, Euler_ResidInterp_Momentum);
  BOOST_CHECK_EQUAL(pde2.nMonitor(), 3);
  DLA::VectorD<Real> vecout2(3);
  pde2.interpResidVariable(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)+q(2)*q(2)+q(3)*q(3)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(4),vecout2(2), tol, tol);

  vecout2 = 0;
  pde2.interpResidGradient(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)+q(2)*q(2)+q(3)*q(3)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(4),vecout2(2), tol, tol);

  vecout2 = 0;
  pde2.interpResidBC(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)+q(2)*q(2)+q(3)*q(3)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(4),vecout2(2), tol, tol);

  PDEClass pde3(gas, Euler_ResidInterp_Entropy);
  BOOST_CHECK_EQUAL(pde3.nMonitor(), 1);
//  DLA::VectorD<Real> vecout3(1);
//  pde2.interpResidVariable(q, vecout3);
//
//  vecout2 = 0;
//  pde2.interpResidGradient(q, vecout3);
//
//  vecout2 = 0;
//  pde2.interpResidBC(q, vecout3);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/PDEEuler3D_pattern.txt", true );

  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  pde.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
