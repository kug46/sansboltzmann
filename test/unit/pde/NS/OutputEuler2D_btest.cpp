// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// testing routines for conversion of solution variables (Q) to
//     output of interest (Output): 2-D Navier-Stokes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NS/TraitsNavierStokes.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( OutputEuler2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Force_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef OutputEuler2D_Force<PDEClass> OutputWeightsType;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
//  const Real tSutherland = 110;     // K
//  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real tol = 1.e-13;

  OutputWeightsType outWeights(pde, 3.0, 2.0);

  ArrayQ Fn = {0.1, 0.2, 0.3, 0.4};
  ArrayQ q = {0.5, 0.6, 0.7, 0.8};
  ArrayQ qx = {-0.37, 0.92, 0.53, -0.21};
  ArrayQ qy = { 0.84, 0.45, 0.04,  0.93};

  Real x = 0.2, y = 0.4;
  Real time = 1.0;

  ArrayQ weights, weightsTrue;
  outWeights(x, y, time, weights);

  weightsTrue = {0, 3, 2, 0};

  SANS_CHECK_CLOSE( weightsTrue[0], weights[0], tol, tol );
  SANS_CHECK_CLOSE( weightsTrue[1], weights[1], tol, tol );
  SANS_CHECK_CLOSE( weightsTrue[2], weights[2], tol, tol );
  SANS_CHECK_CLOSE( weightsTrue[3], weights[3], tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( TotalEnthalpyErrorSquare_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef OutputEuler2D_totalEnthalpyErrorSquare<PDEClass> OutputType;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
//  const Real tSutherland = 110;     // K
//  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real tol = 1.e-13;

  Real rhoExact = 1.0;
  Real uExact = 1.4;
  Real vExact = 5.0;
  Real tExact = 750.0;
  Real hExact = gas.enthalpy(rhoExact, tExact);

  Real totalEnthalpyExact = (hExact + 0.5*(uExact*uExact + vExact*vExact));

  Real rho = 32.0;
  Real u = 21.0;
  Real v = 65.0;
  Real t = 325.0;
  ArrayQ q = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  Real h = gas.enthalpy(rho, t);
  Real totalEnthalpy = (h + 0.5*(u*u + v*v));

  ArrayQ qx = {-0.37, 0.92, 0.53, -0.21};
  ArrayQ qy = { 0.84, 0.45, 0.04,  0.93};

  OutputType error(pde, totalEnthalpyExact);

  Real x = 0.2, y = 0.4;
  Real time = 1.0;

  Real output;
  error(x, y, time,  q, qx, qy, output);

  Real outputTrue = pow((totalEnthalpyExact - totalEnthalpy), 2);

  SANS_CHECK_CLOSE( outputTrue, output, tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( TotalPressure_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef OutputEuler2D_TotalPressure<PDEClass> OutputType;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
//  const Real tSutherland = 110;     // K
//  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real tol = 1.e-13;

  Real rhoExact = 1.0;
  Real uExact = 1.4;
  Real vExact = 5.0;
  Real tExact = 750.0;
  Real pExact = gas.pressure(rhoExact, tExact);
  Real cExact = gas.speedofSound(tExact);
  Real M2Exact = (uExact*uExact + vExact*vExact)/(cExact*cExact);

  Real p0Exact = pExact*pow(1 + (gamma-1)/2*M2Exact, gamma/(gamma-1));

  Real rho = 32.0;
  Real u = 21.0;
  Real v = 65.0;
  Real t = 325.0;
  ArrayQ q = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  Real p = gas.pressure(rho, t);
  Real c = gas.speedofSound(t);
  Real M2 = (u*u + v*v)/(c*c);
  Real p0 = p*pow(1 + (gamma-1)/2*M2, gamma/(gamma-1));

  ArrayQ qx = {-0.37, 0.92, 0.53, -0.21};
  ArrayQ qy = { 0.84, 0.45, 0.04,  0.93};

  {
    OutputType OutputFcn(pde);

    Real x = 0.2, y = 0.4;
    Real time = 1.0;

    Real output;
    OutputFcn(x, y, time, q, qx, qy, output);

    Real outputTrue = p0;

    SANS_CHECK_CLOSE( outputTrue, output, tol, tol );
  }

  {
    OutputType OutputFcn(pde, 0, 0, 0, p0Exact);

    Real x = 0.2, y = 0.4;
    Real time = 1.0;

    Real output;
    OutputFcn(x, y, time, q, qx, qy, output);

    Real outputTrue = p0 - p0Exact;

    SANS_CHECK_CLOSE( outputTrue, output, tol, tol );
  }

  {
    Real s = 0.75;
    Real x0 = 0.01;
    Real y0 = 0.2;
    Real s2 = s*s;
    OutputType OutputFcn(pde, s, x0, y0, p0Exact);

    Real x = 0.2, y = 0.4;
    Real time = 1.0;

    // Area integral
    Real Gauss = exp( -0.5*(pow(x-x0,2) + pow(y-y0,2))/s2 )/( 2*s2*PI);

    Real output;
    OutputFcn(x, y, time, q, qx, qy, output);

    Real outputTrue = (p0 - p0Exact)*Gauss;

    SANS_CHECK_CLOSE( outputTrue, output, tol, tol );
  }

  {
    Real s = 0.75;
    Real x0 = 0.01;
    Real y0 = 0.2;
    Real s2 = s*s;
    OutputType OutputFcn(pde, s, x0, y0, p0Exact);

    Real x = 0.2, y = 0.4, nx = 1, ny = 2;
    Real time = 1.0;

    // Line integral
    Real Gauss = exp( -0.5*(pow(x-x0,2) + pow(y-y0,2))/s2 )/sqrt( 2*s2*PI);

    Real output;
    OutputFcn(x, y, time, nx, ny, q, qx, qy, output);

    Real outputTrue = (p0 - p0Exact)*Gauss;

    SANS_CHECK_CLOSE( outputTrue, output, tol, tol );
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
