// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCRANSSA2D_btest
//
// test of 2-D compressible RANS SA BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/QRANSSA2D.h"
#include "pde/NS/BCRANSSA2D.h"
#include "pde/NS/TraitsRANSSA.h"
#include "pde/BCParameters.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"


//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
typedef TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> TraitsModel;

template class BCRANSSA2D< BCTypeWall_mitState,
                           BCNavierStokes2D< BCTypeWallNoSlipAdiabatic_mitState, PDERANSSA2D<TraitsSizeRANSSA, TraitsModel> > >;

template class BCRANSSA2D<BCTypeInflowSubsonic_sHqt_BN,
                          BCEuler2D< BCTypeInflowSubsonic_sHqt_BN, PDERANSSA2D<TraitsSizeRANSSA, TraitsModel> > >;


template class BCRANSSA2D< BCTypeFullState_mitState,
                           BCEuler2D< BCTypeFullState_mitState, PDERANSSA2D<TraitsSizeRANSSA, TraitsModel> > >;

template class BCRANSSA2D<BCTypeNeumann_mitState,
                          BCEuler2D< BCTypeOutflowSubsonic_Pressure_mitState, PDERANSSA2D<TraitsSizeRANSSA, TraitsModel> > >;

template class BCRANSSA2D<BCTypeFunctionInflow_sHqt_BN,
                          BCEuler2D< BCTypeFunctionInflow_sHqt_BN, PDERANSSA2D<TraitsSizeRANSSA, TraitsModel> > >;

// Instantiate BCParamaters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters< BCRANSSA2DVector< TraitsSizeRANSSA, TraitsModel > >;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BCRANSSA2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  {
  typedef BCRANSSA2D<BCTypeWall_mitState,
                     BCNavierStokes2D< BCTypeWallNoSlipAdiabatic_mitState, PDERANSSA2D<TraitsSizeRANSSA, TraitsModel> > > BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 5 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 5 );
  BOOST_CHECK( MatrixQ::M == 5 );
  BOOST_CHECK( MatrixQ::N == 5 );
  }

  {
  typedef BCRANSSA2D<BCTypeDirichlet_mitState,
                     BCNavierStokes2D< BCTypeWallNoSlipAdiabatic_mitState, PDERANSSA2D<TraitsSizeRANSSA, TraitsModel> > > BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 5 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 5 );
  BOOST_CHECK( MatrixQ::M == 5 );
  BOOST_CHECK( MatrixQ::N == 5 );
  }

  {
  typedef BCRANSSA2D< BCTypeFullState_mitState,
                      BCEuler2D< BCTypeFullState_mitState, PDERANSSA2D<TraitsSizeRANSSA, TraitsModel> > > BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 5 );
  BOOST_CHECK( BCClass::NBC == 5 );
  BOOST_CHECK( ArrayQ::M == 5 );
  BOOST_CHECK( MatrixQ::M == 5 );
  BOOST_CHECK( MatrixQ::N == 5 );
  }

  {
  typedef BCRANSSA2D< BCTypeInflowSubsonic_sHqt_BN,
                      BCEuler2D< BCTypeInflowSubsonic_sHqt_BN, PDERANSSA2D<TraitsSizeRANSSA, TraitsModel> > > BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 5 );
  BOOST_CHECK( BCClass::NBC == 4 );
  BOOST_CHECK( ArrayQ::M == 5 );
  BOOST_CHECK( MatrixQ::M == 5 );
  BOOST_CHECK( MatrixQ::N == 5 );
  }

  {
  typedef BCRANSSA2D< BCTypeFunctionInflow_sHqt_BN,
                      BCEuler2D< BCTypeFunctionInflow_sHqt_BN, PDERANSSA2D<TraitsSizeRANSSA, TraitsModel> > > BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;
  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 5 );
  BOOST_CHECK( BCClass::NBC == 4 );
  BOOST_CHECK( ArrayQ::M == 5 );
  BOOST_CHECK( MatrixQ::M == 5 );
  BOOST_CHECK( MatrixQ::N == 5 );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor )
{
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModel> PDEClass;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  typedef BCParameters< BCRANSSA2DVector<TraitsSizeRANSSA, TraitsModel> > BCParams;

  {
  typedef BCRANSSA2D<BCTypeWall_mitState, BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass>> BCClass;

  BCClass bc1(pde);

  //Pydict constructor
  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  PyDict BCList;
  BCList["BC1"] = BCNoSlip;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCNoSlip);
  }

  {
  typedef BCRANSSA2D<BCTypeNeumann_mitState, BCNavierStokes2D<BCTypeSymmetry_mitState, PDEClass>> BCClass;

  BCClass bc1(pde);

  //Pydict constructor
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCList;
  BCList["BC1"] = BCSymmetry;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCSymmetry);
  }

  {
  typedef BCRANSSA2D<BCTypeWall_mitState, BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState, PDEClass> > BCClass;

  const Real pSpec = 2.1;
  BCClass bc(pde, pSpec);

  PyDict BCOutflowSubsonic;
  BCOutflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOutflowSubsonic[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pSpec;

  PyDict BCList;
  BCList["BC1"] = BCOutflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCOutflowSubsonic);
  }

  {
  typedef BCRANSSA2D< BCTypeFullState_mitState,
                      BCEuler2D< BCTypeFullState_mitState, PDERANSSA2D<TraitsSizeRANSSA, TraitsModel> > > BCClass;

  //bool upwind = true;
  //BCClass bc(pde, SAnt2D<DensityVelocityPressure2D<Real>>({1.1, 1.3, -0.4, 5.45, 4.6}), upwind);

  typedef SAnt2D<DensityVelocityPressure2D<Real>>::ParamsType ParamsType;

  PyDict d;
  d[NSVariableType2DParams::params.StateVector.Variables] = SAVariableType2DParams::params.StateVector.PrimitivePressure;
  d[ParamsType::params.rho] = 1.1;
  d[ParamsType::params.u] = 1.3;
  d[ParamsType::params.v] = -0.4;
  d[ParamsType::params.p] = 5.45;
  d[ParamsType::params.nt] = 4.6;

  PyDict BCFullState;
  BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFullState[BCClass::ParamsType::params.Characteristic] = false;
  BCFullState[BCClass::ParamsType::params.StateVector] = d;

  PyDict BCList;
  BCList["BC1"] = BCFullState;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCFullState);
  }

  {
  typedef BCRANSSA2D<BCTypeDirichlet_mitState, BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDEClass> > BCClass;

  typedef BCClass::ParamsType ParamsType;

  Real TtSpec = 20.5;
  Real PtSpec = 71.4;
  Real aSpec = 2.1;
  Real nt = 1.1;
  BCClass bc1(pde, nt, TtSpec, PtSpec, aSpec);

  PyDict BCInflowSubsonic;
  BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  BCInflowSubsonic[ParamsType::params.TtSpec] = TtSpec;
  BCInflowSubsonic[ParamsType::params.PtSpec] = PtSpec;
  BCInflowSubsonic[ParamsType::params.aSpec] = aSpec;
  BCInflowSubsonic[ParamsType::params.nt] = nt;

  PyDict BCList;
  BCList["BC1"] = BCInflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCInflowSubsonic);
  }

  {
  typedef BCRANSSA2D<BCTypeInflowSubsonic_sHqt_BN, BCEuler2D<BCTypeInflowSubsonic_sHqt_BN, PDEClass> > BCClass;

  typedef BCClass::ParamsType ParamsType;

  Real sSpec = 2.2;
  Real HSpec = 1.0;
  Real aSpec = 0.1;
  Real nt = 1.1;
  BCClass bc1(pde, nt, sSpec, HSpec, aSpec);

  PyDict BCInflowSubsonic;
  BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  BCInflowSubsonic[ParamsType::params.sSpec] = sSpec;
  BCInflowSubsonic[ParamsType::params.HSpec] = HSpec;
  BCInflowSubsonic[ParamsType::params.aSpec] = aSpec;
  BCInflowSubsonic[ParamsType::params.nt] = nt;

  PyDict BCList;
  BCList["BC1"] = BCInflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCInflowSubsonic);
  }

  {
  typedef BCRANSSA2D<BCTypeFunctionInflow_sHqt_BN, BCEuler2D<BCTypeFunctionInflow_sHqt_BN, PDEClass> > BCClass;
  typedef TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel>
    TraitsModelRANSSAClass;
  typedef SolutionFunction_RANSSA2D_Const<TraitsSizeRANSSA, TraitsModelRANSSAClass> SolutionFunction_RANSSAClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;

  PyDict Function;
  Function[BCClass::ParamsType::params.Function.Name] = BCClass::ParamsType::params.Function.Const;
  Function[SolutionFunction_RANSSAClass::ParamsType::params.gasModel] = gasModelDict;
  Function[SolutionFunction_RANSSAClass::ParamsType::params.rho] = 1.0;
  Function[SolutionFunction_RANSSAClass::ParamsType::params.u] = 0.4;
  Function[SolutionFunction_RANSSAClass::ParamsType::params.v] = 0.1;
  Function[SolutionFunction_RANSSAClass::ParamsType::params.p] = 1.0;
  Function[SolutionFunction_RANSSAClass::ParamsType::params.nt] = 3;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.SolutionInflow_sHqt_BN;
  BCIn[BCClass::ParamsType::params.Function] = Function;

  BCClass bc2(pde, BCIn);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_wallnoslip )
{
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModel> PDEClass;
  typedef BCParameters< BCRANSSA2DVector<TraitsSizeRANSSA, TraitsModel> > BCParams;

  typedef BCRANSSA2D<BCTypeWall_mitState, BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass>> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);

  //Pydict constructor
  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;
  BCClass bc(pde, BCNoSlip);

  Real x, y, time;
  Real nx, ny, dist = 0;
  Real rho, u, v, t, p, nt;

  // Test conditions - M = 0.3, T0 = 300.1, P0 = 101325; alpha = 0.1 rad
  x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137;
  t = 300.1;
  p  = R*rho*t;
  nt = 1.3;

  Real M = 0.3;
  Real c =  sqrt(gamma*R*t);
  Real alpha = 0.1;
  u = M*c*cos(alpha);
  v = M*c*sin(alpha);

  ArrayQ q = pde.setDOFFrom( SAnt2D<DensityVelocityPressure2D<Real>>({rho, u, v, p, nt}) );

  ArrayQ qB;
  bc.state(dist, x, y, time, nx, ny, q, qB);

  Real rhoB = rho;
  Real uB = 0;
  Real vB = 0;
  Real pB = p;
  Real ntB = 0;

  ArrayQ qTrue = pde.setDOFFrom( SAnt2D<DensityVelocityPressure2D<Real>>({rhoB, uB, vB, pB, ntB}) );

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;
  SANS_CHECK_CLOSE(    qTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE(    qTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE(    qTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE(    qTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE(    qTrue(4), qB(4), small_tol, close_tol );

  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  Real ntx = -0.23; Real nty = 1.3;
  ArrayQ qx = {rhox, ux, vx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, py, nty};

  ArrayQ fx = 0;
  ArrayQ fy = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, time, qTrue, fx, fy);
  FnTrue += fx*nx + fy*ny;

  // Compute the viscous flux
  ArrayQ fv = 0, gv = 0;
  pde.fluxViscous(x, y, time, qTrue, qx, qy, fv, gv);
  ArrayQ Fvn = fv*nx + gv*ny;

  // Zero out the energy equation for the adiabatic wall
  Fvn[pde.iEngy] = 0;

  // Set the Truth value
  FnTrue += Fvn;

  // Compute the flux from the BC
  ArrayQ Fn = 0;
  bc.fluxNormal( dist, x, y, time, nx, ny, q, qx, qy, qB, Fn);

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_symmetry )
{
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModel> PDEClass;
  typedef BCParameters< BCRANSSA2DVector<TraitsSizeRANSSA, TraitsModel> > BCParams;

  typedef BCRANSSA2D<BCTypeNeumann_mitState, BCNavierStokes2D<BCTypeSymmetry_mitState, PDEClass>> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);

  //Pydict constructor
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  BCClass bc(pde, BCSymmetry);

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p, nt, dist = 0;

  // Test conditions - M = 0.3, T0 = 300.1, P0 = 101325; alpha = 0.1 rad
  x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137;
  t = 300.1;
  p  = R*rho*t;
  nt = 1.3;

  Real M = 0.3;
  Real c =  sqrt(gamma*R*t);
  Real alpha = 0.1;
  u = M*c*cos(alpha);
  v = M*c*sin(alpha);

  ArrayQ q = pde.setDOFFrom( SAnt2D<DensityVelocityPressure2D<Real>>({rho, u, v, p, nt}) );

  Real rhoB = rho;

  //VB = V - n*dot(V,n)
  Real uB = u - nx*(u*nx + v*ny);
  Real vB = v - ny*(u*nx + v*ny);

  Real pB = p;

  Real ntB = nt;

  ArrayQ qTrue = pde.setDOFFrom( SAnt2D<DensityVelocityPressure2D<Real>>({rhoB, uB, vB, pB, ntB}) );

  ArrayQ qB;
  bc.state(dist, x, y, time, nx, ny, q, qB);

  const Real small_tol = 1.0e-9;
  const Real close_tol = 1.0e-12;
  SANS_CHECK_CLOSE( qTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(4), qB(4), small_tol, close_tol );


  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  Real ntx = -0.23; Real nty = 1.3;
  ArrayQ qx = {rhox, ux, vx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, py, nty};

  ArrayQ Fn = 0;
  bc.fluxNormal(dist, x, y, time, nx, ny, q, qx, qy, qB, Fn);

  ArrayQ fx = 0;
  ArrayQ fy = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, time, qTrue, fx, fy);
  FnTrue += fx*nx + fy*ny;

  // Compute the viscous flux
  ArrayQ fv = 0, gv = 0;
  pde.fluxViscous(x, y, time, qB, qx, qy, fv, gv);

  // Compute normal x-momentum and y-momentum equations
  Real xMomN = fv[pde.ixMom]*nx + gv[pde.ixMom]*ny;
  Real yMomN = fv[pde.iyMom]*nx + gv[pde.iyMom]*ny;

  FnTrue[pde.ixMom] += (xMomN*nx + yMomN*ny)*nx;
  FnTrue[pde.iyMom] += (xMomN*nx + yMomN*ny)*ny;

  // No viscous constribution to SA intentionally

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_fullstate )
{
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModel> PDEClass;

  typedef BCRANSSA2D< BCTypeFullState_mitState,
                      BCEuler2D< BCTypeFullState_mitState, PDERANSSA2D<TraitsSizeRANSSA, TraitsModel> > > BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCParameters< BCRANSSA2DVector<TraitsSizeRANSSA, TraitsModel> > BCParams;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real dist, x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p, nt;

  dist = x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  p  = R*rho*t;
  nt = 1.3;

  typedef SAnt2D<DensityVelocityPressure2D<Real>>::ParamsType ParamsType;

  PyDict d;
  d[NSVariableType2DParams::params.StateVector.Variables] = SAVariableType2DParams::params.StateVector.PrimitivePressure;
  d[ParamsType::params.rho] = rho;
  d[ParamsType::params.u] = u;
  d[ParamsType::params.v] = v;
  d[ParamsType::params.p] = p;
  d[ParamsType::params.nt] = nt;

  PyDict BCFullState;
  BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFullState[BCClass::ParamsType::params.StateVector] = d;

  ArrayQ q = pde.setDOFFrom( BCFullState );

  BCFullState[BCClass::ParamsType::params.Characteristic] = false;
  BCClass bc(pde, BCFullState);

  BCFullState[BCClass::ParamsType::params.Characteristic] = true;
  BCClass bc2(pde, BCFullState);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 5 );
  BOOST_CHECK( bc.NBC == 5 );

  ArrayQ qI = 0;
  ArrayQ qB;

  bc.state(dist, x, y, time, nx, ny, qI, qB);

  Real rhoB, uB, vB, tB, ntB;
  pde.variableInterpreter().eval(qB, rhoB, uB, vB, tB);
  pde.variableInterpreter().evalSA(qB, ntB);

  SANS_CHECK_CLOSE(rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(u  , uB  , small_tol, close_tol );
  SANS_CHECK_CLOSE(v  , vB  , small_tol, close_tol );
  SANS_CHECK_CLOSE(t  , tB  , small_tol, close_tol );
  SANS_CHECK_CLOSE(nt , ntB , small_tol, close_tol );

  qI = pde.setDOFFrom( SAnt2D<DensityVelocityTemperature2D<Real>>({rhoB, uB, vB, tB, nt}) );
  BOOST_CHECK_EQUAL( true, bc.isValidState(nx, ny, qI) );
  BOOST_CHECK_EQUAL( true, bc2.isValidState(nx, ny, qI) );

  qI = pde.setDOFFrom( SAnt2D<DensityVelocityTemperature2D<Real>>({rhoB, -uB, -vB, tB, nt}) );
  BOOST_CHECK_EQUAL( false, bc.isValidState(nx, ny, qI) );
  BOOST_CHECK_EQUAL( true, bc2.isValidState(nx, ny, qI) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_outflow_pressure )
{
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModel> PDEClass;
  typedef BCParameters< BCRANSSA2DVector<TraitsSizeRANSSA, TraitsModel> > BCParams;

  typedef BCRANSSA2D<BCTypeDirichlet_mitState, BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState, PDEClass>> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::ParamsType ParamsType;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);

  const Real small_tol = 3.e-12;
  const Real close_tol = 3.e-12;

  //Pydict constructor
  PyDict BCSubsonicOut;
  BCSubsonicOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;

  Real ntB = 2.1;

  BCSubsonicOut[ParamsType::params.pSpec] = 101325.0;
  BCSubsonicOut[ParamsType::params.nt] = ntB;

  BCClass bc(pde, BCSubsonicOut);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 5 );
  BOOST_CHECK( bc.NBC == 4 );

  Real x, y, time;
  Real nx, ny, dist = 0;
  Real rho, u, v, t, p, nt;

  // Test conditions - M = 0.3, T0 = 300.1, P0 = 101325; alpha = 0.1 rad
  x = y = time = 0;   // not actually used in functions
  nx = 0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137;
  t = 300.1;
  p  = R*rho*t;
  nt = 1.3;

  Real M = 0.3;
  Real c =  sqrt(gamma*R*t);
  Real alpha = 0.1;
  u = M*c*cos(alpha);
  v = M*c*sin(alpha);

  ArrayQ q = pde.setDOFFrom( SAnt2D<DensityVelocityPressure2D<Real>>({rho, u, v, p, nt}) );

  ArrayQ qB, qBTrue;

  bc.state(dist, x, y, time, nx, ny, q, qB);

  qBTrue[0] = 1.164915860299754;
  qBTrue[1] = 95.7574160100521;
  qBTrue[2] = 7.428571205323625;
  qBTrue[3] = 101325.0;
  qBTrue[4] = ntB;

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(4), qB(4), small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_PtTt )
{
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModel> PDEClass;
  typedef BCParameters< BCRANSSA2DVector<TraitsSizeRANSSA, TraitsModel> > BCParams;

  typedef BCRANSSA2D<BCTypeDirichlet_mitState, BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDEClass>> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::ParamsType ParamsType;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R     = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);

  Real x, y, time;
  Real nx, ny, dist = 0;
  Real rho, u, v, t, p, nt;

  x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  p  = R*rho*t; // Hard coded R = 0.4
  nt = 1.3;

  ArrayQ q = pde.setDOFFrom( SAnt2D<DensityVelocityPressure2D<Real>>({rho, u, v, p, nt}) );

  Real TtSpec = t*(1+0.2*0.1*0.1);
  Real PtSpec = p*pow( (1+0.2*0.1*0.1), 3.5 );
  Real aSpec = 0.1;
  Real ntSpec = 2.1;

  //Pydict constructor
  PyDict PtTtIn;
  PtTtIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  PtTtIn[ParamsType::params.TtSpec] = TtSpec;
  PtTtIn[ParamsType::params.PtSpec] = PtSpec;
  PtTtIn[ParamsType::params.aSpec] = aSpec;
  PtTtIn[ParamsType::params.nt] = ntSpec;

  BCClass bc(pde, PtTtIn);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 5 );
  BOOST_CHECK( bc.NBC == 4 );

  ArrayQ qB = 0;
  bc.state(dist, x, y, time, nx, ny, q, qB);

  Real rhoB, uB, vB, tB, ntB;
  pde.variableInterpreter().eval(qB, rhoB, uB, vB, tB);
  pde.variableInterpreter().evalSA(qB, ntB);

  Real rhoTrue = 0.789121117837660;
  Real uTrue = 0.614296756252870;
  Real vTrue = 0.061635263601788;
  Real tTrue = 0.852845639120693;
  Real ntTrue = ntSpec;

  SANS_CHECK_CLOSE(rhoTrue, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(uTrue, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(vTrue, vB, small_tol, close_tol );
  SANS_CHECK_CLOSE(tTrue, tB, small_tol, close_tol );
  SANS_CHECK_CLOSE(ntTrue, ntB, small_tol, close_tol );


  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  Real ntx = -0.23; Real nty = 1.3;
  ArrayQ qx = {rhox, ux, vx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, py, nty};

  ArrayQ Fn = 0;
  bc.fluxNormal( dist, x, y, time, nx, ny, q, qx, qy, qB, Fn);

  ArrayQ fx = 0;
  ArrayQ fy = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, time, qB, fx, fy);
  FnTrue += fx*nx + fy*ny;

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
}



//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_sHqt_BN )
{
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModel> PDEClass;
  typedef BCParameters< BCRANSSA2DVector<TraitsSizeRANSSA, TraitsModel> > BCParams;

  typedef BCRANSSA2D<BCTypeInflowSubsonic_sHqt_BN, BCEuler2D<BCTypeInflowSubsonic_sHqt_BN, PDEClass>> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::ParamsType ParamsType;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R     = 1.0;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);

  Real x, y, time;
  Real nx, ny, dist = 0;
  Real rho, u, v, t, p, nt;

  x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  p  = R*rho*t;
  nt = 1.3;

  ArrayQ q = pde.setDOFFrom( SAnt2D<DensityVelocityPressure2D<Real>>({rho, u, v, p, nt}) );

//  Real TtSpec = t*(1+0.2*0.1*0.1);
//  Real PtSpec = p*pow( (1+0.2*0.1*0.1), 3.5 );
  Real rhoSpec =rho*0.973;
  Real uSpec = u*1.01;
  Real vSpec = v*1.04;
  Real tSpec = t*1.06;
  Real pSpec = gas.pressure(rhoSpec,tSpec);

  Real sSpec = log( pSpec / pow(rhoSpec, gamma) );
  Real HSpec = (gas.enthalpy(rhoSpec,tSpec) + 0.5*(uSpec*uSpec+vSpec*vSpec));
  Real aSpec = atan(vSpec / uSpec);
  Real ntSpec = 2.1;

  // REGULAR INFLOW
  //Pydict constructor
  PyDict sHqt;
  sHqt[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  sHqt[ParamsType::params.sSpec] = sSpec;
  sHqt[ParamsType::params.HSpec] = HSpec;
  sHqt[ParamsType::params.aSpec] = aSpec;
  sHqt[ParamsType::params.nt] = ntSpec;

  BCClass bc(pde, sHqt);

  // SOLUTION INFLOW
  typedef BCRANSSA2D<BCTypeFunctionInflow_sHqt_BN, BCEuler2D<BCTypeFunctionInflow_sHqt_BN, PDEClass> > BCClass2;
  typedef TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel>
    TraitsModelRANSSAClass;
  typedef SolutionFunction_RANSSA2D_Const<TraitsSizeRANSSA, TraitsModelRANSSAClass> SolutionFunction_RANSSAClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;

  PyDict Function;
  Function[BCClass2::ParamsType::params.Function.Name] = BCClass2::ParamsType::params.Function.Const;
  Function[SolutionFunction_RANSSAClass::ParamsType::params.gasModel] = gasModelDict;
  Function[SolutionFunction_RANSSAClass::ParamsType::params.rho] = rhoSpec;
  Function[SolutionFunction_RANSSAClass::ParamsType::params.u] = uSpec;
  Function[SolutionFunction_RANSSAClass::ParamsType::params.v] = vSpec;
  Function[SolutionFunction_RANSSAClass::ParamsType::params.p] = pSpec;
  Function[SolutionFunction_RANSSAClass::ParamsType::params.p] = pSpec;
  Function[SolutionFunction_RANSSAClass::ParamsType::params.nt] = ntSpec;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.SolutionInflow_sHqt_BN;
  BCIn[BCClass2::ParamsType::params.Function] = Function;

  BCClass2 bc2(pde, BCIn);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 5 );
  BOOST_CHECK( bc.NBC == 4 );
  BOOST_CHECK( bc2.D == 2 );
  BOOST_CHECK( bc2.N == 5 );
  BOOST_CHECK( bc2.NBC == 4 );

  ArrayQ qB = 0;
  bc.state(dist, x, y, time, nx, ny, q, qB);

  Real rhoB, uB, vB, tB, ntB;
  pde.variableInterpreter().eval(qB, rhoB, uB, vB, tB);
  pde.variableInterpreter().evalSA(qB, ntB);

  Real rhoTrue = rho;
  Real uTrue = u;
  Real vTrue = v;
  Real tTrue = t;
  Real ntTrue = nt;

  SANS_CHECK_CLOSE(rhoTrue, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(uTrue, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(vTrue, vB, small_tol, close_tol );
  SANS_CHECK_CLOSE(tTrue, tB, small_tol, close_tol );
  SANS_CHECK_CLOSE(ntTrue, ntB, small_tol, close_tol );

  ArrayQ qB2 = 0;
  bc2.state(dist, x, y, time, nx, ny, q, qB2);
  Real rhoB2, uB2, vB2, tB2, ntB2;
  pde.variableInterpreter().eval(qB2, rhoB2, uB2, vB2, tB2);
  pde.variableInterpreter().evalSA(qB2, ntB2);

  SANS_CHECK_CLOSE(rhoTrue, rhoB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(uTrue, uB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(vTrue, vB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(tTrue, tB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(ntTrue, ntB2, small_tol, close_tol );

  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  Real ntx = -0.23; Real nty = 1.3;
  ArrayQ qx = {rhox, ux, vx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, py, nty};

  ArrayQ Fn = 0;
  bc.fluxNormal( dist, x, y, time, nx, ny, q, qx, qy, qB, Fn);

  ArrayQ Fn2 = 0;
  bc2.fluxNormal( dist, x, y, time, nx, ny, q, qx, qy, qB2, Fn2);

  ArrayQ fx = 0;
  ArrayQ fy = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, time, q, fx, fy);
  FnTrue += fx*nx + fy*ny;

  // tangential vector
  const Real tx = -ny;
  const Real ty =  nx;

  Real gmi = gamma - 1.;
  Real H = gas.enthalpy(rho, t) + 0.5*(u*u+v*v);
  Real c = gas.speedofSound(t);
  Real s = log( p /pow(rho,gamma) );

  // normal and tangential velocities
  Real vn = u*nx + v*ny;
  Real vt = u*tx + v*ty;
  Real V2 = u*u+v*v;
  Real V = sqrt(V2);

  // Strong BC's that are imposed
  Real ds  = s  - sSpec;
  Real dH  = H  - HSpec;
  Real dvt = v/sqrt(u*u+v*v) - sin(aSpec);
  Real dnt = nt - ntSpec;

  FnTrue[4] -= -vn*nt*rho/gmi*ds;
  FnTrue[4] -= vn*gamma*nt*rho/(c*c+V2*gmi)*dH;
  FnTrue[4] -=  -(vt*V2*V*rho*gamma*nt)/(u*(c*c+V2*gmi))*dvt;
  FnTrue[4] -=  rho*vn*dnt;

  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn2(4), small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/BCRANSSA2D_pattern.txt", true );

  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModel> PDEClass;
  typedef BCParameters< BCRANSSA2DVector<TraitsSizeRANSSA, TraitsModel> > BCParams;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  {
    typedef BCRANSSA2D<BCTypeWall_mitState, BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass>> BCClass;

    BCClass bc(pde);

    bc.dump( 2, output );
  }

  {
    typedef BCRANSSA2D<BCTypeNeumann_mitState, BCNavierStokes2D<BCTypeSymmetry_mitState, PDEClass>> BCClass;

    BCClass bc(pde);

    bc.dump( 2, output );
  }

  {
    typedef BCRANSSA2D< BCTypeFullState_mitState,
                        BCEuler2D< BCTypeFullState_mitState, PDERANSSA2D<TraitsSizeRANSSA, TraitsModel> > > BCClass;
    typedef SAnt2D<DensityVelocityPressure2D<Real>>::ParamsType ParamsType;

    PyDict d;
    d[NSVariableType2DParams::params.StateVector.Variables] = SAVariableType2DParams::params.StateVector.PrimitivePressure;
    d[ParamsType::params.rho] = 1.1;
    d[ParamsType::params.u] = 1.3;
    d[ParamsType::params.v] = -0.4;
    d[ParamsType::params.p] = 5.45;
    d[ParamsType::params.nt] = 4.6;

    PyDict BCFullState;
    BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
    BCFullState[BCClass::ParamsType::params.Characteristic] = false;
    BCFullState[BCClass::ParamsType::params.StateVector] = d;

    BCClass bc(pde, BCFullState);

    bc.dump( 2, output );
  }

  {
    typedef BCRANSSA2D<BCTypeDirichlet_mitState, BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDEClass> > BCClass;

    Real TtSpec = 20.5;
    Real PtSpec = 71.4;
    Real aSpec = 2.1;
    Real nt = 1.1;
    BCClass bc(pde, nt, TtSpec, PtSpec, aSpec);

    bc.dump( 2, output );
  }

  {
    typedef BCRANSSA2D<BCTypeInflowSubsonic_sHqt_BN, BCEuler2D<BCTypeInflowSubsonic_sHqt_BN, PDEClass> > BCClass;

    Real sSpec = 2.2;
    Real HSpec = 1.0;
    Real aSpec = 0.1;
    Real nt = 1.1;
    BCClass bc(pde, nt, sSpec, HSpec, aSpec);

    bc.dump( 2, output );
  }

  {
    typedef BCRANSSA2D<BCTypeFunctionInflow_sHqt_BN, BCEuler2D<BCTypeFunctionInflow_sHqt_BN, PDEClass> > BCClass;
    typedef TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel>
    TraitsModelRANSSAClass;
    typedef SolutionFunction_RANSSA2D_Const<TraitsSizeRANSSA, TraitsModelRANSSAClass> SolutionFunction_RANSSAClass;

    PyDict gasModelDict;
    gasModelDict[GasModelParams::params.gamma] = 1.4;
    gasModelDict[GasModelParams::params.R] = 0.4;

    PyDict Function;
    Function[BCClass::ParamsType::params.Function.Name] = BCClass::ParamsType::params.Function.Const;
    Function[SolutionFunction_RANSSAClass::ParamsType::params.gasModel] = gasModelDict;
    Function[SolutionFunction_RANSSAClass::ParamsType::params.rho] = 1.0;
    Function[SolutionFunction_RANSSAClass::ParamsType::params.u] = 0.4;
    Function[SolutionFunction_RANSSAClass::ParamsType::params.v] = 0.1;
    Function[SolutionFunction_RANSSAClass::ParamsType::params.p] = 1.0;
    Function[SolutionFunction_RANSSAClass::ParamsType::params.nt] = 3;

    PyDict BCIn;
    BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.SolutionInflow_sHqt_BN;
    BCIn[BCClass::ParamsType::params.Function] = Function;

    BCClass bc(pde, BCIn);
    bc.dump( 2, output);
  }

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
