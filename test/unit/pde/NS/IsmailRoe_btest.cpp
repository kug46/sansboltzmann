// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IsmailRoe_btest
//
// test of IsmailRoe upwinding scheme

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/vector.hpp>

#include "pde/NS/IsmailRoe.h"
#include "pde/NS/TraitsEuler.h"

#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DEntropy.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

//Explicitly instantiate the class so coverage information is correct
namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
class QTypeEntropy {};

template class IsmailRoe2D<QTypePrimitiveRhoPressure, TraitsSizeEuler>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( IsmailRoe_test_suite )

typedef boost::mpl::vector< QTypePrimitiveRhoPressure
                          , QTypeConservative
                          , QTypePrimitiveSurrogate
                          , QTypeEntropy
                          > QTypes2D;

//----------------------------------------------------------------------------//
// Here we check if F(q,q) = f(q)
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( consistency_IsmailRoe2D_noFix, QType, QTypes2D )
{
  typedef IsmailRoe2D<QType, TraitsSizeEuler> IsmailRoeClass;
  typedef typename IsmailRoeClass::template ArrayQ<Real> ArrayQ;

  typedef Q2D<QType, TraitsSizeEuler> QInterpret;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  IsmailRoeClass ismailRoe(gas, eIsmailRoeEntropyNeutral);
  QInterpret qInterpret(gas);

  // Roe flux function test

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p, H;
  Real Cp;
  Cp  = R*gamma/(gamma - 1);
  Real Vel = 5.0;

  // We're going to try "all" the normal and velocity vector directions
  for (int i = 0; i < 36; i++)
  {
    Real alpha = Real(i) / 36 * 2.0 * M_PI;
    for (int j = 0; j < 36; j++)
    {
      Real beta = Real(j) / 36 * 2.0 * M_PI;

      x = y = time = 0;   // not actually used in functions
      nx = cos(beta);
      ny = sin(beta);

      rho = 1.034;
      u = Vel*sin(alpha + beta);
      v = Vel*cos(alpha + beta);
      t = 5.78;

      p = R*rho*t;
      H = Cp*t + 0.5*(u*u + v*v);

      // set
      ArrayQ qL, qR;
      qInterpret.setFromPrimitive( qL, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
      qInterpret.setFromPrimitive( qR, DensityVelocityTemperature2D<Real>(rho, u, v, t) );

      // Compute the average normal flux
      ArrayQ fnTrue = 0;
      fnTrue(0) = (nx*(rho*u      ) + ny*(rho*v      ));
      fnTrue(1) = (nx*(rho*u*u + p) + ny*(rho*v*u    ));
      fnTrue(2) = (nx*(rho*u*v    ) + ny*(rho*v*v + p));
      fnTrue(3) = (nx*(rho*u*H    ) + ny*(rho*v*H    ));

      ArrayQ fn;
      fn = 0;
      ismailRoe.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn, 1.0 );
      SANS_CHECK_CLOSE( fnTrue(0), fn(0), small_tol, close_tol );
      SANS_CHECK_CLOSE( fnTrue(1), fn(1), small_tol, close_tol );
      SANS_CHECK_CLOSE( fnTrue(2), fn(2), small_tol, close_tol );
      SANS_CHECK_CLOSE( fnTrue(3), fn(3), small_tol, close_tol );

      // Are we accumulating
      ismailRoe.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn, 1.0 );
      SANS_CHECK_CLOSE( 2.0*fnTrue(0), fn(0), small_tol, close_tol );
      SANS_CHECK_CLOSE( 2.0*fnTrue(1), fn(1), small_tol, close_tol );
      SANS_CHECK_CLOSE( 2.0*fnTrue(2), fn(2), small_tol, close_tol );
      SANS_CHECK_CLOSE( 2.0*fnTrue(3), fn(3), small_tol, close_tol );
    }
  }
}

BOOST_AUTO_TEST_CASE_TEMPLATE( consistency_IsmailRoe2D_Fix, QType, QTypes2D )
{
  typedef IsmailRoe2D<QType, TraitsSizeEuler> IsmailRoeClass;
  typedef typename IsmailRoeClass::template ArrayQ<Real> ArrayQ;

  typedef Q2D<QType, TraitsSizeEuler> QInterpret;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  IsmailRoeClass ismailRoe(gas, eIsmailRoeHartenFix);
  QInterpret qInterpret(gas);

  // Roe flux function test

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p, H;
  Real Cp;
  Cp  = R*gamma/(gamma - 1);
  Real Vel = 5.0;

  // We're going to try "all" the normal and velocity vector directions
  for (int i = 0; i < 36; i++)
  {
    Real alpha = Real(i) / 36 * 2.0 * M_PI;
    for (int j = 0; j < 36; j++)
    {
      Real beta = Real(j) / 36 * 2.0 * M_PI;

      x = y = time = 0;   // not actually used in functions
      nx = cos(beta);
      ny = sin(beta);

      rho = 1.034;
      u = Vel*sin(alpha + beta);
      v = Vel*cos(alpha + beta);
      t = 5.78;

      p = R*rho*t;
      H = Cp*t + 0.5*(u*u + v*v);

      // set
      ArrayQ qL, qR;
      qInterpret.setFromPrimitive( qL, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
      qInterpret.setFromPrimitive( qR, DensityVelocityTemperature2D<Real>(rho, u, v, t) );

      // Compute the average normal flux
      ArrayQ fnTrue = 0;
      fnTrue(0) = (nx*(rho*u      ) + ny*(rho*v      ));
      fnTrue(1) = (nx*(rho*u*u + p) + ny*(rho*v*u    ));
      fnTrue(2) = (nx*(rho*u*v    ) + ny*(rho*v*v + p));
      fnTrue(3) = (nx*(rho*u*H    ) + ny*(rho*v*H    ));

      ArrayQ fn;
      fn = 0;
      ismailRoe.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn, 1.0 );
      SANS_CHECK_CLOSE( fnTrue(0), fn(0), small_tol, close_tol );
      SANS_CHECK_CLOSE( fnTrue(1), fn(1), small_tol, close_tol );
      SANS_CHECK_CLOSE( fnTrue(2), fn(2), small_tol, close_tol );
      SANS_CHECK_CLOSE( fnTrue(3), fn(3), small_tol, close_tol );

      // Are we accumulating
      ismailRoe.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn, 1.0 );
      SANS_CHECK_CLOSE( 2.0*fnTrue(0), fn(0), small_tol, close_tol );
      SANS_CHECK_CLOSE( 2.0*fnTrue(1), fn(1), small_tol, close_tol );
      SANS_CHECK_CLOSE( 2.0*fnTrue(2), fn(2), small_tol, close_tol );
      SANS_CHECK_CLOSE( 2.0*fnTrue(3), fn(3), small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
// Here we check if [v]^{T} F(vL, vR) = [\psi]
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( jump_IsmailRoe2D_noFix, QType, QTypes2D )
{
  typedef IsmailRoe2D<QType, TraitsSizeEuler> IsmailRoeClass;
  typedef typename IsmailRoeClass::template ArrayQ<Real> ArrayQ;

  typedef Q2D<QType, TraitsSizeEuler> QInterpret;

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-10;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  IsmailRoeClass ismailRoe(gas, eIsmailRoeEntropyNeutral);
  QInterpret qInterpret(gas);

  // Roe flux function test

  Real x, y, time;
  Real nx, ny;
  Real rhoL, uL, vL, tL, pL;
  Real rhoR, uR, vR, tR, pR;
  Real VelL = 5.0;
  Real VelR = 2.0;

  // We're going to try "all" the normal and velocity vector directions
  for (int i = 0; i < 36; i++)
  {
    Real alpha = Real(i) / 36 * 2.0 * M_PI;
    for (int j = 0; j < 36; j++)
    {
      Real beta = Real(j) / 36 * 2.0 * M_PI;
      for (int k = 0; k < 36; k++)
      {
        Real delta = Real(k) / 36 * 2.0 * M_PI;

        x = y = time = 0;   // not actually used in functions
        nx = sin(beta);
        ny = cos(beta);

        rhoL = 1.034;
        uL = VelL*sin(alpha + beta);
        vL = VelL*cos(alpha + beta);
        tL = 5.78;
        pL  = R*rhoL*tL;

        rhoR = 0.973;
        uR = VelR*sin(alpha + beta + delta);
        vR = VelR*cos(alpha + beta + delta);
        tR = 6.13;
        pR  = R*rhoR*tR;

        // set
        ArrayQ qL, qR;
        qInterpret.setFromPrimitive( qL, DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
        qInterpret.setFromPrimitive( qR, DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );

        // What are our entropy variable equivalents for qL and qR
        Real sL = log(pL) - gamma * log(rhoL);
        ArrayQ VL = 0;
        VL(0) = (gamma - sL) / (gamma - 1.0) - 0.5 * rhoL / pL * (uL*uL + vL*vL);
        VL(1) = rhoL * uL / pL;
        VL(2) = rhoL * vL / pL;
        VL(3) = -rhoL / pL;

        Real sR = log(pR) - gamma * log(rhoR);
        ArrayQ VR = 0;
        VR(0) = (gamma - sR) / (gamma - 1.0) - 0.5 * rhoR / pR * (uR*uR + vR*vR);
        VR(1) = rhoR * uR / pR;
        VR(2) = rhoR * vR / pR;
        VR(3) = -rhoR / pR;

        ArrayQ fn;
        fn = 0;
        ismailRoe.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn, 1.0 );

        Real psi = dot(VR - VL, fn);
        Real psiTrue = rhoR*(uR*nx+vR*ny) - rhoL*(uL*nx+vL*ny);
        SANS_CHECK_CLOSE( psiTrue, psi, small_tol, close_tol );
      }
    }
  }
}

//----------------------------------------------------------------------------//
// Here we check if [v]^{T} F(vL, vR) <= [\psi]
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( jump_IsmailRoe2D_Fix, QType, QTypes2D )
{
  typedef IsmailRoe2D<QType, TraitsSizeEuler> IsmailRoeClass;
  typedef typename IsmailRoeClass::template ArrayQ<Real> ArrayQ;

  typedef Q2D<QType, TraitsSizeEuler> QInterpret;

  const Real small_tol = 1e-11;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  IsmailRoeClass ismailRoe(gas, eIsmailRoeHartenFix);
  QInterpret qInterpret(gas);

  // Roe flux function test

  Real x, y, time;
  Real nx, ny;
  Real rhoL, uL, vL, tL, pL;
  Real rhoR, uR, vR, tR, pR;
  Real VelL = 5.0;
  Real VelR = 2.0;

  // We're going to try "all" the normal and velocity vector directions
  for (int i = 0; i < 36; i++)
  {
    Real alpha = Real(i) / 36 * 2.0 * M_PI;
    for (int j = 0; j < 36; j++)
    {
      Real beta = Real(j) / 36 * 2.0 * M_PI;
      for (int k = 0; k < 36; k++)
      {
        Real delta = Real(k) / 36 * 2.0 * M_PI;

        x = y = time = 0;   // not actually used in functions
        nx = sin(beta);
        ny = cos(beta);

        rhoL = 1.034;
        uL = VelL*sin(alpha + beta);
        vL = VelL*cos(alpha + beta);
        tL = 5.78;
        pL  = R*rhoL*tL;

        rhoR = 0.973;
        uR = VelR*sin(alpha + beta + delta);
        vR = VelR*cos(alpha + beta + delta);
        tR = 6.13;
        pR  = R*rhoR*tR;

        // set
        ArrayQ qL, qR;
        qInterpret.setFromPrimitive( qL, DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
        qInterpret.setFromPrimitive( qR, DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );

        // What are our entropy variable equivalents for qL and qR
        Real sL = log(pL) - gamma * log(rhoL);
        ArrayQ VL = 0;
        VL(0) = (gamma - sL) / (gamma - 1.0) - 0.5 * rhoL / pL * (uL*uL + vL*vL);
        VL(1) = rhoL * uL / pL;
        VL(2) = rhoL * vL / pL;
        VL(3) = -rhoL / pL;

        Real sR = log(pR) - gamma * log(rhoR);
        ArrayQ VR = 0;
        VR(0) = (gamma - sR) / (gamma - 1.0) - 0.5 * rhoR / pR * (uR*uR + vR*vR);
        VR(1) = rhoR * uR / pR;
        VR(2) = rhoR * vR / pR;
        VR(3) = -rhoR / pR;

        ArrayQ fn;
        fn = 0;
        ismailRoe.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn, 1.0 );

        Real psi = dot(VR - VL, fn);
        Real psiTrue = rhoR*(uR*nx+vR*ny) - rhoL*(uL*nx+vL*ny);
        BOOST_CHECK( (psi <= (psiTrue + small_tol)) );
      }
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
