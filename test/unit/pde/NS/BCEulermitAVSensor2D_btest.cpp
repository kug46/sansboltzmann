// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCEulermitAVSensor2D
//
// test of 2-D artificial viscosity BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/BCParameters.h"

#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Fluids2D_Sensor.h"
#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/BCEulermitAVSensor2D.h"

#include "pde/ArtificialViscosity/AVVariable.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#define AVSENSOR2D_INSTANTIATE
#include "pde/ArtificialViscosity/AVSensor_Source2D_impl.h"

#define BCPARAMETERS_INSTANTIATE // included because testing without NDConvert
#include "pde/BCParameters_impl.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{

typedef QTypePrimitiveRhoPressure QType;
typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEBase;
typedef BCEuler2D<BCTypeFullState_mitState, PDEBase> BCBaseEuler;
typedef BCEuler2DFullStateParams<NSVariableType2DParams> BCBaseEulerParams;


typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
typedef AVSensor_Source2D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> PDEClass;

template class BCEuler2D<BCTypeFullState_mitState, PDEClass>;


// Instantiate BCParameters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters<BCEulermitAVSensor2DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV>>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( BCEulermitAVSensor2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBase;

  {
    typedef BCNone<PhysD2,TraitsSizeEuler<PhysD2>::N> BCClass;

    BOOST_CHECK( BCClass::D == 2 );
    BOOST_CHECK( BCClass::NBC == 0 );
  }

  {
    typedef BCEuler2D<BCTypeSymmetry_mitState, PDEBase> BCBaseEuler;
    typedef BCmitAVSensor2D<BCTypeSymmetry_mitState,BCBaseEuler> BCClass;

    BOOST_CHECK( BCClass::D == 2 );
    BOOST_CHECK( BCClass::N == 5 );
    BOOST_CHECK( BCClass::NBC == 1 );
  }

  {
    typedef BCEuler2D<BCTypeFullState_mitState, PDEBase> BCBaseEuler;
    typedef BCmitAVSensor2D<BCTypeFullState_mitState,BCBaseEuler> BCClass;

    BOOST_CHECK( BCClass::D == 2 );
    BOOST_CHECK( BCClass::N == 5 );
    BOOST_CHECK( BCClass::NBC == 5 );
  }

  {
    typedef BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDEBase> BCBaseEuler;
    typedef BCmitAVSensor2D<BCTypeDirichlet_mitState,BCBaseEuler> BCClass;

    BOOST_CHECK( BCClass::D == 2 );
    BOOST_CHECK( BCClass::N == 5 );
    BOOST_CHECK( BCClass::NBC == 4 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBase;

  typedef BCParameters< BCEulermitAVSensor2DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> > BCParams;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool hasSpaceTimeDiffusion = false;
  PDEBase pde(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  {
    typedef BCNone<PhysD2,TraitsSizeEuler<PhysD2>::N> BCClass;

    BCClass bc1;

    //Pydict constructor
    PyDict BCNone;
    BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

    PyDict BCList;
    BCList["BC1"] = BCNone;
    BCParams::checkInputs(BCList);

    BCClass bc2(pde, BCNone);
  }

  {
    typedef BCEuler2D<BCTypeSymmetry_mitState, PDEBase> BCBaseEuler;
    typedef BCmitAVSensor2D<BCTypeSymmetry_mitState, BCBaseEuler> BCClass;

    BCClass bc1(pde);

    //Pydict constructor
    PyDict BCSymmetry;
    BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

    PyDict BCList;
    BCList["BC1"] = BCSymmetry;
    BCParams::checkInputs(BCList);

    BCClass bc2(pde, BCSymmetry);
  }

  {
    typedef BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDEBase> BCBaseEuler;
    typedef BCmitAVSensor2D<BCTypeDirichlet_mitState, BCBaseEuler> BCClass;

    Real TtSpec = 20.5;
    Real PtSpec = 71.4;
    Real aSpec = 2.1;
    Real sSpec = 0.2;
    BCClass bc1(sSpec, pde, TtSpec, PtSpec, aSpec);

    //Pydict constructor
    PyDict BCInflowSubsonic;
    BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
    BCInflowSubsonic[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.TtSpec] = TtSpec;
    BCInflowSubsonic[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.PtSpec] = PtSpec;
    BCInflowSubsonic[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.aSpec] = aSpec;
    //BCInflowSubsonic[BCClass::ParamsType::params.sensor] = sSpec;

    PyDict BCList;
    BCList["BC1"] = BCInflowSubsonic;
    BCParams::checkInputs(BCList);

    BCClass bc2(pde, BCInflowSubsonic);
  }

  {
    typedef BCEuler2D<BCTypeFullState_mitState, PDEBase> BCBaseEuler;
    typedef BCmitAVSensor2D<BCTypeFullState_mitState, BCBaseEuler> BCClass;

    bool upwind = true;
    Real sB = 0.2;
    BCClass bc1(sB, pde, DensityVelocityPressure2D<Real>(1.1, 1.3, -0.4, 5.45), upwind);

    PyDict d;
    d[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.PrimitivePressure;
    d[DensityVelocityPressure2DParams::params.rho] = 1.1;
    d[DensityVelocityPressure2DParams::params.u] = 1.3;
    d[DensityVelocityPressure2DParams::params.v] = -0.4;
    d[DensityVelocityPressure2DParams::params.p] = 5.45;

    //Pydict constructor
    PyDict BCFullState;
    BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
    BCFullState[BCClass::ParamsType::params.Characteristic] = false;
    BCFullState[BCClass::ParamsType::params.StateVector] = d;
    //BCFullState[BCClass::ParamsType::params.sensor] = sB;

    PyDict BCList;
    BCList["BC1"] = BCFullState;
    BCParams::checkInputs(BCList);

    BCClass bc2(pde, BCFullState);
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCNone_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBase;

//  typedef BCParameters< BCEulermitAVSensor2DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> > BCParams;

  typedef BCNone<PhysD2,TraitsSizeEuler<PhysD2>::N> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool hasSpaceTimeDiffusion = false;
  PDEBase pde(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  BCClass bc;

  Real nx = 0.8;
  Real ny = 0.6;
  ArrayQ q = 0.1;
  BOOST_CHECK_EQUAL( true, bc.isValidState( nx, ny, q ));
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeSymmetry_mitState_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBase;

  typedef BCParameters< BCEulermitAVSensor2DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> > BCParams;

  typedef BCEuler2D<BCTypeSymmetry_mitState, PDEBase> BCBaseEuler;
  typedef BCmitAVSensor2D<BCTypeSymmetry_mitState, BCBaseEuler> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool hasSpaceTimeDiffusion = false;
  PDEBase pde(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  //Pydict constructor
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCList;
  BCList["BC1"] = BCSymmetry;
  BCParams::checkInputs(BCList);

  BCClass bc(pde, BCSymmetry);

  Real x = 0, y = 0, time = 0;   // not actually used in functions
  Real nx = -0.936, ny = 0.352;   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, v = -0.231, t = 0.987;
  Real p  = R*rho*t;
  Real qn = nx*u + ny*v;
  Real param = 0.1;

  Real uBTrue = u - qn*nx;
  Real vBTrue = v - qn*ny;

  Real pBTrue = p;
  Real sBTrue = 0.265;

  ArrayQ qI;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( qI, qdata );
  qI(4) = sBTrue;

  ArrayQ qB;
  bc.state( param, x, y, time, nx, ny, qI, qB );

  Real rhoB, uB, vB, tB;
  pde.variableInterpreter().eval( qB, rhoB, uB, vB, tB );
  Real pB = gas.pressure(rhoB, tB);

  SANS_CHECK_CLOSE(    rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE( uBTrue,   uB, small_tol, close_tol );
  SANS_CHECK_CLOSE( vBTrue,   vB, small_tol, close_tol );
  SANS_CHECK_CLOSE( pBTrue,   pB, small_tol, close_tol );
  SANS_CHECK_CLOSE( sBTrue,qB(4), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFullState_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBase;

  typedef BCParameters< BCEulermitAVSensor2DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> > BCParams;

  typedef BCEuler2D<BCTypeFullState_mitState, PDEBase> BCBaseEuler;
  typedef BCmitAVSensor2D<BCTypeFullState_mitState, BCBaseEuler> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool hasSpaceTimeDiffusion = false;
  PDEBase pde(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  Real x = 0, y = 0, time = 0;   // not actually used in functions
  Real nx = -0.936, ny = 0.352;   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, v = -0.231, t = 0.987;
  Real p  = R*rho*t;
  //Real s = 0.265;
  Real param = 0.1;

  PyDict d;
  d[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.PrimitivePressure;
  d[DensityVelocityPressure2DParams::params.rho] = rho;
  d[DensityVelocityPressure2DParams::params.u] = u;
  d[DensityVelocityPressure2DParams::params.v] = v;
  d[DensityVelocityPressure2DParams::params.p] = p;

  //Pydict constructor
  PyDict BCFullState;
  BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFullState[BCClass::ParamsType::params.Characteristic] = false;
  BCFullState[BCClass::ParamsType::params.StateVector] = d;

  PyDict BCList;
  BCList["BC1"] = BCFullState;
  BCParams::checkInputs(BCList);

  BCClass bc(pde, BCFullState);

  ArrayQ qI = 0;
  ArrayQ qB;
  bc.state( param, x, y, time, nx, ny, qI, qB );

  Real rhoB, uB, vB, tB;
  pde.variableInterpreter().eval( qB, rhoB, uB, vB, tB );
  Real pB = gas.pressure(rhoB, tB);

  SANS_CHECK_CLOSE(rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(v, vB, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB, small_tol, close_tol );
  SANS_CHECK_CLOSE(qI(4), qB(4), small_tol, close_tol );
  SANS_CHECK_CLOSE(p, pB, small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeInflowSubsonic_PtTta_mitState_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBase;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBase, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef BCParameters< BCEulermitAVSensor2DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> > BCParams;

  typedef BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, AVPDEClass> BCBaseEuler;
  typedef BCmitAVSensor2D<BCTypeDirichlet_mitState, BCBaseEuler> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  Real us = 1.0;
  Real vs = 0.6;
  Real ksxx = 0.8, ksxy = 0.1, ksyx = 0.17, ksyy = 1.3;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs);
  SensorViscousFlux visc(ksxx, ksxy, ksyx, ksyy);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass pde(adv, visc, source, isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);
//  PDEBase pde(order, gas, Euler_ResidInterp_Raw);

  Real x = 0, y = 0, time = 0;   // not actually used in functions
  Real nx = -0.936, ny = 0.352;   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, v = -0.231, t = 0.987;
  Real p  = R*rho*t;
  Real s = 0.167;
  Real param = 0.1;

  Real TtSpec = t*(1+0.2*0.1*0.1);
  Real PtSpec = p*pow( (1+0.2*0.1*0.1), 3.5 );
  Real aSpec = 0.1;
  //Real sSpec = 0.265;

  //Pydict constructor
  PyDict BCInflowSubsonic;
  BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  BCInflowSubsonic[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.TtSpec] = TtSpec;
  BCInflowSubsonic[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.PtSpec] = PtSpec;
  BCInflowSubsonic[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.aSpec] = aSpec;
  //BCInflowSubsonic[BCClass::ParamsType::params.sensor] = sSpec;

  PyDict BCList;
  BCList["BC1"] = BCInflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc(pde, BCInflowSubsonic);

  ArrayQ qI;
//  DensityVelocityPressure2D<Real> qdata(rho, u, v, p);
  AVVariable<DensityVelocityPressure2D, Real> qdata = {{rho, u, v, p}, s};
  pde.setDOFFrom( qI, qdata );

  ArrayQ qB = 0;
  bc.state( param, x, y, time, nx, ny, qI, qB );

  Real rhoB, uB, vB, tB;
  pde.variableInterpreter().eval( qB, rhoB, uB, vB, tB );

  Real rhoTrue = 0.789121117837660;
  Real uTrue = 0.614296756252870;
  Real vTrue = 0.061635263601788;
  Real tTrue = 0.852845639120693;
  //Real sTrue = sSpec;

  SANS_CHECK_CLOSE(rhoTrue, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(uTrue, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(vTrue, vB, small_tol, close_tol );
  SANS_CHECK_CLOSE(tTrue, tB, small_tol, close_tol );
  SANS_CHECK_CLOSE(qI(4), qB(4), small_tol, close_tol );

  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  Real sx = 0.325; Real sy = -0.12;
  ArrayQ qx = {rhox, ux, vx, px, sx};
  ArrayQ qy = {rhoy, uy, vy, py, sy};

  ArrayQ Fn = 0;
  bc.fluxNormal( param, x, y, time, nx, ny, qI, qx, qy, qB, Fn);

  ArrayQ fx = 0;
  ArrayQ fy = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(param, x, y, time, qB, fx, fy);
  FnTrue += fx*nx + fy*ny;

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
