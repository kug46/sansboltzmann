// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCNavierStokes3D_mitState_btest
//
// test of 3-D compressible Navier-Stokes BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/Q3DConservative.h"
#include "pde/NS/PDENavierStokes3D.h"
#include "pde/NS/BCNavierStokes3D.h"
#include "pde/BCParameters.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"


//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
template class BCNavierStokes3D< BCTypeWallNoSlipAdiabatic_mitState,
  PDENavierStokes3D< TraitsSizeNavierStokes,
                     TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > >;

template class BCNavierStokes3D< BCTypeSymmetry_mitState,
  PDENavierStokes3D< TraitsSizeNavierStokes,
                     TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > >;

template class BCEuler3D< BCTypeReflect_mitState,
  PDENavierStokes3D< TraitsSizeNavierStokes,
                     TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > >;

// Instantiate BCParamaters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters< BCNavierStokes3DVector<
    TraitsSizeNavierStokes,
    TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel>
                                                    > >;
}

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( BCNavierStokes3D_mitState_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test_BCTypeWallNoSlipAdiabatic_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;

  typedef BCNavierStokes3D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  typedef BCParameters< BCNavierStokes3DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> > BCParams;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 5 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 5 );
  BOOST_CHECK( MatrixQ::M == 5 );
  BOOST_CHECK( MatrixQ::N == 5 );

  // ctors

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;

  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);

  BCClass bc1(pde);

  BOOST_CHECK( bc1.D == 3 );
  BOOST_CHECK( bc1.N == 5 );
  BOOST_CHECK( bc1.NBC == 3 );

  //Pydict constructor
  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  BCClass bc(pde, BCNoSlip);

  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 5 );
  BOOST_CHECK( bc.NBC == 3 );

  // functions

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  Real x, y, z, time;
  Real nx, ny, nz;
  Real rho, u, v, w, t, p;

  x = y = z = time = 0;   // not actually used in functions
  nx = -0.856;  ny = 0.48;  nz = 0.192;    // Note: magnitude = 1
  rho = 1.137;
  u =  1.451;
  v = -0.567;
  w = -1.003;
  t = 300.1;
  p = R*rho*t;

  ArrayQ q, qB;
  DensityVelocityPressure3D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.VelocityZ = w;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  bc.state(x, y, z, time, nx, ny, nz, q, qB);

  ArrayQ qTrue = {rho, 0, 0, 0, p};
  SANS_CHECK_CLOSE( qTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(4), qB(4), small_tol, close_tol );

  Real rhox = 0.01, rhoy = -0.025, rhoz = -0.344;
  Real ux = 0.049, uy =  0.072, uz =  1.431;
  Real vx = 0.143, vy = -0.024, vz = -0.987;
  Real wx = 0.659, wy =  0.349, wz =  0.776;
  Real px = 0.002, py = -4.231, pz = -0.451;

  ArrayQ qx = {rhox, ux, vx, wx, px};
  ArrayQ qy = {rhoy, uy, vy, wy, py};
  ArrayQ qz = {rhoz, uz, vz, wz, pz};

  ArrayQ Fn = 0;
  bc.fluxNormal( x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn);

  ArrayQ fx = 0, fy = 0, fz = 0;
  ArrayQ FnTrue;
  pde.fluxAdvective(x, y, z, time, qTrue, fx, fy, fz);
  FnTrue = fx*nx + fy*ny + fz*nz;

  // Compute the viscous flux
  ArrayQ fv = 0, gv = 0, hv = 0;
  pde.fluxViscous(x, y, z, time, qTrue, qx, qy, qz, fv, gv, hv);
  ArrayQ Fvn = fv*nx + gv*ny + hv*nz;

  // Zero out the energy equation for the adiabatic wall
  Fvn[pde.iEngy] = 0;

  FnTrue += Fvn;
  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );

  //printf( "btest: Fn  = %e %e %e %e %e\n", Fn(0), Fn(1), Fn(2), Fn(3), Fn(4) );
  //printf( "btest: FnT = %e %e %e %e %e\n", FnTrue(0), FnTrue(1), FnTrue(2), FnTrue(3), FnTrue(4) );

  // there is no invalid state for a wall
  BOOST_CHECK( bc.isValidState(nx, ny, nz, q) == true );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test_BCTypeWallNoSlipIsothermal_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;
  const Real Twall = 300.0;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);

  typedef BCNavierStokes3D<BCTypeWallNoSlipIsothermal_mitState, PDEClass> BCClass;
  typedef BCParameters< BCNavierStokes3DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> > BCParams;

  //Pydict constructor
  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipIsothermal_mitState;
  BCNoSlip[BCClass::ParamsType::params.Twall] = Twall;

  BCClass bc(pde, BCNoSlip);
  typedef BCClass::ArrayQ<Real> ArrayQ;

  Real x, y, z, time;
  Real nx, ny, nz;
  Real rho, u, v, w, t, p;

  x = y = z = time = 0;   // not actually used in functions
  nx = -0.856;  ny = 0.48;  nz = 0.192;    // Note: magnitude = 1
  rho = 1.137;
  u =  1.451;
  v = -0.567;
  w = -1.003;
  t = 310.1;
  p = R*rho*t;

  ArrayQ q, qB;
  DensityVelocityPressure3D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.VelocityZ = w;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  bc.state(x, y, z, time, nx, ny, nz, q, qB);

  rho = p / (R*Twall);
  ArrayQ qTrue = {rho, 0.0, 0.0, 0.0, p};

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;
  SANS_CHECK_CLOSE( qTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(4), qB(4), small_tol, close_tol );

  Real rhox = 0.01, rhoy = -0.025, rhoz = -0.344;
  Real ux = 0.049, uy =  0.072, uz =  1.431;
  Real vx = 0.143, vy = -0.024, vz = -0.987;
  Real wx = 0.659, wy =  0.349, wz =  0.776;
  Real px = 0.002, py = -4.231, pz = -0.451;

  ArrayQ qx = {rhox, ux, vx, wx, px};
  ArrayQ qy = {rhoy, uy, vy, wy, py};
  ArrayQ qz = {rhoz, uz, vz, wz, pz};

  ArrayQ Fn = 0;
  bc.fluxNormal( x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn);

  ArrayQ fx = 0, fy = 0, fz = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, z, time, qTrue, fx, fy, fz);
  FnTrue = fx*nx + fy*ny + fz*nz;

  // Compute the viscous flux
  ArrayQ fv = 0, gv = 0, hv = 0;
  pde.fluxViscous(x, y, z, time, qTrue, qx, qy, qz, fv, gv, hv);
  ArrayQ Fvn = fv*nx + gv*ny + hv*nz;

  FnTrue += Fvn;
  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );

  // check accumulation
  bc.fluxNormal( x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn);

  SANS_CHECK_CLOSE( 2*FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*FnTrue(4), Fn(4), small_tol, close_tol );

  // there is no invalid state for a wall
  BOOST_CHECK( bc.isValidState(nx, ny, nz, q) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test_BCTypeSymmetry_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;

  typedef BCNavierStokes3D<BCTypeSymmetry_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  typedef BCParameters< BCNavierStokes3DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> > BCParams;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 5 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( ArrayQ::M == 5 );
  BOOST_CHECK( MatrixQ::M == 5 );
  BOOST_CHECK( MatrixQ::N == 5 );

  // ctors

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;

  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);

  BCClass bc1(pde);

  BOOST_CHECK( bc1.D == 3 );
  BOOST_CHECK( bc1.N == 5 );
  BOOST_CHECK( bc1.NBC == 1 );

  //Pydict constructor
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCList;
  BCList["BC1"] = BCSymmetry;
  BCParams::checkInputs(BCList);

  BCClass bc(pde, BCSymmetry);

  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 5 );
  BOOST_CHECK( bc.NBC == 1 );
  BOOST_CHECK( bc.hasFluxViscous() == true );

  // functions

  const Real small_tol = 1.0e-9;
  const Real close_tol = 1.0e-12;

  Real x, y, z, time;
  Real nx, ny, nz;
  Real rho, u, v, w, t, p;

  x = y = z = time = 0;   // not actually used in functions
  nx = -0.856;  ny = 0.48;  nz = 0.192;    // Note: magnitude = 1
  rho = 1.137;
  u =  1.451;
  v = -0.567;
  w = -1.003;
  t = 300.1;
  p = R*rho*t;

  // VB = V - n*dot(V,n)
  Real uB = u - nx*(u*nx + v*ny + w*nz);
  Real vB = v - ny*(u*nx + v*ny + w*nz);
  Real wB = w - nz*(u*nx + v*ny + w*nz);

  Real rhoB = rho;
  Real pB = p;

  ArrayQ q, qB, qTrue;
  DensityVelocityPressure3D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.VelocityZ = w;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  pde.variableInterpreter().setFromPrimitive( qTrue, DensityVelocityPressure3D<Real>(rhoB, uB, vB, wB, pB) );

  bc.state(x, y, z, time, nx, ny, nz, q, qB);
  SANS_CHECK_CLOSE( qTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(4), qB(4), small_tol, close_tol );


  Real rhox = 0.01, rhoy = -0.025, rhoz = -0.344;
  Real ux = 0.049, uy =  0.072, uz =  1.431;
  Real vx = 0.143, vy = -0.024, vz = -0.987;
  Real wx = 0.659, wy =  0.349, wz =  0.776;
  Real px = 0.002, py = -4.231, pz = -0.451;

  ArrayQ qx = {rhox, ux, vx, wx, px};
  ArrayQ qy = {rhoy, uy, vy, wy, py};
  ArrayQ qz = {rhoz, uz, vz, wz, pz};

  ArrayQ Fn = 0;
  bc.fluxNormal( x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn);

  ArrayQ fx = 0, fy = 0, fz = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, z, time, qB, fx, fy, fz);
  FnTrue += fx*nx + fy*ny + fz*nz;

  // Compute the viscous flux
  ArrayQ fv = 0, gv = 0, hv = 0;
  pde.fluxViscous(x, y, z, time, qB, qx, qy, qz, fv, gv, hv);

  // Compute normal x-, y- and z-momentum fluxes
  Real xMomN = fv[pde.ixMom]*nx + gv[pde.ixMom]*ny + hv[pde.ixMom]*nz;
  Real yMomN = fv[pde.iyMom]*nx + gv[pde.iyMom]*ny + hv[pde.iyMom]*nz;
  Real zMomN = fv[pde.izMom]*nx + gv[pde.izMom]*ny + hv[pde.izMom]*nz;

  FnTrue[pde.ixMom] += (xMomN*nx + yMomN*ny + zMomN*nz)*nx;
  FnTrue[pde.iyMom] += (xMomN*nx + yMomN*ny + zMomN*nz)*ny;
  FnTrue[pde.izMom] += (xMomN*nx + yMomN*ny + zMomN*nz)*nz;

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );

  BOOST_CHECK( bc.isValidState(nx, ny, nz, q) == true );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test_BCTypeReflect_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;

  typedef BCEuler3D<BCTypeReflect_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;
  typedef DLA::VectorS<BCClass::D,Real> VectorX;
  typedef DLA::MatrixS<BCClass::D,BCClass::D,Real> MatrixX;

  typedef BCParameters< BCNavierStokes3DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> > BCParams;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 5 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( ArrayQ::M == 5 );
  BOOST_CHECK( MatrixQ::M == 5 );
  BOOST_CHECK( MatrixQ::N == 5 );

  // ctors

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;

  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);

  BCClass bc1(pde);

  BOOST_CHECK( bc1.D == 3 );
  BOOST_CHECK( bc1.N == 5 );
  BOOST_CHECK( bc1.NBC == 1 );

  //Pydict constructor
  PyDict BCReflect;
  BCReflect[BCParams::params.BC.BCType] = BCParams::params.BC.Reflect_mitState;

  PyDict BCList;
  BCList["BC1"] = BCReflect;
  BCParams::checkInputs(BCList);

  BCClass bc(pde, BCReflect);

  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 5 );
  BOOST_CHECK( bc.NBC == 1 );
  BOOST_CHECK( bc.hasFluxViscous() == true );

  // functions

  const Real small_tol = 1.0e-9;
  const Real close_tol = 1.0e-12;

  Real x, y, z, time;
  Real nx, ny, nz;
  Real rho, u, v, w, t, p;

  x = y = z = time = 0;   // not actually used in functions
  nx = -0.856;  ny = 0.48;  nz = 0.192;    // Note: magnitude = 1
  rho = 1.137;
  u =  1.451;
  v = -0.567;
  w = -1.003;
  t = 300.1;
  p = R*rho*t;

  // VB = V - 2*n*dot(V,n)
  Real uB = u - 2*nx*(u*nx + v*ny + w*nz);
  Real vB = v - 2*ny*(u*nx + v*ny + w*nz);
  Real wB = w - 2*nz*(u*nx + v*ny + w*nz);

  Real rhoB = rho;
  Real pB = p;

  ArrayQ qI, qB, qTrue;
  DensityVelocityPressure3D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.VelocityZ = w;
  qdata.Pressure  = p;
  pde.setDOFFrom( qI, qdata );

  pde.variableInterpreter().setFromPrimitive( qTrue, DensityVelocityPressure3D<Real>(rhoB, uB, vB, wB, pB) );

  bc.state(x, y, z, time, nx, ny, nz, qI, qB);
  SANS_CHECK_CLOSE( qTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(4), qB(4), small_tol, close_tol );


  Real rhox = 0.01, rhoy = -0.025, rhoz = -0.344;
  Real ux = 0.049, uy =  0.072, uz =  1.431;
  Real vx = 0.143, vy = -0.024, vz = -0.987;
  Real wx = 0.659, wy =  0.349, wz =  0.776;
  Real px = 0.002, py = -4.231, pz = -0.451;

  ArrayQ qIx = {rhox, ux, vx, wx, px};
  ArrayQ qIy = {rhoy, uy, vy, wy, py};
  ArrayQ qIz = {rhoz, uz, vz, wz, pz};

  ArrayQ Fn = 0;
  bc.fluxNormal( x, y, z, time, nx, ny, nz, qI, qIx, qIy, qIz, qB, Fn);

  // compute advective flux with reflected state vector

  ArrayQ FnTrue = 0;
  pde.fluxAdvectiveUpwind( x, y, z, time, qI, qB, nx, ny, nz, FnTrue );

  // compute the reflected boundary gradient

  // reflect scalars
  Real rhoBx = rhox - 2*nx*(rhox*nx + rhoy*ny + rhoz*nz);
  Real rhoBy = rhoy - 2*ny*(rhox*nx + rhoy*ny + rhoz*nz);
  Real rhoBz = rhoz - 2*nz*(rhox*nx + rhoy*ny + rhoz*nz);

  Real pBx = px - 2*nx*(px*nx + py*ny + pz*nz);
  Real pBy = py - 2*ny*(px*nx + py*ny + pz*nz);
  Real pBz = pz - 2*nz*(px*nx + py*ny + pz*nz);

  VectorX gradIu = {ux, uy, uz};
  VectorX gradIv = {vx, vy, vz};
  VectorX gradIw = {wx, wy, wz};

  MatrixX Jr = { {1-2*nx*nx,  -2*nx*ny,  -2*nx*nz},
                 { -2*ny*nx, 1-2*ny*ny,  -2*ny*nz},
                 { -2*nz*nx,  -2*nz*ny, 1-2*nz*nz} };

  VectorX gradBu = Jr*(gradIu - 2*nx*(gradIu*nx + gradIv*ny + gradIw*nz));
  VectorX gradBv = Jr*(gradIv - 2*ny*(gradIu*nx + gradIv*ny + gradIw*nz));
  VectorX gradBw = Jr*(gradIw - 2*nz*(gradIu*nx + gradIv*ny + gradIw*nz));

  ArrayQ qBx = {rhoBx, gradBu[0], gradBv[0], gradBw[0], pBx};
  ArrayQ qBy = {rhoBy, gradBu[1], gradBv[1], gradBw[1], pBy};
  ArrayQ qBz = {rhoBz, gradBu[2], gradBv[2], gradBw[2], pBz};

  // Compute the viscous flux
  pde.fluxViscous(x, y, z, time, qI, qIx, qIy, qIz, qB, qBx, qBy, qBz, nx, ny, nz, FnTrue);

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );

  BOOST_CHECK( bc.isValidState(nx, ny, nz, qI) == true );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/BCNavierStokes3D_mitState_pattern.txt", true );

  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;

  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);

  {
    typedef BCNavierStokes3D<BCTypeSymmetry_mitState, PDEClass> BCClass;

    BCClass bc(pde);

    bc.dump( 2, output );
  }

  {
    typedef BCNavierStokes3D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass> BCClass;

    BCClass bc(pde);

    bc.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
