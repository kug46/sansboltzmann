// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCBoltzmannD1Q3_btest
//
// test of quasi 1-D compressible Euler BC classes
#if 1

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/NS/QD1Q3PrimitiveDistributionFunctions.h"
#include "pde/NS/PDEBoltzmannD1Q3.h"
#include "pde/NS/BCBoltzmannD1Q3.h"
#include "pde/BCParameters.h"
#include "pde/BCCategory.h"

#include "pde/NDConvert/BCNDConvertSpace1D.h"
//#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveDistributionFunctions {};
//class QTypePrimitiveSurrogate {};
//class QTypeConservative {};
//class QTypeEntropy {};

//template class BCBoltzmannD1Q3<BCTypeTimeIC,
 // PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3<QTypePrimitiveDistributionFunctions, GasModel>> >;

//template class BCBoltzmannD1Q3< BCTypeReflect,
//  PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3<QTypePrimitiveDistributionFunctions, GasModel>> >;

//template class BCBoltzmannD1Q3< BCTypeInflowSupersonic,
//  PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3<QTypePrimitiveDistributionFunctions, GasModel>> >;

//template class BCBoltzmannD1Q3< BCTypeOutflowSubsonic_Pressure,
//  PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3<QTypePrimitiveDistributionFunctions, GasModel>> >;

//template class BCBoltzmannD1Q3 < BCTypeFunction_mitState,
//  PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3<QTypePrimitiveDistributionFunctions, GasModel>> >;

//template class BCBoltzmannD1Q3< BCTypeOutflowSubsonic_Pressure_mitState,
//  PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3<QTypePrimitiveDistributionFunctions, GasModel>> >;

template class BCBoltzmannD1Q3< BCTypeBounceBackDirichlet_pdf0,
  PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3<QTypePrimitiveDistributionFunctions, GasModel>> >;

template class BCBoltzmannD1Q3< BCTypeBounceBackDirichlet_pdf2,
  PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3<QTypePrimitiveDistributionFunctions, GasModel>> >;

//template class BCBoltzmannD1Q3< BCTypeInflowSubsonic_PtTt_mitState,
//  PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3<QTypePrimitiveDistributionFunctions, GasModel>> >;

//template struct SpaceTimeBC< BCBoltzmannD1Q3<BCTypeFullStateSpaceTime_mitState ,
//  PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3<QTypePrimitiveDistributionFunctions, GasModel>> > >;

//template struct SpaceTimeBC< BCBoltzmannD1Q3<BCTypeOutflowSupersonic_Pressure_mitState,
//  PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3<QTypePrimitiveDistributionFunctions, GasModel>> > >;

//template struct SpaceTimeBC< BCBoltzmannD1Q3<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState,
//  PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3<QTypePrimitiveDistributionFunctions, GasModel>> > >;

///template struct SpaceTimeBC< BCBoltzmannD1Q3<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState,
  //PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3<QTypePrimitiveDistributionFunctions, GasModel>> > >;

//template struct SpaceTimeBC< BCBoltzmannD1Q3<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState,
//  PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3<QTypePrimitiveDistributionFunctions, GasModel>> > >;

//template class BCBoltzmannD1Q3<BCTypeTimeIC_Function,
//  PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3<QTypePrimitiveDistributionFunctions, GasModel>> >;

// Instantiate BCParamaters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters<
                BCBoltzmannD1Q3Vector<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3<QTypePrimitiveDistributionFunctions, GasModel>> >;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BCBoltzmannD1Q3_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef QTypePrimitiveDistributionFunctions QType;
  typedef TraitsModelBoltzmannD1Q3<QType, GasModel> TraitsModelBoltzmannD1Q3Class;
  typedef PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> PDEClass;

  {
  typedef BCBoltzmannD1Q3<BCTypeBounceBackDirichlet_pdf0, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 1 );
  BOOST_CHECK( BCClass::N == 3 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( ArrayQ::M == 3 );
  BOOST_CHECK( MatrixQ::M == 3 );
  BOOST_CHECK( MatrixQ::N == 3 );
  }

  {
  typedef BCBoltzmannD1Q3<BCTypeBounceBackDirichlet_pdf2, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 1 );
  BOOST_CHECK( BCClass::N == 3 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( ArrayQ::M == 3 );
  BOOST_CHECK( MatrixQ::M == 3 );
  BOOST_CHECK( MatrixQ::N == 3 );
  }

}


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctorND )
{
  typedef QTypePrimitiveDistributionFunctions QType;
  typedef TraitsModelBoltzmannD1Q3<QType, GasModel> TraitsModelBoltzmannD1Q3Class;
  typedef PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> PDEClass;
  typedef BCParameters< BCBoltzmannD1Q3Vector<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> > BCParams;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  {
  typedef BCBoltzmannD1Q3<BCTypeNone, PDEClass> BCClass;

  BCClass bc1(pde);

  //Pydict constructor
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BCList;
  BCList["BC1"] = BCNone;
  BCParams::checkInputs(BCList);

  // ISSUES WITH BCNDConvertSpace
  std::cout<< " BCNDConvertSpace causing issues with incomplete arguments list " << std::endl;
#if 1
  std::map< std::string, std::shared_ptr<BCBase> > BCs =
      BCParams::template createBCs<BCNDConvertSpace>(pde, BCList);
#endif

  }
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
#endif
