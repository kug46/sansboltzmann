// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// SolutionFunction_NavierStokes2D_btest
//
// test of 2-D Navier-Stokes solution function classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"

// Explicitly instantiate the class so coverage information is correct
namespace SANS
{
template class SolutionFunction_NavierStokes2D_OjedaMMS<
                 TraitsSizeNavierStokes,
                 TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
template class SolutionFunction_NavierStokes2D_QuadraticMMS<
                 TraitsSizeNavierStokes,
                 TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( NavierStokes_SolutionFunction2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( OjedaMMS_Test )
{

  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef SolutionFunction_NavierStokes2D_OjedaMMS<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> SolutionClass;

  // PDE
  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)

  const Real muRef = 5.1;      // kg/(m s) // FOR RE = 10
  //  const Real muRef = 0.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef); //single argument for constant viscosity
  ThermalConductivityModel tcond(Prandtl, Cp);

  // INFLOW CONDITIONS TO MATCH OJEDA'S MMS

  SolutionClass soln(gas);

  Real x = 0.0; Real y = 0.0;
  Real time = 0.0;
  ArrayQ out = soln(x, y, time);
  ArrayQ outTrue = {1.0, 0.2, 0.1, 1.0};

  Real tol = 1e-12;
  SANS_CHECK_CLOSE( out(0), outTrue(0), tol, tol);
  SANS_CHECK_CLOSE( out(1), outTrue(1), tol, tol);
  SANS_CHECK_CLOSE( out(2), outTrue(2), tol, tol);
  SANS_CHECK_CLOSE( out(3), outTrue(3), tol, tol);

//  x= 0.5; y=0.5;
//
//  out = soln(x, y, time);
//  outTrue(0) = 2;
//  outTrue(1) = 0.6;
//  outTrue(2) = 0.12;
//  outTrue(3) = 11.0;
//  SANS_CHECK_CLOSE( out(0), outTrue(0), tol, tol);
//  SANS_CHECK_CLOSE( out(1), outTrue(1), tol, tol);
//  SANS_CHECK_CLOSE( out(2), outTrue(2), tol, tol);
//  SANS_CHECK_CLOSE( out(3), outTrue(3), tol, tol);
//
//  PyDict gasModelDict;
//  gasModelDict[GasModelParams::params.gamma] = 1.4;
//  gasModelDict[GasModelParams::params.R] = 1.0;
//
//  PyDict solnArgs;
//  solnArgs[SolutionClass::ParamsType::params.Ec] = Ec;
//  solnArgs[SolutionClass::ParamsType::params.gasModel] = gasModelDict;
//  SolutionClass::ParamsType::checkInputs(solnArgs);
//
//  SolutionClass solnExact(solnArgs);
//
//  out = solnExact(x, y, time);
//  BOOST_CHECK_CLOSE(outTrue(0), out(0), tol);
//  BOOST_CHECK_CLOSE(outTrue(1), out(1), tol);
//  BOOST_CHECK_CLOSE(outTrue(2), out(2), tol);
//  BOOST_CHECK_CLOSE(outTrue(3), out(3), tol);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( QuadraticMMS_Test )
{

  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  typedef SolutionFunction_NavierStokes2D_QuadraticMMS<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> SolutionClass;

  // PDE
  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)

  const Real muRef = 5.1;      // kg/(m s) // FOR RE = 10
  //  const Real muRef = 0.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef); //single argument for constant viscosity
  ThermalConductivityModel tcond(Prandtl, Cp);

  // INFLOW CONDITIONS TO MATCH OJEDA'S MMS

  SolutionClass soln(gas);

  Real x = 0.0; Real y = 0.0;
  Real time = 0.0;
  ArrayQ out = soln(x, y, time);
  ArrayQ outTrue = {1.0, 0.3, 0.1, 1.0};

  Real tol = 1e-12;
  SANS_CHECK_CLOSE( out(0), outTrue(0), tol, tol);
  SANS_CHECK_CLOSE( out(1), outTrue(1), tol, tol);
  SANS_CHECK_CLOSE( out(2), outTrue(2), tol, tol);
  SANS_CHECK_CLOSE( out(3), outTrue(3), tol, tol);

  x= 0.5; y=0.5;

  out = soln(x, y, time);
  outTrue(0) = 1.00875;
  outTrue(1) = 0.3 + 0.25*0.015;
  outTrue(2) = 0.1 + 0.25*0.012;
  outTrue(3) = 1.0;
  SANS_CHECK_CLOSE( out(0), outTrue(0), tol, tol);
  SANS_CHECK_CLOSE( out(1), outTrue(1), tol, tol);
  SANS_CHECK_CLOSE( out(2), outTrue(2), tol, tol);
  SANS_CHECK_CLOSE( out(3), outTrue(3), tol, tol);

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.gasModel] = gasModelDict;
  SolutionClass::ParamsType::checkInputs(solnArgs);

  SolutionClass solnExact(solnArgs);

  out = solnExact(x, y, time);
  BOOST_CHECK_CLOSE(outTrue(0), out(0), tol);
  BOOST_CHECK_CLOSE(outTrue(1), out(1), tol);
  BOOST_CHECK_CLOSE(outTrue(2), out(2), tol);
  BOOST_CHECK_CLOSE(outTrue(3), out(3), tol);
}



//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
