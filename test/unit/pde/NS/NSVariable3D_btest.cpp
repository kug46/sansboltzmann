// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// NSVariable3D_btest
// testing of NSVariable classes that represent a specific set of variables used to set the working variables

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/NS/NSVariable3D.h"
#include "pde/NS/SAVariable3D.h"

#include <string>
#include <iostream>

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( NSVariable3D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( NSVariable3D_test )
{
  const Real tol = 5.0e-12;

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  const Real Cv    = R/(gamma - 1);

  // set/eval
  Real rho, u, v, w, t, p, e, E;

  rho = 1.225;          // kg/m^3 (standard atmosphere)
  t   = 288.15;         // K (standard atmosphere)
  u   = 15.03;          // m/s
  v   =  1.71;
  w   =  10.23;
  p   = R*rho*t;
  e   = Cv*t;
  E   = e + 0.5*(u*u + v*v + w*w);

  DensityVelocityPressure3D<Real> rvp1(rho, u, v, w, p);

  BOOST_CHECK_CLOSE( rho, rvp1.Density, tol );
  BOOST_CHECK_CLOSE( u, rvp1.VelocityX, tol );
  BOOST_CHECK_CLOSE( v, rvp1.VelocityY, tol );
  BOOST_CHECK_CLOSE( w, rvp1.VelocityZ, tol );
  BOOST_CHECK_CLOSE( p, rvp1.Pressure, tol );

  DensityVelocityPressure3D<Real> rvp2 = {rho, u, v, w, p};

  BOOST_CHECK_CLOSE( rho, rvp2.Density, tol );
  BOOST_CHECK_CLOSE( u, rvp2.VelocityX, tol );
  BOOST_CHECK_CLOSE( v, rvp2.VelocityY, tol );
  BOOST_CHECK_CLOSE( w, rvp2.VelocityZ, tol );
  BOOST_CHECK_CLOSE( p, rvp2.Pressure, tol );

  PyDict drvp;
  drvp[DensityVelocityPressure3DParams::params.rho] = rho;
  drvp[DensityVelocityPressure3DParams::params.u] = u;
  drvp[DensityVelocityPressure3DParams::params.v] = v;
  drvp[DensityVelocityPressure3DParams::params.w] = w;
  drvp[DensityVelocityPressure3DParams::params.p] = p;
  DensityVelocityPressure3DParams::checkInputs(drvp);

  DensityVelocityPressure3D<Real> rvp3(drvp);

  BOOST_CHECK_CLOSE( rho, rvp3.Density, tol );
  BOOST_CHECK_CLOSE( u, rvp3.VelocityX, tol );
  BOOST_CHECK_CLOSE( v, rvp3.VelocityY, tol );
  BOOST_CHECK_CLOSE( w, rvp3.VelocityZ, tol );
  BOOST_CHECK_CLOSE( p, rvp3.Pressure, tol );

  DensityVelocityTemperature3D<Real> rvt1(rho, u, v, w, t);

  BOOST_CHECK_CLOSE( rho, rvt1.Density, tol );
  BOOST_CHECK_CLOSE( u, rvt1.VelocityX, tol );
  BOOST_CHECK_CLOSE( v, rvt1.VelocityY, tol );
  BOOST_CHECK_CLOSE( w, rvt1.VelocityZ, tol );
  BOOST_CHECK_CLOSE( t, rvt1.Temperature, tol );

  DensityVelocityTemperature3D<Real> rvt2 = {rho, u, v, w, t};

  BOOST_CHECK_CLOSE( rho, rvt2.Density, tol );
  BOOST_CHECK_CLOSE( u, rvt2.VelocityX, tol );
  BOOST_CHECK_CLOSE( v, rvt2.VelocityY, tol );
  BOOST_CHECK_CLOSE( w, rvt2.VelocityZ, tol );
  BOOST_CHECK_CLOSE( t, rvt2.Temperature, tol );

  PyDict drvt;
  drvt[DensityVelocityTemperature3DParams::params.rho] = rho;
  drvt[DensityVelocityTemperature3DParams::params.u] = u;
  drvt[DensityVelocityTemperature3DParams::params.v] = v;
  drvt[DensityVelocityTemperature3DParams::params.w] = w;
  drvt[DensityVelocityTemperature3DParams::params.t] = t;
  DensityVelocityTemperature3DParams::checkInputs(drvt);

  DensityVelocityTemperature3D<Real> rvt3(drvt);

  BOOST_CHECK_CLOSE( rho, rvt3.Density, tol );
  BOOST_CHECK_CLOSE( u, rvt3.VelocityX, tol );
  BOOST_CHECK_CLOSE( v, rvt3.VelocityY, tol );
  BOOST_CHECK_CLOSE( w, rvt3.VelocityZ, tol );
  BOOST_CHECK_CLOSE( t, rvt3.Temperature, tol );

  Conservative3D<Real> u1(rho, rho*u, rho*v, rho*w, rho*E);

  BOOST_CHECK_CLOSE( rho, u1.Density, tol );
  BOOST_CHECK_CLOSE( rho*u, u1.MomentumX, tol );
  BOOST_CHECK_CLOSE( rho*v, u1.MomentumY, tol );
  BOOST_CHECK_CLOSE( rho*w, u1.MomentumZ, tol );
  BOOST_CHECK_CLOSE( rho*E, u1.TotalEnergy, tol );

  Conservative3D<Real> u2 = {rho, rho*u, rho*v, rho*w, rho*E};

  BOOST_CHECK_CLOSE( rho, u2.Density, tol );
  BOOST_CHECK_CLOSE( rho*u, u2.MomentumX, tol );
  BOOST_CHECK_CLOSE( rho*v, u2.MomentumY, tol );
  BOOST_CHECK_CLOSE( rho*w, u2.MomentumZ, tol );
  BOOST_CHECK_CLOSE( rho*E, u2.TotalEnergy, tol );

  PyDict du;
  du[Conservative3DParams::params.rho] = rho;
  du[Conservative3DParams::params.rhou] = rho*u;
  du[Conservative3DParams::params.rhov] = rho*v;
  du[Conservative3DParams::params.rhow] = rho*w;
  du[Conservative3DParams::params.rhoE] = rho*E;
  Conservative3DParams::checkInputs(du);

  Conservative3D<Real> u3(du);

  BOOST_CHECK_CLOSE( rho, u3.Density, tol );
  BOOST_CHECK_CLOSE( rho*u, u3.MomentumX, tol );
  BOOST_CHECK_CLOSE( rho*v, u3.MomentumY, tol );
  BOOST_CHECK_CLOSE( rho*w, u3.MomentumZ, tol );
  BOOST_CHECK_CLOSE( rho*E, u3.TotalEnergy, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SAVariable3D_test )
{
  const Real tol = 5.0e-12;

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  const Real Cv    = R/(gamma - 1);

  // set/eval
  Real rho, u, v, w, t, p, e, E, nt;

  rho = 1.225;          // kg/m^3 (standard atmosphere)
  t   = 288.15;         // K (standard atmosphere)
  u   = 15.03;          // m/s
  v   =  1.71;
  w   =  10.23;
  p   = R*rho*t;
  e   = Cv*t;
  E   = e + 0.5*(u*u + v*v);
  nt  = 4.6;

  SAnt3D<DensityVelocityPressure3D<Real>> rvp1({rho, u, v, w, p, nt});

  BOOST_CHECK_CLOSE( rho, rvp1.Density, tol );
  BOOST_CHECK_CLOSE( u  , rvp1.VelocityX, tol );
  BOOST_CHECK_CLOSE( v  , rvp1.VelocityY, tol );
  BOOST_CHECK_CLOSE( w  , rvp1.VelocityZ, tol );
  BOOST_CHECK_CLOSE( p  , rvp1.Pressure, tol );
  BOOST_CHECK_CLOSE( nt , rvp1.SANutilde, tol );

  SAnt3D<DensityVelocityPressure3D<Real>> rvp2 = {rho, u, v, w, p, nt};

  BOOST_CHECK_CLOSE( rho, rvp2.Density, tol );
  BOOST_CHECK_CLOSE( u  , rvp2.VelocityX, tol );
  BOOST_CHECK_CLOSE( v  , rvp2.VelocityY, tol );
  BOOST_CHECK_CLOSE( w  , rvp1.VelocityZ, tol );
  BOOST_CHECK_CLOSE( p  , rvp2.Pressure, tol );
  BOOST_CHECK_CLOSE( nt , rvp2.SANutilde, tol );

  PyDict drvp;
  drvp[SAnt3DParams<DensityVelocityPressure3DParams>::params.rho] = rho;
  drvp[SAnt3DParams<DensityVelocityPressure3DParams>::params.u] = u;
  drvp[SAnt3DParams<DensityVelocityPressure3DParams>::params.v] = v;
  drvp[SAnt3DParams<DensityVelocityPressure3DParams>::params.w] = w;
  drvp[SAnt3DParams<DensityVelocityPressure3DParams>::params.p] = p;
  drvp[SAnt3DParams<DensityVelocityPressure3DParams>::params.nt] = nt;
  SAnt3DParams<DensityVelocityPressure3DParams>::checkInputs(drvp);

  SAnt3D<DensityVelocityPressure3D<Real>> rvp3(drvp);
  BOOST_CHECK_CLOSE( rho, rvp3.Density, tol );
  BOOST_CHECK_CLOSE( u  , rvp3.VelocityX, tol );
  BOOST_CHECK_CLOSE( v  , rvp3.VelocityY, tol );
  BOOST_CHECK_CLOSE( w  , rvp3.VelocityZ, tol );
  BOOST_CHECK_CLOSE( p  , rvp3.Pressure, tol );
  BOOST_CHECK_CLOSE( nt , rvp3.SANutilde, tol );

  SAnt3D<DensityVelocityTemperature3D<Real>> rvt1({rho, u, v, w, t, nt});
  BOOST_CHECK_CLOSE( rho, rvt1.Density, tol );
  BOOST_CHECK_CLOSE( u  , rvt1.VelocityX, tol );
  BOOST_CHECK_CLOSE( v  , rvt1.VelocityY, tol );
  BOOST_CHECK_CLOSE( w  , rvt1.VelocityZ, tol );
  BOOST_CHECK_CLOSE( t  , rvt1.Temperature, tol );
  BOOST_CHECK_CLOSE( nt , rvt1.SANutilde, tol );

  SAnt3D<DensityVelocityTemperature3D<Real>> rvt2 = {rho, u, v, w, t,nt};

  BOOST_CHECK_CLOSE( rho, rvt2.Density, tol );
  BOOST_CHECK_CLOSE( u  , rvt2.VelocityX, tol );
  BOOST_CHECK_CLOSE( v  , rvt2.VelocityY, tol );
  BOOST_CHECK_CLOSE( w  , rvt2.VelocityZ, tol );
  BOOST_CHECK_CLOSE( t  , rvt2.Temperature, tol );
  BOOST_CHECK_CLOSE( nt , rvt2.SANutilde, tol );

  PyDict drvt;
  drvt[SAnt3DParams<DensityVelocityTemperature3DParams>::params.rho] = rho;
  drvt[SAnt3DParams<DensityVelocityTemperature3DParams>::params.u] = u;
  drvt[SAnt3DParams<DensityVelocityTemperature3DParams>::params.v] = v;
  drvt[SAnt3DParams<DensityVelocityTemperature3DParams>::params.w] = w;
  drvt[SAnt3DParams<DensityVelocityTemperature3DParams>::params.t] = t;
  drvt[SAnt3DParams<DensityVelocityTemperature3DParams>::params.nt] = nt;
  SAnt3DParams<DensityVelocityTemperature3DParams>::checkInputs(drvt);

  SAnt3D<DensityVelocityTemperature3D<Real>> rvt3(drvt);
  BOOST_CHECK_CLOSE( rho, rvt3.Density, tol );
  BOOST_CHECK_CLOSE( u  , rvt3.VelocityX, tol );
  BOOST_CHECK_CLOSE( v  , rvt3.VelocityY, tol );
  BOOST_CHECK_CLOSE( w  , rvt3.VelocityZ, tol );
  BOOST_CHECK_CLOSE( t  , rvt3.Temperature, tol );
  BOOST_CHECK_CLOSE( nt , rvt3.SANutilde, tol );

  SAnt3D<Conservative3D<Real>> u1({rho, rho*u, rho*v, rho*w, rho*E, rho*nt});
  BOOST_CHECK_CLOSE( rho   , u1.Density, tol );
  BOOST_CHECK_CLOSE( rho*u , u1.MomentumX, tol );
  BOOST_CHECK_CLOSE( rho*v , u1.MomentumY, tol );
  BOOST_CHECK_CLOSE( rho*w , u1.MomentumZ, tol );
  BOOST_CHECK_CLOSE( rho*E , u1.TotalEnergy, tol );
  BOOST_CHECK_CLOSE( rho*nt, u1.rhoSANutilde, tol );

  SAnt3D<Conservative3D<Real>> u2 = {rho, rho*u, rho*v, rho*w, rho*E, rho*nt};
  BOOST_CHECK_CLOSE( rho   , u2.Density, tol );
  BOOST_CHECK_CLOSE( rho*u , u2.MomentumX, tol );
  BOOST_CHECK_CLOSE( rho*v , u2.MomentumY, tol );
  BOOST_CHECK_CLOSE( rho*w , u2.MomentumZ, tol );
  BOOST_CHECK_CLOSE( rho*E , u2.TotalEnergy, tol );
  BOOST_CHECK_CLOSE( rho*nt, u2.rhoSANutilde, tol );

  PyDict du;
  du[SAnt3DParams<Conservative3DParams>::params.rho] = rho;
  du[SAnt3DParams<Conservative3DParams>::params.rhou] = rho*u;
  du[SAnt3DParams<Conservative3DParams>::params.rhov] = rho*v;
  du[SAnt3DParams<Conservative3DParams>::params.rhow] = rho*w;
  du[SAnt3DParams<Conservative3DParams>::params.rhoE] = rho*E;
  du[SAnt3DParams<Conservative3DParams>::params.rhont] = rho*nt;
  SAnt3DParams<Conservative3DParams>::checkInputs(du);

  SAnt3D<Conservative3D<Real>> u3(du);
  BOOST_CHECK_CLOSE( rho   , u3.Density, tol );
  BOOST_CHECK_CLOSE( rho*u , u3.MomentumX, tol );
  BOOST_CHECK_CLOSE( rho*v , u3.MomentumY, tol );
  BOOST_CHECK_CLOSE( rho*w , u3.MomentumZ, tol );
  BOOST_CHECK_CLOSE( rho*E , u3.TotalEnergy, tol );
  BOOST_CHECK_CLOSE( rho*nt, u3.rhoSANutilde, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
