// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCEulermitAVSensor1D
//
// test of 1-D artificial viscosity BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D.h"
#include "pde/BCParameters.h"

#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/Fluids1D_Sensor.h"
#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/BCEulermitAVSensor1D.h"

#include "pde/ArtificialViscosity/AVVariable.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_Source1D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor1D.h"

#define AVSENSOR1D_INSTANTIATE
#include "pde/ArtificialViscosity/AVSensor_Source1D_impl.h"

#define BCPARAMETERS_INSTANTIATE // included because testing without NDConvert
#include "pde/BCParameters_impl.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{

typedef QTypePrimitiveRhoPressure QType;
typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEBase;
typedef BCEuler1D<BCTypeFullState_mitState, PDEBase> BCBaseEuler;
typedef BCEuler1DFullStateParams<NSVariableType1DParams> BCBaseEulerParams;


typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
typedef AVSensor_ViscousFlux1D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
typedef Fluids_Sensor<PhysD1, PDEBaseClass> Sensor;
typedef AVSensor_Source1D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
typedef PDEmitAVSensor1D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> PDEClass;

template class BCEuler1D<BCTypeFullState_mitState, PDEClass>;


// Instantiate BCParameters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters<BCEulermitAVSensor1DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV>>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( BCEulermitAVSensor1D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBase;

  {
    typedef BCNone<PhysD1,TraitsSizeEuler<PhysD1>::N> BCClass;

    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::NBC == 0 );
  }

  {
    typedef BCEuler1D<BCTypeReflect_mitState, PDEBase> BCBaseEuler;
    typedef BCmitAVSensor1D<BCTypeReflect_mitState,BCBaseEuler> BCClass;

    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::N == 4 );
    BOOST_CHECK( BCClass::NBC == 1 );
  }

  {
    typedef BCEuler1D<BCTypeFullState_mitState, PDEBase> BCBaseEuler;
    typedef BCmitAVSensor1D<BCTypeFullState_mitState,BCBaseEuler> BCClass;

    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::N == 4 );
    BOOST_CHECK( BCClass::NBC == 4 );
  }

  {
    typedef BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDEBase> BCBaseEuler;
    typedef BCmitAVSensor1D<BCTypeDirichlet_mitState,BCBaseEuler> BCClass;

    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::N == 4 );
    BOOST_CHECK( BCClass::NBC == 3 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBase;

  typedef BCParameters< BCEulermitAVSensor1DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> > BCParams;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool hasSpaceTimeDiffusion = false;
  PDEBase pde(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEBase::Euler_ResidInterp_Raw);

  {
    typedef BCNone<PhysD1,TraitsSizeEuler<PhysD1>::N> BCClass;

    BCClass bc1;

    //Pydict constructor
    PyDict BCNone;
    BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

    PyDict BCList;
    BCList["BC1"] = BCNone;
    BCParams::checkInputs(BCList);

    BCClass bc2(pde, BCNone);
  }

  {
    typedef BCEuler1D<BCTypeReflect_mitState, PDEBase> BCBaseEuler;
    typedef BCmitAVSensor1D<BCTypeReflect_mitState, BCBaseEuler> BCClass;

    BCClass bc1(pde);

    //Pydict constructor
    PyDict BCReflect;
    BCReflect[BCParams::params.BC.BCType] = BCParams::params.BC.Reflect_mitState;

    PyDict BCList;
    BCList["BC1"] = BCReflect;
    BCParams::checkInputs(BCList);

    BCClass bc2(pde, BCReflect);
  }

  {
    typedef BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDEBase> BCBaseEuler;
    typedef BCmitAVSensor1D<BCTypeDirichlet_mitState, BCBaseEuler> BCClass;

    Real TtSpec = 20.5;
    Real PtSpec = 71.4;
    Real sSpec = 0.2;
    BCClass bc1(sSpec, pde, TtSpec, PtSpec);

    //Pydict constructor
    PyDict BCInflowSubsonic;
    BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTt_mitState;
    BCInflowSubsonic[BCEuler1DParams<BCTypeInflowSubsonic_PtTt_mitState>::params.TtSpec] = TtSpec;
    BCInflowSubsonic[BCEuler1DParams<BCTypeInflowSubsonic_PtTt_mitState>::params.PtSpec] = PtSpec;

    PyDict BCList;
    BCList["BC1"] = BCInflowSubsonic;
    BCParams::checkInputs(BCList);

    BCClass bc2(pde, BCInflowSubsonic);
  }

  {
    typedef BCEuler1D<BCTypeFullState_mitState, PDEBase> BCBaseEuler;
    typedef BCmitAVSensor1D<BCTypeFullState_mitState, BCBaseEuler> BCClass;

    bool upwind = true;
    Real sB = 0.2;
    BCClass bc1(sB, pde, DensityVelocityPressure1D<Real>(1.1, 1.3, 5.45), upwind);

    PyDict d;
    d[NSVariableType1DParams::params.StateVector.Variables] = NSVariableType1DParams::params.StateVector.PrimitivePressure;
    d[DensityVelocityPressure1DParams::params.rho] = 1.1;
    d[DensityVelocityPressure1DParams::params.u] = 1.3;
    d[DensityVelocityPressure1DParams::params.p] = 5.45;

    //Pydict constructor
    PyDict BCFullState;
    BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
    BCFullState[BCClass::ParamsType::params.Characteristic] = false;
    BCFullState[BCClass::ParamsType::params.StateVector] = d;
    //BCFullState[BCClass::ParamsType::params.sensor] = sB;

    PyDict BCList;
    BCList["BC1"] = BCFullState;
    BCParams::checkInputs(BCList);

    BCClass bc2(pde, BCFullState);
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCNone_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBase;

//  typedef BCParameters< BCEulermitAVSensor1DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> > BCParams;

  typedef BCNone<PhysD1,TraitsSizeEuler<PhysD1>::N> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool hasSpaceTimeDiffusion = false;
  PDEBase pde(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEBase::Euler_ResidInterp_Raw);

  BCClass bc;

  Real nx = 1.0;
  ArrayQ q = 0.1;
  BOOST_CHECK_EQUAL( true, bc.isValidState( nx, q ));
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeReflect_mitState_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBase;

  typedef BCParameters< BCEulermitAVSensor1DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> > BCParams;

  typedef BCEuler1D<BCTypeReflect_mitState, PDEBase> BCBaseEuler;
  typedef BCmitAVSensor1D<BCTypeReflect_mitState, BCBaseEuler> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool hasSpaceTimeDiffusion = false;
  PDEBase pde(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEBase::Euler_ResidInterp_Raw);

  //Pydict constructor
  PyDict BCReflect;
  BCReflect[BCParams::params.BC.BCType] = BCParams::params.BC.Reflect_mitState;

  PyDict BCList;
  BCList["BC1"] = BCReflect;
  BCParams::checkInputs(BCList);

  BCClass bc(pde, BCReflect);

  Real x = 0, time = 0;   // not actually used in functions
  Real nx = -1.0;   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, t = 0.987;
  Real p  = R*rho*t;
  Real qn = nx*u;
  Real param = 0.1;

  Real uBTrue = u - qn*nx;

  Real pBTrue = p;
  Real sBTrue = 0.265;

  ArrayQ qI;
  DensityVelocityPressure1D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.Pressure  = p;
  pde.setDOFFrom( qI, qdata );
  qI(3) = sBTrue;

  ArrayQ qB;
  bc.state( param, x, time, nx, qI, qB );

  Real rhoB, uB, tB;
  pde.variableInterpreter().eval( qB, rhoB, uB, tB );
  Real pB = gas.pressure(rhoB, tB);

  SANS_CHECK_CLOSE(    rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE( uBTrue,   uB, small_tol, close_tol );
  SANS_CHECK_CLOSE( pBTrue,   pB, small_tol, close_tol );
  SANS_CHECK_CLOSE( sBTrue,qB(3), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFullState_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBase;

  typedef BCParameters< BCEulermitAVSensor1DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> > BCParams;

  typedef BCEuler1D<BCTypeFullState_mitState, PDEBase> BCBaseEuler;
  typedef BCmitAVSensor1D<BCTypeFullState_mitState, BCBaseEuler> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool hasSpaceTimeDiffusion = false;
  PDEBase pde(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEBase::Euler_ResidInterp_Raw);

  Real x = 0, time = 0;   // not actually used in functions
  Real nx = 1.0;   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, t = 0.987;
  Real p  = R*rho*t;
  Real param = 0.1;

  PyDict d;
  d[NSVariableType1DParams::params.StateVector.Variables] = NSVariableType1DParams::params.StateVector.PrimitivePressure;
  d[DensityVelocityPressure1DParams::params.rho] = rho;
  d[DensityVelocityPressure1DParams::params.u] = u;
  d[DensityVelocityPressure1DParams::params.p] = p;

  //Pydict constructor
  PyDict BCFullState;
  BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFullState[BCClass::ParamsType::params.Characteristic] = false;
  BCFullState[BCClass::ParamsType::params.StateVector] = d;

  PyDict BCList;
  BCList["BC1"] = BCFullState;
  BCParams::checkInputs(BCList);

  BCClass bc(pde, BCFullState);

  ArrayQ qI = 0;
  ArrayQ qB;
  bc.state( param, x, time, nx, qI, qB );

  Real rhoB, uB, tB;
  pde.variableInterpreter().eval( qB, rhoB, uB, tB );
  Real pB = gas.pressure(rhoB, tB);

  SANS_CHECK_CLOSE(rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB, small_tol, close_tol );
  SANS_CHECK_CLOSE(qI(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE(p, pB, small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeInflowSubsonic_PtTt_mitState_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBase;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBase, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef BCParameters< BCEulermitAVSensor1DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> > BCParams;

  typedef BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, AVPDEClass> BCBaseEuler;
  typedef BCmitAVSensor1D<BCTypeDirichlet_mitState, BCBaseEuler> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  Real us = 1.0;
  Real ksxx = 0.8;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us);
  SensorViscousFlux visc(ksxx);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass pde(adv, visc, source, isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                 gas, PDEBase::Euler_ResidInterp_Raw);
//  PDEBase pde(order, gas, PDEBase::Euler_ResidInterp_Raw);

  Real x = 0, time = 0;   // not actually used in functions
  Real nx = -1.0;   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, t = 0.987;
  Real p  = R*rho*t;
  Real s = 0.167;
  Real param = 0.1;

  Real TtSpec = t*(1+0.2*0.1*0.1);
  Real PtSpec = p*pow( (1+0.2*0.1*0.1), 3.5 );
  //Real sSpec = 0.265;

  //Pydict constructor
  PyDict BCInflowSubsonic;
  BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTt_mitState;
  BCInflowSubsonic[BCEuler1DParams<BCTypeInflowSubsonic_PtTt_mitState>::params.TtSpec] = TtSpec;
  BCInflowSubsonic[BCEuler1DParams<BCTypeInflowSubsonic_PtTt_mitState>::params.PtSpec] = PtSpec;
  //BCInflowSubsonic[BCClass::ParamsType::params.sensor] = sSpec;

  PyDict BCList;
  BCList["BC1"] = BCInflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc(pde, BCInflowSubsonic);

  ArrayQ qI;
//  DensityVelocityPressure1D<Real> qdata(rho, u, v, p);
  AVVariable<DensityVelocityPressure1D, Real> qdata = {{rho, u, p}, s};
  pde.setDOFFrom( qI, qdata );

  ArrayQ qB = 0;
  bc.state( param, x, time, nx, qI, qB );

  Real rhoB, uB, tB;
  pde.variableInterpreter().eval( qB, rhoB, uB, tB );

  Real rhoTrue = 0.84038917838672367;
  Real uTrue = 0.56592542899297926;
  Real tTrue = 0.87459128886396864;

  SANS_CHECK_CLOSE(rhoTrue, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(uTrue, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(tTrue, tB, small_tol, close_tol );
  SANS_CHECK_CLOSE(qI(3), qB(3), small_tol, close_tol );

  Real rhox = 0.01;
  Real ux = 0.049;
  Real px = 0.002;
  Real sx = 0.325;
  ArrayQ qx = {rhox, ux, px, sx};

  ArrayQ Fn = 0;
  bc.fluxNormal( param, x, time, nx, qI, qx, qB, Fn);

  ArrayQ fx = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(param, x, time, qB, fx);
  FnTrue += fx*nx;

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
