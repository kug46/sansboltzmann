// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCEuler1D_btest
//
// test of quasi 1-D compressible Euler BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/PDEEuler1D.h"
#include "pde/NS/BCEuler1D.h"
#include "pde/BCParameters.h"
#include "pde/BCCategory.h"

#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
class QTypeEntropy {};

template class BCEuler1D<BCTypeTimeIC,
  PDEEuler1D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;

template class BCEuler1D< BCTypeReflect,
  PDEEuler1D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;

template class BCEuler1D< BCTypeInflowSupersonic,
  PDEEuler1D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;

template class BCEuler1D< BCTypeOutflowSubsonic_Pressure,
  PDEEuler1D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;

template class BCEuler1D < BCTypeFunction_mitState,
  PDEEuler1D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;

template class BCEuler1D< BCTypeOutflowSubsonic_Pressure_mitState,
  PDEEuler1D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;

template class BCEuler1D< BCTypeFullState_mitState,
  PDEEuler1D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;

template class BCEuler1D< BCTypeInflowSubsonic_PtTt_mitState,
  PDEEuler1D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;

template struct SpaceTimeBC< BCEuler1D<BCTypeFullStateSpaceTime_mitState ,
  PDEEuler1D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> > >;

template struct SpaceTimeBC< BCEuler1D<BCTypeOutflowSupersonic_Pressure_mitState,
  PDEEuler1D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> > >;

template struct SpaceTimeBC< BCEuler1D<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState,
  PDEEuler1D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> > >;

template struct SpaceTimeBC< BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState,
  PDEEuler1D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> > >;

template struct SpaceTimeBC< BCEuler1D<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState,
  PDEEuler1D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> > >;

template class BCEuler1D<BCTypeTimeIC_Function,
  PDEEuler1D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;

// Instantiate BCParamaters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters< BCEuler1DVector<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BCEuler1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  {
  typedef BCEuler1D<BCTypeTimeIC, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 1 );
  BOOST_CHECK( BCClass::N == 3 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 3 );
  BOOST_CHECK( MatrixQ::M == 3 );
  BOOST_CHECK( MatrixQ::N == 3 );
  }

  {
  typedef BCEuler1D<BCTypeReflect, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 1 );
  BOOST_CHECK( BCClass::N == 3 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( ArrayQ::M == 3 );
  BOOST_CHECK( MatrixQ::M == 3 );
  BOOST_CHECK( MatrixQ::N == 3 );
  }

  {
  typedef BCEuler1D<BCTypeInflowSupersonic, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 1 );
  BOOST_CHECK( BCClass::N == 3 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 3 );
  BOOST_CHECK( MatrixQ::M == 3 );
  BOOST_CHECK( MatrixQ::N == 3 );
  }

  {
  typedef BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 1 );
  BOOST_CHECK( BCClass::N == 3 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( ArrayQ::M == 3 );
  BOOST_CHECK( MatrixQ::M == 3 );
  BOOST_CHECK( MatrixQ::N == 3 );
  }

  {
  typedef BCEuler1D<BCTypeFunction_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 1 );
  BOOST_CHECK( BCClass::N == 3 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 3 );
  BOOST_CHECK( MatrixQ::M == 3 );
  BOOST_CHECK( MatrixQ::N == 3 );
  }

  {
  typedef BCEuler1D<BCTypeFullState_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 1 );
  BOOST_CHECK( BCClass::N == 3 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 3 );
  BOOST_CHECK( MatrixQ::M == 3 );
  BOOST_CHECK( MatrixQ::N == 3 );
  }

  {
  typedef BCEuler1D<BCTypeOutflowSubsonic_Pressure_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 1 );
  BOOST_CHECK( BCClass::N == 3 );
  BOOST_CHECK( BCClass::NBC == 2 );
  BOOST_CHECK( ArrayQ::M == 3 );
  BOOST_CHECK( MatrixQ::M == 3 );
  BOOST_CHECK( MatrixQ::N == 3 );
  }

  {
  typedef BCEuler1D<BCTypeOutflowSupersonic_Pressure_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 1 );
  BOOST_CHECK( BCClass::N == 3 );
  BOOST_CHECK( BCClass::NBC == 2 );
  BOOST_CHECK( ArrayQ::M == 3 );
  BOOST_CHECK( MatrixQ::M == 3 );
  BOOST_CHECK( MatrixQ::N == 3 );
  }

  {
  typedef BCEuler1D<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 1 );
  BOOST_CHECK( BCClass::N == 3 );
  BOOST_CHECK( BCClass::NBC == 2 );
  BOOST_CHECK( ArrayQ::M == 3 );
  BOOST_CHECK( MatrixQ::M == 3 );
  BOOST_CHECK( MatrixQ::N == 3 );
  }

  {
  typedef BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 1 );
  BOOST_CHECK( BCClass::N == 3 );
  BOOST_CHECK( BCClass::NBC == 2 );
  BOOST_CHECK( ArrayQ::M == 3 );
  BOOST_CHECK( MatrixQ::M == 3 );
  BOOST_CHECK( MatrixQ::N == 3 );
  }

  {
    typedef BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDEClass> BCClass;
    typedef BCClass::ArrayQ<Real> ArrayQ;
    typedef BCClass::MatrixQ<Real> MatrixQ;

    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::N == 3 );
    BOOST_CHECK( BCClass::NBC == 2 );
    BOOST_CHECK( ArrayQ::M == 3 );
    BOOST_CHECK( MatrixQ::M == 3 );
    BOOST_CHECK( MatrixQ::N == 3 );
  }
  {
    typedef SpaceTimeBC< BCEuler1D<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState, PDEClass>> BCClass;
    typedef BCClass::ArrayQ<Real> ArrayQ;
    typedef BCClass::MatrixQ<Real> MatrixQ;

    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::N == 3 );
    BOOST_CHECK( BCClass::NBC == 2 );
    BOOST_CHECK( ArrayQ::M == 3 );
    BOOST_CHECK( MatrixQ::M == 3 );
    BOOST_CHECK( MatrixQ::N == 3 );
  }

  {
  typedef BCEuler1D<BCTypeTimeIC_Function, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 1 );
  BOOST_CHECK( BCClass::N == 3 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 3 );
  BOOST_CHECK( MatrixQ::M == 3 );
  BOOST_CHECK( MatrixQ::N == 3 );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctorND )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef BCParameters< BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  {
  typedef BCNone<PhysD1,TraitsSizeEuler<PhysD1>::N> BCClass;

  BCClass bc1;

  //Pydict constructor
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BCList;
  BCList["BC1"] = BCNone;
  BCParams::checkInputs(BCList);


  std::map< std::string, std::shared_ptr<BCBase> > BCs =
      BCParams::template createBCs<BCNDConvertSpace>(pde, BCList);

  }

  {
  typedef BCNone<PhysD1,TraitsSizeEuler<PhysD1>::N> BCClass;

  BCClass bc1;

  //Pydict constructor
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BCList;
  BCList["BC1"] = BCNone;
  BCParams::checkInputs(BCList);


  std::map< std::string, std::shared_ptr<BCBase> > BCs =
      BCParams::template createBCs<BCNDConvertSpaceTime>(pde, BCList);

  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  {
  typedef BCEuler1D<BCTypeTimeIC, PDEClass> BCClass;
  typedef BCParameters< BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;

  BCClass bc(pde, DensityVelocityPressure1D<Real>(1.1, 1.3, 5.45));

  PyDict d;
  d[NSVariableType1DParams::params.StateVector.Variables] = NSVariableType1DParams::params.StateVector.PrimitivePressure;
  d[DensityVelocityPressure1DParams::params.rho] = 1.1;
  d[DensityVelocityPressure1DParams::params.u] = 1.3;
  d[DensityVelocityPressure1DParams::params.p] = 5.45;

  PyDict BCTimeIC;
  BCTimeIC[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC;
  BCTimeIC[BCClass::ParamsType::params.StateVector] = d;

  PyDict BCList;
  BCList["BC1"] = BCTimeIC;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCTimeIC);
  }

//  {
//  typedef BCEuler1D<BCTypeReflect, PDEClass> BCClass;
//  typedef BCClass::ArrayQ<Real> ArrayQ;
//
//  typedef BCParameters< BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;
//
//  const ArrayQ bcdata = {0, 0, 0};
//  BCClass bc1(pde, bcdata);
//
//  //Pydict constructor
//  PyDict BCReflect;
//  BCReflect[BCParams::params.BC.BCType] = BCParams::params.BC.Reflect;
//
//  PyDict BCList;
//  BCList["BC1"] = BCReflect;
//  BCParams::checkInputs(BCList);
//
//  BCClass bc2(pde, BCReflect);
//  }

//  {
//  typedef BCEuler1D<BCTypeInflowSupersonic, PDEClass> BCClass;
//  typedef BCClass::ArrayQ<Real> ArrayQ;
//
//  typedef BCParameters< BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;
//
//  const ArrayQ bcdata = {0, 0, 0};
//  BCClass bc1(pde, bcdata);
//
//  PyDict BCInflowSupersonic;
//  BCInflowSupersonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSupersonic;
//  BCInflowSupersonic[BCEuler1DParams<BCTypeInflowSupersonic>::params.rho] = 1.0;
//  BCInflowSupersonic[BCEuler1DParams<BCTypeInflowSupersonic>::params.rhou] = 1.0;
//  BCInflowSupersonic[BCEuler1DParams<BCTypeInflowSupersonic>::params.rhoE] = 1.0;
//
//  PyDict BCList;
//  BCList["BC1"] = BCInflowSupersonic;
//  BCParams::checkInputs(BCList);
//
//  BCClass bc2(pde, BCInflowSupersonic);
//  }

//  {
//  typedef BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEClass> BCClass;
//  typedef BCClass::ArrayQ<Real> ArrayQ;
//
//  typedef BCParameters< BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;
//
//  const ArrayQ bcdata = {0, 0, 0};
//  BCClass bc1(pde, bcdata);
//
//  PyDict BCOutflowSubsonic;
//  BCOutflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure;
//  BCOutflowSubsonic[BCEuler1DParams<BCTypeOutflowSubsonic_Pressure>::params.pSpec] = 1.0;
//
//  PyDict BCList;
//  BCList["BC1"] = BCOutflowSubsonic;
//  BCParams::checkInputs(BCList);
//
//  BCClass bc2(pde, BCOutflowSubsonic);
//  }

  {
  typedef BCEuler1D<BCTypeFunction_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  typedef BCParameters< BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;
  typedef SolutionFunction_Euler1D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionClass;

  const ArrayQ bcdata = {0, 0, 0};
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  BCClass::Function_ptr constsol( new SolutionClass(gas, 1.0, 1.0, 1.0) );
  BCClass bc(pde, constsol, true);

  PyDict Function;
  Function[BCClass::ParamsType::params.Function.Name] = BCClass::ParamsType::params.Function.Const;
  Function[SolutionClass::ParamsType::params.gasModel] = gasModelDict;
  Function[SolutionClass::ParamsType::params.rho] = 1.0;
  Function[SolutionClass::ParamsType::params.u] = 1.0;
  Function[SolutionClass::ParamsType::params.p] = 1.0;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCIn[BCClass::ParamsType::params.Function] = Function;
  BCIn[BCClass::ParamsType::params.Characteristic] = true;

  PyDict BCList;
  BCList["BC1"] = BCIn;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCIn);
  }

  {
  typedef BCEuler1D<BCTypeFullState_mitState, PDEClass> BCClass;
  typedef BCParameters< BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;

  bool upwind = true;
  BCClass bc(pde, DensityVelocityPressure1D<Real>(1.1, 1.3, 5.45), upwind);

  PyDict d;
  d[NSVariableType1DParams::params.StateVector.Variables] = NSVariableType1DParams::params.StateVector.PrimitivePressure;
  d[DensityVelocityPressure1DParams::params.rho] = 1.1;
  d[DensityVelocityPressure1DParams::params.u] = 1.3;
  d[DensityVelocityPressure1DParams::params.p] = 5.45;

  PyDict BCFullState;
  BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFullState[BCClass::ParamsType::params.Characteristic] = false;
  BCFullState[BCClass::ParamsType::params.StateVector] = d;

  PyDict BCList;
  BCList["BC1"] = BCFullState;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCFullState);
  }

  {
  typedef BCEuler1D<BCTypeOutflowSubsonic_Pressure_mitState, PDEClass> BCClass;
  typedef BCParameters< BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;

  const Real pSpec = 2.1;
  BCClass bc(pde, pSpec);

  PyDict BCOutflowSubsonic;
  BCOutflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOutflowSubsonic[BCEuler1DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pSpec;

  PyDict BCList;
  BCList["BC1"] = BCOutflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCOutflowSubsonic);
  }

  {
  typedef BCEuler1D<BCTypeOutflowSupersonic_Pressure_mitState, PDEClass> BCClass;
  typedef BCParameters< BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;

  const Real pSpec = 2.1;
  BCClass bc(pde, pSpec);

  PyDict BCOutflowSupersonic;
  BCOutflowSupersonic[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSupersonic_Pressure_mitState;
  BCOutflowSupersonic[BCEuler1DParams<BCTypeOutflowSupersonic_Pressure_mitState>::params.pSpec] = pSpec;

  PyDict BCList;
  BCList["BC1"] = BCOutflowSupersonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCOutflowSupersonic);
  }


  {
  typedef BCEuler1D<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState, PDEClass> BCClass;
  typedef BCParameters< BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;

  const Real pSpec = 2.1;
  BCClass bc(pde, pSpec);

  PyDict BCOutflowSupersonic;
  BCOutflowSupersonic[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSupersonic_Pressure_SpaceTime_mitState;
  BCOutflowSupersonic[BCEuler1DParams<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState>::params.pSpec] = pSpec;

  PyDict BCList;
  BCList["BC1"] = BCOutflowSupersonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCOutflowSupersonic);
  }

  {
    typedef BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDEClass> BCClass;
    typedef BCParameters< BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;

    Real TtSpec = 20.5;
    Real PtSpec = 71.4;
    BCClass bc1(pde, TtSpec, PtSpec);

    PyDict BCInflowSubsonic;
    //BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTt_mitState;
    BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTt_mitState;
    BCInflowSubsonic[BCEuler1DParams<BCTypeInflowSubsonic_PtTt_mitState>::params.TtSpec] = TtSpec;
    BCInflowSubsonic[BCEuler1DParams<BCTypeInflowSubsonic_PtTt_mitState>::params.PtSpec] = PtSpec;

    PyDict BCList;
    BCList["BC1"] = BCInflowSubsonic;
    BCParams::checkInputs(BCList);

    BCClass bc2(pde, BCInflowSubsonic);
  }

  {
    typedef BCEuler1D<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState, PDEClass> BCClass;
    typedef BCParameters< BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;

    Real TtSpec = 20.5;
    Real PtSpec = 71.4;
    BCClass bc1(pde, TtSpec, PtSpec);

    PyDict BCInflowSubsonic;
    //BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTt_mitState;
    BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTt_SpaceTime_mitState;
    BCInflowSubsonic[BCEuler1DParams<BCTypeInflowSubsonic_PtTt_mitState>::params.TtSpec] = TtSpec;
    BCInflowSubsonic[BCEuler1DParams<BCTypeInflowSubsonic_PtTt_mitState>::params.PtSpec] = PtSpec;

    PyDict BCList;
    BCList["BC1"] = BCInflowSubsonic;
    BCParams::checkInputs(BCList);

    BCClass bc2(pde, BCInflowSubsonic);
  }

  {
  typedef BCEuler1D<BCTypeTimeIC_Function, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  typedef BCParameters< BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;
  typedef SolutionFunction_Euler1D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionClass;

  const ArrayQ bcdata = {0, 0, 0};
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  BCClass::Function_ptr constsol( new SolutionClass(gas, 1.0, 1.0, 1.0) );
  BCClass bc(pde, constsol);

  PyDict Function;
  Function[BCClass::ParamsType::params.Function.Name] = BCClass::ParamsType::params.Function.Const;
  Function[SolutionClass::ParamsType::params.gasModel] = gasModelDict;
  Function[SolutionClass::ParamsType::params.rho] = 1.0;
  Function[SolutionClass::ParamsType::params.u] = 1.0;
  Function[SolutionClass::ParamsType::params.p] = 1.0;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC_Function;
  BCIn[BCClass::ParamsType::params.Function] = Function;

  PyDict BCList;
  BCList["BC1"] = BCIn;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCIn);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_timeic )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler1D<BCTypeTimeIC, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  Real x, time;
  Real nx, nt;
  Real rho, u, t, p;

  x = time = 0;   // not actually used in functions
  nx = 0.000;   // Note: magnitude = 1
  nt = 1.000;
  rho = 1.137; u = 0.784; t = 0.987;
  p  = R*rho*t;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  ArrayQ q;
  DensityVelocityPressure1D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  BCClass bc(pde, qdata);
  BCClass bc2(pde, qdata);

  // static tests
  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 3 );
  BOOST_CHECK( bc.NBC == 3 );

  ArrayQ qI = 0;
  ArrayQ qB, qB2;

  bc.state(x, time, nx, nt, qI, qB);

  Real rhoB, uB, tB;
  pde.variableInterpreter().eval(qB, rhoB, uB, tB);

  SANS_CHECK_CLOSE(rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB, small_tol, close_tol );

  bc.state(param, x, time, nx, nt, qI, qB2);

  Real rhoB2, uB2, tB2;
  pde.variableInterpreter().eval(qB2, rhoB2, uB2, tB2);

  SANS_CHECK_CLOSE(rho, rhoB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB2, small_tol, close_tol );

  qI = pde.setDOFFrom( DensityVelocityPressure1D<Real>(rhoB, uB, tB) );
  BOOST_CHECK_EQUAL( true, bc.isValidState(nx, nt, qI) );
  BOOST_CHECK_EQUAL( true, bc2.isValidState(nx, nt, qI) );
}


#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_reflect )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef BCEuler1D<BCTypeReflect, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  const ArrayQ bcdata = {0, 0, 0};
  BCClass bc(pde, bcdata);

  // static tests
  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 3 );
  BOOST_CHECK( bc.NBC == 1 );

  Real x, time;
  Real nx;
  Real rho, u, t, p, E, H, qn;
  Real Cv, Cp;

  x = time = 0;   // not actually used in functions
  nx = -1.0;
  rho = 1.137; u = 0.784; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  E  = Cv*t + 0.5*(u*u);
  H  = Cp*t + 0.5*(u*u);
  qn = nx*u;

  ArrayQ q;
  DensityVelocityPressure1D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  ArrayQ qx = 0;

  ArrayQ lg = {1.551, -0.237, 3.119};

  // strong-form BC
  ArrayQ rsdBCTrue = {qn - bcdata(0), lg(1) - rho*E, lg(2) - rho};
  ArrayQ rsdBC = 0;
  bc.strongBC( x, time, nx, q, qx, lg, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(2), rsdBC(2), small_tol, close_tol );

  rsdBC = 0;
  bc.strongBC( x, time, nx, q, qx, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,            rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,            rsdBC(2), small_tol, close_tol );

  // BC weight
  Real wghtBCdata[9] = {rho            , 0, 0,
                        rho*(u + nx*qn), 0, 0,
                        rho*(H + qn*qn), 0, 0};
  MatrixQ wghtBCTrue( wghtBCdata, 9 );
  MatrixQ wghtBC = 0;
  bc.weightBC( x, time, nx, q, qx, wghtBC );
  SANS_CHECK_CLOSE( wghtBCTrue(0,0), wghtBC(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,1), wghtBC(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,2), wghtBC(0,2), small_tol, close_tol );

  SANS_CHECK_CLOSE( wghtBCTrue(1,0), wghtBC(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,1), wghtBC(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,2), wghtBC(1,2), small_tol, close_tol );

  SANS_CHECK_CLOSE( wghtBCTrue(2,0), wghtBC(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,1), wghtBC(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,2), wghtBC(2,2), small_tol, close_tol );

  // Lagrange multiplier weight
  Real wghtLGdata[9] = { 0.0   , 0.0, 0.0,
                        -0.40  , 0.0, 0.0,
                        -1.0976, 0.0, 0.0};
  MatrixQ wghtLGTrue( wghtLGdata, 9 );
  MatrixQ wghtLG = 0;
  bc.weightLagrange( x, time, nx, q, qx, wghtLG );
  SANS_CHECK_CLOSE( wghtLGTrue(0,0), wghtLG(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,1), wghtLG(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,2), wghtLG(0,2), small_tol, close_tol );

  SANS_CHECK_CLOSE( wghtLGTrue(1,0), wghtLG(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,1), wghtLG(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,2), wghtLG(1,2), small_tol, close_tol );

  SANS_CHECK_CLOSE( wghtLGTrue(2,0), wghtLG(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,1), wghtLG(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,2), wghtLG(2,2), small_tol, close_tol );

  // Lagrange rhs
  ArrayQ rhsLGTrue = {rho*E, 0, 0};
  ArrayQ rhsLG = 0;
  bc.rhsLagrange( x, time, nx, q, qx, rhsLG );
  SANS_CHECK_CLOSE( rhsLGTrue(0), rhsLG(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rhsLGTrue(1), rhsLG(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rhsLGTrue(2), rhsLG(2), small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_inflow_supersonic )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef BCEuler1D<BCTypeInflowSupersonic, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  const ArrayQ bcdata = {7, 5.447, 9.443};
  BCClass bc(pde, bcdata);

  // static tests
  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 3 );
  BOOST_CHECK( bc.NBC == 3 );

  Real x, time;
  Real nx;
  Real rho, u, t, p, e, E;
  Real Cv;

  x = time = 0;   // not actually used in functions
  nx = -1.0;  // Note: magnitude = 1
  rho = 1.137; u = 0.784; t = 0.987;
  Cv = R/(gamma - 1);
  p = R*rho*t;
  e = Cv*t;
  E = e + 0.5*(u*u);

  ArrayQ q;
  DensityVelocityPressure1D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  ArrayQ qx = 0;

  ArrayQ lg = {1.551, -0.237, 3.119};

  // strong-form BC
  ArrayQ rsdBCTrue = {rho - bcdata(0), rho*u - bcdata(1), rho*E - bcdata(2)};
  ArrayQ rsdBC = 0;
  bc.strongBC( x, time, nx, q, qx, lg, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(2), rsdBC(2), small_tol, close_tol );

  rsdBC = 0;
  bc.strongBC( x, time, nx, q, qx, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(2), rsdBC(2), small_tol, close_tol );

  // BC weight (B^t)
  Real dfdudata[9] = {           0.0,         1,      0,
                          -0.4917248,    1.2544,    0.4,
                       -1.2278982912, 1.4432656, 1.0976};

  Real wghtBCdata[9];
  for (int n = 0; n < 9; n++)
    wghtBCdata[n] = nx*dfdudata[n];

  MatrixQ wghtBCTrue( wghtBCdata, 9 );
  MatrixQ wghtBC = 0;
  bc.weightBC( x, time, nx, q, qx,wghtBC );
  SANS_CHECK_CLOSE( wghtBCTrue(0,0), wghtBC(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,1), wghtBC(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,2), wghtBC(0,2), small_tol, close_tol );

  SANS_CHECK_CLOSE( wghtBCTrue(1,0), wghtBC(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,1), wghtBC(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,2), wghtBC(1,2), small_tol, close_tol );

  SANS_CHECK_CLOSE( wghtBCTrue(2,0), wghtBC(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,1), wghtBC(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,2), wghtBC(2,2), small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_outflow_pressure )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  const ArrayQ bcdata = {1,0,0};
  BCClass bc(pde, bcdata);

  // static tests
  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 3 );
  BOOST_CHECK( bc.NBC == 1 );

  Real x, time;
  Real nx;
  Real rho, u, t, p;

  x = time = 0;   // not actually used in functions
  nx = 1.0;
  rho = 1.137; u = 0.784; t = 0.987;
  p  = R*rho*t;

  ArrayQ q;
  DensityVelocityPressure1D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  ArrayQ qx = 0;

  ArrayQ lg = {1.551, -0.237, 3.119};

  // strong-form BC
  ArrayQ rsdBCTrue = {p - bcdata(0), 0, 0};
  ArrayQ rsdBC = 0;
  bc.strongBC( x, time, nx, q, qx, lg, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(2), rsdBC(2), small_tol, close_tol );

  rsdBC = 0;
  bc.strongBC( x, time, nx, q, qx, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,            rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,            rsdBC(2), small_tol, close_tol );

  // BC weight (B^t)
  Real wghtBCdata[9] = {     200./141., 0, 0,
                            1489./705., 0, 0,
                        280231./88125., 0, 0};
  MatrixQ wghtBCTrue( wghtBCdata, 9 );
  MatrixQ wghtBC = 0;
  bc.weightBC( x, time, nx, q, qx, wghtBC );
  SANS_CHECK_CLOSE( wghtBCTrue(0,0), wghtBC(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,1), wghtBC(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,2), wghtBC(0,2), small_tol, close_tol );

  SANS_CHECK_CLOSE( wghtBCTrue(1,0), wghtBC(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,1), wghtBC(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,2), wghtBC(1,2), small_tol, close_tol );

  SANS_CHECK_CLOSE( wghtBCTrue(2,0), wghtBC(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,1), wghtBC(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,2), wghtBC(2,2), small_tol, close_tol );

  // Lagrange multiplier weight (\bar {A}^t)
  Real wghtLGdata[9] = {                 0.,              1., 0.,
                               9604./15625.,       196./125., 0.,
                         12228293./7812500., 287973./125000., 0.};
  MatrixQ wghtLGTrue( wghtLGdata, 9 );
  MatrixQ wghtLG = 0;
  bc.weightLagrange( x, time, nx, q, qx, wghtLG );
  SANS_CHECK_CLOSE( wghtLGTrue(0,0), wghtLG(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,1), wghtLG(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,2), wghtLG(0,2), small_tol, close_tol );

  SANS_CHECK_CLOSE( wghtLGTrue(1,0), wghtLG(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,1), wghtLG(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,2), wghtLG(1,2), small_tol, close_tol );

  SANS_CHECK_CLOSE( wghtLGTrue(2,0), wghtLG(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,1), wghtLG(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,2), wghtLG(2,2), small_tol, close_tol );
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_fullstate_solution )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef BCEuler1D<BCTypeFunction_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  typedef SolutionFunction_Euler1D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionClass;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  Real x, time;
  Real nx;
  Real rho, u, t, p;

  x = time = 0;   // not actually used in functions
  nx = -0.936;
  rho = 1.137; u = 0.784; t = 0.987;
  p  = R*rho*t;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  ArrayQ q;
  DensityVelocityPressure1D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  BCClass::Function_ptr constsol( new SolutionClass(gas, rho, u, p) );
  BCClass bc(pde, constsol, true);
  BCClass bc2(pde, constsol, false);

  // static tests
  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 3 );
  BOOST_CHECK( bc.NBC == 3 );

  ArrayQ qI = 0;
  ArrayQ qB, qB2;

  bc.state(x, time, nx, qI, qB);

  Real rhoB, uB, tB;
  pde.variableInterpreter().eval(qB, rhoB, uB, tB);

  SANS_CHECK_CLOSE(rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB, small_tol, close_tol );

  bc.state(param, x, time, nx, qI, qB2);

  Real rhoB2, uB2, tB2;
  pde.variableInterpreter().eval(qB2, rhoB2, uB2, tB2);

  SANS_CHECK_CLOSE(rho, rhoB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB2, small_tol, close_tol );

  qI = pde.setDOFFrom( DensityVelocityPressure1D<Real>(rhoB, uB, tB) );
  BOOST_CHECK_EQUAL( true, bc.isValidState(nx ,qI) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_fullstate )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler1D<BCTypeFullState_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  Real x, time;
  Real nx;
  Real rho, u, t, p;

  x = time = 0;   // not actually used in functions
  nx = -1.000;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; t = 0.987;
  p  = R*rho*t;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  ArrayQ q;
  DensityVelocityPressure1D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  BCClass bc(pde, qdata, false);
  BCClass bc2(pde, qdata, true);

  // static tests
  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 3 );
  BOOST_CHECK( bc.NBC == 3 );

  ArrayQ qI = 0;
  ArrayQ qB, qB2;

  bc.state(x, time, nx, qI, qB);

  Real rhoB, uB, tB;
  pde.variableInterpreter().eval(qB, rhoB, uB, tB);

  SANS_CHECK_CLOSE(rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB, small_tol, close_tol );

  bc.state(param, x, time, nx, qI, qB2);

  Real rhoB2, uB2, tB2;
  pde.variableInterpreter().eval(qB2, rhoB2, uB2, tB2);

  SANS_CHECK_CLOSE(rho, rhoB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB2, small_tol, close_tol );

  qI = pde.setDOFFrom( DensityVelocityPressure1D<Real>(rhoB, uB, tB) );
  BOOST_CHECK_EQUAL( true, bc.isValidState(nx, qI) );
  BOOST_CHECK_EQUAL( true, bc2.isValidState(nx, qI) );

  qI = pde.setDOFFrom( DensityVelocityPressure1D<Real>(rhoB, -uB, tB) );
  BOOST_CHECK_EQUAL( false, bc.isValidState(nx, qI) );
  BOOST_CHECK_EQUAL( true, bc2.isValidState(nx, qI) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_outflow_pressure_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler1D<BCTypeOutflowSubsonic_Pressure_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  typedef BCParameters< BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;

  const Real gamma = 1.4;
  const Real R = 287.04;        // J/(kg K)
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  const Real small_tol = 3.e-12;
  const Real close_tol = 3.e-12;

  //Pydict constructor
  PyDict BCSubsonicOut;
  BCSubsonicOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;

  BCSubsonicOut[BCEuler1DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec]
                 = 101325.0;

  BCClass bc(pde, BCSubsonicOut);
  typedef BCClass::ArrayQ<Real> ArrayQ;

  // static tests
  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 3 );
  BOOST_CHECK( bc.NBC == 2 );

  Real x, time;
  Real nx;
  Real rho, u, t, p;

  // Test conditions - M = 0.3, T0 = 300.1, P0 = 101325; alpha = 0.0 rad
  x = time = 0;   // not actually used in functions
  nx = 1.000;
  rho = 1.137;
  t = 300.1;
  p  = R*rho*t;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  Real M = 0.3;
  Real c =  sqrt(1.4*287.04*300.1);
  Real alpha = 0.0;
  u = M*c*cos(alpha);

  ArrayQ q, qB, qB2, qBTrue;
  DensityVelocityPressure1D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  bc.state(x,time,nx,q,qB);

  qBTrue[0] = 1.164915860299754;
  qBTrue[1] = 95.737488772104143;
  qBTrue[2] = 101325.0;

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );

  bc.state(param,x,time,nx,q,qB2);

  SANS_CHECK_CLOSE( qBTrue(0), qB2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB2(2), small_tol, close_tol );


  Real rhox = 0.01;
  Real ux = 0.049;
  Real px = 0.002;
  ArrayQ qx = {rhox, ux, px};

  ArrayQ Fn = 0;
  bc.fluxNormal( x, time, nx, q, qx, qB, Fn);

  ArrayQ fx = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, time, qB, fx);
  FnTrue += fx*nx;

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );

  Fn = 0;
  bc.fluxNormal( param, x, time, nx, q, qx, qB, Fn);

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_outflow_pressure_supersonic_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler1D<BCTypeOutflowSupersonic_Pressure_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  typedef BCParameters< BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;

  const Real gamma = 1.4;
  const Real R = 287.04;        // J/(kg K)
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  const Real small_tol = 3.e-12;
  const Real close_tol = 3.e-12;

  //Pydict constructor
  PyDict BCSubsonicOut;
  BCSubsonicOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSupersonic_Pressure_mitState;

  BCSubsonicOut[BCEuler1DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec]
                 = 101325.0;

  BCClass bc(pde, BCSubsonicOut);
  typedef BCClass::ArrayQ<Real> ArrayQ;

  // static tests
  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 3 );
  BOOST_CHECK( bc.NBC == 2 );

  Real x, time;
  Real nx;
  Real rho, u, t, p;

  // Test conditions - M = 0.3, T0 = 300.1, P0 = 101325; alpha = 0.0 rad
  x = time = 0;   // not actually used in functions
  nx = 1.000;
  rho = 1.137;
  t = 300.1;
  p  = R*rho*t;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  Real M = 0.3;
  Real c =  sqrt(1.4*287.04*300.1);
  Real alpha = 0.0;
  u = M*c*cos(alpha);

  ArrayQ q, qB, qB2, qBTrue;
  DensityVelocityPressure1D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  bc.state(x,time,nx,q,qB);

  qBTrue[0] = 1.164915860299754;
  qBTrue[1] = 95.737488772104143;
  qBTrue[2] = 101325.0;

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );

  bc.state(param,x,time,nx,q,qB2);

  SANS_CHECK_CLOSE( qBTrue(0), qB2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB2(2), small_tol, close_tol );


  Real rhox = 0.01;
  Real ux = 0.049;
  Real px = 0.002;
  ArrayQ qx = {rhox, ux, px};

  ArrayQ Fn = 0;
  bc.fluxNormal( x, time, nx, q, qx, qB, Fn);

  ArrayQ fx = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvectiveUpwind(x, time, q, qB, nx, fx);
  FnTrue += fx*nx;

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );

  Fn = 0;
  bc.fluxNormal( param, x, time, nx, q, qx, qB, Fn);

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_outflow_pressure_supersonic_spacetime_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler1D<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  typedef BCParameters< BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;

  const Real gamma = 1.4;
  const Real R = 287.04;        // J/(kg K)
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  const Real small_tol = 3.e-12;
  const Real close_tol = 3.e-12;

  //Pydict constructor
  PyDict BCSubsonicOut;
  BCSubsonicOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSupersonic_Pressure_SpaceTime_mitState;

  BCSubsonicOut[BCEuler1DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec]
                 = 101325.0;

  BCClass bc(pde, BCSubsonicOut);
  typedef BCClass::ArrayQ<Real> ArrayQ;

  // static tests
  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 3 );
  BOOST_CHECK( bc.NBC == 2 );

  Real x, time;
  Real nx, nt;
  Real rho, u, t, p;

  // Test conditions - M = 0.3, T0 = 300.1, P0 = 101325; alpha = 0.0 rad
  x = time = 0;   // not actually used in functions
  nx = 1.000; nt = 0.0;
  rho = 1.137;
  t = 300.1;
  p  = R*rho*t;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  Real M = 0.3;
  Real c =  sqrt(1.4*287.04*300.1);
  Real alpha = 0.0;
  u = M*c*cos(alpha);

  ArrayQ q, qB, qB2, qBTrue;
  DensityVelocityPressure1D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  bc.state(x, time, nx, nt, q, qB);

  qBTrue[0] = 1.164915860299754;
  qBTrue[1] = 95.737488772104143;
  qBTrue[2] = 101325.0;

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );

  bc.state(param,x,time,nx, nt, q,qB2);

  SANS_CHECK_CLOSE( qBTrue(0), qB2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB2(2), small_tol, close_tol );


  Real rhox = 0.01;
  Real ux = 0.049;
  Real px = 0.002;
  ArrayQ qx = {rhox, ux, px};
  ArrayQ qIt = {0, 0, 0};

  ArrayQ Fn = 0;
  bc.fluxNormalSpaceTime( x, time, nx, nt, q, qx, qIt, qB, Fn);

  ArrayQ fx = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvectiveUpwindSpaceTime(x, time, q, qB, nx, nt, fx);
  FnTrue += fx*nx;

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );

  Fn = 0;
  bc.fluxNormalSpaceTime( param, x, time, nx, nt, q, qx, qIt, qB, Fn);

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_SubsonicInflow_PtTt_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  Real x, time;
  Real nx;
  Real rho, u, t, p;

  x = time = 0;   // not actually used in functions
  nx = -1;
  rho = 1.137; u = 0.784; t = 0.987;
  p  = R*rho*t;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  ArrayQ q;
  DensityVelocityPressure1D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );


  Real TtSpec = t*(1+0.2*0.1*0.1);
  Real PtSpec = p*pow( (1+0.2*0.1*0.1), 3.5 );
  BCClass bc(pde, TtSpec, PtSpec);

  // static tests
  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 3 );
  BOOST_CHECK( bc.NBC == 2 );
  BOOST_CHECK_EQUAL( false, bc.hasFluxViscous() );

  ArrayQ qB = 0;

  bc.state(x, time, nx, q, qB);

  Real rhoB, uB, tB;
  pde.variableInterpreter().eval(qB, rhoB, uB, tB);

  Real rhoTrue = 0.84038917838672367;
  Real uTrue = 0.56592542899297926;
  Real tTrue = 0.87459128886396864;

  SANS_CHECK_CLOSE(rhoTrue, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(uTrue, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(tTrue, tB, small_tol, close_tol );


  Real rhox = 0.01;
  Real ux = 0.049;
  Real px = 0.002;
  ArrayQ qx = {rhox, ux, px};

  ArrayQ Fn = 0;
  bc.fluxNormal( x, time, nx, q, qx,  qB, Fn);

  ArrayQ fx = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, time, qB, fx);
  FnTrue += fx*nx;

  SANS_CHECK_CLOSE(    FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE(    FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE(    FnTrue(2), Fn(2), small_tol, close_tol );



}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_SubsonicInflow_PtTt_SpaceTime_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler1D<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  Real x, time;
  Real nx, nt;
  Real rho, u, t, p;

  x = time = 0;   // not actually used in functions
  nx = -1;
  nt = 0.0;
  rho = 1.137; u = 0.784; t = 0.987;
  p  = R*rho*t;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  ArrayQ q;
  DensityVelocityPressure1D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );


  Real TtSpec = t*(1+0.2*0.1*0.1);
  Real PtSpec = p*pow( (1+0.2*0.1*0.1), 3.5 );
  BCClass bc(pde, TtSpec, PtSpec);

  // static tests
  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 3 );
  BOOST_CHECK( bc.NBC == 2 );
  BOOST_CHECK_EQUAL( false, bc.hasFluxViscous() );

  ArrayQ qB = 0;

  bc.state(x, time, nx, nt, q, qB);

  Real rhoB, uB, tB;
  pde.variableInterpreter().eval(qB, rhoB, uB, tB);

  Real rhoTrue = 0.84038917838672367;
  Real uTrue = 0.56592542899297926;
  Real tTrue = 0.87459128886396864;

  SANS_CHECK_CLOSE(rhoTrue, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(uTrue, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(tTrue, tB, small_tol, close_tol );


  Real rhox = 0.01;
  Real ux = 0.049;
  Real px = 0.002;
  ArrayQ qx = {rhox, ux, px};
  ArrayQ qIt = {0, 0, 0};

  ArrayQ Fn = 0;
  bc.fluxNormalSpaceTime( x, time, nx, nt, q, qx, qIt, qB, Fn);

  ArrayQ fx = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvectiveUpwindSpaceTime(x, time, q, qB, nx, nt, FnTrue);

  SANS_CHECK_CLOSE(    FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE(    FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE(    FnTrue(2), Fn(2), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_spacetimeic_functions )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef BCEuler1D<BCTypeTimeIC_Function, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  typedef SolutionFunction_Euler1D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionClass;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  Real x, time;
  Real nx, nt;
  Real rho, u, t, p;

  x = time = 0;   // not actually used in functions
  nx = -0.936, nt = sqrt(1.0 - nx*nx);
  rho = 1.137; u = 0.784; t = 0.987;
  p  = R*rho*t;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  ArrayQ q;
  DensityVelocityPressure1D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  BCClass::Function_ptr constsol( new SolutionClass(gas, rho, u, p) );
  BCClass bc(pde, constsol);

  // static tests
  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 3 );
  BOOST_CHECK( bc.NBC == 3 );

  ArrayQ qI = 0;
  ArrayQ qB, qB2;

  bc.state(x, time, nx, nt, qI, qB);

  Real rhoB, uB, tB;
  pde.variableInterpreter().eval(qB, rhoB, uB, tB);

  SANS_CHECK_CLOSE(rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB, small_tol, close_tol );

  bc.state(param, x, time, nx, nt, qI, qB2);

  Real rhoB2, uB2, tB2;
  pde.variableInterpreter().eval(qB2, rhoB2, uB2, tB2);

  SANS_CHECK_CLOSE(rho, rhoB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB2, small_tol, close_tol );

  qI = pde.setDOFFrom( DensityVelocityPressure1D<Real>(rhoB, uB, tB) );
  BOOST_CHECK_EQUAL( true, bc.isValidState(nx, nt, qI) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/BCEuler1D_pattern.txt", true );

  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  {
  typedef BCEuler1D<BCTypeTimeIC, PDEClass> BCClass;

  BCClass bc(pde, DensityVelocityPressure1D<Real>(1.1, 1.3, 5.45));

  bc.dump( 2, output );
  }

  {
  typedef BCEuler1D<BCTypeReflect, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {0, 0, 0};
  BCClass bc(pde, bcdata);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler1D<BCTypeInflowSupersonic, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {0, 0, 0};
  BCClass bc(pde, bcdata);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {0, 0, 0};
  BCClass bc(pde, bcdata);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler1D<BCTypeFunction_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  typedef SolutionFunction_Euler1D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionClass;

  const ArrayQ bcdata = {0, 0, 0};
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  BCClass::Function_ptr constsol( new SolutionClass(gas, 1.0, 1.0, 1.0) );
  BCClass bc(pde, constsol, true);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDEClass> BCClass;

  const Real PtSpec = 1.0;
  const Real TtSpec = 2.0;
  BCClass bc(pde, PtSpec, TtSpec);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler1D<BCTypeTimeIC_Function, PDEClass> BCClass;

  typedef SolutionFunction_Euler1D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  BCClass::Function_ptr constsol( new SolutionClass(gas, 1.0, 1.0, 1.0) );
  BCClass bc(pde, constsol);

  bc.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
