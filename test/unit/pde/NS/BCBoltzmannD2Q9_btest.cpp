// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCBoltzmannD2Q9_btest
//
// test of quasi 1-D compressible Euler BC classes
#if 1

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/NS/QD2Q9PrimitiveDistributionFunctions.h"
#include "pde/NS/PDEBoltzmannD2Q9.h"
#include "pde/NS/BCBoltzmannD2Q9.h"
#include "pde/BCParameters.h"
#include "pde/BCCategory.h"

#include "pde/NDConvert/BCNDConvertSpace2D.h"
//#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveDistributionFunctions {};

template class BCBoltzmannD2Q9< BCTypeNone,
  PDEBoltzmannD2Q9<TraitsSizeBoltzmannD2Q9, TraitsModelBoltzmannD2Q9<QTypePrimitiveDistributionFunctions, GasModel>> >;

template class BCBoltzmannD2Q9< BCTypeDiffuseKinetic,
  PDEBoltzmannD2Q9<TraitsSizeBoltzmannD2Q9, TraitsModelBoltzmannD2Q9<QTypePrimitiveDistributionFunctions, GasModel>> >;

// Instantiate BCParamaters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters<
                BCBoltzmannD2Q9Vector<TraitsSizeBoltzmannD2Q9, TraitsModelBoltzmannD2Q9<QTypePrimitiveDistributionFunctions, GasModel>> >;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BCBoltzmannD2Q9_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{

  typedef QTypePrimitiveDistributionFunctions QType;
  typedef TraitsModelBoltzmannD2Q9<QType, GasModel> TraitsModelBoltzmannD2Q9Class;
  typedef PDEBoltzmannD2Q9<TraitsSizeBoltzmannD2Q9, TraitsModelBoltzmannD2Q9Class> PDEClass;

  {
  typedef BCBoltzmannD2Q9<BCTypeNone, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 9 );
  BOOST_CHECK( BCClass::NBC == 0 );
  BOOST_CHECK( ArrayQ::M == 9 );
  BOOST_CHECK( MatrixQ::M == 9 );
  BOOST_CHECK( MatrixQ::N == 9 );
  }

  {
  typedef BCBoltzmannD2Q9<BCTypeDiffuseKinetic, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 9 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( ArrayQ::M == 9 );
  BOOST_CHECK( MatrixQ::M == 9 );
  BOOST_CHECK( MatrixQ::N == 9 );
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctorND )
{
  typedef QTypePrimitiveDistributionFunctions QType;
  typedef TraitsModelBoltzmannD2Q9<QType, GasModel> TraitsModelBoltzmannD2Q9Class;
  typedef PDEBoltzmannD2Q9<TraitsSizeBoltzmannD2Q9, TraitsModelBoltzmannD2Q9Class> PDEClass;
  typedef BCParameters< BCBoltzmannD2Q9Vector<TraitsSizeBoltzmannD2Q9, TraitsModelBoltzmannD2Q9Class> > BCParams;


  //const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  Real tau = 1.3;
  Real Uadv = 0;
  Real Vadv = 0;
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw,
                PDEClass::BGK, tau,
                PDEClass::Hydrodynamics, Uadv, Vadv );

  {
    typedef BCBoltzmannD2Q9<BCTypeNone, PDEClass> BCClass;

    BCClass bc1(pde);

    //Pydict constructor
    PyDict BCNone;
    BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

    PyDict BCList;
    BCList["BC1"] = BCNone;
    BCParams::checkInputs(BCList);

    std::map< std::string, std::shared_ptr<BCBase> > BCs =
        BCParams::template createBCs<BCNDConvertSpace>(pde, BCList);
  }

  {
    typedef BCBoltzmannD2Q9<BCTypeDiffuseKinetic, PDEClass> BCClass;

    PyDict BCVel;
    BCVel[BCParams::params.BC.BCType] = BCParams::params.BC.DiffuseKinetic;
    BCVel[BCBoltzmannD2Q9Params<BCTypeDiffuseKinetic>::params.Rhow] = -1;
    BCVel[BCBoltzmannD2Q9Params<BCTypeDiffuseKinetic>::params.Uw] = -0.0;
    BCVel[BCBoltzmannD2Q9Params<BCTypeDiffuseKinetic>::params.Vw] = 0.0;


    BCClass bc1(pde, BCVel);

    PyDict BCList;
    BCList["BC1"] = BCVel;
    BCParams::checkInputs(BCList);

    std::map< std::string, std::shared_ptr<BCBase> > BCs =
        BCParams::template createBCs<BCNDConvertSpace>(pde, BCList);
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( diffuse_kinetic )
{
  typedef QTypePrimitiveDistributionFunctions QType;
  typedef TraitsModelBoltzmannD2Q9<QType, GasModel> TraitsModelBoltzmannD2Q9Class;
  typedef PDEBoltzmannD2Q9<TraitsSizeBoltzmannD2Q9, TraitsModelBoltzmannD2Q9Class> PDEClass;
  typedef BCParameters< BCBoltzmannD2Q9Vector<TraitsSizeBoltzmannD2Q9, TraitsModelBoltzmannD2Q9Class> > BCParams;

  typedef BCBoltzmannD2Q9<BCTypeDiffuseKinetic, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  Real tau = 1.3;
  Real Uadv = 1.2;
  Real Vadv = 2.3;
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw,
                PDEClass::BGK, tau,
                PDEClass::AdvectionDiffusion, Uadv, Vadv );

  PyDict BCVel;
  BCVel[BCParams::params.BC.BCType] = BCParams::params.BC.DiffuseKinetic;
  BCVel[BCBoltzmannD2Q9Params<BCTypeDiffuseKinetic>::params.Rhow] = -1;
  BCVel[BCBoltzmannD2Q9Params<BCTypeDiffuseKinetic>::params.Uw] = -0.0;
  BCVel[BCBoltzmannD2Q9Params<BCTypeDiffuseKinetic>::params.Vw] = 0.0;

  BCClass bc(pde,BCVel);

  ArrayQ q;
  PrimitiveDistributionFunctionsD2Q9<Real> qdata;
  qdata.DistributionFunction0 = 1.1;
  qdata.DistributionFunction1 = 1.2;
  qdata.DistributionFunction2 = 1.3;
  qdata.DistributionFunction3 = 1.4;
  qdata.DistributionFunction4 = 1.5;
  qdata.DistributionFunction5 = 1.6;
  qdata.DistributionFunction6 = 1.7;
  qdata.DistributionFunction7 = 1.8;
  qdata.DistributionFunction8 = 1.9;

  pde.setDOFFrom(q,qdata);

  ArrayQ qB = 0.;
  Real nx = 0, ny = 1;
  Real x = 1., y = 2., time = 21.;
  bc.state(x, y, time, nx, ny, q, qB);

  Real q0True = 1.1;
  Real q1True = 1.2;
  Real q2True = 1.3;
  Real q3True = 1.4;
  Real q5True = 1.6;
  Real q6True = 1.7;

  SANS_CHECK_CLOSE(q0True, qB(0), small_tol, close_tol);
  SANS_CHECK_CLOSE(q1True, qB(1), small_tol, close_tol);
  SANS_CHECK_CLOSE(q2True, qB(2), small_tol, close_tol);
  SANS_CHECK_CLOSE(q3True, qB(3), small_tol, close_tol);
  SANS_CHECK_CLOSE(q5True, qB(5), small_tol, close_tol);
  SANS_CHECK_CLOSE(q6True, qB(6), small_tol, close_tol);

  Real Rho = 13.5; // Sum_i q_i

  const Real weight[9] = { 16./36.,
                           4./36., 4./36., 4./36., 4./36.,
                           1./36., 1./36., 1./36., 1./36.};

  Real q4True = weight[4]*Rho;
  Real q7True = weight[7]*Rho;
  Real q8True = weight[8]*Rho;

  SANS_CHECK_CLOSE(q4True, qB(4), small_tol, close_tol);
  SANS_CHECK_CLOSE(q7True, qB(7), small_tol, close_tol);
  SANS_CHECK_CLOSE(q8True, qB(8), small_tol, close_tol);

  PyDict BCVel2;
  BCVel2[BCParams::params.BC.BCType] = BCParams::params.BC.DiffuseKinetic;
  BCVel2[BCBoltzmannD2Q9Params<BCTypeDiffuseKinetic>::params.Rhow] = -1;
  BCVel2[BCBoltzmannD2Q9Params<BCTypeDiffuseKinetic>::params.Uw] = 1.2;
  BCVel2[BCBoltzmannD2Q9Params<BCTypeDiffuseKinetic>::params.Vw] = 0.0;

  BCClass bc2(pde,BCVel2);

  qdata.DistributionFunction0 = 1.1;
  qdata.DistributionFunction1 = 1.2;
  qdata.DistributionFunction2 = 1.3;
  qdata.DistributionFunction3 = 1.4;
  qdata.DistributionFunction4 = 1.5;
  qdata.DistributionFunction5 = 1.6;
  qdata.DistributionFunction6 = 1.7;
  qdata.DistributionFunction7 = 1.8;
  qdata.DistributionFunction8 = 1.9;

  pde.setDOFFrom(q,qdata);

  qB = 0.;

  bc2.state(x, y, time, nx, ny, q, qB);

  q0True = 1.1;
  q1True = 1.2;
  q2True = 1.3;
  q3True = 1.4;
  q5True = 1.6;
  q6True = 1.7;

  SANS_CHECK_CLOSE(q0True, qB(0), small_tol, close_tol);
  SANS_CHECK_CLOSE(q1True, qB(1), small_tol, close_tol);
  SANS_CHECK_CLOSE(q2True, qB(2), small_tol, close_tol);
  SANS_CHECK_CLOSE(q3True, qB(3), small_tol, close_tol);
  SANS_CHECK_CLOSE(q5True, qB(5), small_tol, close_tol);
  SANS_CHECK_CLOSE(q6True, qB(6), small_tol, close_tol);

  const int e[9][2] = {   {  0,  0 }, {  1,  0 }, {  0,  1 },
                          { -1,  0 }, {  0, -1 }, {  1,  1 },
                          { -1,  1 }, { -1, -1 }, {  1, -1 }    };
  Real u_dot_u = 1.2*1.2 + 0*0;
  Real e_dot_u, csq = 1./3;
  e_dot_u = 0;
  q4True = weight[4]*Rho*(1 - u_dot_u/(2*csq));
  e_dot_u = e[7][0] * 1.2 + 0;
  q7True = weight[7]*Rho*(1 + e_dot_u/csq + (e_dot_u*e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq) );
  e_dot_u = e[8][0] * 1.2 + 0;
  q8True = weight[8]*Rho*(1 + e_dot_u/csq + (e_dot_u*e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq) );

  SANS_CHECK_CLOSE(q4True, qB(4), small_tol, close_tol);
  SANS_CHECK_CLOSE(q7True, qB(7), small_tol, close_tol);
  SANS_CHECK_CLOSE(q8True, qB(8), small_tol, close_tol);
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
#endif
