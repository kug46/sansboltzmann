// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEEuler2D_btest
//
// test of 2-D compressible Euler PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/PDEEuler2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"

namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
class QTypeEntropy {};

// Explicitly instantiate the class so coverage information is correct
template class TraitsSizeEuler<PhysD2>;
template class TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>;
template class TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>;
template class TraitsModelEuler<QTypeConservative, GasModel>;
template class TraitsModelEuler<QTypeEntropy, GasModel>;
template class PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>>;
template class PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>>;
template class PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypeConservative, GasModel>>;
template class PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypeEntropy, GasModel>>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEEuler2D_test_suite )

typedef boost::mpl::list< QTypePrimitiveRhoPressure,
                          QTypeConservative,
                          QTypePrimitiveSurrogate,
                          QTypeEntropy > QTypes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( static_test, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  BOOST_REQUIRE( PDEClass::D == 2 );
  BOOST_REQUIRE( PDEClass::N == 4 );
  BOOST_REQUIRE( ArrayQ::M == 4 );
  BOOST_REQUIRE( MatrixQ::M == 4 );
  BOOST_REQUIRE( MatrixQ::N == 4 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctor, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( pde.D == 2 );
  BOOST_REQUIRE( pde.N == 4 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setDOFFrom )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  Real rho = 1.137;
  Real u = 0.784;
  Real v = -0.231;
  Real t = 0.987;
  Real p = R*rho*t;
  Real E = gas.energy(rho,t) + 0.5*(u*u + v*v);

  PyDict d;

  // set
  Real qDataPrim[4] = {rho, u, v, t};
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY", "Temperature"};
  ArrayQ qTrue = {rho, u, v, p};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );

  DensityVelocityPressure2D<Real> qdata1;
  qdata1.Density = rho; qdata1.VelocityX = u; qdata1.VelocityY = v; qdata1.Pressure = p;
  q = 0;
  pde.setDOFFrom( q, qdata1 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );

  PyDict rhoVP;
  rhoVP[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.PrimitivePressure;
  rhoVP[DensityVelocityPressure2DParams::params.rho] = rho;
  rhoVP[DensityVelocityPressure2DParams::params.u] = u;
  rhoVP[DensityVelocityPressure2DParams::params.v] = v;
  rhoVP[DensityVelocityPressure2DParams::params.p] = p;

  d[NSVariableType2DParams::params.StateVector] = rhoVP;
  NSVariableType2DParams::checkInputs(d);
  q = pde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );


  DensityVelocityTemperature2D<Real> qdata2(rho, u, v, t);
  q = 0;
  pde.setDOFFrom( q, qdata2 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );

  PyDict rhoVT;
  rhoVT[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.PrimitiveTemperature;
  rhoVT[DensityVelocityTemperature2DParams::params.rho] = rho;
  rhoVT[DensityVelocityTemperature2DParams::params.u] = u;
  rhoVT[DensityVelocityTemperature2DParams::params.v] = v;
  rhoVT[DensityVelocityTemperature2DParams::params.t] = t;

  d[NSVariableType2DParams::params.StateVector] = rhoVT;
  NSVariableType2DParams::checkInputs(d);
  q = pde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );


  Conservative2D<Real> qdata3(rho, rho*u, rho*v, rho*E);
  q = 0;
  pde.setDOFFrom( q, qdata3 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );

  PyDict Conservative;
  Conservative[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.Conservative;
  Conservative[Conservative2DParams::params.rho] = rho;
  Conservative[Conservative2DParams::params.rhou] = rho*u;
  Conservative[Conservative2DParams::params.rhov] = rho*v;
  Conservative[Conservative2DParams::params.rhoE] = rho*E;

  d[NSVariableType2DParams::params.StateVector] = Conservative;
  NSVariableType2DParams::checkInputs(d);
  q = pde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  // function tests

  Real x, y, time;
  Real rho, u, v, t, p, e0, h0;
  Real Cv, Cp;

  x = y = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u + v*v);
  h0 = Cp*t + 0.5*(u*u + v*v);

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );

  // conservative flux
  ArrayQ uTrue = {rho, rho*u, rho*v, rho*e0};
  ArrayQ uCons = 0;
  pde.masterState( x, y, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );
  BOOST_CHECK_CLOSE( uTrue(3), uCons(3), tol );

  // Should not accumulate
  pde.masterState( x, y, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );
  BOOST_CHECK_CLOSE( uTrue(3), uCons(3), tol );

  // conservative flux
  ArrayQ ftTrue = {rho, rho*u, rho*v, rho*e0};
  ArrayQ ft = 0;
  pde.fluxAdvectiveTime( x, y, time, q, ft );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( ftTrue(3), ft(3), tol );

  // Flux accumulate
  pde.fluxAdvectiveTime( x, y, time, q, ft );
  BOOST_CHECK_CLOSE( 2*ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(3), ft(3), tol );

  // jacobianMasterState
  MatrixQ dudq = 0;
  MatrixQ dudqTrue = 0;

  ArrayQ rho_q, u_q, v_q, t_q = 0;
  Real e_rho, e_t = 0;
  pde.variableInterpreter().evalJacobian( q, rho_q, u_q, v_q, t_q );
  Real e = pde.gasModel().energy(rho, t);
  pde.gasModel().energyJacobian(rho, t, e_rho, e_t);
  Real E = e + 0.5*(u*u + v*v);
  ArrayQ E_q = e_rho*rho_q + e_t*t_q + u*u_q + v*v_q;

  dudqTrue(0,0) = rho_q[0];
  dudqTrue(0,1) = rho_q[1];
  dudqTrue(0,2) = rho_q[2];
  dudqTrue(0,3) = rho_q[3];

  dudqTrue(1,0) = rho_q[0]*u + rho*u_q[0];
  dudqTrue(1,1) = rho_q[1]*u + rho*u_q[1];
  dudqTrue(1,2) = rho_q[2]*u + rho*u_q[2];
  dudqTrue(1,3) = rho_q[3]*u + rho*u_q[3];

  dudqTrue(2,0) = rho_q[0]*v + rho*v_q[0];
  dudqTrue(2,1) = rho_q[1]*v + rho*v_q[1];
  dudqTrue(2,2) = rho_q[2]*v + rho*v_q[2];
  dudqTrue(2,3) = rho_q[3]*v + rho*v_q[3];

  dudqTrue(3,0) = rho_q[0]*E + rho*E_q[0];
  dudqTrue(3,1) = rho_q[1]*E + rho*E_q[1];
  dudqTrue(3,2) = rho_q[2]*E + rho*E_q[2];
  dudqTrue(3,3) = rho_q[3]*E + rho*E_q[3];

  pde.jacobianMasterState(x, y, time, q, dudq);
  for (int i=0; i<4; i++)
    for (int j=0; j<4; j++)
      BOOST_CHECK_CLOSE( dudq(i,j), dudqTrue(i,j), tol );

  // Should not accumulate
  pde.jacobianMasterState(x, y, time, q, dudq);
  for (int i=0; i<4; i++)
    for (int j=0; j<4; j++)
      BOOST_CHECK_CLOSE( dudq(i,j), dudqTrue(i,j), tol );

  // advective flux
  ArrayQ fTrue = {rho*u, rho*u*u + p, rho*u*v, rho*u*h0};
  ArrayQ gTrue = {rho*v, rho*v*u, rho*v*v + p, rho*v*h0};
  ArrayQ f = 0, g = 0;

  pde.fluxAdvective( x, y, time, q, f, g );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( gTrue(1), g(1), tol );
  BOOST_CHECK_CLOSE( gTrue(2), g(2), tol );
  BOOST_CHECK_CLOSE( gTrue(3), g(3), tol );

  // Flux accumulate
  pde.fluxAdvective( x, y, time, q, f, g );
  BOOST_CHECK_CLOSE( 2*fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( 2*gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( 2*gTrue(1), g(1), tol );
  BOOST_CHECK_CLOSE( 2*gTrue(2), g(2), tol );
  BOOST_CHECK_CLOSE( 2*gTrue(3), g(3), tol );

  // advective flux jacobian
  Real dfdudata[16] = {0,1,0,0, -0.4810526,1.2544,0.0924,0.4, 0.181104,-0.231,0.784,0,       -1.2404487984,1.4699461,0.0724416,1.0976};
  Real dgdudata[16] = {0,0,1,0,  0.181104,-0.231,0.784,0,     0.0802424,-0.3136,-0.3696,0.4,  0.3654893781,0.0724416,1.6944641,-0.3234};
  MatrixQ dfduTrue( dfdudata, 16 );
  MatrixQ dgduTrue( dgdudata, 16 );
  MatrixQ dfdu, dgdu;
  dfdu = 0;  dgdu = 0;
  pde.jacobianFluxAdvective( x, y, time, q, dfdu, dgdu );
  for (int i = 0; i < 4; i++)
  for (int j = 0; j < 4; j++)
  {
    BOOST_CHECK_CLOSE( dfduTrue(i,j), dfdu(i,j), tol );
    BOOST_CHECK_CLOSE( dgduTrue(i,j), dgdu(i,j), tol );
  }

  // Flux accumulate
  pde.jacobianFluxAdvective( x, y, time, q, dfdu, dgdu );
  for (int i = 0; i < 4; i++)
  for (int j = 0; j < 4; j++)
  {
    BOOST_CHECK_CLOSE( 2*dfduTrue(i,j), dfdu(i,j), tol );
    BOOST_CHECK_CLOSE( 2*dgduTrue(i,j), dgdu(i,j), tol );
  }

  // Call these simply to make sure coverage is high
  ArrayQ qL, qR, qx, qy, qt, qxL, qxR, qyL, qyR, source = 0.0;
  Real dummy = 0;
  f = 0.0;
  MatrixQ kxx = 0.0, kxy = 0.0, kyx = 0.0, kyy = 0.0;
  pde.fluxViscous( dummy, dummy, dummy, q, qx, qy, f, g );
  BOOST_CHECK_CLOSE( 0.0, f(0), tol );
  BOOST_CHECK_CLOSE( 0.0, f(1), tol );
  BOOST_CHECK_CLOSE( 0.0, f(2), tol );
  BOOST_CHECK_CLOSE( 0.0, f(3), tol );

  pde.diffusionViscous( dummy, dummy, dummy, q, qx, qy, kxx, kxy, kyx, kyy );
  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      BOOST_CHECK_CLOSE( 0.0, kxx(i,j), tol );
      BOOST_CHECK_CLOSE( 0.0, kxy(i,j), tol );
      BOOST_CHECK_CLOSE( 0.0, kyx(i,j), tol );
      BOOST_CHECK_CLOSE( 0.0, kyy(i,j), tol );
    }
  }

  MatrixQ kxt = 0.0, kyt = 0.0, ktx = 0.0, kty = 0.0, ktt = 0.0;
  pde.diffusionViscousSpaceTime( dummy, dummy, dummy, q, qx, qy, qt,
                                 kxx, kxy, kxt,
                                 kyx, kyy, kyt,
                                 ktx, kty, ktt);
  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      BOOST_CHECK_CLOSE( 0.0, kxx(i,j), tol );
      BOOST_CHECK_CLOSE( 0.0, kxy(i,j), tol );
      BOOST_CHECK_CLOSE( 0.0, kxt(i,j), tol );
      BOOST_CHECK_CLOSE( 0.0, kyx(i,j), tol );
      BOOST_CHECK_CLOSE( 0.0, kyt(i,j), tol );
      BOOST_CHECK_CLOSE( 0.0, kyy(i,j), tol );
      BOOST_CHECK_CLOSE( 0.0, ktx(i,j), tol );
      BOOST_CHECK_CLOSE( 0.0, kty(i,j), tol );
      BOOST_CHECK_CLOSE( 0.0, ktt(i,j), tol );
    }
  }

  pde.source( dummy, dummy, dummy, q, qx, qy, source );
  BOOST_CHECK_CLOSE( 0.0, source(0), tol );
  BOOST_CHECK_CLOSE( 0.0, source(1), tol );
  BOOST_CHECK_CLOSE( 0.0, source(2), tol );
  BOOST_CHECK_CLOSE( 0.0, source(3), tol );
  //pde.forcingFunction( x, y, dummy, source );

  ArrayQ qp = 0, qpx = 0, qpy = 0;
  source = 0;
  pde.source( dummy, dummy, dummy, q, qp, qx, qy, qpx, qpy, source );
  BOOST_CHECK_CLOSE( 0.0, source(0), tol );
  BOOST_CHECK_CLOSE( 0.0, source(1), tol );
  BOOST_CHECK_CLOSE( 0.0, source(2), tol );
  BOOST_CHECK_CLOSE( 0.0, source(3), tol );

  ArrayQ fn = 0, sourceL = 0 , sourceR = 0;
  Real nx=0, ny=0;
  pde.fluxViscous( dummy, dummy, dummy, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );
  BOOST_CHECK_CLOSE( 0.0, fn(0), tol );
  BOOST_CHECK_CLOSE( 0.0, fn(1), tol );
  BOOST_CHECK_CLOSE( 0.0, fn(2), tol );
  BOOST_CHECK_CLOSE( 0.0, fn(3), tol );

  pde.sourceTrace( dummy, dummy, dummy, dummy, dummy, qL, qxL, qyL, qR, qxR, qyR, sourceL, sourceR );
  BOOST_CHECK_CLOSE( 0.0, sourceL(0), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceL(1), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceL(2), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceL(3), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceR(0), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceR(1), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceR(2), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceR(3), tol );
  
  MatrixQ dsdu = 0.0;
  pde.jacobianSourceHACK( dummy, dummy, dummy, q, qx, qy, dsdu );
  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      BOOST_CHECK_CLOSE( 0.0, dsdu(i,j), tol );
    }
  }
  
  MatrixQ dsdux = 0.0, dsduy = 0.0;
  pde.jacobianGradientSourceHACK( dummy, dummy, dummy, q, qx, qy, dsdux, dsduy );
  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      BOOST_CHECK_CLOSE( 0.0, dsdux(i,j), tol );
      BOOST_CHECK_CLOSE( 0.0, dsduy(i,j), tol );
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Conservative_Jacobians )
{
  // Uses automatic differentiation to verify Jacobian functions d()/du
  typedef QTypeConservative QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template ArrayQ< SurrealS<PDEClass::N> > ArrayQSurreal;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  // function tests

  Real x, y, time;
  Real rho, u, v, t;

  x = y = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;

  ArrayQ q = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  ArrayQSurreal qSurreal = q;
  ArrayQSurreal f = 0, g = 0;

  qSurreal[0].deriv(0) = 1;
  qSurreal[1].deriv(1) = 1;
  qSurreal[2].deriv(2) = 1;
  qSurreal[3].deriv(3) = 1;

  pde.fluxAdvective( x, y, time, qSurreal, f, g );

  MatrixQ dfdu=0, dgdu=0;

  pde.jacobianFluxAdvective( x, y, time, q, dfdu, dgdu );
  for (int i = 0; i < PDEClass::N; i++)
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( f[i].deriv(j), dfdu(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( g[i].deriv(j), dgdu(i,j), small_tol, close_tol );
    }

  // Flux accumulate
  pde.jacobianFluxAdvective( x, y, time, q, dfdu, dgdu );
  for (int i = 0; i < PDEClass::N; i++)
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( 2*f[i].deriv(j), dfdu(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( 2*g[i].deriv(j), dgdu(i,j), small_tol, close_tol );
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PrimitiveRhoPressure_StrongFlux )
{
  // Separate test for Primitive here to bypass generalization of QInterpret.
  // This ensures that the strong flux works barring any bugs in QInterpret
  // Next test uses QInterpret
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  // function tests

  Real x, y, time;
  Real rho, u, v, t, p, e0;
  Real Cv;

  x = y = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  Cv = R/(gamma - 1);
//  Cp = gamma*Cv;
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u + v*v);

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );

  // Strong Conservative Flux (PRIMITIVE SPECIFIC)
  Real rhot = -0.9;
  Real ut = -0.25;
  Real vt = 0.14;
  Real pt = 0.23;
  Real tt = (t/p) * pt - (t/rho)*rhot;

  Real e0t = Cv*tt + u*ut + v*vt;

  ArrayQ qt = {rhot, ut, vt, pt};
  ArrayQ utCons, utConsTrue;
  utConsTrue[0] = rhot;
  utConsTrue[1] = rho*ut + rhot*u;
  utConsTrue[2] = rho*vt + rhot*v;
  utConsTrue[3] = rho*e0t + rhot*e0;
  utCons = 0;
  pde.strongFluxAdvectiveTime( x, y, time, q, qt, utCons );
  BOOST_CHECK_CLOSE( utConsTrue(0), utCons(0), tol );
  BOOST_CHECK_CLOSE( utConsTrue(1), utCons(1), tol );
  BOOST_CHECK_CLOSE( utConsTrue(2), utCons(2), tol );
  BOOST_CHECK_CLOSE( utConsTrue(3), utCons(3), tol );

  // Flux accumulation
  pde.strongFluxAdvectiveTime( x, y, time, q, qt, utCons );
  BOOST_CHECK_CLOSE( 2*utConsTrue(0), utCons(0), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(1), utCons(1), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(2), utCons(2), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(3), utCons(3), tol );

  // Strong Advective Flux
  Real rhox = -0.02;
  Real rhoy = 0.05;
  Real ux = 0.072;
  Real uy = -0.036;
  Real vx = -0.0234;
  Real vy = 0.0898;
  Real px = 10.2;
  Real py = 0.05;

  //PRIMITIVE SPECIFIC
  ArrayQ qx = {rhox, ux, vx, px};
  ArrayQ qy = {rhoy, uy, vy, py};

//  Real tx = (t/p) * px - (t/rho)*rhox;
//  Real ty = (t/p) * py - (t/rho)*rhoy;

//  Real H = Cp*t + 0.5*(u*u + v*v);
//  Real Hx = Cp*tx + (ux*u + vx*v);
//  Real Hy = Cp*ty + (uy*u + vy*v);

  ArrayQ strongAdv = 0;
  ArrayQ strongAdvTrue = 0;
//  strongAdvTrue(0) += rhox*u   +   rho*ux;
//  strongAdvTrue(1) += rhox*u*u + 2*rho*ux*u + px;
//  strongAdvTrue(2) += rhox*u*v +   rho*ux*v + rho*u*vx;
//  strongAdvTrue(3) += rhox*u*H +   rho*ux*H + rho*u*Hx;
//
//  strongAdvTrue(0) += rhoy*v   +   rho*vy;
//  strongAdvTrue(1) += rhoy*v*u +   rho*vy*u + rho*v*uy;
//  strongAdvTrue(2) += rhoy*v*v + 2*rho*vy*v + py;
//  strongAdvTrue(3) += rhoy*v*H +   rho*vy*H + rho*v*Hy;
  strongAdvTrue(0) = 0.1567366;
  strongAdvTrue(1) = 10.39651816240000;
  strongAdvTrue(2) = -0.0306508024;
  strongAdvTrue(3) = 28.32292926589490;
  //See Euler2D.nb

  pde.strongFluxAdvective( x, y, time, q, qx, qy, strongAdv );
  BOOST_CHECK_CLOSE( strongAdvTrue(0), strongAdv(0), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(1), strongAdv(1), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(2), strongAdv(2), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(3), strongAdv(3), tol );

  // Flux accumulation
  pde.strongFluxAdvective( x, y, time, q, qx, qy, strongAdv );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(0), strongAdv(0), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(1), strongAdv(1), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(2), strongAdv(2), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(3), strongAdv(3), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( strongFlux, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  typedef Q2D<QType, TraitsSizeEuler> QInterpret;

  const Real tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  QInterpret qInterpret(gas);

  // function tests

  Real x, y, time;
  Real rho, u, v, t, e0;
  Real Cv, Cp;

  x = y = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  e0 = Cv*t + 0.5*(u*u + v*v);

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );

  // Strong Conservative Flux
  ArrayQ qt = {-0.9, -0.25, 0.14, 0.23};

  Real rhot, ut, vt, et;
  Real tt; //d(temperature)/d(time)
  qInterpret.evalGradient( q, qt, rhot, ut, vt, tt);
  gas.energyGradient(rho, t, rhot, tt, et);
  Real e0t = et + u*ut + v*vt;

  ArrayQ utCons, utConsTrue;

  utConsTrue[0] = rhot;
  utConsTrue[1] = rho*ut + rhot*u;
  utConsTrue[2] = rho*vt + rhot*v;
  utConsTrue[3] = rho*e0t + rhot*e0;
  utCons = 0;
  pde.strongFluxAdvectiveTime( x, y, time, q, qt, utCons );
  BOOST_CHECK_CLOSE( utConsTrue(0), utCons(0), tol );
  BOOST_CHECK_CLOSE( utConsTrue(1), utCons(1), tol );
  BOOST_CHECK_CLOSE( utConsTrue(2), utCons(2), tol );
  BOOST_CHECK_CLOSE( utConsTrue(3), utCons(3), tol );

  // Flux accumulation
  pde.strongFluxAdvectiveTime( x, y, time, q, qt, utCons );
  BOOST_CHECK_CLOSE( 2*utConsTrue(0), utCons(0), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(1), utCons(1), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(2), utCons(2), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(3), utCons(3), tol );


  // Strong Advective Flux
  Real rhox, ux, vx, tx, px, hx;
  Real rhoy, uy, vy, ty, py, hy;

  ArrayQ qx = {-0.02, 0.072, -0.0234, 10.2};
  ArrayQ qy = {0.05, -0.036, 0.0898, 0.05};

  qInterpret.eval( q, rho, u, v, t );
  qInterpret.evalGradient( q, qx, rhox, ux, vx, tx);
  qInterpret.evalGradient( q, qy, rhoy, uy, vy, ty);

  gas.pressureGradient(rho, t, rhox, tx, px);
  gas.pressureGradient(rho, t, rhoy, ty, py);

  gas.enthalpyGradient(rho, t, rhox, tx, hx);
  gas.enthalpyGradient(rho, t, rhoy, ty, hy);

  Real H = Cp*t + 0.5*(u*u + v*v);
  Real Hx = hx + (ux*u + vx*v);
  Real Hy = hy + (uy*u + vy*v);

  ArrayQ strongAdv = 0;
  ArrayQ strongAdvTrue = 0;
  strongAdvTrue(0) += rhox*u   +   rho*ux;
  strongAdvTrue(1) += rhox*u*u + 2*rho*ux*u + px;
  strongAdvTrue(2) += rhox*u*v +   rho*ux*v + rho*u*vx;
  strongAdvTrue(3) += rhox*u*H +   rho*ux*H + rho*u*Hx;

  strongAdvTrue(0) += rhoy*v   +   rho*vy;
  strongAdvTrue(1) += rhoy*v*u +   rho*vy*u + rho*v*uy;
  strongAdvTrue(2) += rhoy*v*v + 2*rho*vy*v + py;
  strongAdvTrue(3) += rhoy*v*H +   rho*vy*H + rho*v*Hy;

  pde.strongFluxAdvective( x, y, time, q, qx, qy, strongAdv );
  BOOST_CHECK_CLOSE( strongAdvTrue(0), strongAdv(0), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(1), strongAdv(1), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(2), strongAdv(2), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(3), strongAdv(3), tol );

  // Flux accumulation
  pde.strongFluxAdvective( x, y, time, q, qx, qy, strongAdv );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(0), strongAdv(0), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(1), strongAdv(1), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(2), strongAdv(2), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(3), strongAdv(3), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( jacobianMasterState, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename PDEClass::template ArrayQ<SurrealS<PDEClass::N>> ArrayQSurreal;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( pde.N == 4 );

  // function tests

  Real rho, u, v, t;

  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;

  Real x = 0, y = 0, time = 0;

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );

  if ( std::is_same<QType,QTypeEntropy>::value )
  {
    // Entropy variables seem to have numerical problems when comparing Surreal and
    // hand differentiated code...
    q[0] = -10;
    q[1] = 2;
    q[2] = 4;
    q[3] = -3;
  }

  ArrayQSurreal qSurreal = q;

  qSurreal[0].deriv(0) = 1;
  qSurreal[1].deriv(1) = 1;
  qSurreal[2].deriv(2) = 1;
  qSurreal[3].deriv(3) = 1;

  // conservative flux
  ArrayQSurreal uConsSurreal = 0;
  pde.masterState( x, y, time, qSurreal, uConsSurreal );

  MatrixQ dudq;
  pde.jacobianMasterState( x, y, time, q, dudq );

  const Real small_tol = 5e-10;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(0), dudq(0,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(1), dudq(0,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(2), dudq(0,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(3), dudq(0,3), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(0), dudq(1,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(1), dudq(1,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(2), dudq(1,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(3), dudq(1,3), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(0), dudq(2,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(1), dudq(2,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(2), dudq(2,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(3), dudq(2,3), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(0), dudq(3,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(1), dudq(3,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(2), dudq(3,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(3), dudq(3,3), small_tol, close_tol )

  // Should not accumulate
  pde.jacobianMasterState( x, y, time, q, dudq );

  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(0), dudq(0,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(1), dudq(0,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(2), dudq(0,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(3), dudq(0,3), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(0), dudq(1,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(1), dudq(1,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(2), dudq(1,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(3), dudq(1,3), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(0), dudq(2,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(1), dudq(2,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(2), dudq(2,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(3), dudq(2,3), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(0), dudq(3,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(1), dudq(3,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(2), dudq(3,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(3), dudq(3,3), small_tol, close_tol )
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( jacobianFluxAdvectiveAbsoluteValue, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 2.e-11;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  // jacobianFluxAdvectiveAbsoluteValue function test

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t;

  x = y = time = 0;   // not actually used in functions
  nx = 2.0/7.0; ny = sqrt(1.0-nx*nx);
  rho = 1.034; u = 3.26; v = -2.17; t = 5.78;

  // set
  ArrayQ q = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rho, u, v, t) );

  MatrixQ mtx;
  mtx = 0;

  Real scale = 10.0;


  //Values taken from pde/NS/Roe.nb
  MatrixQ mtxTrue = {
    {1.0323434331949190979834072919713,
        -0.44459540501697074517797177104962,
        -0.43697991171460424043588329213258,
        0.080449362812368834954522463171426},
    {-0.72312835498151845646006425483631,
        -0.010364873336226558791267034135421,
        -1.4045718375886762151895973056639,
        0.18933272986886306466125181349454},
    {-0.90833712876943321732424020657867,
        1.9404853199183346620104522800780,
        2.1633849842368432379888624467138,
        -0.41919712890981821676619255996783},
    {-0.43536491993382434782224675355112,
        -8.1758936855015456959218531030353,
        -6.9672110585297722485152708900077,
        2.7090875342447734040794006013828}
                    };

  MatrixQ mtxTrue2 = scale*mtxTrue;

  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, time, q, scale*nx, scale*ny, mtx );
  for (int i=0; i<4; i++)
    for (int j=0; j<4; j++)
      BOOST_CHECK_CLOSE( mtx(i,j), mtxTrue2(i,j), tol );

  // Flux accumulation
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, time, q, scale*nx, scale*ny, mtx );
  for (int i=0; i<4; i++)
    for (int j=0; j<4; j++)
      BOOST_CHECK_CLOSE( mtx(i,j), 2*mtxTrue2(i,j), tol );

  const Real eps = 1.e-9;
  MatrixQ mtxTrue3 = eps*mtxTrue;

  mtx = 0;
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, time, q, eps*nx, eps*ny, mtx );
  for (int i=0; i<4; i++)
    for (int j=0; j<4; j++)
      BOOST_CHECK_CLOSE( mtx(i,j), mtxTrue3(i,j), tol );

  nx = 0;  ny = 0;  // Note: magnitude == 0

  mtxTrue = 0;

  mtx = 0;
  const Real small_tol2 = 1.e-12;
  const Real close_tol = 1.e-12;
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, time, q, nx, ny, mtx );
  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++)
      SANS_CHECK_CLOSE( mtxTrue(i,j), mtx(i,j), small_tol2, close_tol );


#if 0
  printf( "btest: mtx = \n" );
  for (int i=0; i<4; i++)
  {
    for (int j = 0; j < 4; j++)
      printf( " %13.5e", mtx(i,j) );
    printf( "\n" );
  }
#endif
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux_NDSteady, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> PDENDClass;
  typedef typename PDENDClass::VectorX VectorX;
  typedef typename PDENDClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDENDClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename PDENDClass::template TensorMatrixQ<Real> TensorMatrixQ;

  const Real tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDENDClass pdeND(gas, Euler_ResidInterp_Raw);

  // function tests

  VectorX X;
  Real rho, u, v, t, p, e0, h0;
  Real Cv, Cp;

  X = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u + v*v);
  h0 = Cp*t + 0.5*(u*u + v*v);

  // set
  ArrayQ q;
  pdeND.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );

  // conservative flux
  ArrayQ uTrue = {rho, rho*u, rho*v, rho*e0};
  ArrayQ uCons = 0;
  pdeND.masterState( X, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );
  BOOST_CHECK_CLOSE( uTrue(3), uCons(3), tol );

  // advective flux
  VectorArrayQ F = 0;
  ArrayQ fTrue = {rho*u, rho*u*u + p, rho*u*v, rho*u*h0};
  ArrayQ gTrue = {rho*v, rho*v*u, rho*v*v + p, rho*v*h0};
  ArrayQ f = 0, g = 0;

  pdeND.fluxAdvective( X, q, F );
  BOOST_CHECK_CLOSE( fTrue(0), F[0](0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), F[0](1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), F[0](2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), F[0](3), tol );
  BOOST_CHECK_CLOSE( gTrue(0), F[1](0), tol );
  BOOST_CHECK_CLOSE( gTrue(1), F[1](1), tol );
  BOOST_CHECK_CLOSE( gTrue(2), F[1](2), tol );
  BOOST_CHECK_CLOSE( gTrue(3), F[1](3), tol );

  // Flux accumulation
  pdeND.fluxAdvective( X, q, F );
  BOOST_CHECK_CLOSE( 2*fTrue(0), F[0](0), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(1), F[0](1), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(2), F[0](2), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(3), F[0](3), tol );
  BOOST_CHECK_CLOSE( 2*gTrue(0), F[1](0), tol );
  BOOST_CHECK_CLOSE( 2*gTrue(1), F[1](1), tol );
  BOOST_CHECK_CLOSE( 2*gTrue(2), F[1](2), tol );
  BOOST_CHECK_CLOSE( 2*gTrue(3), F[1](3), tol );

  //Call these simply to make sure coverage is high
  ArrayQ source = 0, qp = 0;
  VectorArrayQ gradq = 0, gradqp = 0;
  TensorMatrixQ K;
  pdeND.fluxViscous( X, q, gradq, F );
  pdeND.diffusionViscous( X, q, gradq, K );
  pdeND.source( X, q, gradq, source );
  pdeND.source( X, q, qp, gradq, gradqp, source );
  //pdeND.forcingFunction( X, source );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxRoe, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 2.e-11;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  // Roe flux function test

  Real x, y, time;
  Real nx, ny;
  Real rhoL, uL, vL, tL, pL, h0L;
  Real rhoR, uR, vR, tR, pR, h0R;
  Real Cp;

  x = y = time = 0;   // not actually used in functions
  ny = sqrt(1.0-4.0/49.0); nx = 2.0/7.0;

  rhoL = 1.034; uL = 3.26; vL = -2.17; tL = 5.78;
  rhoR = 0.973; uR = 1.79; vR =  0.93; tR = 6.13;
  Cp  = R*gamma/(gamma - 1);

  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL + vL*vL);

  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR + vR*vR);

  // set
  ArrayQ qL = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
  ArrayQ qR = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );

  // advective normal flux (average)
  Real fL[4] = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*vL     , rhoL*uL*h0L};
  Real fR[4] = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*vR     , rhoR*uR*h0R};
  Real gL[4] = {rhoL*vL, rhoL*vL*uL     , rhoL*vL*vL + pL, rhoL*vL*h0L};
  Real gR[4] = {rhoR*vR, rhoR*vR*uR     , rhoR*vR*vR + pR, rhoR*vR*h0R};

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < PDEClass::N; k++)
    fnTrue[k] = 0.5*(nx*(fL[k] + fR[k]) + ny*(gL[k] + gR[k]));

#if 1

  // Exact values for Roe scheme from Roe.nb
  fnTrue[0] -= -0.021574952725013058474459426733768;
  fnTrue[1] -= -1.7958753728220592172800048653566;
  fnTrue[2] -=  4.4515996249161671139371535471951;
  fnTrue[3] -= -6.8094427666218353919034995028459;
#endif


  ArrayQ fn = 0;
  pde.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( fnTrue(3), fn(3), tol );

  // Flux accumulation
  pde.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(3), fn(3), tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( speedCharacteristic, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  Real x, y, time, dx, dy;
  Real rho, u, v, t, c;
  Real speed, speedTrue;

  x = y = time = 0;   // not actually used in functions
  dx = 0.57; dy = -0.43;
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  c = sqrt(gamma*R*t);
  speedTrue = fabs(dx*u + dy*v)/sqrt(dx*dx + dy*dy) + c;

  Real qDataPrim[4] = {rho, u, v, t};
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY", "Temperature"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );

  pde.speedCharacteristic( x, y, time, dx, dy, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  pde.speedCharacteristic( x, y, time, q, speed );
  BOOST_CHECK_CLOSE( sqrt(u*u + v*v) + c, speed, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( isValidState, QType, QTypes )
{
  // Surrogates are special
  // Entropy has log( x < 0 ) when constructing invalid states
  if ( std::is_same<QType,QTypePrimitiveSurrogate>::value ) return;
  if ( std::is_same<QType,QTypeEntropy>::value ) return;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  ArrayQ q;
  Real rho, u = 0, v = 0, t;

  rho = 1;
  t = 1;
  pde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( pde.isValidState(q) == true );

  rho = 1;
  t = -1;
  pde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( pde.isValidState(q) == false );

  rho = -1;
  t = 1;
  pde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( pde.isValidState(q) == false );

  rho = -1;
  t = -1;
  pde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( pde.isValidState(q) == false );

  rho = 1;
  t = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( pde.isValidState(q) == false );

  rho = 0;
  t = 1;
  pde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( pde.isValidState(q) == false );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState_surrogate )
{
  typedef QTypePrimitiveSurrogate QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  ArrayQ q;
  Real rho, u = 0, v = 0, t;

  rho = 1;
  t = 1;
  pde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( pde.isValidState(q) == true );

  rho = 1;
  t = -1;
  pde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( pde.isValidState(q) == true );

  rho = -1;
  t = 1;
  pde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( pde.isValidState(q) == true );

  rho = -1;
  t = -1;
  pde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( pde.isValidState(q) == true );

  rho = 1;
  t = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( pde.isValidState(q) == true );

  rho = 0;
  t = 1;
  pde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( pde.isValidState(q) == true );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( interpResidBC )
{
  typedef QTypePrimitiveSurrogate QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde1(gas, Euler_ResidInterp_Raw);
  Real tol = 1.0e-12;

  ArrayQ q;
  Real rho, u = 0, v = 0, t;

  rho = 1;
  t = 1;
  pde1.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );

  BOOST_CHECK_EQUAL(pde1.category(), 0);

  BOOST_CHECK_EQUAL(pde1.nMonitor(), 4);
  BOOST_CHECK( pde1.isValidState(q) == true );
  DLA::VectorD<Real> vecout1(4);
  pde1.interpResidVariable(q, vecout1);
  for (int i=0; i<4; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );
  vecout1 = 0;
  pde1.interpResidGradient(q, vecout1);
  for (int i=0; i<4; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );
  vecout1 = 0;
  pde1.interpResidBC(q, vecout1);
  for (int i=0; i<4; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );

  PDEClass pde2(gas, Euler_ResidInterp_Momentum);
  BOOST_CHECK_EQUAL(pde2.nMonitor(), 3);
  DLA::VectorD<Real> vecout2(3);
  pde2.interpResidVariable(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)+q(2)*q(2)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(3),vecout2(2), tol, tol);

  vecout2 = 0;
  pde2.interpResidGradient(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)+q(2)*q(2)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(3),vecout2(2), tol, tol);

  vecout2 = 0;
  pde2.interpResidBC(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)+q(2)*q(2)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(3),vecout2(2), tol, tol);

  PDEClass pde3(gas, Euler_ResidInterp_Entropy);
  BOOST_CHECK_EQUAL(pde3.nMonitor(), 1);
//  DLA::VectorD<Real> vecout3(1);
//  pde2.interpResidVariable(q, vecout3);
//
//  vecout2 = 0;
//  pde2.interpResidGradient(q, vecout3);
//
//  vecout2 = 0;
//  pde2.interpResidBC(q, vecout3);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/PDEEuler2D_pattern.txt", true );

  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  pde.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
