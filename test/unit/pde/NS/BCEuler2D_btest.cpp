// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCEuler2D_btest
//
// test of 2-D compressible Euler BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/BCEuler2D.h"
#include "pde/NS/SolutionFunction_Euler2D.h"

#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime2D.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include "pde/BCParameters.h"
#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
template class BCEuler2D< BCTypeSymmetry,
  PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler2D< BCTypeInflowSupersonic,
  PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler2D< BCTypeInflowSubsonic_sHqt,
  PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler2D< BCTypeInflowSubsonic_sHqt_Profile<
  SolutionFunction_Euler2D_Wake<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >,
  PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler2D< BCTypeInflowSubsonic_sHqt_Profile<
  SolutionFunction_Euler2D_Const<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >,
  PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler2D< BCTypeOutflowSubsonic_Pressure,
  PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler2D < BCTypeFunctionInflow_sHqt_BN,
  PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler2D< BCTypeInflowSubsonic_sHqt_BN,
  PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler2D< BCTypeOutflowSubsonic_Pressure_BN,
  PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler2D< BCTypeTimeIC,
  PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler2D< BCTypeTimeIC_Function,
  PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler2D< BCTypeFullState_mitState,
  PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler2D < BCTypeFunction_mitState,
  PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler2D < BCTypeFunctionInflow_sHqt_mitState,
  PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler2D< BCTypeInflowSubsonic_PtTta_mitState,
  PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler2D< BCTypeInflowSubsonic_sHqt_mitState,
  PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler2D< BCTypeOutflowSubsonic_Pressure_mitState,
  PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler2D< BCTypeOutflowSupersonic_Pressure_mitState,
  PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler2D< BCTypeSymmetry_mitState,
  PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler2D< BCTypeReflect_mitState,
  PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler2D< BCTypeFullStateRadialInflow_mitState,
  PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;

// Instantiate BCParamaters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters< BCEuler2DVector<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BCEuler2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  {
  typedef BCEuler2D<BCTypeSymmetry, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler2D<BCTypeInflowSupersonic, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 4 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef SolutionFunction_Euler2D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction_EulerClass> BCTypeClass;
  typedef BCEuler2D<BCTypeClass, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler2D<BCTypeInflowSubsonic_sHqt_BN, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler2D<BCTypeFunctionInflow_sHqt_BN, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler2D<BCTypeOutflowSubsonic_Pressure_BN, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler2D<BCTypeTimeIC, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 4 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler2D<BCTypeTimeIC_Function, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 4 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler2D<BCTypeSymmetry_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler2D<BCTypeFullState_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 4 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler2D<BCTypeFunction_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 4 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler2D<BCTypeInflowSubsonic_sHqt_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler2D<BCTypeFunctionInflow_sHqt_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler2D<BCTypeOutflowSupersonic_Pressure_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler2D<BCTypeReflect_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler2D<BCTypeFullStateRadialInflow_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 4 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctorND )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef BCParameters< BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  {
  typedef BCNone<PhysD2,TraitsSizeEuler<PhysD2>::N> BCClass;

  BCClass bc1;

  //Pydict constructor
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BCList;
  BCList["BC1"] = BCNone;
  BCParams::checkInputs(BCList);


  std::map< std::string, std::shared_ptr<BCBase> > BCs =
      BCParams::template createBCs<BCNDConvertSpace>(pde, BCList);

  }

  {
  typedef BCEuler2D<BCTypeTimeOut, PDEClass> BCClass;

  BCClass bc1(pde);

  //Pydict constructor
  PyDict BCTimeOut;
  BCTimeOut[BCParams::params.BC.BCType] = BCParams::params.BC.TimeOut;

  PyDict BCList;
  BCList["BC1"] = BCTimeOut;
  BCParams::checkInputs(BCList);


  std::map< std::string, std::shared_ptr<BCBase> > BCs =
      BCParams::template createBCs<BCNDConvertSpaceTime>(pde, BCList);

  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef BCParameters< BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  {
  typedef BCNone<PhysD2,TraitsSizeEuler<PhysD2>::N> BCClass;

  BCClass bc1;

  //Pydict constructor
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BCList;
  BCList["BC1"] = BCNone;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCNone);
  }

  {
  typedef BCEuler2D<BCTypeSymmetry, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {0, 0, 0, 0};
  BCClass bc1(pde, bcdata);

  //Pydict constructor
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry;
  BCSymmetry[BCEuler2DParams<BCTypeSymmetry>::params.qn] = -7.0;

  PyDict BCList;
  BCList["BC1"] = BCSymmetry;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCSymmetry);
  }

  {
  typedef BCEuler2D<BCTypeInflowSupersonic, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {0, 0, 0, 0};
  BCClass bc1(pde, bcdata);

  PyDict BCInflowSupersonic;
  BCInflowSupersonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSupersonic;
  BCInflowSupersonic[BCEuler2DParams<BCTypeInflowSupersonic>::params.rho] = 1.0;
  BCInflowSupersonic[BCEuler2DParams<BCTypeInflowSupersonic>::params.rhou] = 1.0;
  BCInflowSupersonic[BCEuler2DParams<BCTypeInflowSupersonic>::params.rhov] = 1.0;
  BCInflowSupersonic[BCEuler2DParams<BCTypeInflowSupersonic>::params.rhoE] = 1.0;

  PyDict BCList;
  BCList["BC1"] = BCInflowSupersonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCInflowSupersonic);
  }

  {
  typedef BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {0, 0, 0, 0};
  BCClass bc1(pde, bcdata);

  PyDict BCInflowSubsonic;
  BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt;
  BCInflowSubsonic[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.sSpec] = 1.0;
  BCInflowSubsonic[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.HSpec] = 1.0;
  BCInflowSubsonic[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.qtSpec] = 1.0;

  PyDict BCList;
  BCList["BC1"] = BCInflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCInflowSubsonic);
  }

  {
  typedef SolutionFunction_Euler2D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction_EulerClass> BCTypeClass;
  typedef BCEuler2D<BCTypeClass, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {0, 0, 0, 0};
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  SolutionFunction_EulerClass constsol(gas, 1.0, 1.0, 1.0, 1.0);
  BCClass bc(pde, constsol);

  PyDict solnArgs;
  solnArgs[SolutionFunction_EulerClass::ParamsType::params.gasModel] = gasModelDict;
  solnArgs[SolutionFunction_EulerClass::ParamsType::params.rho] = 1.0;
  solnArgs[SolutionFunction_EulerClass::ParamsType::params.u] = 1.0;
  solnArgs[SolutionFunction_EulerClass::ParamsType::params.v] = 1.0;
  solnArgs[SolutionFunction_EulerClass::ParamsType::params.p] = 1.0;

  // Add the solution BC to the BCVector
  typedef boost::mpl::push_back< BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass>, BCClass >::type BCVector;
  typedef BCParameters<BCVector> BCParams;

  PyDict BCInflowSubsonicProfile;
  BCInflowSubsonicProfile[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_Profile;
  BCInflowSubsonicProfile[BCEuler2DParams<BCTypeClass>::params.SolutionArgs] = solnArgs;

  PyDict BCList;
  BCList["BC1"] = BCInflowSubsonicProfile;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCInflowSubsonicProfile);
  }

  {
  typedef BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {0, 0, 0, 0};
  BCClass bc(pde, bcdata);

  PyDict BCOutflowSubsonic;
  BCOutflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure;
  BCOutflowSubsonic[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure>::params.pSpec] = 1.0;

  PyDict BCList;
  BCList["BC1"] = BCOutflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCOutflowSubsonic);
  }

  {
  typedef SolutionFunction_Euler2D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler2D<BCTypeFunctionInflow_sHqt_BN, PDEClass> BCClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  BCClass::Function_ptr constsol( new SolutionFunction_EulerClass(gas, 1.1, 1.3, -0.4, 5.45) );
  BCClass bc(pde, constsol);

  PyDict Function;
  Function[BCClass::ParamsType::params.Function.Name] = BCClass::ParamsType::params.Function.Const;
  Function[SolutionFunction_EulerClass::ParamsType::params.gasModel] = gasModelDict;
  Function[SolutionFunction_EulerClass::ParamsType::params.rho] = 1.1;
  Function[SolutionFunction_EulerClass::ParamsType::params.u] = 1.3;
  Function[SolutionFunction_EulerClass::ParamsType::params.v] = -0.4;
  Function[SolutionFunction_EulerClass::ParamsType::params.p] = 5.45;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.SolutionInflow_sHqt_BN;
  BCIn[BCClass::ParamsType::params.Function] = Function;

  PyDict BCList;
  BCList["BC1"] = BCIn;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCIn);
  }

  {
  typedef BCEuler2D<BCTypeInflowSubsonic_sHqt_BN, PDEClass> BCClass;

  Real sSpec = 20.5;
  Real HSpec = 71.4;
  Real VxSpec = 2.1;
  Real VySpec = -1.1;
  Real aSpec = atan(VySpec/VxSpec);
  BCClass bc1(pde, sSpec, HSpec, aSpec);

  PyDict BCInflowSubsonic;
  BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  BCInflowSubsonic[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] = sSpec;
  BCInflowSubsonic[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = HSpec;
  BCInflowSubsonic[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.aSpec] = aSpec;

  PyDict BCList;
  BCList["BC1"] = BCInflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCInflowSubsonic);
  }

  {
  typedef BCEuler2D<BCTypeOutflowSubsonic_Pressure_BN, PDEClass> BCClass;

  const Real pSpec = 2.1;
  BCClass bc(pde, pSpec);

  PyDict BCOutflowSubsonic;
  BCOutflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOutflowSubsonic[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = pSpec;

  PyDict BCList;
  BCList["BC1"] = BCOutflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCOutflowSubsonic);
  }

  {
  typedef BCEuler2D<BCTypeTimeIC, PDEClass> BCClass;

  BCClass bc(pde, DensityVelocityPressure2D<Real>(1.1, 1.3, -0.4, 5.45));

  PyDict d;
  d[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.PrimitivePressure;
  d[DensityVelocityPressure2DParams::params.rho] = 1.1;
  d[DensityVelocityPressure2DParams::params.u] = 1.3;
  d[DensityVelocityPressure2DParams::params.v] = -0.4;
  d[DensityVelocityPressure2DParams::params.p] = 5.45;

  PyDict BCFullState;
  BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC;
  BCFullState[BCClass::ParamsType::params.StateVector] = d;

  PyDict BCList;
  BCList["BC1"] = BCFullState;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCFullState);
  }

  {
  typedef SolutionFunction_Euler2D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler2D<BCTypeTimeIC_Function, PDEClass> BCClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  BCClass::Function_ptr constsol( new SolutionFunction_EulerClass(gas, 1.1, 1.3, -0.4, 5.45) );
  BCClass bc(pde, constsol);

  PyDict Function;
  Function[BCClass::ParamsType::params.Function.Name] = BCClass::ParamsType::params.Function.Const;
  Function[SolutionFunction_EulerClass::ParamsType::params.gasModel] = gasModelDict;
  Function[SolutionFunction_EulerClass::ParamsType::params.rho] = 1.1;
  Function[SolutionFunction_EulerClass::ParamsType::params.u] = 1.3;
  Function[SolutionFunction_EulerClass::ParamsType::params.v] = -0.4;
  Function[SolutionFunction_EulerClass::ParamsType::params.p] = 5.45;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC_Function;
  BCIn[BCClass::ParamsType::params.Function] = Function;

  PyDict BCList;
  BCList["BC1"] = BCIn;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCIn);
  }

  {
  typedef BCEuler2D<BCTypeSymmetry_mitState, PDEClass> BCClass;

  BCClass bc1(pde);

  //Pydict constructor
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCList;
  BCList["BC1"] = BCSymmetry;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCSymmetry);
  }

  {
  typedef BCEuler2D<BCTypeFullState_mitState, PDEClass> BCClass;

  bool upwind = true;
  BCClass bc(pde, DensityVelocityPressure2D<Real>(1.1, 1.3, -0.4, 5.45), upwind);

  PyDict d;
  d[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.PrimitivePressure;
  d[DensityVelocityPressure2DParams::params.rho] = 1.1;
  d[DensityVelocityPressure2DParams::params.u] = 1.3;
  d[DensityVelocityPressure2DParams::params.v] = -0.4;
  d[DensityVelocityPressure2DParams::params.p] = 5.45;

  PyDict BCFullState;
  BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFullState[BCClass::ParamsType::params.Characteristic] = false;
  BCFullState[BCClass::ParamsType::params.StateVector] = d;

  PyDict BCList;
  BCList["BC1"] = BCFullState;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCFullState);
  }

  {
  typedef SolutionFunction_Euler2D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler2D<BCTypeFunction_mitState, PDEClass> BCClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  BCClass::Function_ptr constsol( new SolutionFunction_EulerClass(gas, 1.1, 1.3, -0.4, 5.45) );
  BCClass bc(pde, constsol, true);

  PyDict Function;
  Function[BCClass::ParamsType::params.Function.Name] = BCClass::ParamsType::params.Function.Const;
  Function[SolutionFunction_EulerClass::ParamsType::params.gasModel] = gasModelDict;
  Function[SolutionFunction_EulerClass::ParamsType::params.rho] = 1.1;
  Function[SolutionFunction_EulerClass::ParamsType::params.u] = 1.3;
  Function[SolutionFunction_EulerClass::ParamsType::params.v] = -0.4;
  Function[SolutionFunction_EulerClass::ParamsType::params.p] = 5.45;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCIn[BCClass::ParamsType::params.Function] = Function;
  BCIn[BCClass::ParamsType::params.Characteristic] = true;

  PyDict BCList;
  BCList["BC1"] = BCIn;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCIn);
  }

  {
  typedef SolutionFunction_Euler2D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler2D<BCTypeFunctionInflow_sHqt_mitState, PDEClass> BCClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  BCClass::Function_ptr constsol( new SolutionFunction_EulerClass(gas, 1.1, 1.3, -0.4, 5.45) );
  BCClass bc(pde, constsol);

  PyDict Function;
  Function[BCClass::ParamsType::params.Function.Name] = BCClass::ParamsType::params.Function.Const;
  Function[SolutionFunction_EulerClass::ParamsType::params.gasModel] = gasModelDict;
  Function[SolutionFunction_EulerClass::ParamsType::params.rho] = 1.1;
  Function[SolutionFunction_EulerClass::ParamsType::params.u] = 1.3;
  Function[SolutionFunction_EulerClass::ParamsType::params.v] = -0.4;
  Function[SolutionFunction_EulerClass::ParamsType::params.p] = 5.45;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.SolutionInflow_sHqt_mitState;
  BCIn[BCClass::ParamsType::params.Function] = Function;

  PyDict BCList;
  BCList["BC1"] = BCIn;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCIn);
  }

  {
  typedef BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDEClass> BCClass;

  Real TtSpec = 20.5;
  Real PtSpec = 71.4;
  Real aSpec = 2.1;
  BCClass bc1(pde, TtSpec, PtSpec, aSpec);

  PyDict BCInflowSubsonic;
  BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  BCInflowSubsonic[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.TtSpec] = TtSpec;
  BCInflowSubsonic[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.PtSpec] = PtSpec;
  BCInflowSubsonic[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.aSpec] = aSpec;

  PyDict BCList;
  BCList["BC1"] = BCInflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCInflowSubsonic);
  }

  {
  typedef BCEuler2D<BCTypeInflowSubsonic_sHqt_mitState, PDEClass> BCClass;

  Real sSpec = 20.5;
  Real HSpec = 71.4;
  Real VxSpec = 2.1;
  Real VySpec = -1.1;
  BCClass bc1(pde, sSpec, HSpec, VxSpec, VySpec);

  PyDict BCInflowSubsonic;
  BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_mitState;
  BCInflowSubsonic[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.sSpec] = sSpec;
  BCInflowSubsonic[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.HSpec] = HSpec;
  BCInflowSubsonic[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.VxSpec] = VxSpec;
  BCInflowSubsonic[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.VySpec] = VySpec;

  PyDict BCList;
  BCList["BC1"] = BCInflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCInflowSubsonic);
  }

  {
  typedef BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState, PDEClass> BCClass;

  const Real pSpec = 2.1;
  BCClass bc(pde, pSpec);

  PyDict BCOutflowSubsonic;
  BCOutflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOutflowSubsonic[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pSpec;

  PyDict BCList;
  BCList["BC1"] = BCOutflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCOutflowSubsonic);
  }

  {
  typedef BCEuler2D<BCTypeOutflowSupersonic_Pressure_mitState, PDEClass> BCClass;

  const Real pSpec = 2.1;
  BCClass bc( pde, pSpec );

  PyDict BCOutflowSupersonic;
  BCOutflowSupersonic[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSupersonic_Pressure_mitState;
  BCOutflowSupersonic[BCEuler2DParams<BCTypeOutflowSupersonic_Pressure_mitState>::params.pSpec] = pSpec;

  PyDict BCList;
  BCList["BC1"] = BCOutflowSupersonic;
  BCParams::checkInputs( BCList );

  BCClass bc2( pde, BCOutflowSupersonic );
  }

  {
  typedef BCEuler2D<BCTypeReflect_mitState, PDEClass> BCClass;

  BCClass bc1(pde);

  //Pydict constructor
  PyDict BCReflect;
  BCReflect[BCParams::params.BC.BCType] = BCParams::params.BC.Reflect_mitState;

  PyDict BCList;
  BCList["BC1"] = BCReflect;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCReflect);
  }

  {
  typedef BCEuler2D<BCTypeFullStateRadialInflow_mitState, PDEClass> BCClass;

  Real rho = 1.0, V = 1.5, t = 0.4;
  bool upwind = true;

  BCClass bc1(pde, upwind, rho, V, t);

  PyDict BCInflowRadial;
  BCInflowRadial[BCParams::params.BC.BCType] = BCParams::params.BC.FullStateRadialInflow_mitState;
  BCInflowRadial[BCEuler2DParams<BCTypeFullStateRadialInflow_mitState>::params.Characteristic] = upwind;
  BCInflowRadial[BCEuler2DParams<BCTypeFullStateRadialInflow_mitState>::params.rho] = rho;
  BCInflowRadial[BCEuler2DParams<BCTypeFullStateRadialInflow_mitState>::params.V] = V;
  BCInflowRadial[BCEuler2DParams<BCTypeFullStateRadialInflow_mitState>::params.T] = t;

  PyDict BCList;
  BCList["BC1"] = BCInflowRadial;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCInflowRadial);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_Symmetry )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler2D<BCTypeSymmetry, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  const ArrayQ bcdata = {-7, 0, 0, 0};
  BCClass bc(pde, bcdata);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 1 );

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p, E, H, qn;
  Real Cv, Cp;

  x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  E  = Cv*t + 0.5*(u*u + v*v);
  H  = Cp*t + 0.5*(u*u + v*v);
  qn = nx*u + ny*v;

  ArrayQ q;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  ArrayQ qx = 0;
  ArrayQ qy = 0;

  ArrayQ lg = {1.551, -0.237, 9.887, 3.119};

  // strong-form BC
  ArrayQ rsdBCTrue = {qn - bcdata(0), lg(1) - rho*E, lg(2), lg(3) - rho};
  ArrayQ rsdBC = 0;
  bc.strongBC( x, y, time, nx, ny, q, qx, qy, lg, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(2), rsdBC(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(3), rsdBC(3), small_tol, close_tol );

  rsdBC = 0;
  bc.strongBC( x, y, time, nx, ny, q, qx, qy, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,            rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,            rsdBC(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,            rsdBC(3), small_tol, close_tol );

  // BC weight
  Real wghtBCdata[16] = {rho, 0, 0, 0, rho*(u + nx*qn), 0, 0, 0, rho*(v + ny*qn), 0, 0, 0, rho*(H + qn*qn), 0, 0, 0};
  MatrixQ wghtBCTrue( wghtBCdata, 16 );
  MatrixQ wghtBC = 0;
  bc.weightBC( x, y, time, nx, ny, q, qx, qy, wghtBC );
  SANS_CHECK_CLOSE( wghtBCTrue(0,0), wghtBC(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,1), wghtBC(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,2), wghtBC(0,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,3), wghtBC(0,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,0), wghtBC(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,1), wghtBC(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,2), wghtBC(1,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,3), wghtBC(1,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,0), wghtBC(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,1), wghtBC(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,2), wghtBC(2,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,3), wghtBC(2,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,0), wghtBC(3,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,1), wghtBC(3,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,2), wghtBC(3,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,3), wghtBC(3,3), small_tol, close_tol );

  // Lagrange multiplier weight
  Real wghtLGdata[16] = {0, 0, -0.815136, 0,
                         -0.3744, -0.2645567232, -0.5140138416, 0,
                         0.1408, -0.7713803776, 0.1412680192, 0,
                         -1.1411904, 0.0194824025088, 0.1089049410624, 0};
  MatrixQ wghtLGTrue( wghtLGdata, 16 );
  MatrixQ wghtLG = 0;
  bc.weightLagrange( x, y, time, nx, ny, q, qx, qy, wghtLG );
  SANS_CHECK_CLOSE( wghtLGTrue(0,0), wghtLG(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,1), wghtLG(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,2), wghtLG(0,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,3), wghtLG(0,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,0), wghtLG(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,1), wghtLG(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,2), wghtLG(1,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,3), wghtLG(1,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,0), wghtLG(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,1), wghtLG(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,2), wghtLG(2,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,3), wghtLG(2,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,0), wghtLG(3,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,1), wghtLG(3,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,2), wghtLG(3,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,3), wghtLG(3,3), small_tol, close_tol );

  // Lagrange rhs
  ArrayQ rhsLGTrue = {rho*E, 0, rho, 0};
  ArrayQ rhsLG = 0;
  bc.rhsLagrange( x, y, time, nx, ny, q, qx, qy, rhsLG );
  SANS_CHECK_CLOSE( rhsLGTrue(0), rhsLG(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rhsLGTrue(1), rhsLG(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rhsLGTrue(2), rhsLG(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( rhsLGTrue(3), rhsLG(3), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_inflow_supersonic )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler2D<BCTypeInflowSupersonic, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  const ArrayQ bcdata = {7, 5.447, -2.441, 9.443};
  BCClass bc(pde, bcdata);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 4 );

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p, e, E;
  Real Cv;

  x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  Cv = R/(gamma - 1);
  p = R*rho*t;
  e = Cv*t;
  E = e + 0.5*(u*u + v*v);

  ArrayQ q;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  ArrayQ qx = 0;
  ArrayQ qy = 0;

  ArrayQ lg = {1.551, -0.237, 9.887, 3.119};

  // strong-form BC
  ArrayQ rsdBCTrue = {rho - bcdata(0), rho*u - bcdata(1), rho*v - bcdata(2), rho*E - bcdata(3)};
  ArrayQ rsdBC = 0;
  bc.strongBC( x, y, time, nx, ny, q, qx, qy, lg, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(2), rsdBC(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(3), rsdBC(3), small_tol, close_tol );

  rsdBC = 0;
  bc.strongBC( x, y, time, nx, ny, q, qx, qy, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(2), rsdBC(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(3), rsdBC(3), small_tol, close_tol );

  // BC weight (B^t)
  Real dfdudata[16] = {0,1,0,0, -0.4810526,1.2544,0.0924,0.4, 0.181104,-0.231,0.784,0,       -1.2404487984,1.4699461,0.0724416,1.0976};
  Real dgdudata[16] = {0,0,1,0,  0.181104,-0.231,0.784,0,     0.0802424,-0.3136,-0.3696,0.4,  0.3654893781,0.0724416,1.6944641,-0.3234};
  Real wghtBCdata[16];
  for (int n = 0; n < 16; n++)
    wghtBCdata[n] = nx*dfdudata[n] + ny*dgdudata[n];
  MatrixQ wghtBCTrue( wghtBCdata, 16 );
  MatrixQ wghtBC = 0;
  bc.weightBC( x, y, time, nx, ny, q, qx, qy, wghtBC );
  SANS_CHECK_CLOSE( wghtBCTrue(0,0), wghtBC(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,1), wghtBC(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,2), wghtBC(0,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,3), wghtBC(0,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,0), wghtBC(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,1), wghtBC(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,2), wghtBC(1,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,3), wghtBC(1,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,0), wghtBC(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,1), wghtBC(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,2), wghtBC(2,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,3), wghtBC(2,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,0), wghtBC(3,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,1), wghtBC(3,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,2), wghtBC(3,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,3), wghtBC(3,3), small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_inflow_sHqt )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  const ArrayQ bcdata = {-7.153, 3.551, 4.993, 0};
  BCClass bc(pde, bcdata);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 3 );

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p, s, H, qt;

  x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  p   = R*rho*t;
  s   = log(p / pow(rho, gamma));
  H   = gamma*p/((gamma - 1)*rho) + 0.5*(u*u + v*v);
  qt  = nx*v - ny*u;

  ArrayQ q;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  ArrayQ qx = 0;
  ArrayQ qy = 0;

  ArrayQ lg = {1.551, -0.237, 9.887, 3.119};

  // strong-form BC
  ArrayQ rsdBCTrue = {s - bcdata(0), H - bcdata(1), qt - bcdata(2), 0};
  ArrayQ rsdBC = 0;
  bc.strongBC( x, y, time, nx, ny, q, qx, qy, lg, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(2), rsdBC(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(3), rsdBC(3), small_tol, close_tol );

  rsdBC = 0;
  bc.strongBC( x, y, time, nx, ny, q, qx, qy, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(2), rsdBC(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,            rsdBC(3), small_tol, close_tol );

  // BC weight (B^t)
#if 0   // NOTE: compiler warning: integer constant is too large for its type
  Real wghtBCdata[16] = {109418801737851/50537387500000., -172397625/113203748., -1471586127/16171964000., 0,
                         4717164992928621/12634346875000000., -1141226737239/505373875000., 154689715257/808598200000., 0,
                         -45239076996371181/50537387500000000., 1520191218117/2021495500000., 14755754480649/16171964000000., 0,
                         375483420163239035067/101074775000000000000., -14311415513561187/4042991000000000., -5049919970377359/32343928000000000., 0};
#endif
  Real wghtBCdata[16] = { 2.1651060165674571128157346875123, -1.5228967949011723534100655395261, -0.090996129288934850460958236117765, 0,
                          2.7478401009888863764475359950096, -2.2581830872025191250734913829885,  0.19130603463747507723860874288367,  0,
                         -0.89516057782708259306043471281534, 0.75201315962217081363772513963054, 0.91242810586574395045648135254320,  0,
                          3.7149073066275837375546965105784, -3.5398088973141881839459944382760, -0.15613193210105337236714105967587,  0};
  MatrixQ wghtBCTrue( wghtBCdata, 16 );
  MatrixQ wghtBC = 0;
  bc.weightBC( x, y, time, nx, ny, q, qx, qy, wghtBC );
  SANS_CHECK_CLOSE( wghtBCTrue(0,0), wghtBC(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,1), wghtBC(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,2), wghtBC(0,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,3), wghtBC(0,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,0), wghtBC(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,1), wghtBC(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,2), wghtBC(1,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,3), wghtBC(1,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,0), wghtBC(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,1), wghtBC(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,2), wghtBC(2,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,3), wghtBC(2,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,0), wghtBC(3,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,1), wghtBC(3,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,2), wghtBC(3,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,3), wghtBC(3,3), small_tol, close_tol );

  // Lagrange multiplier weight (\bar {A}^t)
  Real wghtLGdata[16] = {27277026/244140625., 0, 0, 0,
                         2673148548/30517578125., 0, 0, 0,
                         -3150496503/122070312500., 0, 0, 0,
                         46802153065521/244140625000000., 0, 0, 0};
  MatrixQ wghtLGTrue( wghtLGdata, 16 );
  MatrixQ wghtLG = 0;
  bc.weightLagrange( x, y, time, nx, ny, q, qx, qy, wghtLG );
  SANS_CHECK_CLOSE( wghtLGTrue(0,0), wghtLG(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,1), wghtLG(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,2), wghtLG(0,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,3), wghtLG(0,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,0), wghtLG(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,1), wghtLG(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,2), wghtLG(1,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,3), wghtLG(1,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,0), wghtLG(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,1), wghtLG(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,2), wghtLG(2,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,3), wghtLG(2,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,0), wghtLG(3,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,1), wghtLG(3,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,2), wghtLG(3,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,3), wghtLG(3,3), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_inflow_sHqt_Profile )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef SolutionFunction_Euler2D_Wake<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction_EulerClass>, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  Real wakedepth = 0.1; // 10% wake depth
  Real u0 = 1.0;
  Real rho0 = 1.0;
  Real p0 = 1.0;

  SolutionFunction_EulerClass wakesol(gas, wakedepth, rho0, u0, p0);
  BCClass bc(pde, wakesol);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 3 );

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p, s, H, qt;

  x = time = 0;   // not actually used in functions
  y = 0.0; // y matters for this case
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  p   = gas.pressure(rho, t);
  s   = log(p / pow(rho, gamma));
  H   = gamma*p/((gamma - 1)*rho) + 0.5*(u*u + v*v);
  qt  = nx*v - ny*u;

  ArrayQ q;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  ArrayQ qx = 0;
  ArrayQ qy = 0;

  ArrayQ lg = {1.551, -0.237, 9.887, 3.119}; //lg not used...

  ArrayQ qSpec = wakesol(x,y,time);
  Real rhoSpec, uSpec, vSpec, tSpec = 0;
  pde.variableInterpreter().eval(qSpec, rhoSpec, uSpec, vSpec, tSpec);
  Real pSpec = gas.pressure(rhoSpec, tSpec);

  Real sSpec   = log(pSpec / pow(rhoSpec, gamma));
  Real HSpec   = gamma*pSpec/((gamma - 1)*rhoSpec) + 0.5*(uSpec*uSpec + vSpec*vSpec);
  Real qtSpec  = nx*vSpec - ny*uSpec;

  //actual values for s, H, qt at y=0.5;
  const ArrayQ bcdata = { sSpec, HSpec, qtSpec, 0};

  // strong-form BC
  ArrayQ rsdBCTrue = {s - bcdata(0), H - bcdata(1), qt - bcdata(2), 0};
  ArrayQ rsdBC = 0;
  bc.strongBC( x, y, time, nx, ny, q, qx, qy, lg, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(2), rsdBC(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(3), rsdBC(3), small_tol, close_tol );

  rsdBC = 0;
  bc.strongBC( x, y, time, nx, ny, q, qx, qy, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(2), rsdBC(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,            rsdBC(3), small_tol, close_tol );

  // BC weight (B^t)
#if 0   // NOTE: compiler warning: integer constant is too large for its type
  Real wghtBCdata[16] = {109418801737851/50537387500000., -172397625/113203748., -1471586127/16171964000., 0,
                         4717164992928621/12634346875000000., -1141226737239/505373875000., 154689715257/808598200000., 0,
                         -45239076996371181/50537387500000000., 1520191218117/2021495500000., 14755754480649/16171964000000., 0,
                         375483420163239035067/101074775000000000000., -14311415513561187/4042991000000000., -5049919970377359/32343928000000000., 0};
#endif
  Real wghtBCdata[16] = { 2.1651060165674571128157346875123, -1.5228967949011723534100655395261, -0.090996129288934850460958236117765, 0,
                          2.7478401009888863764475359950096, -2.2581830872025191250734913829885,  0.19130603463747507723860874288367,  0,
                         -0.89516057782708259306043471281534, 0.75201315962217081363772513963054, 0.91242810586574395045648135254320,  0,
                          3.7149073066275837375546965105784, -3.5398088973141881839459944382760, -0.15613193210105337236714105967587,  0};
  MatrixQ wghtBCTrue( wghtBCdata, 16 );
  MatrixQ wghtBC = 0;
  bc.weightBC( x, y, time, nx, ny, q, qx, qy, wghtBC );
  SANS_CHECK_CLOSE( wghtBCTrue(0,0), wghtBC(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,1), wghtBC(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,2), wghtBC(0,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,3), wghtBC(0,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,0), wghtBC(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,1), wghtBC(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,2), wghtBC(1,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,3), wghtBC(1,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,0), wghtBC(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,1), wghtBC(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,2), wghtBC(2,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,3), wghtBC(2,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,0), wghtBC(3,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,1), wghtBC(3,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,2), wghtBC(3,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,3), wghtBC(3,3), small_tol, close_tol );

  // Lagrange multiplier weight (\bar {A}^t)
  Real wghtLGdata[16] = {27277026/244140625., 0, 0, 0,
                         2673148548/30517578125., 0, 0, 0,
                         -3150496503/122070312500., 0, 0, 0,
                         46802153065521/244140625000000., 0, 0, 0};
  MatrixQ wghtLGTrue( wghtLGdata, 16 );
  MatrixQ wghtLG = 0;
  bc.weightLagrange( x, y, time, nx, ny, q, qx, qy, wghtLG );
  SANS_CHECK_CLOSE( wghtLGTrue(0,0), wghtLG(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,1), wghtLG(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,2), wghtLG(0,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,3), wghtLG(0,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,0), wghtLG(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,1), wghtLG(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,2), wghtLG(1,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,3), wghtLG(1,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,0), wghtLG(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,1), wghtLG(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,2), wghtLG(2,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,3), wghtLG(2,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,0), wghtLG(3,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,1), wghtLG(3,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,2), wghtLG(3,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,3), wghtLG(3,3), small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_outflow_pressure )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  const ArrayQ bcdata = {-7, 0, 0, 0};
  BCClass bc(pde, bcdata);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 1 );

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p;

  x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  p  = R*rho*t;

  ArrayQ q;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  ArrayQ qx = 0;
  ArrayQ qy = 0;

  ArrayQ lg = {1.551, -0.237, 9.887, 3.119};

  // strong-form BC
  ArrayQ rsdBCTrue = {p - bcdata(0), 0, 0, 0};
  ArrayQ rsdBC = 0;
  bc.strongBC( x, y, time, nx, ny, q, qx, qy, lg, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(2), rsdBC(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(3), rsdBC(3), small_tol, close_tol );

  rsdBC = 0;
  bc.strongBC( x, y, time, nx, ny, q, qx, qy, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,            rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,            rsdBC(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,            rsdBC(3), small_tol, close_tol );

  // BC weight (B^t)
  Real wghtBCdata[16] = {-2426/1645., 0, 0, 0,
                         -61459/29375., 0, 0, 0,
                          81389/117500., 0, 0, 0,
                         -786207163/235000000., 0, 0, 0};
  MatrixQ wghtBCTrue( wghtBCdata, 16 );
  MatrixQ wghtBC = 0;
  bc.weightBC( x, y, time, nx, ny, q, qx, qy, wghtBC );
  SANS_CHECK_CLOSE( wghtBCTrue(0,0), wghtBC(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,1), wghtBC(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,2), wghtBC(0,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,3), wghtBC(0,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,0), wghtBC(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,1), wghtBC(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,2), wghtBC(1,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,3), wghtBC(1,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,0), wghtBC(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,1), wghtBC(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,2), wghtBC(2,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,3), wghtBC(2,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,0), wghtBC(3,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,1), wghtBC(3,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,2), wghtBC(3,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,3), wghtBC(3,3), small_tol, close_tol );

  // Lagrange multiplier weight (\bar {A}^t)
  Real wghtLGdata[16] = {0, -117/125., 44/125., 0,
                         -1248177/1953125., -9681/6250., 4312/15625., 0,
                         5884263/31250000., 27027/125000., -14007/15625., 0,
                         -52214988441/31250000000., -112253169/50000000., 49516313/62500000., 0};
  MatrixQ wghtLGTrue( wghtLGdata, 16 );
  MatrixQ wghtLG = 0;
  bc.weightLagrange( x, y, time, nx, ny, q, qx, qy, wghtLG );
  SANS_CHECK_CLOSE( wghtLGTrue(0,0), wghtLG(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,1), wghtLG(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,2), wghtLG(0,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,3), wghtLG(0,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,0), wghtLG(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,1), wghtLG(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,2), wghtLG(1,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,3), wghtLG(1,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,0), wghtLG(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,1), wghtLG(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,2), wghtLG(2,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,3), wghtLG(2,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,0), wghtLG(3,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,1), wghtLG(3,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,2), wghtLG(3,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,3), wghtLG(3,3), small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_sHqt_BN )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  typedef BCEuler2D<BCTypeInflowSubsonic_sHqt_BN, PDEClass> BCClass;
  typedef SolutionFunction_Euler2D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler2D<BCTypeFunctionInflow_sHqt_BN, PDEClass> BCClass2;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real R     = 0.4;
  const Real gamma = 1.4;
  const Real gmi   = gamma - 1;

  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p;

  x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  p  = R*rho*t;

  ArrayQ q;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );
  ParamType param(0.1, 0.0); // grid spacing and jump values

  Real rhoSpec = rho*1.037;
  Real uSpec = u*(0.981);
  Real vSpec = v*(1.12);
  Real tSpec = t*(1.003);
  Real pSpec = gas.pressure(rhoSpec, tSpec);

  Real sSpec = log( pSpec / pow(rhoSpec,gamma) );
  Real HSpec = gas.enthalpy(rhoSpec, tSpec) + 0.5*(uSpec*uSpec+vSpec*vSpec);
  Real aSpec = atan(vSpec/uSpec);

  BCClass bc(pde, sSpec, HSpec, aSpec);

  BCClass2::Function_ptr constsol( new SolutionFunction_EulerClass(gas, rhoSpec, uSpec, vSpec, pSpec) );
  BCClass2 bc2(pde, constsol);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 3 );
  BOOST_CHECK_EQUAL( false, bc.hasFluxViscous() );

  BOOST_CHECK( bc2.D == 2 );
  BOOST_CHECK( bc2.N == 4 );
  BOOST_CHECK( bc2.NBC == 3 );
  BOOST_CHECK_EQUAL( false, bc2.hasFluxViscous() );

  ArrayQ qB = 0;
  ArrayQ qB2 = 0;

  // Check state is valid for inflow
  BOOST_CHECK(bc.isValidState(nx, ny, q) == true);

  // Check stat is not valid when outflow
  BOOST_CHECK(bc.isValidState(-nx, ny, q) == false);

  // Check state is valid for inflow
  BOOST_CHECK(bc2.isValidState(nx, ny, q) == true);

  // Check stat is not valid when outflow
  BOOST_CHECK(bc2.isValidState(-nx, ny, q) == false);

  bc.state(x, y, time, nx, ny, q, qB);
  bc2.state(x, y, time, nx, ny, q, qB2);

  Real rhoB, uB, vB, tB = 0;
  pde.variableInterpreter().eval(qB, rhoB, uB, vB, tB);

  Real rhoB2, uB2, vB2, tB2 = 0;
  pde.variableInterpreter().eval(qB2, rhoB2, uB2, vB2, tB2);

  SANS_CHECK_CLOSE(rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(v, vB, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB, small_tol, close_tol );

  SANS_CHECK_CLOSE(rho, rhoB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(v, vB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB2, small_tol, close_tol );

  qB2 = 0;
  bc2.state(param, x, y, time, nx, ny, q, qB2);

  rhoB2 = 0;
  uB2 = 0;
  vB2 =0;
  tB2 = 0;
  pde.variableInterpreter().eval(qB2, rhoB2, uB2, vB2, tB2);

  SANS_CHECK_CLOSE(rho, rhoB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(v, vB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB2, small_tol, close_tol );

  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  ArrayQ qx = {rhox, ux, vx, px};
  ArrayQ qy = {rhoy, uy, vy, py};

  ArrayQ Fn = 0;
  ArrayQ Fn2 = 0;
  bc.fluxNormal( x, y, time, nx, ny, q, qx, qy, qB, Fn);
  bc2.fluxNormal( x, y, time, nx, ny, q, qx, qy, qB2, Fn2);

  ArrayQ fx = 0;
  ArrayQ fy = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, time, q, fx, fy);
  FnTrue += fx*nx + fy*ny;

  //compute BN Flux

  Real H = gas.enthalpy(rho, t) + 0.5*(u*u+v*v);
  Real c = gas.speedofSound(t);
  Real s = log( p /pow(rho,gamma) );

  // Tangential vector
  Real tx = -ny;
  Real ty =  nx;

  // normal and tangential velocities
  Real vn = u*nx + v*ny;
  Real vt = u*tx + v*ty;
  Real V2 = u*u+v*v;
  Real V = sqrt(V2);
  Real V3 = V2*V;
  Real c2 = c*c;

  // Strong BC's that are imposed
  Real ds  = s  - sSpec;
  Real dH  = H  - HSpec;
  Real dvt = v/V - vSpec/sqrt(uSpec*uSpec+vSpec*vSpec);

  //COMPUTE NORMAL FLUX
//  FnTrue[0] -= -rho*vn/gmi*ds;
//  FnTrue[0] -= vn*gamma*rho/(c*c + V2*gmi)*dH;
//  FnTrue[0] -=  -vt*V2*V*rho*gamma/u/(c*c+V2*gmi)*dvt;
//
//  FnTrue[1] -= -rho*(c*c*nx + u*vn*gamma)/(gamma*gmi)*ds;
//  FnTrue[1] -= rho*(c*c*nx + u*vn*(2*gamma-1))/(c*c+V2*gmi)*dH;
//  FnTrue[1] -= -(rho*V2*V*(c*c*v + vn*(v*vn*gmi+u*vt*gamma)))/(u*vn*(c*c+V2*gmi))*dvt;
//
//  FnTrue[2] -=  -rho*(c*c*ny+v*vn*gamma*rho)/(gamma*gmi)*ds;
//  FnTrue[2] -= rho*(c*c*ny + v*vn*(2*gamma-1))/(c*c+V2*gmi)*dH;
//  FnTrue[2] -= (rho*V2*V*(c*c*u + vn*(u*vn*gmi-v*vt*gamma)))/(u*vn*(c*c+V2*gmi))*dvt;
//
//  FnTrue[3] -= -rho*vn*( 2*c*c + V2*gmi)/(2*gmi*gmi)*ds;
//  FnTrue[3] -= (rho*vn*(c*c*(4*gamma-2) + V2*(2-5*gamma+3*gamma*gamma)))/(2*gmi*(c*c + V2*gmi) )*dH;
//  FnTrue[3] -=  -(vt*V2*V*rho*gamma*(2*c*c+V2*gmi))/(2*u*gmi*(c*c+V2*gmi))*dvt;

  FnTrue[0] -= -rho*vn/gmi*ds;
  FnTrue[0] -= rho*vn/c2*dH;
  FnTrue[0] -=  -vt*V3*rho/(c*c*u)*dvt;

  FnTrue[1] -= -rho*(c2*nx + u*vn*gamma)/(gamma*gmi)*ds;
  FnTrue[1] -= rho*(nx + u*vn/c2)*dH;
  FnTrue[1] -= -rho*(c2*v + u*vn*vt)*V3/(c2*u*vn)*dvt;

  FnTrue[2] -=  -rho*(c2*ny + v*vn*gamma)/(gamma*gmi)*ds;
  FnTrue[2] -= rho*(ny + v*vn/c2)*dH;
  FnTrue[2] -= rho*(c2*u - v*vn*vt)*V3/(c2*u*vn)*dvt;

  FnTrue[3] -= -rho*vn*H/gmi*ds;
  FnTrue[3] -= rho*vn*(0.5*V2/c2 + gamma/gmi)*dH;
  FnTrue[3] -=  -(vt*V3*rho*H)/(c2*u)*dvt;

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );

  SANS_CHECK_CLOSE( FnTrue(0), Fn2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn2(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn2(3), small_tol, close_tol );

  Fn2 = 0;
  bc2.fluxNormal( param, x, y, time, nx, ny, q, qx, qy, qB2, Fn2);

  SANS_CHECK_CLOSE( Fn(0), Fn2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(1), Fn2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(2), Fn2(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(3), Fn2(3), small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_outflow_pressure_BN )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler2D<BCTypeOutflowSubsonic_Pressure_BN, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  typedef BCParameters< BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;

  const Real gamma = 1.4;
  const Real R = 287.04;        // J/(kg K)
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  const Real small_tol = 3.e-12;
  const Real close_tol = 3.e-12;

  Real pSpec = 101325.0;
  //Pydict constructor
  PyDict BCSubsonicOut;
  BCSubsonicOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;

  BCSubsonicOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec]
                 = pSpec;

  BCClass bc(pde, BCSubsonicOut);
  typedef BCClass::ArrayQ<Real> ArrayQ;

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 3 );
  BOOST_CHECK_EQUAL( false, bc.hasFluxViscous() );

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p;

  // Test conditions - M = 0.3, T0 = 300.1, P0 = 101325; alpha = 0.1 rad
  x = y = time = 0;   // not actually used in functions
  nx = 0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137;
  t = 300.1;
  p  = R*rho*t;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  Real M = 0.3;
  Real c =  sqrt(1.4*287.04*300.1);
  Real alpha = 0.1;
  u = M*c*cos(alpha);
  v = M*c*sin(alpha);

  ArrayQ q, qB, qB2, qBTrue;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  bc.state(x,y,time,nx,ny,q,qB);
  bc.state(param,x,y,time,nx,ny,q,qB2);

  SANS_CHECK_CLOSE( q(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( q(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( q(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( q(3), qB(3), small_tol, close_tol );

  SANS_CHECK_CLOSE( q(0), qB2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( q(1), qB2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( q(2), qB2(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( q(3), qB2(3), small_tol, close_tol );

  BOOST_CHECK(bc.isValidState(nx, ny, q));

  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  ArrayQ qx = {rhox, ux, vx, px};
  ArrayQ qy = {rhoy, uy, vy, py};

  ArrayQ Fn = 0, Fn2 = 0;
  bc.fluxNormal( x, y, time, nx, ny, q, qx, qy, qB, Fn);
  bc.fluxNormal( param, x, y, time, nx, ny, q, qx, qy, qB, Fn2);

  ArrayQ fx = 0;
  ArrayQ fy = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, time, qB, fx, fy);
  FnTrue += fx*nx + fy*ny;

  Real vn = u*nx + v*ny;

  FnTrue[1] -= ( (p-pSpec) - rho*vn*vn/gamma*log(p/pSpec) )*nx;
  FnTrue[2] -= ( (p-pSpec) - rho*vn*vn/gamma*log(p/pSpec) )*ny;
  FnTrue[3] -= ( vn*(p-pSpec) - rho*vn*vn*vn/gamma*log(p/pSpec) );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );

  SANS_CHECK_CLOSE( FnTrue(0), Fn2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn2(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn2(3), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_timeic )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler2D<BCTypeTimeIC, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  Real x, y, time;
  Real nx, ny, nt;
  Real rho, u, v, t, p;

  x = y = time = 0;   // not actually used in functions
  nx = -0.600;  ny = 0.500;  nt = sqrt(1 - nx*nx - ny*ny); // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  p  = R*rho*t;

  ArrayQ q;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  BCClass bc(pde, qdata);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 4 );

  ArrayQ qI = 0;
  ArrayQ qB;

  bc.state(x, y, time, nx, ny, nt, qI, qB);

  Real rhoB, uB, vB, tB;
  pde.variableInterpreter().eval(qB, rhoB, uB, vB, tB);

  SANS_CHECK_CLOSE(rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(v, vB, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB, small_tol, close_tol );

  qI = pde.setDOFFrom( DensityVelocityPressure2D<Real>(rhoB, uB, vB, tB) );
  BOOST_CHECK_EQUAL( true, bc.isValidState(nx, ny, nt, qI) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_timeic_function )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef SolutionFunction_Euler2D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler2D<BCTypeTimeIC_Function, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  Real x, y, time;
  Real nx, ny, nt;
  Real rho, u, v, t, p;

  x = y = time = 0;   // not actually used in functions
  nx = -0.800;  ny = 0.600;  nt = -1.0 * sqrt(1.0 - nx*nx + ny*ny); // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  p  = R*rho*t;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  ArrayQ q;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  BCClass::Function_ptr constsol( new SolutionFunction_EulerClass(gas, rho, u, v, p) );
  BCClass bc(pde, constsol);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 4 );

  ArrayQ qI = 0;
  ArrayQ qB;

  bc.state(x, y, time, nx, ny, nt, qI, qB);

  Real rhoB, uB, vB, tB;
  pde.variableInterpreter().eval(qB, rhoB, uB, vB, tB);

  SANS_CHECK_CLOSE(rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(v, vB, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB, small_tol, close_tol );

  bc.state( param, x, y, time, nx, ny, nt, qI, qB );

  Real rhoB2, uB2, vB2, tB2;
  pde.variableInterpreter().eval( qB, rhoB2, uB2, vB2, tB2 );

  SANS_CHECK_CLOSE(   rhoB, rhoB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     uB,   uB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     vB,   vB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     tB,   tB2, small_tol, close_tol );

  qI = pde.setDOFFrom( DensityVelocityPressure2D<Real>(rhoB, uB, vB, tB) );
  BOOST_CHECK_EQUAL( true, bc.isValidState(nx, ny, nt, qI) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_Symmetry_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler2D<BCTypeSymmetry_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  BCClass bc(pde);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 1 );

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p, qn;

  x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  p  = R*rho*t;
  qn = nx*u + ny*v;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  Real uBTrue = u - qn*nx;
  Real vBTrue = v - qn*ny;

  Real pBTrue = p;

  ArrayQ qI;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( qI, qdata );

  ArrayQ qB;
  bc.state( x, y, time, nx, ny, qI, qB );

  Real rhoB, uB, vB, tB;
  pde.variableInterpreter().eval( qB, rhoB, uB, vB, tB );
  Real pB = gas.pressure(rhoB, tB);

  SANS_CHECK_CLOSE(    rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE( uBTrue,   uB, small_tol, close_tol );
  SANS_CHECK_CLOSE( vBTrue,   vB, small_tol, close_tol );
  SANS_CHECK_CLOSE( pBTrue,   pB, small_tol, close_tol );

  bc.state( param, x, y, time, nx, ny, qI, qB );

  Real rhoB2, uB2, vB2, tB2;
  pde.variableInterpreter().eval( qB, rhoB2, uB2, vB2, tB2 );
  Real pB2 = gas.pressure(rhoB2, tB2);

  SANS_CHECK_CLOSE(   rhoB, rhoB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     uB,   uB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     vB,   vB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     pB,   pB2, small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_fullstate )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler2D<BCTypeFullState_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p;

  x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  p  = R*rho*t;

  ArrayQ q;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  BCClass bc(pde, qdata, false);
  BCClass bc2(pde, qdata, true);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 4 );

  ArrayQ qI = 0;
  ArrayQ qB;

  bc.state(x, y, time, nx, ny, qI, qB);

  Real rhoB, uB, vB, tB;
  pde.variableInterpreter().eval(qB, rhoB, uB, vB, tB);

  SANS_CHECK_CLOSE(rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(v, vB, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB, small_tol, close_tol );

  qI = pde.setDOFFrom( DensityVelocityPressure2D<Real>(rhoB, uB, vB, tB) );
  BOOST_CHECK_EQUAL( true, bc.isValidState(nx, ny, qI) );
  BOOST_CHECK_EQUAL( true, bc2.isValidState(nx, ny, qI) );

  qI = pde.setDOFFrom( DensityVelocityPressure2D<Real>(rhoB, -uB, -vB, tB) );
  BOOST_CHECK_EQUAL( false, bc.isValidState(nx, ny, qI) );
  BOOST_CHECK_EQUAL( true, bc2.isValidState(nx, ny, qI) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_fullstate_solution )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef SolutionFunction_Euler2D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler2D<BCTypeFunction_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p;

  x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  p  = R*rho*t;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  ArrayQ q;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  BCClass::Function_ptr constsol( new SolutionFunction_EulerClass(gas, rho, u, v, p) );
  BCClass bc(pde, constsol, true);
  BCClass bc2(pde, constsol, false);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 4 );

  ArrayQ qI = 0;
  ArrayQ qB;

  bc.state(x, y, time, nx, ny, qI, qB);

  Real rhoB, uB, vB, tB;
  pde.variableInterpreter().eval(qB, rhoB, uB, vB, tB);

  SANS_CHECK_CLOSE(rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(v, vB, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB, small_tol, close_tol );

  bc.state( param, x, y, time, nx, ny, qI, qB );

  Real rhoB2, uB2, vB2, tB2;
  pde.variableInterpreter().eval( qB, rhoB2, uB2, vB2, tB2 );

  SANS_CHECK_CLOSE(   rhoB, rhoB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     uB,   uB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     vB,   vB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     tB,   tB2, small_tol, close_tol );

  qI = pde.setDOFFrom( DensityVelocityPressure2D<Real>(rhoB, uB, vB, tB) );
  BOOST_CHECK_EQUAL( true, bc.isValidState(nx, ny, qI) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_SubsonicInflow_PtTta_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p;

  x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  p  = R*rho*t;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  ArrayQ q;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  Real TtSpec = t*(1+0.2*0.1*0.1);
  Real PtSpec = p*pow( (1+0.2*0.1*0.1), 3.5 );
  Real aSpec = 0.1;

  BCClass bc(pde, TtSpec, PtSpec, aSpec);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 3 );
  BOOST_CHECK_EQUAL( false, bc.hasFluxViscous() );

  ArrayQ qB = 0;

  bc.state(x, y, time, nx, ny, q, qB);

  Real rhoB, uB, vB, tB;
  pde.variableInterpreter().eval(qB, rhoB, uB, vB, tB);

  Real rhoTrue = 0.789121117837660;
  Real uTrue = 0.614296756252870;
  Real vTrue = 0.061635263601788;
  Real tTrue = 0.852845639120693;

  SANS_CHECK_CLOSE(rhoTrue, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(uTrue, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(vTrue, vB, small_tol, close_tol );
  SANS_CHECK_CLOSE(tTrue, tB, small_tol, close_tol );


  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  ArrayQ qx = {rhox, ux, vx, px};
  ArrayQ qy = {rhoy, uy, vy, py};

  ArrayQ Fn = 0;
  bc.fluxNormal( x, y, time, nx, ny, q, qx, qy, qB, Fn);

  ArrayQ fx = 0;
  ArrayQ fy = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, time, qB, fx, fy);
  FnTrue += fx*nx + fy*ny;

  SANS_CHECK_CLOSE(    FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE(    FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE(    FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE(    FnTrue(3), Fn(3), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_SubsonicInflow_sHqt_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler2D<BCTypeInflowSubsonic_sHqt_mitState, PDEClass> BCClass;

  typedef SolutionFunction_Euler2D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler2D<BCTypeFunctionInflow_sHqt_mitState, PDEClass> BCClass2;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p;

  x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.0784; v = -0.0231; t = 0.987;
  p  = R*rho*t;

  ArrayQ q;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  Real sSpec = log(p / pow(rho,gamma));
  Real HSpec = gas.enthalpy(rho,t) + 0.5*(u*u+v*v);
  Real VxSpec = u;
  Real VySpec = v;

  BCClass bc(pde, sSpec, HSpec, VxSpec, VySpec);

  BCClass2::Function_ptr constsol( new SolutionFunction_EulerClass(gas, rho, u, v, p) );
  BCClass2 bc2(pde, constsol);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 3 );
  BOOST_CHECK_EQUAL( false, bc.hasFluxViscous() );

  ArrayQ qB = 0;
  ArrayQ qB2 = 0;

  bc.state(x, y, time, nx, ny, q, qB);
  bc2.state(x, y, time, nx, ny, q, qB2);

  Real rhoB, uB, vB, tB;
  pde.variableInterpreter().eval(qB, rhoB, uB, vB, tB);

  Real rhoB2, uB2, vB2, tB2;
  pde.variableInterpreter().eval(qB2, rhoB2, uB2, vB2, tB2);

  Real rhoTrue = rho;
  Real uTrue = u;
  Real vTrue = v;
  Real tTrue = t;

  SANS_CHECK_CLOSE(rhoTrue, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(uTrue, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(vTrue, vB, small_tol, close_tol );
  SANS_CHECK_CLOSE(tTrue, tB, small_tol, close_tol );

  SANS_CHECK_CLOSE(rhoTrue, rhoB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(uTrue, uB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(vTrue, vB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(tTrue, tB2, small_tol, close_tol );


  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  ArrayQ qx = {rhox, ux, vx, px};
  ArrayQ qy = {rhoy, uy, vy, py};

  ArrayQ Fn = 0;
  ArrayQ Fn2 = 0;
  bc.fluxNormal( x, y, time, nx, ny, q, qx, qy, qB, Fn);
  bc2.fluxNormal( x, y, time, nx, ny, q, qx, qy, qB, Fn2);

  ArrayQ fx = 0;
  ArrayQ fy = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, time, qB, fx, fy);
  FnTrue += fx*nx + fy*ny;

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );

  SANS_CHECK_CLOSE( FnTrue(0), Fn2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn2(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn2(3), small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_outflow_pressure_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, DLA::MatrixSymS<2, Real>, Real>::type ParamType;

  typedef BCParameters< BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;

  const Real gamma = 1.4;
  const Real R = 287.04;        // J/(kg K)
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  const Real small_tol = 3.e-12;
  const Real close_tol = 3.e-12;

  //Pydict constructor
  PyDict BCSubsonicOut;
  BCSubsonicOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;

  BCSubsonicOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec]
                 = 101325.0;

  BCClass bc(pde, BCSubsonicOut);
  typedef BCClass::ArrayQ<Real> ArrayQ;

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 3 );
  BOOST_CHECK_EQUAL( false, bc.hasFluxViscous() );

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p;

  // Test conditions - M = 0.3, T0 = 300.1, P0 = 101325; alpha = 0.1 rad
  x = y = time = 0;   // not actually used in functions
  nx = 0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137;
  t = 300.1;
  p  = R*rho*t;
  DLA::MatrixSymS<2, Real> hMat = {{2}, {3, 4}};
  ParamType param(hMat, 0.0); // grid spacing and jump values

  Real M = 0.3;
  Real c =  sqrt(1.4*287.04*300.1);
  Real alpha = 0.1;
  u = M*c*cos(alpha);
  v = M*c*sin(alpha);

  ArrayQ q, qB, qB2, qBTrue;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  bc.state(x,y,time,nx,ny,q,qB);
  bc.state(param,x,y,time,nx,ny,q,qB2);

  qBTrue[0] = 1.164915860299754;
  qBTrue[1] = 95.7574160100521;
  qBTrue[2] = 7.428571205323625;
  qBTrue[3] = 101325.0;

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );

  SANS_CHECK_CLOSE( qB(0), qB2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qB(1), qB2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qB(2), qB2(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qB(3), qB2(3), small_tol, close_tol );


  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  ArrayQ qx = {rhox, ux, vx, px};
  ArrayQ qy = {rhoy, uy, vy, py};

  ArrayQ Fn = 0, Fn2 = 0;
  bc.fluxNormal( x, y, time, nx, ny, q, qx, qy, qB, Fn);
  bc.fluxNormal( param, x, y, time, nx, ny, q, qx, qy, qB, Fn2);

  ArrayQ fx = 0;
  ArrayQ fy = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, time, qB, fx, fy);
  FnTrue += fx*nx + fy*ny;

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );

  SANS_CHECK_CLOSE( Fn(0), Fn2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(1), Fn2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(2), Fn2(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(3), Fn2(3), small_tol, close_tol );

  // state is always valid
  BOOST_CHECK( bc.isValidState(nx, ny, q) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_Reflect_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler2D<BCTypeReflect_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, DLA::MatrixSymS<2, Real>, Real>::type ParamType;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  BCClass bc(pde);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 1 );

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p, qn;

  x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  p  = R*rho*t;
  qn = nx*u + ny*v;
  DLA::MatrixSymS<2, Real> hMat = {{2}, {3, 4}};
  ParamType param(hMat, 0.0); // grid spacing and jump values

  Real uBTrue = u - 2*qn*nx;
  Real vBTrue = v - 2*qn*ny;

  Real pBTrue = p;

  ArrayQ qI;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( qI, qdata );

  Real rhox = 0.01, rhoy = -0.025;
  Real ux = 0.049, uy =  0.072;
  Real vx = 0.143, vy = -0.024;
  Real px = 0.002, py = -4.231;

  ArrayQ qIx = {rhox, ux, vx, px};
  ArrayQ qIy = {rhoy, uy, vy, py};

  ArrayQ qB;
  bc.state( x, y, time, nx, ny, qI, qB );

  Real rhoB, uB, vB, tB;
  pde.variableInterpreter().eval( qB, rhoB, uB, vB, tB );
  Real pB = gas.pressure(rhoB, tB);

  SANS_CHECK_CLOSE(    rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE( uBTrue,   uB, small_tol, close_tol );
  SANS_CHECK_CLOSE( vBTrue,   vB, small_tol, close_tol );
  SANS_CHECK_CLOSE( pBTrue,   pB, small_tol, close_tol );

  bc.state( param, x, y, time, nx, ny, qI, qB );

  Real rhoB2, uB2, vB2, tB2;
  pde.variableInterpreter().eval( qB, rhoB2, uB2, vB2, tB2 );
  Real pB2 = gas.pressure(rhoB2, tB2);

  SANS_CHECK_CLOSE(   rhoB, rhoB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     uB,   uB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     vB,   vB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     pB,   pB2, small_tol, close_tol );

  // check the normal flux
  ArrayQ Fn = 0, FnTrue = 0;
  bc.fluxNormal( x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);

  pde.fluxAdvectiveUpwind( x, y, time, qI, qB, nx, ny, FnTrue );

  SANS_CHECK_CLOSE( FnTrue[0], Fn[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue[1], Fn[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue[2], Fn[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue[3], Fn[3], small_tol, close_tol );

  BOOST_CHECK( bc.isValidState(nx, ny, qI) == true );

  BOOST_CHECK( bc.hasFluxViscous() == pde.hasFluxViscous() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_outflow_supersonic_pressure_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler2D<BCTypeOutflowSupersonic_Pressure_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, DLA::MatrixSymS<2, Real>, Real>::type ParamType;

  typedef BCParameters< BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;

  const Real gamma = 1.4;
  const Real R = 287.04;        // J/(kg K)
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  const Real small_tol = 3.e-12;
  const Real close_tol = 3.e-12;

  //Pydict constructor
  PyDict BCSupersonicOut;
  BCSupersonicOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSupersonic_Pressure_mitState;

  BCSupersonicOut[BCEuler2DParams<BCTypeOutflowSupersonic_Pressure_mitState>::params.pSpec]
                 = 101325.0;

  BCClass bc(pde, BCSupersonicOut);
  typedef BCClass::ArrayQ<Real> ArrayQ;

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 3 );
  BOOST_CHECK_EQUAL( false, bc.hasFluxViscous() );

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p;

  // Test conditions - M = 0.3, T0 = 300.1, P0 = 101325; alpha = 0.1 rad
  x = y = time = 0;   // not actually used in functions
  nx = 0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137;
  t = 300.1;
  p  = R*rho*t;
  DLA::MatrixSymS<2, Real> hMat = {{2}, {3, 4}};
  ParamType param(hMat, 0.0); // grid spacing and jump values

  Real M = 0.3;
  Real c =  sqrt(1.4*287.04*300.1);
  Real alpha = 0.1;
  u = M*c*cos(alpha);
  v = M*c*sin(alpha);

  ArrayQ q, qB, qB2, qBTrue;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  bc.state(x,y,time,nx,ny,q,qB);
  bc.state(param,x,y,time,nx,ny,q,qB2);

  qBTrue[0] = 1.164915860299754;
  qBTrue[1] = 95.7574160100521;
  qBTrue[2] = 7.428571205323625;
  qBTrue[3] = 101325.0;

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );

  SANS_CHECK_CLOSE( qB(0), qB2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qB(1), qB2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qB(2), qB2(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qB(3), qB2(3), small_tol, close_tol );


  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  ArrayQ qx = {rhox, ux, vx, px};
  ArrayQ qy = {rhoy, uy, vy, py};

  ArrayQ Fn = 0, Fn2 = 0;
  bc.fluxNormal( x, y, time, nx, ny, q, qx, qy, qB, Fn);
  bc.fluxNormal( param, x, y, time, nx, ny, q, qx, qy, qB, Fn2);

  ArrayQ fx = 0;
  ArrayQ fy = 0;
  ArrayQ f = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvectiveUpwind(x, y, time, q, qB, nx, ny, f);
  FnTrue += f;

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );

  SANS_CHECK_CLOSE( Fn(0), Fn2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(1), Fn2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(2), Fn2(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(3), Fn2(3), small_tol, close_tol );

  // state is always valid
  BOOST_CHECK( bc.isValidState(nx, ny, q) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_FullStateRadialInflow )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler2D<BCTypeFullStateRadialInflow_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p;

  x = y = time = 1.0;   // not actually used in functions
  nx = -1.0/sqrt(2.0);  ny = -1.0/sqrt(2.0);   // Note: magnitude = 1
  rho = 1.137; u = 0.284; v = 0.284; t = 0.987;
  p  = R*rho*t;
  Real V = sqrt(u*u + v*v);

  ArrayQ q;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  BCClass bc(pde, false, rho, V, t);
  BCClass bc2(pde, true, rho, V, t);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 4 );

  ArrayQ qI = 0;
  ArrayQ qB;

  bc.state(x, y, time, nx, ny, qI, qB);

  Real rhoB, uB, vB, tB;
  pde.variableInterpreter().eval(qB, rhoB, uB, vB, tB);

  SANS_CHECK_CLOSE(rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(v, vB, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB, small_tol, close_tol );

  qI = pde.setDOFFrom( DensityVelocityPressure2D<Real>(rhoB, uB, vB, tB) );
  BOOST_CHECK_EQUAL( true, bc.isValidState(nx, ny, qI) );
  BOOST_CHECK_EQUAL( true, bc2.isValidState(nx, ny, qI) );

  qI = pde.setDOFFrom( DensityVelocityPressure2D<Real>(rhoB, -uB, -vB, tB) );
  BOOST_CHECK_EQUAL( false, bc.isValidState(nx, ny, qI) );
  BOOST_CHECK_EQUAL( true, bc2.isValidState(nx, ny, qI) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/BCEuler2D_pattern.txt", true );

  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  {
  typedef BCEuler2D<BCTypeSymmetry, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {-7, 0, 0, 0};
  BCClass bc(pde, bcdata);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler2D<BCTypeInflowSupersonic, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {7, 3.551, -4.993, 2.997};
  BCClass bc(pde, bcdata);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {-7.153, 3.551, 4.993, 0};
  BCClass bc(pde, bcdata);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler2D<BCTypeInflowSubsonic_sHqt_BN, PDEClass> BCClass;

  const Real sSpec = 1.0;
  const Real HSpec = 2.0;
  const Real aSpec = 0.1;
  BCClass bc(pde, sSpec, HSpec, aSpec);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {-7, 0, 0, 0};
  BCClass bc(pde, bcdata);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler2D<BCTypeOutflowSubsonic_Pressure_BN, PDEClass> BCClass;

  const Real pSpec = 2.3;
  BCClass bc(pde, pSpec);

  bc.dump( 2, output );
  }

  {
  typedef SolutionFunction_Euler2D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler2D<BCTypeFunctionInflow_sHqt_BN, PDEClass> BCClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  BCClass::Function_ptr constsol( new SolutionFunction_EulerClass(gas, 1.1, 1.3, -0.4, 5.45) );
  BCClass bc(pde, constsol);

  bc.dump( 2, output );

  }

  {
  typedef BCEuler2D<BCTypeTimeIC, PDEClass> BCClass;

  BCClass bc(pde, DensityVelocityPressure2D<Real>(1.1, 1.3, -0.4, 5.45));

  bc.dump( 2, output );
  }

  {
  typedef SolutionFunction_Euler2D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler2D<BCTypeTimeIC_Function, PDEClass> BCClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  BCClass::Function_ptr constsol( new SolutionFunction_EulerClass(gas, 1.1, 1.3, -0.4, 5.45) );
  BCClass bc(pde, constsol);

  bc.dump( 2, output );

  }

  {
  typedef BCEuler2D<BCTypeSymmetry, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {-7, 0, 0, 0};
  BCClass bc(pde, bcdata);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler2D<BCTypeInflowSupersonic, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {7, 3.551, -4.993, 2.997};
  BCClass bc(pde, bcdata);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {-7.153, 3.551, 4.993, 0};
  BCClass bc(pde, bcdata);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDEClass> BCClass;

  const Real PtSpec = 1.0;
  const Real TtSpec = 2.0;
  const Real aoaSpec = 3.0;
  BCClass bc(pde, PtSpec, TtSpec, aoaSpec);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler2D<BCTypeInflowSubsonic_sHqt_mitState, PDEClass> BCClass;

  const Real sSpec = 1.0;
  const Real HSpec = 2.0;
  const Real VxSpec = 3.0;
  const Real VySpec = 4.0;
  BCClass bc(pde, sSpec, HSpec, VxSpec, VySpec);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler2D<BCTypeInflowSubsonic_sHqt_BN, PDEClass> BCClass;

  const Real sSpec = 1.0;
  const Real HSpec = 2.0;
  const Real aSpec = 0.1;
  BCClass bc(pde, sSpec, HSpec, aSpec);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {-7, 0, 0, 0};
  BCClass bc(pde, bcdata);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState, PDEClass> BCClass;

  const Real pSpec = 2.3;
  BCClass bc(pde, pSpec);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler2D<BCTypeOutflowSupersonic_Pressure_mitState, PDEClass> BCClass;

  const Real pSpec = 2.3;
  BCClass bc(pde, pSpec);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler2D<BCTypeOutflowSubsonic_Pressure_BN, PDEClass> BCClass;

  const Real pSpec = 2.3;
  BCClass bc(pde, pSpec);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler2D<BCTypeFullState_mitState, PDEClass> BCClass;

  bool upwind = true;
  BCClass bc(pde, DensityVelocityPressure2D<Real>(1.1, 1.3, -0.4, 5.45), upwind);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler2D<BCTypeSymmetry_mitState, PDEClass> BCClass;

  BCClass bc(pde);

  bc.dump( 2, output );
  }

  {
  typedef SolutionFunction_Euler2D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler2D<BCTypeFunction_mitState, PDEClass> BCClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  BCClass::Function_ptr constsol( new SolutionFunction_EulerClass(gas, 1.1, 1.3, -0.4, 5.45) );
  BCClass bc(pde, constsol, true);

  bc.dump( 2, output );

  }

  {
  typedef SolutionFunction_Euler2D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler2D<BCTypeFunctionInflow_sHqt_mitState, PDEClass> BCClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  BCClass::Function_ptr constsol( new SolutionFunction_EulerClass(gas, 1.1, 1.3, -0.4, 5.45) );
  BCClass bc(pde, constsol);

  bc.dump( 2, output );

  }

  {
  typedef SolutionFunction_Euler2D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler2D<BCTypeFunctionInflow_sHqt_BN, PDEClass> BCClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  BCClass::Function_ptr constsol( new SolutionFunction_EulerClass(gas, 1.1, 1.3, -0.4, 5.45) );
  BCClass bc(pde, constsol);

  bc.dump( 2, output );

  }

  {
  typedef BCEuler2D<BCTypeTimeIC, PDEClass> BCClass;

  BCClass bc(pde, DensityVelocityPressure2D<Real>(1.1, 1.3, -0.4, 5.45));

  bc.dump( 2, output );
  }

  {
  typedef SolutionFunction_Euler2D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler2D<BCTypeTimeIC_Function, PDEClass> BCClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  BCClass::Function_ptr constsol( new SolutionFunction_EulerClass(gas, 1.1, 1.3, -0.4, 5.45) );
  BCClass bc(pde, constsol);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler2D<BCTypeReflect_mitState, PDEClass> BCClass;

  BCClass bc(pde);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler2D<BCTypeFullStateRadialInflow_mitState, PDEClass> BCClass;

  bool upwind = true;
  BCClass bc(pde, upwind, 1.1, 2.3, 5.45);

  bc.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
