// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// SolutionFunction_Euler1D_btest
//
// test of 1-D solution function class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/SolutionFunction_Euler1D.h"

// Explicitly instantiate the class so coverage information is correct
namespace SANS
{
template class SolutionFunction_Euler1D_Const<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>>;
template class SolutionFunction_Euler1D_Riemann<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>>;
template class SolutionFunction_Euler1D_DensityPulse<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>>;
template class SolutionFunction_Euler1D_ArtShock<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>>;
template class SolutionFunction_Euler1D_SineBackPressure<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( SolutionFunction_Euler1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_Const )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef SolutionFunction_Euler1D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  SolutionClass constsol(gas, 1.1, 1.2, 1.4);

  Real x = 1;
  Real time = 0;
  ArrayQ sln = 0;
  sln = constsol(x, time);

  const Real tol = 1.e-13;
  BOOST_CHECK_CLOSE( 1.1, sln[0], tol );
  BOOST_CHECK_CLOSE( 1.2, sln[1], tol );
  BOOST_CHECK_CLOSE( 1.4, sln[2], tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_Riemann )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef SolutionFunction_Euler1D_Riemann<TraitsSizeEuler, TraitsModelEulerClass> SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  SolutionClass constsol(gas, 0.5, 1.1, 1.2, 1.4, 2.1, 2.2, 2.4);

  Real x = 0;
  Real time = 0;
  ArrayQ sln = 0;
  sln = constsol(x, time);

  const Real tol = 1.e-13;
  BOOST_CHECK_CLOSE( 1.1, sln[0], tol );
  BOOST_CHECK_CLOSE( 1.2, sln[1], tol );
  BOOST_CHECK_CLOSE( 1.4, sln[2], tol );


  x = 1;
  sln = constsol(x, time);
  BOOST_CHECK_CLOSE( 2.1, sln[0], tol );
  BOOST_CHECK_CLOSE( 2.2, sln[1], tol );
  BOOST_CHECK_CLOSE( 2.4, sln[2], tol );

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.interface] = 0.5;
  solnArgs[SolutionClass::ParamsType::params.rhoL] = 1.1;
  solnArgs[SolutionClass::ParamsType::params.uL] = 1.2;
  solnArgs[SolutionClass::ParamsType::params.pL] = 1.4;
  solnArgs[SolutionClass::ParamsType::params.rhoR] = 2.1;
  solnArgs[SolutionClass::ParamsType::params.uR] = 2.2;
  solnArgs[SolutionClass::ParamsType::params.pR] = 2.4;
  solnArgs[SolutionClass::ParamsType::params.gasModel] = gasModelDict;
  SolutionClass::ParamsType::checkInputs(solnArgs);

  SolutionClass solnExact(solnArgs);

  x = 1;
  sln = solnExact(x, time);
  BOOST_CHECK_CLOSE( 2.1, sln[0], tol );
  BOOST_CHECK_CLOSE( 2.2, sln[1], tol );
  BOOST_CHECK_CLOSE( 2.4, sln[2], tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_DensityPulse )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef SolutionFunction_Euler1D_DensityPulse<TraitsSizeEuler, TraitsModelEulerClass> SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  SolutionClass constsol(gas, 0.5, 0.1, 0.5, 0.5, 1.2, 1.4);

  Real x = 0.5;
  Real time = 0;
  ArrayQ sln = 0;
  sln = constsol(x, time);

  const Real tol = 1.e-13;
  BOOST_CHECK_CLOSE( 1.0, sln[0], tol );
  BOOST_CHECK_CLOSE( 1.2, sln[1], tol );
  BOOST_CHECK_CLOSE( 1.4, sln[2], tol );

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.center] = 0.5;
  solnArgs[SolutionClass::ParamsType::params.width] = 0.1;
  solnArgs[SolutionClass::ParamsType::params.height] = 0.5;
  solnArgs[SolutionClass::ParamsType::params.rho] = 0.5;
  solnArgs[SolutionClass::ParamsType::params.u] = 1.2;
  solnArgs[SolutionClass::ParamsType::params.p] = 1.4;
  solnArgs[SolutionClass::ParamsType::params.gasModel] = gasModelDict;
  SolutionClass::ParamsType::checkInputs(solnArgs);

  SolutionClass solnExact(solnArgs);

  sln = solnExact(x, time);
  BOOST_CHECK_CLOSE( 1.0, sln[0], tol );
  BOOST_CHECK_CLOSE( 1.2, sln[1], tol );
  BOOST_CHECK_CLOSE( 1.4, sln[2], tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_SineBackPressure )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef SolutionFunction_Euler1D_SineBackPressure<TraitsSizeEuler, TraitsModelEulerClass> SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 1.0;
  GasModel gas(gamma, R);

  const Real rho = 1.0;
  const Real u = 0.5;
  const Real p = 0.78;
  const Real dp = 0.05;
  const Real dt = 0.1;

  SolutionClass soln(gas, rho, u, p, dp, dt);

  Real x = 0;
  Real time = 0.58;
  ArrayQ sln = 0;
  sln = soln(x, time);

  Real pTrue = p - dp*sin(2*PI*time / dt);

  const Real tol = 1.e-3;
  BOOST_CHECK_CLOSE( rho, sln[0], tol );
  BOOST_CHECK_CLOSE( u, sln[1], tol );
  BOOST_CHECK_CLOSE( pTrue, sln[2], tol );

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 1.0;

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.rho] = rho;
  solnArgs[SolutionClass::ParamsType::params.u] = u;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  solnArgs[SolutionClass::ParamsType::params.dp] = dp;
  solnArgs[SolutionClass::ParamsType::params.dt] = dt;
  solnArgs[SolutionClass::ParamsType::params.gasModel] = gasModelDict;
  SolutionClass::ParamsType::checkInputs(solnArgs);

  SolutionClass solnExact(solnArgs);

  sln = solnExact(x, time);
  BOOST_CHECK_CLOSE( rho, sln[0], tol );
  BOOST_CHECK_CLOSE( u, sln[1], tol );
  BOOST_CHECK_CLOSE( pTrue, sln[2], tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_ArtShock )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef SolutionFunction_Euler1D_ArtShock<TraitsSizeEuler, TraitsModelEulerClass> SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 1.0;
  GasModel gas(gamma, R);

  const Real xthroat = 0.5;
  const Real xshock = 0.8150186197870202;
  const Real Tt = 1.0;
  const Real Pt1 = 1.0;
  const Real Pt2 = 0.947561264038085938;
  const Real pe = 1.0/1.4;

  SolutionClass soln(gas, xthroat, xshock, Pt1, Pt2, Tt, pe);

  Real x = 0.75;
  Real time = 0;
  ArrayQ sln = 0;
  sln = soln(x, time);

  Real rhoTrue = 0.449896097183227539;
  Real uTrue =1.38361537456512451;
  Real pTrue =0.326856493949890137;

  const Real tol = 1.e-3;
  BOOST_CHECK_CLOSE( rhoTrue, sln[0], tol );
  BOOST_CHECK_CLOSE( uTrue, sln[1], tol );
  BOOST_CHECK_CLOSE( pTrue, sln[2], tol );

  x = 0.9;
  sln = soln(x, time);

  rhoTrue = 0.764001910756522462;
  uTrue = 0.760043371935598344;
  pTrue = 0.700953609011633949;

  BOOST_CHECK_CLOSE( rhoTrue, sln[0], tol );
  BOOST_CHECK_CLOSE( uTrue, sln[1], tol );
  BOOST_CHECK_CLOSE( pTrue, sln[2], tol );

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 1.0;

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.xThroat] = xthroat;
  solnArgs[SolutionClass::ParamsType::params.xShock] = xshock;
  solnArgs[SolutionClass::ParamsType::params.pt1] = Pt1;
  solnArgs[SolutionClass::ParamsType::params.pt2] = Pt2;
  solnArgs[SolutionClass::ParamsType::params.Tt] = Tt;
  solnArgs[SolutionClass::ParamsType::params.pe] = pe;
  solnArgs[SolutionClass::ParamsType::params.gasModel] = gasModelDict;
  SolutionClass::ParamsType::checkInputs(solnArgs);

  SolutionClass solnExact(solnArgs);

  sln = solnExact(x, time);
  BOOST_CHECK_CLOSE( rhoTrue, sln[0], tol );
  BOOST_CHECK_CLOSE( uTrue, sln[1], tol );
  BOOST_CHECK_CLOSE( pTrue, sln[2], tol );
}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_SSMEIC )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef SolutionFunction_Euler1D_SSME<TraitsSizeEuler, TraitsModelEulerClass> SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;

  const Real gamma = 1.1875;
  const Real R = 942;
  GasModel gas(gamma, R);

  const Real Tc = 1.0;
  const Real rhoc = 1.0;

  SolutionClass soln(gas, gamma, R, rhoc, Tc);

  Real x = 3.0734;
  Real time = 0;
  ArrayQ sln = 0;
  sln = soln(x, time);

  Real rhoTrue = 0.002869613386940;
  Real uTrue = 89.165143956378429;
  Real pTrue = 0.902020022888405;

  const Real tol = 1.e-3;
  BOOST_CHECK_CLOSE( rhoTrue, sln[0], tol );
  BOOST_CHECK_CLOSE( uTrue, sln[1], tol );
  BOOST_CHECK_CLOSE( pTrue, sln[2], tol );

  x = 0.0;

  sln = soln(x, time);

  rhoTrue =  0.62006466;
  uTrue = 31.980351035420952;
  pTrue = 534.0351149549962;

  BOOST_CHECK_CLOSE( rhoTrue, sln[0], tol );
  BOOST_CHECK_CLOSE( uTrue, sln[1], tol );
  BOOST_CHECK_CLOSE( pTrue, sln[2], tol );

}



//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
