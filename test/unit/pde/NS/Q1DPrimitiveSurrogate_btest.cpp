// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Q1DPrimitiveSurrogate_btest
// testing of Q1D<QTypePrimitiveSurrogate, TraitsSizeEuler> class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/NS/Q1DPrimitiveSurrogate.h"
#include "pde/NS/TraitsEuler.h"
#include "Surreal/SurrealS.h"

#include <string>
#include <iostream>


//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
template class Q1D<QTypePrimitiveSurrogate, TraitsSizeEuler>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Q1DPrimitiveSurrogate_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test )
{
  const Real tol = 1.e-13;

  typedef Q1D<QTypePrimitiveSurrogate, TraitsSizeEuler> QInterpret;
  BOOST_CHECK( QInterpret::N == 3 );

  typedef QInterpret::ArrayQ<Real> ArrayQ;
  BOOST_CHECK( ArrayQ::M == 3 );

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  // set/eval
  ArrayQ q;
  Real rho, u, t, p;
  Real rho1, u1, t1;
  Real rho2, u2, t2;

  rho = 1.225;          // kg/m^3 (standard atmosphere)
  t   = 288.15;         // K (standard atmosphere)
  u   = 15.03;          // m/s
  p   = R*rho*t;

  Real E = gas.energy(rho, t) + 0.5*u*u;

  Real qDataPrim[3] = {rho, u, t};
  string qNamePrim[3] = {"Density", "VelocityX", "Temperature"};
  qInterpret.setFromPrimitive( q, qDataPrim, qNamePrim, 3 );
  qInterpret.eval( q, rho1, u1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );

  qInterpret.evalDensity( q, rho1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );

  qInterpret.evalTemperature( q, t1 );
  BOOST_CHECK_CLOSE( t, t1, tol );

  qInterpret.evalVelocity( q, u1 );
  BOOST_CHECK_CLOSE( u, u1, tol );

  DensityVelocityPressure1D<Real> prim1(rho, u, p);
  q = 1;
  qInterpret.setFromPrimitive( q, prim1 );
  qInterpret.eval( q, rho1, u1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );

  DensityVelocityTemperature1D<Real> prim2(rho, u, t);
  q = 2;
  qInterpret.setFromPrimitive( q, prim2 );
  qInterpret.eval( q, rho1, u1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );

  Conservative1D<Real> cons(rho, rho*u, rho*E);
  q = 3;
  qInterpret.setFromPrimitive( q, cons );
  qInterpret.eval( q, rho1, u1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );


  // copy constructor
  QInterpret qInterpret2(qInterpret);

  qInterpret2.eval( q, rho2, u2, t2 );
  BOOST_CHECK_CLOSE( rho, rho2, tol );
  BOOST_CHECK_CLOSE( u, u2, tol );
  BOOST_CHECK_CLOSE( t, t2, tol );

  // gradient
  Real rhox, ux, tx, px;
  Real rhox1, ux1, tx1;

  rhox = 0.37, ux = -0.85, px = 1.71;
  tx = t*(px/p - rhox/rho);

  Real qxData[3] = {rhox, ux, tx};
  ArrayQ qx(qxData, 3);

  qInterpret.evalGradient( q, qx, rhox1, ux1, tx1 );
  BOOST_CHECK_CLOSE( rhox, rhox1, tol );
  BOOST_CHECK_CLOSE(   ux,   ux1, tol );
  BOOST_CHECK_CLOSE(   tx,   tx1, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( evalJacobian_aboveCritical )
{
  typedef Q1D<QTypePrimitiveSurrogate, TraitsSizeEuler> QInterpret;

  typedef QInterpret::ArrayQ< Real > ArrayQ;
  typedef QInterpret::ArrayQ< SurrealS<QInterpret::N> > ArrayQSurreal;

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas); // q is above critical values

  ArrayQ q, rho_q, u_q, t_q;
  ArrayQSurreal qSurreal;
  SurrealS<QInterpret::N> rho, u, t;

  qInterpret.setFromPrimitive( q, DensityVelocityTemperature1D<Real>(1.225, 15.03, 288.15) );

  qInterpret.evalJacobian(q, rho_q, u_q, t_q);

  qSurreal = q;

  qSurreal[0].deriv(0) = 1;
  qSurreal[1].deriv(1) = 1;
  qSurreal[2].deriv(2) = 1;

  qInterpret.eval(qSurreal, rho, u, t);

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  SANS_CHECK_CLOSE( rho.deriv(0), rho_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(1), rho_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(2), rho_q[2], small_tol, close_tol )

  SANS_CHECK_CLOSE( u.deriv(0), u_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(1), u_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(2), u_q[2], small_tol, close_tol )

  SANS_CHECK_CLOSE( t.deriv(0), t_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(1), t_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(2), t_q[2], small_tol, close_tol )
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( evalJacobian_belowCritical )
{
  typedef Q1D<QTypePrimitiveSurrogate, TraitsSizeEuler> QInterpret;

  typedef QInterpret::ArrayQ< Real > ArrayQ;
  typedef QInterpret::ArrayQ< SurrealS<QInterpret::N> > ArrayQSurreal;

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas, 2, 300); // q is below critical values

  ArrayQ q, rho_q, u_q, t_q;
  ArrayQSurreal qSurreal;
  SurrealS<QInterpret::N> rho, u, t;

  qInterpret.setFromPrimitive( q, DensityVelocityTemperature1D<Real>(1.225, 15.03, 288.15) );

  qInterpret.evalJacobian(q, rho_q, u_q, t_q);

  qSurreal = q;

  qSurreal[0].deriv(0) = 1;
  qSurreal[1].deriv(1) = 1;
  qSurreal[2].deriv(2) = 1;

  qInterpret.eval(qSurreal, rho, u, t);

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  SANS_CHECK_CLOSE( rho.deriv(0), rho_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(1), rho_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(2), rho_q[2], small_tol, close_tol )

  SANS_CHECK_CLOSE( u.deriv(0), u_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(1), u_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(2), u_q[2], small_tol, close_tol )

  SANS_CHECK_CLOSE( t.deriv(0), t_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(1), t_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(2), t_q[2], small_tol, close_tol )
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef Q1D<QTypePrimitiveSurrogate, TraitsSizeEuler> QInterpret;
  typedef QInterpret::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  ArrayQ q;
  q(0) =  1;
  q(2) =  1;
  BOOST_CHECK( qInterpret.isValidState(q) );

  q(0) =  1;
  q(2) = -1;
  BOOST_CHECK( qInterpret.isValidState(q) );

  q(0) = -1;
  q(2) =  1;
  BOOST_CHECK( qInterpret.isValidState(q) );

  q(0) = -1;
  q(2) = -1;
  BOOST_CHECK( qInterpret.isValidState(q) );

  q(0) =  1;
  q(2) =  0;
  BOOST_CHECK( qInterpret.isValidState(q) );

  q(0) =  0;
  q(2) =  1;
  BOOST_CHECK( qInterpret.isValidState(q) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/Q1DPrimitiveSurrogate_pattern.txt", true );

  typedef Q1D<QTypePrimitiveSurrogate, TraitsSizeEuler> QInterpret;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  qInterpret.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
