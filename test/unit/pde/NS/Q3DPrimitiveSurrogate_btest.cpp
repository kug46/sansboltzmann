// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Q3DPrimitiveSurrogate_btest
// testing of Q3D<QTypePrimitiveSurrogate, TraitsSizeEuler> class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/NS/Q3DPrimitiveSurrogate.h"
#include "pde/NS/TraitsEuler.h"
#include "Surreal/SurrealS.h"

#include <string>
#include <iostream>


//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
template class Q3D<QTypePrimitiveSurrogate, TraitsSizeEuler>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Q3DPrimitiveSurrogate_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_est )
{
  typedef Q3D<QTypePrimitiveSurrogate, TraitsSizeEuler> QInterpret;
  BOOST_CHECK( QInterpret::N == 5 );

  typedef QInterpret::ArrayQ<Real> ArrayQ;
  BOOST_CHECK( ArrayQ::M == 5 );

  // make sure the vector comonents are set correctly
  BOOST_CHECK( QInterpret::ix == QInterpret::iu );
  BOOST_CHECK( QInterpret::iy == QInterpret::iv );
  BOOST_CHECK( QInterpret::iz == QInterpret::iw );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test )
{
  const Real tol = 1.e-13;

  typedef Q3D<QTypePrimitiveSurrogate, TraitsSizeEuler> QInterpret;
  typedef QInterpret::ArrayQ<Real> ArrayQ;

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  // set/eval
  ArrayQ q;
  Real rho, u, v, w, t, p, E;
  Real rho1, u1, v1, w1, t1;
  Real rho2, u2, v2, w2, t2;

  rho = 1.225;          // kg/m^3 (standard atmosphere)
  t   = 288.15;         // K (standard atmosphere)
  u   = 15.03;          // m/s
  v   =  1.71;
  w   = 10.25;
  p   = R*rho*t;
  E   = gas.energy(rho,t) + 0.5*(u*u + v*v + w*w);

  Real qDataPrim[5] = {rho, u, v, w, t};
  string qNamePrim[5] = {"Density", "VelocityX", "VelocityY", "VelocityZ", "Temperature"};
  qInterpret.setFromPrimitive( q, qDataPrim, qNamePrim, 5 );
  qInterpret.eval( q, rho1, u1, v1, w1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( w, w1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );

  qInterpret.evalDensity( q, rho1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );

  qInterpret.evalTemperature( q, t1 );
  BOOST_CHECK_CLOSE( t, t1, tol );

  qInterpret.evalVelocity( q, u1, v1, w1 );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( w, w1, tol );

  DensityVelocityPressure3D<Real> prim1(rho, u, v, w, p);
  q = 1;
  qInterpret.setFromPrimitive( q, prim1 );
  qInterpret.eval( q, rho1, u1, v1, w1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( w, w1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );

  DensityVelocityTemperature3D<Real> prim2(rho, u, v, w, t);
  q = 2;
  qInterpret.setFromPrimitive( q, prim2 );
  qInterpret.eval( q, rho1, u1, v1, w1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( w, w1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );

  Conservative3D<Real> prim3(rho, rho*u, rho*v, rho*w, rho*E);
  q = 2;
  qInterpret.setFromPrimitive( q, prim3 );
  qInterpret.eval( q, rho1, u1, v1, w1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( w, w1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );


  // copy constructor
  QInterpret qInterpret2(qInterpret);

  qInterpret2.eval( q, rho2, u2, v2, w2, t2 );
  BOOST_CHECK_CLOSE( rho, rho2, tol );
  BOOST_CHECK_CLOSE( u, u2, tol );
  BOOST_CHECK_CLOSE( v, v2, tol );
  BOOST_CHECK_CLOSE( w, w2, tol );
  BOOST_CHECK_CLOSE( t, t2, tol );


  // gradient
  Real rhox, ux, vx, wx, tx;
  Real rhoy, uy, vy, wy, ty;
  Real rhoz, uz, vz, wz, tz;
  Real rhox1, ux1, vx1, wx1, tx1;
  Real rhoy1, uy1, vy1, wy1, ty1;
  Real rhoz1, uz1, vz1, wz1, tz1;

  rhox = 0.37, ux = -0.85, vx = 0.09, wx =  0.36, tx = 1.71;
  rhoy = 2.31, uy =  1.65, vy = 0.87, wy = -1.07, ty = 0.29;
  rhoz = 0.45, uz =  0.12, vz = 1.27, wz =  0.07, tz = 2.12;

  ArrayQ qx = {rhox, ux, vx, wx, tx};
  ArrayQ qy = {rhoy, uy, vy, wy, ty};
  ArrayQ qz = {rhoz, uz, vz, wz, tz};

  qInterpret.evalGradient( q, qx, rhox1, ux1, vx1, wx1, tx1 );
  qInterpret.evalGradient( q, qy, rhoy1, uy1, vy1, wy1, ty1 );
  qInterpret.evalGradient( q, qz, rhoz1, uz1, vz1, wz1, tz1 );

  BOOST_CHECK_CLOSE( rhox, rhox1, tol );
  BOOST_CHECK_CLOSE(   ux,   ux1, tol );
  BOOST_CHECK_CLOSE(   vx,   vx1, tol );
  BOOST_CHECK_CLOSE(   wx,   wx1, tol );
  BOOST_CHECK_CLOSE(   tx,   tx1, tol );

  BOOST_CHECK_CLOSE( rhoy, rhoy1, tol );
  BOOST_CHECK_CLOSE(   uy,   uy1, tol );
  BOOST_CHECK_CLOSE(   vy,   vy1, tol );
  BOOST_CHECK_CLOSE(   wy,   wy1, tol );
  BOOST_CHECK_CLOSE(   ty,   ty1, tol );

  BOOST_CHECK_CLOSE( rhoz, rhoz1, tol );
  BOOST_CHECK_CLOSE(   uz,   uz1, tol );
  BOOST_CHECK_CLOSE(   vz,   vz1, tol );
  BOOST_CHECK_CLOSE(   wz,   wz1, tol );
  BOOST_CHECK_CLOSE(   tz,   tz1, tol );

  // Hessian
  // TODO: Hessian not implemented yet

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( evalJacobian_aboveCritical )
{
  typedef Q3D<QTypePrimitiveSurrogate, TraitsSizeEuler> QInterpret;

  typedef QInterpret::ArrayQ< Real > ArrayQ;
  typedef QInterpret::ArrayQ< SurrealS<QInterpret::N> > ArrayQSurreal;

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas); // q is above critical values

  ArrayQ q, rho_q, u_q, v_q, w_q, t_q;
  ArrayQSurreal qSurreal;
  SurrealS<QInterpret::N> rho, u, v, w, t;

  qInterpret.setFromPrimitive( q, DensityVelocityTemperature3D<Real>(1.225, 15.03, 1.71, 10.25, 288.15) );

  qInterpret.evalJacobian(q, rho_q, u_q, v_q, w_q, t_q);

  qSurreal = q;

  qSurreal[0].deriv(0) = 1;
  qSurreal[1].deriv(1) = 1;
  qSurreal[2].deriv(2) = 1;
  qSurreal[3].deriv(3) = 1;
  qSurreal[4].deriv(4) = 1;

  qInterpret.eval(qSurreal, rho, u, v, w, t);

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  SANS_CHECK_CLOSE( rho.deriv(0), rho_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(1), rho_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(2), rho_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(3), rho_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(4), rho_q[4], small_tol, close_tol )

  SANS_CHECK_CLOSE( u.deriv(0), u_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(1), u_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(2), u_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(3), u_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(4), u_q[4], small_tol, close_tol )

  SANS_CHECK_CLOSE( v.deriv(0), v_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(1), v_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(2), v_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(3), v_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(4), v_q[4], small_tol, close_tol )

  SANS_CHECK_CLOSE( w.deriv(0), w_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( w.deriv(1), w_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( w.deriv(2), w_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( w.deriv(3), w_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( w.deriv(4), w_q[4], small_tol, close_tol )

  SANS_CHECK_CLOSE( t.deriv(0), t_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(1), t_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(2), t_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(3), t_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(4), t_q[4], small_tol, close_tol )


  rho_q = 0;
  qInterpret.evalDensityJacobian(q, rho_q);

  SANS_CHECK_CLOSE( rho.deriv(0), rho_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(1), rho_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(2), rho_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(3), rho_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(4), rho_q[4], small_tol, close_tol )
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( evalJacobian_belowCritical )
{
  typedef Q3D<QTypePrimitiveSurrogate, TraitsSizeEuler> QInterpret;

  typedef QInterpret::ArrayQ< Real > ArrayQ;
  typedef QInterpret::ArrayQ< SurrealS<QInterpret::N> > ArrayQSurreal;

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas, 2, 300); // q is below critical values

  ArrayQ q, rho_q, u_q, v_q, w_q, t_q;
  ArrayQSurreal qSurreal;
  SurrealS<QInterpret::N> rho, u, v, w, t;

  qInterpret.setFromPrimitive( q, DensityVelocityTemperature3D<Real>(1.225, 15.03, 1.71, 10.25, 288.15) );

  qInterpret.evalJacobian(q, rho_q, u_q, v_q, w_q, t_q);

  qSurreal = q;

  qSurreal[0].deriv(0) = 1;
  qSurreal[1].deriv(1) = 1;
  qSurreal[2].deriv(2) = 1;
  qSurreal[3].deriv(3) = 1;
  qSurreal[4].deriv(4) = 1;

  qInterpret.eval(qSurreal, rho, u, v, w, t);

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  SANS_CHECK_CLOSE( rho.deriv(0), rho_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(1), rho_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(2), rho_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(3), rho_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(4), rho_q[4], small_tol, close_tol )

  SANS_CHECK_CLOSE( u.deriv(0), u_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(1), u_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(2), u_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(3), u_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(4), u_q[4], small_tol, close_tol )

  SANS_CHECK_CLOSE( v.deriv(0), v_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(1), v_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(2), v_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(3), v_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(4), v_q[4], small_tol, close_tol )

  SANS_CHECK_CLOSE( w.deriv(0), w_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( w.deriv(1), w_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( w.deriv(2), w_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( w.deriv(3), w_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( w.deriv(4), w_q[4], small_tol, close_tol )

  SANS_CHECK_CLOSE( t.deriv(0), t_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(1), t_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(2), t_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(3), t_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(4), t_q[4], small_tol, close_tol )


  rho_q = 0;
  qInterpret.evalDensityJacobian(q, rho_q);

  SANS_CHECK_CLOSE( rho.deriv(0), rho_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(1), rho_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(2), rho_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(3), rho_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(4), rho_q[4], small_tol, close_tol )
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef Q3D<QTypePrimitiveSurrogate, TraitsSizeEuler> QInterpret;
  typedef QInterpret::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  ArrayQ q;
  q = 0;
  q(0) =  1;
  q(4) =  1;
  BOOST_CHECK( qInterpret.isValidState(q) );

  q(0) =  1;
  q(4) = -1;
  BOOST_CHECK( qInterpret.isValidState(q) );

  q(0) = -1;
  q(4) =  1;
  BOOST_CHECK( qInterpret.isValidState(q) );

  q(0) = -1;
  q(4) = -1;
  BOOST_CHECK( qInterpret.isValidState(q) );

  q(0) =  1;
  q(4) =  0;
  BOOST_CHECK( qInterpret.isValidState(q) );

  q(0) =  0;
  q(4) =  1;
  BOOST_CHECK( qInterpret.isValidState(q) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/Q3DPrimitiveSurrogate_pattern.txt", true );

  typedef Q3D<QTypePrimitiveSurrogate, TraitsSizeEuler> QInterpret;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  qInterpret.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
