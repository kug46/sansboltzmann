// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// SolutionFunction_Euler2D_btest
//
// test of 2-D Euler solution function classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/AnalyticFunction/ScalarFunction2D.h"
//#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/SolutionFunction_Euler2D.h"

// Explicitly instantiate the class so coverage information is correct
namespace SANS
{
template class SolutionFunction_Euler2D_Const<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>>;
template class SolutionFunction_Euler2D_Wake<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( SolutionFunction_Euler2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_Const )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef SolutionFunction_Euler2D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  SolutionClass constsol(gas, 1.1, 1.2, 1.3, 1.4);

  Real x = 1;
  Real y = 2;
  Real time = 0;
  ArrayQ sln = 0;
  sln = constsol(x, y, time);

  const Real tol = 1.e-13;
  BOOST_CHECK_CLOSE( 1.1, sln[0], tol );
  BOOST_CHECK_CLOSE( 1.2, sln[1], tol );
  BOOST_CHECK_CLOSE( 1.3, sln[2], tol );
  BOOST_CHECK_CLOSE( 1.4, sln[3], tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_Wake )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef SolutionFunction_Euler2D_Wake<TraitsSizeEuler, TraitsModelEulerClass> SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 1.0;
  GasModel gas(gamma, R);
  Q2D<QType,TraitsSizeEuler> qinterp(gas);

  Real Mach = 0.1;
  Real Gamma = 1 + 0.5*(gamma-1.)*Mach*Mach;
  Real p = 1.;
  Real t = 1.;

  Real Tt = t*Gamma;
  Real Pt = p*pow(Gamma, 3.5);

  SolutionClass wakesol(gas, 0.1, Pt, Tt, p);

  Real x = 1;
  Real y = 0.0;
  Real time = 0;
  ArrayQ sln = 0;
  sln = wakesol(x, y, time);

  Real rhos, us, vs, ts, ps;
  qinterp.eval(sln, rhos, us, vs, ts);
  ps = gas.pressure(rhos, ts);
  Real cs = sqrt(gamma*ps/rhos);
  Real Ms = sqrt(us*us + vs*vs)/cs;
  Real Tts = Tt;

  const Real tol = 1.e-13;
  BOOST_CHECK_CLOSE( Tt, Tts, tol );
  BOOST_CHECK_CLOSE( Ms, Mach*0.9, tol );
  BOOST_CHECK_CLOSE( 0.0, vs, tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_Riemann )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef SolutionFunction_Euler2D_Riemann<TraitsSizeEuler, TraitsModelEulerClass> SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  SolutionClass constsol(gas, 0.5, 1.1, 1.2, 1.3, 1.4, 2.1, 2.2, 2.3, 2.4);

  Real x = 0;
  Real y = 0;
  Real time = 0;
  ArrayQ sln = 0;
  sln = constsol(x, y, time);

  const Real tol = 1.e-13;
  BOOST_CHECK_CLOSE( 1.1, sln[0], tol );
  BOOST_CHECK_CLOSE( 1.2, sln[1], tol );
  BOOST_CHECK_CLOSE( 1.3, sln[2], tol );
  BOOST_CHECK_CLOSE( 1.4, sln[3], tol );


  x = 1;
  sln = constsol(x, y, time);
  BOOST_CHECK_CLOSE( 2.1, sln[0], tol );
  BOOST_CHECK_CLOSE( 2.2, sln[1], tol );
  BOOST_CHECK_CLOSE( 2.3, sln[2], tol );
  BOOST_CHECK_CLOSE( 2.4, sln[3], tol );
  
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.interface] = 0.5;
  solnArgs[SolutionClass::ParamsType::params.rhoL] = 1.1;
  solnArgs[SolutionClass::ParamsType::params.uL] = 1.2;
  solnArgs[SolutionClass::ParamsType::params.vL] = 1.3;
  solnArgs[SolutionClass::ParamsType::params.pL] = 1.4;
  solnArgs[SolutionClass::ParamsType::params.rhoR] = 2.1;
  solnArgs[SolutionClass::ParamsType::params.uR] = 2.2;
  solnArgs[SolutionClass::ParamsType::params.vR] = 2.3;
  solnArgs[SolutionClass::ParamsType::params.pR] = 2.4;
  solnArgs[SolutionClass::ParamsType::params.gasModel] = gasModelDict;
  SolutionClass::ParamsType::checkInputs(solnArgs);
  
  SolutionClass solnExact(solnArgs);

  x = 1;
  sln = solnExact(x, y, time);
  BOOST_CHECK_CLOSE( 2.1, sln[0], tol );
  BOOST_CHECK_CLOSE( 2.2, sln[1], tol );
  BOOST_CHECK_CLOSE( 2.3, sln[2], tol );
  BOOST_CHECK_CLOSE( 2.4, sln[3], tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
