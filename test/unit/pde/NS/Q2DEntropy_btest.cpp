// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Q2DEntropy_btest
// testing of Q2D<QTypeEntropy, TraitsSizeEuler> class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/TraitsEuler.h"
#include "Surreal/SurrealS.h"

#include <string>
#include <iostream>


//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
template class Q2D<QTypeEntropy, TraitsSizeEuler>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Q2DEntropy_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test )
{
  const Real tol = 1.e-11;

  typedef Q2D<QTypeEntropy, TraitsSizeEuler> QInterpret;
  BOOST_CHECK( QInterpret::N == 4 );

  typedef QInterpret::ArrayQ<Real> ArrayQ;
  BOOST_CHECK( ArrayQ::M == 4 );

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  // set/eval
  ArrayQ q;
  Real rho, u, v, t, p, E;
  Real rho1, u1, v1, t1;
  Real rho2, u2, v2, t2;

  rho = 1.225;          // kg/m^3 (standard atmosphere)
  t   = 288.15;         // K (standard atmosphere)
  u   = 15.03;          // m/s
  v   =  1.71;
  p   = R*rho*t;
  E   = gas.energy(rho,t) + 0.5*(u*u + v*v);

  Real qDataPrim[4] = {rho, u, v, t};
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY", "Temperature"};
  qInterpret.setFromPrimitive( q, qDataPrim, qNamePrim, 4 );
  qInterpret.eval( q, rho1, u1, v1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );

  qInterpret.evalDensity( q, rho1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );

  qInterpret.evalTemperature( q, t1 );
  BOOST_CHECK_CLOSE( t, t1, tol );

  qInterpret.evalVelocity( q, u1, v1 );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );

  DensityVelocityPressure2D<Real> prim1(rho, u, v, p);
  q = 1;
  qInterpret.setFromPrimitive( q, prim1 );
  qInterpret.eval( q, rho1, u1, v1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );

  DensityVelocityTemperature2D<Real> prim2(rho, u, v, t);
  q = 2;
  qInterpret.setFromPrimitive( q, prim2 );
  qInterpret.eval( q, rho1, u1, v1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );

  Conservative2D<Real> prim3(rho, rho*u, rho*v, rho*E);
  q = 2;
  qInterpret.setFromPrimitive( q, prim3 );
  qInterpret.eval( q, rho1, u1, v1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );


  // copy constructor
  QInterpret qInterpret2(qInterpret);

  qInterpret2.eval( q, rho2, u2, v2, t2 );
  BOOST_CHECK_CLOSE( rho, rho2, tol );
  BOOST_CHECK_CLOSE( u, u2, tol );
  BOOST_CHECK_CLOSE( v, v2, tol );
  BOOST_CHECK_CLOSE( t, t2, tol );


  // gradient
  Real rhox, ux, vx, tx, px;
  Real rhoy, uy, vy, ty, py;
  Real rhox1, ux1, vx1, tx1;
  Real rhoy1, uy1, vy1, ty1;
  Real V0x, V1x, V2x, V3x;
  Real V0y, V1y, V2y, V3y;

  rhox = 0.37, ux = -0.85, vx = 0.09, px = 1.71;
  rhoy = 2.31, uy =  1.65, vy = 0.87, py = 0.29;
  tx = t*(px/p - rhox/rho);
  ty = t*(py/p - rhoy/rho);

  Real rhoe, rhoE;
  Real rhoex, rhoey;
  Real rhoEx, rhoEy;
  Real sx, sy;
  rhoe = rho*gas.energy(rho, t);
  rhoE = rho*(gas.energy(rho, t) + 0.5*(u*u + v*v));

  // rhoe = rho*gas.energy(rho, t);
  rhoex = rhox*gas.Cv()*t + rho*gas.Cv()*tx;
  rhoey = rhoy*gas.Cv()*t + rho*gas.Cv()*ty;


  // rhoE = rho*(gas.energy(rho, t) + 0.5*(u*u + v*v));
  rhoEx = rhox*(gas.Cv()*t + 0.5*(u*u + v*v)) + rho*(gas.Cv()*tx + (u*ux + v*vx));
  rhoEy = rhoy*(gas.Cv()*t + 0.5*(u*u + v*v)) + rho*(gas.Cv()*ty + (u*uy + v*vy));

  // s = log((gamma-1.0)*rhoe / pow(rho,gamma));
  sx = rhoex/rhoe - gamma * rhox/rho;
  sy = rhoey/rhoe - gamma * rhoy/rho;

  // q(0) = -rhoE/rhoe + gamma + 1 - s;
  V0x = -rhoEx / rhoe - rhoE / rhoe * (-rhoex / rhoe) - sx;
  V0y = -rhoEy / rhoe - rhoE / rhoe * (-rhoey / rhoe) - sy;

  // q(1) =  rho*u/rhoe;
  V1x = rhox*u/rhoe + rho*ux/rhoe + rho*u/rhoe*(-rhoex/rhoe);
  V1y = rhoy*u/rhoe + rho*uy/rhoe + rho*u/rhoe*(-rhoey/rhoe);
  // q(2) =  rho*v/rhoe;
  V2x = rhox*v/rhoe + rho*vx/rhoe + rho*v/rhoe*(-rhoex/rhoe);
  V2y = rhoy*v/rhoe + rho*vy/rhoe + rho*v/rhoe*(-rhoey/rhoe);

  // q(3) = -rho/rhoe;
  V3x = -rhox/rhoe - rho/rhoe * (-rhoex/rhoe);
  V3y = -rhoy/rhoe - rho/rhoe * (-rhoey/rhoe);

  Real qxData[4] = {V0x, V1x, V2x, V3x};
  Real qyData[4] = {V0y, V1y, V2y, V3y};
  ArrayQ qx(qxData, 4);
  ArrayQ qy(qyData, 4);

  qInterpret.evalGradient( q, qx, rhox1, ux1, vx1, tx1 );
  qInterpret.evalGradient( q, qy, rhoy1, uy1, vy1, ty1 );
  BOOST_CHECK_CLOSE( rhox, rhox1, tol );
  BOOST_CHECK_CLOSE(   ux,   ux1, tol );
  BOOST_CHECK_CLOSE(   vx,   vx1, tol );
  BOOST_CHECK_CLOSE(   tx,   tx1, tol );
  BOOST_CHECK_CLOSE( rhoy, rhoy1, tol );
  BOOST_CHECK_CLOSE(   uy,   uy1, tol );
  BOOST_CHECK_CLOSE(   vy,   vy1, tol );
  BOOST_CHECK_CLOSE(   ty,   ty1, tol );

  // second gradient
  Real rhoxx = -0.253; Real rhoxy = 0.782; Real rhoyy = 1.08;
  Real uxx = 1.02; Real uxy = -0.95; Real uyy =-0.42;
  Real vxx = 0.99; Real vxy = -0.92; Real vyy =-0.44;
  Real pxx = 2.99; Real pxy = -1.92; Real pyy =-2.44;

  Real txx = 1.0/R*( (-2.*px*rhox)/(rho*rho) + 2.*p*rhox*rhox/(rho*rho*rho) + pxx/rho - p*rhoxx/(rho*rho) );
  Real txy = 1.0/R*( -rhoy*px/(rho*rho) - py*rhox/(rho*rho) + 2.*p*rhoy*rhox/(rho*rho*rho) + pxy/(rho) - p*rhoxy/(rho*rho) );
  Real tyy = 1.0/R*( -2.*py*rhoy/(rho*rho) + 2.*p*rhoy*rhoy/(rho*rho*rho) + pyy/(rho) - p*rhoyy/(rho*rho) );

  //rhoex = rhox*gas.Cv()*t + rho*gas.Cv()*tx;
  Real rhoexx = rhoxx*gas.Cv()*t + rhox*gas.Cv()*tx + rhox*gas.Cv()*tx + rho*gas.Cv()*txx;
  Real rhoeyy = rhoyy*gas.Cv()*t + rhoy*gas.Cv()*ty + rhoy*gas.Cv()*ty + rho*gas.Cv()*tyy;
  Real rhoexy = rhoxy*gas.Cv()*t + rhox*gas.Cv()*ty + rhoy*gas.Cv()*tx + rho*gas.Cv()*txy;

//  rhoEx = rhox*(gas.Cv()*t + 0.5*(u*u + v*v)) + rho*(gas.Cv()*tx + (u*ux + v*vx));
  Real rhoExx = rhoxx*(gas.Cv()*t + 0.5*(u*u + v*v)) + rhox*(gas.Cv()*tx + u*ux + v*vx)
                +rhox*(gas.Cv()*tx + (u*ux + v*vx)) + rho*(gas.Cv()*txx + (ux*ux + u*uxx + vx*vx + v*vxx));
  Real rhoEyy = rhoyy*(gas.Cv()*t + 0.5*(u*u + v*v)) + rhoy*(gas.Cv()*ty + u*uy + v*vy)
                +rhoy*(gas.Cv()*ty + (u*uy + v*vy)) + rho*(gas.Cv()*tyy + (uy*uy + u*uyy + vy*vy + v*vyy));
  Real rhoExy = rhoxy*(gas.Cv()*t + 0.5*(u*u + v*v)) + rhox*(gas.Cv()*ty + u*uy + v*vy)
                    +rhoy*(gas.Cv()*tx + (u*ux + v*vx)) + rho*(gas.Cv()*txy + (ux*uy + u*uxy + vx*vy + v*vxy));

  //sx = rhoex/rhoe - gamma* rhox/rho
  Real sxx = rhoexx/rhoe + rhoex/rhoe * (-rhoex / rhoe) - gamma * rhoxx/rho - gamma * rhox/rho * (-rhox /rho);
  Real syy = rhoeyy/rhoe + rhoey/rhoe * (-rhoey / rhoe) - gamma * rhoyy/rho - gamma * rhoy/rho * (-rhoy /rho);
  Real sxy = rhoexy/rhoe + rhoex/rhoe * (-rhoey / rhoe) - gamma * rhoxy/rho - gamma * rhox/rho * (-rhoy/ rho);

  //V0x = -rhoEx / rhoe + rhoE / rhoe * (-rhoex / rhoe) - sx;
  Real V0xx = -rhoExx / rhoe - rhoEx / rhoe*(-rhoex / rhoe)
              + rhoEx*rhoex / rhoe / rhoe + rhoE * rhoexx / rhoe /rhoe + rhoE * rhoex / (rhoe * rhoe *rhoe) * (-2*rhoex)
              - sxx;
  Real V0yy = -rhoEyy / rhoe - rhoEy / rhoe*(-rhoey / rhoe)
              + rhoEy*rhoey / rhoe / rhoe + rhoE * rhoeyy / rhoe /rhoe + rhoE * rhoey / (rhoe * rhoe *rhoe) * (-2*rhoey)
              - syy;
  Real V0xy = -rhoExy / rhoe - rhoEx / rhoe*(-rhoey / rhoe)
               + rhoEy*rhoex / rhoe / rhoe + rhoE * rhoexy / rhoe /rhoe + rhoE * rhoex / (rhoe * rhoe *rhoe) * (-2*rhoey)
               - sxy;

//  V1x = rhox*u/rhoe + rho*ux/rhoe + rho*u/rhoe*(-rhoex/rhoe);
  Real V1xx = rhoxx*u/rhoe + rhox*ux/rhoe + rhox*u/rhoe * (-rhoex/rhoe)
         + rhox*ux/rhoe + rho*uxx/rhoe + rho*ux/rhoe * (-rhoex/rhoe)
         + rhox*u/rhoe*(-rhoex/rhoe) + rho*ux/rhoe*(-rhoex/rhoe) + rho*u/rhoe*(-rhoexx/rhoe) + rho*u*rhoex/(rhoe*rhoe*rhoe)*(2.*rhoex);
  Real V1yy = rhoyy*u/rhoe + rhoy*uy/rhoe + rhoy*u/rhoe * (-rhoey/rhoe)
         + rhoy*uy/rhoe + rho*uyy/rhoe + rho*uy/rhoe * (-rhoey/rhoe)
         + rhoy*u/rhoe*(-rhoey/rhoe) + rho*uy/rhoe*(-rhoey/rhoe) + rho*u/rhoe*(-rhoeyy/rhoe) + rho*u*rhoey/(rhoe*rhoe*rhoe)*(2.*rhoey);
  Real V1xy = rhoxy*u/rhoe + rhox*uy/rhoe + rhox*u/rhoe * (-rhoey/rhoe)
         + rhoy*ux/rhoe + rho*uxy/rhoe + rho*ux/rhoe * (-rhoey/rhoe)
         + rhoy*u/rhoe*(-rhoex/rhoe) + rho*uy/rhoe*(-rhoex/rhoe) + rho*u/rhoe*(-rhoexy/rhoe) + rho*u*rhoex/(rhoe*rhoe*rhoe)*(2.*rhoey);

  //  V1x = rhox*v/rhoe + rho*vx/rhoe + rho*v/rhoe*(-rhoex/rhoe);
  Real V2xx = rhoxx*v/rhoe + rhox*vx/rhoe + rhox*v/rhoe * (-rhoex/rhoe)
         + rhox*vx/rhoe + rho*vxx/rhoe + rho*vx/rhoe * (-rhoex/rhoe)
         + rhox*v/rhoe*(-rhoex/rhoe) + rho*vx/rhoe*(-rhoex/rhoe) + rho*v/rhoe*(-rhoexx/rhoe) + rho*v*rhoex/(rhoe*rhoe*rhoe)*(2.*rhoex);
  Real V2yy = rhoyy*v/rhoe + rhoy*vy/rhoe + rhoy*v/rhoe * (-rhoey/rhoe)
         + rhoy*vy/rhoe + rho*vyy/rhoe + rho*vy/rhoe * (-rhoey/rhoe)
         + rhoy*v/rhoe*(-rhoey/rhoe) + rho*vy/rhoe*(-rhoey/rhoe) + rho*v/rhoe*(-rhoeyy/rhoe) + rho*v*rhoey/(rhoe*rhoe*rhoe)*(2.*rhoey);
  Real V2xy = rhoxy*v/rhoe + rhox*vy/rhoe + rhox*v/rhoe * (-rhoey/rhoe)
         + rhoy*vx/rhoe + rho*vxy/rhoe + rho*vx/rhoe * (-rhoey/rhoe)
         + rhoy*v/rhoe*(-rhoex/rhoe) + rho*vy/rhoe*(-rhoex/rhoe) + rho*v/rhoe*(-rhoexy/rhoe) + rho*v*rhoex/(rhoe*rhoe*rhoe)*(2.*rhoey);

//  V3x = -rhox/rhoe - rho/rhoe * (-rhoex/rhoe);
  Real V3xx = -rhoxx/rhoe - rhox/rhoe*(-rhoex/rhoe)
              + rhox*rhoex/rhoe/rhoe + rho*rhoexx/rhoe/rhoe + rho*rhoex/(rhoe*rhoe*rhoe)*(-2.*rhoex);
  Real V3yy = -rhoyy/rhoe - rhoy/rhoe*(-rhoey/rhoe)
              + rhoy*rhoey/rhoe/rhoe + rho*rhoeyy/rhoe/rhoe + rho*rhoey/(rhoe*rhoe*rhoe)*(-2.*rhoey);
  Real V3xy = -rhoxy/rhoe - rhox/rhoe*(-rhoey/rhoe)
              + rhoy*rhoex/rhoe/rhoe + rho*rhoexy/rhoe/rhoe + rho*rhoex/(rhoe*rhoe*rhoe)*(-2.*rhoey);

//  ArrayQ qxx = {V0xx, V1xx, V2xx, V3xx};
//  ArrayQ qxy = {V0xy, V1xy, V2xy, V3xy};
//  ArrayQ qyy = {V0yy, V1yy, V2yy, V3yy};

  Real qxxData[4] = {V0xx, V1xx, V2xx, V3xx};
  Real qxyData[4] = {V0xy, V1xy, V2xy, V3xy};
  Real qyyData[4] = {V0yy, V1yy, V2yy, V3yy};
  ArrayQ qxx(qxxData, 4);
  ArrayQ qxy(qxyData, 4);
  ArrayQ qyy(qyyData, 4);

  Real rhoxx2, rhoxy2, rhoyy2;
  Real uxx2, uxy2, uyy2;
  Real vxx2, vxy2, vyy2;
  Real txx2, txy2, tyy2;

  qInterpret.evalSecondGradient( q, qx, qy,
                                 qxx, qxy, qyy,
                                 rhoxx2, rhoxy2, rhoyy2,
                                 uxx2, uxy2, uyy2,
                                 vxx2, vxy2, vyy2,
                                 txx2, txy2, tyy2);

  BOOST_CHECK_CLOSE( rhoxx, rhoxx2, tol );
  BOOST_CHECK_CLOSE( rhoxy, rhoxy2, tol );
  BOOST_CHECK_CLOSE( rhoyy, rhoyy2, tol );
  BOOST_CHECK_CLOSE(   uxx,   uxx2, tol );
  BOOST_CHECK_CLOSE(   uxy,   uxy2, tol );
  BOOST_CHECK_CLOSE(   uyy,   uyy2, tol );
  BOOST_CHECK_CLOSE(   vxx,   vxx2, tol );
  BOOST_CHECK_CLOSE(   vxy,   vxy2, tol );
  BOOST_CHECK_CLOSE(   vyy,   vyy2, tol );
  BOOST_CHECK_CLOSE(   txx,   txx2, tol );
  BOOST_CHECK_CLOSE(   txy,   txy2, tol );
  BOOST_CHECK_CLOSE(   tyy,   tyy2, tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( evalJacobian )
{
  typedef Q2D<QTypeEntropy, TraitsSizeEuler> QInterpret;

  typedef QInterpret::ArrayQ< Real > ArrayQ;
  typedef QInterpret::ArrayQ< SurrealS<QInterpret::N> > ArrayQSurreal;

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  ArrayQ q, rho_q, u_q, v_q, t_q;
  ArrayQSurreal qSurreal;
  SurrealS<QInterpret::N> rho, u, v, t;

  //qInterpret.setFromPrimitive( q, DensityVelocityTemperature2D(1.225, 15.03, 1.71, 288.15) );

  // There are some numerical issues with trying to pick a physical number to compare with Surreals
  q[0] = -10;
  q[1] = 2;
  q[2] = -4;
  q[3] = -3;

  qInterpret.evalJacobian(q, rho_q, u_q, v_q, t_q);

  qSurreal = q;

  qSurreal[0].deriv(0) = 1;
  qSurreal[1].deriv(1) = 1;
  qSurreal[2].deriv(2) = 1;
  qSurreal[3].deriv(3) = 1;

  qInterpret.eval(qSurreal, rho, u, v, t);

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( rho.deriv(0), rho_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(1), rho_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(2), rho_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(3), rho_q[3], small_tol, close_tol )

  SANS_CHECK_CLOSE( u.deriv(0), u_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(1), u_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(2), u_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(3), u_q[3], small_tol, close_tol )

  SANS_CHECK_CLOSE( v.deriv(0), v_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(1), v_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(2), v_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(3), v_q[3], small_tol, close_tol )

  SANS_CHECK_CLOSE( t.deriv(0), t_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(1), t_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(2), t_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(3), t_q[3], small_tol, close_tol )
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef Q2D<QTypeEntropy, TraitsSizeEuler> QInterpret;
  typedef QInterpret::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  /*
   Entropy calluation from primites.
   This must be done explicitly to avoid log( x < 0 ) floating errors

    rhoe = rho*gas_.energy(rho, t);
    rhoE = rho*(gas_.energy(rho, t) + 0.5*(u*u + v*v));
    gamma = gas_.gamma();
    s = log((gamma-1.0)*rhoe / pow(rho,gamma));

    q(0) = -rhoE/rhoe + gamma + 1 - s;
    q(1) =  rho*u/rhoe;
    q(2) =  rho*v/rhoe;
    q(3) = -rho/rhoe;
   */

  ArrayQ q;
  Real rho, u, v, t, rhoe, rhoE, s;
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY", "Temperature"};

  // Start with physical data
  rho = 1.225;          // kg/m^3 (standard atmosphere)
  t   = 288.15;         // K (standard atmosphere)
  u   = 15.03;          // m/s
  v   =  1.71;
  Real qDataPrim[4] = {rho, u, v, t};
  qInterpret.setFromPrimitive( q, qDataPrim, qNamePrim, 4 );
  BOOST_CHECK( qInterpret.isValidState(q) == true );

  // Negative temperature
  rho = 1.225;          // kg/m^3
  t   = -288.15;        // K

  rhoe = rho*gas.energy(rho, t);
  rhoE = rho*(gas.energy(rho, t) + 0.5*(u*u + v*v));
  s = -1; //log((gamma-1.0)*rhoe / pow(rho,gamma)); rhoe < 0

  q(0) = -rhoE/rhoe + gamma + 1 - s;
  q(1) =  rho*u/rhoe;
  q(2) =  rho*v/rhoe;
  q(3) = -rho/rhoe;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  // Negative density
  rho = -1.225;        // kg/m^3
  t   = 288.15;        // K

  rhoe = rho*gas.energy(rho, t);
  rhoE = rho*(gas.energy(rho, t) + 0.5*(u*u + v*v));
  s = 1e6; //log((gamma-1.0)*rhoe / pow(rho,gamma)); rho < 0

  q(0) = -rhoE/rhoe + gamma + 1 - s;
  q(1) =  rho*u/rhoe;
  q(2) =  rho*v/rhoe;
  q(3) = -rho/rhoe;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  // Negative density and temperature
  rho = -1.225;         // kg/m^3
  t   = -288.15;        // K

  rhoe = rho*gas.energy(rho, t);
  rhoE = rho*(gas.energy(rho, t) + 0.5*(u*u + v*v));
  s = -1; //log((gamma-1.0)*rhoe / pow(rho,gamma)); rho < 0

  q(0) = -rhoE/rhoe + gamma + 1 - s;
  q(1) =  rho*u/rhoe;
  q(2) =  rho*v/rhoe;
  q(3) = -rho/rhoe;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  // simply results in 1/0 everywhere...
#if 0
  // Zero temperature
  rho = 1.225;          // kg/m^3
  t   = 0.0;            // K

  rhoe = rho*gas.energy(rho, t); // rhoe == 0
  rhoE = rho*(gas.energy(rho, t) + 0.5*(u*u + v*v));
  s = log((gamma-1.0)*rhoe / pow(rho,gamma));

  q(0) = -rhoE/rhoe + gamma + 1 - s;
  q(1) =  rho*u/rhoe;
  q(2) =  rho*v/rhoe;
  q(3) = -rho/rhoe;

  BOOST_CHECK( qInterpret.isValidState(q) == false );

  // Zero density
  rho = 0.0;            // kg/m^3
  t   = 288.15;        // K

  rhoe = rho*gas.energy(rho, t); // rhoe == 0
  rhoE = rho*(gas.energy(rho, t) + 0.5*(u*u + v*v));
  s = log((gamma-1.0)*rhoe / pow(rho,gamma));

  q(0) = -rhoE/rhoe + gamma + 1 - s;
  q(1) =  rho*u/rhoe;
  q(2) =  rho*v/rhoe;
  q(3) = -rho/rhoe;

  BOOST_CHECK( qInterpret.isValidState(q) == false );
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/Q2DEntropy_pattern.txt", true );

  typedef Q2D<QTypeEntropy, TraitsSizeEuler> QInterpret;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  qInterpret.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
