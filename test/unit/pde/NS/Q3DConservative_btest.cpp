// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Q3DConservative_btest
// testing of Q3D<QTypeConservative, TraitsSizeEuler> class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

//#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include "pde/NS/Q3DConservative.h"
#include "pde/NS/TraitsEuler.h"
#include "Surreal/SurrealS.h"

#include <string>
#include <iostream>


//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
template class Q3D<QTypeConservative, TraitsSizeEuler>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Q3DConservative_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_est )
{
  typedef Q3D<QTypeConservative, TraitsSizeEuler> QInterpret;
  BOOST_CHECK( QInterpret::N == 5 );

  typedef QInterpret::ArrayQ<Real> ArrayQ;
  BOOST_CHECK( ArrayQ::M == 5 );

  // make sure the vector comonents are set correctly
  BOOST_CHECK( QInterpret::ix == QInterpret::irhou );
  BOOST_CHECK( QInterpret::iy == QInterpret::irhov );
  BOOST_CHECK( QInterpret::iz == QInterpret::irhow );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test )
{
  const Real tol = 5.0e-12;

  typedef Q3D<QTypeConservative, TraitsSizeEuler> QInterpret;
  typedef QInterpret::ArrayQ<Real> ArrayQ;

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  // set/eval
  ArrayQ q;
  Real rho, u, v, w, t, p, E;
  Real rho1, u1, v1, w1, t1;
  Real rho2, u2, v2, w2, t2;

  rho = 1.225;          // kg/m^3 (standard atmosphere)
  t   = 288.15;         // K (standard atmosphere)
  u   = 15.03;          // m/s
  v   =  1.71;
  w   = 10.25;
  p   = R*rho*t;
  E   = gas.energy(rho,t) + 0.5*(u*u + v*v + w*w);

  Real qDataPrim[5] = {rho, u, v, w, t};
  string qNamePrim[5] = {"Density", "VelocityX", "VelocityY", "VelocityZ", "Temperature"};
  qInterpret.setFromPrimitive( q, qDataPrim, qNamePrim, 5 );
  qInterpret.eval( q, rho1, u1, v1, w1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( w, w1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );

  qInterpret.evalDensity( q, rho1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );

  qInterpret.evalTemperature( q, t1 );
  BOOST_CHECK_CLOSE( t, t1, tol );

  qInterpret.evalVelocity( q, u1, v1, w1 );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( w, w1, tol );

  DensityVelocityPressure3D<Real> prim1(rho, u, v, w, p);
  q = 1;
  qInterpret.setFromPrimitive( q, prim1 );
  qInterpret.eval( q, rho1, u1, v1, w1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( w, w1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );

  DensityVelocityTemperature3D<Real> prim2(rho, u, v, w, t);
  q = 2;
  qInterpret.setFromPrimitive( q, prim2 );
  qInterpret.eval( q, rho1, u1, v1, w1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( w, w1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );

  Conservative3D<Real> prim3(rho, rho*u, rho*v, rho*w, rho*E);
  q = 2;
  qInterpret.setFromPrimitive( q, prim3 );
  qInterpret.eval( q, rho1, u1, v1, w1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( w, w1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );



  // copy constructor
  QInterpret qInterpret2(qInterpret);

  qInterpret2.eval( q, rho2, u2, v2, w2, t2 );
  BOOST_CHECK_CLOSE( rho, rho2, tol );
  BOOST_CHECK_CLOSE( u, u2, tol );
  BOOST_CHECK_CLOSE( v, v2, tol );
  BOOST_CHECK_CLOSE( w, w2, tol );
  BOOST_CHECK_CLOSE( t, t2, tol );


  // gradient
  Real rhox, ux, vx, wx, tx, px;
  Real rhoy, uy, vy, wy, ty, py;
  Real rhoz, uz, vz, wz, tz, pz;
  Real rhox1, ux1, vx1, wx1, tx1;
  Real rhoy1, uy1, vy1, wy1, ty1;
  Real rhoz1, uz1, vz1, wz1, tz1;
  Real rhoux, rhovx, rhowx, rhoEx;
  Real rhouy, rhovy, rhowy, rhoEy;
  Real rhouz, rhovz, rhowz, rhoEz;

  rhox = 0.37, ux = -0.85, vx = 0.09, wx =  0.36, px = 1.71;
  rhoy = 2.31, uy =  1.65, vy = 0.87, wy = -1.07, py = 0.29;
  rhoz = 0.45, uz =  0.12, vz = 1.27, wz =  0.07, pz = 2.12;
  tx = t*(px/p - rhox/rho);
  ty = t*(py/p - rhoy/rho);
  tz = t*(pz/p - rhoz/rho);

  rhoux = rhox*u + rho*ux;
  rhouy = rhoy*u + rho*uy;
  rhouz = rhoz*u + rho*uz;

  rhovx = rhox*v + rho*vx;
  rhovy = rhoy*v + rho*vy;
  rhovz = rhoz*v + rho*vz;

  rhowx = rhox*w + rho*wx;
  rhowy = rhoy*w + rho*wy;
  rhowz = rhoz*w + rho*wz;

  rhoEx = rhox*(gas.Cv()*t + 0.5*(u*u + v*v + w*w)) + rho*(gas.Cv()*tx + (u*ux + v*vx + w*wx));
  rhoEy = rhoy*(gas.Cv()*t + 0.5*(u*u + v*v + w*w)) + rho*(gas.Cv()*ty + (u*uy + v*vy + w*wy));
  rhoEz = rhoz*(gas.Cv()*t + 0.5*(u*u + v*v + w*w)) + rho*(gas.Cv()*tz + (u*uz + v*vz + w*wz));

  ArrayQ qx = {rhox, rhoux, rhovx, rhowx, rhoEx};
  ArrayQ qy = {rhoy, rhouy, rhovy, rhowy, rhoEy};
  ArrayQ qz = {rhoz, rhouz, rhovz, rhowz, rhoEz};

  qInterpret.evalGradient( q, qx, rhox1, ux1, vx1, wx1, tx1 );
  qInterpret.evalGradient( q, qy, rhoy1, uy1, vy1, wy1, ty1 );
  qInterpret.evalGradient( q, qz, rhoz1, uz1, vz1, wz1, tz1 );

  BOOST_CHECK_CLOSE( rhox, rhox1, tol );
  BOOST_CHECK_CLOSE(   ux,   ux1, tol );
  BOOST_CHECK_CLOSE(   vx,   vx1, tol );
  BOOST_CHECK_CLOSE(   wx,   wx1, tol );
  BOOST_CHECK_CLOSE(   tx,   tx1, tol );

  BOOST_CHECK_CLOSE( rhoy, rhoy1, tol );
  BOOST_CHECK_CLOSE(   uy,   uy1, tol );
  BOOST_CHECK_CLOSE(   vy,   vy1, tol );
  BOOST_CHECK_CLOSE(   wy,   wy1, tol );
  BOOST_CHECK_CLOSE(   ty,   ty1, tol );

  BOOST_CHECK_CLOSE( rhoz, rhoz1, tol );
  BOOST_CHECK_CLOSE(   uz,   uz1, tol );
  BOOST_CHECK_CLOSE(   vz,   vz1, tol );
  BOOST_CHECK_CLOSE(   wz,   wz1, tol );
  BOOST_CHECK_CLOSE(   tz,   tz1, tol );


  // Hessian
  Real rhoxx = -0.253;
  Real rhoxy =  0.782, rhoyy = 1.08;
  Real rhoxz =  0.152, rhoyz = 0.13, rhozz = 2.15;

  Real uxx =  1.02;
  Real uxy = -0.95, uyy =-0.42;
  Real uxz =  2.31, uyz = 0.34, uzz = 3.14;

  Real vxx = 0.99;
  Real vxy = -0.92, vyy =-0.44;
  Real vxz = -1.62, vyz = 2.32, vzz = 1.15;

  Real wxx =  3.11;
  Real wxy =  0.46, wyy =-2.14;
  Real wxz = -2.13, wyz = 1.78, wzz = -0.76;

  Real pxx = 2.99;
  Real pxy = 0.91, pyy =-2.44;
  Real pxz = 2.38, pyz = 1.88, pzz =-1.98;

  Real rho3 = rho*rho*rho;

  Real txx = 1.0/R*( -2*px*rhox/(rho*rho) + 2*p*rhox*rhox/rho3 + pxx/rho - p*rhoxx/(rho*rho) );
  Real txy = 1.0/R*( -rhoy*px/(rho*rho) - py*rhox/(rho*rho) + 2*p*rhoy*rhox/rho3 + pxy/rho - p*rhoxy/(rho*rho) );
  Real tyy = 1.0/R*( -2*py*rhoy/(rho*rho) + 2*p*rhoy*rhoy/rho3 + pyy/rho - p*rhoyy/(rho*rho) );

  Real txz = 1.0/R*( -rhoz*px/(rho*rho) - pz*rhox/(rho*rho) + 2*p*rhoz*rhox/rho3 + pxz/rho - p*rhoxz/(rho*rho) );
  Real tyz = 1.0/R*( -rhoz*py/(rho*rho) - pz*rhoy/(rho*rho) + 2*p*rhoz*rhoy/rho3 + pyz/rho - p*rhoyz/(rho*rho) );
  Real tzz = 1.0/R*( -2*pz*rhoz/(rho*rho) + 2*p*rhoz*rhoz/rho3 + pzz/rho - p*rhozz/(rho*rho) );


  Real rhouxx = rhoxx*u + 2.0*rhox*ux + rho*uxx;
  Real rhouyy = rhoyy*u + 2.0*rhoy*uy + rho*uyy;
  Real rhouzz = rhozz*u + 2.0*rhoz*uz + rho*uzz;

  Real rhouxy = rhoxy*u+ rhox*uy + rhoy*ux + rho*uxy;
  Real rhouxz = rhoxz*u+ rhox*uz + rhoz*ux + rho*uxz;
  Real rhouyz = rhoyz*u+ rhoy*uz + rhoz*uy + rho*uyz;


  Real rhovxx = rhoxx*v + 2.0*rhox*vx + rho*vxx;
  Real rhovyy = rhoyy*v + 2.0*rhoy*vy + rho*vyy;
  Real rhovzz = rhozz*v + 2.0*rhoz*vz + rho*vzz;

  Real rhovxy = rhoxy*v+ rhox*vy + rhoy*vx + rho*vxy;
  Real rhovxz = rhoxz*v+ rhox*vz + rhoz*vx + rho*vxz;
  Real rhovyz = rhoyz*v+ rhoy*vz + rhoz*vy + rho*vyz;


  Real rhowxx = rhoxx*w + 2.0*rhox*wx + rho*wxx;
  Real rhowyy = rhoyy*w + 2.0*rhoy*wy + rho*wyy;
  Real rhowzz = rhozz*w + 2.0*rhoz*wz + rho*wzz;

  Real rhowxy = rhoxy*w+ rhox*wy + rhoy*wx + rho*wxy;
  Real rhowxz = rhoxz*w+ rhox*wz + rhoz*wx + rho*wxz;
  Real rhowyz = rhoyz*w+ rhoy*wz + rhoz*wy + rho*wyz;


  Real rhoExx = rhoxx*(gas.Cv()*t + 0.5*(u*u + v*v + w*w)) + rhox*(gas.Cv()*tx + (u*ux + v*vx + w*wx)) +
                rhox*(gas.Cv()*tx + (u*ux + v*vx + w*wx)) +
                rho*(gas.Cv()*txx + (ux*ux + u*uxx + vx*vx + v*vxx + wx*wx + w*wxx));

  Real rhoEyy = rhoyy*(gas.Cv()*t + 0.5*(u*u + v*v + w*w)) + rhoy*(gas.Cv()*ty + (u*uy + v*vy + w*wy)) +
                rhoy*(gas.Cv()*ty + (u*uy + v*vy + w*wy)) +
                rho*(gas.Cv()*tyy + (uy*uy + u*uyy + vy*vy + v*vyy + wy*wy + w*wyy));

  Real rhoEzz = rhozz*(gas.Cv()*t + 0.5*(u*u + v*v + w*w)) + rhoz*(gas.Cv()*tz + (u*uz + v*vz + w*wz)) +
                rhoz*(gas.Cv()*tz + (u*uz + v*vz + w*wz)) +
                rho*(gas.Cv()*tzz + (uz*uz + u*uzz + vz*vz + v*vzz + wz*wz + w*wzz));


  Real rhoExy = rhoxy*(gas.Cv()*t + 0.5*(u*u + v*v + w*w)) + rhoy*(gas.Cv()*tx + (u*ux + v*vx + w*wx)) +
                rhox*(gas.Cv()*ty + (u*uy + v*vy + w*wy)) +
                rho*(gas.Cv()*txy + (ux*uy + u*uxy + vx*vy + v*vxy + wx*wy + w*wxy));

  Real rhoExz = rhoxz*(gas.Cv()*t + 0.5*(u*u + v*v + w*w)) + rhoz*(gas.Cv()*tx + (u*ux + v*vx + w*wx)) +
                rhox*(gas.Cv()*tz + (u*uz + v*vz + w*wz)) +
                rho*(gas.Cv()*txz + (ux*uz + u*uxz + vx*vz + v*vxz + wx*wz + w*wxz));

  Real rhoEyz = rhoyz*(gas.Cv()*t + 0.5*(u*u + v*v + w*w)) + rhoz*(gas.Cv()*ty + (u*uy + v*vy + w*wy)) +
                rhoy*(gas.Cv()*tz + (u*uz + v*vz + w*wz)) +
                rho*(gas.Cv()*tyz + (uy*uz + u*uyz + vy*vz + v*vyz + wy*wz + w*wyz));


  ArrayQ qxx = {rhoxx, rhouxx, rhovxx, rhowxx, rhoExx};
  ArrayQ qxy = {rhoxy, rhouxy, rhovxy, rhowxy, rhoExy};
  ArrayQ qyy = {rhoyy, rhouyy, rhovyy, rhowyy, rhoEyy};
  ArrayQ qxz = {rhoxz, rhouxz, rhovxz, rhowxz, rhoExz};
  ArrayQ qyz = {rhoyz, rhouyz, rhovyz, rhowyz, rhoEyz};
  ArrayQ qzz = {rhozz, rhouzz, rhovzz, rhowzz, rhoEzz};

  DLA::MatrixSymS<3, Real> rhoH, uH, vH, wH, tH;

  qInterpret.evalSecondGradient( q, qx, qy, qz,
                                 qxx,
                                 qxy, qyy,
                                 qxz, qyz, qzz,
                                 rhoH, uH, vH, wH, tH );

  BOOST_CHECK_CLOSE( rhoxx, rhoH(0,0), tol );
  BOOST_CHECK_CLOSE( rhoxy, rhoH(1,0), tol );
  BOOST_CHECK_CLOSE( rhoyy, rhoH(1,1), tol );
  BOOST_CHECK_CLOSE( rhoxz, rhoH(2,0), tol );
  BOOST_CHECK_CLOSE( rhoyz, rhoH(2,1), tol );
  BOOST_CHECK_CLOSE( rhozz, rhoH(2,2), tol );

  BOOST_CHECK_CLOSE(   uxx,   uH(0,0), tol );
  BOOST_CHECK_CLOSE(   uxy,   uH(1,0), tol );
  BOOST_CHECK_CLOSE(   uyy,   uH(1,1), tol );
  BOOST_CHECK_CLOSE(   uxz,   uH(2,0), tol );
  BOOST_CHECK_CLOSE(   uyz,   uH(2,1), tol );
  BOOST_CHECK_CLOSE(   uzz,   uH(2,2), tol );

  BOOST_CHECK_CLOSE(   vxx,   vH(0,0), tol );
  BOOST_CHECK_CLOSE(   vxy,   vH(1,0), tol );
  BOOST_CHECK_CLOSE(   vyy,   vH(1,1), tol );
  BOOST_CHECK_CLOSE(   vxz,   vH(2,0), tol );
  BOOST_CHECK_CLOSE(   vyz,   vH(2,1), tol );
  BOOST_CHECK_CLOSE(   vzz,   vH(2,2), tol );

  BOOST_CHECK_CLOSE(   wxx,   wH(0,0), tol );
  BOOST_CHECK_CLOSE(   wxy,   wH(1,0), tol );
  BOOST_CHECK_CLOSE(   wyy,   wH(1,1), tol );
  BOOST_CHECK_CLOSE(   wxz,   wH(2,0), tol );
  BOOST_CHECK_CLOSE(   wyz,   wH(2,1), tol );
  BOOST_CHECK_CLOSE(   wzz,   wH(2,2), tol );

  BOOST_CHECK_CLOSE(   txx,   tH(0,0), tol );
  BOOST_CHECK_CLOSE(   txy,   tH(1,0), tol );
  BOOST_CHECK_CLOSE(   tyy,   tH(1,1), tol );
  BOOST_CHECK_CLOSE(   txz,   tH(2,0), tol );
  BOOST_CHECK_CLOSE(   tyz,   tH(2,1), tol );
  BOOST_CHECK_CLOSE(   tzz,   tH(2,2), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( evalJacobian )
{
  typedef Q3D<QTypeConservative, TraitsSizeEuler> QInterpret;

  typedef QInterpret::ArrayQ< Real > ArrayQ;
  typedef QInterpret::ArrayQ< SurrealS<QInterpret::N> > ArrayQSurreal;

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  ArrayQ q, rho_q, u_q, v_q, w_q, t_q;
  ArrayQSurreal qSurreal;
  SurrealS<QInterpret::N> rho, u, v, w, t;

  qInterpret.setFromPrimitive( q, DensityVelocityTemperature3D<Real>(1.225, 15.03, 1.71, 10.25, 288.15) );

  qInterpret.evalJacobian(q, rho_q, u_q, v_q, w_q, t_q);

  qSurreal = q;

  qSurreal[0].deriv(0) = 1;
  qSurreal[1].deriv(1) = 1;
  qSurreal[2].deriv(2) = 1;
  qSurreal[3].deriv(3) = 1;
  qSurreal[4].deriv(4) = 1;

  qInterpret.eval(qSurreal, rho, u, v, w, t);

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  SANS_CHECK_CLOSE( rho.deriv(0), rho_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(1), rho_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(2), rho_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(3), rho_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(4), rho_q[4], small_tol, close_tol )

  SANS_CHECK_CLOSE( u.deriv(0), u_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(1), u_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(2), u_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(3), u_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(4), u_q[4], small_tol, close_tol )

  SANS_CHECK_CLOSE( v.deriv(0), v_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(1), v_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(2), v_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(3), v_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(4), v_q[4], small_tol, close_tol )

  SANS_CHECK_CLOSE( w.deriv(0), w_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( w.deriv(1), w_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( w.deriv(2), w_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( w.deriv(3), w_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( w.deriv(4), w_q[4], small_tol, close_tol )

  SANS_CHECK_CLOSE( t.deriv(0), t_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(1), t_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(2), t_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(3), t_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(4), t_q[4], small_tol, close_tol )
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef Q3D<QTypeConservative, TraitsSizeEuler> QInterpret;
  typedef QInterpret::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  ArrayQ q = 0;
  q(0) =  1;
  q(4) =  1;
  BOOST_CHECK( qInterpret.isValidState(q) );

  q(0) =  1;
  q(4) = -1;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  q(0) = -1;
  q(4) =  1;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  q(0) = -1;
  q(4) = -1;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  q(0) =  1;
  q(4) =  0;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  q(0) =  0;
  q(4) =  1;
  BOOST_CHECK( qInterpret.isValidState(q) == false );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/Q3DConservative_pattern.txt", true );

  typedef Q3D<QTypeConservative, TraitsSizeEuler> QInterpret;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  qInterpret.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
