// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCRANSSA3D_mitState_btest
//
// test of 3-D compressible RANS SA BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/QRANSSA3D.h"
#include "pde/NS/BCEuler3D.h"
#include "pde/NS/BCRANSSA3D.h"
#include "pde/NS/TraitsRANSSA.h"
#include "pde/BCParameters.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"


//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
typedef TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> TraitsModel;

template class BCRANSSA3D< BCTypeWall_mitState,
                           BCNavierStokes3D< BCTypeWallNoSlipAdiabatic_mitState, PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> > >;

template class BCRANSSA3D< BCTypeNeumann_mitState,
                           BCNavierStokes3D< BCTypeSymmetry_mitState, PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> > >;

template class BCEuler3D< BCTypeReflect_mitState, PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> >;

template class BCRANSSA3D< BCTypeFullState_mitState,
                           BCEuler3D< BCTypeFullState_mitState, PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> > >;

template class BCRANSSA3D<BCTypeNeumann_mitState,
                          BCEuler3D< BCTypeOutflowSubsonic_Pressure_mitState, PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> > >;

//template class BCRANSSA3D<BCTypeInflowSubsonic_sHqt_BN,
//                          BCEuler3D< BCTypeInflowSubsonic_sHqt_BN, PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> > >;

//template class BCRANSSA3D<BCTypeFunctionInflow_sHqt_BN,
//                          BCEuler3D< BCTypeFunctionInflow_sHqt_BN, PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> > >;

// Instantiate BCParamaters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters< BCRANSSA3DVector< TraitsSizeRANSSA, TraitsModel > >;
}

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( BCRANSSA3D_mitState_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test_BCTypeWall_mitState )
{
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> PDEClass;

  typedef BCRANSSA3D< BCTypeWall_mitState,
                      BCNavierStokes3D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass> > BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  typedef BCParameters< BCRANSSA3DVector<TraitsSizeRANSSA, TraitsModel> > BCParams;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 6 );
  BOOST_CHECK( BCClass::NBC == 4 );
  BOOST_CHECK( ArrayQ::M == 6 );
  BOOST_CHECK( MatrixQ::M == 6 );
  BOOST_CHECK( MatrixQ::N == 6 );

  // ctors

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;

  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  BCClass bc1(pde);

  BOOST_CHECK( bc1.D == 3 );
  BOOST_CHECK( bc1.N == 6 );
  BOOST_CHECK( bc1.NBC == 4 );

  //Pydict constructor
  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  PyDict BCList;
  BCList["BC1"] = BCNoSlip;
  BCParams::checkInputs(BCList);

  BCClass bc(pde, BCNoSlip);

  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 6 );
  BOOST_CHECK( bc.NBC == 4 );

  // functions

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  Real x, y, z, time;
  Real nx, ny, nz, dist = 0;
  Real rho, u, v, w, t, p, nt;

  x = y = z = time = 0;   // not actually used in functions
  nx = -0.856;  ny = 0.48;  nz = 0.192;    // Note: magnitude = 1
  rho = 1.137;
  u =  1.451;
  v = -0.567;
  w = -1.003;
  t = 300.1;
  p = R*rho*t;
  nt = 1.3;

  ArrayQ q = pde.setDOFFrom( SAnt3D<DensityVelocityPressure3D<Real>>({rho, u, v, w, p, nt}) );

  ArrayQ qB;
  bc.state(dist, x, y, z, time, nx, ny, nz, q, qB);

  Real rhoB = rho;
  Real uB = 0;
  Real vB = 0;
  Real wB = 0;
  Real pB = p;
  Real ntB = 0;

  ArrayQ qTrue = pde.setDOFFrom( SAnt3D<DensityVelocityPressure3D<Real>>({rhoB, uB, vB, wB, pB, ntB}) );

  SANS_CHECK_CLOSE( qTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(4), qB(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(5), qB(5), small_tol, close_tol );

  Real rhox = 0.01, rhoy = -0.025, rhoz = -0.344;
  Real ux = 0.049, uy =  0.072, uz =  1.431;
  Real vx = 0.143, vy = -0.024, vz = -0.987;
  Real wx = 0.659, wy =  0.349, wz =  0.776;
  Real px = 0.002, py = -4.231, pz = -0.451;
  Real ntx = -0.23, nty = 1.3, ntz = 5.1;

  ArrayQ qx = {rhox, ux, vx, wx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, wy, py, nty};
  ArrayQ qz = {rhoz, uz, vz, wz, pz, ntz};

  ArrayQ fx = 0, fy = 0, fz = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, z, time, qTrue, fx, fy, fz);
  FnTrue += fx*nx + fy*ny + fz*nz;

  // viscous flux
  ArrayQ fv = 0, gv = 0, hv = 0;
  pde.fluxViscous(x, y, z, time, qTrue, qx, qy, qz, fv, gv, hv);
  ArrayQ Fvn = fv*nx + gv*ny + hv*nz;

  // Zero out the energy equation for the adiabatic wall
  Fvn[pde.iEngy] = 0;

  // Set the Truth value
  FnTrue += Fvn;

  // flux from the BC
  ArrayQ Fn = 0;
  bc.fluxNormal( dist, x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn);

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(5), Fn(5), small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test_BCTypeSymmetry_mitState )
{
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> PDEClass;

  typedef BCRANSSA3D< BCTypeNeumann_mitState,
                      BCNavierStokes3D<BCTypeSymmetry_mitState, PDEClass> > BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  typedef BCParameters< BCRANSSA3DVector<TraitsSizeRANSSA, TraitsModel> > BCParams;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 6 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( ArrayQ::M == 6 );
  BOOST_CHECK( MatrixQ::M == 6 );
  BOOST_CHECK( MatrixQ::N == 6 );

  // ctors

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;

  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  BCClass bc1(pde);

  BOOST_CHECK( bc1.D == 3 );
  BOOST_CHECK( bc1.N == 6 );
  BOOST_CHECK( bc1.NBC == 1 );

  //Pydict constructor
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCList;
  BCList["BC1"] = BCSymmetry;
  BCParams::checkInputs(BCList);

  BCClass bc(pde, BCSymmetry);

  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 6 );
  BOOST_CHECK( bc.NBC == 1 );
  BOOST_CHECK( bc.hasFluxViscous() == true );

  // functions

  const Real small_tol = 1.0e-9;
  const Real close_tol = 1.0e-12;

  Real x, y, z, time, dist;
  Real nx, ny, nz;
  Real rho, u, v, w, t, p, nt;

  x = y = z = time = dist = 0;   // not actually used in functions
  nx = -0.856;  ny = 0.48;  nz = 0.192;    // Note: magnitude = 1
  rho = 1.137;
  u =  1.451;
  v = -0.567;
  w = -1.003;
  t = 300.1;
  p = R*rho*t;
  nt = 1.3;

  // VB = V - n*dot(V,n)
  Real uB = u - nx*(u*nx + v*ny + w*nz);
  Real vB = v - ny*(u*nx + v*ny + w*nz);
  Real wB = w - nz*(u*nx + v*ny + w*nz);

  Real rhoB = rho;
  Real pB = p;
  Real ntB = nt;

  ArrayQ q, qB, qTrue;
  SAnt3D<DensityVelocityPressure3D<Real>> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.VelocityZ = w;
  qdata.Pressure  = p;
  qdata.SANutilde = nt;
  pde.setDOFFrom( q, qdata );

  pde.variableInterpreter().setFromPrimitive( qTrue, SAnt3D<DensityVelocityPressure3D<Real>>({rhoB, uB, vB, wB, pB, ntB}) );

  bc.state( dist, x, y, z, time, nx, ny, nz, q, qB );
  SANS_CHECK_CLOSE( qTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(4), qB(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(5), qB(5), small_tol, close_tol );


  Real rhox = 0.01, rhoy = -0.025, rhoz = -0.344;
  Real ux = 0.049, uy =  0.072, uz =  1.431;
  Real vx = 0.143, vy = -0.024, vz = -0.987;
  Real wx = 0.659, wy =  0.349, wz =  0.776;
  Real px = 0.002, py = -4.231, pz = -0.451;
  Real ntx = -0.23, nty = 1.3, ntz = 5.1;

  ArrayQ qx = {rhox, ux, vx, wx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, wy, py, nty};
  ArrayQ qz = {rhoz, uz, vz, wz, pz, ntz};

  ArrayQ Fn = 0;
  bc.fluxNormal( dist, x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn);

  ArrayQ fx = 0, fy = 0, fz = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, z, time, qB, fx, fy, fz);
  FnTrue += fx*nx + fy*ny + fz*nz;

  // Compute the viscous flux
  ArrayQ fv = 0, gv = 0, hv = 0;
  pde.fluxViscous( x, y, z, time, qB, qx, qy, qz, fv, gv, hv );

  // Compute normal x-, y- and z-momentum fluxes
  Real xMomN = fv[pde.ixMom]*nx + gv[pde.ixMom]*ny + hv[pde.ixMom]*nz;
  Real yMomN = fv[pde.iyMom]*nx + gv[pde.iyMom]*ny + hv[pde.iyMom]*nz;
  Real zMomN = fv[pde.izMom]*nx + gv[pde.izMom]*ny + hv[pde.izMom]*nz;

  FnTrue[pde.ixMom] += (xMomN*nx + yMomN*ny + zMomN*nz)*nx;
  FnTrue[pde.iyMom] += (xMomN*nx + yMomN*ny + zMomN*nz)*ny;
  FnTrue[pde.izMom] += (xMomN*nx + yMomN*ny + zMomN*nz)*nz;

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(5), Fn(5), small_tol, close_tol );

  BOOST_CHECK( bc.isValidState(nx, ny, nz, q) == true );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test_BCTypeReflect_mitState )
{
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> PDEClass;

  typedef BCEuler3D<BCTypeReflect_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;
  typedef DLA::VectorS<BCClass::D,Real> VectorX;
  typedef DLA::MatrixS<BCClass::D,BCClass::D,Real> MatrixX;

  typedef BCParameters< BCRANSSA3DVector<TraitsSizeRANSSA, TraitsModel> > BCParams;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 6 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( ArrayQ::M == 6 );
  BOOST_CHECK( MatrixQ::M == 6 );
  BOOST_CHECK( MatrixQ::N == 6 );

  // ctors

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;

  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  BCClass bc1(pde);

  BOOST_CHECK( bc1.D == 3 );
  BOOST_CHECK( bc1.N == 6 );
  BOOST_CHECK( bc1.NBC == 1 );
  BOOST_CHECK( bc1.hasFluxViscous() == true );

  //Pydict constructor
  PyDict BCReflect;
  BCReflect[BCParams::params.BC.BCType] = BCParams::params.BC.Reflect_mitState;

  PyDict BCList;
  BCList["BC1"] = BCReflect;
  BCParams::checkInputs(BCList);

  BCClass bc(pde, BCReflect);

  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 6 );
  BOOST_CHECK( bc.NBC == 1 );
  BOOST_CHECK( bc.hasFluxViscous() == true );

  // functions

  const Real small_tol = 1.0e-9;
  const Real close_tol = 1.0e-12;

  Real x, y, z, time, dist;
  Real nx, ny, nz;
  Real rho, u, v, w, t, p, nt;

  x = y = z = time = dist = 0;   // not actually used in functions
  nx = -0.856;  ny = 0.48;  nz = 0.192;    // Note: magnitude = 1
  rho = 1.137;
  u =  1.451;
  v = -0.567;
  w = -1.003;
  t = 300.1;
  p = R*rho*t;
  nt = 1.3;

  // VB = V - n*dot(V,n)
  Real uB = u - 2*nx*(u*nx + v*ny + w*nz);
  Real vB = v - 2*ny*(u*nx + v*ny + w*nz);
  Real wB = w - 2*nz*(u*nx + v*ny + w*nz);

  Real rhoB = rho;
  Real pB = p;
  Real ntB = nt;

  ArrayQ qI, qB, qTrue;
  SAnt3D<DensityVelocityPressure3D<Real>> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.VelocityZ = w;
  qdata.Pressure  = p;
  qdata.SANutilde = nt;
  pde.setDOFFrom( qI, qdata );

  pde.variableInterpreter().setFromPrimitive( qTrue, SAnt3D<DensityVelocityPressure3D<Real>>({rhoB, uB, vB, wB, pB, ntB}) );

  bc.state( dist, x, y, z, time, nx, ny, nz, qI, qB );
  SANS_CHECK_CLOSE( qTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(4), qB(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(5), qB(5), small_tol, close_tol );


  Real rhox = 0.01, rhoy = -0.025, rhoz = -0.344;
  Real ux = 0.049, uy =  0.072, uz =  1.431;
  Real vx = 0.143, vy = -0.024, vz = -0.987;
  Real wx = 0.659, wy =  0.349, wz =  0.776;
  Real px = 0.002, py = -4.231, pz = -0.451;
  Real ntx = -0.23, nty = 1.3, ntz = 5.1;

  ArrayQ qIx = {rhox, ux, vx, wx, px, ntx};
  ArrayQ qIy = {rhoy, uy, vy, wy, py, nty};
  ArrayQ qIz = {rhoz, uz, vz, wz, pz, ntz};

  ArrayQ Fn = 0;
  bc.fluxNormal( dist, x, y, z, time, nx, ny, nz, qI, qIx, qIy, qIz, qB, Fn );

  // compute advective flux with reflected state vector

  ArrayQ FnTrue = 0;
  pde.fluxAdvectiveUpwind( x, y, z, time, qI, qB, nx, ny, nz, FnTrue );

  // reflect scalars
  Real rhoBx = rhox - 2*nx*(rhox*nx + rhoy*ny + rhoz*nz);
  Real rhoBy = rhoy - 2*ny*(rhox*nx + rhoy*ny + rhoz*nz);
  Real rhoBz = rhoz - 2*nz*(rhox*nx + rhoy*ny + rhoz*nz);

  Real pBx = px - 2*nx*(px*nx + py*ny + pz*nz);
  Real pBy = py - 2*ny*(px*nx + py*ny + pz*nz);
  Real pBz = pz - 2*nz*(px*nx + py*ny + pz*nz);

  Real ntBx = ntx - 2*nx*(ntx*nx + nty*ny + ntz*nz);
  Real ntBy = nty - 2*ny*(ntx*nx + nty*ny + ntz*nz);
  Real ntBz = ntz - 2*nz*(ntx*nx + nty*ny + ntz*nz);

  VectorX gradIu = {ux, uy, uz};
  VectorX gradIv = {vx, vy, vz};
  VectorX gradIw = {wx, wy, wz};

  MatrixX Jr = { {1-2*nx*nx,  -2*nx*ny,  -2*nx*nz},
                 { -2*ny*nx, 1-2*ny*ny,  -2*ny*nz},
                 { -2*nz*nx,  -2*nz*ny, 1-2*nz*nz} };

  VectorX gradBu = Jr*(gradIu - 2*nx*(gradIu*nx + gradIv*ny + gradIw*nz));
  VectorX gradBv = Jr*(gradIv - 2*ny*(gradIu*nx + gradIv*ny + gradIw*nz));
  VectorX gradBw = Jr*(gradIw - 2*nz*(gradIu*nx + gradIv*ny + gradIw*nz));

  ArrayQ qBx = {rhoBx, gradBu[0], gradBv[0], gradBw[0], pBx, ntBx};
  ArrayQ qBy = {rhoBy, gradBu[1], gradBv[1], gradBw[1], pBy, ntBy};
  ArrayQ qBz = {rhoBz, gradBu[2], gradBv[2], gradBw[2], pBz, ntBz};

  // Compute the viscous flux
  pde.fluxViscous( x, y, z, time, qI, qIx, qIy, qIz, qB, qBx, qBy, qBz, nx, ny, nz, FnTrue);

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(5), Fn(5), small_tol, close_tol );

  BOOST_CHECK( bc.isValidState(nx, ny, nz, qI) == true );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test_BCTypeFullState_mitState )
{
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> PDEClass;

  typedef BCRANSSA3D< BCTypeFullState_mitState,
                      BCEuler3D<BCTypeFullState_mitState, PDEClass> > BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  typedef BCParameters< BCRANSSA3DVector<TraitsSizeRANSSA, TraitsModel> > BCParams;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 6 );
  BOOST_CHECK( BCClass::NBC == 6 );
  BOOST_CHECK( ArrayQ::M == 6 );
  BOOST_CHECK( MatrixQ::M == 6 );
  BOOST_CHECK( MatrixQ::N == 6 );

  // ctors

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;

  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  typedef SAnt3D<DensityVelocityPressure3D<Real>>::ParamsType ParamsType;

  PyDict d;
  d[SAVariableType3DParams::params.StateVector.Variables] = SAVariableType3DParams::params.StateVector.PrimitivePressure;
  d[ParamsType::params.rho] = 1.1;
  d[ParamsType::params.u] = 1.3;
  d[ParamsType::params.v] = -0.4;
  d[ParamsType::params.w] =  0.9;
  d[ParamsType::params.p] = 5.45;
  d[ParamsType::params.nt] = 4.6;

  PyDict BCFullState;
  BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFullState[BCClass::ParamsType::params.Characteristic] = false;
  BCFullState[BCClass::ParamsType::params.StateVector] = d;

  PyDict BCList;
  BCList["BC1"] = BCFullState;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCFullState);

  BOOST_CHECK( bc2.D == 3 );
  BOOST_CHECK( bc2.N == 6 );
  BOOST_CHECK( bc2.NBC == 6 );

  // functions

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  Real dist, x, y, z, time;
  Real nx, ny, nz;
  Real rho, u, v, w, t, p, nt;

  dist = x = y = z = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; w = 0.347, t = 0.987;
  p  = R*rho*t;
  nt = 1.3;

  ArrayQ q;
  SAnt3D<DensityVelocityPressure3D<Real>> qdata({rho, u, v, w, p, nt});
  pde.setDOFFrom( q, qdata );

  BCClass bcT(pde, qdata, true);
  BCClass bcF(pde, qdata, false);

  BOOST_CHECK( bcT.D == 3 );
  BOOST_CHECK( bcT.N == 6 );
  BOOST_CHECK( bcT.NBC == 6 );
  BOOST_CHECK( bcT.useUpwind() == true );
  BOOST_CHECK( bcF.useUpwind() == false );

  ArrayQ qI = 0;
  ArrayQ qB;

  bcT.state(dist, x, y, z, time, nx, ny, nz, qI, qB);

  Real rhoB, uB, vB, wB, tB, ntB;
  pde.variableInterpreter().eval(qB, rhoB, uB, vB, wB, tB);
  pde.variableInterpreter().evalSA(qB, ntB);

  SANS_CHECK_CLOSE( rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE( u, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE( v, vB, small_tol, close_tol );
  SANS_CHECK_CLOSE( w, wB, small_tol, close_tol );
  SANS_CHECK_CLOSE( t, tB, small_tol, close_tol );
  SANS_CHECK_CLOSE( nt, ntB, small_tol, close_tol );

  qI = pde.setDOFFrom( SAnt3D<DensityVelocityPressure3D<Real>>({rhoB, uB, vB, wB, tB, ntB}) );
  BOOST_CHECK( bcT.isValidState(nx, ny, nz, qI) == true );
  BOOST_CHECK( bcF.isValidState(nx, ny, nz, qI) == true );

  qI = pde.setDOFFrom( SAnt3D<DensityVelocityPressure3D<Real>>({rhoB, -uB, -vB, -wB, tB, ntB}) );
  BOOST_CHECK( bcT.isValidState(nx, ny, nz, qI) == true );
  BOOST_CHECK( bcF.isValidState(nx, ny, nz, qI) == false );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test_BCTypeOutflowSubsonic_Pressure_mitState )
{
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> PDEClass;

  typedef BCRANSSA3D< BCTypeNeumann_mitState,
                      BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDEClass> > BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  typedef BCParameters< BCRANSSA3DVector<TraitsSizeRANSSA, TraitsModel> > BCParams;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 6 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( ArrayQ::M == 6 );
  BOOST_CHECK( MatrixQ::M == 6 );
  BOOST_CHECK( MatrixQ::N == 6 );

  // ctors

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;

  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  const Real pSpec = 101325;
  BCClass bc(pde, pSpec);

  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 6 );
  BOOST_CHECK( bc.NBC == 1 );

  PyDict BCOutflowSubsonic;
  BCOutflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOutflowSubsonic[BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pSpec;

  PyDict BCList;
  BCList["BC1"] = BCOutflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCOutflowSubsonic);

  BOOST_CHECK( bc2.D == 3 );
  BOOST_CHECK( bc2.N == 6 );
  BOOST_CHECK( bc2.NBC == 1 );

  // functions

  const Real small_tol = 3.e-12;
  const Real close_tol = 3.e-12;

  Real x, y, z, time;
  Real nx, ny, nz, dist = 0;
  Real rho, u, v, w, t, p, c, nt;

  // Test conditions: M = 0.3
  x = y = z = time = 0;   // not actually used in functions
  nx = 0.856;  ny = 0.48;  nz = 0.192;    // Note: magnitude = 1
  rho = 1.137;
  t = 300.1;
  p = R*rho*t;
  c = sqrt(gamma*R*t);
  nt = 2.1;

  Real M = 0.3;
  Real alpha = 0.1;
  Real beta  = 0.4;
  u = M*c*cos(alpha)*cos(beta);
  v = M*c*           sin(beta);
  w = M*c*sin(alpha)*cos(beta);

  ArrayQ q = pde.setDOFFrom( SAnt3D<DensityVelocityPressure3D<Real>>({rho, u, v, w, p, nt}) );

  ArrayQ qB, qBTrue;

  bc.state(dist, x, y, z, time, nx, ny, nz, q, qB);

  qBTrue[0] = 1.1649158602997538;
  qBTrue[1] = 88.25003812727218;
  qBTrue[2] = 36.517086088327524;
  qBTrue[3] = 7.958543517473888;
  qBTrue[4] = 101325;
  qBTrue[5] = nt;

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(4), qB(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(5), qB(5), small_tol, close_tol );


  Real rhox = 0.01, rhoy = -0.025, rhoz = -0.344;
  Real ux = 0.049, uy =  0.072, uz =  1.431;
  Real vx = 0.143, vy = -0.024, vz = -0.987;
  Real wx = 0.659, wy =  0.349, wz =  0.776;
  Real px = 0.002, py = -4.231, pz = -0.451;
  Real ntx = -0.23, nty = 1.3, ntz = 5.1;

  ArrayQ qx = {rhox, ux, vx, wx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, wy, py, nty};
  ArrayQ qz = {rhoz, uz, vz, wz, pz, ntz};

  ArrayQ Fn = 0;
  bc.fluxNormal( dist, x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn);

  ArrayQ fx = 0, fy = 0, fz = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, z, time, qB, fx, fy, fz);
  FnTrue += fx*nx + fy*ny + fz*nz;

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(5), Fn(5), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test_BCTypeInflowSubsonic_PtTta_mitState )
{
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> PDEClass;

  typedef BCRANSSA3D< BCTypeDirichlet_mitState,
                      BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDEClass> > BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  typedef BCParameters< BCRANSSA3DVector<TraitsSizeRANSSA, TraitsModel> > BCParams;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 6 );
  BOOST_CHECK( BCClass::NBC == 5 );
  BOOST_CHECK( ArrayQ::M == 6 );
  BOOST_CHECK( MatrixQ::M == 6 );
  BOOST_CHECK( MatrixQ::N == 6 );

  // ctors

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;

  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // functions

  const Real small_tol = 3.e-12;
  const Real close_tol = 3.e-12;

  Real x, y, z, time;
  Real nx, ny, nz, dist = 0;
  Real rho, u, v, w, t, p, c, nt;

  // Test conditions: M = 0.3
  x = y = z = time = 0;   // not actually used in functions
  nx = -0.856;  ny = 0.48;  nz = 0.192;    // Note: magnitude = 1
  rho = 1.137;
  t = 300.1;
  p = R*rho*t;
  c = sqrt(gamma*R*t);
  nt = 2.1;

  Real M = 0.3;
  Real alpha = 0.1;
  Real beta  = 0.4;
  u = M*c*cos(alpha)*cos(beta);
  v = M*c*           sin(beta);
  w = M*c*sin(alpha)*cos(beta);

  const Real gmi = gamma - 1;

  Real TtSpec = t*(1+0.5*gmi*M*M);
  Real PtSpec = p*pow( (1+0.5*gmi*M*M), gamma/gmi );
  Real aSpec = alpha;
  Real bSpec = beta;

  BCClass bc2(pde, nt, TtSpec, PtSpec, aSpec, bSpec);

  typedef BCRANSA3DParams<BCTypeDirichlet_mitState, BCEuler3DParams<BCTypeInflowSubsonic_PtTta_mitState>> PtTtaParams;

  PyDict BCInflowSubsonic;
  BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  BCInflowSubsonic[PtTtaParams::params.TtSpec] = TtSpec;
  BCInflowSubsonic[PtTtaParams::params.PtSpec] = PtSpec;
  BCInflowSubsonic[PtTtaParams::params.aSpec] = aSpec;
  BCInflowSubsonic[PtTtaParams::params.bSpec] = bSpec;
  BCInflowSubsonic[PtTtaParams::params.nt] = nt;

  PyDict BCList;
  BCList["BC1"] = BCInflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc(pde, BCInflowSubsonic);

  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 6 );
  BOOST_CHECK( bc.NBC == 5 );

  ArrayQ q = pde.setDOFFrom( SAnt3D<DensityVelocityPressure3D<Real>>({rho, u, v, w, p, nt}) );

  ArrayQ qB, qBTrue;

  bc.state(dist, x, y, z, time, nx, ny, nz, q, qB);

  Real rhoB, uB, vB, wB, tB;
  pde.variableInterpreter().eval(qB, rhoB, uB, vB, wB, tB);

  Real rhoTrue = rho;
  Real uTrue = u;
  Real vTrue = v;
  Real wTrue = w;
  Real tTrue = t;

  SANS_CHECK_CLOSE(rhoTrue, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(uTrue, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(vTrue, vB, small_tol, close_tol );
  SANS_CHECK_CLOSE(wTrue, wB, small_tol, close_tol );
  SANS_CHECK_CLOSE(tTrue, tB, small_tol, close_tol );


  Real rhox = 0.01, rhoy = -0.025, rhoz = -0.344;
  Real ux = 0.049, uy =  0.072, uz =  1.431;
  Real vx = 0.143, vy = -0.024, vz = -0.987;
  Real wx = 0.659, wy =  0.349, wz =  0.776;
  Real px = 0.002, py = -4.231, pz = -0.451;
  Real ntx = -0.23, nty = 1.3, ntz = 5.1;

  ArrayQ qx = {rhox, ux, vx, wx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, wy, py, nty};
  ArrayQ qz = {rhoz, uz, vz, wz, pz, ntz};

  ArrayQ Fn = 0;
  bc.fluxNormal( dist, x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn);

  ArrayQ fx = 0, fy = 0, fz = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, z, time, qB, fx, fy, fz);
  FnTrue += fx*nx + fy*ny + fz*nz;

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(5), Fn(5), small_tol, close_tol );
}



#if 0   // old file organization
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  {
  typedef BCRANSSA3D<BCTypeWall_mitState,
                     BCNavierStokes3D< BCTypeWallNoSlipAdiabatic_mitState, PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> > > BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 6 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 6 );
  BOOST_CHECK( MatrixQ::M == 6 );
  BOOST_CHECK( MatrixQ::N == 6 );
  }

  {
  typedef BCRANSSA3D<BCTypeDirichlet_mitState,
                     BCNavierStokes3D< BCTypeWallNoSlipAdiabatic_mitState, PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> > > BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 6 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 6 );
  BOOST_CHECK( MatrixQ::M == 6 );
  BOOST_CHECK( MatrixQ::N == 6 );
  }

  {
  typedef BCRANSSA3D< BCTypeFullState_mitState,
                      BCEuler3D< BCTypeFullState_mitState, PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> > > BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 6 );
  BOOST_CHECK( BCClass::NBC == 5 );
  BOOST_CHECK( ArrayQ::M == 6 );
  BOOST_CHECK( MatrixQ::M == 6 );
  BOOST_CHECK( MatrixQ::N == 6 );
  }

  {
  typedef BCRANSSA3D< BCTypeInflowSubsonic_sHqt_BN,
                      BCEuler3D< BCTypeInflowSubsonic_sHqt_BN, PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> > > BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 6 );
  BOOST_CHECK( BCClass::NBC == 4 );
  BOOST_CHECK( ArrayQ::M == 6 );
  BOOST_CHECK( MatrixQ::M == 6 );
  BOOST_CHECK( MatrixQ::N == 6 );
  }

  {
  typedef BCRANSSA3D< BCTypeFunctionInflow_sHqt_BN,
                      BCEuler3D< BCTypeFunctionInflow_sHqt_BN, PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> > > BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;
  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 6 );
  BOOST_CHECK( BCClass::NBC == 4 );
  BOOST_CHECK( ArrayQ::M == 6 );
  BOOST_CHECK( MatrixQ::M == 6 );
  BOOST_CHECK( MatrixQ::N == 6 );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor )
{
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> PDEClass;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  typedef BCParameters< BCRANSSA3DVector<TraitsSizeRANSSA, TraitsModel> > BCParams;

  {
  typedef BCRANSSA3D<BCTypeWall_mitState, BCNavierStokes3D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass>> BCClass;

  BCClass bc1(pde);

  //Pydict constructor
  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  PyDict BCList;
  BCList["BC1"] = BCNoSlip;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCNoSlip);
  }

  {
  typedef BCRANSSA3D<BCTypeNeumann_mitState, BCNavierStokes3D<BCTypeSymmetry_mitState, PDEClass>> BCClass;

  BCClass bc1(pde);

  //Pydict constructor
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCList;
  BCList["BC1"] = BCSymmetry;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCSymmetry);
  }

  {
  typedef BCRANSSA3D<BCTypeWall_mitState, BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDEClass> > BCClass;

  const Real pSpec = 2.1;
  BCClass bc(pde, pSpec);

  PyDict BCOutflowSubsonic;
  BCOutflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOutflowSubsonic[BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pSpec;

  PyDict BCList;
  BCList["BC1"] = BCOutflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCOutflowSubsonic);
  }

  {
  typedef BCRANSSA3D< BCTypeFullState_mitState,
                      BCEuler3D< BCTypeFullState_mitState, PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> > > BCClass;

  //bool upwind = true;
  //BCClass bc(pde, SAnt3D<DensityVelocityPressure3D<Real>>({1.1, 1.3, -0.4, 5.45, 4.6}), upwind);

  typedef SAnt3D<DensityVelocityPressure3D<Real>>::ParamsType ParamsType;

  PyDict d;
  d[NSVariableType3DParams::params.StateVector.Variables] = SAVariableType3DParams::params.StateVector.PrimitivePressure;
  d[ParamsType::params.rho] = 1.1;
  d[ParamsType::params.u] = 1.3;
  d[ParamsType::params.v] = -0.4;
  d[ParamsType::params.p] = 5.45;
  d[ParamsType::params.nt] = 4.6;

  PyDict BCFullState;
  BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFullState[BCClass::ParamsType::params.Characteristic] = false;
  BCFullState[BCClass::ParamsType::params.StateVector] = d;

  PyDict BCList;
  BCList["BC1"] = BCFullState;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCFullState);
  }

  {
  typedef BCRANSSA3D<BCTypeDirichlet_mitState, BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDEClass> > BCClass;

  typedef BCClass::ParamsType ParamsType;

  Real TtSpec = 20.5;
  Real PtSpec = 71.4;
  Real aSpec = 2.1;
  Real nt = 1.1;
  BCClass bc1(pde, nt, TtSpec, PtSpec, aSpec);

  PyDict BCInflowSubsonic;
  BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  BCInflowSubsonic[ParamsType::params.TtSpec] = TtSpec;
  BCInflowSubsonic[ParamsType::params.PtSpec] = PtSpec;
  BCInflowSubsonic[ParamsType::params.aSpec] = aSpec;
  BCInflowSubsonic[ParamsType::params.nt] = nt;

  PyDict BCList;
  BCList["BC1"] = BCInflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCInflowSubsonic);
  }

  {
  typedef BCRANSSA3D<BCTypeInflowSubsonic_sHqt_BN, BCEuler3D<BCTypeInflowSubsonic_sHqt_BN, PDEClass> > BCClass;

  typedef BCClass::ParamsType ParamsType;

  Real sSpec = 2.2;
  Real HSpec = 1.0;
  Real aSpec = 0.1;
  Real nt = 1.1;
  BCClass bc1(pde, nt, sSpec, HSpec, aSpec);

  PyDict BCInflowSubsonic;
  BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  BCInflowSubsonic[ParamsType::params.sSpec] = sSpec;
  BCInflowSubsonic[ParamsType::params.HSpec] = HSpec;
  BCInflowSubsonic[ParamsType::params.aSpec] = aSpec;
  BCInflowSubsonic[ParamsType::params.nt] = nt;

  PyDict BCList;
  BCList["BC1"] = BCInflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCInflowSubsonic);
  }

  {
  typedef BCRANSSA3D<BCTypeFunctionInflow_sHqt_BN, BCEuler3D<BCTypeFunctionInflow_sHqt_BN, PDEClass> > BCClass;
  typedef TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel>
    TraitsModelRANSSAClass;
  typedef SolutionFunction_Euler3D_Const<TraitsSizeRANSSA, TraitsModelRANSSAClass> SolutionFunction_EulerClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;

  PyDict Function;
  Function[BCClass::ParamsType::params.Function.Name] = BCClass::ParamsType::params.Function.Const;
  Function[SolutionFunction_EulerClass::ParamsType::params.gasModel] = gasModelDict;
  Function[SolutionFunction_EulerClass::ParamsType::params.rho] = 1.0;
  Function[SolutionFunction_EulerClass::ParamsType::params.u] = 0.4;
  Function[SolutionFunction_EulerClass::ParamsType::params.v] = 0.1;
  Function[SolutionFunction_EulerClass::ParamsType::params.p] = 1.0;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.SolutionInflow_sHqt_BN;
  BCIn[BCClass::ParamsType::params.Function] = Function;
  BCIn["nt"]  = 3; //TODO: not sure about this, was previously just "nt" which gave a Python dictionary error

  BCClass bc2(pde, BCIn);
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_wallnoslip )
{
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> PDEClass;
  typedef BCParameters< BCRANSSA3DVector<TraitsSizeRANSSA, TraitsModel> > BCParams;

  typedef BCRANSSA3D<BCTypeWall_mitState, BCNavierStokes3D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass>> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);

  //Pydict constructor
  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;
  BCClass bc(pde, BCNoSlip);

  Real x, y, z, time;
  Real nx, ny, nz, dist = 0;
  Real rho, u, v, w, t, p, nt;

  // Test conditions - M = 0.3, T0 = 300.1, P0 = 101325; alpha = 0.1 rad
  x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137;
  t = 300.1;
  p  = R*rho*t;
  nt = 1.3;

  Real M = 0.3;
  Real c =  sqrt(gamma*R*t);
  Real alpha = 0.1;
  u = M*c*cos(alpha);
  v = M*c*sin(alpha);

  ArrayQ q = pde.setDOFFrom( SAnt3D<DensityVelocityPressure3D<Real>>({rho, u, v, w, p, nt}) );

  ArrayQ qB;
  bc.state(dist, x, y, z, time, nx, ny, nz, q, qB);

  Real rhoB = rho;
  Real uB = 0;
  Real vB = 0;
  Real pB = p;
  Real ntB = 0;

  ArrayQ qTrue = pde.setDOFFrom( SAnt3D<DensityVelocityPressure3D<Real>>({rhoB, uB, vB, pB, ntB}) );

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;
  SANS_CHECK_CLOSE(    qTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE(    qTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE(    qTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE(    qTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE(    qTrue(4), qB(4), small_tol, close_tol );

  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  Real ntx = -0.23; Real nty = 1.3;
  ArrayQ qx = {rhox, ux, vx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, py, nty};

  ArrayQ fx = 0;
  ArrayQ fy = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, z, time, qTrue, fx, fy);
  FnTrue += fx*nx + fy*ny;

  // Compute the viscous flux
  ArrayQ fv = 0, gv = 0;
  pde.fluxViscous(x, y, z, time, qTrue, qx, qy, fv, gv);
  ArrayQ Fvn = fv*nx + gv*ny;

  // Zero out the energy equation for the adiabatic wall
  Fvn[pde.iEngy] = 0;

  // Set the Truth value
  FnTrue += Fvn;

  // Compute the flux from the BC
  ArrayQ Fn = 0;
  bc.fluxNormal( dist, x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn);

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_symmetry )
{
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> PDEClass;
  typedef BCParameters< BCRANSSA3DVector<TraitsSizeRANSSA, TraitsModel> > BCParams;

  typedef BCRANSSA3D<BCTypeNeumann_mitState, BCNavierStokes3D<BCTypeSymmetry_mitState, PDEClass>> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);

  //Pydict constructor
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  BCClass bc(pde, BCSymmetry);

  Real x, y, z, time;
  Real nx, ny, nz;
  Real rho, u, v, w, t, p, nt, dist = 0;

  // Test conditions - M = 0.3, T0 = 300.1, P0 = 101325; alpha = 0.1 rad
  x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137;
  t = 300.1;
  p  = R*rho*t;
  nt = 1.3;

  Real M = 0.3;
  Real c =  sqrt(gamma*R*t);
  Real alpha = 0.1;
  u = M*c*cos(alpha);
  v = M*c*sin(alpha);

  ArrayQ q = pde.setDOFFrom( SAnt3D<DensityVelocityPressure3D<Real>>({rho, u, v, w, p, nt}) );

  Real rhoB = rho;

  //VB = V - n*dot(V,n)
  Real uB = u - nx*(u*nx + v*ny);
  Real vB = v - ny*(u*nx + v*ny);

  Real pB = p;

  Real ntB = nt;

  ArrayQ qTrue = pde.setDOFFrom( SAnt3D<DensityVelocityPressure3D<Real>>({rhoB, uB, vB, pB, ntB}) );

  ArrayQ qB;
  bc.state(dist, x, y, z, time, nx, ny, nz, q, qB);

  const Real small_tol = 1.0e-9;
  const Real close_tol = 1.0e-12;
  SANS_CHECK_CLOSE( qTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(4), qB(4), small_tol, close_tol );


  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  Real ntx = -0.23; Real nty = 1.3;
  ArrayQ qx = {rhox, ux, vx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, py, nty};

  ArrayQ Fn = 0;
  bc.fluxNormal(dist, x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn);

  ArrayQ fx = 0;
  ArrayQ fy = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, z, time, qTrue, fx, fy);
  FnTrue += fx*nx + fy*ny;

  // Compute the viscous flux
  ArrayQ fv = 0, gv = 0;
  pde.fluxViscous(x, y, z, time, qB, qx, qy, fv, gv);

  // Compute normal x-momentum and y-momentum equations
  Real xMomN = fv[pde.ixMom]*nx + gv[pde.ixMom]*ny;
  Real yMomN = fv[pde.iyMom]*nx + gv[pde.iyMom]*ny;

  FnTrue[pde.ixMom] += (xMomN*nx + yMomN*ny)*nx;
  FnTrue[pde.iyMom] += (xMomN*nx + yMomN*ny)*ny;

  // No viscous constribution to SA intentionally

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_fullstate )
{
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> PDEClass;

  typedef BCRANSSA3D< BCTypeFullState_mitState,
                      BCEuler3D< BCTypeFullState_mitState, PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> > > BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCParameters< BCRANSSA3DVector<TraitsSizeRANSSA, TraitsModel> > BCParams;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real dist, x, y, z, time;
  Real nx, ny, nz;
  Real rho, u, v, w, t, p, nt;

  dist = x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  p  = R*rho*t;
  nt = 1.3;

  typedef SAnt3D<DensityVelocityPressure3D<Real>>::ParamsType ParamsType;

  PyDict d;
  d[NSVariableType3DParams::params.StateVector.Variables] = SAVariableType3DParams::params.StateVector.PrimitivePressure;
  d[ParamsType::params.rho] = rho;
  d[ParamsType::params.u] = u;
  d[ParamsType::params.v] = v;
  d[ParamsType::params.p] = p;
  d[ParamsType::params.nt] = nt;

  PyDict BCFullState;
  BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFullState[BCClass::ParamsType::params.StateVector] = d;

  ArrayQ q = pde.setDOFFrom( BCFullState );

  BCFullState[BCClass::ParamsType::params.Characteristic] = false;
  BCClass bc(pde, BCFullState);

  BCFullState[BCClass::ParamsType::params.Characteristic] = true;
  BCClass bc2(pde, BCFullState);

  // static tests
  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 6 );
  BOOST_CHECK( bc.NBC == 5 );
  BOOST_CHECK_EQUAL(false, bc.useUpwind());
  BOOST_CHECK_EQUAL(true, bc2.useUpwind());

  ArrayQ qI = 0;
  ArrayQ qB;

  bc.state(dist, x, y, z, time, nx, ny, nz, qI, qB);

  Real rhoB, uB, vB, tB, ntB;
  pde.variableInterpreter().eval(qB, rhoB, uB, vB, tB);
  pde.variableInterpreter().evalSA(qB, ntB);

  SANS_CHECK_CLOSE(rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(u  , uB  , small_tol, close_tol );
  SANS_CHECK_CLOSE(v  , vB  , small_tol, close_tol );
  SANS_CHECK_CLOSE(t  , tB  , small_tol, close_tol );
  SANS_CHECK_CLOSE(nt , ntB , small_tol, close_tol );

  qI = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rhoB, uB, vB, tB, nt}) );
  BOOST_CHECK_EQUAL( true, bc.isValidState(nx, ny, nz, qI) );
  BOOST_CHECK_EQUAL( true, bc2.isValidState(nx, ny, nz, qI) );

  qI = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rhoB, -uB, -vB, tB, nt}) );
  BOOST_CHECK_EQUAL( false, bc.isValidState(nx, ny, nz, qI) );
  BOOST_CHECK_EQUAL( true, bc2.isValidState(nx, ny, nz, qI) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_outflow_pressure )
{
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> PDEClass;
  typedef BCParameters< BCRANSSA3DVector<TraitsSizeRANSSA, TraitsModel> > BCParams;

  typedef BCRANSSA3D<BCTypeDirichlet_mitState, BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDEClass>> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::ParamsType ParamsType;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);

  const Real small_tol = 3.e-12;
  const Real close_tol = 3.e-12;

  //Pydict constructor
  PyDict BCSubsonicOut;
  BCSubsonicOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;

  Real ntB = 2.1;

  BCSubsonicOut[ParamsType::params.pSpec] = 101325.0;
  BCSubsonicOut[ParamsType::params.nt] = ntB;

  BCClass bc(pde, BCSubsonicOut);

  // static tests
  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 6 );
  BOOST_CHECK( bc.NBC == 4 );

  Real x, y, z, time;
  Real nx, ny, nz, dist = 0;
  Real rho, u, v, w, t, p, nt;

  // Test conditions - M = 0.3, T0 = 300.1, P0 = 101325; alpha = 0.1 rad
  x = y = time = 0;   // not actually used in functions
  nx = 0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137;
  t = 300.1;
  p  = R*rho*t;
  nt = 1.3;

  Real M = 0.3;
  Real c =  sqrt(gamma*R*t);
  Real alpha = 0.1;
  u = M*c*cos(alpha);
  v = M*c*sin(alpha);

  ArrayQ q = pde.setDOFFrom( SAnt3D<DensityVelocityPressure3D<Real>>({rho, u, v, w, p, nt}) );

  ArrayQ qB, qBTrue;

  bc.state(dist, x, y, z, time, nx, ny, nz, q, qB);

  qBTrue[0] = 1.164915860299754;
  qBTrue[1] = 95.7574160100521;
  qBTrue[2] = 7.428571205323625;
  qBTrue[3] = 101325.0;
  qBTrue[4] = ntB;

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(4), qB(4), small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_PtTt )
{
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> PDEClass;
  typedef BCParameters< BCRANSSA3DVector<TraitsSizeRANSSA, TraitsModel> > BCParams;

  typedef BCRANSSA3D<BCTypeDirichlet_mitState, BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDEClass>> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::ParamsType ParamsType;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R     = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);

  Real x, y, z, time;
  Real nx, ny, nz, dist = 0;
  Real rho, u, v, w, t, p, nt;

  x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  p  = R*rho*t; // Hard coded R = 0.4
  nt = 1.3;

  ArrayQ q = pde.setDOFFrom( SAnt3D<DensityVelocityPressure3D<Real>>({rho, u, v, w, p, nt}) );

  Real TtSpec = t*(1+0.2*0.1*0.1);
  Real PtSpec = p*pow( (1+0.2*0.1*0.1), 3.5 );
  Real aSpec = 0.1;
  Real ntSpec = 2.1;

  //Pydict constructor
  PyDict PtTtIn;
  PtTtIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  PtTtIn[ParamsType::params.TtSpec] = TtSpec;
  PtTtIn[ParamsType::params.PtSpec] = PtSpec;
  PtTtIn[ParamsType::params.aSpec] = aSpec;
  PtTtIn[ParamsType::params.nt] = ntSpec;

  BCClass bc(pde, PtTtIn);

  // static tests
  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 6 );
  BOOST_CHECK( bc.NBC == 4 );

  ArrayQ qB = 0;
  bc.state(dist, x, y, z, time, nx, ny, nz, q, qB);

  Real rhoB, uB, vB, tB, ntB;
  pde.variableInterpreter().eval(qB, rhoB, uB, vB, tB);
  pde.variableInterpreter().evalSA(qB, ntB);

  Real rhoTrue = 0.789121117837660;
  Real uTrue = 0.614296756252870;
  Real vTrue = 0.061635263601788;
  Real tTrue = 0.852845639120693;
  Real ntTrue = ntSpec;

  SANS_CHECK_CLOSE(rhoTrue, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(uTrue, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(vTrue, vB, small_tol, close_tol );
  SANS_CHECK_CLOSE(tTrue, tB, small_tol, close_tol );
  SANS_CHECK_CLOSE(ntTrue, ntB, small_tol, close_tol );


  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  Real ntx = -0.23; Real nty = 1.3;
  ArrayQ qx = {rhox, ux, vx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, py, nty};

  ArrayQ Fn = 0;
  bc.fluxNormal( dist, x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn);

  ArrayQ fx = 0;
  ArrayQ fy = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, z, time, qB, fx, fy);
  FnTrue += fx*nx + fy*ny;

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
}



//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_sHqt_BN )
{
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> PDEClass;
  typedef BCParameters< BCRANSSA3DVector<TraitsSizeRANSSA, TraitsModel> > BCParams;

  typedef BCRANSSA3D<BCTypeInflowSubsonic_sHqt_BN, BCEuler3D<BCTypeInflowSubsonic_sHqt_BN, PDEClass>> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::ParamsType ParamsType;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R     = 1.0;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);

  Real x, y, z, time;
  Real nx, ny, nz, dist = 0;
  Real rho, u, v, w, t, p, nt;

  x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  p  = R*rho*t;
  nt = 1.3;

  ArrayQ q = pde.setDOFFrom( SAnt3D<DensityVelocityPressure3D<Real>>({rho, u, v, w, p, nt}) );

//  Real TtSpec = t*(1+0.2*0.1*0.1);
//  Real PtSpec = p*pow( (1+0.2*0.1*0.1), 3.5 );
  Real rhoSpec =rho*0.973;
  Real uSpec = u*1.01;
  Real vSpec = v*1.04;
  Real tSpec = t*1.06;
  Real pSpec = gas.pressure(rhoSpec,tSpec);

  Real sSpec = log( pSpec / pow(rhoSpec, gamma) );
  Real HSpec = (gas.enthalpy(rhoSpec,tSpec) + 0.5*(uSpec*uSpec+vSpec*vSpec));
  Real aSpec = atan(vSpec / uSpec);
  Real ntSpec = 2.1;

  // REGULAR INFLOW
  //Pydict constructor
  PyDict sHqt;
  sHqt[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  sHqt[ParamsType::params.sSpec] = sSpec;
  sHqt[ParamsType::params.HSpec] = HSpec;
  sHqt[ParamsType::params.aSpec] = aSpec;
  sHqt[ParamsType::params.nt] = ntSpec;

  BCClass bc(pde, sHqt);

  // SOLUTION INFLOW
  typedef BCRANSSA3D<BCTypeFunctionInflow_sHqt_BN, BCEuler3D<BCTypeFunctionInflow_sHqt_BN, PDEClass> > BCClass2;
  typedef TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel>
    TraitsModelRANSSAClass;
  typedef SolutionFunction_Euler3D_Const<TraitsSizeRANSSA, TraitsModelRANSSAClass> SolutionFunction_EulerClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;

  PyDict Function;
  Function[BCClass2::ParamsType::params.Function.Name] = BCClass2::ParamsType::params.Function.Const;
  Function[SolutionFunction_EulerClass::ParamsType::params.gasModel] = gasModelDict;
  Function[SolutionFunction_EulerClass::ParamsType::params.rho] = rhoSpec;
  Function[SolutionFunction_EulerClass::ParamsType::params.u] = uSpec;
  Function[SolutionFunction_EulerClass::ParamsType::params.v] = vSpec;
  Function[SolutionFunction_EulerClass::ParamsType::params.p] = pSpec;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.SolutionInflow_sHqt_BN;
  BCIn[BCClass2::ParamsType::params.Function] = Function;
  BCIn["nt"]  = ntSpec; //TODO: not sure about this, was previously just "nt" which gave a Python dictionary error

  BCClass2 bc2(pde, BCIn);

  // static tests
  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 6 );
  BOOST_CHECK( bc.NBC == 4 );
  BOOST_CHECK( bc2.D == 3 );
  BOOST_CHECK( bc2.N == 6 );
  BOOST_CHECK( bc2.NBC == 4 );

  ArrayQ qB = 0;
  bc.state(dist, x, y, z, time, nx, ny, nz, q, qB);

  Real rhoB, uB, vB, tB, ntB;
  pde.variableInterpreter().eval(qB, rhoB, uB, vB, tB);
  pde.variableInterpreter().evalSA(qB, ntB);

  Real rhoTrue = rho;
  Real uTrue = u;
  Real vTrue = v;
  Real tTrue = t;
  Real ntTrue = nt;

  SANS_CHECK_CLOSE(rhoTrue, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(uTrue, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(vTrue, vB, small_tol, close_tol );
  SANS_CHECK_CLOSE(tTrue, tB, small_tol, close_tol );
  SANS_CHECK_CLOSE(ntTrue, ntB, small_tol, close_tol );

  ArrayQ qB2 = 0;
  bc2.state(dist, x, y, z, time, nx, ny, nz, q, qB2);
  Real rhoB2, uB2, vB2, tB2, ntB2;
  pde.variableInterpreter().eval(qB2, rhoB2, uB2, vB2, tB2);
  pde.variableInterpreter().evalSA(qB2, ntB2);

  SANS_CHECK_CLOSE(rhoTrue, rhoB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(uTrue, uB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(vTrue, vB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(tTrue, tB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(ntTrue, ntB2, small_tol, close_tol );

  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  Real ntx = -0.23; Real nty = 1.3;
  ArrayQ qx = {rhox, ux, vx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, py, nty};

  ArrayQ Fn = 0;
  bc.fluxNormal( dist, x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn);

  ArrayQ Fn2 = 0;
  bc2.fluxNormal( dist, x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB2, Fn2);

  ArrayQ fx = 0;
  ArrayQ fy = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, z, time, q, fx, fy);
  FnTrue += fx*nx + fy*ny;

  // tangential vector
  const Real tx = -ny;
  const Real ty =  nx;

  Real gmi = gamma - 1.;
  Real H = gas.enthalpy(rho, t) + 0.5*(u*u+v*v);
  Real c = gas.speedofSound(t);
  Real s = log( p /pow(rho,gamma) );

  // normal and tangential velocities
  Real vn = u*nx + v*ny;
  Real vt = u*tx + v*ty;
  Real V2 = u*u+v*v;
  Real V = sqrt(V2);

  // Strong BC's that are imposed
  Real ds  = s  - sSpec;
  Real dH  = H  - HSpec;
  Real dvt = v/sqrt(u*u+v*v) - sin(aSpec);
  Real dnt = nt - ntSpec;

  FnTrue[4] -= -vn*nt*rho/gmi*ds;
  FnTrue[4] -= vn*gamma*nt*rho/(c*c+V2*gmi)*dH;
  FnTrue[4] -=  -(vt*V2*V*rho*gamma*nt)/(u*(c*c+V2*gmi))*dvt;
  FnTrue[4] -=  rho*vn*dnt;

  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn2(4), small_tol, close_tol );
}
#endif   // old file organization


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/BCRANSSA3D_mitState_pattern.txt", true );

  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModel> PDEClass;
  typedef BCParameters< BCRANSSA3DVector<TraitsSizeRANSSA, TraitsModel> > BCParams;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  {
    typedef BCRANSSA3D<BCTypeWall_mitState, BCNavierStokes3D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass>> BCClass;

    BCClass bc(pde);

    bc.dump( 2, output );
  }

  {
    typedef BCRANSSA3D<BCTypeNeumann_mitState, BCNavierStokes3D<BCTypeSymmetry_mitState, PDEClass>> BCClass;

    BCClass bc(pde);

    bc.dump( 2, output );
  }

  {
    typedef BCRANSSA3D< BCTypeFullState_mitState,
                        BCEuler3D< BCTypeFullState_mitState, PDEClass> > BCClass;
    typedef SAnt3D<DensityVelocityPressure3D<Real>>::ParamsType ParamsType;

    PyDict d;
    d[NSVariableType3DParams::params.StateVector.Variables] = SAVariableType3DParams::params.StateVector.PrimitivePressure;
    d[ParamsType::params.rho] = 1.1;
    d[ParamsType::params.u] = 1.3;
    d[ParamsType::params.v] = -0.4;
    d[ParamsType::params.w] =  0.9;
    d[ParamsType::params.p] = 5.45;
    d[ParamsType::params.nt] = 4.6;

    PyDict BCFullState;
    BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
    BCFullState[BCClass::ParamsType::params.Characteristic] = false;
    BCFullState[BCClass::ParamsType::params.StateVector] = d;

    BCClass bc(pde, BCFullState);

    bc.dump( 2, output );
  }

  {
    typedef BCRANSSA3D< BCTypeNeumann_mitState,
                        BCEuler3D< BCTypeOutflowSubsonic_Pressure_mitState, PDEClass> > BCClass;

    const Real pSpec = 2.3;
    BCClass bc(pde, pSpec);

    bc.dump( 2, output );
  }

  {
    typedef BCRANSSA3D<BCTypeDirichlet_mitState, BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDEClass> > BCClass;

    Real TtSpec = 20.5;
    Real PtSpec = 71.4;
    Real aSpec = 2.1;
    Real bSpec = 1.1;
    Real nt = 1.1;
    BCClass bc(pde, nt, TtSpec, PtSpec, aSpec, bSpec);

    bc.dump( 2, output );
  }

#if 0   // not yet implemented in 3D
  {
    typedef BCRANSSA3D<BCTypeInflowSubsonic_sHqt_BN, BCEuler3D<BCTypeInflowSubsonic_sHqt_BN, PDEClass> > BCClass;

    Real sSpec = 2.2;
    Real HSpec = 1.0;
    Real aSpec = 0.1;
    Real nt = 1.1;
    BCClass bc(pde, nt, sSpec, HSpec, aSpec);

    bc.dump( 2, output );
  }

  {
    typedef BCRANSSA3D<BCTypeFunctionInflow_sHqt_BN, BCEuler3D<BCTypeFunctionInflow_sHqt_BN, PDEClass> > BCClass;
    typedef TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel>
    TraitsModelRANSSAClass;
    typedef SolutionFunction_Euler3D_Const<TraitsSizeRANSSA, TraitsModelRANSSAClass> SolutionFunction_EulerClass;

    PyDict gasModelDict;
    gasModelDict[GasModelParams::params.gamma] = 1.4;
    gasModelDict[GasModelParams::params.R] = 0.4;

    PyDict Function;
    Function[BCClass::ParamsType::params.Function.Name] = BCClass::ParamsType::params.Function.Const;
    Function[SolutionFunction_EulerClass::ParamsType::params.gasModel] = gasModelDict;
    Function[SolutionFunction_EulerClass::ParamsType::params.rho] = 1.0;
    Function[SolutionFunction_EulerClass::ParamsType::params.u] = 0.4;
    Function[SolutionFunction_EulerClass::ParamsType::params.v] = 0.1;
    Function[SolutionFunction_EulerClass::ParamsType::params.p] = 1.0;

    PyDict BCIn;
    BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.SolutionInflow_sHqt_BN;
    BCIn[BCClass::ParamsType::params.Function] = Function;
    BCIn["nt"]  = 3; //TODO: not sure about this, was previously just "nt" which gave a Python dictionary error

    BCClass bc(pde, BCIn);
    bc.dump( 2, output);
  }
#endif

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
