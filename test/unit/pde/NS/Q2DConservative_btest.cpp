// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Q2DConservative_btest
// testing of Q2D<QTypeConservative, TraitsSizeEuler> class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/NS/Q2DConservative.h"
#include "pde/NS/TraitsEuler.h"
#include "Surreal/SurrealS.h"

#include <string>
#include <iostream>


//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
template class Q2D<QTypeConservative, TraitsSizeEuler>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Q2DConservative_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test )
{
  const Real tol = 5.0e-12;

  typedef Q2D<QTypeConservative, TraitsSizeEuler> QInterpret;
  BOOST_CHECK( QInterpret::N == 4 );

  typedef QInterpret::ArrayQ<Real> ArrayQ;
  BOOST_CHECK( ArrayQ::M == 4 );

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  // set/eval
  ArrayQ q;
  Real rho, u, v, t, p, E;
  Real rho1, u1, v1, t1;
  Real rho2, u2, v2, t2;

  rho = 1.225;          // kg/m^3 (standard atmosphere)
  t   = 288.15;         // K (standard atmosphere)
  u   = 15.03;          // m/s
  v   =  1.71;
  p   = R*rho*t;
  E   = gas.energy(rho,t) + 0.5*(u*u + v*v);

  Real qDataPrim[4] = {rho, u, v, t};
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY", "Temperature"};
  qInterpret.setFromPrimitive( q, qDataPrim, qNamePrim, 4 );
  qInterpret.eval( q, rho1, u1, v1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );

  qInterpret.evalDensity( q, rho1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );

  qInterpret.evalTemperature( q, t1 );
  BOOST_CHECK_CLOSE( t, t1, tol );

  qInterpret.evalVelocity( q, u1, v1 );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );

  DensityVelocityPressure2D<Real> prim1(rho, u, v, p);
  q = 1;
  qInterpret.setFromPrimitive( q, prim1 );
  qInterpret.eval( q, rho1, u1, v1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );

  DensityVelocityTemperature2D<Real> prim2(rho, u, v, t);
  q = 2;
  qInterpret.setFromPrimitive( q, prim2 );
  qInterpret.eval( q, rho1, u1, v1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );

  Conservative2D<Real> prim3(rho, rho*u, rho*v, rho*E);
  q = 2;
  qInterpret.setFromPrimitive( q, prim3 );
  qInterpret.eval( q, rho1, u1, v1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );


  // copy constructor
  QInterpret qInterpret2(qInterpret);

  qInterpret2.eval( q, rho2, u2, v2, t2 );
  BOOST_CHECK_CLOSE( rho, rho2, tol );
  BOOST_CHECK_CLOSE( u, u2, tol );
  BOOST_CHECK_CLOSE( v, v2, tol );
  BOOST_CHECK_CLOSE( t, t2, tol );


  // gradient
  Real rhox, ux, vx, tx, px;
  Real rhoy, uy, vy, ty, py;
  Real rhox1, ux1, vx1, tx1;
  Real rhoy1, uy1, vy1, ty1;
  Real rhoux, rhovx, rhoEx;
  Real rhouy, rhovy, rhoEy;

  rhox = 0.37, ux = -0.85, vx = 0.09, px = 1.71;
  rhoy = 2.31, uy =  1.65, vy = 0.87, py = 0.29;
  tx = t*(px/p - rhox/rho);
  ty = t*(py/p - rhoy/rho);

  rhoux = rhox*u + rho*ux;
  rhouy = rhoy*u + rho*uy;
  rhovx = rhox*v + rho*vx;
  rhovy = rhoy*v + rho*vy;

  rhoEx = rhox*(gas.Cv()*t + 0.5*(u*u + v*v)) + rho*(gas.Cv()*tx + (u*ux + v*vx));
  rhoEy = rhoy*(gas.Cv()*t + 0.5*(u*u + v*v)) + rho*(gas.Cv()*ty + (u*uy + v*vy));

  Real qxData[4] = {rhox, rhoux, rhovx, rhoEx};
  Real qyData[4] = {rhoy, rhouy, rhovy, rhoEy};
  ArrayQ qx(qxData, 4);
  ArrayQ qy(qyData, 4);

  qInterpret.evalGradient( q, qx, rhox1, ux1, vx1, tx1 );
  qInterpret.evalGradient( q, qy, rhoy1, uy1, vy1, ty1 );
  BOOST_CHECK_CLOSE( rhox, rhox1, tol );
  BOOST_CHECK_CLOSE(   ux,   ux1, tol );
  BOOST_CHECK_CLOSE(   vx,   vx1, tol );
  BOOST_CHECK_CLOSE(   tx,   tx1, tol );
  BOOST_CHECK_CLOSE( rhoy, rhoy1, tol );
  BOOST_CHECK_CLOSE(   uy,   uy1, tol );
  BOOST_CHECK_CLOSE(   vy,   vy1, tol );
  BOOST_CHECK_CLOSE(   ty,   ty1, tol );

  // second gradient

  Real rhoxx = -0.253; Real rhoxy = 0.782; Real rhoyy = 1.08;
  Real uxx = 1.02; Real uxy = -0.95; Real uyy =-0.420;
  Real vxx = 0.99; Real vxy = -0.92; Real vyy =-0.44;
  Real pxx = 2.99; Real pxy = -1.92; Real pyy =-2.44;

  Real txx = 1.0/R*( (2.0*px*rhox)/(rho*rho) + 2.0*p*rhox*rhox/(rho*rho*rho) + pxx/rho - p*rhoxx/(rho*rho) );
  Real txy = 1.0/R*( -rhoy*px/(rho*rho) - py*rhoy/(rho*rho) + 2*p*rhoy*rhox/(rho*rho*rho) + pxy/(rho) - p*rhoxy/(rho*rho) );
  Real tyy = 1.0/R*( -2.0*py*rhoy/(rho*rho) + 2.0*p*rhoy*rhoy/(rho*rho*rho) + pyy/(rho) - p*rhoyy/(rho*rho) );

  Real rhouxx = rhoxx*u + 2.0*rhox*ux + rho*uxx;
  Real rhouxy = rhoxy*u+ rhox*uy + rhoy*ux + rho*uxy;
  Real rhouyy = rhoyy*u + 2.0*rhoy*uy + rho*uyy;

  Real rhovxx = rhoxx*v + 2.0*rhox*vx + rho*vxx;
  Real rhovxy = rhoxy*v+ rhox*vy + rhoy*vx + rho*vxy;
  Real rhovyy = rhoyy*v + 2.0*rhoy*vy + rho*vyy;

  Real rhoExx = rhoxx*(gas.Cv()*t + 0.5*(u*u + v*v)) + rhox*(gas.Cv()*tx + (u*ux + v*vx)) +
                rhox*(gas.Cv()*tx + (u*ux + v*vx)) + rho*(gas.Cv()*txx + (ux*ux + u*uxx + vx*vx + v*vxx));

  Real rhoExy = rhoxy*(gas.Cv()*t + 0.5*(u*u + v*v)) + rhoy*(gas.Cv()*tx + (u*ux + v*vx)) +
                rhox*(gas.Cv()*ty + (u*uy + v*vy)) + rho*(gas.Cv()*txy + (ux*uy + u*uxy + vx*vy + v*vxy));

  Real rhoEyy = rhoyy*(gas.Cv()*t + 0.5*(u*u + v*v)) + rhoy*(gas.Cv()*ty + (u*uy + v*vy)) +
                rhoy*(gas.Cv()*ty + (u*uy + v*vy)) + rho*(gas.Cv()*tyy + (uy*uy + u*uyy + vy*vy + v*vyy));


  ArrayQ qxx = {rhoxx, rhouxx, rhovxx, rhoExx};
  ArrayQ qxy = {rhoxy, rhouxy, rhovxy, rhoExy};
  ArrayQ qyy = {rhoyy, rhouyy, rhovyy, rhoEyy};

  Real rhoxx2, rhoxy2, rhoyy2 = 0;
  Real uxx2, uxy2, uyy2 = 0;
  Real vxx2, vxy2, vyy2 = 0;
  Real txx2, txy2, tyy2 = 0;


  qInterpret.evalSecondGradient( q, qx, qy,
                                 qxx, qxy, qyy,
                                 rhoxx2, rhoxy2, rhoyy2,
                                 uxx2, uxy2, uyy2,
                                 vxx2, vxy2, vyy2,
                                 txx2, txy2, tyy2);

  BOOST_CHECK_CLOSE( rhoxx, rhoxx2, tol );
  BOOST_CHECK_CLOSE( rhoxy, rhoxy2, tol );
  BOOST_CHECK_CLOSE( rhoyy, rhoyy2, tol );
  BOOST_CHECK_CLOSE(   uxx,   uxx2, tol );
  BOOST_CHECK_CLOSE(   uxy,   uxy2, tol );
  BOOST_CHECK_CLOSE(   uyy,   uyy2, tol );
  BOOST_CHECK_CLOSE(   vxx,   vxx2, tol );
  BOOST_CHECK_CLOSE(   vxy,   vxy2, tol );
  BOOST_CHECK_CLOSE(   vyy,   vyy2, tol );
  BOOST_CHECK_CLOSE(   txx,   txx2, tol );
  BOOST_CHECK_CLOSE(   txy,   txy2, tol );
  BOOST_CHECK_CLOSE(   tyy,   tyy2, tol );



}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( evalJacobian )
{
  typedef Q2D<QTypeConservative, TraitsSizeEuler> QInterpret;

  typedef QInterpret::ArrayQ< Real > ArrayQ;
  typedef QInterpret::ArrayQ< SurrealS<QInterpret::N> > ArrayQSurreal;

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  ArrayQ q, rho_q, u_q, v_q, t_q;
  ArrayQSurreal qSurreal;
  SurrealS<QInterpret::N> rho, u, v, t;

  qInterpret.setFromPrimitive( q, DensityVelocityTemperature2D<Real>(1.225, 15.03, 1.71, 288.15) );

  qInterpret.evalJacobian(q, rho_q, u_q, v_q, t_q);

  qSurreal = q;

  qSurreal[0].deriv(0) = 1;
  qSurreal[1].deriv(1) = 1;
  qSurreal[2].deriv(2) = 1;
  qSurreal[3].deriv(3) = 1;

  qInterpret.eval(qSurreal, rho, u, v, t);

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  SANS_CHECK_CLOSE( rho.deriv(0), rho_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(1), rho_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(2), rho_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(3), rho_q[3], small_tol, close_tol )

  SANS_CHECK_CLOSE( u.deriv(0), u_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(1), u_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(2), u_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(3), u_q[3], small_tol, close_tol )

  SANS_CHECK_CLOSE( v.deriv(0), v_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(1), v_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(2), v_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(3), v_q[3], small_tol, close_tol )

  SANS_CHECK_CLOSE( t.deriv(0), t_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(1), t_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(2), t_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(3), t_q[3], small_tol, close_tol )
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef Q2D<QTypeConservative, TraitsSizeEuler> QInterpret;
  typedef QInterpret::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  ArrayQ q = 0;
  q(0) =  1;
  q(3) =  1;
  BOOST_CHECK( qInterpret.isValidState(q) );

  q(0) =  1;
  q(3) = -1;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  q(0) = -1;
  q(3) =  1;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  q(0) = -1;
  q(3) = -1;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  q(0) =  1;
  q(3) =  0;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  q(0) =  0;
  q(3) =  1;
  BOOST_CHECK( qInterpret.isValidState(q) == false );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/Q2DConservative_pattern.txt", true );

  typedef Q2D<QTypeConservative, TraitsSizeEuler> QInterpret;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  qInterpret.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
