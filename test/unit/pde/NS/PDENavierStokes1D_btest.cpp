// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDENavierStokes1D_btest
//
// test of 1-D compressible Navier-Stokes PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "Topology/Dimension.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/Q1DPrimitiveSurrogate.h"
#include "pde/NS/PDENavierStokes1D.h"

//Explicitly instantiate the class so coverage information is correct
namespace SANS
{
template class TraitsSizeNavierStokes<PhysD1>;
template class TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel>;
template class TraitsModelNavierStokes<QTypePrimitiveSurrogate, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel>;
template class PDENavierStokes1D< TraitsSizeNavierStokes,
                                  TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> >;
template class PDENavierStokes1D< TraitsSizeNavierStokes,
                                  TraitsModelNavierStokes<QTypePrimitiveSurrogate, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> >;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDENavierStokes1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test_rhoPressure )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes1D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( PDEClass::D == 1 );
  BOOST_CHECK( PDEClass::N == 3 );
  BOOST_CHECK( ArrayQ::M == 3 );
  BOOST_CHECK( MatrixQ::M == 3 );
  BOOST_CHECK( MatrixQ::N == 3 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test_surrogate )
{
  typedef QTypePrimitiveSurrogate QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes1D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( PDEClass::D == 1 );
  BOOST_CHECK( PDEClass::N == 3 );
  BOOST_CHECK( ArrayQ::M == 3 );
  BOOST_CHECK( MatrixQ::M == 3 );
  BOOST_CHECK( MatrixQ::N == 3 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux_rhoPressure )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes1D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, PDEClass::Euler_ResidInterp_Raw);

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 3 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // function tests

  Real x, time;
  Real rho, u, t, p, e0, h0;

  x = 0;
  time = 0;   // not actually used in functions
  rho = 1.225;              // kg/m^3
  u = 69.784;               // m/s
  t = 288.15;               // K
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u);
  h0 = Cp*t + 0.5*(u*u);

  // set
  Real qDataPrim[3] = {rho, u, t};
  string qNamePrim[3] = {"Density", "VelocityX", "Temperature"};
  Real qData[3] = {rho, u, p};
  ArrayQ qTrue(qData, 3);
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );

  // unsteady conservative flux
  Real uData[3] = {rho, rho*u, rho*e0};
  ArrayQ uTrue(uData, 3);
  ArrayQ uCons = 0;
  pde.masterState( x, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // advective flux
  Real fData[3] = {rho*u, rho*u*u + p, rho*u*h0};
  ArrayQ fTrue(fData, 3);
  ArrayQ f;
  f = 0;
  pde.fluxAdvective( x, time, q, f);
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux_surrogate )
{
  typedef QTypePrimitiveSurrogate QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes1D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, PDEClass::Euler_ResidInterp_Raw);

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 3 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // function tests

  Real x, time;
  Real rho, u, t, p, e0, h0;

  x = 0;   // not actually used in functions
  time = 0;
  rho = 1.225;              // kg/m^3
  u = 69.784;               // m/s
  t = 288.15;               // K
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u);
  h0 = Cp*t + 0.5*(u*u);

  // set
  Real qDataPrim[3] = {rho, u, t};
  string qNamePrim[3] = {"Density", "VelocityX", "Temperature"};
  Real qData[3] = {rho, u, t};
  ArrayQ qTrue(qData, 3);
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );

  // unsteady conservative flux
  Real uData[3] = {rho, rho*u, rho*e0};
  ArrayQ uTrue(uData, 3);
  ArrayQ uCons = 0;
  pde.masterState( x, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // unsteady conservative flux
  ArrayQ ftTrue(uData, 3);
  ArrayQ ft = 0;
  pde.fluxAdvectiveTime( x, time, q, ft );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );

  pde.fluxAdvectiveTime( x, time, q, ft );
  BOOST_CHECK_CLOSE( 2*ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(2), ft(2), tol );

  // advective flux
  Real fData[3] = {rho*u, rho*u*u + p, rho*u*h0};
  ArrayQ fTrue(fData, 3);
  ArrayQ f;
  f = 0;
  pde.fluxAdvective( x, time, q, f);
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );

  pde.fluxAdvective( x, time, q, f);
  BOOST_CHECK_CLOSE( 2*fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(2), f(2), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxRoe_rhoPressure )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes1D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R     = 0.4;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, PDEClass::Euler_ResidInterp_Raw);

  // Roe flux function test

  Real x, time;
  Real nx;
  Real rhoL, uL, tL, pL, h0L;
  Real rhoR, uR, tR, pR, h0R;

  x = time = 0;   // not actually used in functions
  nx = 1.0;
  rhoL = 1.034; uL = 3.26; tL = 5.78;
  rhoR = 0.973; uR = 1.79; tR = 6.13;
  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL);
  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR);

  // set
  ArrayQ qL = pde.setDOFFrom( DensityVelocityTemperature1D<Real>(rhoL, uL, tL) );
  ArrayQ qR = pde.setDOFFrom( DensityVelocityTemperature1D<Real>(rhoR, uR, tR) );

  // advective normal flux (average)
  Real fL[3] = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*h0L};
  Real fR[3] = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*h0R};

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < 3; k++)
    fnTrue[k] = 0.5*(nx*(fL[k] + fR[k]));
#if 1
  // Exact values for Roe scheme
  fnTrue[0] -= -0.63318276027140017468491006399040;
  fnTrue[1] -=  -3.8160625416529803007039694937525;
  fnTrue[2] -=  -12.431949286034470439767789109435;
#endif

  ArrayQ fn;
  fn = 0;
  pde.fluxAdvectiveUpwind( x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );

  pde.fluxAdvectiveUpwind( x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(2), fn(2), tol );


  //Call these simply to make sure coverage is high
  ArrayQ qxL, qxR, sourceL, sourceR;
  Real dummy = 0;
  pde.sourceTrace( dummy, dummy, time, qL, qxL, qR, qxR, sourceL, sourceR );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxRoe_surrogate )
{
  typedef QTypePrimitiveSurrogate QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes1D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R     = 0.4;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, PDEClass::Euler_ResidInterp_Raw);

  // Roe flux function test

  Real x, time;
  Real nx;
  Real rhoL, uL, tL, pL, h0L;
  Real rhoR, uR, tR, pR, h0R;

  x = time = 0;   // not actually used in functions
  nx = 1.0;
  rhoL = 1.034; uL = 3.26; tL = 5.78;
  rhoR = 0.973; uR = 1.79; tR = 6.13;
  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL);
  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR);

  // set
  ArrayQ qL = pde.setDOFFrom( DensityVelocityTemperature1D<Real>(rhoL, uL, tL) );
  ArrayQ qR = pde.setDOFFrom( DensityVelocityTemperature1D<Real>(rhoR, uR, tR) );

  // advective normal flux (average)
  Real fL[3] = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*h0L};
  Real fR[3] = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*h0R};

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < 3; k++)
    fnTrue[k] = 0.5*(nx*(fL[k] + fR[k]));
#if 1
  // Exact values for Roe scheme
  fnTrue[0] -= -0.63318276027140017468491006399040;
  fnTrue[1] -=  -3.8160625416529803007039694937525;
  fnTrue[2] -=  -12.431949286034470439767789109435;
#endif

  ArrayQ fn;
  fn = 0;
  pde.fluxAdvectiveUpwind( x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );

  pde.fluxAdvectiveUpwind( x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(2), fn(2), tol );

  //Call these simply to make sure coverage is high
  ArrayQ qxL, qxR, sourceL, sourceR;
  Real dummy = 0;
  pde.sourceTrace( dummy, dummy, time, qL, qxL, qR, qxR, sourceL, sourceR );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( viscous_rhoPressure )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes1D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, PDEClass::Euler_ResidInterp_Raw);

  Real x, time;
  Real rho, u, t, p, e0;
  Real mu, lambda, k;
  Real tauxx;

  x = 0;   // not actually used in functions
  time = 0;
  rho = 1.225;              // kg/m^3
  u = 69.784;               // m/s
  t = 288.15;               // K
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u);

  mu = visc.viscosity(t);
  k  = tcond.conductivity(mu);
  lambda = -(2./3.)*mu;

  // gradient
  Real rhox, ux, tx, px;

  rhox = 0.37, ux = -0.85, px = 1.71;
  tx = t*(px/p - rhox/rho);

  // viscous shear stress
  tauxx = mu*(2*ux   ) + lambda*(ux) ;

  // set
  Real qDataPrim[3] = {rho, u, t};
  string qNamePrim[3] = {"Density", "VelocityX", "Temperature"};
  Real qData[3] = {rho, u, p};
  ArrayQ qTrue(qData, 3);
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );

  Real qxData[3] = {rhox, ux, px};
  ArrayQ qx(qxData, 3);

  // viscous flux
  Real fData[3] = {0, -tauxx, -(k*tx + u*tauxx)};
  ArrayQ fTrue(fData, 3);
  ArrayQ f;
  f = 0;
  pde.fluxViscous( x, time, q, qx, f );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );

  pde.fluxViscous( x, time, q, qx, f );
  BOOST_CHECK_CLOSE( 2*fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(2), f(2), tol );

  // viscous diffusion matrix
  Real kxxData[9] = {
    0, 0, 0,
    -u*(2*mu + lambda)/rho, (2*mu + lambda)/rho, 0,
    -u*u*((2*mu + lambda) - k/Cv)/rho - e0*k/(Cv*rho),
       u*((2*mu + lambda) - k/Cv)/rho,    k/(Cv*rho) };
  MatrixQ kxxTrue(kxxData, 9);
  MatrixQ kxx=0;
  pde.diffusionViscous( x, time, q, qx, kxx );

  for (int i = 0; i < 3; i++)
  {
    for (int j = 0; j < 3; j++)
    {
      BOOST_CHECK_CLOSE( kxxTrue(i,j), kxx(i,j), (kxxTrue(i,j) == 0) ? tol : tol );
    }
  }


  //Call these simply to make sure coverage is high
  ArrayQ source = 0;
  Real dummy = 0;
  pde.source( dummy, time, q, qx, source );

  ArrayQ qp = 0, qpx = 0;
  ArrayQ source_split = 0;
  pde.source( dummy, time, q, qp, qx, qpx, source_split );
  BOOST_CHECK_CLOSE( source_split(0), source(0), (source_split(0) == 0) ? tol : tol );
  BOOST_CHECK_CLOSE( source_split(1), source(1), (source_split(1) == 0) ? tol : tol );
  BOOST_CHECK_CLOSE( source_split(2), source(2), (source_split(2) == 0) ? tol : tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( viscous_surrogate )
{
  typedef QTypePrimitiveSurrogate QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes1D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, PDEClass::Euler_ResidInterp_Raw);

  Real x, time;
  Real rho, u, t, p, e0;
  Real mu, lambda, k;
  Real tauxx;

  x = 0;   // not actually used in functions
  time = 0;
  rho = 1.225;              // kg/m^3
  u = 69.784;               // m/s
  t = 288.15;               // K
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u);

  mu = visc.viscosity(t);
  k  = tcond.conductivity(mu);
  lambda = -(2./3.)*mu;

  // gradient
  Real rhox, ux, tx, px;

  rhox = 0.37, ux = -0.85, px = 1.71;
  tx = t*(px/p - rhox/rho);

  // viscous shear stress
  tauxx = mu*(2*ux   ) + lambda*(ux) ;

  // set
  Real qDataPrim[3] = {rho, u, t};
  string qNamePrim[3] = {"Density", "VelocityX", "Temperature"};
  Real qData[3] = {rho, u, t};
  ArrayQ qTrue(qData, 3);
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );

  Real qxData[3] = {rhox, ux, tx};
  ArrayQ qx(qxData, 3);

  // viscous flux
  Real fData[3] = {0, -tauxx, -(k*tx + u*tauxx)};
  ArrayQ fTrue(fData, 3);
  ArrayQ f;
  f = 0;
  pde.fluxViscous( x, time, q, qx, f );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );

  pde.fluxViscous( x, time, q, qx, f );
  BOOST_CHECK_CLOSE( 2*fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(2), f(2), tol );

  // viscous diffusion matrix
  Real kxxData[9] = {
    0, 0, 0,
    -u*(2*mu + lambda)/rho, (2*mu + lambda)/rho, 0,
    -u*u*((2*mu + lambda) - k/Cv)/rho - e0*k/(Cv*rho),
       u*((2*mu + lambda) - k/Cv)/rho,    k/(Cv*rho) };
  MatrixQ kxxTrue(kxxData, 9);
  MatrixQ kxx=0;
  pde.diffusionViscous( x, time, q, qx, kxx );

  for (int i = 0; i < 3; i++)
  {
    for (int j = 0; j < 3; j++)
    {
      BOOST_CHECK_CLOSE( kxxTrue(i,j), kxx(i,j), (kxxTrue(i,j) == 0) ? tol : tol );
    }
  }


  //Call these simply to make sure coverage is high
  ArrayQ source;
  Real dummy = 0;
  pde.source( dummy, time, q, qx, source );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic_rhoPressure )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes1D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;
  const Real tSutherland = 0.5;
  const Real tRef = 1.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, PDEClass::Euler_ResidInterp_Raw);

  Real x, dx, time;
  Real rho, u, t, c;
  Real speed, speedTrue;

  x = 0;   // not actually used in functions
  time = 0;
  dx = 0.57;
  rho = 1.137; u = 0.784; t = 0.987;
  c = sqrt(gamma*R*t);
  speedTrue = fabs(dx*u)/sqrt(dx*dx) + c;

  Real qDataPrim[3] = {rho, u, t};
  string qNamePrim[3] = {"Density", "VelocityX", "Temperature"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );

  pde.speedCharacteristic( x, time, dx, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  pde.speedCharacteristic( x, time, q, speed );
  BOOST_CHECK_CLOSE( fabs(u) + c, speed, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic_surrogate )
{
  typedef QTypePrimitiveSurrogate QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes1D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;
  const Real tSutherland = 0.5;
  const Real tRef = 1.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, PDEClass::Euler_ResidInterp_Raw);

  Real x, dx, time;
  Real rho, u, t, c;
  Real speed, speedTrue;

  x = 0;   // not actually used in functions
  time = 0;
  dx = 0.57;
  rho = 1.137; u = 0.784; t = 0.987;
  c = sqrt(gamma*R*t);
  speedTrue = fabs(dx*u)/sqrt(dx*dx) + c;

  Real qDataPrim[3] = {rho, u, t};
  string qNamePrim[3] = {"Density", "VelocityX", "Temperature"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );

  pde.speedCharacteristic( x, time, dx, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  pde.speedCharacteristic( x, time, q, speed );
  BOOST_CHECK_CLOSE( fabs(u) + c, speed, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState_rhoPressure )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes1D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;
  const Real tSutherland = 0.5;
  const Real tRef = 1.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, PDEClass::Euler_ResidInterp_Raw);

  ArrayQ q;
  q(0) =  1;
  q(2) =  1;
  BOOST_CHECK( pde.isValidState(q) );

  q(0) =  1;
  q(2) = -1;
  BOOST_CHECK( pde.isValidState(q) == false );

  q(0) = -1;
  q(2) =  1;
  BOOST_CHECK( pde.isValidState(q) == false );

  q(0) = -1;
  q(2) = -1;
  BOOST_CHECK( pde.isValidState(q) == false );

  q(0) =  1;
  q(2) =  0;
  BOOST_CHECK( pde.isValidState(q) == false );

  q(0) =  0;
  q(2) =  1;
  BOOST_CHECK( pde.isValidState(q) == false );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState_surrogate )
{
  typedef QTypePrimitiveSurrogate QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes1D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;
  const Real tSutherland = 0.5;
  const Real tRef = 1.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, PDEClass::Euler_ResidInterp_Raw);

  ArrayQ q;
  q(0) =  1;
  q(2) =  1;
  BOOST_CHECK( pde.isValidState(q) );

  q(0) =  1;
  q(2) = -1;
  BOOST_CHECK( pde.isValidState(q) );

  q(0) = -1;
  q(2) =  1;
  BOOST_CHECK( pde.isValidState(q) );

  q(0) = -1;
  q(2) = -1;
  BOOST_CHECK( pde.isValidState(q) );

  q(0) =  1;
  q(2) =  0;
  BOOST_CHECK( pde.isValidState(q) );

  q(0) =  0;
  q(2) =  1;
  BOOST_CHECK( pde.isValidState(q) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO_rhoPressure )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/PDENavierStokes1D_pattern.txt", true );

  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes1D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, PDEClass::Euler_ResidInterp_Raw);

  pde.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO_surrogate )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/PDENavierStokes1DSurrogate_pattern.txt", true );

  typedef QTypePrimitiveSurrogate QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes1D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, PDEClass::Euler_ResidInterp_Raw);

  pde.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
