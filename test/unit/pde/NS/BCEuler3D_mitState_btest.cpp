// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCEuler3D_mitState_btest
//
// test of 3-D compressible Euler BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/PDEEuler3D.h"
#include "pde/NS/BCEuler3D.h"

#include "pde/BCParameters.h"
#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
template class BCEuler3D< BCTypeFullState_mitState,
  PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler3D< BCTypeSymmetry_mitState,
  PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler3D< BCTypeReflect_mitState,
  PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler3D< BCTypeOutflowSubsonic_Pressure_mitState,
  PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
//template class BCEuler3D< BCTypeSymmetry,
//  PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
//template class BCEuler3D< BCTypeInflowSupersonic,
//  PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
//template class BCEuler3D< BCTypeInflowSubsonic_sHqt,
//  PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
//template class BCEuler3D< BCTypeInflowSubsonic_sHqt_Profile<
//  SolutionFunction_Euler3D_Wake<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >,
//  PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
//template class BCEuler3D< BCTypeOutflowSubsonic_Pressure,
//  PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
//template class BCEuler3D < BCTypeFunction_mitState,
//  PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
//template class BCEuler3D < BCTypeFunctionInflow_sHqt_mitState,
//  PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
//template class BCEuler3D < BCTypeFunctionInflow_sHqt_BN,
//  PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler3D< BCTypeInflowSubsonic_PtTta_mitState,
  PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
//template class BCEuler3D< BCTypeInflowSubsonic_sHqt_mitState,
//  PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
//template class BCEuler3D< BCTypeInflowSubsonic_sHqt_BN,
//  PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
template class BCEuler3D< BCTypeOutflowSubsonic_Pressure_BN,
  PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;

// Instantiate BCParamaters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters< BCEuler3DVector<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
}

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( BCEuler3D_mitState_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test_BCTypeFullState_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler3D<BCTypeFullState_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  typedef BCParameters< BCEuler3DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 5 );
  BOOST_CHECK( BCClass::NBC == 5 );
  BOOST_CHECK( ArrayQ::M == 5 );
  BOOST_CHECK( MatrixQ::M == 5 );
  BOOST_CHECK( MatrixQ::N == 5 );

  // ctors

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  bool upwind = true;
  BCClass bc1(pde, DensityVelocityPressure3D<Real>(1.1, 1.3, -0.4, 2.56, 5.45), upwind);

  BOOST_CHECK( bc1.D == 3 );
  BOOST_CHECK( bc1.N == 5 );
  BOOST_CHECK( bc1.NBC == 5 );
  BOOST_CHECK( bc1.useUpwind() == true );

  PyDict d;
  d[NSVariableType3DParams::params.StateVector.Variables] = NSVariableType3DParams::params.StateVector.PrimitivePressure;
  d[DensityVelocityPressure3DParams::params.rho] = 1.1;
  d[DensityVelocityPressure3DParams::params.u] = 1.3;
  d[DensityVelocityPressure3DParams::params.v] = -0.4;
  d[DensityVelocityPressure3DParams::params.w] = 2.56;
  d[DensityVelocityPressure3DParams::params.p] = 5.45;

  PyDict BCFullState;
  BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFullState[BCClass::ParamsType::params.Characteristic] = false;
  BCFullState[BCClass::ParamsType::params.StateVector] = d;

  PyDict BCList;
  BCList["BC1"] = BCFullState;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCFullState);

  BOOST_CHECK( bc2.D == 3 );
  BOOST_CHECK( bc2.N == 5 );
  BOOST_CHECK( bc2.NBC == 5 );
  BOOST_CHECK( bc2.useUpwind() == false );

  // functions

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  Real x, y, z, time;
  Real nx, ny, nz;
  Real rho, u, v, w, t, p;

  x = y = z = time = 0;   // not actually used in functions
  nx = -0.856;  ny = 0.48;  nz = 0.192;    // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; w = 0.456; t = 0.987;
  p  = R*rho*t;

  ArrayQ q;
  DensityVelocityPressure3D<Real> qdata(rho, u, v, w, p);
  pde.setDOFFrom( q, qdata );

  BCClass bcT(pde, qdata, true);
  BCClass bcF(pde, qdata, false);

  BOOST_CHECK( bcT.D == 3 );
  BOOST_CHECK( bcT.N == 5 );
  BOOST_CHECK( bcT.NBC == 5 );
  BOOST_CHECK( bcT.useUpwind() == true );
  BOOST_CHECK( bcF.useUpwind() == false );

  ArrayQ qI = 0;
  ArrayQ qB;

  bcT.state(x, y, z, time, nx, ny, nz, qI, qB);

  Real rhoB, uB, vB, wB, tB;
  pde.variableInterpreter().eval(qB, rhoB, uB, vB, wB, tB);

  SANS_CHECK_CLOSE( rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE( u, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE( v, vB, small_tol, close_tol );
  SANS_CHECK_CLOSE( w, wB, small_tol, close_tol );
  SANS_CHECK_CLOSE( t, tB, small_tol, close_tol );

  qI = pde.setDOFFrom( DensityVelocityPressure3D<Real>(rhoB, uB, vB, wB, tB) );
  BOOST_CHECK( bcT.isValidState(nx, ny, nz, qI) == true );
  BOOST_CHECK( bcF.isValidState(nx, ny, nz, qI) == true );

  qI = pde.setDOFFrom( DensityVelocityPressure3D<Real>(rhoB, -uB, -vB, -wB, tB) );
  BOOST_CHECK( bcT.isValidState(nx, ny, nz, qI) == true );
  BOOST_CHECK( bcF.isValidState(nx, ny, nz, qI) == false );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test_BCTypeSymmetry_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler3D<BCTypeSymmetry_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  typedef BCParameters< BCEuler3DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 5 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( ArrayQ::M == 5 );
  BOOST_CHECK( MatrixQ::M == 5 );
  BOOST_CHECK( MatrixQ::N == 5 );

  // ctors

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  BCClass bc1(pde);

  BOOST_CHECK( bc1.D == 3 );
  BOOST_CHECK( bc1.N == 5 );
  BOOST_CHECK( bc1.NBC == 1 );

  // Pydict constructor
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCList;
  BCList["BC1"] = BCSymmetry;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCSymmetry);

  BOOST_CHECK( bc2.D == 3 );
  BOOST_CHECK( bc2.N == 5 );
  BOOST_CHECK( bc2.NBC == 1 );

  // functions

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  BCClass bc(pde);

  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 5 );
  BOOST_CHECK( bc.NBC == 1 );

  Real x, y, z, time;
  Real nx, ny, nz;
  Real rho, u, v, w, t, p, qn;

  x = y = z = time = 0;   // not actually used in functions
  nx = -0.856;  ny = 0.48;  nz = 0.192;    // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; w = 0.456; t = 0.987;
  p  = R*rho*t;
  qn = nx*u + ny*v + nz*w;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  Real uBTrue = u - qn*nx;
  Real vBTrue = v - qn*ny;
  Real wBTrue = w - qn*nz;

  Real pBTrue = p;

  ArrayQ qI;
  pde.setDOFFrom( qI, DensityVelocityPressure3D<Real>(rho, u, v, w, p) );

  ArrayQ qB;
  bc.state( x, y, z, time, nx, ny, nz, qI, qB );

  Real rhoB, uB, vB, wB, tB;
  pde.variableInterpreter().eval( qB, rhoB, uB, vB, wB, tB );
  Real pB = gas.pressure(rhoB, tB);

  SANS_CHECK_CLOSE(    rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE( uBTrue,   uB, small_tol, close_tol );
  SANS_CHECK_CLOSE( vBTrue,   vB, small_tol, close_tol );
  SANS_CHECK_CLOSE( wBTrue,   wB, small_tol, close_tol );
  SANS_CHECK_CLOSE( pBTrue,   pB, small_tol, close_tol );

  bc.state( param, x, y, z, time, nx, ny, nz, qI, qB );

  Real rhoB2, uB2, vB2, wB2, tB2;
  pde.variableInterpreter().eval( qB, rhoB2, uB2, vB2, wB2, tB2 );
  Real pB2 = gas.pressure(rhoB2, tB2);

  SANS_CHECK_CLOSE(   rhoB, rhoB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     uB,   uB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     vB,   vB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     wB,   wB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     pB,   pB2, small_tol, close_tol );

  BOOST_CHECK( bc.isValidState(nx, ny, nz, qI) == true );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test_BCTypeReflect_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler3D<BCTypeReflect_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  typedef BCParameters< BCEuler3DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 5 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( ArrayQ::M == 5 );
  BOOST_CHECK( MatrixQ::M == 5 );
  BOOST_CHECK( MatrixQ::N == 5 );

  // ctors

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  BCClass bc1(pde);

  BOOST_CHECK( bc1.D == 3 );
  BOOST_CHECK( bc1.N == 5 );
  BOOST_CHECK( bc1.NBC == 1 );
  BOOST_CHECK( bc1.hasFluxViscous() == false );

  // Pydict constructor
  PyDict BCReflect;
  BCReflect[BCParams::params.BC.BCType] = BCParams::params.BC.Reflect_mitState;

  PyDict BCList;
  BCList["BC1"] = BCReflect;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCReflect);

  BOOST_CHECK( bc2.D == 3 );
  BOOST_CHECK( bc2.N == 5 );
  BOOST_CHECK( bc2.NBC == 1 );
  BOOST_CHECK( bc2.hasFluxViscous() == false );

  // functions

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  BCClass bc(pde);

  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 5 );
  BOOST_CHECK( bc.NBC == 1 );

  Real x, y, z, time;
  Real nx, ny, nz;
  Real rho, u, v, w, t, p, qn;

  x = y = z = time = 0;   // not actually used in functions
  nx = -0.856;  ny = 0.48;  nz = 0.192;    // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; w = 0.456; t = 0.987;
  p  = R*rho*t;
  qn = nx*u + ny*v + nz*w;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  Real rhox = 0.01, rhoy = -0.025, rhoz = -0.344;
  Real ux = 0.049, uy =  0.072, uz =  1.431;
  Real vx = 0.143, vy = -0.024, vz = -0.987;
  Real wx = 0.659, wy =  0.349, wz =  0.776;
  Real px = 0.002, py = -4.231, pz = -0.451;

  ArrayQ qIx = {rhox, ux, vx, wx, px};
  ArrayQ qIy = {rhoy, uy, vy, wy, py};
  ArrayQ qIz = {rhoz, uz, vz, wz, pz};


  Real uBTrue = u - 2*qn*nx;
  Real vBTrue = v - 2*qn*ny;
  Real wBTrue = w - 2*qn*nz;

  Real pBTrue = p;

  ArrayQ qI;
  pde.setDOFFrom( qI, DensityVelocityPressure3D<Real>(rho, u, v, w, p) );

  ArrayQ qB;
  bc.state( x, y, z, time, nx, ny, nz, qI, qB );

  Real rhoB, uB, vB, wB, tB;
  pde.variableInterpreter().eval( qB, rhoB, uB, vB, wB, tB );
  Real pB = gas.pressure(rhoB, tB);

  SANS_CHECK_CLOSE(    rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE( uBTrue,   uB, small_tol, close_tol );
  SANS_CHECK_CLOSE( vBTrue,   vB, small_tol, close_tol );
  SANS_CHECK_CLOSE( wBTrue,   wB, small_tol, close_tol );
  SANS_CHECK_CLOSE( pBTrue,   pB, small_tol, close_tol );

  bc.state( param, x, y, z, time, nx, ny, nz, qI, qB );

  Real rhoB2, uB2, vB2, wB2, tB2;
  pde.variableInterpreter().eval( qB, rhoB2, uB2, vB2, wB2, tB2 );
  Real pB2 = gas.pressure(rhoB2, tB2);

  SANS_CHECK_CLOSE(   rhoB, rhoB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     uB,   uB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     vB,   vB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     wB,   wB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     pB,   pB2, small_tol, close_tol );

  // check the normal flux
  ArrayQ Fn = 0, FnTrue = 0;
  bc.fluxNormal( x, y, z, time, nx, ny, nz, qI, qIx, qIy, qIz, qB, Fn);

  pde.fluxAdvectiveUpwind( x, y, z, time, qI, qB, nx, ny, nz, FnTrue );

  SANS_CHECK_CLOSE( FnTrue[0], Fn[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue[1], Fn[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue[2], Fn[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue[3], Fn[3], small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue[4], Fn[4], small_tol, close_tol );

  BOOST_CHECK( bc.isValidState(nx, ny, nz, qI) == true );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test_BCTypeOutflowSubsonic_Pressure_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  typedef BCParameters< BCEuler3DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 5 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( ArrayQ::M == 5 );

  // ctors

  const Real gamma = 1.4;
  const Real R = 287.04;        // J/(kg K)
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  const Real pSpec = 101325;
  BCClass bc1(pde, pSpec);

  BOOST_CHECK( bc1.D == 3 );
  BOOST_CHECK( bc1.N == 5 );
  BOOST_CHECK( bc1.NBC == 1 );
  BOOST_CHECK( bc1.hasFluxViscous() == false );

  //Pydict constructor
  PyDict BCOutflowSubsonic;
  BCOutflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOutflowSubsonic[BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pSpec;

  PyDict BCList;
  BCList["BC1"] = BCOutflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc(pde, BCOutflowSubsonic);

  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 5 );
  BOOST_CHECK( bc.NBC == 1 );
  BOOST_CHECK( bc.hasFluxViscous() == false );

  // functions

  const Real small_tol = 3.e-12;
  const Real close_tol = 3.e-12;

  Real x, y, z, time;
  Real nx, ny, nz;
  Real rho, u, v, w, t, p, c;

  // Test conditions: M = 0.3
  x = y = z = time = 0;   // not actually used in functions
  nx = 0.856;  ny = 0.48;  nz = 0.192;    // Note: magnitude = 1
  rho = 1.137;
  t = 300.1;
  p = R*rho*t;
  c = sqrt(gamma*R*t);
  ParamType param(0.1, 0.0); // grid spacing and jump values

  Real M = 0.3;
  Real alpha = 0.1;
  Real beta  = 0.4;
  u = M*c*cos(alpha)*cos(beta);
  v = M*c*           sin(beta);
  w = M*c*sin(alpha)*cos(beta);

  ArrayQ q, qB, qB2, qBTrue;
  DensityVelocityPressure3D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.VelocityZ = w;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  bc.state(x, y, z, time, nx, ny, nz, q, qB);
  bc.state(param, x, y, z, time, nx, ny, nz, q, qB2);

  qBTrue[0] = 1.1649158602997538;
  qBTrue[1] = 88.25003812727218;
  qBTrue[2] = 36.517086088327524;
  qBTrue[3] = 7.958543517473888;
  qBTrue[4] = 101325;

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(4), qB(4), small_tol, close_tol );

  SANS_CHECK_CLOSE( qB(0), qB2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qB(1), qB2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qB(2), qB2(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qB(3), qB2(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qB(4), qB2(4), small_tol, close_tol );


  Real rhox = 0.01, rhoy = -0.025, rhoz = -0.344;
  Real ux = 0.049, uy =  0.072, uz =  1.431;
  Real vx = 0.143, vy = -0.024, vz = -0.987;
  Real wx = 0.659, wy =  0.349, wz =  0.776;
  Real px = 0.002, py = -4.231, pz = -0.451;

  ArrayQ qx = {rhox, ux, vx, wx, px};
  ArrayQ qy = {rhoy, uy, vy, wy, py};
  ArrayQ qz = {rhoz, uz, vz, wz, pz};

  ArrayQ Fn = 0, Fn2 = 0;
  bc.fluxNormal( x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn);
  bc.fluxNormal( param, x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn2);

  ArrayQ fx = 0, fy = 0, fz = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, z, time, qB, fx, fy, fz);
  FnTrue += fx*nx + fy*ny + fz*nz;

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );

  SANS_CHECK_CLOSE( Fn(0), Fn2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(1), Fn2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(2), Fn2(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(3), Fn2(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(4), Fn2(4), small_tol, close_tol );

  // inflow/outflow
  BOOST_CHECK( bc.isValidState( nx, ny, nz, q) == true );
  BOOST_CHECK( bc.isValidState(-nx, ny, nz, q) == false );

  // supersonic
  qdata.Pressure *= 0.01;
  pde.setDOFFrom( q, qdata );
  BOOST_CHECK( bc.isValidState( nx, ny, nz, q) == false );
  qdata.Pressure *= 100;

  // negative density
  qdata.Density *= -1;
  pde.setDOFFrom( q, qdata );
  BOOST_CHECK( bc.isValidState( nx, ny, nz, q) == false );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_SubsonicInflow_PtTta_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  typedef BCParameters< BCEuler3DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 5 );
  BOOST_CHECK( BCClass::NBC == 4 );
  BOOST_CHECK( ArrayQ::M == 5 );
  BOOST_CHECK( MatrixQ::M == 5 );
  BOOST_CHECK( MatrixQ::N == 5 );

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  Real x, y, z, time;
  Real nx, ny, nz;
  Real rho, u, v, w, t, p, c;

  // Test conditions: M = 0.3
  x = y = z = time = 0;   // not actually used in functions
  nx = -0.856;  ny = 0.48;  nz = 0.192;    // Note: magnitude = 1
  rho = 1.137;
  t = 300.1;
  p = R*rho*t;
  c = sqrt(gamma*R*t);
  ParamType param(0.1, 0.0); // grid spacing and jump values

  Real M = 0.3;
  Real alpha = 0.1;
  Real beta  = 0.4;
  u = M*c*cos(alpha)*cos(beta);
  v = M*c*           sin(beta);
  w = M*c*sin(alpha)*cos(beta);

  ArrayQ q, qB, qB2;

  q = pde.setDOFFrom( DensityVelocityPressure3D<Real>(rho, u, v, w, p) );

  const Real gmi = gamma - 1;

  Real TtSpec = t*(1+0.5*gmi*M*M);
  Real PtSpec = p*pow( (1+0.5*gmi*M*M), gamma/gmi );
  Real aSpec = alpha;
  Real bSpec = beta;

  BCClass bc2(pde, TtSpec, PtSpec, aSpec, bSpec);

  PyDict BCInflowSubsonic;
  BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  BCInflowSubsonic[BCEuler3DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.TtSpec] = TtSpec;
  BCInflowSubsonic[BCEuler3DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.PtSpec] = PtSpec;
  BCInflowSubsonic[BCEuler3DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.aSpec] = aSpec;
  BCInflowSubsonic[BCEuler3DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.bSpec] = bSpec;

  PyDict BCList;
  BCList["BC1"] = BCInflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc(pde, BCInflowSubsonic);

  // static tests
  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 5 );
  BOOST_CHECK( bc.NBC == 4 );
  BOOST_CHECK_EQUAL( false, bc.hasFluxViscous() );

  bc.state(x, y, z, time, nx, ny, nz, q, qB);
  bc.state(param, x, y, z, time, nx, ny, nz, q, qB2);

  Real rhoB, uB, vB, wB, tB;
  pde.variableInterpreter().eval(qB, rhoB, uB, vB, wB, tB);

  Real rhoTrue = rho;
  Real uTrue = u;
  Real vTrue = v;
  Real wTrue = w;
  Real tTrue = t;

  SANS_CHECK_CLOSE(rhoTrue, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(uTrue, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(vTrue, vB, small_tol, close_tol );
  SANS_CHECK_CLOSE(wTrue, wB, small_tol, close_tol );
  SANS_CHECK_CLOSE(tTrue, tB, small_tol, close_tol );



  Real rhox = 0.01, rhoy = -0.025, rhoz = -0.344;
  Real ux = 0.049, uy =  0.072, uz =  1.431;
  Real vx = 0.143, vy = -0.024, vz = -0.987;
  Real wx = 0.659, wy =  0.349, wz =  0.776;
  Real px = 0.002, py = -4.231, pz = -0.451;

  ArrayQ qx = {rhox, ux, vx, wx, px};
  ArrayQ qy = {rhoy, uy, vy, wy, py};
  ArrayQ qz = {rhoz, uz, vz, wz, pz};

  ArrayQ Fn = 0, Fn2 = 0;
  bc.fluxNormal( x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn);
  bc.fluxNormal( param, x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn2);

  ArrayQ fx = 0, fy = 0, fz = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, z, time, qB, fx, fy, fz);
  FnTrue += fx*nx + fy*ny + fz*nz;

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );

  SANS_CHECK_CLOSE( Fn(0), Fn2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(1), Fn2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(2), Fn2(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(3), Fn2(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(4), Fn2(4), small_tol, close_tol );

  // inflow/outflow
  BOOST_CHECK( bc.isValidState( nx, ny, nz, q) == true );
  BOOST_CHECK( bc.isValidState(-nx, ny, nz, q) == false );

  // supersonic
  q = pde.setDOFFrom( DensityVelocityPressure3D<Real>(rho, u, v, w, p*0.01) );
  BOOST_CHECK( bc.isValidState( nx, ny, nz, q) == false );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test_BCTypeOutflowSubsonic_Pressure_BN )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler3D<BCTypeOutflowSubsonic_Pressure_BN, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  typedef BCParameters< BCEuler3DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 5 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 5 );

  // ctors

  const Real gamma = 1.4;
  const Real R = 287.04;        // J/(kg K)
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  const Real pSpec = 101325;
  BCClass bc1(pde, pSpec);

  BOOST_CHECK( bc1.D == 3 );
  BOOST_CHECK( bc1.N == 5 );
  BOOST_CHECK( bc1.NBC == 3 );
  BOOST_CHECK( bc1.hasFluxViscous() == false );

  //Pydict constructor
  PyDict BCOutflowSubsonic;
  BCOutflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOutflowSubsonic[BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = pSpec;

  PyDict BCList;
  BCList["BC1"] = BCOutflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc(pde, BCOutflowSubsonic);

  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 5 );
  BOOST_CHECK( bc.NBC == 3 );
  BOOST_CHECK( bc.hasFluxViscous() == false );

  // functions

  const Real small_tol = 3.e-12;
  const Real close_tol = 3.e-12;

  Real x, y, z, time;
  Real nx, ny, nz;
  Real rho, u, v, w, t, p, c;

  // Test conditions: M = 0.3
  x = y = z = time = 0;   // not actually used in functions
  nx = 0.856;  ny = 0.48;  nz = 0.192;    // Note: magnitude = 1
  rho = 1.137;
  t = 300.1;
  p = R*rho*t;
  c = sqrt(gamma*R*t);
  ParamType param(0.1, 0.0); // grid spacing and jump values

  Real M = 0.3;
  Real alpha = 0.1;
  Real beta  = 0.4;
  u = M*c*cos(alpha)*cos(beta);
  v = M*c*           sin(beta);
  w = M*c*sin(alpha)*cos(beta);

  ArrayQ q, qB, qB2, qBTrue;
  DensityVelocityPressure3D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.VelocityZ = w;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  bc.state(x, y, z, time, nx, ny, nz, q, qB);
  bc.state(param, x, y, z, time, nx, ny, nz, q, qB2);

  qBTrue[0] = rho;
  qBTrue[1] = u;
  qBTrue[2] = v;
  qBTrue[3] = w;
  qBTrue[4] = p;

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(4), qB(4), small_tol, close_tol );

  SANS_CHECK_CLOSE( qB(0), qB2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qB(1), qB2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qB(2), qB2(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qB(3), qB2(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qB(4), qB2(4), small_tol, close_tol );


  Real rhox = 0.01, rhoy = -0.025, rhoz = -0.344;
  Real ux = 0.049, uy =  0.072, uz =  1.431;
  Real vx = 0.143, vy = -0.024, vz = -0.987;
  Real wx = 0.659, wy =  0.349, wz =  0.776;
  Real px = 0.002, py = -4.231, pz = -0.451;

  ArrayQ qx = {rhox, ux, vx, wx, px};
  ArrayQ qy = {rhoy, uy, vy, wy, py};
  ArrayQ qz = {rhoz, uz, vz, wz, pz};

  ArrayQ Fn = 0, Fn2 = 0;
  bc.fluxNormal( x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn);
  bc.fluxNormal( param, x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn2);

  ArrayQ fx = 0, fy = 0, fz = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, z, time, qB, fx, fy, fz);
  FnTrue += fx*nx + fy*ny + fz*nz;

  Real vn = u*nx + v*ny + w*nz;

  FnTrue[1] -= ( (p-pSpec) - rho*vn*vn/gamma*log(p/pSpec) )*nx;
  FnTrue[2] -= ( (p-pSpec) - rho*vn*vn/gamma*log(p/pSpec) )*ny;
  FnTrue[3] -= ( (p-pSpec) - rho*vn*vn/gamma*log(p/pSpec) )*nz;
  FnTrue[4] -= ( (p-pSpec) - rho*vn*vn/gamma*log(p/pSpec) )*vn;

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );

  SANS_CHECK_CLOSE( Fn(0), Fn2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(1), Fn2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(2), Fn2(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(3), Fn2(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(4), Fn2(4), small_tol, close_tol );

  // inflow/outflow
  BOOST_CHECK( bc.isValidState( nx, ny, nz, q) == true );
  BOOST_CHECK( bc.isValidState(-nx, ny, nz, q) == false );

#if 0   // currently does not check for supersonic or density
  // supersonic
  qdata.Pressure *= 0.01;
  pde.setDOFFrom( q, qdata );
  BOOST_CHECK( bc.isValidState( nx, ny, nz, q) == false );
  qdata.Pressure *= 100;

  // negative density
  qdata.Density *= -1;
  pde.setDOFFrom( q, qdata );
  BOOST_CHECK( bc.isValidState( nx, ny, nz, q) == false );
#endif
}



#if 0   // old file organization
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  {
  typedef BCEuler3D<BCTypeSymmetry_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 5 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( ArrayQ::M == 5 );
  BOOST_CHECK( MatrixQ::M == 5 );
  BOOST_CHECK( MatrixQ::N == 5 );
  }

  {
  typedef BCEuler3D<BCTypeFullState_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 5 );
  BOOST_CHECK( BCClass::NBC == 5 );
  BOOST_CHECK( ArrayQ::M == 5 );
  BOOST_CHECK( MatrixQ::M == 5 );
  BOOST_CHECK( MatrixQ::N == 5 );
  }

  {
  typedef SolutionFunction_Euler3D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction_EulerClass> BCTypeClass;
  typedef BCEuler3D<BCTypeClass, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler3D<BCTypeFunction_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 4 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler3D<BCTypeInflowSubsonic_sHqt_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler3D<BCTypeFunctionInflow_sHqt_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler3D<BCTypeInflowSubsonic_sHqt_BN, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler3D<BCTypeFunctionInflow_sHqt_BN, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler3D<BCTypeOutflowSubsonic_Pressure_BN, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef BCParameters< BCEuler3DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  {
  typedef BCEuler3D<BCTypeSymmetry_mitState, PDEClass> BCClass;

  BCClass bc1(pde);

  //Pydict constructor
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCList;
  BCList["BC1"] = BCSymmetry;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCSymmetry);
  }

  {
  typedef BCEuler3D<BCTypeFullState_mitState, PDEClass> BCClass;

  bool upwind = true;
  BCClass bc(pde, DensityVelocityPressure3D<Real>(1.1, 1.3, -0.4, 2.56, 5.45), upwind);

  PyDict d;
  d[NSVariableType3DParams::params.StateVector.Variables] = NSVariableType3DParams::params.StateVector.PrimitivePressure;
  d[DensityVelocityPressure3DParams::params.rho] = 1.1;
  d[DensityVelocityPressure3DParams::params.u] = 1.3;
  d[DensityVelocityPressure3DParams::params.v] = -0.4;
  d[DensityVelocityPressure3DParams::params.w] = 2.56;
  d[DensityVelocityPressure3DParams::params.p] = 5.45;

  PyDict BCFullState;
  BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFullState[BCClass::ParamsType::params.Characteristic] = false;
  BCFullState[BCClass::ParamsType::params.StateVector] = d;

  PyDict BCList;
  BCList["BC1"] = BCFullState;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCFullState);
  }

  {
  typedef SolutionFunction_Euler3D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction_EulerClass> BCTypeClass;
  typedef BCEuler3D<BCTypeClass, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {0, 0, 0, 0};
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  SolutionFunction_EulerClass constsol(gas, 1.0, 1.0, 1.0, 1.0);
  BCClass bc(pde, constsol);

  PyDict solnArgs;
  solnArgs[SolutionFunction_EulerClass::ParamsType::params.gasModel] = gasModelDict;
  solnArgs[SolutionFunction_EulerClass::ParamsType::params.rho] = 1.0;
  solnArgs[SolutionFunction_EulerClass::ParamsType::params.u] = 1.0;
  solnArgs[SolutionFunction_EulerClass::ParamsType::params.v] = 1.0;
  solnArgs[SolutionFunction_EulerClass::ParamsType::params.p] = 1.0;

  // Add the solution BC to the BCVector
  typedef boost::mpl::push_back< BCEuler3DVector<TraitsSizeEuler, TraitsModelEulerClass>, BCClass >::type BCVector;
  typedef BCParameters<BCVector> BCParams;

  PyDict BCInflowSubsonicProfile;
  BCInflowSubsonicProfile[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_Profile;
  BCInflowSubsonicProfile[BCEuler3DParams<BCTypeClass>::params.SolutionArgs] = solnArgs;

  PyDict BCList;
  BCList["BC1"] = BCInflowSubsonicProfile;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCInflowSubsonicProfile);
  }

  {
  typedef SolutionFunction_Euler3D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler3D<BCTypeFunction_mitState, PDEClass> BCClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  BCClass::Function_ptr constsol( new SolutionFunction_EulerClass(gas, 1.1, 1.3, -0.4, 5.45) );
  BCClass bc(pde, constsol, true);

  PyDict Function;
  Function[BCClass::ParamsType::params.Function.Name] = BCClass::ParamsType::params.Function.Const;
  Function[SolutionFunction_EulerClass::ParamsType::params.gasModel] = gasModelDict;
  Function[SolutionFunction_EulerClass::ParamsType::params.rho] = 1.1;
  Function[SolutionFunction_EulerClass::ParamsType::params.u] = 1.3;
  Function[SolutionFunction_EulerClass::ParamsType::params.v] = -0.4;
  Function[SolutionFunction_EulerClass::ParamsType::params.p] = 5.45;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCIn[BCClass::ParamsType::params.Function] = Function;
  BCIn[BCClass::ParamsType::params.Characteristic] = true;

  PyDict BCList;
  BCList["BC1"] = BCIn;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCIn);
  }

  {
  typedef SolutionFunction_Euler3D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler3D<BCTypeFunctionInflow_sHqt_mitState, PDEClass> BCClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  BCClass::Function_ptr constsol( new SolutionFunction_EulerClass(gas, 1.1, 1.3, -0.4, 5.45) );
  BCClass bc(pde, constsol);

  PyDict Function;
  Function[BCClass::ParamsType::params.Function.Name] = BCClass::ParamsType::params.Function.Const;
  Function[SolutionFunction_EulerClass::ParamsType::params.gasModel] = gasModelDict;
  Function[SolutionFunction_EulerClass::ParamsType::params.rho] = 1.1;
  Function[SolutionFunction_EulerClass::ParamsType::params.u] = 1.3;
  Function[SolutionFunction_EulerClass::ParamsType::params.v] = -0.4;
  Function[SolutionFunction_EulerClass::ParamsType::params.p] = 5.45;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.SolutionInflow_sHqt_mitState;
  BCIn[BCClass::ParamsType::params.Function] = Function;

  PyDict BCList;
  BCList["BC1"] = BCIn;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCIn);
  }

  {
  typedef SolutionFunction_Euler3D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler3D<BCTypeFunctionInflow_sHqt_BN, PDEClass> BCClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  BCClass::Function_ptr constsol( new SolutionFunction_EulerClass(gas, 1.1, 1.3, -0.4, 5.45) );
  BCClass bc(pde, constsol);

  PyDict Function;
  Function[BCClass::ParamsType::params.Function.Name] = BCClass::ParamsType::params.Function.Const;
  Function[SolutionFunction_EulerClass::ParamsType::params.gasModel] = gasModelDict;
  Function[SolutionFunction_EulerClass::ParamsType::params.rho] = 1.1;
  Function[SolutionFunction_EulerClass::ParamsType::params.u] = 1.3;
  Function[SolutionFunction_EulerClass::ParamsType::params.v] = -0.4;
  Function[SolutionFunction_EulerClass::ParamsType::params.p] = 5.45;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.SolutionInflow_sHqt_BN;
  BCIn[BCClass::ParamsType::params.Function] = Function;

  PyDict BCList;
  BCList["BC1"] = BCIn;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCIn);
  }


  {
  typedef BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDEClass> BCClass;

  Real TtSpec = 20.5;
  Real PtSpec = 71.4;
  Real aSpec = 2.1;
  BCClass bc1(pde, TtSpec, PtSpec, aSpec);

  PyDict BCInflowSubsonic;
  BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  BCInflowSubsonic[BCEuler3DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.TtSpec] = TtSpec;
  BCInflowSubsonic[BCEuler3DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.PtSpec] = PtSpec;
  BCInflowSubsonic[BCEuler3DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.aSpec] = aSpec;

  PyDict BCList;
  BCList["BC1"] = BCInflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCInflowSubsonic);
  }

  {
  typedef BCEuler3D<BCTypeInflowSubsonic_sHqt_mitState, PDEClass> BCClass;

  Real sSpec = 20.5;
  Real HSpec = 71.4;
  Real VxSpec = 2.1;
  Real VySpec = -1.1;
  BCClass bc1(pde, sSpec, HSpec, VxSpec, VySpec);

  PyDict BCInflowSubsonic;
  BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_mitState;
  BCInflowSubsonic[BCEuler3DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.sSpec] = sSpec;
  BCInflowSubsonic[BCEuler3DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.HSpec] = HSpec;
  BCInflowSubsonic[BCEuler3DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.VxSpec] = VxSpec;
  BCInflowSubsonic[BCEuler3DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.VySpec] = VySpec;

  PyDict BCList;
  BCList["BC1"] = BCInflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCInflowSubsonic);
  }

  {
  typedef BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDEClass> BCClass;

  const Real pSpec = 2.1;
  BCClass bc(pde, pSpec);

  PyDict BCOutflowSubsonic;
  BCOutflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOutflowSubsonic[BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pSpec;

  PyDict BCList;
  BCList["BC1"] = BCOutflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCOutflowSubsonic);
  }

  {
  typedef BCEuler3D<BCTypeInflowSubsonic_sHqt_BN, PDEClass> BCClass;

  Real sSpec = 20.5;
  Real HSpec = 71.4;
  Real VxSpec = 2.1;
  Real VySpec = -1.1;
  Real aSpec = atan(VySpec/VxSpec);
  BCClass bc1(pde, sSpec, HSpec, aSpec);

  PyDict BCInflowSubsonic;
  BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  BCInflowSubsonic[BCEuler3DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] = sSpec;
  BCInflowSubsonic[BCEuler3DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = HSpec;
  BCInflowSubsonic[BCEuler3DParams<BCTypeInflowSubsonic_sHqt_BN>::params.aSpec] = aSpec;

  PyDict BCList;
  BCList["BC1"] = BCInflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCInflowSubsonic);
  }

  {
  typedef BCEuler3D<BCTypeOutflowSubsonic_Pressure_BN, PDEClass> BCClass;

  const Real pSpec = 2.1;
  BCClass bc(pde, pSpec);

  PyDict BCOutflowSubsonic;
  BCOutflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOutflowSubsonic[BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = pSpec;

  PyDict BCList;
  BCList["BC1"] = BCOutflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCOutflowSubsonic);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_Symmetry_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler3D<BCTypeSymmetry_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  BCClass bc(pde);

  // static tests
  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 5 );
  BOOST_CHECK( bc.NBC == 1 );

  Real x, y, z, time;
  Real nx, ny, nz;
  Real rho, u, v, w, t, p, qn;

  x = y = z = time = 0;   // not actually used in functions
  nx = -0.59;  ny = 0.152; nz = sqrt(1 - nx*nx + ny*ny);   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; w = 0.456; t = 0.987;
  p  = R*rho*t;
  qn = nx*u + ny*v + nz*w;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  Real uBTrue = u - qn*nx;
  Real vBTrue = v - qn*ny;
  Real wBTrue = w - qn*nz;

  Real pBTrue = p;

  ArrayQ qI;
  pde.setDOFFrom( qI, DensityVelocityPressure3D<Real>(rho, u, v, w, p) );

  ArrayQ qB;
  bc.state( x, y, z, time, nx, ny, nz, qI, qB );

  Real rhoB, uB, vB, wB, tB;
  pde.variableInterpreter().eval( qB, rhoB, uB, vB, wB, tB );
  Real pB = gas.pressure(rhoB, tB);

  SANS_CHECK_CLOSE(    rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE( uBTrue,   uB, small_tol, close_tol );
  SANS_CHECK_CLOSE( vBTrue,   vB, small_tol, close_tol );
  SANS_CHECK_CLOSE( wBTrue,   wB, small_tol, close_tol );
  SANS_CHECK_CLOSE( pBTrue,   pB, small_tol, close_tol );

  bc.state( param, x, y, z, time, nx, ny, nz, qI, qB );

  Real rhoB2, uB2, vB2, wB2, tB2;
  pde.variableInterpreter().eval( qB, rhoB2, uB2, vB2, wB2, tB2 );
  Real pB2 = gas.pressure(rhoB2, tB2);

  SANS_CHECK_CLOSE(   rhoB, rhoB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     uB,   uB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     vB,   vB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     wB,   wB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     pB,   pB2, small_tol, close_tol );

  BOOST_CHECK( bc.isValidState(nx, ny, nz, qI) == true );
  BOOST_CHECK( bc.useUpwind() == false );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_fullstate )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler3D<BCTypeFullState_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  Real x, y, z, time;
  Real nx, ny, nz;
  Real rho, u, v, w, t, p;

  x = y = z = time = 0;   // not actually used in functions
  nx = -0.59;  ny = 0.152; nz = sqrt(1 - nx*nx + ny*ny);   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; w = 0.456; t = 0.987;
  p  = R*rho*t;

  ArrayQ q;
  DensityVelocityPressure3D<Real> qdata(rho, u, v, w, p);
  pde.setDOFFrom( q, qdata );

  BCClass bc(pde, qdata, false);
  BCClass bc2(pde, qdata, true);

  // static tests
  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 5 );
  BOOST_CHECK( bc.NBC == 5 );
  BOOST_CHECK_EQUAL(false, bc.useUpwind());
  BOOST_CHECK_EQUAL(true, bc2.useUpwind());

  ArrayQ qI = 0;
  ArrayQ qB;

  bc.state(x, y, z, time, nx, ny, nz, qI, qB);

  Real rhoB, uB, vB, wB, tB;
  pde.variableInterpreter().eval(qB, rhoB, uB, vB, wB, tB);

  SANS_CHECK_CLOSE(rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(v, vB, small_tol, close_tol );
  SANS_CHECK_CLOSE(w, wB, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB, small_tol, close_tol );

  qI = pde.setDOFFrom( DensityVelocityPressure3D<Real>(rhoB, uB, vB, wB, tB) );
  BOOST_CHECK_EQUAL( true, bc.isValidState(nx, ny, nz, qI) );
  BOOST_CHECK_EQUAL( true, bc2.isValidState(nx, ny, nz, qI) );

  qI = pde.setDOFFrom( DensityVelocityPressure3D<Real>(rhoB, -uB, -vB, -wB, tB) );
  BOOST_CHECK_EQUAL( false, bc.isValidState(nx, ny, nz, qI) );
  BOOST_CHECK_EQUAL( true, bc2.isValidState(nx, ny, nz, qI) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_fullstate_solution )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef SolutionFunction_Euler3D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler3D<BCTypeFunction_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  Real x, y, z, time;
  Real nx, ny;
  Real rho, u, v, t, p;

  x = y = z = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  p  = R*rho*t;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  ArrayQ q;
  DensityVelocityPressure3D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  BCClass::Function_ptr constsol( new SolutionFunction_EulerClass(gas, rho, u, v, p) );
  BCClass bc(pde, constsol, true);
  BCClass bc2(pde, constsol, false);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 4 );
  BOOST_CHECK_EQUAL(true, bc.useUpwind());
  BOOST_CHECK_EQUAL(false, bc2.useUpwind());

  ArrayQ qI = 0;
  ArrayQ qB;

  bc.state(x, y, z, time, nx, ny, qI, qB);

  Real rhoB, uB, vB, tB;
  pde.variableInterpreter().eval(qB, rhoB, uB, vB, tB);

  SANS_CHECK_CLOSE(rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(v, vB, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB, small_tol, close_tol );

  bc.state( param, x, y, z, time, nx, ny, qI, qB );

  Real rhoB2, uB2, vB2, tB2;
  pde.variableInterpreter().eval( qB, rhoB2, uB2, vB2, tB2 );

  SANS_CHECK_CLOSE(   rhoB, rhoB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     uB,   uB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     vB,   vB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(     tB,   tB2, small_tol, close_tol );

  qI = pde.setDOFFrom( DensityVelocityPressure3D<Real>(rhoB, uB, vB, tB) );
  BOOST_CHECK_EQUAL( true, bc.isValidState(nx, ny, qI) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_SubsonicInflow_sHqt_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler3D<BCTypeInflowSubsonic_sHqt_mitState, PDEClass> BCClass;

  typedef SolutionFunction_Euler3D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler3D<BCTypeFunctionInflow_sHqt_mitState, PDEClass> BCClass2;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  Real x, y, z, time;
  Real nx, ny;
  Real rho, u, v, t, p;

  x = y = z = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.0784; v = -0.0231; t = 0.987;
  p  = R*rho*t;

  ArrayQ q;
  DensityVelocityPressure3D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  Real sSpec = log(p / pow(rho,gamma));
  Real HSpec = gas.enthalpy(rho,t) + 0.5*(u*u+v*v);
  Real VxSpec = u;
  Real VySpec = v;

  BCClass bc(pde, sSpec, HSpec, VxSpec, VySpec);

  BCClass2::Function_ptr constsol( new SolutionFunction_EulerClass(gas, rho, u, v, p) );
  BCClass2 bc2(pde, constsol);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 3 );
  BOOST_CHECK_EQUAL( false, bc.hasFluxViscous() );

  ArrayQ qB = 0;
  ArrayQ qB2 = 0;

  bc.state(x, y, z, time, nx, ny, q, qB);
  bc2.state(x, y, z, time, nx, ny, q, qB2);

  Real rhoB, uB, vB, tB;
  pde.variableInterpreter().eval(qB, rhoB, uB, vB, tB);

  Real rhoB2, uB2, vB2, tB2;
  pde.variableInterpreter().eval(qB2, rhoB2, uB2, vB2, tB2);

  Real rhoTrue = rho;
  Real uTrue = u;
  Real vTrue = v;
  Real tTrue = t;

  SANS_CHECK_CLOSE(rhoTrue, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(uTrue, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(vTrue, vB, small_tol, close_tol );
  SANS_CHECK_CLOSE(tTrue, tB, small_tol, close_tol );

  SANS_CHECK_CLOSE(rhoTrue, rhoB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(uTrue, uB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(vTrue, vB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(tTrue, tB2, small_tol, close_tol );


  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  ArrayQ qx = {rhox, ux, vx, px};
  ArrayQ qy = {rhoy, uy, vy, py};

  ArrayQ Fn = 0;
  ArrayQ Fn2 = 0;
  bc.fluxNormal( x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn);
  bc2.fluxNormal( x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn2);

  ArrayQ fx = 0;
  ArrayQ fy = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, z, time, qB, fx, fy);
  FnTrue += fx*nx + fy*ny;

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );

  SANS_CHECK_CLOSE( FnTrue(0), Fn2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn2(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn2(3), small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_outflow_pressure_mitState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  typedef BCParameters< BCEuler3DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;

  const Real gamma = 1.4;
  const Real R = 287.04;        // J/(kg K)
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  const Real small_tol = 3.e-12;
  const Real close_tol = 3.e-12;

  //Pydict constructor
  PyDict BCSubsonicOut;
  BCSubsonicOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;

  BCSubsonicOut[BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec]
                 = 101325.0;

  BCClass bc(pde, BCSubsonicOut);
  typedef BCClass::ArrayQ<Real> ArrayQ;

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 3 );
  BOOST_CHECK_EQUAL( false, bc.hasFluxViscous() );

  Real x, y, z, time;
  Real nx, ny;
  Real rho, u, v, t, p;

  // Test conditions - M = 0.3, T0 = 300.1, P0 = 101325; alpha = 0.1 rad
  x = y = z = time = 0;   // not actually used in functions
  nx = 0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137;
  t = 300.1;
  p  = R*rho*t;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  Real M = 0.3;
  Real c =  sqrt(1.4*287.04*300.1);
  Real alpha = 0.1;
  u = M*c*cos(alpha);
  v = M*c*sin(alpha);

  ArrayQ q, qB, qB2, qBTrue;
  DensityVelocityPressure3D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  bc.state(x,y,time,nx,ny,q,qB);
  bc.state(param,x,y,time,nx,ny,q,qB2);

  qBTrue[0] = 1.164915860299754;
  qBTrue[1] = 95.7574160100521;
  qBTrue[2] = 7.428571205323625;
  qBTrue[3] = 101325.0;

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );

  SANS_CHECK_CLOSE( qB(0), qB2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qB(1), qB2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qB(2), qB2(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qB(3), qB2(3), small_tol, close_tol );


  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  ArrayQ qx = {rhox, ux, vx, px};
  ArrayQ qy = {rhoy, uy, vy, py};

  ArrayQ Fn = 0, Fn2 = 0;
  bc.fluxNormal( x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn);
  bc.fluxNormal( param, x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn2);

  ArrayQ fx = 0;
  ArrayQ fy = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, z, time, qB, fx, fy);
  FnTrue += fx*nx + fy*ny;

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );

  SANS_CHECK_CLOSE( Fn(0), Fn2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(1), Fn2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(2), Fn2(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(3), Fn2(3), small_tol, close_tol );

  // state is always valid
  BOOST_CHECK( bc.isValidState(nx, ny, q) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_sHqt_BN )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  typedef BCEuler3D<BCTypeInflowSubsonic_sHqt_BN, PDEClass> BCClass;
  typedef SolutionFunction_Euler3D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler3D<BCTypeFunctionInflow_sHqt_BN, PDEClass> BCClass2;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real R     = 0.4;
  const Real gamma = 1.4;
  const Real gmi   = gamma - 1;

  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  Real x, y, z, time;
  Real nx, ny;
  Real rho, u, v, t, p;

  x = y = z = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  p  = R*rho*t;

  ArrayQ q;
  DensityVelocityPressure3D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );
  ParamType param(0.1, 0.0); // grid spacing and jump values

  Real rhoSpec = rho*1.037;
  Real uSpec = u*(0.981);
  Real vSpec = v*(1.12);
  Real tSpec = t*(1.003);
  Real pSpec = gas.pressure(rhoSpec, tSpec);

  Real sSpec = log( pSpec / pow(rhoSpec,gamma) );
  Real HSpec = gas.enthalpy(rhoSpec, tSpec) + 0.5*(uSpec*uSpec+vSpec*vSpec);
  Real aSpec = atan(vSpec/uSpec);

  BCClass bc(pde, sSpec, HSpec, aSpec);

  BCClass2::Function_ptr constsol( new SolutionFunction_EulerClass(gas, rhoSpec, uSpec, vSpec, pSpec) );
  BCClass2 bc2(pde, constsol);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 3 );
  BOOST_CHECK_EQUAL( false, bc.hasFluxViscous() );

  BOOST_CHECK( bc2.D == 2 );
  BOOST_CHECK( bc2.N == 4 );
  BOOST_CHECK( bc2.NBC == 3 );
  BOOST_CHECK_EQUAL( false, bc2.hasFluxViscous() );

  ArrayQ qB = 0;
  ArrayQ qB2 = 0;

  // Check state is valid for inflow
  BOOST_CHECK(bc.isValidState(nx, ny, q) == true);

  // Check stat is not valid when outflow
  BOOST_CHECK(bc.isValidState(-nx, ny, q) == false);

  // Check state is valid for inflow
  BOOST_CHECK(bc2.isValidState(nx, ny, q) == true);

  // Check stat is not valid when outflow
  BOOST_CHECK(bc2.isValidState(-nx, ny, q) == false);

  bc.state(x, y, z, time, nx, ny, q, qB);
  bc2.state(x, y, z, time, nx, ny, q, qB2);

  Real rhoB, uB, vB, tB = 0;
  pde.variableInterpreter().eval(qB, rhoB, uB, vB, tB);

  Real rhoB2, uB2, vB2, tB2 = 0;
  pde.variableInterpreter().eval(qB2, rhoB2, uB2, vB2, tB2);

  SANS_CHECK_CLOSE(rho, rhoB, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB, small_tol, close_tol );
  SANS_CHECK_CLOSE(v, vB, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB, small_tol, close_tol );

  SANS_CHECK_CLOSE(rho, rhoB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(v, vB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB2, small_tol, close_tol );

  qB2 = 0;
  bc2.state(param, x, y, z, time, nx, ny, q, qB2);

  rhoB2 = 0;
  uB2 = 0;
  vB2 =0;
  tB2 = 0;
  pde.variableInterpreter().eval(qB2, rhoB2, uB2, vB2, tB2);

  SANS_CHECK_CLOSE(rho, rhoB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(u, uB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(v, vB2, small_tol, close_tol );
  SANS_CHECK_CLOSE(t, tB2, small_tol, close_tol );

  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  ArrayQ qx = {rhox, ux, vx, px};
  ArrayQ qy = {rhoy, uy, vy, py};

  ArrayQ Fn = 0;
  ArrayQ Fn2 = 0;
  bc.fluxNormal( x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn);
  bc2.fluxNormal( x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB2, Fn2);

  ArrayQ fx = 0;
  ArrayQ fy = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, z, time, q, fx, fy);
  FnTrue += fx*nx + fy*ny;

  //compute BN Flux

  Real H = gas.enthalpy(rho, t) + 0.5*(u*u+v*v);
  Real c = gas.speedofSound(t);
  Real s = log( p /pow(rho,gamma) );

  // Tangential vector
  Real tx = -ny;
  Real ty =  nx;

  // normal and tangential velocities
  Real vn = u*nx + v*ny;
  Real vt = u*tx + v*ty;
  Real V2 = u*u+v*v;
  Real V = sqrt(V2);

  // Strong BC's that are imposed
  Real ds  = s  - sSpec;
  Real dH  = H  - HSpec;
  Real dvt = v/V - vSpec/sqrt(uSpec*uSpec+vSpec*vSpec);

  //COMPUTE NORMAL FLUX
  FnTrue[0] -= -rho*vn/gmi*ds;
  FnTrue[0] -= vn*gamma*rho/(c*c + V2*gmi)*dH;
  FnTrue[0] -=  -vt*V2*V*rho*gamma/u/(c*c+V2*gmi)*dvt;

  FnTrue[1] -= -rho*(c*c*nx + u*vn*gamma)/(gamma*gmi)*ds;
  FnTrue[1] -= rho*(c*c*nx + u*vn*(2*gamma-1))/(c*c+V2*gmi)*dH;
  FnTrue[1] -= -(rho*V2*V*(c*c*v + vn*(v*vn*gmi+u*vt*gamma)))/(u*vn*(c*c+V2*gmi))*dvt;

  FnTrue[2] -=  -rho*(c*c*ny+v*vn*gamma*rho)/(gamma*gmi)*ds;
  FnTrue[2] -= rho*(c*c*ny + v*vn*(2*gamma-1))/(c*c+V2*gmi)*dH;
  FnTrue[2] -= (rho*V2*V*(c*c*u + vn*(u*vn*gmi-v*vt*gamma)))/(u*vn*(c*c+V2*gmi))*dvt;

  FnTrue[3] -= -rho*vn*( 2*c*c + V2*gmi)/(2*gmi*gmi)*ds;
  FnTrue[3] -= (rho*vn*(c*c*(4*gamma-2) + V2*(2-5*gamma+3*gamma*gamma)))/(2*gmi*(c*c + V2*gmi) )*dH;
  FnTrue[3] -=  -(vt*V2*V*rho*gamma*(2*c*c+V2*gmi))/(2*u*gmi*(c*c+V2*gmi))*dvt;

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );

  SANS_CHECK_CLOSE( FnTrue(0), Fn2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn2(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn2(3), small_tol, close_tol );

  Fn2 = 0;
  bc2.fluxNormal( param, x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB2, Fn2);

  SANS_CHECK_CLOSE( Fn(0), Fn2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(1), Fn2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(2), Fn2(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fn(3), Fn2(3), small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_outflow_pressure_BN )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler3D<BCTypeOutflowSubsonic_Pressure_BN, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  typedef BCParameters< BCEuler3DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;

  const Real gamma = 1.4;
  const Real R = 287.04;        // J/(kg K)
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  const Real small_tol = 3.e-12;
  const Real close_tol = 3.e-12;

  Real pSpec = 101325.0;
  //Pydict constructor
  PyDict BCSubsonicOut;
  BCSubsonicOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;

  BCSubsonicOut[BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec]
                 = pSpec;

  BCClass bc(pde, BCSubsonicOut);
  typedef BCClass::ArrayQ<Real> ArrayQ;

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 3 );
  BOOST_CHECK_EQUAL( false, bc.hasFluxViscous() );

  Real x, y, z, time;
  Real nx, ny;
  Real rho, u, v, t, p, c;

  // Test conditions - M = 0.3, T0 = 300.1, P0 = 101325; alpha = 0.1 rad
  x = y = z = time = 0;   // not actually used in functions
  nx = 0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137;
  t = 300.1;
  p  = R*rho*t;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  Real M = 0.3;
  Real alpha = 0.1;
  c = sqrt(gamma*R*t);
  u = M*c*cos(alpha);
  v = M*c*sin(alpha);

  ArrayQ q, qB, qB2, qBTrue;
  DensityVelocityPressure3D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  bc.state(x,y,time,nx,ny,q,qB);
  bc.state(param,x,y,time,nx,ny,q,qB2);

  SANS_CHECK_CLOSE( q(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( q(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( q(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( q(3), qB(3), small_tol, close_tol );

  SANS_CHECK_CLOSE( q(0), qB2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( q(1), qB2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( q(2), qB2(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( q(3), qB2(3), small_tol, close_tol );

  BOOST_CHECK(bc.isValidState(nx, ny, q));

  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  ArrayQ qx = {rhox, ux, vx, px};
  ArrayQ qy = {rhoy, uy, vy, py};

  ArrayQ Fn = 0, Fn2 = 0;
  bc.fluxNormal( x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn);
  bc.fluxNormal( param, x, y, z, time, nx, ny, nz, q, qx, qy, qz, qB, Fn2);

  ArrayQ fx = 0;
  ArrayQ fy = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, z, time, qB, fx, fy);
  FnTrue += fx*nx + fy*ny;

  Real vn = u*nx + v*ny;

  FnTrue[1] -= ( (p-pSpec) - rho*vn*vn/gamma*log(p/pSpec) )*nx;
  FnTrue[2] -= ( (p-pSpec) - rho*vn*vn/gamma*log(p/pSpec) )*ny;
  FnTrue[3] -= ( vn*(p-pSpec) - rho*vn*vn*vn/gamma*log(p/pSpec) );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );

  SANS_CHECK_CLOSE( FnTrue(0), Fn2(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn2(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn2(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn2(3), small_tol, close_tol );
}
#endif   // old file organization


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/BCEuler3D_mitState_pattern.txt", true );

  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  {
  typedef BCEuler3D<BCTypeFullState_mitState, PDEClass> BCClass;

  bool upwind = true;
  BCClass bc(pde, DensityVelocityPressure3D<Real>(1.1, 1.3, -0.4, 2.56, 5.45), upwind);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler3D<BCTypeSymmetry_mitState, PDEClass> BCClass;

  BCClass bc(pde);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDEClass> BCClass;

  const Real pSpec = 2.3;
  BCClass bc(pde, pSpec);

  bc.dump( 2, output );
  }

#if 0   // not fully implemented in 3D
  {
  typedef BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDEClass> BCClass;

  const Real PtSpec = 1.0;
  const Real TtSpec = 2.0;
  const Real aoaSpec = 3.0;
  BCClass bc(pde, PtSpec, TtSpec, aoaSpec);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler3D<BCTypeInflowSubsonic_sHqt_mitState, PDEClass> BCClass;

  const Real sSpec = 1.0;
  const Real HSpec = 2.0;
  const Real VxSpec = 3.0;
  const Real VySpec = 4.0;
  BCClass bc(pde, sSpec, HSpec, VxSpec, VySpec);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler3D<BCTypeInflowSubsonic_sHqt_BN, PDEClass> BCClass;

  const Real sSpec = 1.0;
  const Real HSpec = 2.0;
  const Real aSpec = 0.1;
  BCClass bc(pde, sSpec, HSpec, aSpec);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler3D<BCTypeOutflowSubsonic_Pressure_BN, PDEClass> BCClass;

  const Real pSpec = 2.3;
  BCClass bc(pde, pSpec);

  bc.dump( 2, output );
  }

  {
  typedef SolutionFunction_Euler3D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler3D<BCTypeFunction_mitState, PDEClass> BCClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  BCClass::Function_ptr constsol( new SolutionFunction_EulerClass(gas, 1.1, 1.3, -0.4, 5.45) );
  BCClass bc(pde, constsol, true);

  bc.dump( 2, output );
  }

  {
  typedef SolutionFunction_Euler3D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler3D<BCTypeFunctionInflow_sHqt_mitState, PDEClass> BCClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  BCClass::Function_ptr constsol( new SolutionFunction_EulerClass(gas, 1.1, 1.3, -0.4, 5.45) );
  BCClass bc(pde, constsol);

  bc.dump( 2, output );

  }

  {
  typedef SolutionFunction_Euler3D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler3D<BCTypeFunctionInflow_sHqt_BN, PDEClass> BCClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  BCClass::Function_ptr constsol( new SolutionFunction_EulerClass(gas, 1.1, 1.3, -0.4, 5.45) );
  BCClass bc(pde, constsol);

  bc.dump( 2, output );

  }
#endif   // not fully implemented in 3D

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
