// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// HLLC2D_btest
//
// test of 2-D compressible Euler PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/NS/TraitsEuler.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/HLLC2D.h"

#include "tools/minmax.h"


//Explicitly instantiate the class so coverage information is correct
namespace SANS
{
template class Roe2D<QTypePrimitiveRhoPressure, TraitsSizeEuler>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( HLLC2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AllLeft )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef HLLC2D<QTypePrimitiveRhoPressure, TraitsSizeEuler> fluxClass;
  typedef fluxClass::ArrayQ<Real> ArrayQ;

  const Real tol = 2.e-11;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  fluxClass HLLC2D(gas);
  PDEClass pde(gas, Euler_ResidInterp_Entropy);

  // Roe flux function test

  Real x, y, time;
  Real nx, ny;
  Real rhoL, uL, vL, tL, pL, h0L;
  Real rhoR, uR, vR, tR;
  Real Cp;

  x = y = time = 0;   // not actually used in functions
  nx = 2.0/7.0; ny = sqrt(1.0-nx*nx);
  rhoL = 1.034; uL = 30.26; vL = -2.17; tL = 5.78;
  rhoR = 0.973; uR = 10.79; vR =  0.93; tR = 6.13;
  Cp  = R*gamma/(gamma - 1);
  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL + vL*vL);

  // set
  ArrayQ qL = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
  ArrayQ qR = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );

  // advective normal flux (average)
  Real fL[4] = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*vL     , rhoL*uL*h0L};
//  Real fR[4] = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*vR     , rhoR*uR*h0R};
  Real gL[4] = {rhoL*vL, rhoL*vL*uL     , rhoL*vL*vL + pL, rhoL*vL*h0L};
//  Real gR[4] = {rhoR*vR, rhoR*vR*uR     , rhoR*vR*vR + pR, rhoR*vR*h0R};

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < 4; k++)
    fnTrue[k] = nx*fL[k] + ny*gL[k];

  ArrayQ fn;
  fn = 0;
  HLLC2D.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( fnTrue(3), fn(3), tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AllRight )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef HLLC2D<QTypePrimitiveRhoPressure, TraitsSizeEuler> fluxClass;
  typedef fluxClass::ArrayQ<Real> ArrayQ;

  const Real tol = 2.e-11;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  fluxClass HLLC2D(gas);
  PDEClass pde(gas, Euler_ResidInterp_Entropy);

  // Roe flux function test

  Real x, y, time;
  Real nx, ny;
  Real rhoL, uL, vL, tL;
  Real rhoR, uR, vR, tR, pR, h0R;
  Real Cp;

  x = y = time = 0;   // not actually used in functions
  nx = 2.0/7.0; ny = sqrt(1.0-nx*nx);
  rhoL = 1.034; uL = -30.26; vL = -2.17; tL = 5.78;
  rhoR = 0.973; uR = -10.79; vR =  0.93; tR = 6.13;
  Cp  = R*gamma/(gamma - 1);
  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR + vR*vR);

  // set
  ArrayQ qL = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
  ArrayQ qR = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );

  // advective normal flux (average)
//  Real fL[4] = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*vL     , rhoL*uL*h0L};
  Real fR[4] = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*vR     , rhoR*uR*h0R};
//  Real gL[4] = {rhoL*vL, rhoL*vL*uL     , rhoL*vL*vL + pL, rhoL*vL*h0L};
  Real gR[4] = {rhoR*vR, rhoR*vR*uR     , rhoR*vR*vR + pR, rhoR*vR*h0R};

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < 4; k++)
    fnTrue[k] = nx*fR[k] + ny*gR[k];

  ArrayQ fn;
  fn = 0;
  HLLC2D.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( fnTrue(3), fn(3), tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MushingLeft )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef HLLC2D<QTypePrimitiveRhoPressure, TraitsSizeEuler> fluxClass;
  typedef fluxClass::ArrayQ<Real> ArrayQ;

  const Real tol = 2.e-11;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  fluxClass HLLC2D(gas);
  PDEClass pde(gas, Euler_ResidInterp_Entropy);

  // Roe flux function test

  Real x, y, time;
  Real nx, ny;
  Real rhoL, uL, vL, tL, pL, h0L, EL;
  Real rhoR, uR, vR, tR, pR;
  Real Cp;

  x = y = time = 0;   // not actually used in functions
  nx = 2.0/7.0; ny = sqrt(1.0-nx*nx);
  rhoL = 1.034; uL = 3.26; vL = -2.17; tL = 5.78;
  rhoR = 0.973; uR = 1.79; vR =  0.93; tR = 6.13;
  Cp  = R*gamma/(gamma - 1);
  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL + vL*vL);
  EL  = gas.energy(rhoL, tL) + 0.5*(uL*uL + vL*vL);
  pR  = R*rhoR*tR;

  // set
  ArrayQ qL = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
  ArrayQ qR = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );

  // advective normal flux (average)
  Real fL[4] = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*vL     , rhoL*uL*h0L};
  Real gL[4] = {rhoL*vL, rhoL*vL*uL     , rhoL*vL*vL + pL, rhoL*vL*h0L};

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < 4; k++)
    fnTrue[k] = nx*fL[k] + ny*gL[k];


  // We need to resolve the velocity components and flux in that frame...
  Real unL =  uL*nx + vL*ny;
  Real unR =  uR*nx + vR*ny;
  // Get the left and right sound speeds and normal Mach numbers
  Real aL = gas.speedofSound(tL);
  Real aR = gas.speedofSound(tR);
  // Compute average density and speed of sound
  Real rhobar = 0.5*(rhoL + rhoR);
  Real abar   = 0.5*(aL + aR);
  // Now for pressure calculations
  Real p_pvrs = 0.5*(pL + pR) - 0.5*(unR - unL)*rhobar*abar;
  Real p_star = ::max(0., p_pvrs);
  // Now for qk calculation
  Real qnL = 0.;
  if (p_star/pL <= 1.)
  {
    qnL = 1.0;
  }
  else
  {
    qnL = sqrt(1. + (gamma+1.)/(2.*gamma)*(p_star/pL - 1.));
  }
  Real qnR = 0.;
  if (p_star/pR <= 1.)
  {
    qnR = 1.0;
  }
  else
  {
    qnR = sqrt(1. + (gamma+1.)/(2.*gamma)*(p_star/pR - 1.));
  }
  // Wave speeds
  Real SL = unL - aL*qnL;
  Real SR = unR + aR*qnR;
  Real S_star = (pR - pL + rhoL*unL*(SL - unL) - rhoR*unR*(SR - unR)) / (rhoL*(SL - unL) - rhoR*(SR - unR));
  // Weighting variables
  Real cL = (SL - unL)/(SL - S_star);
  Real C1 = rhoL*cL*(S_star - unL);
  Real C2 = rhoL*cL*(S_star - unL)*(S_star + pL/(rhoL*(SL - unL)));

  // Now add these contributions
  fnTrue[0] += SL* rhoL*   (cL-1.);
  fnTrue[1] += SL*(rhoL*uL*(cL-1.) + C1*nx);
  fnTrue[2] += SL*(rhoL*vL*(cL-1.) + C1*ny);
  fnTrue[3] += SL*(rhoL*EL*(cL-1.) + C2   );

  ArrayQ fn;
  fn = 0;
  HLLC2D.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( fnTrue(3), fn(3), tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MushingRight )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef HLLC2D<QTypePrimitiveRhoPressure, TraitsSizeEuler> fluxClass;
  typedef fluxClass::ArrayQ<Real> ArrayQ;

  const Real tol = 2.e-11;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  fluxClass HLLC2D(gas);
  PDEClass pde(gas, Euler_ResidInterp_Entropy);

  // Roe flux function test

  Real x, y, time;
  Real nx, ny;
  Real rhoL, uL, vL, tL, pL;
  Real rhoR, uR, vR, tR, pR, h0R, ER;
  Real Cp;

  x = y = time = 0;   // not actually used in functions
  nx = 2.0/7.0; ny = sqrt(1.0-nx*nx);
  rhoL = 1.034; uL = -3.26; vL = -2.17; tL = 5.78;
  rhoR = 0.973; uR = -1.79; vR =  0.93; tR = 6.13;
  Cp  = R*gamma/(gamma - 1);
  pL  = R*rhoL*tL;
  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR + vR*vR);
  ER  = gas.energy(rhoR, tR) + 0.5*(uR*uR + vR*vR);

  // set
  ArrayQ qL = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
  ArrayQ qR = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );

  // advective normal flux (average)
  Real fR[4] = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*vR     , rhoR*uR*h0R};
  Real gR[4] = {rhoR*vR, rhoR*vR*uR     , rhoR*vR*vR + pR, rhoR*vR*h0R};

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < 4; k++)
    fnTrue[k] = nx*fR[k] + ny*gR[k];


  // We need to resolve the velocity components and flux in that frame...
  Real unL =  uL*nx + vL*ny;
  Real unR =  uR*nx + vR*ny;
  // Get the left and right sound speeds and normal Mach numbers
  Real aL = gas.speedofSound(tL);
  Real aR = gas.speedofSound(tR);
  // Compute average density and speed of sound
  Real rhobar = 0.5*(rhoL + rhoR);
  Real abar   = 0.5*(aL + aR);
  // Now for pressure calculations
  Real p_pvrs = 0.5*(pL + pR) - 0.5*(unR - unL)*rhobar*abar;
  Real p_star = ::max(0., p_pvrs);
  // Now for qk calculation
  Real qnL = 0.;
  if (p_star/pL <= 1.)
  {
    qnL = 1.0;
  }
  else
  {
    qnL = sqrt(1. + (gamma+1.)/(2.*gamma)*(p_star/pL - 1.));
  }
  Real qnR = 0.;
  if (p_star/pR <= 1.)
  {
    qnR = 1.0;
  }
  else
  {
    qnR = sqrt(1. + (gamma+1.)/(2.*gamma)*(p_star/pR - 1.));
  }
  // Wave speeds
  Real SL = unL - aL*qnL;
  Real SR = unR + aR*qnR;
  Real S_star = (pR - pL + rhoL*unL*(SL - unL) - rhoR*unR*(SR - unR)) / (rhoL*(SL - unL) - rhoR*(SR - unR));
  // Weighting variables
  Real cR = (SR - unR)/(SR - S_star);
  Real C1 = rhoR*cR*(S_star - unR);
  Real C2 = rhoR*cR*(S_star - unR)*(S_star + pR/(rhoR*(SR - unR)));

  // Now add these contributions
  fnTrue[0] += SR* rhoR*   (cR-1.);
  fnTrue[1] += SR*(rhoR*uR*(cR-1.) + C1*nx);
  fnTrue[2] += SR*(rhoR*vR*(cR-1.) + C1*ny);
  fnTrue[3] += SR*(rhoR*ER*(cR-1.) + C2   );

  ArrayQ fn;
  fn = 0;
  HLLC2D.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( fnTrue(3), fn(3), tol );

}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux_DG_HDG_Roe )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef Roe2D<QTypePrimitiveRhoPressure, TraitsSizeEuler> RoeClass;
  typedef RoeClass::ArrayQ<Real> ArrayQ;
  typedef RoeClass::MatrixQ<Real> MatrixQ;

  const Real tol = 2.e-11;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  RoeClass roe2D(gas);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Entropy);

  // Roe flux function test

  Real x, y, time;
  Real nx, ny;
  Real rhoL, uL, vL, tL, pL, h0L;
  Real rhoI, uI, vI, tI, pI, h0I;
  Real rhoR, uR, vR, tR, pR, h0R;
  Real Cp;

  x = y = time = 0;   // not actually used in functions
  nx = 2.0/7.0; ny = sqrt(1.0-nx*nx);
  rhoL = 1.034; uL = 1.6; vL = 0.85; tL = 5.78;
  rhoR = 0.973; uR = 1.79; vR =  0.93; tR = 6.13;

  Cp  = R*gamma/(gamma - 1);
  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL + vL*vL);
  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR + vR*vR);

  rhoI = 0.996454832345328;
  uI = 1.61894452004249;
  vI = 0.913541851924181;
  tI = 5.69508497216806;

  // set
  ArrayQ qL = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
  ArrayQ qR = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );
  ArrayQ qI = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rhoI, uI, vI, tI) );


  ArrayQ fnDG;
  fnDG = 0;
  roe2D.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fnDG );

  std::cout << fnDG << "\n";

  ArrayQ fnHDGL, fnHDGR;
  fnHDGL = 0;
  fnHDGR = 0;
  ArrayQ f, g;
  f = 0; g = 0;
//  pde.fluxAdvective(x,y,time,qL, f, g);
//  fnHDGL = f*nx + g*ny;
  roe2D.fluxAdvectiveUpwind( x, y, time, qL, qI, nx, ny, fnHDGL );
  roe2D.fluxAdvectiveUpwind( x, y, time, qI, qR, nx, ny, fnHDGR );
//
  std::cout << fnHDGL << "\n";
  std::cout << fnHDGR << "\n";

}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
