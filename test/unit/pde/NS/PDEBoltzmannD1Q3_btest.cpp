// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEBoltzmannD1Q3_btest
//
// test of quasi 1-D compressible Euler PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsBoltzmannD1Q3.h"
//#include "pde/NS/Q1DPrimitiveRhoPressure.h"
//#include "pde/NS/Q1DPrimitiveSurrogate.h"
//#include "pde/NS/Q1DConservative.h"
//#include "pde/NS/Q1DEntropy.h"
#include "pde/NS/QD1Q3PrimitiveDistributionFunctions.h"
#include "pde/NS/PDEBoltzmannD1Q3.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"

namespace SANS
{
// Needed for Boost Test
//class QTypePrimitiveDistributionFunctions {};
class QTypePrimitiveSurrogate {};
//class QTypeConservative {};
//class QTypeEntropy {};
class QTypePrimitiveDistributionFunctions {};

//Explicitly instantiate the class so coverage information is correct
template class TraitsSizeBoltzmannD1Q3<PhysD1>;
template class TraitsModelBoltzmannD1Q3<QTypePrimitiveDistributionFunctions, GasModel>;
//template class TraitsModelBoltzmannD1Q3<QTypePrimitiveSurrogate, GasModel>;
//template class TraitsModelBoltzmannD1Q3<QTypeConservative, GasModel>;
//template class TraitsModelBoltzmannD1Q3<QTypeEntropy, GasModel>;
template class PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3<QTypePrimitiveDistributionFunctions, GasModel>>;
//template class PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3<QTypePrimitiveSurrogate, GasModel>>;
//template class PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3<QTypeConservative, GasModel>>;
//template class PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3<QTypeEntropy, GasModel>>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEBoltzmannD1Q3_test_suite )

typedef boost::mpl::list< QTypePrimitiveDistributionFunctions//,
  //                        QTypeConservative,
    //                      QTypePrimitiveSurrogate,
      //                    QTypeEntropy
                         > QTypes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( static_test, QType, QTypes )
{
  typedef TraitsModelBoltzmannD1Q3<QType, GasModel> TraitsModelBoltzmannD1Q3Class;
  typedef PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  BOOST_REQUIRE( PDEClass::D == 1 );
  BOOST_REQUIRE( PDEClass::N == 3 );
  BOOST_REQUIRE( ArrayQ::M == 3 );
  BOOST_REQUIRE( MatrixQ::M == 3 );
  BOOST_REQUIRE( MatrixQ::N == 3 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctor, QType, QTypes )
{
  typedef TraitsModelBoltzmannD1Q3<QType, GasModel> TraitsModelBoltzmannD1Q3Class;
  typedef PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> PDEClass;
  std::shared_ptr<ScalarFunction1D_OffsetLog> area( new ScalarFunction1D_OffsetLog() );

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( pde.D == 1 );
  BOOST_REQUIRE( pde.N == 3 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  Real tau = 1.3;
  Real Uadv = 2.2;
  PDEClass pde2(gas, PDEClass::Euler_ResidInterp_Raw,
                PDEClass::BGK, tau,
                PDEClass::AdvectionDiffusion, Uadv, area); //RoeEntropyFix: none (overloaded default)

  // static tests
  BOOST_REQUIRE( pde2.D == 1 );
  BOOST_REQUIRE( pde2.N == 3 );

  // flux components
  BOOST_CHECK( pde2.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde2.hasFluxAdvective() == true );
  BOOST_CHECK( pde2.hasFluxViscous() == false );
  BOOST_CHECK( pde2.hasSource() == true );
  BOOST_CHECK( pde2.hasForcingFunction() == false );
  BOOST_CHECK( pde2.needsSolutionGradientforSource() == false );
}
#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( set_PrimitiveDistributionFunctions )
{
  typedef TraitsModelBoltzmannD1Q3<QTypePrimitiveDistributionFunctions, GasModel> TraitsModelBoltzmannD1Q3Class;
  typedef PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  Real tau = 1.3;
  Real Uadv = 2.2;
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw,
                PDEClass::BGK, tau,
                PDEClass::AdvectionDiffusion, Uadv ); //area =null; RoeEntropyFix: none (overloaded default)


  Real f1, f2, f3;

  f1 = 1.137; f2 = 0.784; f3 = 0.987;

  PyDict d;

  // set
  Real qDataPrim[3] = {f1, f2, f3};
  string qNamePrim[3] = {"DistributionFunction0", "DistributionFunction1", "DistributionFunction2"};
  ArrayQ qTrue = {f1, f2, f3};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );

  PrimitiveDistributionFunctionsD1Q3<Real> qdata1;
  qdata1.DistributionFunction0 = f1;
  qdata1.DistributionFunction1 = f2;
  qdata1.DistributionFunction2 = f3;
  q = 1;
  pde.setDOFFrom( q, qdata1 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );

  PyDict rhoVP;
  rhoVP[BoltzmannVariableTypeD1Q3Params::params.StateVector.Variables]
        = BoltzmannVariableTypeD1Q3Params::params.StateVector.PrimitiveDistributionFunctions3;
  rhoVP[PrimitiveDistributionFunctionsD1Q3Params::params.pdf0] = f1;
  rhoVP[PrimitiveDistributionFunctionsD1Q3Params::params.pdf1] = f2;
  rhoVP[PrimitiveDistributionFunctionsD1Q3Params::params.pdf2] = f3;

  d[BoltzmannVariableTypeD1Q3Params::params.StateVector] = rhoVP;
  BoltzmannVariableTypeD1Q3Params::checkInputs(d);
  q = pde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );

#if 0
  DensityVelocityTemperature1D<Real> qdata2(rho, u, t);
  q = 1;

  pde.setDOFFrom( q, qdata2 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );

  PyDict rhoVT;
  rhoVT[BoltzmannVariableTypeD1Q3Params::params.StateVector.Variables] = BoltzmannVariableTypeD1Q3Params::params.StateVector.PrimitiveTemperature;
  rhoVT[DensityVelocityTemperature1DParams::params.rho] = rho;
  rhoVT[DensityVelocityTemperature1DParams::params.u] = u;
  rhoVT[DensityVelocityTemperature1DParams::params.t] = t;

  d[BoltzmannVariableTypeD1Q3Params::params.StateVector] = rhoVT;
  BoltzmannVariableTypeD1Q3Params::checkInputs(d);
  q = pde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );


  Conservative1D<Real> qdata3(rho, rho*u, rho*E);
  q = 0;
  pde.setDOFFrom( q, qdata3 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );

  PyDict Conservative;
  Conservative[BoltzmannVariableTypeD1Q3Params::params.StateVector.Variables] = BoltzmannVariableTypeD1Q3Params::params.StateVector.Conservative;
  Conservative[Conservative1DParams::params.rho] = rho;
  Conservative[Conservative1DParams::params.rhou] = rho*u;
  Conservative[Conservative1DParams::params.rhoE] = rho*E;

  d[BoltzmannVariableTypeD1Q3Params::params.StateVector] = Conservative;
  BoltzmannVariableTypeD1Q3Params::checkInputs(d);
  q = pde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
#endif
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux, QType, QTypes )
{
  typedef TraitsModelBoltzmannD1Q3<QType, GasModel> TraitsModelBoltzmannD1Q3Class;
  typedef PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  Real tau = 1.3;
  Real Uadv = 2.2;
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw,
                PDEClass::BGK, tau,
                PDEClass::AdvectionDiffusion, Uadv ); //area =null; RoeEntropyFix: none (overloaded default)

  // static tests
  BOOST_REQUIRE( pde.N == 3 );

  // function tests

  Real x, time;
  Real f1, f2, f3;

  x = time = 0;   // not actually used in functions
  f1 = 1.137; f2 = 0.784; f3 = 0.987;

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, PrimitiveDistributionFunctionsD1Q3<Real>(f1, f2, f3) );

  // conservative flux
  //ArrayQ uTrue = {rho, rho*u, rho*e0};
  ArrayQ uTrue = {f1, f2, f3};
  ArrayQ uCons = 0;
  pde.masterState( x, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // Should not accumulate
  pde.masterState( x, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // conservative flux
  ArrayQ ftTrue = {f1, f2, f3};
  ArrayQ ft = 0;
  pde.fluxAdvectiveTime( x, time, q, ft );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );

  // Flux accumulate
  pde.fluxAdvectiveTime( x, time, q, ft );
  BOOST_CHECK_CLOSE( 2*ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(2), ft(2), tol );

  // advective flux
  Real e1 = -1.;
  Real e2 =  0.;
  Real e3 = +1.;
  ArrayQ fTrue = {e1*f1, e2*f2, e3*f3};
  ArrayQ f = 0;
  pde.fluxAdvective( x, time, q, f );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );

  // Flux accumulate
  pde.fluxAdvective( x, time, q, f );
  BOOST_CHECK_CLOSE( 2*fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(2), f(2), tol );

  //Call these simply to make sure coverage is high
  ArrayQ qL, qR, qx, qy, qt, qxL, qxR, qyL, qyR, source;
  Real dummy = 0;

  MatrixQ kxx = 0, kxt = 0, ktx = 0, ktt = 0;
  ArrayQ g = 0;
  pde.fluxViscous( dummy, dummy, q, qx, f );
  pde.fluxViscousSpaceTime( dummy, dummy, q, qx, qt, f, g );
  pde.diffusionViscous( dummy, dummy, q, qx, kxx );
  pde.diffusionViscousSpaceTime( dummy, dummy, q, qx, qt, kxx, kxt, ktx, ktt );
  pde.source( dummy, dummy, q, qx, source );
  pde.forcingFunction( x, dummy, source );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux_areaChange, QType, QTypes )
{
  typedef TraitsModelBoltzmannD1Q3<QType, GasModel> TraitsModelBoltzmannD1Q3Class;
  typedef PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  std::shared_ptr<ScalarFunction1D_OffsetLog> area( new ScalarFunction1D_OffsetLog() );

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  Real tau = 1.3;
  Real Uadv = 2.2;
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw,
                PDEClass::BGK, tau,
                PDEClass::AdvectionDiffusion, Uadv ); //area =null; RoeEntropyFix: none (overloaded default)

  // static tests
  BOOST_REQUIRE( pde.N == 3 );

  // function tests

  Real x, time;
  Real f1, f2, f3;

  x = time = 0;   // not actually used in functions
  f1 = 1.137; f2 = 0.784; f3 = 0.987;

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, PrimitiveDistributionFunctionsD1Q3<Real>(f1, f2, f3) );

  // conservative flux
  //ArrayQ uTrue = {rho, rho*u, rho*e0};
  ArrayQ uTrue = {f1, f2, f3};
  ArrayQ uCons = 0;
  pde.masterState( x, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // Should not accumulate
  pde.masterState( x, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // advective flux
  Real e1 = -1.;
  Real e2 =  0.;
  Real e3 = +1.;
  ArrayQ fTrue = {e1*f1, e2*f2, e3*f3};
  ArrayQ f = 0;
  pde.fluxAdvective( x, time, q, f );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );

  // Flux accumulate
  pde.fluxAdvective( x, time, q, f );
  BOOST_CHECK_CLOSE( 2*fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(2), f(2), tol );


  //Call these simply to make sure coverage is high
  ArrayQ qL, qR, qx, qy, qxL, qxR, qyL, qyR, source;
  Real dummy = 0;

  MatrixQ kxx;
  pde.fluxViscous( dummy, dummy, q, qx, f );
  pde.diffusionViscous( dummy, dummy, q, qx, kxx );
  pde.source( dummy, dummy, q, qx, source );
  pde.forcingFunction( x, dummy, source );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( jacobianMasterState, QType, QTypes )
{
  typedef TraitsModelBoltzmannD1Q3<QType, GasModel> TraitsModelBoltzmannD1Q3Class;
  typedef PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename PDEClass::template ArrayQ<SurrealS<PDEClass::N>> ArrayQSurreal;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  Real tau = 1.3;
  Real Uadv = 2.2;
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw,
                PDEClass::BGK, tau,
                PDEClass::AdvectionDiffusion, Uadv ); //area =null; RoeEntropyFix: none (overloaded default)

  // static tests
  BOOST_REQUIRE( pde.N == 3 );

  // function tests

  Real x, time;
  Real f1, f2, f3;

  x = time = 0;   // not actually used in functions
  f1 = 1.137; f2 = 0.784; f3 = 0.987;

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, PrimitiveDistributionFunctionsD1Q3<Real>(f1, f2, f3) );

  if ( false ) //std::is_same<QType,QTypeEntropy>::value )
  {
    // Entropy variables seem to have numerical problems when comparing Surreal and
    // hand differentiated code...
    // Not entropy variables in Boltzmann
    q[0] = -10;
    q[1] = 2;
    q[2] = -3;
  }

  ArrayQSurreal qSurreal = q;

  qSurreal[0].deriv(0) = 1;
  qSurreal[1].deriv(1) = 1;
  qSurreal[2].deriv(2) = 1;

  // conservative flux
  ArrayQSurreal uConsSurreal = 0;
  pde.masterState( x, time, qSurreal, uConsSurreal );

  MatrixQ dudq = 0;
  pde.jacobianMasterState( x, time, q, dudq );

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(0), dudq(0,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(1), dudq(0,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(2), dudq(0,2), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(0), dudq(1,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(1), dudq(1,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(2), dudq(1,2), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(0), dudq(2,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(1), dudq(2,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(2), dudq(2,2), small_tol, close_tol )

  // Should not accumulate
  pde.jacobianMasterState( x, time, q, dudq );

  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(0), dudq(0,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(1), dudq(0,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(2), dudq(0,2), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(0), dudq(1,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(1), dudq(1,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(2), dudq(1,2), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(0), dudq(2,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(1), dudq(2,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(2), dudq(2,2), small_tol, close_tol )
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux_NDSteady, QType, QTypes )
{
  typedef TraitsModelBoltzmannD1Q3<QType, GasModel> TraitsModelBoltzmannD1Q3Class;
  typedef PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> PDENDClass;
  typedef typename PDENDClass::VectorX VectorX;
  typedef typename PDENDClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDENDClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename PDENDClass::template TensorMatrixQ<Real> TensorMatrixQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  Real tau = 1.3;
  Real Uadv = 2.2;
  PDENDClass pdeND(gas, PDEClass::Euler_ResidInterp_Raw,
                PDEClass::BGK, tau,
                PDEClass::AdvectionDiffusion, Uadv ); //area =null; RoeEntropyFix: none (overloaded default)


  // function tests

  VectorX X;
  Real f1, f2, f3;

  X = 0;   // not actually used in functions
  f1 = 1.137; f2 = 0.784; f3 = 0.987;

  // set
  ArrayQ q = 0;
  pdeND.setDOFFrom( q, PrimitiveDistributionFunctionsD1Q3<Real>(f1, f2, f3) );

  // conservative flux
  //ArrayQ uTrue = {rho, rho*u, rho*e0};
  ArrayQ uTrue = {f1, f2, f3};
  ArrayQ uCons = 0;
  pdeND.masterState( X, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // Should not accumulate
  pdeND.masterState( X, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // advective flux
  Real e1 = -1.;
  Real e2 =  0.;
  Real e3 = +1.;
  ArrayQ fTrue = {e1*f1, e2*f2, e3*f3};
  VectorArrayQ F = 0;
  pdeND.fluxAdvective( X, q, F );
  BOOST_CHECK_CLOSE( fTrue(0), F[0](0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), F[0](1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), F[0](2), tol );

  // Flux accumulate
  pdeND.fluxAdvective( X, q, F );
  BOOST_CHECK_CLOSE( 2*fTrue(0), F[0](0), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(1), F[0](1), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(2), F[0](2), tol );

  //Call these simply to make sure coverage is high
  ArrayQ qL, qR, qx, qy, qxL, qxR, qyL, qyR, source;
  VectorArrayQ gradq = 0;
  TensorMatrixQ K;
  pdeND.fluxViscous( X, q, gradq, F );
  pdeND.diffusionViscous( X, q, gradq, K );
  pdeND.source( X, q, gradq, source );
  pdeND.forcingFunction( X, source );

}

// Area isn't probably formulated correctly in Boltzmann. It isn't needed anyway.
#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux_NDSteady_areaChange, QType, QTypes )
{
  typedef TraitsModelBoltzmannD1Q3<QType, GasModel> TraitsModelBoltzmannD1Q3Class;
  typedef PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> PDENDClass;
  typedef typename PDENDClass::VectorX VectorX;
  typedef typename PDENDClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDENDClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename PDENDClass::template TensorMatrixQ<Real> TensorMatrixQ;

  std::shared_ptr<ScalarFunction1D_OffsetLog> area( new ScalarFunction1D_OffsetLog() );

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  Real tau = 1.3;
  Real Uadv = 2.2;
  PDENDClass pdeND(gas, PDEClass::Euler_ResidInterp_Raw,
                PDEClass::BGK, tau,
                PDEClass::AdvectionDiffusion, Uadv, area ); //RoeEntropyFix: none (overloaded default)

  // function tests

  VectorX X;
  Real f1, f2, f3;

  X = 0;   // not actually used in functions
  f1 = 1.137; f2 = 0.784; f3 = 0.987;

  // set
  ArrayQ q = 0;
  pdeND.setDOFFrom( q, PrimitiveDistributionFunctionsD1Q3<Real>(f1, f2, f3) );

  // conservative flux
  //ArrayQ uTrue = {rho, rho*u, rho*e0};
  ArrayQ uTrue = {f1, f2, f3};
  ArrayQ uCons = 0;
  pdeND.masterState( X, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // Should not accumulate
  pdeND.masterState( X, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // advective flux
  Real e1 = -1.;
  Real e2 =  0.;
  Real e3 = +1.;
  VectorArrayQ F = 0;
  ArrayQ fTrue = {e1*f1, e2*f2, e3*f3};
  ArrayQ f = 0;
  pdeND.fluxAdvective( X, q, F );
  BOOST_CHECK_CLOSE( 10.*fTrue(0), F[0](0), tol );
  BOOST_CHECK_CLOSE( 10.*fTrue(1), F[0](1), tol );
  BOOST_CHECK_CLOSE( 10.*fTrue(2), F[0](2), tol );

  // Flux accumulate
  pdeND.fluxAdvective( X, q, F );
  BOOST_CHECK_CLOSE( 2*10.*fTrue(0), F[0](0), tol );
  BOOST_CHECK_CLOSE( 2*10.*fTrue(1), F[0](1), tol );
  BOOST_CHECK_CLOSE( 2*10.*fTrue(2), F[0](2), tol );

  //Call these simply to make sure coverage is high
  ArrayQ qL, qR, qx, qy, qxL, qxR, qyL, qyR, source;
  VectorArrayQ gradq = 0;
  TensorMatrixQ K;
  pdeND.fluxViscous( X, q, gradq, F );
  pdeND.diffusionViscous( X, q, gradq, K );
  pdeND.source( X, q, gradq, source );
  pdeND.forcingFunction( X, source );
}
#endif


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxRoe, QType, QTypes )
{
  typedef TraitsModelBoltzmannD1Q3<QType, GasModel> TraitsModelBoltzmannD1Q3Class;
  typedef PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  Real tau = 1.3;
  Real Uadv = 2.2;
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw,
                PDEClass::BGK, tau,
                PDEClass::AdvectionDiffusion, Uadv ); //area =null; RoeEntropyFix: none (overloaded default)

  // Roe flux function test

  Real x, time;
  Real nx;
  Real f1L, f2L, f3L;
  Real f1R, f2R, f3R;

  x = time = 0;   // not actually used in functions
  nx = 1.0;
  f1L = 1.034; f2L = 3.26; f3L = 5.78;
  f1R = 0.973; f2R = 1.79; f3R = 6.13;

  // set
  ArrayQ qL = pde.setDOFFrom( PrimitiveDistributionFunctionsD1Q3<Real>(f1L, f2L, f3L) );
  ArrayQ qR = pde.setDOFFrom( PrimitiveDistributionFunctionsD1Q3<Real>(f1R, f2R, f3R) );

//  Real e1 = -1.;
//  Real e2 =  0.;
//  Real e3 =  1.;

  // advective normal flux (average)
//  Real fL[3] = {f1L*e1, f2L*e2, f3L*e3};
//  Real fR[3] = {f1R*e1, f2R*e2, f3R*e3};

  // Compute the average normal flux
  ArrayQ fnTrue;
  //for (int k = 0; k < 3; k++)
  //  fnTrue[k] = 0.5*(nx*(fL[k] + fR[k]));
#if 1
  // Exact values for Upwinding ...
  fnTrue[0] =  -0.973;
  fnTrue[1] =  0.;
  fnTrue[2] =  5.78;
#endif

  ArrayQ fn;
  fn = 0;
  pde.fluxAdvectiveUpwind( x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );

  // Flux accumulate
  pde.fluxAdvectiveUpwind( x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(2), fn(2), tol );

  //Call these simply to make sure coverage is high
  ArrayQ qxL, qxR, sourceL, sourceR;
  Real dummy = 0;
  pde.fluxViscous( dummy, dummy, qL, qxL, qR, qxR, nx, fn );
  pde.sourceTrace( dummy, dummy, dummy, qL, qxL, qR, qxR, sourceL, sourceR );
}

// Changing Area not needed/implemented correctly for Boltzmann.
#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxRoe_areaChange, QType, QTypes )
{
  typedef TraitsModelBoltzmannD1Q3<QType, GasModel> TraitsModelBoltzmannD1Q3Class;
  typedef PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  std::shared_ptr<ScalarFunction1D_OffsetLog> area( new ScalarFunction1D_OffsetLog() );

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  Real tau = 1.3;
  Real Uadv = 2.2;
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw,
                PDEClass::BGK, tau,
                PDEClass::AdvectionDiffusion, Uadv, area ); //RoeEntropyFux: none (overloaded default)

  // Roe flux function test

  Real x, time;
  Real nx;
  Real f1L, f2L, f3L;
  Real f1R, f2R, f3R;

  x = time = 0;   // not actually used in functions
  nx = 1.0;
  f1L = 1.034; f2L = 3.26; f3L = 5.78;
  f1R = 0.973; f2R = 1.79; f3R = 6.13;

  // set
  ArrayQ qL = pde.setDOFFrom( PrimitiveDistributionFunctionsD1Q3<Real>(f1L, f2L, f3L) );
  ArrayQ qR = pde.setDOFFrom( PrimitiveDistributionFunctionsD1Q3<Real>(f1R, f2R, f3R) );

  Real e1 = -1.;
  Real e2 =  0.;
  Real e3 =  1.;

  // advective normal flux (average)
  Real fL[3] = {f1L*e1, f2L*e2, f3L*e3};
  Real fR[3] = {f1R*e1, f2R*e2, f3R*e3};

  // Compute the average normal flux
  ArrayQ fnTrue;
  //for (int k = 0; k < 3; k++)
  //  fnTrue[k] = 0.5*(nx*(fL[k] + fR[k]));
#if 1
  // Exact values for Roe scheme
  fnTrue[0] =  -0.973;
  fnTrue[1] =  0.;
  fnTrue[2] =  5.955;
#endif

  ArrayQ fn;
  fn = 0;
  pde.fluxAdvectiveUpwind( x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( 10.*fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 10.*fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 10.*fnTrue(2), fn(2), tol );

  // Flux accumulate
  pde.fluxAdvectiveUpwind( x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( 2*10.*fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 2*10.*fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 2*10.*fnTrue(2), fn(2), tol );

  //Call these simply to make sure coverage is high
  ArrayQ qxL, qxR, sourceL, sourceR;
  Real dummy = 0;
  pde.fluxViscous( dummy, dummy, qL, qxL, qR, qxR, nx, fn );
  pde.sourceTrace( dummy, dummy, dummy, qL, qxL, qR, qxR, sourceL, sourceR );
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( speedCharacteristic, QType, QTypes )
{
  typedef TraitsModelBoltzmannD1Q3<QType, GasModel> TraitsModelBoltzmannD1Q3Class;
  typedef PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  Real tau = 1.3;
  Real Uadv = 2.2;
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw,
                PDEClass::BGK, tau,
                PDEClass::AdvectionDiffusion, Uadv );

  Real x, time, dx;
  Real f1, f2, f3, c, t;
  Real speed, speedTrue;

  x = time = 0;   // not actually used in functions
  dx = 0.57;
  f1 = 1.137; f2 = 0.784; f3 = 0.987;
  const Real csq = 1/3.;
  const Real rho = f1+f2+f3;
  const Real p   = csq*rho;  // Krueger, (Book) LBM principals and practice eq. 7.15, Springer '17

  t = p/(rho * R);
  c = sqrt(gamma*R*t);
  Real u = std::max(c, 1.);     // e1 = 1.
  speedTrue = fabs(dx*u)/sqrt(dx*dx) + c;

  Real qDataPrim[3] = {f1, f2, f3};
  string qNamePrim[3] = {"DistributionFunction0", "DistributionFunction1",  "DistributionFunction2"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );

  pde.speedCharacteristic( x, time, dx, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  pde.speedCharacteristic( x, time, q, speed );
  BOOST_CHECK_CLOSE( fabs(u) + c, speed, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( source, QType, QTypes )
{
  typedef TraitsModelBoltzmannD1Q3<QType, GasModel> TraitsModelBoltzmannD1Q3Class;
  typedef PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  Real tau = 1.3;
  Real Uadv = 2.2;
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw,
                PDEClass::BGK, tau,
                PDEClass::AdvectionDiffusion, Uadv );

  Real x, time;
  Real f1, f2, f3;
  Real feq0, feq1, feq2;
  x = time = 1.0;   // not actually used in functions
  x = time = 0;   // not actually used in functions
  f1 = 1.137; f2 = 0.784; f3 = 4.987;

  Real qDataPrim[3] = {f1, f2, f3};
  string qNamePrim[3] = {"DistributionFunction0", "DistributionFunction1",  "DistributionFunction2"};
  ArrayQ q, qx = 0.0;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );

  const Real csq = 1./3.;
  const Real e1  = -1.;
  const Real e2  =  0.;
  const Real e3  =  1.;
  const Real w1  = 1./6.;
  const Real w2  = 4./6.;
  const Real w3  = 1./6.;

  Real rho = f1 + f2 + f3;

  feq0 = w1*rho*(1+e1*Uadv/csq +(e1*Uadv)*(e1*Uadv)/(2*(csq*csq))+ Uadv*Uadv/(2*csq) );
  feq1 = w2*rho*(1+e2*Uadv/csq +(e2*Uadv)*(e2*Uadv)/(2*(csq*csq))+ Uadv*Uadv/(2*csq) );
  feq2 = w3*rho*(1+e3*Uadv/csq +(e3*Uadv)*(e3*Uadv)/(2*(csq*csq))+ Uadv*Uadv/(2*csq) );

  Real sourcetermTrue[3];
  sourcetermTrue[0] = (1/tau)*(feq0 - f1);
  sourcetermTrue[1] = (1/tau)*(feq1 - f2);
  sourcetermTrue[2] = (1/tau)*(feq2 - f3);


  ArrayQ sourceterm = 0;
  pde.source( x, time, q, qx, sourceterm );
  BOOST_CHECK_CLOSE( sourcetermTrue[0], sourceterm(0), tol );
  BOOST_CHECK_CLOSE( sourcetermTrue[1], sourceterm(1), tol );
  BOOST_CHECK_CLOSE( sourcetermTrue[2], sourceterm(2), tol );
}

// Changing area not needed/correctly implemented in Boltzmann.
#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( source_areaChange, QType, QTypes )
{
  typedef TraitsModelBoltzmannD1Q3<QType, GasModel> TraitsModelBoltzmannD1Q3Class;
  typedef PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  std::shared_ptr<ScalarFunction1D_OffsetLog> area( new ScalarFunction1D_OffsetLog() );

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw, area);

  Real x, time;
  Real rho, u, p, t;

  x = time = 1.0;   // not actually used in functions
  rho = 1.137; u = 0.784; p = 0.987;
  t = p/(R*rho);

  Real qDataPrim[3] = {rho, u, t};
  string qNamePrim[3] = {"Density", "VelocityX",  "Temperature"};
  ArrayQ q, qx = 0.0;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );

  ArrayQ sourceterm = 0;
  pde.source( x, time, q, qx, sourceterm );
  BOOST_CHECK_CLOSE( 0.0, sourceterm(0), tol );
  BOOST_CHECK_CLOSE( -p * 9.0/log(2.0) / (1.0+x), sourceterm(1), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceterm(2), tol );

  // Source accumulate
  pde.source( x, time, q, qx, sourceterm );
  BOOST_CHECK_CLOSE( 2*0.0, sourceterm(0), tol );
  BOOST_CHECK_CLOSE( 2*-p * 9.0/log(2.0) / (1.0+x), sourceterm(1), tol );
  BOOST_CHECK_CLOSE( 2*0.0, sourceterm(2), tol );
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( isValidState, QType, QTypes )
{
  // Surrogates are special
  //if ( std::is_same<QType,QTypePrimitiveSurrogate>::value ) return;

  typedef TraitsModelBoltzmannD1Q3<QType, GasModel> TraitsModelBoltzmannD1Q3Class;
  typedef PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  ArrayQ q;
  Real f1, f2, f3;

  f1 = 1.2;
  f2 = 0.3;
  f3 = 5.3;
  pde.setDOFFrom( q, PrimitiveDistributionFunctionsD1Q3<Real>(f1, f2, f3) );
  BOOST_CHECK( pde.isValidState(q) == true );

  f1 = -1.2;
  f2 = 0.3;
  f3 = 5.3;
  pde.setDOFFrom( q, PrimitiveDistributionFunctionsD1Q3<Real>(f1, f2, f3) );
  BOOST_CHECK( pde.isValidState(q) == false );

  f1 = 1.2;
  f2 = -0.3;
  f3 = 5.3;
  pde.setDOFFrom( q, PrimitiveDistributionFunctionsD1Q3<Real>(f1, f2, f3) );
  BOOST_CHECK( pde.isValidState(q) == false );

  f1 = 1.2;
  f2 = 0.3;
  f3 = -5.3;
  pde.setDOFFrom( q, PrimitiveDistributionFunctionsD1Q3<Real>(f1, f2, f3) );
  BOOST_CHECK( pde.isValidState(q) == false );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO_PrimitiveDistributionFunctions )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/PDEBoltzmannD1Q3_pattern.txt", true );

  typedef TraitsModelBoltzmannD1Q3<QTypePrimitiveDistributionFunctions, GasModel> TraitsModelBoltzmannD1Q3Class;
  typedef PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> PDEClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  pde.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
