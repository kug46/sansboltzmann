// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// GasModel_btest
// testing of GasModel, ViscosityModel, ThermalConductivityModel classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/NS/GasModel.h"         // GasModel, ViscosityModel, ThermalConductivityModel

using namespace std;
using namespace SANS;

namespace SANS
{

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( GasModel_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_copy )
{
  const Real tol = 1.e-13;
  Real gamma, R, Cv, Cp;

  gamma = 1.4;
  R     = 287.04;       // J/(kg K)
  Cv    = R/(gamma - 1);
  Cp    = gamma*Cv;

  PyDict d;
  d[GasModelParams::params.gamma] = gamma;
  d[GasModelParams::params.R] = R;

  //Check that the dict is ok
  GasModelParams::checkInputs(d);

  // constructor
  GasModel gas(gamma, R);
  BOOST_CHECK_CLOSE( gamma, gas.gamma(), tol );
  BOOST_CHECK_CLOSE( R, gas.R(), tol );
  BOOST_CHECK_CLOSE( Cv, gas.Cv(), tol );
  BOOST_CHECK_CLOSE( Cp, gas.Cp(), tol );

  // copy constructor
  GasModel gas2(gas);
  BOOST_CHECK_CLOSE( gamma, gas2.gamma(), tol );
  BOOST_CHECK_CLOSE( R, gas2.R(), tol );
  BOOST_CHECK_CLOSE( Cv, gas2.Cv(), tol );
  BOOST_CHECK_CLOSE( Cp, gas2.Cp(), tol );

  // PyDict constructor
  GasModel gas3(d);
  BOOST_CHECK_CLOSE( gamma, gas3.gamma(), tol );
  BOOST_CHECK_CLOSE( R, gas3.R(), tol );
  BOOST_CHECK_CLOSE( Cv, gas3.Cv(), tol );
  BOOST_CHECK_CLOSE( Cp, gas3.Cp(), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jacobian )
{
  const Real tol = 1.e-13;
  Real gamma, R, Cv, Cp;
  Real rho, rho_p, rho_t;  // density
  Real t, t_rho, t_e;      // temperature
  Real p, p_rho, p_t;      // pressure
  Real e, e_rho, e_t;      // static internal energy
  Real h, h_rho, h_t;      // static enthalpy
  Real c, c_t;             // speed of sound

  gamma = 1.4;
  R     = 287.04;       // J/(kg K)
  Cv    = R/(gamma - 1);
  Cp    = gamma*Cv;

  GasModel gas(gamma, R);

  rho = 1.225;          // kg/m^3 (standard atmosphere)
  t   = 288.15;         // K (standard atmosphere)
  p   = R*rho*t;
  e   = Cv*t;
  h   = Cp*t;
  c   = sqrt(gamma*R*t);

  BOOST_CHECK_CLOSE( rho, gas.density(p, t), tol );
  BOOST_CHECK_CLOSE(   p, gas.pressure(rho, t), tol );
  BOOST_CHECK_CLOSE(   e, gas.energy(rho, t), tol );
  BOOST_CHECK_CLOSE(   h, gas.enthalpy(rho, t), tol );
  BOOST_CHECK_CLOSE(   t, gas.temperature(rho, e), tol );
  BOOST_CHECK_CLOSE(   c, gas.speedofSound(t), tol );

  rho_p = 1/(R*t);
  rho_t = -p/(R*t*t);
  p_rho = R*t;
  p_t   = R*rho;
  e_rho = 0;
  e_t   = Cv;
  h_rho = 0;
  h_t   = Cp;
  t_rho = 0;
  t_e   = 1/Cv;
  c_t   = 0.5*gamma*R/c;

  Real a, b;
  gas.densityJacobian(p, t, a, b);
  BOOST_CHECK_CLOSE( rho_p, a, tol );
  BOOST_CHECK_CLOSE( rho_t, b, tol );

  gas.pressureJacobian(rho, t, a, b);
  BOOST_CHECK_CLOSE( p_rho, a, tol );
  BOOST_CHECK_CLOSE( p_t,   b, tol );

  gas.energyJacobian(rho, t, a, b);
  BOOST_CHECK_CLOSE( e_rho, a, tol );
  BOOST_CHECK_CLOSE( e_t,   b, tol );

  gas.enthalpyJacobian(rho, t, a, b);
  BOOST_CHECK_CLOSE( h_rho, a, tol );
  BOOST_CHECK_CLOSE( h_t,   b, tol );

  gas.temperatureJacobian(rho, t, a, b);
  BOOST_CHECK_CLOSE( t_rho, a, tol );
  BOOST_CHECK_CLOSE( t_e,   b, tol );

  gas.speedofSoundJacobian(t, a);
  BOOST_CHECK_CLOSE( c_t,   a, tol );

  Real rhox, tx;
  Real px, ex, hx, ttx;

  px = 2.1, tx = 1.4;

  gas.densityGradient(p, t, px, tx, rhox);
  BOOST_CHECK_CLOSE( px/(R*t) - tx*p/(R*t*t), rhox, tol );

  rhox = 2.1, tx = 1.4;

  gas.pressureGradient(rho, t, rhox, tx, px);
  BOOST_CHECK_CLOSE( R*rhox*t + R*rho*tx, px, tol );

  gas.energyGradient(rho, t, rhox, tx, ex);
  BOOST_CHECK_CLOSE( Cv*tx, ex, tol );

  gas.enthalpyGradient(rho, t, rhox, tx, hx);
  BOOST_CHECK_CLOSE( Cp*tx, hx, tol );

  gas.temperatureGradient(rho, t, rhox, ex, ttx);
  BOOST_CHECK_CLOSE( ex/Cv, ttx, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/GasModel_pattern.txt", true );

  GasModel gas(1.4, 287.04);

  gas.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()

//############################################################################//
BOOST_AUTO_TEST_SUITE( ViscosityModel_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_copy )
{
  const Real tol = 1.e-13;
  Real muRef, tSuth, tRef;
  Real t, mu;

  muRef = 1.789e-5;     // kg/(m s) (standard atmosphere)
  tSuth = 110;          // K
  tRef = 300;

  t = 288.15;           // K (standard atmosphere)
  mu = muRef * (t/tRef)*sqrt(t/tRef)*(tRef + tSuth)/(t + tSuth);

  // constructor
  ViscosityModel_Sutherland visc(muRef, tRef, tSuth);
  BOOST_CHECK_CLOSE( mu, visc.viscosity(t), tol );

  // copy constructor
  ViscosityModel_Sutherland visc2(visc);
  BOOST_CHECK_CLOSE( mu, visc2.viscosity(t), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jacobian )
{
  Real tol = 1.e-13;
  Real muRef, tSuth, tRef;
  Real t, mu_t;

  muRef = 1.789e-5;     // kg/(m s) (standard atmosphere)
  tRef = 300;
  tSuth = 110;          // K

  t = 288.15;           // K (standard atmosphere)
  mu_t = muRef*( (sqrt(t) + t*(0.5/sqrt(t)))*(tRef + tSuth)/(t + tSuth)/(tRef*sqrt(tRef)) +
                 t*sqrt(t)*(tRef + tSuth)*(-1./((t + tSuth)*(t + tSuth)))/(tRef*sqrt(tRef)) );

  ViscosityModel_Sutherland visc(muRef, tRef, tSuth);

  Real a;
  visc.viscosityJacobian(t, a);
  BOOST_CHECK_CLOSE( mu_t, a, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/ViscosityModel_pattern.txt", true );

  Real muRef = 1.789e-5;     // kg/(m s) (standard atmosphere)
  Real tSuth = 110;          // K
  Real tRef = 300.0;

  ViscosityModel_Sutherland visc(muRef, tRef, tSuth);

  visc.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()

//############################################################################//
BOOST_AUTO_TEST_SUITE( ThermalConductivityModel_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_copy )
{
  const Real tol = 1.e-13;
  Real Prandtl, gamma, R, Cp;
  Real mu, k;

  Prandtl = 0.72;
  gamma = 1.4;
  R     = 287.04;       // J/(kg K)
  Cp    = R*gamma/(gamma - 1);

  mu = 1.789e-5;        // kg/(m s) (standard atmosphere)
  k  = mu*Cp/Prandtl;

  // constructor
  ThermalConductivityModel cond(Prandtl, Cp);
  BOOST_CHECK_CLOSE( k, cond.conductivity(mu), tol );

  // copy constructor
  ThermalConductivityModel cond2(cond);
  BOOST_CHECK_CLOSE( k, cond2.conductivity(mu), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jacobian )
{
  const Real tol = 1.e-13;
  Real Prandtl, gamma, R, Cp;
  Real mu, k_mu;

  Prandtl = 0.72;
  gamma = 1.4;
  R     = 287.04;       // J/(kg K)
  Cp    = R*gamma/(gamma - 1);

  mu = 1.789e-5;        // kg/(m s) (standard atmosphere)
  k_mu = Cp/Prandtl;

  ThermalConductivityModel cond(Prandtl, Cp);

  Real a;
  cond.conductivityJacobian(mu, a);
  BOOST_CHECK_CLOSE( k_mu, a, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/ThermalConductivityModel_pattern.txt", true );

  Real Prandtl, gamma, R, Cp;

  Prandtl = 0.72;
  gamma = 1.4;
  R     = 287.04;       // J/(kg K)
  Cp    = R*gamma/(gamma - 1);

  ThermalConductivityModel cond(Prandtl, Cp);

  cond.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
