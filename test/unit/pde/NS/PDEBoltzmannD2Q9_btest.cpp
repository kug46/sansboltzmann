// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEBoltzmannD1Q3_btest
//
// test Boltzmann D2Q9 PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsBoltzmannD2Q9.h"

#include "pde/NS/QD2Q9PrimitiveDistributionFunctions.h"
#include "pde/NS/PDEBoltzmannD2Q9.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

namespace SANS
{
  class QTypePrimitiveSurrogate {};
  class QTypePrimitiveDistributionFunctions {};
  //Explicitly instantiate the class so coverage information is correct
  template class TraitsSizeBoltzmannD2Q9<PhysD2>;
  template class TraitsModelBoltzmannD2Q9<QTypePrimitiveDistributionFunctions, GasModel>;
  template class PDEBoltzmannD2Q9<TraitsSizeBoltzmannD2Q9, TraitsModelBoltzmannD2Q9<QTypePrimitiveDistributionFunctions, GasModel>>;

}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEBoltzmannD2Q9_test_suite )

typedef boost::mpl::list< QTypePrimitiveDistributionFunctions> QTypes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( static_test, QType, QTypes )
{
  typedef TraitsModelBoltzmannD2Q9<QType, GasModel> TraitsModelBoltzmannD2Q9Class;
  typedef PDEBoltzmannD2Q9<TraitsSizeBoltzmannD2Q9, TraitsModelBoltzmannD2Q9Class> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  BOOST_REQUIRE( PDEClass::D == 2 );
  BOOST_REQUIRE( PDEClass::N == 9 );
  BOOST_REQUIRE( ArrayQ::M == 9 );
  BOOST_REQUIRE( MatrixQ::M == 9 );
  BOOST_REQUIRE( MatrixQ::N == 9 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctor, QType, QTypes )
{
  typedef TraitsModelBoltzmannD2Q9<QType, GasModel> TraitsModelBoltzmannD2Q9Class;
  typedef PDEBoltzmannD2Q9<TraitsSizeBoltzmannD2Q9, TraitsModelBoltzmannD2Q9Class> PDEClass;
  //std::shared_ptr<ScalarFunction1D_OffsetLog> area( new ScalarFunction1D_OffsetLog() );

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( pde.D == 2 );
  BOOST_REQUIRE( pde.N == 9 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  Real tau = 1.3;
  Real Uadv = 2.2;
  Real Vadv = 3.3;
  PDEClass pde2(gas, PDEClass::Euler_ResidInterp_Raw,
                PDEClass::BGK, tau,
                PDEClass::AdvectionDiffusion, Uadv, Vadv); //RoeEntropyFix: none (overloaded default)

  // static tests
  BOOST_REQUIRE( pde2.D == 2 );
  BOOST_REQUIRE( pde2.N == 9 );

  // flux components
  BOOST_CHECK( pde2.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde2.hasFluxAdvective() == true );
  BOOST_CHECK( pde2.hasFluxViscous() == false );
  BOOST_CHECK( pde2.hasSource() == true );
  BOOST_CHECK( pde2.hasForcingFunction() == false );
  BOOST_CHECK( pde2.needsSolutionGradientforSource() == false );
}
#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( set_PrimitiveDistributionFunctions )
{

  typedef TraitsModelBoltzmannD2Q9<QTypePrimitiveDistributionFunctions, GasModel> TraitsModelBoltzmannD2Q9Class;
  typedef PDEBoltzmannD2Q9<TraitsSizeBoltzmannD2Q9, TraitsModelBoltzmannD2Q9Class> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  Real tau = 1.3;
  Real Uadv = 2.2;
  Real Vadv = 1.2;
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw,
                PDEClass::BGK, tau,
                PDEClass::AdvectionDiffusion, Uadv, Vadv ); //area =null; RoeEntropyFix: none (overloaded default)

  Real f0, f1, f2;
  Real f3, f4, f5;
  Real f6, f7, f8;

  f0 = 1.137; f1 = 0.784; f2 = 0.987;
  f3 = 1.137; f4 = 0.784; f5 = 0.987;
  f6 = 1.137; f7 = 0.784; f8 = 0.987;

  PyDict d;

  // set
  Real qDataPrim[9] = {f0, f1, f2, f3, f4, f5, f6, f7, f8};
  string qNamePrim[9] = {"DistributionFunction0", "DistributionFunction1", "DistributionFunction2",
                         "DistributionFunction3", "DistributionFunction4", "DistributionFunction5",
                         "DistributionFunction6", "DistributionFunction7", "DistributionFunction8"};
  ArrayQ qTrue = {f0, f1, f2, f3, f4, f5, f6, f7, f8};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 9 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol);
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol);
  BOOST_CHECK_CLOSE( qTrue(5), q(5), tol);
  BOOST_CHECK_CLOSE( qTrue(6), q(6), tol);
  BOOST_CHECK_CLOSE( qTrue(7), q(7), tol);
  BOOST_CHECK_CLOSE( qTrue(8), q(8), tol);


  PrimitiveDistributionFunctionsD2Q9<Real> qdata1;
  qdata1.DistributionFunction0 = f0;
  qdata1.DistributionFunction1 = f1;
  qdata1.DistributionFunction2 = f2;
  qdata1.DistributionFunction3 = f3;
  qdata1.DistributionFunction4 = f4;
  qdata1.DistributionFunction5 = f5;
  qdata1.DistributionFunction6 = f6;
  qdata1.DistributionFunction7 = f7;
  qdata1.DistributionFunction8 = f8;

  q = 1;
  pde.setDOFFrom( q, qdata1 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol);
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol);
  BOOST_CHECK_CLOSE( qTrue(5), q(5), tol);
  BOOST_CHECK_CLOSE( qTrue(6), q(6), tol);
  BOOST_CHECK_CLOSE( qTrue(7), q(7), tol);
  BOOST_CHECK_CLOSE( qTrue(8), q(8), tol);


  PyDict rhoVP;
  rhoVP[BoltzmannVariableTypeD2Q9Params::params.StateVector.Variables]
        = BoltzmannVariableTypeD2Q9Params::params.StateVector.PrimitiveDistributionFunctions9;
  rhoVP[PrimitiveDistributionFunctionsD2Q9Params::params.pdf0] = f0;
  rhoVP[PrimitiveDistributionFunctionsD2Q9Params::params.pdf1] = f1;
  rhoVP[PrimitiveDistributionFunctionsD2Q9Params::params.pdf2] = f2;
  rhoVP[PrimitiveDistributionFunctionsD2Q9Params::params.pdf3] = f3;
  rhoVP[PrimitiveDistributionFunctionsD2Q9Params::params.pdf4] = f4;
  rhoVP[PrimitiveDistributionFunctionsD2Q9Params::params.pdf5] = f5;
  rhoVP[PrimitiveDistributionFunctionsD2Q9Params::params.pdf6] = f6;
  rhoVP[PrimitiveDistributionFunctionsD2Q9Params::params.pdf7] = f7;
  rhoVP[PrimitiveDistributionFunctionsD2Q9Params::params.pdf8] = f8;

  d[BoltzmannVariableTypeD2Q9Params::params.StateVector] = rhoVP;
  BoltzmannVariableTypeD2Q9Params::checkInputs(d);
  q = pde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol);
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol);
  BOOST_CHECK_CLOSE( qTrue(5), q(5), tol);
  BOOST_CHECK_CLOSE( qTrue(6), q(6), tol);
  BOOST_CHECK_CLOSE( qTrue(7), q(7), tol);
  BOOST_CHECK_CLOSE( qTrue(8), q(8), tol);

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux, QType, QTypes )
{
  typedef TraitsModelBoltzmannD2Q9<QType, GasModel> TraitsModelBoltzmannD2Q9Class;
  typedef PDEBoltzmannD2Q9<TraitsSizeBoltzmannD2Q9, TraitsModelBoltzmannD2Q9Class> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  Real tau = 1.3;
  Real Uadv = 2.2;
  Real Vadv = 2.2;
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw,
                PDEClass::BGK, tau,
                PDEClass::AdvectionDiffusion, Uadv, Vadv ); //area =null; RoeEntropyFix: none (overloaded default)

  // static tests
  BOOST_REQUIRE( pde.N == 9 );

  // function tests

  Real x, y, time;
  x = y = time = 0;   // not actually used in functions

  Real f0, f1, f2;
  Real f3, f4, f5;
  Real f6, f7, f8;

  f0 = 1.137; f1 = 0.784; f2 = 0.987;
  f3 = 1.137; f4 = 0.784; f5 = 0.987;
  f6 = 1.137; f7 = 0.784; f8 = 0.987;

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, PrimitiveDistributionFunctionsD2Q9<Real>(f0, f1, f2,
                                                              f3, f4, f5,
                                                              f6, f7, f8) );

  // conservative flux
  //ArrayQ uTrue = {rho, rho*u, rho*e0};
  ArrayQ uTrue = {f0, f1, f2, f3, f4, f5, f6, f7, f8};
  ArrayQ uCons = 0;
  pde.masterState( x, y, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );
  BOOST_CHECK_CLOSE( uTrue(3), uCons(3), tol);
  BOOST_CHECK_CLOSE( uTrue(4), uCons(4), tol);
  BOOST_CHECK_CLOSE( uTrue(5), uCons(5), tol);
  BOOST_CHECK_CLOSE( uTrue(6), uCons(6), tol);
  BOOST_CHECK_CLOSE( uTrue(7), uCons(7), tol);
  BOOST_CHECK_CLOSE( uTrue(8), uCons(8), tol);

  // Should not accumulate
  pde.masterState( x, y, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );
  BOOST_CHECK_CLOSE( uTrue(3), uCons(3), tol);
  BOOST_CHECK_CLOSE( uTrue(4), uCons(4), tol);
  BOOST_CHECK_CLOSE( uTrue(5), uCons(5), tol);
  BOOST_CHECK_CLOSE( uTrue(6), uCons(6), tol);
  BOOST_CHECK_CLOSE( uTrue(7), uCons(7), tol);
  BOOST_CHECK_CLOSE( uTrue(8), uCons(8), tol);

  // conservative flux
  ArrayQ ftTrue = {f0, f1, f2, f3, f4, f5, f6, f7, f8};
  ArrayQ ft = 0;
  pde.fluxAdvectiveTime( x, y, time, q, ft );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( ftTrue(3), ft(3), tol);
  BOOST_CHECK_CLOSE( ftTrue(4), ft(4), tol);
  BOOST_CHECK_CLOSE( ftTrue(5), ft(5), tol);
  BOOST_CHECK_CLOSE( ftTrue(6), ft(6), tol);
  BOOST_CHECK_CLOSE( ftTrue(7), ft(7), tol);
  BOOST_CHECK_CLOSE( ftTrue(8), ft(8), tol);

  // Flux accumulate
  pde.fluxAdvectiveTime( x, y, time, q, ft );
  BOOST_CHECK_CLOSE( 2*ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(3), ft(3), tol);
  BOOST_CHECK_CLOSE( 2*ftTrue(4), ft(4), tol);
  BOOST_CHECK_CLOSE( 2*ftTrue(5), ft(5), tol);
  BOOST_CHECK_CLOSE( 2*ftTrue(6), ft(6), tol);
  BOOST_CHECK_CLOSE( 2*ftTrue(7), ft(7), tol);
  BOOST_CHECK_CLOSE( 2*ftTrue(8), ft(8), tol);

  // advective flux
  const int e[9][2] = {   {  0,  0 }, {  1,  0 }, {  0,  1 },
                          { -1,  0 }, {  0, -1 }, {  1,  1 },
                          { -1,  1 }, { -1, -1 }, {  1, -1 }    };
  ArrayQ fTrue = {e[0][0]*f0, e[1][0]*f1, e[2][0]*f2,
                  e[3][0]*f3, e[4][0]*f4, e[5][0]*f5,
                  e[6][0]*f6, e[7][0]*f7, e[8][0]*f8};
  ArrayQ gTrue = {e[0][1]*f0, e[1][1]*f1, e[2][1]*f2,
                  e[3][1]*f3, e[4][1]*f4, e[5][1]*f5,
                  e[6][1]*f6, e[7][1]*f7, e[8][1]*f8};
  ArrayQ f = 0;
  ArrayQ g = 0;
  pde.fluxAdvective( x, y, time, q, f, g );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol);
  BOOST_CHECK_CLOSE( fTrue(4), f(4), tol);
  BOOST_CHECK_CLOSE( fTrue(5), f(5), tol);
  BOOST_CHECK_CLOSE( fTrue(6), f(6), tol);
  BOOST_CHECK_CLOSE( fTrue(7), f(7), tol);
  BOOST_CHECK_CLOSE( fTrue(8), f(8), tol);
  BOOST_CHECK_CLOSE( gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( gTrue(1), g(1), tol );
  BOOST_CHECK_CLOSE( gTrue(2), g(2), tol );
  BOOST_CHECK_CLOSE( gTrue(3), g(3), tol);
  BOOST_CHECK_CLOSE( gTrue(4), g(4), tol);
  BOOST_CHECK_CLOSE( gTrue(5), g(5), tol);
  BOOST_CHECK_CLOSE( gTrue(6), g(6), tol);
  BOOST_CHECK_CLOSE( gTrue(7), g(7), tol);
  BOOST_CHECK_CLOSE( gTrue(8), g(8), tol);

  // Flux accumulate
  pde.fluxAdvective( x, y, time, q, f, g );
  BOOST_CHECK_CLOSE( 2*fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(3), f(3), tol);
  BOOST_CHECK_CLOSE( 2*fTrue(4), f(4), tol);
  BOOST_CHECK_CLOSE( 2*fTrue(5), f(5), tol);
  BOOST_CHECK_CLOSE( 2*fTrue(6), f(6), tol);
  BOOST_CHECK_CLOSE( 2*fTrue(7), f(7), tol);
  BOOST_CHECK_CLOSE( 2*fTrue(8), f(8), tol);
  BOOST_CHECK_CLOSE( 2*gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( 2*gTrue(1), g(1), tol );
  BOOST_CHECK_CLOSE( 2*gTrue(2), g(2), tol );
  BOOST_CHECK_CLOSE( 2*gTrue(3), g(3), tol);
  BOOST_CHECK_CLOSE( 2*gTrue(4), g(4), tol);
  BOOST_CHECK_CLOSE( 2*gTrue(5), g(5), tol);
  BOOST_CHECK_CLOSE( 2*gTrue(6), g(6), tol);
  BOOST_CHECK_CLOSE( 2*gTrue(7), g(7), tol);
  BOOST_CHECK_CLOSE( 2*gTrue(8), g(8), tol);

  // Call these simply to make sure coverage is high
  ArrayQ qL, qR, qx, qy, qxL, qxR, qyL, qyR, source;
  Real dummy = 0;
  MatrixQ kxx, kxy, kyx, kyy;
  pde.fluxViscous( dummy, dummy, dummy, q, qx, qy, f, g );
  pde.diffusionViscous( dummy, dummy, dummy, q, qx, qy, kxx, kxy, kyx, kyy );
  pde.source( dummy, dummy, dummy, q, qx, qy, source );
  //pde.forcingFunction( x, y, dummy, source );

  ArrayQ fn = 0, sourceL = 0 , sourceR = 0;
  Real nx=0, ny=0;
  pde.fluxViscous( dummy, dummy, dummy, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );
  pde.sourceTrace( dummy, dummy, dummy, dummy, dummy, qL, qxL, qyL, qR, qxR, qyR, sourceL, sourceR );
}

#endif

// TODO: Further testing is needed

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO_PrimitiveDistributionFunctions )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/PDEBoltzmannD2Q9_pattern.txt", true );

  typedef TraitsModelBoltzmannD2Q9<QTypePrimitiveDistributionFunctions, GasModel> TraitsModelBoltzmannD2Q9Class;
  typedef PDEBoltzmannD2Q9<TraitsSizeBoltzmannD2Q9, TraitsModelBoltzmannD2Q9Class> PDEClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  pde.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
