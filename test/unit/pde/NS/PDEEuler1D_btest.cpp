// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEEuler1D_btest
//
// test of quasi 1-D compressible Euler PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/Q1DPrimitiveSurrogate.h"
#include "pde/NS/Q1DConservative.h"
#include "pde/NS/Q1DEntropy.h"
#include "pde/NS/PDEEuler1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"

namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
class QTypeEntropy {};

//Explicitly instantiate the class so coverage information is correct
template class TraitsSizeEuler<PhysD1>;
template class TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>;
template class TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>;
template class TraitsModelEuler<QTypeConservative, GasModel>;
template class TraitsModelEuler<QTypeEntropy, GasModel>;
template class PDEEuler1D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>>;
template class PDEEuler1D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>>;
template class PDEEuler1D<TraitsSizeEuler, TraitsModelEuler<QTypeConservative, GasModel>>;
template class PDEEuler1D<TraitsSizeEuler, TraitsModelEuler<QTypeEntropy, GasModel>>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEEuler1D_test_suite )

typedef boost::mpl::list< QTypePrimitiveRhoPressure,
                          QTypeConservative,
                          QTypePrimitiveSurrogate,
                          QTypeEntropy > QTypes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( static_test, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  BOOST_REQUIRE( PDEClass::D == 1 );
  BOOST_REQUIRE( PDEClass::N == 3 );
  BOOST_REQUIRE( ArrayQ::M == 3 );
  BOOST_REQUIRE( MatrixQ::M == 3 );
  BOOST_REQUIRE( MatrixQ::N == 3 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctor, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  std::shared_ptr<ScalarFunction1D_OffsetLog> area( new ScalarFunction1D_OffsetLog() );

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( pde.D == 1 );
  BOOST_REQUIRE( pde.N == 3 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  PDEClass pde2(gas, PDEClass::Euler_ResidInterp_Raw, area);

  // static tests
  BOOST_REQUIRE( pde2.D == 1 );
  BOOST_REQUIRE( pde2.N == 3 );

  // flux components
  BOOST_CHECK( pde2.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde2.hasFluxAdvective() == true );
  BOOST_CHECK( pde2.hasFluxViscous() == false );
  BOOST_CHECK( pde2.hasSource() == true );
  BOOST_CHECK( pde2.hasForcingFunction() == false );
  BOOST_CHECK( pde2.needsSolutionGradientforSource() == false );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( set_RhoPressure )
{
  typedef TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  Real rho, u, t, p, E;

  rho = 1.137; u = 0.784; t = 0.987;
  p  = R*rho*t;
  E = gas.energy(rho,t) + 0.5*u*u;

  PyDict d;

  // set
  Real qDataPrim[3] = {rho, u, t};
  string qNamePrim[3] = {"Density", "VelocityX", "Temperature"};
  ArrayQ qTrue = {rho, u, p};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );

  DensityVelocityPressure1D<Real> qdata1;
  qdata1.Density = rho; qdata1.VelocityX = u; qdata1.Pressure = p;
  q = 1;
  pde.setDOFFrom( q, qdata1 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );

  PyDict rhoVP;
  rhoVP[NSVariableType1DParams::params.StateVector.Variables] = NSVariableType1DParams::params.StateVector.PrimitivePressure;
  rhoVP[DensityVelocityPressure1DParams::params.rho] = rho;
  rhoVP[DensityVelocityPressure1DParams::params.u] = u;
  rhoVP[DensityVelocityPressure1DParams::params.p] = p;

  d[NSVariableType1DParams::params.StateVector] = rhoVP;
  NSVariableType1DParams::checkInputs(d);
  q = pde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );

  DensityVelocityTemperature1D<Real> qdata2(rho, u, t);
  q = 1;

  pde.setDOFFrom( q, qdata2 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );

  PyDict rhoVT;
  rhoVT[NSVariableType1DParams::params.StateVector.Variables] = NSVariableType1DParams::params.StateVector.PrimitiveTemperature;
  rhoVT[DensityVelocityTemperature1DParams::params.rho] = rho;
  rhoVT[DensityVelocityTemperature1DParams::params.u] = u;
  rhoVT[DensityVelocityTemperature1DParams::params.t] = t;

  d[NSVariableType1DParams::params.StateVector] = rhoVT;
  NSVariableType1DParams::checkInputs(d);
  q = pde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );


  Conservative1D<Real> qdata3(rho, rho*u, rho*E);
  q = 0;
  pde.setDOFFrom( q, qdata3 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );

  PyDict Conservative;
  Conservative[NSVariableType1DParams::params.StateVector.Variables] = NSVariableType1DParams::params.StateVector.Conservative;
  Conservative[Conservative1DParams::params.rho] = rho;
  Conservative[Conservative1DParams::params.rhou] = rho*u;
  Conservative[Conservative1DParams::params.rhoE] = rho*E;

  d[NSVariableType1DParams::params.StateVector] = Conservative;
  NSVariableType1DParams::checkInputs(d);
  q = pde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( pde.N == 3 );

  // function tests

  Real x, time;
  Real rho, u, t, p, e0, h0;
  Real Cv, Cp;

  x = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u);
  h0 = Cp*t + 0.5*(u*u);

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );

  // conservative flux
  ArrayQ uTrue = {rho, rho*u, rho*e0};
  ArrayQ uCons = 0;
  pde.masterState( x, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // Should not accumulate
  pde.masterState( x, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // conservative flux
  ArrayQ ftTrue = {rho, rho*u, rho*e0};
  ArrayQ ft = 0;
  pde.fluxAdvectiveTime( x, time, q, ft );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );

  // Flux accumulate
  pde.fluxAdvectiveTime( x, time, q, ft );
  BOOST_CHECK_CLOSE( 2*ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(2), ft(2), tol );

  // advective flux
  ArrayQ fTrue = {rho*u, rho*u*u + p, rho*u*h0};
  ArrayQ f = 0;
  pde.fluxAdvective( x, time, q, f );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );

  // Flux accumulate
  pde.fluxAdvective( x, time, q, f );
  BOOST_CHECK_CLOSE( 2*fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(2), f(2), tol );

  //Call these simply to make sure coverage is high
  ArrayQ qx = 0, qt = 0, source = 0;
  ArrayQ qp = 0, qpx = 0;
  Real dummy = 0;

  MatrixQ kxx = 0, kxt = 0, ktx = 0, ktt = 0;
  ArrayQ g = 0;
  pde.fluxViscous( dummy, dummy, q, qx, f );
  pde.fluxViscousSpaceTime( dummy, dummy, q, qx, qt, f, g );
  pde.diffusionViscous( dummy, dummy, q, qx, kxx );
  pde.diffusionViscousSpaceTime( dummy, dummy, q, qx, qt, kxx, kxt, ktx, ktt );
  pde.source( dummy, dummy, q, qx, source );
  pde.source( dummy, dummy, q, qp, qx, qpx, source );
  pde.forcingFunction( x, dummy, source );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux_areaChange, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  std::shared_ptr<ScalarFunction1D_OffsetLog> area( new ScalarFunction1D_OffsetLog() );

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw, area);

  // static tests
  BOOST_REQUIRE( pde.N == 3 );

  // function tests

  Real x, time;
  Real rho, u, t, p, e0, h0;
  Real Cv, Cp;

  x = time = 1.0;   // not actually used in functions
  rho = 1.137; u = 0.784; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u);
  h0 = Cp*t + 0.5*(u*u);

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );

  // conservative flux
  ArrayQ uTrue = {rho, rho*u, rho*e0};
  ArrayQ uCons = 0;
  pde.masterState( x, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // Should not accumulate
  pde.masterState( x, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // advective flux
  ArrayQ fTrue = {rho*u, rho*u*u + p, rho*u*h0};
  ArrayQ f = 0;
  pde.fluxAdvective( x, time, q, f );
  BOOST_CHECK_CLOSE( 10.*fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( 10.*fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( 10.*fTrue(2), f(2), tol );

  // Flux accumulate
  pde.fluxAdvective( x, time, q, f );
  BOOST_CHECK_CLOSE( 2*10.*fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( 2*10.*fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( 2*10.*fTrue(2), f(2), tol );

  //Call these simply to make sure coverage is high
  ArrayQ qL, qR, qx, qy, qxL, qxR, qyL, qyR, source;
  Real dummy = 0;

  MatrixQ kxx;
  pde.fluxViscous( dummy, dummy, q, qx, f );
  pde.diffusionViscous( dummy, dummy, q, qx, kxx );
  pde.source( dummy, dummy, q, qx, source );
  pde.forcingFunction( x, dummy, source );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( jacobianMasterState, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename PDEClass::template ArrayQ<SurrealS<PDEClass::N>> ArrayQSurreal;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( pde.N == 3 );

  // function tests

  Real rho, u, t;
  Real x = 0, time = 0;

  rho = 1.137; u = 0.784; t = 0.987;

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );

  if ( std::is_same<QType,QTypeEntropy>::value )
  {
    // Entropy variables seem to have numerical problems when comparing Surreal and
    // hand differentiated code...
    q[0] = -10;
    q[1] = 2;
    q[2] = -3;
  }

  ArrayQSurreal qSurreal = q;

  qSurreal[0].deriv(0) = 1;
  qSurreal[1].deriv(1) = 1;
  qSurreal[2].deriv(2) = 1;

  // conservative flux
  ArrayQSurreal uConsSurreal = 0;
  pde.masterState( x, time, qSurreal, uConsSurreal );

  MatrixQ dudq = 0;
  pde.jacobianMasterState( x, time, q, dudq );

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(0), dudq(0,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(1), dudq(0,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(2), dudq(0,2), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(0), dudq(1,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(1), dudq(1,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(2), dudq(1,2), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(0), dudq(2,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(1), dudq(2,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(2), dudq(2,2), small_tol, close_tol )

  // Should not accumulate
  pde.jacobianMasterState( x, time, q, dudq );

  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(0), dudq(0,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(1), dudq(0,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(2), dudq(0,2), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(0), dudq(1,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(1), dudq(1,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(2), dudq(1,2), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(0), dudq(2,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(1), dudq(2,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(2), dudq(2,2), small_tol, close_tol )
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux_NDSteady, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> PDENDClass;
  typedef typename PDENDClass::VectorX VectorX;
  typedef typename PDENDClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDENDClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename PDENDClass::template TensorMatrixQ<Real> TensorMatrixQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDENDClass pdeND(gas, PDEClass::Euler_ResidInterp_Raw);

  // function tests

  VectorX X;
  Real rho, u, t, p, e0, h0;
  Real Cv, Cp;

  X = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u);
  h0 = Cp*t + 0.5*(u*u);

  // set
  ArrayQ q;

  pdeND.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );

  // conservative flux
  ArrayQ uTrue = {rho, rho*u, rho*e0};
  ArrayQ uCons = 0;
  pdeND.masterState( X, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // Should not accumulate
  pdeND.masterState( X, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // advective flux
  VectorArrayQ F = 0;
  ArrayQ fTrue = {rho*u, rho*u*u + p, rho*u*h0};
  ArrayQ f = 0;
  pdeND.fluxAdvective( X, q, F );
  BOOST_CHECK_CLOSE( fTrue(0), F[0](0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), F[0](1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), F[0](2), tol );

  // Flux accumulate
  pdeND.fluxAdvective( X, q, F );
  BOOST_CHECK_CLOSE( 2*fTrue(0), F[0](0), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(1), F[0](1), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(2), F[0](2), tol );

  //Call these simply to make sure coverage is high
  ArrayQ qp = 0, source = 0;
  VectorArrayQ gradq = 0, gradqp = 0;
  TensorMatrixQ K;
  pdeND.fluxViscous( X, q, gradq, F );
  pdeND.diffusionViscous( X, q, gradq, K );
  pdeND.source( X, q, gradq, source );
  pdeND.source( X, q, qp, gradq, gradqp, source );
  pdeND.forcingFunction( X, source );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux_NDSteady_areaChange, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> PDENDClass;
  typedef typename PDENDClass::VectorX VectorX;
  typedef typename PDENDClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDENDClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename PDENDClass::template TensorMatrixQ<Real> TensorMatrixQ;

  std::shared_ptr<ScalarFunction1D_OffsetLog> area( new ScalarFunction1D_OffsetLog() );

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDENDClass pdeND(gas, PDEClass::Euler_ResidInterp_Raw, area);

  // function tests

  VectorX X;
  Real rho, u, t, p, e0, h0;
  Real Cv, Cp;

  X = 1.0;
  rho = 1.137; u = 0.784; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u);
  h0 = Cp*t + 0.5*(u*u);

  // set
  ArrayQ q;

  pdeND.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );

  // conservative flux
  ArrayQ uTrue = {rho, rho*u, rho*e0};
  ArrayQ uCons = 0;
  pdeND.masterState( X, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // Should not accumulate
  pdeND.masterState( X, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // advective flux
  VectorArrayQ F = 0;
  ArrayQ fTrue = {rho*u, rho*u*u + p, rho*u*h0};
  ArrayQ f = 0;
  pdeND.fluxAdvective( X, q, F );
  BOOST_CHECK_CLOSE( 10.*fTrue(0), F[0](0), tol );
  BOOST_CHECK_CLOSE( 10.*fTrue(1), F[0](1), tol );
  BOOST_CHECK_CLOSE( 10.*fTrue(2), F[0](2), tol );

  // Flux accumulate
  pdeND.fluxAdvective( X, q, F );
  BOOST_CHECK_CLOSE( 2*10.*fTrue(0), F[0](0), tol );
  BOOST_CHECK_CLOSE( 2*10.*fTrue(1), F[0](1), tol );
  BOOST_CHECK_CLOSE( 2*10.*fTrue(2), F[0](2), tol );

  //Call these simply to make sure coverage is high
  ArrayQ qp = 0, source = 0;
  VectorArrayQ gradq = 0, gradqp = 0;
  TensorMatrixQ K;
  pdeND.fluxViscous( X, q, gradq, F );
  pdeND.diffusionViscous( X, q, gradq, K );
  pdeND.source( X, q, gradq, source );
  pdeND.source( X, q, qp, gradq, gradqp, source );
  pdeND.forcingFunction( X, source );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxRoe, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  // Roe flux function test

  Real x, time;
  Real nx;
  Real rhoL, uL, tL, pL, h0L;
  Real rhoR, uR, tR, pR, h0R;
  Real Cp;

  x = time = 0;   // not actually used in functions
  nx = 1.0;
  rhoL = 1.034; uL = 3.26; tL = 5.78;
  rhoR = 0.973; uR = 1.79; tR = 6.13;
  Cp  = R*gamma/(gamma - 1);
  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL);
  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR);

  // set
  ArrayQ qL = pde.setDOFFrom( DensityVelocityTemperature1D<Real>(rhoL, uL, tL) );
  ArrayQ qR = pde.setDOFFrom( DensityVelocityTemperature1D<Real>(rhoR, uR, tR) );

  // advective normal flux (average)
  Real fL[3] = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*h0L};
  Real fR[3] = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*h0R};

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < 3; k++)
    fnTrue[k] = 0.5*(nx*(fL[k] + fR[k]));
#if 1
  // Exact values for Roe scheme
  fnTrue[0] -= -0.63318276027140017468491006399040;
  fnTrue[1] -=  -3.8160625416529803007039694937525;
  fnTrue[2] -=  -12.431949286034470439767789109435;
#endif

  ArrayQ fn;
  fn = 0;
  pde.fluxAdvectiveUpwind( x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );

  // Flux accumulate
  pde.fluxAdvectiveUpwind( x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(2), fn(2), tol );

  //Call these simply to make sure coverage is high
  ArrayQ qxL, qxR, sourceL, sourceR;
  Real dummy = 0;
  pde.fluxViscous( dummy, dummy, qL, qxL, qR, qxR, nx, fn );
  pde.sourceTrace( dummy, dummy, dummy, qL, qxL, qR, qxR, sourceL, sourceR );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxRoe_areaChange, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  std::shared_ptr<ScalarFunction1D_OffsetLog> area( new ScalarFunction1D_OffsetLog() );

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw, area);

  // Roe flux function test

  Real x, time;
  Real nx;
  Real rhoL, uL, tL, pL, h0L;
  Real rhoR, uR, tR, pR, h0R;
  Real Cp;

  x = time = 1.0;
  nx = 1.0;
  rhoL = 1.034; uL = 3.26; tL = 5.78;
  rhoR = 0.973; uR = 1.79; tR = 6.13;
  Cp  = R*gamma/(gamma - 1);
  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL);
  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR);

  // set
  ArrayQ qL = pde.setDOFFrom( DensityVelocityTemperature1D<Real>(rhoL, uL, tL) );
  ArrayQ qR = pde.setDOFFrom( DensityVelocityTemperature1D<Real>(rhoR, uR, tR) );

  // advective normal flux (average)
  Real fL[3] = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*h0L};
  Real fR[3] = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*h0R};

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < 3; k++)
    fnTrue[k] = 0.5*(nx*(fL[k] + fR[k]));
#if 1
  // Exact values for Roe scheme
  fnTrue[0] -= -0.63318276027140017468491006399040;
  fnTrue[1] -=  -3.8160625416529803007039694937525;
  fnTrue[2] -=  -12.431949286034470439767789109435;
#endif

  ArrayQ fn;
  fn = 0;
  pde.fluxAdvectiveUpwind( x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( 10.*fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 10.*fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 10.*fnTrue(2), fn(2), tol );

  // Flux accumulate
  pde.fluxAdvectiveUpwind( x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( 2*10.*fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 2*10.*fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 2*10.*fnTrue(2), fn(2), tol );

  //Call these simply to make sure coverage is high
  ArrayQ qxL, qxR, sourceL, sourceR;
  Real dummy = 0;
  pde.fluxViscous( dummy, dummy, qL, qxL, qR, qxR, nx, fn );
  pde.sourceTrace( dummy, dummy, dummy, qL, qxL, qR, qxR, sourceL, sourceR );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( speedCharacteristic, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  Real x, time, dx;
  Real rho, u, t, c;
  Real speed, speedTrue;

  x = time = 0;   // not actually used in functions
  dx = 0.57;
  rho = 1.137; u = 0.784; t = 0.987;
  c = sqrt(gamma*R*t);
  speedTrue = fabs(dx*u)/sqrt(dx*dx) + c;

  Real qDataPrim[3] = {rho, u, t};
  string qNamePrim[3] = {"Density", "VelocityX",  "Temperature"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );

  pde.speedCharacteristic( x, time, dx, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  pde.speedCharacteristic( x, time, q, speed );
  BOOST_CHECK_CLOSE( fabs(u) + c, speed, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( source, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  Real x, time;
  Real rho, u, p, t;
  x = time = 1.0;   // not actually used in functions
  rho = 1.137; u = 0.784; p = 0.987;
  t = p/(R*rho);

  Real qDataPrim[3] = {rho, u, t};
  string qNamePrim[3] = {"Density", "VelocityX",  "Temperature"};
  ArrayQ q, qx = 0.0;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );

  ArrayQ sourceterm = 0;
  pde.source( x, time, q, qx, sourceterm );
  BOOST_CHECK_CLOSE( 0.0, sourceterm(0), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceterm(1), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceterm(2), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( source_areaChange, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  std::shared_ptr<ScalarFunction1D_OffsetLog> area( new ScalarFunction1D_OffsetLog() );

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw, area);

  Real x, time;
  Real rho, u, p, t;

  x = time = 1.0;   // not actually used in functions
  rho = 1.137; u = 0.784; p = 0.987;
  t = p/(R*rho);

  Real qDataPrim[3] = {rho, u, t};
  string qNamePrim[3] = {"Density", "VelocityX",  "Temperature"};
  ArrayQ q, qx = 0.0;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );

  ArrayQ sourceterm = 0;
  pde.source( x, time, q, qx, sourceterm );
  BOOST_CHECK_CLOSE( 0.0, sourceterm(0), tol );
  BOOST_CHECK_CLOSE( -p * 9.0/log(2.0) / (1.0+x), sourceterm(1), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceterm(2), tol );

  // Source accumulate
  pde.source( x, time, q, qx, sourceterm );
  BOOST_CHECK_CLOSE( 2*0.0, sourceterm(0), tol );
  BOOST_CHECK_CLOSE( 2*-p * 9.0/log(2.0) / (1.0+x), sourceterm(1), tol );
  BOOST_CHECK_CLOSE( 2*0.0, sourceterm(2), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( isValidState, QType, QTypes )
{
  // Surrogates are special
  // Entropy has log( x < 0 ) when constructing invalid states
  if ( std::is_same<QType,QTypePrimitiveSurrogate>::value ) return;
  if ( std::is_same<QType,QTypeEntropy>::value ) return;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  ArrayQ q;
  Real rho, u = 0, t;

  rho = 1;
  t = 1;
  pde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( pde.isValidState(q) == true );

  rho = 1;
  t = -1;
  pde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( pde.isValidState(q) == false );

  rho = -1;
  t = 1;
  pde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( pde.isValidState(q) == false );

  rho = -1;
  t = -1;
  pde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( pde.isValidState(q) == false );

  rho = 1;
  t = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( pde.isValidState(q) == false );

  rho = 0;
  t = 1;
  pde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( pde.isValidState(q) == false );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState_surrogate )
{
  typedef TraitsModelEuler<QTypePrimitiveSurrogate, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  ArrayQ q;
  Real rho, u = 0, t;

  rho = 1;
  t = 1;
  pde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( pde.isValidState(q) == true );

  rho = 1;
  t = -1;
  pde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( pde.isValidState(q) == true );

  rho = -1;
  t = 1;
  pde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( pde.isValidState(q) == true );

  rho = -1;
  t = -1;
  pde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( pde.isValidState(q) == true );

  rho = 1;
  t = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( pde.isValidState(q) == true );

  rho = 0;
  t = 1;
  pde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( pde.isValidState(q) == true );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO_rhoPressure )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/PDEEuler1D_pattern.txt", true );

  typedef TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  pde.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO_surrogate )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/PDEEuler1DSurrogate_pattern.txt", true );

  typedef TraitsModelEuler<QTypePrimitiveSurrogate, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);

  pde.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
