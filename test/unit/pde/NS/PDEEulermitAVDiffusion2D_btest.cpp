// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEEulermitAVDiffusion2D_btest
//
// test of 2-D compressible Euler PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/PDEEulermitAVDiffusion2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"

namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
class QTypeEntropy {};

// Explicitly instantiate the class so coverage information is correct
template class TraitsSizeEuler<PhysD2>;
template class TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>;
template class TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>;
template class TraitsModelEuler<QTypeConservative, GasModel>;
template class TraitsModelEuler<QTypeEntropy, GasModel>;
template class PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>>;
template class PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>>;
template class PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEuler<QTypeConservative, GasModel>>;
template class PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEuler<QTypeEntropy, GasModel>>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEEulermitAVDiffusion2D_test_suite )

typedef boost::mpl::list< QTypePrimitiveRhoPressure,
                          QTypeConservative,
                          QTypePrimitiveSurrogate,
                          QTypeEntropy > QTypes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( static_test, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  BOOST_REQUIRE( AVPDEClass::D == 2 );
  BOOST_REQUIRE( AVPDEClass::N == 4 );
  BOOST_REQUIRE( ArrayQ::M == 4 );
  BOOST_REQUIRE( MatrixQ::M == 4 );
  BOOST_REQUIRE( MatrixQ::N == 4 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctor, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( avpde.D == 2 );
  BOOST_REQUIRE( avpde.N == 4 );

  // flux components
  BOOST_CHECK( avpde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( avpde.hasFluxAdvective() == true );
  BOOST_CHECK( avpde.hasFluxViscous() == true );
  BOOST_CHECK( avpde.hasSource() == false );
  BOOST_CHECK( avpde.hasSourceTrace() == false );
  BOOST_CHECK( avpde.hasForcingFunction() == false );

  BOOST_CHECK( avpde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( avpde.needsSolutionGradientforSource() == false );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setDOFFrom )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + sensor

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  Real rho = 1.137;
  Real u = 0.784;
  Real v = -0.231;
  Real t = 0.987;
  Real p = R*rho*t;
  Real E = gas.energy(rho,t) + 0.5*(u*u + v*v);

  Real sensor = 0.15;

  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType paramL(logH, sensor); // grid spacing and sensor values

  PyDict d;

  // set
  Real qDataPrim[4] = {rho, u, v, t};
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY", "Temperature"};
  ArrayQ qTrue = {rho, u, v, p};
  ArrayQ q;
  avpde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );

  DensityVelocityPressure2D<Real> qdata1;
  qdata1.Density = rho; qdata1.VelocityX = u; qdata1.VelocityY = v; qdata1.Pressure = p;
  q = 0;
  avpde.setDOFFrom( q, qdata1 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );

  PyDict rhoVP;
  rhoVP[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.PrimitivePressure;
  rhoVP[DensityVelocityPressure2DParams::params.rho] = rho;
  rhoVP[DensityVelocityPressure2DParams::params.u] = u;
  rhoVP[DensityVelocityPressure2DParams::params.v] = v;
  rhoVP[DensityVelocityPressure2DParams::params.p] = p;

  d[NSVariableType2DParams::params.StateVector] = rhoVP;
  NSVariableType2DParams::checkInputs(d);
  q = avpde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );


  DensityVelocityTemperature2D<Real> qdata2(rho, u, v, t);
  q = 0;
  avpde.setDOFFrom( q, qdata2 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );

  PyDict rhoVT;
  rhoVT[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.PrimitiveTemperature;
  rhoVT[DensityVelocityTemperature2DParams::params.rho] = rho;
  rhoVT[DensityVelocityTemperature2DParams::params.u] = u;
  rhoVT[DensityVelocityTemperature2DParams::params.v] = v;
  rhoVT[DensityVelocityTemperature2DParams::params.t] = t;

  d[NSVariableType2DParams::params.StateVector] = rhoVT;
  NSVariableType2DParams::checkInputs(d);
  q = avpde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );


  Conservative2D<Real> qdata3(rho, rho*u, rho*v, rho*E);
  q = 0;
  avpde.setDOFFrom( q, qdata3 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );

  PyDict Conservative;
  Conservative[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.Conservative;
  Conservative[Conservative2DParams::params.rho] = rho;
  Conservative[Conservative2DParams::params.rhou] = rho*u;
  Conservative[Conservative2DParams::params.rhov] = rho*v;
  Conservative[Conservative2DParams::params.rhoE] = rho*E;

  d[NSVariableType2DParams::params.StateVector] = Conservative;
  NSVariableType2DParams::checkInputs(d);
  q = avpde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
}

#if 0 // turned this off because this is no longer using the separate sensor field, and the
// case where the sensor is in q is tested in the PDEmitAVSensor tests
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux_param_tensorH, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length + sensor

  const Real tol = 5e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  const int order = 1;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  // function tests

  Real x, y, time;
  Real rho, u, v, t, p, e0, h0;
  Real Cv, Cp;

  x = y = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u + v*v);
  h0 = Cp*t + 0.5*(u*u + v*v);

  Real sensor = 0.15;

  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType paramL = logH; // grid spacing
  ParamType paramR = logH; // grid spacing

  // set
  ArrayQ q = 0;
  avpde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );

  // conservative flux
  ArrayQ uTrue = {rho, rho*u, rho*v, rho*e0};
  ArrayQ uCons = 0;
  avpde.masterState( paramL, x, y, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );
  BOOST_CHECK_CLOSE( uTrue(3), uCons(3), tol );

  // Should not accumulate
  avpde.masterState( paramL, x, y, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );
  BOOST_CHECK_CLOSE( uTrue(3), uCons(3), tol );

  // conservative flux
  ArrayQ ftTrue = {rho, rho*u, rho*v, rho*e0};
  ArrayQ ft = 0;
  avpde.fluxAdvectiveTime( paramL, x, y, time, q, ft );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( ftTrue(3), ft(3), tol );

  // Flux accumulate
  avpde.fluxAdvectiveTime( paramL, x, y, time, q, ft );
  BOOST_CHECK_CLOSE( 2*ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(3), ft(3), tol );

  MatrixQ dudq = 0;
  MatrixQ dudqTrue = 0;

  ArrayQ rho_q, u_q, v_q, t_q = 0;
  Real e_rho, e_t = 0;
  avpde.variableInterpreter().evalJacobian( q, rho_q, u_q, v_q, t_q );
  Real e = avpde.gasModel().energy(rho, t);
  avpde.gasModel().energyJacobian(rho, t, e_rho, e_t);
  Real E = e + 0.5*(u*u + v*v);
  ArrayQ E_q = e_rho*rho_q + e_t*t_q + u*u_q + v*v_q;

  dudqTrue(0,0) = rho_q[0];
  dudqTrue(0,1) = rho_q[1];
  dudqTrue(0,2) = rho_q[2];
  dudqTrue(0,3) = rho_q[3];

  dudqTrue(1,0) = rho_q[0]*u + rho*u_q[0];
  dudqTrue(1,1) = rho_q[1]*u + rho*u_q[1];
  dudqTrue(1,2) = rho_q[2]*u + rho*u_q[2];
  dudqTrue(1,3) = rho_q[3]*u + rho*u_q[3];

  dudqTrue(2,0) = rho_q[0]*v + rho*v_q[0];
  dudqTrue(2,1) = rho_q[1]*v + rho*v_q[1];
  dudqTrue(2,2) = rho_q[2]*v + rho*v_q[2];
  dudqTrue(2,3) = rho_q[3]*v + rho*v_q[3];

  dudqTrue(3,0) = rho_q[0]*E + rho*E_q[0];
  dudqTrue(3,1) = rho_q[1]*E + rho*E_q[1];
  dudqTrue(3,2) = rho_q[2]*E + rho*E_q[2];
  dudqTrue(3,3) = rho_q[3]*E + rho*E_q[3];

  avpde.jacobianMasterState(paramL, x, y, time, q, dudq);
  for (int i=0; i<4; i++)
    for (int j=0; j<4; j++)
      BOOST_CHECK_CLOSE( dudq(i,j), dudqTrue(i,j), tol );

  // Should not accumulate
  avpde.jacobianMasterState(paramL, x, y, time, q, dudq);
  for (int i=0; i<4; i++)
    for (int j=0; j<4; j++)
      BOOST_CHECK_CLOSE( dudq(i,j), dudqTrue(i,j), tol );

  // advective flux
  ArrayQ fTrue = {rho*u, rho*u*u + p, rho*u*v, rho*u*h0};
  ArrayQ gTrue = {rho*v, rho*v*u, rho*v*v + p, rho*v*h0};
  ArrayQ f = 0, g = 0;

  avpde.fluxAdvective( paramL, x, y, time, q, f, g );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( gTrue(1), g(1), tol );
  BOOST_CHECK_CLOSE( gTrue(2), g(2), tol );
  BOOST_CHECK_CLOSE( gTrue(3), g(3), tol );

  // Flux accumulate
  avpde.fluxAdvective( paramL, x, y, time, q, f, g );
  BOOST_CHECK_CLOSE( 2*fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( 2*gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( 2*gTrue(1), g(1), tol );
  BOOST_CHECK_CLOSE( 2*gTrue(2), g(2), tol );
  BOOST_CHECK_CLOSE( 2*gTrue(3), g(3), tol );

  // advective flux jacobian
  Real dfdudata[16] = {0,1,0,0, -0.4810526,1.2544,0.0924,0.4, 0.181104,-0.231,0.784,0,       -1.2404487984,1.4699461,0.0724416,1.0976};
  Real dgdudata[16] = {0,0,1,0,  0.181104,-0.231,0.784,0,     0.0802424,-0.3136,-0.3696,0.4,  0.3654893781,0.0724416,1.6944641,-0.3234};
  MatrixQ dfduTrue( dfdudata, 16 );
  MatrixQ dgduTrue( dgdudata, 16 );
  MatrixQ dfdu, dgdu;
  dfdu = 0;  dgdu = 0;
  avpde.jacobianFluxAdvective( paramL, x, y, time, q, dfdu, dgdu );
  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++)
    {
      BOOST_CHECK_CLOSE( dfduTrue(i,j), dfdu(i,j), tol );
      BOOST_CHECK_CLOSE( dgduTrue(i,j), dgdu(i,j), tol );
    }

  // Flux accumulation
  avpde.jacobianFluxAdvective( paramL, x, y, time, q, dfdu, dgdu );
  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++)
    {
      BOOST_CHECK_CLOSE( 2*dfduTrue(i,j), dfdu(i,j), tol );
      BOOST_CHECK_CLOSE( 2*dgduTrue(i,j), dgdu(i,j), tol );
    }

  Real rhoL = 1.137, uL = 0.784, vL = -0.231, tL = 0.987;
  Real rhoR = 1.240, uR = 0.831, vR =  0.142, tR = 0.865;

  ArrayQ qL = 0, qR = 0;
  avpde.setDOFFrom( qL, DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
  avpde.setDOFFrom( qR, DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );

  ArrayQ qxL = {0.17, 0.21, -0.34, 0.19};
  ArrayQ qyL = {-0.05, 0.61, 0.12, -0.21};

  ArrayQ qxR = {0.21, 0.31, -0.14, 0.26};
  ArrayQ qyR = {0.11, 0.48, 0.27, -0.15};

  MatrixQ kxx = 0, kxy = 0;
  MatrixQ kyx = 0, kyy = 0;

  MatrixQ kxxT = 0, kxyT = 0;
  MatrixQ kyxT = 0, kyyT = 0;

  MatrixQ kxx_true = 0, kxy_true = 0;
  MatrixQ kyx_true = 0, kyy_true = 0;

  Real lambda = 0;
  avpde.speedCharacteristic( paramL, x, y, time, qL, lambda );

#if 0
  Real theta_L = 0.001;
  Real theta_H = 1.0;
  sensor = smoothActivation_sine(sensor, theta_L, theta_H);
#else
//  Real alpha = 500;
//  Real eps_exp = 1e-10;
//
//  sensor = smoothActivation_exp(sensor, alpha, eps_exp);


  Real zz = 0.0;
  sensor = std::max(sensor, zz);
#endif

  Real factor = 2.0/(Real(order)) * lambda * sensor; //smoothabs0(sensorL, 1.0e-5);

  MatrixQ dutdu = DLA::Identity();
  dutdu(3, 0) = 0.5*(gamma - 1.0)*(uL*uL + vL*vL);
  dutdu(3, 1) = -(gamma - 1.0)*uL;
  dutdu(3, 2) = -(gamma - 1.0)*vL;
  dutdu(3, 3) = gamma;

  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++)
    {
      if (i == j)
      {
        kxxT(i,j) = factor*H(0,0);
        kxyT(i,j) = factor*H(0,1);
        kyxT(i,j) = factor*H(1,0);
        kyyT(i,j) = factor*H(1,1);
      }
      else
      {
        kxxT(i,j) = 0.0;
        kxyT(i,j) = 0.0;
        kyxT(i,j) = 0.0;
        kyyT(i,j) = 0.0;
      }
    }

  kxx_true = kxxT*dutdu;
  kxy_true = kxyT*dutdu;
  kyx_true = kyxT*dutdu;
  kyy_true = kyyT*dutdu;

  avpde.diffusionViscous( paramL, x, y, time, qL, qxL, qyL, kxx, kxy, kyx, kyy );
  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++)
    {
      BOOST_CHECK_CLOSE( kxx_true(i,j), kxx(i,j), tol );
      BOOST_CHECK_CLOSE( kxy_true(i,j), kxy(i,j), tol );
      BOOST_CHECK_CLOSE( kyx_true(i,j), kyx(i,j), tol );
      BOOST_CHECK_CLOSE( kyy_true(i,j), kyy(i,j), tol );
    }

  // Flux accumulate
  avpde.diffusionViscous( paramL, x, y, time, qL, qxL, qyL, kxx, kxy, kyx, kyy );
  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++)
    {
      BOOST_CHECK_CLOSE( 2*kxx_true(i,j), kxx(i,j), tol );
      BOOST_CHECK_CLOSE( 2*kxy_true(i,j), kxy(i,j), tol );
      BOOST_CHECK_CLOSE( 2*kyx_true(i,j), kyx(i,j), tol );
      BOOST_CHECK_CLOSE( 2*kyy_true(i,j), kyy(i,j), tol );
    }

  MatrixQ dudqL = 0;
  avpde.jacobianMasterState(paramL, x, y, time, qL, dudqL);

  ArrayQ fxL_true = -kxx_true*dudqL*qxL - kxy_true*dudqL*qyL;
  ArrayQ fyL_true = -kyx_true*dudqL*qxL - kyy_true*dudqL*qyL;

  ArrayQ fxL = 0, fyL = 0;
  avpde.fluxViscous( paramL, x, y, time, qL, qxL, qyL, fxL, fyL );
  BOOST_CHECK_CLOSE( fxL_true(0), fxL(0), tol );
  BOOST_CHECK_CLOSE( fxL_true(1), fxL(1), tol );
  BOOST_CHECK_CLOSE( fxL_true(2), fxL(2), tol );
  BOOST_CHECK_CLOSE( fxL_true(3), fxL(3), tol );

  BOOST_CHECK_CLOSE( fyL_true(0), fyL(0), tol );
  BOOST_CHECK_CLOSE( fyL_true(1), fyL(1), tol );
  BOOST_CHECK_CLOSE( fyL_true(2), fyL(2), tol );
  BOOST_CHECK_CLOSE( fyL_true(3), fyL(3), tol );

  // Flux accumulate
  avpde.fluxViscous( paramL, x, y, time, qL, qxL, qyL, fxL, fyL );
  BOOST_CHECK_CLOSE( 2*fxL_true(0), fxL(0), tol );
  BOOST_CHECK_CLOSE( 2*fxL_true(1), fxL(1), tol );
  BOOST_CHECK_CLOSE( 2*fxL_true(2), fxL(2), tol );
  BOOST_CHECK_CLOSE( 2*fxL_true(3), fxL(3), tol );

  BOOST_CHECK_CLOSE( 2*fyL_true(0), fyL(0), tol );
  BOOST_CHECK_CLOSE( 2*fyL_true(1), fyL(1), tol );
  BOOST_CHECK_CLOSE( 2*fyL_true(2), fyL(2), tol );
  BOOST_CHECK_CLOSE( 2*fyL_true(3), fyL(3), tol );

  ArrayQ fxR_true = 0, fyR_true = 0;
  avpde.fluxViscous( paramR, x, y, time, qR, qxR, qyR, fxR_true, fyR_true );

  Real nx = 0.526, ny = -0.124;
  ArrayQ fn_true = 0.5*(fxL_true + fxR_true)*nx + 0.5*(fyL_true + fyR_true)*ny;

  ArrayQ fn = 0;
  avpde.fluxViscous( paramL, paramR, x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fn_true(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fn_true(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fn_true(2), fn(2), tol );
  BOOST_CHECK_CLOSE( fn_true(3), fn(3), tol );

  // Flux accumulate
  avpde.fluxViscous( paramL, paramR, x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );
  BOOST_CHECK_CLOSE( 2*fn_true(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 2*fn_true(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 2*fn_true(2), fn(2), tol );
  BOOST_CHECK_CLOSE( 2*fn_true(3), fn(3), tol );

  ArrayQ source = 0.0;
  avpde.source( paramL, x, y, time, qL, qxL, qyL, source );
  BOOST_CHECK_CLOSE( 0.0, source(0), tol );
  BOOST_CHECK_CLOSE( 0.0, source(1), tol );
  BOOST_CHECK_CLOSE( 0.0, source(2), tol );
  BOOST_CHECK_CLOSE( 0.0, source(3), tol );
}
#endif
#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux_param_scalarH, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType; //grid length + sensor

  const Real tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  const int order = 1;
  AVPDEClass avpde(order, gas, Euler_ResidInterp_Raw);

  // function tests
  Real x, y, time;

  Real sensor = 0.15;
  Real h = 0.3;
  ParamType paramL(h, sensor); // grid spacing and sensor values

  Real rhoL = 1.137, uL = 0.784, vL = -0.231, tL = 0.987;
  Real rhoR = 1.240, uR = 0.831, vR =  0.142, tR = 0.865;

  ArrayQ qL = 0, qR = 0;
  avpde.setDOFFrom( qL, DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
  avpde.setDOFFrom( qR, DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );

  ArrayQ qxL = {0.17, 0.21, -0.34, 0.19};
  ArrayQ qyL = {-0.05, 0.61, 0.12, -0.21};

  ArrayQ qxR = {0.21, 0.31, -0.14, 0.26};
  ArrayQ qyR = {0.11, 0.48, 0.27, -0.15};

  MatrixQ kxx = 0, kxy = 0, kyx = 0, kyy = 0;
  MatrixQ kxx_true = 0, kxy_true = 0, kyx_true = 0, kyy_true = 0;
  avpde.diffusionViscous( paramL, x, y, time, qL, qxL, qyL, kxx, kxy, kyx, kyy );

  Real lambda = 0;
  avpde.speedCharacteristic( paramL, x, y, time, qL, lambda );
  Real s = sensor;
  Real theta_L = 0.01;
  Real theta_H = 1.0;
  if (sensor < theta_L)
  {
    s = 0;
  }
  else if (sensor > theta_H)
  {
    s = theta_H;
  }
  else
  {
    s = 0.5*theta_H * (sin(PI*((sensor - theta_L)/(theta_H - theta_L) - 0.5)) + 1.0);
  }

  Real factor = 1.0/(Real(order)) * lambda * s;

  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++)
    {
      if (i == j)
      {
        kxx_true(i,j) = factor*h;
        kxy_true(i,j) = 0.0;
        kyx_true(i,j) = 0.0;
        kyy_true(i,j) = factor*h;
      }
      else
      {
        kxx_true(i,j) = 0.0;
        kxy_true(i,j) = 0.0;
        kyx_true(i,j) = 0.0;
        kyy_true(i,j) = 0.0;
      }

      BOOST_CHECK_CLOSE( kxx_true(i,j), kxx(i,j), tol );
      BOOST_CHECK_CLOSE( kxy_true(i,j), kxy(i,j), tol );
      BOOST_CHECK_CLOSE( kyx_true(i,j), kyx(i,j), tol );
      BOOST_CHECK_CLOSE( kyy_true(i,j), kyy(i,j), tol );
    }

  MatrixQ dudqL = 0;
  avpde.jacobianMasterState(qL, dudqL);

  ArrayQ fxL_true = -kxx_true*dudqL*qxL - kxy_true*dudqL*qyL;
  ArrayQ fyL_true = -kyx_true*dudqL*qxL - kyy_true*dudqL*qyL;

  ArrayQ fxL = 0, fyL = 0;
  avpde.fluxViscous( paramL, x, y, time, qL, qxL, qyL, fxL, fyL );
  BOOST_CHECK_CLOSE( fxL_true(0), fxL(0), tol );
  BOOST_CHECK_CLOSE( fxL_true(1), fxL(1), tol );
  BOOST_CHECK_CLOSE( fxL_true(2), fxL(2), tol );
  BOOST_CHECK_CLOSE( fxL_true(3), fxL(3), tol );

  BOOST_CHECK_CLOSE( fyL_true(0), fyL(0), tol );
  BOOST_CHECK_CLOSE( fyL_true(1), fyL(1), tol );
  BOOST_CHECK_CLOSE( fyL_true(2), fyL(2), tol );
  BOOST_CHECK_CLOSE( fyL_true(3), fyL(3), tol );

  ArrayQ fxR_true = 0, fyR_true = 0;
  avpde.fluxViscous( paramL, x, y, time, qR, qxR, qyR, fxR_true, fyR_true );

  Real nx = 0.526, ny = -0.124;
  ArrayQ fn_true = 0.5*(fxL_true + fxR_true)*nx + 0.5*(fyL_true + fyR_true)*ny;

  ArrayQ fn = 0;
  avpde.fluxViscous( paramL, x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fn_true(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fn_true(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fn_true(2), fn(2), tol );
  BOOST_CHECK_CLOSE( fn_true(3), fn(3), tol );

  ArrayQ source = 0.0;
  avpde.source( paramL, x, y, time, qL, qxL, qyL, source );
  BOOST_CHECK_CLOSE( 0.0, source(0), tol );
  BOOST_CHECK_CLOSE( 0.0, source(1), tol );
  BOOST_CHECK_CLOSE( 0.0, source(2), tol );
  BOOST_CHECK_CLOSE( 0.0, source(3), tol );
}
#endif
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PDEEuler2D_PrimitiveStrongFlux )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length + sensor

  const Real tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  // function tests

  Real x, y, time;
  Real rho, u, v, t, p, e0;
  Real Cv, Cp;

  x = y = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u + v*v);

  HType Htensor = {{0.5},{0.1, 0.3}};
  HType logH = log(Htensor);
  ParamType paramL(logH); // grid spacing and sensor values

  // set
  ArrayQ q = 0;
  avpde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );

  // Strong Conservative Flux
  Real rhot = -0.9;
  Real ut = -0.25;
  Real vt = 0.14;
  Real pt = 0.23;
  Real tt = (t/p) * pt - (t/rho)*rhot;

  Real e0t = Cv*tt + u*ut + v*vt;

  ArrayQ qt = {rhot, ut, vt, pt};
  ArrayQ utCons, utConsTrue;
  utCons = 0;
  utConsTrue[0] = rhot;
  utConsTrue[1] = rho*ut + rhot*u;
  utConsTrue[2] = rho*vt + rhot*v;
  utConsTrue[3] = rho*e0t + rhot*e0;
  avpde.strongFluxAdvectiveTime( paramL, x, y, time, q, qt, utCons );
  BOOST_CHECK_CLOSE( utConsTrue(0), utCons(0), tol );
  BOOST_CHECK_CLOSE( utConsTrue(1), utCons(1), tol );
  BOOST_CHECK_CLOSE( utConsTrue(2), utCons(2), tol );
  BOOST_CHECK_CLOSE( utConsTrue(3), utCons(3), tol );

  // Flux accumulate
  avpde.strongFluxAdvectiveTime( paramL, x, y, time, q, qt, utCons );
  BOOST_CHECK_CLOSE( 2*utConsTrue(0), utCons(0), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(1), utCons(1), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(2), utCons(2), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(3), utCons(3), tol );

  // Strong Advective Flux
  Real rhox = -0.02;
  Real rhoy = 0.05;
  Real ux = 0.072;
  Real uy = -0.036;
  Real vx = -0.0234;
  Real vy = 0.0898;
  Real px = 10.2;
  Real py = 0.05;

  //PRIMITIVE SPECIFIC
  ArrayQ qx = {rhox, ux, vx, px};
  ArrayQ qy = {rhoy, uy, vy, py};

  Real tx = (t/p) * px - (t/rho)*rhox;
  Real ty = (t/p) * py - (t/rho)*rhoy;

  Real H = Cp*t + 0.5*(u*u + v*v);
  Real Hx = Cp*tx + (ux*u + vx*v);
  Real Hy = Cp*ty + (uy*u + vy*v);

  ArrayQ strongAdv = 0;
  ArrayQ strongAdvTrue = 0;
  strongAdvTrue(0) += rhox*u   +   rho*ux;
  strongAdvTrue(1) += rhox*u*u + 2*rho*ux*u + px;
  strongAdvTrue(2) += rhox*u*v +   rho*ux*v + rho*u*vx;
  strongAdvTrue(3) += rhox*u*H +   rho*ux*H + rho*u*Hx;

  strongAdvTrue(0) += rhoy*v   +   rho*vy;
  strongAdvTrue(1) += rhoy*v*u +   rho*vy*u + rho*v*uy;
  strongAdvTrue(2) += rhoy*v*v + 2*rho*vy*v + py;
  strongAdvTrue(3) += rhoy*v*H +   rho*vy*H + rho*v*Hy;

  avpde.strongFluxAdvective( paramL, x, y, time, q, qx, qy, strongAdv );
  BOOST_CHECK_CLOSE( strongAdvTrue(0), strongAdv(0), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(1), strongAdv(1), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(2), strongAdv(2), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(3), strongAdv(3), tol );

  // Flux accumulate
  avpde.strongFluxAdvective( paramL, x, y, time, q, qx, qy, strongAdv );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(0), strongAdv(0), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(1), strongAdv(1), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(2), strongAdv(2), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(3), strongAdv(3), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( jacobianMasterState, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<SurrealS<PDEClass::N>> ArrayQSurreal;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( avpde.N == 4 );

  // function tests

  Real rho, u, v, t;

  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;

  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType paramL(logH); // grid spacing

  Real x = 0, y = 0, time = 0;

  // set
  ArrayQ q = 0;
  avpde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );

  if ( std::is_same<QType,QTypeEntropy>::value )
  {
    // Entropy variables seem to have numerical problems when comparing Surreal and
    // hand differentiated code...
    q[0] = -10;
    q[1] = 2;
    q[2] = 4;
    q[3] = -3;
  }

  ArrayQSurreal qSurreal = q;

  qSurreal[0].deriv(0) = 1;
  qSurreal[1].deriv(1) = 1;
  qSurreal[2].deriv(2) = 1;
  qSurreal[3].deriv(3) = 1;

  // conservative flux
  ArrayQSurreal uConsSurreal = 0;
  avpde.masterState( paramL, x, y, time, qSurreal, uConsSurreal );

  MatrixQ dudq;

  const Real small_tol = 5e-10;
  const Real close_tol = 1e-12;

  avpde.jacobianMasterState( paramL, x, y, time, q, dudq );
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(0), dudq(0,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(1), dudq(0,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(2), dudq(0,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(3), dudq(0,3), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(0), dudq(1,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(1), dudq(1,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(2), dudq(1,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(3), dudq(1,3), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(0), dudq(2,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(1), dudq(2,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(2), dudq(2,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(3), dudq(2,3), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(0), dudq(3,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(1), dudq(3,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(2), dudq(3,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(3), dudq(3,3), small_tol, close_tol )

  // Should not accumulate
  avpde.jacobianMasterState( paramL, x, y, time, q, dudq );
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(0), dudq(0,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(1), dudq(0,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(2), dudq(0,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(3), dudq(0,3), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(0), dudq(1,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(1), dudq(1,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(2), dudq(1,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(3), dudq(1,3), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(0), dudq(2,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(1), dudq(2,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(2), dudq(2,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(3), dudq(2,3), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(0), dudq(3,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(1), dudq(3,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(2), dudq(3,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(3), dudq(3,3), small_tol, close_tol )
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxRoe, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length + sensor

  const Real tol = 2.e-11;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  // Roe flux function test

  Real x, y, time;
  Real nx, ny;
  Real rhoL, uL, vL, tL, pL, h0L;
  Real rhoR, uR, vR, tR, pR, h0R;
  Real Cp;

  x = y = time = 0;   // not actually used in functions
  nx = 2.0/7.0; ny = sqrt(1.0-nx*nx);
  rhoL = 1.034; uL = 3.26; vL = -2.17; tL = 5.78;
  rhoR = 0.973; uR = 1.79; vR =  0.93; tR = 6.13;
  Cp  = R*gamma/(gamma - 1);
  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL + vL*vL);
  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR + vR*vR);

  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType paramL = logH;

  // set
  ArrayQ qL = avpde.setDOFFrom( DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
  ArrayQ qR = avpde.setDOFFrom( DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );

  // advective normal flux (average)
  Real fL[4] = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*vL     , rhoL*uL*h0L};
  Real fR[4] = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*vR     , rhoR*uR*h0R};
  Real gL[4] = {rhoL*vL, rhoL*vL*uL     , rhoL*vL*vL + pL, rhoL*vL*h0L};
  Real gR[4] = {rhoR*vR, rhoR*vR*uR     , rhoR*vR*vR + pR, rhoR*vR*h0R};

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < 4; k++)
    fnTrue[k] = 0.5*(nx*(fL[k] + fR[k]) + ny*(gL[k] + gR[k]));

#if 1
  // Exact values for Roe scheme from Roe2D.nb
  fnTrue[0] -= -0.021574952725013058474459426733768;
  fnTrue[1] -= -1.7958753728220592172800048653566;
  fnTrue[2] -=  4.4515996249161671139371535471951;
  fnTrue[3] -= -6.8094427666218353919034995028459;
#endif


  ArrayQ fn = 0;
  avpde.fluxAdvectiveUpwind( paramL, x, y, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( fnTrue(3), fn(3), tol );

  // Flux accumulate
  avpde.fluxAdvectiveUpwind( paramL, x, y, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(3), fn(3), tol );

  MatrixQ mtx;
  mtx = 0;

  MatrixQ mtxTrue = {
    {1.0323434331949190979834072919713,
        -0.44459540501697074517797177104962,
        -0.43697991171460424043588329213258,
        0.080449362812368834954522463171426},
    {-0.72312835498151845646006425483631,
        -0.010364873336226558791267034135421,
        -1.4045718375886762151895973056639,
        0.18933272986886306466125181349454},
    {-0.90833712876943321732424020657867,
        1.9404853199183346620104522800780,
        2.1633849842368432379888624467138,
        -0.41919712890981821676619255996783},
    {-0.43536491993382434782224675355112,
        -8.1758936855015456959218531030353,
        -6.9672110585297722485152708900077,
        2.7090875342447734040794006013828}
                    };
  //Values taken from pde/NS/Roe2D.nb

  avpde.jacobianFluxAdvectiveAbsoluteValue(paramL, x, y, time, qL, nx, ny, mtx );
  for (int i=0; i<4; i++)
    for (int j=0; j<4; j++)
      BOOST_CHECK_CLOSE( mtx(i,j), mtxTrue(i,j), tol );

  // Flux accumulate
  avpde.jacobianFluxAdvectiveAbsoluteValue(paramL, x, y, time, qL, nx, ny, mtx );
  for (int i=0; i<4; i++)
    for (int j=0; j<4; j++)
      BOOST_CHECK_CLOSE( mtx(i,j), 2*mtxTrue(i,j), tol );


  //Call these simply to make sure coverage is high
//  ArrayQ qxL, qxR, qyL, qyR, sourceL, sourceR;
//  Real dummy = 0;
//  avpde.fluxViscous( paramL, dummy, paramL, dummy, dummy, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );
//  avpde.sourceTrace( paramL, dummy, dummy, paramL, dummy, dummy, dummy, qL, qxL, qyL, qR, qxR, qyR, sourceL, sourceR );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Holst2020 )
{
  typedef TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length + sensor

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eHolst2020, gas, Euler_ResidInterp_Raw);

  Real hxx = 1.0;
  Real hyy = 2.0;
  HType logH = {{log(hxx)},
                    {0.0, log(hyy)}};
  ParamType param = logH;

  Real x = 1, y = 0.5;
  Real time = 0;

  Real V = 5.0;
  Real P = 1.0;

  Real alpha = 30.0 / 180.0 * M_PI;
  for (int j = 0; j < 1; j++)
  {
    Real beta = Real(j) / 36 * 2.0 * M_PI;

    Real rho = 1.0;
    Real u = V*cos(alpha);
    Real v = V*sin(alpha);
    Real p = 1.0;
    // Derived properties
    Real c = sqrt(gamma*p/rho);

    Real rhox = 0.1;
    Real ux = 0.6*cos(alpha) + 0.8*sin(alpha);
    Real vx = 0.6*sin(alpha) - 0.8*cos(alpha);
    Real px = P*cos(alpha + beta);

    Real rhoy = 0.2;
    Real uy = 0.8*cos(alpha) + 0.6*sin(alpha);
    Real vy = 0.8*sin(alpha) - 0.6*cos(alpha);
    Real py = P*sin(alpha + beta);

    ArrayQ q = avpde.setDOFFrom( DensityVelocityPressure2D<Real>(rho, u, v, p) );

    ArrayQ qx = {rhox, ux, vx, px};
    ArrayQ qy = {rhoy, uy, vy, py};

    Real fac = sqrt(px*hxx*hxx*px + py*hyy*hyy*py);
    Real sRef = fac / (fac + 3.0*p);
    sRef = sRef * tanh(10.0 * sRef);
    Real ctilde = c + abs(V);
    DLA::MatrixSymS<2,Real> eTrue = 0.0;
    eTrue(0,0) = ctilde * sRef * hxx;
    eTrue(1,1) = ctilde * sRef * hyy;

//    DLA::MatrixSymS<2,Real> eps = 0.0;
//    avpde.artificialViscosity(param, x, y, time, q, qx, qy, eps);
//    SANS_CHECK_CLOSE( eTrue(0,0), eps(0,0), small_tol, close_tol );
//    SANS_CHECK_CLOSE( eTrue(0,1), eps(0,1), small_tol, close_tol );
//    SANS_CHECK_CLOSE( eTrue(1,0), eps(1,0), small_tol, close_tol );
//    SANS_CHECK_CLOSE( eTrue(1,1), eps(1,1), small_tol, close_tol );

    MatrixQ dutdu = DLA::Identity();
    dutdu(3, 0) = 0.5*(gamma - 1.0)*(u*u + v*v);
    dutdu(3, 1) = -(gamma - 1.0)*u;
    dutdu(3, 2) = -(gamma - 1.0)*v;
    dutdu(3, 3) = gamma;

    MatrixQ kxx = 0, kxy = 0;
    MatrixQ kyx = 0, kyy = 0;

    MatrixQ kxxT = 0, kxyT = 0;
    MatrixQ kyxT = 0, kyyT = 0;

    MatrixQ kxx_true = 0, kxy_true = 0;
    MatrixQ kyx_true = 0, kyy_true = 0;


    for (int i = 0; i < 4; i++)
    {
      for (int j = 0; j < 4; j++)
      {
        if (i == j)
        {
          kxxT(i,j) = ctilde * sRef * hxx;
          kxyT(i,j) = 0.0;
          kyxT(i,j) = 0.0;
          kyyT(i,j) = ctilde * sRef * hyy;
        }
        else
        {
          kxxT(i,j) = 0.0;
          kxyT(i,j) = 0.0;
          kyxT(i,j) = 0.0;
          kyyT(i,j) = 0.0;
        }
      }
    }

    kxx_true = kxxT*dutdu;
    kxy_true = kxyT*dutdu;
    kyx_true = kyxT*dutdu;
    kyy_true = kyyT*dutdu;

    avpde.diffusionViscous( param, x, y, time, q, qx, qy, kxx, kxy, kyx, kyy );
    for (int i = 0; i < 4; i++)
    {
      for (int j = 0; j < 4; j++)
      {
        BOOST_CHECK_CLOSE( kxx_true(i,j), kxx(i,j), close_tol );
        BOOST_CHECK_CLOSE( kxy_true(i,j), kxy(i,j), close_tol );
        BOOST_CHECK_CLOSE( kyx_true(i,j), kyx(i,j), close_tol );
        BOOST_CHECK_CLOSE( kyy_true(i,j), kyy(i,j), close_tol );
      }
    }

    // Flux accumulate
    avpde.diffusionViscous( param, x, y, time, q, qx, qy, kxx, kxy, kyx, kyy );
    for (int i = 0; i < 4; i++)
    {
      for (int j = 0; j < 4; j++)
      {
        BOOST_CHECK_CLOSE( 2*kxx_true(i,j), kxx(i,j), close_tol );
        BOOST_CHECK_CLOSE( 2*kxy_true(i,j), kxy(i,j), close_tol );
        BOOST_CHECK_CLOSE( 2*kyx_true(i,j), kyx(i,j), close_tol );
        BOOST_CHECK_CLOSE( 2*kyy_true(i,j), kyy(i,j), close_tol );
      }
    }

    ArrayQ f = 0;
    ArrayQ g = 0;
    avpde.fluxViscous(param, x, y, time, q, qx, qy, f, g);

    MatrixQ dudq = 0;
    avpde.jacobianMasterState(param, x, y, time, q, dudq);

    ArrayQ fTrue = -kxx_true*dudq*qx - kxy_true*dudq*qy;
    ArrayQ gTrue = -kyx_true*dudq*qx - kyy_true*dudq*qy;

    for (int i = 0; i < 4; i++)
    {
      SANS_CHECK_CLOSE( fTrue[i], f[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( gTrue[i], g[i], small_tol, close_tol );
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( speedCharacteristic, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length + sensor

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  Real x, y, time, dx, dy;
  Real rho, u, v, t, c;
  Real speed, speedTrue;

  x = y = time = 0;   // not actually used in functions
  dx = 0.57; dy = -0.43;
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  c = sqrt(gamma*R*t);
  speedTrue = fabs(dx*u + dy*v)/sqrt(dx*dx + dy*dy) + c;

  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType paramL = logH;

  Real qDataPrim[4] = {rho, u, v, t};
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY", "Temperature"};
  ArrayQ q;
  avpde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );

  avpde.speedCharacteristic( paramL, x, y, time, dx, dy, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  avpde.speedCharacteristic( paramL, x, y, time, q, speed );
  BOOST_CHECK_CLOSE( sqrt(u*u + v*v) + c, speed, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( isValidState, QType, QTypes )
{
  // Surrogates are special
  // Entropy has log( x < 0 ) when constructing invalid states
  if ( std::is_same<QType,QTypePrimitiveSurrogate>::value ) return;
  if ( std::is_same<QType,QTypeEntropy>::value ) return;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  ArrayQ q;
  Real rho, u = 0, v = 0, t;

  rho = 1;
  t = 1;
  avpde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( avpde.isValidState(q) == true );

  rho = 1;
  t = -1;
  avpde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( avpde.isValidState(q) == false );

  rho = -1;
  t = 1;
  avpde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( avpde.isValidState(q) == false );

  rho = -1;
  t = -1;
  avpde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( avpde.isValidState(q) == false );

  rho = 1;
  t = 0;
  avpde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( avpde.isValidState(q) == false );

  rho = 0;
  t = 1;
  avpde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( avpde.isValidState(q) == false );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState_surrogate )
{
  typedef QTypePrimitiveSurrogate QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  ArrayQ q;
  Real rho, u = 0, v = 0, t;

  rho = 1;
  t = 1;
  avpde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( avpde.isValidState(q) == true );

  rho = 1;
  t = -1;
  avpde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( avpde.isValidState(q) == true );

  rho = -1;
  t = 1;
  avpde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( avpde.isValidState(q) == true );

  rho = -1;
  t = -1;
  avpde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( avpde.isValidState(q) == true );

  rho = 1;
  t = 0;
  avpde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( avpde.isValidState(q) == true );

  rho = 0;
  t = 1;
  avpde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  BOOST_CHECK( avpde.isValidState(q) == true );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( interpResidBC )
{
  typedef QTypePrimitiveSurrogate QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde1(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);
  Real tol = 1.0e-12;

  ArrayQ q;
  Real rho, u = 0, v = 0, t;

  rho = 1;
  t = 1;
  avpde1.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );

  BOOST_CHECK_EQUAL(avpde1.category(), 0);

  BOOST_CHECK_EQUAL(avpde1.nMonitor(), 4);
  BOOST_CHECK( avpde1.isValidState(q) == true );
  DLA::VectorD<Real> vecout1(4);
  avpde1.interpResidVariable(q, vecout1);
  for (int i=0; i<4; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );
  vecout1 = 0;
  avpde1.interpResidGradient(q, vecout1);
  for (int i=0; i<4; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );
  vecout1 = 0;
  avpde1.interpResidBC(q, vecout1);
  for (int i=0; i<4; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );

  AVPDEClass avpde2(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Momentum);

  BOOST_CHECK_EQUAL(avpde2.nMonitor(), 3);
  DLA::VectorD<Real> vecout2(3);
  avpde2.interpResidVariable(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)+q(2)*q(2)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(3),vecout2(2), tol, tol);

  vecout2 = 0;
  avpde2.interpResidGradient(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)+q(2)*q(2)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(3),vecout2(2), tol, tol);

  vecout2 = 0;
  avpde2.interpResidBC(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)+q(2)*q(2)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(3),vecout2(2), tol, tol);

  AVPDEClass avpde3(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Entropy);
  BOOST_CHECK_EQUAL(avpde3.nMonitor(), 1);
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
