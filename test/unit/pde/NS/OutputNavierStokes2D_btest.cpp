// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// testing routines for conversion of solution variables (Q) to
//     output of interest (Output): 2-D Navier-Stokes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/OutputNavierStokes2D.h"
#include "pde/NS/TraitsNavierStokes.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( OutputNavierStokes2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( TotalHeatFlux_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef OutputNavierStokes2D_TotalHeatFlux<PDEClass> OutputWeightsType;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
//  const Real tSutherland = 110;     // K
//  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real tol = 1.e-13;

  OutputWeightsType outWeights(pde);

  ArrayQ Fn = {0.1, 0.2, 0.3, 0.4};
  ArrayQ q = {0.5, 0.6, 0.7, 0.8};
  ArrayQ qx = {-0.37, 0.92, 0.53, -0.21};
  ArrayQ qy = { 0.84, 0.45, 0.04,  0.93};

  Real x = 0.2, y = 0.4;
  Real time = 1.0;

  ArrayQ weights, weightsTrue;
  outWeights(x, y, time, weights);

  weightsTrue = {0, 0, 0, 1.0};

  SANS_CHECK_CLOSE( weightsTrue[0], weights[0], tol, tol );
  SANS_CHECK_CLOSE( weightsTrue[1], weights[1], tol, tol );
  SANS_CHECK_CLOSE( weightsTrue[2], weights[2], tol, tol );
  SANS_CHECK_CLOSE( weightsTrue[3], weights[3], tol, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
