// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCNavierStokes2D_btest
//
// test of 2-D compressible Navier-Stokes BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/BCParameters.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"


//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
template class BCNavierStokes2D< BCTypeWallNoSlipAdiabatic_mitState,
  PDENavierStokes2D< TraitsSizeNavierStokes,
                     TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > >;

template class BCNavierStokes2D< BCTypeSymmetry_mitState,
  PDENavierStokes2D< TraitsSizeNavierStokes,
                     TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > >;

// Instantiate BCParamaters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters< BCNavierStokes2DVector<
    TraitsSizeNavierStokes,
    TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel>
                                                    > >;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BCNavierStokes2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;

  {
  typedef BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 2 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCNavierStokes2D<BCTypeSymmetry_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 2 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCNavierStokes2D<BCTypeWallNoSlipIsothermal_mitState, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef BCParameters< BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> > BCParams;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  {
  typedef BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass> BCClass;

  BCClass bc1(pde);

  //Pydict constructor
  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  PyDict BCList;
  BCList["BC1"] = BCNoSlip;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCNoSlip);
  }

  {
  typedef BCNavierStokes2D<BCTypeSymmetry_mitState, PDEClass> BCClass;

  BCClass bc1(pde);

  //Pydict constructor
  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCList;
  BCList["BC1"] = BCNoSlip;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCNoSlip);
  }

  {
  typedef BCNavierStokes2D<BCTypeWallNoSlipIsothermal_mitState, PDEClass> BCClass;
  const Real Twall = 300.0;

  BCClass bc1(pde, Twall);

  //Pydict constructor
  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipIsothermal_mitState;
  BCNoSlip[BCClass::ParamsType::params.Twall] = Twall;

  PyDict BCList;
  BCList["BC1"] = BCNoSlip;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCNoSlip);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_wallnoslip )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);

  typedef BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass> BCClass;
  typedef BCParameters< BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> > BCParams;

  //Pydict constructor
  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  BCClass bc(pde, BCNoSlip);
  typedef BCClass::ArrayQ<Real> ArrayQ;

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p;

  // Test conditions - M = 0.3, T0 = 300.1, P0 = 101325; alpha = 0.1 rad
  x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137;
  t = 300.1;
  p  = R*rho*t;

  Real M = 0.3;
  Real c =  sqrt(1.4*287.04*300.1);
  Real alpha = 0.1;
  u = M*c*cos(alpha);
  v = M*c*sin(alpha);

  ArrayQ q, qB;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  bc.state(x,y,time,nx,ny,q,qB);

  ArrayQ qTrue = {rho, 0.0, 0.0, p};

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;
  SANS_CHECK_CLOSE(    qTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE(    qTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE(    qTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE(    qTrue(3), qB(3), small_tol, close_tol );

  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  ArrayQ qx = {rhox, ux, vx, px};
  ArrayQ qy = {rhoy, uy, vy, py};

  ArrayQ Fn = 0;
  bc.fluxNormal( x, y, time, nx, ny, q, qx, qy, qB, Fn);

  ArrayQ fx = 0;
  ArrayQ fy = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, time, qTrue, fx, fy);
  FnTrue += fx*nx + fy*ny;

  // Compute the viscous flux
  ArrayQ fv = 0, gv = 0;
  pde.fluxViscous(x, y, time, qTrue, qx, qy, fv, gv);
  ArrayQ Fvn = fv*nx + gv*ny;

  // Zero out the energy equation for the adiabatic wall
  Fvn[pde.iEngy] = 0;

  FnTrue += Fvn;
  SANS_CHECK_CLOSE(    FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE(    FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE(    FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE(    FnTrue(3), Fn(3), small_tol, close_tol );

  // there is no invalid state for a wall
  BOOST_CHECK( bc.isValidState(nx, ny, q) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_symmetry )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);

  typedef BCNavierStokes2D<BCTypeSymmetry_mitState, PDEClass> BCClass;
  typedef BCParameters< BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> > BCParams;

  //Pydict constructor
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  BCClass bc(pde, BCSymmetry);
  typedef BCClass::ArrayQ<Real> ArrayQ;

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p;

  // Test conditions - M = 0.3, T0 = 300.1, P0 = 101325; alpha = 0.1 rad
  x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137;
  t = 300.1;
  p  = R*rho*t;

  Real M = 0.3;
  Real c =  sqrt(1.4*287.04*300.1);
  Real alpha = 0.1;
  u = M*c*cos(alpha);
  v = M*c*sin(alpha);

  Real rhoB = rho;

  //VB = V - n*dot(V,n)
  Real uB = u - nx*(u*nx + v*ny);
  Real vB = v - ny*(u*nx + v*ny);

  Real pB = p;

  ArrayQ q, qB, qTrue;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  pde.variableInterpreter().setFromPrimitive( qTrue, DensityVelocityPressure2D<Real>(rhoB, uB, vB, pB ) );

  bc.state(x, y, time, nx, ny, q, qB);

  const Real small_tol = 1.0e-9;
  const Real close_tol = 1.0e-12;
  SANS_CHECK_CLOSE( qTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue(3), qB(3), small_tol, close_tol );


  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  ArrayQ qx = {rhox, ux, vx, px};
  ArrayQ qy = {rhoy, uy, vy, py};

  ArrayQ Fn = 0;
  bc.fluxNormal( x, y, time, nx, ny, q, qx, qy, qB, Fn);

  ArrayQ fx = 0;
  ArrayQ fy = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, time, qB, fx, fy);
  FnTrue += fx*nx + fy*ny;

  // Compute the viscous flux
  ArrayQ fv = 0, gv = 0;
  pde.fluxViscous(x, y, time, qB, qx, qy, fv, gv);

  // Compute normal x-momentum and y-momentum equations
  Real xMomN = fv[pde.ixMom]*nx + gv[pde.ixMom]*ny;
  Real yMomN = fv[pde.iyMom]*nx + gv[pde.iyMom]*ny;

  FnTrue[pde.ixMom] += (xMomN*nx + yMomN*ny)*nx;
  FnTrue[pde.iyMom] += (xMomN*nx + yMomN*ny)*ny;

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_wallnoslipIsothermal )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;
  const Real Twall = 300.0;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);

  typedef BCNavierStokes2D<BCTypeWallNoSlipIsothermal_mitState, PDEClass> BCClass;
  typedef BCParameters< BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> > BCParams;

  //Pydict constructor
  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipIsothermal_mitState;
  BCNoSlip[BCClass::ParamsType::params.Twall] = Twall;

  BCClass bc(pde, BCNoSlip);
  typedef BCClass::ArrayQ<Real> ArrayQ;

  Real x, y, time;
  Real nx, ny;
  Real rho, u, v, t, p;

  // Test conditions - M = 0.3, T0 = 300.1, P0 = 101325; alpha = 0.1 rad
  x = y = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137;
  t = 300.1;
  p  = R*rho*t;

  Real M = 0.3;
  Real c =  sqrt(1.4*287.04*300.1);
  Real alpha = 0.1;
  u = M*c*cos(alpha);
  v = M*c*sin(alpha);

  ArrayQ q, qB;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  bc.state(x,y,time,nx,ny,q,qB);

  rho = p / (R*Twall);
  ArrayQ qTrue = {rho, 0.0, 0.0, p};

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;
  SANS_CHECK_CLOSE(    qTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE(    qTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE(    qTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE(    qTrue(3), qB(3), small_tol, close_tol );

  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  ArrayQ qx = {rhox, ux, vx, px};
  ArrayQ qy = {rhoy, uy, vy, py};

  ArrayQ Fn = 0;
  bc.fluxNormal( x, y, time, nx, ny, q, qx, qy, qB, Fn);

  ArrayQ fx = 0;
  ArrayQ fy = 0;
  ArrayQ FnTrue = 0;
  pde.fluxAdvective(x, y, time, qTrue, fx, fy);
  FnTrue += fx*nx + fy*ny;

  // Compute the viscous flux
  ArrayQ fv = 0, gv = 0;
  pde.fluxViscous(x, y, time, qTrue, qx, qy, fv, gv);
  ArrayQ Fvn = fv*nx + gv*ny;

  FnTrue += Fvn;
  SANS_CHECK_CLOSE(    FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE(    FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE(    FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE(    FnTrue(3), Fn(3), small_tol, close_tol );

  // there is no invalid state for a wall
  BOOST_CHECK( bc.isValidState(nx, ny, q) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/BCNavierStokes2D_pattern.txt", true );

  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real Tref = 1.0;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, Tref, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);

  {
    typedef BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass> BCClass;

    BCClass bc(pde);

    bc.dump( 2, output );
  }

  {
    typedef BCNavierStokes2D<BCTypeSymmetry_mitState, PDEClass> BCClass;

    BCClass bc(pde);

    bc.dump( 2, output );
  }

  {
    typedef BCNavierStokes2D<BCTypeWallNoSlipIsothermal_mitState, PDEClass> BCClass;
    const Real Twall = 300.0;

    BCClass bc(pde, Twall);

    bc.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
