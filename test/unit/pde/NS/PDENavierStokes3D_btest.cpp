// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDENavierStokes3D_btest
//
// test of 3-D compressible Navier-Stokes PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/list.hpp>

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "Topology/Dimension.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/Q3DPrimitiveSurrogate.h"
#include "pde/NS/Q3DConservative.h"
#include "pde/NS/PDENavierStokes3D.h"

//Explicitly instantiate the class so coverage information is correct
namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
//class QTypeEntropy {};

template class TraitsSizeNavierStokes<PhysD3>;
template class TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel>;
template class PDENavierStokes3D< TraitsSizeNavierStokes,
                                  TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> >;
template class PDENavierStokes3D< TraitsSizeNavierStokes,
                                  TraitsModelNavierStokes<QTypeConservative, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> >;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDENavierStokes3D_test_suite )


typedef boost::mpl::list< QTypePrimitiveRhoPressure,
                          QTypeConservative,
                          QTypePrimitiveSurrogate > QTypeList;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( static_test, QType, QTypeList )
{
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  BOOST_CHECK( PDEClass::D == 3 );
  BOOST_CHECK( PDEClass::N == 5 );
  BOOST_CHECK( ArrayQ::M == 5 );
  BOOST_CHECK( MatrixQ::M == 5 );
  BOOST_CHECK( MatrixQ::N == 5 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux, QType, QTypeList )
{
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-10;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 5 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // function tests

  Real x, y, z, time;
  Real rho, u, v, w, t, p, e0, h0;

  x = 0; y = 0; z = 0;  // not actually used in functions
  rho = 1.225;                        // kg/m^3
  u = 69.784; v = -3.231, w = 10.23;  // m/s
  t = 288.15;                         // K
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u + v*v + w*w);
  h0 = Cp*t + 0.5*(u*u + v*v + w*w);

  // set
  ArrayQ q = pde.setDOFFrom( DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );

  // unsteady conservative flux
  ArrayQ uTrue = {rho, rho*u, rho*v, rho*w, rho*e0};
  ArrayQ uCons = 0;
  pde.masterState( x, y, z, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );
  BOOST_CHECK_CLOSE( uTrue(3), uCons(3), tol );
  BOOST_CHECK_CLOSE( uTrue(4), uCons(4), tol );

  // unsteady conservative flux
  ArrayQ ftTrue = {rho, rho*u, rho*v, rho*w, rho*e0};
  ArrayQ ft = 0;
  pde.fluxAdvectiveTime( x, y, z, time, q, ft );
  for (int i=0; i<5; i++)
    BOOST_CHECK_CLOSE( ftTrue(i), ft(i), tol );

  pde.fluxAdvectiveTime( x, y, z, time, q, ft );
  for (int i=0; i<5; i++)
    BOOST_CHECK_CLOSE( 2*ftTrue(i), ft(i), tol );

  // advective flux
  ArrayQ fTrue = {rho*u, rho*u*u + p, rho*u*v    , rho*u*w    , rho*u*h0};
  ArrayQ gTrue = {rho*v, rho*v*u    , rho*v*v + p, rho*v*w    , rho*v*h0};
  ArrayQ hTrue = {rho*w, rho*w*u    , rho*w*v    , rho*w*w + p, rho*w*h0};
  ArrayQ f, g, h;
  f = 0;   g = 0;  h = 0;
  pde.fluxAdvective( x, y, z, time, q, f, g, h );
  for (int i=0; i<5; i++)
  {
    BOOST_CHECK_CLOSE( fTrue(i), f(i), tol );
    BOOST_CHECK_CLOSE( gTrue(i), g(i), tol );
    BOOST_CHECK_CLOSE( hTrue(i), h(i), tol );
  }
  pde.fluxAdvective( x, y, z, time, q, f, g, h );
  for (int i=0; i<5; i++)
  {
    BOOST_CHECK_CLOSE( 2*fTrue(i), f(i), tol );
    BOOST_CHECK_CLOSE( 2*gTrue(i), g(i), tol );
    BOOST_CHECK_CLOSE( 2*hTrue(i), h(i), tol );
  }

  ArrayQ qx, qy, qz;
  Real rhox, ux, vx, wx, px, tx;
  Real rhoy, uy, vy, wy, py, ty;
  Real rhoz, uz, vz, wz, pz, tz;

  if ( std::is_same<QType,QTypePrimitiveRhoPressure>::value )
  {
    rhox = 0.37, ux = -0.85, vx = 0.09, wx =  0.36, px = 1.71;
    rhoy = 2.31, uy =  1.65, vy = 0.87, wy = -1.07, py = 0.29;
    rhoz = 0.45, uz =  0.12, vz = 1.27, wz =  0.07, pz = 2.12;
    tx = t*(px/p - rhox/rho);
    ty = t*(py/p - rhoy/rho);
    tz = t*(pz/p - rhoz/rho);

    qx = {rhox, ux, vx, wx, px};
    qy = {rhoy, uy, vy, wy, py};
    qz = {rhoz, uz, vz, wz, pz};
  }
  else if ( std::is_same<QType,QTypePrimitiveSurrogate>::value )
  {
    rhox = 0.37, ux = -0.85, vx = 0.09, wx =  0.36, tx = 1.71;
    rhoy = 2.31, uy =  1.65, vy = 0.87, wy = -1.07, ty = 0.29;
    rhoz = 0.45, uz =  0.12, vz = 1.27, wz =  0.07, tz = 2.12;

    qx = {rhox, ux, vx, wx, tx};
    qy = {rhoy, uy, vy, wy, ty};
    qz = {rhoz, uz, vz, wz, tz};
  }
  else if ( std::is_same<QType,QTypeConservative>::value )
  {
    rhox = 0.37, ux = -0.85, vx = 0.09, wx =  0.36, tx = 1.71;
    rhoy = 2.31, uy =  1.65, vy = 0.87, wy = -1.07, ty = 0.29;
    rhoz = 0.45, uz =  0.12, vz = 1.27, wz =  0.07, tz = 2.12;

    Real e0x = Cv*tx + (u*ux + v*vx + w*wx);
    Real e0y = Cv*ty + (u*uy + v*vy + w*wy);
    Real e0z = Cv*tz + (u*uz + v*vz + w*wz);

    qx = {rhox, rho*ux + rhox*u, rho*vx + rhox*v, rho*wx + rhox*w, rho*e0x + rhox*e0};
    qy = {rhoy, rho*uy + rhoy*u, rho*vy + rhoy*v, rho*wy + rhoy*w, rho*e0y + rhoy*e0};
    qz = {rhoz, rho*uz + rhoz*u, rho*vz + rhoz*v, rho*wz + rhoz*w, rho*e0z + rhoz*e0};
  }
  else // Need to add the QType to this list
    BOOST_REQUIRE( false );


  f = 0; fTrue = 0;
  g = 0; gTrue = 0;
  h = 0; hTrue = 0;
  pde.fluxViscous(x, y, z, time, q, qx, qy, qz, f, g, h);

  Real lambda = -2./3. * muRef;
  Real k = muRef * Cp / Prandtl;

  Real tauxx = muRef*(2*ux   ) + lambda*(ux + vy + wz);
  Real tauxy = muRef*(uy + vx);
  Real tauyy = muRef*(2*vy   ) + lambda*(ux + vy + wz);
  Real tauxz = muRef*(uz + wx);
  Real tauyz = muRef*(vz + wy);
  Real tauzz = muRef*(2*wz   ) + lambda*(ux + vy + wz);

  fTrue(1) -= tauxx;
  fTrue(2) -= tauxy;
  fTrue(3) -= tauxz;
  fTrue(4) -= k*tx + u*tauxx + v*tauxy + w*tauxz;

  gTrue(1) -= tauxy;
  gTrue(2) -= tauyy;
  gTrue(3) -= tauyz;
  gTrue(4) -= k*ty + u*tauxy + v*tauyy + w*tauyz;

  hTrue(1) -= tauxz;
  hTrue(2) -= tauyz;
  hTrue(3) -= tauzz;
  hTrue(4) -= k*tz + u*tauxz + v*tauyz + w*tauzz;

  for (int i=0; i<5; i++)
  {
    BOOST_CHECK_CLOSE( fTrue(i), f(i), tol );
    BOOST_CHECK_CLOSE( gTrue(i), g(i), tol );
    BOOST_CHECK_CLOSE( hTrue(i), h(i), tol );
  }
  pde.fluxViscous(x, y, z, time, q, qx, qy, qz, f, g, h);
  for (int i=0; i<5; i++)
  {
    BOOST_CHECK_CLOSE( 2*fTrue(i), f(i), tol );
    BOOST_CHECK_CLOSE( 2*gTrue(i), g(i), tol );
    BOOST_CHECK_CLOSE( 2*hTrue(i), h(i), tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( jacobianMasterState_test, QType, QTypeList )
{
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  const Real small_tol = 5.e-8;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // function tests

  Real rho, u, v, w, t;

  rho = 1.225;                        // kg/m^3
  u = 69.784; v = -3.231, w = 10.23;  // m/s
  t = 288.15;                         // K

  // set
  ArrayQ q;
  q = pde.setDOFFrom( DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );

  Real x = 0, y = 0, z = 0, time = 0;

  // conservative flux
  ArrayQ uCons0, uCons1;
  MatrixQ dudq_diff[3] = {0,0};

  std::vector<Real> step = {1e-3, 5e-4};
  std::vector<Real> rate_range = {1.9, 2.1};

  for (std::size_t istep = 0; istep < step.size(); istep++ )
  {
    for (int ivar = 0; ivar < ArrayQ::M; ivar++)
    {
      q[ivar] -= step[istep];
      uCons0 = 0;
      pde.masterState( x, y, z, time, q, uCons0 );

      q[ivar] += 2*step[istep];
      uCons1 = 0;
      pde.masterState( x, y, z, time, q, uCons1 );

      q[ivar] -= step[istep];

      for (int iEq = 0; iEq < pde.N; iEq++)
        dudq_diff[istep](iEq,ivar) = (uCons1[iEq] - uCons0[iEq])/(2*step[istep]);
    }
  }

  MatrixQ dudq = 0;
  pde.jacobianMasterState(x, y, z, time, q, dudq);

  for (int iEq = 0; iEq < pde.N; iEq++)
  {
    for (int ivar = 0; ivar < ArrayQ::M; ivar++)
    {
      Real err_vec[2] = { fabs( dudq(iEq,ivar) - dudq_diff[0](iEq,ivar) ),
                          fabs( dudq(iEq,ivar) - dudq_diff[1](iEq,ivar) )};
#if 0
      std::cout << "dudq(iEq,ivar) = " << dudq(iEq,ivar) <<
                   " dudq_diff[0] " << dudq_diff[0](iEq,ivar) <<
                   " dudq_diff[1] " << dudq_diff[1](iEq,ivar) << std::endl;
#endif
      // Error in finite-difference jacobian is either zero as for linear or quadratic residuals,
      // or is nonzero for general nonlinear residual where we need to check error convergence rate
      if (err_vec[0] > small_tol && err_vec[1] > small_tol)
      {
        Real rate = log(err_vec[1]/err_vec[0])/log(step[1]/step[0]);

        BOOST_CHECK_MESSAGE( rate >= rate_range[0] && rate <= rate_range[1],
                             "Rate check failed at : ivar = " << ivar <<
                             " iEq = " << iEq << ": rate = " << rate <<
                             ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" );
      }

    }
  }
}


//----------------------------------------------------------------------------//
// NOTE: currently checked only as 2D case (w = 0; all d/dz are zero)
BOOST_AUTO_TEST_CASE( StrongViscousFlux_test_xy )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // function tests

  Real x, y, z, time;
  Real rho, u, v, w, t, p;

  x = 0; y = 0; z = 0;   // not actually used in functions
  rho = 1.225;                    // kg/m^3
  u = 69.784; v = -3.231, w = 0;  // m/s
  t = 288.15;                     // K
  p  = R*rho*t;

  // set
  Real qDataPrim[5] = {rho, u, v, w, t};
  string qNamePrim[5] = {"Density", "VelocityX", "VelocityY", "VelocityZ", "Temperature"};
  Real qData[5] = {rho, u, v, w, p};
  ArrayQ qTrue(qData, 5);
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;

  Real rhoz = 0, uz = 0, vz = 0, pz = 0;
  Real wx = 0, wy = 0, wz = 0;

  Real rhoxx = -0.253; Real rhoxy = 0.782; Real rhoyy = 1.08;
  Real uxx = 1.02; Real uxy = -0.95; Real uyy =-0.42;
  Real vxx = 0.99; Real vxy = -0.92; Real vyy =-0.44;
  Real pxx = 2.99; Real pxy = -1.92; Real pyy =-2.44;

  Real rhoxz = 0, rhoyz = 0, rhozz = 0;
  Real uxz = 0, uyz = 0, uzz = 0;
  Real vxz = 0, vyz = 0, vzz = 0;
  Real pxz = 0, pyz = 0, pzz = 0;
  Real wxx = 0, wxy = 0, wyy = 0, wxz = 0, wyz = 0, wzz = 0;

  ArrayQ qx = {rhox, ux, vx, wx, px};
  ArrayQ qy = {rhoy, uy, vy, wy, py};
  ArrayQ qz = {rhoz, uz, vz, wz, pz};
  ArrayQ qxx = {rhoxx, uxx, vxx, wxx, pxx};
  ArrayQ qxy = {rhoxy, uxy, vxy, wxy, pxy};
  ArrayQ qyy = {rhoyy, uyy, vyy, wyy, pyy};
  ArrayQ qxz = {rhoxz, uxz, vxz, wxz, pxz};
  ArrayQ qyz = {rhoyz, uyz, vyz, wyz, pyz};
  ArrayQ qzz = {rhozz, uzz, vzz, wzz, pzz};

  ArrayQ strongVisc = 0;
  ArrayQ strongViscTrue = 0;
  pde.strongFluxViscous(x, y, z, time, q, qx, qy, qz, qxx, qxy, qyy, qxz, qyz, qzz, strongVisc);

  strongViscTrue(0) = 0;
  strongViscTrue(1) = -0.000011330333333333333;
  strongViscTrue(2) = -1.5504666666666666666e-6;
  strongViscTrue(3) = 0;
  strongViscTrue(4) = 4.84821037760516;
  BOOST_CHECK_CLOSE(strongViscTrue(0), strongVisc(0), tol);
  BOOST_CHECK_CLOSE(strongViscTrue(1), strongVisc(1), tol);
  BOOST_CHECK_CLOSE(strongViscTrue(2), strongVisc(2), tol);
  BOOST_CHECK_CLOSE(strongViscTrue(3), strongVisc(3), tol);
  BOOST_CHECK_CLOSE(strongViscTrue(4), strongVisc(4), tol);

  for (int i=0; i<5; i++)
    BOOST_CHECK_CLOSE( strongViscTrue(i), strongVisc(i), tol);

  pde.strongFluxViscous(x, y, z, time, q, qx, qy, qz, qxx, qxy, qyy, qxz, qyz, qzz, strongVisc);

  for (int i=0; i<5; i++)
    BOOST_CHECK_CLOSE( 2*strongViscTrue(i), strongVisc(i), tol);
}


//----------------------------------------------------------------------------//
// NOTE: currently checked only as 2D case (v = 0; all d/dy are zero)
// copy/paste from above (*_test_xy) and then judicious swap {w,z} <--> {v,y}
BOOST_AUTO_TEST_CASE( StrongViscousFlux_test_xz )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // function tests

  Real x, y, z, time;
  Real rho, u, v, w, t, p;

  x = 0; y = 0; z = 0;   // not actually used in functions
  rho = 1.225;                    // kg/m^3
  u = 69.784; w = -3.231, v = 0;  // m/s
  t = 288.15;                     // K
  p  = R*rho*t;

  // set
  Real qDataPrim[5] = {rho, u, v, w, t};
  string qNamePrim[5] = {"Density", "VelocityX", "VelocityY", "VelocityZ", "Temperature"};
  Real qData[5] = {rho, u, v, w, p};
  ArrayQ qTrue(qData, 5);
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

  Real rhox = 0.01; Real rhoz = -0.025;
  Real ux = 0.049; Real uz = 0.072;
  Real wx = 0.14; Real wz = -0.024;
  Real px = 0.002; Real pz = -4.23;

  Real rhoy = 0, uy = 0, wy = 0, py = 0;
  Real vx = 0, vy = 0, vz = 0;

  Real rhoxx = -0.253; Real rhoxz = 0.782; Real rhozz = 1.08;
  Real uxx = 1.02; Real uxz = -0.95; Real uzz =-0.42;
  Real wxx = 0.99; Real wxz = -0.92; Real wzz =-0.44;
  Real pxx = 2.99; Real pxz = -1.92; Real pzz =-2.44;

  Real rhoxy = 0, rhoyy = 0, rhoyz = 0;
  Real uxy = 0, uyy = 0, uyz = 0;
  Real wxy = 0, wyy = 0, wyz = 0;
  Real pxy = 0, pyy = 0, pyz = 0;
  Real vxx = 0, vxy = 0, vyy = 0, vxz = 0, vyz = 0, vzz = 0;

  ArrayQ qx = {rhox, ux, vx, wx, px};
  ArrayQ qy = {rhoy, uy, vy, wy, py};
  ArrayQ qz = {rhoz, uz, vz, wz, pz};
  ArrayQ qxx = {rhoxx, uxx, vxx, wxx, pxx};
  ArrayQ qxy = {rhoxy, uxy, vxy, wxy, pxy};
  ArrayQ qyy = {rhoyy, uyy, vyy, wyy, pyy};
  ArrayQ qxz = {rhoxz, uxz, vxz, wxz, pxz};
  ArrayQ qyz = {rhoyz, uyz, vyz, wyz, pyz};
  ArrayQ qzz = {rhozz, uzz, vzz, wzz, pzz};

  ArrayQ strongVisc = 0;
  ArrayQ strongViscTrue = 0;
  pde.strongFluxViscous(x, y, z, time, q, qx, qy, qz, qxx, qxy, qyy, qxz, qyz, qzz, strongVisc);

  strongViscTrue(0) = 0;
  strongViscTrue(1) = -0.000011330333333333333;
  strongViscTrue(2) = 0;
  strongViscTrue(3) = -1.5504666666666666666e-6;
  strongViscTrue(4) = 4.84821037760516;

  for (int i=0; i<5; i++)
    BOOST_CHECK_CLOSE( strongViscTrue(i), strongVisc(i), tol);

  pde.strongFluxViscous(x, y, z, time, q, qx, qy, qz, qxx, qxy, qyy, qxz, qyz, qzz, strongVisc);

  for (int i=0; i<5; i++)
    BOOST_CHECK_CLOSE( 2*strongViscTrue(i), strongVisc(i), tol);
}


//----------------------------------------------------------------------------//
// NOTE: currently checked only as 2D case (u = 0; all d/dx are zero)
// copy/paste from above (*_test_xy) and then judicious swap {w,z} <--> {u,x}
BOOST_AUTO_TEST_CASE( StrongViscousFlux_test_yz )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // function tests

  Real x, y, z, time;
  Real rho, u, v, w, t, p;

  x = 0; y = 0; z = 0;   // not actually used in functions
  rho = 1.225;                    // kg/m^3
  w = 69.784; v = -3.231, u = 0;  // m/s
  t = 288.15;                     // K
  p  = R*rho*t;

  // set
  Real qDataPrim[5] = {rho, u, v, w, t};
  string qNamePrim[5] = {"Density", "VelocityX", "VelocityY", "VelocityZ", "Temperature"};
  Real qData[5] = {rho, u, v, w, p};
  ArrayQ qTrue(qData, 5);
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

  Real rhoz = 0.01; Real rhoy = -0.025;
  Real vz = 0.14;   Real vy = -0.024;
  Real wz = 0.049;  Real wy = 0.072;
  Real pz = 0.002;  Real py = -4.23;

  Real rhox = 0, wx = 0, vx = 0, px = 0;
  Real ux = 0, uy = 0, uz = 0;

  Real rhozz = -0.253; Real rhoyz = 0.782; Real rhoyy = 1.08;
  Real wzz = 1.02; Real wyz = -0.95; Real wyy =-0.42;
  Real vzz = 0.99; Real vyz = -0.92; Real vyy =-0.44;
  Real pzz = 2.99; Real pyz = -1.92; Real pyy =-2.44;

  Real rhoxx = 0, rhoxy = 0, rhoxz = 0;
  Real wxx = 0, wxy = 0, wxz = 0;
  Real vxx = 0, vxy = 0, vxz = 0;
  Real pxx = 0, pxy = 0, pxz = 0;
  Real uxx = 0, uxy = 0, uyy = 0, uxz = 0, uyz = 0, uzz = 0;

  ArrayQ qx = {rhox, ux, vx, wx, px};
  ArrayQ qy = {rhoy, uy, vy, wy, py};
  ArrayQ qz = {rhoz, uz, vz, wz, pz};
  ArrayQ qxx = {rhoxx, uxx, vxx, wxx, pxx};
  ArrayQ qxy = {rhoxy, uxy, vxy, wxy, pxy};
  ArrayQ qyy = {rhoyy, uyy, vyy, wyy, pyy};
  ArrayQ qxz = {rhoxz, uxz, vxz, wxz, pxz};
  ArrayQ qyz = {rhoyz, uyz, vyz, wyz, pyz};
  ArrayQ qzz = {rhozz, uzz, vzz, wzz, pzz};

  ArrayQ strongVisc = 0;
  ArrayQ strongViscTrue = 0;
  pde.strongFluxViscous(x, y, z, time, q, qx, qy, qz, qxx, qxy, qyy, qxz, qyz, qzz, strongVisc);

  strongViscTrue(0) = 0;
  strongViscTrue(1) = 0;
  strongViscTrue(2) = -1.5504666666666666666e-6;
  strongViscTrue(3) = -0.000011330333333333333;
  strongViscTrue(4) = 4.84821037760516;

  for (int i=0; i<5; i++)
    BOOST_CHECK_CLOSE( strongViscTrue(i), strongVisc(i), tol);

  pde.strongFluxViscous(x, y, z, time, q, qx, qy, qz, qxx, qxy, qyy, qxz, qyz, qzz, strongVisc);

  for (int i=0; i<5; i++)
    BOOST_CHECK_CLOSE( 2*strongViscTrue(i), strongVisc(i), tol);

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( diffusionViscousGradient_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-10;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // function tests

  Real x, y, z, time;
  Real rho, u, v, w, t, p;

  x = 0; y = 0; z = 0;   // not actually used in functions
  rho = 1.225;                        // kg/m^3
  u = 6.974; v = -3.231; w = 69.784;  // m/s
  t = 288.15;                         // K
  p  = R*rho*t;

  Real e = gas.energy(rho, t);
  Real E = e + 0.5*(u*u + v*v + w*w);

  // set
  ArrayQ q = pde.setDOFFrom( DensityVelocityPressure3D<Real>(rho, u, v, w, p) );


  Real rhox = 0.0421, rhoy = 0.0127, rhoz = 0.691;
  Real ux =  0.0521, uy = -0.0124, uz = -0.145;
  Real vx = -0.0312, vy = -0.0022, vz =  0.089;
  Real wx = -0.0546, wy = -0.0735, wz =  0.032;
  Real px =  0.0120, py = -3.2300, pz =  0.052;

  Real rhoxx = -0.253, uxx =  1.02, vxx =  0.99, wxx =  0.99, pxx =  2.99;
  Real rhoxy =  0.782, uxy = -0.95, vxy = -0.92, wxy =  0.82, pxy = -1.92;
  Real rhoyy =  1.080, uyy = -0.42, vyy = -0.44, wyy = -0.42, pyy = -1.44;
  Real rhoxz =  0.172, uxz = -0.95, vxz =  0.23, wxz = -0.92, pxz = -1.82;
  Real rhoyz =  0.602, uyz =  0.87, vyz = -0.35, wyz =  0.78, pyz =  1.04;
  Real rhozz =  1.267, uzz = -0.32, vzz =  0.65, wzz =  0.14, pzz =  1.52;

  ArrayQ qx = {rhox, ux, vx, wx, px};
  ArrayQ qy = {rhoy, uy, vy, wy, py};
  ArrayQ qz = {rhoz, uz, vz, wz, pz};

  // t = p/(rho*R)
  Real tx = px/(rho*R) - p/(rho*rho*R)*rhox;
  Real ty = py/(rho*R) - p/(rho*rho*R)*rhoy;
  Real tz = pz/(rho*R) - p/(rho*rho*R)*rhoz;

  Real ex = Cv*tx;
  Real ey = Cv*ty;
  Real ez = Cv*tz;

  Real Ex = ex + u*ux + v*vx + w*wx;
  Real Ey = ey + u*uy + v*vy + w*wy;
  Real Ez = ez + u*uz + v*vz + w*wz;

  ArrayQ qxx = {rhoxx, uxx, vxx, wxx, pxx};
  ArrayQ qxy = {rhoxy, uxy, vxy, wxy, pxy};
  ArrayQ qyy = {rhoyy, uyy, vyy, wyy, pyy};
  ArrayQ qxz = {rhoxz, uxz, vxz, wxz, pxz};
  ArrayQ qyz = {rhoyz, uyz, vyz, wyz, pyz};
  ArrayQ qzz = {rhozz, uzz, vzz, wzz, pzz};

  Real rho2 = rho*rho;
  Real rho3 = rho*rho*rho;

  Real txx = pxx/(rho*R) - px/(rho2*R)*rhox - px/(rho2*R)*rhox + 2*p/(rho3*R)*rhox*rhox - p/(rho2*R)*rhoxx;
  Real txy = pxy/(rho*R) - py/(rho2*R)*rhox - px/(rho2*R)*rhoy + 2*p/(rho3*R)*rhox*rhoy - p/(rho2*R)*rhoxy;
  Real tyy = pyy/(rho*R) - py/(rho2*R)*rhoy - py/(rho2*R)*rhoy + 2*p/(rho3*R)*rhoy*rhoy - p/(rho2*R)*rhoyy;
  Real txz = pxz/(rho*R) - px/(rho2*R)*rhoz - pz/(rho2*R)*rhox + 2*p/(rho3*R)*rhox*rhoz - p/(rho2*R)*rhoxz;
  Real tyz = pyz/(rho*R) - py/(rho2*R)*rhoz - pz/(rho2*R)*rhoy + 2*p/(rho3*R)*rhoy*rhoz - p/(rho2*R)*rhoyz;
  Real tzz = pzz/(rho*R) - pz/(rho2*R)*rhoz - pz/(rho2*R)*rhoz + 2*p/(rho3*R)*rhoz*rhoz - p/(rho2*R)*rhozz;

  Real exx = Cv*txx;
  Real exy = Cv*txy;
  Real eyy = Cv*tyy;
  Real exz = Cv*txz;
  Real eyz = Cv*tyz;
  Real ezz = Cv*tzz;

  Real Exx = exx + ux*ux + u*uxx + vx*vx + v*vxx + wx*wx + w*wxx;
  Real Exy = exy + ux*uy + u*uxy + vx*vy + v*vxy + wx*wy + w*wxy;
  Real Eyy = eyy + uy*uy + u*uyy + vy*vy + v*vyy + wy*wy + w*wyy;
  Real Exz = exz + ux*uz + u*uxz + vx*vz + v*vxz + wx*wz + w*wxz;
  Real Eyz = eyz + uy*uz + u*uyz + vy*vz + v*vyz + wy*wz + w*wyz;
  Real Ezz = ezz + uz*uz + u*uzz + vz*vz + v*vzz + wz*wz + w*wzz;

  ArrayQ Ux=0, Uy=0, Uz=0;
  pde.strongFluxAdvectiveTime(x, y, z, time, q, qx, Ux);
  pde.strongFluxAdvectiveTime(x, y, z, time, q, qy, Uy);
  pde.strongFluxAdvectiveTime(x, y, z, time, q, qz, Uz);

  ArrayQ Uxx = {rhoxx,
                rhoxx*u + rhox*ux + rhox*ux + rho*uxx,
                rhoxx*v + rhox*vx + rhox*vx + rho*vxx,
                rhoxx*w + rhox*wx + rhox*wx + rho*wxx,
                rhoxx*E + rhox*Ex + rhox*Ex + rho*Exx };

  ArrayQ Uxy = {rhoxy,
                rhoxy*u + rhoy*ux + rhox*uy + rho*uxy,
                rhoxy*v + rhoy*vx + rhox*vy + rho*vxy,
                rhoxy*w + rhoy*wx + rhox*wy + rho*wxy,
                rhoxy*E + rhoy*Ex + rhox*Ey + rho*Exy };

  ArrayQ Uyy = {rhoyy,
                rhoyy*u + rhoy*uy + rhoy*uy + rho*uyy,
                rhoyy*v + rhoy*vy + rhoy*vy + rho*vyy,
                rhoyy*w + rhoy*wy + rhoy*wy + rho*wyy,
                rhoyy*E + rhoy*Ey + rhoy*Ey + rho*Eyy };

  ArrayQ Uxz = {rhoxz,
                rhoxz*u + rhox*uz + rhoz*ux + rho*uxz,
                rhoxz*v + rhox*vz + rhoz*vx + rho*vxz,
                rhoxz*w + rhox*wz + rhoz*wx + rho*wxz,
                rhoxz*E + rhox*Ez + rhoz*Ex + rho*Exz };

  ArrayQ Uyz = {rhoyz,
                rhoyz*u + rhoy*uz + rhoz*uy + rho*uyz,
                rhoyz*v + rhoy*vz + rhoz*vy + rho*vyz,
                rhoyz*w + rhoy*wz + rhoz*wy + rho*wyz,
                rhoyz*E + rhoy*Ez + rhoz*Ey + rho*Eyz };

  ArrayQ Uzz = {rhozz,
                rhozz*u + rhoz*uz + rhoz*uz + rho*uzz,
                rhozz*v + rhoz*vz + rhoz*vz + rho*vzz,
                rhozz*w + rhoz*wz + rhoz*wz + rho*wzz,
                rhozz*E + rhoz*Ez + rhoz*Ez + rho*Ezz };

  ArrayQ strongViscTrue = 0;
  pde.strongFluxViscous(x, y, z, time, q, qx, qy, qz,
                        qxx,
                        qxy, qyy,
                        qxz, qyz, qzz, strongViscTrue);

  MatrixQ kxx = 0, kxy = 0, kxz = 0;
  MatrixQ kyx = 0, kyy = 0, kyz = 0;
  MatrixQ kzx = 0, kzy = 0, kzz = 0;

  pde.diffusionViscous( x, y, z, time, q, qx, qy, qz,
                        kxx, kxy, kxz,
                        kyx, kyy, kyz,
                        kzx, kzy, kzz );

  MatrixQ kxx_x = 0, kxy_x = 0, kxz_x = 0, kyx_x = 0, kzx_x = 0;
  MatrixQ kxy_y = 0, kyx_y = 0, kyy_y = 0, kyz_y = 0, kzy_y = 0;
  MatrixQ kxz_z = 0, kyz_z = 0, kzx_z = 0, kzy_z = 0, kzz_z = 0;

  pde.diffusionViscousGradient( x, y, z, time, q, qx, qy, qz,
                                qxx,
                                qxy, qyy,
                                qxz, qyz, qzz,
                                kxx_x, kxy_x, kxz_x, kyx_x, kzx_x,
                                kxy_y, kyx_y, kyy_y, kyz_y, kzy_y,
                                kxz_z, kyz_z, kzx_z, kzy_z, kzz_z );

  ArrayQ strongVisc = 0;

  // d(kxx*Ux + kxy*Uy + kxz*Uz)/dx = kxx_x*Ux + kxy_x*Uy + kxz_x*Uz + kxx*Uxx + kxy*Uxy + kxz*Uxz
  // d(kyx*Ux + kyy*Uy + kyz*Uz)/dy = kyx_y*Ux + kyy_y*Uy + kyz_y*Uz + kyx*Uxy + kyy*Uyy + kyz*Uyz
  // d(kzx*Ux + kzy*Uy + kzz*Uz)/dz = kzx_z*Ux + kzy_z*Uy + kzz_z*Uz + kzx*Uxz + kzy*Uyz + kzz*Uzz

  strongVisc = -(kxx_x*Ux + kxy_x*Uy + kxz_x*Uz + kxx*Uxx + kxy*Uxy + kxz*Uxz
             +   kyx_y*Ux + kyy_y*Uy + kyz_y*Uz + kyx*Uxy + kyy*Uyy + kyz*Uyz
             +   kzx_z*Ux + kzy_z*Uy + kzz_z*Uz + kzx*Uxz + kzy*Uyz + kzz*Uzz);

  SANS_CHECK_CLOSE(strongViscTrue(0), strongVisc(0), small_tol, close_tol);
  SANS_CHECK_CLOSE(strongViscTrue(1), strongVisc(1), small_tol, close_tol);
  SANS_CHECK_CLOSE(strongViscTrue(2), strongVisc(2), small_tol, close_tol);
  SANS_CHECK_CLOSE(strongViscTrue(3), strongVisc(3), small_tol, close_tol);
  SANS_CHECK_CLOSE(strongViscTrue(4), strongVisc(4), small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxRoe_test, QType, QTypeList )
{
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 2.e-11;

  const Real gamma = 1.4;
  const Real R     = 0.4;           // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // Roe flux function test (same as Euler Roe test, from Roe3D.nb)

  Real x, y, z, time;
  Real nx, ny, nz;
  Real rhoL, uL, vL, wL, tL, pL, h0L;
  Real rhoR, uR, vR, wR, tR, pR, h0R;

  x = y = z = time = 0;   // not actually used in functions
  nx = 2.0/7.0; ny = 1.0/7.0; nz = sqrt(1.0 - nx*nx - ny*ny);
  rhoL = 1.034; uL = 3.26; vL = -2.17; wL = 3.56; tL = 5.78;
  rhoR = 0.973; uR = 1.79; vR =  0.93; wR = 5.23; tR = 6.13;

  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL + vL*vL + wL*wL);

  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR + vR*vR + wR*wR);

  // set
  ArrayQ qL = pde.setDOFFrom( DensityVelocityTemperature3D<Real>(rhoL, uL, vL, wL, tL) );
  ArrayQ qR = pde.setDOFFrom( DensityVelocityTemperature3D<Real>(rhoR, uR, vR, wR, tR) );

  // advective normal flux (average)
  ArrayQ fxL = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*vL     , rhoL*uL*wL     , rhoL*uL*h0L};
  ArrayQ fyL = {rhoL*vL, rhoL*vL*uL     , rhoL*vL*vL + pL, rhoL*vL*wL     , rhoL*vL*h0L};
  ArrayQ fzL = {rhoL*wL, rhoL*wL*uL     , rhoL*wL*vL     , rhoL*wL*wL + pL, rhoL*wL*h0L};

  ArrayQ fxR = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*vR     , rhoR*uR*wR     , rhoR*uR*h0R};
  ArrayQ fyR = {rhoR*vR, rhoR*vR*uR     , rhoR*vR*vR + pR, rhoR*vR*wR     , rhoR*vR*h0R};
  ArrayQ fzR = {rhoR*wR, rhoR*wR*uR     , rhoR*wR*vR     , rhoR*wR*wR + pR, rhoR*wR*h0R};

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < PDEClass::N; k++)
    fnTrue[k] = 0.5*(nx*(fxL[k] + fxR[k]) + ny*(fyL[k] + fyR[k]) + nz*(fzL[k] + fzR[k]));

#if 1
  // Exact values for Roe scheme from Roe.nb
  fnTrue[0] -= 0.65558457182422097471369683881548;
  fnTrue[1] -= -1.8638923372862592525182437515145;
  fnTrue[2] -=  7.0186535948930936601201334787757;
  fnTrue[3] -=  6.8854604538052794714508533474917;
  fnTrue[4] -= 20.499927239215613126430884098724;
#endif

  ArrayQ fn;
  fn = 0;
  pde.fluxAdvectiveUpwind( x, y, z, time, qL, qR, nx, ny, nz, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( fnTrue(3), fn(3), tol );
  BOOST_CHECK_CLOSE( fnTrue(4), fn(4), tol );
}


//----------------------------------------------------------------------------//
// identical to PDEEuler3D_best since jacobianFluxAdvectiveAbsoluteValue inherited
BOOST_AUTO_TEST_CASE_TEMPLATE( jacobianFluxAdvectiveAbsoluteValue_test, QType, QTypeList )
{
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  const Real small_tol = 1.e-14;
  const Real close_tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, z, time;
  Real nx, ny, nz;
  Real rho, u, v, w, t;

  x = y = z = time = 0;   // not actually used in functions
  rho = 1.034; u = 1.26; v = -2.17; w = 0.56; t = 5.78;

  ArrayQ q = pde.setDOFFrom( DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );

  MatrixQ mtx, mtxTrue;

  // {qn - c, qn, qn + c} = {neg, neg, neg}

  nx = -0.856;  ny = 0.48;  nz = 0.192;   // Note: magnitude = 1

  mtxTrue =
    {{0., 0.856, -0.48, -0.192, 0.},
     {-1.40427728, 2.659776, 0.138208, -0.433664, 0.3424},
     {3.7328592, -1.6156, 2.6376, 0.52416, -0.192},
     {-1.38090624, 0.576128, -0.435456, 1.948128, -0.0768},
     {-20.2774083792, 8.74150424, -3.72361248, -2.63906496, 2.817696}};

  mtx = 0;
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, nx, ny, nz, mtx );

  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
      SANS_CHECK_CLOSE( mtxTrue(i,j), mtx(i,j), small_tol, close_tol );

  // {qn - c, qn, qn + c} = {neg, neg, pos}

  nx = -0.192;  ny = 0.48;  nz = -0.856;   // Note: magnitude = 1

  mtxTrue =
    {{0.05029920307361089, 0.1824919733978881, -0.4606177577277211, 0.8362543552680354, 0.004477371666327435},
     {-1.921398782191514, 1.899336237370957, -0.4204175689463774, 1.017493211701233, 0.08089487297855683},
     {3.125167774490924, -0.1622984594974613, 2.362518519792777, -1.724203761090408, -0.1978493582133912},
     {0.09514114318471302, -0.3145857638476372, 0.4552126481553504, 2.069847500399908, 0.3380120014936149},
     {-17.34733614186995, 1.221534440499240, -3.780976817210479, 9.198573194484009, 2.504860322410591}};

  mtx = 0;
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, nx, ny, nz, mtx );

  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
      SANS_CHECK_CLOSE( mtxTrue(i,j), mtx(i,j), small_tol, close_tol );

  // {qn - c, qn, qn + c} = {neg, pos, pos}

  nx = -0.48;  ny = -0.856;  nz = -0.192;   // Note: magnitude = 1

  mtxTrue =
    {{0.6833178495735146, -0.4073576217113168, -0.3695186359806880, -0.1674683835739656, 0.08080955159712328},
     {-0.6264461084779726, 0.9365814097471189, -0.4621211994809992, -0.08230531611978917, -0.02039499966720123},
     {0.9229712316480684, 1.427262129147915, 1.953247676052250, 0.5929300364992487, -0.3933068721443357},
     {-0.2764438438150722, -0.1062594629169862, -0.2055415234073182, 1.102899644071942, -0.003632664977441590},
     {-5.157984981879059, -5.369524110454105, -4.219706655995295, -2.215713855583411, 2.357775203603017}};

  mtx = 0;
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, nx, ny, nz, mtx );

  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
      SANS_CHECK_CLOSE( mtxTrue(i,j), mtx(i,j), small_tol, close_tol );

  // {qn - c, qn, qn + c} = {pos, pos, pos}

  nx = -0.192;  ny = -0.856;  nz = 0.48;   // Note: magnitude = 1

  mtxTrue =
    {{0, -0.192, -0.856, 0.48, 0},
     {-2.62817184, 1.739248, -1.245216, 0.647808, -0.0768},
     {2.95749888, 0.848064, 2.998912, -0.849856, -0.3424},
     {-0.4206944, -0.34944, -0.06272, 2.04568, 0.192},
     {-18.985386532, -3.1379712, -8.1202156, 5.0484784, 2.63816}};

  mtx = 0;
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, nx, ny, nz, mtx );

  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
      SANS_CHECK_CLOSE( mtxTrue(i,j), mtx(i,j), small_tol, close_tol );

  // {qn - c, qn, qn + c} = {pos, pos, pos}

  nx = u;  ny = v;  nz = w;   // Note: magnitude > 1

  mtxTrue =
  {{0, 1.26, -2.17, 0.56, 0},
   {-6.6629808, 7.56266, -1.64052, 0.42336, 0.504},
   {11.4751336, -1.64052, 9.43544, -0.72912, -0.868},
   {-2.9613248, 0.42336, -0.72912, 6.79826, 0.224},
   {-66.596955803, 11.0287926, -18.9940317, 4.9016856, 9.25414}};

  mtx = 0;
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, nx, ny, nz, mtx );

  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
      SANS_CHECK_CLOSE( mtxTrue(i,j), mtx(i,j), small_tol, close_tol );

#if 0
  printf( "btest: mtx = \n" );
  for (int i = 0; i < 5; i++)
  {
    for (int j = 0; j < 5; j++)
      printf( " %13.5e", mtx(i,j) );
    printf( "\n" );
  }
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( viscous )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, z, time;
  Real rho, u, v, w, t, p, e0;
  Real mu, lambda, k;
  Real tauxx, tauxy, tauyy, tauxz, tauyz, tauzz;

  x = 0; y = 0; z = 0;   // not actually used in functions
  rho = 1.225;                    // kg/m^3
  u = 69.784; v = -3.231; w = 0;  // m/s
  t = 288.15;                     // K
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u + v*v + w*w);

  mu = visc.viscosity(t);
  k  = tcond.conductivity(mu);
  lambda = -(2./3.)*mu;

  Real nx =  0.35;
  Real ny = -0.78;
  Real nz =  sqrt(1.0 - nx*nx - ny*ny);

  // gradient
  Real rhox, ux, vx, wx, px, tx;
  Real rhoy, uy, vy, wy, py, ty;
  Real rhoz, uz, vz, wz, pz, tz;
  rhox = 0.37, ux = -0.85, vx = 0.09, wx =  0.36, px = 1.71;
  rhoy = 2.31, uy =  1.65, vy = 0.87, wy = -1.07, py = 0.29;
  rhoz = 0.45, uz =  0.12, vz = 1.27, wz =  0.07, pz = 2.12;
  tx = t*(px/p - rhox/rho);
  ty = t*(py/p - rhoy/rho);
  tz = t*(pz/p - rhoz/rho);

  // viscous shear stress
  tauxx = mu*(2*ux   ) + lambda*(ux + vy + wz);
  tauxy = mu*(uy + vx);
  tauyy = mu*(2*vy   ) + lambda*(ux + vy + wz);
  tauxz = mu*(uz + wx);
  tauyz = mu*(vz + wy);
  tauzz = mu*(2*wz   ) + lambda*(ux + vy + wz);

  // set
  Real qDataPrim[5] = {rho, u, v, w, t};
  string qNamePrim[5] = {"Density", "VelocityX", "VelocityY", "VelocityZ", "Temperature"};
  ArrayQ qTrue = {rho, u, v, w, p};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );

  SANS_CHECK_CLOSE( qTrue[0], q[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue[1], q[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue[2], q[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue[3], q[3], small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue[4], q[4], small_tol, close_tol );

  ArrayQ qx = {rhox, ux, vx, wx, px};
  ArrayQ qy = {rhoy, uy, vy, wy, py};
  ArrayQ qz = {rhoz, uz, vz, wz, pz};

  // viscous flux

  ArrayQ fTrue = {0, -tauxx, -tauxy, -tauxz, -(k*tx + u*tauxx + v*tauxy + w*tauxz)};
  ArrayQ gTrue = {0, -tauxy, -tauyy, -tauyz, -(k*ty + u*tauxy + v*tauyy + w*tauyz)};
  ArrayQ hTrue = {0, -tauxz, -tauyz, -tauzz, -(k*tz + u*tauxz + v*tauyz + w*tauzz)};
  ArrayQ f, g, h;
  f = 0;  g = 0;  h = 0;
  pde.fluxViscous( x, y, z, time, q, qx, qy, qz, f, g, h );
  for (int i=0; i<5; i++)
  {
    SANS_CHECK_CLOSE( fTrue(i), f(i), small_tol, close_tol );
    SANS_CHECK_CLOSE( gTrue(i), g(i), small_tol, close_tol );
    SANS_CHECK_CLOSE( hTrue(i), h(i), small_tol, close_tol );
  }
  pde.fluxViscous( x, y, z, time, q, qx, qy, qz, f, g, h );
  for (int i=0; i<5; i++)
  {
    SANS_CHECK_CLOSE( 2*fTrue(i), f(i), small_tol, close_tol );
    SANS_CHECK_CLOSE( 2*gTrue(i), g(i), small_tol, close_tol );
    SANS_CHECK_CLOSE( 2*hTrue(i), h(i), small_tol, close_tol );
  }

// reset viscous flux
  f = 0;  g = 0;  h = 0;
  pde.fluxViscous( x, y, z, time, q, qx, qy, qz, f, g, h );

  Real rhoL = 1.225;                         // kg/m^3
  Real uL = 69.784, vL = -3.231, wL = 3.56;  // m/s
  Real tL = 288.15;                          // K
  Real pL  = R*rhoL*tL;

  Real rhoR = 1.253;                         // kg/m^3
  Real uR = 71.381, vR = -2.107, wR = 5.23;  // m/s
  Real tR = 278.35;                          // K
  Real pR  = R*rhoR*tR;

  Real rhoxL = 0.37, uxL = -0.85, vxL = 0.09, wxL = 0.56, pxL = 1.71;
  Real rhoyL = 2.31, uyL =  1.65, vyL = 0.87, wyL = 1.45, pyL = 0.29;
  Real rhozL = 1.05, uzL = -2.34, vzL = 0.19, wzL = 0.49, pzL = 2.10;

  Real rhoxR = 0.41, uxR = -0.25, vxR = -0.03, wxR = 0.76, pxR = 0.61;
  Real rhoyR = 1.28, uyR =  0.21, vyR =  1.06, wyR = 0.92, pyR = 0.49;
  Real rhozR = 2.87, uzR = -0.67, vzR =  0.51, wzR = 0.38, pzR = 0.72;

  ArrayQ qL = {rhoL, uL, vL, wL, pL};
  ArrayQ qR = {rhoR, uR, vR, wR, pR};
  ArrayQ qxL = {rhoxL, uxL, vxL, wxL, pxL};
  ArrayQ qyL = {rhoyL, uyL, vyL, wyL, pyL};
  ArrayQ qzL = {rhozL, uzL, vzL, wzL, pzL};
  ArrayQ qxR = {rhoxR, uxR, vxR, wxR, pxR};
  ArrayQ qyR = {rhoyR, uyR, vyR, wyR, pyR};
  ArrayQ qzR = {rhozR, uzR, vzR, wzR, pzR};

  Real txL = tL*(pxL/pL - rhoxL/rhoL);
  Real tyL = tL*(pyL/pL - rhoyL/rhoL);
  Real tzL = tL*(pzL/pL - rhozL/rhoL);
  Real txR = tR*(pxR/pR - rhoxR/rhoR);
  Real tyR = tR*(pyR/pR - rhoyR/rhoR);
  Real tzR = tR*(pzR/pR - rhozR/rhoR);

  Real muL = visc.viscosity(tL);
  Real kL  = tcond.conductivity(muL);
  Real lambdaL = -(2./3.)*muL;

  Real muR = visc.viscosity(tR);
  Real kR  = tcond.conductivity(muR);
  Real lambdaR = -(2./3.)*muR;

  // viscous shear stress

  Real tauxxL = muL*(2*uxL   ) + lambdaL*(uxL + vyL + wzL);
  Real tauxyL = muL*(uyL + vxL) ;
  Real tauyyL = muL*(2*vyL   ) + lambdaL*(uxL + vyL + wzL);
  Real tauxzL = muL*(uzL + wxL);
  Real tauyzL = muL*(vzL + wyL);
  Real tauzzL = muL*(2*wzL   ) + lambdaL*(uxL + vyL + wzL);

  Real tauxxR = muR*(2*uxR   ) + lambdaR*(uxR + vyR + wzR);
  Real tauxyR = muR*(uyR + vxR);
  Real tauyyR = muR*(2*vyR   ) + lambdaR*(uxR + vyR + wzR);
  Real tauxzR = muR*(uzR + wxR);
  Real tauyzR = muR*(vzR + wyR);
  Real tauzzR = muR*(2*wzR   ) + lambdaR*(uxR + vyR + wzR);

  ArrayQ fLTrue = {0, -tauxxL, -tauxyL, -tauxzL, -(kL*txL + uL*tauxxL + vL*tauxyL + wL*tauxzL)};
  ArrayQ gLTrue = {0, -tauxyL, -tauyyL, -tauyzL, -(kL*tyL + uL*tauxyL + vL*tauyyL + wL*tauyzL)};
  ArrayQ hLTrue = {0, -tauxzL, -tauyzL, -tauzzL, -(kL*tzL + uL*tauxzL + vL*tauyzL + wL*tauzzL)};
  ArrayQ fRTrue = {0, -tauxxR, -tauxyR, -tauxzR, -(kR*txR + uR*tauxxR + vR*tauxyR + wR*tauxzR)};
  ArrayQ gRTrue = {0, -tauxyR, -tauyyR, -tauyzR, -(kR*tyR + uR*tauxyR + vR*tauyyR + wR*tauyzR)};
  ArrayQ hRTrue = {0, -tauxzR, -tauyzR, -tauzzR, -(kR*tzR + uR*tauxzR + vR*tauyzR + wR*tauzzR)};

  ArrayQ fnTrue = 0.5*(fLTrue + fRTrue)*nx + 0.5*(gLTrue + gRTrue)*ny + 0.5*(hLTrue + hRTrue)*nz;
  ArrayQ fn;
  fn = 0;
  pde.fluxViscous( x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, nx, ny, nz, fn );

  for (int i=0; i<5; i++)
    SANS_CHECK_CLOSE( fnTrue(i), fn(i), small_tol, close_tol );

  pde.fluxViscous( x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, nx, ny, nz, fn );

  for (int i=0; i<5; i++)
    SANS_CHECK_CLOSE( 2*fnTrue(i), fn(i), small_tol, close_tol );

  // viscous diffusion matrix

  MatrixQ kxxTrue = {
    {0, 0, 0, 0, 0},
    {-u*(2*mu + lambda)/rho, (2*mu + lambda)/rho, 0, 0, 0},
    {-v*mu/rho, 0, mu/rho, 0, 0},
    {-w*mu/rho, 0, 0, mu/rho, 0},
    {-u*u*((2*mu + lambda) - k/Cv)/rho - v*v*(mu - k/Cv)/rho - w*w*(mu - k/Cv)/rho - e0*k/(Cv*rho),
       u*((2*mu + lambda) - k/Cv)/rho,    v*(mu - k/Cv)/rho,    w*(mu - k/Cv)/rho,     k/(Cv*rho)} };
  MatrixQ kxyTrue = {
    {0, 0, 0, 0, 0},
    {-v*lambda/rho, 0, lambda/rho, 0, 0},
    {-u*mu/rho, mu/rho, 0, 0, 0},
    {0, 0, 0, 0, 0},
    {-u*v*(mu + lambda)/rho, v*mu/rho, u*lambda/rho, 0, 0} };
  MatrixQ kxzTrue = {
    {0, 0, 0, 0, 0},
    {-w*lambda/rho, 0, 0, lambda/rho, 0},
    {0, 0, 0, 0, 0},
    {-u*mu/rho, mu/rho, 0, 0, 0},
    {-u*w*(mu + lambda)/rho, w*mu/rho, 0, u*lambda/rho, 0} };

  MatrixQ kyxTrue = {
    {0, 0, 0, 0, 0},
    {-v*mu/rho, 0, mu/rho, 0, 0},
    {-u*lambda/rho, lambda/rho, 0, 0, 0},
    {0, 0, 0, 0, 0},
    {-u*v*(mu + lambda)/rho, v*lambda/rho, u*mu/rho, 0, 0} };
  MatrixQ kyyTrue = {
    {0, 0, 0, 0, 0},
    {-u*mu/rho, mu/rho, 0, 0, 0},
    {-v*(2*mu + lambda)/rho, 0, (2*mu + lambda)/rho, 0, 0},
    {-w*mu/rho, 0, 0, mu/rho, 0},
    {-u*u*(mu - k/Cv)/rho - v*v*((2*mu + lambda) - k/Cv)/rho - w*w*(mu - k/Cv)/rho - e0*k/(Cv*rho),
       u*(mu - k/Cv)/rho,    v*((2*mu + lambda) - k/Cv)/rho,    w*(mu - k/Cv)/rho,     k/(Cv*rho)} };
  MatrixQ kyzTrue = {
    {0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0},
    {-w*lambda/rho, 0, 0, lambda/rho, 0},
    {-v*mu/rho, 0, mu/rho, 0, 0},
    {-v*w*(mu + lambda)/rho, 0, w*mu/rho, v*lambda/rho, 0} };

  MatrixQ kzxTrue = {
    {0, 0, 0, 0, 0},
    {-w*mu/rho, 0, 0, mu/rho, 0},
    {0, 0, 0, 0, 0},
    {-u*lambda/rho, lambda/rho, 0, 0, 0},
    {-w*u*(mu + lambda)/rho, w*lambda/rho, 0, u*mu/rho, 0} };
  MatrixQ kzyTrue = {
    {0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0},
    {-w*mu/rho, 0, 0, mu/rho, 0},
    {-v*lambda/rho, 0, lambda/rho, 0, 0},
    {-w*v*(mu + lambda)/rho, 0, w*lambda/rho, v*mu/rho, 0} };
  MatrixQ kzzTrue = {
    {0, 0, 0, 0, 0},
    {-u*mu/rho, mu/rho, 0, 0, 0},
    {-v*mu/rho, 0, mu/rho, 0, 0},
    {-w*(2*mu + lambda)/rho, 0, 0, (2*mu + lambda)/rho, 0},
    {-u*u*(mu - k/Cv)/rho - v*v*(mu - k/Cv)/rho - w*w*((2*mu + lambda) - k/Cv)/rho - e0*k/(Cv*rho),
       u*(mu - k/Cv)/rho,    v*(mu - k/Cv)/rho,    w*((2*mu + lambda) - k/Cv)/rho,     k/(Cv*rho)} };

  MatrixQ kxx=0, kxy=0, kxz=0, kyx=0, kyy=0, kyz=0, kzx=0, kzy=0, kzz=0;
  pde.diffusionViscous( x, y, z, time,
                        q, qx, qy, qz,
                        kxx, kxy, kxz,
                        kyx, kyy, kyz,
                        kzx, kzy, kzz );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      //cout << "i = " << i << "  j = " << j << endl;
      SANS_CHECK_CLOSE( kxxTrue(i,j), kxx(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kxyTrue(i,j), kxy(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kxzTrue(i,j), kxz(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kyxTrue(i,j), kyx(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kyyTrue(i,j), kyy(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kyzTrue(i,j), kyz(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kzxTrue(i,j), kzx(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kzyTrue(i,j), kzy(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kzzTrue(i,j), kzz(i,j), small_tol, close_tol );
    }
  }

  // check that viscous flux is consistent with diffusion matrix

  MatrixQ dudq = 0;
  pde.jacobianMasterState(x, y, z, time, q, dudq);

  f += kxx*dudq*qx + kxy*dudq*qy + kxz*dudq*qz;
  g += kyx*dudq*qx + kyy*dudq*qy + kyz*dudq*qz;
  h += kzx*dudq*qx + kzy*dudq*qy + kzz*dudq*qz;

  for (int i = 0; i < PDEClass::N; i++)
  {
    BOOST_CHECK_SMALL( f(i), small_tol );
    BOOST_CHECK_SMALL( g(i), small_tol );
    BOOST_CHECK_SMALL( h(i), small_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jacobianFluxViscous )
{
  // Use conservative variables here in order to differentiate with Surreal
  typedef QTypeConservative QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef SurrealS<PDEClass::N> SurrealClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;
  typedef PDEClass::ArrayQ<SurrealClass> ArrayQSurreal;

  const Real small_tol = 1.e-12;
  const Real close_tol = 1.e-11;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real tSuth = 110./300.; //somewhat random...
  const Real tRef  = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  //pde.dump(0);
  bool verbose = false;

  Real x, y, z, time;
  Real rho, u, v, w, t, E;

  x = 0; y = 0; z = 0;   // not actually used in functions
  rho = 1.225;                    // kg/m^3
  u = 9.784; v = -3.231; w = 10.56;  // m/s
  t = 288.15;                     // K
  E = gas.energy(rho, t) + 0.5*(u*u + v*v + w*w);

  Real rhou = rho*u;
  Real rhov = rho*v;
  Real rhow = rho*w;
  Real rhoE = rho*E;

  // gradient
  Real rhox, rhoux, rhovx, rhowx, rhoEx;
  Real rhoy, rhouy, rhovy, rhowy, rhoEy;
  Real rhoz, rhouz, rhovz, rhowz, rhoEz;
  rhox = 0.37, rhoux = -0.85, rhovx = 0.09, rhowx =  0.36, rhoEx = 1.71;
  rhoy = 2.31, rhouy =  1.65, rhovy = 0.87, rhowy = -1.07, rhoEy = 0.29;
  rhoz = 0.45, rhouz =  0.12, rhovz = 1.27, rhowz =  0.07, rhoEz = 2.12;

  ArrayQ q, qx, qy, qz;
  MatrixQ dfdu, dgdu, dhdu;
  ArrayQSurreal qS;
  ArrayQSurreal fv, gv, hv;

  SurrealClass rhoS  = rho;
  SurrealClass rhouS = rhou;
  SurrealClass rhovS = rhov;
  SurrealClass rhowS = rhow;
  SurrealClass rhoES = rhoE;

  rhoS.deriv(0)  = 1;
  rhouS.deriv(1) = 1;
  rhovS.deriv(2) = 1;
  rhowS.deriv(3) = 1;
  rhoES.deriv(4) = 1;

  // set the complete state vectors
  pde.setDOFFrom( q,Conservative3D<Real>({rho , rhou, rhov, rhow, rhoE }));
  pde.setDOFFrom(qS, Conservative3D<SurrealClass>({rhoS, rhouS, rhovS, rhowS, rhoES }));

  qx = {rhox, rhoux, rhovx, rhowx, rhoEx};
  qy = {rhoy, rhouy, rhovy, rhowy, rhoEy};
  qz = {rhoz, rhouz, rhovz, rhowz, rhoEz};


  // Compute the true jacobian with Surreals
  fv = 0;
  gv = 0;
  hv = 0;
  pde.fluxViscous( x, y, z, time, qS, qx, qy, qz, fv, gv, hv );

  // Compute the hand coded Jacobian
  dfdu = 0;
  dgdu = 0;
  dhdu = 0;
  pde.jacobianFluxViscous( x, y, z, time, q, qx, qy, qz, dfdu, dgdu, dhdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " fv(" << i << ").deriv(" << j << ") = " << fv(i).deriv(j) << " | " << dfdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( fv(i).deriv(j), dfdu(i,j), small_tol, close_tol );
//
      if (verbose) std::cout << " gv(" << i << ").deriv(" << j << ") = " << gv(i).deriv(j) << " | " << dgdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( gv(i).deriv(j), dgdu(i,j), small_tol, close_tol );
//
      if (verbose) std::cout << " hv(" << i << ").deriv(" << j << ") = " << hv(i).deriv(j) << " | " << dhdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( hv(i).deriv(j), dhdu(i,j), small_tol, close_tol );
    }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( speedCharacteristic_test, QType, QTypeList )
{
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;
  const Real tSutherland = 0.5;
  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, z, time, dx, dy, dz;
  Real rho, u, v, w, t, c;
  Real speed, speedTrue1, speedTrue2;

  x = 0; y = 0; z = 0; time = 0;  // not actually used in functions
  dx = 0.57; dy = -0.43; dz = 0.31;
  rho = 1.137; u = 0.784; v = -0.231; w = 0.344; t = 0.987;
  c = sqrt(gamma*R*t);
  speedTrue1 = fabs(dx*u + dy*v + dz*w)/sqrt(dx*dx + dy*dy + dz*dz) + c;
  speedTrue2 = sqrt(u*u + v*v + w*w) + c;

  ArrayQ q = pde.setDOFFrom( DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );

  pde.speedCharacteristic( x, y, z, time, dx, dy, dz, q, speed );
  BOOST_CHECK_CLOSE( speedTrue1, speed, tol );

  pde.speedCharacteristic( x, y, z, time, q, speed );
  BOOST_CHECK_CLOSE( speedTrue2, speed, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;
  const Real tSutherland = 0.5;
  const Real tRef = 1.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  ArrayQ q;
  q(0) =  1;
  q(4) =  1;
  BOOST_CHECK( pde.isValidState(q) == true );

  q(0) =  1;
  q(4) = -1;
  BOOST_CHECK( pde.isValidState(q) == false );

  q(0) = -1;
  q(4) =  1;
  BOOST_CHECK( pde.isValidState(q) == false );

  q(0) = -1;
  q(4) = -1;
  BOOST_CHECK( pde.isValidState(q) == false );

  q(0) =  1;
  q(4) =  0;
  BOOST_CHECK( pde.isValidState(q) == false );

  q(0) =  0;
  q(4) =  1;
  BOOST_CHECK( pde.isValidState(q) == false );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/PDENavierStokes3D_pattern.txt", true );

  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  pde.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
