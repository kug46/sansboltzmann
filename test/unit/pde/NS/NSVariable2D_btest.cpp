// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// NSVariable2D_btest
// testing of NSVariable classes that represent a specific set of variables used to set the working variables

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/NS/NSVariable2D.h"
#include "pde/NS/SAVariable2D.h"

#include <string>
#include <iostream>

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( NSVariable2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( NSVariable2D_test )
{
  const Real tol = 5.0e-12;

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  const Real Cv    = R/(gamma - 1);

  // set/eval
  Real rho, u, v, t, p, e, E;

  rho = 1.225;          // kg/m^3 (standard atmosphere)
  t   = 288.15;         // K (standard atmosphere)
  u   = 15.03;          // m/s
  v   =  1.71;
  p   = R*rho*t;
  e   = Cv*t;
  E   = e + 0.5*(u*u + v*v);

  DensityVelocityPressure2D<Real> rvp1(rho, u, v, p);

  BOOST_CHECK_CLOSE( rho, rvp1.Density, tol );
  BOOST_CHECK_CLOSE( u, rvp1.VelocityX, tol );
  BOOST_CHECK_CLOSE( v, rvp1.VelocityY, tol );
  BOOST_CHECK_CLOSE( p, rvp1.Pressure, tol );

  DensityVelocityPressure2D<Real> rvp2 = {rho, u, v, p};

  BOOST_CHECK_CLOSE( rho, rvp2.Density, tol );
  BOOST_CHECK_CLOSE( u, rvp2.VelocityX, tol );
  BOOST_CHECK_CLOSE( v, rvp2.VelocityY, tol );
  BOOST_CHECK_CLOSE( p, rvp2.Pressure, tol );

  PyDict drvp;
  drvp[DensityVelocityPressure2DParams::params.rho] = rho;
  drvp[DensityVelocityPressure2DParams::params.u] = u;
  drvp[DensityVelocityPressure2DParams::params.v] = v;
  drvp[DensityVelocityPressure2DParams::params.p] = p;
  DensityVelocityPressure2DParams::checkInputs(drvp);

  DensityVelocityPressure2D<Real> rvp3(drvp);

  BOOST_CHECK_CLOSE( rho, rvp3.Density, tol );
  BOOST_CHECK_CLOSE( u, rvp3.VelocityX, tol );
  BOOST_CHECK_CLOSE( v, rvp3.VelocityY, tol );
  BOOST_CHECK_CLOSE( p, rvp3.Pressure, tol );

  DensityVelocityTemperature2D<Real> rvt1(rho, u, v, t);

  BOOST_CHECK_CLOSE( rho, rvt1.Density, tol );
  BOOST_CHECK_CLOSE( u, rvt1.VelocityX, tol );
  BOOST_CHECK_CLOSE( v, rvt1.VelocityY, tol );
  BOOST_CHECK_CLOSE( t, rvt1.Temperature, tol );

  DensityVelocityTemperature2D<Real> rvt2 = {rho, u, v, t};

  BOOST_CHECK_CLOSE( rho, rvt2.Density, tol );
  BOOST_CHECK_CLOSE( u, rvt2.VelocityX, tol );
  BOOST_CHECK_CLOSE( v, rvt2.VelocityY, tol );
  BOOST_CHECK_CLOSE( t, rvt2.Temperature, tol );

  PyDict drvt;
  drvt[DensityVelocityTemperature2DParams::params.rho] = rho;
  drvt[DensityVelocityTemperature2DParams::params.u] = u;
  drvt[DensityVelocityTemperature2DParams::params.v] = v;
  drvt[DensityVelocityTemperature2DParams::params.t] = t;
  DensityVelocityTemperature2DParams::checkInputs(drvt);

  DensityVelocityTemperature2D<Real> rvt3(drvt);

  BOOST_CHECK_CLOSE( rho, rvt3.Density, tol );
  BOOST_CHECK_CLOSE( u, rvt3.VelocityX, tol );
  BOOST_CHECK_CLOSE( v, rvt3.VelocityY, tol );
  BOOST_CHECK_CLOSE( t, rvt3.Temperature, tol );


  Conservative2D<Real> u1(rho, rho*u, rho*v, rho*E);

  BOOST_CHECK_CLOSE( rho, u1.Density, tol );
  BOOST_CHECK_CLOSE( rho*u, u1.MomentumX, tol );
  BOOST_CHECK_CLOSE( rho*v, u1.MomentumY, tol );
  BOOST_CHECK_CLOSE( rho*E, u1.TotalEnergy, tol );

  Conservative2D<Real> u2 = {rho, rho*u, rho*v, rho*E};

  BOOST_CHECK_CLOSE( rho, u2.Density, tol );
  BOOST_CHECK_CLOSE( rho*u, u2.MomentumX, tol );
  BOOST_CHECK_CLOSE( rho*v, u2.MomentumY, tol );
  BOOST_CHECK_CLOSE( rho*E, u2.TotalEnergy, tol );

  PyDict du;
  du[Conservative2DParams::params.rho] = rho;
  du[Conservative2DParams::params.rhou] = rho*u;
  du[Conservative2DParams::params.rhov] = rho*v;
  du[Conservative2DParams::params.rhoE] = rho*E;
  Conservative2DParams::checkInputs(du);

  Conservative2D<Real> u3(du);

  BOOST_CHECK_CLOSE( rho, u3.Density, tol );
  BOOST_CHECK_CLOSE( rho*u, u3.MomentumX, tol );
  BOOST_CHECK_CLOSE( rho*v, u3.MomentumY, tol );
  BOOST_CHECK_CLOSE( rho*E, u3.TotalEnergy, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SAVariable2D_test )
{
  const Real tol = 5.0e-12;

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  const Real Cv    = R/(gamma - 1);

  // set/eval
  Real rho, u, v, t, p, e, E, nt;

  rho = 1.225;          // kg/m^3 (standard atmosphere)
  t   = 288.15;         // K (standard atmosphere)
  u   = 15.03;          // m/s
  v   =  1.71;
  p   = R*rho*t;
  e   = Cv*t;
  E   = e + 0.5*(u*u + v*v);
  nt  = 4.6;

  SAnt2D<DensityVelocityPressure2D<Real>> rvp1({rho, u, v, p, nt});

  BOOST_CHECK_CLOSE( rho, rvp1.Density, tol );
  BOOST_CHECK_CLOSE( u  , rvp1.VelocityX, tol );
  BOOST_CHECK_CLOSE( v  , rvp1.VelocityY, tol );
  BOOST_CHECK_CLOSE( p  , rvp1.Pressure, tol );
  BOOST_CHECK_CLOSE( nt , rvp1.SANutilde, tol );

  SAnt2D<DensityVelocityPressure2D<Real>> rvp2 = {rho, u, v, p, nt};

  BOOST_CHECK_CLOSE( rho, rvp2.Density, tol );
  BOOST_CHECK_CLOSE( u  , rvp2.VelocityX, tol );
  BOOST_CHECK_CLOSE( v  , rvp2.VelocityY, tol );
  BOOST_CHECK_CLOSE( p  , rvp2.Pressure, tol );
  BOOST_CHECK_CLOSE( nt , rvp2.SANutilde, tol );

  PyDict drvp;
  drvp[SAnt2DParams<DensityVelocityPressure2DParams>::params.rho] = rho;
  drvp[SAnt2DParams<DensityVelocityPressure2DParams>::params.u] = u;
  drvp[SAnt2DParams<DensityVelocityPressure2DParams>::params.v] = v;
  drvp[SAnt2DParams<DensityVelocityPressure2DParams>::params.p] = p;
  drvp[SAnt2DParams<DensityVelocityPressure2DParams>::params.nt] = nt;
  SAnt2DParams<DensityVelocityPressure2DParams>::checkInputs(drvp);

  SAnt2D<DensityVelocityPressure2D<Real>> rvp3(drvp);
  BOOST_CHECK_CLOSE( rho, rvp3.Density, tol );
  BOOST_CHECK_CLOSE( u  , rvp3.VelocityX, tol );
  BOOST_CHECK_CLOSE( v  , rvp3.VelocityY, tol );
  BOOST_CHECK_CLOSE( p  , rvp3.Pressure, tol );
  BOOST_CHECK_CLOSE( nt , rvp3.SANutilde, tol );

  SAnt2D<DensityVelocityTemperature2D<Real>> rvt1({rho, u, v, t, nt});
  BOOST_CHECK_CLOSE( rho, rvt1.Density, tol );
  BOOST_CHECK_CLOSE( u  , rvt1.VelocityX, tol );
  BOOST_CHECK_CLOSE( v  , rvt1.VelocityY, tol );
  BOOST_CHECK_CLOSE( t  , rvt1.Temperature, tol );
  BOOST_CHECK_CLOSE( nt , rvt1.SANutilde, tol );

  SAnt2D<DensityVelocityTemperature2D<Real>> rvt2 = {rho, u, v, t,nt};

  BOOST_CHECK_CLOSE( rho, rvt2.Density, tol );
  BOOST_CHECK_CLOSE( u  , rvt2.VelocityX, tol );
  BOOST_CHECK_CLOSE( v  , rvt2.VelocityY, tol );
  BOOST_CHECK_CLOSE( t  , rvt2.Temperature, tol );
  BOOST_CHECK_CLOSE( nt , rvt2.SANutilde, tol );

  PyDict drvt;
  drvt[SAnt2DParams<DensityVelocityTemperature2DParams>::params.rho] = rho;
  drvt[SAnt2DParams<DensityVelocityTemperature2DParams>::params.u] = u;
  drvt[SAnt2DParams<DensityVelocityTemperature2DParams>::params.v] = v;
  drvt[SAnt2DParams<DensityVelocityTemperature2DParams>::params.t] = t;
  drvt[SAnt2DParams<DensityVelocityTemperature2DParams>::params.nt] = nt;
  SAnt2DParams<DensityVelocityTemperature2DParams>::checkInputs(drvt);

  SAnt2D<DensityVelocityTemperature2D<Real>> rvt3(drvt);
  BOOST_CHECK_CLOSE( rho, rvt3.Density, tol );
  BOOST_CHECK_CLOSE( u  , rvt3.VelocityX, tol );
  BOOST_CHECK_CLOSE( v  , rvt3.VelocityY, tol );
  BOOST_CHECK_CLOSE( t  , rvt3.Temperature, tol );
  BOOST_CHECK_CLOSE( nt , rvt3.SANutilde, tol );

  SAnt2D<Conservative2D<Real>> u1({rho, rho*u, rho*v, rho*E, rho*nt});
  BOOST_CHECK_CLOSE( rho   , u1.Density, tol );
  BOOST_CHECK_CLOSE( rho*u , u1.MomentumX, tol );
  BOOST_CHECK_CLOSE( rho*v , u1.MomentumY, tol );
  BOOST_CHECK_CLOSE( rho*E , u1.TotalEnergy, tol );
  BOOST_CHECK_CLOSE( rho*nt, u1.rhoSANutilde, tol );

  SAnt2D<Conservative2D<Real>> u2 = {rho, rho*u, rho*v, rho*E, rho*nt};
  BOOST_CHECK_CLOSE( rho   , u2.Density, tol );
  BOOST_CHECK_CLOSE( rho*u , u2.MomentumX, tol );
  BOOST_CHECK_CLOSE( rho*v , u2.MomentumY, tol );
  BOOST_CHECK_CLOSE( rho*E , u2.TotalEnergy, tol );
  BOOST_CHECK_CLOSE( rho*nt, u2.rhoSANutilde, tol );

  PyDict du;
  du[SAnt2DParams<Conservative2DParams>::params.rho] = rho;
  du[SAnt2DParams<Conservative2DParams>::params.rhou] = rho*u;
  du[SAnt2DParams<Conservative2DParams>::params.rhov] = rho*v;
  du[SAnt2DParams<Conservative2DParams>::params.rhoE] = rho*E;
  du[SAnt2DParams<Conservative2DParams>::params.rhont] = rho*nt;
  SAnt2DParams<Conservative2DParams>::checkInputs(du);

  SAnt2D<Conservative2D<Real>> u3(du);
  BOOST_CHECK_CLOSE( rho   , u3.Density, tol );
  BOOST_CHECK_CLOSE( rho*u , u3.MomentumX, tol );
  BOOST_CHECK_CLOSE( rho*v , u3.MomentumY, tol );
  BOOST_CHECK_CLOSE( rho*E , u3.TotalEnergy, tol );
  BOOST_CHECK_CLOSE( rho*nt, u3.rhoSANutilde, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
