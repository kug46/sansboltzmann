// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEEulermitAVDiffusion1D_btest
//
// test of quasi 1-D compressible Euler PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"
#include "tools/smoothmath.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/Q1DPrimitiveSurrogate.h"
#include "pde/NS/Q1DConservative.h"
#include "pde/NS/Q1DEntropy.h"
#include "pde/NS/PDEEulermitAVDiffusion1D.h"
#include "pde/NS/PDEEuler1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"

namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
class QTypeEntropy {};

//Explicitly instantiate the class so coverage information is correct
template class TraitsSizeEuler<PhysD1>;
template class TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>;
template class TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>;
template class TraitsModelEuler<QTypeConservative, GasModel>;
template class TraitsModelEuler<QTypeEntropy, GasModel>;
template class PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>>;
template class PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>>;
template class PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEuler<QTypeConservative, GasModel>>;
template class PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEuler<QTypeEntropy, GasModel>>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEEulermitAVDiffusion1D_test_suite )

typedef boost::mpl::list< QTypePrimitiveRhoPressure,
                          QTypeConservative,
                          QTypePrimitiveSurrogate,
                          QTypeEntropy > QTypes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( static_test, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  BOOST_REQUIRE( PDEClass::D == 1 );
  BOOST_REQUIRE( PDEClass::N == 3 );
  BOOST_REQUIRE( ArrayQ::M == 3 );
  BOOST_REQUIRE( MatrixQ::M == 3 );
  BOOST_REQUIRE( MatrixQ::N == 3 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctor, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  std::shared_ptr<ScalarFunction1D_OffsetLog> area( new ScalarFunction1D_OffsetLog() );

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEClass::Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( avpde.D == 1 );
  BOOST_REQUIRE( avpde.N == 3 );

  // flux components
  BOOST_CHECK( avpde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( avpde.hasFluxAdvective() == true );
  BOOST_CHECK( avpde.hasFluxViscous() == true );
  BOOST_CHECK( avpde.hasSource() == false );
  BOOST_CHECK( avpde.hasForcingFunction() == false );
  BOOST_CHECK( avpde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( avpde.needsSolutionGradientforSource() == false );

  AVPDEClass avpde2(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEClass::Euler_ResidInterp_Raw, area);

  // static tests
  BOOST_REQUIRE( avpde2.D == 1 );
  BOOST_REQUIRE( avpde2.N == 3 );

  // flux components
  BOOST_CHECK( avpde2.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( avpde2.hasFluxAdvective() == true );
  BOOST_CHECK( avpde2.hasFluxViscous() == true );
  BOOST_CHECK( avpde2.hasSource() == true );
  BOOST_CHECK( avpde2.hasForcingFunction() == false );
  BOOST_CHECK( avpde2.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( avpde2.needsSolutionGradientforSource() == false );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( set_RhoPressure )
{
  typedef TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEClass::Euler_ResidInterp_Raw);

  Real rho, u, t, p;

  rho = 1.137; u = 0.784; t = 0.987;
  p  = R*rho*t;
  ParamType paramL(0.1, 0.0); // grid spacing and jump values

  // set
  Real qDataPrim[3] = {rho, u, t};
  string qNamePrim[3] = {"Density", "VelocityX", "Temperature"};
  ArrayQ qTrue = {rho, u, p};
  ArrayQ q;
  avpde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );

  DensityVelocityPressure1D<Real> qdata1;
  qdata1.Density = rho; qdata1.VelocityX = u; qdata1.Pressure = p;
  q = 1;
  avpde.setDOFFrom( q, qdata1 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );

  DensityVelocityTemperature1D<Real> qdata2(rho, u, t);
  q = 1;

  avpde.setDOFFrom( q, qdata2 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD1::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + sensor

  const Real tol = 1.e-13;

  Real order = 1;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEClass::Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( avpde.N == 3 );

  // function tests

  Real x, time;
  Real rho, u, t, p, e0, h0;
  Real Cv, Cp;

  x = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u);
  h0 = Cp*t + 0.5*(u*u);

  Real sensor = 0.1;
  HType H = {{0.5}};
  HType logH = log(H);
  ParamType paramL(logH, sensor); // grid spacing and sensor values
  ParamType paramR(logH, 0.5*sensor); // grid spacing and sensor values

  // set
  ArrayQ q = 0;
  avpde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );

  // conservative flux
  ArrayQ uTrue = {rho, rho*u, rho*e0};
  ArrayQ uCons = 0;
  avpde.masterState( paramL, x, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // Should not accumulate
  avpde.masterState( paramL, x, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // conservative flux
  ArrayQ ftTrue = {rho, rho*u, rho*e0};
  ArrayQ ft = 0;
  avpde.fluxAdvectiveTime( paramL, x, time, q, ft );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );

  // Flux accumulate
  avpde.fluxAdvectiveTime( paramL, x, time, q, ft );
  BOOST_CHECK_CLOSE( 2*ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(2), ft(2), tol );

  // advective flux
  ArrayQ fTrue = {rho*u, rho*u*u + p, rho*u*h0};
  ArrayQ f = 0;
  avpde.fluxAdvective( paramL, x, time, q, f );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );

  // Flux accumulate
  avpde.fluxAdvective( paramL, x, time, q, f );
  BOOST_CHECK_CLOSE( 2*fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(2), f(2), tol );

  Real rhoL = 1.137, uL = 0.784, tL = 0.987;
  Real rhoR = 1.240, uR = 0.831, tR = 0.865;

  ArrayQ qL = 0, qR = 0;
  avpde.setDOFFrom( qL, DensityVelocityTemperature1D<Real>(rhoL, uL, tL) );
  avpde.setDOFFrom( qR, DensityVelocityTemperature1D<Real>(rhoR, uR, tR) );

  ArrayQ qxL = {0.17, 0.21, 0.19};
  ArrayQ qxR = {0.21, 0.31, 0.26};

  MatrixQ kxx = 0;
  MatrixQ kxxT = 0;
  MatrixQ kxx_true = 0;

  Real lambda = 0;
  avpde.speedCharacteristic( paramL, x, time, qL, lambda );

#if 0
  Real theta_L = 0.001;
  Real theta_H = 1.0;
  sensor = smoothActivation_sine(sensor, theta_L, theta_H);
#else
//  Real alpha = 500;
//  Real eps_exp = 1e-10;

  Real z = 0.0;

  sensor = std::max(sensor, z);
#endif

  Real factor = 2.0/(Real(order)) * lambda * sensor; //smoothabs0(sensorL, 1.0e-5);

  MatrixQ dutdu = DLA::Identity();
  dutdu(2, 0) = 0.5*(gamma - 1.0)*(uL*uL);
  dutdu(2, 1) = -(gamma - 1.0)*uL;
  dutdu(2, 2) = gamma;

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
    {
      if (i == j)
      {
        kxxT(i,j) = factor*H(0,0);
      }
      else
      {
        kxxT(i,j) = 0.0;
      }
    }

  kxx_true = kxxT*dutdu;

  avpde.diffusionViscous( paramL, x, time, qL, qxL, kxx );
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
    {
      BOOST_CHECK_CLOSE( kxx_true(i,j), kxx(i,j), tol );
    }

  // Flux accumulation
  avpde.diffusionViscous( paramL, x, time, qL, qxL, kxx );
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
    {
      BOOST_CHECK_CLOSE( 2*kxx_true(i,j), kxx(i,j), tol );
    }

  MatrixQ dudqL = 0;
  avpde.jacobianMasterState(paramL, x, time, qL, dudqL);

  ArrayQ fxL_true = -kxx_true*dudqL*qxL;

  ArrayQ fxL = 0;
  avpde.fluxViscous( paramL, x, time, qL, qxL, fxL );
  BOOST_CHECK_CLOSE( fxL_true(0), fxL(0), tol );
  BOOST_CHECK_CLOSE( fxL_true(1), fxL(1), tol );
  BOOST_CHECK_CLOSE( fxL_true(2), fxL(2), tol );

  // Flux accumulate
  avpde.fluxViscous( paramL, x, time, qL, qxL, fxL );
  BOOST_CHECK_CLOSE( 2*fxL_true(0), fxL(0), tol );
  BOOST_CHECK_CLOSE( 2*fxL_true(1), fxL(1), tol );
  BOOST_CHECK_CLOSE( 2*fxL_true(2), fxL(2), tol );

  ArrayQ fxR_true = 0;
  avpde.fluxViscous( paramR, x, time, qR, qxR, fxR_true );

  Real nx = 0.526;
  ArrayQ fn_true = 0.5*(fxL_true + fxR_true)*nx;

  ArrayQ fn = 0;
  avpde.fluxViscous( paramL, paramR, x, time, qL, qxL, qR, qxR, nx, fn );
  BOOST_CHECK_CLOSE( fn_true(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fn_true(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fn_true(2), fn(2), tol );

  // Flux accumulate
  avpde.fluxViscous( paramL, paramR, x, time, qL, qxL, qR, qxR, nx, fn );
  BOOST_CHECK_CLOSE( 2*fn_true(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 2*fn_true(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 2*fn_true(2), fn(2), tol );

  ArrayQ source = 0.0;
  avpde.source( paramL, x, time, qL, qxL, source );
  BOOST_CHECK_CLOSE( 0.0, source(0), tol );
  BOOST_CHECK_CLOSE( 0.0, source(1), tol );
  BOOST_CHECK_CLOSE( 0.0, source(2), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux_areaChange, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;
  std::shared_ptr<ScalarFunction1D_OffsetLog> area( new ScalarFunction1D_OffsetLog() );

  typedef DLA::MatrixSymS<PhysD1::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + sensor

  const Real tol = 2.e-13;

  int order = 1;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEClass::Euler_ResidInterp_Raw, area);

  // static tests
  BOOST_REQUIRE( avpde.N == 3 );

  // function tests

  Real x, time;
  Real rho, u, t, p, e0, h0;
  Real Cv, Cp;

  x = time = 1.0;   // not actually used in functions
  rho = 1.137; u = 0.784; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u);
  h0 = Cp*t + 0.5*(u*u);

  Real sensor = 0.1;
  HType H = {{0.5}};
  HType logH = log(H);
  ParamType paramL(logH, sensor); // grid spacing and sensor values
  ParamType paramR(logH, 0.5*sensor); // grid spacing and sensor values

  // set
  ArrayQ q = 0;
  avpde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );

  // conservative flux
  ArrayQ uTrue = {rho, rho*u, rho*e0};
  ArrayQ uCons = 0;
  avpde.masterState( paramL, x, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // Should not accumulate
  avpde.masterState( paramL, x, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // advective flux
  ArrayQ fTrue = {rho*u, rho*u*u + p, rho*u*h0};
  ArrayQ f = 0;
  avpde.fluxAdvective( paramL, x, time, q, f );
  BOOST_CHECK_CLOSE( 10.*fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( 10.*fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( 10.*fTrue(2), f(2), tol );

  // Flux acumulate
  avpde.fluxAdvective( paramL, x, time, q, f );
  BOOST_CHECK_CLOSE( 2*10.*fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( 2*10.*fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( 2*10.*fTrue(2), f(2), tol );

  Real rhoL = 1.137, uL = 0.784, tL = 0.987;
  Real rhoR = 1.240, uR = 0.831, tR = 0.865;
  Real pL = rhoL * R* tL;

  ArrayQ qL = 0, qR = 0;
  avpde.setDOFFrom( qL, DensityVelocityTemperature1D<Real>(rhoL, uL, tL) );
  avpde.setDOFFrom( qR, DensityVelocityTemperature1D<Real>(rhoR, uR, tR) );

  ArrayQ qxL = {0.17, 0.21, 0.19};
  ArrayQ qxR = {0.21, 0.31, 0.26};

  MatrixQ kxx = 0;
  MatrixQ kxxT = 0;
  MatrixQ kxx_true = 0;

  Real lambda = 0;
  avpde.speedCharacteristic( paramL, x, time, qL, lambda );

#if 0
  Real theta_L = 0.001;
  Real theta_H = 1.0;
  sensor = smoothActivation_sine(sensor, theta_L, theta_H);
#else
//  Real alpha = 500;
//  Real eps_exp = 1e-10;

  Real z = 0.0;
  sensor = std::max(sensor, z);
#endif

  Real factor = 2.0/(Real(order)) * lambda * sensor; //smoothabs0(sensorL, 1.0e-5);

  MatrixQ dutdu = DLA::Identity();
  dutdu(2, 0) = 0.5*(gamma - 1.0)*(uL*uL);
  dutdu(2, 1) = -(gamma - 1.0)*uL;
  dutdu(2, 2) = gamma;

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
    {
      if (i == j)
      {
        kxxT(i,j) = factor*H(0,0);
      }
      else
      {
        kxxT(i,j) = 0.0;
      }
    }

  kxx_true = kxxT*dutdu;

  avpde.diffusionViscous( paramL, x, time, qL, qxL, kxx );
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
    {
      BOOST_CHECK_CLOSE( kxx_true(i,j), kxx(i,j), tol );
    }

  // Flux accumulate
  avpde.diffusionViscous( paramL, x, time, qL, qxL, kxx );
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
    {
      BOOST_CHECK_CLOSE( 2*kxx_true(i,j), kxx(i,j), tol );
    }

  MatrixQ dudqL = 0;
  avpde.jacobianMasterState(paramL, x, time, qL, dudqL);

  ArrayQ fxL_true = -kxx_true*dudqL*qxL;

  ArrayQ fxL = 0;
  avpde.fluxViscous( paramL, x, time, qL, qxL, fxL );
  BOOST_CHECK_CLOSE( 10.*fxL_true(0), fxL(0), tol );
  BOOST_CHECK_CLOSE( 10.*fxL_true(1), fxL(1), tol );
  BOOST_CHECK_CLOSE( 10.*fxL_true(2), fxL(2), tol );

  // Flux accumulate
  avpde.fluxViscous( paramL, x, time, qL, qxL, fxL );
  BOOST_CHECK_CLOSE( 2*10.*fxL_true(0), fxL(0), tol );
  BOOST_CHECK_CLOSE( 2*10.*fxL_true(1), fxL(1), tol );
  BOOST_CHECK_CLOSE( 2*10.*fxL_true(2), fxL(2), tol );

  fxL_true = 0;
  ArrayQ fxR_true = 0;
  avpde.fluxViscous( paramL, x, time, qL, qxL, fxL_true );
  avpde.fluxViscous( paramR, x, time, qR, qxR, fxR_true );

  Real nx = 0.526;
  ArrayQ fn_true = 0.5*(fxL_true + fxR_true)*nx;

  ArrayQ fn = 0;
  avpde.fluxViscous( paramL, paramR, x, time, qL, qxL, qR, qxR, nx, fn );
  BOOST_CHECK_CLOSE( fn_true(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fn_true(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fn_true(2), fn(2), tol );

  // Flux accumulate
  avpde.fluxViscous( paramL, paramR, x, time, qL, qxL, qR, qxR, nx, fn );
  BOOST_CHECK_CLOSE( 2*fn_true(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 2*fn_true(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 2*fn_true(2), fn(2), tol );

  ArrayQ source = 0.0;
  avpde.source( paramL, x, time, qL, qxL, source );
  BOOST_CHECK_CLOSE( 0.0, source(0), tol );
  BOOST_CHECK_CLOSE( -9./log(4.)*pL, source(1), tol );
  BOOST_CHECK_CLOSE( 0.0, source(2), tol );

  // Source accumulate
  avpde.source( paramL, x, time, qL, qxL, source );
  BOOST_CHECK_CLOSE( 0.0, source(0), tol );
  BOOST_CHECK_CLOSE( 2*-9./log(4.)*pL, source(1), tol );
  BOOST_CHECK_CLOSE( 0.0, source(2), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( jacobianMasterState, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<SurrealS<PDEClass::N>> ArrayQSurreal;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEClass::Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( avpde.N == 3 );

  // function tests

  Real rho, u, t;

  rho = 1.137; u = 0.784; t = 0.987;
  ParamType paramL(0.1, 0.0); // grid spacing and jump values

  Real x = 0, time = 0;

  // set
  ArrayQ q = 0;
  avpde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );

  if ( std::is_same<QType,QTypeEntropy>::value )
  {
    // Entropy variables seem to have numerical problems when comparing Surreal and
    // hand differentiated code...
    q[0] = -10;
    q[1] = 2;
    q[2] = -3;
  }

  ArrayQSurreal qSurreal = q;

  qSurreal[0].deriv(0) = 1;
  qSurreal[1].deriv(1) = 1;
  qSurreal[2].deriv(2) = 1;

  // conservative flux
  ArrayQSurreal uConsSurreal = 0;
  avpde.masterState( paramL, x, time, qSurreal, uConsSurreal );

  MatrixQ dudq;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;

  avpde.jacobianMasterState( paramL, x, time, q, dudq );
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(0), dudq(0,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(1), dudq(0,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(2), dudq(0,2), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(0), dudq(1,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(1), dudq(1,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(2), dudq(1,2), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(0), dudq(2,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(1), dudq(2,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(2), dudq(2,2), small_tol, close_tol )

  // Should not accumulate
  avpde.jacobianMasterState( paramL, x, time, q, dudq );
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(0), dudq(0,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(1), dudq(0,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(2), dudq(0,2), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(0), dudq(1,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(1), dudq(1,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(2), dudq(1,2), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(0), dudq(2,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(1), dudq(2,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(2), dudq(2,2), small_tol, close_tol )
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxRoe, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEClass::Euler_ResidInterp_Raw);

  // Roe flux function test

  Real x, time;
  Real nx;
  Real rhoL, uL, tL, pL, h0L;
  Real rhoR, uR, tR, pR, h0R;
  Real Cp;

  x = time = 0;   // not actually used in functions
  nx = 1.0;
  rhoL = 1.034; uL = 3.26; tL = 5.78;
  rhoR = 0.973; uR = 1.79; tR = 6.13;
  Cp  = R*gamma/(gamma - 1);
  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL);
  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR);
  ParamType paramL(0.1, 0.0); // grid spacing and jump values

  // set
  ArrayQ qL = avpde.setDOFFrom( DensityVelocityTemperature1D<Real>(rhoL, uL, tL) );
  ArrayQ qR = avpde.setDOFFrom( DensityVelocityTemperature1D<Real>(rhoR, uR, tR) );

  // advective normal flux (average)
  Real fL[3] = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*h0L};
  Real fR[3] = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*h0R};

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < 3; k++)
    fnTrue[k] = 0.5*(nx*(fL[k] + fR[k]));
#if 1
  // Exact values for Roe scheme
  fnTrue[0] -= -0.63318276027140017468491006399040;
  fnTrue[1] -=  -3.8160625416529803007039694937525;
  fnTrue[2] -=  -12.431949286034470439767789109435;
#endif

  ArrayQ fn;
  fn = 0;
  avpde.fluxAdvectiveUpwind( paramL, x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );

  // Flux accumulate
  avpde.fluxAdvectiveUpwind( paramL, x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(2), fn(2), tol );

  //Call these simply to make sure coverage is high
  ArrayQ qxL, qxR, sourceL, sourceR;
  Real dummy = 0;
//  avpde.fluxViscous( paramL, dummy, dummy, qL, qxL, qR, qxR, nx, fn );
  avpde.sourceTrace( paramL, dummy, paramL, dummy, dummy, qL, qxL, qR, qxR, sourceL, sourceR );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxRoe_areaChange, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;
  std::shared_ptr<ScalarFunction1D_OffsetLog> area( new ScalarFunction1D_OffsetLog() );

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEClass::Euler_ResidInterp_Raw, area);

  // Roe flux function test

  Real x, time;
  Real nx;
  Real rhoL, uL, tL, pL, h0L;
  Real rhoR, uR, tR, pR, h0R;
  Real Cp;

  x = time = 1.0;
  nx = 1.0;
  rhoL = 1.034; uL = 3.26; tL = 5.78;
  rhoR = 0.973; uR = 1.79; tR = 6.13;
  Cp  = R*gamma/(gamma - 1);
  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL);
  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR);
  ParamType paramL(0.1, 0.0); // grid spacing and jump values

  // set
  ArrayQ qL = avpde.setDOFFrom( DensityVelocityTemperature1D<Real>(rhoL, uL, tL) );
  ArrayQ qR = avpde.setDOFFrom( DensityVelocityTemperature1D<Real>(rhoR, uR, tR) );

  // advective normal flux (average)
  Real fL[3] = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*h0L};
  Real fR[3] = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*h0R};

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < 3; k++)
    fnTrue[k] = 0.5*(nx*(fL[k] + fR[k]));
#if 1
  // Exact values for Roe scheme
  fnTrue[0] -= -0.63318276027140017468491006399040;
  fnTrue[1] -=  -3.8160625416529803007039694937525;
  fnTrue[2] -=  -12.431949286034470439767789109435;
#endif

  ArrayQ fn;
  fn = 0;
  avpde.fluxAdvectiveUpwind( paramL, x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( 10.*fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 10.*fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 10.*fnTrue(2), fn(2), tol );

  // Flux accumulate
  avpde.fluxAdvectiveUpwind( paramL, x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( 2*10.*fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 2*10.*fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 2*10.*fnTrue(2), fn(2), tol );

  //Call these simply to make sure coverage is high
  ArrayQ qxL, qxR, sourceL, sourceR;
  Real dummy = 0;
//  avpde.fluxViscous( paramL, dummy, dummy, qL, qxL, qR, qxR, nx, fn );
  avpde.sourceTrace( paramL, dummy, paramL, dummy, dummy, qL, qxL, qR, qxR, sourceL, sourceR );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( speedCharacteristic, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEClass::Euler_ResidInterp_Raw);

  Real x, time, dx;
  Real rho, u, t, c;
  Real speed, speedTrue;

  x = time = 0;   // not actually used in functions
  dx = 0.57;
  rho = 1.137; u = 0.784; t = 0.987;
  c = sqrt(gamma*R*t);
  speedTrue = fabs(dx*u)/sqrt(dx*dx) + c;
  ParamType paramL(0.1, 0.0); // grid spacing and jump values

  Real qDataPrim[3] = {rho, u, t};
  string qNamePrim[3] = {"Density", "VelocityX",  "Temperature"};
  ArrayQ q;
  avpde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );

  avpde.speedCharacteristic( paramL, x, time, dx, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  avpde.speedCharacteristic( paramL, x, time, q, speed );
  BOOST_CHECK_CLOSE( fabs(u) + c, speed, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( source, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEClass::Euler_ResidInterp_Raw);

  Real x, time;
  Real rho, u, p, t;
  x = time = 1.0;   // not actually used in functions
  rho = 1.137; u = 0.784; p = 0.987;
  t = p/(R*rho);
  ParamType paramL(0.1, 0.0); // grid spacing and jump values

  Real qDataPrim[3] = {rho, u, t};
  string qNamePrim[3] = {"Density", "VelocityX",  "Temperature"};
  ArrayQ q, qx = 0.0;
  avpde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );

  ArrayQ sourceterm = 0;
  avpde.source( paramL, x, time, q, qx, sourceterm );
  BOOST_CHECK_CLOSE( 0.0, sourceterm(0), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceterm(1), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceterm(2), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( source_areaChange, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;
  std::shared_ptr<ScalarFunction1D_OffsetLog> area( new ScalarFunction1D_OffsetLog() );

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEClass::Euler_ResidInterp_Raw, area);

  Real x, time;
  Real rho, u, p, t;

  x = time = 1.0;   // not actually used in functions
  rho = 1.137; u = 0.784; p = 0.987;
  t = p/(R*rho);
  ParamType paramL(0.1, 0.0); // grid spacing and jump values

  Real qDataPrim[3] = {rho, u, t};
  string qNamePrim[3] = {"Density", "VelocityX",  "Temperature"};
  ArrayQ q, qx = 0.0;
  avpde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );

  ArrayQ sourceterm = 0;
  avpde.source( paramL, x, time, q, qx, sourceterm );
  BOOST_CHECK_CLOSE( 0.0, sourceterm(0), tol );
  BOOST_CHECK_CLOSE( -p * 9.0/log(2.0) / (1.0+x), sourceterm(1), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceterm(2), tol );

  // Flux accumulate
  avpde.source( paramL, x, time, q, qx, sourceterm );
  BOOST_CHECK_CLOSE( 0.0, sourceterm(0), tol );
  BOOST_CHECK_CLOSE( 2*-p * 9.0/log(2.0) / (1.0+x), sourceterm(1), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceterm(2), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( isValidState, QType, QTypes )
{
  // Surrogates are special
  // Entropy has log( x < 0 ) when constructing invalid states
  if ( std::is_same<QType,QTypePrimitiveSurrogate>::value ) return;
  if ( std::is_same<QType,QTypeEntropy>::value ) return;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEClass::Euler_ResidInterp_Raw);

  ArrayQ q;
  Real rho, u = 0, t;

  rho = 1;
  t = 1;
  avpde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( avpde.isValidState(q) == true );

  rho = 1;
  t = -1;
  avpde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( avpde.isValidState(q) == false );

  rho = -1;
  t = 1;
  avpde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( avpde.isValidState(q) == false );

  rho = -1;
  t = -1;
  avpde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( avpde.isValidState(q) == false );

  rho = 1;
  t = 0;
  avpde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( avpde.isValidState(q) == false );

  rho = 0;
  t = 1;
  avpde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( avpde.isValidState(q) == false );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState_surrogate )
{
  typedef TraitsModelEuler<QTypePrimitiveSurrogate, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef AVPDEClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEClass::Euler_ResidInterp_Raw);

  ArrayQ q;
  Real rho, u = 0, t;

  rho = 1;
  t = 1;
  avpde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( avpde.isValidState(q) == true );

  rho = 1;
  t = -1;
  avpde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( avpde.isValidState(q) == true );

  rho = -1;
  t = 1;
  avpde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( avpde.isValidState(q) == true );

  rho = -1;
  t = -1;
  avpde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( avpde.isValidState(q) == true );

  rho = 1;
  t = 0;
  avpde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( avpde.isValidState(q) == true );

  rho = 0;
  t = 1;
  avpde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );
  BOOST_CHECK( avpde.isValidState(q) == true );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO_rhoPressure )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/PDEEulermitAVDiffusion1D_pattern.txt", true );

  typedef TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEClass::Euler_ResidInterp_Raw);

  avpde.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO_surrogate )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/PDEEulermitAVDiffusion1DSurrogate_pattern.txt", true );

  typedef TraitsModelEuler<QTypePrimitiveSurrogate, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEClass::Euler_ResidInterp_Raw);

  avpde.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
