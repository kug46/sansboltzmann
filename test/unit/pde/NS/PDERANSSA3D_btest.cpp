// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDERANSSA3D_btest
//
// test of 3-D compressible RANS with SA PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/list.hpp>

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "Topology/Dimension.h"
#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/QRANSSA3D.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/Q3DPrimitiveSurrogate.h"
#include "pde/NS/Q3DConservative.h"
#include "pde/NS/PDERANSSA3D.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
//Explicitly instantiate the class so coverage information is correct
namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
//class QTypeEntropy {};

template class TraitsSizeRANSSA<PhysD3>;
template class TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel>;
template class PDERANSSA3D< TraitsSizeRANSSA,
                            TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
template class PDERANSSA3D< TraitsSizeRANSSA,
                            TraitsModelRANSSA<QTypeConservative, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
template class PDERANSSA3D< TraitsSizeRANSSA,
                            TraitsModelRANSSA<QTypePrimitiveSurrogate, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDERANSSA3D_test_suite )


typedef boost::mpl::list< QTypePrimitiveRhoPressure,
                          QTypePrimitiveSurrogate,
                          QTypeConservative > QTypeList;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( static_test, QType, QTypeList )
{
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  BOOST_CHECK( PDEClass::D == 3 );
  BOOST_CHECK( PDEClass::N == 6 );
  BOOST_CHECK( ArrayQ::M == 6 );
  BOOST_CHECK( MatrixQ::M == 6 );
  BOOST_CHECK( MatrixQ::N == 6 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setDOFFrom )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real rho = 1.137;
  Real u = 0.784;
  Real v = -0.231;
  Real w = 10.69;
  Real t = 0.987;
  Real p = R*rho*t;
  Real E = gas.energy(rho,t) + 0.5*(u*u + v*v + w*w);
  Real nt = 4.2;

  PyDict d;

  // set
  Real qDataPrim[6] = {rho, u, v, w, t, nt};
  string qNamePrim[6] = {"Density", "VelocityX", "VelocityY", "VelocityZ", "Temperature", "SANutilde"};
  ArrayQ qTrue = {rho, u, v, w, p, nt};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 6 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );
  BOOST_CHECK_CLOSE( qTrue(5), q(5), tol );

  SAnt3D<DensityVelocityPressure3D<Real>> qdata1;
  qdata1.Density = rho; qdata1.VelocityX = u; qdata1.VelocityY = v; qdata1.VelocityZ = w; qdata1.Pressure = p; qdata1.SANutilde = nt;
  q = 0;
  pde.setDOFFrom( q, qdata1 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );
  BOOST_CHECK_CLOSE( qTrue(5), q(5), tol );

  PyDict rhoVP;
  rhoVP[NSVariableType3DParams::params.StateVector.Variables] = NSVariableType3DParams::params.StateVector.PrimitivePressure;
  rhoVP[SAnt3DParams<DensityVelocityPressure3DParams>::params.rho] = rho;
  rhoVP[SAnt3DParams<DensityVelocityPressure3DParams>::params.u] = u;
  rhoVP[SAnt3DParams<DensityVelocityPressure3DParams>::params.v] = v;
  rhoVP[SAnt3DParams<DensityVelocityPressure3DParams>::params.w] = w;
  rhoVP[SAnt3DParams<DensityVelocityPressure3DParams>::params.p] = p;
  rhoVP[SAnt3DParams<DensityVelocityPressure3DParams>::params.nt] = nt;

  d[SAVariableType3DParams::params.StateVector] = rhoVP;
  SAVariableType3DParams::checkInputs(d);
  q = pde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );
  BOOST_CHECK_CLOSE( qTrue(5), q(5), tol );


  SAnt3D<DensityVelocityTemperature3D<Real>> qdata2({rho, u, v, w, t, nt});
  q = 0;
  pde.setDOFFrom( q, qdata2 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );
  BOOST_CHECK_CLOSE( qTrue(5), q(5), tol );

  PyDict rhoVT;
  rhoVT[NSVariableType3DParams::params.StateVector.Variables] = NSVariableType3DParams::params.StateVector.PrimitiveTemperature;
  rhoVT[SAnt3DParams<DensityVelocityTemperature3DParams>::params.rho] = rho;
  rhoVT[SAnt3DParams<DensityVelocityTemperature3DParams>::params.u] = u;
  rhoVT[SAnt3DParams<DensityVelocityTemperature3DParams>::params.v] = v;
  rhoVT[SAnt3DParams<DensityVelocityTemperature3DParams>::params.w] = w;
  rhoVT[SAnt3DParams<DensityVelocityTemperature3DParams>::params.t] = t;
  rhoVT[SAnt3DParams<DensityVelocityTemperature3DParams>::params.nt]  = nt;

  d[SAVariableType3DParams::params.StateVector] = rhoVT;
  SAVariableType3DParams::checkInputs(d);
  q = pde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );
  BOOST_CHECK_CLOSE( qTrue(5), q(5), tol );


  SAnt3D<Conservative3D<Real>> qdata3({rho, rho*u, rho*v, rho*w, rho*E, rho*nt});
  q = 0;
  pde.setDOFFrom( q, qdata3 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );
  BOOST_CHECK_CLOSE( qTrue(5), q(5), tol );

  PyDict Conservative;
  Conservative[NSVariableType3DParams::params.StateVector.Variables] = NSVariableType3DParams::params.StateVector.Conservative;
  Conservative[SAnt3DParams<Conservative3DParams>::params.rho]   = rho;
  Conservative[SAnt3DParams<Conservative3DParams>::params.rhou]  = rho*u;
  Conservative[SAnt3DParams<Conservative3DParams>::params.rhov]  = rho*v;
  Conservative[SAnt3DParams<Conservative3DParams>::params.rhow]  = rho*w;
  Conservative[SAnt3DParams<Conservative3DParams>::params.rhoE]  = rho*E;
  Conservative[SAnt3DParams<Conservative3DParams>::params.rhont] = rho*nt;

  d[SAVariableType3DParams::params.StateVector] = Conservative;
  SAVariableType3DParams::checkInputs(d);
  q = pde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );
  BOOST_CHECK_CLOSE( qTrue(5), q(5), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( masterState_test, QType, QTypeList )
{
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

//  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-9;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 6 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // function tests

  Real x, y, z, time;
  Real rho, u, v, w, t, p, nt, e0;

  x = 0; y = 0; z = 0;   // not actually used in functions
  rho = 1.225;                      // kg/m^3
  u = 6.974; v = -3.231; w = 10.23; // m/s
  t = 288.15;                       // K
  nt = 4.279e-4;                    // m^2/s; gives chi = 29.3
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u + v*v + w*w);

  // set
  ArrayQ q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  // conservative flux
  ArrayQ uTrue = {rho, rho*u, rho*v, rho*w, rho*e0, rho*nt};
  ArrayQ uCons = 0;
  pde.masterState( x, y, z, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), close_tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), close_tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), close_tol );
  BOOST_CHECK_CLOSE( uTrue(3), uCons(3), close_tol );
  BOOST_CHECK_CLOSE( uTrue(4), uCons(4), close_tol );
  BOOST_CHECK_CLOSE( uTrue(5), uCons(5), close_tol );

  // viscous flux
  ArrayQ qx, qy, qz;
  Real rhox, ux, vx, wx, px, tx, ntx;
  Real rhoy, uy, vy, wy, py, ty, nty;
  Real rhoz, uz, vz, wz, pz, tz, ntz;

  ntx = 7.515e-5, nty = -3.421e-5, ntz = 8.621e-5;

  if ( std::is_same<QType,QTypePrimitiveRhoPressure>::value )
  {
    rhox = 0.37, ux = -0.85, vx = 0.09, wx =  0.36, px = 1.71;
    rhoy = 2.31, uy =  1.65, vy = 0.87, wy = -1.07, py = 0.29;
    rhoz = 0.45, uz =  0.12, vz = 1.27, wz =  0.07, pz = 2.12;
    tx = t*(px/p - rhox/rho);
    ty = t*(py/p - rhoy/rho);
    tz = t*(pz/p - rhoz/rho);

    qx = {rhox, ux, vx, wx, px, ntx};
    qy = {rhoy, uy, vy, wy, py, nty};
    qz = {rhoz, uz, vz, wz, pz, ntz};
  }
  else if ( std::is_same<QType,QTypePrimitiveSurrogate>::value )
  {
    rhox = 0.37, ux = -0.85, vx = 0.09, wx =  0.36, tx = 1.71;
    rhoy = 2.31, uy =  1.65, vy = 0.87, wy = -1.07, ty = 0.29;
    rhoz = 0.45, uz =  0.12, vz = 1.27, wz =  0.07, tz = 2.12;

    px = R*(rho*tx + rhox*t);
    py = R*(rho*ty + rhoy*t);
    pz = R*(rho*tz + rhoz*t);

    qx = {rhox, ux, vx, wx, tx, ntx};
    qy = {rhoy, uy, vy, wy, ty, nty};
    qz = {rhoz, uz, vz, wz, tz, ntz};
  }
  else if ( std::is_same<QType,QTypeConservative>::value )
  {
    rhox = 0.37, ux = -0.85, vx = 0.09, wx =  0.36, tx = 1.71;
    rhoy = 2.31, uy =  1.65, vy = 0.87, wy = -1.07, ty = 0.29;
    rhoz = 0.45, uz =  0.12, vz = 1.27, wz =  0.07, tz = 2.12;

    px = R*(rho*tx + rhox*t);
    py = R*(rho*ty + rhoy*t);
    pz = R*(rho*tz + rhoz*t);

    Real e0x = Cv*tx + (u*ux + v*vx + w*wx);
    Real e0y = Cv*ty + (u*uy + v*vy + w*wy);
    Real e0z = Cv*tz + (u*uz + v*vz + w*wz);

    qx = {rhox, rho*ux + rhox*u, rho*vx + rhox*v, rho*wx + rhox*w, rho*e0x + rhox*e0, rho*ntx + rhox*nt};
    qy = {rhoy, rho*uy + rhoy*u, rho*vy + rhoy*v, rho*wy + rhoy*w, rho*e0y + rhoy*e0, rho*nty + rhoy*nt};
    qz = {rhoz, rho*uz + rhoz*u, rho*vz + rhoz*v, rho*wz + rhoz*w, rho*e0z + rhoz*e0, rho*ntz + rhoz*nt};
  }
  else // Need to add the QType to this list
    BOOST_REQUIRE( false );

  Real rhoEx = px/(gamma-1) + 0.5*rhox*(u*u + v*v + w*w) + rho*(u*ux + v*vx + w*wx);
  Real rhoEy = py/(gamma-1) + 0.5*rhoy*(u*u + v*v + w*w) + rho*(u*uy + v*vy + w*wy);
  Real rhoEz = pz/(gamma-1) + 0.5*rhoz*(u*u + v*v + w*w) + rho*(u*uz + v*vz + w*wz);

  ArrayQ Ux = 0, Uy = 0, Uz = 0;
  ArrayQ UxTrue = 0, UyTrue = 0, UzTrue = 0;
// master state gradient
  pde.masterStateGradient(x, y, z, time, q, qx, qy, qz, Ux, Uy, Uz);

  UxTrue[0] = rhox;
  UxTrue[1] = rho*ux + rhox*u;
  UxTrue[2] = rho*vx + rhox*v;
  UxTrue[3] = rho*wx + rhox*w;
  UxTrue[4] = rhoEx;
  UxTrue[5] = rho*ntx + rhox*nt;

  UyTrue[0] = rhoy;
  UyTrue[1] = rho*uy + rhoy*u;
  UyTrue[2] = rho*vy + rhoy*v;
  UyTrue[3] = rho*wy + rhoy*w;
  UyTrue[4] = rhoEy;
  UyTrue[5] = rho*nty + rhoy*nt;

  UzTrue[0] = rhoz;
  UzTrue[1] = rho*uz + rhoz*u;
  UzTrue[2] = rho*vz + rhoz*v;
  UzTrue[3] = rho*wz + rhoz*w;
  UzTrue[4] = rhoEz;
  UzTrue[5] = rho*ntz + rhoz*nt;

  for (int i=0; i< 6; i++)
  {
    BOOST_CHECK_CLOSE( UxTrue(i), Ux(i), close_tol );
    BOOST_CHECK_CLOSE( UyTrue(i), Uy(i), close_tol );
    BOOST_CHECK_CLOSE( UzTrue(i), Uz(i), close_tol );
  }


  if ( std::is_same<QType,QTypePrimitiveRhoPressure>::value )
  {

    Real rhoxx = -0.253, uxx =  1.02, vxx =  0.99, wxx =  0.99, pxx =  2.99, ntxx =  0.345;
    Real rhoxy =  0.782, uxy = -0.95, vxy = -0.92, wxy =  0.82, pxy = -1.92, ntxy =  1.580;
    Real rhoyy =  1.080, uyy = -0.42, vyy = -0.44, wyy = -0.42, pyy = -1.44, ntyy = -0.152;
    Real rhoxz =  0.172, uxz = -0.95, vxz =  0.23, wxz = -0.92, pxz = -1.82, ntxz =  0.026;
    Real rhoyz =  0.602, uyz =  0.87, vyz = -0.35, wyz =  0.78, pyz =  1.04, ntyz =  0.145;
    Real rhozz =  1.267, uzz = -0.32, vzz =  0.65, wzz =  0.14, pzz =  1.52, ntzz = -1.053;

    ArrayQ qxx = {rhoxx, uxx, vxx, wxx, pxx, ntxx};
    ArrayQ qxy = {rhoxy, uxy, vxy, wxy, pxy, ntxy};
    ArrayQ qxz = {rhoxz, uxz, vxz, wxz, pxz, ntxz};
    ArrayQ qyy = {rhoyy, uyy, vyy, wyy, pyy, ntyy};
    ArrayQ qyz = {rhoyz, uyz, vyz, wyz, pyz, ntyz};
    ArrayQ qzz = {rhozz, uzz, vzz, wzz, pzz, ntzz};

    //  Real rhoE = p/(gamma-1) + 0.5*rho*(u*u + v*v);
    //  Real rhoEx = px/(gamma-1) + 0.5*rhox*(u*u + v*v) + rho*(u*ux + v*vx);
    //  Real rhoEy = py/(gamma-1) + 0.5*rhoy*(u*u + v*v) + rho*(u*uy + v*vy);

    Real rhoExx = pxx/(gamma-1.) + 0.5*rhoxx*(u*u + v*v + w*w) + 2.*rhox*(u*ux + v*vx + w*wx)
                + rho*(ux*ux + u*uxx + vx*vx + v*vxx + wx*wx + w*wxx);

    Real rhoExy = pxy/(gamma-1.) + 0.5*rhoxy*(u*u + v*v + w*w)
                   + rhox*(u*uy + v*vy + w*wy)  + rhoy*(u*ux + v*vx + w*wx) + rho*(uy*ux + u*uxy + vy*vx + v*vxy + wy*wx + w*wxy);

    Real rhoExz = pxz/(gamma-1.) + 0.5*rhoxz*(u*u + v*v + w*w)
                   + rhox*(u*uz + v*vz + w*wz)  + rhoz*(u*ux + v*vx + w*wx) + rho*(uz*ux + u*uxz + vz*vx + v*vxz + wx*wz + w*wxz);

    Real rhoEyy = pyy/(gamma-1.) + 0.5*rhoyy*(u*u + v*v + w*w) + 2.*rhoy*(u*uy + v*vy + w*wy)
                + rho*(uy*uy + u*uyy + vy*vy + v*vyy + wy*wy + w*wyy);

    Real rhoEyz = pyz/(gamma-1.) + 0.5*rhoyz*(u*u + v*v + w*w)
                   + rhoy*(u*uz + v*vz + w*wz)  + rhoz*(u*uy + v*vy + w*wy) + rho*(uz*uy + u*uyz + vz*vy + v*vyz + wy*wz + w*wyz);

    Real rhoEzz = pzz/(gamma-1.) + 0.5*rhozz*(u*u + v*v + w*w) + 2.*rhoz*(u*uz + v*vz + w*wz)
                + rho*(uz*uz + u*uzz + vz*vz + v*vzz + wz*wz + w*wzz);

    ArrayQ Uxx = 0, Uxy = 0, Uxz = 0, Uyy = 0, Uyz = 0, Uzz = 0;
    ArrayQ UxxTrue = 0, UxyTrue = 0, UxzTrue = 0, UyyTrue = 0, UyzTrue = 0, UzzTrue = 0;
    // master state gradient
    pde.masterStateHessian(x, y, z, time, q, qx, qy, qz,
                           qxx, qxy, qxz, qyy, qyz, qzz,
                           Uxx, Uxy, Uxz, Uyy, Uyz, Uzz);

    UxxTrue[0] = rhoxx;
    UxxTrue[1] = rho*uxx + 2*rhox*ux + rhoxx*u;
    UxxTrue[2] = rho*vxx + 2*rhox*vx + rhoxx*v;
    UxxTrue[3] = rho*wxx + 2*rhox*wx + rhoxx*w;
    UxxTrue[4] = rhoExx;
    UxxTrue[5] = rho*ntxx + 2*rhox*ntx + rhoxx*nt;

    UxyTrue[0] = rhoxy;
    UxyTrue[1] = rho*uxy + rhox*uy + rhoy*ux + rhoxy*u;
    UxyTrue[2] = rho*vxy + rhox*vy + rhoy*vx + rhoxy*v;
    UxyTrue[3] = rho*wxy + rhox*wy + rhoy*wx + rhoxy*w;
    UxyTrue[4] = rhoExy;
    UxyTrue[5] = rho*ntxy + rhox*nty + rhoy*ntx + rhoxy*nt;

    UxzTrue[0] = rhoxz;
    UxzTrue[1] = rho*uxz + rhox*uz + rhoz*ux + rhoxz*u;
    UxzTrue[2] = rho*vxz + rhox*vz + rhoz*vx + rhoxz*v;
    UxzTrue[3] = rho*wxz + rhox*wz + rhoz*wx + rhoxz*w;
    UxzTrue[4] = rhoExz;
    UxzTrue[5] = rho*ntxz + rhox*ntz + rhoz*ntx + rhoxz*nt;

    UyyTrue[0] = rhoyy;
    UyyTrue[1] = rho*uyy + 2*rhoy*uy + rhoyy*u;
    UyyTrue[2] = rho*vyy + 2*rhoy*vy + rhoyy*v;
    UyyTrue[3] = rho*wyy + 2*rhoy*wy + rhoyy*w;
    UyyTrue[4] = rhoEyy;
    UyyTrue[5] = rho*ntyy + 2*rhoy*nty + rhoyy*nt;

    UyzTrue[0] = rhoyz;
    UyzTrue[1] = rho*uyz + rhoy*uz + rhoz*uy + rhoyz*u;
    UyzTrue[2] = rho*vyz + rhoy*vz + rhoz*vy + rhoyz*v;
    UyzTrue[3] = rho*wyz + rhoy*wz + rhoz*wy + rhoyz*w;
    UyzTrue[4] = rhoEyz;
    UyzTrue[5] = rho*ntyz + rhoy*ntz + rhoz*nty + rhoyz*nt;

    UzzTrue[0] = rhozz;
    UzzTrue[1] = rho*uzz + 2*rhoz*uz + rhozz*u;
    UzzTrue[2] = rho*vzz + 2*rhoz*vz + rhozz*v;
    UzzTrue[3] = rho*wzz + 2*rhoz*wz + rhozz*w;
    UzzTrue[4] = rhoEzz;
    UzzTrue[5] = rho*ntzz + 2*rhoz*ntz + rhozz*nt;

    for (int i=0; i< 6; i++)
    {
      BOOST_CHECK_CLOSE( UxxTrue(i), Uxx(i), close_tol );
      BOOST_CHECK_CLOSE( UxyTrue(i), Uxy(i), close_tol );
      BOOST_CHECK_CLOSE( UxzTrue(i), Uxz(i), close_tol );
      BOOST_CHECK_CLOSE( UyyTrue(i), Uyy(i), close_tol );
      BOOST_CHECK_CLOSE( UyzTrue(i), Uyz(i), close_tol );
      BOOST_CHECK_CLOSE( UzzTrue(i), Uzz(i), close_tol );
    }
  }

}




//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux_test, QType, QTypeList )
{
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-10;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // SA2012 constants
  const Real sigma = 2./3.;
  const Real cv1   = 7.1;
  const Real prandtlEddy = 0.9;
  const Real cn1   = 16;

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 6 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // function tests

  Real x, y, z, time;
  Real rho, u, v, w, t, p, nt, e0, h0;

  x = 0; y = 0; z = 0;   // not actually used in functions
  rho = 1.225;                      // kg/m^3
  u = 6.974; v = -3.231; w = 10.23; // m/s
  t = 288.15;                       // K
  nt = 4.279e-4;                    // m^2/s; gives chi = 29.3
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u + v*v + w*w);
  h0 = Cp*t + 0.5*(u*u + v*v + w*w);

  // set
  ArrayQ q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  // conservative flux
  ArrayQ uTrue = {rho, rho*u, rho*v, rho*w, rho*e0, rho*nt};
  ArrayQ uCons = 0;
  pde.masterState( x, y, z, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), close_tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), close_tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), close_tol );
  BOOST_CHECK_CLOSE( uTrue(3), uCons(3), close_tol );
  BOOST_CHECK_CLOSE( uTrue(4), uCons(4), close_tol );
  BOOST_CHECK_CLOSE( uTrue(5), uCons(5), close_tol );

  // conservative flux
  ArrayQ ftTrue = {rho, rho*u, rho*v, rho*w, rho*e0, rho*nt};
  ArrayQ ft = 0;
  pde.fluxAdvectiveTime( x, y, z, time, q, ft );
  for (int i=0; i<6; i++)
    BOOST_CHECK_CLOSE( ftTrue(i), ft(i), close_tol );

  pde.fluxAdvectiveTime( x, y, z, time, q, ft );
  for (int i=0; i<6; i++)
    BOOST_CHECK_CLOSE( 2*ftTrue(i), ft(i), close_tol );

  // advective flux
  ArrayQ fTrue = {rho*u, rho*u*u + p, rho*u*v    , rho*u*w    , rho*u*h0, rho*u*nt};
  ArrayQ gTrue = {rho*v, rho*v*u    , rho*v*v + p, rho*v*w    , rho*v*h0, rho*v*nt};
  ArrayQ hTrue = {rho*w, rho*w*u    , rho*w*v    , rho*w*w + p, rho*w*h0, rho*w*nt};
  ArrayQ f, g, h;
  f = 0;  g = 0;  h = 0;
  pde.fluxAdvective( x, y, z, time, q, f, g, h );
  for (int i=0; i<6; i++)
  {
    BOOST_CHECK_CLOSE( fTrue(i), f(i), close_tol );
    BOOST_CHECK_CLOSE( gTrue(i), g(i), close_tol );
    BOOST_CHECK_CLOSE( hTrue(i), h(i), close_tol );
  }
  pde.fluxAdvective( x, y, z, time, q, f, g, h );
  for (int i=0; i<6; i++)
  {
    BOOST_CHECK_CLOSE( 2*fTrue(i), f(i), close_tol );
    BOOST_CHECK_CLOSE( 2*gTrue(i), g(i), close_tol );
    BOOST_CHECK_CLOSE( 2*hTrue(i), h(i), close_tol );
  }

  // viscous flux
  ArrayQ qx, qy, qz;
  Real rhox, ux, vx, wx, px, tx, ntx;
  Real rhoy, uy, vy, wy, py, ty, nty;
  Real rhoz, uz, vz, wz, pz, tz, ntz;

  ntx = 7.515e-5, nty = -3.421e-5, ntz = 8.621e-5;

  if ( std::is_same<QType,QTypePrimitiveRhoPressure>::value )
  {
    rhox = 0.37, ux = -0.85, vx = 0.09, wx =  0.36, px = 1.71;
    rhoy = 2.31, uy =  1.65, vy = 0.87, wy = -1.07, py = 0.29;
    rhoz = 0.45, uz =  0.12, vz = 1.27, wz =  0.07, pz = 2.12;
    tx = t*(px/p - rhox/rho);
    ty = t*(py/p - rhoy/rho);
    tz = t*(pz/p - rhoz/rho);

    qx = {rhox, ux, vx, wx, px, ntx};
    qy = {rhoy, uy, vy, wy, py, nty};
    qz = {rhoz, uz, vz, wz, pz, ntz};
  }
  else if ( std::is_same<QType,QTypePrimitiveSurrogate>::value )
  {
    rhox = 0.37, ux = -0.85, vx = 0.09, wx =  0.36, tx = 1.71;
    rhoy = 2.31, uy =  1.65, vy = 0.87, wy = -1.07, ty = 0.29;
    rhoz = 0.45, uz =  0.12, vz = 1.27, wz =  0.07, tz = 2.12;

    qx = {rhox, ux, vx, wx, tx, ntx};
    qy = {rhoy, uy, vy, wy, ty, nty};
    qz = {rhoz, uz, vz, wz, tz, ntz};
  }
  else if ( std::is_same<QType,QTypeConservative>::value )
  {
    rhox = 0.37, ux = -0.85, vx = 0.09, wx =  0.36, tx = 1.71;
    rhoy = 2.31, uy =  1.65, vy = 0.87, wy = -1.07, ty = 0.29;
    rhoz = 0.45, uz =  0.12, vz = 1.27, wz =  0.07, tz = 2.12;

    Real e0x = Cv*tx + (u*ux + v*vx + w*wx);
    Real e0y = Cv*ty + (u*uy + v*vy + w*wy);
    Real e0z = Cv*tz + (u*uz + v*vz + w*wz);

    qx = {rhox, rho*ux + rhox*u, rho*vx + rhox*v, rho*wx + rhox*w, rho*e0x + rhox*e0, rho*ntx + rhox*nt};
    qy = {rhoy, rho*uy + rhoy*u, rho*vy + rhoy*v, rho*wy + rhoy*w, rho*e0y + rhoy*e0, rho*nty + rhoy*nt};
    qz = {rhoz, rho*uz + rhoz*u, rho*vz + rhoz*v, rho*wz + rhoz*w, rho*e0z + rhoz*e0, rho*ntz + rhoz*nt};
  }
  else // Need to add the QType to this list
    BOOST_REQUIRE( false );

  f = 0; fTrue = 0;
  g = 0; gTrue = 0;
  h = 0; hTrue = 0;
  pde.fluxViscous(x, y, z, time, q, qx, qy, qz, f, g, h);

  Real chi  = rho*nt/muRef;
  Real chi3 = power<3>(chi);

  Real fv1 = chi3 / (chi3 + power<3>(cv1));
  Real muEddy = rho*nt*fv1;

#ifdef SANS_SA_QCR
  Real cr1 = 0.3;

  Real lambdaEddy = -2./3. * muEddy;

  Real tauRxx = muEddy*(2*ux   ) + lambdaEddy*(ux + vy + wz);
  Real tauRyy = muEddy*(2*vy   ) + lambdaEddy*(ux + vy + wz);
  Real tauRzz = muEddy*(2*wz   ) + lambdaEddy*(ux + vy + wz);
  Real tauRxy = muEddy*(uy + vx);
  Real tauRxz = muEddy*(uz + wx);
  Real tauRyz = muEddy*(vz + wy);

  Real magGradV = sqrt(ux*ux + uy*uy + uz*uz +
                       vx*vx + vy*vy + vz*vz +
                       wx*wx + wy*wy + wz*wz);

  Real Wxy = (uy - vx);
  Real Wxz = (uz - wx);
  Real Wyz = (vz - wy);

  Real Oxy = Wxy/magGradV;
  Real Oyx = -Oxy;

  Real Oxz = Wxz/magGradV;
  Real Ozx = -Oxz;

  Real Oyz = Wyz/magGradV;
  Real Ozy = -Oyz;

  Real qcrxx = cr1*2*(Oxy*tauRxy + Oxz*tauRxz);
  Real qcrxy = cr1*  (Oxy*tauRyy + Oxz*tauRyz + Oyx*tauRxx + Oyz*tauRxz);
  Real qcrxz = cr1*  (Oxy*tauRyz + Oxz*tauRzz + Ozx*tauRxx + Ozy*tauRxy);

  Real qcryy = cr1*2*(Oyx*tauRxy + Oyz*tauRyz);
  Real qcryz = cr1*  (Oyx*tauRxz + Oyz*tauRzz + Ozx*tauRxy + Ozy*tauRyy);

  Real qcrzz = cr1*  (Ozx*tauRxz + Ozy*tauRyz + Ozx*tauRxz + Ozy*tauRyz);

  fTrue(pde.ixMom) += qcrxx;
  fTrue(pde.iyMom) += qcrxy;
  fTrue(pde.izMom) += qcrxz;
  fTrue(pde.iEngy) += u*qcrxx + v*qcrxy + w*qcrxz;

  gTrue(pde.ixMom) += qcrxy;
  gTrue(pde.iyMom) += qcryy;
  gTrue(pde.izMom) += qcryz;
  gTrue(pde.iEngy) += u*qcrxy + v*qcryy + w*qcryz;

  hTrue(pde.ixMom) += qcrxz;
  hTrue(pde.iyMom) += qcryz;
  hTrue(pde.izMom) += qcrzz;
  hTrue(pde.iEngy) += u*qcrxz + v*qcryz + w*qcrzz;
#endif

  Real lambda = -2./3. * (muRef + muEddy);
  Real k = muRef*Cp/Prandtl + muEddy*Cp/prandtlEddy;

  Real tauxx = (muRef + muEddy)*(2*ux   ) + lambda*(ux + vy + wz);
  Real tauxy = (muRef + muEddy)*(uy + vx);
  Real tauyy = (muRef + muEddy)*(2*vy   ) + lambda*(ux + vy + wz);
  Real tauxz = (muRef + muEddy)*(uz + wx);
  Real tauyz = (muRef + muEddy)*(vz + wy);
  Real tauzz = (muRef + muEddy)*(2*wz   ) + lambda*(ux + vy + wz);

  fTrue(1) -= tauxx;
  fTrue(2) -= tauxy;
  fTrue(3) -= tauxz;
  fTrue(4) -= k*tx + u*tauxx + v*tauxy + w*tauxz;
  fTrue(5) -= (muRef + rho*nt)*ntx/sigma;

  gTrue(1) -= tauxy;
  gTrue(2) -= tauyy;
  gTrue(3) -= tauyz;
  gTrue(4) -= k*ty + u*tauxy + v*tauyy + w*tauyz;
  gTrue(5) -= (muRef + rho*nt)*nty/sigma;

  hTrue(1) -= tauxz;
  hTrue(2) -= tauyz;
  hTrue(3) -= tauzz;
  hTrue(4) -= k*tz + u*tauxz + v*tauyz + w*tauzz;
  hTrue(5) -= (muRef + rho*nt)*ntz/sigma;

  for (int i=0; i<6; i++)
  {
    BOOST_CHECK_CLOSE( fTrue(i), f(i), close_tol );
    BOOST_CHECK_CLOSE( gTrue(i), g(i), close_tol );
    BOOST_CHECK_CLOSE( hTrue(i), h(i), close_tol );
  }


  pde.fluxViscous(x, y, z, time, q, qx, qy, qz, f, g, h);
  for (int i=0; i<6; i++)
  {
    BOOST_CHECK_CLOSE( 2*fTrue(i), f(i), close_tol );
    BOOST_CHECK_CLOSE( 2*gTrue(i), g(i), close_tol );
    BOOST_CHECK_CLOSE( 2*hTrue(i), h(i), close_tol );
  }

  Real nx, ny, nz;

  nx = 1; ny = 0; nz = 0;
  f = 0;
  pde.fluxViscous(x, y, z, time, q, qx, qy, qz, q, qx, qy, qz, nx, ny, nz, f);
  for (int i=0; i<6; i++)
    SANS_CHECK_CLOSE( fTrue(i), f(i), small_tol, close_tol );


  pde.fluxViscous(x, y, z, time, q, qx, qy, qz, q, qx, qy, qz, nx, ny, nz, f);
  for (int i=0; i<6; i++)
    SANS_CHECK_CLOSE( 2*fTrue(i), f(i), small_tol, close_tol );

  nx = 0; ny = 1; nz = 0;
  f = 0;
  pde.fluxViscous(x, y, z, time, q, qx, qy, qz, q, qx, qy, qz, nx, ny, nz, f);
  for (int i=0; i<6; i++)
    SANS_CHECK_CLOSE( gTrue(i), f(i), small_tol, close_tol );

  pde.fluxViscous(x, y, z, time, q, qx, qy, qz, q, qx, qy, qz, nx, ny, nz, f);
  for (int i=0; i<6; i++)
    SANS_CHECK_CLOSE( 2*gTrue(i), f(i), small_tol, close_tol );


  nx = 0; ny = 0; nz = 1;
  f = 0;
  pde.fluxViscous(x, y, z, time, q, qx, qy, qz, q, qx, qy, qz, nx, ny, nz, f);
  for (int i=0; i<6; i++)
    SANS_CHECK_CLOSE( hTrue(i), f(i), small_tol, close_tol );

  pde.fluxViscous(x, y, z, time, q, qx, qy, qz, q, qx, qy, qz, nx, ny, nz, f);
  for (int i=0; i<6; i++)
    SANS_CHECK_CLOSE( 2*hTrue(i), f(i), small_tol, close_tol );

  // viscous flux -- negative SA

  nt *= -1;
  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  if ( std::is_same<QType,QTypeConservative>::value )
  {
    qx[5] = rho*ntx + rhox*nt;
    qy[5] = rho*nty + rhoy*nt;
    qz[5] = rho*ntz + rhoz*nt;
  }

  f = 0; fTrue = 0;
  g = 0; gTrue = 0;
  h = 0; hTrue = 0;
  pde.fluxViscous(x, y, z, time, q, qx, qy, qz, f, g, h);

  chi  = rho*nt/muRef;
  chi3 = power<3>(chi);
  Real fn  = (cn1 + chi3) / (cn1 - chi3);

  lambda = -2./3. * muRef;
  k = muRef*Cp/Prandtl;

  tauxx = muRef*(2*ux   ) + lambda*(ux + vy + wz);
  tauxy = muRef*(uy + vx);
  tauyy = muRef*(2*vy   ) + lambda*(ux + vy + wz);
  tauxz = muRef*(uz + wx);
  tauyz = muRef*(vz + wy);
  tauzz = muRef*(2*wz   ) + lambda*(ux + vy + wz);

  fTrue(1) -= tauxx;
  fTrue(2) -= tauxy;
  fTrue(3) -= tauxz;
  fTrue(4) -= k*tx + u*tauxx + v*tauxy + w*tauxz;
  fTrue(5) -= (muRef + rho*nt*fn)*ntx/sigma;

  gTrue(1) -= tauxy;
  gTrue(2) -= tauyy;
  gTrue(3) -= tauyz;
  gTrue(4) -= k*ty + u*tauxy + v*tauyy + w*tauyz;
  gTrue(5) -= (muRef + rho*nt*fn)*nty/sigma;

  hTrue(1) -= tauxz;
  hTrue(2) -= tauyz;
  hTrue(3) -= tauzz;
  hTrue(4) -= k*tz + u*tauxz + v*tauyz + w*tauzz;
  hTrue(5) -= (muRef + rho*nt*fn)*ntz/sigma;

  for (int i=0; i<6; i++)
  {
    BOOST_CHECK_CLOSE( fTrue(i), f(i), close_tol );
    BOOST_CHECK_CLOSE( gTrue(i), g(i), close_tol );
    BOOST_CHECK_CLOSE( hTrue(i), h(i), close_tol );
  }

  nx = 1; ny = 0; nz = 0;
  f = 0;
  pde.fluxViscous(x, y, z, time, q, qx, qy, qz, q, qx, qy, qz, nx, ny, nz, f);
  for (int i=0; i<6; i++)
    SANS_CHECK_CLOSE( fTrue(i), f(i), small_tol, close_tol );

  nx = 0; ny = 1; nz = 0;
  f = 0;
  pde.fluxViscous(x, y, z, time, q, qx, qy, qz, q, qx, qy, qz, nx, ny, nz, f);
  for (int i=0; i<6; i++)
    SANS_CHECK_CLOSE( gTrue(i), f(i), small_tol, close_tol );

  nx = 0; ny = 0; nz = 1;
  f = 0;
  pde.fluxViscous(x, y, z, time, q, qx, qy, qz, q, qx, qy, qz, nx, ny, nz, f);
  for (int i=0; i<6; i++)
    SANS_CHECK_CLOSE( hTrue(i), f(i), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( jacobianMasterState_test, QType, QTypeList )
{
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  const Real small_tol = 5.e-8;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // function tests

  Real rho, u, v, w, t, nt;

  rho = 1.225;                        // kg/m^3
  u = 6.974; v = -3.231; w = 10.23;   // m/s
  t = 288.15;                         // K
  nt = 4.279e-4;                      // m^2/s; gives chi = 29.3

  Real x= 0, y = 0, z = 0, time = 0;

  // set
  ArrayQ q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  // conservative flux
  ArrayQ uCons0(0), uCons1(0);
  MatrixQ dudq_diff[2] = {0,0};

  std::vector<Real> step = {1e-3, 5e-4};
  std::vector<Real> rate_range = {1.9, 2.1};

  for (std::size_t istep = 0; istep < step.size(); istep++ )
  {
    for (int ivar = 0; ivar < ArrayQ::M; ivar++)
    {
      q[ivar] -= step[istep];
      uCons0 = 0;
      pde.masterState( x, y, z, time, q, uCons0 );

      q[ivar] += 2*step[istep];
      uCons1 = 0;
      pde.masterState( x, y, z, time, q, uCons1 );

      q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

      for (int iEq = 0; iEq < pde.N; iEq++)
        dudq_diff[istep](iEq,ivar) = (uCons1[iEq] - uCons0[iEq])/(2*step[istep]);
    }
  }

  MatrixQ dudq = 0;
  pde.jacobianMasterState(x, y, z, time, q, dudq);

  for (int iEq = 0; iEq < pde.N; iEq++)
  {
    for (int ivar = 0; ivar < ArrayQ::M; ivar++)
    {
      Real err_vec[2] = { fabs( dudq(iEq,ivar) - dudq_diff[0](iEq,ivar) ),
                          fabs( dudq(iEq,ivar) - dudq_diff[1](iEq,ivar) )};

#if 0
      std::cout << "dudq(" << iEq << "," << ivar << ") = " << dudq(iEq,ivar) <<
                   " dudq_diff[0] " << dudq_diff[0](iEq,ivar) <<
                   " dudq_diff[1] " << dudq_diff[1](iEq,ivar) << std::endl;
#endif

      // Error in finite-difference jacobian is either zero as for linear or quadratic residuals,
      // or is nonzero for general nonlinear residual where we need to check error convergence rate
      if (err_vec[0] > small_tol && err_vec[1] > small_tol)
      {
        Real rate = log(err_vec[1]/err_vec[0])/log(step[1]/step[0]);

        BOOST_CHECK_MESSAGE( rate >= rate_range[0] && rate <= rate_range[1],
                             "Rate check failed at : ivar = " << ivar <<
                             " iEq = " << iEq << ": rate = " << rate <<
                             ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" );
      }

    }
  }
}

//----------------------------------------------------------------------------//
// identical to PDEEuler3D_best since jacobianFluxAdvectiveAbsoluteValue inherited
BOOST_AUTO_TEST_CASE_TEMPLATE( jacobianFluxAdvectiveAbsoluteValue_test, QType, QTypeList )
{
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  const Real small_tol = 1.e-14;
  const Real close_tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, z, time;
  Real nx, ny, nz;
  Real rho, u, v, w, t, nt;

  x = y = z = time = 0;   // not actually used in functions
  rho = 1.034; u = 1.26; v = -2.17; w = 0.56; t = 5.78;
  nt = 7.33;    // enormous, but makes jacobian entries O(1)

  ArrayQ q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  MatrixQ mtx, mtxTrue;

  // {qn - c, qn, qn + c} = {neg, neg, neg}

  nx = -0.856;  ny = 0.48;  nz = 0.192;   // Note: magnitude = 1

  mtxTrue =
    {{0, 0.856, -0.48, -0.192, 0, 0},
     {-1.40427728, 2.659776, 0.138208, -0.433664, 0.3424, 0},
     {3.7328592, -1.6156, 2.6376, 0.52416, -0.192, 0},
     {-1.38090624, 0.576128, -0.435456, 1.948128, -0.0768, 0},
     {-20.2774083792, 8.74150424, -3.72361248, -2.63906496, 2.817696, 0},
     {-14.7526512, 6.27448, -3.5184, -1.40736, 0, 2.01264}};

  mtx = 0;
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, nx, ny, nz, mtx );

  for (int i = 0; i < 6; i++)
    for (int j = 0; j < 6; j++)
      SANS_CHECK_CLOSE( mtxTrue(i,j), mtx(i,j), small_tol, close_tol );

  // {qn - c, qn, qn + c} = {neg, neg, pos}

  nx = -0.192;  ny = 0.48;  nz = -0.856;   // Note: magnitude = 1

  mtxTrue =
    {{0.05029920307361089, 0.1824919733978881, -0.4606177577277211, 0.8362543552680354, 0.004477371666327435, 0},
     {-1.921398782191514, 1.899336237370957, -0.4204175689463774, 1.017493211701233, 0.08089487297855683, 0},
     {3.125167774490924, -0.1622984594974613, 2.362518519792777, -1.724203761090408, -0.1978493582133912, 0},
     {0.09514114318471302, -0.3145857638476372, 0.4552126481553504, 2.069847500399908, 0.3380120014936149, 0},
     {-17.34733614186995, 1.221534440499240, -3.780976817210479, 9.198573194484009, 2.504860322410591, 0},
     {-12.55321724147043, 1.337666165006520, -3.376328164144196, 6.129744424114699, 0.03281913431418010, 1.76288}};

  mtx = 0;
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, nx, ny, nz, mtx );

  for (int i = 0; i < 6; i++)
    for (int j = 0; j < 6; j++)
      SANS_CHECK_CLOSE( mtxTrue(i,j), mtx(i,j), small_tol, close_tol );

  // {qn - c, qn, qn + c} = {neg, pos, pos}

  nx = -0.48;  ny = -0.856;  nz = -0.192;   // Note: magnitude = 1

  mtxTrue =
    {{0.6833178495735146, -0.4073576217113168, -0.3695186359806880, -0.1674683835739656, 0.08080955159712328, 0},
     {-0.6264461084779726, 0.9365814097471189, -0.4621211994809992, -0.08230531611978917, -0.02039499966720123, 0},
     {0.9229712316480684, 1.427262129147915, 1.953247676052250, 0.5929300364992487, -0.3933068721443357, 0},
     {-0.2764438438150722, -0.1062594629169862, -0.2055415234073182, 1.102899644071942, -0.003632664977441590, 0},
     {-5.157984981879059, -5.369524110454105, -4.219706655995295, -2.215713855583411, 2.357775203603017, 0},
     {-3.385596162626138, -2.985931367143952, -2.708571601738443, -1.227543251597168, 0.5923340132069137, 1.1452}};

  mtx = 0;
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, nx, ny, nz, mtx );

  for (int i = 0; i < 6; i++)
    for (int j = 0; j < 6; j++)
      SANS_CHECK_CLOSE( mtxTrue(i,j), mtx(i,j), small_tol, close_tol );

  // {qn - c, qn, qn + c} = {pos, pos, pos}

  nx = -0.192;  ny = -0.856;  nz = 0.48;   // Note: magnitude = 1

  mtxTrue =
    {{0, -0.192, -0.856, 0.48, 0, 0},
     {-2.62817184, 1.739248, -1.245216, 0.647808, -0.0768, 0},
     {2.95749888, 0.848064, 2.998912, -0.849856, -0.3424, 0},
     {-0.4206944, -0.34944, -0.06272, 2.04568, 0.192, 0},
     {-18.985386532, -3.1379712, -8.1202156, 5.0484784, 2.63816, 0},
     {-13.812652, -1.40736, -6.27448, 3.5184, 0, 1.8844}};

  mtx = 0;
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, nx, ny, nz, mtx );

  for (int i = 0; i < 6; i++)
    for (int j = 0; j < 6; j++)
      SANS_CHECK_CLOSE( mtxTrue(i,j), mtx(i,j), small_tol, close_tol );

  // {qn - c, qn, qn + c} = {pos, pos, pos}

  nx = u;  ny = v;  nz = w;   // Note: magnitude > 1

  mtxTrue =
  {{0, 1.26, -2.17, 0.56, 0, 0},
   {-6.6629808, 7.56266, -1.64052, 0.42336, 0.504, 0},
   {11.4751336, -1.64052, 9.43544, -0.72912, -0.868, 0},
   {-2.9613248, 0.42336, -0.72912, 6.79826, 0.224, 0},
   {-66.596955803, 11.0287926, -18.9940317, 4.9016856, 9.25414, 0},
   {-48.452033, 9.2358, -15.9061, 4.1048, 0, 6.6101}};

  mtx = 0;
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, nx, ny, nz, mtx );

  for (int i = 0; i < 6; i++)
    for (int j = 0; j < 6; j++)
      SANS_CHECK_CLOSE( mtxTrue(i,j), mtx(i,j), small_tol, close_tol );
}


#ifndef SANS_SA_QCR
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( diffusionViscous )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-10;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, z, time;
  Real rho, u, v, w, t;
  Real nt, chi;

  x = 0; y = 0; time = 0;   // not actually used in functions
  rho = 1.225;
  u = 6.974;
  v = -3.231;
  w = 10.23;
  t = 2.881;

  // gradient

  Real rhox = 0.0421, rhoy = 0.0127, rhoz = 0.691;
  Real ux =  0.0521, uy = -0.0124, uz = -0.145;
  Real vx = -0.0312, vy = -0.0022, vz =  0.089;
  Real wx = -0.0546, wy = -0.0735, wz =  0.032;
  Real tx = -0.0715, ty = -0.0111, tz =  0.567;
  Real ntx = 7.515e-5, nty = -3.421e-5, ntz = 8.621e-5;
  Real px, py, pz;

  px = R*(rho*tx + rhox*t);
  py = R*(rho*ty + rhoy*t);
  pz = R*(rho*tz + rhoz*t);

  ArrayQ qx = {rhox, ux, vx, wx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, wy, py, nty};
  ArrayQ qz = {rhoz, uz, vz, wz, pz, ntz};

  // case 1: nt > 0

  chi = 5.241;
  nt  = chi*(muRef/rho);

  ArrayQ q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  MatrixQ kxx = 0, kxy = 0, kxz = 0, kyx = 0, kyy = 0, kyz = 0, kzx = 0, kzy = 0, kzz = 0;
  pde.diffusionViscous( x, y, z, time,
                        q, qx, qy, qz,
                        kxx, kxy, kxz,
                        kyx, kyy, kyz,
                        kzx, kzy, kzz );

#if 0    // not fully implemented; matrices from 2D
  MatrixQ kxxTrue =
    {{0, 0, 0, 0, 0},
     {-19.00241147480699, 2.724750713336247, 0, 0, 0},
     {6.602752166092062, 0, 2.043563035002186, 0, 0},
     {-75.76382912041833, -5.381036833102752, 4.693909933543209, 3.496336149685939, 0},
     {-26.69016970819981*rho, 0, 0, 0, 6.238400666389005*rho}};

  MatrixQ kxyTrue =
    {{0, 0, 0, 0, 0},
     {-4.401834777394708, 0, -1.362375356668124, 0, 0},
     {-14.25180860610524, 2.043563035002186, 0, 0, 0},
     {15.34919786877535, -6.602752166092062, -9.501205737403495, 0, 0},
     {0, 0, 0, 0, 0}};

  MatrixQ kyxTrue =
    {{0, 0, 0, 0, 0},
     {6.602752166092062, 0, 2.043563035002186, 0, 0},
     {9.501205737403495, -1.362375356668124, 0, 0, 0},
     {15.34919786877535, 4.401834777394708, 14.25180860610524, 0, 0},
     {0, 0, 0, 0, 0}};

  MatrixQ kyyTrue =
    {{0, 0, 0, 0, 0},
     {-14.25180860610524, 2.043563035002186, 0, 0, 0},
     {8.803669554789415, 0, 2.724750713336247, 0, 0},
     {-49.74428879697349, -10.13163970180450, 2.492992544845855, 3.496336149685939, 0},
     {-26.69016970819981*rho, 0, 0, 0, 6.238400666389005*rho}};

  for (int i = 0; i < 6; i++)
  {
    for (int j = 0; j < 6; j++)
    {
      SANS_CHECK_CLOSE( kxxTrue(i,j), kxx(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kxyTrue(i,j), kxy(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kyxTrue(i,j), kyx(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kyyTrue(i,j), kyy(i,j), small_tol, close_tol );
    }
  }
#endif

  ArrayQ fTrue = 0;
  ArrayQ gTrue = 0;
  ArrayQ hTrue = 0;
  pde.fluxViscous(x, y, z, time, q, qx, qy, qz, fTrue, gTrue, hTrue);

  MatrixQ dudq = 0;
  pde.jacobianMasterState(x, y, z, time, q, dudq);

  ArrayQ f = -kxx*dudq*qx - kxy*dudq*qy - kxz*dudq*qz;
  ArrayQ g = -kyx*dudq*qx - kyy*dudq*qy - kyz*dudq*qz;
  ArrayQ h = -kzx*dudq*qx - kzy*dudq*qy - kzz*dudq*qz;

  for (int i = 0; i < 6; i++)
  {
    SANS_CHECK_CLOSE( fTrue[i], f[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( gTrue[i], g[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( hTrue[i], h[i], small_tol, close_tol );
  }


  // case 2: zero nt

  nt = 0;

  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  kxx = 0, kxy = 0, kxz = 0, kyx = 0, kyy = 0, kyz = 0, kzx = 0, kzy = 0, kzz = 0;
  pde.diffusionViscous( x, y, z, time,
                        q, qx, qy, qz,
                        kxx, kxy, kxz,
                        kyx, kyy, kyz,
                        kzx, kzy, kzz );

#if 0   // not fully implemented; matrices from 2D
  MatrixQ kxxTrue =
    {{0, 0, 0, 0, 0, 0},
     {-7.590748299319728, 1.088435374149660, 0, 0, 0, 0},
     {2.637551020408163, 0, 0.8163265306122449, 0, 0, 0},
     {0, 0, 0, 0, 0, 0},
     {-26.00660201814059, -3.479092970521542, 2.491020408163265, 1.587301587301587, 0, 0},
     {0, 0, 0, 0, 0, 0.9995835068721366*rho}};

  MatrixQ kxyTrue =
    {{0, 0, 0, 0, 0, 0},
     {-1.758367346938776, 0, -0.5442176870748299, 0, 0, 0},
     {-5.693061224489796, 0.8163265306122449, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0},
     {6.131426938775510, -2.637551020408163, -3.795374149659864, 0, 0, 0},
     {0, 0, 0, 0, 0, 0}};

  MatrixQ kyxTrue =
    {{0, 0, 0, 0, 0, 0},{2.637551020408163, 0, 0.8163265306122449, 0, 0, 0},
     {3.795374149659864, -0.5442176870748299, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0},
     {6.131426938775510, 1.758367346938776, 5.693061224489796, 0, 0, 0},
     {0, 0, 0, 0, 0, 0}};

  MatrixQ kyyTrue =
    {{0, 0, 0, 0, 0, 0},
     {-5.693061224489796, 0.8163265306122449, 0, 0, 0, 0},
     {3.516734693877551, 0, 1.088435374149660, 0, 0, 0},
     {0, 0, 0, 0, 0, 0},
     {-15.61277480725624, -5.376780045351474, 1.611836734693878, 1.587301587301587, 0, 0},
     {0, 0, 0, 0, 0, 0.9995835068721366*rho}};

  for (int i = 0; i < 6; i++)
  {
    for (int j = 0; j < 6; j++)
    {
      //printf( "i = %d  j = %d\n", i, j );
      SANS_CHECK_CLOSE( kxxTrue(i,j), kxx(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kxyTrue(i,j), kxy(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kyxTrue(i,j), kyx(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kyyTrue(i,j), kyy(i,j), small_tol, close_tol );
    }
  }
#endif

  fTrue = 0;
  gTrue = 0;
  hTrue = 0;
  pde.fluxViscous(x, y, z, time, q, qx, qy, qz, fTrue, gTrue, hTrue);

  dudq = 0;
  pde.jacobianMasterState(x, y, z, time, q, dudq);

  f = -kxx*dudq*qx - kxy*dudq*qy - kxz*dudq*qz;
  g = -kyx*dudq*qx - kyy*dudq*qy - kyz*dudq*qz;
  h = -kzx*dudq*qx - kzy*dudq*qy - kzz*dudq*qz;

  for (int i = 0; i < 6; i++)
  {
    SANS_CHECK_CLOSE( fTrue[i], f[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( gTrue[i], g[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( hTrue[i], h[i], small_tol, close_tol );
  }


  // case 3: nt < 0 (negative SA)

  chi = -5.241;
  nt  = chi*(muRef/rho);

  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  kxx = 0, kxy = 0, kxz = 0, kyx = 0, kyy = 0, kyz = 0, kzx = 0, kzy = 0, kzz = 0;
  pde.diffusionViscous( x, y, z, time,
                        q, qx, qy, qz,
                        kxx, kxy, kxz,
                        kyx, kyy, kyz,
                        kzx, kzy, kzz );

#if 0   // not fully implemented; matrices from 2D
  kxxTrue =
    {{0, 0, 0, 0, 0},
     {-7.590748299319728, 1.088435374149660, 0, 0, 0},
     {2.637551020408163, 0, 0.8163265306122449, 0, 0},
     {-26.00660201814059, -3.479092970521542, 2.491020408163265, 1.587301587301587, 0},
     {22.20633785154143*rho, 0, 0, 0, 5.190376620518652*rho}};

  kxyTrue =
    {{0, 0, 0, 0, 0},
     {-1.758367346938776, 0, -0.5442176870748299, 0, 0},
     {-5.693061224489796, 0.8163265306122449, 0, 0, 0},
     {6.131426938775510, -2.637551020408163, -3.795374149659864, 0, 0},
     {0, 0, 0, 0, 0}};

  kyxTrue =
    {{0, 0, 0, 0, 0},
     {2.637551020408163, 0, 0.8163265306122449, 0, 0},
     {3.795374149659864, -0.5442176870748299, 0, 0, 0},
     {6.131426938775510, 1.758367346938776, 5.693061224489796, 0, 0},
     {0, 0, 0, 0, 0}};

  kyyTrue =
    {{0, 0, 0, 0, 0},
     {-5.693061224489796, 0.8163265306122449, 0, 0, 0},
     {3.516734693877551, 0, 1.088435374149660, 0, 0},
     {-15.61277480725624, -5.376780045351474, 1.611836734693878, 1.587301587301587, 0},
     {22.20633785154143*rho, 0, 0, 0, 5.190376620518652*rho}};

  for (int i = 0; i < 6; i++)
  {
    for (int j = 0; j < 6; j++)
    {
      SANS_CHECK_CLOSE( kxxTrue(i,j), kxx(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kxyTrue(i,j), kxy(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kyxTrue(i,j), kyx(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kyyTrue(i,j), kyy(i,j), small_tol, close_tol );
    }
  }
#endif

  fTrue = 0;
  gTrue = 0;
  hTrue = 0;
  pde.fluxViscous(x, y, z, time, q, qx, qy, qz, fTrue, gTrue, hTrue);

  dudq = 0;
  pde.jacobianMasterState(x, y, z, time, q, dudq);

  f = -kxx*dudq*qx - kxy*dudq*qy - kxz*dudq*qz;
  g = -kyx*dudq*qx - kyy*dudq*qy - kyz*dudq*qz;
  h = -kzx*dudq*qx - kzy*dudq*qy - kzz*dudq*qz;

  for (int i = 0; i < 6; i++)
  {
    SANS_CHECK_CLOSE( fTrue[i], f[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( gTrue[i], g[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( hTrue[i], h[i], small_tol, close_tol );
  }
}
#endif


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( diffusionViscous_Surreal )
{

  // Use conservative variables here in order to differentiate with Surreal
  typedef QTypeConservative QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef SurrealS<PhysD3::D*PDEClass::N> SurrealClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;
  typedef PDEClass::ArrayQ<SurrealClass> ArrayQSurreal;

  bool verbose = false;

  const Real small_tol = 1.e-10;
  const Real close_tol = 1.e-10;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real tSuth = 110./300.; //somewhat random...
  const Real tRef  = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, z, time;
  Real rho, u, v, w, t;
  Real nt, chi;

  x = 0; y = 0; time = 0;   // not actually used in functions
  rho = 1.225;
  u = 6.974;
  v = -3.231;
  w = 10.23;
  t = 2.8815;

  Real e = gas.energy(rho, t);
  Real E = e + 0.5*(u*u + v*v + w*w);

  // gradient

  Real rhox = 0.0421, rhoy = 0.0127, rhoz = 0.691;
  Real ux =  0.0521, uy = -0.0124, uz = -0.145;
  Real vx = -0.0312, vy = -0.0022, vz =  0.089;
  Real wx = -0.0546, wy = -0.0735, wz =  0.032;
  Real tx = -0.0715, ty = -0.0111, tz =  0.567;

  Real rhoux = rhox*u + ux*rho;
  Real rhouy = rhoy*u + uy*rho;
  Real rhouz = rhoz*u + uz*rho;

  Real rhovx = rhox*v + vx*rho;
  Real rhovy = rhoy*v + vy*rho;
  Real rhovz = rhoz*v + vz*rho;

  Real rhowx = rhox*w + wx*rho;
  Real rhowy = rhoy*w + wy*rho;
  Real rhowz = rhoz*w + wz*rho;

  Real ex, ey, ez;
  gas.energyGradient(rho, t, rhox, tx, ex);
  gas.energyGradient(rho, t, rhoy, ty, ey);
  gas.energyGradient(rho, t, rhoz, tz, ez);

  Real Ex = ex + ux*u + vx*v + wx*w;
  Real Ey = ey + uy*u + vy*v + wy*w;
  Real Ez = ez + uz*u + vz*v + wz*w;

  Real rhoEx = rhox*E + rho*Ex;
  Real rhoEy = rhoy*E + rho*Ey;
  Real rhoEz = rhoz*E + rho*Ez;

  Real chix = 1.2, chiy = -0.3, chiz = 0.2;

  Real ntx = chix*(muRef/rho);
  Real nty = chiy*(muRef/rho);
  Real ntz = chiz*(muRef/rho);

  ArrayQ q, qx, qy, qz;
  ArrayQSurreal qxS, qyS, qzS;
  MatrixQ kxx = 0, kxy = 0, kxz = 0,
          kyx = 0, kyy = 0, kyz = 0,
          kzx = 0, kzy = 0, kzz = 0;
  MatrixQ dudq;
  ArrayQ f, g, h;
  ArrayQSurreal fTrue, gTrue, hTrue;

  ArrayQ dqx = 0, dqy = 0, dqz = 0;
  ArrayQ dux = 0, duy = 0, duz = 0;
  ArrayQ dF = 0, dG = 0, dH = 0;
  ArrayQ dF2 = 0, dG2 = 0, dH2 = 0;
  ArrayQ dFTrue = 0, dGTrue = 0, dHTrue = 0;
  dqx = {0.01, -0.01, 0.02, -0.02, 0.023, 0.03};
  dqy = {0.005, -0.005, 0.025, -0.025, -0.023, -0.017};
  dqz = {-0.0025, -0.0035, 0.0245, -0.0025, -0.0123, -0.0117};

  // set the complete state vectors (sans nt)
  // the nt values are set for each case
  pde.setDOFFrom( q, SAnt3D<Conservative3D<Real>>({rho , rho*u, rho*v, rho*w, rho*E, 0}));

  qxS = qx = {rhox, rhoux, rhovx, rhowx, rhoEx, 0};
  qyS = qy = {rhoy, rhouy, rhovy, rhowy, rhoEy, 0};
  qzS = qz = {rhoz, rhouz, rhovz, rhowz, rhoEz, 0};

  // set derivatives
  for (int i = 0; i < pde.N; i++)
  {
    qxS[i].deriv(i        ) = 1;
    qyS[i].deriv(i+  pde.N) = 1;
    qzS[i].deriv(i+2*pde.N) = 1;
  }

  // case 1: positive nt
  if (verbose) cout << "diffusionViscous: case1" << endl;

  chi  = 5.241;

  nt  = chi*(muRef/rho);

  q[5]  = rho*nt;

  qxS[5].value() = qx[5] = rhox*nt + rho*ntx;
  qyS[5].value() = qy[5] = rhoy*nt + rho*nty;
  qzS[5].value() = qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  fTrue = gTrue = hTrue = 0;
  pde.fluxViscous(x, y, z, time, q, qxS, qyS, qzS, fTrue, gTrue, hTrue);

  // Compute the hand coded diffusion matrix
  kxx = kxy = kxz = kyx = kyy = kyz = kzx = kzy = kzz = 0;
  pde.diffusionViscous( x, y, z, time,
                        q, qx, qy, qz,
                        kxx, kxy, kxz,
                        kyx, kyy, kyz,
                        kzx, kzy, kzz );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " -f(" << i << ").deriv(" << j         << ") = " << -fTrue(i).deriv(j        ) << " | " << kxx(i,j) << std::endl;
      SANS_CHECK_CLOSE( -fTrue(i).deriv(j        ), kxx(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -f(" << i << ").deriv(" << pde.N+j   << ") = " << -fTrue(i).deriv(pde.N+j  ) << " | " << kxy(i,j) << std::endl;
      SANS_CHECK_CLOSE( -fTrue(i).deriv(pde.N+j  ), kxy(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -f(" << i << ").deriv(" << 2*pde.N+j << ") = " << -fTrue(i).deriv(2*pde.N+j) << " | " << kxz(i,j) << std::endl;
      SANS_CHECK_CLOSE( -fTrue(i).deriv(2*pde.N+j), kxz(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -g(" << i << ").deriv(" << j         << ") = " << -gTrue(i).deriv(j        ) << " | " << kyx(i,j) << std::endl;
      SANS_CHECK_CLOSE( -gTrue(i).deriv(j        ), kyx(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -g(" << i << ").deriv(" << pde.N+j   << ") = " << -gTrue(i).deriv(pde.N+j  ) << " | " << kyy(i,j) << std::endl;
      SANS_CHECK_CLOSE( -gTrue(i).deriv(pde.N+j  ), kyy(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -g(" << i << ").deriv(" << 2*pde.N+j << ") = " << -gTrue(i).deriv(2*pde.N+j) << " | " << kyz(i,j) << std::endl;
      SANS_CHECK_CLOSE( -gTrue(i).deriv(2*pde.N+j), kyz(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -h(" << i << ").deriv(" << j         << ") = " << -hTrue(i).deriv(j        ) << " | " << kzx(i,j) << std::endl;
      SANS_CHECK_CLOSE( -hTrue(i).deriv(j        ), kzx(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -h(" << i << ").deriv(" << pde.N+j   << ") = " << -hTrue(i).deriv(pde.N+j  ) << " | " << kzy(i,j) << std::endl;
      SANS_CHECK_CLOSE( -hTrue(i).deriv(pde.N+j  ), kzy(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -h(" << i << ").deriv(" << 2*pde.N+j << ") = " << -hTrue(i).deriv(2*pde.N+j) << " | " << kzz(i,j) << std::endl;
      SANS_CHECK_CLOSE( -hTrue(i).deriv(2*pde.N+j), kzz(i,j), small_tol, close_tol );
    }

  dudq = 0;
  pde.jacobianMasterState(x, y, z, time, q, dudq);

  f = -kxx*dudq*qx - kxy*dudq*qy - kxz*dudq*qz;
  g = -kyx*dudq*qx - kyy*dudq*qy - kyz*dudq*qz;
  h = -kzx*dudq*qx - kzy*dudq*qy - kzz*dudq*qz;

  for (int i = 0; i < pde.N; i++)
  {
    SANS_CHECK_CLOSE( fTrue[i].value(), f[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( gTrue[i].value(), g[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( hTrue[i].value(), h[i], small_tol, close_tol );
  }

  dF = 0; dG = 0; dH = 0;
  pde.perturbedGradFluxViscous(x, y, z, time, q, qx, qy, qz, dqx, dqy, dqz, dF, dG, dH);

  dFTrue = -kxx*dudq*dqx - kxy*dudq*dqy - kxz*dudq*dqz;
  dGTrue = -kyx*dudq*dqx - kyy*dudq*dqy - kyz*dudq*dqz;
  dHTrue = -kzx*dudq*dqx - kzy*dudq*dqy - kzz*dudq*dqz;

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( dFTrue[i], dF[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dGTrue[i], dG[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dHTrue[i], dH[i], small_tol, close_tol );
  }

  dux = dudq*dqx;
  duy = dudq*dqy;
  duz = dudq*dqz;

  dF2 = 0; dG2 = 0; dH2 = 0;
  pde.perturbedGradFluxViscous(x, y, z, time,
                               kxx, kxy, kxz,
                               kyx, kyy, kyz,
                               kzx, kzy, kzz,
                               dux, duy, duz,
                               dF2, dG2, dH2);

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( dFTrue[i], dF2[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dGTrue[i], dG2[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dHTrue[i], dH2[i], small_tol, close_tol );
  }

  // case 2: negative nt
  if (verbose) cout << "diffusionViscous: case2" << endl;

  chi  = -5.241;

  nt  = chi*(muRef/rho);

  q[5]  = rho*nt;

  qxS[5].value() = qx[5] = rhox*nt + rho*ntx;
  qyS[5].value() = qy[5] = rhoy*nt + rho*nty;
  qzS[5].value() = qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  fTrue = gTrue = hTrue = 0;
  pde.fluxViscous(x, y, z, time, q, qxS, qyS, qzS, fTrue, gTrue, hTrue);

  // Compute the hand coded diffusion matrix
  kxx = kxy = kxz = kyx = kyy = kyz = kzx = kzy = kzz = 0;
  pde.diffusionViscous( x, y, z, time,
                        q, qx, qy, qz,
                        kxx, kxy, kxz,
                        kyx, kyy, kyz,
                        kzx, kzy, kzz );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " -f(" << i << ").deriv(" << j         << ") = " << -fTrue(i).deriv(j        ) << " | " << kxx(i,j) << std::endl;
      SANS_CHECK_CLOSE( -fTrue(i).deriv(j        ), kxx(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -f(" << i << ").deriv(" << pde.N+j   << ") = " << -fTrue(i).deriv(pde.N+j  ) << " | " << kxy(i,j) << std::endl;
      SANS_CHECK_CLOSE( -fTrue(i).deriv(pde.N+j  ), kxy(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -f(" << i << ").deriv(" << 2*pde.N+j << ") = " << -fTrue(i).deriv(2*pde.N+j) << " | " << kxz(i,j) << std::endl;
      SANS_CHECK_CLOSE( -fTrue(i).deriv(2*pde.N+j), kxz(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -g(" << i << ").deriv(" << j         << ") = " << -gTrue(i).deriv(j        ) << " | " << kyx(i,j) << std::endl;
      SANS_CHECK_CLOSE( -gTrue(i).deriv(j        ), kyx(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -g(" << i << ").deriv(" << pde.N+j   << ") = " << -gTrue(i).deriv(pde.N+j  ) << " | " << kyy(i,j) << std::endl;
      SANS_CHECK_CLOSE( -gTrue(i).deriv(pde.N+j  ), kyy(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -g(" << i << ").deriv(" << 2*pde.N+j << ") = " << -gTrue(i).deriv(2*pde.N+j) << " | " << kyz(i,j) << std::endl;
      SANS_CHECK_CLOSE( -gTrue(i).deriv(2*pde.N+j), kyz(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -h(" << i << ").deriv(" << j         << ") = " << -hTrue(i).deriv(j        ) << " | " << kzx(i,j) << std::endl;
      SANS_CHECK_CLOSE( -hTrue(i).deriv(j        ), kzx(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -h(" << i << ").deriv(" << pde.N+j   << ") = " << -hTrue(i).deriv(pde.N+j  ) << " | " << kzy(i,j) << std::endl;
      SANS_CHECK_CLOSE( -hTrue(i).deriv(pde.N+j  ), kzy(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -h(" << i << ").deriv(" << 2*pde.N+j << ") = " << -hTrue(i).deriv(2*pde.N+j) << " | " << kzz(i,j) << std::endl;
      SANS_CHECK_CLOSE( -hTrue(i).deriv(2*pde.N+j), kzz(i,j), small_tol, close_tol );
    }

  dudq = 0;
  pde.jacobianMasterState(x, y, z, time, q, dudq);

  f = -kxx*dudq*qx - kxy*dudq*qy - kxz*dudq*qz;
  g = -kyx*dudq*qx - kyy*dudq*qy - kyz*dudq*qz;
  h = -kzx*dudq*qx - kzy*dudq*qy - kzz*dudq*qz;

  for (int i = 0; i < pde.N; i++)
  {
    SANS_CHECK_CLOSE( fTrue[i].value(), f[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( gTrue[i].value(), g[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( hTrue[i].value(), h[i], small_tol, close_tol );
  }

  dF = 0; dG = 0; dH = 0;
  pde.perturbedGradFluxViscous(x, y, z, time, q, qx, qy, qz, dqx, dqy, dqz, dF, dG, dH);

  dFTrue = -kxx*dudq*dqx - kxy*dudq*dqy - kxz*dudq*dqz;
  dGTrue = -kyx*dudq*dqx - kyy*dudq*dqy - kyz*dudq*dqz;
  dHTrue = -kzx*dudq*dqx - kzy*dudq*dqy - kzz*dudq*dqz;

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( dFTrue[i], dF[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dGTrue[i], dG[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dHTrue[i], dH[i], small_tol, close_tol );
  }

  dux = dudq*dqx;
  duy = dudq*dqy;
  duz = dudq*dqz;

  dF2 = 0; dG2 = 0; dH2 = 0;
  pde.perturbedGradFluxViscous(x, y, z, time,
                               kxx, kxy, kxz,
                               kyx, kyy, kyz,
                               kzx, kzy, kzz,
                               dux, duy, duz,
                               dF2, dG2, dH2);

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( dFTrue[i], dF2[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dGTrue[i], dG2[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dHTrue[i], dH2[i], small_tol, close_tol );
  }

  // case 3: positive nt with zero gradient
  if (verbose) cout << "diffusionViscous: case3" << endl;

  chi  = 5.241;

  nt  = chi*(muRef/rho);

  q[5]  = rho*nt;

  for (int i = 0; i < pde.N; i++)
  {
    qxS[i].value() = qx[i] = 0;
    qyS[i].value() = qy[i] = 0;
    qzS[i].value() = qz[i] = 0;
  }

  // Compute the true jacobian with Surreals
  fTrue = gTrue = hTrue = 0;
  pde.fluxViscous(x, y, z, time, q, qxS, qyS, qzS, fTrue, gTrue, hTrue);

  // Compute the hand coded diffusion matrix
  kxx = kxy = kxz = kyx = kyy = kyz = kzx = kzy = kzz = 0;
  pde.diffusionViscous( x, y, z, time,
                        q, qx, qy, qz,
                        kxx, kxy, kxz,
                        kyx, kyy, kyz,
                        kzx, kzy, kzz );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " -f(" << i << ").deriv(" << j         << ") = " << -fTrue(i).deriv(j        ) << " | " << kxx(i,j) << std::endl;
      SANS_CHECK_CLOSE( -fTrue(i).deriv(j        ), kxx(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -f(" << i << ").deriv(" << pde.N+j   << ") = " << -fTrue(i).deriv(pde.N+j  ) << " | " << kxy(i,j) << std::endl;
      SANS_CHECK_CLOSE( -fTrue(i).deriv(pde.N+j  ), kxy(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -f(" << i << ").deriv(" << 2*pde.N+j << ") = " << -fTrue(i).deriv(2*pde.N+j) << " | " << kxz(i,j) << std::endl;
      SANS_CHECK_CLOSE( -fTrue(i).deriv(2*pde.N+j), kxz(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -g(" << i << ").deriv(" << j         << ") = " << -gTrue(i).deriv(j        ) << " | " << kyx(i,j) << std::endl;
      SANS_CHECK_CLOSE( -gTrue(i).deriv(j        ), kyx(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -g(" << i << ").deriv(" << pde.N+j   << ") = " << -gTrue(i).deriv(pde.N+j  ) << " | " << kyy(i,j) << std::endl;
      SANS_CHECK_CLOSE( -gTrue(i).deriv(pde.N+j  ), kyy(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -g(" << i << ").deriv(" << 2*pde.N+j << ") = " << -gTrue(i).deriv(2*pde.N+j) << " | " << kyz(i,j) << std::endl;
      SANS_CHECK_CLOSE( -gTrue(i).deriv(2*pde.N+j), kyz(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -h(" << i << ").deriv(" << j         << ") = " << -hTrue(i).deriv(j        ) << " | " << kzx(i,j) << std::endl;
      SANS_CHECK_CLOSE( -hTrue(i).deriv(j        ), kzx(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -h(" << i << ").deriv(" << pde.N+j   << ") = " << -hTrue(i).deriv(pde.N+j  ) << " | " << kzy(i,j) << std::endl;
      SANS_CHECK_CLOSE( -hTrue(i).deriv(pde.N+j  ), kzy(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -h(" << i << ").deriv(" << 2*pde.N+j << ") = " << -hTrue(i).deriv(2*pde.N+j) << " | " << kzz(i,j) << std::endl;
      SANS_CHECK_CLOSE( -hTrue(i).deriv(2*pde.N+j), kzz(i,j), small_tol, close_tol );
    }


  dudq = 0;
  pde.jacobianMasterState(x, y, z, time, q, dudq);

  f = -kxx*dudq*qx - kxy*dudq*qy - kxz*dudq*qz;
  g = -kyx*dudq*qx - kyy*dudq*qy - kyz*dudq*qz;
  h = -kzx*dudq*qx - kzy*dudq*qy - kzz*dudq*qz;

  for (int i = 0; i < pde.N; i++)
  {
    SANS_CHECK_CLOSE( fTrue[i].value(), f[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( gTrue[i].value(), g[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( hTrue[i].value(), h[i], small_tol, close_tol );
  }

  dF = 0; dG = 0; dH = 0;
  pde.perturbedGradFluxViscous(x, y, z, time, q, qx, qy, qz, dqx, dqy, dqz, dF, dG, dH);

  dFTrue = -kxx*dudq*dqx - kxy*dudq*dqy - kxz*dudq*dqz;
  dGTrue = -kyx*dudq*dqx - kyy*dudq*dqy - kyz*dudq*dqz;
  dHTrue = -kzx*dudq*dqx - kzy*dudq*dqy - kzz*dudq*dqz;

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( dFTrue[i], dF[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dGTrue[i], dG[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dHTrue[i], dH[i], small_tol, close_tol );
  }

  dux = dudq*dqx;
  duy = dudq*dqy;
  duz = dudq*dqz;

  dF2 = 0; dG2 = 0; dH2 = 0;
  pde.perturbedGradFluxViscous(x, y, z, time,
                               kxx, kxy, kxz,
                               kyx, kyy, kyz,
                               kzx, kzy, kzz,
                               dux, duy, duz,
                               dF2, dG2, dH2);

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( dFTrue[i], dF2[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dGTrue[i], dG2[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dHTrue[i], dH2[i], small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
template<class PDEClass, class ArrayQ>
void diffusionViscousGradient_tester(const PDEClass& pde, const ArrayQ& q)
{
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  // not used
  Real x, y, z, time;
  Real dist;

  const GasModel& gas = pde.gasModel();
  Real R = gas.R();
  Real Cv = gas.Cv();

  Real rho, u, v, w, t, nt;

  pde.variableInterpreter().eval( q, rho, u, v, w, t );
  pde.variableInterpreter().evalSA( q, nt );

  Real p = gas.pressure(rho, t);

  Real e = gas.energy(rho, t);
  Real E = e + 0.5*(u*u + v*v + w*w);

  Real rhox = 0.0421, rhoy = 0.0127, rhoz = 0.691;
  Real ux =  0.0521, uy = -0.0124, uz = -0.145;
  Real vx = -0.0312, vy = -0.0022, vz =  0.089;
  Real wx = -0.0546, wy = -0.0735, wz =  0.032;
  Real px =  0.0120, py = -3.2300, pz =  0.052;
  Real ntx =  0.023, nty = 0.1250, ntz = 0.953;

  Real rhoxx = -0.253, uxx =  1.02, vxx =  0.99, wxx =  0.99, pxx =  2.99, ntxx =  0.345;
  Real rhoxy =  0.782, uxy = -0.95, vxy = -0.92, wxy =  0.82, pxy = -1.92, ntxy =  1.580;
  Real rhoyy =  1.080, uyy = -0.42, vyy = -0.44, wyy = -0.42, pyy = -1.44, ntyy = -0.152;
  Real rhoxz =  0.172, uxz = -0.95, vxz =  0.23, wxz = -0.92, pxz = -1.82, ntxz =  0.026;
  Real rhoyz =  0.602, uyz =  0.87, vyz = -0.35, wyz =  0.78, pyz =  1.04, ntyz =  0.145;
  Real rhozz =  1.267, uzz = -0.32, vzz =  0.65, wzz =  0.14, pzz =  1.52, ntzz = -1.053;

  ArrayQ qx = {rhox, ux, vx, wx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, wy, py, nty};
  ArrayQ qz = {rhoz, uz, vz, wz, pz, ntz};

  // t = p/(rho*R)
  Real tx = px/(rho*R) - p/(rho*rho*R)*rhox;
  Real ty = py/(rho*R) - p/(rho*rho*R)*rhoy;
  Real tz = pz/(rho*R) - p/(rho*rho*R)*rhoz;

  Real ex = Cv*tx;
  Real ey = Cv*ty;
  Real ez = Cv*tz;

  Real Ex = ex + u*ux + v*vx + w*wx;
  Real Ey = ey + u*uy + v*vy + w*wy;
  Real Ez = ez + u*uz + v*vz + w*wz;

  ArrayQ qxx = {rhoxx, uxx, vxx, wxx, pxx, ntxx};
  ArrayQ qxy = {rhoxy, uxy, vxy, wxy, pxy, ntxy};
  ArrayQ qyy = {rhoyy, uyy, vyy, wyy, pyy, ntyy};
  ArrayQ qxz = {rhoxz, uxz, vxz, wxz, pxz, ntxz};
  ArrayQ qyz = {rhoyz, uyz, vyz, wyz, pyz, ntyz};
  ArrayQ qzz = {rhozz, uzz, vzz, wzz, pzz, ntzz};

  Real rho2 = rho*rho;
  Real rho3 = rho*rho*rho;

  Real txx = pxx/(rho*R) - px/(rho2*R)*rhox - px/(rho2*R)*rhox + 2*p/(rho3*R)*rhox*rhox - p/(rho2*R)*rhoxx;
  Real txy = pxy/(rho*R) - py/(rho2*R)*rhox - px/(rho2*R)*rhoy + 2*p/(rho3*R)*rhox*rhoy - p/(rho2*R)*rhoxy;
  Real tyy = pyy/(rho*R) - py/(rho2*R)*rhoy - py/(rho2*R)*rhoy + 2*p/(rho3*R)*rhoy*rhoy - p/(rho2*R)*rhoyy;
  Real txz = pxz/(rho*R) - px/(rho2*R)*rhoz - pz/(rho2*R)*rhox + 2*p/(rho3*R)*rhox*rhoz - p/(rho2*R)*rhoxz;
  Real tyz = pyz/(rho*R) - py/(rho2*R)*rhoz - pz/(rho2*R)*rhoy + 2*p/(rho3*R)*rhoy*rhoz - p/(rho2*R)*rhoyz;
  Real tzz = pzz/(rho*R) - pz/(rho2*R)*rhoz - pz/(rho2*R)*rhoz + 2*p/(rho3*R)*rhoz*rhoz - p/(rho2*R)*rhozz;

  Real exx = Cv*txx;
  Real exy = Cv*txy;
  Real eyy = Cv*tyy;
  Real exz = Cv*txz;
  Real eyz = Cv*tyz;
  Real ezz = Cv*tzz;

  Real Exx = exx + ux*ux + u*uxx + vx*vx + v*vxx + wx*wx + w*wxx;
  Real Exy = exy + ux*uy + u*uxy + vx*vy + v*vxy + wx*wy + w*wxy;
  Real Eyy = eyy + uy*uy + u*uyy + vy*vy + v*vyy + wy*wy + w*wyy;
  Real Exz = exz + ux*uz + u*uxz + vx*vz + v*vxz + wx*wz + w*wxz;
  Real Eyz = eyz + uy*uz + u*uyz + vy*vz + v*vyz + wy*wz + w*wyz;
  Real Ezz = ezz + uz*uz + u*uzz + vz*vz + v*vzz + wz*wz + w*wzz;

  ArrayQ Ux=0, Uy=0, Uz=0;
  pde.strongFluxAdvectiveTime(x, y, z, time, q, qx, Ux);
  pde.strongFluxAdvectiveTime(x, y, z, time, q, qy, Uy);
  pde.strongFluxAdvectiveTime(x, y, z, time, q, qz, Uz);

  ArrayQ Uxx = {rhoxx,
                rhoxx*u + rhox*ux + rhox*ux + rho*uxx,
                rhoxx*v + rhox*vx + rhox*vx + rho*vxx,
                rhoxx*w + rhox*wx + rhox*wx + rho*wxx,
                rhoxx*E + rhox*Ex + rhox*Ex + rho*Exx,
                rhoxx*nt + rhox*ntx + rhox*ntx + rho*ntxx};

  ArrayQ Uxy = {rhoxy,
                rhoxy*u + rhoy*ux + rhox*uy + rho*uxy,
                rhoxy*v + rhoy*vx + rhox*vy + rho*vxy,
                rhoxy*w + rhoy*wx + rhox*wy + rho*wxy,
                rhoxy*E + rhoy*Ex + rhox*Ey + rho*Exy,
                rhoxy*nt + rhoy*ntx + rhox*nty + rho*ntxy};

  ArrayQ Uyy = {rhoyy,
                rhoyy*u + rhoy*uy + rhoy*uy + rho*uyy,
                rhoyy*v + rhoy*vy + rhoy*vy + rho*vyy,
                rhoyy*w + rhoy*wy + rhoy*wy + rho*wyy,
                rhoyy*E + rhoy*Ey + rhoy*Ey + rho*Eyy,
                rhoyy*nt + rhoy*nty + rhoy*nty + rho*ntyy};

  ArrayQ Uxz = {rhoxz,
                rhoxz*u + rhox*uz + rhoz*ux + rho*uxz,
                rhoxz*v + rhox*vz + rhoz*vx + rho*vxz,
                rhoxz*w + rhox*wz + rhoz*wx + rho*wxz,
                rhoxz*E + rhox*Ez + rhoz*Ex + rho*Exz,
                rhoxz*nt + rhox*ntz + rhoz*ntx + rho*ntxz};

  ArrayQ Uyz = {rhoyz,
                rhoyz*u + rhoy*uz + rhoz*uy + rho*uyz,
                rhoyz*v + rhoy*vz + rhoz*vy + rho*vyz,
                rhoyz*w + rhoy*wz + rhoz*wy + rho*wyz,
                rhoyz*E + rhoy*Ez + rhoz*Ey + rho*Eyz,
                rhoyz*nt + rhoy*ntz + rhoz*nty + rho*ntyz};

  ArrayQ Uzz = {rhozz,
                rhozz*u + rhoz*uz + rhoz*uz + rho*uzz,
                rhozz*v + rhoz*vz + rhoz*vz + rho*vzz,
                rhozz*w + rhoz*wz + rhoz*wz + rho*wzz,
                rhozz*E + rhoz*Ez + rhoz*Ez + rho*Ezz,
                rhozz*nt + rhoz*ntz + rhoz*ntz + rho*ntzz};

  MatrixQ kxx = 0, kxy = 0, kxz = 0;
  MatrixQ kyx = 0, kyy = 0, kyz = 0;
  MatrixQ kzx = 0, kzy = 0, kzz = 0;

  MatrixQ kxx_x = 0, kxy_x = 0, kxz_x = 0, kyx_x = 0, kzx_x = 0;
  MatrixQ kxy_y = 0, kyx_y = 0, kyy_y = 0, kyz_y = 0, kzy_y = 0;
  MatrixQ kxz_z = 0, kyz_z = 0, kzx_z = 0, kzy_z = 0, kzz_z = 0;

  // d(kxx*Ux + kxy*Uy + kxz*Uz)/dx = kxx_x*Ux + kxy_x*Uy + kxz_x*Uz + kxx*Uxx + kxy*Uxy + kxz*Uxz
  // d(kyx*Ux + kyy*Uy + kyz*Uz)/dy = kyx_y*Ux + kyy_y*Uy + kyz_y*Uz + kyx*Uxy + kyy*Uyy + kyz*Uyz
  // d(kzx*Ux + kzy*Uy + kzz*Uz)/dz = kzx_z*Ux + kzy_z*Uy + kzz_z*Uz + kzx*Uxz + kzy*Uyz + kzz*Uzz

  ArrayQ strongViscTrue, strongVisc;

  strongViscTrue = 0;
  pde.strongFluxViscous(dist, x, y, z, time, q, qx, qy, qz,
                        qxx,
                        qxy, qyy,
                        qxz, qyz, qzz, strongViscTrue);

  kxx = 0, kxy = 0, kxz = 0;
  kyx = 0, kyy = 0, kyz = 0;
  kzx = 0, kzy = 0, kzz = 0;
  pde.diffusionViscous( x, y, z, time, q, qx, qy, qz,
                        kxx, kxy, kxz,
                        kyx, kyy, kyz,
                        kzx, kzy, kzz );

  kxx_x = 0, kxy_x = 0, kxz_x = 0;
  kyx_y = 0, kyy_y = 0, kyz_y = 0;
  kzx_z = 0, kzy_z = 0, kzz_z = 0;
#ifdef SANS_SA_QCR
  BOOST_CHECK_THROW(
    pde.diffusionViscousGradient( dist, x, y, z, time, q, qx, qy, qz,
                                  qxx,
                                  qxy, qyy,
                                  qxz, qyz, qzz,
                                  kxx_x, kxy_x, kxz_x, kyx_x, kzx_x,
                                  kxy_y, kyx_y, kyy_y, kyz_y, kzy_y,
                                  kxz_z, kyz_z, kzx_z, kzy_z, kzz_z ),
    DeveloperException );
  return;
#else
  pde.diffusionViscousGradient( dist, x, y, z, time, q, qx, qy, qz,
                                qxx,
                                qxy, qyy,
                                qxz, qyz, qzz,
                                kxx_x, kxy_x, kxz_x, kyx_x, kzx_x,
                                kxy_y, kyx_y, kyy_y, kyz_y, kzy_y,
                                kxz_z, kyz_z, kzx_z, kzy_z, kzz_z );
#endif


  strongVisc = -(kxx_x*Ux + kxy_x*Uy + kxz_x*Uz + kxx*Uxx + kxy*Uxy + kxz*Uxz
             +   kyx_y*Ux + kyy_y*Uy + kyz_y*Uz + kyx*Uxy + kyy*Uyy + kyz*Uyz
             +   kzx_z*Ux + kzy_z*Uy + kzz_z*Uz + kzx*Uxz + kzy*Uyz + kzz*Uzz);

//  std::cout << "strongViscTrue = " << strongViscTrue << std::endl;
//  std::cout << "strongVisc     = " << strongVisc << std::endl;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-10;

  SANS_CHECK_CLOSE(strongViscTrue(0), strongVisc(0), small_tol, close_tol);
  SANS_CHECK_CLOSE(strongViscTrue(1), strongVisc(1), small_tol, close_tol);
  SANS_CHECK_CLOSE(strongViscTrue(2), strongVisc(2), small_tol, close_tol);
  SANS_CHECK_CLOSE(strongViscTrue(3), strongVisc(3), small_tol, close_tol);
  SANS_CHECK_CLOSE(strongViscTrue(4), strongVisc(4), small_tol, close_tol);
  SANS_CHECK_CLOSE(strongViscTrue(5), strongVisc(5), small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( diffusionViscousGradient_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // function tests

  Real rho, u, v, w, t;
  Real chi, nt;

  rho = 1.225;                        // kg/m^3
  u = 6.974; v = -3.231; w = 69.784;  // m/s
  t = 288.15;                         // K

  // case 1: nt > 0

  chi = 5.241;
  nt  = chi*(muRef/rho);

  ArrayQ q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  diffusionViscousGradient_tester(pde, q);


  // case 2: zero nt

  nt = 0;

  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  diffusionViscousGradient_tester(pde, q);


  // case 3: nt < 0 (negative SA)

  chi = -5.241;
  nt  = chi*(muRef/rho);

  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  diffusionViscousGradient_tester(pde, q);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( source_xy )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  bool verbose = false;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, z, time;
  Real rho, u, v, w, t;

  ArrayQ q, qx, qy, qz;
  ArrayQ qp = 0, qpx = 0, qpy = 0, qpz = 0;
  Real srcTrue;
  ArrayQ src, src_split;

  x = 0; y = 0; z = 0; time = 0;   // not actually used in functions
  rho = 1.225;
  u = 6.974;
  v = -3.231;
  w = 0;
  t = 2.8815;

  Real rhox = 0.0421, ux =  0.0521, vx = -0.0312, wx = 0, tx = -0.0715;
  Real rhoy = 0.0127, uy = -0.0124, vy = -0.0022, wy = 0, ty = -0.0111;
  Real rhoz = 0,      uz =  0     , vz =  0     , wz = 0, tz =  0     ;
  Real px, py, pz;

  px = R*(rho*tx + rhox*t);
  py = R*(rho*ty + rhoy*t);
  pz = R*(rho*tz + rhoz*t);

  Real nt, ntx, nty, ntz;
  Real chi, chix, chiy, chiz;
  Real dist;

  // case 1: prod/wall dominant; S~ branch 1
  if (verbose) cout << "source: case1" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0; chiz = 0;
  dist = 60;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  qx = {rhox, ux, vx, wx, px, ntx};
  qy = {rhoy, uy, vy, wy, py, nty};
  qz = {rhoz, uz, vz, wz, pz, ntz};

  srcTrue = 0.0015950811868193507;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, z, time, q, qx, qy, qz, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_SMALL( src(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(5), close_tol*10 );

  pde.source( dist, x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_SMALL( src_split(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(5), close_tol*10 );

  // case 2: prod/wall dominant; S~ branch 2
  if (verbose) cout << "source: case2" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0; chiz = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  qx = {rhox, ux, vx, wx, px, ntx};
  qy = {rhoy, uy, vy, wy, py, nty};
  qz = {rhoz, uz, vz, wz, pz, ntz};

  srcTrue = 0.08843084458519657;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, z, time, q, qx, qy, qz, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_SMALL( src(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(5), close_tol );

  pde.source( dist, x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_SMALL( src_split(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(5), close_tol*10 );

  // case 3: prod/wall ft2 terms dominant
  if (verbose) cout << "source: case3" << endl;

  chi  = 1.241;
  chix = 0; chiy = 0; chiz = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  qx = {rhox, ux, vx, wx, px, ntx};
  qy = {rhoy, uy, vy, wy, py, nty};
  qz = {rhoz, uz, vz, wz, pz, ntz};

  srcTrue = -0.001315819430510617;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, z, time, q, qx, qy, qz, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_SMALL( src(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(5), close_tol );

  pde.source( dist, x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_SMALL( src_split(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(5), close_tol*10 );

  // case 4: nonconservative diffusion dominant
  if (verbose) cout << "source: case4" << endl;

  chi  = 5.241;
  chix = 1.963; chiy = -0.781; chiz = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  qx = {rhox, ux, vx, wx, px, ntx};
  qy = {rhoy, uy, vy, wy, py, nty};
  qz = {rhoz, uz, vz, wz, pz, ntz};

  srcTrue = -2.857308070100351;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, z, time, q, qx, qy, qz, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_SMALL( src(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(5), close_tol );

  pde.source( dist, x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_SMALL( src_split(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(5), close_tol*10 );

  // case 5: negative SA; prod/wall dominant
  if (verbose) cout << "source: case5" << endl;

  chi  = -5.241;
  chix = 0; chiy = 0; chiz = 0;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  qx = {rhox, ux, vx, wx, px, ntx};
  qy = {rhoy, uy, vy, wy, py, nty};
  qz = {rhoz, uz, vz, wz, pz, ntz};

  srcTrue = -0.009933121590668479;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, z, time, q, qx, qy, qz, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_SMALL( src(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(5), close_tol );

  pde.source( dist, x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_SMALL( src_split(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(5), close_tol*10 );

  // case 6: negative SA; nonconservative diffusion dominant
  if (verbose) cout << "source: case6" << endl;

  chi  = -5.241;
  chix = 1.963; chiy = -0.781; chiz = 0;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  qx = {rhox, ux, vx, wx, px, ntx};
  qy = {rhoy, uy, vy, wy, py, nty};
  qz = {rhoz, uz, vz, wz, pz, ntz};

  srcTrue = -3.031888117778473;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, z, time, q, qx, qy, qz, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_SMALL( src(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(5), close_tol );

  pde.source( dist, x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_SMALL( src_split(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(5), close_tol*10 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( source_zy )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  bool verbose = false;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, z, time;
  Real rho, u, v, w, t;

  ArrayQ q, qx, qy, qz;
  ArrayQ qp = 0, qpx = 0, qpy = 0, qpz = 0;
  Real srcTrue;
  ArrayQ src, src_split;

  x = 0; y = 0; z = 0; time = 0;   // not actually used in functions
  rho = 1.225;
  u = 0;
  v = -3.231;
  w = 6.974;
  t = 2.8815;

  Real rhox = 0,      ux = 0, vx =  0     , wx =  0     , tx =  0     ;
  Real rhoy = 0.0127, uy = 0, vy = -0.0022, wy = -0.0124, ty = -0.0111;
  Real rhoz = 0.0421, uz = 0, vz = -0.0312, wz =  0.0521, tz = -0.0715;

  Real px, py, pz;

  px = R*(rho*tx + rhox*t);
  py = R*(rho*ty + rhoy*t);
  pz = R*(rho*tz + rhoz*t);

  Real nt, ntx, nty, ntz;
  Real chi, chix, chiy, chiz;
  Real dist;

  // case 1: prod/wall dominant; S~ branch 1
  if (verbose) cout << "source: case1" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0; chiz = 0;
  dist = 60;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  qx = {rhox, ux, vx, wx, px, ntx};
  qy = {rhoy, uy, vy, wy, py, nty};
  qz = {rhoz, uz, vz, wz, pz, ntz};

  srcTrue = 0.0015950811868193507;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, z, time, q, qx, qy, qz, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_SMALL( src(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(5), close_tol*10 );

  pde.source( dist, x, y, z, time, q, qx, qy, qz, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_SMALL( src(4), small_tol );
  BOOST_CHECK_CLOSE( 2*srcTrue, src(5), close_tol*10 );

  pde.source( dist, x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_SMALL( src_split(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(5), close_tol*10 );

  pde.source( dist, x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_SMALL( src_split(4), small_tol );
  BOOST_CHECK_CLOSE( 2*srcTrue, src_split(5), close_tol*10 );

  // case 2: prod/wall dominant; S~ branch 2
  if (verbose) cout << "source: case2" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0; chiz = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  qx = {rhox, ux, vx, wx, px, ntx};
  qy = {rhoy, uy, vy, wy, py, nty};
  qz = {rhoz, uz, vz, wz, pz, ntz};

  srcTrue = 0.08843084458519657;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, z, time, q, qx, qy, qz, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_SMALL( src(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(5), close_tol );

  pde.source( dist, x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_SMALL( src_split(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(5), close_tol*10 );

  // case 3: prod/wall ft2 terms dominant
  if (verbose) cout << "source: case3" << endl;

  chi  = 1.241;
  chix = 0; chiy = 0; chiz = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  qx = {rhox, ux, vx, wx, px, ntx};
  qy = {rhoy, uy, vy, wy, py, nty};
  qz = {rhoz, uz, vz, wz, pz, ntz};

  srcTrue = -0.001315819430510617;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, z, time, q, qx, qy, qz, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_SMALL( src(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(5), close_tol );

  pde.source( dist, x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_SMALL( src_split(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(5), close_tol*10 );

  // case 4: nonconservative diffusion dominant
  if (verbose) cout << "source: case4" << endl;

  chi  = 5.241;
  chix = 0; chiy = -0.781; chiz = 1.963;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  qx = {rhox, ux, vx, wx, px, ntx};
  qy = {rhoy, uy, vy, wy, py, nty};
  qz = {rhoz, uz, vz, wz, pz, ntz};

  srcTrue = -2.857308070100351;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, z, time, q, qx, qy, qz, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_SMALL( src(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(5), close_tol );

  pde.source( dist, x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_SMALL( src_split(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(5), close_tol*10 );

  // case 5: negative SA; prod/wall dominant
  if (verbose) cout << "source: case5" << endl;

  chi  = -5.241;
  chix = 0; chiy = 0; chiz = 0;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  qx = {rhox, ux, vx, wx, px, ntx};
  qy = {rhoy, uy, vy, wy, py, nty};
  qz = {rhoz, uz, vz, wz, pz, ntz};

  srcTrue = -0.009933121590668479;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, z, time, q, qx, qy, qz, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_SMALL( src(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(5), close_tol );

  pde.source( dist, x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_SMALL( src_split(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(5), close_tol*10 );

  // case 6: negative SA; nonconservative diffusion dominant
  if (verbose) cout << "source: case6" << endl;

  chi  = -5.241;
  chix = 0; chiy = -0.781; chiz = 1.963;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  qx = {rhox, ux, vx, wx, px, ntx};
  qy = {rhoy, uy, vy, wy, py, nty};
  qz = {rhoz, uz, vz, wz, pz, ntz};

  srcTrue = -3.031888117778473;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, z, time, q, qx, qy, qz, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_SMALL( src(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(5), close_tol );

  pde.source( dist, x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_SMALL( src_split(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(5), close_tol*10 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( source_xz )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  bool verbose = false;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, z, time;
  Real rho, u, v, w, t;

  ArrayQ q, qx, qy, qz;
  ArrayQ qp = 0, qpx = 0, qpy = 0, qpz = 0;
  Real srcTrue;
  ArrayQ src, src_split;

  x = 0; y = 0; z = 0; time = 0;   // not actually used in functions
  rho = 1.225;
  u = 6.974;
  v = 0;
  w = -3.231;
  t = 2.8815;

  Real rhox = 0.0421, ux =  0.0521, vx = 0, wx = -0.0312, tx = -0.0715;
  Real rhoy = 0     , uy =  0     , vy = 0, wy =  0     , ty =  0     ;
  Real rhoz = 0.0127, uz = -0.0124, vz = 0, wz = -0.0022, tz = -0.0111;

  Real px, py, pz;

  px = R*(rho*tx + rhox*t);
  py = R*(rho*ty + rhoy*t);
  pz = R*(rho*tz + rhoz*t);

  Real nt, ntx, nty, ntz;
  Real chi, chix, chiy, chiz;
  Real dist;

  // case 1: prod/wall dominant; S~ branch 1
  if (verbose) cout << "source: case1" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0; chiz = 0;
  dist = 60;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  qx = {rhox, ux, vx, wx, px, ntx};
  qy = {rhoy, uy, vy, wy, py, nty};
  qz = {rhoz, uz, vz, wz, pz, ntz};

  srcTrue = 0.0015950811868193507;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, z, time, q, qx, qy, qz, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_SMALL( src(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(5), close_tol*10 );

  pde.source( dist, x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_SMALL( src_split(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(5), close_tol*10 );

  // case 2: prod/wall dominant; S~ branch 2
  if (verbose) cout << "source: case2" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0; chiz = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  qx = {rhox, ux, vx, wx, px, ntx};
  qy = {rhoy, uy, vy, wy, py, nty};
  qz = {rhoz, uz, vz, wz, pz, ntz};

  srcTrue = 0.08843084458519657;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, z, time, q, qx, qy, qz, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_SMALL( src(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(5), close_tol );

  pde.source( dist, x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_SMALL( src_split(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(5), close_tol*10 );

  // case 3: prod/wall ft2 terms dominant
  if (verbose) cout << "source: case3" << endl;

  chi  = 1.241;
  chix = 0; chiy = 0; chiz = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  qx = {rhox, ux, vx, wx, px, ntx};
  qy = {rhoy, uy, vy, wy, py, nty};
  qz = {rhoz, uz, vz, wz, pz, ntz};

  srcTrue = -0.001315819430510617;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, z, time, q, qx, qy, qz, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_SMALL( src(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(5), close_tol );

  pde.source( dist, x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_SMALL( src_split(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(5), close_tol*10 );

  // case 4: nonconservative diffusion dominant
  if (verbose) cout << "source: case4" << endl;

  chi  = 5.241;
  chix = 1.963; chiy = 0; chiz = -0.781;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  qx = {rhox, ux, vx, wx, px, ntx};
  qy = {rhoy, uy, vy, wy, py, nty};
  qz = {rhoz, uz, vz, wz, pz, ntz};

  srcTrue = -2.857308070100351;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, z, time, q, qx, qy, qz, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_SMALL( src(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(5), close_tol );

  pde.source( dist, x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_SMALL( src_split(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(5), close_tol*10 );

  // case 5: negative SA; prod/wall dominant
  if (verbose) cout << "source: case5" << endl;

  chi  = -5.241;
  chix = 0; chiy = 0; chiz = 0;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  qx = {rhox, ux, vx, wx, px, ntx};
  qy = {rhoy, uy, vy, wy, py, nty};
  qz = {rhoz, uz, vz, wz, pz, ntz};

  srcTrue = -0.009933121590668479;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, z, time, q, qx, qy, qz, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_SMALL( src(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(5), close_tol );

  pde.source( dist, x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_SMALL( src_split(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(5), close_tol*10 );

  // case 6: negative SA; nonconservative diffusion dominant
  if (verbose) cout << "source: case6" << endl;

  chi  = -5.241;
  chix = 1.963; chiy = 0; chiz = -0.781;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q = pde.setDOFFrom( SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nt}) );

  qx = {rhox, ux, vx, wx, px, ntx};
  qy = {rhoy, uy, vy, wy, py, nty};
  qz = {rhoz, uz, vz, wz, pz, ntz};

  srcTrue = -3.031888117778473;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, z, time, q, qx, qy, qz, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_SMALL( src(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(5), close_tol );

  pde.source( dist, x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_SMALL( src_split(4), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(5), close_tol*10 );
}



//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( perturbedSource_xy )
{

  // Use conservative variables here in order to differentiate with Surreal
  typedef QTypeConservative QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  bool verbose = false;

  const Real small_tol = 1.e-10;
  const Real close_tol = 1.e-10;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real tSuth = 110./300.; //somewhat random...
  const Real tRef  = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, z, time;
  Real rho, u, v, w, t;

  x = 0; y = 0; z =0; time = 0;   // not actually used in functions
  rho = 1.225;
  u = 6.974;
  v = -3.231;
  w = 0;
  t = 2.8815;

  Real e = gas.energy(rho, t);
  Real E = e + 0.5*(u*u + v*v + w*w);

  Real rhox = 0.0421, rhoy = 0.0127, rhoz = 0;
  Real ux = 0.0521, uy = -0.0124, uz = 0;
  Real vx = -0.0312, vy = -0.0022, vz = 0;
  Real wx = 0, wy = 0, wz = 0;
  Real tx = -0.0715, ty = -0.0111, tz = 0;

  //Real px = R*(rho*tx + rhox*t);
  //Real py = R*(rho*ty + rhoy*t);

  Real rhoux = rhox*u + ux*rho;
  Real rhouy = rhoy*u + uy*rho;
  Real rhouz = rhoz*u + uz*rho;

  Real rhovx = rhox*v + vx*rho;
  Real rhovy = rhoy*v + vy*rho;
  Real rhovz = rhoz*v + vz*rho;

  Real rhowx = rhox*w + wx*rho;
  Real rhowy = rhoy*w + wy*rho;
  Real rhowz = rhoz*w + wz*rho;

  Real ex, ey, ez;
  gas.energyGradient(rho, t, rhox, tx, ex);
  gas.energyGradient(rho, t, rhoy, ty, ey);
  gas.energyGradient(rho, t, rhoz, tz, ez);

  Real Ex = ex + ux*u + vx*v + wx*w;
  Real Ey = ey + uy*u + vy*v + wy*w;
  Real Ez = ez + uz*u + vz*v + wz*w;

  Real rhoEx = rhox*E + rho*Ex;
  Real rhoEy = rhoy*E + rho*Ey;
  Real rhoEz = rhoy*E + rho*Ez;

  Real nt, ntx, nty, ntz;
  Real chi, chix, chiy, chiz;
  Real dist;

  ArrayQ q, qx, qy, qz, dU, dUx, dUy, dUz, qp, qpx, qpy, qpz, dS, dStrue;
  MatrixQ dsdu, dsdux, dsduy, dsduz;

  MatrixQ dudq = 0;

  // set the complete state vectors (sans nt)
  // the nt values are set for each case
  pde.setDOFFrom( q, SAnt3D<Conservative3D<Real>>({rho , rho*u, rho*v, rho*w, rho*E, 0}));

  qx = {rhox, rhoux, rhovx, rhowx, rhoEx, 0};
  qy = {rhoy, rhouy, rhovy, rhowy, rhoEy, 0};
  qz = {rhoz, rhouz, rhovz, rhowz, rhoEz, 0};

  // perturbed state
  qp  = { 0.01, -0.3,  0.1, 0, 0.5, 0.002};
  qpx = {  0.2,  0.1, -0.9, 0, 0.4, -0.03};
  qpy = {-0.01, -0.6,  0.2, 0, 0.7, 0.005};
  qpz = {0,0,0,0,0,0};

  // case 1: prod/wall dominant; S~ branch 1
  if (verbose) cout << "source: case1" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0; chiz = 0;
  dist = 60;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q[5]  = rho*nt;
  qx[5] = rhox*nt + rho*ntx;
  qy[5] = rhoy*nt + rho*nty;
  qz[5] = rhoz*nt + rho*ntz;

  dudq = 0;
  pde.jacobianMasterState(dist, x, y, z, q, dudq);

  dU = dudq*qp;
  dUx = dudq*qpx;
  dUy = dudq*qpy;
  dUz = dudq*qpz;

  dsdu = 0;
  pde.jacobianSource( dist, x, y, z, time, q, qx, qy, qz, dsdu );

  dsdux = 0; dsduy = 0; dsduz = 0;
  pde.jacobianGradientSource( dist, x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz );

  dStrue = dsdu*dU + dsdux*dUx + dsduy*dUy + dsduz*dUz;

  dS = 0;
  pde.perturbedSource( dist, x, y, z, time, q, qx, qy, qz, dU, dUx, dUy, dUz, dS );

  for (int i = 0; i < pde.N; i++)
  {
    if (verbose) std::cout << " dStrue(" << i << ") = " << dStrue(i) << " | " << dS(i) << std::endl;
    SANS_CHECK_CLOSE( dStrue(i), dS(i), small_tol, close_tol );
  }

  // case 2: prod/wall dominant; S~ branch 2
  if (verbose) cout << "source: case2" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0; chiz = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q[5]  = rho*nt;
  qx[5] = rhox*nt + rho*ntx;
  qy[5] = rhoy*nt + rho*nty;
  qz[5] = rhoz*nt + rho*ntz;

  dudq = 0;
  pde.jacobianMasterState(dist, x, y, z, q, dudq);

  dU = dudq*qp;
  dUx = dudq*qpx;
  dUy = dudq*qpy;
  dUz = dudq*qpz;

  dsdu = 0;
  pde.jacobianSource( dist, x, y, z, time, q, qx, qy, qz, dsdu );

  dsdux = 0; dsduy = 0; dsduz = 0;
  pde.jacobianGradientSource( dist, x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz );

  dStrue = dsdu*dU + dsdux*dUx + dsduy*dUy + dsduz*dUz;

  dS = 0;
  pde.perturbedSource( dist, x, y, z, time, q, qx, qy, qz, dU, dUx, dUy, dUz, dS );

  for (int i = 0; i < pde.N; i++)
  {
    if (verbose) std::cout << " dStrue(" << i << ") = " << dStrue(i) << " | " << dS(i) << std::endl;
    SANS_CHECK_CLOSE( dStrue(i), dS(i), small_tol, close_tol );
  }


  // case 3: prod/wall ft2 terms dominant
  if (verbose) cout << "source: case3" << endl;

  chi  = 1.241;
  chix = 0; chiy = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q[5]  = rho*nt;
  qx[5] = rhox*nt + rho*ntx;
  qy[5] = rhoy*nt + rho*nty;
  qz[5] = rhoz*nt + rho*ntz;

  dudq = 0;
  pde.jacobianMasterState(dist, x, y, z, q, dudq);

  dU = dudq*qp;
  dUx = dudq*qpx;
  dUy = dudq*qpy;
  dUz = dudq*qpz;

  dsdu = 0;
  pde.jacobianSource( dist, x, y, z, time, q, qx, qy, qz, dsdu );

  dsdux = 0; dsduy = 0; dsduz = 0;
  pde.jacobianGradientSource( dist, x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz );

  dStrue = dsdu*dU + dsdux*dUx + dsduy*dUy + dsduz*dUz;

  dS = 0;
  pde.perturbedSource( dist, x, y, z, time, q, qx, qy, qz, dU, dUx, dUy, dUz, dS );

  for (int i = 0; i < pde.N; i++)
  {
    if (verbose) std::cout << " dStrue(" << i << ") = " << dStrue(i) << " | " << dS(i) << std::endl;
    SANS_CHECK_CLOSE( dStrue(i), dS(i), small_tol, close_tol );
  }


  // case 4: nonconservative diffusion dominant
  if (verbose) cout << "source: case4" << endl;

  chi  = 5.241;
  chix = 1.963; chiy = -0.781;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q[5]  = rho*nt;
  qx[5] = rhox*nt + rho*ntx;
  qy[5] = rhoy*nt + rho*nty;
  qz[5] = rhoz*nt + rho*ntz;

  dudq = 0;
  pde.jacobianMasterState(dist, x, y, z, q, dudq);

  dU = dudq*qp;
  dUx = dudq*qpx;
  dUy = dudq*qpy;
  dUz = dudq*qpz;

  dsdu = 0;
  pde.jacobianSource( dist, x, y, z, time, q, qx, qy, qz, dsdu );

  dsdux = 0; dsduy = 0; dsduz = 0;
  pde.jacobianGradientSource( dist, x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz );

  dStrue = dsdu*dU + dsdux*dUx + dsduy*dUy + dsduz*dUz;

  dS = 0;
  pde.perturbedSource( dist, x, y, z, time, q, qx, qy, qz, dU, dUx, dUy, dUz, dS );

  for (int i = 0; i < pde.N; i++)
  {
    if (verbose) std::cout << " dStrue(" << i << ") = " << dStrue(i) << " | " << dS(i) << std::endl;
    SANS_CHECK_CLOSE( dStrue(i), dS(i), small_tol, close_tol );
  }

  // case 5: negative SA; prod/wall dominant
  if (verbose) cout << "source: case5" << endl;

  chi  = -5.241;
  chix = 0; chiy = 0;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q[5]  = rho*nt;
  qx[5] = rhox*nt + rho*ntx;
  qy[5] = rhoy*nt + rho*nty;
  qz[5] = rhoz*nt + rho*ntz;

  dudq = 0;
  pde.jacobianMasterState(dist, x, y, z, q, dudq);

  dU = dudq*qp;
  dUx = dudq*qpx;
  dUy = dudq*qpy;
  dUz = dudq*qpz;

  dsdu = 0;
  pde.jacobianSource( dist, x, y, z, time, q, qx, qy, qz, dsdu );

  dsdux = 0; dsduy = 0; dsduz = 0;
  pde.jacobianGradientSource( dist, x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz );

  dStrue = dsdu*dU + dsdux*dUx + dsduy*dUy + dsduz*dUz;

  dS = 0;
  pde.perturbedSource( dist, x, y, z, time, q, qx, qy, qz, dU, dUx, dUy, dUz, dS );

  for (int i = 0; i < pde.N; i++)
  {
    if (verbose) std::cout << " dStrue(" << i << ") = " << dStrue(i) << " | " << dS(i) << std::endl;
    SANS_CHECK_CLOSE( dStrue(i), dS(i), small_tol, close_tol );
  }


  // case 6: negative SA; nonconservative diffusion dominant
  if (verbose) cout << "source: case6" << endl;

  chi  = -5.241;
  chix = 1.963; chiy = -0.781;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q[5]  = rho*nt;
  qx[5] = rhox*nt + rho*ntx;
  qy[5] = rhoy*nt + rho*nty;
  qz[5] = rhoz*nt + rho*ntz;

  dudq = 0;
  pde.jacobianMasterState(dist, x, y, z, q, dudq);

  dU = dudq*qp;
  dUx = dudq*qpx;
  dUy = dudq*qpy;
  dUz = dudq*qpz;

  dsdu = 0;
  pde.jacobianSource( dist, x, y, z, time, q, qx, qy, qz, dsdu );

  dsdux = 0; dsduy = 0; dsduz = 0;
  pde.jacobianGradientSource( dist, x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz );

  dStrue = dsdu*dU + dsdux*dUx + dsduy*dUy + dsduz*dUz;

  dS = 0;
  pde.perturbedSource( dist, x, y, z, time, q, qx, qy, qz, dU, dUx, dUy, dUz, dS );

  for (int i = 0; i < pde.N; i++)
  {
    if (verbose) std::cout << " dStrue(" << i << ") = " << dStrue(i) << " | " << dS(i) << std::endl;
    SANS_CHECK_CLOSE( dStrue(i), dS(i), small_tol, close_tol );
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jacobianSource )
{
  // Use conservative variables here in order to differentiate with Surreal
  typedef QTypeConservative QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef SurrealS<PDEClass::N> SurrealClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;
  typedef PDEClass::ArrayQ<SurrealClass> ArrayQSurreal;

  bool verbose = false;

  const Real small_tol = 5.e-9;
  const Real close_tol = 5.e-9;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real tSuth = 110/300; //somewhat random...
  const Real tRef  = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, z, time;
  Real rho, u, v, w, t;

  x = 0; y = 0; time = 0;   // not actually used in functions
  rho = 1.225;
  u = 6.974;
  v = -3.231;
  w = 1.903;
  t = 2.8815;

  Real e = gas.energy(rho, t);
  Real E = e + 0.5*(u*u + v*v + w*w);

  Real rhox = 0.0421, rhoy = 0.0127, rhoz = 0.691;
  Real ux =  0.0521, uy = -0.0124, uz = -0.145;
  Real vx = -0.0312, vy = -0.0022, vz =  0.089;
  Real wx = -0.0546, wy = -0.0735, wz =  0.032;
  Real tx = -0.0715, ty = -0.0111, tz =  0.567;

  Real rhoux = rhox*u + ux*rho;
  Real rhouy = rhoy*u + uy*rho;
  Real rhouz = rhoz*u + uz*rho;

  Real rhovx = rhox*v + vx*rho;
  Real rhovy = rhoy*v + vy*rho;
  Real rhovz = rhoz*v + vz*rho;

  Real rhowx = rhox*w + wx*rho;
  Real rhowy = rhoy*w + wy*rho;
  Real rhowz = rhoz*w + wz*rho;

  Real ex, ey, ez;
  gas.energyGradient(rho, t, rhox, tx, ex);
  gas.energyGradient(rho, t, rhoy, ty, ey);
  gas.energyGradient(rho, t, rhoz, tz, ez);

  Real Ex = ex + ux*u + vx*v + wx*w;
  Real Ey = ey + uy*u + vy*v + wy*w;
  Real Ez = ez + uz*u + vz*v + wz*w;

  Real rhoEx = rhox*E + rho*Ex;
  Real rhoEy = rhoy*E + rho*Ey;
  Real rhoEz = rhoz*E + rho*Ez;

  Real nt, ntx, nty, ntz;
  Real chi, chix, chiy, chiz;
  Real dist;

  ArrayQ q, qx, qy, qz;
  MatrixQ dsdu;
  ArrayQSurreal qS;
  ArrayQSurreal src;

  SurrealClass rhoS  = rho;
  SurrealClass rhouS = rho*u;
  SurrealClass rhovS = rho*v;
  SurrealClass rhowS = rho*w;
  SurrealClass rhoES = rho*E;
  SurrealClass rhontS = 0;

  rhoS.deriv(0)   = 1;
  rhouS.deriv(1)  = 1;
  rhovS.deriv(2)  = 1;
  rhowS.deriv(3)  = 1;
  rhoES.deriv(4)  = 1;
  rhontS.deriv(5) = 1;

  // set the complete state vectors (sans nt)
  // the nt values are set for each case
  pde.setDOFFrom( q, SAnt3D<Conservative3D<Real>>        ({rho , rho*u, rho*v, rho*w, rho*E, 0     }));
  pde.setDOFFrom(qS, SAnt3D<Conservative3D<SurrealClass>>({rhoS, rhouS, rhovS, rhowS, rhoES, rhontS}));

  qx = {rhox, rhoux, rhovx, rhowx, rhoEx, 0};
  qy = {rhoy, rhouy, rhovy, rhowy, rhoEy, 0};
  qz = {rhoz, rhouz, rhovz, rhowz, rhoEz, 0};


  // case 1: prod/wall dominant; S~ branch 1
  if (verbose) cout << "source: case1" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0; chiz = 0;
  dist = 60;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  qS[5].value() = q[5]  = rho*nt;

  qx[5] = rhox*nt + rho*ntx;
  qy[5] = rhoy*nt + rho*nty;
  qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, z, time, qS, qx, qy, qz, src );

  // Compute the hand coded Jacobian
  dsdu = 0;
  pde.jacobianSource( dist, x, y, z, time, q, qx, qy, qz, dsdu );

  MatrixQ dsduh = 0;
  pde.jacobianSourceHACK( dist, x, y, z, time, q, qx, qy, qz, dsduh );

  MatrixQ dudv = 0, dvdu = 0;
  pde.adjointVariableJacobian(x, y, z, time, q, dudv);
  pde.adjointVariableJacobianInverse(x, y, z, time, q, dvdu);

  MatrixQ dsduhTRUE = dvdu*dsdu*dudv;

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      SANS_CHECK_CLOSE( dsduhTRUE(i,j), dsduh(i,j), small_tol, close_tol );
    }


  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << j << ") = " << src(i).deriv(j) << " | " << dsdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(j), dsdu(i,j), small_tol, close_tol );
    }


  // case 2: prod/wall dominant; S~ branch 2
  if (verbose) cout << "source: case2" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0; chiz = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  qS[5].value() = q[5]  = rho*nt;

  qx[5] = rhox*nt + rho*ntx;
  qy[5] = rhoy*nt + rho*nty;
  qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, z, time, qS, qx, qy, qz, src );

  // Compute the hand coded Jacobian
  dsdu = 0;
  pde.jacobianSource( dist, x, y, z, time, q, qx, qy, qz, dsdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << j << ") = " << src(i).deriv(j) << " | " << dsdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(j), dsdu(i,j), small_tol, close_tol );
    }


  // case 3: prod/wall ft2 terms dominant
  if (verbose) cout << "source: case3" << endl;

  chi  = 1.241;
  chix = 0; chiy = 0; chiz = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  qS[5].value() = q[5]  = rho*nt;

  qx[5] = rhox*nt + rho*ntx;
  qy[5] = rhoy*nt + rho*nty;
  qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, z, time, qS, qx, qy, qz, src );

  // Compute the hand coded Jacobian
  dsdu = 0;
  pde.jacobianSource( dist, x, y, z, time, q, qx, qy, qz, dsdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << j << ") = " << src(i).deriv(j) << " | " << dsdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(j), dsdu(i,j), small_tol, close_tol );
    }


  // case 4: nonconservative diffusion dominant
  if (verbose) cout << "source: case4" << endl;

  chi  = 5.241;
  chix = 1.963; chiy = -0.781; chiz = 1.456;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  qS[5].value() = q[5]  = rho*nt;

  qx[5] = rhox*nt + rho*ntx;
  qy[5] = rhoy*nt + rho*nty;
  qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, z, time, qS, qx, qy, qz, src );

  // Compute the hand coded Jacobian
  dsdu = 0;
  pde.jacobianSource( dist, x, y, z, time, q, qx, qy, qz, dsdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << j << ") = " << src(i).deriv(j) << " | " << dsdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(j), dsdu(i,j), small_tol, close_tol );
    }


  // case 5: negative SA; prod/wall dominant
  if (verbose) cout << "source: case5" << endl;

  chi  = -5.241;
  chix = 0; chiy = 0; chiz = 0;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  qS[5].value() = q[5]  = rho*nt;

  qx[5] = rhox*nt + rho*ntx;
  qy[5] = rhoy*nt + rho*nty;
  qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, z, time, qS, qx, qy, qz, src );

  // Compute the hand coded Jacobian
  dsdu = 0;
  pde.jacobianSource( dist, x, y, z, time, q, qx, qy, qz, dsdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << j << ") = " << src(i).deriv(j) << " | " << dsdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(j), dsdu(i,j), small_tol, close_tol );
    }


  // case 6: negative SA; nonconservative diffusion dominant
  if (verbose) cout << "source: case6" << endl;

  chi  = -5.241;
  chix = 1.963; chiy = -0.781; chiz = 1.456;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  qS[5].value() = q[5]  = rho*nt;

  qx[5] = rhox*nt + rho*ntx;
  qy[5] = rhoy*nt + rho*nty;
  qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, z, time, qS, qx, qy, qz, src );

  // Compute the hand coded Jacobian
  dsdu = 0;
  pde.jacobianSource( dist, x, y, z, time, q, qx, qy, qz, dsdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << j << ") = " << src(i).deriv(j) << " | " << dsdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(j), dsdu(i,j), small_tol, close_tol );
    }


  // case 7: negative SA; shrmod < -cv2_*shr0
  if (verbose) cout << "source: case7" << endl;

  chi  = 10.241;
  chix = 1.963; chiy = -0.781; chiz = 1.456;
  dist = 0.1;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  qS[5].value() = q[5]  = rho*nt;

  qx[5] = rhox*nt + rho*ntx;
  qy[5] = rhoy*nt + rho*nty;
  qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, z, time, qS, qx, qy, qz, src );

  // Compute the hand coded Jacobian
  dsdu = 0;
  pde.jacobianSource( dist, x, y, z, time, q, qx, qy, qz, dsdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << j << ") = " << src(i).deriv(j) << " | " << dsdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(j), dsdu(i,j), small_tol, close_tol );
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jacobianGradientSource )
{
  // Use conservative variables here in order to differentiate with Surreal
  typedef QTypeConservative QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef SurrealS<PhysD3::D*PDEClass::N> SurrealClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;
  typedef PDEClass::ArrayQ<SurrealClass> ArrayQSurreal;

  bool verbose = false;

  const Real small_tol = 1.e-10;
  const Real close_tol = 1.e-10;
  //const Real ping_tol = 2.e-5;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real tSuth = 110/300; //somewhat random...
  const Real tRef  = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, z, time;
  Real rho, u, v, w, t;

  x = 0; y = 0; time = 0;   // not actually used in functions
  rho = 1.225;
  u = 6.974;
  v = -3.231;
  w = 1.903;
  t = 2.8815;

  Real e = gas.energy(rho, t);
  Real E = e + 0.5*(u*u + v*v + w*w);

  Real rhox = 0.0421, rhoy = 0.0127, rhoz = 0.691;
  Real ux =  0.0521, uy = -0.0124, uz = -0.145;
  Real vx = -0.0312, vy = -0.0022, vz =  0.089;
  Real wx = -0.0546, wy = -0.0735, wz =  0.032;
  Real tx = -0.0715, ty = -0.0111, tz =  0.567;

  Real rhoux = rhox*u + ux*rho;
  Real rhouy = rhoy*u + uy*rho;
  Real rhouz = rhoz*u + uz*rho;

  Real rhovx = rhox*v + vx*rho;
  Real rhovy = rhoy*v + vy*rho;
  Real rhovz = rhoz*v + vz*rho;

  Real rhowx = rhox*w + wx*rho;
  Real rhowy = rhoy*w + wy*rho;
  Real rhowz = rhoz*w + wz*rho;

  Real ex, ey, ez;
  gas.energyGradient(rho, t, rhox, tx, ex);
  gas.energyGradient(rho, t, rhoy, ty, ey);
  gas.energyGradient(rho, t, rhoz, tz, ez);

  Real Ex = ex + ux*u + vx*v + wx*w;
  Real Ey = ey + uy*u + vy*v + wy*w;
  Real Ez = ez + uz*u + vz*v + wz*w;

  Real rhoEx = rhox*E + rho*Ex;
  Real rhoEy = rhoy*E + rho*Ey;
  Real rhoEz = rhoz*E + rho*Ez;

  Real nt, ntx, nty, ntz;
  Real chi, chix, chiy, chiz;
  Real dist;

  ArrayQ q, qx, qy, qz;
  MatrixQ dsdux, dsduy, dsduz;
  ArrayQSurreal qxS, qyS, qzS;
  ArrayQSurreal src;

  // set the complete state vectors (sans nt)
  // the nt values are set for each case
  pde.setDOFFrom( q, SAnt3D<Conservative3D<Real>>({rho , rho*u, rho*v, rho*w, rho*E, 0}));

  qxS = qx = {rhox, rhoux, rhovx, rhowx, rhoEx, 0};
  qyS = qy = {rhoy, rhouy, rhovy, rhowy, rhoEy, 0};
  qzS = qz = {rhoz, rhouz, rhovz, rhowz, rhoEz, 0};

  ArrayQ qxx = {-0.001378, -0.002479, 0.004789, 0.0015661, 0.001924, 0.0019838};
  ArrayQ qxy = {-0.00183873, -0.0028383, 0.004891, 0.003218, 0.004729, 0.004848};
  ArrayQ qxz = {-0.0015647, -0.0098565, 0.003331, 0.001523, 0.004656, 0.0041651};
  ArrayQ qyy = {-0.0014934, -0.005294, 0.001748, 0.003546, 0.0029808, 0.002974};
  ArrayQ qyz = {-0.001561, -0.005954, 0.0016576, 0.003454, 0.00189746, 0.004156};
  ArrayQ qzz = {-0.001746, -0.009531, 0.0011464, 0.0093158, 0.006343, 0.003645};

  for (int n = 0; n < PDEClass::N; n++)
  {
    qxS[n].deriv(0*PDEClass::N + n) = 1;
    qyS[n].deriv(1*PDEClass::N + n) = 1;
    qzS[n].deriv(2*PDEClass::N + n) = 1;
  }

  // case 1: prod/wall dominant; S~ branch 1
  if (verbose) cout << "source: case1" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0; chiz = 0;
  dist = 60;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q[5]  = rho*nt;

  qxS[5].value() = qx[5] = rhox*nt + rho*ntx;
  qyS[5].value() = qy[5] = rhoy*nt + rho*nty;
  qzS[5].value() = qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, z, time, q, qxS, qyS, qzS, src );

  // Compute the hand coded Jacobian
  dsdux=0; dsduy=0; dsduz=0;
  pde.jacobianGradientSource( dist, x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << 0*pde.N+j << ") = " << src(i).deriv(0*pde.N+j) << " | " << dsdux(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(0*pde.N+j), dsdux(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 1*pde.N+j << ") = " << src(i).deriv(1*pde.N+j) << " | " << dsduy(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(1*pde.N+j), dsduy(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 2*pde.N+j << ") = " << src(i).deriv(2*pde.N+j) << " | " << dsduz(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(2*pde.N+j), dsduz(i,j), small_tol, close_tol );
    }

#if 0
  Real distx = 0, disty= 0, distz= 0; //NEGLECTING DISTANCE FUNCTION GRADIENTS
  //PING TEST FOR DIV OF JACOBIANGRADIENTSOURCE
  {

    MatrixQ grad_dsdgradu = 0;
    pde.jacobianGradientSourceGradient(dist, x, y, z, time,
                                       q, qx, qy, qz,
                                       qxx, qxy, qyy,
                                       qxz, qyz, qzz,
                                       grad_dsdgradu);

    //rough ping test
    Real dx = 1e-6;
    Real dy = 1e-6;
    Real dz = 1e-6;

    Real dist1x = dist + distx*dx;
    Real dist0x = dist - distx*dx;

    //x derivative
    ArrayQ q1x = q + qx*dx;
    ArrayQ q0x = q - qx*dx;
    ArrayQ qx1x = qx+qxx*dx;
    ArrayQ qx0x = qx-qxx*dx;
    ArrayQ qy1x = qy+qxy*dx;
    ArrayQ qy0x = qy-qxy*dx;
    ArrayQ qz1x = qz+qxz*dx;
    ArrayQ qz0x = qz-qxz*dx;
    MatrixQ dsdux1x=0; MatrixQ dsduy1x=0; MatrixQ dsduz1x=0;
    MatrixQ dsdux0x=0; MatrixQ dsduy0x=0; MatrixQ dsduz0x=0;
    pde.jacobianGradientSource( dist1x, x, y, z, time, q1x, qx1x, qy1x, qz1x, dsdux1x, dsduy1x, dsduz1x );
    pde.jacobianGradientSource( dist0x, x, y, z, time, q0x, qx0x, qy0x, qz0x, dsdux0x, dsduy0x, dsduz0x );

    Real dist1y = dist + disty*dy;
    Real dist0y = dist - disty*dy;
    //y derivative
    ArrayQ q1y = q + qy*dy;
    ArrayQ q0y = q - qy*dy;
    ArrayQ qx1y = qx+qxy*dy;
    ArrayQ qx0y = qx-qxy*dy;
    ArrayQ qy1y = qy+qyy*dy;
    ArrayQ qy0y = qy-qyy*dy;
    ArrayQ qz1y = qz+qyz*dy;
    ArrayQ qz0y = qz-qyz*dy;

    MatrixQ dsdux1y=0; MatrixQ dsduy1y=0; MatrixQ dsduz1y=0;
    MatrixQ dsdux0y=0; MatrixQ dsduy0y=0; MatrixQ dsduz0y=0;
    pde.jacobianGradientSource( dist1y, x, y, z, time, q1y, qx1y, qy1y, qz1y, dsdux1y, dsduy1y, dsduz1y );
    pde.jacobianGradientSource( dist0y, x, y, z, time, q0y, qx0y, qy0y, qz0y, dsdux0y, dsduy0y, dsduz0y );

    Real dist1z = dist + distz*dz;
    Real dist0z = dist - distz*dz;
    //y derivative
    ArrayQ q1z = q + qz*dz;
    ArrayQ q0z = q - qz*dz;
    ArrayQ qx1z = qx+qxz*dz;
    ArrayQ qx0z = qx-qxz*dz;
    ArrayQ qy1z = qy+qyz*dz;
    ArrayQ qy0z = qy-qyz*dz;
    ArrayQ qz1z = qz+qzz*dz;
    ArrayQ qz0z = qz-qzz*dz;

    MatrixQ dsdux1z=0; MatrixQ dsduy1z=0; MatrixQ dsduz1z=0;
    MatrixQ dsdux0z=0; MatrixQ dsduy0z=0; MatrixQ dsduz0z=0;
    pde.jacobianGradientSource( dist1z, x, y, z, time, q1z, qx1z, qy1z, qz1z, dsdux1z, dsduy1z, dsduz1z );
    pde.jacobianGradientSource( dist0z, x, y, z, time, q0z, qx0z, qy0z, qz0z, dsdux0z, dsduy0z, dsduz0z );

    MatrixQ pingmat = (dsdux1x - dsdux0x)/(2*dx) + (dsduy1y - dsduy0y)/(2*dy) + (dsduz1z - dsduz0z)/(2*dz);

    for (int i = 0; i < pde.N; i++)
    {
      for (int j = 0; j < pde.N; j++)
      {
        std::cout << "(" << i << "," << j << "), " << pingmat(i,j) << ", " << grad_dsdgradu(i,j) << std::endl;
        SANS_CHECK_CLOSE( pingmat(i,j), grad_dsdgradu(i,j), small_tol, ping_tol );
      }
    }

  }
#else
  MatrixQ grad_dsdgradu = 0;
  BOOST_CHECK_THROW(pde.jacobianGradientSourceGradient(dist, x, y, z, time,
                                                       q, qx, qy, qz,
                                                       qxx, qxy, qyy,
                                                       qxz, qyz, qzz,
                                                       grad_dsdgradu), DeveloperException);
#endif

  // case 2: prod/wall dominant; S~ branch 2
  if (verbose) cout << "source: case2" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0; chiz = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q[5]  = rho*nt;

  qxS[5].value() = qx[5] = rhox*nt + rho*ntx;
  qyS[5].value() = qy[5] = rhoy*nt + rho*nty;
  qzS[5].value() = qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, z, time, q, qxS, qyS, qzS, src );

  // Compute the hand coded Jacobian
  dsdux=0; dsduy=0; dsduz=0;
  pde.jacobianGradientSource( dist, x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << 0*pde.N+j << ") = " << src(i).deriv(0*pde.N+j) << " | " << dsdux(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(0*pde.N+j), dsdux(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 1*pde.N+j << ") = " << src(i).deriv(1*pde.N+j) << " | " << dsduy(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(1*pde.N+j), dsduy(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 2*pde.N+j << ") = " << src(i).deriv(2*pde.N+j) << " | " << dsduz(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(2*pde.N+j), dsduz(i,j), small_tol, close_tol );
    }


  // case 3: prod/wall ft2 terms dominant
  if (verbose) cout << "source: case3" << endl;

  chi  = 1.241;
  chix = 0; chiy = 0; chiz = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q[5]  = rho*nt;

  qxS[5].value() = qx[5] = rhox*nt + rho*ntx;
  qyS[5].value() = qy[5] = rhoy*nt + rho*nty;
  qzS[5].value() = qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, z, time, q, qxS, qyS, qzS, src );

  // Compute the hand coded Jacobian
  dsdux=0; dsduy=0; dsduz=0;
  pde.jacobianGradientSource( dist, x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << 0*pde.N+j << ") = " << src(i).deriv(0*pde.N+j) << " | " << dsdux(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(0*pde.N+j), dsdux(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 1*pde.N+j << ") = " << src(i).deriv(1*pde.N+j) << " | " << dsduy(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(1*pde.N+j), dsduy(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 2*pde.N+j << ") = " << src(i).deriv(2*pde.N+j) << " | " << dsduz(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(2*pde.N+j), dsduz(i,j), small_tol, close_tol );
    }


  // case 4: nonconservative diffusion dominant
  if (verbose) cout << "source: case4" << endl;

  chi  = 5.241;
  chix = 1.963; chiy = -0.781; chiz = 1.456;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q[5]  = rho*nt;

  qxS[5].value() = qx[5] = rhox*nt + rho*ntx;
  qyS[5].value() = qy[5] = rhoy*nt + rho*nty;
  qzS[5].value() = qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, z, time, q, qxS, qyS, qzS, src );

  // Compute the hand coded Jacobian
  dsdux=0; dsduy=0; dsduz=0;
  pde.jacobianGradientSource( dist, x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << 0*pde.N+j << ") = " << src(i).deriv(0*pde.N+j) << " | " << dsdux(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(0*pde.N+j), dsdux(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 1*pde.N+j << ") = " << src(i).deriv(1*pde.N+j) << " | " << dsduy(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(1*pde.N+j), dsduy(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 2*pde.N+j << ") = " << src(i).deriv(2*pde.N+j) << " | " << dsduz(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(2*pde.N+j), dsduz(i,j), small_tol, close_tol );
    }


  // case 5: negative SA; prod/wall dominant
  if (verbose) cout << "source: case5" << endl;

  chi  = -5.241;
  chix = 0; chiy = 0; chiz = 0;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q[5]  = rho*nt;

  qxS[5].value() = qx[5] = rhox*nt + rho*ntx;
  qyS[5].value() = qy[5] = rhoy*nt + rho*nty;
  qzS[5].value() = qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, z, time, q, qxS, qyS, qzS, src );

  // Compute the hand coded Jacobian
  dsdux=0; dsduy=0; dsduz=0;
  pde.jacobianGradientSource( dist, x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << 0*pde.N+j << ") = " << src(i).deriv(0*pde.N+j) << " | " << dsdux(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(0*pde.N+j), dsdux(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 1*pde.N+j << ") = " << src(i).deriv(1*pde.N+j) << " | " << dsduy(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(1*pde.N+j), dsduy(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 2*pde.N+j << ") = " << src(i).deriv(2*pde.N+j) << " | " << dsduz(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(2*pde.N+j), dsduz(i,j), small_tol, close_tol );
    }


  // case 6: negative SA; nonconservative diffusion dominant
  if (verbose) cout << "source: case6" << endl;

  chi  = -5.241;
  chix = 1.963; chiy = -0.781; chiz = 1.456;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q[5]  = rho*nt;

  qxS[5].value() = qx[5] = rhox*nt + rho*ntx;
  qyS[5].value() = qy[5] = rhoy*nt + rho*nty;
  qzS[5].value() = qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, z, time, q, qxS, qyS, qzS, src );

  // Compute the hand coded Jacobian
  dsdux=0; dsduy=0; dsduz=0;
  pde.jacobianGradientSource( dist, x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << 0*pde.N+j << ") = " << src(i).deriv(0*pde.N+j) << " | " << dsdux(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(0*pde.N+j), dsdux(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 1*pde.N+j << ") = " << src(i).deriv(1*pde.N+j) << " | " << dsduy(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(1*pde.N+j), dsduy(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 2*pde.N+j << ") = " << src(i).deriv(2*pde.N+j) << " | " << dsduz(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(2*pde.N+j), dsduz(i,j), small_tol, close_tol );
    }


  // case 7: negative SA; shrmod < -cv2_*shr0
  if (verbose) cout << "source: case7" << endl;

  chi  = 10.241;
  chix = 1.963; chiy = -0.781; chiz = 1.456;
  dist = 0.1;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q[5]  = rho*nt;

  qxS[5].value() = qx[5] = rhox*nt + rho*ntx;
  qyS[5].value() = qy[5] = rhoy*nt + rho*nty;
  qzS[5].value() = qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, z, time, q, qxS, qyS, qzS, src );

  // Compute the hand coded Jacobian
  dsdux=0; dsduy=0; dsduz=0;
  pde.jacobianGradientSource( dist, x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << 0*pde.N+j << ") = " << src(i).deriv(0*pde.N+j) << " | " << dsdux(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(0*pde.N+j), dsdux(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 1*pde.N+j << ") = " << src(i).deriv(1*pde.N+j) << " | " << dsduy(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(1*pde.N+j), dsduy(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 2*pde.N+j << ") = " << src(i).deriv(2*pde.N+j) << " | " << dsduz(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(2*pde.N+j), dsduz(i,j), small_tol, close_tol );
    }


  // case 8: shr0 == 0
  if (verbose) cout << "source: case7" << endl;

  chi  = 5.241;
  chix = 1.963; chiy = -0.781; chiz = 1.456;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q[5]  = rho*nt;

  qxS[1].value() = qx[1] = 0;
  qyS[1].value() = qy[1] = 0;
  qzS[1].value() = qz[1] = 0;

  qxS[2].value() = qx[2] = 0;
  qyS[2].value() = qy[2] = 0;
  qzS[2].value() = qz[2] = 0;

  qxS[3].value() = qx[3] = 0;
  qyS[3].value() = qy[3] = 0;
  qzS[3].value() = qz[3] = 0;

  qxS[5].value() = qx[5] = rhox*nt + rho*ntx;
  qyS[5].value() = qy[5] = rhoy*nt + rho*nty;
  qzS[5].value() = qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, z, time, q, qxS, qyS, qzS, src );

  // Compute the hand coded Jacobian
  dsdux=0; dsduy=0; dsduz=0;
  pde.jacobianGradientSource( dist, x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << 0*pde.N+j << ") = " << src(i).deriv(0*pde.N+j) << " | " << dsdux(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(0*pde.N+j), dsdux(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 1*pde.N+j << ") = " << src(i).deriv(1*pde.N+j) << " | " << dsduy(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(1*pde.N+j), dsduy(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 2*pde.N+j << ") = " << src(i).deriv(2*pde.N+j) << " | " << dsduz(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(2*pde.N+j), dsduz(i,j), small_tol, close_tol );
    }
}

#ifndef SANS_SA_QCR
//----------------------------------------------------------------------------//
// NOTE: 2D case (w = 0; all d/dz are zero)
BOOST_AUTO_TEST_CASE( StrongFlux_xy )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  //typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real close_tol = 1.e-10;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real tSuth = 110./300.; //somewhat random...
  const Real tRef  = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, z, time;
  Real rho, u, v, w, p, nt;
  Real rhox, ux, vx, wx, px, ntx;
  Real rhoy, uy, vy, wy, py, nty;
  Real rhoz, uz, vz, wz, pz, ntz;
  Real rhoxx, uxx, vxx, wxx, pxx, ntxx;
  Real rhoxy, uxy, vxy, wxy, pxy, ntxy;
  Real rhoyy, uyy, vyy, wyy, pyy, ntyy;
  Real rhoxz, uxz, vxz, wxz, pxz, ntxz;
  Real rhoyz, uyz, vyz, wyz, pyz, ntyz;
  Real rhozz, uzz, vzz, wzz, pzz, ntzz;

  x = 0.73; y = 36./99.; z = 1; time = 0;   // not actually used in functions

  rho = 1.0074365505202103681442524417731;
  u = 0.10508967589710564917176487424421;
  v = 0.024335124272671981161838412026241;
  p = 1.0005144026446280991735537190083;
  nt = 5.0876529752066115702479338842975;

  rhox = 0.010748222028549962434259954921112;
  ux = 0.016369696969696969696969696969697;
  vx = 0.013539393939393939393939393939394;
  px = 0.0021139834710743801652892561983471;
  ntx = 0.20391735537190082644628099173554;

  rhoy = 0.0011265417917355371900826446280992;
  uy = 0.003044338055883510428964974419520;
  vy = 0.018728366999391792780222532288648;
  py = 0.0028292145454545454545454545454545;
  nty = 0.18899636363636363636363636363636;

  rhoxx = 0.0030748850488354620586025544703231;
  uxx = 0.022424242424242424242424242424242;
  vxx = 0.0048484848484848484848484848484848;
  pxx = 0.0057917355371900826446280991735537;
  ntxx = 0.27933884297520661157024793388430;

  rhoxy = 0.0061728317355371900826446280991736;
  uxy = 0.0048666666666666666666666666666667;
  vxy = 0.0097333333333333333333333333333333;
  pxy = 0.011626909090909090909090909090909;
  ntxy = 0.31854545454545454545454545454545;

  rhoyy = 0.0061959798545454545454545454545455;
  uyy = 0.030545454545454545454545454545455;
  vyy = -0.0068004722550177095631641086186541;
  pyy = 0.0077803400000000000000000000000000;
  ntyy = 0.5197400000000000000000000000000;

  w = 0; wx = 0; wy = 0; wz = 0;
  wxx = 0; wxy = 0; wyy = 0; wxz = 0; wyz = 0; wzz = 0;
  rhoz = 0; uz = 0; vz = 0; pz = 0; ntz = 0;
  rhoxz = 0; uxz = 0; vxz = 0; pxz = 0; ntxz = 0;
  rhoyz = 0; uyz = 0; vyz = 0; pyz = 0; ntyz = 0;
  rhozz = 0; uzz = 0; vzz = 0; pzz = 0; ntzz = 0;

  Real dist = 60;

  // set
  ArrayQ q;
  pde.setDOFFrom( q, SAnt3D<DensityVelocityPressure3D<Real>>({rho, u, v, w, p, nt }) );

  ArrayQ qx = {rhox, ux, vx, wx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, wy, py, nty};
  ArrayQ qz = {rhoz, uz, vz, wz, pz, ntz};
  ArrayQ qxx = {rhoxx, uxx, vxx, wxx, pxx, ntxx};
  ArrayQ qxy = {rhoxy, uxy, vxy, wxy, pxy, ntxy};
  ArrayQ qyy = {rhoyy, uyy, vyy, wyy, pyy, ntyy};
  ArrayQ qxz = {rhoxz, uxz, vxz, wxz, pxz, ntxz};
  ArrayQ qyz = {rhoyz, uyz, vyz, wyz, pyz, ntyz};
  ArrayQ qzz = {rhozz, uzz, vzz, wzz, pzz, ntzz};

  // Strong Conservative Flux (PRIMITIVE SPECIFIC)
  Real rhot = -0.9;
  Real ut = -0.25;
  Real vt = 0.14;
  Real pt = 0.23;
  Real t  = p/(R*rho);
  Real tt = (t/p) * pt - (t/rho)*rhot;
  Real ntt = 3.14;
  Real wt = 0;

  Real e0 = Cv*t + 0.5*(u*u + v*v + w*w);
  Real e0t = Cv*tt + u*ut + v*vt + w*wt;

  ArrayQ qt = {rhot, ut, vt, wt, pt, ntt};
  ArrayQ utCons, utConsTrue;
  utCons = 0;
  pde.strongFluxAdvectiveTime( x, y, z, time, q, qt, utCons );
  utConsTrue[0] = rhot;
  utConsTrue[1] = rho*ut + rhot*u;
  utConsTrue[2] = rho*vt + rhot*v;
  utConsTrue[3] = rho*wt + rhot*w;
  utConsTrue[4] = rho*e0t + rhot*e0;
  utConsTrue[5] = rho*ntt + rhot*nt;
  BOOST_CHECK_CLOSE( utConsTrue(0), utCons(0), close_tol );
  BOOST_CHECK_CLOSE( utConsTrue(1), utCons(1), close_tol );
  BOOST_CHECK_CLOSE( utConsTrue(2), utCons(2), close_tol );
  BOOST_CHECK_CLOSE( utConsTrue(3), utCons(3), close_tol );
  BOOST_CHECK_CLOSE( utConsTrue(4), utCons(4), close_tol );
  BOOST_CHECK_CLOSE( utConsTrue(5), utCons(5), close_tol );

  ArrayQ Fadv = 0; ArrayQ FadvTrue;
  ArrayQ Gadv = 0; ArrayQ GadvTrue;
  ArrayQ Hadv = 0; ArrayQ HadvTrue;
  pde.fluxAdvective(x, y, z, time, q, Fadv, Gadv, Hadv);

  FadvTrue[0] = 0.10587118058106700917370666456360;
  FadvTrue[1] = 1.0116403706987363765045998373284;
  FadvTrue[2] = 0.0025763883363347622775335854515046;
  FadvTrue[3] = 0;
  FadvTrue[4] = 0.3686190306186793850142013748494;
  FadvTrue[5] = 0.5386358268719020087608178238215;

  GadvTrue[0] = 0.024516093653741303940070306229868;
  GadvTrue[1] = 0.0025763883363347622775335854515046;
  GadvTrue[2] = 1.0011110048303723586957718335520;
  GadvTrue[3] = 0;
  GadvTrue[4] = 0.08535938323913415860950277947394;
  GadvTrue[5] = 0.12472937681790087347722281270783;

  HadvTrue[0] = 0;
  HadvTrue[1] = 0;
  HadvTrue[2] = 0;
  HadvTrue[3] = 1.0005144026446280991735537190083;    // p
  HadvTrue[4] = 0;
  HadvTrue[5] = 0;

  for (int i=0; i<6; i++)
  {
    BOOST_CHECK_CLOSE(Fadv(i), FadvTrue(i), close_tol);
    BOOST_CHECK_CLOSE(Gadv(i), GadvTrue(i), close_tol);
    BOOST_CHECK_CLOSE(Hadv(i), HadvTrue(i), close_tol);
    //std::cout << "btest: Hadv(" << i << ") = " << Hadv(i) << std::endl;
  }

  ArrayQ strongAdv = 0;
  ArrayQ strongAdvTrue = 0;
  pde.strongFluxAdvective(dist, x, y, z, time, q, qx, qy, qz, strongAdv);

  strongAdvTrue(0) = 0.03651601419890715612108675424023;
  strongAdvTrue(1) = 0.007759153989119463332939283278032;
  strongAdvTrue(2) = 0.005610414308982112887949627535613;
  strongAdvTrue(3) = 0;
  strongAdvTrue(4) = 0.1243734188228572701745553762010;
  strongAdvTrue(5) = 0.21200323198707463105895095938310;

  BOOST_CHECK_CLOSE(strongAdvTrue(0), strongAdv(0), close_tol);
  BOOST_CHECK_CLOSE(strongAdvTrue(1), strongAdv(1), close_tol);
  BOOST_CHECK_CLOSE(strongAdvTrue(2), strongAdv(2), close_tol);
  BOOST_CHECK_CLOSE(strongAdvTrue(3), strongAdv(3), close_tol);
  BOOST_CHECK_CLOSE(strongAdvTrue(4), strongAdv(4), close_tol);
  BOOST_CHECK_CLOSE(strongAdvTrue(5), strongAdv(5), close_tol);

  ArrayQ Fvisc = 0;  ArrayQ FviscTrue;
  ArrayQ Gvisc = 0;  ArrayQ GviscTrue;
  ArrayQ Hvisc = 0;  ArrayQ HviscTrue;

  FviscTrue(0) = 0;
  FviscTrue(1) = -0.0225306858220465230739517769814;
  FviscTrue(2) = -0.04000165622505323194410409329417;
  FviscTrue(3) = 0;
  FviscTrue(4) =  0.0845833783703484013428985020496;
  FviscTrue(5) = -1.872022413362562239862325433058;

  GviscTrue(0) = 0;
  GviscTrue(1) = -0.04000165622505323194410409329417;
  GviscTrue(2) = -0.0339093920991446656273492501600;
  GviscTrue(3) = 0;
  GviscTrue(4) = -0.02259677129049221380087236976276;
  GviscTrue(5) = -1.7350432390908063684860979535301;

  HviscTrue(0) = 0;
  HviscTrue(1) = 0;
  HviscTrue(2) = 0;
  HviscTrue(3) = 0.056440077921191184;    // lambda*(ux + vy)
  HviscTrue(4) = 0;
  HviscTrue(5) = 0;

  pde.fluxViscous(x, y, z, time, q, qx, qy, qz, Fvisc, Gvisc, Hvisc);

  for (int i=0; i<6; i++)
  {
    BOOST_CHECK_CLOSE(Fvisc(i), FviscTrue(i), close_tol);
    BOOST_CHECK_CLOSE(Gvisc(i), GviscTrue(i), close_tol);
    BOOST_CHECK_CLOSE(Hvisc(i), HviscTrue(i), close_tol);
    //std::cout << "btest: Hvisc(" << i << ") = " << std::setprecision(15) << Hvisc(i) << std::endl;
  }

  ArrayQ strongVisc = 0;
  ArrayQ strongViscTrue;

  strongViscTrue(0) = 0;
  strongViscTrue(1) = -0.1586885687759146272058388151894;
  strongViscTrue(2) = -0.0001249081136479725918195084228;
  strongViscTrue(3) = 0;
  strongViscTrue(4) = -0.0582007392120158256116833654119;
  strongViscTrue(5) = -7.469316914316503527742653726732;

  pde.strongFluxViscous(dist, x, y, z, time, q, qx, qy, qz, qxx, qxy, qyy, qxz, qyz, qzz, strongVisc);

  BOOST_CHECK_CLOSE(strongViscTrue(0), strongVisc(0), close_tol);
  BOOST_CHECK_CLOSE(strongViscTrue(1), strongVisc(1), close_tol);
  BOOST_CHECK_CLOSE(strongViscTrue(2), strongVisc(2), close_tol);
  BOOST_CHECK_CLOSE(strongViscTrue(3), strongVisc(3), close_tol);
  BOOST_CHECK_CLOSE(strongViscTrue(4), strongVisc(4), close_tol);
  BOOST_CHECK_CLOSE(strongViscTrue(5), strongVisc(5), close_tol);
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jacobianFluxViscous )
{

  // Use conservative variables here in order to differentiate with Surreal
  typedef QTypeConservative QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef SurrealS<PDEClass::N> SurrealClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;
  typedef PDEClass::ArrayQ<SurrealClass> ArrayQSurreal;

  bool verbose = false;

  const Real small_tol = 1.e-10;
  const Real close_tol = 1.e-10;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real tSuth = 110./300.; //somewhat random...
  const Real tRef  = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, z, time;
  Real rho, u, v, w, t;

  x = 0; y = 0; z = 0; time = 0;   // not actually used in functions
  rho = 1.225;
  u = 6.974;
  v = -3.231;
  w = 0.433;
  t = 2.8815;

  Real e = gas.energy(rho, t);
  Real E = e + 0.5*(u*u + v*v + w*w);

  Real rhox = 0.0421, rhoy = 0.0127, rhoz = 0.0532;
  Real ux = 0.0521, uy = -0.0124, uz = 0.0121;
  Real vx = -0.0312, vy = -0.0022, vz = 0.432;
  Real wx = -0.435, wy = -0.0235, wz = 0.0035;
  Real tx = -0.0715, ty = -0.0111, tz = -0.1102;

  //Real px = R*(rho*tx + rhox*t);
  //Real py = R*(rho*ty + rhoy*t);

  Real rhoux = rhox*u + ux*rho;
  Real rhouy = rhoy*u + uy*rho;
  Real rhouz = rhoz*u + uz*rho;

  Real rhovx = rhox*v + vx*rho;
  Real rhovy = rhoy*v + vy*rho;
  Real rhovz = rhoz*v + vz*rho;

  Real rhowx = rhox*w + wx*rho;
  Real rhowy = rhoy*w + wy*rho;
  Real rhowz = rhoz*w + wz*rho;

  Real ex, ey, ez;
  gas.energyGradient(rho, t, rhox, tx, ex);
  gas.energyGradient(rho, t, rhoy, ty, ey);
  gas.energyGradient(rho, t, rhoz, tz, ez);

  Real Ex = ex + ux*u + vx*v + wz*w;
  Real Ey = ey + uy*u + vy*v + wz*w;
  Real Ez = ez + uz*u + vz*v + wz*w;

  Real rhoEx = rhox*E + rho*Ex;
  Real rhoEy = rhoy*E + rho*Ey;
  Real rhoEz = rhoz*E + rho*Ez;

  Real nt, ntx, nty, ntz;
  Real chi, chix, chiy, chiz;
  Real dist;

  ArrayQ q, qx, qy, qz;
  MatrixQ dfdu;
  MatrixQ dgdu;
  MatrixQ dhdu;
  ArrayQSurreal qS, qxS, qyS, qzS;
  ArrayQSurreal fv;
  ArrayQSurreal gv;
  ArrayQSurreal hv;

  SurrealClass rhoS  = rho;
  SurrealClass rhouS = rho*u;
  SurrealClass rhovS = rho*v;
  SurrealClass rhowS = rho*w;
  SurrealClass rhoES = rho*E;
  SurrealClass rhontS = 0;

  rhoS.deriv(0)   = 1;
  rhouS.deriv(1)  = 1;
  rhovS.deriv(2)  = 1;
  rhowS.deriv(3)  = 1;
  rhoES.deriv(4)  = 1;
  rhontS.deriv(5) = 1;

  // set the complete state vectors (sans nt)
  // the nt values are set for each case
  pde.setDOFFrom( q, SAnt3D<Conservative3D<Real>>        ({rho , rho*u, rho*v, rho*w, rho*E, 0     }));
  pde.setDOFFrom(qS, SAnt3D<Conservative3D<SurrealClass>>({rhoS, rhouS, rhovS, rhowS, rhoES, rhontS}));

  qxS = qx = {rhox, rhoux, rhovx, rhowx, rhoEx, 0};
  qyS = qy = {rhoy, rhouy, rhovy, rhowy, rhoEy, 0};
  qzS = qz = {rhoz, rhouz, rhovz, rhowz, rhoEz, 0};


  // case 1: prod/wall dominant; S~ branch 1
  if (verbose) cout << "source: case1" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0, chiz = 0;
  dist = 60;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  qS[5].value() = q[5]  = rho*nt;

  qxS[5] = qx[5] = rhox*nt + rho*ntx;
  qyS[5] = qy[5] = rhoy*nt + rho*nty;
  qzS[5] = qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  fv = 0;
  gv = 0;
  hv = 0;
  pde.fluxViscous( dist, x, y, z, time, qS, qxS, qyS, qzS, fv, gv, hv );

  // Compute the hand coded Jacobian
  dfdu = 0;
  dgdu = 0;
  dhdu = 0;
  pde.jacobianFluxViscous( dist, x, y, z, time, q, qx, qy, qz, dfdu, dgdu, dhdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " fv(" << i << ").deriv(" << j << ") = " << fv(i).deriv(j) << " | " << dfdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( fv(i).deriv(j), dfdu(i,j), small_tol, close_tol );
      //
      if (verbose) std::cout << " gv(" << i << ").deriv(" << j << ") = " << gv(i).deriv(j) << " | " << dgdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( gv(i).deriv(j), dgdu(i,j), small_tol, close_tol );
      //
      if (verbose) std::cout << " hv(" << i << ").deriv(" << j << ") = " << hv(i).deriv(j) << " | " << dhdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( hv(i).deriv(j), dhdu(i,j), small_tol, close_tol );
    }


  // case 2: prod/wall dominant; S~ branch 2
  if (verbose) cout << "source: case2" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0, chiz = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  qS[5].value() = q[5]  = rho*nt;

  qxS[5] = qx[5] = rhox*nt + rho*ntx;
  qyS[5] = qy[5] = rhoy*nt + rho*nty;
  qzS[5] = qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  fv = 0;
  gv = 0;
  hv = 0;
  pde.fluxViscous( dist, x, y, z, time, qS, qxS, qyS, qzS, fv, gv, hv );

  // Compute the hand coded Jacobian
  dfdu = 0;
  dgdu = 0;
  dhdu = 0;
  pde.jacobianFluxViscous( dist, x, y, z, time, q, qx, qy, qz, dfdu, dgdu, dhdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " fv(" << i << ").deriv(" << j << ") = " << fv(i).deriv(j) << " | " << dfdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( fv(i).deriv(j), dfdu(i,j), small_tol, close_tol );
      //
      if (verbose) std::cout << " gv(" << i << ").deriv(" << j << ") = " << gv(i).deriv(j) << " | " << dgdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( gv(i).deriv(j), dgdu(i,j), small_tol, close_tol );
      //
      if (verbose) std::cout << " hv(" << i << ").deriv(" << j << ") = " << hv(i).deriv(j) << " | " << dhdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( hv(i).deriv(j), dhdu(i,j), small_tol, close_tol );
    }


  // case 3: prod/wall ft2 terms dominant
  if (verbose) cout << "source: case3" << endl;

  chi  = 1.241;
  chix = 0; chiy = 0, chiz = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  qS[5].value() = q[5]  = rho*nt;

  qxS[5] = qx[5] = rhox*nt + rho*ntx;
  qyS[5] = qy[5] = rhoy*nt + rho*nty;
  qzS[5] = qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  fv = 0;
  gv = 0;
  hv = 0;
  pde.fluxViscous( dist, x, y, z, time, qS, qxS, qyS, qzS, fv, gv, hv );

  // Compute the hand coded Jacobian
  dfdu = 0;
  dgdu = 0;
  dhdu = 0;
  pde.jacobianFluxViscous( dist, x, y, z, time, q, qx, qy, qz, dfdu, dgdu, dhdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " fv(" << i << ").deriv(" << j << ") = " << fv(i).deriv(j) << " | " << dfdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( fv(i).deriv(j), dfdu(i,j), small_tol, close_tol );
      //
      if (verbose) std::cout << " gv(" << i << ").deriv(" << j << ") = " << gv(i).deriv(j) << " | " << dgdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( gv(i).deriv(j), dgdu(i,j), small_tol, close_tol );
      //
      if (verbose) std::cout << " hv(" << i << ").deriv(" << j << ") = " << hv(i).deriv(j) << " | " << dhdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( hv(i).deriv(j), dhdu(i,j), small_tol, close_tol );
    }


  // case 4: nonconservative diffusion dominant
  if (verbose) cout << "source: case4" << endl;

  chi  = 5.241;
  chix = 1.963; chiy = -0.781;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  qS[5].value() = q[5]  = rho*nt;

  qxS[5] = qx[5] = rhox*nt + rho*ntx;
  qyS[5] = qy[5] = rhoy*nt + rho*nty;
  qzS[5] = qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  fv = 0;
  gv = 0;
  hv = 0;
  pde.fluxViscous( dist, x, y, z, time, qS, qxS, qyS, qzS, fv, gv, hv );

  // Compute the hand coded Jacobian
  dfdu = 0;
  dgdu = 0;
  dhdu = 0;
  pde.jacobianFluxViscous( dist, x, y, z, time, q, qx, qy, qz, dfdu, dgdu, dhdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " fv(" << i << ").deriv(" << j << ") = " << fv(i).deriv(j) << " | " << dfdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( fv(i).deriv(j), dfdu(i,j), small_tol, close_tol );
      //
      if (verbose) std::cout << " gv(" << i << ").deriv(" << j << ") = " << gv(i).deriv(j) << " | " << dgdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( gv(i).deriv(j), dgdu(i,j), small_tol, close_tol );
      //
      if (verbose) std::cout << " hv(" << i << ").deriv(" << j << ") = " << hv(i).deriv(j) << " | " << dhdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( hv(i).deriv(j), dhdu(i,j), small_tol, close_tol );
    }

  // case 5: negative SA; prod/wall dominant
  if (verbose) cout << "source: case5" << endl;

  chi  = -5.241;
  chix = 0; chiy = 0, chiz = 0;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  qS[5].value() = q[5]  = rho*nt;

  qxS[5] = qx[5] = rhox*nt + rho*ntx;
  qyS[5] = qy[5] = rhoy*nt + rho*nty;
  qzS[5] = qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  fv = 0;
  gv = 0;
  hv = 0;
  pde.fluxViscous( dist, x, y, z, time, qS, qxS, qyS, qzS, fv, gv, hv );

  // Compute the hand coded Jacobian
  dfdu = 0;
  dgdu = 0;
  dhdu = 0;
  pde.jacobianFluxViscous( dist, x, y, z, time, q, qx, qy, qz, dfdu, dgdu, dhdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " fv(" << i << ").deriv(" << j << ") = " << fv(i).deriv(j) << " | " << dfdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( fv(i).deriv(j), dfdu(i,j), small_tol, close_tol );
      //
      if (verbose) std::cout << " gv(" << i << ").deriv(" << j << ") = " << gv(i).deriv(j) << " | " << dgdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( gv(i).deriv(j), dgdu(i,j), small_tol, close_tol );
      //
      if (verbose) std::cout << " hv(" << i << ").deriv(" << j << ") = " << hv(i).deriv(j) << " | " << dhdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( hv(i).deriv(j), dhdu(i,j), small_tol, close_tol );
    }


  // case 6: negative SA; nonconservative diffusion dominant
  if (verbose) cout << "source: case6" << endl;

  chi  = -5.241;
  chix = 1.963; chiy = -0.781, chiz = -0.103;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  qS[5].value() = q[5]  = rho*nt;

  qxS[5] = qx[5] = rhox*nt + rho*ntx;
  qyS[5] = qy[5] = rhoy*nt + rho*nty;
  qzS[5] = qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  fv = 0;
  gv = 0;
  hv = 0;
  pde.fluxViscous( dist, x, y, z, time, qS, qxS, qyS, qzS, fv, gv, hv );

  // Compute the hand coded Jacobian
  dfdu = 0;
  dgdu = 0;
  dhdu = 0;
  pde.jacobianFluxViscous( dist, x, y, z, time, q, qx, qy, qz, dfdu, dgdu, dhdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " fv(" << i << ").deriv(" << j << ") = " << fv(i).deriv(j) << " | " << dfdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( fv(i).deriv(j), dfdu(i,j), small_tol, close_tol );
      //
      if (verbose) std::cout << " gv(" << i << ").deriv(" << j << ") = " << gv(i).deriv(j) << " | " << dgdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( gv(i).deriv(j), dgdu(i,j), small_tol, close_tol );
      //
      if (verbose) std::cout << " hv(" << i << ").deriv(" << j << ") = " << hv(i).deriv(j) << " | " << dhdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( hv(i).deriv(j), dhdu(i,j), small_tol, close_tol );
    }


  // case 7: negative SA; shrmod < -cv2_*shr0
  if (verbose) cout << "source: case7" << endl;

  chi  = 10.241;
  chix = 1.963; chiy = -0.781, chiz = -0.103;
  dist = 0.1;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  qS[5].value() = q[5]  = rho*nt;

  qxS[5] = qx[5] = rhox*nt + rho*ntx;
  qyS[5] = qy[5] = rhoy*nt + rho*nty;
  qzS[5] = qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  fv = 0;
  gv = 0;
  hv = 0;
  pde.fluxViscous( dist, x, y, z, time, qS, qxS, qyS, qzS, fv, gv, hv );

  // Compute the hand coded Jacobian
  dfdu = 0;
  dgdu = 0;
  dhdu = 0;
  pde.jacobianFluxViscous( dist, x, y, z, time, q, qx, qy, qz, dfdu, dgdu, dhdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " fv(" << i << ").deriv(" << j << ") = " << fv(i).deriv(j) << " | " << dfdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( fv(i).deriv(j), dfdu(i,j), small_tol, close_tol );
      //
      if (verbose) std::cout << " gv(" << i << ").deriv(" << j << ") = " << gv(i).deriv(j) << " | " << dgdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( gv(i).deriv(j), dgdu(i,j), small_tol, close_tol );
      //
      if (verbose) std::cout << " hv(" << i << ").deriv(" << j << ") = " << hv(i).deriv(j) << " | " << dhdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( hv(i).deriv(j), dhdu(i,j), small_tol, close_tol );
    }


  // case 8: shr0 == 0
  if (verbose) cout << "source: case8" << endl;

  chi  = 5.241;
  chix = 1.963; chiy = -0.781, chiz = -0.103;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  qS[5].value() = q[5]  = rho*nt;

  qxS[5] = qx[5] = rhox*nt + rho*ntx;
  qyS[5] = qy[5] = rhoy*nt + rho*nty;
  qzS[5] = qz[5] = rhoz*nt + rho*ntz;

  // Compute the true jacobian with Surreals
  fv = 0;
  gv = 0;
  hv = 0;
  pde.fluxViscous( dist, x, y, z, time, qS, qxS, qyS, qzS, fv, gv, hv );

  // Compute the hand coded Jacobian
  dfdu = 0;
  dgdu = 0;
  dhdu = 0;
  pde.jacobianFluxViscous( dist, x, y, z, time, q, qx, qy, qz, dfdu, dgdu, dhdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " fv(" << i << ").deriv(" << j << ") = " << fv(i).deriv(j) << " | " << dfdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( fv(i).deriv(j), dfdu(i,j), small_tol, close_tol );
      //
      if (verbose) std::cout << " gv(" << i << ").deriv(" << j << ") = " << gv(i).deriv(j) << " | " << dgdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( gv(i).deriv(j), dgdu(i,j), small_tol, close_tol );
      //
      if (verbose) std::cout << " hv(" << i << ").deriv(" << j << ") = " << hv(i).deriv(j) << " | " << dhdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( hv(i).deriv(j), dhdu(i,j), small_tol, close_tol );
    }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReferenceEquivalency )
{

  // Use conservative variables here in order to differentiate with Surreal
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  bool verbose = false;

  const Real small_tol = 1.e-10;
  const Real close_tol = 1.e-10;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real tSuth = 110./300.; //somewhat random...
  const Real tRef  = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  const Real ntRef = 0.001;
  PDEClass pde2(gas, visc, tcond, Euler_ResidInterp_Raw, eVanLeer, ntRef );

  Real x, y, z, time;
  Real rho, u, v, w, t;

  x = 0; y = 0; z = 0; time = 0;   // not actually used in functions
  rho = 1.225;
  u = 6.974;
  v = -3.231;
  w = 0.433;
  t = 2.8815;

  Real p = gas.pressure(rho, t);
//  Real e = gas.energy(rho, t);
//  Real E = e + 0.5*(u*u + v*v + w*w);

  Real rhox = 0.0421, rhoy = 0.0127, rhoz = 0.0532;
  Real ux = 0.0521, uy = -0.0124, uz = 0.0121;
  Real vx = -0.0312, vy = -0.0022, vz = 0.432;
  Real wx = -0.435, wy = -0.0235, wz = 0.0035;
  Real tx = -0.0715, ty = -0.0111, tz = -0.1102;

  Real px = R*(rho*tx + rhox*t);
  Real py = R*(rho*ty + rhoy*t);
  Real pz = R*(rho*tz + rhoz*t);

  Real nt, ntx, nty, ntz;
  Real chi, chix, chiy, chiz;
  Real dist;

  ArrayQ q, qx, qy, qz;
  ArrayQ q2, qx2, qy2, qz2;

  // set the complete state vectors (sans nt)
  // the nt values are set for each case
  pde.setDOFFrom( q, SAnt3D<DensityVelocityPressure3D<Real>>({rho , u, v, w, p, 0     }));
  pde2.setDOFFrom( q2, SAnt3D<DensityVelocityPressure3D<Real>>({rho , u, v, w, p, 0     }));

  qx = qx2 = {rhox, ux, vx, wx, px, 0};
  qy = qy2 = {rhoy, uy, vy, wy, py, 0};
  qz = qz2 = {rhoz, uz, vz, wz, pz, 0};


  // case 1: prod/wall dominant; S~ branch 1
  if (verbose) cout << "source: case1" << endl;

  chi  = 5.241;
  chix = 0.032; chiy = -0.071, chiz = 0.00876;
  dist = 60;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);
  ntz = chiz*(muRef/rho);

  q[5]  = nt;
  q2[5] = nt/ntRef;

  qx[5] = ntx;
  qy[5] = nty;
  qz[5] = ntz;

  qx2[5] = ntx/ntRef;
  qy2[5] = nty/ntRef;
  qz2[5] = ntz/ntRef;

  //perturbedMasterState
  ArrayQ dq, dq2;
  dq = dq2 = {0.01/7., -0.02/7., 0.03/7., -0.04/7., 0.05/7., -0.06/7. };
  dq2[5] /= ntRef;
  ArrayQ dU = 0, dU2 = 0;

  pde.perturbedMasterState(dist, x, y, z, time, q, dq, dU);
  pde2.perturbedMasterState(dist, x, y, z, time, q2, dq2, dU2);

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( dU[i], dU2[i], small_tol, close_tol );
  }
  SANS_CHECK_CLOSE( dU[5]/ntRef, dU2[5], small_tol, close_tol );

  //fluxes
  ArrayQ fv, gv, hv, fv2, gv2, hv2;
  fv = fv2 = 0;
  gv = gv2 = 0;
  hv = hv2 = 0;

  pde.fluxAdvective( dist, x, y, z, time, q, fv, gv, hv );
  pde2.fluxAdvective( dist, x, y, z, time, q2, fv2, gv2, hv2 );

  pde.fluxViscous( dist, x, y, z, time, q, qx, qy, qz, fv, gv, hv );
  pde2.fluxViscous( dist, x, y, z, time, q2, qx2, qy2, qz2, fv2, gv2, hv2 );

  // check that the jacobian is correct
  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( fv[i], fv2[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( gv[i], gv2[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( hv[i], hv2[i], small_tol, close_tol );
  }
  SANS_CHECK_CLOSE( fv[5]/ntRef, fv2[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( gv[5]/ntRef, gv2[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( hv[5]/ntRef, hv2[5], small_tol, close_tol );

  ArrayQ s = 0, s2 = 0;

  //source
  pde.source(dist, x, y, z, time, q, qx, qy, qz, s);
  pde2.source(dist, x, y, z, time, q2, qx2, qy2, qz2, s2);

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( s[i], s2[i], small_tol, close_tol );
  }
  SANS_CHECK_CLOSE( s[5]/ntRef, s2[5], small_tol, close_tol );

  //jacobianFlux
  MatrixQ dfdu = 0, dgdu = 0, dhdu = 0;
  MatrixQ dfdu2 = 0, dgdu2 = 0, dhdu2 = 0;

  pde.jacobianFluxAdvective(dist, x, y, z, time, q, dfdu, dgdu, dhdu);

  pde2.jacobianFluxAdvective(dist, x, y, z, time, q2, dfdu2, dgdu2, dhdu2);

  ArrayQ dF = dfdu*dU;
  ArrayQ dG = dgdu*dU;
  ArrayQ dH = dhdu*dU;
  ArrayQ dF2 = dfdu2*dU2;
  ArrayQ dG2 = dgdu2*dU2;
  ArrayQ dH2 = dhdu2*dU2;

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( dF[i], dF2[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dG[i], dG2[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dH[i], dH2[i], small_tol, close_tol );
  }
  SANS_CHECK_CLOSE( dF[5]/ntRef, dF2[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( dG[5]/ntRef, dG2[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( dH[5]/ntRef, dH2[5], small_tol, close_tol );

  pde.jacobianFluxViscous(dist, x, y, z, time, q, qx, qy, qz, dfdu, dgdu, dhdu);
  pde2.jacobianFluxViscous(dist, x, y, z, time, q2, qx2, qy2, qz2, dfdu2, dgdu2, dhdu2);

  dF = dfdu*dU;
  dG = dgdu*dU;
  dH = dhdu*dU;
  dF2 = dfdu2*dU2;
  dG2 = dgdu2*dU2;
  dH2 = dhdu2*dU2;

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( dF[i], dF2[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dG[i], dG2[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dH[i], dH2[i], small_tol, close_tol );
  }
  SANS_CHECK_CLOSE( dF[5]/ntRef, dF2[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( dG[5]/ntRef, dG2[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( dH[5]/ntRef, dH2[5], small_tol, close_tol );


  MatrixQ dsdu = 0, dsdu2 = 0;
  ArrayQ du = 0, du2 = 0;

  pde.jacobianSource(dist, x, y, z, time, q, qx, qy, qz, dsdu);
  pde2.jacobianSource(dist, x, y, z, time, q2, qx2, qy2, qz2, dsdu2);


  ArrayQ dS = dsdu*dU;
  ArrayQ dS2 = dsdu2*dU2;

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( dS[i], dS2[i], small_tol, close_tol );
  }
  SANS_CHECK_CLOSE( dS[5]/ntRef, dS2[5], small_tol, close_tol );

}



#if 0       // implemenation & tests not complete
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxRoe )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real tol = 2.e-11;

  const Real gamma = 1.4;
  const Real R     = 0.4;           // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // Roe flux function test (same as Euler Roe test, from Roe3D.nb)

  Real x, y, z, time;
  Real nx, ny;
  Real rhoL, uL, vL, tL, pL, h0L;
  Real rhoR, uR, vR, tR, pR, h0R;

  x = y = time = 0;   // not actually used in functions
  nx = 2.0/7.0; ny = sqrt(1.0-nx*nx);
  rhoL = 1.034; uL = 3.26; vL = -2.17; tL = 5.78;
  rhoR = 0.973; uR = 1.79; vR =  0.93; tR = 6.13;
  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL + vL*vL);
  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR + vR*vR);

  // set
  ArrayQ qL = 0;
  ArrayQ qR = 0;

  pde.setDOFFrom( qL, DensityVelocityTemperature3D<Real>(rhoL, uL, vL, tL) );
  pde.setDOFFrom( qR, DensityVelocityTemperature3D<Real>(rhoR, uR, vR, tR) );

  // advective normal flux (average)
  Real fL[4] = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*vL     , rhoL*uL*h0L};
  Real fR[4] = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*vR     , rhoR*uR*h0R};
  Real gL[4] = {rhoL*vL, rhoL*vL*uL     , rhoL*vL*vL + pL, rhoL*vL*h0L};
  Real gR[4] = {rhoR*vR, rhoR*vR*uR     , rhoR*vR*vR + pR, rhoR*vR*h0R};

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < 4; k++)
    fnTrue[k] = 0.5*(nx*(fL[k] + fR[k]) + ny*(gL[k] + gR[k]));

#if 1
  // Exact values for Roe scheme from Roe3D.nb
  fnTrue[0] -= -0.021574952725013058474459426733768;
  fnTrue[1] -= -1.7958753728220592172800048653566;
  fnTrue[2] -=  4.4515996249161671139371535471951;
  fnTrue[3] -= -6.8094427666218353919034995028459;
#endif

  ArrayQ fn;
  fn = 0;
  pde.fluxAdvectiveUpwind( x, y, z, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( fnTrue(3), fn(3), tol );

  MatrixQ mtx;
  mtx = 0;

  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, qL, nx, ny, mtx );
  MatrixQ mtxTrue = {
    {1.0323434331949190979834072919713,
        -0.44459540501697074517797177104962,
        -0.43697991171460424043588329213258,
        0.080449362812368834954522463171426},
    {-0.72312835498151845646006425483631,
        -0.010364873336226558791267034135421,
        -1.4045718375886762151895973056639,
        0.18933272986886306466125181349454},
    {-0.90833712876943321732424020657867,
        1.9404853199183346620104522800780,
        2.1633849842368432379888624467138,
        -0.41919712890981821676619255996783},
    {-0.43536491993382434782224675355112,
        -8.1758936855015456959218531030353,
        -6.9672110585297722485152708900077,
        2.7090875342447734040794006013828}
                    };
  // Values taken from pde/NS/Roe3D.nb

  for (int i=0; i<4; i++)
    for (int j=0; j<4; j++)
      BOOST_CHECK_CLOSE( mtx(i,j), mtxTrue(i,j), tol );


  // Call these simply to make sure coverage is high
  ArrayQ qxL, qxR, qyL, qyR, sourceL, sourceR;
  Real dummy = 0;
  pde.sourceDualConsistent( dummy, dummy, dummy, qL, qxL, qyL, qR, qxR, qyR, sourceL, sourceR );
}
#endif      // implemenation & tests not complete


#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, z, time, dx, dy;
  Real rho, u, v, w, t, c, nt;
  Real speed, speedTrue;
  Real dist;

  x = 0; y = 0; time = 0;  // not actually used in functions
  dist = 0;
  dx = 0.57; dy = -0.43;
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987; nt = 5.241;
  c = sqrt(gamma*R*t);
  speedTrue = fabs(dx*u + dy*v)/sqrt(dx*dx + dy*dy) + c;

  Real qDataPrim[5] = {rho, u, v, w, t, nt};
  string qNamePrim[5] = {"Density", "VelocityX", "VelocityY", "Temperature", "SANutilde"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );

  pde.speedCharacteristic( dist, x, y, z, time, dx, dy, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  speedTrue = sqrt(u*u + v*v) + c;
  pde.speedCharacteristic( dist, x, y, z, time, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );
}
#endif


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  ArrayQ q;
  q(0) =  1;
  q(4) =  1;
  BOOST_CHECK( pde.isValidState(q) == true );

  q(0) =  1;
  q(4) = -1;
  BOOST_CHECK( pde.isValidState(q) == false );

  q(0) = -1;
  q(4) =  1;
  BOOST_CHECK( pde.isValidState(q) == false );

  q(0) = -1;
  q(4) = -1;
  BOOST_CHECK( pde.isValidState(q) == false );

  q(0) =  1;
  q(4) =  0;
  BOOST_CHECK( pde.isValidState(q) == false );

  q(0) =  0;
  q(4) =  1;
  BOOST_CHECK( pde.isValidState(q) == false );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/PDERANSSA3D_pattern.txt", true  );

  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  pde.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
