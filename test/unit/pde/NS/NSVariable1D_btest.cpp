// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// NSVariable1D_btest
// testing of NSVariable classes that represent a specific set of variables used to set the working variables

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/NS/NSVariable1D.h"

#include <string>
#include <iostream>

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( NSVariable1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( NSVariable1D_test )
{
  const Real tol = 5.0e-12;

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  const Real Cv    = R/(gamma - 1);

  // set/eval
  Real rho, u, t, p, e, E;

  rho = 1.225;          // kg/m^3 (standard atmosphere)
  t   = 288.15;         // K (standard atmosphere)
  u   = 15.03;          // m/s
  p   = R*rho*t;
  e   = Cv*t;
  E   = e + 0.5*(u*u);

  DensityVelocityPressure1D<Real> rvp1(rho, u, p);

  BOOST_CHECK_CLOSE( rho, rvp1.Density, tol );
  BOOST_CHECK_CLOSE( u, rvp1.VelocityX, tol );
  BOOST_CHECK_CLOSE( p, rvp1.Pressure, tol );

  DensityVelocityPressure1D<Real> rvp2 = {rho, u, p};

  BOOST_CHECK_CLOSE( rho, rvp2.Density, tol );
  BOOST_CHECK_CLOSE( u, rvp2.VelocityX, tol );
  BOOST_CHECK_CLOSE( p, rvp2.Pressure, tol );

  PyDict drvp;
  drvp[DensityVelocityPressure1DParams::params.rho] = rho;
  drvp[DensityVelocityPressure1DParams::params.u] = u;
  drvp[DensityVelocityPressure1DParams::params.p] = p;
  DensityVelocityPressure1DParams::checkInputs(drvp);

  DensityVelocityPressure1D<Real> rvp3(drvp);

  BOOST_CHECK_CLOSE( rho, rvp3.Density, tol );
  BOOST_CHECK_CLOSE( u, rvp3.VelocityX, tol );
  BOOST_CHECK_CLOSE( p, rvp3.Pressure, tol );

  DensityVelocityTemperature1D<Real> rvt1(rho, u, t);

  BOOST_CHECK_CLOSE( rho, rvt1.Density, tol );
  BOOST_CHECK_CLOSE( u, rvt1.VelocityX, tol );
  BOOST_CHECK_CLOSE( t, rvt1.Temperature, tol );

  DensityVelocityTemperature1D<Real> rvt2 = {rho, u, t};

  BOOST_CHECK_CLOSE( rho, rvt2.Density, tol );
  BOOST_CHECK_CLOSE( u, rvt2.VelocityX, tol );
  BOOST_CHECK_CLOSE( t, rvt2.Temperature, tol );

  PyDict drvt;
  drvt[DensityVelocityTemperature1DParams::params.rho] = rho;
  drvt[DensityVelocityTemperature1DParams::params.u] = u;
  drvt[DensityVelocityTemperature1DParams::params.t] = t;
  DensityVelocityTemperature1DParams::checkInputs(drvt);

  DensityVelocityTemperature1D<Real> rvt3(drvt);

  BOOST_CHECK_CLOSE( rho, rvt3.Density, tol );
  BOOST_CHECK_CLOSE( u, rvt3.VelocityX, tol );
  BOOST_CHECK_CLOSE( t, rvt3.Temperature, tol );


  Conservative1D<Real> u1(rho, rho*u, rho*E);

  BOOST_CHECK_CLOSE( rho, u1.Density, tol );
  BOOST_CHECK_CLOSE( rho*u, u1.MomentumX, tol );
  BOOST_CHECK_CLOSE( rho*E, u1.TotalEnergy, tol );

  Conservative1D<Real> u2 = {rho, rho*u, rho*E};

  BOOST_CHECK_CLOSE( rho, u2.Density, tol );
  BOOST_CHECK_CLOSE( rho*u, u2.MomentumX, tol );
  BOOST_CHECK_CLOSE( rho*E, u2.TotalEnergy, tol );

  PyDict du;
  du[Conservative1DParams::params.rho] = rho;
  du[Conservative1DParams::params.rhou] = rho*u;
  du[Conservative1DParams::params.rhoE] = rho*E;
  Conservative1DParams::checkInputs(du);

  Conservative1D<Real> u3(du);

  BOOST_CHECK_CLOSE( rho, u3.Density, tol );
  BOOST_CHECK_CLOSE( rho*u, u3.MomentumX, tol );
  BOOST_CHECK_CLOSE( rho*E, u3.TotalEnergy, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
