// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// QRANSSA3D_btest
// testing of Q3D<QTypePrimitiveRhoPressure, TraitsSizeRANSSA> class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/NS/QRANSSA3D.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/Q3DConservative.h"
#include "pde/NS/TraitsRANSSA.h"
#include "Surreal/SurrealS.h"

#include <string>
#include <iostream>

//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
template class QRANSSA3D<QTypePrimitiveRhoPressure, TraitsSizeRANSSA>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( QRANSSA3D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test )
{
  const Real tol = 1.e-13;

  typedef QRANSSA3D<QTypePrimitiveRhoPressure, TraitsSizeRANSSA> QInterpret;
  BOOST_CHECK( QInterpret::N == 6 );

  typedef QInterpret::ArrayQ<Real> ArrayQ;
  BOOST_CHECK( ArrayQ::M == 6 );

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  // set/eval
  ArrayQ q;
  Real rho, u, v, w, t, p, nt;
  Real rho1, u1, v1, w1, t1, nt1;
  Real rho2, u2, v2, w2, t2, nt2;

  rho = 1.225;          // kg/m^3 (standard atmosphere)
  t   = 288.15;         // K (standard atmosphere)
  u   = 15.03;          // m/s
  v   =  1.71;
  w   =  10.23;
  p   = R*rho*t;
  nt  = 42.789;

  Real E = gas.energy(rho, t) + 0.5*(u*u + v*v + w*w);

  Real qDataPrim[6] = {rho, u, v, w, t, nt};
  string qNamePrim[6] = {"Density", "VelocityX", "VelocityY", "VelocityZ", "Temperature", "SANutilde"};
  qInterpret.setFromPrimitive( q, qDataPrim, qNamePrim, 6 );
  qInterpret.eval( q, rho1, u1, v1, w1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( w, w1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );

  qInterpret.evalDensity( q, rho1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );

  qInterpret.evalTemperature( q, t1 );
  BOOST_CHECK_CLOSE( t, t1, tol );

  qInterpret.evalVelocity( q, u1, v1, w1 );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( w, w1, tol );

  qInterpret.evalSA( q, nt1 );
  BOOST_CHECK_CLOSE( nt, nt1, tol );


  SAnt3D<DensityVelocityPressure3D<Real>> prim1 = {rho, u, v, w, p, nt};
  q = 1;
  nt1 = 1;
  qInterpret.setFromPrimitive( q, prim1 );
  qInterpret.eval( q, rho1, u1, v1, w1, t1 );
  qInterpret.evalSA( q, nt1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( w, w1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );
  BOOST_CHECK_CLOSE( nt, nt1, tol );

  SAnt3D<DensityVelocityTemperature3D<Real>> prim2 = {rho, u, v, w, t, nt};
  q = 2;
  nt1 = 2;
  qInterpret.setFromPrimitive( q, prim2 );
  qInterpret.eval( q, rho1, u1, v1, w1, t1 );
  qInterpret.evalSA( q, nt1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( w, w1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );
  BOOST_CHECK_CLOSE( nt, nt1, tol );

  SAnt3D<Conservative3D<Real>> prim3 = {rho, rho*u, rho*v, rho*w, rho*E, rho*nt};
  q = 0;
  nt1 = 0;
  qInterpret.setFromPrimitive( q, prim3 );
  qInterpret.eval( q, rho1, u1, v1, w1, t1 );
  qInterpret.evalSA( q, nt1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( w, w1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );
  BOOST_CHECK_CLOSE( nt, nt1, tol );


  // copy constructor
  QInterpret qInterpret2(qInterpret);

  qInterpret2.eval( q, rho2, u2, v2, w2, t2 );
  BOOST_CHECK_CLOSE( rho, rho2, tol );
  BOOST_CHECK_CLOSE( u, u2, tol );
  BOOST_CHECK_CLOSE( v, v2, tol );
  BOOST_CHECK_CLOSE( w, w2, tol );
  BOOST_CHECK_CLOSE( t, t2, tol );

  qInterpret2.evalSA( q, nt2 );
  BOOST_CHECK_CLOSE( nt, nt2, tol );

  // gradient
  Real rhox, ux, vx, wx, tx, px, ntx;
  Real rhoy, uy, vy, wy, ty, py, nty;
  Real rhoz, uz, vz, wz, tz, pz, ntz;
  Real rhox1, ux1, vx1, wx1, tx1, ntx1;
  Real rhoy1, uy1, vy1, wy1, ty1, nty1;
  Real rhoz1, uz1, vz1, wz1, tz1, ntz1;

  rhox = 0.37, ux = -0.85, vx = 0.09, wx =  0.36, px = 1.71; ntx = -3.41;
  rhoy = 2.31, uy =  1.65, vy = 0.87, wy = -1.07, py = 0.29; nty =  8.93;
  rhoz = 0.45, uz =  0.12, vz = 1.27, wz =  0.07, pz = 2.12; ntz =  0.81;
  tx = t*(px/p - rhox/rho);
  ty = t*(py/p - rhoy/rho);
  tz = t*(pz/p - rhoz/rho);

  Real qxData[6] = {rhox, ux, vx, wx, px, ntx};
  Real qyData[6] = {rhoy, uy, vy, wy, py, nty};
  Real qzData[6] = {rhoz, uz, vz, wz, pz, ntz};
  ArrayQ qx(qxData, 6);
  ArrayQ qy(qyData, 6);
  ArrayQ qz(qzData, 6);

  qInterpret.evalGradient( q, qx, rhox1, ux1, vx1, wx1, tx1 );
  qInterpret.evalGradient( q, qy, rhoy1, uy1, vy1, wy1, ty1 );
  qInterpret.evalGradient( q, qz, rhoz1, uz1, vz1, wz1, tz1 );
  BOOST_CHECK_CLOSE( rhox, rhox1, tol );
  BOOST_CHECK_CLOSE(   ux,   ux1, tol );
  BOOST_CHECK_CLOSE(   vx,   vx1, tol );
  BOOST_CHECK_CLOSE(   wx,   wx1, tol );
  BOOST_CHECK_CLOSE(   tx,   tx1, tol );
  BOOST_CHECK_CLOSE( rhoy, rhoy1, tol );
  BOOST_CHECK_CLOSE(   uy,   uy1, tol );
  BOOST_CHECK_CLOSE(   vy,   vy1, tol );
  BOOST_CHECK_CLOSE(   wy,   wy1, tol );
  BOOST_CHECK_CLOSE(   ty,   ty1, tol );
  BOOST_CHECK_CLOSE( rhoz, rhoz1, tol );
  BOOST_CHECK_CLOSE(   uz,   uz1, tol );
  BOOST_CHECK_CLOSE(   vz,   vz1, tol );
  BOOST_CHECK_CLOSE(   wz,   wz1, tol );
  BOOST_CHECK_CLOSE(   tz,   tz1, tol );

  qInterpret.evalSAGradient( q, qx, ntx1 );
  qInterpret.evalSAGradient( q, qy, nty1 );
  qInterpret.evalSAGradient( q, qz, ntz1 );
  BOOST_CHECK_CLOSE( ntx, ntx1, tol );
  BOOST_CHECK_CLOSE( nty, nty1, tol );
  BOOST_CHECK_CLOSE( ntz, ntz1, tol );

  // Hessian
  Real rhoxx = -0.253;
  Real rhoxy =  0.782, rhoyy = 1.08;
  Real rhoxz =  0.152, rhoyz = 0.13, rhozz = 2.15;

  Real uxx =  1.02;
  Real uxy = -0.95, uyy =-0.42;
  Real uxz =  2.31, uyz = 0.34, uzz = 3.14;

  Real vxx = 0.99;
  Real vxy = -0.92, vyy =-0.44;
  Real vxz = -1.62, vyz = 2.32, vzz = 1.15;

  Real wxx =  3.11;
  Real wxy =  0.46, wyy =-2.14;
  Real wxz = -2.13, wyz = 1.78, wzz = -0.76;

  Real pxx = 2.99;
  Real pxy = 0.91, pyy =-2.44;
  Real pxz = 2.38, pyz = 1.88, pzz =-1.98;

  Real ntxx = 17.51;
  Real ntxy = 2.27, ntyy = -3.87;
  Real ntxz = 1.39, ntyz =  5.79, ntzz = -0.87;
  Real ntxx1, ntyy1, ntzz1;

  Real rho3 = rho*rho*rho;

  Real txx = 1.0/R*( -2*px*rhox/(rho*rho) + 2*p*rhox*rhox/rho3 + pxx/rho - p*rhoxx/(rho*rho) );
  Real txy = 1.0/R*( -rhoy*px/(rho*rho) - py*rhox/(rho*rho) + 2*p*rhoy*rhox/rho3 + pxy/rho - p*rhoxy/(rho*rho) );
  Real tyy = 1.0/R*( -2*py*rhoy/(rho*rho) + 2*p*rhoy*rhoy/rho3 + pyy/rho - p*rhoyy/(rho*rho) );

  Real txz = 1.0/R*( -rhoz*px/(rho*rho) - pz*rhox/(rho*rho) + 2*p*rhoz*rhox/rho3 + pxz/rho - p*rhoxz/(rho*rho) );
  Real tyz = 1.0/R*( -rhoz*py/(rho*rho) - pz*rhoy/(rho*rho) + 2*p*rhoz*rhoy/rho3 + pyz/rho - p*rhoyz/(rho*rho) );
  Real tzz = 1.0/R*( -2*pz*rhoz/(rho*rho) + 2*p*rhoz*rhoz/rho3 + pzz/rho - p*rhozz/(rho*rho) );

  ArrayQ qxx = {rhoxx, uxx, vxx, wxx, pxx, ntxx};
  ArrayQ qxy = {rhoxy, uxy, vxy, wxy, pxy, ntxy};
  ArrayQ qyy = {rhoyy, uyy, vyy, wyy, pyy, ntyy};
  ArrayQ qxz = {rhoxz, uxz, vxz, wxz, pxz, ntxz};
  ArrayQ qyz = {rhoyz, uyz, vyz, wyz, pyz, ntyz};
  ArrayQ qzz = {rhozz, uzz, vzz, wzz, pzz, ntzz};

  DLA::MatrixSymS<3, Real> rhoH, uH, vH, wH, tH;

  qInterpret.evalSecondGradient( q, qx, qy, qz,
                                 qxx,
                                 qxy, qyy,
                                 qxz, qyz, qzz,
                                 rhoH, uH, vH, wH, tH );

  BOOST_CHECK_CLOSE( rhoxx, rhoH(0,0), tol );
  BOOST_CHECK_CLOSE( rhoxy, rhoH(1,0), tol );
  BOOST_CHECK_CLOSE( rhoyy, rhoH(1,1), tol );
  BOOST_CHECK_CLOSE( rhoxz, rhoH(2,0), tol );
  BOOST_CHECK_CLOSE( rhoyz, rhoH(2,1), tol );
  BOOST_CHECK_CLOSE( rhozz, rhoH(2,2), tol );

  BOOST_CHECK_CLOSE(   uxx,   uH(0,0), tol );
  BOOST_CHECK_CLOSE(   uxy,   uH(1,0), tol );
  BOOST_CHECK_CLOSE(   uyy,   uH(1,1), tol );
  BOOST_CHECK_CLOSE(   uxz,   uH(2,0), tol );
  BOOST_CHECK_CLOSE(   uyz,   uH(2,1), tol );
  BOOST_CHECK_CLOSE(   uzz,   uH(2,2), tol );

  BOOST_CHECK_CLOSE(   vxx,   vH(0,0), tol );
  BOOST_CHECK_CLOSE(   vxy,   vH(1,0), tol );
  BOOST_CHECK_CLOSE(   vyy,   vH(1,1), tol );
  BOOST_CHECK_CLOSE(   vxz,   vH(2,0), tol );
  BOOST_CHECK_CLOSE(   vyz,   vH(2,1), tol );
  BOOST_CHECK_CLOSE(   vzz,   vH(2,2), tol );

  BOOST_CHECK_CLOSE(   wxx,   wH(0,0), tol );
  BOOST_CHECK_CLOSE(   wxy,   wH(1,0), tol );
  BOOST_CHECK_CLOSE(   wyy,   wH(1,1), tol );
  BOOST_CHECK_CLOSE(   wxz,   wH(2,0), tol );
  BOOST_CHECK_CLOSE(   wyz,   wH(2,1), tol );
  BOOST_CHECK_CLOSE(   wzz,   wH(2,2), tol );

  BOOST_CHECK_CLOSE(   txx,   tH(0,0), tol );
  BOOST_CHECK_CLOSE(   txy,   tH(1,0), tol );
  BOOST_CHECK_CLOSE(   tyy,   tH(1,1), tol );
  BOOST_CHECK_CLOSE(   txz,   tH(2,0), tol );
  BOOST_CHECK_CLOSE(   tyz,   tH(2,1), tol );
  BOOST_CHECK_CLOSE(   tzz,   tH(2,2), tol );

  qInterpret.evalSAHessian( q, qx, qx, qxx, ntxx1 );
  qInterpret.evalSAHessian( q, qy, qy, qyy, ntyy1 );
  qInterpret.evalSAHessian( q, qz, qz, qzz, ntzz1 );
  BOOST_CHECK_CLOSE( ntxx, ntxx1, tol );
  BOOST_CHECK_CLOSE( ntyy, ntyy1, tol );
  BOOST_CHECK_CLOSE( ntzz, ntzz1, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( evalJacobian )
{
  typedef QRANSSA3D<QTypePrimitiveRhoPressure, TraitsSizeRANSSA> QInterpret;

  typedef QInterpret::ArrayQ< Real > ArrayQ;
  typedef QInterpret::ArrayQ< SurrealS<QInterpret::N> > ArrayQSurreal;

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  ArrayQ q=0, rho_q, u_q, v_q, w_q, t_q, nt_q;
  ArrayQSurreal qSurreal;
  SurrealS<QInterpret::N> rho, u, v, w, t, nt;

  SAnt3D<DensityVelocityTemperature3D<Real>> qPrim = {1.225, 15.03, 1.71, 10.23, 288.15, 42.789};
  qInterpret.setFromPrimitive( q, qPrim );

  qInterpret.evalJacobian(q, rho_q, u_q, v_q, w_q, t_q);
  qInterpret.evalSAJacobian(q, nt_q);

  qSurreal = q;

  qSurreal[0].deriv(0) = 1;
  qSurreal[1].deriv(1) = 1;
  qSurreal[2].deriv(2) = 1;
  qSurreal[3].deriv(3) = 1;
  qSurreal[4].deriv(4) = 1;
  qSurreal[5].deriv(5) = 1;

  qInterpret.eval(qSurreal, rho, u, v, w, t);
  qInterpret.evalSA(qSurreal, nt);

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  SANS_CHECK_CLOSE( rho.deriv(0), rho_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(1), rho_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(2), rho_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(3), rho_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(4), rho_q[4], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(5), rho_q[5], small_tol, close_tol )

  SANS_CHECK_CLOSE( u.deriv(0), u_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(1), u_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(2), u_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(3), u_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(4), u_q[4], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(5), u_q[5], small_tol, close_tol )

  SANS_CHECK_CLOSE( v.deriv(0), v_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(1), v_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(2), v_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(3), v_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(4), v_q[4], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(5), v_q[5], small_tol, close_tol )

  SANS_CHECK_CLOSE( w.deriv(0), w_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( w.deriv(1), w_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( w.deriv(2), w_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( w.deriv(3), w_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( w.deriv(4), w_q[4], small_tol, close_tol )
  SANS_CHECK_CLOSE( w.deriv(5), w_q[5], small_tol, close_tol )

  SANS_CHECK_CLOSE( t.deriv(0), t_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(1), t_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(2), t_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(3), t_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(4), t_q[4], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(5), t_q[5], small_tol, close_tol )

  SANS_CHECK_CLOSE( nt.deriv(0), nt_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( nt.deriv(1), nt_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( nt.deriv(2), nt_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( nt.deriv(3), nt_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( nt.deriv(4), nt_q[4], small_tol, close_tol )
  SANS_CHECK_CLOSE( nt.deriv(5), nt_q[5], small_tol, close_tol )
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( updateFraction )
{
  typedef QRANSSA3D<QTypePrimitiveRhoPressure, TraitsSizeRANSSA> QInterpret;
  typedef QInterpret::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  ArrayQ q, dq;
  Real rho, u, v, w, t, p, nt;
  Real maxChangeFraction, updateFraction;

  rho = 1.225;          // kg/m^3 (standard atmosphere)
  t   = 288.15;         // K (standard atmosphere)
  u   = 15.03;          // m/s
  v   =  1.71;
  w   = 10.25;
  p   = R*rho*t;
  nt  = 42.789;

  q(QInterpret::ir) = rho;
  q(QInterpret::iu) = u;
  q(QInterpret::iv) = v;
  q(QInterpret::iw) = w;
  q(QInterpret::ip) = p;
  q(QInterpret::iSA) = nt;

  updateFraction = 1;

  dq(QInterpret::ir) = rho * updateFraction;
  dq(QInterpret::iu) = u  * updateFraction;
  dq(QInterpret::iv) = v * updateFraction;
  dq(QInterpret::iw) = w * updateFraction;
  dq(QInterpret::ip) = p * updateFraction;
  dq(QInterpret::iSA) = nt * updateFraction;

  // no limiting
  maxChangeFraction = 1;

  qInterpret.updateFraction( q, dq, maxChangeFraction, updateFraction );
  BOOST_CHECK_EQUAL(1., updateFraction);

  // density limiting
  maxChangeFraction = 0.1;
  dq(QInterpret::ir) = rho * 0.5; // limit density drop
  dq(QInterpret::ip) = p * 0.01;
  dq(QInterpret::iSA) = nt * 0.01;

  qInterpret.updateFraction( q, dq, maxChangeFraction, updateFraction );
  BOOST_CHECK_CLOSE(0.2, updateFraction, 1e-10);

#if 0 // don't limit density growth
  maxChangeFraction = 0.1;
  dq(QInterpret::ir) = -rho * 0.5; // limit density growth
  dq(QInterpret::ip) = p * 0.01;
  dq(QInterpret::iSA) = nt * 0.01;

  qInterpret.updateFraction( q, dq, maxChangeFraction, updateFraction );
  BOOST_CHECK_CLOSE(0.2, updateFraction, 1e-10);
#endif

  // pressure limiting
  maxChangeFraction = 0.1;
  dq(QInterpret::ir) = rho * 0.01;
  dq(QInterpret::ip) = p * 0.5; // limit pressure drop
  dq(QInterpret::iSA) = nt * 0.01;

  qInterpret.updateFraction( q, dq, maxChangeFraction, updateFraction );
  BOOST_CHECK_CLOSE(0.2, updateFraction, 1e-10);

#if 0 // don't limit pressure growth
  maxChangeFraction = 0.1;
  dq(QInterpret::ir) = rho * 0.01;
  dq(QInterpret::ip) = -p * 0.5; // limit pressure growth
  dq(QInterpret::iSA) = nt * 0.01;

  qInterpret.updateFraction( q, dq, maxChangeFraction, updateFraction );
  BOOST_CHECK_CLOSE(0.2, updateFraction, 1e-10);
#endif

  // SA limiting
  maxChangeFraction = 0.1;
  dq(QInterpret::ir) = rho * 0.01;
  dq(QInterpret::ip) = p * 0.01;
  dq(QInterpret::iSA) = nt * 0.5; // limit SA drop

  qInterpret.updateFraction( q, dq, maxChangeFraction, updateFraction );
  BOOST_CHECK_CLOSE(0.2, updateFraction, 1e-10);

  maxChangeFraction = 0.1;
  dq(QInterpret::ir) = rho * 0.01;
  dq(QInterpret::ip) = p * 0.01;
  dq(QInterpret::iSA) = -nt * 0.5; // limit SA growth

  qInterpret.updateFraction( q, dq, maxChangeFraction, updateFraction );
  BOOST_CHECK_CLOSE(0.2, updateFraction, 1e-10);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef QRANSSA3D<QTypePrimitiveRhoPressure, TraitsSizeRANSSA> QInterpret;
  typedef QInterpret::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  ArrayQ q;
  q(0) =  1;
  q(4) =  1;
  BOOST_CHECK( qInterpret.isValidState(q) );

  q(0) =  1;
  q(4) = -1;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  q(0) = -1;
  q(4) =  1;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  q(0) = -1;
  q(4) = -1;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  q(0) =  1;
  q(4) =  0;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  q(0) =  0;
  q(4) =  1;
  BOOST_CHECK( qInterpret.isValidState(q) == false );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/QRANSSA3D_pattern.txt", true );

  {
  typedef QRANSSA3D<QTypePrimitiveRhoPressure, TraitsSizeRANSSA> QInterpret;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  qInterpret.dump( 2, output );
  }

  {
  typedef QRANSSA3D<QTypeConservative, TraitsSizeRANSSA> QInterpret;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  qInterpret.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
