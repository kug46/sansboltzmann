// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// OjedaMMS_ForcingFunction2D_btest
//
// test of 2-D Navier-Stokes forcing function classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/SolutionFunction_Euler2D.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/ForcingFunction2D_MMS.h"

// Explicitly instantiate the class so coverage information is correct
namespace SANS
{
template class SolutionFunction_NavierStokes2D_OjedaMMS<
                 TraitsSizeNavierStokes,
                 TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( OjedaMMS_ForcingFunction2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( QuadraticMMS_Forcing_Test )
{

  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef SolutionFunction_NavierStokes2D_QuadraticMMS<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> SolutionClass;
  typedef ForcingFunction2D_MMS<PDEClass> ForcingFunctionClass;

  // PDE
  const Real gamma = 1.4;
  const Real R     = 287.04;    // J/(kg K)

  const Real muRef = 5.1;       // kg/(m s) // FOR RE = 10
  //  const Real muRef = 0.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef); //single argument for constant viscosity
  ThermalConductivityModel tcond(Prandtl, Cp);

  // INFLOW CONDITIONS TO MATCH OJEDA'S MMS
  SolutionClass soln(gas);
  std::shared_ptr<ForcingFunctionClass> forcingptr( new ForcingFunctionClass(soln) );

  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw, eVanLeer, forcingptr );

  ArrayQ forcing = 0.0;
//  Real x = 0.52; Real y = 0.77; Real t = 0;
  Real x = 0.52; Real y = 0.77; Real t = 0;
  (*forcingptr)(pde, x, y, t, forcing);

  ArrayQ q = soln(x,y,t);
  ArrayQ qx = {y*0.035, y*0.015, y*0.012, 0};
  ArrayQ qxx = {0,0,0,0};
  ArrayQ qy = {x*0.035, x*0.015, x*0.012, 0};
  ArrayQ qyy = {0,0,0,0};
  ArrayQ qyx = {0.035,0.015,0.012,0};

  ArrayQ forcingTrue = 0;
  pde.strongFluxAdvective(x, y, t, q, qx, qy, forcingTrue);
  pde.strongFluxViscous(x, y, t, q, qx, qy, qxx, qyx, qyy, forcingTrue);
  pde.source(x, y, t, q, qx, qy, forcingTrue);

  Real tol  = 1e-12;

  BOOST_CHECK_CLOSE(forcingTrue(0), forcing(0), tol);
  BOOST_CHECK_CLOSE(forcingTrue(1), forcing(1), tol);
  BOOST_CHECK_CLOSE(forcingTrue(2), forcing(2), tol);
  BOOST_CHECK_CLOSE(forcingTrue(3), forcing(3), tol);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
