// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// QRANSSA2D_btest
// testing of Q2D<QTypePrimitiveRhoPressure, TraitsSizeRANSSA> class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/NS/QRANSSA2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/TraitsRANSSA.h"
#include "Surreal/SurrealS.h"

#include <string>
#include <iostream>

//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
template class QRANSSA2D<QTypePrimitiveRhoPressure, TraitsSizeRANSSA>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( QRANSSA2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test )
{
  const Real tol = 1.e-13;

  typedef QRANSSA2D<QTypePrimitiveRhoPressure, TraitsSizeRANSSA> QInterpret;
  BOOST_CHECK( QInterpret::N == 5 );

  typedef QInterpret::ArrayQ<Real> ArrayQ;
  BOOST_CHECK( ArrayQ::M == 5 );

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  // set/eval
  ArrayQ q;
  Real rho, u, v, t, p, nt;
  Real rho1, u1, v1, t1, nt1;
  Real rho2, u2, v2, t2, nt2;

  rho = 1.225;          // kg/m^3 (standard atmosphere)
  t   = 288.15;         // K (standard atmosphere)
  u   = 15.03;          // m/s
  v   =  1.71;
  p   = R*rho*t;
  nt  = 42.789;

  Real E = gas.energy(rho, t) + 0.5*(u*u + v*v);

  Real qDataPrim[5] = {rho, u, v, t, nt};
  string qNamePrim[5] = {"Density", "VelocityX", "VelocityY", "Temperature", "SANutilde"};
  qInterpret.setFromPrimitive( q, qDataPrim, qNamePrim, 5 );
  qInterpret.eval( q, rho1, u1, v1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );

  qInterpret.evalDensity( q, rho1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );

  qInterpret.evalTemperature( q, t1 );
  BOOST_CHECK_CLOSE( t, t1, tol );

  qInterpret.evalVelocity( q, u1, v1 );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );

  qInterpret.evalSA( q, nt1 );
  BOOST_CHECK_CLOSE( nt, nt1, tol );


  SAnt2D<DensityVelocityPressure2D<Real>> prim1 = {rho, u, v, p, nt};
  q = 1;
  nt1 = 1;
  qInterpret.setFromPrimitive( q, prim1 );
  qInterpret.eval( q, rho1, u1, v1, t1 );
  qInterpret.evalSA( q, nt1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );
  BOOST_CHECK_CLOSE( nt, nt1, tol );

  SAnt2D<DensityVelocityTemperature2D<Real>> prim2 = {rho, u, v, t, nt};
  q = 2;
  nt1 = 2;
  qInterpret.setFromPrimitive( q, prim2 );
  qInterpret.eval( q, rho1, u1, v1, t1 );
  qInterpret.evalSA( q, nt1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );
  BOOST_CHECK_CLOSE( nt, nt1, tol );

  SAnt2D<Conservative2D<Real>> prim3 = {rho, rho*u, rho*v, rho*E, rho*nt};
  q = 0;
  nt1 = 0;
  qInterpret.setFromPrimitive( q, prim3 );
  qInterpret.eval( q, rho1, u1, v1, t1 );
  qInterpret.evalSA( q, nt1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( v, v1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );
  BOOST_CHECK_CLOSE( nt, nt1, tol );


  // copy constructor
  QInterpret qInterpret2(qInterpret);

  qInterpret2.eval( q, rho2, u2, v2, t2 );
  BOOST_CHECK_CLOSE( rho, rho2, tol );
  BOOST_CHECK_CLOSE( u, u2, tol );
  BOOST_CHECK_CLOSE( v, v2, tol );
  BOOST_CHECK_CLOSE( t, t2, tol );

  qInterpret2.evalSA( q, nt2 );
  BOOST_CHECK_CLOSE( nt, nt2, tol );

  // gradient
  Real rhox, ux, vx, tx, px, ntx;
  Real rhoy, uy, vy, ty, py, nty;
  Real rhox1, ux1, vx1, tx1, ntx1;
  Real rhoy1, uy1, vy1, ty1, nty1;

  rhox = 0.37, ux = -0.85, vx = 0.09, px = 1.71; ntx = -3.41;
  rhoy = 2.31, uy =  1.65, vy = 0.87, py = 0.29; nty =  8.93;
  tx = t*(px/p - rhox/rho);
  ty = t*(py/p - rhoy/rho);

  Real qxData[5] = {rhox, ux, vx, px, ntx};
  Real qyData[5] = {rhoy, uy, vy, py, nty};
  ArrayQ qx(qxData, 5);
  ArrayQ qy(qyData, 5);

  qInterpret.evalGradient( q, qx, rhox1, ux1, vx1, tx1 );
  qInterpret.evalGradient( q, qy, rhoy1, uy1, vy1, ty1 );
  BOOST_CHECK_CLOSE( rhox, rhox1, tol );
  BOOST_CHECK_CLOSE(   ux,   ux1, tol );
  BOOST_CHECK_CLOSE(   vx,   vx1, tol );
  BOOST_CHECK_CLOSE(   tx,   tx1, tol );
  BOOST_CHECK_CLOSE( rhoy, rhoy1, tol );
  BOOST_CHECK_CLOSE(   uy,   uy1, tol );
  BOOST_CHECK_CLOSE(   vy,   vy1, tol );
  BOOST_CHECK_CLOSE(   ty,   ty1, tol );

  qInterpret.evalSAGradient( q, qx, ntx1 );
  qInterpret.evalSAGradient( q, qy, nty1 );
  BOOST_CHECK_CLOSE( ntx, ntx1, tol );
  BOOST_CHECK_CLOSE( nty, nty1, tol );


  // hessian
  Real rhoxx = -0.253; Real rhoxy = 0.782; Real rhoyy = 1.08;
  Real uxx = 1.02; Real uxy = -0.95; Real uyy =-0.42;
  Real vxx = 0.99; Real vxy = -0.92; Real vyy =-0.44;
  Real pxx = 2.99; Real pxy = -1.92; Real pyy =-2.44;
  Real ntxx = 17.51, ntxy = 2.27, ntyy = -3.87;

  Real txx = 1.0/R*( (-2*px*rhox)/(rho*rho) + 2*p*rhox*rhox/(rho*rho*rho) + pxx/rho - p*rhoxx/(rho*rho) );
  Real txy = 1.0/R*( -rhoy*px/(rho*rho) - py*rhox/(rho*rho) + 2*p*rhoy*rhox/(rho*rho*rho) + pxy/(rho) - p*rhoxy/(rho*rho) );
  Real tyy = 1.0/R*( -2*py*rhoy/(rho*rho) + 2*p*rhoy*rhoy/(rho*rho*rho) + pyy/(rho) - p*rhoyy/(rho*rho) );

  ArrayQ qxx = {rhoxx, uxx, vxx, pxx, ntxx};
  ArrayQ qxy = {rhoxy, uxy, vxy, pxy, ntxy};
  ArrayQ qyy = {rhoyy, uyy, vyy, pyy, ntyy};

  Real rhoxx1, rhoxy1, rhoyy1;
  Real uxx1, uxy1, uyy1;
  Real vxx1, vxy1, vyy1;
  Real txx1, txy1, tyy1;
  Real ntxx1, ntyy1;

  qInterpret.evalSecondGradient( q, qx, qy,
                                 qxx, qxy, qyy,
                                 rhoxx1, rhoxy1, rhoyy1,
                                 uxx1, uxy1, uyy1,
                                 vxx1, vxy1, vyy1,
                                 txx1, txy1, tyy1);

  BOOST_CHECK_CLOSE( rhoxx, rhoxx1, tol );
  BOOST_CHECK_CLOSE( rhoxy, rhoxy1, tol );
  BOOST_CHECK_CLOSE( rhoyy, rhoyy1, tol );
  BOOST_CHECK_CLOSE(   uxx,   uxx1, tol );
  BOOST_CHECK_CLOSE(   uxy,   uxy1, tol );
  BOOST_CHECK_CLOSE(   uyy,   uyy1, tol );
  BOOST_CHECK_CLOSE(   vxx,   vxx1, tol );
  BOOST_CHECK_CLOSE(   vxy,   vxy1, tol );
  BOOST_CHECK_CLOSE(   vyy,   vyy1, tol );
  BOOST_CHECK_CLOSE(   txx,   txx1, tol );
  BOOST_CHECK_CLOSE(   txy,   txy1, tol );
  BOOST_CHECK_CLOSE(   tyy,   tyy1, tol );

  qInterpret.evalSAHessian( q, qx, qx, qxx, ntxx1 );
  qInterpret.evalSAHessian( q, qy, qy, qyy, ntyy1 );
  BOOST_CHECK_CLOSE( ntxx, ntxx1, tol );
  BOOST_CHECK_CLOSE( ntyy, ntyy1, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( evalJacobian )
{
  typedef QRANSSA2D<QTypePrimitiveRhoPressure, TraitsSizeRANSSA> QInterpret;

  typedef QInterpret::ArrayQ< Real > ArrayQ;
  typedef QInterpret::ArrayQ< SurrealS<QInterpret::N> > ArrayQSurreal;

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  ArrayQ q=0, rho_q, u_q, v_q, t_q, nt_q;
  ArrayQSurreal qSurreal;
  SurrealS<QInterpret::N> rho, u, v, t, nt;

  SAnt2D<DensityVelocityTemperature2D<Real>> qPrim = {1.225, 15.03, 1.71, 288.15, 42.789};
  qInterpret.setFromPrimitive( q, qPrim );

  qInterpret.evalJacobian(q, rho_q, u_q, v_q, t_q);
  qInterpret.evalSAJacobian(q, nt_q);

  qSurreal = q;

  qSurreal[0].deriv(0) = 1;
  qSurreal[1].deriv(1) = 1;
  qSurreal[2].deriv(2) = 1;
  qSurreal[3].deriv(3) = 1;
  qSurreal[4].deriv(4) = 1;

  qInterpret.eval(qSurreal, rho, u, v, t);
  qInterpret.evalSA(qSurreal, nt);

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  SANS_CHECK_CLOSE( rho.deriv(0), rho_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(1), rho_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(2), rho_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(3), rho_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(4), rho_q[4], small_tol, close_tol )

  SANS_CHECK_CLOSE( u.deriv(0), u_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(1), u_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(2), u_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(3), u_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(4), u_q[4], small_tol, close_tol )

  SANS_CHECK_CLOSE( v.deriv(0), v_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(1), v_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(2), v_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(3), v_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( v.deriv(4), v_q[4], small_tol, close_tol )

  SANS_CHECK_CLOSE( t.deriv(0), t_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(1), t_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(2), t_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(3), t_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(4), t_q[4], small_tol, close_tol )

  SANS_CHECK_CLOSE( nt.deriv(0), nt_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( nt.deriv(1), nt_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( nt.deriv(2), nt_q[2], small_tol, close_tol )
  SANS_CHECK_CLOSE( nt.deriv(3), nt_q[3], small_tol, close_tol )
  SANS_CHECK_CLOSE( nt.deriv(4), nt_q[4], small_tol, close_tol )
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef QRANSSA2D<QTypePrimitiveRhoPressure, TraitsSizeRANSSA> QInterpret;
  typedef QInterpret::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  ArrayQ q;
  q(0) =  1;
  q(3) =  1;
  BOOST_CHECK( qInterpret.isValidState(q) );

  q(0) =  1;
  q(3) = -1;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  q(0) = -1;
  q(3) =  1;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  q(0) = -1;
  q(3) = -1;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  q(0) =  1;
  q(3) =  0;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  q(0) =  0;
  q(3) =  1;
  BOOST_CHECK( qInterpret.isValidState(q) == false );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/QRANSSA2D_pattern.txt", true );

  {
  typedef QRANSSA2D<QTypePrimitiveRhoPressure, TraitsSizeRANSSA> QInterpret;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  qInterpret.dump( 2, output );
  }

  {
  typedef QRANSSA2D<QTypeConservative, TraitsSizeRANSSA> QInterpret;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  qInterpret.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
