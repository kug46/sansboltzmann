// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Fluids2D_Sensor_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/TraitsEuler.h"

#include "pde/NS/Fluids2D_Sensor.h"


using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
class QTypeEntropy {};

}


using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Fluids2D_Sensor_test_suite )

typedef boost::mpl::list< QTypePrimitiveRhoPressure,
                          QTypeConservative,
                          QTypePrimitiveSurrogate,
                          QTypeEntropy > QTypes;


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( steady_ctors, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef Fluids_Sensor<PhysD2, PDEClass> Sensor;

  // Construct the base Euler pde properties
  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  PDEClass pde(gas, Euler_ResidInterp_Raw);
  Sensor sensor(pde);

  BOOST_CHECK( sensor.D == 2 );
  BOOST_CHECK( sensor.N == 4 );;
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( steady_jumpQuantity, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef Fluids_Sensor<PhysD2, PDEClass> Sensor;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 2.e-13;

  // Construct the base Euler pde properties
  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);


  PDEClass pde(gas, Euler_ResidInterp_Raw);
  Sensor sensor(pde);

  Real rho, u, v, t, p;

  rho = 1.137; u = 0.784; v = 0.784; t = 0.987;
  p  = R*rho*t;

  // set
  Real qDataPrim[4] = {rho, u, v, t};
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY", "Temperature"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );
  Real JumpQuantityTrue = p;
  Real JumpQunatitypde = 0;
  sensor.jumpQuantity(q, JumpQunatitypde);
  BOOST_CHECK_CLOSE( JumpQuantityTrue, JumpQunatitypde, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
