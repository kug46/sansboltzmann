// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCEuler3D_mitLagrange_btest
//
// test of 3-D compressible Euler BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/PDEEuler3D.h"
#include "pde/NS/BCEuler3D.h"

#include "pde/BCParameters.h"
#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
//template class BCEuler3D< BCTypeSymmetry,
//  PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
//template class BCEuler3D< BCTypeInflowSupersonic,
//  PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
//template class BCEuler3D< BCTypeInflowSubsonic_sHqt,
//  PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
//template class BCEuler3D< BCTypeInflowSubsonic_sHqt_Profile<
//  SolutionFunction_Euler3D_Wake<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >,
//  PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
//template class BCEuler3D< BCTypeOutflowSubsonic_Pressure,
//  PDEEuler3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;

// Instantiate BCParamaters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters< BCEuler3DVector<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> >;
}

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( BCEuler3D_mitLagrange_test_suite )

#if 0   // old file organization
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  {
  typedef BCEuler3D<BCTypeSymmetry, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler3D<BCTypeInflowSupersonic, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 4 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler3D<BCTypeInflowSubsonic_sHqt, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef SolutionFunction_Euler3D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction_EulerClass> BCTypeClass;
  typedef BCEuler3D<BCTypeClass, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 3 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }

  {
  typedef BCEuler3D<BCTypeOutflowSubsonic_Pressure, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 4 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef BCParameters< BCEuler3DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  {
  typedef BCEuler3D<BCTypeSymmetry, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {0, 0, 0, 0};
  BCClass bc1(pde, bcdata);

  //Pydict constructor
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry;
  BCSymmetry[BCEuler3DParams<BCTypeSymmetry>::params.qn] = -7.0;

  PyDict BCList;
  BCList["BC1"] = BCSymmetry;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCSymmetry);
  }

  {
  typedef BCEuler3D<BCTypeInflowSupersonic, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {0, 0, 0, 0};
  BCClass bc1(pde, bcdata);

  PyDict BCInflowSupersonic;
  BCInflowSupersonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSupersonic;
  BCInflowSupersonic[BCEuler3DParams<BCTypeInflowSupersonic>::params.rho] = 1.0;
  BCInflowSupersonic[BCEuler3DParams<BCTypeInflowSupersonic>::params.rhou] = 1.0;
  BCInflowSupersonic[BCEuler3DParams<BCTypeInflowSupersonic>::params.rhov] = 1.0;
  BCInflowSupersonic[BCEuler3DParams<BCTypeInflowSupersonic>::params.rhoE] = 1.0;

  PyDict BCList;
  BCList["BC1"] = BCInflowSupersonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCInflowSupersonic);
  }

  {
  typedef BCEuler3D<BCTypeInflowSubsonic_sHqt, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {0, 0, 0, 0};
  BCClass bc1(pde, bcdata);

  PyDict BCInflowSubsonic;
  BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt;
  BCInflowSubsonic[BCEuler3DParams<BCTypeInflowSubsonic_sHqt>::params.sSpec] = 1.0;
  BCInflowSubsonic[BCEuler3DParams<BCTypeInflowSubsonic_sHqt>::params.HSpec] = 1.0;
  BCInflowSubsonic[BCEuler3DParams<BCTypeInflowSubsonic_sHqt>::params.qtSpec] = 1.0;

  PyDict BCList;
  BCList["BC1"] = BCInflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCInflowSubsonic);
  }

  {
  typedef SolutionFunction_Euler3D_Const<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction_EulerClass> BCTypeClass;
  typedef BCEuler3D<BCTypeClass, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {0, 0, 0, 0};
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  SolutionFunction_EulerClass constsol(gas, 1.0, 1.0, 1.0, 1.0);
  BCClass bc(pde, constsol);

  PyDict solnArgs;
  solnArgs[SolutionFunction_EulerClass::ParamsType::params.gasModel] = gasModelDict;
  solnArgs[SolutionFunction_EulerClass::ParamsType::params.rho] = 1.0;
  solnArgs[SolutionFunction_EulerClass::ParamsType::params.u] = 1.0;
  solnArgs[SolutionFunction_EulerClass::ParamsType::params.v] = 1.0;
  solnArgs[SolutionFunction_EulerClass::ParamsType::params.p] = 1.0;

  // Add the solution BC to the BCVector
  typedef boost::mpl::push_back< BCEuler3DVector<TraitsSizeEuler, TraitsModelEulerClass>, BCClass >::type BCVector;
  typedef BCParameters<BCVector> BCParams;

  PyDict BCInflowSubsonicProfile;
  BCInflowSubsonicProfile[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_Profile;
  BCInflowSubsonicProfile[BCEuler3DParams<BCTypeClass>::params.SolutionArgs] = solnArgs;

  PyDict BCList;
  BCList["BC1"] = BCInflowSubsonicProfile;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCInflowSubsonicProfile);
  }

  {
  typedef BCEuler3D<BCTypeOutflowSubsonic_Pressure, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {0, 0, 0, 0};
  BCClass bc(pde, bcdata);

  PyDict BCOutflowSubsonic;
  BCOutflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure;
  BCOutflowSubsonic[BCEuler3DParams<BCTypeOutflowSubsonic_Pressure>::params.pSpec] = 1.0;

  PyDict BCList;
  BCList["BC1"] = BCOutflowSubsonic;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCOutflowSubsonic);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_Symmetry )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler3D<BCTypeSymmetry, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  const ArrayQ bcdata = {-7, 0, 0, 0};
  BCClass bc(pde, bcdata);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 1 );

  Real x, y, z, time;
  Real nx, ny;
  Real rho, u, v, t, p, E, H, qn;
  Real Cv, Cp;

  x = y = z = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  E  = Cv*t + 0.5*(u*u + v*v);
  H  = Cp*t + 0.5*(u*u + v*v);
  qn = nx*u + ny*v;

  ArrayQ q;
  DensityVelocityPressure3D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  ArrayQ qx = 0;
  ArrayQ qy = 0;

  ArrayQ lg = {1.551, -0.237, 9.887, 3.119};

  // strong-form BC
  ArrayQ rsdBCTrue = {qn - bcdata(0), lg(1) - rho*E, lg(2), lg(3) - rho};
  ArrayQ rsdBC = 0;
  bc.strongBC( x, y, z, time, nx, ny, nz, q, qx, qy, qz, lg, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(2), rsdBC(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(3), rsdBC(3), small_tol, close_tol );

  rsdBC = 0;
  bc.strongBC( x, y, z, time, nx, ny, nz, q, qx, qy, qz, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,            rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,            rsdBC(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,            rsdBC(3), small_tol, close_tol );

  // BC weight
  Real wghtBCdata[16] = {rho, 0, 0, 0, rho*(u + nx*qn), 0, 0, 0, rho*(v + ny*qn), 0, 0, 0, rho*(H + qn*qn), 0, 0, 0};
  MatrixQ wghtBCTrue( wghtBCdata, 16 );
  MatrixQ wghtBC = 0;
  bc.weightBC( x, y, z, time, nx, ny, nz, q, qx, qy, qz, wghtBC );
  SANS_CHECK_CLOSE( wghtBCTrue(0,0), wghtBC(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,1), wghtBC(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,2), wghtBC(0,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,3), wghtBC(0,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,0), wghtBC(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,1), wghtBC(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,2), wghtBC(1,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,3), wghtBC(1,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,0), wghtBC(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,1), wghtBC(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,2), wghtBC(2,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,3), wghtBC(2,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,0), wghtBC(3,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,1), wghtBC(3,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,2), wghtBC(3,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,3), wghtBC(3,3), small_tol, close_tol );

  // Lagrange multiplier weight
  Real wghtLGdata[16] = {0, 0, -0.815136, 0,
                         -0.3744, -0.2645567232, -0.5140138416, 0,
                         0.1408, -0.7713803776, 0.1412680192, 0,
                         -1.1411904, 0.0194824025088, 0.1089049410624, 0};
  MatrixQ wghtLGTrue( wghtLGdata, 16 );
  MatrixQ wghtLG = 0;
  bc.weightLagrange( x, y, z, time, nx, ny, nz, q, qx, qy, qz, wghtLG );
  SANS_CHECK_CLOSE( wghtLGTrue(0,0), wghtLG(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,1), wghtLG(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,2), wghtLG(0,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,3), wghtLG(0,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,0), wghtLG(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,1), wghtLG(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,2), wghtLG(1,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,3), wghtLG(1,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,0), wghtLG(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,1), wghtLG(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,2), wghtLG(2,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,3), wghtLG(2,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,0), wghtLG(3,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,1), wghtLG(3,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,2), wghtLG(3,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,3), wghtLG(3,3), small_tol, close_tol );

  // Lagrange rhs
  ArrayQ rhsLGTrue = {rho*E, 0, rho, 0};
  ArrayQ rhsLG = 0;
  bc.rhsLagrange( x, y, z, time, nx, ny, nz, q, qx, qy, qz, rhsLG );
  SANS_CHECK_CLOSE( rhsLGTrue(0), rhsLG(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rhsLGTrue(1), rhsLG(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rhsLGTrue(2), rhsLG(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( rhsLGTrue(3), rhsLG(3), small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_inflow_supersonic )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler3D<BCTypeInflowSupersonic, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  const ArrayQ bcdata = {7, 5.447, -2.441, 9.443};
  BCClass bc(pde, bcdata);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 4 );

  Real x, y, z, time;
  Real nx, ny;
  Real rho, u, v, t, p, e, E;
  Real Cv;

  x = y = z = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  Cv = R/(gamma - 1);
  p = R*rho*t;
  e = Cv*t;
  E = e + 0.5*(u*u + v*v);

  ArrayQ q;
  DensityVelocityPressure3D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  ArrayQ qx = 0;
  ArrayQ qy = 0;

  ArrayQ lg = {1.551, -0.237, 9.887, 3.119};

  // strong-form BC
  ArrayQ rsdBCTrue = {rho - bcdata(0), rho*u - bcdata(1), rho*v - bcdata(2), rho*E - bcdata(3)};
  ArrayQ rsdBC = 0;
  bc.strongBC( x, y, z, time, nx, ny, nz, q, qx, qy, qz, lg, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(2), rsdBC(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(3), rsdBC(3), small_tol, close_tol );

  rsdBC = 0;
  bc.strongBC( x, y, z, time, nx, ny, nz, q, qx, qy, qz, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(2), rsdBC(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(3), rsdBC(3), small_tol, close_tol );

  // BC weight (B^t)
  Real dfdudata[16] = {0,1,0,0, -0.4810526,1.2544,0.0924,0.4, 0.181104,-0.231,0.784,0,       -1.2404487984,1.4699461,0.0724416,1.0976};
  Real dgdudata[16] = {0,0,1,0,  0.181104,-0.231,0.784,0,     0.0802424,-0.3136,-0.3696,0.4,  0.3654893781,0.0724416,1.6944641,-0.3234};
  Real wghtBCdata[16];
  for (int n = 0; n < 16; n++)
    wghtBCdata[n] = nx*dfdudata[n] + ny*dgdudata[n];
  MatrixQ wghtBCTrue( wghtBCdata, 16 );
  MatrixQ wghtBC = 0;
  bc.weightBC( x, y, z, time, nx, ny, nz, q, qx, qy, qz, wghtBC );
  SANS_CHECK_CLOSE( wghtBCTrue(0,0), wghtBC(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,1), wghtBC(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,2), wghtBC(0,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,3), wghtBC(0,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,0), wghtBC(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,1), wghtBC(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,2), wghtBC(1,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,3), wghtBC(1,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,0), wghtBC(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,1), wghtBC(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,2), wghtBC(2,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,3), wghtBC(2,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,0), wghtBC(3,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,1), wghtBC(3,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,2), wghtBC(3,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,3), wghtBC(3,3), small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_inflow_sHqt )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler3D<BCTypeInflowSubsonic_sHqt, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  const ArrayQ bcdata = {-7.153, 3.551, 4.993, 0};
  BCClass bc(pde, bcdata);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 3 );

  Real x, y, z, time;
  Real nx, ny;
  Real rho, u, v, t, p, s, H, qt;

  x = y = z = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  p   = R*rho*t;
  s   = log(p / pow(rho, gamma));
  H   = gamma*p/((gamma - 1)*rho) + 0.5*(u*u + v*v);
  qt  = nx*v - ny*u;

  ArrayQ q;
  DensityVelocityPressure3D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  ArrayQ qx = 0;
  ArrayQ qy = 0;

  ArrayQ lg = {1.551, -0.237, 9.887, 3.119};

  // strong-form BC
  ArrayQ rsdBCTrue = {s - bcdata(0), H - bcdata(1), qt - bcdata(2), 0};
  ArrayQ rsdBC = 0;
  bc.strongBC( x, y, z, time, nx, ny, nz, q, qx, qy, qz, lg, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(2), rsdBC(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(3), rsdBC(3), small_tol, close_tol );

  rsdBC = 0;
  bc.strongBC( x, y, z, time, nx, ny, nz, q, qx, qy, qz, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(2), rsdBC(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,            rsdBC(3), small_tol, close_tol );

  // BC weight (B^t)
#if 0   // NOTE: compiler warning: integer constant is too large for its type
  Real wghtBCdata[16] = {109418801737851/50537387500000., -172397625/113203748., -1471586127/16171964000., 0,
                         4717164992928621/12634346875000000., -1141226737239/505373875000., 154689715257/808598200000., 0,
                         -45239076996371181/50537387500000000., 1520191218117/2021495500000., 14755754480649/16171964000000., 0,
                         375483420163239035067/101074775000000000000., -14311415513561187/4042991000000000., -5049919970377359/32343928000000000., 0};
#endif
  Real wghtBCdata[16] = { 2.1651060165674571128157346875123, -1.5228967949011723534100655395261, -0.090996129288934850460958236117765, 0,
                          2.7478401009888863764475359950096, -2.2581830872025191250734913829885,  0.19130603463747507723860874288367,  0,
                         -0.89516057782708259306043471281534, 0.75201315962217081363772513963054, 0.91242810586574395045648135254320,  0,
                          3.7149073066275837375546965105784, -3.5398088973141881839459944382760, -0.15613193210105337236714105967587,  0};
  MatrixQ wghtBCTrue( wghtBCdata, 16 );
  MatrixQ wghtBC = 0;
  bc.weightBC( x, y, z, time, nx, ny, nz, q, qx, qy, qz, wghtBC );
  SANS_CHECK_CLOSE( wghtBCTrue(0,0), wghtBC(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,1), wghtBC(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,2), wghtBC(0,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,3), wghtBC(0,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,0), wghtBC(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,1), wghtBC(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,2), wghtBC(1,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,3), wghtBC(1,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,0), wghtBC(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,1), wghtBC(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,2), wghtBC(2,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,3), wghtBC(2,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,0), wghtBC(3,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,1), wghtBC(3,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,2), wghtBC(3,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,3), wghtBC(3,3), small_tol, close_tol );

  // Lagrange multiplier weight (\bar {A}^t)
  Real wghtLGdata[16] = {27277026/244140625., 0, 0, 0,
                         2673148548/30517578125., 0, 0, 0,
                         -3150496503/122070312500., 0, 0, 0,
                         46802153065521/244140625000000., 0, 0, 0};
  MatrixQ wghtLGTrue( wghtLGdata, 16 );
  MatrixQ wghtLG = 0;
  bc.weightLagrange( x, y, z, time, nx, ny, nz, q, qx, qy, qz, wghtLG );
  SANS_CHECK_CLOSE( wghtLGTrue(0,0), wghtLG(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,1), wghtLG(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,2), wghtLG(0,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,3), wghtLG(0,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,0), wghtLG(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,1), wghtLG(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,2), wghtLG(1,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,3), wghtLG(1,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,0), wghtLG(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,1), wghtLG(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,2), wghtLG(2,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,3), wghtLG(2,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,0), wghtLG(3,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,1), wghtLG(3,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,2), wghtLG(3,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,3), wghtLG(3,3), small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_inflow_sHqt_Profile )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef SolutionFunction_Euler3D_Wake<TraitsSizeEuler, TraitsModelEulerClass> SolutionFunction_EulerClass;
  typedef BCEuler3D<BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction_EulerClass>, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  Real wakedepth = 0.1; // 10% wake depth
  Real u0 = 1.0;
  Real rho0 = 1.0;
  Real p0 = 1.0;

  SolutionFunction_EulerClass wakesol(gas, wakedepth, rho0, u0, p0);
  BCClass bc(pde, wakesol);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 3 );

  Real x, y, z, time;
  Real nx, ny;
  Real rho, u, v, t, p, s, H, qt;

  x = time = 0;   // not actually used in functions
  y = 0.0; // y matters for this case
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  p   = gas.pressure(rho, t);
  s   = log(p / pow(rho, gamma));
  H   = gamma*p/((gamma - 1)*rho) + 0.5*(u*u + v*v);
  qt  = nx*v - ny*u;

  ArrayQ q;
  DensityVelocityPressure3D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  ArrayQ qx = 0;
  ArrayQ qy = 0;

  ArrayQ lg = {1.551, -0.237, 9.887, 3.119}; //lg not used...

  ArrayQ qSpec = wakesol(x,y,time);
  Real rhoSpec, uSpec, vSpec, tSpec = 0;
  pde.variableInterpreter().eval(qSpec, rhoSpec, uSpec, vSpec, tSpec);
  Real pSpec = gas.pressure(rhoSpec, tSpec);

  Real sSpec   = log(pSpec / pow(rhoSpec, gamma));
  Real HSpec   = gamma*pSpec/((gamma - 1)*rhoSpec) + 0.5*(uSpec*uSpec + vSpec*vSpec);
  Real qtSpec  = nx*vSpec - ny*uSpec;

  //actual values for s, H, qt at y=0.5;
  const ArrayQ bcdata = { sSpec, HSpec, qtSpec, 0};

  // strong-form BC
  ArrayQ rsdBCTrue = {s - bcdata(0), H - bcdata(1), qt - bcdata(2), 0};
  ArrayQ rsdBC = 0;
  bc.strongBC( x, y, z, time, nx, ny, nz, q, qx, qy, qz, lg, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(2), rsdBC(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(3), rsdBC(3), small_tol, close_tol );

  rsdBC = 0;
  bc.strongBC( x, y, z, time, nx, ny, nz, q, qx, qy, qz, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(2), rsdBC(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,            rsdBC(3), small_tol, close_tol );

  // BC weight (B^t)
#if 0   // NOTE: compiler warning: integer constant is too large for its type
  Real wghtBCdata[16] = {109418801737851/50537387500000., -172397625/113203748., -1471586127/16171964000., 0,
                         4717164992928621/12634346875000000., -1141226737239/505373875000., 154689715257/808598200000., 0,
                         -45239076996371181/50537387500000000., 1520191218117/2021495500000., 14755754480649/16171964000000., 0,
                         375483420163239035067/101074775000000000000., -14311415513561187/4042991000000000., -5049919970377359/32343928000000000., 0};
#endif
  Real wghtBCdata[16] = { 2.1651060165674571128157346875123, -1.5228967949011723534100655395261, -0.090996129288934850460958236117765, 0,
                          2.7478401009888863764475359950096, -2.2581830872025191250734913829885,  0.19130603463747507723860874288367,  0,
                         -0.89516057782708259306043471281534, 0.75201315962217081363772513963054, 0.91242810586574395045648135254320,  0,
                          3.7149073066275837375546965105784, -3.5398088973141881839459944382760, -0.15613193210105337236714105967587,  0};
  MatrixQ wghtBCTrue( wghtBCdata, 16 );
  MatrixQ wghtBC = 0;
  bc.weightBC( x, y, z, time, nx, ny, nz, q, qx, qy, qz, wghtBC );
  SANS_CHECK_CLOSE( wghtBCTrue(0,0), wghtBC(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,1), wghtBC(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,2), wghtBC(0,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,3), wghtBC(0,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,0), wghtBC(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,1), wghtBC(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,2), wghtBC(1,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,3), wghtBC(1,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,0), wghtBC(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,1), wghtBC(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,2), wghtBC(2,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,3), wghtBC(2,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,0), wghtBC(3,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,1), wghtBC(3,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,2), wghtBC(3,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,3), wghtBC(3,3), small_tol, close_tol );

  // Lagrange multiplier weight (\bar {A}^t)
  Real wghtLGdata[16] = {27277026/244140625., 0, 0, 0,
                         2673148548/30517578125., 0, 0, 0,
                         -3150496503/122070312500., 0, 0, 0,
                         46802153065521/244140625000000., 0, 0, 0};
  MatrixQ wghtLGTrue( wghtLGdata, 16 );
  MatrixQ wghtLG = 0;
  bc.weightLagrange( x, y, z, time, nx, ny, nz, q, qx, qy, qz, wghtLG );
  SANS_CHECK_CLOSE( wghtLGTrue(0,0), wghtLG(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,1), wghtLG(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,2), wghtLG(0,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,3), wghtLG(0,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,0), wghtLG(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,1), wghtLG(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,2), wghtLG(1,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,3), wghtLG(1,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,0), wghtLG(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,1), wghtLG(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,2), wghtLG(2,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,3), wghtLG(2,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,0), wghtLG(3,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,1), wghtLG(3,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,2), wghtLG(3,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,3), wghtLG(3,3), small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_outflow_pressure )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef BCEuler3D<BCTypeOutflowSubsonic_Pressure, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

  const ArrayQ bcdata = {-7, 0, 0, 0};
  BCClass bc(pde, bcdata);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 1 );

  Real x, y, z, time;
  Real nx, ny;
  Real rho, u, v, t, p;

  x = y = z = time = 0;   // not actually used in functions
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  p  = R*rho*t;

  ArrayQ q;
  DensityVelocityPressure3D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( q, qdata );

  ArrayQ qx = 0;
  ArrayQ qy = 0;

  ArrayQ lg = {1.551, -0.237, 9.887, 3.119};

  // strong-form BC
  ArrayQ rsdBCTrue = {p - bcdata(0), 0, 0, 0};
  ArrayQ rsdBC = 0;
  bc.strongBC( x, y, z, time, nx, ny, nz, q, qx, qy, qz, lg, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(2), rsdBC(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue(3), rsdBC(3), small_tol, close_tol );

  rsdBC = 0;
  bc.strongBC( x, y, z, time, nx, ny, nz, q, qx, qy, qz, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,            rsdBC(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,            rsdBC(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,            rsdBC(3), small_tol, close_tol );

  // BC weight (B^t)
  Real wghtBCdata[16] = {-2426/1645., 0, 0, 0,
                         -61459/29375., 0, 0, 0,
                          81389/117500., 0, 0, 0,
                         -786207163/235000000., 0, 0, 0};
  MatrixQ wghtBCTrue( wghtBCdata, 16 );
  MatrixQ wghtBC = 0;
  bc.weightBC( x, y, z, time, nx, ny, nz, q, qx, qy, qz, wghtBC );
  SANS_CHECK_CLOSE( wghtBCTrue(0,0), wghtBC(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,1), wghtBC(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,2), wghtBC(0,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,3), wghtBC(0,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,0), wghtBC(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,1), wghtBC(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,2), wghtBC(1,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,3), wghtBC(1,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,0), wghtBC(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,1), wghtBC(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,2), wghtBC(2,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,3), wghtBC(2,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,0), wghtBC(3,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,1), wghtBC(3,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,2), wghtBC(3,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtBCTrue(3,3), wghtBC(3,3), small_tol, close_tol );

  // Lagrange multiplier weight (\bar {A}^t)
  Real wghtLGdata[16] = {0, -117/125., 44/125., 0,
                         -1248177/1953125., -9681/6250., 4312/15625., 0,
                         5884263/31250000., 27027/125000., -14007/15625., 0,
                         -52214988441/31250000000., -112253169/50000000., 49516313/62500000., 0};
  MatrixQ wghtLGTrue( wghtLGdata, 16 );
  MatrixQ wghtLG = 0;
  bc.weightLagrange( x, y, z, time, nx, ny, nz, q, qx, qy, qz, wghtLG );
  SANS_CHECK_CLOSE( wghtLGTrue(0,0), wghtLG(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,1), wghtLG(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,2), wghtLG(0,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(0,3), wghtLG(0,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,0), wghtLG(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,1), wghtLG(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,2), wghtLG(1,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(1,3), wghtLG(1,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,0), wghtLG(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,1), wghtLG(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,2), wghtLG(2,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(2,3), wghtLG(2,3), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,0), wghtLG(3,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,1), wghtLG(3,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,2), wghtLG(3,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( wghtLGTrue(3,3), wghtLG(3,3), small_tol, close_tol );
}
#endif   // old file organization


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/BCEuler3D_mitLagrange_pattern.txt", true );

  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);

#if 0   // not fully implemented in 3D
  {
  typedef BCEuler3D<BCTypeSymmetry, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {-7, 0, 0, 0};
  BCClass bc(pde, bcdata);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler3D<BCTypeInflowSupersonic, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {7, 3.551, -4.993, 2.997};
  BCClass bc(pde, bcdata);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler3D<BCTypeInflowSubsonic_sHqt, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {-7.153, 3.551, 4.993, 0};
  BCClass bc(pde, bcdata);

  bc.dump( 2, output );
  }

  {
  typedef BCEuler3D<BCTypeOutflowSubsonic_Pressure, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;

  const ArrayQ bcdata = {-7, 0, 0, 0};
  BCClass bc(pde, bcdata);

  bc.dump( 2, output );
  }
#endif   // not fully implemented in 3D

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
