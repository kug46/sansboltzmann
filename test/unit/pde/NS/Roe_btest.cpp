// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Roe_btest
//
// test of Roe upwinding scheme

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/vector.hpp>

#include "pde/NS/Roe.h"
#include "pde/NS/TraitsEuler.h"

#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DEntropy.h"

#include "pde/NS/Q3DPrimitiveRhoPressure.h"
//#include "pde/NS/Q3DPrimitiveSurrogate.h"
#include "pde/NS/Q3DConservative.h"
//#include "pde/NS/Q3DEntropy.h"


//Explicitly instantiate the class so coverage information is correct
namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
class QTypeEntropy {};

template class Roe2D<QTypePrimitiveRhoPressure, TraitsSizeEuler>;
//template class Roe3D<QTypePrimitiveRhoPressure, TraitsSizeEuler>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Roe_test_suite )

typedef boost::mpl::vector< QTypePrimitiveRhoPressure
                          , QTypeConservative
                          , QTypePrimitiveSurrogate
                          , QTypeEntropy
                          > QTypes2D;

typedef boost::mpl::vector< QTypePrimitiveRhoPressure
                          , QTypeConservative
                        //, QTypePrimitiveSurrogate
                        //, QTypeEntropy
                          > QTypes3D;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxRoe2D, QType, QTypes2D )
{
  typedef Roe2D<QType, TraitsSizeEuler> RoeClass;
  typedef typename RoeClass::template ArrayQ<Real> ArrayQ;

  typedef Q2D<QType, TraitsSizeEuler> QInterpret;

  const Real tol = 2.e-11;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  RoeClass roe(gas, eVanLeer);
  QInterpret qInterpret(gas);

  // Roe flux function test

  Real x, y, time;
  Real nx, ny;
  Real rhoL, uL, vL, tL, pL, h0L;
  Real rhoR, uR, vR, tR, pR, h0R;
  Real Cp;

  x = y = time = 0;   // not actually used in functions
  nx = 2.0/7.0; ny = sqrt(1.0-nx*nx);
  rhoL = 1.034; uL = 3.26; vL = -2.17; tL = 5.78;
  rhoR = 0.973; uR = 1.79; vR =  0.93; tR = 6.13;
  Cp  = R*gamma/(gamma - 1);

  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL + vL*vL);

  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR + vR*vR);

  // set
  ArrayQ qL, qR;
  qInterpret.setFromPrimitive( qL, DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
  qInterpret.setFromPrimitive( qR, DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );

  // advective normal flux (average)
  ArrayQ fL = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*vL     , rhoL*uL*h0L};
  ArrayQ fR = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*vR     , rhoR*uR*h0R};
  ArrayQ gL = {rhoL*vL, rhoL*vL*uL     , rhoL*vL*vL + pL, rhoL*vL*h0L};
  ArrayQ gR = {rhoR*vR, rhoR*vR*uR     , rhoR*vR*vR + pR, rhoR*vR*h0R};

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < ArrayQ::M; k++)
    fnTrue[k] = 0.5*(nx*(fL[k] + fR[k]) + ny*(gL[k] + gR[k]));

  // Exact values for Roe scheme from Roe2D.nb
  fnTrue[0] -= -0.021574952725013058474459426733768;
  fnTrue[1] -= -1.7958753728220592172800048653566;
  fnTrue[2] -=  4.4515996249161671139371535471951;
  fnTrue[3] -= -6.8094427666218353919034995028459;

  ArrayQ fn;
  fn = 0;
  roe.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn, 1 );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( fnTrue(3), fn(3), tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxRoe3D, QType, QTypes3D )
{
  typedef Roe3D<QType, TraitsSizeEuler> RoeClass;
  typedef typename RoeClass::template ArrayQ<Real> ArrayQ;

  typedef Q3D<QType, TraitsSizeEuler> QInterpret;

  const Real tol = 2.e-11;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  RoeClass roe(gas, eVanLeer);
  QInterpret qInterpret(gas);

  // Roe flux function test

  Real x, y, z, time;
  Real nx, ny, nz;
  Real rhoL, uL, vL, wL, tL, pL, h0L;
  Real rhoR, uR, vR, wR, tR, pR, h0R;
  Real Cp;

  x = y = z = time = 0;   // not actually used in functions
  nx = 2.0/7.0; ny = 1.0/7.0; nz = sqrt(1.0 - nx*nx - ny*ny);
  rhoL = 1.034; uL = 3.26; vL = -2.17; wL = 3.56; tL = 5.78;
  rhoR = 0.973; uR = 1.79; vR =  0.93; wR = 5.23; tR = 6.13;
  Cp  = R*gamma/(gamma - 1);

  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL + vL*vL + wL*wL);

  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR + vR*vR + wR*wR);

  // set
  ArrayQ qL, qR;
  qInterpret.setFromPrimitive( qL, DensityVelocityTemperature3D<Real>(rhoL, uL, vL, wL, tL) );
  qInterpret.setFromPrimitive( qR, DensityVelocityTemperature3D<Real>(rhoR, uR, vR, wR, tR) );

  // advective normal flux (average)
  ArrayQ fxL = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*vL     , rhoL*uL*wL     , rhoL*uL*h0L};
  ArrayQ fyL = {rhoL*vL, rhoL*vL*uL     , rhoL*vL*vL + pL, rhoL*vL*wL     , rhoL*vL*h0L};
  ArrayQ fzL = {rhoL*wL, rhoL*wL*uL     , rhoL*wL*vL     , rhoL*wL*wL + pL, rhoL*wL*h0L};

  ArrayQ fxR = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*vR     , rhoR*uR*wR     , rhoR*uR*h0R};
  ArrayQ fyR = {rhoR*vR, rhoR*vR*uR     , rhoR*vR*vR + pR, rhoR*vR*wR     , rhoR*vR*h0R};
  ArrayQ fzR = {rhoR*wR, rhoR*wR*uR     , rhoR*wR*vR     , rhoR*wR*wR + pR, rhoR*wR*h0R};

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < ArrayQ::M; k++)
    fnTrue[k] = 0.5*(nx*(fxL[k] + fxR[k]) + ny*(fyL[k] + fyR[k]) + nz*(fzL[k] + fzR[k]));

  // Exact values for Roe scheme from Roe.nb
  fnTrue[0] -= 0.65558457182422097471369683881548;
  fnTrue[1] -= -1.8638923372862592525182437515145;
  fnTrue[2] -=  7.0186535948930936601201334787757;
  fnTrue[3] -=  6.8854604538052794714508533474917;
  fnTrue[4] -= 20.499927239215613126430884098724;

  ArrayQ fn;
  fn = 0;
  roe.fluxAdvectiveUpwind( x, y, z, time, qL, qR, nx, ny, nz, fn, 1 );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( fnTrue(3), fn(3), tol );
  BOOST_CHECK_CLOSE( fnTrue(4), fn(4), tol );
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux_DG_HDG_Roe )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef Roe2D<QTypePrimitiveRhoPressure, TraitsSizeEuler> RoeClass;
  typedef RoeClass::ArrayQ<Real> ArrayQ;
  typedef RoeClass::MatrixQ<Real> MatrixQ;

  const Real tol = 2.e-11;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  RoeClass roe2D(gas);
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Entropy);

  // Roe flux function test

  Real x, y, time;
  Real nx, ny;
  Real rhoL, uL, vL, tL, pL, h0L;
  Real rhoI, uI, vI, tI, pI, h0I;
  Real rhoR, uR, vR, tR, pR, h0R;
  Real Cp;

  x = y = time = 0;   // not actually used in functions
  nx = 2.0/7.0; ny = sqrt(1.0-nx*nx);
  rhoL = 1.034; uL = 1.6; vL = 0.85; tL = 5.78;
  rhoR = 0.973; uR = 1.79; vR =  0.93; tR = 6.13;

  Cp  = R*gamma/(gamma - 1);
  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL + vL*vL);
  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR + vR*vR);

  rhoI = 0.996454832345328;
  uI = 1.61894452004249;
  vI = 0.913541851924181;
  tI = 5.69508497216806;

  // set
  ArrayQ qL = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
  ArrayQ qR = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );
  ArrayQ qI = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rhoI, uI, vI, tI) );


  ArrayQ fnDG;
  fnDG = 0;
  roe2D.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fnDG );

  std::cout << fnDG << "\n";

  ArrayQ fnHDGL, fnHDGR;
  fnHDGL = 0;
  fnHDGR = 0;
  ArrayQ f, g;
  f = 0; g = 0;
//  pde.fluxAdvective(x,y,time,qL, f, g);
//  fnHDGL = f*nx + g*ny;
  roe2D.fluxAdvectiveUpwind( x, y, time, qL, qI, nx, ny, fnHDGL );
  roe2D.fluxAdvectiveUpwind( x, y, time, qI, qR, nx, ny, fnHDGR );
//
  std::cout << fnHDGL << "\n";
  std::cout << fnHDGR << "\n";

}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
