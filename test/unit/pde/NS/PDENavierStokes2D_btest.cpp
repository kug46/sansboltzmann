// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDENavierStokes2D_btest
//
// test of 2-D compressible Navier-Stokes PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/list.hpp>

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "Topology/Dimension.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/Q2DConservative.h"
//#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/PDENavierStokes2D.h"

//Explicitly instantiate the class so coverage information is correct
namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
//class QTypeEntropy {};

template class TraitsSizeNavierStokes<PhysD2>;
template class TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel>;
template class PDENavierStokes2D< TraitsSizeNavierStokes,
                                  TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> >;
template class PDENavierStokes2D< TraitsSizeNavierStokes,
                                  TraitsModelNavierStokes<QTypeConservative, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> >;
template class PDENavierStokes2D< TraitsSizeNavierStokes,
                                  TraitsModelNavierStokes<QTypePrimitiveSurrogate, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> >;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDENavierStokes2D_test_suite )


typedef boost::mpl::list< QTypePrimitiveRhoPressure,
                          QTypeConservative,
                          QTypePrimitiveSurrogate > QTypeList;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( PDEClass::D == 2 );
  BOOST_CHECK( PDEClass::N == 4 );
  BOOST_CHECK( ArrayQ::M == 4 );
  BOOST_CHECK( MatrixQ::M == 4 );
  BOOST_CHECK( MatrixQ::N == 4 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 4 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // function tests

  Real x, y, time;
  Real rho, u, v, t, p, e0, h0;

  x = 0; y = 0;   // not actually used in functions
  rho = 1.225;              // kg/m^3
  u = 69.784; v = -3.231;   // m/s
  t = 288.15;               // K
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u + v*v);
  h0 = Cp*t + 0.5*(u*u + v*v);

  // set
  Real qDataPrim[4] = {rho, u, v, t};
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY", "Temperature"};
  ArrayQ qTrue = {rho, u, v, p};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );

  // unsteady conservative flux
  Real uData[4] = {rho, rho*u, rho*v, rho*e0};
  ArrayQ uTrue(uData, 4);
  ArrayQ uCons = 0;
  pde.masterState( x, y, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );
  BOOST_CHECK_CLOSE( uTrue(3), uCons(3), tol );

  // unsteady conservative flux
  ArrayQ ftTrue(uData, 4);
  ArrayQ ft = 0;
  pde.fluxAdvectiveTime( x, y, time, q, ft );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( ftTrue(3), ft(3), tol );

  // advective flux
  Real fData[4] = {rho*u, rho*u*u + p, rho*u*v, rho*u*h0};
  Real gData[4] = {rho*v, rho*v*u, rho*v*v + p, rho*v*h0};
  ArrayQ fTrue(fData, 4);
  ArrayQ gTrue(gData, 4);
  ArrayQ f, g;
  f = 0;
  g = 0;
  pde.fluxAdvective( x, y, time, q, f, g );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( gTrue(1), g(1), tol );
  BOOST_CHECK_CLOSE( gTrue(2), g(2), tol );
  BOOST_CHECK_CLOSE( gTrue(3), g(3), tol );

  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;

  Real tx = px/(rho*R) - t/rho * rhox;
  Real ty = py/(rho*R) - t/rho * rhoy;

  ArrayQ qx = {rhox, ux, vx, px};
  ArrayQ qy = {rhoy, uy, vy, py};

  f = 0; fTrue = 0;
  g = 0; gTrue = 0;
  pde.fluxViscous(x, y, time, q, qx, qy, f, g);

  Real lambda = -2./3. * muRef;
  Real k = muRef * Cp / Prandtl;

  Real tauxx = muRef*(2*ux   ) + lambda*(ux + vy);
  Real tauxy = muRef*(uy + vx);
  Real tauyy = muRef*(2*vy   ) + lambda*(ux + vy);

  fTrue(1) -= tauxx;
  fTrue(2) -= tauxy;
  fTrue(3) -= k*tx + u*tauxx + v*tauxy;

  gTrue(1) -= tauxy;
  gTrue(2) -= tauyy;
  gTrue(3) -= k*ty + u*tauxy + v*tauyy;

  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( gTrue(1), g(1), tol );
  BOOST_CHECK_CLOSE( gTrue(2), g(2), tol );
  BOOST_CHECK_CLOSE( gTrue(3), g(3), tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( jacobianMasterState_test, QType, QTypeList )
{
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  const Real small_tol = 5.e-8;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // function tests

  Real rho, u, v, t;

  rho = 1.225;              // kg/m^3
  u = 6.974; v = -3.231;    // m/s
  t = 288.15;               // K

  Real x = 0, y = 0, time = 0;

  // set
  ArrayQ q;
  pde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );

  // conservative flux
  ArrayQ uCons0, uCons1;
  MatrixQ dudq_diff[2] = {0,0};

  std::vector<Real> step = {1e-3, 5e-4};
  std::vector<Real> rate_range = {1.9, 2.1};

  for (std::size_t istep = 0; istep < step.size(); istep++ )
  {
    for (int ivar = 0; ivar < ArrayQ::M; ivar++)
    {
      q[ivar] -= step[istep];
      uCons0 = 0;
      pde.masterState( x, y, time, q, uCons0 );

      q[ivar] += 2*step[istep];
      uCons1 = 0;
      pde.masterState( x, y, time, q, uCons1 );

      q[ivar] -= step[istep];

      for (int iEq = 0; iEq < pde.N; iEq++)
        dudq_diff[istep](iEq,ivar) = (uCons1[iEq] - uCons0[iEq])/(2*step[istep]);
    }
  }

  MatrixQ dudq = 0;
  pde.jacobianMasterState(x, y, time, q, dudq);

  for (int iEq = 0; iEq < pde.N; iEq++)
  {
    for (int ivar = 0; ivar < ArrayQ::M; ivar++)
    {
      Real err_vec[2] = { fabs( dudq(iEq,ivar) - dudq_diff[0](iEq,ivar) ),
                          fabs( dudq(iEq,ivar) - dudq_diff[1](iEq,ivar) )};
#if 0
      std::cout << "dudq(iEq,ivar) = " << dudq(iEq,ivar) <<
                   " dudq_diff[0] " << dudq_diff[0](iEq,ivar) <<
                   " dudq_diff[1] " << dudq_diff[1](iEq,ivar) << std::endl;
#endif
      // Error in finite-difference jacobian is either zero as for linear or quadratic residuals,
      // or is nonzero for general nonlinear residual where we need to check error convergence rate
      if (err_vec[0] > small_tol && err_vec[1] > small_tol)
      {
        Real rate = log(err_vec[1]/err_vec[0])/log(step[1]/step[0]);

        BOOST_CHECK_MESSAGE( rate >= rate_range[0] && rate <= rate_range[1],
                             "Rate check failed at : ivar = " << ivar <<
                             " iEq = " << iEq << ": rate = " << rate <<
                             ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" );
      }

    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( StrongViscousFlux )
{
//  typedef QTypePrimitiveRhoPressure QType;
//  typedef ViscosityModel_Const ViscosityModelType;
//  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
//  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
//  typedef PDEClass::ArrayQ<Real> ArrayQ;
//
//  const Real tol = 1.e-12;
//
//  const Real gamma = 1.4;
//  const Real R     = 287.04;        // J/(kg K)
//  const Real muRef = 1.789e-5;      // kg/(m s)
//  const Real Prandtl = 0.72;
//  const Real Cv = R/(gamma - 1);
//  const Real Cp = gamma*Cv;
//  GasModel gas(gamma, R);
//  ViscosityModel_Const visc(muRef);
//  ThermalConductivityModel tcond(Prandtl, Cp);
//  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );
//
//  // function tests
//
//  Real x, y, time;
//  Real rho, u, v, t, p;
//
//  x = 0; y = 0;   // not actually used in functions
//  rho = 1.225;              // kg/m^3
//  u = 69.784; v = -3.231;   // m/s
//  t = 288.15;               // K
//  p  = R*rho*t;
//
//  // set
//  Real qDataPrim[4] = {rho, u, v, t};
//  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY", "Temperature"};
//  Real qData[4] = {rho, u, v, p};
//  ArrayQ qTrue(qData, 4);
//  ArrayQ q;
//  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );
//  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
//  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
//  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
//  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
//
//  Real rhox = 0.01; Real rhoy = -0.025;
//  Real ux = 0.049; Real uy = 0.072;
//  Real vx = 0.14; Real vy = -0.024;
//  Real px = 0.002; Real py = -4.23;
//
//  Real rhoxx = -0.253; Real rhoxy = 0.782; Real rhoyy = 1.08;
//  Real uxx = 1.02; Real uxy = -0.95; Real uyy =-0.42;
//  Real vxx = 0.99; Real vxy = -0.92; Real vyy =-0.44;
//  Real pxx = 2.99; Real pxy = -1.92; Real pyy =-2.44;
//
//  ArrayQ qx = {rhox, ux, vx, px};
//  ArrayQ qy = {rhoy, uy, vy, py};
//  ArrayQ qxx = {rhoxx, uxx, vxx, pxx};
//  ArrayQ qxy = {rhoxy, uxy, vxy, pxy};
//  ArrayQ qyy = {rhoyy, uyy, vyy, pyy};
//
//  Real rhoxx2 = 0; Real uxx2 = 0; Real vxx2 = 0; Real txx2 = 0;
//  Real rhoxy2 = 0; Real uxy2 = 0; Real vxy2 = 0; Real txy2 = 0;
//  Real rhoyy2 = 0; Real uyy2 = 0; Real vyy2 = 0; Real tyy2 = 0;
//  pde.variableInterpreter().evalSecondGradient( q, qx, qy,
//                                                qxx, qxy, qyy,
//                                                rhoxx2, rhoxy2, rhoyy2,
//                                                uxx2, uxy2, uyy2,
//                                                vxx2, vxy2, vyy2,
//                                                txx2, txy2, tyy2);
//
//  ArrayQ strongVisc = 0;
//  ArrayQ strongViscTrue = 0;
//  pde.strongFluxViscous(x,y,time,q,qx,qy,qxx,qxy,qyy,strongVisc);
//
//  strongViscTrue(0) = 0;
//  strongViscTrue(1) = -0.000011330333333333333;
//  strongViscTrue(2) = -1.5504666666666666666e-6;
//  strongViscTrue(3) = 4.84821037760516;
//  BOOST_CHECK_CLOSE(strongViscTrue(0),strongVisc(0),tol);
//  BOOST_CHECK_CLOSE(strongViscTrue(1),strongVisc(1),tol);
//  BOOST_CHECK_CLOSE(strongViscTrue(2),strongVisc(2),tol);
//  BOOST_CHECK_CLOSE(strongViscTrue(3),strongVisc(3),tol);

  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R     = 0.4;
  const Real muRef = 0.1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // function tests

  Real x, y, time;
  Real rho, u, v, t, p;

  x = 0; y = 0;   // not actually used in functions
  rho = 1.00139;              // kg/m^3
  u = 0.20003946; v = 0.100128432;
  p  = 0.999755787;
  t = p/R/rho;

  // set
  Real qDataPrim[4] = {rho, u, v, t};
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY", "Temperature"};
  Real qData[4] = {rho, u, v, p};
  ArrayQ qTrue(qData, 4);
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );

  Real rhox = 0.0072; Real rhoy = 0.0043;
  Real ux = 0.0002992; Real uy = 0.0001464;
  Real vx = 0.00060743; Real vy = 0.00072036;
  Real px = -0.002; Real py = -0.00090897;

  Real rhoxx = 0.03; Real rhoxy = 0.008; Real rhoyy = 0.018;
  Real uxx = 0.002136; Real uxy = 0.000288; Real uyy = 0.001208;
  Real vxx = 0.004416; Real vxy = 0.0002484; Real vyy = 0.0051336;
  Real pxx = -0.0146; Real pxy = -0.0048; Real pyy = -0.0021794;

  ArrayQ qx = {rhox, ux, vx, px};
  ArrayQ qy = {rhoy, uy, vy, py};
  ArrayQ qxx = {rhoxx, uxx, vxx, pxx};
  ArrayQ qxy = {rhoxy, uxy, vxy, pxy};
  ArrayQ qyy = {rhoyy, uyy, vyy, pyy};

  Real rhoxx2 = 0; Real uxx2 = 0; Real vxx2 = 0; Real txx2 = 0;
  Real rhoxy2 = 0; Real uxy2 = 0; Real vxy2 = 0; Real txy2 = 0;
  Real rhoyy2 = 0; Real uyy2 = 0; Real vyy2 = 0; Real tyy2 = 0;
  pde.variableInterpreter().evalSecondGradient( q, qx, qy,
                                                qxx, qxy, qyy,
                                                rhoxx2, rhoxy2, rhoyy2,
                                                uxx2, uxy2, uyy2,
                                                vxx2, vxy2, vyy2,
                                                txx2, txy2, tyy2);

  ArrayQ strongVisc = 0;
  ArrayQ strongViscTrue = 0;
  pde.strongFluxViscous(x,y,time,q,qx,qy,qxx,qxy,qyy,strongVisc);

  strongViscTrue(0) = 0;
  strongViscTrue(1) = -0.00041388;
  strongViscTrue(2) = -0.00113568;
  strongViscTrue(3) = 0.0311258084936706959866;
  BOOST_CHECK_CLOSE(strongViscTrue(0),strongVisc(0),tol);
  BOOST_CHECK_CLOSE(strongViscTrue(1),strongVisc(1),tol);
  BOOST_CHECK_CLOSE(strongViscTrue(2),strongVisc(2),tol);
  BOOST_CHECK_CLOSE(strongViscTrue(3),strongVisc(3),tol);

  pde.strongFluxViscous(x,y,time,q,qx,qy,qxx,qxy,qyy,strongVisc);
  BOOST_CHECK_CLOSE(2*strongViscTrue(0),strongVisc(0),tol);
  BOOST_CHECK_CLOSE(2*strongViscTrue(1),strongVisc(1),tol);
  BOOST_CHECK_CLOSE(2*strongViscTrue(2),strongVisc(2),tol);
  BOOST_CHECK_CLOSE(2*strongViscTrue(3),strongVisc(3),tol);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( diffusionFluxGradient )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real close_tol = 1.e-10;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real tSuth = 110./300.; //somewhat random...
  const Real tRef  = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // data taken from RANS-SA; so use effective viscosity
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, time;
  Real rho, u, v, p;
  Real rhox, ux, vx, px;
  Real rhoy, uy, vy, py;

  x = 0.73; y = 36./99.; time = 0;   // not actually used in functions

  rho = 1.0074365505202103681442524417731;
  u = 0.10508967589710564917176487424421;
  v = 0.024335124272671981161838412026241;
  p = 1.0005144026446280991735537190083;

  rhox = 0.010748222028549962434259954921112;
  ux = 0.016369696969696969696969696969697;
  vx = 0.013539393939393939393939393939394;
  px = 0.0021139834710743801652892561983471;

  rhoy = 0.0011265417917355371900826446280992;
  uy = 0.003044338055883510428964974419520;
  vy = 0.018728366999391792780222532288648;
  py = 0.0028292145454545454545454545454545;

  // set
  ArrayQ q;
  pde.setDOFFrom( q, DensityVelocityPressure2D<Real>({rho, u, v, p}) );

  ArrayQ qx = {rhox, ux, vx, px};
  ArrayQ qy = {rhoy, uy, vy, py};

  // Not used for NS
  ArrayQ qxx = {0, 0, 0, 0};
  ArrayQ qxy = {0, 0, 0, 0};
  ArrayQ qyy = {0, 0, 0, 0};

  MatrixQ kxx_x = 0;
  MatrixQ kxy_x = 0;
  MatrixQ kyx_x = 0;
  MatrixQ kxy_y = 0;
  MatrixQ kyx_y = 0;
  MatrixQ kyy_y = 0;

  pde.diffusionViscousGradient(x, y, time, q, qx, qy, qxx, qxy, qyy, kxx_x, kxy_x, kyx_x, kxy_y, kyx_y, kyy_y);

  MatrixQ kxx_xTrue = {{0,0,0,0},
                       {-0.01916350189272866572345465695878,-0.02271474270572182294193106976073,0,0},
                       {-0.01295381057145772613119409714184,0,-0.01703605702929136720644830232055,0},
                       {0.1218526647072586670454630104683,-0.00878327170083397178991671777278,
                        -0.01223415442859896356834998063396,-0.03312566644584432512364947673440}};

  MatrixQ kxy_xTrue = {{0,0,0,0},
                       {0.008635873714305150754129398094559,0,0.01135737135286091147096553488037,0},
                       {-0.01437262641954649929259099271909,-0.01703605702929136720644830232055,0,0},
                       {-0.0005848796370981355424280488862474,0.01295381057145772613119409714184,-0.00958175094636433286172732847939,0}};

  MatrixQ kyx_yTrue = {{0,0,0,0},
                       {-0.01849656488556001382274975945456,0,0.000195019227515653240457845990960,0},
                       {0.002017587444424500689058855596261,-0.000130012818343768826971897327307,0,0},
                       {-0.0006723155448116852693604683361807,-0.012331043257040009215166506303041,0.003026381166636751033588283394391,0}};

  MatrixQ kyy_yTrue = {{0,0,0,0},
                       {-0.003026381166636751033588283394391,0.000195019227515653240457845990960,0,0},
                       {-0.02466208651408001843033301260608,0,0.000260025636687537653943794654614,0},
                       {-0.00943304092100333625154298491142,-0.002858248879601375976166712094703,
                        -0.01130345631895334178056929744445,0.00037920405350265907866803387131}};

  for (int i=0; i < 3; i++)
    for (int j=0; j < 3; j++)
    {
      BOOST_CHECK_CLOSE(kxx_x(i,j), kxx_xTrue(i,j), close_tol);
      BOOST_CHECK_CLOSE(kxy_x(i,j), kxy_xTrue(i,j), close_tol);
      BOOST_CHECK_CLOSE(kyx_y(i,j), kyx_yTrue(i,j), close_tol);
      BOOST_CHECK_CLOSE(kyy_y(i,j), kyy_yTrue(i,j), close_tol);
    }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxRoe )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real tol = 2.e-11;

  const Real gamma = 1.4;
  const Real R     = 0.4;           // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // Roe flux function test (same as Euler Roe test, from Roe2D.nb)

  Real x, y, time;
  Real nx, ny;
  Real rhoL, uL, vL, tL, pL, h0L;
  Real rhoR, uR, vR, tR, pR, h0R;

  x = y = time = 0;   // not actually used in functions
  nx = 2.0/7.0; ny = sqrt(1.0-nx*nx);
  rhoL = 1.034; uL = 3.26; vL = -2.17; tL = 5.78;
  rhoR = 0.973; uR = 1.79; vR =  0.93; tR = 6.13;
  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL + vL*vL);
  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR + vR*vR);

  // set
  ArrayQ qL = 0;
  ArrayQ qR = 0;

  pde.setDOFFrom( qL, DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
  pde.setDOFFrom( qR, DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );

  // advective normal flux (average)
  Real fL[4] = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*vL     , rhoL*uL*h0L};
  Real fR[4] = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*vR     , rhoR*uR*h0R};
  Real gL[4] = {rhoL*vL, rhoL*vL*uL     , rhoL*vL*vL + pL, rhoL*vL*h0L};
  Real gR[4] = {rhoR*vR, rhoR*vR*uR     , rhoR*vR*vR + pR, rhoR*vR*h0R};

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < 4; k++)
    fnTrue[k] = 0.5*(nx*(fL[k] + fR[k]) + ny*(gL[k] + gR[k]));

#if 1
  // Exact values for Roe scheme from Roe2D.nb
  fnTrue[0] -= -0.021574952725013058474459426733768;
  fnTrue[1] -= -1.7958753728220592172800048653566;
  fnTrue[2] -=  4.4515996249161671139371535471951;
  fnTrue[3] -= -6.8094427666218353919034995028459;
#endif

  ArrayQ fn;
  fn = 0;
  pde.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( fnTrue(3), fn(3), tol );

  pde.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(3), fn(3), tol );

  MatrixQ mtx;
  mtx = 0;

  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, time, qL, nx, ny, mtx );
  MatrixQ mtxTrue = {
    {1.0323434331949190979834072919713,
        -0.44459540501697074517797177104962,
        -0.43697991171460424043588329213258,
        0.080449362812368834954522463171426},
    {-0.72312835498151845646006425483631,
        -0.010364873336226558791267034135421,
        -1.4045718375886762151895973056639,
        0.18933272986886306466125181349454},
    {-0.90833712876943321732424020657867,
        1.9404853199183346620104522800780,
        2.1633849842368432379888624467138,
        -0.41919712890981821676619255996783},
    {-0.43536491993382434782224675355112,
        -8.1758936855015456959218531030353,
        -6.9672110585297722485152708900077,
        2.7090875342447734040794006013828}
                    };
  // Values taken from pde/NS/Roe2D.nb

  for (int i=0; i<4; i++)
    for (int j=0; j<4; j++)
      BOOST_CHECK_CLOSE( mtx(i,j), mtxTrue(i,j), tol );


  // Call these simply to make sure coverage is high
  ArrayQ qxL, qxR, qyL, qyR, sourceL, sourceR;
  Real dummy = 0;
  pde.sourceTrace( dummy, dummy, dummy, dummy, dummy, qL, qxL, qyL, qR, qxR, qyR, sourceL, sourceR );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( viscous )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, time;
  Real rho, u, v, t, p, e0;
  Real mu, lambda, k;
  Real tauxx, tauxy, tauyy;

  x = 0; y = 0;   // not actually used in functions
  rho = 1.225;              // kg/m^3
  u = 69.784; v = -3.231;   // m/s
  t = 288.15;               // K
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u + v*v);

  mu = visc.viscosity(t);
  k  = tcond.conductivity(mu);
  lambda = -(2./3.)*mu;

  Real nx = 0.35;
  Real ny = -0.78;

  // gradient
  Real rhox, ux, vx, tx, px;
  Real rhoy, uy, vy, ty, py;

  rhox = 0.37, ux = -0.85, vx = 0.09, px = 1.71;
  rhoy = 2.31, uy =  1.65, vy = 0.87, py = 0.29;
  tx = t*(px/p - rhox/rho);
  ty = t*(py/p - rhoy/rho);

  // viscous shear stress
  tauxx = mu*(2*ux   ) + lambda*(ux + vy) ;
  tauxy = mu*(uy + vx) ;
  tauyy = mu*(2*vy   ) + lambda*(ux + vy) ;

  // set
  Real qDataPrim[4] = {rho, u, v, t};
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY", "Temperature"};
  ArrayQ qTrue = {rho, u, v, p};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );

  BOOST_CHECK_CLOSE( qTrue[0], q[0], tol );
  BOOST_CHECK_CLOSE( qTrue[1], q[1], tol );
  BOOST_CHECK_CLOSE( qTrue[2], q[2], tol );
  BOOST_CHECK_CLOSE( qTrue[3], q[3], tol );

  ArrayQ qx = {rhox, ux, vx, px};
  ArrayQ qy = {rhoy, uy, vy, py};

  // viscous flux
  Real fData[4] = {0, -tauxx, -tauxy, -(k*tx + u*tauxx + v*tauxy)};
  Real gData[4] = {0, -tauxy, -tauyy, -(k*ty + u*tauxy + v*tauyy)};
  ArrayQ fTrue(fData, 4);
  ArrayQ gTrue(gData, 4);
  ArrayQ f, g;
  f = 0;
  g = 0;
  pde.fluxViscous( x, y, time, q, qx, qy, f, g );

  for (int i=0; i<4; i++)
  {
    BOOST_CHECK_CLOSE( fTrue(i), f(i), tol );
    BOOST_CHECK_CLOSE( gTrue(i), g(i), tol );
  }

  pde.fluxViscous( x, y, time, q, qx, qy, f, g );
  for (int i=0; i<4; i++)
  {
    BOOST_CHECK_CLOSE( 2*fTrue(i), f(i), tol );
    BOOST_CHECK_CLOSE( 2*gTrue(i), g(i), tol );
  }

  Real rhoL = 1.225;              // kg/m^3
  Real uL = 69.784, vL = -3.231;   // m/s
  Real tL = 288.15;               // K
  Real pL  = R*rhoL*tL;

  Real rhoR = 1.253;              // kg/m^3
  Real uR = 71.381, vR = -2.107;   // m/s
  Real tR = 278.35;               // K
  Real pR  = R*rhoR*tR;

  Real rhoxL = 0.37, uxL = -0.85, vxL = 0.09, pxL = 1.71;
  Real rhoyL = 2.31, uyL =  1.65, vyL = 0.87, pyL = 0.29;

  Real rhoxR = 0.41, uxR = -0.25, vxR = -0.03, pxR = 0.61;
  Real rhoyR = 1.28, uyR =  0.21, vyR =  1.06, pyR = 0.49;

  ArrayQ qL = {rhoL, uL, vL, pL};
  ArrayQ qR = {rhoR, uR, vR, pR};
  ArrayQ qxL = {rhoxL, uxL, vxL, pxL};
  ArrayQ qyL = {rhoyL, uyL, vyL, pyL};
  ArrayQ qxR = {rhoxR, uxR, vxR, pxR};
  ArrayQ qyR = {rhoyR, uyR, vyR, pyR};

  Real txL = tL*(pxL/pL - rhoxL/rhoL);
  Real tyL = tL*(pyL/pL - rhoyL/rhoL);
  Real txR = tR*(pxR/pR - rhoxR/rhoR);
  Real tyR = tR*(pyR/pR - rhoyR/rhoR);

  Real muL = visc.viscosity(tL);
  Real kL  = tcond.conductivity(muL);
  Real lambdaL = -(2./3.)*muL;

  Real muR = visc.viscosity(tR);
  Real kR  = tcond.conductivity(muR);
  Real lambdaR = -(2./3.)*muR;

  // viscous shear stress
  Real tauxxL = muL*(2*uxL   ) + lambdaL*(uxL + vyL) ;
  Real tauxyL = muL*(uyL + vxL) ;
  Real tauyyL = muL*(2*vyL   ) + lambdaL*(uxL + vyL) ;

  Real tauxxR = muR*(2*uxR   ) + lambdaR*(uxR + vyR) ;
  Real tauxyR = muR*(uyR + vxR) ;
  Real tauyyR = muR*(2*vyR   ) + lambdaR*(uxR + vyR) ;

  Real fLData[4] = {0, -tauxxL, -tauxyL, -(kL*txL + uL*tauxxL + vL*tauxyL)};
  Real gLData[4] = {0, -tauxyL, -tauyyL, -(kL*tyL + uL*tauxyL + vL*tauyyL)};
  Real fRData[4] = {0, -tauxxR, -tauxyR, -(kR*txR + uR*tauxxR + vR*tauxyR)};
  Real gRData[4] = {0, -tauxyR, -tauyyR, -(kR*tyR + uR*tauxyR + vR*tauyyR)};
  ArrayQ fLTrue(fLData, 4);
  ArrayQ gLTrue(gLData, 4);
  ArrayQ fRTrue(fRData, 4);
  ArrayQ gRTrue(gRData, 4);

  ArrayQ fnTrue = 0.5*(fLTrue + fRTrue)*nx + 0.5*(gLTrue + gRTrue)*ny;
  ArrayQ fn;
  fn = 0;
  pde.fluxViscous( x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );
  for (int i=0; i<4; i++)
    BOOST_CHECK_CLOSE( fnTrue(i), fn(i), tol );

  pde.fluxViscous( x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );
  for (int i=0; i<4; i++)
    BOOST_CHECK_CLOSE( 2*fnTrue(i), fn(i), tol );

  // viscous diffusion matrix
  Real kxxData[16] = {
    0, 0, 0, 0,
    -u*(2*mu + lambda)/rho, (2*mu + lambda)/rho, 0, 0,
    -v*mu/rho, 0, mu/rho, 0,
    -u*u*((2*mu + lambda) - k/Cv)/rho - v*v*(mu - k/Cv)/rho - e0*k/(Cv*rho),
       u*((2*mu + lambda) - k/Cv)/rho,    v*(mu - k/Cv)/rho,     k/(Cv*rho) };
  Real kxyData[16] = {
    0, 0, 0, 0,
    -v*lambda/rho, 0, lambda/rho, 0,
    -u*mu/rho, mu/rho, 0, 0,
    -u*v*(mu + lambda)/rho, v*mu/rho, u*lambda/rho, 0 };
  Real kyxData[16] = {
    0, 0, 0, 0,
    -v*mu/rho, 0, mu/rho, 0,
    -u*lambda/rho, lambda/rho, 0, 0,
    -u*v*(mu + lambda)/rho, v*lambda/rho, u*mu/rho };
  Real kyyData[16] = {
    0, 0, 0, 0,
    -u*mu/rho, mu/rho, 0, 0,
    -v*(2*mu + lambda)/rho, 0, (2*mu + lambda)/rho, 0,
    -u*u*(mu - k/Cv)/rho - v*v*((2*mu + lambda) - k/Cv)/rho - e0*k/(Cv*rho),
       u*(mu - k/Cv)/rho,    v*((2*mu + lambda) - k/Cv)/rho,     k/(Cv*rho) };
  MatrixQ kxxTrue(kxxData, 16);
  MatrixQ kxyTrue(kxyData, 16);
  MatrixQ kyxTrue(kyxData, 16);
  MatrixQ kyyTrue(kyyData, 16);
  MatrixQ kxx=0, kxy=0, kyx=0, kyy=0;
  pde.diffusionViscous( x, y, time, q, qx, qy, kxx, kxy, kyx, kyy );

  for (int i = 0; i < 4; i++)
  {
    for (int j = 0; j < 4; j++)
    {
      BOOST_CHECK_CLOSE( kxxTrue(i,j), kxx(i,j), (kxxTrue(i,j) == 0) ? tol : tol );
      BOOST_CHECK_CLOSE( kxyTrue(i,j), kxy(i,j), (kxyTrue(i,j) == 0) ? tol : tol );
      BOOST_CHECK_CLOSE( kyxTrue(i,j), kyx(i,j), (kyxTrue(i,j) == 0) ? tol : tol );
      BOOST_CHECK_CLOSE( kyyTrue(i,j), kyy(i,j), (kyyTrue(i,j) == 0) ? tol : tol );
    }
  }

  // Call these simply to make sure coverage is high
  ArrayQ qp = 0, qpx = 0, qpy = 0, source = 0;
  Real dummy = 0;
  pde.source( dummy, dummy, dummy, q, qx, qy, source );
  pde.source( dummy, dummy, dummy, q, qp, qx, qy, qpx, qpy, source );
//  pde.forcingFunction( x, y, time, source );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jacobianFluxViscous )
{
  // Use conservative variables here in order to differentiate with Surreal
  typedef QTypeConservative QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef SurrealS<PDEClass::N> SurrealClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;
  typedef PDEClass::ArrayQ<SurrealClass> ArrayQSurreal;

  const Real small_tol = 1.e-12;
  const Real close_tol = 1.e-11;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real tSuth = 110./300.; //somewhat random...
  const Real tRef  = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  //pde.dump(0);
  bool verbose = false;

  Real x, y, time;
  Real rho, u, v, t;
  x = 0; y = 0; time = 0;   // not actually used in functions
  rho = 1.225;
  u = 6.974;
  v = -3.231;
  t = 2.8815;

  Real e = gas.energy(rho, t);
  Real E = e + 0.5*(u*u + v*v);

  Real rhox = 0.0421, rhoy = 0.0127;
  Real ux = 0.0521, uy = -0.0124;
  Real vx = -0.0312, vy = -0.0022;
  Real tx = -0.0715, ty = -0.0111;

  Real rhoux = rhox*u + ux*rho;
  Real rhouy = rhoy*u + uy*rho;

  Real rhovx = rhox*v + vx*rho;
  Real rhovy = rhoy*v + vy*rho;

  Real ex, ey;
  gas.energyGradient(rho, t, rhox, tx, ex);
  gas.energyGradient(rho, t, rhoy, ty, ey);

  Real Ex = ex + ux*u + vx*v;
  Real Ey = ey + uy*u + vy*v;

  Real rhoEx = rhox*E + rho*Ex;
  Real rhoEy = rhoy*E + rho*Ey;

  ArrayQ q, qx, qy;
  MatrixQ dfdu;
  MatrixQ dgdu;
  ArrayQSurreal qS;
  ArrayQSurreal fv;
  ArrayQSurreal gv;

  SurrealClass rhoS  = rho;
  SurrealClass rhouS = rho*u;
  SurrealClass rhovS = rho*v;
  SurrealClass rhoES = rho*E;

  rhoS.deriv(0)   = 1;
  rhouS.deriv(1)  = 1;
  rhovS.deriv(2)  = 1;
  rhoES.deriv(3)  = 1;

  // set the complete state vectors
  pde.setDOFFrom( q,Conservative2D<Real>({rho , rho*u, rho*v, rho*E }));
  pde.setDOFFrom(qS, Conservative2D<SurrealClass>({rhoS, rhouS, rhovS, rhoES }));

  qx = {rhox, rhoux, rhovx, rhoEx};
  qy = {rhoy, rhouy, rhovy, rhoEy};

  // Compute the true jacobian with Surreals
  fv = 0;
  gv = 0;
  pde.fluxViscous( x, y, time, qS, qx, qy, fv, gv );

  // Compute the hand coded Jacobian
  dfdu = 0;
  dgdu = 0;
  pde.jacobianFluxViscous( x, y, time, q, qx, qy, dfdu, dgdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << j << ") = " << fv(i).deriv(j) << " | " << dfdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( fv(i).deriv(j), dfdu(i,j), small_tol, close_tol );
//
      if (verbose) std::cout << " src(" << i << ").deriv(" << j << ") = " << gv(i).deriv(j) << " | " << dgdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( gv(i).deriv(j), dgdu(i,j), small_tol, close_tol );
    }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( diffusionViscous )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  //pde.dump(0);

  Real x, y, time;
  Real rho, u, v, t;

  x = 0; y = 0; time = 0;   // not actually used in functions
  rho = 1.225;
  u = 6.974;
  v = -3.231;
  t = 2.881;

  ArrayQ q;
  Real qDataPrim[4] = {rho, u, v, t};
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY", "Temperature"};
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );

  // gradient

  Real rhox = 0.37, ux = -0.85, vx = 0.09, px = 1.71;
  Real rhoy = 2.31, uy =  1.65, vy = 0.87, py = 0.29;

  ArrayQ qx = {rhox, ux, vx, px};
  ArrayQ qy = {rhoy, uy, vy, py};


  MatrixQ kxx = 0, kxy = 0, kyx = 0, kyy = 0;

  pde.diffusionViscous( x, y, time, q, qx, qy, kxx, kxy, kyx, kyy );

  // see PDERANSSA2D.nb for data (nt = 0 case)
  MatrixQ kxxTrue =
    {{0, 0, 0, 0},
     {-7.590748299319728, 1.088435374149660, 0, 0},
     {2.637551020408163, 0, 0.8163265306122449, 0},
     {-26.00660201814059, -3.479092970521542, 2.491020408163265, 1.587301587301587}};

  MatrixQ kxyTrue =
    {{0, 0, 0, 0},
     {-1.758367346938776, 0, -0.5442176870748299, 0},
     {-5.693061224489796, 0.8163265306122449, 0, 0},
     {6.131426938775510, -2.637551020408163, -3.795374149659864, 0}};

  MatrixQ kyxTrue =
    {{0, 0, 0, 0},
     {2.637551020408163, 0, 0.8163265306122449, 0},
     {3.795374149659864, -0.5442176870748299, 0, 0},
     {6.131426938775510, 1.758367346938776, 5.693061224489796, 0}};

  MatrixQ kyyTrue =
    {{0, 0, 0, 0},
     {-5.693061224489796, 0.8163265306122449, 0, 0},
     {3.516734693877551, 0, 1.088435374149660, 0},
     {-15.61277480725624, -5.376780045351474, 1.611836734693878, 1.587301587301587}};

  for (int i = 0; i < 4; i++)
  {
    for (int j = 0; j < 4; j++)
    {
      SANS_CHECK_CLOSE( kxxTrue(i,j), kxx(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kxyTrue(i,j), kxy(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kyxTrue(i,j), kyx(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kyyTrue(i,j), kyy(i,j), small_tol, close_tol );
    }
  }

  MatrixQ dudq = 0;
  pde.jacobianMasterState(x, y, time, q, dudq);

  // check that viscous flux is consistent with diffusionViscous

  ArrayQ fTrue = 0;
  ArrayQ gTrue = 0;
  pde.fluxViscous(x, y, time, q, qx, qy, fTrue, gTrue);

  ArrayQ f = -kxx*dudq*qx - kxy*dudq*qy;
  ArrayQ g = -kyx*dudq*qx - kyy*dudq*qy;

  for (int i = 0; i < ArrayQ::M; i++)
  {
    SANS_CHECK_CLOSE( fTrue[i], f[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( gTrue[i], g[i], small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;
  const Real tSutherland = 0.5;
  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, time, dx, dy;
  Real rho, u, v, t, c;
  Real speed, speedTrue;

  x = 0; y = 0; time = 0;  // not actually used in functions
  dx = 0.57; dy = -0.43;
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  c = sqrt(gamma*R*t);
  speedTrue = fabs(dx*u + dy*v)/sqrt(dx*dx + dy*dy) + c;

  Real qDataPrim[4] = {rho, u, v, t};
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY", "Temperature"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );

  pde.speedCharacteristic( x, y, time, dx, dy, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  pde.speedCharacteristic( x, y, time, q, speed );
  BOOST_CHECK_CLOSE( sqrt(u*u+v*v) + c, speed, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;
  const Real tSutherland = 0.5;
  const Real tRef = 1.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  ArrayQ q;
  q(0) =  1;
  q(3) =  1;
  BOOST_CHECK( pde.isValidState(q) == true );

  q(0) =  1;
  q(3) = -1;
  BOOST_CHECK( pde.isValidState(q) == false );

  q(0) = -1;
  q(3) =  1;
  BOOST_CHECK( pde.isValidState(q) == false );

  q(0) = -1;
  q(3) = -1;
  BOOST_CHECK( pde.isValidState(q) == false );

  q(0) =  1;
  q(3) =  0;
  BOOST_CHECK( pde.isValidState(q) == false );

  q(0) =  0;
  q(3) =  1;
  BOOST_CHECK( pde.isValidState(q) == false );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/PDENavierStokes2D_pattern.txt", true  );

  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real tSutherland = 110;     // K
  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Sutherland visc(muRef, tRef, tSutherland);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  pde.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
