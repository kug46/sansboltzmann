// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// GasModel_Surreal1_btest
// testing of GasModel, ViscosityModel, ThermalConductivityModel classes
// with Surreal1 elements

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/NS/GasModel.h"         // GasModel, ViscosityModel, ThermalConductivityModel
#include "Surreal/SurrealS.h"


using namespace std;
using namespace SANS;

namespace SANS
{
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( GasModel_Surreal1_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_copy )
{
  const Real tol = 1.e-13;
  Real gamma, R, Cv, Cp;

  gamma = 1.4;
  R     = 287.04;       // J/(kg K)
  Cv    = R/(gamma - 1);
  Cp    = gamma*Cv;

  PyDict d;
  d[GasModelParams::params.gamma] = gamma;
  d[GasModelParams::params.R] = R;

  //Check that the dict is ok
  GasModelParams::checkInputs(d);

  // constructor
  GasModel gas(gamma, R);
  BOOST_CHECK_CLOSE( gamma, gas.gamma(), tol );
  BOOST_CHECK_CLOSE( R , gas.R() , tol );
  BOOST_CHECK_CLOSE( Cv, gas.Cv(), tol );
  BOOST_CHECK_CLOSE( Cp, gas.Cp(), tol );

  // copy constructor
  GasModel gas2(gas);
  BOOST_CHECK_CLOSE( gamma, gas2.gamma(), tol );
  BOOST_CHECK_CLOSE( R , gas2.R() , tol );
  BOOST_CHECK_CLOSE( Cv, gas2.Cv(), tol );
  BOOST_CHECK_CLOSE( Cp, gas2.Cp(), tol );

  // PyDict constructor
  GasModel gas3(d);
  BOOST_CHECK_CLOSE( gamma, gas3.gamma(), tol );
  BOOST_CHECK_CLOSE( R, gas3.R(), tol );
  BOOST_CHECK_CLOSE( Cv, gas3.Cv(), tol );
  BOOST_CHECK_CLOSE( Cp, gas3.Cp(), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jacobian )
{
  const Real tol = 1.e-13;
  Real gamma, R, Cv, Cp;
  SurrealS<1> rho;            // density
  SurrealS<1> t, t_rho, t_e;  // temperature
  SurrealS<1> p, p_rho, p_t;  // pressure
  SurrealS<1> e, e_rho, e_t;  // static internal energy
  SurrealS<1> h, h_rho, h_t;  // static enthalpy
  SurrealS<1> c, c_t;         // speed of sound

  gamma = 1.4;
  R     = 287.04;       // J/(kg K)
  Cv    = R/(gamma - 1);
  Cp    = gamma*Cv;

  GasModel gas(gamma, R);

  rho = 1.225;          // kg/m^3 (standard atmosphere)
  t   = 288.15;         // K (standard atmosphere)
  rho.deriv() = 3;
  t.deriv() = 5;
  p   = R*rho*t;
  e   = Cv*t;
  h   = Cp*t;
  c   = sqrt(gamma*R*t);

  BOOST_CHECK_CLOSE( p.value(), gas.pressure(rho, t).value(), tol );
  BOOST_CHECK_CLOSE( p.deriv(), gas.pressure(rho, t).deriv(), tol );

  BOOST_CHECK_CLOSE( e.value(), gas.energy(rho, t).value(), tol );
  BOOST_CHECK_CLOSE( e.deriv(), gas.energy(rho, t).deriv(), tol );

  BOOST_CHECK_CLOSE( h.value(), gas.enthalpy(rho, t).value(), tol );
  BOOST_CHECK_CLOSE( h.deriv(), gas.enthalpy(rho, t).deriv(), tol );

  BOOST_CHECK_CLOSE( c.value(), gas.speedofSound(t).value(), tol );
  BOOST_CHECK_CLOSE( c.deriv(), gas.speedofSound(t).deriv(), tol );

  p_rho = R*t;
  p_t   = R*rho;
  e_rho = 0;
  e_t   = Cv;
  h_rho = 0;
  h_t   = Cp;
  c_t   = 0.5*gamma*R/c;

  SurrealS<1> a(1), b(1);
  gas.pressureJacobian(rho, t, a, b);
  BOOST_CHECK_CLOSE( p_rho.value(), a.value(), tol );
  BOOST_CHECK_CLOSE( p_rho.deriv(), a.deriv(), tol );

  BOOST_CHECK_CLOSE( p_t.value(),   b.value(), tol );
  BOOST_CHECK_CLOSE( p_t.deriv(),   b.deriv(), tol );

  gas.energyJacobian(rho, t, a, b);
  BOOST_CHECK_CLOSE( e_rho.value(), a.value(), tol );
  BOOST_CHECK_CLOSE( e_rho.deriv(), a.deriv(), tol );

  BOOST_CHECK_CLOSE( e_t.value(),   b.value(), tol );
  BOOST_CHECK_CLOSE( e_t.deriv(),   b.deriv(), tol );

  gas.enthalpyJacobian(rho, t, a, b);
  BOOST_CHECK_CLOSE( h_rho.value(), a.value(), tol );
  BOOST_CHECK_CLOSE( h_rho.deriv(), a.deriv(), tol );

  BOOST_CHECK_CLOSE( h_t.value(),   b.value(), tol );
  BOOST_CHECK_CLOSE( h_t.deriv(),   b.deriv(), tol );

  gas.enthalpyJacobian(rho, t, a, b);
  BOOST_CHECK_CLOSE( h_rho.value(), a.value(), tol );
  BOOST_CHECK_CLOSE( h_rho.deriv(), a.deriv(), tol );

  BOOST_CHECK_CLOSE( h_t.value(),   b.value(), tol );
  BOOST_CHECK_CLOSE( h_t.deriv(),   b.deriv(), tol );

  gas.speedofSoundJacobian(t, a);
  BOOST_CHECK_CLOSE( c_t.value(),   a.value(), tol );
  BOOST_CHECK_CLOSE( c_t.deriv(),   a.deriv(), tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()

//############################################################################//
BOOST_AUTO_TEST_SUITE( ViscosityModel_Surreal1_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_copy )
{
  const Real tol = 1.e-13;
  Real muRef, tSuth, tRef;
  SurrealS<1> t, mu;

  muRef = 1.789e-5;     // kg/(m s) (standard atmosphere)
  tSuth = 110;          // K
  tRef = 300;

  t = 288.15;           // K (standard atmosphere)
  mu = muRef * t*sqrt(t) * (tRef + tSuth)/(t + tSuth) / (tRef*sqrt(tRef));

  // constructor
  ViscosityModel_Sutherland visc(muRef, tRef, tSuth);
  BOOST_CHECK_CLOSE( mu.value(), visc.viscosity(t).value(), tol );

  // copy constructor
  ViscosityModel_Sutherland visc2(visc);
  BOOST_CHECK_CLOSE( mu.value(), visc2.viscosity(t).value(), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jacobian )
{
  Real tol = 1.e-12;
  Real muRef, tSuth, tRef;
  SurrealS<1> t, mu, mu_t;

  muRef = 1.789e-5;     // kg/(m s) (standard atmosphere)
  tSuth = 110;          // K
  tRef = 300;

  t = 288.15;           // K (standard atmosphere)
  t.deriv() = 5;
  mu = muRef * t*sqrt(t) * (tRef + tSuth)/(t + tSuth) / (tRef*sqrt(tRef));
  mu_t = muRef*( (sqrt(t) + t*(0.5/sqrt(t)))*(tRef + tSuth)/(t + tSuth)/ (tRef*sqrt(tRef)) +
                 t*sqrt(t)*(tRef + tSuth)*(-1./((t + tSuth)*(t + tSuth)))/ (tRef*sqrt(tRef)) );

  ViscosityModel_Sutherland visc(muRef, tRef, tSuth);

  SurrealS<1> a;
  visc.viscosityJacobian(t, a);
  BOOST_CHECK_CLOSE( mu_t.value(), a.value(), tol );
  BOOST_CHECK_CLOSE( mu_t.deriv(), a.deriv(), tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()

//############################################################################//
BOOST_AUTO_TEST_SUITE( ThermalConductivityModel_Surreal1_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_copy )
{
  const Real tol = 1.e-13;
  Real Prandtl, gamma, R, Cp;
  SurrealS<1> mu, k;

  Prandtl = 0.72;
  gamma = 1.4;
  R     = 287.04;       // J/(kg K)
  Cp    = R*gamma/(gamma - 1);

  mu = 1.789e-5;        // kg/(m s) (standard atmosphere)
  mu.deriv() = 5;
  k  = mu*Cp/Prandtl;

  // constructor
  ThermalConductivityModel cond(Prandtl, Cp);
  BOOST_CHECK_CLOSE( k.value(), cond.conductivity(mu).value(), tol );
  BOOST_CHECK_CLOSE( k.deriv(), cond.conductivity(mu).deriv(), tol );

  // copy constructor
  ThermalConductivityModel cond2(cond);
  BOOST_CHECK_CLOSE( k.value(), cond2.conductivity(mu).value(), tol );
  BOOST_CHECK_CLOSE( k.deriv(), cond2.conductivity(mu).deriv(), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jacobian )
{
  const Real tol = 1.e-13;
  Real Prandtl, gamma, R, Cp;
  SurrealS<1> mu, k, k_mu;

  Prandtl = 0.72;
  gamma = 1.4;
  R     = 287.04;       // J/(kg K)
  Cp    = R*gamma/(gamma - 1);

  mu = 1.789e-5;        // kg/(m s) (standard atmosphere)
  k  = mu*Cp/Prandtl;
  k_mu = Cp/Prandtl;

  ThermalConductivityModel cond(Prandtl, Cp);

  SurrealS<1> a;
  cond.conductivityJacobian(mu, a);
  BOOST_CHECK_CLOSE( k_mu.value(), a.value(), tol );
  BOOST_CHECK_CLOSE( k_mu.deriv(), a.deriv(), tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
