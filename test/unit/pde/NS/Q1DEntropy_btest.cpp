// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Q1DEntropy_btest
// testing of Q1D<QTypeEntropy, TraitsSizeEuler> class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/NS/Q1DEntropy.h"
#include "pde/NS/TraitsEuler.h"
#include "Surreal/SurrealS.h"

#include <string>
#include <iostream>


//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
template class Q1D<QTypeEntropy, TraitsSizeEuler>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Q1DEntropy_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test )
{
  const Real tol = 1.e-12;

  typedef Q1D<QTypeEntropy, TraitsSizeEuler> QInterpret;
  BOOST_CHECK( QInterpret::N == 3 );

  typedef QInterpret::ArrayQ<Real> ArrayQ;
  BOOST_CHECK( ArrayQ::M == 3 );

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  // set/eval
  ArrayQ q;
  Real rho, u, t, p;
  Real rho1, u1, t1;
  Real rho2, u2, t2;

  rho = 1.225;          // kg/m^3 (standard atmosphere)
  t   = 288.15;         // K (standard atmosphere)
  u   = 15.03;          // m/s
  p   = R*rho*t;

  Real qDataPrim[3] = {rho, u, t};
  string qNamePrim[3] = {"Density", "VelocityX", "Temperature"};
  qInterpret.setFromPrimitive( q, qDataPrim, qNamePrim, 3 );
  qInterpret.eval( q, rho1, u1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );

  qInterpret.evalDensity( q, rho1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );

  qInterpret.evalTemperature( q, t1 );
  BOOST_CHECK_CLOSE( t, t1, tol );

  qInterpret.evalVelocity( q, u1 );
  BOOST_CHECK_CLOSE( u, u1, tol );

  DensityVelocityPressure1D<Real> prim1(rho, u, p);
  q = 1;
  qInterpret.setFromPrimitive( q, prim1 );
  qInterpret.eval( q, rho1, u1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );

  DensityVelocityTemperature1D<Real> prim2(rho, u, t);
  q = 2;
  qInterpret.setFromPrimitive( q, prim2 );
  qInterpret.eval( q, rho1, u1, t1 );
  BOOST_CHECK_CLOSE( rho, rho1, tol );
  BOOST_CHECK_CLOSE( u, u1, tol );
  BOOST_CHECK_CLOSE( t, t1, tol );

  // copy constructor
  QInterpret qInterpret2(qInterpret);

  qInterpret2.eval( q, rho2, u2, t2 );
  BOOST_CHECK_CLOSE( rho, rho2, tol );
  BOOST_CHECK_CLOSE( u, u2, tol );
  BOOST_CHECK_CLOSE( t, t2, tol );

  // gradient
  Real rhox, ux, tx, px;
  Real rhox1, ux1, tx1;
  Real V0x, V1x, V2x;

  rhox = 0.37, ux = -0.85, px = 1.71;
  tx = t*(px/p - rhox/rho);

  Real rhoe, rhoE;
  Real rhoex;
  Real rhoEx;
  Real sx;
  rhoe = rho*gas.energy(rho, t);
  rhoE = rho*(gas.energy(rho, t) + 0.5*(u*u));

  // rhoe = rho*gas.energy(rho, t);
  rhoex = rhox*gas.Cv()*t + rho*gas.Cv()*tx;

  // rhoE = rho*(gas.energy(rho, t) + 0.5*(u*u + v*v));
  rhoEx = rhox*(gas.Cv()*t + 0.5*(u*u)) + rho*(gas.Cv()*tx + (u*ux));

  // s = log((gamma-1.0)*rhoe / pow(rho,gamma));
  sx = rhoex/rhoe - gamma * rhox/rho;

  // q(0) = -rhoE/rhoe + gamma + 1 - s;
  V0x = -rhoEx / rhoe - rhoE / rhoe * (-rhoex / rhoe) - sx;

  // q(1) =  rho*u/rhoe;
  V1x = rhox*u/rhoe + rho*ux/rhoe + rho*u/rhoe*(-rhoex/rhoe);

  // q(3) = -rho/rhoe;
  V2x = -rhox/rhoe - rho/rhoe * (-rhoex/rhoe);

  Real qxData[3] = {V0x, V1x, V2x};
  ArrayQ qx(qxData, 3);
  qInterpret.evalGradient( q, qx, rhox1, ux1, tx1 );
  BOOST_CHECK_CLOSE( rhox, rhox1, tol );
  BOOST_CHECK_CLOSE(   ux,   ux1, tol );
  BOOST_CHECK_CLOSE(   tx,   tx1, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( evalJacobian )
{
  typedef Q1D<QTypeEntropy, TraitsSizeEuler> QInterpret;

  typedef QInterpret::ArrayQ< Real > ArrayQ;
  typedef QInterpret::ArrayQ< SurrealS<QInterpret::N> > ArrayQSurreal;

  // constructor
  const Real gamma = 1.4;
  const Real R     = 287.04;       // J/(kg K)
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  ArrayQ q, rho_q, u_q, t_q;
  ArrayQSurreal qSurreal;
  SurrealS<QInterpret::N> rho, u, t;

  //qInterpret.setFromPrimitive( q, DensityVelocityTemperature1D(1.225, 150.03, 288.15) );

  // There are some numerical issues with trying to pick a physical number to compare with Surreals
  q[0] = -10;
  q[1] = 2;
  q[2] = -3;

  qInterpret.evalJacobian(q, rho_q, u_q, t_q);

  qSurreal = q;

  qSurreal[0].deriv(0) = 1;
  qSurreal[1].deriv(1) = 1;
  qSurreal[2].deriv(2) = 1;

  qInterpret.eval(qSurreal, rho, u, t);

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( rho.deriv(0), rho_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(1), rho_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( rho.deriv(2), rho_q[2], small_tol, close_tol )

  SANS_CHECK_CLOSE( u.deriv(0), u_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(1), u_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( u.deriv(2), u_q[2], small_tol, close_tol )

  SANS_CHECK_CLOSE( t.deriv(0), t_q[0], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(1), t_q[1], small_tol, close_tol )
  SANS_CHECK_CLOSE( t.deriv(2), t_q[2], small_tol, close_tol )

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef Q1D<QTypeEntropy, TraitsSizeEuler> QInterpret;
  typedef QInterpret::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  /*
   Entropy calluation from primites.
   This must be done explicitly to avoid log( x < 0 ) floating errors

    rhoe = rho*gas_.energy(rho, t);
    rhoE = rho*(gas_.energy(rho, t) + 0.5*u*u);
    gamma = gas_.gamma();
    s = log((gamma-1.0)*rhoe / pow(rho,gamma));

    q(0) = -rhoE/rhoe + gamma + 1 - s;
    q(1) =  rho*u/rhoe;
    q(2) = -rho/rhoe;
   */

  ArrayQ q;
  Real rho, u, t, rhoe, rhoE, s;
  string qNamePrim[3] = {"Density", "VelocityX", "Temperature"};

  // Start with physical data
  rho = 1.225;          // kg/m^3 (standard atmosphere)
  t   = 288.15;         // K (standard atmosphere)
  u   = 15.03;          // m/s
  Real qDataPrim[3] = {rho, u,  t};
  qInterpret.setFromPrimitive( q, qDataPrim, qNamePrim, 3 );
  BOOST_CHECK( qInterpret.isValidState(q) == true );

  // Negative temperature
  rho = 1.225;          // kg/m^3 (standard atmosphere)
  t   = -288.15;        // K (standard atmosphere)

  rhoe = rho*gas.energy(rho, t);
  rhoE = rho*(gas.energy(rho, t) + 0.5*u*u);
  s = -1; //log((gamma-1.0)*rhoe / pow(rho,gamma)); rhoe < 0

  q(0) = -rhoE/rhoe + gamma + 1 - s;
  q(1) =  rho*u/rhoe;
  q(2) = -rho/rhoe;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  // Negative density
  rho = -1.225;        // kg/m^3 (standard atmosphere)
  t   = 288.15;        // K (standard atmosphere)

  rhoe = rho*gas.energy(rho, t);
  rhoE = rho*(gas.energy(rho, t) + 0.5*u*u);
  s = 1e6; //log((gamma-1.0)*rhoe / pow(rho,gamma)); rho < 0

  q(0) = -rhoE/rhoe + gamma + 1 - s;
  q(1) =  rho*u/rhoe;
  q(2) = -rho/rhoe;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  // Negative density and temperature
  rho = -1.225;         // kg/m^3 (standard atmosphere)
  t   = -288.15;        // K (standard atmosphere)

  rhoe = rho*gas.energy(rho, t);
  rhoE = rho*(gas.energy(rho, t) + 0.5*u*u);
  s = -1; //log((gamma-1.0)*rhoe / pow(rho,gamma)); rho < 0

  q(0) = -rhoE/rhoe + gamma + 1 - s;
  q(1) =  rho*u/rhoe;
  q(2) = -rho/rhoe;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  // simply results in 1/0 everywhere...
#if 0
  // Zero temperature
  rho = 1.225;          // kg/m^3 (standard atmosphere)
  t   = 0.0;            // K (standard atmosphere)

  rhoe = rho*gas.energy(rho, t);
  rhoE = rho*(gas.energy(rho, t) + 0.5*u*u);
  s = log((gamma-1.0)*rhoe / pow(rho,gamma));

  q(0) = -rhoE/rhoe + gamma + 1 - s;
  q(1) =  rho*u/rhoe;
  q(2) = -rho/rhoe;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  // Zero density
  rho = 0.0;            // kg/m^3 (standard atmosphere)
  t   = -288.15;        // K (standard atmosphere)

  rhoe = rho*gas.energy(rho, t);
  rhoE = rho*(gas.energy(rho, t) + 0.5*u*u);
  s = log((gamma-1.0)*rhoe / pow(rho,gamma));

  q(0) = -rhoE/rhoe + gamma + 1 - s;
  q(1) =  rho*u/rhoe;
  q(2) = -rho/rhoe;
  BOOST_CHECK( qInterpret.isValidState(q) == false );
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/Q1DEntropy_pattern.txt", true );

  typedef Q1D<QTypeEntropy, TraitsSizeEuler> QInterpret;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  QInterpret qInterpret(gas);

  qInterpret.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
