// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEEulermitAVDiffusion3D_btest
//
// test of 3-D compressible Euler PDE class with artificial viscosity

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
//#include "pde/NS/Q3DPrimitiveSurrogate.h"
#include "pde/NS/Q3DConservative.h"
//#include "pde/NS/Q3DEntropy.h"
#include "pde/NS/PDEEuler3D.h"
#include "pde/NS/PDEEulermitAVDiffusion3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"

namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
class QTypeEntropy {};

// Explicitly instantiate the class so coverage information is correct
template class TraitsSizeEuler<PhysD3>;
template class TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>;
template class TraitsModelEuler<QTypeConservative, GasModel>;

template class PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>>;
template class PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEuler<QTypeConservative, GasModel>>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEEulermitAVDiffusion3D_test_suite )

typedef boost::mpl::list< QTypePrimitiveRhoPressure, QTypeConservative > QTypes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( static_test, QType, QTypes )
{
  {
    typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
    typedef PDEEulermitAVDiffusion3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
    typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
    typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

    BOOST_REQUIRE( PDEClass::D == 3 );
    BOOST_REQUIRE( PDEClass::N == 5 );
    BOOST_REQUIRE( ArrayQ::M == 5 );
    BOOST_REQUIRE( MatrixQ::M == 5 );
    BOOST_REQUIRE( MatrixQ::N == 5 );
  }

  {
    typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
    typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEClass;
    typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
    typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

    BOOST_REQUIRE( PDEClass::D == 3 );
    BOOST_REQUIRE( PDEClass::N == 6 );
    BOOST_REQUIRE( ArrayQ::M == 6 );
    BOOST_REQUIRE( MatrixQ::M == 6 );
    BOOST_REQUIRE( MatrixQ::N == 6 );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctor, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  PDEClass pde(order, gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( pde.D == 3 );
  BOOST_REQUIRE( pde.N == 5 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasSourceTrace() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setDOFFrom )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  PDEClass pde(order, gas, Euler_ResidInterp_Raw);

  Real rho = 1.137;
  Real u = 0.784;
  Real v = -0.231;
  Real w = 0.456;
  Real t = 0.987;
  Real p = R*rho*t;
  Real E = gas.energy(rho,t) + 0.5*(u*u + v*v + w*w);

  PyDict d;

  // set
  Real qDataPrim[5] = {rho, u, v, w, t};
  string qNamePrim[5] = {"Density", "VelocityX", "VelocityY", "VelocityZ", "Temperature"};
  ArrayQ qTrue = {rho, u, v, w, p};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

  DensityVelocityPressure3D<Real> qdata1;
  qdata1.Density = rho; qdata1.VelocityX = u; qdata1.VelocityY = v; qdata1.VelocityZ = w; qdata1.Pressure = p;
  q = 0;
  pde.setDOFFrom( q, qdata1 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

  PyDict rhoVP;
  rhoVP[NSVariableType3DParams::params.StateVector.Variables] = NSVariableType3DParams::params.StateVector.PrimitivePressure;
  rhoVP[DensityVelocityPressure3DParams::params.rho] = rho;
  rhoVP[DensityVelocityPressure3DParams::params.u] = u;
  rhoVP[DensityVelocityPressure3DParams::params.v] = v;
  rhoVP[DensityVelocityPressure3DParams::params.w] = w;
  rhoVP[DensityVelocityPressure3DParams::params.p] = p;

  d[NSVariableType3DParams::params.StateVector] = rhoVP;
  NSVariableType3DParams::checkInputs(d);
  q = pde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );


  DensityVelocityTemperature3D<Real> qdata2(rho, u, v, w, t);
  q = 0;
  pde.setDOFFrom( q, qdata2 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

  PyDict rhoVT;
  rhoVT[NSVariableType3DParams::params.StateVector.Variables] = NSVariableType3DParams::params.StateVector.PrimitiveTemperature;
  rhoVT[DensityVelocityTemperature3DParams::params.rho] = rho;
  rhoVT[DensityVelocityTemperature3DParams::params.u] = u;
  rhoVT[DensityVelocityTemperature3DParams::params.v] = v;
  rhoVT[DensityVelocityTemperature3DParams::params.w] = w;
  rhoVT[DensityVelocityTemperature3DParams::params.t] = t;

  d[NSVariableType3DParams::params.StateVector] = rhoVT;
  NSVariableType3DParams::checkInputs(d);
  q = pde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );


  Conservative3D<Real> qdata3(rho, rho*u, rho*v, rho*w, rho*E);
  q = 0;
  pde.setDOFFrom( q, qdata3 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

  PyDict Conservative;
  Conservative[NSVariableType3DParams::params.StateVector.Variables] = NSVariableType3DParams::params.StateVector.Conservative;
  Conservative[Conservative3DParams::params.rho] = rho;
  Conservative[Conservative3DParams::params.rhou] = rho*u;
  Conservative[Conservative3DParams::params.rhov] = rho*v;
  Conservative[Conservative3DParams::params.rhow] = rho*w;
  Conservative[Conservative3DParams::params.rhoE] = rho*E;

  d[NSVariableType3DParams::params.StateVector] = Conservative;
  NSVariableType3DParams::checkInputs(d);
  q = pde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxAV, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD3::D,Real> ParamType;

  const Real tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  PDEClass pde(order, gas, Euler_ResidInterp_Raw);

  // function tests

  Real x, y, z, time;
  Real rho, u, v, w, t, p, e0, h0;
  Real Cv, Cp;

  x = y = z = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = -0.231; w = 0.456; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u + v*v + w*w);
  h0 = Cp*t + 0.5*(u*u + v*v + w*w);

  Real sensor = 0.1;

  ParamType H = {{0.5},{0.1, 0.3},{0.06, 0.2, 0.7}};
  ParamType paramL = log(H);
  ParamType paramR = 0.5*log(H);

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t));
  q(5) = sensor;

  //The sensor equation entries will be zero since this PDE does not fill them.

  // conservative flux
  ArrayQ uTrue = {rho, rho*u, rho*v, rho*w, rho*e0, 0.0};
  ArrayQ uCons = 0;
  pde.masterState( paramL, x, y, z, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );
  BOOST_CHECK_CLOSE( uTrue(3), uCons(3), tol );
  BOOST_CHECK_CLOSE( uTrue(4), uCons(4), tol );
  BOOST_CHECK_CLOSE( uTrue(5), uCons(5), tol );

  // Should not accumulate
  pde.masterState( paramL, x, y, z, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );
  BOOST_CHECK_CLOSE( uTrue(3), uCons(3), tol );
  BOOST_CHECK_CLOSE( uTrue(4), uCons(4), tol );
  BOOST_CHECK_CLOSE( uTrue(5), uCons(5), tol );

  // conservative flux
  ArrayQ ftTrue = {rho, rho*u, rho*v, rho*w, rho*e0, 0.0};
  ArrayQ ft = 0;
  pde.fluxAdvectiveTime( paramL, x, y, z, time, q, ft );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( ftTrue(3), ft(3), tol );
  BOOST_CHECK_CLOSE( ftTrue(4), ft(4), tol );
  BOOST_CHECK_CLOSE( ftTrue(5), ft(5), tol );

  // Flux accumulate
  pde.fluxAdvectiveTime( paramL, x, y, z, time, q, ft );
  BOOST_CHECK_CLOSE( 2*ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(3), ft(3), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(4), ft(4), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(5), ft(5), tol );

  // jacobianMasterState
  MatrixQ dudq = 0;
  MatrixQ dudqTrue = 0;

  ArrayQ rho_q, u_q, v_q, w_q, t_q;
  Real e_rho, e_t = 0;
  pde.variableInterpreter().evalJacobian( q, rho_q, u_q, v_q, w_q, t_q );
  Real e = pde.gasModel().energy(rho, t);
  pde.gasModel().energyJacobian(rho, t, e_rho, e_t);
  Real E = e + 0.5*(u*u + v*v + w*w);
  ArrayQ E_q = e_rho*rho_q + e_t*t_q + u*u_q + v*v_q + w*w_q;

  dudqTrue(0,0) = rho_q[0];
  dudqTrue(0,1) = rho_q[1];
  dudqTrue(0,2) = rho_q[2];
  dudqTrue(0,3) = rho_q[3];
  dudqTrue(0,4) = rho_q[4];
  dudqTrue(0,5) = 0.0;

  dudqTrue(1,0) = rho_q[0]*u + rho*u_q[0];
  dudqTrue(1,1) = rho_q[1]*u + rho*u_q[1];
  dudqTrue(1,2) = rho_q[2]*u + rho*u_q[2];
  dudqTrue(1,3) = rho_q[3]*u + rho*u_q[3];
  dudqTrue(1,4) = rho_q[4]*u + rho*u_q[4];
  dudqTrue(1,5) = 0.0;

  dudqTrue(2,0) = rho_q[0]*v + rho*v_q[0];
  dudqTrue(2,1) = rho_q[1]*v + rho*v_q[1];
  dudqTrue(2,2) = rho_q[2]*v + rho*v_q[2];
  dudqTrue(2,3) = rho_q[3]*v + rho*v_q[3];
  dudqTrue(2,4) = rho_q[4]*v + rho*v_q[4];
  dudqTrue(2,5) = 0.0;

  dudqTrue(3,0) = rho_q[0]*w + rho*w_q[0];
  dudqTrue(3,1) = rho_q[1]*w + rho*w_q[1];
  dudqTrue(3,2) = rho_q[2]*w + rho*w_q[2];
  dudqTrue(3,3) = rho_q[3]*w + rho*w_q[3];
  dudqTrue(3,4) = rho_q[4]*w + rho*w_q[4];
  dudqTrue(3,5) = 0.0;

  dudqTrue(4,0) = rho_q[0]*E + rho*E_q[0];
  dudqTrue(4,1) = rho_q[1]*E + rho*E_q[1];
  dudqTrue(4,2) = rho_q[2]*E + rho*E_q[2];
  dudqTrue(4,3) = rho_q[3]*E + rho*E_q[3];
  dudqTrue(4,4) = rho_q[4]*E + rho*E_q[4];
  dudqTrue(4,5) = 0.0;

  dudqTrue(5,0) = 0.0;
  dudqTrue(5,1) = 0.0;
  dudqTrue(5,2) = 0.0;
  dudqTrue(5,3) = 0.0;
  dudqTrue(5,4) = 0.0;
  dudqTrue(5,5) = 0.0;

  pde.jacobianMasterState(paramL, x, y, z, time, q, dudq);
  for (int i = 0; i < 6; i++)
    for (int j = 0; j < 6; j++)
      BOOST_CHECK_CLOSE( dudqTrue(i,j), dudq(i,j), tol );

  // Should not accumulate
  pde.jacobianMasterState(paramL, x, y, z, time, q, dudq);
  for (int i = 0; i < 6; i++)
    for (int j = 0; j < 6; j++)
      BOOST_CHECK_CLOSE( dudqTrue(i,j), dudq(i,j), tol );

  // advective flux
  ArrayQ fxTrue = {rho*u, rho*u*u + p, rho*u*v    , rho*u*w    , rho*u*h0, 0.0};
  ArrayQ fyTrue = {rho*v, rho*v*u    , rho*v*v + p, rho*v*w    , rho*v*h0, 0.0};
  ArrayQ fzTrue = {rho*w, rho*w*u    , rho*w*v    , rho*w*w + p, rho*w*h0, 0.0};
  ArrayQ fx = 0, fy = 0, fz = 0;

  pde.fluxAdvective( paramL, x, y, z, time, q, fx, fy, fz );
  BOOST_CHECK_CLOSE( fxTrue(0), fx(0), tol );
  BOOST_CHECK_CLOSE( fxTrue(1), fx(1), tol );
  BOOST_CHECK_CLOSE( fxTrue(2), fx(2), tol );
  BOOST_CHECK_CLOSE( fxTrue(3), fx(3), tol );
  BOOST_CHECK_CLOSE( fxTrue(4), fx(4), tol );

  BOOST_CHECK_CLOSE( fyTrue(0), fy(0), tol );
  BOOST_CHECK_CLOSE( fyTrue(1), fy(1), tol );
  BOOST_CHECK_CLOSE( fyTrue(2), fy(2), tol );
  BOOST_CHECK_CLOSE( fyTrue(3), fy(3), tol );
  BOOST_CHECK_CLOSE( fyTrue(4), fy(4), tol );

  BOOST_CHECK_CLOSE( fzTrue(0), fz(0), tol );
  BOOST_CHECK_CLOSE( fzTrue(1), fz(1), tol );
  BOOST_CHECK_CLOSE( fzTrue(2), fz(2), tol );
  BOOST_CHECK_CLOSE( fzTrue(3), fz(3), tol );
  BOOST_CHECK_CLOSE( fzTrue(4), fz(4), tol );

  // Flux accumulate
  pde.fluxAdvective( paramL, x, y, z, time, q, fx, fy, fz );
  BOOST_CHECK_CLOSE( 2*fxTrue(0), fx(0), tol );
  BOOST_CHECK_CLOSE( 2*fxTrue(1), fx(1), tol );
  BOOST_CHECK_CLOSE( 2*fxTrue(2), fx(2), tol );
  BOOST_CHECK_CLOSE( 2*fxTrue(3), fx(3), tol );
  BOOST_CHECK_CLOSE( 2*fxTrue(4), fx(4), tol );

  BOOST_CHECK_CLOSE( 2*fyTrue(0), fy(0), tol );
  BOOST_CHECK_CLOSE( 2*fyTrue(1), fy(1), tol );
  BOOST_CHECK_CLOSE( 2*fyTrue(2), fy(2), tol );
  BOOST_CHECK_CLOSE( 2*fyTrue(3), fy(3), tol );
  BOOST_CHECK_CLOSE( 2*fyTrue(4), fy(4), tol );

  BOOST_CHECK_CLOSE( 2*fzTrue(0), fz(0), tol );
  BOOST_CHECK_CLOSE( 2*fzTrue(1), fz(1), tol );
  BOOST_CHECK_CLOSE( 2*fzTrue(2), fz(2), tol );
  BOOST_CHECK_CLOSE( 2*fzTrue(3), fz(3), tol );
  BOOST_CHECK_CLOSE( 2*fzTrue(4), fz(4), tol );


  Real rhoL = 1.137, uL = 0.784, vL = -0.231, wL = 0.456, tL = 0.987;
  Real rhoR = 1.240, uR = 0.831, vR =  0.142, wR = 0.638, tR = 0.865;

  Real sensorL = 0.11;
  Real sensorR = 0.17;

  ArrayQ qL = 0, qR = 0;
  pde.setDOFFrom( qL, DensityVelocityTemperature3D<Real>(rhoL, uL, vL, wL, tL) );
  pde.setDOFFrom( qR, DensityVelocityTemperature3D<Real>(rhoR, uR, vR, wR, tR) );
  qL(5) = sensorL;
  qR(5) = sensorR;

  ArrayQ qxL = {0.17, 0.21, -0.34, 0.07, 0.19, 0.31};
  ArrayQ qyL = {-0.05, 0.61, 0.12, 0.72,-0.21, -0.21};
  ArrayQ qzL = {0.25, 0.21, 0.32, 0.82, 0.07, 0.46};

  ArrayQ qxR = {0.21, 0.31, -0.14, 0.82, 0.26, 0.87};
  ArrayQ qyR = {0.11, 0.48, 0.27, -0.31, -0.15, 0.23};
  ArrayQ qzR = {-0.31, 0.56, 0.37, 0.46, 0.75, -0.61};

  MatrixQ kxx = 0, kxy = 0, kxz = 0;
  MatrixQ kyx = 0, kyy = 0, kyz = 0;
  MatrixQ kzx = 0, kzy = 0, kzz = 0;

  MatrixQ kxxT = 0, kxyT = 0, kxzT = 0;
  MatrixQ kyxT = 0, kyyT = 0, kyzT = 0;
  MatrixQ kzxT = 0, kzyT = 0, kzzT = 0;

  MatrixQ kxx_true = 0, kxy_true = 0, kxz_true = 0;
  MatrixQ kyx_true = 0, kyy_true = 0, kyz_true = 0;
  MatrixQ kzx_true = 0, kzy_true = 0, kzz_true = 0;

  Real lambda = 0;
  pde.speedCharacteristic( paramL, x, y, z, time, qL, lambda );


  Real C = 2.0;
//  Real alpha = 500;
//  Real eps_exp = 1e-10;
//  sensor = smoothActivation_exp(sensorL, alpha, eps_exp);

  Real zz = 0.0;
  sensor = std::max(sensorL, zz);

  Real factor = C/(Real(order)) * lambda * sensor; //smoothabs0(sensorL, 1.0e-5);

  MatrixQ dutdu = DLA::Identity();
  dutdu(4, 0) = 0.5*(gamma - 1.0)*(uL*uL + vL*vL + wL*wL);
  dutdu(4, 1) = -(gamma - 1.0)*uL;
  dutdu(4, 2) = -(gamma - 1.0)*vL;
  dutdu(4, 3) = -(gamma - 1.0)*wL;
  dutdu(4, 4) = gamma;

  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
    {
      if (i == j)
      {
        kxxT(i,j) = factor*H(0,0);
        kxyT(i,j) = factor*H(0,1);
        kxzT(i,j) = factor*H(0,2);
        kyxT(i,j) = factor*H(1,0);
        kyyT(i,j) = factor*H(1,1);
        kyzT(i,j) = factor*H(1,2);
        kzxT(i,j) = factor*H(2,0);
        kzyT(i,j) = factor*H(2,1);
        kzzT(i,j) = factor*H(2,2);
      }
      else
      {
        kxxT(i,j) = 0.0;
        kxyT(i,j) = 0.0;
        kxzT(i,j) = 0.0;
        kyxT(i,j) = 0.0;
        kyyT(i,j) = 0.0;
        kyzT(i,j) = 0.0;
        kzxT(i,j) = 0.0;
        kzyT(i,j) = 0.0;
        kzzT(i,j) = 0.0;
      }
    }

  kxx_true = kxxT*dutdu;
  kxy_true = kxyT*dutdu;
  kxz_true = kxzT*dutdu;
  kyx_true = kyxT*dutdu;
  kyy_true = kyyT*dutdu;
  kyz_true = kyzT*dutdu;
  kzx_true = kzxT*dutdu;
  kzy_true = kzyT*dutdu;
  kzz_true = kzzT*dutdu;

  pde.diffusionViscous( paramL, x, y, z, time, qL, qxL, qyL, qzL,
                        kxx, kxy, kxz,
                        kyx, kyy, kyz,
                        kzx, kzy, kzz );
  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
    {
      BOOST_CHECK_CLOSE( kxx_true(i,j), kxx(i,j), tol );
      BOOST_CHECK_CLOSE( kxy_true(i,j), kxy(i,j), tol );
      BOOST_CHECK_CLOSE( kxz_true(i,j), kxz(i,j), tol );
      BOOST_CHECK_CLOSE( kyx_true(i,j), kyx(i,j), tol );
      BOOST_CHECK_CLOSE( kyy_true(i,j), kyy(i,j), tol );
    }

  //Check that sensor PDE entries are zero
  BOOST_CHECK_CLOSE( 0.0, kxx(5,5), tol );
  BOOST_CHECK_CLOSE( 0.0, kxy(5,5), tol );
  BOOST_CHECK_CLOSE( 0.0, kxz(5,5), tol );
  BOOST_CHECK_CLOSE( 0.0, kyx(5,5), tol );
  BOOST_CHECK_CLOSE( 0.0, kyy(5,5), tol );

  // Flux accumulate
  pde.diffusionViscous( paramL, x, y, z, time, qL, qxL, qyL, qzL,
                        kxx, kxy, kxz,
                        kyx, kyy, kyz,
                        kzx, kzy, kzz );
  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
    {
      BOOST_CHECK_CLOSE( 2*kxx_true(i,j), kxx(i,j), tol );
      BOOST_CHECK_CLOSE( 2*kxy_true(i,j), kxy(i,j), tol );
      BOOST_CHECK_CLOSE( 2*kxz_true(i,j), kxz(i,j), tol );
      BOOST_CHECK_CLOSE( 2*kyx_true(i,j), kyx(i,j), tol );
      BOOST_CHECK_CLOSE( 2*kyy_true(i,j), kyy(i,j), tol );
    }

  //Check that sensor PDE entries are zero
  BOOST_CHECK_CLOSE( 0.0, kxx(5,5), tol );
  BOOST_CHECK_CLOSE( 0.0, kxy(5,5), tol );
  BOOST_CHECK_CLOSE( 0.0, kxz(5,5), tol );
  BOOST_CHECK_CLOSE( 0.0, kyx(5,5), tol );
  BOOST_CHECK_CLOSE( 0.0, kyy(5,5), tol );


  MatrixQ dudqL = 0;
  pde.jacobianMasterState(paramL, x, y, z, time, qL, dudqL);

  ArrayQ fxL_true = -kxx_true*dudqL*qxL - kxy_true*dudqL*qyL - kxz_true*dudqL*qzL;
  ArrayQ fyL_true = -kyx_true*dudqL*qxL - kyy_true*dudqL*qyL - kyz_true*dudqL*qzL;
  ArrayQ fzL_true = -kzx_true*dudqL*qxL - kzy_true*dudqL*qyL - kzz_true*dudqL*qzL;

  ArrayQ fxL = 0, fyL = 0, fzL = 0;
  pde.fluxViscous( paramL, x, y, z, time, qL, qxL, qyL, qzL, fxL, fyL, fzL );
  BOOST_CHECK_CLOSE( fxL_true(0), fxL(0), tol );
  BOOST_CHECK_CLOSE( fxL_true(1), fxL(1), tol );
  BOOST_CHECK_CLOSE( fxL_true(2), fxL(2), tol );
  BOOST_CHECK_CLOSE( fxL_true(3), fxL(3), tol );
  BOOST_CHECK_CLOSE( fxL_true(4), fxL(4), tol );
  BOOST_CHECK_CLOSE( fxL_true(5), fxL(5), tol );

  BOOST_CHECK_CLOSE( fyL_true(0), fyL(0), tol );
  BOOST_CHECK_CLOSE( fyL_true(1), fyL(1), tol );
  BOOST_CHECK_CLOSE( fyL_true(2), fyL(2), tol );
  BOOST_CHECK_CLOSE( fyL_true(3), fyL(3), tol );
  BOOST_CHECK_CLOSE( fyL_true(4), fyL(4), tol );
  BOOST_CHECK_CLOSE( fyL_true(5), fyL(5), tol );

  BOOST_CHECK_CLOSE( fzL_true(0), fzL(0), tol );
  BOOST_CHECK_CLOSE( fzL_true(1), fzL(1), tol );
  BOOST_CHECK_CLOSE( fzL_true(2), fzL(2), tol );
  BOOST_CHECK_CLOSE( fzL_true(3), fzL(3), tol );
  BOOST_CHECK_CLOSE( fzL_true(4), fzL(4), tol );
  BOOST_CHECK_CLOSE( fzL_true(5), fzL(5), tol );

  // Flux accumulate
  pde.fluxViscous( paramL, x, y, z, time, qL, qxL, qyL, qzL, fxL, fyL, fzL );
  BOOST_CHECK_CLOSE( 2*fxL_true(0), fxL(0), tol );
  BOOST_CHECK_CLOSE( 2*fxL_true(1), fxL(1), tol );
  BOOST_CHECK_CLOSE( 2*fxL_true(2), fxL(2), tol );
  BOOST_CHECK_CLOSE( 2*fxL_true(3), fxL(3), tol );
  BOOST_CHECK_CLOSE( 2*fxL_true(4), fxL(4), tol );
  BOOST_CHECK_CLOSE( 2*fxL_true(5), fxL(5), tol );

  BOOST_CHECK_CLOSE( 2*fyL_true(0), fyL(0), tol );
  BOOST_CHECK_CLOSE( 2*fyL_true(1), fyL(1), tol );
  BOOST_CHECK_CLOSE( 2*fyL_true(2), fyL(2), tol );
  BOOST_CHECK_CLOSE( 2*fyL_true(3), fyL(3), tol );
  BOOST_CHECK_CLOSE( 2*fyL_true(4), fyL(4), tol );
  BOOST_CHECK_CLOSE( 2*fyL_true(5), fyL(5), tol );

  BOOST_CHECK_CLOSE( 2*fzL_true(0), fzL(0), tol );
  BOOST_CHECK_CLOSE( 2*fzL_true(1), fzL(1), tol );
  BOOST_CHECK_CLOSE( 2*fzL_true(2), fzL(2), tol );
  BOOST_CHECK_CLOSE( 2*fzL_true(3), fzL(3), tol );
  BOOST_CHECK_CLOSE( 2*fzL_true(4), fzL(4), tol );
  BOOST_CHECK_CLOSE( 2*fzL_true(5), fzL(5), tol );

  ArrayQ fxR_true = 0, fyR_true = 0, fzR_true = 0;
  pde.fluxViscous( paramR, x, y, z, time, qR, qxR, qyR, qzR, fxR_true, fyR_true, fzR_true );

  Real nx = 0.526, ny = -0.124, nz = 0.391;
  ArrayQ fn_true = 0.5*(fxL_true + fxR_true)*nx + 0.5*(fyL_true + fyR_true)*ny + 0.5*(fzL_true + fzR_true)*nz;

  ArrayQ fn = 0;
  pde.fluxViscous( paramL, paramR, x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, nx, ny, nz, fn );
  BOOST_CHECK_CLOSE( fn_true(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fn_true(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fn_true(2), fn(2), tol );
  BOOST_CHECK_CLOSE( fn_true(3), fn(3), tol );
  BOOST_CHECK_CLOSE( fn_true(4), fn(4), tol );
  BOOST_CHECK_CLOSE( fn_true(5), fn(5), tol );

  // Flux accumulate
  pde.fluxViscous( paramL, paramR, x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, nx, ny, nz, fn );
  BOOST_CHECK_CLOSE( 2*fn_true(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 2*fn_true(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 2*fn_true(2), fn(2), tol );
  BOOST_CHECK_CLOSE( 2*fn_true(3), fn(3), tol );
  BOOST_CHECK_CLOSE( 2*fn_true(4), fn(4), tol );
  BOOST_CHECK_CLOSE( 2*fn_true(5), fn(5), tol );

  ArrayQ source = 0.0;
  pde.source( paramL, x, y, z, time, qL, qxL, qyL, qzL, source );
  BOOST_CHECK_CLOSE( 0.0, source(0), tol );
  BOOST_CHECK_CLOSE( 0.0, source(1), tol );
  BOOST_CHECK_CLOSE( 0.0, source(2), tol );
  BOOST_CHECK_CLOSE( 0.0, source(3), tol );
  BOOST_CHECK_CLOSE( 0.0, source(4), tol );
  BOOST_CHECK_CLOSE( 0.0, source(5), tol );

  ArrayQ sourceL = 0.0, sourceR = 0.0;
  pde.sourceTrace( paramL, x, y, z, paramL, x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, sourceL, sourceR );
  BOOST_CHECK_CLOSE( 0.0, sourceL(0), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceL(1), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceL(2), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceL(3), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceL(4), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceL(5), tol );

  BOOST_CHECK_CLOSE( 0.0, sourceR(0), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceR(1), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceR(2), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceR(3), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceR(4), tol );
  BOOST_CHECK_CLOSE( 0.0, sourceR(5), tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( strongFlux, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  typedef Q3D<QType, TraitsSizeEuler> QInterpret;

  typedef DLA::MatrixSymS<PhysD3::D,Real> ParamType;

  const Real tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  PDEClass pde(order, gas, Euler_ResidInterp_Raw);
  QInterpret qInterpret(gas);

  // function tests

  Real x, y, z, time;
  Real rho, u, v, w, t, e0;
  Real Cv, Cp;

  x = y = z = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = -0.231; w = 0.456; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  e0 = Cv*t + 0.5*(u*u + v*v + w*w);

  ParamType Htensor = {{0.5},{0.1, 0.3},{0.06, 0.2, 0.7}};
  ParamType paramL = log(Htensor);

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );

  // Strong Conservative Flux
  ArrayQ qt = {-0.9, -0.25, 0.14, 0.78, 0.23};

  Real rhot, ut, vt, wt, et;
  Real tt; //d(temperature)/d(time)
  qInterpret.evalGradient( q, qt, rhot, ut, vt, wt, tt);
  gas.energyGradient(rho, t, rhot, tt, et);
  Real e0t = et + u*ut + v*vt + w*wt;

  ArrayQ utCons, utConsTrue;

  utConsTrue[0] = rhot;
  utConsTrue[1] = rho*ut + rhot*u;
  utConsTrue[2] = rho*vt + rhot*v;
  utConsTrue[3] = rho*wt + rhot*w;
  utConsTrue[4] = rho*e0t + rhot*e0;

  utCons = 0;
  pde.strongFluxAdvectiveTime( paramL, x, y, z, time, q, qt, utCons );
  BOOST_CHECK_CLOSE( utConsTrue(0), utCons(0), tol );
  BOOST_CHECK_CLOSE( utConsTrue(1), utCons(1), tol );
  BOOST_CHECK_CLOSE( utConsTrue(2), utCons(2), tol );
  BOOST_CHECK_CLOSE( utConsTrue(3), utCons(3), tol );
  BOOST_CHECK_CLOSE( utConsTrue(4), utCons(4), tol );

  // Flux accumulate
  pde.strongFluxAdvectiveTime( paramL, x, y, z, time, q, qt, utCons );
  BOOST_CHECK_CLOSE( 2*utConsTrue(0), utCons(0), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(1), utCons(1), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(2), utCons(2), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(3), utCons(3), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(4), utCons(4), tol );


  // Strong Advective Flux
  Real rhox, ux, vx, wx, tx, px, hx;
  Real rhoy, uy, vy, wy, ty, py, hy;
  Real rhoz, uz, vz, wz, tz, pz, hz;

  ArrayQ qx = {-0.02,  0.072, -0.0234, -0.12, 10.2};
  ArrayQ qy = { 0.05, -0.036,  0.0898, 0.024, 0.05};
  ArrayQ qz = { 0.03,  0.125,  0.0456, 0.026, 0.16};

  qInterpret.eval( q, rho, u, v, w, t );
  qInterpret.evalGradient( q, qx, rhox, ux, vx, wx, tx);
  qInterpret.evalGradient( q, qy, rhoy, uy, vy, wy, ty);
  qInterpret.evalGradient( q, qz, rhoz, uz, vz, wz, tz);

  gas.pressureGradient(rho, t, rhox, tx, px);
  gas.pressureGradient(rho, t, rhoy, ty, py);
  gas.pressureGradient(rho, t, rhoz, tz, pz);

  gas.enthalpyGradient(rho, t, rhox, tx, hx);
  gas.enthalpyGradient(rho, t, rhoy, ty, hy);
  gas.enthalpyGradient(rho, t, rhoz, tz, hz);

  Real H = Cp*t + 0.5*(u*u + v*v + w*w);
  Real Hx = Cp*tx + (ux*u + vx*v + wx*w);
  Real Hy = Cp*ty + (uy*u + vy*v + wy*w);
  Real Hz = Cp*tz + (uz*u + vz*v + wz*w);

  ArrayQ strongAdv = 0;
  ArrayQ strongAdvTrue = 0;
  strongAdvTrue(0) += rhox*u   +   rho*ux;
  strongAdvTrue(1) += rhox*u*u + 2*rho*ux*u + px;
  strongAdvTrue(2) += rhox*u*v +   rho*ux*v + rho*u*vx;
  strongAdvTrue(3) += rhox*u*w +   rho*ux*w + rho*u*wx;
  strongAdvTrue(4) += rhox*u*H +   rho*ux*H + rho*u*Hx;

  strongAdvTrue(0) += rhoy*v   +   rho*vy;
  strongAdvTrue(1) += rhoy*v*u +   rho*vy*u + rho*v*uy;
  strongAdvTrue(2) += rhoy*v*v + 2*rho*vy*v + py;
  strongAdvTrue(3) += rhoy*v*w +   rho*vy*w + rho*v*wy;
  strongAdvTrue(4) += rhoy*v*H +   rho*vy*H + rho*v*Hy;

  strongAdvTrue(0) += rhoz*w   +   rho*wz;
  strongAdvTrue(1) += rhoz*w*u +   rho*wz*u + rho*w*uz;
  strongAdvTrue(2) += rhoz*w*v +   rho*wz*v + rho*w*vz;
  strongAdvTrue(3) += rhoz*w*w + 2*rho*wz*w + pz;
  strongAdvTrue(4) += rhoz*w*H +   rho*wz*H + rho*w*Hz;

  pde.strongFluxAdvective( paramL, x, y, z, time, q, qx, qy, qz, strongAdv );
  BOOST_CHECK_CLOSE( strongAdvTrue(0), strongAdv(0), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(1), strongAdv(1), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(2), strongAdv(2), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(3), strongAdv(3), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(4), strongAdv(4), tol );

  // Flux accumulate
  pde.strongFluxAdvective( paramL, x, y, z, time, q, qx, qy, qz, strongAdv );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(0), strongAdv(0), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(1), strongAdv(1), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(2), strongAdv(2), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(3), strongAdv(3), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(4), strongAdv(4), tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( jacobianMasterState, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename PDEClass::template ArrayQ<SurrealS<PDEClass::N>> ArrayQSurreal;

  typedef DLA::MatrixSymS<PhysD3::D,Real> ParamType;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  PDEClass pde(order, gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( pde.N == 5 );

  // function tests

  Real rho, u, v, w, t;

  rho = 1.137; u = 0.784; v = -0.231; w = 0.456; t = 0.987;

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );

  ParamType Htensor = {{0.5},{0.1, 0.3},{0.06, 0.2, 0.7}};
  ParamType paramL = log(Htensor);

  Real x = 0, y = 0, z = 0, time = 0;

  if ( std::is_same<QType,QTypeEntropy>::value )
  {
    // Entropy variables seem to have numerical problems when comparing Surreal and
    // hand differentiated code...
    q[0] = -10;
    q[1] = 2;
    q[2] = 4;
    q[3] = -3;
    q[4] = 3;
  }

  ArrayQSurreal qSurreal = q;

  qSurreal[0].deriv(0) = 1;
  qSurreal[1].deriv(1) = 1;
  qSurreal[2].deriv(2) = 1;
  qSurreal[3].deriv(3) = 1;
  qSurreal[4].deriv(4) = 1;

  // conservative flux
  ArrayQSurreal uConsSurreal = 0;
  pde.masterState( paramL, x, y, z, time, qSurreal, uConsSurreal );

  MatrixQ dudq = 0;

  const Real small_tol = 5e-10;
  const Real close_tol = 1e-12;

  pde.jacobianMasterState( paramL, x, y, z, time, q, dudq );
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(0), dudq(0,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(1), dudq(0,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(2), dudq(0,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(3), dudq(0,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(4), dudq(0,4), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(0), dudq(1,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(1), dudq(1,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(2), dudq(1,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(3), dudq(1,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(4), dudq(1,4), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(0), dudq(2,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(1), dudq(2,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(2), dudq(2,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(3), dudq(2,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(4), dudq(2,4), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(0), dudq(3,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(1), dudq(3,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(2), dudq(3,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(3), dudq(3,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(4), dudq(3,4), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[4].deriv(0), dudq(4,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[4].deriv(1), dudq(4,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[4].deriv(2), dudq(4,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[4].deriv(3), dudq(4,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[4].deriv(4), dudq(4,4), small_tol, close_tol )

  // Should not accumulate
  pde.jacobianMasterState( paramL, x, y, z, time, q, dudq );
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(0), dudq(0,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(1), dudq(0,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(2), dudq(0,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(3), dudq(0,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[0].deriv(4), dudq(0,4), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(0), dudq(1,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(1), dudq(1,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(2), dudq(1,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(3), dudq(1,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[1].deriv(4), dudq(1,4), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(0), dudq(2,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(1), dudq(2,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(2), dudq(2,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(3), dudq(2,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[2].deriv(4), dudq(2,4), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(0), dudq(3,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(1), dudq(3,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(2), dudq(3,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(3), dudq(3,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[3].deriv(4), dudq(3,4), small_tol, close_tol )

  SANS_CHECK_CLOSE( uConsSurreal[4].deriv(0), dudq(4,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[4].deriv(1), dudq(4,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[4].deriv(2), dudq(4,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[4].deriv(3), dudq(4,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uConsSurreal[4].deriv(4), dudq(4,4), small_tol, close_tol )
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxRoe, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  typedef DLA::MatrixSymS<PhysD3::D,Real> ParamType;

  const Real tol = 2.e-11;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  PDEClass pde(order, gas, Euler_ResidInterp_Raw);

  // Roe flux function test

  Real x, y, z, time;
  Real nx, ny, nz;
  Real rhoL, uL, vL, wL, tL, pL, h0L;
  Real rhoR, uR, vR, wR, tR, pR, h0R;
  Real Cp;

  x = y = z = time = 0;   // not actually used in functions
  nx = 2.0/7.0; ny = 1.0/7.0; nz = sqrt(1.0 - nx*nx - ny*ny);
  rhoL = 1.034; uL = 3.26; vL = -2.17; wL = 3.56; tL = 5.78;
  rhoR = 0.973; uR = 1.79; vR =  0.93; wR = 5.23; tR = 6.13;
  Cp  = R*gamma/(gamma - 1);

  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL + vL*vL + wL*wL);

  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR + vR*vR + wR*wR);

  ParamType Htensor = {{0.5},{0.1, 0.3},{0.06, 0.2, 0.7}};
  ParamType paramL = log(Htensor);

  // set
  ArrayQ qL = pde.setDOFFrom( DensityVelocityTemperature3D<Real>(rhoL, uL, vL, wL, tL) );
  ArrayQ qR = pde.setDOFFrom( DensityVelocityTemperature3D<Real>(rhoR, uR, vR, wR, tR) );

  // advective normal flux (average)
  ArrayQ fxL = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*vL     , rhoL*uL*wL     , rhoL*uL*h0L};
  ArrayQ fyL = {rhoL*vL, rhoL*vL*uL     , rhoL*vL*vL + pL, rhoL*vL*wL     , rhoL*vL*h0L};
  ArrayQ fzL = {rhoL*wL, rhoL*wL*uL     , rhoL*wL*vL     , rhoL*wL*wL + pL, rhoL*wL*h0L};

  ArrayQ fxR = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*vR     , rhoR*uR*wR     , rhoR*uR*h0R};
  ArrayQ fyR = {rhoR*vR, rhoR*vR*uR     , rhoR*vR*vR + pR, rhoR*vR*wR     , rhoR*vR*h0R};
  ArrayQ fzR = {rhoR*wR, rhoR*wR*uR     , rhoR*wR*vR     , rhoR*wR*wR + pR, rhoR*wR*h0R};

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < PDEClass::N; k++)
    fnTrue[k] = 0.5*(nx*(fxL[k] + fxR[k]) + ny*(fyL[k] + fyR[k]) + nz*(fzL[k] + fzR[k]));

#if 1
  // Exact values for Roe scheme from Roe.nb
  fnTrue[0] -= 0.65558457182422097471369683881548;
  fnTrue[1] -= -1.8638923372862592525182437515145;
  fnTrue[2] -=  7.0186535948930936601201334787757;
  fnTrue[3] -=  6.8854604538052794714508533474917;
  fnTrue[4] -= 20.499927239215613126430884098724;
#endif

  ArrayQ fn = 0;
  pde.fluxAdvectiveUpwind( paramL, x, y, z, time, qL, qR, nx, ny, nz, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( fnTrue(3), fn(3), tol );
  BOOST_CHECK_CLOSE( fnTrue(4), fn(4), tol );

  // Flux accumulate
  pde.fluxAdvectiveUpwind( paramL, x, y, z, time, qL, qR, nx, ny, nz, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(3), fn(3), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(4), fn(4), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( speedCharacteristic, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  typedef DLA::MatrixSymS<PhysD3::D,Real> ParamType;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  PDEClass pde(order, gas, Euler_ResidInterp_Raw);

  Real x, y, z, time, dx, dy, dz;
  Real rho, u, v, w, t, c;
  Real speed, speedTrue;

  x = y = z = time = 0;   // not actually used in functions
  dx = 0.57; dy = -0.43; dz = 0.76;
  rho = 1.137; u = 0.784; v = -0.231; w = 0.456; t = 0.987;
  c = sqrt(gamma*R*t);
  speedTrue = fabs(dx*u + dy*v + dz*w)/sqrt(dx*dx + dy*dy + dz*dz) + c;

  ParamType Htensor = {{0.5},{0.1, 0.3},{0.06, 0.2, 0.7}};
  ParamType paramL = log(Htensor);

  ArrayQ q;
  pde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );

  pde.speedCharacteristic( paramL, x, y, z, time, dx, dy, dz, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  pde.speedCharacteristic( paramL, x, y, z, time, q, speed );
  BOOST_CHECK_CLOSE( sqrt(u*u + v*v + w*w) + c, speed, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( isValidState, QType, QTypes )
{
  // Surrogates are special
  if ( std::is_same<QType,QTypePrimitiveSurrogate>::value ) return;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  PDEClass pde(order, gas, Euler_ResidInterp_Raw);

  ArrayQ q;
  Real rho, u = 0, v = 0, w = 0, t;

  rho = 1;
  t = 1;
  pde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );
  BOOST_CHECK( pde.isValidState(q) == true );

  rho = 1;
  t = -1;
  pde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );
  BOOST_CHECK( pde.isValidState(q) == false );

  rho = -1;
  t = 1;
  pde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );
  BOOST_CHECK( pde.isValidState(q) == false );

  rho = -1;
  t = -1;
  pde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );
  BOOST_CHECK( pde.isValidState(q) == false );

  rho = 1;
  t = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );
  BOOST_CHECK( pde.isValidState(q) == false );

  rho = 0;
  t = 1;
  pde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );
  BOOST_CHECK( pde.isValidState(q) == false );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( interpResidBC )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  PDEClass pde1(order, gas, Euler_ResidInterp_Raw);
  Real tol = 1.0e-12;

  ArrayQ q;
  Real rho = 1.137, u = 0.784, v = -0.231, w = 0.456, t = 0.987;

  pde1.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );

  BOOST_CHECK_EQUAL(pde1.category(), 0);

  BOOST_CHECK_EQUAL(pde1.nMonitor(), 5);
  BOOST_CHECK( pde1.isValidState(q) == true );
  DLA::VectorD<Real> vecout1(5);
  pde1.interpResidVariable(q, vecout1);
  for (int i = 0; i < PDEClass::N; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );
  vecout1 = 0;
  pde1.interpResidGradient(q, vecout1);
  for (int i = 0; i < PDEClass::N; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );
  vecout1 = 0;
  pde1.interpResidBC(q, vecout1);
  for (int i = 0; i < PDEClass::N; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );

  PDEClass pde2(order, gas, Euler_ResidInterp_Momentum);
  BOOST_CHECK_EQUAL(pde2.nMonitor(), 3);
  DLA::VectorD<Real> vecout2(3);
  pde2.interpResidVariable(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)+q(2)*q(2)+q(3)*q(3)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(4),vecout2(2), tol, tol);

  vecout2 = 0;
  pde2.interpResidGradient(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)+q(2)*q(2)+q(3)*q(3)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(4),vecout2(2), tol, tol);

  vecout2 = 0;
  pde2.interpResidBC(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)+q(2)*q(2)+q(3)*q(3)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(4),vecout2(2), tol, tol);

  PDEClass pde3(order, gas, Euler_ResidInterp_Entropy);
  BOOST_CHECK_EQUAL(pde3.nMonitor(), 1);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( interpResidBC_AV )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  PDEClass pde1(order, gas, Euler_ResidInterp_Raw);
  Real tol = 1.0e-12;

  ArrayQ q;
  Real rho = 1.137, u = 0.784, v = -0.231, w = 0.456, t = 0.987, sensor = 0.13;

  pde1.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );
  q(5) = sensor;

  BOOST_CHECK_EQUAL(pde1.category(), 0);

  BOOST_CHECK_EQUAL(pde1.nMonitor(), 6);
  BOOST_CHECK( pde1.isValidState(q) == true );
  DLA::VectorD<Real> vecout1(6);
  pde1.interpResidVariable(q, vecout1);
  for (int i = 0; i < PDEClass::N; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );
  vecout1 = 0;
  pde1.interpResidGradient(q, vecout1);
  for (int i = 0; i < PDEClass::N; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );
  vecout1 = 0;
  pde1.interpResidBC(q, vecout1);
  for (int i = 0; i < PDEClass::N; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );

  PDEClass pde2(order, gas, Euler_ResidInterp_Momentum);
  BOOST_CHECK_EQUAL(pde2.nMonitor(), 4);
  DLA::VectorD<Real> vecout2(4);
  pde2.interpResidVariable(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)+q(2)*q(2)+q(3)*q(3)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(4),vecout2(2), tol, tol);
  SANS_CHECK_CLOSE(q(5),vecout2(3), tol, tol);

  vecout2 = 0;
  pde2.interpResidGradient(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)+q(2)*q(2)+q(3)*q(3)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(4),vecout2(2), tol, tol);
  SANS_CHECK_CLOSE(q(5),vecout2(3), tol, tol);

  vecout2 = 0;
  pde2.interpResidBC(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)+q(2)*q(2)+q(3)*q(3)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(4),vecout2(2), tol, tol);
  SANS_CHECK_CLOSE(q(5),vecout2(3), tol, tol);

  PDEClass pde3(order, gas, Euler_ResidInterp_Entropy);
  BOOST_CHECK_EQUAL(pde3.nMonitor(), 1);

  DLA::VectorD<Real> vecout3(1);
  vecout3 = 0;
  pde3.interpResidVariable(q, vecout3);

  Real pRef = 1; Real rhoRef = 1; Real uRef = sqrt(gamma);
  Real rhoE = pRef/(gamma-1) + 0.5*rhoRef*(uRef*uRef);

  ArrayQ Vref = 0;
  Vref[0] = (gamma+1)/(gamma-1) - rhoE/pRef;
  Vref[1] = rhoRef*uRef/pRef;
  Vref[2] = rhoRef*uRef/pRef;
  Vref[3] = rhoRef*uRef/pRef;
  Vref[4] = -rhoRef/pRef;

  Real tmp = 0;
  tmp += Vref[0]*Vref[0]*q(0)*q(0);
  tmp += Vref[1]*Vref[1]*q(1)*q(1);
  tmp += Vref[2]*Vref[2]*q(2)*q(2);
  tmp += Vref[3]*Vref[3]*q(3)*q(3);
  tmp += Vref[4]*Vref[4]*q(4)*q(4);

  SANS_CHECK_CLOSE(sqrt(  tmp ),vecout3(0), tol, tol);

}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
