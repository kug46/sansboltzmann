// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEEulermitAVDiffusion2D_RANSSA_btest
//
// test of 2-D compressible Euler PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/QRANSSA2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/PDERANSSA2D.h"
#include "pde/NS/PDEEulermitAVDiffusion2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"

//Explicitly instantiate the class so coverage information is correct
namespace SANS
{
template class TraitsSizeRANSSA<PhysD2>;
template class TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel>;
template class PDERANSSA2D< TraitsSizeRANSSA,
                            TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
}
using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEEulermitAVDiffusion2D_RANSSA_test_suite )

typedef boost::mpl::list< QTypePrimitiveRhoPressure,
                          QTypeConservative,
                          QTypePrimitiveSurrogate,
                          QTypeEntropy > QTypes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSA, TraitsModelRANSSAClass, PDERANSSA2D> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  BOOST_REQUIRE( AVPDEClass::D == 2 );
  BOOST_REQUIRE( AVPDEClass::N == 5 );
  BOOST_REQUIRE( ArrayQ::M == 5 );
  BOOST_REQUIRE( MatrixQ::M == 5 );
  BOOST_REQUIRE( MatrixQ::N == 5 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSA, TraitsModelRANSSAClass, PDERANSSA2D> AVPDEClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, visc, tcond, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( avpde.D == 2 );
  BOOST_REQUIRE( avpde.N == 5 );

  // flux components
  BOOST_CHECK( avpde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( avpde.hasFluxAdvective() == true );
  BOOST_CHECK( avpde.hasFluxViscous() == true );
  BOOST_CHECK( avpde.hasSource() == true );
  BOOST_CHECK( avpde.hasSourceTrace() == false );
  BOOST_CHECK( avpde.hasForcingFunction() == false );

  BOOST_CHECK( avpde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( avpde.needsSolutionGradientforSource() == true );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setDOFFrom )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSA, TraitsModelRANSSAClass, PDERANSSA2D> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + sensor

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, visc, tcond, Euler_ResidInterp_Raw);

  Real rho = 1.137;
  Real u = 0.784;
  Real v = -0.231;
  Real t = 0.987;
  Real p = R*rho*t;
  Real E = gas.energy(rho,t) + 0.5*(u*u + v*v);
  Real nt = 4.2;

  Real sensor = 0.15;

  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType paramL(logH, sensor); // grid spacing and sensor values

  PyDict d;

  // set
  Real qDataPrim[5] = {rho, u, v, t, nt};
  string qNamePrim[5] = {"Density", "VelocityX", "VelocityY", "Temperature", "SANutilde"};
  ArrayQ qTrue = {rho, u, v, p, nt};
  ArrayQ q;
  avpde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

  SAnt2D<DensityVelocityPressure2D<Real>> qdata1;
  qdata1.Density = rho; qdata1.VelocityX = u; qdata1.VelocityY = v; qdata1.Pressure = p; qdata1.SANutilde = nt;
  q = 0;
  avpde.setDOFFrom( q, qdata1 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

  PyDict rhoVP;
  rhoVP[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.PrimitivePressure;
  rhoVP[SAnt2DParams<DensityVelocityPressure2DParams>::params.rho] = rho;
  rhoVP[SAnt2DParams<DensityVelocityPressure2DParams>::params.u] = u;
  rhoVP[SAnt2DParams<DensityVelocityPressure2DParams>::params.v] = v;
  rhoVP[SAnt2DParams<DensityVelocityPressure2DParams>::params.p] = p;
  rhoVP[SAnt2DParams<DensityVelocityPressure2DParams>::params.nt]  = nt;

  d[SAVariableType2DParams::params.StateVector] = rhoVP;
  SAVariableType2DParams::checkInputs(d);
  q = avpde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );


  SAnt2D<DensityVelocityTemperature2D<Real>> qdata2({rho, u, v, t, nt});
  q = 0;
  avpde.setDOFFrom( q, qdata2 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );


  SAnt2D<Conservative2D<Real>> qdata3({rho, rho*u, rho*v, rho*E, rho*nt});
  q = 0;
  avpde.setDOFFrom( q, qdata3 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

  PyDict Conservative;
  Conservative[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.Conservative;
  Conservative[SAnt2DParams<Conservative2DParams>::params.rho]   = rho;
  Conservative[SAnt2DParams<Conservative2DParams>::params.rhou]  = rho*u;
  Conservative[SAnt2DParams<Conservative2DParams>::params.rhov]  = rho*v;
  Conservative[SAnt2DParams<Conservative2DParams>::params.rhoE]  = rho*E;
  Conservative[SAnt2DParams<Conservative2DParams>::params.rhont] = rho*nt;

  d[SAVariableType2DParams::params.StateVector] = Conservative;
  SAVariableType2DParams::checkInputs(d);
  q = avpde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );
}

#if 0 // this stuff needs to move into the sensor pde test
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PDERANSSA2D_PrimitiveStrongFlux )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSA, TraitsModelRANSSAClass, PDERANSSA2D> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance function

  const Real close_tol = 1.e-10;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real tSuth = 110./300.; //somewhat random...
  const Real tRef  = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, visc, tcond, Euler_ResidInterp_Raw);

  Real dist = 60;

  HType Htensor = {{0.5},{0.1, 0.3}};
  HType logH = log(Htensor);
  ParamType paramL(logH, dist); // grid spacing and sensor values

  Real x, y, time;
  Real rho, u, v, p, nt;
  Real rhox, ux, vx, px, ntx;
  Real rhoy, uy, vy, py, nty;
  Real rhoxx, uxx, vxx, pxx, ntxx;
  Real rhoxy, uxy, vxy, pxy, ntxy;
  Real rhoyy, uyy, vyy, pyy, ntyy;

  x = 0.73; y = 36./99.; time = 0;   // not actually used in functions

  rho = 1.0074365505202103681442524417731;
  u = 0.10508967589710564917176487424421;
  v = 0.024335124272671981161838412026241;
  p = 1.0005144026446280991735537190083;
  nt = 5.0876529752066115702479338842975;

  rhox = 0.010748222028549962434259954921112;
  ux = 0.016369696969696969696969696969697;
  vx = 0.013539393939393939393939393939394;
  px = 0.0021139834710743801652892561983471;
  ntx = 0.20391735537190082644628099173554;

  rhoy = 0.0011265417917355371900826446280992;
  uy = 0.003044338055883510428964974419520;
  vy = 0.018728366999391792780222532288648;
  py = 0.0028292145454545454545454545454545;
  nty = 0.18899636363636363636363636363636;

  rhoxx = 0.0030748850488354620586025544703231;
  uxx = 0.022424242424242424242424242424242;
  vxx = 0.0048484848484848484848484848484848;
  pxx = 0.0057917355371900826446280991735537;
  ntxx = 0.27933884297520661157024793388430;

  rhoxy = 0.0061728317355371900826446280991736;
  uxy = 0.0048666666666666666666666666666667;
  vxy = 0.0097333333333333333333333333333333;
  pxy = 0.011626909090909090909090909090909;
  ntxy = 0.31854545454545454545454545454545;

  rhoyy = 0.0061959798545454545454545454545455;
  uyy = 0.030545454545454545454545454545455;
  vyy = -0.0068004722550177095631641086186541;
  pyy = 0.0077803400000000000000000000000000;
  ntyy = 0.5197400000000000000000000000000;

  // set
  ArrayQ q;
  avpde.setDOFFrom( q, SAnt2D<DensityVelocityPressure2D<Real>>({rho, u, v, p, nt }) );

  ArrayQ qx = {rhox, ux, vx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, py, nty};
  ArrayQ qxx = {rhoxx, uxx, vxx, pxx, ntxx};
  ArrayQ qxy = {rhoxy, uxy, vxy, pxy, ntxy};
  ArrayQ qyy = {rhoyy, uyy, vyy, pyy, ntyy};

  // Strong Conservative Flux (PRIMITIVE SPECIFIC)
  Real rhot = -0.9;
  Real ut = -0.25;
  Real vt = 0.14;
  Real pt = 0.23;
  Real t  = p/(R*rho);
  Real tt = (t/p) * pt - (t/rho)*rhot;
  Real ntt = 3.14;

#if 0
  Real mu = visc.viscosity(t);
  Real k = tcond.conductivity(mu);
  std::cout << std::setprecision(15) << __func__ << ": mu = " << mu << "  k = " << k << std::endl;
#endif

  Real e0 = Cv*t + 0.5*(u*u + v*v);
  Real e0t = Cv*tt + u*ut + v*vt;

  ArrayQ qt = {rhot, ut, vt, pt, ntt};
  ArrayQ utCons, utConsTrue;
  utCons = 0;
  avpde.strongFluxAdvectiveTime(paramL, x, y, time, q, qt, utCons );
  utConsTrue[0] = rhot;
  utConsTrue[1] = rho*ut + rhot*u;
  utConsTrue[2] = rho*vt + rhot*v;
  utConsTrue[3] = rho*e0t + rhot*e0;
  utConsTrue[4] = rho*ntt + rhot*nt;
  for (int i=0; i<5; i++)
    BOOST_CHECK_CLOSE( utConsTrue(i), utCons(i), close_tol );

  ArrayQ Fadv = 0; ArrayQ FadvTrue;
  ArrayQ Gadv = 0; ArrayQ GadvTrue;
  avpde.fluxAdvective(paramL, x, y, time, q, Fadv, Gadv);

  FadvTrue[0] = 0.10587118058106700917370666456360;
  FadvTrue[1] = 1.0116403706987363765045998373284;
  FadvTrue[2] = 0.0025763883363347622775335854515046;
  FadvTrue[3] = 0.3686190306186793850142013748494;
  FadvTrue[4] = 0.5386358268719020087608178238215;

  GadvTrue[0] = 0.024516093653741303940070306229868;
  GadvTrue[1] = 0.0025763883363347622775335854515046;
  GadvTrue[2] = 1.0011110048303723586957718335520;
  GadvTrue[3] = 0.08535938323913415860950277947394;
  GadvTrue[4] = 0.12472937681790087347722281270783;
  for (int i=0; i<5; i++)
  {
    BOOST_CHECK_CLOSE(Fadv(i), FadvTrue(i), close_tol);
    BOOST_CHECK_CLOSE(Gadv(i), GadvTrue(i), close_tol);
  }

  avpde.fluxAdvective(paramL, x, y, time, q, Fadv, Gadv);
  for (int i=0; i<5; i++)
  {
    BOOST_CHECK_CLOSE(Fadv(i), 2*FadvTrue(i), close_tol);
    BOOST_CHECK_CLOSE(Gadv(i), 2*GadvTrue(i), close_tol);
  }

  ArrayQ strongAdv = 0;
  ArrayQ strongAdvTrue = 0;
  avpde.strongFluxAdvective(paramL, x, y, time, q, qx, qy, strongAdv);

  strongAdvTrue(0) = 0.03651601419890715612108675424023;
  strongAdvTrue(1) = 0.007759153989119463332939283278032;
  strongAdvTrue(2) = 0.005610414308982112887949627535613;
  strongAdvTrue(3) = 0.1243734188228572701745553762010;
  strongAdvTrue(4) = 0.21200323198707463105895095938310;

  for (int i=0; i<5; i++)
    BOOST_CHECK_CLOSE(strongAdvTrue(i), strongAdv(i), close_tol);

  avpde.strongFluxAdvective(paramL, x, y, time, q, qx, qy, strongAdv);
  for (int i=0; i<5; i++)
    BOOST_CHECK_CLOSE(2*strongAdvTrue(i), strongAdv(i), close_tol);

  ArrayQ FviscTrue, GviscTrue;
  ArrayQ Fvisc = 0; ArrayQ Gvisc = 0;

  FviscTrue(0) = 0;
  FviscTrue(1) = -0.0225306858220465230739517769814;
  FviscTrue(2) = -0.04000165622505323194410409329417;
  FviscTrue(3) = 0.0845833783703484013428985020496;
  FviscTrue(4) = -1.872022413362562239862325433058;

  GviscTrue(0) = 0;
  GviscTrue(1) = -0.04000165622505323194410409329417;
  GviscTrue(2) = -0.0339093920991446656273492501600;
  GviscTrue(3) = -0.02259677129049221380087236976276;
  GviscTrue(4) = -1.7350432390908063684860979535301;

  avpde.fluxViscous(paramL, x, y, time, q, qx, qy, Fvisc, Gvisc);

  for (int i=0; i<5; i++)
  {
    BOOST_CHECK_CLOSE(Fvisc(i), FviscTrue(i), close_tol);
    BOOST_CHECK_CLOSE(Gvisc(i), GviscTrue(i), close_tol);
  }


  avpde.fluxViscous(paramL, x, y, time, q, qx, qy, Fvisc, Gvisc);

  for (int i=0; i<5; i++)
  {
    BOOST_CHECK_CLOSE(Fvisc(i), 2*FviscTrue(i), close_tol);
    BOOST_CHECK_CLOSE(Gvisc(i), 2*GviscTrue(i), close_tol);
  }

  ArrayQ strongVisc = 0;
  ArrayQ strongViscTrue;

  strongViscTrue(0) = 0;
  strongViscTrue(1) = -0.1586885687759146272058388151894;
  strongViscTrue(2) = -0.0001249081136479725918195084228;
  strongViscTrue(3) = -0.0582007392120158256116833654119;
  strongViscTrue(4) = -7.469316914316503527742653726732;

  avpde.strongFluxViscous(paramL, x, y, time, q, qx, qy, qxx, qxy, qyy, strongVisc);

  for (int i=0; i<5; i++)
    BOOST_CHECK_CLOSE(strongViscTrue(i), strongVisc(i), close_tol);

  avpde.strongFluxViscous(paramL, x, y, time, q, qx, qy, qxx, qxy, qyy, strongVisc);

  for (int i=0; i<5; i++)
    BOOST_CHECK_CLOSE(2*strongViscTrue(i), strongVisc(i), close_tol);

  MatrixQ kxx_x = 0;
  MatrixQ kxy_x = 0;
  MatrixQ kyx_y = 0;
  MatrixQ kyy_y = 0;

  avpde.diffusionViscousGradient(paramL, x, y, time, q, qx, qy, kxx_x, kxy_x, kyx_y, kyy_y);

  MatrixQ kxx_xTrue =  {{0,0,0,0,0},
      {-0.0823045358265325411848575757752,0.285908040982372569951761031892,0,0,0},
      {-0.0376355344192862796342691791478,0,0.214431030736779427463820773919,0,0},
      {-0.729966604002541726790175206296,-0.0193067773564679510334838709088,
       -0.0259462232329481599589472484151,0.326934247856932466585657975194,0},
       {-3.284384577308392773104067438466,0,0,0,0.280321947513941888597490341225}};

  MatrixQ kxy_xTrue = {{0,0,0,0,0},
                       {0.02509035627952418642284611943189,0,-0.142954020491186284975880515946,0,0},
                       {-0.0617284018698994058886431818314,0.214431030736779427463820773919,0,0,0},
                       {-0.001636298296905254836713274388442,0.0376355344192862796342691791478,
                        -0.0411522679132662705924287878876,0,0},
                        {0,0,0,0,0}};

  MatrixQ kyx_yTrue = {{0,0,0,0,0},
                       {-0.0488636456269307946152477039139,0,0.1652909163793633646593041741214,0,0},
                       {0.01643961195577396837721068807557,-0.1101939442529089097728694494142,0,0,0},
                       {-0.001770814865942333260434457485998,-0.03257576375128719641016513594259,
                        0.0246594179336609525658160321134,0,0},
                        {0,0,0,0,0}};

  MatrixQ kyy_yTrue = {{0,0,0,0,0},
                       {-0.0246594179336609525658160321134,0.1652909163793633646593041741214,0,0,0},
                       {-0.0651515275025743928203302718852,0,0.220387888505817819545738898828,0,0},
                       {-0.657722409904461693830210199723,-0.0148766026390592657162932391608,
                        -0.0180516964837024041789021739910,0.257195044067487987952428988741,0},
                        {-3.166045898824578388379801280024,0,0,0,0.28378707429581893440614131444099}};

  for (int i=0; i< 5; i++)
    for (int j=0; j< 5; j++)
    {
      BOOST_CHECK_CLOSE(kxx_x(i,j), kxx_xTrue(i,j), close_tol);
      BOOST_CHECK_CLOSE(kxy_x(i,j), kxy_xTrue(i,j), close_tol);
      BOOST_CHECK_CLOSE(kyx_y(i,j), kyx_yTrue(i,j), close_tol);
      BOOST_CHECK_CLOSE(kyy_y(i,j), kyy_yTrue(i,j), close_tol);
    }
}
#endif


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxRoe )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSA, TraitsModelRANSSAClass, PDERANSSA2D> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance function

  const Real tol = 2.e-11;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real muRef = 1.789e-5;      // kg/(m s)
//  const Real tSutherland = 110;     // K
//  const Real tRef = 300.0;
  const Real Prandtl = 0.72;

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, visc, tcond, Euler_ResidInterp_Raw);
  // Roe flux function test (same as Euler Roe test, from Roe2D.nb)

  Real dist = 60;

  HType Htensor = {{0.5},{0.1, 0.3}};
  HType logH = log(Htensor);
  ParamType paramL(logH, dist); // grid spacing and sensor values

  Real x, y, time;
  Real nx, ny;
  Real rhoL, uL, vL, tL, pL, h0L, ntL;
  Real rhoR, uR, vR, tR, pR, h0R, ntR;

  x = y = time = 0;   // not actually used in functions
  nx = 2.0/7.0; ny = sqrt(1.0-nx*nx);
  rhoL = 1.034; uL = 3.26; vL = -2.17; tL = 5.78; ntL = 4.2;
  rhoR = 0.973; uR = 1.79; vR =  0.93; tR = 6.13; ntR = 5.7;
  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL + vL*vL);
  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR + vR*vR);

  // set
  ArrayQ qL = 0;
  ArrayQ qR = 0;

  avpde.setDOFFrom( qL, SAnt2D<DensityVelocityTemperature2D<Real>>({rhoL, uL, vL, tL, ntL}) );
  avpde.setDOFFrom( qR, SAnt2D<DensityVelocityTemperature2D<Real>>({rhoR, uR, vR, tR, ntR}) );

  // advective normal flux (average)
  Real fL[5] = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*vL     , rhoL*uL*h0L, rhoL*uL*ntL};
  Real fR[5] = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*vR     , rhoR*uR*h0R, rhoR*uR*ntR};
  Real gL[5] = {rhoL*vL, rhoL*vL*uL     , rhoL*vL*vL + pL, rhoL*vL*h0L, rhoL*vL*ntL };
  Real gR[5] = {rhoR*vR, rhoR*vR*uR     , rhoR*vR*vR + pR, rhoR*vR*h0R, rhoR*vR*ntR };

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < 5; k++)
    fnTrue[k] = 0.5*(nx*(fL[k] + fR[k]) + ny*(gL[k] + gR[k]));

#if 1
  // Exact values for Roe scheme from Roe2D.nb
  fnTrue[0] -= -0.021574952725013058474459426733768;
  fnTrue[1] -= -1.7958753728220592172800048653566;
  fnTrue[2] -=  4.4515996249161671139371535471951;
  fnTrue[3] -= -6.8094427666218353919034995028459;
  fnTrue[4] -= 1.8131990192972219759746035955243;
#endif

  ArrayQ fn;
  fn = 0;
  avpde.fluxAdvectiveUpwind( paramL, x, y, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( fnTrue(3), fn(3), tol );
  BOOST_CHECK_CLOSE( fnTrue(4), fn(4), tol );

  MatrixQ mtx;
  mtx = 0;

  avpde.jacobianFluxAdvectiveAbsoluteValue(paramL, x, y, time, qL, nx, ny, mtx );
  MatrixQ mtxTrue = {
    {1.0323434331949190979834072919713,
        -0.44459540501697074517797177104962,
        -0.43697991171460424043588329213258,
        0.080449362812368834954522463171426,
        0.0},
    {-0.72312835498151845646006425483631,
        -0.010364873336226558791267034135421,
        -1.4045718375886762151895973056639,
        0.18933272986886306466125181349454,
        0.0},
    {-0.90833712876943321732424020657867,
        1.9404853199183346620104522800780,
        2.1633849842368432379888624467138,
        -0.41919712890981821676619255996783,
         0.0},
    {-0.43536491993382434782224675355112,
        -8.1758936855015456959218531030353,
        -6.9672110585297722485152708900077,
        2.7090875342447734040794006013828,
        0.0},
    { -0.4862391006955183426,
      -1.867300701071277129747,
      -1.8353156292013378,
      0.337887323811949106,
      1.1481146476462329890891029404915}
                    };
  // Values taken from pde/NS/Roe2D.nb

  for (int i=0; i<5; i++)
    for (int j=0; j<5; j++)
      BOOST_CHECK_CLOSE( mtx(i,j), mtxTrue(i,j), tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Holst2020 )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSA, TraitsModelRANSSAClass, PDERANSSA2D> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance function

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real muRef = 1.789e-5;      // kg/(m s)
//  const Real tSutherland = 110;     // K
//  const Real tRef = 300.0;
  const Real Prandtl = 0.72;

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);


  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eHolst2020,
                   gas, visc, tcond, Euler_ResidInterp_Raw);

  Real dist = 60;
  Real hxx = 1.0;
  Real hyy = 2.0;
  HType logH = {{log(hxx)},
                 {0.0, log(hyy)}};
  ParamType param(logH, dist); // grid spacing and sensor values

  Real x = 1, y = 0.5;
  Real time = 0;

  Real V = 5.0;
  Real P = 1.0;
  Real nt = 0.1;

  Real alpha = 30.0 / 180.0 * M_PI;
  for (int j = 0; j < 1; j++)
  {
    Real beta = Real(j) / 36 * 2.0 * M_PI;

    Real rho = 1.0;
    Real u = V*cos(alpha);
    Real v = V*sin(alpha);
    Real p = 1.0;
    // Derived properties
    Real c = sqrt(gamma*p/rho);
    Real t = p / (rho*R);
    Real e0 = gas.energy(rho, t) + 0.5*(u*u + v*v);
    Real mu = visc.viscosity(t);
    Real k = tcond.conductivity(mu);

    Real rhox = 0.1;
    Real ux = 0.6*cos(alpha) + 0.8*sin(alpha);
    Real vx = 0.6*sin(alpha) - 0.8*cos(alpha);
    Real px = P*cos(alpha + beta);
    Real ntx = 0.0;

    Real rhoy = 0.2;
    Real uy = 0.8*cos(alpha) + 0.6*sin(alpha);
    Real vy = 0.8*sin(alpha) - 0.6*cos(alpha);
    Real py = P*sin(alpha + beta);
    Real nty = 0.0;

    ArrayQ q = 0;
    avpde.setDOFFrom( q, SAnt2D<DensityVelocityPressure2D<Real>>({rho, u, v, p, nt}) );

    ArrayQ qx = {rhox, ux, vx, px, ntx};
    ArrayQ qy = {rhoy, uy, vy, py, nty};

    Real fac = sqrt(px*hxx*hxx*px + py*hyy*hyy*py);
    Real sRef = fac / (fac + 3.0*p);
    sRef = sRef * tanh(10.0 * sRef);
    Real ctilde = c + abs(V);
    DLA::MatrixSymS<2,Real> eTrue = 0.0;
    eTrue(0,0) = ctilde * sRef * hxx;
    eTrue(1,1) = ctilde * sRef * hyy;

//    DLA::MatrixSymS<2,Real> eps = 0.0;
//    avpde.artificialViscosity(param, x, y, time, q, qx, qy, eps);
//    SANS_CHECK_CLOSE( eTrue(0,0), eps(0,0), small_tol, close_tol );
//    SANS_CHECK_CLOSE( eTrue(0,1), eps(0,1), small_tol, close_tol );
//    SANS_CHECK_CLOSE( eTrue(1,0), eps(1,0), small_tol, close_tol );
//    SANS_CHECK_CLOSE( eTrue(1,1), eps(1,1), small_tol, close_tol );

    MatrixQ dutdu = DLA::Identity();
    dutdu(3, 0) = 0.5*(gamma - 1.0)*(u*u + v*v);
    dutdu(3, 1) = -(gamma - 1.0)*u;
    dutdu(3, 2) = -(gamma - 1.0)*v;
    dutdu(3, 3) = gamma;

    MatrixQ kxx = 0, kxy = 0;
    MatrixQ kyx = 0, kyy = 0;

    MatrixQ kxxT = 0, kxyT = 0;
    MatrixQ kyxT = 0, kyyT = 0;

    MatrixQ kxx_true = 0, kxy_true = 0;
    MatrixQ kyx_true = 0, kyy_true = 0;

    for (int m = 0; m < 5; m++)
    {
      for (int n = 0; n < 5; n++)
      {
        if (m == n)
        {
          kxxT(m,n) = ctilde * sRef * hxx;
          kxyT(m,n) = 0.0;
          kyxT(m,n) = 0.0;
          kyyT(m,n) = ctilde * sRef * hyy;
        }
        else
        {
          kxxT(m,n) = 0.0;
          kxyT(m,n) = 0.0;
          kyxT(m,n) = 0.0;
          kyyT(m,n) = 0.0;
        }
      }
    }

    kxx_true = kxxT*dutdu;
    kxy_true = kxyT*dutdu;
    kyx_true = kyxT*dutdu;
    kyy_true = kyyT*dutdu;

    // RANSSA part
    Real prandtlEddy = 0.9;
    Real cn1 = 16;
    Real sigma = 2./3.;
    Real cv1   = 7.1;
    Real ntref = 1.0;

    Real nu = mu/rho;
    Real ntr = nt/ntref;
    Real chi = rho*nt/mu;
    Real chi3 = chi*chi*chi;

    if (nt < 0)
    {
      Real fn  = (cn1 + chi3) / (cn1 - chi3);
      kxx_true(4,0) += -ntr*(nu + nt*fn)/sigma;
      kxx_true(4,4) +=     (nu + nt*fn)/sigma;

      kyy_true(4,0) += -ntr*(nu + nt*fn)/sigma;
      kyy_true(4,4) +=     (nu + nt*fn)/sigma;
    }
    else
    {
      kxx_true(4,0) += -ntr*(nu + nt)/sigma;
      kxx_true(4,4) +=     (nu + nt)/sigma;

      kyy_true(4,0) += -ntr*(nu + nt)/sigma;
      kyy_true(4,4) +=     (nu + nt)/sigma;
    }

    // Reynolds stresses diffusion matrix

    if (nt < 0)
    {
      // negative-SA has zero Reynolds stresses
    }
    else
    {
      Real fv1 = chi3 / (chi3 + cv1*cv1*cv1);
      Real muEddy = rho*nt*fv1;

      // Augment the molecular viscosity and thermal conductivity
      mu += muEddy;
      k  += muEddy*Cp/prandtlEddy;
    }

    // Navier-Stokes part
    nu = mu/rho;
    Real lambda = -2./3. * nu;
    Real kn = k/(Cv*rho);

    // d(Fv)/d(Ux)
    kxx_true(1,0) += -u*(2*nu + lambda);
    kxx_true(1,1) +=    (2*nu + lambda);

    kxx_true(2,0) += -v*nu;
    kxx_true(2,2) +=    nu;

    kxx_true(3,0) += -u*u*((2*nu + lambda) - kn) - v*v*(nu - kn) - e0*kn;
    kxx_true(3,1) +=    u*((2*nu + lambda) - kn);
    kxx_true(3,2) +=                                 v*(nu - kn);
    kxx_true(3,3) +=                                                  kn;

    // d(Fv)/d(Uy)
    kxy_true(1,0) += -v*lambda;
    kxy_true(1,2) +=    lambda;

    kxy_true(2,0) += -u*nu;
    kxy_true(2,1) +=    nu;

    kxy_true(3,0) += -u*v*(nu + lambda);
    kxy_true(3,1) +=  v*nu;
    kxy_true(3,2) +=  u*lambda;

    // d(Gv)/d(Ux)
    kyx_true(1,0) += -v*nu;
    kyx_true(1,2) +=    nu;

    kyx_true(2,0) += -u*lambda;
    kyx_true(2,1) +=    lambda;

    kyx_true(3,0) += -u*v*(nu + lambda);
    kyx_true(3,1) +=  v*lambda;
    kyx_true(3,2) +=  u*nu;

    // d(Gv)/d(Uy)
    kyy_true(1,0) += -u*nu;
    kyy_true(1,1) +=    nu;

    kyy_true(2,0) += -v*(2*nu + lambda);
    kyy_true(2,2) +=    (2*nu + lambda);

    kyy_true(3,0) += -u*u*(nu - kn) - v*v*((2*nu + lambda) - kn) - e0*kn;
    kyy_true(3,1) +=    u*(nu - kn);
    kyy_true(3,2) +=                    v*((2*nu + lambda) - kn);
    kyy_true(3,3) +=                                                  kn;

    avpde.diffusionViscous( param, x, y, time, q, qx, qy, kxx, kxy, kyx, kyy );
    for (int m = 0; m < 5; m++)
    {
      for (int n = 0; n < 5; n++)
      {
        BOOST_CHECK_CLOSE( kxx_true(m,n), kxx(m,n), close_tol );
        BOOST_CHECK_CLOSE( kxy_true(m,n), kxy(m,n), close_tol );
        BOOST_CHECK_CLOSE( kyx_true(m,n), kyx(m,n), close_tol );
        BOOST_CHECK_CLOSE( kyy_true(m,n), kyy(m,n), close_tol );
      }
    }

    // Flux accumulate
    avpde.diffusionViscous( param, x, y, time, q, qx, qy, kxx, kxy, kyx, kyy );
    for (int m = 0; m < 5; m++)
    {
      for (int n = 0; n < 5; n++)
      {
        BOOST_CHECK_CLOSE( 2*kxx_true(m,n), kxx(m,n), close_tol );
        BOOST_CHECK_CLOSE( 2*kxy_true(m,n), kxy(m,n), close_tol );
        BOOST_CHECK_CLOSE( 2*kyx_true(m,n), kyx(m,n), close_tol );
        BOOST_CHECK_CLOSE( 2*kyy_true(m,n), kyy(m,n), close_tol );
      }
    }

    ArrayQ f = 0;
    ArrayQ g = 0;
    avpde.fluxViscous(param, x, y, time, q, qx, qy, f, g);

    MatrixQ dudq = 0;
    avpde.jacobianMasterState(param, x, y, time, q, dudq);

    ArrayQ fTrue = -kxx_true*dudq*qx - kxy_true*dudq*qy;
    ArrayQ gTrue = -kyx_true*dudq*qx - kyy_true*dudq*qy;

    for (int i = 0; i < 5; i++)
    {
      SANS_CHECK_CLOSE( fTrue[i], f[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( gTrue[i], g[i], small_tol, close_tol );
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSA, TraitsModelRANSSAClass, PDERANSSA2D> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance function

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real muRef = 1.789e-5;      // kg/(m s)
//  const Real tSutherland = 110;     // K
//  const Real tRef = 300.0;
  const Real Prandtl = 0.72;

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, visc, tcond, Euler_ResidInterp_Raw);

  Real x, y, time, dx, dy;
  Real rho, u, v, t, c, nt;
  Real speed, speedTrue;

  Real dist = 60;

  HType Htensor = {{0.5},{0.1, 0.3}};
  HType logH = log(Htensor);
  ParamType paramL(logH, dist); // grid spacing and sensor values

  x = 0; y = 0; time = 0;  // not actually used in functions
  dx = 0.57; dy = -0.43;
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987; nt = 5.241;
  c = sqrt(gamma*R*t);
  speedTrue = fabs(dx*u + dy*v)/sqrt(dx*dx + dy*dy) + c;

  Real qDataPrim[5] = {rho, u, v, t, nt};
  string qNamePrim[5] = {"Density", "VelocityX", "VelocityY", "Temperature", "SANutilde"};
  ArrayQ q;
  avpde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );

  avpde.speedCharacteristic( paramL, x, y, time, dx, dy, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  speedTrue = sqrt(u*u + v*v) + c;
  avpde.speedCharacteristic( paramL, x, y, time, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSA, TraitsModelRANSSAClass, PDERANSSA2D> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real muRef = 1.789e-5;      // kg/(m s)
//  const Real tSutherland = 110;     // K
//  const Real tRef = 300.0;
  const Real Prandtl = 0.72;

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, visc, tcond, Euler_ResidInterp_Raw);

  ArrayQ q;
  q(0) =  1;
  q(3) =  1;
  BOOST_CHECK( avpde.isValidState(q) == true );

  q(0) =  1;
  q(3) = -1;
  BOOST_CHECK( avpde.isValidState(q) == false );

  q(0) = -1;
  q(3) =  1;
  BOOST_CHECK( avpde.isValidState(q) == false );

  q(0) = -1;
  q(3) = -1;
  BOOST_CHECK( avpde.isValidState(q) == false );

  q(0) =  1;
  q(3) =  0;
  BOOST_CHECK( avpde.isValidState(q) == false );

  q(0) =  0;
  q(3) =  1;
  BOOST_CHECK( avpde.isValidState(q) == false );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
