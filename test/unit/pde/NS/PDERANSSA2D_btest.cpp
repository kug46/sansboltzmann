// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDERANSSA2D_btest
//
// test of 2-D compressible RANS with SA PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "Topology/Dimension.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/QRANSSA2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/PDERANSSA2D.h"

//Explicitly instantiate the class so coverage information is correct
namespace SANS
{
template class TraitsSizeRANSSA<PhysD2>;
template class TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel>;
template class PDERANSSA2D< TraitsSizeRANSSA,
                            TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDERANSSA2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( PDEClass::D == 2 );
  BOOST_CHECK( PDEClass::N == 5 );
  BOOST_CHECK( ArrayQ::M == 5 );
  BOOST_CHECK( MatrixQ::M == 5 );
  BOOST_CHECK( MatrixQ::N == 5 );

#ifdef SANS_SA_QCR
  BOOST_CHECK_MESSAGE(false, "Dot not commit with SANS_SA_QCR on!");
#endif
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setDOFFrom )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real rho = 1.137;
  Real u = 0.784;
  Real v = -0.231;
  Real t = 0.987;
  Real p = R*rho*t;
  Real E = gas.energy(rho,t) + 0.5*(u*u + v*v);
  Real nt = 4.2;

  PyDict d;

  // set
  Real qDataPrim[5] = {rho, u, v, t, nt};
  string qNamePrim[5] = {"Density", "VelocityX", "VelocityY", "Temperature", "SANutilde"};
  ArrayQ qTrue = {rho, u, v, p, nt};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

  SAnt2D<DensityVelocityPressure2D<Real>> qdata1;
  qdata1.Density = rho; qdata1.VelocityX = u; qdata1.VelocityY = v; qdata1.Pressure = p; qdata1.SANutilde = nt;
  q = 0;
  pde.setDOFFrom( q, qdata1 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

  PyDict rhoVP;
  rhoVP[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.PrimitivePressure;
  rhoVP[SAnt2DParams<DensityVelocityPressure2DParams>::params.rho] = rho;
  rhoVP[SAnt2DParams<DensityVelocityPressure2DParams>::params.u] = u;
  rhoVP[SAnt2DParams<DensityVelocityPressure2DParams>::params.v] = v;
  rhoVP[SAnt2DParams<DensityVelocityPressure2DParams>::params.p] = p;
  rhoVP[SAnt2DParams<DensityVelocityPressure2DParams>::params.nt]  = nt;

  d[SAVariableType2DParams::params.StateVector] = rhoVP;
  SAVariableType2DParams::checkInputs(d);
  q = pde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );


  SAnt2D<DensityVelocityTemperature2D<Real>> qdata2({rho, u, v, t, nt});
  q = 0;
  pde.setDOFFrom( q, qdata2 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

  PyDict rhoVT;
  rhoVT[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.PrimitiveTemperature;
  rhoVT[SAnt2DParams<DensityVelocityTemperature2DParams>::params.rho] = rho;
  rhoVT[SAnt2DParams<DensityVelocityTemperature2DParams>::params.u] = u;
  rhoVT[SAnt2DParams<DensityVelocityTemperature2DParams>::params.v] = v;
  rhoVT[SAnt2DParams<DensityVelocityTemperature2DParams>::params.t] = t;
  rhoVT[SAnt2DParams<DensityVelocityTemperature2DParams>::params.nt]  = nt;

  d[SAVariableType2DParams::params.StateVector] = rhoVT;
  SAVariableType2DParams::checkInputs(d);
  q = pde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );


  SAnt2D<Conservative2D<Real>> qdata3({rho, rho*u, rho*v, rho*E, rho*nt});
  q = 0;
  pde.setDOFFrom( q, qdata3 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

  PyDict Conservative;
  Conservative[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.Conservative;
  Conservative[SAnt2DParams<Conservative2DParams>::params.rho]   = rho;
  Conservative[SAnt2DParams<Conservative2DParams>::params.rhou]  = rho*u;
  Conservative[SAnt2DParams<Conservative2DParams>::params.rhov]  = rho*v;
  Conservative[SAnt2DParams<Conservative2DParams>::params.rhoE]  = rho*E;
  Conservative[SAnt2DParams<Conservative2DParams>::params.rhont] = rho*nt;

  d[SAVariableType2DParams::params.StateVector] = Conservative;
  SAVariableType2DParams::checkInputs(d);
  q = pde.setDOFFrom( d );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( masterState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real close_tol = 2.e-12;

  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 5 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // function tests

  Real x, y, time;
  Real rho, u, v, t, nt;

  x = 0; y = 0;   // not actually used in functions
  rho = 1.225;              // kg/m^3
  u = 0.974; v = -0.231;    // m/s
  t = 1.3;               // K
  nt = 4.279e-4;            // m^2/s; gives chi = 29.3

  Real E = Cv*t + 0.5*(u*u + v*v);

  // set
  ArrayQ q;
  pde.setDOFFrom( q, SAnt2D<DensityVelocityTemperature2D<Real>>({rho, u, v, t, nt}) );

  // conservative flux
  ArrayQ uTrue;
  uTrue[0] = rho;
  uTrue[1] = rho*u;
  uTrue[2] = rho*v;
  uTrue[3] = rho*E;
  uTrue[4] = rho*nt;

  ArrayQ uCons = 0;
  pde.masterState( x, y, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), close_tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), close_tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), close_tol );
  BOOST_CHECK_CLOSE( uTrue(3), uCons(3), close_tol );
  BOOST_CHECK_CLOSE( uTrue(4), uCons(4), close_tol );

  Real rhox = 0.0421, rhoy = 0.0127;
  Real ux = 0.0521, uy = -0.0124;
  Real vx = -0.0312, vy = -0.0022;
  Real tx = -0.0715, ty = -0.0111;
  Real ntx = 7.515e-5, nty = -3.421e-5;
  Real px, py;

  px = R*(rho*tx + rhox*t);
  py = R*(rho*ty + rhoy*t);

//  Real rhoE = p/(gamma-1) + 0.5*rho*(u*u + v*v);
  Real rhoEx = px/(gamma-1) + 0.5*rhox*(u*u + v*v) + rho*(u*ux + v*vx);
  Real rhoEy = py/(gamma-1) + 0.5*rhoy*(u*u + v*v) + rho*(u*uy + v*vy);

  ArrayQ qx = {rhox, ux, vx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, py, nty};

  ArrayQ Ux = 0, Uy = 0;
  ArrayQ UxTrue = 0, UyTrue = 0;
// master state gradient
  pde.masterStateGradient(x, y, time, q, qx, qy, Ux, Uy);

  UxTrue[0] = rhox;
  UxTrue[1] = rho*ux + rhox*u;
  UxTrue[2] = rho*vx + rhox*v;
  UxTrue[3] = rhoEx;
  UxTrue[4] = rho*ntx + rhox*nt;

  UyTrue[0] = rhoy;
  UyTrue[1] = rho*uy + rhoy*u;
  UyTrue[2] = rho*vy + rhoy*v;
  UyTrue[3] = rhoEy;
  UyTrue[4] = rho*nty + rhoy*nt;

  for (int i=0; i< 5; i++)
  {
    BOOST_CHECK_CLOSE( UxTrue(i), Ux(i), close_tol );
    BOOST_CHECK_CLOSE( UyTrue(i), Uy(i), close_tol );
  }

  Real rhoxx, uxx, vxx, pxx, ntxx;
  Real rhoxy, uxy, vxy, pxy, ntxy;
  Real rhoyy, uyy, vyy, pyy, ntyy;

  rhoxx = 0.0030748850488354620586025544703231;
  uxx = 0.022424242424242424242424242424242;
  vxx = 0.0048484848484848484848484848484848;
  pxx = 0.0057917355371900826446280991735537;
  ntxx = 0.27933884297520661157024793388430;

  rhoxy = 0.0061728317355371900826446280991736;
  uxy = 0.0048666666666666666666666666666667;
  vxy = 0.0097333333333333333333333333333333;
  pxy = 0.011626909090909090909090909090909;
  ntxy = 0.31854545454545454545454545454545;

  rhoyy = 0.0061959798545454545454545454545455;
  uyy = 0.030545454545454545454545454545455;
  vyy = -0.0068004722550177095631641086186541;
  pyy = 0.0077803400000000000000000000000000;
  ntyy = 0.5197400000000000000000000000000;

  ArrayQ qxx = {rhoxx, uxx, vxx, pxx, ntxx};
  ArrayQ qxy = {rhoxy, uxy, vxy, pxy, ntxy};
  ArrayQ qyy = {rhoyy, uyy, vyy, pyy, ntyy};

//  Real rhoE = p/(gamma-1) + 0.5*rho*(u*u + v*v);
//  Real rhoEx = px/(gamma-1) + 0.5*rhox*(u*u + v*v) + rho*(u*ux + v*vx);
//  Real rhoEy = py/(gamma-1) + 0.5*rhoy*(u*u + v*v) + rho*(u*uy + v*vy);

  Real rhoExx = pxx/(gamma-1.) + 0.5*rhoxx*(u*u + v*v) + 2.*rhox*(u*ux + v*vx)
            + rho*(ux*ux + u*uxx + vx*vx + v*vxx);
  Real rhoExy = pxy/(gamma-1.) + 0.5*rhoxy*(u*u + v*v)
               + rhox*(u*uy + v*vy)  + rhoy*(u*ux + v*vx) + rho*(uy*ux + u*uxy + vy*vx + v*vxy);
  Real rhoEyy = pyy/(gamma-1.) + 0.5*rhoyy*(u*u + v*v) + 2.*rhoy*(u*uy + v*vy)
            + rho*(uy*uy + u*uyy + vy*vy + v*vyy);

  ArrayQ Uxx = 0, Uxy = 0, Uyy;
  ArrayQ UxxTrue = 0, UxyTrue = 0, UyyTrue = 0;
// master state gradient
  pde.masterStateHessian(x, y, time, q, qx, qy, qxx, qxy, qyy, Uxx, Uxy, Uyy);

  UxxTrue[0] = rhoxx;
  UxxTrue[1] = rho*uxx + 2*rhox*ux + rhoxx*u;
  UxxTrue[2] = rho*vxx + 2*rhox*vx + rhoxx*v;
  UxxTrue[3] = rhoExx;
  UxxTrue[4] = rho*ntxx + 2*rhox*ntx + rhoxx*nt;

  UxyTrue[0] = rhoxy;
  UxyTrue[1] = rho*uxy + rhox*uy + rhoy*ux + rhoxy*u;
  UxyTrue[2] = rho*vxy + rhox*vy + rhoy*vx + rhoxy*v;
  UxyTrue[3] = rhoExy;
  UxyTrue[4] = rho*ntxy + rhox*nty + rhoy*ntx + rhoxy*nt;

  UyyTrue[0] = rhoyy;
  UyyTrue[1] = rho*uyy + 2*rhoy*uy + rhoyy*u;
  UyyTrue[2] = rho*vyy + 2*rhoy*vy + rhoyy*v;
  UyyTrue[3] = rhoEyy;
  UyyTrue[4] = rho*ntyy + 2*rhoy*nty + rhoyy*nt;

  for (int i=0; i< 5; i++)
  {
    BOOST_CHECK_CLOSE( UxxTrue(i), Uxx(i), close_tol );
    BOOST_CHECK_CLOSE( UxyTrue(i), Uxy(i), close_tol );
    BOOST_CHECK_CLOSE( UyyTrue(i), Uyy(i), close_tol );
  }


}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jacobianMasterState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 5.e-8;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // function tests

  Real rho, u, v, t, nt;

  rho = 1.225;              // kg/m^3
  u = 6.974; v = -3.231;    // m/s
  t = 288.15;               // K
  nt = 4.279e-4;            // m^2/s; gives chi = 29.3

  Real x = 0, y = 0, time = 0;

  // set
  ArrayQ q;
  pde.setDOFFrom( q, SAnt2D<DensityVelocityTemperature2D<Real>>({rho, u, v, t, nt}) );

  // conservative flux
  ArrayQ uCons0, uCons1;
  MatrixQ dudq_diff[2] = {0,0};

  std::vector<Real> step = {1e-3, 5e-4};
  std::vector<Real> rate_range = {1.9, 2.1};

  for (std::size_t istep = 0; istep < step.size(); istep++ )
  {
    for (int ivar = 0; ivar < ArrayQ::M; ivar++)
    {
      q[ivar] -= step[istep];
      uCons0 = 0;
      pde.masterState( x, y, time, q, uCons0 );

      q[ivar] += 2*step[istep];
      uCons1 = 0;
      pde.masterState( x, y, time, q, uCons1 );

      q[ivar] -= step[istep];

      for (int iEq = 0; iEq < pde.N; iEq++)
        dudq_diff[istep](iEq,ivar) = (uCons1[iEq] - uCons0[iEq])/(2*step[istep]);
    }
  }

  MatrixQ dudq = 0;
  pde.jacobianMasterState(x, y, time, q, dudq);

  for (int iEq = 0; iEq < pde.N; iEq++)
  {
    for (int ivar = 0; ivar < ArrayQ::M; ivar++)
    {
      Real err_vec[2] = { fabs( dudq(iEq,ivar) - dudq_diff[0](iEq,ivar) ),
                          fabs( dudq(iEq,ivar) - dudq_diff[1](iEq,ivar) )};

#if 0
      std::cout << "dudq(iEq,ivar) = " << dudq(iEq,ivar) <<
                   " dudq_diff[0] " << dudq_diff[0](iEq,ivar) <<
                   " dudq_diff[1] " << dudq_diff[1](iEq,ivar) << std::endl;
#endif

      // Error in finite-difference jacobian is either zero as for linear or quadratic residuals,
      // or is nonzero for general nonlinear residual where we need to check error convergence rate
      if (err_vec[0] > small_tol && err_vec[1] > small_tol)
      {
        Real rate = log(err_vec[1]/err_vec[0])/log(step[1]/step[0]);

        BOOST_CHECK_MESSAGE( rate >= rate_range[0] && rate <= rate_range[1],
                             "Rate check failed at : ivar = " << ivar <<
                             " iEq = " << iEq << ": rate = " << rate <<
                             ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" );
      }

    }
  }
}



//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 5 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // function tests

  Real x, y, time;
  Real rho, u, v, t, p, nt;

  x = 0; y = 0;   // not actually used in functions
  rho = 1.225;              // kg/m^3
  u = 6.974; v = -3.231;    // m/s
  t = 288.15;               // K
  nt = 4.279e-4;            // m^2/s; gives chi = 29.3
  p  = R*rho*t;

  // set
  Real qDataPrim[5] = {rho, u, v, t, nt};
  string qNamePrim[5] = {"Density", "VelocityX", "VelocityY", "Temperature", "SANutilde"};
  ArrayQ qTrue = {rho, u, v, p, nt};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), close_tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), close_tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), close_tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), close_tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), close_tol );

  // conservative flux
  ArrayQ uTrue = {1.225, 8.54315, -3.957975, 253337.3230726625, 0.0005241775};
  ArrayQ uCons = 0;
  pde.masterState( x, y, time, q, uCons );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), close_tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), close_tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), close_tol );
  BOOST_CHECK_CLOSE( uTrue(3), uCons(3), close_tol );
  BOOST_CHECK_CLOSE( uTrue(4), uCons(4), close_tol );

  // conservative flux
  ArrayQ ftTrue = {1.225, 8.54315, -3.957975, 253337.3230726625, 0.0005241775};
  ArrayQ ft = 0;
  pde.fluxAdvectiveTime( x, y, time, q, ft );
  for (int i = 0; i < pde.N; i++)
    BOOST_CHECK_CLOSE( ftTrue(i), ft(i), close_tol );

  pde.fluxAdvectiveTime( x, y, time, q, ft );

  for (int i = 0; i < pde.N; i++)
    BOOST_CHECK_CLOSE( 2*ftTrue(i), ft(i), close_tol );

  // advective flux
  ArrayQ fTrue = {8.54315, 101380.0355281, -27.60291765, 2.4733833484631483e6, 0.003655613885};
  ArrayQ gTrue = {-3.957975, -27.60291765, 101333.243817225, -1.1458992828913727e6, -0.0016936175025};
  ArrayQ f, g;
  f = 0;
  g = 0;
  pde.fluxAdvective( x, y, time, q, f, g );
  for (int i = 0; i < pde.N; i++)
  {
    BOOST_CHECK_CLOSE( fTrue(i), f(i), close_tol );
    BOOST_CHECK_CLOSE( gTrue(i), g(i), close_tol );
  }


  Real rhox = 0.0421, rhoy = 0.0127;
  Real ux = 0.0521, uy = -0.0124;
  Real vx = -0.0312, vy = -0.0022;
  Real tx = -0.0715, ty = -0.0111;
  Real ntx = 7.515e-5, nty = -3.421e-5;
  Real px, py;

  px = R*(rho*tx + rhox*t);
  py = R*(rho*ty + rhoy*t);

  ArrayQ qx = {rhox, ux, vx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, py, nty};


  // viscous flux
  f = 0;
  g = 0;
  pde.fluxViscous(x, y, time, q, qx, qy, f, g);

  fTrue = {0, -0.00003792902328566897, 0.00002331351619250705, 0.042694259083174105, -6.11045589375e-8};
  gTrue = {0, 0.00002331351619250705, 0.000020140881725942635, 0.00677833191492301, 2.78161937625e-8};

#ifdef SANS_SA_QCR
  Real cv1 = 7.1;
  Real cr1 = 0.3;
  Real mu = visc.viscosity( t );

  Real chi  = rho*nt/mu;
  Real chi3 = power<3>(chi);

  Real fv1 = chi3 / (chi3 + power<3>(cv1));
  Real muEddy = rho*nt*fv1;
  Real lambdaEddy = -2./3. * muEddy;

  Real tauRxx = muEddy*(2*ux   ) + lambdaEddy*(ux + vy);
  Real tauRxy = muEddy*(uy + vx);
  Real tauRyy = muEddy*(2*vy   ) + lambdaEddy*(ux + vy);

  Real magGradV = sqrt(ux*ux + uy*uy + vx*vx + vy*vy);

  Real Wxy = (uy - vx);

  Real Oxy = Wxy/magGradV;
  Real Oyx = -Oxy;

  Real qcrxx = cr1*(2*Oxy*tauRxy);
  Real qcrxy = cr1*(Oxy*tauRyy + Oyx*tauRxx);
  Real qcryy = cr1*(2*Oyx*tauRxy);

  fTrue(pde.ixMom) += qcrxx;
  fTrue(pde.iyMom) += qcrxy;
  fTrue(pde.iEngy) += u*qcrxx + v*qcrxy;

  gTrue(pde.ixMom) += qcrxy;
  gTrue(pde.iyMom) += qcryy;
  gTrue(pde.iEngy) += u*qcrxy + v*qcryy;
#endif

  SANS_CHECK_CLOSE( fTrue(0), f(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(1), f(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(2), f(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(3), f(3), small_tol, close_tol*10 );
  SANS_CHECK_CLOSE( fTrue(4), f(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(0), g(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(1), g(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(2), g(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(3), g(3), small_tol, close_tol*100 );
  SANS_CHECK_CLOSE( gTrue(4), g(4), small_tol, close_tol );

  pde.fluxViscous(x, y, time, q, qx, qy, f, g); //accumulations
  SANS_CHECK_CLOSE( 2*fTrue(0), f(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*fTrue(1), f(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*fTrue(2), f(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*fTrue(3), f(3), small_tol, close_tol*10 );
  SANS_CHECK_CLOSE( 2*fTrue(4), f(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*gTrue(0), g(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*gTrue(1), g(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*gTrue(2), g(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*gTrue(3), g(3), small_tol, close_tol*100 );
  SANS_CHECK_CLOSE( 2*gTrue(4), g(4), small_tol, close_tol );

  Real nx, ny;

  nx = 1; ny = 0;
  f = 0;
  pde.fluxViscous(x, y, time, q, qx, qy, q, qx, qy, nx, ny, f);
  SANS_CHECK_CLOSE( fTrue(0), f(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(1), f(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(2), f(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(3), f(3), small_tol, close_tol*10 );
  SANS_CHECK_CLOSE( fTrue(4), f(4), small_tol, close_tol );

  nx = 0; ny = 1;
  f = 0;
  pde.fluxViscous(x, y, time, q, qx, qy, q, qx, qy, nx, ny, f);
  SANS_CHECK_CLOSE( gTrue(0), f(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(1), f(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(2), f(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(3), f(3), small_tol, close_tol*100 );
  SANS_CHECK_CLOSE( gTrue(4), f(4), small_tol, close_tol );

  // viscous flux -- negative SA

  qDataPrim[4] *= -1;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );

  f = 0;
  g = 0;
  pde.fluxViscous(x, y, time, q, qx, qy, f, g);

  fTrue = {0, -1.2689973333333333e-6, 7.80004e-7, 0.0017734495230066667, -6.102943673148725e-8};
  gTrue = {0, 7.80004e-7, 6.738566666666667e-7, 0.000280346415006, 2.7781996414959137e-8};
  SANS_CHECK_CLOSE( fTrue(0), f(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(1), f(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(2), f(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(3), f(3), small_tol, close_tol*10 );
  SANS_CHECK_CLOSE( fTrue(4), f(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(0), g(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(1), g(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(2), g(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(3), g(3), small_tol, close_tol*100 );
  SANS_CHECK_CLOSE( gTrue(4), g(4), small_tol, close_tol );

  nx = 1; ny = 0;
  f = 0;
  pde.fluxViscous(x, y, time, q, qx, qy, q, qx, qy, nx, ny, f);
  SANS_CHECK_CLOSE( fTrue(0), f(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(1), f(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(2), f(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(3), f(3), small_tol, close_tol*10 );
  SANS_CHECK_CLOSE( fTrue(4), f(4), small_tol, close_tol );

  nx = 0; ny = 1;
  f = 0;
  pde.fluxViscous(x, y, time, q, qx, qy, q, qx, qy, nx, ny, f);
  SANS_CHECK_CLOSE( gTrue(0), f(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(1), f(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(2), f(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(3), f(3), small_tol, close_tol*100 );
  SANS_CHECK_CLOSE( gTrue(4), f(4), small_tol, close_tol );
}

#ifndef SANS_SA_QCR
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( diffusionViscous )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-12;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, time;
  Real rho, u, v, t;
  Real nt, chi;

  x = 0; y = 0; time = 0;   // not actually used in functions
  rho = 1.225;
  u = 6.974;
  v = -3.231;
  t = 2.881;

  // gradient

  Real rhox = 0.0421, rhoy = 0.0127;
  Real ux = 0.0521, uy = -0.0124;
  Real vx = -0.0312, vy = -0.0022;
  Real tx = -0.0715, ty = -0.0111;
  Real ntx = 7.515e-5, nty = -3.421e-5;
  Real px, py;

  px = R*(rho*tx + rhox*t);
  py = R*(rho*ty + rhoy*t);

  ArrayQ qx = {rhox, ux, vx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, py, nty};

  // case 1: nt > 0

  chi = 5.241;
  nt  = chi*(muRef/rho);

  ArrayQ q;
  Real qDataPrim[5] = {rho, u, v, t, nt};
  string qNamePrim[5] = {"Density", "VelocityX", "VelocityY", "Temperature", "SANutilde"};
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );

  MatrixQ kxx = 0, kxy = 0, kyx = 0, kyy = 0;
  pde.diffusionViscous( x, y, time, q, qx, qy, kxx, kxy, kyx, kyy );

  MatrixQ kxxTrue =
    {{0, 0, 0, 0, 0},
     {-19.00241147480699, 2.724750713336247, 0, 0, 0},
     {6.602752166092062, 0, 2.043563035002186, 0, 0},
     {-75.76382912041833, -5.381036833102752, 4.693909933543209, 3.496336149685939, 0},
     {-26.69016970819981*rho, 0, 0, 0, 6.238400666389005*rho}};

  MatrixQ kxyTrue =
    {{0, 0, 0, 0, 0},
     {-4.401834777394708, 0, -1.362375356668124, 0, 0},
     {-14.25180860610524, 2.043563035002186, 0, 0, 0},
     {15.34919786877535, -6.602752166092062, -9.501205737403495, 0, 0},
     {0, 0, 0, 0, 0}};

  MatrixQ kyxTrue =
    {{0, 0, 0, 0, 0},
     {6.602752166092062, 0, 2.043563035002186, 0, 0},
     {9.501205737403495, -1.362375356668124, 0, 0, 0},
     {15.34919786877535, 4.401834777394708, 14.25180860610524, 0, 0},
     {0, 0, 0, 0, 0}};

  MatrixQ kyyTrue =
    {{0, 0, 0, 0, 0},
     {-14.25180860610524, 2.043563035002186, 0, 0, 0},
     {8.803669554789415, 0, 2.724750713336247, 0, 0},
     {-49.74428879697349, -10.13163970180450, 2.492992544845855, 3.496336149685939, 0},
     {-26.69016970819981*rho, 0, 0, 0, 6.238400666389005*rho}};

  for (int i = 0; i < 5; i++)
  {
    for (int j = 0; j < 5; j++)
    {
      SANS_CHECK_CLOSE( kxxTrue(i,j), kxx(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kxyTrue(i,j), kxy(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kyxTrue(i,j), kyx(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kyyTrue(i,j), kyy(i,j), small_tol, close_tol );
    }
  }

  ArrayQ fTrue = 0;
  ArrayQ gTrue = 0;
  pde.fluxViscous(x, y, time, q, qx, qy, fTrue, gTrue);

  MatrixQ dudq = 0;
  pde.jacobianMasterState(x, y, time, q, dudq);

  ArrayQ f = -kxx*dudq*qx - kxy*dudq*qy;
  ArrayQ g = -kyx*dudq*qx - kyy*dudq*qy;

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( fTrue[i], f[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( gTrue[i], g[i], small_tol, close_tol );
  }


  // case 2: zero nt

  nt = 0;

  qDataPrim[4] = nt;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );

  kxx = 0, kxy = 0, kyx = 0, kyy = 0;
  pde.diffusionViscous( x, y, time, q, qx, qy, kxx, kxy, kyx, kyy );

  kxxTrue =
    {{0, 0, 0, 0, 0},
     {-7.590748299319728, 1.088435374149660, 0, 0, 0},
     {2.637551020408163, 0, 0.8163265306122449, 0, 0},
     {-26.00660201814059, -3.479092970521542, 2.491020408163265, 1.587301587301587, 0},
     {0, 0, 0, 0, 0.9995835068721366*rho}};

  kxyTrue =
    {{0, 0, 0, 0, 0},
     {-1.758367346938776, 0, -0.5442176870748299, 0, 0},
     {-5.693061224489796, 0.8163265306122449, 0, 0, 0},
     {6.131426938775510, -2.637551020408163, -3.795374149659864, 0, 0},
     {0, 0, 0, 0, 0}};

  kyxTrue =
    {{0, 0, 0, 0, 0},{2.637551020408163, 0, 0.8163265306122449, 0, 0},
     {3.795374149659864, -0.5442176870748299, 0, 0, 0},
     {6.131426938775510, 1.758367346938776, 5.693061224489796, 0, 0},
     {0, 0, 0, 0, 0}};

  kyyTrue =
    {{0, 0, 0, 0, 0},
     {-5.693061224489796, 0.8163265306122449, 0, 0, 0},
     {3.516734693877551, 0, 1.088435374149660, 0, 0},
     {-15.61277480725624, -5.376780045351474, 1.611836734693878, 1.587301587301587, 0},
     {0, 0, 0, 0, 0.9995835068721366*rho}};

  for (int i = 0; i < 5; i++)
  {
    for (int j = 0; j < 5; j++)
    {
      SANS_CHECK_CLOSE( kxxTrue(i,j), kxx(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kxyTrue(i,j), kxy(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kyxTrue(i,j), kyx(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kyyTrue(i,j), kyy(i,j), small_tol, close_tol );
    }
  }


  fTrue = 0;
  gTrue = 0;
  pde.fluxViscous(x, y, time, q, qx, qy, fTrue, gTrue);

  dudq = 0;
  pde.jacobianMasterState(x, y, time, q, dudq);

  f = -kxx*dudq*qx - kxy*dudq*qy;
  g = -kyx*dudq*qx - kyy*dudq*qy;

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( fTrue[i], f[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( gTrue[i], g[i], small_tol, close_tol );
  }


  // case 3: nt < 0 (negative SA)

  chi = -5.241;
  nt  = chi*(muRef/rho);

  qDataPrim[4] = nt;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );

  kxx = 0, kxy = 0, kyx = 0, kyy = 0;
  pde.diffusionViscous( x, y, time, q, qx, qy, kxx, kxy, kyx, kyy );

  kxxTrue =
    {{0, 0, 0, 0, 0},
     {-7.590748299319728, 1.088435374149660, 0, 0, 0},
     {2.637551020408163, 0, 0.8163265306122449, 0, 0},
     {-26.00660201814059, -3.479092970521542, 2.491020408163265, 1.587301587301587, 0},
     {22.20633785154143*rho, 0, 0, 0, 5.190376620518652*rho}};

  kxyTrue =
    {{0, 0, 0, 0, 0},
     {-1.758367346938776, 0, -0.5442176870748299, 0, 0},
     {-5.693061224489796, 0.8163265306122449, 0, 0, 0},
     {6.131426938775510, -2.637551020408163, -3.795374149659864, 0, 0},
     {0, 0, 0, 0, 0}};

  kyxTrue =
    {{0, 0, 0, 0, 0},
     {2.637551020408163, 0, 0.8163265306122449, 0, 0},
     {3.795374149659864, -0.5442176870748299, 0, 0, 0},
     {6.131426938775510, 1.758367346938776, 5.693061224489796, 0, 0},
     {0, 0, 0, 0, 0}};

  kyyTrue =
    {{0, 0, 0, 0, 0},
     {-5.693061224489796, 0.8163265306122449, 0, 0, 0},
     {3.516734693877551, 0, 1.088435374149660, 0, 0},
     {-15.61277480725624, -5.376780045351474, 1.611836734693878, 1.587301587301587, 0},
     {22.20633785154143*rho, 0, 0, 0, 5.190376620518652*rho}};

  for (int i = 0; i < 5; i++)
  {
    for (int j = 0; j < 5; j++)
    {
      SANS_CHECK_CLOSE( kxxTrue(i,j), kxx(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kxyTrue(i,j), kxy(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kyxTrue(i,j), kyx(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( kyyTrue(i,j), kyy(i,j), small_tol, close_tol );
    }
  }

  fTrue = 0;
  gTrue = 0;
  pde.fluxViscous(x, y, time, q, qx, qy, fTrue, gTrue);

  dudq = 0;
  pde.jacobianMasterState(x, y, time, q, dudq);

  f = -kxx*dudq*qx - kxy*dudq*qy;
  g = -kyx*dudq*qx - kyy*dudq*qy;

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( fTrue[i], f[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( gTrue[i], g[i], small_tol, close_tol );
  }
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( diffusionViscous_Surreal )
{

  // Use conservative variables here in order to differentiate with Surreal
  typedef QTypeConservative QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef SurrealS<PhysD2::D*PDEClass::N> SurrealClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;
  typedef PDEClass::ArrayQ<SurrealClass> ArrayQSurreal;

  bool verbose = false;

  const Real small_tol = 1.e-10;
  const Real close_tol = 1.e-10;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real tSuth = 110./300.; //somewhat random...
  const Real tRef  = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, time;
  Real rho, u, v, t;

  x = 0; y = 0; time = 0;   // not actually used in functions
  rho = 1.225;
  u = 6.974;
  v = -3.231;
  t = 2.8815;

  Real e = gas.energy(rho, t);
  Real E = e + 0.5*(u*u + v*v);

  Real rhox = 0.0421, rhoy = 0.0127;
  Real ux = 0.0521, uy = -0.0124;
  Real vx = -0.0312, vy = -0.0022;
  Real tx = -0.0715, ty = -0.0111;

  //Real px = R*(rho*tx + rhox*t);
  //Real py = R*(rho*ty + rhoy*t);

  Real rhoux = rhox*u + ux*rho;
  Real rhouy = rhoy*u + uy*rho;

  Real rhovx = rhox*v + vx*rho;
  Real rhovy = rhoy*v + vy*rho;

  Real ex, ey;
  gas.energyGradient(rho, t, rhox, tx, ex);
  gas.energyGradient(rho, t, rhoy, ty, ey);

  Real Ex = ex + ux*u + vx*v;
  Real Ey = ey + uy*u + vy*v;

  Real rhoEx = rhox*E + rho*Ex;
  Real rhoEy = rhoy*E + rho*Ey;

  Real nt, ntx, nty;
  Real chi, chix, chiy;

  ArrayQ q, qx, qy;
  ArrayQSurreal qxS, qyS;
  MatrixQ kxx = 0, kxy = 0, kyx = 0, kyy = 0, dudq;
  ArrayQ f, g;
  ArrayQSurreal fTrue, gTrue;

  ArrayQ dqx = 0, dqy = 0;
  ArrayQ dux = 0, duy = 0;
  ArrayQ dF = 0, dG = 0;
  ArrayQ dF2 = 0, dG2 = 0;
  ArrayQ dFTrue = 0, dGTrue = 0;
  dqx = {0.01, -0.01, 0.02, -0.02, 0.03};
  dqy = {0.005, -0.005, 0.025, -0.025, -0.017};

  // set the complete state vectors (sans nt)
  // the nt values are set for each case
  pde.setDOFFrom( q, SAnt2D<Conservative2D<Real>>({rho , rho*u, rho*v, rho*E, 0}));

  qxS = qx = {rhox, rhoux, rhovx, rhoEx, 0};
  qyS = qy = {rhoy, rhouy, rhovy, rhoEy, 0};

  // set derivatives
  for (int i = 0; i < PDEClass::N; i++)
  {
    qxS[i].deriv(i            ) = 1;
    qyS[i].deriv(i+PDEClass::N) = 1;
  }

  // case 1: positive nt
  if (verbose) cout << "diffusionViscous: case1" << endl;

  chi  = 5.241;
  chix = 1.2; chiy = -0.3;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  fTrue = gTrue = 0;
  pde.fluxViscous(x, y, time, q, qxS, qyS, fTrue, gTrue);

  // Compute the hand coded diffusion matrix
  kxx = kxy = kyx = kyy = 0;
  pde.diffusionViscous( x, y, time, q, qx, qy, kxx, kxy, kyx, kyy );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " -f(" << i << ").deriv(" << j << ") = " << -fTrue(i).deriv(j) << " | " << kxx(i,j) << std::endl;
      SANS_CHECK_CLOSE( -fTrue(i).deriv(j      ), kxx(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -f(" << i << ").deriv(" << pde.N+j << ") = " << -fTrue(i).deriv(pde.N+j) << " | " << kxy(i,j) << std::endl;
      SANS_CHECK_CLOSE( -fTrue(i).deriv(pde.N+j), kxy(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -g(" << i << ").deriv(" << j << ") = " << -gTrue(i).deriv(j) << " | " << kyx(i,j) << std::endl;
      SANS_CHECK_CLOSE( -gTrue(i).deriv(j      ), kyx(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -g(" << i << ").deriv(" << pde.N+j << ") = " << -gTrue(i).deriv(pde.N+j) << " | " << kyy(i,j) << std::endl;
      SANS_CHECK_CLOSE( -gTrue(i).deriv(pde.N+j), kyy(i,j), small_tol, close_tol );
    }

  dudq = 0;
  pde.jacobianMasterState(x, y, time, q, dudq);

  f = -kxx*dudq*qx - kxy*dudq*qy;
  g = -kyx*dudq*qx - kyy*dudq*qy;

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( fTrue[i].value(), f[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( gTrue[i].value(), g[i], small_tol, close_tol );
  }

  dF = 0; dG = 0;
  pde.perturbedGradFluxViscous(x, y, time, q, qx, qy, dqx, dqy, dF, dG);

  dux = dudq*dqx;
  duy = dudq*dqy;

  dFTrue = -kxx*dudq*dqx - kxy*dudq*dqy;
  dGTrue = -kyx*dudq*dqx - kyy*dudq*dqy;

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( dFTrue[i], dF[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dGTrue[i], dG[i], small_tol, close_tol );
  }

  dF2 = 0; dG2 = 0;
  pde.perturbedGradFluxViscous(x, y, time, kxx, kxy, kyx, kyy, dux, duy, dF2, dG2);

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( dFTrue[i], dF2[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dGTrue[i], dG2[i], small_tol, close_tol );
  }

  // case 2: negative nt
  if (verbose) cout << "diffusionViscous: case2" << endl;

  chi  = -5.241;
  chix = 1.2; chiy = -0.3;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  fTrue = gTrue = 0;
  pde.fluxViscous(x, y, time, q, qxS, qyS, fTrue, gTrue);

  // Compute the hand coded diffusion matrix
  kxx = kxy = kyx = kyy = 0;
  pde.diffusionViscous( x, y, time, q, qx, qy, kxx, kxy, kyx, kyy );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " -f(" << i << ").deriv(" << j << ") = " << -fTrue(i).deriv(j) << " | " << kxx(i,j) << std::endl;
      SANS_CHECK_CLOSE( -fTrue(i).deriv(j      ), kxx(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -f(" << i << ").deriv(" << pde.N+j << ") = " << -fTrue(i).deriv(pde.N+j) << " | " << kxy(i,j) << std::endl;
      SANS_CHECK_CLOSE( -fTrue(i).deriv(pde.N+j), kxy(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -g(" << i << ").deriv(" << j << ") = " << -gTrue(i).deriv(j) << " | " << kyx(i,j) << std::endl;
      SANS_CHECK_CLOSE( -gTrue(i).deriv(j      ), kyx(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -g(" << i << ").deriv(" << pde.N+j << ") = " << -gTrue(i).deriv(pde.N+j) << " | " << kyy(i,j) << std::endl;
      SANS_CHECK_CLOSE( -gTrue(i).deriv(pde.N+j), kyy(i,j), small_tol, close_tol );
    }

  dudq = 0;
  pde.jacobianMasterState(x, y, time, q, dudq);

  f = -kxx*dudq*qx - kxy*dudq*qy;
  g = -kyx*dudq*qx - kyy*dudq*qy;

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( fTrue[i].value(), f[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( gTrue[i].value(), g[i], small_tol, close_tol );
  }

  dF = 0; dG = 0;
  pde.perturbedGradFluxViscous(x, y, time, q, qx, qy, dqx, dqy, dF, dG);

  dux = dudq*dqx;
  duy = dudq*dqy;

  dFTrue = -kxx*dudq*dqx - kxy*dudq*dqy;
  dGTrue = -kyx*dudq*dqx - kyy*dudq*dqy;

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( dFTrue[i], dF[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dGTrue[i], dG[i], small_tol, close_tol );
  }

  dF2 = 0; dG2 = 0;
  pde.perturbedGradFluxViscous(x, y, time, kxx, kxy, kyx, kyy, dux, duy, dF2, dG2);

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( dFTrue[i], dF2[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dGTrue[i], dG2[i], small_tol, close_tol );
  }

  // case 3: positive nt with zero gradient
  if (verbose) cout << "diffusionViscous: case3" << endl;

  chi  = 5.241;

  nt  = chi*(muRef/rho);

  q[4]  = rho*nt;

  for (int i = 0; i < pde.N; i++)
  {
    qxS[i].value() = qx[i] = 0;
    qyS[i].value() = qy[i] = 0;
  }

  // Compute the true jacobian with Surreals
  fTrue = gTrue = 0;
  pde.fluxViscous(x, y, time, q, qxS, qyS, fTrue, gTrue);

  // Compute the hand coded diffusion matrix
  kxx = kxy = kyx = kyy = 0;
  pde.diffusionViscous( x, y, time, q, qx, qy, kxx, kxy, kyx, kyy );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " -f(" << i << ").deriv(" << j << ") = " << -fTrue(i).deriv(j) << " | " << kxx(i,j) << std::endl;
      SANS_CHECK_CLOSE( -fTrue(i).deriv(j      ), kxx(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -f(" << i << ").deriv(" << pde.N+j << ") = " << -fTrue(i).deriv(pde.N+j) << " | " << kxy(i,j) << std::endl;
      SANS_CHECK_CLOSE( -fTrue(i).deriv(pde.N+j), kxy(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -g(" << i << ").deriv(" << j << ") = " << -gTrue(i).deriv(j) << " | " << kyx(i,j) << std::endl;
      SANS_CHECK_CLOSE( -gTrue(i).deriv(j      ), kyx(i,j), small_tol, close_tol );

      if (verbose) std::cout << " -g(" << i << ").deriv(" << pde.N+j << ") = " << -gTrue(i).deriv(pde.N+j) << " | " << kyy(i,j) << std::endl;
      SANS_CHECK_CLOSE( -gTrue(i).deriv(pde.N+j), kyy(i,j), small_tol, close_tol );
    }


  dudq = 0;
  pde.jacobianMasterState(x, y, time, q, dudq);

  f = -kxx*dudq*qx - kxy*dudq*qy;
  g = -kyx*dudq*qx - kyy*dudq*qy;

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( fTrue[i].value(), f[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( gTrue[i].value(), g[i], small_tol, close_tol );
  }

  dF = 0; dG = 0;
  pde.perturbedGradFluxViscous(x, y, time, q, qx, qy, dqx, dqy, dF, dG);

  dux = dudq*dqx;
  duy = dudq*dqy;

  dFTrue = -kxx*dudq*dqx - kxy*dudq*dqy;
  dGTrue = -kyx*dudq*dqx - kyy*dudq*dqy;

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( dFTrue[i], dF[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dGTrue[i], dG[i], small_tol, close_tol );
  }

  dF2 = 0; dG2 = 0;
  pde.perturbedGradFluxViscous(x, y, time, kxx, kxy, kyx, kyy, dux, duy, dF2, dG2);

  for (int i = 0; i < 5; i++)
  {
    SANS_CHECK_CLOSE( dFTrue[i], dF2[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dGTrue[i], dG2[i], small_tol, close_tol );
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( source )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  bool verbose = false;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, time;
  Real rho, u, v, t;

  x = 0; y = 0; time = 0;   // not actually used in functions
  rho = 1.225;
  u = 6.974;
  v = -3.231;
  t = 2.8815;

  Real rhox = 0.0421, rhoy = 0.0127;
  Real ux = 0.0521, uy = -0.0124;
  Real vx = -0.0312, vy = -0.0022;
  Real tx = -0.0715, ty = -0.0111;
  Real px, py;

  px = R*(rho*tx + rhox*t);
  py = R*(rho*ty + rhoy*t);

  Real nt, ntx, nty;
  Real chi, chix, chiy;
  Real dist;

  // case 1: prod/wall dominant; S~ branch 1
  if (verbose) cout << "source: case1" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0;
  dist = 60;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  ArrayQ q;
  Real qDataPrim[5] = {rho, u, v, t, nt};
  string qNamePrim[5] = {"Density", "VelocityX", "VelocityY", "Temperature", "SANutilde"};
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );

  ArrayQ qx = {rhox, ux, vx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, py, nty};
  ArrayQ qp = 0, qpx = 0, qpy = 0;

  Real srcTrue = 0.0015950811868193507;
  ArrayQ src = 0;
  ArrayQ src_split = 0;

  pde.source( dist, x, y, time, q, qx, qy, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(4), close_tol*10 );

  pde.source( dist, x, y, time, q, qp, qx, qy, qpx, qpy, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(4), close_tol*10 );

  // case 2: prod/wall dominant; S~ branch 2
  if (verbose) cout << "source: case2" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qDataPrim[4] = nt;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );

  qx = {rhox, ux, vx, px, ntx};
  qy = {rhoy, uy, vy, py, nty};

  srcTrue = 0.08843084458519657;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, time, q, qx, qy, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(4), close_tol );

  pde.source( dist, x, y, time, q, qp, qx, qy, qpx, qpy, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(4), close_tol );

  // check accumulation
  pde.source( dist, x, y, time, q, qx, qy, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_CLOSE( 2*srcTrue, src(4), close_tol );

  pde.source( dist, x, y, time, q, qp, qx, qy, qpx, qpy, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_CLOSE( 2*srcTrue, src_split(4), close_tol );

  // case 3: prod/wall ft2 terms dominant
  if (verbose) cout << "source: case3" << endl;

  chi  = 1.241;
  chix = 0; chiy = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qDataPrim[4] = nt;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );

  qx = {rhox, ux, vx, px, ntx};
  qy = {rhoy, uy, vy, py, nty};

  srcTrue = -0.001315819430510617;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, time, q, qx, qy, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(4), close_tol );

  pde.source( dist, x, y, time, q, qp, qx, qy, qpx, qpy, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(4), close_tol );

  // case 4: nonconservative diffusion dominant
  if (verbose) cout << "source: case4" << endl;

  chi  = 5.241;
  chix = 1.963; chiy = -0.781;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qDataPrim[4] = nt;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );

  qx = {rhox, ux, vx, px, ntx};
  qy = {rhoy, uy, vy, py, nty};

  srcTrue = -2.857308070100351;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, time, q, qx, qy, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(4), close_tol );

  pde.source( dist, x, y, time, q, qp, qx, qy, qpx, qpy, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(4), close_tol );

  // case 5: negative SA; prod/wall dominant
  if (verbose) cout << "source: case5" << endl;

  chi  = -5.241;
  chix = 0; chiy = 0;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qDataPrim[4] = nt;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );

  qx = {rhox, ux, vx, px, ntx};
  qy = {rhoy, uy, vy, py, nty};

  srcTrue = -0.009933121590668479;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, time, q, qx, qy, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(4), close_tol );

  pde.source( dist, x, y, time, q, qp, qx, qy, qpx, qpy, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(4), close_tol );

  // case 6: negative SA; nonconservative diffusion dominant
  if (verbose) cout << "source: case6" << endl;

  chi  = -5.241;
  chix = 1.963; chiy = -0.781;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qDataPrim[4] = nt;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );

  qx = {rhox, ux, vx, px, ntx};
  qy = {rhoy, uy, vy, py, nty};

  srcTrue = -3.031888117778473;
  src = 0;
  src_split = 0;

  pde.source( dist, x, y, time, q, qx, qy, src );
  BOOST_CHECK_SMALL( src(0), small_tol );
  BOOST_CHECK_SMALL( src(1), small_tol );
  BOOST_CHECK_SMALL( src(2), small_tol );
  BOOST_CHECK_SMALL( src(3), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src(4), close_tol );

  pde.source( dist, x, y, time, q, qp, qx, qy, qpx, qpy, src_split );
  BOOST_CHECK_SMALL( src_split(0), small_tol );
  BOOST_CHECK_SMALL( src_split(1), small_tol );
  BOOST_CHECK_SMALL( src_split(2), small_tol );
  BOOST_CHECK_SMALL( src_split(3), small_tol );
  BOOST_CHECK_CLOSE( srcTrue, src_split(4), close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jacobianSource )
{

  // Use conservative variables here in order to differentiate with Surreal
  typedef QTypeConservative QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef SurrealS<PDEClass::N> SurrealClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;
  typedef PDEClass::ArrayQ<SurrealClass> ArrayQSurreal;

  bool verbose = false;

  const Real small_tol = 1.e-10;
  const Real close_tol = 1.e-10;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real tSuth = 110./300.; //somewhat random...
  const Real tRef  = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, time;
  Real rho, u, v, t;

  x = 0; y = 0; time = 0;   // not actually used in functions
  rho = 1.225;
  u = 6.974;
  v = -3.231;
  t = 2.8815;

  Real e = gas.energy(rho, t);
  Real E = e + 0.5*(u*u + v*v);

  Real rhox = 0.0421, rhoy = 0.0127;
  Real ux = 0.0521, uy = -0.0124;
  Real vx = -0.0312, vy = -0.0022;
  Real tx = -0.0715, ty = -0.0111;

  //Real px = R*(rho*tx + rhox*t);
  //Real py = R*(rho*ty + rhoy*t);

  Real rhoux = rhox*u + ux*rho;
  Real rhouy = rhoy*u + uy*rho;

  Real rhovx = rhox*v + vx*rho;
  Real rhovy = rhoy*v + vy*rho;

  Real ex, ey;
  gas.energyGradient(rho, t, rhox, tx, ex);
  gas.energyGradient(rho, t, rhoy, ty, ey);

  Real Ex = ex + ux*u + vx*v;
  Real Ey = ey + uy*u + vy*v;

  Real rhoEx = rhox*E + rho*Ex;
  Real rhoEy = rhoy*E + rho*Ey;

  Real nt, ntx, nty;
  Real chi, chix, chiy;
  Real dist;

  ArrayQ q, qx, qy;
  MatrixQ dsdu;
  ArrayQSurreal qS, qxS, qyS;
  ArrayQSurreal src;

  SurrealClass rhoS  = rho;
  SurrealClass rhouS = rho*u;
  SurrealClass rhovS = rho*v;
  SurrealClass rhoES = rho*E;
  SurrealClass rhontS = 0;

  rhoS.deriv(0)   = 1;
  rhouS.deriv(1)  = 1;
  rhovS.deriv(2)  = 1;
  rhoES.deriv(3)  = 1;
  rhontS.deriv(4) = 1;

  // set the complete state vectors (sans nt)
  // the nt values are set for each case
  pde.setDOFFrom( q, SAnt2D<Conservative2D<Real>>        ({rho , rho*u, rho*v, rho*E, 0     }));
  pde.setDOFFrom(qS, SAnt2D<Conservative2D<SurrealClass>>({rhoS, rhouS, rhovS, rhoES, rhontS}));

  qxS = qx = {rhox, rhoux, rhovx, rhoEx, 0};
  qyS = qy = {rhoy, rhouy, rhovy, rhoEy, 0};


  // case 1: prod/wall dominant; S~ branch 1
  if (verbose) cout << "source: case1" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0;
  dist = 60;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qS[4].value() = q[4]  = rho*nt;

  qxS[4] = qx[4] = rhox*nt + rho*ntx;
  qyS[4] = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, time, qS, qxS, qyS, src );

  // Compute the hand coded Jacobian
  dsdu = 0;
  pde.jacobianSource( dist, x, y, time, q, qx, qy, dsdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << j << ") = " << src(i).deriv(j) << " | " << dsdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(j), dsdu(i,j), small_tol, close_tol );
    }


  MatrixQ dsduh = 0;
  pde.jacobianSourceHACK( dist, x, y, time, q, qx, qy, dsduh );

  MatrixQ dudv = 0, dvdu = 0;
  pde.adjointVariableJacobian(x, y, time, q, dudv);
  pde.adjointVariableJacobianInverse(x, y, time, q, dvdu);

  MatrixQ dsduhTRUE = dvdu*dsdu*dudv;

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      SANS_CHECK_CLOSE( dsduhTRUE(i,j), dsduh(i,j), small_tol, close_tol );
    }



  // case 2: prod/wall dominant; S~ branch 2
  if (verbose) cout << "source: case2" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qS[4].value() = q[4]  = rho*nt;

  qxS[4] = qx[4] = rhox*nt + rho*ntx;
  qyS[4] = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, time, qS, qxS, qyS, src );

  // Compute the hand coded Jacobian
  dsdu = 0;
  pde.jacobianSource( dist, x, y, time, q, qx, qy, dsdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << j << ") = " << src(i).deriv(j) << " | " << dsdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(j), dsdu(i,j), small_tol, close_tol );
    }


  // case 3: prod/wall ft2 terms dominant
  if (verbose) cout << "source: case3" << endl;

  chi  = 1.241;
  chix = 0; chiy = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qS[4].value() = q[4]  = rho*nt;

  qxS[4] = qx[4] = rhox*nt + rho*ntx;
  qyS[4] = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, time, qS, qxS, qyS, src );

  // Compute the hand coded Jacobian
  dsdu = 0;
  pde.jacobianSource( dist, x, y, time, q, qx, qy, dsdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << j << ") = " << src(i).deriv(j) << " | " << dsdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(j), dsdu(i,j), small_tol, close_tol );
    }


  // case 4: nonconservative diffusion dominant
  if (verbose) cout << "source: case4" << endl;

  chi  = 5.241;
  chix = 1.963; chiy = -0.781;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qS[4].value() = q[4]  = rho*nt;

  qxS[4] = qx[4] = rhox*nt + rho*ntx;
  qyS[4] = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, time, qS, qxS, qyS, src );

  // Compute the hand coded Jacobian
  dsdu = 0;
  pde.jacobianSource( dist, x, y, time, q, qx, qy, dsdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << j << ") = " << src(i).deriv(j) << " | " << dsdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(j), dsdu(i,j), small_tol, close_tol );
    }


  // case 5: negative SA; prod/wall dominant
  if (verbose) cout << "source: case5" << endl;

  chi  = -5.241;
  chix = 0; chiy = 0;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qS[4].value() = q[4]  = rho*nt;

  qxS[4] = qx[4] = rhox*nt + rho*ntx;
  qyS[4] = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, time, qS, qxS, qyS, src );

  // Compute the hand coded Jacobian
  dsdu = 0;
  pde.jacobianSource( dist, x, y, time, q, qx, qy, dsdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << j << ") = " << src(i).deriv(j) << " | " << dsdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(j), dsdu(i,j), small_tol, close_tol );
    }


  // case 6: negative SA; nonconservative diffusion dominant
  if (verbose) cout << "source: case6" << endl;

  chi  = -5.241;
  chix = 1.963; chiy = -0.781;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qS[4].value() = q[4]  = rho*nt;

  qxS[4] = qx[4] = rhox*nt + rho*ntx;
  qyS[4] = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, time, qS, qxS, qyS, src );

  // Compute the hand coded Jacobian
  dsdu = 0;
  pde.jacobianSource( dist, x, y, time, q, qx, qy, dsdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << j << ") = " << src(i).deriv(j) << " | " << dsdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(j), dsdu(i,j), small_tol, close_tol );
    }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jacobianGradientSource )
{
  // Use conservative variables here in order to differentiate with Surreal
  typedef QTypeConservative QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef SurrealS<PhysD2::D*PDEClass::N> SurrealClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;
  typedef PDEClass::ArrayQ<SurrealClass> ArrayQSurreal;

  bool verbose = false;

  const Real small_tol = 1.e-10;
  const Real close_tol = 1.e-9;
  const Real ping_tol = 2.e-5;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real tSuth = 110/300; //somewhat random...
  const Real tRef  = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, time;
  Real rho, u, v, t;

  x = 0; y = 0; time = 0;   // not actually used in functions
  rho = 1.225;
  u = 6.974;
  v = -3.231;
  t = 2.8815;

  Real e = gas.energy(rho, t);
  Real p = gas.pressure(rho, t);
  Real E = e + 0.5*(u*u + v*v);

  Real rhox = 0.0421, rhoy = 0.0127;
  Real ux =  0.0521, uy = -0.0124;
  Real vx = -0.0312, vy = -0.0022;
  Real tx = -0.0715, ty = -0.0111;

  Real rhoux = rhox*u + ux*rho;
  Real rhouy = rhoy*u + uy*rho;

  Real rhovx = rhox*v + vx*rho;
  Real rhovy = rhoy*v + vy*rho;

  Real ex, ey;
  gas.energyGradient(rho, t, rhox, tx, ex);
  gas.energyGradient(rho, t, rhoy, ty, ey);

  Real Ex = ex + ux*u + vx*v;
  Real Ey = ey + uy*u + vy*v;

  Real rhoEx = rhox*E + rho*Ex;
  Real rhoEy = rhoy*E + rho*Ey;

  Real nt = 0.0;
  Real ntx, nty;
  Real chi, chix, chiy;
  Real dist;

  ArrayQ q, qx, qy;
  MatrixQ dsdux, dsduy;
  ArrayQSurreal qxS, qyS;
  ArrayQSurreal src;

  // set the complete state vectors (sans nt)
  // the nt values are set for each case
  pde.setDOFFrom( q, SAnt2D<Conservative2D<Real>>({rho , rho*u, rho*v, rho*E, rho*nt}));

  qxS = qx = {rhox, rhoux, rhovx, rhoEx, 0};
  qyS = qy = {rhoy, rhouy, rhovy, rhoEy, 0};


  ArrayQ qxx = {-0.001378, -0.002479, 0.004789, 0.001924, 0.0019838};
  ArrayQ qxy = {-0.00183873, -0.0028383, 0.004891, 0.004729, 0.004848};
  ArrayQ qyy = {-0.0014934, -0.005294, 0.001748, 0.0029808, 0.002974};

  for (int n = 0; n < PDEClass::N; n++)
  {
    qxS[n].deriv(0*PDEClass::N + n) = 1;
    qyS[n].deriv(1*PDEClass::N + n) = 1;
  }

  // Strong Conservative Flux (PRIMITIVE SPECIFIC)
  Real rhot = -0.9;
  Real ut = -0.25;
  Real vt = 0.14;
  Real pt = 0.23;
  Real tt = (t/p) * pt - (t/rho)*rhot;
  Real ntt = 3.14;


  Real e0 = Cv*t + 0.5*(u*u + v*v);
  Real e0t = Cv*tt + u*ut + v*vt;

  Real rhout = rhot*u + rho* ut;
  Real rhovt = rhot*v + rho* vt;
  Real rhoEt = rhot*e0 + rho*e0t;
  Real rhontt = rho*ntt + rhot*nt;

  ArrayQ qt = {rhot, rhout, rhovt, rhoEt, rhontt};
  ArrayQ utCons, utConsTrue;
  utCons = 0;
  pde.strongFluxAdvectiveTime( dist, x, y, time, q, qt, utCons );
  utConsTrue[0] = rhot;
  utConsTrue[1] = rho*ut + rhot*u;
  utConsTrue[2] = rho*vt + rhot*v;
  utConsTrue[3] = rho*e0t + rhot*e0;
  utConsTrue[4] = rho*ntt + rhot*nt;
  BOOST_CHECK_CLOSE( utConsTrue(0), utCons(0), close_tol );
  BOOST_CHECK_CLOSE( utConsTrue(1), utCons(1), close_tol );
  BOOST_CHECK_CLOSE( utConsTrue(2), utCons(2), close_tol );
  BOOST_CHECK_CLOSE( utConsTrue(3), utCons(3), close_tol );
  BOOST_CHECK_CLOSE( utConsTrue(4), utCons(4), close_tol );

  chi  = 5.241;
  chix = 0; chiy = 0;
  dist = 60;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, time, q, qxS, qyS, src );

  // Compute the hand coded Jacobian
  dsdux=0; dsduy=0;
  pde.jacobianGradientSource( dist, x, y, time, q, qx, qy, dsdux, dsduy );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << 0*pde.N+j << ") = " << src(i).deriv(0*pde.N+j) << " | " << dsdux(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(0*pde.N+j), dsdux(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 1*pde.N+j << ") = " << src(i).deriv(1*pde.N+j) << " | " << dsduy(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(1*pde.N+j), dsduy(i,j), small_tol, close_tol );

    }


  MatrixQ dsduxh = 0, dsduyh = 0;
  pde.jacobianGradientSourceHACK( dist, x, y, time, q, qx, qy, dsduxh, dsduyh );

  MatrixQ dudv = 0, dvdu = 0;
  pde.adjointVariableJacobian(x, y, time, q, dudv);
  pde.adjointVariableJacobianInverse(x, y, time, q, dvdu);

  MatrixQ dsduxhTRUE = dvdu*dsdux*dudv;
  MatrixQ dsduyhTRUE = dvdu*dsduy*dudv;

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      SANS_CHECK_CLOSE( dsduxhTRUE(i,j), dsduxh(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( dsduyhTRUE(i,j), dsduyh(i,j), small_tol, close_tol );
    }

//  Real distx = 0.25, disty = sqrt(1-0.25*0.25);
  Real distx = 0, disty= 0; //NEGLECTING DISTANCE FUNCTION GRADIENTS
  //PING TEST FOR DIV OF JACOBIANGRADIENTSOURCE
  {

    MatrixQ grad_dsdgradu = 0;
    pde.jacobianGradientSourceGradient(dist, x, y, time, q, qx, qy, qxx, qxy, qyy, grad_dsdgradu);

    //rough ping test
    Real dx = 1e-6;
    Real dy = 1e-6;

    Real dist1x = dist + distx*dx;
    Real dist0x = dist - distx*dx;

    //x derivative
    ArrayQ q1x = q + qx*dx;
    ArrayQ q0x = q - qx*dx;
    ArrayQ qx1x = qx+qxx*dx;
    ArrayQ qx0x = qx-qxx*dx;
    ArrayQ qy1x = qy+qxy*dx;
    ArrayQ qy0x = qy-qxy*dx;
    MatrixQ dsdux1x=0; MatrixQ dsduy1x=0;
    MatrixQ dsdux0x=0; MatrixQ dsduy0x=0;
    pde.jacobianGradientSource( dist1x, x, y, time, q1x, qx1x, qy1x, dsdux1x, dsduy1x );
    pde.jacobianGradientSource( dist0x, x, y, time, q0x, qx0x, qy0x, dsdux0x, dsduy0x );

    Real dist1y = dist + disty*dy;
    Real dist0y = dist - disty*dy;
    //y derivative
    ArrayQ q1y = q + qy*dy;
    ArrayQ q0y = q - qy*dy;
    ArrayQ qx1y = qx+qxy*dy;
    ArrayQ qx0y = qx-qxy*dy;
    ArrayQ qy1y = qy+qyy*dy;
    ArrayQ qy0y = qy-qyy*dy;

    MatrixQ dsdux1y=0; MatrixQ dsduy1y=0;
    MatrixQ dsdux0y=0; MatrixQ dsduy0y=0;
    pde.jacobianGradientSource( dist1y, x, y, time, q1y, qx1y, qy1y, dsdux1y, dsduy1y );
    pde.jacobianGradientSource( dist0y, x, y, time, q0y, qx0y, qy0y, dsdux0y, dsduy0y );

    MatrixQ pingmat = (dsdux1x - dsdux0x)/(2*dx) + (dsduy1y - dsduy0y)/(2*dx);

    for (int i = 0; i < pde.N; i++)
      for (int j = 0; j < pde.N; j++)
      {
        SANS_CHECK_CLOSE( pingmat(i,j), grad_dsdgradu(i,j), small_tol, ping_tol );
      }

  }

  // case 2: prod/wall dominant; S~ branch 2
  if (verbose) cout << "source: case2" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, time, q, qxS, qyS, src );

  // Compute the hand coded Jacobian
  dsdux=0; dsduy=0;
  pde.jacobianGradientSource( dist, x, y, time, q, qx, qy, dsdux, dsduy );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << 0*pde.N+j << ") = " << src(i).deriv(0*pde.N+j) << " | " << dsdux(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(0*pde.N+j), dsdux(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 1*pde.N+j << ") = " << src(i).deriv(1*pde.N+j) << " | " << dsduy(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(1*pde.N+j), dsduy(i,j), small_tol, close_tol );

    }

  //PING TEST FOR DIV OF JACOBIANGRADIENTSOURCE
  {

    MatrixQ grad_dsdgradu = 0;
    pde.jacobianGradientSourceGradient(dist, x, y, time, q, qx, qy, qxx, qxy, qyy, grad_dsdgradu);

    //rough ping test
    Real dx = 1e-6;
    Real dy = 1e-6;

    Real dist1x = dist + distx*dx;
    Real dist0x = dist - distx*dx;

    //x derivative
    ArrayQ q1x = q + qx*dx;
    ArrayQ q0x = q - qx*dx;
    ArrayQ qx1x = qx+qxx*dx;
    ArrayQ qx0x = qx-qxx*dx;
    ArrayQ qy1x = qy+qxy*dx;
    ArrayQ qy0x = qy-qxy*dx;
    MatrixQ dsdux1x=0; MatrixQ dsduy1x=0;
    MatrixQ dsdux0x=0; MatrixQ dsduy0x=0;
    pde.jacobianGradientSource( dist1x, x, y, time, q1x, qx1x, qy1x, dsdux1x, dsduy1x );
    pde.jacobianGradientSource( dist0x, x, y, time, q0x, qx0x, qy0x, dsdux0x, dsduy0x );

    Real dist1y = dist + disty*dy;
    Real dist0y = dist - disty*dy;
    //y derivative
    ArrayQ q1y = q + qy*dy;
    ArrayQ q0y = q - qy*dy;
    ArrayQ qx1y = qx+qxy*dy;
    ArrayQ qx0y = qx-qxy*dy;
    ArrayQ qy1y = qy+qyy*dy;
    ArrayQ qy0y = qy-qyy*dy;

    MatrixQ dsdux1y=0; MatrixQ dsduy1y=0;
    MatrixQ dsdux0y=0; MatrixQ dsduy0y=0;
    pde.jacobianGradientSource( dist1y, x, y, time, q1y, qx1y, qy1y, dsdux1y, dsduy1y );
    pde.jacobianGradientSource( dist0y, x, y, time, q0y, qx0y, qy0y, dsdux0y, dsduy0y );

    MatrixQ pingmat = (dsdux1x - dsdux0x)/(2*dx) + (dsduy1y - dsduy0y)/(2*dx);

    for (int i = 0; i < pde.N; i++)
      for (int j = 0; j < pde.N; j++)
      {
        SANS_CHECK_CLOSE( pingmat(i,j), grad_dsdgradu(i,j), small_tol, ping_tol );
      }
  }

  // case 3: prod/wall ft2 terms dominant
  if (verbose) cout << "source: case3" << endl;

  chi  = 1.241;
  chix = 0; chiy = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, time, q, qxS, qyS, src );

  // Compute the hand coded Jacobian
  dsdux=0; dsduy=0;
  pde.jacobianGradientSource( dist, x, y, time, q, qx, qy, dsdux, dsduy );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << 0*pde.N+j << ") = " << src(i).deriv(0*pde.N+j) << " | " << dsdux(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(0*pde.N+j), dsdux(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 1*pde.N+j << ") = " << src(i).deriv(1*pde.N+j) << " | " << dsduy(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(1*pde.N+j), dsduy(i,j), small_tol, close_tol );

    }

  //PING TEST FOR DIV OF JACOBIANGRADIENTSOURCE
  {

    MatrixQ grad_dsdgradu = 0;
    pde.jacobianGradientSourceGradient(dist, x, y, time, q, qx, qy, qxx, qxy, qyy, grad_dsdgradu);

    //rough ping test
    Real dx = 1e-6;
    Real dy = 1e-6;

    Real dist1x = dist + distx*dx;
    Real dist0x = dist - distx*dx;

    //x derivative
    ArrayQ q1x = q + qx*dx;
    ArrayQ q0x = q - qx*dx;
    ArrayQ qx1x = qx+qxx*dx;
    ArrayQ qx0x = qx-qxx*dx;
    ArrayQ qy1x = qy+qxy*dx;
    ArrayQ qy0x = qy-qxy*dx;
    MatrixQ dsdux1x=0; MatrixQ dsduy1x=0;
    MatrixQ dsdux0x=0; MatrixQ dsduy0x=0;
    pde.jacobianGradientSource( dist1x, x, y, time, q1x, qx1x, qy1x, dsdux1x, dsduy1x );
    pde.jacobianGradientSource( dist0x, x, y, time, q0x, qx0x, qy0x, dsdux0x, dsduy0x );

    Real dist1y = dist + disty*dy;
    Real dist0y = dist - disty*dy;
    //y derivative
    ArrayQ q1y = q + qy*dy;
    ArrayQ q0y = q - qy*dy;
    ArrayQ qx1y = qx+qxy*dy;
    ArrayQ qx0y = qx-qxy*dy;
    ArrayQ qy1y = qy+qyy*dy;
    ArrayQ qy0y = qy-qyy*dy;

    MatrixQ dsdux1y=0; MatrixQ dsduy1y=0;
    MatrixQ dsdux0y=0; MatrixQ dsduy0y=0;
    pde.jacobianGradientSource( dist1y, x, y, time, q1y, qx1y, qy1y, dsdux1y, dsduy1y );
    pde.jacobianGradientSource( dist0y, x, y, time, q0y, qx0y, qy0y, dsdux0y, dsduy0y );

    MatrixQ pingmat = (dsdux1x - dsdux0x)/(2*dx) + (dsduy1y - dsduy0y)/(2*dx);

    for (int i = 0; i < pde.N; i++)
      for (int j = 0; j < pde.N; j++)
      {
        SANS_CHECK_CLOSE( pingmat(i,j), grad_dsdgradu(i,j), small_tol, ping_tol );
      }

  }


  // case 4: nonconservative diffusion dominant
  if (verbose) cout << "source: case4" << endl;

  chi  = 5.241;
  chix = 1.963; chiy = -0.781;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, time, q, qxS, qyS, src );

  // Compute the hand coded Jacobian
  dsdux=0; dsduy=0;
  pde.jacobianGradientSource( dist, x, y, time, q, qx, qy, dsdux, dsduy );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << 0*pde.N+j << ") = " << src(i).deriv(0*pde.N+j) << " | " << dsdux(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(0*pde.N+j), dsdux(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 1*pde.N+j << ") = " << src(i).deriv(1*pde.N+j) << " | " << dsduy(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(1*pde.N+j), dsduy(i,j), small_tol, close_tol );
    }

  //PING TEST FOR DIV OF JACOBIANGRADIENTSOURCE
  {

    MatrixQ grad_dsdgradu = 0;
    pde.jacobianGradientSourceGradient(dist, x, y, time, q, qx, qy, qxx, qxy, qyy, grad_dsdgradu);

    //rough ping test
    Real dx = 1e-6;
    Real dy = 1e-6;

    Real dist1x = dist + distx*dx;
    Real dist0x = dist - distx*dx;

    //x derivative
    ArrayQ q1x = q + qx*dx;
    ArrayQ q0x = q - qx*dx;
    ArrayQ qx1x = qx+qxx*dx;
    ArrayQ qx0x = qx-qxx*dx;
    ArrayQ qy1x = qy+qxy*dx;
    ArrayQ qy0x = qy-qxy*dx;
    MatrixQ dsdux1x=0; MatrixQ dsduy1x=0;
    MatrixQ dsdux0x=0; MatrixQ dsduy0x=0;
    pde.jacobianGradientSource( dist1x, x, y, time, q1x, qx1x, qy1x, dsdux1x, dsduy1x );
    pde.jacobianGradientSource( dist0x, x, y, time, q0x, qx0x, qy0x, dsdux0x, dsduy0x );

    Real dist1y = dist + disty*dy;
    Real dist0y = dist - disty*dy;
    //y derivative
    ArrayQ q1y = q + qy*dy;
    ArrayQ q0y = q - qy*dy;
    ArrayQ qx1y = qx+qxy*dy;
    ArrayQ qx0y = qx-qxy*dy;
    ArrayQ qy1y = qy+qyy*dy;
    ArrayQ qy0y = qy-qyy*dy;

    MatrixQ dsdux1y=0; MatrixQ dsduy1y=0;
    MatrixQ dsdux0y=0; MatrixQ dsduy0y=0;
    pde.jacobianGradientSource( dist1y, x, y, time, q1y, qx1y, qy1y, dsdux1y, dsduy1y );
    pde.jacobianGradientSource( dist0y, x, y, time, q0y, qx0y, qy0y, dsdux0y, dsduy0y );

    MatrixQ pingmat = (dsdux1x - dsdux0x)/(2*dx) + (dsduy1y - dsduy0y)/(2*dx);

    for (int i = 0; i < pde.N; i++)
      for (int j = 0; j < pde.N; j++)
      {
        SANS_CHECK_CLOSE( pingmat(i,j), grad_dsdgradu(i,j), small_tol, ping_tol );
      }

  }


  // case 5: negative SA; prod/wall dominant
  if (verbose) cout << "source: case5" << endl;

  chi  = -5.241;
  chix = 0; chiy = 0;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, time, q, qxS, qyS, src );

  // Compute the hand coded Jacobian
  dsdux=0; dsduy=0;
  pde.jacobianGradientSource( dist, x, y, time, q, qx, qy, dsdux, dsduy );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << 0*pde.N+j << ") = " << src(i).deriv(0*pde.N+j) << " | " << dsdux(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(0*pde.N+j), dsdux(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 1*pde.N+j << ") = " << src(i).deriv(1*pde.N+j) << " | " << dsduy(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(1*pde.N+j), dsduy(i,j), small_tol, close_tol );

    }

  //PING TEST FOR DIV OF JACOBIANGRADIENTSOURCE
  {


    MatrixQ grad_dsdgradu = 0;
    pde.jacobianGradientSourceGradient(dist, x, y, time, q, qx, qy, qxx, qxy, qyy, grad_dsdgradu);

    //rough ping test
    Real dx = 1e-6;
    Real dy = 1e-6;

    Real dist1x = dist + distx*dx;
    Real dist0x = dist - distx*dx;

    //x derivative
    ArrayQ q1x = q + qx*dx;
    ArrayQ q0x = q - qx*dx;
    ArrayQ qx1x = qx+qxx*dx;
    ArrayQ qx0x = qx-qxx*dx;
    ArrayQ qy1x = qy+qxy*dx;
    ArrayQ qy0x = qy-qxy*dx;
    MatrixQ dsdux1x=0; MatrixQ dsduy1x=0;
    MatrixQ dsdux0x=0; MatrixQ dsduy0x=0;
    pde.jacobianGradientSource( dist1x, x, y, time, q1x, qx1x, qy1x, dsdux1x, dsduy1x );
    pde.jacobianGradientSource( dist0x, x, y, time, q0x, qx0x, qy0x, dsdux0x, dsduy0x );

    Real dist1y = dist + disty*dy;
    Real dist0y = dist - disty*dy;
    //y derivative
    ArrayQ q1y = q + qy*dy;
    ArrayQ q0y = q - qy*dy;
    ArrayQ qx1y = qx+qxy*dy;
    ArrayQ qx0y = qx-qxy*dy;
    ArrayQ qy1y = qy+qyy*dy;
    ArrayQ qy0y = qy-qyy*dy;

    MatrixQ dsdux1y=0; MatrixQ dsduy1y=0;
    MatrixQ dsdux0y=0; MatrixQ dsduy0y=0;
    pde.jacobianGradientSource( dist1y, x, y, time, q1y, qx1y, qy1y, dsdux1y, dsduy1y );
    pde.jacobianGradientSource( dist0y, x, y, time, q0y, qx0y, qy0y, dsdux0y, dsduy0y );

    MatrixQ pingmat = (dsdux1x - dsdux0x)/(2*dx) + (dsduy1y - dsduy0y)/(2*dx);

    for (int i = 0; i < pde.N; i++)
      for (int j = 0; j < pde.N; j++)
      {
        SANS_CHECK_CLOSE( pingmat(i,j), grad_dsdgradu(i,j), small_tol, ping_tol );
      }

  }

  // case 6: negative SA; nonconservative diffusion dominant
  if (verbose) cout << "source: case6" << endl;

  chi  = -5.241;
  chix = 1.963; chiy = -0.781;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, time, q, qxS, qyS, src );

  // Compute the hand coded Jacobian
  dsdux=0; dsduy=0;
  pde.jacobianGradientSource( dist, x, y, time, q, qx, qy, dsdux, dsduy );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << 0*pde.N+j << ") = " << src(i).deriv(0*pde.N+j) << " | " << dsdux(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(0*pde.N+j), dsdux(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 1*pde.N+j << ") = " << src(i).deriv(1*pde.N+j) << " | " << dsduy(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(1*pde.N+j), dsduy(i,j), small_tol, close_tol );

    }

  //PING TEST FOR DIV OF JACOBIANGRADIENTSOURCE
  {

    MatrixQ grad_dsdgradu = 0;
    pde.jacobianGradientSourceGradient(dist, x, y, time, q, qx, qy, qxx, qxy, qyy, grad_dsdgradu);

    //rough ping test
    Real dx = 1e-6;
    Real dy = 1e-6;

    Real dist1x = dist + distx*dx;
    Real dist0x = dist - distx*dx;

    //x derivative
    ArrayQ q1x = q + qx*dx;
    ArrayQ q0x = q - qx*dx;
    ArrayQ qx1x = qx+qxx*dx;
    ArrayQ qx0x = qx-qxx*dx;
    ArrayQ qy1x = qy+qxy*dx;
    ArrayQ qy0x = qy-qxy*dx;
    MatrixQ dsdux1x=0; MatrixQ dsduy1x=0;
    MatrixQ dsdux0x=0; MatrixQ dsduy0x=0;
    pde.jacobianGradientSource( dist1x, x, y, time, q1x, qx1x, qy1x, dsdux1x, dsduy1x );
    pde.jacobianGradientSource( dist0x, x, y, time, q0x, qx0x, qy0x, dsdux0x, dsduy0x );

    Real dist1y = dist + disty*dy;
    Real dist0y = dist - disty*dy;
    //y derivative
    ArrayQ q1y = q + qy*dy;
    ArrayQ q0y = q - qy*dy;
    ArrayQ qx1y = qx+qxy*dy;
    ArrayQ qx0y = qx-qxy*dy;
    ArrayQ qy1y = qy+qyy*dy;
    ArrayQ qy0y = qy-qyy*dy;

    MatrixQ dsdux1y=0; MatrixQ dsduy1y=0;
    MatrixQ dsdux0y=0; MatrixQ dsduy0y=0;
    pde.jacobianGradientSource( dist1y, x, y, time, q1y, qx1y, qy1y, dsdux1y, dsduy1y );
    pde.jacobianGradientSource( dist0y, x, y, time, q0y, qx0y, qy0y, dsdux0y, dsduy0y );

    MatrixQ pingmat = (dsdux1x - dsdux0x)/(2*dx) + (dsduy1y - dsduy0y)/(2*dx);

    for (int i = 0; i < pde.N; i++)
      for (int j = 0; j < pde.N; j++)
      {
        SANS_CHECK_CLOSE( pingmat(i,j), grad_dsdgradu(i,j), small_tol, ping_tol );
      }

  }

  // case 7: negative SA; shrmod < -cv2_*shr0
  if (verbose) cout << "source: case7" << endl;

  chi  = 10.241;
  chix = 1.963; chiy = -0.781;
  dist = 0.1;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, time, q, qxS, qyS, src );

  // Compute the hand coded Jacobian
  dsdux=0; dsduy=0;
  pde.jacobianGradientSource( dist, x, y, time, q, qx, qy, dsdux, dsduy );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << 0*pde.N+j << ") = " << src(i).deriv(0*pde.N+j) << " | " << dsdux(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(0*pde.N+j), dsdux(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 1*pde.N+j << ") = " << src(i).deriv(1*pde.N+j) << " | " << dsduy(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(1*pde.N+j), dsduy(i,j), small_tol, close_tol );

    }

  //PING TEST FOR DIV OF JACOBIANGRADIENTSOURCE
  {
    MatrixQ grad_dsdgradu = 0;
//    pde.jacobianGradientSourceGradient(dist, distx, disty, x, y, time, q, qx, qy, qxx, qxy, qyy, grad_dsdgradu);
    pde.jacobianGradientSourceGradient(dist, x, y, time, q, qx, qy, qxx, qxy, qyy, grad_dsdgradu);

    //rough ping test
    Real dx = 1e-6;
    Real dy = 1e-6;

    Real dist1x = dist + distx*dx;
    Real dist0x = dist - distx*dx;

    //x derivative
    ArrayQ q1x = q + qx*dx;
    ArrayQ q0x = q - qx*dx;
    ArrayQ qx1x = qx+qxx*dx;
    ArrayQ qx0x = qx-qxx*dx;
    ArrayQ qy1x = qy+qxy*dx;
    ArrayQ qy0x = qy-qxy*dx;
    MatrixQ dsdux1x=0; MatrixQ dsduy1x=0;
    MatrixQ dsdux0x=0; MatrixQ dsduy0x=0;
    pde.jacobianGradientSource( dist1x, x, y, time, q1x, qx1x, qy1x, dsdux1x, dsduy1x );
    pde.jacobianGradientSource( dist0x, x, y, time, q0x, qx0x, qy0x, dsdux0x, dsduy0x );

    Real dist1y = dist + disty*dy;
    Real dist0y = dist - disty*dy;
    //y derivative
    ArrayQ q1y = q + qy*dy;
    ArrayQ q0y = q - qy*dy;
    ArrayQ qx1y = qx+qxy*dy;
    ArrayQ qx0y = qx-qxy*dy;
    ArrayQ qy1y = qy+qyy*dy;
    ArrayQ qy0y = qy-qyy*dy;

    MatrixQ dsdux1y=0; MatrixQ dsduy1y=0;
    MatrixQ dsdux0y=0; MatrixQ dsduy0y=0;
    pde.jacobianGradientSource( dist1y, x, y, time, q1y, qx1y, qy1y, dsdux1y, dsduy1y );
    pde.jacobianGradientSource( dist0y, x, y, time, q0y, qx0y, qy0y, dsdux0y, dsduy0y );

    MatrixQ pingmat = (dsdux1x - dsdux0x)/(2*dx) + (dsduy1y - dsduy0y)/(2*dx);

    for (int i = 0; i < pde.N; i++)
      for (int j = 0; j < pde.N; j++)
      {
        SANS_CHECK_CLOSE( pingmat(i,j), grad_dsdgradu(i,j), small_tol, ping_tol );
      }

  }


  // case 8: shr0 == 0
  if (verbose) cout << "source: case7" << endl;

  chi  = 5.241;
  chix = 1.963; chiy = -0.781;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;

  qxS[1].value() = qx[1] = 0;
  qyS[1].value() = qy[1] = 0;

  qxS[2].value() = qx[2] = 0;
  qyS[2].value() = qy[2] = 0;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  src = 0;
  pde.source( dist, x, y, time, q, qxS, qyS, src );

  // Compute the hand coded Jacobian
  dsdux=0; dsduy=0;
  pde.jacobianGradientSource( dist, x, y, time, q, qx, qy, dsdux, dsduy );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " src(" << i << ").deriv(" << 0*pde.N+j << ") = " << src(i).deriv(0*pde.N+j) << " | " << dsdux(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(0*pde.N+j), dsdux(i,j), small_tol, close_tol );

      if (verbose) std::cout << " src(" << i << ").deriv(" << 1*pde.N+j << ") = " << src(i).deriv(1*pde.N+j) << " | " << dsduy(i,j) << std::endl;
      SANS_CHECK_CLOSE( src(i).deriv(1*pde.N+j), dsduy(i,j), small_tol, close_tol );
    }

  //DON'T EXPECT THE GRADIENT OF THE JACOBIAN TO BE CORRECT AT ZERO SHEAR
//  {
//
//    MatrixQ grad_dsdgradu = 0;
//    pde.jacobianGradientSourceGradient(dist, x, y, time, q, qx, qy, qxx, qxy, qyy, grad_dsdgradu);
//
//    //rough ping test
//    Real dx = 1e-6;
//    Real dy = 1e-6;
//
//    //x derivative
//    ArrayQ q1x = q + qx*dx;
//    ArrayQ q0x = q - qx*dx;
//    ArrayQ qx1x = qx+qxx*dx;
//    ArrayQ qx0x = qx-qxx*dx;
//    ArrayQ qy1x = qy+qxy*dx;
//    ArrayQ qy0x = qy-qxy*dx;
//    MatrixQ dsdux1x=0; MatrixQ dsduy1x=0;
//    MatrixQ dsdux0x=0; MatrixQ dsduy0x=0;
//    pde.jacobianGradientSource( dist, x, y, time, q1x, qx1x, qy1x, dsdux1x, dsduy1x );
//    pde.jacobianGradientSource( dist, x, y, time, q0x, qx0x, qy0x, dsdux0x, dsduy0x );
//
//    //y derivative
//    ArrayQ q1y = q + qy*dy;
//    ArrayQ q0y = q - qy*dy;
//    ArrayQ qx1y = qx+qxy*dy;
//    ArrayQ qx0y = qx-qxy*dy;
//    ArrayQ qy1y = qy+qyy*dy;
//    ArrayQ qy0y = qy-qyy*dy;
//
//    MatrixQ dsdux1y=0; MatrixQ dsduy1y=0;
//    MatrixQ dsdux0y=0; MatrixQ dsduy0y=0;
//    pde.jacobianGradientSource( dist, x, y, time, q1y, qx1y, qy1y, dsdux1y, dsduy1y );
//    pde.jacobianGradientSource( dist, x, y, time, q0y, qx0y, qy0y, dsdux0y, dsduy0y );
//
//    MatrixQ pingmat = (dsdux1x - dsdux0x)/(2*dx) + (dsduy1y - dsduy0y)/(2*dx);
//
//    for (int i = 0; i < pde.N; i++)
//      for (int j = 0; j < pde.N; j++)
//      {
//        SANS_CHECK_CLOSE( pingmat(i,j), grad_dsdgradu(i,j), small_tol, ping_tol );
//      }
//
//  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( perturbedSource )
{

  // Use conservative variables here in order to differentiate with Surreal
  typedef QTypeConservative QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  bool verbose = false;

  const Real small_tol = 1.e-10;
  const Real close_tol = 1.e-10;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real tSuth = 110./300.; //somewhat random...
  const Real tRef  = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, time;
  Real rho, u, v, t;

  x = 0; y = 0; time = 0;   // not actually used in functions
  rho = 1.225;
  u = 6.974;
  v = -3.231;
  t = 2.8815;

  Real e = gas.energy(rho, t);
  Real E = e + 0.5*(u*u + v*v);

  Real rhox = 0.0421, rhoy = 0.0127;
  Real ux = 0.0521, uy = -0.0124;
  Real vx = -0.0312, vy = -0.0022;
  Real tx = -0.0715, ty = -0.0111;

  //Real px = R*(rho*tx + rhox*t);
  //Real py = R*(rho*ty + rhoy*t);

  Real rhoux = rhox*u + ux*rho;
  Real rhouy = rhoy*u + uy*rho;

  Real rhovx = rhox*v + vx*rho;
  Real rhovy = rhoy*v + vy*rho;

  Real ex, ey;
  gas.energyGradient(rho, t, rhox, tx, ex);
  gas.energyGradient(rho, t, rhoy, ty, ey);

  Real Ex = ex + ux*u + vx*v;
  Real Ey = ey + uy*u + vy*v;

  Real rhoEx = rhox*E + rho*Ex;
  Real rhoEy = rhoy*E + rho*Ey;

  Real nt, ntx, nty;
  Real chi, chix, chiy;
  Real dist;

  ArrayQ q, qx, qy, dU, dUx, dUy, qp, qpx, qpy, dS, dStrue;
  MatrixQ dsdu, dsdux, dsduy;

  MatrixQ dudq = 0;

  // set the complete state vectors (sans nt)
  // the nt values are set for each case
  pde.setDOFFrom( q, SAnt2D<Conservative2D<Real>>({rho , rho*u, rho*v, rho*E, 0}));

  qx = {rhox, rhoux, rhovx, rhoEx, 0};
  qy = {rhoy, rhouy, rhovy, rhoEy, 0};

  // perturbed state
  qp  = { 0.01, -0.3,  0.1, 0.5, 0.002};
  qpx = {  0.2,  0.1, -0.9, 0.4, -0.03};
  qpy = {-0.01, -0.6,  0.2, 0.7, 0.005};

  // case 1: prod/wall dominant; S~ branch 1
  if (verbose) cout << "source: case1" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0;
  dist = 60;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;
  qx[4] = rhox*nt + rho*ntx;
  qy[4] = rhoy*nt + rho*nty;

  dudq = 0;
  pde.jacobianMasterState(dist, x, y, q, dudq);

  dU = dudq*qp;
  dUx = dudq*qpx;
  dUy = dudq*qpy;

  dsdu = 0;
  pde.jacobianSource( dist, x, y, time, q, qx, qy, dsdu );

  dsdux = dsduy = 0;
  pde.jacobianGradientSource( dist, x, y, time, q, qx, qy, dsdux, dsduy );

  dStrue = dsdu*dU + dsdux*dUx + dsduy*dUy;

  dS = 0;
  pde.perturbedSource( dist, x, y, time, q, qx, qy, dU, dUx, dUy, dS );

  for (int i = 0; i < pde.N; i++)
  {
    if (verbose) std::cout << " dStrue(" << i << ") = " << dStrue(i) << " | " << dS(i) << std::endl;
    SANS_CHECK_CLOSE( dStrue(i), dS(i), small_tol, close_tol );
  }

  // case 2: prod/wall dominant; S~ branch 2
  if (verbose) cout << "source: case2" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;
  qx[4] = rhox*nt + rho*ntx;
  qy[4] = rhoy*nt + rho*nty;

  dudq = 0;
  pde.jacobianMasterState(dist, x, y, q, dudq);

  dU = dudq*qp;
  dUx = dudq*qpx;
  dUy = dudq*qpy;

  dsdu = 0;
  pde.jacobianSource( dist, x, y, time, q, qx, qy, dsdu );

  dsdux = dsduy = 0;
  pde.jacobianGradientSource( dist, x, y, time, q, qx, qy, dsdux, dsduy );

  dStrue = dsdu*dU + dsdux*dUx + dsduy*dUy;

  dS = 0;
  pde.perturbedSource( dist, x, y, time, q, qx, qy, dU, dUx, dUy, dS );

  for (int i = 0; i < pde.N; i++)
  {
    if (verbose) std::cout << " dStrue(" << i << ") = " << dStrue(i) << " | " << dS(i) << std::endl;
    SANS_CHECK_CLOSE( dStrue(i), dS(i), small_tol, close_tol );
  }


  // case 3: prod/wall ft2 terms dominant
  if (verbose) cout << "source: case3" << endl;

  chi  = 1.241;
  chix = 0; chiy = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;
  qx[4] = rhox*nt + rho*ntx;
  qy[4] = rhoy*nt + rho*nty;

  dudq = 0;
  pde.jacobianMasterState(dist, x, y, q, dudq);

  dU = dudq*qp;
  dUx = dudq*qpx;
  dUy = dudq*qpy;

  dsdu = 0;
  pde.jacobianSource( dist, x, y, time, q, qx, qy, dsdu );

  dsdux = dsduy = 0;
  pde.jacobianGradientSource( dist, x, y, time, q, qx, qy, dsdux, dsduy );

  dStrue = dsdu*dU + dsdux*dUx + dsduy*dUy;

  dS = 0;
  pde.perturbedSource( dist, x, y, time, q, qx, qy, dU, dUx, dUy, dS );

  for (int i = 0; i < pde.N; i++)
  {
    if (verbose) std::cout << " dStrue(" << i << ") = " << dStrue(i) << " | " << dS(i) << std::endl;
    SANS_CHECK_CLOSE( dStrue(i), dS(i), small_tol, close_tol );
  }


  // case 4: nonconservative diffusion dominant
  if (verbose) cout << "source: case4" << endl;

  chi  = 5.241;
  chix = 1.963; chiy = -0.781;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;
  qx[4] = rhox*nt + rho*ntx;
  qy[4] = rhoy*nt + rho*nty;

  dudq = 0;
  pde.jacobianMasterState(dist, x, y, q, dudq);

  dU = dudq*qp;
  dUx = dudq*qpx;
  dUy = dudq*qpy;

  dsdu = 0;
  pde.jacobianSource( dist, x, y, time, q, qx, qy, dsdu );

  dsdux = dsduy = 0;
  pde.jacobianGradientSource( dist, x, y, time, q, qx, qy, dsdux, dsduy );

  dStrue = dsdu*dU + dsdux*dUx + dsduy*dUy;

  dS = 0;
  pde.perturbedSource( dist, x, y, time, q, qx, qy, dU, dUx, dUy, dS );

  for (int i = 0; i < pde.N; i++)
  {
    if (verbose) std::cout << " dStrue(" << i << ") = " << dStrue(i) << " | " << dS(i) << std::endl;
    SANS_CHECK_CLOSE( dStrue(i), dS(i), small_tol, close_tol );
  }


  // case 5: negative SA; prod/wall dominant
  if (verbose) cout << "source: case5" << endl;

  chi  = -5.241;
  chix = 0; chiy = 0;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;
  qx[4] = rhox*nt + rho*ntx;
  qy[4] = rhoy*nt + rho*nty;

  dudq = 0;
  pde.jacobianMasterState(dist, x, y, q, dudq);

  dU = dudq*qp;
  dUx = dudq*qpx;
  dUy = dudq*qpy;

  dsdu = 0;
  pde.jacobianSource( dist, x, y, time, q, qx, qy, dsdu );

  dsdux = dsduy = 0;
  pde.jacobianGradientSource( dist, x, y, time, q, qx, qy, dsdux, dsduy );

  dStrue = dsdu*dU + dsdux*dUx + dsduy*dUy;

  dS = 0;
  pde.perturbedSource( dist, x, y, time, q, qx, qy, dU, dUx, dUy, dS );

  for (int i = 0; i < pde.N; i++)
  {
    if (verbose) std::cout << " dStrue(" << i << ") = " << dStrue(i) << " | " << dS(i) << std::endl;
    SANS_CHECK_CLOSE( dStrue(i), dS(i), small_tol, close_tol );
  }


  // case 6: negative SA; nonconservative diffusion dominant
  if (verbose) cout << "source: case6" << endl;

  chi  = -5.241;
  chix = 1.963; chiy = -0.781;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;
  qx[4] = rhox*nt + rho*ntx;
  qy[4] = rhoy*nt + rho*nty;

  dudq = 0;
  pde.jacobianMasterState(dist, x, y, q, dudq);

  dU = dudq*qp;
  dUx = dudq*qpx;
  dUy = dudq*qpy;

  // Compute the hand coded Jacobian
  dsdu = 0;
  pde.jacobianSource( dist, x, y, time, q, qx, qy, dsdu );

  dsdux = dsduy = 0;
  pde.jacobianGradientSource( dist, x, y, time, q, qx, qy, dsdux, dsduy );

  dStrue = dsdu*dU + dsdux*dUx + dsduy*dUy;

  dS = 0;
  pde.perturbedSource( dist, x, y, time, q, qx, qy, dU, dUx, dUy, dS );

  for (int i = 0; i < pde.N; i++)
  {
    if (verbose) std::cout << " dStrue(" << i << ") = " << dStrue(i) << " | " << dS(i) << std::endl;
    SANS_CHECK_CLOSE( dStrue(i), dS(i), small_tol, close_tol );
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( StrongFlux )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real close_tol = 1.e-10;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real tSuth = 110./300.; //somewhat random...
  const Real tRef  = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, time;
  Real rho, u, v, p, nt;
  Real rhox, ux, vx, px, ntx;
  Real rhoy, uy, vy, py, nty;
  Real rhoxx, uxx, vxx, pxx, ntxx;
  Real rhoxy, uxy, vxy, pxy, ntxy;
  Real rhoyy, uyy, vyy, pyy, ntyy;

  x = 0.73; y = 36./99.; time = 0;   // not actually used in functions

  rho = 1.0074365505202103681442524417731;
  u = 0.10508967589710564917176487424421;
  v = 0.024335124272671981161838412026241;
  p = 1.0005144026446280991735537190083;
  nt = 5.0876529752066115702479338842975;

  rhox = 0.010748222028549962434259954921112;
  ux = 0.016369696969696969696969696969697;
  vx = 0.013539393939393939393939393939394;
  px = 0.0021139834710743801652892561983471;
  ntx = 0.20391735537190082644628099173554;

  rhoy = 0.0011265417917355371900826446280992;
  uy = 0.003044338055883510428964974419520;
  vy = 0.018728366999391792780222532288648;
  py = 0.0028292145454545454545454545454545;
  nty = 0.18899636363636363636363636363636;

  rhoxx = 0.0030748850488354620586025544703231;
  uxx = 0.022424242424242424242424242424242;
  vxx = 0.0048484848484848484848484848484848;
  pxx = 0.0057917355371900826446280991735537;
  ntxx = 0.27933884297520661157024793388430;

  rhoxy = 0.0061728317355371900826446280991736;
  uxy = 0.0048666666666666666666666666666667;
  vxy = 0.0097333333333333333333333333333333;
  pxy = 0.011626909090909090909090909090909;
  ntxy = 0.31854545454545454545454545454545;

  rhoyy = 0.0061959798545454545454545454545455;
  uyy = 0.030545454545454545454545454545455;
  vyy = -0.0068004722550177095631641086186541;
  pyy = 0.0077803400000000000000000000000000;
  ntyy = 0.5197400000000000000000000000000;

  Real dist = 60;

  // set
  ArrayQ q;
  pde.setDOFFrom( q, SAnt2D<DensityVelocityPressure2D<Real>>({rho, u, v, p, nt }) );

  ArrayQ qx = {rhox, ux, vx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, py, nty};
  ArrayQ qxx = {rhoxx, uxx, vxx, pxx, ntxx};
  ArrayQ qxy = {rhoxy, uxy, vxy, pxy, ntxy};
  ArrayQ qyy = {rhoyy, uyy, vyy, pyy, ntyy};

  ArrayQ Vx = 0, Vy = 0;
  ArrayQ VxTrue = 0, VyTrue = 0;

  VxTrue[0] = -1.007515966802465234683732693122;
  VxTrue[1] =  0.01738831876391961693945354071885;
  VxTrue[2] =  0.01384271894326165714695068342078;
  VxTrue[3] = -0.00861518110288027304467156321286;
  VxTrue[4] =  0.20391735537190082644628099173554;

  VyTrue[0] = -0.965474557812269355722042663777;
  VyTrue[1] =  0.00288450326224433526322168322838;
  VyTrue[2] =  0.01881605132720310581660691085446;
  VyTrue[3] =  0.00172136145328438821184671869356;
  VyTrue[4] =  0.1889963636363636363636363636363;

  pde.adjointStateGradient(x,y,time,q, qx,qy, Vx,Vy);

  for (int i = 0; i < pde.N; i++)
  {
    BOOST_CHECK_CLOSE( VxTrue(i), Vx(i), close_tol );
    BOOST_CHECK_CLOSE( VyTrue(i), Vy(i), close_tol );
  }


  ArrayQ Vxx = 0, Vxy = 0, Vyy =0;
  ArrayQ VxxTrue = 0, VxyTrue = 0, VyyTrue = 0;

  VxxTrue[0] = -1.4699029936109695129390418922161;
  VxxTrue[1] =  0.02256804146634102169844825845111;
  VxxTrue[2] =  0.00504737671517475066591427601033;
  VxxTrue[3] =  0.00279190966094791143931392330541;
  VxxTrue[4] =  0.2793388429752066115702479338843;

  VxyTrue[0] = -1.667851914716723313367006260941;
  VxyTrue[1] =  0.00431488675779504174585423188804;
  VxyTrue[2] =  0.00980359804665719780912072801758;
  VxyTrue[3] =  0.00555239825763835616923378590762;
  VxyTrue[4] =  0.3185454545454545454545454545455;

  VyyTrue[0] = -2.681291722886156953501350904607;
  VyyTrue[1] =  0.03057525999402143057665438499304;
  VyyTrue[2] = -0.00695160663744885222473264493321;
  VyyTrue[3] =  0.00162761167021932648144241415658;
  VyyTrue[4] =  0.519740000000000000000000000000;

  pde.adjointStateHessian(x,y,time,q, qx,qy, qxx, qxy, qyy, Vxx,Vxy, Vyy);
  for (int i=0; i< 5; i++)
  {
    BOOST_CHECK_CLOSE( VxxTrue(i), Vxx(i), close_tol );
    BOOST_CHECK_CLOSE( VxyTrue(i), Vxy(i), close_tol );
    BOOST_CHECK_CLOSE( VyyTrue(i), Vyy(i), close_tol );
  }


  // Strong Conservative Flux (PRIMITIVE SPECIFIC)
  Real rhot = -0.9;
  Real ut = -0.25;
  Real vt = 0.14;
  Real pt = 0.23;
  Real t = p/(rho*R);
  Real tt = (t/p) * pt - (t/rho)*rhot;
  Real ntt = 3.14;

  Real e0t = Cv*tt + u*ut + v*vt;
  Real e0 = Cv*t + 0.5*(u*u + v*v);

#if 0
  Real mu = visc.viscosity(t);
  Real k = tcond.conductivity(mu);
  std::cout << std::setprecision(15) << __func__ << ": mu = " << mu << "  k = " << k << std::endl;
#endif


  ArrayQ qt = {rhot, ut, vt, pt, ntt};
  ArrayQ utCons, utConsTrue;
  utCons = 0;
  pde.strongFluxAdvectiveTime(dist, x, y, time, q, qt, utCons );
  utConsTrue[0] = rhot;
  utConsTrue[1] = rho*ut + rhot*u;
  utConsTrue[2] = rho*vt + rhot*v;
  utConsTrue[3] = rho*e0t + rhot*e0;
  utConsTrue[4] = rho*ntt + rhot*nt;
  for (int i = 0; i < pde.N; i++)
    BOOST_CHECK_CLOSE( utConsTrue(i), utCons(i), close_tol );

  ArrayQ Fadv = 0; ArrayQ FadvTrue;
  ArrayQ Gadv = 0; ArrayQ GadvTrue;
  pde.fluxAdvective(x, y, time, q, Fadv, Gadv);

  FadvTrue[0] = 0.10587118058106700917370666456360;
  FadvTrue[1] = 1.0116403706987363765045998373284;
  FadvTrue[2] = 0.0025763883363347622775335854515046;
  FadvTrue[3] = 0.3686190306186793850142013748494;
  FadvTrue[4] = 0.5386358268719020087608178238215;

  GadvTrue[0] = 0.024516093653741303940070306229868;
  GadvTrue[1] = 0.0025763883363347622775335854515046;
  GadvTrue[2] = 1.0011110048303723586957718335520;
  GadvTrue[3] = 0.08535938323913415860950277947394;
  GadvTrue[4] = 0.12472937681790087347722281270783;
  for (int i = 0; i < pde.N; i++)
  {
    BOOST_CHECK_CLOSE(Fadv(i), FadvTrue(i), close_tol);
    BOOST_CHECK_CLOSE(Gadv(i), GadvTrue(i), close_tol);
  }

  pde.fluxAdvective(x, y, time, q, Fadv, Gadv);
  for (int i = 0; i < pde.N; i++)
  {
    BOOST_CHECK_CLOSE(Fadv(i), 2*FadvTrue(i), close_tol);
    BOOST_CHECK_CLOSE(Gadv(i), 2*GadvTrue(i), close_tol);
  }

  ArrayQ strongAdv = 0;
  ArrayQ strongAdvTrue = 0;
  pde.strongFluxAdvective(dist, x, y, time, q, qx, qy, strongAdv);

  strongAdvTrue(0) = 0.03651601419890715612108675424023;
  strongAdvTrue(1) = 0.007759153989119463332939283278032;
  strongAdvTrue(2) = 0.005610414308982112887949627535613;
  strongAdvTrue(3) = 0.1243734188228572701745553762010;
  strongAdvTrue(4) = 0.21200323198707463105895095938310;

  for (int i = 0; i < pde.N; i++)
    BOOST_CHECK_CLOSE(strongAdvTrue(i), strongAdv(i), close_tol);

  pde.strongFluxAdvective(dist, x, y, time, q, qx, qy, strongAdv);
  for (int i = 0; i < pde.N; i++)
    BOOST_CHECK_CLOSE(2*strongAdvTrue(i), strongAdv(i), close_tol);

  ArrayQ FviscTrue, GviscTrue;
  ArrayQ Fvisc = 0; ArrayQ Gvisc = 0;

  FviscTrue(0) = 0;
  FviscTrue(1) = -0.0225306858220465230739517769814;
  FviscTrue(2) = -0.04000165622505323194410409329417;
  FviscTrue(3) = 0.0845833783703484013428985020496;
  FviscTrue(4) = -1.872022413362562239862325433058;

  GviscTrue(0) = 0;
  GviscTrue(1) = -0.04000165622505323194410409329417;
  GviscTrue(2) = -0.0339093920991446656273492501600;
  GviscTrue(3) = -0.02259677129049221380087236976276;
  GviscTrue(4) = -1.7350432390908063684860979535301;

#ifdef SANS_SA_QCR
  Real cv1 = 7.1;
  Real cr1 = 0.3;
  Real mu = visc.viscosity( t );

  Real chi  = rho*nt/mu;
  Real chi3 = power<3>(chi);

  Real fv1 = chi3 / (chi3 + power<3>(cv1));
  Real muEddy = rho*nt*fv1;
  Real lambdaEddy = -2./3. * muEddy;

  Real tauRxx = muEddy*(2*ux   ) + lambdaEddy*(ux + vy);
  Real tauRxy = muEddy*(uy + vx);
  Real tauRyy = muEddy*(2*vy   ) + lambdaEddy*(ux + vy);

  Real magGradV = sqrt(ux*ux + uy*uy + vx*vx + vy*vy);

  Real Wxy = (uy - vx);

  Real Oxy = Wxy/magGradV;
  Real Oyx = -Oxy;

  Real qcrxx = cr1*(2*Oxy*tauRxy);
  Real qcrxy = cr1*(Oxy*tauRyy + Oyx*tauRxx);
  Real qcryy = cr1*(2*Oyx*tauRxy);

  FviscTrue(pde.ixMom) += qcrxx;
  FviscTrue(pde.iyMom) += qcrxy;
  FviscTrue(pde.iEngy) += u*qcrxx + v*qcrxy;

  GviscTrue(pde.ixMom) += qcrxy;
  GviscTrue(pde.iyMom) += qcryy;
  GviscTrue(pde.iEngy) += u*qcrxy + v*qcryy;
#endif

  pde.fluxViscous(x, y, time, q, qx, qy, Fvisc, Gvisc);

  for (int i = 0; i < pde.N; i++)
  {
    BOOST_CHECK_CLOSE(Fvisc(i), FviscTrue(i), close_tol);
    BOOST_CHECK_CLOSE(Gvisc(i), GviscTrue(i), close_tol);
  }


  pde.fluxViscous(x, y, time, q, qx, qy, Fvisc, Gvisc);

  for (int i = 0; i < pde.N; i++)
  {
    BOOST_CHECK_CLOSE(Fvisc(i), 2*FviscTrue(i), close_tol);
    BOOST_CHECK_CLOSE(Gvisc(i), 2*GviscTrue(i), close_tol);
  }

  ArrayQ strongVisc = 0;
  ArrayQ strongViscTrue;

  strongViscTrue(0) = 0;
  strongViscTrue(1) = -0.1586885687759146272058388151894;
  strongViscTrue(2) = -0.0001249081136479725918195084228;
  strongViscTrue(3) = -0.0582007392120158256116833654119;
  strongViscTrue(4) = -7.469316914316503527742653726732;

#ifdef SANS_SA_QCR
  Real mu_t = 0;
  visc.viscosityJacobian( t, mu_t );

  Real tx = (t/p)*px - (t/rho)*(rhox);
  Real ty = (t/p)*py - (t/rho)*(rhoy);

  Real mux = mu_t*tx;
  Real muy = mu_t*ty;

  Real chix = rhox*nt/mu + rho*ntx/mu - rho*nt*mux/(mu*mu);
  Real chiy = rhoy*nt/mu + rho*nty/mu - rho*nt*muy/(mu*mu);

  Real cv13 = power<3>(cv1);
  Real fv1_chi = 3*cv13*chi*chi / power<2>(cv13 + chi3);

  Real fv1x = fv1_chi*chix;
  Real fv1y = fv1_chi*chiy;

  Real muEddy_x = rhox*nt*fv1 + rho*ntx*fv1 + rho*nt*fv1x;
  Real muEddy_y = rhoy*nt*fv1 + rho*nty*fv1 + rho*nt*fv1y;

  Real lambdaEddy_x = -2./3. * muEddy_x;
  Real lambdaEddy_y = -2./3. * muEddy_y;

  //   tauRxx   = muEddy*(2*ux   ) + lambdaEddy*(ux + vy);
  Real tauRxx_x = muEddy_x*(2*ux   ) + muEddy*(2*uxx   ) + lambdaEddy_x*(ux + vy) + lambdaEddy*(uxx + vxy);
  Real tauRxx_y = muEddy_y*(2*ux   ) + muEddy*(2*uxy   ) + lambdaEddy_y*(ux + vy) + lambdaEddy*(uxy + vyy);

  //   tauRxy   = muEddy*(uy + vx);
  Real tauRxy_x = muEddy_x*(uy + vx) + muEddy*(uxy + vxx);
  Real tauRxy_y = muEddy_y*(uy + vx) + muEddy*(uyy + vxy);

  //   tauRyy   = muEddy*(2*vy   ) + lambdaEddy*(ux + vy);
  Real tauRyy_x = muEddy_x*(2*vy   ) + muEddy*(2*vxy   ) + lambdaEddy_x*(ux + vy) + lambdaEddy*(uxx + vxy);
  Real tauRyy_y = muEddy_y*(2*vy   ) + muEddy*(2*vyy   ) + lambdaEddy_y*(ux + vy) + lambdaEddy*(uxy + vyy);

  Real magGradV_x = (ux*uxx + uy*uxy + vx*vxx + vy*vxy)/magGradV;
  Real magGradV_y = (ux*uxy + uy*uyy + vx*vxy + vy*vyy)/magGradV;
  Real magGradV2  = magGradV*magGradV;

  Real Wxy_x = (uxy - vxx);
  Real Wxy_y = (uyy - vxy);

  //   Oxy   = Wxy/magGradV
  Real Oxy_x = Wxy_x/magGradV - Wxy*magGradV_x/magGradV2;
  Real Oxy_y = Wxy_y/magGradV - Wxy*magGradV_y/magGradV2;

  //   Oyx   = -Oxy
  Real Oyx_x = -Oxy_x;
  Real Oyx_y = -Oxy_y;

  //   qcrxx   = cr1*2*(Oxy*tauRxy);
  Real qcrxx_x = cr1*2*(Oxy_x*tauRxy + Oxy*tauRxy_x);

  //   qcrxy   = cr1*(Oxy*tauRyy + Oyx*tauRxx);
  Real qcrxy_x = cr1*(Oxy_x*tauRyy + Oxy*tauRyy_x + Oyx_x*tauRxx + Oyx*tauRxx_x);
  Real qcrxy_y = cr1*(Oxy_y*tauRyy + Oxy*tauRyy_y + Oyx_y*tauRxx + Oyx*tauRxx_y);

  //   qcryy   = cr1*2*(Oyx*tauRxy);
  Real qcryy_y = cr1*2*(Oyx_y*tauRxy + Oyx*tauRxy_y);

  strongViscTrue(pde.ixMom) += qcrxx_x;
  strongViscTrue(pde.iyMom) += qcrxy_x;
  strongViscTrue(pde.iEngy) += ux*qcrxx + u*qcrxx_x + vx*qcrxy + v*qcrxy_x;

  strongViscTrue(pde.ixMom) += qcrxy_y;
  strongViscTrue(pde.iyMom) += qcryy_y;
  strongViscTrue(pde.iEngy) += uy*qcrxy + u*qcrxy_y + vy*qcryy + v*qcryy_y;
#endif

  pde.strongFluxViscous(dist, x, y, time, q, qx, qy, qxx, qxy, qyy, strongVisc);

  for (int i = 0; i < pde.N; i++)
    BOOST_CHECK_CLOSE(strongViscTrue(i), strongVisc(i), close_tol);

  pde.strongFluxViscous(dist, x, y, time, q, qx, qy, qxx, qxy, qyy, strongVisc);

  for (int i = 0; i < pde.N; i++)
    BOOST_CHECK_CLOSE(2*strongViscTrue(i), strongVisc(i), close_tol);

  MatrixQ kxx_x = 0;
  MatrixQ kxy_x = 0;
  MatrixQ kyx_x = 0;
  MatrixQ kxy_y = 0;
  MatrixQ kyx_y = 0;
  MatrixQ kyy_y = 0;

  pde.diffusionViscousGradient(dist, x, y, time, q, qx, qy, qxx, qxy, qyy, kxx_x, kxy_x, kyx_x, kxy_y, kyx_y, kyy_y);

  MatrixQ kxx_xTrue =  {{0,0,0,0,0},
                        {-0.0823045358265325411848575757752,0.285908040982372569951761031892,0,0,0},
                        {-0.0376355344192862796342691791478,0,0.214431030736779427463820773919,0,0},
                        {-0.729966604002541726790175206296,-0.0193067773564679510334838709088,
                         -0.0259462232329481599589472484151,0.326934247856932466585657975194,0},
                        {-3.284384577308392773104067438466,0,0,0,0.280321947513941888597490341225}};

  MatrixQ kxy_xTrue = {{0,0,0,0,0},
                       {0.02509035627952418642284611943189,0,-0.142954020491186284975880515946,0,0},
                       {-0.0617284018698994058886431818314,0.214431030736779427463820773919,0,0,0},
                       {-0.001636298296905254836713274388442,0.0376355344192862796342691791478,
                        -0.0411522679132662705924287878876,0,0},
                       {0,0,0,0,0}};

  MatrixQ kyx_yTrue = {{0,0,0,0,0},
                       {-0.0488636456269307946152477039139,0,0.1652909163793633646593041741214,0,0},
                       {0.01643961195577396837721068807557,-0.1101939442529089097728694494142,0,0,0},
                       {-0.001770814865942333260434457485998,-0.03257576375128719641016513594259,
                        0.0246594179336609525658160321134,0,0},
                       {0,0,0,0,0}};

  MatrixQ kyy_yTrue = {{0,0,0,0,0},
                       {-0.0246594179336609525658160321134,0.1652909163793633646593041741214,0,0,0},
                       {-0.0651515275025743928203302718852,0,0.220387888505817819545738898828,0,0},
                       {-0.657722409904461693830210199723,-0.0148766026390592657162932391608,
                        -0.0180516964837024041789021739910,0.257195044067487987952428988741,0},
                       {-3.166045898824578388379801280024,0,0,0,0.28378707429581893440614131444099}};

#ifdef SANS_SA_QCR

  MatrixQ kyx_xTrue = 0; // TODO: Need real truth values!
  MatrixQ kxy_yTrue = 0;

  Real rhoi   = 1/rho;
  Real rhoi_x = -rhox*rhoi*rhoi;
  Real rhoi_y = -rhoy*rhoi*rhoi;

  // velocity gradient Jacobian wrt conservative variables
  Real ux_rhox    = -u*rhoi;
  Real ux_rhox_x  = -ux*rhoi - u*rhoi_x;
  Real ux_rhox_y  = -uy*rhoi - u*rhoi_y;

  Real ux_rhoux   = rhoi;
  Real ux_rhoux_x = rhoi_x;
  Real ux_rhoux_y = rhoi_y;

  Real uy_rhoy    = ux_rhox;
  Real uy_rhoy_x  = ux_rhox_x;
  Real uy_rhoy_y  = ux_rhox_y;

  Real uy_rhouy   = rhoi;
  Real uy_rhouy_x = rhoi_x;
  Real uy_rhouy_y = rhoi_y;

  Real vx_rhox    = -v*rhoi;
  Real vx_rhox_x  = -vx*rhoi - v*rhoi_x;
  Real vx_rhox_y  = -vy*rhoi - v*rhoi_y;

  Real vx_rhovx   = rhoi;
  Real vx_rhovx_x = rhoi_x;
  Real vx_rhovx_y = rhoi_y;

  Real vy_rhoy    = vx_rhox;
  Real vy_rhoy_x  = vx_rhox_x;
  Real vy_rhoy_y  = vx_rhox_y;

  Real vy_rhovy   = rhoi;
  Real vy_rhovy_x = rhoi_x;
  Real vy_rhovy_y = rhoi_y;

  Real tauRxx_ux   = muEddy*2   + lambdaEddy;
  Real tauRxx_ux_x = muEddy_x*2 + lambdaEddy_x;
  Real tauRxx_ux_y = muEddy_y*2 + lambdaEddy_y;

  Real tauRxx_vy   = lambdaEddy;
  Real tauRxx_vy_x = lambdaEddy_x;
  Real tauRxx_vy_y = lambdaEddy_y;

  Real tauRxy_uy   = muEddy;
  Real tauRxy_uy_x = muEddy_x;
  Real tauRxy_uy_y = muEddy_y;

  Real tauRxy_vx   = muEddy;
  Real tauRxy_vx_x = muEddy_x;
  Real tauRxy_vx_y = muEddy_y;

  Real tauRyy_ux   = lambdaEddy;
  Real tauRyy_ux_x = lambdaEddy_x;
  Real tauRyy_ux_y = lambdaEddy_y;

  Real tauRyy_vy   = muEddy*2   + lambdaEddy;
  Real tauRyy_vy_x = muEddy_x*2 + lambdaEddy_x;
  Real tauRyy_vy_y = muEddy_y*2 + lambdaEddy_y;

  Real magGradVi   =  1/magGradV;
  Real magGradVi2  =  magGradVi*magGradVi;
  Real magGradVi_x = -magGradV_x*magGradVi2;
  Real magGradVi_y = -magGradV_y*magGradVi2;

  Real magGradVi2_x = -2*magGradV_x*magGradVi2*magGradVi;
  Real magGradVi2_y = -2*magGradV_y*magGradVi2*magGradVi;

  Real magGradV_ux   = ux*magGradVi;
  Real magGradV_ux_x = uxx*magGradVi + ux*magGradVi_x;
  Real magGradV_ux_y = uxy*magGradVi + ux*magGradVi_y;

  Real magGradV_uy   = uy*magGradVi;
  Real magGradV_uy_x = uxy*magGradVi + uy*magGradVi_x;
  Real magGradV_uy_y = uyy*magGradVi + uy*magGradVi_y;

  Real magGradV_vx   = vx*magGradVi;
  Real magGradV_vx_x = vxx*magGradVi + vx*magGradVi_x;
  Real magGradV_vx_y = vxy*magGradVi + vx*magGradVi_y;

  Real magGradV_vy   = vy*magGradVi;
  Real magGradV_vy_x = vxy*magGradVi + vy*magGradVi_x;
  Real magGradV_vy_y = vyy*magGradVi + vy*magGradVi_y;

  Real Wxy_uy = (1        );
  Real Wxy_vx = (    - 1  );

  Real Oxy_ux   = -Wxy*magGradV_ux*magGradVi2;
  Real Oxy_ux_x = -Wxy_x*magGradV_ux*magGradVi2 - Wxy*magGradV_ux_x*magGradVi2 - Wxy*magGradV_ux*magGradVi2_x;
  Real Oxy_ux_y = -Wxy_y*magGradV_ux*magGradVi2 - Wxy*magGradV_ux_y*magGradVi2 - Wxy*magGradV_ux*magGradVi2_y;

  Real Oxy_uy   =  Wxy_uy*magGradVi   - Wxy*magGradV_uy*magGradVi2;
  Real Oxy_uy_x =  Wxy_uy*magGradVi_x - Wxy_x*magGradV_uy*magGradVi2 - Wxy*magGradV_uy_x*magGradVi2 - Wxy*magGradV_uy*magGradVi2_x;
  Real Oxy_uy_y =  Wxy_uy*magGradVi_y - Wxy_y*magGradV_uy*magGradVi2 - Wxy*magGradV_uy_y*magGradVi2 - Wxy*magGradV_uy*magGradVi2_y;

  Real Oxy_vx   =  Wxy_vx*magGradVi   - Wxy*magGradV_vx*magGradVi2;
  Real Oxy_vx_x =  Wxy_vx*magGradVi_x - Wxy_x*magGradV_vx*magGradVi2 - Wxy*magGradV_vx_x*magGradVi2 - Wxy*magGradV_vx*magGradVi2_x;
  Real Oxy_vx_y =  Wxy_vx*magGradVi_y - Wxy_y*magGradV_vx*magGradVi2 - Wxy*magGradV_vx_y*magGradVi2 - Wxy*magGradV_vx*magGradVi2_y;

  Real Oxy_vy   = -Wxy*magGradV_vy*magGradVi2;
  Real Oxy_vy_x = -Wxy_x*magGradV_vy*magGradVi2 - Wxy*magGradV_vy_x*magGradVi2 - Wxy*magGradV_vy*magGradVi2_x;
  Real Oxy_vy_y = -Wxy_y*magGradV_vy*magGradVi2 - Wxy*magGradV_vy_y*magGradVi2 - Wxy*magGradV_vy*magGradVi2_y;

  Real Oyx_ux   = -Oxy_ux;
  Real Oyx_ux_x = -Oxy_ux_x;
  Real Oyx_ux_y = -Oxy_ux_y;

  Real Oyx_uy   = -Oxy_uy;
  Real Oyx_uy_x = -Oxy_uy_x;
  Real Oyx_uy_y = -Oxy_uy_y;

  Real Oyx_vx   = -Oxy_vx;
  Real Oyx_vx_x = -Oxy_vx_x;
  Real Oyx_vx_y = -Oxy_vx_y;

  Real Oyx_vy   = -Oxy_vy;
  Real Oyx_vy_x = -Oxy_vy_x;
  Real Oyx_vy_y = -Oxy_vy_y;

  // tau_ij,QCR = tau_ij - cr1*(O_ik*tau_jk + O_jk*tau_ik)

  // qcrxx = cr1*(Oxx*tauxx + Oxy*tauxy + Oxx*tauxx + Oxy*tauxy)
  // qcrxy = cr1*(Oxx*tauyx + Oxy*tauyy + Oyx*tauxx + Oyy*tauxy)
  // qcryx = cr1*(Oyx*tauxx + Oyy*tauxy + Oxx*tauyx + Oxy*tauyy)
  // qcryy = cr1*(Oyx*tauyx + Oyy*tauyy + Oyx*tauyx + Oyy*tauyy)

  //   qcrxx      = cr1*2*(Oxy*tauRxy);
  Real qcrxx_ux   = cr1*2*(Oxy_ux*tauRxy                     );
  Real qcrxx_ux_x = cr1*2*(Oxy_ux_x*tauRxy + Oxy_ux*tauRxy_x );
//Real qcrxx_ux_y = cr1*2*(Oxy_ux_y*tauRxy + Oxy_ux*tauRxy_y );

  Real qcrxx_uy   = cr1*2*(Oxy_uy*tauRxy                     + Oxy*tauRxy_uy);
  Real qcrxx_uy_x = cr1*2*(Oxy_uy_x*tauRxy + Oxy_uy*tauRxy_x + Oxy_x*tauRxy_uy + Oxy*tauRxy_uy_x);
  Real qcrxx_uy_y = cr1*2*(Oxy_uy_y*tauRxy + Oxy_uy*tauRxy_y + Oxy_y*tauRxy_uy + Oxy*tauRxy_uy_y);

  Real qcrxx_vx   = cr1*2*(Oxy_vx*tauRxy                     + Oxy*tauRxy_vx);
  Real qcrxx_vx_x = cr1*2*(Oxy_vx_x*tauRxy + Oxy_vx*tauRxy_x + Oxy_x*tauRxy_vx + Oxy*tauRxy_vx_x);
//Real qcrxx_vx_y = cr1*2*(Oxy_vx_y*tauRxy + Oxy_vx*tauRxy_y + Oxy_y*tauRxy_vx + Oxy*tauRxy_vx_y);

  Real qcrxx_vy   = cr1*2*(Oxy_vy*tauRxy                );
  Real qcrxx_vy_x = cr1*2*(Oxy_vy_x*tauRxy + Oxy_vy*tauRxy_x );
  Real qcrxx_vy_y = cr1*2*(Oxy_vy_y*tauRxy + Oxy_vy*tauRxy_y );

  //   qcrxy      = cr1*(Oxy*tauRyy    + Oyx*tauRxx);
  Real qcrxy_ux   = cr1*(Oxy_ux*tauRyy + Oxy*tauRyy_ux + Oyx_ux*tauRxx + Oyx*tauRxx_ux);
  Real qcrxy_ux_x = cr1*(Oxy_ux_x*tauRyy + Oxy_ux*tauRyy_x +
                          Oxy_x*tauRyy_ux + Oxy*tauRyy_ux_x +
                          Oyx_ux_x*tauRxx + Oyx_ux*tauRxx_x +
                          Oyx_x*tauRxx_ux + Oyx*tauRxx_ux_x);
  Real qcrxy_ux_y = cr1*(Oxy_ux_y*tauRyy + Oxy_ux*tauRyy_y +
                          Oxy_y*tauRyy_ux + Oxy*tauRyy_ux_y +
                          Oyx_ux_y*tauRxx + Oyx_ux*tauRxx_y +
                          Oyx_y*tauRxx_ux + Oyx*tauRxx_ux_y);

  Real qcrxy_uy   = cr1*(Oxy_uy*tauRyy                     + Oyx_uy*tauRxx                     );
  Real qcrxy_uy_x = cr1*(Oxy_uy_x*tauRyy + Oxy_uy*tauRyy_x + Oyx_uy_x*tauRxx + Oyx_uy*tauRxx_x );
  Real qcrxy_uy_y = cr1*(Oxy_uy_y*tauRyy + Oxy_uy*tauRyy_y + Oyx_uy_y*tauRxx + Oyx_uy*tauRxx_y );

  Real qcrxy_vx   = cr1*(Oxy_vx*tauRyy                     + Oyx_vx*tauRxx                     );
  Real qcrxy_vx_x = cr1*(Oxy_vx_x*tauRyy + Oxy_vx*tauRyy_x + Oyx_vx_x*tauRxx + Oyx_vx*tauRxx_x );
  Real qcrxy_vx_y = cr1*(Oxy_vx_y*tauRyy + Oxy_vx*tauRyy_y + Oyx_vx_y*tauRxx + Oyx_vx*tauRxx_y );

  Real qcrxy_vy   = cr1*(Oxy_vy*tauRyy + Oxy*tauRyy_vy + Oyx_vy*tauRxx + Oyx*tauRxx_vy);
  Real qcrxy_vy_x = cr1*(Oxy_vy_x*tauRyy + Oxy_vy*tauRyy_x +
                          Oxy_x*tauRyy_vy + Oxy*tauRyy_vy_x +
                          Oyx_vy_x*tauRxx + Oyx_vy*tauRxx_x +
                          Oyx_x*tauRxx_vy + Oyx*tauRxx_vy_x);
  Real qcrxy_vy_y = cr1*(Oxy_vy_y*tauRyy + Oxy_vy*tauRyy_y +
                          Oxy_y*tauRyy_vy + Oxy*tauRyy_vy_y +
                          Oyx_vy_y*tauRxx + Oyx_vy*tauRxx_y +
                          Oyx_y*tauRxx_vy + Oyx*tauRxx_vy_y);

  //   qcryy      = cr1*2*(Oyx*tauRxy);
  Real qcryy_ux   = cr1*2*(Oyx_ux*tauRxy                    );
  Real qcryy_ux_x = cr1*2*(Oyx_ux_x*tauRxy + Oyx_ux*tauRxy_x);
  Real qcryy_ux_y = cr1*2*(Oyx_ux_y*tauRxy + Oyx_ux*tauRxy_y);

  Real qcryy_uy   = cr1*2*(Oyx_uy*tauRxy                     + Oyx*tauRxy_uy                    );
//Real qcryy_uy_x = cr1*2*(Oyx_uy_x*tauRxy + Oyx_uy*tauRxy_x + Oyx_x*tauRxy_uy + Oyx*tauRxy_uy_x);
  Real qcryy_uy_y = cr1*2*(Oyx_uy_y*tauRxy + Oyx_uy*tauRxy_y + Oyx_y*tauRxy_uy + Oyx*tauRxy_uy_y);

  Real qcryy_vx   = cr1*2*(Oyx_vx*tauRxy                     + Oyx*tauRxy_vx);
  Real qcryy_vx_x = cr1*2*(Oyx_vx_x*tauRxy + Oyx_vx*tauRxy_x + Oyx_x*tauRxy_vx + Oyx*tauRxy_vx_x);
  Real qcryy_vx_y = cr1*2*(Oyx_vx_y*tauRxy + Oyx_vx*tauRxy_y + Oyx_y*tauRxy_vx + Oyx*tauRxy_vx_y);

  Real qcryy_vy   = cr1*2*(Oyx_vy*tauRxy                    );
//Real qcryy_vy_x = cr1*2*(Oyx_vy_x*tauRxy + Oyx_vy*tauRxy_x);
  Real qcryy_vy_y = cr1*2*(Oyx_vy_y*tauRxy + Oyx_vy*tauRxy_y);

  // Convert to derivatives w.r.t. master state
  //qcrxx
  Real qcrxx_rhox    = qcrxx_ux*ux_rhox                        + qcrxx_vx*vx_rhox;
  Real qcrxx_rhox_x  = qcrxx_ux_x*ux_rhox + qcrxx_ux*ux_rhox_x + qcrxx_vx_x*vx_rhox + qcrxx_vx*vx_rhox_x;
//Real qcrxx_rhox_y  = qcrxx_ux_y*ux_rhox + qcrxx_ux*ux_rhox_y + qcrxx_vx_y*vx_rhox + qcrxx_vx*vx_rhox_y;

  Real qcrxx_rhoy    = qcrxx_uy*uy_rhoy                        + qcrxx_vy*vy_rhoy;
  Real qcrxx_rhoy_x  = qcrxx_uy_x*uy_rhoy + qcrxx_uy*uy_rhoy_x + qcrxx_vy_x*vy_rhoy + qcrxx_vy*vy_rhoy_x;
  Real qcrxx_rhoy_y  = qcrxx_uy_y*uy_rhoy + qcrxx_uy*uy_rhoy_y + qcrxx_vy_y*vy_rhoy + qcrxx_vy*vy_rhoy_y;

  Real qcrxx_rhoux   = qcrxx_ux*ux_rhoux;
  Real qcrxx_rhoux_x = qcrxx_ux_x*ux_rhoux + qcrxx_ux*ux_rhoux_x;
//Real qcrxx_rhoux_y = qcrxx_ux_y*ux_rhoux + qcrxx_ux*ux_rhoux_y;

  Real qcrxx_rhouy   = qcrxx_uy*uy_rhouy;
  Real qcrxx_rhouy_x = qcrxx_uy_x*uy_rhouy + qcrxx_uy*uy_rhouy_x;
  Real qcrxx_rhouy_y = qcrxx_uy_y*uy_rhouy + qcrxx_uy*uy_rhouy_y;

  Real qcrxx_rhovx   = qcrxx_vx*vx_rhovx;
  Real qcrxx_rhovx_x = qcrxx_vx_x*vx_rhovx + qcrxx_vx*vx_rhovx_x;
//Real qcrxx_rhovx_y = qcrxx_vx_y*vx_rhovx + qcrxx_vx*vx_rhovx_y;

  Real qcrxx_rhovy   = qcrxx_vy*vy_rhovy;
  Real qcrxx_rhovy_x = qcrxx_vy_x*vy_rhovy + qcrxx_vy*vy_rhovy_x;
  Real qcrxx_rhovy_y = qcrxx_vy_y*vy_rhovy + qcrxx_vy*vy_rhovy_y;

  //qcrxy
  Real qcrxy_rhox    = qcrxy_ux*ux_rhox                        + qcrxy_vx*vx_rhox;
  Real qcrxy_rhox_x  = qcrxy_ux_x*ux_rhox + qcrxy_ux*ux_rhox_x + qcrxy_vx_x*vx_rhox + qcrxy_vx*vx_rhox_x;
  Real qcrxy_rhox_y  = qcrxy_ux_y*ux_rhox + qcrxy_ux*ux_rhox_y + qcrxy_vx_y*vx_rhox + qcrxy_vx*vx_rhox_y;

  Real qcrxy_rhoy    = qcrxy_uy*uy_rhoy                        + qcrxy_vy*vy_rhoy;
  Real qcrxy_rhoy_x  = qcrxy_uy_x*uy_rhoy + qcrxy_uy*uy_rhoy_x + qcrxy_vy_x*vy_rhoy + qcrxy_vy*vy_rhoy_x;
  Real qcrxy_rhoy_y  = qcrxy_uy_y*uy_rhoy + qcrxy_uy*uy_rhoy_y + qcrxy_vy_y*vy_rhoy + qcrxy_vy*vy_rhoy_y;

  Real qcrxy_rhoux   = qcrxy_ux*ux_rhoux;
  Real qcrxy_rhoux_x = qcrxy_ux_x*ux_rhoux + qcrxy_ux*ux_rhoux_x;
  Real qcrxy_rhoux_y = qcrxy_ux_y*ux_rhoux + qcrxy_ux*ux_rhoux_y;

  Real qcrxy_rhouy   = qcrxy_uy*uy_rhouy;
  Real qcrxy_rhouy_x = qcrxy_uy_x*uy_rhouy + qcrxy_uy*uy_rhouy_x;
  Real qcrxy_rhouy_y = qcrxy_uy_y*uy_rhouy + qcrxy_uy*uy_rhouy_y;

  Real qcrxy_rhovx   = qcrxy_vx*vx_rhovx;
  Real qcrxy_rhovx_x = qcrxy_vx_x*vx_rhovx + qcrxy_vx*vx_rhovx_x;
  Real qcrxy_rhovx_y = qcrxy_vx_y*vx_rhovx + qcrxy_vx*vx_rhovx_y;

  Real qcrxy_rhovy   = qcrxy_vy*vy_rhovy;
  Real qcrxy_rhovy_x = qcrxy_vy_x*vy_rhovy + qcrxy_vy*vy_rhovy_x;
  Real qcrxy_rhovy_y = qcrxy_vy_y*vy_rhovy + qcrxy_vy*vy_rhovy_y;

  //qcryy
  Real qcryy_rhox   = qcryy_ux*ux_rhox                        + qcryy_vx*vx_rhox;
  Real qcryy_rhox_x = qcryy_ux_x*ux_rhox + qcryy_ux*ux_rhox_x + qcryy_vx_x*vx_rhox + qcryy_vx*vx_rhox_x;
  Real qcryy_rhox_y = qcryy_ux_y*ux_rhox + qcryy_ux*ux_rhox_y + qcryy_vx_y*vx_rhox + qcryy_vx*vx_rhox_y;

  Real qcryy_rhoy   = qcryy_uy*uy_rhoy                        + qcryy_vy*vy_rhoy;
//Real qcryy_rhoy_x = qcryy_uy_x*uy_rhoy + qcryy_uy*uy_rhoy_x + qcryy_vy_x*vy_rhoy + qcryy_vy*vy_rhoy_x;
  Real qcryy_rhoy_y = qcryy_uy_y*uy_rhoy + qcryy_uy*uy_rhoy_y + qcryy_vy_y*vy_rhoy + qcryy_vy*vy_rhoy_y;

  Real qcryy_rhoux   = qcryy_ux*ux_rhoux;
  Real qcryy_rhoux_x = qcryy_ux_x*ux_rhoux + qcryy_ux*ux_rhoux_x;
  Real qcryy_rhoux_y = qcryy_ux_y*ux_rhoux + qcryy_ux*ux_rhoux_y;

  Real qcryy_rhouy   = qcryy_uy*uy_rhouy;
//Real qcryy_rhouy_x = qcryy_uy_x*uy_rhouy + qcryy_uy*uy_rhouy_x;
  Real qcryy_rhouy_y = qcryy_uy_y*uy_rhouy + qcryy_uy*uy_rhouy_y;

  Real qcryy_rhovx   = qcryy_vx*vx_rhovx;
  Real qcryy_rhovx_x = qcryy_vx_x*vx_rhovx + qcryy_vx*vx_rhovx_x;
  Real qcryy_rhovx_y = qcryy_vx_y*vx_rhovx + qcryy_vx*vx_rhovx_y;

  Real qcryy_rhovy   = qcryy_vy*vy_rhovy;
//Real qcryy_rhovy_x = qcryy_vy_x*vy_rhovy + qcryy_vy*vy_rhovy_x;
  Real qcryy_rhovy_y = qcryy_vy_y*vy_rhovy + qcryy_vy*vy_rhovy_y;


  // d(Fv)/d(Ux)
  kxx_xTrue(pde.ixMom,0) -= qcrxx_rhox_x ;
  kxx_xTrue(pde.ixMom,1) -= qcrxx_rhoux_x;
  kxx_xTrue(pde.ixMom,2) -= qcrxx_rhovx_x;

//    kxx_y(ixMom,0) -= qcrxx_rhox_y ;
//    kxx_y(ixMom,1) -= qcrxx_rhoux_y;
//    kxx_y(ixMom,2) -= qcrxx_rhovx_y;

  kxx_xTrue(pde.iyMom,0) -= qcrxy_rhox_x ;
  kxx_xTrue(pde.iyMom,1) -= qcrxy_rhoux_x;
  kxx_xTrue(pde.iyMom,2) -= qcrxy_rhovx_x;

//    kxx_y(iyMom,0) -= qcrxy_rhox_y ;
//    kxx_y(iyMom,1) -= qcrxy_rhoux_y;
//    kxx_y(iyMom,2) -= qcrxy_rhovx_y;

  kxx_xTrue(pde.iEngy,0) -= ux*qcrxx_rhox  + u*qcrxx_rhox_x  + vx*qcrxy_rhox  + v*qcrxy_rhox_x ;
  kxx_xTrue(pde.iEngy,1) -= ux*qcrxx_rhoux + u*qcrxx_rhoux_x + vx*qcrxy_rhoux + v*qcrxy_rhoux_x;
  kxx_xTrue(pde.iEngy,2) -= ux*qcrxx_rhovx + u*qcrxx_rhovx_x + vx*qcrxy_rhovx + v*qcrxy_rhovx_x;

//    kxx_y(iEngy,0) -= uy*qcrxx_rhox  + u*qcrxx_rhox_y  + vy*qcrxy_rhox  + v*qcrxy_rhox_y ;
//    kxx_y(iEngy,1) -= uy*qcrxx_rhoux + u*qcrxx_rhoux_y + vy*qcrxy_rhoux + v*qcrxy_rhoux_y;
//    kxx_y(iEngy,2) -= uy*qcrxx_rhovx + u*qcrxx_rhovx_y + vy*qcrxy_rhovx + v*qcrxy_rhovx_y;

  // d(Fv)/d(Uy)
  kxy_xTrue(pde.ixMom,0) -= qcrxx_rhoy_x ;
  kxy_xTrue(pde.ixMom,1) -= qcrxx_rhouy_x;
  kxy_xTrue(pde.ixMom,2) -= qcrxx_rhovy_x;

  kxy_yTrue(pde.ixMom,0) -= qcrxx_rhoy_y ;
  kxy_yTrue(pde.ixMom,1) -= qcrxx_rhouy_y;
  kxy_yTrue(pde.ixMom,2) -= qcrxx_rhovy_y;

  kxy_xTrue(pde.iyMom,0) -= qcrxy_rhoy_x ;
  kxy_xTrue(pde.iyMom,1) -= qcrxy_rhouy_x;
  kxy_xTrue(pde.iyMom,2) -= qcrxy_rhovy_x;

  kxy_yTrue(pde.iyMom,0) -= qcrxy_rhoy_y ;
  kxy_yTrue(pde.iyMom,1) -= qcrxy_rhouy_y;
  kxy_yTrue(pde.iyMom,2) -= qcrxy_rhovy_y;

  kxy_xTrue(pde.iEngy,0) -= ux*qcrxx_rhoy  + u*qcrxx_rhoy_x  + vx*qcrxy_rhoy  + v*qcrxy_rhoy_x ;
  kxy_xTrue(pde.iEngy,1) -= ux*qcrxx_rhouy + u*qcrxx_rhouy_x + vx*qcrxy_rhouy + v*qcrxy_rhouy_x;
  kxy_xTrue(pde.iEngy,2) -= ux*qcrxx_rhovy + u*qcrxx_rhovy_x + vx*qcrxy_rhovy + v*qcrxy_rhovy_x;

  kxy_yTrue(pde.iEngy,0) -= uy*qcrxx_rhoy  + u*qcrxx_rhoy_y  + vy*qcrxy_rhoy  + v*qcrxy_rhoy_y ;
  kxy_yTrue(pde.iEngy,1) -= uy*qcrxx_rhouy + u*qcrxx_rhouy_y + vy*qcrxy_rhouy + v*qcrxy_rhouy_y;
  kxy_yTrue(pde.iEngy,2) -= uy*qcrxx_rhovy + u*qcrxx_rhovy_y + vy*qcrxy_rhovy + v*qcrxy_rhovy_y;

  // d(Gv)/d(Ux)
  kyx_xTrue(pde.ixMom,0) -= qcrxy_rhox_x ;
  kyx_xTrue(pde.ixMom,1) -= qcrxy_rhoux_x;
  kyx_xTrue(pde.ixMom,2) -= qcrxy_rhovx_x;

  kyx_yTrue(pde.ixMom,0) -= qcrxy_rhox_y ;
  kyx_yTrue(pde.ixMom,1) -= qcrxy_rhoux_y;
  kyx_yTrue(pde.ixMom,2) -= qcrxy_rhovx_y;

  kyx_xTrue(pde.iyMom,0) -= qcryy_rhox_x ;
  kyx_xTrue(pde.iyMom,1) -= qcryy_rhoux_x;
  kyx_xTrue(pde.iyMom,2) -= qcryy_rhovx_x;

  kyx_yTrue(pde.iyMom,0) -= qcryy_rhox_y ;
  kyx_yTrue(pde.iyMom,1) -= qcryy_rhoux_y;
  kyx_yTrue(pde.iyMom,2) -= qcryy_rhovx_y;

  kyx_xTrue(pde.iEngy,0) -= ux*qcrxy_rhox  + u*qcrxy_rhox_x  + vx*qcryy_rhox  + v*qcryy_rhox_x ;
  kyx_xTrue(pde.iEngy,1) -= ux*qcrxy_rhoux + u*qcrxy_rhoux_x + vx*qcryy_rhoux + v*qcryy_rhoux_x;
  kyx_xTrue(pde.iEngy,2) -= ux*qcrxy_rhovx + u*qcrxy_rhovx_x + vx*qcryy_rhovx + v*qcryy_rhovx_x;

  kyx_yTrue(pde.iEngy,0) -= uy*qcrxy_rhox  + u*qcrxy_rhox_y  + vy*qcryy_rhox  + v*qcryy_rhox_y ;
  kyx_yTrue(pde.iEngy,1) -= uy*qcrxy_rhoux + u*qcrxy_rhoux_y + vy*qcryy_rhoux + v*qcryy_rhoux_y;
  kyx_yTrue(pde.iEngy,2) -= uy*qcrxy_rhovx + u*qcrxy_rhovx_y + vy*qcryy_rhovx + v*qcryy_rhovx_y;

  // d(Gv)/d(Uy)
//    kyy_x(ixMom,0) -= qcrxy_rhoy_x ;
//    kyy_x(ixMom,1) -= qcrxy_rhouy_x;
//    kyy_x(ixMom,2) -= qcrxy_rhovy_x;

  kyy_yTrue(pde.ixMom,0) -= qcrxy_rhoy_y ;
  kyy_yTrue(pde.ixMom,1) -= qcrxy_rhouy_y;
  kyy_yTrue(pde.ixMom,2) -= qcrxy_rhovy_y;

//    kyy_x(iyMom,0) -= qcryy_rhoy_x ;
//    kyy_x(iyMom,1) -= qcryy_rhouy_x;
//    kyy_x(iyMom,2) -= qcryy_rhovy_x;

  kyy_yTrue(pde.iyMom,0) -= qcryy_rhoy_y ;
  kyy_yTrue(pde.iyMom,1) -= qcryy_rhouy_y;
  kyy_yTrue(pde.iyMom,2) -= qcryy_rhovy_y;

//    kyy_x(iEngy,0) -= ux*qcrxy_rhoy  + u*qcrxy_rhoy_x  + vx*qcryy_rhoy  + v*qcryy_rhoy_x ;
//    kyy_x(iEngy,1) -= ux*qcrxy_rhouy + u*qcrxy_rhouy_x + vx*qcryy_rhouy + v*qcryy_rhouy_x;
//    kyy_x(iEngy,2) -= ux*qcrxy_rhovy + u*qcrxy_rhovy_x + vx*qcryy_rhovy + v*qcryy_rhovy_x;

  kyy_yTrue(pde.iEngy,0) -= uy*qcrxy_rhoy  + u*qcrxy_rhoy_y  + vy*qcryy_rhoy  + v*qcryy_rhoy_y ;
  kyy_yTrue(pde.iEngy,1) -= uy*qcrxy_rhouy + u*qcrxy_rhouy_y + vy*qcryy_rhouy + v*qcryy_rhouy_y;
  kyy_yTrue(pde.iEngy,2) -= uy*qcrxy_rhovy + u*qcrxy_rhovy_y + vy*qcryy_rhovy + v*qcryy_rhovy_y;
#endif

  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
    {
      BOOST_CHECK_CLOSE(kxx_x(i,j), kxx_xTrue(i,j), close_tol);
      BOOST_CHECK_CLOSE(kxy_x(i,j), kxy_xTrue(i,j), close_tol);
      BOOST_CHECK_CLOSE(kyx_y(i,j), kyx_yTrue(i,j), close_tol);
      BOOST_CHECK_CLOSE(kyy_y(i,j), kyy_yTrue(i,j), close_tol);
    }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jacobianFluxViscous )
{

  // Use conservative variables here in order to differentiate with Surreal
  typedef QTypeConservative QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef SurrealS<PDEClass::N> SurrealClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;
  typedef PDEClass::ArrayQ<SurrealClass> ArrayQSurreal;

  bool verbose = false;

  const Real small_tol = 1.e-10;
  const Real close_tol = 1.e-10;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real tSuth = 110./300.; //somewhat random...
  const Real tRef  = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, time;
  Real rho, u, v, t;

  x = 0; y = 0; time = 0;   // not actually used in functions
  rho = 1.225;
  u = 6.974;
  v = -3.231;
  t = 2.8815;

  Real e = gas.energy(rho, t);
  Real E = e + 0.5*(u*u + v*v);

  Real rhox = 0.0421, rhoy = 0.0127;
  Real ux = 0.0521, uy = -0.0124;
  Real vx = -0.0312, vy = -0.0022;
  Real tx = -0.0715, ty = -0.0111;

  //Real px = R*(rho*tx + rhox*t);
  //Real py = R*(rho*ty + rhoy*t);

  Real rhoux = rhox*u + ux*rho;
  Real rhouy = rhoy*u + uy*rho;

  Real rhovx = rhox*v + vx*rho;
  Real rhovy = rhoy*v + vy*rho;

  Real ex, ey;
  gas.energyGradient(rho, t, rhox, tx, ex);
  gas.energyGradient(rho, t, rhoy, ty, ey);

  Real Ex = ex + ux*u + vx*v;
  Real Ey = ey + uy*u + vy*v;

  Real rhoEx = rhox*E + rho*Ex;
  Real rhoEy = rhoy*E + rho*Ey;

  Real nt, ntx, nty;
  Real chi, chix, chiy;
  Real dist;

  ArrayQ q, qx, qy;
  MatrixQ dfdu;
  MatrixQ dgdu;
  ArrayQSurreal qS, qxS, qyS;
  ArrayQSurreal fv;
  ArrayQSurreal gv;

  SurrealClass rhoS  = rho;
  SurrealClass rhouS = rho*u;
  SurrealClass rhovS = rho*v;
  SurrealClass rhoES = rho*E;
  SurrealClass rhontS = 0;

  rhoS.deriv(0)   = 1;
  rhouS.deriv(1)  = 1;
  rhovS.deriv(2)  = 1;
  rhoES.deriv(3)  = 1;
  rhontS.deriv(4) = 1;

  // set the complete state vectors (sans nt)
  // the nt values are set for each case
  pde.setDOFFrom( q, SAnt2D<Conservative2D<Real>>        ({rho , rho*u, rho*v, rho*E, 0     }));
  pde.setDOFFrom(qS, SAnt2D<Conservative2D<SurrealClass>>({rhoS, rhouS, rhovS, rhoES, rhontS}));

  qxS = qx = {rhox, rhoux, rhovx, rhoEx, 0};
  qyS = qy = {rhoy, rhouy, rhovy, rhoEy, 0};


  // case 1:
  if (verbose) cout << "jacobianFluxViscous: case1" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0;
  dist = 60;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qS[4].value() = q[4]  = rho*nt;

  qxS[4] = qx[4] = rhox*nt + rho*ntx;
  qyS[4] = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  fv = 0;
  gv = 0;
  pde.fluxViscous( dist, x, y, time, qS, qxS, qyS, fv, gv );

  // Compute the hand coded Jacobian
  dfdu = 0;
  dgdu = 0;
  pde.jacobianFluxViscous( dist, x, y, time, q, qx, qy, dfdu, dgdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " fv(" << i << ").deriv(" << j << ") = " << fv(i).deriv(j) << " | " << dfdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( fv(i).deriv(j), dfdu(i,j), small_tol, close_tol );

      if (verbose) std::cout << " gv(" << i << ").deriv(" << j << ") = " << gv(i).deriv(j) << " | " << dgdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( gv(i).deriv(j), dgdu(i,j), small_tol, close_tol );
    }


  // case 2: prod/wall dominant; S~ branch 2
  if (verbose) cout << "source: case2" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qS[4].value() = q[4]  = rho*nt;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  fv = 0;
  gv = 0;
  pde.fluxViscous( dist, x, y, time, qS, qxS, qyS, fv, gv );

  // Compute the hand coded Jacobian
  dfdu = 0;
  dgdu = 0;
  pde.jacobianFluxViscous( dist, x, y, time, q, qx, qy, dfdu, dgdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " fv(" << i << ").deriv(" << j << ") = " << fv(i).deriv(j) << " | " << dfdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( fv(i).deriv(j), dfdu(i,j), small_tol, close_tol );
//
      if (verbose) std::cout << " gv(" << i << ").deriv(" << j << ") = " << gv(i).deriv(j) << " | " << dgdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( gv(i).deriv(j), dgdu(i,j), small_tol, close_tol );
    }


  // case 3: prod/wall ft2 terms dominant
  if (verbose) cout << "source: case3" << endl;

  chi  = 1.241;
  chix = 0; chiy = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qS[4].value() = q[4]  = rho*nt;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  fv = 0;
  gv = 0;
  pde.fluxViscous( dist, x, y, time, qS, qxS, qyS, fv, gv );

  // Compute the hand coded Jacobian
  dfdu = 0;
  dgdu = 0;
  pde.jacobianFluxViscous( dist, x, y, time, q, qx, qy, dfdu, dgdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " fv(" << i << ").deriv(" << j << ") = " << fv(i).deriv(j) << " | " << dfdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( fv(i).deriv(j), dfdu(i,j), small_tol, close_tol );
//
      if (verbose) std::cout << " gv(" << i << ").deriv(" << j << ") = " << gv(i).deriv(j) << " | " << dgdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( gv(i).deriv(j), dgdu(i,j), small_tol, close_tol );
    }


  // case 4: nonconservative diffusion dominant
  if (verbose) cout << "source: case4" << endl;

  chi  = 5.241;
  chix = 1.963; chiy = -0.781;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qS[4].value() = q[4]  = rho*nt;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  fv = 0;
  gv = 0;
  pde.fluxViscous( dist, x, y, time, qS, qxS, qyS, fv, gv );

  // Compute the hand coded Jacobian
  dfdu = 0;
  dgdu = 0;
  pde.jacobianFluxViscous( dist, x, y, time, q, qx, qy, dfdu, dgdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " fv(" << i << ").deriv(" << j << ") = " << fv(i).deriv(j) << " | " << dfdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( fv(i).deriv(j), dfdu(i,j), small_tol, close_tol );
//
      if (verbose) std::cout << " gv(" << i << ").deriv(" << j << ") = " << gv(i).deriv(j) << " | " << dgdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( gv(i).deriv(j), dgdu(i,j), small_tol, close_tol );
    }

  // case 5: negative SA; prod/wall dominant
  if (verbose) cout << "source: case5" << endl;

  chi  = -5.241;
  chix = 0; chiy = 0;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qS[4].value() = q[4]  = rho*nt;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  fv = 0;
  gv = 0;
  pde.fluxViscous( dist, x, y, time, qS, qxS, qyS, fv, gv );

  // Compute the hand coded Jacobian
  dfdu = 0;
  dgdu = 0;
  pde.jacobianFluxViscous( dist, x, y, time, q, qx, qy, dfdu, dgdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " fv(" << i << ").deriv(" << j << ") = " << fv(i).deriv(j) << " | " << dfdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( fv(i).deriv(j), dfdu(i,j), small_tol, close_tol );
//
      if (verbose) std::cout << " gv(" << i << ").deriv(" << j << ") = " << gv(i).deriv(j) << " | " << dgdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( gv(i).deriv(j), dgdu(i,j), small_tol, close_tol );
    }


  // case 6: negative SA; nonconservative diffusion dominant
  if (verbose) cout << "source: case6" << endl;

  chi  = -5.241;
  chix = 1.963; chiy = -0.781;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qS[4].value() = q[4]  = rho*nt;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  fv = 0;
  gv = 0;
  pde.fluxViscous( dist, x, y, time, qS, qxS, qyS, fv, gv );

  // Compute the hand coded Jacobian
  dfdu = 0;
  dgdu = 0;
  pde.jacobianFluxViscous( dist, x, y, time, q, qx, qy, dfdu, dgdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " fv(" << i << ").deriv(" << j << ") = " << fv(i).deriv(j) << " | " << dfdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( fv(i).deriv(j), dfdu(i,j), small_tol, close_tol );
//
      if (verbose) std::cout << " gv(" << i << ").deriv(" << j << ") = " << gv(i).deriv(j) << " | " << dgdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( gv(i).deriv(j), dgdu(i,j), small_tol, close_tol );
    }


  // case 7: negative SA; shrmod < -cv2_*shr0
  if (verbose) cout << "source: case7" << endl;

  chi  = 10.241;
  chix = 1.963; chiy = -0.781;
  dist = 0.1;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qS[4].value() = q[4]  = rho*nt;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  fv = 0;
  gv = 0;
  pde.fluxViscous( dist, x, y, time, qS, qxS, qyS, fv, gv );

  // Compute the hand coded Jacobian
  dfdu = 0;
  dgdu = 0;
  pde.jacobianFluxViscous( dist, x, y, time, q, qx, qy, dfdu, dgdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " fv(" << i << ").deriv(" << j << ") = " << fv(i).deriv(j) << " | " << dfdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( fv(i).deriv(j), dfdu(i,j), small_tol, close_tol );
//
      if (verbose) std::cout << " gv(" << i << ").deriv(" << j << ") = " << gv(i).deriv(j) << " | " << dgdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( gv(i).deriv(j), dgdu(i,j), small_tol, close_tol );
    }


  // case 8: shr0 == 0
  if (verbose) cout << "source: case8" << endl;

  chi  = 5.241;
  chix = 1.963; chiy = -0.781;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qS[4].value() = q[4]  = rho*nt;

  qxS[1].value() = qx[1] = 0;
  qyS[1].value() = qy[1] = 0;

  qxS[2].value() = qx[2] = 0;
  qyS[2].value() = qy[2] = 0;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;

  // Compute the true jacobian with Surreals
  fv = 0;
  gv = 0;
  pde.fluxViscous( dist, x, y, time, qS, qxS, qyS, fv, gv );

  // Compute the hand coded Jacobian
  dfdu = 0;
  dgdu = 0;
  pde.jacobianFluxViscous( dist, x, y, time, q, qx, qy, dfdu, dgdu );

  // check that the jacobian is correct
  for (int i = 0; i < pde.N; i++)
    for (int j = 0; j < pde.N; j++)
    {
      if (verbose) std::cout << " fv(" << i << ").deriv(" << j << ") = " << fv(i).deriv(j) << " | " << dfdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( fv(i).deriv(j), dfdu(i,j), small_tol, close_tol );
//
      if (verbose) std::cout << " gv(" << i << ").deriv(" << j << ") = " << gv(i).deriv(j) << " | " << dgdu(i,j) << std::endl;
      SANS_CHECK_CLOSE( gv(i).deriv(j), dgdu(i,j), small_tol, close_tol );
    }

}


//----------------------------------------------------------------------------//
template<class PDEClass, class ArrayQ>
void diffusionViscousGradient_tester(const PDEClass& pde, const ArrayQ& q)
{
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  // not used
  Real x, y, time;
  Real dist;

  const GasModel& gas = pde.gasModel();
  Real R = gas.R();
  Real Cv = gas.Cv();

  Real rho, u, v, t, nt;

  pde.variableInterpreter().eval( q, rho, u, v, t );
  pde.variableInterpreter().evalSA( q, nt );

  Real p = gas.pressure(rho, t);

  Real e = gas.energy(rho, t);
  Real E = e + 0.5*(u*u + v*v);

  Real rhox = 0.0421, rhoy = 0.0127;
  Real ux =  0.0521, uy = -0.0124;
  Real vx = -0.0312, vy = -0.0022;
  Real px =  0.0120, py = -3.2300;
  Real ntx =  0.023, nty = 0.1250;

  Real rhoxx = -0.253, uxx =  1.02, vxx =  0.99, pxx =  2.99, ntxx =  0.345;
  Real rhoxy =  0.782, uxy = -0.95, vxy = -0.92, pxy = -1.92, ntxy =  1.580;
  Real rhoyy =  1.080, uyy = -0.42, vyy = -0.44, pyy = -1.44, ntyy = -0.152;

  ArrayQ qx = {rhox, ux, vx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, py, nty};

  // t = p/(rho*R)
  Real tx = px/(rho*R) - p/(rho*rho*R)*rhox;
  Real ty = py/(rho*R) - p/(rho*rho*R)*rhoy;

  Real ex = Cv*tx;
  Real ey = Cv*ty;

  Real Ex = ex + u*ux + v*vx;
  Real Ey = ey + u*uy + v*vy;

  ArrayQ qxx = {rhoxx, uxx, vxx, pxx, ntxx};
  ArrayQ qxy = {rhoxy, uxy, vxy, pxy, ntxy};
  ArrayQ qyy = {rhoyy, uyy, vyy, pyy, ntyy};

  Real rho2 = rho*rho;
  Real rho3 = rho*rho*rho;

  Real txx = pxx/(rho*R) - px/(rho2*R)*rhox - px/(rho2*R)*rhox + 2*p/(rho3*R)*rhox*rhox - p/(rho2*R)*rhoxx;
  Real txy = pxy/(rho*R) - py/(rho2*R)*rhox - px/(rho2*R)*rhoy + 2*p/(rho3*R)*rhox*rhoy - p/(rho2*R)*rhoxy;
  Real tyy = pyy/(rho*R) - py/(rho2*R)*rhoy - py/(rho2*R)*rhoy + 2*p/(rho3*R)*rhoy*rhoy - p/(rho2*R)*rhoyy;

  Real exx = Cv*txx;
  Real exy = Cv*txy;
  Real eyy = Cv*tyy;

  Real Exx = exx + ux*ux + u*uxx + vx*vx + v*vxx;
  Real Exy = exy + ux*uy + u*uxy + vx*vy + v*vxy;
  Real Eyy = eyy + uy*uy + u*uyy + vy*vy + v*vyy;

  ArrayQ Ux=0, Uy=0, Uz=0;
  pde.strongFluxAdvectiveTime(dist, x, y, time, q, qx, Ux );
  pde.strongFluxAdvectiveTime(dist, x, y, time, q, qy, Uy );

  ArrayQ Uxx = {rhoxx,
                rhoxx*u + rhox*ux + rhox*ux + rho*uxx,
                rhoxx*v + rhox*vx + rhox*vx + rho*vxx,
                rhoxx*E + rhox*Ex + rhox*Ex + rho*Exx,
                rhoxx*nt + rhox*ntx + rhox*ntx + rho*ntxx};

  ArrayQ Uxy = {rhoxy,
                rhoxy*u + rhoy*ux + rhox*uy + rho*uxy,
                rhoxy*v + rhoy*vx + rhox*vy + rho*vxy,
                rhoxy*E + rhoy*Ex + rhox*Ey + rho*Exy,
                rhoxy*nt + rhoy*ntx + rhox*nty + rho*ntxy};

  ArrayQ Uyy = {rhoyy,
                rhoyy*u + rhoy*uy + rhoy*uy + rho*uyy,
                rhoyy*v + rhoy*vy + rhoy*vy + rho*vyy,
                rhoyy*E + rhoy*Ey + rhoy*Ey + rho*Eyy,
                rhoyy*nt + rhoy*nty + rhoy*nty + rho*ntyy};

  MatrixQ kxx = 0, kxy = 0;
  MatrixQ kyx = 0, kyy = 0;

  MatrixQ kxx_x = 0, kxy_x = 0, kyx_x = 0;
  MatrixQ kxy_y = 0, kyx_y = 0, kyy_y = 0;

  // d(kxx*Ux + kxy*Uy + kxz*Uz)/dx = kxx_x*Ux + kxy_x*Uy + kxz_x*Uz + kxx*Uxx + kxy*Uxy + kxz*Uxz
  // d(kyx*Ux + kyy*Uy + kyz*Uz)/dy = kyx_y*Ux + kyy_y*Uy + kyz_y*Uz + kyx*Uxy + kyy*Uyy + kyz*Uyz
  // d(kzx*Ux + kzy*Uy + kzz*Uz)/dz = kzx_z*Ux + kzy_z*Uy + kzz_z*Uz + kzx*Uxz + kzy*Uyz + kzz*Uzz

  ArrayQ strongViscTrue, strongVisc;

  strongViscTrue = 0;
  pde.strongFluxViscous(dist, x, y, time, q, qx, qy,
                        qxx,
                        qxy, qyy, strongViscTrue);

  kxx = 0, kxy = 0;
  kyx = 0, kyy = 0;
  pde.diffusionViscous( x, y, time, q, qx, qy,
                        kxx, kxy,
                        kyx, kyy );

  kxx_x = 0, kxy_x = 0, kyx_x = 0;
  kxy_y = 0, kyx_y = 0, kyy_y = 0;

  pde.diffusionViscousGradient( dist, x, y, time, q, qx, qy,
                                qxx, qxy, qyy,
                                kxx_x, kxy_x, kyx_x,
                                kxy_y, kyx_y, kyy_y);

  strongVisc = -(kxx_x*Ux + kxy_x*Uy + kxx*Uxx + kxy*Uxy
             +   kyx_y*Ux + kyy_y*Uy + kyx*Uxy + kyy*Uyy);

//  std::cout << "strongViscTrue = " << strongViscTrue << std::endl;
//  std::cout << "strongVisc     = " << strongVisc << std::endl;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-10;

  SANS_CHECK_CLOSE(strongViscTrue(0), strongVisc(0), small_tol, close_tol);
  SANS_CHECK_CLOSE(strongViscTrue(1), strongVisc(1), small_tol, close_tol);
  SANS_CHECK_CLOSE(strongViscTrue(2), strongVisc(2), small_tol, close_tol);
  SANS_CHECK_CLOSE(strongViscTrue(3), strongVisc(3), small_tol, close_tol);
  SANS_CHECK_CLOSE(strongViscTrue(4), strongVisc(4), small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( diffusionViscousGradient_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // function tests

  Real rho, u, v, t;
  Real chi, nt;

  rho = 1.225;                        // kg/m^3
  u = 6.974; v = -3.231;  // m/s
  t = 288.15;                         // K

  // case 1: nt > 0

  chi = 5.241;
  nt  = chi*(muRef/rho);

  ArrayQ q = pde.setDOFFrom( SAnt2D<DensityVelocityTemperature2D<Real>>({rho, u, v, t, nt}) );

  diffusionViscousGradient_tester(pde, q);


  // case 2: zero nt

  nt = 0;

  q = pde.setDOFFrom( SAnt2D<DensityVelocityTemperature2D<Real>>({rho, u, v, t, nt}) );

  diffusionViscousGradient_tester(pde, q);


  // case 3: nt < 0 (negative SA)

  chi = -5.241;
  nt  = chi*(muRef/rho);

  q = pde.setDOFFrom( SAnt2D<DensityVelocityTemperature2D<Real>>({rho, u, v, t, nt}) );

  diffusionViscousGradient_tester(pde, q);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( diffusionViscousGradientPing )
{
  // Use conservative variables here in order to differentiate with Surreal
  typedef QTypeConservative QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef SurrealS<PhysD2::D*PDEClass::N> SurrealClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;
  typedef PDEClass::ArrayQ<SurrealClass> ArrayQSurreal;

  bool verbose = false;

  const Real small_tol = 1.e-10;
  const Real close_tol = 1.e-9;
  const Real ping_tol = 2.e-5;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real tSuth = 110/300; //somewhat random...
  const Real tRef  = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, time;
  Real rho, u, v, t;

  x = 0; y = 0; time = 0;   // not actually used in functions
  rho = 1.225;
  u = 6.974;
  v = -3.231;
  t = 2.8815;

  Real e = gas.energy(rho, t);
  Real p = gas.pressure(rho, t);
  Real E = e + 0.5*(u*u + v*v);

  Real rhox = 0.0421, rhoy = 0.0127;
  Real ux =  0.0521, uy = -0.0124;
  Real vx = -0.0312, vy = -0.0022;
  Real tx = -0.0715, ty = -0.0111;

  Real rhoux = rhox*u + ux*rho;
  Real rhouy = rhoy*u + uy*rho;

  Real rhovx = rhox*v + vx*rho;
  Real rhovy = rhoy*v + vy*rho;

  Real ex, ey;
  gas.energyGradient(rho, t, rhox, tx, ex);
  gas.energyGradient(rho, t, rhoy, ty, ey);

  Real Ex = ex + ux*u + vx*v;
  Real Ey = ey + uy*u + vy*v;

  Real rhoEx = rhox*E + rho*Ex;
  Real rhoEy = rhoy*E + rho*Ey;

  Real nt = 0.0;
  Real ntx, nty;
  Real chi, chix, chiy;
  Real dist;

  ArrayQ q, qx, qy;
  MatrixQ dsdux, dsduy;
  ArrayQSurreal qxS, qyS;
  ArrayQSurreal src;

  // set the complete state vectors (sans nt)
  // the nt values are set for each case
  pde.setDOFFrom( q, SAnt2D<Conservative2D<Real>>({rho , rho*u, rho*v, rho*E, rho*nt}));

  qxS = qx = {rhox, rhoux, rhovx, rhoEx, 0};
  qyS = qy = {rhoy, rhouy, rhovy, rhoEy, 0};


  ArrayQ qxx = {-0.001378, -0.002479, 0.004789, 0.001924, 0.0019838};
  ArrayQ qxy = {-0.00183873, -0.0028383, 0.004891, 0.004729, 0.004848};
  ArrayQ qyy = {-0.0014934, -0.005294, 0.001748, 0.0029808, 0.002974};

  for (int n = 0; n < PDEClass::N; n++)
  {
    qxS[n].deriv(0*PDEClass::N + n) = 1;
    qyS[n].deriv(1*PDEClass::N + n) = 1;
  }

  // Strong Conservative Flux (PRIMITIVE SPECIFIC)
  Real rhot = -0.9;
  Real ut = -0.25;
  Real vt = 0.14;
  Real pt = 0.23;
  Real tt = (t/p) * pt - (t/rho)*rhot;
  Real ntt = 3.14;


  Real e0 = Cv*t + 0.5*(u*u + v*v);
  Real e0t = Cv*tt + u*ut + v*vt;

  Real rhout = rhot*u + rho* ut;
  Real rhovt = rhot*v + rho* vt;
  Real rhoEt = rhot*e0 + rho*e0t;
  Real rhontt = rho*ntt + rhot*nt;

  ArrayQ qt = {rhot, rhout, rhovt, rhoEt, rhontt};
  ArrayQ utCons, utConsTrue;
  utCons = 0;
  pde.strongFluxAdvectiveTime( dist, x, y, time, q, qt, utCons );
  utConsTrue[0] = rhot;
  utConsTrue[1] = rho*ut + rhot*u;
  utConsTrue[2] = rho*vt + rhot*v;
  utConsTrue[3] = rho*e0t + rhot*e0;
  utConsTrue[4] = rho*ntt + rhot*nt;
  BOOST_CHECK_CLOSE( utConsTrue(0), utCons(0), close_tol );
  BOOST_CHECK_CLOSE( utConsTrue(1), utCons(1), close_tol );
  BOOST_CHECK_CLOSE( utConsTrue(2), utCons(2), close_tol );
  BOOST_CHECK_CLOSE( utConsTrue(3), utCons(3), close_tol );
  BOOST_CHECK_CLOSE( utConsTrue(4), utCons(4), close_tol );

  chi  = 5.241;
  chix = 0; chiy = 0;
  dist = 60;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;


//  Real distx = 0.25, disty = sqrt(1-0.25*0.25);
  //PING TEST FOR DIV OF JACOBIANGRADIENTSOURCE
  {

    MatrixQ grad_dsdgradu = 0;
    pde.jacobianGradientSourceGradient(dist, x, y, time, q, qx, qy, qxx, qxy, qyy, grad_dsdgradu);

    //rough ping test
    Real dx = 1e-6;
    Real dy = 1e-6;

    //x derivative
    ArrayQ q1x = q + qx*dx;
    ArrayQ q0x = q - qx*dx;
    ArrayQ qx1x = qx+qxx*dx;
    ArrayQ qx0x = qx-qxx*dx;
    ArrayQ qy1x = qy+qxy*dx;
    ArrayQ qy0x = qy-qxy*dx;

    MatrixQ kxx0x = 0, kxy0x = 0, kyx0x = 0, kyy0x = 0;
    MatrixQ kxx1x = 0, kxy1x = 0, kyx1x = 0, kyy1x = 0;

    pde.diffusionViscous( x, y, time, q0x, qx0x, qy0x, kxx0x, kxy0x, kyx0x, kyy0x );
    pde.diffusionViscous( x, y, time, q1x, qx1x, qy1x, kxx1x, kxy1x, kyx1x, kyy1x );

    //y derivative
    ArrayQ q1y = q + qy*dy;
    ArrayQ q0y = q - qy*dy;
    ArrayQ qx1y = qx+qxy*dy;
    ArrayQ qx0y = qx-qxy*dy;
    ArrayQ qy1y = qy+qyy*dy;
    ArrayQ qy0y = qy-qyy*dy;

    MatrixQ kxx0y = 0, kxy0y = 0, kyx0y = 0, kyy0y = 0;
    MatrixQ kxx1y = 0, kxy1y = 0, kyx1y = 0, kyy1y = 0;

    pde.diffusionViscous( x, y, time, q0y, qx0y, qy0y, kxx0y, kxy0y, kyx0y, kyy0y );
    pde.diffusionViscous( x, y, time, q1y, qx1y, qy1y, kxx1y, kxy1y, kyx1y, kyy1y );

    MatrixQ kxx_xTrue = 0.5*(kxx1x - kxx0x)/dx;
    MatrixQ kxy_xTrue = 0.5*(kxy1x - kxy0x)/dx;
    MatrixQ kyx_xTrue = 0.5*(kyx1x - kyx0x)/dx;
    MatrixQ kxy_yTrue = 0.5*(kxy1y - kxy0y)/dy;
    MatrixQ kyx_yTrue = 0.5*(kyx1y - kyx0y)/dy;
    MatrixQ kyy_yTrue = 0.5*(kyy1y - kyy0y)/dy;

    MatrixQ kxx_x = 0, kxy_x = 0, kyx_x = 0, kxy_y = 0, kyx_y = 0, kyy_y = 0;
    pde.diffusionViscousGradient(dist, x, y, time, q, qx, qy, qxx, qxy, qyy, kxx_x, kxy_x, kyx_x, kxy_y, kyx_y, kyy_y);

    for (int i = 0; i < pde.N; i++)
      for (int j = 0; j < pde.N; j++)
      {
        SANS_CHECK_CLOSE( kxx_x(i,j), kxx_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kxy_x(i,j), kxy_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyx_x(i,j), kyx_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kxy_y(i,j), kxy_yTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyx_y(i,j), kyx_yTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyy_y(i,j), kyy_yTrue(i,j), small_tol, ping_tol );
      }

  }

  // case 2: prod/wall dominant; S~ branch 2
  if (verbose) cout << "source: case2" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;


  //PING TEST FOR DERIVATIVE OF K
  {
    //rough ping test
    Real dx = 1e-6;
    Real dy = 1e-6;

    //x derivative
    ArrayQ q1x = q + qx*dx;
    ArrayQ q0x = q - qx*dx;
    ArrayQ qx1x = qx+qxx*dx;
    ArrayQ qx0x = qx-qxx*dx;
    ArrayQ qy1x = qy+qxy*dx;
    ArrayQ qy0x = qy-qxy*dx;

    MatrixQ kxx0x = 0, kxy0x = 0, kyx0x = 0, kyy0x = 0;
    MatrixQ kxx1x = 0, kxy1x = 0, kyx1x = 0, kyy1x = 0;

    pde.diffusionViscous( x, y, time, q0x, qx0x, qy0x, kxx0x, kxy0x, kyx0x, kyy0x );
    pde.diffusionViscous( x, y, time, q1x, qx1x, qy1x, kxx1x, kxy1x, kyx1x, kyy1x );

    //y derivative
    ArrayQ q1y = q + qy*dy;
    ArrayQ q0y = q - qy*dy;
    ArrayQ qx1y = qx+qxy*dy;
    ArrayQ qx0y = qx-qxy*dy;
    ArrayQ qy1y = qy+qyy*dy;
    ArrayQ qy0y = qy-qyy*dy;

    MatrixQ kxx0y = 0, kxy0y = 0, kyx0y = 0, kyy0y = 0;
    MatrixQ kxx1y = 0, kxy1y = 0, kyx1y = 0, kyy1y = 0;

    pde.diffusionViscous( x, y, time, q0y, qx0y, qy0y, kxx0y, kxy0y, kyx0y, kyy0y );
    pde.diffusionViscous( x, y, time, q1y, qx1y, qy1y, kxx1y, kxy1y, kyx1y, kyy1y );

    MatrixQ kxx_xTrue = 0.5*(kxx1x - kxx0x)/dx;
    MatrixQ kxy_xTrue = 0.5*(kxy1x - kxy0x)/dx;
    MatrixQ kyx_xTrue = 0.5*(kyx1x - kyx0x)/dx;
    MatrixQ kxy_yTrue = 0.5*(kxy1y - kxy0y)/dy;
    MatrixQ kyx_yTrue = 0.5*(kyx1y - kyx0y)/dy;
    MatrixQ kyy_yTrue = 0.5*(kyy1y - kyy0y)/dy;

    MatrixQ kxx_x = 0, kxy_x = 0, kyx_x = 0, kxy_y = 0, kyx_y = 0, kyy_y = 0;
    pde.diffusionViscousGradient(dist, x, y, time, q, qx, qy, qxx, qxy, qyy, kxx_x, kxy_x, kyx_x, kxy_y, kyx_y, kyy_y);

    for (int i = 0; i < pde.N; i++)
      for (int j = 0; j < pde.N; j++)
      {
        SANS_CHECK_CLOSE( kxx_x(i,j), kxx_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kxy_x(i,j), kxy_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyx_x(i,j), kyx_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kxy_y(i,j), kxy_yTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyx_y(i,j), kyx_yTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyy_y(i,j), kyy_yTrue(i,j), small_tol, ping_tol );
      }

  }

  // case 3: prod/wall ft2 terms dominant
  if (verbose) cout << "source: case3" << endl;

  chi  = 1.241;
  chix = 0; chiy = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;


  //PING TEST FOR DERIVATIVE OF K
  {

    //rough ping test
    Real dx = 1e-6;
    Real dy = 1e-6;

    //x derivative
    ArrayQ q1x = q + qx*dx;
    ArrayQ q0x = q - qx*dx;
    ArrayQ qx1x = qx+qxx*dx;
    ArrayQ qx0x = qx-qxx*dx;
    ArrayQ qy1x = qy+qxy*dx;
    ArrayQ qy0x = qy-qxy*dx;

    MatrixQ kxx0x = 0, kxy0x = 0, kyx0x = 0, kyy0x = 0;
    MatrixQ kxx1x = 0, kxy1x = 0, kyx1x = 0, kyy1x = 0;

    pde.diffusionViscous( x, y, time, q0x, qx0x, qy0x, kxx0x, kxy0x, kyx0x, kyy0x );
    pde.diffusionViscous( x, y, time, q1x, qx1x, qy1x, kxx1x, kxy1x, kyx1x, kyy1x );

    //y derivative
    ArrayQ q1y = q + qy*dy;
    ArrayQ q0y = q - qy*dy;
    ArrayQ qx1y = qx+qxy*dy;
    ArrayQ qx0y = qx-qxy*dy;
    ArrayQ qy1y = qy+qyy*dy;
    ArrayQ qy0y = qy-qyy*dy;

    MatrixQ kxx0y = 0, kxy0y = 0, kyx0y = 0, kyy0y = 0;
    MatrixQ kxx1y = 0, kxy1y = 0, kyx1y = 0, kyy1y = 0;

    pde.diffusionViscous( x, y, time, q0y, qx0y, qy0y, kxx0y, kxy0y, kyx0y, kyy0y );
    pde.diffusionViscous( x, y, time, q1y, qx1y, qy1y, kxx1y, kxy1y, kyx1y, kyy1y );

    MatrixQ kxx_xTrue = 0.5*(kxx1x - kxx0x)/dx;
    MatrixQ kxy_xTrue = 0.5*(kxy1x - kxy0x)/dx;
    MatrixQ kyx_xTrue = 0.5*(kyx1x - kyx0x)/dx;
    MatrixQ kxy_yTrue = 0.5*(kxy1y - kxy0y)/dy;
    MatrixQ kyx_yTrue = 0.5*(kyx1y - kyx0y)/dy;
    MatrixQ kyy_yTrue = 0.5*(kyy1y - kyy0y)/dy;

    MatrixQ kxx_x = 0, kxy_x = 0, kyx_x = 0, kxy_y = 0, kyx_y = 0, kyy_y = 0;
    pde.diffusionViscousGradient(dist, x, y, time, q, qx, qy, qxx, qxy, qyy, kxx_x, kxy_x, kyx_x, kxy_y, kyx_y, kyy_y);

    for (int i = 0; i < pde.N; i++)
      for (int j = 0; j < pde.N; j++)
      {
        SANS_CHECK_CLOSE( kxx_x(i,j), kxx_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kxy_x(i,j), kxy_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyx_x(i,j), kyx_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kxy_y(i,j), kxy_yTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyx_y(i,j), kyx_yTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyy_y(i,j), kyy_yTrue(i,j), small_tol, ping_tol );
      }


  }


  // case 4: nonconservative diffusion dominant
  if (verbose) cout << "source: case4" << endl;

  chi  = 5.241;
  chix = 1.963; chiy = -0.781;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;

  //PING TEST FOR DERIV OF K
  {

    //rough ping test
    Real dx = 1e-6;
    Real dy = 1e-6;

    //x derivative
    ArrayQ q1x = q + qx*dx;
    ArrayQ q0x = q - qx*dx;
    ArrayQ qx1x = qx+qxx*dx;
    ArrayQ qx0x = qx-qxx*dx;
    ArrayQ qy1x = qy+qxy*dx;
    ArrayQ qy0x = qy-qxy*dx;

    MatrixQ kxx0x = 0, kxy0x = 0, kyx0x = 0, kyy0x = 0;
    MatrixQ kxx1x = 0, kxy1x = 0, kyx1x = 0, kyy1x = 0;

    pde.diffusionViscous( x, y, time, q0x, qx0x, qy0x, kxx0x, kxy0x, kyx0x, kyy0x );
    pde.diffusionViscous( x, y, time, q1x, qx1x, qy1x, kxx1x, kxy1x, kyx1x, kyy1x );

    //y derivative
    ArrayQ q1y = q + qy*dy;
    ArrayQ q0y = q - qy*dy;
    ArrayQ qx1y = qx+qxy*dy;
    ArrayQ qx0y = qx-qxy*dy;
    ArrayQ qy1y = qy+qyy*dy;
    ArrayQ qy0y = qy-qyy*dy;

    MatrixQ kxx0y = 0, kxy0y = 0, kyx0y = 0, kyy0y = 0;
    MatrixQ kxx1y = 0, kxy1y = 0, kyx1y = 0, kyy1y = 0;

    pde.diffusionViscous( x, y, time, q0y, qx0y, qy0y, kxx0y, kxy0y, kyx0y, kyy0y );
    pde.diffusionViscous( x, y, time, q1y, qx1y, qy1y, kxx1y, kxy1y, kyx1y, kyy1y );

    MatrixQ kxx_xTrue = 0.5*(kxx1x - kxx0x)/dx;
    MatrixQ kxy_xTrue = 0.5*(kxy1x - kxy0x)/dx;
    MatrixQ kyx_xTrue = 0.5*(kyx1x - kyx0x)/dx;
    MatrixQ kxy_yTrue = 0.5*(kxy1y - kxy0y)/dy;
    MatrixQ kyx_yTrue = 0.5*(kyx1y - kyx0y)/dy;
    MatrixQ kyy_yTrue = 0.5*(kyy1y - kyy0y)/dy;

    MatrixQ kxx_x = 0, kxy_x = 0, kyx_x = 0, kxy_y = 0, kyx_y = 0, kyy_y = 0;
    pde.diffusionViscousGradient(dist, x, y, time, q, qx, qy, qxx, qxy, qyy, kxx_x, kxy_x, kyx_x, kxy_y, kyx_y, kyy_y);

    for (int i = 0; i < pde.N; i++)
      for (int j = 0; j < pde.N; j++)
      {
        SANS_CHECK_CLOSE( kxx_x(i,j), kxx_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kxy_x(i,j), kxy_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyx_x(i,j), kyx_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kxy_y(i,j), kxy_yTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyx_y(i,j), kyx_yTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyy_y(i,j), kyy_yTrue(i,j), small_tol, ping_tol );
      }


  }


  // case 5: negative SA; prod/wall dominant
  if (verbose) cout << "source: case5" << endl;

  chi  = -5.241;
  chix = 0; chiy = 0;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;

  //PING TEST FOR DERIVATIVE OF K
  {

    //rough ping test
    Real dx = 1e-6;
    Real dy = 1e-6;

    //x derivative
    ArrayQ q1x = q + qx*dx;
    ArrayQ q0x = q - qx*dx;
    ArrayQ qx1x = qx+qxx*dx;
    ArrayQ qx0x = qx-qxx*dx;
    ArrayQ qy1x = qy+qxy*dx;
    ArrayQ qy0x = qy-qxy*dx;

    MatrixQ kxx0x = 0, kxy0x = 0, kyx0x = 0, kyy0x = 0;
    MatrixQ kxx1x = 0, kxy1x = 0, kyx1x = 0, kyy1x = 0;

    pde.diffusionViscous( x, y, time, q0x, qx0x, qy0x, kxx0x, kxy0x, kyx0x, kyy0x );
    pde.diffusionViscous( x, y, time, q1x, qx1x, qy1x, kxx1x, kxy1x, kyx1x, kyy1x );

    //y derivative
    ArrayQ q1y = q + qy*dy;
    ArrayQ q0y = q - qy*dy;
    ArrayQ qx1y = qx+qxy*dy;
    ArrayQ qx0y = qx-qxy*dy;
    ArrayQ qy1y = qy+qyy*dy;
    ArrayQ qy0y = qy-qyy*dy;

    MatrixQ kxx0y = 0, kxy0y = 0, kyx0y = 0, kyy0y = 0;
    MatrixQ kxx1y = 0, kxy1y = 0, kyx1y = 0, kyy1y = 0;

    pde.diffusionViscous( x, y, time, q0y, qx0y, qy0y, kxx0y, kxy0y, kyx0y, kyy0y );
    pde.diffusionViscous( x, y, time, q1y, qx1y, qy1y, kxx1y, kxy1y, kyx1y, kyy1y );

    MatrixQ kxx_xTrue = 0.5*(kxx1x - kxx0x)/dx;
    MatrixQ kxy_xTrue = 0.5*(kxy1x - kxy0x)/dx;
    MatrixQ kyx_xTrue = 0.5*(kyx1x - kyx0x)/dx;
    MatrixQ kxy_yTrue = 0.5*(kxy1y - kxy0y)/dy;
    MatrixQ kyx_yTrue = 0.5*(kyx1y - kyx0y)/dy;
    MatrixQ kyy_yTrue = 0.5*(kyy1y - kyy0y)/dy;

    MatrixQ kxx_x = 0, kxy_x = 0, kyx_x = 0, kxy_y = 0, kyx_y = 0, kyy_y = 0;
    pde.diffusionViscousGradient(dist, x, y, time, q, qx, qy, qxx, qxy, qyy, kxx_x, kxy_x, kyx_x, kxy_y, kyx_y, kyy_y);

    for (int i = 0; i < pde.N; i++)
      for (int j = 0; j < pde.N; j++)
      {
        SANS_CHECK_CLOSE( kxx_x(i,j), kxx_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kxy_x(i,j), kxy_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyx_x(i,j), kyx_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kxy_y(i,j), kxy_yTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyx_y(i,j), kyx_yTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyy_y(i,j), kyy_yTrue(i,j), small_tol, ping_tol );
      }


  }

  // case 6: negative SA; nonconservative diffusion dominant
  if (verbose) cout << "source: case6" << endl;

  chi  = -5.241;
  chix = 1.963; chiy = -0.781;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;


  //PING TEST FOR DERIVATIVE OF K
  {

    //rough ping test
    Real dx = 1e-6;
    Real dy = 1e-6;

    //x derivative
    ArrayQ q1x = q + qx*dx;
    ArrayQ q0x = q - qx*dx;
    ArrayQ qx1x = qx+qxx*dx;
    ArrayQ qx0x = qx-qxx*dx;
    ArrayQ qy1x = qy+qxy*dx;
    ArrayQ qy0x = qy-qxy*dx;

    MatrixQ kxx0x = 0, kxy0x = 0, kyx0x = 0, kyy0x = 0;
    MatrixQ kxx1x = 0, kxy1x = 0, kyx1x = 0, kyy1x = 0;

    pde.diffusionViscous( x, y, time, q0x, qx0x, qy0x, kxx0x, kxy0x, kyx0x, kyy0x );
    pde.diffusionViscous( x, y, time, q1x, qx1x, qy1x, kxx1x, kxy1x, kyx1x, kyy1x );

    //y derivative
    ArrayQ q1y = q + qy*dy;
    ArrayQ q0y = q - qy*dy;
    ArrayQ qx1y = qx+qxy*dy;
    ArrayQ qx0y = qx-qxy*dy;
    ArrayQ qy1y = qy+qyy*dy;
    ArrayQ qy0y = qy-qyy*dy;

    MatrixQ kxx0y = 0, kxy0y = 0, kyx0y = 0, kyy0y = 0;
    MatrixQ kxx1y = 0, kxy1y = 0, kyx1y = 0, kyy1y = 0;

    pde.diffusionViscous( x, y, time, q0y, qx0y, qy0y, kxx0y, kxy0y, kyx0y, kyy0y );
    pde.diffusionViscous( x, y, time, q1y, qx1y, qy1y, kxx1y, kxy1y, kyx1y, kyy1y );

    MatrixQ kxx_xTrue = 0.5*(kxx1x - kxx0x)/dx;
    MatrixQ kxy_xTrue = 0.5*(kxy1x - kxy0x)/dx;
    MatrixQ kyx_xTrue = 0.5*(kyx1x - kyx0x)/dx;
    MatrixQ kxy_yTrue = 0.5*(kxy1y - kxy0y)/dy;
    MatrixQ kyx_yTrue = 0.5*(kyx1y - kyx0y)/dy;
    MatrixQ kyy_yTrue = 0.5*(kyy1y - kyy0y)/dy;

    MatrixQ kxx_x = 0, kxy_x = 0, kyx_x = 0, kxy_y = 0, kyx_y = 0, kyy_y = 0;
    pde.diffusionViscousGradient(dist, x, y, time, q, qx, qy, qxx, qxy, qyy, kxx_x, kxy_x, kyx_x, kxy_y, kyx_y, kyy_y);

    for (int i = 0; i < pde.N; i++)
      for (int j = 0; j < pde.N; j++)
      {
        SANS_CHECK_CLOSE( kxx_x(i,j), kxx_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kxy_x(i,j), kxy_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyx_x(i,j), kyx_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kxy_y(i,j), kxy_yTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyx_y(i,j), kyx_yTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyy_y(i,j), kyy_yTrue(i,j), small_tol, ping_tol );
      }


  }

  // case 7: negative SA; shrmod < -cv2_*shr0
  if (verbose) cout << "source: case7" << endl;

  chi  = 10.241;
  chix = 1.963; chiy = -0.781;
  dist = 0.1;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;


  //PING TEST FOR DERIVATIVE OF K
  {

    //rough ping test
    Real dx = 1e-6;
    Real dy = 1e-6;

    //x derivative
    ArrayQ q1x = q + qx*dx;
    ArrayQ q0x = q - qx*dx;
    ArrayQ qx1x = qx+qxx*dx;
    ArrayQ qx0x = qx-qxx*dx;
    ArrayQ qy1x = qy+qxy*dx;
    ArrayQ qy0x = qy-qxy*dx;

    MatrixQ kxx0x = 0, kxy0x = 0, kyx0x = 0, kyy0x = 0;
    MatrixQ kxx1x = 0, kxy1x = 0, kyx1x = 0, kyy1x = 0;

    pde.diffusionViscous( x, y, time, q0x, qx0x, qy0x, kxx0x, kxy0x, kyx0x, kyy0x );
    pde.diffusionViscous( x, y, time, q1x, qx1x, qy1x, kxx1x, kxy1x, kyx1x, kyy1x );

    //y derivative
    ArrayQ q1y = q + qy*dy;
    ArrayQ q0y = q - qy*dy;
    ArrayQ qx1y = qx+qxy*dy;
    ArrayQ qx0y = qx-qxy*dy;
    ArrayQ qy1y = qy+qyy*dy;
    ArrayQ qy0y = qy-qyy*dy;

    MatrixQ kxx0y = 0, kxy0y = 0, kyx0y = 0, kyy0y = 0;
    MatrixQ kxx1y = 0, kxy1y = 0, kyx1y = 0, kyy1y = 0;

    pde.diffusionViscous( x, y, time, q0y, qx0y, qy0y, kxx0y, kxy0y, kyx0y, kyy0y );
    pde.diffusionViscous( x, y, time, q1y, qx1y, qy1y, kxx1y, kxy1y, kyx1y, kyy1y );

    MatrixQ kxx_xTrue = 0.5*(kxx1x - kxx0x)/dx;
    MatrixQ kxy_xTrue = 0.5*(kxy1x - kxy0x)/dx;
    MatrixQ kyx_xTrue = 0.5*(kyx1x - kyx0x)/dx;
    MatrixQ kxy_yTrue = 0.5*(kxy1y - kxy0y)/dy;
    MatrixQ kyx_yTrue = 0.5*(kyx1y - kyx0y)/dy;
    MatrixQ kyy_yTrue = 0.5*(kyy1y - kyy0y)/dy;

    MatrixQ kxx_x = 0, kxy_x = 0, kyx_x = 0, kxy_y = 0, kyx_y = 0, kyy_y = 0;
    pde.diffusionViscousGradient(dist, x, y, time, q, qx, qy, qxx, qxy, qyy, kxx_x, kxy_x, kyx_x, kxy_y, kyx_y, kyy_y);

    for (int i = 0; i < pde.N; i++)
      for (int j = 0; j < pde.N; j++)
      {
        SANS_CHECK_CLOSE( kxx_x(i,j), kxx_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kxy_x(i,j), kxy_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyx_x(i,j), kyx_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kxy_y(i,j), kxy_yTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyx_y(i,j), kyx_yTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyy_y(i,j), kyy_yTrue(i,j), small_tol, ping_tol );
      }


  }


  // case 8: shr0 == 0
  if (verbose) cout << "source: case7" << endl;

  chi  = 5.241;
  chix = 1.963; chiy = -0.781;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = rho*nt;

  qxS[1].value() = qx[1] = 0;
  qyS[1].value() = qy[1] = 0;

  qxS[2].value() = qx[2] = 0;
  qyS[2].value() = qy[2] = 0;

  qxS[4].value() = qx[4] = rhox*nt + rho*ntx;
  qyS[4].value() = qy[4] = rhoy*nt + rho*nty;

  //PING TEST FOR DERIVATIVE OF K
  {

    //rough ping test
    Real dx = 1e-6;
    Real dy = 1e-6;

    //x derivative
    ArrayQ q1x = q + qx*dx;
    ArrayQ q0x = q - qx*dx;
    ArrayQ qx1x = qx+qxx*dx;
    ArrayQ qx0x = qx-qxx*dx;
    ArrayQ qy1x = qy+qxy*dx;
    ArrayQ qy0x = qy-qxy*dx;

    MatrixQ kxx0x = 0, kxy0x = 0, kyx0x = 0, kyy0x = 0;
    MatrixQ kxx1x = 0, kxy1x = 0, kyx1x = 0, kyy1x = 0;

    pde.diffusionViscous( x, y, time, q0x, qx0x, qy0x, kxx0x, kxy0x, kyx0x, kyy0x );
    pde.diffusionViscous( x, y, time, q1x, qx1x, qy1x, kxx1x, kxy1x, kyx1x, kyy1x );

    //y derivative
    ArrayQ q1y = q + qy*dy;
    ArrayQ q0y = q - qy*dy;
    ArrayQ qx1y = qx+qxy*dy;
    ArrayQ qx0y = qx-qxy*dy;
    ArrayQ qy1y = qy+qyy*dy;
    ArrayQ qy0y = qy-qyy*dy;

    MatrixQ kxx0y = 0, kxy0y = 0, kyx0y = 0, kyy0y = 0;
    MatrixQ kxx1y = 0, kxy1y = 0, kyx1y = 0, kyy1y = 0;

    pde.diffusionViscous( x, y, time, q0y, qx0y, qy0y, kxx0y, kxy0y, kyx0y, kyy0y );
    pde.diffusionViscous( x, y, time, q1y, qx1y, qy1y, kxx1y, kxy1y, kyx1y, kyy1y );

    MatrixQ kxx_xTrue = 0.5*(kxx1x - kxx0x)/dx;
    MatrixQ kxy_xTrue = 0.5*(kxy1x - kxy0x)/dx;
    MatrixQ kyx_xTrue = 0.5*(kyx1x - kyx0x)/dx;
    MatrixQ kxy_yTrue = 0.5*(kxy1y - kxy0y)/dy;
    MatrixQ kyx_yTrue = 0.5*(kyx1y - kyx0y)/dy;
    MatrixQ kyy_yTrue = 0.5*(kyy1y - kyy0y)/dy;

    MatrixQ kxx_x = 0, kxy_x = 0, kyx_x = 0, kxy_y = 0, kyx_y = 0, kyy_y = 0;
    pde.diffusionViscousGradient(dist, x, y, time, q, qx, qy, qxx, qxy, qyy, kxx_x, kxy_x, kyx_x, kxy_y, kyx_y, kyy_y);

    for (int i = 0; i < pde.N; i++)
      for (int j = 0; j < pde.N; j++)
      {
        SANS_CHECK_CLOSE( kxx_x(i,j), kxx_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kxy_x(i,j), kxy_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyx_x(i,j), kyx_xTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kxy_y(i,j), kxy_yTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyx_y(i,j), kyx_yTrue(i,j), small_tol, ping_tol );
        SANS_CHECK_CLOSE( kyy_y(i,j), kyy_yTrue(i,j), small_tol, ping_tol );
      }


  }



}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxRoe )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real tol = 2.e-11;

  const Real gamma = 1.4;
  const Real R     = 0.4;           // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
//  const Real tSutherland = 110;     // K
//  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // Roe flux function test (same as Euler Roe test, from Roe2D.nb)

  Real x, y, time;
  Real nx, ny;
  Real rhoL, uL, vL, tL, pL, h0L, ntL;
  Real rhoR, uR, vR, tR, pR, h0R, ntR;

  x = y = time = 0;   // not actually used in functions
  nx = 2.0/7.0; ny = sqrt(1.0-nx*nx);
  rhoL = 1.034; uL = 3.26; vL = -2.17; tL = 5.78; ntL = 4.2;
  rhoR = 0.973; uR = 1.79; vR =  0.93; tR = 6.13; ntR = 5.7;
  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL + vL*vL);
  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR + vR*vR);

  // set
  ArrayQ qL = 0;
  ArrayQ qR = 0;

  pde.setDOFFrom( qL, SAnt2D<DensityVelocityTemperature2D<Real>>({rhoL, uL, vL, tL, ntL}) );
  pde.setDOFFrom( qR, SAnt2D<DensityVelocityTemperature2D<Real>>({rhoR, uR, vR, tR, ntR}) );

  // advective normal flux (average)
  Real fL[5] = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*vL     , rhoL*uL*h0L, rhoL*uL*ntL};
  Real fR[5] = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*vR     , rhoR*uR*h0R, rhoR*uR*ntR};
  Real gL[5] = {rhoL*vL, rhoL*vL*uL     , rhoL*vL*vL + pL, rhoL*vL*h0L, rhoL*vL*ntL };
  Real gR[5] = {rhoR*vR, rhoR*vR*uR     , rhoR*vR*vR + pR, rhoR*vR*h0R, rhoR*vR*ntR };

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < 5; k++)
    fnTrue[k] = 0.5*(nx*(fL[k] + fR[k]) + ny*(gL[k] + gR[k]));

#if 1
  // Exact values for Roe scheme from Roe2D.nb
  fnTrue[0] -= -0.021574952725013058474459426733768;
  fnTrue[1] -= -1.7958753728220592172800048653566;
  fnTrue[2] -=  4.4515996249161671139371535471951;
  fnTrue[3] -= -6.8094427666218353919034995028459;
  fnTrue[4] -= 1.8131990192972219759746035955243;
#endif

  ArrayQ fn;
  fn = 0;
  pde.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( fnTrue(3), fn(3), tol );
  BOOST_CHECK_CLOSE( fnTrue(4), fn(4), tol );

  MatrixQ mtx;
  mtx = 0;

  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, time, qL, nx, ny, mtx );
  MatrixQ mtxTrue = {
    {1.0323434331949190979834072919713,
        -0.44459540501697074517797177104962,
        -0.43697991171460424043588329213258,
        0.080449362812368834954522463171426,
        0.0},
    {-0.72312835498151845646006425483631,
        -0.010364873336226558791267034135421,
        -1.4045718375886762151895973056639,
        0.18933272986886306466125181349454,
        0.0},
    {-0.90833712876943321732424020657867,
        1.9404853199183346620104522800780,
        2.1633849842368432379888624467138,
        -0.41919712890981821676619255996783,
         0.0},
    {-0.43536491993382434782224675355112,
        -8.1758936855015456959218531030353,
        -6.9672110585297722485152708900077,
        2.7090875342447734040794006013828,
        0.0},
    { -0.4862391006955183426,
      -1.867300701071277129747,
      -1.8353156292013378,
      0.337887323811949106,
      1.1481146476462329890891029404915}
                    };
  // Values taken from pde/NS/Roe2D.nb

  for (int i = 0; i < pde.N; i++)
    for (int j=0; j<5; j++)
      BOOST_CHECK_CLOSE( mtx(i,j), mtxTrue(i,j), tol );


  ArrayQ dq = 0;
  dq[0] = 0.15;
  dq[1] = 0.24;
  dq[2] = 0.33;
  dq[3] = 0.42;
  dq[4] = 0.51;


  MatrixQ dudq = 0;
  pde.jacobianMasterState(x, y, time, qL, dudq);

  ArrayQ duTrue = dudq*dq;

  ArrayQ du = 0;

  pde.variableInterpreter().conservativePerturbation(qL, dq, du);

  for (int i = 0; i < pde.N; i++)
    BOOST_CHECK_CLOSE( du[i], duTrue[i], tol );

  ArrayQ FlinTrue = 0;
  ArrayQ fl = 0, gl = 0;
  pde.fluxAdvective(x, y, time, qL, fl, gl );
  FlinTrue = fl*nx + gl*ny + mtx*du;

  ArrayQ Flin = 0;
  pde.fluxAdvectiveUpwindLinear(x, y, time, qL, dq, nx, ny, Flin);
  for (int i = 0; i < pde.N; i++)
  {
    BOOST_CHECK_CLOSE( Flin[i], FlinTrue[i], tol );
  }

  // Call these simply to make sure coverage is high
//  ArrayQ qxL, qxR, qyL, qyR, sourceL, sourceR;
//  Real dummy = 0;
//  pde.sourceDualConsistent( dummy, dummy, dummy, qL, qxL, qyL, qR, qxR, qyR, sourceL, sourceR );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real x, y, time, dx, dy;
  Real rho, u, v, t, c, nt;
  Real speed, speedTrue;
  Real dist;

  x = 0; y = 0; time = 0;  // not actually used in functions
  dist = 0;
  dx = 0.57; dy = -0.43;
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987; nt = 5.241;
  c = sqrt(gamma*R*t);
  speedTrue = fabs(dx*u + dy*v)/sqrt(dx*dx + dy*dy) + c;

  Real qDataPrim[5] = {rho, u, v, t, nt};
  string qNamePrim[5] = {"Density", "VelocityX", "VelocityY", "Temperature", "SANutilde"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );

  pde.speedCharacteristic( dist, x, y, time, dx, dy, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  speedTrue = sqrt(u*u + v*v) + c;
  pde.speedCharacteristic( dist, x, y, time, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  ArrayQ q;
  q(0) =  1;
  q(3) =  1;
  BOOST_CHECK( pde.isValidState(q) == true );

  q(0) =  1;
  q(3) = -1;
  BOOST_CHECK( pde.isValidState(q) == false );

  q(0) = -1;
  q(3) =  1;
  BOOST_CHECK( pde.isValidState(q) == false );

  q(0) = -1;
  q(3) = -1;
  BOOST_CHECK( pde.isValidState(q) == false );

  q(0) =  1;
  q(3) =  0;
  BOOST_CHECK( pde.isValidState(q) == false );

  q(0) =  0;
  q(3) =  1;
  BOOST_CHECK( pde.isValidState(q) == false );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EntropyVariables )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real close_tol = 1.e-10;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real tSuth = 110./300.; //somewhat random...
  const Real tRef  = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real ntref = 6;

  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw, eVanLeer, ntref );

  Real x, y, time;
  Real rho, u, v, p, nt;
  Real rhox, ux, vx, px, ntx;
  Real rhoy, uy, vy, py, nty;
  Real rhoxx, uxx, vxx, pxx, ntxx;
  Real rhoxy, uxy, vxy, pxy, ntxy;
  Real rhoyy, uyy, vyy, pyy, ntyy;

  x = 0.73; y = 36./99.; time = 0;   // not actually used in functions

  rho = 1.0074365505202103681442524417731;
  u = 0.10508967589710564917176487424421;
  v = 0.024335124272671981161838412026241;
  p = 1.0005144026446280991735537190083;
  nt = 5.0876529752066115702479338842975;

  rhox = 0.010748222028549962434259954921112;
  ux = 0.016369696969696969696969696969697;
  vx = 0.013539393939393939393939393939394;
  px = 0.0021139834710743801652892561983471;
  ntx = 0.20391735537190082644628099173554;

  rhoy = 0.0011265417917355371900826446280992;
  uy = 0.003044338055883510428964974419520;
  vy = 0.018728366999391792780222532288648;
  py = 0.0028292145454545454545454545454545;
  nty = 0.18899636363636363636363636363636;

  rhoxx = 0.0030748850488354620586025544703231;
  uxx = 0.022424242424242424242424242424242;
  vxx = 0.0048484848484848484848484848484848;
  pxx = 0.0057917355371900826446280991735537;
  ntxx = 0.27933884297520661157024793388430;

  rhoxy = 0.0061728317355371900826446280991736;
  uxy = 0.0048666666666666666666666666666667;
  vxy = 0.0097333333333333333333333333333333;
  pxy = 0.011626909090909090909090909090909;
  ntxy = 0.31854545454545454545454545454545;

  rhoyy = 0.0061959798545454545454545454545455;
  uyy = 0.030545454545454545454545454545455;
  vyy = -0.0068004722550177095631641086186541;
  pyy = 0.0077803400000000000000000000000000;
  ntyy = 0.5197400000000000000000000000000;

  Real E = p/rho/(gamma-1) + 0.5*(u*u + v*v);

  Real ntr = nt/ntref;

  // set
  ArrayQ q;
  pde.setDOFFrom( q, SAnt2D<DensityVelocityPressure2D<Real>>({rho, u, v, p, ntr }) );

  ArrayQ qx = {rhox, ux, vx, px, ntx/ntref};
  ArrayQ qy = {rhoy, uy, vy, py, nty/ntref};
  ArrayQ qxx = {rhoxx, uxx, vxx, pxx, ntxx/ntref};
  ArrayQ qxy = {rhoxy, uxy, vxy, pxy, ntxy/ntref};
  ArrayQ qyy = {rhoyy, uyy, vyy, pyy, ntyy/ntref};

  Real s = log(p/ pow(rho, gamma));
  ArrayQ VTrue;
  VTrue[0] = -s/(gamma-1) + (gamma+1)/(gamma-1) - rho*E/p - 0.5*(nt*nt)/(ntref*ntref);
  VTrue[1] = rho*u/p;
  VTrue[2] = rho*v/p;
  VTrue[3] = -rho/p;
  VTrue[4] = nt/ntref;

  ArrayQ V = 0;

  pde.adjointState(x,y,time,q, V);

  for (int i=0; i< 5; i++)
  {
    BOOST_CHECK_CLOSE( VTrue(i), V(i), close_tol );
  }


  ArrayQ Vx = 0, Vy = 0;
  ArrayQ VxTrue = {0.0011264190695911670496036758263,
                   0.01738831876391961693945354071885,
                   0.01384271894326165714695068342078,
                   -0.00861518110288027304467156321286,
                   0.03398622589531680440771349862259};
  ArrayQ VyTrue = {-0.03063631026998702831574831533439,
            0.00288450326224433526322168322838,
            0.01881605132720310581660691085446,
            0.00172136145328438821184671869356,
            0.03149939393939393939393939393939};

  pde.adjointStateGradient(x,y,time,q, qx,qy, Vx,Vy);

  for (int i=0; i< 5; i++)
  {
    BOOST_CHECK_CLOSE( VxTrue(i), Vx(i), close_tol );
    BOOST_CHECK_CLOSE( VyTrue(i), Vy(i), close_tol );
  }


  ArrayQ Vxx = 0, Vxy = 0, Vyy =0;
  ArrayQ VxxTrue = {-0.0477738708848351414252564039753,
                    0.02256804146634102169844825845111,
                    0.00504737671517475066591427601033,
                    0.00279190966094791143931392330541,
                    0.04655647382920110192837465564738};

  ArrayQ VxyTrue = {-0.0547521122958200660174779196774,
                    0.00431488675779504174585423188804,
                    0.00980359804665719780912072801758,
                    0.00555239825763835616923378590762,
                    0.05309090909090909090909090909091};

  ArrayQ VyyTrue = {-0.0757591284956610857327558632850,
                    0.03057525999402143057665438499304,
                    -0.00695160663744885222473264493321,
                    0.00162761167021932648144241415658,
                    0.0866233333333333333333333333333};

  pde.adjointStateHessian(x,y,time,q, qx,qy, qxx, qxy, qyy, Vxx,Vxy, Vyy);
  for (int i=0; i< 5; i++)
  {
    BOOST_CHECK_CLOSE( VxxTrue(i), Vxx(i), close_tol );
    BOOST_CHECK_CLOSE( VxyTrue(i), Vxy(i), close_tol );
    BOOST_CHECK_CLOSE( VyyTrue(i), Vyy(i), close_tol );
  }

  MatrixQ dudv = 0;
  MatrixQ dvdu = 0;

  MatrixQ dudvTrue = {{1.0074365505202103681442524418,
                       0.10587118058106700917370666456,
                       0.024516093653741303940070306230,
                       2.5071472917314965163605164140,
                       0.8542479272643390207905156338},
                      {0.10587118058106700917370666456,
                       1.0116403706987363765045998373,
                       0.0025763883363347622775335854515,
                       0.36861903061867938501420137485,
                       0.08977263781198366812680297064},
                       {0.024516093653741303940070306230,
                        0.0025763883363347622775335854515,
                        1.0011110048303723586957718336,
                        0.08535938323913415860950277947,
                        0.020788229469650145579537135451},
                        {2.5071472917314965163605164140,
                         0.36861903061867938501420137485,
                         0.08535938323913415860950277947,
                         8.735129585085830939428697955,
                         2.1259158963431577985752027977},
                         {0.8542479272643390207905156338,
                          0.08977263781198366812680297064,
                          0.020788229469650145579537135451,
                          2.1259158963431577985752027977,
                          1.7317893853052929575053493772}};

  MatrixQ dvduTrue = {{4.18787628517498031385809596961,
                       -0.000246130935500412695144687969218,
                       -0.0000569953884777041114224275665981,
                       -0.997143757970018867861862717410,
                       -0.841682944793479119986499103812},
                      {-0.000246130935500412695144687969218,
                       1.003931673999567450604796591980,
                       0.001029495911280116030235267075209,
                       -0.0423049374946577182281547537622,
                       0},
                       {-0.0000569953884777041114224275665981,
                        0.001029495911280116030235267075209,
                        0.999724257378021047054047510979,
                        -0.00979635632607817493784218488125,
                        0},
                        {-0.997143757970018867861862717410,
                         -0.0423049374946577182281547537622,
                         -0.00979635632607817493784218488125,
                         0.402560357461554119534478754773,
                         0},
                         {-0.841682944793479119986499103812, 0, 0, 0,
                          0.9926183435409700452895161783225}};

  pde.adjointVariableJacobian(x,y,time,q,dudv);
  pde.adjointVariableJacobianInverse(x,y,time,q,dvdu);

  for (int i=0; i< 5; i++)
    for (int j=0; j< 5; j++)
    {
      BOOST_CHECK_CLOSE( dudvTrue(i,j), dudv(i,j), close_tol );
      BOOST_CHECK_CLOSE( dvduTrue(i,j), dvdu(i,j), close_tol );
    }

//  MatrixQ dudvx = 0, dudvy = 0;
//  MatrixQ dudvxTrue =  {{0.010748222028549962434259954921, 0.017620958217663102757828776295,0.013901639645212306370126665335,
//                         0.00741250423279595716738216219,0.04335283681146751939861700124},
//                        {0.017620958217663102757828776295,0.00569884340310116933799042026,0.0018622398287461721738897756405,
//                         0.05842049451438848130141414056,0.018539715276046200547271360165},
//                        {0.013901639645212306370126665335,0.0018622398287461721738897756405,0.00278421464926760450090613552,
//                          0.047723441748436144342538258696,0.012620995880522557108579276002},
//                        {0.00741250423279595716738216219,0.05842049451438848130141414056,0.047723441748436144342538258696,
//                         -0.04155221147550984528253310856,0.09149384907857104046917688293},
//                       {0.04335283681146751939861700124,0.018539715276046200547271360165,0.012620995880522557108579276002,
//                        0.09149384907857104046917688293,0.07654158325308018676010760561}};
//
//  MatrixQ dudvyTrue =  {{0.001126541791735537190082644628,0.0031853653414147194639024432071,0.018895055981244053363257977945,
//                         0.00786104466873076314204060026,0.032688883056849899505953600619},
//                       {0.0031853653414147194639024432071,0.003486271220861949206384984981,0.0020603105860182939949488630168,
//                        0.011801943859428852741100959627,0.006035883600013180396352318696},
//                       {0.018895055981244053363257977945,0.0020603105860182939949488630168,0.003748174480235940714059851895,
//                        0.065952924308468788873141235643,0.016794156721799571295887133066},
//                       {0.00786104466873076314204060026,0.011801943859428852741100959627,0.065952924308468788873141235643,
//                        0.04500576004867864738582821142,0.08563933142251838682687070031},
//                       {0.032688883056849899505953600619,0.006035883600013180396352318696,0.016794156721799571295887133066,
//                        0.08563933142251838682687070031,0.05575311596460625697114355582}};
//
//
//  pde.adjointVariableJacobianGradient(x,y,time, q, qx, qy, dudvx, dudvy);
//  for (int i=0; i< 5; i++)
//    for (int j=0; j< 5; j++)
//    {
//      BOOST_CHECK_CLOSE( dudvxTrue(i,j), dudvx(i,j), close_tol );
//      BOOST_CHECK_CLOSE( dudvyTrue(i,j), dudvy(i,j), close_tol );
//    }


  MatrixQ dudq = 0;

  pde.jacobianMasterState(x,y,time,q,dudq);

  MatrixQ dudqTrue = {{1,0,0,0,0},
              {0.1050896758971056491717648742442,1.007436550520210368144252441773,0,0,0},
              {0.02433512427267198116183841202624,0,1.007436550520210368144252441773,0,0},
              {0.00581801912676254852459806917867,0.1058711805810670091737066645636,0.02451609365374130394007030622987,2.5,0},
              {0.8479421625344352617079889807163,0,0,0,1.007436550520210368144252441773}};

  for (int i=0; i< 5; i++)
    for (int j=0; j< 5; j++)
    {
      BOOST_CHECK_CLOSE( dudqTrue(i,j), dudq(i,j), close_tol );
    }


}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReferenceEquivalency )
{

  // Use conservative variables here in order to differentiate with Surreal
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  bool verbose = false;

  const Real small_tol = 1.e-10;
  const Real close_tol = 1.e-10;

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real muRef = 1;
  const Real tSuth = 110./300.; //somewhat random...
  const Real tRef  = 1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  const Real ntRef = 0.001;
  PDEClass pde2(gas, visc, tcond, Euler_ResidInterp_Raw, eVanLeer, ntRef );

  Real x, y, time;
  Real rho, u, v, t;

  x = 0; y = 0; time = 0;   // not actually used in functions
  rho = 1.225;
  u = 6.974;
  v = -3.231;
  t = 2.8815;

  Real p = gas.pressure(rho, t);
//  Real e = gas.energy(rho, t);
//  Real E = e + 0.5*(u*u + v*v + w*w);

  Real rhox = 0.0421, rhoy = 0.0127;
  Real ux = 0.0521, uy = -0.0124;
  Real vx = -0.0312, vy = -0.0022;
  Real tx = -0.0715, ty = -0.0111;

  Real px = R*(rho*tx + rhox*t);
  Real py = R*(rho*ty + rhoy*t);

  Real nt, ntx, nty;
  Real chi, chix, chiy;
  Real dist;

  ArrayQ q, qx, qy;
  ArrayQ q2, qx2, qy2;

  // set the complete state vectors (sans nt)
  // the nt values are set for each case
  pde.setDOFFrom( q, SAnt2D<DensityVelocityPressure2D<Real>>({rho , u, v, p, 0     }));
  pde2.setDOFFrom( q2, SAnt2D<DensityVelocityPressure2D<Real>>({rho , u, v, p, 0     }));

  qx = qx2 = {rhox, ux, vx, px, 0};
  qy = qy2 = {rhoy, uy, vy, py, 0};

  // case 1: prod/wall dominant; S~ branch 1
  if (verbose) cout << "source: case1" << endl;

  chi  = 5.241;
  chix = 0.032; chiy = -0.071;
  dist = 60;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  q[4]  = nt;
  q2[4] = nt/ntRef;

  qx[4] = ntx;
  qy[4] = nty;

  qx2[4] = ntx/ntRef;
  qy2[4] = nty/ntRef;

  //perturbedMasterState
  ArrayQ dq, dq2;
  dq = dq2 = {0.01/7., -0.02/7., 0.03/7., 0.05/7., -0.06/7. };
  dq2[4] /= ntRef;
  ArrayQ dU = 0, dU2 = 0;

  pde.perturbedMasterState(dist, x, y, time, q, dq, dU);
  pde2.perturbedMasterState(dist, x, y, time, q2, dq2, dU2);

  for (int i = 0; i < 4; i++)
  {
    SANS_CHECK_CLOSE( dU[i], dU2[i], small_tol, close_tol );
  }
  SANS_CHECK_CLOSE( dU[4]/ntRef, dU2[4], small_tol, close_tol );

  //fluxes
  ArrayQ fv, gv, fv2, gv2;
  fv = fv2 = 0;
  gv = gv2 = 0;

  pde.fluxAdvective( dist, x, y, time, q, fv, gv );
  pde2.fluxAdvective( dist, x, y, time, q2, fv2, gv2 );

  pde.fluxViscous( dist, x, y, time, q, qx, qy, fv, gv );
  pde2.fluxViscous( dist, x, y, time, q2, qx2, qy2, fv2, gv2 );

  // check that the jacobian is correct
  for (int i = 0; i < 4; i++)
  {
    SANS_CHECK_CLOSE( fv[i], fv2[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( gv[i], gv2[i], small_tol, close_tol );
  }
  SANS_CHECK_CLOSE( fv[4]/ntRef, fv2[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( gv[4]/ntRef, gv2[4], small_tol, close_tol );

  ArrayQ s = 0, s2 = 0;

  //source
  pde.source(dist, x, y,time, q, qx, qy, s);
  pde2.source(dist, x, y, time, q2, qx2, qy2, s2);

  for (int i = 0; i < 4; i++)
  {
    SANS_CHECK_CLOSE( s[i], s2[i], small_tol, close_tol );
  }
  SANS_CHECK_CLOSE( s[4]/ntRef, s2[4], small_tol, close_tol );

  //jacobianFlux
  MatrixQ dfdu = 0, dgdu = 0;
  MatrixQ dfdu2 = 0, dgdu2 = 0;

  pde.jacobianFluxAdvective(dist, x, y, time, q, dfdu, dgdu);
  pde2.jacobianFluxAdvective(dist, x, y, time, q2, dfdu2, dgdu2);

  ArrayQ dF = dfdu*dU;
  ArrayQ dG = dgdu*dU;
  ArrayQ dF2 = dfdu2*dU2;
  ArrayQ dG2 = dgdu2*dU2;

  for (int i = 0; i < 4; i++)
  {
    SANS_CHECK_CLOSE( dF[i], dF2[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dG[i], dG2[i], small_tol, close_tol );
  }
  SANS_CHECK_CLOSE( dF[4]/ntRef, dF2[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( dG[4]/ntRef, dG2[4], small_tol, close_tol );

  pde.jacobianFluxViscous(dist, x, y, time, q, qx, qy, dfdu, dgdu);
  pde2.jacobianFluxViscous(dist, x, y, time, q2, qx2, qy2, dfdu2, dgdu2);

  dF = dfdu*dU;
  dG = dgdu*dU;
  dF2 = dfdu2*dU2;
  dG2 = dgdu2*dU2;

  for (int i = 0; i < 4; i++)
  {
    SANS_CHECK_CLOSE( dF[i], dF2[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( dG[i], dG2[i], small_tol, close_tol );
  }
  SANS_CHECK_CLOSE( dF[4]/ntRef, dF2[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( dG[4]/ntRef, dG2[4], small_tol, close_tol );

  MatrixQ dsdu = 0, dsdu2 = 0;
  ArrayQ du = 0, du2 = 0;

  pde.jacobianSource(dist, x, y, time, q, qx, qy, dsdu);
  pde2.jacobianSource(dist, x, y, time, q2, qx2, qy2, dsdu2);


  ArrayQ dS = dsdu*dU;
  ArrayQ dS2 = dsdu2*dU2;

  for (int i = 0; i < 4; i++)
  {
    SANS_CHECK_CLOSE( dS[i], dS2[i], small_tol, close_tol );
  }
  SANS_CHECK_CLOSE( dS[4]/ntRef, dS2[4], small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/PDERANSSA2D_pattern.txt", true  );

  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  pde.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
