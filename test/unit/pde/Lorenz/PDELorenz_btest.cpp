// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// -------------------------------------------------------------------------- //
// PDELorenz_btest
//
// test of 1D Burgers PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "Surreal/SurrealS.h"

#include "pde/Lorenz/PDELorenz.h"

using namespace std;

using namespace SANS;

// ########################################################################## //
BOOST_AUTO_TEST_SUITE(PDELorenz_test_suite)

// -------------------------------------------------------------------------- //
BOOST_AUTO_TEST_CASE(static_test)
{
  typedef PDELorenz PDEClass;

    BOOST_CHECK(PDEClass::D == 1);
    BOOST_CHECK(PDEClass::N == 3);
}

// -------------------------------------------------------------------------- //
BOOST_AUTO_TEST_CASE(constructors)
{
  typedef PDELorenz PDEClass;

  const Real alpha0= 0.0;
  const Real alpha1= 0.0;
  const Real alpha2= 0.0;

  PDEClass pde(alpha0, alpha1, alpha2);

  BOOST_CHECK(pde.D == 1);
  BOOST_CHECK(pde.N == 3);
}

// -------------------------------------------------------------------------- //
BOOST_AUTO_TEST_CASE(flux)
{
  typedef PDELorenz PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real tol= 1.e-13;

  const Real alpha0= 10.0;
  const Real alpha1= 8.0/3.0;
  const Real alpha2= 28.0;

  PDEClass pde(alpha0, alpha1, alpha2);

  // static tests
  BOOST_CHECK(pde.D == 1);
  BOOST_CHECK(pde.N == 3);

  // flux components
  BOOST_CHECK(pde.hasFluxAdvectiveTime() == false);
  BOOST_CHECK(pde.hasFluxAdvective() == true);
  BOOST_CHECK(pde.hasFluxViscous() == false);
  BOOST_CHECK(pde.hasSource() == true);
  BOOST_CHECK(pde.hasForcingFunction() == false);

  BOOST_CHECK(pde.fluxViscousLinearInGradient() == true); // it's zero so... ???
  BOOST_CHECK(pde.needsSolutionGradientforSource() == false);

  // function tests

  Real x;
  Real time;
  ArrayQ q;
  ArrayQ qx;
  ArrayQ qt;

  x= 0;
  time= 0;
  q=  {-4.731,  4.362, -0.890};
  qx= { 9.021, -8.975,  2.831};
  qt= { 8.608,  8.752, -3.673};

  // master state: identity
  ArrayQ q_MS;
  pde.masterState(x, time, q, q_MS);
  for (int i= 0; i < ArrayQ::M; i++)
    BOOST_CHECK_CLOSE(q(i), q_MS(i), tol);

  // should not accumulate
  pde.masterState(x, time, q, q_MS);
  for (int i= 0; i < ArrayQ::M; i++)
    BOOST_CHECK_CLOSE(q(i), q_MS(i), tol);

  // unsteady flux- should be zero
  ArrayQ ft= 0;
  pde.fluxAdvectiveTime(x, time, q, ft);
  for (int i= 0; i < ArrayQ::M; i++)
    BOOST_CHECK_CLOSE(ft(i), 0, tol);

  // master state jacobian: identity matrix
  MatrixQ dudq= 0;
  pde.jacobianMasterState(x, time, q, dudq);
  for (int i= 0; i < MatrixQ::M; i++)
    for (int j= 0; j < MatrixQ::N; j++)
      BOOST_CHECK_CLOSE(dudq(i, j), (i == j) ? 1.0 : 0.0, tol);

  // should not accumulate
  pde.jacobianMasterState(x, time, q, dudq);
  for (int i= 0; i < MatrixQ::M; i++)
    for (int j= 0; j < MatrixQ::N; j++)
      BOOST_CHECK_CLOSE(dudq(i, j), (i == j) ? 1.0 : 0.0, tol);

  // advective flux
  ArrayQ fAdv= 0;
  pde.fluxAdvective(x, time, q, fAdv);
  for (int i= 0; i < ArrayQ::M; i++)
    BOOST_CHECK_CLOSE(fAdv(i), q[i], tol);

  // advective flux accumulation
  pde.fluxAdvective(x, time, q, fAdv);
  for (int i= 0; i < ArrayQ::M; i++)
    BOOST_CHECK_CLOSE(fAdv(i), 2.0*q[i], tol);

  // advective flux jacobian: identity lol
  MatrixQ dfdu= 0.0;
  pde.jacobianFluxAdvective(x, time, q, dfdu);
  for (int i= 0; i < MatrixQ::M; i++)
    for (int j= 0; j < MatrixQ::N; j++)
      BOOST_CHECK_CLOSE(dfdu(i, j), (i == j) ? 1.0 : 0.0, tol);

  // and it should accumulate
  pde.jacobianFluxAdvective(x, time, q, dfdu);
  for (int i= 0; i < MatrixQ::M; i++)
    for (int j= 0; j < MatrixQ::N; j++)
      BOOST_CHECK_CLOSE(dfdu(i, j), (i == j) ? 2.0 : 0.0, tol);

  // source term
  ArrayQ src= 0;
  ArrayQ srcTrue= {-90.9300000000000068,
                    21.1885899999999978,
                   -4.2833780000000026};

  pde.source(x, time, q, qx, src);

  BOOST_CHECK_CLOSE(src(0), srcTrue(0), tol);
  BOOST_CHECK_CLOSE(src(1), srcTrue(1), tol);
  BOOST_CHECK_CLOSE(src(2), srcTrue(2), tol);

  // source term jacobian!
  MatrixQ srcJac= 0;
  MatrixQ jacTrue= 0;

  jacTrue(0, 0)= 10.0000000000000000;
  jacTrue(1, 0)= -3.5566666666666666;
  jacTrue(2, 0)= -4.3620000000000001;

  jacTrue(0, 1)= -10.0000000000000000;
  jacTrue(1, 1)=   1.0000000000000000;
  jacTrue(2, 1)=   4.7309999999999999;

  jacTrue(0, 2)=  0.0000000000000000;
  jacTrue(1, 2)= -4.7309999999999999;
  jacTrue(2, 2)= 28.0000000000000000;

  pde.jacobianSource(x, time, q, qx, srcJac);

  BOOST_CHECK_CLOSE(srcJac(0, 0), jacTrue(0, 0), tol);
  BOOST_CHECK_CLOSE(srcJac(1, 0), jacTrue(1, 0), tol);
  BOOST_CHECK_CLOSE(srcJac(2, 0), jacTrue(2, 0), tol);

  BOOST_CHECK_CLOSE(srcJac(0, 1), jacTrue(0, 1), tol);
  BOOST_CHECK_CLOSE(srcJac(1, 1), jacTrue(1, 1), tol);
  BOOST_CHECK_CLOSE(srcJac(2, 1), jacTrue(2, 1), tol);

  BOOST_CHECK_CLOSE(srcJac(0, 2), jacTrue(0, 2), tol);
  BOOST_CHECK_CLOSE(srcJac(1, 2), jacTrue(1, 2), tol);
  BOOST_CHECK_CLOSE(srcJac(2, 2), jacTrue(2, 2), tol);
}

// -------------------------------------------------------------------------- //
BOOST_AUTO_TEST_CASE(flux_normal)
{
  typedef PDELorenz PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol= 1.e-13;

  const Real alpha0= 10.0;
  const Real alpha1= 8.0/3.0;
  const Real alpha2= 28.0;

  PDEClass pde(alpha0, alpha1, alpha2);

  Real x;
  Real time;
  ArrayQ qL;
  ArrayQ qR;
  ArrayQ qx;
  ArrayQ qt;
  Real nx;

  x= 0;
  time= 0;
  qL= {-4.731,  4.362, -0.890};
  qR= {-4.741,  4.352, -0.895};
  qx= { 9.021, -8.975,  2.831};
  qt= { 8.608,  8.752, -3.673};

  // advective normal flux: pure advection

  ArrayQ fn;
  ArrayQ fnTrue;

  // in one direction, upwinding should work one way
  nx= 1.0; // normal from L into R
  fn= 0;
  fnTrue= qL; // therefore should have internal flux
  pde.fluxAdvectiveUpwind(x, time, qL, qR, nx, fn);
  for (int i= 0; i < ArrayQ::M; i++)
    BOOST_CHECK_CLOSE(fn[i], fnTrue[i], tol);

  // and it should accumulate
  pde.fluxAdvectiveUpwind(x, time, qL, qR, nx, fn);
  for (int i= 0; i < ArrayQ::M; i++)
    BOOST_CHECK_CLOSE(fn[i], 2.*fnTrue[i], tol);

  // and in the other... the other
  nx= -1.0; // normal from R into L
  fn= 0;
  fnTrue= -qR; // therefore should have external flux
  pde.fluxAdvectiveUpwind(x, time, qL, qR, nx, fn);
  for (int i= 0; i < ArrayQ::M; i++)
    BOOST_CHECK_CLOSE(fn[i], fnTrue[i], tol);

  // and it should accumulate
  pde.fluxAdvectiveUpwind(x, time, qL, qR, nx, fn);
  for (int i= 0; i < ArrayQ::M; i++)
    BOOST_CHECK_CLOSE(fn[i], 2.*fnTrue[i], tol);
}

BOOST_AUTO_TEST_CASE(isValidState)
{
    typedef PDELorenz PDEClass;

    typedef PDEClass::ArrayQ<Real> ArrayQ;

    const Real alpha0= 10.0;
    const Real alpha1= 8.0/3.0;
    const Real alpha2= 28.0;

    ArrayQ q= 0;

    PDEClass pde(alpha0, alpha1, alpha2);

    BOOST_CHECK(pde.isValidState(q));
}

// -------------------------------------------------------------------------- //
BOOST_AUTO_TEST_CASE(jacobian_check)
{
  typedef PDELorenz PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename PDEClass::template ArrayQ<SurrealS<PDEClass::N>> ArrayQSurreal;

  const Real tol= 1.e-13;

  const Real alpha0= 10.0;
  const Real alpha1= 8.0/3.0;
  const Real alpha2= 28.0;

  PDEClass pde(alpha0, alpha1, alpha2);

  // static tests
  BOOST_CHECK(pde.D == 1);
  BOOST_CHECK(pde.N == 3);

  // flux components
  BOOST_CHECK(pde.hasFluxAdvectiveTime() == false);
  BOOST_CHECK(pde.hasFluxAdvective() == true);
  BOOST_CHECK(pde.hasFluxViscous() == false);
  BOOST_CHECK(pde.hasSource() == true);
  BOOST_CHECK(pde.hasForcingFunction() == false);

  BOOST_CHECK(pde.fluxViscousLinearInGradient() == true); // it's zero so... ???
  BOOST_CHECK(pde.needsSolutionGradientforSource() == false);

  // function tests

  Real x;
  Real time;
  ArrayQ q;
  ArrayQ qx;
  ArrayQ qt;

  x= 0;
  time= 0;
  q=  {-4.731,  4.362, -0.890};
  qx= { 9.021, -8.975,  2.831};
  qt= { 8.608,  8.752, -3.673};
  // advective normal flux: pure advection

  ArrayQSurreal qSurreal= q;
  ArrayQSurreal qxSurreal= qx;

  qSurreal[0].deriv(0)= 1;
  qSurreal[1].deriv(1)= 1;
  qSurreal[2].deriv(2)= 1;

  ArrayQSurreal src=  0.0;
  MatrixQ srcJac= 0.0;

  pde.source(x, time, qSurreal, qxSurreal, src);
  pde.jacobianSource(x, time, q, qx, srcJac);

  SANS_CHECK_CLOSE(src[0].deriv(0), srcJac(0, 0), tol, tol);
  SANS_CHECK_CLOSE(src[0].deriv(1), srcJac(0, 1), tol, tol);
  SANS_CHECK_CLOSE(src[0].deriv(2), srcJac(0, 2), tol, tol);

  SANS_CHECK_CLOSE(src[1].deriv(0), srcJac(1, 0), tol, tol);
  SANS_CHECK_CLOSE(src[1].deriv(1), srcJac(1, 1), tol, tol);
  SANS_CHECK_CLOSE(src[1].deriv(2), srcJac(1, 2), tol, tol);

  SANS_CHECK_CLOSE(src[2].deriv(0), srcJac(2, 0), tol, tol);
  SANS_CHECK_CLOSE(src[2].deriv(1), srcJac(2, 1), tol, tol);
  SANS_CHECK_CLOSE(src[2].deriv(2), srcJac(2, 2), tol, tol);

  ArrayQSurreal uSurreal= 0.0;
  MatrixQ dudq;

  pde.masterState(x, time, qSurreal, uSurreal);
  pde.jacobianMasterState(x, time, q, dudq);

  SANS_CHECK_CLOSE(uSurreal[0].deriv(0), dudq(0, 0), tol, tol);
  SANS_CHECK_CLOSE(uSurreal[0].deriv(1), dudq(0, 1), tol, tol);
  SANS_CHECK_CLOSE(uSurreal[0].deriv(2), dudq(0, 2), tol, tol);

  SANS_CHECK_CLOSE(uSurreal[1].deriv(0), dudq(1, 0), tol, tol);
  SANS_CHECK_CLOSE(uSurreal[1].deriv(1), dudq(1, 1), tol, tol);
  SANS_CHECK_CLOSE(uSurreal[1].deriv(2), dudq(1, 2), tol, tol);

  SANS_CHECK_CLOSE(uSurreal[2].deriv(0), dudq(2, 0), tol, tol);
  SANS_CHECK_CLOSE(uSurreal[2].deriv(1), dudq(2, 1), tol, tol);
  SANS_CHECK_CLOSE(uSurreal[2].deriv(2), dudq(2, 2), tol, tol);

  ArrayQSurreal fSurreal= 0.0;
  MatrixQ dfdq = 0;

  pde.fluxAdvective(x, time, qSurreal, fSurreal);
  pde.jacobianFluxAdvective(x, time, q, dfdq);

  SANS_CHECK_CLOSE(fSurreal[0].deriv(0), dfdq(0, 0), tol, tol);
  SANS_CHECK_CLOSE(fSurreal[0].deriv(1), dfdq(0, 1), tol, tol);
  SANS_CHECK_CLOSE(fSurreal[0].deriv(2), dfdq(0, 2), tol, tol);

  SANS_CHECK_CLOSE(fSurreal[1].deriv(0), dfdq(1, 0), tol, tol);
  SANS_CHECK_CLOSE(fSurreal[1].deriv(1), dfdq(1, 1), tol, tol);
  SANS_CHECK_CLOSE(fSurreal[1].deriv(2), dfdq(1, 2), tol, tol);

  SANS_CHECK_CLOSE(fSurreal[2].deriv(0), dfdq(2, 0), tol, tol);
  SANS_CHECK_CLOSE(fSurreal[2].deriv(1), dfdq(2, 1), tol, tol);
  SANS_CHECK_CLOSE(fSurreal[2].deriv(2), dfdq(2, 2), tol, tol);
}

BOOST_AUTO_TEST_CASE(developer_exceptions)
{
  typedef PDELorenz PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  const Real alpha0= 10.0;
  const Real alpha1= 8.0/3.0;
  const Real alpha2= 28.0;

  PDEClass pde(alpha0, alpha1, alpha2);

  PyDict dummy;

  // function tests

  Real x;
  Real time;
  Real nx;
  ArrayQ q;
  ArrayQ qx;
  ArrayQ qt;
  ArrayQ f;
  MatrixQ mtx;

  x= 0;
  time= 0;
  nx= 1.0;
  q=  {-4.731,  4.362, -0.890};
  qx= { 9.021, -8.975,  2.831};
  qt= { 8.608,  8.752, -3.673};

  BOOST_CHECK_THROW(pde.jacobianFluxAdvectiveAbsoluteValue(x, time, q, nx, mtx), DeveloperException);
  BOOST_CHECK_THROW(pde.jacobianFluxAdvectiveAbsoluteValueSpaceTime(x, time, q, nx, nx, mtx), DeveloperException);
  BOOST_CHECK_THROW(pde.sourceTrace(x, x, time, q, q, q, q, f, f),  DeveloperException);
  BOOST_CHECK_THROW(pde.sourceLiftedQuantity(x, x, time, q, q, f), DeveloperException);
  BOOST_CHECK_THROW(pde.jacobianSourceHACK(x, time, q, qx, mtx), DeveloperException);
  BOOST_CHECK_THROW(pde.jacobianSourceAbsoluteValue(x, time, q, qx, mtx), DeveloperException);
  BOOST_CHECK_THROW(pde.jacobianGradientSourceHACK(x, time, q, qx, mtx), DeveloperException);
  BOOST_CHECK_THROW(pde.jacobianGradientSourceAbsoluteValue(x, time, nx, q, qx, mtx), DeveloperException);
  BOOST_CHECK_THROW(pde.jacobianGradientSourceGradientHACK(x, time, q, qx, qx, mtx), DeveloperException);
  BOOST_CHECK_THROW(pde.speedCharacteristic(x, time, nx, q, f), DeveloperException);
  BOOST_CHECK_THROW(pde.speedCharacteristic(x, time, q, f), DeveloperException);
}

// ########################################################################## //
BOOST_AUTO_TEST_SUITE_END()
