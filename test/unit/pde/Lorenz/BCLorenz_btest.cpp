// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// -------------------------------------------------------------------------- //
// BCLorenz_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/Lorenz/BCLorenz.h"
#include "pde/Lorenz/PDELorenz.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/BCParameters.h"

using namespace std;
using namespace SANS;

typedef PDELorenz PDEClass;

namespace SANS
{
// template class BCLorenz<PhysD1, BCTypeDirichlet_mitState>;
// template struct BCParameters<BCLorenzVector>;
}

// ########################################################################## //
BOOST_AUTO_TEST_SUITE(BCLorenz_test_suite)

BOOST_AUTO_TEST_CASE(static_test)
{
  typedef BCLorenz<PhysD1, BCTypeDirichlet_mitState> BCClass;

  BOOST_CHECK(BCClass::D == 1);
  BOOST_CHECK(BCClass::N == 3);
  BOOST_CHECK(BCClass::NBC == 1);
} // static_test

BOOST_AUTO_TEST_CASE(BCParameters_test)
{
  typedef BCParameters<BCLorenzVector> BCParams;

  BOOST_CHECK_EQUAL(BCParams::params.BC.None, "None");
  BOOST_CHECK_EQUAL(BCParams::params.BC.Dirichlet_mitState, "Dirichlet_mitState");

  PyDict BC0;
  BC0[BCParams::params.BC.BCType]= BCParams::params.BC.None;

  PyDict BC1;
  BC1[BCParams::params.BC.BCType]= BCParams::params.BC.Dirichlet_mitState;
  BC1[BCLorenzParams<PhysD1, BCTypeDirichlet_mitStateParam>::params.q0B]= 1.0;
  BC1[BCLorenzParams<PhysD1, BCTypeDirichlet_mitStateParam>::params.q1B]= 1.0;
  BC1[BCLorenzParams<PhysD1, BCTypeDirichlet_mitStateParam>::params.q2B]= 1.0;

  PyDict BCList;
  BCList["BC0"]= BC0;
  BCList["BC1"]= BC1;

  // no exceptions should be thrown
  BCParams::checkInputs(BCList);

  // construct a pde for the BCs
  PDELorenz pde;

  // create the BCs
  std::map<std::string, std::shared_ptr<BCBase>> BCs= BCParams::createBCs<BCNDConvertSpace>(pde, BCList);

  BOOST_CHECK_EQUAL(2, BCs.size());

  // the python dictionary does not preserve order, so count the BCs
  int NoneCount= 0;
  int DirichletCount= 0;

  // extract the keys from the dictionary
  std::vector<std::string> keys= BCList.stringKeys();

  for (auto &key : keys)
  {
    if (BCs[key]->derivedTypeID() == typeid(BCNDConvertSpace<PhysD1, BCNone<PhysD1, 3>>))
      NoneCount++;
    else if (BCs[key]->derivedTypeID() == typeid(BCNDConvertSpace<PhysD1,
        BCLorenz<PhysD1, BCTypeDirichlet_mitState>>))
      DirichletCount++;
    else
      BOOST_CHECK(false); // fail!
  }

  BOOST_CHECK_EQUAL(NoneCount, 1);
  BOOST_CHECK_EQUAL(DirichletCount, 1);

  std::map<std::string, std::vector<int>> boundaryGroups;

  boundaryGroups["BC0"]= {0};
  boundaryGroups["BC1"]= {4};

  std::vector<int> boundaryGroupsLG= BCParams::getLGBoundaryGroups(BCList, boundaryGroups);

  BOOST_REQUIRE_EQUAL(0, boundaryGroupsLG.size());
}

// -------------------------------------------------------------------------- //
BOOST_AUTO_TEST_CASE(BCNone_test)
{
  typedef BCNone<PhysD1, LorenzTraits::N> BCClass;

  BCClass bc;

  // static tests
  BOOST_CHECK(bc.D == 1);
  BOOST_CHECK(bc.N == 3);
  BOOST_CHECK(bc.NBC == 0);
}

// -------------------------------------------------------------------------- //
BOOST_AUTO_TEST_CASE(BCTypeDirichlet_mitState_test)
{
  typedef BCLorenz<PhysD1, BCTypeDirichlet_mitState> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  // typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol= 1.e-13;

  PDELorenz pde;

  const ArrayQ qB= {1.0, 0.0, 2.0};

  BCClass bc(pde, qB);
  BCClass bc2(pde, {0.0, 0.0, 0.0});

  Real x= 4.25;
  Real time= 0.256;
  Real nx= 1.0;
  ArrayQ qI= {6.27, 9.25, -3.26};
  ArrayQ qIx= {2.13, -9.07, 5.11};
  ArrayQ q;

  BOOST_CHECK(bc.isValidState(nx, q));
  BOOST_CHECK(!bc.hasFluxViscous());

  // set q to qB (qI ignored)
  bc.state(x, time, nx, qI, q);
  BOOST_CHECK_CLOSE(q(0), qB(0), tol);
  BOOST_CHECK_CLOSE(q(1), qB(1), tol);
  BOOST_CHECK_CLOSE(q(2), qB(2), tol);

  // // make sure equality operator works (i turned it off)
  // bc2= bc;
  // bc2.state(x, time, nx, qI, q);
  // BOOST_CHECK_CLOSE(q(0), qB(0), tol);
  // BOOST_CHECK_CLOSE(q(1), qB(1), tol);
  // BOOST_CHECK_CLOSE(q(2), qB(2), tol);

  // set it to 2*qB, should not accumulate
  bc.state(x, time, nx, {2.*qB(0), 2.*qB(1), 2.*qB(2)}, q);
  BOOST_CHECK_CLOSE(q(0), qB(0), tol);
  BOOST_CHECK_CLOSE(q(1), qB(1), tol);
  BOOST_CHECK_CLOSE(q(2), qB(2), tol);

  // reset q, try flux function
  q= {0., 0., 0.};
  bc.fluxNormal(x, time, nx, qI, qIx, qB, q);
  BOOST_CHECK_CLOSE(q(0), qI(0), tol);
  BOOST_CHECK_CLOSE(q(1), qI(1), tol);
  BOOST_CHECK_CLOSE(q(2), qI(2), tol);

  nx= -1.0; // reverse the flow, add (negative due to nx)
  bc.fluxNormal(x, time, nx, qI, qIx, qB, q);
  BOOST_CHECK_CLOSE(q(0), qI(0) - qB(0), tol);
  BOOST_CHECK_CLOSE(q(1), qI(1) - qB(1), tol);
  BOOST_CHECK_CLOSE(q(2), qI(2) - qB(2), tol);

  // static tests
  BOOST_CHECK(bc.D == 1);
  BOOST_CHECK(bc.N == 3);
  BOOST_CHECK(bc.NBC == 1);

}

BOOST_AUTO_TEST_SUITE_END() // BCLorenz_test_suite
