// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// testing 2D incompressible secondary variable class

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "SANS_btest.h"

#include "pde/IBL/ThicknessesCoefficients2D.h"

using namespace std;

namespace SANS
{
// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
}
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE (ThicknessesCoefficients2D_ProfileClosure_test_suite)

typedef IncompressibleBL CompressibilityType;
typedef VarData2DDANCt VarDataType;
typedef VarTypeDANCt VarType;
typedef ProfileClosure IBLClosureType;
typedef ThicknessesCoefficients2D<IBLClosureType, VarType, CompressibilityType> ThicknessesCoefficientsType;
typedef ThicknessesCoefficientsType::Functor<Real,Real> ThicknessesCoefficientsFunctorType;
typedef ThicknessesCoefficientsParams2D_ProfileClosure ThicknessesCoefficientsParamsType;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficientsParams2D_ProfileClosure_Dict_test )
{
  BOOST_CHECK(39 == ThicknessesCoefficientsParamsType::params.quadratureOrder.Default);
  BOOST_CHECK(39 == ThicknessesCoefficientsParamsType::params.quadratureOrder.min);

  BOOST_CHECK( ThicknessesCoefficientsParamsType::params.nameCDclosure.xfoillag
            == ThicknessesCoefficientsParamsType::params.nameCDclosure.Default);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_test )
{
  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  {
    ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret, PyDict());

    BOOST_CHECK_EQUAL(ThicknessesCoefficientsParamsType::params.quadratureOrder.Default,
                      thicknessesCoefficientsObj.quadratureOrder());
    BOOST_CHECK_EQUAL(ThicknessesCoefficientsParamsType::params.nameCDclosure.Default,
                      thicknessesCoefficientsObj.nameCDclosure());
  }

  {
    const int quadratureOrder = 39;
    const std::string nameCDclosure = ThicknessesCoefficientsParamsType::params.nameCDclosure.ibl3lag;

    PyDict tcDict;
    tcDict[ThicknessesCoefficientsParamsType::params.quadratureOrder] = quadratureOrder;
    tcDict[ThicknessesCoefficientsParamsType::params.nameCDclosure] = nameCDclosure;
    ThicknessesCoefficientsParamsType::checkInputs(tcDict);

    const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret, tcDict);

    BOOST_CHECK_EQUAL(quadratureOrder, thicknessesCoefficientsObj.quadratureOrder());
    BOOST_CHECK_EQUAL(nameCDclosure, thicknessesCoefficientsObj.nameCDclosure());
  }

  {
    PyDict tcDict;
    tcDict[ThicknessesCoefficientsParamsType::params.quadratureOrder] = 10;

    BOOST_CHECK_THROW(ThicknessesCoefficientsType secondaryVarObj(varInterpret, paramInterpret, tcDict);, SANSException);
  }

  {
    PyDict tcDict;
    tcDict[ThicknessesCoefficientsParamsType::params.nameCDclosure] = "SomeInvalidName";

    BOOST_CHECK_THROW(ThicknessesCoefficientsType secondaryVarObj(varInterpret, paramInterpret, tcDict);, SANSException);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficients_LaminarBL_ibl3eqm_test )
{
  const Real small_tol = 9.E-14;
  const Real close_tol = 2.5E-13;

  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  PyDict tcDict;
  tcDict[ThicknessesCoefficientsParamsType::params.quadratureOrder] = 39;
  tcDict[ThicknessesCoefficientsParamsType::params.nameCDclosure]
             = ThicknessesCoefficientsParamsType::params.nameCDclosure.ibl3eqm;

  const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret, tcDict);

  const Real deltaLami = 1.0, ALami = 3.0, deltaTurb = 1.e-2, ATurb = 1.0, nt = 2.3, ctau = 0.01;
  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
  const typename ThicknessesCoefficientsType::ArrayParam<Real> params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);
  const Real nue = 1.e-5;

  { // invalid profile type in Funtor initialization
    const IBLProfileCategory profileCatInvalid = static_cast<IBLProfileCategory>(4);
    BOOST_CHECK_THROW(thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(profileCatInvalid, params, var, nue);,
                      SANSException);
  }

  const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarBL, params, var, nue);

  BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
  BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

  const Real
  delta1s = 2.6215449513138855e-01,
  theta11 = 1.1110378361109696e-01,
  theta1s = 1.7716681897092956e-01,
  delta1p = 2.6215449513138855e-01,
  deltarho = 0.0000000000000000e+00,
  theta0s = 3.7325827874248552e-01,
  RetCf1 = 5.6577743566100276e+00 * theta11/deltaLami,
  RetCD = 1.7101012560435254e+00 * theta11/deltaLami;

  const Real H  = delta1s/theta11;
  const Real Hk = delta1s/theta11;
  const Real Hs = theta1s/theta11;

  SANS_CHECK_CLOSE(delta1s, fcn.getdelta1s(), small_tol, close_tol);
  SANS_CHECK_CLOSE(theta11, fcn.gettheta11(), small_tol, close_tol);
  SANS_CHECK_CLOSE(theta1s, fcn.gettheta1s(), small_tol, close_tol);

  SANS_CHECK_CLOSE(H, fcn.getH(), small_tol, close_tol);
  SANS_CHECK_CLOSE(Hk, fcn.getHk(), small_tol, close_tol);
  SANS_CHECK_CLOSE(Hs, fcn.getHs(), small_tol, close_tol);

  SANS_CHECK_CLOSE(delta1p, fcn.getdelta1p(), small_tol, close_tol);
  SANS_CHECK_CLOSE(fcn.getdelta1s(), fcn.getdelta1p(), small_tol, close_tol);
  SANS_CHECK_CLOSE(deltarho, fcn.getdeltarho(), small_tol, close_tol);
  SANS_CHECK_CLOSE(theta0s, fcn.gettheta0s(), small_tol, close_tol);
  SANS_CHECK_CLOSE(fcn.gettheta11()+fcn.getdelta1s()-fcn.getdeltarho(), fcn.gettheta0s(), small_tol, close_tol);
  SANS_CHECK_CLOSE(RetCf1, fcn.getRetCf1(), small_tol, close_tol);
  SANS_CHECK_CLOSE(RetCD, fcn.getRetCD(), small_tol, close_tol);

  const Real GH = (H-1.0)/H;

  const Real Us = 0.5*(-1.0 + 3.0/H);

  const Real A_GbetaLocus = fcn.getA_GbetaLocus();
  const Real B_GbetaLocus = fcn.getB_GbetaLocus();

  const Real ctaueq = Hs/(2.0-Us) * GH*GH*GH / (B_GbetaLocus*A_GbetaLocus*A_GbetaLocus);

  SANS_CHECK_CLOSE(ctaueq, fcn.getctaueq(), small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficients_LaminarWake_ibl3eqm_test )
{
  const Real small_tol = 1.E-14;
  const Real close_tol = 2.E-13;

  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  PyDict tcDict;
  tcDict[ThicknessesCoefficientsParamsType::params.quadratureOrder] = 39;
  tcDict[ThicknessesCoefficientsParamsType::params.nameCDclosure]
             = ThicknessesCoefficientsParamsType::params.nameCDclosure.ibl3eqm;

  const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret, tcDict);

  const Real deltaLami = 1.0, ALami = 3.0, deltaTurb = 1.e-2, ATurb = 1.0, nt = 2.3, ctau = 0.01;
  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
  const typename ThicknessesCoefficientsType::ArrayParam<Real> params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);
  const Real nue = 1.e-5;

  const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarWake, params, var, nue);

  const Real
  delta1s = 2.4999999999999997e-01,
  theta11 = 1.3839285714285715e-01,
  theta1s = 2.2186017107892111e-01,
  delta1p = 2.4999999999999997e-01,
  deltarho = 0.0000000000000000e+00,
  theta0s = 3.8839285714285710e-01,
  RetCf1 = 0.0,
  RetCD = 2.1428571428571446e+00 * theta11/deltaLami;

  const Real H  = delta1s/theta11;
  const Real Hk = delta1s/theta11;
  const Real Hs = theta1s/theta11;

  SANS_CHECK_CLOSE(delta1s, fcn.getdelta1s(), small_tol, close_tol);
  SANS_CHECK_CLOSE(theta11, fcn.gettheta11(), small_tol, close_tol);
  SANS_CHECK_CLOSE(theta1s, fcn.gettheta1s(), small_tol, close_tol);

  SANS_CHECK_CLOSE(H, fcn.getH(), small_tol, close_tol);
  SANS_CHECK_CLOSE(Hk, fcn.getHk(), small_tol, close_tol);
  SANS_CHECK_CLOSE(Hs, fcn.getHs(), small_tol, close_tol);

  SANS_CHECK_CLOSE(delta1p, fcn.getdelta1p(), small_tol, close_tol);
  SANS_CHECK_CLOSE(fcn.getdelta1s(), fcn.getdelta1p(), small_tol, close_tol);
  SANS_CHECK_CLOSE(deltarho, fcn.getdeltarho(), small_tol, close_tol);
  SANS_CHECK_CLOSE(theta0s, fcn.gettheta0s(), small_tol, close_tol);
  SANS_CHECK_CLOSE(fcn.gettheta11()+fcn.getdelta1s()-fcn.getdeltarho(), fcn.gettheta0s(), small_tol, close_tol);
  SANS_CHECK_CLOSE(RetCf1, fcn.getRetCf1(), small_tol, close_tol);
  SANS_CHECK_CLOSE(RetCD, fcn.getRetCD(), small_tol, close_tol);

  const Real GH = (H-1.0)/H;

  const Real Us = 0.5*(-1.0 + 3.0/H);

  const Real A_GbetaLocus = fcn.getA_GbetaLocus();
  const Real B_GbetaLocus = fcn.getB_GbetaLocus();

  const Real ctaueq = Hs/(2.0-Us) * GH*GH*GH / (B_GbetaLocus*A_GbetaLocus*A_GbetaLocus);

  SANS_CHECK_CLOSE(ctaueq, fcn.getctaueq(), small_tol, close_tol);

  const Real Uslip = 0.5*Hs*(1.0 - (Hk-1.0)/(H*B_GbetaLocus));
  SANS_CHECK_CLOSE(Uslip, fcn.getUslip(Hk, H, Hs), small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficients_TurbulentBL_ibl3eqm_test )
{
  const Real small_tol = 1.E-16;
  const Real close_tol = 2.E-13;

  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  PyDict tcDict;
  tcDict[ThicknessesCoefficientsParamsType::params.quadratureOrder] = 39;
  tcDict[ThicknessesCoefficientsParamsType::params.nameCDclosure]
             = ThicknessesCoefficientsParamsType::params.nameCDclosure.ibl3eqm;

  const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret, tcDict);

  const Real deltaLami = 1.0, ALami = 3.0, deltaTurb = 0.2, ATurb = 3.0, nt = 2.3, ctau = 1e-3;
  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
  const typename ThicknessesCoefficientsType::ArrayParam<Real> params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);
  const Real nue = paramInterpret.getqe(params)*deltaTurb/500.0;

  const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

#if ISIBL3SRCTURB
  const Real
  delta1s = 5.7816241506692803e-02,
  theta11 = 2.6233306658144610e-02,
  theta1s = 4.2270896239374398e-02,
  delta1p = 5.7816241506692803e-02,
  deltarho = 0.0000000000000000e+00,
  theta0s = 8.4049548164837423e-02,
  RetCf1 = 6.0000000000000000e+00 * theta11/deltaTurb,
  RetCD = 2.6069854917913080e+00 * theta11/deltaTurb;
#else
  const Real
  delta1s = 4.8565586937152821e-02,
  theta11 = 2.2377390914514794e-02,
  theta1s = 3.6669470849196417e-02,
  delta1p = 4.8565586937152821e-02,
  deltarho = 0.0000000000000000e+00,
  theta0s = 7.0942977851667616e-02,
  RetCf1 = 6.0000000000000000e+00 * theta11/deltaTurb,
  RetCD = 2.5986570651160221e+00 * theta11/deltaTurb;
#endif

  const Real H  = delta1s/theta11;
  const Real Hk = delta1s/theta11;
  const Real Hs = theta1s/theta11;

  SANS_CHECK_CLOSE(delta1s, fcn.getdelta1s(), small_tol, close_tol);
  SANS_CHECK_CLOSE(theta11, fcn.gettheta11(), small_tol, close_tol);
  SANS_CHECK_CLOSE(theta1s, fcn.gettheta1s(), small_tol, close_tol);

  SANS_CHECK_CLOSE(H, fcn.getH(), small_tol, close_tol);
  SANS_CHECK_CLOSE(Hk, fcn.getHk(), small_tol, close_tol);
  SANS_CHECK_CLOSE(Hs, fcn.getHs(), small_tol, close_tol);

  SANS_CHECK_CLOSE(delta1p, fcn.getdelta1p(), small_tol, close_tol);
  SANS_CHECK_CLOSE(fcn.getdelta1s(), fcn.getdelta1p(), small_tol, close_tol);
  SANS_CHECK_CLOSE(deltarho, fcn.getdeltarho(), small_tol, close_tol);
  SANS_CHECK_CLOSE(theta0s, fcn.gettheta0s(), small_tol, close_tol);
  SANS_CHECK_CLOSE(fcn.gettheta11()+fcn.getdelta1s()-fcn.getdeltarho(), fcn.gettheta0s(), small_tol, close_tol);
  SANS_CHECK_CLOSE(RetCf1, fcn.getRetCf1(), small_tol, close_tol);
  SANS_CHECK_CLOSE(RetCD, fcn.getRetCD(), small_tol, close_tol);

  const Real GH = (H-1.0)/H;

  const Real Us = 0.5*(-1.0 + 3.0/H);

  const Real A_GbetaLocus = fcn.getA_GbetaLocus();
  const Real B_GbetaLocus = fcn.getB_GbetaLocus();

  const Real ctaueq = Hs/(2.0-Us) * GH*GH*GH / (B_GbetaLocus*A_GbetaLocus*A_GbetaLocus);

  SANS_CHECK_CLOSE(ctaueq, fcn.getctaueq(), small_tol, close_tol);

  const Real Uslip = 0.5*Hs*(1.0 - (Hk-1.0)/(H*B_GbetaLocus));
  SANS_CHECK_CLOSE(Uslip, fcn.getUslip(Hk, H, Hs), small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficients_TurbulentBL_ibl3lag_CD_ctaueq_test )
{
  const Real small_tol = 1.E-16;
  const Real close_tol = 1.E-16;

  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  PyDict tcDict;
  tcDict[ThicknessesCoefficientsParamsType::params.quadratureOrder] = 39;
  tcDict[ThicknessesCoefficientsParamsType::params.nameCDclosure]
             = ThicknessesCoefficientsParamsType::params.nameCDclosure.ibl3lag;

  const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret, tcDict);

  const Real deltaLami = 1.0, ALami = 3.0, deltaTurb = 0.2, ATurb = 3.0, nt = 2.3, ctau = 1e-3;
  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
  const typename ThicknessesCoefficientsType::ArrayParam<Real> params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);
  const Real nue = paramInterpret.getqe(params)*deltaTurb/500.0;

  const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

  const Real
  delta1s = fcn.getdelta1s(),
  theta11 = fcn.gettheta11(),
  theta1s = fcn.gettheta1s(),
  RetCf1 = fcn.getRetCf1();

  const Real H  = delta1s/theta11;
  const Real Hs = theta1s/theta11;
  const Real GH = (H-1.0)/H;

  const Real Us = 0.5*(-1.0 + 3.0/H);

  const Real A_GbetaLocus = fcn.getA_GbetaLocus();
  const Real B_GbetaLocus = fcn.getB_GbetaLocus();

  const Real Ret = paramInterpret.getqe(params)*theta11/nue;

  const Real RedCD = 0.5*(0.5*RetCf1 + Ret*ctau) *Us + Ret*ctau*(1.0 - Us);
  const Real ctaueq = Hs/(2.0-Us) * GH*GH*GH / (B_GbetaLocus*A_GbetaLocus*A_GbetaLocus);

  SANS_CHECK_CLOSE(RedCD, fcn.getRetCD(), small_tol, close_tol);
  SANS_CHECK_CLOSE(ctaueq, fcn.getctaueq(), small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficients_TurbulentBL_xfoillag_CD_ctaueq_test )
{
  const Real small_tol = 1.E-16;
  const Real close_tol = 1.E-16;

  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  PyDict tcDict;
  tcDict[ThicknessesCoefficientsParamsType::params.quadratureOrder] = 39;
  tcDict[ThicknessesCoefficientsParamsType::params.nameCDclosure]
             = ThicknessesCoefficientsParamsType::params.nameCDclosure.xfoillag;

  const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret, tcDict);

  const Real deltaLami = 1.0, ALami = 3.0, deltaTurb = 0.2, ATurb = 3.0, nt = 2.3, ctau = 1e-3;
  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
  const typename ThicknessesCoefficientsType::ArrayParam<Real> params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);
  const Real nue = paramInterpret.getqe(params)*deltaTurb/500.0;

  const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

  const Real
  delta1s = fcn.getdelta1s(),
  theta11 = fcn.gettheta11(),
  theta1s = fcn.gettheta1s(),
  RetCf1 = fcn.getRetCf1();

  const Real H  = delta1s/theta11;
  const Real Hs = theta1s/theta11;
  const Real GH = (H-1.0)/H;

  const Real A_GbetaLocus = fcn.getA_GbetaLocus();
  const Real B_GbetaLocus = fcn.getB_GbetaLocus();

  const Real Us = 0.5*Hs*(1.0 - GH/B_GbetaLocus);

  const Real Ret = paramInterpret.getqe(params)*theta11/nue;

  const Real RetCD = 0.5*RetCf1*Us + Ret*ctau*(0.995 - Us) + 0.15*pow(0.995-Us,2.0);
  const Real ctaueq = Hs * 0.5/ (B_GbetaLocus*A_GbetaLocus*A_GbetaLocus) / (1.0-Us) * GH*GH*GH;

  SANS_CHECK_CLOSE(RetCD, fcn.getRetCD(), small_tol, close_tol);
  SANS_CHECK_CLOSE(ctaueq, fcn.getctaueq(), small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficients_TurbulentWake_ibl3eqm_test )
{
  const Real small_tol = 1.E-16;
  const Real close_tol = 2.E-13;

  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  PyDict tcDict;
  tcDict[ThicknessesCoefficientsParamsType::params.quadratureOrder] = 39;
  tcDict[ThicknessesCoefficientsParamsType::params.nameCDclosure]
             = ThicknessesCoefficientsParamsType::params.nameCDclosure.ibl3eqm;

  const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret, tcDict);

  const Real deltaLami = 1.0, ALami = 3.0, deltaTurb = 0.2, ATurb = 3.0, nt = 2.3, ctau = 1e-3;
  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
  const typename ThicknessesCoefficientsType::ArrayParam<Real> params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);
  const Real nue = paramInterpret.getqe(params)*deltaTurb/500.0;

  const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentWake, params, var, nue);

#if ISIBL3SRCTURB
  const Real
  delta1s = 6.2500000000000000e-02,
  theta11 = 3.3482142857142863e-02,
  theta1s = 5.2943638392857151e-02,
  delta1p = 6.2500000000000000e-02,
  deltarho = 0.0000000000000000e+00,
  theta0s = 9.5982142857142849e-02,
  RetCf1 = 0.0000000000000000e+00 * theta11/deltaTurb,
  RetCD = 2.3502585266197613e+00 * theta11/deltaTurb;
#else
  const Real
  delta1s = 5.6249999999999988e-02,
  theta11 = 3.0859375000000001e-02,
  theta1s = 4.9274468207573682e-02,
  delta1p = 5.6249999999999988e-02,
  deltarho = 0.0000000000000000e+00,
  theta0s = 8.7109375000000003e-02,
  RetCf1 = 0.0000000000000000e+00 * theta11/deltaTurb,
  RetCD = 2.1809542179909385e+00 * theta11/deltaTurb;
#endif

  const Real H  = delta1s/theta11;
  const Real Hk = delta1s/theta11;
  const Real Hs = theta1s/theta11;

  SANS_CHECK_CLOSE(delta1s, fcn.getdelta1s(), small_tol, close_tol);
  SANS_CHECK_CLOSE(theta11, fcn.gettheta11(), small_tol, close_tol);
  SANS_CHECK_CLOSE(theta1s, fcn.gettheta1s(), small_tol, close_tol);

  SANS_CHECK_CLOSE(H, fcn.getH(), small_tol, close_tol);
  SANS_CHECK_CLOSE(Hk, fcn.getHk(), small_tol, close_tol);
  SANS_CHECK_CLOSE(Hs, fcn.getHs(), small_tol, close_tol);

  SANS_CHECK_CLOSE(delta1p, fcn.getdelta1p(), small_tol, close_tol);
  SANS_CHECK_CLOSE(fcn.getdelta1s(), fcn.getdelta1p(), small_tol, close_tol);
  SANS_CHECK_CLOSE(deltarho, fcn.getdeltarho(), small_tol, close_tol);
  SANS_CHECK_CLOSE(theta0s, fcn.gettheta0s(), small_tol, close_tol);
  SANS_CHECK_CLOSE(fcn.gettheta11()+fcn.getdelta1s()-fcn.getdeltarho(), fcn.gettheta0s(), small_tol, close_tol);
  SANS_CHECK_CLOSE(RetCf1, fcn.getRetCf1(), small_tol, close_tol);
  SANS_CHECK_CLOSE(RetCD, fcn.getRetCD(), small_tol, close_tol);

  const Real GH = (H-1.0)/H;

  const Real Us = 0.5*(-1.0 + 3.0/H);

  const Real A_GbetaLocus = fcn.getA_GbetaLocus();
  const Real B_GbetaLocus = fcn.getB_GbetaLocus();

  const Real ctaueq = Hs/(2.0-Us) * GH*GH*GH / (B_GbetaLocus*A_GbetaLocus*A_GbetaLocus);

  SANS_CHECK_CLOSE(ctaueq, fcn.getctaueq(), small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficients_TurbulentWake_ibl3lag_CD_ctaueq_test )
{
  const Real small_tol = 1.E-14;
  const Real close_tol = 1.4E-14;

  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  PyDict tcDict;
  tcDict[ThicknessesCoefficientsParamsType::params.quadratureOrder] = 39;
  tcDict[ThicknessesCoefficientsParamsType::params.nameCDclosure]
             = ThicknessesCoefficientsParamsType::params.nameCDclosure.ibl3lag;

  const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret, tcDict);

  const Real deltaLami = 1.0, ALami = 3.0, deltaTurb = 0.2, ATurb = 3.0, nt = 2.3, ctau = 1e-3;
  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
  const typename ThicknessesCoefficientsType::ArrayParam<Real> params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);
  const Real nue = paramInterpret.getqe(params)*deltaTurb/500.0;

  const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentWake, params, var, nue);

  const Real
  delta1s = fcn.getdelta1s(),
  theta11 = fcn.gettheta11(),
  theta1s = fcn.gettheta1s();

  const Real H  = delta1s/theta11;
  const Real Hs = theta1s/theta11;
  const Real GH = (H-1.0)/H;

  const Real Us = 0.5*(-1.0 + 3.0/H);

  const Real A_GbetaLocus = fcn.getA_GbetaLocus();
  const Real B_GbetaLocus = fcn.getB_GbetaLocus();

  const Real Ret = paramInterpret.getqe(params)*theta11/nue;

  const Real RetCD = ( Ret*ctau*(1.0 - Us) ) * 2.0;
  const Real ctaueq = Hs/(2.0-Us) * GH*GH*GH / (B_GbetaLocus*A_GbetaLocus*A_GbetaLocus);

  SANS_CHECK_CLOSE(RetCD, fcn.getRetCD(), small_tol, close_tol);
  SANS_CHECK_CLOSE(ctaueq, fcn.getctaueq(), small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficients_TurbulentWake_xfoillag_CD_ctaueq_test )
{
  const Real small_tol = 1.E-16;
  const Real close_tol = 2.3E-14;

  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  PyDict tcDict;
  tcDict[ThicknessesCoefficientsParamsType::params.quadratureOrder] = 39;
  tcDict[ThicknessesCoefficientsParamsType::params.nameCDclosure]
             = ThicknessesCoefficientsParamsType::params.nameCDclosure.xfoillag;

  const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret, tcDict);

  const Real deltaLami = 1.0, ALami = 3.0, deltaTurb = 0.2, ATurb = 3.0, nt = 2.3, ctau = 1e-3;
  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
  const typename ThicknessesCoefficientsType::ArrayParam<Real> params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);
  const Real nue = paramInterpret.getqe(params)*deltaTurb/500.0;

  const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentWake, params, var, nue);

  const Real
  delta1s = fcn.getdelta1s(),
  theta11 = fcn.gettheta11(),
  theta1s = fcn.gettheta1s();

  const Real H  = delta1s/theta11;
  const Real Hs = theta1s/theta11;
  const Real GH = (H-1.0)/H;

  const Real A_GbetaLocus = fcn.getA_GbetaLocus();
  const Real B_GbetaLocus = fcn.getB_GbetaLocus();

  const Real Us = 0.5*Hs*(1.0 - GH/B_GbetaLocus);

  const Real Ret = paramInterpret.getqe(params)*theta11/nue;

  const Real RetCD = 2.0 * (Ret*ctau*(0.995 - Us) + 0.15*pow(0.995-Us,2.0));
  const Real ctaueq = Hs * 0.5/ (B_GbetaLocus*A_GbetaLocus*A_GbetaLocus) / (1.0-Us) * GH*GH*GH;

  SANS_CHECK_CLOSE(RetCD, fcn.getRetCD(), small_tol, close_tol);
  SANS_CHECK_CLOSE(ctaueq, fcn.getctaueq(), small_tol, close_tol);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
