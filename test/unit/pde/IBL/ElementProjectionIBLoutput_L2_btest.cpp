// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//#define DISPLAY_FOR_DEBUGGING

//----------------------------------------------------------------------------//
#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>
#include <iomanip>

#include "pde/IBL/ElementProjectionIBLoutput_L2.h"

#include "unit/UnitGrids/XField2D_Line_X1_2Group_AirfoilWithWake.h"

//----------------------------------------------------------------------------//

using namespace std;

namespace SANS
{
typedef VarTypeDANCt VarType;

typedef ElementProjectionIBLoutput_L2<VarType> ElementProjectionType;

typedef typename ElementProjectionType::PhysDim PhysDim;
typedef typename ElementProjectionType::TopoDim TopoDim;

typedef typename ElementProjectionType::PDEClass PDEClass;

typedef typename ElementProjectionType::ArrayQ<Real> ArrayQ;
typedef typename ElementProjectionType::ArrayParam<Real> ArrayParam;
typedef typename ElementProjectionType::ArrayOutput<Real> ArrayOutput;
typedef typename ElementProjectionType::VectorX<Real> VectorX;

typedef ElementXField<PhysDim,TopoDim,Line> ElementXFieldClass;
typedef Element<ArrayQ,TopoDim,Line> ElementQFieldClass;
typedef Element<ArrayParam,TopoDim,Line> ElementParamFieldClass;
typedef Element<ArrayOutput,TopoDim,Line> ElementOutputFieldClass;

typedef typename ElementXFieldClass::BaseType::RefCoordType RefCoordType;
typedef typename ElementProjectionType::outputID outputID;
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementProjectionIBLoutput_L2_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  const int nOutput = ElementProjectionType::nOutput;
  BOOST_CHECK_EQUAL(22,nOutput);

  const std::vector<std::string> output_names_true
  = {"deltaLami", "ALami", "delta1*", "theta11", "theta1*",
     "C_f", "C_Diss", "H", "H*", "Re_theta",
     "|Ue/Vinf|", "c_p", "m", "qx", "qz",
     "nt", "ctau", "gam", "beta", "ctau_eq",
     "deltaTurb", "ATurb"};

  const std::vector<std::string> output_names = ElementProjectionType::outputNames();

  for (int i=0; i<nOutput; ++i)
  {
    BOOST_CHECK_EQUAL(output_names_true[i], output_names[i]);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementProjectionIBLoutput_L2_Line_2D_X1Q0_OutputVariousOrders_test )
{
  const Real tol = 5.e-13;

  // ----------------------- SETUP ----------------------- //
  const int nOutput = ElementProjectionType::nOutput;

  // constanst/parameters
  const Real qinf = 1.2;

  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const typename PDEClass::GasModelType gasModel(gasModelDict);

  const typename PDEClass::ViscosityModelType viscosityModel(1.8725e-5);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.ntinit] = 0.0;
  transitionModelDict[TransitionModelParams::params.ntcrit] = 9.0;
  transitionModelDict[TransitionModelParams::params.xtr] = 10.0;
  transitionModelDict[TransitionModelParams::params.isTransitionActive] = true;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const typename PDEClass::TransitionModelType transitionModel(transitionModelDict);

  // pde
  PDEClass pde(gasModel, viscosityModel, transitionModel);

  // grid element
  const int order_xfld = 1;
  ElementXFieldClass xfldElem(order_xfld, BasisFunctionCategory_Hierarchical);

  xfldElem.DOF(0) = {0.0, 0.0};
  xfldElem.DOF(1) = {0.1, 0.5};

  BOOST_CHECK_EQUAL( order_xfld, xfldElem.order() );
  BOOST_CHECK_EQUAL( order_xfld+1, xfldElem.nDOF() );

  // IBL solution & parameter element: constant --> output should be constant as well regardless of output order
  const int order_qfld = 0;
  ElementQFieldClass qfldElem(order_qfld, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( order_qfld, qfldElem.order() );
  BOOST_CHECK_EQUAL( order_qfld+1, qfldElem.nDOF() );

  qfldElem.DOF(0) = pde.setDOFFrom(VarData2DDANCt(0.020,0.8,0.040,2.8,2.3,1e-3));

  const int order_param = 0;
  ElementParamFieldClass paramfldElem(order_param, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( order_param, paramfldElem.order() );
  BOOST_CHECK_EQUAL( order_param+1, paramfldElem.nDOF() );

  const Real p0 = 1.e5, T0 = 300.0;
  const PDEClass::ParamInterpType& paramInterpret = pde.getParamInterpreter();
  paramfldElem.DOF(0) = paramInterpret.setDOFFrom(1.2, 0.2, 0., 0., 0., 0., p0, T0);

  // compute reference values

  const auto& varInterpret = pde.getVarInterpreter();
  const auto& secondaryVarObj = pde.getSecondaryVarObj();

  ArrayOutput OutputTrue;

  {
    const int i = 0;
    const Real sRef = 0.5;
    const Real x = xfldElem.DOF(i)[0]; //TODO: a bit hacky here
    const Real z = xfldElem.DOF(i)[1]; //TODO: a bit hacky here

    VectorX e1;                // basis direction vector
    xfldElem.unitTangent(sRef, e1);

    const ArrayQ var = qfldElem.DOF(i);
    const ArrayParam params = paramfldElem.DOF(i);

    const auto profileCat = pde.getProfileCategory(var,x);

    const Real nt = varInterpret.getnt(var);
    const Real ctau = varInterpret.getctau(var);

    const Real gam = pde.calcIntermittency(params, e1, x, z, var);

    const VectorX q1 = paramInterpret.getq1(params);
    const Real qe = paramInterpret.getqe(params);
    const Real UeVinf = qe / qinf;

    const Real rhoe = gasModel.density(qe, p0, T0);
    const Real nue = viscosityModel.dynamicViscosity()/rhoe;

    const auto secondaryVarFcn
      = secondaryVarObj.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

    const Real delta1s = secondaryVarFcn.getdelta1s();
    const Real theta11 = secondaryVarFcn.gettheta11();
    const Real theta1s = secondaryVarFcn.gettheta1s();

    const Real Ret = qe*theta11/nue;

    Real beta = 0.0; // only needed for BL flows; set to zero in wake
    if (profileCat == IBLProfileCategory::LaminarBL ||
        profileCat == IBLProfileCategory::TurbulentBL)
    {
      beta = - delta1s/(0.5*secondaryVarFcn.getRetCf1()*nue/theta11)
             * paramInterpret.getdivq1(params);
    }

    OutputTrue[(int) outputID::deltaLami] = varInterpret.getdeltaLami(var);
    OutputTrue[(int) outputID::ALami] = varInterpret.getALami(var);
    OutputTrue[(int) outputID::delta1s] = delta1s;
    OutputTrue[(int) outputID::theta11] = theta11;
    OutputTrue[(int) outputID::theta1s] = theta1s;
    OutputTrue[(int) outputID::Cf1] = secondaryVarFcn.getRetCf1() / Ret;
    OutputTrue[(int) outputID::CD] = secondaryVarFcn.getRetCD() / Ret;
    OutputTrue[(int) outputID::H] = delta1s / theta11;
    OutputTrue[(int) outputID::Hs] = theta1s / theta11;
    OutputTrue[(int) outputID::Retheta] = Ret;
    OutputTrue[(int) outputID::UeVinf] = UeVinf;
    OutputTrue[(int) outputID::cp] = 1 - UeVinf*UeVinf;
    OutputTrue[(int) outputID::m] = rhoe*qe*delta1s;
    OutputTrue[(int) outputID::qx] = q1[0];
    OutputTrue[(int) outputID::qz] = q1[1];
    OutputTrue[(int) outputID::nt] = nt;
    OutputTrue[(int) outputID::ctau] = ctau;
    OutputTrue[(int) outputID::gam] = gam;
    OutputTrue[(int) outputID::beta] = beta;
    OutputTrue[(int) outputID::ctaueq] = secondaryVarFcn.getctaueq();
    OutputTrue[(int) outputID::deltaTurb] = varInterpret.getdeltaTurb(var);
    OutputTrue[(int) outputID::ATurb] = varInterpret.getATurb(var);
  }
  // ----------------------------- CHECK: Elemental output projection ----------------------------- //
  const int quadratureOrder = 39;

  const std::vector<int> order_output_arr = {0, 1, 2, 3, 4};
  const std::vector<RefCoordType> sRefArr = {0.0, 0.6, 1.0};

  // projected output element
  for (int jorder = 0; jorder < (int)order_output_arr.size(); ++jorder)
  {
    const int order_output = order_output_arr.at(jorder);
    ElementOutputFieldClass outputfldElem(order_output, BasisFunctionCategory_Legendre);

    BOOST_CHECK_EQUAL( order_output, outputfldElem.order() );
    BOOST_CHECK_EQUAL( order_output+1, outputfldElem.nDOF() );


    if (order_output == 1)
    {
#ifdef DISPLAY_FOR_DEBUGGING
    cout << "Output order = 1 uses linear interpolation instead of L2 projection!!!" << endl;
    cout << endl;
#endif
      BOOST_CHECK_THROW( ElementProjectionType elemOutputProjection(outputfldElem.basis(), quadratureOrder);, SANSException );
      continue;
    }
    else
    {
      ElementProjectionType elemOutputProjection(outputfldElem.basis(), quadratureOrder);
      elemOutputProjection.project(pde, qinf, qfldElem, paramfldElem, xfldElem, outputfldElem);
    }

#ifdef DISPLAY_FOR_DEBUGGING
    cout << "Output order = " << stringify(order_output) << endl;
#endif
    for (int pt = 0; pt < (int)sRefArr.size(); ++pt)
    {
      ArrayOutput OutputActual = outputfldElem.eval(sRefArr.at(pt));

      for (int n = 0; n < nOutput; ++n)
      {
        SANS_CHECK_CLOSE(OutputTrue[n], OutputActual[n], tol, tol);
#ifdef DISPLAY_FOR_DEBUGGING
        cout.precision(8);
        cout.setf(ios::scientific, ios::floatfield);
        cout << "OutputTrue["<<n<<"] = " << setw(15) << OutputTrue[n] << ",     " <<
            "OutputActual["<<n<<"] = " << setw(15) << OutputActual[n] << endl;
#endif
      }
    }
#ifdef DISPLAY_FOR_DEBUGGING
    cout << endl;
#endif
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementProjectionIBLoutput_L2_Line_2D_X1Q1_OutputP1_test )
{
  const Real tol = 2.e-7;

  // ----------------------- SETUP ----------------------- //
  const int nOutput = ElementProjectionType::nOutput;

  // constanst/parameters
  const Real qinf = 1.2;

  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const typename PDEClass::GasModelType gasModel(gasModelDict);

  const typename PDEClass::ViscosityModelType viscosityModel(1.8725e-5);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.ntinit] = 0.0;
  transitionModelDict[TransitionModelParams::params.ntcrit] = 9.0;
  transitionModelDict[TransitionModelParams::params.xtr] = 10.0;
  transitionModelDict[TransitionModelParams::params.isTransitionActive] = true;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const typename PDEClass::TransitionModelType transitionModel(transitionModelDict);

  // pde
  PDEClass pde(gasModel, viscosityModel, transitionModel);

  // grid element
  const int order_xfld = 1;
  ElementXFieldClass xfldElem(order_xfld, BasisFunctionCategory_Hierarchical);

  xfldElem.DOF(0) = {0.0, 0.0};
  xfldElem.DOF(1) = {0.1, 0.5};

  BOOST_CHECK_EQUAL( order_xfld, xfldElem.order() );
  BOOST_CHECK_EQUAL( order_xfld+1, xfldElem.nDOF() );

  // IBL solution element
  const int order_qfld = 1;
  ElementQFieldClass qfldElem(order_qfld, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( order_qfld, qfldElem.order() );
  BOOST_CHECK_EQUAL( order_qfld+1, qfldElem.nDOF() );

  qfldElem.DOF(0) = pde.setDOFFrom(VarData2DDANCt(0.020,0.8,0.010,0.5,2.3,1e-3));
  qfldElem.DOF(1) = pde.setDOFFrom(VarData2DDANCt(0.025,0.4,0.030,0.6,2.3,1e-3));

  // IBL parameter element
  const int order_param = 1;
  ElementParamFieldClass paramfldElem(order_param, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( order_param, paramfldElem.order() );
  BOOST_CHECK_EQUAL( order_param+1, paramfldElem.nDOF() );

  const Real p0 = 1.e5, T0 = 300.0;
  const PDEClass::ParamInterpType& paramInterpret = pde.getParamInterpreter();

  paramfldElem.DOF(0) = paramInterpret.setDOFFrom(1.2, 0.2, 0., 0., 0., 0., p0, T0);
  paramfldElem.DOF(1) = paramInterpret.setDOFFrom(1.4, 0.5, 0., 0., 0., 0., p0, T0);

  // projected output element
  const int order_output = 1;
  ElementOutputFieldClass outputfldElem(order_output, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( order_output, outputfldElem.order() );
  BOOST_CHECK_EQUAL( order_output+1, outputfldElem.nDOF() );

  // ----------------------- CHECK ----------------------- //

  const auto& varInterpret = pde.getVarInterpreter();
  const auto& secondaryVarObj = pde.getSecondaryVarObj();

  // compute reference values
  const int nnode = xfldElem.nDOF();
  vector<ArrayOutput> OutputTrue(nnode, 0.0);
  for (int i = 0; i < nnode; i++)
  {
    const Real sRef = 0.5;
    const Real x = xfldElem.DOF(i)[0]; //TODO: a bit hacky here
    const Real z = xfldElem.DOF(i)[1]; //TODO: a bit hacky here

    VectorX e1;                // basis direction vector
    xfldElem.unitTangent(sRef, e1);

    const ArrayQ var = qfldElem.DOF(i);
    const ArrayParam params = paramfldElem.DOF(i);

    const auto profileCat = pde.getProfileCategory(var,x);

    const Real nt = varInterpret.getnt(var);
    const Real ctau = varInterpret.getctau(var);

    const Real gam = pde.calcIntermittency(params, e1, x, z, var);

    const VectorX q1 = paramInterpret.getq1(params);
    const Real qe = paramInterpret.getqe(params);
    const Real UeVinf = qe / qinf;

    const Real rhoe = gasModel.density(qe, p0, T0);
    const Real nue = viscosityModel.dynamicViscosity()/rhoe;

    const auto secondaryVarFcn
      = secondaryVarObj.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

    const Real delta1s = secondaryVarFcn.getdelta1s();
    const Real theta11 = secondaryVarFcn.gettheta11();
    const Real theta1s = secondaryVarFcn.gettheta1s();

    const Real Ret = qe*theta11/nue;

    Real beta = 0.0; // only needed for BL flows; set to zero in wake
    if (profileCat == IBLProfileCategory::LaminarBL ||
        profileCat == IBLProfileCategory::TurbulentBL)
    {
      beta = - delta1s/(0.5*secondaryVarFcn.getRetCf1()*nue/theta11)
             * paramInterpret.getdivq1(params);
    }

    OutputTrue.at(i)[(int) outputID::deltaLami] = varInterpret.getdeltaLami(var);
    OutputTrue.at(i)[(int) outputID::ALami] = varInterpret.getALami(var);
    OutputTrue.at(i)[(int) outputID::delta1s] = delta1s;
    OutputTrue.at(i)[(int) outputID::theta11] = theta11;
    OutputTrue.at(i)[(int) outputID::theta1s] = theta1s;
    OutputTrue.at(i)[(int) outputID::Cf1] = secondaryVarFcn.getRetCf1() / Ret;
    OutputTrue.at(i)[(int) outputID::CD] = secondaryVarFcn.getRetCD() / Ret;
    OutputTrue.at(i)[(int) outputID::H] = delta1s / theta11;
    OutputTrue.at(i)[(int) outputID::Hs] = theta1s / theta11;
    OutputTrue.at(i)[(int) outputID::Retheta] = Ret;
    OutputTrue.at(i)[(int) outputID::UeVinf] = UeVinf;
    OutputTrue.at(i)[(int) outputID::cp] = 1 - UeVinf*UeVinf;
    OutputTrue.at(i)[(int) outputID::m] = rhoe*qe*delta1s;
    OutputTrue.at(i)[(int) outputID::qx] = q1[0];
    OutputTrue.at(i)[(int) outputID::qz] = q1[1];
    OutputTrue.at(i)[(int) outputID::nt] = nt;
    OutputTrue.at(i)[(int) outputID::ctau] = ctau;
    OutputTrue.at(i)[(int) outputID::gam] = gam;
    OutputTrue.at(i)[(int) outputID::beta] = beta;
    OutputTrue.at(i)[(int) outputID::ctaueq] = secondaryVarFcn.getctaueq();
    OutputTrue.at(i)[(int) outputID::deltaTurb] = varInterpret.getdeltaTurb(var);
    OutputTrue.at(i)[(int) outputID::ATurb] = varInterpret.getATurb(var);
  }

  // ----------------------------- Elemental output projection ----------------------------- //
  const int quadratureOrder = 39;
  ElementProjectionType elemOutputProjection(outputfldElem.basis(), quadratureOrder);
  elemOutputProjection.project(pde, qinf, qfldElem, paramfldElem, xfldElem, outputfldElem);

  for (int i = 0; i < nnode; ++i)
  {
    for (int n = 0; n < nOutput; ++n)
    {
      SANS_CHECK_CLOSE(OutputTrue[i][n], outputfldElem.DOF(i)[n], tol, tol);
#ifdef DISPLAY_FOR_DEBUGGING
      cout.precision(8);
      cout.setf(ios::scientific, ios::floatfield);
      cout << "OutputTrue["     <<i<<"]["<<n<<"] = " << setw(15) << OutputTrue[i][n] << ",     " <<
          "outputfldElem.DOF(" <<i<<")["<<n<<"] = " << setw(15) << outputfldElem.DOF(i)[n]<< endl;
#endif
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
