// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEIBL2D_btest
//
// test of 2-D Integral boundary layer (IBL) PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/IBL/PDEIBL2D.h"

using namespace std;

namespace SANS
{
typedef PhysD2 PhysDim;
typedef VarData2DDANCt VarDataType;
typedef VarTypeDANCt VarType;
typedef PDEIBL<PhysDim,VarType> PDEClass;

typedef typename PDEClass::ThicknessesCoefficientsType::ClassParamsType ThicknessesCoefficientsParamsType;

typedef typename PDEClass::IBLTraitsType IBLTraitsType;

typedef typename PDEClass::VarInterpType VarInterpretType;
typedef typename PDEClass::ProfileCategory ProfileCategory;
typedef typename PDEClass::GasModelType GasModelType;
typedef typename PDEClass::ViscosityModelType ViscosityModelType;
typedef typename PDEClass::TransitionModelType TransitionModelType;

typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

typedef typename PDEClass::template ArrayParam<Real> ArrayParam;
typedef typename PDEClass::template MatrixParam<Real> MatrixParam;

typedef typename PDEClass::template VectorX<Real> VectorX;
typedef typename PDEClass::template TensorX<Real> TensorX;

typedef typename PDEClass::template ArrayParam<Real> ArrayParam;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
template class PDEIBL<PhysD2,VarType>;
}

namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEIBL2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  BOOST_CHECK( (std::is_same<PDEClass::PhysDim, PhysDim>::value) );

  BOOST_CHECK( PDEClass::D == PhysDim::D );
  BOOST_CHECK( PDEClass::N == IBLTraitsType::N );
  BOOST_CHECK( PDEClass::Nparam == IBLTraitsType::Nparam );

  BOOST_CHECK( PDEClass::ir_momLami == 0 );
  BOOST_CHECK( PDEClass::ir_keLami == 1 );
  BOOST_CHECK( PDEClass::ir_momTurb == 2 );
  BOOST_CHECK( PDEClass::ir_keTurb == 3 );
  BOOST_CHECK( PDEClass::ir_amp == 4 );
  BOOST_CHECK( PDEClass::ir_lag == 5 );

  BOOST_CHECK( ArrayQ::M == PDEClass::N );
  BOOST_CHECK( MatrixQ::M == PDEClass::N );
  BOOST_CHECK( MatrixQ::N == PDEClass::N );

  BOOST_CHECK( ArrayParam::M == PDEClass::Nparam );
  BOOST_CHECK( MatrixParam::M == PDEClass::N );
  BOOST_CHECK( MatrixParam::N == PDEClass::Nparam );

  BOOST_CHECK( VectorX::M == PDEClass::D );
  BOOST_CHECK( TensorX::M == PDEClass::D );
  BOOST_CHECK( TensorX::N == PDEClass::D );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctorsANDbooleanANDmiscellaneous_test )
{
  const Real gam = 1.6, R = 28.715, mue = 7.0E-5;

  {
    // models
    PyDict gasModelDict;
    gasModelDict[GasModelParamsIBL::params.gamma] = gam;
    gasModelDict[GasModelParamsIBL::params.R] = R;
    GasModelParamsIBL::checkInputs(gasModelDict);
    const GasModelType gasModel(gasModelDict);

    ViscosityModelType viscosityModel(mue);

    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.ntinit] = 0.0;
    transitionModelDict[TransitionModelParams::params.ntcrit] = 9.0;
    transitionModelDict[TransitionModelParams::params.xtr] = 10.0;
    transitionModelDict[TransitionModelParams::params.isTransitionActive] = true;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                             = TransitionModelParams::params.profileCatDefault.laminarBL;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    // constructor
    PDEClass pde(gasModel, viscosityModel, transitionModel);

    // hasFlux functions
    BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
    BOOST_CHECK( pde.hasFluxAdvective() == true );
    BOOST_CHECK( pde.hasFluxViscous() == false );
    BOOST_CHECK( pde.hasSource() == true );
    BOOST_CHECK( pde.hasSourceTrace() == false );
    BOOST_CHECK( pde.hasSourceLiftedQuantity() == false );
    BOOST_CHECK( pde.hasForcingFunction() == false );

    BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

    // accessor functions
    auto& gasModel_ = pde.getGasModel();
    BOOST_CHECK(typeid(GasModelType&) == typeid(gasModel_) );

    auto& viscosityModel_ = pde.getViscosityModel();
    BOOST_CHECK(typeid(ViscosityModelType&) == typeid(viscosityModel_) );

    auto& transitionModel_ = pde.getTransitionModel();
    BOOST_CHECK(typeid(TransitionModelType&) == typeid(transitionModel_) );

    auto varInterpret_ = pde.getVarInterpreter();
    BOOST_CHECK(typeid(VarInterpretType&) == typeid(varInterpret_) );

    const auto& secondaryVarObj = pde.getSecondaryVarObj();
    BOOST_CHECK_EQUAL(ThicknessesCoefficientsParamsType::params.quadratureOrder.Default,
                      secondaryVarObj.quadratureOrder() );

    Real gam_, R_, mue_;
    pde.evalParam( gam_, R_, mue_ );
    BOOST_CHECK_EQUAL(gam, gam_);
    BOOST_CHECK_EQUAL(R, R_);
    BOOST_CHECK_EQUAL(mue, mue_);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( calcIntermittency_getProfileCategory_test )
{
  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(1.827e-5);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.ntinit] = 0.0;
  transitionModelDict[TransitionModelParams::params.ntcrit] = 9.0;
  transitionModelDict[TransitionModelParams::params.xtr] = 0.5;
  transitionModelDict[TransitionModelParams::params.isTransitionActive] = true;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                      = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const TransitionModelType transitionModel(transitionModelDict);

  // constructor
  PDEClass pde(gasModel, viscosityModel, transitionModel);

//  const Real deltaLami = 2.0e-2, ALami = -0.5, deltaTurb = 1.e-2, ATurb = 1.0, ctau = 1.2e-4;
//
//  const std::vector<Real> x_array = {0.0, 0.5, 1.0};
//  const std::vector<Real> nt_array = {0.0, 9.0, 10.0};
//
//  for (int j = 0; j < (int) nt_array.size(); ++j)
//  {
//    const Real nt = nt_array.at(j);
//    const ArrayQ var = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
//
//    for (int i = 0; i < (int) x_array.size(); ++i)
//    {
//      const Real x = x_array.at(i);
//
//      BOOST_CHECK_EQUAL(pde.calcIntermittency(params, e1, x, z, var), transitionModel.binaryIntermittency(nt, x));
//      BOOST_CHECK(pde.getProfileCategory(var, x) == transitionModel.getProfileCategory(nt, x));
//    }
//  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxAdvectiveTime_jacobianFluxAdvectiveTime_masterState_test )
{
  const Real small_tol = 1.E-13;
  const Real close_tol = 1.E-13;

  const Real gam = 1.6;
  const Real R = 350.0;
  const Real mue = 7.0E-5;

  const Real deltaLami = 2.0e-2, ALami = -0.5, deltaTurb = 1.e-2, ATurb = 1.0, nt = 2.3, ctau = 1.2e-4;
  const Real qx = 4.0, qz = 3.0, p0 = 6e3, T0 = 400.0;
  const Real qx_x = 0, qx_z = 0, qz_x = 0, qz_z = 0;

  const typename PDEClass::ParamInterpType paramInterpret;
  const ArrayParam params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const Real x = 0.1, z = 0.3;
  const VectorX e1 = { 0.6, 0.8 };

  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = gam;
  gasModelDict[GasModelParamsIBL::params.R] = R;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(mue);

  // Not implemented stuff
  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                             = TransitionModelParams::params.profileCatDefault.laminarBL;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    const ArrayQ var = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
    BOOST_CHECK( pde.isValidState(var) );

    ArrayQ ft = 0.0;
    BOOST_CHECK_THROW(pde.fluxAdvectiveTime(params,e1,x,z,var,ft);, DeveloperException);

    MatrixQ J = 0.0;
    BOOST_CHECK_THROW(pde.jacobianFluxAdvectiveTime(params,e1,x,z,var,J);, DeveloperException);
  }


  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.isTransitionActive] = true;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                             = TransitionModelParams::params.profileCatDefault.laminarBL;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    ArrayQ var = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
    BOOST_CHECK( pde.isValidState(var) );

    const auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();

    const Real qe = paramInterpret.getqe(params);
    const Real rhoe = gasModel.density(qe, p0, T0);
    const Real nue = viscosityModel.dynamicViscosity()/rhoe;

    const auto fcn = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarBL, params, var, nue);
    const Real delta1s = fcn.getdelta1s();
    const Real deltarho = fcn.getdeltarho();
    const Real theta0s = fcn.gettheta0s();

    const auto q1 = paramInterpret.getq1(params);

    const ArrayQ uConsTrue = { rhoe*(delta1s-deltarho)*dot(e1,q1),
                               rhoe*theta0s*pow(qe,2.0),
                               0.0,
                               0.0,
                               nt,
                               ctau };

    ArrayQ uCons = 0.0;

    pde.masterState(params,e1,x,z,var,uCons);
    for ( int i = 0; i < PDEClass::N; i++ )
      SANS_CHECK_CLOSE( uConsTrue[i], uCons[i], small_tol, close_tol );

    // check that conservative variables are not cumulative
    pde.masterState(params,e1,x,z,var,uCons);
    for ( int i = 0; i < PDEClass::N; i++ )
      SANS_CHECK_CLOSE( uConsTrue[i], uCons[i], small_tol, close_tol );
  }

  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.isTransitionActive] = false;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                             = TransitionModelParams::params.profileCatDefault.laminarWake;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    ArrayQ var = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
    BOOST_CHECK( pde.isValidState(var) );

    const auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();

    const Real qe = paramInterpret.getqe(params);
    const Real rhoe = gasModel.density(qe, p0, T0);
    const Real nue = viscosityModel.dynamicViscosity()/rhoe;

    const auto fcn = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarWake, params, var, nue);
    const Real delta1s = fcn.getdelta1s();
    const Real deltarho = fcn.getdeltarho();
    const Real theta0s = fcn.gettheta0s();

    const auto q1 = paramInterpret.getq1(params);

    const ArrayQ uConsTrue = { rhoe*(delta1s-deltarho)*dot(e1,q1),
                               rhoe*theta0s*pow(qe,2.0),
                               0.0,
                               0.0,
                               nt,
                               ctau };

    ArrayQ uCons = 0.0;

    pde.masterState(params,e1,x,z,var,uCons);
    for ( int i = 0; i < PDEClass::N; i++ )
      SANS_CHECK_CLOSE( uConsTrue[i], uCons[i], small_tol, close_tol );

    // check that conservative variables are not cumulative
    pde.masterState(params,e1,x,z,var,uCons);
    for ( int i = 0; i < PDEClass::N; i++ )
      SANS_CHECK_CLOSE( uConsTrue[i], uCons[i], small_tol, close_tol );
  }

  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.isTransitionActive] = false;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                             = TransitionModelParams::params.profileCatDefault.turbulentBL;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    ArrayQ var = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
    BOOST_CHECK( pde.isValidState(var) );

    const auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();

    const Real qe = paramInterpret.getqe(params);
    const Real rhoe = gasModel.density(qe, p0, T0);
    const Real nue = viscosityModel.dynamicViscosity()/rhoe;

    const auto fcn = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);
    const Real delta1s = fcn.getdelta1s();
    const Real deltarho = fcn.getdeltarho();
    const Real theta0s = fcn.gettheta0s();

    const auto q1 = paramInterpret.getq1(params);

    const ArrayQ uConsTrue = { 0.0,
                               0.0,
                               rhoe*(delta1s-deltarho)*dot(e1,q1),
                               rhoe*theta0s*pow(qe,2.0),
                               nt,
                               ctau };

    ArrayQ uCons = 0.0;

    pde.masterState(params,e1,x,z,var,uCons);
    for ( int i = 0; i < PDEClass::N; i++ )
      SANS_CHECK_CLOSE( uConsTrue[i], uCons[i], small_tol, close_tol );

    // check that conservative variables are not cumulative
    pde.masterState(params,e1,x,z,var,uCons);
    for ( int i = 0; i < PDEClass::N; i++ )
      SANS_CHECK_CLOSE( uConsTrue[i], uCons[i], small_tol, close_tol );
  }

  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.isTransitionActive] = false;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                             = TransitionModelParams::params.profileCatDefault.turbulentWake;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    ArrayQ var = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
    BOOST_CHECK( pde.isValidState(var) );

    const auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();

    const Real qe = paramInterpret.getqe(params);
    const Real rhoe = gasModel.density(qe, p0, T0);
    const Real nue = viscosityModel.dynamicViscosity()/rhoe;

    const auto fcn = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentWake, params, var, nue);
    const Real delta1s = fcn.getdelta1s();
    const Real deltarho = fcn.getdeltarho();
    const Real theta0s = fcn.gettheta0s();

    const auto q1 = paramInterpret.getq1(params);

    const ArrayQ uConsTrue = { 0.0,
                               0.0,
                               rhoe*(delta1s-deltarho)*dot(e1,q1),
                               rhoe*theta0s*pow(qe,2.0),
                               nt,
                               ctau };

    ArrayQ uCons = 0.0;

    pde.masterState(params,e1,x,z,var,uCons);
    for ( int i = 0; i < PDEClass::N; i++ )
      SANS_CHECK_CLOSE( uConsTrue[i], uCons[i], small_tol, close_tol );

    // check that conservative variables are not cumulative
    pde.masterState(params,e1,x,z,var,uCons);
    for ( int i = 0; i < PDEClass::N; i++ )
      SANS_CHECK_CLOSE( uConsTrue[i], uCons[i], small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxAdvective_test )
{
  const Real small_tol = 1.E-13;
  const Real close_tol = 3.E-13;

  const Real gam = 1.6;
  const Real R = 66.0;
  const Real mue = 7.0E-5;

  const Real x = 0.1;
  const Real z = 3.8;
  const Real t = 0.3;

  const Real deltaLami = 2.0e-2, ALami = -0.5, deltaTurb = 1.e-2, ATurb = 1.0, nt = 2.3, ctau = 1.2e-4;
  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  const typename PDEClass::ParamInterpType paramInterpret;
  const ArrayParam params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const VectorX e1 = { 0.6, 0.8 };

  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = gam;
  gasModelDict[GasModelParamsIBL::params.R] = R;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(mue);

  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                             = TransitionModelParams::params.profileCatDefault.laminarBL;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    ArrayQ var = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
    BOOST_CHECK( pde.isValidState(var) );

    const Real intermittency = pde.calcIntermittency(params, e1, x, z, var);

    const auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();

    const Real qe = paramInterpret.getqe(params);
    const Real rhoe = gasModel.density(qe, p0, T0);
    const Real nue = viscosityModel.dynamicViscosity()/rhoe;

    const auto fcn = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarBL, params, var, nue);
    const Real theta11 = fcn.gettheta11();
    const Real theta1s = fcn.gettheta1s();

    const auto q1 = paramInterpret.getq1(params);

//    const TensorX P = rhoe*theta11*(q1*Transpose(q1));
    const VectorX PTdote = rhoe*theta11*(q1*Transpose(q1))*e1;
    const VectorX K = rhoe*theta1s*pow(qe,2.0)*q1;

    const ArrayQ fxTrue = { (1.0-intermittency)*PTdote[0],
                            (1.0-intermittency)*K[0],
                            (   +intermittency)*PTdote[0],
                            (   +intermittency)*K[0],
                            nt*qx,
                            intermittency*ctau*qx};

    const ArrayQ fzTrue = { (1.0-intermittency)*PTdote[1],
                            (1.0-intermittency)*K[1],
                            (   +intermittency)*PTdote[1],
                            (   +intermittency)*K[1],
                            nt*qz,
                            intermittency*ctau*qz};

    ArrayQ fx = 0.0, fz = 0.0;

    // Advective flux
    pde.fluxAdvective(params,e1,x,z,t,var,fx,fz);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( fxTrue[i], fx[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( fzTrue[i], fz[i], small_tol, close_tol );
    }

    // Advective flux cumulation
    pde.fluxAdvective(params,e1,x,z,t,var,fx,fz);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( 2.*fxTrue[i], fx[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( 2.*fzTrue[i], fz[i], small_tol, close_tol );
    }
  }

  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                             = TransitionModelParams::params.profileCatDefault.laminarWake;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    ArrayQ var = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
    BOOST_CHECK( pde.isValidState(var) );

    const Real intermittency = pde.calcIntermittency(params, e1, x, z, var);

    const auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();

    const Real qe = paramInterpret.getqe(params);
    const Real rhoe = gasModel.density(qe, p0, T0);
    const Real nue = viscosityModel.dynamicViscosity()/rhoe;

    const auto fcn = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarWake, params, var, nue);
    const Real theta11 = fcn.gettheta11();
    const Real theta1s = fcn.gettheta1s();

    const auto q1 = paramInterpret.getq1(params);

    //    const TensorX P = rhoe*theta11*(q1*Transpose(q1));
    const VectorX PTdote = rhoe*theta11*(q1*Transpose(q1))*e1;
    const VectorX K = rhoe*theta1s*pow(qe,2.0)*q1;

    const ArrayQ fxTrue = { (1.0-intermittency)*PTdote[0],
                            (1.0-intermittency)*K[0],
                            (   +intermittency)*PTdote[0],
                            (   +intermittency)*K[0],
                            nt*qx,
                            intermittency*ctau*qx};

    const ArrayQ fzTrue = { (1.0-intermittency)*PTdote[1],
                            (1.0-intermittency)*K[1],
                            (   +intermittency)*PTdote[1],
                            (   +intermittency)*K[1],
                            nt*qz,
                            intermittency*ctau*qz};

    ArrayQ fx = 0.0, fz = 0.0;

    // Advective flux
    pde.fluxAdvective(params,e1,x,z,t,var,fx,fz);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( fxTrue[i], fx[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( fzTrue[i], fz[i], small_tol, close_tol );
    }

    // Advective flux cumulation
    pde.fluxAdvective(params,e1,x,z,t,var,fx,fz);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( 2.*fxTrue[i], fx[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( 2.*fzTrue[i], fz[i], small_tol, close_tol );
    }
  }

  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
      = TransitionModelParams::params.profileCatDefault.turbulentBL;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    ArrayQ var = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
    BOOST_CHECK( pde.isValidState(var) );

    const Real intermittency = pde.calcIntermittency(params, e1, x, z, var);

    const auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();

    const Real qe = paramInterpret.getqe(params);
    const Real rhoe = gasModel.density(qe, p0, T0);
    const Real nue = viscosityModel.dynamicViscosity()/rhoe;

    const auto fcn = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);
    const Real theta11 = fcn.gettheta11();
    const Real theta1s = fcn.gettheta1s();

    const auto q1 = paramInterpret.getq1(params);

    //    const TensorX P = rhoe*theta11*(q1*Transpose(q1));
    const VectorX PTdote = rhoe*theta11*(q1*Transpose(q1))*e1;
    const VectorX K = rhoe*theta1s*pow(qe,2.0)*q1;

    const ArrayQ fxTrue = { (1.0-intermittency)*PTdote[0],
                            (1.0-intermittency)*K[0],
                            (   +intermittency)*PTdote[0],
                            (   +intermittency)*K[0],
                            nt*qx,
                            intermittency*ctau*qx};

    const ArrayQ fzTrue = { (1.0-intermittency)*PTdote[1],
                            (1.0-intermittency)*K[1],
                            (   +intermittency)*PTdote[1],
                            (   +intermittency)*K[1],
                            nt*qz,
                            intermittency*ctau*qz};

    ArrayQ fx = 0.0, fz = 0.0;

    // Advective flux
    pde.fluxAdvective(params,e1,x,z,t,var,fx,fz);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( fxTrue[i], fx[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( fzTrue[i], fz[i], small_tol, close_tol );
    }

    // Advective flux cumulation
    pde.fluxAdvective(params,e1,x,z,t,var,fx,fz);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( 2.*fxTrue[i], fx[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( 2.*fzTrue[i], fz[i], small_tol, close_tol );
    }
  }

  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                             = TransitionModelParams::params.profileCatDefault.turbulentWake;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    ArrayQ var = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
    BOOST_CHECK( pde.isValidState(var) );

    const Real intermittency = pde.calcIntermittency(params, e1, x, z, var);

    const auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();

    const Real qe = paramInterpret.getqe(params);
    const Real rhoe = gasModel.density(qe, p0, T0);
    const Real nue = viscosityModel.dynamicViscosity()/rhoe;

    const auto fcn = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentWake, params, var, nue);
    const Real theta11 = fcn.gettheta11();
    const Real theta1s = fcn.gettheta1s();

    const auto q1 = paramInterpret.getq1(params);

    //    const TensorX P = rhoe*theta11*(q1*Transpose(q1));
    const VectorX PTdote = rhoe*theta11*(q1*Transpose(q1))*e1;
    const VectorX K = rhoe*theta1s*pow(qe,2.0)*q1;

    const ArrayQ fxTrue = { (1.0-intermittency)*PTdote[0],
                            (1.0-intermittency)*K[0],
                            (   +intermittency)*PTdote[0],
                            (   +intermittency)*K[0],
                            nt*qx,
                            intermittency*ctau*qx};

    const ArrayQ fzTrue = { (1.0-intermittency)*PTdote[1],
                            (1.0-intermittency)*K[1],
                            (   +intermittency)*PTdote[1],
                            (   +intermittency)*K[1],
                            nt*qz,
                            intermittency*ctau*qz};

    ArrayQ fx = 0.0, fz = 0.0;

    // Advective flux
    pde.fluxAdvective(params,e1,x,z,t,var,fx,fz);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( fxTrue[i], fx[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( fzTrue[i], fz[i], small_tol, close_tol );
    }

    // Advective flux cumulation
    pde.fluxAdvective(params,e1,x,z,t,var,fx,fz);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( 2.*fxTrue[i], fx[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( 2.*fzTrue[i], fz[i], small_tol, close_tol );
    }
  }
}

#if ISIBLLFFLUX_PDEIBL
////----------------------------------------------------------------------------//
//BOOST_AUTO_TEST_CASE( fluxAdvectiveUpwind_test )
//{
//  SANS_DEVELOPER_EXCEPTION("Not implemented!");
//
//  // settings
//  const Real small_tol = 1.E-13;
//  const Real close_tol = 1.E-13;
//
//  const Real gam = 1.4;
//  const Real R = 287.15;
//  const Real mue = 1.827E-5;
//
//  PyDict gasModelDict;
//  gasModelDict[GasModelParamsIBL::params.gamma] = gam;
//  gasModelDict[GasModelParamsIBL::params.R] = R;
//  GasModelParamsIBL::checkInputs(gasModelDict);
//  const GasModelType gasModel(gasModelDict);
//
//  ViscosityModelType viscosityModel(mue);
//
//  PyDict transitionModelDict;
//  transitionModelDict[TransitionModelParams::params.isTransitionActive] = true;
//  transitionModelDict[TransitionModelParams::params.profileCatDefault]
//                      = TransitionModelParams::params.profileCatDefault.laminarBL;
//  transitionModelDict[TransitionModelParams::params.ntcrit] = 9.0;
//  transitionModelDict[TransitionModelParams::params.xtr] = 10.0;
//  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
//  const TransitionModelType transitionModel(transitionModelDict);
//
//  PDEClass pde(gasModel, viscosityModel, transitionModel);
//
//  const Real x = 0.1;
//  const Real z = 3.8;
//  const Real t = 0.3;
//
//  const Real deltaLamiL = 1.2, ALamiL =  2.3, deltaTurbL = 0.2, ATurbL =  1.3, ntL = 2.3, ctauL = 1e-3; // laminar
//  const Real deltaLamiR = 0.2, ALamiR = -0.2, deltaTurbR = 1.2, ATurbR =  1.6, ntR = 3.3, ctauR = 3e-3; // laminar
//
//  const Real qxL =  3.0, qzL = 4.0, p0L = 1.2e5, T0L = 300;
//  const Real qxR =  4.0, qzR = -3.0, p0R = 0.3e5, T0R = 400;
//
//  const Real qx_xL = 0.7, qx_zL = 2.9, qz_xL = -0.5, qz_zL = 1.3;
//  const Real qx_xR = 1.9, qx_zR = 2.3, qz_xR = -0.3, qz_zR = -1.2;
//
//  const ArrayQ varL = pde.setDOFFrom( VarDataType(deltaLamiL, ALamiL, deltaTurbL, ATurbL, ntL, ctauL) );
//  const ArrayQ varR = pde.setDOFFrom( VarDataType(deltaLamiR, ALamiR, deltaTurbR, ATurbR, ntR, ctauR) );
//  BOOST_CHECK( pde.isValidState(varL) );
//  BOOST_CHECK( pde.isValidState(varR) );
//
//  const auto& paramInterpret = pde.getParamInterpreter();
//  const ArrayParam paramsL = paramInterpret.setDOFFrom(qxL, qzL, qx_xL, qx_zL, qz_xL, qz_zL, p0L, T0L);
//  const ArrayParam paramsR = paramInterpret.setDOFFrom(qxR, qzR, qx_xR, qx_zR, qz_xR, qz_zR, p0R, T0R);
//
//  const VectorX e1L = { 0.6, 0.8 };
//  const VectorX e1R = { -0.8, 0.6 };
//
//  const VectorX q1L = {qxL, qzL}, q1R = {qxR,qzR};
//  const Real qeL = sqrt(dot(q1L,q1L)), qeR = sqrt(dot(q1R,q1R));
//  const Real alpha = std::max( fabs(qeL), fabs(qeR) );
//
//  const auto& varInterpret = pde.getVarInterpreter();
//  const auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();
//
//  {
//    const VectorX nrm = { sqrt(0.5), sqrt(0.5) };
//
//    ArrayQ fx = 0.0, fz = 0.0;
//    BOOST_REQUIRE(dot(q1L,nrm) > 0);
//    pde.fluxAdvective(paramsL, e1L, x, z, t, varL, fx, fz);
//
//    const ArrayQ fTrue = fx*nrm[0]+fx*nrm[1];
//
//    ArrayQ fL = 0.0, fR = 0.0;
//
//    // results
//    pde.fluxAdvectiveUpwind(paramsL, paramsR, e1L, e1R, x, z, t, varL, varR, nrm[0], nrm[1], fL, fR);
//    for ( int i = 0; i < PDEClass::N; i++ )
//    {
//      SANS_CHECK_CLOSE( fTrue[i], fL[i], small_tol, close_tol );
//      SANS_CHECK_CLOSE( fTrue[i], fR[i], small_tol, close_tol );
//    }
//
//    pde.fluxAdvectiveUpwind(paramsL, paramsR, e1L, e1R, x, z, t, varL, varR, nrm[0], nrm[1], fL, fR);
//    for ( int i = 0; i < PDEClass::N; i++ )
//    {
//      SANS_CHECK_CLOSE( 2*fTrue[i], fL[i], small_tol, close_tol );
//      SANS_CHECK_CLOSE( 2*fTrue[i], fR[i], small_tol, close_tol );
//    }
//  }
//
//
//  // fluxAdvectiveUpwind with this signature is turned off
//  {
//    const VectorX nrm = { sqrt(0.5), sqrt(0.5) };
//    ArrayQ f_uni = 0.0;
//
//    BOOST_CHECK_THROW(pde.fluxAdvectiveUpwind(paramsL, paramsR, e1L, e1R, x, z, t, varL, varR, nrm[0], nrm[1], f_uni);,
//    SANSException);
//  }
//}
#else
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxAdvectiveUpwind_LaminarToLaminar_test )
{
  // settings
  const Real small_tol = 1.E-13;
  const Real close_tol = 1.E-13;

  const Real gam = 1.4;
  const Real R = 287.15;
  const Real mue = 1.827E-5;

  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = gam;
  gasModelDict[GasModelParamsIBL::params.R] = R;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(mue);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.isTransitionActive] = true;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                      = TransitionModelParams::params.profileCatDefault.laminarBL;
  transitionModelDict[TransitionModelParams::params.ntcrit] = 9.0;
  transitionModelDict[TransitionModelParams::params.xtr] = 10.0;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const TransitionModelType transitionModel(transitionModelDict);

  PDEClass pde(gasModel, viscosityModel, transitionModel);

  const Real x = 0.1;
  const Real z = 3.8;
  const Real t = 0.3;

  const Real deltaLamiL = 1.2, ALamiL =  2.3, deltaTurbL = 0.2, ATurbL =  1.3, ntL = 2.3, ctauL = 1e-3; // laminar
  const Real deltaLamiR = 0.2, ALamiR = -0.2, deltaTurbR = 1.2, ATurbR =  1.6, ntR = 3.3, ctauR = 3e-3; // laminar

  const Real qxL =  3.0, qzL = 4.0, p0L = 1.2e5, T0L = 300;
  const Real qxR =  4.0, qzR = -3.0, p0R = 0.3e5, T0R = 400;

  const Real qx_xL = 0.7, qx_zL = 2.9, qz_xL = -0.5, qz_zL = 1.3;
  const Real qx_xR = 1.9, qx_zR = 2.3, qz_xR = -0.3, qz_zR = -1.2;

  const ArrayQ varL = pde.setDOFFrom( VarDataType(deltaLamiL, ALamiL, deltaTurbL, ATurbL, ntL, ctauL) );
  const ArrayQ varR = pde.setDOFFrom( VarDataType(deltaLamiR, ALamiR, deltaTurbR, ATurbR, ntR, ctauR) );
  BOOST_CHECK( pde.isValidState(varL) );
  BOOST_CHECK( pde.isValidState(varR) );

  const auto& paramInterpret = pde.getParamInterpreter();
  const ArrayParam paramsL = paramInterpret.setDOFFrom(qxL, qzL, qx_xL, qx_zL, qz_xL, qz_zL, p0L, T0L);
  const ArrayParam paramsR = paramInterpret.setDOFFrom(qxR, qzR, qx_xR, qx_zR, qz_xR, qz_zR, p0R, T0R);

  const VectorX e1L = { 0.6, 0.8 };
  const VectorX e1R = { -0.8, 0.6 };

  const VectorX q1L = {qxL, qzL}, q1R = {qxR,qzR};

  {
    VectorX nrmL = { sqrt(0.5), sqrt(0.5) };
    nrmL /= sqrt(dot(nrmL,nrmL));
    VectorX nrmR = { sqrt(0.5), sqrt(0.6) };
    nrmR /= sqrt(dot(nrmR,nrmR));

    ArrayQ fx = 0.0, fz = 0.0;
    BOOST_REQUIRE(dot(q1L,nrmL) > 0);
    pde.fluxAdvective(paramsL, e1L, x, z, t, varL, fx, fz);

    const ArrayQ fTrue = fx*nrmL[0]+fz*nrmL[1];

    ArrayQ fL = 0.0, fR = 0.0;

    // results
    pde.fluxAdvectiveUpwind(paramsL, paramsR, e1L, e1R, x, z, t, varL, varR, nrmL[0], nrmL[1], nrmR[0], nrmR[1], fL, fR);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( fTrue[i], fL[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( fTrue[i], fR[i], small_tol, close_tol );
    }

    pde.fluxAdvectiveUpwind(paramsL, paramsR, e1L, e1R, x, z, t, varL, varR, nrmL[0], nrmL[1], nrmR[0], nrmR[1], fL, fR);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( 2*fTrue[i], fL[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( 2*fTrue[i], fR[i], small_tol, close_tol );
    }
  }

  {
    VectorX nrmL = { -sqrt(0.5), -sqrt(0.5) };
    nrmL /= sqrt(dot(nrmL,nrmL));
    VectorX nrmR = { -sqrt(0.5), -sqrt(0.6) };
    nrmR /= sqrt(dot(nrmR,nrmR));

    ArrayQ fx = 0.0, fz = 0.0;
    BOOST_REQUIRE(dot(q1L,nrmL) < 0);
    pde.fluxAdvective(paramsR, e1R, x, z, t, varR, fx, fz);

    const ArrayQ fTrue = fx*nrmR[0]+fz*nrmR[1];

    ArrayQ fL = 0.0, fR = 0.0;

    pde.fluxAdvectiveUpwind(paramsL, paramsR, e1L, e1R, x, z, t, varL, varR, nrmL[0], nrmL[1], nrmR[0], nrmR[1], fL, fR);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( fTrue[i], fL[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( fTrue[i], fR[i], small_tol, close_tol );
    }

    pde.fluxAdvectiveUpwind(paramsL, paramsR, e1L, e1R, x, z, t, varL, varR, nrmL[0], nrmL[1], nrmR[0], nrmR[1], fL, fR);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( 2*fTrue[i], fL[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( 2*fTrue[i], fR[i], small_tol, close_tol );
    }
  }

  // fluxAdvectiveUpwind with this signature is turned off
  {
    const VectorX nrm = { sqrt(0.5), sqrt(0.5) };
    ArrayQ f_uni = 0.0;

    BOOST_CHECK_THROW(pde.fluxAdvectiveUpwind(paramsL, paramsR, e1L, e1R, x, z, t, varL, varR, nrm[0], nrm[1], f_uni);,
                      SANSException);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxAdvectiveUpwind_LaminarLeft_TurbulentRight_test )
{
  // settings
  const Real small_tol = 1.E-13;
  const Real close_tol = 1.E-13;

  const Real gam = 1.4;
  const Real R = 287.15;
  const Real mue = 1.827E-5;

  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = gam;
  gasModelDict[GasModelParamsIBL::params.R] = R;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(mue);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.isTransitionActive] = true;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                      = TransitionModelParams::params.profileCatDefault.laminarBL;
  transitionModelDict[TransitionModelParams::params.ntcrit] = 9.0;
  transitionModelDict[TransitionModelParams::params.xtr] = 10.0;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const TransitionModelType transitionModel(transitionModelDict);

  PDEClass pde(gasModel, viscosityModel, transitionModel);

  const Real x = 0.1;
  const Real z = 3.8;
  const Real t = 0.3;

  const Real deltaLamiL = 1.2, ALamiL =  2.3, deltaTurbL = 0.2, ATurbL =  1.3, ntL = 2.3, ctauL = 1e-3; // laminar
  const Real deltaLamiR = 0.2, ALamiR = -0.2, deltaTurbR = 1.2, ATurbR =  1.6, ntR = 10.3, ctauR = 3e-3; // turbulent

  const Real qxL =  3.0, qzL = 4.0, p0L = 1.2e5, T0L = 300;
  const Real qxR =  4.0, qzR = -3.0, p0R = 0.3e5, T0R = 400;

  const Real qx_xL = 0.7, qx_zL = 2.9, qz_xL = -0.5, qz_zL = 1.3;
  const Real qx_xR = 1.9, qx_zR = 2.3, qz_xR = -0.3, qz_zR = -1.2;

  const ArrayQ varL = pde.setDOFFrom( VarDataType(deltaLamiL, ALamiL, deltaTurbL, ATurbL, ntL, ctauL) );
  const ArrayQ varR = pde.setDOFFrom( VarDataType(deltaLamiR, ALamiR, deltaTurbR, ATurbR, ntR, ctauR) );
  BOOST_CHECK( pde.isValidState(varL) );
  BOOST_CHECK( pde.isValidState(varR) );

  const auto& paramInterpret = pde.getParamInterpreter();
  const ArrayParam paramsL = paramInterpret.setDOFFrom(qxL, qzL, qx_xL, qx_zL, qz_xL, qz_zL, p0L, T0L);
  const ArrayParam paramsR = paramInterpret.setDOFFrom(qxR, qzR, qx_xR, qx_zR, qz_xR, qz_zR, p0R, T0R);

  const VectorX e1L = { 0.6, 0.8 };
  const VectorX e1R = { -0.8, 0.6 };

  const VectorX q1L = {qxL, qzL}, q1R = {qxR,qzR};

  const auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();

  {
    VectorX nrmL = { sqrt(0.5), sqrt(0.5) };
    nrmL /= sqrt(dot(nrmL,nrmL));
    VectorX nrmR = { sqrt(0.5), sqrt(0.6) };
    nrmR /= sqrt(dot(nrmR,nrmR));

    ArrayQ fx = 0.0, fz = 0.0;
    BOOST_REQUIRE(dot(q1L,nrmL) > 0);
    pde.fluxAdvective(paramsL, e1L, x, z, t, varL, fx, fz);
    const Real f_mom = fx[PDEClass::ir_momLami]*nrmL[0] + fz[PDEClass::ir_momLami]*nrmL[1];
    const Real f_ke = fx[PDEClass::ir_keLami]*nrmL[0] + fz[PDEClass::ir_keLami]*nrmL[1];
    const Real f_amp =  fx[PDEClass::ir_amp]*nrmL[0] + fz[PDEClass::ir_amp]*nrmL[1];

    const Real qeL = paramInterpret.getqe(paramsL);
    const Real rhoeL = gasModel.density(qeL, p0L, T0L);
    const Real nueL = viscosityModel.dynamicViscosity()/rhoeL;

    const auto& fcn = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarBL, paramsL, varL, nueL);
    const Real ctauTurbInit = IBLMiscClosures::getctauTurbInit_XFOIL(fcn.getHk(), fcn.getctaueq());
    const Real f_lag = ctauTurbInit*dot(q1L,nrmL);

    const ArrayQ fL_True = {f_mom, f_ke, 0.0, 0.0, f_amp, 0.0};
    const ArrayQ fR_True = {0.0, 0.0, f_mom, f_ke, f_amp, f_lag};

    ArrayQ fL = 0.0, fR = 0.0;

    pde.fluxAdvectiveUpwind(paramsL, paramsR, e1L, e1R, x, z, t, varL, varR, nrmL[0], nrmL[1], nrmR[0], nrmR[1], fL, fR);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( fL_True[i], fL[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( fR_True[i], fR[i], small_tol, close_tol );
    }

    pde.fluxAdvectiveUpwind(paramsL, paramsR, e1L, e1R, x, z, t, varL, varR, nrmL[0], nrmL[1], nrmR[0], nrmR[1], fL, fR);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( 2*fL_True[i], fL[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( 2*fR_True[i], fR[i], small_tol, close_tol );
    }
  }

  {
    VectorX nrmL = { -sqrt(0.5), -sqrt(0.5) };
    nrmL /= sqrt(dot(nrmL,nrmL));
    VectorX nrmR = { -sqrt(0.5), -sqrt(0.6) };
    nrmR /= sqrt(dot(nrmR,nrmR));

    ArrayQ fx = 0.0, fz = 0.0;
    BOOST_REQUIRE(dot(q1L,nrmL) < 0);
    pde.fluxAdvective(paramsR, e1R, x, z, t, varR, fx, fz);
    const Real f_mom = fx[PDEClass::ir_momTurb]*nrmR[0] + fz[PDEClass::ir_momTurb]*nrmR[1];
    const Real f_ke = fx[PDEClass::ir_keTurb]*nrmR[0] + fz[PDEClass::ir_keTurb]*nrmR[1];
    const Real f_amp =  fx[PDEClass::ir_amp]*nrmR[0] + fz[PDEClass::ir_amp]*nrmR[1];
    const Real f_lag = fx[PDEClass::ir_lag]*nrmR[0] + fz[PDEClass::ir_lag]*nrmR[1];

    const ArrayQ fL_True = {f_mom, f_ke, 0.0, 0.0, f_amp, 0.0};
    const ArrayQ fR_True = {0.0, 0.0, f_mom, f_ke, f_amp, f_lag};

    ArrayQ fL = 0.0, fR = 0.0;

    pde.fluxAdvectiveUpwind(paramsL, paramsR, e1L, e1R, x, z, t, varL, varR, nrmL[0], nrmL[1], nrmR[0], nrmR[1], fL, fR);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( fL_True[i], fL[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( fR_True[i], fR[i], small_tol, close_tol );
    }

    pde.fluxAdvectiveUpwind(paramsL, paramsR, e1L, e1R, x, z, t, varL, varR, nrmL[0], nrmL[1], nrmR[0], nrmR[1], fL, fR);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( 2*fL_True[i], fL[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( 2*fR_True[i], fR[i], small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxAdvectiveUpwind_TurbulentLeft_LaminarRight_test )
{
  // settings
  const Real small_tol = 1.E-13;
  const Real close_tol = 1.E-13;

  const Real gam = 1.4;
  const Real R = 287.15;
  const Real mue = 1.827E-5;

  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = gam;
  gasModelDict[GasModelParamsIBL::params.R] = R;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(mue);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.isTransitionActive] = true;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                      = TransitionModelParams::params.profileCatDefault.laminarBL;
  transitionModelDict[TransitionModelParams::params.ntcrit] = 9.0;
  transitionModelDict[TransitionModelParams::params.xtr] = 10.0;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const TransitionModelType transitionModel(transitionModelDict);

  PDEClass pde(gasModel, viscosityModel, transitionModel);

  const Real x = 0.1;
  const Real z = 3.8;
  const Real t = 0.3;

  const Real deltaLamiL = 0.2, ALamiL = -0.2, deltaTurbL = 1.2, ATurbL = 1.6, ntL = 10.3, ctauL = 3e-3; // turbulent
  const Real deltaLamiR = 1.2, ALamiR =  2.3, deltaTurbR = 0.2, ATurbR = 1.3, ntR = 2.3, ctauR = 1e-3; // laminar

  const Real qxL = -4.0, qzL = 3.0, p0L = 0.3e5, T0L = 400;
  const Real qxR = -3.0, qzR = -4.0, p0R = 1.2e5, T0R = 300;

  const Real qx_xL = 1.9, qx_zL = 2.3, qz_xL = -0.3, qz_zL = -1.2;
  const Real qx_xR = 0.7, qx_zR = 2.9, qz_xR = -0.5, qz_zR = 1.3;

  const ArrayQ varL = pde.setDOFFrom( VarDataType(deltaLamiL, ALamiL, deltaTurbL, ATurbL, ntL, ctauL) );
  const ArrayQ varR = pde.setDOFFrom( VarDataType(deltaLamiR, ALamiR, deltaTurbR, ATurbR, ntR, ctauR) );
  BOOST_CHECK( pde.isValidState(varL) );
  BOOST_CHECK( pde.isValidState(varR) );

  const auto& paramInterpret = pde.getParamInterpreter();
  const ArrayParam paramsL = paramInterpret.setDOFFrom(qxL, qzL, qx_xL, qx_zL, qz_xL, qz_zL, p0L, T0L);
  const ArrayParam paramsR = paramInterpret.setDOFFrom(qxR, qzR, qx_xR, qx_zR, qz_xR, qz_zR, p0R, T0R);

  const VectorX e1L = { -0.8, 0.6 };
  const VectorX e1R = { -0.6, -0.8 };

  const VectorX q1L = {qxL, qzL}, q1R = {qxR,qzR};

  const auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();

  {
    VectorX nrmL = { sqrt(0.5), sqrt(0.5) };
    nrmL /= sqrt(dot(nrmL,nrmL));
    VectorX nrmR = { sqrt(0.5), sqrt(0.6) };
    nrmR /= sqrt(dot(nrmR,nrmR));

    ArrayQ fx = 0.0, fz = 0.0;
    BOOST_REQUIRE(dot(q1L,nrmL) < 0);
    pde.fluxAdvective(paramsR, e1R, x, z, t, varR, fx, fz);
    const Real f_mom = fx[PDEClass::ir_momLami]*nrmR[0] + fz[PDEClass::ir_momLami]*nrmR[1];
    const Real f_ke = fx[PDEClass::ir_keLami]*nrmR[0] + fz[PDEClass::ir_keLami]*nrmR[1];
    const Real f_amp =  fx[PDEClass::ir_amp]*nrmR[0] + fz[PDEClass::ir_amp]*nrmR[1];

    const Real qeR = paramInterpret.getqe(paramsR);
    const Real rhoeR = gasModel.density(qeR, p0R, T0R);
    const Real nueR = viscosityModel.dynamicViscosity()/rhoeR;

    const auto& fcn = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarBL, paramsR, varR, nueR);
    const Real ctauTurbInit = IBLMiscClosures::getctauTurbInit_XFOIL(fcn.getHk(), fcn.getctaueq());
    const Real f_lag = ctauTurbInit*dot(q1R,nrmR);

    const ArrayQ fL_True = {0.0, 0.0, f_mom, f_ke, f_amp, f_lag};
    const ArrayQ fR_True = {f_mom, f_ke, 0.0, 0.0, f_amp, 0.0};

    ArrayQ fL = 0.0, fR = 0.0;

    pde.fluxAdvectiveUpwind(paramsL, paramsR, e1L, e1R, x, z, t, varL, varR, nrmL[0], nrmL[1], nrmR[0], nrmR[1], fL, fR);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( fL_True[i], fL[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( fR_True[i], fR[i], small_tol, close_tol );
    }

    pde.fluxAdvectiveUpwind(paramsL, paramsR, e1L, e1R, x, z, t, varL, varR, nrmL[0], nrmL[1], nrmR[0], nrmR[1], fL, fR);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( 2*fL_True[i], fL[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( 2*fR_True[i], fR[i], small_tol, close_tol );
    }
  }

  {
    VectorX nrmL = { -sqrt(0.5), -sqrt(0.5) };
    nrmL /= sqrt(dot(nrmL,nrmL));
    VectorX nrmR = { -sqrt(0.5), -sqrt(0.6) };
    nrmR /= sqrt(dot(nrmR,nrmR));

    ArrayQ fx = 0.0, fz = 0.0;
    BOOST_REQUIRE(dot(q1L,nrmL) > 0);
    pde.fluxAdvective(paramsL, e1L, x, z, t, varL, fx, fz);
    const Real f_mom = fx[PDEClass::ir_momTurb]*nrmL[0] + fz[PDEClass::ir_momTurb]*nrmL[1];
    const Real f_ke = fx[PDEClass::ir_keTurb]*nrmL[0] + fz[PDEClass::ir_keTurb]*nrmL[1];
    const Real f_amp =  fx[PDEClass::ir_amp]*nrmL[0] + fz[PDEClass::ir_amp]*nrmL[1];
    const Real f_lag = fx[PDEClass::ir_lag]*nrmL[0] + fz[PDEClass::ir_lag]*nrmL[1];

    const ArrayQ fL_True = {0.0, 0.0, f_mom, f_ke, f_amp, f_lag};
    const ArrayQ fR_True = {f_mom, f_ke, 0.0, 0.0, f_amp, 0.0};

    ArrayQ fL = 0.0, fR = 0.0;

    pde.fluxAdvectiveUpwind(paramsL, paramsR, e1L, e1R, x, z, t, varL, varR, nrmL[0], nrmL[1], nrmR[0], nrmR[1], fL, fR);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( fL_True[i], fL[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( fR_True[i], fR[i], small_tol, close_tol );
    }

    pde.fluxAdvectiveUpwind(paramsL, paramsR, e1L, e1R, x, z, t, varL, varR, nrmL[0], nrmL[1], nrmR[0], nrmR[1], fL, fR);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( 2*fL_True[i], fL[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( 2*fR_True[i], fR[i], small_tol, close_tol );
    }
  }
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( source_test )
{
  const Real small_tol = 1.E-13;
  const Real close_tol = 1.3E-13;

  const Real gam = 1.6;
  const Real R = 66.0;
  const Real mue = 7.0E-5;

  const Real x = 0.1;
  const Real z = 3.8;
  const Real t = 0.3;

  const Real deltaLami = 2.0e-2, ALami = -0.5, deltaTurb = 1.e-2, ATurb = 1.0, nt = 2.3, ctau = 1e-3;

  const Real deltaLami_x = 0.3, ALami_x = 0.5, deltaTurb_x = 1.0, ATurb_x = 2.3, nt_x = 0.8, ctau_x = 0.2;
  const Real deltaLami_z = 0.1, ALami_z = 0.2, deltaTurb_z = 2.0, ATurb_z = 4.3, nt_z = 0.3, ctau_z = 0.3;

  const Real qx = 4.0, qz = 3.0, qx_x = 2.5, qx_z = 5.2, qz_x = 3.6, qz_z = 6.6, p0 = 28.125, T0 = 4.6875;

  const typename PDEClass::ParamInterpType paramInterpret;
  const ArrayParam params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const VectorX e1 = { 0.6, 0.8 };
  const VectorX e1x = { 0.7, 0.3 };
  const VectorX e1z = { 0.5, 0.6 };

  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = gam;
  gasModelDict[GasModelParamsIBL::params.R] = R;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(mue);

  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                             = TransitionModelParams::params.profileCatDefault.laminarBL;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    const ArrayQ var = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
    const ArrayQ var_x = pde.setDOFFrom(VarDataType(deltaLami_x,ALami_x,deltaTurb_x,ATurb_x,nt_x,ctau_x));
    const ArrayQ var_z = pde.setDOFFrom(VarDataType(deltaLami_z,ALami_z,deltaTurb_z,ATurb_z,nt_z,ctau_z));
    BOOST_CHECK( pde.isValidState(var) );

    const auto& varInterpret = pde.getVarInterpreter();

    const auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();

    const Real qe = paramInterpret.getqe(params);
    const Real rhoe = gasModel.density(qe, p0, T0);
    const Real nue = viscosityModel.dynamicViscosity()/rhoe;

    const auto fcn = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarBL, params, var, nue);
    const Real delta1s = fcn.getdelta1s();
    const Real theta11 = fcn.gettheta11();
    const Real deltap = fcn.getdelta1p();
    SANS_CHECK_CLOSE(0.0, delta1s-deltap, small_tol, close_tol); // delta' is zero for incompressible flows
    const Real RetCf1 = fcn.getRetCf1();
    const Real RetCDiss = fcn.getRetCD();

    const Real Hk = fcn.getHk();

    const VectorX q1 = paramInterpret.getq1(params);
    const VectorX tauw = 0.5*rhoe*(RetCf1*nue/theta11)*q1;

    const TensorX gradsq1 = paramInterpret.getgradq1(params);
    const VectorX M = rhoe*delta1s*q1;
    const Real edotgradqdotM = dot(e1, gradsq1*M);
    const Real trPTdotgrade = rhoe*theta11*(qx*qx*e1x[0]+qx*qz*e1x[1]+qz*qx*e1z[0]+qz*qz*e1z[1]);

    const Real Diss = rhoe*pow(qe,2.0)*(RetCDiss*nue/theta11);

    const Real Ret = qe*theta11/nue;
    const Real ramp_amp = IBLMiscClosures::AmplificationClosure_IBL::ampGrowthOnsetRamp(Hk, Ret);
    const Real amprate = IBLMiscClosures::AmplificationClosure_IBL::ampGrowthRate(Hk, Ret, theta11);

    const Real t_ref = pow(theta11,2.0)/nue;
    const Real q_ref = 1.0;
    const Real S_stagnationBC_amp = (1.0-ramp_amp)/t_ref*exp(-pow(qe/q_ref,2.0))*(nt - transitionModel.ntinit());

    const Real divq1 = paramInterpret.getdivq1(params);

    const ArrayQ sourceTrue = { -trPTdotgrade + edotgradqdotM - dot(tauw,e1),
                                -2.0*Diss,
                                varInterpret.dummySource_momTurb(var),
                                varInterpret.dummySource_keTurb(var),
                                -nt*divq1 - qe*amprate + S_stagnationBC_amp,
                                varInterpret.dummySource_lag(var) };

    ArrayQ source = 0.0;

    pde.source(params,e1,e1x,e1z,x,z,t,var,var_x,var_z,source);
    for ( int i = 0; i < PDEClass::N; i++ )
      SANS_CHECK_CLOSE( sourceTrue[i], source[i], small_tol, close_tol );

    // Source cumulation
    pde.source(params,e1,e1x,e1z,x,z,t,var,var_x,var_z,source);
    for ( int i = 0; i < PDEClass::N; i++ )
      SANS_CHECK_CLOSE( 2*sourceTrue[i], source[i], small_tol, close_tol );
  }

  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                        = TransitionModelParams::params.profileCatDefault.laminarWake;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    const ArrayQ var = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
    const ArrayQ var_x = pde.setDOFFrom(VarDataType(deltaLami_x,ALami_x,deltaTurb_x,ATurb_x,nt_x,ctau_x));
    const ArrayQ var_z = pde.setDOFFrom(VarDataType(deltaLami_z,ALami_z,deltaTurb_z,ATurb_z,nt_z,ctau_z));
    BOOST_CHECK( pde.isValidState(var) );

    const auto& varInterpret = pde.getVarInterpreter();

    const auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();

    const Real qe = paramInterpret.getqe(params);
    const Real rhoe = gasModel.density(qe, p0, T0);
    const Real nue = viscosityModel.dynamicViscosity()/rhoe;

    const auto fcn = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarWake, params, var, nue);
    const Real delta1s = fcn.getdelta1s();
    const Real theta11 = fcn.gettheta11();
    const Real deltap = fcn.getdelta1p();
    SANS_CHECK_CLOSE(0.0, delta1s-deltap, small_tol, close_tol); // delta' is zero for incompressible flows
    const Real RetCf1 = fcn.getRetCf1();
    const Real RetCDiss = fcn.getRetCD();

    const VectorX q1 = paramInterpret.getq1(params);
    const VectorX tauw = 0.5*rhoe*(RetCf1*nue/theta11)*q1;

    const TensorX gradsq1 = paramInterpret.getgradq1(params);
    const VectorX M = rhoe*delta1s*q1;
    const Real edotgradqdotM = dot(e1, gradsq1*M);
    const Real trPTdotgrade = rhoe*theta11*(qx*qx*e1x[0]+qx*qz*e1x[1]+qz*qx*e1z[0]+qz*qz*e1z[1]);

    const Real Diss = rhoe*pow(qe,2.0)*(RetCDiss*nue/theta11);

    const Real divq1 = paramInterpret.getdivq1(params);

    const ArrayQ sourceTrue = { -trPTdotgrade + edotgradqdotM - dot(tauw,e1),
                                -2.0*Diss,
                                varInterpret.dummySource_momTurb(var),
                                varInterpret.dummySource_keTurb(var),
                                -nt*divq1,
                                varInterpret.dummySource_lag(var) };

    ArrayQ source = 0.0;

    pde.source(params,e1,e1x,e1z,x,z,t,var,var_x,var_z,source);
    for ( int i = 0; i < PDEClass::N; i++ )
      SANS_CHECK_CLOSE( sourceTrue[i], source[i], small_tol, close_tol );

    // Source cumulation
    pde.source(params,e1,e1x,e1z,x,z,t,var,var_x,var_z,source);
    for ( int i = 0; i < PDEClass::N; i++ )
      SANS_CHECK_CLOSE( 2*sourceTrue[i], source[i], small_tol, close_tol );
  }

  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                        = TransitionModelParams::params.profileCatDefault.turbulentBL;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    const ArrayQ var = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
    const ArrayQ var_x = pde.setDOFFrom(VarDataType(deltaLami_x,ALami_x,deltaTurb_x,ATurb_x,nt_x,ctau_x));
    const ArrayQ var_z = pde.setDOFFrom(VarDataType(deltaLami_z,ALami_z,deltaTurb_z,ATurb_z,nt_z,ctau_z));
    BOOST_CHECK( pde.isValidState(var) );

    const auto& varInterpret = pde.getVarInterpreter();

    const auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();

    const Real qe = paramInterpret.getqe(params);
    const Real rhoe = gasModel.density(qe, p0, T0);
    const Real nue = viscosityModel.dynamicViscosity()/rhoe;

    const auto fcn = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);
    const Real delta1s = fcn.getdelta1s();
    const Real theta11 = fcn.gettheta11();
    const Real deltap = fcn.getdelta1p();
    SANS_CHECK_CLOSE(0.0, delta1s-deltap, small_tol, close_tol); // delta' is zero for incompressible flows
    const Real RetCf1 = fcn.getRetCf1();
    const Real RetCDiss = fcn.getRetCD();

    const Real Hk = fcn.getHk();

    const VectorX q1 = paramInterpret.getq1(params);
    const VectorX tauw = 0.5*rhoe*(RetCf1*nue/theta11)*q1;

    const TensorX gradsq1 = paramInterpret.getgradq1(params);
    const VectorX M = rhoe*delta1s*q1;
    const Real edotgradqdotM = dot(e1, gradsq1*M);
    const Real trPTdotgrade = rhoe*theta11*(qx*qx*e1x[0]+qx*qz*e1x[1]+qz*qx*e1z[0]+qz*qz*e1z[1]);

    const Real Diss = rhoe*pow(qe,2.0)*(RetCDiss*nue/theta11);

    const Real divq1 = paramInterpret.getdivq1(params);

    const Real delta_nom = IBLMiscClosures::getdeltaNominal(Hk, delta1s, theta11);
    const Real ctaueq = fcn.getctaueq();

    const Real GACON = fcn.getA_GbetaLocus();
    const Real GBCON = fcn.getB_GbetaLocus();

    const Real Kdl = 1.0;
#if IS_LAGFACTOR_XFOILPAPER_NOT_XFOILV699_IBLTwoField
    const Real scc = 5.6;
    const Real HKC = Hk - 1.0;
#else
    const Real scc = 5.6*1.333/(1.0 + fcn.getUslip(Hk, fcn.getH(), fcn.getHs()));

    const Real Ret = qe*theta11/nue;
    Real HKC = Hk - 1.0 - 18.0/Ret;
    std::cout << "Unclamped HKC = " << HKC << std::endl;
    BOOST_REQUIRE(HKC < 0.01);
    HKC = 0.01;
#endif
    const Real HR = HKC/(GACON*Kdl*Hk);
    const Real duxcon = 1.0;

    const Real S_lag =
        2.0*ctau*(-0.5*qe/delta_nom * scc*(pow(ctaueq,0.5) - pow(ctau,0.5)*Kdl)
                  -duxcon*(0.5*RetCf1*nue/theta11 - qe*pow(HR,2.0))/(GBCON*delta1s)
                  +duxcon*divq1);

    const ArrayQ sourceTrue = { varInterpret.dummySource_momLami(var),
                                varInterpret.dummySource_keLami(var),
                                -trPTdotgrade + edotgradqdotM - dot(tauw,e1),
                                -2.0*Diss,
                                -nt*divq1,
                                -ctau*divq1 + S_lag };

    ArrayQ source = 0.0;

    pde.source(params,e1,e1x,e1z,x,z,t,var,var_x,var_z,source);
    for ( int i = 0; i < PDEClass::N; i++ )
      SANS_CHECK_CLOSE( sourceTrue[i], source[i], small_tol, close_tol );

    // Source cumulation
    pde.source(params,e1,e1x,e1z,x,z,t,var,var_x,var_z,source);
    for ( int i = 0; i < PDEClass::N; i++ )
      SANS_CHECK_CLOSE( 2*sourceTrue[i], source[i], small_tol, close_tol );
  }

  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                        = TransitionModelParams::params.profileCatDefault.turbulentWake;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    const ArrayQ var = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
    const ArrayQ var_x = pde.setDOFFrom(VarDataType(deltaLami_x,ALami_x,deltaTurb_x,ATurb_x,nt_x,ctau_x));
    const ArrayQ var_z = pde.setDOFFrom(VarDataType(deltaLami_z,ALami_z,deltaTurb_z,ATurb_z,nt_z,ctau_z));
    BOOST_CHECK( pde.isValidState(var) );

    const auto& varInterpret = pde.getVarInterpreter();

    const auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();

    const Real qe = paramInterpret.getqe(params);
    const Real rhoe = gasModel.density(qe, p0, T0);
    const Real nue = viscosityModel.dynamicViscosity()/rhoe;

    const auto fcn = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentWake, params, var, nue);
    const Real delta1s = fcn.getdelta1s();
    const Real theta11 = fcn.gettheta11();
    const Real deltap = fcn.getdelta1p();
    SANS_CHECK_CLOSE(0.0, delta1s-deltap, small_tol, close_tol); // delta' is zero for incompressible flows
    const Real RetCf1 = fcn.getRetCf1();
    const Real RetCDiss = fcn.getRetCD();

    const Real Hk = fcn.getHk();

    const VectorX q1 = paramInterpret.getq1(params);
    const VectorX tauw = 0.5*rhoe*(RetCf1*nue/theta11)*q1;

    const TensorX gradsq1 = paramInterpret.getgradq1(params);
    const VectorX M = rhoe*delta1s*q1;
    const Real edotgradqdotM = dot(e1, gradsq1*M);
    const Real trPTdotgrade = rhoe*theta11*(qx*qx*e1x[0]+qx*qz*e1x[1]+qz*qx*e1z[0]+qz*qz*e1z[1]);

    const Real Diss = rhoe*pow(qe,2.0)*(RetCDiss*nue/theta11);

    const Real divq1 = paramInterpret.getdivq1(params);

    const Real delta_nom = IBLMiscClosures::getdeltaNominal(Hk, delta1s, theta11);
    const Real ctaueq = fcn.getctaueq();

#if IS_LAGFACTOR_XFOILPAPER_NOT_XFOILV699_IBLTwoField
    const Real scc = 5.6;
#else
    const Real scc = 5.6*1.333/(1.0 + fcn.getUslip(Hk, fcn.getH(), fcn.getHs()));
#endif
    const Real GACON = fcn.getA_GbetaLocus();
    const Real GBCON = fcn.getB_GbetaLocus();

    const Real Kdl = 0.9;
    const Real HKC = Hk-1.0;
    const Real HR = HKC/(GACON*Kdl*Hk);
    const Real duxcon = 1.0;

    const Real S_lag =
        2.0*ctau*(-0.5*qe/delta_nom * scc*(pow(ctaueq,0.5) - pow(ctau,0.5)*Kdl)
                  -duxcon*(0.5*RetCf1*nue/theta11 - qe*pow(HR,2.0))/(GBCON*delta1s)
                  +duxcon*divq1);

    const ArrayQ sourceTrue = { varInterpret.dummySource_momLami(var),
                                varInterpret.dummySource_keLami(var),
                                -trPTdotgrade + edotgradqdotM - dot(tauw,e1),
                                -2.0*Diss,
                                -nt*divq1,
                                -ctau*divq1 + S_lag };

    ArrayQ source = 0.0;

    pde.source(params,e1,e1x,e1z,x,z,t,var,var_x,var_z,source);
    for ( int i = 0; i < PDEClass::N; i++ )
      SANS_CHECK_CLOSE( sourceTrue[i], source[i], small_tol, close_tol );

    // Source cumulation
    pde.source(params,e1,e1x,e1z,x,z,t,var,var_x,var_z,source);
    for ( int i = 0; i < PDEClass::N; i++ )
      SANS_CHECK_CLOSE( 2*sourceTrue[i], source[i], small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( source_extra_test )
{
  const Real small_tol = 1.E-13;
  const Real close_tol = 1.3E-13;

  const Real gam = 1.4;
  const Real R = 287.15;
  const Real mue = 1.827E-5;

  const Real x = 0.1;
  const Real z = 3.8;
  const Real t = 0.3;

  const Real deltaLami = 2.0e-2, ALami = -0.5, deltaTurb = 3e-2, ATurb = 3.0, nt = 2.3, ctau = 1e-3;

  const Real deltaLami_x = 0.3, ALami_x = 0.5, deltaTurb_x = 1.0, ATurb_x = 2.3, nt_x = 0.8, ctau_x = 0.2;
  const Real deltaLami_z = 0.1, ALami_z = 0.2, deltaTurb_z = 2.0, ATurb_z = 4.3, nt_z = 0.3, ctau_z = 0.3;

  const Real qx = 4.0, qz = 3.0, qx_x = 2.5, qx_z = 5.2, qz_x = 3.6, qz_z = 6.6, p0 = 28.125, T0 = 4.6875;

  const typename PDEClass::ParamInterpType paramInterpret;
  const ArrayParam params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const VectorX e1 = { 0.6, 0.8 };
  const VectorX e1x = { 0.7, 0.3 };
  const VectorX e1z = { 0.5, 0.6 };

  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = gam;
  gasModelDict[GasModelParamsIBL::params.R] = R;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(mue);

  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                        = TransitionModelParams::params.profileCatDefault.turbulentBL;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    const ArrayQ var = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
    const ArrayQ var_x = pde.setDOFFrom(VarDataType(deltaLami_x,ALami_x,deltaTurb_x,ATurb_x,nt_x,ctau_x));
    const ArrayQ var_z = pde.setDOFFrom(VarDataType(deltaLami_z,ALami_z,deltaTurb_z,ATurb_z,nt_z,ctau_z));
    BOOST_CHECK( pde.isValidState(var) );

    const auto& varInterpret = pde.getVarInterpreter();

    const auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();

    const Real qe = paramInterpret.getqe(params);
    const Real rhoe = gasModel.density(qe, p0, T0);
    const Real nue = viscosityModel.dynamicViscosity()/rhoe;

    const auto fcn = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);
    const Real delta1s = fcn.getdelta1s();
    const Real theta11 = fcn.gettheta11();
    const Real deltap = fcn.getdelta1p();
    SANS_CHECK_CLOSE(0.0, delta1s-deltap, small_tol, close_tol); // delta' is zero for incompressible flows
    const Real RetCf1 = fcn.getRetCf1();
    const Real RetCDiss = fcn.getRetCD();

    const Real Hk = fcn.getHk();

    const VectorX q1 = paramInterpret.getq1(params);
    const VectorX tauw = 0.5*rhoe*(RetCf1*nue/theta11)*q1;

    const TensorX gradsq1 = paramInterpret.getgradq1(params);
    const VectorX M = rhoe*delta1s*q1;
    const Real edotgradqdotM = dot(e1, gradsq1*M);
    const Real trPTdotgrade = rhoe*theta11*(qx*qx*e1x[0]+qx*qz*e1x[1]+qz*qx*e1z[0]+qz*qz*e1z[1]);

    const Real Diss = rhoe*pow(qe,2.0)*(RetCDiss*nue/theta11);

    const Real divq1 = paramInterpret.getdivq1(params);

    const Real delta_nom = IBLMiscClosures::getdeltaNominal(Hk, delta1s, theta11);
    const Real ctaueq = fcn.getctaueq();

    const Real GACON = fcn.getA_GbetaLocus();
    const Real GBCON = fcn.getB_GbetaLocus();

    const Real Kdl = 1.0;
#if IS_LAGFACTOR_XFOILPAPER_NOT_XFOILV699_IBLTwoField
    const Real scc = 5.6;
    const Real HKC = Hk - 1.0;
#else
    const Real scc = 5.6*1.333/(1.0 + fcn.getUslip(Hk, fcn.getH(), fcn.getHs()));

    const Real Ret = qe*theta11/nue;
    Real HKC = Hk - 1.0 - 18.0/Ret;
    std::cout << "Unclamped HKC = " << HKC << std::endl;
    BOOST_REQUIRE(HKC > 0.01);
#endif
    const Real HR = HKC/(GACON*Kdl*Hk);
    const Real duxcon = 1.0;

    const Real S_lag =
        2.0*ctau*(-0.5*qe/delta_nom * scc*(pow(ctaueq,0.5) - pow(ctau,0.5)*Kdl)
                  -duxcon*(0.5*RetCf1*nue/theta11 - qe*pow(HR,2.0))/(GBCON*delta1s)
                  +duxcon*divq1);

    const ArrayQ sourceTrue = { varInterpret.dummySource_momLami(var),
                                varInterpret.dummySource_keLami(var),
                                -trPTdotgrade + edotgradqdotM - dot(tauw,e1),
                                -2.0*Diss,
                                -nt*divq1,
                                -ctau*divq1 + S_lag };

    ArrayQ source = 0.0;

    pde.source(params,e1,e1x,e1z,x,z,t,var,var_x,var_z,source);
    for ( int i = 0; i < PDEClass::N; i++ )
      SANS_CHECK_CLOSE( sourceTrue[i], source[i], small_tol, close_tol );

    // Source cumulation
    pde.source(params,e1,e1x,e1z,x,z,t,var,var_x,var_z,source);
    for ( int i = 0; i < PDEClass::N; i++ )
      SANS_CHECK_CLOSE( 2*sourceTrue[i], source[i], small_tol, close_tol );
  }
}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( cutCellTransitionFluxAndSource_transition_test )
{
  const Real small_tol = 1.E-13;
  const Real close_tol = 3.9E-13;

  const Real gam = 1.6;
  const Real R = 66.0;
  const Real mue = 7.0E-5;

  const Real x = 0.1;
  const Real z = 3.8;
  const Real t = 0.3;

  const Real deltaLami = 2.0e-2, ALami = -0.5, deltaTurb = 1.e-2, ATurb = 1.0, nt = 2.3, ctau = 1e-3;
  const Real varmatch_deltaLami = 3.0e-2, varmatch_ALami = 3.0,
             varmatch_deltaTurb = 1.3e-2, varmatch_ATurb = 2.6,
             varmatch_nt = 3.0, varmatch_ctau = 6.6e-3;

  const Real qx = 4.0, qz = 3.0, qx_x = 2.5, qx_z = 5.2, qz_x = 3.6, qz_z = 6.6, p0 = 28.125, T0 = 4.6875;

  const typename PDEClass::ParamInterpType paramInterpret;
  const ArrayParam params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const VectorX e1 = { 0.6, 0.8 };
  const Real nx = 0.8, nz = 0.6;

  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = gam;
  gasModelDict[GasModelParamsIBL::params.R] = R;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(mue);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.ntinit] = 0.0;
  transitionModelDict[TransitionModelParams::params.ntcrit] = 9.0;
  transitionModelDict[TransitionModelParams::params.xtr] = 10.0;
  transitionModelDict[TransitionModelParams::params.isTransitionActive] = true;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                      = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const TransitionModelType transitionModel(transitionModelDict);

  // constructor
  PDEClass pde(gasModel, viscosityModel, transitionModel);

  const ArrayQ var = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
  const ArrayQ varLami = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,1.0,ctau));
  const ArrayQ varTurb = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,9.9,ctau));

  const ArrayQ varMatch = pde.setDOFFrom(VarDataType(varmatch_deltaLami,varmatch_ALami,
                                                        varmatch_deltaTurb,varmatch_ATurb,
                                                        varmatch_nt,varmatch_ctau));
  const ArrayQ varMatchTurb = pde.setDOFFrom(VarDataType(varmatch_deltaLami,varmatch_ALami,
                                                            varmatch_deltaTurb,varmatch_ATurb,
                                                            9.9,varmatch_ctau));

  ArrayQ fTrue = 0.0, sourceMatchingTrue = 0.0;;
  {
    ArrayQ fx = 0.0, fz = 0.0;
    pde.fluxAdvective(params, e1, x, z, t, varLami, fx, fz);
    const Real fmomLami_out = fx[PDEClass::ir_momLami]*nx + fz[PDEClass::ir_momLami]*nz;
    const Real fkeLami_out = fx[PDEClass::ir_keLami]*nx + fz[PDEClass::ir_keLami]*nz;

    ArrayQ fL = 0.0, fR = 0.0;
    pde.fluxAdvectiveUpwind(params, params, e1, e1, x, z, t, varMatchTurb, varTurb, nx, nz, nx, nz, fL, fR);
    const Real fmomTurb_in = fL[PDEClass::ir_momTurb];
    const Real fkeTurb_in = fL[PDEClass::ir_keTurb];
    const Real flag_in = fL[PDEClass::ir_lag];

    fTrue = {fmomLami_out, fkeLami_out, fmomTurb_in, fkeTurb_in, 0.0, flag_in};

    const auto& varInterpret = pde.getVarInterpreter();
    const auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();

    const Real qe = paramInterpret.getqe(params);
    const Real rhoe = gasModel.density(qe, p0, T0);
    const Real nue = viscosityModel.dynamicViscosity()/rhoe;

    const auto& fcnL = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarBL, params, varLami, nue);
    const auto& fcnT = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, varMatch, nue);
//    const auto& fcnR = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, varTurb, nue);

    const Real Hk_L = fcnL.getHk();
    const Real ctaueq_L = fcnL.getctaueq();
    const Real ctau_turbInit = IBLMiscClosures::getctauTurbInit_XFOIL(Hk_L, ctaueq_L);

    sourceMatchingTrue[PDEClass::ir_momLami] += varInterpret.dummySource_momLami(varMatch);
    sourceMatchingTrue[PDEClass::ir_keLami] += varInterpret.dummySource_keLami(varMatch);
    sourceMatchingTrue[PDEClass::ir_momTurb] += fcnT.getdelta1s() - fcnL.getdelta1s();
    sourceMatchingTrue[PDEClass::ir_keTurb] += fcnT.gettheta11() - fcnL.gettheta11();
    sourceMatchingTrue[PDEClass::ir_amp] += varInterpret.dummySource_amp(varMatch);
    sourceMatchingTrue[PDEClass::ir_lag] += varInterpret.getctau(varMatch) - ctau_turbInit;
  }

  // check flux and source
  ArrayQ f = 0.0, sourceMatching = 0.0;

  pde.cutCellTransitionFluxAndSource(params, e1, x, z, t, varMatch, var, nx, nz, f, sourceMatching);
  for ( int i = 0; i < PDEClass::N; i++ )
  {
    SANS_CHECK_CLOSE( fTrue[i], f[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( sourceMatchingTrue[i], sourceMatching[i], small_tol, close_tol );
  }

  // cumulation
  pde.cutCellTransitionFluxAndSource(params, e1, x, z, t, varMatch, var, nx, nz, f, sourceMatching);
  for ( int i = 0; i < PDEClass::N; i++ )
  {
    SANS_CHECK_CLOSE( 2.0*fTrue[i], f[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( 2.0*sourceMatchingTrue[i], sourceMatching[i], small_tol, close_tol );
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( cutCellTransitionFluxAndSource_relaminarization_test )
{
  const Real small_tol = 1.E-13;
  const Real close_tol = 3.9E-13;

  const Real gam = 1.6;
  const Real R = 66.0;
  const Real mue = 7.0E-5;

  const Real x = 0.1;
  const Real z = 3.8;
  const Real t = 0.3;

  const Real deltaLami = 2.0e-2, ALami = -0.5,
             deltaTurb = 1.e-2, ATurb = 1.0,
             nt = 2.3, ctau = 1e-3;
  const Real varmatch_deltaLami = 3.0e-2, varmatch_ALami = 3.0,
             varmatch_deltaTurb = 1.3e-2, varmatch_ATurb = 2.6,
             varmatch_nt = 3.0, varmatch_ctau = 6.6e-3;

  const Real qx = 4.0, qz = 3.0, qx_x = 2.5, qx_z = 5.2, qz_x = 3.6, qz_z = 6.6, p0 = 28.125, T0 = 4.6875;

  const typename PDEClass::ParamInterpType paramInterpret;
  const ArrayParam params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const VectorX e1 = { 0.6, 0.8 };
  const Real nx = -0.8, nz = -0.6;

  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = gam;
  gasModelDict[GasModelParamsIBL::params.R] = R;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(mue);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.ntinit] = 0.0;
  transitionModelDict[TransitionModelParams::params.ntcrit] = 9.0;
  transitionModelDict[TransitionModelParams::params.xtr] = 10.0;
  transitionModelDict[TransitionModelParams::params.isTransitionActive] = true;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                      = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const TransitionModelType transitionModel(transitionModelDict);

  // constructor
  PDEClass pde(gasModel, viscosityModel, transitionModel);

  const ArrayQ var = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
  const ArrayQ varLami = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,1.0,ctau));
  const ArrayQ varTurb = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,9.9,ctau));

  const ArrayQ varMatch = pde.setDOFFrom(VarDataType(varmatch_deltaLami,varmatch_ALami,
                                                     varmatch_deltaTurb,varmatch_ATurb,
                                                     varmatch_nt,varmatch_ctau));
  const ArrayQ varMatchTurb = pde.setDOFFrom(VarDataType(varmatch_deltaLami,varmatch_ALami,
                                                         varmatch_deltaTurb,varmatch_ATurb,
                                                         9.9,varmatch_ctau));

  ArrayQ fTrue = 0.0, sourceMatchingTrue = 0.0;;
  {
    ArrayQ fx = 0.0, fz = 0.0;
    pde.fluxAdvective(params, e1, x, z, t, varMatchTurb, fx, fz);
    const Real fmomLami_in = fx[PDEClass::ir_momTurb]*nx + fz[PDEClass::ir_momTurb]*nz;
    const Real fkeLami_in = fx[PDEClass::ir_keTurb]*nx + fz[PDEClass::ir_keTurb]*nz;

    fx = 0.0;
    fz = 0.0;
    pde.fluxAdvective(params, e1, x, z, t, varTurb, fx, fz);
    const Real fmomTurb_out = fx[PDEClass::ir_momTurb]*nx + fz[PDEClass::ir_momTurb]*nz;
    const Real fkeTurb_out = fx[PDEClass::ir_keTurb]*nx + fz[PDEClass::ir_keTurb]*nz;
    const Real flag_out = fx[PDEClass::ir_lag]*nx + fz[PDEClass::ir_lag]*nz;

    fTrue = {fmomLami_in, fkeLami_in, fmomTurb_out, fkeTurb_out, 0.0, flag_out};

    const auto& varInterpret = pde.getVarInterpreter();
    const auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();

    const Real qe = paramInterpret.getqe(params);
    const Real rhoe = gasModel.density(qe, p0, T0);
    const Real nue = viscosityModel.dynamicViscosity()/rhoe;

    const auto& fcnT = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, varMatch, nue);
    const auto& fcnR = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, varTurb, nue);

    sourceMatchingTrue[PDEClass::ir_momLami] += varInterpret.dummySource_momLami(varMatch);
    sourceMatchingTrue[PDEClass::ir_keLami] += varInterpret.dummySource_keLami(varMatch);
    sourceMatchingTrue[PDEClass::ir_momTurb] += fcnT.getdelta1s() - fcnR.getdelta1s();
    sourceMatchingTrue[PDEClass::ir_keTurb] += fcnT.gettheta11() - fcnR.gettheta11();
    sourceMatchingTrue[PDEClass::ir_amp] += varInterpret.dummySource_amp(varMatch);
//    sourceMatchingTrue[PDEClass::ir_lag] += varInterpret.getctau(varMatch) - varInterpret.getctau(varTurb);
    sourceMatchingTrue[PDEClass::ir_lag] += varInterpret.getctau(varMatch) - pde.varMatchDummy_;
  }

  // check flux and source
  ArrayQ f = 0.0, sourceMatching = 0.0;

  pde.cutCellTransitionFluxAndSource(params, e1, x, z, t, varMatch, var, nx, nz, f, sourceMatching);
  for ( int i = 0; i < PDEClass::N; i++ )
  {
    SANS_CHECK_CLOSE( fTrue[i], f[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( sourceMatchingTrue[i], sourceMatching[i], small_tol, close_tol );
  }

  // cumulation
  pde.cutCellTransitionFluxAndSource(params, e1, x, z, t, varMatch, var, nx, nz, f, sourceMatching);
  for ( int i = 0; i < PDEClass::N; i++ )
  {
    SANS_CHECK_CLOSE( 2.0*fTrue[i], f[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( 2.0*sourceMatchingTrue[i], sourceMatching[i], small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( cutCellTransitionFluxAndSource_stagnation_test )
{
  const Real gam = 1.6;
  const Real R = 66.0;
  const Real mue = 7.0E-5;

  const Real x = 0.1;
  const Real z = 3.8;
  const Real t = 0.3;

  const Real deltaLami = 2.0e-2, ALami = -0.5,
             deltaTurb = 1.e-2, ATurb = 1.0,
             nt = 2.3, ctau = 1e-3;
  const Real varmatch_deltaLami = 3.0e-2, varmatch_ALami = 3.0,
             varmatch_deltaTurb = 1.3e-2, varmatch_ATurb = 2.6,
             varmatch_nt = 3.0, varmatch_ctau = 6.6e-3;

  const Real qx = 0.0, qz = 0.0, qx_x = 2.5, qx_z = 5.2, qz_x = 3.6, qz_z = 6.6, p0 = 28.125, T0 = 4.6875;

  const typename PDEClass::ParamInterpType paramInterpret;
  const ArrayParam params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const VectorX e1 = { 0.6, 0.8 };
  const Real nx = -0.8, nz = -0.6;

  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = gam;
  gasModelDict[GasModelParamsIBL::params.R] = R;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(mue);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.ntinit] = 0.0;
  transitionModelDict[TransitionModelParams::params.ntcrit] = 9.0;
  transitionModelDict[TransitionModelParams::params.xtr] = 10.0;
  transitionModelDict[TransitionModelParams::params.isTransitionActive] = true;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                      = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const TransitionModelType transitionModel(transitionModelDict);

  // constructor
  PDEClass pde(gasModel, viscosityModel, transitionModel);

  const ArrayQ var = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
  const ArrayQ varMatch = pde.setDOFFrom(VarDataType(varmatch_deltaLami,varmatch_ALami,
                                                     varmatch_deltaTurb,varmatch_ATurb,
                                                     varmatch_nt,varmatch_ctau));

  // check flux and source
  ArrayQ f = 0.0, sourceMatching = 0.0;
  BOOST_CHECK_THROW(pde.cutCellTransitionFluxAndSource(params, e1, x, z, t, varMatch, var, nx, nz, f, sourceMatching);, DeveloperException);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( sourceMatchNonTransitionDummy_test )
{
  const Real small_tol = 1.E-13;
  const Real close_tol = 1.E-13;

  const ArrayQ varMatch = {1., 2., 3., 4., 5., 6.};

  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(1.827e-5);
  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.turbulentWake;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const TransitionModelType transitionModel(transitionModelDict);

  PDEClass pde(gasModel, viscosityModel, transitionModel);

  const auto& varInterpret = pde.getVarInterpreter();
  ArrayQ sourceMatchingTrue = 0.0;
  sourceMatchingTrue[PDEClass::ir_momLami] += varInterpret.dummySource_momLami(varMatch);
  sourceMatchingTrue[PDEClass::ir_keLami] += varInterpret.dummySource_keLami(varMatch);
  sourceMatchingTrue[PDEClass::ir_momTurb] += varInterpret.dummySource_momTurb(varMatch);
  sourceMatchingTrue[PDEClass::ir_keTurb] += varInterpret.dummySource_keTurb(varMatch);
  sourceMatchingTrue[PDEClass::ir_amp] += varInterpret.dummySource_amp(varMatch);
  sourceMatchingTrue[PDEClass::ir_lag] += varInterpret.dummySource_lag(varMatch);

  ArrayQ sourceMatching = 0.0;

  pde.sourceMatchNonTransitionDummy(varMatch, sourceMatching);
  for ( int i = 0; i < PDEClass::N; i++ )
    SANS_CHECK_CLOSE( sourceMatchingTrue[i], sourceMatching[i], small_tol, close_tol );

  pde.sourceMatchNonTransitionDummy(varMatch, sourceMatching);
  for ( int i = 0; i < PDEClass::N; i++ )
    SANS_CHECK_CLOSE( 2.0*sourceMatchingTrue[i], sourceMatching[i], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( sourceTurbAmp_test )
{
  const Real small_tol = 1.E-13;
  const Real close_tol = 1.3E-13;

  const Real gam = 1.6;
  const Real R = 66.0;
  const Real mue = 7.0E-5;

  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = gam;
  gasModelDict[GasModelParamsIBL::params.R] = R;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(mue);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.isTransitionActive] = true;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const TransitionModelType transitionModel(transitionModelDict);

  PDEClass pde(gasModel, viscosityModel, transitionModel);


  const Real x = 0.1;
  const Real z = 3.8;
  const Real t = 0.3;

  const Real deltaLami = 2.0e-2, ALami = -0.5, deltaTurb = 1.e-2, ATurb = 1.0, nt = 2.3, ctau = 1e-3;
  const ArrayQ var = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));
  BOOST_CHECK( pde.isValidState(var) );

  const Real qx = 4.0, qz = 3.0, qx_x = 2.5, qx_z = 5.2, qz_x = 3.6, qz_z = 6.6, p0 = 28.125, T0 = 4.6875;
  const auto& paramInterpret = pde.getParamInterpreter();
  const ArrayParam params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const VectorX e1 = { 0.6, 0.8 };

  const auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();

  const Real qe = paramInterpret.getqe(params);
  const Real rhoe = gasModel.density(qe, p0, T0);
  const Real nue = viscosityModel.dynamicViscosity()/rhoe;

  const auto fcn = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarBL, params, var, nue);
  const Real theta11 = fcn.gettheta11();
  const Real Hk = fcn.getHk();

  const Real Ret = qe*theta11/nue;
  const Real amprate = IBLMiscClosures::AmplificationClosure_IBL::ampGrowthRate(Hk, Ret, theta11);

  const Real source_turbAmp_True = -qe*amprate;

  Real source = 0.0;

  pde.sourceTurbAmp(params, e1, x, z, t, var, source);
  SANS_CHECK_CLOSE( source_turbAmp_True, source, small_tol, close_tol );

  pde.sourceTurbAmp(params, e1, x, z, t, var, source); // Source cumulation
  SANS_CHECK_CLOSE( 2.0*source_turbAmp_True, source, small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
