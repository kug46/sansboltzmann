// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "SANS_btest.h"

#include "pde/IBL/IBLMiscClosures.h"

namespace SANS
{

namespace AmpClosureIBL
{
Real get_ampramp(const Real& Hk, const Real& Ret)
{
  const Real GRTinit = 5.738/pow(Hk-1,0.43) + 1.612*(tanh(14.0/(Hk-1.0) - 9.24) + 1.0);
  const Real ramp = 0.5 + 0.5*tanh(10.0*(log(Ret)-GRTinit));
  return ramp;
}

Real get_amprate_unramped(const Real& Hk, const Real& theta11)
{
  const Real tmp = Hk-1.0;
  const Real dntdRet = 0.028*tmp - 0.0345*exp(-pow(3.87/tmp-2.52, 2.0));
  const Real thetadRetds = -0.05 + 2.7/tmp - 5.5/pow(tmp,2.0) + 3.0/pow(tmp,3.0) + 0.1*exp(-20.0/tmp);
  const Real amprate = dntdRet*thetadRetds/theta11;
  return amprate;
}
} // namespace AmpClosureIBL

namespace AmpClosureXFOIL
{
Real get_ampramp(const Real& Hk, const Real& Ret)
{
  const Real HMI = 1.0/(Hk - 1.0);
  const Real AA = 2.492*pow(HMI,0.43);
  const Real BB = tanh(14.0*HMI - 9.24);

  const Real GRCRIT = AA + 0.7*(BB+1.0);
  const Real GR = log10(Ret);

  const Real DGR = 0.08;

  Real ramp = 0.0;
  if (GR < GRCRIT-DGR)
    ramp = 0.0;
  else
  {
    const Real RNORM = (GR - (GRCRIT-DGR)) / (2.0*DGR);
    if (RNORM >= 1.0)
      ramp = 1.0;
    else
      ramp = 3.0*pow(RNORM,2.0) - 2.0*pow(RNORM,3.0);
  }

  return ramp;
}

Real get_amprate_unramped(const Real& Hk, const Real& theta11)
{
  const Real HMI = 1.0/(Hk - 1.0);
  const Real ARG = 3.87*HMI - 2.52;
  const Real EX = exp(-pow(ARG,2.0));
  const Real DADR = 0.028*(Hk-1.0) - 0.0345*EX;
  const Real AF = -0.05 + 2.7*HMI - 5.5*pow(HMI,2.0) + 3.0*pow(HMI,3.0);
  const Real amprate = AF*DADR/theta11;
  return amprate;
}
} // namespace AmpClosureXFOIL

Real get_deltaNominal(const Real& delta1s, const Real& theta11, const Real& Hk)
{
  Real delta_nom = (3.15+1.72/(Hk-1.0))*theta11 + delta1s;

  const Real HDMAX = 12.0;
  if (delta_nom > HDMAX*theta11)
    delta_nom = HDMAX*theta11;

  return delta_nom;
}
}
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE (IBLMiscClosures_test_suite)

BOOST_AUTO_TEST_CASE( AmplificationGrowth_IBL3document_test )
{
  const Real small_tol = 1e-15;
  const Real close_tol = 3.2e-13;

  {
    const Real Hk = 2.0;
    const Real Ret = 500.0;
    const Real theta11 = 0.005;

    const Real ramp = AmpClosureIBL::get_ampramp(Hk, Ret);
    const Real amprate_unramped = AmpClosureIBL::get_amprate_unramped(Hk, theta11);
    const Real amprate = amprate_unramped * ramp;

#if defined(SANS_VERBOSE)
    std::cout << "Amplification growth ramp = " << ramp << std::endl;
#endif
    BOOST_REQUIRE(!(ramp > 0));

    SANS_CHECK_CLOSE(ramp, IBLMiscClosures::AmplificationClosure_IBL::ampGrowthOnsetRamp(Hk, Ret),
                     small_tol, close_tol);
    SANS_CHECK_CLOSE(amprate_unramped, IBLMiscClosures::AmplificationClosure_IBL::ampGrowthRateUnramped(Hk, theta11),
                     small_tol, close_tol);
    SANS_CHECK_CLOSE(amprate, IBLMiscClosures::AmplificationClosure_IBL::ampGrowthRate(Hk, Ret, theta11),
                     small_tol, close_tol);
  }

  {
    const Real Hk = 2.4;
    const Real Ret = 2000.0;
    const Real theta11 = 0.005;

    const Real ramp = AmpClosureIBL::get_ampramp(Hk, Ret);
    const Real amprate_unramped = AmpClosureIBL::get_amprate_unramped(Hk, theta11);
    const Real amprate = amprate_unramped * ramp;

#if defined(SANS_VERBOSE)
    std::cout << "Amplification growth ramp = " << ramp << std::endl;
#endif
    BOOST_REQUIRE((ramp > 0) && (ramp <1.0));

    SANS_CHECK_CLOSE(ramp, IBLMiscClosures::AmplificationClosure_IBL::ampGrowthOnsetRamp(Hk, Ret),
                     small_tol, close_tol);
    SANS_CHECK_CLOSE(amprate_unramped, IBLMiscClosures::AmplificationClosure_IBL::ampGrowthRateUnramped(Hk, theta11),
                     small_tol, close_tol);
    SANS_CHECK_CLOSE(amprate, IBLMiscClosures::AmplificationClosure_IBL::ampGrowthRate(Hk, Ret, theta11),
                     small_tol, close_tol);
  }

  {
    const Real Hk = 2.8;
    const Real Ret = 2000.0;
    const Real theta11 = 0.005;

    const Real ramp = AmpClosureIBL::get_ampramp(Hk, Ret);
    const Real amprate_unramped = AmpClosureIBL::get_amprate_unramped(Hk, theta11);
    const Real amprate = amprate_unramped * ramp;

#if defined(SANS_VERBOSE)
    std::cout << "Amplification growth ramp = " << ramp << std::endl;
#endif
    BOOST_REQUIRE(!(ramp < 1));

    SANS_CHECK_CLOSE(ramp, IBLMiscClosures::AmplificationClosure_IBL::ampGrowthOnsetRamp(Hk, Ret),
                     small_tol, close_tol);
    SANS_CHECK_CLOSE(amprate_unramped, IBLMiscClosures::AmplificationClosure_IBL::ampGrowthRateUnramped(Hk, theta11),
                     small_tol, close_tol);
    SANS_CHECK_CLOSE(amprate, IBLMiscClosures::AmplificationClosure_IBL::ampGrowthRate(Hk, Ret, theta11),
                     small_tol, close_tol);
  }
}

BOOST_AUTO_TEST_CASE( AmplificationGrowth_XFOIL_test )
{
  const Real small_tol = 1e-15;
  const Real close_tol = 1e-15;

  {
    const Real Hk = 2.0;
    const Real Ret = 500.0;
    const Real theta11 = 0.005;

    const Real ramp = AmpClosureXFOIL::get_ampramp(Hk, Ret);
    const Real amprate_unramped = AmpClosureXFOIL::get_amprate_unramped(Hk, theta11);
    const Real amprate = amprate_unramped * ramp;

#if defined(SANS_VERBOSE)
    std::cout << "Amplification growth ramp = " << ramp << std::endl;
#endif
    BOOST_REQUIRE(!(ramp > 0));

    SANS_CHECK_CLOSE(ramp, IBLMiscClosures::AmplificationClosure_XfoilOriginal::ampGrowthOnsetRamp(Hk, Ret),
                     small_tol, close_tol);
    SANS_CHECK_CLOSE(amprate_unramped, IBLMiscClosures::AmplificationClosure_XfoilOriginal::ampGrowthRateUnramped(Hk, theta11),
                     small_tol, close_tol);
    SANS_CHECK_CLOSE(amprate, IBLMiscClosures::AmplificationClosure_XfoilOriginal::ampGrowthRate(Hk, Ret, theta11),
                     small_tol, close_tol);
  }

  {
    const Real Hk = 2.4;
    const Real Ret = 2000.0;
    const Real theta11 = 0.005;

    const Real ramp = AmpClosureXFOIL::get_ampramp(Hk, Ret);
    const Real amprate_unramped = AmpClosureXFOIL::get_amprate_unramped(Hk, theta11);
    const Real amprate = amprate_unramped * ramp;

#if defined(SANS_VERBOSE)
    std::cout << "Amplification growth ramp = " << ramp << std::endl;
#endif
    BOOST_REQUIRE((ramp > 0) && (ramp <1.0));

    SANS_CHECK_CLOSE(ramp, IBLMiscClosures::AmplificationClosure_XfoilOriginal::ampGrowthOnsetRamp(Hk, Ret),
                     small_tol, close_tol);
    SANS_CHECK_CLOSE(amprate_unramped, IBLMiscClosures::AmplificationClosure_XfoilOriginal::ampGrowthRateUnramped(Hk, theta11),
                     small_tol, close_tol);
    SANS_CHECK_CLOSE(amprate, IBLMiscClosures::AmplificationClosure_XfoilOriginal::ampGrowthRate(Hk, Ret, theta11),
                     small_tol, close_tol);
  }

  {
    const Real Hk = 2.8;
    const Real Ret = 2000.0;
    const Real theta11 = 0.005;

    const Real ramp = AmpClosureXFOIL::get_ampramp(Hk, Ret);
    const Real amprate_unramped = AmpClosureXFOIL::get_amprate_unramped(Hk, theta11);
    const Real amprate = amprate_unramped * ramp;

#if defined(SANS_VERBOSE)
    std::cout << "Amplification growth ramp = " << ramp << std::endl;
#endif
    BOOST_REQUIRE(!(ramp < 1));

    SANS_CHECK_CLOSE(ramp, IBLMiscClosures::AmplificationClosure_XfoilOriginal::ampGrowthOnsetRamp(Hk, Ret),
                     small_tol, close_tol);
    SANS_CHECK_CLOSE(amprate_unramped, IBLMiscClosures::AmplificationClosure_XfoilOriginal::ampGrowthRateUnramped(Hk, theta11),
                     small_tol, close_tol);
    SANS_CHECK_CLOSE(amprate, IBLMiscClosures::AmplificationClosure_XfoilOriginal::ampGrowthRate(Hk, Ret, theta11),
                     small_tol, close_tol);
  }
}

BOOST_AUTO_TEST_CASE( deltaNominal_test )
{
  const Real small_tol = 1e-15;
  const Real close_tol = 1e-15;

  {
    const Real delta1s = 0.02;
    const Real theta11 = 0.01;
    const Real Hk = delta1s/theta11; // true for incompressible only

    const Real delta_nom = get_deltaNominal(delta1s, theta11, Hk);

#if defined(SANS_VERBOSE)
    std::cout << "delta_nom/theta11 = " << delta_nom/theta11 << std::endl;
#endif
    BOOST_REQUIRE(delta_nom/theta11 < 12.0);
    SANS_CHECK_CLOSE(delta_nom, IBLMiscClosures::getdeltaNominal(Hk, delta1s, theta11),
                     small_tol, close_tol);
  }

  {
    const Real delta1s = 0.011;
    const Real theta11 = 0.01;
    const Real Hk = delta1s/theta11; // true for incompressible only

    const Real delta_nom = get_deltaNominal(delta1s, theta11, Hk);

#if defined(SANS_VERBOSE)
    std::cout << "delta_nom/theta11 = " << delta_nom/theta11 << std::endl;
#endif
    BOOST_REQUIRE(delta_nom/theta11 == 12.0);
    SANS_CHECK_CLOSE(delta_nom, IBLMiscClosures::getdeltaNominal(Hk, delta1s, theta11),
                     small_tol, close_tol);
  }
}

BOOST_AUTO_TEST_CASE( getctauTurbInit_XFOIL_test )
{
  const Real small_tol = 1e-15;
  const Real close_tol = 1e-15;

  const Real Hk = 2.0;
  const Real ctaueq = 0.01;
  const Real ctauTurbInitXFOIL_True = pow(1.8*exp(-3.3/(Hk-1.0)),2.0)*ctaueq;

  SANS_CHECK_CLOSE(ctauTurbInitXFOIL_True, IBLMiscClosures::getctauTurbInit_XFOIL(Hk, ctaueq),
                   small_tol, close_tol);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END ()
