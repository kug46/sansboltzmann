// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// SetSolnDofCell_IBL_btest

//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>

#include <iomanip>

#include "SANS_btest.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/tools/for_each_CellGroup.h"

#include "pde/IBL/GetSolnDofCell_IBL2D.h"

#include "unit/UnitGrids/XField2D_Line_X1_1Group.h"

using namespace std;

namespace SANS
{
typedef PhysD2 PhysDim;
typedef TopoD1 TopoDim;
typedef VarTypeDANCt VarType;
typedef VarInterpret2D<VarType> VarInterpType;
typedef GetSolnDofCell_IBL2D_impl<VarType,TopoDim> FunctorType;
typedef typename GetSolnDofCell_IBL2D_impl<VarType,TopoDim>::RefCoordType RefCoordType;
typedef typename FunctorType::ArrayQ ArrayQ;

typedef DLA::VectorS<PhysDim::D,Real> VectorX;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct
}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( GetSolnDofCell_IBL_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_ctor_test )
{
  // check
  BOOST_CHECK( FunctorType::D == PhysDim::D );
  BOOST_CHECK( FunctorType::N == IBLTraits<PhysDim>::N );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( GetSolnDofCell_IBL2D_Q0_test )
{
  const Real tol = 1.e-13;

  const int N = FunctorType::N; // state/solution vector dimension

  VarInterpType varInterpret;

  // grid field
  std::vector<VectorX> coordinates(3);
  coordinates.at(0) = {-1,0};
  coordinates.at(1) = { 0,0};
  coordinates.at(2) = { 1,0};
  XField2D_Line_X1_1Group xfld(coordinates);

  // solution field
  const int order = 0;
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld.DOF(0) = varInterpret.setDOFFrom<Real>( VarData2DDANCt(0.1, 1.2, 0.05, 1.0, 2.3, 1.2) );
  qfld.DOF(1) = varInterpret.setDOFFrom<Real>( VarData2DDANCt(0.6, 2.5, 0.08, 1.6, 2.3, 1.2) );

  // get solution DOFs
  const std::vector<RefCoordType> sRefArr = {0, 0.6, 1};
  const std::vector<int> CellGroups = {0};
  std::vector<std::vector<ArrayQ> > qdata;
  for_each_CellGroup<TopoD1>::apply( GetSolnDofCell_IBL2D<VarType,TopoDim>(sRefArr, CellGroups, qdata), (xfld, qfld) );

  // Check
  const int npt = sRefArr.size();
  for (int group=0; group<(int)qdata.size(); ++group)
  {
    const int nelem = qdata.at(group).size() / npt;
    for (int elem=0; elem<nelem; ++elem)
    {
      for (int pt=0; pt<npt; ++pt)
      {
        const int indx = npt*elem + pt;
#ifdef SANS_VERBOSE
        std::cout << std::setprecision(16);
        std::cout << qdata.at(group).at(elem) << std::endl;
#endif
        for (int n=0; n<N; ++n)
          SANS_CHECK_CLOSE(qfld.DOF(elem)[n], qdata.at(group).at(indx)[n], tol, tol);
      }
    }
  }
#ifdef SANS_VERBOSE
  std::cout << std::endl;
#endif
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( GetSolnDofCell_IBL2D_Q1_test )
{
  const Real tol = 1.e-13;

  const int N = FunctorType::N; // state/solution vector dimension

  VarInterpType varInterpret;

  // grid field
  std::vector<VectorX> coordinates(3);
  coordinates.at(0) = {-1,0};
  coordinates.at(1) = { 0,0};
  coordinates.at(2) = { 1,0};
  XField2D_Line_X1_1Group xfld(coordinates);

  // solution field
  const int order = 1;
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);
  qfld.DOF(0) = varInterpret.setDOFFrom<Real>( VarData2DDANCt(0.1, 1.2, 0.28, 4.0, 2.3, 1.2) );
  qfld.DOF(1) = varInterpret.setDOFFrom<Real>( VarData2DDANCt(0.2, 1.3, 0.30, 3.0, 2.3, 1.2) );
  qfld.DOF(2) = varInterpret.setDOFFrom<Real>( VarData2DDANCt(0.3, 1.4, 0.16, 2.0, 2.3, 1.2) );
  qfld.DOF(3) = varInterpret.setDOFFrom<Real>( VarData2DDANCt(0.4, 1.5, 0.59, 8.0, 2.3, 1.2) );

  // get solution DOFs
  const std::vector<RefCoordType> sRefArr = {0, 0.6, 1};
  const std::vector<int> CellGroups = {0};
  std::vector<std::vector<ArrayQ> > qdata;
  for_each_CellGroup<TopoD1>::apply( GetSolnDofCell_IBL2D<VarType,TopoDim>(sRefArr, CellGroups, qdata), (xfld, qfld) );

  // Check
  const int npt = sRefArr.size();
  for (int group=0; group<(int)qdata.size(); ++group)
  {
    const int nelem = qdata.at(group).size() / npt;
    for (int elem=0; elem<nelem; ++elem)
    {
      for (int pt=0; pt<npt; ++pt)
      {
        const int indx = npt*elem + pt;
#ifdef SANS_VERBOSE
        std::cout << std::setprecision(16);
        std::cout << qdata.at(group).at(indx) << std::endl;
#endif
        ArrayQ qTrue = qfld.DOF(2*elem) + sRefArr.at(pt)[0]*( qfld.DOF(2*elem+1) - qfld.DOF(2*elem) );
        for (int n=0; n<N; ++n)
          SANS_CHECK_CLOSE(qTrue[n], qdata.at(group).at(indx)[n], tol, tol);
      }
    }
  }
#ifdef SANS_VERBOSE
  std::cout << std::endl;
#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
