// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// FluidModels_btest
//
// test of fluid models used in IBL

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/IBL/FluidModels.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( GasModel_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( GasModelParamsIBL_Dict_test )
{
  BOOST_CHECK_EQUAL(1.4, GasModelParamsIBL::params.gamma.Default);
  BOOST_CHECK_EQUAL(287.15, GasModelParamsIBL::params.R.Default);

  // check bounds on gamma and R
  Real gamma, R;

  { // valid
    gamma = 1.4;
    R = 287.15;

    PyDict gasModelDict;
    gasModelDict[GasModelParamsIBL::params.gamma] = gamma;
    gasModelDict[GasModelParamsIBL::params.R] = R;

    BOOST_CHECK_NO_THROW((GasModelParamsIBL::checkInputs(gasModelDict)));
  }

  const std::vector<Real> gamma_invalid_arr = {0.9, 1.7};
  for (size_t i = 0; i < gamma_invalid_arr.size(); ++i)
  {
    gamma = gamma_invalid_arr.at(i);
    R = 287.15;

    PyDict gasModelDict;
    gasModelDict[GasModelParamsIBL::params.gamma] = gamma;
    gasModelDict[GasModelParamsIBL::params.R] = R;

    BOOST_CHECK_THROW((GasModelParamsIBL::checkInputs(gasModelDict)), SANSException);
  }

  const std::vector<Real> R_invalid_arr = {-1.0, 0.0};
  for (size_t i = 0; i < R_invalid_arr.size(); ++i)
  {
    gamma = 1.4;
    R = R_invalid_arr.at(i);

    PyDict gasModelDict;
    gasModelDict[GasModelParamsIBL::params.gamma] = gamma;
    gasModelDict[GasModelParamsIBL::params.R] = R;

    BOOST_CHECK_THROW((GasModelParamsIBL::checkInputs(gasModelDict)), SANSException);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_accessor_test )
{
  Real gamma, R;

  { // PyDict constructor
    gamma = 1.4;
    R = 287.15;

    PyDict gasModelDict;
    gasModelDict[GasModelParamsIBL::params.gamma] = gamma;
    gasModelDict[GasModelParamsIBL::params.R] = R;

    GasModelParamsIBL::checkInputs(gasModelDict);

    GasModelIBL gasModel(gasModelDict);

    BOOST_CHECK_EQUAL(gamma, gasModel.gamma());
    BOOST_CHECK_EQUAL(R, gasModel.R());

    // should fail when parameter is invalid
    gasModelDict[GasModelParamsIBL::params.gamma] = 0.7;
    BOOST_CHECK_THROW((GasModelIBL(gasModelDict)), SANSException);
  }

  { // value constructor
    gamma = 1.4;
    R = 287.15;

    GasModelIBL gasModel(gamma,R);

    BOOST_CHECK_EQUAL(gamma, gasModel.gamma());
    BOOST_CHECK_EQUAL(R, gasModel.R());

    // should fail when parameter is invalid
    gamma = 0.7;
    BOOST_CHECK_THROW((GasModelIBL(gamma,R)), SANSException);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( density_test )
{
  const Real small_tol = 1.E-15;
  const Real close_tol = 3.E-14;

  const Real gamma = 1.4;
  const Real R   = 287.15;

  GasModelIBL gasModel(gamma,R);

  const Real p0 = 1.e5, T0 = 300.0;

  { // incompressible limit
    const Real q = 0;

    const Real rho_true = p0/(R*T0);  // ideal gas law @ incompressible limit Mach = 0
    const Real rho = gasModel.density(q,p0,T0);

    SANS_CHECK_CLOSE( rho_true, rho, small_tol, close_tol );
  }

  { // compressible flow
    const Real M = 0.6;
    const Real T = T0 / ( 1 + 0.5*(gamma-1)*M*M ); // T0/T = 1 + 0.5*(gamma-1)*M^2
    const Real q = M * sqrt(gamma*R*T);            // q = M*sqrt(gamma*R*T)

    Real p = p0 * pow( 1.0 + 0.5*(gamma-1.0) * pow(M,2.0), -gamma/(gamma-1.0) ); // static pressure

    // results
    const Real rho_true = p/(R*T);
    const Real rho = gasModel.density(q,p0,T0);

    SANS_CHECK_CLOSE( rho_true, rho, small_tol, close_tol );
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO_test )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/IBL/GasModel_pattern.txt", true );

  GasModelIBL gas(1.4, 287.15);

  gas.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

BOOST_AUTO_TEST_SUITE_END()
//############################################################################//

//############################################################################//
BOOST_AUTO_TEST_SUITE( ViscosityModel_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( miscellaneous_test )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/IBL/ViscosityModel_pattern.txt", true );

  const Real mu = 1.827e-5;
  ViscosityModel<ViscosityConstant> viscosity(mu);

  viscosity.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  // parameter inquiry
  BOOST_CHECK_EQUAL(mu, viscosity.dynamicViscosity());

  { // ctor validity check
    BOOST_CHECK_THROW(ViscosityModel<ViscosityConstant>(0.0);, SANSException);
  }
}

BOOST_AUTO_TEST_SUITE_END()
//############################################################################//

//############################################################################//
BOOST_AUTO_TEST_SUITE( TransitionModel_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( TransitionModelParam_Dict_ctor_test )
{
  BOOST_CHECK_EQUAL(0.0, TransitionModelParams::params.ntinit.Default);
  BOOST_CHECK_EQUAL(9.0, TransitionModelParams::params.ntcrit.Default);
  BOOST_CHECK_EQUAL(1.e+5, TransitionModelParams::params.xtr.Default);
  BOOST_CHECK_EQUAL(false, TransitionModelParams::params.isTransitionActive.Default);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( TransitionModel_ctor_test )
{
  Real ntinit = 0.5, ntcrit = 6.6, xtr = 0.2;
  bool isTransitionActive;
  IBLProfileCategory profileCatDefault;

  { // PyDict
    isTransitionActive = true;
    profileCatDefault = IBLProfileCategory::LaminarBL;

    PyDict trModelDict;
    trModelDict[TransitionModelParams::params.ntinit] = ntinit;
    trModelDict[TransitionModelParams::params.ntcrit] = ntcrit;
    trModelDict[TransitionModelParams::params.xtr] = xtr;
    trModelDict[TransitionModelParams::params.isTransitionActive] = isTransitionActive;
    trModelDict[TransitionModelParams::params.profileCatDefault]
                = TransitionModelParams::params.profileCatDefault.laminarBL;

    TransitionModel trModel(trModelDict);

    BOOST_CHECK_EQUAL(ntinit, trModel.ntinit());
    BOOST_CHECK_EQUAL(ntcrit, trModel.ntcrit());
    BOOST_CHECK_EQUAL(xtr, trModel.xtr());
    BOOST_CHECK_EQUAL(isTransitionActive, trModel.isTransitionActive());
    BOOST_CHECK(profileCatDefault == trModel.getProfileCategoryDefault());
  }

  {
    PyDict trModelDict;

    trModelDict[TransitionModelParams::params.isTransitionActive] = true;
    trModelDict[TransitionModelParams::params.profileCatDefault]
                = TransitionModelParams::params.profileCatDefault.turbulentBL;

    BOOST_CHECK_THROW(TransitionModel trModel(trModelDict);, SANSException);

    trModelDict[TransitionModelParams::params.isTransitionActive] = false;

    TransitionModel trModel(trModelDict);

    BOOST_CHECK(IBLProfileCategory::TurbulentBL == trModel.getProfileCategoryDefault());
  }

  {
    PyDict trModelDict;

    trModelDict[TransitionModelParams::params.isTransitionActive] = true;
    trModelDict[TransitionModelParams::params.profileCatDefault]
                = TransitionModelParams::params.profileCatDefault.laminarWake;

    BOOST_CHECK_THROW(TransitionModel trModel(trModelDict);, SANSException);

    trModelDict[TransitionModelParams::params.isTransitionActive] = false;

    TransitionModel trModel(trModelDict);

    BOOST_CHECK(IBLProfileCategory::LaminarWake == trModel.getProfileCategoryDefault());
  }

  {
    PyDict trModelDict;

    trModelDict[TransitionModelParams::params.isTransitionActive] = true;
    trModelDict[TransitionModelParams::params.profileCatDefault]
                = TransitionModelParams::params.profileCatDefault.turbulentWake;

    BOOST_CHECK_THROW(TransitionModel trModel(trModelDict);, SANSException);

    trModelDict[TransitionModelParams::params.isTransitionActive] = false;

    TransitionModel trModel(trModelDict);

    BOOST_CHECK(IBLProfileCategory::TurbulentWake == trModel.getProfileCategoryDefault());
  }

  {
    PyDict trModelDict;

    trModelDict[TransitionModelParams::params.profileCatDefault] = "Invalid option";

    BOOST_CHECK_THROW(TransitionModel trModel(trModelDict);, SANSException);
  }

  { // value & copy
    isTransitionActive = false;
    profileCatDefault = IBLProfileCategory::TurbulentBL;

    TransitionModel trModel(profileCatDefault, ntinit, ntcrit, xtr, isTransitionActive);

    BOOST_CHECK_EQUAL(ntinit, trModel.ntinit());
    BOOST_CHECK_EQUAL(ntcrit, trModel.ntcrit());
    BOOST_CHECK_EQUAL(xtr, trModel.xtr());
    BOOST_CHECK_EQUAL(isTransitionActive, trModel.isTransitionActive());
    BOOST_CHECK(profileCatDefault == trModel.getProfileCategoryDefault());
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( transitionFlags_test )
{
  Real ntinit = 0.0, ntcrit = 9.0, xtr = 0.2;
  bool isTransitionActive = true;
  IBLProfileCategory profileCatDefault = IBLProfileCategory::LaminarBL;

  TransitionModel trModel(profileCatDefault, ntinit, ntcrit, xtr, isTransitionActive);

  BOOST_CHECK( !trModel.isNaturalTransition(ntcrit-0.1) );
  BOOST_CHECK( trModel.isNaturalTransition(ntcrit) );
  BOOST_CHECK( trModel.isNaturalTransition(ntcrit+0.1) );

  BOOST_CHECK( !trModel.isForcedTransition(xtr-0.1) );
  BOOST_CHECK( trModel.isForcedTransition(xtr) );
  BOOST_CHECK( trModel.isForcedTransition(xtr+0.1) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( getProfileCategory_binaryIntermittency_test )
{
  Real ntinit = 0.0, ntcrit = 9.0, xtr = 0.2;
  bool isTransitionActive;
  IBLProfileCategory profileCatDefault;

  {
    isTransitionActive = true;
    profileCatDefault = IBLProfileCategory::LaminarBL;
    TransitionModel trModel(profileCatDefault, ntinit, ntcrit, xtr, isTransitionActive);

    BOOST_CHECK(IBLProfileCategory::LaminarBL == trModel.getProfileCategory(ntcrit-0.1, xtr-0.1));
    BOOST_CHECK(IBLProfileCategory::TurbulentBL == trModel.getProfileCategory(ntcrit+0.1, xtr-0.1));
    BOOST_CHECK(IBLProfileCategory::TurbulentBL == trModel.getProfileCategory(ntcrit-0.1, xtr+0.1));
    BOOST_CHECK(IBLProfileCategory::TurbulentBL == trModel.getProfileCategory(ntcrit+0.1, xtr+0.1));

    BOOST_CHECK_EQUAL(0.0, trModel.binaryIntermittency(ntcrit-0.1, xtr-0.1));
    BOOST_CHECK_EQUAL(1.0, trModel.binaryIntermittency(ntcrit+0.1, xtr-0.1));
  }

  {
    isTransitionActive = false;
    profileCatDefault = IBLProfileCategory::LaminarBL;
    TransitionModel trModel(profileCatDefault, ntinit, ntcrit, xtr, isTransitionActive);

    BOOST_CHECK(IBLProfileCategory::LaminarBL == trModel.getProfileCategory(ntcrit-0.1, xtr-0.1));
    BOOST_CHECK(IBLProfileCategory::LaminarBL == trModel.getProfileCategory(ntcrit+0.1, xtr-0.1));
    BOOST_CHECK(IBLProfileCategory::LaminarBL == trModel.getProfileCategory(ntcrit-0.1, xtr+0.1));
    BOOST_CHECK(IBLProfileCategory::LaminarBL == trModel.getProfileCategory(ntcrit+0.1, xtr+0.1));
  }

  {
    isTransitionActive = false;
    profileCatDefault = IBLProfileCategory::TurbulentBL;
    TransitionModel trModel(profileCatDefault, ntinit, ntcrit, xtr, isTransitionActive);

    BOOST_CHECK(profileCatDefault == trModel.getProfileCategory(ntcrit-0.1, xtr-0.1));
    BOOST_CHECK(profileCatDefault == trModel.getProfileCategory(ntcrit+0.1, xtr-0.1));
    BOOST_CHECK(profileCatDefault == trModel.getProfileCategory(ntcrit-0.1, xtr+0.1));
    BOOST_CHECK(profileCatDefault == trModel.getProfileCategory(ntcrit+0.1, xtr+0.1));
  }

  {
    isTransitionActive = false;
    profileCatDefault = IBLProfileCategory::LaminarWake;
    TransitionModel trModel(profileCatDefault, ntinit, ntcrit, xtr, isTransitionActive);

    BOOST_CHECK(profileCatDefault == trModel.getProfileCategory(ntcrit-0.1, xtr-0.1));
    BOOST_CHECK(profileCatDefault == trModel.getProfileCategory(ntcrit+0.1, xtr-0.1));
    BOOST_CHECK(profileCatDefault == trModel.getProfileCategory(ntcrit-0.1, xtr+0.1));
    BOOST_CHECK(profileCatDefault == trModel.getProfileCategory(ntcrit+0.1, xtr+0.1));

    BOOST_CHECK_EQUAL(0.0, trModel.binaryIntermittency(ntcrit-0.1, xtr-0.1));
  }

  {
    isTransitionActive = false;
    profileCatDefault = IBLProfileCategory::TurbulentWake;
    TransitionModel trModel(profileCatDefault, ntinit, ntcrit, xtr, isTransitionActive);

    BOOST_CHECK(profileCatDefault == trModel.getProfileCategory(ntcrit-0.1, xtr-0.1));
    BOOST_CHECK(profileCatDefault == trModel.getProfileCategory(ntcrit+0.1, xtr-0.1));
    BOOST_CHECK(profileCatDefault == trModel.getProfileCategory(ntcrit-0.1, xtr+0.1));
    BOOST_CHECK(profileCatDefault == trModel.getProfileCategory(ntcrit+0.1, xtr+0.1));

    BOOST_CHECK_EQUAL(1.0, trModel.binaryIntermittency(ntcrit-0.1, xtr-0.1));
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( transition_param_change_test )
{
  const Real ntinit = 0.0, ntcrit = 9.0, xtr = 0.2;
  const bool isTransitionActive = true;
  const IBLProfileCategory profileCatDefault = IBLProfileCategory::LaminarBL;

  const TransitionModel trModel(profileCatDefault, ntinit, ntcrit, xtr, isTransitionActive);

  const Real ntcrit_new = 10.0, xtr_new = 0.8;

  trModel.set_ntcrit(ntcrit_new);
  BOOST_CHECK( trModel.ntcrit() == ntcrit_new );

  trModel.set_xtr(xtr_new);
  BOOST_CHECK( trModel.xtr() == xtr_new );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO_test )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/IBL/TransitionModel_pattern.txt", true );

  // transition model
  TransitionModel transition(IBLProfileCategory::LaminarBL, 0.0, 9.0, 1.1, false);

  transition.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

BOOST_AUTO_TEST_SUITE_END()
//############################################################################//
