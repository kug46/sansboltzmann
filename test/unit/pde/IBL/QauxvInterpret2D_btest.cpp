// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/IBL/QauxvInterpret2D.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( QauxvInterpret2D_test_suite )

typedef QauxvInterpret2D QauxvInterpType;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  BOOST_CHECK( QauxvInterpType::D == 2 );
  BOOST_CHECK( QauxvInterpType::N == 8 );

  BOOST_CHECK( QauxvInterpType::iqx == 0 );
  BOOST_CHECK( QauxvInterpType::iqz == 1 );
  BOOST_CHECK( QauxvInterpType::iqx_x == 2 );
  BOOST_CHECK( QauxvInterpType::iqx_z == 3 );
  BOOST_CHECK( QauxvInterpType::iqz_x == 4 );
  BOOST_CHECK( QauxvInterpType::iqz_z == 5 );
  BOOST_CHECK( QauxvInterpType::ip0 == 6 );
  BOOST_CHECK( QauxvInterpType::iT0 == 7 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( get_setDOFFrom_test )
{
  QauxvInterpType interpreter;

  typedef QauxvInterpType::ArrayQ<Real> ArrayQ;
  typedef QauxvInterpType::VectorX<Real> VectorX;
  typedef QauxvInterpType::TensorX<Real> TensorX;

  const Real qx = 1.3, qz = 3.0, qx_x = -0.3, qx_z = 0.4, qz_x = 2.3, qz_z = 0.6, p0 = 1.e5, T0 = 300.0;

  const ArrayQ qauxv = interpreter.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const VectorX q1 = interpreter.getq1(qauxv);
  BOOST_CHECK_EQUAL(qx, q1[0]);
  BOOST_CHECK_EQUAL(qz, q1[1]);

  BOOST_CHECK_EQUAL(sqrt(dot(q1, q1)), interpreter.getqe(qauxv));

  BOOST_CHECK_EQUAL(qx_x+qz_z, interpreter.getdivq1(qauxv));

  const TensorX gradq1 = interpreter.getgradq1(qauxv);
  BOOST_CHECK_EQUAL(qx_x, gradq1(0,0));
  BOOST_CHECK_EQUAL(qx_z, gradq1(0,1));
  BOOST_CHECK_EQUAL(qz_x, gradq1(1,0));
  BOOST_CHECK_EQUAL(qz_z, gradq1(1,1));

  BOOST_CHECK_EQUAL(p0, interpreter.getp0(qauxv));
  BOOST_CHECK_EQUAL(T0, interpreter.getT0(qauxv));

  // more tests on setDOFFrom
  {
    const VectorX q1 = interpreter.getq1(qauxv);
    const TensorX gradq1 = interpreter.getgradq1(qauxv);

    const ArrayQ qauxv_copy = interpreter.setDOFFrom(q1, gradq1, p0, T0);
    for (int i=0; i<QauxvInterpType::N; ++i)
      BOOST_CHECK_EQUAL(qauxv[i], qauxv_copy[i]);
  }

  {
    const typename QauxvInterpType::VectorX<VectorX> gradq1 = {{qx_x, qz_x}, {qx_z, qz_z}};
    const ArrayQ qauxv_copy = interpreter.setDOFFrom(q1, gradq1, p0, T0);
    for (int i=0; i<QauxvInterpType::N; ++i)
      BOOST_CHECK_EQUAL(qauxv[i], qauxv_copy[i]);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState_test )
{
  QauxvInterpType interpreter;

  // valid state
  {
    const Real qx = 1.3, qz = 3.0, qx_x = -0.3, qx_z = 0.4, qz_x = 2.3, qz_z = 0.6, p0 = 1.e5, T0 = 300.0;
    BOOST_CHECK( interpreter.isValidState(interpreter.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0)) );
  }

  // non-positive p0 alone
  {
    const Real qx = 1.3, qz = 3.0, qx_x = -0.3, qx_z = 0.4, qz_x = 2.3, qz_z = 0.6, p0 = 0.0, T0 = 300.0;
    BOOST_CHECK( !interpreter.isValidState(interpreter.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0)) );
  }

  {
    const Real qx = 1.3, qz = 3.0, qx_x = -0.3, qx_z = 0.4, qz_x = 2.3, qz_z = 0.6, p0 = -1.e5, T0 = 300.0;
    BOOST_CHECK( !interpreter.isValidState(interpreter.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0)) );
  }

  // non-positive T0 alone
  {
    const Real qx = 1.3, qz = 3.0, qx_x = -0.3, qx_z = 0.4, qz_x = 2.3, qz_z = 0.6, p0 = 1.e5, T0 = 0.0;
    BOOST_CHECK( !interpreter.isValidState(interpreter.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0)) );
  }

  {
    const Real qx = 1.3, qz = 3.0, qx_x = -0.3, qx_z = 0.4, qz_x = 2.3, qz_z = 0.6, p0 = 1.e5, T0 = -1.0;
    BOOST_CHECK( !interpreter.isValidState(interpreter.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0)) );
  }

  // non-positive p0 and T0
  {
    const Real qx = 1.3, qz = 3.0, qx_x = -0.3, qx_z = 0.4, qz_x = 2.3, qz_z = 0.6, p0 = -1.e5, T0 = -1.0;
    BOOST_CHECK( !interpreter.isValidState(interpreter.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0)) );
  }
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
