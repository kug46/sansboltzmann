// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// SetSolnDofCell_IBL_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/tools/for_each_CellGroup.h"

#include "pde/IBL/SetSolnDofCell_IBL2D.h"

#include "unit/UnitGrids/XField2D_Line_X1_1Group.h"

using namespace std;

namespace SANS
{
typedef PhysD2 PhysDim;
typedef VarTypeDANCt VarType;
typedef VarInterpret2D<VarType> VarInterpType;
typedef SetSolnDofCell_IBL2D_impl<VarType> FunctorType;
typedef typename FunctorType::ArrayQ ArrayQ;

typedef DLA::VectorS<PhysDim::D,Real> VectorX;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct
}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( SetSolnDofCell_IBL_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_ctor_test )
{
  // check
  BOOST_CHECK( FunctorType::D == PhysDim::D );
  BOOST_CHECK( FunctorType::N == IBLTraits<PhysDim>::N );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SetSolnDofCell_IBL2D_ctor1_X1Q0_test )
{
  const Real tol = 1.e-13;

  const int N = FunctorType::N; // state/solution vector dimension


  // Read from file
  const VarInterpType varInterpret;
  const std::string Qfilename = "IO/pde/IBL/Qinit_nelem32.txt";

  std::vector<ArrayQ> qdata = QIBLDataReader<VarData2DDANCt>::getqIBLvec(varInterpret, Qfilename);

  BOOST_CHECK_EQUAL( 33, static_cast<int>(qdata.size()) );

  // Read in grid from file
  const std::string Gridfilename = "IO/pde/IBL/naca0004_nelem32.txt";
  std::vector<VectorX> coordinates;
  XField2D_Line_X1_1Group::readXFoilGrid(Gridfilename, coordinates);
  XField2D_Line_X1_1Group xfld(coordinates);

  // solution: P1 (aka Q1), which spans the space of the Linear solution
  // the check below assumes Hierarchical basis
  const int order = 0;
  Field_DG_Cell<PhysDim, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

  for_each_CellGroup<TopoD1>::apply( SetSolnDofCell_IBL2D<VarType>(qdata, {0}, order), (xfld, qfld) );

  // Check
  const XField2D_Line_X1_1Group       ::template FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);
  const Field<PhysDim, TopoD1, ArrayQ>::template FieldCellGroupType<Line>& qfldCell = qfld.getCellGroup<Line>(0);

  typedef XField2D_Line_X1_1Group       ::template FieldCellGroupType<Line>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysDim, TopoD1, ArrayQ>::template FieldCellGroupType<Line>::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass qfldElem( qfldCell.basis() );

  BOOST_REQUIRE_EQUAL( xfldCell.nElem(), qfldCell.nElem() );

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for ( int elem = 0; elem < nelem; elem++ )
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    qfldCell.getElement( qfldElem, elem );

    // Check

    Real sRef = 0.3;
    ArrayQ qActual = qfldElem.eval(sRef);

    int indx = elem;

    VarData2DDANCt vardata = { 0.5*(qdata[indx][0] + qdata[indx+1][0]),
                               0.5*(qdata[indx][1] + qdata[indx+1][1]),
                               0.5*(qdata[indx][2] + qdata[indx+1][2]),
                               0.5*(qdata[indx][3] + qdata[indx+1][3]),
                               0.5*(qdata[indx][4] + qdata[indx+1][4]),
                               0.5*(qdata[indx][5] + qdata[indx+1][5]) };

    ArrayQ qTrue = varInterpret.setDOFFrom<Real>( vardata );

    for ( int iq = 0; iq < N; iq++ )
    {
      SANS_CHECK_CLOSE( qTrue[iq], qActual[iq], tol, tol );
    }

#if 0
      std::cout << "Element " << elem << " node " << n << ": "
                << qTrue[n][0] << " " << qTrue[n][1] << std::endl;
#endif

    // Check mid node
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SetSolnDofCell_IBL2D_ctor1_X1Q1_test )
{
  const Real tol = 1.e-13;

  const int N = FunctorType::N; // state/solution vector dimension

  // Read from file
  const VarInterpType varInterpret;
  const std::string Qfilename = "IO/pde/IBL/Qinit_nelem32.txt";

  std::vector<ArrayQ> qdata = QIBLDataReader<VarData2DDANCt>::getqIBLvec(varInterpret, Qfilename);

  BOOST_CHECK_EQUAL( 33, static_cast<int>(qdata.size()) );

  // Read in grid from file
  const std::string Gridfilename = "IO//pde/IBL/naca0004_nelem32.txt";
  std::vector<VectorX> coordinates;
  XField2D_Line_X1_1Group::readXFoilGrid(Gridfilename, coordinates);
  XField2D_Line_X1_1Group xfld(coordinates);

  // solution: P1 (aka Q1), which spans the space of the Linear solution
  // the check below assumes Hierarchical basis
  const int order = 1;
  Field_DG_Cell<PhysDim, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  for_each_CellGroup<TopoD1>::apply( SetSolnDofCell_IBL2D<VarType>(qdata, {0}, order), (xfld, qfld) );

  // Check
  const XField2D_Line_X1_1Group       ::template FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);
  const Field<PhysDim, TopoD1, ArrayQ>::template FieldCellGroupType<Line>& qfldCell = qfld.getCellGroup<Line>(0);

  typedef XField2D_Line_X1_1Group       ::template FieldCellGroupType<Line>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysDim, TopoD1, ArrayQ>::template FieldCellGroupType<Line>::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass qfldElem( qfldCell.basis() );

  BOOST_REQUIRE_EQUAL( xfldCell.nElem(), qfldCell.nElem() );

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for ( int elem = 0; elem < nelem; elem++ )
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    qfldCell.getElement( qfldElem, elem );

    BOOST_REQUIRE_EQUAL(xfldElem.nDOF(), order+1);
    BOOST_REQUIRE_EQUAL(xfldElem.nDOF(), qfldElem.nDOF());

    ArrayQ qTrue[2];  // at sRef = {0,1}; true solution
    ArrayQ q_mid;     // at sRef 0.5

    // Check trace nodes
    Real sRef[] = { 0.0, 1.0 };

    for ( int n = 0; n < qfldElem.nDOF(); n++ )
    {
      ArrayQ qActual = qfldElem.eval(sRef[n]);

      int indx = elem * order + n;

      qTrue[n] = varInterpret.setDOFFrom<Real>( VarData2DDANCt(qdata[indx][0],qdata[indx][1],
                                                               qdata[indx][2],qdata[indx][3],
                                                               qdata[indx][4],qdata[indx][5]) );

      for ( int iq = 0; iq < N; iq++ )
      {
        SANS_CHECK_CLOSE( qTrue[n][iq], qActual[iq], tol, tol );
      }

#if 0
      std::cout << "Element " << elem << " node " << n << ": "
                << qTrue[n][0] << " " << qTrue[n][1] << std::endl;
#endif
    }

    // Check mid node
    q_mid = qfldElem.eval( 0.5 );

    for ( int iq = 0; iq < N; iq++ )
    {
      SANS_CHECK_CLOSE( 0.5*( qTrue[0][iq] + qTrue[1][iq] ),  // averaging is valid for linear solution
                        q_mid[iq], tol, tol );
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SetSolnDofCell_IBL2D_ctor1_X1Q2orHigher_test )
{
  std::vector<ArrayQ> qdata;
  for (int order = 2; order <= 5; ++order)
    BOOST_CHECK_THROW( FunctorType functor(qdata, {0}, order);, AssertionException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( readSoln_test )
{
  const Real tol = 1.e-13;

  const VarInterpType varInterpret;

  // read a non-existent file
  BOOST_CHECK_THROW( QIBLDataReader<VarData2DDANCt>::getqIBLvec(varInterpret, "FileNotExistent");, SANSException );

  // Read from file
  const std::string filename = "IO/pde/IBL/Qinit_nelem32.txt";
  std::vector<ArrayQ> qdata = QIBLDataReader<VarData2DDANCt>::getqIBLvec(varInterpret, filename);


  BOOST_CHECK_EQUAL( 33, static_cast<int>(qdata.size()) );

  // 1st node
  SANS_CHECK_CLOSE(  0.012302, qdata[0][0], tol, tol );
  SANS_CHECK_CLOSE(  1,        qdata[0][1], tol, tol );
  SANS_CHECK_CLOSE(  0.012302, qdata[0][2], tol, tol );
  SANS_CHECK_CLOSE(  1,        qdata[0][3], tol, tol );

  // 26th node
  SANS_CHECK_CLOSE( 0.007988, qdata[25][0], tol, tol );
  SANS_CHECK_CLOSE( 26,       qdata[25][1], tol, tol );
  SANS_CHECK_CLOSE( 0.007988, qdata[25][2], tol, tol );
  SANS_CHECK_CLOSE( 26,       qdata[25][3], tol, tol );

  // 33rd node
  SANS_CHECK_CLOSE( 0.012302, qdata[32][0], tol, tol );
  SANS_CHECK_CLOSE( 33,       qdata[32][1], tol, tol );
  SANS_CHECK_CLOSE( 0.012302, qdata[32][2], tol, tol );
  SANS_CHECK_CLOSE( 33,       qdata[32][3], tol, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
