// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// testing 2D incompressible secondary variable class

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "SANS_btest.h"

#include "pde/IBL/SecondaryVar2D.h"

using namespace std;

namespace SANS
{

typedef IncompressibleBL CompressibilityType;

typedef SecondaryVar2D<CompressibilityType> SecondaryVarType;
typedef SecondaryVarType::Functor<Real,Real> SecondaryVarFunctor;
typedef typename SecondaryVarType::ProfileCategory ProfileCategory;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
template class SecondaryVarType::Functor<Real,Real>;
}
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE (SecondaryVar2D_test_suite)

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SecondaryVar2DParam_Dict_test )
{
  BOOST_CHECK(39 == SecondaryVar2DParams::params.quadratureOrder.Default);
  BOOST_CHECK(39 == SecondaryVar2DParams::params.quadratureOrder.min);

  BOOST_CHECK( SecondaryVar2DParams::params.nameCDclosure.ibl3eqm
            == SecondaryVar2DParams::params.nameCDclosure.Default);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_test )
{
  {
    SecondaryVarType secondaryVarObj;

    BOOST_CHECK_EQUAL(SecondaryVar2DParams::params.quadratureOrder.Default,
                      secondaryVarObj.quadratureOrder());
    BOOST_CHECK_EQUAL(SecondaryVar2DParams::params.nameCDclosure.Default,
                      secondaryVarObj.nameCDclosure());
  }

  {
    const int quadratureOrder = 39;
    const int nquad = 20;
    const std::string nameCDclosure = SecondaryVar2DParams::params.nameCDclosure.ibl3lag;

    PyDict secVarDict;
    secVarDict[SecondaryVar2DParams::params.quadratureOrder] = quadratureOrder;
    secVarDict[SecondaryVar2DParams::params.nameCDclosure] = nameCDclosure;
    SecondaryVar2DParams::checkInputs(secVarDict);

    SecondaryVarType secondaryVarObj(secVarDict);

    BOOST_CHECK_EQUAL(quadratureOrder, secondaryVarObj.quadratureOrder());
    BOOST_CHECK_EQUAL(nquad, secondaryVarObj.nQuadrature());
    BOOST_CHECK_EQUAL(nameCDclosure, secondaryVarObj.nameCDclosure());
  }

  {
    PyDict secVarDict;
    secVarDict[SecondaryVar2DParams::params.quadratureOrder] = 10;

    BOOST_CHECK_THROW(SecondaryVarType secondaryVarObj(secVarDict);, SANSException);
  }

  {
    PyDict secVarDict;
    secVarDict[SecondaryVar2DParams::params.nameCDclosure] = "SomeInvalidName";

    BOOST_CHECK_THROW(SecondaryVarType secondaryVarObj(secVarDict);, SANSException);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SecondaryVar_LaminarBL_ibl3eqm_test )
{
  const Real small_tol = 9.E-14;
  const Real close_tol = 2.5E-13;

  const Real delta = 1.0, A = 3.0;
  const Real ctau = 0.01, Redelta = -1.0;

  PyDict secVarDict;
  secVarDict[SecondaryVar2DParams::params.quadratureOrder] = 39;
  secVarDict[SecondaryVar2DParams::params.nameCDclosure]
             = SecondaryVar2DParams::params.nameCDclosure.ibl3eqm;

  SecondaryVarType secondaryVarObj(secVarDict);

  { // invalid profile type in Funtor initialization
    const ProfileCategory profileCatInvalid = static_cast<ProfileCategory>(4);
    BOOST_CHECK_THROW(SecondaryVarFunctor fcn = secondaryVarObj.secondaryVarCalc(profileCatInvalid, delta, A, ctau, Redelta);,
                      SANSException);
  }

  SecondaryVarFunctor fcn
    = secondaryVarObj.secondaryVarCalc(ProfileCategory::LaminarBL, delta, A, ctau, Redelta);

  const Real
  delta1s = 2.6215449513138855e-01,
  theta11 = 1.1110378361109696e-01,
  theta1s = 1.7716681897092956e-01,
  delta1p = 2.6215449513138855e-01,
  deltarho = 0.0000000000000000e+00,
  deltaq = 3.7325827874248552e-01,
  RedCf1 = 5.6577743566100276e+00,
  RedCD = 1.7101012560435254e+00;

  SANS_CHECK_CLOSE(delta1s, fcn.getdelta1s(), small_tol, close_tol);
  SANS_CHECK_CLOSE(theta11, fcn.gettheta11(), small_tol, close_tol);
  SANS_CHECK_CLOSE(theta1s, fcn.gettheta1s(), small_tol, close_tol);
  SANS_CHECK_CLOSE(delta1p, fcn.getdelta1p(), small_tol, close_tol);
  SANS_CHECK_CLOSE(deltarho, fcn.getdeltarho(), small_tol, close_tol);
  SANS_CHECK_CLOSE(deltaq, fcn.getdeltaq(), small_tol, close_tol);
  SANS_CHECK_CLOSE(RedCf1, fcn.getRedCf1(), small_tol, close_tol);
  SANS_CHECK_CLOSE(RedCD, fcn.getRedCD(), small_tol, close_tol);

  const Real H  = delta1s/theta11;
  const Real Hs = theta1s/theta11;
  const Real GH = (H-1.0)/H;

  const Real Us = 0.5*(-1.0 + 3.0/H);

  const Real A_GbetaLocus = secondaryVarObj.getA_GbetaLocus();
  const Real B_GbetaLocus = secondaryVarObj.getB_GbetaLocus();

  const Real ctaueq = Hs/(2.0-Us) * GH*GH*GH / (B_GbetaLocus*A_GbetaLocus*A_GbetaLocus);

  SANS_CHECK_CLOSE(ctaueq, fcn.getctaueq(), small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SecondaryVar_LaminarWake_ibl3eqm_test )
{
  const Real small_tol = 1.E-14;
  const Real close_tol = 2.E-13;

  const Real delta = 1.0, A = 3.0;
  const Real ctau = 0.01, Redelta = -1.0; // dummy

  PyDict secVarDict;
  secVarDict[SecondaryVar2DParams::params.quadratureOrder] = 39;
  secVarDict[SecondaryVar2DParams::params.nameCDclosure]
             = SecondaryVar2DParams::params.nameCDclosure.ibl3eqm;

  SecondaryVarType secondaryVarObj(secVarDict);

  SecondaryVarFunctor fcn
    = secondaryVarObj.secondaryVarCalc(ProfileCategory::LaminarWake, delta, A, ctau, Redelta);

  const Real
  delta1s = 2.4999999999999997e-01,
  theta11 = 1.3839285714285715e-01,
  theta1s = 2.2186017107892111e-01,
  delta1p = 2.4999999999999997e-01,
  deltarho = 0.0000000000000000e+00,
  deltaq = 3.8839285714285710e-01,
  RedCf1 = 0.0,
  RedCD = 2.1428571428571446e+00;

  SANS_CHECK_CLOSE(delta1s, fcn.getdelta1s(), small_tol, close_tol);
  SANS_CHECK_CLOSE(theta11, fcn.gettheta11(), small_tol, close_tol);
  SANS_CHECK_CLOSE(theta1s, fcn.gettheta1s(), small_tol, close_tol);
  SANS_CHECK_CLOSE(delta1p, fcn.getdelta1p(), small_tol, close_tol);
  SANS_CHECK_CLOSE(deltarho, fcn.getdeltarho(), small_tol, close_tol);
  SANS_CHECK_CLOSE(deltaq, fcn.getdeltaq(), small_tol, close_tol);
  SANS_CHECK_CLOSE(RedCf1, fcn.getRedCf1(), small_tol, close_tol);
  SANS_CHECK_CLOSE(RedCD, fcn.getRedCD(), small_tol, close_tol);

  const Real H  = delta1s/theta11;
  const Real Hs = theta1s/theta11;
  const Real GH = (H-1.0)/H;

  const Real Us = 0.5*(-1.0 + 3.0/H);

  const Real A_GbetaLocus = secondaryVarObj.getA_GbetaLocus();
  const Real B_GbetaLocus = secondaryVarObj.getB_GbetaLocus();

  const Real ctaueq = Hs/(2.0-Us) * GH*GH*GH / (B_GbetaLocus*A_GbetaLocus*A_GbetaLocus);

  SANS_CHECK_CLOSE(ctaueq, fcn.getctaueq(), small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SecondaryVar_TurbulentBL_ibl3eqm_test )
{
  const Real small_tol = 1.E-16;
  const Real close_tol = 2.E-13;

  const Real delta = 0.2, A = 3.0, ctau = 1e-3, Redelta = 500.0;

  PyDict secVarDict;
  secVarDict[SecondaryVar2DParams::params.quadratureOrder] = 39;
  secVarDict[SecondaryVar2DParams::params.nameCDclosure]
             = SecondaryVar2DParams::params.nameCDclosure.ibl3eqm;

  SecondaryVarType secondaryVarObj(secVarDict);

  SecondaryVarFunctor fcn
    = secondaryVarObj.secondaryVarCalc(ProfileCategory::TurbulentBL, delta, A, ctau, Redelta);

#if ISIBL3SRCTURB
  const Real
  delta1s = 5.7816241506692803e-02,
  theta11 = 2.6233306658144610e-02,
  theta1s = 4.2270896239374398e-02,
  delta1p = 5.7816241506692803e-02,
  deltarho = 0.0000000000000000e+00,
  deltaq = 8.4049548164837423e-02,
  RedCf1 = 6.0000000000000000e+00,
  RedCD = 2.6069854917913080e+00;
#else
  const Real
  delta1s = 4.8565586937152821e-02,
  theta11 = 2.2377390914514794e-02,
  theta1s = 3.6669470849196417e-02,
  delta1p = 4.8565586937152821e-02,
  deltarho = 0.0000000000000000e+00,
  deltaq = 7.0942977851667616e-02,
  RedCf1 = 6.0000000000000000e+00,
  RedCD = 2.5986570651160221e+00;
#endif

  SANS_CHECK_CLOSE(delta1s, fcn.getdelta1s(), small_tol, close_tol);
  SANS_CHECK_CLOSE(theta11, fcn.gettheta11(), small_tol, close_tol);
  SANS_CHECK_CLOSE(theta1s, fcn.gettheta1s(), small_tol, close_tol);
  SANS_CHECK_CLOSE(delta1p, fcn.getdelta1p(), small_tol, close_tol);
  SANS_CHECK_CLOSE(deltarho, fcn.getdeltarho(), small_tol, close_tol);
  SANS_CHECK_CLOSE(deltaq, fcn.getdeltaq(), small_tol, close_tol);
  SANS_CHECK_CLOSE(RedCf1, fcn.getRedCf1(), small_tol, close_tol);
  SANS_CHECK_CLOSE(RedCD, fcn.getRedCD(), small_tol, close_tol);

  const Real H  = delta1s/theta11;
  const Real Hs = theta1s/theta11;
  const Real GH = (H-1.0)/H;

  const Real Us = 0.5*(-1.0 + 3.0/H);

  const Real A_GbetaLocus = secondaryVarObj.getA_GbetaLocus();
  const Real B_GbetaLocus = secondaryVarObj.getB_GbetaLocus();

  const Real ctaueq = Hs/(2.0-Us) * GH*GH*GH / (B_GbetaLocus*A_GbetaLocus*A_GbetaLocus);

  SANS_CHECK_CLOSE(ctaueq, fcn.getctaueq(), small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SecondaryVar_TurbulentBL_ibl3lag_CD_ctaueq_test )
{
  const Real small_tol = 1.E-16;
  const Real close_tol = 1.E-16;

  const Real delta = 0.2, A = 3.0, ctau = 1e-3, Redelta = 500.0;

  PyDict secVarDict;
  secVarDict[SecondaryVar2DParams::params.quadratureOrder] = 39;
  secVarDict[SecondaryVar2DParams::params.nameCDclosure]
             = SecondaryVar2DParams::params.nameCDclosure.ibl3lag;

  SecondaryVarType secondaryVarObj(secVarDict);

  SecondaryVarFunctor fcn
    = secondaryVarObj.secondaryVarCalc(ProfileCategory::TurbulentBL, delta, A, ctau, Redelta);

  const Real
  delta1s = fcn.getdelta1s(),
  theta11 = fcn.gettheta11(),
  theta1s = fcn.gettheta1s(),
  RedCf1 = fcn.getRedCf1();

  const Real H  = delta1s/theta11;
  const Real Hs = theta1s/theta11;
  const Real GH = (H-1.0)/H;

  const Real Us = 0.5*(-1.0 + 3.0/H);

  const Real A_GbetaLocus = secondaryVarObj.getA_GbetaLocus();
  const Real B_GbetaLocus = secondaryVarObj.getB_GbetaLocus();

  const Real RedCD = 0.5*(0.5*RedCf1 + Redelta*ctau) *Us + Redelta*ctau*(1.0 - Us);
  const Real ctaueq = Hs/(2.0-Us) * GH*GH*GH / (B_GbetaLocus*A_GbetaLocus*A_GbetaLocus);

  SANS_CHECK_CLOSE(RedCD, fcn.getRedCD(), small_tol, close_tol);
  SANS_CHECK_CLOSE(ctaueq, fcn.getctaueq(), small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SecondaryVar_TurbulentBL_xfoillag_CD_ctaueq_test )
{
  const Real small_tol = 1.E-16;
  const Real close_tol = 1.E-16;

  const Real delta = 0.2, A = 3.0, ctau = 1e-3, Redelta = 500.0;

  PyDict secVarDict;
  secVarDict[SecondaryVar2DParams::params.quadratureOrder] = 39;
  secVarDict[SecondaryVar2DParams::params.nameCDclosure]
             = SecondaryVar2DParams::params.nameCDclosure.xfoillag;

  SecondaryVarType secondaryVarObj(secVarDict);

  SecondaryVarFunctor fcn
    = secondaryVarObj.secondaryVarCalc(ProfileCategory::TurbulentBL, delta, A, ctau, Redelta);

  const Real
  delta1s = fcn.getdelta1s(),
  theta11 = fcn.gettheta11(),
  theta1s = fcn.gettheta1s(),
  RedCf1 = fcn.getRedCf1();

  const Real H  = delta1s/theta11;
  const Real Hs = theta1s/theta11;
  const Real GH = (H-1.0)/H;

  const Real A_GbetaLocus = secondaryVarObj.getA_GbetaLocus();
  const Real B_GbetaLocus = secondaryVarObj.getB_GbetaLocus();

  const Real Us = 0.5*Hs*(1.0 - GH/B_GbetaLocus);

  const Real RedCD = 0.5*RedCf1*Us + Redelta*ctau*(0.995 - Us)
                   + 0.15*pow(0.995-Us,2.0)*delta/theta11;
  const Real ctaueq = Hs * 0.5/ (B_GbetaLocus*A_GbetaLocus*A_GbetaLocus) / (1.0-Us) * GH*GH*GH;

  SANS_CHECK_CLOSE(RedCD, fcn.getRedCD(), small_tol, close_tol);
  SANS_CHECK_CLOSE(ctaueq, fcn.getctaueq(), small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SecondaryVar_TurbulentWake_ibl3eqm_test )
{
  const Real small_tol = 2.E-14;
  const Real close_tol = 2.E-11;

  const Real delta = 0.2, A = 3.0, ctau = 1e-3, Redelta = 500.0;

  PyDict secVarDict;
  secVarDict[SecondaryVar2DParams::params.quadratureOrder] = 39;
  secVarDict[SecondaryVar2DParams::params.nameCDclosure]
             = SecondaryVar2DParams::params.nameCDclosure.ibl3eqm;

  SecondaryVarType secondaryVarObj(secVarDict);

  SecondaryVarFunctor fcn
    = secondaryVarObj.secondaryVarCalc(ProfileCategory::TurbulentWake, delta, A, ctau, Redelta);

#if ISIBL3SRCTURB
  const Real
  delta1s = 6.2500000000000000e-02,
  theta11 = 3.3482142857142863e-02,
  theta1s = 5.2943638392857151e-02,
  delta1p = 6.2500000000000000e-02,
  deltarho = 0.0000000000000000e+00,
  deltaq = 9.5982142857142849e-02,
  RedCf1 = 0.0000000000000000e+00,
  RedCD = 2.3502585266197613e+00;
#else
  const Real
  delta1s = 5.6249999999999988e-02,
  theta11 = 3.0859375000000001e-02,
  theta1s = 4.9274468207573682e-02,
  delta1p = 5.6249999999999988e-02,
  deltarho = 0.0000000000000000e+00,
  deltaq = 8.7109375000000003e-02,
  RedCf1 = 0.0000000000000000e+00,
  RedCD = 2.1809542179909385e+00;
#endif

  SANS_CHECK_CLOSE(delta1s, fcn.getdelta1s(), small_tol, close_tol);
  SANS_CHECK_CLOSE(theta11, fcn.gettheta11(), small_tol, close_tol);
  SANS_CHECK_CLOSE(theta1s, fcn.gettheta1s(), small_tol, close_tol);
  SANS_CHECK_CLOSE(delta1p, fcn.getdelta1p(), small_tol, close_tol);
  SANS_CHECK_CLOSE(deltarho, fcn.getdeltarho(), small_tol, close_tol);
  SANS_CHECK_CLOSE(deltaq, fcn.getdeltaq(), small_tol, close_tol);
  SANS_CHECK_CLOSE(RedCf1, fcn.getRedCf1(), small_tol, close_tol);
  SANS_CHECK_CLOSE(RedCD, fcn.getRedCD(), small_tol, close_tol);

  const Real H  = delta1s/theta11;
  const Real Hs = theta1s/theta11;
  const Real GH = (H-1.0)/H;

  const Real Us = 0.5*(-1.0 + 3.0/H);

  const Real A_GbetaLocus = secondaryVarObj.getA_GbetaLocus();
  const Real B_GbetaLocus = secondaryVarObj.getB_GbetaLocus();

  const Real ctaueq = Hs/(2.0-Us) * GH*GH*GH / (B_GbetaLocus*A_GbetaLocus*A_GbetaLocus);

  SANS_CHECK_CLOSE(ctaueq, fcn.getctaueq(), small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SecondaryVar_TurbulentWake_ibl3lag_CD_ctaueq_test )
{
  const Real small_tol = 1.E-16;
  const Real close_tol = 1.E-16;

  const Real delta = 0.2, A = 3.0, ctau = 1e-3, Redelta = 500.0;

  PyDict secVarDict;
  secVarDict[SecondaryVar2DParams::params.quadratureOrder] = 39;
  secVarDict[SecondaryVar2DParams::params.nameCDclosure]
             = SecondaryVar2DParams::params.nameCDclosure.ibl3lag;

  SecondaryVarType secondaryVarObj(secVarDict);

  SecondaryVarFunctor fcn
    = secondaryVarObj.secondaryVarCalc(ProfileCategory::TurbulentWake, delta, A, ctau, Redelta);

  const Real
  delta1s = fcn.getdelta1s(),
  theta11 = fcn.gettheta11(),
  theta1s = fcn.gettheta1s();

  const Real H  = delta1s/theta11;
  const Real Hs = theta1s/theta11;
  const Real GH = (H-1.0)/H;

  const Real Us = 0.5*(-1.0 + 3.0/H);

  const Real A_GbetaLocus = secondaryVarObj.getA_GbetaLocus();
  const Real B_GbetaLocus = secondaryVarObj.getB_GbetaLocus();

  const Real RedCD = ( Redelta*ctau*(1.0 - Us) ) * 2.0;
  const Real ctaueq = Hs/(2.0-Us) * GH*GH*GH / (B_GbetaLocus*A_GbetaLocus*A_GbetaLocus);

  SANS_CHECK_CLOSE(RedCD, fcn.getRedCD(), small_tol, close_tol);
  SANS_CHECK_CLOSE(ctaueq, fcn.getctaueq(), small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SecondaryVar_TurbulentWake_xfoillag_CD_ctaueq_test )
{
  const Real small_tol = 1.E-16;
  const Real close_tol = 2.E-14;

  const Real delta = 0.2, A = 3.0, ctau = 1e-3, Redelta = 500.0;

  PyDict secVarDict;
  secVarDict[SecondaryVar2DParams::params.quadratureOrder] = 39;
  secVarDict[SecondaryVar2DParams::params.nameCDclosure]
             = SecondaryVar2DParams::params.nameCDclosure.xfoillag;

  SecondaryVarType secondaryVarObj(secVarDict);

  SecondaryVarFunctor fcn
    = secondaryVarObj.secondaryVarCalc(ProfileCategory::TurbulentWake, delta, A, ctau, Redelta);

  const Real
  delta1s = fcn.getdelta1s(),
  theta11 = fcn.gettheta11(),
  theta1s = fcn.gettheta1s();

  const Real H  = delta1s/theta11;
  const Real Hs = theta1s/theta11;
  const Real GH = (H-1.0)/H;

  const Real A_GbetaLocus = secondaryVarObj.getA_GbetaLocus();
  const Real B_GbetaLocus = secondaryVarObj.getB_GbetaLocus();

  const Real Us = 0.5*Hs*(1.0 - GH/B_GbetaLocus);

  const Real RedCD = ( Redelta*ctau*(0.995 - Us)
                     + 0.15*pow(0.995-Us,2.0)*delta/theta11 ) * 2.0;
  const Real ctaueq = Hs * 0.5/ (B_GbetaLocus*A_GbetaLocus*A_GbetaLocus) / (1.0-Us) * GH*GH*GH;

  SANS_CHECK_CLOSE(RedCD, fcn.getRedCD(), small_tol, close_tol);
  SANS_CHECK_CLOSE(ctaueq, fcn.getctaueq(), small_tol, close_tol);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
