// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//#define DISPLAY_FOR_DEBUGGING

//----------------------------------------------------------------------------//
// test of SetIBLoutputCellGroup

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>
#include <iomanip>

#include "Field/tools/for_each_CellGroup.h"

#include "unit/UnitGrids/XField2D_1Line_X1_1Group.h"

#include "pde/IBL/SetIBLoutputCellGroup.h"

using namespace std;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef VarTypeDANCt VarType;

typedef SetIBLoutputCellGroup_impl<VarType> SetIBLoutputCellGroupType;

typedef typename SetIBLoutputCellGroupType::ElementProjectionType ElementProjectionType;

typedef typename ElementProjectionType::PhysDim PhysDim;
typedef typename ElementProjectionType::TopoDim TopoDim;

typedef typename ElementProjectionType::PDEClass PDEClass;

typedef typename SetIBLoutputCellGroupType::template QFieldType<Real> QFieldType;
typedef typename SetIBLoutputCellGroupType::template ParamFieldType<Real> ParamFieldType;
typedef typename SetIBLoutputCellGroupType::template OutputFieldType<Real> OutputFieldType;

typedef typename ElementProjectionType::ArrayQ<Real> ArrayQ;
typedef typename ElementProjectionType::ArrayParam<Real> ArrayParam;
typedef typename ElementProjectionType::ArrayOutput<Real> ArrayOutput;
typedef typename ElementProjectionType::VectorX<Real> VectorX;

typedef typename ElementProjectionType::outputID outputID;
}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( SetIBLoutputCellGroup_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SetIBLoutputCellGroup_OutputP1_Q1_test )
{
  const Real tol = 2e-7;

  // ----------------------- SETUP ----------------------- //

  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const typename PDEClass::GasModelType gasModel(gasModelDict);

  const typename PDEClass::ViscosityModelType viscosityModel(1.8725e-5);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.ntinit] = 0.0;
  transitionModelDict[TransitionModelParams::params.ntcrit] = 9.0;
  transitionModelDict[TransitionModelParams::params.xtr] = 10.0;
  transitionModelDict[TransitionModelParams::params.isTransitionActive] = true;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const typename PDEClass::TransitionModelType transitionModel(transitionModelDict);

  // pde
  PDEClass pde(gasModel, viscosityModel, transitionModel);

  // fields
  const int order = 1;
  const int nBasis = order + 1;

  // constanst/parameters
  const Real qinf = 1.2;

  // grid: single line, P1 (aka X1)
  Real x_L = 0, x_R = 0.1;
  Real z_L = 0, z_R = 0.5;

  VectorX X1 = { x_L, z_L };
  VectorX X2 = { x_R, z_R };

  XField2D_1Line_X1_1Group xfld( X1, X2 );

  BOOST_CHECK_EQUAL( nBasis, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  typedef typename XField2D_1Line_X1_1Group::template FieldCellGroupType<Line> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<>  ElementXFieldCell;

  const XFieldCellGroupType& xfldCell = xfld.getCellGroup<Line>(0);
  ElementXFieldCell xfldElem( xfldCell.basis() );
  xfldCell.getElement( xfldElem, 0);

  // solution field
  QFieldType qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( nBasis, qfld.nDOF() );
  BOOST_CHECK_EQUAL( 1,      qfld.nElem() );

  qfld.DOF(0) = pde.setDOFFrom(VarData2DDANCt(0.020,0.8,0.010,0.5,2.3,1.2));
  qfld.DOF(1) = pde.setDOFFrom(VarData2DDANCt(0.025,0.4,0.030,0.6,2.3,1.2));

  // parameter field
  ParamFieldType paramfld(xfld, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( nBasis, paramfld.nDOF() );
  BOOST_CHECK_EQUAL( 1,      paramfld.nElem() );

  const Real p0 = 1.e5, T0 = 300.0;
  const PDEClass::ParamInterpType& paramInterpret = pde.getParamInterpreter();

  paramfld.DOF(0) = paramInterpret.setDOFFrom(1.2, 0.2, 0., 0., 0., 0., p0, T0);
  paramfld.DOF(1) = paramInterpret.setDOFFrom(1.4, 0.5, 0., 0., 0., 0., p0, T0);

  // output field
  OutputFieldType outputfld(xfld, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( nBasis, outputfld.nDOF() );
  BOOST_CHECK_EQUAL( 1,      outputfld.nElem() );

  // ----------------------- CHECK ----------------------- //

  // call SetIBLoutputCellGroup
  std::vector<int> CellGroups = {0};

  const int quadratureOrder = -1;

  for_each_CellGroup<TopoD1>::apply( SetIBLoutputCellGroup<VarType>(pde, qinf, qfld, paramfld, CellGroups, quadratureOrder), (xfld, outputfld) );

  // compute reference values
  const auto& varInterpret = pde.getVarInterpreter();
  const auto& secondaryVarObj = pde.getSecondaryVarObj();

  vector<ArrayOutput> OutputTrue(nBasis,0);
  for (int i = 0; i < nBasis; ++i)
  {
    const Real x = xfld.DOF(i)[0]; //TODO: a bit hacky here
    const Real z = xfld.DOF(i)[1]; //TODO: a bit hacky here

    VectorX e1;                // basis direction vector
    xfldElem.unitTangent(0.5, e1);

    const ArrayQ var = qfld.DOF(i);
    const ArrayParam params = paramfld.DOF(i);

    const auto profileCat = pde.getProfileCategory(var,x);

    const Real nt = varInterpret.getnt(var);
    const Real ctau = varInterpret.getctau(var);


    const Real gam = pde.calcIntermittency(params, e1, x, z, var); // TODO: missing access to e1

    const VectorX q1 = paramInterpret.getq1(params);
    const Real qe = paramInterpret.getqe(params);
    const Real UeVinf = qe / qinf;

    const Real rhoe = gasModel.density(qe, p0, T0);
    const Real nue = viscosityModel.dynamicViscosity()/rhoe;

    const auto secondaryVarFcn
      = secondaryVarObj.getCalculatorThicknessesCoefficients(profileCat,params,var,nue);

    const Real delta1s = secondaryVarFcn.getdelta1s();
    const Real theta11 = secondaryVarFcn.gettheta11();
    const Real theta1s = secondaryVarFcn.gettheta1s();

    const Real Ret = qe * theta11 / nue;

    Real beta = 0.0; // only needed for BL flows; set to zero in wake
    if (profileCat == IBLProfileCategory::LaminarBL ||
        profileCat == IBLProfileCategory::TurbulentBL)
    {
      beta = - delta1s/(0.5*secondaryVarFcn.getRetCf1()*nue/theta11)
             * paramInterpret.getdivq1(params);
    }

    OutputTrue.at(i)[(int) outputID::deltaLami] = varInterpret.getdeltaLami(var);
    OutputTrue.at(i)[(int) outputID::ALami] = varInterpret.getALami(var);
    OutputTrue.at(i)[(int) outputID::delta1s] = delta1s;
    OutputTrue.at(i)[(int) outputID::theta11] = theta11;
    OutputTrue.at(i)[(int) outputID::theta1s] = theta1s;
    OutputTrue.at(i)[(int) outputID::Cf1] = secondaryVarFcn.getRetCf1() / Ret;
    OutputTrue.at(i)[(int) outputID::CD] = secondaryVarFcn.getRetCD() / Ret;
    OutputTrue.at(i)[(int) outputID::H] = delta1s / theta11;
    OutputTrue.at(i)[(int) outputID::Hs] = theta1s / theta11;
    OutputTrue.at(i)[(int) outputID::Retheta] = Ret;
    OutputTrue.at(i)[(int) outputID::UeVinf] = UeVinf;
    OutputTrue.at(i)[(int) outputID::cp] = 1 - UeVinf*UeVinf;
    OutputTrue.at(i)[(int) outputID::m] = rhoe*qe*delta1s;
    OutputTrue.at(i)[(int) outputID::qx] = q1[0];
    OutputTrue.at(i)[(int) outputID::qz] = q1[1];
    OutputTrue.at(i)[(int) outputID::nt] = nt;
    OutputTrue.at(i)[(int) outputID::ctau] = ctau;
    OutputTrue.at(i)[(int) outputID::gam] = gam;
    OutputTrue.at(i)[(int) outputID::beta] = beta;
    OutputTrue.at(i)[(int) outputID::ctaueq] = secondaryVarFcn.getctaueq();
    OutputTrue.at(i)[(int) outputID::deltaTurb] = varInterpret.getdeltaTurb(var);
    OutputTrue.at(i)[(int) outputID::ATurb] = varInterpret.getATurb(var);
  }

  // check
  const int nOutput = ElementProjectionType::nOutput;

  for (int i = 0; i < nBasis; ++i)
  {
    for (int n = 0; n < nOutput; ++n)
    {
      SANS_CHECK_CLOSE( OutputTrue[i][n], outputfld.DOF(i)[n], tol, tol );
#ifdef DISPLAY_FOR_DEBUGGING
      cout.precision(8);
      cout.setf(ios::scientific, ios::floatfield);
      cout << "OutputTrue["    <<i<<"]["<<n<<"] = " << setw(15) << OutputTrue[i][n] << ",     "
           << "outputfld.DOF(" <<i<<")["<<n<<"] = " << setw(15) << outputfld.DOF(i)[n]<< endl;
#endif
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
