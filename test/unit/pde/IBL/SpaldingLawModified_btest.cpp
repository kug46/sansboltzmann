// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// SpaldingLawModified_btest
//
// test of the modified Spalding Law class [key words: IBL, turbulent velocity profile]

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "SANS_btest.h"

#include "tools/SANSException.h"
#include "pde/IBL/SpaldingLawModified.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( SpaldingLawModified_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( wallprofile_test )
{
  // settings
  const Real tol = 7.e-14;

  SpaldingLawModified spalding;

  { // etabar = 0
    Real yplus = 0.0;
    Real etabar = 0.0;

    Real uplus = spalding.wallprofile(yplus, etabar);
    Real uplusTrue = 0.0;

    BOOST_CHECK_CLOSE( uplusTrue, uplus, tol );
  }

  {
    Real yplus = 1.0;
    Real etabar = 0.6;

    Real uplus = spalding.wallprofile(yplus, etabar);
#if ISIBL3SRCTURB
    Real uplusTrue = 0.405996681584915; //from MATLAB
#else
    Real uplusTrue = 0.97393196534857462; //from MATLAB
#endif
    BOOST_CHECK_CLOSE( uplusTrue, uplus, tol );
  }

  {
    Real yplus = 20.0;
    Real etabar = 0.6;

    Real uplus = spalding.wallprofile(yplus, etabar);
#if ISIBL3SRCTURB
    Real uplusTrue = 7.389789556954178;
#else
    Real uplusTrue = 11.14935884955823119924;
#endif
    BOOST_CHECK_CLOSE( uplusTrue, uplus, tol );
  }

  {
    Real yplus = 60.0;
    Real etabar = 0.8;

    Real uplus = spalding.wallprofile(yplus, etabar);
#if ISIBL3SRCTURB
    Real uplusTrue = 11.454266596321057;
#else
    Real uplusTrue = 14.45454967211574093255;
#endif

    BOOST_CHECK_CLOSE( uplusTrue, uplus, tol );
  }

  { // negative yplus
    Real yplus = -1.e0;
    Real etabar = 0.8;

    BOOST_CHECK_THROW( spalding.wallprofile(yplus, etabar);, DeveloperException );
  }

  {
    const Real kappa_true = 0.41;
    const Real B_true = 5.0;
    const Real newtonRelativeTolTrue = 1E-10, newtonAbsoluteTolTrue = 1e-12;
    const int maxNewtonIter_true = 3;
    SpaldingLawModified spaldingNew(kappa_true, B_true, newtonRelativeTolTrue, newtonAbsoluteTolTrue, maxNewtonIter_true);

    Real yplus = 20.0;
    Real etabar = 0.6;

    BOOST_CHECK_THROW( spaldingNew.wallprofile(yplus, etabar);, DeveloperException );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( customParameter_test )
{
  // settings
  const Real tol = 1.E-15;

  // test variables
  const Real kappa_true = 0.3;
  const Real B_true = 6.0;
  const Real newtonRelativeTolTrue = 1.E-3, newtonAbsoluteTolTrue = 1.E-6;
  const int maxNewtonIter_true = 66;
  SpaldingLawModified spalding( kappa_true, B_true, newtonRelativeTolTrue, newtonAbsoluteTolTrue, maxNewtonIter_true );

  Real kappa, B, newtonRelTol, newtonAbsTol;
  int maxNewtonIter;

  spalding.getParameter(kappa, B, newtonRelTol, newtonAbsTol, maxNewtonIter);

  SANS_CHECK_CLOSE( kappa_true, kappa, tol, tol );
  SANS_CHECK_CLOSE( B_true, B, tol, tol );
  SANS_CHECK_CLOSE( newtonRelativeTolTrue, newtonRelTol, tol, tol );
  SANS_CHECK_CLOSE( newtonAbsoluteTolTrue, newtonAbsTol, tol, tol );
  BOOST_CHECK_EQUAL(maxNewtonIter_true, maxNewtonIter);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
