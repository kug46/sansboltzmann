// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCIBL2D_btest
//
// test of 2-D IBL BC classes

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/IBL/BCIBLUniField.h"

#define BCPARAMETERS_INSTANTIATE // included because testing without NDConvert
#include "pde/BCParameters_impl.h"

namespace SANS
{
typedef PhysD2 PhysDim;
typedef VarData2DDsThG VarDataType;
typedef VarTypeDsThG VarType;
typedef PDEIBLUniField<PhysDim,VarType> PDEClass;

typedef typename PDEClass::VarInterpType VarInterpretType;

typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
typedef typename PDEClass::template ArrayParam<Real> ArrayParam;
typedef typename PDEClass::template VectorX<Real> VectorX;
typedef typename PDEClass::ProfileCategory ProfileCategory;
typedef typename PDEClass::GasModelType GasModelType;
typedef typename PDEClass::ViscosityModelType ViscosityModelType;
typedef typename PDEClass::TransitionModelType TransitionModelType;

typedef BCNone<PhysD2,PDEClass::N> BCClassNone;
typedef BCIBLUniField2D<BCTypeWakeMatchUniField,VarType> BCClassWakeMatch;
typedef typename BCClassWakeMatch::ParamsType BCParamType_WakeMatch;
typedef BCParameters<BCIBLUniField2DVector<VarType> > BCParamsIBL;

//Explicitly instantiate classes so coverage information is correct
template class BCIBLUniField2D<BCTypeWakeMatchUniField,VarType>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BCIBLUniField2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  { // BCNone
    BOOST_CHECK( (std::is_same<typename BCCategory::None, typename BCClassNone::Category>::value) );
    BOOST_CHECK( (std::is_same<BCNone<PhysDim,PDEClass::N>::ParamsType, typename BCClassNone::ParamsType>::value) );

    BOOST_CHECK( (std::is_same<typename BCClassNone::PhysDim, PhysDim>::value) );
    // BOOST_CHECK( (std::is_same<typename BCClassNone::PDEClass, PDEClass>::value) );

    BOOST_CHECK( BCClassNone::D == PhysDim::D );
    BOOST_CHECK( BCClassNone::N == PDEClass::N );
    BOOST_CHECK( BCClassNone::NBC == 0 );

    BOOST_CHECK( (std::is_same<typename BCClassNone::template ArrayQ<Real>, typename PDEClass::template ArrayQ<Real>>::value) );
    BOOST_CHECK( (std::is_same<typename BCClassNone::template MatrixQ<Real>, typename PDEClass::template MatrixQ<Real>>::value) );
  }

  { // BCTypeWakeMatchUniField
    BOOST_CHECK( (std::is_same<typename BCCategory::HubTrace, typename BCClassWakeMatch::Category>::value) );
    BOOST_CHECK( (std::is_same<BCIBL2DParamsUniField<BCTypeWakeMatchUniField>, typename BCClassWakeMatch::ParamsType>::value) );

    BOOST_CHECK( (std::is_same<typename BCClassWakeMatch::PhysDim, PhysDim>::value) );
    BOOST_CHECK( (std::is_same<typename BCClassWakeMatch::PDEClass, PDEClass>::value) );

    BOOST_CHECK( BCClassWakeMatch::D == PhysDim::D );
    BOOST_CHECK( BCClassWakeMatch::N == PDEClass::N );
    BOOST_CHECK( BCClassWakeMatch::NBC == 0 );

    BOOST_CHECK( (std::is_same<typename BCClassWakeMatch::template ArrayQ<Real>, typename PDEClass::template ArrayQ<Real>>::value) );
    BOOST_CHECK( (std::is_same<typename BCClassWakeMatch::template MatrixQ<Real>, typename PDEClass::template MatrixQ<Real>>::value) );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_test )
{
  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const typename PDEClass::GasModelType gasModel(gasModelDict);

  const typename PDEClass::ViscosityModelType viscosityModel(1.8725e-5);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const typename PDEClass::TransitionModelType transitionModel(transitionModelDict);

  // pde
  PDEClass pde(gasModel, viscosityModel, transitionModel);

  { // BCNone
    // pde constructor
    BCClassNone bc1;

    // PyDict constructor
    PyDict BCNoneArgs;
    BCNoneArgs[BCParamsIBL::params.BC.BCType] = BCParamsIBL::params.BC.None;

    BCClassNone bc2(pde,BCNoneArgs);

    PyDict PyBCList;
    PyBCList["BCNone"] = BCNoneArgs;

    BCParamsIBL::checkInputs(PyBCList);
  }

  { // BCTypeWakeMatchUniField
    // enum constructor
    {
      BCClassWakeMatch bc1(pde, BCParamType_WakeMatch::params.matchingType.trailingEdge);
      BCClassWakeMatch bc2(pde, BCParamType_WakeMatch::params.matchingType.wakeInflow);

      BOOST_CHECK_THROW( BCClassWakeMatch(pde, "InvalidMatchingType");, SANSException );
    }

    // PyDict constuctor
    PyDict BCWakeMatchArgs;
    BCWakeMatchArgs[BCParamsIBL::params.BC.BCType] = BCParamsIBL::params.BC.MatchingUniField;
    { // TE
      BCWakeMatchArgs[BCParamType_WakeMatch::params.matchingType] = BCParamType_WakeMatch::params.matchingType.trailingEdge;

      PyDict PyBCList;
      PyBCList["BCWakeMatch_TE"] = BCWakeMatchArgs;

      BCParamsIBL::checkInputs(PyBCList);

      BCClassWakeMatch bc1(pde,BCWakeMatchArgs);
    }

    { // Wake inflow
      BCWakeMatchArgs[BCParamType_WakeMatch::params.matchingType] = BCParamType_WakeMatch::params.matchingType.wakeInflow;

      PyDict PyBCList;
      PyBCList["BCWakeMatch_WakeInflow"] = BCWakeMatchArgs;

      BCParamsIBL::checkInputs(PyBCList);

      BCClassWakeMatch bc1(pde,BCWakeMatchArgs);
    }

    { // wrong BC type
      BCWakeMatchArgs[BCParamType_WakeMatch::params.matchingType] = "WrongWakeMatchBCType";

      BOOST_CHECK_THROW( BCClassWakeMatch bc1(pde,BCWakeMatchArgs);, SANSException );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCNone_test )
{
  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const typename PDEClass::GasModelType gasModel(gasModelDict);

  const typename PDEClass::ViscosityModelType viscosityModel(1.8725e-5);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const typename PDEClass::TransitionModelType transitionModel(transitionModelDict);

  // pde
  PDEClass pde(gasModel, viscosityModel, transitionModel);

  PyDict BCArgs;
  BCArgs[BCParamsIBL::params.BC.BCType] = BCParamsIBL::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCNone"] = BCArgs;

  BCParamsIBL::checkInputs(PyBCList);

  BCClassNone bc(pde,BCArgs);

  BOOST_CHECK( bc.useFluxViscous() == false );

  // state

  const Real nx = 1.0;
  const Real ny = 0.0;

  // isValidState
  const std::vector<Real> delta1s_arr = {-1.0, 0.0, 1.0, 2.0};
  const std::vector<Real> theta11_arr = {-1.0, 0.0, 1.0};

  const Real G = -5.0;

  for (int j = 0; j < (int) delta1s_arr.size(); ++j)
  {
    Real delta1s = delta1s_arr.at(j);
    for (int k = 0; k < (int) theta11_arr.size(); ++k)
    {
      Real theta11 = theta11_arr.at(k);
      ArrayQ qI = pde.setDOFFrom( VarDataType(delta1s, theta11, G) );
      BOOST_CHECK( bc.isValidState(nx,ny,qI) == true );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeWakeMatchUniField_test )
{
  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const typename PDEClass::GasModelType gasModel(gasModelDict);

  const typename PDEClass::ViscosityModelType viscosityModel(1.8725e-5);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const typename PDEClass::TransitionModelType transitionModel(transitionModelDict);

  // pde
  PDEClass pde(gasModel, viscosityModel, transitionModel);

  BCClassWakeMatch bc(pde, BCParamType_WakeMatch::params.matchingType.trailingEdge);

  const Real nx = 1;
  const Real ny = 0;

  const std::vector<Real> delta1s_arr = {-1.0, 0.0, 1.0, 2.0};
  const std::vector<Real> theta11_arr = {-1.0, 0.0, 1.0};

  const Real G = -5.0;

  for (int j = 0; j < (int) delta1s_arr.size(); ++j)
  {
    Real delta1s = delta1s_arr.at(j);
    for (int k = 0; k < (int) theta11_arr.size(); ++k)
    {
      Real theta11 = theta11_arr.at(k);
      ArrayQ qI = pde.setDOFFrom( VarDataType(delta1s, theta11, G) );
      BOOST_CHECK( bc.isValidState(nx,ny,qI) == pde.isValidState(qI) );
    }
  }
}

////----------------------------------------------------------------------------//
//BOOST_AUTO_TEST_CASE( BCTypeWakeMatchUniField_TrailingEdge_fluxMatchingSourceHubtrace_test )
//{
//  const Real small_tol = 1e-15;
//  const Real close_tol = 2E-13;
//
//  const Real gam = 1.6;
//  const Real R = 66.0;
//  const Real mue = 7.0E-5;
//
//  const auto& matchingType = BCParamType_WakeMatch::params.matchingType.trailingEdge;
//
//  const Real x = 0.1;
//  const Real z = 3.8;
//  const Real t = 0.3;
//
//  const Real delta1s = 0.02, theta11 = 0.01, G = -5.0;
//  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
//  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;
//
//  VarInterpretType varInterpret;
//  ArrayQ var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));
//
//  BOOST_CHECK( varInterpret.isValidState(var) );
//
//  ArrayQ varhb = 0.0;
//  varhb[PDEClass::ir_mom] = -0.1;
//  varhb[PDEClass::ir_ke] = -0.2;
//  varhb[PDEClass::ir_amplag] = -0.3;
//
//  const typename PDEClass::ParamInterpType paramInterpret;
//  const ArrayParam params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);
//
//  const VectorX e1 = {0.6, 0.8};
//  const VectorX nrm = {0.8, 0.6};
//  const Real nx = nrm[0], nz = nrm[1];
//
//  // models
//  PyDict gasModelDict;
//  gasModelDict[GasModelParamsIBL::params.gamma] = gam;
//  gasModelDict[GasModelParamsIBL::params.R] = R;
//  GasModelParamsIBL::checkInputs(gasModelDict);
//  const GasModelType gasModel(gasModelDict);
//
//  ViscosityModelType viscosityModel(mue);
//
//  {
//    PyDict transitionModelDict;
//    transitionModelDict[TransitionModelParams::params.isTransitionActive] = true;
//    transitionModelDict[TransitionModelParams::params.profileCatDefault]
//                             = TransitionModelParams::params.profileCatDefault.laminarBL;
//    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
//    const TransitionModelType transitionModel(transitionModelDict);
//
//    PDEClass pde(gasModel, viscosityModel, transitionModel);
//
//    BCClassWakeMatch bc(pde, matchingType);
//
//    const ProfileCategory profileCat = pde.getProfileCategory(var,x);
//    const auto& paramInterpret_ = pde.getParamInterpreter();
//    const Real qe = paramInterpret_.getqe(params);
//    const Real rhoe = (pde.getGasModel()).density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));
//    const Real nue = (pde.getViscosityModel()).dynamicViscosity() / rhoe;
//
//    const auto& thicknessesCoefficientsFunctor
//      = (pde.getSecondaryVarObj()).getCalculatorThicknessesCoefficients(profileCat, params, var, nue);
//
//    ArrayQ fTrue = 0.0; // zero by default
//    {
//      ArrayQ fx = 0.0, fz = 0.0;
//      pde.fluxAdvective(params, e1, x, z, t, var, fx, fz);
//
//      fTrue[PDEClass::ir_mom] = fx[PDEClass::ir_mom]*nx + fz[PDEClass::ir_mom]*nz;
//      fTrue[PDEClass::ir_ke] = fx[PDEClass::ir_ke]*nx + fz[PDEClass::ir_ke]*nz;
//      fTrue[PDEClass::ir_amplag] = fx[PDEClass::ir_amplag]*nx + fz[PDEClass::ir_amplag]*nz;
//    }
//
//    ArrayQ sourcehubTrue = 0.0;
//    {
//      const Real delta1s = thicknessesCoefficientsFunctor.getdelta1s();
//      const Real theta11 = thicknessesCoefficientsFunctor.gettheta11();
//
//      const Real Hk = thicknessesCoefficientsFunctor.getHk();
//      const Real ctau_eq = thicknessesCoefficientsFunctor.getctaueq();
//      const Real CTR = 1.8*exp(-3.3/(Hk-1.0));
//      const Real ctau_TurbInit = pow(CTR,2.0)*ctau_eq; // Matching: sum ctau weighted by momentum defect thicknesses (XFOIL,1989)
//
//      sourcehubTrue[PDEClass::ir_mom] = delta1s + fabs(z);
//      sourcehubTrue[PDEClass::ir_ke] = theta11;
//      sourcehubTrue[PDEClass::ir_amplag] = theta11 * ctau_TurbInit;
//    }
//
//    ArrayQ f = 0.0, sourcehub = 0.0;
//
//    // Matching flux at a hubtrace
//    bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);
//    for ( int i = 0; i < PDEClass::N; i++ )
//    {
//      SANS_CHECK_CLOSE( fTrue[i], f[i], small_tol, close_tol );
//      SANS_CHECK_CLOSE( sourcehubTrue[i], sourcehub[i], small_tol, close_tol );
//    }
//
//    // cumulation: matching flux from airfoil to wake
//    bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);
//    for ( int i = 0; i < PDEClass::N; i++ )
//    {
//      SANS_CHECK_CLOSE( 2.0*fTrue[i], f[i], small_tol, close_tol );
//      SANS_CHECK_CLOSE( 2.0*sourcehubTrue[i], sourcehub[i], small_tol, close_tol );
//    }
//  }
//
//  {
//    PyDict transitionModelDict;
//    transitionModelDict[TransitionModelParams::params.isTransitionActive] = false;
//    transitionModelDict[TransitionModelParams::params.profileCatDefault]
//                             = TransitionModelParams::params.profileCatDefault.laminarBL;
//    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
//    const TransitionModelType transitionModel(transitionModelDict);
//
//    PDEClass pde(gasModel, viscosityModel, transitionModel);
//
//    BCClassWakeMatch bc(pde, matchingType);
//
//    const ProfileCategory profileCat = pde.getProfileCategory(var,x);
//    const auto& paramInterpret_ = pde.getParamInterpreter();
//    const Real qe = paramInterpret_.getqe(params);
//    const Real rhoe = (pde.getGasModel()).density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));
//    const Real nue = (pde.getViscosityModel()).dynamicViscosity() / rhoe;
//
//    const auto& thicknessesCoefficientsFunctor
//      = (pde.getSecondaryVarObj()).getCalculatorThicknessesCoefficients(profileCat, params, var, nue);
//
//    ArrayQ fTrue = 0.0; // zero by default
//    {
//      ArrayQ fx = 0.0, fz = 0.0;
//      pde.fluxAdvective(params, e1, x, z, t, var, fx, fz);
//
//      fTrue = fx*nx + fz*nz;
//    }
//
//    ArrayQ sourcehubTrue = 0.0;
//    {
//      const Real delta1s = thicknessesCoefficientsFunctor.getdelta1s();
//      const Real theta11 = thicknessesCoefficientsFunctor.gettheta11();
//
//      const Real Hk = thicknessesCoefficientsFunctor.getHk();
//      const Real ctau_eq = thicknessesCoefficientsFunctor.getctaueq();
//      const Real CTR = 1.8*exp(-3.3/(Hk-1.0));
//      const Real ctau_TurbInit = pow(CTR,2.0)*ctau_eq; // Matching: sum ctau weighted by momentum defect thicknesses (XFOIL,1989)
//
//      sourcehubTrue[PDEClass::ir_mom] = delta1s + fabs(z);
//      sourcehubTrue[PDEClass::ir_ke] = theta11;
//      sourcehubTrue[PDEClass::ir_amplag] = theta11 * ctau_TurbInit;
//    }
//
//    ArrayQ f = 0.0, sourcehub = 0.0;
//
//    // Matching flux at a hubtrace
//    bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);
//    for ( int i = 0; i < PDEClass::N; i++ )
//    {
//      SANS_CHECK_CLOSE( fTrue[i], f[i], small_tol, close_tol );
//      SANS_CHECK_CLOSE( sourcehubTrue[i], sourcehub[i], small_tol, close_tol );
//    }
//
//    // cumulation: matching flux from airfoil to wake
//    bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);
//    for ( int i = 0; i < PDEClass::N; i++ )
//    {
//      SANS_CHECK_CLOSE( 2.0*fTrue[i], f[i], small_tol, close_tol );
//      SANS_CHECK_CLOSE( 2.0*sourcehubTrue[i], sourcehub[i], small_tol, close_tol );
//    }
//  }
//
//  {
//    PyDict transitionModelDict;
//    transitionModelDict[TransitionModelParams::params.profileCatDefault]
//                        = TransitionModelParams::params.profileCatDefault.turbulentBL;
//    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
//    const TransitionModelType transitionModel(transitionModelDict);
//
//    PDEClass pde(gasModel, viscosityModel, transitionModel);
//
//    BCClassWakeMatch bc(pde, matchingType);
//
//    const ProfileCategory profileCat = pde.getProfileCategory(var,x);
//    const auto& paramInterpret_ = pde.getParamInterpreter();
//    const Real qe = paramInterpret_.getqe(params);
//    const Real rhoe = (pde.getGasModel()).density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));
//    const Real nue = (pde.getViscosityModel()).dynamicViscosity() / rhoe;
//
//    const auto& thicknessesCoefficientsFunctor
//      = (pde.getSecondaryVarObj()).getCalculatorThicknessesCoefficients(profileCat, params, var, nue);
//
//    ArrayQ fTrue = 0.0; // zero by default
//    {
//      ArrayQ fx = 0.0, fz = 0.0;
//      pde.fluxAdvective(params, e1, x, z, t, var, fx, fz);
//
//      fTrue = fx*nx + fz*nz;
//    }
//
//    ArrayQ sourcehubTrue = 0.0;
//    {
//      const Real delta1s = thicknessesCoefficientsFunctor.getdelta1s();
//      const Real theta11 = thicknessesCoefficientsFunctor.gettheta11();
//
//      sourcehubTrue[PDEClass::ir_mom] = delta1s + fabs(z);
//      sourcehubTrue[PDEClass::ir_ke] = theta11;
//      sourcehubTrue[PDEClass::ir_amplag] = theta11 * varInterpret.getctau(var);
//    }
//
//    ArrayQ f = 0.0, sourcehub = 0.0;
//
//    // Matching flux at a hubtrace
//    bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);
//    for ( int i = 0; i < PDEClass::N; i++ )
//    {
//      SANS_CHECK_CLOSE( fTrue[i], f[i], small_tol, close_tol );
//      SANS_CHECK_CLOSE( sourcehubTrue[i], sourcehub[i], small_tol, close_tol );
//    }
//
//    // cumulation: matching flux from airfoil to wake
//    bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);
//    for ( int i = 0; i < PDEClass::N; i++ )
//    {
//      SANS_CHECK_CLOSE( 2.0*fTrue[i], f[i], small_tol, close_tol );
//      SANS_CHECK_CLOSE( 2.0*sourcehubTrue[i], sourcehub[i], small_tol, close_tol );
//    }
//  }
//
//  {
//    PyDict transitionModelDict;
//    transitionModelDict[TransitionModelParams::params.profileCatDefault]
//                        = TransitionModelParams::params.profileCatDefault.turbulentWake;
//    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
//    const TransitionModelType transitionModel(transitionModelDict);
//
//    PDEClass pde(gasModel, viscosityModel, transitionModel);
//
//    BCClassWakeMatch bc(pde, matchingType);
//
//    ArrayQ f = 0.0, sourcehub = 0.0;
//
//    BOOST_CHECK_THROW( bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);,
//                       SANSException);
//
//  }
//}
//
////----------------------------------------------------------------------------//
//BOOST_AUTO_TEST_CASE( BCTypeWakeMatchUniField_WakeInflow_fluxMatchingSourceHubtrace_test )
//{
//  const Real small_tol = 1e-15;
//  const Real close_tol = 1.1E-13;
//
//  const Real gam = 1.6;
//  const Real R = 66.0;
//  const Real mue = 7.0E-5;
//
//  const auto& matchingType = BCParamType_WakeMatch::params.matchingType.wakeInflow;
//
//  const Real x = 0.1;
//  const Real z = 3.8;
//  const Real t = 0.3;
//
//  const Real delta1s = 0.02, theta11 = 0.01, G = -5.0;
//  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
//  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;
//
//  VarInterpretType varInterpret;
//  ArrayQ var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));
//
//  BOOST_CHECK( varInterpret.isValidState(var) );
//
//  ArrayQ varhb = 0.0;
//  varhb[PDEClass::ir_mom] = -0.1;
//  varhb[PDEClass::ir_ke] = -0.2;
//  varhb[PDEClass::ir_amplag] = -0.3;
//
//  const typename PDEClass::ParamInterpType paramInterpret;
//  const ArrayParam params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);
//
//  const VectorX e1 = {0.6, 0.8};
//  const VectorX nrm = {0.8, 0.6};
//  const Real nx = nrm[0], nz = nrm[1];
//
//  // models
//  PyDict gasModelDict;
//  gasModelDict[GasModelParamsIBL::params.gamma] = gam;
//  gasModelDict[GasModelParamsIBL::params.R] = R;
//  GasModelParamsIBL::checkInputs(gasModelDict);
//  const GasModelType gasModel(gasModelDict);
//
//  ViscosityModelType viscosityModel(mue);
//
//  {
//    PyDict transitionModelDict;
//    transitionModelDict[TransitionModelParams::params.profileCatDefault]
//                        = TransitionModelParams::params.profileCatDefault.turbulentWake;
//    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
//    const TransitionModelType transitionModel(transitionModelDict);
//
//    PDEClass pde(gasModel, viscosityModel, transitionModel);
//
//    BCClassWakeMatch bc(pde, matchingType);
//
//    const ProfileCategory profileCat = pde.getProfileCategory(var,x);
//    const auto& paramInterpret_ = pde.getParamInterpreter();
//    const Real qe = paramInterpret_.getqe(params);
//    const Real rhoe = (pde.getGasModel()).density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));
//    const Real nue = (pde.getViscosityModel()).dynamicViscosity() / rhoe;
//
//    const auto& thicknessesCoefficientsFunctor
//      = (pde.getSecondaryVarObj()).getCalculatorThicknessesCoefficients(profileCat, params, var, nue);
//
//    ArrayQ fTrue = 0.0; // zero by default
//    {
//      ArrayQ fx = 0.0, fz = 0.0;
//      pde.fluxAdvective(params, e1, x, z, t, var, fx, fz);
//
//      fTrue = varhb;
//    }
//
//    ArrayQ sourcehubTrue = 0.0;
//    {
//      const Real delta1s = thicknessesCoefficientsFunctor.getdelta1s();
//      const Real theta11 = thicknessesCoefficientsFunctor.gettheta11();
//
//      sourcehubTrue[PDEClass::ir_mom] = -delta1s; // delta1s is computed from MATLAB
//      sourcehubTrue[PDEClass::ir_ke] = -theta11; // theta11 is computed from MATLAB
//      sourcehubTrue[PDEClass::ir_amplag] = -theta11 * varInterpret.getctau(var);
//    }
//
//    ArrayQ f = 0.0, sourcehub = 0.0;
//
//    // Matching flux at a hubtrace
//    bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);
//    for ( int i = 0; i < PDEClass::N; i++ )
//    {
//      SANS_CHECK_CLOSE( fTrue[i], f[i], small_tol, close_tol );
//      SANS_CHECK_CLOSE( sourcehubTrue[i], sourcehub[i], small_tol, close_tol );
//    }
//
//    // cumulation: matching flux from airfoil to wake
//    bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);
//    for ( int i = 0; i < PDEClass::N; i++ )
//    {
//      SANS_CHECK_CLOSE( 2.0*fTrue[i], f[i], small_tol, close_tol );
//      SANS_CHECK_CLOSE( 2.0*sourcehubTrue[i], sourcehub[i], small_tol, close_tol );
//    }
//  }
//
//  {
//    PyDict transitionModelDict;
//    transitionModelDict[TransitionModelParams::params.profileCatDefault]
//                        = TransitionModelParams::params.profileCatDefault.laminarBL;
//    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
//    const TransitionModelType transitionModel(transitionModelDict);
//
//    PDEClass pde(gasModel, viscosityModel, transitionModel);
//
//    BCClassWakeMatch bc(pde, matchingType);
//
//    ArrayQ f = 0.0, sourcehub = 0.0;
//
//    BOOST_CHECK_THROW( bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);,
//                       SANSException);
//
//  }
//
//  {
//    PyDict transitionModelDict;
//    transitionModelDict[TransitionModelParams::params.profileCatDefault]
//                        = TransitionModelParams::params.profileCatDefault.turbulentBL;
//    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
//    const TransitionModelType transitionModel(transitionModelDict);
//
//    PDEClass pde(gasModel, viscosityModel, transitionModel);
//
//    BCClassWakeMatch bc(pde, matchingType);
//
//    ArrayQ f = 0.0, sourcehub = 0.0;
//
//    BOOST_CHECK_THROW( bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);,
//                       SANSException);
//
//  }
//
//  {
//    PyDict transitionModelDict;
//    transitionModelDict[TransitionModelParams::params.profileCatDefault]
//                        = TransitionModelParams::params.profileCatDefault.laminarWake;
//    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
//    const TransitionModelType transitionModel(transitionModelDict);
//
//    PDEClass pde(gasModel, viscosityModel, transitionModel);
//
//    BCClassWakeMatch bc(pde, matchingType);
//
//    ArrayQ f = 0.0, sourcehub = 0.0;
//
//    BOOST_CHECK_THROW( bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);,
//                       SANSException);
//
//  }
//}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
