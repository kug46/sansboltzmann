function u_p0 = wallProfInitial(y_p,k,B)
% This function computes the initial guess of u^+.

if 0<=y_p && y_p<7
    u_p0 = y_p;
elseif 7<=y_p && y_p<40
    u_p0 = 1.6/k*log(y_p) + B - 5.9;
elseif 40<=y_p
    u_p0 = 1.0/k*log(y_p) + B - 0.4;
else
    error('y_p is negative')
end