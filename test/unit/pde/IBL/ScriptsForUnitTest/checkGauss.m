% This scripts performs convergence study on Guass-Legendre quadrature.
clear all; close all;

%% variables
Re_d_array = 5.*10.^[2,3,4,5,6]';
A_array = [1.0,5.0;  0.8,20.0;  1.5,100.0;  1.0,700.0;  5.0,4000.0];
Nq_array = [1 2 4 8 16 32 64 128 256 512 2000]; % initialize; number of quadrature points
quadr = 'gauss';

%% execution
for iR = 3%:length(Re_d_array)
    for iA = 2%:size(A_array,2)
        theta_nq = zeros(size(Nq_array));
        for iq = 3%length(Nq_array)
            A = A_array(iR,iA) % turbulent viscous velocity streamwise shape parameter
            Re_d =  Re_d_array(iR) % BL thickness Reynolds number
            Nq = Nq_array(iq) % number of Gauss quadrature points across viscous layer; quadrature order up to 2*Nq - 1
            
            %% construct shear layer profile
            delta = 1; % reference viscous layer thickness
            q_i = 2.0; % reference EIF edge velocity (not used)
            etaqLim = [0, 1]; % integration limits on \etab
            [etaq,wq] = lgwt(etaqLim,Nq); % evaluate Gaussian quadrature weights and locations
            
            U = zeros(Nq,1); % initialize
            R = zeros(Nq,1); % initialize
            for i = 1:Nq
                U(i,1) = TurbProf(etaq(i),A,Re_d,q_i); % viscous velocity streamwise component; evaluate at quadrature points
                R(i,1) = 1; % viscous density profile; evaluate at quadrature points
            end
            
            %% evaluate thickness integrals using viscous profiles by quadrature
            delta_star = delta * wq' * ( 1 - R.*U ); % \delta^*;
            theta = delta * wq' * ( (1 - U) .* R .* U ); % \theta
            theta_star = delta * wq' * ( (1 - U.^2) .* R .* U ); % \theta
            delta_prime = delta * wq' * ( 1 - U ); % \delta^';
            delta_rho = delta * wq' * ( 1 - R ); % \delta_rho;
            delta_q = delta * wq' * ( 1 - U.^2 ); % \delta_q;
            
            theta_nq(iq) = theta; % record theta for nq (number of quadrature points)
        end
        relerr = abs( (theta_nq(1:end-1) - theta_nq(end)) / theta_nq(end) ); % compute error in theta; using result of highest quadrature order as reference
        
        %% velocity profile for plot
        q_i = 2.0;
        eta_vec = linspace(0,1,50);    
        U_plot = zeros(size(eta_vec)); % U to be plotted
        for i = 1:length(eta_vec)
            eta = eta_vec(i);
            U_plot(i) = TurbProf(eta,A,Re_d,q_i);
        end
        
        %% Save data
        % case naming
        if mod(A,1) < 0.01
            caseName = sprintf('A%d_Re%d_%s',A,Re_d,quadr);
        else
            if A < 1
                caseName = sprintf('A0p%d_Re%d_%s',10*mod(A,1)-mod(10*mod(A,1),1),Re_d,quadr);
            else
                caseName = sprintf('A%dp%d_Re%d_%s',A-mod(A,1),10*mod(A,1)-mod(10*mod(A,1),1),Re_d,quadr);
            end
        end
        % saving data
        datname = sprintf('%s.mat',caseName);
        clear fig; % avoid saving figure handles
        save(datname);
        
        %% Post-processing
        % error convergence
        fig = figure(1);
        fig.PaperUnits = 'inches';
        fig.PaperPosition = [0 0 10 4];
        
        subplot(1,2,2)
        set(gca,'fontsize',25)
        loglog(Nq_array(1:end-1),relerr,'k-x',...
            'markersize',7)
        title({sprintf('$A = %5.1f, Re_\\delta =5\\times10^%d, \\theta_{ref} = %2.6f$',A,log10(Re_d/5),theta_nq(end))}, ...
            'interpreter','latex')
        xlabel('Number of quadrature nodes $N_q$', 'interpreter','latex')
        ylabel('Relative error $|\theta - \theta_{ref}| / |\theta_{ref}|$', 'interpreter','latex')
        
        % velocity profile
        subplot(1,2,1)
        plot(eta_vec,U_plot,'k-+')
        xlabel('\eta')
        ylabel('U')
        
%         % print figure
%         figname = sprintf('%s.eps', caseName);
%         print('-depsc2',figname);
%         unix(sprintf('epstopdf %s', figname));
%         delete(figname); % delete eps files
    end
end

% %% print unit test data
% fprintf('%1.20f\n',delta_star);
% fprintf('%1.20f\n',theta);
% fprintf('%1.20f\n',theta_star);
% fprintf('%1.20f\n',delta_prime);
% fprintf('%1.20f\n',delta_rho);
% fprintf('%1.20f\n',delta_q);