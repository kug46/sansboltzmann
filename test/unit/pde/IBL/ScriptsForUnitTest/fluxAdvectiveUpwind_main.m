clear all; close all;

%% state vector
deltaL = 1.2;
AL = 2.3;
qxL = 4;  
qzL = 3;  
p0L = 1.2E+5;  
T0L = 300;

varL = [deltaL; AL; qxL; qzL; p0L; T0L];
varxL = 0 * varL;
varzL = 0 * varL;

deltaR = 0.2;
AR = -0.2;
qxR = -2.3;  
qzR = 0.9;  
p0R = 0.3E+5;  
T0R = 400;

varR = [deltaR; AR; qxR; qzR; p0R; T0R];
varxR = 0 * varR;
varzR = 0 * varR;

%% parameters
e1L = [0.6; 0.8]; % basis vector
e1R = [-0.8; 0.6]; % basis vector
Nrm = [sqrt(0.5); sqrt(0.5)]; % trace normal

param = struct;

param.gam_ = 1.4;
param.R_ = 287.15;
param.mui_ = 1.827E-5;
param.Pr_ = 0.715;

%%
varType = 'DAGVelcoityStagPT';
q1L = varInterpret(varL,varxL,varzL,varType,'VelocityEdge');
p0L = varInterpret(varL,varxL,varzL,varType,'p0Edge');
T0L = varInterpret(varL,varxL,varzL,varType,'T0Edge');

qL = varInterpret(varL,varxL,varzL,varType,'SpeedEdge');
rhoL = varInterpret(varL,varxL,varzL,varType,'DensityEdge',param);

q1R = varInterpret(varR,varxR,varzR,varType,'VelocityEdge');
p0R = varInterpret(varR,varxR,varzR,varType,'p0Edge');
T0R = varInterpret(varR,varxR,varzR,varType,'T0Edge');

qR = varInterpret(varR,varxR,varzR,varType,'SpeedEdge');
rhoR = varInterpret(varR,varxR,varzR,varType,'DensityEdge',param);

%%
Re_d_L = rhoL * qL * deltaL / param.mui_;

secondaryVarL = ThicknessCoefficient(Re_d_L, deltaL, AL);
delta1sL = secondaryVarL.delta1s;
deltaqL = secondaryVarL.deltaq;
deltarhoL = secondaryVarL.deltarho;
theta11L = secondaryVarL.theta11;
theta1sL = secondaryVarL.theta1s;

mL = rhoL * deltarhoL;
ML = rhoL * delta1sL * q1L;
pL = ML - mL * q1L;
kL = rhoL * qL^2 * deltaqL;

PL = rhoL * theta11L * ( q1L * q1L' );
KL = rhoL * qL^2 * theta1sL * q1L;

PTdoteL = PL' * e1L;


Re_d_R = rhoR * qR * deltaR / param.mui_;

secondaryVarR = ThicknessCoefficient(Re_d_R, deltaR, AR);
delta1sR = secondaryVarR.delta1s;
deltaqR = secondaryVarR.deltaq;
deltarhoR = secondaryVarR.deltarho;
theta11R = secondaryVarR.theta11;
theta1sR = secondaryVarR.theta1s;

mR = rhoR * deltarhoR;
MR = rhoR * delta1sR * q1R;
pR = MR - mR * q1R;
kR = rhoR * qR^2 * deltaqR;

PR = rhoR * theta11R * ( q1R * q1R' );
KR = rhoR * qR^2 * theta1sR * q1R;

PTdoteR = PR' * e1R;
%%
uConsL = [pL'*e1L; kL; qxL; qzL; p0L; T0L];
uConsR = [pR'*e1R; kR; qxR; qzR; p0R; T0R];

f = zeros(6,1);

alpha = max( abs(qL), abs(qR) );

f(1) = 0.5 * (PTdoteL + PTdoteR)' * Nrm + 0.5 * alpha * ( uConsL(1) - uConsR(1));
f(2) = 0.5 * (KL + KR)' * Nrm + 0.5 * alpha * ( uConsL(2) - uConsR(2));

fprintf(sprintf('fTrue = { %5.16e,\n', f(1)));
for i = 2:5
    fprintf(sprintf('          %5.16e,\n', f(i)));
end
fprintf(sprintf('          %5.16e };\n', f(6)));
  