function [S] = viscStressProfile(etab,A,Re_d)
% This function computes the viscous shear stress profile at wall
% coordinates eta with parameters A and Re_d.

% scaleFactor = 100;

if A < 0 % modify if A is negative
%     Abar = 1.1*scaleFactor*A / (1-scaleFactor*A);
    Abar = A / (1-A);
%     etab = etaq ./ (1 - A + A.*etaq);
else
    Abar = A;
%     etab = etaq;
end 

% f0 = 6*etab.^2 - 8*etab.^3 + 3*etab.^4;
df0 = 6*2*etab  - 8*3*etab.^2 + 3*4*etab.^3;
% f1 = etab - 3*etab.^2 + 3*etab.^3 - etab.^4;
df1 = 1 - 3*2*etab + 3*3*etab.^2 - 4*etab.^3;

V = 1.0; % viscosity profile
S = 1/Re_d * V * (Abar * df1 + df0) / DetaDetab(etab,A);

end