clear all; close all;

%% state vector
delta = 2.e-2;
A = -0.5;
qx = 4;  
qz = 3;  
p0 = 28.125;  
T0 = 4.6875;

var = [delta; A; qx; qz; p0; T0];

varx = [ 0, 0, 2.5, 3.6, 0, 0 ]';
varz = [ 0, 0, 5.2, 6.6, 0, 0 ]';

%% parameters
e1 = [0.6; 0.8]; % basis vector
e1x = [0.7; 0.3];
e1z = [0.5; 0.6];

if ( (norm(e1) - 1) > 100*eps )
    error('e1 is not normalized.')
else
end

param = struct;

param.gam_ = 2;
param.R_ = 2;
param.mui_ = 2E-5;
param.Pr_ = 0.715;

qxi = 1;  qzi = 0;  p0i = 28.125;  T0i = 4.6875;
EIF = [ qxi; qzi; p0i; T0i ];

param.qxi = EIF(1);
param.qzi = EIF(2);
param.p0i = EIF(3);
param.T0i = EIF(4);

%% 
source = source(e1,e1x,e1z,var,varx,varz,param);

%% print output
fprintf(sprintf('sourceTrue = { %5.16e,\n', source(1)));
for i = 2:5
    fprintf(sprintf('               %5.16e,\n', source(i)));
end
fprintf(sprintf('               %5.16e };\n', source(6)));

