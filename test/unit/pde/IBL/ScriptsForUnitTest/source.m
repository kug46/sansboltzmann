function source = source(e1,e1x,e1z,var,varx,varz,param)

%% pde parameters
qxi = param.qxi;
qzi = param.qzi;
p0i = param.p0i;
T0i = param.T0i;
mui_ = param.mui_;

%% state vectors & derived variables
varType = 'DAGVelcoityStagPT';
delta = varInterpret(var,varx,varz,varType,'delta');
A = varInterpret(var,varx,varz,varType,'A');
q1 = varInterpret(var,varx,varz,varType,'VelocityEdge');
nablasq1 = varInterpret(var,varx,varz,varType,'VelocityGradEdge');
p0 = varInterpret(var,varx,varz,varType,'p0Edge');
T0 = varInterpret(var,varx,varz,varType,'T0Edge');

q = varInterpret(var,varx,varz,varType,'SpeedEdge');
rho = varInterpret(var,varx,varz,varType,'DensityEdge',param);

Re_d = rho * q * delta / mui_;
 
%% COMPUTE DEFECT INTEGRALS AND SURFACE GRADIENTS
% secondary variables
secondaryVar = ThicknessCoefficient(Re_d, delta, A);
theta11 = secondaryVar.theta11;
delta1s = secondaryVar.delta1s;
delta1p = secondaryVar.delta1p;
Cf1 = secondaryVar.Cf1;
CD = secondaryVar.CD;

% defect integrals
P = rho * theta11 * ( q1 * q1' );
M = rho * delta1s * q1;
tauw = 0.5 * rho * q * Cf1 * q1;
Q = delta1p * q1;
D = M - rho * Q;

nablasqsq = 2 * nablasq1' * q1;

calD = rho * q^3 * CD;

Pdotnablasdote1 = P(1,1)*e1x(1) + P(1,2)*e1z(1) + P(2,1)*e1x(2) + P(2,2)*e1z(2);

% EIF
qi = sqrt(qxi^2+qzi^2);

%% assemble source terms
source = zeros(size(var));

source(1) = - Pdotnablasdote1 + M' * (nablasq1*e1) - tauw' * e1;
source(2) = D' * nablasqsq - 2 * calD;
source(3) = q1(1) - qi*e1(1);
source(4) = q1(2) - qi*e1(2);
source(5) = p0 - p0i;
source(6) = T0 - T0i;

