function [U,varargout] = TurbProf(eta,A,Re_d,q_i)
% This function computes the normalized velocity profile of turbulent flow.

vi_vw = 1; % v_i/v_w = 1 according to incompressibility assumption
delta_p = vi_vw^0.5 * Re_d^0.5 * A^0.5; % delta^+
[u_p,u_p0guess] = wallProf(eta,A,Re_d);

%% overall profile
% U_o
b = 1; % approximation
b1 = 3*(b+2)*(b+3)/(b+7);
b2 = -5*(b+1)*(b+3)/(b+7);
b3 = 2*(b+1)*(b+2)/(b+7);
g_o = (b1*eta + b2*eta.^2 + b3*eta.^3).*eta.^b;

% q_tau = q_i * (vi_vw)^0.5 * A^0.5 / Re_d^0.5;
% u_pe = q_i / q_tau; % u^+ at the edge of the viscous layer
% why doesn't this work?

eta_e = 1.0; % eta = 1 at the edge of the viscous layer
[u_pe,u_pe0guess] = wallProf(eta_e,A,Re_d);
U_ie = A / delta_p * u_pe;
Q_o = 1 - U_ie;
U_ie0_guess = A / delta_p * u_pe0guess;
Q_o0_guess = 1 - U_ie0_guess;

U_o = Q_o * g_o;
U_o0_guess = Q_o0_guess * g_o;

% U_i
U_i = A / delta_p * u_p;
U_i0_guess = A / delta_p * u_p0guess;

% U overall
U = U_i + U_o; % overall q_i-normalized velocity
U0_guess = U_i0_guess + U_o0_guess;
varargout{1} = U0_guess;

end

% % print U
% fprintf('u_p = %3.20f\n',u_p);