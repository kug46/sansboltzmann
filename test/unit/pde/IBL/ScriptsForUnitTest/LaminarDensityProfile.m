function R = viscDensityProfile(eta)
% gamma = 1.4;
% Pr = 1;
% r = Pr^0.5;
% Epsp = r * 0.5*(gamma - 1) * Mi^2; % \Epsilon'
% dHw = hw / hi - ( 1+Epsp );
% R_reciprocal = 1 + dHw.*(1-U) + Epsp.*(1-U.^2);

% incompressible
R = ones(size(eta));

end