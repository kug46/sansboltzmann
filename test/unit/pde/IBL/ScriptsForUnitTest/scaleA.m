function Abar = scaleA(A)
% compute the scaled BL shape parameter \bar{A}

if A >= 0
    Abar = A;
else
    Abar = A / (1-A);
end

end