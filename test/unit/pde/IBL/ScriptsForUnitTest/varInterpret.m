function output = varInterpret(var,varx,varz,...
                               varType,outputType,...
                               varargin)

if nargin > 5
    param = varargin{1};
end

if strcmp( varType, 'DAGVelcoityStagPT' )
    
    switch outputType
        case 'delta'
            output = var(1);
        case 'A'
            output = var(2);
        case 'VelocityEdge'
            output = var([3,4]);
        case 'VelocityGradEdge'
            output = [ varx(3), varz(3); ...
                       varx(4), varz(4) ];
        case 'p0Edge'
            output = var(5);
        case 'T0Edge'
            output = var(6);
        case 'DensityEdge'
            output = getDensityEdge(var,varType,param);
        case 'SpeedEdge'
            output = sqrt( var(3)^2 + var(4)^2 );
    end
    
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rhoe = getDensityEdge(var,varType,param)
% compute density at BL edge.
gam_ = param.gam_;
R_ = param.R_;

T0 = varInterpret(var,var,var,varType,'T0Edge');
p0 = varInterpret(var,var,var,varType,'p0Edge');
qe = varInterpret(var,var,var,varType,'SpeedEdge');

Te = T0 - ( gam_ - 1 )/( 2 * gam_ * R_ ) * qe^2;
Me = qe / sqrt( gam_ * R_ * Te );
rho0e = p0 / (R_*T0);
rhoe = rho0e * ( 1 + (gam_-1)/2 * Me^2 )^(-1/(gam_-1));

end