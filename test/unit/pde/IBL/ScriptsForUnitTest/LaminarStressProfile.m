function [S] = LaminarStressProfile(etab,A,Re_d)
% This function computes the viscous shear stress profile at wall
% coordinates eta with parameters A and Re_d.

[~,dUdetab] = LaminarVelProfile(etab,A);

V = 1.0; % viscosity profile is 1 (constant) assuming incompressibility
S = 1/Re_d * V * dUdetab ./ DetaDetab(etab,A);

end