function [U] = viscVelProfile(etab,A)
% This function computes the viscous velocity profile at wall coordinates
% eta with parameter A.

% scaleFactor = 100;

if A < 0 % modify if A is negative
%     Abar = 1.1*scaleFactor*A / (1-scaleFactor*A);
    Abar = A / (1-A);
%     etab = etaq ./ (1 - A + A.*etaq);
else
    Abar = A;
%     etab = etaq;
end 

f0 = 6*etab.^2 - 8*etab.^3 + 3*etab.^4;
f1 = etab - 3*etab.^2 + 3*etab.^3 - etab.^4;
    
U = Abar * f1 + f0;

end