function D = DetaDetab(etab,A)
% computes (d eta)/(d \bar{eta})
if A < 0
    D = (1 - A) ./ (1 - A.*etab).^2;
else
    D = ones(size(etab));
end