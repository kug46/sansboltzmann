function [R,varargout] = wallRes(u_p,eta,y_p,k,B)
% This function computes the residual in the modified Spalding law of
% turbulent velocity profile and a vanishing residual defines the wall
% profile u^+

% Input:
% u_p: u^+
% eta
% A: profile shape parameter
% Re_d: Re_delta; viscouslayer thickness Reynolds number
% Output:
% R: residual of the modified spalding law

%% test
% A = 1.0;
% Re_d = 5E+3;
% q_i = 1.0;
% eta = 0;
% u = 0;

%% parameters
n = 4; % or 3 ?

%% evaluate residual R
% vi_vw = 1; % v_i/v_w = 1 according to incompressibility assumption
% delta_p = vi_vw^0.5 * Re_d^0.5 * A^0.5; % delta^+
% y_p = eta * delta_p; % y^+

% R = (1 - (1-eta)^n) / (1+n) * y_p - u_p ...
%     - exp(-k*B)*( exp(k*u_p) - 1 - k*u_p - (k*u_p)^2/2 - (k*u_p)^3/6 );
R = (1 - eta^n / (1+n)) * y_p - u_p ...
    - exp(-k*B)*( exp(k*u_p) - 1 - k*u_p - (k*u_p)^2/2 - (k*u_p)^3/6 );

dRdu =                        - 1   ...
    - exp(-k*B)*( k*exp(k*u_p)   - k     -  k^2*u_p    -  k^3*u_p^2/2 );

varargout{1} = dRdu;
end

%% comment