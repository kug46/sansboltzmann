function [u_p,varargout] = wallProf(eta,A,Re_d,varargin)
% This functions solves the modified Spalding law and obtains the wall
% profile u^+

%% initial guess
vi_vw = 1; % v_i/v_w = 1 according to incompressibility assumption
delta_p = vi_vw^0.5 * Re_d^0.5 * A^0.5; % delta^+
y_p_vec = eta * delta_p; % y^+

% % used for SANS unit tests
% clear all;
% nargin = 4;
% varargin{1} = 'Newton';
% eta = 0.8;
% y_p_vec = 60.0;

u_p0 = zeros(size(y_p_vec));
% initial guess of u_p
k = 0.41; % kappa
B = 5.0;
guess = 1;
switch guess
    case 1
        for i = 1:length(y_p_vec)
            y_p = y_p_vec(i);
            %         fprintf('y_p = %1.4f\n',y_p)
            %         fprintf('y_p length = %d\n',length(y_p))
            u_p0(i) = wallProfInitial(y_p,k,B);
        end
        %
    case 0 % without a good initial guess
        u_p0 = zeros(size(y_p_vec));
end

%% solve residual equation
u_p = zeros(size(y_p_vec));

if nargin < 4
    whichSolve = 'fsolve';
else
    whichSolve = varargin{1};
end
% whichSolve = 'Newton';
switch whichSolve
    case 'fsolve'
        for i = 1:length(y_p_vec)
            residual = @(u)wallRes(u,eta(i),y_p_vec(i),k,B);
            options = optimoptions('fsolve','Display','none','TolFun',1E-20,'TolX',1E-20);
            u_p(i) = fsolve(residual,u_p0(i),options);
        end
    %----------------------------%
    case 'Newton'
        maxNewton = 50;
        resTol = 1E-10;
        for i = 1:length(y_p_vec)
            u_pn = u_p0(i);
            for iter = 1:maxNewton
                [Rn,dRn_du] = wallRes(u_pn,eta(i),y_p_vec(i),k,B);
                fprintf('Newton Iter # %d, residual = %1.3e, jacobian = %1.3e \n',iter,Rn,dRn_du);
                du = -Rn/dRn_du;
                u_pn = u_pn + du;
                u_p(i) = u_pn;
                if iter == 1
                    Rn_0 = Rn;
                end
                if abs(Rn_0) < 10*eps
                    resDrop = 0.1*resTol;
                else
                    resDrop = abs(Rn/Rn_0);
                end
                if resDrop < resTol
                    break;
                end
            end
        end

end

varargout{1} = u_p0;

% % print u_plus
% fprintf('u_p = %3.20f',u_p);