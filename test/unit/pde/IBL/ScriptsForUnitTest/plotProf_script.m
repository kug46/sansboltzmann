% This script plots the normalized velocity profile U(eta) for
% incompressible turbulent flows.

close all; clear all

%% variables
A = 100;
Re_d = 5E+4;
q_i = 2.0;

%% computes profile
eta_vec = linspace(0,1,11);

for i = 1:length(eta_vec)
    eta = eta_vec(i);
    [U(i),Uini(i)] = TurbProf(eta,A,Re_d,q_i);
    Itheta(i) = (1-U(i))*U(i);
    Itheta_star(i) = (1-U(i)^2)*U(i);
end

%% plot
figure(1)

% U(eta)
subplot(2,2,1)
plot(eta_vec,U,'k-+',...
     eta_vec,Uini,'r--x')
legend({'Iteratively solved','Initially guessed'},...
    'Location','Best')
xlabel('\eta')
ylabel('U')

% (d U)/(d eta)
dUdeta = diff(U)./diff(eta_vec);
dUdeta_initial = diff(Uini)./diff(eta_vec);
subplot(2,2,2)
plot(eta_vec(2:end),dUdeta,'k-+',...
    eta_vec(2:end),dUdeta_initial,'r--x')
legend({'Iteratively solved','Initially guessed'},...
    'Location','Best')
xlabel('\eta')
ylabel('dU/deta')
title('First-order FD differentiation')

% Integrand of theta
subplot(2,2,3)
plot(eta_vec,Itheta,'k-+')
xlabel('\eta')
ylabel('Itheta')

% Integrand of theta_star
subplot(2,2,4)
plot(eta_vec,Itheta_star,'k-+')
xlabel('\eta')
ylabel('Itheta-star')

% % Set one tile for subfigures
% set(gcf,'NextPlot','add');
% axes;
% h = title(sprintf('A = %5.1f, Re_{\\delta} = %.1e',A,Re_d));
% set(gca,'Visible','off');
% set(h,'Visible','on');

%% save figure
% figname = sprintf('profile_A%dp%d_Re%d.eps',A-mod(A,1),10*mod(A,1)-mod(10*mod(A,1),1),Re_d);
% print('-depsc2',figname);
% unix(sprintf('epstopdf %s', figname));
% delete(figname); % delete eps files