// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <type_traits>

#include "pde/IBL/VarInterpret2D.h"

namespace SANS
{
template<int N>
DLA::VectorS<N, double> fabs(const DLA::VectorS<N, double>& x)
{
  SANS::DLA::VectorS<N, double> y = x;
  for (int i = 0; i < N; ++i)
    y[i] = std::fabs(y[i]);
  return y;
}

}
using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( VarInterp2D_VarTypeDANCt_test_suite )

typedef VarData2DDANCt VarDataType;
typedef VarInterpret2D<VarTypeDANCt> VarInterpType;
typedef VarInterpType::ArrayQ<Real> ArrayQ;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef IBLTraits<PhysD2, IBLTwoField> IBLTraitsType;
  BOOST_CHECK( IBLTraitsType::D == 2 );
  BOOST_CHECK( IBLTraitsType::N == 6 );
  BOOST_CHECK( IBLTraitsType::Nparam == 8 );

  BOOST_CHECK( (std::is_same<VarInterpType::IBLTraitsType, IBLTraitsType>::value) );

  BOOST_CHECK( VarInterpType::D == IBLTraitsType::D );
  BOOST_CHECK( VarInterpType::N == IBLTraitsType::N );

  BOOST_CHECK( VarInterpType::ideltaLami == 0 );
  BOOST_CHECK( VarInterpType::iALami == 1 );
  BOOST_CHECK( VarInterpType::ideltaTurb == 2 );
  BOOST_CHECK( VarInterpType::iATurb == 3 );
  BOOST_CHECK( VarInterpType::i_nt == 4 );
  BOOST_CHECK( VarInterpType::ictau == 5 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setDOFFrom_get_dummySource_test )
{
  VarInterpType varInterpret;

  const Real deltaLami = 2.0e-2, ALami = -0.5, deltaTurb = 1.0e-2, ATurb = 1.0, nt = 1.3, ctau = 1e-3;

  VarDataType vardata(deltaLami, ALami, deltaTurb, ATurb, nt, ctau); // constructor
  vardata = {deltaLami,ALami,deltaTurb,ATurb,nt,ctau}; // overload "="

  const ArrayQ var = varInterpret.setDOFFrom<Real>(vardata);

  BOOST_CHECK_EQUAL(deltaLami, varInterpret.getdeltaLami(var));
  BOOST_CHECK_EQUAL(ALami, varInterpret.getALami(var));
  BOOST_CHECK_EQUAL(deltaTurb, varInterpret.getdeltaTurb(var));
  BOOST_CHECK_EQUAL(ATurb, varInterpret.getATurb(var));

  BOOST_CHECK_EQUAL(deltaLami, varInterpret.getdelta(IBLProfileCategory::LaminarBL, var));
  BOOST_CHECK_EQUAL(deltaLami, varInterpret.getdelta(IBLProfileCategory::LaminarWake, var));
  BOOST_CHECK_EQUAL(deltaTurb, varInterpret.getdelta(IBLProfileCategory::TurbulentBL, var));
  BOOST_CHECK_EQUAL(deltaTurb, varInterpret.getdelta(IBLProfileCategory::TurbulentWake, var));

  BOOST_CHECK_EQUAL(ALami, varInterpret.getA(IBLProfileCategory::LaminarBL, var));
  BOOST_CHECK_EQUAL(ALami, varInterpret.getA(IBLProfileCategory::LaminarWake, var));
  BOOST_CHECK_EQUAL(ATurb, varInterpret.getA(IBLProfileCategory::TurbulentBL, var));
  BOOST_CHECK_EQUAL(ATurb, varInterpret.getA(IBLProfileCategory::TurbulentWake, var));

  BOOST_CHECK_THROW( varInterpret.getdelta(IBLProfileCategory(4), var);, SANSException );
  BOOST_CHECK_THROW( varInterpret.getA(IBLProfileCategory(4), var);, SANSException );

  BOOST_CHECK_EQUAL(nt, varInterpret.getnt(var));
  BOOST_CHECK_EQUAL(ctau, varInterpret.getctau(var));
  BOOST_CHECK_EQUAL(log(ctau), varInterpret.getG(var));

  BOOST_CHECK_EQUAL(deltaLami-varInterpret.deltaLami_dummy_, varInterpret.dummySource_momLami(var));
  BOOST_CHECK_EQUAL(ALami-varInterpret.ALami_dummy_, varInterpret.dummySource_keLami(var));
  BOOST_CHECK_EQUAL(deltaTurb-varInterpret.deltaTurb_dummy_, varInterpret.dummySource_momTurb(var));
  BOOST_CHECK_EQUAL(ATurb-varInterpret.ATurb_dummy_, varInterpret.dummySource_keTurb(var));
  BOOST_CHECK_EQUAL(nt-varInterpret.nt_dummy_, varInterpret.dummySource_amp(var));
  BOOST_CHECK_EQUAL(ctau-varInterpret.ctauLami_dummy_, varInterpret.dummySource_lag(var));
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState_test )
{
  VarInterpType varInterpret;

  // verify valid state
  {
    const Real deltaLami = 1.0, ALami = 1.0, deltaTurb = 1.0, ATurb = 1.0, nt = 0.1, ctau = 1e-3;
    BOOST_CHECK( varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau))) );
  }

  // catch invalid delta alone
  {
    const Real deltaLami = -1.0, ALami = 1.0, deltaTurb = 1.0, ATurb = 1.0, nt = 0.1, ctau = 1e-3;
    BOOST_CHECK( !varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau))) );
  }

  {
    const Real deltaLami = 0.0, ALami = 1.0, deltaTurb = 1.0, ATurb = 1.0, nt = 0.1, ctau = 1e-3;
    BOOST_CHECK( !varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau))) );
  }

  {
    const Real deltaLami = 1.0, ALami = 1.0, deltaTurb = -1.0, ATurb = 1.0, nt = 0.1, ctau = 1e-3;
    BOOST_CHECK( !varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau))) );
  }

  {
    const Real deltaLami = 1.0, ALami = 1.0, deltaTurb = 0.0, ATurb = 1.0, nt = 0.1, ctau = 1e-3;
    BOOST_CHECK( !varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau))) );
  }

  // catch invalid ctau alone
  {
    const Real deltaLami = 1.0, ALami = 1.0, deltaTurb = 1.0, ATurb = 1.0, nt = 0.1, ctau = -1e-3;
    BOOST_CHECK( !varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau))) );
  }

  {
    const Real deltaLami = 1.0, ALami = 1.0, deltaTurb = 1.0, ATurb = 1.0, nt = 0.1, ctau = 0.0;
    BOOST_CHECK( !varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau))) );
  }

  // catch invalid delta and ctau
  {
    const Real deltaLami = -1.0, ALami = 1.0, deltaTurb = -1.0, ATurb = 1.0, nt = 0.1, ctau = -1e-3;
    BOOST_CHECK( !varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau))) );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( updateFraction_LaminarBL_test )
{
  const Real small_tol = 1e-15, close_tol = 1.3e-14;

  const VarInterpType varInterpret;

  const IBLProfileCategory profileCat = IBLProfileCategory::LaminarBL;

  const Real deltaLami = 2.0e-2, ALami = -0.5, deltaTurb = 1.0e-2, ATurb = 1.0, nt = 1.3, ctau = 1e-3;
  const ArrayQ var = varInterpret.setDOFFrom<Real>(VarDataType(deltaLami, ALami, deltaTurb, ATurb, nt, ctau));
  const Real maxChangeFraction = 1.0;

  const Real dnvar_max = 2.0; // should be > 0
  const Real dnvar_min = -0.9; // should be  < 0

  {
    const ArrayQ dvar = -(dnvar_max*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_max*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 0.5;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(0.5, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 0.5;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(0.5, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_max*1.1)*fabs(var); // increase too much
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*1.1)*fabs(var); // decrease too much
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }

  {
    ArrayQ dvar = 0.0;
    dvar[VarInterpType::i_nt] = -(dnvar_max*1.1)*10.0; // nt increases too much

    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }

  {
    ArrayQ dvar = 0.0;
    dvar[VarInterpType::i_nt] = -(dnvar_min*1.1)*10.0; // nt decreases too much

    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }
}

BOOST_AUTO_TEST_CASE( updateFraction_TurbulentBL_test )
{
  const Real small_tol = 1e-15, close_tol = 1.3e-14;

  const VarInterpType varInterpret;

  const IBLProfileCategory profileCat = IBLProfileCategory::TurbulentBL;

  const Real deltaLami = 2.0e-2, ALami = -0.5, deltaTurb = 1.0e-2, ATurb = 1.0, nt = 1.3, ctau = 1e-3;
  const ArrayQ var = varInterpret.setDOFFrom<Real>(VarDataType(deltaLami, ALami, deltaTurb, ATurb, nt, ctau));
  const Real maxChangeFraction = 1.0;

  const Real dnvar_max = 2.0; // should be > 0
  const Real dnvar_min = -0.9; // should be  < 0

  {
    const ArrayQ dvar = -(dnvar_max*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_max*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 0.5;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(0.5, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 0.5;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(0.5, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_max*1.1)*fabs(var); // increase too much
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*1.1)*fabs(var); // decrease too much
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }

  {
    ArrayQ dvar = 0.0;
    dvar[VarInterpType::i_nt] = -(dnvar_max*1.1)*10.0; // nt increases too much

    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }

  {
    ArrayQ dvar = 0.0;
    dvar[VarInterpType::i_nt] = -(dnvar_min*1.1)*10.0; // nt decreases too much

    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }
}

BOOST_AUTO_TEST_CASE( updateFraction_TurbulentWake_test )
{
  const Real small_tol = 1e-15, close_tol = 1.3e-14;

  const VarInterpType varInterpret;

  const IBLProfileCategory profileCat = IBLProfileCategory::TurbulentWake;

  const Real deltaLami = 2.0e-2, ALami = -0.5, deltaTurb = 1.0e-2, ATurb = 1.0, nt = 1.3, ctau = 1e-3;
  const ArrayQ var = varInterpret.setDOFFrom<Real>(VarDataType(deltaLami, ALami, deltaTurb, ATurb, nt, ctau));
  const Real maxChangeFraction = 1.0;

  const Real dnvar_max = 2.0; // should be > 0
  const Real dnvar_min = -0.9; // should be  < 0

  {
    const ArrayQ dvar = -(dnvar_max*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_max*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 0.5;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(0.5, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 0.5;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(0.5, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_max*1.1)*fabs(var); // increase too much
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*1.1)*fabs(var); // decrease too much
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }

  {
    ArrayQ dvar = 0.0;
    dvar[VarInterpType::i_nt] = -(dnvar_max*1.1)*10.0; // nt change does not matter

    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0, updateFraction, small_tol, close_tol);
  }

  {
    ArrayQ dvar = 0.0;
    dvar[VarInterpType::i_nt] = -(dnvar_min*1.1)*10.0; // nt change does not matter

    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0, updateFraction, small_tol, close_tol);
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()


//############################################################################//
BOOST_AUTO_TEST_SUITE( VarInterp2D_VarTypeDAG_test_suite )

typedef VarData2DDAG VarDataType;
typedef VarInterpret2D<VarTypeDAG> VarInterpType;
typedef VarInterpType::ArrayQ<Real> ArrayQ;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef IBLTraits<PhysD2, IBLUniField> IBLTraitsType;
  BOOST_CHECK( IBLTraitsType::D == 2 );
  BOOST_CHECK( IBLTraitsType::N == 3 );
  BOOST_CHECK( IBLTraitsType::Nparam == 8 );

  BOOST_CHECK( (std::is_same<VarInterpType::IBLTraitsType, IBLTraitsType>::value) );

  BOOST_CHECK( VarInterpType::D == IBLTraitsType::D );
  BOOST_CHECK( VarInterpType::N == IBLTraitsType::N );

  BOOST_CHECK( VarInterpType::idelta == 0 );
  BOOST_CHECK( VarInterpType::iA == 1 );
  BOOST_CHECK( VarInterpType::iG == 2 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( get_setDOFFrom_updateFraction_test )
{
  VarInterpType varInterpret;

  const Real delta = 2.0e-2, A = -0.5, G = -1.6;

  VarDataType vardata(delta, A, G); // constructor
  vardata = {delta, A, G}; // overload "="

  const ArrayQ var = varInterpret.setDOFFrom<Real>(vardata);

  BOOST_CHECK_EQUAL(delta, varInterpret.getdelta(var));
  BOOST_CHECK_EQUAL(A, varInterpret.getA(var));

  BOOST_CHECK_EQUAL(delta, varInterpret.getdelta(IBLProfileCategory::LaminarBL, var));
  BOOST_CHECK_EQUAL(delta, varInterpret.getdelta(IBLProfileCategory::LaminarWake, var));
  BOOST_CHECK_EQUAL(delta, varInterpret.getdelta(IBLProfileCategory::TurbulentBL, var));
  BOOST_CHECK_EQUAL(delta, varInterpret.getdelta(IBLProfileCategory::TurbulentWake, var));

  BOOST_CHECK_EQUAL(A, varInterpret.getA(IBLProfileCategory::LaminarBL, var));
  BOOST_CHECK_EQUAL(A, varInterpret.getA(IBLProfileCategory::LaminarWake, var));
  BOOST_CHECK_EQUAL(A, varInterpret.getA(IBLProfileCategory::TurbulentBL, var));
  BOOST_CHECK_EQUAL(A, varInterpret.getA(IBLProfileCategory::TurbulentWake, var));


  BOOST_CHECK_EQUAL(G, varInterpret.getG(var));
  BOOST_CHECK_EQUAL(exp(2.0*G), varInterpret.getctau(var));

  const Real ctau = 0.001;
  BOOST_CHECK_EQUAL(log(pow(ctau,0.5)), varInterpret.getG_from_ctau(ctau));

  Real updateFraction = 1.0;
  const ArrayQ dvar = 0.2*fabs(var);
  BOOST_CHECK_THROW(
      varInterpret.updateFraction(IBLProfileCategory::LaminarBL, var, dvar, 1.0, updateFraction);,
      DeveloperException); // not implemented error
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState_test )
{
  VarInterpType varInterpret;

  // verify valid state
  {
    const Real delta = 2.0e-2, A = -0.5, G = -6.6;
    BOOST_CHECK( varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(delta, A, G))) );
  }

  // check invalid delta alone
  {
    const Real delta = 0.0, A = -0.5, G = -6.6;
    BOOST_CHECK( !varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(delta, A, G))) );
  }

  {
    const Real delta = -1.0, A = -0.5, G = -6.6;
    BOOST_CHECK( !varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(delta, A, G))) );
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()


//############################################################################//
BOOST_AUTO_TEST_SUITE( VarInterp2D_VarTypeDsThG_test_suite )

typedef VarData2DDsThG VarDataType;
typedef VarInterpret2D<VarTypeDsThG> VarInterpType;
typedef VarInterpType::ArrayQ<Real> ArrayQ;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef IBLTraits<PhysD2, IBLUniField> IBLTraitsType;
  BOOST_CHECK( IBLTraitsType::D == 2 );
  BOOST_CHECK( IBLTraitsType::N == 3 );
  BOOST_CHECK( IBLTraitsType::Nparam == 8 );

  BOOST_CHECK( (std::is_same<VarInterpType::IBLTraitsType, IBLTraitsType>::value) );

  BOOST_CHECK( VarInterpType::D == IBLTraitsType::D );
  BOOST_CHECK( VarInterpType::N == IBLTraitsType::N );

  BOOST_CHECK( VarInterpType::ideltas == 0 );
  BOOST_CHECK( VarInterpType::itheta == 1 );
  BOOST_CHECK( VarInterpType::iG == 2 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( get_setDOFFrom_test )
{
  VarInterpType varInterpret;

  const Real deltas = 2.0e-2, theta = 5.e-3, G = -1.6;

  VarDataType vardata(deltas, theta, G); // constructor
  vardata = {deltas, theta, G}; // overload "="

  const ArrayQ var = varInterpret.setDOFFrom<Real>(vardata);

  BOOST_CHECK_EQUAL(deltas, varInterpret.getdelta1s(var));
  BOOST_CHECK_EQUAL(theta, varInterpret.gettheta11(var));
  BOOST_CHECK_EQUAL(G, varInterpret.getG(var));
  BOOST_CHECK_EQUAL(exp(2.0*G), varInterpret.getctau(var));

  const Real ctau = 0.001;
  BOOST_CHECK_EQUAL(log(pow(ctau,0.5)), varInterpret.getG_from_ctau(ctau));

  BOOST_CHECK_EQUAL(deltas/theta, varInterpret.getH(var));
}

////----------------------------------------------------------------------------//
//BOOST_AUTO_TEST_CASE( isValidState_test )
//{
//  VarInterpType varInterpret;
//
//  // verify valid state
//  {
//    const Real deltas = 2.0e-2, theta = 5.e-3, G = -1.6;
//    BOOST_CHECK( varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(deltas, theta, G))) );
//  }
//
//  // check invalid H
//  {
//    const Real deltas = 4.e-3, theta = 5.e-3, G = -1.6;
//    BOOST_CHECK( !varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(deltas, theta, G))) );
//  }
//
//  {
//    const Real deltas = -2.0e-2, theta = 5.e-3, G = -1.6;
//    BOOST_CHECK( !varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(deltas, theta, G))) );
//  }
//
//  {
//    const Real deltas = 2.0e-2, theta = -5.e-3, G = -1.6;
//    BOOST_CHECK( !varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(deltas, theta, G))) );
//  }
//
//  {
//    const Real deltas = -2.0e-2, theta = -5.e-3, G = -1.6;
//    BOOST_CHECK( !varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(deltas, theta, G))) );
//  }
//}

//----------------------------------------------------------------------------//
// TODO: more elaborate updateFraction tests to be added
BOOST_AUTO_TEST_CASE( updateFraction_LaminarBL_test )
{
  const Real small_tol = 1e-15, close_tol = 1.3e-14;

  const VarInterpType varInterpret;

  const IBLProfileCategory profileCat = IBLProfileCategory::LaminarBL;

  const Real deltas = 2.0e-2, theta = 5.e-3, G = -1.6;
  const ArrayQ var = varInterpret.setDOFFrom<Real>(VarDataType(deltas, theta, G));
  const Real maxChangeFraction = 1.0;

  const Real dnvar_max = 1.5; // should be > 0
  const Real dnvar_min = -0.5; // should be  < 0

  {
    const ArrayQ dvar = -(dnvar_max*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_max*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 0.5;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(0.5, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 0.5;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(0.5, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_max*1.1)*fabs(var); // increase too much
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*1.1)*fabs(var); // decrease too much
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }
}

BOOST_AUTO_TEST_CASE( updateFraction_TurbulentBL_test )
{
  const Real small_tol = 1e-15, close_tol = 1.3e-14;

  const VarInterpType varInterpret;

  const IBLProfileCategory profileCat = IBLProfileCategory::TurbulentBL;

  const Real deltas = 2.0e-2, theta = 5.e-3, G = -1.6;
  const ArrayQ var = varInterpret.setDOFFrom<Real>(VarDataType(deltas, theta, G));
  const Real maxChangeFraction = 1.0;

  const Real dnvar_max = 1.5; // should be > 0
  const Real dnvar_min = -0.5; // should be  < 0

  {
    const ArrayQ dvar = -(dnvar_max*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_max*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 0.5;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(0.5, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 0.5;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(0.5, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_max*1.1)*fabs(var); // increase too much
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*1.1)*fabs(var); // decrease too much
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()

//############################################################################//
BOOST_AUTO_TEST_SUITE( VarInterp2D_VarTypeDsThNCt_test_suite )

typedef VarData2DDsThNCt VarDataType;
typedef VarInterpret2D<VarTypeDsThNCt> VarInterpType;
typedef VarInterpType::ArrayQ<Real> ArrayQ;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef IBLTraits<PhysD2, IBLTwoField> IBLTraitsType;
  BOOST_CHECK( IBLTraitsType::D == 2 );
  BOOST_CHECK( IBLTraitsType::N == 6 );
  BOOST_CHECK( IBLTraitsType::Nparam == 8 );

  BOOST_CHECK( (std::is_same<VarInterpType::IBLTraitsType, IBLTraitsType>::value) );

  BOOST_CHECK( VarInterpType::D == IBLTraitsType::D );
  BOOST_CHECK( VarInterpType::N == IBLTraitsType::N );

  BOOST_CHECK( VarInterpType::ideltasLami == 0 );
  BOOST_CHECK( VarInterpType::ithetaLami == 1 );
  BOOST_CHECK( VarInterpType::ideltasTurb == 2 );
  BOOST_CHECK( VarInterpType::ithetaTurb == 3 );
  BOOST_CHECK( VarInterpType::i_nt == 4 );
  BOOST_CHECK( VarInterpType::ictau == 5 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( get_setDOFFrom_dummySource_test )
{
  VarInterpType varInterpret;

  const Real deltasLami = 2.0e-2, thetaLami = -0.5, deltasTurb = 1.0e-2, thetaTurb = 1.0, nt = 1.3, ctau = 1e-3;

  VarDataType vardata(deltasLami, thetaLami, deltasTurb, thetaTurb, nt, ctau); // constructor
  vardata = {deltasLami,thetaLami,deltasTurb,thetaTurb,nt,ctau}; // overload "="

  const ArrayQ var = varInterpret.setDOFFrom<Real>(vardata);

  BOOST_CHECK_EQUAL(deltasLami, varInterpret.getdelta1sLami(var));
  BOOST_CHECK_EQUAL(thetaLami, varInterpret.gettheta11Lami(var));
  BOOST_CHECK_EQUAL(deltasTurb, varInterpret.getdelta1sTurb(var));
  BOOST_CHECK_EQUAL(thetaTurb, varInterpret.gettheta11Turb(var));

  BOOST_CHECK_EQUAL(deltasLami, varInterpret.getdelta1s(IBLProfileCategory::LaminarBL, var));
  BOOST_CHECK_EQUAL(deltasLami, varInterpret.getdelta1s(IBLProfileCategory::LaminarWake, var));
  BOOST_CHECK_EQUAL(deltasTurb, varInterpret.getdelta1s(IBLProfileCategory::TurbulentBL, var));
  BOOST_CHECK_EQUAL(deltasTurb, varInterpret.getdelta1s(IBLProfileCategory::TurbulentWake, var));

  BOOST_CHECK_EQUAL(thetaLami, varInterpret.gettheta11(IBLProfileCategory::LaminarBL, var));
  BOOST_CHECK_EQUAL(thetaLami, varInterpret.gettheta11(IBLProfileCategory::LaminarWake, var));
  BOOST_CHECK_EQUAL(thetaTurb, varInterpret.gettheta11(IBLProfileCategory::TurbulentBL, var));
  BOOST_CHECK_EQUAL(thetaTurb, varInterpret.gettheta11(IBLProfileCategory::TurbulentWake, var));

  BOOST_CHECK_THROW( varInterpret.getdelta1s(IBLProfileCategory(4), var);, SANSException );
  BOOST_CHECK_THROW( varInterpret.gettheta11(IBLProfileCategory(4), var);, SANSException );

  BOOST_CHECK_EQUAL(nt, varInterpret.getnt(var));
  BOOST_CHECK_EQUAL(ctau, varInterpret.getctau(var));

  BOOST_CHECK_EQUAL(deltasLami-varInterpret.delta1sLami_dummy_, varInterpret.dummySource_momLami(var));
  BOOST_CHECK_EQUAL(thetaLami-varInterpret.theta11Lami_dummy_, varInterpret.dummySource_keLami(var));
  BOOST_CHECK_EQUAL(deltasTurb-varInterpret.delta1sTurb_dummy_, varInterpret.dummySource_momTurb(var));
  BOOST_CHECK_EQUAL(thetaTurb-varInterpret.theta11Turb_dummy_, varInterpret.dummySource_keTurb(var));
  BOOST_CHECK_EQUAL(nt-varInterpret.nt_dummy_, varInterpret.dummySource_amp(var));
  BOOST_CHECK_EQUAL(ctau-varInterpret.ctauLami_dummy_, varInterpret.dummySource_lag(var));
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState_test )
{
  VarInterpType varInterpret;

  // verify valid state
  {
    const Real deltasLami = 0.2, thetaLami = 0.1, deltasTurb = 0.4, thetaTurb = 0.2, nt = 1.3, ctau = 1e-3;
    BOOST_CHECK( varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(deltasLami,thetaLami,deltasTurb,thetaTurb,nt,ctau))) );
  }

  // invalid lami thicknesses
  {
    const Real deltasLami = 0.2, thetaLami = 0.2, deltasTurb = 0.4, thetaTurb = 0.2, nt = 1.3, ctau = 1e-3;
    BOOST_CHECK( !varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(deltasLami,thetaLami,deltasTurb,thetaTurb,nt,ctau))) );
  }

  // invalid turb thicknesses
  {
    const Real deltasLami = 0.2, thetaLami = 0.1, deltasTurb = 0.4, thetaTurb = 0.4, nt = 1.3, ctau = 1e-3;
    BOOST_CHECK( !varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(deltasLami,thetaLami,deltasTurb,thetaTurb,nt,ctau))) );
  }

  // invalid ctau
  {
    const Real deltasLami = 0.2, thetaLami = 0.1, deltasTurb = 0.4, thetaTurb = 0.2, nt = 1.3, ctau = -1e-3;
    BOOST_CHECK( !varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(deltasLami,thetaLami,deltasTurb,thetaTurb,nt,ctau))) );
  }

  // invalid many things...
  {
    const Real deltasLami = 0.2, thetaLami = 0.3, deltasTurb = 0.4, thetaTurb = -1.0, nt = 1.3, ctau = -1e-3;
    BOOST_CHECK( !varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(deltasLami,thetaLami,deltasTurb,thetaTurb,nt,ctau))) );
  }
}

//----------------------------------------------------------------------------//
// TODO: more elaborate updateFraction tests to be added
BOOST_AUTO_TEST_CASE( updateFraction_LaminarBL_test )
{
  const Real small_tol = 1e-15, close_tol = 1.3e-14;

  const VarInterpType varInterpret;

  const IBLProfileCategory profileCat = IBLProfileCategory::LaminarBL;

  const Real deltasLami = 2.0e-2, thetaLami = -0.5, deltasTurb = 1.0e-2, thetaTurb = 1.0, nt = 1.3, ctau = 1e-3;
  const ArrayQ var = varInterpret.setDOFFrom<Real>(VarDataType(deltasLami, thetaLami, deltasTurb, thetaTurb, nt, ctau));
  const Real maxChangeFraction = 1.0;

  const Real dnvar_max = 1.5; // should be > 0
  const Real dnvar_min = -0.5; // should be  < 0

  {
    const ArrayQ dvar = -(dnvar_max*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_max*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 0.5;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(0.5, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 0.5;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(0.5, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_max*1.1)*fabs(var); // increase too much
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*1.1)*fabs(var); // decrease too much
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }

  {
    ArrayQ dvar = 0.0;
    dvar[VarInterpType::i_nt] = -(dnvar_max*1.1)*10.0; // nt increases too much

    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }

  {
    ArrayQ dvar = 0.0;
    dvar[VarInterpType::i_nt] = -(dnvar_min*1.1)*10.0; // nt decreases too much

    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }
}

BOOST_AUTO_TEST_CASE( updateFraction_TurbulentBL_test )
{
  const Real small_tol = 1e-15, close_tol = 1.3e-14;

  const VarInterpType varInterpret;

  const IBLProfileCategory profileCat = IBLProfileCategory::TurbulentBL;

  const Real deltasLami = 2.0e-2, thetaLami = -0.5, deltasTurb = 1.0e-2, thetaTurb = 1.0, nt = 1.3, ctau = 1e-3;
  const ArrayQ var = varInterpret.setDOFFrom<Real>(VarDataType(deltasLami, thetaLami, deltasTurb, thetaTurb, nt, ctau));
  const Real maxChangeFraction = 1.0;

  const Real dnvar_max = 1.5; // should be > 0
  const Real dnvar_min = -0.5; // should be  < 0

  {
    const ArrayQ dvar = -(dnvar_max*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_max*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 0.5;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(0.5, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 0.5;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(0.5, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_max*1.1)*fabs(var); // increase too much
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*1.1)*fabs(var); // decrease too much
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }

  {
    ArrayQ dvar = 0.0;
    dvar[VarInterpType::i_nt] = -(dnvar_max*1.1)*10.0; // nt increases too much

    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }

  {
    ArrayQ dvar = 0.0;
    dvar[VarInterpType::i_nt] = -(dnvar_min*1.1)*10.0; // nt decreases too much

    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }
}

BOOST_AUTO_TEST_CASE( updateFraction_TurbulentWake_test )
{
  const Real small_tol = 1e-15, close_tol = 1.3e-14;

  const VarInterpType varInterpret;

  const IBLProfileCategory profileCat = IBLProfileCategory::TurbulentWake;

  const Real deltasLami = 2.0e-2, thetaLami = -0.5, deltasTurb = 1.0e-2, thetaTurb = 1.0, nt = 1.3, ctau = 1e-3;
  const ArrayQ var = varInterpret.setDOFFrom<Real>(VarDataType(deltasLami, thetaLami, deltasTurb, thetaTurb, nt, ctau));
  const Real maxChangeFraction = 1.0;

  const Real dnvar_max = 1.5; // should be > 0
  const Real dnvar_min = -0.5; // should be  < 0

  {
    const ArrayQ dvar = -(dnvar_max*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_max*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 0.5;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(0.5, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*0.9)*fabs(var); // no under-relaxation required
    Real updateFraction = 0.5;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(0.5, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_max*1.1)*fabs(var); // increase too much
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -(dnvar_min*1.1)*fabs(var); // decrease too much
    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0/1.1, updateFraction, small_tol, close_tol);
  }

  {
    ArrayQ dvar = 0.0;
    dvar[VarInterpType::i_nt] = -(dnvar_max*1.1)*10.0; // nt change does not matter

    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0, updateFraction, small_tol, close_tol);
  }

  {
    ArrayQ dvar = 0.0;
    dvar[VarInterpType::i_nt] = -(dnvar_min*1.1)*10.0; // nt change does not matter

    Real updateFraction = 2.0;
    varInterpret.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0, updateFraction, small_tol, close_tol);
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()

//############################################################################//
BOOST_AUTO_TEST_SUITE( VarInterp2D_VarTypeDsThNGsplit_test_suite )

typedef VarData2DDsThNGsplit VarDataType;
typedef VarInterpret2D<VarTypeDsThNGsplit> VarInterpType;
typedef VarInterpType::ArrayQ<Real> ArrayQ;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef IBLTraits<PhysD2, IBLSplitAmpLagField> IBLTraitsType;
  BOOST_CHECK( IBLTraitsType::D == 2 );
  BOOST_CHECK( IBLTraitsType::N == 4 );
  BOOST_CHECK( IBLTraitsType::Nparam == 8 );

  BOOST_CHECK( (std::is_same<VarInterpType::IBLTraitsType, IBLTraitsType>::value) );

  BOOST_CHECK( VarInterpType::D == IBLTraitsType::D );
  BOOST_CHECK( VarInterpType::N == IBLTraitsType::N );

  BOOST_CHECK( VarInterpType::ideltas == 0 );
  BOOST_CHECK( VarInterpType::itheta == 1 );
  BOOST_CHECK( VarInterpType::i_nt == 2 );
  BOOST_CHECK( VarInterpType::iG == 3 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setDOFFrom_get_dummySource_test )
{
  VarInterpType varInterpret;

  const Real delta1s = 2e-2, theta11 = 5e-3, nt = 1.3, G = -5.0;

  VarDataType vardata(delta1s,theta11,nt,G); // constructor
  vardata = {delta1s,theta11,nt,G}; // overload "="

  const ArrayQ var = varInterpret.setDOFFrom<Real>(vardata);

  BOOST_CHECK_EQUAL(delta1s, varInterpret.getdelta1s(var));
  BOOST_CHECK_EQUAL(theta11, varInterpret.gettheta11(var));
  BOOST_CHECK_EQUAL(nt, varInterpret.getnt(var));
  BOOST_CHECK_EQUAL(G, varInterpret.getG(var));

  BOOST_CHECK_EQUAL(delta1s, varInterpret.getdelta1s(IBLProfileCategory::LaminarBL, var));
  BOOST_CHECK_EQUAL(theta11, varInterpret.gettheta11(IBLProfileCategory::LaminarBL, var));

  BOOST_CHECK_EQUAL(delta1s, varInterpret.getdelta1s(IBLProfileCategory::LaminarWake, var));
  BOOST_CHECK_EQUAL(theta11, varInterpret.gettheta11(IBLProfileCategory::LaminarWake, var));

  BOOST_CHECK_EQUAL(delta1s, varInterpret.getdelta1s(IBLProfileCategory::TurbulentBL, var));
  BOOST_CHECK_EQUAL(theta11, varInterpret.gettheta11(IBLProfileCategory::TurbulentBL, var));

  BOOST_CHECK_EQUAL(delta1s, varInterpret.getdelta1s(IBLProfileCategory::TurbulentWake, var));
  BOOST_CHECK_EQUAL(theta11, varInterpret.gettheta11(IBLProfileCategory::TurbulentWake, var));

  BOOST_CHECK_EQUAL(exp(2.0*G), varInterpret.getctau(var));
  BOOST_CHECK_EQUAL(G, varInterpret.getG_from_ctau(varInterpret.getctau(var)));

  BOOST_CHECK_EQUAL(delta1s/theta11, varInterpret.getH(var));
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState_test )
{
  VarInterpType varInterpret;

  // verify valid state
  {
    const Real delta1s = 2e-2, theta11 = 5e-3, nt = 1.3, G = -5.0;
    BOOST_CHECK( varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(delta1s,theta11,nt,G))) );
  }

  {
    const Real delta1s = -2e-2, theta11 = 5e-3, nt = 1.3, G = -5.0;
    BOOST_CHECK( !varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(delta1s,theta11,nt,G))) );
  }

  {
    const Real delta1s = 2e-2, theta11 = -5e-3, nt = 1.3, G = -5.0;
    BOOST_CHECK( !varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(delta1s,theta11,nt,G))) );
  }

  {
    const Real delta1s = -2e-2, theta11 = -5e-3, nt = 1.3, G = -5.0;
    BOOST_CHECK( !varInterpret.isValidState(varInterpret.setDOFFrom<Real>(VarDataType(delta1s,theta11,nt,G))) );
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
