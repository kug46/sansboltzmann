// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// testing 2D incompressible secondary variable class

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "SANS_btest.h"

#include "pde/IBL/ThicknessesCoefficients2D.h"

using namespace std;

namespace SANS
{
// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct

Real HSL(const Real& Hk)
{
  Real Hs = 0.0;
  const Real tmp = Hk - 4.35;
  if (Hk < 4.35)
    Hs = 0.0111*pow(tmp,2.0)/(Hk+1.0) - 0.0278*pow(tmp,3.0)/(Hk+1.0) + 1.528 - 0.0002*pow(tmp*Hk,2.0);
  else
    Hs = 0.015*pow(tmp,2.0)/Hk + 1.528;
  return Hs;
}

Real RetCFL(const Real& Hk)
{
  Real RetCf = 0.0;
  if (Hk < 5.5)
  {
    const Real tmp = pow(5.5-Hk,3.0)/(Hk+1.0);
    RetCf =0.0727*tmp - 0.07;
  }
  else
  {
    const Real tmp = 1.0 - 1.0/(Hk-4.5);
    RetCf = 0.015*pow(tmp,2.0) - 0.07;
  }
  return RetCf;
}

Real RetDIL(const Real& Hk, const Real& Hs)
{
  Real RetCDiss = 0.0;
  if (Hk < 4.0)
    RetCDiss =0.00205*pow(4.0-Hk,5.5) + 0.207;
  else
  {
    const Real Hkb = Hk - 4.0;
    const Real Den = 1.0 + 0.02*pow(Hkb,2.0);
    RetCDiss = -0.0016*pow(Hkb,2.0)/Den + 0.207;
  }

  RetCDiss *= 0.5*Hs;
  return RetCDiss;
}

Real HST(const Real& Hk, const Real& Ret)
{
  Real Hs = 0.0;

  const Real Hsmin = 1.500;
  const Real DHSINF = 0.015;

  Real Ho = 0.0;
  if (Ret > 400.0)
    Ho = 3.0 + 400/Ret;
  else
    Ho = 4.0;

  Real Retz = 0.0;
  if (Ret > 200.0)
    Retz = Ret;
  else
    Retz = 200.0;

  if (Hk < Ho)
  {
    const Real Hr = (Ho-Hk)/(Ho-1.0);

    Hs = (2.0-Hsmin-4.0/Retz)*pow(Hr,2.0) * 1.5/(Hk+0.5) + Hsmin + 4.0/Retz;
  }
  else
  {
    const Real GRT = log(Retz);
    const Real Hdif = Hk-Ho;
    const Real Rtmp = Hk-Ho+4.0/GRT;
    const Real Htmp = 0.007*GRT/pow(Rtmp,2.0) + DHSINF/Hk;
    Hs = pow(Hdif, 2.0) * Htmp + Hsmin + 4.0/Retz;
  }

  return Hs;
}

Real RetCFT(const Real& Hk, const Real& Ret, const Real& MSQ = 0.0)
{

  const Real Fc = sqrt(1.0 + 0.5*(1.4-1.0)*MSQ); //MSQ is Mach^2
  Real GRT = log(Ret/Fc);
  if (GRT < 3.0)
    GRT = 3.0;

  const Real GEX = -1.74 - 0.31*Hk;
  Real arg = -1.33*Hk;
  if (arg < -20.0)
    arg = -20.0;

  const Real Thk = tanh(4.0-Hk/0.875);
  const Real CFFAC = 1.0;
  const Real Cf0 = CFFAC*0.3*exp(arg)*pow(GRT/log(10), GEX);

  Real RetCf = Ret*(Cf0 + 1.1e-4*(Thk-1.0))/Fc;

  const Real RetCf_lami = RetCFL(Hk);
  if (RetCf <  RetCf_lami)
    RetCf = RetCf_lami;

  return RetCf;
}

Real Uslip(const Real& Hk, const Real& H, const Real& Hs, const Real& GBCON=0.75)
{
  Real Us = 0.5*Hs*(1.0 - (Hk-1.0)/(GBCON*H));

  return Us;
}

Real Uslip_clamped(const IBLProfileCategory& profileCat, const Real& Hk, const Real& H, const Real& Hs, const Real& GBCON=0.75)
{
  Real Us = Uslip(Hk,H,Hs,GBCON);

  if (profileCat == IBLProfileCategory::LaminarBL ||
      profileCat == IBLProfileCategory::TurbulentBL)
  {
    if (Us > 0.98)
      Us = 0.98;
  }
  else if (profileCat == IBLProfileCategory::TurbulentWake)
  {
    if (Us > 0.99995)
      Us = 0.99995;
  }

  return Us;
}

Real calc_ctau_eq(const IBLProfileCategory& profileCat,
                  const Real& Hk, const Real& H, const Real& Hs, const Real& Ret,
                  const Real& GACON=6.7, const Real& GBCON=0.75)
{
  const Real Us = Uslip_clamped(profileCat, Hk, H, Hs, GBCON);

  Real GCC = 0.0;
  Real HKC = Hk - 1.0;
  if (profileCat == IBLProfileCategory::TurbulentBL)
  {
    GCC = 18.0;
    HKC = Hk - 1.0 - GCC/Ret;
    if (HKC < 0.01)
      HKC = 0.01;
  }

  const Real HKB = Hk - 1.0;
  const Real USB = 1.0-Us;
  const Real CTCON = 0.5/(GACON*GACON*GBCON);

  const Real ctau_eq = CTCON*Hs*HKB*pow(HKC,2.0) / (USB*H*pow(Hk,2.0));

  return ctau_eq;
}

Real RetDIT_BL(const Real& ctau,
               const Real& Hk, const Real& H, const Real& Hs, const Real& Ret,
               const Real& GACON=6.7, const Real& GBCON=0.75, const Real& MSQ=0.0)
{
  Real RetCD = 0.0;

  const Real RetCf = RetCFT(Hk, Ret, MSQ);

  const Real Us = Uslip_clamped(IBLProfileCategory::TurbulentBL, Hk, H, Hs, GBCON);

  RetCD = 0.5*RetCf*Us;

  const Real GRT = log(Ret);
  const Real Hmin = 1.0 + 2.1/GRT;
  const Real Fl = (Hk-1.0)/(Hmin-1.0);

  const Real Tfl = tanh(Fl);
  const Real Dfac = 0.5 + 0.5*Tfl;

  RetCD *= Dfac;

  RetCD += Ret*ctau*(0.995-Us) + 0.15*pow(0.995-Us,2.0);

  return RetCD;
}

Real RetDIL_Wake(const Real& Hk)
{
  Real RetCD = 1.10*pow(1.0 - 1.0/Hk, 2.0)/Hk;
  RetCD *= 2.0;

  return RetCD;
}

Real RetDIT_Wake(const Real& ctau,
                 const Real& Hk, const Real& H, const Real& Hs, const Real& Ret,
                 const Real& GACON=6.7, const Real& GBCON=0.75, const Real& MSQ=0.0)
{
  Real RetCD = 0.0;

  const Real Us = Uslip_clamped(IBLProfileCategory::TurbulentWake, Hk, H, Hs, GBCON);

  RetCD += Ret*ctau*(0.995-Us) + 0.15*pow(0.995-Us,2.0);

  RetCD *= 2.0;

  return RetCD;
}


}
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE (ThicknessesCoefficients2D_CorrelationClosure_test_suite)

typedef IncompressibleBL CompressibilityType;
typedef VarData2DDsThG VarDataType;
typedef VarTypeDsThG VarType;
typedef CorrelationClosure IBLClosureType;
typedef ThicknessesCoefficients2D<IBLClosureType, VarType, CompressibilityType> ThicknessesCoefficientsType;
typedef ThicknessesCoefficientsType::Functor<Real,Real> ThicknessesCoefficientsFunctorType;
typedef typename ThicknessesCoefficientsType::ClassParamsType ThicknessesCoefficientsParamsType;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficientsParams2D_CorrelationClosure_Dict_test )
{
  BOOST_CHECK( ThicknessesCoefficientsParamsType::params.nameCDclosure.xfoillag
            == ThicknessesCoefficientsParamsType::params.nameCDclosure.Default);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_test )
{
  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  {
    ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret, PyDict());

    BOOST_CHECK_EQUAL(ThicknessesCoefficientsParamsType::params.nameCDclosure.Default,
                      thicknessesCoefficientsObj.nameCDclosure());
  }

  {
    const std::string nameCDclosure = ThicknessesCoefficientsParamsType::params.nameCDclosure.ibl3lag;

    PyDict tcDict;
    tcDict[ThicknessesCoefficientsParamsType::params.nameCDclosure] = nameCDclosure;
    ThicknessesCoefficientsParamsType::checkInputs(tcDict);

    const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret, tcDict);

    BOOST_CHECK_EQUAL(nameCDclosure, thicknessesCoefficientsObj.nameCDclosure());
  }

  {
    PyDict tcDict;
    tcDict[ThicknessesCoefficientsParamsType::params.nameCDclosure] = "SomeInvalidName";

    BOOST_CHECK_THROW(ThicknessesCoefficientsType secondaryVarObj(varInterpret, paramInterpret, tcDict);, SANSException);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficients_Functor_test )
{
  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  PyDict tcDict;
  tcDict[ThicknessesCoefficientsParamsType::params.nameCDclosure]
     = ThicknessesCoefficientsParamsType::params.nameCDclosure.xfoillag;

  const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret, tcDict);

  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  const typename ThicknessesCoefficientsType::ArrayParam<Real> params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);
  const Real nue = 1.e-5;

  { // not-allowed profile type in Funtor initialization
    const Real delta1s = 0.1, theta11 = 0.04, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const IBLProfileCategory profileCatInvalid = IBLProfileCategory::LaminarWake;
    BOOST_CHECK_THROW(thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(profileCatInvalid, params, var, nue);,
                      SANSException);
  }

  { // invalid profile type in Funtor initialization
    const Real delta1s = 0.1, theta11 = 0.04, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const IBLProfileCategory profileCatInvalid = static_cast<IBLProfileCategory>(4);
    BOOST_CHECK_THROW(thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(profileCatInvalid, params, var, nue);,
                      SANSException);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficients_Hk_test )
{
  const Real small_tol = 1.E-15;
  const Real close_tol = 1.E-15;

  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  PyDict tcDict;
  tcDict[ThicknessesCoefficientsParamsType::params.nameCDclosure]
     = ThicknessesCoefficientsParamsType::params.nameCDclosure.xfoillag;

  const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret, tcDict);

  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  const typename ThicknessesCoefficientsType::ArrayParam<Real> params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);
  const Real nue = 1.e-5;

  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarBL, params, var, nue);

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk > 1.02);

    SANS_CHECK_CLOSE(Hk, fcn.getHk(), small_tol, close_tol);
  }

  {
    const Real delta1s = 0.005, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarBL, params, var, nue);

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk < 1.02);

    SANS_CHECK_CLOSE(1.02, fcn.getHk(), small_tol, close_tol);
  }

  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk > 1.02);

    SANS_CHECK_CLOSE(Hk, fcn.getHk(), small_tol, close_tol);
  }

  {
    const Real delta1s = 0.005, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk < 1.02);

    SANS_CHECK_CLOSE(1.02, fcn.getHk(), small_tol, close_tol);
  }

  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentWake, params, var, nue);

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk > 1.00005);

    SANS_CHECK_CLOSE(Hk, fcn.getHk(), small_tol, close_tol);
  }

  {
    const Real delta1s = 0.005, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentWake, params, var, nue);

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk < 1.00005);

    SANS_CHECK_CLOSE(1.00005, fcn.getHk(), small_tol, close_tol);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficients_LaminarBL_test )
{
  const Real small_tol = 1.E-15;
  const Real close_tol = 1.E-15;

  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  PyDict tcDict;
  tcDict[ThicknessesCoefficientsParamsType::params.nameCDclosure]
     = ThicknessesCoefficientsParamsType::params.nameCDclosure.xfoillag;

  const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret, tcDict);

  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  const typename ThicknessesCoefficientsType::ArrayParam<Real> params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);
  const Real nue = 1.e-5;

  // small Hk
  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarBL, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk < 4.35);
    const Real Hs = HSL(Hk);

    const Real theta1s = theta11 * Hs;
    const Real delta1p = delta1s;
    const Real deltarho = 0.0000000000000000e+00;
    const Real theta0s = theta11+delta1s-deltarho;

    BOOST_REQUIRE(Hk < 5.5);
    const Real RetCf1 = RetCFL(Hk);

    BOOST_REQUIRE(Hk < 4.0);
    const Real RetCD = RetDIL(Hk, Hs);

    SANS_CHECK_CLOSE(delta1s, fcn.getdelta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta11, fcn.gettheta11(), small_tol, close_tol);
    SANS_CHECK_CLOSE(H, fcn.getH(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hk, fcn.getHk(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hs, fcn.getHs(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta1s, fcn.gettheta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(delta1p, fcn.getdelta1p(), small_tol, close_tol);
    SANS_CHECK_CLOSE(deltarho, fcn.getdeltarho(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta0s, fcn.gettheta0s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(RetCf1, fcn.getRetCf1(), small_tol, close_tol);
    SANS_CHECK_CLOSE(RetCD, fcn.getRetCD(), small_tol, close_tol);
  }

  // large Hk
  {
    const Real delta1s = 0.06, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarBL, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk > 4.35);
    const Real Hs = HSL(Hk);

    const Real theta1s = theta11 * Hs;
    const Real delta1p = delta1s;
    const Real deltarho = 0.0000000000000000e+00;
    const Real theta0s = theta11+delta1s-deltarho;

    BOOST_REQUIRE(Hk > 5.5);
    const Real RetCf1 = RetCFL(Hk);

    BOOST_REQUIRE(Hk > 4.0);
    const Real RetCD = RetDIL(Hk, Hs);

    SANS_CHECK_CLOSE(delta1s, fcn.getdelta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta11, fcn.gettheta11(), small_tol, close_tol);
    SANS_CHECK_CLOSE(H, fcn.getH(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hk, fcn.getHk(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hs, fcn.getHs(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta1s, fcn.gettheta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(delta1p, fcn.getdelta1p(), small_tol, close_tol);
    SANS_CHECK_CLOSE(deltarho, fcn.getdeltarho(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta0s, fcn.gettheta0s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(RetCf1, fcn.getRetCf1(), small_tol, close_tol);
    SANS_CHECK_CLOSE(RetCD, fcn.getRetCD(), small_tol, close_tol);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficients_LaminarBL_Hs_derivatives_test )
{
  const Real small_tol = 1.E-14;
  const Real close_tol = 1.3E-14;

  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  PyDict tcDict;
  tcDict[ThicknessesCoefficientsParamsType::params.nameCDclosure]
     = ThicknessesCoefficientsParamsType::params.nameCDclosure.xfoillag;

  const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret, tcDict);

  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  const typename ThicknessesCoefficientsType::ArrayParam<Real> params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);
  const Real nue = 1.e-5;

  // small Hk
  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarBL, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk < 4.35);

    const Real Hs_Hk_python = -2.1847179722222212e-01;
    {
      Real Hs_Hk;
      fcn.getHs_derivatives_Lami(Hk, Hs_Hk);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
    }

    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives(Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(0.0, Hs_Rt, small_tol, close_tol);
    }
  }

  // large Hk
  {
    const Real delta1s = 0.06, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarBL, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk > 4.35);

    const Real Hs_Hk_python = 7.1156249999999996e-03;
    {
      Real Hs_Hk;
      fcn.getHs_derivatives_Lami(Hk, Hs_Hk);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
    }

    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives(Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(0.0, Hs_Rt, small_tol, close_tol);
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficients_TurbulentBL_Hs_test )
{
  const Real small_tol = 1.E-15;
  const Real close_tol = 1.E-15;

  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  PyDict tcDict;
  tcDict[ThicknessesCoefficientsParamsType::params.nameCDclosure]
     = ThicknessesCoefficientsParamsType::params.nameCDclosure.xfoillag;

  const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret, tcDict);

  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  const typename ThicknessesCoefficientsType::ArrayParam<Real> params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const Real qe = paramInterpret.getqe(params);

  // large Ret, large Hk
  {
    const Real delta1s = 0.05, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 500.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk > (3.0+400.0/Ret));
    const Real Hs = HST(Hk, Ret);

    const Real theta1s = theta11 * Hs;
    const Real delta1p = delta1s;
    const Real deltarho = 0.0000000000000000e+00;
    const Real theta0s = theta11+delta1s-deltarho;

    SANS_CHECK_CLOSE(delta1s, fcn.getdelta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta11, fcn.gettheta11(), small_tol, close_tol);
    SANS_CHECK_CLOSE(H, fcn.getH(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hk, fcn.getHk(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hs, fcn.getHs(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta1s, fcn.gettheta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(delta1p, fcn.getdelta1p(), small_tol, close_tol);
    SANS_CHECK_CLOSE(deltarho, fcn.getdeltarho(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta0s, fcn.gettheta0s(), small_tol, close_tol);
  }

  // large Ret, small Hk
  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 500.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk < (3.0+400.0/Ret));
    const Real Hs = HST(Hk, Ret);

    const Real theta1s = theta11 * Hs;
    const Real delta1p = delta1s;
    const Real deltarho = 0.0000000000000000e+00;
    const Real theta0s = theta11+delta1s-deltarho;

    SANS_CHECK_CLOSE(delta1s, fcn.getdelta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta11, fcn.gettheta11(), small_tol, close_tol);
    SANS_CHECK_CLOSE(H, fcn.getH(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hk, fcn.getHk(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hs, fcn.getHs(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta1s, fcn.gettheta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(delta1p, fcn.getdelta1p(), small_tol, close_tol);
    SANS_CHECK_CLOSE(deltarho, fcn.getdeltarho(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta0s, fcn.gettheta0s(), small_tol, close_tol);
  }

  // small Ret, large Hk
  {
    const Real delta1s = 0.05, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 300.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk > 4.0);
    const Real Hs = HST(Hk, Ret);

    const Real theta1s = theta11 * Hs;
    const Real delta1p = delta1s;
    const Real deltarho = 0.0000000000000000e+00;
    const Real theta0s = theta11+delta1s-deltarho;

    SANS_CHECK_CLOSE(delta1s, fcn.getdelta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta11, fcn.gettheta11(), small_tol, close_tol);
    SANS_CHECK_CLOSE(H, fcn.getH(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hk, fcn.getHk(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hs, fcn.getHs(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta1s, fcn.gettheta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(delta1p, fcn.getdelta1p(), small_tol, close_tol);
    SANS_CHECK_CLOSE(deltarho, fcn.getdeltarho(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta0s, fcn.gettheta0s(), small_tol, close_tol);
  }

  // small Ret, small Hk
  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 300.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk < 4.0);
    const Real Hs = HST(Hk, Ret);

    const Real theta1s = theta11 * Hs;
    const Real delta1p = delta1s;
    const Real deltarho = 0.0000000000000000e+00;
    const Real theta0s = theta11+delta1s-deltarho;

    SANS_CHECK_CLOSE(delta1s, fcn.getdelta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta11, fcn.gettheta11(), small_tol, close_tol);
    SANS_CHECK_CLOSE(H, fcn.getH(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hk, fcn.getHk(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hs, fcn.getHs(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta1s, fcn.gettheta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(delta1p, fcn.getdelta1p(), small_tol, close_tol);
    SANS_CHECK_CLOSE(deltarho, fcn.getdeltarho(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta0s, fcn.gettheta0s(), small_tol, close_tol);
  }

  // very small Ret, large Hk
  {
    const Real delta1s = 0.05, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 150.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk > 4.0);
    const Real Hs = HST(Hk, Ret);

    const Real theta1s = theta11 * Hs;
    const Real delta1p = delta1s;
    const Real deltarho = 0.0000000000000000e+00;
    const Real theta0s = theta11+delta1s-deltarho;

    SANS_CHECK_CLOSE(delta1s, fcn.getdelta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta11, fcn.gettheta11(), small_tol, close_tol);
    SANS_CHECK_CLOSE(H, fcn.getH(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hk, fcn.getHk(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hs, fcn.getHs(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta1s, fcn.gettheta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(delta1p, fcn.getdelta1p(), small_tol, close_tol);
    SANS_CHECK_CLOSE(deltarho, fcn.getdeltarho(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta0s, fcn.gettheta0s(), small_tol, close_tol);
  }

  // very small Ret, small Hk
  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 150.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk < 4.0);
    const Real Hs = HST(Hk, Ret);

    const Real theta1s = theta11 * Hs;
    const Real delta1p = delta1s;
    const Real deltarho = 0.0000000000000000e+00;
    const Real theta0s = theta11+delta1s-deltarho;

    SANS_CHECK_CLOSE(delta1s, fcn.getdelta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta11, fcn.gettheta11(), small_tol, close_tol);
    SANS_CHECK_CLOSE(H, fcn.getH(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hk, fcn.getHk(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hs, fcn.getHs(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta1s, fcn.gettheta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(delta1p, fcn.getdelta1p(), small_tol, close_tol);
    SANS_CHECK_CLOSE(deltarho, fcn.getdeltarho(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta0s, fcn.gettheta0s(), small_tol, close_tol);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficients_TurbulentBLandWake_Hs_derivatives_test )
{
  const Real small_tol = 1.E-14;
  const Real close_tol = 7.5E-14;

  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  PyDict tcDict;
  tcDict[ThicknessesCoefficientsParamsType::params.nameCDclosure]
     = ThicknessesCoefficientsParamsType::params.nameCDclosure.xfoillag;

  const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret, tcDict);

  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  const typename ThicknessesCoefficientsType::ArrayParam<Real> params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const Real qe = paramInterpret.getqe(params);

  // large Ret, large Hk
  {
    const Real delta1s = 0.05, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 500.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk > (3.0+400.0/Ret));

    const Real Hs_Hk_python = 1.7059524191584099e-02;
    const Real Hs_Rt_python = 2.2750033650966732e-05;
    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives_Turb(Hk, Ret, Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }

    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives(Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }
  }

  // large Ret, small Hk
  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 500.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk < (3.0+400.0/Ret));

    const Real Hs_Hk_python = -1.8434938775510201e-01;
    const Real Hs_Rt_python = -8.9490379008746383e-05;
    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives_Turb(Hk, Ret, Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }

    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives(Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }
  }

  // small Ret, large Hk
  {
    const Real delta1s = 0.05, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 300.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk > 4.0);

    const Real Hs_Hk_python = 1.6772442053470400e-02;
    const Real Hs_Rt_python = -2.9736716048501947e-05;
    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives_Turb(Hk, Ret, Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }

    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives(Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }
  }

  // small Ret, small Hk
  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 300.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk < 4.0);

    const Real Hs_Hk_python = -1.8168888888888887e-01;
    const Real Hs_Rt_python = -3.2592592592592600e-05;
    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives_Turb(Hk, Ret, Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }

    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives(Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }
  }

  // very small Ret, large Hk
  {
    const Real delta1s = 0.05, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 150.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk > 4.0);

    const Real Hs_Hk_python = 1.5760693769683976e-02;
    const Real Hs_Rt_python = 0.0;
    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives_Turb(Hk, Ret, Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }

    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives(Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }
  }

  // very small Ret, small Hk
  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 150.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk < 4.0);

    const Real Hs_Hk_python = -1.7920000000000000e-01;
    const Real Hs_Rt_python = 0.0;
    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives_Turb(Hk, Ret, Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }

    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives(Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }
  }

  // large Ret, large Hk
  {
    const Real delta1s = 0.05, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 500.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentWake, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk > (3.0+400.0/Ret));

    const Real Hs_Hk_python = 1.7059524191584099e-02;
    const Real Hs_Rt_python = 2.2750033650966732e-05;
    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives_Turb(Hk, Ret, Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }

    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives(Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }
  }

  // large Ret, small Hk
  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 500.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentWake, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk < (3.0+400.0/Ret));

    const Real Hs_Hk_python = -1.8434938775510201e-01;
    const Real Hs_Rt_python = -8.9490379008746383e-05;
    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives_Turb(Hk, Ret, Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }

    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives(Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }
  }

  // small Ret, large Hk
  {
    const Real delta1s = 0.05, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 300.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentWake, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk > 4.0);

    const Real Hs_Hk_python = 1.6772442053470400e-02;
    const Real Hs_Rt_python = -2.9736716048501947e-05;
    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives_Turb(Hk, Ret, Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }

    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives(Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }
  }

  // small Ret, small Hk
  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 300.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentWake, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk < 4.0);

    const Real Hs_Hk_python = -1.8168888888888887e-01;
    const Real Hs_Rt_python = -3.2592592592592600e-05;
    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives_Turb(Hk, Ret, Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }

    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives(Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }
  }

  // very small Ret, large Hk
  {
    const Real delta1s = 0.05, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 150.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentWake, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk > 4.0);

    const Real Hs_Hk_python = 1.5760693769683976e-02;
    const Real Hs_Rt_python = 0.0;
    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives_Turb(Hk, Ret, Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }

    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives(Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }
  }

  // very small Ret, small Hk
  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 150.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk < 4.0);

    const Real Hs_Hk_python = -1.7920000000000000e-01;
    const Real Hs_Rt_python = 0.0;
    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives_Turb(Hk, Ret, Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }

    {
      Real Hs_Hk, Hs_Rt;
      fcn.getHs_derivatives(Hs_Hk, Hs_Rt);
      SANS_CHECK_CLOSE(Hs_Hk_python, Hs_Hk, small_tol, close_tol);
      SANS_CHECK_CLOSE(Hs_Rt_python, Hs_Rt, small_tol, close_tol);
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficients_TurbulentWake_Hs_test )
{
  const Real small_tol = 1.E-15;
  const Real close_tol = 1.E-15;

  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  PyDict tcDict;
  tcDict[ThicknessesCoefficientsParamsType::params.nameCDclosure]
     = ThicknessesCoefficientsParamsType::params.nameCDclosure.xfoillag;

  const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret, tcDict);

  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  const typename ThicknessesCoefficientsType::ArrayParam<Real> params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const Real qe = paramInterpret.getqe(params);

  // large Ret, large Hk
  {
    const Real delta1s = 0.05, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 500.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentWake, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk > (3.0+400.0/Ret));
    const Real Hs = HST(Hk, Ret);

    const Real theta1s = theta11 * Hs;
    const Real delta1p = delta1s;
    const Real deltarho = 0.0000000000000000e+00;
    const Real theta0s = theta11+delta1s-deltarho;

    SANS_CHECK_CLOSE(delta1s, fcn.getdelta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta11, fcn.gettheta11(), small_tol, close_tol);
    SANS_CHECK_CLOSE(H, fcn.getH(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hk, fcn.getHk(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hs, fcn.getHs(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta1s, fcn.gettheta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(delta1p, fcn.getdelta1p(), small_tol, close_tol);
    SANS_CHECK_CLOSE(deltarho, fcn.getdeltarho(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta0s, fcn.gettheta0s(), small_tol, close_tol);
  }

  // large Ret, small Hk
  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 500.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentWake, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk < (3.0+400.0/Ret));
    const Real Hs = HST(Hk, Ret);

    const Real theta1s = theta11 * Hs;
    const Real delta1p = delta1s;
    const Real deltarho = 0.0000000000000000e+00;
    const Real theta0s = theta11+delta1s-deltarho;

    SANS_CHECK_CLOSE(delta1s, fcn.getdelta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta11, fcn.gettheta11(), small_tol, close_tol);
    SANS_CHECK_CLOSE(H, fcn.getH(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hk, fcn.getHk(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hs, fcn.getHs(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta1s, fcn.gettheta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(delta1p, fcn.getdelta1p(), small_tol, close_tol);
    SANS_CHECK_CLOSE(deltarho, fcn.getdeltarho(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta0s, fcn.gettheta0s(), small_tol, close_tol);
  }

  // small Ret, large Hk
  {
    const Real delta1s = 0.05, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 300.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentWake, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk > 4.0);
    const Real Hs = HST(Hk, Ret);

    const Real theta1s = theta11 * Hs;
    const Real delta1p = delta1s;
    const Real deltarho = 0.0000000000000000e+00;
    const Real theta0s = theta11+delta1s-deltarho;

    SANS_CHECK_CLOSE(delta1s, fcn.getdelta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta11, fcn.gettheta11(), small_tol, close_tol);
    SANS_CHECK_CLOSE(H, fcn.getH(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hk, fcn.getHk(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hs, fcn.getHs(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta1s, fcn.gettheta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(delta1p, fcn.getdelta1p(), small_tol, close_tol);
    SANS_CHECK_CLOSE(deltarho, fcn.getdeltarho(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta0s, fcn.gettheta0s(), small_tol, close_tol);
  }

  // small Ret, small Hk
  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 300.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentWake, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk < 4.0);
    const Real Hs = HST(Hk, Ret);

    const Real theta1s = theta11 * Hs;
    const Real delta1p = delta1s;
    const Real deltarho = 0.0000000000000000e+00;
    const Real theta0s = theta11+delta1s-deltarho;

    SANS_CHECK_CLOSE(delta1s, fcn.getdelta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta11, fcn.gettheta11(), small_tol, close_tol);
    SANS_CHECK_CLOSE(H, fcn.getH(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hk, fcn.getHk(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hs, fcn.getHs(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta1s, fcn.gettheta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(delta1p, fcn.getdelta1p(), small_tol, close_tol);
    SANS_CHECK_CLOSE(deltarho, fcn.getdeltarho(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta0s, fcn.gettheta0s(), small_tol, close_tol);
  }

  // very small Ret, large Hk
  {
    const Real delta1s = 0.05, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 150.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentWake, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk > 4.0);
    const Real Hs = HST(Hk, Ret);

    const Real theta1s = theta11 * Hs;
    const Real delta1p = delta1s;
    const Real deltarho = 0.0000000000000000e+00;
    const Real theta0s = theta11+delta1s-deltarho;

    SANS_CHECK_CLOSE(delta1s, fcn.getdelta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta11, fcn.gettheta11(), small_tol, close_tol);
    SANS_CHECK_CLOSE(H, fcn.getH(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hk, fcn.getHk(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hs, fcn.getHs(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta1s, fcn.gettheta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(delta1p, fcn.getdelta1p(), small_tol, close_tol);
    SANS_CHECK_CLOSE(deltarho, fcn.getdeltarho(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta0s, fcn.gettheta0s(), small_tol, close_tol);
  }

  // very small Ret, small Hk
  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 150.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentWake, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk < 4.0);
    const Real Hs = HST(Hk, Ret);

    const Real theta1s = theta11 * Hs;
    const Real delta1p = delta1s;
    const Real deltarho = 0.0000000000000000e+00;
    const Real theta0s = theta11+delta1s-deltarho;

    SANS_CHECK_CLOSE(delta1s, fcn.getdelta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta11, fcn.gettheta11(), small_tol, close_tol);
    SANS_CHECK_CLOSE(H, fcn.getH(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hk, fcn.getHk(), small_tol, close_tol);
    SANS_CHECK_CLOSE(Hs, fcn.getHs(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta1s, fcn.gettheta1s(), small_tol, close_tol);
    SANS_CHECK_CLOSE(delta1p, fcn.getdelta1p(), small_tol, close_tol);
    SANS_CHECK_CLOSE(deltarho, fcn.getdeltarho(), small_tol, close_tol);
    SANS_CHECK_CLOSE(theta0s, fcn.gettheta0s(), small_tol, close_tol);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficients_TurbulentBL_RetCf_test )
{
  const Real small_tol = 1.E-15;
  const Real close_tol = 1.E-15;

  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret);

  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  const typename ThicknessesCoefficientsType::ArrayParam<Real> params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const Real qe = paramInterpret.getqe(params);

  // large Ret, large Hk, RetCf_turb < RetCf_lami
  {
    const Real delta1s = 0.16, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 5.0e5;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk > (20.0/1.33));
    const Real RetCf1 = RetCFT(Hk, Ret, 0.0);
//    std::cout << "RetCf1_lami = " << (RetCFL(Hk)) << std::endl;
//    std::cout << "RetCf1_turb - RetCf1_lami = " << (RetCf1 - RetCFL(Hk)) << std::endl;
//    BOOST_REQUIRE((RetCf1 - RetCFL(Hk)) > 1e-6); // making sure turb and lami Cf are different //TODO: almost impossible...

    SANS_CHECK_CLOSE(RetCf1, fcn.getRetCf1(), small_tol, close_tol);
  }

  // large Ret, small Hk
  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 500.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk < (20.0/1.33));
    const Real RetCf1 = RetCFT(Hk, Ret, 0.0);
//    std::cout << "RetCf1_lami = " << (RetCFL(Hk)) << std::endl;
//    std::cout << "RetCf1_turb - RetCf1_lami = " << (RetCf1 - RetCFL(Hk)) << std::endl;
    BOOST_REQUIRE((RetCf1 - RetCFL(Hk)) > 1e-6); // making sure turb and lami Cf are different

    SANS_CHECK_CLOSE(RetCf1, fcn.getRetCf1(), small_tol, close_tol);
  }

  // small Ret, large Hk
  {
    const Real delta1s = 0.16, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 15.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk > (20.0/1.33));
    const Real RetCf1 = RetCFT(Hk, Ret, 0.0);
//    std::cout << "RetCf1_lami = " << (RetCFL(Hk)) << std::endl;
//    std::cout << "RetCf1_turb - RetCf1_lami = " << (RetCf1 - RetCFL(Hk)) << std::endl;
//    BOOST_REQUIRE((RetCf1 - RetCFL(Hk)) > 1e-10); // making sure turb and lami Cf are different //TODO: almost impossible...

    SANS_CHECK_CLOSE(RetCf1, fcn.getRetCf1(), small_tol, close_tol);
  }

  // small Ret, small Hk
  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 15.0;
    const Real nue = qe*theta11/Ret;

    const ThicknessesCoefficientsFunctorType fcn
    = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

    BOOST_CHECK_EQUAL(6.7, fcn.getA_GbetaLocus());
    BOOST_CHECK_EQUAL(0.75, fcn.getB_GbetaLocus());

    const Real H = delta1s/theta11;
    const Real Hk = H;

    BOOST_REQUIRE(Hk < (20.0/1.33));
    const Real RetCf1 = RetCFT(Hk, Ret, 0.0);
//    std::cout << "RetCf1_lami = " << (RetCFL(Hk)) << std::endl;
//    std::cout << "RetCf1_turb - RetCf1_lami = " << (RetCf1 - RetCFL(Hk)) << std::endl;
//    BOOST_REQUIRE((RetCf1 - RetCFL(Hk)) > 1e-10); // making sure turb and lami Cf are different //TODO: almost impossible...

    SANS_CHECK_CLOSE(RetCf1, fcn.getRetCf1(), small_tol, close_tol);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficients_TurbulentWake_RetCf_test )
{
  const Real small_tol = 1.E-15;
  const Real close_tol = 1.E-15;

  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret);

  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  const typename ThicknessesCoefficientsType::ArrayParam<Real> params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const Real RetCf1 = 0.0;

  const Real qe = paramInterpret.getqe(params);

  const std::vector<Real> delta1s_arr = {0.02, 0.04, 0.06, 0.10, 0.16};
  const std::vector<Real> Ret_arr = {10.0, 100.0, 200.0, 300.0, 1000.0, 2000.0};

  const Real theta11 = 0.01, G = -10.0;

  for (int i=0; i<(int)delta1s_arr.size(); ++i)
  {
    const Real delta1s = delta1s_arr.at(i);
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));
    for (int j=0; j<(int)Ret_arr.size(); ++j)
    {
      const Real Ret = Ret_arr.at(j);
      const Real nue = qe*theta11/Ret;

      const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentWake, params, var, nue);

      SANS_CHECK_CLOSE(RetCf1, fcn.getRetCf1(), small_tol, close_tol);
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficients_Uslip_test )
{
  const Real small_tol = 1.E-15;
  const Real close_tol = 1.E-15;

  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret);

  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  const typename ThicknessesCoefficientsType::ArrayParam<Real> params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);
  const Real qe = paramInterpret.getqe(params);

  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 500.0;
    const Real nue = qe*theta11/Ret;

    const Real H = delta1s/theta11;
    const Real Hk = H;
    const Real Hs = HSL(Hk);

    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarBL, params, var, nue);

    Real Us = Uslip(Hk, H, Hs, fcn.getB_GbetaLocus());
    BOOST_REQUIRE(Us < 0.98); // Us does not have to be clamped

    SANS_CHECK_CLOSE(Us, fcn.getUslip(Hk,H,Hs), small_tol, close_tol);
  }

  {
    const Real delta1s = 0.0101, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 500.0;
    const Real nue = qe*theta11/Ret;

    const Real H = delta1s/theta11;
    const Real Hk = H;
    const Real Hs = HSL(Hk);

    const IBLProfileCategory profileCat = IBLProfileCategory::LaminarBL;
    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

    Real Us = Uslip(Hk, H, Hs, fcn.getB_GbetaLocus());
    BOOST_REQUIRE(Us > 0.98); // Us has to be clamped
    Us = Uslip_clamped(profileCat, Hk, H, Hs, fcn.getB_GbetaLocus());

    SANS_CHECK_CLOSE(Us, fcn.getUslip(Hk,H,Hs), small_tol, close_tol);
  }

  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 500.0;
    const Real nue = qe*theta11/Ret;

    const Real H = delta1s/theta11;
    const Real Hk = H;
    const Real Hs = HSL(Hk);

    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

    Real Us = Uslip(Hk, H, Hs, fcn.getB_GbetaLocus());
    BOOST_REQUIRE(Us < 0.98); // Us does not have to be clamped

    SANS_CHECK_CLOSE(Us, fcn.getUslip(Hk,H,Hs), small_tol, close_tol);
  }

  {
    const Real delta1s = 0.0101, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 500.0;
    const Real nue = qe*theta11/Ret;

    const Real H = delta1s/theta11;
    const Real Hk = H;
    const Real Hs = HSL(Hk);

    const IBLProfileCategory profileCat = IBLProfileCategory::TurbulentBL;

    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

    Real Us = Uslip(Hk, H, Hs, fcn.getB_GbetaLocus());
    BOOST_REQUIRE(Us > 0.98); // Us has to be clamped

    Us = Uslip_clamped(profileCat, Hk, H, Hs, fcn.getB_GbetaLocus());

    SANS_CHECK_CLOSE(Us, fcn.getUslip(Hk,H,Hs), small_tol, close_tol);
  }

  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 500.0;
    const Real nue = qe*theta11/Ret;

    const Real H = delta1s/theta11;
    const Real Hk = H;
    const Real Hs = HSL(Hk);

    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentWake, params, var, nue);

    Real Us = Uslip(Hk, H, Hs, fcn.getB_GbetaLocus());
    BOOST_REQUIRE(Us < 0.99995); // Us does not have to be clamped

    SANS_CHECK_CLOSE(Us, fcn.getUslip(Hk,H,Hs), small_tol, close_tol);
  }

  {
    const Real delta1s = 0.0101, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 500.0;
    const Real nue = qe*theta11/Ret;

    const Real H = delta1s/theta11;
    const Real Hk = H;
    const Real Hs = HSL(Hk);

    const IBLProfileCategory profileCat = IBLProfileCategory::TurbulentWake;

    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

    Real Us = Uslip(Hk, H, Hs, fcn.getB_GbetaLocus());
    BOOST_REQUIRE(Us > 0.99995); // Us has to be clamped

    Us = Uslip_clamped(profileCat, Hk, H, Hs, fcn.getB_GbetaLocus());

    SANS_CHECK_CLOSE(Us, fcn.getUslip(Hk,H,Hs), small_tol, close_tol);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficients_ctaueq_test )
{
  const Real small_tol = 1.E-15;
  const Real close_tol = 1.E-15;

  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret);

  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  const typename ThicknessesCoefficientsType::ArrayParam<Real> params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const Real qe = paramInterpret.getqe(params);

  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 500.0;
    const Real nue = qe*theta11/Ret;

    const Real H = delta1s/theta11;
    const Real Hk = H;
    const Real Hs = HST(Hk, Ret);

    const IBLProfileCategory profileCat = IBLProfileCategory::TurbulentBL;

    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

    const Real ctaueq = calc_ctau_eq(profileCat, Hk, H, Hs, Ret, fcn.getA_GbetaLocus(), fcn.getB_GbetaLocus());

    BOOST_REQUIRE(Hk-1.0-18.0/Ret > 0.01);

    SANS_CHECK_CLOSE(ctaueq, fcn.getctaueq(), small_tol, close_tol);
  }

  {
    const Real delta1s = 0.0103, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real Ret = 500.0;
    const Real nue = qe*theta11/Ret;

    const Real H = delta1s/theta11;
    const Real Hk = H;
    const Real Hs = HST(Hk, Ret);

    const IBLProfileCategory profileCat = IBLProfileCategory::TurbulentBL;

    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

    const Real ctaueq = calc_ctau_eq(profileCat, Hk, H, Hs, Ret, fcn.getA_GbetaLocus(), fcn.getB_GbetaLocus());

    BOOST_REQUIRE(Hk-1.0-18.0/Ret < 0.01);

    SANS_CHECK_CLOSE(ctaueq, fcn.getctaueq(), small_tol, close_tol);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficients_RetCD_TurbulentBL_test )
{
  const Real small_tol = 1.E-15;
  const Real close_tol = 1.E-15;

  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret);

  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  const typename ThicknessesCoefficientsType::ArrayParam<Real> params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const Real qe = paramInterpret.getqe(params);

  const IBLProfileCategory profileCat = IBLProfileCategory::TurbulentBL;

  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real ctau = varInterpret.getctau(var);
    const Real Ret = 500.0;
    const Real nue = qe*theta11/Ret;

    const Real H = delta1s/theta11;
    const Real Hk = H;
    const Real Hs = HST(Hk, Ret);

    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

    Real RetCD = RetDIT_BL(ctau, Hk, H, Hs, Ret, 6.7, 0.75, 0.0);
    const Real RetCD_lami = RetDIL(Hk, Hs);
    BOOST_REQUIRE(RetCD < RetCD_lami);

    RetCD = RetCD_lami; // lami-turb blending

    SANS_CHECK_CLOSE(RetCD, fcn.getRetCD(), small_tol, close_tol);
  }

  {
    const Real delta1s = 0.0103, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real ctau = varInterpret.getctau(var);
    const Real Ret = 500.0;
    const Real nue = qe*theta11/Ret;

    const Real H = delta1s/theta11;
    const Real Hk = H;
    const Real Hs = HST(Hk, Ret);

    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

    const Real RetCD = RetDIT_BL(ctau, Hk, H, Hs, Ret, 6.7, 0.75, 0.0);
    const Real RetCD_lami = RetDIL(Hk, Hs);
//    std::cout << "RetCD      = " << RetCD << std::endl;
//    std::cout << "RetCD_lami = " << RetCD_lami << std::endl;
    BOOST_REQUIRE(RetCD > RetCD_lami);

    SANS_CHECK_CLOSE(RetCD, fcn.getRetCD(), small_tol, close_tol);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThicknessesCoefficients_RetCD_TurbulentWake_test )
{
  const Real small_tol = 1.E-15;
  const Real close_tol = 1.E-15;

  const typename ThicknessesCoefficientsType::VarInterpType varInterpret;
  const typename ThicknessesCoefficientsType::ParamInterpType paramInterpret;

  const ThicknessesCoefficientsType thicknessesCoefficientsObj(varInterpret, paramInterpret);

  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  const typename ThicknessesCoefficientsType::ArrayParam<Real> params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const Real qe = paramInterpret.getqe(params);

  const IBLProfileCategory profileCat = IBLProfileCategory::TurbulentWake;

  {
    const Real delta1s = 0.02, theta11 = 0.01, G = -10.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real ctau = varInterpret.getctau(var);
    const Real Ret = 500.0;
    const Real nue = qe*theta11/Ret;

    const Real H = delta1s/theta11;
    const Real Hk = H;
    const Real Hs = HST(Hk, Ret);


    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

    Real RetCD = RetDIT_Wake(ctau, Hk, H, Hs, Ret, 6.7, 0.75, 0.0);
    const Real RetCD_lami = RetDIL_Wake(Hk);
    BOOST_REQUIRE(RetCD < RetCD_lami);

    RetCD = RetCD_lami; // lami-turb blending

    SANS_CHECK_CLOSE(RetCD, fcn.getRetCD(), small_tol, close_tol);
  }

  {
    const Real delta1s = 0.0103, theta11 = 0.01, G = -2.0;
    const typename ThicknessesCoefficientsType::ArrayQ<Real> var = varInterpret.setDOFFrom<Real>(VarDataType(delta1s, theta11, G));

    const Real ctau = varInterpret.getctau(var);
    const Real Ret = 2000.0;
    const Real nue = qe*theta11/Ret;

    const Real H = delta1s/theta11;
    const Real Hk = H;
    const Real Hs = HST(Hk, Ret);

    const ThicknessesCoefficientsFunctorType fcn
      = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

    const Real RetCD = RetDIT_Wake(ctau, Hk, H, Hs, Ret, 6.7, 0.75, 0.0);
    const Real RetCD_lami = RetDIL_Wake(Hk);
//    std::cout << "RetCD      = " << RetCD << std::endl;
//    std::cout << "RetCD_lami = " << RetCD_lami << std::endl;
    BOOST_REQUIRE(RetCD > RetCD_lami);

    SANS_CHECK_CLOSE(RetCD, fcn.getRetCD(), small_tol, close_tol);
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
