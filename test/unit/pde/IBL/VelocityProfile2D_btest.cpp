// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// testing 2D velocity profile class

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include "SANS_btest.h"

#include "pde/IBL/VelocityProfile2D.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE (VelocityProfile2D_test_suite)

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_static_test )
{
  BOOST_CHECK_EQUAL(0, static_cast<int>(IBLProfileCategory::LaminarBL));
  BOOST_CHECK_EQUAL(1, static_cast<int>(IBLProfileCategory::LaminarWake));
  BOOST_CHECK_EQUAL(2, static_cast<int>(IBLProfileCategory::TurbulentBL));
  BOOST_CHECK_EQUAL(3, static_cast<int>(IBLProfileCategory::TurbulentWake));

  BOOST_CHECK_EQUAL(0, VelocityProfile2D::U);
  BOOST_CHECK_EQUAL(1, VelocityProfile2D::dUdeta);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LaminarBL_test )
{
  const Real tol = 1.3e-12;

  VelocityProfile2D profile;

  IBLProfileCategory profileCat = IBLProfileCategory::LaminarBL;
  typedef typename VelocityProfile2D::VelocityInfoType VelocityInfoType;

  const std::vector<Real> eta = {0.0, 0.2, 0.8, 1.0};
  std::vector<Real> U(eta.size(),-1);
  std::vector<Real> dUdeta(eta.size(),-1);
  std::map<VelocityInfoType, std::vector<Real>> velocityInfo = {{VelocityInfoType::U, U}, {VelocityInfoType::dUdeta, dUdeta}};

  { // wrong profile type
    const Real A = 0.66;

    BOOST_CHECK_THROW(profile.LaminarVelocityProfile(IBLProfileCategory::TurbulentBL, A, eta, U, dUdeta);,
                      SANSException);
    BOOST_CHECK_THROW(profile.LaminarVelocityProfile(IBLProfileCategory::TurbulentWake, A, eta, U, dUdeta);,
                      SANSException);

    const Real delta = -1, Redelta = -1; //dummies
    // note that 4 is out of the scope of this enumeration
    IBLProfileCategory profileCatInvalid = static_cast<IBLProfileCategory>(4);
    BOOST_CHECK_THROW(profile.velocityProfile(profileCatInvalid, delta, A, Redelta, eta, velocityInfo);,
                      SANSException);
  }

  {
    const Real A = 0.66;
    const Real delta = -1, Redelta = -1; //dummies

    profile.velocityProfile(profileCat, delta, A, Redelta, eta, velocityInfo);
    profile.LaminarVelocityProfile(profileCat, A, eta, U, dUdeta);

    const std::vector<Real> U_true = {0.0,
                                      2.349073055379957e-01,
                                      9.747986717286610e-01,
                                      1.0};
    const std::vector<Real> dUdeta_true = {6.580511744692173e-01,
                                           1.544624644244041e+00,
                                           3.555962404859357e-01,
                                           0.0};
    for (size_t i=0; i < eta.size(); i++)
    {
      BOOST_CHECK_CLOSE( U_true[i], velocityInfo[VelocityInfoType::U].at(i), tol );
      BOOST_CHECK_CLOSE( dUdeta_true[i], velocityInfo[VelocityInfoType::dUdeta].at(i), tol );

      BOOST_CHECK_CLOSE( U_true[i], U[i], tol );
      BOOST_CHECK_CLOSE( dUdeta_true[i], dUdeta[i], tol );
    }
  }

  {
    const Real A = 0.0;
    const Real delta = -1, Redelta = -1; //dummies

    profile.velocityProfile(profileCat, delta, A, Redelta, eta, velocityInfo);
    profile.LaminarVelocityProfile(profileCat, A, eta, U, dUdeta);

    const std::vector<Real> U_true = {0.000000000000000e+00,
                                      1.513088000000000e-01,
                                      9.678848000000000e-01,
                                      1.000000000000000e+00};
    const std::vector<Real> dUdeta_true = {0.000000000000000e+00,
                                           1.368064000000000e+00,
                                           4.495359999999985e-01,
                                           0.000000000000000e+00};
    for (size_t i=0; i < eta.size(); i++)
    {
      BOOST_CHECK_CLOSE( U_true[i], velocityInfo[VelocityInfoType::U].at(i), tol );
      BOOST_CHECK_CLOSE( dUdeta_true[i], velocityInfo[VelocityInfoType::dUdeta].at(i), tol );

      BOOST_CHECK_CLOSE( U_true[i], U[i], tol );
      BOOST_CHECK_CLOSE( dUdeta_true[i], dUdeta[i], tol );
    }
  }

  {
    const Real A = -0.66;
    const Real delta = -1, Redelta = -1; //dummies

    profile.velocityProfile(profileCat, delta, A, Redelta, eta, velocityInfo);
    profile.LaminarVelocityProfile(profileCat, A, eta, U, dUdeta);

    const std::vector<Real> U_true = {0.000000000000000e+00,
                                      6.132498741368685e-02,
                                      9.599067104299528e-01,
                                      1.000000000000000e+00};
    const std::vector<Real> dUdeta_true = {-6.580511744692173e-01,
                                           1.155142579508596e+00,
                                           5.576653307325443e-01,
                                           0.000000000000000e+00};
    for (size_t i=0; i < eta.size(); i++)
    {
      BOOST_CHECK_CLOSE( U_true[i], velocityInfo[VelocityInfoType::U].at(i), tol );
      BOOST_CHECK_CLOSE( dUdeta_true[i], velocityInfo[VelocityInfoType::dUdeta].at(i), tol );

      BOOST_CHECK_CLOSE( U_true[i], U[i], tol );
      BOOST_CHECK_CLOSE( dUdeta_true[i], dUdeta[i], tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LaminarWake_test )
{
  const Real tol = 6.e-14;

  VelocityProfile2D profile;

  IBLProfileCategory profileCat = IBLProfileCategory::LaminarWake;
  typedef typename VelocityProfile2D::VelocityInfoType VelocityInfoType;

  const std::vector<Real> eta = {0.0, 0.2, 0.8, 1.0};
  std::vector<Real> U(eta.size(),-1);
  std::vector<Real> dUdeta(eta.size(),-1);
  std::map<VelocityInfoType, std::vector<Real>> velocityInfo = {{VelocityInfoType::U, U}, {VelocityInfoType::dUdeta, dUdeta}};

  // positive A
  {
    const Real A = 0.66;
    const Real delta = -1, Redelta = -1; //dummies
    profile.velocityProfile(profileCat, delta, A, Redelta, eta, velocityInfo);
    profile.LaminarVelocityProfile(profileCat, A, eta, U, dUdeta);

    const std::vector<Real> U_true = {8.250000000000000e-02,
                                      2.483840000000000e-01,
                                      9.750439999999999e-01,
                                      1.0};
    const std::vector<Real> dUdeta_true = {0.0,
                                           1.409280000000000e+00,
                                           3.523200000000003e-01,
                                           0.0};
    for (size_t i=0; i < eta.size(); i++)
    {
      BOOST_CHECK_CLOSE( U_true[i], velocityInfo[VelocityInfoType::U].at(i), tol );
      BOOST_CHECK_CLOSE( dUdeta_true[i], velocityInfo[VelocityInfoType::dUdeta].at(i), tol );

      BOOST_CHECK_CLOSE( U_true[i], U[i], tol );
      BOOST_CHECK_CLOSE( dUdeta_true[i], dUdeta[i], tol );
    }
  }

  // negative A
  {
    const Real A = -0.66;
    const Real delta = -1, Redelta = -1; //dummies
    profile.velocityProfile(profileCat, delta, A, Redelta, eta, velocityInfo);
    profile.LaminarVelocityProfile(profileCat, A, eta, U, dUdeta);

    const std::vector<Real> U_true = {-4.969879518072289e-02,
                                      1.400867469879518e-01,
                                      9.714481927710843e-01,
                                      1.0};
    const std::vector<Real> dUdeta_true = {0.0,
                                           1.612337349397591e+00,
                                           4.030843373493979e-01,
                                           0.0};
    for (size_t i=0; i < eta.size(); i++)
    {
      BOOST_CHECK_CLOSE( U_true[i], velocityInfo[VelocityInfoType::U].at(i), tol );
      BOOST_CHECK_CLOSE( dUdeta_true[i], velocityInfo[VelocityInfoType::dUdeta].at(i), tol );

      BOOST_CHECK_CLOSE( U_true[i], U[i], tol );
      BOOST_CHECK_CLOSE( dUdeta_true[i], dUdeta[i], tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( TurbulentBL_test )
{
  VelocityProfile2D profile;

  IBLProfileCategory profileCat = IBLProfileCategory::TurbulentBL;
  typedef typename VelocityProfile2D::VelocityInfoType VelocityInfoType;

  const std::vector<Real> eta = {0.0, 0.2, 0.8, 1.0};
  std::vector<Real> U(eta.size(),-1);
  std::map<VelocityInfoType, std::vector<Real>> velocityInfo = {{VelocityInfoType::U, U}};

  {
    const Real A = 1.0, Redelta = 500.0;
    // Exceptions should be thrown because of invalid profile types
    BOOST_CHECK_THROW(profile.TurbulentVelocityProfile(IBLProfileCategory::LaminarBL, Redelta, A, eta, U);,
                      SANSException);
    BOOST_CHECK_THROW(profile.TurbulentVelocityProfile(IBLProfileCategory::LaminarWake, Redelta, A, eta, U);,
                      SANSException);

    // Exceptions should be thrown because either Redelta or A is non-positive
    Real Redelta_invalid = -1;
    BOOST_CHECK_THROW(profile.TurbulentVelocityProfile(profileCat,Redelta_invalid,A,eta,U);,
                      SANSException);
    Redelta_invalid = 0;
    BOOST_CHECK_THROW(profile.TurbulentVelocityProfile(profileCat,Redelta_invalid,A,eta,U);,
                      SANSException);
  }

  const Real tol = 2.e-13;

  { // non-negative A
    const Real A = 3.0, Redelta = 500.0;
    const Real delta = -1; //dummy
    profile.velocityProfile(profileCat, delta, A, Redelta, eta, velocityInfo);
    profile.TurbulentVelocityProfile(profileCat, Redelta, A, eta, U);

#if ISIBL3SRCTURB
    const std::vector<Real> U_true = {0.000000000000000e+00,
                                      4.653348416412721e-01,
                                      9.624149964837364e-01,
                                      1.000000000000000e+00};
#else
    const std::vector<Real> U_true = {0.000000000000000e+00,
                                      5.445664988547779e-01,
                                      9.777983774309451e-01,
                                      1.000000000000000e+00};
#endif
    for (size_t i=0; i < eta.size(); i++)
    {
      BOOST_CHECK_CLOSE( U_true[i], velocityInfo[VelocityInfoType::U].at(i), tol );
      BOOST_CHECK_CLOSE( U_true[i], U[i], tol );
    }
  }

  { // negative A
    const Real A = -1.0, Redelta = 500.0;
    const Real delta = -1; //dummy
    profile.velocityProfile(profileCat, delta, A, Redelta, eta, velocityInfo);
    profile.TurbulentVelocityProfile(profileCat, Redelta, A, eta, U);

#if ISIBL3SRCTURB
    const std::vector<Real> U_true = {0.000000000000000e+00,
                                      -1.756876015623898e-02,
                                      8.711364103362106e-01,
                                      1.000000000000000e+00};
#else
    const std::vector<Real> U_true = {0.000000000000000e+00,
                                      1.519006687754815e-02,
                                      9.175572433247572e-01,
                                      1.000000000000000e+00};
#endif
    for (size_t i=0; i < eta.size(); i++)
    {
      BOOST_CHECK_CLOSE( U_true[i], velocityInfo[VelocityInfoType::U].at(i), tol );
      BOOST_CHECK_CLOSE( U_true[i], U[i], tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( TurbulentWake_test )
{
  const Real tol = 7.e-14;

  VelocityProfile2D profile;

  IBLProfileCategory profileCat = IBLProfileCategory::TurbulentWake;
  typedef typename VelocityProfile2D::VelocityInfoType VelocityInfoType;

  const std::vector<Real> eta = {0.0, 0.2, 0.8, 1.0};
  std::vector<Real> U(eta.size(),-1);
  std::map<VelocityInfoType, std::vector<Real>> velocityInfo = {{VelocityInfoType::U, U}};

  { // wrong profile type
    const Real A = 1.0, Redelta = 500.0;
    // Exceptions should be thrown because of invalid profile types
    BOOST_CHECK_THROW(profile.TurbulentVelocityProfile(IBLProfileCategory::LaminarBL, Redelta, A, eta, U);,
                      SANSException);
    BOOST_CHECK_THROW(profile.TurbulentVelocityProfile(IBLProfileCategory::LaminarWake, Redelta, A, eta, U);,
                      SANSException);

    // Exceptions should be thrown because Redelta is non-positive
    Real Redelta_invalid = -1;
    BOOST_CHECK_THROW(profile.TurbulentVelocityProfile(profileCat,Redelta_invalid,A,eta,U);,
                      SANSException);
  }

  { // non-negative A
    const Real A = 3.0, Redelta = 500.0;
    const Real delta = -1; //dummy
    profile.velocityProfile(profileCat, delta, A, Redelta, eta, velocityInfo);
    profile.TurbulentVelocityProfile(profileCat, Redelta, A, eta, U);

#if ISIBL3SRCTURB
    const std::vector<Real> U_true = {3.750000000000000e-01,
                                      4.400000000000000e-01,
                                      9.350000000000001e-01,
                                      1.000000000000000e+00};
#else
    const std::vector<Real> U_true = {3.750000000000000e-01,
                                      4.640000000000000e-01,
                                      9.589999999999999e-01,
                                      1.000000000000000e+00};
#endif

    for (size_t i=0; i < eta.size(); i++)
    {
      BOOST_CHECK_CLOSE( U_true[i], velocityInfo[VelocityInfoType::U].at(i), tol );
      BOOST_CHECK_CLOSE( U_true[i], U[i], tol );
    }
  }

  { // negative A
    const Real A = -1.0, Redelta = 500.0;
    const Real delta = -1; //dummy
    profile.velocityProfile(profileCat, delta, A, Redelta, eta, velocityInfo);
    profile.TurbulentVelocityProfile(profileCat, Redelta, A, eta, U);

#if ISIBL3SRCTURB
    const std::vector<Real> U_true = {-6.250000000000000e-02,
                                      4.800000000000003e-02,
                                      8.895000000000002e-01,
                                      1.000000000000000e+00};
#else
    const std::vector<Real> U_true = {-6.250000000000000e-02,
                                      8.879999999999999e-02,
                                      9.302999999999998e-01,
                                      1.000000000000000e+00};
#endif

    for (size_t i=0; i < eta.size(); i++)
    {
      BOOST_CHECK_CLOSE( U_true[i], velocityInfo[VelocityInfoType::U].at(i), tol );
      BOOST_CHECK_CLOSE( U_true[i], U[i], tol );
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
