// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/IBL/PDEIBLSplitAmpLag2D.h"

using namespace std;

namespace SANS
{
typedef VarData2DDsThNGsplit VarDataType;

typedef PhysD2 PhysDim;
typedef VarTypeDsThNGsplit VarType;
typedef PDEIBLSplitAmpLag<PhysDim,VarType> PDEClass;

typedef typename PDEClass::IBLTraitsType IBLTraitsType;

typedef typename PDEClass::VarInterpType VarInterpretType;
typedef typename PDEClass::ProfileCategory ProfileCategory;
typedef typename PDEClass::GasModelType GasModelType;
typedef typename PDEClass::ViscosityModelType ViscosityModelType;
typedef typename PDEClass::TransitionModelType TransitionModelType;

typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

typedef typename PDEClass::template ArrayParam<Real> ArrayParam;
typedef typename PDEClass::template MatrixParam<Real> MatrixParam;

typedef typename PDEClass::template VectorX<Real> VectorX;
typedef typename PDEClass::template TensorX<Real> TensorX;

typedef typename PDEClass::template ArrayParam<Real> ArrayParam;
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEIBLSplitAmpLag2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef IBLTraits<PhysDim, IBLSplitAmpLagField> IBLTraitsType;
  BOOST_CHECK( (std::is_same<typename PDEClass::IBLTraitsType, IBLTraitsType>::value) );

  BOOST_CHECK( PDEClass::D == PhysDim::D );
  BOOST_CHECK( PDEClass::N == IBLTraitsType::N );
  BOOST_CHECK( PDEClass::Nparam == IBLTraitsType::Nparam );

  BOOST_CHECK( PDEClass::ir_mom == 0 );
  BOOST_CHECK( PDEClass::ir_ke == 1 );
  BOOST_CHECK( PDEClass::ir_amp == 2 );
  BOOST_CHECK( PDEClass::ir_lag == 3 );

  BOOST_CHECK( ArrayQ::M == PDEClass::N );
  BOOST_CHECK( MatrixQ::M == PDEClass::N );
  BOOST_CHECK( MatrixQ::N == PDEClass::N );

  BOOST_CHECK( ArrayParam::M == PDEClass::Nparam );
  BOOST_CHECK( MatrixParam::M == PDEClass::N );
  BOOST_CHECK( MatrixParam::N == PDEClass::Nparam );

  BOOST_CHECK( VectorX::M == PDEClass::D );
  BOOST_CHECK( TensorX::M == PDEClass::D );
  BOOST_CHECK( TensorX::N == PDEClass::D );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctorsANDbooleanANDmiscellaneous_test )
{
  const Real gam = 1.6, R = 28.715, mue = 7.0E-5;

  {
    // models
    PyDict gasModelDict;
    gasModelDict[GasModelParamsIBL::params.gamma] = gam;
    gasModelDict[GasModelParamsIBL::params.R] = R;
    GasModelParamsIBL::checkInputs(gasModelDict);
    const GasModelType gasModel(gasModelDict);

    ViscosityModelType viscosityModel(mue);

    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.ntinit] = 0.0;
    transitionModelDict[TransitionModelParams::params.ntcrit] = 9.0;
    transitionModelDict[TransitionModelParams::params.xtr] = 10.0;
    transitionModelDict[TransitionModelParams::params.isTransitionActive] = true;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                             = TransitionModelParams::params.profileCatDefault.laminarBL;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    // constructor
    PDEClass pde(gasModel, viscosityModel, transitionModel);

    // hasFlux functions
    BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
    BOOST_CHECK( pde.hasFluxAdvective() == true );
    BOOST_CHECK( pde.hasFluxViscous() == false );
    BOOST_CHECK( pde.hasSource() == true );
    BOOST_CHECK( pde.hasSourceTrace() == false );
    BOOST_CHECK( pde.hasSourceLiftedQuantity() == false );
    BOOST_CHECK( pde.hasForcingFunction() == false );

    BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

    // accessor functions
    auto& viscosityModel_ = pde.getViscosityModel();
    BOOST_CHECK(typeid(ViscosityModelType&) == typeid(viscosityModel_) );

    auto& transitionModel_ = pde.getTransitionModel();
    BOOST_CHECK(typeid(TransitionModelType&) == typeid(transitionModel_) );

    auto varInterpret_ = pde.getVarInterpreter();
    BOOST_CHECK(typeid(VarInterpretType&) == typeid(varInterpret_) );

    auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();
    BOOST_CHECK(typeid(typename PDEClass::ThicknessesCoefficientsType&) == typeid(thicknessesCoefficientsObj) );

    Real gam_, R_, mue_;
    pde.evalParam( gam_, R_, mue_ );
    BOOST_CHECK_EQUAL(gam, gam_);
    BOOST_CHECK_EQUAL(R, R_);
    BOOST_CHECK_EQUAL(mue, mue_);
  }
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxAdvectiveTime_jacobianFluxAdvectiveTime_NotImplemented_test )
{
  const Real gam = 1.6;
  const Real R = 350.0;
  const Real mue = 7.0e-5;

  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = gam;
  gasModelDict[GasModelParamsIBL::params.R] = R;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(mue);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const TransitionModelType transitionModel(transitionModelDict);

  PDEClass pde(gasModel, viscosityModel, transitionModel);

  const Real delta1s = 2.0e-2, theta11 = 1.0e-2, nt = 2.3, G = -9.0;

  const ArrayQ var = pde.setDOFFrom(VarDataType(delta1s, theta11, nt, G));
  BOOST_CHECK( pde.isValidState(var) );

  const Real qx = 4.0, qz = 3.0, p0 = 6e+3, T0 = 400.0;
  const Real qx_x = 0, qx_z = 0, qz_x = 0, qz_z = 0;

  const typename PDEClass::ParamInterpType paramInterpret;
  const ArrayParam param = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const Real x = 0.1, z = 0.3, t = 0.0;
  const VectorX e1 = { 0.6, 0.8 };

  ArrayQ ft = 0.0;
  BOOST_CHECK_THROW(pde.fluxAdvectiveTime(param,e1,x,z,var,ft);, DeveloperException);

  MatrixQ J = 0.0;
  BOOST_CHECK_THROW(pde.jacobianFluxAdvectiveTime(param,e1,x,z,t,var,J);, DeveloperException);
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( masterState_fluxAdvective_source_calcIntermittency_NoBlending_test )
{
  // TODO: currently not testing fully laminar/turbulent source terms
  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  const Real gam = 1.6;
  const Real R = 350.0;
  const Real mue = 7.0E-5;

  const Real qx = 4.0, qz = 3.0, p0 = 6e+3, T0 = 400.0;
  const Real qx_x = 0, qx_z = 0, qz_x = 0, qz_z = 0;

  const typename PDEClass::ParamInterpType paramInterpret;
  const ArrayParam params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const Real x = 0.1, z = 0.3, t = 0.2;
  const VectorX e1 = { 0.6, 0.8 };

  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = gam;
  gasModelDict[GasModelParamsIBL::params.R] = R;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(mue);

  // laminar BL
  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.isTransitionActive] = true;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                             = TransitionModelParams::params.profileCatDefault.laminarBL;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    const Real delta1s = 2.0e-2, theta11 = 1.0e-2, nt = 2.3, G = -9.0;
    ArrayQ var = pde.setDOFFrom(VarDataType(delta1s,theta11,nt,G));
    BOOST_CHECK( pde.isValidState(var) );
    BOOST_REQUIRE( pde.calcIntermittency(params, e1, x, z, var) == 0.0 ); // make sure to test fully laminar

    const auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();

    const Real qe = paramInterpret.getqe(params);
    const Real rhoe = gasModel.density(qe, p0, T0);
    const Real nue = viscosityModel.dynamicViscosity()/rhoe;

    const auto fcn = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarBL, params, var, nue);
    const Real deltarho = fcn.getdeltarho();
    const Real theta0s = fcn.gettheta0s();
    const Real theta1s = fcn.gettheta1s();

    const auto q1 = paramInterpret.getq1(params);

    const ArrayQ uConsTrue = { rhoe*(delta1s-deltarho)*dot(q1,e1),
                               rhoe*theta0s,
                               nt,
                               G };

    const VectorX PTdote = rhoe*theta11*dot(q1,e1)*q1;
    const VectorX K = rhoe*theta1s*q1;

    const ArrayQ fx_true = {PTdote[0], K[0], nt*q1[0], G*q1[0]};
    const ArrayQ fz_true = {PTdote[1], K[1], nt*q1[1], G*q1[1]};

    ArrayQ uCons = 0.0, fx = 0.0, fz = 0.0;
    pde.masterState(params,e1,x,z,var,uCons);
    pde.fluxAdvective(params, e1, x, z, t, var, fx, fz);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( uConsTrue[i], uCons[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( fx_true[i], fx[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( fz_true[i], fz[i], small_tol, close_tol );
    }

    pde.masterState(params,e1,x,z,var,uCons);
    pde.fluxAdvective(params, e1, x, z, t, var, fx, fz);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( uConsTrue[i], uCons[i], small_tol, close_tol ); // master state is not cumulative
      SANS_CHECK_CLOSE( 2.0*fx_true[i], fx[i], small_tol, close_tol ); // flux is cumulative
      SANS_CHECK_CLOSE( 2.0*fz_true[i], fz[i], small_tol, close_tol );
    }
  }

  // turbulent BL
  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.isTransitionActive] = false;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                             = TransitionModelParams::params.profileCatDefault.turbulentBL;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    const Real delta1s = 2.0e-2, theta11 = 1.0e-2, nt = 2.3, G = -9.0;
    ArrayQ var = pde.setDOFFrom(VarDataType(delta1s,theta11,nt,G));
    BOOST_CHECK( pde.isValidState(var) );
    BOOST_REQUIRE( pde.calcIntermittency(params, e1, x, z, var) == 1.0 ); // make sure to test fully turbulent

    const auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();

    const Real qe = paramInterpret.getqe(params);
    const Real rhoe = gasModel.density(qe, p0, T0);
    const Real nue = viscosityModel.dynamicViscosity()/rhoe;

    const auto fcn = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);
    const Real deltarho = fcn.getdeltarho();
    const Real theta0s = fcn.gettheta0s();
    const Real theta1s = fcn.gettheta1s();

    const auto q1 = paramInterpret.getq1(params);

    const ArrayQ uConsTrue = { rhoe*(delta1s-deltarho)*dot(q1,e1),
                               rhoe*theta0s,
                               nt,
                               G };

    const VectorX PTdote = rhoe*theta11*dot(q1,e1)*q1;
    const VectorX K = rhoe*theta1s*q1;

    const ArrayQ fx_true = {PTdote[0], K[0], nt*q1[0], G*q1[0]};
    const ArrayQ fz_true = {PTdote[1], K[1], nt*q1[1], G*q1[1]};

    ArrayQ uCons = 0.0, fx = 0.0, fz = 0.0;
    pde.masterState(params,e1,x,z,var,uCons);
    pde.fluxAdvective(params, e1, x, z, t, var, fx, fz);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( uConsTrue[i], uCons[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( fx_true[i], fx[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( fz_true[i], fz[i], small_tol, close_tol );
    }

    pde.masterState(params,e1,x,z,var,uCons);
    pde.fluxAdvective(params, e1, x, z, t, var, fx, fz);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( uConsTrue[i], uCons[i], small_tol, close_tol ); // master state is not cumulative
      SANS_CHECK_CLOSE( 2.0*fx_true[i], fx[i], small_tol, close_tol ); // flux is cumulative
      SANS_CHECK_CLOSE( 2.0*fz_true[i], fz[i], small_tol, close_tol );
    }
  }

  // turbulent wake
  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.isTransitionActive] = false;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                        = TransitionModelParams::params.profileCatDefault.turbulentWake;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    const Real delta1s = 2.0e-2, theta11 = 1.0e-2, nt = 2.3, G = -9.0;
    ArrayQ var = pde.setDOFFrom(VarDataType(delta1s,theta11,nt,G));
    BOOST_CHECK( pde.isValidState(var) );
    BOOST_REQUIRE( pde.calcIntermittency(params, e1, x, z, var) == 1.0 ); // make sure to test fully turbulent

    const auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();

    const Real qe = paramInterpret.getqe(params);
    const Real rhoe = gasModel.density(qe, p0, T0);
    const Real nue = viscosityModel.dynamicViscosity()/rhoe;

    const auto fcn = thicknessesCoefficientsObj.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentWake, params, var, nue);
    const Real deltarho = fcn.getdeltarho();
    const Real theta0s = fcn.gettheta0s();
    const Real theta1s = fcn.gettheta1s();

    const auto q1 = paramInterpret.getq1(params);

    const ArrayQ uConsTrue = { rhoe*(delta1s-deltarho)*dot(q1,e1),
                               rhoe*theta0s,
                               nt,
                               G };

    const VectorX PTdote = rhoe*theta11*dot(q1,e1)*q1;
    const VectorX K = rhoe*theta1s*q1;

    const ArrayQ fx_true = {PTdote[0], K[0], nt*q1[0], G*q1[0]};
    const ArrayQ fz_true = {PTdote[1], K[1], nt*q1[1], G*q1[1]};

    ArrayQ uCons = 0.0, fx = 0.0, fz = 0.0;
    pde.masterState(params,e1,x,z,var,uCons);
    pde.fluxAdvective(params, e1, x, z, t, var, fx, fz);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( uConsTrue[i], uCons[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( fx_true[i], fx[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( fz_true[i], fz[i], small_tol, close_tol );
    }

    pde.masterState(params,e1,x,z,var,uCons);
    pde.fluxAdvective(params, e1, x, z, t, var, fx, fz);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( uConsTrue[i], uCons[i], small_tol, close_tol ); // master state is not cumulative
      SANS_CHECK_CLOSE( 2.0*fx_true[i], fx[i], small_tol, close_tol ); // flux is cumulative
      SANS_CHECK_CLOSE( 2.0*fz_true[i], fz[i], small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( masterState_fluxAdvective_source_calcIntermittency_WithBlending_test )
{
  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  const Real gam = 1.6;
  const Real R = 350.0;
  const Real mue = 7.0E-5;

  const Real qx = 4.0, qz = 3.0, p0 = 6e+3, T0 = 400.0;
  const Real qx_x = 0.1, qx_z = 0.2, qz_x = 0.3, qz_z = 0.4;

  const typename PDEClass::ParamInterpType paramInterpret;
  const ArrayParam params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const Real x = 0.1, z = 0.3, t = 0.2;
  const VectorX e1 = { 0.6, 0.8 };

  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = gam;
  gasModelDict[GasModelParamsIBL::params.R] = R;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(mue);

  // laminar-turbulent blending
  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.isTransitionActive] = true;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                             = TransitionModelParams::params.profileCatDefault.laminarBL;
    transitionModelDict[TransitionModelParams::params.intermittencyLength] = 0.2;
    transitionModelDict[TransitionModelParams::params.xtr]
                        = x + 0.3*transitionModelDict.get(TransitionModelParams::params.intermittencyLength);

    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    const Real delta1s = 2.0e-2, theta11 = 1.0e-2, nt = 2.3, G = -9.0;
    const ArrayQ var = pde.setDOFFrom(VarDataType(delta1s,theta11,nt,G));
    BOOST_CHECK( pde.isValidState(var) );

    const Real intermittency = pde.calcIntermittency(params, e1, x, z, var);
    BOOST_REQUIRE( 0.0 < intermittency && intermittency < 1.0); // make sure to test blending

    const Real intermittency_true = 0.5*(1.0+sin(PI*(x-transitionModel.xtr())/transitionModel.getIntermittencyLength()));
    SANS_CHECK_CLOSE(intermittency_true, intermittency, small_tol, close_tol);

    ArrayQ uConsTrue_lami = 0.0, uConsTrue_turb = 0.0;
    pde.masterState(IBLProfileCategory::LaminarBL, params, e1, x, z, var, uConsTrue_lami);
    pde.masterState(IBLProfileCategory::TurbulentBL, params, e1, x, z, var, uConsTrue_turb);
    const ArrayQ uConsTrue = (1.0-intermittency)*uConsTrue_lami + intermittency*uConsTrue_turb;

    ArrayQ fxTrue_lami = 0.0, fxTrue_turb = 0.0;
    ArrayQ fzTrue_lami = 0.0, fzTrue_turb = 0.0;
    pde.fluxAdvective(IBLProfileCategory::LaminarBL, params, e1, x, z, t, var, fxTrue_lami, fzTrue_lami);
    pde.fluxAdvective(IBLProfileCategory::TurbulentBL, params, e1, x, z, t, var, fxTrue_turb, fzTrue_turb);
    const ArrayQ fx_true = (1.0-intermittency)*fxTrue_lami + intermittency*fxTrue_turb;
    const ArrayQ fz_true = (1.0-intermittency)*fzTrue_lami + intermittency*fzTrue_turb;

    ArrayQ src_lami_true = 0.0, src_turb_true = 0.0;
    const ArrayQ var_x = var, var_z = var; // values are dummy
    const VectorX e1_x = 0.0, e1_z = 0.0; // values are dummy
    pde.source(IBLProfileCategory::LaminarBL, params, e1, e1_x, e1_z, x, z, t, var, var_x, var_z, src_lami_true);
    pde.source(IBLProfileCategory::TurbulentBL, params, e1, e1_x, e1_z, x, z, t, var, var_x, var_z, src_turb_true);
    const ArrayQ src_true = (1.0-intermittency)*src_lami_true + intermittency*src_turb_true;

    ArrayQ uCons = 0.0, fx = 0.0, fz = 0.0, src = 0.0;
    pde.masterState(params,e1,x,z,var,uCons);
    pde.fluxAdvective(params, e1, x, z, t, var, fx, fz);
    pde.source(params, e1, e1_x, e1_z, x, z, t, var, var_x, var_z, src);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( uConsTrue[i], uCons[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( fx_true[i], fx[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( fz_true[i], fz[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( src_true[i], src[i], small_tol, close_tol );
    }

    pde.masterState(params,e1,x,z,var,uCons);
    pde.fluxAdvective(params, e1, x, z, t, var, fx, fz);
    pde.source(params, e1, e1_x, e1_z, x, z, t, var, var_x, var_z, src);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( uConsTrue[i], uCons[i], small_tol, close_tol ); // master state is not cumulative
      SANS_CHECK_CLOSE( 2.0*fx_true[i], fx[i], small_tol, close_tol ); // flux is cumulative
      SANS_CHECK_CLOSE( 2.0*fz_true[i], fz[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( 2.0*src_true[i], src[i], small_tol, close_tol ); // source is cumulative
    }
  }

  { // check forcing binary intermittency by nullifying intermittency length scale
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.isTransitionActive] = true;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                        = TransitionModelParams::params.profileCatDefault.laminarBL;
    transitionModelDict[TransitionModelParams::params.intermittencyLength] = 0.0;
    transitionModelDict[TransitionModelParams::params.xtr]
                        = x + 0.3*transitionModelDict.get(TransitionModelParams::params.intermittencyLength);

    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    const Real delta1s = 2.0e-2, theta11 = 1.0e-2, nt = 2.3, G = -9.0;
    const ArrayQ var = pde.setDOFFrom(VarDataType(delta1s,theta11,nt,G));
    BOOST_CHECK( pde.isValidState(var) );

    const Real intermittency = pde.calcIntermittency(params, e1, x, z, var);
    BOOST_CHECK( intermittency == transitionModel.binaryIntermittency(nt, x));
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
