// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCIBL2D_btest
//
// test of 2-D IBL BC classes

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/IBL/BCIBL2D.h"

#define BCPARAMETERS_INSTANTIATE // included because testing without NDConvert
#include "pde/BCParameters_impl.h"

namespace SANS
{
typedef PhysD2 PhysDim;
typedef VarData2DDANCt VarDataType;
typedef VarTypeDANCt VarType;
typedef PDEIBL<PhysDim,VarType> PDEClass;

typedef typename PDEClass::VarInterpType VarInterpretType;

typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
typedef typename PDEClass::template ArrayParam<Real> ArrayParam;
typedef typename PDEClass::template VectorX<Real> VectorX;
typedef typename PDEClass::ProfileCategory ProfileCategory;
typedef typename PDEClass::GasModelType GasModelType;
typedef typename PDEClass::ViscosityModelType ViscosityModelType;
typedef typename PDEClass::TransitionModelType TransitionModelType;

typedef BCNone<PhysD2,PDEClass::N> BCClassNone;

typedef BCIBL2D<BCTypeWakeMatch,VarType> BCClassWakeMatch;
typedef typename BCClassWakeMatch::ParamsType BCParamType_WakeMatch;

typedef BCIBL2D<BCTypeFullState,VarType> BCClassFullState;

typedef BCParameters<BCIBL2DVector<VarType> > BCParamsIBL;

//Explicitly instantiate classes so coverage information is correct
template class BCIBL2D<BCTypeWakeMatch,VarType>;
template class BCIBL2D<BCTypeFullState,VarType>;

// Instantiate BCParamaters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters< BCIBL2DVector<VarType> >;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BCIBL2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  { // BCNone
    BOOST_CHECK( (std::is_same<typename BCCategory::None, typename BCClassNone::Category>::value) );
    BOOST_CHECK( (std::is_same<BCNone<PhysDim,PDEClass::N>::ParamsType, typename BCClassNone::ParamsType>::value) );

    BOOST_CHECK( (std::is_same<typename BCClassNone::PhysDim, PhysDim>::value) );
    // BOOST_CHECK( (std::is_same<typename BCClassNone::PDEClass, PDEClass>::value) );

    BOOST_CHECK( BCClassNone::D == PhysDim::D );
    BOOST_CHECK( BCClassNone::N == PDEClass::N );
    BOOST_CHECK( BCClassNone::NBC == 0 );

    BOOST_CHECK( (std::is_same<typename BCClassNone::template ArrayQ<Real>, typename PDEClass::template ArrayQ<Real>>::value) );
    BOOST_CHECK( (std::is_same<typename BCClassNone::template MatrixQ<Real>, typename PDEClass::template MatrixQ<Real>>::value) );
  }

  { // BCTypeFullState
    BOOST_CHECK( (std::is_same<typename BCCategory::Flux_mitState, typename BCClassFullState::Category>::value) );
    BOOST_CHECK( (std::is_same<BCIBL2DParams<BCTypeFullState>, typename BCClassFullState::ParamsType>::value) );

    BOOST_CHECK( (std::is_same<typename BCClassFullState::PhysDim, PhysDim>::value) );
    BOOST_CHECK( (std::is_same<typename BCClassFullState::PDEClass, PDEClass>::value) );

    BOOST_CHECK( BCClassFullState::D == PhysDim::D );
    BOOST_CHECK( BCClassFullState::N == PDEClass::N );
    BOOST_CHECK( BCClassFullState::NBC == PDEClass::N );

    BOOST_CHECK( (std::is_same<typename BCClassFullState::template ArrayQ<Real>, typename PDEClass::template ArrayQ<Real>>::value) );
    BOOST_CHECK( (std::is_same<typename BCClassFullState::template MatrixQ<Real>, typename PDEClass::template MatrixQ<Real>>::value) );
    BOOST_CHECK( (std::is_same<typename BCClassFullState::template ArrayParam<Real>, typename PDEClass::template ArrayParam<Real>>::value) );
  }

  { // BCTypeWakeMatch
    BOOST_CHECK( (std::is_same<typename BCCategory::HubTrace, typename BCClassWakeMatch::Category>::value) );
    BOOST_CHECK( (std::is_same<BCIBL2DParams<BCTypeWakeMatch>, typename BCClassWakeMatch::ParamsType>::value) );

    BOOST_CHECK( (std::is_same<typename BCClassWakeMatch::PhysDim, PhysDim>::value) );
    BOOST_CHECK( (std::is_same<typename BCClassWakeMatch::PDEClass, PDEClass>::value) );

    BOOST_CHECK( BCClassWakeMatch::D == PhysDim::D );
    BOOST_CHECK( BCClassWakeMatch::N == PDEClass::N );
    BOOST_CHECK( BCClassWakeMatch::NBC == 0 );

    BOOST_CHECK( (std::is_same<typename BCClassWakeMatch::template ArrayQ<Real>, typename PDEClass::template ArrayQ<Real>>::value) );
    BOOST_CHECK( (std::is_same<typename BCClassWakeMatch::template MatrixQ<Real>, typename PDEClass::template MatrixQ<Real>>::value) );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_test )
{
  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const typename PDEClass::GasModelType gasModel(gasModelDict);

  const typename PDEClass::ViscosityModelType viscosityModel(1.8725e-5);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const typename PDEClass::TransitionModelType transitionModel(transitionModelDict);

  // pde
  PDEClass pde(gasModel, viscosityModel, transitionModel);

  { // BCNone
    // pde constructor
    BCClassNone bc1;

    // PyDict constructor
    PyDict BCNoneArgs;
    BCNoneArgs[BCParamsIBL::params.BC.BCType] = BCParamsIBL::params.BC.None;

    BCClassNone bc2(pde,BCNoneArgs);

    PyDict PyBCList;
    PyBCList["BCNone"] = BCNoneArgs;

    BCParamsIBL::checkInputs(PyBCList);
  }

  { // BCTypeWakeMatch
    // enum constructor
    {
      BCClassWakeMatch bc1(pde, BCParamType_WakeMatch::params.matchingType.trailingEdge);
      BCClassWakeMatch bc2(pde, BCParamType_WakeMatch::params.matchingType.wakeInflow);

      BOOST_CHECK_THROW( BCClassWakeMatch(pde, "InvalidMatchingType");, AssertionException );
    }

    // PyDict constuctor
    PyDict BCWakeMatchArgs;
    BCWakeMatchArgs[BCParamsIBL::params.BC.BCType] = BCParamsIBL::params.BC.Matching;
    { // TE
      BCWakeMatchArgs[BCClassWakeMatch::ParamsType::params.matchingType] = BCClassWakeMatch::ParamsType::params.matchingType.trailingEdge;

      BCClassWakeMatch bc1(pde,BCWakeMatchArgs);

      PyDict PyBCList;
      PyBCList["BCWakeMatch_TE"] = BCWakeMatchArgs;

      BCParamsIBL::checkInputs(PyBCList);
    }

    { // Wake inflow
      BCWakeMatchArgs[BCClassWakeMatch::ParamsType::params.matchingType] = BCClassWakeMatch::ParamsType::params.matchingType.wakeInflow;

      BCClassWakeMatch bc1(pde,BCWakeMatchArgs);

      PyDict PyBCList;
      PyBCList["BCWakeMatch_WakeInflow"] = BCWakeMatchArgs;

      BCParamsIBL::checkInputs(PyBCList);
    }

    { // wrong BC type
      BCWakeMatchArgs[BCClassWakeMatch::ParamsType::params.matchingType] = "WrongWakeMatchBCType";

      BOOST_CHECK_THROW( BCClassWakeMatch bc1(pde,BCWakeMatchArgs);, SANSException );
    }
  }

  { // BCTypeFullState
    PyDict BCFullStateArgs;
    BCFullStateArgs[BCParamsIBL::params.BC.BCType] = BCParamsIBL::params.BC.FullState;
    BCFullStateArgs[BCClassFullState::ParamsType::params.isNumericalFlux] = true;
    BCFullStateArgs[BCClassFullState::ParamsType::params.deltaLami] = 0.01;
    BCFullStateArgs[BCClassFullState::ParamsType::params.ALami] = 0.2;
    BCFullStateArgs[BCClassFullState::ParamsType::params.deltaTurb] = 0.02;
    BCFullStateArgs[BCClassFullState::ParamsType::params.ATurb] = 1.2;
    BCFullStateArgs[BCClassFullState::ParamsType::params.nt] = 2.3;
    BCFullStateArgs[BCClassFullState::ParamsType::params.ctau] = 1.2;

    PyDict PyBCList;
    PyBCList["BCFullState"] = BCFullStateArgs;

    BCParamsIBL::checkInputs(PyBCList);
    BOOST_CHECK_THROW(BCClassFullState bc1(pde,BCFullStateArgs);, SANSException);

//    BOOST_CHECK( bc1.useUpwind() );
//
//    BOOST_CHECK( bc1.hasFluxViscous() == pde.hasFluxViscous() );


//    BCFullStateArgs[BCClassFullState::ParamsType::params.isNumericalFlux] = false;
//    BCClassFullState bc2(pde,BCFullStateArgs);
//
//    BOOST_CHECK( !(bc2.useUpwind()) );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCNone_test )
{
  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const typename PDEClass::GasModelType gasModel(gasModelDict);

  const typename PDEClass::ViscosityModelType viscosityModel(1.8725e-5);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const typename PDEClass::TransitionModelType transitionModel(transitionModelDict);

  // pde
  PDEClass pde(gasModel, viscosityModel, transitionModel);

  PyDict BCArgs;
  BCArgs[BCParamsIBL::params.BC.BCType] = BCParamsIBL::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCNone"] = BCArgs;

  BCParamsIBL::checkInputs(PyBCList);

  BCClassNone bc(pde,BCArgs);

  BOOST_CHECK( bc.useFluxViscous() == false );

  // state

  const Real nx = 1.0;
  const Real ny = 0.0;

  // isValidState

  const std::vector<Real> deltaLami_arr = {-1.0, 0.0, 1.0, 2.0, 3.0};
  const std::vector<Real> deltaTurb_arr = {-1.0, 0.0, 1.0, 2.0};
  const std::vector<Real> ctau_arr = {-1.0, 0.0, 1.0};

  const Real ALami = 1.0, ATurb = 1.0, nt = 0.1;

  for (int i = 0; i < (int) deltaLami_arr.size(); ++i)
  {
    Real deltaLami = deltaLami_arr.at(i);
    for (int j = 0; j < (int) deltaTurb_arr.size(); ++j)
    {
      Real deltaTurb = deltaTurb_arr.at(j);
      for (int k = 0; k < (int) ctau_arr.size(); ++k)
      {
        Real ctau = ctau_arr.at(k);
        ArrayQ qI = pde.setDOFFrom( VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau) );
        BOOST_CHECK( bc.isValidState(nx,ny,qI) == true );
      }
    }
  }
}

#if 0 //Turned off until BCTypeFullState is needed again
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFullState_test )
{
  const Real tol = 1e-15;

  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const typename PDEClass::GasModelType gasModel(gasModelDict);

  const typename PDEClass::ViscosityModelType viscosityModel(1.8725e-5);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const typename PDEClass::TransitionModelType transitionModel(transitionModelDict);

  // pde
  PDEClass pde(gasModel, viscosityModel, transitionModel);

  PyDict BCArgs;
  BCArgs[BCParamsIBL::params.BC.BCType] = BCParamsIBL::params.BC.FullState;
  BCArgs[BCClassFullState::ParamsType::params.isNumericalFlux] = true;
  BCArgs[BCClassFullState::ParamsType::params.deltaLami] = 0.01;
  BCArgs[BCClassFullState::ParamsType::params.ALami] = 0.2;
  BCArgs[BCClassFullState::ParamsType::params.deltaTurb] = 0.02;
  BCArgs[BCClassFullState::ParamsType::params.ATurb] = 1.2;
  BCArgs[BCClassFullState::ParamsType::params.nt] = 2.3;
  BCArgs[BCClassFullState::ParamsType::params.ctau] = 1.2;

  BCClassFullState bc(pde,BCArgs);

  PyDict PyBCList;
  PyBCList["BCFullState"] = BCArgs;

  BCParamsIBL::checkInputs(PyBCList);

  // state

  const Real x = 1;
  const Real y = 1;
  const Real t = 1;
  const Real nx = 1;
  const Real ny = 0;

  ArrayQ qI = 0.0;
  ArrayQ qB = 0;
  VarDataType qBdatatrue = { BCArgs.get(BCClassFullState::ParamsType::params.deltaLami),
                                BCArgs.get(BCClassFullState::ParamsType::params.ALami),
                                BCArgs.get(BCClassFullState::ParamsType::params.deltaTurb),
                                BCArgs.get(BCClassFullState::ParamsType::params.ATurb),
                                BCArgs.get(BCClassFullState::ParamsType::params.nt),
                                BCArgs.get(BCClassFullState::ParamsType::params.ctau) };
  ArrayQ qBtrue = pde.setDOFFrom(qBdatatrue);

  const Real qx = 0, qz = 0, p0 = 1e5, T0 = 300;
  const Real qx_x = 0, qx_z = 0, qz_x = 0, qz_z = 0;
  const ArrayParam params = pde.getParamInterpreter().setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  bc.state(params,x,y,t,nx,ny,qI,qB);

  const int N = BCClassFullState::N;
  for (int i = 0; i < N; ++i)
    SANS_CHECK_CLOSE( qBtrue[i], qB[i], tol, tol );

  // isValidState

  const std::vector<Real> deltaLami_arr = {-1.0, 0.0, 1.0};
  const std::vector<Real> deltaTurb_arr = {-1.0, 0.0, 1.0};
  const std::vector<Real> ctau_arr = {-1.0, 0.0, 1.0};

  const Real ALami = 1.0, ATurb = 1.0, nt = 0.1;

  for (int i = 0; i < (int) deltaLami_arr.size(); ++i)
  {
    Real deltaLami = deltaLami_arr.at(i);
    for (int j = 0; j < (int) deltaTurb_arr.size(); ++j)
    {
      Real deltaTurb = deltaTurb_arr.at(j);
      for (int k = 0; k < (int) ctau_arr.size(); ++k)
      {
        Real ctau = ctau_arr.at(j);
        qI = pde.setDOFFrom( VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau) );
        BOOST_CHECK( bc.isValidState(nx,ny,qI) == pde.isValidState(qI) );
      }
    }
  }
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeWakeMatch_test )
{
  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const typename PDEClass::GasModelType gasModel(gasModelDict);

  const typename PDEClass::ViscosityModelType viscosityModel(1.8725e-5);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const typename PDEClass::TransitionModelType transitionModel(transitionModelDict);

  // pde
  PDEClass pde(gasModel, viscosityModel, transitionModel);

  BCClassWakeMatch bc(pde, BCParamType_WakeMatch::params.matchingType.trailingEdge);

  const Real nx = 1;
  const Real ny = 0;

  const std::vector<Real> deltaLami_arr = {-1.0, 0.0, 1.0, 2.0, 3.0};
  const std::vector<Real> deltaTurb_arr = {-1.0, 0.0, 1.0, 2.0};
  const std::vector<Real> ctau_arr = {-1.0, 0.0, 1.0};

  const Real ALami = 1.0, ATurb = 1.0, nt = 0.1;

  for (int i = 0; i < (int) deltaLami_arr.size(); ++i)
  {
    Real deltaLami = deltaLami_arr.at(i);
    for (int j = 0; j < (int) deltaTurb_arr.size(); ++j)
    {
      Real deltaTurb = deltaTurb_arr.at(j);
      for (int k = 0; k < (int) ctau_arr.size(); ++k)
      {
        Real ctau = ctau_arr.at(k);
        ArrayQ qI = pde.setDOFFrom( VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau) );
        BOOST_CHECK( bc.isValidState(nx,ny,qI) == pde.isValidState(qI) );
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeWakeMatch_TrailingEdge_fluxMatchingSourceHubtrace_test )
{
  const Real small_tol = 1e-15;
  const Real close_tol = 2E-13;

  const Real gam = 1.6;
  const Real R = 66.0;
  const Real mue = 7.0E-5;

  const auto& matchingType = BCParamType_WakeMatch::params.matchingType.trailingEdge;

  const Real x = 0.1;
  const Real z = 3.8;
  const Real t = 0.3;

  const Real deltaLami = 2.0e-2, ALami = -0.5, deltaTurb = 1.e-2, ATurb = 1.0, nt = 2.3, ctau = 1.2e-3;
  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  VarInterpretType varInterpret;
  ArrayQ var = varInterpret.setDOFFrom<Real>(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));

  BOOST_CHECK( varInterpret.isValidState(var) );

  ArrayQ varhb = 0.0;
  varhb[PDEClass::ir_momLami] = -0.1;
  varhb[PDEClass::ir_keLami] = -0.2;
  varhb[PDEClass::ir_momTurb] = -0.3;
  varhb[PDEClass::ir_keTurb] = -0.4;
  varhb[PDEClass::ir_amp] = -0.5;
  varhb[PDEClass::ir_lag] = -0.6;

  const typename PDEClass::ParamInterpType paramInterpret;
  const ArrayParam params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const VectorX e1 = {0.6, 0.8};
  const VectorX nrm = {0.8, 0.6};
  const Real nx = nrm[0], nz = nrm[1];

  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = gam;
  gasModelDict[GasModelParamsIBL::params.R] = R;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(mue);

  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.isTransitionActive] = true;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                             = TransitionModelParams::params.profileCatDefault.laminarBL;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    BCClassWakeMatch bc(pde, matchingType);

    const ProfileCategory profileCat = pde.getProfileCategory(var,x);
    const auto& paramInterpret_ = pde.getParamInterpreter();
    const Real qe = paramInterpret_.getqe(params);
    const Real rhoe = (pde.getGasModel()).density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));
    const Real nue = (pde.getViscosityModel()).dynamicViscosity() / rhoe;

    const auto& thicknessesCoefficientsFunctor
      = (pde.getSecondaryVarObj()).getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

    ArrayQ fTrue = 0.0; // zero by default
    {
      ArrayQ fx = 0.0, fz = 0.0;
      pde.fluxAdvective(params, e1, x, z, t, var, fx, fz);

      fTrue[PDEClass::ir_momLami] = fx[PDEClass::ir_momLami]*nx + fz[PDEClass::ir_momLami]*nz;
      fTrue[PDEClass::ir_keLami] = fx[PDEClass::ir_keLami]*nx + fz[PDEClass::ir_keLami]*nz;
      fTrue[PDEClass::ir_momTurb] = 0.0;
      fTrue[PDEClass::ir_keTurb] = 0.0;
      fTrue[PDEClass::ir_amp] = fx[PDEClass::ir_amp]*nx + fz[PDEClass::ir_amp]*nz;
      fTrue[PDEClass::ir_lag] = 0.0;
    }

    ArrayQ sourcehubTrue = 0.0;
    {
      const Real delta1s = thicknessesCoefficientsFunctor.getdelta1s();
      const Real theta11 = thicknessesCoefficientsFunctor.gettheta11();

      const Real Hk = thicknessesCoefficientsFunctor.getHk();
      const Real ctau_eq = thicknessesCoefficientsFunctor.getctaueq();
      const Real CTR = 1.8*exp(-3.3/(Hk-1.0));
      const Real ctau_TurbInit = pow(CTR,2.0)*ctau_eq; // Matching: sum ctau weighted by momentum defect thicknesses (XFOIL,1989)

      sourcehubTrue[PDEClass::ir_momLami] = varhb[PDEClass::ir_momLami] - bc.hb_dummy_;
      sourcehubTrue[PDEClass::ir_keLami] = varhb[PDEClass::ir_keLami] - bc.hb_dummy_;
      sourcehubTrue[PDEClass::ir_momTurb] = delta1s + fabs(z);
      sourcehubTrue[PDEClass::ir_keTurb] = theta11;
      sourcehubTrue[PDEClass::ir_amp] = theta11 * nt;
      sourcehubTrue[PDEClass::ir_lag] = theta11 * ctau_TurbInit;
    }

    ArrayQ f = 0.0, sourcehub = 0.0;

    // Matching flux at a hubtrace
    bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( fTrue[i], f[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( sourcehubTrue[i], sourcehub[i], small_tol, close_tol );
    }

    // cumulation: matching flux from airfoil to wake
    bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( 2.0*fTrue[i], f[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( 2.0*sourcehubTrue[i], sourcehub[i], small_tol, close_tol );
    }
  }

  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.isTransitionActive] = false;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                             = TransitionModelParams::params.profileCatDefault.laminarBL;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    BCClassWakeMatch bc(pde, matchingType);

    const ProfileCategory profileCat = pde.getProfileCategory(var,x);
    const auto& paramInterpret_ = pde.getParamInterpreter();
    const Real qe = paramInterpret_.getqe(params);
    const Real rhoe = (pde.getGasModel()).density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));
    const Real nue = (pde.getViscosityModel()).dynamicViscosity() / rhoe;

    const auto& thicknessesCoefficientsFunctor
      = (pde.getSecondaryVarObj()).getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

    ArrayQ fTrue = 0.0; // zero by default
    {
      ArrayQ fx = 0.0, fz = 0.0;
      pde.fluxAdvective(params, e1, x, z, t, var, fx, fz);

      fTrue[PDEClass::ir_momLami] = fx[PDEClass::ir_momLami]*nx + fz[PDEClass::ir_momLami]*nz;
      fTrue[PDEClass::ir_keLami] = fx[PDEClass::ir_keLami]*nx + fz[PDEClass::ir_keLami]*nz;
      fTrue[PDEClass::ir_momTurb] = 0.0;
      fTrue[PDEClass::ir_keTurb] = 0.0;
      fTrue[PDEClass::ir_amp] = fx[PDEClass::ir_amp]*nx + fz[PDEClass::ir_amp]*nz;
      fTrue[PDEClass::ir_lag] = 0.0;
    }

    ArrayQ sourcehubTrue = 0.0;
    {
      const Real delta1s = thicknessesCoefficientsFunctor.getdelta1s();
      const Real theta11 = thicknessesCoefficientsFunctor.gettheta11();

      sourcehubTrue[PDEClass::ir_momLami] = varhb[PDEClass::ir_momLami] - bc.hb_dummy_;
      sourcehubTrue[PDEClass::ir_keLami] = varhb[PDEClass::ir_keLami] - bc.hb_dummy_;
      sourcehubTrue[PDEClass::ir_momTurb] = delta1s + fabs(z);
      sourcehubTrue[PDEClass::ir_keTurb] = theta11;
      sourcehubTrue[PDEClass::ir_amp] = theta11 * nt;
      sourcehubTrue[PDEClass::ir_lag] = varhb[PDEClass::ir_lag] - bc.hb_dummy_;
    }

    ArrayQ f = 0.0, sourcehub = 0.0;

    // Matching flux at a hubtrace
    bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( fTrue[i], f[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( sourcehubTrue[i], sourcehub[i], small_tol, close_tol );
    }

    // cumulation: matching flux from airfoil to wake
    bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( 2.0*fTrue[i], f[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( 2.0*sourcehubTrue[i], sourcehub[i], small_tol, close_tol );
    }
  }

  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                        = TransitionModelParams::params.profileCatDefault.turbulentBL;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    BCClassWakeMatch bc(pde, matchingType);

    const ProfileCategory profileCat = pde.getProfileCategory(var,x);
    const auto& paramInterpret_ = pde.getParamInterpreter();
    const Real qe = paramInterpret_.getqe(params);
    const Real rhoe = (pde.getGasModel()).density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));
    const Real nue = (pde.getViscosityModel()).dynamicViscosity() / rhoe;

    const auto& thicknessesCoefficientsFunctor
      = (pde.getSecondaryVarObj()).getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

    ArrayQ fTrue = 0.0; // zero by default
    {
      ArrayQ fx = 0.0, fz = 0.0;
      pde.fluxAdvective(params, e1, x, z, t, var, fx, fz);

      fTrue[PDEClass::ir_momLami] = 0.0;
      fTrue[PDEClass::ir_keLami] = 0.0;
      fTrue[PDEClass::ir_momTurb] = fx[PDEClass::ir_momTurb]*nx + fz[PDEClass::ir_momTurb]*nz;
      fTrue[PDEClass::ir_keTurb] = fx[PDEClass::ir_keTurb]*nx + fz[PDEClass::ir_keTurb]*nz;
      fTrue[PDEClass::ir_amp] = fx[PDEClass::ir_amp]*nx + fz[PDEClass::ir_amp]*nz;
      fTrue[PDEClass::ir_lag] = fx[PDEClass::ir_lag]*nx + fz[PDEClass::ir_lag]*nz;
    }

    ArrayQ sourcehubTrue = 0.0;
    {
      const Real delta1s = thicknessesCoefficientsFunctor.getdelta1s();
      const Real theta11 = thicknessesCoefficientsFunctor.gettheta11();

      sourcehubTrue[PDEClass::ir_momLami] = varhb[PDEClass::ir_momLami] - bc.hb_dummy_;
      sourcehubTrue[PDEClass::ir_keLami] = varhb[PDEClass::ir_keLami] - bc.hb_dummy_;
      sourcehubTrue[PDEClass::ir_momTurb] = delta1s + fabs(z);
      sourcehubTrue[PDEClass::ir_keTurb] = theta11;
      sourcehubTrue[PDEClass::ir_amp] = theta11 * nt;
      sourcehubTrue[PDEClass::ir_lag] = theta11 * ctau;
    }

    ArrayQ f = 0.0, sourcehub = 0.0;

    // Matching flux at a hubtrace
    bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( fTrue[i], f[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( sourcehubTrue[i], sourcehub[i], small_tol, close_tol );
    }

    // cumulation: matching flux from airfoil to wake
    bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( 2.0*fTrue[i], f[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( 2.0*sourcehubTrue[i], sourcehub[i], small_tol, close_tol );
    }
  }

  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                        = TransitionModelParams::params.profileCatDefault.turbulentWake;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    BCClassWakeMatch bc(pde, matchingType);

    ArrayQ f = 0.0, sourcehub = 0.0;

    BOOST_CHECK_THROW( bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);,
                       SANSException);

  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeWakeMatch_WakeInflow_fluxMatchingSourceHubtrace_test )
{

  const Real small_tol = 1e-15;
  const Real close_tol = 1.1E-13;

  const Real gam = 1.6;
  const Real R = 66.0;
  const Real mue = 7.0E-5;

  const auto& matchingType = BCParamType_WakeMatch::params.matchingType.wakeInflow;

  const Real x = 0.1;
  const Real z = 3.8;
  const Real t = 0.3;

  const Real deltaLami = 2.0e-2, ALami = -0.5, deltaTurb = 1.e-2, ATurb = 1.0, nt = 2.3, ctau = 1.2e-3;
  const Real qx = 4.0, qz = 3.0, p0 = 28.125, T0 = 4.6875;
  const Real qx_x = 2.3, qx_z = 1.5, qz_x = -0.3, qz_z = 0.6;

  VarInterpretType varInterpret;
  ArrayQ var = varInterpret.setDOFFrom<Real>(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));

  BOOST_CHECK( varInterpret.isValidState(var) );

  ArrayQ varhb = 0.0;
  varhb[PDEClass::ir_momLami] = -0.1;
  varhb[PDEClass::ir_keLami] = -0.2;
  varhb[PDEClass::ir_momTurb] = -0.3;
  varhb[PDEClass::ir_keTurb] = -0.4;
  varhb[PDEClass::ir_amp] = -0.5;
  varhb[PDEClass::ir_lag] = -0.6;

  const typename PDEClass::ParamInterpType paramInterpret;
  const ArrayParam params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const VectorX e1 = {0.6, 0.8};
  const VectorX nrm = {0.8, 0.6};
  const Real nx = nrm[0], nz = nrm[1];

  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = gam;
  gasModelDict[GasModelParamsIBL::params.R] = R;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(mue);

  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                        = TransitionModelParams::params.profileCatDefault.turbulentWake;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    BCClassWakeMatch bc(pde, matchingType);

    const ProfileCategory profileCat = pde.getProfileCategory(var,x);
    const auto& paramInterpret_ = pde.getParamInterpreter();
    const Real qe = paramInterpret_.getqe(params);
    const Real rhoe = (pde.getGasModel()).density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));
    const Real nue = (pde.getViscosityModel()).dynamicViscosity() / rhoe;

    const auto& thicknessesCoefficientsFunctor
      = (pde.getSecondaryVarObj()).getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

    ArrayQ fTrue = 0.0; // zero by default
    {
      ArrayQ fx = 0.0, fz = 0.0;
      pde.fluxAdvective(params, e1, x, z, t, var, fx, fz);

      fTrue[PDEClass::ir_momLami] = 0.0;
      fTrue[PDEClass::ir_keLami] = 0.0;
      fTrue[PDEClass::ir_momTurb] = varhb[PDEClass::ir_momTurb];
      fTrue[PDEClass::ir_keTurb] = varhb[PDEClass::ir_keTurb];
      fTrue[PDEClass::ir_amp] = varhb[PDEClass::ir_amp];
      fTrue[PDEClass::ir_lag] = varhb[PDEClass::ir_lag];
    }

    ArrayQ sourcehubTrue = 0.0;
    {
      const Real delta1s = thicknessesCoefficientsFunctor.getdelta1s();
      const Real theta11 = thicknessesCoefficientsFunctor.gettheta11();

      sourcehubTrue[PDEClass::ir_momLami] = varhb[PDEClass::ir_momLami] - bc.hb_dummy_;
      sourcehubTrue[PDEClass::ir_keLami] = varhb[PDEClass::ir_keLami] - bc.hb_dummy_;
      sourcehubTrue[PDEClass::ir_momTurb] = -delta1s; // delta1s is computed from MATLAB
      sourcehubTrue[PDEClass::ir_keTurb] = -theta11; // theta11 is computed from MATLAB
      sourcehubTrue[PDEClass::ir_amp] = -theta11 * nt;
      sourcehubTrue[PDEClass::ir_lag] = -theta11 * ctau;
    }

    ArrayQ f = 0.0, sourcehub = 0.0;

    // Matching flux at a hubtrace
    bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( fTrue[i], f[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( sourcehubTrue[i], sourcehub[i], small_tol, close_tol );
    }

    // cumulation: matching flux from airfoil to wake
    bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);
    for ( int i = 0; i < PDEClass::N; i++ )
    {
      SANS_CHECK_CLOSE( 2.0*fTrue[i], f[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( 2.0*sourcehubTrue[i], sourcehub[i], small_tol, close_tol );
    }
  }

  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                        = TransitionModelParams::params.profileCatDefault.laminarBL;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    BCClassWakeMatch bc(pde, matchingType);

    ArrayQ f = 0.0, sourcehub = 0.0;

    BOOST_CHECK_THROW( bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);,
                       SANSException);

  }

  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                        = TransitionModelParams::params.profileCatDefault.turbulentBL;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    BCClassWakeMatch bc(pde, matchingType);

    ArrayQ f = 0.0, sourcehub = 0.0;

    BOOST_CHECK_THROW( bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);,
                       SANSException);

  }

  {
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                        = TransitionModelParams::params.profileCatDefault.laminarWake;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    PDEClass pde(gasModel, viscosityModel, transitionModel);

    BCClassWakeMatch bc(pde, matchingType);

    ArrayQ f = 0.0, sourcehub = 0.0;

    BOOST_CHECK_THROW( bc.fluxMatchingSourceHubtrace(params,e1,x,z,t,nx,nz,var,varhb,f,sourcehub);,
                       SANSException);

  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
