// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEIBL2D_btest
//
// test of 2-D Integral boundary layer (IBL) PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/IBL/PDEIBL2D.h"

using namespace std;

namespace SANS
{
typedef PhysD2 PhysDim;
typedef VarData2DDANCt VarDataType;
typedef VarTypeDANCt VarType;
typedef PDEIBL<PhysDim,VarType> PDEClass;

typedef typename PDEClass::ThicknessesCoefficientsType::ClassParamsType ThicknessesCoefficientsParamsType;

typedef typename PDEClass::IBLTraitsType IBLTraitsType;

typedef typename PDEClass::VarInterpType VarInterpretType;
typedef typename PDEClass::ProfileCategory ProfileCategory;
typedef typename PDEClass::GasModelType GasModelType;
typedef typename PDEClass::ViscosityModelType ViscosityModelType;
typedef typename PDEClass::TransitionModelType TransitionModelType;

typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

typedef typename PDEClass::template ArrayParam<Real> ArrayParam;
typedef typename PDEClass::template MatrixParam<Real> MatrixParam;

typedef typename PDEClass::template VectorX<Real> VectorX;
typedef typename PDEClass::template TensorX<Real> TensorX;

typedef typename PDEClass::template ArrayParam<Real> ArrayParam;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
template class PDEIBL<PhysD2,VarType>;
}

namespace SANS
{
template<int N>
DLA::VectorS<N, double> fabs(const DLA::VectorS<N, double>& x)
{
  SANS::DLA::VectorS<N, double> y = x;
  for (int i = 0; i < N; ++i)
    y[i] = std::fabs(y[i]);
  return y;
}

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEIBL2D_continued_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic_test )
{
  const Real small_tol = 1.E-13;
  const Real close_tol = 1.1E-13;

  const Real gam = 1.6;
  const Real R = 66.0;
  const Real mue = 7.0E-5;

  const Real x = 0.1;
  const Real z = 3.8;
  const Real t = 0.3;

  const Real deltaLami = 2.0e-2, ALami = -0.5, deltaTurb = 1.e-2, ATurb = 1.0, nt = 2.3, ctau = 1e-3;

  const Real qx_x = 2.5, qx_z = 5.2, qz_x = 3.6, qz_z = 6.6, p0 = 28.125, T0 = 4.6875;

  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = gam;
  gasModelDict[GasModelParamsIBL::params.R] = R;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(mue);
  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const TransitionModelType transitionModel(transitionModelDict);

  PDEClass pde(gasModel, viscosityModel, transitionModel);

  const auto paramInterpret = pde.getParamInterpreter();

  const ArrayQ var = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));

  {
    const Real qx = 4.0, qz = 3.0;

    const ArrayParam params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

    Real speed = 0.0;

    pde.speedCharacteristic(params, x, z, t, var, speed);

    SANS_CHECK_CLOSE( paramInterpret.getqe(params), speed, small_tol, close_tol );
  }

  {
    const Real qx = 0.0, qz = 0.0;

    const ArrayParam params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

    Real speed = 0.0;

    pde.speedCharacteristic(params, x, z, t, var, speed);

    SANS_CHECK_CLOSE( 1.0, speed, small_tol, close_tol );
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState_test )
{
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(1.827e-5);
  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const TransitionModelType transitionModel(transitionModelDict);

  PDEClass pde(gasModel, viscosityModel, transitionModel);

  // verify valid state
  {
    const Real deltaLami = 1.0, ALami = 1.0, deltaTurb = 1.0, ATurb = 1.0, nt = 0.1, ctau = 1e-3;
    BOOST_CHECK( pde.isValidState(pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau))) );
  }

  // catch invalid delta alone
  {
    const Real deltaLami = -1.0, ALami = 1.0, deltaTurb = 1.0, ATurb = 1.0, nt = 0.1, ctau = 1e-3;
    BOOST_CHECK( !pde.isValidState(pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau))) );
  }

  {
    const Real deltaLami = 0.0, ALami = 1.0, deltaTurb = 1.0, ATurb = 1.0, nt = 0.1, ctau = 1e-3;
    BOOST_CHECK( !pde.isValidState(pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau))) );
  }

  {
    const Real deltaLami = 1.0, ALami = 1.0, deltaTurb = -1.0, ATurb = 1.0, nt = 0.1, ctau = 1e-3;
    BOOST_CHECK( !pde.isValidState(pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau))) );
  }

  {
    const Real deltaLami = 1.0, ALami = 1.0, deltaTurb = 0.0, ATurb = 1.0, nt = 0.1, ctau = 1e-3;
    BOOST_CHECK( !pde.isValidState(pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau))) );
  }

  // catch invalid ctau alone
  {
    const Real deltaLami = 1.0, ALami = 1.0, deltaTurb = 1.0, ATurb = 1.0, nt = 0.1, ctau = -1e-3;
    BOOST_CHECK( !pde.isValidState(pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau))) );
  }

  {
    const Real deltaLami = 1.0, ALami = 1.0, deltaTurb = 1.0, ATurb = 1.0, nt = 0.1, ctau = 0.0;
    BOOST_CHECK( !pde.isValidState(pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau))) );
  }

  // catch invalid delta and ctau
  {
    const Real deltaLami = -1.0, ALami = 1.0, deltaTurb = -1.0, ATurb = 1.0, nt = 0.1, ctau = -1e-3;
    BOOST_CHECK( !pde.isValidState(pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau))) );
  }
}

//----------------------------------------------------------------------------//
// TODO: more elaborate updateFraction tests to be added
BOOST_AUTO_TEST_CASE( updateFraction_laminarBL_test )
{
  // construct pde object
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(1.827e-5);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                      = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const TransitionModelType transitionModel(transitionModelDict);

  PDEClass pde(gasModel, viscosityModel, transitionModel);

  // test
  const Real small_tol = 1e-15;
  const Real close_tol = 1e-15;

  const Real x = 0.1;
  const Real z = 3.8;
  const Real t = 0.3;

  const Real qx = 1.0, qz = 0.3, qx_x = 2.5, qx_z = 5.2, qz_x = 3.6, qz_z = 6.6, p0 = 28.125, T0 = 4.6875;
  const auto paramInterpret = pde.getParamInterpreter();
  const ArrayParam params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const Real deltaLami = 2.0e-2, ALami = -0.5, deltaTurb = 1.e-2, ATurb = 1.0, nt = 2.3, ctau = 1e-3;
  const ArrayQ var = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));

  const Real maxChangeFraction = 1.0;

  {
    const ArrayQ dvar = -0.1 * fabs(var); // no under-relaxation required
    Real updateFraction = 2.0;
    pde.updateFraction(params, x, z, t, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -0.1 * fabs(var); // no under-relaxation required
    Real updateFraction = 0.5;
    pde.updateFraction(params, x, z, t, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(0.5, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -3.0 * fabs(var); // overshoot
    Real updateFraction = 1.0;
    pde.updateFraction(params, x, z, t, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(2.0/3.0, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -3.0 * fabs(var); // overshoot
    Real updateFraction = 1./3.;
    pde.updateFraction(params, x, z, t, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1./3., updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = +0.95 * fabs(var); // undershoot
    Real updateFraction = 1.0;
    pde.updateFraction(params, x, z, t, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE((-0.9)/(-0.95), updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = +0.95 * fabs(var); // undershoot
    Real updateFraction = 0.8;
    pde.updateFraction(params, x, z, t, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(0.8, updateFraction, small_tol, close_tol);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( updateFraction_turbulentBL_test )
{
  // construct pde object
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(1.827e-5);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                      = TransitionModelParams::params.profileCatDefault.turbulentBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const TransitionModelType transitionModel(transitionModelDict);

  PDEClass pde(gasModel, viscosityModel, transitionModel);

  // test
  const Real small_tol = 1e-15;
  const Real close_tol = 1e-15;

  const Real x = 0.1;
  const Real z = 3.8;
  const Real t = 0.3;

  const Real qx = 1.0, qz = 0.3, qx_x = 2.5, qx_z = 5.2, qz_x = 3.6, qz_z = 6.6, p0 = 28.125, T0 = 4.6875;
  const auto paramInterpret = pde.getParamInterpreter();
  const ArrayParam params = paramInterpret.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);

  const Real deltaLami = 2.0e-2, ALami = -0.5, deltaTurb = 1.e-2, ATurb = 1.0, nt = 2.3, ctau = 1e-3;
  const ArrayQ var = pde.setDOFFrom(VarDataType(deltaLami,ALami,deltaTurb,ATurb,nt,ctau));

  const Real maxChangeFraction = 1.0;

  {
    const ArrayQ dvar = -0.1 * fabs(var); // no under-relaxation required
    Real updateFraction = 2.0;
    pde.updateFraction(params, x, z, t, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1.0, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -0.1 * fabs(var); // no under-relaxation required
    Real updateFraction = 0.5;
    pde.updateFraction(params, x, z, t, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(0.5, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -3.0 * fabs(var); // overshoot
    Real updateFraction = 1.0;
    pde.updateFraction(params, x, z, t, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(2.0/3.0, updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = -3.0 * fabs(var); // overshoot
    Real updateFraction = 1./3.;
    pde.updateFraction(params, x, z, t, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(1./3., updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = +0.95 * fabs(var); // undershoot
    Real updateFraction = 1.0;
    pde.updateFraction(params, x, z, t, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE((-0.9)/(-0.95), updateFraction, small_tol, close_tol);
  }

  {
    const ArrayQ dvar = +0.95 * fabs(var); // undershoot
    Real updateFraction = 0.8;
    pde.updateFraction(params, x, z, t, var, dvar, maxChangeFraction, updateFraction);
    SANS_CHECK_CLOSE(0.8, updateFraction, small_tol, close_tol);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( interpResidualStuff_test )
{
  const Real tol = 1.e-15;

  const int N = PDEClass::N;

  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(1.827e-5);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                      = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const TransitionModelType transitionModel(transitionModelDict);

  PDEClass pde(gasModel, viscosityModel, transitionModel);

  BOOST_CHECK( pde.category() == PDEClass::IBL_ResidInterp_Raw );
  BOOST_CHECK( N == pde.nMonitor() );

  // PDE
  ArrayQ rsdPDEIn;
  for (int n = 0; n < N; ++n)
    rsdPDEIn[n] = static_cast<Real>(n) + 1.0;

  DLA::VectorD<Real> rsdPDEOut(N);

  pde.interpResidVariable(rsdPDEIn, rsdPDEOut);

  for (int n = 0; n < N; n++ )
    SANS_CHECK_CLOSE(rsdPDEIn[n], rsdPDEOut[n], tol, tol);

  // BC
  ArrayQ rsdBCIn;
  for (int n = 0; n < N; ++n)
    rsdBCIn[n] = static_cast<Real>(n) + 1.0;

  DLA::VectorD<Real> rsdBCOut(N);

  pde.interpResidBC(rsdBCIn, rsdBCOut);

  for (int n = 0; n < N; n++ )
    SANS_CHECK_CLOSE(rsdBCIn[n], rsdBCOut[n], tol, tol);

  // Aux
  ArrayQ rsdAuxIn;
  for (int n = 0; n < N; ++n)
    rsdAuxIn[n] = static_cast<Real>(n) + 1.0;

  DLA::VectorD<Real> rsdAuxOut(N);

  pde.interpResidGradient(rsdAuxIn, rsdAuxOut);

  for (int n = 0; n < N; n++ )
    SANS_CHECK_CLOSE(rsdPDEIn[n], rsdAuxOut[n], tol, tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( interpResidual_Exception_test )
{
  Real gam = 1.2;
  Real R = 2875;
  Real mue = 1.827e-03;

#if 1 // throw exception in constructor
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = gam;
  gasModelDict[GasModelParamsIBL::params.R] = R;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(mue);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                      = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const TransitionModelType transitionModel(transitionModelDict);

  BOOST_CHECK_THROW( PDEClass pde(gasModel, viscosityModel, transitionModel, PyDict(),
                                  PDEClass::IBLResidualInterpCategory(1) );, // 1 is out of the enum range
                     SANSException );
#else // throw exception in methods
//  BOOST_CHECK( IBL2D.category() != PDEClass::IBL_ResidInterp_Raw );
//  BOOST_CHECK_THROW( IBL2D.nMonitor();, SANSException );
//
//  const int N = PDEClass::N;
//
//  // PDE
//  ArrayQ rsdPDEIn = { 1, 2 };
//  DLA::VectorD<Real> rsdPDEOut(N);
//
//  BOOST_CHECK_THROW( IBL2D.interpResidVariable(rsdPDEIn, rsdPDEOut);, SANSException );
//
//  // BC
//  ArrayQ rsdBCIn = { 1, 2 };
//  DLA::VectorD<Real> rsdBCOut(N);
//
//
//  BOOST_CHECK_THROW( IBL2D.interpResidBC(rsdBCIn, rsdBCOut);, SANSException );
//
//  // Aux
//  ArrayQ rsdAuxIn = { 1, 2 };
//  DLA::VectorD<Real> rsdAuxOut(N);
//
//  BOOST_CHECK_THROW( IBL2D.interpResidGradient(rsdAuxIn, rsdAuxOut);, SANSException );
#endif
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO_test )
{
  Real gam = 1.2;
  Real R = 2875;
  Real mue = 1.827e-03;

  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = gam;
  gasModelDict[GasModelParamsIBL::params.R] = R;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelType gasModel(gasModelDict);

  ViscosityModelType viscosityModel(mue);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                      = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const TransitionModelType transitionModel(transitionModelDict);

  PDEClass pde(gasModel, viscosityModel, transitionModel);

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/IBL/PDEIBL2D_pattern.txt", true );

  pde.dump( 1, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
