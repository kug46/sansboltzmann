// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// SetVelDofCell_IBL_btest

//#define DEBUG_PRINT

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/tools/for_each_CellGroup.h"

#include "pde/IBL/SetVelDofCell_IBL2D.h"

#include "unit/UnitGrids/XField2D_Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Line_X1_1Group.h"

using namespace std;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct
namespace SANS
{
}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( SetVelocityDofCell_IBL_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_ctor_test )
{
  // true
  const int D = 2;

  // check
  BOOST_CHECK( SetVelocityDofCell_IBL2D::D == D );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( readVelocity_test )
{
  const Real tol = 1.e-13;

  typedef SetVelocityDofCell_IBL2D FunctorType;
  std::vector<Real> dataVel;

  // read a non-existent file
  BOOST_CHECK_THROW( FunctorType::readVelocity( "FileNotExistent", dataVel );, SANSException );

  // read an existing file
  const std::string filename = "IO/pde/IBL/EIFvelData_nelem32.txt";

  FunctorType::readVelocity( filename, dataVel );

  BOOST_CHECK_EQUAL( 33, static_cast<int>(dataVel.size()) );

  // 1st node
  SANS_CHECK_CLOSE( -1.53902, dataVel[0], tol, tol );

  // 26th node
  SANS_CHECK_CLOSE( 1.63590, dataVel[25], tol, tol );

  // 33rd node
  SANS_CHECK_CLOSE( 1.53902, dataVel[32], tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SetVelDofCell_IBL_X1Q1_1Line_test )
{
  const Real tol = 1.e-13;

  typedef PhysD2 PhysDim;
  typedef SetVelocityDofCell_IBL2D FunctorType;
  typedef typename FunctorType::VectorX VectorX;

  // Read in EIF data from file
  std::vector<Real> dataVel = {1.0, 2.0};

  BOOST_CHECK_EQUAL( 2, static_cast<int>(dataVel.size()) );

  // set up grid
  VectorX e1 = {0.8, 0.6};
  VectorX X0 = {0.0, 0.0};
  VectorX X1 = X0 + e1;

  XField2D_1Line_X1_1Group xfld(X0,X1);

  // solution: P1 (aka Q1), which spans the space of the Linear solution
  // the check below assumes Hierarchical basis
  const int order = 1;
  Field_DG_Cell<PhysDim, TopoD1, VectorX> vfld(xfld, order, BasisFunctionCategory_Hierarchical);
  Field_DG_Cell<PhysDim, TopoD1, VectorX> vxXfld(xfld, order, BasisFunctionCategory_Hierarchical);
  Field_DG_Cell<PhysDim, TopoD1, VectorX> vzXfld(xfld, order, BasisFunctionCategory_Hierarchical);

  for_each_CellGroup<TopoD1>::apply( SetVelocityDofCell_IBL2D(dataVel, {0}), (vfld, vxXfld, vzXfld, xfld) );

  // Check
  const XField2D_Line_X1_1Group::template FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);
  const Field<PhysDim, TopoD1, VectorX>::template FieldCellGroupType<Line>& VelfldCell = vfld.getCellGroup<Line>(0);
  const Field<PhysDim, TopoD1, VectorX>::template FieldCellGroupType<Line>& VelxXfldCell = vxXfld.getCellGroup<Line>(0);
  const Field<PhysDim, TopoD1, VectorX>::template FieldCellGroupType<Line>& VelzXfldCell = vzXfld.getCellGroup<Line>(0);

  typedef XField2D_Line_X1_1Group::template FieldCellGroupType<Line>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysDim, TopoD1, VectorX>::template FieldCellGroupType<Line>::template ElementType<> ElementVelFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementVelFieldClass VelfldElem( VelfldCell.basis() );
  ElementVelFieldClass VelxXfldElem( VelxXfldCell.basis() );
  ElementVelFieldClass VelzXfldElem( VelzXfldCell.basis() );

  BOOST_REQUIRE_EQUAL( xfldCell.nElem(), VelfldCell.nElem() );

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for ( int elem = 0; elem < nelem; elem++ )
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    VelfldCell.getElement( VelfldElem, elem );
    VelxXfldCell.getElement( VelxXfldElem, elem );
    VelzXfldCell.getElement( VelzXfldElem, elem );

    BOOST_REQUIRE_EQUAL(xfldElem.nDOF(), order+1);
    BOOST_REQUIRE_EQUAL(xfldElem.nDOF(), VelfldElem.nDOF());
    BOOST_REQUIRE_EQUAL(xfldElem.nDOF(), VelxXfldElem.nDOF());
    BOOST_REQUIRE_EQUAL(xfldElem.nDOF(), VelzXfldElem.nDOF());

    VectorX velTrue[2];  // at sRef = {0,1}
    VectorX velxXTrue;  // at sRef = {0,1}
    VectorX velzXTrue;  // at sRef = {0,1}
    VectorX vel_mid;     // at sRef 0.5

    // Check trace nodes
    Real sRef[] = { 0.0, 1.0 };

    BOOST_REQUIRE_EQUAL(VelfldElem.nDOF(), 2);
    for ( int n = 0; n < VelfldElem.nDOF(); n++ )
    {
      VectorX e0;
      xfldElem.unitTangent( sRef[n], e0 );

      VectorX vel = VelfldElem.eval(sRef[n]);

      int indx = elem * order + n;

      velTrue[n] = { dataVel[indx] * e0[0],
                     dataVel[indx] * e0[1] };

      for ( int ivel = 0; ivel < PhysDim::D; ivel++ )
      {
        SANS_CHECK_CLOSE( velTrue[n][ivel], vel[ivel], tol, tol );
      }

#ifdef DEBUG_PRINT
      std::cout << "Element " << elem << " node " << n << ": "
                << velTrue[n][0] << " " << velTrue[n][1] << " "
                << velTrue[n][2] << " " << velTrue[n][3] << std::endl;
#endif
    }

    velxXTrue = { 0.64, 0.48 }; // TODO: General formula to be derived
    velzXTrue = { 0.48, 0.36 };

//    for ( int n = 0; n < VelfldElem.nDOF(); n++ )
    for ( int n = 0; n < 1; n++ )
    {
      VectorX velx_X = VelxXfldElem.eval(sRef[n]);
      VectorX velz_X = VelzXfldElem.eval(sRef[n]);

      for ( int ivel = 0; ivel < PhysDim::D; ivel++ )
      {
        SANS_CHECK_CLOSE( velxXTrue[ivel], velx_X[ivel], tol, tol );
        SANS_CHECK_CLOSE( velzXTrue[ivel], velz_X[ivel], tol, tol );
      }
    }

    // Check mid node
    vel_mid = VelfldElem.eval( 0.5 );

    for ( int ivel = 0; ivel < PhysDim::D; ivel++ )
    {
      SANS_CHECK_CLOSE( 0.5*( velTrue[0][ivel] + velTrue[1][ivel] ),  // averaging is valid for linear solution
                        vel_mid[ivel], tol, tol );
    }

  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SetVelDofCell_IBL_X1Q1_airfoil_test )
{
  const Real tol = 1.e-13;

  typedef PhysD2 PhysDim;
  typedef SetVelocityDofCell_IBL2D FunctorType;
  typedef typename FunctorType::VectorX VectorX;

  // Read in EIF data from file
  std::vector<Real> dataVel;
  const std::string filenameVelocity = "IO//pde/IBL/EIFvelData_nelem32.txt";

  FunctorType::readVelocity( filenameVelocity, dataVel );

  BOOST_CHECK_EQUAL( 33, static_cast<int>(dataVel.size()) );

  // Read in grid from file
  const std::string Gridfilename = "IO//pde/IBL/naca0004_nelem32.txt"; // exported from XFoil (without label)
  std::vector<VectorX> coordinates;
  XField2D_Line_X1_1Group::readXFoilGrid(Gridfilename, coordinates);
  XField2D_Line_X1_1Group xfld(coordinates);

  // solution: P1 (aka Q1), which spans the space of the Linear solution
  // the check below assumes Hierarchical basis
  const int order = 1;
  Field_DG_Cell<PhysDim, TopoD1, VectorX> vfld(xfld, order, BasisFunctionCategory_Hierarchical);
  Field_DG_Cell<PhysDim, TopoD1, VectorX> vxXfld(xfld, order, BasisFunctionCategory_Hierarchical);
  Field_DG_Cell<PhysDim, TopoD1, VectorX> vzXfld(xfld, order, BasisFunctionCategory_Hierarchical);

  for_each_CellGroup<TopoD1>::apply( SetVelocityDofCell_IBL2D(dataVel, {0}), (vfld, vxXfld, vzXfld, xfld) );

  // Check
  const XField2D_Line_X1_1Group::template FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);
  const Field<PhysDim, TopoD1, VectorX>::template FieldCellGroupType<Line>& VelfldCell = vfld.getCellGroup<Line>(0);

  typedef XField2D_Line_X1_1Group::template FieldCellGroupType<Line>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysDim, TopoD1, VectorX>::template FieldCellGroupType<Line>::template ElementType<> ElementVelFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementVelFieldClass VelfldElem( VelfldCell.basis() );

  BOOST_REQUIRE_EQUAL( xfldCell.nElem(), VelfldCell.nElem() );

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for ( int elem = 0; elem < nelem; elem++ )
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    VelfldCell.getElement( VelfldElem, elem );

    BOOST_REQUIRE_EQUAL(xfldElem.nDOF(), order+1);
    BOOST_REQUIRE_EQUAL(xfldElem.nDOF(), VelfldElem.nDOF());

    VectorX velTrue[2];  // at sRef = {0,1}
    VectorX vel_mid;     // at sRef 0.5

    // Check trace nodes
    Real sRef[] = { 0.0, 1.0 };


    for ( int n = 0; n < VelfldElem.nDOF(); n++ )
    {
      VectorX e0;
      xfldElem.unitTangent( sRef[n], e0 );

      VectorX vel = VelfldElem.eval(sRef[n]);

      int indx = elem * order + n;

      velTrue[n] = { dataVel[indx] * e0[0],
                     dataVel[indx] * e0[1] };

      for ( int ivel = 0; ivel < PhysDim::D; ivel++ )
      {
        SANS_CHECK_CLOSE( velTrue[n][ivel], vel[ivel], tol, tol );
      }

#if 0
      std::cout << "Element " << elem << " node " << n << ": "
                << velTrue[n][0] << " " << velTrue[n][1] << " "
                << velTrue[n][2] << " " << velTrue[n][3] << std::endl;
#endif
    }

    // Check mid node
    vel_mid = VelfldElem.eval( 0.5 );

    for ( int ivel = 0; ivel < PhysDim::D; ivel++ )
    {
      SANS_CHECK_CLOSE( 0.5*( velTrue[0][ivel] + velTrue[1][ivel] ),  // averaging is valid for linear solution
                        vel_mid[ivel], tol, tol );
    }

  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
