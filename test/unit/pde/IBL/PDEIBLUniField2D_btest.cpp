// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/IBL/PDEIBLUniField.h"

using namespace std;

namespace SANS
{
typedef PhysD2 PhysDim;
typedef VarTypeDAG VarType;
typedef PDEIBLUniField<PhysDim,VarType> PDEClass;

typedef typename PDEClass::IBLTraitsType IBLTraitsType;

typedef typename PDEClass::VarInterpType VarInterpretType;
typedef typename PDEClass::ProfileCategory ProfileCategory;
typedef typename PDEClass::GasModelType GasModelType;
typedef typename PDEClass::ViscosityModelType ViscosityModelType;
typedef typename PDEClass::TransitionModelType TransitionModelType;

typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

typedef typename PDEClass::template ArrayParam<Real> ArrayParam;
typedef typename PDEClass::template MatrixParam<Real> MatrixParam;

typedef typename PDEClass::template VectorX<Real> VectorX;
typedef typename PDEClass::template TensorX<Real> TensorX;

typedef typename PDEClass::template ArrayParam<Real> ArrayParam;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
template class PDEIBLUniField<PhysDim,VarType>;
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEIBLUniField2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef IBLTraits<PhysDim, IBLUniField> IBLTraitsType;
  BOOST_CHECK( (std::is_same<typename PDEClass::IBLTraitsType, IBLTraitsType>::value) );

  BOOST_CHECK( PDEClass::D == PhysDim::D );
  BOOST_CHECK( PDEClass::N == IBLTraitsType::N );
  BOOST_CHECK( PDEClass::Nparam == IBLTraitsType::Nparam );

  BOOST_CHECK( PDEClass::ir_mom == 0 );
  BOOST_CHECK( PDEClass::ir_ke == 1 );
  BOOST_CHECK( PDEClass::ir_amplag == 2 );

  BOOST_CHECK( ArrayQ::M == PDEClass::N );
  BOOST_CHECK( MatrixQ::M == PDEClass::N );
  BOOST_CHECK( MatrixQ::N == PDEClass::N );

  BOOST_CHECK( ArrayParam::M == PDEClass::Nparam );
  BOOST_CHECK( MatrixParam::M == PDEClass::N );
  BOOST_CHECK( MatrixParam::N == PDEClass::Nparam );

  BOOST_CHECK( VectorX::M == PDEClass::D );
  BOOST_CHECK( TensorX::M == PDEClass::D );
  BOOST_CHECK( TensorX::N == PDEClass::D );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctorsANDbooleanANDmiscellaneous_test )
{
  const Real gam = 1.6, R = 28.715, mue = 7.0E-5;

  {
    // models
    PyDict gasModelDict;
    gasModelDict[GasModelParamsIBL::params.gamma] = gam;
    gasModelDict[GasModelParamsIBL::params.R] = R;
    GasModelParamsIBL::checkInputs(gasModelDict);
    const GasModelType gasModel(gasModelDict);

    ViscosityModelType viscosityModel(mue);

    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.ntinit] = 0.0;
    transitionModelDict[TransitionModelParams::params.ntcrit] = 9.0;
    transitionModelDict[TransitionModelParams::params.xtr] = 10.0;
    transitionModelDict[TransitionModelParams::params.isTransitionActive] = true;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
                             = TransitionModelParams::params.profileCatDefault.laminarBL;
    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
    const TransitionModelType transitionModel(transitionModelDict);

    // constructor
    PDEClass pde(gasModel, viscosityModel, transitionModel);

    // hasFlux functions
    BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
    BOOST_CHECK( pde.hasFluxAdvective() == true );
    BOOST_CHECK( pde.hasFluxViscous() == false );
    BOOST_CHECK( pde.hasSource() == true );
    BOOST_CHECK( pde.hasSourceTrace() == false );
    BOOST_CHECK( pde.hasSourceLiftedQuantity() == false );
    BOOST_CHECK( pde.hasForcingFunction() == false );

    BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

    // accessor functions
    auto& viscosityModel_ = pde.getViscosityModel();
    BOOST_CHECK(typeid(ViscosityModelType&) == typeid(viscosityModel_) );

    auto& transitionModel_ = pde.getTransitionModel();
    BOOST_CHECK(typeid(TransitionModelType&) == typeid(transitionModel_) );

    auto varInterpret_ = pde.getVarInterpreter();
    BOOST_CHECK(typeid(VarInterpretType&) == typeid(varInterpret_) );

    auto& thicknessesCoefficientsObj = pde.getSecondaryVarObj();
    BOOST_CHECK(typeid(typename PDEClass::ThicknessesCoefficientsType&) == typeid(thicknessesCoefficientsObj) );

    Real gam_, R_, mue_;
    pde.evalParam( gam_, R_, mue_ );
    BOOST_CHECK_EQUAL(gam, gam_);
    BOOST_CHECK_EQUAL(R, R_);
    BOOST_CHECK_EQUAL(mue, mue_);
  }
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
