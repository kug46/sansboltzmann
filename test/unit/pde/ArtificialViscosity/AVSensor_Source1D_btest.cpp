// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AVSensor_Source1D_btest
// test for the 1-D source class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "pde/PorousMedia/Q1DPrimitive_pnSw.h"
#include "pde/PorousMedia/TraitsTwoPhaseArtificialViscosity.h"
#include "pde/PorousMedia/PDETwoPhase_ArtificialViscosity1D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/CapillaryModel.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_Source1D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor1D.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( AVSensor_Source1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AVSensor_Source1D_TwoPhase_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef QTypePrimitive_pnSw QType;

  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, Real,
                              CapillaryModel> TraitsModelTwoPhaseClass;

  typedef PDETwoPhase_ArtificialViscosity1D<TraitsSizeTwoPhaseArtificialViscosity,
                                            TraitsModelTwoPhaseClass> PDEClass;

  typedef AVSensor_Source1D_TwoPhase<PDEClass> SensorSource;

  typedef SensorSource::ArrayQ<Real> ArrayQ;
  typedef SensorSource::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  // PDE
  const Real pref = 14.7;

  DensityModel rhow(62.4, 5.0e-6, pref);
  DensityModel rhon(52.1, 1.5e-5, pref);

  PorosityModel phi(0.3, 3.0e-6, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  CapillaryModel pc(2);
  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real Kxx = conversion*200; //mD

  int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEClass pde(order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, Kxx, pc);

  SensorSource source_sensor(pde);

  BOOST_CHECK( source_sensor.hasSourceTerm() == true );
  BOOST_CHECK( source_sensor.hasSourceTrace() == false );
  BOOST_CHECK( source_sensor.hasSourceLiftedQuantity() == true );
  BOOST_CHECK( source_sensor.needsSolutionGradientforSource() == true );

  Real x = 1;
  Real time = 0;

  Real qsL = 2.45;
  Real qsR = 3.45;
  Real qsxL = 5.0;
  Real qsxR = 3.0;

  ArrayQ qL = {2500.0, 0.5, qsL};
  ArrayQ qR = {2510.0, 0.6, qsR};

  ArrayQ qxL = {0.341, 0.83, qsxL};
  ArrayQ qxR = {0.523, -0.21, qsxR};

  DLA::MatrixSymS<PhysD1::D,Real> H = {{0.13}};
  DLA::MatrixSymS<PhysD1::D,Real> param = log(H);
  Real liftedQuantity = 0.043;

  Real Sk = log10(liftedQuantity + 1e-16);
  Real Sk_min = -4;
  Real Sk_max = -1;
  Real xi = (Sk - Sk_min)/(Sk_max - Sk_min);

//    Real sensor = smoothActivation_tanh(xi);
  Real alpha = 10;
  Real sensor = smoothActivation_exp(xi, alpha);

  Real nu_max;
  pde.artViscMax(param, x, time, qL, qxL, nu_max);

  ArrayQ sTrue = {0.0, 0.0, qsL - sensor*nu_max};
  ArrayQ s = 0.0;

  BOOST_CHECK_THROW( source_sensor.source(pde, param, x, time, qL, qxL, s), DeveloperException );

  s = 0.0;
  source_sensor.source(pde, param, x, time, liftedQuantity, qL, qxL, s);
  BOOST_CHECK_EQUAL( sTrue(0), s(0) );
  BOOST_CHECK_EQUAL( sTrue(1), s(1) );
  BOOST_CHECK_EQUAL( sTrue(2), s(2) );

  Real liftedQuantityTrue = qL(1) - qR(1);
  liftedQuantityTrue = smoothabs0(liftedQuantityTrue, 1.0e-3);

  liftedQuantity = 0;
  source_sensor.sourceLiftedQuantity(param, x, param, x, time, qL, qR, liftedQuantity);
  SANS_CHECK_CLOSE( liftedQuantityTrue, liftedQuantity, small_tol, close_tol );

  ArrayQ sLTrue = {0.0, 0.0, 0.0};
  ArrayQ sRTrue = {0.0, 0.0, 0.0};
  ArrayQ sL = 0, sR = 0;
  source_sensor.sourceTrace(pde, param, x, param, x, time, qL, qxL, qR, qxR, sL, sR);
  SANS_CHECK_CLOSE( sLTrue(0), sL(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( sLTrue(1), sL(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( sLTrue(2), sL(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue(0), sR(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue(1), sR(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue(2), sR(2), small_tol, close_tol );

  MatrixQ dsduTrue = {{0.0, 0.0, 0.0},
                      {0.0, 0.0, 0.0},
                      {0.0, 0.0, 1.0}};
  MatrixQ dsdu = 0.0;

  source_sensor.jacobianSource(pde, param, x, time, qL, qxL, dsdu);
  SANS_CHECK_CLOSE( dsduTrue(0,0), dsdu(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( dsduTrue(0,1), dsdu(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( dsduTrue(0,2), dsdu(0,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( dsduTrue(1,0), dsdu(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( dsduTrue(1,1), dsdu(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( dsduTrue(1,2), dsdu(1,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( dsduTrue(2,0), dsdu(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( dsduTrue(2,1), dsdu(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( dsduTrue(2,2), dsdu(2,2), small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
