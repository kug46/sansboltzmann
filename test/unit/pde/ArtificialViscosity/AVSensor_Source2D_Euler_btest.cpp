// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AVSensor_Source2D_Euler_btest
// test for the 2-D source class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/PDEEulermitAVDiffusion2D.h"
#include "pde/NS/BCEulermitAVSensor2D.h"
#include "pde/NS/Fluids2D_Sensor.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( AVSensor_Source2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AVSensor_Source2D_Jump_test )
{
  typedef TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
  typedef AVSensor_Source2D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;
  typedef AVPDEClass::ArrayQ<Real> ArrayQ;
  typedef AVPDEClass::MatrixQ<Real> MatrixQ;
  typedef DLA::MatrixSymS<PhysD2::D,Real> MatrixSym;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  int order = 1;

  // Gas Model
  Real gamma, R;
  gamma = 1.4;
  R = 1;
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;
  GasModel gas(gasModelDict);
  // PDE
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  EulerResidualInterpCategory interp = Euler_ResidInterp_Momentum;
  PDEBaseClass pdeEulerAV(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, eHarten);
  // Sensor equation terms
  Sensor sensor(pdeEulerAV);
  SensorAdvectiveFlux sensor_adv(0.0, 0.0);
  SensorViscousFlux sensor_visc(order);
  SensorSource sensor_source(order, sensor);
  // AV PDE with sensor equation
  AVPDEClass avpde(sensor_adv, sensor_visc, sensor_source, isSteady, order, hasSpaceTimeDiffusion,
                   EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, eHarten);

  BOOST_CHECK( sensor_source.hasSourceTerm() == true );
  BOOST_CHECK( sensor_source.hasSourceTrace() == true );
  BOOST_CHECK( sensor_source.hasSourceLiftedQuantity() == true );
  BOOST_CHECK( sensor_source.needsSolutionGradientforSource() == false );

  Real x = 1, y = 0.5;
  Real time = 0;

  Real qsL = 2.45;
  Real qsR = 3.45;
  Real qsxL = 5.0;
  Real qsxR = 3.0;
  Real qsyL = -1.0;
  Real qsyR = 2.0;

  ArrayQ qxL = {0.31, -0.21, 0.16, 0.67, qsxL};
  ArrayQ qyL = {-0.24, -0.73, 0.56, 0.81, qsyL};
  ArrayQ qxR = {0.31, -0.21, 0.16, 0.67, qsxR};
  ArrayQ qyR = {-0.24, -0.73, 0.56, 0.81, qsyR};

  Real rho = 1.137, u = 0.784, v = -0.231, pL = 2.45, pR = 2.75;
  ArrayQ qL = 0, qR = 0;
  AVVariable<DensityVelocityPressure2D, Real> varL( DensityVelocityPressure2D<Real>(rho, u, v, pL), qsL );
  avpde.setDOFFrom( qL, varL );
  AVVariable<DensityVelocityPressure2D, Real> varR( DensityVelocityPressure2D<Real>(rho, u, v, pR), qsR );
  avpde.setDOFFrom( qR, varR );


  Real hxx = 0.461, hyy = 0.912;
  MatrixSym logH = {{log(hxx)},
                    {0.0, log(hyy)}};
  MatrixSym param = logH;

  Real liftedQuantity = 0.0083;
  Real Sk = log10(liftedQuantity + 1e-16);
  //Real psi0 = 2.25 + 3.0*log10(order);
  Real psi0 = 3.0 + 2.0*log10(order);
  Real xi = Sk + psi0;
#if 0
  xi += 0.5; // Add +0.5 to center around smooth activiation
  Real sensor_value = smoothActivation_tanh(xi);
#else
  xi += 0.5; // Add +0.5 to center around smooth activiation
  xi /= 2;   // Scale so it activates over 2 orders of magnitude
  Real alpha = 10;
  Real sensor_value = smoothActivation_exp(xi, alpha);
#endif
  ArrayQ sTrue = {0, 0, 0, 0, qsL - sensor_value};
  ArrayQ s = {0, 0, 0, 0, 0};

  BOOST_CHECK_THROW( sensor_source.source(avpde, param, x, y, time, qL, qxL, qyL, s), DeveloperException );

  sensor_source.source(avpde, param, x, y, time, liftedQuantity, qL, qxL, qyL, s);
  BOOST_CHECK_EQUAL( sTrue(0), s(0) );
  BOOST_CHECK_EQUAL( sTrue(1), s(1) );
  BOOST_CHECK_EQUAL( sTrue(2), s(2) );
  BOOST_CHECK_EQUAL( sTrue(3), s(3) );
  BOOST_CHECK_EQUAL( sTrue(4), s(4) );

  Real dg = pL - pR;
  dg = smoothabs0(dg, 1.0e-5);
  Real gbar = 0.5*(fabs(pL) + fabs(pR));

  Real liftedQuantityTrue = dg / gbar;
  liftedQuantityTrue = smoothabs0(liftedQuantityTrue, 0.03);

  liftedQuantity = 0;
  sensor_source.sourceLiftedQuantity(param, x, y, param, x, y, time, qL, qR, liftedQuantity);
  SANS_CHECK_CLOSE( liftedQuantityTrue, liftedQuantity, small_tol, close_tol );

  ArrayQ sLTrue = {0, 0, 0, 0, 0};
  ArrayQ sRTrue = {0, 0, 0, 0, 0};
  ArrayQ sL = 0, sR = 0;
  sensor_source.sourceTrace(avpde, param, x, y, param, x, y, time, qL, qxL, qyL, qR, qxR, qyR, sL, sR);
  SANS_CHECK_CLOSE( sLTrue(0), sL(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( sLTrue(1), sL(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( sLTrue(2), sL(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( sLTrue(3), sL(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( sLTrue(4), sL(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue(0), sR(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue(1), sR(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue(2), sR(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue(3), sR(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue(4), sR(4), small_tol, close_tol );

  MatrixQ dsduTrue = {{0, 0, 0, 0, 0},
                      {0, 0, 0, 0, 0},
                      {0, 0, 0, 0, 0},
                      {0, 0, 0, 0, 0},
                      {0, 0, 0, 0, 1}};
  MatrixQ dsdu = 0.0;

  //TODO: We need to remove this Jacobian from PseudoTime!
#if 0
  sensor_source.jacobianSource(avpde, param, x, y, time, qL, qxL, qyL, dsdu);
  for (int i = 0; i < AVPDEClass::N; i++)
    for (int j = 0; j < AVPDEClass::N; j++)
      SANS_CHECK_CLOSE( dsduTrue(i,j), dsdu(i,j), small_tol, close_tol );
#endif

  output_test_stream output( "IO/pde/ArtificialViscosity/AVSensor_Source2D_Euler_Jump_pattern.txt", true );
  avpde.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}



//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AVSensor_Source2D_PressureGrad_test )
{
  typedef TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
  typedef AVSensor_Source2D_PressureGrad<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;
  typedef AVPDEClass::ArrayQ<Real> ArrayQ;
  typedef AVPDEClass::MatrixQ<Real> MatrixQ;
  typedef SurrealS<AVPDEClass::N> SurrealClass;
  typedef AVPDEClass::ArrayQ<SurrealClass> ArrayQSurreal;
  typedef DLA::MatrixSymS<PhysD2::D,Real> MatrixSym;

  int order = 1;

  // Gas Model
  Real gamma, R;
  gamma = 1.4;
  R = 1;
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;
  GasModel gas(gasModelDict);
  // PDE
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  EulerResidualInterpCategory interp = Euler_ResidInterp_Momentum;
  PDEBaseClass pdeEulerAV(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, eHarten);
  // Sensor equation terms
  Sensor sensor(pdeEulerAV);
  SensorAdvectiveFlux sensor_adv(0.0, 0.0);
  SensorViscousFlux sensor_visc(order);
  SensorSource sensor_source(order, sensor);
  // AV PDE with sensor equation
  AVPDEClass avpde(sensor_adv, sensor_visc, sensor_source, isSteady, order, hasSpaceTimeDiffusion,
                   EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, eHarten);

  BOOST_CHECK( sensor_source.hasSourceTerm() == true );
  BOOST_CHECK( sensor_source.hasSourceTrace() == false );
  BOOST_CHECK( sensor_source.hasSourceLiftedQuantity() == false );
  BOOST_CHECK( sensor_source.needsSolutionGradientforSource() == true );

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;
  
  Real hxx = 1.0;
  MatrixSym logH = {{log(hxx)},
                    {0.0, log(hxx)}};
  MatrixSym param = logH;

  Real x = 1, y = 0.5;
  Real time = 0;
  
  Real V = 5.0;
  Real P = 1.0;

  for (int i = 0; i < 36; i++)
  {
    Real alpha = Real(i) / 36 * 2.0 * M_PI;
    for (int j = 0; j < 36; j++)
    {
      Real beta = Real(j) / 36 * 2.0 * M_PI;

      Real rho = 1.0;
      Real u = V*cos(alpha);
      Real v = V*sin(alpha);
      Real p = 1.0;
      Real s = 0.0;
      // Derived properties
      Real c = sqrt(gamma*p/rho);
      Real t = p / (rho*R);
      Real e = gas.energy(rho, t);
      Real E = e + 0.5*(u*u + v*v);
      
      Real rhox = 0.1;
      Real ux = 0.6*cos(alpha) + 0.8*sin(alpha);
      Real vx = 0.6*sin(alpha) - 0.8*cos(alpha);
      Real px = P*cos(alpha + beta);
      Real sx = 0.0;
      
      Real rhoy = 0.2;
      Real uy = 0.8*cos(alpha) + 0.6*sin(alpha);
      Real vy = 0.8*sin(alpha) - 0.6*cos(alpha);
      Real py = P*sin(alpha + beta);
      Real sy = 0.0;
      
      ArrayQ q = 0;
      AVVariable<DensityVelocityPressure2D, Real> var( DensityVelocityPressure2D<Real>(rho, u, v, p), s );
      avpde.setDOFFrom( q, var );
      
      ArrayQ qx = {rhox, ux, vx, px, sx};
      ArrayQ qy = {rhoy, uy, vy, py, sy};
      
      Real fac = V*P*cos(beta) * hxx;
      Real sRef = 0.0;
      if (fac > 0)
        sRef = fac / (fac + 5.0*c*p);
      sRef = sRef * tanh(10.0 * sRef);
      ArrayQ sTrue = {0, 0, 0, 0, 5.0*(s - sRef)};
      
      ArrayQ src = {0, 0, 0, 0, 0};
      sensor_source.source(avpde, param, x, y, time, q, qx, qy, src);
      SANS_CHECK_CLOSE( sTrue(0), src(0), small_tol, close_tol );
      SANS_CHECK_CLOSE( sTrue(1), src(1), small_tol, close_tol );
      SANS_CHECK_CLOSE( sTrue(2), src(2), small_tol, close_tol );
      SANS_CHECK_CLOSE( sTrue(3), src(3), small_tol, close_tol );
      SANS_CHECK_CLOSE( sTrue(4), src(4), small_tol, close_tol );


      SurrealClass rhoS  = rho;
      SurrealClass rhouS = rho*u;
      SurrealClass rhovS = rho*v;
      SurrealClass rhoES = rho*E;
      SurrealClass sSurr = s;

      rhoS.deriv(0)   = 1;
      rhouS.deriv(1)  = 1;
      rhovS.deriv(2)  = 1;
      rhoES.deriv(3)  = 1;
      sSurr.deriv(4)  = 1;
      
      ArrayQSurreal qSurreal = 0.0;
      AVVariable<Conservative2D, SurrealClass> varSurreal( Conservative2D<SurrealClass>(rhoS, rhouS, rhovS, rhoES), sSurr );
      avpde.setDOFFrom( qSurreal, varSurreal );

      // conservative flux
      ArrayQSurreal srcSurreal = 0;
      sensor_source.source(avpde, param, x, y, time, qSurreal, qx, qy, srcSurreal);

      MatrixQ dsdq = 0;
      sensor_source.jacobianSource(avpde, param, x, y, time, q, qx, qy, dsdq);

      for (int a = 0; a < AVPDEClass::N; a++)
      {
        for (int b = 0; b < AVPDEClass::N; b++)
        {
          SANS_CHECK_CLOSE( srcSurreal[a].deriv(b), dsdq(a,b), small_tol, close_tol )
        }
      }
    }
  }

  output_test_stream output( "IO/pde/ArtificialViscosity/AVSensor_Source2D_Euler_PressureGrad_pattern.txt", true );
  avpde.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
