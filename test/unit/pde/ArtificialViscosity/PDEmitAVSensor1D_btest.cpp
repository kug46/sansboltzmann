// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEmitAVSensor1D_btest
//

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/Q1DPrimitiveSurrogate.h"
#include "pde/NS/Q1DConservative.h"
#include "pde/NS/Q1DEntropy.h"
#include "pde/NS/PDEEulermitAVDiffusion1D.h"

#include "pde/ArtificialViscosity/AVVariable.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_Source1D.h"
#include "pde/ArtificialViscosity/TraitsArtificialViscosity.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"

namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
class QTypeEntropy {};
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEmitAVSensor1D_test_suite )

typedef boost::mpl::list< QTypePrimitiveRhoPressure,
                          QTypeConservative,
                          QTypePrimitiveSurrogate,
                          QTypeEntropy > QTypes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( static_test, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  BOOST_REQUIRE( AVPDEClass::D == 1 );
  BOOST_REQUIRE( AVPDEClass::N == 4 );
  BOOST_REQUIRE( ArrayQ::M == 4 );
  BOOST_REQUIRE( MatrixQ::M == 4 );
  BOOST_REQUIRE( MatrixQ::N == 4 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctor, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  SensorAdvectiveFlux adv(1.0);
  SensorViscousFlux visc(1.0);
  SensorSource source(1.0);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(adv, visc, source, isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, AVPDEClass::Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( avpde.D == 1 );
  BOOST_REQUIRE( avpde.N == 4 );

  // flux components
  BOOST_CHECK( avpde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( avpde.hasFluxAdvective() == true );
  BOOST_CHECK( avpde.hasFluxViscous() == true );
  BOOST_CHECK( avpde.hasSource() == true );
  BOOST_CHECK( avpde.hasSourceTrace() == false );
  BOOST_CHECK( avpde.hasSourceLiftedQuantity() == false );
  BOOST_CHECK( avpde.hasForcingFunction() == false );

  BOOST_CHECK( avpde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( avpde.needsSolutionGradientforSource() == false );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setDOFFrom )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  typedef DLA::MatrixSymS<PhysD1::D,Real> paramType;

  const Real tol = 1.e-13;

  SensorAdvectiveFlux adv(1.0);
  SensorViscousFlux visc(1.0);
  SensorSource source(1.0);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(adv, visc, source, isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, AVPDEClass::Euler_ResidInterp_Raw);

  Real rho = 1.137;
  Real u = 0.784;
  Real t = 0.987;
  Real p = R*rho*t;
  Real E = gas.energy(rho,t) + 0.5*(u*u);

  paramType H = {{0.5}};
  paramType paramL = log(H);

  Real s = 0.35; //sensor value

  // set
  ArrayQ qTrue = {rho, u, p, s};
  ArrayQ q;

  DensityVelocityPressure1D<Real> qdata1;
  qdata1.Density = rho; qdata1.VelocityX = u; qdata1.Pressure = p;
  AVVariable<DensityVelocityPressure1D, Real> var1( qdata1, s );
  q = 0;
  avpde.setDOFFrom( q, var1 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );


  DensityVelocityTemperature1D<Real> qdata2(rho, u, t);
  AVVariable<DensityVelocityTemperature1D, Real> var2( qdata2, s );
  q = 0;
  avpde.setDOFFrom( q, var2 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );

  Conservative1D<Real> qdata3(rho, rho*u, rho*E);
  AVVariable<Conservative1D, Real> var3( qdata3, s );
  q = 0;
  avpde.setDOFFrom( q, var3 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );

  AVVariable<DensityVelocityPressure1D, Real> var4 = {{rho, u, p}, s};
  q = 0;
  avpde.setDOFFrom( q, var4 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );

  q = 0;
  q = avpde.setDOFFrom( var4 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD1::D,Real> paramType;

  const int N = AVPDEClass::N;
  BOOST_REQUIRE( N == 4 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real us = 1.0;
  Real ksxx = 0.8;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us);
  SensorViscousFlux visc(ksxx);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(adv, visc, source, isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, AVPDEClass::Euler_ResidInterp_Raw);

  // function tests

  Real x, time;
  Real rho, u, p, t, e0, h0;
  Real Cv, Cp;

  x = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u);
  h0 = Cp*t + 0.5*(u*u);

  paramType H = {{0.5}};
  paramType paramL = log(H);
  paramType paramR = 0.5*log(H);

  Real s = 0.35;

  // set
  ArrayQ q = 0;
  AVVariable<DensityVelocityTemperature1D, Real> var( DensityVelocityTemperature1D<Real>(rho, u, t), s );
  avpde.setDOFFrom( q, var );

  // Now to calculate tau
  paramType Hsq = H*H;
  Real tau = 1.0;
  typedef DLA::VectorS<1, Real> VectorX;
  VectorX hPrincipal;
  DLA::EigenValues(H, hPrincipal);
  //Get minimum length scale
  Real hmin = std::numeric_limits<Real>::max();
  for (int d = 0; d < 1; d++)
    hmin = std::min(hmin, hPrincipal[d]);
  Real lambda = 0;
  avpde.speedCharacteristic( paramL, x, time, q, lambda );
  // characteristic time of the PDE
  tau = hmin/(3.0*order*lambda);

  // master state
  ArrayQ uTrue = {rho, rho*u, rho*e0, s};
  ArrayQ uCons = 0;
  avpde.masterState( paramL, x, time, q, uCons );
  SANS_CHECK_CLOSE( uTrue(0), uCons(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(1), uCons(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(2), uCons(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(3), uCons(3), small_tol, close_tol );

  // Should not accumulate
  avpde.masterState( paramL, x, time, q, uCons );
  SANS_CHECK_CLOSE( uTrue(0), uCons(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(1), uCons(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(2), uCons(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(3), uCons(3), small_tol, close_tol );

  // conservative flux
  ArrayQ ftTrue = {rho, rho*u, rho*e0, tau*s};
  ArrayQ ft = 0;
  avpde.fluxAdvectiveTime( paramL, x, time, q, ft );
  SANS_CHECK_CLOSE( ftTrue(0), ft(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( ftTrue(1), ft(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( ftTrue(2), ft(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( ftTrue(3), ft(3), small_tol, close_tol );

  // Flux accumulation
  avpde.fluxAdvectiveTime( paramL, x, time, q, ft );
  for ( int i = 0; i < AVPDEClass::N; i++ )
  {
    SANS_CHECK_CLOSE( 2.*ftTrue[i], ft[i], small_tol, close_tol );
  }

  // advective flux
  ArrayQ fTrue = {rho*u, rho*u*u + p, rho*u*h0, us*s};
  ArrayQ f = 0;

  avpde.fluxAdvective( paramL, x, time, q, f );

  SANS_CHECK_CLOSE( fTrue(0), f(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(1), f(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(2), f(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(3), f(3), small_tol, close_tol );

  // Flux accumulation
  avpde.fluxAdvective( paramL, x, time, q, f );
  for ( int i = 0; i < AVPDEClass::N; i++ )
  {
    SANS_CHECK_CLOSE( 2.*fTrue[i], f[i], small_tol, close_tol );
  }

  ArrayQ qx = {0.31, -0.21, 0.67, 0.14};

  //Make H really small so that the artificial viscosity terms in Euler go away.
  //We only want to test the sensor equation here. Artificial viscosity is tested elsewhere.
  H = {{1e-20}};
  paramL = log(H);

  MatrixQ kxx = 0;
  BOOST_CHECK_THROW( avpde.diffusionViscous( paramL, x, time, q, qx, kxx ) , DeveloperException);

#if 0
  Real lambda = 0.0;
  avpde.speedCharacteristic( paramL, x, y, time, q, lambda );
  Real avvisc = 1.0/(order+1) * h * lambda * smoothabs0(s, 1e-5);

  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
    {
      if (i == N-1 && j == N-1)
      {
        SANS_CHECK_CLOSE( kxx(i,j), ksxx, small_tol, close_tol );
        SANS_CHECK_CLOSE( kxy(i,j), ksxy, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyx(i,j), ksyx, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyy(i,j), ksyy, small_tol, close_tol );
      }
      else
      {
        if (i == j)
        {
          SANS_CHECK_CLOSE( kxx(i,j), avvisc, small_tol, close_tol );
          SANS_CHECK_CLOSE( kyy(i,j), avvisc, small_tol, close_tol );
        }
        else
        {
          SANS_CHECK_CLOSE( kxx(i,j), 0.0, small_tol, close_tol );
          SANS_CHECK_CLOSE( kyy(i,j), 0.0, small_tol, close_tol );
        }

        SANS_CHECK_CLOSE( kxy(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyx(i,j), 0.0, small_tol, close_tol );
      }
    }
#endif

//  paramL = 0;

  f = 0;
  avpde.fluxViscous( paramL, x, time, q, qx, f );

  SANS_CHECK_CLOSE( f(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(3), -(ksxx*qx(3)), small_tol, close_tol );

  // Flux accumulation
  avpde.fluxViscous( paramL, x, time, q, qx, f );

  SANS_CHECK_CLOSE( f(0), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(1), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(2), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(3), 2*-(ksxx*qx(3)), small_tol, close_tol );

  Real nx = 0.3674;
  
  ArrayQ fL = 0;
  avpde.fluxViscous( paramL, x, time, q, qx, fL );
  ArrayQ fR = 0;
  avpde.fluxViscous( paramR, x, time, q, qx, fR );
  ArrayQ fnTrue = 0.5*(fL + fR)*nx ;

  ArrayQ fn = 0.0;
  avpde.fluxViscous( paramL, paramR, x, time, q, qx, q, qx, nx, fn );
  SANS_CHECK_CLOSE( fn(0), fnTrue(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fn(1), fnTrue(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fn(2), fnTrue(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( fn(3), fnTrue(3), small_tol, close_tol );

  // Flux accumulation
  avpde.fluxViscous( paramL, paramR, x, time, q, qx, q, qx, nx, fn );
  SANS_CHECK_CLOSE( fn(0), 2.0*fnTrue(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fn(1), 2.0*fnTrue(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fn(2), 2.0*fnTrue(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( fn(3), 2.0*fnTrue(3), small_tol, close_tol );

  ArrayQ src = 0;
  avpde.source( paramL, x, time, q, qx, src );

  SANS_CHECK_CLOSE( src(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(3), as*s, small_tol, close_tol );

  // Source accumulation
  avpde.source( paramL, x, time, q, qx, src );

  SANS_CHECK_CLOSE( src(0), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(1), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(2), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(3), 2*as*s, small_tol, close_tol );


  MatrixQ dsdu = 0;
  avpde.jacobianSource( paramL, x, time, q, qx, dsdu );

  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
    {
      if (i == N-1 && j == N-1)
      {
        SANS_CHECK_CLOSE( dsdu(i,j), as, small_tol, close_tol );
      }
      else
      {
        SANS_CHECK_CLOSE( dsdu(i,j), 0.0, small_tol, close_tol );
      }
    }

  // Source accumulation
  avpde.jacobianSource( paramL, x, time, q, qx, dsdu );

  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
    {
      if (i == N-1 && j == N-1)
      {
        SANS_CHECK_CLOSE( dsdu(i,j), 2*as, small_tol, close_tol );
      }
      else
      {
        SANS_CHECK_CLOSE( dsdu(i,j), 2*0.0, small_tol, close_tol );
      }
    }

  ArrayQ srcL = 0, srcR = 0;
  avpde.sourceTrace( paramL, x, paramL, x, time,
                     q, qx, q, qx, srcL, srcR );

  SANS_CHECK_CLOSE( srcL(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(3), as*s, small_tol, close_tol );

  SANS_CHECK_CLOSE( srcR(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(3), as*s, small_tol, close_tol );

  // Source accumulation
  avpde.sourceTrace( paramL, x, paramL, x, time,
                     q, qx, q, qx, srcL, srcR );

  SANS_CHECK_CLOSE( srcL(0), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(1), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(2), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(3), 2*as*s, small_tol, close_tol );

  SANS_CHECK_CLOSE( srcR(0), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(1), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(2), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(3), 2*as*s, small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AVEuler1D_PrimitiveStrongFlux )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD1::D,Real> paramType;
  const int N = AVPDEClass::N;


  const Real tol = 1.e-12;
  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real us = 1.0;
  Real ksxx = 0.8;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us);
  SensorViscousFlux visc(ksxx);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(adv, visc, source, isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, AVPDEClass::Euler_ResidInterp_Raw);

  // function tests

  Real x, time;
  Real rho, u, t, p, e0;
  Real Cv, Cp;

  x = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u);

  paramType Htensor = {{0.5}};
  paramType paramL = log(Htensor);

  Real s = 0.35;

  // set
  ArrayQ q = 0;
  DensityVelocityTemperature1D<Real> var(rho, u, t);
  AVVariable<DensityVelocityTemperature1D, Real> qdata( var, s );
  avpde.setDOFFrom( q, qdata );


  // Strong Conservative Flux
  Real rhot = -0.9;
  Real ut = -0.25;
  Real pt = 0.23;
  Real tt = (t/p) * pt - (t/rho)*rhot;

  Real e0t = Cv*tt + u*ut;

  paramType Hsq = Htensor*Htensor;

  Real tau = 1.0;
  typedef DLA::VectorS<1, Real> VectorX;
  VectorX hPrincipal;
  DLA::EigenValues(Htensor, hPrincipal);

  //Get minimum length scale
  Real hmin = std::numeric_limits<Real>::max();
  for (int d = 0; d < 1; d++)
    hmin = std::min(hmin, hPrincipal[d]);

  Real lambda = 0;
  avpde.speedCharacteristic( paramL, x, time, q, lambda );

  // characteristic time of the PDE
  tau = hmin/(3.0*Real(order)*lambda);

  Real dtau_dlambda = -1.0 / lambda / lambda * hmin / (3.0 * Real(order));
  // lambda = sqrt(u*u + v*v) + sqrt(gamma * p / rho)
  Real dlambda_drho = - sqrt(gamma*p/rho)  / 2.0 / rho;
  Real dlambda_du   = u / sqrt(u*u);
  Real dlambda_dp   = sqrt(gamma*p/rho)  / 2.0 / p;

  Real st = 0.49;

  ArrayQ qt = {rhot, ut, pt, st};
  ArrayQ utCons, utConsTrue;
  utConsTrue[0] = rhot;
  utConsTrue[1] = rho*ut + rhot*u;
  utConsTrue[2] = rho*e0t + rhot*e0;
  utConsTrue[3] = tau*st + s*dtau_dlambda*(dlambda_drho*rhot + dlambda_du*ut + dlambda_dp*pt);

  utCons = 0;
  avpde.strongFluxAdvectiveTime( paramL, x, time, q, qt, utCons );
  BOOST_CHECK_CLOSE( utConsTrue(0), utCons(0), tol );
  BOOST_CHECK_CLOSE( utConsTrue(1), utCons(1), tol );
  BOOST_CHECK_CLOSE( utConsTrue(2), utCons(2), tol );
  BOOST_CHECK_CLOSE( utConsTrue(3), utCons(3), tol );

  // Flux accumulation
  avpde.strongFluxAdvectiveTime( paramL, x, time, q, qt, utCons );
  BOOST_CHECK_CLOSE( 2*utConsTrue(0), utCons(0), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(1), utCons(1), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(2), utCons(2), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(3), utCons(3), tol );

  // Strong Advective Flux
  Real rhox = -0.02;
  Real ux = 0.072;
  Real px = 10.2;
  Real sx = 2.37;

  //PRIMITIVE SPECIFIC
  ArrayQ qx = {rhox, ux, px, sx};
  Real tx = (t/p) * px - (t/rho)*rhox;

  Real H = Cp*t + 0.5*(u*u);
  Real Hx = Cp*tx + (ux*u);

  ArrayQ strongAdv = 0;
  ArrayQ strongAdvTrue = 0;
  strongAdvTrue(0) = rhox*u   +   rho*ux;
  strongAdvTrue(1) = rhox*u*u + 2*rho*ux*u + px;
  strongAdvTrue(2) = rhox*u*H +   rho*ux*H + rho*u*Hx;
  strongAdvTrue(3) = us*sx;

  avpde.strongFluxAdvective( paramL, x, time, q, qx, strongAdv );
  BOOST_CHECK_CLOSE( strongAdvTrue(0), strongAdv(0), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(1), strongAdv(1), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(2), strongAdv(2), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(3), strongAdv(3), tol );

  // Flux accumulation
  avpde.strongFluxAdvective( paramL, x, time, q, qx, strongAdv );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(0), strongAdv(0), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(1), strongAdv(1), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(2), strongAdv(2), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(3), strongAdv(3), tol );


  MatrixQ dudq = 0;
  MatrixQ dudqTrue = 0;
  avpde.jacobianMasterState(paramL, x, time, q, dudq);

  ArrayQ rho_q = 0, u_q = 0, t_q = 0, taus_q = 0;
  Real e_rho = 0, e_t = 0;
  avpde.variableInterpreter().evalJacobian( q, rho_q, u_q, t_q );
  Real e = avpde.gasModel().energy(rho, t);
  avpde.gasModel().energyJacobian(rho, t, e_rho, e_t);
  Real E = e + 0.5*(u*u);
  ArrayQ E_q = e_rho*rho_q + e_t*t_q + u*u_q;
  taus_q[0] = s*dtau_dlambda*dlambda_drho;
  taus_q[1] = s*dtau_dlambda*dlambda_du;
  taus_q[2] = s*dtau_dlambda*dlambda_dp;
  taus_q[3] = tau;

  for (int j = 0; j < N; j++)
    dudqTrue(0,j) = rho_q[j];

  for (int j = 0; j < N; j++)
    dudqTrue(1,j) = rho_q[j]*u + rho*u_q[j];

  for (int j = 0; j < N; j++)
    dudqTrue(2,j) = rho_q[j]*E + rho*E_q[j];

  dudqTrue(3,3) = 1.0;

  avpde.jacobianMasterState(paramL, x, time, q, dudq);
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      SANS_CHECK_CLOSE( dudq(i,j), dudqTrue(i,j), small_tol, close_tol );

  // Should not accumulate
  avpde.jacobianMasterState(paramL, x, time, q, dudq);
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      SANS_CHECK_CLOSE( dudq(i,j), dudqTrue(i,j), small_tol, close_tol );


  MatrixQ dFtdu = 0;
  MatrixQ dFtduTrue = 0;

  dFtduTrue = DLA::Identity();
  for (int j = 0; j < N; j++)
    dFtduTrue(3,j) += taus_q[j];

  avpde.jacobianFluxAdvectiveTime(paramL, x, time, q, dFtdu);
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      SANS_CHECK_CLOSE( dFtdu(i,j), dFtduTrue(i,j), small_tol, close_tol );

  // Should accumulate
  avpde.jacobianFluxAdvectiveTime(paramL, x, time, q, dFtdu);
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      SANS_CHECK_CLOSE( dFtdu(i,j), 2.0*dFtduTrue(i,j), small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxRoe, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD1::D,Real> paramType;

  const int N = AVPDEClass::N;
  BOOST_REQUIRE( N == 4 );

  const Real tol = 2.e-11;

  Real us = 1.0;
  Real ksxx = 0.8;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us);
  SensorViscousFlux visc(ksxx);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(adv, visc, source, isSteady, order, hasSpaceTimeDiffusion,
                   EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, AVPDEClass::Euler_ResidInterp_Raw);

  // Roe flux function test

  Real x, time;
  Real nx;
  Real rhoL, uL, tL, pL, h0L;
  Real rhoR, uR, tR, pR, h0R;
  Real Cp;

  x = time = 0;   // not actually used in functions
  nx = 1.0;
  rhoL = 1.034; uL = 3.26; tL = 5.78;
  rhoR = 0.973; uR = 1.79; tR = 6.13;
  Cp  = R*gamma/(gamma - 1);
  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL);
  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR);

  paramType H = {{0.5}};
  paramType paramL = log(H);

  Real sL = 0.35;
  Real sR = 0.41;

  // set
  AVVariable<DensityVelocityTemperature1D, Real> qdataL = {{rhoL, uL, tL}, sL};
  AVVariable<DensityVelocityTemperature1D, Real> qdataR = {{rhoR, uR, tR}, sR};
  ArrayQ qL = avpde.setDOFFrom( qdataL );
  ArrayQ qR = avpde.setDOFFrom( qdataR );

  // advective normal flux (average)
  Real fL[N] = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*h0L, us*sL};
  Real fR[N] = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*h0R, us*sR};

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < N; k++)
    fnTrue[k] = 0.5*(nx*(fL[k] + fR[k]));

#if 1
  // Exact values for Roe scheme from Roe1D.nb
  fnTrue[0] -= -0.63318276027140017468491006399040;
  fnTrue[1] -=  -3.8160625416529803007039694937525;
  fnTrue[2] -=  -12.431949286034470439767789109435;
  fnTrue[3] -= 0.5*fabs(nx*us)*(sR - sL);
#endif

  ArrayQ fn = 0;
  avpde.fluxAdvectiveUpwind( paramL, x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( fnTrue(3), fn(3), tol );

  // Flux accumulation
  avpde.fluxAdvectiveUpwind( paramL, x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(3), fn(3), tol );

  MatrixQ mtx;
  mtx = 0;
  BOOST_CHECK_THROW( avpde.jacobianFluxAdvectiveAbsoluteValue(paramL, x, time, qL, nx, mtx ), DeveloperException);

  Real rhox = -0.02;
  Real ux = 0.072;
  Real px = 10.2;
  Real sx = 2.37;

  //PRIMITIVE SPECIFIC
  ArrayQ qx = {rhox, ux, px, sx};

  ArrayQ srcL = 0, srcR = 0;
  avpde.sourceTrace( paramL, x, paramL, x, time,
                     qL, qx, qR, qx, srcL, srcR );

  BOOST_CHECK_CLOSE( srcL(0), 0.0, tol );
  BOOST_CHECK_CLOSE( srcL(1), 0.0, tol );
  BOOST_CHECK_CLOSE( srcL(2), 0.0, tol );
  BOOST_CHECK_CLOSE( srcL(3), as*sL, tol );

  BOOST_CHECK_CLOSE( srcR(0), 0.0, tol );
  BOOST_CHECK_CLOSE( srcR(1), 0.0, tol );
  BOOST_CHECK_CLOSE( srcR(2), 0.0, tol );
  BOOST_CHECK_CLOSE( srcR(3), as*sR, tol );

  // Source accumulation
  avpde.sourceTrace( paramL, x, paramL, x, time,
                     qL, qx, qR, qx, srcL, srcR );

  BOOST_CHECK_CLOSE( srcL(0), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcL(1), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcL(2), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcL(3), 2*as*sL, tol );

  BOOST_CHECK_CLOSE( srcR(0), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcR(1), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcR(2), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcR(3), 2*as*sR, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( speedCharacteristic, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  typedef DLA::MatrixSymS<PhysD1::D,Real> paramType;

  const Real tol = 1.e-13;

  Real us = 1.0;
  Real ksxx = 0.8;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us);
  SensorViscousFlux visc(ksxx);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(adv, visc, source, isSteady, order, hasSpaceTimeDiffusion,
                   EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, AVPDEClass::Euler_ResidInterp_Raw);

  Real x, time, dx;
  Real rho, u, t, c;
  Real speed, speedTrue;

  x = time = 0;   // not actually used in functions
  dx = 0.57;
  rho = 1.137; u = 0.784; t = 0.987;
  c = sqrt(gamma*R*t);
  speedTrue = fabs(dx*u)/sqrt(dx*dx) + c;

  paramType H = {{0.5}};
  paramType paramL = log(H);

  Real s = 0.35;

  AVVariable<DensityVelocityTemperature1D, Real> qdata = {{rho, u, t}, s};
  ArrayQ q = avpde.setDOFFrom( qdata );

  avpde.speedCharacteristic( paramL, x, time, dx, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  avpde.speedCharacteristic( paramL, x, time, q, speed );
  BOOST_CHECK_CLOSE( sqrt(u*u) + c, speed, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( isValidState, QType, QTypes )
{
  // Surrogates are special
  // Entropy has log( x < 0 ) when constructing invalid states
  if ( std::is_same<QType,QTypePrimitiveSurrogate>::value ) return;
  if ( std::is_same<QType,QTypeEntropy>::value ) return;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  Real us = 1.0;
  Real ksxx = 0.8;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us);
  SensorViscousFlux visc(ksxx);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(adv, visc, source, isSteady, order, hasSpaceTimeDiffusion,
                   EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, AVPDEClass::Euler_ResidInterp_Raw);

  ArrayQ q;
  Real rho, u = 0, t;
  Real s = 0.35;

  rho = 1;
  t = 1;
  avpde.setDOFFrom( q, AVVariable<DensityVelocityTemperature1D, Real>({rho, u, t}, s) );
  BOOST_CHECK( avpde.isValidState(q) == true );

  rho = 1;
  t = -1;
  avpde.setDOFFrom( q, AVVariable<DensityVelocityTemperature1D, Real>({rho, u, t}, s) );
  BOOST_CHECK( avpde.isValidState(q) == false );

  rho = -1;
  t = 1;
  avpde.setDOFFrom( q, AVVariable<DensityVelocityTemperature1D, Real>({rho, u, t}, s) );
  BOOST_CHECK( avpde.isValidState(q) == false );

  rho = -1;
  t = -1;
  avpde.setDOFFrom( q, AVVariable<DensityVelocityTemperature1D, Real>({rho, u, t}, s) );
  BOOST_CHECK( avpde.isValidState(q) == false );

  rho = 1;
  t = 0;
  avpde.setDOFFrom( q, AVVariable<DensityVelocityTemperature1D, Real>({rho, u, t}, s) );
  BOOST_CHECK( avpde.isValidState(q) == false );

  rho = 0;
  t = 1;
  avpde.setDOFFrom( q, AVVariable<DensityVelocityTemperature1D, Real>({rho, u, t}, s) );
  BOOST_CHECK( avpde.isValidState(q) == false );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( interpResidBC )
{
  typedef QTypePrimitiveSurrogate QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  const int N = AVPDEClass::N;
  BOOST_REQUIRE( N == 4 );

  Real tol = 1.0e-12;

  Real us = 1.0;
  Real ksxx = 0.8;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us);
  SensorViscousFlux visc(ksxx);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde1(adv, visc, source, isSteady, order, hasSpaceTimeDiffusion,
                    EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, AVPDEClass::Euler_ResidInterp_Raw);

  ArrayQ q;
  Real rho, u = 0, t, s;

  rho = 1;
  t = 1;
  s = 0.35;

  avpde1.setDOFFrom( q, AVVariable<DensityVelocityTemperature1D, Real>({rho, u, t}, s) );
  BOOST_CHECK_EQUAL(avpde1.category(), 0);
  BOOST_CHECK_EQUAL(avpde1.nMonitor(), N);
  BOOST_CHECK( avpde1.isValidState(q) == true );

  DLA::VectorD<Real> vecout1(N);
  avpde1.interpResidVariable(q, vecout1);
  for (int i = 0; i < N; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );
//  SANS_CHECK_CLOSE( 0, vecout1(N-1), tol, tol );

  vecout1 = 0;
  avpde1.interpResidGradient(q, vecout1);
  for (int i = 0; i < N; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );
//  SANS_CHECK_CLOSE( 0, vecout1(N-1), tol, tol );

  vecout1 = 0;
  avpde1.interpResidBC(q, vecout1);
  for (int i = 0; i < N; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );
//  SANS_CHECK_CLOSE( 0, vecout1(N-1), tol, tol );


  AVPDEClass avpde2(adv, visc, source, isSteady, order, hasSpaceTimeDiffusion,
                    EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, AVPDEClass::Euler_ResidInterp_Momentum);
  BOOST_CHECK_EQUAL(avpde2.category(), 1);
  BOOST_CHECK_EQUAL(avpde2.nMonitor(), N);

  DLA::VectorD<Real> vecout2(4);
  avpde2.interpResidVariable(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(2),vecout2(2), tol, tol);
  SANS_CHECK_CLOSE(q(3),vecout2(3), tol, tol);

  vecout2 = 0;
  avpde2.interpResidGradient(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(2),vecout2(2), tol, tol);
  SANS_CHECK_CLOSE(q(3),vecout2(3), tol, tol);

  vecout2 = 0;
  avpde2.interpResidBC(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(2),vecout2(2), tol, tol);
  SANS_CHECK_CLOSE(q(3),vecout2(3), tol, tol);

  AVPDEClass avpde3(adv, visc, source, isSteady, order, hasSpaceTimeDiffusion,
                    EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, AVPDEClass::Euler_ResidInterp_Entropy);
  BOOST_CHECK_EQUAL(avpde3.nMonitor(), 1+1);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/ArtificialViscosity/PDEmitAVSensor1D_Euler_pattern.txt", true );

  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  Real us = 1.0;
  Real ksxx = 0.8;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us);
  SensorViscousFlux visc(ksxx);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(adv, visc, source, isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, AVPDEClass::Euler_ResidInterp_Raw);

  avpde.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
