// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEmitAVSensor3D_btest
//

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/Q3DPrimitiveSurrogate.h"
#include "pde/NS/Q3DConservative.h"
#include "pde/NS/PDEEulermitAVDiffusion3D.h"

#include "pde/ArtificialViscosity/AVVariable.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux3D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux3D.h"
#include "pde/ArtificialViscosity/AVSensor_Source3D.h"
#include "pde/ArtificialViscosity/TraitsArtificialViscosity.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"

namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEmitAVSensor3D_test_suite )

typedef boost::mpl::list< QTypePrimitiveRhoPressure,
                          QTypeConservative,
                          QTypePrimitiveSurrogate > QTypes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( static_test, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  BOOST_REQUIRE( AVPDEClass::D == 3 );
  BOOST_REQUIRE( AVPDEClass::N == 6 );
  BOOST_REQUIRE( ArrayQ::M == 6 );
  BOOST_REQUIRE( MatrixQ::M == 6 );
  BOOST_REQUIRE( MatrixQ::N == 6 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctor, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  SensorAdvectiveFlux adv(1.0, 0.0, 0.0);
  SensorViscousFlux visc(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0);
  SensorSource source(1.0);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  AVPDEClass avpde(adv, visc, source, isSteady, order, gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( avpde.D == 3 );
  BOOST_REQUIRE( avpde.N == 6 );

  // flux components
  BOOST_CHECK( avpde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( avpde.hasFluxAdvective() == true );
  BOOST_CHECK( avpde.hasFluxViscous() == true );
  BOOST_CHECK( avpde.hasSource() == true );
  BOOST_CHECK( avpde.hasSourceTrace() == false );
  BOOST_CHECK( avpde.hasSourceLiftedQuantity() == false );
  BOOST_CHECK( avpde.hasForcingFunction() == false );

  BOOST_CHECK( avpde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( avpde.needsSolutionGradientforSource() == false );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setDOFFrom )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  typedef DLA::MatrixSymS<PhysD3::D,Real> ParamType;

  const Real tol = 1.e-13;

  SensorAdvectiveFlux adv(1.0, 0.0, 0.0);
  SensorViscousFlux visc(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0);
  SensorSource source(1.0);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  AVPDEClass avpde(adv, visc, source, isSteady, order, gas, Euler_ResidInterp_Raw);

  Real rho = 1.137;
  Real u = 0.784;
  Real v = -0.231;
  Real w = 0.123;
  Real t = 0.987;
  Real p = R*rho*t;
  Real E = gas.energy(rho,t) + 0.5*(u*u + v*v + w*w);

  ParamType H = {{0.5},{0.1, 0.3},{0.4, 0.2, 0.8}};
  ParamType param = log(H);

  Real s = 0.35; //sensor value

  ArrayQ qTrue = {rho, u, v, w, p, s};
  ArrayQ q;

  DensityVelocityPressure3D<Real> qdata1;
  qdata1.Density = rho; qdata1.VelocityX = u; qdata1.VelocityY = v; qdata1.VelocityZ = w; qdata1.Pressure = p;
  AVVariable<DensityVelocityPressure3D, Real> var1( qdata1, s );
  q = 0;
  avpde.setDOFFrom( q, var1 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );
  BOOST_CHECK_CLOSE( qTrue(5), q(5), tol );


  DensityVelocityTemperature3D<Real> qdata2(rho, u, v, w, t);
  AVVariable<DensityVelocityTemperature3D, Real> var2( qdata2, s );
  q = 0;
  avpde.setDOFFrom( q, var2 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );
  BOOST_CHECK_CLOSE( qTrue(5), q(5), tol );

  Conservative3D<Real> qdata3(rho, rho*u, rho*v, rho*w, rho*E);
  AVVariable<Conservative3D, Real> var3( qdata3, s );
  q = 0;
  avpde.setDOFFrom( q, var3 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );
  BOOST_CHECK_CLOSE( qTrue(5), q(5), tol );

  AVVariable<DensityVelocityPressure3D, Real> var4 = {{rho, u, v, w, p}, s};
  q = 0;
  avpde.setDOFFrom( q, var4 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );
  BOOST_CHECK_CLOSE( qTrue(5), q(5), tol );

  q = 0;
  q = avpde.setDOFFrom( var4 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );
  BOOST_CHECK_CLOSE( qTrue(5), q(5), tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD3::D,Real> ParamType;

  const int N = AVPDEClass::N;
  BOOST_REQUIRE( N == 6 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real us = 1.0;
  Real vs = 0.6;
  Real ws = 0.1;
  Real kxxTrue = 2.123, kxyTrue = 0.553, kxzTrue = 0.643;
  Real kyxTrue = 0.378, kyyTrue = 1.007, kyzTrue = 0.765;
  Real kzxTrue = 1.642, kzyTrue = 1.299, kzzTrue = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws);
  SensorViscousFlux visc( kxxTrue, kxyTrue, kxzTrue,
                          kyxTrue, kyyTrue, kyzTrue,
                          kzxTrue, kzyTrue, kzzTrue );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  AVPDEClass avpde(adv, visc, source, isSteady, order, gas, Euler_ResidInterp_Raw);

  // function tests

  Real x, y, z, time;
  Real rho, u, v, w, t, p, e0, h0;
  Real Cv, Cp;

  x = y = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = -0.231; w = 0.123; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u + v*v + w*w);
  h0 = Cp*t + 0.5*(u*u + v*v + w*w);

  ParamType H = {{0.5},{0.1, 0.3},{0.4, 0.2, 0.8}};
  ParamType param = log(H);

  Real s = 0.35;

  // set
  ArrayQ q = 0;
  AVVariable<DensityVelocityTemperature3D, Real> var( DensityVelocityTemperature3D<Real>(rho, u, v, w, t), s );
  avpde.setDOFFrom( q, var );

  // Now to calculate tau
  ParamType Hsq = H*H;
  Real tau = 1.0;
  typedef DLA::VectorS<3, Real> VectorX;
  VectorX hPrincipal;
  DLA::EigenValues(H, hPrincipal);
  //Get minimum length scale
  Real hmin = std::numeric_limits<Real>::max();
  for (int d = 0; d < 3; d++)
    hmin = std::min(hmin, hPrincipal[d]);
  Real lambda = 0;
  avpde.speedCharacteristic( param, x, y, z, time, q, lambda );
  // characteristic time of the PDE
  tau = hmin/(3.0*order*lambda);

  // master state
  ArrayQ uTrue = {rho, rho*u, rho*v, rho*w, rho*e0, s};
  ArrayQ uCons = 0;
  avpde.masterState( param, x, y, z, time, q, uCons );
  SANS_CHECK_CLOSE( uTrue(0), uCons(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(1), uCons(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(2), uCons(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(3), uCons(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(4), uCons(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(5), uCons(5), small_tol, close_tol );

  // Should not accumulate
  avpde.masterState( param, x, y, z, time, q, uCons );
  SANS_CHECK_CLOSE( uTrue(0), uCons(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(1), uCons(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(2), uCons(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(3), uCons(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(4), uCons(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(5), uCons(5), small_tol, close_tol );

  // conservative flux
  ArrayQ ftTrue = {rho, rho*u, rho*v, rho*w, rho*e0, tau*s};
  ArrayQ ft = 0;
  avpde.fluxAdvectiveTime( param, x, y, z, time, q, ft );
  SANS_CHECK_CLOSE( ftTrue(0), ft(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( ftTrue(1), ft(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( ftTrue(2), ft(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( ftTrue(3), ft(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( ftTrue(4), ft(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( ftTrue(5), ft(5), small_tol, close_tol );

  // Flux accumulation
  avpde.fluxAdvectiveTime( param, x, y, z, time, q, ft );
  for ( int i = 0; i < AVPDEClass::N; i++ )
  {
    SANS_CHECK_CLOSE( 2.*ftTrue[i], ft[i], small_tol, close_tol );
  }

  // advective flux
  ArrayQ fTrue = {rho*u, rho*u*u + p, rho*u*v, rho*u*w, rho*u*h0, us*s};
  ArrayQ gTrue = {rho*v, rho*v*u, rho*v*v + p, rho*v*w, rho*v*h0, vs*s};
  ArrayQ hTrue = {rho*w, rho*w*u, rho*w*v, rho*w*w + p, rho*w*h0, ws*s};
  ArrayQ f = 0, g = 0, h = 0;

  avpde.fluxAdvective( param, x, y, z, time, q, f, g, h );

  SANS_CHECK_CLOSE( fTrue(0), f(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(1), f(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(2), f(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(3), f(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(4), f(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(5), f(5), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(0), g(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(1), g(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(2), g(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(3), g(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(4), g(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(5), g(5), small_tol, close_tol );
  SANS_CHECK_CLOSE( hTrue(0), h(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( hTrue(1), h(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( hTrue(2), h(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( hTrue(3), h(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( hTrue(4), h(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( hTrue(5), h(5), small_tol, close_tol );

  // Flux accumulate
  avpde.fluxAdvective( param, x, y, z, time, q, f, g, h );

  SANS_CHECK_CLOSE( 2*fTrue(0), f(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*fTrue(1), f(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*fTrue(2), f(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*fTrue(3), f(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*fTrue(4), f(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*fTrue(5), f(5), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*gTrue(0), g(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*gTrue(1), g(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*gTrue(2), g(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*gTrue(3), g(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*gTrue(4), g(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*gTrue(5), g(5), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*hTrue(0), h(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*hTrue(1), h(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*hTrue(2), h(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*hTrue(3), h(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*hTrue(4), h(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*hTrue(5), h(5), small_tol, close_tol );

  ArrayQ qx = {0.31, -0.21, 0.16, 0.32, 0.67, 0.14};
  ArrayQ qy = {-0.24, -0.73, 0.56, 0.45, 0.81, 0.39};
  ArrayQ qz = {0.14, -0.32, 0.12, 0.52, 0.35, 0.56};

  //Make H really small so that the artificial viscosity terms in Euler go away.
  //We only want to test the sensor equation here. Artificial viscosity is tested elsewhere.
  H = {{1e-20},{0.0, 1e-20}, {0.0, 0.0, 1e-20}};
  param = log(H);


  MatrixQ kxx, kxy, kxz;
  MatrixQ kyx, kyy, kyz;
  MatrixQ kzx, kzy, kzz;

  kxx = kxy = kxz = 0;
  kyx = kyy = kyz = 0;
  kzx = kzy = kzz = 0;

  avpde.diffusionViscous( param, x, y, z, time,
                          q, qx, qy, qz,
                          kxx, kxy, kxz,
                          kyx, kyy, kyz,
                          kzx, kzy, kzz  );

#if 1

  avpde.speedCharacteristic( param, x, y, z, time, q, lambda );
  DLA::MatrixSymS<PhysD3::D,Real> avvisc = 1.0/(order+1) * H * lambda * s;

  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
    {
      if (i == N-1 && j == N-1)
      {
        SANS_CHECK_CLOSE( kxx(i,j), kxxTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kxy(i,j), kxyTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kxz(i,j), kxzTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyx(i,j), kyxTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyy(i,j), kyyTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyz(i,j), kyzTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kzx(i,j), kzxTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kzy(i,j), kzyTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kzz(i,j), kzzTrue, small_tol, close_tol );
      }
      else
      {
        SANS_CHECK_CLOSE( kxx(i,j), avvisc(0,0), small_tol, close_tol );
        SANS_CHECK_CLOSE( kxy(i,j), avvisc(0,1), small_tol, close_tol );
        SANS_CHECK_CLOSE( kxz(i,j), avvisc(0,2), small_tol, close_tol );
        SANS_CHECK_CLOSE( kyx(i,j), avvisc(1,0), small_tol, close_tol );
        SANS_CHECK_CLOSE( kyy(i,j), avvisc(1,1), small_tol, close_tol );
        SANS_CHECK_CLOSE( kyz(i,j), avvisc(1,2), small_tol, close_tol );
        SANS_CHECK_CLOSE( kzx(i,j), avvisc(2,0), small_tol, close_tol );
        SANS_CHECK_CLOSE( kzy(i,j), avvisc(2,1), small_tol, close_tol );
        SANS_CHECK_CLOSE( kzz(i,j), avvisc(2,2), small_tol, close_tol );
      }
    }
#endif

//  param = 0;

  f = 0, g = 0, h = 0;
  avpde.fluxViscous( param, x, y, z, time, q, qx, qy, qz, f, g, h );

  SANS_CHECK_CLOSE( f(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(3), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(4), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(5), -(kxxTrue*qx(5) + kxyTrue*qy(5) + kxzTrue*qz(5)), small_tol, close_tol );

  SANS_CHECK_CLOSE( g(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( g(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( g(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( g(3), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( g(4), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( g(5), -(kyxTrue*qx(5) + kyyTrue*qy(5) + kyzTrue*qz(5)), small_tol, close_tol );

  SANS_CHECK_CLOSE( h(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( h(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( h(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( h(3), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( h(4), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( h(5), -(kzxTrue*qx(5) + kzyTrue*qy(5) + kzzTrue*qz(5)), small_tol, close_tol );

  // Flux accumulation
  avpde.fluxViscous( param, x, y, z, time, q, qx, qy, qz, f, g, h );

  SANS_CHECK_CLOSE( f(0), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(1), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(2), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(3), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(4), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(5), 2*-(kxxTrue*qx(5) + kxyTrue*qy(5) + kxzTrue*qz(5)), small_tol, close_tol );

  SANS_CHECK_CLOSE( g(0), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( g(1), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( g(2), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( g(3), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( g(4), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( g(5), 2*-(kyxTrue*qx(5) + kyyTrue*qy(5) + kyzTrue*qz(5)), small_tol, close_tol );

  SANS_CHECK_CLOSE( h(0), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( h(1), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( h(2), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( h(3), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( h(4), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( h(5), 2*-(kzxTrue*qx(5) + kzyTrue*qy(5) + kzzTrue*qz(5)), small_tol, close_tol );

  ArrayQ src = 0;
  avpde.source( param, x, y, z, time, q, qx, qy, qz, src );

  SANS_CHECK_CLOSE( src(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(3), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(4), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(5), as*s, small_tol, close_tol );

  // Source accumulate
  avpde.source( param, x, y, z, time, q, qx, qy, qz, src );

  SANS_CHECK_CLOSE( src(0), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(1), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(2), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(3), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(4), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(5), 2*as*s, small_tol, close_tol );

  MatrixQ dsdu = 0;
  avpde.jacobianSource( param, x, y, z, time, q, qx, qy, qz, dsdu );

  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
    {
      if (i == N-1 && j == N-1)
      {
        SANS_CHECK_CLOSE( dsdu(i,j), as, small_tol, close_tol );
      }
      else
      {
        SANS_CHECK_CLOSE( dsdu(i,j), 0.0, small_tol, close_tol );
      }
    }

  // Source accumulate
  avpde.jacobianSource( param, x, y, z, time, q, qx, qy, qz, dsdu );

  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
    {
      if (i == N-1 && j == N-1)
      {
        SANS_CHECK_CLOSE( dsdu(i,j), 2*as, small_tol, close_tol );
      }
      else
      {
        SANS_CHECK_CLOSE( dsdu(i,j), 2*0.0, small_tol, close_tol );
      }
    }

  ArrayQ srcL = 0, srcR = 0;
  avpde.sourceTrace( param, x, y, z, param, x, y, z, time,
                     q, qx, qy, qz, q, qx, qy, qz, srcL, srcR );

  SANS_CHECK_CLOSE( srcL(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(3), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(4), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(5), as*s, small_tol, close_tol );

  SANS_CHECK_CLOSE( srcR(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(3), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(4), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(5), as*s, small_tol, close_tol );

  // Source accumulate
  avpde.sourceTrace( param, x, y, z, param, x, y, z, time,
                     q, qx, qy, qz, q, qx, qy, qz, srcL, srcR );

  SANS_CHECK_CLOSE( srcL(0), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(1), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(2), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(3), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(4), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(5), 2*as*s, small_tol, close_tol );

  SANS_CHECK_CLOSE( srcR(0), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(1), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(2), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(3), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(4), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(5), 2*as*s, small_tol, close_tol );
}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AVEuler3D_strongFluxAdvectiveTime )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD3::D,Real> ParamType;

  const int N = AVPDEClass::N;
  BOOST_REQUIRE( N == 6 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real us = 1.0;
  Real vs = 0.6;
  Real ws = 0.1;
  Real kxxTrue = 2.123, kxyTrue = 0.553, kxzTrue = 0.643;
  Real kyxTrue = 0.378, kyyTrue = 1.007, kyzTrue = 0.765;
  Real kzxTrue = 1.642, kzyTrue = 1.299, kzzTrue = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws);
  SensorViscousFlux visc( kxxTrue, kxyTrue, kxzTrue,
                          kyxTrue, kyyTrue, kyzTrue,
                          kzxTrue, kzyTrue, kzzTrue );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  AVPDEClass avpde(adv, visc, source, isSteady, order, gas, Euler_ResidInterp_Raw);

  // function tests

  Real x, y, z, time;
  Real rho, u, v, w, t, p, e0;
  Real Cv;

  x = y = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = -0.231; w = 0.123; t = 0.987;
  Cv = R/(gamma - 1);
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u + v*v + w*w);

  ParamType Htensor = {{0.5},{0.1, 0.3},{0.4, 0.2, 0.8}};
  ParamType param = log(Htensor);

  Real s = 0.35;

  // set
  ArrayQ q = 0;
  DensityVelocityTemperature3D<Real> var(rho, u, v, w, t);
  AVVariable<DensityVelocityTemperature3D, Real> qdata( var, s );
  avpde.setDOFFrom( q, qdata );


  // Strong Conservative Flux
  Real rhot = -0.9;
  Real ut = -0.25;
  Real vt = 0.14;
  Real wt = 0.24;
  Real pt = 0.23;
  Real tt = (t/p) * pt - (t/rho)*rhot;

  Real e0t = Cv*tt + u*ut + v*vt + w*wt;

  Real st = 0.49;

  ParamType Hsq = Htensor*Htensor;

  Real tau = 1.0;
  typedef DLA::VectorS<3, Real> VectorX;
  VectorX hPrincipal;
  DLA::EigenValues(Htensor, hPrincipal);

  //Get minimum length scale
  Real hmin = std::numeric_limits<Real>::max();
  for (int d = 0; d < 3; d++)
    hmin = std::min(hmin, hPrincipal[d]);

  Real lambda = 0;
  avpde.speedCharacteristic( param, x, y, z, time, q, lambda );

  // characteristic time of the PDE
  tau = hmin/(3.0*Real(order)*lambda);

  Real dtau_dlambda = -1.0 / lambda / lambda * hmin / (3.0 * Real(order));
  // lambda = sqrt(u*u + v*v) + sqrt(gamma * p / rho)
  Real dlambda_drho = - sqrt(gamma*p/rho)  / 2.0 / rho;
  Real dlambda_du   = u / sqrt(u*u + v*v + w*w);
  Real dlambda_dv   = v / sqrt(u*u + v*v + w*w);
  Real dlambda_dw   = w / sqrt(u*u + v*v + w*w);
  Real dlambda_dp   = sqrt(gamma*p/rho)  / 2.0 / p;

  ArrayQ qt = {rhot, ut, vt, wt, pt, st};
  ArrayQ utCons, utConsTrue;
  utConsTrue[0] = rhot;
  utConsTrue[1] = rho*ut + rhot*u;
  utConsTrue[2] = rho*vt + rhot*v;
  utConsTrue[3] = rho*wt + rhot*w;
  utConsTrue[4] = rho*e0t + rhot*e0;
  utConsTrue[5] = tau*st + s*dtau_dlambda*(    dlambda_drho*rhot
                                             + dlambda_du*ut
                                             + dlambda_dv*vt
                                             + dlambda_dw*wt
                                             + dlambda_dp*pt);

  utCons = 0;
  avpde.strongFluxAdvectiveTime( param, x, y, z, time, q, qt, utCons );
  SANS_CHECK_CLOSE( utConsTrue(0), utCons(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( utConsTrue(1), utCons(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( utConsTrue(2), utCons(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( utConsTrue(3), utCons(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( utConsTrue(4), utCons(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( utConsTrue(5), utCons(5), small_tol, close_tol );

  // Flux accumulation
  avpde.strongFluxAdvectiveTime( param, x, y, z, time, q, qt, utCons );
  SANS_CHECK_CLOSE( 2*utConsTrue(0), utCons(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*utConsTrue(1), utCons(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*utConsTrue(2), utCons(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*utConsTrue(3), utCons(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*utConsTrue(4), utCons(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2*utConsTrue(5), utCons(5), small_tol, close_tol );

  MatrixQ dudq = 0;
  MatrixQ dudqTrue = 0;

  ArrayQ rho_q = 0, u_q = 0, v_q = 0, w_q = 0, t_q = 0, taus_q = 0;
  Real e_rho = 0, e_t = 0;
  avpde.variableInterpreter().evalJacobian( q, rho_q, u_q, v_q, w_q, t_q );
  Real e = avpde.gasModel().energy(rho, t);
  avpde.gasModel().energyJacobian(rho, t, e_rho, e_t);
  Real E = e + 0.5*(u*u + v*v + w*w);
  ArrayQ E_q = e_rho*rho_q + e_t*t_q + u*u_q + v*v_q + w*w_q;
  taus_q[0] = s*dtau_dlambda*dlambda_drho;
  taus_q[1] = s*dtau_dlambda*dlambda_du;
  taus_q[2] = s*dtau_dlambda*dlambda_dv;
  taus_q[3] = s*dtau_dlambda*dlambda_dw;
  taus_q[4] = s*dtau_dlambda*dlambda_dp;
  taus_q[5] = tau;

  for (int j = 0; j < N; j++)
    dudqTrue(0,j) = rho_q[j];

  for (int j = 0; j < N; j++)
    dudqTrue(1,j) = rho_q[j]*u + rho*u_q[j];

  for (int j = 0; j < N; j++)
    dudqTrue(2,j) = rho_q[j]*v + rho*v_q[j];

  for (int j = 0; j < N; j++)
    dudqTrue(3,j) = rho_q[j]*w + rho*w_q[j];

  for (int j = 0; j < N; j++)
    dudqTrue(4,j) = rho_q[j]*E + rho*E_q[j];

  dudqTrue(5,5) = 1.0;

  avpde.jacobianMasterState(param, x, y, z, time, q, dudq);
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      SANS_CHECK_CLOSE( dudq(i,j), dudqTrue(i,j), small_tol, close_tol );

  // Should not accumulate
  avpde.jacobianMasterState(param, x, y, z, time, q, dudq);
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      SANS_CHECK_CLOSE( dudq(i,j), dudqTrue(i,j), small_tol, close_tol );


  MatrixQ dFtdu = 0;
  MatrixQ dFtduTrue = 0;

  dFtduTrue = DLA::Identity();
  for (int j = 0; j < N; j++)
    dFtduTrue(5,j) += taus_q[j];

  avpde.jacobianFluxAdvectiveTime(param, x, y, z, time, q, dFtdu);
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      SANS_CHECK_CLOSE( dFtdu(i,j), dFtduTrue(i,j), small_tol, close_tol );

  // Should accumulate
  avpde.jacobianFluxAdvectiveTime(param, x, y, z, time, q, dFtdu);
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      SANS_CHECK_CLOSE( dFtdu(i,j), 2.0*dFtduTrue(i,j), small_tol, close_tol );
}
#if 0 // This stuff is dissabled in the PDE parts
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AVEuler3D_PrimitiveStrongFlux )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  typedef DLA::MatrixSymS<PhysD3::D,Real> ParamType;

  const Real tol = 1.e-12;

  Real us = 1.0;
  Real vs = 0.6;
  Real ksxx = 0.8, ksxy = 0.1, ksyx = 0.17, ksyy = 1.3;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs);
  SensorViscousFlux visc(ksxx, ksxy, ksyx, ksyy);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  AVPDEClass avpde(adv, visc, source, isSteady, order, gas, Euler_ResidInterp_Raw);

  // function tests

  Real x, y, time;
  Real rho, u, v, t, p, e0;
  Real Cv, Cp;

  x = y = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u + v*v);

  ParamType Htensor = {{0.5},{0.0, 0.3}};
  ParamType param = log(Htensor);

  Real s = 0.35;

  // set
  ArrayQ q = 0;
  DensityVelocityTemperature3D<Real> var(rho, u, v, t);
  AVVariable<DensityVelocityTemperature3D, Real> qdata( var, s );
  avpde.setDOFFrom( q, qdata );


  // Strong Conservative Flux
  Real rhot = -0.9;
  Real ut = -0.25;
  Real vt = 0.14;
  Real pt = 0.23;
  Real tt = (t/p) * pt - (t/rho)*rhot;

  Real e0t = Cv*tt + u*ut + v*vt;

  Real st = 0.49;

  ArrayQ qt = {rhot, ut, vt, pt, st};
  ArrayQ utCons, utConsTrue;
  utCons = 0;
  avpde.strongFluxAdvectiveTime( q, qt, utCons );
  utConsTrue[0] = rhot;
  utConsTrue[1] = rho*ut + rhot*u;
  utConsTrue[2] = rho*vt + rhot*v;
  utConsTrue[3] = rho*e0t + rhot*e0;
  utConsTrue[4] = st;
  BOOST_CHECK_CLOSE( utConsTrue(0), utCons(0), tol );
  BOOST_CHECK_CLOSE( utConsTrue(1), utCons(1), tol );
  BOOST_CHECK_CLOSE( utConsTrue(2), utCons(2), tol );
  BOOST_CHECK_CLOSE( utConsTrue(3), utCons(3), tol );
  BOOST_CHECK_CLOSE( utConsTrue(4), utCons(4), tol );

  // Strong Advective Flux
  Real rhox = -0.02;
  Real rhoy = 0.05;
  Real ux = 0.072;
  Real uy = -0.036;
  Real vx = -0.0234;
  Real vy = 0.0898;
  Real px = 10.2;
  Real py = 0.05;
  Real sx = 2.37;
  Real sy = -1.49;

  //PRIMITIVE SPECIFIC
  ArrayQ qx = {rhox, ux, vx, px, sx};
  ArrayQ qy = {rhoy, uy, vy, py, sy};

  Real tx = (t/p) * px - (t/rho)*rhox;
  Real ty = (t/p) * py - (t/rho)*rhoy;

  Real H = Cp*t + 0.5*(u*u + v*v);
  Real Hx = Cp*tx + (ux*u + vx*v);
  Real Hy = Cp*ty + (uy*u + vy*v);

  ArrayQ strongAdv = 0;
  ArrayQ strongAdvTrue = 0;
  strongAdvTrue(0) += rhox*u   +   rho*ux;
  strongAdvTrue(1) += rhox*u*u + 2*rho*ux*u + px;
  strongAdvTrue(2) += rhox*u*v +   rho*ux*v + rho*u*vx;
  strongAdvTrue(3) += rhox*u*H +   rho*ux*H + rho*u*Hx;
  strongAdvTrue(4) += us*sx;

  strongAdvTrue(0) += rhoy*v   +   rho*vy;
  strongAdvTrue(1) += rhoy*v*u +   rho*vy*u + rho*v*uy;
  strongAdvTrue(2) += rhoy*v*v + 2*rho*vy*v + py;
  strongAdvTrue(3) += rhoy*v*H +   rho*vy*H + rho*v*Hy;
  strongAdvTrue(4) += vs*sy;

  avpde.strongFluxAdvective( param, x, y, time, q, qx, qy, strongAdv );
  BOOST_CHECK_CLOSE( strongAdvTrue(0), strongAdv(0), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(1), strongAdv(1), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(2), strongAdv(2), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(3), strongAdv(3), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(4), strongAdv(4), tol );
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxRoe, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD3::D,Real> ParamType;

  const int N = AVPDEClass::N;
  BOOST_REQUIRE( N == 6 );

  const Real tol = 2.e-11;

  Real us = 1.0;
  Real vs = 0.6;
  Real ws = 0.1;
  Real kxxTrue = 2.123, kxyTrue = 0.553, kxzTrue = 0.643;
  Real kyxTrue = 0.378, kyyTrue = 1.007, kyzTrue = 0.765;
  Real kzxTrue = 1.642, kzyTrue = 1.299, kzzTrue = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws);
  SensorViscousFlux visc( kxxTrue, kxyTrue, kxzTrue,
                          kyxTrue, kyyTrue, kyzTrue,
                          kzxTrue, kzyTrue, kzzTrue );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  PDEBaseClass pde(order, gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady, order, gas, Euler_ResidInterp_Raw);

  // Roe flux function test

  Real x, y, z, time;
  Real nx, ny, nz;
  Real rhoL, uL, vL, wL, tL, pL, h0L;
  Real rhoR, uR, vR, wR, tR, pR, h0R;
  Real Cp;

  x = y = z = time = 0;   // not actually used in functions
  nx = 2.0/7.0; ny = 1.0/7.0; nz = sqrt(1.0 - nx*nx - ny*ny);
  rhoL = 1.034; uL = 3.26; vL = -2.17; wL = 3.56; tL = 5.78;
  rhoR = 0.973; uR = 1.79; vR =  0.93; wR = 5.23; tR = 6.13;
  Cp  = R*gamma/(gamma - 1);

  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL + vL*vL + wL*wL);

  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR + vR*vR + wR*wR);

  ParamType H = {{0.5},{0.0, 0.3}, {0.0, 0.0, 0.8}};
  ParamType param = log(H);

  Real sL = 0.35;
  Real sR = 0.41;

  // set
  AVVariable<DensityVelocityTemperature3D, Real> qdataL = {{rhoL, uL, vL, wL, tL}, sL};
  AVVariable<DensityVelocityTemperature3D, Real> qdataR = {{rhoR, uR, vR, wR, tR}, sR};
  ArrayQ qL = avpde.setDOFFrom( qdataL );
  ArrayQ qR = avpde.setDOFFrom( qdataR );

  // advective normal flux (average)
  ArrayQ fxL = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*vL     , rhoL*uL*wL     , rhoL*uL*h0L, us*sL};
  ArrayQ fyL = {rhoL*vL, rhoL*vL*uL     , rhoL*vL*vL + pL, rhoL*vL*wL     , rhoL*vL*h0L, vs*sL};
  ArrayQ fzL = {rhoL*wL, rhoL*wL*uL     , rhoL*wL*vL     , rhoL*wL*wL + pL, rhoL*wL*h0L, ws*sL};

  ArrayQ fxR = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*vR     , rhoR*uR*wR     , rhoR*uR*h0R, us*sL};
  ArrayQ fyR = {rhoR*vR, rhoR*vR*uR     , rhoR*vR*vR + pR, rhoR*vR*wR     , rhoR*vR*h0R, vs*sL};
  ArrayQ fzR = {rhoR*wR, rhoR*wR*uR     , rhoR*wR*vR     , rhoR*wR*wR + pR, rhoR*wR*h0R, ws*sL};

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < AVPDEClass::N; k++)
    fnTrue[k] = 0.5*(nx*(fxL[k] + fxR[k]) + ny*(fyL[k] + fyR[k]) + nz*(fzL[k] + fzR[k]));

#if 1
  // Exact values for Roe scheme from Roe.nb
  fnTrue[0] -= 0.65558457182422097471369683881548;
  fnTrue[1] -= -1.8638923372862592525182437515145;
  fnTrue[2] -=  7.0186535948930936601201334787757;
  fnTrue[3] -=  6.8854604538052794714508533474917;
  fnTrue[4] -= 20.499927239215613126430884098724;
#endif

  ArrayQ fn = 0;
  avpde.fluxAdvectiveUpwind( param, x, y, z, time, qL, qR, nx, ny, nz, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( fnTrue(3), fn(3), tol );
  BOOST_CHECK_CLOSE( fnTrue(4), fn(4), tol );
  BOOST_CHECK_CLOSE( fnTrue(5), fn(5), tol );

  // Flux accumulation
  avpde.fluxAdvectiveUpwind( param, x, y, z, time, qL, qR, nx, ny, nz, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(3), fn(3), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(4), fn(4), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(5), fn(5), tol );

  MatrixQ mtx, avmtx;
  mtx = 0;
  avmtx = 0;
  avpde.jacobianFluxAdvectiveAbsoluteValue(param, x, y, z, time, qL, nx, ny, nz, avmtx );
  pde.jacobianFluxAdvectiveAbsoluteValue(param, x, y, z, time, qL, nx, ny, nz, mtx);
  for (int i = 0; i < MatrixQ::M; i++)
  {
    for (int j = 0; j < MatrixQ::N; j++)
    {
      BOOST_CHECK_CLOSE( mtx(i,j), avmtx(i,j), tol );
    }
  }

  Real rhox = -0.02;
  Real rhoy = 0.05;
  Real rhoz = 0.12;
  Real ux = 0.072;
  Real uy = -0.036;
  Real uz = -0.411;
  Real vx = -0.0234;
  Real vy = 0.0898;
  Real vz = 0.4633;
  Real wx = -0.1242;
  Real wy = 0.6434;
  Real wz = 0.6434;
  Real px = 10.2;
  Real py = 0.05;
  Real pz = 0.34;
  Real sx = 2.37;
  Real sy = -1.49;
  Real sz = -4.123;

  //PRIMITIVE SPECIFIC
  ArrayQ qx = {rhox, ux, vx, wx, px, sx};
  ArrayQ qy = {rhoy, uy, vy, wy, py, sy};
  ArrayQ qz = {rhoz, uz, vz, wz, pz, sz};

  ArrayQ srcL = 0, srcR = 0;
  avpde.sourceTrace( param, x, y, z, param, x, y, z, time,
                     qL, qx, qy, qz, qR, qx, qy, qz, srcL, srcR );

  BOOST_CHECK_CLOSE( srcL(0), 0.0, tol );
  BOOST_CHECK_CLOSE( srcL(1), 0.0, tol );
  BOOST_CHECK_CLOSE( srcL(2), 0.0, tol );
  BOOST_CHECK_CLOSE( srcL(3), 0.0, tol );
  BOOST_CHECK_CLOSE( srcL(4), 0.0, tol );
  BOOST_CHECK_CLOSE( srcL(5), as*sL, tol );

  BOOST_CHECK_CLOSE( srcR(0), 0.0, tol );
  BOOST_CHECK_CLOSE( srcR(1), 0.0, tol );
  BOOST_CHECK_CLOSE( srcR(2), 0.0, tol );
  BOOST_CHECK_CLOSE( srcR(3), 0.0, tol );
  BOOST_CHECK_CLOSE( srcR(4), 0.0, tol );
  BOOST_CHECK_CLOSE( srcR(5), as*sR, tol );

  // Source accumulation
  avpde.sourceTrace( param, x, y, z, param, x, y, z, time,
                     qL, qx, qy, qz, qR, qx, qy, qz, srcL, srcR );

  BOOST_CHECK_CLOSE( srcL(0), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcL(1), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcL(2), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcL(3), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcL(4), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcL(5), 2*as*sL, tol );

  BOOST_CHECK_CLOSE( srcR(0), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcR(1), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcR(2), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcR(3), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcR(4), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcR(5), 2*as*sR, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( speedCharacteristic, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  typedef DLA::MatrixSymS<PhysD3::D,Real> ParamType;

  const Real tol = 1.e-13;

  Real us = 1.0;
  Real vs = 0.6;
  Real ws = 0.1;
  Real kxxTrue = 2.123, kxyTrue = 0.553, kxzTrue = 0.643;
  Real kyxTrue = 0.378, kyyTrue = 1.007, kyzTrue = 0.765;
  Real kzxTrue = 1.642, kzyTrue = 1.299, kzzTrue = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws);
  SensorViscousFlux visc( kxxTrue, kxyTrue, kxzTrue,
                          kyxTrue, kyyTrue, kyzTrue,
                          kzxTrue, kzyTrue, kzzTrue );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  AVPDEClass avpde(adv, visc, source, isSteady, order, gas, Euler_ResidInterp_Raw);

  Real x, y, z, time, dx, dy, dz;
  Real rho, u, v, w, t, c;
  Real speed, speedTrue;

  x = y = z = time = 0;   // not actually used in functions
  dx = 0.57; dy = -0.43; dz = 0.76;
  rho = 1.137; u = 0.784; v = -0.231; w = 0.456; t = 0.987;
  c = sqrt(gamma*R*t);
  speedTrue = fabs(dx*u + dy*v + dz*w)/sqrt(dx*dx + dy*dy + dz*dz) + c;

  ParamType H = {{0.5},{0.0, 0.3}, {0.0, 0.0, 0.8}};
  ParamType param = log(H);

  Real s = 0.35;

  AVVariable<DensityVelocityTemperature3D, Real> qdata = {{rho, u, v, w, t}, s};
  ArrayQ q = avpde.setDOFFrom( qdata );

  avpde.speedCharacteristic( param, x, y, z, time, dx, dy, dz, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  avpde.speedCharacteristic( param, x, y, z, time, q, speed );
  BOOST_CHECK_CLOSE( sqrt(u*u + v*v + w*w) + c, speed, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( isValidState, QType, QTypes )
{
  // Surrogates are special
  // Entropy has log( x < 0 ) when constructing invalid states
  if ( std::is_same<QType,QTypePrimitiveSurrogate>::value ) return;
  //if ( std::is_same<QType,QTypeEntropy>::value ) return;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  Real us = 1.0;
  Real vs = 0.6;
  Real ws = 0.1;
  Real kxxTrue = 2.123, kxyTrue = 0.553, kxzTrue = 0.643;
  Real kyxTrue = 0.378, kyyTrue = 1.007, kyzTrue = 0.765;
  Real kzxTrue = 1.642, kzyTrue = 1.299, kzzTrue = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws);
  SensorViscousFlux visc( kxxTrue, kxyTrue, kxzTrue,
                          kyxTrue, kyyTrue, kyzTrue,
                          kzxTrue, kzyTrue, kzzTrue );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  AVPDEClass avpde(adv, visc, source, isSteady, order, gas, Euler_ResidInterp_Raw);

  ArrayQ q;
  Real rho, u = 0, v = 0, w = 0, t;
  Real s = 0.35;

  rho = 1;
  t = 1;
  avpde.setDOFFrom( q, AVVariable<DensityVelocityTemperature3D, Real>({rho, u, v, w, t}, s) );
  BOOST_CHECK( avpde.isValidState(q) == true );

  rho = 1;
  t = -1;
  avpde.setDOFFrom( q, AVVariable<DensityVelocityTemperature3D, Real>({rho, u, v, w, t}, s) );
  BOOST_CHECK( avpde.isValidState(q) == false );

  rho = -1;
  t = 1;
  avpde.setDOFFrom( q, AVVariable<DensityVelocityTemperature3D, Real>({rho, u, v, w, t}, s) );
  BOOST_CHECK( avpde.isValidState(q) == false );

  rho = -1;
  t = -1;
  avpde.setDOFFrom( q, AVVariable<DensityVelocityTemperature3D, Real>({rho, u, v, w, t}, s) );
  BOOST_CHECK( avpde.isValidState(q) == false );

  rho = 1;
  t = 0;
  avpde.setDOFFrom( q, AVVariable<DensityVelocityTemperature3D, Real>({rho, u, v, w, t}, s) );
  BOOST_CHECK( avpde.isValidState(q) == false );

  rho = 0;
  t = 1;
  avpde.setDOFFrom( q, AVVariable<DensityVelocityTemperature3D, Real>({rho, u, v, w, t}, s) );
  BOOST_CHECK( avpde.isValidState(q) == false );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( interpResidBC )
{
  typedef QTypePrimitiveSurrogate QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  const int N = AVPDEClass::N;
  BOOST_REQUIRE( N == 6 );

  Real tol = 1.0e-12;

  Real us = 1.0;
  Real vs = 0.6;
  Real ws = 0.1;
  Real kxxTrue = 2.123, kxyTrue = 0.553, kxzTrue = 0.643;
  Real kyxTrue = 0.378, kyyTrue = 1.007, kyzTrue = 0.765;
  Real kzxTrue = 1.642, kzyTrue = 1.299, kzzTrue = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws);
  SensorViscousFlux visc( kxxTrue, kxyTrue, kxzTrue,
                          kyxTrue, kyyTrue, kyzTrue,
                          kzxTrue, kzyTrue, kzzTrue );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  AVPDEClass avpde1(adv, visc, source, isSteady, order, gas, Euler_ResidInterp_Raw);

  ArrayQ q;
  Real rho = 1.137, u = 0.784, v = -0.231, w = 0.456, t = 0.987, s = 0.35;

  avpde1.setDOFFrom( q, AVVariable<DensityVelocityTemperature3D, Real>({rho, u, v, w, t}, s) );
  BOOST_CHECK_EQUAL(avpde1.category(), 0);
  BOOST_CHECK_EQUAL(avpde1.nMonitor(), N);
  BOOST_CHECK( avpde1.isValidState(q) == true );

  DLA::VectorD<Real> vecout1(N);
  avpde1.interpResidVariable(q, vecout1);
  for (int i=0; i<N-1; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );
  SANS_CHECK_CLOSE( 0, vecout1(N-1), tol, tol );
  vecout1 = 0;
  avpde1.interpResidGradient(q, vecout1);
  for (int i=0; i<N; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );
  vecout1 = 0;
  avpde1.interpResidBC(q, vecout1);
  for (int i=0; i<N; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );

  AVPDEClass pde2(adv, visc, source, isSteady, order, gas, Euler_ResidInterp_Momentum);
  BOOST_CHECK_EQUAL(pde2.nMonitor(), 4);
  DLA::VectorD<Real> vecout2(4);
  pde2.interpResidVariable(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)+q(2)*q(2)+q(3)*q(3)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(4),vecout2(2), tol, tol);
  //SANS_CHECK_CLOSE(q(5),vecout2(3), tol, tol);
  SANS_CHECK_CLOSE(0,vecout2(3), tol, tol);

  vecout2 = 0;
  pde2.interpResidGradient(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)+q(2)*q(2)+q(3)*q(3)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(4),vecout2(2), tol, tol);
  SANS_CHECK_CLOSE(q(5),vecout2(3), tol, tol);

  vecout2 = 0;
  pde2.interpResidBC(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)+q(2)*q(2)+q(3)*q(3)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(4),vecout2(2), tol, tol);
  SANS_CHECK_CLOSE(q(5),vecout2(3), tol, tol);

  AVPDEClass avpde3(adv, visc, source, isSteady, order, gas, Euler_ResidInterp_Entropy);
  BOOST_CHECK_EQUAL(avpde3.nMonitor(), 1);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/ArtificialViscosity/PDEmitAVSensor3D_Euler_pattern.txt", true );

  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  Real us = 1.0;
  Real vs = 0.6;
  Real ws = 0.1;
  Real kxxTrue = 2.123, kxyTrue = 0.553, kxzTrue = 0.643;
  Real kyxTrue = 0.378, kyyTrue = 1.007, kyzTrue = 0.765;
  Real kzxTrue = 1.642, kzyTrue = 1.299, kzzTrue = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws);
  SensorViscousFlux visc( kxxTrue, kxyTrue, kxzTrue,
                          kyxTrue, kyyTrue, kyzTrue,
                          kzxTrue, kzyTrue, kzzTrue );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  AVPDEClass avpde(adv, visc, source, isSteady, order, gas, Euler_ResidInterp_Raw);

  avpde.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
