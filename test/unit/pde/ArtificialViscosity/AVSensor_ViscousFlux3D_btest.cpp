// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AVSensor_ViscousFlux3D_btest
//
// test of 3-D diffusion matrix class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/PDEEulermitAVDiffusion3D.h"
#include "pde/NS/BCEulermitAVSensor3D.h"
#include "pde/NS/Fluids3D_Sensor.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux3D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux3D.h"
#include "pde/ArtificialViscosity/AVSensor_Source3D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor3D.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( AVSensor_ViscousFlux3D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  BOOST_CHECK( AVSensor_ViscousFlux3D_Uniform<TraitsSizeEulerArtificialViscosity>::D == 3 );
  BOOST_CHECK( AVSensor_DiffusionMatrix3D_GenHScale<TraitsSizeEulerArtificialViscosity>::D == 3 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Uniform )
{
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> Sensor_ViscousFlux;
  typedef Sensor_ViscousFlux::ArrayQ<Real> ArrayQ;
  typedef Sensor_ViscousFlux::MatrixQ<Real> MatrixQ;

  typedef TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD3, PDEBaseClass> Sensor;
  typedef AVSensor_Source3D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real kxxTrue = 2.123, kxyTrue = 0.553, kxzTrue = 0.643;
  Real kyxTrue = 0.378, kyyTrue = 1.007, kyzTrue = 0.765;
  Real kzxTrue = 1.642, kzyTrue = 1.299, kzzTrue = 1.234;

  Sensor_ViscousFlux visc( kxxTrue, kxyTrue, kxzTrue,
                           kyxTrue, kyyTrue, kyzTrue,
                           kzxTrue, kzyTrue, kzzTrue );

  // static tests
  BOOST_CHECK( visc.D == 3 );
  BOOST_CHECK_EQUAL( visc.hasFluxViscous(), true );

  // Gas Model
  Real gamma, R;
  gamma = 1.4;
  R = 1;
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;
  GasModel gas(gasModelDict);
  // PDE
  bool isSteady = false;
  int order_pde = 1;
  EulerResidualInterpCategory interp = Euler_ResidInterp_Momentum;
  PDEBaseClass pdeEulerAV(order_pde, gas, interp, eHarten);
  // Sensor equation terms
  Sensor sensor(pdeEulerAV);
  SensorAdvectiveFlux sensor_adv(0.0, 0.0);
  SensorViscousFlux sensor_visc(order_pde);
  SensorSource sensor_source(order_pde, sensor);
  // AV PDE with sensor equation
  AVPDEClass avpde(sensor_adv, sensor_visc, sensor_source, isSteady, order_pde, gas, interp, eHarten);

  Real s = 0.35;
  Real sx = 0.67;
  Real sy = 0.41;
  Real sz = 4.531;

  ArrayQ qx = {0.31, -0.21, 0.16, 0.32, 0.67, sx};
  ArrayQ qy = {-0.24, -0.73, 0.56, 0.45, 0.81, sy};
  ArrayQ qz = {0.14, -0.32, 0.12, 0.52, 0.35, sz};

  Real rho = 1.137, u = 0.784, v = -0.231, w = 0.123, t = 0.987;
  ArrayQ q = 0;
  AVVariable<DensityVelocityTemperature3D, Real> var( DensityVelocityTemperature3D<Real>(rho, u, v, w, t), s );
  avpde.setDOFFrom( q, var );
  Real param = 0.1;

  // functor
  Real x = 1;
  Real y = 0.5;
  Real z = 0.25;
  Real time = 0;

  ArrayQ fx = 0, fy = 0, fz = 0;

  visc.fluxViscous(avpde, param, x, y, z, time, q, qx, qy, qz, fx, fy, fz);

  SANS_CHECK_CLOSE( fx(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fx(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fx(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fx(3), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fx(4), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fx(5), -(kxxTrue*qx(5) + kxyTrue*qy(5) + kxzTrue*qz(5)), small_tol, close_tol );

  SANS_CHECK_CLOSE( fy(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fy(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fy(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fy(3), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fy(4), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fy(5), -(kyxTrue*qx(5) + kyyTrue*qy(5) + kyzTrue*qz(5)), small_tol, close_tol );

  SANS_CHECK_CLOSE( fz(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fz(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fz(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fz(3), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fz(4), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fz(5), -(kzxTrue*qx(5) + kzyTrue*qy(5) + kzzTrue*qz(5)), small_tol, close_tol );

  MatrixQ kxx, kxy, kxz;
  MatrixQ kyx, kyy, kyz;
  MatrixQ kzx, kzy, kzz;

  kxx = kxy = kxz = 0;
  kyx = kyy = kyz = 0;
  kzx = kzy = kzz = 0;

  visc.diffusionViscous(avpde, param,
                        x, y, z, time,
                        q, qx, qy, qz,
                        kxx, kxy, kxz,
                        kyx, kyy, kyz,
                        kzx, kzy, kzz );

  for (int i = 0; i < AVPDEClass::N; i++)
    for (int j = 0; j < AVPDEClass::N; j++)
    {
      if (i == AVPDEClass::N-1 && j == AVPDEClass::N-1)
      {
        SANS_CHECK_CLOSE( kxx(i,j), kxxTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kxy(i,j), kxyTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kxz(i,j), kxzTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyx(i,j), kyxTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyy(i,j), kyyTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyz(i,j), kyzTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kzx(i,j), kzxTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kzy(i,j), kzyTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kzz(i,j), kzzTrue, small_tol, close_tol );
      }
      else
      {
        SANS_CHECK_CLOSE( kxx(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kxy(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kxz(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyx(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyy(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyz(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kzx(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kzy(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kzz(i,j), 0.0, small_tol, close_tol );
      }
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( GenHScale )
{
  typedef AVSensor_ViscousFlux3D_GenHScale<TraitsSizeEulerArtificialViscosity> Sensor_ViscousFlux;
  typedef Sensor_ViscousFlux::ArrayQ<Real> ArrayQ;
  typedef Sensor_ViscousFlux::MatrixQ<Real> MatrixQ;
  typedef DLA::MatrixSymS<PhysD3::D,Real> MatrixSym;

  typedef TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD3, PDEBaseClass> Sensor;
  typedef AVSensor_Source3D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real C = 5.0;

  Real hxx = 0.461, hyy = 0.912, hzz = 1.023;
  MatrixSym logH = {{log(hxx)},
                    {0.0, log(hyy)},
                    {0.0, 0.0, log(hzz)}};
  Real kxxTrue = C*hxx*hxx;
  Real kxyTrue = 0.0, kyxTrue = 0.0, kzxTrue = 0.0;
  Real kyyTrue = C*hyy*hyy;
  Real kxzTrue = 0.0, kyzTrue = 0.0, kzyTrue = 0.0;
  Real kzzTrue = C*hzz*hzz;

  int order = 1;
  Sensor_ViscousFlux visc(order);
  BOOST_CHECK_EQUAL( visc.hasFluxViscous(), true );

  // Gas Model
  Real gamma, R;
  gamma = 1.4;
  R = 1;
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;
  GasModel gas(gasModelDict);
  // PDE
  bool isSteady = false;
  int order_pde = 1;
  EulerResidualInterpCategory interp = Euler_ResidInterp_Momentum;
  PDEBaseClass pdeEulerAV(order_pde, gas, interp, eHarten);
  // Sensor equation terms
  Sensor sensor(pdeEulerAV);
  SensorAdvectiveFlux sensor_adv(0.0, 0.0);
  SensorViscousFlux sensor_visc(order_pde);
  SensorSource sensor_source(order_pde, sensor);
  // AV PDE with sensor equation
  AVPDEClass avpde(sensor_adv, sensor_visc, sensor_source, isSteady, order_pde, gas, interp, eHarten);

  Real s = 0.35;
  Real sx = 0.67;
  Real sy = 0.41;
  Real sz = 4.531;

  ArrayQ qx = {0.31, -0.21, 0.16, 0.32, 0.67, sx};
  ArrayQ qy = {-0.24, -0.73, 0.56, 0.45, 0.81, sy};
  ArrayQ qz = {0.14, -0.32, 0.12, 0.52, 0.35, sz};

  ArrayQ qxx = 0;
  ArrayQ qxy = 0, qyy = 0;
  ArrayQ qxz = 0, qyz = 0, qzz = 0;

  Real rho = 1.137, u = 0.784, v = -0.231, w = 0.123, t = 0.987;
  ArrayQ q = 0;
  AVVariable<DensityVelocityTemperature3D, Real> var( DensityVelocityTemperature3D<Real>(rho, u, v, w, t), s );
  avpde.setDOFFrom( q, var );
  MatrixSym param = logH;

  // functor
  Real x = 1;
  Real y = 0.5;
  Real z = 0.25;
  Real time = 0;

  ArrayQ fx = 0, fy = 0, fz = 0;

  visc.fluxViscous(avpde, param, x, y, z, time, q, qx, qy, qz, fx, fy, fz);

  SANS_CHECK_CLOSE( fx(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fx(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fx(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fx(3), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fx(4), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fx(5), -(kxxTrue*qx(5) + kxyTrue*qy(5) + kxzTrue*qz(5)), small_tol, close_tol );

  SANS_CHECK_CLOSE( fy(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fy(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fy(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fy(3), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fy(4), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fy(5), -(kyxTrue*qx(5) + kyyTrue*qy(5) + kyzTrue*qz(5)), small_tol, close_tol );

  SANS_CHECK_CLOSE( fz(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fz(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fz(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fz(3), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fz(4), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fz(5), -(kzxTrue*qx(5) + kzyTrue*qy(5) + kzzTrue*qz(5)), small_tol, close_tol );

  MatrixQ kxx, kxy, kxz;
  MatrixQ kyx, kyy, kyz;
  MatrixQ kzx, kzy, kzz;

  kxx = kxy = kxz = 0;
  kyx = kyy = kyz = 0;
  kzx = kzy = kzz = 0;

  visc.diffusionViscous(avpde, param,
                         x, y, z, time,
                         q, qx, qy, qz,
                         kxx, kxy, kxz,
                         kyx, kyy, kyz,
                         kzx, kzy, kzz );

  for (int i = 0; i < AVPDEClass::N; i++)
    for (int j = 0; j < AVPDEClass::N; j++)
    {
      if (i == AVPDEClass::N-1 && j == AVPDEClass::N-1)
      {
        SANS_CHECK_CLOSE( kxx(i,j), kxxTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kxy(i,j), kxyTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kxz(i,j), kxzTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyx(i,j), kyxTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyy(i,j), kyyTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyz(i,j), kyzTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kzx(i,j), kzxTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kzy(i,j), kzyTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kzz(i,j), kzzTrue, small_tol, close_tol );
      }
      else
      {
        SANS_CHECK_CLOSE( kxx(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kxy(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kxz(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyx(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyy(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyz(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kzx(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kzy(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kzz(i,j), 0.0, small_tol, close_tol );
      }
    }

  MatrixQ dfdu = 0, dgdu = 0, dhdu = 0;
  visc.jacobianFluxViscous(avpde, param,
                           x, y, z, time,
                           q, qx, qy, qz,
                           dfdu, dgdu, dhdu );

  for (int i = 0; i < AVPDEClass::N; i++)
  {
    for (int j = 0; j < AVPDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( dfdu(i,j), 0.0, small_tol, close_tol );
      SANS_CHECK_CLOSE( dgdu(i,j), 0.0, small_tol, close_tol );
      SANS_CHECK_CLOSE( dhdu(i,j), 0.0, small_tol, close_tol );
    }
  }
  
  MatrixQ kxx_x = 0, kxy_x = 0, kxz_x = 0, kyx_x = 0, kzx_x = 0;
  MatrixQ kxy_y = 0, kyx_y = 0, kyy_y = 0, kyz_y = 0, kzy_y = 0;
  MatrixQ kxz_z = 0, kyz_z = 0, kzx_z = 0, kzy_z = 0, kzz_z = 0;
  visc.diffusionViscousGradient(avpde, param,
                                x, y, z, time,
                                q, qx, qy, qz,
                                qxx,
                                qxy, qyy,
                                qxz, qyz, qzz,
                                kxx_x, kxy_x, kxz_x, kyx_x, kzx_x,
                                kxy_y, kyx_y, kyy_y, kyz_y, kzy_y,
                                kxz_z, kyz_z, kzx_z, kzy_z, kzz_z );

  for (int i = 0; i < AVPDEClass::N; i++)
  {
    for (int j = 0; j < AVPDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( kxx_x(i,j), 0.0, small_tol, close_tol );
      SANS_CHECK_CLOSE( kxy_x(i,j), 0.0, small_tol, close_tol );
      SANS_CHECK_CLOSE( kxz_x(i,j), 0.0, small_tol, close_tol );
      SANS_CHECK_CLOSE( kyx_x(i,j), 0.0, small_tol, close_tol );
      SANS_CHECK_CLOSE( kzx_x(i,j), 0.0, small_tol, close_tol );

      SANS_CHECK_CLOSE( kxy_y(i,j), 0.0, small_tol, close_tol );
      SANS_CHECK_CLOSE( kyx_y(i,j), 0.0, small_tol, close_tol );
      SANS_CHECK_CLOSE( kyy_y(i,j), 0.0, small_tol, close_tol );
      SANS_CHECK_CLOSE( kyz_y(i,j), 0.0, small_tol, close_tol );
      SANS_CHECK_CLOSE( kyz_y(i,j), 0.0, small_tol, close_tol );

      SANS_CHECK_CLOSE( kxz_z(i,j), 0.0, small_tol, close_tol );
      SANS_CHECK_CLOSE( kyz_z(i,j), 0.0, small_tol, close_tol );
      SANS_CHECK_CLOSE( kzx_z(i,j), 0.0, small_tol, close_tol );
      SANS_CHECK_CLOSE( kzy_z(i,j), 0.0, small_tol, close_tol );
      SANS_CHECK_CLOSE( kzz_z(i,j), 0.0, small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/ArtificialViscosity/AVSensor_ViscousFlux3D_pattern.txt", true );

  {
    typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> Sensor_ViscousFlux;
    Real kxxTrue = 2.123, kxyTrue = 0.553, kxzTrue = 0.643;
    Real kyxTrue = 0.378, kyyTrue = 1.007, kyzTrue = 0.765;
    Real kzxTrue = 1.642, kzyTrue = 1.299, kzzTrue = 1.234;

    Sensor_ViscousFlux visc( kxxTrue, kxyTrue, kxzTrue,
                           kyxTrue, kyyTrue, kyzTrue,
                           kzxTrue, kzyTrue, kzzTrue );
    visc.dump( 2, output );
  }

  {
    typedef AVSensor_ViscousFlux3D_GenHScale<TraitsSizeEulerArtificialViscosity> Sensor_ViscousFlux;
    int order = 1;
    Sensor_ViscousFlux visc(order);
    visc.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
