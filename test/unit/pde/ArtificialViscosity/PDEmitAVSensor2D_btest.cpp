// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEmitAVSensor2D_btest
//

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/PDEEulermitAVDiffusion2D.h"

#include "pde/ArtificialViscosity/AVVariable.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/TraitsArtificialViscosity.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"

namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
class QTypeEntropy {};
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEmitAVSensor2D_test_suite )

typedef boost::mpl::list< QTypePrimitiveRhoPressure,
                          QTypeConservative,
                          QTypePrimitiveSurrogate,
                          QTypeEntropy > QTypes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( static_test, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  BOOST_REQUIRE( AVPDEClass::D == 2 );
  BOOST_REQUIRE( AVPDEClass::N == 5 );
  BOOST_REQUIRE( ArrayQ::M == 5 );
  BOOST_REQUIRE( MatrixQ::M == 5 );
  BOOST_REQUIRE( MatrixQ::N == 5 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctor, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  SensorAdvectiveFlux adv(1.0, 0.0);
  SensorViscousFlux visc(1.0, 0.0, 0.0, 1.0);
  SensorSource source(1.0);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(adv, visc, source,
                   isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( avpde.D == 2 );
  BOOST_REQUIRE( avpde.N == 5 );

  // flux components
  BOOST_CHECK( avpde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( avpde.hasFluxAdvective() == true );
  BOOST_CHECK( avpde.hasFluxViscous() == true );
  BOOST_CHECK( avpde.hasSource() == true );
  BOOST_CHECK( avpde.hasSourceTrace() == false );
  BOOST_CHECK( avpde.hasSourceLiftedQuantity() == false );
  BOOST_CHECK( avpde.hasForcingFunction() == false );

  BOOST_CHECK( avpde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( avpde.needsSolutionGradientforSource() == false );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setDOFFrom )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> paramType;

  const Real tol = 1.e-13;

  SensorAdvectiveFlux adv(1.0, 0.0);
  SensorViscousFlux visc(1.0, 0.0, 0.0, 1.0);
  SensorSource source(1.0);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(adv, visc, source,
                   isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, Euler_ResidInterp_Raw);

  Real rho = 1.137;
  Real u = 0.784;
  Real v = -0.231;
  Real t = 0.987;
  Real p = R*rho*t;
  Real E = gas.energy(rho,t) + 0.5*(u*u + v*v);

  paramType H = {{0.5},{0.1, 0.3}};
  paramType paramL = log(H);

  Real s = 0.35; //sensor value

  // set
//  Real qDataPrim[5] = {rho, u, v, t};
//  string qNamePrim[5] = {"Density", "VelocityX", "VelocityY", "Temperature", "Sensor"};
  ArrayQ qTrue = {rho, u, v, p, s};
  ArrayQ q;
//  avpde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );
//  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
//  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
//  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
//  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
//  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

  DensityVelocityPressure2D<Real> qdata1;
  qdata1.Density = rho; qdata1.VelocityX = u; qdata1.VelocityY = v; qdata1.Pressure = p;
  AVVariable<DensityVelocityPressure2D, Real> var1( qdata1, s );
  q = 0;
  avpde.setDOFFrom( q, var1 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );


  DensityVelocityTemperature2D<Real> qdata2(rho, u, v, t);
  AVVariable<DensityVelocityTemperature2D, Real> var2( qdata2, s );
  q = 0;
  avpde.setDOFFrom( q, var2 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

  Conservative2D<Real> qdata3(rho, rho*u, rho*v, rho*E);
  AVVariable<Conservative2D, Real> var3( qdata3, s );
  q = 0;
  avpde.setDOFFrom( q, var3 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

  AVVariable<DensityVelocityPressure2D, Real> var4 = {{rho, u, v, p}, s};
  q = 0;
  avpde.setDOFFrom( q, var4 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );

  q = 0;
  q = avpde.setDOFFrom( var4 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );
  BOOST_CHECK_CLOSE( qTrue(3), q(3), tol );
  BOOST_CHECK_CLOSE( qTrue(4), q(4), tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> paramType;

  const int N = AVPDEClass::N;
  BOOST_REQUIRE( N == 5 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real us = 1.0;
  Real vs = 0.6;
  Real ksxx = 0.8, ksxy = 0.1, ksyx = 0.17, ksyy = 1.3;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs);
  SensorViscousFlux visc(ksxx, ksxy, ksyx, ksyy);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(adv, visc, source,
                   isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, Euler_ResidInterp_Raw);

  // function tests

  Real x, y, time;
  Real rho, u, v, t, p, e0, h0;
  Real Cv, Cp;

  x = y = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u + v*v);
  h0 = Cp*t + 0.5*(u*u + v*v);

  paramType H = {{0.5},{0.1, 0.3}};
  paramType paramL = log(H);
  paramType paramR = 0.5*log(H);

  Real s = 0.35;

  // set
  ArrayQ q = 0;
  AVVariable<DensityVelocityTemperature2D, Real> var( DensityVelocityTemperature2D<Real>(rho, u, v, t), s );
  avpde.setDOFFrom( q, var );

  // Now to calculate tau
  paramType Hsq = H*H;
  Real tau = 1.0;
  typedef DLA::VectorS<2, Real> VectorX;
  VectorX hPrincipal;
  DLA::EigenValues(H, hPrincipal);
  //Get minimum length scale
  Real hmin = std::numeric_limits<Real>::max();
  for (int d = 0; d < 2; d++)
    hmin = std::min(hmin, hPrincipal[d]);
  Real lambda = 0;
  avpde.speedCharacteristic( paramL, x, y, time, q, lambda );
  // characteristic time of the PDE
  tau = hmin/(3.0*order*lambda);

  // master state
  ArrayQ uTrue = {rho, rho*u, rho*v, rho*e0, s};
  ArrayQ uCons = 0;
  avpde.masterState( paramL, x, y, time, q, uCons );
  SANS_CHECK_CLOSE( uTrue(0), uCons(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(1), uCons(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(2), uCons(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(3), uCons(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(4), uCons(4), small_tol, close_tol );

  // Should not accumulate
  avpde.masterState( paramL, x, y, time, q, uCons );
  SANS_CHECK_CLOSE( uTrue(0), uCons(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(1), uCons(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(2), uCons(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(3), uCons(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(4), uCons(4), small_tol, close_tol );

  // conservative flux
  ArrayQ ftTrue = {rho, rho*u, rho*v, rho*e0, tau*s};
  ArrayQ ft = 0;
  avpde.fluxAdvectiveTime( paramL, x, y, time, q, ft );
  SANS_CHECK_CLOSE( ftTrue(0), ft(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( ftTrue(1), ft(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( ftTrue(2), ft(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( ftTrue(3), ft(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( ftTrue(4), ft(4), small_tol, close_tol );

  // Flux accumulation
  avpde.fluxAdvectiveTime( paramL, x, y, time, q, ft );
  for ( int i = 0; i < AVPDEClass::N; i++ )
  {
    SANS_CHECK_CLOSE( 2.*ftTrue[i], ft[i], small_tol, close_tol );
  }

  // advective flux
  ArrayQ fTrue = {rho*u, rho*u*u + p, rho*u*v, rho*u*h0, us*s};
  ArrayQ gTrue = {rho*v, rho*v*u, rho*v*v + p, rho*v*h0, vs*s};
  ArrayQ f = 0, g = 0;

  avpde.fluxAdvective( paramL, x, y, time, q, f, g );

  SANS_CHECK_CLOSE( fTrue(0), f(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(1), f(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(2), f(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(3), f(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(4), f(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(0), g(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(1), g(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(2), g(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(3), g(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( gTrue(4), g(4), small_tol, close_tol );

  // Flux accumulation
  avpde.fluxAdvective( paramL, x, y, time, q, f, g );
  for ( int i = 0; i < AVPDEClass::N; i++ )
  {
    SANS_CHECK_CLOSE( 2.*fTrue[i], f[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( 2.*gTrue[i], g[i], small_tol, close_tol );
  }


  // advective flux jacobian
  Real dfdudata[25] = {0, 1, 0, 0, 0,
                       -0.4810526, 1.2544, 0.0924, 0.4, 0,
                       0.181104, -0.231, 0.784, 0, 0,
                       -1.2404487984, 1.4699461, 0.0724416, 1.0976, 0,
                       0, 0, 0, 0, us};

  Real dgdudata[25] = {0, 0, 1, 0, 0,
                       0.181104, -0.231, 0.784, 0, 0,
                       0.0802424, -0.3136, -0.3696, 0.4, 0,
                       0.3654893781, 0.0724416, 1.6944641, -0.3234, 0,
                       0, 0, 0, 0, vs};

  MatrixQ dfduTrue( dfdudata, N*N );
  MatrixQ dgduTrue( dgdudata, N*N );
  MatrixQ dfdu, dgdu;
  dfdu = 0;  dgdu = 0;
  avpde.jacobianFluxAdvective( paramL, x, y, time, q, dfdu, dgdu );
  for (int i = 0; i < N; i++)
  for (int j = 0; j < N; j++)
  {
    SANS_CHECK_CLOSE( dfduTrue(i,j), dfdu(i,j), small_tol, close_tol );
    SANS_CHECK_CLOSE( dgduTrue(i,j), dgdu(i,j), small_tol, close_tol );
  }

  // Flux accumulation
  avpde.jacobianFluxAdvective( paramL, x, y, time, q, dfdu, dgdu );
  for (int i = 0; i < N; i++)
  for (int j = 0; j < N; j++)
  {
    SANS_CHECK_CLOSE( 2*dfduTrue(i,j), dfdu(i,j), small_tol, close_tol );
    SANS_CHECK_CLOSE( 2*dgduTrue(i,j), dgdu(i,j), small_tol, close_tol );
  }

  ArrayQ qx = {0.31, -0.21, 0.16, 0.67, 0.14};
  ArrayQ qy = {-0.24, -0.73, 0.56, 0.81, 0.39};

  //Make H really small so that the artificial viscosity terms in Euler go away.
  //We only want to test the sensor equation here. Artificial viscosity is tested elsewhere.
  H = {{1e-20},{0.0, 1e-20}};
  paramL = log(H);


  MatrixQ kxx, kxy;
  MatrixQ kyx, kyy;

  kxx = kxy =  0;
  kyx = kyy = 0;

  avpde.diffusionViscous( paramL, x, y,time,
                          q, qx, qy,
                          kxx, kxy,
                          kyx, kyy  );

#if 1

  avpde.speedCharacteristic( paramL, x, y, time, q, lambda );
  DLA::MatrixSymS<PhysD2::D,Real> avvisc = 1.0/(order+1) * H * lambda * s;

  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
    {
      if (i == N-1 && j == N-1)
      {
        SANS_CHECK_CLOSE( kxx(i,j), ksxx, small_tol, close_tol );
        SANS_CHECK_CLOSE( kxy(i,j), ksxy, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyx(i,j), ksyx, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyy(i,j), ksyy, small_tol, close_tol );
      }
      else
      {
        SANS_CHECK_CLOSE( kxx(i,j), avvisc(0,0), small_tol, close_tol );
        SANS_CHECK_CLOSE( kxy(i,j), avvisc(0,1), small_tol, close_tol );
        SANS_CHECK_CLOSE( kyx(i,j), avvisc(1,0), small_tol, close_tol );
        SANS_CHECK_CLOSE( kyy(i,j), avvisc(1,1), small_tol, close_tol );
      }
    }
#endif

//  paramL = 0;

  f = 0, g = 0;
  avpde.fluxViscous( paramL, x, y, time, q, qx, qy, f, g );

  SANS_CHECK_CLOSE( f(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(3), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(4), -(ksxx*qx(4) + ksxy*qy(4)), small_tol, close_tol );

  SANS_CHECK_CLOSE( g(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( g(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( g(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( g(3), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( g(4), -(ksyx*qx(4) + ksyy*qy(4)), small_tol, close_tol );

  // Flux accumulation
  avpde.fluxViscous( paramL, x, y, time, q, qx, qy, f, g );

  SANS_CHECK_CLOSE( f(0), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(1), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(2), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(3), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( f(4), 2*-(ksxx*qx(4) + ksxy*qy(4)), small_tol, close_tol );

  SANS_CHECK_CLOSE( g(0), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( g(1), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( g(2), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( g(3), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( g(4), 2*-(ksyx*qx(4) + ksyy*qy(4)), small_tol, close_tol );

  Real nx = 0.367, ny = -0.194;
  
  ArrayQ fL = 0, gL = 0;
  avpde.fluxViscous( paramL, x, y, time, q, qx, qy, fL, gL );
  ArrayQ fR = 0, gR = 0;
  avpde.fluxViscous( paramR, x, y, time, q, qx, qy, fR, gR );
  ArrayQ fnTrue = 0.5*(fL + fR)*nx + 0.5*(gL + gR)*ny;

  ArrayQ fn = 0.0;
  avpde.fluxViscous( paramL, paramR, x, y, time, q, qx, qy, q, qx, qy, nx, ny, fn );
  SANS_CHECK_CLOSE( fn(0), fnTrue(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fn(1), fnTrue(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fn(2), fnTrue(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( fn(3), fnTrue(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( fn(4), fnTrue(4), small_tol, close_tol );

  // Flux accumulation
  avpde.fluxViscous( paramL, paramR, x, y, time, q, qx, qy, q, qx, qy, nx, ny, fn );
  SANS_CHECK_CLOSE( fn(0), 2.0*fnTrue(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fn(1), 2.0*fnTrue(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fn(2), 2.0*fnTrue(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( fn(3), 2.0*fnTrue(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( fn(4), 2.0*fnTrue(4), small_tol, close_tol );

  ArrayQ src = 0;
  avpde.source( paramL, x, y, time, q, qx, qy, src );

  SANS_CHECK_CLOSE( src(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(3), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(4), as*s, small_tol, close_tol );

  // Source accumulation
  avpde.source( paramL, x, y, time, q, qx, qy, src );
  SANS_CHECK_CLOSE( src(0), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(1), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(2), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(3), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( src(4), 2*as*s, small_tol, close_tol );

  MatrixQ dsdu = 0;
  avpde.jacobianSource( paramL, x, y, time, q, qx, qy, dsdu );

  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
    {
      if (i == N-1 && j == N-1)
      {
        SANS_CHECK_CLOSE( dsdu(i,j), as, small_tol, close_tol );
      }
      else
      {
        SANS_CHECK_CLOSE( dsdu(i,j), 0.0, small_tol, close_tol );
      }
    }

  // Source accumulation
  avpde.jacobianSource( paramL, x, y, time, q, qx, qy, dsdu );

  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
    {
      if (i == N-1 && j == N-1)
      {
        SANS_CHECK_CLOSE( dsdu(i,j), 2*as, small_tol, close_tol );
      }
      else
      {
        SANS_CHECK_CLOSE( dsdu(i,j), 2*0.0, small_tol, close_tol );
      }
    }

  ArrayQ srcL = 0, srcR = 0;
  avpde.sourceTrace( paramL, x, y, paramL, x, y, time,
                     q, qx, qy, q, qx, qy, srcL, srcR );

  SANS_CHECK_CLOSE( srcL(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(3), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(4), as*s, small_tol, close_tol );

  SANS_CHECK_CLOSE( srcR(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(3), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(4), as*s, small_tol, close_tol );

  // Source accumulation
  avpde.sourceTrace( paramL, x, y, paramL, x, y, time,
                     q, qx, qy, q, qx, qy, srcL, srcR );

  SANS_CHECK_CLOSE( srcL(0), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(1), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(2), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(3), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(4), 2*as*s, small_tol, close_tol );

  SANS_CHECK_CLOSE( srcR(0), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(1), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(2), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(3), 2*0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(4), 2*as*s, small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AVEuler2D_PrimitiveStrongFlux )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> paramType;
  const int N = AVPDEClass::N;


  const Real tol = 1.e-12;
  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real us = 1.0;
  Real vs = 0.6;
  Real ksxx = 0.8, ksxy = 0.1, ksyx = 0.17, ksyy = 1.3;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs);
  SensorViscousFlux visc(ksxx, ksxy, ksyx, ksyy);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(adv, visc, source,
                   isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, Euler_ResidInterp_Raw);

  // function tests

  Real x, y, time;
  Real rho, u, v, t, p, e0;
  Real Cv, Cp;

  x = y = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  Cv = R/(gamma - 1);
  Cp = gamma*Cv;
  p  = R*rho*t;
  e0 = Cv*t + 0.5*(u*u + v*v);

  paramType Htensor = {{0.5},{0.0, 0.3}};
  paramType paramL = log(Htensor);

  Real s = 0.35;

  // set
  ArrayQ q = 0;
  DensityVelocityTemperature2D<Real> var(rho, u, v, t);
  AVVariable<DensityVelocityTemperature2D, Real> qdata( var, s );
  avpde.setDOFFrom( q, qdata );


  // Strong Conservative Flux
  Real rhot = -0.9;
  Real ut = -0.25;
  Real vt = 0.14;
  Real pt = 0.23;
  Real tt = (t/p) * pt - (t/rho)*rhot;

  Real e0t = Cv*tt + u*ut + v*vt;

  paramType Hsq = Htensor*Htensor;

  Real tau = 1.0;
  typedef DLA::VectorS<2, Real> VectorX;
  VectorX hPrincipal;
  DLA::EigenValues(Htensor, hPrincipal);

  //Get minimum length scale
  Real hmin = std::numeric_limits<Real>::max();
  for (int d = 0; d < 2; d++)
    hmin = std::min(hmin, hPrincipal[d]);

  Real lambda = 0;
  avpde.speedCharacteristic( paramL, x, y,  time, q, lambda );

  // characteristic time of the PDE
  tau = hmin/(3.0*Real(order)*lambda);

  Real dtau_dlambda = -1.0 / lambda / lambda * hmin / (3.0 * Real(order));
  // lambda = sqrt(u*u + v*v) + sqrt(gamma * p / rho)
  Real dlambda_drho = - sqrt(gamma*p/rho)  / 2.0 / rho;
  Real dlambda_du   = u / sqrt(u*u + v*v);
  Real dlambda_dv   = v / sqrt(u*u + v*v);
  Real dlambda_dp   = sqrt(gamma*p/rho)  / 2.0 / p;

  Real st = 0.49;

  ArrayQ qt = {rhot, ut, vt, pt, st};
  ArrayQ utCons, utConsTrue;
  utConsTrue[0] = rhot;
  utConsTrue[1] = rho*ut + rhot*u;
  utConsTrue[2] = rho*vt + rhot*v;
  utConsTrue[3] = rho*e0t + rhot*e0;
  utConsTrue[4] = tau*st + s*dtau_dlambda*(dlambda_drho*rhot + dlambda_du*ut + dlambda_dv*vt + dlambda_dp*pt);

  utCons = 0;
  avpde.strongFluxAdvectiveTime( paramL, x, y, time, q, qt, utCons );
  BOOST_CHECK_CLOSE( utConsTrue(0), utCons(0), tol );
  BOOST_CHECK_CLOSE( utConsTrue(1), utCons(1), tol );
  BOOST_CHECK_CLOSE( utConsTrue(2), utCons(2), tol );
  BOOST_CHECK_CLOSE( utConsTrue(3), utCons(3), tol );
  BOOST_CHECK_CLOSE( utConsTrue(4), utCons(4), tol );

  // Flux accumulation
  avpde.strongFluxAdvectiveTime( paramL, x, y, time, q, qt, utCons );
  BOOST_CHECK_CLOSE( 2*utConsTrue(0), utCons(0), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(1), utCons(1), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(2), utCons(2), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(3), utCons(3), tol );
  BOOST_CHECK_CLOSE( 2*utConsTrue(4), utCons(4), tol );

  // Strong Advective Flux
  Real rhox = -0.02;
  Real rhoy = 0.05;
  Real ux = 0.072;
  Real uy = -0.036;
  Real vx = -0.0234;
  Real vy = 0.0898;
  Real px = 10.2;
  Real py = 0.05;
  Real sx = 2.37;
  Real sy = -1.49;

  //PRIMITIVE SPECIFIC
  ArrayQ qx = {rhox, ux, vx, px, sx};
  ArrayQ qy = {rhoy, uy, vy, py, sy};

  Real tx = (t/p) * px - (t/rho)*rhox;
  Real ty = (t/p) * py - (t/rho)*rhoy;

  Real H = Cp*t + 0.5*(u*u + v*v);
  Real Hx = Cp*tx + (ux*u + vx*v);
  Real Hy = Cp*ty + (uy*u + vy*v);

  ArrayQ strongAdv = 0;
  ArrayQ strongAdvTrue = 0;
  strongAdvTrue(0) += rhox*u   +   rho*ux;
  strongAdvTrue(1) += rhox*u*u + 2*rho*ux*u + px;
  strongAdvTrue(2) += rhox*u*v +   rho*ux*v + rho*u*vx;
  strongAdvTrue(3) += rhox*u*H +   rho*ux*H + rho*u*Hx;
  strongAdvTrue(4) += us*sx;

  strongAdvTrue(0) += rhoy*v   +   rho*vy;
  strongAdvTrue(1) += rhoy*v*u +   rho*vy*u + rho*v*uy;
  strongAdvTrue(2) += rhoy*v*v + 2*rho*vy*v + py;
  strongAdvTrue(3) += rhoy*v*H +   rho*vy*H + rho*v*Hy;
  strongAdvTrue(4) += vs*sy;

  avpde.strongFluxAdvective( paramL, x, y, time, q, qx, qy, strongAdv );
  BOOST_CHECK_CLOSE( strongAdvTrue(0), strongAdv(0), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(1), strongAdv(1), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(2), strongAdv(2), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(3), strongAdv(3), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(4), strongAdv(4), tol );

  // Flux accumulation
  avpde.strongFluxAdvective( paramL, x, y, time, q, qx, qy, strongAdv );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(0), strongAdv(0), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(1), strongAdv(1), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(2), strongAdv(2), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(3), strongAdv(3), tol );
  BOOST_CHECK_CLOSE( 2*strongAdvTrue(4), strongAdv(4), tol );


  MatrixQ dudq = 0;
  MatrixQ dudqTrue = 0;

  ArrayQ rho_q = 0, u_q = 0, v_q = 0, t_q = 0, taus_q = 0;
  Real e_rho = 0, e_t = 0;
  avpde.variableInterpreter().evalJacobian( q, rho_q, u_q, v_q, t_q );
  Real e = avpde.gasModel().energy(rho, t);
  avpde.gasModel().energyJacobian(rho, t, e_rho, e_t);
  Real E = e + 0.5*(u*u + v*v);
  ArrayQ E_q = e_rho*rho_q + e_t*t_q + u*u_q + v*v_q ;
  taus_q[0] = s*dtau_dlambda*dlambda_drho;
  taus_q[1] = s*dtau_dlambda*dlambda_du;
  taus_q[2] = s*dtau_dlambda*dlambda_dv;
  taus_q[3] = s*dtau_dlambda*dlambda_dp;
  taus_q[4] = tau;

  for (int j = 0; j < N; j++)
    dudqTrue(0,j) = rho_q[j];

  for (int j = 0; j < N; j++)
    dudqTrue(1,j) = rho_q[j]*u + rho*u_q[j];

  for (int j = 0; j < N; j++)
    dudqTrue(2,j) = rho_q[j]*v + rho*v_q[j];

  for (int j = 0; j < N; j++)
    dudqTrue(3,j) = rho_q[j]*E + rho*E_q[j];

  dudqTrue(4,4) = 1.0;

  avpde.jacobianMasterState(paramL, x, y, time, q, dudq);
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      SANS_CHECK_CLOSE( dudq(i,j), dudqTrue(i,j), small_tol, close_tol );

  // Should not accumulate
  avpde.jacobianMasterState(paramL, x, y, time, q, dudq);
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      SANS_CHECK_CLOSE( dudq(i,j), dudqTrue(i,j), small_tol, close_tol );


  MatrixQ dFtdu = 0;
  MatrixQ dFtduTrue = 0;

  dFtduTrue = DLA::Identity();
  for (int j = 0; j < N; j++)
    dFtduTrue(4,j) += taus_q[j];

  avpde.jacobianFluxAdvectiveTime(paramL, x, y, time, q, dFtdu);
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      SANS_CHECK_CLOSE( dFtdu(i,j), dFtduTrue(i,j), small_tol, close_tol );

  // Should accumulate
  avpde.jacobianFluxAdvectiveTime(paramL, x, y, time, q, dFtdu);
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      SANS_CHECK_CLOSE( dFtdu(i,j), 2.0*dFtduTrue(i,j), small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxRoe, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> paramType;

  const int N = AVPDEClass::N;
  BOOST_REQUIRE( N == 5 );

  const Real tol = 2.e-11;

  Real us = 1.0;
  Real vs = 0.6;
  Real ksxx = 0.8, ksxy = 0.1, ksyx = 0.17, ksyy = 1.3;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs);
  SensorViscousFlux visc(ksxx, ksxy, ksyx, ksyy);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  PDEBaseClass pde(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source,
                   isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, Euler_ResidInterp_Raw);

  // Roe flux function test

  Real x, y, time;
  Real nx, ny;
  Real rhoL, uL, vL, tL, pL, h0L;
  Real rhoR, uR, vR, tR, pR, h0R;
  Real Cp;

  x = y = time = 0;   // not actually used in functions
  nx = 2.0/7.0; ny = sqrt(1.0-nx*nx);
  rhoL = 1.034; uL = 3.26; vL = -2.17; tL = 5.78;
  rhoR = 0.973; uR = 1.79; vR =  0.93; tR = 6.13;
  Cp  = R*gamma/(gamma - 1);
  pL  = R*rhoL*tL;
  h0L = Cp*tL + 0.5*(uL*uL + vL*vL);
  pR  = R*rhoR*tR;
  h0R = Cp*tR + 0.5*(uR*uR + vR*vR);

  paramType H = {{0.5},{0.0, 0.3}};
  paramType paramL = log(H);

  Real sL = 0.35;
  Real sR = 0.41;

  // set
  AVVariable<DensityVelocityTemperature2D, Real> qdataL = {{rhoL, uL, vL, tL}, sL};
  AVVariable<DensityVelocityTemperature2D, Real> qdataR = {{rhoR, uR, vR, tR}, sR};
  ArrayQ qL = avpde.setDOFFrom( qdataL );
  ArrayQ qR = avpde.setDOFFrom( qdataR );

  // advective normal flux (average)
  Real fL[N] = {rhoL*uL, rhoL*uL*uL + pL, rhoL*uL*vL     , rhoL*uL*h0L, us*sL};
  Real fR[N] = {rhoR*uR, rhoR*uR*uR + pR, rhoR*uR*vR     , rhoR*uR*h0R, us*sR};
  Real gL[N] = {rhoL*vL, rhoL*vL*uL     , rhoL*vL*vL + pL, rhoL*vL*h0L, vs*sL};
  Real gR[N] = {rhoR*vR, rhoR*vR*uR     , rhoR*vR*vR + pR, rhoR*vR*h0R, vs*sR};

  // Compute the average normal flux
  ArrayQ fnTrue;
  for (int k = 0; k < N; k++)
    fnTrue[k] = 0.5*(nx*(fL[k] + fR[k]) + ny*(gL[k] + gR[k]));

#if 1
  // Exact values for Roe scheme from Roe2D.nb
  fnTrue[0] -= -0.021574952725013058474459426733768;
  fnTrue[1] -= -1.7958753728220592172800048653566;
  fnTrue[2] -=  4.4515996249161671139371535471951;
  fnTrue[3] -= -6.8094427666218353919034995028459;

  fnTrue[4] -= 0.5*fabs(nx*us + ny*vs)*(sR - sL);
#endif

  ArrayQ fn = 0;
  avpde.fluxAdvectiveUpwind( paramL, x, y, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( fnTrue(3), fn(3), tol );
  BOOST_CHECK_CLOSE( fnTrue(4), fn(4), tol );

  // Flux accumulation
  avpde.fluxAdvectiveUpwind( paramL, x, y, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(2), fn(2), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(3), fn(3), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(4), fn(4), tol );

  MatrixQ mtx, avmtx;
  mtx = 0;
  avmtx = 0;
  avpde.jacobianFluxAdvectiveAbsoluteValue(paramL, x, y, time, qL, nx, ny, avmtx );
  pde.jacobianFluxAdvectiveAbsoluteValue(paramL, x, y, time, qL, nx, ny, mtx);
  for (int i = 0; i < MatrixQ::M; i++)
  {
    for (int j = 0; j < MatrixQ::N; j++)
    {
      BOOST_CHECK_CLOSE( mtx(i,j), avmtx(i,j), tol );
    }
  }

  Real rhox = -0.02;
  Real rhoy = 0.05;
  Real ux = 0.072;
  Real uy = -0.036;
  Real vx = -0.0234;
  Real vy = 0.0898;
  Real px = 10.2;
  Real py = 0.05;
  Real sx = 2.37;
  Real sy = -1.49;

  //PRIMITIVE SPECIFIC
  ArrayQ qx = {rhox, ux, vx, px, sx};
  ArrayQ qy = {rhoy, uy, vy, py, sy};

  ArrayQ srcL = 0, srcR = 0;
  avpde.sourceTrace( paramL, x, y, paramL, x, y, time,
                     qL, qx, qy, qR, qx, qy, srcL, srcR );

  BOOST_CHECK_CLOSE( srcL(0), 0.0, tol );
  BOOST_CHECK_CLOSE( srcL(1), 0.0, tol );
  BOOST_CHECK_CLOSE( srcL(2), 0.0, tol );
  BOOST_CHECK_CLOSE( srcL(3), 0.0, tol );
  BOOST_CHECK_CLOSE( srcL(4), as*sL, tol );

  BOOST_CHECK_CLOSE( srcR(0), 0.0, tol );
  BOOST_CHECK_CLOSE( srcR(1), 0.0, tol );
  BOOST_CHECK_CLOSE( srcR(2), 0.0, tol );
  BOOST_CHECK_CLOSE( srcR(3), 0.0, tol );
  BOOST_CHECK_CLOSE( srcR(4), as*sR, tol );

  // Source accumulation
  avpde.sourceTrace( paramL, x, y, paramL, x, y, time,
                     qL, qx, qy, qR, qx, qy, srcL, srcR );

  BOOST_CHECK_CLOSE( srcL(0), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcL(1), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcL(2), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcL(3), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcL(4), 2*as*sL, tol );

  BOOST_CHECK_CLOSE( srcR(0), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcR(1), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcR(2), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcR(3), 2*0.0, tol );
  BOOST_CHECK_CLOSE( srcR(4), 2*as*sR, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( speedCharacteristic, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> paramType;

  const Real tol = 1.e-13;

  Real us = 1.0;
  Real vs = 0.6;
  Real ksxx = 0.8, ksxy = 0.1, ksyx = 0.17, ksyy = 1.3;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs);
  SensorViscousFlux visc(ksxx, ksxy, ksyx, ksyy);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(adv, visc, source,
                   isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, Euler_ResidInterp_Raw);

  Real x, y, time, dx, dy;
  Real rho, u, v, t, c;
  Real speed, speedTrue;

  x = y = time = 0;   // not actually used in functions
  dx = 0.57; dy = -0.43;
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;
  c = sqrt(gamma*R*t);
  speedTrue = fabs(dx*u + dy*v)/sqrt(dx*dx + dy*dy) + c;

  paramType H = {{0.5},{0.0, 0.3}};
  paramType paramL = log(H);

  Real s = 0.35;

  AVVariable<DensityVelocityTemperature2D, Real> qdata = {{rho, u, v, t}, s};
  ArrayQ q = avpde.setDOFFrom( qdata );

  avpde.speedCharacteristic( paramL, x, y, time, dx, dy, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  avpde.speedCharacteristic( paramL, x, y, time, q, speed );
  BOOST_CHECK_CLOSE( sqrt(u*u + v*v) + c, speed, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( isValidState, QType, QTypes )
{
  // Surrogates are special
  // Entropy has log( x < 0 ) when constructing invalid states
  if ( std::is_same<QType,QTypePrimitiveSurrogate>::value ) return;
  if ( std::is_same<QType,QTypeEntropy>::value ) return;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  Real us = 1.0;
  Real vs = 0.6;
  Real ksxx = 0.8, ksxy = 0.1, ksyx = 0.17, ksyy = 1.3;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs);
  SensorViscousFlux visc(ksxx, ksxy, ksyx, ksyy);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(adv, visc, source,
                   isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, Euler_ResidInterp_Raw);

  ArrayQ q;
  Real rho, u = 0, v = 0, t;
  Real s = 0.35;

  rho = 1;
  t = 1;
  avpde.setDOFFrom( q, AVVariable<DensityVelocityTemperature2D, Real>({rho, u, v, t}, s) );
  BOOST_CHECK( avpde.isValidState(q) == true );

  rho = 1;
  t = -1;
  avpde.setDOFFrom( q, AVVariable<DensityVelocityTemperature2D, Real>({rho, u, v, t}, s) );
  BOOST_CHECK( avpde.isValidState(q) == false );

  rho = -1;
  t = 1;
  avpde.setDOFFrom( q, AVVariable<DensityVelocityTemperature2D, Real>({rho, u, v, t}, s) );
  BOOST_CHECK( avpde.isValidState(q) == false );

  rho = -1;
  t = -1;
  avpde.setDOFFrom( q, AVVariable<DensityVelocityTemperature2D, Real>({rho, u, v, t}, s) );
  BOOST_CHECK( avpde.isValidState(q) == false );

  rho = 1;
  t = 0;
  avpde.setDOFFrom( q, AVVariable<DensityVelocityTemperature2D, Real>({rho, u, v, t}, s) );
  BOOST_CHECK( avpde.isValidState(q) == false );

  rho = 0;
  t = 1;
  avpde.setDOFFrom( q, AVVariable<DensityVelocityTemperature2D, Real>({rho, u, v, t}, s) );
  BOOST_CHECK( avpde.isValidState(q) == false );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( interpResidBC )
{
  typedef QTypePrimitiveSurrogate QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  const int N = AVPDEClass::N;
  BOOST_REQUIRE( N == 5 );

  Real tol = 1.0e-12;

  Real us = 1.0;
  Real vs = 0.6;
  Real ksxx = 0.8, ksxy = 0.1, ksyx = 0.17, ksyy = 1.3;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs);
  SensorViscousFlux visc(ksxx, ksxy, ksyx, ksyy);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde1(adv, visc, source,
                    isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                    gas, Euler_ResidInterp_Raw);

  ArrayQ q;
  Real rho, u = 0, v = 0, t, s;

  rho = 1;
  t = 1;
  s = 0.35;

  avpde1.setDOFFrom( q, AVVariable<DensityVelocityTemperature2D, Real>({rho, u, v, t}, s) );
  BOOST_CHECK_EQUAL(avpde1.category(), 0);
  BOOST_CHECK_EQUAL(avpde1.nMonitor(), N);
  BOOST_CHECK( avpde1.isValidState(q) == true );

  DLA::VectorD<Real> vecout1(N);
  avpde1.interpResidVariable(q, vecout1);
  for (int i=0; i<N-1; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );
  SANS_CHECK_CLOSE( 0, vecout1(N-1), tol, tol );
  vecout1 = 0;
  avpde1.interpResidGradient(q, vecout1);
  for (int i=0; i<N; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );
  vecout1 = 0;
  avpde1.interpResidBC(q, vecout1);
  for (int i=0; i<N; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );


  AVPDEClass avpde2(adv, visc, source,
                    isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                    gas, Euler_ResidInterp_Momentum);
  BOOST_CHECK_EQUAL(avpde2.category(), 1);
  BOOST_CHECK_EQUAL(avpde2.nMonitor(), 4);

  DLA::VectorD<Real> vecout2(4);
  avpde2.interpResidVariable(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)+q(2)*q(2)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(3),vecout2(2), tol, tol);
  SANS_CHECK_CLOSE(0,vecout2(3), tol, tol);

  vecout2 = 0;
  avpde2.interpResidGradient(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)+q(2)*q(2)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(3),vecout2(2), tol, tol);
  SANS_CHECK_CLOSE(q(4),vecout2(3), tol, tol);

  vecout2 = 0;
  avpde2.interpResidBC(q, vecout2);
  SANS_CHECK_CLOSE(q(0),vecout2(0), tol, tol);
  SANS_CHECK_CLOSE(sqrt(q(1)*q(1)+q(2)*q(2)),vecout2(1), tol, tol);
  SANS_CHECK_CLOSE(q(3),vecout2(2), tol, tol);
  SANS_CHECK_CLOSE(q(4),vecout2(3), tol, tol);

  AVPDEClass avpde3(adv, visc, source,
                    isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                    gas, Euler_ResidInterp_Entropy);
  BOOST_CHECK_EQUAL(avpde3.nMonitor(), 1);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/ArtificialViscosity/PDEmitAVSensor2D_Euler_pattern.txt", true );

  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  Real us = 1.0;
  Real vs = 0.6;
  Real ksxx = 0.8, ksxy = 0.1, ksyx = 0.17, ksyy = 1.3;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs);
  SensorViscousFlux visc(ksxx, ksxy, ksyx, ksyy);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(adv, visc, source,
                   isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, Euler_ResidInterp_Raw);

  avpde.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
