// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AVSensor_ViscousFlux2D_btest
//
// test of 2-D diffusion matrix class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/PDEEulermitAVDiffusion2D.h"
#include "pde/NS/BCEulermitAVSensor2D.h"
#include "pde/NS/Fluids2D_Sensor.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( AVSensor_ViscousFlux2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  BOOST_CHECK( AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity>::D == 2 );
  BOOST_CHECK( AVSensor_DiffusionMatrix2D_GenHScale<TraitsSizeEulerArtificialViscosity>::D == 2 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Uniform )
{
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> Sensor_ViscousFlux;
  typedef Sensor_ViscousFlux::ArrayQ<Real> ArrayQ;
  typedef Sensor_ViscousFlux::MatrixQ<Real> MatrixQ;

  typedef TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
  typedef AVSensor_Source2D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real kxxTrue = 2.123, kxyTrue = 0.553;
  Real kyxTrue = 0.378, kyyTrue = 1.007;

  Sensor_ViscousFlux visc( kxxTrue, kxyTrue,
                           kyxTrue, kyyTrue );

  // static tests
  BOOST_CHECK( visc.D == 2 );
  BOOST_CHECK_EQUAL( visc.hasFluxViscous(), true );

  // Gas Model
  Real gamma, R;
  gamma = 1.4;
  R = 1;
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;
  GasModel gas(gasModelDict);
  // PDE
  bool isSteady = false;
  int order_pde = 1;
  EulerResidualInterpCategory interp = Euler_ResidInterp_Momentum;
  bool hasSpaceTimeDiffusion = false;
  PDEBaseClass pdeEulerAV(order_pde, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, eHarten);
  // Sensor equation terms
  Sensor sensor(pdeEulerAV);
  SensorAdvectiveFlux sensor_adv(0.0, 0.0);
  SensorViscousFlux sensor_visc(order_pde);
  SensorSource sensor_source(order_pde, sensor);
  // AV PDE with sensor equation
  AVPDEClass avpde(sensor_adv, sensor_visc, sensor_source, isSteady, order_pde,
                   hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, eHarten);

  Real s = 0.35;
  Real sx = 0.67;
  Real sy = 0.41;

  ArrayQ qx = {0.31, -0.21, 0.16, 0.67, sx};
  ArrayQ qy = {-0.24, -0.73, 0.56, 0.81, sy};

  Real rho = 1.137, u = 0.784, v = -0.231, t = 0.987;
  ArrayQ q = 0;
  AVVariable<DensityVelocityTemperature2D, Real> var( DensityVelocityTemperature2D<Real>(rho, u, v, t), s );
  avpde.setDOFFrom( q, var );
  Real param = 0.1;

  // functor
  Real x = 1;
  Real y = 0.5;
  Real time = 0;

  ArrayQ fx = 0, fy = 0;

  visc.fluxViscous(avpde, param, x, y, time, q, qx, qy, fx, fy);

  SANS_CHECK_CLOSE( fx(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fx(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fx(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fx(3), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fx(4), -(kxxTrue*qx(4) + kxyTrue*qy(4)), small_tol, close_tol );

  SANS_CHECK_CLOSE( fy(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fy(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fy(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fy(3), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fy(4), -(kyxTrue*qx(4) + kyyTrue*qy(4)), small_tol, close_tol );
  MatrixQ kxx, kxy;
  MatrixQ kyx, kyy;

  kxx = kxy = 0;
  kyx = kyy = 0;

  visc.diffusionViscous(avpde, param,
                        x, y, time,
                        q, qx, qy,
                        kxx, kxy,
                        kyx, kyy );

  for (int i = 0; i < AVPDEClass::N; i++)
    for (int j = 0; j < AVPDEClass::N; j++)
    {
      if (i == AVPDEClass::N-1 && j == AVPDEClass::N-1)
      {
        SANS_CHECK_CLOSE( kxx(i,j), kxxTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kxy(i,j), kxyTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyx(i,j), kyxTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyy(i,j), kyyTrue, small_tol, close_tol );
      }
      else
      {
        SANS_CHECK_CLOSE( kxx(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kxy(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyx(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyy(i,j), 0.0, small_tol, close_tol );
      }
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( GenHScale )
{
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> Sensor_ViscousFlux;
  typedef Sensor_ViscousFlux::ArrayQ<Real> ArrayQ;
  typedef Sensor_ViscousFlux::MatrixQ<Real> MatrixQ;
  typedef DLA::MatrixSymS<PhysD2::D,Real> MatrixSym;

  typedef TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
  typedef AVSensor_Source2D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real C = 5.0;

  Real hxx = 0.461, hyy = 0.912;
  MatrixSym logH = {{log(hxx)},
                    {0.0, log(hyy)}};
  Real kxxTrue = C*hxx*hxx;
  Real kxyTrue = 0.0, kyxTrue = 0.0;
  Real kyyTrue = C*hyy*hyy;

  int order = 1;
  Sensor_ViscousFlux visc(order);
  BOOST_CHECK_EQUAL( visc.hasFluxViscous(), true );

  // Gas Model
  Real gamma, R;
  gamma = 1.4;
  R = 1;
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;
  GasModel gas(gasModelDict);
  // PDE
  bool isSteady = false;
  int order_pde = 1;
  EulerResidualInterpCategory interp = Euler_ResidInterp_Momentum;
  bool hasSpaceTimeDiffusion = false;
  PDEBaseClass pdeEulerAV(order_pde, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, eHarten);
  // Sensor equation terms
  Sensor sensor(pdeEulerAV);
  SensorAdvectiveFlux sensor_adv(0.0, 0.0);
  SensorViscousFlux sensor_visc(order_pde);
  SensorSource sensor_source(order_pde, sensor);
  // AV PDE with sensor equation
  AVPDEClass avpde(sensor_adv, sensor_visc, sensor_source, isSteady, order_pde,
                   hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, eHarten);

  Real s = 0.35;
  Real sx = 0.67;
  Real sy = 0.41;

  ArrayQ qx = {0.31, -0.21, 0.16, 0.67, sx};
  ArrayQ qy = {-0.24, -0.73, 0.56, 0.81, sy};

  ArrayQ qxx = 0, qxy = 0, qyy = 0;

  Real rho = 1.137, u = 0.784, v = -0.231,  t = 0.987;
  ArrayQ q = 0;
  AVVariable<DensityVelocityTemperature2D, Real> var( DensityVelocityTemperature2D<Real>(rho, u, v, t), s );
  avpde.setDOFFrom( q, var );
  MatrixSym param = logH;

  // functor
  Real x = 1;
  Real y = 0.5;
  Real time = 0;

  ArrayQ fx = 0, fy = 0;

  visc.fluxViscous(avpde, param, x, y, time, q, qx, qy, fx, fy);

  SANS_CHECK_CLOSE( fx(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fx(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fx(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fx(3), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fx(4), -(kxxTrue*qx(4) + kxyTrue*qy(4)), small_tol, close_tol );

  SANS_CHECK_CLOSE( fy(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fy(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fy(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fy(3), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fy(4), -(kyxTrue*qx(4) + kyyTrue*qy(4)), small_tol, close_tol );

  MatrixQ kxx, kxy;
  MatrixQ kyx, kyy;

  kxx = kxy = 0;
  kyx = kyy  = 0;

  visc.diffusionViscous(avpde, param,
                        x, y, time,
                        q, qx, qy,
                        kxx, kxy,
                        kyx, kyy );

  for (int i = 0; i < AVPDEClass::N; i++)
    for (int j = 0; j < AVPDEClass::N; j++)
    {
      if (i == AVPDEClass::N-1 && j == AVPDEClass::N-1)
      {
        SANS_CHECK_CLOSE( kxx(i,j), kxxTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kxy(i,j), kxyTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyx(i,j), kyxTrue, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyy(i,j), kyyTrue, small_tol, close_tol );
      }
      else
      {
        SANS_CHECK_CLOSE( kxx(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kxy(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyx(i,j), 0.0, small_tol, close_tol );
        SANS_CHECK_CLOSE( kyy(i,j), 0.0, small_tol, close_tol );
      }
    }

  MatrixQ dfdu = 0, dgdu = 0;
  visc.jacobianFluxViscous(avpde, param,
                           x, y, time,
                           q, qx, qy,
                           dfdu, dgdu );

  for (int i = 0; i < AVPDEClass::N; i++)
  {
    for (int j = 0; j < AVPDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( dfdu(i,j), 0.0, small_tol, close_tol );
      SANS_CHECK_CLOSE( dgdu(i,j), 0.0, small_tol, close_tol );
    }
  }
  
  MatrixQ kxx_x = 0, kxy_x = 0, kyx_x = 0;
  MatrixQ kxy_y = 0, kyx_y = 0, kyy_y = 0;
  visc.diffusionViscousGradient(avpde, param,
                                x, y, time,
                                q, qx, qy,
                                qxx, qxy, qyy,
                                kxx_x, kxy_x, kyx_x,
                                kxy_y, kyx_y, kyy_y );

  for (int i = 0; i < AVPDEClass::N; i++)
  {
    for (int j = 0; j < AVPDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( kxx_x(i,j), 0.0, small_tol, close_tol );
      SANS_CHECK_CLOSE( kxy_x(i,j), 0.0, small_tol, close_tol );
      SANS_CHECK_CLOSE( kyx_x(i,j), 0.0, small_tol, close_tol );
      SANS_CHECK_CLOSE( kxy_y(i,j), 0.0, small_tol, close_tol );
      SANS_CHECK_CLOSE( kyx_y(i,j), 0.0, small_tol, close_tol );
      SANS_CHECK_CLOSE( kyy_y(i,j), 0.0, small_tol, close_tol );
    }
  }
  
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/ArtificialViscosity/AVSensor_ViscousFlux2D_pattern.txt", true );

  {
    typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> Sensor_ViscousFlux;
    Real kxxTrue = 2.123, kxyTrue = 0.553;
    Real kyxTrue = 0.378, kyyTrue = 1.007;

    Sensor_ViscousFlux visc( kxxTrue, kxyTrue,
                             kyxTrue, kyyTrue );
    visc.dump( 2, output );
  }

  {
    typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> Sensor_ViscousFlux;
    int order = 1;
    Sensor_ViscousFlux visc(order);
    visc.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
