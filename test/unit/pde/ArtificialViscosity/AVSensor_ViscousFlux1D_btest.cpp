// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AVSensor_ViscousFlux1D_btest
//
// test of 1-D diffusion matrix class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AdvectionDiffusion/TraitsAdvectionDiffusionArtificialViscosity.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"

#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/PDEEulermitAVDiffusion1D.h"
#include "pde/NS/BCEulermitAVSensor1D.h"
#include "pde/NS/Fluids1D_Sensor.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_Source1D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor1D.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( AVSensor_ViscousFlux1D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  BOOST_CHECK( AVSensor_ViscousFlux1D_Uniform<TraitsSizeADArtificialViscosity>::D == 1 );
  BOOST_CHECK( AVSensor_DiffusionMatrix1D_GridDependent<TraitsSizeADArtificialViscosity>::D == 1 );
  BOOST_CHECK( AVSensor_DiffusionMatrix1D_GenHScale<TraitsSizeADArtificialViscosity>::D == 1 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Uniform )
{
  typedef AVSensor_ViscousFlux1D_Uniform<TraitsSizeADArtificialViscosity> Sensor_ViscousFlux;
  typedef Sensor_ViscousFlux::ArrayQ<Real> ArrayQ;
  typedef Sensor_ViscousFlux::MatrixQ<Real> MatrixQ;
  const Real tol = 1.e-13;

  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEClass;
  Real u = 1.1;
  AdvectiveFlux1D_Uniform pde_adv(u);
  Real pde_kxx = 2.123;
  ViscousFlux1D_Uniform pde_visc(pde_kxx);
  Source1D_None pde_source;
  PDEClass pde(pde_adv, pde_visc, pde_source);

  Real kxxTrue =  1.33479;

  Sensor_ViscousFlux visc(kxxTrue);

  // static tests
  BOOST_CHECK( visc.D == 1 );
  BOOST_CHECK_EQUAL( visc.hasFluxViscous(), true );

  Real s = 0.35;
  Real sx = 0.67;
  Real sxx = 0.812;

  ArrayQ q  = {3.263, s};
  ArrayQ qx = {-0.445, sx};
  ArrayQ qxx = {4.230, sxx};
  Real param = 0.1;

  // functor
  Real x = 1;
  Real time = 0;

  ArrayQ fTrue = {0.0, -kxxTrue*sx};
  ArrayQ f = 0;

  visc.fluxViscous(pde, param, x, time, q, qx, f);
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );

  MatrixQ kxx = 0;

  visc.diffusionViscous(pde, param, x, time, q, qx, kxx);
  BOOST_CHECK_CLOSE( 0.0, kxx(0,0), tol );
  BOOST_CHECK_CLOSE( 0.0, kxx(0,1), tol );
  BOOST_CHECK_CLOSE( 0.0, kxx(1,0), tol );
  BOOST_CHECK_CLOSE( kxxTrue, kxx(1,1), tol );

  ArrayQ strongPDETrue = {0.0, -kxxTrue*sxx};

  ArrayQ strongPDE = 0;
  visc.strongFluxViscous(pde, param, x, time, q, qx, qxx, strongPDE);
  BOOST_CHECK_CLOSE( strongPDETrue(0), strongPDE(0), tol );
  BOOST_CHECK_CLOSE( strongPDETrue(1), strongPDE(1), tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( GridDependent )
{
  typedef AVSensor_ViscousFlux1D_GridDependent<TraitsSizeADArtificialViscosity> Sensor_ViscousFlux;
  typedef Sensor_ViscousFlux::ArrayQ<Real> ArrayQ;
  typedef Sensor_ViscousFlux::MatrixQ<Real> MatrixQ;
  const Real tol = 1.e-13;

  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEClass;
  Real u = 1.1;
  AdvectiveFlux1D_Uniform pde_adv(u);
  Real pde_kxx = 2.123;
  ViscousFlux1D_Uniform pde_visc(pde_kxx);
  Source1D_None pde_source;
  PDEClass pde(pde_adv, pde_visc, pde_source);

  Real C =  1.33479;
  Real h = 0.461;
  Real kxxTrue = C*h*h;

  Sensor_ViscousFlux visc(C);

  // static tests
  BOOST_CHECK( visc.D == 1 );
  BOOST_CHECK_EQUAL( visc.hasFluxViscous(), true );

  Real s = 0.35;
  Real sx = 0.67;
  Real sxx = 0.812;

  ArrayQ q  = {3.263, s};
  ArrayQ qx = {-0.445, sx};
  ArrayQ qxx = {4.230, sxx};
  Real param = h;

  // functor
  Real x = 1;
  Real time = 0;

  ArrayQ fTrue = {0.0, -kxxTrue*sx};
  ArrayQ f = 0;

  visc.fluxViscous(pde, param, x, time, q, qx, f);
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );

  MatrixQ kxx = 0;

  visc.diffusionViscous(pde, param, x, time, q, qx, kxx);
  BOOST_CHECK_CLOSE( 0.0, kxx(0,0), tol );
  BOOST_CHECK_CLOSE( 0.0, kxx(0,1), tol );
  BOOST_CHECK_CLOSE( 0.0, kxx(1,0), tol );
  BOOST_CHECK_CLOSE( kxxTrue, kxx(1,1), tol );

  ArrayQ strongPDETrue = {0.0, -kxxTrue*sxx};

  ArrayQ strongPDE = 0;
  visc.strongFluxViscous(pde, param, x, time, q, qx, qxx, strongPDE);
  BOOST_CHECK_CLOSE( strongPDETrue(0), strongPDE(0), tol );
  BOOST_CHECK_CLOSE( strongPDETrue(1), strongPDE(1), tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( GenHScale )
{
  typedef AVSensor_ViscousFlux1D_GenHScale<TraitsSizeEulerArtificialViscosity> Sensor_ViscousFlux;
  typedef Sensor_ViscousFlux::ArrayQ<Real> ArrayQ;
  typedef Sensor_ViscousFlux::MatrixQ<Real> MatrixQ;
  typedef DLA::MatrixSymS<PhysD1::D,Real> MatrixSym;

  typedef TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD1, PDEBaseClass> Sensor;
  typedef AVSensor_Source1D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real C = 5.0;

  Real hxx = 0.461;
  MatrixSym logH = {{log(hxx)}};
  Real kxxTrue = C*hxx*hxx;

  int order = 1;
  Sensor_ViscousFlux visc(order);

  // Gas Model
  Real gamma, R;
  gamma = 1.4;
  R = 1;
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;
  GasModel gas(gasModelDict);
  // PDE
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  int order_pde = 1;
  PDEBaseClass pdeEulerAV(order_pde, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEBaseClass::Euler_ResidInterp_Raw);
  // Sensor equation terms
  Sensor sensor(pdeEulerAV);
  SensorAdvectiveFlux sensor_adv(0.0);
  SensorViscousFlux sensor_visc(order_pde);
  SensorSource sensor_source(order_pde, sensor);
  // AV PDE with sensor equation
  AVPDEClass avpde(sensor_adv, sensor_visc, sensor_source, isSteady, order_pde, hasSpaceTimeDiffusion,
                   EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, AVPDEClass::Euler_ResidInterp_Raw);

  Real s = 0.35;
  Real sx = 0.67;

  ArrayQ qx = {0.31, -0.21, 0.67, sx};

  Real rho = 1.137, u = 0.784, t = 0.987;
  ArrayQ q = 0;
  AVVariable<DensityVelocityTemperature1D, Real> var( DensityVelocityTemperature1D<Real>(rho, u, t), s );
  avpde.setDOFFrom( q, var );
  MatrixSym param = logH;

  // functor
  Real x = 1;
  Real time = 0;

  ArrayQ fx = 0;

  visc.fluxViscous(avpde, param, x, time, q, qx, fx);

  SANS_CHECK_CLOSE( fx(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fx(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fx(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( fx(3), -(kxxTrue*qx(3)), small_tol, close_tol );

  MatrixQ kxx;

  kxx = 0;

  visc.diffusionViscous(avpde, param,
                         x, time,
                         q, qx,
                         kxx);

  for (int i = 0; i < AVPDEClass::N; i++)
    for (int j = 0; j < AVPDEClass::N; j++)
    {
      if (i == AVPDEClass::N-1 && j == AVPDEClass::N-1)
      {
        SANS_CHECK_CLOSE( kxx(i,j), kxxTrue, small_tol, close_tol );
      }
      else
      {
        SANS_CHECK_CLOSE( kxx(i,j), 0.0, small_tol, close_tol );
      }
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/ArtificialViscosity/AVSensor_ViscousFlux1D_pattern.txt", true );

  {
    typedef AVSensor_ViscousFlux1D_Uniform<TraitsSizeADArtificialViscosity> Sensor_ViscousFlux;
    Real kxx =  1.33479;
    Sensor_ViscousFlux visc(kxx);
    visc.dump( 2, output );
  }

  {
    typedef AVSensor_ViscousFlux1D_GridDependent<TraitsSizeADArtificialViscosity> Sensor_ViscousFlux;
    Real C =  1.33479;
    Sensor_ViscousFlux visc(C);
    visc.dump( 2, output );
  }

  {
    typedef AVSensor_ViscousFlux1D_GenHScale<TraitsSizeADArtificialViscosity> Sensor_ViscousFlux;
    Real C =  1.33479;
    Sensor_ViscousFlux visc(C);
    visc.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
