// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AVSensor_Source1D_Euler_btest
// test for the 1-D source class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/PDEEulermitAVDiffusion1D.h"
#include "pde/NS/BCEulermitAVSensor1D.h"
#include "pde/NS/Fluids1D_Sensor.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_Source1D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor1D.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( AVSensor_Source1D_Euler_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AVSensor_Source1D_Jump_test )
{
  typedef TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD1, PDEBaseClass> Sensor;
  typedef AVSensor_Source1D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;
  typedef AVPDEClass::ArrayQ<Real> ArrayQ;
  typedef AVPDEClass::MatrixQ<Real> MatrixQ;
  typedef DLA::MatrixSymS<PhysD1::D,Real> MatrixSym;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  int order = 1;

  // Gas Model
  Real gamma, R;
  gamma = 1.4;
  R = 1;
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;
  GasModel gas(gasModelDict);
  // PDE
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  PDEBaseClass pdeEulerAV(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEBaseClass::Euler_ResidInterp_Raw);
  // Sensor equation terms
  Sensor sensor(pdeEulerAV);
  SensorAdvectiveFlux sensor_adv(0.0);
  SensorViscousFlux sensor_visc(order);
  SensorSource sensor_source(order, sensor);
  // AV PDE with sensor equation
  AVPDEClass avpde(sensor_adv, sensor_visc, sensor_source, isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, PDEBaseClass::Euler_ResidInterp_Raw);

  BOOST_CHECK( sensor_source.hasSourceTerm() == true );
  BOOST_CHECK( sensor_source.hasSourceTrace() == true );
  BOOST_CHECK( sensor_source.hasSourceLiftedQuantity() == true );
  BOOST_CHECK( sensor_source.needsSolutionGradientforSource() == false );

  Real x = 1;
  Real time = 0;

  Real qsL = 2.45;
  Real qsR = 3.45;
  Real qsxL = 5.0;
  Real qsxR = 3.0;

  ArrayQ qxL = {0.31, -0.21, 0.67, qsxL};
  ArrayQ qxR = {0.31, -0.21, 0.67, qsxR};

  Real rho = 1.137, u = 0.784, pL = 2.45, pR = 2.75;
  ArrayQ qL = 0, qR = 0;
  AVVariable<DensityVelocityPressure1D, Real> varL( DensityVelocityPressure1D<Real>(rho, u, pL), qsL );
  avpde.setDOFFrom( qL, varL );
  AVVariable<DensityVelocityPressure1D, Real> varR( DensityVelocityPressure1D<Real>(rho, u, pR), qsR );
  avpde.setDOFFrom( qR, varR );


  Real hxx = 0.461;
  MatrixSym logH = {{log(hxx)}};
  MatrixSym param = logH;

  Real liftedQuantity = 0.0083;
  Real Sk = log10(liftedQuantity + 1e-16);

#if 0

  Real psi0 = 1;
  if ( PDEorder_ > 0 )
    psi0 = 3.0 + 2.0*log10(PDEorder_);

  Tlq xi = Sk + psi0;

#if 0
  // Add +0.5 to center around smooth activiation
  xi += 0.5;
  Tlq sensor = smoothActivation_tanh(xi);
#else
  xi += 0.5; // Add +0.5 to center around smooth activiation
  xi /= 2;   // Scale so it activates over 2 orders of magnitude
#endif

#else
  const Real Sk_min = -4;
  const Real Sk_max = -1;
  Real xi = (Sk - Sk_min)/(Sk_max - Sk_min);
#endif

  Real alpha = 10;
  Real sensor_value = smoothActivation_exp(xi, alpha);


  ArrayQ sTrue = {0, 0, 0, qsL - sensor_value};
  ArrayQ s = {0, 0, 0, 0};

  BOOST_CHECK_THROW( sensor_source.source(avpde, param, x, time, qL, qxL, s), DeveloperException );

  sensor_source.source(avpde, param, x, time, liftedQuantity, qL, qxL, s);
  BOOST_CHECK_EQUAL( sTrue(0), s(0) );
  BOOST_CHECK_EQUAL( sTrue(1), s(1) );
  BOOST_CHECK_EQUAL( sTrue(2), s(2) );
  BOOST_CHECK_EQUAL( sTrue(3), s(3) );

  Real dg = pL - pR;
  dg = smoothabs0(dg, 1.0e-5);

  Real gbar = 0.5*(fabs(pL) + fabs(pR));

  Real jump = dg / gbar;
  Real liftedQuantityTrue = smoothabs0(jump, 0.03);

  liftedQuantity = 0;
  sensor_source.sourceLiftedQuantity(param, x, param, x, time, qL, qR, liftedQuantity);
  SANS_CHECK_CLOSE( liftedQuantityTrue, liftedQuantity, small_tol, close_tol );

  ArrayQ sLTrue = {0, 0, 0, 0};
  ArrayQ sRTrue = {0, 0, 0, 0};
  ArrayQ sL = 0, sR = 0;
  sensor_source.sourceTrace(avpde, param, x, param, x, time, qL, qxL, qR, qxR, sL, sR);
  SANS_CHECK_CLOSE( sLTrue(0), sL(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( sLTrue(1), sL(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( sLTrue(2), sL(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( sLTrue(3), sL(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue(0), sR(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue(1), sR(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue(2), sR(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue(3), sR(3), small_tol, close_tol );

  MatrixQ dsduTrue = {{0, 0, 0, 0},
                      {0, 0, 0, 0},
                      {0, 0, 0, 0},
                      {0, 0, 0, 1}};
  MatrixQ dsdu = 0.0;

  //TODO: We need to remove this Jacobian from PseudoTime!
#if 0
  sensor_source.jacobianSource(avpde, param, x, y, time, qL, qxL, qyL, dsdu);
  for (int i = 0; i < AVPDEClass::N; i++)
    for (int j = 0; j < AVPDEClass::N; j++)
      SANS_CHECK_CLOSE( dsduTrue(i,j), dsdu(i,j), small_tol, close_tol );
#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
