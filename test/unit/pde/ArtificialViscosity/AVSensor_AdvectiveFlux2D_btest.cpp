// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AVSensor_AdvectiveFlux1D_btest
//
// test of 2-D advection velocity class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/NS/TraitsEulerArtificialViscosity.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( AVSensor_AdvectiveFlux2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Uniform )
{
  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;

  typedef SensorAdvectiveFlux::ArrayQ<Real> ArrayQ;
  typedef SensorAdvectiveFlux::MatrixQ<Real> MatrixQ;

  Real small_tol = 1e-12;
  Real close_tol = 1e-12;

  Real nx = 0.65;
  Real ny = -0.27;
  Real nt = 0.5;
  Real x = 1, y = 0.5;
  Real time = 0;

  Real us = 0.35, vs = 0.81;
  SensorAdvectiveFlux adv(us, vs);

  BOOST_CHECK( adv.hasFluxAdvective() == true );

  Real rhoL = 1.137, rhoR = 1.849;
  Real uL = 0.784, uR = 0.417;
  Real vL = 0.581, vR = 0.938;
  Real tL = 0.987, tR = 0.866;
  Real sL = 0.342, sR = 0.401;

  Real rhoLx = 0.234, rhoLy = 0.745;
  Real uLx = 0.721, uLy = -0.345;
  Real vLx = -0.203, vLy = 0.982;
  Real tLx = 1.442, tLy = -0.145;
  Real sLx = -0.312, sLy = 0.751;

  Real param = 0.1;

  ArrayQ qL = {rhoL, uL, vL, tL, sL};
  ArrayQ qR = {rhoR, uR, vR, tR, sR};

  ArrayQ qx = {rhoLx, uLx, vLx, tLx, sLx};
  ArrayQ qy = {rhoLy, uLy, vLy, tLy, sLy};

  ArrayQ fxTrue = {0.0, 0.0, 0.0, 0.0, us*sL};
  ArrayQ fyTrue = {0.0, 0.0, 0.0, 0.0, vs*sL};
  ArrayQ fx = 0.0, fy = 0.0;
  adv.flux(param, x, y, time, qL, fx, fy);
  SANS_CHECK_CLOSE( fxTrue(0), fx(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fxTrue(1), fx(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fxTrue(2), fx(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( fxTrue(3), fx(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( fxTrue(4), fx(4), small_tol, close_tol );

  SANS_CHECK_CLOSE( fyTrue(0), fy(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fyTrue(1), fy(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fyTrue(2), fy(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( fyTrue(3), fy(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( fyTrue(4), fy(4), small_tol, close_tol );

  ArrayQ fnTrue = {0.0, 0.0, 0.0, 0.0, us*sL*nx + vs*sL*ny};
  ArrayQ fn = 0.0;
  adv.fluxUpwind(param, x, y, time, qL, qR, nx, ny, fn);
  SANS_CHECK_CLOSE( fnTrue(0), fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fnTrue(1), fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fnTrue(2), fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( fnTrue(3), fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( fnTrue(4), fn(4), small_tol, close_tol );

  fnTrue = {0.0, 0.0, 0.0, 0.0, us*sL*nx + vs*sL*ny + sL*nt};
  fn = 0.0;
  adv.fluxUpwindSpaceTime(param, x, y, time, qL, qR, nx, ny, nt, false, fn); //unsteady sensor
  SANS_CHECK_CLOSE( fnTrue(0), fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fnTrue(1), fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fnTrue(2), fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( fnTrue(3), fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( fnTrue(4), fn(4), small_tol, close_tol );

  fnTrue = {0.0, 0.0, 0.0, 0.0, us*sL*nx + vs*sL*ny};
  fn = 0.0;
  adv.fluxUpwindSpaceTime(param, x, y, time, qL, qR, nx, ny, nt, true, fn); //steady sensor
  SANS_CHECK_CLOSE( fnTrue(0), fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fnTrue(1), fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fnTrue(2), fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( fnTrue(3), fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( fnTrue(4), fn(4), small_tol, close_tol );

  MatrixQ axTrue = 0, ayTrue = 0;
  axTrue(4,4) = us;
  ayTrue(4,4) = vs;
  MatrixQ ax = 0.0;
  MatrixQ ay = 0.0;
  adv.jacobian(param, x, y, time, qL, ax, ay);
  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
    {
      SANS_CHECK_CLOSE( axTrue(i,j), ax(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( ayTrue(i,j), ay(i,j), small_tol, close_tol );
    }

  ArrayQ strongPDETrue = {0.0, 0.0, 0.0, 0.0, us*sLx + vs*sLy};
  ArrayQ strongPDE = 0.0;
  adv.strongFlux(param, x, y, time, qL, qx, qy, strongPDE);
  SANS_CHECK_CLOSE( strongPDETrue(0), strongPDE(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( strongPDETrue(1), strongPDE(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( strongPDETrue(2), strongPDE(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( strongPDETrue(3), strongPDE(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( strongPDETrue(4), strongPDE(4), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D_pattern.txt", true );

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;

  Real u =  1.33479;
  Real v =  -0.3412;
  SensorAdvectiveFlux adv(u,v);
  adv.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
