// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDESourceOnly2D_btest
//
// test of 2-D Advection-Diffusion PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;
#include <boost/mpl/list.hpp>

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"

#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/PDEEulermitAVDiffusion2D.h"
#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Fluids2D_Sensor.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/SourceOnly/PDESourceOnly2D.h"


using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
// Advection diffusion
template class PDEAdvectionDiffusion<PhysD2,
                                     AdvectiveFlux2D_Uniform,
                                     ViscousFlux2D_Uniform,
                                     Source2D_None>;

template class PDESourceOnly2D<PDEAdvectionDiffusion<PhysD2,
                                                     AdvectiveFlux2D_Uniform,
                                                     ViscousFlux2D_Uniform,
                                                     Source2D_None> >;

// Euler
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
class QTypeEntropy {};
template class TraitsSizeEuler<PhysD2>;
template class TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>;
template class TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>;
template class TraitsModelEuler<QTypeConservative, GasModel>;
template class TraitsModelEuler<QTypeEntropy, GasModel>;
template class PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>>;
template class PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>>;
template class PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypeConservative, GasModel>>;
template class PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypeEntropy, GasModel>>;
template class PDESourceOnly2D<PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>>>;
template class PDESourceOnly2D<PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>>>;
template class PDESourceOnly2D<PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypeConservative, GasModel>>>;
template class PDESourceOnly2D<PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QTypeEntropy, GasModel>>>;
}


using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDESourceOnly2D_test_suite )

typedef boost::mpl::list< QTypePrimitiveRhoPressure,
                          QTypeConservative,
                          QTypePrimitiveSurrogate,
                          QTypeEntropy > QTypes;


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_AD_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None> PDEBaseClass;
  typedef PDESourceOnly2D<PDEBaseClass> PDEClass;

  BOOST_CHECK( PDEClass::D == PDEBaseClass::D );
  BOOST_CHECK( PDEClass::N == PDEBaseClass::N );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors_AD_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None> PDEBaseClass;
  typedef PDESourceOnly2D<PDEBaseClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  ArrayQ a = 0.0;
  PDEClass pde( a, adv, visc, source );

  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( source_AD_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None> PDEBaseClass;
  typedef PDESourceOnly2D<PDEBaseClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real h = 0.1;
  DLA::MatrixSymS<2,Real> logH = {{log(h)}, {0, log(h)}};

  Real x = 0.0, y = 0.0, time = 0.0;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  ArrayQ a = 0.0;
  PDEClass pde( a, adv, visc, source );

  ArrayQ q = 1.0;
  ArrayQ qx = 0.0, qy = 0.0;

  Real lambda = sqrt(u*u + v*v);
  Real tau = h/lambda;
  Real sourceTrue = 1.0 / tau * (q - a);

  ArrayQ src = 0.0;
  pde.source(logH, x, y, time, q, qx, qy, src);
  BOOST_CHECK_CLOSE( sourceTrue, src, tol );

  ArrayQ qp = 0, qpx = 0, qpy = 0;
  src = 0.0;
  pde.source(logH, x, y, time, q, qp, qx, qy, qpx, qpy, src);
  BOOST_CHECK_CLOSE( sourceTrue, src, tol );

  MatrixQ dsdqTrue = 1.0 / tau;
  MatrixQ dsdq = 0.0;
  pde.jacobianSource(logH, x, y, time, q, qx, qy, dsdq);
  BOOST_CHECK_CLOSE( dsdqTrue, dsdq, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux_AD_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None> PDEBaseClass;
  typedef PDESourceOnly2D<PDEBaseClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real h = 0.1;
  DLA::MatrixSymS<2,Real> logH = {{log(h)}, {0, log(h)}};

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  typedef ForcingFunction2D_Const<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(1) );

  ArrayQ a = 0.0;
  PDEClass pde( a, adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components - should come off the base type
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // function tests

  Real x, y, time;
  Real sln, slnx, slny;

  x = 0; y = 0, time = 0;
  sln  =  3.263;
  slnx = -0.445;
  slny =  1.741;

  DLA::VectorS<2,Real> X = {x,y};


  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = qDataPrim[0];
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  // master state
  ArrayQ uCons = 0;
  pde.masterState( logH, x, y, time, q, uCons );
  BOOST_CHECK_CLOSE( qTrue, uCons, tol );

  // unsteady flux
  ArrayQ ft = 0;
  pde.fluxAdvectiveTime( logH, x, y, time, q, ft );
  BOOST_CHECK_CLOSE( qTrue, ft, tol );

  pde.fluxAdvectiveTime( logH, x, y, time, q, ft );
  BOOST_CHECK_CLOSE( 2*qTrue, ft, tol );

  MatrixQ dudq = 0;
  pde.jacobianMasterState( logH, x, y, time, q, dudq );
  BOOST_CHECK_CLOSE( 1, dudq, tol );

  // advective flux
  ArrayQ fTrue = 0;
  ArrayQ gTrue = 0;
  ArrayQ f, g;
  DLA::VectorS<2,ArrayQ> F;
  f = 0;
  g = 0;
  pde.fluxAdvective( logH, x, y, time, q, f, g );
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( gTrue, g, tol );

  // advective flux jacobian
  MatrixQ dfdu = 0;
  MatrixQ dgdu = 0;
  pde.jacobianFluxAdvective( logH, x, y, time, q, dfdu, dgdu );
  BOOST_CHECK_CLOSE( 0.0, dfdu, tol );
  BOOST_CHECK_CLOSE( 0.0, dgdu, tol );

  // set gradient
  qDataPrim[0] = slnx;
  ArrayQ qxTrue = qDataPrim[0];
  ArrayQ qx;
  pde.setDOFFrom( qx, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxTrue, qx, tol );

  qDataPrim[0] = slny;
  ArrayQ qyTrue = qDataPrim[0];
  ArrayQ qy;
  pde.setDOFFrom( qy, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyTrue, qy, tol );

  DLA::VectorS<2, ArrayQ> gradq = {qx, qy};

  // diffusive flux
  fTrue = 0.0;
  gTrue = 0.0;
  f = 0;
  g = 0;
  pde.fluxViscous( logH, x, y, time, q, qx, qy, f, g );
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( gTrue, g, tol );

  // diffusive flux jacobian
  MatrixQ kxxMtx = 0, kxyMtx = 0, kyxMtx = 0, kyyMtx = 0;
  pde.diffusionViscous( logH, x, y, time,
                        q, qx, qy,
                        kxxMtx, kxyMtx,
                        kyxMtx, kyyMtx );
  BOOST_CHECK_CLOSE( 0.0, kxxMtx, tol );
  BOOST_CHECK_CLOSE( 0.0, kxyMtx, tol );
  BOOST_CHECK_CLOSE( 0.0, kyxMtx, tol );
  BOOST_CHECK_CLOSE( 0.0, kyyMtx, tol );

  Real nx = 1.22, ny = -0.432;
  DLA::VectorS<2, Real> N = {nx, ny};

  Real slnL = 3.26, slnR = 1.79;
  // set
  Real qLDataPrim[1] = {slnL};
  string qLNamePrim[1] = {"Solution"};
  ArrayQ qLTrue = qLDataPrim[0];
  ArrayQ qL;
  pde.setDOFFrom( qL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qLTrue, qL, tol );

  Real qRDataPrim[1] = {slnR};
  string qRNamePrim[1] = {"Solution"};
  ArrayQ qRTrue = qRDataPrim[0];
  ArrayQ qR;
  pde.setDOFFrom( qR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qRTrue, qR, tol );

  // advective normal flux
  ArrayQ fnTrue = 0.0;
  ArrayQ fn;
  fn = 0;
  pde.fluxAdvectiveUpwind( logH, x, y, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fnTrue, fn, tol );

  // viscous flux function
  Real slnxL, slnyL;
  Real slnxR, slnyR;

  slnxL = 1.325;  slnyL = -0.457;
  slnxR = 0.327;  slnyR =  3.421;

  // set gradient
  qLDataPrim[0] = slnxL;
  ArrayQ qxLTrue = qLDataPrim[0];
  ArrayQ qxL;
  pde.setDOFFrom( qxL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxLTrue, qxL, tol );

  qLDataPrim[0] = slnyL;
  ArrayQ qyLTrue = qLDataPrim[0];
  ArrayQ qyL;
  pde.setDOFFrom( qyL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyLTrue, qyL, tol );

  DLA::VectorS<2, ArrayQ> gradqL = {qxL, qyL};

  qRDataPrim[0] = slnxR;
  ArrayQ qxRTrue = qRDataPrim[0];
  ArrayQ qxR;
  pde.setDOFFrom( qxR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxRTrue, qxR, tol );

  qRDataPrim[0] = slnyR;
  ArrayQ qyRTrue = qRDataPrim[0];
  ArrayQ qyR;
  pde.setDOFFrom( qyR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyRTrue, qyR, tol );

  DLA::VectorS<2, ArrayQ> gradqR = {qxR, qyR};

  // viscous normal flux
  fnTrue = 0.0;
  fn = 0;
  pde.fluxViscous( logH, logH, x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fnTrue, fn, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState_AD_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None> PDEBaseClass;
  typedef PDESourceOnly2D<PDEBaseClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  ArrayQ a = 0.0;
  PDEClass pde( a, adv, visc, source );

  ArrayQ q = 0;
  BOOST_CHECK( pde.isValidState(q) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( static_Euler_test, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEBaseClass;
  typedef PDESourceOnly2D<PDEBaseClass> PDEClass;

  BOOST_CHECK( PDEClass::D == PDEBaseClass::D );
  BOOST_CHECK( PDEClass::N == PDEBaseClass::N );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctors_Euler_test, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEBaseClass;
  typedef PDESourceOnly2D<PDEBaseClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEBaseClass pdebase(gas, Euler_ResidInterp_Raw);

  Real rho = 1.137;
  Real u = 0.784;
  Real v = -0.231;
  Real t = 0.987;

  // set
  Real qDataPrim[4] = {rho, u, v, t};
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY", "Temperature"};
  ArrayQ a = 0;
  pdebase.setDOFFrom( a, qDataPrim, qNamePrim, 4 );

  PDEClass pde1(a, gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( pde1.D == 2 );
  BOOST_REQUIRE( pde1.N == 4 );

  // flux components
  BOOST_CHECK( pde1.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde1.hasFluxAdvective() == true );
  BOOST_CHECK( pde1.hasFluxViscous() == false );
  BOOST_CHECK( pde1.hasSource() == true );
  BOOST_CHECK( pde1.hasForcingFunction() == false );

  BOOST_CHECK( pde1.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde1.needsSolutionGradientforSource() == false );

  // set invalid state
  if (std::is_same<QType,QTypePrimitiveSurrogate>::value) return;
  qDataPrim[3] = -1.0*t;
  a = 0;
  pdebase.setDOFFrom( a, qDataPrim, qNamePrim, 4 );
  BOOST_CHECK_THROW( PDEClass pde2(a, gas, Euler_ResidInterp_Raw) , AssertionException);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( source_Euler_test, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEBaseClass;
  typedef PDESourceOnly2D<PDEBaseClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real h = 0.1;
  DLA::MatrixSymS<2,Real> logH = {{log(h)}, {0, log(h)}};

  Real x = 0.0, y = 0.0, time = 0.0;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEBaseClass pdebase(gas, Euler_ResidInterp_Raw);

  Real rho = 2.15;
  Real u = 0.131;
  Real v = -0.864;
  Real t = 0.432;

  // set
  Real qDataPrim[4] = {rho, u, v, t};
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY", "Temperature"};
  ArrayQ a = 0;
  pdebase.setDOFFrom( a, qDataPrim, qNamePrim, 4 );

  PDEClass pde(a, gas, Euler_ResidInterp_Raw);

  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;

  // set
  ArrayQ q = 0;
  pdebase.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );


  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;

  ArrayQ qx = {rhox, ux, vx, px};
  ArrayQ qy = {rhoy, uy, vy, py};

  Real lambda = 0.0;
  pdebase.speedCharacteristic(x, y, time, a, lambda);
  Real tau = h/lambda;
  MatrixQ dudq = 0;
  pdebase.jacobianMasterState(x, y, time, a, dudq);
  ArrayQ sourceTrue = 1.0 / tau * dudq * (q - a);

  ArrayQ src = 0.0;
  pde.source(logH, x, y, time, q, qx, qy, src);
  for (int i = 0; i < PDEClass::N; i++)
  {
    BOOST_CHECK_CLOSE( sourceTrue(i), src(i), tol );
  }

  ArrayQ qp = 0, qpx = 0, qpy = 0;
  src = 0.0;
  pde.source(logH, x, y, time, q, qp, qx, qy, qpx, qpy, src);
  for (int i = 0; i < PDEClass::N; i++)
  {
    BOOST_CHECK_CLOSE( sourceTrue(i), src(i), tol );
  }

  MatrixQ dsdqTrue = 1.0 / tau * dudq;
  MatrixQ dsdq = 0.0;
  pde.jacobianSource(logH, x, y, time, q, qx, qy, dsdq);
  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      BOOST_CHECK_CLOSE( dsdqTrue(i,j), dsdq(i,j), tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( static_EulermitAVSensor_test, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDENonAVClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDENonAVClass> Sensor;
  typedef AVSensor_Source2D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDENonAVClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> PDEBaseClass;
  typedef PDESourceOnly2D<PDEBaseClass> PDEClass;

  BOOST_CHECK( PDEClass::D == PDEBaseClass::D );
  BOOST_CHECK( PDEClass::N == PDEBaseClass::N );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctors_EulermitAVSensor_test, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDENonAVClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDENonAVClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> PDEBaseClass;
  typedef PDESourceOnly2D<PDEBaseClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;

  SensorAdvectiveFlux adv(1.0, 0.0);
  SensorViscousFlux visc(1.0, 0.0, 0.0, 1.0);
  SensorSource source(1.0);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEBaseClass pdebase(adv, visc, source,
                       isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                       gas, Euler_ResidInterp_Raw);

  Real rho = 1.137;
  Real u = 0.784;
  Real v = -0.231;
  Real t = 0.987;
  Real s = 1.0;

  // set
  AVVariable<DensityVelocityPressure2D, Real> var = {{rho, u, v, t}, s};
  ArrayQ a = 0;
  pdebase.setDOFFrom(a, var);

  PDEClass pde1(a,
                adv, visc, source,
                isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( pde1.D == 2 );
  BOOST_REQUIRE( pde1.N == 5 );

  // flux components
  BOOST_CHECK( pde1.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde1.hasFluxAdvective() == true );
  BOOST_CHECK( pde1.hasFluxViscous() == true );
  BOOST_CHECK( pde1.hasSource() == true );
  BOOST_CHECK( pde1.hasForcingFunction() == false );

  BOOST_CHECK( pde1.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde1.needsSolutionGradientforSource() == false );

  // set invalid state
  if (std::is_same<QType,QTypePrimitiveSurrogate>::value) return;
  var = {{rho, u, v, -1.0*t}, s};
  a = 0;
  pdebase.setDOFFrom(a, var);
  BOOST_CHECK_THROW( PDEClass pde2(a,
                                   adv, visc, source,
                                   isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                                   gas, Euler_ResidInterp_Raw) , AssertionException);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( source_EulermitAVSensor_test, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDENonAVClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDENonAVClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> PDEBaseClass;
  typedef PDESourceOnly2D<PDEBaseClass> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real h = 0.1;
  DLA::MatrixSymS<2,Real> logH = {{log(h)}, {0, log(h)}};

  Real x = 0.0, y = 0.0, time = 0.0;

  const int order = 1;
  bool isSteady = true;
  bool hasSpaceTimeDiffusion = false;

  SensorAdvectiveFlux adv(1.0, 0.0);
  SensorViscousFlux visc(1.0, 0.0, 0.0, 1.0);
  SensorSource source(1.0);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEBaseClass pdebase(adv, visc, source,
                       isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                       gas, Euler_ResidInterp_Raw);

  Real rho = 1.137;
  Real u = 0.784;
  Real v = -0.231;
  Real t = 0.987;
  Real s = 1.0;

  // set
  AVVariable<DensityVelocityPressure2D, Real> var = {{rho, u, v, t}, s};
  ArrayQ a = 0;
  pdebase.setDOFFrom(a, var);

  PDEClass pde(a,
               adv, visc, source,
               isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
               gas, Euler_ResidInterp_Raw);

  rho = 1.137; u = 0.784; v = -0.231; t = 0.987; s = 0.0032;

  // set
  ArrayQ q = 0;
  var = {{rho, u, v, t}, s};
  pdebase.setDOFFrom(q, var);


  Real rhox = 0.01; Real rhoy = -0.025;
  Real ux = 0.049; Real uy = 0.072;
  Real vx = 0.14; Real vy = -0.024;
  Real px = 0.002; Real py = -4.23;
  Real sx = 0.151; Real sy = -0.001;

  ArrayQ qx = {rhox, ux, vx, px, sx};
  ArrayQ qy = {rhoy, uy, vy, py, sy};

  Real lambda = 0.0;
  pdebase.speedCharacteristic(logH, x, y, time, a, lambda);
  Real tau = h/lambda;
  MatrixQ dudq = 0;
  pdebase.jacobianMasterState(logH, x, y, time, a, dudq);
  ArrayQ sourceTrue = 1.0 / tau * dudq * (q - a);

  ArrayQ src = 0.0;
  pde.source(logH, x, y, time, q, qx, qy, src);
  for (int i = 0; i < PDEClass::N; i++)
  {
    BOOST_CHECK_CLOSE( sourceTrue(i), src(i), tol );
  }
  BOOST_CHECK(abs(src(PDEClass::N-1)) > tol);

  ArrayQ qp = 0, qpx = 0, qpy = 0;
  src = 0.0;
  pde.source(logH, x, y, time, q, qp, qx, qy, qpx, qpy, src);
  for (int i = 0; i < PDEClass::N; i++)
  {
    BOOST_CHECK_CLOSE( sourceTrue(i), src(i), tol );
  }
  BOOST_CHECK(abs(src(PDEClass::N-1)) > tol);

  MatrixQ dsdqTrue = 1.0 / tau * dudq;
  MatrixQ dsdq = 0.0;
  pde.jacobianSource(logH, x, y, time, q, qx, qy, dsdq);
  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      BOOST_CHECK_CLOSE( dsdqTrue(i,j), dsdq(i,j), tol );
    }
  }
  BOOST_CHECK(abs(dsdq(PDEClass::N-1, PDEClass::N-1)) > tol);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PDESourceOnly2D_pattern.txt", true );

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None> PDEBaseClass;
  typedef PDESourceOnly2D<PDEBaseClass> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  ArrayQ a = 0.0;
  PDEClass pde( a, adv, visc, source );
  pde.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
