// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// OutputCell_Solution_btest
//

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/list.hpp>

#include "Topology/Dimension.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "pde/OutputCell_Solution.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{

}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( OutputCell_Solution_test_suite )

struct DummyPDE
{
  typedef PhysD2 PhysDim;

  template<class T>
  using ArrayQ = DLA::VectorS<2,T>;

  template<class T>
  using MatrixQ = DLA::MatrixS<2,2,T>;
};


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( OutputCell_Solution_test )
{
  typedef OutputCell_Solution<DummyPDE> OutputClass;

  typedef OutputClass::template ArrayQ<Real> ArrayQ;
  typedef OutputClass::template ArrayJ<Real> ArrayJ;

  const Real tol = 1e-13;

  // function tests
  ArrayQ q = {0.65, 0.83};
  ArrayQ qx = {-0.37, 0.92};
  ArrayQ qy = { 0.84, 0.45};
  ArrayQ qz = { 1.03, 0.61};
  ArrayJ J;

  Real x = 0.2, y = 0.4, z = 0.6;
  Real time = 1.0;

  OutputClass output;

  output(x, time, q, qx, J);
  BOOST_CHECK_CLOSE( J[0], q[0], tol );
  BOOST_CHECK_CLOSE( J[1], q[1], tol );

  output(x, y, time, q, qx, qy, J);
  BOOST_CHECK_CLOSE( J[0], q[0], tol );
  BOOST_CHECK_CLOSE( J[1], q[1], tol );

  output(x, y, z, time, q, qx, qy, qz, J);
  BOOST_CHECK_CLOSE( J[0], q[0], tol );
  BOOST_CHECK_CLOSE( J[1], q[1], tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
