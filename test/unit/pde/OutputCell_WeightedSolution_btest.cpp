// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// OutputCell_WeightedSolution_btest
//

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Topology/Dimension.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AnalyticFunction/ScalarFunction3D.h"

#include "pde/OutputCell_WeightedSolution.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( OutputCell_WeightedSolution_test_suite )

template<class PhysDim_>
struct DummyPDE
{
  typedef PhysDim_ PhysDim;

  template<class T>
  using ArrayQ = DLA::VectorS<2,T>;

  template<class T>
  using MatrixQ = DLA::MatrixS<2,2,T>;
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( OutputCell_WeightedSolution1D_test )
{
  typedef ScalarFunction1D_Linear WeightFcn;
  typedef OutputCell_WeightedSolution<DummyPDE<PhysD1>, WeightFcn> OutputClass;

  typedef OutputClass::template ArrayQ<Real> ArrayQ;
  typedef OutputClass::template ArrayJ<Real> ArrayJ;

  const Real tol = 1e-13;

  // function tests
  ArrayQ q = {0.65, 0.83};
  ArrayQ qx = {-0.37, 0.92};
  ArrayJ J;

  Real x = 0.2;
  Real time = 1.0;

  Real a0 = 1.4, a1 = 0.25;
  WeightFcn weightFcn(a0,a1);

  OutputClass output(weightFcn);
  Real w = a0 + a1*x;

  output(x, time, q, qx, J);
  BOOST_CHECK_CLOSE( J[0], w*q[0], tol );
  BOOST_CHECK_CLOSE( J[1], w*q[1], tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( OutputCell_WeightedSolution2D_test )
{
  typedef ScalarFunction2D_Linear WeightFcn;
  typedef OutputCell_WeightedSolution<DummyPDE<PhysD2>, WeightFcn> OutputClass;

  typedef OutputClass::template ArrayQ<Real> ArrayQ;
  typedef OutputClass::template ArrayJ<Real> ArrayJ;

  const Real tol = 1e-13;

  // function tests
  ArrayQ q = {0.65, 0.83};
  ArrayQ qx = {-0.37, 0.92};
  ArrayQ qy = { 0.84, 0.45};
  ArrayJ J;

  Real x = 0.2, y = 0.4;
  Real time = 1.0;

  Real a0 = 1.4, a1 = 0.25, a2 = 0.8;
  WeightFcn weightFcn(a0,a1,a2);

  OutputClass output(weightFcn);
  Real w = a0 + a1*x + a2*y;

  output(x, y, time, q, qx, qy, J);
  BOOST_CHECK_CLOSE( J[0], w*q[0], tol );
  BOOST_CHECK_CLOSE( J[1], w*q[1], tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( OutputCell_WeightedSolution3D_test )
{
  typedef ScalarFunction3D_Linear WeightFcn;
  typedef OutputCell_WeightedSolution<DummyPDE<PhysD3>, WeightFcn> OutputClass;

  typedef OutputClass::template ArrayQ<Real> ArrayQ;
  typedef OutputClass::template ArrayJ<Real> ArrayJ;

  const Real tol = 1e-13;

  // function tests
  ArrayQ q = {0.65, 0.83};
  ArrayQ qx = {-0.37, 0.92};
  ArrayQ qy = { 0.84, 0.45};
  ArrayQ qz = { 1.03, 0.61};
  ArrayJ J;

  Real x = 0.2, y = 0.4, z = 0.6;
  Real time = 1.0;

  Real a0 = 1.4, a1 = 0.25, a2 = 0.8, a3 = -0.63;
  WeightFcn weightFcn(a0, a1, a2, a3);

  OutputClass output(weightFcn);
  Real w = a0 + a1*x + a2*y + a3*z;

  output(x, y, z, time, q, qx, qy, qz, J);
  BOOST_CHECK_CLOSE( J[0], w*q[0], tol );
  BOOST_CHECK_CLOSE( J[1], w*q[1], tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
