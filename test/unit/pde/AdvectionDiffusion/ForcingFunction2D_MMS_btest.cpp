// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ForcingFunction2D_MMS_btest
//
// test of 2-D MMS forcing functions for advection diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/ForcingFunction2D_MMS.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ForcingFunction2D_MMS_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SineSine )
{
  const Real tol = 1.e-13;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;

  ScalarFunction2D_SineSine soln;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123, kxy = 0.553;
  Real kyx = 0.378, kyy = 1.007;

  ViscousFlux2D_Uniform visc( kxx, kxy,
                                  kyx, kyy );

  Real a = 0.5, b = 2.1, c = 0.12;
  Source2D_UniformGrad source(a, b, c);

  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr< ForcingType > forcingptr( new ForcingType(soln) );

  PDEClass pde(adv, visc, source, forcingptr);

  // static tests
  BOOST_CHECK( (*forcingptr).D == 2 );

  // functor
  Real x = sqrt(3);
  Real y = sqrt(7);
  Real snx = sin(2*PI*x), csx = cos(2*PI*x);
  Real sny = sin(2*PI*y), csy = cos(2*PI*y);

  Real frcAdvTrue = 2*PI*( u*csx*sny + v*snx*csy);

  Real frcViscTrue = 2*PI*( 2*PI*( kxx*snx*sny - kxy*csx*csy) +
                            2*PI*(-kyx*csx*csy + kyy*snx*sny));

  Real frcSrcTrue = a*snx*sny + 2*PI*(b*csx*sny + c*snx*csy);

  Real frcTrue = frcAdvTrue + frcViscTrue + frcSrcTrue;

  Real time = 0;
  ArrayQ frc = 0;
  pde.forcingFunction(x, y, time, frc);
  BOOST_CHECK_CLOSE( frcTrue, frc, tol );

  pde.forcingFunction(x, y, time, frc);
  BOOST_CHECK_CLOSE( 2*frcTrue, frc, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( monomial )
{
  const Real tol = 1.e-13;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;

  int i = 2, j = 3;
  ScalarFunction2D_Monomial soln(i,j);

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123, kxy = 0.553;
  Real kyx = 0.378, kyy = 1.007;

  ViscousFlux2D_Uniform visc( kxx, kxy,
                                  kyx, kyy );

  Real a = 0.5, b = 2.1, c = 0.12;
  Source2D_UniformGrad source(a, b, c);

  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr< ForcingType > forcingptr( new ForcingType(soln) );

  PDEClass pde(adv, visc, source, forcingptr);

  // static tests
  BOOST_CHECK( (*forcingptr).D == 2 );

  // functor
  Real x = sqrt(3);
  Real y = sqrt(7);

  Real frcAdvTrue = u*pow(x, i-1) * pow(y, j  ) * i
                  + v*pow(x, i  ) * pow(y, j-1) * j;

  Real frcViscTrue = - kxx * pow(x, i-2) * pow(y, j  ) * i*(i-1)
                     - kxy * pow(x, i-1) * pow(y, j-1) * i*j

                     - kyx * pow(x, i-1) * pow(y, j-1) * i*j
                     - kyy * pow(x, i  ) * pow(y, j-2) * j*(j-1);

  Real frcSrcTrue = a*pow(x, i  ) * pow(y, j  )
                  + b*pow(x, i-1) * pow(y, j  ) * i
                  + c*pow(x, i  ) * pow(y, j-1) * j;

  Real frcTrue = frcAdvTrue + frcViscTrue + frcSrcTrue;

  Real time = 0;
  ArrayQ frc = 0;
  pde.forcingFunction(x, y, time, frc);
  BOOST_CHECK_CLOSE( frcTrue, frc, tol );

  pde.forcingFunction(x, y, time, frc);
  BOOST_CHECK_CLOSE( 2*frcTrue, frc, tol );
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PDEAdvectionDiffusion2D_ForcingFunction2D_MMS_pattern.txt", true );

  ForcingFunction2D_Const forcing2( 1 );
  forcing2.dump( 2, output );

  ForcingFunction2D_Monomial forcing3( 2, 5, 4, 1 );
  forcing3.dump( 2, output );

  ForcingFunction2D_SineSineSine forcing4( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 );
  forcing4.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
