// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEAdvectionDiffusion3D_btest
//
// test of 3-D Advection-Diffusion PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class PDEAdvectionDiffusion<PhysD3,
                                     AdvectiveFlux3D_Uniform,
                                     ViscousFlux3D_Uniform,
                                     Source3D_None>;
}


using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEAdvectionDiffusion3D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEClass;

  BOOST_CHECK( PDEClass::D == 3 ); // 3D AdVec
  BOOST_CHECK( PDEClass::N == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEClass;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123, kxy = 0.553, kxz = 0.643;
  Real kyx = 0.378, kyy = 1.007, kyz = 0.765;
  Real kzx = 1.642, kzy = 1.299, kzz = 1.234;

  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                              kyx, kyy, kyz,
                              kzx, kzy, kzz );

  Source3D_None source;

  PDEClass pde1(adv, visc, source);

  BOOST_CHECK( pde1.D == 3 );
  BOOST_CHECK( pde1.N == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123, kxy = 0.553, kxz = 0.643;
  Real kyx = 0.378, kyy = 1.007, kyz = 0.765;
  Real kzx = 1.642, kzy = 1.299, kzz = 1.234;

  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                                  kyx, kyy, kyz,
                                  kzx, kzy, kzz );

//  ForcingFunction3D_Const forcing( 1 );


  typedef ForcingFunction3D_Const<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(1));

  Source3D_None source;

  PDEClass pde(adv, visc, source, forcingptr);

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == true );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // function tests

  Real x, y, z, time;
  Real sln, slnx, slny, slnz;

  x = 0;
  y = 0;
  z = 0;
  time = 0;
  sln  =  3.263;
  slnx = -0.445;
  slny =  1.741;
  slnz =  0.719; // New value

  DLA::VectorS<3,Real> X = {x,y,z};

  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = qDataPrim[0];
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  // master state
  ArrayQ uCons = 0;
  pde.masterState( x, y, z, time, q, uCons );
  BOOST_CHECK_CLOSE( qTrue, uCons, tol );

  // unsteady flux
  ArrayQ ft = 0;
  pde.fluxAdvectiveTime( x, y, z, time, q, ft );
  BOOST_CHECK_CLOSE( qTrue, ft, tol );

  pde.fluxAdvectiveTime( x, y, z, time, q, ft );
  BOOST_CHECK_CLOSE( 2*qTrue, ft, tol );

  MatrixQ dudq = 0;
  pde.jacobianMasterState( x, y, z, time, q, dudq );
  BOOST_CHECK_CLOSE( 1, dudq, tol );

  // advective flux
  Real fData[1] = {u*sln};
  Real gData[1] = {v*sln};
  Real hData[1] = {w*sln};
  ArrayQ fTrue = fData[0];
  ArrayQ gTrue = gData[0];
  ArrayQ hTrue = hData[0];
  ArrayQ f, g, h;
  DLA::VectorS<3,ArrayQ> F;
  f = 0;
  g = 0;
  h = 0;
  pde.fluxAdvective( x, y, z, time, q, f, g, h );
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( gTrue, g, tol );
  BOOST_CHECK_CLOSE( hTrue, h, tol );

  pde.fluxAdvective( x, y, z, time, q, f, g, h );
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );
  BOOST_CHECK_CLOSE( 2*gTrue, g, tol );
  BOOST_CHECK_CLOSE( 2*hTrue, h, tol );


  // advective flux jacobian
  MatrixQ dfdu = 0;
  MatrixQ dgdu = 0;
  MatrixQ dhdu = 0;
  pde.jacobianFluxAdvective( x, y, z, time, q, dfdu, dgdu, dhdu );
  BOOST_CHECK_CLOSE( u, dfdu, tol );
  BOOST_CHECK_CLOSE( v, dgdu, tol );
  BOOST_CHECK_CLOSE( w, dhdu, tol );

  // set gradient
  // x-direction
  qDataPrim[0] = slnx;
  ArrayQ qxTrue = qDataPrim[0];
  ArrayQ qx;
  pde.setDOFFrom( qx, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxTrue, qx, tol );

  // y-direction
  qDataPrim[0] = slny;
  ArrayQ qyTrue = qDataPrim[0];
  ArrayQ qy;
  pde.setDOFFrom( qy, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyTrue, qy, tol );

  // z-direction
  qDataPrim[0] = slnz;
  ArrayQ qzTrue = qDataPrim[0];
  ArrayQ qz;
  pde.setDOFFrom( qz, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qzTrue, qz, tol );

  DLA::VectorS<3, ArrayQ> gradq = {qx, qy, qz};

  // diffusive flux
  fTrue = -(kxx*slnx + kxy*slny + kxz*slnz);
  gTrue = -(kyx*slnx + kyy*slny + kyz*slnz);
  hTrue = -(kzx*slnx + kzy*slny + kzz*slnz);
  f = 0;
  g = 0;
  h = 0;
  pde.fluxViscous( x, y, z, time, q, qx, qy, qz, f, g, h);
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( gTrue, g, tol );
  BOOST_CHECK_CLOSE( hTrue, h, tol );

  pde.fluxViscous( x, y, z, time, q, qx, qy, qz, f, g, h);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );
  BOOST_CHECK_CLOSE( 2*gTrue, g, tol );
  BOOST_CHECK_CLOSE( 2*hTrue, h, tol );

  // diffusive flux jacobian
  MatrixQ kxxMtx = 0, kxyMtx = 0, kxzMtx = 0;
  MatrixQ kyxMtx = 0, kyyMtx = 0, kyzMtx = 0;
  MatrixQ kzxMtx = 0, kzyMtx = 0, kzzMtx = 0;
  pde.diffusionViscous( x, y, z, time,
                        q, qx, qy, qz,
                        kxxMtx, kxyMtx, kxzMtx,
                        kyxMtx, kyyMtx, kyzMtx,
                        kzxMtx, kzyMtx, kzzMtx );

  BOOST_CHECK_CLOSE( kxx, kxxMtx, tol );
  BOOST_CHECK_CLOSE( kxy, kxyMtx, tol );
  BOOST_CHECK_CLOSE( kxz, kxzMtx, tol );
  BOOST_CHECK_CLOSE( kyx, kyxMtx, tol );
  BOOST_CHECK_CLOSE( kyy, kyyMtx, tol );
  BOOST_CHECK_CLOSE( kyz, kyzMtx, tol );
  BOOST_CHECK_CLOSE( kzx, kzxMtx, tol );
  BOOST_CHECK_CLOSE( kzy, kzyMtx, tol );
  BOOST_CHECK_CLOSE( kzz, kzzMtx, tol );

  pde.diffusionViscous( x, y, z, time,
                        q, qx, qy, qz,
                        kxxMtx, kxyMtx, kxzMtx,
                        kyxMtx, kyyMtx, kyzMtx,
                        kzxMtx, kzyMtx, kzzMtx );

  BOOST_CHECK_CLOSE( 2*kxx, kxxMtx, tol );
  BOOST_CHECK_CLOSE( 2*kxy, kxyMtx, tol );
  BOOST_CHECK_CLOSE( 2*kxz, kxzMtx, tol );
  BOOST_CHECK_CLOSE( 2*kyx, kyxMtx, tol );
  BOOST_CHECK_CLOSE( 2*kyy, kyyMtx, tol );
  BOOST_CHECK_CLOSE( 2*kyz, kyzMtx, tol );
  BOOST_CHECK_CLOSE( 2*kzx, kzxMtx, tol );
  BOOST_CHECK_CLOSE( 2*kzy, kzyMtx, tol );
  BOOST_CHECK_CLOSE( 2*kzz, kzzMtx, tol );

  // forcing function
  ArrayQ src = 0;
  pde.forcingFunction( x, y, z, time, src );
  BOOST_CHECK_CLOSE( 1, src, tol ); // Check the dimension of the source array

  pde.forcingFunction( x, y, z, time, src );
  BOOST_CHECK_CLOSE( 2, src, tol ); // Check the dimension of the source array

  // for coverage; functions are empty
  src = 0;
  pde.source( x, y, z, time, q, qx, qy, qz, src );

  ArrayQ qp = 0, qpx = 0, qpy = 0, qpz = 0;
  ArrayQ sourceterm_split = 0;
  pde.source( x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, sourceterm_split );
  BOOST_CHECK_CLOSE( sourceterm_split, src, tol );

  pde.sourceTrace( x, y, z, x, y, z, time, q, qx, qy, qz, q, qx, qy, qz, src, src );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxViscous )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123, kxy = 0.553, kxz = 0.643;
  Real kyx = 0.378, kyy = 1.007, kyz = 0.765;
  Real kzx = 1.642, kzy = 1.299, kzz = 1.234;

  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                                  kyx, kyy, kyz,
                                  kzx, kzy, kzz );

  Source3D_None source;

  PDEClass pde(adv, visc, source);

  // advective flux function

  Real x, y, z, time;
  Real nx, ny, nz;
  Real slnL, slnR;

  x = 0; y = 0; z = 0; time = 0; // not actually used in functions
  nx = 1.22; ny = -0.432; nz = 0.275;
  slnL = 3.26; slnR = 1.79;

  DLA::VectorS<3, Real> X = {x, y, z};
  DLA::VectorS<3, Real> N = {nx, ny, nz};

  // set
  Real qLDataPrim[1] = {slnL};
  string qLNamePrim[1] = {"Solution"};
  ArrayQ qLTrue = qLDataPrim[0];
  ArrayQ qL;
  pde.setDOFFrom( qL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qLTrue, qL, tol );

  Real qRDataPrim[1] = {slnR};
  string qRNamePrim[1] = {"Solution"};
  ArrayQ qRTrue = qRDataPrim[0];
  ArrayQ qR;
  pde.setDOFFrom( qR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qRTrue, qR, tol );

  // advective normal flux
  Real fnData[1] = {(nx*u + ny*v + nz*w)*slnL}; // V . n
  ArrayQ fnTrue = fnData[0];
  ArrayQ fn;
  fn = 0;
  pde.fluxAdvectiveUpwind( x, y, z, time, qL, qR, nx, ny, nz, fn );
  BOOST_CHECK_CLOSE( fnTrue, fn, tol );

  pde.fluxAdvectiveUpwind( x, y, z, time, qL, qR, nx, ny, nz, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue, fn, tol );

  // viscous flux function

  Real slnxL, slnyL, slnzL;
  Real slnxR, slnyR, slnzR;

  slnxL = 1.325;  slnyL = -0.457;  slnzL = 2.412;
  slnxR = 0.327;  slnyR =  3.421;  slnzR = -0.832;

  // set gradient
  qLDataPrim[0] = slnxL;
  ArrayQ qxLTrue =qLDataPrim[0];
  ArrayQ qxL;
  pde.setDOFFrom( qxL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxLTrue, qxL, tol );

  qLDataPrim[0] = slnyL;
  ArrayQ qyLTrue = qLDataPrim[0];
  ArrayQ qyL;
  pde.setDOFFrom( qyL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyLTrue, qyL, tol );

  qLDataPrim[0] = slnzL;
  ArrayQ qzLTrue = qLDataPrim[0];
  ArrayQ qzL;
  pde.setDOFFrom( qzL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qzLTrue, qzL, tol );

  DLA::VectorS<3, ArrayQ> gradqL = {qxL, qyL, qzL};

  qRDataPrim[0] = slnxR;
  ArrayQ qxRTrue = qRDataPrim[0];
  ArrayQ qxR;
  pde.setDOFFrom( qxR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxRTrue, qxR, tol );

  qRDataPrim[0] = slnyR;
  ArrayQ qyRTrue = qRDataPrim[0];
  ArrayQ qyR;
  pde.setDOFFrom( qyR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyRTrue, qyR, tol );

  qRDataPrim[0] = slnzR;
  ArrayQ qzRTrue = qRDataPrim[0];
  ArrayQ qzR;
  pde.setDOFFrom( qzR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qzRTrue, qzR, tol );

  DLA::VectorS<3, ArrayQ> gradqR = {qxR, qyR, qzR};

  // viscous normal flux
  fnData[0] = -0.5*( (nx*kxx + ny*kyx + nz*kzx)*(slnxL + slnxR) +
                     (nx*kxy + ny*kyy + nz*kzy)*(slnyL + slnyR) +
                     (nx*kxz + ny*kyz + nz*kzz)*(slnzL + slnzR) );
  fn = 0;
  pde.fluxViscous( x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, nx, ny, nz, fn );
  BOOST_CHECK_CLOSE( fnData[0], fn, tol );

  pde.fluxViscous( x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, nx, ny, nz, fn );
  BOOST_CHECK_CLOSE( 2*fnData[0], fn, tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123, kxy = 0.553, kxz = 0.643;
  Real kyx = 0.378, kyy = 1.007, kyz = 0.765;
  Real kzx = 1.642, kzy = 1.299, kzz = 1.234;

  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                                  kyx, kyy, kyz,
                                  kzx, kzy, kzz );

  Source3D_None source;

  PDEClass pde(adv, visc, source );

  Real time = 0;

  Real x, y, z;
  Real dx, dy, dz;
  Real sln;
  Real speed, speedTrue;

  x = 0; y = 0; z = 0;   // not actually used in functions
  dx = 1.22; dy = -0.432; dz = 0.275;  // Same numbers as for nx, ny, nz
  sln  =  3.263;


  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = qDataPrim[0];
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  speedTrue = fabs(dx*u + dy*v + dz*w)/sqrt(dx*dx + dy*dy + dz*dz);

  pde.speedCharacteristic( x, y, z, time, dx, dy, dz, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  pde.speedCharacteristic( x, y, z, time, q, speed );
  BOOST_CHECK_CLOSE( sqrt(u*u + v*v + w*w), speed, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123, kxy = 0.553, kxz = 0.643;
  Real kyx = 0.378, kyy = 1.007, kyz = 0.765;
  Real kzx = 1.642, kzy = 1.299, kzz = 1.234;

  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                                  kyx, kyy, kyz,
                                  kzx, kzy, kzz );

  Source3D_None source;

  PDEClass pde(adv, visc, source);

  ArrayQ q = 0;
  BOOST_CHECK( pde.isValidState(q) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PDEAdvectionDiffusion3D_pattern.txt", true );

  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEClass;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123, kxy = 0.553, kxz = 0.643;
  Real kyx = 0.378, kyy = 1.007, kyz = 0.765;
  Real kzx = 1.642, kzy = 1.299, kzz = 1.234;

  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                                  kyx, kyy, kyz,
                                  kzx, kzy, kzz );

//  ForcingFunction3D_Const forcing( 1 );
  typedef ForcingFunction3D_Const<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(1));


  Source3D_None source;

  PDEClass pde1(adv, visc, source, forcingptr);
  pde1.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
