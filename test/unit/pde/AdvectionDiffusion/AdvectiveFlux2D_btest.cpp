// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AdvectieFlux2D_btest
//
// test of 2-D advection flux class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AdvectionDiffusion/AdvectiveFlux2D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( AdvectiveFlux2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( None )
{
  typedef AdvectiveFlux2D_None::ArrayQ<Real> ArrayQ;
  typedef AdvectiveFlux2D_None::MatrixQ<Real> MatrixQ;

  Real nx = 1.0;
  Real ny = 0.25;
  Real nt = 0.5;
  Real x = 1;
  Real y = 2;
  Real time = 0;

  AdvectiveFlux2D_None adv;

  ArrayQ q = 1.45;
  ArrayQ qL = 2.45;
  ArrayQ qR = 3.45;
  ArrayQ qx = 1.56;
  ArrayQ qy = 2.56;
  ArrayQ fxTrue = 1.234;
  ArrayQ fx = fxTrue;
  ArrayQ fyTrue = 2.234;
  ArrayQ fy = fyTrue;
  MatrixQ axTrue = 3.45;
  MatrixQ ax = axTrue;
  MatrixQ ayTrue = 5.45;
  MatrixQ ay = ayTrue;

  BOOST_CHECK( adv.hasFluxAdvective() == false );

  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_EQUAL( fxTrue, fx );
  BOOST_CHECK_EQUAL( fyTrue, fy );

  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, fx);
  BOOST_CHECK_EQUAL( fxTrue, fx );

  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, fx);
  BOOST_CHECK_EQUAL( fxTrue + qL*nt, fx );

  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, fx);
  BOOST_CHECK_EQUAL( fxTrue + 2*qL*nt, fx );

  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_EQUAL( axTrue, ax );
  BOOST_CHECK_EQUAL( ayTrue, ay );

  ArrayQ strongPDETrue = 5.78;
  ArrayQ strongPDE = strongPDETrue;
  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_EQUAL( strongPDETrue, strongPDE );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform_hasFluxAdvective )
{
  Real u =  1.33479;
  Real v = -0.33886;

  AdvectiveFlux2D_Uniform adv(u, v);

  BOOST_CHECK( adv.hasFluxAdvective() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform_flux )
{
  typedef AdvectiveFlux2D_Uniform::ArrayQ<Real> ArrayQ;
  const Real tol = 1.e-13;

  ArrayQ q = 2.34;
  Real u =  1.33479;
  Real v = -0.33886;
  ArrayQ fxTrue = u*q;
  ArrayQ fyTrue = v*q;
  ArrayQ fx = 0, fy = 0;

  AdvectiveFlux2D_Uniform adv(u, v);

  // functor
  Real x = 1;
  Real y = 2;
  Real time = 0;
  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( fyTrue, fy, tol );

  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( 2*fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( 2*fyTrue, fy, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform_jacobian )
{
  typedef AdvectiveFlux2D_Uniform::ArrayQ<Real> ArrayQ;
  const Real tol = 1.e-13;

  Real x = 1;
  Real y = 2;
  Real time = 0;

  ArrayQ q = 2.34;
  Real u =  1.33479;
  Real v = -0.33886;
  ArrayQ jacxTrue = u;
  ArrayQ jacyTrue = v;
  ArrayQ jacx = 0, jacy = 0;

  AdvectiveFlux2D_Uniform adv(u, v);

  // jacobian
  adv.jacobian(x, y, time, q, jacx, jacy);
  BOOST_CHECK_CLOSE( jacxTrue, jacx, tol );
  BOOST_CHECK_CLOSE( jacyTrue, jacy, tol );

  adv.jacobian(x, y, time, q, jacx, jacy);
  BOOST_CHECK_CLOSE( 2*jacxTrue, jacx, tol );
  BOOST_CHECK_CLOSE( 2*jacyTrue, jacy, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform_fluxUpwind )
{
  typedef AdvectiveFlux2D_Uniform::ArrayQ<Real> ArrayQ;
  const Real tol = 1.e-13;

  Real x = 1;
  Real y = 2;
  Real time = 0;

  Real u =  1.33479;
  Real v = -0.33886;

  AdvectiveFlux2D_Uniform adv(u, v);

  Real nx = 1.1;
  Real ny = 0.25;
  ArrayQ qL = u;
  ArrayQ qR = 0;
  ArrayQ f = 0;
  Real fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);

  // flux
  f = 0;
  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  qL = 0;
  qR = u;
  fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);
  // flux
  f = 0;
  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  nx = -1.1;
  ny = 0.25;
  qL = u;
  qR = 0;
  fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);
  // flux
  f = 0;
  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  nx = -1.1;
  ny = 0.25;
  qL = 0;
  qR = u;
  fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);
  // flux
  f = 0;
  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform_fluxUpwindSpaceTime )
{
  typedef AdvectiveFlux2D_Uniform::ArrayQ<Real> ArrayQ;
  const Real tol = 1.e-13;

  Real x = 1;
  Real y = 2;
  Real time = 0;

  Real u =  1.33479;
  Real v = -0.33886;

  AdvectiveFlux2D_Uniform adv(u, v);

  Real nx = 1.1;
  Real ny = 0.25;
  Real nt = 0.65;
  ArrayQ qL = u;
  ArrayQ qR = 0;
  ArrayQ f = 0;
  Real fTrue = 0.5*(nt*1 + nx*u + ny*v)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v)*(qR - qL);

  // flux
  f = 0;
  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  nx = 1.0;
  qL = 0;
  qR = u;
  fTrue = 0.5*(nt*1 + nx*u + ny*v)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v)*(qR - qL);
  // flux
  f = 0;
  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  nx = -1.1;
  qL = u;
  qR = 0;
  fTrue = 0.5*(nt*1 + nx*u + ny*v)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v)*(qR - qL);
  // flux
  f = 0;
  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  nx = -1.1;
  qL = 0;
  qR = u;
  fTrue = 0.5*(nt*1 + nx*u + ny*v)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v)*(qR - qL);
  // flux
  f = 0;
  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  nx = -1.1;
  ny = -0.5;
  nt = -0.1;
  qL = 0;
  qR = u;
  fTrue = 0.5*(nt*1 + nx*u + ny*v)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v)*(qR - qL);
  // flux
  f = 0;
  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform_strongFlux )
{
  typedef AdvectiveFlux2D_Uniform::ArrayQ<Real> ArrayQ;
  const Real tol = 1.e-13;

  Real x = 1;
  Real y = 2;
  Real time = 0;

  Real u =  1.33479;
  Real v = -0.33886;

  ArrayQ q = u;
  ArrayQ qx = 1.56;
  ArrayQ qy = 2.56;
  Real strongPDETrue = u*qx + v*qy;

  AdvectiveFlux2D_Uniform adv(u, v);

  // strong flux
  ArrayQ strongPDE = 0;
  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( strongPDETrue, strongPDE, tol );

  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( 2*strongPDETrue, strongPDE, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( cubicsourcebump_flux )
{
  typedef AdvectiveFlux2D_CubicSourceBump::ArrayQ<Real> ArrayQ;
  const Real tol = 1.e-10;

  Real tau = 0.1;
  AdvectiveFlux2D_CubicSourceBump adv(tau);

  // functor

  ArrayQ q = 2.34;
  ArrayQ fx, fy;
  ArrayQ fxTrue, fyTrue;
  Real x, y;
  Real time = 0;

  x = -1;
  y =  0;
  fxTrue = 9.891151724088069E-01*q;
  fyTrue = 0*q;
  fx = fy = 0;
  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( fyTrue, fy, tol );

  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( 2*fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( 2*fyTrue, fy, tol );

  x = 0;
  y = 0;
  fxTrue = 7.669276655348198E-01*q;
  fyTrue = 0*q;
  fx = fy = 0;
  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( fyTrue, fy, tol );

  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( 2*fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( 2*fyTrue, fy, tol );

  x = 0.5;
  y = 0;
  fxTrue = 1.466144668930360E+00*q;
  fyTrue = 0*q;
  fx = fy = 0;
  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( fyTrue, fy, tol );

  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( 2*fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( 2*fyTrue, fy, tol );

  x = 1;
  y = 0;
  fxTrue = 7.669276655348198E-01*q;
  fyTrue = 0*q;
  fx = fy = 0;
  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( fyTrue, fy, tol );

  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( 2*fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( 2*fyTrue, fy, tol );

  x = 2;
  y = 0;
  fxTrue = 9.891151724088069E-01*q;
  fyTrue = 0*q;
  fx = fy = 0;
  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( fyTrue, fy, tol );

  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( 2*fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( 2*fyTrue, fy, tol );

  x = 0.5;
  y = 0.1;
  fxTrue = 1.294369873971500E+00*q;
  fyTrue = 0*q;
  fx = fy = 0;
  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( fyTrue, fy, tol );

  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( 2*fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( 2*fyTrue, fy, tol );

  x = -1;
  y =  0.5;
  fxTrue = 9.924389181161534E-01*q;
  fyTrue = 5.998829903835843E-03*q;
  fx = fy = 0;
  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( fyTrue, fy, tol );

  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( 2*fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( 2*fyTrue, fy, tol );

  x = 0;
  y = 0.5;
  fxTrue = 1.009203507602450E+00*q;
  fyTrue = 4.410839493355371E-02*q;
  fx = fy = 0;
  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( fyTrue, fy, tol );

  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( 2*fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( 2*fyTrue, fy, tol );

  x = 0.5;
  y = 0.5;
  fxTrue = 1.067034171759651E+00*q;
  fyTrue = 0*q;
  fx = fy = 0;
  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( fyTrue, fy, tol );

  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( 2*fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( 2*fyTrue, fy, tol );

  x = 1;
  y = 0.5;
  fxTrue =  1.009203507602450E+00*q;
  fyTrue = -4.410839493355371E-02*q;
  fx = fy = 0;
  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( fyTrue, fy, tol );

  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( 2*fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( 2*fyTrue, fy, tol );

  x = 2;
  y = 0.5;
  fxTrue =  9.924389181161534E-01*q;
  fyTrue = -5.998829903835843E-03*q;
  fx = fy = 0;
  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( fyTrue, fy, tol );

  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( 2*fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( 2*fyTrue, fy, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( cubicsourcebump_Jacobian )
{
  typedef AdvectiveFlux2D_CubicSourceBump::ArrayQ<Real> ArrayQ;
  const Real tol = 1.e-10;

  Real tau = 0.1;
  AdvectiveFlux2D_CubicSourceBump adv(tau);

  // functor

  ArrayQ q = 2.34;
  ArrayQ ax, ay;
  ArrayQ axTrue, ayTrue;
  Real x, y;
  Real time = 0;

  x = -1;
  y =  0;
  axTrue = 9.891151724088069E-01;
  ayTrue = 0;
  ax = ay = 0;
  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_CLOSE( axTrue, ax, tol );
  BOOST_CHECK_CLOSE( ayTrue, ay, tol );

  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_CLOSE( 2*axTrue, ax, tol );
  BOOST_CHECK_CLOSE( 2*ayTrue, ay, tol );

  x = 0;
  y = 0;
  axTrue = 7.669276655348198E-01;
  ayTrue = 0;
  ax = ay = 0;
  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_CLOSE( axTrue, ax, tol );
  BOOST_CHECK_CLOSE( ayTrue, ay, tol );

  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_CLOSE( 2*axTrue, ax, tol );
  BOOST_CHECK_CLOSE( 2*ayTrue, ay, tol );

  x = 0.5;
  y = 0;
  axTrue = 1.466144668930360E+00;
  ayTrue = 0;
  ax = ay = 0;
  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_CLOSE( axTrue, ax, tol );
  BOOST_CHECK_CLOSE( ayTrue, ay, tol );

  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_CLOSE( 2*axTrue, ax, tol );
  BOOST_CHECK_CLOSE( 2*ayTrue, ay, tol );

  x = 1;
  y = 0;
  axTrue = 7.669276655348198E-01;
  ayTrue = 0;
  ax = ay = 0;
  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_CLOSE( axTrue, ax, tol );
  BOOST_CHECK_CLOSE( ayTrue, ay, tol );

  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_CLOSE( 2*axTrue, ax, tol );
  BOOST_CHECK_CLOSE( 2*ayTrue, ay, tol );

  x = 2;
  y = 0;
  axTrue = 9.891151724088069E-01;
  ayTrue = 0;
  ax = ay = 0;
  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_CLOSE( axTrue, ax, tol );
  BOOST_CHECK_CLOSE( ayTrue, ay, tol );

  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_CLOSE( 2*axTrue, ax, tol );
  BOOST_CHECK_CLOSE( 2*ayTrue, ay, tol );

  x = 0.5;
  y = 0.1;
  axTrue = 1.294369873971500E+00;
  ayTrue = 0;
  ax = ay = 0;
  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_CLOSE( axTrue, ax, tol );
  BOOST_CHECK_CLOSE( ayTrue, ay, tol );

  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_CLOSE( 2*axTrue, ax, tol );
  BOOST_CHECK_CLOSE( 2*ayTrue, ay, tol );

  x = -1;
  y =  0.5;
  axTrue = 9.924389181161534E-01;
  ayTrue = 5.998829903835843E-03;
  ax = ay = 0;
  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_CLOSE( axTrue, ax, tol );
  BOOST_CHECK_CLOSE( ayTrue, ay, tol );

  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_CLOSE( 2*axTrue, ax, tol );
  BOOST_CHECK_CLOSE( 2*ayTrue, ay, tol );

  x = 0;
  y = 0.5;
  axTrue = 1.009203507602450E+00;
  ayTrue = 4.410839493355371E-02;
  ax = ay = 0;
  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_CLOSE( axTrue, ax, tol );
  BOOST_CHECK_CLOSE( ayTrue, ay, tol );

  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_CLOSE( 2*axTrue, ax, tol );
  BOOST_CHECK_CLOSE( 2*ayTrue, ay, tol );

  x = 0.5;
  y = 0.5;
  axTrue = 1.067034171759651E+00;
  ayTrue = 0;
  ax = ay = 0;
  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_CLOSE( axTrue, ax, tol );
  BOOST_CHECK_CLOSE( ayTrue, ay, tol );

  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_CLOSE( 2*axTrue, ax, tol );
  BOOST_CHECK_CLOSE( 2*ayTrue, ay, tol );

  x = 1;
  y = 0.5;
  axTrue =  1.009203507602450E+00;
  ayTrue = -4.410839493355371E-02;
  ax = ay = 0;
  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_CLOSE( axTrue, ax, tol );
  BOOST_CHECK_CLOSE( ayTrue, ay, tol );

  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_CLOSE( 2*axTrue, ax, tol );
  BOOST_CHECK_CLOSE( 2*ayTrue, ay, tol );

  x = 2;
  y = 0.5;
  axTrue =  9.924389181161534E-01;
  ayTrue = -5.998829903835843E-03;
  ax = ay = 0;
  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_CLOSE( axTrue, ax, tol );
  BOOST_CHECK_CLOSE( ayTrue, ay, tol );

  adv.jacobian(x, y, time, q, ax, ay);
  BOOST_CHECK_CLOSE( 2*axTrue, ax, tol );
  BOOST_CHECK_CLOSE( 2*ayTrue, ay, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( cubicsourcebump_fluxUpwind )
{
  typedef AdvectiveFlux2D_CubicSourceBump::ArrayQ<Real> ArrayQ;
  const Real tol = 1.e-10;

  Real tau = 0.1;
  AdvectiveFlux2D_CubicSourceBump adv(tau);

  // functor

  ArrayQ qL = 2.34;
  ArrayQ qR = -1.34;
  ArrayQ f;
  ArrayQ fTrue;
  Real u, v;
  Real nx = 1.1, ny = -0.25;
  Real x, y;
  Real time = 0;

  x = -1;
  y =  0;
  u = 9.891151724088069E-01;
  v = 0;
  fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);
  f = 0;
  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  x = 0;
  y = 0;
  u = 7.669276655348198E-01;
  v = 0;
  fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);
  f = 0;
  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  x = 0.5;
  y = 0;
  u = 1.466144668930360E+00;
  v = 0;
  fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);
  f = 0;
  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  x = 1;
  y = 0;
  u = 7.669276655348198E-01;
  v = 0;
  fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);
  f = 0;
  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  x = 2;
  y = 0;
  u = 9.891151724088069E-01;
  v = 0;
  fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);
  f = 0;
  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  x = 0.5;
  y = 0.1;
  u = 1.294369873971500E+00;
  v = 0;
  fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);
  f = 0;
  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  x = -1;
  y =  0.5;
  u = 9.924389181161534E-01;
  v = 5.998829903835843E-03;
  fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);
  f = 0;
  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  x = 0;
  y = 0.5;
  u = 1.009203507602450E+00;
  v = 4.410839493355371E-02;
  fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);
  f = 0;
  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  x = 0.5;
  y = 0.5;
  u = 1.067034171759651E+00;
  v = 0;
  fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);
  f = 0;
  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  x = 1;
  y = 0.5;
  u =  1.009203507602450E+00;
  v = -4.410839493355371E-02;
  fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);
  f = 0;
  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  x = 2;
  y = 0.5;
  u =  9.924389181161534E-01;
  v = -5.998829903835843E-03;
  fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);
  f = 0;
  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( cubicsourcebump_fluxUpwindSpaceTime )
{
  typedef AdvectiveFlux2D_CubicSourceBump::ArrayQ<Real> ArrayQ;
  const Real tol = 1.e-10;

  Real tau = 0.1;
  AdvectiveFlux2D_CubicSourceBump adv(tau);

  // functor

  ArrayQ qL = 2.34;
  ArrayQ qR = -1.34;
  ArrayQ f;
  ArrayQ fTrue;
  Real u, v;
  Real nx = 1.1, ny = -0.25, nt = 1.3;
  Real x, y;
  Real time = 0;

  x = -1;
  y =  0;
  u = 9.891151724088069E-01;
  v = 0;
  fTrue = 0.5*(nt*1 + nx*u + ny*v)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v)*(qR - qL);
  f = 0;
  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  x = 0;
  y = 0;
  u = 7.669276655348198E-01;
  v = 0;
  fTrue = 0.5*(nt*1 + nx*u + ny*v)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v)*(qR - qL);
  f = 0;
  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  x = 0.5;
  y = 0;
  u = 1.466144668930360E+00;
  v = 0;
  fTrue = 0.5*(nt*1 + nx*u + ny*v)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v)*(qR - qL);
  f = 0;
  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  x = 1;
  y = 0;
  u = 7.669276655348198E-01;
  v = 0;
  fTrue = 0.5*(nt*1 + nx*u + ny*v)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v)*(qR - qL);
  f = 0;
  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  x = 2;
  y = 0;
  u = 9.891151724088069E-01;
  v = 0;
  fTrue = 0.5*(nt*1 + nx*u + ny*v)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v)*(qR - qL);
  f = 0;
  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  x = 0.5;
  y = 0.1;
  u = 1.294369873971500E+00;
  v = 0;
  fTrue = 0.5*(nt*1 + nx*u + ny*v)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v)*(qR - qL);
  f = 0;
  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  x = -1;
  y =  0.5;
  u = 9.924389181161534E-01;
  v = 5.998829903835843E-03;
  fTrue = 0.5*(nt*1 + nx*u + ny*v)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v)*(qR - qL);
  f = 0;
  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  x = 0;
  y = 0.5;
  u = 1.009203507602450E+00;
  v = 4.410839493355371E-02;
  fTrue = 0.5*(nt*1 + nx*u + ny*v)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v)*(qR - qL);
  f = 0;
  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  x = 0.5;
  y = 0.5;
  u = 1.067034171759651E+00;
  v = 0;
  fTrue = 0.5*(nt*1 + nx*u + ny*v)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v)*(qR - qL);
  f = 0;
  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  x = 1;
  y = 0.5;
  u =  1.009203507602450E+00;
  v = -4.410839493355371E-02;
  fTrue = 0.5*(nt*1 + nx*u + ny*v)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v)*(qR - qL);
  f = 0;
  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  x = 2;
  y = 0.5;
  u =  9.924389181161534E-01;
  v = -5.998829903835843E-03;
  fTrue = 0.5*(nt*1 + nx*u + ny*v)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v)*(qR - qL);
  f = 0;
  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( cubicsourcebump_strongFlux )
{
  typedef AdvectiveFlux2D_CubicSourceBump::ArrayQ<Real> ArrayQ;
  const Real tol = 1.e-10;

  Real tau = 0.1;
  AdvectiveFlux2D_CubicSourceBump adv(tau);

  // functor

  ArrayQ q = 2.34;
  ArrayQ qx = 1.56;
  ArrayQ qy = 2.56;
  ArrayQ strongPDE;
  ArrayQ strongPDETrue;
  Real u, v;
  Real x, y;
  Real time = 0;

  x = -1;
  y =  0;
  u = 9.891151724088069E-01;
  v = 0;
  strongPDETrue = u*qx + v*qy;
  strongPDE = 0;
  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( strongPDETrue, strongPDE, tol );

  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( 2*strongPDETrue, strongPDE, tol );

  x = 0;
  y = 0;
  u = 7.669276655348198E-01;
  v = 0;
  strongPDETrue = u*qx + v*qy;
  strongPDE = 0;
  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( strongPDETrue, strongPDE, tol );

  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( 2*strongPDETrue, strongPDE, tol );

  x = 0.5;
  y = 0;
  u = 1.466144668930360E+00;
  v = 0;
  strongPDETrue = u*qx + v*qy;
  strongPDE = 0;
  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( strongPDETrue, strongPDE, tol );

  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( 2*strongPDETrue, strongPDE, tol );

  x = 1;
  y = 0;
  u = 7.669276655348198E-01;
  v = 0;
  strongPDETrue = u*qx + v*qy;
  strongPDE = 0;
  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( strongPDETrue, strongPDE, tol );

  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( 2*strongPDETrue, strongPDE, tol );

  x = 2;
  y = 0;
  u = 9.891151724088069E-01;
  v = 0;
  strongPDETrue = u*qx + v*qy;
  strongPDE = 0;
  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( strongPDETrue, strongPDE, tol );

  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( 2*strongPDETrue, strongPDE, tol );

  x = 0.5;
  y = 0.1;
  u = 1.294369873971500E+00;
  v = 0;
  strongPDETrue = u*qx + v*qy;
  strongPDE = 0;
  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( strongPDETrue, strongPDE, tol );

  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( 2*strongPDETrue, strongPDE, tol );

  x = -1;
  y =  0.5;
  u = 9.924389181161534E-01;
  v = 5.998829903835843E-03;
  strongPDETrue = u*qx + v*qy;
  strongPDE = 0;
  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( strongPDETrue, strongPDE, tol );

  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( 2*strongPDETrue, strongPDE, tol );

  x = 0;
  y = 0.5;
  u = 1.009203507602450E+00;
  v = 4.410839493355371E-02;
  strongPDETrue = u*qx + v*qy;
  strongPDE = 0;
  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( strongPDETrue, strongPDE, tol );

  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( 2*strongPDETrue, strongPDE, tol );

  x = 0.5;
  y = 0.5;
  u = 1.067034171759651E+00;
  v = 0;
  strongPDETrue = u*qx + v*qy;
  strongPDE = 0;
  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( strongPDETrue, strongPDE, tol );

  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( 2*strongPDETrue, strongPDE, tol );

  x = 1;
  y = 0.5;
  u =  1.009203507602450E+00;
  v = -4.410839493355371E-02;
  strongPDETrue = u*qx + v*qy;
  strongPDE = 0;
  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( strongPDETrue, strongPDE, tol );

  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( 2*strongPDETrue, strongPDE, tol );

  x = 2;
  y = 0.5;
  u =  9.924389181161534E-01;
  v = -5.998829903835843E-03;
  strongPDETrue = u*qx + v*qy;
  strongPDE = 0;
  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( strongPDETrue, strongPDE, tol );

  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( 2*strongPDETrue, strongPDE, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ConstRotation_flux )
{
  typedef AdvectiveFlux2D_ConstRotation::ArrayQ<Real> ArrayQ;
  const Real tol = 1.e-13;

  ArrayQ q = 2.34;
  Real V = 1.66;
  ArrayQ fx = 0, fy = 0;

  AdvectiveFlux2D_ConstRotation adv(V);

  // functor
  Real x = 1;
  Real y = 2;
  Real time = 0;
  Real u = - V * y / sqrt(x*x + y*y);
  Real v =   V * x / sqrt(x*x + y*y);
  ArrayQ fxTrue = u*q;
  ArrayQ fyTrue = v*q;
  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( fyTrue, fy, tol );

  adv.flux(x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( 2*fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( 2*fyTrue, fy, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ConstRotation_jacobian )
{
  typedef AdvectiveFlux2D_ConstRotation::ArrayQ<Real> ArrayQ;
  const Real tol = 1.e-13;

  Real x = 1;
  Real y = 2;
  Real time = 0;

  ArrayQ q = 2.34;
  Real V = 1.66;
  Real u = - V * y / sqrt(x*x + y*y);
  Real v =   V * x / sqrt(x*x + y*y);
  ArrayQ jacxTrue = u;
  ArrayQ jacyTrue = v;
  ArrayQ jacx = 0, jacy = 0;

  AdvectiveFlux2D_ConstRotation adv(V);

  // jacobian
  adv.jacobian(x, y, time, q, jacx, jacy);
  BOOST_CHECK_CLOSE( jacxTrue, jacx, tol );
  BOOST_CHECK_CLOSE( jacyTrue, jacy, tol );

  adv.jacobian(x, y, time, q, jacx, jacy);
  BOOST_CHECK_CLOSE( 2*jacxTrue, jacx, tol );
  BOOST_CHECK_CLOSE( 2*jacyTrue, jacy, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ConstRotation_fluxUpwind )
{
  typedef AdvectiveFlux2D_ConstRotation::ArrayQ<Real> ArrayQ;
  const Real tol = 1.e-13;

  Real x = 1;
  Real y = 2;
  Real time = 0;

  Real V = 1.66;
  Real u = - V * y / sqrt(x*x + y*y);
  Real v =   V * x / sqrt(x*x + y*y);

  AdvectiveFlux2D_ConstRotation adv(V);

  Real nx = 1.1;
  Real ny = 0.25;
  ArrayQ qL = u;
  ArrayQ qR = 0;
  ArrayQ f = 0;
  Real fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);

  // flux
  f = 0;
  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  qL = 0;
  qR = u;
  fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);
  // flux
  f = 0;
  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  nx = -1.1;
  ny = 0.25;
  qL = u;
  qR = 0;
  fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);
  // flux
  f = 0;
  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  nx = -1.1;
  ny = 0.25;
  qL = 0;
  qR = u;
  fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);
  // flux
  f = 0;
  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ConstRotation_strongFlux )
{
  typedef AdvectiveFlux2D_ConstRotation::ArrayQ<Real> ArrayQ;
  const Real tol = 1.e-13;

  Real x = 1;
  Real y = 2;
  Real time = 0;

  Real V = 1.66;
  Real u = - V * y / sqrt(x*x + y*y);
  Real v =   V * x / sqrt(x*x + y*y);

  ArrayQ q = u;
  ArrayQ qx = 1.56;
  ArrayQ qy = 2.56;
  Real strongPDETrue = u*qx + v*qy;

  AdvectiveFlux2D_ConstRotation adv(V);

  // strong flux
  ArrayQ strongPDE = 0;
  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( strongPDETrue, strongPDE, tol );

  adv.strongFlux(x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( 2*strongPDETrue, strongPDE, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PDEAdvectionDiffusion2D_AdvectiveFlux2D_pattern.txt", true );

  AdvectiveFlux2D_None adv0;
  adv0.dump( 2, output );

  Real u =  1.33479;
  Real v = -0.33886;
  AdvectiveFlux2D_Uniform adv1(u, v);
  adv1.dump( 2, output );

  Real tau = 0.1;
  AdvectiveFlux2D_CubicSourceBump adv2(tau);
  adv2.dump( 2, output );

  Real V = 1.66;
  AdvectiveFlux2D_ConstRotation adv3(V);
  adv3.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
