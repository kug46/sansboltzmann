// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ViscousFlux3D_btest
//
// test of 3-D diffusion matrix class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AdvectionDiffusion/ViscousFlux3D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ViscousFlux3D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  BOOST_CHECK( ViscousFlux3D_Uniform::D == 3 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform )
{
  typedef ViscousFlux3D_Uniform::ArrayQ<Real> ArrayQ;
  const Real tol = 1.e-13;

  Real kxx = 2.123, kxy = 0.553, kxz = 0.643;
  Real kyx = 0.378, kyy = 1.007, kyz = 0.765;
  Real kzx = 1.642, kzy = 1.299, kzz = 1.234;
  Real kxxTrue = kxx, kxyTrue = kxy, kxzTrue = kxz;
  Real kyxTrue = kyx, kyyTrue = kyy, kyzTrue = kyz;
  Real kzxTrue = kzx, kzyTrue = kzy, kzzTrue = kzz;

  ViscousFlux3D_Uniform visc( kxx, kxy, kxz, kyx, kyy, kyz, kzx, kzy, kzz );

  // static tests
  BOOST_CHECK( visc.D == 3 );

  // functor
  Real x = 1;
  Real y = 2;
  Real z = 3;
  Real time = 0;

  ArrayQ q = 3.263;
  ArrayQ qx = -0.445;
  ArrayQ qy = 1.741;
  ArrayQ qz = 4.531;
  ArrayQ qxx = 4.230;
  ArrayQ qyx = 3.904;
  ArrayQ qyy = -0.482;
  ArrayQ qzx = 7.512;
  ArrayQ qzy = -1.382;
  ArrayQ qzz = 0.781;

  kxx = kxy = kxz = 0;
  kyx = kyy = kyz = 0;
  kzx = kzy = kzz = 0;

  visc.diffusionViscous( x, y, z, time,
                         q, qx, qy, qz,
                         kxx, kxy, kxz,
                         kyx, kyy, kyz,
                         kzx, kzy, kzz );

  BOOST_CHECK_CLOSE( kxxTrue, kxx, tol );
  BOOST_CHECK_CLOSE( kxyTrue, kxy, tol );
  BOOST_CHECK_CLOSE( kxzTrue, kxz, tol );
  BOOST_CHECK_CLOSE( kyxTrue, kyx, tol );
  BOOST_CHECK_CLOSE( kyyTrue, kyy, tol );
  BOOST_CHECK_CLOSE( kyzTrue, kyz, tol );
  BOOST_CHECK_CLOSE( kzxTrue, kzx, tol );
  BOOST_CHECK_CLOSE( kzyTrue, kzy, tol );
  BOOST_CHECK_CLOSE( kzzTrue, kzz, tol );

  visc.diffusionViscous( x, y, z, time,
                         q, qx, qy, qz,
                         kxx, kxy, kxz,
                         kyx, kyy, kyz,
                         kzx, kzy, kzz );

  BOOST_CHECK_CLOSE( 2*kxxTrue, kxx, tol );
  BOOST_CHECK_CLOSE( 2*kxyTrue, kxy, tol );
  BOOST_CHECK_CLOSE( 2*kxzTrue, kxz, tol );
  BOOST_CHECK_CLOSE( 2*kyxTrue, kyx, tol );
  BOOST_CHECK_CLOSE( 2*kyyTrue, kyy, tol );
  BOOST_CHECK_CLOSE( 2*kyzTrue, kyz, tol );
  BOOST_CHECK_CLOSE( 2*kzxTrue, kzx, tol );
  BOOST_CHECK_CLOSE( 2*kzyTrue, kzy, tol );
  BOOST_CHECK_CLOSE( 2*kzzTrue, kzz, tol );

  // diffusive flux
  ArrayQ fTrue = -(kxxTrue*qx + kxyTrue*qy + kxzTrue*qz);
  ArrayQ gTrue = -(kyxTrue*qx + kyyTrue*qy + kyzTrue*qz);
  ArrayQ hTrue = -(kzxTrue*qx + kzyTrue*qy + kzzTrue*qz);
  ArrayQ f = 0;
  ArrayQ g = 0;
  ArrayQ h = 0;
  visc.fluxViscous( x, y, z, time, q, qx, qy, qz, f, g, h);
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( gTrue, g, tol );
  BOOST_CHECK_CLOSE( hTrue, h, tol );

  visc.fluxViscous( x, y, z, time, q, qx, qy, qz, f, g, h);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );
  BOOST_CHECK_CLOSE( 2*gTrue, g, tol );
  BOOST_CHECK_CLOSE( 2*hTrue, h, tol );

  ArrayQ strongPDETrue = - kxxTrue * qxx - (kxyTrue + kyxTrue) * qyx
                         - kyyTrue * qyy - (kxzTrue + kzxTrue) * qzx - (kyzTrue + kzyTrue) * qzy
                         - kzzTrue * qzz;

  ArrayQ strongPDE = 0;
  visc.strongFluxViscous( x, y, z, time, q, qx, qy, qz, qxx, qyx, qyy, qzx, qzy, qzz, strongPDE );
  BOOST_CHECK_CLOSE( strongPDE, strongPDETrue, tol );
  visc.strongFluxViscous( x, y, z, time, q, qx, qy, qz, qxx, qyx, qyy, qzx, qzy, qzz, strongPDE );
  BOOST_CHECK_CLOSE( strongPDE, 2*strongPDETrue, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( poly )
{
  typedef ViscousFlux3D_Poly::ArrayQ<Real> ArrayQ;
  typedef ViscousFlux3D_Poly::MatrixQ<Real> MatrixQ;
  const Real tol = 1.e-13;

  ArrayQ q = 2.;

  Real A = 15, B = 1;
  Real ATrue = A, BTrue = B;
  Real kxx = A + B * pow( q, 1 );      //Linear
  Real kyy = kxx;
  Real kzz = kxx;
  Real kxy = 0, kyx = 0, kyz = 0, kzy = 0, kzx = 0, kxz = 0;
  Real kxxTrue = kxx, kxyTrue = kxy, kxzTrue = kxz;
  Real kyxTrue = kyx, kyyTrue = kyy, kyzTrue = kyz;
  Real kzxTrue = kxz, kzyTrue = kzy, kzzTrue = kzz;

  ViscousFlux3D_Poly visc( A, B );

  // static tests
  BOOST_CHECK( visc.D == 3 );

  // functor
  Real x = .1;
  Real y = 2.;
  Real z = .3;
  Real time = 0.;


  ArrayQ qx = 3.;
  ArrayQ qxx = 4.;
  ArrayQ qy = .1;
  ArrayQ qyy = 5.;
  ArrayQ qz = .5;
  ArrayQ qzz = .6;
  ArrayQ qyx = 0.;
  ArrayQ qzx = 0.;
  ArrayQ qzy = 0.;

  kxx = kxy = kxz = 0;
  kyx = kyy = kyz = 0;
  kzx = kzy = kzz = 0;

  visc.diffusionViscous( x, y, z, time,
                         q, qx, qy, qz,
                         kxx, kxy, kxz,
                         kyx, kyy, kyz,
                         kzx, kzy, kzz );

  BOOST_CHECK_CLOSE( ATrue, A, tol );
  BOOST_CHECK_CLOSE( BTrue, B, tol );
  BOOST_CHECK_CLOSE( kxxTrue, kxx, tol );
  BOOST_CHECK_CLOSE( kxyTrue, kxy, tol );
  BOOST_CHECK_CLOSE( kxzTrue, kxz, tol );
  BOOST_CHECK_CLOSE( kyxTrue, kyx, tol );
  BOOST_CHECK_CLOSE( kyyTrue, kyy, tol );
  BOOST_CHECK_CLOSE( kyzTrue, kyz, tol );
  BOOST_CHECK_CLOSE( kzxTrue, kzx, tol );
  BOOST_CHECK_CLOSE( kzyTrue, kzy, tol );
  BOOST_CHECK_CLOSE( kzzTrue, kzz, tol );

  visc.diffusionViscous( x, y, z, time,
                         q, qx, qy, qz,
                         kxx, kxy, kxz,
                         kyx, kyy, kyz,
                         kzx, kzy, kzz );

  BOOST_CHECK_CLOSE( 2*kxxTrue, kxx, tol );
  BOOST_CHECK_CLOSE( 2*kxyTrue, kxy, tol );
  BOOST_CHECK_CLOSE( 2*kxzTrue, kxz, tol );
  BOOST_CHECK_CLOSE( 2*kyxTrue, kyx, tol );
  BOOST_CHECK_CLOSE( 2*kyyTrue, kyy, tol );
  BOOST_CHECK_CLOSE( 2*kyzTrue, kyz, tol );
  BOOST_CHECK_CLOSE( 2*kzxTrue, kzx, tol );
  BOOST_CHECK_CLOSE( 2*kzyTrue, kzy, tol );
  BOOST_CHECK_CLOSE( 2*kzzTrue, kzz, tol );

  // diffusive flux
  ArrayQ fTrue = -(kxxTrue*qx + kxyTrue*qy + kxzTrue*qz);
  ArrayQ gTrue = -(kyxTrue*qx + kyyTrue*qy + kyzTrue*qz);
  ArrayQ hTrue = -(kzxTrue*qx + kzyTrue*qy + kzzTrue*qz);
  ArrayQ f = 0;
  ArrayQ g = 0;
  ArrayQ h = 0;
  visc.fluxViscous( x, y, z, time, q, qx, qy, qz, f, g, h);
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( gTrue, g, tol );
  BOOST_CHECK_CLOSE( hTrue, h, tol );

  visc.fluxViscous( x, y, z, time, q, qx, qy, qz, f, g, h);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );
  BOOST_CHECK_CLOSE( 2*gTrue, g, tol );
  BOOST_CHECK_CLOSE( 2*hTrue, h, tol );

  ArrayQ strongPDETrue = -(B * qx * qx + (A + B * q) * qxx  +B * qy * qy + (A + B * q) * qyy  + B * qz * qz + (A + B * q) * qzz);

  ArrayQ strongPDE = 0;

  visc.strongFluxViscous( x, y, z, time, q, qx, qy, qz, qxx, qyx, qyy, qzx, qzy, qzz, strongPDE );

  BOOST_CHECK_CLOSE( strongPDE, strongPDETrue, tol );

  visc.strongFluxViscous( x, y, z, time, q, qx, qy, qz, qxx, qyx, qyy, qzx, qzy, qzz, strongPDE );

  BOOST_CHECK_CLOSE( strongPDE, 2*strongPDETrue, tol );

  MatrixQ axTrue = -B*qx;
  MatrixQ ayTrue = -B*qy;
  MatrixQ azTrue = -B*qz;

  MatrixQ ax = 0;
  MatrixQ ay = 0;
  MatrixQ az = 0;

  visc.jacobianFluxViscous(x, y, z, time, q, qx, qy, qz,  ax, ay, az);

  BOOST_CHECK_CLOSE(axTrue, ax, tol);
  BOOST_CHECK_CLOSE(ayTrue, ay, tol);
  BOOST_CHECK_CLOSE(azTrue, az, tol);

  visc.jacobianFluxViscous(x, y, z, time, q, qx, qy, qz,  ax, ay, az);

  BOOST_CHECK_CLOSE(2*axTrue, ax, tol);
  BOOST_CHECK_CLOSE(2*ayTrue, ay, tol);
  BOOST_CHECK_CLOSE(2*azTrue, az, tol);

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PDEAdvectionDiffusion3D_ViscousFlux3D_pattern.txt", true );

  {
    Real kxx = 2.123, kxy = 0.553, kxz = 0.643;
    Real kyx = 0.378, kyy = 1.007, kyz = 0.765;
    Real kzx = 1.642, kzy = 1.299, kzz = 1.234;
    ViscousFlux3D_Uniform visc1( kxx, kxy, kxz, kyx, kyy, kyz, kzx, kzy, kzz );
    visc1.dump( 2, output );
  }

  {
    Real A = 15;
    Real B = 1.2;
    ViscousFlux3D_Poly visc1( A, B );
    visc1.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
