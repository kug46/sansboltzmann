// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ViscousFlux2D_btest
//
// test of 2-D diffusion matrix class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AdvectionDiffusion/ViscousFlux2D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ViscousFlux2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  BOOST_CHECK( ViscousFlux2D_Uniform::D == 2 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform )
{
  typedef ViscousFlux2D_Uniform::ArrayQ<Real> ArrayQ;
  const Real tol = 1.e-13;

  Real kxx =  1.33479;
  Real kxy = -0.33886;
  Real kyx = -0.33886;
  Real kyy =  2.05673;
  Real kxxTrue = kxx;
  Real kxyTrue = kxy;
  Real kyxTrue = kyx;
  Real kyyTrue = kyy;
  Real dummyq = 1.0;

  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  // static tests
  BOOST_CHECK( visc.D == 2 );

  // diffusionViscous
  Real x = 1;
  Real y = 2;
  Real time = 0;

  ArrayQ q  =  3.263;
  ArrayQ qx = -0.445;
  ArrayQ qy =  1.741;
  ArrayQ qxx = 4.230;
  ArrayQ qyx = 3.904;
  ArrayQ qyy = -0.482;

  kxx = kxy = 0;
  kyx = kyy = 0;

  visc.diffusionViscous(x, y, time, dummyq, qx, qy, kxx, kxy, kyx, kyy);

  BOOST_CHECK_CLOSE( kxxTrue, kxx, tol );
  BOOST_CHECK_CLOSE( kxyTrue, kxy, tol );
  BOOST_CHECK_CLOSE( kyxTrue, kyx, tol );
  BOOST_CHECK_CLOSE( kyyTrue, kyy, tol );

  visc.diffusionViscous(x, y, time, dummyq, qx, qy, kxx, kxy, kyx, kyy);

  BOOST_CHECK_CLOSE( 2*kxxTrue, kxx, tol );
  BOOST_CHECK_CLOSE( 2*kxyTrue, kxy, tol );
  BOOST_CHECK_CLOSE( 2*kyxTrue, kyx, tol );
  BOOST_CHECK_CLOSE( 2*kyyTrue, kyy, tol );

  // diffusive flux
  ArrayQ fTrue = -(kxxTrue*qx + kxyTrue*qy);
  ArrayQ gTrue = -(kxyTrue*qx + kyyTrue*qy);
  ArrayQ f = 0;
  ArrayQ g = 0;
  visc.fluxViscous( x, y, time, q, qx, qy, f, g );
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( gTrue, g, tol );

  visc.fluxViscous( x, y, time, q, qx, qy, f, g );
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );
  BOOST_CHECK_CLOSE( 2*gTrue, g, tol );


  ArrayQ strongPDETrue = -kxxTrue*qxx - (kxyTrue + kyxTrue)*qyx - kyyTrue*qyy;

  ArrayQ strongPDE = 0;
  visc.strongFluxViscous(x, y, time, q, qx, qy, qxx, qyx, qyy, strongPDE);
  BOOST_CHECK_CLOSE( strongPDE, strongPDETrue, tol );
  visc.strongFluxViscous(x, y, time, q, qx, qy, qxx, qyx, qyy, strongPDE);
  BOOST_CHECK_CLOSE( strongPDE, 2*strongPDETrue, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PDEAdvectionDiffusion2D_ViscousFlux2D_pattern.txt", true );

  Real kxx =  1.33479;
  Real kxy = -0.33886;
  Real kyx = -0.33886;
  Real kyy =  2.05673;
  ViscousFlux2D_Uniform visc1(kxx, kxy, kyx, kyy);
  visc1.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
