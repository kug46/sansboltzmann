// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AdvectionDiffusion_Sensor_btest
//

#include <iostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Sensor.h"


using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{

}


using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( AdvectionDiffusion_Sensor_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors_AD )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None> PDEAdvectionDiffusion2D;

  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEAdvectionDiffusion3D;

  typedef AdvectionDiffusion_Sensor Sensor;

  // Construct the base AD PDE properties
  Real u = 1.0;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux1D_Uniform adv1D(u);
  AdvectiveFlux2D_Uniform adv2D(u, v);
  AdvectiveFlux3D_Uniform adv3D(u, v, w);

  Real kxx = 2.123, kxy = 0.553, kxz = 0.643;
  Real kyx = 0.378, kyy = 1.007, kyz = 0.765;
  Real kzx = 1.642, kzy = 1.299, kzz = 1.234;
  ViscousFlux1D_Uniform visc1D(kxx);
  ViscousFlux2D_Uniform visc2D(kxx, kxy, kyx, kyy);
  ViscousFlux3D_Uniform visc3D( kxx, kxy, kxz,
                                    kyx, kyy, kyz,
                                    kzx, kzy, kzz );

  Source1D_None source1D;
  Source2D_None source2D;
  Source3D_None source3D;

  PDEAdvectionDiffusion1D pde1D(adv1D, visc1D, source1D);
  PDEAdvectionDiffusion2D pde2D(adv2D, visc2D, source2D);
  PDEAdvectionDiffusion3D pde3D(adv3D, visc3D, source3D);

  Sensor sensor1D(pde1D);
  Sensor sensor2D(pde2D);
  Sensor sensor3D(pde3D);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jumpSensor_1D )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef AdvectionDiffusion_Sensor Sensor;
  typedef PDEAdvectionDiffusion1D::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None src;

  PDEAdvectionDiffusion1D pde(adv, visc, src);
  Sensor sensor(pde);

  Real sln = 3.263;

  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );

  Real JumpQuantityTrue = sln;
  Real JumpQunatityPDE = 0;
  sensor.jumpQuantity(q, JumpQunatityPDE);
  BOOST_CHECK_CLOSE( JumpQuantityTrue, JumpQunatityPDE, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jumpSensor_2D )
{

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None> PDEAdvectionDiffusion2D;
  typedef AdvectionDiffusion_Sensor Sensor2D;
  typedef PDEAdvectionDiffusion2D::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.724;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy,
                                 kyx, kyy);

  Source2D_None source;

  PDEAdvectionDiffusion2D pde(adv, visc, source);
  Sensor2D sensor(pde);

  Real sln = 3.263;

  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );

  Real JumpQuantityTrue = sln;
  Real JumpQunatityPDE = 0;
  sensor.jumpQuantity(q, JumpQunatityPDE);
  BOOST_CHECK_CLOSE( JumpQuantityTrue, JumpQunatityPDE, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jumpSensor_3D )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEAdvectionDiffusion3D;
  typedef AdvectionDiffusion_Sensor Sensor3D;
  typedef PDEAdvectionDiffusion3D::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123, kxy = 0.553, kxz = 0.643;
  Real kyx = 0.378, kyy = 1.007, kyz = 0.765;
  Real kzx = 1.642, kzy = 1.299, kzz = 1.234;

  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                                  kyx, kyy, kyz,
                                  kzx, kzy, kzz );

  Source3D_None source;

  PDEAdvectionDiffusion3D pde(adv, visc, source);
  Sensor3D sensor(pde);

  Real sln = 3.263;

  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );

  Real JumpQuantityTrue = sln;
  Real JumpQunatityPDE = 0;
  sensor.jumpQuantity(q, JumpQunatityPDE);
  BOOST_CHECK_CLOSE( JumpQuantityTrue, JumpQunatityPDE, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
