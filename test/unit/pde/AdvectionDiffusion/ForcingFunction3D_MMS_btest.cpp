// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ForcingFunction3D_MMS_btest
//
// test of 3-D MMS forcing functions for advection diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/ForcingFunction3D_MMS.h"
#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ForcingFunction3D_MMS_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( sinesinesine )
{
  const Real tol = 1.e-13;

  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None > PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;

  ScalarFunction3D_SineSineSine soln;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123, kxy = 0.553, kxz = 0.643;
  Real kyx = 0.378, kyy = 1.007, kyz = 0.765;
  Real kzx = 1.642, kzy = 1.299, kzz = 1.234;

  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                                  kyx, kyy, kyz,
                                  kzx, kzy, kzz );

  typedef ForcingFunction3D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(soln));

  Source3D_None source;

  PDEClass pde(adv, visc, source, forcingptr);

  // static tests
  BOOST_CHECK( (*forcingptr).D == 3 );

  // functor
  Real x = sqrt(3);
  Real y = sqrt(7);
  Real z = sqrt(5);
  Real snx = sin(2*PI*x), csx = cos(2*PI*x);
  Real sny = sin(2*PI*y), csy = cos(2*PI*y);
  Real snz = sin(2*PI*z), csz = cos(2*PI*z);

  Real srcTrue = 2*PI*((u*csx*sny*snz + 2*PI*( kxx*snx*sny*snz - kxy*csx*csy*snz - kxz*csx*sny*csz)) +
                       (v*snx*csy*snz + 2*PI*(-kyx*csx*csy*snz + kyy*snx*sny*snz - kyz*snx*csy*csz)) +
                       (w*snx*sny*csz + 2*PI*(-kzx*csx*sny*csz - kzy*snx*csy*csz + kzz*snx*sny*snz)));

  Real time = 0;
  ArrayQ src = 0;
  pde.forcingFunction(x, y, z, time, src);
  BOOST_CHECK_CLOSE( srcTrue, src, tol );

  pde.forcingFunction(x, y, z, time, src);
  BOOST_CHECK_CLOSE( 2*srcTrue, src, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( monomial )
{
  const Real tol = 1.e-13;

  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None > PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;

  int i = 2, j = 3, k = 4;
  ScalarFunction3D_Monomial soln(i,j,k);

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123, kxy = 0.553, kxz = 0.643;
  Real kyx = 0.378, kyy = 1.007, kyz = 0.765;
  Real kzx = 1.642, kzy = 1.299, kzz = 1.234;

  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                                  kyx, kyy, kyz,
                                  kzx, kzy, kzz );

  typedef ForcingFunction3D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(soln));
  Source3D_None source;

  PDEClass pde(adv, visc, source, forcingptr);

  // static tests
  BOOST_CHECK( (*forcingptr).D == 3 );

  // functor
  Real x = sqrt(3);
  Real y = sqrt(7);
  Real z = sqrt(5);

  Real srcTrue = u*pow(x, i-1) * pow(y, j  ) * pow(z, k  ) * i
               + v*pow(x, i  ) * pow(y, j-1) * pow(z, k  ) * j
               + w*pow(x, i  ) * pow(y, j  ) * pow(z, k-1) * k

               - kxx * pow(x, i-2) * pow(y, j  ) * pow(z, k  ) * i*(i-1)
               - kxy * pow(x, i-1) * pow(y, j-1) * pow(z, k  ) * i*j
               - kxz * pow(x, i-1) * pow(y, j  ) * pow(z, k-1) * i*k

               - kyx * pow(x, i-1) * pow(y, j-1) * pow(z, k  ) * i*j
               - kyy * pow(x, i  ) * pow(y, j-2) * pow(z, k  ) * j*(j-1)
               - kyz * pow(x, i  ) * pow(y, j-1) * pow(z, k-1) * j*k

               - kzx * pow(x, i-1) * pow(y, j  ) * pow(z, k-1) * i*k
               - kzy * pow(x, i  ) * pow(y, j-1) * pow(z, k-1) * j*k
               - kzz * pow(x, i  ) * pow(y, j  ) * pow(z, k-2) * k*(k-1);

  Real time = 0;
  ArrayQ src = 0;
  pde.forcingFunction(x, y, z, time, src);
  BOOST_CHECK_CLOSE( srcTrue, src, tol );

  pde.forcingFunction(x, y, z, time, src);
  BOOST_CHECK_CLOSE( 2*srcTrue, src, tol );
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PDEAdvectionDiffusion3D_ForcingFunction3D_MMS_pattern.txt", true );

  ForcingFunction3D_Const forcing2( 1 );
  forcing2.dump( 2, output );

  ForcingFunction3D_Monomial forcing3( 2, 5, 4, 1 );
  forcing3.dump( 2, output );

  ForcingFunction3D_SineSineSine forcing4( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 );
  forcing4.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
