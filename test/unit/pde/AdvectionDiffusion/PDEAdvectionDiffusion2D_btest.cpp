// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEAdvectionDiffusion2D_btest
//
// test of 2-D Advection-Diffusion PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"


using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class PDEAdvectionDiffusion<PhysD2,
                                     AdvectiveFlux2D_Uniform,
                                     ViscousFlux2D_Uniform,
                                     Source2D_None>;

}


using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEAdvectionDiffusion2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None> PDEClass;

  BOOST_CHECK( PDEClass::D == 2 );
  BOOST_CHECK( PDEClass::N == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None> PDEClass;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  typedef ForcingFunction2D_Const<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(1) );

  PDEClass pde(adv, visc, source, forcingptr);

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == true );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // function tests

  Real x, y, time;
  Real sln, slnx, slny;

  x = 0; y = 0, time = 0;
  sln  =  3.263;
  slnx = -0.445;
  slny =  1.741;

  DLA::VectorS<2,Real> X = {x,y};


  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = qDataPrim[0];
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  // master state
  ArrayQ uCons = 0;
  pde.masterState( x, y, time, q, uCons );
  BOOST_CHECK_CLOSE( qTrue, uCons, tol );

  // unsteady flux
  ArrayQ ft = 0;
  pde.fluxAdvectiveTime( x, y, time, q, ft );
  BOOST_CHECK_CLOSE( qTrue, ft, tol );

  pde.fluxAdvectiveTime( x, y, time, q, ft );
  BOOST_CHECK_CLOSE( 2*qTrue, ft, tol );

  MatrixQ dudq = 0;
  pde.jacobianMasterState( x, y, time, q, dudq );
  BOOST_CHECK_CLOSE( 1, dudq, tol );

  // advective flux
  Real fData[1] = {u*sln};
  Real gData[1] = {v*sln};
  ArrayQ fTrue = fData[0];
  ArrayQ gTrue = gData[0];
  ArrayQ f, g;
  DLA::VectorS<2,ArrayQ> F;
  f = 0;
  g = 0;
  pde.fluxAdvective( x, y, time, q, f, g );
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( gTrue, g, tol );

  pde.fluxAdvective( x, y, time, q, f, g );
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );
  BOOST_CHECK_CLOSE( 2*gTrue, g, tol );


  // advective flux jacobian
  MatrixQ dfdu = 0;
  MatrixQ dgdu = 0;
  pde.jacobianFluxAdvective( x, y, time, q, dfdu, dgdu );
  BOOST_CHECK_CLOSE( u, dfdu, tol );
  BOOST_CHECK_CLOSE( v, dgdu, tol );

  pde.jacobianFluxAdvective( x, y, time, q, dfdu, dgdu );
  BOOST_CHECK_CLOSE( 2*u, dfdu, tol );
  BOOST_CHECK_CLOSE( 2*v, dgdu, tol );

  // set gradient
  qDataPrim[0] = slnx;
  ArrayQ qxTrue = qDataPrim[0];
  ArrayQ qx;
  pde.setDOFFrom( qx, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxTrue, qx, tol );

  qDataPrim[0] = slny;
  ArrayQ qyTrue = qDataPrim[0];
  ArrayQ qy;
  pde.setDOFFrom( qy, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyTrue, qy, tol );

  DLA::VectorS<2, ArrayQ> gradq = {qx, qy};

  // diffusive flux
  fTrue = -(kxx*slnx + kxy*slny);
  gTrue = -(kxy*slnx + kyy*slny);
  f = 0;
  g = 0;
  pde.fluxViscous( x, y, time, q, qx, qy, f, g );
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( gTrue, g, tol );

  pde.fluxViscous( x, y, time, q, qx, qy, f, g );
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );
  BOOST_CHECK_CLOSE( 2*gTrue, g, tol );

  // diffusive flux jacobian
  MatrixQ kxxMtx = 0, kxyMtx = 0, kyxMtx = 0, kyyMtx = 0;
  pde.diffusionViscous( x, y, time,
                        q, qx, qy,
                        kxxMtx, kxyMtx,
                        kyxMtx, kyyMtx );
  BOOST_CHECK_CLOSE( kxx, kxxMtx, tol );
  BOOST_CHECK_CLOSE( kxy, kxyMtx, tol );
  BOOST_CHECK_CLOSE( kxy, kyxMtx, tol );
  BOOST_CHECK_CLOSE( kyy, kyyMtx, tol );

  pde.diffusionViscous( x, y, time,
                        q, qx, qy,
                        kxxMtx, kxyMtx,
                        kyxMtx, kyyMtx );
  BOOST_CHECK_CLOSE( 2*kxx, kxxMtx, tol );
  BOOST_CHECK_CLOSE( 2*kxy, kxyMtx, tol );
  BOOST_CHECK_CLOSE( 2*kxy, kyxMtx, tol );
  BOOST_CHECK_CLOSE( 2*kyy, kyyMtx, tol );

  // forcing function
  ArrayQ src = 0;
  pde.forcingFunction( x, y, time, src );
  BOOST_CHECK_CLOSE( 1, src, tol );

  pde.forcingFunction( x, y, time, src );
  BOOST_CHECK_CLOSE( 2, src, tol );

  // for coverage; functions are empty
  src = 0;
  pde.source( x, y, time, q, qx, qy, src );

  ArrayQ qp = 0, qpx = 0, qpy = 0;
  ArrayQ sourceterm_split = 0;
  pde.source( x, y, time, q, qp, qx, qy, qpx, qpy, sourceterm_split );
  BOOST_CHECK_CLOSE( sourceterm_split, src, tol );

  pde.sourceTrace( x, y, x, y, time, q, qx, qy, q, qx, qy, src, src );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxRoe )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // advective flux function

  Real x, y, time;
  Real nx, ny;
  Real slnL, slnR;

  x = 0; y = 0, time = 0;   // not actually used in functions
  nx = 1.22; ny = -0.432;
  slnL = 3.26; slnR = 1.79;

  DLA::VectorS<2, Real> X = {x, y};
  DLA::VectorS<2, Real> N = {nx, ny};

  // set
  Real qLDataPrim[1] = {slnL};
  string qLNamePrim[1] = {"Solution"};
  ArrayQ qLTrue = qLDataPrim[0];
  ArrayQ qL;
  pde.setDOFFrom( qL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qLTrue, qL, tol );

  Real qRDataPrim[1] = {slnR};
  string qRNamePrim[1] = {"Solution"};
  ArrayQ qRTrue = qRDataPrim[0];
  ArrayQ qR;
  pde.setDOFFrom( qR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qRTrue, qR, tol );

  // advective normal flux
  Real fnData[1] = {(nx*u + ny*v)*slnL};
  ArrayQ fnTrue = fnData[0];
  ArrayQ fn;
  fn = 0;
  pde.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fnTrue, fn, tol );

  pde.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue, fn, tol );

  // viscous flux function

  Real slnxL, slnyL;
  Real slnxR, slnyR;

  slnxL = 1.325;  slnyL = -0.457;
  slnxR = 0.327;  slnyR =  3.421;

  // set gradient
  qLDataPrim[0] = slnxL;
  ArrayQ qxLTrue = qLDataPrim[0];
  ArrayQ qxL;
  pde.setDOFFrom( qxL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxLTrue, qxL, tol );

  qLDataPrim[0] = slnyL;
  ArrayQ qyLTrue = qLDataPrim[0];
  ArrayQ qyL;
  pde.setDOFFrom( qyL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyLTrue, qyL, tol );

  DLA::VectorS<2, ArrayQ> gradqL = {qxL, qyL};

  qRDataPrim[0] = slnxR;
  ArrayQ qxRTrue = qRDataPrim[0];
  ArrayQ qxR;
  pde.setDOFFrom( qxR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxRTrue, qxR, tol );

  qRDataPrim[0] = slnyR;
  ArrayQ qyRTrue = qRDataPrim[0];
  ArrayQ qyR;
  pde.setDOFFrom( qyR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyRTrue, qyR, tol );

  DLA::VectorS<2, ArrayQ> gradqR = {qxR, qyR};

  // viscous normal flux
  fnData[0] = -0.5*((nx*kxx + kxy*ny)*(slnxL + slnxR) + (nx*kxy + ny*kyy)*(slnyL + slnyR));
  fn = 0;
  pde.fluxViscous( x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fnData[0], fn, tol );

  pde.fluxViscous( x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );
  BOOST_CHECK_CLOSE( 2*fnData[0], fn, tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  Real time = 0;

  PDEClass pde(adv, visc, source);

  Real x, y;
  Real dx, dy;
  Real sln;
  Real speed, speedTrue;

  x = 0; y = 0;   // not actually used in functions
  dx = 1.22; dy = -0.432;
  sln  =  3.263;

  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = qDataPrim[0];
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  speedTrue = fabs(dx*u + dy*v)/sqrt(dx*dx + dy*dy);

  pde.speedCharacteristic( x, y, time, dx, dy, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  pde.speedCharacteristic( x, y, time, q, speed );
  BOOST_CHECK_CLOSE( sqrt(u*u + v*v), speed, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  ArrayQ q = 0;
  BOOST_CHECK( pde.isValidState(q) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PDEAdvectionDiffusion2D_pattern.txt", true );

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None> PDEClass;
  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

//  ForcingFunction2D_Const forcing( 1 );
//

  typedef ForcingFunction2D_Const<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(1) );

  PDEClass pde1(adv, visc, source, forcingptr);
  pde1.dump( 2, output );

  PDEClass pde2(adv, visc, source, forcingptr);
  pde2.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
