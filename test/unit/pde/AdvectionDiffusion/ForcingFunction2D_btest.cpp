// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ForcingFunction2D_btest
//
// test of 2-D advection velocity class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AdvectionDiffusion/ForcingFunction2D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ForcingFunction2D_test_suite )

class DummyPDE
{
public:
  template <class T>
  using ArrayQ = T;    // solution/residual arrays
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( constant )
{
  const Real tol = 1.e-13;
  DummyPDE pde;

  ForcingFunction2D_Const<DummyPDE> forcing( 1 );

  // functor
  Real x = 1;
  Real y = 2;
  Real time = 0;
  ForcingFunction2D_Const<DummyPDE>::ArrayQ<Real> src = 0;
  forcing(pde, x, y, time, src);
  BOOST_CHECK_CLOSE( 1, src, tol );

  forcing(pde, x, y, time, src);
  BOOST_CHECK_CLOSE( 2, src, tol );
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( monomial )
{
  const Real tol = 1.e-13;

  Real nu = 1;
  ForcingFunction2D_Monomial forcing( 3, 4, nu );

  // static tests
  BOOST_CHECK( forcing.D == 2 );

  // functor
  Real x, y, src;

  x = 1;
  y = 2;
  Real time = 0;
  forcing(x, y, time, src);
  BOOST_CHECK_CLOSE( -144, src, tol );

  x = 2;
  y = 1;
  forcing(x, y, time, src);
  BOOST_CHECK_CLOSE( -108, src, tol );

  x = -2;
  y =  0;
  forcing(x, y, time, src);
  BOOST_CHECK_SMALL( src, tol );
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( sinesine )
{
  const Real tol = 1.e-13;
  DummyPDE pde;

  Real u = 1;
  Real v = 0.2;

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;

  Real time = 0;

  ForcingFunction2D_SineSine<DummyPDE> forcing( u, v, kxx, kxy, kxy, kyy );

  // functor
  Real x = sqrt(3);
  Real y = sqrt(7);
  Real snx = sin(2*PI*x), csx = cos(2*PI*x);
  Real sny = sin(2*PI*y), csy = cos(2*PI*y);
  Real srcTrue = 2*PI*(csx*(u*sny - 4*PI*kxy*csy) + snx*(v*csy + 2*PI*(kxx + kyy)*sny));

  Real src = 0;
  forcing(pde, x, y, time, src);
  BOOST_CHECK_CLOSE( srcTrue, src, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( sineTheta )
{
  const Real tol = 1.e-13;
  DummyPDE pde;

  const Real c = log( 2.0 );

  Real time = 0;

  ForcingFunction2D_SineTheta<DummyPDE> forcing( c );

  // functor
  Real x = 4;
  Real y = 3;
  Real srcTrue = c * 0.8;  // = c * cos(theta) = c * x / sqrt(x^2+y^2)

  Real src = 0;
  forcing(pde, x, y, time, src);
  BOOST_CHECK_CLOSE( srcTrue, src, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ThetaGeometricSeries )
{
  const Real tol = 1.e-13;
  DummyPDE pde;

  const Real c = log( 2.0 );
  const int pmax = 10;
  Real monomial;
  Real theta;

  Real x, y, time;
  Real src;
  Real srcTrue;

  x = 0.8;
  y = 0.6;
  time = 0;
  theta = atan2(y,x);

  monomial = 1;
  srcTrue = 0;
  for (int i = 0; i < pmax; i++)
  {
    if ( i < 1)
      srcTrue = 0;
    else
    {
      srcTrue += static_cast<Real>(i) * monomial;
      monomial = pow(theta,i);
    }

    ForcingFunction2D_ThetaGeometricSeries<DummyPDE> forcing( c, i );
    forcing(pde, x, y, time, src);

    SANS_CHECK_CLOSE( c*srcTrue, src, tol, tol );
  }

  ForcingFunction2D_ThetaGeometricSeries<DummyPDE> forcing( c, -1 );
  BOOST_CHECK_THROW( forcing(pde, x, y, time, src);, SANSException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PDEAdvectionDiffusion2D_ForcingFunction2D_pattern.txt", true );

  ForcingFunction2D_Const<DummyPDE> forcing2( 1 );
  forcing2.dump( 2, output );
#if 0
  ForcingFunction2D_Monomial<DummyPDE> forcing3( 2, 5, 1 );
  forcing3.dump( 2, output );
#endif
  ForcingFunction2D_SineSine<DummyPDE> forcing4( 1, 2, 3, 4, 5, 6 );
  forcing4.dump( 2, output );
  ForcingFunction2D_SineTheta<DummyPDE> forcing5( 2 );
  forcing5.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
