// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//--------------------------------------------------------------------------- //
// BCAdvectionDiffusion1D_btest
//
// test of 1-D Advection-Diffusion BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/BCParameters.h"

using namespace std;
using namespace SANS;


typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_None> PDEAdvectionDiffusion1D;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class BCAdvectionDiffusion< PhysD1, BCTypeFunction_mitState<AdvectiveFlux1D_Uniform,ViscousFlux1D_Uniform> >;

// Instantiate BCParamaters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters<BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform,ViscousFlux1D_Uniform>>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( BCAdvectionDiffusion1D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  {
  typedef BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_mitLG> BCClass;

  BOOST_CHECK( BCClass::D == 1 );
  BOOST_CHECK( BCClass::N == 1 );
  BOOST_CHECK( BCClass::NBC == 1 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCParameters_test )
{
  typedef BCParameters<BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform,ViscousFlux1D_Uniform>> BCParams;

  BOOST_CHECK_EQUAL( "None"              , BCParams::params.BC.None );
  BOOST_CHECK_EQUAL( "LinearRobin_mitLG" , BCParams::params.BC.LinearRobin_mitLG );
  BOOST_CHECK_EQUAL( "LinearRobin_sansLG", BCParams::params.BC.LinearRobin_sansLG );
  BOOST_CHECK_EQUAL( "Function_mitState" , BCParams::params.BC.Function_mitState);
  BOOST_CHECK_EQUAL( "FunctionLinearRobin_mitLG"    , BCParams::params.BC.FunctionLinearRobin_mitLG);
  BOOST_CHECK_EQUAL( "FunctionLinearRobin_sansLG"   , BCParams::params.BC.FunctionLinearRobin_sansLG );

  PyDict BC0;
  BC0[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BC3;
  BC3[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BC3[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_mitLG>::params.A] = 2;
  BC3[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_mitLG>::params.B] = 3;
  BC3[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_mitLG>::params.bcdata] = 4;

  PyDict BCList;
  BCList["BC0"] = BC0;
  BCList["BC3"] = BC3;

  //No exceptions should be thrown
  BCParams::checkInputs(BCList);

  // Construct a pde for the BC's
  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEAdvectionDiffusion1D pde(adv, visc, source);

  // Create the BC's
  std::map<std::string, std::shared_ptr<BCBase> > BCs = BCParams::createBCs<BCNDConvertSpace>(pde, BCList);

  BOOST_CHECK_EQUAL( 2, BCs.size() );

  // The python dictionary does not preserve order, so the count of each BC should be 1
  int NoneCount = 0;
  int LinearRobinCount = 0;

  // Extract the keys from the dictionary
  std::vector<std::string> keys = BCList.stringKeys();

  for ( std::size_t i = 0; i < keys.size(); i++ )
  {
    if ( BCs[keys[i]]->derivedTypeID() == typeid(BCNDConvertSpace<PhysD1,BCNone<PhysD1,1>>) )
      NoneCount++;
    else if ( BCs[keys[i]]->derivedTypeID() == typeid(BCNDConvertSpace<PhysD1,BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_mitLG>>) )
      LinearRobinCount++;
    else
      BOOST_CHECK( false ); // Failure..
  }

  BOOST_CHECK_EQUAL( 1, NoneCount );
  BOOST_CHECK_EQUAL( 1, LinearRobinCount );

  std::map< std::string, std::vector<int> > BoundaryGroups;

  BoundaryGroups["BC0"] = {0};
  BoundaryGroups["BC3"] = {4};

  std::vector<int> boundaryGroupsLG = BCParams::getLGBoundaryGroups(BCList, BoundaryGroups);

  BOOST_REQUIRE_EQUAL( 1, boundaryGroupsLG.size() );
  BOOST_CHECK_EQUAL( 4, boundaryGroupsLG[0] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCNone_test )
{
  typedef BCNone<PhysD1,AdvectionDiffusionTraits<PhysD1>::N> BCClass;

  BCClass bc;

  // static tests
  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 0 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeLinearRobin_test )
{
  typedef BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_mitLG> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;
  Real time = 0;

  Real A = 1;
  Real B = 0.2;
  Real bcdata = 3;

  BCClass bc( A, B, bcdata );

  // static tests
  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  MatrixQ AMtx;
  MatrixQ BMtx;
  ArrayQ bcdataVec;

  Real x = 0;
  Real nx = 0.8;
  //Real ny = 0.6;

  AMtx = 0;
  BMtx = 0;
  bc.coefficients( x, time, nx, AMtx, BMtx );
  BOOST_CHECK_CLOSE( A, AMtx, tol );
  BOOST_CHECK_CLOSE( B, BMtx, tol );

  bcdataVec = 0;
  bc.data( x, time, nx, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFlux_test )
{
  typedef BCAdvectionDiffusion< PhysD2, BCTypeFlux<PDEAdvectionDiffusion2D> > BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  typedef PDEAdvectionDiffusion2D PDEClass;

  const Real tol = 1.e-13;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  Real time = 0;

  PDEClass pde(adv, visc, source);

  Real bcdata = 3;

  BCClass bc( pde, bcdata );

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  Real x = 0;
  Real y = 0;
  Real nx = 0.8;
  Real ny = 0.6;

  Real A = nx*u + ny*v;
  Real B = -1;

  MatrixQ AMtx;
  MatrixQ BMtx;
  bc.coefficients( x, y, time, nx, ny, AMtx, BMtx );
  BOOST_CHECK_CLOSE( A, AMtx, tol );
  BOOST_CHECK_CLOSE( B, BMtx, tol );

  ArrayQ bcdataVec;
  bc.data( x, y, time, nx, ny, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFunction_test )
{
  typedef BCAdvectionDiffusion< PhysD1, BCTypeFunctionLinearRobin_mitLG > BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  typedef ScalarFunction1D_Sine SolutionType;
  std::shared_ptr<SolutionType> slnExact( new SolutionType );

  Real kxx = 1.003;
  ViscousFlux1D_Uniform visc(kxx);

  {
    BCClass bc( slnExact, visc );

    Real time = 0;

    // static tests
    BOOST_CHECK( bc.D == 1 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 1 );

    // function tests

    MatrixQ AMtx;
    MatrixQ BMtx;
    ArrayQ bcdataVec;

    Real x = 0.75;
    Real nx = 0.8;

    AMtx = 0;
    BMtx = 0;
    bc.coefficients( x, time, nx, AMtx, BMtx );
    BOOST_CHECK_CLOSE( 1, AMtx, tol );
    BOOST_CHECK_CLOSE( 0, BMtx, tol );

    bcdataVec = 0;
    bc.data( x, time, nx, bcdataVec );
    BOOST_CHECK_CLOSE( -1, bcdataVec, tol );
  }

  {
    Real A = 0.35;
    Real B = 0.84;
    BCClass bc( slnExact, visc, A, B );

    Real time = 0;

    // static tests
    BOOST_CHECK( bc.D == 1 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 1 );

    // function tests

    MatrixQ AMtx;
    MatrixQ BMtx;
    ArrayQ bcdataVec;

    Real x = 0.75;
    Real nx = 0.8;

    AMtx = 0;
    BMtx = 0;
    bc.coefficients( x, time, nx, AMtx, BMtx );
    BOOST_CHECK_CLOSE( A, AMtx, tol );
    BOOST_CHECK_CLOSE( B, BMtx, tol );

    Real u = sin(2*PI*x);
    Real ux = 2*PI*cos(2*PI*x);
    Real bcdataTrue = A*u + B*kxx*ux*nx;

    bcdataVec = 0;
    bc.data( x, time, nx, bcdataVec );
    BOOST_CHECK_CLOSE( bcdataTrue, bcdataVec, tol );
  }

}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/BCAdvectionDiffusion1D_pattern.txt", true );

  {
  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin> BCClass;

  Real A = 1;
  Real B = 0.2;
  Real bcdata = 3;

  BCClass bc1( A, B, bcdata );
  bc1.dump( 2, output );
  }


  {
  typedef BCAdvectionDiffusion< PhysD2, BCTypeFlux<PDEAdvectionDiffusion2D> > BCClass;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEAdvectionDiffusion2D pde( adv, visc, source );

  Real bcdata = 3;

  BCClass bc2( pde, bcdata );
  bc2.dump( 2, output );
  }


  BOOST_CHECK( output.match_pattern() );
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
