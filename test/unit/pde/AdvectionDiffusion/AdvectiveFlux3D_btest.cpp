// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AdvectieFlux3D_btest
//
// test of 3-D advection flux class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AdvectionDiffusion/AdvectiveFlux3D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( AdvectiveFlux3D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( None )
{
  typedef AdvectiveFlux3D_None::ArrayQ<Real> ArrayQ;
  typedef AdvectiveFlux3D_None::MatrixQ<Real> MatrixQ;

  Real nx = 1.0;
  Real ny = 0.25;
  Real nz = 0.3;
  Real nt = 0.5;
  Real x = 1;
  Real y = 2;
  Real z = 3;
  Real time = 0;

  AdvectiveFlux3D_None adv;

  ArrayQ q = 1.45;
  ArrayQ qL = 2.45;
  ArrayQ qR = 3.45;
  ArrayQ qx = 1.56;
  ArrayQ qy = 2.56;
  ArrayQ qz = 3.56;
  ArrayQ fxTrue = 1.234;
  ArrayQ fx = fxTrue;
  ArrayQ fyTrue = 2.234;
  ArrayQ fy = fyTrue;
  ArrayQ fzTrue = 3.234;
  ArrayQ fz = fzTrue;
  MatrixQ axTrue = 3.45;
  MatrixQ ax = axTrue;
  MatrixQ ayTrue = 5.45;
  MatrixQ ay = ayTrue;
  MatrixQ azTrue = 6.45;
  MatrixQ az = azTrue;

  BOOST_CHECK( adv.hasFluxAdvective() == false );

  adv.flux(x, y, z, time, q, fx, fy, fz);
  BOOST_CHECK_EQUAL( fxTrue, fx );
  BOOST_CHECK_EQUAL( fyTrue, fy );
  BOOST_CHECK_EQUAL( fzTrue, fz );

  adv.fluxUpwind(x, y, z, time, qL, qR, nx, ny, nz, fx);
  BOOST_CHECK_EQUAL( fxTrue, fx );

  adv.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, fx);
  BOOST_CHECK_EQUAL( fxTrue + qL*nt, fx );

  adv.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, fx);
  BOOST_CHECK_EQUAL( fxTrue + 2*qL*nt, fx );

  adv.jacobian(x, y, z, time, q, ax, ay, az);
  BOOST_CHECK_EQUAL( axTrue, ax );
  BOOST_CHECK_EQUAL( ayTrue, ay );
  BOOST_CHECK_EQUAL( azTrue, az );

  ArrayQ strongPDETrue = 5.78;
  ArrayQ strongPDE = strongPDETrue;
  adv.strongFlux(x, y, z, time, q, qx, qy, qz, strongPDE);
  BOOST_CHECK_EQUAL( strongPDETrue, strongPDE );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform )
{
  typedef AdvectiveFlux3D_Uniform::ArrayQ<Real> ArrayQ;
  const Real tol = 1.e-13;

  Real x = 1;
  Real y = 2;
  Real z = 3;
  Real time = 0;

  ArrayQ q = 2.34;
  Real u =  1.33479;
  Real v = -0.33886;
  Real w = 0.2578;

  AdvectiveFlux3D_Uniform adv(u, v, w);

  BOOST_CHECK( adv.hasFluxAdvective() );

  //--------------------------//
  // flux                     //
  //--------------------------//

  ArrayQ fxTrue = u*q;
  ArrayQ fyTrue = v*q;
  ArrayQ fzTrue = w*q;
  ArrayQ fx = 0, fy = 0, fz = 0;

  adv.flux(x, y, z, time, q, fx, fy, fz);
  BOOST_CHECK_CLOSE( fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( fyTrue, fy, tol );
  BOOST_CHECK_CLOSE( fzTrue, fz, tol );

  adv.flux(x, y, z, time, q, fx, fy, fz);
  BOOST_CHECK_CLOSE( 2*fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( 2*fyTrue, fy, tol );
  BOOST_CHECK_CLOSE( 2*fzTrue, fz, tol );

  //--------------------------//
  // jacobian                 //
  //--------------------------//

  ArrayQ jacxTrue = u;
  ArrayQ jacyTrue = v;
  ArrayQ jaczTrue = w;
  ArrayQ jacx = 0, jacy = 0, jacz = 0;

  adv.jacobian(x, y, z, time, q, jacx, jacy, jacz);
  BOOST_CHECK_CLOSE( jacxTrue, jacx, tol );
  BOOST_CHECK_CLOSE( jacyTrue, jacy, tol );
  BOOST_CHECK_CLOSE( jaczTrue, jacz, tol );

  adv.jacobian(x, y, z, time, q, jacx, jacy, jacz);
  BOOST_CHECK_CLOSE( 2*jacxTrue, jacx, tol );
  BOOST_CHECK_CLOSE( 2*jacyTrue, jacy, tol );
  BOOST_CHECK_CLOSE( 2*jaczTrue, jacz, tol );

  //--------------------------//
  // fluxUpwind               //
  //--------------------------//

  Real nx = 1.1;
  Real ny = 0.25;
  Real nz = 0.3;
  ArrayQ qL = u;
  ArrayQ qR = 0;
  ArrayQ f = 0;
  Real fTrue = 0.5*(nx*u + ny*v + nz*w)*(qR + qL) - 0.5*fabs(nx*u + ny*v + nz*w)*(qR - qL);
  f = 0;
  adv.fluxUpwind(x, y, z, time, qL, qR, nx, ny, nz, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, z, time, qL, qR, nx, ny, nz, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  qL = 0;
  qR = u;
  fTrue = 0.5*(nx*u + ny*v + nz*w)*(qR + qL) - 0.5*fabs(nx*u + ny*v + nz*w)*(qR - qL);
  f = 0;
  adv.fluxUpwind(x, y, z, time, qL, qR, nx, ny, nz, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, z, time, qL, qR, nx, ny, nz, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  nx = -1.1;
  ny = 0.25;
  qL = u;
  qR = 0;
  fTrue = 0.5*(nx*u + ny*v + nz*w)*(qR + qL) - 0.5*fabs(nx*u + ny*v + nz*w)*(qR - qL);
  f = 0;
  adv.fluxUpwind(x, y, z, time, qL, qR, nx, ny, nz, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, z, time, qL, qR, nx, ny, nz, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  nx = -1.1;
  ny = 0.25;
  qL = 0;
  qR = u;
  fTrue = 0.5*(nx*u + ny*v + nz*w)*(qR + qL) - 0.5*fabs(nx*u + ny*v + nz*w)*(qR - qL);
  f = 0;
  adv.fluxUpwind(x, y, z, time, qL, qR, nx, ny, nz, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwind(x, y, z, time, qL, qR, nx, ny, nz, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  //--------------------------//
  // fluxUpwindSpaceTime      //
  //--------------------------//
  Real nt = 0.65;

  fTrue = 0.5*(nt*1 + nx*u + ny*v + nz*w)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v + nz*w)*(qR - qL);
  f = 0;
  adv.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  nx = 1.0;
  qL = 0;
  qR = u;
  fTrue = 0.5*(nt*1 + nx*u + ny*v + nz*w)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v + nz*w)*(qR - qL);
  f = 0;
  adv.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  nx = -1.1;
  qL = u;
  qR = 0;
  fTrue = 0.5*(nt*1 + nx*u + ny*v + nz*w)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v + nz*w)*(qR - qL);
  f = 0;
  adv.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  nx = -1.1;
  qL = 0;
  qR = u;
  fTrue = 0.5*(nt*1 + nx*u + ny*v + nz*w)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v + nz*w)*(qR - qL);
  f = 0;
  adv.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  nx = -1.1;
  ny = -0.5;
  nt = -0.1;
  qL = 0;
  qR = u;
  fTrue = 0.5*(nt*1 + nx*u + ny*v + nz*w)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v + nz*w)*(qR - qL);
  f = 0;
  adv.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  adv.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  //--------------------------//
  // strongFlux               //
  //--------------------------//

  ArrayQ qx = 1.56;
  ArrayQ qy = 2.56;
  ArrayQ qz = 3.56;
  Real strongPDETrue = u*qx + v*qy + w*qz;

  ArrayQ strongPDE = 0;
  adv.strongFlux(x, y, z, time, q, qx, qy, qz, strongPDE);
  BOOST_CHECK_CLOSE( strongPDETrue, strongPDE, tol );

  adv.strongFlux(x, y, z, time, q, qx, qy, qz, strongPDE);
  BOOST_CHECK_CLOSE( 2*strongPDETrue, strongPDE, tol );
}

//---------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PolyZ )
{
  typedef AdvectiveFlux3D_PolyZ::ArrayQ<Real> ArrayQ;
  const Real close_tol = 1.e-13;
  const Real small_tol = 5.e-14;

   Real x = 1.1;
   Real y = 2.1;
   Real z = 5.;
   Real time = 0;

   Real a0, a1, a2, a3, a4, a5, a6, a7, a8, a9;
   a0 = 1.2;
   a1 = -0.2;
   a2 = 3.5;
   a3 = 1.5;
   a4 = -1;
   a5 = 0.256;
   a6 = 3.5;
   a7 = 1.56;
   a8 = 1.89;
   a9 = 0.5;

   ArrayQ q  = 2.6;

   Real u = 0;
   Real v = 0;
   Real w = (a0+ a1*z+  a2*pow(z,2)+  a3*pow(z,3)+  a4*pow(z,4)+  a5*pow(z,5)+  a6*pow(z,6)+  a7*pow(z,7)+  a8*pow(z,8)+  a9*pow(z,9));

   AdvectiveFlux3D_PolyZ adv(a0, a1, a2, a3, a4, a5, a6, a7, a8, a9);

   BOOST_CHECK( adv.hasFluxAdvective() );

   //---------------------------------------//
   //              flux                     //
   //---------------------------------------//

   ArrayQ fxTrue = u*q;
   ArrayQ fyTrue = v*q;
   ArrayQ fzTrue = w*q;
   ArrayQ fx = 0, fy = 0, fz = 0;

   adv.flux(x, y, z, time, q, fx, fy, fz);
   SANS_CHECK_CLOSE( fxTrue, fx, small_tol, close_tol );
   SANS_CHECK_CLOSE( fyTrue, fy, small_tol, close_tol );
   SANS_CHECK_CLOSE( fzTrue, fz, small_tol, close_tol );

   adv.flux(x, y, z, time, q, fx, fy, fz);
   SANS_CHECK_CLOSE( 2*fxTrue, fx, small_tol, close_tol );
   SANS_CHECK_CLOSE( 2*fyTrue, fy, small_tol, close_tol );
   SANS_CHECK_CLOSE( 2*fzTrue, fz, small_tol, close_tol );


   //---------------------------------------//
   //             jacobian                  //
   //---------------------------------------//

   ArrayQ jacxTrue = u;
   ArrayQ jacyTrue = v;
   ArrayQ jaczTrue = w;
   ArrayQ jacx = 0, jacy = 0, jacz = 0;

   adv.jacobian(x, y, z, time, q, jacx, jacy, jacz);
   SANS_CHECK_CLOSE( jacxTrue, jacx, small_tol, close_tol );
   SANS_CHECK_CLOSE( jacyTrue, jacy, small_tol, close_tol );
   SANS_CHECK_CLOSE( jaczTrue, jacz, small_tol, close_tol );

   adv.jacobian(x, y, z, time, q, jacx, jacy, jacz);
   SANS_CHECK_CLOSE( 2*jacxTrue, jacx, small_tol, close_tol );
   SANS_CHECK_CLOSE( 2*jacyTrue, jacy, small_tol, close_tol );
   SANS_CHECK_CLOSE( 2*jaczTrue, jacz, small_tol, close_tol );

   //---------------------------------------//
   //             fluxUpwind                //
   //---------------------------------------//

   Real nx = 1.1;
   Real ny = 0.25;
   Real nz = -0.3;
   ArrayQ qL = w;
   ArrayQ qR = 0;
   ArrayQ f = 0;
   Real fTrue = 0.5*(nz*w)*(qR + qL) - 0.5*fabs(nz*w)*(qR - qL);
   f = 0;
   adv.fluxUpwind( x, y, z, time, qL, qR, nx, ny, nz, f );
   SANS_CHECK_CLOSE( fTrue, f, small_tol, close_tol );

   adv.fluxUpwind( x, y, z, time, qL, qR, nx, ny, nz, f );
   SANS_CHECK_CLOSE( 2*fTrue, f, small_tol, close_tol );

   qL = 0;
   qR = w;
   fTrue = 0.5*(nz*w)*(qR + qL) - 0.5*fabs(nz*w)*(qR - qL);
   f = 0;
   adv.fluxUpwind( x, y, z, time, qL, qR, nx, ny, nz, f );
   SANS_CHECK_CLOSE( fTrue, f, small_tol, close_tol );

   adv.fluxUpwind( x, y, z, time, qL, qR, nx, ny, nz, f );
   SANS_CHECK_CLOSE( 2*fTrue, f, small_tol, close_tol );

   nx = 1.1;
   ny = 0.25;
   nz = -0.3;
   qL = w;
   qR = 0;
   fTrue = 0.5*(nz*w)*(qR + qL) - 0.5*fabs(nz*w)*(qR - qL);
   f = 0;
   adv.fluxUpwind( x, y, z, time, qL, qR, nx, ny, nz, f );
   SANS_CHECK_CLOSE( fTrue, f, small_tol, close_tol );

   adv.fluxUpwind( x, y, z, time, qL, qR, nx, ny, nz, f );
   SANS_CHECK_CLOSE( 2*fTrue, f, small_tol, close_tol );

   nx = 1.1;
   ny = 0.25;
   nz = -0.3;
   qL = 0;
   qR = w;
   fTrue = 0.5*(nz*w)*(qR + qL) - 0.5*fabs(nz*w)*(qR - qL);
   f = 0;
   adv.fluxUpwind( x, y, z, time, qL, qR, nx, ny, nz, f );
   SANS_CHECK_CLOSE( fTrue, f, small_tol, close_tol );

   adv.fluxUpwind( x, y, z, time, qL, qR, nx, ny, nz, f );
   SANS_CHECK_CLOSE( 2*fTrue, f, small_tol, close_tol );

   //---------------------------------------//
   //       fluxUpwindSpaceTime             //
   //---------------------------------------//

   Real nt = 0.55;

   fTrue = 0.5*(nt*1 + nz*w)*(qR + qL) - 0.5*fabs(nt*1 + nz*w)*(qR - qL);
   f = 0;
   adv.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f);
   SANS_CHECK_CLOSE( fTrue, f, small_tol, close_tol );

   adv.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f);
   SANS_CHECK_CLOSE( 2*fTrue, f, small_tol, close_tol );

   nz = 1.;
   qL = 0;
   qR = w;
   fTrue = 0.5*(nt*1 + nz*w)*(qR + qL) - 0.5*fabs(nt*1 + nz*w)*(qR - qL);
   f = 0;
   adv.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f);
   SANS_CHECK_CLOSE( fTrue, f, small_tol, close_tol );

   adv.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f);
   SANS_CHECK_CLOSE( 2*fTrue, f, small_tol, close_tol );

   nz = -1.2;
   qL = w;
   qR = 0;
   fTrue = 0.5*(nt*1 + nz*w)*(qR + qL) - 0.5*fabs(nt*1 + nz*w)*(qR - qL);
   f = 0;
   adv.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f);
   SANS_CHECK_CLOSE( fTrue, f, small_tol, close_tol );

   adv.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f);
   SANS_CHECK_CLOSE( 2*fTrue, f, small_tol, close_tol );

   nz = -1.2;
   qL = 0;
   qR = w;
   fTrue = 0.5*(nt*1 + nz*w)*(qR + qL) - 0.5*fabs(nt*1 + nz*w)*(qR - qL);
   f = 0;
   adv.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f);
   SANS_CHECK_CLOSE( fTrue, f, small_tol, close_tol );

   adv.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f);
   SANS_CHECK_CLOSE( 2*fTrue, f, small_tol, close_tol );

   nt = -0.55;
   nz = -1.2;
   qL = 0;
   qR = w;
   fTrue = 0.5*(nt*1 + nz*w)*(qR + qL) - 0.5*fabs(nt*1 + nz*w)*(qR - qL);
   f = 0;
   adv.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f);
   SANS_CHECK_CLOSE( fTrue, f, small_tol, close_tol );

   adv.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f);
   SANS_CHECK_CLOSE( 2*fTrue, f, small_tol, close_tol );

   //---------------------------------------//
   //           strongflux                  //
   //---------------------------------------//

   ArrayQ qx = 1.45;
   ArrayQ qy = 2.45;
   ArrayQ qz = 3.45;
   Real dwdz = a1 +2*a2*z +3*a3*pow(z,2)  +4*a4*pow(z,3)  +5*a5*pow(z,4)  +6*a6*pow(z,5)  +7*a7*pow(z,6)  +8*a8*pow(z,7)  +9*a9*pow(z,8);

   Real strongPDETrue = w*qz + dwdz*q;

   // strong flux
   ArrayQ strongPDE = 0;
   adv.strongFlux(x, y, z, time, q, qx, qy, qz, strongPDE);
   SANS_CHECK_CLOSE( strongPDETrue, strongPDE, small_tol, close_tol );

   adv.strongFlux(x, y, z, time, q, qx, qy, qz, strongPDE);
   SANS_CHECK_CLOSE( 2*strongPDETrue, strongPDE, small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PDEAdvectionDiffusion3D_AdvectiveFlux3D_pattern.txt", true );

  {
    AdvectiveFlux3D_None adv0;
    adv0.dump( 2, output );

    Real u =  1.33479;
    Real v = -0.33886;
    Real w = 0.2578;
    AdvectiveFlux3D_Uniform adv1(u, v, w);
    adv1.dump( 2, output );
  }

  {

    Real a0, a1, a2, a3, a4, a5, a6, a7, a8, a9;
    a0 = 1.2;
    a1 = -0.2;
    a2 = 3.5;
    a3 = 1.5;
    a4 = -1;
    a5 = 0.256;
    a6 = 3.5;
    a7 = 1.56;
    a8 = 1.89;
    a9 = 0.5;

    AdvectiveFlux3D_PolyZ adv1(a0, a1, a2, a3, a4, a5, a6, a7, a8, a9);
    adv1.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );


}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
