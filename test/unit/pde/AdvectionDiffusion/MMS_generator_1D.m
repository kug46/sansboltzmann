close all

xplot = linspace(0,1,10000);
xunit = [0, 0.25, 0.5, 0.75, 1];

visc = 1;
conv = 0;

syms f1 f2 x u
a =21;
f1 = x*(1-x)*exp(7*x-1)/21;
y = matlabFunction(f1);
z = y(xplot);
plot(xplot,z);
hold all
% Manufactured Solution
fx1 = simplify(diff(f1));
fxx1 = simplify(diff(diff(f1)));
yxx = matlabFunction(-visc*fxx1 + conv*fx1);

y(xunit)
yxx(xunit)
