// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ViscousFlux1D_btest
//
// test of 1-D diffusion matrix class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AdvectionDiffusion/ViscousFlux1D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ViscousFlux1D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  BOOST_CHECK( ViscousFlux1D_Uniform::D == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( none )
{
  typedef ViscousFlux1D_None::ArrayQ<Real> ArrayQ;

  Real kxxTrue = 0;

  ViscousFlux1D_None visc;

  // static tests
  BOOST_CHECK( visc.D == 1 );

  ArrayQ q  =  3.263;
  ArrayQ qx = -0.445;
  ArrayQ qxx = 4.230;

  // functor
  Real x = 1;
  Real time = 0;

  ArrayQ fTrue = 0;
  ArrayQ f = 0;

  visc.fluxViscous(x, time, q, qx, f);
  BOOST_CHECK_EQUAL( fTrue, f );

  visc.fluxViscous(x, time, q, qx, f);
  BOOST_CHECK_EQUAL( 2*fTrue, f );

  Real kxx = 0;

  visc.diffusionViscous(x, time, q, qx, kxx);
  BOOST_CHECK_EQUAL( kxxTrue, kxx );

  visc.diffusionViscous(x, time, q, qx, kxx);
  BOOST_CHECK_EQUAL( 2*kxxTrue, kxx );

  ArrayQ strongPDETrue = 0;
  ArrayQ strongPDE = 0;

  visc.strongFluxViscous(x, time, q, qx, qxx, strongPDE);
  BOOST_CHECK_EQUAL( strongPDE, strongPDETrue );

  visc.strongFluxViscous(x, time, q, qx, qxx, strongPDE);
  BOOST_CHECK_EQUAL( strongPDE, 2*strongPDETrue );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform )
{
  typedef ViscousFlux1D_Uniform::ArrayQ<Real> ArrayQ;
  const Real tol = 1.e-13;

  Real kxx =  1.33479;
  Real kxxTrue = kxx;

  ViscousFlux1D_Uniform visc(kxx);

  // static tests
  BOOST_CHECK( visc.D == 1 );

  ArrayQ q  =  3.263;
  ArrayQ qx = -0.445;
  ArrayQ qxx = 4.230;

  // functor
  Real x = 1;
  Real time = 0;

  ArrayQ fTrue = -kxxTrue*qx;
  ArrayQ f = 0;

  visc.fluxViscous(x, time, q, qx, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  visc.fluxViscous(x, time, q, qx, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  kxx = 0;

  visc.diffusionViscous(x, time, q, qx, kxx);
  BOOST_CHECK_CLOSE( kxxTrue, kxx, tol );

  visc.diffusionViscous(x, time, q, qx, kxx);
  BOOST_CHECK_CLOSE( 2*kxxTrue, kxx, tol );

  ArrayQ strongPDETrue = -kxxTrue*qxx;

  ArrayQ strongPDE = 0;
  visc.strongFluxViscous(x, time, q, qx, qxx, strongPDE);
  BOOST_CHECK_CLOSE( strongPDE, strongPDETrue, tol );

  visc.strongFluxViscous(x, time, q, qx, qxx, strongPDE);
  BOOST_CHECK_CLOSE( strongPDE, 2*strongPDETrue, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Exp )
{
  typedef ViscousFlux1D_Exp::ArrayQ<Real> ArrayQ;
  typedef ViscousFlux1D_Exp::MatrixQ<Real> MatrixQ;
  const Real tol = 1.e-13;

  ArrayQ q  =  3.263;
  ArrayQ qx = -0.445;
  ArrayQ qxx = 4.230;

  Real lam =  1.33479;
  Real kxxTrue = exp(lam*q);

  ViscousFlux1D_Exp visc(lam);

  // static tests
  BOOST_CHECK( visc.D == 1 );

  // functor
  Real x = 1;
  Real time = 0;

  ArrayQ fTrue = -kxxTrue*qx;
  ArrayQ f = 0;

  visc.fluxViscous(x, time, q, qx, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  visc.fluxViscous(x, time, q, qx, f);
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  Real kxx = 0;

  visc.diffusionViscous(x, time, q, qx, kxx);
  BOOST_CHECK_CLOSE( kxxTrue, kxx, tol );

  visc.diffusionViscous(x, time, q, qx, kxx);
  BOOST_CHECK_CLOSE( 2*kxxTrue, kxx, tol );

  ArrayQ strongPDETrue = -lam*exp(lam*q)*qx*qx - kxxTrue*qxx;

  ArrayQ strongPDE = 0;
  visc.strongFluxViscous(x, time, q, qx, qxx, strongPDE);
  BOOST_CHECK_CLOSE( strongPDETrue, strongPDE, tol );

  visc.strongFluxViscous(x, time, q, qx, qxx, strongPDE);
  BOOST_CHECK_CLOSE( 2*strongPDETrue, strongPDE, tol );

  MatrixQ axTrue = -lam*exp(lam*q)*qx;

  MatrixQ ax = 0;
  visc.jacobianFluxViscous(x, time, q, qx, ax);
  BOOST_CHECK_CLOSE( axTrue, ax, tol );

  visc.jacobianFluxViscous(x, time, q, qx, ax);
  BOOST_CHECK_CLOSE( 2*axTrue, ax, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PDEAdvectionDiffusion1D_ViscousFlux1D_pattern.txt", true );

  Real kxx =  1.33479;
  ViscousFlux1D_Uniform visc1(kxx);
  visc1.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
