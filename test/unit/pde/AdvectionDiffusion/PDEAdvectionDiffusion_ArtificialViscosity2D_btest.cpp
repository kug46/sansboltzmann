// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEAdvectionDiffusion2D_btest
//
// test of 2-D Advection-Diffusion PDE class with artificial viscosity

#include <boost/test/output_test_stream.hpp>
#include <boost/test/test_tools.hpp>
#include <boost/test/unit_test_suite.hpp>
#include <cmath>
#include <memory>
#include <string>

#include "Field/Tuple/ParamTuple.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "pde/AdvectionDiffusion/AdvectionDiffusion_Traits.h"
#include "pde/AdvectionDiffusion/AdvectiveFlux2D.h"
#include "pde/AdvectionDiffusion/ForcingFunction2D.h"
#include "pde/AdvectionDiffusion/Source2D.h"
#include "pde/AdvectionDiffusion/ViscousFlux2D.h"
#include "tools/smoothmath.h"
#include "tools/SANSnumerics.h"
#include "tools/Tuple.h"
#include "Topology/Dimension.h"

using boost::test_tools::output_test_stream;

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion_ArtificialViscosity2D.h"


using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class PDEAdvectionDiffusion_ArtificialViscosity<PhysD2,
                                                         AdvectiveFlux2D_Uniform,
                                                         ViscousFlux2D_Uniform,
                                                         Source2D_None>;

}


using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEAdvectionDiffusion_ArtificialViscosity2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDEAdvectionDiffusion_ArtificialViscosity<PhysD2,
                                                    AdvectiveFlux2D_Uniform,
                                                    ViscousFlux2D_Uniform,
                                                    Source2D_None> PDEClass;

  BOOST_CHECK( PDEClass::D == 2 );
  BOOST_CHECK( PDEClass::N == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef PDEAdvectionDiffusion_ArtificialViscosity<PhysD2,
                                                    AdvectiveFlux2D_Uniform,
                                                    ViscousFlux2D_Uniform,
                                                    Source2D_None> PDEClass;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde(adv, visc, source , 1);

  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux )
{
  typedef PDEAdvectionDiffusion_ArtificialViscosity<PhysD2,
                                                    AdvectiveFlux2D_Uniform,
                                                    ViscousFlux2D_Uniform,
                                                    Source2D_None> PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> MatrixSym;
  typedef MakeTuple<ParamTuple, MatrixSym, Real>::type ParamType_GenH;

  const Real tol = 1.e-11;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  typedef ForcingFunction2D_Const<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(1) );

  int order = 1;
  PDEClass pde(adv, visc, source, forcingptr, order);

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == true );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // function tests

  Real x, y, time;
  Real sln, slnx, slny;

  x = 0; y = 0, time = 0;
  sln  =  3.263;
  slnx = -0.445;
  slny =  1.741;

  DLA::VectorS<2,Real> X = {x,y};

  Real hbar = 0.2;
  MatrixSym logH = {{log(hbar)},{0.0, log(hbar)}};
  ParamType_GenH param_genHL(logH, 0.2); // grid spacing and sensor values


  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = qDataPrim[0];
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  // unsteady flux
  ArrayQ uCons = 0;
  pde.masterState( param_genHL, x, y, time, q, uCons );
  BOOST_CHECK_CLOSE( qTrue, uCons, tol );

  MatrixQ dudq = 0;
  pde.jacobianMasterState( param_genHL, x, y, time, q, dudq );
  BOOST_CHECK_CLOSE( 1, dudq, tol );

  // advective flux
  Real fData[1] = {u*sln};
  Real gData[1] = {v*sln};
  ArrayQ fTrue = fData[0];
  ArrayQ gTrue = gData[0];
  ArrayQ f, g;
  DLA::VectorS<2,ArrayQ> F;
  f = 0;
  g = 0;
  pde.fluxAdvective( param_genHL, x, y, time, q, f, g );
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( gTrue, g, tol );

  pde.fluxAdvective( param_genHL, x, y, time, q, f, g );
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );
  BOOST_CHECK_CLOSE( 2*gTrue, g, tol );


  // advective flux jacobian
  MatrixQ dfdu = 0;
  MatrixQ dgdu = 0;
  pde.jacobianFluxAdvective( param_genHL, x, y, time, q, dfdu, dgdu );
  BOOST_CHECK_CLOSE( u, dfdu, tol );
  BOOST_CHECK_CLOSE( v, dgdu, tol );

  pde.jacobianFluxAdvective( param_genHL, x, y, time, q, dfdu, dgdu );
  BOOST_CHECK_CLOSE( 2*u, dfdu, tol );
  BOOST_CHECK_CLOSE( 2*v, dgdu, tol );

  // set gradient
  qDataPrim[0] = slnx;
  ArrayQ qxTrue = qDataPrim[0];
  ArrayQ qx;
  pde.setDOFFrom( qx, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxTrue, qx, tol );

  qDataPrim[0] = slny;
  ArrayQ qyTrue = qDataPrim[0];
  ArrayQ qy;
  pde.setDOFFrom( qy, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyTrue, qy, tol );

  DLA::VectorS<2, ArrayQ> gradq = {qx, qy};

  Real speed_true = sqrt(u*u + v*v);
  Real speed = 0;
  pde.speedCharacteristic(param_genHL, x, y, time, q, speed);
  BOOST_CHECK_CLOSE( speed_true, speed, tol );

  // Artificial viscosity with genH scale
  Real kxx_art_true = hbar/(Real(order)+1) * speed_true * smoothabs0(param_genHL.right(), 1.0e-5);
  MatrixQ kxx_art00 = 0;
  MatrixQ kxx_art01 = 0;
  MatrixQ kxx_art10 = 0;
  MatrixQ kxx_art11 = 0;
  pde.artificialViscosity(x, y, time, q, param_genHL.left(), param_genHL.right(),
                          kxx_art00, kxx_art01, kxx_art10, kxx_art11);
  BOOST_CHECK_CLOSE( kxx_art_true, kxx_art00, tol );
  BOOST_CHECK_CLOSE( 0.0, kxx_art01, tol );
  BOOST_CHECK_CLOSE( 0.0, kxx_art10, tol );
  BOOST_CHECK_CLOSE( kxx_art_true, kxx_art11, tol );

  // diffusive flux
  fTrue = -((kxx + kxx_art_true)*slnx + kxy*slny);
  gTrue = -(kxy*slnx + (kyy + kxx_art_true)*slny);
  f = 0;
  g = 0;
  pde.fluxViscous( param_genHL, x, y, time, q, qx, qy, f, g );
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( gTrue, g, tol );

  pde.fluxViscous( param_genHL, x, y, time, q, qx, qy, f, g );
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );
  BOOST_CHECK_CLOSE( 2*gTrue, g, tol );

  // diffusive flux jacobian
  MatrixQ kxxMtx = 0, kxyMtx = 0, kyxMtx = 0, kyyMtx = 0;
  pde.diffusionViscous( param_genHL, x, y, time, q, qx, qy, kxxMtx, kxyMtx, kyxMtx, kyyMtx );
  BOOST_CHECK_CLOSE( kxx + kxx_art_true, kxxMtx, tol );
  BOOST_CHECK_CLOSE( kxy, kxyMtx, tol );
  BOOST_CHECK_CLOSE( kxy, kyxMtx, tol );
  BOOST_CHECK_CLOSE( kyy + kxx_art_true, kyyMtx, tol );

  pde.diffusionViscous( param_genHL, x, y, time, q, qx, qy, kxxMtx, kxyMtx, kyxMtx, kyyMtx );
  BOOST_CHECK_CLOSE( 2*(kxx + kxx_art_true), kxxMtx, tol );
  BOOST_CHECK_CLOSE( 2*kxy, kxyMtx, tol );
  BOOST_CHECK_CLOSE( 2*kxy, kyxMtx, tol );
  BOOST_CHECK_CLOSE( 2*(kyy + kxx_art_true), kyyMtx, tol );

  // forcing function
  ArrayQ src = 0;
  pde.forcingFunction( param_genHL, x, y, time, src );
  BOOST_CHECK_CLOSE( 1, src, tol );

  pde.forcingFunction( param_genHL, x, y, time, src );
  BOOST_CHECK_CLOSE( 2, src, tol );

  // for coverage; functions are empty
  pde.source( param_genHL, x, y, time, q, qx, qy, src );
  pde.sourceTrace( param_genHL, x, y, param_genHL, x, y, time, q, qx, qy, q, qx, qy, src, src );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxRoe )
{
  typedef PDEAdvectionDiffusion_ArtificialViscosity<PhysD2,
                                                    AdvectiveFlux2D_Uniform,
                                                    ViscousFlux2D_Uniform,
                                                    Source2D_None> PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> MatrixSym;
  typedef MakeTuple<ParamTuple, MatrixSym, Real>::type ParamType_GenH;

  const Real tol = 1.e-13;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  int order = 1;
  PDEClass pde(adv, visc, source, order);

  // advective flux function

  Real x, y, time;
  Real nx, ny;
  Real slnL, slnR;

  x = 0; y = 0, time = 0;   // not actually used in functions
  nx = 1.22; ny = -0.432;
  slnL = 3.26; slnR = 1.79;

  DLA::VectorS<2, Real> X = {x, y};
  DLA::VectorS<2, Real> N = {nx, ny};

  Real hbar = 0.2;
  MatrixSym logH = {{log(hbar)},{0.0, log(hbar)}};
  ParamType_GenH param_genHL(logH, 0.2); // grid spacing and sensor values
  ParamType_GenH param_genHR(logH, 0.1); // grid spacing and sensor values

  // set
  Real qLDataPrim[1] = {slnL};
  string qLNamePrim[1] = {"Solution"};
  ArrayQ qLTrue = qLDataPrim[0];
  ArrayQ qL;
  pde.setDOFFrom( qL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qLTrue, qL, tol );

  Real qRDataPrim[1] = {slnR};
  string qRNamePrim[1] = {"Solution"};
  ArrayQ qRTrue = qRDataPrim[0];
  ArrayQ qR;
  pde.setDOFFrom( qR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qRTrue, qR, tol );

  // advective normal flux
  Real fnData[1] = {(nx*u + ny*v)*slnL};
  ArrayQ fnTrue = fnData[0];
  ArrayQ fn;
  fn = 0;
  pde.fluxAdvectiveUpwind( param_genHL, x, y, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fnTrue, fn, tol );

  pde.fluxAdvectiveUpwind( param_genHL, x, y, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue, fn, tol );

  // viscous flux function

  Real slnxL, slnyL;
  Real slnxR, slnyR;

  slnxL = 1.325;  slnyL = -0.457;
  slnxR = 0.327;  slnyR =  3.421;

  // set gradient
  qLDataPrim[0] = slnxL;
  ArrayQ qxLTrue = qLDataPrim[0];
  ArrayQ qxL;
  pde.setDOFFrom( qxL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxLTrue, qxL, tol );

  qLDataPrim[0] = slnyL;
  ArrayQ qyLTrue = qLDataPrim[0];
  ArrayQ qyL;
  pde.setDOFFrom( qyL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyLTrue, qyL, tol );

  DLA::VectorS<2, ArrayQ> gradqL = {qxL, qyL};

  qRDataPrim[0] = slnxR;
  ArrayQ qxRTrue = qRDataPrim[0];
  ArrayQ qxR;
  pde.setDOFFrom( qxR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxRTrue, qxR, tol );

  qRDataPrim[0] = slnyR;
  ArrayQ qyRTrue = qRDataPrim[0];
  ArrayQ qyR;
  pde.setDOFFrom( qyR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyRTrue, qyR, tol );

  DLA::VectorS<2, ArrayQ> gradqR = {qxR, qyR};

  // viscous normal flux
  Real speed_true = sqrt(u*u + v*v);
  Real kxx_art_trueL = hbar/(Real(order)+1) * speed_true * smoothabs0(param_genHL.right(), 1.0e-5);
  Real kxx_art_trueR = hbar/(Real(order)+1) * speed_true * smoothabs0(param_genHR.right(), 1.0e-5);

  Real fLTrue = -((kxx + kxx_art_trueL)*slnxL + kxy*slnyL);
  Real gLTrue = -(kxy*slnxL + (kyy + kxx_art_trueL)*slnyL);

  Real fRTrue = -((kxx + kxx_art_trueR)*slnxR + kxy*slnyR);
  Real gRTrue = -(kxy*slnxR + (kyy + kxx_art_trueR)*slnyR);

  fnTrue = 0.5*(fLTrue + fRTrue)*nx + 0.5*(gLTrue + gRTrue)*ny;

  fn = 0;
  pde.fluxViscous( param_genHL, param_genHR, x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );

  BOOST_CHECK_CLOSE( fnTrue, fn, tol );

  pde.fluxViscous( param_genHL, param_genHR, x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );

  BOOST_CHECK_CLOSE( 2*fnTrue, fn, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic )
{
  typedef PDEAdvectionDiffusion_ArtificialViscosity<PhysD2,
                                                    AdvectiveFlux2D_Uniform,
                                                    ViscousFlux2D_Uniform,
                                                    Source2D_None> PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> MatrixSym;
  typedef MakeTuple<ParamTuple, MatrixSym, Real>::type ParamType_GenH;

  const Real tol = 1.e-13;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  Real time = 0;

  int order = 1;
  PDEClass pde(adv, visc, source, order);

  Real x, y;
  Real dx, dy;
  Real sln;
  Real speed, speedTrue;

  x = 0; y = 0;   // not actually used in functions
  dx = 1.22; dy = -0.432;
  sln  =  3.263;

  Real hbar = 0.2;
  MatrixSym logH = {{log(hbar)},{0.0, log(hbar)}};
  ParamType_GenH param_genHL(logH, 0.2); // grid spacing and sensor values

  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = qDataPrim[0];
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  speedTrue = fabs(dx*u + dy*v)/sqrt(dx*dx + dy*dy);

  pde.speedCharacteristic( param_genHL, x, y, time, dx, dy, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  pde.speedCharacteristic( param_genHL, x, y, time, q, speed );
  BOOST_CHECK_CLOSE( sqrt(u*u + v*v), speed, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef PDEAdvectionDiffusion_ArtificialViscosity<PhysD2,
                                                    AdvectiveFlux2D_Uniform,
                                                    ViscousFlux2D_Uniform,
                                                    Source2D_None> PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  int order = 1;
  PDEClass pde(adv, visc, source, order);

  ArrayQ q = 0;
  BOOST_CHECK( pde.isValidState(q) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PDEAdvectionDiffusion_ArtificialViscosity2D_pattern.txt", true );

  typedef PDEAdvectionDiffusion_ArtificialViscosity<PhysD2,
                                                    AdvectiveFlux2D_Uniform,
                                                    ViscousFlux2D_Uniform,
                                                    Source2D_None> PDEClass;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  typedef ForcingFunction2D_Const<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(1) );

  int order = 1;
  PDEClass pde(adv, visc, source, forcingptr, order);

  pde.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
