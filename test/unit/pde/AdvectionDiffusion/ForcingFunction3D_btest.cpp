// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ForcingFunction3D_btest
//
// test of 3-D forcing functions for advection diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AdvectionDiffusion/ForcingFunction3D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ForcingFunction3D_test_suite )

class DummyPDE
{
public:
  template<class T>
  using ArrayQ = T;
  // solution/residual arrays
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( constant )
{
  const Real tol = 1.e-13;
  DummyPDE pde;

  ForcingFunction3D_Const<DummyPDE> forcing( 1 );

  // functor
  Real x = 1;
  Real y = 2;
  Real z = 3;
  Real time = 0;
  ForcingFunction3D_Const<DummyPDE>::ArrayQ<Real> src = 0;
  forcing( pde, x, y, z, time, src );
  BOOST_CHECK_CLOSE( 1, src, tol );

  forcing( pde, x, y, z, time, src );
  BOOST_CHECK_CLOSE( 2, src, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( monomial )
{
  const Real tol = 1.e-13;
  DummyPDE pde;

  Real nu = 2;
  ForcingFunction3D_Monomial<DummyPDE> forcing( 3, 4, 5, nu );

  // functor
  Real x, y, z;
  ForcingFunction3D_Monomial<DummyPDE>::ArrayQ<Real> src;

  src = 0;
  x = 1;
  y = 2;
  z = 3;
  Real time = 0;
  forcing( pde, x, y, z, time, src );
  BOOST_CHECK_CLOSE( -87264, src, tol );

  forcing( pde, x, y, z, time, src );
  BOOST_CHECK_CLOSE( 2*-87264, src, tol );

  src = 0;
  x = 2;
  y = 1;
  z = 3;
  forcing( pde, x, y, z, time, src );
  BOOST_CHECK_CLOSE( -61128, src, tol );

  forcing( pde, x, y, z, time, src );
  BOOST_CHECK_CLOSE( 2*-61128, src, tol );

  src = 0;
  x = 3;
  y = 2;
  z = 1;
  forcing( pde, x, y, z, time, src );
  BOOST_CHECK_CLOSE( -20448, src, tol );

  forcing( pde, x, y, z, time, src );
  BOOST_CHECK_CLOSE( 2*-20448, src, tol );

  src = 0;
  x = -2;
  y = 0;
  z = 1;
  forcing( pde, x, y, z, time, src );
  BOOST_CHECK_SMALL( src, tol );

  forcing( pde, x, y, z, time, src );
  BOOST_CHECK_SMALL( src, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( sinesinesine )
{
  const Real tol = 1.e-13;
  DummyPDE pde;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;

  Real kxx = 2.123, kxy = 0.553, kxz = 0.643;
  Real kyx = 0.378, kyy = 1.007, kyz = 0.765;
  Real kzx = 1.642, kzy = 1.299, kzz = 1.234;

  Real time = 0;

  ForcingFunction3D_SineSineSine<DummyPDE> forcing( u, v, w, kxx, kxy, kxz, kyx, kyy, kyz, kzx, kzy, kzz );

  // functor
  Real x = sqrt( 3 );
  Real y = sqrt( 7 );
  Real z = sqrt( 5 );
  Real snx = sin( 2 * PI * x ), csx = cos( 2 * PI * x );
  Real sny = sin( 2 * PI * y ), csy = cos( 2 * PI * y );
  Real snz = sin( 2 * PI * z ), csz = cos( 2 * PI * z );

  Real srcTrue = 2 * PI * ((u * csx * sny * snz + 2 * PI * (kxx * snx * sny * snz - kxy * csx * csy * snz - kxz * csx * sny * csz)) +
                 (v * snx * csy * snz + 2 * PI * (-kyx * csx * csy * snz + kyy * snx * sny * snz - kyz * snx * csy * csz))
                    + (w * snx * sny * csz + 2 * PI * (-kzx * csx * sny * csz - kzy * snx * csy * csz + kzz * snx * sny * snz)));

  ForcingFunction3D_SineSineSine<DummyPDE>::ArrayQ<Real> src = 0;
  forcing( pde, x, y, z, time, src );
  BOOST_CHECK_CLOSE( srcTrue, src, tol );

  forcing( pde, x, y, z, time, src );
  BOOST_CHECK_CLOSE( 2*srcTrue, src, tol );
}

//-----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Gaussian )
{
  const Real tol = 1.e-13;
  DummyPDE pde;

  Real fa, fc, fx0, fy0, fz0, fsigma;
  fa  = 1.2;
  fc  = 1.1;
  fx0 = 0.2;
  fy0 = 0.4;
  fz0 = 0.6;
  fsigma = 1.5;

  ForcingFunction3D_Gaussian<DummyPDE> forcing(fa, fc, fx0, fy0, fz0, fsigma);

  //functor
  ForcingFunction3D_Gaussian<DummyPDE>::ArrayQ<Real> src;
  Real x = 2;
  Real y = 3;
  Real z = 5;
  Real time = 0.2;

  Real srcTrue = (fa*(1/pow(sqrt(2*PI)*fsigma,1))*exp((-1/(2*fsigma*fsigma))*(pow(x-fx0,2))+pow(y-fy0,2)+pow(z-fz0,2)))+fc;

  src = 0;
  forcing(pde, x, y, z, time, src);

  BOOST_CHECK_CLOSE(srcTrue, src, tol);

  forcing(pde, x, y, z, time, src);

  BOOST_CHECK_CLOSE(2*srcTrue, src, tol);

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PDEAdvectionDiffusion3D_ForcingFunction3D_pattern.txt", true );

  ForcingFunction3D_Const<DummyPDE> forcing2( 1 );
  forcing2.dump( 2, output );

  ForcingFunction3D_Monomial<DummyPDE> forcing3( 2, 5, 4, 1 );
  forcing3.dump( 2, output );

  ForcingFunction3D_SineSineSine<DummyPDE> forcing4( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 );
  forcing4.dump( 2, output );

  ForcingFunction3D_Gaussian<DummyPDE> forcing5( 1, 2.3, 5.2, 5.3, 1.2, 0.4);
  forcing5.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
