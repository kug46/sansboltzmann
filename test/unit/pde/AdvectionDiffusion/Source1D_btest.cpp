// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Source1D_btest
// test for the 1-D source class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/AdvectionDiffusion/Source1D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Source1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( None )
{
  typedef Source1D_None::ArrayQ<Real> ArrayQ;
  typedef Source1D_None::MatrixQ<Real> MatrixQ;

  Real x = 1;
  Real time = 0;

  Source1D_None source;

  ArrayQ q = 1.45;
  ArrayQ qL = 2.45;
  ArrayQ qR = 3.45;

  ArrayQ qx = 1.56;
  ArrayQ qxL = 1.56;
  ArrayQ qxR = 0.83;

  ArrayQ sTrue = 0.0;
  ArrayQ s = 0.0;

  ArrayQ sLTrue = 0.0, sRTrue = 0.0;
  ArrayQ sL = 0.0, sR = 0.0;

  MatrixQ dsduTrue = 0.0;
  MatrixQ dsdu = 0.0;

  BOOST_CHECK( source.hasSourceTerm() == false );
  BOOST_CHECK( source.needsSolutionGradientforSource() == false );

  source.source(x, time, q, qx, s);
  BOOST_CHECK_EQUAL( sTrue, s );

  source.source(x, time, q, qx, s);
  BOOST_CHECK_EQUAL( 2*sTrue, s );

  source.sourceTrace(x, x, time, qL, qxL, qR, qxR, sL, sR);
  BOOST_CHECK_EQUAL( sLTrue, sL );
  BOOST_CHECK_EQUAL( sRTrue, sR );

  source.sourceTrace(x, x, time, qL, qxL, qR, qxR, sL, sR);
  BOOST_CHECK_EQUAL( 2*sLTrue, sL );
  BOOST_CHECK_EQUAL( 2*sRTrue, sR );

  source.jacobianSource(x, time, q, qx, dsdu);
  BOOST_CHECK_EQUAL( dsduTrue, dsdu );

  source.jacobianSource(x, time, q, qx, dsdu);
  BOOST_CHECK_EQUAL( 2*dsduTrue, dsdu );

  MatrixQ dsduxTrue = 0.0;
  MatrixQ dsdux = 0.0;

  source.jacobianGradientSource(x, time, q, qx, dsdux );
  BOOST_CHECK_EQUAL( dsduxTrue, dsdux );

  source.jacobianGradientSource(x, time, q, qx, dsdux );
  BOOST_CHECK_EQUAL( 2*dsduxTrue, dsdux );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Uniform )
{
  typedef Source1D_Uniform::ArrayQ<Real> ArrayQ;
  typedef Source1D_Uniform::MatrixQ<Real> MatrixQ;

  Real x = 1;
  Real time = 0;

  Real a = 0.35;
  Source1D_Uniform source(a);

  ArrayQ q = 1.45;
  ArrayQ qL = 2.45;
  ArrayQ qR = 3.45;

  ArrayQ qx = 1.56;
  ArrayQ qxL = 1.56;
  ArrayQ qxR = 0.83;

  ArrayQ sTrue = a*q;
  ArrayQ s = 0.0;

  ArrayQ sLTrue = a*qL, sRTrue = a*qR;
  ArrayQ sL = 0.0, sR = 0.0;

  MatrixQ dsduTrue = a;
  MatrixQ dsdu = 0.0;

  BOOST_CHECK( source.hasSourceTerm() == true );
  BOOST_CHECK( source.needsSolutionGradientforSource() == false );

  source.source(x, time, q, qx, s);
  BOOST_CHECK_EQUAL( sTrue, s );

  source.source(x, time, q, qx, s);
  BOOST_CHECK_EQUAL( 2*sTrue, s );

  source.sourceTrace(x, x, time, qL, qxL, qR, qxR, sL, sR);
  BOOST_CHECK_EQUAL( sLTrue, sL );
  BOOST_CHECK_EQUAL( sRTrue, sR );

  source.sourceTrace(x, x, time, qL, qxL, qR, qxR, sL, sR);
  BOOST_CHECK_EQUAL( 2*sLTrue, sL );
  BOOST_CHECK_EQUAL( 2*sRTrue, sR );

  source.jacobianSource(x, time, q, qx, dsdu);
  BOOST_CHECK_EQUAL( dsduTrue, dsdu );

  source.jacobianSource(x, time, q, qx, dsdu);
  BOOST_CHECK_EQUAL( 2*dsduTrue, dsdu );

  MatrixQ dsduxTrue = 0.0;
  MatrixQ dsdux = 0.0;

  source.jacobianGradientSource(x, time, q, qx, dsdux );
  BOOST_CHECK_EQUAL( dsduxTrue, dsdux );

  source.jacobianGradientSource(x, time, q, qx, dsdux );
  BOOST_CHECK_EQUAL( 2*dsduxTrue, dsdux );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( UniformGrad )
{
  typedef Source1D_UniformGrad::ArrayQ<Real> ArrayQ;
  typedef Source1D_UniformGrad::MatrixQ<Real> MatrixQ;

  Real x = 1;
  Real time = 0;

  Real a = 0.35;
  Real b = -0.84;
  Source1D_UniformGrad source(a, b);

  ArrayQ q = 1.45;
  ArrayQ qL = 2.45;
  ArrayQ qR = 3.45;

  ArrayQ qx = 1.56;
  ArrayQ qxL = 1.56;
  ArrayQ qxR = 0.83;

  ArrayQ sTrue = a*q + b*qx;
  ArrayQ s = 0.0;

  ArrayQ sLTrue = a*qL + b*qxL, sRTrue = a*qR + b*qxR;
  ArrayQ sL = 0.0, sR = 0.0;

  MatrixQ dsduTrue = a;
  MatrixQ dsdu = 0.0;

  BOOST_CHECK( source.hasSourceTerm() == true );
  BOOST_CHECK( source.needsSolutionGradientforSource() == true );

  source.source(x, time, q, qx, s);
  BOOST_CHECK_EQUAL( sTrue, s );

  source.source(x, time, q, qx, s);
  BOOST_CHECK_EQUAL( 2*sTrue, s );

  source.sourceTrace(x, x, time, qL, qxL, qR, qxR, sL, sR);
  BOOST_CHECK_EQUAL( sLTrue, sL );
  BOOST_CHECK_EQUAL( sRTrue, sR );

  source.sourceTrace(x, x, time, qL, qxL, qR, qxR, sL, sR);
  BOOST_CHECK_EQUAL( 2*sLTrue, sL );
  BOOST_CHECK_EQUAL( 2*sRTrue, sR );

  source.jacobianSource(x, time, q, qx, dsdu);
  BOOST_CHECK_EQUAL( dsduTrue, dsdu );

  source.jacobianSource(x, time, q, qx, dsdu);
  BOOST_CHECK_EQUAL( 2*dsduTrue, dsdu );

  MatrixQ dsduxTrue = b;
  MatrixQ dsdux = 0.0;

  source.jacobianGradientSource(x, time, q, qx, dsdux );
  BOOST_CHECK_EQUAL( dsduxTrue, dsdux );

  source.jacobianGradientSource(x, time, q, qx, dsdux );
  BOOST_CHECK_EQUAL( 2*dsduxTrue, dsdux );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
