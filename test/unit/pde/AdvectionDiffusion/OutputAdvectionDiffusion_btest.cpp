// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// OutputAdvectionDiffusion_btest
//
// test of weighted residual outputs for AD

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AdvectionDiffusion/OutputAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/OutputAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/OutputAdvectionDiffusion3D.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AnalyticFunction/ScalarFunction3D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( OutputAdvectionDiffusion_test_suite )
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WeightedResidual_1D )
{
  const Real tol = 1.e-13;

  Real win = 2.1;
  OutputAdvectionDiffusion1D_WeightedResidual fcn(win);
  static_assert( std::is_same<PhysD1, OutputAdvectionDiffusion1D_WeightedResidual::PhysDim>::value, "Physical dimensions should match" );

  Real x = 0, t = 0;
  Real weight = -1;

  fcn(x,t,weight);

  BOOST_CHECK_CLOSE( 2.1, weight, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FunctionWeightedResidual_1D )
{
  const Real tol = 1.e-13;

  ScalarFunction1D_Const fcn1( 1.3 );


  OutputAdvectionDiffusion1D_FunctionWeightedResidual<ScalarFunction1D_Const> fcn(fcn1);
  static_assert( std::is_same<PhysD1, OutputAdvectionDiffusion1D_FunctionWeightedResidual<ScalarFunction1D_Const>::PhysDim>::value,
                 "Physical dimensions should match" );

  Real x = 0, t = 0;
  Real weight = -1;

  fcn(x,t,weight);

  BOOST_CHECK_CLOSE( 1.3, weight, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FunctionWeightedFlux_1D )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEClass;

  Real u = 11./10;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2123./1000;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  const Real tol = 1.e-13;

  Real weight = 1.3;
  ScalarFunction1D_Const fcn1( weight );

  OutputAdvectionDiffusion1D_FunctionWeightedFlux<PDEClass,ScalarFunction1D_Const> fcn(pde,fcn1);
  static_assert( std::is_same<PhysD1, OutputAdvectionDiffusion1D_FunctionWeightedFlux<PDEClass,ScalarFunction1D_Const>::PhysDim>::value,
                 "Physical dimensions should match" );

  Real x = 0, t = 0;
  Real q = 21./10, qx = 789./100;
  Real output = -1;

  BOOST_CHECK_THROW( fcn(x,t,q,qx,output), DeveloperException );

  Real nx = -1;

  fcn(x,t,nx,q,qx,output);

  Real viscx = - kxx*qx;

  Real outputTrue = weight*(q*(nx*u) + viscx*nx);

  SANS_CHECK_CLOSE( output, outputTrue, tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WeightedFlux_1D )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEClass;

  Real u = 11./10;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2123./1000;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  const Real tol = 1.e-13;

  Real weight = 1.3;

  OutputAdvectionDiffusion1D_WeightedFlux<PDEClass> fcn(pde,weight);
  static_assert( std::is_same<PhysD1, OutputAdvectionDiffusion1D_WeightedFlux<PDEClass>::PhysDim>::value, "Physical dimensions should match" );

  Real x = 0, t = 0;
  Real q = 21./10, qx = 789./100;
  Real output = -1;

  BOOST_CHECK_THROW( fcn(x,t,q,qx,output), DeveloperException );

  Real nx = -1;

  fcn(x,t,nx,q,qx,output);

  Real viscx = - kxx*qx;

  Real outputTrue = weight*(q*(nx*u) + viscx*nx);

  SANS_CHECK_CLOSE( output, outputTrue, tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FunctionWeightedState_1D )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEClass;

  Real u = 11./10;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2123./1000;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  const Real tol = 1.e-13;

  Real weight = 1.3;
  ScalarFunction1D_Const fcn1( weight );

  OutputAdvectionDiffusion1D_FunctionWeightedState<PDEClass,ScalarFunction1D_Const> fcn(pde,fcn1);
  static_assert( std::is_same<PhysD1, OutputAdvectionDiffusion1D_FunctionWeightedState<PDEClass,ScalarFunction1D_Const>::PhysDim>::value,
                 "Physical dimensions should match" );


  Real x = 0,t = 0;
  Real q = 21./10, qx = 789./100;
  Real output = -1, outputTrue = weight*q;

  fcn(x,t,q,qx,output);

  SANS_CHECK_CLOSE( output, outputTrue, tol, tol );
  output = -1;

  Real nx = -1;

  fcn(x,t,nx,q,qx,output);

  SANS_CHECK_CLOSE( output, outputTrue, tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WeightedState_1D )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEClass;

  Real u = 11./10;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2123./1000;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  const Real tol = 1.e-13;

  Real weight = 1.3;

  OutputAdvectionDiffusion1D_WeightedState<PDEClass> fcn(pde,weight);
  static_assert( std::is_same<PhysD1, OutputAdvectionDiffusion1D_WeightedState<PDEClass>::PhysDim>::value, "Physical dimensions should match" );


  Real x = 0, t = 0;
  Real q = 21./10, qx = 789./100;
  Real output = -1, outputTrue = weight*q;

  fcn(x,t,q,qx,output);

  SANS_CHECK_CLOSE( output, outputTrue, tol, tol );
  output = -1;

  Real nx = -1;

  fcn(x,t,nx,q,qx,output);

  SANS_CHECK_CLOSE( output, outputTrue, tol, tol );
}



//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WeightedResidual_2D )
{
  const Real tol = 1.e-13;

  OutputAdvectionDiffusion2D_WeightedResidual fcn(2.1);
  static_assert( std::is_same<PhysD2, OutputAdvectionDiffusion2D_WeightedResidual::PhysDim>::value, "Physical dimensions should match" );


  Real x = 0, y = 0, t = 0;
  Real weight = -1;

  fcn(x,y,t,weight);

  BOOST_CHECK_CLOSE( 2.1, weight, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FunctionWeightedResidual_2D )
{
  const Real tol = 1.e-13;

  ScalarFunction2D_Const fcn1( 1.3 );

  OutputAdvectionDiffusion2D_FunctionWeightedResidual<ScalarFunction2D_Const> fcn(fcn1);
  static_assert( std::is_same<PhysD2, OutputAdvectionDiffusion2D_FunctionWeightedResidual<ScalarFunction2D_Const>::PhysDim>::value,
                 "Physical dimensions should match" );


  Real x = 0, y = 0, t = 0;
  Real weight = -1;

  fcn(x,y,t,weight);

  BOOST_CHECK_CLOSE( 1.3, weight, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FunctionWeightedFlux_2D )
{

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None> PDEClass;

  Real u = 11./10;
  Real v = 2./10;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2123./1000;
  Real kxy = 0553./1000;
  Real kyx = 0789./1000;
  Real kyy = 1007./1000;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  const Real tol = 1.e-13;

  Real weight = 1.3;
  ScalarFunction2D_Const fcn1( weight );

  OutputAdvectionDiffusion2D_FunctionWeightedFlux<PDEClass,ScalarFunction2D_Const> fcn(pde,fcn1);
  static_assert( std::is_same<PhysD2, OutputAdvectionDiffusion2D_FunctionWeightedFlux<PDEClass,ScalarFunction2D_Const>::PhysDim>::value,
                 "Physical dimensions should match" );


  Real x = 0, y = 0, t = 0;
  Real q = 21./10, qx = 789./100, qy = 254./100;
  Real output = -1;

  BOOST_CHECK_THROW( fcn(x,y,t,q,qx,qy,output), DeveloperException );

  Real nx = 2, ny = 1, norm = sqrt( pow(nx,2) + pow(ny,2) );

  nx /= norm; ny /= norm;

  fcn(x,y,t,nx,ny,q,qx,qy,output);

  Real viscx = - kxx*qx - kxy*qy;
  Real viscy = - kyx*qx - kyy*qy;

  Real outputTrue = weight*(q*(nx*u + ny*v) + viscx*nx + viscy*ny);

  SANS_CHECK_CLOSE( output, outputTrue, tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WeightedFlux_2D )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None> PDEClass;

  Real u = 11./10;
  Real v = 2./10;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2123./1000;
  Real kxy = 0553./1000;
  Real kyx = 0789./1000;
  Real kyy = 1007./1000;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  const Real tol = 1.e-13;

  Real weight = 1.3;

  OutputAdvectionDiffusion2D_WeightedFlux<PDEClass> fcn(pde,weight);
  static_assert( std::is_same<PhysD2, OutputAdvectionDiffusion2D_WeightedFlux<PDEClass>::PhysDim>::value, "Physical dimensions should match" );


  Real x = 0, y = 0, t = 0;
  Real q = 21./10, qx = 789./100, qy = 254./100;
  Real output = -1;

  BOOST_CHECK_THROW( fcn(x,y,t,q,qx,qy,output), DeveloperException );

  Real nx = 2, ny = 1, norm = sqrt( pow(nx,2) + pow(ny,2) );

  nx /= norm; ny /= norm;

  fcn(x,y,t,nx,ny,q,qx,qy,output);

  Real viscx = - kxx*qx - kxy*qy;
  Real viscy = - kyx*qx - kyy*qy;

  Real outputTrue = weight*(q*(nx*u + ny*v) + viscx*nx + viscy*ny);

  SANS_CHECK_CLOSE( output, outputTrue, tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FunctionWeightedState_2D )
{

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None> PDEClass;

  Real u = 11./10;
  Real v = 2./10;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2123./1000;
  Real kxy = 0553./1000;
  Real kyx = 0789./1000;
  Real kyy = 1007./1000;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  const Real tol = 1.e-13;

  Real weight = 1.3;
  ScalarFunction2D_Const fcn1( weight );

  OutputAdvectionDiffusion2D_FunctionWeightedState<PDEClass,ScalarFunction2D_Const> fcn(pde,fcn1);
  static_assert( std::is_same<PhysD2, OutputAdvectionDiffusion2D_FunctionWeightedState<PDEClass,ScalarFunction2D_Const>::PhysDim>::value,
                 "Physical dimensions should match" );


  Real x = 0, y = 0, t = 0;
  Real q = 21./10, qx = 789./100, qy = 254./100;
  Real output = -1, outputTrue = weight*q;

  fcn(x,y,t,q,qx,qy,output);

  SANS_CHECK_CLOSE( output, outputTrue, tol, tol );
  output = -1;

  Real nx = 2, ny = 1, norm = sqrt( pow(nx,2) + pow(ny,2) );
  nx /= norm; ny /= norm;

  fcn(x,y,t,nx,ny,q,qx,qy,output);

  SANS_CHECK_CLOSE( output, outputTrue, tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WeightedState_2D )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None> PDEClass;

  Real u = 11./10;
  Real v = 2./10;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2123./1000;
  Real kxy = 0553./1000;
  Real kyx = 0789./1000;
  Real kyy = 1007./1000;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  const Real tol = 1.e-13;

  Real weight = 1.3;

  OutputAdvectionDiffusion2D_WeightedState<PDEClass> fcn(pde,weight);
  static_assert( std::is_same<PhysD2, OutputAdvectionDiffusion2D_WeightedState<PDEClass>::PhysDim>::value, "Physical dimensions should match" );

  Real x = 0, y = 0, t = 0;
  Real q = 21./10, qx = 789./100, qy = 254./100;
  Real output = -1, outputTrue = weight*q;

  fcn(x,y,t,q,qx,qy,output);

  SANS_CHECK_CLOSE( output, outputTrue, tol, tol );
  output = -1;

  Real nx = 2, ny = 1, norm = sqrt( pow(nx,2) + pow(ny,2) );
  nx /= norm; ny /= norm;

  fcn(x,y,t,nx,ny,q,qx,qy,output);

  SANS_CHECK_CLOSE( output, outputTrue, tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WeightedResidual_3D )
{
  const Real tol = 1.e-13;

  OutputAdvectionDiffusion3D_WeightedResidual fcn(2.1);
  static_assert( std::is_same<PhysD3, OutputAdvectionDiffusion3D_WeightedResidual::PhysDim>::value, "Physical dimensions should match" );

  Real x = 0, y = 0, z = 0, t = 0;
  Real weight = -1;

  fcn(x,y,z,t,weight);

  BOOST_CHECK_CLOSE( 2.1, weight, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FunctionWeightedResidual_3D )
{
  const Real tol = 1.e-13;

  ScalarFunction3D_Const fcn1( 1.3 );

  OutputAdvectionDiffusion3D_FunctionWeightedResidual<ScalarFunction3D_Const> fcn(fcn1);
  static_assert( std::is_same<PhysD3, OutputAdvectionDiffusion3D_FunctionWeightedResidual<ScalarFunction3D_Const>::PhysDim>::value,
                 "Physical dimensions should match" );


  Real x = 0, y = 0, z = 0, t = 0;
  Real weight = -1;

  fcn(x,y,z,t,weight);

  BOOST_CHECK_CLOSE( 1.3, weight, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FunctionWeightedFlux_3D )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEClass;

  Real u = 11./10;
  Real v = 2./10;
  Real w = -5./10;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2123./1000, kxy = 553./1000,  kxz = 643./1000;
  Real kyx = 378./1000,  kyy = 1007./1000, kyz = 765./1000;
  Real kzx = 1642./1000, kzy = 1299./1000, kzz = 1234./1000;

  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                              kyx, kyy, kyz,
                              kzx, kzy, kzz );

  Source3D_None source;

  PDEClass pde( adv, visc, source );

  const Real tol = 1.e-12;

  Real weight = 1.3;
  ScalarFunction3D_Const fcn1( weight );

  OutputAdvectionDiffusion3D_FunctionWeightedFlux<PDEClass,ScalarFunction3D_Const> fcn(pde,fcn1);
  static_assert( std::is_same<PhysD3, OutputAdvectionDiffusion3D_FunctionWeightedFlux<PDEClass,ScalarFunction3D_Const>::PhysDim>::value,
                 "Physical dimensions should match" );


  Real x = 0, y = 0, z = 0, t = 0;
  Real q = 21./10, qx = 789./100, qy = 254./100, qz = -387./100;
  Real output = -1;

  BOOST_CHECK_THROW( fcn(x,y,z,t,q,qx,qy,qz,output), DeveloperException );

  Real nx = 2, ny = 1, nz = -3, norm = sqrt( pow(nx,2) + pow(ny,2) + pow(nz,2) );

  nx /= norm; ny /= norm; nz /= norm;

  fcn(x,y,z,t,nx,ny,nz,q,qx,qy,qz,output);

  Real viscx = - kxx*qx - kxy*qy - kxz*qz;
  Real viscy = - kyx*qx - kyy*qy - kyz*qz;
  Real viscz = - kzx*qx - kzy*qy - kzz*qz;

  Real outputTrue = weight*(q*(nx*u + ny*v + nz*w) + viscx*nx + viscy*ny + viscz*nz);

  SANS_CHECK_CLOSE( output, outputTrue, tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WeightedFlux_3D )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEClass;

  Real u = 11./10;
  Real v = 2./10;
  Real w = -5./10;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2123./1000, kxy = 553./1000,  kxz = 643./1000;
  Real kyx = 378./1000,  kyy = 1007./1000, kyz = 765./1000;
  Real kzx = 1642./1000, kzy = 1299./1000, kzz = 1234./1000;

  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                              kyx, kyy, kyz,
                              kzx, kzy, kzz );

  Source3D_None source;

  PDEClass pde( adv, visc, source );

  const Real tol = 1.e-12;

  Real weight = 1.3;

  OutputAdvectionDiffusion3D_WeightedFlux<PDEClass> fcn(pde,weight);
  static_assert( std::is_same<PhysD3, OutputAdvectionDiffusion3D_WeightedFlux<PDEClass>::PhysDim>::value, "Physical dimensions should match" );


  Real x = 0, y = 0, z = 0, t = 0;
  Real q = 21./10, qx = 789./100, qy = 254./100, qz = -387./100;
  Real output = -1;

  BOOST_CHECK_THROW( fcn(x,y,z,t,q,qx,qy,qz,output), DeveloperException );

  Real nx = 2, ny = 1, nz = -3, norm = sqrt( pow(nx,2) + pow(ny,2) + pow(nz,2) );

  nx /= norm; ny /= norm; nz /= norm;

  fcn(x,y,z,t,nx,ny,nz,q,qx,qy,qz,output);

  Real viscx = - kxx*qx - kxy*qy - kxz*qz;
  Real viscy = - kyx*qx - kyy*qy - kyz*qz;
  Real viscz = - kzx*qx - kzy*qy - kzz*qz;

  Real outputTrue = weight*(q*(nx*u + ny*v + nz*w) + viscx*nx + viscy*ny + viscz*nz);

  SANS_CHECK_CLOSE( output, outputTrue, tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FunctionWeightedState_3D )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEClass;

  Real u = 11./10;
  Real v = 2./10;
  Real w = -5./10;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2123./1000, kxy = 553./1000,  kxz = 643./1000;
  Real kyx = 378./1000,  kyy = 1007./1000, kyz = 765./1000;
  Real kzx = 1642./1000, kzy = 1299./1000, kzz = 1234./1000;

  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                              kyx, kyy, kyz,
                              kzx, kzy, kzz );

  Source3D_None source;

  PDEClass pde( adv, visc, source );

  const Real tol = 1.e-13;

  Real weight = 1.3;
  ScalarFunction3D_Const fcn1( weight );

  OutputAdvectionDiffusion3D_FunctionWeightedState<PDEClass,ScalarFunction3D_Const> fcn(pde,fcn1);
  static_assert( std::is_same<PhysD3, OutputAdvectionDiffusion3D_FunctionWeightedState<PDEClass,ScalarFunction3D_Const>::PhysDim>::value,
                 "Physical dimensions should match" );


  Real x = 0, y = 0, z = 0, t = 0;
  Real q = 21./10, qx = 789./100, qy = 254./100, qz = -387./100;
  Real output = -1, outputTrue = weight*q;;

  fcn(x,y,z,t,q,qx,qy,qz,output);

  SANS_CHECK_CLOSE( output, outputTrue, tol, tol );

  output = -1;

  Real nx = 2, ny = 1, nz = -3, norm = sqrt( pow(nx,2) + pow(ny,2) + pow(nz,2) );

  nx /= norm; ny /= norm; nz /= norm;

  fcn(x,y,z,t,nx,ny,nz,q,qx,qy,qz,output);

  SANS_CHECK_CLOSE( output, outputTrue, tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WeightedState_3D )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEClass;

  Real u = 11./10;
  Real v = 2./10;
  Real w = -5./10;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2123./1000, kxy = 553./1000,  kxz = 643./1000;
  Real kyx = 378./1000,  kyy = 1007./1000, kyz = 765./1000;
  Real kzx = 1642./1000, kzy = 1299./1000, kzz = 1234./1000;

  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                              kyx, kyy, kyz,
                              kzx, kzy, kzz );

  Source3D_None source;

  PDEClass pde( adv, visc, source );

  const Real tol = 1.e-13;

  Real weight = 1.3;

  OutputAdvectionDiffusion3D_WeightedState<PDEClass> fcn(pde,weight);
  static_assert( std::is_same<PhysD3, OutputAdvectionDiffusion3D_WeightedState<PDEClass>::PhysDim>::value, "Physical dimensions should match" );


  Real x = 0, y = 0, z = 0, t = 0;
  Real q = 21./10, qx = 789./100, qy = 254./100, qz = -387./100;
  Real output = -1, outputTrue = weight*q;;

  fcn(x,y,z,t,q,qx,qy,qz,output);

  SANS_CHECK_CLOSE( output, outputTrue, tol, tol );

  output = -1;

  Real nx = 2, ny = 1, nz = -3, norm = sqrt( pow(nx,2) + pow(ny,2) + pow(nz,2) );

  nx /= norm; ny /= norm; nz /= norm;

  fcn(x,y,z,t,nx,ny,nz,q,qx,qy,qz,output);

  SANS_CHECK_CLOSE( output, outputTrue, tol, tol );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
