// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Source2D_btest
// test for the 2-D source class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/AdvectionDiffusion/Source2D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Source2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( None )
{
  typedef Source2D_None::ArrayQ<Real> ArrayQ;
  typedef Source2D_None::MatrixQ<Real> MatrixQ;

  Real x = 1, y = 0.5;
  Real time = 0;

  Source2D_None source;

  ArrayQ q = 1.45;
  ArrayQ qL = 2.45;
  ArrayQ qR = 3.45;

  ArrayQ qx = 1.56;
  ArrayQ qy = -0.73;
  ArrayQ qxL = 1.56;
  ArrayQ qyL = 0.51;
  ArrayQ qxR = 0.83;
  ArrayQ qyR = 1.97;

  ArrayQ sTrue = 0.0;
  ArrayQ s = 0.0;

  ArrayQ sLTrue = 0.0, sRTrue = 0.0;
  ArrayQ sL = 0.0, sR = 0.0;

  MatrixQ dsduTrue = 0.0;
  MatrixQ dsdu = 0.0;

  BOOST_CHECK( source.hasSourceTerm() == false );
  BOOST_CHECK( source.needsSolutionGradientforSource() == false );

  source.source(x, y, time, q, qx, qy, s);
  BOOST_CHECK_EQUAL( sTrue, s );

  source.source(x, y, time, q, qx, qy, s);
  BOOST_CHECK_EQUAL( 2*sTrue, s );

  source.sourceTrace(x, y, x, y, time, qL, qxL, qyL, qR, qxR, qyR, sL, sR);
  BOOST_CHECK_EQUAL( sLTrue, sL );
  BOOST_CHECK_EQUAL( sRTrue, sR );

  source.sourceTrace(x, y, x, y, time, qL, qxL, qyL, qR, qxR, qyR, sL, sR);
  BOOST_CHECK_EQUAL( 2*sLTrue, sL );
  BOOST_CHECK_EQUAL( 2*sRTrue, sR );

  source.jacobianSource(x, y, time, q, qx, qy, dsdu);
  BOOST_CHECK_EQUAL( dsduTrue, dsdu );

  source.jacobianSource(x, y, time, q, qx, qy, dsdu);
  BOOST_CHECK_EQUAL( 2*dsduTrue, dsdu );

  MatrixQ dsduxTrue = 0, dsduyTrue = 0;
  MatrixQ dsdux = 0, dsduy = 0.0;

  source.jacobianGradientSource(x, y, time, q, qx, qy, dsdux, dsduy );
  BOOST_CHECK_EQUAL( dsduxTrue, dsdux );
  BOOST_CHECK_EQUAL( dsduyTrue, dsduy );

  source.jacobianGradientSource(x, y, time, q, qx, qy, dsdux, dsduy );
  BOOST_CHECK_EQUAL( 2*dsduxTrue, dsdux );
  BOOST_CHECK_EQUAL( 2*dsduyTrue, dsduy );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Uniform )
{
  typedef Source2D_Uniform::ArrayQ<Real> ArrayQ;
  typedef Source2D_Uniform::MatrixQ<Real> MatrixQ;

  Real x = 1, y = 0.5;
  Real time = 0;

  Real a = 0.35;
  Source2D_Uniform source(a);

  ArrayQ q = 1.45;
  ArrayQ qL = 2.45;
  ArrayQ qR = 3.45;

  ArrayQ qx = 1.56;
  ArrayQ qy = -0.73;
  ArrayQ qxL = 1.56;
  ArrayQ qyL = 0.51;
  ArrayQ qxR = 0.83;
  ArrayQ qyR = 1.97;

  ArrayQ sTrue = a*q;
  ArrayQ s = 0.0;

  ArrayQ sLTrue = a*qL, sRTrue = a*qR;
  ArrayQ sL = 0.0, sR = 0.0;

  MatrixQ dsduTrue = a;
  MatrixQ dsdu = 0.0;

  BOOST_CHECK( source.hasSourceTerm() == true );
  BOOST_CHECK( source.needsSolutionGradientforSource() == false );

  source.source(x, y, time, q, qx, qy, s);
  BOOST_CHECK_EQUAL( sTrue, s );

  source.source(x, y, time, q, qx, qy, s);
  BOOST_CHECK_EQUAL( 2*sTrue, s );

  source.sourceTrace(x, y, x, y, time, qL, qxL, qyL, qR, qxR, qyR, sL, sR);
  BOOST_CHECK_EQUAL( sLTrue, sL );
  BOOST_CHECK_EQUAL( sRTrue, sR );

  source.sourceTrace(x, y, x, y, time, qL, qxL, qyL, qR, qxR, qyR, sL, sR);
  BOOST_CHECK_EQUAL( 2*sLTrue, sL );
  BOOST_CHECK_EQUAL( 2*sRTrue, sR );

  source.jacobianSource(x, y, time, q, qx, qy, dsdu);
  BOOST_CHECK_EQUAL( dsduTrue, dsdu );

  source.jacobianSource(x, y, time, q, qx, qy, dsdu);
  BOOST_CHECK_EQUAL( 2*dsduTrue, dsdu );

  MatrixQ dsduxTrue = 0, dsduyTrue = 0;
  MatrixQ dsdux = 0, dsduy = 0.0;

  source.jacobianGradientSource(x, y, time, q, qx, qy, dsdux, dsduy );
  BOOST_CHECK_EQUAL( dsduxTrue, dsdux );
  BOOST_CHECK_EQUAL( dsduyTrue, dsduy );

  source.jacobianGradientSource(x, y, time, q, qx, qy, dsdux, dsduy );
  BOOST_CHECK_EQUAL( 2*dsduxTrue, dsdux );
  BOOST_CHECK_EQUAL( 2*dsduyTrue, dsduy );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( UniformGrad )
{
  typedef Source2D_UniformGrad::ArrayQ<Real> ArrayQ;
  typedef Source2D_UniformGrad::MatrixQ<Real> MatrixQ;

  Real x = 1, y = 0.5;
  Real time = 0;

  Real a = 0.35, b = 1.82, c = -2.49;
  Source2D_UniformGrad source(a, b, c);

  ArrayQ q = 1.45;
  ArrayQ qL = 2.45;
  ArrayQ qR = 3.45;

  ArrayQ qx = 1.56;
  ArrayQ qy = -0.73;
  ArrayQ qxL = 1.56;
  ArrayQ qyL = 0.51;
  ArrayQ qxR = 0.83;
  ArrayQ qyR = 1.97;

  ArrayQ sTrue = a*q + b*qx + c*qy;
  ArrayQ s = 0.0;

  ArrayQ sLTrue = a*qL + b*qxL + c*qyL;
  ArrayQ sRTrue = a*qR + b*qxR + c*qyR;
  ArrayQ sL = 0.0, sR = 0.0;

  MatrixQ dsduTrue = a;
  MatrixQ dsdu = 0.0;

  BOOST_CHECK( source.hasSourceTerm() == true );
  BOOST_CHECK( source.needsSolutionGradientforSource() == true );

  source.source(x, y, time, q, qx, qy, s);
  BOOST_CHECK_EQUAL( sTrue, s );

  source.source(x, y, time, q, qx, qy, s);
  BOOST_CHECK_EQUAL( 2*sTrue, s );

  source.sourceTrace(x, y, x, y, time, qL, qxL, qyL, qR, qxR, qyR, sL, sR);
  BOOST_CHECK_EQUAL( sLTrue, sL );
  BOOST_CHECK_EQUAL( sRTrue, sR );

  source.sourceTrace(x, y, x, y, time, qL, qxL, qyL, qR, qxR, qyR, sL, sR);
  BOOST_CHECK_EQUAL( 2*sLTrue, sL );
  BOOST_CHECK_EQUAL( 2*sRTrue, sR );

  source.jacobianSource(x, y, time, q, qx, qy, dsdu);
  BOOST_CHECK_EQUAL( dsduTrue, dsdu );

  source.jacobianSource(x, y, time, q, qx, qy, dsdu);
  BOOST_CHECK_EQUAL( 2*dsduTrue, dsdu );

  MatrixQ dsduxTrue = b, dsduyTrue = c;
  MatrixQ dsdux = 0, dsduy = 0.0;

  source.jacobianGradientSource(x, y, time, q, qx, qy, dsdux, dsduy );
  BOOST_CHECK_EQUAL( dsduxTrue, dsdux );
  BOOST_CHECK_EQUAL( dsduyTrue, dsduy );

  source.jacobianGradientSource(x, y, time, q, qx, qy, dsdux, dsduy );
  BOOST_CHECK_EQUAL( 2*dsduxTrue, dsdux );
  BOOST_CHECK_EQUAL( 2*dsduyTrue, dsduy );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
