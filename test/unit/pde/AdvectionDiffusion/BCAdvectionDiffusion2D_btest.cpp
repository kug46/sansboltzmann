// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCAdvectionDiffusion2D_btest
//
// test of 2-D Advection-Diffusion BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/BCParameters.h"

using namespace std;
using namespace SANS;


typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_None> PDEAdvectionDiffusion2D;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class BCAdvectionDiffusion< PhysD2, BCTypeFunction_mitState<AdvectiveFlux2D_Uniform,ViscousFlux2D_Uniform> >;


// Instantiate BCParamaters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters<BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform,ViscousFlux2D_Uniform>>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( BCAdvectionDiffusion2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  {
  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_mitLG> BCClass;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 1 );
  BOOST_CHECK( BCClass::NBC == 1 );
  }

  {
  typedef BCAdvectionDiffusion< PhysD2,BCTypeFlux<AdvectiveFlux2D_Uniform,ViscousFlux2D_Uniform> > BCClass;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 1 );
  BOOST_CHECK( BCClass::NBC == 1 );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeLinearRobin_mitLG_test )
{
  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_mitLG> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;
  Real time = 0;

  Real A = 1;
  Real B = 0.2;
  Real bcdata = 3;

  BCClass bc( A, B, bcdata );

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  MatrixQ AMtx;
  MatrixQ BMtx;
  ArrayQ bcdataVec;

  Real x = 0;
  Real y = 0;
  Real nx = 0.8;
  Real ny = 0.6;

  AMtx = 0;
  BMtx = 0;
  bc.coefficients( x, y, time, nx, ny, AMtx, BMtx );
  BOOST_CHECK_CLOSE( A, AMtx, tol );
  BOOST_CHECK_CLOSE( B, BMtx, tol );

  bcdataVec = 0;
  bc.data( x, y, time, nx, ny, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeDirichlet_mitLG_test )
{
  typedef BCAdvectionDiffusion<PhysD2,BCTypeDirichlet_mitLG> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  typedef PDEAdvectionDiffusion2D PDEClass;

  const Real tol = 1.e-13;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde(adv, visc, source);

  Real uB = 3;

  BCClass bc( pde, uB );
  BCClass bc2( pde, uB, true );

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  BOOST_CHECK( bc.isStrongBC() == false );
  BOOST_CHECK( bc2.isStrongBC() == true );

  ArrayQ qI = 2, qIx, qIy;
  ArrayQ lg = 4, lgterm, residualBC;

  Real x = 0;
  Real y = 0;
  Real time = 0;

  Real nx = 0.8;
  Real ny = 0.6;

  bc.lagrangeTerm( x, y, time, nx, ny, qI, qIx, qIy, lg, lgterm );
  BOOST_CHECK_CLOSE( lg, lgterm, tol );

  bc.strongBC( x, y, time, nx, ny, qI, qIx, qIy, lg, residualBC );
  BOOST_CHECK_CLOSE( qI - uB, residualBC, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFlux_Viscous_test )
{
  typedef BCAdvectionDiffusion< PhysD2, BCTypeFlux<AdvectiveFlux2D_Uniform,ViscousFlux2D_Uniform> > BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  typedef PDEAdvectionDiffusion2D PDEClass;

  const Real tol = 1.e-13;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  Real time = 0;

  PDEClass pde(adv, visc, source);

  Real bcdata = 3;

  Real x = 0;
  Real y = 0;
  Real nx = 0.8;
  Real ny = 0.6;

  ArrayQ qI = 3, qIx = 0, qIy = 0, qB;

  {
    // inflow
    bool inflow = true;
    BCClass bc( adv, visc, true, bcdata, inflow );

    bc.state(x,y,time,nx,ny,qI,qB);

    // function tests
    Real Fn = 0;
    bc.fluxNormal(x,y,time,nx,ny,qI,qIx,qIy,qB,Fn);

    SANS_CHECK_CLOSE( Fn, bcdata, tol, tol );

    bc.fluxNormal(x,y,time,nx,ny,qI,qIx,qIy,qB,Fn);

    SANS_CHECK_CLOSE( Fn, 2*bcdata, tol, tol );
  }

  {
    // outflow
    bool inflow = false;
    BCClass bc( pde, bcdata, inflow ); // different constructor

    bc.state(x,y,time,nx,ny,qI,qB);

    // function tests
    Real Fn = 0;
    bc.fluxNormal(x,y,time,nx,ny,qI,qIx,qIy,qB,Fn);

    SANS_CHECK_CLOSE( Fn, bcdata + qI*(u*nx + v*ny), tol, tol );

    bc.fluxNormal(x,y,time,nx,ny,qI,qIx,qIy,qB,Fn);

    SANS_CHECK_CLOSE( Fn, 2*(bcdata + qI*(u*nx + v*ny)), tol, tol );
  }


}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFlux_Inviscid_test )
{
  typedef BCAdvectionDiffusion< PhysD2, BCTypeFlux<AdvectiveFlux2D_Uniform,ViscousFlux2D_None> > BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  ViscousFlux2D_None visc;

  Source2D_None source;

  Real time = 0;

  PDEAdvectionDiffusion<PhysD2,
                        AdvectiveFlux2D_Uniform,
                        ViscousFlux2D_None,
                        Source2D_None> pde(adv, visc, source);

  Real bcdata = 3;

  Real x = 0;
  Real y = 0;
  Real nx = 0.8;
  Real ny = 0.6;

  ArrayQ qI = 3, qIx = 0, qIy = 0, qB;

  {
    // inflow
    bool inflow = true;
    BCClass bc( adv, visc, true, bcdata, inflow );

    bc.state(x,y,time,nx,ny,qI,qB);

    // function tests
    Real Fn = 0;
    bc.fluxNormal(x,y,time,nx,ny,qI,qIx,qIy,qB,Fn);

    SANS_CHECK_CLOSE( Fn, bcdata, tol, tol );

    bc.fluxNormal(x,y,time,nx,ny,qI,qIx,qIy,qB,Fn);

    SANS_CHECK_CLOSE( Fn, 2*bcdata, tol, tol );
  }

  {
    // outflow
    bool inflow = false;
    BCClass bc( pde, bcdata, inflow ); // different constructor

    bc.state(x,y,time,nx,ny,qI,qB);

    // function tests
    Real Fn = 0;
    bc.fluxNormal(x,y,time,nx,ny,qI,qIx,qIy,qB,Fn);

    SANS_CHECK_CLOSE( Fn, qI*(u*nx + v*ny), tol, tol );

    bc.fluxNormal(x,y,time,nx,ny,qI,qIx,qIy,qB,Fn);

    SANS_CHECK_CLOSE( Fn, 2*qI*(u*nx + v*ny), tol, tol );
  }


}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFunction_test )
{
  typedef BCAdvectionDiffusion< PhysD2, BCTypeFunctionLinearRobin_mitLG > BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  typedef ScalarFunction2D_SineSine SolutionType;
  std::shared_ptr<SolutionType> slnExact( new SolutionType );

  Real kxx = 1.003;
  Real kxy = 0.572;
  Real kyx = 0.478;
  Real kyy = 1.452;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  {
    BCClass bc( slnExact, visc );

    Real time = 0;

    // static tests
    BOOST_CHECK( bc.D == 2 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 1 );

    // function tests

    MatrixQ AMtx;
    MatrixQ BMtx;
    ArrayQ bcdataVec;

    Real x = 0.75;
    Real y = 0.25;
    Real nx = 0.8;
    Real ny = 0.6;

    AMtx = 0;
    BMtx = 0;
    bc.coefficients( x, y, time, nx, ny, AMtx, BMtx );
    BOOST_CHECK_CLOSE( 1, AMtx, tol );
    BOOST_CHECK_CLOSE( 0, BMtx, tol );

    bcdataVec = 0;
    bc.data( x, y, time, nx, ny, bcdataVec );
    BOOST_CHECK_CLOSE( -1, bcdataVec, tol );
  }

  {
    Real A = 0.35;
    Real B = 0.84;
    BCClass bc( slnExact, visc, A, B );

    Real time = 0;

    // static tests
    BOOST_CHECK( bc.D == 2 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 1 );

    // function tests

    MatrixQ AMtx;
    MatrixQ BMtx;
    ArrayQ bcdataVec;

    Real x = 0.75;
    Real y = 0.25;
    Real nx = 0.8;
    Real ny = 0.6;

    AMtx = 0;
    BMtx = 0;
    bc.coefficients( x, y, time, nx, ny, AMtx, BMtx );
    BOOST_CHECK_CLOSE( A, AMtx, tol );
    BOOST_CHECK_CLOSE( B, BMtx, tol );

    Real u = sin(2*PI*x)*sin(2*PI*y);
    Real ux = 2*PI*cos(2*PI*x)*sin(2*PI*y);
    Real uy = 2*PI*sin(2*PI*x)*cos(2*PI*y);
    Real bcdataTrue = A*u + B*((kxx*ux + kxy*uy)*nx + (kyx*ux + kyy*uy)*ny);

    bcdataVec = 0;
    bc.data( x, y, time, nx, ny, bcdataVec );
    BOOST_CHECK_CLOSE( bcdataTrue, bcdataVec, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeTimeOut_test )
{
  typedef BCAdvectionDiffusion< PhysD2, BCTypeTimeOut<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform>> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  typedef ScalarFunction2D_SineSineSineUnsteady SolutionType;
  std::shared_ptr<SolutionType> slnExact( new SolutionType );

  Real ax= 0.947;
  Real ay= 0.436;
  AdvectiveFlux2D_Uniform adv(ax, ay);

  Real kxx = 1.003;
  Real kxy = 0.572;
  Real kyx = 0.478;
  Real kyy = 1.452;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEAdvectionDiffusion<PhysD2, AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform, Source2D_None> pde(adv, visc, source);

  {
    PyDict d;
    BCClass bc(pde, d);

    // static tests

    BOOST_CHECK( bc.D == 2 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 0 );

  }

  {
    PyDict d;
    BCClass bc(pde, d);

    // running tests

    Real nx= 0.8;
    Real ny= 0.6;
    Real nt= 0.5;

    Real x= 1.2;
    Real y= 2.8;
    Real time= 2.0;

    ArrayQ qI= 1.7;
    ArrayQ qIx= 3.4;
    ArrayQ qIy= -2.2;
    ArrayQ qIt= 0.2;

    ArrayQ qB;

    BOOST_CHECK(bc.hasFluxViscous() == false);

    bc.state(x, y, time, nx, ny, nt, qI, qB);
    SANS_CHECK_CLOSE(qI, qB, tol, tol);

    Real Fn= 0;
    bc.fluxNormalSpaceTime(x, y, time, nx, ny, nt, qI, qIx, qIy, qIt, qB, Fn);
    SANS_CHECK_CLOSE(Fn, 0.5*(nt + nx*ax + ny*ay)*(qB + qI)
                     - 0.5*fabs(nt + nx*ax + ny*ay)*(qB - qI), tol, tol);
    bc.fluxNormalSpaceTime(x, y, time, nx, ny, nt, qI, qIx, qIy, qIt, qB, Fn);
    SANS_CHECK_CLOSE(Fn, (nt + nx*ax + ny*ay)*(qB + qI)
                     - fabs(nt + nx*ax + ny*ay)*(qB - qI), tol, tol);

    BOOST_CHECK(bc.isValidState(nx, ny, nt, qI) == true);
  }

  {
    PyDict d;
    BCClass bc(pde, d);

    // running tests

    Real nx= 0.8;
    Real ny= 0.6;
    Real nt= -0.2;

    Real x= 1.2;
    Real y= 2.8;
    Real time= 2.0;

    ArrayQ qI= 1.7;
    ArrayQ qIx= 3.4;
    ArrayQ qIy= -2.2;
    ArrayQ qIt= 0.2;

    ArrayQ qB;

    BOOST_CHECK(bc.hasFluxViscous() == false);

    bc.state(x, y, time, nx, ny, nt, qI, qB);
    SANS_CHECK_CLOSE(qI, qB, tol, tol);

    Real Fn= 0;
    bc.fluxNormalSpaceTime(x, y, time, nx, ny, nt, qI, qIx, qIy, qIt, qB, Fn);
    SANS_CHECK_CLOSE(Fn, 0.5*(nt + nx*ax + ny*ay)*(qB + qI)
                     - 0.5*fabs(nt + nx*ax + ny*ay)*(qB - qI), tol, tol);
    bc.fluxNormalSpaceTime(x, y, time, nx, ny, nt, qI, qIx, qIy, qIt, qB, Fn);
    SANS_CHECK_CLOSE(Fn, (nt + nx*ax + ny*ay)*(qB + qI)
                     - fabs(nt + nx*ax + ny*ay)*(qB - qI), tol, tol);

    BOOST_CHECK(bc.isValidState(nx, ny, nt, qI) == true);
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeTimeIC_test )
{
  typedef BCAdvectionDiffusion< PhysD2, BCTypeTimeIC<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform>> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  typedef ScalarFunction2D_SineSineSineUnsteady SolutionType;
  std::shared_ptr<SolutionType> slnExact( new SolutionType );

  Real ax= 0.947;
  Real ay= 0.436;
  AdvectiveFlux2D_Uniform adv(ax, ay);

  Real kxx = 1.003;
  Real kxy = 0.572;
  Real kyx = 0.478;
  Real kyy = 1.452;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEAdvectionDiffusion<PhysD2, AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform, Source2D_None> pde(adv, visc, source);

  {

    ArrayQ const bcdata= 3.14;
    BCClass bc(pde, bcdata);

    // static tests
    BOOST_CHECK(bc.D == 2);
    BOOST_CHECK(bc.N == 1);
    BOOST_CHECK(bc.NBC == 1);

    // running tests

    Real nx= 0.8;
    Real ny= 0.6;
    Real nt= -0.2;

    Real x= 1.2;
    Real y= 2.8;
    Real time= 2.0;

    ArrayQ qI= 1.7;
    ArrayQ qIx= 3.4;
    ArrayQ qIy= -2.2;
    ArrayQ qIt= 0.2;

    ArrayQ qB;

    BOOST_CHECK(bc.hasFluxViscous() == false);

    bc.state(x, y, time, nx, ny, nt, qI, qB);
    SANS_CHECK_CLOSE(bcdata, qB, tol, tol);

    Real Fn= 0;
    bc.fluxNormalSpaceTime(x, y, time, nx, ny, nt, qI, qIx, qIy, qIt, qB, Fn);
    SANS_CHECK_CLOSE(Fn, 0.5*(nt + nx*ax + ny*ay)*(qB + qI)
                     - 0.5*fabs(nt + nx*ax + ny*ay)*(qB - qI), tol, tol);
    bc.fluxNormalSpaceTime(x, y, time, nx, ny, nt, qI, qIx, qIy, qIt, qB, Fn);
    SANS_CHECK_CLOSE(Fn, (nt + nx*ax + ny*ay)*(qB + qI)
                     - fabs(nt + nx*ax + ny*ay)*(qB - qI), tol, tol);

    BOOST_CHECK(bc.isValidState(nx, ny, nt, qI) == true);
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeTimeIC_Function_test )
{
  typedef BCAdvectionDiffusion< PhysD2, BCTypeTimeIC_Function<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform>> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  typedef ScalarFunction2D_SineSineSineUnsteady SolutionType;
  std::shared_ptr<SolutionType> slnExact( new SolutionType );

  Real ax= 0.947;
  Real ay= 0.436;
  AdvectiveFlux2D_Uniform adv(ax, ay);

  Real kxx = 1.003;
  Real kxy = 0.572;
  Real kyx = 0.478;
  Real kyy = 1.452;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEAdvectionDiffusion<PhysD2, AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform, Source2D_None> pde(adv, visc, source);

  {

    BCClass bc(pde, slnExact);

    // static tests
    BOOST_CHECK(bc.D == 2);
    BOOST_CHECK(bc.N == 1);
    BOOST_CHECK(bc.NBC == 1);

    // running tests

    Real nx= 0.8;
    Real ny= 0.6;
    Real nt= -0.2;

    Real x= 1.2;
    Real y= 2.8;
    Real time= 2.0;

    ArrayQ qI= 1.7;
    ArrayQ qIx= 3.4;
    ArrayQ qIy= -2.2;
    ArrayQ qIt= 0.2;

    ArrayQ qB;

    BOOST_CHECK(bc.hasFluxViscous() == false);

    bc.state(x, y, time, nx, ny, nt, qI, qB);
    SANS_CHECK_CLOSE((*slnExact)(x, y, time), qB, tol, tol);

    Real Fn= 0;
    bc.fluxNormalSpaceTime(x, y, time, nx, ny, nt, qI, qIx, qIy, qIt, qB, Fn);
    SANS_CHECK_CLOSE(Fn, 0.5*(nt + nx*ax + ny*ay)*(qB + qI)
                     - 0.5*fabs(nt + nx*ax + ny*ay)*(qB - qI), tol, tol);
    bc.fluxNormalSpaceTime(x, y, time, nx, ny, nt, qI, qIx, qIy, qIt, qB, Fn);
    SANS_CHECK_CLOSE(Fn, (nt + nx*ax + ny*ay)*(qB + qI)
                     - fabs(nt + nx*ax + ny*ay)*(qB - qI), tol, tol);

    BOOST_CHECK(bc.isValidState(nx, ny, nt, qI) == true);
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCParameters_test )
{
  typedef BCParameters<BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform,ViscousFlux2D_Uniform>> BCParams;

  BOOST_CHECK_EQUAL( "None"              , BCParams::params.BC.None );
  BOOST_CHECK_EQUAL( "LinearRobin_mitLG" , BCParams::params.BC.LinearRobin_mitLG );
  BOOST_CHECK_EQUAL( "LinearRobin_sansLG", BCParams::params.BC.LinearRobin_sansLG );
  BOOST_CHECK_EQUAL( "Function_mitState" , BCParams::params.BC.Function_mitState);
  BOOST_CHECK_EQUAL( "FunctionLinearRobin_mitLG"    , BCParams::params.BC.FunctionLinearRobin_mitLG);
  BOOST_CHECK_EQUAL( "FunctionLinearRobin_sansLG"   , BCParams::params.BC.FunctionLinearRobin_sansLG );
  BOOST_CHECK_EQUAL( "TimeOut", BCParams::params.BC.TimeOut );
  BOOST_CHECK_EQUAL( "TimeIC", BCParams::params.BC.TimeIC );
  BOOST_CHECK_EQUAL( "TimeIC_Function", BCParams::params.BC.TimeIC_Function );

  PyDict BC0;
  BC0[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BC3;
  BC3[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BC3[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_mitLG>::params.A] = 2;
  BC3[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_mitLG>::params.B] = 3;
  BC3[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_mitLG>::params.bcdata] = 4;

  PyDict BCList;
  BCList["BC0"] = BC0;
  BCList["BC3"] = BC3;

  //No exceptions should be thrown
  BCParams::checkInputs(BCList);

  // Create a pde for the BC constructors
  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEAdvectionDiffusion2D pde(adv, visc, source);

  // Create all the BC's
  std::map< std::string, std::shared_ptr<BCBase> > BCs = BCParams::createBCs<BCNDConvertSpace>(pde, BCList);

  BOOST_CHECK_EQUAL( 2, BCs.size() );

  // The python dictionary does not preserve order, so the count of each BC should be 1
  int NoneCount = 0;
  int LinearRobinCount = 0;

  // Extract the keys from the dictionary
  std::vector<std::string> keys = BCList.stringKeys();

  for ( std::size_t i = 0; i < keys.size(); i++ )
  {
    if ( BCs[keys[i]]->derivedTypeID() == typeid(BCNDConvertSpace<PhysD2,BCNone<PhysD2,1>>) )
      NoneCount++;
    else if ( BCs[keys[i]]->derivedTypeID() == typeid(BCNDConvertSpace<PhysD2,BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_mitLG>>) )
      LinearRobinCount++;
    else
      BOOST_CHECK( false ); // Failure..
  }

  BOOST_CHECK_EQUAL( 1, NoneCount );
  BOOST_CHECK_EQUAL( 1, LinearRobinCount );

  std::map< std::string, std::vector<int> > BoundaryGroups;

  BoundaryGroups["BC0"] = {0};
  BoundaryGroups["BC3"] = {4};

  std::vector<int> boundaryGroupsLG = BCParams::getLGBoundaryGroups(BCList, BoundaryGroups);

  BOOST_REQUIRE_EQUAL( 1, boundaryGroupsLG.size() );
  BOOST_CHECK_EQUAL( 4, boundaryGroupsLG[0] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/BCAdvectionDiffusion2D_pattern.txt", true );

  {
  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_mitLG> BCClass;

  Real A = 1;
  Real B = 0.2;
  Real bcdata = 3;

  BCClass bc1( A, B, bcdata );
  bc1.dump( 2, output );
  }

  {
  typedef BCAdvectionDiffusion< PhysD2, BCTypeFlux<AdvectiveFlux2D_Uniform,
            ViscousFlux2D_Uniform> > BCClass;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEAdvectionDiffusion2D pde( adv, visc, source );

  Real bcdata = 3;

  BCClass bc2( pde, bcdata, true );
  bc2.dump( 2, output );
  }


  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
