// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Source3D_btest
// test for the 3-D source class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/AdvectionDiffusion/Source3D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Source3D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( None )
{
  typedef Source3D_None::ArrayQ<Real> ArrayQ;
  typedef Source3D_None::MatrixQ<Real> MatrixQ;

  Real x = 1.0, y = 0.5, z = 1.5;
  Real time = 0.0;

  Source3D_None source;

  ArrayQ q = 1.45;
  ArrayQ qL = 2.45;
  ArrayQ qR = 3.45;

  ArrayQ qx = 1.56;
  ArrayQ qy =-0.73;
  ArrayQ qz = 0.28;
  ArrayQ qxL = 1.56;
  ArrayQ qyL = 0.51;
  ArrayQ qzL = 2.85;
  ArrayQ qxR = 0.83;
  ArrayQ qyR = 1.97;
  ArrayQ qzR =-0.63;

  ArrayQ sTrue = 0.0;
  ArrayQ s = 0.0;

  ArrayQ sLTrue = 0.0, sRTrue = 0.0;
  ArrayQ sL = 0.0, sR = 0.0;

  MatrixQ dsduTrue = 0.0;
  MatrixQ dsdu = 0.0;

  BOOST_CHECK( source.hasSourceTerm() == false );
  BOOST_CHECK( source.needsSolutionGradientforSource() == false );

  source.source(x, y, z, time, q, qx, qy, qz, s);
  BOOST_CHECK_EQUAL( sTrue, s );

  source.source(x, y, z, time, q, qx, qy, qz, s);
  BOOST_CHECK_EQUAL( 2*sTrue, s );

  source.sourceTrace(x, y, z, x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, sL, sR);
  BOOST_CHECK_EQUAL( sLTrue, sL );
  BOOST_CHECK_EQUAL( sRTrue, sR );

  source.sourceTrace(x, y, z, x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, sL, sR);
  BOOST_CHECK_EQUAL( 2*sLTrue, sL );
  BOOST_CHECK_EQUAL( 2*sRTrue, sR );

  source.jacobianSource(x, y, z, time, q, qx, qy, qz, dsdu);
  BOOST_CHECK_EQUAL( dsduTrue, dsdu );

  source.jacobianSource(x, y, z, time, q, qx, qy, qz, dsdu);
  BOOST_CHECK_EQUAL( 2*dsduTrue, dsdu );

  MatrixQ dsduxTrue = 0, dsduyTrue = 0, dsduzTrue = 0;
  MatrixQ dsdux = 0, dsduy = 0, dsduz = 0;

  source.jacobianGradientSource(x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz );
  BOOST_CHECK_EQUAL( dsduxTrue, dsdux );
  BOOST_CHECK_EQUAL( dsduyTrue, dsduy );
  BOOST_CHECK_EQUAL( dsduzTrue, dsduz );

  source.jacobianGradientSource(x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz );
  BOOST_CHECK_EQUAL( 2*dsduxTrue, dsdux );
  BOOST_CHECK_EQUAL( 2*dsduyTrue, dsduy );
  BOOST_CHECK_EQUAL( 2*dsduzTrue, dsduz );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Uniform )
{
  typedef Source3D_Uniform::ArrayQ<Real> ArrayQ;
  typedef Source3D_Uniform::MatrixQ<Real> MatrixQ;

  Real x = 1.0, y = 0.5, z = 1.5;
  Real time = 0.0;

  Real a = 0.35;
  Source3D_Uniform source(a);

  ArrayQ q = 1.45;
  ArrayQ qL = 2.45;
  ArrayQ qR = 3.45;

  ArrayQ qx = 1.56;
  ArrayQ qy =-0.73;
  ArrayQ qz = 0.28;
  ArrayQ qxL = 1.56;
  ArrayQ qyL = 0.51;
  ArrayQ qzL = 2.85;
  ArrayQ qxR = 0.83;
  ArrayQ qyR = 1.97;
  ArrayQ qzR =-0.63;

  ArrayQ sTrue = a*q;
  ArrayQ s = 0.0;

  ArrayQ sLTrue = a*qL;
  ArrayQ sRTrue = a*qR;
  ArrayQ sL = 0.0, sR = 0.0;

  MatrixQ dsduTrue = a;
  MatrixQ dsdu = 0.0;

  BOOST_CHECK( source.hasSourceTerm() == true );
  BOOST_CHECK( source.needsSolutionGradientforSource() == false );

  source.source(x, y, z, time, q, qx, qy, qz, s);
  BOOST_CHECK_EQUAL( sTrue, s );

  source.source(x, y, z, time, q, qx, qy, qz, s);
  BOOST_CHECK_EQUAL( 2*sTrue, s );

  source.sourceTrace(x, y, z, x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, sL, sR);
  BOOST_CHECK_EQUAL( sLTrue, sL );
  BOOST_CHECK_EQUAL( sRTrue, sR );

  source.sourceTrace(x, y, z, x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, sL, sR);
  BOOST_CHECK_EQUAL( 2*sLTrue, sL );
  BOOST_CHECK_EQUAL( 2*sRTrue, sR );

  source.jacobianSource(x, y, z, time, q, qx, qy, qz, dsdu);
  BOOST_CHECK_EQUAL( dsduTrue, dsdu );

  source.jacobianSource(x, y, z, time, q, qx, qy, qz, dsdu);
  BOOST_CHECK_EQUAL( 2*dsduTrue, dsdu );

  MatrixQ dsduxTrue = 0, dsduyTrue = 0, dsduzTrue = 0;
  MatrixQ dsdux = 0, dsduy = 0, dsduz = 0;

  source.jacobianGradientSource(x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz );
  BOOST_CHECK_EQUAL( dsduxTrue, dsdux );
  BOOST_CHECK_EQUAL( dsduyTrue, dsduy );
  BOOST_CHECK_EQUAL( dsduzTrue, dsduz );

  source.jacobianGradientSource(x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz );
  BOOST_CHECK_EQUAL( 2*dsduxTrue, dsdux );
  BOOST_CHECK_EQUAL( 2*dsduyTrue, dsduy );
  BOOST_CHECK_EQUAL( 2*dsduzTrue, dsduz );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( UniformGrad )
{
  typedef Source3D_UniformGrad::ArrayQ<Real> ArrayQ;
  typedef Source3D_UniformGrad::MatrixQ<Real> MatrixQ;

  Real x = 1.0, y = 0.5, z = 1.5;
  Real time = 0.0;

  Real a = 0.35, b = 1.82, c = -0.57, d = 2.04;
  Source3D_UniformGrad source(a, b, c, d);

  ArrayQ q = 1.45;
  ArrayQ qL = 2.45;
  ArrayQ qR = 3.45;

  ArrayQ qx = 1.56;
  ArrayQ qy =-0.73;
  ArrayQ qz = 0.28;
  ArrayQ qxL = 1.56;
  ArrayQ qyL = 0.51;
  ArrayQ qzL = 2.85;
  ArrayQ qxR = 0.83;
  ArrayQ qyR = 1.97;
  ArrayQ qzR =-0.63;

  ArrayQ sTrue = a*q + b*qx + c*qy + d*qz;
  ArrayQ s = 0.0;

  ArrayQ sLTrue = a*qL + b*qxL + c*qyL + d*qzL;
  ArrayQ sRTrue = a*qR + b*qxR + c*qyR + d*qzR;
  ArrayQ sL = 0.0, sR = 0.0;

  MatrixQ dsduTrue = a;
  MatrixQ dsdu = 0.0;

  BOOST_CHECK( source.hasSourceTerm() == true );
  BOOST_CHECK( source.needsSolutionGradientforSource() == true );

  source.source(x, y, z, time, q, qx, qy, qz, s);
  BOOST_CHECK_EQUAL( sTrue, s );

  source.source(x, y, z, time, q, qx, qy, qz, s);
  BOOST_CHECK_EQUAL( 2*sTrue, s );

  source.sourceTrace(x, y, z, x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, sL, sR);
  BOOST_CHECK_EQUAL( sLTrue, sL );
  BOOST_CHECK_EQUAL( sRTrue, sR );

  source.sourceTrace(x, y, z, x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, sL, sR);
  BOOST_CHECK_EQUAL( 2*sLTrue, sL );
  SANS_CHECK_CLOSE( 2*sRTrue, sR, 1e-12, 1e-12 );

  source.jacobianSource(x, y, z, time, q, qx, qy, qz, dsdu);
  BOOST_CHECK_EQUAL( dsduTrue, dsdu );

  source.jacobianSource(x, y, z, time, q, qx, qy, qz, dsdu);
  BOOST_CHECK_EQUAL( 2*dsduTrue, dsdu );

  MatrixQ dsduxTrue = b, dsduyTrue = c, dsduzTrue = d;
  MatrixQ dsdux = 0, dsduy = 0, dsduz = 0;

  source.jacobianGradientSource(x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz );
  BOOST_CHECK_EQUAL( dsduxTrue, dsdux );
  BOOST_CHECK_EQUAL( dsduyTrue, dsduy );
  BOOST_CHECK_EQUAL( dsduzTrue, dsduz );

  source.jacobianGradientSource(x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz );
  BOOST_CHECK_EQUAL( 2*dsduxTrue, dsdux );
  BOOST_CHECK_EQUAL( 2*dsduyTrue, dsduy );
  BOOST_CHECK_EQUAL( 2*dsduzTrue, dsduz );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
