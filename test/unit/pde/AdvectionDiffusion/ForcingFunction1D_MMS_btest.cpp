// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ForcingFunction1D_MMS_btest
//
// test of 1-D MMS forcing functions for advection diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/ForcingFunction1D_MMS.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ForcingFunction1D_MMS_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( sine )
{
  const Real tol = 1.e-13;

  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;

  ScalarFunction1D_Sine soln;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;

  ViscousFlux1D_Uniform visc( kxx );

  typedef ForcingFunction1D_MMS<PDEClass> ForcingClass;
  std::shared_ptr<ForcingClass> forcingptr( new ForcingClass(soln) );
  Source1D_None source;

  PDEClass pde(adv, visc, source, forcingptr);

  // static tests
  BOOST_CHECK( (*forcingptr).D == 1 );

  // functor
  Real x = sqrt(3);
  Real snx = sin(2*PI*x), csx = cos(2*PI*x);

  Real srcTrue = 2*PI*(u*csx + 2*PI*kxx*snx);

  Real time = 0;
  ArrayQ src = 0;
  pde.forcingFunction(x, time, src);
  BOOST_CHECK_CLOSE( srcTrue, src, tol );

  pde.forcingFunction(x, time, src);
  BOOST_CHECK_CLOSE( 2*srcTrue, src, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Exp_1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEAdvectionDiffusion1D;
  typedef PDEAdvectionDiffusion1D PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  ScalarFunction1D_Exp1 fcn;

  Real x[5] = { 0, 0.25, 0.5, 0.75, 1 };
  Real t = 0;
  // taken from matlab symbolic
  // Generated using MMS_generator_1D.m
  Real ext[5] = { 0, 0.018901785862613, 0.145029690008375, 0.625941181666856, 0};
  ArrayQ sln;

  const Real close_tol = 1e-9;
  const Real small_tol = 1e-12;
  for (int i=0;i<5;i++)
  {
    sln = fcn(x[i], t);
    SANS_CHECK_CLOSE( ext[i], sln, small_tol, close_tol );
  }

  // taken from matlab symbolic
  // Generated using MMS_generator_1D.m

  Real fext[5] =  {-0.002102168235265e+2, -0.014302351302711e+2, -0.059462172903434e+2, -0.006259411816669e+2, 3.073743188516077e+2};

  // PDE
  Real u = 0;
  AdvectiveFlux1D_Uniform adv( u );

  Real nu = 1;
  ViscousFlux1D_Uniform visc( nu );

  typedef ForcingFunction1D_MMS<PDEClass> ForcingClass;
  std::shared_ptr<ForcingClass> forcingptr( new ForcingClass(fcn) );
  Source1D_None source;

  PDEClass pde( adv, visc, source, forcingptr );

  for (int i=0;i<5;i++)
  {
    sln = 0;
    pde.forcingFunction(x[i], t, sln);
    SANS_CHECK_CLOSE( fext[i], sln, small_tol, close_tol );

    pde.forcingFunction(x[i], t, sln);
    SANS_CHECK_CLOSE( 2*fext[i], sln, small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Exp_2 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEAdvectionDiffusion1D;
  typedef PDEAdvectionDiffusion1D PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  ScalarFunction1D_Exp2 fcn;

  Real x[5] = { 0, 0.25, 0.5, 0.75, 1 };
  Real time = 0;
  // taken from matlab symbolic
  // Generated using MMS_generator_1D.m
  Real ext[5] = { 0, 0.418369050278306, 0.045789097221835, 0.002818948486833, 0};
  ArrayQ sln;

  const Real close_tol = 1e-10;
  const Real small_tol = 1e-12;
  for (int i=0;i<5;i++)
  {
    sln = fcn(x[i], time);
    SANS_CHECK_CLOSE( ext[i], sln, small_tol, close_tol );
  }

  // taken from matlab symbolic
  // Generated using MMS_generator_1D.m
  Real fext[5] =  {5.980220022609900e+2, -0.150612858100190e+2, -0.042125969444089e+2, -0.004021699841215e+2, -0.000222137647356e+2};

  // PDE
  Real u = 0;
  AdvectiveFlux1D_Uniform adv( u );

  Real nu = 1;
  ViscousFlux1D_Uniform visc( nu );

  typedef ForcingFunction1D_MMS<PDEClass> ForcingClass;
  std::shared_ptr<ForcingClass> forcingptr( new ForcingClass(fcn) );
  Source1D_None source;

  PDEClass pde( adv, visc, source, forcingptr );

  for (int i=0;i<5;i++)
  {
    sln = 0;
    pde.forcingFunction(x[i], time,sln);
    SANS_CHECK_CLOSE( fext[i], sln, small_tol, close_tol );

    pde.forcingFunction(x[i], time,sln);
    SANS_CHECK_CLOSE( 2*fext[i], sln, small_tol, close_tol );
  }
}



//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{

  typedef PDEAdvectionDiffusion<PhysD1,
    AdvectiveFlux1D_Uniform,
    ViscousFlux1D_Uniform,
    Source1D_None > PDEAdvectionDiffusion1D;
  typedef PDEAdvectionDiffusion1D PDEClass;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PDEAdvectionDiffusion1D_ForcingFunction1D_pattern.txt", true );

  ForcingFunction1D_Const<PDEClass> forcing1( 1 );
  forcing1.dump( 2, output );

  ForcingFunction1D_Monomial<PDEClass> forcing2( 2, 1 );
  forcing2.dump( 2, output );

  ForcingFunction1D_Sine<PDEClass> forcing3( 1, 2 );
  forcing3.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
