// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCShallowWater2D_btest
//
// test of 2-D shallow water BC classes

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/ShallowWater/BCShallowWater2D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
template class BCShallowWater2D< BCTypeInflowSupercritical,
                                  PDEShallowWater<PhysD2,VarTypeHVelocity2D,
                                                  ShallowWaterSolutionFunction2D_GeometricSeriesx
                                                  >
                                >;
//
// Instantiate BCParamaters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters< BCShallowWater2DVector<VarTypeHVelocity2D,ShallowWaterSolutionFunction2D_GeometricSeriesx> >;
}

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BCShallowWater2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDEShallowWater<PhysD2,VarTypeHVelocity2D,ShallowWaterSolutionFunction2D_GeometricSeriesx> PDEClass;

  {
    typedef BCShallowWater2D< BCTypeInflowSupercritical, PDEClass > BCClass;
    typedef BCClass::ArrayQ<Real> ArrayQ;
    typedef BCClass::MatrixQ<Real> MatrixQ;

    BOOST_CHECK( BCClass::D == 2 );
    BOOST_CHECK( BCClass::N == 3 );
    BOOST_CHECK( BCClass::NBC == 3 );
    BOOST_CHECK( ArrayQ::M == 3 );
    BOOST_CHECK( MatrixQ::M == 3 );
    BOOST_CHECK( MatrixQ::N == 3 );
  }

  {
    typedef BCNone<PhysD2,PDEClass::N> BCClass;
    typedef BCClass::ArrayQ<Real> ArrayQ;
    typedef BCClass::MatrixQ<Real> MatrixQ;

    BOOST_CHECK( BCClass::D == 2 );
    BOOST_CHECK( BCClass::N == 3 );
    BOOST_CHECK( BCClass::NBC == 0 );
    BOOST_CHECK( ArrayQ::M == 3 );
    BOOST_CHECK( MatrixQ::M == 3 );
    BOOST_CHECK( MatrixQ::N == 3 );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_test )
{
  typedef ShallowWaterSolutionFunction2D_CosineTheta SolutionClass;
  typedef PDEShallowWater<PhysD2,VarTypeHVelocity2D,SolutionClass> PDEClass;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 20;   // q0 = v*H

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;

  PDEClass pde( q0, solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  // BCTypeInflowSupercritical
  {
    typedef BCShallowWater2D< BCTypeInflowSupercritical, PDEClass > BCClass;
    typedef BCShallowWater2DParams<BCTypeInflowSupercritical> BCParamsType;
    typedef BCClass::ArrayQ<Real> ArrayQ;
    typedef boost::mpl::vector< BCShallowWater2D< BCTypeInflowSupercritical, PDEClass > > BCVectorClass;
    typedef BCParameters< BCVectorClass > BCParams;

    const ArrayQ bcdata = { 0, 0, 0 };

    // ArrayQ constructor
    BCClass bc1(pde,bcdata);

    // Pydic constructor
    PyDict BCInflowSupercriticalArgs;
    BCInflowSupercriticalArgs[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSupercritical;
    BCInflowSupercriticalArgs[BCParamsType::params.H] = 2.1;
    BCInflowSupercriticalArgs[BCParamsType::params.vx] = -1.2;
    BCInflowSupercriticalArgs[BCParamsType::params.vy] = 4.9;

    PyDict PyBCList;
    PyBCList["BCInflow"] = BCInflowSupercriticalArgs;

    BCParams::checkInputs(PyBCList);

    BCClass bc2(pde, BCInflowSupercriticalArgs);
  }

  // BCNone
  {
    typedef BCNone<PhysD2,PDEClass::N> BCClass;
    BCClass bc1;

    // Pydic constructor
    PyDict BCNoneArgs;

    BCClass bc2(pde, BCNoneArgs);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_InflowSupercritical_test )
{
  typedef ShallowWaterSolutionFunction2D_CosineTheta SolutionClass;
  typedef PDEShallowWater<PhysD2,VarTypeHVelocity2D,SolutionClass> PDEClass;
  typedef BCShallowWater2D< BCTypeInflowSupercritical, PDEClass > BCClass;

  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 20;   // q0 = v*H

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;

  PDEClass pde( q0, solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  const ArrayQ bcdata = { 7.2, -1.2, 16.2 };
  BCClass bc(pde,bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 3 );
  BOOST_CHECK( bc.NBC == 3 );

  Real x, y, t;
  Real nx, ny;
  Real H, vx, vy;

  x = y = t = 0;  // not used
  nx = 0.4; ny = 0.9;
  Real n = sqrt(nx*nx+ny*ny);
  nx = nx / n;
  ny = ny / n;

  BOOST_CHECK_CLOSE( nx*nx+ny*ny, 1, tol );

  H = 3.9;
  vx = 4.8;
  vy = 0.3;

  ArrayQ q;
  ArrayQ qx, qy; // dummy
  VarTypeHVelocity2D qdata( H, vx, vy );
  pde.setDOFFrom( qdata, q );
  pde.setDOFFrom( qdata, qx );
  pde.setDOFFrom( qdata, qy );

  // strong-form BC
#if ShallowWaterBC_ConvervativeDirichlet2D
  ArrayQ rsdBCTrue = { H - bcdata(0), H*vx - bcdata(1), H*vy - bcdata(2) };
#else
  ArrayQ rsdBCTrue = { H - bcdata(0), vx - bcdata(1), vy - bcdata(2) };
#endif

  ArrayQ rsdBC = 0;
  bc.strongBC( x, y, t, nx, ny, q, qx, qy, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), tol, tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), tol, tol );
  SANS_CHECK_CLOSE( rsdBCTrue(2), rsdBC(2), tol, tol );

#if ShallowWaterHasLGStrongBC2D
  ArrayQ lg = {1.551, -0.237, 9.887 };

  rsdBC = 0;
  bc.strongBC( x, y, t, nx, ny, q, qx, qy, lg, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), tol, tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), tol, tol );
  SANS_CHECK_CLOSE( rsdBCTrue(2), rsdBC(2), tol, tol );
#endif

  // BC weight (B^t)
#if PDEShallowWater_sansgH2fluxAdvective2D  // 0.5*g*H^2 not included in advective flux
  Real dfdudata[9] = { 0, 1, 0,
                       -vx*vx, 2*vx, 0,
                       -vx*vy, vy, vx };
  Real dgdudata[9] = { 0, 0, 1,
                       -vx*vy, vy, vx,
                       -vy*vy, 0, 2*vy };
#else  // 0.5*g*H^2 included in advective flux

#if ShallowWater_IsCircle_NotFlat
  Real theta = atan2(y,x);

  Real dfdudata[9] = { 0, 1, 0,
                       -vx*vx+g*H * sin(theta)*cos(theta), 2*vx, 0,
                       -vx*vy-g*H * sin(theta)*cos(theta), vy, vx };
  Real dgdudata[9] = { 0, 0, 1,
                       -vx*vy-g*H * sin(theta)*cos(theta), vy, vx,
                       -vy*vy+g*H * cos(theta)*cos(theta), 0, 2*vy };
#else
  Real dfdudata[9] = { 0, 1, 0,
                       -vx*vx+g*H, 2*vx, 0,
                       -vx*vy, vy, vx };
  Real dgdudata[9] = { 0, 0, 1,
                       -vx*vy, vy, vx,
                       -vy*vy+g*H, 0, 2*vy };

//  Real theta = atan2(y,x);
//
//  Real dfdudata[9] = { 0, 1, 0,
//                       -vx*vx+g*H * cos(theta)*cos(theta), 2*vx, 0,
//                       -vx*vy+g*H * cos(theta)*sin(theta), vy, vx };
//  Real dgdudata[9] = { 0, 0, 1,
//                       -vx*vy+g*H * sin(theta)*cos(theta), vy, vx,
//                       -vy*vy+g*H * sin(theta)*sin(theta), 0, 2*vy };
#endif

#endif

  Real wghtBCdata[9];
  for (int n = 0; n < 9; n++)
    wghtBCdata[n] = nx*dfdudata[n] + ny*dgdudata[n];

  MatrixQ wghtBCTrue( wghtBCdata, 9 );
  MatrixQ wghtBC = 0;

  bc.weightBC( x, y, t, nx, ny, q, qx, qy, wghtBC );
  SANS_CHECK_CLOSE( wghtBCTrue(0,0), wghtBC(0,0), tol, tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,1), wghtBC(0,1), tol, tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,2), wghtBC(0,2), tol, tol );

  SANS_CHECK_CLOSE( wghtBCTrue(1,0), wghtBC(1,0), tol, tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,1), wghtBC(1,1), tol, tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,2), wghtBC(1,2), tol, tol );

  SANS_CHECK_CLOSE( wghtBCTrue(2,0), wghtBC(2,0), tol, tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,1), wghtBC(2,1), tol, tol );
  SANS_CHECK_CLOSE( wghtBCTrue(2,2), wghtBC(2,2), tol, tol );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
