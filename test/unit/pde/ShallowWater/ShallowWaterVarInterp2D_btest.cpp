// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ShallowWaterVarInterp2D_btest
//
// test of 2-D shallow water PDE interpreter class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/ShallowWater/VarInterp2D.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( VarInterp2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_ctor_test )
{
  // true
  const int PhysDimTrue = 2;
  const int VarDimTrue = 3;

  // check
  BOOST_CHECK( VarInterpret2D<VarTypeHVelocity2D>::D == PhysDimTrue );
  BOOST_CHECK( VarInterpret2D<VarTypeHVelocity2D>::ArrayQ<Real>::M == VarDimTrue );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( get_test )
{
  typedef VarInterpret2D<VarTypeHVelocity2D>::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-15;

  VarInterpret2D<VarTypeHVelocity2D> varInterpret;

  Real H = 16.2, vx = 0.8, vy = -0.6;
  DLA::VectorS<2,Real> v;
  const ArrayQ var = { H, vx, vy };

  varInterpret.getH( var, H );
  SANS_CHECK_CLOSE( var[0], H, tol, tol );

  varInterpret.getVelocity( var, v );
  SANS_CHECK_CLOSE( var[1], v[0], tol, tol );
  SANS_CHECK_CLOSE( var[2], v[1], tol, tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState_test )
{
  typedef VarInterpret2D<VarTypeHVelocity2D>::ArrayQ<Real> ArrayQ;

  VarInterpret2D<VarTypeHVelocity2D> varInterpret;

  Real H = 16.2;
  Real vx = 0.8, vy = -0.6;

  ArrayQ var;

  var = { H, vx, vy };

  BOOST_CHECK( varInterpret.isValidState(var) ); // valid state

  var(0) =  - 0.7;
  BOOST_CHECK( !( varInterpret.isValidState(var) ) ); // invalid state: negative H
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setFromPrimitive_VarTypeHVelocity2D_test )
{
  const int PhysDimTrue = 2;
  const int VarDimTrue = 3;

  typedef VarInterpret2D<VarTypeHVelocity2D>::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-15;

  VarInterpret2D<VarTypeHVelocity2D> varInterpret;

  BOOST_CHECK( varInterpret.D == PhysDimTrue );
  BOOST_CHECK( VarInterpret2D<VarTypeHVelocity2D>::ArrayQ<Real>::M == VarDimTrue );

  Real H = 16.2;
  Real vx = 0.8, vy = 0.6;

  VarTypeHVelocity2D vardata( H, vx, vy );

  ArrayQ var;

  varInterpret.setFromPrimitive( vardata, var);

  SANS_CHECK_CLOSE( H,  var[0], tol, tol );
  SANS_CHECK_CLOSE( vx, var[1], tol, tol );
  SANS_CHECK_CLOSE( vy, var[2], tol, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
