// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ShallowWaterVarInterp1D_btest
//
// test of 1-D shallow water PDE interpreter class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/ShallowWater/VarInterp1D.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( VarInterp1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_ctor_test )
{
  // true
  const int PhysDimTrue = 1;
  const int VarDimTrue = 2;

  // check
  BOOST_CHECK( VarInterpret1D<VarTypeHVelocity1D>::D == PhysDimTrue );
  BOOST_CHECK( VarInterpret1D<VarTypeHVelocity1D>::N == VarDimTrue );
  BOOST_CHECK( VarInterpret1D<VarTypeHVelocity1D>::ArrayQ<Real>::M == VarDimTrue );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( get_test )
{
  typedef VarInterpret1D<VarTypeHVelocity1D>::ArrayQ<Real> ArrayQ;
  const int D = VarInterpret1D<VarTypeHVelocity1D>::D;

  const Real tol = 1.e-15;

  VarInterpret1D<VarTypeHVelocity1D> varInterpret;

  Real H = 16.2;
  DLA::VectorS<D,Real> q = { 0.8 };
  const ArrayQ var = { H, q[0] };

  varInterpret.getH( var, H );
  SANS_CHECK_CLOSE( var[0], H, tol, tol );

  varInterpret.getVelocity( var, q );
  SANS_CHECK_CLOSE( var[1], q[0], tol, tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState_test )
{
  typedef VarInterpret1D<VarTypeHVelocity1D>::ArrayQ<Real> ArrayQ;
  const int D = VarInterpret1D<VarTypeHVelocity1D>::D;

  VarInterpret1D<VarTypeHVelocity1D> varInterpret;

  Real H = 16.2;
  DLA::VectorS<D,Real> q = { 0.8 };

  ArrayQ var;

  var = { H, q[0] };

  BOOST_CHECK( varInterpret.isValidState(var) ); // valid state

  var(0) =  - 0.7;
  BOOST_CHECK( !( varInterpret.isValidState(var) ) ); // invalid state: negative H
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setFromPrimitive_test )
{
  const int PhysDimTrue = 1;
  const int VarDimTrue = 2;

  typedef VarInterpret1D<VarTypeHVelocity1D>::ArrayQ<Real> ArrayQ;
  const int D = VarInterpret1D<VarTypeHVelocity1D>::D;

  const Real tol = 1.e-15;

  VarInterpret1D<VarTypeHVelocity1D> varInterpret;

  BOOST_CHECK( varInterpret.D == PhysDimTrue );
  BOOST_CHECK( varInterpret.N == VarDimTrue );

  Real H = 16.2;
  DLA::VectorS<D,Real> q = { 0.8 };

  VarTypeHVelocity1D vardata( H, q[0] );

  ArrayQ var;

  varInterpret.setFromPrimitive( vardata, var);

  SANS_CHECK_CLOSE( H, var[0], tol, tol );
  SANS_CHECK_CLOSE( q[0], var[1], tol, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
