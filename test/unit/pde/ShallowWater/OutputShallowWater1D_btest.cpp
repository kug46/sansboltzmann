// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// testing routines for conversion of solution variables (Q) to
//     output of interest (Output): 1-D shallow water equations

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"

#include "pde/ShallowWater/OutputShallowWater1D.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class OutputShallowWater1D_H<VarTypeHVelocity1D,
                                      PDENDConvertSpace<PhysD1,
                                                         PDEShallowWater<PhysD1,VarTypeHVelocity1D,
                                                                         ShallowWaterSolutionFunction1D_GeometricSeriesx>
                                                        >
                                     >;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( OutputShallowWater1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( OutputShallowWater1D_H_test )
{
  typedef PhysD1 PhysDim;
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysDim,VarTypeHVelocity1D,SolutionClass> PDEClass;
  typedef PDENDConvertSpace<PhysDim, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 18.5;    // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  NDPDEClass pde(q0,solnArgs,PDEClass::ShallowWater_ResidInterp_Raw);

  const Real tol = 1.e-15;

  // static
  BOOST_CHECK( NDPDEClass::PhysDim::D == 1 );

  // ctor
  OutputShallowWater1D_H<VarTypeHVelocity1D,NDPDEClass> qtoout(pde);

  // overloaded operator()
  const Real H = 1.8, u = 10.2;
  ArrayQ q = { H, u };
  ArrayQ qx = {0.0, 0.0};

  Real x = 0.0, time = 0.0;

  Real out;
  qtoout(x, time, q, qx, out);

  SANS_CHECK_CLOSE( H, out, tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( OutputShallowWater1D_V_test )
{
  typedef PhysD1 PhysDim;
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysDim,VarTypeHVelocity1D,SolutionClass> PDEClass;
  typedef PDENDConvertSpace<PhysDim, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 18.5;    // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  NDPDEClass pde(q0,solnArgs,PDEClass::ShallowWater_ResidInterp_Raw);

  const Real tol = 1.e-15;

  // static
  BOOST_CHECK( NDPDEClass::PhysDim::D == 1 );

  // ctor
  OutputShallowWater1D_V<VarTypeHVelocity1D,NDPDEClass> qtoout(pde);

  // overloaded operator()
  const Real H = 1.8, u = 10.2;
  ArrayQ q = { H, u };
  ArrayQ qx = {0.0, 0.0};

  Real x = 0.0, time = 0.0;

  Real out;
  qtoout(x, time, q, qx, out);

  SANS_CHECK_CLOSE( u, out, tol, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
