// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEShallowWater1D_btest
//
// test of 1-D shallow water PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/ShallowWater/PDEShallowWater1D.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEShallowWater1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD1, VarTypeHVelocity1D, SolutionClass> PDEClass;
  // true
  const int PhysDimTrue = 1;
  const int VarDimTrue = 2;

  // check
  BOOST_CHECK( (PDEClass::D == PhysDimTrue) );
  BOOST_CHECK( (PDEClass::N == VarDimTrue) );
  BOOST_CHECK( (PDEClass::ArrayQ<Real>::M == VarDimTrue) );
  BOOST_CHECK( (PDEClass::MatrixQ<Real>::M == VarDimTrue) );
  BOOST_CHECK( (PDEClass::MatrixQ<Real>::N == VarDimTrue) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctorsANDhas_test )
{
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD1, VarTypeHVelocity1D, SolutionClass> PDEClass;

  const Real tol = 1.e-15;

  // true
  const int PhysDimTrue = 1;
  const int VarDimTrue = 2;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 20;   // q0 = v*H
  const int p = 3;      // polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  // actual: test constructor
  PDEClass pde( q0, solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );
  SANS_CHECK_CLOSE( g, pde.getg(), tol, tol );

  // check dimensions
  BOOST_CHECK( pde.D == PhysDimTrue );
  BOOST_CHECK( pde.N == VarDimTrue );

  // hasFLux functions
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

#if PDEShallowWater_sansgH2fluxAdvective1D
  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );
#else
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );
#endif

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( masterState_test )
{
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD1, VarTypeHVelocity1D, SolutionClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  const int N = PDEClass::N;

  const Real tol = 3.E-14;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 20;   // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  PDEClass pde( q0, solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  // used
  Real H = 16.6, u = 0.3;
  const VarTypeHVelocity1D vardata(H, u);
  ArrayQ var;
  pde.setDOFFrom(vardata, var);

  BOOST_CHECK( pde.isValidState(var) );

  ArrayQ uConsTrue = { H, H*u };
  ArrayQ uCons;

  Real x = 0, time = 0;

  // results
  pde.masterState(x, time, var, uCons);

  for ( int i = 0; i < N; i++ )
    SANS_CHECK_CLOSE( uConsTrue[i], uCons[i], tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxAdvective_test )
{
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD1, VarTypeHVelocity1D, SolutionClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  const int N = PDEClass::N;

  const Real tol = 1.E-14;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 20;   // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  PDEClass pde( q0, solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  // unused
  const Real x = 1.6;
  const Real t = 0.3;
  // used
  Real H = 16.6, u = -0.5;
  const VarTypeHVelocity1D vardata(H, u);
  ArrayQ var;
  pde.setDOFFrom(vardata, var);

  BOOST_CHECK( pde.isValidState(var) );

  ArrayQ f = { 0., 0. };

  // results
#if PDEShallowWater_sansgH2fluxAdvective1D  // 0.5*g*H^2 not included in advective flux
  DLA::VectorS<N,Real> ftrue = { H*u, H*u*u };
#else  // 0.5*g*H^2 included in advective flux
  DLA::VectorS<N,Real> ftrue = { H*u, H*u*u + 0.5*g*H*H };
#endif

  // Advective flux
  pde.fluxAdvective(x,t,var,f);

  for ( int i = 0; i < N; i++ )
    SANS_CHECK_CLOSE( ftrue[i], f[i], tol, tol );

  // Advective flux cumulation
  pde.fluxAdvective(x,t,var,f);
  for ( int i = 0; i < N; i++ )
    SANS_CHECK_CLOSE( 2*ftrue[i], f[i], tol, tol );

  // Advective flux in time
  ArrayQ ftTrue = { H, H*u };
  ArrayQ ft = 0;
  pde.fluxAdvectiveTime(x, t, var, ft);

  for ( int i = 0; i < N; i++ )
    SANS_CHECK_CLOSE( ftTrue[i], ft[i], tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxAdvectiveUpwind_test )
{
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD1, VarTypeHVelocity1D, SolutionClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  const int N = PDEClass::N;

  const Real tol = 1.E-13;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 15;   // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  PDEClass pde( q0, solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  // unused
  const Real x = 1.6;
  const Real t = 0.3;

  // used
  Real HL = 16.6, uL = -0.5;
  Real HR = 3.7,  uR = 0.3;

  Real nx = -1;

  SANS_CHECK_CLOSE( 1., fabs(nx), tol, tol );

  const VarTypeHVelocity1D vardataL(HL, uL);
  const VarTypeHVelocity1D vardataR(HR, uR);

  ArrayQ varL, varR;

  pde.setDOFFrom(vardataL, varL);
  pde.setDOFFrom(vardataR, varR);

  BOOST_CHECK( pde.isValidState(varL) );
  BOOST_CHECK( pde.isValidState(varR) );

  ArrayQ f = { 0., 0. };
  ArrayQ ftrue;

#if PDEShallowWater_sansgH2fluxAdvective1D  // 0.5*g*H^2 not included in advective flux
  Real lambdaAbsMaxL, lambdaAbsMaxR; // largest eigenvalue in absolute value of flux jacobians
  Real alpha;

  lambdaAbsMaxL = fabs(uL*nx);
  lambdaAbsMaxR = fabs(uR*nx);

  alpha = std::max( lambdaAbsMaxL, lambdaAbsMaxR );

  ftrue(0) = 0.5 * (HL*uL + HR*uR)*nx + 0.5*(HL-HR)*alpha;
  ftrue(1) = 0.5 * (HL*uL*uL + HR*uR*uR )*nx + 0.5*( HL*uL - HR*uR )*alpha;
#else  // 0.5*g*H^2 included in advective flux
  Real lambdaAbsMaxL, lambdaAbsMaxR; // largest eigenvalue in absolute value of flux jacobians
  Real alpha;

  lambdaAbsMaxL = fabs(uL*nx) + sqrt(g*HL);
  lambdaAbsMaxR = fabs(uR*nx) + sqrt(g*HR);

  alpha = std::max( lambdaAbsMaxL, lambdaAbsMaxR );

  ftrue(0) = 0.5 * (HL*uL + HR*uR)*nx + 0.5*(HL-HR)*alpha;
  ftrue(1) = 0.5 * (HL*uL*uL + 0.5*g*HL*HL + HR*uR*uR + 0.5*g*HR*HR)*nx
           + 0.5*( HL*uL - HR*uR )*alpha;
#endif

  // upwind advective flux
  pde.fluxAdvectiveUpwind(x,t,varL,varR,nx,f);
  for ( int i = 0; i < N; i++ )
  {
    SANS_CHECK_CLOSE( ftrue[i], f[i], tol, tol );
  }

  // Advective flux cumulation
  pde.fluxAdvectiveUpwind(x,t,varL,varR,nx,f);
  for ( int i = 0; i < N; i++ )
  {
    SANS_CHECK_CLOSE( 2*ftrue[i], f[i], tol, tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jacobianFluxAdvective_test )
{
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD1, VarTypeHVelocity1D, SolutionClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;

  const int N = PDEClass::N;

  const Real tol = 1.E-13;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 2;    // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  PDEClass pde( q0, solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  // unused
  const Real x = 1.6;
  const Real t = 0.3;
  // used
  Real H = 16.6, vx = -0.5;
  const VarTypeHVelocity1D vardata(H, vx);
  ArrayQ var;
  pde.setDOFFrom(vardata, var);

  BOOST_CHECK( pde.isValidState(var) );

  // results
#if PDEShallowWater_sansgH2fluxAdvective1D  // 0.5*g*H^2 not included in advective flux
  Real dfdudata[4] = {0,      1,
                      -vx*vx, 2*vx};
#else  // 0.5*g*H^2 included in advective flux
  Real dfdudata[4] = {0,            1,
                      -vx*vx + g*H, 2*vx};
#endif

  MatrixQ dfduTrue( dfdudata, 4 );
  MatrixQ dfdu = 0;

  // advective flux jabocian
  pde.jacobianFluxAdvective(x,t,var,dfdu);

  for ( int i = 0; i < N; i++)
  {
    for ( int j = 0; j < N; j++)
    {
      SANS_CHECK_CLOSE( dfduTrue(i,j), dfdu(i,j), tol, tol );
    }
  }

  // advective flux jacobian cumulation
  pde.jacobianFluxAdvective(x,t,var,dfdu);

  for ( int i = 0; i < N; i++)
  {
    for ( int j = 0; j < N; j++)
    {
      SANS_CHECK_CLOSE( 2*dfduTrue(i,j), dfdu(i,j), tol, tol );
    }
  }
}


#if ShallowWaterHasStrongFluxAdvective1D
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( strongFluxAdvective_test )
{
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD1, VarTypeHVelocity1D, SolutionClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  const int N = PDEClass::N;

  const Real tol = 1.E-13;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 17;    // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  PDEClass pde( q0, solnArgs );

  // unused
  const Real x = 1.6;
  const Real t = 0.3;
  // used
  Real H = 16.6, u = -0.5;
  Real H_x = 0.4, u_x = 3.5;

  const VarTypeHVelocity1D vardata(H, u);
  const VarTypeHVelocity1D varxdata(H_x, u_x);
  ArrayQ var, varx;
  pde.setDOFFrom(vardata, var);
  pde.setDOFFrom(varxdata, varx);

  BOOST_CHECK( pde.isValidState(var) );

  ArrayQ strongPDE = { 0., 0. };
  ArrayQ strongPDEtrue;

  // results
#if PDEShallowWater_sansgH2fluxAdvective1D  // 0.5*g*H^2 not included in advective flux
  strongPDEtrue(0) = H_x*u + H*u_x;
  strongPDEtrue(1) = H_x*u*u + 2*H*u*u_x;
#else  // 0.5*g*H^2 included in advective flux
  strongPDEtrue(0) = H_x*u + H*u_x;
  strongPDEtrue(1) = H_x*u*u + 2*H*u*u_x + g*H*H_x;
#endif

  // strong flux advective
  pde.strongFluxAdvective(x,t,var,varx,strongPDE);

  for ( int i = 0; i < N; i++ )
    SANS_CHECK_CLOSE( strongPDEtrue(i), strongPDE(i), tol, tol );

  // strong flux advective cumulation
  pde.strongFluxAdvective(x,t,var,varx,strongPDE);

  for ( int i = 0; i < N; i++ )
    SANS_CHECK_CLOSE( 2*strongPDEtrue(i), strongPDE(i), tol, tol );
}
#endif


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( source_test )
{
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD1, VarTypeHVelocity1D, SolutionClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  const int N = PDEClass::N;

  const Real tol = 1.E-13;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 6.5;    // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  PDEClass pde( q0, solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  // unused
  const Real x = 1.6;
  const Real t = 0.3;
  // used
  Real u = 0.6;
  Real u_x = 3.5;

  Real H = 1 + x + pow(x,2) + pow(x,3) + pow(x,4) + pow(x,5);
  Real H_x = 1 + 2*x + 3*pow(x,2) + 4*pow(x,3) + 5*pow(x,4);

  const VarTypeHVelocity1D vardata(H, u);
  const VarTypeHVelocity1D varxdata(H_x, u_x);
  ArrayQ var, varx;
  pde.setDOFFrom(vardata, var);
  pde.setDOFFrom(varxdata, varx);

  BOOST_CHECK( pde.isValidState(var) );

  ArrayQ source = { 0., 0., };
  ArrayQ sourcetrue;

  Real b_x;
  // choose a bathymetry type
  Real geoSeries = 1 + x + pow(x,2) + pow(x,3) + pow(x,4) + pow(x,5);
  Real geoSeries_x =   1 + 2*x + 3*pow(x,2) + 4*pow(x,3) + 5*pow(x,4);
  b_x = ( 1 - q0*q0/(g*pow(geoSeries,3)) ) * geoSeries_x;

  // results
#if PDEShallowWater_sansgH2fluxAdvective1D  // 0.5*g*H^2 not included in advective flux
  sourcetrue(0) = 0;
  sourcetrue(1) = g*H*(H_x-b_x);
#else  // 0.5*g*H^2 included in advective flux
  sourcetrue(0) = 0;
  sourcetrue(1) = -g*H*b_x;
#endif

  // strong flux advective
  pde.source(x,t,var,varx,source);

  for ( int i = 0; i < N; i++ )
    SANS_CHECK_CLOSE( sourcetrue(i), source(i), tol, tol );

  // strong flux advective cumulation
  pde.source(x,t,var,varx,source);

  for ( int i = 0; i < N; i++ )
    SANS_CHECK_CLOSE( 2*sourcetrue(i), source(i), tol, tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( forcingFunction_test )
{
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx_Forcing SolutionClass;
  typedef PDEShallowWater<PhysD1, VarTypeHVelocity1D, SolutionClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  const int N = PDEClass::N;

  const Real tol = 1.E-13;

  const Real g = 9.81;   // g: gravitational acceleration [m/s^2]
  const Real q0 = 6.5;   // q0 = v*H
  const Real cH = 0.5;
  const int pH = 5;      // solution H polynomial degree
  const int pu = 5;      // solution u polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.pH] = pH;
  solnArgs[SolutionClass::ParamsType::params.cH] = cH;
  solnArgs[SolutionClass::ParamsType::params.pu] = pu;

  PDEClass pde( q0, solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  BOOST_CHECK( pde.hasForcingFunction() == true );

  // unused
  const Real x = 1.6;
  const Real t = 0.3;

  // used
  Real u = 1 + x + pow(x,2) + pow(x,3) + pow(x,4) + pow(x,5);
  Real u_x = 1 + 2*x + 3*pow(x,2) + 4*pow(x,3) + 5*pow(x,4);

  Real H = cH*( 1 + x + pow(x,2) + pow(x,3) + pow(x,4) + pow(x,5) );
  Real H_x = cH*( 1 + 2*x + 3*pow(x,2) + 4*pow(x,3) + 5*pow(x,4) );

  const VarTypeHVelocity1D vardata(H, u);
  const VarTypeHVelocity1D varxdata(H_x, u_x);
  ArrayQ var, varx;
  pde.setDOFFrom(vardata, var);
  pde.setDOFFrom(varxdata, varx);

  BOOST_CHECK( pde.isValidState(var) );

  ArrayQ forcing = { 0., 0. };
  ArrayQ forcingTrue = { u*H_x+H*u_x, (u*u+g*H)*H_x+2*H*u*u_x - g*H*1 };

  // strong flux advective
  pde.forcingFunction(x,t,forcing);

  for ( int i = 0; i < N; i++ )
    SANS_CHECK_CLOSE( forcingTrue(i), forcing(i), tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState_test )
{

  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD1, VarTypeHVelocity1D, SolutionClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 1.2;    // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  PDEClass pde( q0, solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  Real H = 16.2;
  Real u = 0.8;

  ArrayQ var;

  var = { H, u };

  BOOST_CHECK( pde.isValidState(var) ); // valid state

  var(0) =  - 0.7;
  BOOST_CHECK( !( pde.isValidState(var) ) ); // invalid state: negative H
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setDOFFrom_test )
{
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD1, VarTypeHVelocity1D, SolutionClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-15;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 6.5;  // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  PDEClass pde( q0, solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  Real H = 16.2;
  Real u = 0.8;

  VarTypeHVelocity1D vardata( H, u );

  ArrayQ var;

  pde.setDOFFrom( vardata, var );

  SANS_CHECK_CLOSE( H,  var[0], tol, tol );
  SANS_CHECK_CLOSE( u, var[1], tol, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
