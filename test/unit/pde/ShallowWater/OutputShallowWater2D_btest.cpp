// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// testing routines for conversion of solution variables (Q) to
//     output of interest (Output): 2-D shallow water equations

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "pde/ShallowWater/OutputShallowWater2D.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class OutputShallowWater2D_H<VarTypeHVelocity2D,
                                      PDENDConvertSpace<PhysD2,
                                                         PDEShallowWater<PhysD2,VarTypeHVelocity2D,
                                                                         ShallowWaterSolutionFunction2D_GeometricSeriesx>
                                                        >
                                     >;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( OutputShallowWater2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( OutputShallowWater2D_H_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD2,VarTypeHVelocity2D,SolutionClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::PhysDim PhysDim;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 18.5;    // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  NDPDEClass pde(q0,solnArgs,PDEClass::ShallowWater_ResidInterp_Raw);

  const Real tol = 1.e-15;

  // static
  BOOST_CHECK( PhysDim::D == 2 );

  // ctor
  OutputShallowWater2D_H<VarTypeHVelocity2D,NDPDEClass> qtoout(pde);

  // overloaded operator()
  const Real H = 1.8, vx = 10.2, vy = -0.2;
  ArrayQ q = { H, vx, vy };
  ArrayQ qx = {0.3, 5.0, 0.1}; //Not used
  ArrayQ qy = {0.2, 1.0, 0.4}; //Not used

  Real x = 0.4, y = 0.6, time = 1.0; //Not used

  Real out;
  qtoout(x, y, time, q, qx, qy, out);

  SANS_CHECK_CLOSE( H, out, tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( OutputShallowWater2D_V_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD2,VarTypeHVelocity2D,SolutionClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::PhysDim PhysDim;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 18.5;    // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  NDPDEClass pde(q0,solnArgs, PDEClass::ShallowWater_ResidInterp_Raw);

  const Real tol = 1.e-13;

  // static
  BOOST_CHECK( PhysDim::D == 2 );

  // ctor
  OutputShallowWater2D_V<VarTypeHVelocity2D,NDPDEClass> qtoout(pde);

  // overloaded operator()
  const Real H = 1.8, vx = 10.2, vy = -0.2;
  ArrayQ q = { H, vx, vy };
  ArrayQ qx = {0.3, 5.0, 0.1}; //Not used
  ArrayQ qy = {0.2, 1.0, 0.4}; //Not used

  Real x = 0.4, y = 0.6, time = 1.0; //Not used

  Real out;
  out = qtoout(x, y, time, q, qx, qy);

  SANS_CHECK_CLOSE( sqrt(vx*vx+vy*vy), out, tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( OutputShallowWater2D_Vx_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD2,VarTypeHVelocity2D,SolutionClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::PhysDim PhysDim;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 18.5;    // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  NDPDEClass pde(q0,solnArgs,PDEClass::ShallowWater_ResidInterp_Raw);

  const Real tol = 1.e-15;

  // static
  BOOST_CHECK( PhysDim::D == 2 );

  // ctor
  OutputShallowWater2D_Vx<VarTypeHVelocity2D,NDPDEClass> qtoout(pde);

  // overloaded operator()
  const Real H = 1.8, vx = 10.2, vy = -0.2;
  ArrayQ q = { H, vx, vy };
  ArrayQ qx = {0.3, 5.0, 0.1}; //Not used
  ArrayQ qy = {0.2, 1.0, 0.4}; //Not used

  Real x = 0.4, y = 0.6, time = 1.0; //Not used

  Real out;
  out = qtoout(x, y, time, q, qx, qy);

  SANS_CHECK_CLOSE( vx, out, tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( OutputShallowWater2D_Vy_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD2,VarTypeHVelocity2D,SolutionClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::PhysDim PhysDim;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 18.5;    // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  NDPDEClass pde(q0,solnArgs,PDEClass::ShallowWater_ResidInterp_Raw);

  const Real tol = 1.e-15;

  // static
  BOOST_CHECK( PhysDim::D == 2 );

  // ctor
  OutputShallowWater2D_Vy<VarTypeHVelocity2D,NDPDEClass> qtoout(pde);

  // overloaded operator()
  const Real H = 1.8, vx = 10.2, vy = -0.2;
  ArrayQ q = { H, vx, vy };
  ArrayQ qx = {0.3, 5.0, 0.1}; //Not used
  ArrayQ qy = {0.2, 1.0, 0.4}; //Not used

  Real x = 0.4, y = 0.6, time = 1.0; //Not used

  Real out;
  out = qtoout(x, y, time, q, qx, qy);

  SANS_CHECK_CLOSE( vy, out, tol, tol );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
