// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEShallowWater2D_manifold_btest
//
// test of 2-D shallow water manifold PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/ShallowWater/PDEShallowWater2D_manifold.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEShallowWater2D_manifold_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta SolutionClass;
  typedef VarTypeHVelocity2D_manifold VarType;
  typedef PhysD2 PhysDim;
  typedef PDEShallowWater_manifold<PhysDim, VarType, SolutionClass> PDEClass;

  // true
  const int PhysDimTrue = 2;
  const int VarDimTrue = 2;

  // check
  BOOST_CHECK( (PDEClass::D == PhysDimTrue) );
  BOOST_CHECK( (PDEClass::N == VarDimTrue) );
  BOOST_CHECK( (PDEClass::ArrayQ<Real>::M == VarDimTrue) );
  BOOST_CHECK( (PDEClass::MatrixQ<Real>::M == VarDimTrue) );
  BOOST_CHECK( (PDEClass::MatrixQ<Real>::N == VarDimTrue) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctorsANDhas_test )
{
  typedef ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta SolutionClass;
  typedef VarTypeHVelocity2D_manifold VarType;
  typedef PhysD2 PhysDim;
  typedef PDEShallowWater_manifold<PhysDim, VarType, SolutionClass> PDEClass;

  const Real tol = 1.e-15;

  // true
  const int PhysDimTrue = 2;
  const int VarDimTrue = 2;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 20;   // q0 = v*H
  const int p = 5;      // solution H polynomial degree
  const Real a = 3.4, b = 1.3;

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  solnArgs[SolutionClass::ParamsType::params.a] = a;
  solnArgs[SolutionClass::ParamsType::params.b] = b;

  // actual: test constructor
  PDEClass pde( solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );
  SANS_CHECK_CLOSE( g, pde.getg(), tol, tol );


  // check dimensions
  BOOST_CHECK( pde.D == PhysDimTrue );
  BOOST_CHECK( pde.N == VarDimTrue );

  // hasFLux functions
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

#if PDEShallowWaterManifold_sansgH2fluxAdvective2D
  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );
#else
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );
#endif

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( masterState_test )
{
  typedef ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta SolutionClass;
  typedef VarTypeHVelocity2D_manifold VarType;
  typedef PhysD2 PhysDim;
  typedef PDEShallowWater_manifold<PhysDim, VarType, SolutionClass> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  const int N = PDEClass::N;

  const Real tol = 3.E-14;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 20;   // q0 = v*H
  const int p = 5;      // solution H polynomial degree
  const Real a = 3.4, b = 1.3;

  const Real x = 1.6;
  const Real y = 3.8;
  const Real t = 0.3;

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  solnArgs[SolutionClass::ParamsType::params.a] = a;
  solnArgs[SolutionClass::ParamsType::params.b] = b;

  PDEClass pde( solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  Real H = 16.6, vs = 1.2;
  const VarTypeHVelocity2D_manifold vardata(H, vs);
  ArrayQ var;
  pde.setDOFFrom(vardata, var);

  BOOST_CHECK( pde.isValidState(var) );

  ArrayQ uConsTrue = { H, H*vs };
  ArrayQ uCons;

  // results
  pde.masterState(x, y, t, var, uCons);

  for ( int i = 0; i < N; i++ )
    SANS_CHECK_CLOSE( uConsTrue[i], uCons[i], tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxAdvective_test )
{
  typedef ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta SolutionClass;
  typedef VarTypeHVelocity2D_manifold VarType;
  typedef PhysD2 PhysDim;
  typedef PDEShallowWater_manifold<PhysDim, VarType, SolutionClass> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  const int N = PDEClass::N;

  typedef PDEClass::VectorX VectorX;

  const Real tol = 3.E-14;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 20;   // q0 = v*H
  const int p = 5;      // solution H polynomial degree
  const Real a = 3.4, b = 1.3;

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  solnArgs[SolutionClass::ParamsType::params.a] = a;
  solnArgs[SolutionClass::ParamsType::params.b] = b;

  PDEClass pde( solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  const Real x = 1.6;
  const Real y = 3.8;
  const Real t = 0.3;

  const VectorX e1 = { 0.8, 0.6 };
  const VectorX e1x = { -1., 0.3 };
  const VectorX e1y = { 0.2, -0.1 };

  VectorX s; // streamwise unit vector
  const Real theta = atan2(a*y,b*x);

  const Real d = sqrt( pow( -a*sin(theta), 2 ) + pow( b*cos(theta), 2 ) );
  s[0] = -a*sin(theta)/d;
  s[1] =  b*cos(theta)/d;

  Real H = 16.6, vs = 1.2;
  const VarTypeHVelocity2D_manifold vardata(H, vs);
  ArrayQ var;
  pde.setDOFFrom(vardata, var);

  BOOST_CHECK( pde.isValidState(var) );

  const Real vx = vs*s[0], vy = vs*s[1];

  ArrayQ fx = { 0., 0. };
  ArrayQ fy = { 0., 0. };

  // results
#if PDEShallowWaterManifold_sansgH2fluxAdvective2D  // 0.5*g*H^2 not included in advective flux
  DLA::VectorS<N,Real> fxtrue = { H*vx, H*vx*vs*dot(s,e1) };
  DLA::VectorS<N,Real> fytrue = { H*vy, H*vy*vs*dot(s,e1) };
#else  // 0.5*g*H^2 included in advective flux
#endif

  // Advective flux
    pde.fluxAdvective(e1, e1x, e1y, x, y, t, var, fx, fy);

  for ( int i = 0; i < N; i++ )
  {
    SANS_CHECK_CLOSE( fxtrue[i], fx[i], tol, tol );
    SANS_CHECK_CLOSE( fytrue[i], fy[i], tol, tol );
  }

  // Advective flux cumulation
  pde.fluxAdvective(e1, e1x, e1y, x, y, t, var, fx, fy);

  for ( int i = 0; i < N; i++ )
  {
    SANS_CHECK_CLOSE( 2*fxtrue[i], fx[i], tol, tol );
    SANS_CHECK_CLOSE( 2*fytrue[i], fy[i], tol, tol );
  }

  // Advective flux in time
  ArrayQ ftTrue = { H, H*vs };
  ArrayQ ft = 0;
  pde.fluxAdvectiveTime(x, y, t, var, ft);

  for ( int i = 0; i < N; i++ )
    SANS_CHECK_CLOSE( ftTrue[i], ft[i], tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxAdvectiveUpwind_test )
{
  typedef ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta SolutionClass;
  typedef VarTypeHVelocity2D_manifold VarType;
  typedef PhysD2 PhysDim;
  typedef PDEShallowWater_manifold<PhysDim, VarType, SolutionClass> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  const int N = PDEClass::N;

  //  typedef PDEClass::VectorEIF VectorEIF;
  typedef PDEClass::VectorX VectorX;

  const Real tol = 1.E-13;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 20;   // q0 = v*H
  const int p = 5;      // solution H polynomial degree
  const Real a = 3.4, b = 1.3;

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  solnArgs[SolutionClass::ParamsType::params.a] = a;
  solnArgs[SolutionClass::ParamsType::params.b] = b;

  PDEClass pde( solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  const Real x = 1.6;
  const Real y = 3.8;
  const Real t = 0.3;

  const VectorX e1L = { 0.8, 0.6 };
  const VectorX e1R = { sqrt(0.5), sqrt(0.5) };

  VectorX s; // streamwise unit vector
  const Real theta = atan2(a*y,b*x);

  const Real d = sqrt( pow( -a*sin(theta), 2 ) + pow( b*cos(theta), 2 ) );
  s[0] = -a*sin(theta)/d;
  s[1] =  b*cos(theta)/d;

  const Real HL = 16.6, HR = 2.1;
  const Real vsL = 1.2, vsR = -0.3;
  const VarType vardataL(HL, vsL);
  const VarType vardataR(HR, vsR);

  ArrayQ varL, varR;
  pde.setDOFFrom(vardataL, varL);
  pde.setDOFFrom(vardataR, varR);

  BOOST_CHECK( pde.isValidState(varL) );
  BOOST_CHECK( pde.isValidState(varR) );

  const Real vxL = vsL*s[0], vyL = vsL*s[1];
  const Real vxR = vsR*s[0], vyR = vsR*s[1];

  Real nx = 0.6, ny = 0.8;
  nx = nx / sqrt(nx*nx + ny*ny);
  ny = ny / sqrt(nx*nx + ny*ny);

  SANS_CHECK_CLOSE( 1., nx*nx + ny*ny, tol, tol );

  ArrayQ f = { 0., 0. };
  ArrayQ ftrue;

#if PDEShallowWaterManifold_sansgH2fluxAdvective2D  // 0.5*g*H^2 not included in advective flux
  Real lambdaAbsMaxL, lambdaAbsMaxR; // largest eigenvalue in absolute value of flux jacobians
  Real alpha;

  lambdaAbsMaxL = fabs(vsL);
  lambdaAbsMaxR = fabs(vsR);
  alpha = std::max( lambdaAbsMaxL, lambdaAbsMaxR );

  ftrue(0) = 0.5*( (HL*vxL + HR*vxR)*nx + (HL*vyL + HR*vyR)*ny )
               + 0.5*(HL - HR)*alpha;
  ftrue(1) = 0.5*( (HL*vxL*vsL*dot(s,e1L) + HR*vxR*vsR*dot(s,e1R))*nx + (HL*vyL*vsL*dot(s,e1L) + HR*vyR*vsR*dot(s,e1R))*ny )
               + 0.5*( HL*vsL*dot(s,e1L) - HR*vsR*dot(s,e1R) )*alpha;
#else  // 0.5*g*H^2 included in advective flux
#endif

  // upwind advective flux
  pde.fluxAdvectiveUpwind(e1L, e1R, x , y , t, varL, varR, nx, ny, f);
  for ( int i = 0; i < N; i++ )
  {
    SANS_CHECK_CLOSE( ftrue[i], f[i], tol, tol );
  }

  // Advective flux cumulation
  pde.fluxAdvectiveUpwind(e1L, e1R, x , y , t, varL, varR, nx, ny, f);
  for ( int i = 0; i < N; i++ )
  {
    SANS_CHECK_CLOSE( 2*ftrue[i], f[i], tol, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jacobianFluxAdvective_case1_test )
{
  typedef ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta SolutionClass;
  typedef VarTypeHVelocity2D_manifold VarType;
  typedef PhysD2 PhysDim;
  typedef PDEShallowWater_manifold<PhysDim, VarType, SolutionClass> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  const int N = PDEClass::N;

  typedef PDEClass::VectorX VectorX;

  const Real tol = 1.E-13;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 20;   // q0 = v*H
  const int p = 5;      // solution H polynomial degree
  const Real a = 3.4, b = 1.3;

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  solnArgs[SolutionClass::ParamsType::params.a] = a;
  solnArgs[SolutionClass::ParamsType::params.b] = b;

  PDEClass pde( solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  const Real x = 1.6;
  const Real y = 3.8;
  const Real t = 0.3;

  VectorX e1 = { 0.8, 0.6 };
  const VectorX e1x = { -1., 0.3 };
  const VectorX e1y = { 0.2, -0.1 };

  VectorX s;

  const Real theta = atan2(a*y,b*x);

  const Real d = sqrt( pow( -a*sin(theta), 2 ) + pow( b*cos(theta), 2 ) );
  s[0] = -a*sin(theta)/d;
  s[1] =  b*cos(theta)/d;

  Real H = 16.6, vs = 1.2;
  const VarType vardata(H, vs);
  ArrayQ var;
  pde.setDOFFrom(vardata, var);

  BOOST_CHECK( pde.isValidState(var) );

  // results
#if PDEShallowWaterManifold_sansgH2fluxAdvective2D  // 0.5*g*H^2 not included in advective flux
  Real dfdudata[4] = {0,                            1 / e1[0],
                      -pow(vs*dot(s,e1),2) / e1[0], 2*vs*dot(s,e1) / e1[0]};
  Real dgdudata[4] = {0,                            1 / e1[1],
                      -pow(vs*dot(s,e1),2) / e1[1], 2*vs*dot(s,e1) / e1[1]};
#else  // 0.5*g*H^2 included in advective flux
#endif

  MatrixQ dfduTrue( dfdudata, 4 );
  MatrixQ dgduTrue( dgdudata, 4 );
  MatrixQ dfdu = 0 , dgdu = 0;

  // advective flux jabocian
  pde.jacobianFluxAdvective(e1,x,y,t,var,dfdu,dgdu);

  for ( int i = 0; i < N; i++)
  {
    for ( int j = 0; j < N; j++)
    {
      SANS_CHECK_CLOSE( dfduTrue(i,j), dfdu(i,j), tol, tol );
      SANS_CHECK_CLOSE( dgduTrue(i,j), dgdu(i,j), tol, tol );
    }
  }

  // advective flux jacobian cumulation
  pde.jacobianFluxAdvective(e1,x,y,t,var,dfdu,dgdu);

  for ( int i = 0; i < N; i++)
  {
    for ( int j = 0; j < N; j++)
    {
      SANS_CHECK_CLOSE( 2*dfduTrue(i,j), dfdu(i,j), tol, tol );
      SANS_CHECK_CLOSE( 2*dgduTrue(i,j), dgdu(i,j), tol, tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jacobianFluxAdvective_case2_test )
{
  typedef ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta SolutionClass;
  typedef VarTypeHVelocity2D_manifold VarType;
  typedef PhysD2 PhysDim;
  typedef PDEShallowWater_manifold<PhysDim, VarType, SolutionClass> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  const int N = PDEClass::N;

  typedef PDEClass::VectorX VectorX;

  const Real tol = 1.E-15;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 20;   // q0 = v*H
  const int p = 5;      // solution H polynomial degree
  const Real a = 3.4, b = 1.3;

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  solnArgs[SolutionClass::ParamsType::params.a] = a;
  solnArgs[SolutionClass::ParamsType::params.b] = b;

  PDEClass pde( solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  const Real x = 1.6;
  const Real y = 3.8;
  const Real t = 0.3;

  VectorX e1 = { 0, 1 };
  const VectorX e1x = { -1., 0.3 };
  const VectorX e1y = { 0.2, -0.1 };

  VectorX s;

  const Real theta = atan2(a*y,b*x);

  const Real d = sqrt( pow( -a*sin(theta), 2 ) + pow( b*cos(theta), 2 ) );
  s[0] = -a*sin(theta)/d;
  s[1] =  b*cos(theta)/d;

  Real H = 16.6, vs = 1.2;
  const VarType vardata(H, vs);
  ArrayQ var;
  pde.setDOFFrom(vardata, var);

  BOOST_CHECK( pde.isValidState(var) );

  // results
#if PDEShallowWaterManifold_sansgH2fluxAdvective2D  // 0.5*g*H^2 not included in advective flux
  Real dgdudata[4] = {0,                            1 / e1[1],
                      -pow(vs*dot(s,e1),2) / e1[1], 2*vs*dot(s,e1) / e1[1]};
#else  // 0.5*g*H^2 included in advective flux
#endif

  MatrixQ dfduTrue = 0;
  MatrixQ dgduTrue( dgdudata, 4 );
  MatrixQ dfdu = 0 , dgdu = 0;

  // advective flux jabocian
  pde.jacobianFluxAdvective(e1,x,y,t,var,dfdu,dgdu);

  for ( int i = 0; i < N; i++)
  {
    for ( int j = 0; j < N; j++)
    {
      SANS_CHECK_CLOSE( dfduTrue(i,j), dfdu(i,j), tol, tol );
      SANS_CHECK_CLOSE( dgduTrue(i,j), dgdu(i,j), tol, tol );
    }
  }

  // advective flux jacobian cumulation
  pde.jacobianFluxAdvective(e1,x,y,t,var,dfdu,dgdu);

  for ( int i = 0; i < N; i++)
  {
    for ( int j = 0; j < N; j++)
    {
      SANS_CHECK_CLOSE( 2*dfduTrue(i,j), dfdu(i,j), tol, tol );
      SANS_CHECK_CLOSE( 2*dgduTrue(i,j), dgdu(i,j), tol, tol );
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jacobianFluxAdvective_case3_test )
{
  typedef ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta SolutionClass;
  typedef VarTypeHVelocity2D_manifold VarType;
  typedef PhysD2 PhysDim;
  typedef PDEShallowWater_manifold<PhysDim, VarType, SolutionClass> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  const int N = PDEClass::N;

  typedef PDEClass::VectorX VectorX;

  const Real tol = 1.E-15;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 20;   // q0 = v*H
  const int p = 5;      // solution H polynomial degree
  const Real a = 3.4, b = 1.3;

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  solnArgs[SolutionClass::ParamsType::params.a] = a;
  solnArgs[SolutionClass::ParamsType::params.b] = b;

  PDEClass pde( solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  const Real x = 1.6;
  const Real y = 3.8;
  const Real t = 0.3;

  VectorX e1 = { 1, 0. };
  const VectorX e1x = { -1., 0.3 };
  const VectorX e1y = { 0.2, -0.1 };

  VectorX s;

  const Real theta = atan2(a*y,b*x);

  const Real d = sqrt( pow( -a*sin(theta), 2 ) + pow( b*cos(theta), 2 ) );
  s[0] = -a*sin(theta)/d;
  s[1] =  b*cos(theta)/d;

  Real H = 16.6, vs = 1.2;
  const VarType vardata(H, vs);
  ArrayQ var;
  pde.setDOFFrom(vardata, var);

  BOOST_CHECK( pde.isValidState(var) );

  // results
#if PDEShallowWaterManifold_sansgH2fluxAdvective2D  // 0.5*g*H^2 not included in advective flux
  Real dfdudata[4] = {0,                            1 / e1[0],
                      -pow(vs*dot(s,e1),2) / e1[0], 2*vs*dot(s,e1) / e1[0]};
#else  // 0.5*g*H^2 included in advective flux
#endif

  MatrixQ dfduTrue( dfdudata, 4 );
  MatrixQ dgduTrue = 0;
  MatrixQ dfdu = 0 , dgdu = 0;

  // advective flux jabocian
  pde.jacobianFluxAdvective(e1,x,y,t,var,dfdu,dgdu);

  for ( int i = 0; i < N; i++)
  {
    for ( int j = 0; j < N; j++)
    {
      SANS_CHECK_CLOSE( dfduTrue(i,j), dfdu(i,j), tol, tol );
      SANS_CHECK_CLOSE( dgduTrue(i,j), dgdu(i,j), tol, tol );
    }
  }

  // advective flux jacobian cumulation
  pde.jacobianFluxAdvective(e1,x,y,t,var,dfdu,dgdu);

  for ( int i = 0; i < N; i++)
  {
    for ( int j = 0; j < N; j++)
    {
      SANS_CHECK_CLOSE( 2*dfduTrue(i,j), dfdu(i,j), tol, tol );
      SANS_CHECK_CLOSE( 2*dgduTrue(i,j), dgdu(i,j), tol, tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( source_test )
{
  typedef ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta SolutionClass;
  typedef VarTypeHVelocity2D_manifold VarType;
  typedef PhysD2 PhysDim;
  typedef PDEShallowWater_manifold<PhysDim, VarType, SolutionClass> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  const int N = PDEClass::N;

  typedef PDEClass::VectorX VectorX;

  const Real tol = 1.E-12;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 20;   // q0 = v*H
  const int p = 5;      // solution H polynomial degree
  const Real a = 3.4, b = 1.3;

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  solnArgs[SolutionClass::ParamsType::params.a] = a;
  solnArgs[SolutionClass::ParamsType::params.b] = b;

  PDEClass pde( solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  const Real x = 1.6, y = 3.8;
  const Real t = 0.3;

  const Real theta = atan2(a*y,b*x);
  const Real d = sqrt( a*a*sin(theta)*sin(theta) + b*b*cos(theta)*cos(theta) );

  const VectorX e1 = { -a*sin(theta)/d, b*cos(theta)/d };
  const VectorX e1x = { 1.2, 0.2 };
  const VectorX e1y = { 0.05, -0.8 };

  Real H = 1 + theta + pow(theta,2) + pow(theta,3) + pow(theta,4) + pow(theta,5);
  Real H_x = (-a*sin(theta)/d)*( 1 + 2*theta + 3*pow(theta,2) + 4*pow(theta,3) + 5*pow(theta,4) )/d;
  Real H_y = ( b*cos(theta)/d)*( 1 + 2*theta + 3*pow(theta,2) + 4*pow(theta,3) + 5*pow(theta,4) )/d;

  Real vs = 1.2;
  Real vs_x = -0.3, vs_y = 2.3;

  const VarType vardata(H, vs);
  const VarType varxdata(H_x, vs_x);
  const VarType varydata(H_y, vs_y);

  ArrayQ var, varx, vary;
  pde.setDOFFrom(vardata, var);
  pde.setDOFFrom(varxdata, varx);
  pde.setDOFFrom(varydata, vary);

  BOOST_CHECK( pde.isValidState(var) );

  ArrayQ source = { 0., 0. };
  ArrayQ sourcetrue;

  Real b_x, b_y;
  b_x = ( 1 - q0*q0/(g*pow(H,3)) ) * H_x;
  b_y = ( 1 - q0*q0/(g*pow(H,3)) ) * H_y;

  // results
#if PDEShallowWaterManifold_sansgH2fluxAdvective2D  // 0.5*g*H^2 not included in advective flux
  VectorX gradex = { e1x[0], e1x[1] };
  VectorX gradey = { e1y[0], e1y[1] };
  VectorX v = { vs*e1[0], vs*e1[1] };
  VectorX gradzeta = { H_x-b_x, H_y-b_y };

  VectorX HvDotGrade;
  HvDotGrade[0] = H*dot(v,gradex);
  HvDotGrade[1] = H*dot(v,gradey);

  sourcetrue[0] = 0;
  sourcetrue[1] = dot(v,HvDotGrade) + g*H*dot(gradzeta,e1);
#else  // 0.5*g*H^2 included in advective flux
#endif

  // strong flux advective
  pde.source(e1,e1x,e1y,x,y,t,var,varx,vary,source);

  for ( int i = 0; i < N; i++ )
    SANS_CHECK_CLOSE( sourcetrue(i), source(i), tol, tol );

  // strong flux advective cumulation
  pde.source(e1,e1x,e1y,x,y,t,var,varx,vary,source);

  for ( int i = 0; i < N; i++ )
    SANS_CHECK_CLOSE( 2*sourcetrue(i), source(i), tol, tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState_test )
{

  typedef ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta SolutionClass;
  typedef VarTypeHVelocity2D_manifold VarType;
  typedef PhysD2 PhysDim;
  typedef PDEShallowWater_manifold<PhysDim, VarType, SolutionClass> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 20;   // q0 = v*H
  const int p = 5;      // solution H polynomial degree
  const Real a = 3.4, b = 1.3;

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  solnArgs[SolutionClass::ParamsType::params.a] = a;
  solnArgs[SolutionClass::ParamsType::params.b] = b;

  PDEClass pde( solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  Real H = 16.2;
  Real vs = 12.3;

  ArrayQ var;

  var = { H, vs };

  BOOST_CHECK( pde.isValidState(var) ); // valid state

  var(0) =  - 0.7;
  BOOST_CHECK( !( pde.isValidState(var) ) ); // invalid state: negative H
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setDOFFrom_test )
{
  typedef ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta SolutionClass;
  typedef VarTypeHVelocity2D_manifold VarType;
  typedef PhysD2 PhysDim;
  typedef PDEShallowWater_manifold<PhysDim, VarType, SolutionClass> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-15;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 20;   // q0 = v*H
  const int p = 5;      // solution H polynomial degree
  const Real a = 3.4, b = 1.3;

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  solnArgs[SolutionClass::ParamsType::params.a] = a;
  solnArgs[SolutionClass::ParamsType::params.b] = b;

  PDEClass pde( solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  Real H = 16.2;
  Real vs = -1.2;

  VarType vardata( H, vs );

  ArrayQ var;

  pde.setDOFFrom( vardata, var );

  SANS_CHECK_CLOSE( H,  var[0], tol, tol );
  SANS_CHECK_CLOSE( vs, var[1], tol, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
