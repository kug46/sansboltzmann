// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ShallowWaterSolution2D_manifold_btest
//   test of 2-D shallow water manifold solution class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/ShallowWater/ShallowWaterSolution2D_manifold.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ShallowWaterSolution2D_manifold_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta_p0_test )
{
  typedef ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;
  typedef SolutionClass::VectorX VectorX;

  const int VarDimTrue = 2;
  BOOST_CHECK( (ArrayQ::M == VarDimTrue) );

  const Real g = 9.81;
  const Real q0 = 18.6;
  const int p = 0;
  const Real a = 3.1;
  const Real b = 1.2;

  const Real tol = 1.e-13;
  Real x, y, time, theta, d;
  ArrayQ sln, slntrue;
  Real H, H_theta; // used in computing db/dtheta
  VectorX nablasb, nablasbtrue;

  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  solnArgs[SolutionClass::ParamsType::params.a] = a;
  solnArgs[SolutionClass::ParamsType::params.b] = b;
  SolutionClass fcn( solnArgs );

  SolutionClass::ParamsType::checkInputs(solnArgs);

  Real a_actual, b_actual;
  fcn.getab( a_actual, b_actual );
  SANS_CHECK_CLOSE( a, a_actual, tol, tol );
  SANS_CHECK_CLOSE( b, b_actual, tol, tol );

  // check
  x = 1;
  y = 0;
  time = 0;
  slntrue = { 1, q0/1 };

  sln = fcn(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );

  H = slntrue(0);
  H_theta = 0;
  nablasbtrue[0] = 0;
  nablasbtrue[1] = (1 - q0*q0/g/pow(H,3)) * H_theta;

  fcn.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  // check
  x = 0;
  y = 1;
  time = 0;
  slntrue = { 1, q0/1 };

  sln = fcn(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );

  H = slntrue(0);
  H_theta = 0;
  nablasbtrue[0] = -(1 - q0*q0/g/pow(H,3)) * H_theta;
  nablasbtrue[1] = 0;

  nablasb = fcn.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  // check
  theta = PI/4;
  x = a * cos(theta);
  y = b * sin(theta);
  d = sqrt( a*a*sin(theta)*sin(theta) + b*b*cos(theta)*cos(theta) );
  time = 0;
  slntrue = { 1, q0/1 };

  sln = fcn(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );

  H = slntrue(0);
  H_theta = 0;
  nablasbtrue[0] = (-a*sqrt(0.5)/d) * (1 - q0*q0/g/pow(H,3)) * H_theta / d;
  nablasbtrue[1] = ( b*sqrt(0.5)/d) * (1 - q0*q0/g/pow(H,3)) * H_theta / d;

  fcn.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta_p1_test )
{
  typedef ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;
  typedef SolutionClass::VectorX VectorX;

  const Real g = 9.81;
  const Real q0 = 18.6;
  const int p = 1;
  const Real a = 3.1;
  const Real b = 1.2;

  const Real tol = 1.e-13;
  Real x, y, time, theta, d;
  ArrayQ sln, slntrue;
  Real H, H_theta; // used in computing db/dtheta
  VectorX nablasb, nablasbtrue;

  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  solnArgs[SolutionClass::ParamsType::params.a] = a;
  solnArgs[SolutionClass::ParamsType::params.b] = b;
  SolutionClass fcn( solnArgs );

  SolutionClass::ParamsType::checkInputs(solnArgs);

  // check
  theta = PI/3;
  x = a * cos(theta);
  y = b * sin(theta);
  d = sqrt( a*a*sin(theta)*sin(theta) + b*b*cos(theta)*cos(theta) );
  time = 0;

  H = 1 + theta;
  H_theta = 1;
  slntrue = { H, q0/H };

  sln = fcn(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );

  nablasbtrue[0] = (-a*sin(theta)/d) * (1 - q0*q0/g/pow(H,3)) * H_theta / d;
  nablasbtrue[1] = ( b*cos(theta)/d) * (1 - q0*q0/g/pow(H,3)) * H_theta / d;

  fcn.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta_p5_test )
{
  typedef ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;
  typedef SolutionClass::VectorX VectorX;

  const Real g = 9.81;
  const Real q0 = 18.6;
  const int p = 5;
  const Real a = 3.1;
  const Real b = 1.2;

  const Real tol = 3.e-13;
  Real x, y, time, theta, d;
  ArrayQ sln, slntrue;
  Real H, H_theta; // used in computing db/dtheta
  VectorX nablasb, nablasbtrue;

  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  solnArgs[SolutionClass::ParamsType::params.a] = a;
  solnArgs[SolutionClass::ParamsType::params.b] = b;
  SolutionClass fcn( solnArgs );

  SolutionClass::ParamsType::checkInputs(solnArgs);

  // check
  theta = PI/5;
  x = a * cos(theta);
  y = b * sin(theta);
  d = sqrt( a*a*sin(theta)*sin(theta) + b*b*cos(theta)*cos(theta) );
  time = 0;

  H = 1 + theta + pow(theta,2) + pow(theta,3) + pow(theta,4) + pow(theta,5);
  H_theta = 1 + 2*theta + 3*pow(theta,2) + 4*pow(theta,3) + 5*pow(theta,4);
  slntrue = { H, q0/H };

  sln = fcn(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );

  nablasbtrue[0] = (-a*sin(theta)/d) * (1 - q0*q0/g/pow(H,3)) * H_theta / d;
  nablasbtrue[1] = ( b*cos(theta)/d) * (1 - q0*q0/g/pow(H,3)) * H_theta / d;

  fcn.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
