// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ShallowWaterSolution2D_btest
//   test of 2-D shallow water solution class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/ShallowWater/ShallowWaterSolution2D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ShallowWaterSolution2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction2D_CosineTheta_test )
{
  typedef ShallowWaterSolutionFunction2D_CosineTheta SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;
  typedef SolutionClass::VectorX VectorX;

  const Real g = 9.81;
  const Real q0 = 18.6;

  const Real tol = 1.e-13;
  Real x, y, time;
  ArrayQ sln;
  VectorX nablasb, nablasbtrue;

  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  SolutionClass fcn1( solnArgs );

  SolutionClass::ParamsType::checkInputs(solnArgs);

  // ArrayQ constructor
  SolutionClass fcn2( q0);

  // check
  x = 1;
  y = 0;
  time = 0;

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( 1.0, sln(0), tol, tol );
  BOOST_CHECK_SMALL( sln(1), tol );
  SANS_CHECK_CLOSE( q0/1.0, sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( 1.0, sln(0), tol, tol );
  BOOST_CHECK_SMALL( sln(1), tol );
  SANS_CHECK_CLOSE( q0/1.0, sln(2), tol, tol );

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( 0, nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( 0, nablasb[1], tol, tol );
//
  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( 0, nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( 0, nablasb[1], tol, tol );

  // check
  x = 1;
  y = 1;
  time = 0;

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( sqrt(0.5), sln(0), tol, tol );
  SANS_CHECK_CLOSE( -q0, sln(1), tol, tol );
  SANS_CHECK_CLOSE( q0, sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( sqrt(0.5), sln(0), tol, tol );
  SANS_CHECK_CLOSE( -q0, sln(1), tol, tol );
  SANS_CHECK_CLOSE( q0, sln(2), tol, tol );

  nablasbtrue[0] = -sqrt(0.5) * sqrt(0.5)/sqrt(2) *(-1+q0*q0/g/sqrt(0.125));
  nablasbtrue[1] =  sqrt(0.5) * sqrt(0.5)/sqrt(2) *(-1+q0*q0/g/sqrt(0.125));

  fcn2.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );
//
  nablasb = fcn1.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  // check
  x = 0.8;
  y = 0.6;
  time = 0;

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( 0.8, sln(0), tol, tol );
  SANS_CHECK_CLOSE( -0.75*q0, sln(1), tol, tol );
  SANS_CHECK_CLOSE( q0, sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( 0.8, sln(0), tol, tol );
  SANS_CHECK_CLOSE( -0.75*q0, sln(1), tol, tol );
  SANS_CHECK_CLOSE( q0, sln(2), tol, tol );

  nablasbtrue[0] = -0.6 * 0.6 * (-1 + q0*q0/g/pow(0.8,3) );
  nablasbtrue[1] =  0.8 * 0.6 * (-1 + q0*q0/g/pow(0.8,3) );

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction2D_GeometricSeriesx_p_smaller_than_0_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesx SolutionClass;
  typedef SolutionClass::VectorX VectorX;

  const Real g = 9.81;
  const Real q0 = 18.6;
  const int p = -1; // should throw an exception for p < 0

  Real x, y, t;

  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  SolutionClass fcn1( solnArgs );

  SolutionClass::ParamsType::checkInputs(solnArgs);

  // ArrayQ constructor
  SolutionClass fcn2( q0, p );

  x = 0;
  y = 0;
  t = 0;
  VectorX nablasb;

  // overloaded operator()
  BOOST_CHECK_THROW( fcn2(x,y,t);, SANSException );

  // compute bathymetry gradient
  BOOST_CHECK_THROW( fcn2.getBathymetryGrad(x,y,t,nablasb);, SANSException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction2D_GeometricSeriesx_p0_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesx SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;
  typedef SolutionClass::VectorX VectorX;

  const Real g = 9.81;
  const Real q0 = 18.6;
  const int p = 0;

  const Real tol = 1.e-13;
  Real x, y, time;
  ArrayQ sln, slntrue;
  Real H, H_x; // used in computing db/dx
  VectorX nablasb, nablasbtrue;

  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  SolutionClass fcn1( solnArgs );

  // ArrayQ constructor
  SolutionClass fcn2( q0, p );

  // check
  x = 1;
  time = 0;
  slntrue = { 1, q0/1, 0 };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  H = slntrue(0);
  H_x = 0;
  nablasbtrue = { (1 - q0*q0/g/pow(H,3)) * H_x, 0 };

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );
//
  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  // check
  x = 0.8;
  time = 0;

  H = 1;
  H_x = 0;
  slntrue = { H, q0/H, 0 };

  sln = fcn1(x,y,time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x,y,time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  nablasbtrue = { (1 - q0*q0/g/pow(H,3)) * H_x, 0 };

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );
//
  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction2D_GeometricSeriesx_p1_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesx SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;
  typedef SolutionClass::VectorX VectorX;

  const Real g = 9.81;
  const Real q0 = 18.6;
  const int p = 1;

  const Real tol = 1.e-13;
  Real x, y, time;
  ArrayQ sln, slntrue;
  Real H, H_x; // used in computing db/dx
  VectorX nablasb, nablasbtrue;

  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  SolutionClass fcn1( solnArgs );

  // ArrayQ constructor
  SolutionClass fcn2( q0, p );

  // check
  x = 1;
  y = 0;
  time = 0;
  slntrue = { 2, q0/2, 0 };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  H = slntrue(0);
  H_x = 1;
  nablasbtrue[0] = (1 - q0*q0/g/pow(H,3)) * H_x;
  nablasbtrue[1] = 0;

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );
//
  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  // check
  x = 1;
  y = 1;
  time = 0;
  slntrue = { 2, q0/2, 0 };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  H = slntrue(0);
  H_x = 1;
  nablasbtrue[0] = (1 - q0*q0/g/pow(H,3)) * H_x;
  nablasbtrue[1] = 0;

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  // check
  x = 0.8;
  y = 0.6;
  time = 0;

  H = 1 + x;
  H_x = 1;
  slntrue = { H, q0/H, 0 };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );


  nablasbtrue[0] = (1 - q0*q0/g/pow(H,3)) * H_x;
  nablasbtrue[1] = 0;

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction2D_GeometricSeriesx_p5_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesx SolutionClass;
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesx::template ArrayQ<Real> ArrayQ;
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesx::VectorX VectorX;

  const Real g = 9.81;
  const Real q0 = 18.6;
  const int p = 5;

  const Real tol = 1.e-12;
  Real x, y, time;
  ArrayQ sln;
  Real H, H_x; // used in computing db/dx
  VectorX nablasb, nablasbtrue;

  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  SolutionClass fcn1( solnArgs );

  // ArrayQ constructor
  SolutionClass fcn2( q0, p );

  // check
  x = 1;
  y = 0;
  time = 0;

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( 6.0, sln(0), tol, tol );
  SANS_CHECK_CLOSE( q0/6.0, sln(1), tol, tol );
  BOOST_CHECK_SMALL( sln(2), tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( 6.0, sln(0), tol, tol );
  SANS_CHECK_CLOSE( q0/6.0, sln(1), tol, tol );
  BOOST_CHECK_SMALL( sln(2), tol );

  H = 1 + x + pow(x,2) + pow(x,3) + pow(x,4) + pow(x,5);
  H_x = 1 + 2*x + 3*pow(x,2) + 4*pow(x,3) + 5*pow(x,4);
  nablasbtrue[0] = (1 - q0*q0/g/pow(H,3)) * H_x;
  nablasbtrue[1] = 0;

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  // check
  x = 1;
  y = 1;
  time = 0;

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( 6.0, sln(0), tol, tol );
  SANS_CHECK_CLOSE( q0/6.0, sln(1), tol, tol );
  BOOST_CHECK_SMALL( sln(2), tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( 6.0, sln(0), tol, tol );
  SANS_CHECK_CLOSE( q0/6.0, sln(1), tol, tol );
  BOOST_CHECK_SMALL( sln(2), tol );

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  // check
  x = 0.8;
  y = 0.6;
  time = 0;

  H = 1 + x + pow(x,2) + pow(x,3) + pow(x,4) + pow(x,5);
  H_x = 1 + 2*x + 3*pow(x,2) + 4*pow(x,3) + 5*pow(x,4);

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( H, sln(0), tol, tol );
  SANS_CHECK_CLOSE( q0/H, sln(1), tol, tol );
  SANS_CHECK_CLOSE( 0, sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( H, sln(0), tol, tol );
  SANS_CHECK_CLOSE( q0/H, sln(1), tol, tol );
  SANS_CHECK_CLOSE( 0, sln(2), tol, tol );


  nablasbtrue[0] = (1 - q0*q0/g/pow(H,3)) * H_x;
  nablasbtrue[1] = 0;

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction2D_GeometricSeriesSlopeRef_p_smaller_than_0_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesSlopeRef SolutionClass;
  typedef SolutionClass::VectorX VectorX;

  const Real g = 9.81;
  const Real q0 = 18.6;
  const int p = -1; // should throw an exception for p < 0

  Real x, y, t;

  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  SolutionClass fcn1( solnArgs );

  SolutionClass::ParamsType::checkInputs(solnArgs);

  // ArrayQ constructor
  SolutionClass fcn2( q0, p );

  x = 0;
  y = 0;
  t = 0;
  VectorX nablasb;

  // overloaded operator()
  BOOST_CHECK_THROW( fcn2(x,y,t);, SANSException );

  // compute bathymetry gradient
  BOOST_CHECK_THROW( fcn2.getBathymetryGrad(x,y,t,nablasb);, SANSException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction2D_GeometricSeriesSlopeRef_p0_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesSlopeRef SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;
  typedef SolutionClass::VectorX VectorX;

  const Real g = 9.81;
  const Real q0 = 18.6;
  const int p = 0;

  const Real tol = 1.e-13;
  Real x, y, time;
  ArrayQ sln, slntrue;
  Real H, H_s; // used in computing db/ds
  VectorX nablasb, nablasbtrue;

  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  SolutionClass fcn1( solnArgs );

  // ArrayQ constructor
  SolutionClass fcn2( q0, p );

  // check
  x = 1;
  y = 0;
  time = 0;
  slntrue = { 1, q0/1, 0 };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  H = slntrue(0);
  H_s = 0;
  nablasbtrue[0] = (1 - q0*q0/g/pow(H,3)) * H_s;
  nablasbtrue[1] = 0;

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  // check
  x = 1;
  y = 1;
  time = 0;
  slntrue = { 1, sqrt(0.5)*q0/1, sqrt(0.5)*q0/1 };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  H = slntrue(0);
  H_s = 0;
  nablasbtrue[0] = sqrt(0.5) * (1 - q0*q0/g/pow(H,3)) * H_s;
  nablasbtrue[1] = sqrt(0.5) * (1 - q0*q0/g/pow(H,3)) * H_s;

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  // check
  x = 0.8;
  y = 0.6;
  time = 0;

  H = 1;
  H_s = 0;
  slntrue = { H, 0.8*q0/H, 0.6*q0/H };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );


  nablasbtrue[0] = 0.8 * (1 - q0*q0/g/pow(H,3)) * H_s;
  nablasbtrue[1] = 0.6 * (1 - q0*q0/g/pow(H,3)) * H_s;

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  //  nablasb = fcn2.getBathymetryGrad(x,y,time);
  //  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  //  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction2D_GeometricSeriesSlopeRef_p1_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesSlopeRef SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;
  typedef SolutionClass::VectorX VectorX;

  const Real g = 9.81;
  const Real q0 = 18.6;
  const int p = 1;

  const Real tol = 1.e-13;
  Real x, y, time;
  ArrayQ sln, slntrue;
  Real H, H_s; // used in computing db/ds
  VectorX nablasb, nablasbtrue;

  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  SolutionClass fcn1( solnArgs );

  // ArrayQ constructor
  SolutionClass fcn2( q0, p );

  // check
  x = 1;
  y = 0;
  time = 0;
  slntrue = { 2, q0/2, 0 };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  H = slntrue(0);
  H_s = 1;
  nablasbtrue[0] = (1 - q0*q0/g/pow(H,3)) * H_s;
  nablasbtrue[1] = 0;

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  // check
  x = 1;
  y = 1;
  time = 0;
  slntrue = { 1+sqrt(2), sqrt(0.5)*q0/(1+sqrt(2)), sqrt(0.5)*q0/(1+sqrt(2)) };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  H = slntrue(0);
  H_s = 1;
  nablasbtrue[0] = sqrt(0.5) * (1 - q0*q0/g/pow(H,3)) * H_s;
  nablasbtrue[1] = sqrt(0.5) * (1 - q0*q0/g/pow(H,3)) * H_s;

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  // check
  x = 0.8;
  y = 0.6;
  time = 0;

  H = 1 + 1;
  H_s = 1;
  slntrue = { H, 0.8*q0/H, 0.6*q0/H };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );


  nablasbtrue[0] = 0.8 * (1 - q0*q0/g/pow(H,3)) * H_s;
  nablasbtrue[1] = 0.6 * (1 - q0*q0/g/pow(H,3)) * H_s;

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction2D_GeometricSeriesSlopeRef_p5_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesSlopeRef SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;
  typedef SolutionClass::VectorX VectorX;

  const Real g = 9.81;
  const Real q0 = 18.6;
  const int p = 5;

  const Real tol = 1.e-13;
  Real x, y, time, s;
  ArrayQ sln, slntrue;
  Real H, H_s; // used in computing db/ds
  VectorX nablasb, nablasbtrue;

  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  SolutionClass fcn1( solnArgs );

  // ArrayQ constructor
  SolutionClass fcn2( q0, p );

  // check
  x = 1;
  y = 0;
  time = 0;
  s = sqrt(x*x+y*y);

  slntrue = { 6.0, q0/6.0, 0 };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  H = 1 + s + pow(s,2) + pow(s,3) + pow(s,4) + pow(s,5);
  H_s = 1 + 2*s + 3*pow(s,2) + 4*pow(s,3) + 5*pow(s,4);

  nablasbtrue[0] = (1 - q0*q0/g/pow(H,3)) * H_s;
  nablasbtrue[1] = 0;

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  // check
  x = sqrt(0.5);
  y = sqrt(0.5);
  time = 0;
  s = sqrt(x*x+y*y);

  slntrue = { 6.0, sqrt(0.5)*q0/6.0, sqrt(0.5)*q0/6.0 };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  H = 1 + s + pow(s,2) + pow(s,3) + pow(s,4) + pow(s,5);
  H_s = 1 + 2*s + 3*pow(s,2) + 4*pow(s,3) + 5*pow(s,4);

  nablasbtrue[0] = sqrt(0.5) * (1 - q0*q0/g/pow(H,3)) * H_s;
  nablasbtrue[1] = sqrt(0.5) * (1 - q0*q0/g/pow(H,3)) * H_s;

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );
//
  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  // check
  x = 1.6;
  y = 1.2;
  time = 0;
  s = sqrt(x*x+y*y);

  H = 1 + s + pow(s,2) + pow(s,3) + pow(s,4) + pow(s,5);
  H_s = 1 + 2*s + 3*pow(s,2) + 4*pow(s,3) + 5*pow(s,4);

  slntrue = { H, 0.8*q0/H, 0.6*q0/H };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );


  nablasbtrue[0] = 0.8*(1 - q0*q0/g/pow(H,3)) * H_s;
  nablasbtrue[1] = 0.6*(1 - q0*q0/g/pow(H,3)) * H_s;

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction2D_GeometricSeriesTheta_p_smaller_than_0_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesTheta SolutionClass;
  typedef SolutionClass::VectorX VectorX;

  const Real g = 9.81;
  const Real q0 = 18.6;
  const int p = -1; // should throw an exception for p < 0

  Real x, y, t;

  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  SolutionClass fcn1( solnArgs );

  SolutionClass::ParamsType::checkInputs(solnArgs);

  // ArrayQ constructor
  SolutionClass fcn2( q0, p );

  x = 0;
  y = 0;
  t = 0;
  VectorX nablasb;

  // overloaded operator()
  BOOST_CHECK_THROW( fcn2(x,y,t);, SANSException );

  // compute bathymetry gradient
  BOOST_CHECK_THROW( fcn2.getBathymetryGrad(x,y,t,nablasb);, SANSException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction2D_GeometricSeriesTheta_p0_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesTheta SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;
  typedef SolutionClass::VectorX VectorX;

  const Real g = 9.81;
  const Real q0 = 18.6;
  const int p = 0;

  const Real tol = 1.e-13;
  Real x, y, time;
  ArrayQ sln, slntrue;
  Real H, H_theta; // used in computing db/dtheta
  VectorX nablasb, nablasbtrue;

  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  SolutionClass fcn1( solnArgs );

  // ArrayQ constructor
  SolutionClass fcn2( q0, p );

  // check
  x = 1;
  y = 0;
  time = 0;
  slntrue = { 1, 0, q0/1 };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  H = slntrue(0);
  H_theta = 0;
  nablasbtrue[0] = 0;
  nablasbtrue[1] = (1 - q0*q0/g/pow(H,3)) * H_theta;

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  // check
  x = 0;
  y = 1;
  time = 0;
  slntrue = { 1, -q0/1, 0 };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  H = slntrue(0);
  H_theta = 0;
  nablasbtrue[0] = -(1 - q0*q0/g/pow(H,3)) * H_theta;
  nablasbtrue[1] = 0;

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  // check
  x = 1;
  y = 1;
  time = 0;
  slntrue = { 1, -sqrt(0.5)*q0/1, sqrt(0.5)*q0/1 };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  H = slntrue(0);
  H_theta = 0;
  nablasbtrue[0] = -sqrt(0.5) * (1 - q0*q0/g/pow(H,3)) * H_theta / sqrt(2);
  nablasbtrue[1] =  sqrt(0.5) * (1 - q0*q0/g/pow(H,3)) * H_theta / sqrt(2);

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction2D_GeometricSeriesTheta_p1_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesTheta SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;
  typedef SolutionClass::VectorX VectorX;

  const Real g = 9.81;
  const Real q0 = 18.6;
  const int p = 1;

  const Real tol = 1.e-13;
  Real x, y, time;
  ArrayQ sln, slntrue;
  Real H, H_theta; // used in computing db/dtheta
  VectorX nablasb, nablasbtrue;

  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  SolutionClass fcn1( solnArgs );

  // ArrayQ constructor
  SolutionClass fcn2( q0, p );

  // check
  x = 1;
  y = 0;
  time = 0;
  slntrue = { 1, 0, q0/1 };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  H = slntrue(0);
  H_theta = 1;
  nablasbtrue[0] = 0;
  nablasbtrue[1] = (1 - q0*q0/g/pow(H,3)) * H_theta;

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  // check
  x = 0;
  y = 1;
  time = 0;
  slntrue = { 1 + PI/2, -q0/(1 + PI/2), 0 };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  H = slntrue(0);
  H_theta = 1;
  nablasbtrue[0] = -(1 - q0*q0/g/pow(H,3)) * H_theta;
  nablasbtrue[1] = 0;

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  // check
  x = 1;
  y = 1;
  time = 0;
  slntrue = { 1+PI/4, -sqrt(0.5)*q0/(1+PI/4), sqrt(0.5)*q0/(1+PI/4) };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  H = slntrue(0);
  H_theta = 1;
  nablasbtrue[0] = -sqrt(0.5) * (1 - q0*q0/g/pow(H,3)) * H_theta / sqrt(2);
  nablasbtrue[1] =  sqrt(0.5) * (1 - q0*q0/g/pow(H,3)) * H_theta / sqrt(2);

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  // check
  x = cos(PI/8);
  y = sin(PI/8);
  time = 0;

  H = 1 + PI/8;
  H_theta = 1;
  slntrue = { H, -sin(PI/8)*q0/H, cos(PI/8)*q0/H };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );


  nablasbtrue[0] = -sin(PI/8) * (1 - q0*q0/g/pow(H,3)) * H_theta;
  nablasbtrue[1] =  cos(PI/8) * (1 - q0*q0/g/pow(H,3)) * H_theta;

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction2D_GeometricSeriesTheta_p5_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesTheta SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;
  typedef SolutionClass::VectorX VectorX;

  const Real g = 9.81;
  const Real q0 = 18.6;
  const int p = 5;

  const Real tol = 2.e-13;
  Real x, y, time, theta, r;
  ArrayQ sln, slntrue;
  Real H, H_theta; // used in computing db/dtheta
  VectorX nablasb, nablasbtrue;

  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  SolutionClass fcn1( solnArgs );

  // ArrayQ constructor
  SolutionClass fcn2( q0, p );

  // check
  x = 1;
  y = 0;
  time = 0;

  slntrue = { 1, 0, q0/1 };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  H = slntrue(0);
  H_theta = 1 + 0 + 0 + 0 + 0;

  nablasbtrue[0] = 0;
  nablasbtrue[1] = (1 - q0*q0/g/pow(H,3)) * H_theta;

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  // check
  x = sqrt(0.5);
  y = sqrt(0.5);
  time = 0;
  theta = PI/4;

  H = 1 + theta + pow(theta,2) + pow(theta,3) + pow(theta,4) + pow(theta,5);

  slntrue = { H, -sqrt(0.5)*q0/H, sqrt(0.5)*q0/H };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  H_theta = 1 + 2*theta + 3*pow(theta,2) + 4*pow(theta,3) + 5*pow(theta,4);

  nablasbtrue[0] = -sqrt(0.5) * (1 - q0*q0/g/pow(H,3)) * H_theta;
  nablasbtrue[1] =  sqrt(0.5) * (1 - q0*q0/g/pow(H,3)) * H_theta;

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  // check
  x = 1.6;
  y = 1.2;
  time = 0;
  theta = atan2(y,x);
  r = sqrt(x*x+y*y);

  H = 1 + theta + pow(theta,2) + pow(theta,3) + pow(theta,4) + pow(theta,5);
  H_theta = 1 + 2*theta + 3*pow(theta,2) + 4*pow(theta,3) + 5*pow(theta,4);

  slntrue = { H, -0.6*q0/H, 0.8*q0/H };

  sln = fcn1(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );

  sln = fcn2(x, y, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );
  SANS_CHECK_CLOSE( slntrue(2), sln(2), tol, tol );


  nablasbtrue[0] = -sin(theta)*(1 - q0*q0/g/pow(H,3)) * H_theta / r;
  nablasbtrue[1] =  cos(theta)*(1 - q0*q0/g/pow(H,3)) * H_theta / r;

  fcn1.getBathymetryGrad(x,y,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,y,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
  SANS_CHECK_CLOSE( nablasbtrue[1], nablasb[1], tol, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
