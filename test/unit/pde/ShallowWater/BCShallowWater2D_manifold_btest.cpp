// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCShallowWater2D_manifold_btest
//
// test of 2-D shallow water manifold BC classes

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/ShallowWater/BCShallowWater2D_manifold.h"

#define BCPARAMETERS_INSTANTIATE // included because testing without NDConvert
#include "pde/BCParameters_impl.h"

//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
template class BCShallowWater2D_manifold< BCTypeInflowSupercritical,
                                          PDEShallowWater_manifold<PhysD2,VarTypeHVelocity2D_manifold,
                                                                   ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta
                                                                  >
                                        >;
//
// Instantiate BCParamaters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters< BCShallowWater2DVector_manifold<VarTypeHVelocity2D_manifold,
                                                              ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta> >;
}

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BCShallowWater2D_manifold_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PhysD2 PhysDim;
  typedef VarTypeHVelocity2D_manifold VarType;
  typedef ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta SolutionClass;
  typedef PDEShallowWater_manifold<PhysDim,VarType,SolutionClass> PDEClass;

  // BCTypeInflowSupercritical
  {
    typedef BCShallowWater2D_manifold< BCTypeInflowSupercritical, PDEClass > BCClass;
    typedef BCClass::ArrayQ<Real> ArrayQ;
    typedef BCClass::MatrixQ<Real> MatrixQ;

    BOOST_CHECK( BCClass::D == 2 );
    BOOST_CHECK( BCClass::N == 2 );
    BOOST_CHECK( BCClass::NBC == 2 );
    BOOST_CHECK( ArrayQ::M == 2 );
    BOOST_CHECK( MatrixQ::M == 2 );
    BOOST_CHECK( MatrixQ::N == 2 );
  }

  // BCNone
  {
    typedef BCNone<PhysD2,PDEClass::N> BCClass;
    typedef BCClass::ArrayQ<Real> ArrayQ;
    typedef BCClass::MatrixQ<Real> MatrixQ;

    BOOST_CHECK( BCClass::D == 2 );
    BOOST_CHECK( BCClass::N == 2 );
    BOOST_CHECK( BCClass::NBC == 0 );
    BOOST_CHECK( ArrayQ::M == 2 );
    BOOST_CHECK( MatrixQ::M == 2 );
    BOOST_CHECK( MatrixQ::N == 2 );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_test )
{
  typedef PhysD2 PhysDim;
  typedef VarTypeHVelocity2D_manifold VarType;
  typedef ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta SolutionClass;
  typedef PDEShallowWater_manifold<PhysDim,VarType,SolutionClass> PDEClass;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 20;   // q0 = v*H
  const int p = 8;
  const Real a = 10.2, b = 2.3;

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  solnArgs[SolutionClass::ParamsType::params.a] = a;
  solnArgs[SolutionClass::ParamsType::params.b] = b;

  PDEClass pde( solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  // BCTypeInflowSupercritical
  {
    typedef BCShallowWater2D_manifold< BCTypeInflowSupercritical, PDEClass > BCClass;
    typedef BCShallowWater2DParams_manifold<BCTypeInflowSupercritical> BCParamsType;
    typedef BCClass::ArrayQ<Real> ArrayQ;
    typedef boost::mpl::vector< BCShallowWater2D_manifold< BCTypeInflowSupercritical, PDEClass > > BCVectorClass;
    typedef BCParameters< BCVectorClass > BCParams;

    const ArrayQ bcdata = { 0, 0 };

    // ArrayQ constructor
    BCClass bc1(pde,bcdata);

    // Pydic constructor
    PyDict BCInflowSupercriticalArgs;
    BCInflowSupercriticalArgs[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSupercritical;
    BCInflowSupercriticalArgs[BCParamsType::params.H] = 2.1;
    BCInflowSupercriticalArgs[BCParamsType::params.vs] = -1.2;

    PyDict PyBCList;
    PyBCList["BCInflow"] = BCInflowSupercriticalArgs;

    BCParams::checkInputs(PyBCList);

    BCClass bc2(pde, BCInflowSupercriticalArgs);
  }

  // BCNone
  {
    typedef BCNone<PhysD2,PDEClass::N> BCClass;

    BCClass bc1;

    // Pydic constructor
    PyDict BCNoneArgs;

    BCClass bc2(pde, BCNoneArgs);
  }
}

#if PDEShallowWaterManifold2D_manifold
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_InflowSupercritical_test )
{
  typedef PhysD2 PhysDim;
  typedef VarTypeHVelocity2D_manifold VarType;
  typedef ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta SolutionClass;
  typedef PDEShallowWater_manifold<PhysDim,VarType,SolutionClass> PDEClass;

  typedef BCShallowWater2D_manifold< BCTypeInflowSupercritical, PDEClass > BCClass;

  typedef PDEClass::VectorX VectorX;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 20;   // q0 = v*H
  const int p = 5;
  const Real a = 10.2, b = 2.3;

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  solnArgs[SolutionClass::ParamsType::params.a] = a;
  solnArgs[SolutionClass::ParamsType::params.b] = b;

  PDEClass pde( solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  const ArrayQ bcdata = { 7.2, -1.2 };
  BCClass bc(pde,bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 2 );
  BOOST_CHECK( bc.NBC == 2 );

  const Real theta = PI/8;
  Real x = a*cos(theta), y = b*sin(theta), t = 0;
  Real nx, ny;
  Real H, vs;

  VectorX e1;

  const Real d = sqrt( pow( -a*sin(theta), 2 ) + pow( b*cos(theta), 2 ) );
  e1[0] = -a*sin(theta)/d;
  e1[1] =  b*cos(theta)/d;

  nx = 0.4; ny = 0.9;
  Real n = sqrt(nx*nx+ny*ny);
  nx = nx / n;
  ny = ny / n;

  BOOST_CHECK_CLOSE( nx*nx+ny*ny, 1, tol );

  H = 3.9;
  vs = 4.8;

  ArrayQ q;
  ArrayQ qx, qy; // dummy
  VarType qdata( H, vs );
  pde.setDOFFrom( qdata, q );
  pde.setDOFFrom( qdata, qx );
  pde.setDOFFrom( qdata, qy );

  // strong-form BC
  ArrayQ rsdBCTrue = { H - bcdata(0), vs - bcdata(1) };

  ArrayQ rsdBC = 0;
  bc.strongBC( x, y, t, nx, ny, q, qx, qy, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), tol, tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), tol, tol );

  // BC weight (B^t)
#if PDEShallowWaterManifold_sansgH2fluxAdvective2D  // 0.5*g*H^2 not included in advective flux
  Real dfdudata[4] = { 0,            1/e1[0],
                       -vs*vs/e1[0], 2*vs/e1[0] };
  Real dgdudata[4] = { 0,            1/e1[1],
                       -vs*vs/e1[1], 2*vs/e1[1] };
#else  // 0.5*g*H^2 included in advective flux
#endif

  Real wghtBCdata[4];
  for (int n = 0; n < 4; n++)
    wghtBCdata[n] = nx*dfdudata[n] + ny*dgdudata[n];

  MatrixQ wghtBCTrue( wghtBCdata, 4 );
  MatrixQ wghtBC = 0;

  bc.weightBC( e1, x, y, t, nx, ny, q, qx, qy, wghtBC );
  SANS_CHECK_CLOSE( wghtBCTrue(0,0), wghtBC(0,0), tol, tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,1), wghtBC(0,1), tol, tol );

  SANS_CHECK_CLOSE( wghtBCTrue(1,0), wghtBC(1,0), tol, tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,1), wghtBC(1,1), tol, tol );
}
#endif


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
