// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ShallowWaterVarInterp2D_btest
//
// test of 2-D shallow water PDE interpreter class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/ShallowWater/VarInterp2D_manifold.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( VarInterp2D_manifold_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_ctor_test )
{
  // true
  const int VarDimTrue = 2;

  // check
  BOOST_CHECK( VarInterpret2D_manifold<VarTypeHVelocity2D_manifold>::ArrayQ<Real>::M == VarDimTrue );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( get_test )
{
  typedef VarInterpret2D_manifold<VarTypeHVelocity2D_manifold>::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-15;

  VarInterpret2D_manifold<VarTypeHVelocity2D_manifold> varInterpret;

  Real H = 16.2, vs = 0.8;
  const ArrayQ var = { H, vs };

  varInterpret.getH( var, H );
  SANS_CHECK_CLOSE( var[0], H, tol, tol );

  varInterpret.getVelocity( var, vs );
  SANS_CHECK_CLOSE( var[1], vs, tol, tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState_test )
{
  typedef VarInterpret2D_manifold<VarTypeHVelocity2D_manifold>::ArrayQ<Real> ArrayQ;

  VarInterpret2D_manifold<VarTypeHVelocity2D_manifold> varInterpret;

  Real H = 16.2;
  Real vs = 0.8;

  ArrayQ var;

  var = { H, vs };

  BOOST_CHECK( varInterpret.isValidState(var) ); // valid state

  var(0) =  - 0.7;
  BOOST_CHECK( !( varInterpret.isValidState(var) ) ); // invalid state: negative H
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setFromPrimitive_VarTypeHVelocity2D_manifold_test )
{
  const int VarDimTrue = 2;

  typedef VarInterpret2D_manifold<VarTypeHVelocity2D_manifold>::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-15;

  VarInterpret2D_manifold<VarTypeHVelocity2D_manifold> varInterpret;

  BOOST_CHECK( VarInterpret2D_manifold<VarTypeHVelocity2D_manifold>::ArrayQ<Real>::M == VarDimTrue );


  Real H = 16.2;
  Real vs = 0.8;

  VarTypeHVelocity2D_manifold vardata( H, vs );

  ArrayQ var;

  varInterpret.setFromPrimitive( vardata, var);

  SANS_CHECK_CLOSE( H,  var[0], tol, tol );
  SANS_CHECK_CLOSE( vs, var[1], tol, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
