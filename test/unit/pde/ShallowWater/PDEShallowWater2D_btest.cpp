// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEShallowWater2D_btest
//
// test of 2-D shallow water PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/ShallowWater/PDEShallowWater2D.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEShallowWater2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef ShallowWaterSolutionFunction2D_CosineTheta SolutionClass;
  typedef PDEShallowWater<PhysD2, VarTypeHVelocity2D, SolutionClass> PDEClass;
  // true
  const int PhysDimTrue = 2;
  const int VarDimTrue = 3;

  // check
  BOOST_CHECK( (PDEClass::D == PhysDimTrue) );
  BOOST_CHECK( (PDEClass::N == VarDimTrue) );
  BOOST_CHECK( (PDEClass::ArrayQ<Real>::M == VarDimTrue) );
  BOOST_CHECK( (PDEClass::MatrixQ<Real>::M == VarDimTrue) );
  BOOST_CHECK( (PDEClass::MatrixQ<Real>::N == VarDimTrue) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctorsANDhas_test )
{
  typedef ShallowWaterSolutionFunction2D_CosineTheta SolutionClass;
  typedef PDEShallowWater<PhysD2, VarTypeHVelocity2D, SolutionClass> PDEClass;

  const Real tol = 1.e-15;

  // true
  const int PhysDimTrue = 2;
  const int VarDimTrue = 3;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 20;   // q0 = v*H

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;

  // actual: test constructor
  PDEClass pde( q0, solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );
  SANS_CHECK_CLOSE( g, pde.getg(), tol, tol );


  // check dimensions
  BOOST_CHECK( pde.D == PhysDimTrue );
  BOOST_CHECK( pde.N == VarDimTrue );

  // hasFLux functions
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

#if PDEShallowWater_sansgH2fluxAdvective2D
  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );
#else
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );
#endif

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( masterState_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD2, VarTypeHVelocity2D, SolutionClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  const int N = PDEClass::N;

  const Real tol = 3.E-14;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 20;   // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  PDEClass pde( q0, solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  // used
  Real H = 16.6, vx = -0.5, vy = 0.7;
  const VarTypeHVelocity2D vardata(H, vx, vy);
  ArrayQ var;
  pde.setDOFFrom(vardata, var);

  BOOST_CHECK( pde.isValidState(var) );

  ArrayQ uConsTrue = { H, H*vx, H*vy };
  ArrayQ uCons;

  Real x = 0, y = 0, time = 0;

  // results
  pde.masterState(x, y, time, var,uCons);

  for ( int i = 0; i < N; i++ )
    SANS_CHECK_CLOSE( uConsTrue[i], uCons[i], tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxAdvective_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD2, VarTypeHVelocity2D, SolutionClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  const int N = PDEClass::N;

  const Real tol = 3.E-14;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 20;   // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  PDEClass pde( q0, solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  // unused
  const Real x = 1.6;
  const Real y = 3.8;
  const Real t = 0.3;
  // used
  Real H = 16.6, vx = -0.5, vy = 0.7;
  const VarTypeHVelocity2D vardata(H, vx, vy);
  ArrayQ var;
  pde.setDOFFrom(vardata, var);

  BOOST_CHECK( pde.isValidState(var) );

  ArrayQ fx = { 0., 0., 0. };
  ArrayQ fy = { 0., 0., 0. };

  // results
#if PDEShallowWater_sansgH2fluxAdvective2D  // 0.5*g*H^2 not included in advective flux
  DLA::VectorS<N,Real> fxtrue = { H*vx, H*vx*vx, H*vy*vx };
  DLA::VectorS<N,Real> fytrue = { H*vy, H*vx*vy, H*vy*vy };
#else  // 0.5*g*H^2 included in advective flux

#if ShallowWater_IsCircle_NotFlat
  Real theta = atan2(y,x);

  DLA::VectorS<N,Real> fxtrue = { H*vx,
                                  H*vx*vx + 0.5*g*H*H*(-sin(theta)) * (-sin(theta)),
                                  H*vy*vx + 0.5*g*H*H*(-sin(theta)) * (cos(theta)) };
  DLA::VectorS<N,Real> fytrue = { H*vy,
                                  H*vx*vy + 0.5*g*H*H*(-sin(theta)) * (cos(theta)),
                                  H*vy*vy + 0.5*g*H*H*(cos(theta)) * (cos(theta)) };
#else
  DLA::VectorS<N,Real> fxtrue = { H*vx, H*vx*vx + 0.5*g*H*H, H*vy*vx };
  DLA::VectorS<N,Real> fytrue = { H*vy, H*vx*vy,             H*vy*vy + 0.5*g*H*H };

// Alternative:
//  Real theta = atan2(y,x);
//
//  DLA::VectorS<N,Real> fxtrue = { H*vx,
//                                  H*vx*vx + 0.5*g*H*H*(cos(theta)) * (cos(theta)),
//                                  H*vy*vx + 0.5*g*H*H*(cos(theta)) * (sin(theta)) };
//  DLA::VectorS<N,Real> fytrue = { H*vy,
//                                  H*vx*vy + 0.5*g*H*H*(sin(theta)) * (cos(theta)),
//                                  H*vy*vy + 0.5*g*H*H*(sin(theta)) * (sin(theta)) };
#endif

#endif
  // Advective flux
#if ShallowWaterIntegrandManifold2D // use integrand_galerkin_manifold
  typedef PDEClass::VectorEIF VectorEIF;
  typedef PDEClass::VectorX VectorX;

  const VectorEIF EIF = { -1, -1, -1, -1 };
  const VectorX e1 = { 1., 0. };
  const VectorX e1x = { 0., 0. };
  const VectorX e1y = { 0., 0. };

  pde.fluxAdvective(EIF, e1, e1x, e1y,x,y,t,var,fx,fy);
#else
  pde.fluxAdvective(x,y,t,var,fx,fy);
#endif

  for ( int i = 0; i < N; i++ )
  {
    SANS_CHECK_CLOSE( fxtrue[i], fx[i], tol, tol );
    SANS_CHECK_CLOSE( fytrue[i], fy[i], tol, tol );
  }

  // Advective flux cumulation
#if ShallowWaterIntegrandManifold2D // use integrand_galerkin_manifold
  pde.fluxAdvective(EIF, e1, e1x, e1y,x,y,t,var,fx,fy);
#else
  pde.fluxAdvective(x,y,t,var,fx,fy);
#endif
  for ( int i = 0; i < N; i++ )
  {
    SANS_CHECK_CLOSE( 2*fxtrue[i], fx[i], tol, tol );
    SANS_CHECK_CLOSE( 2*fytrue[i], fy[i], tol, tol );
  }

  // Advective flux in time
  ArrayQ ftTrue = { H, H*vx, H*vy };
  ArrayQ ft = 0;
  pde.fluxAdvectiveTime(x, y, t, var, ft);

  for ( int i = 0; i < N; i++ )
    SANS_CHECK_CLOSE( ftTrue[i], ft[i], tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxAdvectiveUpwind_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD2, VarTypeHVelocity2D, SolutionClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  const int N = PDEClass::N;

  const Real tol = 2.E-13;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 15;   // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  PDEClass pde( q0, solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  // unused
  const Real x = 1.6;
  const Real y = 3.8;
  const Real t = 0.3;

  // used
  Real HL = 16.6, vxL = -0.5, vyL = 0.7;
  Real HR = 3.7,  vxR = 0.3,  vyR = 1.3;

  Real nx = 0.6, ny = 0.8;
//  nx = nx / sqrt(nx*nx + ny*ny);
//  ny = ny / sqrt(nx*nx + ny*ny);

  SANS_CHECK_CLOSE( 1., nx*nx + ny*ny, tol, tol );

  const VarTypeHVelocity2D vardataL(HL, vxL, vyL);
  const VarTypeHVelocity2D vardataR(HR, vxR, vyR);

  ArrayQ varL, varR;

  pde.setDOFFrom(vardataL, varL);
  pde.setDOFFrom(vardataR, varR);

  BOOST_CHECK( pde.isValidState(varL) );
  BOOST_CHECK( pde.isValidState(varR) );

  ArrayQ f = { 0., 0., 0. };
  ArrayQ ftrue;

#if PDEShallowWater_sansgH2fluxAdvective2D  // 0.5*g*H^2 not included in advective flux
  Real lambdaAbsMaxL, lambdaAbsMaxR; // largest eigenvalue in absolute value of flux jacobians
  Real alpha;

  lambdaAbsMaxL = fabs(vxL*nx+vyL*ny);
  lambdaAbsMaxR = fabs(vxR*nx+vyR*ny);
  alpha = std::max( lambdaAbsMaxL, lambdaAbsMaxR );

  ftrue(0) = 0.5*( (HL*vxL + HR*vxR)*nx + (HL*vyL + HR*vyR)*ny ) + 0.5*(HL-HR)*alpha;
  ftrue(1) = 0.5*( (HL*vxL*vxL + HR*vxR*vxR)*nx + (HL*vxL*vyL + HR*vxR*vyR)*ny )
           + 0.5*( HL*vxL - HR*vxR )*alpha;
  ftrue(2) = 0.5*( (HL*vyL*vxL + HR*vyR*vxR)*nx + (HL*vyL*vyL + HR*vyR*vyR)*ny )
           + 0.5*( HL*vyL - HR*vyR )*alpha;
#else  // 0.5*g*H^2 included in advective flux
  Real lambdaAbsMaxL, lambdaAbsMaxR; // largest eigenvalue in absolute value of flux jacobians
  Real alpha;

  lambdaAbsMaxL = fabs(vxL*nx+vyL*ny) + sqrt(g*HL);
  lambdaAbsMaxR = fabs(vxR*nx+vyR*ny) + sqrt(g*HR);

  alpha = std::max( lambdaAbsMaxL, lambdaAbsMaxR );

#if ShallowWater_IsCircle_NotFlat
  Real theta = atan2(y,x);

  ftrue(0) = 0.5*( (HL*vxL + HR*vxR)*nx + (HL*vyL + HR*vyR)*ny ) + 0.5*(HL-HR)*alpha;
  ftrue(1) = 0.5*( (HL*vxL*vxL - sin(theta) * 0.5*g*HL*HL * (-sin(theta)) + HR*vxR*vxR - sin(theta) * 0.5*g*HR*HR * (-sin(theta)))*nx
                 + (HL*vxL*vyL - sin(theta) * 0.5*g*HL*HL *   cos(theta)  + HR*vxR*vyR - sin(theta) * 0.5*g*HR*HR *   cos(theta) )*ny )
           + 0.5*( HL*vxL - HR*vxR )*alpha;
  ftrue(2) = 0.5*( (HL*vyL*vxL + cos(theta) * 0.5*g*HL*HL * (-sin(theta)) + HR*vyR*vxR + cos(theta) * 0.5*g*HR*HR * (-sin(theta)))*nx
                 + (HL*vyL*vyL + cos(theta) * 0.5*g*HL*HL *   cos(theta)  + HR*vyR*vyR + cos(theta) * 0.5*g*HR*HR *   cos(theta) )*ny )
           + 0.5*( HL*vyL - HR*vyR )*alpha;
#else
  ftrue(0) = 0.5*( (HL*vxL + HR*vxR)*nx + (HL*vyL + HR*vyR)*ny ) + 0.5*(HL-HR)*alpha;
  ftrue(1) = 0.5*( (HL*vxL*vxL + 0.5*g*HL*HL + HR*vxR*vxR + 0.5*g*HR*HR)*nx + (HL*vxL*vyL + HR*vxR*vyR)*ny )
                       + 0.5*( HL*vxL - HR*vxR )*alpha;
  ftrue(2) = 0.5*( (HL*vyL*vxL + HR*vyR*vxR)*nx + (HL*vyL*vyL + 0.5*g*HL*HL + HR*vyR*vyR + 0.5*g*HR*HR)*ny )
                       + 0.5*( HL*vyL - HR*vyR )*alpha;

// Alternative
//  Real theta = atan2(y,x);
//
//  ftrue(0) = 0.5*( (HL*vxL + HR*vxR)*nx + (HL*vyL + HR*vyR)*ny ) + 0.5*(HL-HR)*alpha;
//  ftrue(1) = 0.5*( (HL*vxL*vxL + cos(theta) * 0.5*g*HL*HL * (cos(theta)) + HR*vxR*vxR + cos(theta) * 0.5*g*HR*HR * (cos(theta)))*nx
//                 + (HL*vxL*vyL + cos(theta) * 0.5*g*HL*HL *  sin(theta)  + HR*vxR*vyR + cos(theta) * 0.5*g*HR*HR *  sin(theta) )*ny )
//           + 0.5*( HL*vxL - HR*vxR )*alpha;
//  ftrue(2) = 0.5*( (HL*vyL*vxL + sin(theta) * 0.5*g*HL*HL * (cos(theta)) + HR*vyR*vxR + sin(theta) * 0.5*g*HR*HR * (cos(theta)))*nx
//                 + (HL*vyL*vyL + sin(theta) * 0.5*g*HL*HL *  sin(theta)  + HR*vyR*vyR + sin(theta) * 0.5*g*HR*HR *  sin(theta) )*ny )
//           + 0.5*( HL*vyL - HR*vyR )*alpha;
#endif

#endif

  // upwind advective flux
  pde.fluxAdvectiveUpwind(x,y,t,varL,varR,nx,ny,f);
  for ( int i = 0; i < N; i++ )
  {
    SANS_CHECK_CLOSE( ftrue[i], f[i], tol, tol );
  }

  // Advective flux cumulation
  pde.fluxAdvectiveUpwind(x,y,t,varL,varR,nx,ny,f);
  for ( int i = 0; i < N; i++ )
  {
    SANS_CHECK_CLOSE( 2*ftrue[i], f[i], tol, tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jacobianFluxAdvective_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD2, VarTypeHVelocity2D, SolutionClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;

  const int N = PDEClass::N;

  const Real tol = 1.E-13;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 2;    // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  PDEClass pde( q0, solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  // unused
  const Real x = 1.6;
  const Real y = 3.8;
  const Real t = 0.3;
  // used
  Real H = 16.6, vx = -0.5, vy = 0.7;
  const VarTypeHVelocity2D vardata(H, vx, vy);
  ArrayQ var;
  pde.setDOFFrom(vardata, var);

  BOOST_CHECK( pde.isValidState(var) );

  // results
#if PDEShallowWater_sansgH2fluxAdvective2D  // 0.5*g*H^2 not included in advective flux
  Real dfdudata[9] = {0,            1,    0,
                      -vx*vx,       2*vx, 0,
                      -vx*vy,       vy,   vx};
  Real dgdudata[9] = {0,            0,    1,
                      -vx*vy,       vy,  vx,
                      -vy*vy,       0,   2*vy};
  MatrixQ dfduTrue( dfdudata, 9 );
  MatrixQ dgduTrue( dgdudata, 9 );
  MatrixQ dfdu, dgdu;
  dfdu = 0;  dgdu = 0;
#else  // 0.5*g*H^2 included in advective flux

#if ShallowWater_IsCircle_NotFlat
  Real theta = atan2(y,x);

  Real dfdudata[9] = { 0, 1, 0,
                       -vx*vx+g*H * sin(theta)*sin(theta), 2*vx, 0,
                       -vx*vy-g*H * sin(theta)*cos(theta), vy, vx };
  Real dgdudata[9] = { 0, 0, 1,
                       -vx*vy-g*H * sin(theta)*cos(theta), vy, vx,
                       -vy*vy+g*H * cos(theta)*cos(theta), 0, 2*vy };
#else
  Real dfdudata[9] = { 0, 1, 0,
                       -vx*vx+g*H, 2*vx, 0,
                       -vx*vy, vy, vx };
  Real dgdudata[9] = { 0, 0, 1,
                       -vx*vy, vy, vx,
                       -vy*vy+g*H, 0, 2*vy };

//  Real theta = atan2(y,x);
//
//  Real dfdudata[9] = { 0, 1, 0,
//                       -vx*vx+g*H * cos(theta)*cos(theta), 2*vx, 0,
//                       -vx*vy+g*H * cos(theta)*sin(theta), vy, vx };
//  Real dgdudata[9] = { 0, 0, 1,
//                       -vx*vy+g*H * sin(theta)*cos(theta), vy, vx,
//                       -vy*vy+g*H * sin(theta)*sin(theta), 0, 2*vy };
#endif

  MatrixQ dfduTrue( dfdudata, 9 );
  MatrixQ dgduTrue( dgdudata, 9 );
  MatrixQ dfdu, dgdu;
  dfdu = 0;  dgdu = 0;
#endif

  // advective flux jabocian
  pde.jacobianFluxAdvective(x,y,t,var,dfdu,dgdu);

  for ( int i = 0; i < N; i++)
  {
    for ( int j = 0; j < N; j++)
    {
      SANS_CHECK_CLOSE( dfduTrue(i,j), dfdu(i,j), tol, tol );
      SANS_CHECK_CLOSE( dgduTrue(i,j), dgdu(i,j), tol, tol );
    }
  }

  // advective flux jacobian cumulation
  pde.jacobianFluxAdvective(x,y,t,var,dfdu,dgdu);

  for ( int i = 0; i < N; i++)
  {
    for ( int j = 0; j < N; j++)
    {
      SANS_CHECK_CLOSE( 2*dfduTrue(i,j), dfdu(i,j), tol, tol );
      SANS_CHECK_CLOSE( 2*dgduTrue(i,j), dgdu(i,j), tol, tol );
    }
  }
}

#if ShallowWaterHasStrongFluxAdvective2D
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( strongFluxAdvective_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD2, VarTypeHVelocity2D, SolutionClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  const int N = PDEClass::N;

  const Real tol = 1.E-13;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 17;    // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  PDEClass pde( q0, solnArgs );

  // unused
  const Real x = 1.6;
  const Real y = 3.8;
  const Real t = 0.3;
  // used
  Real H = 16.6, vx = -0.5, vy = 0.7;
  Real H_x = 0.4, vx_x = 3.5, vy_x = -1.1;
  Real H_y = -3.4, vx_y = -1.5, vy_y = 4.1;

  const VarTypeHVelocity2D vardata(H, vx, vy);
  const VarTypeHVelocity2D varxdata(H_x, vx_x, vy_x);
  const VarTypeHVelocity2D varydata(H_y, vx_y, vy_y);
  ArrayQ var, varx, vary;
  pde.setDOFFrom(vardata, var);
  pde.setDOFFrom(varxdata, varx);
  pde.setDOFFrom(varydata, vary);

  BOOST_CHECK( pde.isValidState(var) );

  ArrayQ strongPDE = { 0., 0., 0. };
  ArrayQ strongPDEtrue;

  // results
#if PDEShallowWater_sansgH2fluxAdvective2D  // 0.5*g*H^2 not included in advective flux
  strongPDEtrue(0) = (H_x*vx + H*vx_x) + (H_y*vy + H*vy_y);
  strongPDEtrue(1) = (H_x*vx*vx + 2*H*vx*vx_x) + (H_y*vy*vx + H*vy_y*vx + H*vy*vx_y);
  strongPDEtrue(2) = (H_x*vy*vx + H*vy_x*vx + H*vy*vx_x) + (H_y*vy*vy + 2*H*vy*vy_y);
#else  // 0.5*g*H^2 included in advective flux
  strongPDEtrue(0) = (H_x*vx + H*vx_x) + (H_y*vy + H*vy_y);
  strongPDEtrue(1) = (H_x*vx*vx + 2*H*vx*vx_x + g*H*H_x) + (H_y*vy*vx + H*vy_y*vx + H*vy*vx_y);
  strongPDEtrue(2) = (H_x*vy*vx + H*vy_x*vx + H*vy*vx_x) + (H_y*vy*vy + 2*H*vy*vy_y + g*H*H_y);
#endif

  // strong flux advective
  pde.strongFluxAdvective(x,y,t,var,varx,vary,strongPDE);

  for ( int i = 0; i < N; i++ )
    SANS_CHECK_CLOSE( strongPDEtrue(i), strongPDE(i), tol, tol );

  // strong flux advective cumulation
  pde.strongFluxAdvective(x,y,t,var,varx,vary,strongPDE);

  for ( int i = 0; i < N; i++ )
    SANS_CHECK_CLOSE( 2*strongPDEtrue(i), strongPDE(i), tol, tol );

}
#endif


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( source_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD2, VarTypeHVelocity2D, SolutionClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  const int N = PDEClass::N;

  const Real tol = 1.E-13;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 6.5;  // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  PDEClass pde( q0, solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  // unused
  const Real x = 1.6;
  const Real y = 1.2;
  const Real t = 0.3;
  // used
  Real vx = 0.6, vy = 0.7;
  Real vx_x = 3.5, vy_x = -1.1;
  Real vx_y = -1.5, vy_y = 4.1;

  Real H = 1 + x + pow(x,2) + pow(x,3) + pow(x,4) + pow(x,5);
  Real H_x = 1 + 2*x + 3*pow(x,2) + 4*pow(x,3) + 5*pow(x,4);
  Real H_y = 0;

  const VarTypeHVelocity2D vardata(H, vx, vy);
  const VarTypeHVelocity2D varxdata(H_x, vx_x, vy_x);
  const VarTypeHVelocity2D varydata(H_y, vx_y, vy_y);
  ArrayQ var, varx, vary;
  pde.setDOFFrom(vardata, var);
  pde.setDOFFrom(varxdata, varx);
  pde.setDOFFrom(varydata, vary);

  BOOST_CHECK( pde.isValidState(var) );

  ArrayQ source = { 0., 0., 0. };
  ArrayQ sourcetrue;

//  const Real costheta = 0.8, sintheta = 0.6;
//  const Real r = 2;
//
//  SANS_CHECK_CLOSE( sqrt(x*x+y*y), r, tol, tol );
//  SANS_CHECK_CLOSE( 1, costheta*costheta+sintheta*sintheta, tol, tol );

  Real b_x, b_y;

  Real geoSeries = 1 + x + pow(x,2) + pow(x,3) + pow(x,4) + pow(x,5);
  Real geoSeries_x =   1 + 2*x + 3*pow(x,2) + 4*pow(x,3) + 5*pow(x,4);
  b_x = ( 1 - q0*q0/(g*pow(geoSeries,3)) ) * geoSeries_x;
  b_y = 0;

  // results
#if PDEShallowWater_sansgH2fluxAdvective2D  // 0.5*g*H^2 not included in advective flux

#if ShallowWater_IsCircle_NotFlat
  const Real r = sqrt(x*x+y*y);
  const Real Speed = sqrt(vx*vx+vy*vy);

  sourcetrue(0) = 0;
  sourcetrue(1) = g*H*(H_x-b_x) + H*Speed*Speed*x/(r*r);
  sourcetrue(2) = g*H*(H_y-b_y) + H*Speed*Speed*y/(r*r);
#else
  sourcetrue(0) = 0;
  sourcetrue(1) = g*H*(H_x-b_x);
  sourcetrue(2) = g*H*(H_y-b_y);
#endif

#else  // 0.5*g*H^2 included in advective flux

#if ShallowWater_IsCircle_NotFlat
  const Real theta = atan2(y,x);
  const Real r = sqrt(x*x+y*y);
  const Real Speed = sqrt(vx*vx+vy*vy);

  sourcetrue(0) = 0;
  sourcetrue(1) = - g * H * b_x + 0.5*g*H*H*cos(theta)/r + H*Speed*Speed*x/(r*r);
  sourcetrue(2) = - g * H * b_y + 0.5*g*H*H*sin(theta)/r + H*Speed*Speed*y/(r*r);
#else
  sourcetrue(0) = 0;
  sourcetrue(1) = -g*H*b_x;
  sourcetrue(2) = -g*H*b_y;
#endif

#endif

  // strong flux advective

#if ShallowWaterIntegrandManifold2D // use integrand_galerkin_manifold
  typedef PDEClass::VectorEIF VectorEIF;
  typedef PDEClass::VectorX VectorX;

  ArrayQ varxi;
  pde.setDOFFrom(vardata, varxi);

  const VectorEIF EIF = { -1, -1, -1, -1  };
  const VectorX e1 = { 1., 0. };
  const VectorX e1x = { 0., 0. };
  const VectorX e1y = { 0., 0. };

  const Real xxi = -1, zxi = -2;

  pde.source(EIF,e1,e1x,e1y,x,y,t,xxi,zxi,var,varx,vary,varxi,source);
#else
  pde.source(x,y,t,var,varx,vary,source);
#endif
  for ( int i = 0; i < N; i++ )
    SANS_CHECK_CLOSE( sourcetrue(i), source(i), tol, tol );

  // strong flux advective cumulation
#if ShallowWaterIntegrandManifold2D // use integrand_galerkin_manifold
  pde.source(EIF,e1,e1x,e1y,x,y,t,xxi,zxi,var,varx,vary,varxi,source);
#else
  pde.source(x,y,t,var,varx,vary,source);
#endif

  for ( int i = 0; i < N; i++ )
    SANS_CHECK_CLOSE( 2*sourcetrue(i), source(i), tol, tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState_test )
{

  typedef ShallowWaterSolutionFunction2D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD2, VarTypeHVelocity2D, SolutionClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 1.2;    // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  PDEClass pde( q0, solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  Real H = 16.2;
  Real vx = 0.8, vy = -0.6;

  ArrayQ var;

  var = { H, vx, vy };

  BOOST_CHECK( pde.isValidState(var) ); // valid state

  var(0) =  - 0.7;
  BOOST_CHECK( !( pde.isValidState(var) ) ); // invalid state: negative H
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setDOFFrom_test )
{
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD2, VarTypeHVelocity2D, SolutionClass> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-15;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 6.5;  // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  PDEClass pde( q0, solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  Real H = 16.2;
  Real vx = 0.8, vy = 0.6;

  VarTypeHVelocity2D vardata( H, vx, vy );

  ArrayQ var;

  pde.setDOFFrom( vardata, var );

  SANS_CHECK_CLOSE( H,  var[0], tol, tol );
  SANS_CHECK_CLOSE( vx, var[1], tol, tol );
  SANS_CHECK_CLOSE( vy, var[2], tol, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
