// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCShallowWater1D_btest
//
// test of 1-D shallow water BC classes

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/ShallowWater/BCShallowWater1D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
template class BCShallowWater1D< BCTypeInflowSupercritical,
                                  PDEShallowWater<PhysD1,VarTypeHVelocity1D,
                                                  ShallowWaterSolutionFunction1D_GeometricSeriesx
                                                  >
                                >;
//
// Instantiate BCParamaters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters< BCShallowWater1DVector<VarTypeHVelocity1D,ShallowWaterSolutionFunction1D_GeometricSeriesx> >;
}

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BCShallowWater1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDEShallowWater<PhysD1,VarTypeHVelocity1D,ShallowWaterSolutionFunction1D_GeometricSeriesx> PDEClass;

  {
    typedef BCShallowWater1D< BCTypeInflowSupercritical, PDEClass > BCClass;
    typedef BCClass::ArrayQ<Real> ArrayQ;
    typedef BCClass::MatrixQ<Real> MatrixQ;

    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::N == 2 );
    BOOST_CHECK( BCClass::NBC == 2 );
    BOOST_CHECK( ArrayQ::M == 2 );
    BOOST_CHECK( MatrixQ::M == 2 );
    BOOST_CHECK( MatrixQ::N == 2 );
  }

  {
    typedef BCNone<PhysD1,PDEClass::N> BCClass;
    typedef BCClass::ArrayQ<Real> ArrayQ;
    typedef BCClass::MatrixQ<Real> MatrixQ;

    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::N == 2 );
    BOOST_CHECK( BCClass::NBC == 0 );
    BOOST_CHECK( ArrayQ::M == 2 );
    BOOST_CHECK( MatrixQ::M == 2 );
    BOOST_CHECK( MatrixQ::N == 2 );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_test )
{
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD1,VarTypeHVelocity1D,SolutionClass> PDEClass;


  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 20;   // q0 = v*H
  const int p = 7;      // polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  PDEClass pde( q0, solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );
  // BCTypeInflowSupercritical
  {
    typedef BCShallowWater1D< BCTypeInflowSupercritical, PDEClass > BCClass;
    typedef BCShallowWater1DParams<BCTypeInflowSupercritical> BCParamsType;
    typedef BCClass::ArrayQ<Real> ArrayQ;
    typedef boost::mpl::vector< BCShallowWater1D< BCTypeInflowSupercritical, PDEClass > > BCVectorClass;
    typedef BCParameters< BCVectorClass > BCParams;

    const ArrayQ bcdata = { 0, 0 };

    // ArrayQ constructor
    BCClass bc1(pde,bcdata);

    // Pydic constructor
    PyDict BCInflowSupercriticalArgs;
    BCInflowSupercriticalArgs[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSupercritical;
    BCInflowSupercriticalArgs[BCParamsType::params.H] = 2.1;
    BCInflowSupercriticalArgs[BCParamsType::params.u] = -1.2;

    PyDict PyBCList;
    PyBCList["BCInflow"] = BCInflowSupercriticalArgs;

    BCParams::checkInputs(PyBCList);

    BCClass bc2(pde, BCInflowSupercriticalArgs);
  }

  // BCNone
  {
    typedef BCNone<PhysD1,PDEClass::N> BCClass;

    // ArrayQ constructor
    BCClass bc1;

    // Pydict constructor
    PyDict BCNoneArgs;

    BCClass bc2(pde, BCNoneArgs);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_InflowSupercritical_test )
{
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysD1,VarTypeHVelocity1D,SolutionClass> PDEClass;
  typedef BCShallowWater1D< BCTypeInflowSupercritical, PDEClass > BCClass;

  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 20;   // q0 = v*H
  const int p = 7;      // polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  PDEClass pde( q0, solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  const ArrayQ bcdata = { 7.2, -1.2 };
  BCClass bc(pde,bcdata);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 2 );
  BOOST_CHECK( bc.NBC == 2 );

  Real x, t;
  Real nx;
  Real H, u;

  x = t = 0;  // not used
  nx = -1.0;

  H = 2.1;
  u = 3.9;

  BOOST_CHECK_CLOSE( fabs(nx), 1, tol );

  ArrayQ q;
  ArrayQ qx; // dummy
  VarTypeHVelocity1D qdata( H, u );
  pde.setDOFFrom( qdata, q );
  pde.setDOFFrom( qdata, qx );


  // strong-form BC
  ArrayQ rsdBCTrue = { H - bcdata(0), u - bcdata(1) };
  ArrayQ rsdBC = 0;

  bc.strongBC( x, t, nx, q, qx, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), tol, tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), tol, tol );

#if ShallowWaterHasLGStrongBC1D
  ArrayQ lg = {1.551, -0.237 };

  rsdBC = 0;
  bc.strongBC( x, t, nx, q, qx, lg, rsdBC );
  SANS_CHECK_CLOSE( rsdBCTrue(0), rsdBC(0), tol, tol );
  SANS_CHECK_CLOSE( rsdBCTrue(1), rsdBC(1), tol, tol );
#endif

  // BC weight (B^t)
#if PDEShallowWater_sansgH2fluxAdvective1D  // 0.5*g*H^2 not included in advective flux
  Real dfdudata[4] = { 0,    1,
                       -u*u, 2*u};
#else  // 0.5*g*H^2 included in advective flux
  Real dfdudata[4] = { 0,        1,
                       -u*u+g*H, 2*u};
#endif

  Real wghtBCdata[4];
  for (int n = 0; n < 4; n++)
    wghtBCdata[n] = nx*dfdudata[n];

  MatrixQ wghtBCTrue( wghtBCdata, 4 );
  MatrixQ wghtBC = 0;

  bc.weightBC( x, t, nx, q, qx, wghtBC );
  SANS_CHECK_CLOSE( wghtBCTrue(0,0), wghtBC(0,0), tol, tol );
  SANS_CHECK_CLOSE( wghtBCTrue(0,1), wghtBC(0,1), tol, tol );

  SANS_CHECK_CLOSE( wghtBCTrue(1,0), wghtBC(1,0), tol, tol );
  SANS_CHECK_CLOSE( wghtBCTrue(1,1), wghtBC(1,1), tol, tol );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
