// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ShallowWaterSolution1D_btest
//   test of 1-D shallow water solution class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/ShallowWater/ShallowWaterSolution1D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ShallowWaterSolution1D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction1D_GeometricSeriesx_p_smaller_than_0_test )
{
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx SolutionClass;
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx::VectorX VectorX;

  const Real g = 9.81;
  const Real q0 = 18.6;
  const int p = -1; // should throw an exception for p < 0

  Real x, t;

  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  SolutionClass fcn1( solnArgs );

  SolutionClass::ParamsType::checkInputs(solnArgs);

  // ArrayQ constructor
  SolutionClass fcn2( q0, p );

  x = 0;
  t = 0;
  VectorX nablasb;

  // overloaded operator()
  BOOST_CHECK_THROW( fcn2(x,t);, SANSException );

  // compute bathymetry gradient
  BOOST_CHECK_THROW( fcn2.getBathymetryGrad(x,t,nablasb);, SANSException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction1D_GeometricSeriesx_p0_test )
{
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx SolutionClass;
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx::template ArrayQ<Real> ArrayQ;
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx::VectorX VectorX;

  const Real g = 9.81;
  const Real q0 = 18.6;
  const int p = 0;

  const Real tol = 1.e-13;
  Real x, time;
  ArrayQ sln, slntrue;
  Real H, H_x; // used in computing db/dx
  VectorX nablasb, nablasbtrue;

  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  SolutionClass fcn1( solnArgs );

  // ArrayQ constructor
  SolutionClass fcn2( q0, p );

  // check
  x = 1;
  time = 0;
  slntrue = { 1, q0/1 };

  sln = fcn1(x, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );

  sln = fcn2(x, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );

  H = slntrue(0);
  H_x = 0;
  nablasbtrue[0] = (1 - q0*q0/g/pow(H,3)) * H_x;

  fcn1.getBathymetryGrad(x,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );

  // check
  x = 0.8;
  time = 0;

  H = 1;
  H_x = 0;
  slntrue = { H, q0/H };

  sln = fcn1(x, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );

  sln = fcn2(x, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );

  nablasbtrue[0] = (1 - q0*q0/g/pow(H,3)) * H_x;

  fcn1.getBathymetryGrad(x,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction1D_GeometricSeriesx_p1_test )
{
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx SolutionClass;
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx::template ArrayQ<Real> ArrayQ;
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx::VectorX VectorX;

  const Real g = 9.81;
  const Real q0 = 18.6;
  const int p = 1;

  const Real tol = 1.e-13;
  Real x, time;
  ArrayQ sln, slntrue;
  Real H, H_x; // used in computing db/dx
  VectorX nablasb, nablasbtrue;

  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  SolutionClass fcn1( solnArgs );

  // ArrayQ constructor
  SolutionClass fcn2( q0, p );

  // check
  x = 1;
  time = 0;
  slntrue = { 2, q0/2 };

  sln = fcn1(x, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );

  sln = fcn2(x, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );

  H = slntrue(0);
  H_x = 1;
  nablasbtrue[0] = (1 - q0*q0/g/pow(H,3)) * H_x;

  fcn1.getBathymetryGrad(x,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );

  // check
  x = 0.8;
  time = 0;

  H = 1 + x;
  H_x = 1;
  slntrue = { H, q0/H };

  sln = fcn1(x, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );

  sln = fcn2(x, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );

  nablasbtrue[0] = (1 - q0*q0/g/pow(H,3)) * H_x;

  fcn1.getBathymetryGrad(x,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
//
  nablasb = fcn2.getBathymetryGrad(x,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction1D_GeometricSeriesx_p5_test )
{
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx SolutionClass;
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx::template ArrayQ<Real> ArrayQ;
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx::VectorX VectorX;

  const Real g = 9.81;
  const Real q0 = 18.6;
  const int p = 5;

  const Real tol = 1.e-13;
  Real x, time;
  ArrayQ sln, slntrue;
  Real H, H_x; // used in computing db/dx
  VectorX nablasb, nablasbtrue;

  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  SolutionClass fcn1( solnArgs );

  // ArrayQ constructor
  SolutionClass fcn2( q0, p );

  // check
  x = 1;
  time = 0;

  H = 1 + x + pow(x,2) + pow(x,3) + pow(x,4) + pow(x,5);
  H_x = 1 + 2*x + 3*pow(x,2) + 4*pow(x,3) + 5*pow(x,4);
  slntrue = { H, q0/H };

  sln = fcn1(x, time);
  SANS_CHECK_CLOSE( 6.0, sln(0), tol, tol );
  SANS_CHECK_CLOSE( q0/6.0, sln(1), tol, tol );

  sln = fcn2(x, time);
  SANS_CHECK_CLOSE( 6.0, sln(0), tol, tol );
  SANS_CHECK_CLOSE( q0/6.0, sln(1), tol, tol );

  nablasbtrue[0] = (1 - q0*q0/g/pow(H,3)) * H_x;

  fcn1.getBathymetryGrad(x,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );

  // check
  x = 0.3;
  time = 0;

  H = 1 + x + pow(x,2) + pow(x,3) + pow(x,4) + pow(x,5);
  H_x = 1 + 2*x + 3*pow(x,2) + 4*pow(x,3) + 5*pow(x,4);
  slntrue = { H, q0/H };

  sln = fcn1(x, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );

  sln = fcn2(x, time);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );

  nablasbtrue[0] = (1 - q0*q0/g/pow(H,3)) * H_x;

  fcn1.getBathymetryGrad(x,time,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );

  nablasb = fcn2.getBathymetryGrad(x,time);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction1D_GeometricSeriesx_Forcing_p_smaller_than_0_test )
{
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx_Forcing SolutionClass;
  typedef SolutionClass::VectorX VectorX;

  const Real g = 9.81;
  const Real cH = 1.8;
  int pH, pu;

  const Real tol = 1.e-13;
  Real x, t;
  VectorX nablasb, nablasbtrue;

  // test
  pH = -1;
  pu = 0;
  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.pH] = pH;
  solnArgs[SolutionClass::ParamsType::params.cH] = cH;
  solnArgs[SolutionClass::ParamsType::params.pu] = pu;
  SolutionClass fcn1( solnArgs );

  SolutionClass::ParamsType::checkInputs(solnArgs);

  // ArrayQ constructor
  SolutionClass fcn2( pH, cH, pu );

  x = 0;
  t = 0;

  // overloaded operator()
  BOOST_CHECK_THROW( fcn1(x,t);, SANSException );

  // getBathymetryGrad
  fcn2.getBathymetryGrad(x,t,nablasb);
  nablasbtrue[0] = 1;
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );

  // test
  pH = 0;
  pu = -1;
  // PyDict constructor
  solnArgs[SolutionClass::ParamsType::params.pH] = pH;
  solnArgs[SolutionClass::ParamsType::params.pu] = pu;
  SolutionClass fcn3( solnArgs );

  SolutionClass::ParamsType::checkInputs(solnArgs);

  // overloaded operator()
  BOOST_CHECK_THROW( fcn3(x,t);, SANSException );

  // test
  pH = -1;
  pu = -1;
  // PyDict constructor
  solnArgs[SolutionClass::ParamsType::params.pH] = pH;
  solnArgs[SolutionClass::ParamsType::params.pu] = pu;
  SolutionClass fcn4( solnArgs );

  SolutionClass::ParamsType::checkInputs(solnArgs);

  // overloaded operator()
  BOOST_CHECK_THROW( fcn4(x,t);, SANSException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction1D_GeometricSeriesx_Forcing_p0and1_test )
{
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx_Forcing SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;
  typedef SolutionClass::VectorX VectorX;

  const Real g = 9.81;
  const Real cH = 0.24;
  int pH, pu;

  const Real tol = 1.e-13;
  Real x, t;
  ArrayQ sln, slntrue;
  Real H, H_x, u, u_x; // used in computing forcing function
  VectorX nablasb, nablasbtrue;
  DLA::VectorS<2,Real> forcing, forcingTrue;

  // test
  pH = 0;
  pu = 0;
  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.pH] = pH;
  solnArgs[SolutionClass::ParamsType::params.cH] = cH;
  solnArgs[SolutionClass::ParamsType::params.pu] = pu;
  SolutionClass fcn1( solnArgs );

  // check
  x = 0.7;
  t = 0;
  H = cH*( 1 );
  u = 1;
  slntrue = { H, u };

  sln = fcn1(x, t);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );

  nablasbtrue[0] = 1;
  fcn1.getBathymetryGrad(x,t,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );

  nablasb = fcn1.getBathymetryGrad(x,t);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );

  forcingTrue = { 0., 0 - g*H*nablasbtrue[0] };
  fcn1.forcingFunction( x, t, forcing );
  SANS_CHECK_CLOSE( forcingTrue(0), forcing(0), tol, tol );
  SANS_CHECK_CLOSE( forcingTrue(1), forcing(1), tol, tol );

  // test
  pH = 1;
  pu = 1;
  // PyDict constructor
  solnArgs[SolutionClass::ParamsType::params.pH] = pH;
  solnArgs[SolutionClass::ParamsType::params.pu] = pu;
  SolutionClass fcn2( solnArgs );

  // check
  x = 0.3;
  t = 0;
  H = cH*(1 + x);  H_x = cH*1;
  u = 1 + x;  u_x = 1;
  slntrue = { H, u };

  sln = fcn2(x, t);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );

  nablasbtrue[0] = 1;
  fcn2.getBathymetryGrad(x,t,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );

  forcingTrue = { u*H_x+H*u_x, (u*u+g*H)*H_x+2*H*u*u_x - g*H*nablasbtrue[0] };
  forcing = 0;
  fcn2.forcingFunction( x, t, forcing );
  SANS_CHECK_CLOSE( forcingTrue(0), forcing(0), tol, tol );
  SANS_CHECK_CLOSE( forcingTrue(1), forcing(1), tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ShallowWaterSolutionFunction1D_GeometricSeriesx_Forcing_p5_test )
{
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx_Forcing SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;
  typedef SolutionClass::VectorX VectorX;

  const Real g = 9.81;
  const Real cH = 0.3;
  int pH, pu;

  const Real tol = 1.e-13;
  Real x, t;
  ArrayQ sln, slntrue;
  Real H, H_x, u, u_x; // used in computing forcing function
  VectorX nablasb, nablasbtrue;
  DLA::VectorS<2,Real> forcing, forcingTrue;

  // test
  pH = 5;
  pu = 5;
  // PyDict constructor
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.pH] = pH;
  solnArgs[SolutionClass::ParamsType::params.cH] = cH;
  solnArgs[SolutionClass::ParamsType::params.pu] = pu;
  SolutionClass fcn1( solnArgs );

  // check
  x = 0.3;
  t = 0;
  H = cH*( 1 + x + pow(x,2) + pow(x,3) + pow(x,4) + pow(x,5) );
  H_x = cH*( 1 + 2*x + 3*pow(x,2) + 4*pow(x,3) + 5*pow(x,4) );
  u = 1 + x + pow(x,2) + pow(x,3) + pow(x,4) + pow(x,5);
  u_x = 1 + 2*x + 3*pow(x,2) + 4*pow(x,3) + 5*pow(x,4);
  slntrue = { H, u };

  sln = fcn1(x, t);
  SANS_CHECK_CLOSE( slntrue(0), sln(0), tol, tol );
  SANS_CHECK_CLOSE( slntrue(1), sln(1), tol, tol );

  nablasbtrue[0] = 1;
  fcn1.getBathymetryGrad(x,t,nablasb);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );

  nablasb = fcn1.getBathymetryGrad(x,t);
  SANS_CHECK_CLOSE( nablasbtrue[0], nablasb[0], tol, tol );

  forcingTrue = { u*H_x+H*u_x, (u*u+g*H)*H_x+2*H*u*u_x - g*H*nablasbtrue[0] };
  fcn1.forcingFunction( x, t, forcing );
  SANS_CHECK_CLOSE( forcingTrue(0), forcing(0), tol, tol );
  SANS_CHECK_CLOSE( forcingTrue(1), forcing(1), tol, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
