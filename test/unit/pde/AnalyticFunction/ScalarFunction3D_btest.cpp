// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ScalarFunction3D_btest
//
// test of 3D scalar analytic functions

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"

#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"


using namespace std;
using namespace SANS;

//###############################################################################//
BOOST_AUTO_TEST_SUITE (ScalarFunction3D_test_suite)

//-------------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction3D_ASExp_test )
{
  typedef ScalarFunction3D_ASExp::template ArrayQ<Real> ArrayQ;


  const Real tol = 1.e-13;

  Real x, y, z, time = 0;
  ArrayQ sln, sln_true;

  Real u0, x0, y0, z0, a, b, c, alpha;
  u0 = 2.;
  x0 = 1.2; y0 =  2.1; z0 = 0.51;
  a  = 1.1; b  = -0.1; c  = 1.34;
  alpha = 3.4;

  PyDict d;
  d[ScalarFunction3D_ASExp::ParamsType::params.u0] = u0;
  d[ScalarFunction3D_ASExp::ParamsType::params.x0] = x0;
  d[ScalarFunction3D_ASExp::ParamsType::params.y0] = y0;
  d[ScalarFunction3D_ASExp::ParamsType::params.z0] = z0;
  d[ScalarFunction3D_ASExp::ParamsType::params.a] = a;
  d[ScalarFunction3D_ASExp::ParamsType::params.b] = b;
  d[ScalarFunction3D_ASExp::ParamsType::params.c] = c;
  d[ScalarFunction3D_ASExp::ParamsType::params.alpha] = alpha;
  ScalarFunction3D_ASExp::ParamsType::checkInputs(d);

  std::vector<Real> xs = { 0,1,1.5,0.6,0.2};
  std::vector<Real> ys = { 0,2,2.5,1.6,1.2};
  std::vector<Real> zs = { 0,3,3.5,2.6,2.2};

  {
    ScalarFunction3D_ASExp fcn(d);

//    fcn.dump();

    for (std::size_t ix = 0; ix < xs.size(); ix++)
      for (std::size_t iy = 0; iy < ys.size(); iy++)
        for (std::size_t iz = 0; iz < zs.size(); iz++)
        {
          x = xs[ix]; y = ys[iy]; z = zs[iz];
          sln = fcn( x, y, z, time );
          sln_true = u0*::max( exp(alpha*(x0 - x)/a), ::max(exp(alpha*(y0 - y)/b),exp(alpha*(z0 - z)/c)) );
          BOOST_CHECK_CLOSE( sln_true, sln, tol);
        }
  }

  {
    ScalarFunction3D_ASExp fcn(u0,x0,y0,z0,a,b,c,alpha);

    for (std::size_t ix = 0; ix < xs.size(); ix++)
      for (std::size_t iy = 0; iy < ys.size(); iy++)
        for (std::size_t iz = 0; iz < zs.size(); iz++)
        {
          x = xs[ix]; y = ys[iy]; z = zs[iz];
          sln = fcn( x, y, z, time );
          sln_true = u0*::max( exp(alpha*(x0 - x)/a), ::max(exp(alpha*(y0 - y)/b),exp(alpha*(z0 - z)/c)) );
          BOOST_CHECK_CLOSE( sln_true, sln, tol);
        }

  }

}

//-------------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction3D_Gaussian_test )
{


  typedef ScalarFunction3D_Gaussian::template ArrayQ<Real> ArrayQ;
  //typedef ScalarFunction3D_Gaussian ScalarFunctionClass;

  //ScalarFunctionClass fcn;

  const Real tol = 1.e-13;

  Real x, y, z, time;
  ArrayQ sln;

  Real sigma, x0, y0, z0;
  sigma = 1;
  x0 = 1.;
  y0 = 2.1;
  z0 = 0.51;
  {
    ScalarFunction3D_Gaussian fcn(sigma, x0, y0, z0);
    x = 0;
    y = 0;
    z = 0;
    sln = fcn(x, y, z, time);
    BOOST_CHECK_CLOSE((1/pow(sqrt(2*PI)*sigma,3))*exp((-1/(2*sigma*sigma))*(pow(x-x0,2)+pow(y-y0,2)+pow(z-z0,2))),
            sln, tol);

    x = 1;
    y = 2;
    z = 3;
    sln = fcn(x, y, z, time);
    BOOST_CHECK_CLOSE((1/pow(sqrt(2*PI)*sigma,3))*exp((-1/(2*sigma*sigma))*(pow(x-x0,2)+pow(y-y0,2)+pow(z-z0,2))),
                       sln, tol);
  }

  {
    PyDict d;
    d[ScalarFunction3D_Gaussian::ParamsType::params.sigma] = sigma;
    d[ScalarFunction3D_Gaussian::ParamsType::params.x0] = x0;
    d[ScalarFunction3D_Gaussian::ParamsType::params.y0] = y0;
    d[ScalarFunction3D_Gaussian::ParamsType::params.z0] = z0;
    ScalarFunction3D_Gaussian::ParamsType::checkInputs(d);
    ScalarFunction3D_Gaussian fcn(d);

    x = 0;
    y = 0;
    z = 0;
    time = 0;

    sln = fcn(x, y, z, time);
    BOOST_CHECK_CLOSE((1/pow(sqrt(2*PI)*sigma,3))*exp((-1/(2*sigma*sigma))*(pow(x-x0,2)+pow(y-y0,2)+pow(z-z0,2))),
            sln, tol);

    x = 1;
    y = 2;
    z = 3;
    sln = fcn (x, y, z, time);
    BOOST_CHECK_CLOSE((1/pow(sqrt(2*PI)*sigma,3))*exp((-1/(2*sigma*sigma))*(pow(x-x0,2)+pow(y-y0,2)+pow(z-z0,2))),
        sln, tol);
  }
}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction3D_SineSineSine_test )
{
  typedef ScalarFunction3D_SineSineSine::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;
  Real x, y, z, time;
  ArrayQ sln, slnTrue;

  Real a = 2, b = 3, c = 0.5, C = 0.5, A = 4;
  {
    ScalarFunction3D_SineSineSine fcn(a,b,c,C,A);

    x = 0;
    y = 0;
    z = 0;
    time = 0;

    slnTrue = A*sin(a*PI*x)*sin(b*PI*y)*sin(c*PI*z) + C;
    sln = fcn(x, y, z, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 1;
    y = 1;
    z = 1;
    slnTrue = A*sin(a*PI*x)*sin(b*PI*y)*sin(c*PI*z) + C;
    sln = fcn(x, y, z, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.5;
    y = 1;
    z = 1.5;
    slnTrue = A*sin(a*PI*x)*sin(b*PI*y)*sin(c*PI*z) + C;
    sln = fcn(x, y, z, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.25;
    y = 0.25;
    z = 0.25;
    slnTrue = A*sin(a*PI*x)*sin(b*PI*y)*sin(c*PI*z) + C;
    sln = fcn(x, y, z, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );
  }

  {
    PyDict dict;
    dict[ScalarFunction3D_SineSineSine::ParamsType::params.a] = a;
    dict[ScalarFunction3D_SineSineSine::ParamsType::params.b] = b;
    dict[ScalarFunction3D_SineSineSine::ParamsType::params.c] = c;
    dict[ScalarFunction3D_SineSineSine::ParamsType::params.C] = C;
    dict[ScalarFunction3D_SineSineSine::ParamsType::params.A] = A;
    ScalarFunction3D_SineSineSine::ParamsType::checkInputs(dict);
    ScalarFunction3D_SineSineSine fcn(dict);

    x = 0;
    y = 0;
    z = 0;
    time = 0;

    slnTrue = A*sin(a*PI*x)*sin(b*PI*y)*sin(c*PI*z) + C;
    sln = fcn(x, y, z, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 1;
    y = 1;
    z = 1;
    slnTrue = A*sin(a*PI*x)*sin(b*PI*y)*sin(c*PI*z) + C;
    sln = fcn(x, y, z, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.5;
    y = 1;
    z = 1.5;
    slnTrue = A*sin(a*PI*x)*sin(b*PI*y)*sin(c*PI*z) + C;
    sln = fcn(x, y, z, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.25;
    y = 0.25;
    z = 0.25;
    slnTrue = A*sin(a*PI*x)*sin(b*PI*y)*sin(c*PI*z) + C;
    sln = fcn(x, y, z, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/ScalarFunction3D_pattern.txt", true );

  {
    Real sigma = 2;
    Real x0 = 0.1;
    Real y0 = 0.2;
    Real z0 = 0.3;

    ScalarFunction3D_Gaussian soln(sigma, x0, y0, z0);
    soln.dump( 2, output );
  }

  {
    ScalarFunction3D_ASExp soln(1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);
    soln.dump( 2, output );
  }

  {
    PyDict d;
    ScalarFunction3D_SineSineSine soln(d);
    soln.dump( 2, output );
  }

  {
    PyDict d;
    ScalarFunction3D_SineSineSineDecay soln(d);
    soln.dump( 2, output );
  }

  {
    ScalarFunction3D_AdvectionDiffusionDecayingCosine soln(1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);
    soln.dump( 2, output );
  }

  {
    ScalarFunction3D_BoundaryLayer soln(1.0, 1.0, 1);
    soln.dump( 2, output );
  }

  {
    ScalarFunction3D_BoundaryLayerSingularPerturbation soln(1.0, 1.0, 1.0, 1.0, 1);
    soln.dump( 2, output );
  }

  {
    ScalarFunction3D_TripleBL soln(1.0, 1.0, 1.0, 1.0, 1.0, 1);
    soln.dump( 2, output );
  }

  {
    ScalarFunction3D_TripleBLsine soln(1.0, 1.0, 1.0, 1.0, 1.0, 1.0);
    soln.dump( 2, output );
  }

  {
    ScalarFunction3D_CornerSingularity soln(1.0, 1.0, 1.0, 1.0);
    soln.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
