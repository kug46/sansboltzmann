// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ScalarFunction1D_btest
//
// test of 1-D advection diffusion MMS classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AnalyticFunction/ScalarFunction1D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ScalarFunction1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_Const )
{
  typedef ScalarFunction1D_Const::ArrayQ<Real> ArrayQ;

  ScalarFunction1D_Const fcn( 1 );

  Real x = 1;
  Real time = 0;
  ArrayQ sln;
  sln = fcn(x, time);

  const Real tol = 1.e-13;
  BOOST_CHECK_CLOSE( 1, sln, tol );

  PyDict Input;
  Input[ScalarFunction1D_Const::ParamsType::params.a0] = 1;
  ScalarFunction1D_Const::ParamsType::checkInputs(Input);
  ScalarFunction1D_Const fcn2(Input);
  sln = fcn2(x, time);

  BOOST_CHECK_CLOSE( 1, sln, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_Linear )
{
  typedef ScalarFunction1D_Linear::ArrayQ<Real> ArrayQ;

  ScalarFunction1D_Linear fcn( 1, 2 );

  Real x = 0;
  Real time = 0;
  ArrayQ sln;

  const Real tol = 1.e-13;

  x = 0;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( 1, sln, tol );

  x = 1;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( 3, sln, tol );

  PyDict Input;
  Input[ScalarFunction1D_Linear::ParamsType::params.a0] = 1;
  Input[ScalarFunction1D_Linear::ParamsType::params.ax] = 2;
  ScalarFunction1D_Linear::ParamsType::checkInputs(Input);
  ScalarFunction1D_Linear fcn2(Input);
  sln = fcn2(x, time);

  BOOST_CHECK_CLOSE( 3, sln, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_PiecewiseConst )
{
  typedef ScalarFunction1D_PiecewiseConst::ArrayQ<Real> ArrayQ;

  ScalarFunction1D_PiecewiseConst fcn( 0.2, 0.3, 0.5 );

  Real x = 0;
  Real time = 0;
  ArrayQ sln;

  const Real tol = 1.e-13;

  x = 0;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( 0.2, sln, tol );

  x = 1;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( 0.3, sln, tol );

  PyDict Input;
  Input[ScalarFunction1D_PiecewiseConst::ParamsType::params.a0] =  1.0;
  Input[ScalarFunction1D_PiecewiseConst::ParamsType::params.a1] = -3.0;
  Input[ScalarFunction1D_PiecewiseConst::ParamsType::params.x0] =  0.0;
  ScalarFunction1D_PiecewiseConst::ParamsType::checkInputs(Input);
  ScalarFunction1D_PiecewiseConst fcn2(Input);

  x = -0.1;
  sln = fcn2(x, time);
  BOOST_CHECK_CLOSE( 1.0, sln, tol );

  x = 0.1;
  sln = fcn2(x, time);
  BOOST_CHECK_CLOSE(-3.0, sln, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_Monomial )
{
  typedef ScalarFunction1D_Monomial::ArrayQ<Real> ArrayQ;

  ScalarFunction1D_Monomial fcn( 2 );

  Real x = 0;
  Real time = 0;
  ArrayQ sln;

  const Real tol = 1.e-13;

  x = 0;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( pow(x, 2), sln, tol );

  x = 1;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( pow(x, 2), sln, tol );

  PyDict Input;
  Input[ScalarFunction1D_Monomial::ParamsType::params.i] = 2;
  ScalarFunction1D_Monomial::ParamsType::checkInputs(Input);
  ScalarFunction1D_Monomial fcn2(Input);
  sln = fcn2(x, time);

  BOOST_CHECK_CLOSE( pow(x, 2), sln, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_Quad )
{
  typedef ScalarFunction1D_Quad::ArrayQ<Real> ArrayQ;

  ScalarFunction1D_Quad fcn;

  Real x = 0;
  Real time = 0;
  ArrayQ sln;

  const Real tol = 1.e-13;

  x = -0.5;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( 6*x*(1-x), sln, tol );

  x = 2;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( 6*x*(1-x), sln, tol );

  PyDict Input;
  ScalarFunction1D_Quad::ParamsType::checkInputs(Input);
  ScalarFunction1D_Quad fcn2(Input);
  sln = fcn2(x, time);

  BOOST_CHECK_CLOSE( 6*x*(1-x), sln, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_Hex )
{
  typedef ScalarFunction1D_Hex::ArrayQ<Real> ArrayQ;

  ScalarFunction1D_Hex fcn;

  Real x = 0;
  Real time = 0;
  ArrayQ sln;

  const Real tol = 1.e-13;

  x = -0.5;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( -x*x*(x - 1)*(x*x*x - x*x + x - 1), sln, tol );

  x = 2;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( -x*x*(x - 1)*(x*x*x - x*x + x - 1), sln, tol );

  PyDict Input;
  ScalarFunction1D_Hex::ParamsType::checkInputs(Input);
  ScalarFunction1D_Hex fcn2(Input);
  sln = fcn2(x, time);

  BOOST_CHECK_CLOSE( -x*x*(x - 1)*(x*x*x - x*x + x - 1), sln, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_Hex2 )
{
  typedef ScalarFunction1D_Hex2::ArrayQ<Real> ArrayQ;

  ScalarFunction1D_Hex2 fcn;

  Real x = 0;
  Real time = 0;
  ArrayQ sln;

  const Real tol = 1.e-13;

  x = -0.5;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( -x*x*(x - 2)*(x*x*x - x*x + x - 1), sln, tol );

  x = 2;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( -x*x*(x - 2)*(x*x*x - x*x + x - 1), sln, tol );

  PyDict Input;
  ScalarFunction1D_Hex2::ParamsType::checkInputs(Input);
  ScalarFunction1D_Hex2 fcn2(Input);
  sln = fcn2(x, time);

  BOOST_CHECK_CLOSE( -x*x*(x - 2)*(x*x*x - x*x + x - 1), sln, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_Exp_1 )
{
  typedef ScalarFunction1D_Exp1::ArrayQ<Real> ArrayQ;

  ScalarFunction1D_Exp1 fcn;

  Real x = 0;
  Real time = 0;
  ArrayQ sln;

  const Real tol = 1.e-13;

  x = -0.5;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( -(x*exp(7*x - 1)*(x - 1))/21., sln, tol );

  x = 2;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( -(x*exp(7*x - 1)*(x - 1))/21., sln, tol );

  PyDict Input;
  ScalarFunction1D_Exp1::ParamsType::checkInputs(Input);
  ScalarFunction1D_Exp1 fcn2(Input);
  sln = fcn2(x, time);

  BOOST_CHECK_CLOSE( -(x*exp(7*x - 1)*(x - 1))/21., sln, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_Exp_2 )
{
  typedef ScalarFunction1D_Exp2::ArrayQ<Real> ArrayQ;

  ScalarFunction1D_Exp2 fcn;

  Real x = 0;
  Real time = 0;
  ArrayQ sln;

  const Real tol = 1.e-13;

  x = -0.5;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( -10*x*exp(1 - 10*x)*(x - 1), sln, tol );

  x = 2;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( -10*x*exp(1 - 10*x)*(x - 1), sln, tol );

  PyDict Input;
  ScalarFunction1D_Exp2::ParamsType::checkInputs(Input);
  ScalarFunction1D_Exp2 fcn2(Input);
  sln = fcn2(x, time);

  BOOST_CHECK_CLOSE( -10*x*exp(1 - 10*x)*(x - 1), sln, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_Sine )
{
  typedef ScalarFunction1D_Sine::ArrayQ<Real> ArrayQ;

  ScalarFunction1D_Sine fcn;

  Real x = 0;
  Real time = 0;
  ArrayQ sln;

  const Real tol = 1.e-13;

  x = -0.5;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( sin(2*PI*x), sln, tol );

  x = 2;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( sin(2*PI*x), sln, tol );

  PyDict Input;
  ScalarFunction1D_Sine::ParamsType::checkInputs(Input);
  ScalarFunction1D_Sine fcn2(Input);
  sln = fcn2(x, time);

  BOOST_CHECK_CLOSE( sin(2*PI*x), sln, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_ConstLineUnsteady )
{
  typedef ScalarFunction1D_ConstLineUnsteady::ArrayQ<Real> ArrayQ;

  ScalarFunction1D_ConstLineUnsteady fcn;

  Real x = 0;
  Real time = 0;
  ArrayQ sln;

  const Real tol = 1.e-13;

  x = -0.5;
  time = -0.1;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( time, sln, tol );

  x = 2;
  time = 1;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( time, sln, tol );

  PyDict Input;
  ScalarFunction1D_ConstLineUnsteady::ParamsType::checkInputs(Input);
  ScalarFunction1D_ConstLineUnsteady fcn2(Input);
  sln = fcn2(x, time);

  BOOST_CHECK_CLOSE( time, sln, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_SineCosUnsteady )
{
  typedef ScalarFunction1D_SineCosUnsteady::ArrayQ<Real> ArrayQ;

  ScalarFunction1D_SineCosUnsteady fcn;

  Real x = 0;
  Real time = 0;
  ArrayQ sln;

  const Real tol = 1.e-13;

  x = -0.5;
  time = -0.1;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( sin(2*PI*x)*cos(2*PI*time), sln, tol );

  x = 2;
  time = 1;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( sin(2*PI*x)*cos(2*PI*time), sln, tol );

  PyDict Input;
  ScalarFunction1D_SineCosUnsteady::ParamsType::checkInputs(Input);
  ScalarFunction1D_SineCosUnsteady fcn2(Input);
  sln = fcn2(x, time);

  BOOST_CHECK_CLOSE( sin(2*PI*x)*cos(2*PI*time), sln, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_ConstSineUnsteady )
{
  typedef ScalarFunction1D_ConstSineUnsteady::ArrayQ<Real> ArrayQ;

  ScalarFunction1D_ConstSineUnsteady fcn;

  Real x = 0;
  Real time = 0;
  ArrayQ sln;

  const Real tol = 1.e-13;

  x = -0.5;
  time = -0.1;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( sin(2*PI*time), sln, tol );

  x = 2;
  time = 1;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( sin(2*PI*time), sln, tol );

  PyDict Input;
  ScalarFunction1D_ConstSineUnsteady::ParamsType::checkInputs(Input);
  ScalarFunction1D_ConstSineUnsteady fcn2(Input);
  sln = fcn2(x, time);

  BOOST_CHECK_CLOSE( sin(2*PI*time), sln, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_ConstCosUnsteady )
{
  typedef ScalarFunction1D_ConstCosUnsteady::ArrayQ<Real> ArrayQ;

  ScalarFunction1D_ConstCosUnsteady fcn;

  Real x = 0;
  Real time = 0;
  ArrayQ sln;

  const Real tol = 1.e-13;

  x = -0.5;
  time = -0.1;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( cos(2*PI*time), sln, tol );

  x = 2;
  time = 1;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( cos(2*PI*time), sln, tol );

  PyDict Input;
  ScalarFunction1D_ConstCosUnsteady::ParamsType::checkInputs(Input);
  ScalarFunction1D_ConstCosUnsteady fcn2(Input);
  sln = fcn2(x, time);

  BOOST_CHECK_CLOSE( cos(2*PI*time), sln, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_BL )
{
  typedef ScalarFunction1D_BL::ArrayQ<Real> ArrayQ;

  Real a = 0.5;
  Real nu = 1.2;
  ScalarFunction1D_BL fcn( a, nu );

  Real x = 0;
  Real time = 0;
  ArrayQ sln;

  const Real tol = 1.e-13;

  x = -0.5;
  time = -0.1;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( (1 - exp(-a*(1 - x)/nu)) / (1 - exp(-a/nu)), sln, tol );

  x = 2;
  time = 1;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( (1 - exp(-a*(1 - x)/nu)) / (1 - exp(-a/nu)), sln, tol );

  PyDict Input;
  Input[ScalarFunction1D_BL::ParamsType::params.a] = a;
  Input[ScalarFunction1D_BL::ParamsType::params.nu] = nu;
  ScalarFunction1D_BL::ParamsType::checkInputs(Input);
  ScalarFunction1D_BL fcn2(Input);
  sln = fcn2(x, time);

  BOOST_CHECK_CLOSE( (1 - exp(-a*(1 - x)/nu)) / (1 - exp(-a/nu)), sln, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_Tanh )
{
  typedef ScalarFunction1D_Tanh::ArrayQ<Real> ArrayQ;

  Real nu = 0.1;
  Real A = 2.0;
  Real B = 0;
  ScalarFunction1D_Tanh fcn(nu, A, B);

  Real x = 0;
  Real time = 0;
  ArrayQ sln;

  const Real tol = 1.e-13;

  x = -0.5;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( -tanh(x / 2.0 / nu), sln, tol );

  x = 2;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( -tanh(x / 2.0 / nu), sln, tol );

  PyDict Input;
  Input[ScalarFunction1D_Tanh::ParamsType::params.nu] = nu;
  Input[ScalarFunction1D_Tanh::ParamsType::params.A] = A;
  Input[ScalarFunction1D_Tanh::ParamsType::params.B] = B;
  ScalarFunction1D_Tanh::ParamsType::checkInputs(Input);
  ScalarFunction1D_Tanh fcn2(Input);
  sln = fcn2(x, time);

  BOOST_CHECK_CLOSE( -tanh(x / 2.0 / nu), sln, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_NonLinPoisson )
{
  typedef ScalarFunction1D_NonLinPoisson::ArrayQ<Real> ArrayQ;

  Real lam = 2.3;
  Real A = 2.0;
  Real B = 3.0;
  Real D = 4.0;
  ScalarFunction1D_NonLinPoisson fcn(lam, A, B, D);

  Real x = 0;
  Real time = 0;
  ArrayQ sln;

  const Real tol = 1.e-13;

  x = -0.5;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( 1./lam*log((A+B*x)/D), sln, tol );

  x = 2;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( 1./lam*log((A+B*x)/D), sln, tol );

  PyDict Input;
  Input[ScalarFunction1D_NonLinPoisson::ParamsType::params.lam] = lam;
  Input[ScalarFunction1D_NonLinPoisson::ParamsType::params.A] = A;
  Input[ScalarFunction1D_NonLinPoisson::ParamsType::params.B] = B;
  Input[ScalarFunction1D_NonLinPoisson::ParamsType::params.D] = D;
  ScalarFunction1D_NonLinPoisson::ParamsType::checkInputs(Input);
  ScalarFunction1D_NonLinPoisson fcn2(Input);
  sln = fcn2(x, time);

  BOOST_CHECK_CLOSE( 1./lam*log((A+B*x)/D), sln, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_Gaussian )
{
  typedef ScalarFunction1D_Gaussian::ArrayQ<Real> ArrayQ;

  Real A = 2.0;
  Real mu = -0.5;
  Real sigma = 4.0;
  ScalarFunction1D_Gaussian fcn( A, mu, sigma );

  Real x = 0;
  Real time = 0;
  ArrayQ sln;

  const Real tol = 1.e-13;

  x = -0.5;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( A*exp(-(x-mu)*(x-mu)/(2* sigma*sigma)), sln, tol );

  x = 2;
  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( A*exp(-(x-mu)*(x-mu)/(2* sigma*sigma)), sln, tol );

  PyDict Input;
  Input[ScalarFunction1D_Gaussian::ParamsType::params.A] = A;
  Input[ScalarFunction1D_Gaussian::ParamsType::params.mu] = mu;
  Input[ScalarFunction1D_Gaussian::ParamsType::params.sigma] = sigma;
  ScalarFunction1D_Gaussian::ParamsType::checkInputs(Input);
  ScalarFunction1D_Gaussian fcn2(Input);
  sln = fcn2(x, time);

  BOOST_CHECK_CLOSE( A*exp(-(x-mu)*(x-mu)/(2* sigma*sigma)), sln, tol );
}




//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_OffsetLog )
{
  typedef ScalarFunction1D_OffsetLog::ArrayQ<Real> ArrayQ;
  ScalarFunction1D_OffsetLog fcn;

  Real x = 1.0;
  Real time = 0;
  ArrayQ sln;
  ArrayQ sln_x, sln_t;

  const Real tol = 1.e-13;

  sln = fcn(x, time);
  BOOST_CHECK_CLOSE( 10.0, sln, tol );

  fcn.gradient(x, time, sln, sln_x, sln_t);
  BOOST_CHECK_CLOSE( 10.0, sln, tol );
  BOOST_CHECK_CLOSE( 9.0/log(2.0) / (1.0+x), sln_x, tol );
  BOOST_CHECK_CLOSE( 0.0, sln_t, tol );

  PyDict Input;
  ScalarFunction1D_OffsetLog::ParamsType::checkInputs(Input);
  ScalarFunction1D_OffsetLog fcn2(Input);
  sln = fcn2(x, time);

  fcn.gradient(x, time, sln, sln_x, sln_t);
  BOOST_CHECK_CLOSE( 10.0, sln, tol );
  BOOST_CHECK_CLOSE( 9.0/log(2.0) / (1.0+x), sln_x, tol );
  BOOST_CHECK_CLOSE( 0.0, sln_t, tol );
}

// -------------------------------------------------------------------------- //
BOOST_AUTO_TEST_CASE(IO)
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/ScalarFunction1D_pattern.txt", true );

  {
    ScalarFunction1D_Const soln(1.0);
    soln.dump(2, output);
  }

  {
    ScalarFunction1D_Linear soln(1.0, 1.0);
    soln.dump(2, output);
  }

  {
    ScalarFunction1D_PiecewiseConst soln(1.0, 1.0, 1.0);
    soln.dump(2, output);
  }

  {
    ScalarFunction1D_Monomial soln(1.0);
    soln.dump(2, output);
  }

  {
    ScalarFunction1D_Quad soln;
    soln.dump(2, output);
  }

  {
    ScalarFunction1D_Hex soln;
    soln.dump(2, output);
  }

  {
    ScalarFunction1D_Hex2 soln;
    soln.dump(2, output);
  }

  {
    ScalarFunction1D_MaxCubic soln;
    soln.dump(2, output);
  }

  {
    ScalarFunction1D_Exp soln;
    soln.dump(2, output);
  }

  {
    ScalarFunction1D_Exp1 soln;
    soln.dump(2, output);
  }

  {
    ScalarFunction1D_Exp2 soln;
    soln.dump(2, output);
  }

  {
    ScalarFunction1D_Exp3 soln;
    soln.dump(2, output);
  }

  {
    ScalarFunction1D_Sine soln;
    soln.dump(2, output);
  }

  {
    ScalarFunction1D_ConstLineUnsteady soln;
    soln.dump(2, output);
  }

  {
    ScalarFunction1D_SineCosUnsteady soln;
    soln.dump(2, output);
  }

  {
    ScalarFunction1D_SineSineUnsteady soln;
    soln.dump(2, output);
  }

  {
    ScalarFunction1D_ConstSineUnsteady soln;
    soln.dump(2, output);
  }

  {
    ScalarFunction1D_ConstCosUnsteady soln;
    soln.dump(2, output);
  }

  {
    ScalarFunction1D_BL soln(1.0, 1.0);
    soln.dump(2, output);
  }

  {
    PyDict d;
    ScalarFunction1D_Tanh soln(d);
    soln.dump(2, output);
  }

  {
    ScalarFunction1D_OffsetLog soln;
    soln.dump(2, output);
  }

  {
    PyDict d;
    ScalarFunction1D_NonLinPoisson soln(d);
    soln.dump(2, output);
  }

  {
    ScalarFunction1D_PiecewiseLinear soln;
    soln.dump(2, output);
  }

  {
    PyDict d;
    ScalarFunction1D_Gaussian soln(d);
    soln.dump(2, output);
  }

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
