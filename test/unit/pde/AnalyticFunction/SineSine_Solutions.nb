(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     36935,        854]
NotebookOptionsPosition[     34980,        812]
NotebookOutlinePosition[     35314,        827]
CellTagsIndexPosition[     35271,        824]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{"Defining", " ", "the", " ", "SineSine"}], " ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{"a", " ", "=", " ", "1"}], ";", " ", 
    RowBox[{"b", " ", "=", " ", "1"}], ";", " ", 
    RowBox[{"scale", " ", "=", " ", "1"}], ";", " ", 
    RowBox[{"offset", " ", "=", " ", "0"}], ";"}], " ", "*)"}], 
  "\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{
     RowBox[{"A_", "*", "sin", 
      RowBox[{"(", 
       RowBox[{"a_", "*", "PI", "*", "x"}], ")"}], "*", "sin", 
      RowBox[{"(", 
       RowBox[{"b_", "*", "PI", "*", "y"}], ")"}]}], "+", "C_"}], ";"}], " ", 
   "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"f", "[", "x_", "]"}], " ", "=", 
     RowBox[{"Sin", "[", 
      RowBox[{"a", " ", "PI", " ", "x"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"g", "[", "y_", "]"}], " ", "=", 
     RowBox[{"Sin", "[", 
      RowBox[{"b", " ", "PI", " ", "y"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"u", "[", 
      RowBox[{"x_", ",", "y_"}], "]"}], " ", "=", " ", 
     RowBox[{
      RowBox[{"A", " ", 
       RowBox[{"f", "[", "x", "]"}], " ", 
       RowBox[{"g", "[", "y", "]"}]}], "  ", "+", " ", "C"}]}], ";"}], 
   "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.746965266920624*^9, 3.746965290095689*^9}, {
   3.7469653374903383`*^9, 3.746965374401455*^9}, 3.7469654298964453`*^9, {
   3.746965499352722*^9, 3.746965681853582*^9}, {3.7469657203744583`*^9, 
   3.746965775543541*^9}, {3.746965892091379*^9, 3.746965895576724*^9}, {
   3.7469895643484077`*^9, 3.746989571961008*^9}, {3.746989996130821*^9, 
   3.7469900049671707`*^9}, {3.746991641065391*^9, 3.7469916495047417`*^9}, {
   3.749404050760152*^9, 3.749404050803932*^9}, {3.7494041164254923`*^9, 
   3.749404117438053*^9}, {3.74940441714452*^9, 3.7494044189705267`*^9}, {
   3.749404464490638*^9, 3.749404468813402*^9}, 3.749404745416677*^9, {
   3.7494050347055264`*^9, 3.749405036611549*^9}, {3.749405842633115*^9, 
   3.749405854391098*^9}, 3.750158021114443*^9, {3.7567574720700502`*^9, 
   3.756757484972281*^9}, {3.756757585784389*^9, 3.7567576029440727`*^9}, {
   3.7571608673347588`*^9, 3.757160867747429*^9}, {3.758918974885633*^9, 
   3.7589190122032623`*^9}, {3.759064454804091*^9, 3.75906448005801*^9}, {
   3.7628666542859364`*^9, 3.7628666546439877`*^9}, {3.76287797815343*^9, 
   3.762877978208095*^9}, {3.763754185103059*^9, 3.7637541855568542`*^9}, {
   3.763816983144464*^9, 3.763816983380824*^9}, {3.763820033741598*^9, 
   3.7638200338193007`*^9}, {3.763820157751593*^9, 3.763820158125771*^9}, {
   3.763825163778116*^9, 3.763825163830833*^9}, {3.764590667863391*^9, 
   3.7645906690461903`*^9}, {3.764590704845584*^9, 3.76459088688278*^9}, {
   3.7645915211174717`*^9, 3.764591558846713*^9}},
 CellLabel->"In[28]:=",ExpressionUUID->"8ff10dad-60f3-4415-8e67-9a6e95c8a25d"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{"Volume", " ", "Output", " ", "Functional", " ", "solution"}], " ",
    "*)"}], "\[IndentingNewLine]", 
  RowBox[{"J", " ", "=", " ", 
   RowBox[{"Simplify", "[", 
    RowBox[{"Integrate", "[", 
     RowBox[{
      RowBox[{"u", "[", 
       RowBox[{"x", ",", "y"}], "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"x", ",", "0", ",", "1"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"y", " ", ",", "0", ",", "1"}], "}"}]}], "]"}], 
    "]"}]}]}]], "Input",
 CellChangeTimes->{{3.7494044768486013`*^9, 3.749404478566888*^9}, {
   3.749404512316395*^9, 3.7494045428925543`*^9}, {3.7494046367131033`*^9, 
   3.749404678983597*^9}, {3.749405295903439*^9, 3.749405296855926*^9}, {
   3.749405884305381*^9, 3.749405888799534*^9}, {3.750157840757687*^9, 
   3.750157858412465*^9}, {3.756757565223329*^9, 3.75675757094208*^9}, {
   3.757160073545397*^9, 3.75716007427976*^9}, {3.759072924407055*^9, 
   3.759072924628098*^9}, {3.7628780224994183`*^9, 3.7628780408197327`*^9}, 
   3.763817005497213*^9, 3.764590855484119*^9},
 CellLabel->"In[31]:=",ExpressionUUID->"64c52a33-86a5-446e-ba36-5aebd8595af0"],

Cell[BoxData[
 FractionBox[
  RowBox[{"A", "+", 
   RowBox[{"a", " ", "b", " ", "C", " ", 
    SuperscriptBox["PI", "2"]}], "+", 
   RowBox[{"A", " ", 
    RowBox[{"Cos", "[", 
     RowBox[{"a", " ", "PI"}], "]"}], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "1"}], "+", 
      RowBox[{"Cos", "[", 
       RowBox[{"b", " ", "PI"}], "]"}]}], ")"}]}], "-", 
   RowBox[{"A", " ", 
    RowBox[{"Cos", "[", 
     RowBox[{"b", " ", "PI"}], "]"}]}]}], 
  RowBox[{"a", " ", "b", " ", 
   SuperscriptBox["PI", "2"]}]]], "Output",
 CellChangeTimes->{
  3.7494059140321836`*^9, 3.750157866991074*^9, 3.750158026644651*^9, {
   3.75675757759626*^9, 3.756757610477025*^9}, 3.759064308142313*^9, 
   3.7590644390900927`*^9, 3.7590729269584217`*^9, 3.7628561511218023`*^9, 
   3.762877983761454*^9, {3.762878035786824*^9, 3.762878044099043*^9}, 
   3.763754194501726*^9, {3.763816995038587*^9, 3.763817007426118*^9}, 
   3.763820040692712*^9, 3.76382016245993*^9, 3.76459081776687*^9, {
   3.764590849646907*^9, 3.764590913406794*^9}, 3.764591581288753*^9},
 CellLabel->"Out[31]=",ExpressionUUID->"c58d08f5-9291-4539-b8bb-76c62636325c"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"cf", " ", "=", " ", 
   RowBox[{"Compile", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"a", ",", "_Real"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"b", ",", "_Real"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"A", ",", "_Real"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"C", ",", "_Real"}], "}"}]}], "}"}], ",", "J"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"Experimental`OptimizeExpression", "[", "cf", "]"}]}], "Input",
 CellChangeTimes->{{3.764591435563108*^9, 3.7645914926614933`*^9}, {
  3.764591590262866*^9, 3.764591624756654*^9}},
 CellLabel->"In[38]:=",ExpressionUUID->"d8b2ca66-f5a6-4569-87eb-e58e11b7e722"],

Cell[BoxData[
 RowBox[{"Experimental`OptimizedExpression", "[", 
  InterpretationBox[
   RowBox[{
    TagBox["CompiledFunction",
     "SummaryHead"], "[", 
    DynamicModuleBox[{Typeset`open$$ = True, Typeset`embedState$$ = "Ready"}, 
     
     TemplateBox[{PaneSelectorBox[{False -> GridBox[{{
             PaneBox[
              ButtonBox[
               DynamicBox[
                FEPrivate`FrontEndResource[
                "FEBitmaps", "SquarePlusIconMedium"]], 
               ButtonFunction :> (Typeset`open$$ = True), Appearance -> None, 
               Evaluator -> Automatic, Method -> "Preemptive"], 
              Alignment -> {Center, Center}, ImageSize -> 
              Dynamic[{
                Automatic, 3.5 CurrentValue["FontCapHeight"]/
                 AbsoluteCurrentValue[Magnification]}]], 
             GraphicsBox[{
               Thickness[0.038461538461538464`], {
                FaceForm[{
                  GrayLevel[0.93], 
                  Opacity[1.]}], 
                
                FilledCurveBox[{{{1, 4, 3}, {0, 1, 0}, {1, 3, 3}, {0, 1, 0}, {
                 1, 3, 3}, {0, 1, 0}, {1, 3, 3}, {0, 1, 0}}}, {{{
                 25.499999999999996`, 2.5}, {25.499999999999996`, 
                 1.3953100000000003`}, {24.604699999999998`, 
                 0.49999999999999994`}, {23.5, 0.49999999999999994`}, {2.5, 
                 0.49999999999999994`}, {1.3953100000000003`, 
                 0.49999999999999994`}, {0.49999999999999994`, 
                 1.3953100000000003`}, {0.49999999999999994`, 2.5}, {
                 0.49999999999999994`, 23.5}, {0.49999999999999994`, 
                 24.604699999999998`}, {1.3953100000000003`, 
                 25.499999999999996`}, {2.5, 25.499999999999996`}, {23.5, 
                 25.499999999999996`}, {24.604699999999998`, 
                 25.499999999999996`}, {25.499999999999996`, 
                 24.604699999999998`}, {25.499999999999996`, 23.5}, {
                 25.499999999999996`, 2.5}}}]}, {
                FaceForm[{
                  RGBColor[0.5, 0.5, 0.5], 
                  Opacity[1.]}], 
                
                FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {
                 0, 1, 0}, {0, 1, 0}, {0, 1, 0}}}, {{{20.5426, 
                 19.116799999999998`}, {16.3832, 21.876199999999997`}, {
                 16.3832, 20.021499999999996`}, {6.930469999999999, 
                 20.021499999999996`}, {6.930469999999999, 
                 18.212100000000003`}, {16.3832, 18.212100000000003`}, {
                 16.3832, 16.357399999999995`}, {20.5426, 
                 19.116799999999998`}}}], 
                
                FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {
                 0, 1, 0}, {0, 1, 0}, {0, 1, 0}}}, {{{5.30508, 13.8391}, {
                 9.46445, 11.079700000000003`}, {9.46445, 
                 12.933999999999997`}, {18.917199999999998`, 
                 12.933999999999997`}, {18.917199999999998`, 14.7438}, {
                 9.46445, 14.7438}, {9.46445, 16.598}, {5.30508, 13.8391}}}], 
                
                
                FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {
                 0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 
                 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {
                 0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}}}, CompressedData["

1:eJxTTMoPSmVmYGBgBGIRIGYC4r9qIkoPPog5zJd34+ifoeBwf6HGBfU8aYe0
2dKXhaMEHT77F2xc1CQH52elHtL/aqrg0KrArnpmjpRDvTCXYEG9gsOOYKuI
/6tlHZ5WFpRfO4qQn3ww8NOrXYpw/fNmfE3SCFCG8z8EiLwQ7lSF2w82/44y
nM937b/lmXNKDnK7FuxLZZFwMDfq/3NwgpKDccaL+Q9miDok9Mnt3x6k5FDt
mbylT0ECwk9QhOuXWfO/eIajPCq/WNYh5kh50jV+CYc1eV/nfGGShZsXte2a
OGevDNy8ZqG0W/9KpeD60cMLAIwUgfU=
                 "]], 
                
                FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {
                 0, 1, 0}, {0, 1, 0}, {0, 1, 0}}}, {{{12.82453, 
                 5.289294374999999}, {11.960858124999998`, 8.29796625}, {
                 11.080858124999999`, 8.29796625}, {12.417186249999999`, 
                 4.337966249999999}, {13.193201874999998`, 
                 4.337966249999999}, {14.540701874999998`, 8.29796625}, {
                 13.688201874999999`, 8.29796625}, {12.82453, 
                 5.289294374999999}}}], 
                
                FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {
                 0, 1, 0}, {1, 3, 3}, {1, 3, 3}, {0, 1, 0}, {0, 1, 0}, {0, 1, 
                 0}, {1, 3, 3}, {1, 3, 3}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {
                 0, 1, 0}, {0, 1, 0}}}, CompressedData["
1:eJxTTMoPSmVmYGBgBGIpIGYC4qwFP+fsFjB0WFcz7fnBvSIOLFaTza6GGTjM
l3fj6J+h4GC9PHLBXXc9DH7abOnLwlGCDtPNZKaxndfH4Is8/uOrISAD53uI
GTRGxyP4O4KtIv6fR/BbFdhVz8TIOqj9FNyfdEbfIejt5Y8zXiL4h7TunbCJ
koPzweYvkXP4mfT+6PSlBnD7n25Z/mROiSGcr7S/rqPX1AiuHsaHmQfjw+zz
lubzZjYxgrsHxoe5F8aH+QfGn/B91VROQQQfZr/Ei1viF75j8mHh6eiSdevo
cUM4Hz0+AG0eosQ=
                 "]]}}, Background -> GrayLevel[0.93], Axes -> 
              False, AspectRatio -> 1, ImageSize -> {Automatic, 
                Dynamic[
                3.5 (CurrentValue["FontCapHeight"]/AbsoluteCurrentValue[
                  Magnification]), ImageSizeCache -> {45., {0., 9.}}]}, Frame -> 
              True, FrameTicks -> None, FrameStyle -> Directive[
                Thickness[Tiny], 
                GrayLevel[0.7]]], 
             GridBox[{{
                RowBox[{
                  TagBox["\"Argument count: \"", "SummaryItemAnnotation"], 
                  "\[InvisibleSpace]", 
                  TagBox["4", "SummaryItem"]}]}, {
                RowBox[{
                  TagBox["\"Argument types: \"", "SummaryItemAnnotation"], 
                  "\[InvisibleSpace]", 
                  TagBox[
                   StyleBox[
                    PaneBox[
                    RowBox[{"{", 
                    
                    RowBox[{
                    "_Real", ",", "_Real", ",", "_Real", ",", "_Real"}], 
                    "}"}], ContentPadding -> False, FrameMargins -> 0, 
                    StripOnInput -> True, BaselinePosition -> Baseline, 
                    ImageSize -> {{1, 300}, Automatic}], LineBreakWithin -> 
                    False], "SummaryItem"]}]}}, 
              GridBoxAlignment -> {
               "Columns" -> {{Left}}, "Rows" -> {{Automatic}}}, AutoDelete -> 
              False, GridBoxItemSize -> {
               "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}, 
              GridBoxSpacings -> {
               "Columns" -> {{2}}, "Rows" -> {{Automatic}}}, 
              BaseStyle -> {
               ShowStringCharacters -> False, NumberMarks -> False, 
                PrintPrecision -> 3, ShowSyntaxStyles -> False}]}}, 
           GridBoxAlignment -> {"Rows" -> {{Top}}}, AutoDelete -> False, 
           GridBoxItemSize -> {
            "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}, 
           BaselinePosition -> {1, 1}], True -> GridBox[{{
             PaneBox[
              ButtonBox[
               DynamicBox[
                FEPrivate`FrontEndResource[
                "FEBitmaps", "SquareMinusIconMedium"]], 
               ButtonFunction :> (Typeset`open$$ = False), Appearance -> None,
                Evaluator -> Automatic, Method -> "Preemptive"], 
              Alignment -> {Center, Center}, ImageSize -> 
              Dynamic[{
                Automatic, 3.5 CurrentValue["FontCapHeight"]/
                 AbsoluteCurrentValue[Magnification]}]], 
             GraphicsBox[{
               Thickness[0.038461538461538464`], {
                FaceForm[{
                  GrayLevel[0.93], 
                  Opacity[1.]}], 
                
                FilledCurveBox[{{{1, 4, 3}, {0, 1, 0}, {1, 3, 3}, {0, 1, 0}, {
                 1, 3, 3}, {0, 1, 0}, {1, 3, 3}, {0, 1, 0}}}, {{{
                 25.499999999999996`, 2.5}, {25.499999999999996`, 
                 1.3953100000000003`}, {24.604699999999998`, 
                 0.49999999999999994`}, {23.5, 0.49999999999999994`}, {2.5, 
                 0.49999999999999994`}, {1.3953100000000003`, 
                 0.49999999999999994`}, {0.49999999999999994`, 
                 1.3953100000000003`}, {0.49999999999999994`, 2.5}, {
                 0.49999999999999994`, 23.5}, {0.49999999999999994`, 
                 24.604699999999998`}, {1.3953100000000003`, 
                 25.499999999999996`}, {2.5, 25.499999999999996`}, {23.5, 
                 25.499999999999996`}, {24.604699999999998`, 
                 25.499999999999996`}, {25.499999999999996`, 
                 24.604699999999998`}, {25.499999999999996`, 23.5}, {
                 25.499999999999996`, 2.5}}}]}, {
                FaceForm[{
                  RGBColor[0.5, 0.5, 0.5], 
                  Opacity[1.]}], 
                
                FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {
                 0, 1, 0}, {0, 1, 0}, {0, 1, 0}}}, {{{20.5426, 
                 19.116799999999998`}, {16.3832, 21.876199999999997`}, {
                 16.3832, 20.021499999999996`}, {6.930469999999999, 
                 20.021499999999996`}, {6.930469999999999, 
                 18.212100000000003`}, {16.3832, 18.212100000000003`}, {
                 16.3832, 16.357399999999995`}, {20.5426, 
                 19.116799999999998`}}}], 
                
                FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {
                 0, 1, 0}, {0, 1, 0}, {0, 1, 0}}}, {{{5.30508, 13.8391}, {
                 9.46445, 11.079700000000003`}, {9.46445, 
                 12.933999999999997`}, {18.917199999999998`, 
                 12.933999999999997`}, {18.917199999999998`, 14.7438}, {
                 9.46445, 14.7438}, {9.46445, 16.598}, {5.30508, 13.8391}}}], 
                
                
                FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {
                 0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 
                 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {
                 0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}}}, CompressedData["

1:eJxTTMoPSmVmYGBgBGIRIGYC4r9qIkoPPog5zJd34+ifoeBwf6HGBfU8aYe0
2dKXhaMEHT77F2xc1CQH52elHtL/aqrg0KrArnpmjpRDvTCXYEG9gsOOYKuI
/6tlHZ5WFpRfO4qQn3ww8NOrXYpw/fNmfE3SCFCG8z8EiLwQ7lSF2w82/44y
nM937b/lmXNKDnK7FuxLZZFwMDfq/3NwgpKDccaL+Q9miDok9Mnt3x6k5FDt
mbylT0ECwk9QhOuXWfO/eIajPCq/WNYh5kh50jV+CYc1eV/nfGGShZsXte2a
OGevDNy8ZqG0W/9KpeD60cMLAIwUgfU=
                 "]], 
                
                FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {
                 0, 1, 0}, {0, 1, 0}, {0, 1, 0}}}, {{{12.82453, 
                 5.289294374999999}, {11.960858124999998`, 8.29796625}, {
                 11.080858124999999`, 8.29796625}, {12.417186249999999`, 
                 4.337966249999999}, {13.193201874999998`, 
                 4.337966249999999}, {14.540701874999998`, 8.29796625}, {
                 13.688201874999999`, 8.29796625}, {12.82453, 
                 5.289294374999999}}}], 
                
                FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {
                 0, 1, 0}, {1, 3, 3}, {1, 3, 3}, {0, 1, 0}, {0, 1, 0}, {0, 1, 
                 0}, {1, 3, 3}, {1, 3, 3}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {
                 0, 1, 0}, {0, 1, 0}}}, CompressedData["
1:eJxTTMoPSmVmYGBgBGIpIGYC4qwFP+fsFjB0WFcz7fnBvSIOLFaTza6GGTjM
l3fj6J+h4GC9PHLBXXc9DH7abOnLwlGCDtPNZKaxndfH4Is8/uOrISAD53uI
GTRGxyP4O4KtIv6fR/BbFdhVz8TIOqj9FNyfdEbfIejt5Y8zXiL4h7TunbCJ
koPzweYvkXP4mfT+6PSlBnD7n25Z/mROiSGcr7S/rqPX1AiuHsaHmQfjw+zz
lubzZjYxgrsHxoe5F8aH+QfGn/B91VROQQQfZr/Ei1viF75j8mHh6eiSdevo
cUM4Hz0+AG0eosQ=
                 "]]}}, Background -> GrayLevel[0.93], Axes -> 
              False, AspectRatio -> 1, ImageSize -> {Automatic, 
                Dynamic[
                3.5 (CurrentValue["FontCapHeight"]/AbsoluteCurrentValue[
                  Magnification]), ImageSizeCache -> {45., {0., 9.}}]}, Frame -> 
              True, FrameTicks -> None, FrameStyle -> Directive[
                Thickness[Tiny], 
                GrayLevel[0.7]]], 
             GridBox[{{
                RowBox[{
                  TagBox["\"Argument count: \"", "SummaryItemAnnotation"], 
                  "\[InvisibleSpace]", 
                  TagBox["4", "SummaryItem"]}]}, {
                RowBox[{
                  TagBox["\"Argument types: \"", "SummaryItemAnnotation"], 
                  "\[InvisibleSpace]", 
                  TagBox[
                   PaneBox[
                    RowBox[{"{", 
                    
                    RowBox[{
                    "_Real", ",", "_Real", ",", "_Real", ",", "_Real"}], 
                    "}"}], ImageSize -> {{1, 500}, Automatic}, 
                    BaselinePosition -> Baseline, ContentPadding -> False, 
                    FrameMargins -> 0, StripOnInput -> True], 
                   "SummaryItem"]}]}, {
                RowBox[{
                  TagBox["\"Variables: \"", "SummaryItemAnnotation"], 
                  "\[InvisibleSpace]", 
                  TagBox[
                   RowBox[{"{", 
                    RowBox[{"a", ",", "b", ",", "A", ",", "C"}], "}"}], 
                   "SummaryItem"]}]}, {
                RowBox[{
                  TagBox["\"Expression: \"", "SummaryItemAnnotation"], 
                  "\[InvisibleSpace]", 
                  TagBox[
                   PaneBox[
                   "J", ImageSize -> {{1, 500}, Automatic}, BaselinePosition -> 
                    Baseline, ContentPadding -> False, FrameMargins -> 0, 
                    StripOnInput -> True], "SummaryItem"]}]}}, 
              GridBoxAlignment -> {
               "Columns" -> {{Left}}, "Rows" -> {{Automatic}}}, AutoDelete -> 
              False, GridBoxItemSize -> {
               "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}, 
              GridBoxSpacings -> {
               "Columns" -> {{2}}, "Rows" -> {{Automatic}}}, 
              BaseStyle -> {
               ShowStringCharacters -> False, NumberMarks -> False, 
                PrintPrecision -> 3, ShowSyntaxStyles -> False}]}}, 
           GridBoxAlignment -> {"Rows" -> {{Top}}}, AutoDelete -> False, 
           GridBoxItemSize -> {
            "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}, 
           BaselinePosition -> {1, 1}]}, 
        Dynamic[Typeset`open$$], ImageSize -> Automatic]},
      "SummaryPanel"],
     DynamicModuleValues:>{}], "]"}],
   CompiledFunction[{10, 11.3, 5468}, {
     Blank[Real], 
     Blank[Real], 
     Blank[Real], 
     Blank[Real]}, {{3, 0, 0}, {3, 0, 1}, {3, 0, 2}, {3, 0, 3}, {3, 0, 
    5}}, {{-1, {2, 0, 0}}}, {0, 1, 11, 0, 0}, {{46, 
      Function[{$CellContext`a, $CellContext`b, $CellContext`A, 
        C}, $CellContext`J], 3, 0, 0, 3, 0, 1, 3, 0, 2, 3, 0, 3, 3, 0, 5}, {
     1}}, 
    Function[{$CellContext`a, $CellContext`b, $CellContext`A, 
      C}, $CellContext`J], Evaluate],
   Editable->False,
   SelectWithContents->True,
   Selectable->False], "]"}]], "Output",
 CellChangeTimes->{
  3.7645914385755987`*^9, 3.764591488180388*^9, {3.764591584058239*^9, 
   3.76459162545592*^9}},
 CellLabel->"Out[39]=",ExpressionUUID->"a4e2ff8e-288d-4651-b07e-e81025f33bf2"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Simplify", "[", 
  RowBox[{
   FractionBox[
    RowBox[{"A", "+", 
     RowBox[{"a", " ", "b", " ", "C", " ", 
      SuperscriptBox["PI", "2"]}], "+", 
     RowBox[{"A", " ", 
      RowBox[{"Cos", "[", 
       RowBox[{"a", " ", "PI"}], "]"}], " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"-", "1"}], "+", 
        RowBox[{"Cos", "[", 
         RowBox[{"b", " ", "PI"}], "]"}]}], ")"}]}], "-", 
     RowBox[{"A", " ", 
      RowBox[{"Cos", "[", 
       RowBox[{"b", " ", "PI"}], "]"}]}]}], 
    RowBox[{"a", " ", "b", " ", 
     SuperscriptBox["PI", "2"]}]], " ", "-", 
   RowBox[{"(", 
    RowBox[{"C", "+", 
     FractionBox[
      RowBox[{"A", " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"Cos", "[", 
          RowBox[{"a", " ", "PI"}], "]"}], "-", "1"}], ")"}], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"Cos", "[", 
          RowBox[{"b", " ", "PI"}], "]"}], "-", "1"}], ")"}]}], 
      RowBox[{"a", " ", "b", " ", 
       SuperscriptBox["PI", "2"]}]]}], ")"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.7645909673473988`*^9, 3.764591091854396*^9}},
 CellLabel->"In[10]:=",ExpressionUUID->"406faf35-69d5-4028-806d-83925b9453cf"],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{
  3.764590978153842*^9, {3.764591018921537*^9, 3.7645910621052103`*^9}, 
   3.764591092626841*^9},
 CellLabel->"Out[10]=",ExpressionUUID->"36accfd7-9c11-411b-8070-97176620ae72"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{"Volume", " ", "Output", " ", "Functional", " ", "solution"}], " ",
    "*)"}], "\[IndentingNewLine]", 
  RowBox[{"J", " ", "=", " ", 
   RowBox[{"Simplify", "[", 
    RowBox[{"Integrate", "[", 
     RowBox[{
      SuperscriptBox[
       RowBox[{"u", "[", 
        RowBox[{"x", ",", "y"}], "]"}], "2"], ",", 
      RowBox[{"{", 
       RowBox[{"x", ",", "0", ",", "1"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"y", " ", ",", "0", ",", "1"}], "}"}]}], "]"}], 
    "]"}]}]}]], "Input",
 CellChangeTimes->{{3.7494044768486013`*^9, 3.749404478566888*^9}, {
   3.749404512316395*^9, 3.7494045428925543`*^9}, {3.7494046367131033`*^9, 
   3.749404678983597*^9}, {3.749405295903439*^9, 3.749405296855926*^9}, {
   3.749405884305381*^9, 3.749405888799534*^9}, {3.750157840757687*^9, 
   3.750157858412465*^9}, {3.756757565223329*^9, 3.75675757094208*^9}, {
   3.757160073545397*^9, 3.75716007427976*^9}, {3.759072924407055*^9, 
   3.759072924628098*^9}, {3.7628780224994183`*^9, 3.7628780408197327`*^9}, 
   3.763817005497213*^9, 3.764590855484119*^9, {3.764591313490918*^9, 
   3.764591314024413*^9}},
 CellLabel->"In[11]:=",ExpressionUUID->"87c091ea-3531-4075-a893-93d8b11f6a86"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   RowBox[{"32", " ", "A", " ", "C"}], "+", 
   RowBox[{"4", " ", "a", " ", "b", " ", 
    RowBox[{"(", 
     RowBox[{
      SuperscriptBox["A", "2"], "+", 
      RowBox[{"4", " ", 
       SuperscriptBox["C", "2"]}]}], ")"}], " ", 
    SuperscriptBox["PI", "2"]}], "+", 
   RowBox[{"A", " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       RowBox[{"-", "64"}], " ", "C", " ", 
       RowBox[{"Cos", "[", 
        RowBox[{"b", " ", "PI"}], "]"}], " ", 
       SuperscriptBox[
        RowBox[{"Sin", "[", 
         FractionBox[
          RowBox[{"a", " ", "PI"}], "2"], "]"}], "2"]}], "-", 
      RowBox[{"4", " ", 
       RowBox[{"Cos", "[", 
        RowBox[{"a", " ", "PI"}], "]"}], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"8", " ", "C"}], "+", 
         RowBox[{"A", " ", "b", " ", "PI", " ", 
          RowBox[{"Sin", "[", 
           RowBox[{"a", " ", "PI"}], "]"}]}]}], ")"}]}], "+", 
      RowBox[{"A", " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{
          RowBox[{"-", "2"}], " ", "a", " ", "PI"}], "+", 
         RowBox[{"Sin", "[", 
          RowBox[{"2", " ", "a", " ", "PI"}], "]"}]}], ")"}], " ", 
       RowBox[{"Sin", "[", 
        RowBox[{"2", " ", "b", " ", "PI"}], "]"}]}]}], ")"}]}]}], 
  RowBox[{"16", " ", "a", " ", "b", " ", 
   SuperscriptBox["PI", "2"]}]]], "Output",
 CellChangeTimes->{
  3.7494059140321836`*^9, 3.750157866991074*^9, 3.750158026644651*^9, {
   3.75675757759626*^9, 3.756757610477025*^9}, 3.759064308142313*^9, 
   3.7590644390900927`*^9, 3.7590729269584217`*^9, 3.7628561511218023`*^9, 
   3.762877983761454*^9, {3.762878035786824*^9, 3.762878044099043*^9}, 
   3.763754194501726*^9, {3.763816995038587*^9, 3.763817007426118*^9}, 
   3.763820040692712*^9, 3.76382016245993*^9, 3.76459081776687*^9, {
   3.764590849646907*^9, 3.764590913406794*^9}, 3.764591319474305*^9},
 CellLabel->"Out[11]=",ExpressionUUID->"fa3fd297-328e-49b1-8bbc-0342da2b1211"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Simplify", "[", 
  RowBox[{
   FractionBox[
    RowBox[{"A", "+", 
     RowBox[{"a", " ", "b", " ", "C", " ", 
      SuperscriptBox["PI", "2"]}], "+", 
     RowBox[{"A", " ", 
      RowBox[{"Cos", "[", 
       RowBox[{"a", " ", "PI"}], "]"}], " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"-", "1"}], "+", 
        RowBox[{"Cos", "[", 
         RowBox[{"b", " ", "PI"}], "]"}]}], ")"}]}], "-", 
     RowBox[{"A", " ", 
      RowBox[{"Cos", "[", 
       RowBox[{"b", " ", "PI"}], "]"}]}]}], 
    RowBox[{"a", " ", "b", " ", 
     SuperscriptBox["PI", "2"]}]], " ", "-", 
   RowBox[{"(", 
    RowBox[{"C", "+", 
     FractionBox[
      RowBox[{"A", " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"Cos", "[", 
          RowBox[{"a", " ", "PI"}], "]"}], "-", "1"}], ")"}], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"Cos", "[", 
          RowBox[{"b", " ", "PI"}], "]"}], "-", "1"}], ")"}]}], 
      RowBox[{"a", " ", "b", " ", 
       SuperscriptBox["PI", "2"]}]]}], ")"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.7645909673473988`*^9, 3.764591091854396*^9}},
 CellLabel->"In[10]:=",ExpressionUUID->"e5be78fe-bca3-41c6-8877-2fddd74a84d4"],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{
  3.764590978153842*^9, {3.764591018921537*^9, 3.7645910621052103`*^9}, 
   3.764591092626841*^9},
 CellLabel->"Out[10]=",ExpressionUUID->"b79fe46c-b13b-40b4-bd6c-38845dc53ce5"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{
     RowBox[{
     "Boundary", " ", "Output", " ", "Functional", " ", "on", " ", "the", " ",
       "x"}], " ", "=", " ", 
     RowBox[{
      RowBox[{"1", " ", "and", " ", "y"}], " ", "=", " ", 
      RowBox[{"1", " ", "faces"}]}]}], ",", " ", 
    RowBox[{"weighted", " ", "by", " ", "a", " ", "quadratic"}]}], " ", 
   "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"ux", "[", 
      RowBox[{"x_", ",", "y_"}], "]"}], " ", "=", " ", 
     RowBox[{"D", "[", 
      RowBox[{
       RowBox[{"u", "[", 
        RowBox[{"x", ",", "y"}], "]"}], ",", "x"}], "]"}]}], ";", " ", 
    RowBox[{
     RowBox[{"uy", "[", 
      RowBox[{"x_", ",", "y_"}], "]"}], " ", "=", " ", 
     RowBox[{"D", "[", 
      RowBox[{
       RowBox[{"u", "[", 
        RowBox[{"x", ",", "y"}], "]"}], ",", "y"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Fa", "[", 
      RowBox[{"x_", ",", "y_"}], "]"}], " ", "=", " ", 
     RowBox[{
      RowBox[{"u", "[", 
       RowBox[{"x", ",", "y"}], "]"}], " ", 
      RowBox[{"{", 
       RowBox[{"a", ",", "b"}], "}"}]}]}], ";", " ", 
    RowBox[{
     RowBox[{"Fv", "[", 
      RowBox[{"x_", ",", "y_"}], "]"}], " ", "=", " ", 
     RowBox[{
      RowBox[{"-", "\[Nu]"}], 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"ux", "[", 
         RowBox[{"x", ",", "y"}], "]"}], ",", " ", 
        RowBox[{"uy", "[", 
         RowBox[{"x", ",", "y"}], "]"}]}], "}"}]}]}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"w", "[", 
      RowBox[{"x_", ",", "y_"}], "]"}], " ", "=", " ", 
     RowBox[{"y", " ", "x"}]}], ";"}], "\[IndentingNewLine]", 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"nx", " ", "=", " ", 
     RowBox[{"{", 
      RowBox[{"1", ",", " ", "0"}], "}"}]}], ";", " ", 
    RowBox[{"ny", " ", "=", " ", 
     RowBox[{"{", 
      RowBox[{"0", ",", "1"}], "}"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Jx", " ", "=", " ", 
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"w", " ", "[", 
         RowBox[{"1", ",", "y"}], "]"}], 
        RowBox[{
         RowBox[{"(", 
          RowBox[{
           RowBox[{"Fa", "[", 
            RowBox[{"1", ",", "y"}], "]"}], " ", "+", " ", 
           RowBox[{"Fv", "[", 
            RowBox[{"1", ",", "y"}], "]"}]}], " ", ")"}], ".", "nx"}]}], ",", 
       
       RowBox[{"{", 
        RowBox[{"y", ",", "0", ",", "1"}], "}"}]}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Jy", " ", "=", " ", 
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"w", " ", "[", 
         RowBox[{"x", ",", "1"}], "]"}], 
        RowBox[{
         RowBox[{"(", 
          RowBox[{
           RowBox[{"Fa", "[", 
            RowBox[{"x", ",", "1"}], "]"}], " ", "+", " ", 
           RowBox[{"Fv", "[", 
            RowBox[{"x", ",", "1"}], "]"}]}], " ", ")"}], ".", "ny"}]}], ",", 
       
       RowBox[{"{", 
        RowBox[{"x", ",", "0", ",", "1"}], "}"}]}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{"J", " ", "=", " ", 
    RowBox[{"Simplify", "[", "  ", 
     RowBox[{"Jx", " ", "+", " ", "Jy"}], "  ", "]"}]}], 
   "\[IndentingNewLine]", 
   RowBox[{"N", "[", 
    RowBox[{"J", ",", "20"}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.749402607583415*^9, 3.7494026397750473`*^9}, {
   3.749402676337385*^9, 3.749402707304421*^9}, {3.7494027590256157`*^9, 
   3.7494027765373373`*^9}, {3.749402820644176*^9, 3.749402925323412*^9}, {
   3.749402962837289*^9, 3.7494029737395983`*^9}, {3.7494033947269707`*^9, 
   3.74940345576365*^9}, {3.7494034912460823`*^9, 3.749403533732531*^9}, {
   3.7494039402552557`*^9, 3.749403969373014*^9}, {3.749404159855962*^9, 
   3.7494044506996107`*^9}, {3.749404567085166*^9, 3.749404573524551*^9}, 
   3.7494046964640417`*^9, {3.75675762807332*^9, 3.756757730024239*^9}, {
   3.7568226495245733`*^9, 3.756822733244931*^9}, {3.7571593777628736`*^9, 
   3.757159494559993*^9}, {3.75716008201764*^9, 3.757160104168212*^9}, {
   3.7590729781762657`*^9, 3.7590729782448893`*^9}},
 CellLabel->"In[38]:=",ExpressionUUID->"58fc506d-a6a8-4067-b966-8683b2def98c"],

Cell[BoxData[
 FractionBox[
  RowBox[{"5000", "-", 
   RowBox[{"9999", " ", 
    SuperscriptBox["\[ExponentialE]", "100"]}], "+", 
   RowBox[{"99", " ", 
    SuperscriptBox["\[ExponentialE]", "200"]}]}], 
  RowBox[{"5000", " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "1"}], "+", 
      SuperscriptBox["\[ExponentialE]", "100"]}], ")"}], "2"]}]]], "Output",
 CellChangeTimes->{
  3.749403019903942*^9, {3.749403439712821*^9, 3.7494034580144453`*^9}, {
   3.7494035063696337`*^9, 3.749403535641539*^9}, 3.749404325959279*^9, 
   3.749404397982914*^9, 3.749404714127014*^9, 3.749404766123123*^9, 
   3.74940492774511*^9, 3.749405129589617*^9, 3.7494059423002367`*^9, 
   3.7501582175083227`*^9, {3.7567577080493917`*^9, 3.756757730673896*^9}, 
   3.756758280233259*^9, {3.756822683840887*^9, 3.7568227083477097`*^9}, 
   3.756822739956251*^9, 3.75715949761944*^9, 3.7571608721373253`*^9, {
   3.758918961747961*^9, 3.75891900301033*^9}, {3.759064422987516*^9, 
   3.759064481805615*^9}, {3.759072975346642*^9, 3.759072979884624*^9}, 
   3.763754195027594*^9, 3.763816995232284*^9, 3.76382004099378*^9, 
   3.763820162845916*^9},
 CellLabel->"Out[44]=",ExpressionUUID->"ada66be8-2637-4d33-9501-f59d8ad4f182"],

Cell[BoxData["0.01980000000000000000000000000000071658`20."], "Output",
 CellChangeTimes->{
  3.749403019903942*^9, {3.749403439712821*^9, 3.7494034580144453`*^9}, {
   3.7494035063696337`*^9, 3.749403535641539*^9}, 3.749404325959279*^9, 
   3.749404397982914*^9, 3.749404714127014*^9, 3.749404766123123*^9, 
   3.74940492774511*^9, 3.749405129589617*^9, 3.7494059423002367`*^9, 
   3.7501582175083227`*^9, {3.7567577080493917`*^9, 3.756757730673896*^9}, 
   3.756758280233259*^9, {3.756822683840887*^9, 3.7568227083477097`*^9}, 
   3.756822739956251*^9, 3.75715949761944*^9, 3.7571608721373253`*^9, {
   3.758918961747961*^9, 3.75891900301033*^9}, {3.759064422987516*^9, 
   3.759064481805615*^9}, {3.759072975346642*^9, 3.759072979884624*^9}, 
   3.763754195027594*^9, 3.763816995232284*^9, 3.76382004099378*^9, 
   3.763820162848853*^9},
 CellLabel->"Out[45]=",ExpressionUUID->"540fd009-06b1-4218-8a86-6e3b2d17d9d8"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData["C"], "Input",
 CellChangeTimes->{3.76459089549856*^9},
 CellLabel->"In[15]:=",ExpressionUUID->"1e8ae551-28d1-48c7-afda-97dbd7b3b9cd"],

Cell[BoxData["C"], "Output",
 CellChangeTimes->{3.764590895998797*^9},
 CellLabel->"Out[15]=",ExpressionUUID->"efaff187-f846-48c3-85a1-9f6e890a4132"]
}, Open  ]]
},
WindowSize->{1879, 2098},
WindowMargins->{{Automatic, 0}, {0, Automatic}},
FrontEndVersion->"11.3 for Linux x86 (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 3029, 64, 201, "Input",ExpressionUUID->"8ff10dad-60f3-4415-8e67-9a6e95c8a25d"],
Cell[CellGroupData[{
Cell[3612, 88, 1158, 24, 57, "Input",ExpressionUUID->"64c52a33-86a5-446e-ba36-5aebd8595af0"],
Cell[4773, 114, 1134, 26, 61, "Output",ExpressionUUID->"c58d08f5-9291-4539-b8bb-76c62636325c"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5944, 145, 742, 19, 57, "Input",ExpressionUUID->"d8b2ca66-f5a6-4569-87eb-e58e11b7e722"],
Cell[6689, 166, 15280, 293, 91, "Output",ExpressionUUID->"a4e2ff8e-288d-4651-b07e-e81025f33bf2"]
}, Open  ]],
Cell[CellGroupData[{
Cell[22006, 464, 1204, 35, 63, "Input",ExpressionUUID->"406faf35-69d5-4028-806d-83925b9453cf"],
Cell[23213, 501, 226, 4, 36, "Output",ExpressionUUID->"36accfd7-9c11-411b-8070-97176620ae72"]
}, Open  ]],
Cell[CellGroupData[{
Cell[23476, 510, 1238, 26, 65, "Input",ExpressionUUID->"87c091ea-3531-4075-a893-93d8b11f6a86"],
Cell[24717, 538, 1975, 50, 69, "Output",ExpressionUUID->"fa3fd297-328e-49b1-8bbc-0342da2b1211"]
}, Open  ]],
Cell[CellGroupData[{
Cell[26729, 593, 1204, 35, 63, "Input",ExpressionUUID->"e5be78fe-bca3-41c6-8877-2fddd74a84d4"],
Cell[27936, 630, 226, 4, 36, "Output",ExpressionUUID->"b79fe46c-b13b-40b4-bd6c-38845dc53ce5"]
}, Open  ]],
Cell[CellGroupData[{
Cell[28199, 639, 4268, 117, 273, "Input",ExpressionUUID->"58fc506d-a6a8-4067-b966-8683b2def98c"],
Cell[32470, 758, 1236, 25, 68, "Output",ExpressionUUID->"ada66be8-2637-4d33-9501-f59d8ad4f182"],
Cell[33709, 785, 919, 13, 36, "Output",ExpressionUUID->"540fd009-06b1-4218-8a86-6e3b2d17d9d8"]
}, Open  ]],
Cell[CellGroupData[{
Cell[34665, 803, 147, 2, 32, "Input",ExpressionUUID->"1e8ae551-28d1-48c7-afda-97dbd7b3b9cd"],
Cell[34815, 807, 149, 2, 36, "Output",ExpressionUUID->"efaff187-f846-48c3-85a1-9f6e890a4132"]
}, Open  ]]
}
]
*)

