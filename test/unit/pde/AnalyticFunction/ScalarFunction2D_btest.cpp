// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ScalarFunction2D_btest
//
// test of 2D scalar analytic functions

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/SolutionFunction2D.h"

#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ScalarFunction2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_Combination_test )
{
  typedef ScalarFunction2D_Const::template ArrayQ<Real> ArrayQ;

  ScalarFunction2D_Const fcn1( 1 );
  ScalarFunction2D_Const fcn2( 2 );
  ScalarFunction2D_Combination<ScalarFunction2D_Const,ScalarFunction2D_Const> cfcn(1,fcn1,1,fcn2);

  Real x = 1;
  Real y = 2;
  Real time = 0;
  ArrayQ sln;
  sln = cfcn(x, y, time);

  const Real tol = 1.e-13;
  BOOST_CHECK_CLOSE( 3, sln, tol );


  Real a0=1.1,a1=2.2, a2=3.7;
  Real b0=4.3,b1=-0.3,b2=-2.3;
  ScalarFunction2D_Linear fcn3( a0, a1, a2 );
  ScalarFunction2D_Linear fcn4( b0, b1, b2 );

  ScalarFunction2D_Combination<ScalarFunction2D_Linear, ScalarFunction2D_Linear> cfcn2(1.1,fcn3,0.9,fcn4);

  Real truth = 1.1*(a0 + x*a1 + y*a2) + 0.9*(b0 + x*b1 + y*b2);
  sln = cfcn2(x,y,time);
  SANS_CHECK_CLOSE(truth, sln, tol, tol );

  x = 3; y = -2;
  truth = 1.1*(a0 + x*a1 + y*a2) + 0.9*(b0 + x*b1 + y*b2);
  sln = cfcn2(x,y,time);
  SANS_CHECK_CLOSE(truth, sln, tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_Const_test )
{
  typedef ScalarFunction2D_Const::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  {
    ScalarFunction2D_Const fcn( 1 );

    Real x = 1;
    Real y = 2;
    Real time = 0;
    ArrayQ sln;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( 1, sln, tol );
  }

  {
    PyDict d;
    d[ScalarFunction2D_Const::ParamsType::params.a0] = 0.5;
    ScalarFunction2D_Const::ParamsType::checkInputs(d);
    ScalarFunction2D_Const fcn(d);

    Real x = 1;
    Real y = 2;
    Real time = 0;
    ArrayQ sln;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( 0.5, sln, tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_Linear_test )
{
  typedef ScalarFunction2D_Linear::template ArrayQ<Real> ArrayQ;
  const Real tol = 1.e-13;

  Real x, y, time;
  ArrayQ sln;

  Real a0 =  1;
  Real ax = -3;
  Real ay =  4;

  {
    ScalarFunction2D_Linear fcn( a0, ax, ay );
    x = 0;
    y = 0;
    time = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a0, sln, tol );

    x = 1;
    y = 2;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a0 + ax*x + ay*y, sln, tol );
  }

  {
    PyDict d;
    d[ScalarFunction2D_Linear::ParamsType::params.a0] = a0;
    d[ScalarFunction2D_Linear::ParamsType::params.ax] = ax;
    d[ScalarFunction2D_Linear::ParamsType::params.ay] = ay;
    ScalarFunction2D_Linear::ParamsType::checkInputs(d);
    ScalarFunction2D_Linear fcn(d);

    x = 0;
    y = 0;
    time = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a0, sln, tol );

    x = 1;
    y = 2;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a0 + ax*x + ay*y, sln, tol );
  }
}



//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_Quadratic_test )
{
  typedef ScalarFunction2D_Quadratic::template ArrayQ<Real> ArrayQ;
  const Real tol = 1.e-13;

  Real x, y, time;
  ArrayQ sln;

  Real a0 =  1;
  Real ax = -3;
  Real ay =  4;
  Real axx = -2.1;
  Real ayy = 0.3;
  Real axy = 0.7;

  {
    ScalarFunction2D_Quadratic fcn( a0, ax, ay, axx, ayy, axy );
    x = 0;
    y = 0;
    time = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a0, sln, tol );

    x = 1;
    y = 2;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a0 + ax*x + ay*y + axx*x*x + ayy*y*y + axy*x*y, sln, tol );

    x = -1;
    y = 0.5;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a0 + ax*x + ay*y + axx*x*x + ayy*y*y + axy*x*y, sln, tol );
  }

  {
    PyDict d;
    d[ScalarFunction2D_Quadratic::ParamsType::params.a0] = a0;
    d[ScalarFunction2D_Quadratic::ParamsType::params.ax] = ax;
    d[ScalarFunction2D_Quadratic::ParamsType::params.ay] = ay;
    d[ScalarFunction2D_Quadratic::ParamsType::params.axx] = axx;
    d[ScalarFunction2D_Quadratic::ParamsType::params.ayy] = ayy;
    d[ScalarFunction2D_Quadratic::ParamsType::params.axy] = axy;
    ScalarFunction2D_Quadratic::ParamsType::checkInputs(d);
    ScalarFunction2D_Quadratic fcn(d);

    x = 0;
    y = 0;
    time = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a0 + ax*x + ay*y + axx*x*x + ayy*y*y + axy*x*y, sln, tol );

    x = 1;
    y = 2;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a0 + ax*x + ay*y + axx*x*x + ayy*y*y + axy*x*y, sln, tol );

    x = -1;
    y = 0.5;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a0 + ax*x + ay*y + axx*x*x + ayy*y*y + axy*x*y, sln, tol );
  }
}



//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_PiecewiseConstBlock_test )
{
  typedef ScalarFunction2D_PiecewiseConstBlock::template ArrayQ<Real> ArrayQ;
  const Real tol = 1.e-13;

  Real x, y, time;
  ArrayQ sln;

  Real a00 = 0.1;
  Real a01 = 0.2;
  Real a10 = 0.3;
  Real a11 = 0.4;
  Real x0 = 0.0;
  Real y0 = 0.0;

  {
    ScalarFunction2D_PiecewiseConstBlock fcn( a00, a01, a10, a11, x0, y0 );
    time = 0;
    x = -0.5; y = 0.5;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a00, sln, tol );

    x = 0.5; y = 0.5;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a01, sln, tol );

    x = -0.5; y = -0.5;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a10, sln, tol );

    x = 0.5; y = -0.5;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a11, sln, tol );
  }

  {
    PyDict d;
    d[ScalarFunction2D_PiecewiseConstBlock::ParamsType::params.a00] = a00;
    d[ScalarFunction2D_PiecewiseConstBlock::ParamsType::params.a01] = a01;
    d[ScalarFunction2D_PiecewiseConstBlock::ParamsType::params.a10] = a10;
    d[ScalarFunction2D_PiecewiseConstBlock::ParamsType::params.a11] = a11;
    d[ScalarFunction2D_PiecewiseConstBlock::ParamsType::params.x0] = x0;
    d[ScalarFunction2D_PiecewiseConstBlock::ParamsType::params.y0] = y0;
    ScalarFunction2D_PiecewiseConstBlock::ParamsType::checkInputs(d);
    ScalarFunction2D_PiecewiseConstBlock fcn(d);

    time = 0;
    x = -0.5; y = 0.5;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a00, sln, tol );

    x = 0.5; y = 0.5;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a01, sln, tol );

    x = -0.5; y = -0.5;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a10, sln, tol );

    x = 0.5; y = -0.5;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a11, sln, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_PiecewiseConstDiag_test )
{
  typedef ScalarFunction2D_PiecewiseConstDiag::template ArrayQ<Real> ArrayQ;
  const Real tol = 1.e-13;

  Real x, y, time;
  ArrayQ sln;

  Real a0 = 0.1;
  Real a1 = 0.2;
  Real m = 2;
  Real c = 1;

  {
    ScalarFunction2D_PiecewiseConstDiag fcn( a0, a1, m, c );
    time = 0;
    x = -0.5; y = 0.5;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a0, sln, tol );

    x = -0.5; y = -0.2;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a1, sln, tol );

    x = 1.0; y = 3.1;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a0, sln, tol );

    x = 1.0; y = 2.9;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a1, sln, tol );
  }

  {
    PyDict d;
    d[ScalarFunction2D_PiecewiseConstDiag::ParamsType::params.a0] = a0;
    d[ScalarFunction2D_PiecewiseConstDiag::ParamsType::params.a1] = a1;
    d[ScalarFunction2D_PiecewiseConstDiag::ParamsType::params.m] = m;
    d[ScalarFunction2D_PiecewiseConstDiag::ParamsType::params.c] = c;
    ScalarFunction2D_PiecewiseConstDiag::ParamsType::checkInputs(d);
    ScalarFunction2D_PiecewiseConstDiag fcn(d);

    time = 0;
    x = -0.5; y = 0.5;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a0, sln, tol );

    x = -0.5; y = -0.2;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a1, sln, tol );

    x = 1.0; y = 3.1;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a0, sln, tol );

    x = 1.0; y = 2.9;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a1, sln, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_Monomial_test )
{
  typedef ScalarFunction2D_Monomial::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;
  Real x, y, time;
  ArrayQ sln, slnTrue;
  int i = 2, j = 3;

  {
    ScalarFunction2D_Monomial fcn(i,j);

    x = 0;
    y = 0;
    time = 0;

    slnTrue = pow(x, i)*pow(y, j);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 1;
    y = 1;
    slnTrue = pow(x, i)*pow(y, j);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.5;
    y = 1;
    slnTrue = pow(x, i)*pow(y, j);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.25;
    y = 0.25;
    slnTrue = pow(x, i)*pow(y, j);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );
  }

  {
    PyDict d;
    d[ScalarFunction2D_Monomial::ParamsType::params.i] = i;
    d[ScalarFunction2D_Monomial::ParamsType::params.j] = j;
    ScalarFunction2D_Monomial::ParamsType::checkInputs(d);
    ScalarFunction2D_Monomial fcn(d);

    x = 0;
    y = 0;
    time = 0;

    slnTrue = pow(x, i)*pow(y, j);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 1;
    y = 1;
    slnTrue = pow(x, i)*pow(y, j);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.5;
    y = 1;
    slnTrue = pow(x, i)*pow(y, j);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.25;
    y = 0.25;
    slnTrue = pow(x, i)*pow(y, j);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_Cubic_test )
{
  typedef ScalarFunction2D_Cubic::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;
  Real x, y, time;
  ArrayQ sln, slnTrue;

  {
    ScalarFunction2D_Cubic fcn;

    x = 0;
    y = 0;
    time = 0;

    slnTrue = 27*x*y*(1-x-y);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 1;
    y = 1;
    slnTrue = 27*x*y*(1-x-y);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.5;
    y = 1;
    slnTrue = 27*x*y*(1-x-y);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.25;
    y = 0.25;
    slnTrue = 27*x*y*(1-x-y);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );
  }

  {
    PyDict d;
    ScalarFunction2D_Cubic::ParamsType::checkInputs(d);
    ScalarFunction2D_Cubic fcn(d);

    x = 0;
    y = 0;
    time = 0;

    slnTrue = 27*x*y*(1-x-y);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 1;
    y = 1;
    slnTrue = 27*x*y*(1-x-y);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.5;
    y = 1;
    slnTrue = 27*x*y*(1-x-y);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.25;
    y = 0.25;
    slnTrue = 27*x*y*(1-x-y);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );
  }
}

//-------------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_ASExp_test )
{
  typedef ScalarFunction2D_ASExp::template ArrayQ<Real> ArrayQ;

  //ScalarFunctionClass fcn;

  const Real tol = 1.e-13;

  Real x, y, time;
  ArrayQ sln, sln_true;

  Real u0, x0, y0, a, b,  alpha;
  u0 = 2.;
  x0 = 1.2; y0 =  2.1;
  a  = 1.1; b  = -0.1;
  alpha = 3.4;

  PyDict d;
  d[ScalarFunction2D_ASExp::ParamsType::params.u0] = u0;
  d[ScalarFunction2D_ASExp::ParamsType::params.x0] = x0;
  d[ScalarFunction2D_ASExp::ParamsType::params.y0] = y0;
  d[ScalarFunction2D_ASExp::ParamsType::params.a] = a;
  d[ScalarFunction2D_ASExp::ParamsType::params.b] = b;
  d[ScalarFunction2D_ASExp::ParamsType::params.alpha] = alpha;

  {
    ScalarFunction2D_ASExp::ParamsType::checkInputs(d);
    ScalarFunction2D_ASExp fcn(d);

    x = 0;
    y = 0;
    sln = fcn(x, y, time);
    sln_true = u0*::max( exp(alpha*(x0 - x)/a), exp(alpha*(y0 - y)/b) );
    BOOST_CHECK_CLOSE( sln_true, sln, tol );

    x = 1;
    y = 0;
    sln = fcn(x, y, time);
    sln_true = u0*::max( exp(alpha*(x0 - x)/a), exp(alpha*(y0 - y)/b) );
    BOOST_CHECK_CLOSE( sln_true, sln, tol );

    x = 0;
    y = 1;
    sln = fcn(x, y, time);
    sln_true = u0*::max( exp(alpha*(x0 - x)/a), exp(alpha*(y0 - y)/b) );
    BOOST_CHECK_CLOSE( sln_true, sln, tol );

    x = 1;
    y = 0.25;
    sln = fcn(x, y, time);
    sln_true = u0*::max( exp(alpha*(x0 - x)/a), exp(alpha*(y0 - y)/b) );
    BOOST_CHECK_CLOSE( sln_true, sln, tol );

    x = 0;
    y = 0.5;
    sln = fcn(x, y, time);
    sln_true = u0*::max( exp(alpha*(x0 - x)/a), exp(alpha*(y0 - y)/b) );
    BOOST_CHECK_CLOSE( sln_true, sln, tol );

    x = 0.66; y = 0.75;
    sln = fcn(x, y, time);
    sln_true = u0*::max( exp(alpha*(x0 - x)/a), exp(alpha*(y0 - y)/b) );
    BOOST_CHECK_CLOSE( sln_true, sln, tol );
  }

  {
    ScalarFunction2D_ASExp fcn(u0,x0,y0,a,b,alpha);

    x = 0;
    y = 0;
    sln = fcn(x, y, time);
    sln_true = u0*::max( exp(alpha*(x0 - x)/a), exp(alpha*(y0 - y)/b) );
    BOOST_CHECK_CLOSE( sln_true, sln, tol );

    x = 1;
    y = 0;
    sln = fcn(x, y, time);
    sln_true = u0*::max( exp(alpha*(x0 - x)/a), exp(alpha*(y0 - y)/b) );
    BOOST_CHECK_CLOSE( sln_true, sln, tol );

    x = 0;
    y = 1;
    sln = fcn(x, y, time);
    sln_true = u0*::max( exp(alpha*(x0 - x)/a), exp(alpha*(y0 - y)/b) );
    BOOST_CHECK_CLOSE( sln_true, sln, tol );

    x = 1;
    y = 0.25;
    sln = fcn(x, y, time);
    sln_true = u0*::max( exp(alpha*(x0 - x)/a), exp(alpha*(y0 - y)/b) );
    BOOST_CHECK_CLOSE( sln_true, sln, tol );

    x = 0;
    y = 0.5;
    sln = fcn(x, y, time);
    sln_true = u0*::max( exp(alpha*(x0 - x)/a), exp(alpha*(y0 - y)/b) );
    BOOST_CHECK_CLOSE( sln_true, sln, tol );

    x = 0.66; y = 0.75;
    sln = fcn(x, y, time);
    sln_true = u0*::max( exp(alpha*(x0 - x)/a), exp(alpha*(y0 - y)/b) );
    BOOST_CHECK_CLOSE( sln_true, sln, tol );
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_VarExp_test )
{
  typedef ScalarFunction2D_VarExp::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;
  Real x, y, time;
  ArrayQ sln, slnTrue;

  Real a = 2, b = 3, c = 4, d = 5, e = 6;

  {
    ScalarFunction2D_VarExp fcn(a,b,c,d,e);

    x = 0;
    y = 0;
    time = 0;

    slnTrue = x*y*exp(a*x*x + c*x + b*y*y + d*y + e)*(x - 1)*(y - 1);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 1;
    y = 1;
    slnTrue = x*y*exp(a*x*x + c*x + b*y*y + d*y + e)*(x - 1)*(y - 1);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.5;
    y = 1;
    slnTrue = x*y*exp(a*x*x + c*x + b*y*y + d*y + e)*(x - 1)*(y - 1);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.25;
    y = 0.25;
    slnTrue = x*y*exp(a*x*x + c*x + b*y*y + d*y + e)*(x - 1)*(y - 1);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );
  }

  {
    PyDict dict;
    dict[ScalarFunction2D_VarExp::ParamsType::params.a] = a;
    dict[ScalarFunction2D_VarExp::ParamsType::params.b] = b;
    dict[ScalarFunction2D_VarExp::ParamsType::params.c] = c;
    dict[ScalarFunction2D_VarExp::ParamsType::params.d] = d;
    dict[ScalarFunction2D_VarExp::ParamsType::params.e] = e;
    ScalarFunction2D_VarExp::ParamsType::checkInputs(dict);
    ScalarFunction2D_VarExp fcn(dict);

    x = 0;
    y = 0;
    time = 0;

    slnTrue = x*y*exp(a*x*x + c*x + b*y*y + d*y + e)*(x - 1)*(y - 1);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 1;
    y = 1;
    slnTrue = x*y*exp(a*x*x + c*x + b*y*y + d*y + e)*(x - 1)*(y - 1);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.5;
    y = 1;
    slnTrue = x*y*exp(a*x*x + c*x + b*y*y + d*y + e)*(x - 1)*(y - 1);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.25;
    y = 0.25;
    slnTrue = x*y*exp(a*x*x + c*x + b*y*y + d*y + e)*(x - 1)*(y - 1);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_VarExp2_test )
{
  typedef ScalarFunction2D_VarExp2::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;
  Real x, y, time;
  ArrayQ sln, slnTrue;

  Real a = 2, b = 3, c = 4, d = 5;

  {
    ScalarFunction2D_VarExp2 fcn(a,b,c,d);

    x = 0;
    y = 0;
    time = 0;

    slnTrue = (1-exp(a*x))*(1-exp(b*y))*(1-exp(c*(1-x)))*(1-exp(d*(1-y)));
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 1;
    y = 1;
    slnTrue = (1-exp(a*x))*(1-exp(b*y))*(1-exp(c*(1-x)))*(1-exp(d*(1-y)));
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.5;
    y = 1;
    slnTrue = (1-exp(a*x))*(1-exp(b*y))*(1-exp(c*(1-x)))*(1-exp(d*(1-y)));
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.25;
    y = 0.25;
    slnTrue = (1-exp(a*x))*(1-exp(b*y))*(1-exp(c*(1-x)))*(1-exp(d*(1-y)));
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );
  }

  {
    PyDict dict;
    dict[ScalarFunction2D_VarExp2::ParamsType::params.a] = a;
    dict[ScalarFunction2D_VarExp2::ParamsType::params.b] = b;
    dict[ScalarFunction2D_VarExp2::ParamsType::params.c] = c;
    dict[ScalarFunction2D_VarExp2::ParamsType::params.d] = d;
    ScalarFunction2D_VarExp2::ParamsType::checkInputs(dict);
    ScalarFunction2D_VarExp2 fcn(dict);

    x = 0;
    y = 0;
    time = 0;

    slnTrue = (1-exp(a*x))*(1-exp(b*y))*(1-exp(c*(1-x)))*(1-exp(d*(1-y)));
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 1;
    y = 1;
    slnTrue = (1-exp(a*x))*(1-exp(b*y))*(1-exp(c*(1-x)))*(1-exp(d*(1-y)));
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.5;
    y = 1;
    slnTrue = (1-exp(a*x))*(1-exp(b*y))*(1-exp(c*(1-x)))*(1-exp(d*(1-y)));
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.25;
    y = 0.25;
    slnTrue = (1-exp(a*x))*(1-exp(b*y))*(1-exp(c*(1-x)))*(1-exp(d*(1-y)));
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_VarExp3_test )
{
  typedef ScalarFunction2D_VarExp3::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;
  Real x, y, time;
  ArrayQ sln, slnTrue;

  Real a = 2;

  {
    ScalarFunction2D_VarExp3 fcn(a);

    x = 0;
    y = 0;
    time = 0;

    slnTrue = (exp(a*x*y) - 1)*(1-x)*(1-y);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 1;
    y = 1;
    slnTrue = (exp(a*x*y) - 1)*(1-x)*(1-y);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.5;
    y = 1;
    slnTrue = (exp(a*x*y) - 1)*(1-x)*(1-y);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.25;
    y = 0.25;
    slnTrue = (exp(a*x*y) - 1)*(1-x)*(1-y);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );
  }

  {
    PyDict dict;
    dict[ScalarFunction2D_VarExp3::ParamsType::params.a] = a;
    ScalarFunction2D_VarExp3::ParamsType::checkInputs(dict);
    ScalarFunction2D_VarExp3 fcn(dict);

    x = 0;
    y = 0;
    time = 0;

    slnTrue = (exp(a*x*y) - 1)*(1-x)*(1-y);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 1;
    y = 1;
    slnTrue = (exp(a*x*y) - 1)*(1-x)*(1-y);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.5;
    y = 1;
    slnTrue = (exp(a*x*y) - 1)*(1-x)*(1-y);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.25;
    y = 0.25;
    slnTrue = (exp(a*x*y) - 1)*(1-x)*(1-y);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_SineSine_test )
{
  typedef ScalarFunction2D_SineSine::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;
  Real x, y, time;
  ArrayQ sln, slnTrue;

  Real a = 2, b = 3, C = 0.5, A = 4;
  {
    ScalarFunction2D_SineSine fcn(a,b,C,A);

    x = 0;
    y = 0;
    time = 0;

    slnTrue = A*sin(a*PI*x)*sin(b*PI*y) + C;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 1;
    y = 1;
    slnTrue = A*sin(a*PI*x)*sin(b*PI*y) + C;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.5;
    y = 1;
    slnTrue = A*sin(a*PI*x)*sin(b*PI*y) + C;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.25;
    y = 0.25;
    slnTrue = A*sin(a*PI*x)*sin(b*PI*y) + C;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );
  }

  {
    PyDict dict;
    dict[ScalarFunction2D_SineSine::ParamsType::params.a] = a;
    dict[ScalarFunction2D_SineSine::ParamsType::params.b] = b;
    dict[ScalarFunction2D_SineSine::ParamsType::params.C] = C;
    dict[ScalarFunction2D_SineSine::ParamsType::params.A] = A;
    ScalarFunction2D_SineSine::ParamsType::checkInputs(dict);
    ScalarFunction2D_SineSine fcn(dict);

    x = 0;
    y = 0;
    time = 0;

    slnTrue = A*sin(a*PI*x)*sin(b*PI*y) + C;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 1;
    y = 1;
    slnTrue = A*sin(a*PI*x)*sin(b*PI*y) + C;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.5;
    y = 1;
    slnTrue = A*sin(a*PI*x)*sin(b*PI*y) + C;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );

    x = 0.25;
    y = 0.25;
    slnTrue = A*sin(a*PI*x)*sin(b*PI*y) + C;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( slnTrue, sln, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_SineSineSineUnsteady_test )
{
  typedef ScalarFunction2D_SineSineSineUnsteady::template ArrayQ<Real> ArrayQ;

  const Real close_tol = 1.e-13;
  const Real small_tol = 1.e-13;
  Real x, y, time;
  ArrayQ sln, slnTrue;

  {
    ScalarFunction2D_SineSineSineUnsteady fcn;

    x = 0;
    y = 0;
    time = 0;

    slnTrue = sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time);
    sln = fcn(x, y, time);
    SANS_CHECK_CLOSE( slnTrue, sln, small_tol, close_tol );

    x = 1;
    y = 1;
    slnTrue = sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time);
    sln = fcn(x, y, time);
    SANS_CHECK_CLOSE( slnTrue, sln, small_tol, close_tol );

    x = 0.5;
    y = 1;
    slnTrue = sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time);
    sln = fcn(x, y, time);
    SANS_CHECK_CLOSE( slnTrue, sln, small_tol, close_tol );

    x = 0.25;
    y = 0.25;
    slnTrue = sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time);
    sln = fcn(x, y, time);
    SANS_CHECK_CLOSE( slnTrue, sln, small_tol, close_tol );
  }

  {
    PyDict dict;
    ScalarFunction2D_SineSineSineUnsteady::ParamsType::checkInputs(dict);
    ScalarFunction2D_SineSineSineUnsteady fcn(dict);

    x = 0;
    y = 0;
    time = 0;

    slnTrue = sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time);
    sln = fcn(x, y, time);
    SANS_CHECK_CLOSE( slnTrue, sln, small_tol, close_tol );

    x = 1;
    y = 1;
    slnTrue = sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time);
    sln = fcn(x, y, time);
    SANS_CHECK_CLOSE( slnTrue, sln, small_tol, close_tol );

    x = 0.5;
    y = 1;
    slnTrue = sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time);
    sln = fcn(x, y, time);
    SANS_CHECK_CLOSE( slnTrue, sln, small_tol, close_tol );

    x = 0.25;
    y = 0.25;
    slnTrue = sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time);
    sln = fcn(x, y, time);
    SANS_CHECK_CLOSE( slnTrue, sln, small_tol, close_tol );
  }
}

BOOST_AUTO_TEST_CASE(ScalarFunction2D_SineCosineDecay_test)
{

  typedef ScalarFunction2D_SineCosineDecay::template ArrayQ<Real> ArrayQ;

  const Real close_tol= 1.e-13;
  const Real small_tol= 1.e-13;

  Real x;
  Real y;
  Real t;

  Real lambda;

  ArrayQ sln;
  ArrayQ slnTrue;

  {
    Real lambda_default= 0.5;

    // test defaults
    ScalarFunction2D_SineCosineDecay fcn(lambda_default);

    x= 0;
    y= 0;
    t= 0.25;

    slnTrue= exp(-lambda_default*t)*sin(2*PI*x)*cos(2*PI*y);
    sln= fcn(x, y, t);
    SANS_CHECK_CLOSE(slnTrue, sln, small_tol, close_tol);

    x= 1;
    y= 1;
    t= 0;

    slnTrue= exp(-lambda_default*t)*sin(2*PI*x)*cos(2*PI*y);
    sln= fcn(x, y, t);
    SANS_CHECK_CLOSE(slnTrue, sln, small_tol, close_tol);

    x= 0.25;
    y= 0.25;
    t= 0.5;

    slnTrue= exp(-lambda_default*t)*sin(2*PI*x)*cos(2*PI*y);
    sln= fcn(x, y, t);
    SANS_CHECK_CLOSE(slnTrue, sln, small_tol, close_tol);

    x= 0.5;
    y= 0.5;
    t= 0.9;

    slnTrue= exp(-lambda_default*t)*sin(2*PI*x)*cos(2*PI*y);
    sln= fcn(x, y, t);
    SANS_CHECK_CLOSE(slnTrue, sln, small_tol, close_tol);

  }

  {

    Real lambda= 0.25;

    // test parameter passing
    ScalarFunction2D_SineCosineDecay fcn(lambda);

    x= 0;
    y= 0;
    t= 0.25;

    slnTrue= exp(-lambda*t)*sin(2*PI*x)*cos(2*PI*y);
    sln= fcn(x, y, t);
    SANS_CHECK_CLOSE(slnTrue, sln, small_tol, close_tol);

    x= 1;
    y= 1;
    t= 0;

    slnTrue= exp(-lambda*t)*sin(2*PI*x)*cos(2*PI*y);
    sln= fcn(x, y, t);
    SANS_CHECK_CLOSE(slnTrue, sln, small_tol, close_tol);

    x= 0.25;
    y= 0.25;
    t= 0.5;

    slnTrue= exp(-lambda*t)*sin(2*PI*x)*cos(2*PI*y);
    sln= fcn(x, y, t);
    SANS_CHECK_CLOSE(slnTrue, sln, small_tol, close_tol);

    x= 0.5;
    y= 0.5;
    t= 0.9;

    slnTrue= exp(-lambda*t)*sin(2*PI*x)*cos(2*PI*y);
    sln= fcn(x, y, t);
    SANS_CHECK_CLOSE(slnTrue, sln, small_tol, close_tol);

  }

  {
    lambda= 0.25;

    PyDict dict;
    dict[ScalarFunction2D_SineCosineDecay::ParamsType::params.lambda]= lambda;
    ScalarFunction2D_SineCosineDecay::ParamsType::checkInputs(dict);
    ScalarFunction2D_SineCosineDecay fcn(dict);

    slnTrue= exp(-lambda*t)*sin(2*PI*x)*cos(2*PI*y);
    sln= fcn(x, y, t);
    SANS_CHECK_CLOSE(slnTrue, sln, small_tol, close_tol);

    x= 1;
    y= 1;
    t= 0;

    slnTrue= exp(-lambda*t)*sin(2*PI*x)*cos(2*PI*y);
    sln= fcn(x, y, t);
    SANS_CHECK_CLOSE(slnTrue, sln, small_tol, close_tol);

    x= 0.25;
    y= 0.25;
    t= 0.5;

    slnTrue= exp(-lambda*t)*sin(2*PI*x)*cos(2*PI*y);
    sln= fcn(x, y, t);
    SANS_CHECK_CLOSE(slnTrue, sln, small_tol, close_tol);

    x= 0.5;
    y= 0.5;
    t= 0.9;

    slnTrue= exp(-lambda*t)*sin(2*PI*x)*cos(2*PI*y);
    sln= fcn(x, y, t);
    SANS_CHECK_CLOSE(slnTrue, sln, small_tol, close_tol);

  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_DoubleBL_test )
{
  typedef ScalarFunction2D_DoubleBL::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;
  Real x, y;
  ArrayQ sln;
  Real time = 0;

  Real a = 1.121;
  Real b = 0.037;
  Real nu = 1.e-3;

  {
    ScalarFunction2D_DoubleBL fcn( a, b, nu );

    x = 0;
    y = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( 1, sln, tol );

    x = 1;
    y = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_SMALL( sln, tol );

    x = 0;
    y = 1;
    sln = fcn(x, y, time);
    BOOST_CHECK_SMALL( sln, tol );
  }

  {
    PyDict d;
    d[ScalarFunction2D_DoubleBL::ParamsType::params.a] = a;
    d[ScalarFunction2D_DoubleBL::ParamsType::params.b] = b;
    d[ScalarFunction2D_DoubleBL::ParamsType::params.nu] = nu;
    ScalarFunction2D_DoubleBL::ParamsType::checkInputs(d);
    ScalarFunction2D_DoubleBL fcn(d);

    x = 0;
    y = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( 1, sln, tol );

    x = 1;
    y = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_SMALL( sln, tol );

    x = 0;
    y = 1;
    sln = fcn(x, y, time);
    BOOST_CHECK_SMALL( sln, tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_Vortex_test )
{
  typedef ScalarFunction2D_Vortex::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;
  Real x, y, r;
  ArrayQ sln;
  Real time = 0;

  Real x0 = -1;
  Real y0 = -2;

  {
    ScalarFunction2D_Vortex fcn( x0, y0 );

    x = 0;
    y = 0;
    r = sqrt((x - x0)*(x - x0) + (y - y0)*(y - y0));
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( log(r), sln, tol );

    x = 1;
    y = 0;
    r = sqrt((x - x0)*(x - x0) + (y - y0)*(y - y0));
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( log(r), sln, tol );

    x = 0;
    y = 1;
    r = sqrt((x - x0)*(x - x0) + (y - y0)*(y - y0));
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( log(r), sln, tol );
  }

  {
    PyDict d;
    d[ScalarFunction2D_Vortex::ParamsType::params.x0] = x0;
    d[ScalarFunction2D_Vortex::ParamsType::params.y0] = y0;
    ScalarFunction2D_Vortex::ParamsType::checkInputs(d);
    ScalarFunction2D_Vortex fcn(d);

    x = 0;
    y = 0;
    r = sqrt((x - x0)*(x - x0) + (y - y0)*(y - y0));
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( log(r), sln, tol );

    x = 1;
    y = 0;
    r = sqrt((x - x0)*(x - x0) + (y - y0)*(y - y0));
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( log(r), sln, tol );

    x = 0;
    y = 1;
    r = sqrt((x - x0)*(x - x0) + (y - y0)*(y - y0));
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( log(r), sln, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_SineSineRotated_test )
{
  typedef ScalarFunction2D_SineSineRotated::template ArrayQ<Real> ArrayQ;

  const Real close_tol = 1.e-13;
  const Real small_tol = 1.e-13;
  Real x, y, time;
  ArrayQ sln, slnTrue;

  Real th = PI/3.;
  Real xi, eta;

  {
    ScalarFunction2D_SineSineRotated fcn(th);

    x = 0;
    y = 0;
    time = 0;

    xi  =  x*cos(th) + y*sin(th);
    eta = -x*sin(th) + y*cos(th);
    slnTrue =  sin(2*PI*xi)*sin(2*PI*eta);
    sln = fcn(x, y, time);
    SANS_CHECK_CLOSE( slnTrue, sln, small_tol, close_tol );

    x = 1;
    y = 1;
    xi  =  x*cos(th) + y*sin(th);
    eta = -x*sin(th) + y*cos(th);
    slnTrue =  sin(2*PI*xi)*sin(2*PI*eta);
    sln = fcn(x, y, time);
    SANS_CHECK_CLOSE( slnTrue, sln, small_tol, close_tol );

    x = 0.5;
    y = 1;
    xi  =  x*cos(th) + y*sin(th);
    eta = -x*sin(th) + y*cos(th);
    slnTrue =  sin(2*PI*xi)*sin(2*PI*eta);
    sln = fcn(x, y, time);
    SANS_CHECK_CLOSE( slnTrue, sln, small_tol, close_tol );

    x = 0.25;
    y = 0.25;
    xi  =  x*cos(th) + y*sin(th);
    eta = -x*sin(th) + y*cos(th);
    slnTrue =  sin(2*PI*xi)*sin(2*PI*eta);
    sln = fcn(x, y, time);
    SANS_CHECK_CLOSE( slnTrue, sln, small_tol, close_tol );
  }

  {
    PyDict dict;
    dict[ScalarFunction2D_SineSineRotated::ParamsType::params.theta] = th;
    ScalarFunction2D_SineSineRotated::ParamsType::checkInputs(dict);
    ScalarFunction2D_SineSineRotated fcn(dict);

    x = 0;
    y = 0;
    time = 0;

    xi  =  x*cos(th) + y*sin(th);
    eta = -x*sin(th) + y*cos(th);
    slnTrue =  sin(2*PI*xi)*sin(2*PI*eta);
    sln = fcn(x, y, time);
    SANS_CHECK_CLOSE( slnTrue, sln, small_tol, close_tol );

    x = 1;
    y = 1;
    xi  =  x*cos(th) + y*sin(th);
    eta = -x*sin(th) + y*cos(th);
    slnTrue =  sin(2*PI*xi)*sin(2*PI*eta);
    sln = fcn(x, y, time);
    SANS_CHECK_CLOSE( slnTrue, sln, small_tol, close_tol );

    x = 0.5;
    y = 1;
    xi  =  x*cos(th) + y*sin(th);
    eta = -x*sin(th) + y*cos(th);
    slnTrue =  sin(2*PI*xi)*sin(2*PI*eta);
    sln = fcn(x, y, time);
    SANS_CHECK_CLOSE( slnTrue, sln, small_tol, close_tol );

    x = 0.25;
    y = 0.25;
    xi  =  x*cos(th) + y*sin(th);
    eta = -x*sin(th) + y*cos(th);
    slnTrue =  sin(2*PI*xi)*sin(2*PI*eta);
    sln = fcn(x, y, time);
    SANS_CHECK_CLOSE( slnTrue, sln, small_tol, close_tol );
  }
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_LaplaceLagrangeDirichlet_Vortex_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
      AdvectiveFlux2D_None,
      ViscousFlux2D_Uniform,
      Source2D_None> PDEClass;

  typedef ScalarFunction2D_Vortex::template ArrayQ<Real> ArrayQ;

  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Vortex> ScalarFunctionClass;
  typedef ScalarFunction2D_LaplaceLagrangeDirichlet<PDEClass, ScalarFunction2D_Vortex> LagrangeFunctionClass;

  Real x0 = -1;
  Real y0 = -2;
  Real time = 0;

  AdvectiveFlux2D_None adv;
  ViscousFlux2D_Uniform visc( 1, 0, 0, 1 );
  Source2D_None source;
  PDEClass pde( adv, visc, source );

  ScalarFunctionClass fcn( x0, y0 );
  LagrangeFunctionClass fcnLagrange( pde, fcn );

  const Real tol = 1.e-13;
  Real x, y, nx, ny;
  ArrayQ lg;

  nx = 0.8;
  ny = 0.45;

  x = 0.5;
  y = 0;
  lg = fcnLagrange( x, y, time, nx, ny );
  BOOST_CHECK_CLOSE( -6./25.*nx + -8./25.*ny, lg, tol );

  x = 0.5;
  y = 1;
  lg = fcnLagrange( x, y, time, nx, ny );
  BOOST_CHECK_CLOSE( -2./15.*nx + -4./15.*ny, lg, tol );

  x = 0;
  y = 0.5;
  lg = fcnLagrange( x, y, time, nx, ny );
  BOOST_CHECK_CLOSE(  -4./29.*nx + -10./29.*ny, lg, tol );

  x = 1;
  y = 0.5;
  lg = fcnLagrange( x, y, time, nx, ny );
  BOOST_CHECK_CLOSE(  -8./41.*nx + -10./41.*ny, lg, tol );
}
#endif

//----------------------------------------------------------------------------//
// NOTE: hard-coded tolerances show slow convergence of current series
BOOST_AUTO_TEST_CASE( ScalarFunction2D_LaplacianUnitForcing_test )
{
  typedef ScalarFunction2D_LaplacianUnitForcing::template ArrayQ<Real> ArrayQ;

  typedef ScalarFunction2D_LaplacianUnitForcing ScalarFunctionClass;

  ScalarFunctionClass fcn;

  const Real tol = 1.e-13;
  const Real tolgrad = 1e-13;
  Real x, y, time;
  ArrayQ u, ux, uy, ut, uxx, uxy, uyy;

  x = 0;
  y = 0;
  time = 0;
  u = fcn(x, y, time);
  BOOST_CHECK_SMALL( u, tol );

  fcn(x, y, time, u, ux, uy, ut, uxx, uxy, uyy);
  BOOST_CHECK_SMALL( u, tol );
  BOOST_CHECK_SMALL( ux, 1e-3 );
  BOOST_CHECK_SMALL( uy, tolgrad );

  x = 1;
  y = 0;
  u = fcn(x, y, time);
  BOOST_CHECK_SMALL( u, tol );

  fcn(x, y, time, u, ux, uy, ut, uxx, uxy, uyy);
  BOOST_CHECK_SMALL( u, tol );
  BOOST_CHECK_SMALL( ux, 1e-3 );
  BOOST_CHECK_SMALL( uy, tolgrad );

  x = 0;
  y = 1;
  u = fcn(x, y, time);
  BOOST_CHECK_SMALL( u, tol );

  fcn(x, y, time, u, ux, uy, ut, uxx, uxy, uyy);
  BOOST_CHECK_SMALL( u, tol );
  BOOST_CHECK_SMALL( ux, 1e-3 );
  BOOST_CHECK_SMALL( uy, tolgrad );

  x = 1;
  y = 1;
  u = fcn(x, y, time);
  BOOST_CHECK_SMALL( u, tol );

  fcn(x, y, time, u, ux, uy, ut, uxx, uxy, uyy);
  BOOST_CHECK_SMALL( u, tol );
  BOOST_CHECK_SMALL( ux, 1e-3 );
  BOOST_CHECK_SMALL( uy, tolgrad );

  x = 0.5;
  y = 0;
  u = fcn(x, y, time);
  BOOST_CHECK_SMALL( u, 1e-8 );

  fcn(x, y, time, u, ux, uy, ut, uxx, uxy, uyy);
  BOOST_CHECK_SMALL( u, 1e-8 );
  BOOST_CHECK_SMALL( ux, tolgrad );
  BOOST_CHECK_CLOSE( 0.337657241656783805798879334127559, uy, 1e-3 );

  x = 0.5;
  y = 0.5;
  u = fcn(x, y, time);
  BOOST_CHECK_CLOSE( 0.073671353281513815563940025166191, u, tol );

  fcn(x, y, time, u, ux, uy, ut, uxx, uxy, uyy);
  BOOST_CHECK_CLOSE( 0.073671353281513815563940025166191, u, tol );
  BOOST_CHECK_SMALL( ux, tolgrad );
  BOOST_CHECK_SMALL( uy, tolgrad );

  x = 0.25;
  y = 0.25;
  u = fcn(x, y, time);
  BOOST_CHECK_CLOSE( 0.045286158109472705758896774634443, u, 2e-12 );

  fcn(x, y, time, u, ux, uy, ut, uxx, uxy, uyy);
  BOOST_CHECK_CLOSE( 0.045286158109472705758896774634443, u, tol );
  BOOST_CHECK_CLOSE( 0.10195738769071017953637186187860, ux, 1e-12 );
  BOOST_CHECK_CLOSE( 0.10195738769071017953637186187860, uy, 1e-12 );
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_AdjointLaplaceLagrangeDirichlet_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
      AdvectiveFlux2D_None,
      ViscousFlux2D_Uniform,
      Source2D_None> PDEClass;

  typedef ScalarFunction2D_LaplacianUnitForcing::template ArrayQ<Real> ArrayQ;

  typedef ScalarFunction2D_LaplacianUnitForcing  ScalarFunctionClass;
  typedef ScalarFunction2D_AdjointLaplaceLagrangeDirichlet<PDEClass, ScalarFunctionClass> SolutionLagrangeFunctionClass;

  AdvectiveFlux2D_None adv;
  ViscousFlux2D_Uniform visc( 1, 0, 0, 1 );
  Source2D_None source;
  PDEClass pde( adv, visc, source );

  ScalarFunctionClass fcn;
  SolutionLagrangeFunctionClass fcnLagrange( pde, fcn );

  const Real tolgrad = 1.5e-3;
  Real x, y, time, nx, ny;
  ArrayQ lg;

  nx = 0.8;
  ny = 1;

  x = 0.5;
  y = 0;
  time = 0;
  lg = fcnLagrange( x, y, time, nx, ny );
  BOOST_CHECK_CLOSE( 0*nx + -0.33766220752953188773918844676636*ny, lg, tolgrad );

  x = 0.5;
  y = 1;
  lg = fcnLagrange( x, y, time, nx, ny );
  BOOST_CHECK_CLOSE( 0*nx + 0.33766220752953188773918844676636*ny, lg, tolgrad );

  x = 0;
  y = 0.5;
  lg = fcnLagrange( x, y, time, nx, ny );
  BOOST_CHECK_CLOSE( -0.33766220752953188773918844676636*nx + 0*ny, lg, tolgrad );

  x = 1;
  y = 0.5;
  lg = fcnLagrange( x, y, time, nx, ny );
  BOOST_CHECK_CLOSE( 0.33766220752953188773918844676636*nx + 0*ny, lg, tolgrad );
}
#endif
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_AdjointADAverage_test )
{
  typedef ScalarFunction2D_AdjointADAverage::template ArrayQ<Real> ArrayQ;

  typedef ScalarFunction2D_AdjointADAverage ScalarFunctionClass;

  Real a = 1;
  Real b = 0.2;
  Real nu = 0.1;

  ScalarFunctionClass fcn( a, b, nu );

  const Real tol = 1.e-13;
  const Real tolgrad = 1e-12;
  Real x, y, time;
  ArrayQ u, ux, uy, ut, uxx, uxy, uyy;

  x = 0;
  y = 0.5;
  time = 0;
  u = fcn(x, y, time);
  BOOST_CHECK_SMALL( u, tol );

  fcn(x, y, time, u, ux, uy, ut, uxx, uxy, uyy);
  BOOST_CHECK_SMALL( u, tol );
  BOOST_CHECK_CLOSE( 7.1859907197115927248578911005738, ux, tolgrad );
  BOOST_CHECK_SMALL( uy, tolgrad );

  x = 1;
  y = 0.5;
  u = fcn(x, y, time);
  BOOST_CHECK_SMALL( u, tol );

  fcn(x, y, time, u, ux, uy, ut, uxx, uxy, uyy);
  BOOST_CHECK_SMALL( u, tol );
  BOOST_CHECK_CLOSE( -0.97194749065554809036703928181407, ux, tolgrad );
  BOOST_CHECK_SMALL( uy, tolgrad );

  x = 0.5;
  y = 0;
  u = fcn(x, y, time);
  BOOST_CHECK_SMALL( u, tol );

  fcn(x, y, time, u, ux, uy, ut, uxx, uxy, uyy);
  BOOST_CHECK_SMALL( u, tol );
  BOOST_CHECK_SMALL( ux, tolgrad );
  BOOST_CHECK_CLOSE( 2.8931366012296692, uy, tolgrad*100 );

  x = 0.5;
  y = 1;
  u = fcn(x, y, time);
  BOOST_CHECK_SMALL( u, tol );

  fcn(x, y, time, u, ux, uy, ut, uxx, uxy, uyy);
  BOOST_CHECK_SMALL( u, tol );
  BOOST_CHECK_SMALL( ux, tolgrad );
  BOOST_CHECK_CLOSE( -1.9510360453870842, uy, tolgrad*100 );

  x = 0.5;
  y = 0.5;
  u = fcn(x, y, time);
  BOOST_CHECK_CLOSE( 0.42380138122875370058622644411901, u, tol );

  fcn(x, y, time, u, ux, uy, ut, uxx, uxy, uyy);
  BOOST_CHECK_CLOSE(  0.42380138122875370058622644411901, u,  tol );
  BOOST_CHECK_CLOSE( -0.67287357002503086279604132548803, ux, tolgrad );
  BOOST_CHECK_CLOSE( -0.11985922234640658642321717567295, uy, tolgrad );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_SineTheta_test )
{
  typedef ScalarFunction2D_SineTheta::template ArrayQ<Real> ArrayQ;

  const Real c = 1.6;

  const Real tol = 1.e-13;
  Real x, y, time;
  ArrayQ sln;

  // ctor 1
  ScalarFunction2D_SineTheta fcn1(c);

  // ctor 2
  PyDict solnArgs;
  solnArgs[ScalarFunction2D_SineTheta::ParamsType::params.c] = c;
  ScalarFunction2D_SineTheta::ParamsType::checkInputs(solnArgs);
  ScalarFunction2D_SineTheta fcn2( solnArgs );

  x = 1;
  y = 0;
  time = 0;

  sln = fcn1(x, y, time);
  BOOST_CHECK_SMALL( sln, tol );
  sln = fcn2(x, y, time);
  BOOST_CHECK_SMALL( sln, tol );

  x = -1;
  y = 0;
  sln = fcn1(x, y, time);
  BOOST_CHECK_SMALL( sln, tol );
  sln = fcn2(x, y, time);
  BOOST_CHECK_SMALL( sln, tol );

  x = 0;
  y = 1;
  sln = fcn1(x, y, time);
  BOOST_CHECK_CLOSE( c, sln, tol );
  sln = fcn2(x, y, time);
  BOOST_CHECK_CLOSE( c, sln, tol );

  x = 0;
  y = -1;
  sln = fcn1(x, y, time);
  BOOST_CHECK_CLOSE( -c, sln, tol );
  sln = fcn2(x, y, time);
  BOOST_CHECK_CLOSE( -c, sln, tol );

  x =  1;
  y = -1;
  sln = fcn1(x, y, time);
  BOOST_CHECK_CLOSE( -c*sqrt(0.5), sln, tol );
  sln = fcn2(x, y, time);
  BOOST_CHECK_CLOSE( -c*sqrt(0.5), sln, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_CornerSingularity_test )
{
  typedef ScalarFunction2D_CornerSingularity::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;
  Real x, y, r, theta;
  ArrayQ sln;
  Real time = 0;

  Real alpha = 2.0/3.0;
  Real theta0 = 0.1;

  {
    ScalarFunction2D_CornerSingularity fcn( alpha, theta0 );

    x = 0;
    y = 0;
    r = sqrt(x*x + y*y);
    theta = atan2(y,x);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( pow(r,alpha)*sin(alpha*(theta + theta0)), sln, tol );

    x = 1;
    y = 0;
    r = sqrt(x*x + y*y);
    theta = atan2(y,x);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( pow(r,alpha)*sin(alpha*(theta + theta0)), sln, tol );

    x = 0;
    y = 1;
    r = sqrt(x*x + y*y);
    theta = atan2(y,x);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( pow(r,alpha)*sin(alpha*(theta + theta0)), sln, tol );

    x = 0.5;
    y = 0.6;
    r = sqrt(x*x + y*y);
    theta = atan2(y,x);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( pow(r,alpha)*sin(alpha*(theta + theta0)), sln, tol );
  }

  {
    PyDict solnArgs;
    solnArgs[ScalarFunction2D_CornerSingularity::ParamsType::params.alpha] = alpha;
    solnArgs[ScalarFunction2D_CornerSingularity::ParamsType::params.theta0] = theta0;
    ScalarFunction2D_CornerSingularity::ParamsType::checkInputs(solnArgs);
    ScalarFunction2D_CornerSingularity fcn(solnArgs);

    x = 0;
    y = 0;
    r = sqrt(x*x + y*y);
    theta = atan2(y,x);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( pow(r,alpha)*sin(alpha*(theta + theta0)), sln, tol );

    x = 1;
    y = 0;
    r = sqrt(x*x + y*y);
    theta = atan2(y,x);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( pow(r,alpha)*sin(alpha*(theta + theta0)), sln, tol );

    x = 0;
    y = 1;
    r = sqrt(x*x + y*y);
    theta = atan2(y,x);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( pow(r,alpha)*sin(alpha*(theta + theta0)), sln, tol );

    x = 0.5;
    y = 0.6;
    r = sqrt(x*x + y*y);
    theta = atan2(y,x);
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( pow(r,alpha)*sin(alpha*(theta + theta0)), sln, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_BoundaryLayer_test )
{
  typedef ScalarFunction2D_BoundaryLayer::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;
  Real x, y;
  ArrayQ sln;
  Real time = 0;

  Real eps = 0.1;
  Real beta = 5;
  int p = 2;
  Real pP1_fac = 3*2*1;

  {
    ScalarFunction2D_BoundaryLayer fcn( eps, beta, p );

    x = 0;
    y = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( exp(-x/eps ) + beta/(pP1_fac)*pow(y,p+1), sln, tol );

    x = 1;
    y = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( exp(-x/eps ) + beta/(pP1_fac)*pow(y,p+1), sln, tol );

    x = 0;
    y = 1;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( exp(-x/eps ) + beta/(pP1_fac)*pow(y,p+1), sln, tol );

    x = 0.5;
    y = 0.6;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( exp(-x/eps ) + beta/(pP1_fac)*pow(y,p+1), sln, tol );
  }

  {
    PyDict solnArgs;
    solnArgs[ScalarFunction2D_BoundaryLayer::ParamsType::params.epsilon] = eps;
    solnArgs[ScalarFunction2D_BoundaryLayer::ParamsType::params.beta] = beta;
    solnArgs[ScalarFunction2D_BoundaryLayer::ParamsType::params.p] = p;
    ScalarFunction2D_BoundaryLayer::ParamsType::checkInputs(solnArgs);
    ScalarFunction2D_BoundaryLayer fcn(solnArgs);

    x = 0;
    y = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( exp(-x/eps ) + beta/(pP1_fac)*pow(y,p+1), sln, tol );

    x = 1;
    y = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( exp(-x/eps ) + beta/(pP1_fac)*pow(y,p+1), sln, tol );

    x = 0;
    y = 1;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( exp(-x/eps ) + beta/(pP1_fac)*pow(y,p+1), sln, tol );

    x = 0.5;
    y = 0.6;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( exp(-x/eps ) + beta/(pP1_fac)*pow(y,p+1), sln, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_Gaussian_test )
{
  typedef ScalarFunction2D_Gaussian::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;
  Real x, y;
  ArrayQ sln;
  Real time = 0;

  Real a = 0.1;
  Real x0 = 0.3;
  Real y0 = 0.6;
  Real sx = 0.25;
  Real sy = 0.87;

  {
    ScalarFunction2D_Gaussian fcn( a, x0, y0, sx, sy );

    x = 0;
    y = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a*exp( -0.5*( pow((x - x0)/sx, 2.) + pow((y - y0)/sy, 2.) ) ), sln, tol );

    x = 1;
    y = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a*exp( -0.5*( pow((x - x0)/sx, 2.) + pow((y - y0)/sy, 2.) ) ), sln, tol );

    x = 0;
    y = 1;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a*exp( -0.5*( pow((x - x0)/sx, 2.) + pow((y - y0)/sy, 2.) ) ), sln, tol );

    x = 0.5;
    y = 0.6;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a*exp( -0.5*( pow((x - x0)/sx, 2.) + pow((y - y0)/sy, 2.) ) ), sln, tol );
  }

  {
    PyDict solnArgs;
    solnArgs[ScalarFunction2D_Gaussian::ParamsType::params.a] = a;
    solnArgs[ScalarFunction2D_Gaussian::ParamsType::params.x0] = x0;
    solnArgs[ScalarFunction2D_Gaussian::ParamsType::params.y0] = y0;
    solnArgs[ScalarFunction2D_Gaussian::ParamsType::params.sx] = sx;
    solnArgs[ScalarFunction2D_Gaussian::ParamsType::params.sy] = sy;
    ScalarFunction2D_Gaussian::ParamsType::checkInputs(solnArgs);
    ScalarFunction2D_Gaussian fcn(solnArgs);

    x = 0;
    y = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a*exp( -0.5*( pow((x - x0)/sx, 2.) + pow((y - y0)/sy, 2.) ) ), sln, tol );

    x = 1;
    y = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a*exp( -0.5*( pow((x - x0)/sx, 2.) + pow((y - y0)/sy, 2.) ) ), sln, tol );

    x = 0;
    y = 1;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a*exp( -0.5*( pow((x - x0)/sx, 2.) + pow((y - y0)/sy, 2.) ) ), sln, tol );

    x = 0.5;
    y = 0.6;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a*exp( -0.5*( pow((x - x0)/sx, 2.) + pow((y - y0)/sy, 2.) ) ), sln, tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_GaussianCosine_test )
{
  typedef ScalarFunction2D_GaussianCosine::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;
  Real x, y;
  ArrayQ sln;
  Real time = 0;

  Real a = 0.1;
  Real y0 = 0.6;
  Real y1 = 0.2;
  Real sy = 0.87;

  {
    ScalarFunction2D_GaussianCosine fcn( a, y0, y1, sy );

    x = 0;
    y = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a*exp( -0.5*( pow((y - (y0 + y1*cos(2*PI*x)))/sy, 2.) ) ), sln, tol );

    x = 1;
    y = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a*exp( -0.5*( pow((y - (y0 + y1*cos(2*PI*x)))/sy, 2.) ) ), sln, tol );

    x = 0;
    y = 1;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a*exp( -0.5*( pow((y - (y0 + y1*cos(2*PI*x)))/sy, 2.) ) ), sln, tol );

    x = 0.5;
    y = 0.6;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a*exp( -0.5*( pow((y - (y0 + y1*cos(2*PI*x)))/sy, 2.) ) ), sln, tol );
  }

  {
    PyDict solnArgs;
    solnArgs[ScalarFunction2D_GaussianCosine::ParamsType::params.a] = a;
    solnArgs[ScalarFunction2D_GaussianCosine::ParamsType::params.y0] = y0;
    solnArgs[ScalarFunction2D_GaussianCosine::ParamsType::params.y1] = y1;
    solnArgs[ScalarFunction2D_GaussianCosine::ParamsType::params.sy] = sy;
    ScalarFunction2D_GaussianCosine::ParamsType::checkInputs(solnArgs);
    ScalarFunction2D_GaussianCosine fcn(solnArgs);

    x = 0;
    y = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a*exp( -0.5*( pow((y - (y0 + y1*cos(2*PI*x)))/sy, 2.) ) ), sln, tol );

    x = 1;
    y = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a*exp( -0.5*( pow((y - (y0 + y1*cos(2*PI*x)))/sy, 2.) ) ), sln, tol );

    x = 0;
    y = 1;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a*exp( -0.5*( pow((y - (y0 + y1*cos(2*PI*x)))/sy, 2.) ) ), sln, tol );

    x = 0.5;
    y = 0.6;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( a*exp( -0.5*( pow((y - (y0 + y1*cos(2*PI*x)))/sy, 2.) ) ), sln, tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_Tanh2_test )
{
  typedef ScalarFunction2D_Tanh2::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;
  Real x[5] = { 0.12, 0.21, 0.50, 0.72, 0.90 };
  Real y[5] = { 0.32, 0.67, 0.47, 0.38, 0.84 };
  ArrayQ sln;
  Real time = 0;

  {
    ScalarFunction2D_Tanh2 fcn;

    for (int i = 0; i < 5; i++)
    {
      sln = fcn(x[i],y[i],time);
      SANS_CHECK_CLOSE( tanh(pow(x[i] + 1.3, 20.0) * pow(y[i] - 0.3, 9.0) ), sln, tol, tol );
    }
  }

  {
    PyDict solnArgs;
    ScalarFunction2D_Tanh2::ParamsType::checkInputs(solnArgs);
    ScalarFunction2D_Tanh2 fcn(solnArgs);

    for (int i = 0; i < 5; i++)
    {
      sln = fcn(x[i],y[i],time);
      SANS_CHECK_CLOSE( tanh(pow(x[i] + 1.3, 20.0) * pow(y[i] - 0.3, 9.0) ), sln, tol, tol );
    }
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_ThetaGeometricSeries_test )
{
  typedef ScalarFunction2D_ThetaGeometricSeries::template ArrayQ<Real> ArrayQ;

  const Real c = 1.6;
  const int pmax = 10;
  Real monomial;
  Real theta;

  const Real tol = 1.e-13;
  Real x, y, time;
  ArrayQ sln;
  ArrayQ slnTrue;

  x = 0.8;
  y = 0.6;
  time = 0;

  theta = atan2(y,x);

  // ctor1
  typedef ScalarFunction2D_ThetaGeometricSeries SolutionClass;

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.c] = c;

  monomial = 1;
  slnTrue = 0;
  for (int i = 0; i <= pmax; i++)
  {
    slnTrue += monomial;
    monomial *= theta;

    solnArgs[SolutionClass::ParamsType::params.p] = i;
    SolutionClass::ParamsType::checkInputs(solnArgs);
    SolutionClass fcn( solnArgs );
    sln = fcn(x, y, time);

    SANS_CHECK_CLOSE( c*slnTrue, sln, tol, tol );
  }

  solnArgs[SolutionClass::ParamsType::params.p] = -1;
  SolutionClass fcn1( solnArgs );
  BOOST_CHECK_THROW( sln = fcn1(x, y, time);, SANSException );

  // ctor2
  monomial = 1;
  slnTrue = 0;
  for (int i = 0; i <= pmax; i++)
  {
    slnTrue += monomial;
    monomial *= theta;

    ScalarFunction2D_ThetaGeometricSeries fcn(c,i);
    sln = fcn(x, y, time);

    SANS_CHECK_CLOSE( c*slnTrue, sln, tol, tol );

    ScalarFunction2D_ThetaGeometricSeries fcn2(c,-1);
    BOOST_CHECK_THROW( sln = fcn2(x, y, time);, SANSException );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_BoxCornerInterpolant_test )
{
  typedef ScalarFunction2D_BoxCornerInterpolant::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;
  ArrayQ sln;

  Real x0 = 0.0, x1 = 2.0;
  Real y0 = 1.0, y1 = 3.0;
  Real t0 = 0.0, t1 = 8.0;
  Real val000 = 0.0, val100 = 1.0, val110 = 2.0, val010 = 3.0;
  Real val001 = 4.0, val101 = 5.0, val111 = 6.0, val011 = 7.0;

  {
    ScalarFunction2D_BoxCornerInterpolant fcn( x0, x1, y0, y1, t0, t1,
                                               val000, val100, val110, val010,
                                               val001, val101, val111, val011 );

    sln = fcn(x0, y0, t0);
    BOOST_CHECK_CLOSE( val000, sln, tol );

    sln = fcn(x1, y0, t0);
    BOOST_CHECK_CLOSE( val100, sln, tol );

    sln = fcn(x1, y1, t0);
    BOOST_CHECK_CLOSE( val110, sln, tol );

    sln = fcn(x0, y1, t0);
    BOOST_CHECK_CLOSE( val010, sln, tol );

    sln = fcn(x0, y0, t1);
    BOOST_CHECK_CLOSE( val001, sln, tol );

    sln = fcn(x1, y0, t1);
    BOOST_CHECK_CLOSE( val101, sln, tol );

    sln = fcn(x1, y1, t1);
    BOOST_CHECK_CLOSE( val111, sln, tol );

    sln = fcn(x0, y1, t1);
    BOOST_CHECK_CLOSE( val011, sln, tol );

    Real x = 0.5*(x0 + x1);
    Real y = 0.5*(y0 + y1);
    Real t = 0.5*(t0 + t1);
    sln = fcn(x, y, t);
    BOOST_CHECK_CLOSE( 3.5, sln, tol );
  }

  {
    ScalarFunction2D_BoxCornerInterpolant fcn( x0, x1, y0, y1,
                                               val000, val100, val110, val010 );

    sln = fcn(x0, y0, t0);
    BOOST_CHECK_CLOSE( val000, sln, tol );

    sln = fcn(x1, y0, t0);
    BOOST_CHECK_CLOSE( val100, sln, tol );

    sln = fcn(x1, y1, t0);
    BOOST_CHECK_CLOSE( val110, sln, tol );

    sln = fcn(x0, y1, t0);
    BOOST_CHECK_CLOSE( val010, sln, tol );

    sln = fcn(x0, y0, t1);
    BOOST_CHECK_CLOSE( val000, sln, tol );

    sln = fcn(x1, y0, t1);
    BOOST_CHECK_CLOSE( val100, sln, tol );

    sln = fcn(x1, y1, t1);
    BOOST_CHECK_CLOSE( val110, sln, tol );

    sln = fcn(x0, y1, t1);
    BOOST_CHECK_CLOSE( val010, sln, tol );

    Real x = 0.5*(x0 + x1);
    Real y = 0.5*(y0 + y1);
    Real t = 0.5*(t0 + t1);
    sln = fcn(x, y, t);
    BOOST_CHECK_CLOSE( 1.5, sln, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ScalarFunction2D_Pulse_test )
{
  typedef ScalarFunction2D_Pulse::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;
  Real x, y;
  ArrayQ sln;
  Real time = 0;

  Real h = 0.1;
  Real x0 = 0.3;
  Real w = 0.87;

  {
    ScalarFunction2D_Pulse fcn( h, x0, w );

    x = 0;
    y = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( h, sln, tol );

    x = 2;
    y = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( 0.0, sln, tol );

    x = -3;
    y = 1;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( 0.0, sln, tol );

    x = 0.5;
    y = 0.6;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( h, sln, tol );
  }

  {
    PyDict solnArgs;
    solnArgs[ScalarFunction2D_Pulse::ParamsType::params.h] = h;
    solnArgs[ScalarFunction2D_Pulse::ParamsType::params.x0] = x0;
    solnArgs[ScalarFunction2D_Pulse::ParamsType::params.w] = w;
    ScalarFunction2D_Pulse::ParamsType::checkInputs(solnArgs);
    ScalarFunction2D_Pulse fcn(solnArgs);

    x = 0;
    y = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( h, sln, tol );

    x = 2;
    y = 0;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( 0.0, sln, tol );

    x = -3;
    y = 1;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( 0.0, sln, tol );

    x = 0.5;
    y = 0.6;
    sln = fcn(x, y, time);
    BOOST_CHECK_CLOSE( h, sln, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/ScalarFunction2D_pattern.txt", true );

  {
    ScalarFunction2D_Const fcn1( 1 );
    ScalarFunction2D_Const fcn2( 2 );
    ScalarFunction2D_Combination<ScalarFunction2D_Const,ScalarFunction2D_Const> soln(1,fcn1,1,fcn2);
    soln.dump( 2, output );
  }

  {
    ScalarFunction2D_Const soln(2.);
    soln.dump( 2, output );
  }

  {
    ScalarFunction2D_Linear soln(2., 3., 4.);
    soln.dump( 2, output );
  }

  {
    PyDict d;
    ScalarFunction2D_Quadratic soln(d);
    soln.dump( 2, output );
  }

  {
    Real a00 = 0.1;
    Real a01 = 0.2;
    Real a10 = 0.3;
    Real a11 = 0.4;
    Real x0 = 0.0;
    Real y0 = 0.0;

    ScalarFunction2D_PiecewiseConstBlock soln( a00, a01, a10, a11, x0, y0 );
    soln.dump( 2, output );
  }

  {
    Real a0 = 0.1;
    Real a1 = 0.2;
    Real m = 2;
    Real c = 1;

    ScalarFunction2D_PiecewiseConstDiag soln( a0, a1, m, c );
    soln.dump( 2, output );
  }

  {
    ScalarFunction2D_Monomial soln(2, 3);
    soln.dump( 2, output );
  }

  {
    ScalarFunction2D_Cubic soln;
    soln.dump( 2, output );
  }

  {
    PyDict d;
    ScalarFunction2D_Pringle soln(d);
    soln.dump( 2, output );
  }

  {
    ScalarFunction2D_ASExp soln(1., 1., 1., 1., 1., 1.);
    soln.dump( 2, output );
  }

  {
    Real a = 2, b = 3, c = 4, d = 5, e = 6;

    ScalarFunction2D_VarExp soln(a,b,c,d,e);
    soln.dump( 2, output );
  }

  {
    Real a = 2, b = 3, c = 4, d = 5;

    ScalarFunction2D_VarExp2 soln(a,b,c,d);
    soln.dump( 2, output );
  }

  {
    Real a = 2;

    ScalarFunction2D_VarExp3 soln(a);
    soln.dump( 2, output );
  }

  {
    Real a = 2, b = 3, c = 0.5, A = 4;

    ScalarFunction2D_SineSine soln(a,b,c,A);
    soln.dump( 2, output );
  }

  {
    ScalarFunction2D_SineSineSineUnsteady soln;
    soln.dump( 2, output );
  }

  {
    ScalarFunction2D_SineCosineDecay soln(1.0);
    soln.dump( 2, output );
  }

  {
    PyDict d;
    ScalarFunction2D_CircularWaveDecay soln(d);
    soln.dump( 2, output );
  }

  {
    Real a = 1.121;
    Real b = 0.037;
    Real nu = 1.e-3;

    ScalarFunction2D_DoubleBL soln( a, b, nu );
    soln.dump( 2, output );
  }

  {
    Real x0 = -1;
    Real y0 = -2;

    ScalarFunction2D_Vortex soln( x0, y0 );
    soln.dump( 2, output );
  }

  {
    Real th = PI/3.;

    ScalarFunction2D_SineSineRotated soln(th);
    soln.dump( 2, output );
  }

  {
    Real alpha = 2.0/3.0;
    Real theta0 = 0.1;

    ScalarFunction2D_CornerSingularity soln( alpha, theta0 );
    soln.dump( 2, output );
  }

  {
    Real eps = 0.1;
    Real beta = 5;
    int p = 2;

    ScalarFunction2D_BoundaryLayer soln( eps, beta, p );
    soln.dump( 2, output );
  }

  {
    ScalarFunction2D_Ojeda soln(1.0, 1.0);
    soln.dump( 2, output );
  }

  {
    Real a = 0.1;
    Real x0 = 0.3;
    Real y0 = 0.6;
    Real sx = 0.25;
    Real sy = 0.87;

    ScalarFunction2D_Gaussian soln( a, x0, y0, sx, sy );
    soln.dump( 2, output );
  }

  {
    ScalarFunction2D_LaplacianUnitForcing soln;
    soln.dump( 2, output );
  }

  {
    Real a = 1;
    Real b = 0.2;
    Real nu = 0.1;

    ScalarFunction2D_AdjointADAverage soln( a, b, nu );
    soln.dump( 2, output );
  }

  {
    Real c = 3;

    ScalarFunction2D_SineTheta soln( c );
    soln.dump( 2, output );
  }

  {
    Real c = 3;
    int p = 2;

    ScalarFunction2D_ThetaGeometricSeries soln( c, p );
    soln.dump( 2, output );
  }

  {
    Real nu = 3;
    Real A = 2;
    Real B = 2;

    ScalarFunction2D_Tanh soln( nu, A, B );
    soln.dump( 2, output );
  }

  {
    ScalarFunction2D_PiecewiseLinear soln;
    soln.dump( 2, output );
  }

  {
    Real x0 = 0.0, x1 = 2.0;
    Real y0 = 1.0, y1 = 3.0;
    Real t0 = 0.0, t1 = 8.0;
    Real val000 = 0.0, val100 = 1.0, val110 = 2.0, val010 = 3.0;
    Real val001 = 4.0, val101 = 5.0, val111 = 6.0, val011 = 7.0;

    ScalarFunction2D_BoxCornerInterpolant soln( x0, x1, y0, y1, t0, t1,
                                                val000, val100, val110, val010,
                                                val001, val101, val111, val011 );
    soln.dump( 2, output );
  }

  {
    Real h = 3;
    Real x0 = 2;
    Real w = 2;

    ScalarFunction2D_Tanh soln( h, x0, w );
    soln.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
