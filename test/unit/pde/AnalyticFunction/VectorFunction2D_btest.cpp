// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// VectorFunction2D_btest
//
// test of 2D vector analytic functions

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/AnalyticFunction/VectorFunction2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( VectorFunction2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Vector2Function2D_test )
{
  typedef Vector2Function2D<ScalarFunction2D_Linear, ScalarFunction2D_Linear>::template ArrayQ<Real> ArrayQ;

  Real a0 =  1.0, ax = -3.0, ay = 4.0;
  Real b0 = -2.0, bx =  5.0, by = 2.0;

  ScalarFunction2D_Linear fcn1( a0, ax, ay );
  ScalarFunction2D_Linear fcn2( b0, bx, by );

  Vector2Function2D<ScalarFunction2D_Linear, ScalarFunction2D_Linear> fcn(fcn1,fcn2);

  Real x, y, time = 0;
  ArrayQ sln;
  const Real tol = 1.0e-13;

  x = 1.0, y = 2.0;
  Real sol1_true = a0 + ax*x + ay*y;
  Real sol2_true = b0 + bx*x + by*y;
  sln = fcn(x, y, time);

  BOOST_CHECK_CLOSE( sol1_true, sln[0], tol );
  BOOST_CHECK_CLOSE( sol2_true, sln[1], tol );

  x = -0.52, y = 0.85;
  sol1_true = a0 + ax*x + ay*y;
  sol2_true = b0 + bx*x + by*y;
  sln = fcn(x, y, time);

  BOOST_CHECK_CLOSE( sol1_true, sln[0], tol );
  BOOST_CHECK_CLOSE( sol2_true, sln[1], tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorFunction2D_test )
{
  typedef VectorFunction2D_const<3> VectorFcnType;
  typedef typename VectorFcnType::template ArrayQ<Real> ArrayQ;
  const int mvec = ArrayQ::M;

  const ArrayQ vecValueTrue = {0.8, -23.6, 1.6};

  VectorFcnType fcn(vecValueTrue);

  const Real time = 0.0;
  Real x, y;
  ArrayQ sln;

  const Real tol = 1.0e-13;

  x = 1.0, y = 2.0;
  sln = fcn(x, y, time);
  for (int i = 0; i < mvec; ++i)
    BOOST_CHECK_CLOSE( vecValueTrue[i], sln[i], tol );

  x = -0.52, y = 0.85;
  sln = fcn(x, y, time);
  for (int i = 0; i < mvec; ++i)
    BOOST_CHECK_CLOSE( vecValueTrue[i], sln[i], tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
