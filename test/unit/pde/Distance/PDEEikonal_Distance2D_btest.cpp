// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEEikonal_Distance2D_btest
//
// test of 2-D Eikonal-Diffusion PDE class for computing a distance function

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/Distance/PDEEikonal_Distance2D.h"


using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}


using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEEikonal_Distance2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDEEikonal_Distance2D PDEClass;

  BOOST_CHECK( PDEClass::D == 2 );
  BOOST_CHECK( PDEClass::N == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef PDEEikonal_Distance2D PDEClass;

  PDEClass pde;

  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux )
{
  typedef PDEEikonal_Distance2D PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real alpha = 0.1;
  PDEClass pde(alpha);

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == false );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == true );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // function tests

  Real x, y, time;
  Real sln, slnx, slny;

  x = 0; y = 0, time = 0;
  sln  =  3.263;
  slnx = -0.445;
  slny =  1.741;

  DLA::VectorS<2,Real> X = {x,y};


  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = qDataPrim[0];
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  // unsteady flux
  ArrayQ uCons = 0;
  pde.masterState( x, y, time, q, uCons );
  BOOST_CHECK_CLOSE( qTrue, uCons, tol );

  MatrixQ dudq = 0;
  pde.jacobianMasterState( x, y, time, q, dudq );
  BOOST_CHECK_CLOSE( 1, dudq, tol );

  // set gradient
  qDataPrim[0] = slnx;
  ArrayQ qxTrue = qDataPrim[0];
  ArrayQ qx;
  pde.setDOFFrom( qx, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxTrue, qx, tol );

  qDataPrim[0] = slny;
  ArrayQ qyTrue = qDataPrim[0];
  ArrayQ qy;
  pde.setDOFFrom( qy, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyTrue, qy, tol );

  DLA::VectorS<2, ArrayQ> gradq = {qx, qy};

  // diffusive flux
  ArrayQ fTrue = -alpha*(sln*slnx);
  ArrayQ gTrue = -alpha*(sln*slny);
  ArrayQ f = 0;
  ArrayQ g = 0;
  pde.fluxViscous( x, y, time, q, qx, qy, f, g );
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( gTrue, g, tol );

  // check accumulation
  pde.fluxViscous( x, y, time, q, qx, qy, f, g );
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );
  BOOST_CHECK_CLOSE( 2*gTrue, g, tol );

  // diffusive flux jacobian
  MatrixQ kxxMtx = 0, kxyMtx = 0, kyxMtx = 0, kyyMtx = 0;
  pde.diffusionViscous( x, y, time,
                        q, qx, qy,
                        kxxMtx, kxyMtx,
                        kyxMtx, kyyMtx );
  SANS_CHECK_CLOSE( alpha*sln, kxxMtx, tol, tol );
  SANS_CHECK_CLOSE(         0, kxyMtx, tol, tol );
  SANS_CHECK_CLOSE(         0, kyxMtx, tol, tol );
  SANS_CHECK_CLOSE( alpha*sln, kyyMtx, tol, tol );

  // check accumulation
  pde.diffusionViscous( x, y, time,
                        q, qx, qy,
                        kxxMtx, kxyMtx,
                        kyxMtx, kyyMtx );
  SANS_CHECK_CLOSE( 2*alpha*sln, kxxMtx, tol, tol );
  SANS_CHECK_CLOSE(           0, kxyMtx, tol, tol );
  SANS_CHECK_CLOSE(           0, kyxMtx, tol, tol );
  SANS_CHECK_CLOSE( 2*alpha*sln, kyyMtx, tol, tol );

  // forcing function
  ArrayQ force = 0;
  pde.forcingFunction( x, y, time, force );
  BOOST_CHECK_CLOSE( 1, force, tol );

  // check accumulation
  pde.forcingFunction( x, y, time, force );
  BOOST_CHECK_CLOSE( 2, force, tol );

  // source function
  ArrayQ srcTrue = (1+alpha)*(slnx*slnx + slny*slny);
  ArrayQ src = 0;
  pde.source( x, y, time, q, qx, qy, src );
  BOOST_CHECK_CLOSE( srcTrue, src, tol );

  // check accumulation
  pde.source( x, y, time, q, qx, qy, src );
  BOOST_CHECK_CLOSE( 2*srcTrue, src, tol );

  ArrayQ dsduTrue = 0;
  ArrayQ dsdu = 0;
  pde.jacobianSource( x, y, time, q, qx, qy, dsdu );
  BOOST_CHECK_EQUAL( dsduTrue, dsdu );

  // check accumulation
  pde.jacobianSource( x, y, time, q, qx, qy, dsdu );
  BOOST_CHECK_EQUAL( 2*dsduTrue, dsdu );

  ArrayQ dsduxTrue = 2*(1+alpha)*slnx, dsduyTrue = 2*(1+alpha)*slny;
  ArrayQ dsdux = 0, dsduy = 0;
  pde.jacobianGradientSource( x, y, time, q, qx, qy, dsdux, dsduy );
  BOOST_CHECK_CLOSE( dsduxTrue, dsdux, tol );
  BOOST_CHECK_CLOSE( dsduyTrue, dsduy, tol );

  // check accumulation
  pde.jacobianGradientSource( x, y, time, q, qx, qy, dsdux, dsduy );
  BOOST_CHECK_CLOSE( 2*dsduxTrue, dsdux, tol );
  BOOST_CHECK_CLOSE( 2*dsduyTrue, dsduy, tol );

  // no source trace
  BOOST_CHECK_THROW( pde.sourceTrace( x, y, x, y, time, q, qx, qy, q, qx, qy, src, src ), DeveloperException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic )
{
  typedef PDEEikonal_Distance2D PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  Real time = 0;

  PDEClass pde;

  Real x, y;
  Real dx, dy;
  Real sln;
  Real speed, speedTrue;

  x = 0; y = 0;   // not actually used in functions
  dx = 1.22; dy = -0.432;
  sln  =  3.263;

  // set
  ArrayQ q = sln;

  speedTrue = 0;

  pde.speedCharacteristic( x, y, time, dx, dy, q, speed );
  BOOST_CHECK_EQUAL( speedTrue, speed );

  pde.speedCharacteristic( x, y, time, q, speed );
  BOOST_CHECK_EQUAL( speedTrue, speed );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef PDEEikonal_Distance2D PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  PDEClass pde;

  ArrayQ q = 0;
  BOOST_CHECK( pde.isValidState(q) );

  q = 1;
  BOOST_CHECK( pde.isValidState(q) );

  q = -1;
  BOOST_CHECK( not pde.isValidState(q) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PDEEikonal_Distance2D_pattern.txt", true );

  typedef PDEEikonal_Distance2D PDEClass;

  PDEClass pde;
  pde.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
