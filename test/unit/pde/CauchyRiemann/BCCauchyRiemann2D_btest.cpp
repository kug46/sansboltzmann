// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCCauchyRiemann2D_btest
//
// test of 2-D Cauchy-Riemann BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/CauchyRiemann/BCCauchyRiemann2D.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( BCCauchyRiemann2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  {
  typedef BC<PDECauchyRiemann2D,BCTypeNone> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 2 );
  BOOST_CHECK( BCClass::NBC == 0 );
  BOOST_CHECK( ArrayQ::M == 2 );
  BOOST_CHECK( MatrixQ::M == 2 );
  BOOST_CHECK( MatrixQ::N == 2 );
  }

  {
  typedef BC<PDECauchyRiemann2D,BCTypeDirichlet> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 2 );
  BOOST_CHECK( BCClass::NBC == 2 );
  BOOST_CHECK( ArrayQ::M == 2 );
  BOOST_CHECK( MatrixQ::M == 2 );
  BOOST_CHECK( MatrixQ::N == 2 );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeNone_ctors )
{
  typedef BC<PDECauchyRiemann2D,BCTypeNone> BCClass;

  BCClass bc1;

  BOOST_CHECK( bc1.D == 2 );
  BOOST_CHECK( bc1.N == 2 );

  BCClass bc2(bc1);

  BOOST_CHECK( bc2.D == 2 );
  BOOST_CHECK( bc2.N == 2 );

  BCClass bc3;
  bc3 = bc2;

  BOOST_CHECK( bc3.D == 2 );
  BOOST_CHECK( bc3.N == 2 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeNone_test )
{
  typedef BC<PDECauchyRiemann2D,BCTypeNone> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  BCClass bc;

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 2 );
  BOOST_CHECK( bc.NBC == 0 );

  // function tests

  Real x = 0;
  Real y = 0;
  Real nx = 0.8;
  Real ny = 0.6;

  MatrixQ AMtx = 1;
  MatrixQ BMtx = 1;
  bc.coefficients( x, y, 0, nx, ny, AMtx, BMtx );
  BOOST_CHECK_SMALL( AMtx(0,0), tol );
  BOOST_CHECK_SMALL( AMtx(0,1), tol );
  BOOST_CHECK_SMALL( AMtx(1,0), tol );
  BOOST_CHECK_SMALL( AMtx(1,1), tol );
  BOOST_CHECK_SMALL( BMtx(0,0), tol );
  BOOST_CHECK_SMALL( BMtx(0,1), tol );
  BOOST_CHECK_SMALL( BMtx(1,0), tol );
  BOOST_CHECK_SMALL( BMtx(1,1), tol );

  ArrayQ bcdataVec = 1;
  bc.data( x, y, 0, bcdataVec );
  BOOST_CHECK_SMALL( bcdataVec(0), tol );
  BOOST_CHECK_SMALL( bcdataVec(1), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeDirichlet_ctors )
{
  typedef BC<PDECauchyRiemann2D,BCTypeDirichlet> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  Real Adata[4] = {1, 2, 3, 4};
  Real bcdata[2] = {5, 7};

  MatrixQ AMtx( Adata, 4 );
  MatrixQ BMtx = 0;
  ArrayQ bcdataVec( bcdata, 2 );

  BCClass bc1(AMtx, bcdataVec);

  BOOST_CHECK( bc1.D == 2 );
  BOOST_CHECK( bc1.N == 2 );

  BCClass bc2(bc1);

  BOOST_CHECK( bc2.D == 2 );
  BOOST_CHECK( bc2.N == 2 );

  bc2 = bc1;

  BOOST_CHECK( bc2.D == 2 );
  BOOST_CHECK( bc2.N == 2 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeDirichlet_test )
{
  typedef BC<PDECauchyRiemann2D,BCTypeDirichlet> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real Adata[4] = {1, 2, 3, 4};
  Real bcdata[2] = {5, 7};

  MatrixQ AMtx( Adata, 4 );
  MatrixQ BMtx = 0;
  ArrayQ bcdataVec( bcdata, 2 );

  BCClass bc(AMtx, bcdataVec);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 2 );
  BOOST_CHECK( bc.NBC == 2 );

  // function tests

  Real x = 0;
  Real y = 0;
  Real nx = 0.8;
  Real ny = 0.6;

  AMtx = 0;
  BMtx = 1;
  bc.coefficients( x, y, 0, nx, ny, AMtx, BMtx );
  BOOST_CHECK_CLOSE( Adata[0], AMtx(0,0), tol );
  BOOST_CHECK_CLOSE( Adata[1], AMtx(0,1), tol );
  BOOST_CHECK_CLOSE( Adata[2], AMtx(1,0), tol );
  BOOST_CHECK_CLOSE( Adata[3], AMtx(1,1), tol );
  BOOST_CHECK_SMALL( BMtx(0,0), tol );
  BOOST_CHECK_SMALL( BMtx(0,1), tol );
  BOOST_CHECK_SMALL( BMtx(1,0), tol );
  BOOST_CHECK_SMALL( BMtx(1,1), tol );

  bcdataVec = 0;
  bc.data( x, y, 0, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata[0], bcdataVec(0), tol );
  BOOST_CHECK_CLOSE( bcdata[1], bcdataVec(1), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/BCCauchyRiemann2D_pattern.txt", true );

  {
  typedef BC<PDECauchyRiemann2D,BCTypeNone> BCClass;

  BCClass bc1;
  bc1.dump( 2, output );
  }

  {
  typedef BC<PDECauchyRiemann2D,BCTypeDirichlet> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  Real Adata[4] = {1, 2, 3, 4};
  Real bcdata[2] = {5, 7};

  MatrixQ AMtx( Adata, 4 );
  ArrayQ bcdataVec( bcdata, 2 );

  BCClass bc2(AMtx, bcdataVec);
  bc2.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
