// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDECauchyRiemann2D_SurrealS_btest
//
// test of 2-D Cauchy-Riemann PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/CauchyRiemann/PDECauchyRiemann2D.h"
#include "Surreal/SurrealS.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
template class SurrealS<2>;
template class SurrealS<4>;


//############################################################################//
BOOST_AUTO_TEST_SUITE( PDECauchyRiemann2D_SurrealS_test_suite )


//----------------------------------------------------------------------------//
template <int N>
bool
chkSurreal( const SurrealS<N>& a, const SurrealS<N>& b, Real tol )
{
  bool isEqual = true;
  if (abs(a.value() - b.value()) > tol)
    isEqual = false;
  for (int n = 0; n < N; n++)
    if (abs(a.deriv(n) - b.deriv(n)) > tol)
      isEqual = false;

  if (!isEqual)
  {
    cout << "actual (" << a << ")  expected (" << b << ")  diff (" << a - b << ")" << endl;
  }
#if 0
  else
  {
    cout << "a = (" << a << ")  b = (" << b << ")" << endl;
  }
#endif
  return isEqual;
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef SurrealS<2> SurrealClass;
  typedef PDECauchyRiemann2D PDEClass;
  typedef PDEClass::template ArrayQ<SurrealClass> ArrayQ;
  typedef PDEClass::template MatrixQ<SurrealClass> MatrixQ;

  BOOST_CHECK( PDEClass::D == 2 );
  BOOST_CHECK( PDEClass::N == 2 );
  BOOST_CHECK( ArrayQ::M == 2 );
  BOOST_CHECK( MatrixQ::M == 2 );
  BOOST_CHECK( MatrixQ::N == 2 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux )
{
  typedef SurrealS<2> SurrealClass;
  typedef PDECauchyRiemann2D PDEClass;
  typedef PDEClass::template ArrayQ<SurrealClass> ArrayQ;

  const Real tol = 1.e-13;

  PDEClass pde;

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 2 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == false );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // function tests

  Real x, y;
  Real u, v;

  x = 0; y = 0;   // not actually used in flux functions
  u = 3.26; v = -2.17;

  SurrealClass uS, vS;
  uS.value()  = u;  vS.value()  = v;
  uS.deriv(0) = 1;  vS.deriv(0) = 0;
  uS.deriv(1) = 0;  vS.deriv(1) = 1;

  // set
  SurrealClass qDataPrim[2] = {uS, vS};
//  qDataPrim[0].dump();
//  qDataPrim[1].dump();
  string qNamePrim[2] = {"VelocityX", "VelocityY"};
  ArrayQ q = 0;
  ArrayQ qTrue(qDataPrim, 2);
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 2 );
  BOOST_CHECK_CLOSE( qTrue(0).value(), q(0).value(), tol );
  BOOST_CHECK_CLOSE( qTrue(1).value(), q(1).value(), tol );
  BOOST_CHECK( chkSurreal( qTrue(0), q(0), tol ) );
  BOOST_CHECK( chkSurreal( qTrue(1), q(1), tol ) );

  // advective flux
  SurrealClass fData[2] = {uS, -vS};
  SurrealClass gData[2] = {vS,  uS};
  ArrayQ fTrue(fData, 2);
  ArrayQ gTrue(gData, 2);
  ArrayQ f, g;
  f = 0;
  g = 0;
  pde.fluxAdvective( x, y, 0, q, f, g );
  BOOST_CHECK( chkSurreal( qTrue(0), q(0), tol ) );
  BOOST_CHECK( chkSurreal( qTrue(1), q(1), tol ) );
  BOOST_CHECK( chkSurreal( fTrue(0), f(0), tol ) );
  BOOST_CHECK( chkSurreal( fTrue(1), f(1), tol ) );
  BOOST_CHECK( chkSurreal( gTrue(0), g(0), tol ) );
  BOOST_CHECK( chkSurreal( gTrue(1), g(1), tol ) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxRoe )
{
  typedef SurrealS<4> SurrealClass;
  typedef PDECauchyRiemann2D PDEClass;
  typedef PDEClass::template ArrayQ<SurrealClass> ArrayQ;

  const Real tol = 1.e-13;

  PDEClass pde;

  // flux function test

  Real x, y;
  Real nx, ny;
  Real uL, vL;
  Real uR, vR;

  x = 0; y = 0;   // not actually used in functions
  nx = 1.22; ny = -0.432;
  uL = 3.26; vL = -2.17;
  uR = 1.79; vR =  0.93;

  SurrealClass uLS, vLS;
  uLS.value()  = uL;  vLS.value()  = vL;
  uLS.deriv(0) =  1;  vLS.deriv(0) =  0;
  uLS.deriv(1) =  0;  vLS.deriv(1) =  1;
  uLS.deriv(2) =  0;  vLS.deriv(2) =  0;
  uLS.deriv(3) =  0;  vLS.deriv(3) =  0;

  SurrealClass uRS, vRS;
  uRS.value()  = uR;  vRS.value()  = vR;
  uRS.deriv(0) =  0;  vRS.deriv(0) =  0;
  uRS.deriv(1) =  0;  vRS.deriv(1) =  0;
  uRS.deriv(2) =  1;  vRS.deriv(2) =  0;
  uRS.deriv(3) =  0;  vRS.deriv(3) =  1;

  // set
  string qNamePrim[2] = {"VelocityX", "VelocityY"};
  SurrealClass qLDataPrim[2] = {uLS, vLS};
  ArrayQ qLTrue(qLDataPrim, 2);
  ArrayQ qL;
  pde.setDOFFrom( qL, qLDataPrim, qNamePrim, 2 );
  BOOST_CHECK( chkSurreal( qLTrue(0), qL(0), tol ) );
  BOOST_CHECK( chkSurreal( qLTrue(1), qL(1), tol ) );

  SurrealClass qRDataPrim[2] = {uRS, vRS};
  ArrayQ qRTrue(qRDataPrim, 2);
  ArrayQ qR;
  pde.setDOFFrom( qR, qRDataPrim, qNamePrim, 2 );
  BOOST_CHECK( chkSurreal( qRTrue(0), qR(0), tol ) );
  BOOST_CHECK( chkSurreal( qRTrue(1), qR(1), tol ) );

  // advective normal flux
  Real nmag = sqrt(nx*nx + ny*ny);
  SurrealClass fnData[2] = {0.5*(nx*( uRS + uLS) + ny*(vRS + vLS) - nmag*(uRS - uLS)),
                            0.5*(nx*(-vRS - vLS) + ny*(uRS + uLS) - nmag*(vRS - vLS)) };
  ArrayQ fnTrue(fnData, 2);
  ArrayQ fn;
  fn = 0;
  pde.fluxAdvectiveUpwind( x, y, 0, qL, qR, nx, ny, fn );
  BOOST_CHECK( chkSurreal( qLTrue(0), qL(0), tol ) );
  BOOST_CHECK( chkSurreal( qLTrue(1), qL(1), tol ) );
  BOOST_CHECK( chkSurreal( qRTrue(0), qR(0), tol ) );
  BOOST_CHECK( chkSurreal( qRTrue(1), qR(1), tol ) );
  BOOST_CHECK( chkSurreal( fnTrue(0), fn(0), tol ) );
  BOOST_CHECK( chkSurreal( fnTrue(1), fn(1), tol ) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic )
{
  typedef SurrealS<2> SurrealClass;
  typedef PDECauchyRiemann2D PDEClass;
  typedef PDEClass::template ArrayQ<SurrealClass> ArrayQ;

  const Real tol = 1.e-13;

  PDEClass pde;

  Real x, y;
  Real dx, dy;
  Real u, v;

  x = 0; y = 0;   // not actually used in functions
  dx = 1.22; dy = -0.432;
  u = 3.26; v = -2.17;

  SurrealClass uS = u;
  SurrealClass vS = v;

  SurrealClass qDataPrim[2] = {uS, vS};
  string qNamePrim[2] = {"VelocityX", "VelocityY"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 2 );

  Real speed, speedTrue;
  speedTrue = sqrt(dx*dx + dy*dy);

  pde.speedCharacteristic( x, y, 0, dx, dy, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  pde.speedCharacteristic( x, y, 0, q, speed );
  BOOST_CHECK_CLOSE( 1.0, speed, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
