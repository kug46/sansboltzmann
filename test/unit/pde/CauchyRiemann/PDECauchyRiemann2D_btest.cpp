// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDECauchyRiemann2D_btest
//
// test of 2-D Cauchy-Riemann PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/CauchyRiemann/PDECauchyRiemann2D.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
namespace DLA
{
template class VectorS<2,Real>;
template class MatrixS<2,2,Real>;
}
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDECauchyRiemann2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDECauchyRiemann2D PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;

  BOOST_CHECK( PDEClass::D == 2 );
  BOOST_CHECK( PDEClass::N == 2 );
  BOOST_CHECK( ArrayQ::M == 2 );
  BOOST_CHECK( MatrixQ::M == 2 );
  BOOST_CHECK( MatrixQ::N == 2 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef PDECauchyRiemann2D PDEClass;

  PDEClass pde1;

  BOOST_CHECK( pde1.D == 2 );
  BOOST_CHECK( pde1.N == 2 );

  PDEClass pde2(pde1);

  BOOST_CHECK( pde2.D == 2 );
  BOOST_CHECK( pde2.N == 2 );

  PDEClass pde3;
  pde3 = pde2;

  BOOST_CHECK( pde3.D == 2 );
  BOOST_CHECK( pde3.N == 2 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux )
{
  typedef PDECauchyRiemann2D PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  PDEClass pde;

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 2 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == false );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // function tests

  Real x, y;
  Real u, v;

  x = 0; y = 0;   // not actually used in flux functions
  u = 3.26; v = -2.17;

  // set
  Real qDataPrim[2] = {u, v};
  string qNamePrim[2] = {"VelocityX", "VelocityY"};
  ArrayQ qTrue(qDataPrim, 2);
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 2 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );

  // advective flux
  Real fData[2] = {u, -v};
  Real gData[2] = {v,  u};
  ArrayQ fTrue(fData, 2);
  ArrayQ gTrue(gData, 2);
  ArrayQ f, g;
  f = 0;
  g = 0;
  pde.fluxAdvective( x, y, 0, q, f, g );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( gTrue(1), g(1), tol );

  //check accumulation
  pde.fluxAdvective( x, y, 0, q, f, g );
  BOOST_CHECK_CLOSE( 2*fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( 2*gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( 2*gTrue(1), g(1), tol );

  // advective flux jacobian
  MatrixQ dfdu = 0, dgdu = 0;
  pde.jacobianFluxAdvective( x, y, 0, q, dfdu, dgdu );
  BOOST_CHECK_CLOSE(  1, dfdu(0,0), tol );
  BOOST_CHECK_CLOSE(  0, dfdu(0,1), tol );
  BOOST_CHECK_CLOSE(  0, dfdu(1,0), tol );
  BOOST_CHECK_CLOSE( -1, dfdu(1,1), tol );
  BOOST_CHECK_CLOSE(  0, dgdu(0,0), tol );
  BOOST_CHECK_CLOSE(  1, dgdu(0,1), tol );
  BOOST_CHECK_CLOSE(  1, dgdu(1,0), tol );
  BOOST_CHECK_CLOSE(  0, dgdu(1,1), tol );

  //check accumulation
  pde.jacobianFluxAdvective( x, y, 0, q, dfdu, dgdu );
  BOOST_CHECK_CLOSE(  2, dfdu(0,0), tol );
  BOOST_CHECK_CLOSE(  0, dfdu(0,1), tol );
  BOOST_CHECK_CLOSE(  0, dfdu(1,0), tol );
  BOOST_CHECK_CLOSE( -2, dfdu(1,1), tol );
  BOOST_CHECK_CLOSE(  0, dgdu(0,0), tol );
  BOOST_CHECK_CLOSE(  2, dgdu(0,1), tol );
  BOOST_CHECK_CLOSE(  2, dgdu(1,0), tol );
  BOOST_CHECK_CLOSE(  0, dgdu(1,1), tol );

  // Call these simply to make sure coverage is high
  Real dummy = 0;
  ArrayQ uCons, ft, qL, qR, qx, qy, qxL, qxR, qyL, qyR, source;
  MatrixQ kxx, kxy, kyx, kyy;
  pde.masterState(dummy, dummy, dummy, q, uCons );
  BOOST_CHECK_CLOSE(  q(0), uCons(0), tol );
  BOOST_CHECK_CLOSE(  q(1), uCons(1), tol );

  pde.fluxAdvectiveTime(dummy, dummy, dummy, q, ft );
  pde.fluxViscous( dummy, dummy, dummy, q, qx, qy, f, g );
  pde.diffusionViscous( dummy, dummy, dummy, q, qx, qy, kxx, kxy, kyx, kyy );
  pde.source( dummy, dummy, dummy, q, qx, qy, source );
  pde.forcingFunction( x, y, dummy, source );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxRoe )
{
  typedef PDECauchyRiemann2D PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  PDEClass pde;

  // flux function test

  Real x, y;
  Real nx, ny;
  Real uL, vL;
  Real uR, vR;

  x = 0; y = 0;   // not actually used in functions
  nx = 1.22; ny = -0.432;
  uL = 3.26; vL = -2.17;
  uR = 1.79; vR =  0.93;

  // set
  Real qLDataPrim[2] = {uL, vL};
  string qLNamePrim[2] = {"VelocityX", "VelocityY"};
  ArrayQ qLTrue(qLDataPrim, 2);
  ArrayQ qL;
  pde.setDOFFrom( qL, qLDataPrim, qLNamePrim, 2 );
  BOOST_CHECK_CLOSE( qLTrue(0), qL(0), tol );
  BOOST_CHECK_CLOSE( qLTrue(1), qL(1), tol );

  Real qRDataPrim[2] = {uR, vR};
  string qRNamePrim[2] = {"VelocityX", "VelocityY"};
  ArrayQ qRTrue(qRDataPrim, 2);
  ArrayQ qR;
  pde.setDOFFrom( qR, qRDataPrim, qRNamePrim, 2 );
  BOOST_CHECK_CLOSE( qRTrue(0), qR(0), tol );
  BOOST_CHECK_CLOSE( qRTrue(1), qR(1), tol );

  // advective normal flux
  Real nmag = sqrt(nx*nx + ny*ny);
  Real fnData[2] = {0.5*(nx*( uR + uL) + ny*(vR + vL) - nmag*(uR - uL)),
                    0.5*(nx*(-vR - vL) + ny*(uR + uL) - nmag*(vR - vL)) };
  ArrayQ fnTrue(fnData, 2);
  ArrayQ fn;
  fn = 0;
  pde.fluxAdvectiveUpwind( x, y, 0, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );

  //check accumulation
  pde.fluxAdvectiveUpwind( x, y, 0, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(1), fn(1), tol );

  //Call these simply to make sure coverage is high
  ArrayQ qxL, qxR, qyL, qyR, sourceL, sourceR;
  Real dummy = 0;
  pde.fluxViscous( dummy, dummy, dummy, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );
  pde.sourceTrace( dummy, dummy, dummy, dummy, dummy, qL, qxL, qyL, qR, qxR, qyR, sourceL, sourceR );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic )
{
  typedef PDECauchyRiemann2D PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  PDEClass pde;

  Real x, y;
  Real dx, dy;
  Real u, v;
  Real speed, speedTrue;

  x = 0; y = 0;   // not actually used in functions
  dx = 1.22; dy = -0.432;
  u = 3.26; v = -2.17;

  speedTrue = sqrt(dx*dx + dy*dy);

  Real qDataPrim[2] = {u, v};
  string qNamePrim[2] = {"VelocityX", "VelocityY"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 2 );

  pde.speedCharacteristic( x, y, 0, dx, dy, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  pde.speedCharacteristic( x, y, 0, q, speed );
  BOOST_CHECK_CLOSE( 1.0, speed, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef PDECauchyRiemann2D PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  PDEClass pde;

  ArrayQ q = 0;
  BOOST_CHECK( pde.isValidState(q) );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
