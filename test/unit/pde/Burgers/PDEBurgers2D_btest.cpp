// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEBurgers2D_btest
//
// test of 2D Burgers PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "pde/Burgers/PDEBurgers2D.h"
#include "pde/Burgers/BurgersConservative2D.h"

#include "pde/ForcingFunction2D_MMS.h"


using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}


using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEBurgers2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDEBurgers<PhysD2,
                     BurgersConservative2D,
                     ViscousFlux2D_Uniform,
                     Source2D_None> PDEBurgers2D;
  typedef PDEBurgers2D PDEClass;

  BOOST_CHECK( PDEClass::D == 2 );
  BOOST_CHECK( PDEClass::N == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef PDEBurgers<PhysD2,
                     BurgersConservative2D,
                     ViscousFlux2D_Uniform,
                     Source2D_None> PDEBurgers2D;
  typedef PDEBurgers2D PDEClass;

  Real v = 0.2;

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( v, visc, source );

  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux )
{
  typedef PDEBurgers<PhysD2,
                     BurgersConservative2D,
                     ViscousFlux2D_Uniform,
                     Source2D_None > PDEBurgers2D;
  typedef PDEBurgers2D PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real v = 0.2;

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  typedef ForcingFunction2D_Const<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(1) );

  PDEClass pde(v, visc, source, forcingptr);

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == true );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // function tests

  Real x, y, time;
  Real sln, slnx, slny;

  x = 0; y = 0, time = 0;
  sln  =  3.263;
  slnx = -0.445;
  slny =  1.741;

  DLA::VectorS<2,Real> X = {x,y};


  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = qDataPrim[0];
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  // master state
  ArrayQ uCons = 0;
  pde.masterState( x, y, time, q, uCons );
  BOOST_CHECK_CLOSE( qTrue, uCons, tol );

  // Should not accumulate
  pde.masterState( x, y, time, q, uCons );
  BOOST_CHECK_CLOSE( qTrue, uCons, tol );

  // unsteady flux
  ArrayQ ft = 0;
  pde.fluxAdvectiveTime( x, y, time, q, ft );
  BOOST_CHECK_CLOSE( qTrue, ft, tol );

  // Flux accumulation
  pde.fluxAdvectiveTime( x, y, time, q, ft );
  BOOST_CHECK_CLOSE( 2*qTrue, ft, tol );

  MatrixQ dudq = 0;
  pde.jacobianMasterState( x, y, time, q, dudq );
  BOOST_CHECK_CLOSE( 1, dudq, tol );

  // Should not accumulate
  pde.jacobianMasterState( x, y, time, q, dudq );
  BOOST_CHECK_CLOSE( 1, dudq, tol );

  // advective flux
  Real fData[1] = {0.5*sln*sln};
  Real gData[1] = {v*sln};
  ArrayQ fTrue = fData[0];
  ArrayQ gTrue = gData[0];
  ArrayQ f, g;
  DLA::VectorS<2,ArrayQ> F;
  f = 0;
  g = 0;
  pde.fluxAdvective( x, y, time, q, f, g );
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( gTrue, g, tol );

  // Flux accumulate
  pde.fluxAdvective( x, y, time, q, f, g );
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );
  BOOST_CHECK_CLOSE( 2*gTrue, g, tol );


  // advective flux jacobian
  MatrixQ dfdu = 0;
  MatrixQ dgdu = 0;
  pde.jacobianFluxAdvective( x, y, time, q, dfdu, dgdu );
  BOOST_CHECK_CLOSE( sln, dfdu, tol );
  BOOST_CHECK_CLOSE( v, dgdu, tol );

  // Flux accumulation
  pde.jacobianFluxAdvective( x, y, time, q, dfdu, dgdu );
  BOOST_CHECK_CLOSE( 2*sln, dfdu, tol );
  BOOST_CHECK_CLOSE( 2*v, dgdu, tol );

  // set gradient
  qDataPrim[0] = slnx;
  ArrayQ qxTrue = qDataPrim[0];
  ArrayQ qx;
  pde.setDOFFrom( qx, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxTrue, qx, tol );

  qDataPrim[0] = slny;
  ArrayQ qyTrue = qDataPrim[0];
  ArrayQ qy;
  pde.setDOFFrom( qy, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyTrue, qy, tol );

  DLA::VectorS<2, ArrayQ> gradq = {qx, qy};

  // diffusive flux
  fTrue = -(kxx*slnx + kxy*slny);
  gTrue = -(kxy*slnx + kyy*slny);
  f = 0;
  g = 0;
  pde.fluxViscous( x, y, time, q, qx, qy, f, g );
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( gTrue, g, tol );

  // Flux accumulation
  pde.fluxViscous( x, y, time, q, qx, qy, f, g );
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );
  BOOST_CHECK_CLOSE( 2*gTrue, g, tol );

  // diffusive flux jacobian
  MatrixQ kxxMtx = 0, kxyMtx = 0, kyxMtx = 0, kyyMtx = 0;
  pde.diffusionViscous( x, y, time, q, qx, qy, kxxMtx, kxyMtx, kyxMtx, kyyMtx );
  BOOST_CHECK_CLOSE( kxx, kxxMtx, tol );
  BOOST_CHECK_CLOSE( kxy, kxyMtx, tol );
  BOOST_CHECK_CLOSE( kxy, kyxMtx, tol );
  BOOST_CHECK_CLOSE( kyy, kyyMtx, tol );

  // Flux accumulation
  pde.diffusionViscous( x, y, time, q, qx, qy, kxxMtx, kxyMtx, kyxMtx, kyyMtx );
  BOOST_CHECK_CLOSE( 2*kxx, kxxMtx, tol );
  BOOST_CHECK_CLOSE( 2*kxy, kxyMtx, tol );
  BOOST_CHECK_CLOSE( 2*kxy, kyxMtx, tol );
  BOOST_CHECK_CLOSE( 2*kyy, kyyMtx, tol );

  // forcing function
  ArrayQ src = 0;
  pde.forcingFunction( x, y, time, src );
  BOOST_CHECK_CLOSE( 1, src, tol );

  // for coverage; functions are empty
  pde.source( x, y, time, q, qx, qy, src );
  pde.sourceTrace( x, y, x, y, time, q, qx, qy, q, qx, qy, src, src );
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxNormal )
{
  typedef PDEBurgers<PhysD2,
                     BurgersConservative2D,
                     ViscousFlux2D_Uniform,
                     Source2D_None > PDEBurgers2D;
  typedef PDEBurgers2D PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  Real kxx = 2.123;
  ViscousFlux2D_Uniform visc(kxx);

  Source2D_None source;

  ScalarFunction2D_Const solnExact(0.0);

  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  Real time = 0;

  PDEClass pde(visc, source, forcingptr);

  // advective flux function

  Real x;
  Real nx;
  Real slnL, slnR;

  x = 0;  // not actually used in functions
  nx = 1.22;
  slnL = 3.26; slnR = 1.79;

  // set
  ArrayQ qL = slnL;
  ArrayQ qR = slnR;

  // advective normal flux - slnL > slnR => a = slnL
  ArrayQ fnTrue = 0.5*( (nx*0.5*slnR*slnR) + (nx*0.5*slnL*slnL) ) - 0.5*nx*slnL*(slnR - slnL);
  ArrayQ fn;
  fn = 0;
  pde.fluxAdvectiveUpwind( x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( fnTrue, fn, tol );

  // viscous flux function

  Real slnxL;
  Real slnxR;

  slnxL = 1.325;
  slnxR = 0.327;

  // set gradient
  ArrayQ qxL = slnxL;
  ArrayQ qxR = slnxR;

  // viscous normal flux
  fnTrue = -0.5*((nx*kxx)*(slnxL + slnxR));
  fn = 0;
  pde.fluxViscous( x, time, qL, qxL, qR, qxR, nx, fn );
  BOOST_CHECK_CLOSE( fnTrue, fn, tol );
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic )
{
  typedef PDEBurgers<PhysD2,
                     BurgersConservative2D,
                     ViscousFlux2D_Uniform,
                     Source2D_None > PDEBurgers2D;
  typedef PDEBurgers2D PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  Real v = 0.2;

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  ScalarFunction2D_Const solnExact(0.0);

  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  PDEClass pde(v, visc, source, forcingptr);

  Real x, y;
  Real dx, dy;
  Real sln;
  Real speed, speedTrue;
  Real time = 0;

  x = 0; y = 0;   // not actually used in functions
  dx = 1.22; dy = -0.432;
  sln  =  3.263;

  // set
  ArrayQ q = sln;
  speedTrue = fabs(dx*sln + dy*v)/sqrt(dx*dx + dy*dy);

  pde.speedCharacteristic( x, y, time, dx, dy, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  pde.speedCharacteristic( x, y, time, q, speed );
  BOOST_CHECK_CLOSE( sqrt(sln*sln + v*v), speed, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef PDEBurgers<PhysD2,
                     BurgersConservative2D,
                     ViscousFlux2D_Uniform,
                     Source2D_None > PDEBurgers2D;
  typedef PDEBurgers2D PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  Real v = 0.2;

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  ScalarFunction2D_Const solnExact(0.0);

  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  PDEClass pde(v, visc, source, forcingptr);

  ArrayQ q = 0;
  BOOST_CHECK( pde.isValidState(q) );
}




//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PDEBurgers2D_pattern.txt", true );

  typedef PDEBurgers<PhysD2,
                     BurgersConservative2D,
                     ViscousFlux2D_Uniform,
                     Source2D_None> PDEBurgers2D;

  typedef PDEBurgers2D PDEClass;

  Real v = 0.2;

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde1(v, visc, source);
  pde1.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
