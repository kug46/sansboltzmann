// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCBurgers1D_btest
//
// test of 1-D Burgers BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/Burgers/BCBurgers1D.h"
#include "pde/Burgers/PDEBurgers1D.h"
#include "pde/Burgers/BurgersConservative1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/BCParameters.h"

using namespace std;
using namespace SANS;


typedef PDEBurgers<PhysD1,
                   BurgersConservative1D,
                   ViscousFlux1D_Uniform,
                   Source1D_None> PDEBurgers1D;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
// Instantiate BCParamaters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters< BCBurgers1DVector<BurgersConservative1D, ViscousFlux1D_Uniform, Source1D_None> >;
typedef BCParameters< BCBurgers1DVector<BurgersConservative1D, ViscousFlux1D_Uniform, Source1D_None> > BCParams;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( BCBurgers1D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  {
  typedef BCBurgers<PhysD1,BCTypeLinearRobin_mitLG> BCClass;

  BOOST_CHECK( BCClass::D == 1 );
  BOOST_CHECK( BCClass::N == 1 );
  BOOST_CHECK( BCClass::NBC == 1 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCParameters_test )
{
  BOOST_CHECK_EQUAL( "None"              , BCParams::params.BC.None );
  BOOST_CHECK_EQUAL( "LinearRobin_mitLG" , BCParams::params.BC.LinearRobin_mitLG );
  BOOST_CHECK_EQUAL( "LinearRobin_sansLG", BCParams::params.BC.LinearRobin_sansLG );
  BOOST_CHECK_EQUAL( "Function_mitState" , BCParams::params.BC.Function_mitState);
  BOOST_CHECK_EQUAL( "FunctionLinearRobin_mitLG"    , BCParams::params.BC.FunctionLinearRobin_mitLG);
  BOOST_CHECK_EQUAL( "FunctionLinearRobin_sansLG"   , BCParams::params.BC.FunctionLinearRobin_sansLG );

  PyDict BC0;
  BC0[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BC3;
  BC3[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BC3[BCBurgersParams<PhysD1, BCTypeLinearRobin_mitLG>::params.A] = 2;
  BC3[BCBurgersParams<PhysD1, BCTypeLinearRobin_mitLG>::params.B] = 3;
  BC3[BCBurgersParams<PhysD1, BCTypeLinearRobin_mitLG>::params.bcdata] = 4;

  PyDict BCList;
  BCList["BC0"] = BC0;
  BCList["BC3"] = BC3;

  //No exceptions should be thrown
  BCParams::checkInputs(BCList);

  // Construct a pde for the BC's
  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEBurgers1D pde(visc, source);

  // Create the BC's
  std::map<std::string, std::shared_ptr<BCBase> > BCs = BCParams::createBCs<BCNDConvertSpace>(pde, BCList);

  BOOST_CHECK_EQUAL( 2, BCs.size() );

  // The python dictionary does not preserve order, so the count of each BC should be 1
  int NoneCount = 0;
  int LinearRobinCount = 0;

  // Extract the keys from the dictionary
  std::vector<std::string> keys = BCList.stringKeys();

  for ( std::size_t i = 0; i < keys.size(); i++ )
  {
    if ( BCs[keys[i]]->derivedTypeID() == typeid(BCNDConvertSpace<PhysD1,BCNone<PhysD1,AdvectionDiffusionTraits<PhysD1>::N>>) )
      NoneCount++;
    else if ( BCs[keys[i]]->derivedTypeID() == typeid(BCNDConvertSpace<PhysD1,BCBurgers<PhysD1,BCTypeLinearRobin_mitLG>>) )
      LinearRobinCount++;
    else
      BOOST_CHECK( false ); // Failure..
  }

  BOOST_CHECK_EQUAL( 1, NoneCount );
  BOOST_CHECK_EQUAL( 1, LinearRobinCount );

  std::map< std::string, std::vector<int> > BoundaryGroups;

  BoundaryGroups["BC0"] = {0};
  BoundaryGroups["BC3"] = {4};

  std::vector<int> boundaryGroupsLG = BCParams::getLGBoundaryGroups(BCList, BoundaryGroups);

  BOOST_REQUIRE_EQUAL( 1, boundaryGroupsLG.size() );
  BOOST_CHECK_EQUAL( 4, boundaryGroupsLG[0] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCNone_test )
{
  typedef BCNone<PhysD1,AdvectionDiffusionTraits<PhysD1>::N> BCClass;

  BCClass bc;

  // static tests
  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 0 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeLinearRobin_mitLG_test )
{
  typedef BCBurgers<PhysD1,BCTypeLinearRobin_mitLG> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;
  typedef PDEBurgers1D PDEClass;

  const Real tol = 1.e-13;
  Real time = 0;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde(visc, source);

  Real A = 1;
  Real B = 0.2;
  Real bcdata = 3;

  BCClass bc( A, B, bcdata );

  PyDict BCDict;
  BCDict[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BCDict[BCBurgersParams<PhysD1, BCTypeLinearRobin_mitLG>::params.A] = A;
  BCDict[BCBurgersParams<PhysD1, BCTypeLinearRobin_mitLG>::params.B] = B;
  BCDict[BCBurgersParams<PhysD1, BCTypeLinearRobin_mitLG>::params.bcdata] = bcdata;
  PyDict BCList;
  BCList["BCDict"] = BCDict;
  BCParams::checkInputs(BCList);
  BCClass bc2( pde, BCDict );

  // static tests
  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  BOOST_CHECK( bc2.D == 1 );
  BOOST_CHECK( bc2.N == 1 );
  BOOST_CHECK( bc2.NBC == 1 );

  // function tests

  MatrixQ AMtx;
  MatrixQ BMtx;
  ArrayQ bcdataVec;

  Real x = 0;
  Real nx = 0.8;
  //Real ny = 0.6;

  AMtx = 0;
  BMtx = 0;
  bc.coefficients( x, time, nx, AMtx, BMtx );
  BOOST_CHECK_CLOSE( A, AMtx, tol );
  BOOST_CHECK_CLOSE( B, BMtx, tol );

  bc2.coefficients( x, time, nx, AMtx, BMtx );
  BOOST_CHECK_CLOSE( A, AMtx, tol );
  BOOST_CHECK_CLOSE( B, BMtx, tol );

  bcdataVec = 0;
  bc.data( x, time, nx, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );

  bc2.data( x, time, nx, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeLinearRobin_sansLG_test )
{
  typedef BCBurgers<PhysD1,BCTypeLinearRobin_sansLG> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;
  typedef PDEBurgers1D PDEClass;

  const Real tol = 1.e-13;
  Real time = 0;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde(visc, source);

  Real A = 1;
  Real B = 0.2;
  Real bcdata = 3;

  BCClass bc( A, B, bcdata );

  PyDict BCDict;
  BCDict[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCDict[BCBurgersParams<PhysD1, BCTypeLinearRobin_sansLG>::params.A] = A;
  BCDict[BCBurgersParams<PhysD1, BCTypeLinearRobin_sansLG>::params.B] = B;
  BCDict[BCBurgersParams<PhysD1, BCTypeLinearRobin_sansLG>::params.bcdata] = bcdata;
  PyDict BCList;
  BCList["BCDict"] = BCDict;
  BCParams::checkInputs(BCList);
  BCClass bc2( pde, BCDict );

  // static tests
  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  BOOST_CHECK( bc2.D == 1 );
  BOOST_CHECK( bc2.N == 1 );
  BOOST_CHECK( bc2.NBC == 1 );

  // function tests

  MatrixQ AMtx;
  MatrixQ BMtx;
  ArrayQ bcdataVec;

  Real x = 0;
  Real nx = 0.8;
  //Real ny = 0.6;

  AMtx = 0;
  BMtx = 0;
  bc.coefficients( x, time, nx, AMtx, BMtx );
  BOOST_CHECK_CLOSE( A, AMtx, tol );
  BOOST_CHECK_CLOSE( B, BMtx, tol );

  bc2.coefficients( x, time, nx, AMtx, BMtx );
  BOOST_CHECK_CLOSE( A, AMtx, tol );
  BOOST_CHECK_CLOSE( B, BMtx, tol );

  bcdataVec = 0;
  bc.data( x, time, nx, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );

  bc2.data( x, time, nx, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFlux_test )
{
  typedef BCBurgers< PhysD2, BCTypeFlux<PDEBurgers2D> > BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  typedef PDEBurgers2D PDEClass;

  const Real tol = 1.e-13;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  Real time = 0;

  PDEClass pde(adv, visc, source);

  Real bcdata = 3;

  BCClass bc( pde, bcdata );

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  Real x = 0;
  Real y = 0;
  Real nx = 0.8;
  Real ny = 0.6;

  Real A = nx*u + ny*v;
  Real B = -1;

  MatrixQ AMtx;
  MatrixQ BMtx;
  bc.coefficients( x, y, time, nx, ny, AMtx, BMtx );
  BOOST_CHECK_CLOSE( A, AMtx, tol );
  BOOST_CHECK_CLOSE( B, BMtx, tol );

  ArrayQ bcdataVec;
  bc.data( x, y, time, nx, ny, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFunctionLinearRobin_mitLG_test )
{
  typedef BCBurgers< PhysD1, BCTypeFunctionLinearRobin_mitLG > BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;
  typedef PDEBurgers1D PDEClass;

  const Real tol = 1.e-13;

  typedef ScalarFunction1D_Sine SolutionType;
  std::shared_ptr<SolutionType> slnExact( new SolutionType );

  Real kxx = 1.003;
  ViscousFlux1D_Uniform visc(kxx);

  {
    BCClass bc( slnExact, visc );

    Real time = 0;

    // static tests
    BOOST_CHECK( bc.D == 1 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 1 );

    // function tests

    MatrixQ AMtx;
    MatrixQ BMtx;
    ArrayQ bcdataVec;

    Real x = 0.75;
    Real nx = 0.8;

    AMtx = 0;
    BMtx = 0;
    bc.coefficients( x, time, nx, AMtx, BMtx );
    BOOST_CHECK_CLOSE( 1, AMtx, tol );
    BOOST_CHECK_CLOSE( 0, BMtx, tol );

    bcdataVec = 0;
    bc.data( x, time, nx, bcdataVec );
    BOOST_CHECK_CLOSE( -1, bcdataVec, tol );
  }

  {
    Real A = 0.35;
    Real B = 0.84;
    BCClass bc( slnExact, visc, A, B );

    Real time = 0;

    // static tests
    BOOST_CHECK( bc.D == 1 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 1 );

    // function tests

    MatrixQ AMtx;
    MatrixQ BMtx;
    ArrayQ bcdataVec;

    Real x = 0.75;
    Real nx = 0.8;

    AMtx = 0;
    BMtx = 0;
    bc.coefficients( x, time, nx, AMtx, BMtx );
    BOOST_CHECK_CLOSE( A, AMtx, tol );
    BOOST_CHECK_CLOSE( B, BMtx, tol );

    Real u = sin(2*PI*x);
    Real ux = 2*PI*cos(2*PI*x);
    Real bcdataTrue = A*u + B*kxx*ux*nx;

    bcdataVec = 0;
    bc.data( x, time, nx, bcdataVec );
    BOOST_CHECK_CLOSE( bcdataTrue, bcdataVec, tol );
  }

  {
    Real kxx = 2.123;
    ViscousFlux1D_Uniform visc(kxx);

    Source1D_None source;

    PDEClass pde(visc, source);

    PyDict SolnDict;
    SolnDict[BCBurgersParams<PhysD1, BCTypeFunctionLinearRobin_mitLG>::params.Function.Name] =
        BCBurgersParams<PhysD1, BCTypeFunctionLinearRobin_mitLG>::params.Function.Sine;
    Real A = 0.35;
    Real B = 0.84;

    PyDict BCDict;
    BCDict[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
    BCDict[BCBurgersParams<PhysD1, BCTypeFunctionLinearRobin_mitLG>::params.Function] = SolnDict;
    BCDict[BCBurgersParams<PhysD1, BCTypeFunctionLinearRobin_mitLG>::params.A] = A;
    BCDict[BCBurgersParams<PhysD1, BCTypeFunctionLinearRobin_mitLG>::params.B] = B;
    PyDict BCList;
    BCList["BCDict"] = BCDict;
    BCParams::checkInputs(BCList);
    BCClass bc( pde, BCDict );

    Real time = 0;

    // static tests
    BOOST_CHECK( bc.D == 1 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 1 );

    // function tests

    MatrixQ AMtx;
    MatrixQ BMtx;
    ArrayQ bcdataVec;

    Real x = 0.75;
    Real nx = 0.8;

    AMtx = 0;
    BMtx = 0;
    bc.coefficients( x, time, nx, AMtx, BMtx );
    BOOST_CHECK_CLOSE( A, AMtx, tol );
    BOOST_CHECK_CLOSE( B, BMtx, tol );

    Real u = sin(2*PI*x);
    Real ux = 2*PI*cos(2*PI*x);
    Real bcdataTrue = A*u + B*kxx*ux*nx;

    bcdataVec = 0;
    bc.data( x, time, nx, bcdataVec );
    BOOST_CHECK_CLOSE( bcdataTrue, bcdataVec, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFunctionLinearRobin_sansLG_test )
{
  typedef BCBurgers< PhysD1, BCTypeFunctionLinearRobin_sansLG > BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;
  typedef PDEBurgers1D PDEClass;

  const Real tol = 1.e-13;

  typedef ScalarFunction1D_Sine SolutionType;
  std::shared_ptr<SolutionType> slnExact( new SolutionType );

  Real kxx = 1.003;
  ViscousFlux1D_Uniform visc(kxx);

  {
    BCClass bc( slnExact, visc );

    Real time = 0;

    // static tests
    BOOST_CHECK( bc.D == 1 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 1 );

    // function tests

    MatrixQ AMtx;
    MatrixQ BMtx;
    ArrayQ bcdataVec;

    Real x = 0.75;
    Real nx = 0.8;

    AMtx = 0;
    BMtx = 0;
    bc.coefficients( x, time, nx, AMtx, BMtx );
    BOOST_CHECK_CLOSE( 1, AMtx, tol );
    BOOST_CHECK_CLOSE( 0, BMtx, tol );

    bcdataVec = 0;
    bc.data( x, time, nx, bcdataVec );
    BOOST_CHECK_CLOSE( -1, bcdataVec, tol );
  }

  {
    Real A = 0.35;
    Real B = 0.84;
    BCClass bc( slnExact, visc, A, B );

    Real time = 0;

    // static tests
    BOOST_CHECK( bc.D == 1 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 1 );

    // function tests

    MatrixQ AMtx;
    MatrixQ BMtx;
    ArrayQ bcdataVec;

    Real x = 0.75;
    Real nx = 0.8;

    AMtx = 0;
    BMtx = 0;
    bc.coefficients( x, time, nx, AMtx, BMtx );
    BOOST_CHECK_CLOSE( A, AMtx, tol );
    BOOST_CHECK_CLOSE( B, BMtx, tol );

    Real u = sin(2*PI*x);
    Real ux = 2*PI*cos(2*PI*x);
    Real bcdataTrue = A*u + B*kxx*ux*nx;

    bcdataVec = 0;
    bc.data( x, time, nx, bcdataVec );
    BOOST_CHECK_CLOSE( bcdataTrue, bcdataVec, tol );
  }

  {
    Real kxx = 2.123;
    ViscousFlux1D_Uniform visc(kxx);

    Source1D_None source;

    PDEClass pde(visc, source);

    PyDict SolnDict;
    SolnDict[BCBurgersParams<PhysD1, BCTypeFunctionLinearRobin_sansLG>::params.Function.Name] =
        BCBurgersParams<PhysD1, BCTypeFunctionLinearRobin_sansLG>::params.Function.Sine;
    Real A = 0.35;
    Real B = 0.84;

    PyDict BCDict;
    BCDict[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
    BCDict[BCBurgersParams<PhysD1, BCTypeFunctionLinearRobin_sansLG>::params.Function] = SolnDict;
    BCDict[BCBurgersParams<PhysD1, BCTypeFunctionLinearRobin_sansLG>::params.A] = A;
    BCDict[BCBurgersParams<PhysD1, BCTypeFunctionLinearRobin_sansLG>::params.B] = B;
    PyDict BCList;
    BCList["BCDict"] = BCDict;
    BCParams::checkInputs(BCList);
    BCClass bc( pde, BCDict );

    Real time = 0;

    // static tests
    BOOST_CHECK( bc.D == 1 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 1 );

    // function tests

    MatrixQ AMtx;
    MatrixQ BMtx;
    ArrayQ bcdataVec;

    Real x = 0.75;
    Real nx = 0.8;

    AMtx = 0;
    BMtx = 0;
    bc.coefficients( x, time, nx, AMtx, BMtx );
    BOOST_CHECK_CLOSE( A, AMtx, tol );
    BOOST_CHECK_CLOSE( B, BMtx, tol );

    Real u = sin(2*PI*x);
    Real ux = 2*PI*cos(2*PI*x);
    Real bcdataTrue = A*u + B*kxx*ux*nx;

    bcdataVec = 0;
    bc.data( x, time, nx, bcdataVec );
    BOOST_CHECK_CLOSE( bcdataTrue, bcdataVec, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeDirichlet_mitState_test )
{
  typedef BCBurgers< PhysD1, BCTypeDirichlet_mitState<BurgersConservative1D, ViscousFlux1D_Uniform, Source1D_None> > BCClass;
  typedef PDEBurgers1D PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde(visc, source);

  {
    Real qBTrue = 3.0;
    BCClass bc( pde, qBTrue );

    Real time = 0;

    // static tests
    BOOST_CHECK( bc.D == 1 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 1 );

    // function tests
    Real x = 0.75;
    Real nx = 0.8;
    Real qB = 0.0;
    Real qI = qBTrue;
    Real qIx = 0.35;

    BOOST_CHECK( bc.isValidState(nx, qI) == true );

    bc.state( x, time, nx, qI, qB );
    BOOST_CHECK_CLOSE( qBTrue, qB, tol );

    ArrayQ Fn_true = 0.1;
    pde.fluxAdvectiveUpwind(x, time, qI, qB, nx, Fn_true);

    ArrayQ fx_true = 0;
    pde.fluxViscous(x, time, qB, qIx, fx_true);
    Fn_true += fx_true*nx;

    ArrayQ Fn = 0.1;
    bc.fluxNormal(x, time, nx, qI, qIx, qB, Fn);
    BOOST_CHECK_CLOSE( Fn_true, Fn, tol );
  }

  {
    Real qBTrue = 3.0;
    PyDict BCDict;
    BCDict[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
    BCDict[BCBurgersParams<PhysD1, BCTypeDirichlet_mitStateParam>::params.qB] = qBTrue;
    PyDict BCList;
    BCList["BCDict"] = BCDict;
    BCParams::checkInputs(BCList);
    BCClass bc( pde, BCDict );

    Real time = 0;

    // static tests
    BOOST_CHECK( bc.D == 1 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 1 );

    // function tests
    Real x = 0.75;
    Real nx = 0.8;
    Real qB = 0.0;
    Real qI = qBTrue;
    Real qIx = 0.35;

    BOOST_CHECK( bc.isValidState(nx, qI) == true );

    bc.state( x, time, nx, qI, qB );
    BOOST_CHECK_CLOSE( qBTrue, qB, tol );

    ArrayQ Fn_true = 0.1;
    pde.fluxAdvectiveUpwind(x, time, qI, qB, nx, Fn_true);

    ArrayQ fx_true = 0;
    pde.fluxViscous(x, time, qB, qIx, fx_true);
    Fn_true += fx_true*nx;

    ArrayQ Fn = 0.1;
    bc.fluxNormal(x, time, nx, qI, qIx, qB, Fn);
    BOOST_CHECK_CLOSE( Fn_true, Fn, tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeDirichlet_mitStateSpaceTime_test )
{
  typedef BCBurgers< PhysD1, BCTypeDirichlet_mitStateSpaceTime<BurgersConservative1D, ViscousFlux1D_Uniform, Source1D_None> > BCClass;
  typedef PDEBurgers1D PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde(visc, source);

  {
    Real qBTrue = 3.0;
    BCClass bc( pde, qBTrue );

    Real time = 0;

    // static tests
    BOOST_CHECK( bc.D == 1 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 1 );

    // function tests
    Real x = 0.75;
    Real nx = 0.8, nt = 0.2;
    Real qB = 0.0;
    Real qI = qBTrue;
    Real qIx = 0.35, qIt = -0.27;

    BOOST_CHECK( bc.isValidState(nx, nt, qI) == true );

    bc.state( x, time, nx, nt, qI, qB );
    BOOST_CHECK_CLOSE( qBTrue, qB, tol );

    ArrayQ Fn_true = 0.1;
    pde.fluxAdvectiveUpwindSpaceTime(x, time, qI, qB, nx, nt, Fn_true);

    ArrayQ fx_true = 0, ft_true = 0;
    pde.fluxViscousSpaceTime(x, time, qB, qIx, qIt, fx_true, ft_true);
    Fn_true += fx_true*nx + ft_true*nt;

    ArrayQ Fn = 0.1;
    bc.fluxNormalSpaceTime(x, time, nx, nt, qI, qIx, qIt, qB, Fn);
    BOOST_CHECK_CLOSE( Fn_true, Fn, tol );
  }

  {
    Real qBTrue = 3.0;
    PyDict BCDict;
    BCDict[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitStateSpaceTime;
    BCDict[BCBurgersParams<PhysD1, BCTypeDirichlet_mitStateSpaceTimeParam>::params.qB] = qBTrue;
    PyDict BCList;
    BCList["BCDict"] = BCDict;
    BCParams::checkInputs(BCList);
    BCClass bc( pde, BCDict );

    Real time = 0;

    // static tests
    BOOST_CHECK( bc.D == 1 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 1 );

    // function tests
    Real x = 0.75;
    Real nx = 0.8, nt = 0.2;
    Real qB = 0.0;
    Real qI = qBTrue;
    Real qIx = 0.35, qIt = -0.27;

    BOOST_CHECK( bc.isValidState(nx, nt, qI) == true );

    bc.state( x, time, nx, nt, qI, qB );
    BOOST_CHECK_CLOSE( qBTrue, qB, tol );

    ArrayQ Fn_true = 0.1;
    pde.fluxAdvectiveUpwindSpaceTime(x, time, qI, qB, nx, nt, Fn_true);

    ArrayQ fx_true = 0, ft_true = 0;
    pde.fluxViscousSpaceTime(x, time, qB, qIx, qIt, fx_true, ft_true);
    Fn_true += fx_true*nx + ft_true*nt;

    ArrayQ Fn = 0.1;
    bc.fluxNormalSpaceTime(x, time, nx, nt, qI, qIx, qIt, qB, Fn);
    BOOST_CHECK_CLOSE( Fn_true, Fn, tol );
  }
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/BCBurgers1D_pattern.txt", true );

  typedef PDEBurgers1D PDEClass;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde(visc, source);

  {
  typedef BCBurgers<PhysD1,BCTypeLinearRobin_mitLG> BCClass;

  Real A = 1;
  Real B = 0.2;
  Real bcdata = 3;

  BCClass bc1( A, B, bcdata );
  bc1.dump( 2, output );
  }

  {
  typedef BCBurgers<PhysD1,BCTypeDirichlet_mitState<BurgersConservative1D, ViscousFlux1D_Uniform, Source1D_None>> BCClass;
  Real qB = 3;

  BCClass bc1(pde, qB );
  bc1.dump( 2, output );
  }


#if 0
  {
  typedef BCBurgers< PhysD1, BCTypeFlux<PDEBurgers1D> > BCClass;


  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEBurgers1D pde( visc, source );

  Real bcdata = 3;

  BCClass bc2( pde, bcdata );
  bc2.dump( 2, output );
  }
#endif

  BOOST_CHECK( output.match_pattern() );
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
