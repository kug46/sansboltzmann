// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEBurgers1D_btest
//
// test of 1-D Burgers PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "pde/Burgers/PDEBurgers1D.h"
#include "pde/Burgers/BurgersConservative1D.h"

#include "pde/ForcingFunction1D_MMS.h"


using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}


using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEBurgers1D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDEBurgers<PhysD1,
                     BurgersConservative1D,
                     ViscousFlux1D_Uniform,
                     Source1D_None> PDEBurgers1D;
  typedef PDEBurgers1D PDEClass;

  BOOST_CHECK( PDEClass::D == 1 );
  BOOST_CHECK( PDEClass::N == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef PDEBurgers<PhysD1,
                     BurgersConservative1D,
                     ViscousFlux1D_Uniform,
                     Source1D_None> PDEBurgers1D;
  typedef PDEBurgers1D PDEClass;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde1(visc, source);

  BOOST_CHECK( pde1.D == 1 );
  BOOST_CHECK( pde1.N == 1 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux )
{
  typedef PDEBurgers<PhysD1,
                     BurgersConservative1D,
                     ViscousFlux1D_Uniform,
                     Source1D_Uniform > PDEBurgers1D;
  typedef PDEBurgers1D PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);
  Real time = 0;

  ScalarFunction1D_Quad solnExact;

  Real s = 0;
  Source1D_Uniform src(s);

  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> frcptr( new ForcingType(solnExact) );

  PDEClass pde(visc, src, frcptr );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == true );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // function tests

  Real x;
  Real sln, slnx, slnt;

  x = 0;
  sln  =  3.263;
  slnx = -0.445;
  slnt =  0.613;

  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = sln;
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  // master state
  ArrayQ uCons = 0;
  pde.masterState( x, time, q, uCons );
  BOOST_CHECK_CLOSE( qTrue, uCons, tol );

  // Should not accumulate
  pde.masterState( x, time, q, uCons );
  BOOST_CHECK_CLOSE( qTrue, uCons, tol );

  // unsteady flux
  ArrayQ ft = 0;
  pde.fluxAdvectiveTime( x, time, q, ft );
  BOOST_CHECK_CLOSE( qTrue, ft, tol );

  // Flux accumulation
  pde.fluxAdvectiveTime( x, time, q, ft );
  BOOST_CHECK_CLOSE( 2*qTrue, ft, tol );

  MatrixQ dudq = 0;
  pde.jacobianMasterState( x, time, q, dudq );
  BOOST_CHECK_CLOSE( 1, dudq, tol );

  // Should not accumulate
  pde.jacobianMasterState( x, time, q, dudq );
  BOOST_CHECK_CLOSE( 1, dudq, tol );

  // advective flux
  ArrayQ fTrue = 0.5*sln*sln;
  ArrayQ f, g;
  DLA::VectorS<1,ArrayQ> F;
  f = 0;
  pde.fluxAdvective( x, time, q, f );
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  // Flux accumulation
  pde.fluxAdvective( x, time, q, f );
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  // advective flux jacobian
  MatrixQ dfdu = 0.0;
  pde.jacobianFluxAdvective( x, time, q, dfdu );
  BOOST_CHECK_CLOSE( sln, dfdu, tol );

  // Flux accumulation
  pde.jacobianFluxAdvective( x, time, q, dfdu );
  BOOST_CHECK_CLOSE( 2*sln, dfdu, tol );

  // set gradient
  ArrayQ qx = slnx;
  ArrayQ qt = slnt;

  // diffusive flux
  fTrue = -(kxx*slnx);
  f = 0;
  pde.fluxViscous( x, time, q, qx, f );
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  // Flux accumulate
  pde.fluxViscous( x, time, q, qx, f );
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  // space-time diffusive flux
  f = 0;
  g = 0;
  pde.fluxViscousSpaceTime( x, time, q, qx, qt, f, g );
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( 0.0, g, tol );

  // Flux accumulate
  pde.fluxViscousSpaceTime( x, time, q, qx, qt, f, g );
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );
  BOOST_CHECK_CLOSE( 2*0.0, g, tol );

  // diffusive flux jacobian
  MatrixQ kxxMtx = 0;
  pde.diffusionViscous( x, time, q, qx, kxxMtx );
  BOOST_CHECK_CLOSE( kxx, kxxMtx, tol );

  // space-time diffusive flux jacobian
  kxxMtx = 0;
  MatrixQ kxtMtx = 0, ktxMtx = 0, kttMtx = 0;
  pde.diffusionViscousSpaceTime( x, time, q, qx, qt, kxxMtx, kxtMtx, ktxMtx, kttMtx );
  BOOST_CHECK_CLOSE( kxx, kxxMtx, tol );
  BOOST_CHECK_CLOSE( 0.0, kxtMtx, tol );
  BOOST_CHECK_CLOSE( 0.0, ktxMtx, tol );
  BOOST_CHECK_CLOSE( 0.0, kttMtx, tol );

  // Flux accumulate
  pde.diffusionViscousSpaceTime( x, time, q, qx, qt, kxxMtx, kxtMtx, ktxMtx, kttMtx );
  BOOST_CHECK_CLOSE( 2*kxx, kxxMtx, tol );
  BOOST_CHECK_CLOSE( 2*0.0, kxtMtx, tol );
  BOOST_CHECK_CLOSE( 2*0.0, ktxMtx, tol );
  BOOST_CHECK_CLOSE( 2*0.0, kttMtx, tol );

  // forcing function
  ArrayQ forcing = 0;
  x = 0.5;
  pde.forcingFunction( x, time, forcing );
  BOOST_CHECK_CLOSE( 25.476, forcing, tol ); //matlab symbolic MMS

  // Forcing accumulate
  pde.forcingFunction( x, time, forcing );
  BOOST_CHECK_CLOSE( 2*25.476, forcing, tol ); //matlab symbolic MMS

  ArrayQ sourceterm = 0;
  pde.source( x, time, q, qx, sourceterm );

  // for coverage; functions are empty
  pde.sourceTrace( x, x, time, q, qx, q, qx, sourceterm, sourceterm );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxNormal )
{
  typedef PDEBurgers<PhysD1,
                     BurgersConservative1D,
                     ViscousFlux1D_Uniform,
                     Source1D_None > PDEBurgers1D;
  typedef PDEBurgers1D PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  ScalarFunction1D_Const solnExact(0.0);

  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  Real time = 0;

  PDEClass pde(visc, source, forcingptr);

  // advective flux function

  Real x;
  Real nx;
  Real slnL, slnR;

  x = 0;  // not actually used in functions
  nx = 1.22;
  slnL = 3.26; slnR = 1.79;

  // set
  ArrayQ qL = slnL;
  ArrayQ qR = slnR;

  // advective normal flux - slnL > slnR => a = slnL
  ArrayQ fnTrue = 0.5*( (nx*0.5*slnR*slnR) + (nx*0.5*slnL*slnL) ) - 0.5*nx*slnL*(slnR - slnL);
  ArrayQ fn;
  fn = 0;
  pde.fluxAdvectiveUpwind( x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( fnTrue, fn, tol );

  // Flux accumulate
  pde.fluxAdvectiveUpwind( x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue, fn, tol );

  // viscous flux function

  Real slnxL;
  Real slnxR;

  slnxL = 1.325;
  slnxR = 0.327;

  // set gradient
  ArrayQ qxL = slnxL;
  ArrayQ qxR = slnxR;

  // viscous normal flux
  fnTrue = -0.5*((nx*kxx)*(slnxL + slnxR));
  fn = 0;
  pde.fluxViscous( x, time, qL, qxL, qR, qxR, nx, fn );
  BOOST_CHECK_CLOSE( fnTrue, fn, tol );

  // Flux accumulate
  pde.fluxViscous( x, time, qL, qxL, qR, qxR, nx, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue, fn, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic )
{
  typedef PDEBurgers<PhysD1,
                     BurgersConservative1D,
                     ViscousFlux1D_Uniform,
                     Source1D_None > PDEBurgers1D;
  typedef PDEBurgers1D PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  ScalarFunction1D_Const solnExact(0.0);

  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  PDEClass pde(visc, source, forcingptr);

  Real x;
  Real dx;
  Real sln;
  Real speed, speedTrue;
  Real time = 0;

  x = 0;  // not actually used in functions
  dx = 1.22;
  sln  =  3.263;

  // set
  ArrayQ q = sln;

  speedTrue = fabs(dx*sln)/sqrt(dx*dx);

  pde.speedCharacteristic( x, time, dx, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  pde.speedCharacteristic( x, time, q, speed );
  BOOST_CHECK_CLOSE( fabs(sln), speed, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef PDEBurgers<PhysD1,
                     BurgersConservative1D,
                     ViscousFlux1D_Uniform,
                     Source1D_None > PDEBurgers1D;
  typedef PDEBurgers1D PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);
  Source1D_None source;

  ScalarFunction1D_Const solnExact(0.0);

  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  PDEClass pde(visc, source, forcingptr);

  ArrayQ q = 0;
  BOOST_CHECK( pde.isValidState(q) );
}




//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PDEBurgers1D_pattern.txt", true );

  typedef PDEBurgers<PhysD1,
                     BurgersConservative1D,
                     ViscousFlux1D_Uniform,
                     Source1D_None> PDEBurgers1D;

  typedef PDEBurgers1D PDEClass;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  ScalarFunction1D_Const solnExact(1.0);

  //  PDEClass pde1(adv, visc, solnExact);
  PDEClass pde1(visc, source);
  pde1.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
