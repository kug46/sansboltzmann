// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEBurgers_ArtificialViscosity1D_btest
//
// test of 1-D Burgers pde class with artificial viscosity

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "pde/Burgers/PDEBurgers1D.h"
#include "pde/Burgers/PDEBurgers_ArtificialViscosity1D.h"
#include "pde/Burgers/BurgersConservative1D.h"

#include "pde/ForcingFunction1D_MMS.h"

#include "Field/Tuple/ParamTuple.h"


using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}


using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Burgers_ArtificialViscosity1D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDEBurgers_ArtificialViscosity<PhysD1,
                                         BurgersConservative1D,
                                         ViscousFlux1D_Uniform,
                                         Source1D_None> PDEBurgers1D;
  typedef PDEBurgers1D PDEClass;

  BOOST_CHECK( PDEClass::D == 1 );
  BOOST_CHECK( PDEClass::N == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef PDEBurgers<PhysD1,
                     BurgersConservative1D,
                     ViscousFlux1D_Uniform,
                     Source1D_None> PDEBurgers1D;
  typedef PDEBurgers1D PDEClass;
  typedef PDEBurgers_ArtificialViscosity<PhysD1,
                                         BurgersConservative1D,
                                         ViscousFlux1D_Uniform,
                                         Source1D_None> PDEBurgers1DAV;
  typedef PDEBurgers1DAV AVPDEClass;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde1(visc, source);
  AVPDEClass avpde(1, pde1);

  BOOST_CHECK( pde1.D == 1 );
  BOOST_CHECK( pde1.N == 1 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux )
{
  typedef PDEBurgers<PhysD1,
                     BurgersConservative1D,
                     ViscousFlux1D_Uniform,
                     Source1D_Uniform > PDEBurgers1D;
  typedef PDEBurgers1D PDEClass;
  typedef PDEBurgers_ArtificialViscosity<PhysD1,
                                         BurgersConservative1D,
                                         ViscousFlux1D_Uniform,
                                         Source1D_Uniform> PDEBurgers1DAV;
  typedef PDEBurgers1DAV AVPDEClass;
  typedef AVPDEClass::ArrayQ<Real> ArrayQ;
  typedef AVPDEClass::MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD1::D,Real> MatrixSym;
  typedef DLA::MatrixSymS<PhysD2::D,Real> MatrixSym_ST;
  typedef MakeTuple<ParamTuple, MatrixSym, Real>::type ParamType;
  typedef MakeTuple<ParamTuple, MatrixSym_ST, Real>::type ParamType_ST;

  const Real tol = 1.e-13;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);
  Real time = 0;

  ScalarFunction1D_Quad solnExact;

  Real s = 0;
  Source1D_Uniform src(s);

  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> frcptr( new ForcingType(solnExact) );

  int order = 1;

  PDEClass pde(visc, src, frcptr );
  AVPDEClass avpde(order, pde);

  // static tests
  BOOST_CHECK( avpde.D == 1 );
  BOOST_CHECK( avpde.N == 1 );

  // flux components
  BOOST_CHECK( avpde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( avpde.hasFluxAdvective() == true );
  BOOST_CHECK( avpde.hasFluxViscous() == true );
  BOOST_CHECK( avpde.hasSource() == true );
  BOOST_CHECK( avpde.hasForcingFunction() == true );

  BOOST_CHECK( avpde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( avpde.needsSolutionGradientforSource() == false );

  // function tests

  Real x;
  Real sln, slnx, slnt;

  x = 0;
  sln  =  3.263;
  slnx = -0.445;
  slnt =  0.613;

  Real hxx = 0.1, htt = 0.2;
  MatrixSym logH = {log(hxx)};
  MatrixSym_ST logH_ST = {{log(hxx)},{0.0, log(htt)}};

  ParamType param(logH, 0.2); // grid spacing and sensor values
  ParamType_ST param_ST(logH_ST, 0.2);

  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = sln;
  ArrayQ q;
  avpde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  // master state
  ArrayQ uCons = 0;
  avpde.masterState( param, x, time, q, uCons );
  BOOST_CHECK_CLOSE( qTrue, uCons, tol );

  // Should not accumulate
  avpde.masterState( param, x, time, q, uCons );
  BOOST_CHECK_CLOSE( qTrue, uCons, tol );

  // unsteady flux
  ArrayQ ft = 0;
  avpde.fluxAdvectiveTime( param, x, time, q, ft );
  BOOST_CHECK_CLOSE( qTrue, ft, tol );

  // Flux accumulation
  avpde.fluxAdvectiveTime( param, x, time, q, ft );
  BOOST_CHECK_CLOSE( 2*qTrue, ft, tol );

  MatrixQ dudq = 0;
  avpde.jacobianMasterState( param, x, time, q, dudq );
  BOOST_CHECK_CLOSE( 1, dudq, tol );

  // Should not accumulate
  avpde.jacobianMasterState( param, x, time, q, dudq );
  BOOST_CHECK_CLOSE( 1, dudq, tol );

  // advective flux
  ArrayQ fTrue = 0.5*sln*sln;
  ArrayQ f;
  DLA::VectorS<1,ArrayQ> F;
  f = 0;
  avpde.fluxAdvective( param, x, time, q, f );
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  // Flux accumulate
  avpde.fluxAdvective( param, x, time, q, f );
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );

  // advective flux jacobian
  MatrixQ dfdu = 0.0;
  avpde.jacobianFluxAdvective( param, x, time, q, dfdu );
  BOOST_CHECK_CLOSE( sln, dfdu, tol );

  // Flux accumulate
  avpde.jacobianFluxAdvective( param, x, time, q, dfdu );
  BOOST_CHECK_CLOSE( 2*sln, dfdu, tol );

  // set gradient
  ArrayQ qx = slnx;
  ArrayQ qt = slnt;

  // Artificial viscosity with genH scale
  Real lambda = fabs(sln);
  Real kxx_art_true = hxx/(Real(order)+1) * lambda * smoothabs0(param.right(), 1.0e-5);
  MatrixQ kxx_art = 0;
  avpde.artificialViscosity(x, time, q, param.left(), param.right(), kxx_art );
  BOOST_CHECK_CLOSE( kxx_art_true, kxx_art, tol );

  // Flux accumulate
  avpde.artificialViscosity(x, time, q, param.left(), param.right(), kxx_art );
  BOOST_CHECK_CLOSE( 2*kxx_art_true, kxx_art, tol );

  // diffusive matrix
  Real kxx_true = kxx + kxx_art_true;
  MatrixQ kxxMtx = 0;
  avpde.diffusionViscous( param, x, time, q, qx, kxxMtx );
  BOOST_CHECK_CLOSE( kxx_true, kxxMtx, tol );

  // Flux accumulate
  avpde.diffusionViscous( param, x, time, q, qx, kxxMtx );
  BOOST_CHECK_CLOSE( 2*kxx_true, kxxMtx, tol );

  // viscous flux
  fTrue = -((kxx + kxx_art_true)*slnx);
  f = 0;
  avpde.fluxViscous( param, x, time, q, qx, f );
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  // Space-time artificial viscosity
  Real kxt_art_true = 0.0;
  Real ktx_art_true = 0.0;
  Real ktt_art_true = htt/(Real(order)+1) * smoothabs0(param_ST.right(), 1.0e-5);

  kxx_art = 0.0;
  MatrixQ kxt_art = 0, ktx_art = 0, ktt_art = 0;
  avpde.artificialViscositySpaceTime(x, time, q, param_ST.left(), param_ST.right(),
                                     kxx_art, kxt_art, ktx_art, ktt_art );
  BOOST_CHECK_CLOSE( kxx_art_true, kxx_art, tol );
  BOOST_CHECK_CLOSE( kxt_art_true, kxt_art, tol );
  BOOST_CHECK_CLOSE( ktx_art_true, ktx_art, tol );
  BOOST_CHECK_CLOSE( ktt_art_true, ktt_art, tol );

  // Flux accumulate
  avpde.artificialViscositySpaceTime(x, time, q, param_ST.left(), param_ST.right(),
                                     kxx_art, kxt_art, ktx_art, ktt_art );
  BOOST_CHECK_CLOSE( 2*kxx_art_true, kxx_art, tol );
  BOOST_CHECK_CLOSE( 2*kxt_art_true, kxt_art, tol );
  BOOST_CHECK_CLOSE( 2*ktx_art_true, ktx_art, tol );
  BOOST_CHECK_CLOSE( 2*ktt_art_true, ktt_art, tol );

  // Space-time viscous diffusion matrix
  Real ktt_true = ktt_art_true;
  kxxMtx = 0;
  MatrixQ kxtMtx = 0, ktxMtx = 0, kttMtx = 0;
  avpde.diffusionViscousSpaceTime(param_ST, x, time, q, qx, qt, kxxMtx, kxtMtx, ktxMtx, kttMtx );
  BOOST_CHECK_CLOSE( kxx_true, kxxMtx, tol );
  BOOST_CHECK_CLOSE( 0.0, kxtMtx, tol );
  BOOST_CHECK_CLOSE( 0.0, ktxMtx, tol );
  BOOST_CHECK_CLOSE( ktt_true, kttMtx, tol );

  // Flux accumulate
  avpde.diffusionViscousSpaceTime(param_ST, x, time, q, qx, qt, kxxMtx, kxtMtx, ktxMtx, kttMtx );
  BOOST_CHECK_CLOSE( 2*kxx_true, kxxMtx, tol );
  BOOST_CHECK_CLOSE( 2*0.0, kxtMtx, tol );
  BOOST_CHECK_CLOSE( 2*0.0, ktxMtx, tol );
  BOOST_CHECK_CLOSE( 2*ktt_true, kttMtx, tol );

  // Space-time viscous flux
  ArrayQ fvisc_x = 0;
  ArrayQ fvisc_t = 0;
  avpde.fluxViscousSpaceTime(param_ST, x, time, q, qx, qt, fvisc_x, fvisc_t );
  BOOST_CHECK_CLOSE( -(kxx_true*slnx + 0.0*slnt), fvisc_x, tol );
  BOOST_CHECK_CLOSE( -(0.0*slnx + ktt_true*slnt), fvisc_t, tol );

  // Flux accumulate
  avpde.fluxViscousSpaceTime(param_ST, x, time, q, qx, qt, fvisc_x, fvisc_t );
  BOOST_CHECK_CLOSE( 2*-(kxx_true*slnx + 0.0*slnt), fvisc_x, tol );
  BOOST_CHECK_CLOSE( 2*-(0.0*slnx + ktt_true*slnt), fvisc_t, tol );

  // forcing function
  ArrayQ forcing = 0;
  x = 0.5;
  avpde.forcingFunction( param, x, time, forcing );
  BOOST_CHECK_CLOSE( 25.476, forcing, tol ); //matlab symbolic MMS

  // Forcing accumulate
  avpde.forcingFunction( param, x, time, forcing );
  BOOST_CHECK_CLOSE( 2*25.476, forcing, tol ); //matlab symbolic MMS

  ArrayQ sourceterm = 0;
  avpde.source( param, x, time, q, qx, sourceterm );

  // for coverage; functions are empty
  avpde.sourceTrace( param, x, param, x, time, q, qx, q, qx, sourceterm, sourceterm );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxNormal )
{
  typedef PDEBurgers<PhysD1,
                     BurgersConservative1D,
                     ViscousFlux1D_Uniform,
                     Source1D_None > PDEBurgers1D;
  typedef PDEBurgers1D PDEClass;
  typedef PDEBurgers_ArtificialViscosity<PhysD1,
                                         BurgersConservative1D,
                                         ViscousFlux1D_Uniform,
                                         Source1D_None> PDEBurgers1DAV;
  typedef PDEBurgers1DAV AVPDEClass;
  typedef AVPDEClass::ArrayQ<Real> ArrayQ;
//  typedef AVPDEClass::MatrixQ<Real> MatrixQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  const Real tol = 1.e-13;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  ScalarFunction1D_Const solnExact(0.0);

  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  Real time = 0;

  PDEClass pde(visc, source, forcingptr);
  AVPDEClass avpde(1, pde);

  // advective flux function

  Real x;
  Real nx;
  Real slnL, slnR;

  x = 0;  // not actually used in functions
  nx = 1.22;
  slnL = 3.26; slnR = 1.79;

  // set
  ArrayQ qL = slnL;
  ArrayQ qR = slnR;

  ParamType param(0.1, 0.0); // grid spacing and jump values

  // advective normal flux - slnL > slnR => a = slnL
  ArrayQ fnTrue = 0.5*( (nx*0.5*slnR*slnR) + (nx*0.5*slnL*slnL) ) - 0.5*nx*slnL*(slnR - slnL);
  ArrayQ fn;
  fn = 0;
  avpde.fluxAdvectiveUpwind( param, x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( fnTrue, fn, tol );

  // Flux accumulation
  avpde.fluxAdvectiveUpwind( param, x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue, fn, tol );

  // viscous flux function
  //////////////////////////////
  // NOT IMPLEMENTED YET
  //////////////////////////////
//  Real slnxL;
//  Real slnxR;
//
//  slnxL = 1.325;
//  slnxR = 0.327;
//
//  // set gradient
//  ArrayQ qxL = slnxL;
//  ArrayQ qxR = slnxR;
//
//  // viscous normal flux
//  fnTrue = -0.5*((nx*kxx)*(slnxL + slnxR));
//  fn = 0;
//  avpde.fluxViscous( param, x, time, qL, qxL, qR, qxR, nx, fn );
//  BOOST_CHECK_CLOSE( fnTrue, fn, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic )
{
  typedef PDEBurgers<PhysD1,
                     BurgersConservative1D,
                     ViscousFlux1D_Uniform,
                     Source1D_None > PDEBurgers1D;
  typedef PDEBurgers1D PDEClass;
  typedef PDEBurgers_ArtificialViscosity<PhysD1,
                                         BurgersConservative1D,
                                         ViscousFlux1D_Uniform,
                                         Source1D_None> PDEBurgers1DAV;
  typedef PDEBurgers1DAV AVPDEClass;
  typedef AVPDEClass::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  const Real tol = 1.e-13;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  ScalarFunction1D_Const solnExact(0.0);

  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  PDEClass pde(visc, source, forcingptr);
  AVPDEClass avpde(1, pde);

  Real x;
  Real dx;
  Real sln;
  Real speed, speedTrue;
  Real time = 0;

  x = 0;  // not actually used in functions
  dx = 1.22;
  sln  =  3.263;

  // set
  ArrayQ q = sln;

  ParamType param(0.1, 0.0); // grid spacing and jump values

  speedTrue = fabs(dx*sln)/sqrt(dx*dx);

  avpde.speedCharacteristic( param, x, time, dx, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  avpde.speedCharacteristic( param, x, time, q, speed );
  BOOST_CHECK_CLOSE( fabs(sln), speed, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef PDEBurgers<PhysD1,
                     BurgersConservative1D,
                     ViscousFlux1D_Uniform,
                     Source1D_None > PDEBurgers1D;
  typedef PDEBurgers1D PDEClass;
  typedef PDEBurgers_ArtificialViscosity<PhysD1,
                                         BurgersConservative1D,
                                         ViscousFlux1D_Uniform,
                                         Source1D_None> PDEBurgers1DAV;
  typedef PDEBurgers1DAV AVPDEClass;
  typedef AVPDEClass::ArrayQ<Real> ArrayQ;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);
  Source1D_None source;

  ScalarFunction1D_Const solnExact(0.0);

  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  PDEClass pde(visc, source, forcingptr);
  AVPDEClass avpde(1, pde);

  ArrayQ q = 0;
  BOOST_CHECK( pde.isValidState(q) );
}




//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PDEBurgers_ArtificialViscosity1D_pattern.txt", true );

  typedef PDEBurgers<PhysD1,
                     BurgersConservative1D,
                     ViscousFlux1D_Uniform,
                     Source1D_None> PDEBurgers1D;
  typedef PDEBurgers1D PDEClass;
  typedef PDEBurgers_ArtificialViscosity<PhysD1,
                                         BurgersConservative1D,
                                         ViscousFlux1D_Uniform,
                                         Source1D_None> PDEBurgers1DAV;
  typedef PDEBurgers1DAV AVPDEClass;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  ScalarFunction1D_Const solnExact(1.0);

  //  PDEClass pde1(adv, visc, solnExact);
  PDEClass pde(visc, source);
  AVPDEClass avpde(1, pde);
  avpde.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
