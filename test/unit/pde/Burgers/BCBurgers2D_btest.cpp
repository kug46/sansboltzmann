// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCBurgers2D_btest
//
// test of 2-D Advection-Diffusion BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/Burgers/BCBurgers2D.h"
#include "pde/Burgers/PDEBurgers2D.h"
#include "pde/Burgers/BurgersConservative2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#undef ALLOW_NONLINEAR_BCS      // allow nonlinear Dirichlet/Neumann BCs for AD

using namespace std;
using namespace SANS;


typedef PDEBurgers<PhysD2,
                  BurgersConservative2D,
                  ViscousFlux2D_Uniform,
                  Source2D_None> PDEBurgers2D;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
namespace DLA
{
template class VectorS<1,Real>;
template class MatrixS<1,1,Real>;
}

// This function assumes jacobianFluxAdvective is not a function of q, which is clearly not true for Burgers
//template class BCBurgers< PhysD2, BCTypeFlux<PDEBurgers2D> >;
template class BCBurgers< PhysD2, BCTypeDirichlet_mitState<BurgersConservative2D,ViscousFlux2D_Uniform> >;


// Instantiate BCParamaters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters<BCBurgers2DVector<BurgersConservative2D,ViscousFlux2D_Uniform>>;
typedef BCParameters<BCBurgers2DVector<BurgersConservative2D,ViscousFlux2D_Uniform>> BCParams;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( BCBurgers2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  {
  typedef BCBurgers<PhysD2,BCTypeLinearRobin_mitLG> BCClass;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 1 );
  BOOST_CHECK( BCClass::NBC == 1 );
  }

#if 0 // This function assumes jacobianFluxAdvective is not a function of q, which is clearly not true for Burgers
  {
  typedef BCBurgers< PhysD2,BCTypeFlux<PDEBurgers2D> > BCClass;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 1 );
  BOOST_CHECK( BCClass::NBC == 1 );
  }
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeLinearRobin_mitLG_test )
{
  typedef BCBurgers<PhysD2,BCTypeLinearRobin_mitLG> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;
  typedef PDEBurgers2D PDEClass;

  const Real tol = 1.e-13;
  Real time = 0;

  Real v = 0.2;
  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde(v, visc, source);

  Real A = 1;
  Real B = 0.2;
  Real bcdata = 3;

  BCClass bc( A, B, bcdata );

  PyDict BCDict;
  BCDict[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BCDict[BCBurgersParams<PhysD2, BCTypeLinearRobin_mitLG>::params.A] = A;
  BCDict[BCBurgersParams<PhysD2, BCTypeLinearRobin_mitLG>::params.B] = B;
  BCDict[BCBurgersParams<PhysD2, BCTypeLinearRobin_mitLG>::params.bcdata] = bcdata;
  PyDict BCList;
  BCList["BCDict"] = BCDict;
  BCParams::checkInputs(BCList);
  BCClass bc2( pde, BCDict );

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  BOOST_CHECK( bc2.D == 2 );
  BOOST_CHECK( bc2.N == 1 );
  BOOST_CHECK( bc2.NBC == 1 );

  // function tests
  MatrixQ AMtx;
  MatrixQ BMtx;
  ArrayQ bcdataVec;

  Real x = 0;
  Real y = 0;
  Real nx = 0.8;
  Real ny = 0.6;

  AMtx = 0;
  BMtx = 0;
  bc.coefficients( x, y, time, nx, ny, AMtx, BMtx );
  BOOST_CHECK_CLOSE( A, AMtx, tol );
  BOOST_CHECK_CLOSE( B, BMtx, tol );

  bc2.coefficients( x, y, time, nx, ny, AMtx, BMtx );
  BOOST_CHECK_CLOSE( A, AMtx, tol );
  BOOST_CHECK_CLOSE( B, BMtx, tol );

  bcdataVec = 0;
  bc.data( x, y, time, nx, ny, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );

  bc2.data( x, y, time, nx, ny, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeLinearRobin_sansLG_test )
{
  typedef BCBurgers<PhysD2,BCTypeLinearRobin_sansLG> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;
  typedef PDEBurgers2D PDEClass;

  const Real tol = 1.e-13;
  Real time = 0;

  Real v = 0.2;
  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde(v, visc, source);

  Real A = 1;
  Real B = 0.2;
  Real bcdata = 3;

  BCClass bc( A, B, bcdata );

  PyDict BCDict;
  BCDict[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCDict[BCBurgersParams<PhysD2, BCTypeLinearRobin_mitLG>::params.A] = A;
  BCDict[BCBurgersParams<PhysD2, BCTypeLinearRobin_mitLG>::params.B] = B;
  BCDict[BCBurgersParams<PhysD2, BCTypeLinearRobin_mitLG>::params.bcdata] = bcdata;
  PyDict BCList;
  BCList["BCDict"] = BCDict;
  BCParams::checkInputs(BCList);
  BCClass bc2( pde, BCDict );

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  BOOST_CHECK( bc2.D == 2 );
  BOOST_CHECK( bc2.N == 1 );
  BOOST_CHECK( bc2.NBC == 1 );

  // function tests
  MatrixQ AMtx;
  MatrixQ BMtx;
  ArrayQ bcdataVec;

  Real x = 0;
  Real y = 0;
  Real nx = 0.8;
  Real ny = 0.6;

  AMtx = 0;
  BMtx = 0;
  bc.coefficients( x, y, time, nx, ny, AMtx, BMtx );
  BOOST_CHECK_CLOSE( A, AMtx, tol );
  BOOST_CHECK_CLOSE( B, BMtx, tol );

  bc2.coefficients( x, y, time, nx, ny, AMtx, BMtx );
  BOOST_CHECK_CLOSE( A, AMtx, tol );
  BOOST_CHECK_CLOSE( B, BMtx, tol );

  bcdataVec = 0;
  bc.data( x, y, time, nx, ny, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );

  bc2.data( x, y, time, nx, ny, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );
}

#if 0 // This function assumes jacobianFluxAdvective is not a function of q, which is clearly not true for Burgers
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFlux_test )
{
  typedef BCBurgers< PhysD2, BCTypeFlux<PDEBurgers2D> > BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  typedef PDEBurgers2D PDEClass;

  const Real tol = 1.e-13;

  Real v = 0.2;

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;


  Real time = 0;

  PDEClass pde(v, visc, source);

  Real bcdata = 3;

  BCClass bc( pde, bcdata );

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  Real x = 0;
  Real y = 0;
  Real nx = 0.8;
  Real ny = 0.6;

  Real A = nx*u + ny*v;
  Real B = -1;

  MatrixQ AMtx;
  MatrixQ BMtx;
  bc.coefficients( x, y, time, nx, ny, AMtx, BMtx );
  BOOST_CHECK_CLOSE( A, AMtx, tol );
  BOOST_CHECK_CLOSE( B, BMtx, tol );

  ArrayQ bcdataVec;
  bc.data( x, y, time, nx, ny, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFunctionLinearRobin_mitLG_test )
{
  typedef BCBurgers< PhysD2, BCTypeFunctionLinearRobin_mitLG > BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;
  typedef PDEBurgers2D PDEClass;

  const Real tol = 1.e-13;

  typedef ScalarFunction2D_SineSine SolutionType;
  std::shared_ptr<SolutionType> slnExact( new SolutionType );

  Real kxx = 1.003;
  Real kxy = 0.572;
  Real kyx = 0.478;
  Real kyy = 1.452;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  {
    BCClass bc( slnExact, visc );

    Real time = 0;

    // static tests
    BOOST_CHECK( bc.D == 2 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 1 );

    // function tests

    MatrixQ AMtx;
    MatrixQ BMtx;
    ArrayQ bcdataVec;

    Real x = 0.75;
    Real y = 0.25;
    Real nx = 0.8;
    Real ny = 0.6;

    AMtx = 0;
    BMtx = 0;
    bc.coefficients( x, y, time, nx, ny, AMtx, BMtx );
    BOOST_CHECK_CLOSE( 1, AMtx, tol );
    BOOST_CHECK_CLOSE( 0, BMtx, tol );

    bcdataVec = 0;
    bc.data( x, y, time, nx, ny, bcdataVec );
    BOOST_CHECK_CLOSE( -1, bcdataVec, tol );
  }

  {
    Real A = 0.35;
    Real B = 0.84;
    BCClass bc( slnExact, visc, A, B );

    Real time = 0;

    // static tests
    BOOST_CHECK( bc.D == 2 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 1 );

    // function tests

    MatrixQ AMtx;
    MatrixQ BMtx;
    ArrayQ bcdataVec;

    Real x = 0.75;
    Real y = 0.25;
    Real nx = 0.8;
    Real ny = 0.6;

    AMtx = 0;
    BMtx = 0;
    bc.coefficients( x, y, time, nx, ny, AMtx, BMtx );
    BOOST_CHECK_CLOSE( A, AMtx, tol );
    BOOST_CHECK_CLOSE( B, BMtx, tol );

    Real u = sin(2*PI*x)*sin(2*PI*y);
    Real ux = 2*PI*cos(2*PI*x)*sin(2*PI*y);
    Real uy = 2*PI*sin(2*PI*x)*cos(2*PI*y);
    Real bcdataTrue = A*u + B*((kxx*ux + kxy*uy)*nx + (kyx*ux + kyy*uy)*ny);

    bcdataVec = 0;
    bc.data( x, y, time, nx, ny, bcdataVec );
    BOOST_CHECK_CLOSE( bcdataTrue, bcdataVec, tol );
  }

  {

    Real v = 0.2;
    Real kxx = 2.123;
    Real kxy = 0.553;
    Real kyy = 1.007;
    ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

    Source2D_None source;

    PDEClass pde(v, visc, source);

    PyDict SolnDict;
    SolnDict[BCBurgersParams<PhysD2, BCTypeFunctionLinearRobin_mitLG>::params.Function.Name] =
        BCBurgersParams<PhysD2, BCTypeFunctionLinearRobin_mitLG>::params.Function.SineSine;
    Real A = 0.35;
    Real B = 0.84;

    PyDict BCDict;
    BCDict[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
    BCDict[BCBurgersParams<PhysD2, BCTypeFunctionLinearRobin_mitLG>::params.Function] = SolnDict;
    BCDict[BCBurgersParams<PhysD2, BCTypeFunctionLinearRobin_mitLG>::params.A] = A;
    BCDict[BCBurgersParams<PhysD2, BCTypeFunctionLinearRobin_mitLG>::params.B] = B;
    PyDict BCList;
    BCList["BCDict"] = BCDict;
    BCParams::checkInputs(BCList);
    BCClass bc( pde, BCDict );

    Real time = 0;

    // static tests
    BOOST_CHECK( bc.D == 2 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 1 );

    // function tests

    MatrixQ AMtx;
    MatrixQ BMtx;
    ArrayQ bcdataVec;

    Real x = 0.75;
    Real y = 0.25;
    Real nx = 0.8;
    Real ny = 0.6;

    AMtx = 0;
    BMtx = 0;
    bc.coefficients( x, y, time, nx, ny, AMtx, BMtx );
    BOOST_CHECK_CLOSE( A, AMtx, tol );
    BOOST_CHECK_CLOSE( B, BMtx, tol );

    Real u = sin(2*PI*x)*sin(2*PI*y);
    Real ux = 2*PI*cos(2*PI*x)*sin(2*PI*y);
    Real uy = 2*PI*sin(2*PI*x)*cos(2*PI*y);
    Real bcdataTrue = A*u + B*((kxx*ux + kxy*uy)*nx + (kyx*ux + kyy*uy)*ny);

    bcdataVec = 0;
    bc.data( x, y, time, nx, ny, bcdataVec );
    BOOST_CHECK_CLOSE( bcdataTrue, bcdataVec, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFunctionLinearRobin_sansLG_test )
{
  typedef BCBurgers< PhysD2, BCTypeFunctionLinearRobin_sansLG > BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;
  typedef PDEBurgers2D PDEClass;

  const Real tol = 1.e-13;

  typedef ScalarFunction2D_SineSine SolutionType;
  std::shared_ptr<SolutionType> slnExact( new SolutionType );

  Real kxx = 1.003;
  Real kxy = 0.572;
  Real kyx = 0.478;
  Real kyy = 1.452;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  {
    BCClass bc( slnExact, visc );

    Real time = 0;

    // static tests
    BOOST_CHECK( bc.D == 2 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 1 );

    // function tests

    MatrixQ AMtx;
    MatrixQ BMtx;
    ArrayQ bcdataVec;

    Real x = 0.75;
    Real y = 0.25;
    Real nx = 0.8;
    Real ny = 0.6;

    AMtx = 0;
    BMtx = 0;
    bc.coefficients( x, y, time, nx, ny, AMtx, BMtx );
    BOOST_CHECK_CLOSE( 1, AMtx, tol );
    BOOST_CHECK_CLOSE( 0, BMtx, tol );

    bcdataVec = 0;
    bc.data( x, y, time, nx, ny, bcdataVec );
    BOOST_CHECK_CLOSE( -1, bcdataVec, tol );
  }

  {
    Real A = 0.35;
    Real B = 0.84;
    BCClass bc( slnExact, visc, A, B );

    Real time = 0;

    // static tests
    BOOST_CHECK( bc.D == 2 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 1 );

    // function tests

    MatrixQ AMtx;
    MatrixQ BMtx;
    ArrayQ bcdataVec;

    Real x = 0.75;
    Real y = 0.25;
    Real nx = 0.8;
    Real ny = 0.6;

    AMtx = 0;
    BMtx = 0;
    bc.coefficients( x, y, time, nx, ny, AMtx, BMtx );
    BOOST_CHECK_CLOSE( A, AMtx, tol );
    BOOST_CHECK_CLOSE( B, BMtx, tol );

    Real u = sin(2*PI*x)*sin(2*PI*y);
    Real ux = 2*PI*cos(2*PI*x)*sin(2*PI*y);
    Real uy = 2*PI*sin(2*PI*x)*cos(2*PI*y);
    Real bcdataTrue = A*u + B*((kxx*ux + kxy*uy)*nx + (kyx*ux + kyy*uy)*ny);

    bcdataVec = 0;
    bc.data( x, y, time, nx, ny, bcdataVec );
    BOOST_CHECK_CLOSE( bcdataTrue, bcdataVec, tol );
  }

  {

    Real v = 0.2;
    Real kxx = 2.123;
    Real kxy = 0.553;
    Real kyy = 1.007;
    ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

    Source2D_None source;

    PDEClass pde(v, visc, source);

    PyDict SolnDict;
    SolnDict[BCBurgersParams<PhysD2, BCTypeFunctionLinearRobin_sansLG>::params.Function.Name] =
        BCBurgersParams<PhysD2, BCTypeFunctionLinearRobin_sansLG>::params.Function.SineSine;
    Real A = 0.35;
    Real B = 0.84;

    PyDict BCDict;
    BCDict[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
    BCDict[BCBurgersParams<PhysD2, BCTypeFunctionLinearRobin_sansLG>::params.Function] = SolnDict;
    BCDict[BCBurgersParams<PhysD2, BCTypeFunctionLinearRobin_sansLG>::params.A] = A;
    BCDict[BCBurgersParams<PhysD2, BCTypeFunctionLinearRobin_sansLG>::params.B] = B;
    PyDict BCList;
    BCList["BCDict"] = BCDict;
    BCParams::checkInputs(BCList);
    BCClass bc( pde, BCDict );

    Real time = 0;

    // static tests
    BOOST_CHECK( bc.D == 2 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 1 );

    // function tests

    MatrixQ AMtx;
    MatrixQ BMtx;
    ArrayQ bcdataVec;

    Real x = 0.75;
    Real y = 0.25;
    Real nx = 0.8;
    Real ny = 0.6;

    AMtx = 0;
    BMtx = 0;
    bc.coefficients( x, y, time, nx, ny, AMtx, BMtx );
    BOOST_CHECK_CLOSE( A, AMtx, tol );
    BOOST_CHECK_CLOSE( B, BMtx, tol );

    Real u = sin(2*PI*x)*sin(2*PI*y);
    Real ux = 2*PI*cos(2*PI*x)*sin(2*PI*y);
    Real uy = 2*PI*sin(2*PI*x)*cos(2*PI*y);
    Real bcdataTrue = A*u + B*((kxx*ux + kxy*uy)*nx + (kyx*ux + kyy*uy)*ny);

    bcdataVec = 0;
    bc.data( x, y, time, nx, ny, bcdataVec );
    BOOST_CHECK_CLOSE( bcdataTrue, bcdataVec, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeDirichlet_mitState_test )
{
  typedef BCBurgers< PhysD2, BCTypeDirichlet_mitState<BurgersConservative2D,ViscousFlux2D_Uniform> > BCClass;
  typedef PDEBurgers2D PDEClass;

  const Real tol = 1.e-13;

  Real v = 0.2;
  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde(v, visc, source);

  {
    Real qBTrue = 3.0;
    BCClass bc(pde, qBTrue );

    Real time = 0;

    // static tests
    BOOST_CHECK( bc.D == 2 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 1 );

    // function tests
    Real x = 0.75;
    Real y = 0.25;
    Real nx = 0.8;
    Real ny = 0.6;
    Real qB = 0.0;
    Real qI = qBTrue;

    BOOST_CHECK( bc.isValidState(nx, ny, qI) == true );

    bc.state( x, y, time, nx, ny, qI, qB );
    BOOST_CHECK_CLOSE( qBTrue, qB, tol );
  }

  {
    Real qBTrue = 3.0;
    PyDict BCDict;
    BCDict[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
    BCDict[BCBurgersParams<PhysD2, BCTypeDirichlet_mitStateParam>::params.qB] = qBTrue;
    PyDict BCList;
    BCList["BCDict"] = BCDict;
    BCParams::checkInputs(BCList);
    BCClass bc( pde, BCDict );

    Real time = 0;

    // static tests
    BOOST_CHECK( bc.D == 2 );
    BOOST_CHECK( bc.N == 1 );
    BOOST_CHECK( bc.NBC == 1 );

    // function tests
    Real x = 0.75;
    Real y = 0.25;
    Real nx = 0.8;
    Real ny = 0.6;
    Real qB = 0.0;
    Real qI = qBTrue;

    BOOST_CHECK( bc.isValidState(nx, ny, qI) == true );

    bc.state( x, y, time, nx, ny, qI, qB );
    BOOST_CHECK_CLOSE( qBTrue, qB, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCParameters_test )
{
  BOOST_CHECK_EQUAL( "None"              , BCParams::params.BC.None );
  BOOST_CHECK_EQUAL( "LinearRobin_mitLG" , BCParams::params.BC.LinearRobin_mitLG );
  BOOST_CHECK_EQUAL( "LinearRobin_sansLG", BCParams::params.BC.LinearRobin_sansLG );
  BOOST_CHECK_EQUAL( "Dirichlet_mitState"         , BCParams::params.BC.Dirichlet_mitState);
  BOOST_CHECK_EQUAL( "FunctionLinearRobin_mitLG"    , BCParams::params.BC.FunctionLinearRobin_mitLG);
  BOOST_CHECK_EQUAL( "FunctionLinearRobin_sansLG"   , BCParams::params.BC.FunctionLinearRobin_sansLG );

  PyDict BC0;
  BC0[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BC3;
  BC3[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BC3[BCBurgersParams<PhysD2, BCTypeLinearRobin_mitLG>::params.A] = 2;
  BC3[BCBurgersParams<PhysD2, BCTypeLinearRobin_mitLG>::params.B] = 3;
  BC3[BCBurgersParams<PhysD2, BCTypeLinearRobin_mitLG>::params.bcdata] = 4;

  PyDict BCList;
  BCList["BC0"] = BC0;
  BCList["BC3"] = BC3;

  //No exceptions should be thrown
  BCParams::checkInputs(BCList);

  // Create a pde for the BC constructors
  Real v = 0.2;

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEBurgers2D pde(v, visc, source);

  // Create all the BC's
  std::map< std::string, std::shared_ptr<BCBase> > BCs = BCParams::createBCs<BCNDConvertSpace>(pde, BCList);

  BOOST_CHECK_EQUAL( 2, BCs.size() );

  // The python dictionary does not preserve order, so the count of each BC should be 1
  int NoneCount = 0;
  int LinearRobinCount = 0;

  // Extract the keys from the dictionary
  std::vector<std::string> keys = BCList.stringKeys();

  for ( std::size_t i = 0; i < keys.size(); i++ )
  {
    if ( BCs[keys[i]]->derivedTypeID() == typeid(BCNDConvertSpace<PhysD2,BCNone<PhysD2,AdvectionDiffusionTraits<PhysD2>::N>>) )
      NoneCount++;
    else if ( BCs[keys[i]]->derivedTypeID() == typeid(BCNDConvertSpace<PhysD2,BCBurgers<PhysD2,BCTypeLinearRobin_mitLG>>) )
      LinearRobinCount++;
    else
      BOOST_CHECK( false ); // Failure..
  }

  BOOST_CHECK_EQUAL( 1, NoneCount );
  BOOST_CHECK_EQUAL( 1, LinearRobinCount );

  std::map< std::string, std::vector<int> > BoundaryGroups;

  BoundaryGroups["BC0"] = {0};
  BoundaryGroups["BC3"] = {4};

  std::vector<int> boundaryGroupsLG = BCParams::getLGBoundaryGroups(BCList, BoundaryGroups);

  BOOST_REQUIRE_EQUAL( 1, boundaryGroupsLG.size() );
  BOOST_CHECK_EQUAL( 4, boundaryGroupsLG[0] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/BCBurgers2D_pattern.txt", true );

  typedef PDEBurgers2D PDEClass;

  Real v = 0.2;
  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde(v, visc, source);

  {
  typedef BCBurgers<PhysD2,BCTypeLinearRobin_mitLG> BCClass;

  Real A = 1;
  Real B = 0.2;
  Real bcdata = 3;

  BCClass bc1( A, B, bcdata );
  bc1.dump( 2, output );
  }

  {
  typedef BCBurgers<PhysD2, BCTypeDirichlet_mitState<BurgersConservative2D,ViscousFlux2D_Uniform>> BCClass;
  Real qB = 3;

  BCClass bc1(pde, qB );
  bc1.dump( 2, output );
  }

#if 0 // This function assumes jacobianFluxAdvective is not a function of q, which is clearly not true for Burgers
  {
  typedef BCBurgers< PhysD2, BCTypeFlux<PDEBurgers2D> > BCClass;

  Real v = 0.2;

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEBurgers2D pde( v, visc, source );

  Real bcdata = 3;

  BCClass bc2( pde, bcdata );
  bc2.dump( 2, output );
  }
#endif

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
