// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AdvectieFlux2D_btest
//
// test of 2-D advection flux class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Traits.h"
#include "pde/Sensor/Sensor_AdvectiveFlux2D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Sensor_AdvectiveFlux2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( None )
{
  typedef AdvectionDiffusionTraits<PhysD2>::ArrayQ<Real> PDEArrayQ;
  typedef Sensor_AdvectiveFlux2D_None::ArrayQ<Real> ArrayQ;
  typedef Sensor_AdvectiveFlux2D_None::MatrixQ<Real> MatrixQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;

  Real nx = 1.0;
  Real ny = 0.25;
  Real nt = 0.5;
  Real x = 1;
  Real y = 2;
  Real time = 0;

  Sensor_AdvectiveFlux2D_None adv;

  ArrayQ q = 1.45;
  ArrayQ qL = 2.45;
  ArrayQ qR = 3.45;
  ArrayQ qx = 1.56;
  ArrayQ qy = 2.56;
  ArrayQ fxTrue = 1.234;
  ArrayQ fx = fxTrue;
  ArrayQ fyTrue = 2.234;
  ArrayQ fy = fyTrue;
  MatrixQ axTrue = 3.45;
  MatrixQ ax = axTrue;
  MatrixQ ayTrue = 5.45;
  MatrixQ ay = ayTrue;
  PDEArrayQ p = 0.0;
  ParamType param(0.1, p);

  BOOST_CHECK( adv.hasFluxAdvective() == false );

  adv.flux(param, x, y, time, q, fx, fy);
  BOOST_CHECK_EQUAL( fxTrue, fx );
  BOOST_CHECK_EQUAL( fyTrue, fy );

  adv.fluxUpwind(param, x, y, time, qL, qR, nx, ny, fx);
  BOOST_CHECK_EQUAL( fxTrue, fx );

  adv.fluxUpwindSpaceTime(param, x, y, time, qL, qR, nx, ny, nt, fx);
  BOOST_CHECK_EQUAL( fxTrue + qL*nt, fx );

  adv.jacobian(param, x, y, time, q, ax, ay);
  BOOST_CHECK_EQUAL( axTrue, ax );
  BOOST_CHECK_EQUAL( ayTrue, ay );

  ArrayQ strongPDETrue = 5.78;
  ArrayQ strongPDE = strongPDETrue;
  adv.strongFlux(param, x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_EQUAL( strongPDETrue, strongPDE );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform_hasFluxAdvective )
{
  Real u =  1.33479;
  Real v = -0.33886;

  Sensor_AdvectiveFlux2D_Uniform adv(u, v);

  BOOST_CHECK( adv.hasFluxAdvective() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform_flux )
{
  typedef AdvectionDiffusionTraits<PhysD2>::ArrayQ<Real> PDEArrayQ;
  typedef Sensor_AdvectiveFlux2D_Uniform::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;
  const Real tol = 1.e-13;

  ArrayQ q = 2.34;
  Real u =  1.33479;
  Real v = -0.33886;
  ArrayQ fxTrue = u*q;
  ArrayQ fyTrue = v*q;
  ArrayQ fx = 0, fy = 0;
  PDEArrayQ p = 0.0;
  ParamType param(0.1, p);

  Sensor_AdvectiveFlux2D_Uniform adv(u, v);

  // functor
  Real x = 1;
  Real y = 2;
  Real time = 0;
  adv.flux(param, x, y, time, q, fx, fy);
  BOOST_CHECK_CLOSE( fxTrue, fx, tol );
  BOOST_CHECK_CLOSE( fyTrue, fy, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform_jacobian )
{
  typedef AdvectionDiffusionTraits<PhysD2>::ArrayQ<Real> PDEArrayQ;
  typedef Sensor_AdvectiveFlux2D_Uniform::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;
  const Real tol = 1.e-13;

  Real x = 1;
  Real y = 2;
  Real time = 0;

  ArrayQ q = 2.34;
  Real u =  1.33479;
  Real v = -0.33886;
  ArrayQ jacxTrue = u;
  ArrayQ jacyTrue = v;
  ArrayQ jacx = 0, jacy = 0;
  PDEArrayQ p = 0.0;
  ParamType param(0.1, p);

  Sensor_AdvectiveFlux2D_Uniform adv(u, v);

  // jacobian
  adv.jacobian(param, x, y, time, q, jacx, jacy);
  BOOST_CHECK_CLOSE( jacxTrue, jacx, tol );
  BOOST_CHECK_CLOSE( jacyTrue, jacy, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform_fluxUpwind )
{
  typedef AdvectionDiffusionTraits<PhysD2>::ArrayQ<Real> PDEArrayQ;
  typedef Sensor_AdvectiveFlux2D_Uniform::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;
  const Real tol = 1.e-13;

  Real x = 1;
  Real y = 2;
  Real time = 0;

  Real u =  1.33479;
  Real v = -0.33886;

  Sensor_AdvectiveFlux2D_Uniform adv(u, v);

  Real nx = 1.1;
  Real ny = 0.25;
  ArrayQ qL = u;
  ArrayQ qR = 0;
  ArrayQ f = 0;
  Real fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);
  PDEArrayQ p = 0.0;
  ParamType param(0.1, p);

  // flux
  f = 0;
  adv.fluxUpwind(param, x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  qL = 0;
  qR = u;
  fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);
  // flux
  f = 0;
  adv.fluxUpwind(param, x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  nx = -1.1;
  ny = 0.25;
  qL = u;
  qR = 0;
  fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);
  // flux
  f = 0;
  adv.fluxUpwind(param, x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  nx = -1.1;
  ny = 0.25;
  qL = 0;
  qR = u;
  fTrue = 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*fabs(nx*u + ny*v)*(qR - qL);
  // flux
  f = 0;
  adv.fluxUpwind(param, x, y, time, qL, qR, nx, ny, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform_fluxUpwindSpaceTime )
{
  typedef AdvectionDiffusionTraits<PhysD2>::ArrayQ<Real> PDEArrayQ;
  typedef Sensor_AdvectiveFlux2D_Uniform::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;
  const Real tol = 1.e-13;

  Real x = 1;
  Real y = 2;
  Real time = 0;

  Real u =  1.33479;
  Real v = -0.33886;

  Sensor_AdvectiveFlux2D_Uniform adv(u, v);

  Real nx = 1.1;
  Real ny = 0.25;
  Real nt = 0.65;
  ArrayQ qL = u;
  ArrayQ qR = 0;
  ArrayQ f = 0;
  Real fTrue = 0.5*(nt*1 + nx*u + ny*v)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v)*(qR - qL);
  PDEArrayQ p = 0.0;
  ParamType param(0.1, p);

  // flux
  f = 0;
  adv.fluxUpwindSpaceTime(param, x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  nx = 1.0;
  qL = 0;
  qR = u;
  fTrue = 0.5*(nt*1 + nx*u + ny*v)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v)*(qR - qL);
  // flux
  f = 0;
  adv.fluxUpwindSpaceTime(param, x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  nx = -1.1;
  qL = u;
  qR = 0;
  fTrue = 0.5*(nt*1 + nx*u + ny*v)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v)*(qR - qL);
  // flux
  f = 0;
  adv.fluxUpwindSpaceTime(param, x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  nx = -1.1;
  qL = 0;
  qR = u;
  fTrue = 0.5*(nt*1 + nx*u + ny*v)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v)*(qR - qL);
  // flux
  f = 0;
  adv.fluxUpwindSpaceTime(param, x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  nx = -1.1;
  ny = -0.5;
  nt = -0.1;
  qL = 0;
  qR = u;
  fTrue = 0.5*(nt*1 + nx*u + ny*v)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v)*(qR - qL);
  // flux
  f = 0;
  adv.fluxUpwindSpaceTime(param, x, y, time, qL, qR, nx, ny, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform_strongFlux )
{
  typedef AdvectionDiffusionTraits<PhysD2>::ArrayQ<Real> PDEArrayQ;
  typedef Sensor_AdvectiveFlux2D_Uniform::ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;
  const Real tol = 1.e-13;

  Real x = 1;
  Real y = 2;
  Real time = 0;

  Real u =  1.33479;
  Real v = -0.33886;

  ArrayQ q = u;
  ArrayQ qx = 1.56;
  ArrayQ qy = 2.56;
  Real strongPDETrue = u*qx + v*qy;
  PDEArrayQ p = 0.0;
  ParamType param(0.1, p);

  Sensor_AdvectiveFlux2D_Uniform adv(u, v);

  // strong flux
  ArrayQ strongPDE = 0;
  adv.strongFlux(param, x, y, time, q, qx, qy, strongPDE);
  BOOST_CHECK_CLOSE( strongPDETrue, strongPDE, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/Sensor/PDESensorParameter2D_Sensor_AdvectiveFlux2D_pattern.txt", true );

  Sensor_AdvectiveFlux2D_None adv0;
  adv0.dump( 2, output );

  Real u =  1.33479;
  Real v = -0.33886;
  Sensor_AdvectiveFlux2D_Uniform adv1(u, v);
  adv1.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
