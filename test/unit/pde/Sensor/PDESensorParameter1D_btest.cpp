// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDESensorParameter1D_btest
//
// test of 1-D Advection-Diffusion Sensor class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Traits.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/AdvectionDiffusion_Sensor.h"

#include "pde/Sensor/PDESensorParameter1D.h"
#include "pde/Sensor/Source1D_JumpSensor.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}


using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDESensorParameter1D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDESensorParameter<PhysD1,
                             AdvectionDiffusionTraits<PhysD1>,
                             Sensor_AdvectiveFlux1D_None,
                             Sensor_ViscousFlux1D_None,
                             Sensor_Source1D_None> PDESensorParameter1D;
  typedef PDESensorParameter1D SensorClass;

  BOOST_CHECK( SensorClass::D == 1 );
  BOOST_CHECK( SensorClass::N == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef PDESensorParameter<PhysD1,
                             AdvectionDiffusionTraits<PhysD1>,
                             Sensor_AdvectiveFlux1D_None,
                             Sensor_ViscousFlux1D_None,
                             Sensor_Source1D_None> PDESensorParameter1D;
  typedef PDESensorParameter1D SensorClass;

  Sensor_AdvectiveFlux1D_None adv;
  Sensor_ViscousFlux1D_None visc;

  Sensor_Source1D_None source;

  SensorClass sensor1(adv, visc, source);

  BOOST_CHECK( sensor1.D == 1 );
  BOOST_CHECK( sensor1.N == 1 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux )
{
  typedef PDESensorParameter<PhysD1,
                             AdvectionDiffusionTraits<PhysD1>,
                             Sensor_AdvectiveFlux1D_Uniform,
                             Sensor_ViscousFlux1D_Uniform,
                             Sensor_Source1D_None> PDESensorParameter1D;
  typedef PDESensorParameter1D SensorClass;
  typedef SensorClass::ArrayQ<Real> ArrayQ;
  typedef SensorClass::MatrixQ<Real> MatrixQ;
  typedef AdvectionDiffusionTraits<PhysD1>::ArrayQ<Real> PDEArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;

  const Real tol = 1.e-13;

  Real u = 1.1;
  Sensor_AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  Sensor_ViscousFlux1D_Uniform visc(kxx);

  Sensor_Source1D_None src;

  SensorClass sensor(adv, visc, src);

  // static tests
  BOOST_CHECK( sensor.D == 1 );
  BOOST_CHECK( sensor.N == 1 );

  // flux components
  BOOST_CHECK( sensor.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( sensor.hasFluxAdvective() == true );
  BOOST_CHECK( sensor.hasFluxViscous() == true );
  BOOST_CHECK( sensor.hasSource() == false );
  BOOST_CHECK( sensor.hasForcingFunction() == false );

  BOOST_CHECK( sensor.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( sensor.needsSolutionGradientforSource() == false );

  // function tests
  Real time;
  Real x;
  Real sln, slnx;

  x = 0;
  time = 0;
  sln  =  3.263;
  slnx = -0.445;
  PDEArrayQ p = 0.0;
  ParamType paramL(0.1, p);

  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = sln;
  ArrayQ q;
  sensor.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  // master state
  ArrayQ uCons = 0;
  sensor.masterState( paramL, x, time, q, uCons );
  BOOST_CHECK_CLOSE( qTrue, uCons, tol );

  MatrixQ dudq = 0;
  sensor.jacobianMasterState( paramL, x, time,
                                   q, dudq );
  BOOST_CHECK_CLOSE( 1, dudq, tol );

  // unsteady flux
  ArrayQ ft = 0;
  sensor.masterState( paramL, x, time, q, ft );
  BOOST_CHECK_CLOSE( qTrue, ft, tol );

  // advective flux
  ArrayQ fTrue = u*sln;
  ArrayQ f;
  DLA::VectorS<1,ArrayQ> F;
  f = 0;
  sensor.fluxAdvective( paramL, x, time, q, f );
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  // advective flux jacobian
  MatrixQ dfdu = 0;
  sensor.jacobianFluxAdvective( paramL, x, time, q, dfdu );
  BOOST_CHECK_CLOSE( u, dfdu, tol );

  // set gradient
  ArrayQ qx = slnx;

  // diffusive flux
  fTrue = -(kxx*slnx);
  f = 0;
  sensor.fluxViscous( paramL, x, time, q, qx, f );
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  // diffusive flux jacobian
  MatrixQ kxxMtx = 0;
  sensor.diffusionViscous( paramL, x, time, q, qx, kxxMtx );
  BOOST_CHECK_CLOSE( kxx, kxxMtx, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxNormal )
{
  typedef PDESensorParameter<PhysD1,
                             AdvectionDiffusionTraits<PhysD1>,
                             Sensor_AdvectiveFlux1D_Uniform,
                             Sensor_ViscousFlux1D_Uniform,
                             Sensor_Source1D_None> PDESensorParameter1D;
  typedef PDESensorParameter1D SensorClass;
  typedef SensorClass::ArrayQ<Real> ArrayQ;
  typedef AdvectionDiffusionTraits<PhysD1>::ArrayQ<Real> PDEArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;

  const Real tol = 1.e-13;

  Real u = 1.1;
  Sensor_AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  Sensor_ViscousFlux1D_Uniform visc(kxx);

  Sensor_Source1D_None src;

  SensorClass sensor(adv, visc, src);

  // advective flux function
  Real time;
  Real x;
  Real nx;
  Real slnL, slnR;
  PDEArrayQ p = 0.0;
  ParamType paramL(0.1, p);
  ParamType paramR(0.1, 0.5*p);

  x = 0;  // not actually used in functions
  time = 0;
  nx = 1.22;
  slnL = 3.26; slnR = 1.79;

  // set
  ArrayQ qL = slnL;
  ArrayQ qR = slnR;

  // advective normal flux
  ArrayQ fnTrue = (nx*u)*slnL;
  ArrayQ fn;
  fn = 0;
  sensor.fluxAdvectiveUpwind( paramL, x, time, qL, qR, nx, fn );
  BOOST_CHECK_CLOSE( fnTrue, fn, tol );

  // viscous flux function

  Real slnxL;
  Real slnxR;

  slnxL = 1.325;
  slnxR = 0.327;

  // set gradient
  ArrayQ qxL = slnxL;
  ArrayQ qxR = slnxR;

  // viscous normal flux
  fnTrue = -0.5*((nx*kxx)*(slnxL + slnxR));
  fn = 0;
  sensor.fluxViscous( paramL, paramR, x, time, qL, qxL, qR, qxR, nx, fn );
  BOOST_CHECK_CLOSE( fnTrue, fn, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic )
{
  typedef PDESensorParameter<PhysD1,
                             AdvectionDiffusionTraits<PhysD1>,
                             Sensor_AdvectiveFlux1D_Uniform,
                             Sensor_ViscousFlux1D_Uniform,
                             Sensor_Source1D_None> PDESensorParameter1D;
  typedef PDESensorParameter1D SensorClass;
  typedef SensorClass::ArrayQ<Real> ArrayQ;
  typedef AdvectionDiffusionTraits<PhysD1>::ArrayQ<Real> PDEArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;

  const Real tol = 1.e-13;

  Real u = 1.1;
  Sensor_AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  Sensor_ViscousFlux1D_Uniform visc(kxx);

  Sensor_Source1D_None src;

  SensorClass sensor(adv, visc, src);

  Real x;
  Real dx;
  Real sln;
  Real speed, speedTrue;
  Real time = 0;

  x = 0;  // not actually used in functions
  dx = 1.22;
  sln  =  3.263;
  PDEArrayQ p = 0.0;
  ParamType paramL(0.1, p);

  // set
  ArrayQ q = sln;

  speedTrue = fabs(dx*u)/sqrt(dx*dx);

  sensor.speedCharacteristic( paramL, x, time, dx, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  sensor.speedCharacteristic( paramL, x, time, q, speed );
  BOOST_CHECK_CLOSE( fabs(u), speed, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef PDESensorParameter<PhysD1,
                             AdvectionDiffusionTraits<PhysD1>,
                             Sensor_AdvectiveFlux1D_Uniform,
                             Sensor_ViscousFlux1D_Uniform,
                             Sensor_Source1D_None> PDESensorParameter1D;
  typedef PDESensorParameter1D SensorClass;
  typedef SensorClass::ArrayQ<Real> ArrayQ;

  Real u = 1.1;
  Sensor_AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  Sensor_ViscousFlux1D_Uniform visc(kxx);

  Sensor_Source1D_None src;

  SensorClass sensor(adv, visc, src);

  ArrayQ q = 0.1;
  BOOST_CHECK( sensor.isValidState(q) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/Sensor/PDESensorParameter1D_pattern.txt", true );

  typedef PDESensorParameter<PhysD1,
                             AdvectionDiffusionTraits<PhysD1>,
                             Sensor_AdvectiveFlux1D_None,
                             Sensor_ViscousFlux1D_None,
                             Sensor_Source1D_None> PDESensorParameter1D;

  typedef PDESensorParameter1D SensorClass;

  Sensor_AdvectiveFlux1D_None adv;
  Sensor_ViscousFlux1D_None visc;
  Sensor_Source1D_None source;

  SensorClass sensor1(adv, visc, source);
  sensor1.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
