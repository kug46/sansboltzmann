// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDESensorParameter2D_btest
//
// test of 2-D Advection-Diffusion Sensor class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Traits.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/AdvectionDiffusion_Sensor.h"

#include "pde/Sensor/PDESensorParameter2D.h"
#include "pde/Sensor/Source2D_JumpSensor.h"


using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}


using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDESensorParameter2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDESensorParameter<PhysD2,
                             AdvectionDiffusionTraits<PhysD2>,
                             Sensor_AdvectiveFlux2D_None,
                             Sensor_ViscousFlux2D_None,
                             Sensor_Source2D_None> PDESensorParameter2D;
  typedef PDESensorParameter2D SensorClass;

  BOOST_CHECK( SensorClass::D == 2 );
  BOOST_CHECK( SensorClass::N == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef PDESensorParameter<PhysD2,
                             AdvectionDiffusionTraits<PhysD2>,
                             Sensor_AdvectiveFlux2D_None,
                             Sensor_ViscousFlux2D_None,
                             Sensor_Source2D_None> PDESensorParameter2D;
  typedef PDESensorParameter2D SensorClass;

  Sensor_AdvectiveFlux2D_None adv;
  Sensor_ViscousFlux2D_None visc;

  Sensor_Source2D_None source;

  SensorClass sensor1(adv, visc, source);

  BOOST_CHECK( sensor1.D == 2 );
  BOOST_CHECK( sensor1.N == 1 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux )
{
  typedef PDESensorParameter<PhysD2,
                             AdvectionDiffusionTraits<PhysD2>,
                             Sensor_AdvectiveFlux2D_Uniform,
                             Sensor_ViscousFlux2D_Uniform,
                             Sensor_Source2D_None> PDESensorParameter2D;
  typedef PDESensorParameter2D SensorClass;
  typedef SensorClass::ArrayQ<Real> ArrayQ;
  typedef SensorClass::MatrixQ<Real> MatrixQ;
  typedef AdvectionDiffusionTraits<PhysD2>::ArrayQ<Real> PDEArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;

  const Real tol = 1.e-13;

  Real u = 1;
  Real v = 0.2;
  Sensor_AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Sensor_ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Sensor_Source2D_None src;

  SensorClass sensor(adv, visc, src );

  // static tests
  BOOST_CHECK( sensor.D == 2 );
  BOOST_CHECK( sensor.N == 1 );

  // flux components
  BOOST_CHECK( sensor.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( sensor.hasFluxAdvective() == true );
  BOOST_CHECK( sensor.hasFluxViscous() == true );
  BOOST_CHECK( sensor.hasSource() == false );
  BOOST_CHECK( sensor.hasForcingFunction() == false );

  BOOST_CHECK( sensor.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( sensor.needsSolutionGradientforSource() == false );

  // function tests

  Real x, y, time;
  Real sln, slnx, slny;

  x = 0; y = 0, time = 0;
  sln  =  3.263;
  slnx = -0.445;
  slny =  1.741;
  PDEArrayQ p = 0.0;
  ParamType paramL(0.1, p);

  DLA::VectorS<2,Real> X = {x,y};

  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = qDataPrim[0];
  ArrayQ q;
  sensor.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  // master state
  ArrayQ uCons = 0;
  sensor.masterState( paramL, x, y, time, q, uCons );
  BOOST_CHECK_CLOSE( qTrue, uCons, tol );

  MatrixQ dudq = 0;
  sensor.jacobianMasterState( paramL, x, y, time, q, dudq );
  BOOST_CHECK_CLOSE( 1, dudq, tol );

  // unsteady flux
  ArrayQ ft = 0;
  sensor.fluxAdvectiveTime( paramL, x, y, time, q, ft );
  BOOST_CHECK_CLOSE( qTrue, ft, tol );

  // advective flux
  Real fData[1] = {u*sln};
  Real gData[1] = {v*sln};
  ArrayQ fTrue = fData[0];
  ArrayQ gTrue = gData[0];
  ArrayQ f, g;
  DLA::VectorS<2,ArrayQ> F;
  f = 0;
  g = 0;
  sensor.fluxAdvective( paramL, x, y, time, q, f, g );
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( gTrue, g, tol );


  // advective flux jacobian
  MatrixQ dfdu = 0;
  MatrixQ dgdu = 0;
  sensor.jacobianFluxAdvective( paramL, x, y, time, q, dfdu, dgdu );
  BOOST_CHECK_CLOSE( u, dfdu, tol );
  BOOST_CHECK_CLOSE( v, dgdu, tol );

  // set gradient
  qDataPrim[0] = slnx;
  ArrayQ qxTrue = qDataPrim[0];
  ArrayQ qx;
  sensor.setDOFFrom( qx, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxTrue, qx, tol );

  qDataPrim[0] = slny;
  ArrayQ qyTrue = qDataPrim[0];
  ArrayQ qy;
  sensor.setDOFFrom( qy, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyTrue, qy, tol );

  DLA::VectorS<2, ArrayQ> gradq = {qx, qy};

  // diffusive flux
  fTrue = -(kxx*slnx + kxy*slny);
  gTrue = -(kxy*slnx + kyy*slny);
  f = 0;
  g = 0;
  sensor.fluxViscous( paramL, x, y, time, q, qx, qy, f, g );
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( gTrue, g, tol );

  // diffusive flux jacobian
  MatrixQ kxxMtx = 0, kxyMtx = 0, kyxMtx = 0, kyyMtx = 0;
  sensor.diffusionViscous( paramL, x, y, time, q, qx, qy, kxxMtx, kxyMtx, kyxMtx, kyyMtx );
  BOOST_CHECK_CLOSE( kxx, kxxMtx, tol );
  BOOST_CHECK_CLOSE( kxy, kxyMtx, tol );
  BOOST_CHECK_CLOSE( kxy, kyxMtx, tol );
  BOOST_CHECK_CLOSE( kyy, kyyMtx, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxRoe )
{
  typedef PDESensorParameter<PhysD2,
                             AdvectionDiffusionTraits<PhysD2>,
                             Sensor_AdvectiveFlux2D_Uniform,
                             Sensor_ViscousFlux2D_Uniform,
                             Sensor_Source2D_None> PDESensorParameter2D;
  typedef PDESensorParameter2D SensorClass;
  typedef SensorClass::ArrayQ<Real> ArrayQ;
  typedef AdvectionDiffusionTraits<PhysD2>::ArrayQ<Real> PDEArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;

  const Real tol = 1.e-13;

  Real u = 1;
  Real v = 0.2;
  Sensor_AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Sensor_ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Sensor_Source2D_None src;

  SensorClass sensor(adv, visc, src );

  // advective flux function

  Real x, y, time;
  Real nx, ny;
  Real slnL, slnR;

  x = 0; y = 0, time = 0;   // not actually used in functions
  nx = 1.22; ny = -0.432;
  slnL = 3.26; slnR = 1.79;
  PDEArrayQ p = 0.0;
  ParamType paramL(0.1, p);
  ParamType paramR(0.1, 0.5*p);

  DLA::VectorS<2, Real> X = {x, y};
  DLA::VectorS<2, Real> N = {nx, ny};

  // set
  Real qLDataPrim[1] = {slnL};
  string qLNamePrim[1] = {"Solution"};
  ArrayQ qLTrue = qLDataPrim[0];
  ArrayQ qL;
  sensor.setDOFFrom( qL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qLTrue, qL, tol );

  Real qRDataPrim[1] = {slnR};
  string qRNamePrim[1] = {"Solution"};
  ArrayQ qRTrue = qRDataPrim[0];
  ArrayQ qR;
  sensor.setDOFFrom( qR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qRTrue, qR, tol );

  // advective normal flux
  Real fnData[1] = {(nx*u + ny*v)*slnL};
  ArrayQ fnTrue = fnData[0];
  ArrayQ fn;
  fn = 0;
  sensor.fluxAdvectiveUpwind( paramL, x, y, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fnTrue, fn, tol );

  // viscous flux function

  Real slnxL, slnyL;
  Real slnxR, slnyR;

  slnxL = 1.325;  slnyL = -0.457;
  slnxR = 0.327;  slnyR =  3.421;

  // set gradient
  qLDataPrim[0] = slnxL;
  ArrayQ qxLTrue = qLDataPrim[0];
  ArrayQ qxL;
  sensor.setDOFFrom( qxL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxLTrue, qxL, tol );

  qLDataPrim[0] = slnyL;
  ArrayQ qyLTrue = qLDataPrim[0];
  ArrayQ qyL;
  sensor.setDOFFrom( qyL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyLTrue, qyL, tol );

  DLA::VectorS<2, ArrayQ> gradqL = {qxL, qyL};

  qRDataPrim[0] = slnxR;
  ArrayQ qxRTrue = qRDataPrim[0];
  ArrayQ qxR;
  sensor.setDOFFrom( qxR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxRTrue, qxR, tol );

  qRDataPrim[0] = slnyR;
  ArrayQ qyRTrue = qRDataPrim[0];
  ArrayQ qyR;
  sensor.setDOFFrom( qyR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyRTrue, qyR, tol );

  DLA::VectorS<2, ArrayQ> gradqR = {qxR, qyR};

  // viscous normal flux
  fnData[0] = -0.5*((nx*kxx + kxy*ny)*(slnxL + slnxR) + (nx*kxy + ny*kyy)*(slnyL + slnyR));
  fn = 0;
  sensor.fluxViscous( paramL, paramR, x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fnData[0], fn, tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic )
{
  typedef PDESensorParameter<PhysD2,
                             AdvectionDiffusionTraits<PhysD2>,
                             Sensor_AdvectiveFlux2D_Uniform,
                             Sensor_ViscousFlux2D_Uniform,
                             Sensor_Source2D_None> PDESensorParameter2D;
  typedef PDESensorParameter2D SensorClass;
  typedef SensorClass::ArrayQ<Real> ArrayQ;
  typedef AdvectionDiffusionTraits<PhysD2>::ArrayQ<Real> PDEArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;

  const Real tol = 1.e-13;

  Real u = 1;
  Real v = 0.2;
  Sensor_AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Sensor_ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Sensor_Source2D_None src;

  SensorClass sensor(adv, visc, src );

  Real time;
  Real x, y;
  Real dx, dy;
  Real sln;
  Real speed, speedTrue;

  x = 0; y = 0; time = 0;  // not actually used in functions
  dx = 1.22; dy = -0.432;
  sln  =  3.263;
  PDEArrayQ p = 0.0;
  ParamType paramL(0.1, p);

  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = qDataPrim[0];
  ArrayQ q;
  sensor.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  speedTrue = fabs(dx*u + dy*v)/sqrt(dx*dx + dy*dy);

  sensor.speedCharacteristic( paramL, x, y, time, dx, dy, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  sensor.speedCharacteristic( paramL, x, y, time, q, speed );
  BOOST_CHECK_CLOSE( sqrt(u*u + v*v), speed, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef PDESensorParameter<PhysD2,
                             AdvectionDiffusionTraits<PhysD2>,
                             Sensor_AdvectiveFlux2D_None,
                             Sensor_ViscousFlux2D_None,
                             Sensor_Source2D_None> PDESensorParameter2D;
  typedef PDESensorParameter2D SensorClass;
  typedef SensorClass::ArrayQ<Real> ArrayQ;

  Sensor_AdvectiveFlux2D_None adv;
  Sensor_ViscousFlux2D_None visc;
  Sensor_Source2D_None source;

  SensorClass sensor(adv, visc, source);

  ArrayQ q = 0;
  BOOST_CHECK( sensor.isValidState(q) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/Sensor/PDESensorParameter2D_pattern.txt", true );

  typedef PDESensorParameter<PhysD2,
                             AdvectionDiffusionTraits<PhysD2>,
                             Sensor_AdvectiveFlux2D_None,
                             Sensor_ViscousFlux2D_None,
                             Sensor_Source2D_None> PDESensorParameter2D;
  typedef PDESensorParameter2D SensorClass;

  Sensor_AdvectiveFlux2D_None adv;
  Sensor_ViscousFlux2D_None visc;
  Sensor_Source2D_None source;

  SensorClass sensor(adv, visc, source);
  sensor.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
