// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Sensor_ViscousFlux2D_btest
//
// test of 2-D diffusion matrix class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Traits.h"
#include "pde/Sensor/Sensor_ViscousFlux2D.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Sensor_ViscousFlux2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  BOOST_CHECK( Sensor_ViscousFlux2D_None::D == 2 );
  BOOST_CHECK( Sensor_ViscousFlux2D_Uniform::D == 2 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( none )
{
  typedef Sensor_ViscousFlux2D_None::ArrayQ<Real> ArrayQ;
  typedef AdvectionDiffusionTraits<PhysD2>::ArrayQ<Real> PDEArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;
  const Real tol = 1.e-13;

  Real kxx = 0.0;
  Real kxy = 0.0;
  Real kyx = 0.0;
  Real kyy = 0.0;
  Real kxxTrue = kxx;
  Real kxyTrue = kxy;
  Real kyxTrue = kyx;
  Real kyyTrue = kyy;
  Real dummyq = 1.0;
  PDEArrayQ p = 0.0;
  ParamType param(0.1, p);

  Sensor_ViscousFlux2D_None visc;

  // static tests
  BOOST_CHECK( visc.D == 2 );

  // diffusionViscous
  Real x = 1;
  Real y = 2;
  Real time = 0;

  ArrayQ q  =  3.263;
  ArrayQ qx = -0.445;
  ArrayQ qy =  1.741;
  ArrayQ qxx = 4.230;
  ArrayQ qyx = 3.904;
  ArrayQ qyy = -0.482;

  kxx = kxy = 0;
  kyx = kyy = 0;

  visc.diffusionViscous(param, x, y, time, dummyq, qx, qy, kxx, kxy, kyx, kyy);

  BOOST_CHECK_CLOSE( kxxTrue, kxx, tol );
  BOOST_CHECK_CLOSE( kxyTrue, kxy, tol );
  BOOST_CHECK_CLOSE( kyxTrue, kyx, tol );
  BOOST_CHECK_CLOSE( kyyTrue, kyy, tol );

  // diffusive flux
  ArrayQ fTrue = -(kxx*qx + kxy*qy);
  ArrayQ gTrue = -(kxy*qx + kyy*qy);
  ArrayQ f = 0;
  ArrayQ g = 0;
  visc.fluxViscous(param, x, y, time, q, qx, qy, f, g );
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( gTrue, g, tol );

  ArrayQ strongPDETrue = -kxx*qxx - (kxy + kyx)*qyx - kyy*qyy;

  ArrayQ strongPDE = 0;
  visc.strongFluxViscous(param, x, y, time, q, qx, qy, qxx, qyx, qyy, strongPDE);
  BOOST_CHECK_CLOSE( strongPDE, strongPDETrue, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform )
{
  typedef Sensor_ViscousFlux2D_Uniform::ArrayQ<Real> ArrayQ;
  typedef AdvectionDiffusionTraits<PhysD2>::ArrayQ<Real> PDEArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;
  const Real tol = 1.e-13;

  Real kxx =  1.33479;
  Real kxy = -0.33886;
  Real kyx = -0.33886;
  Real kyy =  2.05673;
  Real kxxTrue = kxx;
  Real kxyTrue = kxy;
  Real kyxTrue = kyx;
  Real kyyTrue = kyy;
  Real dummyq = 1.0;
  PDEArrayQ p = 0.0;
  ParamType param(0.1, p);

  Sensor_ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  // static tests
  BOOST_CHECK( visc.D == 2 );

  // diffusionViscous
  Real x = 1;
  Real y = 2;
  Real time = 0;

  ArrayQ q  =  3.263;
  ArrayQ qx = -0.445;
  ArrayQ qy =  1.741;
  ArrayQ qxx = 4.230;
  ArrayQ qyx = 3.904;
  ArrayQ qyy = -0.482;

  kxx = kxy = 0;
  kyx = kyy = 0;

  visc.diffusionViscous(param, x, y, time, dummyq, qx, qy, kxx, kxy, kyx, kyy);

  BOOST_CHECK_CLOSE( kxxTrue, kxx, tol );
  BOOST_CHECK_CLOSE( kxyTrue, kxy, tol );
  BOOST_CHECK_CLOSE( kyxTrue, kyx, tol );
  BOOST_CHECK_CLOSE( kyyTrue, kyy, tol );

  // diffusive flux
  ArrayQ fTrue = -(kxx*qx + kxy*qy);
  ArrayQ gTrue = -(kxy*qx + kyy*qy);
  ArrayQ f = 0;
  ArrayQ g = 0;
  visc.fluxViscous(param, x, y, time, q, qx, qy, f, g );
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( gTrue, g, tol );


  ArrayQ strongPDETrue = -kxx*qxx - (kxy + kyx)*qyx - kyy*qyy;

  ArrayQ strongPDE = 0;
  visc.strongFluxViscous(param, x, y, time, q, qx, qy, qxx, qyx, qyy, strongPDE);
  BOOST_CHECK_CLOSE( strongPDE, strongPDETrue, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( GridDependent )
{
  typedef Sensor_ViscousFlux2D_GridDependent::ArrayQ<Real> ArrayQ;
  typedef AdvectionDiffusionTraits<PhysD2>::ArrayQ<Real> PDEArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;
  const Real tol = 1.e-13;

  Real C = 0.01;
  Real h = 0.1;

  Real KTrue = C*h*h;

  Real kxx, kxy, kyx, kyy;

  Real kxxTrue = KTrue;
  Real kxyTrue = 0.0;
  Real kyxTrue = 0.0;
  Real kyyTrue = KTrue;
  Real dummyq = 1.0;
  PDEArrayQ p = 0.0;
  ParamType param(h, p);

  Sensor_ViscousFlux2D_GridDependent visc(C);

  // static tests
  BOOST_CHECK( visc.D == 2 );

  // diffusionViscous
  Real x = 1;
  Real y = 2;
  Real time = 0;

  ArrayQ q  =  3.263;
  ArrayQ qx = -0.445;
  ArrayQ qy =  1.741;
  ArrayQ qxx = 4.230;
  ArrayQ qyx = 3.904;
  ArrayQ qyy = -0.482;

  kxx = kxy = 0;
  kyx = kyy = 0;

  visc.diffusionViscous(param, x, y, time, dummyq, qx, qy, kxx, kxy, kyx, kyy);

  BOOST_CHECK_CLOSE( kxxTrue, kxx, tol );
  BOOST_CHECK_CLOSE( kxyTrue, kxy, tol );
  BOOST_CHECK_CLOSE( kyxTrue, kyx, tol );
  BOOST_CHECK_CLOSE( kyyTrue, kyy, tol );


  // diffusive flux
  ArrayQ fTrue = -(kxx*qx + kxy*qy);
  ArrayQ gTrue = -(kxy*qx + kyy*qy);
  ArrayQ f = 0;
  ArrayQ g = 0;
  visc.fluxViscous(param, x, y, time, q, qx, qy, f, g );
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( gTrue, g, tol );

  ArrayQ strongPDETrue = -kxx*qxx - (kxy + kyx)*qyx - kyy*qyy;

  ArrayQ strongPDE = 0;
  visc.strongFluxViscous(param, x, y, time, q, qx, qy, qxx, qyx, qyy, strongPDE);
  BOOST_CHECK_CLOSE( strongPDE, strongPDETrue, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( GenHScale )
{
  typedef Sensor_ViscousFlux2D_GenHScale::ArrayQ<Real> ArrayQ;
  typedef AdvectionDiffusionTraits<PhysD2>::ArrayQ<Real> PDEArrayQ;
  typedef DLA::MatrixSymS<PhysD2::D,Real> MatrixSym;
  typedef MakeTuple<ParamTuple, MatrixSym, PDEArrayQ>::type ParamType;
  const Real tol = 1.e-13;

  Real C = 0.1;

  MatrixSym logH = {{0.8},
                    {0.2, 0.3}};

  MatrixSym H = exp(logH);
  MatrixSym Hsq = H*H;

  Real kxx, kxy, kyx, kyy;

  Real kxxTrue = C*Hsq(0,0);
  Real kxyTrue = C*Hsq(0,1);
  Real kyxTrue = C*Hsq(1,0);
  Real kyyTrue = C*Hsq(1,1);
  Real dummyq = 1.0;
  PDEArrayQ p = 0.0;
  ParamType param(logH, p);

  Sensor_ViscousFlux2D_GenHScale visc(C);

  // static tests
  BOOST_CHECK( visc.D == 2 );

  // diffusionViscous
  Real x = 1;
  Real y = 2;
  Real time = 0;

  ArrayQ q  =  3.263;
  ArrayQ qx = -0.445;
  ArrayQ qy =  1.741;
  ArrayQ qxx = 4.230;
  ArrayQ qyx = 3.904;
  ArrayQ qyy = -0.482;

  kxx = kxy = 0;
  kyx = kyy = 0;

  visc.diffusionViscous(param, x, y, time, dummyq, qx, qy, kxx, kxy, kyx, kyy);

  BOOST_CHECK_CLOSE( kxxTrue, kxx, tol );
  BOOST_CHECK_CLOSE( kxyTrue, kxy, tol );
  BOOST_CHECK_CLOSE( kyxTrue, kyx, tol );
  BOOST_CHECK_CLOSE( kyyTrue, kyy, tol );

  // diffusive flux
  ArrayQ fTrue = -(kxx*qx + kxy*qy);
  ArrayQ gTrue = -(kxy*qx + kyy*qy);
  ArrayQ f = 0;
  ArrayQ g = 0;
  visc.fluxViscous(param, x, y, time, q, qx, qy, f, g );
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( gTrue, g, tol );

  ArrayQ strongPDE = 0;
  BOOST_CHECK_THROW( visc.strongFluxViscous(param, x, y, time, q, qx, qy, qxx, qyx, qyy, strongPDE), DeveloperException);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/Sensor/PDESensorParameter2D_Sensor_ViscousFlux2D_pattern.txt", true );

  {
    Sensor_ViscousFlux2D_None visc1;
    visc1.dump( 2, output );
  }

  {
    Real kxx =  1.33479;
    Real kxy = -0.33886;
    Real kyx = -0.33886;
    Real kyy =  2.05673;
    Sensor_ViscousFlux2D_Uniform visc1(kxx, kxy, kyx, kyy);
    visc1.dump( 2, output );
  }

  {
    Real C = 0.01;
    Sensor_ViscousFlux2D_GridDependent visc1(C);
    visc1.dump( 2, output );
  }

  {
    Real C = 0.01;
    Sensor_ViscousFlux2D_GenHScale visc1(C);
    visc1.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
