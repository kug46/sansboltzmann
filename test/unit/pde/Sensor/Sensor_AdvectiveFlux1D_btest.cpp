// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Sensor_AdvectiveFlux1D_btest
//
// test of 2-D advection velocity class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Traits.h"
#include "pde/Sensor/Sensor_AdvectiveFlux1D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Sensor_AdvectiveFlux1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( None )
{
  typedef Sensor_AdvectiveFlux1D_None::ArrayQ<Real> ArrayQ;
  typedef Sensor_AdvectiveFlux1D_None::MatrixQ<Real> MatrixQ;
  typedef AdvectionDiffusionTraits<PhysD1>::ArrayQ<Real> PDEArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;

  Real nx = 1.0;
  Real nt = 0.5;
  Real x = 1;
  Real time = 0;

  Sensor_AdvectiveFlux1D_None adv;

  ArrayQ q = 1.45;
  ArrayQ qL = 2.45;
  ArrayQ qR = 3.45;
  ArrayQ qx = 1.56;
  ArrayQ fxTrue = 1.234;
  ArrayQ fx = fxTrue;
  MatrixQ axTrue = 3.45;
  MatrixQ ax = axTrue;
  PDEArrayQ p = 0.0;
  ParamType param(0.1, p);

  BOOST_CHECK( adv.hasFluxAdvective() == false );

  adv.flux(param, x, time, q, fx);
  BOOST_CHECK_EQUAL( fxTrue, fx );

  adv.fluxUpwind(param, x, time, qL, qR, nx, fx);
  BOOST_CHECK_EQUAL( fxTrue, fx );

  adv.fluxUpwindSpaceTime(param, x, time, qL, qR, nx, nt, fx);
  BOOST_CHECK_EQUAL( fxTrue + qL*nt, fx );

  adv.jacobian(param, x, time, q, ax);
  BOOST_CHECK_EQUAL( axTrue, ax );

  ArrayQ strongPDETrue = 5.78;
  ArrayQ strongPDE = strongPDETrue;
  adv.strongFlux(param, x, time, q, qx, strongPDE);
  BOOST_CHECK_EQUAL( strongPDETrue, strongPDE );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform_hasFluxAdvective )
{
  Real u =  1.33479;

  Sensor_AdvectiveFlux1D_Uniform adv(u);

  BOOST_CHECK( adv.hasFluxAdvective() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform_flux )
{
  typedef Sensor_AdvectiveFlux1D_Uniform::ArrayQ<Real> ArrayQ;
  typedef AdvectionDiffusionTraits<PhysD1>::ArrayQ<Real> PDEArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;
  const Real tol = 1.e-13;

  Real u =  1.33479;
  ArrayQ q = u;
  Real fTrue = u*u;
  PDEArrayQ p = 0.0;
  ParamType param(0.1, p);

  Sensor_AdvectiveFlux1D_Uniform adv(u);

  // flux
  Real x = 1;
  Real time = 0;
  ArrayQ f = 0;
  adv.flux(param, x, time, q, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform_jacobian )
{
  typedef Sensor_AdvectiveFlux1D_Uniform::ArrayQ<Real> ArrayQ;
  typedef AdvectionDiffusionTraits<PhysD1>::ArrayQ<Real> PDEArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;
  const Real tol = 1.e-13;

  Real u =  1.33479;
  ArrayQ q = u;
  Real jacTrue = u;
  PDEArrayQ p = 0.0;
  ParamType param(0.1, p);

  Sensor_AdvectiveFlux1D_Uniform adv(u);

  // jacobian
  Real x = 1;
  Real time = 0;
  ArrayQ jac = 0;
  adv.jacobian(param, x, time, q, jac);
  BOOST_CHECK_CLOSE( jacTrue, jac, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform_fluxUpwind )
{
  typedef Sensor_AdvectiveFlux1D_Uniform::ArrayQ<Real> ArrayQ;
  typedef AdvectionDiffusionTraits<PhysD1>::ArrayQ<Real> PDEArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;
  const Real tol = 1.e-13;

  Real u =  1.33479;
  Real nx = 1.0;
  ArrayQ qL = u;
  ArrayQ qR = 0;
  Real fTrue = nx*u*u;
  PDEArrayQ p = 0.0;
  ParamType param(0.1, p);

  Sensor_AdvectiveFlux1D_Uniform adv(u);

  // flux
  Real x = 1;
  Real time = 0;
  ArrayQ f = 0;
  adv.fluxUpwind(param, x, time, qL, qR, nx, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  u =  1.33479;
  nx = 1.0;
  qL = 0;
  qR = u;
  fTrue = 0;
  // flux
  x = 1;
  time = 0;
  f = 0;
  adv.fluxUpwind(param, x, time, qL, qR, nx, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  u =  1.33479;
  nx = -1.0;
  qL = u;
  qR = 0;
  fTrue = 0;
  // flux
  x = 1;
  time = 0;
  f = 0;
  adv.fluxUpwind(param, x, time, qL, qR, nx, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  u =  1.33479;
  nx = -1.0;
  qL = 0;
  qR = u;
  fTrue = nx*u*u;
  // flux
  x = 1;
  time = 0;
  f = 0;
  adv.fluxUpwind(param, x, time, qL, qR, nx, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform_fluxUpwindSpaceTime )
{
  typedef Sensor_AdvectiveFlux1D_Uniform::ArrayQ<Real> ArrayQ;
  typedef AdvectionDiffusionTraits<PhysD1>::ArrayQ<Real> PDEArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;
  const Real tol = 1.e-13;

  Real u =  1.33479;
  Real nx = 1.0;
  Real nt = 1.5;
  ArrayQ qL = u;
  ArrayQ qR = 0;
  Real fTrue = (nt*1 + nx*u)*qL;
  PDEArrayQ p = 0.0;
  ParamType param(0.1, p);

  Sensor_AdvectiveFlux1D_Uniform adv(u);

  // flux
  Real x = 1;
  Real time = 0;
  ArrayQ f = 0;
  adv.fluxUpwindSpaceTime(param, x, time, qL, qR, nx, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  u =  1.33479;
  nx = 1.0;
  qL = 0;
  qR = u;
  fTrue = 0;
  // flux
  x = 1;
  time = 0;
  f = 0;
  adv.fluxUpwindSpaceTime(param, x, time, qL, qR, nx, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  u =  1.33479;
  nx = -1.0;
  qL = u;
  qR = 0;
  fTrue = 0.5*(nt*1 + nx*u)*qL + 0.5*fabs(nt*1 + nx*u)*qL;
  // flux
  x = 1;
  time = 0;
  f = 0;
  adv.fluxUpwindSpaceTime(param, x, time, qL, qR, nx, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  u =  1.33479;
  nx = -1.0;
  qL = 0;
  qR = u;
  fTrue = 0.5*(nt*1 + nx*u)*qR - 0.5*fabs(nt*1 + nx*u)*qR;
  // flux
  x = 1;
  time = 0;
  f = 0;
  adv.fluxUpwindSpaceTime(param, x, time, qL, qR, nx, nt, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform_strongFlux )
{
  typedef Sensor_AdvectiveFlux1D_Uniform::ArrayQ<Real> ArrayQ;
  typedef AdvectionDiffusionTraits<PhysD1>::ArrayQ<Real> PDEArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;
  const Real tol = 1.e-13;

  Real u =  1.33479;
  ArrayQ q = u;
  ArrayQ qx = 1.56;
  Real strongPDETrue = u*qx;
  PDEArrayQ p = 0.0;
  ParamType param(0.1, p);

  Sensor_AdvectiveFlux1D_Uniform adv(u);

  // strong flux
  Real x = 1;
  Real time = 0;
  ArrayQ strongPDE = 0;
  adv.strongFlux(param, x, time, q, qx, strongPDE);
  BOOST_CHECK_CLOSE( strongPDETrue, strongPDE, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/Sensor/PDESensorParameter1D_Sensor_AdvectiveFlux1D_pattern.txt", true );

  Real u =  1.33479;
  Sensor_AdvectiveFlux1D_Uniform adv1(u);
  adv1.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
