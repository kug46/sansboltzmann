// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Source1D_JumpSensor_btest
// test for the 1-D source class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Field/FieldLine_DG_Cell.h"

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Sensor.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"

#include "pde/Sensor/Source1D_JumpSensor.h"

#include "Field/Tuple/ElementTuple.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Tuple/ParamTuple.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"

#include "Surreal/SurrealS.h"

#include "tools/Tuple.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Source1D_JumpSensor_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Source1D_JumpSensor_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEClass;
  typedef AdvectionDiffusion_Sensor Sensor;
  typedef Source1D_JumpSensor<Sensor> Source_JumpSensor;
  typedef Source_JumpSensor::ArrayQ<Real> ArrayQ;
  typedef Source_JumpSensor::MatrixQ<Real> MatrixQ;
  typedef MakeTuple<ParamTuple, Real, ArrayQ>::type ParamType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  int order = 1;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde(adv, visc, source);
  Sensor sensor(pde);
  Source_JumpSensor source_sensor(order, sensor);

  Real x = 1;
  Real time = 0;

  ArrayQ q = 1.45;
  ArrayQ qL = 2.45;
  ArrayQ qR = 3.45;

  ArrayQ qs = 1.65;
  ArrayQ qsL = 2.45;
  ArrayQ qsR = 3.45;
  ArrayQ qsx = 1.56;
  ArrayQ qsxL = 5.0;
  ArrayQ qsxR = 3.0;

  ArrayQ sTrue = qs;
  ArrayQ s = 0.0;

  ArrayQ sL = 0.0, sR = 0.0;

  MatrixQ dsduTrue = 1.0;
  MatrixQ dsdu = 0.0;

  BOOST_CHECK( source_sensor.hasSourceTerm() == true );
  BOOST_CHECK( source_sensor.hasSourceTrace() == true );
  BOOST_CHECK( source_sensor.needsSolutionGradientforSource() == false );

  ParamType param(1.0, q);
  source_sensor.source(param, x, time, qs, qsx, s);
  BOOST_CHECK_EQUAL( sTrue, s );

  ArrayQ dg = qL - qR;
  dg = smoothabs0(dg, 1.0e-3);
  ArrayQ gbar = 0.5*(fabs(qL) + fabs(qR));
  ArrayQ jump = dg / gbar;
  jump = smoothabs0(jump, 0.03);

  ArrayQ g = log10(jump + 1e-16);
  ArrayQ psi0 = -(2.25 + 3.0*log10(order));
  ArrayQ xi = g - (psi0 - 0.5);

//  ArrayQ Sk = smoothActivation_sine(xi);
//  ArrayQ Sk = smoothActivation_cubic(xi);
  ArrayQ Sk = smoothActivation_tanh(xi);

  ParamType paramL(1.0, qL);
  ParamType paramR(1.0, qR);

  ArrayQ sLTrue = -1.0*Sk;
  ArrayQ sRTrue = -1.0*Sk;
  source_sensor.sourceTrace(paramL, x, paramR, x, time, qsL, qsxL, qsR, qsxR, sL, sR);
  SANS_CHECK_CLOSE( sLTrue, sL, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue, sR, small_tol, close_tol );

  source_sensor.jacobianSource(param, x, time, qs, qsx, dsdu);
  SANS_CHECK_CLOSE( dsduTrue, dsdu, small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Source1D_JumpSensor_Surreal_pde_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEClass;
  typedef PDEClass::ArrayQ<SurrealS<1>> SurrealArrayQ;
  typedef AdvectionDiffusion_Sensor Sensor;
  typedef Source1D_JumpSensor<Sensor> Source_JumpSensor;
  typedef Source_JumpSensor::ArrayQ<Real> SensorArrayQ;
  typedef Source_JumpSensor::ArrayQ<SurrealS<1>> SurrealSensorArrayQ;
  typedef MakeTuple<ParamTuple, Real, SurrealArrayQ>::type ParamType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  int order = 1;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde(adv, visc, source);
  Sensor sensor(pde);
  Source_JumpSensor source_sensor(order, sensor);

  Real x = 1;
  Real time = 0;

  SurrealArrayQ q(1.45, 1.0);
  SurrealArrayQ qL(2.45, 1.0);
  SurrealArrayQ qR(3.45, 1.0);

  ParamType param(1.0, q);
  ParamType paramL(1.0, qL);
  ParamType paramR(1.0, qR);

  SensorArrayQ qs = 1.45;
  SensorArrayQ qsL = 2.45;
  SensorArrayQ qsR = 3.45;
  SensorArrayQ qsx = 1.56;
  SensorArrayQ qsxL = 4.0;
  SensorArrayQ qsxR = 2.0;

  SensorArrayQ sTrue = qs;
  SensorArrayQ s = 0.0;

  SurrealSensorArrayQ sL(0.0, 0.0);
  SurrealSensorArrayQ sR(0.0, 0.0);

  BOOST_CHECK( source_sensor.hasSourceTerm() == true );
  BOOST_CHECK( source_sensor.hasSourceTrace() == true );
  BOOST_CHECK( source_sensor.needsSolutionGradientforSource() == false );

  source_sensor.source(param, x, time, qs, qsx, s);
  BOOST_CHECK_EQUAL( sTrue, s );

  SurrealSensorArrayQ dg = qL - qR;
  dg = smoothabs0(dg, 1.0e-3);
  SurrealSensorArrayQ gbar = 0.5*(qL + qR);
  SurrealSensorArrayQ jump = dg / gbar;
  jump = smoothabs0(jump, 0.03);

  SurrealSensorArrayQ g = log10(jump + 1e-16);
  SurrealSensorArrayQ psi0 = -(2.25 + 3.0*log10(order));
  SurrealSensorArrayQ xi = g - (psi0 - 0.5);

//  SurrealSensorArrayQ Sk = smoothActivation_sine(xi);
//  SurrealSensorArrayQ Sk = smoothActivation_cubic(xi);
  SurrealSensorArrayQ Sk = smoothActivation_tanh(xi);

  SurrealSensorArrayQ sLTrue = -1.0*Sk;
  SurrealSensorArrayQ sRTrue = -1.0*Sk;
  source_sensor.sourceTrace(paramL, x, paramR, x, time, qsL, qsxL, qsR, qsxR, sL, sR);
  SANS_CHECK_CLOSE( sLTrue.value(), sL.value(), small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue.value(), sR.value(), small_tol, close_tol );
  SANS_CHECK_CLOSE( sLTrue.deriv(0), sL.deriv(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue.deriv(0), sR.deriv(0), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Source1D_JumpSensor_Surreal_sensor_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef AdvectionDiffusion_Sensor Sensor;
  typedef Source1D_JumpSensor<Sensor> Source_JumpSensor;
  typedef Source_JumpSensor::ArrayQ<SurrealS<1>> SurrealSensorArrayQ;
  typedef MakeTuple<ParamTuple, Real, ArrayQ>::type ParamType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  int order = 1;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde(adv, visc, source);
  Sensor sensor(pde);
  Source_JumpSensor source_sensor(order, sensor);

  Real x = 1;
  Real time = 0;

  ArrayQ q = 1.45;
  ArrayQ qL = 2.45;
  ArrayQ qR = 3.45;

  ParamType param(1.0, q);
  ParamType paramL(1.0, qL);
  ParamType paramR(1.0, qR);

  SurrealSensorArrayQ qs(1.45, 1.0);
  SurrealSensorArrayQ qsL(2.45, 1.0);
  SurrealSensorArrayQ qsR(3.45, 1.0);
  SurrealSensorArrayQ qsx(1.56, 1.0);
  SurrealSensorArrayQ qsxL(4.0, 1.0);
  SurrealSensorArrayQ qsxR(2.0, 1.0);

  SurrealSensorArrayQ sTrue = qs;
  SurrealSensorArrayQ s = 0.0;

  SurrealSensorArrayQ sL(0.0, 0.0);
  SurrealSensorArrayQ sR(0.0, 0.0);

  BOOST_CHECK( source_sensor.hasSourceTerm() == true );
  BOOST_CHECK( source_sensor.hasSourceTrace() == true );
  BOOST_CHECK( source_sensor.needsSolutionGradientforSource() == false );

  source_sensor.source(param, x, time, qs, qsx, s);
  SANS_CHECK_CLOSE( sTrue.value(), s.value(), small_tol, close_tol );
  SANS_CHECK_CLOSE( sTrue.deriv(0), s.deriv(0), small_tol, close_tol );

  SurrealSensorArrayQ dg = qL - qR;
  dg = smoothabs0(dg, 1.0e-3);
  SurrealSensorArrayQ gbar = 0.5*(qL + qR);
  SurrealSensorArrayQ jump = dg / gbar;
  jump = smoothabs0(jump, 0.03);

  SurrealSensorArrayQ g = log10(jump + 1e-16);
  SurrealSensorArrayQ psi0 = -(2.25 + 3.0*log10(order));
  SurrealSensorArrayQ xi = g - (psi0 - 0.5);

//  SurrealSensorArrayQ Sk = smoothActivation_sine(xi);
//  SurrealSensorArrayQ Sk = smoothActivation_cubic(xi);
  SurrealSensorArrayQ Sk = smoothActivation_tanh(xi);

  SurrealSensorArrayQ sLTrue = -1.0*Sk;
  SurrealSensorArrayQ sRTrue = -1.0*Sk;

  source_sensor.sourceTrace(paramL, x, paramR, x, time, qsL, qsxL, qsR, qsxR, sL, sR);
  SANS_CHECK_CLOSE( sLTrue.value(), sL.value(), small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue.value(), sR.value(), small_tol, close_tol );
  SANS_CHECK_CLOSE( sLTrue.deriv(0), sL.deriv(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue.deriv(0), sR.deriv(0), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Source1D_JumpSensor_BuckleyLeverett_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEClass;
  typedef AdvectionDiffusion_Sensor Sensor;
  typedef Source1D_JumpSensor_BuckleyLeverett<Sensor> Source_JumpSensor;
  typedef Source_JumpSensor::ArrayQ<Real> ArrayQ;
  typedef Source_JumpSensor::MatrixQ<Real> MatrixQ;
  typedef MakeTuple<ParamTuple, Real, ArrayQ>::type ParamType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  int order = 1;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde(adv, visc, source);
  Sensor sensor(pde);
  Source_JumpSensor source_sensor(order, sensor);

  Real x = 1;
  Real time = 0;

  ArrayQ q = 0.15;
  ArrayQ qL = 0.15;
  ArrayQ qR = 0.16;

  ArrayQ qs = 1.65;
  ArrayQ qsL = 2.45;
  ArrayQ qsR = 3.45;
  ArrayQ qsx = 1.56;
  ArrayQ qsxL = 5.0;
  ArrayQ qsxR = 3.0;

  ArrayQ sTrue = qs;
  ArrayQ s = 0.0;

  ArrayQ sL = 0.0, sR = 0.0;

  MatrixQ dsduTrue = 1.0;
  MatrixQ dsdu = 0.0;

  BOOST_CHECK( source_sensor.hasSourceTerm() == true );
  BOOST_CHECK( source_sensor.hasSourceTrace() == true );
  BOOST_CHECK( source_sensor.needsSolutionGradientforSource() == false );

  ParamType param(1.0, q);
  source_sensor.source(param, x, time, qs, qsx, s);
  BOOST_CHECK_EQUAL( sTrue, s );

  ArrayQ jump = qL - qR;
  jump = smoothabs0(jump, 1.0e-3);

  ArrayQ g = log10(jump);
  ArrayQ psi0 = 2.25 + 3.0*log10(order);
  ArrayQ xi = g + psi0 + 0.5;

//  ArrayQ Sk = smoothActivation_sine(xi);
//  ArrayQ Sk = smoothActivation_cubic(xi);
  ArrayQ Sk = smoothActivation_tanh(xi);

  ParamType paramL(1.0, qL);
  ParamType paramR(1.0, qR);

  ArrayQ sLTrue = -1.0*Sk;
  ArrayQ sRTrue = -1.0*Sk;
  source_sensor.sourceTrace(paramL, x, paramR, x, time, qsL, qsxL, qsR, qsxR, sL, sR);
  SANS_CHECK_CLOSE( sLTrue, sL, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue, sR, small_tol, close_tol );

  source_sensor.jacobianSource(param, x, time, qs, qsx, dsdu);
  SANS_CHECK_CLOSE( dsduTrue, dsdu, small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
