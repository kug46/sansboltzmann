// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCAdvectionDiffusion1D_btest
//
// test of 1-D Sensor BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/Sensor/BCSensorParameter1D.h"
#include "pde/Sensor/Sensor_AdvectiveFlux1D.h"
#include "pde/Sensor/Sensor_ViscousFlux1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/BCParameters.h"

#define BCPARAMETERS_INSTANTIATE // included because testing without NDConvert
#include "pde/BCParameters_impl.h"

using namespace std;
using namespace SANS;

//typedef PDEAdvectionDiffusion<PhysD1,
//                              AdvectiveFlux1D_Uniform,
//                              ViscousFlux1D_Uniform,
//                              Source1D_None> PDEAdvectionDiffusion1D;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
// Instantiate BCParameters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters<BCSensorParameter1DVector<Sensor_AdvectiveFlux1D_None,Sensor_ViscousFlux1D_None>>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( BCSensorParameter1D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  {
    typedef BCNone<PhysD1,SensorParameterTraits<PhysD1>::N> BCClass;

    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::NBC == 0 );
  }

  {
    typedef BCSensorParameter<PhysD1,BCTypeLinearRobin_mitLG> BCClass;

    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::N == 1 );
    BOOST_CHECK( BCClass::NBC == 1 );
  }

  {
    typedef BCSensorParameter<PhysD1,BCTypeLinearRobin_sansLG> BCClass;

    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::N == 1 );
    BOOST_CHECK( BCClass::NBC == 1 );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCParameters_test )
{
  typedef BCParameters<BCSensorParameter1DVector<Sensor_AdvectiveFlux1D_None,Sensor_ViscousFlux1D_None>> BCParams;

  BOOST_CHECK_EQUAL( "None"              , BCParams::params.BC.None );
  BOOST_CHECK_EQUAL( "LinearRobin_mitLG" , BCParams::params.BC.LinearRobin_mitLG );
  BOOST_CHECK_EQUAL( "LinearRobin_sansLG", BCParams::params.BC.LinearRobin_sansLG );

  PyDict BC0;
  BC0[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BC1;
  BC1[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BC1[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_mitLG>::params.A] = 2;
  BC1[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_mitLG>::params.B] = 3;
  BC1[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_mitLG>::params.bcdata] = 4;

  PyDict BC2;
  BC2[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BC2[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.A] = 2;
  BC2[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.B] = 3;
  BC2[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.bcdata] = 4;

  PyDict BCList;
  BCList["BC0"] = BC0;
  BCList["BC1"] = BC1;
  BCList["BC2"] = BC2;

  //No exceptions should be thrown
  BCParams::checkInputs(BCList);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCNone_test )
{
  typedef BCNone<PhysD1,SensorParameterTraits<PhysD1>::N> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  BCClass bc;

  Real nx = 0.8;
  ArrayQ q = 0.1;
  BOOST_CHECK_EQUAL( true, bc.isValidState( nx, q ));
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeLinearRobin_mitLG_test )
{
  typedef BCSensorParameter<PhysD1,BCTypeLinearRobin_mitLG> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;
  Real time = 0;

  Real A = 1;
  Real B = 0.2;
  Real bcdata = 3;

  BCClass bc( A, B, bcdata );

  // function tests

  MatrixQ AMtx;
  MatrixQ BMtx;
  ArrayQ bcdataVec;

  Real x = 0;
  Real nx = 0.8;
  Real param = 0.5;

  AMtx = 0;
  BMtx = 0;
  bc.coefficients( param, x, time, nx, AMtx, BMtx );
  BOOST_CHECK_CLOSE( A, AMtx, tol );
  BOOST_CHECK_CLOSE( B, BMtx, tol );

  bcdataVec = 0;
  bc.data( param, x, time, nx, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );

  ArrayQ q = 0.1;
  BOOST_CHECK_EQUAL( true, bc.isValidState( nx, q ));
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeLinearRobin_sansLG_test )
{
  typedef BCSensorParameter<PhysD1,BCTypeLinearRobin_sansLG> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;
  Real time = 0;

  Real A = 1;
  Real B = 0.2;
  Real bcdata = 3;

  BCClass bc( A, B, bcdata );

  // function tests

  MatrixQ AMtx;
  MatrixQ BMtx;
  ArrayQ bcdataVec;

  Real x = 0;
  Real nx = 0.8;
  Real param = 0.5;

  AMtx = 0;
  BMtx = 0;
  bc.coefficients( param, x, time, nx, AMtx, BMtx );
  BOOST_CHECK_CLOSE( A, AMtx, tol );
  BOOST_CHECK_CLOSE( B, BMtx, tol );

  bcdataVec = 0;
  bc.data( param, x, time, nx, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );

  ArrayQ q = 0.1;
  BOOST_CHECK_EQUAL( true, bc.isValidState( nx, q ));
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
