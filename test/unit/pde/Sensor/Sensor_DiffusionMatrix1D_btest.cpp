// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Sensor_ViscousFlux1D_btest
//
// test of 1-D diffusion matrix class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Traits.h"
#include "pde/Sensor/Sensor_ViscousFlux1D.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Sensor_ViscousFlux1D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  BOOST_CHECK( Sensor_ViscousFlux1D_None::D == 1 );
  BOOST_CHECK( Sensor_ViscousFlux1D_Uniform::D == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( none )
{
  typedef Sensor_ViscousFlux1D_None::ArrayQ<Real> ArrayQ;
  typedef AdvectionDiffusionTraits<PhysD1>::ArrayQ<Real> PDEArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;
  const Real tol = 1.e-13;

  Real kxx = 0.0;
  Real kxxTrue = 0.0;

  Sensor_ViscousFlux1D_None visc;

  // static tests
  BOOST_CHECK( visc.D == 1 );

  ArrayQ q  =  3.263;
  ArrayQ qx = -0.445;
  ArrayQ qxx = 4.230;
  PDEArrayQ p = 0.0;
  ParamType param(0.1, p);

  // functor
  Real x = 1;
  Real time = 0;

  ArrayQ fTrue = 0;
  ArrayQ f = 0;

  visc.fluxViscous(param, x, time, q, qx, f);
  BOOST_CHECK_EQUAL( fTrue, f );


  visc.diffusionViscous(param, x, time, q, qx, kxx);
  BOOST_CHECK_CLOSE( kxxTrue, kxx, tol );

  ArrayQ strongPDETrue = -kxx*qxx;

  ArrayQ strongPDE = 0;
  visc.strongFluxViscous(param, x, time, q, qx, qxx, strongPDE);
  BOOST_CHECK_CLOSE( strongPDE, strongPDETrue, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( uniform )
{
  typedef Sensor_ViscousFlux1D_Uniform::ArrayQ<Real> ArrayQ;
  typedef AdvectionDiffusionTraits<PhysD1>::ArrayQ<Real> PDEArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;
  const Real tol = 1.e-13;

  Real kxx =  1.33479;
  Real kxxTrue = kxx;

  Sensor_ViscousFlux1D_Uniform visc(kxx);

  // static tests
  BOOST_CHECK( visc.D == 1 );

  ArrayQ q  =  3.263;
  ArrayQ qx = -0.445;
  ArrayQ qxx = 4.230;
  PDEArrayQ p = 0.0;
  ParamType param(0.1, p);

  // functor
  Real x = 1;
  Real time = 0;

  ArrayQ fTrue = -kxxTrue*qx;
  ArrayQ f = 0;

  visc.fluxViscous(param, x, time, q, qx, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  kxx = 0;

  visc.diffusionViscous(param, x, time, q, qx, kxx);
  BOOST_CHECK_CLOSE( kxxTrue, kxx, tol );

  ArrayQ strongPDETrue = -kxx*qxx;

  ArrayQ strongPDE = 0;
  visc.strongFluxViscous(param, x, time, q, qx, qxx, strongPDE);
  BOOST_CHECK_CLOSE( strongPDE, strongPDETrue, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( GridDependent )
{
  typedef Sensor_ViscousFlux1D_GridDependent::ArrayQ<Real> ArrayQ;
  typedef AdvectionDiffusionTraits<PhysD1>::ArrayQ<Real> PDEArrayQ;
  typedef MakeTuple<ParamTuple, Real, PDEArrayQ>::type ParamType;
  const Real tol = 1.e-13;

  Real h = 0.1;
  Real kxx =  1.33479;
  Real kxxTrue = kxx*h*h;

  Sensor_ViscousFlux1D_GridDependent visc(kxx);

  // static tests
  BOOST_CHECK( visc.D == 1 );

  ArrayQ q  =  3.263;
  ArrayQ qx = -0.445;
  ArrayQ qxx = 4.230;
  PDEArrayQ p = 0.0;
  ParamType param(h, p);

  // functor
  Real x = 1;
  Real time = 0;

  ArrayQ fTrue = -kxxTrue*qx;
  ArrayQ f = 0;

  visc.fluxViscous(param, x, time, q, qx, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  kxx = 0;

  visc.diffusionViscous(param, x, time, q, qx, kxx);
  BOOST_CHECK_CLOSE( kxxTrue, kxx, tol );

  ArrayQ strongPDETrue = -kxx*qxx;

  ArrayQ strongPDE = 0;
  visc.strongFluxViscous(param, x, time, q, qx, qxx, strongPDE);
  BOOST_CHECK_CLOSE( strongPDE, strongPDETrue, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( GenHScale )
{
  typedef Sensor_ViscousFlux1D_GenHScale::ArrayQ<Real> ArrayQ;
  typedef AdvectionDiffusionTraits<PhysD1>::ArrayQ<Real> PDEArrayQ;
  typedef DLA::MatrixSymS<PhysD1::D,Real> MatrixSym;
  typedef MakeTuple<ParamTuple, MatrixSym, PDEArrayQ>::type ParamType;
  const Real tol = 1.e-13;

  Real h = 0.1;
  MatrixSym logH = {log(h)};

  Real C =  1.33479;
  Real kxx = 0.0;
  Real kxxTrue = C*h*h;

  Sensor_ViscousFlux1D_GenHScale visc(C);

  // static tests
  BOOST_CHECK( visc.D == 1 );

  ArrayQ q  =  3.263;
  ArrayQ qx = -0.445;
  ArrayQ qxx = 4.230;
  PDEArrayQ p = 0.0;
  ParamType param(logH, p);

  // functor
  Real x = 1;
  Real time = 0;

  ArrayQ fTrue = -kxxTrue*qx;
  ArrayQ f = 0;

  visc.fluxViscous(param, x, time, q, qx, f);
  BOOST_CHECK_CLOSE( fTrue, f, tol );

  visc.diffusionViscous(param, x, time, q, qx, kxx);
  BOOST_CHECK_CLOSE( kxxTrue, kxx, tol );

  ArrayQ strongPDE = 0;
  BOOST_CHECK_THROW( visc.strongFluxViscous(param, x, time, q, qx, qxx, strongPDE), DeveloperException);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/Sensor/PDESensorParameter1D_Sensor_ViscousFlux1D_pattern.txt", true );

  {
    Sensor_ViscousFlux1D_None visc1;
    visc1.dump( 2, output );
  }

  {
    Real kxx =  1.33479;
    Sensor_ViscousFlux1D_Uniform visc1(kxx);
    visc1.dump( 2, output );
  }

  {
    Real C =  1.33479;
    Sensor_ViscousFlux1D_GridDependent visc1(C);
    visc1.dump( 2, output );
  }

  {
    Real C =  1.33479;
    Sensor_ViscousFlux1D_GenHScale visc1(C);
    visc1.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
