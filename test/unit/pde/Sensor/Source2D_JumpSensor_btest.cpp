// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Source2D_JumpSensor_btest
// test for the 2-D source class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Field/FieldLine_DG_Cell.h"

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Sensor.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"

#include "pde/Sensor/Source2D_JumpSensor.h"

#include "Field/Tuple/ElementTuple.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Tuple/ParamTuple.h"

#include "Surreal/SurrealS.h"

#include "tools/Tuple.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Source2D_JumpSensor_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Source2D_JumpSensor_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEClass;
  typedef AdvectionDiffusion_Sensor Sensor;
  typedef Source2D_JumpSensor<Sensor> Source_JumpSensor;
  typedef Source_JumpSensor::ArrayQ<Real> ArrayQ;
  typedef Source_JumpSensor::MatrixQ<Real> MatrixQ;
  typedef MakeTuple<ParamTuple, Real, ArrayQ>::type ParamType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  int order = 1;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);


  Source2D_None source;

  PDEClass pde(adv, visc, source);
  Sensor sensor(pde);
  Source_JumpSensor source_sensor(order, sensor);

  Real x = 1;
  Real y = 1;
  Real time = 0;

  ArrayQ q = 1.45;
  ArrayQ qL = 2.45;
  ArrayQ qR = 3.45;

  ParamType param(1.0, q);
  ParamType paramL(1.0, qL);
  ParamType paramR(1.0, qR);

  ArrayQ qs = 1.65;
  ArrayQ qsL = 2.45;
  ArrayQ qsR = 3.45;
  ArrayQ qsx = 1.56;
  ArrayQ qsy = 2.56;
  ArrayQ qsxL = 5.0;
  ArrayQ qsxR = 3.0;
  ArrayQ qsyL = 7.0;
  ArrayQ qsyR = 1.0;

  ArrayQ sTrue = qs;
  ArrayQ s = 0.0;

  ArrayQ sL = 0.0, sR = 0.0;

  MatrixQ dsduTrue = 1.0;
  MatrixQ dsdu = 0.0;

  BOOST_CHECK( source_sensor.hasSourceTerm() == true );
  BOOST_CHECK( source_sensor.hasSourceTrace() == true );
  BOOST_CHECK( source_sensor.needsSolutionGradientforSource() == false );

  source_sensor.source(param, x, y, time, qs, qsx, qsy, s);
  BOOST_CHECK_EQUAL( sTrue, s );

  ArrayQ dg = qL - qR;
  dg = smoothabs0(dg, 1.0e-5);
  ArrayQ gbar = 0.5*(qL + qR);
  ArrayQ jump = dg / gbar;
  jump = smoothabs0(jump, 0.03);

  ArrayQ g = log10(jump);
  ArrayQ psi0 = -(2.25 + 3.0*log10(order));
  ArrayQ xi = g - (psi0 - 0.5);

//  ArrayQ Sk = smoothActivation_sine(xi);
//  ArrayQ Sk = smoothActivation_cubic(xi);
  ArrayQ Sk = smoothActivation_tanh(xi);

  ArrayQ sLTrue = -1.0*Sk;
  ArrayQ sRTrue = -1.0*Sk;
  source_sensor.sourceTrace(paramL, x, y, paramR, x, y, time, qsL, qsxL, qsyL, qsR, qsxR, qsyR, sL, sR);
  SANS_CHECK_CLOSE( sLTrue, sL, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue, sR, small_tol, close_tol );

  source_sensor.jacobianSource(param, x, y, time, qs, qsx, qsy, dsdu);
  SANS_CHECK_CLOSE( dsduTrue, dsdu, small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Source2D_JumpSensor_Surreal_pde_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEClass;
  typedef PDEClass::ArrayQ<SurrealS<1>> SurrealArrayQ;
  typedef AdvectionDiffusion_Sensor Sensor;
  typedef Source2D_JumpSensor<Sensor> Source_JumpSensor;
  typedef Source_JumpSensor::ArrayQ<Real> SensorArrayQ;
  typedef Source_JumpSensor::ArrayQ<SurrealS<1>> SurrealSensorArrayQ;
  typedef MakeTuple<ParamTuple, Real, SurrealArrayQ>::type ParamType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  int order = 1;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde(adv, visc, source);
  Sensor sensor(pde);
  Source_JumpSensor source_sensor(order, sensor);

  Real x = 1;
  Real y = 1;
  Real time = 0;

  SurrealArrayQ q(1.45, 1.0);
  SurrealArrayQ qL(2.45, 1.0);
  SurrealArrayQ qR(3.45, 1.0);

  ParamType param(1.0, q);
  ParamType paramL(1.0, qL);
  ParamType paramR(1.0, qR);

  SensorArrayQ qs = 1.45;
  SensorArrayQ qsL = 2.45;
  SensorArrayQ qsR = 3.45;
  SensorArrayQ qsx = 1.56;
  SensorArrayQ qsy = 1.56;
  SensorArrayQ qsxL = 4.0;
  SensorArrayQ qsxR = 2.0;
  SensorArrayQ qsyL = 4.0;
  SensorArrayQ qsyR = 2.0;

  SensorArrayQ sTrue = qs;
  SensorArrayQ s = 0.0;

  SurrealSensorArrayQ sL(0.0, 0.0);
  SurrealSensorArrayQ sR(0.0, 0.0);

  BOOST_CHECK( source_sensor.hasSourceTerm() == true );
  BOOST_CHECK( source_sensor.hasSourceTrace() == true );
  BOOST_CHECK( source_sensor.needsSolutionGradientforSource() == false );

  source_sensor.source(param, x, y, time, qs, qsx, qsy, s);
  BOOST_CHECK_EQUAL( sTrue, s );

  SurrealSensorArrayQ dg = qL - qR;
  dg = smoothabs0(dg, 1.0e-5);
  SurrealSensorArrayQ gbar = 0.5*(qL + qR);
  SurrealSensorArrayQ jump = dg / gbar;
  jump = smoothabs0(jump, 0.03);

  SurrealSensorArrayQ g = log10(jump);
  SurrealSensorArrayQ psi0 = -(2.25 + 3.0*log10(order));
  SurrealSensorArrayQ xi = g - (psi0 - 0.5);

//  SurrealSensorArrayQ Sk = smoothActivation_sine(xi);
//  SurrealSensorArrayQ Sk = smoothActivation_cubic(xi);
  SurrealSensorArrayQ Sk = smoothActivation_tanh(xi);

  SurrealSensorArrayQ sLTrue = -1.0*Sk;
  SurrealSensorArrayQ sRTrue = -1.0*Sk;
  source_sensor.sourceTrace(paramL, x, y, paramR, x, y, time, qsL, qsxL, qsyL, qsR, qsxR, qsyR, sL, sR);
  SANS_CHECK_CLOSE( sLTrue.value(), sL.value(), small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue.value(), sR.value(), small_tol, close_tol );
  SANS_CHECK_CLOSE( sLTrue.deriv(0), sL.deriv(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue.deriv(0), sR.deriv(0), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Source2D_JumpSensor_Surreal_sensor_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef AdvectionDiffusion_Sensor Sensor;
  typedef Source2D_JumpSensor<Sensor> Source_JumpSensor;
  typedef Source_JumpSensor::ArrayQ<SurrealS<1>> SurrealSensorArrayQ;
  typedef MakeTuple<ParamTuple, Real, ArrayQ>::type ParamType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  int order = 1;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde(adv, visc, source);
  Sensor sensor(pde);
  Source_JumpSensor source_sensor(order, sensor);

  Real x = 1;
  Real y = 1;
  Real time = 0;

  ArrayQ q = 1.45;
  ArrayQ qL = 2.45;
  ArrayQ qR = 3.45;

  ParamType param(1.0, q);
  ParamType paramL(1.0, qL);
  ParamType paramR(1.0, qR);

  SurrealSensorArrayQ qs(1.45, 1.0);
  SurrealSensorArrayQ qsL(2.45, 1.0);
  SurrealSensorArrayQ qsR(3.45, 1.0);
  SurrealSensorArrayQ qsx(1.56, 1.0);
  SurrealSensorArrayQ qsy(1.56, 1.0);
  SurrealSensorArrayQ qsxL(4.0, 1.0);
  SurrealSensorArrayQ qsxR(2.0, 1.0);
  SurrealSensorArrayQ qsyL(4.0, 1.0);
  SurrealSensorArrayQ qsyR(2.0, 1.0);

  SurrealSensorArrayQ sTrue = qs;
  SurrealSensorArrayQ s = 0.0;

  SurrealSensorArrayQ sL(0.0, 0.0);
  SurrealSensorArrayQ sR(0.0, 0.0);

  BOOST_CHECK( source_sensor.hasSourceTerm() == true );
  BOOST_CHECK( source_sensor.hasSourceTrace() == true );
  BOOST_CHECK( source_sensor.needsSolutionGradientforSource() == false );

  source_sensor.source(param, x, y, time, qs, qsx, qsy, s);
  SANS_CHECK_CLOSE( sTrue.value(), s.value(), small_tol, close_tol );
  SANS_CHECK_CLOSE( sTrue.deriv(0), s.deriv(0), small_tol, close_tol );

  SurrealSensorArrayQ dg = qL - qR;
  dg = smoothabs0(dg, 1.0e-5);
  SurrealSensorArrayQ gbar = 0.5*(qL + qR);
  SurrealSensorArrayQ jump = dg / gbar;
  jump = smoothabs0(jump, 0.03);

  SurrealSensorArrayQ g = log10(jump);
  SurrealSensorArrayQ psi0 = -(2.25 + 3.0*log10(order));
  SurrealSensorArrayQ xi = g - (psi0 - 0.5);

//  SurrealSensorArrayQ Sk = smoothActivation_sine(xi);
//  SurrealSensorArrayQ Sk = smoothActivation_cubic(xi);
  SurrealSensorArrayQ Sk = smoothActivation_tanh(xi);

  SurrealSensorArrayQ sLTrue = -1.0*Sk;
  SurrealSensorArrayQ sRTrue = -1.0*Sk;

  source_sensor.sourceTrace(paramL, x, y, paramR, x, y, time, qsL, qsxL, qsyL, qsR, qsxR, qsyR, sL, sR);
  SANS_CHECK_CLOSE( sLTrue.value(), sL.value(), small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue.value(), sR.value(), small_tol, close_tol );
  SANS_CHECK_CLOSE( sLTrue.deriv(0), sL.deriv(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue.deriv(0), sR.deriv(0), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Source2D_JumpSensor_BuckleyLeverett_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEClass;
  typedef AdvectionDiffusion_Sensor Sensor;
  typedef Source2D_JumpSensor_BuckleyLeverett<Sensor> Source_JumpSensor;
  typedef Source_JumpSensor::ArrayQ<Real> ArrayQ;
  typedef Source_JumpSensor::MatrixQ<Real> MatrixQ;
  typedef MakeTuple<ParamTuple, Real, ArrayQ>::type ParamType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  int order = 1;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde(adv, visc, source);
  Sensor sensor(pde);
  Source_JumpSensor source_sensor(order, sensor);

  Real x = 1;
  Real y = 1;
  Real time = 0;

  ArrayQ q = 0.15;
  ArrayQ qL = 0.15;
  ArrayQ qR = 0.16;

  ParamType param(1.0, q);
  ParamType paramL(1.0, qL);
  ParamType paramR(1.0, qR);

  ArrayQ qs = 1.65;
  ArrayQ qsL = 2.45;
  ArrayQ qsR = 3.45;
  ArrayQ qsx = 1.56;
  ArrayQ qsy = 2.56;
  ArrayQ qsxL = 5.0;
  ArrayQ qsxR = 3.0;
  ArrayQ qsyL = 7.0;
  ArrayQ qsyR = 1.0;

  ArrayQ sTrue = qs;
  ArrayQ s = 0.0;

  ArrayQ sL = 0.0, sR = 0.0;

  MatrixQ dsduTrue = 1.0;
  MatrixQ dsdu = 0.0;

  BOOST_CHECK( source_sensor.hasSourceTerm() == true );
  BOOST_CHECK( source_sensor.hasSourceTrace() == true );
  BOOST_CHECK( source_sensor.needsSolutionGradientforSource() == false );

  source_sensor.source(param, x, y, time, qs, qsx, qsy, s);
  BOOST_CHECK_EQUAL( sTrue, s );

  ArrayQ jump = qL - qR;
  jump = smoothabs0(jump, 1.0e-3);

  ArrayQ g = log10(jump);
  ArrayQ psi0 = 2.25 + 3.0*log10(order);
  ArrayQ xi = g + psi0 + 0.5;

//  ArrayQ Sk = smoothActivation_sine(xi);
//  ArrayQ Sk = smoothActivation_cubic(xi);
  ArrayQ Sk = smoothActivation_tanh(xi);

  ArrayQ sLTrue = -1.0*Sk;
  ArrayQ sRTrue = -1.0*Sk;
  source_sensor.sourceTrace(paramL, x, y, paramR, x, y, time, qsL, qsxL, qsyL, qsR, qsxR, qsyR, sL, sR);
  SANS_CHECK_CLOSE( sLTrue, sL, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRTrue, sR, small_tol, close_tol );

  source_sensor.jacobianSource(param, x, y, time, qs, qsx, qsy, dsdu);
  SANS_CHECK_CLOSE( dsduTrue, dsdu, small_tol, close_tol );
}

void ping_function(Real dq)
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEClass;
  typedef AdvectionDiffusion_Sensor Sensor;
  typedef Source2D_JumpSensor<Sensor> Source_JumpSensor;
  typedef Source_JumpSensor::ArrayQ<Real> ArrayQ;
  typedef PDEClass::ArrayQ<SurrealS<1>> SurrealArrayQ;
  typedef MakeTuple<ParamTuple, Real, SurrealArrayQ>::type ParamType;
  typedef MakeTuple<ParamTuple, Real, ArrayQ>::type ParamTypeScalar;
  int order = 1;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde(adv, visc, source);
  Sensor sensor(pde);
  Source_JumpSensor source_sensor(order, sensor);

  Real x = 1;
  Real y = 1;
  Real time = 0;

  SurrealArrayQ qL(1.0 - 0.5*dq, 1.0);
  SurrealArrayQ qR(1.0 + 0.5*dq, 0.0);

  ParamType paramL0(1.0, qL);
  ParamType paramR0(1.0, qR);

  ArrayQ qsL = 2.45;
  ArrayQ qsR = 3.45;
  ArrayQ qsxL = 5.0;
  ArrayQ qsxR = 3.0;
  ArrayQ qsyL = 7.0;
  ArrayQ qsyR = 1.0;

  SurrealArrayQ sL = 0.0, sR = 0.0;

  qL.deriv(0) = 1;
  qR.deriv(0) = 0;
  source_sensor.sourceTrace(paramL0, x, y, paramR0, x, y, time, qsL, qsxL, qsyL, qsR, qsxR, qsyR, sL, sR);
  ArrayQ dsL_dqL = sL.deriv(0);
  ArrayQ dsR_dqL = sR.deriv(0);

  sL = 0.0, sR = 0.0;

  qL.deriv(0) = 0;
  qR.deriv(0) = 1;

  ParamType paramL1(1.0, qL);
  ParamType paramR1(1.0, qR);

  source_sensor.sourceTrace(paramL1, x, y, paramR1, x, y, time, qsL, qsxL, qsyL, qsR, qsxR, qsyR, sL, sR);
  ArrayQ dsL_dqR = sL.deriv(0);
  ArrayQ dsR_dqR = sR.deriv(0);

  //Pings
  Real delta[2] = {1e-4, 1e-5};
  ArrayQ dsL_dqL_FD[2];
  ArrayQ dsR_dqL_FD[2];
  ArrayQ dsL_dqR_FD[2];
  ArrayQ dsR_dqR_FD[2];
  ArrayQ errorsLL[2], errorsRL[2];
  ArrayQ errorsLR[2], errorsRR[2];

  for (int i = 0; i < 2; i++)
  {
    ArrayQ sLm = 0.0, sLp = 0.0;
    ArrayQ sRm = 0.0, sRp = 0.0;

    ParamTypeScalar paramScalarL0(1.0, qL.value());
    ParamTypeScalar paramScalarR0(1.0, qR.value());

    //Ping w.r.t qL
    qL.value() += delta[i];
    ParamTypeScalar paramScalarL1(1.0, qL.value());
    source_sensor.sourceTrace(paramScalarL1, x, y, paramScalarR0, x, y, time, qsL, qsxL, qsyL, qsR, qsxR, qsyR, sLp, sRp);
    qL.value() -= 2*delta[i];
    ParamTypeScalar paramScalarL2(1.0, qL.value());
    source_sensor.sourceTrace(paramScalarL2, x, y, paramScalarR0, x, y, time, qsL, qsxL, qsyL, qsR, qsxR, qsyR, sLm, sRm);
    qL.value() += delta[i];

    dsL_dqL_FD[i] = (sLp - sLm)/(2*delta[i]);
    dsR_dqL_FD[i] = (sRp - sRm)/(2*delta[i]);

    errorsLL[i] = fabs(dsL_dqL - dsL_dqL_FD[i]);
    errorsRL[i] = fabs(dsR_dqL - dsR_dqL_FD[i]);

//    std::cout << i << ": " << dsL_dqL << ", " << dsL_dqL_FD[i] << std::endl;
//    std::cout << i << ": " << dsR_dqL << ", " << dsR_dqL_FD[i] << std::endl;

    sLm = 0.0;
    sLp = 0.0;
    sRm = 0.0;
    sRp = 0.0;

    //Ping w.r.t qR
    qR.value() += delta[i];
    ParamTypeScalar paramScalarR1(1.0, qR.value());
    source_sensor.sourceTrace(paramScalarL0, x, y, paramScalarR1, x, y, time, qsL, qsxL, qsyL, qsR, qsxR, qsyR, sLp, sRp);
    qR.value() -= 2*delta[i];
    ParamTypeScalar paramScalarR2(1.0, qR.value());
    source_sensor.sourceTrace(paramScalarL0, x, y, paramScalarR2, x, y, time, qsL, qsxL, qsyL, qsR, qsxR, qsyR, sLm, sRm);
    qR.value() += delta[i];

    dsL_dqR_FD[i] = (sLp - sLm)/(2*delta[i]);
    dsR_dqR_FD[i] = (sRp - sRm)/(2*delta[i]);

    errorsLR[i] = fabs(dsL_dqR - dsL_dqR_FD[i]);
    errorsRR[i] = fabs(dsR_dqR - dsR_dqR_FD[i]);

//    std::cout << i << ": " << dsL_dqR << ", " << dsL_dqR_FD[i] << std::endl;
//    std::cout << i << ": " << dsR_dqR << ", " << dsR_dqR_FD[i] << std::endl;
  }

  Real rateLL = errorsLL[0] == 0 ? 2 : log(errorsLL[0]/errorsLL[1]) / log(delta[0]/delta[1]);
  Real rateRL = errorsRL[0] == 0 ? 2 : log(errorsRL[0]/errorsRL[1]) / log(delta[0]/delta[1]);

  Real rateLR = errorsLR[0] == 0 ? 2 : log(errorsLR[0]/errorsLR[1]) / log(delta[0]/delta[1]);
  Real rateRR = errorsRR[0] == 0 ? 2 : log(errorsRR[0]/errorsRR[1]) / log(delta[0]/delta[1]);

//  std::cout << rateLL << ", " << rateRL << ", " << rateLR << ", " << rateRR <<std::endl;
//  std::cout << std::endl;

  Real small_tol = 1e-12;

  if (errorsLL[1] > small_tol)
    BOOST_CHECK( rateLL >= 1.95 && rateLL <= 2.05 );

  if (errorsRL[1] > small_tol)
    BOOST_CHECK( rateRL >= 1.95 && rateRL <= 2.05 );

  if (errorsLR[1] > small_tol)
    BOOST_CHECK( rateLR >= 1.95 && rateLR <= 2.05 );

  if (errorsRR[1] > small_tol)
    BOOST_CHECK( rateRR >= 1.95 && rateRR <= 2.05 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Source2D_JumpSensor_SourceTrace_ParamPing_test )
{
  ping_function(1e-1); //Large jump - activation function argument > 1
  ping_function(1e-2); //Moderate jump - activation function argument is high (~0.75)
  ping_function(3e-3); //Moderate jump - activation function argument is low (~0.25)
  ping_function(5e-4); //Small jump - activation function argument < 0
  ping_function(0.00); //No jump - activation function argument = -2.75
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
