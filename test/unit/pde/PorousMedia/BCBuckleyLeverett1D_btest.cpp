// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCBuckleyLeverett1D_btest
//
// testing of Buckley-Leverett BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/BCParameters.h"

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverett.h"
#include "pde/PorousMedia/BCBuckleyLeverett1D.h"


//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel_PowerLaw, CapillaryModel_Linear> TraitsModelClass;
typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;
template class BCBuckleyLeverett1D< BCTypeLinearRobin, PDEClass >;
template class BCBuckleyLeverett1D< BCTypeDirichlet_mitState, PDEClass >;
template class BCBuckleyLeverett1D< BCTypeFunction_mitState, PDEClass >;

}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BCBuckleyLeverett1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  {
    typedef BCNone<PhysD1,TraitsSizeBuckleyLeverett<PhysD1>::N> BCClass;
    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::NBC == 0 );
  }

  {
    typedef BCBuckleyLeverett1D< BCTypeLinearRobin, PDEClass> BCClass;
    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::N == 1 );
    BOOST_CHECK( BCClass::NBC == 1 );
  }

  {
    typedef BCBuckleyLeverett1D< BCTypeDirichlet_mitState, PDEClass> BCClass;
    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::N == 1 );
    BOOST_CHECK( BCClass::NBC == 1 );
  }

  {
    typedef BCBuckleyLeverett1D< BCTypeFunction_mitState, PDEClass> BCClass;
    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::N == 1 );
    BOOST_CHECK( BCClass::NBC == 1 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor )
{
  // PDE
  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  CapillaryModel_Linear cap_model(5);

  PDEClass pde(phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  typedef BCParameters< BCBuckleyLeverett1DVector<PDEClass> > BCParams;

  PyDict BCList;

  {
    typedef BCNone<PhysD1,TraitsSizeBuckleyLeverett<PhysD1>::N> BCClass;

    //Pydict constructor
    PyDict BCNone;
    BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;
    BCClass bc1(pde, BCNone);

    BCList["BC0"] = BCNone;
  }

  {
    typedef BCBuckleyLeverett1D< BCTypeLinearRobin, PDEClass > BCClass;
    typedef BCClass::ArrayQ<Real> ArrayQ;

    const ArrayQ bcdata = 0.5;
    BCClass bc1(1.0, 0.0, bcdata);

    //Pydict constructor
    PyDict BCLinearRobin;
    BCLinearRobin[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin;
    BCLinearRobin[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.A] = 1.0;
    BCLinearRobin[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.B] = 0.0;
    BCLinearRobin[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.bcdata] = bcdata;
    BCClass bc2(pde, BCLinearRobin);

    BCList["BC1"] = BCLinearRobin;
  }

  {
    typedef BCBuckleyLeverett1D< BCTypeDirichlet_mitState, PDEClass > BCClass;
    typedef BCClass::ArrayQ<Real> ArrayQ;

    //Direct constructor
    const ArrayQ bcdata = 0.5;
    BCClass bc1(pde, bcdata);

    //Pydict constructor
    PyDict BCDirichlet_mitState;
    BCDirichlet_mitState[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
    BCDirichlet_mitState[BCBuckleyLeverett1DParams<BCTypeDirichlet_mitState>::params.qB] = bcdata;
    BCClass bc2(pde, BCDirichlet_mitState);

    BCList["BC2"] = BCDirichlet_mitState;
  }

  {
    typedef BCBuckleyLeverett1D< BCTypeDirichlet_mitStateSpaceTime, PDEClass > BCClass;
    typedef BCClass::ArrayQ<Real> ArrayQ;

    //Direct constructor
    const ArrayQ bcdata = 0.5;
    BCClass bc1(pde, bcdata);

    //Pydict constructor
    PyDict BCDirichlet_mitStateST;
    BCDirichlet_mitStateST[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitStateSpaceTime;
    BCDirichlet_mitStateST[BCBuckleyLeverett1DParams<BCTypeDirichlet_mitStateSpaceTime>::params.qB] = bcdata;
    BCClass bc2(pde, BCDirichlet_mitStateST);

    BCList["BC3"] = BCDirichlet_mitStateST;
  }

  {
    typedef BCBuckleyLeverett1D< BCTypeFunction_mitState, PDEClass > BCClass;

    typedef ScalarFunction1D_Sine SolutionType;
    std::shared_ptr<SolutionType> slnExact( new SolutionType );

    //Direct constructor
    BCClass bc1(pde, slnExact);

    //Pydict constructor
    PyDict Soln;
    Soln[BCBuckleyLeverett1DParams<BCTypeFunction_mitState>::params.Function.Name] =
        BCBuckleyLeverett1DParams<BCTypeFunction_mitState>::params.Function.Sine;

    PyDict BCSolution;
    BCSolution[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
    BCSolution[BCBuckleyLeverett1DParams<BCTypeFunction_mitState>::params.Function] = Soln;
    BCClass bc2(pde, BCSolution);

    BCList["BC4"] = BCSolution;
  }

  //No exceptions should be thrown
  BCParams::checkInputs(BCList);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeLinearRobin_test )
{
  typedef BCBuckleyLeverett1D< BCTypeLinearRobin, PDEClass > BCClass;

  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real A = 1;
  Real B = 0.2;
  Real bcdata = 0.7;

  BCClass bc( A, B, bcdata );

  MatrixQ AMtx;
  MatrixQ BMtx;
  ArrayQ bcdataVec;

  Real x = 0;
  Real time = 0;
  Real nx = 0.8;

  AMtx = 0;
  BMtx = 0;
  bc.coefficients( x, time, nx, AMtx, BMtx );
  BOOST_CHECK_CLOSE( A, AMtx, tol );
  BOOST_CHECK_CLOSE( B, BMtx, tol );

  bcdataVec = 0;
  bc.data( x, time, nx, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeDirichlet_mitState_test )
{
  typedef BCBuckleyLeverett1D< BCTypeDirichlet_mitState, PDEClass > BCClass;

  typedef BCClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  // PDE
  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  CapillaryModel_Linear cap_model(5);

  PDEClass pde(phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  // BC
  ArrayQ bcdata = 0.7;

  BCClass bc( pde, bcdata );

  Real x = 0;
  Real time = 0;
  Real nx = 0.8;

  ArrayQ qI = 0.5;
  ArrayQ qB = 0;
  ArrayQ qIx = 0.35;

  bc.state( x, time, nx, qI, qB );
  BOOST_CHECK_CLOSE( qB, bcdata, tol );

  ArrayQ Fn_true = 0.1;
  pde.fluxAdvectiveUpwind(x, time, qI, qB, nx, Fn_true);

  ArrayQ fx_true = 0;
  pde.fluxViscous(x, time, qB, qIx, fx_true);
  Fn_true += fx_true*nx;

  ArrayQ Fn = 0.1;
  bc.fluxNormal(x, time, nx, qI, qIx, qB, Fn);
  BOOST_CHECK_CLOSE( Fn_true, Fn, tol );

  BOOST_CHECK_EQUAL( bc.isValidState(nx, qI), true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeDirichlet_mitStateSpaceTime_test )
{
  typedef BCBuckleyLeverett1D< BCTypeDirichlet_mitStateSpaceTime, PDEClass > BCClass;

  typedef BCClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  // PDE
  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  CapillaryModel_Linear cap_model(5);

  PDEClass pde(phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  // BC
  ArrayQ bcdata = 0.7;

  BCClass bc( pde, bcdata );

  Real x = 0;
  Real time = 0;
  Real nx = 0.8, nt = 0.25;

  ArrayQ qI = 0.5;
  ArrayQ qB = 0;
  ArrayQ qIx = 0.35, qIt = -0.27;

  bc.state( x, time, nx, nt, qI, qB );
  BOOST_CHECK_CLOSE( qB, bcdata, tol );

  ArrayQ Fn_true = 0.1;
  pde.fluxAdvectiveUpwindSpaceTime(x, time, qI, qB, nx, nt, Fn_true);

  ArrayQ fx_true = 0, ft_true = 0;
  pde.fluxViscousSpaceTime(x, time, qB, qIx, qIt, fx_true, ft_true);
  Fn_true += fx_true*nx + ft_true*nt;

  ArrayQ Fn = 0.1;
  bc.fluxNormalSpaceTime(x, time, nx, nt, qI, qIx, qIt, qB, Fn);
  BOOST_CHECK_CLOSE( Fn_true, Fn, tol );

  BOOST_CHECK_EQUAL( bc.isValidState(nx, nt, qI), true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFunction_test )
{
  typedef BCBuckleyLeverett1D< BCTypeFunction_mitState, PDEClass > BCClass;

  typedef BCClass::template ArrayQ<Real> ArrayQ;

  typedef ScalarFunction1D_Sine SolutionType;
  std::shared_ptr<SolutionType> slnExact( new SolutionType );

  const Real tol = 1.e-13;

  // PDE
  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  CapillaryModel_Linear cap_model(5);

  PDEClass pde(phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  // BC
  BCClass bc(pde, slnExact );

  Real x = 0;
  Real time = 0;
  Real nx = 0.8;

  ArrayQ qI = 0.5;
  ArrayQ qB = 0;
  ArrayQ bcdata = (*slnExact)(x, time);
  ArrayQ qIx = 0.35;

  bc.state( x, time, nx, qI, qB );
  BOOST_CHECK_CLOSE( qB, bcdata, tol );

  ArrayQ Fn_true = 0.1;
  pde.fluxAdvectiveUpwind(x, time, qI, qB, nx, Fn_true);

  ArrayQ fx_true = 0;
  pde.fluxViscous(x, time, qB, qIx, fx_true);
  Fn_true += fx_true*nx;

  ArrayQ Fn = 0.1;
  bc.fluxNormal(x, time, nx, qI, qIx, qB, Fn);
  BOOST_CHECK_CLOSE( Fn_true, Fn, tol );

  BOOST_CHECK_EQUAL( bc.isValidState(nx, qI), true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/BCBuckleyLeverett1D_pattern.txt", true );

  typedef BCParameters< BCBuckleyLeverett1DVector<PDEClass> > BCParams;

  // PDE
  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  CapillaryModel_Linear cap_model(5);

  PDEClass pde(phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  {
    typedef BCNone<PhysD1,TraitsSizeBuckleyLeverett<PhysD1>::N> BCClass;

    //Pydict constructor
    PyDict BCNone;
    BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

    BCClass bc(pde, BCNone);
    bc.dump( 2, output );
  }

  {
    typedef BCBuckleyLeverett1D< BCTypeLinearRobin, PDEClass > BCClass;

    Real A = 1;
    Real B = 0.2;
    Real bcdata = 0.384;

    BCClass bc( A, B, bcdata );
    bc.dump( 2, output );
  }

  {
    typedef BCBuckleyLeverett1D< BCTypeDirichlet_mitState, PDEClass > BCClass;

    Real bcdata = 0.782;

    BCClass bc( pde, bcdata );
    bc.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
