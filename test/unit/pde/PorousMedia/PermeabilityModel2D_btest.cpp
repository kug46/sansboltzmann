// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PermeabilityModel2D_btest
// testing of the 2D PermeabilityModel class

#include <fstream>
#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/PorousMedia/PermeabilityModel2D.h"


using namespace std;
using namespace SANS;

namespace SANS
{

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( PermeabilityModel2D_Constant_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Constructor )
{
  const Real tol = 1e-13;
  Real Kxx = 1.18, Kxy = 0.54, Kyx = 0.21, Kyy = 2.37;

  PyDict dict;
  dict[PermeabilityModel2D_Constant_Params::params.Kxx] = Kxx;
  dict[PermeabilityModel2D_Constant_Params::params.Kxy] = Kxy;
  dict[PermeabilityModel2D_Constant_Params::params.Kyx] = Kyx;
  dict[PermeabilityModel2D_Constant_Params::params.Kyy] = Kyy;

  //Check dictionary
  PermeabilityModel2D_Constant_Params::checkInputs(dict);

  Real x = 0.5, y = 0.7, t = 1.0;

  // PyDict constructor
  PermeabilityModel2D_Constant Kmodel(dict);
  Real a, b, c, d;
  Kmodel.permeability(x, y, t, a, b, c, d);
  BOOST_CHECK_CLOSE( a, Kxx, tol );
  BOOST_CHECK_CLOSE( b, Kxy, tol );
  BOOST_CHECK_CLOSE( c, Kyx, tol );
  BOOST_CHECK_CLOSE( d, Kyy, tol );

  // copy constructor
  PermeabilityModel2D_Constant Kmodel2(Kmodel);
  a = 0, b = 0, c = 0, d = 0;
  Kmodel2.permeability(x, y, t, a, b, c, d);
  BOOST_CHECK_CLOSE( a, Kxx, tol );
  BOOST_CHECK_CLOSE( b, Kxy, tol );
  BOOST_CHECK_CLOSE( c, Kyx, tol );
  BOOST_CHECK_CLOSE( d, Kyy, tol );

  // direct constructor
  PermeabilityModel2D_Constant Kmodel3(Kxx, Kxy, Kyx, Kyy);
  a = 0, b = 0, c = 0, d = 0;
  Kmodel3.permeability(x, y, t, a, b, c, d);
  BOOST_CHECK_CLOSE( a, Kxx, tol );
  BOOST_CHECK_CLOSE( b, Kxy, tol );
  BOOST_CHECK_CLOSE( c, Kyx, tol );
  BOOST_CHECK_CLOSE( d, Kyy, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/PermeabilityModel2D_Constant_pattern.txt", true );

  Real Kxx = 1.18, Kxy = 0.54, Kyx = 0.21, Kyy = 2.37;

  PyDict dict;
  dict[PermeabilityModel2D_Constant_Params::params.Kxx] = Kxx;
  dict[PermeabilityModel2D_Constant_Params::params.Kxy] = Kxy;
  dict[PermeabilityModel2D_Constant_Params::params.Kyx] = Kyx;
  dict[PermeabilityModel2D_Constant_Params::params.Kyy] = Kyy;

  //Check dictionary
  PermeabilityModel2D_Constant_Params::checkInputs(dict);

  // PyDict constructor
  PermeabilityModel2D_Constant Kmodel(dict);

  Kmodel.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()


//############################################################################//
BOOST_AUTO_TEST_SUITE( PermeabilityModel2D_QuadBlock_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Constructor )
{
  typedef PermeabilityModel2D_QuadBlock RockPermModel;

  const Real tol = 1e-13;
  Real Kxx = 1.18, Kxy = 0.54, Kyx = 0.21, Kyy = 2.37;

  DLA::MatrixS<2,2,Real> Kref_mat = {{Kxx, Kxy}, {Kyx, Kyy}};

  //Block 0 vertex coordinates in counter-clockwise order
  RockPermModel::QuadBlock quadblock0 = {{RockPermModel::Coord({{ -3.0, 10.0}}),
                                          RockPermModel::Coord({{  3.0, 10.0}}),
                                          RockPermModel::Coord({{  3.0, 12.0}}),
                                          RockPermModel::Coord({{ -3.0, 12.0}})}};

  //Block 1 vertex coordinates in counter-clockwise order
  RockPermModel::QuadBlock quadblock1 = {{RockPermModel::Coord({{  5.0,  6.0}}),
                                          RockPermModel::Coord({{ 10.0, -4.0}}),
                                          RockPermModel::Coord({{ 13.0,  8.0}}),
                                          RockPermModel::Coord({{  8.0, 14.0}})}};

  //Block 0 vertex coordinates in clockwise order
  RockPermModel::QuadBlock quadblock0_cw = {{RockPermModel::Coord({{ -3.0, 10.0}}),
                                             RockPermModel::Coord({{ -3.0, 12.0}}),
                                             RockPermModel::Coord({{  3.0, 12.0}}),
                                             RockPermModel::Coord({{  3.0, 10.0}})}};

  DLA::MatrixS<2,2,Real> Kref_block0 = {{0.57, 0.26}, {0.79, 1.38}};
  DLA::MatrixS<2,2,Real> Kref_block1 = {{2.35, 1.32}, {1.56, 4.98}};

  //Background permeability should be positive definite
  BOOST_CHECK_THROW( RockPermModel({{1.0, 1.0},{1.0, 1.0}}), AssertionException );

  //Block permeability cannot be scalar - will not be positive definite when assigned to matrix
  BOOST_CHECK_THROW( RockPermModel(Kref_mat, {quadblock0}, {Kxx}), AssertionException );

  //Block vertices should be in counter-clockwise order
  BOOST_CHECK_THROW( RockPermModel(Kref_mat, {quadblock0_cw}, {Kref_mat}), AssertionException );

  RockPermModel Kmodel(Kref_mat, {quadblock0, quadblock1}, {Kref_block0, Kref_block1});

  Real x = 0, y = 0, t = 0;
  Real a, b, c, d;
  Kmodel.permeability(x, y, t, a, b, c, d);
  BOOST_CHECK_CLOSE( a, Kxx, tol );
  BOOST_CHECK_CLOSE( b, Kxy, tol );
  BOOST_CHECK_CLOSE( c, Kyx, tol );
  BOOST_CHECK_CLOSE( d, Kyy, tol );

  x = -1.0, y = 11;
  a = 0, b = 0, c = 0, d = 0;
  Kmodel.permeability(x, y, t, a, b, c, d);
  BOOST_CHECK_CLOSE( a, Kref_block0(0,0), tol );
  BOOST_CHECK_CLOSE( b, Kref_block0(0,1), tol );
  BOOST_CHECK_CLOSE( c, Kref_block0(1,0), tol );
  BOOST_CHECK_CLOSE( d, Kref_block0(1,1), tol );

  x = 10.5, y = 11.0 - 1e-10;
  a = 0, b = 0, c = 0, d = 0;
  Kmodel.permeability(x, y, t, a, b, c, d);
  BOOST_CHECK_CLOSE( a, Kref_block1(0,0), tol );
  BOOST_CHECK_CLOSE( b, Kref_block1(0,1), tol );
  BOOST_CHECK_CLOSE( c, Kref_block1(1,0), tol );
  BOOST_CHECK_CLOSE( d, Kref_block1(1,1), tol );

  x = 7.5 + 1e-10, y = 1.0;
  a = 0, b = 0, c = 0, d = 0;
  Kmodel.permeability(x, y, t, a, b, c, d);
  BOOST_CHECK_CLOSE( a, Kref_block1(0,0), tol );
  BOOST_CHECK_CLOSE( b, Kref_block1(0,1), tol );
  BOOST_CHECK_CLOSE( c, Kref_block1(1,0), tol );
  BOOST_CHECK_CLOSE( d, Kref_block1(1,1), tol );
}

#if 0
//############################################################################//
BOOST_AUTO_TEST_SUITE( PermeabilityModel2D_RectBlock_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Constructor )
{
  const Real tol = 1e-13;
  Real Kxx = 1.18, Kxy = 0.54, Kyx = 0.21, Kyy = 2.37;

  std::vector<Real> block1_K = {0.57, 0.26, 0.79, 1.38}; //Kxx, Kxy, Kyx, Kyy
  std::vector<Real> block1_loc = {0.5, 1.8, -0.3, -0.1}; //xmin, xmax, ymin, ymax

  std::vector<Real> block2_K = {2.35, 1.32, 1.56, 4.98}; //Kxx, Kxy, Kyx, Kyy
  std::vector<Real> block2_loc = {-0.7, 0.1, 0.8, 1.2}; //xmin, xmax, ymin, ymax

  PyDict dict;
  dict[PermeabilityModel2D_RectBlock_Params::params.Kxx_ref] = Kxx;
  dict[PermeabilityModel2D_RectBlock_Params::params.Kxy_ref] = Kxy;
  dict[PermeabilityModel2D_RectBlock_Params::params.Kyx_ref] = Kyx;
  dict[PermeabilityModel2D_RectBlock_Params::params.Kyy_ref] = Kyy;

  dict[PermeabilityModel2D_RectBlock_Params::params.blocklist_Kxx] = {block1_K[0], block2_K[0]};
  dict[PermeabilityModel2D_RectBlock_Params::params.blocklist_Kxy] = {block1_K[1], block2_K[1]};
  dict[PermeabilityModel2D_RectBlock_Params::params.blocklist_Kyx] = {block1_K[2], block2_K[2]};
  dict[PermeabilityModel2D_RectBlock_Params::params.blocklist_Kyy] = {block1_K[3], block2_K[3]};

  dict[PermeabilityModel2D_RectBlock_Params::params.blocklist_xmin] = {block1_loc[0], block2_loc[0]};
  dict[PermeabilityModel2D_RectBlock_Params::params.blocklist_xmax] = {block1_loc[1], block2_loc[1]};
  dict[PermeabilityModel2D_RectBlock_Params::params.blocklist_ymin] = {block1_loc[2], block2_loc[2]};
  dict[PermeabilityModel2D_RectBlock_Params::params.blocklist_ymax] = {block1_loc[3], block2_loc[3]};

  //Check dictionary
  PermeabilityModel2D_RectBlock_Params::checkInputs(dict);

  Real x = 0.0, y = 0.0, t = 1.0;

  // PyDict constructor
  PermeabilityModel2D_RectBlock Kmodel(dict);
  Real a, b, c, d;
  Kmodel.permeability(x, y, t, a, b, c, d);
  BOOST_CHECK_CLOSE( a, Kxx, tol );
  BOOST_CHECK_CLOSE( b, Kxy, tol );
  BOOST_CHECK_CLOSE( c, Kyx, tol );
  BOOST_CHECK_CLOSE( d, Kyy, tol );

  x = 1.0, y = -0.2;
  a = 0, b = 0, c = 0, d = 0;
  Kmodel.permeability(x, y, t, a, b, c, d);
  BOOST_CHECK_CLOSE( a, block1_K[0], tol );
  BOOST_CHECK_CLOSE( b, block1_K[1], tol );
  BOOST_CHECK_CLOSE( c, block1_K[2], tol );
  BOOST_CHECK_CLOSE( d, block1_K[3], tol );

  x = -0.5, y = 1.0;
  a = 0, b = 0, c = 0, d = 0;
  Kmodel.permeability(x, y, t, a, b, c, d);
  BOOST_CHECK_CLOSE( a, block2_K[0], tol );
  BOOST_CHECK_CLOSE( b, block2_K[1], tol );
  BOOST_CHECK_CLOSE( c, block2_K[2], tol );
  BOOST_CHECK_CLOSE( d, block2_K[3], tol );


  // copy constructor
  PermeabilityModel2D_RectBlock Kmodel2(Kmodel);
  x = 0.0, y = 0.0;
  a = 0, b = 0, c = 0, d = 0;
  Kmodel2.permeability(x, y, t, a, b, c, d);
  BOOST_CHECK_CLOSE( a, Kxx, tol );
  BOOST_CHECK_CLOSE( b, Kxy, tol );
  BOOST_CHECK_CLOSE( c, Kyx, tol );
  BOOST_CHECK_CLOSE( d, Kyy, tol );

  x = 1.0, y = -0.2;
  a = 0, b = 0, c = 0, d = 0;
  Kmodel2.permeability(x, y, t, a, b, c, d);
  BOOST_CHECK_CLOSE( a, block1_K[0], tol );
  BOOST_CHECK_CLOSE( b, block1_K[1], tol );
  BOOST_CHECK_CLOSE( c, block1_K[2], tol );
  BOOST_CHECK_CLOSE( d, block1_K[3], tol );

  x = -0.5, y = 1.0;
  a = 0, b = 0, c = 0, d = 0;
  Kmodel2.permeability(x, y, t, a, b, c, d);
  BOOST_CHECK_CLOSE( a, block2_K[0], tol );
  BOOST_CHECK_CLOSE( b, block2_K[1], tol );
  BOOST_CHECK_CLOSE( c, block2_K[2], tol );
  BOOST_CHECK_CLOSE( d, block2_K[3], tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/PermeabilityModel2D_RectBlock_pattern.txt", true );

  Real Kxx = 1.18, Kxy = 0.54, Kyx = 0.21, Kyy = 2.37;

  std::vector<Real> block1_K = {0.57, 0.26, 0.79, 1.38}; //Kxx, Kxy, Kyx, Kyy
  std::vector<Real> block1_loc = {0.5, 1.8, -0.3, -0.1}; //xmin, xmax, ymin, ymax

  std::vector<Real> block2_K = {2.35, 1.32, 1.56, 4.98}; //Kxx, Kxy, Kyx, Kyy
  std::vector<Real> block2_loc = {-0.7, 0.1, 0.8, 1.2}; //xmin, xmax, ymin, ymax

  PyDict dict;
  dict[PermeabilityModel2D_RectBlock_Params::params.Kxx_ref] = Kxx;
  dict[PermeabilityModel2D_RectBlock_Params::params.Kxy_ref] = Kxy;
  dict[PermeabilityModel2D_RectBlock_Params::params.Kyx_ref] = Kyx;
  dict[PermeabilityModel2D_RectBlock_Params::params.Kyy_ref] = Kyy;

  dict[PermeabilityModel2D_RectBlock_Params::params.blocklist_Kxx] = {block1_K[0], block2_K[0]};
  dict[PermeabilityModel2D_RectBlock_Params::params.blocklist_Kxy] = {block1_K[1], block2_K[1]};
  dict[PermeabilityModel2D_RectBlock_Params::params.blocklist_Kyx] = {block1_K[2], block2_K[2]};
  dict[PermeabilityModel2D_RectBlock_Params::params.blocklist_Kyy] = {block1_K[3], block2_K[3]};

  dict[PermeabilityModel2D_RectBlock_Params::params.blocklist_xmin] = {block1_loc[0], block2_loc[0]};
  dict[PermeabilityModel2D_RectBlock_Params::params.blocklist_xmax] = {block1_loc[1], block2_loc[1]};
  dict[PermeabilityModel2D_RectBlock_Params::params.blocklist_ymin] = {block1_loc[2], block2_loc[2]};
  dict[PermeabilityModel2D_RectBlock_Params::params.blocklist_ymax] = {block1_loc[3], block2_loc[3]};

  //Check dictionary
  PermeabilityModel2D_RectBlock_Params::checkInputs(dict);

  // PyDict constructor
  PermeabilityModel2D_RectBlock Kmodel(dict);

  Kmodel.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()


//############################################################################//
BOOST_AUTO_TEST_SUITE( PermeabilityModel2D_CartTable_test_suite )

// create a simple Cartesian table file
void genCartTable( std::ofstream& file )
{
  int imax = 5;
  int jmax = 7;
  Real dx = 20.;
  Real dy = 30.;

  file << imax << " " << jmax << " " << dx << " " << dy << std::endl;
  for (int i = 0; i < imax; i++)
  {
    for (int j = 0; j < jmax; j++)
      file << i*jmax + j << " ";
    file << std::endl;
  }
  file.close();
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Constructor )
{
  std::string filename = "CartTable.txt";
  std::ofstream file(filename);
  genCartTable( file );

  PyDict d;
  d[PermeabilityModel2D_CartTable_Params::params.PermeabilityFile] = filename;

  // check dictionary
  PermeabilityModel2D_CartTable_Params::checkInputs(d);

  // PyDict constructor
  PermeabilityModel2D_CartTable permModel(d);

  // copy constructor
  PermeabilityModel2D_CartTable permModel2(permModel);

  // delete the written files
  std::remove(filename.c_str());
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Evaluations )
{
  //const Real tol = 1.e-13;
  std::string filename = "CartTable.txt";
  std::ofstream file(filename);
  genCartTable( file );

  PyDict d;
  d[PermeabilityModel2D_CartTable_Params::params.PermeabilityFile] = filename;

  // PyDict constructor
  PermeabilityModel2D_CartTable permModel(d);

  Real x, y, time;
  Real Kxx, Kxy, Kyx, Kyy;

  int imax = 5;
  int jmax = 7;
  Real dx = 20.;
  Real dy = 30.;
  for (int i = 0; i < imax-1; i++)
    for (int j = 0; j < jmax-1; j++)
    {
      x = i*dx + dx/2;
      y = j*dy + dy/2;

      permModel.permeability(x, y, time, Kxx, Kxy, Kyx, Kyy);
      BOOST_CHECK_EQUAL(i*jmax + j, Kxx);
      BOOST_CHECK_EQUAL(0.,         Kxy);
      BOOST_CHECK_EQUAL(0.,         Kyx);
      BOOST_CHECK_EQUAL(i*jmax + j, Kyy);
    }

  BOOST_CHECK_THROW(permModel.permeability(-1,dy,time, Kxx, Kxy, Kyx, Kyy), AssertionException);
  BOOST_CHECK_THROW(permModel.permeability(dx,-1,time, Kxx, Kxy, Kyx, Kyy), AssertionException);
  BOOST_CHECK_THROW(permModel.permeability(imax*dx+dx,dy,time, Kxx, Kxy, Kyx, Kyy), AssertionException);
  BOOST_CHECK_THROW(permModel.permeability(dx,jmax*dy+dy,time, Kxx, Kxy, Kyx, Kyy), AssertionException);

  // delete the written file
  std::remove(filename.c_str());
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/PermeabilityModel2D_CartTable_pattern.txt", true );

  std::string filename = "CartTable.txt";
  std::ofstream file(filename);
  genCartTable( file );

  PyDict d;
  d[PermeabilityModel2D_CartTable_Params::params.PermeabilityFile] = filename;

  // PyDict constructor
  PermeabilityModel2D_CartTable permModel(d);

  permModel.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  // delete the written file
  std::remove(filename.c_str());
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
