// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// SolutionFunction_BuckleyLeverett1D_btest
//
// test of BuckleyLeverett1DSolutionFunction classes

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverett.h"
#include "pde/PorousMedia/PDEBuckleyLeverett1D.h"
#include "pde/PorousMedia/SolutionFunction_BuckleyLeverett1D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( SolutionFunction_BuckleyLeverett1D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_Shock )
{
  Real tol = 1e-8;

  typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel_PowerLaw, CapillaryModel_None> TraitsModelClass;

  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;
  typedef SolutionFunction_BuckleyLeverett1D_Shock<QTypePrimitive_Sw, PDEClass> SolutionClass;

  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  CapillaryModel_None pc_model;
  const Real K = 0.25;

  PDEClass pde(phi, uT, kr_model, mu_w, mu_n, K, pc_model);

  Real SwL = 1.0;
  Real SwR = 0.1;
  Real xinit = 0.0;

  SolutionClass BLsol(pde, SwL, SwR, xinit);

  Real Sw_upstream_true = sqrt(249.0)/24.0 - 1.0/8.0;
  BOOST_CHECK_CLOSE( Sw_upstream_true, BLsol.Sw_upstream(), tol );

  Real x, t;

  tol = 1e-13;

  x = xinit-0.01, t = 2.0; //Left of xinit
  BOOST_CHECK_CLOSE( SwL, BLsol(x,t), tol );

  x = xinit, t = 5.0; //At xinit
  BOOST_CHECK_CLOSE( SwL, BLsol(x,t), tol );

  x = xinit+0.01, t = 0.0; //Right of shock
  BOOST_CHECK_CLOSE( SwR, BLsol(x,t), tol );

  x = 25, t = 15; //Right of shock at later time
  BOOST_CHECK_CLOSE( SwR, BLsol(x,t), tol );

  tol = 1e-6;

  x = 5, t = 10; //In rarefaction region
  BOOST_CHECK_CLOSE( 0.758870570003515, BLsol(x,t), tol );

  x = 35, t = 25; //In rarefaction region
  BOOST_CHECK_CLOSE( 0.571279683358786, BLsol(x,t), tol );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
