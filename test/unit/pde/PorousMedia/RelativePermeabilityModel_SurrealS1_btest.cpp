// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// RelativePermeabilityModel_Surreal1_btest
// testing of RelativePermeabilityModel classes
// with Surreal1 elements

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "Surreal/SurrealS.h"


using namespace std;
using namespace SANS;

namespace SANS
{
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( RelPermModel_PowerLaw_Surreal1_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Jacobian )
{
  const Real tol = 1.e-13;

  PyDict d1, d2;
  d1[RelPermModel_PowerLaw_Params::params.power] = 2.0;
  d2[RelPermModel_PowerLaw_Params::params.power] = 4.0;

  // Check dictionaries
  RelPermModel_PowerLaw_Params::checkInputs(d1);
  RelPermModel_PowerLaw_Params::checkInputs(d2);

  // PyDict constructor
  RelPermModel_PowerLaw kr_model_quadratic(d1);
  RelPermModel_PowerLaw kr_model_quartic(d2);

  SurrealS<1> Sw = 0.378; // saturation
  Sw.deriv() = 0.75;

  SurrealS<1> krw2 = pow(Sw,2); //krw for quadratic
  SurrealS<1> krw4 = pow(Sw,4); //krw for quartic

  Real krw2_Sw = 2*Sw.value()*Sw.deriv();
  Real krw4_Sw = 4*pow(Sw.value(),3)*Sw.deriv();

  SurrealS<1> kr2 = kr_model_quadratic.relativeperm(Sw);
  SurrealS<1> kr4 = kr_model_quartic.relativeperm(Sw);

  BOOST_CHECK_CLOSE( krw2.value(), kr2.value(), tol );
  BOOST_CHECK_CLOSE( krw4.value(), kr4.value(), tol );

  BOOST_CHECK_CLOSE( krw2_Sw, kr2.deriv(), tol );
  BOOST_CHECK_CLOSE( krw4_Sw, kr4.deriv(), tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Gradients )
{
  const Real tol = 1.e-13;

  PyDict d1, d2;
  d1[RelPermModel_PowerLaw_Params::params.power] = 2.0;
  d2[RelPermModel_PowerLaw_Params::params.power] = 4.0;

  // Check dictionaries
  RelPermModel_PowerLaw_Params::checkInputs(d1);
  RelPermModel_PowerLaw_Params::checkInputs(d2);

  // PyDict constructor
  RelPermModel_PowerLaw kr_model_quadratic(d1);
  RelPermModel_PowerLaw kr_model_quartic(d2);

  SurrealS<2> Sw = 0.378;   // saturation
  SurrealS<2> Swx = 0.75;   // saturation x-gradient
  SurrealS<2> Swy = 0.52;   // saturation y-gradient
  Sw.deriv(0) = Swx.value();
  Sw.deriv(1) = Swy.value();

  SurrealS<2> krw2 = pow(Sw,2); //krw for quadratic
  SurrealS<2> krw4 = pow(Sw,4); //krw for quartic

  //1D gradient
  SurrealS<2> krx2, krx4;
  kr_model_quadratic.relativepermGradient(Sw, Swx, krx2);
  kr_model_quartic  .relativepermGradient(Sw, Swx, krx4);

  BOOST_CHECK_CLOSE( krw2.deriv(0), krx2.value(), tol );
  BOOST_CHECK_CLOSE( krw4.deriv(0), krx4.value(), tol );

  //2D gradient
  SurrealS<2> kry2, kry4;
  kr_model_quadratic.relativepermGradient(Sw, Swx, Swy, krx2, kry2);
  kr_model_quartic  .relativepermGradient(Sw, Swx, Swy, krx4, kry4);

  BOOST_CHECK_CLOSE( krw2.deriv(0), krx2.value(), tol );
  BOOST_CHECK_CLOSE( krw4.deriv(0), krx4.value(), tol );

  BOOST_CHECK_CLOSE( krw2.deriv(1), kry2.value(), tol );
  BOOST_CHECK_CLOSE( krw4.deriv(1), kry4.value(), tol );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
