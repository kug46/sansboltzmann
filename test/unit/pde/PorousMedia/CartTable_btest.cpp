// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// CartTable_btest
// testing of CartTable class

#include <fstream>
#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/PorousMedia/CartTable.h"
#include "tools/SANSException.h"

using namespace std;
using namespace SANS;

namespace SANS
{

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( CartTable_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Interpolation )
{
  int imax = 5;
  int jmax = 7;
  Real dx = 20.;
  Real dy = 30.;
  std::string filename = "CartTable.txt";

  // Create a simple Cartesian table file
  {
    std::ofstream file(filename);
    file << imax << " " << jmax << " " << dx << " " << dy << std::endl;
    for (int i = 0; i < imax; i++)
    {
      for (int j = 0; j < jmax; j++)
        file << i*jmax + j << " ";
      file << std::endl;
    }
  }

  // Create the table and check the lookup
  CartTable<2> table(filename);
  for (int i = 0; i < imax-1; i++)
    for (int j = 0; j < jmax-1; j++)
    {
      Real x = i*dx+dx/2;
      Real y = j*dy+dy/2;

      Real c = table(x,y);
      BOOST_CHECK_EQUAL(i*jmax + j, c);
    }

  BOOST_CHECK_THROW(table(-1,dy), AssertionException);
  BOOST_CHECK_THROW(table(dx,-1), AssertionException);
  BOOST_CHECK_THROW(table(imax*dx+dx,dy), AssertionException);
  BOOST_CHECK_THROW(table(dx,jmax*dy+dy), AssertionException);

  //Remove the test file from the file system
  std::remove(filename.c_str());
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Interpolation_1x1 )
{
  int imax = 1;
  int jmax = 1;
  Real dx = 20.;
  Real dy = 30.;
  std::string filename = "CartTable.txt";

  // Create a simple Cartesian table file
  {
    std::ofstream file(filename);
    file << imax << " " << jmax << " " << dx << " " << dy << std::endl;
    file << 0.1237 << std::endl;
  }

  // Create the table and check the lookup
  CartTable<2> table(filename);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
    {
      Real x = (i+1)*dx/4;
      Real y = (j+1)*dy/4;

      Real c = table(x,y);
      BOOST_CHECK_EQUAL(0.1237, c);
    }

  BOOST_CHECK_THROW(table(-1,dy), AssertionException);
  BOOST_CHECK_THROW(table(dx,-1), AssertionException);
  BOOST_CHECK_THROW(table(2*dx,dy), AssertionException);
  BOOST_CHECK_THROW(table(dx,2*dy), AssertionException);

  //Remove the test file from the file system
  std::remove(filename.c_str());
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
