// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDETwoPhase_ArtificialViscosity2D_btest
//
// testing of 2D two phase PDE class with artificial viscosity

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "pde/PorousMedia/Q2DPrimitive_pnSw.h"
#include "pde/PorousMedia/TraitsTwoPhaseArtificialViscosity.h"
#include "pde/PorousMedia/PDETwoPhase_ArtificialViscosity2D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/PermeabilityModel2D.h"
#include "pde/PorousMedia/CapillaryModel.h"

//Explicitly instantiate the class so coverage information is correct
namespace SANS
{

template class TraitsSizeTwoPhase<PhysD2>;
template class TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_Comp,
                                   RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                                   PermeabilityModel2D_Constant, CapillaryModel_Linear>;

typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_Comp,
                            RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                            PermeabilityModel2D_Constant, CapillaryModel_Linear> TraitsModelClass;

template class PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhase, TraitsModelClass>;
template class PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelClass>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDETwoPhase_ArtificialViscosity2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test_pnSw )
{
  {
    typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

    BOOST_CHECK( PDEClass::D == 2 );
    BOOST_CHECK( PDEClass::N == 2 );
  }

  {
    typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelClass> PDEClass;

    BOOST_CHECK( PDEClass::D == 2 );
    BOOST_CHECK( PDEClass::N == 3 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_pnSw )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelClass> PDEClass;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  RockPermModel K(1.1, 0.6, 0.2, 1.5);
  CapillaryModel pc(5);

  const int order = 1;
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == false );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasSourceTrace() == false );
  BOOST_CHECK( pde.hasSourceLiftedQuantity() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  //Copy constructor
  PDEClass pde2(pde);
  BOOST_CHECK( pde2.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde2.hasFluxAdvective() == false );
  BOOST_CHECK( pde2.hasFluxViscous() == true );
  BOOST_CHECK( pde2.hasSource() == false );
  BOOST_CHECK( pde2.hasSourceTrace() == false );
  BOOST_CHECK( pde.hasSourceLiftedQuantity() == false );
  BOOST_CHECK( pde2.hasForcingFunction() == false );
  BOOST_CHECK( pde2.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde2.needsSolutionGradientforSource() == false );

  // Set up PyDicts
  PyDict fixedwellpressure;
  fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.pB] = 2350.0;
  fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.Rwellbore] = 0.5;
  fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.nParam] = 1;
  fixedwellpressure[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureOutflow;

  PyDict well;
  well[SourceTwoPhase2DParam::params.Source] = fixedwellpressure;
  well[SourceTwoPhase2DParam::params.xcentroid] = 10.0;
  well[SourceTwoPhase2DParam::params.ycentroid] = 15.0;
  well[SourceTwoPhase2DParam::params.R] = 5.0;
  well[SourceTwoPhase2DParam::params.Tmin] = 1.0;
  well[SourceTwoPhase2DParam::params.Tmax] = 3.0;
  well[SourceTwoPhase2DParam::params.smoothLr] = 1.0;
  well[SourceTwoPhase2DParam::params.smoothT] = 0.0;
  well[SourceTwoPhase2DParam::params.visibleAngle] = 2*PI;

  PyDict sourceList;
  sourceList["well"] = well;
  SourceTwoPhase2DListParam::checkInputs(sourceList);

  //Constructor with source terms
  PDEClass pde3(order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc, sourceList);

  BOOST_CHECK( pde3.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde3.hasFluxAdvective() == false );
  BOOST_CHECK( pde3.hasFluxViscous() == true );
  BOOST_CHECK( pde3.hasSource() == true );
  BOOST_CHECK( pde3.hasSourceTrace() == false );
  BOOST_CHECK( pde3.hasSourceLiftedQuantity() == false );
  BOOST_CHECK( pde3.hasForcingFunction() == false );
  BOOST_CHECK( pde3.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde3.needsSolutionGradientforSource() == false );

  //Copy constructor
  PDEClass pde4(pde3);
  BOOST_CHECK( pde4.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde4.hasFluxAdvective() == false );
  BOOST_CHECK( pde4.hasFluxViscous() == true );
  BOOST_CHECK( pde4.hasSource() == true );
  BOOST_CHECK( pde4.hasSourceTrace() == false );
  BOOST_CHECK( pde4.hasSourceLiftedQuantity() == false );
  BOOST_CHECK( pde4.hasForcingFunction() == false );
  BOOST_CHECK( pde4.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde4.needsSolutionGradientforSource() == false );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setDOFFrom )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;

  Real tol = 1e-13;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  RockPermModel K(1.1, 0.6, 0.2, 1.5);
  CapillaryModel pc(5);

  const int order = 1;
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  Real pn = 2534.8;
  Real Sw = 0.784;

  // set
  Real qDataPrim[2] = {pn,Sw};
  string qNamePrim[2] = {"pn","Sw"};
  ArrayQ qTrue = {pn,Sw};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 2 );
  BOOST_CHECK_CLOSE( qTrue[0], q[0], tol );
  BOOST_CHECK_CLOSE( qTrue[1], q[1], tol );

  PressureNonWet_SaturationWet<Real> qdata1;
  qdata1.pn = pn; qdata1.Sw = Sw;
  q = 0;
  pde.setDOFFrom( q, qdata1 );
  BOOST_CHECK_CLOSE( qTrue[0], q[0], tol );
  BOOST_CHECK_CLOSE( qTrue[1], q[1], tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux_pnSw )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelClass> PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> MatrixSym;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  const Real pref = 1.47;

  const Real rhow_ref = 62.4;
  const Real rhon_ref = 52.1;
  const Real Cw = 0.03;
  const Real Cn = 0.05;

  const Real phi_ref = 0.3;
  const Real Cr = 0.06;

  const Real muw = 1.0, mun = 2.0;
  const Real pcmax = 5.0;

  DensityModel rhow_model(rhow_ref, Cw, pref);
  DensityModel rhon_model(rhon_ref, Cn, pref);

  PorosityModel phi_model(phi_ref, Cr, pref);

  RelPermModel krw_model(2);
  RelPermModel krn_model(2);

  ViscModel muw_model(muw);
  ViscModel mun_model(mun);

  Real Kxx = 1.1, Kxy = 0.6, Kyx = 0.2, Kyy = 1.5;
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc_model(pcmax);

  const int order = 1;
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(order, hasSpaceTimeDiffusion, rhow_model, rhon_model, phi_model,
               krw_model, krn_model, muw_model, mun_model, K, pc_model);

  // Static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 3 );

  // Flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == false );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasSourceTrace() == false );
  BOOST_CHECK( pde.hasSourceLiftedQuantity() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  BOOST_CHECK_EQUAL( pde.nMonitor(), 3 );

  // Function tests
  Real x = 0, y = 0, time = 0;   // not actually used in functions
  Real nx = 0.35, ny = -0.26;

  Real SwL = 0.375, SwR = 0.418;
  Real SwxL = -0.63, SwxR = -0.37;
  Real SwyL = 0.27, SwyR = 0.46;
  Real pnL = 25.34, pnR = 31.86;
  Real pnxL = -0.52, pnxR = -0.29;
  Real pnyL = 0.96, pnyR = 0.81;

  Real nuL = 0.786, nuR = 1.031;
  Real nuxL = -0.22, nuxR = 0.13;
  Real nuyL =  0.82, nuyR = 0.46;

  Real SnL = 1.0 - SwL;
  Real SnR = 1.0 - SwR;
  Real pcL = pcmax*SnL;
  Real pcR = pcmax*SnR;
  Real pwL = pnL - pcL;
  Real pwR = pnR - pcR;
  Real pwxL = pnxL - pcmax*(-SwxL);
  Real pwxR = pnxR - pcmax*(-SwxR);
  Real pwyL = pnyL - pcmax*(-SwyL);
  Real pwyR = pnyR - pcmax*(-SwyR);

  Real krwL = pow(SwL,2);
  Real krwR = pow(SwR,2);

  Real krnL = pow(SnL,2);
  Real krnR = pow(SnR,2);

  Real krw_SwL = 2*SwL;
  Real krw_SwR = 2*SwR;

  Real krn_SwL = -2*SnL;
  Real krn_SwR = -2*SnR;

  Real rhowL = rhow_ref*exp(Cw*(pwL - pref));
  Real rhowR = rhow_ref*exp(Cw*(pwR - pref));

  Real rhonL = rhon_ref*exp(Cn*(pnL - pref));
  Real rhonR = rhon_ref*exp(Cn*(pnR - pref));

  Real phiL  = phi_ref *exp(Cr*(pnL - pref));

  Real rhow_pnL = Cw*rhowL;
  Real rhow_SwL = Cw*rhowL*pcmax;
  Real rhon_pnL = Cn*rhonL;
  Real phi_pnL = Cr*phiL;

  Real LwL = krwL/muw;
  Real LwR = krwR/muw;

  Real LnL = krnL/mun;
  Real LnR = krnR/mun;

  Real Lw_SwL = krw_SwL / muw;
  Real Lw_SwR = krw_SwR / muw;

  Real Ln_SwL = krn_SwL / mun;
  Real Ln_SwR = krn_SwR / mun;

  MatrixSym H = {{0.27},{0.12,0.63}};
  MatrixSym paramL = log(H);
  MatrixSym paramR = 0.5*log(H);

  // Set
  Real qLDataPrim[3] = {pnL, SwL, nuL};
  Real qRDataPrim[3] = {pnR, SwR, nuR};
  string qNamePrim[3] = {"pn","Sw","nu"};
  ArrayQ qLTrue = {pnL, SwL, nuL};
  ArrayQ qRTrue = {pnR, SwR, nuR};
  ArrayQ qL, qR;
  pde.setDOFFrom( qL, qLDataPrim, qNamePrim, 3 );
  pde.setDOFFrom( qR, qRDataPrim, qNamePrim, 3 );
  SANS_CHECK_CLOSE( qLTrue[0], qL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( qLTrue[1], qL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( qRTrue[0], qR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( qRTrue[1], qR[1], small_tol, close_tol );

  qL[2] = nuL;
  qR[2] = nuR;

  Real Sw_test, Sn_test, pw_test, pn_test;
  pde.variableInterpreter().eval(qL, pw_test, pn_test, Sw_test, Sn_test);
  SANS_CHECK_CLOSE( pwL, pw_test, small_tol, close_tol );
  SANS_CHECK_CLOSE( pnL, pn_test, small_tol, close_tol );
  SANS_CHECK_CLOSE( SwL, Sw_test, small_tol, close_tol );
  SANS_CHECK_CLOSE( SnL, Sn_test, small_tol, close_tol );

  // Master state
  ArrayQ ucons_true, ucons;
  ucons_true[0] = rhowL*phiL*SwL;
  ucons_true[1] = rhonL*phiL*SnL;
  ucons_true[2] = 0.0;

  ucons = 0;
  pde.masterState( paramL, x, y, time, qL, ucons );
  SANS_CHECK_CLOSE( ucons_true[0], ucons[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( ucons_true[1], ucons[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( ucons_true[2], ucons[2], small_tol, close_tol );

  // Master state jacobian
  MatrixQ fcons_jac_true, fcons_jac;
  fcons_jac_true(0,0) = (phi_pnL*rhowL + phiL*rhow_pnL)*SwL;
  fcons_jac_true(0,1) = (                phiL*rhow_SwL)*SwL + phiL*rhowL;
  fcons_jac_true(0,2) = 0.0;
  fcons_jac_true(1,0) = (phi_pnL*rhonL + phiL*rhon_pnL)*SnL;
  fcons_jac_true(1,1) = phiL*rhonL*(-1);
  fcons_jac_true(1,2) = 0.0;
  fcons_jac_true(2,0) = 0.0;
  fcons_jac_true(2,1) = 0.0;
  fcons_jac_true(2,2) = 0.0;

  fcons_jac = 0;
  pde.jacobianMasterState( paramL, x, y, time, qL, fcons_jac );
  SANS_CHECK_CLOSE( fcons_jac_true(0,0), fcons_jac(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_jac_true(0,1), fcons_jac(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_jac_true(0,2), fcons_jac(0,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_jac_true(1,0), fcons_jac(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_jac_true(1,1), fcons_jac(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_jac_true(1,2), fcons_jac(1,2), small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_jac_true(2,0), fcons_jac(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_jac_true(2,1), fcons_jac(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_jac_true(2,2), fcons_jac(2,2), small_tol, close_tol );

  // Conservative flux
  ArrayQ fcons_true, fcons;
  fcons_true[0] = rhowL*phiL*SwL;
  fcons_true[1] = rhonL*phiL*SnL;
  fcons_true[2] = 0.0;

  fcons = 0;
  pde.fluxAdvectiveTime( paramL, x, y, time, qL, fcons );
  SANS_CHECK_CLOSE( fcons_true[0], fcons[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_true[1], fcons[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_true[2], fcons[2], small_tol, close_tol );


  // Advective flux
  ArrayQ fxadv_true = 0.0, fyadv_true = 0.0;
  ArrayQ fxadv = 0.0, fyadv = 0.0;
  pde.fluxAdvective( paramL, x, y, time, qL, fxadv, fyadv );
  SANS_CHECK_CLOSE( fxadv_true[0], fxadv[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fxadv_true[1], fxadv[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( fxadv_true[2], fxadv[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( fyadv_true[0], fyadv[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fyadv_true[1], fyadv[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( fyadv_true[2], fyadv[2], small_tol, close_tol );

  ArrayQ fn_adv = 0;
  pde.fluxAdvectiveUpwind( paramL, x, y, time, qL, qR, nx, ny, fn_adv );
  SANS_CHECK_CLOSE( 0.0, fn_adv[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, fn_adv[1], small_tol, close_tol );

  // Jacobian of advective flux
  MatrixQ fxadv_u = 0, fyadv_u = 0.0;
  BOOST_CHECK_THROW( pde.jacobianFluxAdvective( paramL, x, y, time, qL, fxadv_u, fyadv_u ), SANSException );

  // Viscous flux
  ArrayQ fxvisc_trueL, fyvisc_trueL;
  ArrayQ fxvisc = 0, fyvisc = 0;
  fxvisc_trueL[0] = -rhowL*LwL*(Kxx*pwxL + Kxy*pwyL);
  fxvisc_trueL[1] = -rhonL*LnL*(Kxx*pnxL + Kxy*pnyL);
  fxvisc_trueL[2] = 0.0;
  fyvisc_trueL[0] = -rhowL*LwL*(Kyx*pwxL + Kyy*pwyL);
  fyvisc_trueL[1] = -rhonL*LnL*(Kyx*pnxL + Kyy*pnyL);
  fyvisc_trueL[2] = 0.0;

  //Add artificial flux
  Real nuL_mod = smoothmax(nuL, 0.0, 40.0);
  fxvisc_trueL[0] -=  rhowL*nuL_mod*SwxL;
  fxvisc_trueL[1] -= -rhonL*nuL_mod*SwxL;
  fyvisc_trueL[0] -=  rhowL*nuL_mod*SwyL;
  fyvisc_trueL[1] -= -rhonL*nuL_mod*SwyL;

  ArrayQ qxL = {pnxL, SwxL, nuxL};
  ArrayQ qyL = {pnyL, SwyL, nuyL};
  pde.fluxViscous( paramL, x, y, time, qL, qxL, qyL, fxvisc, fyvisc );
  SANS_CHECK_CLOSE( fxvisc_trueL[0], fxvisc[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fxvisc_trueL[1], fxvisc[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( fxvisc_trueL[2], fxvisc[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( fyvisc_trueL[0], fyvisc[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fyvisc_trueL[1], fyvisc[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( fyvisc_trueL[2], fyvisc[2], small_tol, close_tol );


  ArrayQ qxR = {pnxR, SwxR, nuxR};
  ArrayQ qyR = {pnyR, SwyR, nuyR};

  ArrayQ fxvisc_trueR, fyvisc_trueR;
  fxvisc_trueR[0] = -rhowR*LwR*(Kxx*pwxR + Kxy*pwyR);
  fxvisc_trueR[1] = -rhonR*LnR*(Kxx*pnxR + Kxy*pnyR);
  fxvisc_trueR[2] = 0.0;
  fyvisc_trueR[0] = -rhowR*LwR*(Kyx*pwxR + Kyy*pwyR);
  fyvisc_trueR[1] = -rhonR*LnR*(Kyx*pnxR + Kyy*pnyR);
  fyvisc_trueR[2] = 0.0;

  //Add artificial flux
  Real nuR_mod = smoothmax(nuR, 0.0, 40.0);
  fxvisc_trueR[0] -=  rhowR*nuR_mod*SwxR;
  fxvisc_trueR[1] -= -rhonR*nuR_mod*SwxR;
  fyvisc_trueR[0] -=  rhowR*nuR_mod*SwyR;
  fyvisc_trueR[1] -= -rhonR*nuR_mod*SwyR;

  //Average flux
  ArrayQ fn_true = 0.5*(fxvisc_trueL + fxvisc_trueR)*nx + 0.5*(fyvisc_trueL + fyvisc_trueR)*ny;

  //Add upwinding
  Real KgradpwL = (Kxx*pwxL + Kxy*pwyL)*nx + (Kyx*pwxL + Kyy*pwyL)*ny;
  Real KgradpwR = (Kxx*pwxR + Kxy*pwyR)*nx + (Kyx*pwxR + Kyy*pwyR)*ny;

  Real KgradpnL = (Kxx*pnxL + Kxy*pnyL)*nx + (Kyx*pnxL + Kyy*pnyL)*ny;
  Real KgradpnR = (Kxx*pnxR + Kxy*pnyR)*nx + (Kyx*pnxR + Kyy*pnyR)*ny;

  Real cL = (Lw_SwL*LnL*KgradpwL - LwL*Ln_SwL*KgradpnL) / (LwL + LnL);
  Real cR = (Lw_SwR*LnR*KgradpwR - LwR*Ln_SwR*KgradpnR) / (LwR + LnR);

  const Real eps = 1e-3;
  const Real alpha = 40;

  Real rw_cL = rhowL*cL;
  Real rw_cR = rhowR*cR;
  Real rn_cL = rhonL*cL;
  Real rn_cR = rhonR*cR;
  Real cw_max = smoothmax( smoothabs0(rw_cL, eps), smoothabs0(rw_cR, eps), alpha );
  Real cn_max = smoothmax( smoothabs0(rn_cL, eps), smoothabs0(rn_cR, eps), alpha );

  fn_true(0) += 0.5*cw_max*(SwL - SwR);
  fn_true(1) -= 0.5*cn_max*(SwL - SwR);

  ArrayQ fn = 0.0;
  pde.fluxViscous( paramL, paramR, x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );
  SANS_CHECK_CLOSE( fn_true[0], fn[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fn_true[1], fn[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( fn_true[2], fn[2], small_tol, close_tol );

  //Viscous diffusion matrix
  MatrixQ kxx = 0, kxy = 0, kyx = 0, kyy = 0;
  BOOST_CHECK_THROW( pde.diffusionViscous( paramL, x, y, time, qL, qxL, qyL, kxx, kxy, kyx, kyy ), DeveloperException );

  //Maximum artificial viscosity
  Real KgradpwxL = (Kxx*pwxL + Kxy*pwyL);
  Real KgradpwyL = (Kyx*pwxL + Kyy*pwyL);

  Real KgradpnxL = (Kxx*pnxL + Kxy*pnyL);
  Real KgradpnyL = (Kyx*pnxL + Kyy*pnyL);

  Real vx_true = -(Lw_SwL*LnL*KgradpwxL - LwL*Ln_SwL*KgradpnxL) / (LwL + LnL);
  Real vy_true = -(Lw_SwL*LnL*KgradpwyL - LwL*Ln_SwL*KgradpnyL) / (LwL + LnL);

  Real vx = 0, vy = 0;
  pde.characteristicVelocity(x, y, time, qL, qxL, qyL, vx, vy);
  SANS_CHECK_CLOSE( vx_true, vx, small_tol, close_tol );
  SANS_CHECK_CLOSE( vy_true, vy, small_tol, close_tol );

  ArrayQ vx_single_true = {-KgradpwxL/muw, -KgradpnxL/mun, 0.0};
  ArrayQ vy_single_true = {-KgradpwyL/muw, -KgradpnyL/mun, 0.0};

  ArrayQ vx_single = 0, vy_single = 0;
  pde.characteristicVelocitySinglePhase(x, y, time, qL, qxL, qyL, vx_single, vy_single);
  SANS_CHECK_CLOSE( vx_single_true[0], vx_single[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( vx_single_true[1], vx_single[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( vy_single_true[0], vy_single[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( vy_single_true[1], vy_single[1], small_tol, close_tol );

  MatrixSym Hsq = H*H;
  Real vw_h = sqrt( vx_single[0]*(Hsq(0,0)*vx_single[0] + Hsq(0,1)*vy_single[0])
                  + vy_single[0]*(Hsq(1,0)*vx_single[0] + Hsq(1,1)*vy_single[0]) );
  Real vn_h = sqrt( vx_single[1]*(Hsq(0,0)*vx_single[1] + Hsq(0,1)*vy_single[1])
                  + vy_single[1]*(Hsq(1,0)*vx_single[1] + Hsq(1,1)*vy_single[1]) );
  Real v_h = smoothmax(vw_h, vn_h, alpha);
  Real Pe = 2.0;
  Real nu_maxTrue = (v_h / order) / Pe;

  Real nu_max;
  pde.artViscMax(paramL, x, y, time, qL, qxL, qyL, nu_max);
  SANS_CHECK_CLOSE( nu_maxTrue, nu_max, small_tol, close_tol );

  ArrayQ sourceTrue = 0;
  ArrayQ source = 0;
  pde.source(paramL, x, y, time, qL, qxL, qyL, source);
  SANS_CHECK_CLOSE( sourceTrue[0], source[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( sourceTrue[1], source[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( sourceTrue[2], source[2], small_tol, close_tol );

  ArrayQ sourceL = 0, sourceR = 0;
  pde.sourceTrace(paramL, x, y, paramL, x, y, time, qL, qxL, qyL, qR, qxR, qyR, sourceL, sourceR);
  SANS_CHECK_CLOSE( sourceTrue[0], sourceL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( sourceTrue[1], sourceL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( sourceTrue[2], sourceL[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( sourceTrue[0], sourceR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( sourceTrue[1], sourceR[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( sourceTrue[2], sourceR[2], small_tol, close_tol );

  Real liftedQuantityTrue = 0.0;
  Real liftedQuantity = 0.0;
  pde.sourceLiftedQuantity(paramL, x, y, paramL, x, y, time, qL, qR, liftedQuantity);
  SANS_CHECK_CLOSE( liftedQuantityTrue, liftedQuantity, small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux_pnSw_ST )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelClass> PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> MatrixSym;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  const Real pref = 1.47;

  const Real rhow_ref = 62.4;
  const Real rhon_ref = 52.1;
  const Real Cw = 0.03;
  const Real Cn = 0.05;

  const Real phi_ref = 0.3;
  const Real Cr = 0.06;

  const Real muw = 1.0, mun = 2.0;
  const Real pcmax = 5.0;

  DensityModel rhow_model(rhow_ref, Cw, pref);
  DensityModel rhon_model(rhon_ref, Cn, pref);

  PorosityModel phi_model(phi_ref, Cr, pref);

  RelPermModel krw_model(2);
  RelPermModel krn_model(2);

  ViscModel muw_model(muw);
  ViscModel mun_model(mun);

  Real Kxx = 1.1, Kxy = 0.6, Kyx = 0.2, Kyy = 1.5;
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc_model(pcmax);

  const int order = 1;
  bool hasSpaceTimeDiffusion = true;
  PDEClass pde(order, hasSpaceTimeDiffusion, rhow_model, rhon_model, phi_model,
               krw_model, krn_model, muw_model, mun_model, K, pc_model);

  // Static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 3 );

  // Flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == false );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasSourceTrace() == false );
  BOOST_CHECK( pde.hasSourceLiftedQuantity() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  BOOST_CHECK_EQUAL( pde.nMonitor(), 3 );

  // Function tests
  Real x = 0, y = 0, time = 0; // not actually used in functions
  Real nx = 0.35, ny = -0.26, nt = 0.68;

  Real SwL = 0.375, SwR = 0.418;
  Real SwxL = -0.63, SwxR = -0.37;
  Real SwyL = 0.27, SwyR = 0.46;
  Real SwtL = 0.39, SwtR = -0.28;

  Real pnL = 25.34, pnR = 31.86;
  Real pnxL = -0.52, pnxR = -0.29;
  Real pnyL = 0.96, pnyR = 0.81;
  Real pntL = 1.06, pntR = 2.64;

  Real nuL = 0.786, nuR = 1.031;
  Real nuxL = -0.22, nuxR = 0.13;
  Real nuyL =  0.82, nuyR = 0.46;
  Real nutL =  0.57, nutR = -0.91;

  Real SnL = 1.0 - SwL;
  Real SnR = 1.0 - SwR;
  Real pcL = pcmax*SnL;
  Real pcR = pcmax*SnR;
  Real pwL = pnL - pcL;
  Real pwR = pnR - pcR;
  Real pwxL = pnxL - pcmax*(-SwxL);
  Real pwxR = pnxR - pcmax*(-SwxR);
  Real pwyL = pnyL - pcmax*(-SwyL);
  Real pwyR = pnyR - pcmax*(-SwyR);

  Real krwL = pow(SwL,2);
  Real krwR = pow(SwR,2);

  Real krnL = pow(SnL,2);
  Real krnR = pow(SnR,2);

  Real krw_SwL = 2*SwL;
  Real krw_SwR = 2*SwR;

  Real krn_SwL = -2*SnL;
  Real krn_SwR = -2*SnR;

  Real rhowL = rhow_ref*exp(Cw*(pwL - pref));
  Real rhowR = rhow_ref*exp(Cw*(pwR - pref));

  Real rhonL = rhon_ref*exp(Cn*(pnL - pref));
  Real rhonR = rhon_ref*exp(Cn*(pnR - pref));

  Real LwL = krwL/muw;
  Real LwR = krwR/muw;

  Real LnL = krnL/mun;
  Real LnR = krnR/mun;

  Real Lw_SwL = krw_SwL / muw;
  Real Lw_SwR = krw_SwR / muw;

  Real Ln_SwL = krn_SwL / mun;
  Real Ln_SwR = krn_SwR / mun;

  MatrixSym H = {{0.27},{0.12,0.63}};
  MatrixSym paramL = log(H);
  MatrixSym paramR = 0.5*log(H);

  // Set
  Real qLDataPrim[3] = {pnL, SwL, nuL};
  Real qRDataPrim[3] = {pnR, SwR, nuR};
  string qNamePrim[3] = {"pn","Sw","nu"};
  ArrayQ qLTrue = {pnL, SwL, nuL};
  ArrayQ qRTrue = {pnR, SwR, nuR};
  ArrayQ qL, qR;
  pde.setDOFFrom( qL, qLDataPrim, qNamePrim, 3 );
  pde.setDOFFrom( qR, qRDataPrim, qNamePrim, 3 );
  SANS_CHECK_CLOSE( qLTrue[0], qL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( qLTrue[1], qL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( qRTrue[0], qR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( qRTrue[1], qR[1], small_tol, close_tol );

  qL[2] = nuL;
  qR[2] = nuR;

  // Viscous flux
  ArrayQ fxvisc_trueL, fyvisc_trueL, ftvisc_trueL;
  ArrayQ fxvisc = 0, fyvisc = 0, ftvisc = 0;
  fxvisc_trueL[0] = -rhowL*LwL*(Kxx*pwxL + Kxy*pwyL);
  fxvisc_trueL[1] = -rhonL*LnL*(Kxx*pnxL + Kxy*pnyL);
  fxvisc_trueL[2] = 0.0;
  fyvisc_trueL[0] = -rhowL*LwL*(Kyx*pwxL + Kyy*pwyL);
  fyvisc_trueL[1] = -rhonL*LnL*(Kyx*pnxL + Kyy*pnyL);
  fyvisc_trueL[2] = 0.0;
  ftvisc_trueL = 0;

  //Add artificial flux
  Real nuL_mod = smoothmax(nuL, 0.0, 40.0);
  fxvisc_trueL[0] -=  rhowL*nuL_mod*SwxL;
  fxvisc_trueL[1] -= -rhonL*nuL_mod*SwxL;
  fyvisc_trueL[0] -=  rhowL*nuL_mod*SwyL;
  fyvisc_trueL[1] -= -rhonL*nuL_mod*SwyL;
  ftvisc_trueL[0] -=  rhowL*nuL_mod*SwtL;
  ftvisc_trueL[1] -= -rhonL*nuL_mod*SwtL;

  ArrayQ qxL = {pnxL, SwxL, nuxL};
  ArrayQ qyL = {pnyL, SwyL, nuyL};
  ArrayQ qtL = {pntL, SwtL, nutL};
  pde.fluxViscousSpaceTime( paramL, x, y, time, qL, qxL, qyL, qtL, fxvisc, fyvisc, ftvisc );
  SANS_CHECK_CLOSE( fxvisc_trueL[0], fxvisc[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fxvisc_trueL[1], fxvisc[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( fxvisc_trueL[2], fxvisc[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( fyvisc_trueL[0], fyvisc[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fyvisc_trueL[1], fyvisc[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( fyvisc_trueL[2], fyvisc[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( ftvisc_trueL[0], ftvisc[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( ftvisc_trueL[1], ftvisc[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( ftvisc_trueL[2], ftvisc[2], small_tol, close_tol );

  ArrayQ qxR = {pnxR, SwxR, nuxR};
  ArrayQ qyR = {pnyR, SwyR, nuyR};
  ArrayQ qtR = {pntR, SwtR, nutR};

  ArrayQ fxvisc_trueR, fyvisc_trueR, ftvisc_trueR;
  fxvisc_trueR[0] = -rhowR*LwR*(Kxx*pwxR + Kxy*pwyR);
  fxvisc_trueR[1] = -rhonR*LnR*(Kxx*pnxR + Kxy*pnyR);
  fxvisc_trueR[2] = 0.0;
  fyvisc_trueR[0] = -rhowR*LwR*(Kyx*pwxR + Kyy*pwyR);
  fyvisc_trueR[1] = -rhonR*LnR*(Kyx*pnxR + Kyy*pnyR);
  fyvisc_trueR[2] = 0.0;
  ftvisc_trueR = 0.0;

  //Add artificial flux
  Real nuR_mod = smoothmax(nuR, 0.0, 40.0);
  fxvisc_trueR[0] -=  rhowR*nuR_mod*SwxR;
  fxvisc_trueR[1] -= -rhonR*nuR_mod*SwxR;
  fyvisc_trueR[0] -=  rhowR*nuR_mod*SwyR;
  fyvisc_trueR[1] -= -rhonR*nuR_mod*SwyR;
  ftvisc_trueR[0] -=  rhowR*nuR_mod*SwtR;
  ftvisc_trueR[1] -= -rhonR*nuR_mod*SwtR;

  //Average flux
  ArrayQ fn_true = 0.5*(fxvisc_trueL + fxvisc_trueR)*nx + 0.5*(fyvisc_trueL + fyvisc_trueR)*ny + 0.5*(ftvisc_trueL + ftvisc_trueR)*nt;

  //Add upwinding
  Real KgradpwL = (Kxx*pwxL + Kxy*pwyL)*nx + (Kyx*pwxL + Kyy*pwyL)*ny;
  Real KgradpwR = (Kxx*pwxR + Kxy*pwyR)*nx + (Kyx*pwxR + Kyy*pwyR)*ny;

  Real KgradpnL = (Kxx*pnxL + Kxy*pnyL)*nx + (Kyx*pnxL + Kyy*pnyL)*ny;
  Real KgradpnR = (Kxx*pnxR + Kxy*pnyR)*nx + (Kyx*pnxR + Kyy*pnyR)*ny;

  Real cL = (Lw_SwL*LnL*KgradpwL - LwL*Ln_SwL*KgradpnL) / (LwL + LnL);
  Real cR = (Lw_SwR*LnR*KgradpwR - LwR*Ln_SwR*KgradpnR) / (LwR + LnR);

  const Real eps = 1e-3;
  const Real alpha = 40;

  Real rw_cL = rhowL*cL;
  Real rw_cR = rhowR*cR;
  Real rn_cL = rhonL*cL;
  Real rn_cR = rhonR*cR;
  Real cw_max = smoothmax( smoothabs0(rw_cL, eps), smoothabs0(rw_cR, eps), alpha );
  Real cn_max = smoothmax( smoothabs0(rn_cL, eps), smoothabs0(rn_cR, eps), alpha );

  fn_true(0) += 0.5*cw_max*(SwL - SwR);
  fn_true(1) -= 0.5*cn_max*(SwL - SwR);

  ArrayQ fn = 0.0;
  pde.fluxViscousSpaceTime( paramL, paramR, x, y, time, qL, qxL, qyL, qtL, qR, qxR, qyR, qtR, nx, ny, nt, fn );
  SANS_CHECK_CLOSE( fn_true[0], fn[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fn_true[1], fn[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( fn_true[2], fn[2], small_tol, close_tol );

  //Viscous diffusion matrix
  MatrixQ kxx = 0, kxy = 0, kxt = 0;
  MatrixQ kyx = 0, kyy = 0, kyt = 0;
  MatrixQ ktx = 0, kty = 0, ktt = 0;
  BOOST_CHECK_THROW( pde.diffusionViscous( paramL, x, y, time, qL, qxL, qyL, kxx, kxy, kyx, kyy ), DeveloperException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( derivedQuantities )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelClass> PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> MatrixSym;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  const Real pref = 1.47;

  const Real rhow_ref = 62.4;
  const Real rhon_ref = 52.1;
  const Real Cw = 0.03;
  const Real Cn = 0.05;

  const Real phi_ref = 0.3;
  const Real Cr = 0.06;

  const Real muw = 1.0, mun = 2.0;
  const Real pcmax = 5.0;

  DensityModel rhow_model(rhow_ref, Cw, pref);
  DensityModel rhon_model(rhon_ref, Cn, pref);

  PorosityModel phi_model(phi_ref, Cr, pref);

  RelPermModel krw_model(2);
  RelPermModel krn_model(2);

  ViscModel muw_model(muw);
  ViscModel mun_model(mun);

  Real Kxx = 1.1, Kxy = 0.6, Kyx = 0.2, Kyy = 1.5;
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc_model(pcmax);

  const int order = 1;
  bool hasSpaceTimeDiffusion = true;
  PDEClass pde(order, hasSpaceTimeDiffusion, rhow_model, rhon_model, phi_model,
               krw_model, krn_model, muw_model, mun_model, K, pc_model);

  std::vector<std::string> namesTrue = {"charvelocityX_wet",
                                        "charvelocityY_wet",
                                        "charvelocityX_nonwet",
                                        "charvelocityY_nonwet",
                                        "nu_max",
                                        "hxx",
                                        "hyy",
                                        "pc",
                                        "phi",
                                        "Kxx",
                                        "Kyy"
                                       };
  std::vector<std::string> names = pde.derivedQuantityNames();

  BOOST_CHECK_EQUAL( names.size(), namesTrue.size() );

  for (int i = 0; i < (int) namesTrue.size(); i++)
    BOOST_CHECK_EQUAL( namesTrue[i], names[i] );

  Real x = 0, y = 0, time = 0;

  Real Sw = 0.375;
  Real Swx = -0.63;
  Real Swy = 0.27;

  Real pn = 25.34;
  Real pnx = -0.52;
  Real pny = 0.961;

  Real nu = 0.786;
  Real nux = -0.22;
  Real nuy =  0.82;

  Real Sn = 1.0 - Sw;
  Real pc = pcmax*Sn;

  Real phi = phi_ref*exp(Cr*(pn - pref));

  MatrixSym H = {{0.27},{0.12,0.63}};
  MatrixSym paramL = log(H);

  ArrayQ q = {pn, Sw, nu};
  ArrayQ qx = {pnx, Swx, nux};
  ArrayQ qy = {pny, Swy, nuy};

  std::vector<Real> output(names.size(), 0.0);
  for (int i = 0; i < (int) namesTrue.size(); i++)
    pde.derivedQuantity(i, paramL, x, y, time, q, qx, qy, output[i]);

  ArrayQ vx_single = 0, vy_single = 0;
  pde.characteristicVelocitySinglePhase(x, y, time, q, qx, qy, vx_single, vy_single);
  SANS_CHECK_CLOSE( vx_single[0], output[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( vy_single[0], output[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( vx_single[1], output[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( vy_single[1], output[3], small_tol, close_tol );

  Real nu_max;
  pde.artViscMax(paramL, x, y, time, q, qx, qy, nu_max);
  SANS_CHECK_CLOSE( nu_max, output[4], small_tol, close_tol );

  SANS_CHECK_CLOSE( H(0,0), output[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( H(1,1), output[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( pc, output[7], small_tol, close_tol );
  SANS_CHECK_CLOSE( phi, output[8], small_tol, close_tol );
  SANS_CHECK_CLOSE( Kxx, output[9], small_tol, close_tol );
  SANS_CHECK_CLOSE( Kyy, output[10], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/PDETwoPhase_ArtificialViscosity2D_pattern.txt", true );

  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelClass> PDEClass;

  const Real pref = 1.47;

  const Real rhow_ref = 62.4;
  const Real rhon_ref = 52.1;
  const Real Cw = 0.03;
  const Real Cn = 0.05;

  const Real phi_ref = 0.3;
  const Real Cr = 0.06;

  const Real muw = 1.0, mun = 2.0;
  const Real pcmax = 5.0;

  DensityModel rhow_model(rhow_ref, Cw, pref);
  DensityModel rhon_model(rhon_ref, Cn, pref);

  PorosityModel phi_model(phi_ref, Cr, pref);

  RelPermModel krw_model(2);
  RelPermModel krn_model(2);

  ViscModel muw_model(muw);
  ViscModel mun_model(mun);

  Real Kxx = 1.1, Kxy = 0.6, Kyx = 0.2, Kyy = 1.5;
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc_model(pcmax);

  const int order = 1;
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(order, hasSpaceTimeDiffusion, rhow_model, rhon_model, phi_model,
               krw_model, krn_model, muw_model, mun_model, K, pc_model);

  pde.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
