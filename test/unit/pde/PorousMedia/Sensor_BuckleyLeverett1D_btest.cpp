// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Sensor_BuckleyLeverett1D_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/TraitsBuckleyLeverett.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/PDEBuckleyLeverett_ArtificialViscosity1D.h"
#include "pde/PorousMedia/Sensor_BuckleyLeverett.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel_PowerLaw, CapillaryModel_Linear> TraitsModelClass;

typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;
template class Sensor_BuckleyLeverett< PDEClass >;
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Sensor_BuckleyLeverett1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jumpSensor )
{
  typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel_PowerLaw, CapillaryModel_Linear> TraitsModelClass;
  typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;
  typedef Sensor_BuckleyLeverett<PDEClass> Sensor;
  typedef Sensor::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  // PDE
  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  CapillaryModel_Linear cap_model(5);

  const int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEClass pde(order, hasSpaceTimeDiffusion, phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  Sensor sensor(pde);

  const ArrayQ jump = -0.375;

  Real jumpQuantity = 0;
  Real jumpQuantityTrue = -0.375;
  sensor.jumpQuantity(jump, jumpQuantity);
  BOOST_CHECK_CLOSE( jumpQuantityTrue, jumpQuantity, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/Sensor_BuckleyLeverett1D_pattern.txt", true );

  typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel_PowerLaw, CapillaryModel_Linear> TraitsModelClass;
  typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;
  typedef Sensor_BuckleyLeverett<PDEClass> Sensor;

  // PDE
  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  CapillaryModel_Linear cap_model(5);

  const int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEClass pde(order, hasSpaceTimeDiffusion, phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  Sensor sensor(pde);

  sensor.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
