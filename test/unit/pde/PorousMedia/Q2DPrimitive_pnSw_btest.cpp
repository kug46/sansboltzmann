// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Q2DPrimitive_pnSw_btest
// testing of Q2D<QTypePrimitive_pnSw, CapillaryModel> class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <string>
#include <iostream>
#include "pde/PorousMedia/Q2DPrimitive_pnSw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsTwoPhase.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Q2DPrimitive_pnSw_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test )
{
  const Real tol = 1.e-13;

  Real pc_max = 5.0;
  CapillaryModel_Linear pc_model(pc_max);

  typedef Q2D<QTypePrimitive_pnSw, CapillaryModel_Linear, TraitsSizeTwoPhase> QInterpret;
  BOOST_CHECK( QInterpret::D == 2 );
  BOOST_CHECK( QInterpret::N == 2 );

  typedef QInterpret::ArrayQ<Real> ArrayQ;

  // constructor
  QInterpret qInterpret(pc_model);

  // set/eval
  ArrayQ q;
  Real pw, pn, pc, Sw, Sn;
  Real pw1, pw2;
  Real pn1, pn2;
  Real Sw1, Sw2;
  Real Sn1, Sn2;

  pn = 2512.4;
  Sw = 0.375;

  Sn = 1-Sw;
  pc = pc_max*Sn;
  pw = pn - pc;

  Real qDataPrim[2] = {pn, Sw};
  string qNamePrim[2] = {"pn","Sw"};
  qInterpret.setFromPrimitive( q, qDataPrim, qNamePrim, 2 );
  qInterpret.eval( q, pw1, pn1, Sw1, Sn1 );
  BOOST_CHECK_CLOSE( pw1, pw, tol );
  BOOST_CHECK_CLOSE( pn1, pn, tol );
  BOOST_CHECK_CLOSE( Sw1, Sw, tol );
  BOOST_CHECK_CLOSE( Sn1, Sn, tol );

  pw1 = 0.0; pn1 = 0.0; Sw1 = 0.0; Sn1 = 0.0;
  qInterpret.eval_pw( q, pw1 );
  qInterpret.eval_pn( q, pn1 );
  qInterpret.eval_Sw( q, Sw1 );
  qInterpret.eval_Sn( q, Sn1 );
  BOOST_CHECK_CLOSE( pw1, pw, tol );
  BOOST_CHECK_CLOSE( pn1, pn, tol );
  BOOST_CHECK_CLOSE( Sw1, Sw, tol );
  BOOST_CHECK_CLOSE( Sn1, Sn, tol );

  // copy constructor
  QInterpret qInterpret2(qInterpret);

  qInterpret2.eval( q, pw2, pn2, Sw2, Sn2 );
  BOOST_CHECK_CLOSE( pw2, pw, tol );
  BOOST_CHECK_CLOSE( pn2, pn, tol );
  BOOST_CHECK_CLOSE( Sw2, Sw, tol );
  BOOST_CHECK_CLOSE( Sn2, Sn, tol );

  //jacobian
  ArrayQ pw_q, pn_q, Sw_q, Sn_q;
  qInterpret.jacobian_pw(q, pw_q);
  qInterpret.jacobian_pn(q, pn_q);
  qInterpret.jacobian_Sw(q, Sw_q);
  qInterpret.jacobian_Sn(q, Sn_q);
  BOOST_CHECK_CLOSE( pw_q[0], 1.0, tol );
  BOOST_CHECK_CLOSE( pw_q[1], pc_max, tol );
  BOOST_CHECK_CLOSE( pn_q[0], 1.0, tol );
  BOOST_CHECK_CLOSE( pn_q[1], 0.0, tol );
  BOOST_CHECK_CLOSE( Sw_q[0], 0.0, tol );
  BOOST_CHECK_CLOSE( Sw_q[1], 1.0, tol );
  BOOST_CHECK_CLOSE( Sn_q[0], 0.0, tol );
  BOOST_CHECK_CLOSE( Sn_q[1],-1.0, tol );

  // gradient
  Real pwx, pnx, pcx, Swx, Snx;
  Real pwy, pny, pcy, Swy, Sny;
  Real pnt, Swt;
  Real pwx1, pwx2;
  Real pnx1, pnx2;
  Real Swx1, Swx2;
  Real Snx1, Snx2;
  Real Swt1;

  Real pwy1, pwy2;
  Real pny1, pny2;
  Real Swy1, Swy2;
  Real Sny1, Sny2;
  pnx =  0.429;
  pny = -0.281;

  Swx = -0.673;
  Swy =  0.592;

  pnt = 0.353;
  Swt = -0.527;

  Snx = -Swx;
  Sny = -Swy;
  pcx = pc_max*Snx;
  pcy = pc_max*Sny;
  pwx = pnx - pcx;
  pwy = pny - pcy;

//  Real qxData[1] = {Swx};
  ArrayQ qx = {pnx, Swx};
  ArrayQ qy = {pny, Swy};
  ArrayQ qt = {pnt, Swt};

  qInterpret.eval_pwGradient( q, qx, qy, pwx1, pwy1);
  qInterpret.eval_pnGradient( q, qx, qy, pnx1, pny1);
  qInterpret.eval_SwGradient( q, qx, qy, Swx1, Swy1);
  qInterpret.eval_SnGradient( q, qx, qy, Snx1, Sny1);
  BOOST_CHECK_CLOSE( pwx1, pwx, tol );
  BOOST_CHECK_CLOSE( pwy1, pwy, tol );
  BOOST_CHECK_CLOSE( pnx1, pnx, tol );
  BOOST_CHECK_CLOSE( pny1, pny, tol );
  BOOST_CHECK_CLOSE( Swx1, Swx, tol );
  BOOST_CHECK_CLOSE( Swy1, Swy, tol );
  BOOST_CHECK_CLOSE( Snx1, Snx, tol );
  BOOST_CHECK_CLOSE( Sny1, Sny, tol );

  Swx1 = 0;
  Swy1 = 0;
  qInterpret.eval_SwGradient( q, qx, qy, qt, Swx1, Swy1, Swt1);
  BOOST_CHECK_CLOSE( Swx1, Swx, tol );
  BOOST_CHECK_CLOSE( Swy1, Swy, tol );
  BOOST_CHECK_CLOSE( Swt1, Swt, tol );

  qInterpret2.eval_pwGradient( q, qx, qy, pwx2, pwy2);
  qInterpret2.eval_pnGradient( q, qx, qy, pnx2, pny2);
  qInterpret2.eval_SwGradient( q, qx, qy, Swx2, Swy2);
  qInterpret2.eval_SnGradient( q, qx, qy, Snx2, Sny2);
  BOOST_CHECK_CLOSE( pwx2, pwx, tol );
  BOOST_CHECK_CLOSE( pwy2, pwy, tol );
  BOOST_CHECK_CLOSE( pnx2, pnx, tol );
  BOOST_CHECK_CLOSE( pny2, pny, tol );
  BOOST_CHECK_CLOSE( Swx2, Swx, tol );
  BOOST_CHECK_CLOSE( Swy2, Swy, tol );
  BOOST_CHECK_CLOSE( Snx2, Snx, tol );
  BOOST_CHECK_CLOSE( Sny2, Sny, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  CapillaryModel_Linear pc(5.0);

  typedef Q2D<QTypePrimitive_pnSw, CapillaryModel_Linear, TraitsSizeTwoPhase> QInterpret;
  typedef QInterpret::ArrayQ<Real> ArrayQ;

  QInterpret qInterpret(pc);
  ArrayQ q;

  q(0) = 2500.0;
  q(1) =  0.375;
  BOOST_CHECK( qInterpret.isValidState(q) == true );

  q(0) = 2500.0;
  q(1) =  0.739;
  BOOST_CHECK( qInterpret.isValidState(q) == true );

  q(0) = 2500.0;
  q(1) = -0.038;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  q(0) = 2500.0;
  q(1) =  1.01;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  q(0) = -0.5;
  q(1) =  0.5;
  BOOST_CHECK( qInterpret.isValidState(q) == false );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
