// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDETwoPhase1D_btest
//
// testing of 1-D two phase PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/PorousMedia/TraitsTwoPhase.h"
#include "pde/PorousMedia/Q1DPrimitive_pnSw.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/PDETwoPhase1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"

//Explicitly instantiate the class so coverage information is correct
namespace SANS
{

template class TraitsSizeTwoPhase<PhysD1>;
template class TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_Comp,
                                   RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                                   Real, CapillaryModel_Linear>;

typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_Comp,
                            RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                            Real, CapillaryModel_Linear> TraitsModelClass;

template class PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDETwoPhase1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test_pnSw )
{
  {
    typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

    BOOST_CHECK( PDEClass::D == 1 );
    BOOST_CHECK( PDEClass::N == 2 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_pnSw )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef Real RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  Real tol = 1e-13;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  CapillaryModel pc(5);
  const Real K = 0.25;

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == false );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasSourceLiftedQuantity() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );
  BOOST_CHECK_CLOSE( pde.permeability(), K, tol);

  //Copy constructor
  PDEClass pde2(pde);
  BOOST_CHECK( pde2.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde2.hasFluxAdvective() == false );
  BOOST_CHECK( pde2.hasFluxViscous() == true );
  BOOST_CHECK( pde2.hasSource() == false );
  BOOST_CHECK( pde2.hasSourceLiftedQuantity() == false );
  BOOST_CHECK( pde2.hasForcingFunction() == false );
  BOOST_CHECK( pde2.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde2.needsSolutionGradientforSource() == false );
  BOOST_CHECK_CLOSE( pde2.permeability(), K, tol);

  // Set up PyDicts
  PyDict fixedwellpressure;
  fixedwellpressure[SourceTwoPhase1DType_FixedWellPressure_Param::params.pB] = 2350.0;
  fixedwellpressure[SourceTwoPhase1DParam::params.Source.SourceType] = SourceTwoPhase1DParam::params.Source.FixedPressure;

  PyDict well;
  well[SourceTwoPhase1DParam::params.Source] = fixedwellpressure;
  well[SourceTwoPhase1DParam::params.xmin] = 10.0;
  well[SourceTwoPhase1DParam::params.xmax] = 15.0;
  well[SourceTwoPhase1DParam::params.Tmin] = 1.0;
  well[SourceTwoPhase1DParam::params.Tmax] = 3.0;
  well[SourceTwoPhase1DParam::params.smoothLx] = 0.75;
  well[SourceTwoPhase1DParam::params.smoothT] = 0.0;

  PyDict sourceList;
  sourceList["well"] = well;
  SourceTwoPhase1DListParam::checkInputs(sourceList);

  //Constructor with source terms
  PDEClass pde3(rhow, rhon, phi, krw, krn, muw, mun, K, pc, sourceList);

  BOOST_CHECK( pde3.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde3.hasFluxAdvective() == false );
  BOOST_CHECK( pde3.hasFluxViscous() == true );
  BOOST_CHECK( pde3.hasSource() == true );
  BOOST_CHECK( pde3.hasForcingFunction() == false );
  BOOST_CHECK( pde3.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde3.needsSolutionGradientforSource() == false );
  BOOST_CHECK_CLOSE( pde3.permeability(), K, tol);

  //Copy constructor
  PDEClass pde4(pde3);
  BOOST_CHECK( pde4.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde4.hasFluxAdvective() == false );
  BOOST_CHECK( pde4.hasFluxViscous() == true );
  BOOST_CHECK( pde4.hasSource() == true );
  BOOST_CHECK( pde4.hasForcingFunction() == false );
  BOOST_CHECK( pde4.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde4.needsSolutionGradientforSource() == false );
  BOOST_CHECK_CLOSE( pde4.permeability(), K, tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setDOFFrom )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef Real RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;

  Real tol = 1e-13;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  CapillaryModel pc(5);
  const Real K = 0.25;

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  Real pn = 2534.8;
  Real Sw = 0.784;

  // set
  Real qDataPrim[2] = {pn,Sw};
  string qNamePrim[2] = {"pn","Sw"};
  ArrayQ qTrue = {pn,Sw};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 2 );
  BOOST_CHECK_CLOSE( qTrue[0], q[0], tol );
  BOOST_CHECK_CLOSE( qTrue[1], q[1], tol );

  PressureNonWet_SaturationWet<Real> qdata1;
  qdata1.pn = pn; qdata1.Sw = Sw;
  q = 0;
  pde.setDOFFrom( q, qdata1 );
  BOOST_CHECK_CLOSE( qTrue[0], q[0], tol );
  BOOST_CHECK_CLOSE( qTrue[1], q[1], tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux_pnSw )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef Real RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  const Real pref = 1.47;

  const Real rhow_ref = 62.4;
  const Real rhon_ref = 52.1;
  const Real Cw = 0.03;
  const Real Cn = 0.05;

  const Real phi_ref = 0.3;
  const Real Cr = 0.06;

  const Real muw = 1.0, mun = 2.0;
  const Real pcmax = 5.0;

  DensityModel rhow_model(rhow_ref, Cw, pref);
  DensityModel rhon_model(rhon_ref, Cn, pref);

  PorosityModel phi_model(phi_ref, Cr, pref);

  RelPermModel krw_model(2);
  RelPermModel krn_model(2);

  ViscModel muw_model(muw);
  ViscModel mun_model(mun);

  CapillaryModel pc_model(pcmax);
  const Real K = 0.25;

  PDEClass pde(rhow_model, rhon_model, phi_model, krw_model, krn_model, muw_model, mun_model, K, pc_model);

  // Static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 2 );

  // Flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == false );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // Function tests
  Real x = 0, time = 0;   // not actually used in functions
  Real nx = 0.35;

  Real SwL = 0.375, SwR = 0.418;
  Real SwxL = -0.63, SwxR = -0.37;
  Real SwtL = 0.27;
  Real pnL = 25.34, pnR = 31.86;
  Real pnxL = -0.52, pnxR = -0.29;
  Real pntL = -0.31;

  Real SnL = 1.0 - SwL;
  Real SnR = 1.0 - SwR;
  Real pcL = pcmax*SnL;
  Real pcR = pcmax*SnR;
  Real pwL = pnL - pcL;
  Real pwR = pnR - pcR;
  Real pwxL = pnxL - pcmax*(-SwxL);
  Real pwxR = pnxR - pcmax*(-SwxR);

  Real krwL = pow(SwL,2);
  Real krwR = pow(SwR,2);

  Real krnL = pow(SnL,2);
  Real krnR = pow(SnR,2);

  Real krw_SwL = 2*SwL;
  Real krw_SwR = 2*SwR;

  Real krn_SwL = -2*SnL;
  Real krn_SwR = -2*SnR;

  Real rhowL = rhow_ref*exp(Cw*(pwL - pref));
  Real rhowR = rhow_ref*exp(Cw*(pwR - pref));

  Real rhonL = rhon_ref*exp(Cn*(pnL - pref));
  Real rhonR = rhon_ref*exp(Cn*(pnR - pref));

  Real phiL  = phi_ref *exp(Cr*(pnL - pref));

  Real rhow_pnL = Cw*rhowL;
  Real rhow_SwL = Cw*rhowL*pcmax;
  Real rhon_pnL = Cn*rhonL;
  Real phi_pnL = Cr*phiL;

  Real LwL = krwL/muw;
  Real LwR = krwR/muw;

  Real LnL = krnL/mun;
  Real LnR = krnR/mun;

  Real Lw_SwL = krw_SwL / muw;
  Real Lw_SwR = krw_SwR / muw;

  Real Ln_SwL = krn_SwL / mun;
  Real Ln_SwR = krn_SwR / mun;

  // Set
  Real qLDataPrim[2] = {pnL,SwL};
  Real qRDataPrim[2] = {pnR,SwR};
  string qNamePrim[2] = {"pn","Sw"};
  ArrayQ qLTrue = {pnL,SwL};
  ArrayQ qRTrue = {pnR,SwR};
  ArrayQ qL, qR;
  pde.setDOFFrom( qL, qLDataPrim, qNamePrim, 2 );
  pde.setDOFFrom( qR, qRDataPrim, qNamePrim, 2 );
  SANS_CHECK_CLOSE( qLTrue[0], qL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( qLTrue[1], qL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( qRTrue[0], qR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( qRTrue[1], qR[1], small_tol, close_tol );

  Real Sw_test, Sn_test, pw_test, pn_test;
  pde.variableInterpreter().eval(qL, pw_test, pn_test, Sw_test, Sn_test);
  SANS_CHECK_CLOSE( pwL, pw_test, small_tol, close_tol );
  SANS_CHECK_CLOSE( pnL, pn_test, small_tol, close_tol );
  SANS_CHECK_CLOSE( SwL, Sw_test, small_tol, close_tol );
  SANS_CHECK_CLOSE( SnL, Sn_test, small_tol, close_tol );

  // Master State
  ArrayQ ucons_true, ucons;
  ucons_true[0] = rhowL*phiL*SwL;
  ucons_true[1] = rhonL*phiL*SnL;
  pde.masterState( x, time, qL, ucons );
  SANS_CHECK_CLOSE( ucons_true[0], ucons[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( ucons_true[1], ucons[1], small_tol, close_tol );

  // Master state jacobian
  MatrixQ fcons_jac_true, fcons_jac;
  fcons_jac_true(0,0) = (phi_pnL*rhowL + phiL*rhow_pnL)*SwL;
  fcons_jac_true(0,1) = (                phiL*rhow_SwL)*SwL + phiL*rhowL;
  fcons_jac_true(1,0) = (phi_pnL*rhonL + phiL*rhon_pnL)*SnL;
  fcons_jac_true(1,1) = phiL*rhonL*(-1);
  pde.jacobianMasterState( x, time, qL, fcons_jac );
  SANS_CHECK_CLOSE( fcons_jac_true(0,0), fcons_jac(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_jac_true(0,1), fcons_jac(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_jac_true(1,0), fcons_jac(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_jac_true(1,1), fcons_jac(1,1), small_tol, close_tol );

  // Conservative flux
  ArrayQ fcons_true, fcons = 0;
  fcons_true[0] = rhowL*phiL*SwL;
  fcons_true[1] = rhonL*phiL*SnL;
  pde.fluxAdvectiveTime( x, time, qL, fcons );
  SANS_CHECK_CLOSE( fcons_true[0], fcons[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_true[1], fcons[1], small_tol, close_tol );

  // Advective flux
  ArrayQ fadv_true = 0.0;
  ArrayQ fadv;
  fadv = 0;
  pde.fluxAdvective( x, time, qL, fadv );
  SANS_CHECK_CLOSE( fadv_true[0], fadv[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fadv_true[1], fadv[1], small_tol, close_tol );

  // Jacobian of advective flux
  MatrixQ fadv_u_true = 0.0;
  MatrixQ fadv_u;
  fadv_u = 0;
  pde.jacobianFluxAdvective( x, time, qL, fadv_u );
  SANS_CHECK_CLOSE( fadv_u_true(0,0), fadv_u(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fadv_u_true(0,1), fadv_u(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fadv_u_true(1,0), fadv_u(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fadv_u_true(1,1), fadv_u(1,1), small_tol, close_tol );

  ArrayQ qxL = {pnxL, SwxL};
  ArrayQ qxR = {pnxR, SwxR};

  // Viscous flux
  ArrayQ fvisc_trueL, fvisc = 0;
  fvisc_trueL[0] = -rhowL*K*LwL*pwxL;
  fvisc_trueL[1] = -rhonL*K*LnL*pnxL;
  pde.fluxViscous( x, time, qL, qxL, fvisc );
  SANS_CHECK_CLOSE( fvisc_trueL[0], fvisc[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fvisc_trueL[1], fvisc[1], small_tol, close_tol );

  ArrayQ fvisc_trueR;
  fvisc_trueR[0] = -rhowR*K*LwR*pwxR;
  fvisc_trueR[1] = -rhonR*K*LnR*pnxR;

  Real KgradpwL = K*pwxL*nx;
  Real KgradpwR = K*pwxR*nx;

  Real KgradpnL = K*pnxL*nx;
  Real KgradpnR = K*pnxR*nx;

  Real cL = (Lw_SwL*LnL*KgradpwL - LwL*Ln_SwL*KgradpnL) / (LwL + LnL);
  Real cR = (Lw_SwR*LnR*KgradpwR - LwR*Ln_SwR*KgradpnR) / (LwR + LnR);

  const Real eps = 1e-3;
  const Real alpha = 40;

  Real rw_cL = rhowL*cL;
  Real rw_cR = rhowR*cR;
  Real rn_cL = rhonL*cL;
  Real rn_cR = rhonR*cR;
  Real cw_max = smoothmax( smoothabs0(rw_cL, eps), smoothabs0(rw_cR, eps), alpha );
  Real cn_max = smoothmax( smoothabs0(rn_cL, eps), smoothabs0(rn_cR, eps), alpha );

  ArrayQ fn_true = 0.5*(fvisc_trueL + fvisc_trueR)*nx;
  fn_true(0) += 0.5*cw_max*(SwL - SwR);
  fn_true(1) -= 0.5*cn_max*(SwL - SwR);
  ArrayQ fn = 0.0;
  pde.fluxViscous( x, time, qL, qxL, qR, qxR, nx, fn );
  SANS_CHECK_CLOSE( fn_true[0], fn[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fn_true[1], fn[1], small_tol, close_tol );

  // Space-time viscous flux
  fvisc = 0;
  ArrayQ gvisc = 0;
  ArrayQ gvisc_trueL = {0.0, 0.0};
  ArrayQ qtL = {pntL, SwtL};
  pde.fluxViscousSpaceTime( x, time, qL, qxL, qtL, fvisc, gvisc );
  SANS_CHECK_CLOSE( fvisc_trueL[0], fvisc[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fvisc_trueL[1], fvisc[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( gvisc_trueL[0], gvisc[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( gvisc_trueL[1], gvisc[1], small_tol, close_tol );

  // Viscous diffusion matrix
  Real pc_Sw = -pcmax;
  MatrixQ Fv_qx_true, kxx = 0;
  Fv_qx_true(0,0) = rhowL*K*LwL;
  Fv_qx_true(0,1) = rhowL*K*LwL*(-pc_Sw);
  Fv_qx_true(1,0) = rhonL*K*LnL;
  Fv_qx_true(1,1) = 0.0;

  pde.diffusionViscous( x, time, qL, qxL, kxx );
  MatrixQ Fv_qx = kxx*fcons_jac; //d(Fv)/d(grad q) = d(Fv)/d(grad u) * du/dq

  SANS_CHECK_CLOSE( Fv_qx_true(0,0), Fv_qx(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fv_qx_true(0,1), Fv_qx(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fv_qx_true(1,0), Fv_qx(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fv_qx_true(1,1), Fv_qx(1,1), small_tol, close_tol );

  // Space-time viscous diffusion matrix
  kxx = 0;
  MatrixQ kxt = 0, ktx = 0, ktt = 0;

  pde.diffusionViscousSpaceTime( x, time, qL, qxL, qtL, kxx, kxt, ktx, ktt );
  Fv_qx = kxx*fcons_jac; //d(Fv)/d(grad q) = d(Fv)/d(grad u) * du/dq

  SANS_CHECK_CLOSE( Fv_qx_true(0,0), Fv_qx(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fv_qx_true(0,1), Fv_qx(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fv_qx_true(1,0), Fv_qx(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fv_qx_true(1,1), Fv_qx(1,1), small_tol, close_tol );

  SANS_CHECK_CLOSE( 0.0, kxt(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, kxt(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, kxt(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, kxt(1,1), small_tol, close_tol );

  SANS_CHECK_CLOSE( 0.0, ktx(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, ktx(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, ktx(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, ktx(1,1), small_tol, close_tol );

  SANS_CHECK_CLOSE( 0.0, ktt(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, ktt(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, ktt(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, ktt(1,1), small_tol, close_tol );

  //Checking viscous fluxes with additional viscosity term for single-phase stability
  Real scalar_visc = 1.46;
  PDEClass pde2(rhow_model, rhon_model, phi_model, krw_model, krn_model, muw_model, mun_model, K, pc_model, scalar_visc);

  fvisc_trueL[0] = -rhowL*K*LwL*pwxL - rhowL*scalar_visc*SwxL;
  fvisc_trueL[1] = -rhonL*K*LnL*pnxL + rhonL*scalar_visc*SwxL;

  fvisc = 0;
  pde2.fluxViscous( x, time, qL, qxL, fvisc );
  SANS_CHECK_CLOSE( fvisc_trueL[0], fvisc[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fvisc_trueL[1], fvisc[1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( source_fixedpressure_pnSw )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef Real RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  const Real pref = 1.47;

  const Real rhow_ref = 62.4;
  const Real rhon_ref = 52.1;
  const Real Cw = 0.03;
  const Real Cn = 0.05;

  const Real phi_ref = 0.3;
  const Real Cr = 0.06;

  const Real muw = 1.0, mun = 2.0;
  const Real pcmax = 5.0;

  DensityModel rhow_model(rhow_ref, Cw, pref);
  DensityModel rhon_model(rhon_ref, Cn, pref);

  PorosityModel phi_model(phi_ref, Cr, pref);

  RelPermModel krw_model(2);
  RelPermModel krn_model(2);

  ViscModel muw_model(muw);
  ViscModel mun_model(mun);

  CapillaryModel pc_model(pcmax);
  const Real K = 0.25;

  const Real pB1 = 15.82;
  const Real pB2 = 17.93;

  // Set up PyDicts
  PyDict fixedwellpressure1;
  fixedwellpressure1[SourceTwoPhase1DType_FixedWellPressure_Param::params.pB] = pB1;
  fixedwellpressure1[SourceTwoPhase1DParam::params.Source.SourceType] = SourceTwoPhase1DParam::params.Source.FixedPressure;

  PyDict fixedwellpressure2;
  fixedwellpressure2[SourceTwoPhase1DType_FixedWellPressure_Param::params.pB] = pB2;
  fixedwellpressure2[SourceTwoPhase1DParam::params.Source.SourceType] = SourceTwoPhase1DParam::params.Source.FixedPressure;

  PyDict well1;
  well1[SourceTwoPhase1DParam::params.Source] = fixedwellpressure1;
  well1[SourceTwoPhase1DParam::params.xmin] = 10.0;
  well1[SourceTwoPhase1DParam::params.xmax] = 15.0;
  well1[SourceTwoPhase1DParam::params.Tmin] = 1.0;
  well1[SourceTwoPhase1DParam::params.Tmax] = 3.0;
  well1[SourceTwoPhase1DParam::params.smoothLx] = 1.0;
  well1[SourceTwoPhase1DParam::params.smoothT] = 0.2;

  PyDict well2;
  well2[SourceTwoPhase1DParam::params.Source] = fixedwellpressure2;
  well2[SourceTwoPhase1DParam::params.xmin] = 25.0;
  well2[SourceTwoPhase1DParam::params.xmax] = 50.0;
  well2[SourceTwoPhase1DParam::params.Tmin] = 5.0;
  well2[SourceTwoPhase1DParam::params.Tmax] = 10.0;
  well2[SourceTwoPhase1DParam::params.smoothLx] = 5.0;
  well2[SourceTwoPhase1DParam::params.smoothT] = 1.0;

  PyDict sourceList;
  sourceList["well1"] = well1;
  sourceList["well2"] = well2;
  SourceTwoPhase1DListParam::checkInputs(sourceList);

  PDEClass pde(rhow_model, rhon_model, phi_model, krw_model, krn_model, muw_model, mun_model, K, pc_model, sourceList);

  // Static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 2 );

  // Flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == false );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // Function tests
  Real Sw = 0.375;
  Real Swx = -0.63;
  Real pn = 25.34;
  Real pnx = -0.52;

  Real Sn = 1.0 - Sw;
  Real pc = pcmax*Sn;
  Real pw = pn - pc;

  Real krw = pow(Sw,2);
  Real krn = pow(1-Sw,2);

  Real rhow = rhow_ref*exp(Cw*(pw - pref));
  Real rhon = rhon_ref*exp(Cn*(pn - pref));

  Real lambda_w = krw/muw;
  Real lambda_n = krn/mun;

  Real Ls1 = 5.0;
  Real Lchar1 = 0.25*Ls1;
  Real dpndx1 = (pn - pB1)/Lchar1;

  Real Ls2 = 25.0;
  Real Lchar2 = 0.25*Ls2;
  Real dpndx2 = (pn - pB2)/Lchar2;

  // Set
  Real qDataPrim[2] = {pn,Sw};
  string qNamePrim[2] = {"pn","Sw"};
  ArrayQ qTrue = {pn,Sw};
  ArrayQ q;
  ArrayQ qx = {pnx, Swx};
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 2 );
  SANS_CHECK_CLOSE( qTrue[0], q[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue[1], q[1], small_tol, close_tol );

  //------------------------------------------------------------

  Real x = 9.5;
  Real t = 2.0;
  Real weight1 = 0.0, weight2 = 0.0;

  ArrayQ sourceTrue, source = 0.0;
  sourceTrue[0] = rhow*lambda_w*K*(dpndx1/Ls1*weight1 + dpndx2/Ls2*weight2);
  sourceTrue[1] = rhon*lambda_n*K*(dpndx1/Ls1*weight1 + dpndx2/Ls2*weight2);

  pde.source(x, t, q, qx, source);
  SANS_CHECK_CLOSE( sourceTrue[0], source[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( sourceTrue[1], source[1], small_tol, close_tol);

  //------------------------------------------------------------

  x = 10.0, t = 2.0;
  weight1 = 0.5, weight2 = 0.0;

  source = 0.0;
  sourceTrue[0] = rhow*lambda_w*K*(dpndx1/Ls1*weight1 + dpndx2/Ls2*weight2);
  sourceTrue[1] = rhon*lambda_n*K*(dpndx1/Ls1*weight1 + dpndx2/Ls2*weight2);

  pde.source(x, t, q, qx, source);
  SANS_CHECK_CLOSE( sourceTrue[0], source[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( sourceTrue[1], source[1], small_tol, close_tol);

  //------------------------------------------------------------

  x = 12.0, t = 3.0;
  weight1 = 0.5, weight2 = 0.0;

  source = 0.0;
  sourceTrue[0] = rhow*lambda_w*K*(dpndx1/Ls1*weight1 + dpndx2/Ls2*weight2);
  sourceTrue[1] = rhon*lambda_n*K*(dpndx1/Ls1*weight1 + dpndx2/Ls2*weight2);

  pde.source(x, t, q, qx, source);
  SANS_CHECK_CLOSE( sourceTrue[0], source[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( sourceTrue[1], source[1], small_tol, close_tol);

  //------------------------------------------------------------

  x = 10.25, t = 2.98;
  weight1 = 0.84375*0.6480, weight2 = 0.0;

  source = 0.0;
  sourceTrue[0] = rhow*lambda_w*K*(dpndx1/Ls1*weight1 + dpndx2/Ls2*weight2);
  sourceTrue[1] = rhon*lambda_n*K*(dpndx1/Ls1*weight1 + dpndx2/Ls2*weight2);

  pde.source(x, t, q, qx, source);
  SANS_CHECK_CLOSE( sourceTrue[0], source[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( sourceTrue[1], source[1], small_tol, close_tol);

  //------------------------------------------------------------

  x = 30.0, t = 8.0;
  weight1 = 0.0, weight2 = 1.0;

  source = 0.0;
  sourceTrue[0] = rhow*lambda_w*K*(dpndx1/Ls1*weight1 + dpndx2/Ls2*weight2);
  sourceTrue[1] = rhon*lambda_n*K*(dpndx1/Ls1*weight1 + dpndx2/Ls2*weight2);

  pde.source(x, t, q, qx, source);
  SANS_CHECK_CLOSE( sourceTrue[0], source[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( sourceTrue[1], source[1], small_tol, close_tol);

  //------------------------------------------------------------

  x = 51.0, t = 4.9;
  weight1 = 0.0, weight2 = 0.216*0.352;

  source = 0.0;
  sourceTrue[0] = rhow*lambda_w*K*(dpndx1/Ls1*weight1 + dpndx2/Ls2*weight2);
  sourceTrue[1] = rhon*lambda_n*K*(dpndx1/Ls1*weight1 + dpndx2/Ls2*weight2);

  pde.source(x, t, q, qx, source);
  SANS_CHECK_CLOSE( sourceTrue[0], source[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( sourceTrue[1], source[1], small_tol, close_tol);

  //------------------------------------------------------------

  x = 20.0, t = 4.0;
  weight1 = 0.0, weight2 = 0.0;

  source = 0.0;
  sourceTrue[0] = rhow*lambda_w*K*(dpndx1/Ls1*weight1 + dpndx2/Ls2*weight2);
  sourceTrue[1] = rhon*lambda_n*K*(dpndx1/Ls1*weight1 + dpndx2/Ls2*weight2);

  pde.source(x, t, q, qx, source);
  SANS_CHECK_CLOSE( sourceTrue[0], source[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( sourceTrue[1], source[1], small_tol, close_tol);

  //Check source trace
  ArrayQ sourceL = 0, sourceR = 0;
  pde.sourceTrace(x, x, t, q, qx, q, qx, sourceL, sourceR);
  SANS_CHECK_CLOSE( 0.0, sourceL[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( 0.0, sourceL[1], small_tol, close_tol);
  SANS_CHECK_CLOSE( 0.0, sourceR[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( 0.0, sourceR[1], small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( interpResid )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef Real RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;

  Real tol = 1e-13;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  CapillaryModel pc(5);
  const Real K = 0.25;

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  Real pn = 2534.8;
  Real Sw = 0.784;

  // set
  Real qDataPrim[2] = {pn,Sw};
  string qNamePrim[2] = {"pn","Sw"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 2 );

  BOOST_CHECK_EQUAL(pde.nMonitor(), 2);

  DLA::VectorD<Real> vec(2);
  pde.interpResidVariable(q, vec);
  for (int i = 0; i < ArrayQ::M; i++)
    SANS_CHECK_CLOSE( q(i), vec(i), tol, tol );

  vec = 0;
  pde.interpResidGradient(q, vec);
  for (int i = 0; i < ArrayQ::M; i++)
    SANS_CHECK_CLOSE( q(i), vec(i), tol, tol );

  vec = 0;
  pde.interpResidBC(q, vec);
  for (int i = 0; i < ArrayQ::M; i++)
    SANS_CHECK_CLOSE( q(i), vec(i), tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/PDETwoPhase1D_pattern.txt", true );

  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef Real RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  const Real pref = 1.47;

  const Real rhow_ref = 62.4;
  const Real rhon_ref = 52.1;
  const Real Cw = 0.03;
  const Real Cn = 0.05;

  const Real phi_ref = 0.3;
  const Real Cr = 0.06;

  const Real muw = 1.0, mun = 2.0;
  const Real pcmax = 5.0;

  DensityModel rhow_model(rhow_ref, Cw, pref);
  DensityModel rhon_model(rhon_ref, Cn, pref);

  PorosityModel phi_model(phi_ref, Cr, pref);

  RelPermModel krw_model(2);
  RelPermModel krn_model(2);

  ViscModel muw_model(muw);
  ViscModel mun_model(mun);

  CapillaryModel pc_model(pcmax);
  const Real K = 0.25;

  PDEClass pde(rhow_model, rhon_model, phi_model, krw_model, krn_model, muw_model, mun_model, K, pc_model);

  pde.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
