// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCBuckleyLeverett_ArtificialViscosity1D_btest
//
// testing of Buckley-Leverett BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "pde/BCParameters.h"

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverettArtificialViscosity.h"
#include "pde/PorousMedia/BCBuckleyLeverett_ArtificialViscosity1D.h"

//Explicitly instantiate classes so coverage information is correct
namespace SANS
{

typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel_PowerLaw, CapillaryModel_Linear> TraitsModelClass;

template class BCmitAVSensor1D< BCTypeFlux_mitState,
               BCBuckleyLeverett1D<BCTypeDirichlet_mitState,
                                   PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> >>;
template class BCmitAVSensor1D< BCTypeFlux_mitState,
               BCBuckleyLeverett1D<BCTypeFunction_mitState,
                                   PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> >>;
template class BCmitAVSensor1D< BCTypeFlux_mitStateSpaceTime,
               BCBuckleyLeverett1D<BCTypeTimeIC,
                                   PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> >>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BCBuckleyLeverett_ArtificialViscosity1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;

  {
    typedef BCNone<PhysD1,TraitsSizeBuckleyLeverett<PhysD1>::N> BCClass;
    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::NBC == 0 );
  }

  {
    typedef BCmitAVSensor1D< BCTypeFlux_mitState,
                             BCBuckleyLeverett1D<BCTypeDirichlet_mitState, PDEClass> > BCClass;
    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::N == 1 );
    BOOST_CHECK( BCClass::NBC == 1 );
  }

  {
    typedef BCmitAVSensor1D< BCTypeFlux_mitState,
                             BCBuckleyLeverett1D<BCTypeFunction_mitState, PDEClass> > BCClass;
    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::N == 1 );
    BOOST_CHECK( BCClass::NBC == 1 );
  }

  {
    typedef BCmitAVSensor1D< BCTypeFlux_mitStateSpaceTime,
                             BCBuckleyLeverett1D<BCTypeTimeIC, PDEClass> > BCClass;
    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::N == 1 );
    BOOST_CHECK( BCClass::NBC == 1 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor )
{
  typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;

  // PDE
  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  CapillaryModel_Linear cap_model(5);

  const int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEClass pdeAV(order, hasSpaceTimeDiffusion, phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  typedef BCParameters< BCBuckleyLeverett_ArtificialViscosity1DVector<TraitsSizeBuckleyLeverett, TraitsModelClass> > BCParams;

  PyDict BCList;

  {
    typedef BCNone<PhysD1,TraitsSizeBuckleyLeverett<PhysD1>::N> BCClass;

    //Direct constructor
    BCClass bc1;

    //Pydict constructor
    PyDict BCNone;
    BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;
    BCClass bc2(pdeAV, BCNone);

    BCList["BC0"] = BCNone;
  }

  {
    typedef BCmitAVSensor1D< BCTypeFlux_mitState,
                             BCBuckleyLeverett1D<BCTypeDirichlet_mitState, PDEClass> > BCClass;
    typedef BCClass::ArrayQ<Real> ArrayQ;

    //Direct constructor
    const ArrayQ qB = 0.5;
    const Real Cdiff = 3.0;
    BCClass bc1(Cdiff, pdeAV, qB);

    //Pydict constructor
    PyDict BCDirichlet_mitState;
    BCDirichlet_mitState[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
    BCDirichlet_mitState[BCClass::ParamsType::params.qB] = qB;
    BCDirichlet_mitState[BCClass::ParamsType::params.Cdiff] = Cdiff;
    BCClass bc2(pdeAV, BCDirichlet_mitState);

    BCList["BC2"] = BCDirichlet_mitState;
  }

#if 0
  {
    typedef BCmitAVSensor1D< BCTypeLinearRobin,
                                     BCBuckleyLeverett1D<BCTypeLinearRobin, PDEClass> > BCClass;
    typedef BCClass::ArrayQ<Real> ArrayQ;
    typedef BCmitAVSensor1DParams<BCTypeLinearRobin, BCBuckleyLeverett1DParams<BCTypeLinearRobin>> BCParamsClass;

    //Direct constructor
    const ArrayQ bcdata = 0.5;
    const Real sensor = 0.1;
    BCClass bc1(sensor, 0.5, 0.5, bcdata);

    //Pydict constructor
    PyDict BCLinearRobin;
    BCLinearRobin[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin;
    BCLinearRobin[BCParamsClass::params.A] = 0.5;
    BCLinearRobin[BCParamsClass::params.B] = 0.5;
    BCLinearRobin[BCParamsClass::params.bcdata] = bcdata;
    BCLinearRobin[BCParamsClass::params.sensor] = sensor;
    BCClass bc2(pdeAV, BCLinearRobin);

    BCList["BC1"] = BCLinearRobin;
  }
#endif

  {
    typedef BCmitAVSensor1D< BCTypeFlux_mitState,
                             BCBuckleyLeverett1D<BCTypeFunction_mitState, PDEClass> > BCClass;
    typedef ScalarFunction1D_Sine SolutionType;
    std::shared_ptr<SolutionType> slnExact( new SolutionType );

    //Direct constructor
    const Real Cdiff = 3.0;
    BCClass bc1(Cdiff, pdeAV, slnExact);

    //Pydict constructor
    PyDict Soln;
    Soln[BCBuckleyLeverett1DParams<BCTypeFunction_mitState>::params.Function.Name] =
         BCBuckleyLeverett1DParams<BCTypeFunction_mitState>::params.Function.Sine;

    PyDict BCSolution;
    BCSolution[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
    BCSolution[BCClass::ParamsType::params.Function] = Soln;
    BCSolution[BCClass::ParamsType::params.Cdiff] = Cdiff;
    BCClass bc2(pdeAV, BCSolution);

    BCList["BC3"] = BCSolution;
  }

  {
    typedef BCmitAVSensor1D< BCTypeFlux_mitStateSpaceTime,
                             BCBuckleyLeverett1D<BCTypeTimeIC, PDEClass> > BCClass;
    typedef BCClass::ArrayQ<Real> ArrayQ;

    //Direct constructor
    const ArrayQ qB = 0.5;
    const Real Cdiff = 3.0;
    BCClass bc1(Cdiff, pdeAV, qB);

    //Pydict constructor
    PyDict BCTimeIC;
    BCTimeIC[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC;
    BCTimeIC[BCClass::ParamsType::params.qB] = qB;
    BCTimeIC[BCClass::ParamsType::params.Cdiff] = Cdiff;
    BCClass bc2(pdeAV, BCTimeIC);

    BCList["BC4"] = BCTimeIC;
  }

  //No exceptions should be thrown
  BCParams::checkInputs(BCList);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeDirichlet_mitState_test )
{
  {
    typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;
    typedef BCmitAVSensor1D< BCTypeFlux_mitState,
                             BCBuckleyLeverett1D<BCTypeDirichlet_mitState, PDEClass> > BCClass;

    typedef BCClass::template ArrayQ<Real> ArrayQ;

    typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

    // PDE
    RelPermModel_PowerLaw kr_model(2);
    const Real phi = 0.3;
    const Real uT = 0.3;
    const Real mu_w = 1;
    const Real mu_n = 2;

    const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
    const Real K = conversion*200;
    CapillaryModel_Linear cap_model(5);

    const int order = 1;
    const bool hasSpaceTimeDiffusion = false;
    PDEClass pdeAV(order, hasSpaceTimeDiffusion, phi, uT, kr_model, mu_w, mu_n, K, cap_model);

    ParamType param(0.1, 0.2); // grid spacing and jump values

    const Real tol = 1.e-13;

    ArrayQ bcdata = 0.7;
    Real Cdiff = 3.0;

    BCClass bc( Cdiff, pdeAV, bcdata );

    Real x = 0;
    Real time = 0;
    Real nx = 0.8;

    ArrayQ qI = 0.5;
    ArrayQ qB = 0;

    bc.state(param, x, time, nx, qI, qB );
    BOOST_CHECK_CLOSE( qB, bcdata, tol );

    BOOST_CHECK_EQUAL( bc.isValidState(nx,qI), true);
  }

  //Extended state vector
  {
    typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverettArtificialViscosity, TraitsModelClass> PDEClass;
    typedef BCmitAVSensor1D< BCTypeFlux_mitState,
                             BCBuckleyLeverett1D<BCTypeDirichlet_mitState, PDEClass> > BCClass;

    typedef BCClass::template ArrayQ<Real> ArrayQ;

    typedef DLA::MatrixSymS<1, Real> ParamType;

    // PDE
    RelPermModel_PowerLaw kr_model(2);
    const Real phi = 0.3;
    const Real uT = 0.3;
    const Real mu_w = 1;
    const Real mu_n = 2;

    const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
    const Real K = conversion*200;
    CapillaryModel_Linear cap_model(5);

    const int order = 1;
    const bool hasSpaceTimeDiffusion = false;
    PDEClass pdeAV(order, hasSpaceTimeDiffusion, phi, uT, kr_model, mu_w, mu_n, K, cap_model);

    ParamType H = {{0.1}}; // grid spacing
    ParamType param = log(H);

    const Real tol = 1.e-13;

    ArrayQ bcdata = {0.7, 0.0};

    Real Cdiff = 3.0;
    BCClass bc( Cdiff, pdeAV, bcdata );

    Real x = 0;
    Real time = 0;
    Real nx = 0.8;

    Real hn = nx*H(0,0)*nx;

    ArrayQ qI = {0.5, 0.3};
    ArrayQ qB = 0;

    bc.state(param, x, time, nx, qI, qB );
    BOOST_CHECK_CLOSE( qB(0), bcdata(0), tol );
    BOOST_CHECK_CLOSE( qB(1), qI(1), tol );

    BOOST_CHECK_EQUAL( bc.isValidState(nx,qI), true);

    ArrayQ qIx = {0.25, 0.43};

    ArrayQ Fx_true = 0, Fn_true = 0;
    pdeAV.fluxAdvectiveUpwind(param, x, time, qI, qB, nx, Fn_true);
    pdeAV.fluxViscous(param, x, time, qB, qIx, Fx_true);
    Fn_true += Fx_true*nx;

    ArrayQ Fn = 0.1;
    bc.fluxNormal(param, x, time, nx, qI, qIx, qB, Fn);

    BOOST_CHECK_CLOSE( Fn(0), Fn_true(0) + 0.1, tol );
    BOOST_CHECK_CLOSE( Fn(1), sqrt(Cdiff)*hn*(qI(1) - 0.0), tol );
  }
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeLinearRobin_test )
{
  {
    typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;
    typedef BCmitAVSensor1D< BCTypeLinearRobin,
                                     BCBuckleyLeverett1D<BCTypeLinearRobin, PDEClass> > BCClass;

    typedef BCClass::template ArrayQ<Real> ArrayQ;
    typedef BCClass::template MatrixQ<Real> MatrixQ;

    typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

    const Real tol = 1.e-13;

    ParamType param(0.1, 0.2); // grid spacing and jump values

    Real A = 1;
    Real B = 0.2;
    Real sensor = 0.1;
    Real bcdata = 0.7;

    BCClass bc( sensor, A, B, bcdata );

    MatrixQ AMtx;
    MatrixQ BMtx;
    ArrayQ bcdataVec;

    Real x = 0;
    Real time = 0;
    Real nx = 0.8;

    AMtx = 0;
    BMtx = 0;
    bc.coefficients( param, x, time, nx, AMtx, BMtx );
    BOOST_CHECK_CLOSE( A, AMtx, tol );
    BOOST_CHECK_CLOSE( B, BMtx, tol );

    bcdataVec = 0;
    bc.data( param, x, time, nx, bcdataVec );
    BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );
  }

  //Extended state vector
  {
    typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverettArtificialViscosity, TraitsModelClass> PDEClass;
    typedef BCmitAVSensor1D< BCTypeLinearRobin,
                                     BCBuckleyLeverett1D<BCTypeLinearRobin, PDEClass> > BCClass;

    typedef BCClass::template ArrayQ<Real> ArrayQ;
    typedef BCClass::template MatrixQ<Real> MatrixQ;

    typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

    const Real tol = 1.e-13;

    ParamType param(0.1, 0.2); // grid spacing and jump values

    Real A = 1;
    Real B = 0.2;
    Real sensor = 0.1;
    Real bcdata = 0.7;

    BCClass bc( sensor, A, B, bcdata );

    MatrixQ AMtx;
    MatrixQ BMtx;
    ArrayQ bcdataVec;

    Real x = 0;
    Real time = 0;
    Real nx = 0.8;

    AMtx = 0;
    BMtx = 0;
    bc.coefficients( param, x, time, nx, AMtx, BMtx );
    BOOST_CHECK_CLOSE( A, AMtx, tol );
    BOOST_CHECK_CLOSE( B, BMtx, tol );

    bcdataVec = 0;
    bc.data( param, x, time, nx, bcdataVec );
    BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );
  }
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFunction_test )
{
  {
    typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;
    typedef BCmitAVSensor1D< BCTypeFlux_mitState,
                             BCBuckleyLeverett1D<BCTypeFunction_mitState, PDEClass> > BCClass;

    typedef BCClass::template ArrayQ<Real> ArrayQ;

    typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

    // PDE
    RelPermModel_PowerLaw kr_model(2);
    const Real phi = 0.3;
    const Real uT = 0.3;
    const Real mu_w = 1;
    const Real mu_n = 2;

    const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
    const Real K = conversion*200;
    CapillaryModel_Linear cap_model(5);

    const int order = 1;
    const bool hasSpaceTimeDiffusion = false;
    PDEClass pdeAV(order, hasSpaceTimeDiffusion, phi, uT, kr_model, mu_w, mu_n, K, cap_model);

    ParamType param(0.1, 0.2); // grid spacing and jump values

    const Real tol = 1.e-13;

    typedef ScalarFunction1D_Sine SolutionType;
    std::shared_ptr<SolutionType> slnExact( new SolutionType );

    Real Cdiff = 3.0;
    BCClass bc( Cdiff, pdeAV, slnExact );

    Real x = 0;
    Real time = 0;
    Real nx = 0.8;

    ArrayQ qI = 0.5;
    ArrayQ qB = 0;
    ArrayQ bcdata = (*slnExact)(x, time);

    bc.state(param, x, time, nx, qI, qB );
    BOOST_CHECK_CLOSE( qB, bcdata, tol );

    BOOST_CHECK_EQUAL( bc.isValidState(nx,qI), true);
  }

  //Extended state
  {
    typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverettArtificialViscosity, TraitsModelClass> PDEClass;
    typedef BCmitAVSensor1D< BCTypeFlux_mitState,
                             BCBuckleyLeverett1D<BCTypeFunction_mitState, PDEClass> > BCClass;

    typedef BCClass::template ArrayQ<Real> ArrayQ;

    typedef DLA::MatrixSymS<1, Real> ParamType;

    // PDE
    RelPermModel_PowerLaw kr_model(2);
    const Real phi = 0.3;
    const Real uT = 0.3;
    const Real mu_w = 1;
    const Real mu_n = 2;

    const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
    const Real K = conversion*200;
    CapillaryModel_Linear cap_model(5);

    const int order = 1;
    const bool hasSpaceTimeDiffusion = false;
    PDEClass pdeAV(order, hasSpaceTimeDiffusion, phi, uT, kr_model, mu_w, mu_n, K, cap_model);

    ParamType H = {{0.1}}; // grid spacing
    ParamType param = log(H);

    const Real tol = 1.e-13;

    typedef ScalarFunction1D_Sine SolutionType;
    std::shared_ptr<SolutionType> slnExact( new SolutionType );

    Real Cdiff = 3.0;
    BCClass bc( Cdiff, pdeAV, slnExact );

    Real x = 0;
    Real time = 0;
    Real nx = 0.8;

    ArrayQ qI = {0.5, 0.2};
    ArrayQ qB = 0;
    ArrayQ bcdata = {(*slnExact)(x, time), 0.0};

    bc.state(param, x, time, nx, qI, qB );
    BOOST_CHECK_CLOSE( qB(0), bcdata(0), tol );
    BOOST_CHECK_CLOSE( qB(1), qI(1), tol );

    BOOST_CHECK_EQUAL( bc.isValidState(nx,qI), true);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeTimeIC_test )
{
  {
    typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;
    typedef BCmitAVSensor1D< BCTypeFlux_mitStateSpaceTime,
                             BCBuckleyLeverett1D<BCTypeTimeIC, PDEClass> > BCClass;

    typedef BCClass::template ArrayQ<Real> ArrayQ;

    typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

    // PDE
    RelPermModel_PowerLaw kr_model(2);
    const Real phi = 0.3;
    const Real uT = 0.3;
    const Real mu_w = 1;
    const Real mu_n = 2;

    const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
    const Real K = conversion*200;
    CapillaryModel_Linear cap_model(5);

    const int order = 1;
    const bool hasSpaceTimeDiffusion = false;
    PDEClass pdeAV(order, hasSpaceTimeDiffusion, phi, uT, kr_model, mu_w, mu_n, K, cap_model);

    ParamType param(0.1, 0.2); // grid spacing and jump values

    const Real tol = 1.e-13;

    ArrayQ bcdata = 0.7;
    Real Cdiff = 3.0;

    BCClass bc( Cdiff, pdeAV, bcdata );

    Real x = 0;
    Real time = 0;
    Real nx = 0.8;
    Real nt = 0.2;

    ArrayQ qI = 0.5;
    ArrayQ qB = 0;

    bc.state(param, x, time, nx, nt, qI, qB );
    BOOST_CHECK_CLOSE( qB, bcdata, tol );

    BOOST_CHECK_EQUAL( bc.isValidState(nx, nt, qI), true);
  }

  //Extended state vector
  {
    typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverettArtificialViscosity, TraitsModelClass> PDEClass;
    typedef BCmitAVSensor1D< BCTypeFlux_mitStateSpaceTime,
                             BCBuckleyLeverett1D<BCTypeTimeIC, PDEClass> > BCClass;

    typedef BCClass::template ArrayQ<Real> ArrayQ;

    typedef DLA::MatrixSymS<2, Real> ParamType;

    // PDE
    RelPermModel_PowerLaw kr_model(2);
    const Real phi = 0.3;
    const Real uT = 0.3;
    const Real mu_w = 1;
    const Real mu_n = 2;

    const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
    const Real K = conversion*200;
    CapillaryModel_Linear cap_model(5);

    const int order = 1;
    const bool hasSpaceTimeDiffusion = false;
    PDEClass pdeAV(order, hasSpaceTimeDiffusion, phi, uT, kr_model, mu_w, mu_n, K, cap_model);

    ParamType H = {{0.3},{0.1,0.2}}; // grid spacing
    ParamType param = log(H);

    const Real tol = 1.e-13;

    ArrayQ bcdata = {0.7, 0.0};

    Real Cdiff = 3.0;
    BCClass bc( Cdiff, pdeAV, bcdata );

    Real x = 0;
    Real time = 0;
    Real nx = 0.8;
    Real nt = 0.2;

    Real hn = nx*(H(0,0)*nx + H(0,1)*nt) + nt*(H(1,0)*nx + H(1,1)*nt);

    ArrayQ qI = {0.5, 0.3};
    ArrayQ qB = 0;

    bc.state(param, x, time, nx, nt, qI, qB );
    BOOST_CHECK_CLOSE( qB(0), bcdata(0), tol );
    BOOST_CHECK_CLOSE( qB(1), qI(1), tol );

    BOOST_CHECK_EQUAL( bc.isValidState(nx, nt, qI), true);

    ArrayQ qIx = {0.25, 0.43};
    ArrayQ qIt = {0.83,-0.15};

    ArrayQ Fn_true = 0;
    ArrayQ uB = 0;
    pdeAV.fluxAdvectiveTime(param, x, time, qB, uB);
    Fn_true += nt*uB;

    ArrayQ Fn = 0.1;
    bc.fluxNormalSpaceTime(param, x, time, nx, nt, qI, qIx, qIt, qB, Fn);

    BOOST_CHECK_CLOSE( Fn(0), Fn_true(0) + 0.1, tol );
    BOOST_CHECK_CLOSE( Fn(1), sqrt(Cdiff)*hn*(qI(1) - 0.0), tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/BCBuckleyLeverett_ArtificialViscosity1D_pattern.txt", true );

  typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;

  typedef BCParameters< BCBuckleyLeverett_ArtificialViscosity1DVector<TraitsSizeBuckleyLeverett, TraitsModelClass> > BCParams;

  // PDE
  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  CapillaryModel_Linear cap_model(5);

  const int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEClass pdeAV(order, hasSpaceTimeDiffusion, phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  {
    typedef BCNone<PhysD1,TraitsSizeBuckleyLeverett<PhysD1>::N> BCClass;

    //Pydict constructor
    PyDict BCNone;
    BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

    BCClass bc(pdeAV, BCNone);
    bc.dump( 2, output );
  }

  {
    typedef BCmitAVSensor1D< BCTypeFlux_mitState,
                             BCBuckleyLeverett1D<BCTypeDirichlet_mitState, PDEClass> > BCClass;

    Real bcdata = 0.7;
    Real Cdiff = 3.0;
    BCClass bc( Cdiff, pdeAV, bcdata );
    bc.dump( 2, output );
  }

  {
    typedef BCmitAVSensor1D< BCTypeFlux_mitState,
                             BCBuckleyLeverett1D<BCTypeFunction_mitState, PDEClass> > BCClass;

    typedef ScalarFunction1D_Sine SolutionType;
    std::shared_ptr<SolutionType> slnExact( new SolutionType );

    Real Cdiff = 3.0;
    BCClass bc( Cdiff, pdeAV, slnExact );
    bc.dump( 2, output );
  }

  {
    typedef BCmitAVSensor1D< BCTypeFlux_mitStateSpaceTime,
                             BCBuckleyLeverett1D<BCTypeTimeIC, PDEClass> > BCClass;

    Real bcdata = 0.7;
    Real Cdiff = 3.0;
    BCClass bc( Cdiff, pdeAV, bcdata );
    bc.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
