// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDETwoPhaseLinearized1D_btest
//
// testing of 1-D linearized two phase PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/PorousMedia/Q1DPrimitive_pnSw.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/PDETwoPhaseLinearized1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"

//Explicitly instantiate the class so coverage information is correct
namespace SANS
{

template class PDETwoPhaseLinearized1D<QTypePrimitive_pnSw, RelPermModel_PowerLaw, RelPermModel_PowerLaw,
                                       CapillaryModel_Linear>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDETwoPhaseLinearized1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test_pnSw )
{
  {
    typedef PDETwoPhaseLinearized1D<QTypePrimitive_pnSw, RelPermModel_PowerLaw, RelPermModel_PowerLaw,
                                    CapillaryModel_Linear> PDEClass;

    BOOST_CHECK( PDEClass::D == 1 );
    BOOST_CHECK( PDEClass::N == 2 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_pnSw )
{
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef PDETwoPhaseLinearized1D<QTypePrimitive_pnSw, RelPermModel_PowerLaw, RelPermModel_PowerLaw,
                                  CapillaryModel_Linear> PDEClass;
  Real tol = 1e-13;

  Real rhow = 52.4;
  Real rhon = 52.1;
  Real phi = 0.3;

  RelPermModel krw(2);
  RelPermModel krn(2);

  Real muw = 1;
  Real mun = 2;

  CapillaryModel pc(5);
  const Real K = 0.25;

  Real Swbar = 0.5;
  Real pxbar = -0.25;

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc, pxbar, Swbar);

  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasSourceLiftedQuantity() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );
  BOOST_CHECK_CLOSE( pde.permeability(), K, tol);

  //Copy constructor
  PDEClass pde2(pde);
  BOOST_CHECK( pde2.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde2.hasFluxAdvective() == true );
  BOOST_CHECK( pde2.hasFluxViscous() == true );
  BOOST_CHECK( pde2.hasSource() == false );
  BOOST_CHECK( pde2.hasSourceLiftedQuantity() == false );
  BOOST_CHECK( pde2.hasForcingFunction() == false );
  BOOST_CHECK( pde2.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde2.needsSolutionGradientforSource() == false );
  BOOST_CHECK_CLOSE( pde2.permeability(), K, tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setDOFFrom )
{
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef PDETwoPhaseLinearized1D<QTypePrimitive_pnSw, RelPermModel_PowerLaw, RelPermModel_PowerLaw,
                                  CapillaryModel_Linear> PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;

  Real tol = 1e-13;

  Real rhow = 52.4;
  Real rhon = 52.1;
  Real phi = 0.3;

  RelPermModel krw(2);
  RelPermModel krn(2);

  Real muw = 1;
  Real mun = 2;

  CapillaryModel pc(5);
  const Real K = 0.25;

  Real Swbar = 0.5;
  Real pxbar = -0.25;

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc, pxbar, Swbar);

  Real pn = 2534.8;
  Real Sw = 0.784;

  // set
  Real qDataPrim[2] = {pn,Sw};
  string qNamePrim[2] = {"pn","Sw"};
  ArrayQ qTrue = {pn,Sw};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 2 );
  BOOST_CHECK_CLOSE( qTrue[0], q[0], tol );
  BOOST_CHECK_CLOSE( qTrue[1], q[1], tol );

  PressureNonWet_SaturationWet<Real> qdata1;
  qdata1.pn = pn; qdata1.Sw = Sw;
  q = 0;
  pde.setDOFFrom( q, qdata1 );
  BOOST_CHECK_CLOSE( qTrue[0], q[0], tol );
  BOOST_CHECK_CLOSE( qTrue[1], q[1], tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux_pnSw )
{
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef PDETwoPhaseLinearized1D<QTypePrimitive_pnSw, RelPermModel_PowerLaw, RelPermModel_PowerLaw,
                                  CapillaryModel_Linear> PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Real rhow = 52.4;
  Real rhon = 52.1;
  Real phi = 0.3;

  RelPermModel krw_model(2);
  RelPermModel krn_model(2);

  Real muw = 1;
  Real mun = 2;

  Real pcmax = 1.0;
  CapillaryModel pc_model(pcmax);
  const Real K = 0.25;

  Real Swbar = 0.5;
  Real pxbar = -0.25;

  PDEClass pde(rhow, rhon, phi, krw_model, krn_model, muw, mun, K, pc_model, pxbar, Swbar);

  // Static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 2 );

  // Flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // Function tests
  Real x = 0, time = 0;   // not actually used in functions
  Real nx = 0.35;

  Real SwL = 0.375, SwR = 0.462;
  Real SwxL = -0.63, SwxR = -0.37;
  Real SwtL = 0.27;
  Real pnL = 25.34, pnR = 31.86;
  Real pnxL = -0.52, pnxR = -0.29;
  Real pntL = -0.31;

  Real SnL = 1.0 - SwL;
//  Real SnR = 1.0 - SwR;
  Real pcL = pcmax*SnL;
//  Real pcR = pcmax*SnR;
  Real pwL = pnL - pcL;
//  Real pwR = pnR - pcR;
  Real pwxL = pnxL - pcmax*(-SwxL);
  Real pwxR = pnxR - pcmax*(-SwxR);

  Real krwbar = pow(Swbar, 2);
  Real krnbar = pow(1 - Swbar, 2);

  Real lambda_w = krwbar/muw;
  Real lambda_n = krnbar/mun;

  Real lambda_w_Sw = 2*Swbar/muw;
  Real lambda_n_Sw = -2*(1 - Swbar)/mun;

  // Set
  Real qLDataPrim[2] = {pnL,SwL};
  Real qRDataPrim[2] = {pnR,SwR};
  string qNamePrim[2] = {"pn","Sw"};
  ArrayQ qL, qR;
  pde.setDOFFrom( qL, qLDataPrim, qNamePrim, 2 );
  pde.setDOFFrom( qR, qRDataPrim, qNamePrim, 2 );
  ArrayQ qxL = {pnxL, SwxL};
  ArrayQ qxR = {pnxR, SwxR};

  SANS_CHECK_CLOSE( pnL, qL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( SwL, qL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( pnR, qR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( SwR, qR[1], small_tol, close_tol );

  Real Sw_test, Sn_test, pw_test, pn_test;
  pde.variableInterpreter().eval(qL, pw_test, pn_test, Sw_test, Sn_test);
  SANS_CHECK_CLOSE( pwL, pw_test, small_tol, close_tol );
  SANS_CHECK_CLOSE( pnL, pn_test, small_tol, close_tol );
  SANS_CHECK_CLOSE( SwL, Sw_test, small_tol, close_tol );
  SANS_CHECK_CLOSE( SnL, Sn_test, small_tol, close_tol );

  // Master State
  ArrayQ ucons_true, ucons;
  ucons_true[0] =  rhow*phi*SwL;
  ucons_true[1] = -rhon*phi*SwL;
  pde.masterState( x, time, qL, ucons );
  SANS_CHECK_CLOSE( ucons_true[0], ucons[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( ucons_true[1], ucons[1], small_tol, close_tol );

  // Master state jacobian
  MatrixQ fcons_jac_true, fcons_jac;
  fcons_jac_true(0,0) = 0.0;
  fcons_jac_true(0,1) = phi*rhow;
  fcons_jac_true(1,0) = 0.0;
  fcons_jac_true(1,1) = -phi*rhon;
  pde.jacobianMasterState( x, time, qL, fcons_jac );
  SANS_CHECK_CLOSE( fcons_jac_true(0,0), fcons_jac(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_jac_true(0,1), fcons_jac(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_jac_true(1,0), fcons_jac(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_jac_true(1,1), fcons_jac(1,1), small_tol, close_tol );

  // Temporal flux
  ArrayQ fcons_true, fcons = 0;
  fcons_true[0] =  rhow*phi*SwL;
  fcons_true[1] = -rhon*phi*SwL;
  pde.fluxAdvectiveTime( x, time, qL, fcons );
  SANS_CHECK_CLOSE( fcons_true[0], fcons[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_true[1], fcons[1], small_tol, close_tol );

  // Advective flux
  ArrayQ fadvL_true;
  fadvL_true(0) = -rhow*K*lambda_w_Sw*SwL*pxbar;
  fadvL_true(1) = -rhon*K*lambda_n_Sw*SwL*pxbar;
  ArrayQ fadv = 0;
  pde.fluxAdvective( x, time, qL, fadv );
  SANS_CHECK_CLOSE( fadvL_true[0], fadv[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fadvL_true[1], fadv[1], small_tol, close_tol );

  ArrayQ fadvR_true;
  fadvR_true(0) = -rhow*K*lambda_w_Sw*SwR*pxbar;
  fadvR_true(1) = -rhon*K*lambda_n_Sw*SwR*pxbar;
  ArrayQ fn_adv = 0;
  ArrayQ fn_adv_true = 0.5*(fadvL_true + fadvR_true)*nx;
  pde.fluxAdvectiveUpwind( x, time, qL, qR, nx, fn_adv );
  SANS_CHECK_CLOSE( fn_adv_true[0], fn_adv[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fn_adv_true[1], fn_adv[1], small_tol, close_tol );

  // Jacobian of advective flux
  MatrixQ fadv_u_true = 0.0;
  fadv_u_true(0,1) = -rhow*K*lambda_w_Sw*pxbar;
  fadv_u_true(1,1) = -rhon*K*lambda_n_Sw*pxbar;
  MatrixQ fadv_u = 0;
  pde.jacobianFluxAdvective( x, time, qL, fadv_u );
  SANS_CHECK_CLOSE( fadv_u_true(0,0), fadv_u(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fadv_u_true(0,1), fadv_u(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fadv_u_true(1,0), fadv_u(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fadv_u_true(1,1), fadv_u(1,1), small_tol, close_tol );

  // Viscous flux
  ArrayQ fviscL_true, fvisc = 0;
  fviscL_true[0] = -rhow*K*lambda_w*pwxL;
  fviscL_true[1] = -rhon*K*lambda_n*pnxL;
  pde.fluxViscous( x, time, qL, qxL, fvisc );
  SANS_CHECK_CLOSE( fviscL_true[0], fvisc[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fviscL_true[1], fvisc[1], small_tol, close_tol );

  //Viscous flux with upwinding
  ArrayQ fviscR_true;
  fviscR_true[0] = -rhow*K*lambda_w*pwxR;
  fviscR_true[1] = -rhon*K*lambda_n*pnxR;
  Real c = (lambda_w_Sw * lambda_n - lambda_w * lambda_n_Sw) / (lambda_w + lambda_n) * K * pxbar * nx;
  Real g = 0.5 * fabs(c) * (SwL - SwR);
  ArrayQ fn_true = 0.5*(fviscL_true + fviscR_true)*nx;
  fn_true(0) += rhow*g;
  fn_true(1) -= rhon*g;

  ArrayQ fn = 0.0;
  pde.fluxViscous( x, time, qL, qxL, qR, qxR, nx, fn );
  SANS_CHECK_CLOSE( fn_true[0], fn[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fn_true[1], fn[1], small_tol, close_tol );

  // Space-time viscous flux
  fvisc = 0;
  ArrayQ gvisc = 0;
  ArrayQ gviscL_true = {0.0, 0.0};
  ArrayQ qtL = {pntL, SwtL};
  pde.fluxViscousSpaceTime( x, time, qL, qxL, qtL, fvisc, gvisc );
  SANS_CHECK_CLOSE( fviscL_true[0], fvisc[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fviscL_true[1], fvisc[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( gviscL_true[0], gvisc[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( gviscL_true[1], gvisc[1], small_tol, close_tol );

  // Viscous diffusion matrix
  MatrixQ kxx = 0;
  BOOST_CHECK_THROW( pde.diffusionViscous( x, time, qL, qxL, kxx ), DeveloperException );

  // Space-time viscous diffusion matrix
  MatrixQ kxt = 0;
  MatrixQ ktx = 0;
  MatrixQ ktt = 0;
  BOOST_CHECK_THROW( pde.diffusionViscousSpaceTime( x, time, qL, qxL, qtL, kxx, kxt, ktx, ktt ), DeveloperException );

  //Source
  ArrayQ source = 0.0;
  pde.source( x, time, qL, qxL, source );
  SANS_CHECK_CLOSE( 0.0, source[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, source[1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( interpResid )
{
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef PDETwoPhaseLinearized1D<QTypePrimitive_pnSw, RelPermModel_PowerLaw, RelPermModel_PowerLaw,
                                  CapillaryModel_Linear> PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;

  Real tol = 1e-13;

  Real rhow = 52.4;
  Real rhon = 52.1;
  Real phi = 0.3;

  RelPermModel krw(2);
  RelPermModel krn(2);

  Real muw = 1;
  Real mun = 2;

  CapillaryModel pc(5);
  const Real K = 0.25;

  Real Swbar = 0.5;
  Real pxbar = -0.25;

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc, pxbar, Swbar);

  Real pn = 2534.8;
  Real Sw = 0.784;

  // set
  Real qDataPrim[2] = {pn,Sw};
  string qNamePrim[2] = {"pn","Sw"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 2 );

  BOOST_CHECK_EQUAL(pde.nMonitor(), 2);

  DLA::VectorD<Real> vec(2);
  pde.interpResidVariable(q, vec);
  for (int i = 0; i < ArrayQ::M; i++)
    SANS_CHECK_CLOSE( q(i), vec(i), tol, tol );

  vec = 0;
  pde.interpResidGradient(q, vec);
  for (int i = 0; i < ArrayQ::M; i++)
    SANS_CHECK_CLOSE( q(i), vec(i), tol, tol );

  vec = 0;
  pde.interpResidBC(q, vec);
  for (int i = 0; i < ArrayQ::M; i++)
    SANS_CHECK_CLOSE( q(i), vec(i), tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/PDETwoPhaseLinearized1D_pattern.txt", true );

  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef PDETwoPhaseLinearized1D<QTypePrimitive_pnSw, RelPermModel_PowerLaw, RelPermModel_PowerLaw,
                                  CapillaryModel_Linear> PDEClass;

  Real rhow = 52.4;
  Real rhon = 52.1;
  Real phi = 0.3;

  RelPermModel krw_model(2);
  RelPermModel krn_model(2);

  Real muw = 1;
  Real mun = 2;

  Real pcmax = 1.0;
  CapillaryModel pc_model(pcmax);
  const Real K = 0.25;

  Real Swbar = 0.5;
  Real pxbar = -0.25;

  PDEClass pde(rhow, rhon, phi, krw_model, krn_model, muw, mun, K, pc_model, pxbar, Swbar);

  pde.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
