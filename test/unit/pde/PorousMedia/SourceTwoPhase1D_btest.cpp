// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// SourceTwoPhase1D_btest
//
// testing of 1D two phase source class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/PorousMedia/Q1DPrimitive_pnSw.h"
#include "pde/PorousMedia/TraitsTwoPhase.h"
#include "pde/PorousMedia/SourceTwoPhase1D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/CapillaryModel.h"

//Explicitly instantiate the class so coverage information is correct
namespace SANS
{

}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( SourceTwoPhase1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Source1D_FixedWellPressure_ctor )
{
  typedef DensityModel_Comp DensityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef Q1D<QTypePrimitive_pnSw, CapillaryModel, TraitsSizeTwoPhase> QInterpreter;
  typedef SourceTwoPhase1D_FixedWellPressure<QInterpreter, DensityModel, DensityModel,
                                             RelPermModel, RelPermModel, ViscModel, ViscModel, CapillaryModel> SourceClass;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  CapillaryModel pc(5);
  const Real K = 0.25;

  const Real tol = 1e-13;

  // Set up PyDicts
  PyDict fixedwellpressure1;
  fixedwellpressure1[SourceTwoPhase1DType_FixedWellPressure_Param::params.pB] = 2350.0;
  fixedwellpressure1[SourceTwoPhase1DParam::params.Source.SourceType] = SourceTwoPhase1DParam::params.Source.FixedPressure;

  PyDict fixedwellpressure2;
  fixedwellpressure2[SourceTwoPhase1DType_FixedWellPressure_Param::params.pB] = 1830.0;
  fixedwellpressure2[SourceTwoPhase1DParam::params.Source.SourceType] = SourceTwoPhase1DParam::params.Source.FixedPressure;

  PyDict well1;
  well1[SourceTwoPhase1DParam::params.Source] = fixedwellpressure1;
  well1[SourceTwoPhase1DParam::params.xmin] = 10.0;
  well1[SourceTwoPhase1DParam::params.xmax] = 15.0;
  well1[SourceTwoPhase1DParam::params.Tmin] = 1.0;
  well1[SourceTwoPhase1DParam::params.Tmax] = 3.0;
  well1[SourceTwoPhase1DParam::params.smoothLx] = 1.5;
  well1[SourceTwoPhase1DParam::params.smoothT] = 0.0;

  SourceClass source1(well1, rhow, rhon, krw, krn, muw, mun, K, pc);

  BOOST_CHECK_CLOSE( source1.getWellPressure(), 2350.0, tol);

  BOOST_CHECK_CLOSE( source1.getWeight(5.0, 0.0), 0.0, tol);
  BOOST_CHECK_CLOSE( source1.getWeight(5.0, 2.0), 0.0, tol);
  BOOST_CHECK_CLOSE( source1.getWeight(8.5, 2.0), 0.0, tol);
  BOOST_CHECK_CLOSE( source1.getWeight(10.0, 2.0), 0.5, tol);
  BOOST_CHECK_CLOSE( source1.getWeight(11.5, 2.0), 1.0, tol);
  BOOST_CHECK_CLOSE( source1.getWeight(13.5, 2.0), 1.0, tol);
  BOOST_CHECK_CLOSE( source1.getWeight(15.0, 2.0), 0.5, tol);
  BOOST_CHECK_CLOSE( source1.getWeight(16.5, 2.0), 0.0, tol);

  BOOST_CHECK_CLOSE( source1.getWeight(12.0, 0.99), 0.0, tol);
  BOOST_CHECK_CLOSE( source1.getWeight(12.0, 1.0), 1.0, tol);
  BOOST_CHECK_CLOSE( source1.getWeight(12.0, 2.0), 1.0, tol);
  BOOST_CHECK_CLOSE( source1.getWeight(12.0, 3.0), 1.0, tol);
  BOOST_CHECK_CLOSE( source1.getWeight(12.0, 3.01), 0.0, tol);

  PyDict well2;
  well2[SourceTwoPhase1DParam::params.Source] = fixedwellpressure1;
  well2[SourceTwoPhase1DParam::params.xmin] = 25.0;
  well2[SourceTwoPhase1DParam::params.xmax] = 50.0;
  well2[SourceTwoPhase1DParam::params.Tmin] = 5.0;
  well2[SourceTwoPhase1DParam::params.Tmax] = 10.0;
  well2[SourceTwoPhase1DParam::params.smoothLx] = 2.0;
  well2[SourceTwoPhase1DParam::params.smoothT] = 0.0;

  PyDict sourceList;
  sourceList["well1"] = well1;
  sourceList["well2"] = well2;
  SourceTwoPhase1DListParam::checkInputs(sourceList);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Source1D_FixedWellPressure_eval )
{
  typedef DensityModel_Comp DensityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef Q1D<QTypePrimitive_pnSw, CapillaryModel, TraitsSizeTwoPhase> QInterpreter;
  typedef SourceTwoPhase1D_FixedWellPressure<QInterpreter, DensityModel, DensityModel,
                                             RelPermModel, RelPermModel, ViscModel, ViscModel, CapillaryModel> SourceClass;

  typedef SourceClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  const Real pref = 1.47;
  const Real pB = 15.82;

  const Real rhow_ref = 62.4;
  const Real rhon_ref = 52.1;
  const Real Cw = 0.03;
  const Real Cn = 0.05;

  const Real muw = 1.0, mun = 2.0;
  const Real pcmax = 5.0;

  DensityModel rhow_model(rhow_ref, Cw, pref);
  DensityModel rhon_model(rhon_ref, Cn, pref);

  RelPermModel krw_model(2);
  RelPermModel krn_model(2);

  ViscModel muw_model(muw);
  ViscModel mun_model(mun);

  CapillaryModel pc_model(pcmax);
  const Real K = 0.25;

  // Set up PyDicts
  PyDict fixedwellpressure1;
  fixedwellpressure1[SourceTwoPhase1DType_FixedWellPressure_Param::params.pB] = pB;
  fixedwellpressure1[SourceTwoPhase1DParam::params.Source.SourceType] = SourceTwoPhase1DParam::params.Source.FixedPressure;

  PyDict well1;
  well1[SourceTwoPhase1DParam::params.Source] = fixedwellpressure1;
  well1[SourceTwoPhase1DParam::params.xmin] = 10.0;
  well1[SourceTwoPhase1DParam::params.xmax] = 15.0;
  well1[SourceTwoPhase1DParam::params.Tmin] = 1.0;
  well1[SourceTwoPhase1DParam::params.Tmax] = 3.0;
  well1[SourceTwoPhase1DParam::params.smoothLx] = 1.0;
  well1[SourceTwoPhase1DParam::params.smoothT] = 0.2;

  SourceClass source1(well1, rhow_model, rhon_model, krw_model, krn_model, muw_model, mun_model, K, pc_model);

  Real Sw = 0.375;
  Real pn = 25.34;

  Real Sn = 1.0 - Sw;
  Real pc = pcmax*Sn;
  Real pw = pn - pc;

  Real krw = pow(Sw,2);
  Real krn = pow(1-Sw,2);

  Real rhow = rhow_ref*exp(Cw*(pw - pref));
  Real rhon = rhon_ref*exp(Cn*(pn - pref));

  Real lambda_w = krw/muw;
  Real lambda_n = krn/mun;

  Real Ls = 5.0;
  Real Lchar = 0.25*Ls;
  Real dpndx = (pn - pB)/Lchar;

  Real x = 9.5;
  Real t = 2.0;
  Real weight = 0.0;

  // Set
  ArrayQ q = {pn,Sw};
  ArrayQ qx = {0.7, 0.5};
  ArrayQ sourceTrue, source = 0.0;
  sourceTrue[0] = rhow*lambda_w*K*dpndx/Ls*weight;
  sourceTrue[1] = rhon*lambda_n*K*dpndx/Ls*weight;

  source1.source(x, t, q, qx, source);
  SANS_CHECK_CLOSE( sourceTrue[0], source[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( sourceTrue[1], source[1], small_tol, close_tol);

  x = 10.0; t = 2.0; weight = 0.5;
  sourceTrue[0] = rhow*lambda_w*K*dpndx/Ls*weight;
  sourceTrue[1] = rhon*lambda_n*K*dpndx/Ls*weight;
  source = 0.0;
  source1.source(x, t, q, qx, source);
  SANS_CHECK_CLOSE( sourceTrue[0], source[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( sourceTrue[1], source[1], small_tol, close_tol);

  x = 12.0; t = 3.0; weight = 0.5;
  sourceTrue[0] = rhow*lambda_w*K*dpndx/Ls*weight;
  sourceTrue[1] = rhon*lambda_n*K*dpndx/Ls*weight;
  source = 0.0;
  source1.source(x, t, q, qx, source);
  SANS_CHECK_CLOSE( sourceTrue[0], source[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( sourceTrue[1], source[1], small_tol, close_tol);

  x = 10.25; t = 2.98; weight = 0.84375*0.6480;
  sourceTrue[0] = rhow*lambda_w*K*dpndx/Ls*weight;
  sourceTrue[1] = rhon*lambda_n*K*dpndx/Ls*weight;
  source = 0.0;
  source1.source(x, t, q, qx, source);
  SANS_CHECK_CLOSE( sourceTrue[0], source[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( sourceTrue[1], source[1], small_tol, close_tol);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
