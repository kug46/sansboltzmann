// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PorosityModel_btest
// testing of PorosityModel classes

#include <fstream>
#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/PorousMedia/PorosityModel.h"


using namespace std;
using namespace SANS;

namespace SANS
{

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( PorosityModel_Constant_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Constructor )
{
  const Real tol = 1.e-13;
  Real phi = 0.3;

  PyDict d;
  d[PorosityModel_Constant_Params::params.phi] = phi;

  //Check dictionary
  PorosityModel_Constant_Params::checkInputs(d);

  Real dummy_x = 0.0, dummy_y = 0.0, dummy_time = 0.0;
  Real pn = 2500.0;

  // PyDict constructor
  PorosityModel_Constant phi_model(d);
  BOOST_CHECK_CLOSE( phi_model.porosity(dummy_x, dummy_time, pn), phi, tol );
  BOOST_CHECK_CLOSE( phi_model.porosity(dummy_x, dummy_y, dummy_time, pn), phi, tol );

  // copy constructor
  PorosityModel_Constant phi_model2(phi_model);
  BOOST_CHECK_CLOSE( phi_model2.porosity(dummy_x, dummy_time, pn), phi, tol);
  BOOST_CHECK_CLOSE( phi_model2.porosity(dummy_x, dummy_y, dummy_time, pn), phi, tol);

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Evaluations )
{
//  const Real tol = 1.e-13;
  Real phi = 0.3;

  PyDict d;
  d[PorosityModel_Constant_Params::params.phi] = phi;

  //Check dictionary
  PorosityModel_Constant_Params::checkInputs(d);

  Real dummy_x = 0.0, dummy_y = 0.0, dummy_time = 0.0;
  Real pn = 2500.0;
  Real pnx = -0.75;
  Real pny =  0.41;

  // PyDict constructor
  PorosityModel_Constant phi_model(d);

  Real phi_p, phix, phiy;

  //1D
  phi_model.porosityJacobian(dummy_x, dummy_time, pn, phi_p);
  phi_model.porosityGradient(dummy_x, dummy_time, pn, pnx, phix);
  BOOST_CHECK_EQUAL( phi_p, 0.0 );
  BOOST_CHECK_EQUAL( phix, 0.0 );

  //2D
  phi_model.porosityJacobian(dummy_x, dummy_y, dummy_time, pn, phi_p);
  phi_model.porosityGradient(dummy_x, dummy_y, dummy_time, pn, pnx, pny, phix, phiy);
  BOOST_CHECK_EQUAL( phi_p, 0.0 );
  BOOST_CHECK_EQUAL( phix, 0.0 );
  BOOST_CHECK_EQUAL( phiy, 0.0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/PorosityModel_Constant_pattern.txt", true );

  Real phi = 0.3;

  PyDict d;
  d[PorosityModel_Constant_Params::params.phi] = phi;

  // PyDict constructor
  PorosityModel_Constant phi_model(d);

  phi_model.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()


//############################################################################//
BOOST_AUTO_TEST_SUITE( PorosityModel_Compressible_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Constructor )
{
  const Real tol = 1.e-13;
  Real phi_ref = 0.3;
  Real Cr = 2e-5;
  Real pref = 14.7;

  PyDict d;
  d[PorosityModel_Comp_Params::params.phi_ref] = phi_ref;
  d[PorosityModel_Comp_Params::params.Cr] = Cr;

  //Check dictionary
  PorosityModel_Comp_Params::checkInputs(d);

  // PyDict constructor
  PorosityModel_Comp phi_model(d, pref);
  BOOST_CHECK_CLOSE( phi_model.phi_ref(), phi_ref, tol );
  BOOST_CHECK_CLOSE( phi_model.Cr(), Cr, tol );

  // copy constructor
  PorosityModel_Comp phi_model2(phi_model);
  BOOST_CHECK_CLOSE( phi_model2.phi_ref(), phi_ref, tol );
  BOOST_CHECK_CLOSE( phi_model2.Cr(), Cr, tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Evaluations )
{
  const Real tol = 1.e-13;
  Real phi_ref = 0.3;
  Real Cr = 2e-5;
  Real pref = 14.7;

  PyDict d;
  d[PorosityModel_Comp_Params::params.phi_ref] = phi_ref;
  d[PorosityModel_Comp_Params::params.Cr] = Cr;

  //Check dictionary
  PorosityModel_Comp_Params::checkInputs(d);

  // PyDict constructor
  PorosityModel_Comp phi_model(d, pref);

  Real dummy_x = 0.0, dummy_y = 0.0, dummy_time = 0.0;
  Real pn = 2500.0;
  Real pnx = -0.75;
  Real pny =  0.41;

  Real phi_true = phi_ref*exp(Cr*(pn - pref));
  Real phi_pn_true = Cr*phi_true;
  Real phix_true = phi_pn_true*pnx;
  Real phiy_true = phi_pn_true*pny;

  Real phi, phi_pn, phix, phiy;

  //1D
  phi = phi_model.porosity(dummy_x, dummy_time, pn);
  phi_model.porosityJacobian(dummy_x, dummy_time, pn, phi_pn);
  phi_model.porosityGradient(dummy_x, dummy_time, pn, pnx, phix);
  BOOST_CHECK_CLOSE( phi   , phi_true   , tol );
  BOOST_CHECK_CLOSE( phi_pn, phi_pn_true, tol );
  BOOST_CHECK_CLOSE( phix  , phix_true  , tol );

  //2D
  phi = phi_model.porosity(dummy_x, dummy_y, dummy_time, pn);
  phi_model.porosityJacobian(dummy_x, dummy_y, dummy_time, pn, phi_pn);
  phi_model.porosityGradient(dummy_x, dummy_y, dummy_time, pn, pnx, pny, phix, phiy);
  BOOST_CHECK_CLOSE( phi   , phi_true   , tol );
  BOOST_CHECK_CLOSE( phi_pn, phi_pn_true, tol );
  BOOST_CHECK_CLOSE( phix  , phix_true  , tol );
  BOOST_CHECK_CLOSE( phiy  , phiy_true  , tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/PorosityModel_Comp_pattern.txt", true );

  Real phi_ref = 0.3;
  Real Cr = 2e-5;
  Real pref = 14.7;

  PyDict d;
  d[PorosityModel_Comp_Params::params.phi_ref] = phi_ref;
  d[PorosityModel_Comp_Params::params.Cr] = Cr;

  // PyDict constructor
  PorosityModel_Comp phi_model(d, pref);

  phi_model.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
//cppcheck-suppress unknownMacro
BOOST_AUTO_TEST_SUITE_END()


//############################################################################//
BOOST_AUTO_TEST_SUITE( PorosityModel_CartTable_test_suite )

// create a simple Cartesian table file
void genCartTable( std::ofstream& file )
{
  int imax = 5;
  int jmax = 7;
  Real dx = 20.;
  Real dy = 30.;

  file << imax << " " << jmax << " " << dx << " " << dy << std::endl;
  for (int i = 0; i < imax; i++)
  {
    for (int j = 0; j < jmax; j++)
      file << i*jmax + j << " ";
    file << std::endl;
  }
  file.close();
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Constructor )
{
  std::string filename = "CartTable.txt";
  std::ofstream file(filename);
  genCartTable( file );

  Real Cr = 2e-5;
  Real pref = 14.7;

  PyDict d;
  d[PorosityModel_CartTable_Params::params.PorosityFile] = filename;
  d[PorosityModel_CartTable_Params::params.Cr] = Cr;

  // check dictionary
  PorosityModel_CartTable_Params::checkInputs(d);

  // PyDict constructor
  PorosityModel_CartTable phiModel(d, pref);

  // copy constructor
  PorosityModel_CartTable phiModel2(phiModel);

  // delete the written files
  std::remove(filename.c_str());
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Evaluations )
{
  const Real tol = 1.e-13;
  std::string filename = "CartTable.txt";
  std::ofstream file(filename);
  genCartTable( file );

  Real Cr = 2e-5;
  Real pref = 14.7;

  PyDict d;
  d[PorosityModel_CartTable_Params::params.PorosityFile] = filename;
  d[PorosityModel_CartTable_Params::params.Cr] = Cr;

  // PyDict constructor
  PorosityModel_CartTable phiModel(d, pref);

  Real x, y, time;
  Real pn, pnx, pny;
  Real phi, phix, phiy, phi_pn;

  // 1D  -- not implemented
  x = 0; time = 0;
  pn = 15.0; pnx = 0.3;
  BOOST_CHECK_THROW(phi = phiModel.porosity(x, time, pn), DeveloperException);
  BOOST_CHECK_THROW(phiModel.porosityGradient(x, time, pn, pnx, phix), DeveloperException);
  BOOST_CHECK_THROW(phiModel.porosityJacobian(x, time, pn, phi_pn), DeveloperException);

  // 2D
  int imax = 5;
  int jmax = 7;
  Real dx = 20.;
  Real dy = 30.;
  pn = 15.0; pnx = 0.3; pny = -0.7;
  for (int i = 0; i < imax-1; i++)
    for (int j = 0; j < jmax-1; j++)
    {
      x = i*dx + dx/2;
      y = j*dy + dy/2;
      double phi_true = (i*jmax + j)*exp(Cr*(pn - pref));
      double phi_pn_true = Cr*phi_true;

      phi = phiModel.porosity(x, y, time, pn);
      BOOST_CHECK_CLOSE(phi, phi_true, tol);

      phiModel.porosityJacobian(x, y, time, pn, phi_pn);
      BOOST_CHECK_CLOSE(phi_pn, phi_pn_true, tol);

      phiModel.porosityGradient(x, y, time, pn, pnx, pny, phix, phiy);
      BOOST_CHECK_CLOSE(phix, phi_pn_true*pnx, tol);
      BOOST_CHECK_CLOSE(phiy, phi_pn_true*pny, tol);
    }

  BOOST_CHECK_THROW(phi = phiModel.porosity(-1, dy, time, pn), AssertionException);
  BOOST_CHECK_THROW(phi = phiModel.porosity(dx, -1, time, pn), AssertionException);
  BOOST_CHECK_THROW(phi = phiModel.porosity(imax*dx+dx, dy, time, pn), AssertionException);
  BOOST_CHECK_THROW(phi = phiModel.porosity(dx, jmax*dy+dy, time, pn), AssertionException);

  // delete the written file
  std::remove(filename.c_str());
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/PorosityModel_CartTable_pattern.txt", true );

  std::string filename = "CartTable.txt";
  std::ofstream file(filename);
  genCartTable( file );

  Real Cr = 2e-5;
  Real pref = 14.7;

  PyDict d;
  d[PorosityModel_CartTable_Params::params.PorosityFile] = filename;
  d[PorosityModel_CartTable_Params::params.Cr] = Cr;

  // PyDict constructor
  PorosityModel_CartTable phiModel(d, pref);

  phiModel.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  // delete the written file
  std::remove(filename.c_str());
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
