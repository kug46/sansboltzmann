// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCTwoPhase1D_btest
//
// test of TwoPhase BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/PorousMedia/Q1DPrimitive_pnSw.h"
#include "pde/PorousMedia/BCTwoPhase1D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/CapillaryModel.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_Comp,
                            RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                            Real, CapillaryModel_Linear> TraitsModel_RelPower_ViscConst_CapLinear;

typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModel_RelPower_ViscConst_CapLinear> PDE_Comp_RelPower_ViscConst_CapLinear;

template class BCTwoPhase1D< BCTypeTimeIC, PDE_Comp_RelPower_ViscConst_CapLinear >;
template class BCTwoPhase1D< BCTypeTimeIC_Function, PDE_Comp_RelPower_ViscConst_CapLinear >;
template class BCTwoPhase1D< BCTypeFullState, PDE_Comp_RelPower_ViscConst_CapLinear >;
template class BCTwoPhase1D< BCTypeFullStateSpaceTime, PDE_Comp_RelPower_ViscConst_CapLinear >;
template class BCTwoPhase1D< BCTypeFunction_mitState, PDE_Comp_RelPower_ViscConst_CapLinear >;

// Instantiate BCParameters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters< BCTwoPhase1DVector<PDE_Comp_RelPower_ViscConst_CapLinear> >;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BCTwoPhase1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_Comp,
                              RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                              Real, CapillaryModel_Linear> TraitsModelClass;

  typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  {
  typedef BCTwoPhase1D< BCTypeTimeIC, PDEClass> BCClass;
  BOOST_CHECK( BCClass::D == 1 );
  BOOST_CHECK( BCClass::N == 2 );
  BOOST_CHECK( BCClass::NBC == 2 );
  }

  {
  typedef BCTwoPhase1D< BCTypeTimeIC_Function, PDEClass> BCClass;
  BOOST_CHECK( BCClass::D == 1 );
  BOOST_CHECK( BCClass::N == 2 );
  BOOST_CHECK( BCClass::NBC == 2 );
  }

  {
  typedef BCTwoPhase1D< BCTypeFullState, PDEClass> BCClass;
  BOOST_CHECK( BCClass::D == 1 );
  BOOST_CHECK( BCClass::N == 2 );
  BOOST_CHECK( BCClass::NBC == 2 );
  }

  {
  typedef BCTwoPhase1D< BCTypeFullStateSpaceTime, PDEClass> BCClass;
  BOOST_CHECK( BCClass::D == 1 );
  BOOST_CHECK( BCClass::N == 2 );
  BOOST_CHECK( BCClass::NBC == 2 );
  }

  {
  typedef BCTwoPhase1D< BCTypeFunction_mitState, PDEClass> BCClass;
  BOOST_CHECK( BCClass::D == 1 );
  BOOST_CHECK( BCClass::N == 2 );
  BOOST_CHECK( BCClass::NBC == 2 );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef Real RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  CapillaryModel pc(5);
  const Real K = 0.25;

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCParameters< BCTwoPhase1DVector<PDEClass> > BCParams;

  PyDict BCList;

  {
    typedef BCNone<PhysD1,TraitsSizeTwoPhase<PhysD1>::N> BCClass;

    //Pydict constructor
    PyDict BCNone;
    BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;
    BCClass bc0(pde, BCNone);

    BCList["BC0"] = BCNone;
  }

  {
    typedef BCTwoPhase1D< BCTypeTimeIC, PDEClass> BCClass;

    PressureNonWet_SaturationWet<Real> qdata(2534.5, 0.738);
    BCClass bc0(pde, qdata);

    PyDict d;
    d[TwoPhaseVariableTypeParams::params.StateVector.Variables] = TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
    d[PressureNonWet_SaturationWet_Params::params.pn] = 2534.5;
    d[PressureNonWet_SaturationWet_Params::params.Sw] = 0.738;

    PyDict BCInit;
    BCInit[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC;
    BCInit[BCClass::ParamsType::params.StateVector] = d;
    BCClass bc1(pde, BCInit);

    BCList["BC1"] = BCInit;
  }

  {
    typedef BCTwoPhase1D< BCTypeTimeIC_Function, PDEClass> BCClass;
    typedef Q1D<QTypePrimitive_pnSw, CapillaryModel, TraitsSizeTwoPhase> QInterpreter;
    typedef SolutionFunction_TwoPhase1D_3ConstSaturation<QInterpreter, TraitsSizeTwoPhase> SolutionType;

    PyDict initSoln;
    initSoln[BCClass::ParamsType::params.Function.Name] = BCClass::ParamsType::params.Function.ThreeConstSaturation;
    initSoln[SolutionType::ParamsType::params.pn] = 2500.0;
    initSoln[SolutionType::ParamsType::params.Sw_left] = 1.0;
    initSoln[SolutionType::ParamsType::params.Sw_mid] = 0.1;
    initSoln[SolutionType::ParamsType::params.Sw_right] = 1.0;
    initSoln[SolutionType::ParamsType::params.xLeftMid] = 100.0;
    initSoln[SolutionType::ParamsType::params.xMidRight] = 200.0;

    PyDict BCSolution;
    BCSolution[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC_Function;
    BCSolution[BCClass::ParamsType::params.Function] = initSoln;
    BCClass bc1(pde, BCSolution);

    BCList["BC2"] = BCSolution;
  }

  {
    typedef BCTwoPhase1D< BCTypeFullState, PDEClass> BCClass;

    PressureNonWet_SaturationWet<Real> qdata(2534.5, 0.738);
    BCClass bc0(pde, qdata);

    PyDict d;
    d[TwoPhaseVariableTypeParams::params.StateVector.Variables] = TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
    d[PressureNonWet_SaturationWet_Params::params.pn] = 2534.5;
    d[PressureNonWet_SaturationWet_Params::params.Sw] = 0.738;

    PyDict BCFullState;
    BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
    BCFullState[BCClass::ParamsType::params.StateVector] = d;
    BCClass bc1(pde, BCFullState);

    BCList["BC3"] = BCFullState;
  }

  {
    typedef BCTwoPhase1D< BCTypeFullStateSpaceTime, PDEClass> BCClass;

    PressureNonWet_SaturationWet<Real> qdata(2534.5, 0.738);
    BCClass bc0(pde, qdata);

    PyDict d;
    d[TwoPhaseVariableTypeParams::params.StateVector.Variables] = TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
    d[PressureNonWet_SaturationWet_Params::params.pn] = 2534.5;
    d[PressureNonWet_SaturationWet_Params::params.Sw] = 0.738;

    PyDict BCFullStateSpaceTime;
    BCFullStateSpaceTime[BCParams::params.BC.BCType] = BCParams::params.BC.FullStateSpaceTime;
    BCFullStateSpaceTime[BCClass::ParamsType::params.StateVector] = d;
    BCClass bc1(pde, BCFullStateSpaceTime);

    BCList["BC4"] = BCFullStateSpaceTime;
  }

  {
    typedef BCTwoPhase1D< BCTypeFunction_mitState, PDEClass> BCClass;
    typedef Q1D<QTypePrimitive_pnSw, CapillaryModel, TraitsSizeTwoPhase> QInterpreter;
    typedef SolutionFunction_TwoPhase1D_3ConstSaturation<QInterpreter, TraitsSizeTwoPhase> SolutionType;

    PyDict initSoln;
    initSoln[BCClass::ParamsType::params.Function.Name] = BCClass::ParamsType::params.Function.ThreeConstSaturation;
    initSoln[SolutionType::ParamsType::params.pn] = 2500.0;
    initSoln[SolutionType::ParamsType::params.Sw_left] = 1.0;
    initSoln[SolutionType::ParamsType::params.Sw_mid] = 0.1;
    initSoln[SolutionType::ParamsType::params.Sw_right] = 1.0;
    initSoln[SolutionType::ParamsType::params.xLeftMid] = 100.0;
    initSoln[SolutionType::ParamsType::params.xMidRight] = 200.0;

    PyDict BCSolution;
    BCSolution[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
    BCSolution[BCClass::ParamsType::params.Function] = initSoln;
    BCClass bc1(pde, BCSolution);

    BCList["BC5"] = BCSolution;
  }

  //No exceptions should be thrown
  BCParams::checkInputs(BCList);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCNone_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef Real RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  CapillaryModel pc(5);
  const Real K = 0.25;

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCNone<PhysD1,TraitsSizeTwoPhase<PhysD1>::N> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  BCClass bc;
  ArrayQ qI = {2500,0.8};
  Real nx = 2;
  BOOST_CHECK(bc.isValidState(nx, qI) == true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeTimeIC_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef Real RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  const Real tol = 1.0e-12;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 1e-5, pref);
  DensityModel rhon(52.1, 1e-5, pref);

  PorosityModel phi(0.3, 1e-5, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  CapillaryModel pc(2);
  const Real K = 1.24;

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCTwoPhase1D< BCTypeTimeIC, PDEClass> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  Real pnB = 2534.5;
  Real SwB = 0.738;
  PressureNonWet_SaturationWet<Real> qdata(pnB, SwB);
  BCClass bc(pde, qdata);

  Real x = 0;
  Real time = 0;
  Real nx = 0.8;
  Real nt = 0.25;

  ArrayQ qI = {2500,0.8};
  ArrayQ qIx = {1.35, -0.05};
  ArrayQ qIt = qIx;
  ArrayQ qB = 0;
  bc.state(x, time, nx, nt, qI, qB);

  BOOST_CHECK_CLOSE( pnB, qB[0], tol );
  BOOST_CHECK_CLOSE( SwB, qB[1], tol );

  ArrayQ uB = 0;
  pde.masterState(x, time, qB, uB);
  ArrayQ Fn_true = uB*nt;

  ArrayQ fvx = 0, fvt = 0;
  pde.fluxViscousSpaceTime(x, time, qB, qIx, qIt, fvx, fvt);
  Fn_true += fvx*nx + fvt*nt;

  ArrayQ Fn = 0;
  bc.fluxNormalSpaceTime(x, time, nx, nt, qI, qIx, qIt, qB, Fn);
  BOOST_CHECK_CLOSE( Fn_true[0], Fn[0], tol );
  BOOST_CHECK_CLOSE( Fn_true[1], Fn[1], tol );

  BOOST_CHECK(bc.isValidState(nx, nt, qI) == true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFullState_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef Real RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  const Real tol = 1.0e-12;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 1e-5, pref);
  DensityModel rhon(52.1, 1e-5, pref);

  PorosityModel phi(0.3, 1e-5, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  CapillaryModel pc(2);
  const Real K = 1.24;

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCTwoPhase1D< BCTypeFullState, PDEClass> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  Real pnB = 2534.5;
  Real SwB = 0.738;
  PressureNonWet_SaturationWet<Real> qdata(pnB, SwB);
  BCClass bc(pde, qdata);

  Real x = 0;
  Real time = 0;
  Real nx = 0.8;

  ArrayQ qI = {2500,0.8};
  ArrayQ qIx = {1.35, -0.05};
  ArrayQ qB = 0;
  bc.state(x, time, nx, qI, qB);

  BOOST_CHECK_CLOSE( pnB, qB[0], tol );
  BOOST_CHECK_CLOSE( SwB, qB[1], tol );

  ArrayQ Fn_true = 0;
  pde.fluxAdvectiveUpwind(x, time, qI, qB, nx, Fn_true);

  ArrayQ fx_true = 0;
  pde.fluxViscous(x, time, qB, qIx, fx_true);
  Fn_true += fx_true*nx;

  ArrayQ Fn = 0;
  bc.fluxNormal(x, time, nx, qI, qIx, qB, Fn);
  BOOST_CHECK_CLOSE( Fn_true[0], Fn[0], tol );
  BOOST_CHECK_CLOSE( Fn_true[1], Fn[1], tol );

  BOOST_CHECK(bc.isValidState(nx, qI) == true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFullStateSpaceTime_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef Real RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  const Real tol = 1.0e-12;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 1e-5, pref);
  DensityModel rhon(52.1, 1e-5, pref);

  PorosityModel phi(0.3, 1e-5, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  CapillaryModel pc(2);
  const Real K = 1.24;

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCTwoPhase1D< BCTypeFullStateSpaceTime, PDEClass> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  Real pnB = 2534.5;
  Real SwB = 0.738;
  PressureNonWet_SaturationWet<Real> qdata(pnB, SwB);
  BCClass bc(pde, qdata);

  Real x = 0;
  Real time = 0;
  Real nx = 0.8, nt = 0.25;

  ArrayQ qI = {2500,0.8};
  ArrayQ qIx = {1.35, -0.05};
  ArrayQ qIt = {2.08, 1.03};
  ArrayQ qB = 0;
  bc.state(x, time, nx, nt, qI, qB);

  BOOST_CHECK_CLOSE( pnB, qB[0], tol );
  BOOST_CHECK_CLOSE( SwB, qB[1], tol );

  ArrayQ Fn_true = 0;
  pde.fluxAdvectiveUpwindSpaceTime(x, time, qI, qB, nx, nt, Fn_true);

  ArrayQ fx_true = 0, ft_true = 0;
  pde.fluxViscousSpaceTime(x, time, qB, qIx, qIt, fx_true, ft_true);
  Fn_true += fx_true*nx + ft_true*nt;

  ArrayQ Fn = 0;
  bc.fluxNormalSpaceTime(x, time, nx, nt, qI, qIx, qIt, qB, Fn);
  BOOST_CHECK_CLOSE( Fn_true[0], Fn[0], tol );
  BOOST_CHECK_CLOSE( Fn_true[1], Fn[1], tol );

  BOOST_CHECK(bc.isValidState(nx, nt, qI) == true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFunction_mitState_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef Real RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef Q1D<QTypePrimitive_pnSw, CapillaryModel, TraitsSizeTwoPhase> QInterpreter;
  typedef SolutionFunction_TwoPhase1D_3ConstSaturation<QInterpreter, TraitsSizeTwoPhase> SolutionType;

  const Real tol = 1.0e-12;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 1e-4, pref);
  DensityModel rhon(52.1, 1e-4, pref);

  PorosityModel phi(0.3, 1e-4, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  CapillaryModel pc(5);
  const Real K = 0.25;

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCTwoPhase1D< BCTypeFunction_mitState, PDEClass> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  const Real xLeftMid = 1.0;
  const Real xMidRight = 2.0;
  const Real pnInit = 2500.0;
  const Real SwLeft = 0.1, SwMid = 0.6, SwRight = 1.0;

  std::shared_ptr<Function1DBase<ArrayQ>> functionPtr(new SolutionType(pnInit, SwLeft, SwMid, SwRight,
                                                                       xLeftMid, xMidRight, pde.variableInterpreter()));

  BCClass bc(pde, functionPtr);

  Real x = 0;
  Real time = 0;
  Real nx = 0.8;

  ArrayQ qI = {2500, 0.8};
  ArrayQ qIx = {2.674, 1.362};
  ArrayQ qB = 0;
  bc.state(x, time, nx, qI, qB);
  BOOST_CHECK_CLOSE( pnInit, qB[0], tol );
  BOOST_CHECK_CLOSE( SwLeft, qB[1], tol );

  x = 1.5;
  bc.state(x, time, nx, qI, qB);
  BOOST_CHECK_CLOSE( pnInit, qB[0], tol );
  BOOST_CHECK_CLOSE( SwMid, qB[1], tol );

  x = 3.5;
  bc.state(x, time, nx, qI, qB);
  BOOST_CHECK_CLOSE( pnInit, qB[0], tol );
  BOOST_CHECK_CLOSE( SwRight, qB[1], tol );


  ArrayQ Fn_true = 0;
  pde.fluxAdvectiveUpwind(x, time, qI, qB, nx, Fn_true);

  ArrayQ fx = 0;
  pde.fluxViscous(x, time, qB, qIx, fx);
  Fn_true += fx*nx;

  ArrayQ Fn = 0;
  bc.fluxNormal(x, time, nx, qI, qIx, qB, Fn);
  BOOST_CHECK_CLOSE( Fn_true[0], Fn[0], tol );
  BOOST_CHECK_CLOSE( Fn_true[1], Fn[1], tol );

  BOOST_CHECK(bc.isValidState(nx, qI) == true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeTimeIC_Function_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef Real RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef Q1D<QTypePrimitive_pnSw, CapillaryModel, TraitsSizeTwoPhase> QInterpreter;
  typedef SolutionFunction_TwoPhase1D_3ConstSaturation<QInterpreter, TraitsSizeTwoPhase> SolutionType;

  const Real tol = 1.0e-12;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 1e-4, pref);
  DensityModel rhon(52.1, 1e-4, pref);

  PorosityModel phi(0.3, 1e-4, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  CapillaryModel pc(5);
  const Real K = 0.25;

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCTwoPhase1D< BCTypeTimeIC_Function, PDEClass> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  const Real xLeftMid = 1.0;
  const Real xMidRight = 2.0;
  const Real pnInit = 2500.0;
  const Real SwLeft = 0.1, SwMid = 0.6, SwRight = 1.0;

  std::shared_ptr<Function1DBase<ArrayQ>> functionPtr(new SolutionType(pnInit, SwLeft, SwMid, SwRight,
                                                                       xLeftMid, xMidRight, pde.variableInterpreter()));

  BCClass bc(pde, functionPtr);

  Real x = 0;
  Real time = 0;
  Real nx = 0.8, nt = 0.25;

  ArrayQ qI = {2500, 0.8};
  ArrayQ qIx = {2.674, 1.362};
  ArrayQ qIt = {0.345, -0.937};
  ArrayQ qB = 0;
  bc.state(x, time, nx, nt, qI, qB);
  BOOST_CHECK_CLOSE( pnInit, qB[0], tol );
  BOOST_CHECK_CLOSE( SwLeft, qB[1], tol );

  x = 1.5;
  bc.state(x, time, nx, nt, qI, qB);
  BOOST_CHECK_CLOSE( pnInit, qB[0], tol );
  BOOST_CHECK_CLOSE( SwMid, qB[1], tol );

  x = 3.5;
  bc.state(x, time, nx, nt, qI, qB);
  BOOST_CHECK_CLOSE( pnInit, qB[0], tol );
  BOOST_CHECK_CLOSE( SwRight, qB[1], tol );


  ArrayQ Fn_true = 0;
  ArrayQ uB = 0;
  pde.masterState(x, time, qB, uB);
  Fn_true += nt*uB;

  ArrayQ fx = 0, ft = 0;
  pde.fluxViscousSpaceTime(x, time, qB, qIx, qIt, fx, ft);
  Fn_true += fx*nx + ft*nt;

  ArrayQ Fn = 0;
  bc.fluxNormalSpaceTime(x, time, nx, nt, qI, qIx, qIt, qB, Fn);
  BOOST_CHECK_CLOSE( Fn_true[0], Fn[0], tol );
  BOOST_CHECK_CLOSE( Fn_true[1], Fn[1], tol );

  BOOST_CHECK(bc.isValidState(nx, nt, qI) == true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/BCTwoPhase1D_pattern.txt", true );

  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef Real RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  CapillaryModel pc(5);
  const Real K = 0.25;

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  {
    typedef BCNone<PhysD1,TraitsSizeTwoPhase<PhysD1>::N> BCClass;
    BCClass bc;
    bc.dump( 2, output );
  }

  {
    typedef BCTwoPhase1D< BCTypeFullState, PDEClass> BCClass;

    Real pnB = 2534.5;
    Real SwB = 0.738;
    PressureNonWet_SaturationWet<Real> qdata(pnB, SwB);
    BCClass bc(pde, qdata);

    bc.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
