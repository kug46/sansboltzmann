// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Q1DPrimitiveSaturationWet_btest
// testing of Q1D<T,QTypePrimitiveSaturationWet> class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <string>
#include <iostream>

#include "pde/PorousMedia/TraitsBuckleyLeverett.h"
#include "pde/PorousMedia/Q1DPrimitive_Sw.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( QPrimitive_Sw_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test )
{
  const Real tol = 1.e-13;

  typedef Q1D<QTypePrimitive_Sw, TraitsSizeBuckleyLeverett> QInterpret;
  BOOST_CHECK( QInterpret::N == 1 );

  typedef QInterpret::ArrayQ<Real> ArrayQ;

  // constructor
  QInterpret qInterpret;

  // set/eval
  ArrayQ q;
  Real Sw, Sn;
  Real Sw1, Sw2;
  Real Sn1, Sn2;

  Sw = 0.375;
  Sn = 1-Sw;

  Real qDataPrim[1] = {Sw};
  string qNamePrim[1] = {"Sw"};
  qInterpret.setFromPrimitive( q, qDataPrim, qNamePrim, 1 );
  qInterpret.eval_Sw( q, Sw1 );
  qInterpret.eval_Sn( q, Sn1 );
  BOOST_CHECK_CLOSE( Sw1, Sw, tol );
  BOOST_CHECK_CLOSE( Sn1, Sn, tol );

  // copy constructor
  QInterpret qInterpret2(qInterpret);

  qInterpret2.eval_Sw( q, Sw2 );
  qInterpret2.eval_Sn( q, Sn2 );
  BOOST_CHECK_CLOSE( Sw2, Sw, tol );
  BOOST_CHECK_CLOSE( Sn2, Sn, tol );

  // gradient
  Real Swx, Snx;
  Real Swx1, Swx2;
  Real Snx1, Snx2;
  Swx = -0.673;
  Snx = -Swx;

//  Real qxData[1] = {Swx};
  ArrayQ qx = Swx;

  qInterpret.eval_SwGradient( q, qx, Swx1);
  qInterpret.eval_SnGradient( q, qx, Snx1);
  BOOST_CHECK_CLOSE( Swx1, Swx, tol );
  BOOST_CHECK_CLOSE( Snx1, Snx, tol );

  qInterpret2.eval_SwGradient( q, qx, Swx2);
  qInterpret2.eval_SnGradient( q, qx, Snx2);
  BOOST_CHECK_CLOSE( Swx2, Swx, tol );
  BOOST_CHECK_CLOSE( Snx2, Snx, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef Q1D<QTypePrimitive_Sw, TraitsSizeBuckleyLeverett> QInterpret;
  typedef QInterpret::ArrayQ<Real> ArrayQ;

  QInterpret qInterpret;
  ArrayQ q;

  q =  0.375;
  BOOST_CHECK( qInterpret.isValidState(q) == true );

  q =  0.739;
  BOOST_CHECK( qInterpret.isValidState(q) == true );

  q = -0.038;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  q =  1.01;
  BOOST_CHECK( qInterpret.isValidState(q) == false );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
