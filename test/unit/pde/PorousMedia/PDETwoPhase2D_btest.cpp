// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDETwoPhase2D_btest
//
// testing of 2D two phase PDE class

#include <fstream>
#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/PorousMedia/TraitsTwoPhase.h"
#include "pde/PorousMedia/Q2DPrimitive_pnSw.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/PermeabilityModel2D.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/PDETwoPhase2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"

//Explicitly instantiate the class so coverage information is correct
namespace SANS
{

template class TraitsSizeTwoPhase<PhysD2>;
template class TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_Comp,
                                   RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                                   PermeabilityModel2D_Constant, CapillaryModel_Linear>;

typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_Comp,
                            RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                            PermeabilityModel2D_Constant, CapillaryModel_Linear> TraitsModelClass;

template class PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass>;

template class TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_CartTable,
                                   RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                                   PermeabilityModel2D_CartTable, CapillaryModel_Linear>;

typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_CartTable,
                            RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                            PermeabilityModel2D_CartTable, CapillaryModel_Linear> TraitsModelClass_CartTable;

template class PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass_CartTable>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDETwoPhase2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test_pnSw )
{
  {
    typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

    BOOST_CHECK( PDEClass::D == 2 );
    BOOST_CHECK( PDEClass::N == 2 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_pnSw )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  RockPermModel K(1.1, 0.6, 0.2, 1.5);
  CapillaryModel pc(5);

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == false );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasSourceTrace() == false );
  BOOST_CHECK( pde.hasSourceLiftedQuantity() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  //Copy constructor
  PDEClass pde2(pde);
  BOOST_CHECK( pde2.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde2.hasFluxAdvective() == false );
  BOOST_CHECK( pde2.hasFluxViscous() == true );
  BOOST_CHECK( pde2.hasSource() == false );
  BOOST_CHECK( pde2.hasSourceTrace() == false );
  BOOST_CHECK( pde2.hasSourceLiftedQuantity() == false );
  BOOST_CHECK( pde2.hasForcingFunction() == false );
  BOOST_CHECK( pde2.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde2.needsSolutionGradientforSource() == false );

  // Set up PyDicts
  PyDict fixedwellpressure;
  fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.pB] = 2350.0;
  fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.Rwellbore] = 0.5;
  fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.nParam] = 1;
  fixedwellpressure[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureOutflow;

  PyDict well;
  well[SourceTwoPhase2DParam::params.Source] = fixedwellpressure;
  well[SourceTwoPhase2DParam::params.xcentroid] = 10.0;
  well[SourceTwoPhase2DParam::params.ycentroid] = 15.0;
  well[SourceTwoPhase2DParam::params.R] = 5.0;
  well[SourceTwoPhase2DParam::params.Tmin] = 1.0;
  well[SourceTwoPhase2DParam::params.Tmax] = 3.0;
  well[SourceTwoPhase2DParam::params.smoothLr] = 1.0;
  well[SourceTwoPhase2DParam::params.smoothT] = 0.0;
  well[SourceTwoPhase2DParam::params.visibleAngle] = 2*PI;

  PyDict sourceList;
  sourceList["well"] = well;
  SourceTwoPhase2DListParam::checkInputs(sourceList);

  //Constructor with source terms
  PDEClass pde3(rhow, rhon, phi, krw, krn, muw, mun, K, pc, sourceList);

  BOOST_CHECK( pde3.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde3.hasFluxAdvective() == false );
  BOOST_CHECK( pde3.hasFluxViscous() == true );
  BOOST_CHECK( pde3.hasSource() == true );
  BOOST_CHECK( pde3.hasSourceTrace() == false );
  BOOST_CHECK( pde3.hasForcingFunction() == false );
  BOOST_CHECK( pde3.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde3.needsSolutionGradientforSource() == false );

  //Copy constructor
  PDEClass pde4(pde3);
  BOOST_CHECK( pde4.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde4.hasFluxAdvective() == false );
  BOOST_CHECK( pde4.hasFluxViscous() == true );
  BOOST_CHECK( pde4.hasSource() == true );
  BOOST_CHECK( pde4.hasSourceTrace() == false );
  BOOST_CHECK( pde4.hasForcingFunction() == false );
  BOOST_CHECK( pde4.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde4.needsSolutionGradientforSource() == false );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setDOFFrom )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;

  Real tol = 1e-13;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  RockPermModel K(1.1, 0.6, 0.2, 1.5);
  CapillaryModel pc(5);

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  Real pn = 2534.8;
  Real Sw = 0.784;

  // set
  Real qDataPrim[2] = {pn,Sw};
  string qNamePrim[2] = {"pn","Sw"};
  ArrayQ qTrue = {pn,Sw};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 2 );
  BOOST_CHECK_CLOSE( qTrue[0], q[0], tol );
  BOOST_CHECK_CLOSE( qTrue[1], q[1], tol );

  PressureNonWet_SaturationWet<Real> qdata1;
  qdata1.pn = pn; qdata1.Sw = Sw;
  q = 0;
  pde.setDOFFrom( q, qdata1 );
  BOOST_CHECK_CLOSE( qTrue[0], q[0], tol );
  BOOST_CHECK_CLOSE( qTrue[1], q[1], tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux_pnSw )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  const Real pref = 1.47;

  const Real rhow_ref = 62.4;
  const Real rhon_ref = 52.1;
  const Real Cw = 0.03;
  const Real Cn = 0.05;

  const Real phi_ref = 0.3;
  const Real Cr = 0.06;

  const Real muw = 1.0, mun = 2.0;
  const Real pcmax = 5.0;

  DensityModel rhow_model(rhow_ref, Cw, pref);
  DensityModel rhon_model(rhon_ref, Cn, pref);

  PorosityModel phi_model(phi_ref, Cr, pref);

  RelPermModel krw_model(2);
  RelPermModel krn_model(2);

  ViscModel muw_model(muw);
  ViscModel mun_model(mun);

  Real Kxx = 1.1, Kxy = 0.6, Kyx = 0.2, Kyy = 1.5;
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc_model(pcmax);

  Real scalar_visc = 0.0;
  bool gravity_on = true;

  PDEClass pde(rhow_model, rhon_model, phi_model, krw_model, krn_model, muw_model, mun_model, K, pc_model,
               {}, scalar_visc, gravity_on);

  // Static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 2 );

  // Flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == gravity_on );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasSourceTrace() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  BOOST_CHECK_EQUAL( pde.nMonitor(), 2 );

  // Function tests
  Real x = 0, y = 0, time = 0;   // not actually used in functions
  Real nx = 0.35, ny = -0.26;

  Real SwL = 0.375, SwR = 0.418;
  Real SwxL = -0.63, SwxR = -0.37;
  Real SwyL = 0.27, SwyR = 0.46;
  Real pnL = 25.34, pnR = 31.86;
  Real pnxL = -0.52, pnxR = -0.29;
  Real pnyL = 0.96, pnyR = 0.81;

  Real SnL = 1.0 - SwL;
  Real SnR = 1.0 - SwR;
  Real pcL = pcmax*SnL;
  Real pcR = pcmax*SnR;
  Real pwL = pnL - pcL;
  Real pwR = pnR - pcR;
  Real pwxL = pnxL - pcmax*(-SwxL);
  Real pwxR = pnxR - pcmax*(-SwxR);
  Real pwyL = pnyL - pcmax*(-SwyL);
  Real pwyR = pnyR - pcmax*(-SwyR);

  Real krwL = pow(SwL,2);
  Real krwR = pow(SwR,2);

  Real krnL = pow(SnL,2);
  Real krnR = pow(SnR,2);

  Real krw_SwL = 2*SwL;
  Real krw_SwR = 2*SwR;

  Real krn_SwL = -2*SnL;
  Real krn_SwR = -2*SnR;

  Real rhowL = rhow_ref*exp(Cw*(pwL - pref));
  Real rhowR = rhow_ref*exp(Cw*(pwR - pref));

  Real rhonL = rhon_ref*exp(Cn*(pnL - pref));
  Real rhonR = rhon_ref*exp(Cn*(pnR - pref));

  Real phiL  = phi_ref *exp(Cr*(pnL - pref));

  Real rhow_pnL = Cw*rhowL;
  Real rhow_SwL = Cw*rhowL*pcmax;
  Real rhon_pnL = Cn*rhonL;
  Real phi_pnL = Cr*phiL;

  Real LwL = krwL/muw;
  Real LwR = krwR/muw;

  Real LnL = krnL/mun;
  Real LnR = krnR/mun;

  Real Lw_SwL = krw_SwL / muw;
  Real Lw_SwR = krw_SwR / muw;

  Real Ln_SwL = krn_SwL / mun;
  Real Ln_SwR = krn_SwR / mun;

  Real grav_mask = 6.9468E-3 * gravity_on; //Units: [psi ft^2 / lb]

  // Set
  Real qLDataPrim[2] = {pnL,SwL};
  Real qRDataPrim[2] = {pnR,SwR};
  string qNamePrim[2] = {"pn","Sw"};
  ArrayQ qLTrue = {pnL,SwL};
  ArrayQ qRTrue = {pnR,SwR};
  ArrayQ qL, qR;
  pde.setDOFFrom( qL, qLDataPrim, qNamePrim, 2 );
  pde.setDOFFrom( qR, qRDataPrim, qNamePrim, 2 );
  SANS_CHECK_CLOSE( qLTrue[0], qL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( qLTrue[1], qL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( qRTrue[0], qR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( qRTrue[1], qR[1], small_tol, close_tol );

  Real Sw_test, Sn_test, pw_test, pn_test;
  pde.variableInterpreter().eval(qL, pw_test, pn_test, Sw_test, Sn_test);
  SANS_CHECK_CLOSE( pwL, pw_test, small_tol, close_tol );
  SANS_CHECK_CLOSE( pnL, pn_test, small_tol, close_tol );
  SANS_CHECK_CLOSE( SwL, Sw_test, small_tol, close_tol );
  SANS_CHECK_CLOSE( SnL, Sn_test, small_tol, close_tol );

  // Conservative flux
  ArrayQ ucons_true, ucons;
  ucons_true[0] = rhowL*phiL*SwL;
  ucons_true[1] = rhonL*phiL*SnL;
  pde.masterState( x, y, time, qL, ucons );
  SANS_CHECK_CLOSE( ucons_true[0], ucons[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( ucons_true[1], ucons[1], small_tol, close_tol );

  // Conservative flux jacobian
  MatrixQ fcons_jac_true, fcons_jac;
  fcons_jac_true(0,0) = (phi_pnL*rhowL + phiL*rhow_pnL)*SwL;
  fcons_jac_true(0,1) = (                phiL*rhow_SwL)*SwL + phiL*rhowL;
  fcons_jac_true(1,0) = (phi_pnL*rhonL + phiL*rhon_pnL)*SnL;
  fcons_jac_true(1,1) = phiL*rhonL*(-1);
  pde.jacobianMasterState( x, y, time, qL, fcons_jac );
  SANS_CHECK_CLOSE( fcons_jac_true(0,0), fcons_jac(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_jac_true(0,1), fcons_jac(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_jac_true(1,0), fcons_jac(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_jac_true(1,1), fcons_jac(1,1), small_tol, close_tol );

  // Conservative flux
  ArrayQ fcons_true, fcons = 0;
  fcons_true[0] = rhowL*phiL*SwL;
  fcons_true[1] = rhonL*phiL*SnL;
  pde.fluxAdvectiveTime( x, y, time, qL, fcons );
  SANS_CHECK_CLOSE( fcons_true[0], fcons[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_true[1], fcons[1], small_tol, close_tol );

  // Advective flux
  ArrayQ fxadv_true, fyadv_true;
  fxadv_true[0] = -rhowL*LwL*(Kxy*rhowL*grav_mask);
  fxadv_true[1] = -rhonL*LnL*(Kxy*rhonL*grav_mask);
  fyadv_true[0] = -rhowL*LwL*(Kyy*rhowL*grav_mask);
  fyadv_true[1] = -rhonL*LnL*(Kyy*rhonL*grav_mask);
  ArrayQ fxadv = 0.0, fyadv = 0.0;
  pde.fluxAdvective( x, y, time, qL, fxadv, fyadv );
  SANS_CHECK_CLOSE( fxadv_true[0], fxadv[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fxadv_true[1], fxadv[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( fyadv_true[0], fyadv[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fyadv_true[1], fyadv[1], small_tol, close_tol );

  ArrayQ fn_adv = 0;
  pde.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn_adv );
  SANS_CHECK_CLOSE( 0.0, fn_adv[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, fn_adv[1], small_tol, close_tol );

  // Jacobian of advective flux
  MatrixQ fxadv_u = 0, fyadv_u = 0.0;
  BOOST_CHECK_THROW( pde.jacobianFluxAdvective( x, y, time, qL, fxadv_u, fyadv_u ), SANSException );

  // Viscous flux
  ArrayQ fxvisc_trueL, fyvisc_trueL;
  ArrayQ fxvisc = 0, fyvisc = 0;

  //Volume viscous flux does not contain the gravity term
  fxvisc_trueL[0] = -rhowL*LwL*(Kxx*pwxL + Kxy*pwyL);
  fxvisc_trueL[1] = -rhonL*LnL*(Kxx*pnxL + Kxy*pnyL);
  fyvisc_trueL[0] = -rhowL*LwL*(Kyx*pwxL + Kyy*pwyL);
  fyvisc_trueL[1] = -rhonL*LnL*(Kyx*pnxL + Kyy*pnyL);
  ArrayQ qxL = {pnxL, SwxL};
  ArrayQ qyL = {pnyL, SwyL};
  pde.fluxViscous( x, y, time, qL, qxL, qyL, fxvisc, fyvisc );
  SANS_CHECK_CLOSE( fxvisc_trueL[0], fxvisc[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fxvisc_trueL[1], fxvisc[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( fyvisc_trueL[0], fyvisc[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fyvisc_trueL[1], fyvisc[1], small_tol, close_tol );

  ArrayQ qxR = {pnxR, SwxR};
  ArrayQ qyR = {pnyR, SwyR};
  ArrayQ fxvisc_trueR, fyvisc_trueR;

  //Interface viscous fluxes include gravity contributions
  pwyL += rhowL*grav_mask;
  pwyR += rhowR*grav_mask;
  pnyL += rhonL*grav_mask;
  pnyR += rhonR*grav_mask;

  fxvisc_trueL[0] = -rhowL*LwL*(Kxx*pwxL + Kxy*pwyL);
  fxvisc_trueL[1] = -rhonL*LnL*(Kxx*pnxL + Kxy*pnyL);
  fyvisc_trueL[0] = -rhowL*LwL*(Kyx*pwxL + Kyy*pwyL);
  fyvisc_trueL[1] = -rhonL*LnL*(Kyx*pnxL + Kyy*pnyL);

  fxvisc_trueR[0] = -rhowR*LwR*(Kxx*pwxR + Kxy*pwyR);
  fxvisc_trueR[1] = -rhonR*LnR*(Kxx*pnxR + Kxy*pnyR);
  fyvisc_trueR[0] = -rhowR*LwR*(Kyx*pwxR + Kyy*pwyR);
  fyvisc_trueR[1] = -rhonR*LnR*(Kyx*pnxR + Kyy*pnyR);

  Real KgradpwL = (Kxx*pwxL + Kxy*pwyL)*nx + (Kyx*pwxL + Kyy*pwyL)*ny;
  Real KgradpwR = (Kxx*pwxR + Kxy*pwyR)*nx + (Kyx*pwxR + Kyy*pwyR)*ny;

  Real KgradpnL = (Kxx*pnxL + Kxy*pnyL)*nx + (Kyx*pnxL + Kyy*pnyL)*ny;
  Real KgradpnR = (Kxx*pnxR + Kxy*pnyR)*nx + (Kyx*pnxR + Kyy*pnyR)*ny;

  Real cL = (Lw_SwL*LnL*KgradpwL - LwL*Ln_SwL*KgradpnL) / (LwL + LnL);
  Real cR = (Lw_SwR*LnR*KgradpwR - LwR*Ln_SwR*KgradpnR) / (LwR + LnR);

  const Real eps = 1e-3;
  const Real alpha = 40;

  Real rw_cL = rhowL*cL;
  Real rw_cR = rhowR*cR;
  Real rn_cL = rhonL*cL;
  Real rn_cR = rhonR*cR;
  Real cw_max = smoothmax( smoothabs0(rw_cL, eps), smoothabs0(rw_cR, eps), alpha );
  Real cn_max = smoothmax( smoothabs0(rn_cL, eps), smoothabs0(rn_cR, eps), alpha );

  ArrayQ fn_true = 0.5*(fxvisc_trueL + fxvisc_trueR)*nx + 0.5*(fyvisc_trueL + fyvisc_trueR)*ny;
  fn_true(0) += 0.5*cw_max*(SwL - SwR);
  fn_true(1) -= 0.5*cn_max*(SwL - SwR);
  ArrayQ fn = 0.0;
  pde.fluxViscous( x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );
  SANS_CHECK_CLOSE( fn_true[0], fn[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fn_true[1], fn[1], small_tol, close_tol );

  //Finite volume viscous flux
  //Need to upwind mobilities to the left state for this particular problem
  fxvisc_trueL[0] = -rhowL*LwL*(Kxx*pwxL + Kxy*pwyL);
  fxvisc_trueL[1] = -rhonL*LnL*(Kxx*pnxL + Kxy*pnyL);
  fyvisc_trueL[0] = -rhowL*LwL*(Kyx*pwxL + Kyy*pwyL);
  fyvisc_trueL[1] = -rhonL*LnL*(Kyx*pnxL + Kyy*pnyL);

  fxvisc_trueR[0] = -rhowR*LwL*(Kxx*pwxR + Kxy*pwyR);
  fxvisc_trueR[1] = -rhonR*LnL*(Kxx*pnxR + Kxy*pnyR);
  fyvisc_trueR[0] = -rhowR*LwL*(Kyx*pwxR + Kyy*pwyR);
  fyvisc_trueR[1] = -rhonR*LnL*(Kyx*pnxR + Kyy*pnyR);

  ArrayQ fn_FV_true = 0.5*(fxvisc_trueL + fxvisc_trueR)*nx + 0.5*(fyvisc_trueL + fyvisc_trueR)*ny;
  ArrayQ fn_FV = 0.0;
  pde.fluxViscous_FV_TPFA( x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn_FV );
  SANS_CHECK_CLOSE( fn_FV_true[0], fn_FV[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fn_FV_true[1], fn_FV[1], small_tol, close_tol );

  //Revert gravity contribution
  pwyL -= rhowL*grav_mask;
//pwyR -= rhowR*grav_mask;
  pnyL -= rhonL*grav_mask;
//pnyR -= rhonR*grav_mask;

  // Viscous diffusion matrix
  Real pc_Sw = -pcmax;
  MatrixQ Fvx_qx_true, Fvx_qy_true, Fvy_qx_true, Fvy_qy_true;
  MatrixQ kxx = 0, kxy = 0, kyx = 0, kyy = 0;
  Fvx_qx_true(0,0) = rhowL*Kxx*LwL;
  Fvx_qx_true(0,1) = rhowL*Kxx*LwL*(-pc_Sw);
  Fvx_qx_true(1,0) = rhonL*Kxx*LnL;
  Fvx_qx_true(1,1) = 0.0;

  Fvx_qy_true(0,0) = rhowL*Kxy*LwL;
  Fvx_qy_true(0,1) = rhowL*Kxy*LwL*(-pc_Sw);
  Fvx_qy_true(1,0) = rhonL*Kxy*LnL;
  Fvx_qy_true(1,1) = 0.0;

  Fvy_qx_true(0,0) = rhowL*Kyx*LwL;
  Fvy_qx_true(0,1) = rhowL*Kyx*LwL*(-pc_Sw);
  Fvy_qx_true(1,0) = rhonL*Kyx*LnL;
  Fvy_qx_true(1,1) = 0.0;

  Fvy_qy_true(0,0) = rhowL*Kyy*LwL;
  Fvy_qy_true(0,1) = rhowL*Kyy*LwL*(-pc_Sw);
  Fvy_qy_true(1,0) = rhonL*Kyy*LnL;
  Fvy_qy_true(1,1) = 0.0;

  pde.diffusionViscous( x, y, time, qL, qxL, qyL, kxx, kxy, kyx, kyy );
  MatrixQ Fvx_qx = kxx*fcons_jac; //d(Fv)/d(grad q) = d(Fv)/d(grad u) * du/dq
  MatrixQ Fvx_qy = kxy*fcons_jac;
  MatrixQ Fvy_qx = kyx*fcons_jac;
  MatrixQ Fvy_qy = kyy*fcons_jac;

  SANS_CHECK_CLOSE( Fvx_qx_true(0,0), Fvx_qx(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fvx_qx_true(0,1), Fvx_qx(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fvx_qx_true(1,0), Fvx_qx(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fvx_qx_true(1,1), Fvx_qx(1,1), small_tol, close_tol );

  SANS_CHECK_CLOSE( Fvx_qy_true(0,0), Fvx_qy(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fvx_qy_true(0,1), Fvx_qy(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fvx_qy_true(1,0), Fvx_qy(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fvx_qy_true(1,1), Fvx_qy(1,1), small_tol, close_tol );

  SANS_CHECK_CLOSE( Fvy_qx_true(0,0), Fvy_qx(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fvy_qx_true(0,1), Fvy_qx(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fvy_qx_true(1,0), Fvy_qx(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fvy_qx_true(1,1), Fvy_qx(1,1), small_tol, close_tol );

  SANS_CHECK_CLOSE( Fvy_qy_true(0,0), Fvy_qy(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fvy_qy_true(0,1), Fvy_qy(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fvy_qy_true(1,0), Fvy_qy(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( Fvy_qy_true(1,1), Fvy_qy(1,1), small_tol, close_tol );

  //Checking viscous fluxes with additional viscosity term for single-phase stability
  scalar_visc = 1.46;
  PDEClass pde2(rhow_model, rhon_model, phi_model, krw_model, krn_model, muw_model, mun_model, K, pc_model, scalar_visc);

  fxvisc_trueL[0] = -rhowL*LwL*(Kxx*pwxL + Kxy*pwyL) - rhowL*scalar_visc*SwxL;
  fxvisc_trueL[1] = -rhonL*LnL*(Kxx*pnxL + Kxy*pnyL) + rhonL*scalar_visc*SwxL;
  fyvisc_trueL[0] = -rhowL*LwL*(Kyx*pwxL + Kyy*pwyL) - rhowL*scalar_visc*SwyL;
  fyvisc_trueL[1] = -rhonL*LnL*(Kyx*pnxL + Kyy*pnyL) + rhonL*scalar_visc*SwyL;

  fxvisc = 0;
  fyvisc = 0;
  pde2.fluxViscous( x, y, time, qL, qxL, qyL, fxvisc, fyvisc );
  SANS_CHECK_CLOSE( fxvisc_trueL[0], fxvisc[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fxvisc_trueL[1], fxvisc[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( fyvisc_trueL[0], fyvisc[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fyvisc_trueL[1], fyvisc[1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( source_fixedpressure_pnSw )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  const Real pref = 1.47;

  const Real rhow_ref = 62.4;
  const Real rhon_ref = 52.1;
  const Real Cw = 0.03;
  const Real Cn = 0.05;

  const Real phi_ref = 0.3;
  const Real Cr = 0.06;

  const Real muw = 1.0, mun = 2.0;
  const Real pcmax = 5.0;

  DensityModel rhow_model(rhow_ref, Cw, pref);
  DensityModel rhon_model(rhon_ref, Cn, pref);

  PorosityModel phi_model(phi_ref, Cr, pref);

  RelPermModel krw_model(2);
  RelPermModel krn_model(2);

  ViscModel muw_model(muw);
  ViscModel mun_model(mun);

  Real Kxx = 1.1, Kxy = 0.0, Kyx = 0.0, Kyy = 1.1; //test below requires an isotropic tensor
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc_model(pcmax);

  const Real pB1 = 15.82;
  const Real pB2 = 17.93;
  const Real Sw_in = 0.9;
  const Real Rw = 0.5;

  // Set up PyDicts
  PyDict fixedwellpressure1;
  fixedwellpressure1[SourceTwoPhase2DType_FixedPressureInflow_Param::params.pB] = pB1;
  fixedwellpressure1[SourceTwoPhase2DType_FixedPressureInflow_Param::params.Sw] = Sw_in;
  fixedwellpressure1[SourceTwoPhase2DType_FixedPressureInflow_Param::params.Rwellbore] = Rw;
  fixedwellpressure1[SourceTwoPhase2DType_FixedPressureInflow_Param::params.nParam] = 1;
  fixedwellpressure1[SourceTwoPhase2DType_FixedPressureInflow_Param::params.WellModel] = "Gaussian";
  fixedwellpressure1[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureInflow;

  PyDict fixedwellpressure2;
  fixedwellpressure2[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.pB] = pB2;
  fixedwellpressure2[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.Rwellbore] = Rw;
  fixedwellpressure2[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.nParam] = 1;
  fixedwellpressure2[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.WellModel] = "Gaussian";
  fixedwellpressure2[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureOutflow;

  Real xc1 = 10.0, yc1 = 15.0;
  Real xc2 = 25.0, yc2 = 50.0;
  Real R1 = 5.0, R2 = 1.0;

  PyDict well1;
  well1[SourceTwoPhase2DParam::params.Source] = fixedwellpressure1;
  well1[SourceTwoPhase2DParam::params.xcentroid] = xc1;
  well1[SourceTwoPhase2DParam::params.ycentroid] = yc1;
  well1[SourceTwoPhase2DParam::params.R] = R1;
  well1[SourceTwoPhase2DParam::params.Tmin] = 1.0;
  well1[SourceTwoPhase2DParam::params.Tmax] = 3.0;
  well1[SourceTwoPhase2DParam::params.smoothLr] = 1.0;
  well1[SourceTwoPhase2DParam::params.smoothT] = 0.2;
  well1[SourceTwoPhase2DParam::params.visibleAngle] = 2*PI;

  PyDict well2;
  well2[SourceTwoPhase2DParam::params.Source] = fixedwellpressure2;
  well2[SourceTwoPhase2DParam::params.xcentroid] = xc2;
  well2[SourceTwoPhase2DParam::params.ycentroid] = yc2;
  well2[SourceTwoPhase2DParam::params.R] = R2;
  well2[SourceTwoPhase2DParam::params.Tmin] = 5.0;
  well2[SourceTwoPhase2DParam::params.Tmax] = 10.0;
  well2[SourceTwoPhase2DParam::params.smoothLr] = 0.5;
  well2[SourceTwoPhase2DParam::params.smoothT] = 1.0;
  well2[SourceTwoPhase2DParam::params.visibleAngle] = 2*PI;

  PyDict sourceList;
  sourceList["well1"] = well1;
  sourceList["well2"] = well2;
  SourceTwoPhase2DListParam::checkInputs(sourceList);

  PDEClass pde(rhow_model, rhon_model, phi_model, krw_model, krn_model, muw_model, mun_model, K, pc_model, sourceList);

  // Static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 2 );

  // Flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == false );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // Function tests
  Real Sw = 0.375;
  Real Swx = -0.63, Swy = 0.27;
  Real pn = 25.34;
  Real pnx = -0.52, pny = 0.81;

  Real Sn = 1.0 - Sw;
  Real pc = pcmax*Sn;
  Real pw = pn - pc;

  Real krw = pow(Sw,2);
  Real krn = pow(Sn,2);

  Real rhow = rhow_ref*exp(Cw*(pw - pref));
  Real rhon = rhon_ref*exp(Cn*(pn - pref));

  Real lambda_w = krw/muw;
  Real lambda_n = krn/mun;

  Real pn_in = pn;
  Real Sn_in = 1.0 - Sw_in;
  Real pc_in = pcmax*Sn_in;
  Real pw_in = pn_in - pc_in;

  Real krw_in = pow(Sw_in, 2);
  Real krn_in = pow(1.0 - Sw_in, 2);

  Real rhow_in = rhow_ref*exp(Cw*(pw_in - pref));
  Real rhon_in = rhon_ref*exp(Cn*(pn_in - pref));

  Real lambda_w_in = krw_in/muw;
  Real lambda_n_in = krn_in/mun;

  // Set
  Real qDataPrim[2] = {pn,Sw};
  string qNamePrim[2] = {"pn","Sw"};
  ArrayQ qTrue = {pn,Sw};
  ArrayQ q;
  ArrayQ qx = {pnx, Swx};
  ArrayQ qy = {pny, Swy};
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 2 );
  SANS_CHECK_CLOSE( qTrue[0], q[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( qTrue[1], q[1], small_tol, close_tol );

  //------------------------------------------------------------

  Real x = 4.5, y = 15.0, t = 2.0;
  Real r1 = sqrt(pow(x - xc1, 2.0) +  pow(y - yc1, 2.0));
  Real r2 = sqrt(pow(x - xc2, 2.0) +  pow(y - yc2, 2.0));
  Real factor1 = (pn - pB1) / (exp(-1.0)*(1.0 + 2*log(R1/Rw)) - exp(- pow(-r1/R1, 2)) ) * exp(-pow(-r1/R1, 2)) * (4/(R1*R1) - pow(2*r1/(R1*R1), 2));
  Real factor2 = (pn - pB2) / (exp(-1.0)*(1.0 + 2*log(R2/Rw)) - exp(- pow(-r2/R2, 2)) ) * exp(-pow(-r2/R2, 2)) * (4/(R2*R2) - pow(2*r2/(R2*R2), 2));
  Real weight1 = 0.0, weight2 = 0.0;

  ArrayQ sourceTrue, source = 0.0;
  sourceTrue[0] = weight1*rhow_in*lambda_w_in*sqrt(Kxx*Kyy)*factor1;
  sourceTrue[1] = weight1*rhon_in*lambda_n_in*sqrt(Kxx*Kyy)*factor1;

  sourceTrue[0] += weight2*rhow*lambda_w*sqrt(Kxx*Kyy)*factor2;
  sourceTrue[1] += weight2*rhon*lambda_n*sqrt(Kxx*Kyy)*factor2;

  pde.source(x, y, t, q, qx, qy, source);
  SANS_CHECK_CLOSE( sourceTrue[0], source[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( sourceTrue[1], source[1], small_tol, close_tol);

  //------------------------------------------------------------

  x = 15.0, y = 15.0, t = 2.0;
  r1 = sqrt(pow(x - xc1, 2.0) +  pow(y - yc1, 2.0));
  r2 = sqrt(pow(x - xc2, 2.0) +  pow(y - yc2, 2.0));
  factor1 = (pn - pB1) / (exp(-1.0)*(1.0 + 2*log(R1/Rw)) - exp(- pow(-r1/R1, 2)) ) * exp(-pow(-r1/R1, 2)) * (4/(R1*R1) - pow(2*r1/(R1*R1), 2));
  factor2 = (pn - pB2) / (exp(-1.0)*(1.0 + 2*log(R2/Rw)) - exp(- pow(-r2/R2, 2)) ) * exp(-pow(-r2/R2, 2)) * (4/(R2*R2) - pow(2*r2/(R2*R2), 2));
  weight1 = 0.5, weight2 = 0.0;

  source = 0.0;
  sourceTrue[0] = weight1*rhow_in*lambda_w_in*sqrt(Kxx*Kyy)*factor1;
  sourceTrue[1] = weight1*rhon_in*lambda_n_in*sqrt(Kxx*Kyy)*factor1;

  sourceTrue[0] += weight2*rhow*lambda_w*sqrt(Kxx*Kyy)*factor2;
  sourceTrue[1] += weight2*rhon*lambda_n*sqrt(Kxx*Kyy)*factor2;

  pde.source(x, y, t, q, qx, qy, source);
  SANS_CHECK_CLOSE( sourceTrue[0], source[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( sourceTrue[1], source[1], small_tol, close_tol);

  //------------------------------------------------------------

  x = 12.0, y = 16.0, t = 2.0;
  r1 = sqrt(pow(x - xc1, 2.0) +  pow(y - yc1, 2.0));
  r2 = sqrt(pow(x - xc2, 2.0) +  pow(y - yc2, 2.0));
  factor1 = (pn - pB1) / (exp(-1.0)*(1.0 + 2*log(R1/Rw)) - exp(- pow(-r1/R1, 2)) ) * exp(-pow(-r1/R1, 2)) * (4/(R1*R1) - pow(2*r1/(R1*R1), 2));
  factor2 = (pn - pB2) / (exp(-1.0)*(1.0 + 2*log(R2/Rw)) - exp(- pow(-r2/R2, 2)) ) * exp(-pow(-r2/R2, 2)) * (4/(R2*R2) - pow(2*r2/(R2*R2), 2));
  weight1 = 1.0, weight2 = 0.0;

  source = 0.0;
  sourceTrue[0] = weight1*rhow_in*lambda_w_in*sqrt(Kxx*Kyy)*factor1;
  sourceTrue[1] = weight1*rhon_in*lambda_n_in*sqrt(Kxx*Kyy)*factor1;

  sourceTrue[0] += weight2*rhow*lambda_w*sqrt(Kxx*Kyy)*factor2;
  sourceTrue[1] += weight2*rhon*lambda_n*sqrt(Kxx*Kyy)*factor2;

  pde.source(x, y, t, q, qx, qy, source);
  SANS_CHECK_CLOSE( sourceTrue[0], source[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( sourceTrue[1], source[1], small_tol, close_tol);

  //------------------------------------------------------------

  x = 25.0, y = 49.0, t = 7.0;
  r1 = sqrt(pow(x - xc1, 2.0) +  pow(y - yc1, 2.0));
  r2 = sqrt(pow(x - xc2, 2.0) +  pow(y - yc2, 2.0));
  factor1 = (pn - pB1) / (exp(-1.0)*(1.0 + 2*log(R1/Rw)) - exp(- pow(-r1/R1, 2)) ) * exp(-pow(-r1/R1, 2)) * (4/(R1*R1) - pow(2*r1/(R1*R1), 2));
  factor2 = (pn - pB2) / (exp(-1.0)*(1.0 + 2*log(R2/Rw)) - exp(- pow(-r2/R2, 2)) ) * exp(-pow(-r2/R2, 2)) * (4/(R2*R2) - pow(2*r2/(R2*R2), 2));
  weight1 = 0.0, weight2 = 0.5;

  source = 0.0;
  sourceTrue[0] = weight1*rhow_in*lambda_w_in*sqrt(Kxx*Kyy)*factor1;
  sourceTrue[1] = weight1*rhon_in*lambda_n_in*sqrt(Kxx*Kyy)*factor1;

  sourceTrue[0] += weight2*rhow*lambda_w*sqrt(Kxx*Kyy)*factor2;
  sourceTrue[1] += weight2*rhon*lambda_n*sqrt(Kxx*Kyy)*factor2;

  pde.source(x, y, t, q, qx, qy, source);
  SANS_CHECK_CLOSE( sourceTrue[0], source[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( sourceTrue[1], source[1], small_tol, close_tol);

  //------------------------------------------------------------

  x = 25.0, y = 50.5, t = 7.0;
  r1 = sqrt(pow(x - xc1, 2.0) +  pow(y - yc1, 2.0));
  r2 = sqrt(pow(x - xc2, 2.0) +  pow(y - yc2, 2.0));
  factor1 = (pn - pB1) / (exp(-1.0)*(1.0 + 2*log(R1/Rw)) - exp(- pow(-r1/R1, 2)) ) * exp(-pow(-r1/R1, 2)) * (4/(R1*R1) - pow(2*r1/(R1*R1), 2));
  factor2 = (pn - pB2) / (exp(-1.0)*(1.0 + 2*log(R2/Rw)) - exp(- pow(-r2/R2, 2)) ) * exp(-pow(-r2/R2, 2)) * (4/(R2*R2) - pow(2*r2/(R2*R2), 2));
  weight1 = 0.0, weight2 = 1.0;

  source = 0.0;
  sourceTrue[0] = weight1*rhow_in*lambda_w_in*sqrt(Kxx*Kyy)*factor1;
  sourceTrue[1] = weight1*rhon_in*lambda_n_in*sqrt(Kxx*Kyy)*factor1;

  sourceTrue[0] += weight2*rhow*lambda_w*sqrt(Kxx*Kyy)*factor2;
  sourceTrue[1] += weight2*rhon*lambda_n*sqrt(Kxx*Kyy)*factor2;

  pde.source(x, y, t, q, qx, qy, source);
  SANS_CHECK_CLOSE( sourceTrue[0], source[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( sourceTrue[1], source[1], small_tol, close_tol);

  //Check source trace
  ArrayQ sourceL = 0, sourceR = 0;
  pde.sourceTrace(x, x, y, y, t, q, qx, qy, q, qx, qy, sourceL, sourceR);
  SANS_CHECK_CLOSE( 0.0, sourceL[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( 0.0, sourceL[1], small_tol, close_tol);
  SANS_CHECK_CLOSE( 0.0, sourceR[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( 0.0, sourceR[1], small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( interpResid )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;

  Real tol = 1e-13;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  RockPermModel K(1.1, 0.6, 0.2, 1.5);
  CapillaryModel pc(5);

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  Real pn = 2534.8;
  Real Sw = 0.784;

  // set
  Real qDataPrim[2] = {pn,Sw};
  string qNamePrim[2] = {"pn","Sw"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 2 );

  BOOST_CHECK_EQUAL(pde.nMonitor(), 2);

  DLA::VectorD<Real> vec(2);
  pde.interpResidVariable(q, vec);
  for (int i = 0; i < ArrayQ::M; i++)
    SANS_CHECK_CLOSE( q(i), vec(i), tol, tol );

  vec = 0;
  pde.interpResidGradient(q, vec);
  for (int i = 0; i < ArrayQ::M; i++)
    SANS_CHECK_CLOSE( q(i), vec(i), tol, tol );

  vec = 0;
  pde.interpResidBC(q, vec);
  for (int i = 0; i < ArrayQ::M; i++)
    SANS_CHECK_CLOSE( q(i), vec(i), tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/PDETwoPhase2D_pattern.txt", true );

  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  const Real pref = 1.47;

  const Real rhow_ref = 62.4;
  const Real rhon_ref = 52.1;
  const Real Cw = 0.03;
  const Real Cn = 0.05;

  const Real phi_ref = 0.3;
  const Real Cr = 0.06;

  const Real muw = 1.0, mun = 2.0;
  const Real pcmax = 5.0;

  DensityModel rhow_model(rhow_ref, Cw, pref);
  DensityModel rhon_model(rhon_ref, Cn, pref);

  PorosityModel phi_model(phi_ref, Cr, pref);

  RelPermModel krw_model(2);
  RelPermModel krn_model(2);

  ViscModel muw_model(muw);
  ViscModel mun_model(mun);

  RockPermModel K(1.1, 0.6, 0.2, 1.5);
  CapillaryModel pc_model(pcmax);

  PDEClass pde(rhow_model, rhon_model, phi_model, krw_model, krn_model, muw_model, mun_model, K, pc_model);

  pde.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()


//############################################################################//
BOOST_AUTO_TEST_SUITE( PDETwoPhase2D_CartTable_test_suite )

// create a simple Cartesian table file
void genCartTable( std::ofstream& file, const Real& mult = 1 )
{
  int imax = 5;
  int jmax = 7;
  Real dx = 20.;
  Real dy = 30.;

  file << imax << " " << jmax << " " << dx << " " << dy << std::endl;
  for (int i = 0; i < imax; i++)
  {
    for (int j = 0; j < jmax; j++)
      file << mult*(dx*i*jmax + dy*j) << " ";
    file << std::endl;
  }
  file.close();
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_pnSw )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_CartTable PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_CartTable RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  CapillaryModel pc(5);

  // porosity table lookup
  std::string filenamePoro = "lookupPoro.txt";
  std::ofstream filePoro(filenamePoro);
  genCartTable( filePoro );

  PyDict dictPoro;
  dictPoro[PorosityModel_CartTable_Params::params.PorosityFile] = filenamePoro;
  dictPoro[PorosityModel_CartTable_Params::params.Cr] = 0.01;
  PorosityModel_CartTable_Params::checkInputs(dictPoro);

  PorosityModel phi(dictPoro, pref);

  // permeability table lookup
  std::string filenamePerm = "lookupPerm.txt";
  std::ofstream filePerm(filenamePerm);
  genCartTable( filePerm );

  PyDict dictPerm;
  dictPerm[PermeabilityModel2D_CartTable_Params::params.PermeabilityFile] = filenamePerm;
  PermeabilityModel2D_CartTable_Params::checkInputs(dictPerm);

  RockPermModel K(dictPerm);

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == false );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasSourceTrace() == false );
  BOOST_CHECK( pde.hasSourceLiftedQuantity() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  //Copy constructor
  PDEClass pde2(pde);
  BOOST_CHECK( pde2.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde2.hasFluxAdvective() == false );
  BOOST_CHECK( pde2.hasFluxViscous() == true );
  BOOST_CHECK( pde2.hasSource() == false );
  BOOST_CHECK( pde2.hasSourceTrace() == false );
  BOOST_CHECK( pde2.hasSourceLiftedQuantity() == false );
  BOOST_CHECK( pde2.hasForcingFunction() == false );
  BOOST_CHECK( pde2.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde2.needsSolutionGradientforSource() == false );

  // Set up PyDicts
  PyDict fixedwellpressure;
  fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.pB] = 2350.0;
  fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.Rwellbore] = 0.5;
  fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.nParam] = 1;
  fixedwellpressure[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureOutflow;

  PyDict well;
  well[SourceTwoPhase2DParam::params.Source] = fixedwellpressure;
  well[SourceTwoPhase2DParam::params.xcentroid] = 10.0;
  well[SourceTwoPhase2DParam::params.ycentroid] = 15.0;
  well[SourceTwoPhase2DParam::params.R] = 5.0;
  well[SourceTwoPhase2DParam::params.Tmin] = 1.0;
  well[SourceTwoPhase2DParam::params.Tmax] = 3.0;
  well[SourceTwoPhase2DParam::params.smoothLr] = 1.0;
  well[SourceTwoPhase2DParam::params.smoothT] = 0.0;
  well[SourceTwoPhase2DParam::params.visibleAngle] = 2*PI;

  PyDict sourceList;
  sourceList["well"] = well;
  SourceTwoPhase2DListParam::checkInputs(sourceList);

  //Constructor with source terms
  PDEClass pde3(rhow, rhon, phi, krw, krn, muw, mun, K, pc, sourceList);

  BOOST_CHECK( pde3.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde3.hasFluxAdvective() == false );
  BOOST_CHECK( pde3.hasFluxViscous() == true );
  BOOST_CHECK( pde3.hasSource() == true );
  BOOST_CHECK( pde3.hasSourceTrace() == false );
  BOOST_CHECK( pde3.hasForcingFunction() == false );
  BOOST_CHECK( pde3.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde3.needsSolutionGradientforSource() == false );

  //Copy constructor
  PDEClass pde4(pde3);
  BOOST_CHECK( pde4.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde4.hasFluxAdvective() == false );
  BOOST_CHECK( pde4.hasFluxViscous() == true );
  BOOST_CHECK( pde4.hasSource() == true );
  BOOST_CHECK( pde4.hasSourceTrace() == false );
  BOOST_CHECK( pde4.hasForcingFunction() == false );
  BOOST_CHECK( pde4.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde4.needsSolutionGradientforSource() == false );

  // delete the written files
  std::remove(filenamePoro.c_str());
  std::remove(filenamePerm.c_str());
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux_pnSw )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_CartTable PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_CartTable RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  const Real pref = 1.47;

  const Real rhow_ref = 62.4;
  const Real rhon_ref = 52.1;
  const Real Cw = 0.03;
  const Real Cn = 0.05;
  const Real Cr = 0.01;

  const Real muw = 1.0, mun = 2.0;
  const Real pcmax = 5.0;

  DensityModel rhow_model(rhow_ref, Cw, pref);
  DensityModel rhon_model(rhon_ref, Cn, pref);

  RelPermModel krw_model(2);
  RelPermModel krn_model(2);

  ViscModel muw_model(muw);
  ViscModel mun_model(mun);

  CapillaryModel pc_model(pcmax);

  // porosity table lookup
  std::string filenamePoro = "lookupPoro.txt";
  std::ofstream filePoro(filenamePoro);
  genCartTable( filePoro, 0.001 );

  PyDict dictPoro;
  dictPoro[PorosityModel_CartTable_Params::params.PorosityFile] = filenamePoro;
  dictPoro[PorosityModel_CartTable_Params::params.Cr] = Cr;
  PorosityModel_CartTable_Params::checkInputs(dictPoro);

  PorosityModel phi_model(dictPoro, pref);

  // permeability table lookup
  std::string filenamePerm = "lookupPerm.txt";
  std::ofstream filePerm(filenamePerm);
  genCartTable( filePerm, 0.003 );

  PyDict dictPerm;
  dictPerm[PermeabilityModel2D_CartTable_Params::params.PermeabilityFile] = filenamePerm;
  PermeabilityModel2D_CartTable_Params::checkInputs(dictPerm);

  RockPermModel K_model(dictPerm);

  Real scalar_visc = 0.0;
  bool gravity_on = true;

  PDEClass pde(rhow_model, rhon_model, phi_model, krw_model, krn_model, muw_model, mun_model, K_model, pc_model,
               {}, scalar_visc, gravity_on);

  // Static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 2 );

  // Flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == gravity_on );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasSourceTrace() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  BOOST_CHECK_EQUAL( pde.nMonitor(), 2 );

  // Function tests
  Real x = 49, y = 112, time = 0;   //  (x,y) used in lookup tables
  Real nx = 0.35, ny = -0.26;

  #if 0
  {
    Real pn = 0;
    printf( "phi(1,2) = %22.15e\n", phi_model.porosity(x,y,time,pn) );
    Real Kxx, Kxy, Kyx, Kyy;
    K_model.permeability(x,y,time,Kxx, Kxy, Kyx, Kyy);
    printf( "K(1,2) = %22.15e\n", Kxx );
    fflush(stdout);
  }
  #endif
  Real Kxx_true = 1.11;
  Real Kxy_true =    0;
  Real Kyx_true =    0;
  Real Kyy_true = 1.11;

  Real SwL = 0.375, SwR = 0.418;
  Real SwxL = -0.63, SwxR = -0.37;
  Real SwyL = 0.27, SwyR = 0.46;
  Real pnL = 25.34, pnR = 31.86;
  Real pnxL = -0.52, pnxR = -0.29;
  Real pnyL = 0.96, pnyR = 0.81;

  Real SnL = 1.0 - SwL;
  Real SnR = 1.0 - SwR;
  Real pcL = pcmax*SnL;
  Real pcR = pcmax*SnR;
  Real pwL = pnL - pcL;
  Real pwR = pnR - pcR;
  Real pwxL = pnxL - pcmax*(-SwxL);
  Real pwxR = pnxR - pcmax*(-SwxR);
  Real pwyL = pnyL - pcmax*(-SwyL);
  Real pwyR = pnyR - pcmax*(-SwyR);

  Real phi_true = 0.37*exp(Cr*(pnL - pref));
  Real phi_pn_true = Cr*phi_true;

  Real krwL = pow(SwL,2);
  Real krwR = pow(SwR,2);

  Real krnL = pow(SnL,2);
  Real krnR = pow(SnR,2);

  Real krw_SwL = 2*SwL;
  Real krw_SwR = 2*SwR;

  Real krn_SwL = -2*SnL;
  Real krn_SwR = -2*SnR;

  Real rhowL = rhow_ref*exp(Cw*(pwL - pref));
  Real rhowR = rhow_ref*exp(Cw*(pwR - pref));

  Real rhonL = rhon_ref*exp(Cn*(pnL - pref));
  Real rhonR = rhon_ref*exp(Cn*(pnR - pref));

  Real rhow_pnL = Cw*rhowL;
  Real rhow_SwL = Cw*rhowL*pcmax;
  Real rhon_pnL = Cn*rhonL;

  Real LwL = krwL/muw;
  Real LwR = krwR/muw;

  Real LnL = krnL/mun;
  Real LnR = krnR/mun;

  Real Lw_SwL = krw_SwL / muw;
  Real Lw_SwR = krw_SwR / muw;

  Real Ln_SwL = krn_SwL / mun;
  Real Ln_SwR = krn_SwR / mun;

  Real grav_mask = 6.9468E-3 * gravity_on; //Units: [psi ft^2 / lb]

  // Set
  Real qLDataPrim[2] = {pnL,SwL};
  Real qRDataPrim[2] = {pnR,SwR};
  string qNamePrim[2] = {"pn","Sw"};
  ArrayQ qLTrue = {pnL,SwL};
  ArrayQ qRTrue = {pnR,SwR};
  ArrayQ qL, qR;
  pde.setDOFFrom( qL, qLDataPrim, qNamePrim, 2 );
  pde.setDOFFrom( qR, qRDataPrim, qNamePrim, 2 );
  SANS_CHECK_CLOSE( qLTrue[0], qL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( qLTrue[1], qL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( qRTrue[0], qR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( qRTrue[1], qR[1], small_tol, close_tol );

  Real Sw_test, Sn_test, pw_test, pn_test;
  pde.variableInterpreter().eval(qL, pw_test, pn_test, Sw_test, Sn_test);
  SANS_CHECK_CLOSE( pwL, pw_test, small_tol, close_tol );
  SANS_CHECK_CLOSE( pnL, pn_test, small_tol, close_tol );
  SANS_CHECK_CLOSE( SwL, Sw_test, small_tol, close_tol );
  SANS_CHECK_CLOSE( SnL, Sn_test, small_tol, close_tol );

  // Conservative flux
  ArrayQ ucons_true, ucons;
  ucons_true[0] = rhowL*phi_true*SwL;
  ucons_true[1] = rhonL*phi_true*SnL;
  pde.masterState( x, y, time, qL, ucons );
  SANS_CHECK_CLOSE( ucons_true[0], ucons[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( ucons_true[1], ucons[1], small_tol, close_tol );

  // Conservative flux jacobian
  MatrixQ fcons_jac_true, fcons_jac;
  fcons_jac_true(0,0) = (phi_pn_true*rhowL + phi_true*rhow_pnL)*SwL;
  fcons_jac_true(0,1) = (                    phi_true*rhow_SwL)*SwL + phi_true*rhowL;
  fcons_jac_true(1,0) = (phi_pn_true*rhonL + phi_true*rhon_pnL)*SnL;
  fcons_jac_true(1,1) = phi_true*rhonL*(-1);
  pde.jacobianMasterState( x, y, time, qL, fcons_jac );
  SANS_CHECK_CLOSE( fcons_jac_true(0,0), fcons_jac(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_jac_true(0,1), fcons_jac(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_jac_true(1,0), fcons_jac(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_jac_true(1,1), fcons_jac(1,1), small_tol, close_tol );

  // Conservative flux
  ArrayQ fcons_true, fcons = 0;
  fcons_true[0] = rhowL*phi_true*SwL;
  fcons_true[1] = rhonL*phi_true*SnL;
  pde.fluxAdvectiveTime( x, y, time, qL, fcons );
  SANS_CHECK_CLOSE( fcons_true[0], fcons[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fcons_true[1], fcons[1], small_tol, close_tol );

  // Advective flux
  ArrayQ fxadv_true, fyadv_true;
  fxadv_true[0] = -rhowL*LwL*(Kxy_true*rhowL*grav_mask);
  fxadv_true[1] = -rhonL*LnL*(Kxy_true*rhonL*grav_mask);
  fyadv_true[0] = -rhowL*LwL*(Kyy_true*rhowL*grav_mask);
  fyadv_true[1] = -rhonL*LnL*(Kyy_true*rhonL*grav_mask);
  ArrayQ fxadv = 0.0, fyadv = 0.0;
  pde.fluxAdvective( x, y, time, qL, fxadv, fyadv );
  SANS_CHECK_CLOSE( fxadv_true[0], fxadv[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fxadv_true[1], fxadv[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( fyadv_true[0], fyadv[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fyadv_true[1], fyadv[1], small_tol, close_tol );

  ArrayQ fn_adv = 0;
  pde.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn_adv );
  SANS_CHECK_CLOSE( 0.0, fn_adv[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, fn_adv[1], small_tol, close_tol );

  // Jacobian of advective flux
  MatrixQ fxadv_u = 0, fyadv_u = 0.0;
  BOOST_CHECK_THROW( pde.jacobianFluxAdvective( x, y, time, qL, fxadv_u, fyadv_u ), SANSException );

  // Viscous flux
  ArrayQ fxvisc_trueL, fyvisc_trueL;
  ArrayQ fxvisc = 0, fyvisc = 0;

  //Volume viscous flux does not contain the gravity term
  fxvisc_trueL[0] = -rhowL*LwL*(Kxx_true*pwxL + Kxy_true*pwyL);
  fxvisc_trueL[1] = -rhonL*LnL*(Kxx_true*pnxL + Kxy_true*pnyL);
  fyvisc_trueL[0] = -rhowL*LwL*(Kyx_true*pwxL + Kyy_true*pwyL);
  fyvisc_trueL[1] = -rhonL*LnL*(Kyx_true*pnxL + Kyy_true*pnyL);
  ArrayQ qxL = {pnxL, SwxL};
  ArrayQ qyL = {pnyL, SwyL};
  pde.fluxViscous( x, y, time, qL, qxL, qyL, fxvisc, fyvisc );
  SANS_CHECK_CLOSE( fxvisc_trueL[0], fxvisc[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fxvisc_trueL[1], fxvisc[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( fyvisc_trueL[0], fyvisc[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fyvisc_trueL[1], fyvisc[1], small_tol, close_tol );

  ArrayQ qxR = {pnxR, SwxR};
  ArrayQ qyR = {pnyR, SwyR};
  ArrayQ fxvisc_trueR, fyvisc_trueR;

  //Interface viscous fluxes include gravity contributions
  pwyL += rhowL*grav_mask;
  pwyR += rhowR*grav_mask;
  pnyL += rhonL*grav_mask;
  pnyR += rhonR*grav_mask;

  fxvisc_trueL[0] = -rhowL*LwL*(Kxx_true*pwxL + Kxy_true*pwyL);
  fxvisc_trueL[1] = -rhonL*LnL*(Kxx_true*pnxL + Kxy_true*pnyL);
  fyvisc_trueL[0] = -rhowL*LwL*(Kyx_true*pwxL + Kyy_true*pwyL);
  fyvisc_trueL[1] = -rhonL*LnL*(Kyx_true*pnxL + Kyy_true*pnyL);

  fxvisc_trueR[0] = -rhowR*LwR*(Kxx_true*pwxR + Kxy_true*pwyR);
  fxvisc_trueR[1] = -rhonR*LnR*(Kxx_true*pnxR + Kxy_true*pnyR);
  fyvisc_trueR[0] = -rhowR*LwR*(Kyx_true*pwxR + Kyy_true*pwyR);
  fyvisc_trueR[1] = -rhonR*LnR*(Kyx_true*pnxR + Kyy_true*pnyR);

  Real KgradpwL = (Kxx_true*pwxL + Kxy_true*pwyL)*nx + (Kyx_true*pwxL + Kyy_true*pwyL)*ny;
  Real KgradpwR = (Kxx_true*pwxR + Kxy_true*pwyR)*nx + (Kyx_true*pwxR + Kyy_true*pwyR)*ny;

  Real KgradpnL = (Kxx_true*pnxL + Kxy_true*pnyL)*nx + (Kyx_true*pnxL + Kyy_true*pnyL)*ny;
  Real KgradpnR = (Kxx_true*pnxR + Kxy_true*pnyR)*nx + (Kyx_true*pnxR + Kyy_true*pnyR)*ny;

  Real cL = (Lw_SwL*LnL*KgradpwL - LwL*Ln_SwL*KgradpnL) / (LwL + LnL);
  Real cR = (Lw_SwR*LnR*KgradpwR - LwR*Ln_SwR*KgradpnR) / (LwR + LnR);

  const Real eps = 1e-3;
  const Real alpha = 40;

  Real rw_cL = rhowL*cL;
  Real rw_cR = rhowR*cR;
  Real rn_cL = rhonL*cL;
  Real rn_cR = rhonR*cR;
  Real cw_max = smoothmax( smoothabs0(rw_cL, eps), smoothabs0(rw_cR, eps), alpha );
  Real cn_max = smoothmax( smoothabs0(rn_cL, eps), smoothabs0(rn_cR, eps), alpha );

  ArrayQ fn_true = 0.5*(fxvisc_trueL + fxvisc_trueR)*nx + 0.5*(fyvisc_trueL + fyvisc_trueR)*ny;
  fn_true(0) += 0.5*cw_max*(SwL - SwR);
  fn_true(1) -= 0.5*cn_max*(SwL - SwR);
  ArrayQ fn = 0.0;
  pde.fluxViscous( x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );
  SANS_CHECK_CLOSE( fn_true[0], fn[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fn_true[1], fn[1], small_tol, close_tol );

  //Finite volume viscous flux
  //Need to upwind mobilities to the left state for this particular problem
  fxvisc_trueL[0] = -rhowL*LwL*(Kxx_true*pwxL + Kxy_true*pwyL);
  fxvisc_trueL[1] = -rhonL*LnL*(Kxx_true*pnxL + Kxy_true*pnyL);
  fyvisc_trueL[0] = -rhowL*LwL*(Kyx_true*pwxL + Kyy_true*pwyL);
  fyvisc_trueL[1] = -rhonL*LnL*(Kyx_true*pnxL + Kyy_true*pnyL);

  fxvisc_trueR[0] = -rhowR*LwL*(Kxx_true*pwxR + Kxy_true*pwyR);
  fxvisc_trueR[1] = -rhonR*LnL*(Kxx_true*pnxR + Kxy_true*pnyR);
  fyvisc_trueR[0] = -rhowR*LwL*(Kyx_true*pwxR + Kyy_true*pwyR);
  fyvisc_trueR[1] = -rhonR*LnL*(Kyx_true*pnxR + Kyy_true*pnyR);

  ArrayQ fn_FV_true = 0.5*(fxvisc_trueL + fxvisc_trueR)*nx + 0.5*(fyvisc_trueL + fyvisc_trueR)*ny;
  ArrayQ fn_FV = 0.0;
  pde.fluxViscous_FV_TPFA( x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn_FV );
  SANS_CHECK_CLOSE( fn_FV_true[0], fn_FV[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( fn_FV_true[1], fn_FV[1], small_tol, close_tol );

  //Revert gravity contribution
//pwyL -= rhowL*grav_mask;
//pwyR -= rhowR*grav_mask;
//pnyL -= rhonL*grav_mask;
//pnyR -= rhonR*grav_mask;

  // Viscous diffusion matrix
  Real pc_Sw = -pcmax;
  MatrixQ Fvx_qx_true, Fvx_qy_true, Fvy_qx_true, Fvy_qy_true;
  MatrixQ kxx = 0, kxy = 0, kyx = 0, kyy = 0;
  Fvx_qx_true(0,0) = rhowL*Kxx_true*LwL;
  Fvx_qx_true(0,1) = rhowL*Kxx_true*LwL*(-pc_Sw);
  Fvx_qx_true(1,0) = rhonL*Kxx_true*LnL;
  Fvx_qx_true(1,1) = 0.0;

  Fvx_qy_true(0,0) = rhowL*Kxy_true*LwL;
  Fvx_qy_true(0,1) = rhowL*Kxy_true*LwL*(-pc_Sw);
  Fvx_qy_true(1,0) = rhonL*Kxy_true*LnL;
  Fvx_qy_true(1,1) = 0.0;

  Fvy_qx_true(0,0) = rhowL*Kyx_true*LwL;
  Fvy_qx_true(0,1) = rhowL*Kyx_true*LwL*(-pc_Sw);
  Fvy_qx_true(1,0) = rhonL*Kyx_true*LnL;
  Fvy_qx_true(1,1) = 0.0;

  Fvy_qy_true(0,0) = rhowL*Kyy_true*LwL;
  Fvy_qy_true(0,1) = rhowL*Kyy_true*LwL*(-pc_Sw);
  Fvy_qy_true(1,0) = rhonL*Kyy_true*LnL;
  Fvy_qy_true(1,1) = 0.0;

  pde.diffusionViscous( x, y, time, qL, qxL, qyL, kxx, kxy, kyx, kyy );
  MatrixQ Fvx_qx = kxx*fcons_jac; //d(Fv)/d(grad q) = d(Fv)/d(grad u) * du/dq
  MatrixQ Fvx_qy = kxy*fcons_jac;
  MatrixQ Fvy_qx = kyx*fcons_jac;
  MatrixQ Fvy_qy = kyy*fcons_jac;
  #if 0
  cout << setprecision(16);
  cout << "kxx = " << kxx << endl;
  cout << "kyy = " << kyy << endl;
  cout << "fcons_jac = " << fcons_jac << endl;
  cout << "kxx*fcons_jac = " << Fvx_qx << endl;
  #endif

  SANS_CHECK_CLOSE( Fvx_qx_true(0,0), Fvx_qx(0,0), 10*small_tol, 10*close_tol );
  SANS_CHECK_CLOSE( Fvx_qx_true(0,1), Fvx_qx(0,1), 10*small_tol, 10*close_tol );
  SANS_CHECK_CLOSE( Fvx_qx_true(1,0), Fvx_qx(1,0), 10*small_tol, 10*close_tol );
  SANS_CHECK_CLOSE( Fvx_qx_true(1,1), Fvx_qx(1,1), 10*small_tol, 10*close_tol );

  SANS_CHECK_CLOSE( Fvx_qy_true(0,0), Fvx_qy(0,0), 10*small_tol, 10*close_tol );
  SANS_CHECK_CLOSE( Fvx_qy_true(0,1), Fvx_qy(0,1), 10*small_tol, 10*close_tol );
  SANS_CHECK_CLOSE( Fvx_qy_true(1,0), Fvx_qy(1,0), 10*small_tol, 10*close_tol );
  SANS_CHECK_CLOSE( Fvx_qy_true(1,1), Fvx_qy(1,1), 10*small_tol, 10*close_tol );

  SANS_CHECK_CLOSE( Fvy_qx_true(0,0), Fvy_qx(0,0), 10*small_tol, 10*close_tol );
  SANS_CHECK_CLOSE( Fvy_qx_true(0,1), Fvy_qx(0,1), 10*small_tol, 10*close_tol );
  SANS_CHECK_CLOSE( Fvy_qx_true(1,0), Fvy_qx(1,0), 10*small_tol, 10*close_tol );
  SANS_CHECK_CLOSE( Fvy_qx_true(1,1), Fvy_qx(1,1), 10*small_tol, 10*close_tol );

  SANS_CHECK_CLOSE( Fvy_qy_true(0,0), Fvy_qy(0,0), 10*small_tol, 10*close_tol );
  SANS_CHECK_CLOSE( Fvy_qy_true(0,1), Fvy_qy(0,1), 10*small_tol, 10*close_tol );
  SANS_CHECK_CLOSE( Fvy_qy_true(1,0), Fvy_qy(1,0), 10*small_tol, 10*close_tol );
  SANS_CHECK_CLOSE( Fvy_qy_true(1,1), Fvy_qy(1,1), 10*small_tol, 10*close_tol );

  // delete the written files
  std::remove(filenamePoro.c_str());
  std::remove(filenamePerm.c_str());
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
