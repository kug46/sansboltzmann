// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// DensityModel_btest
// testing of DensityModel classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/PorousMedia/DensityModel.h"


using namespace std;
using namespace SANS;

namespace SANS
{

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( DensityModel_Constant_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Constructor )
{
  const Real tol = 1.e-13;
  Real rho = 52.1;

  PyDict d;
  d[DensityModel_Constant_Params::params.rho] = rho;

  //Check dictionary
  DensityModel_Constant_Params::checkInputs(d);

  Real dummy_x = 0.0, dummy_y = 0.0, dummy_time = 0.0;
  Real pn = 2500.0;

  // PyDict constructor
  DensityModel_Constant rho_model(d);
  BOOST_CHECK_CLOSE( rho_model.density(dummy_x, dummy_time, pn), rho, tol );
  BOOST_CHECK_CLOSE( rho_model.density(dummy_x, dummy_y, dummy_time, pn), rho, tol );

  // copy constructor
  DensityModel_Constant rho_model2(rho_model);
  BOOST_CHECK_CLOSE( rho_model2.density(dummy_x, dummy_time, pn), rho, tol);
  BOOST_CHECK_CLOSE( rho_model2.density(dummy_x, dummy_y, dummy_time, pn), rho, tol);

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Evaluations )
{
//  const Real tol = 1.e-13;
  Real rho = 0.3;

  PyDict d;
  d[DensityModel_Constant_Params::params.rho] = rho;

  //Check dictionary
  DensityModel_Constant_Params::checkInputs(d);

  Real dummy_x = 0.0, dummy_y = 0.0, dummy_time = 0.0;
  Real pn = 2500.0;
  Real pnx = -0.75;
  Real pny =  0.41;

  // PyDict constructor
  DensityModel_Constant rho_model(d);

  Real rho_p, rhox, rhoy;

  //1D
  rho_model.densityJacobian(dummy_x, dummy_time, pn, rho_p);
  rho_model.densityGradient(dummy_x, dummy_time, pn, pnx, rhox);
  BOOST_CHECK_EQUAL( rho_p, 0.0 );
  BOOST_CHECK_EQUAL( rhox, 0.0 );

  //2D
  rho_model.densityJacobian(dummy_x, dummy_y, dummy_time, pn, rho_p);
  rho_model.densityGradient(dummy_x, dummy_y, dummy_time, pn, pnx, pny, rhox, rhoy);
  BOOST_CHECK_EQUAL( rho_p, 0.0 );
  BOOST_CHECK_EQUAL( rhox, 0.0 );
  BOOST_CHECK_EQUAL( rhoy, 0.0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/DensityModel_Constant_pattern.txt", true );

  Real rho = 52.1;

  PyDict d;
  d[DensityModel_Constant_Params::params.rho] = rho;

  // PyDict constructor
  DensityModel_Constant rho_model(d);

  rho_model.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()


//############################################################################//
BOOST_AUTO_TEST_SUITE( DensityModel_Compressible_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Constructor )
{
  const Real tol = 1.e-13;
  Real rho_ref = 0.3;
  Real C = 2e-5;
  Real pref = 14.7;

  PyDict d;
  d[DensityModel_Comp_Params::params.rho_ref] = rho_ref;
  d[DensityModel_Comp_Params::params.C] = C;

  //Check dictionary
  DensityModel_Comp_Params::checkInputs(d);

  // PyDict constructor
  DensityModel_Comp rho_model(d, pref);
  BOOST_CHECK_CLOSE( rho_model.rho_ref(), rho_ref, tol );
  BOOST_CHECK_CLOSE( rho_model.C(), C, tol );

  // copy constructor
  DensityModel_Comp rho_model2(rho_model);
  BOOST_CHECK_CLOSE( rho_model2.rho_ref(), rho_ref, tol );
  BOOST_CHECK_CLOSE( rho_model2.C(), C, tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Evaluations )
{
  const Real tol = 1.e-13;
  Real rho_ref = 0.3;
  Real C = 2e-5;
  Real pref = 14.7;

  PyDict d;
  d[DensityModel_Comp_Params::params.rho_ref] = rho_ref;
  d[DensityModel_Comp_Params::params.C] = C;

  //Check dictionary
  DensityModel_Comp_Params::checkInputs(d);

  // PyDict constructor
  DensityModel_Comp rho_model(d, pref);

  Real dummy_x = 0.0, dummy_y = 0.0, dummy_time = 0.0;
  Real pn = 2500.0;
  Real pnx = -0.75;
  Real pny =  0.41;

  Real rho_true = rho_ref*exp(C*(pn - pref));
  Real rho_pn_true = C*rho_true;
  Real rhox_true = rho_pn_true*pnx;
  Real rhoy_true = rho_pn_true*pny;

  Real rho, rho_pn, rhox, rhoy;

  //1D
  rho = rho_model.density(dummy_x, dummy_time, pn);
  rho_model.densityJacobian(dummy_x, dummy_time, pn, rho_pn);
  rho_model.densityGradient(dummy_x, dummy_time, pn, pnx, rhox);
  BOOST_CHECK_CLOSE( rho   , rho_true   , tol );
  BOOST_CHECK_CLOSE( rho_pn, rho_pn_true, tol );
  BOOST_CHECK_CLOSE( rhox  , rhox_true  , tol );

  //2D
  rho = rho_model.density(dummy_x, dummy_y, dummy_time, pn);
  rho_model.densityJacobian(dummy_x, dummy_y, dummy_time, pn, rho_pn);
  rho_model.densityGradient(dummy_x, dummy_y, dummy_time, pn, pnx, pny, rhox, rhoy);
  BOOST_CHECK_CLOSE( rho   , rho_true   , tol );
  BOOST_CHECK_CLOSE( rho_pn, rho_pn_true, tol );
  BOOST_CHECK_CLOSE( rhox  , rhox_true  , tol );
  BOOST_CHECK_CLOSE( rhoy  , rhoy_true  , tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/DensityModel_Comp_pattern.txt", true );

  Real rho_ref = 52.1;
  Real C = 2e-5;
  Real pref = 14.7;

  PyDict d;
  d[DensityModel_Comp_Params::params.rho_ref] = rho_ref;
  d[DensityModel_Comp_Params::params.C] = C;

  // PyDict constructor
  DensityModel_Comp rho_model(d, pref);

  rho_model.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
