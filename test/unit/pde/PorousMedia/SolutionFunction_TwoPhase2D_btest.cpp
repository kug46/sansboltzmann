// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// SolutionFunction_TwoPhase2D_btest
//
// test of SolutionFunction_TwoPhase2D classes

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "pde/PorousMedia/Q2DPrimitive_pnSw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/SolutionFunction_TwoPhase2D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( SolutionFunction_TwoPhase2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor2D_SingleWell_Peaceman )
{
  typedef CapillaryModel_Linear CapillaryModel;
  typedef QTypePrimitive_pnSw QType;
  typedef Q2D<QType, CapillaryModel, TraitsSizeTwoPhase> QInterpreter;

  const Real pB = 2000.0;
  const Real R_bore = 1.0/6.0;
  const Real Sw = 1.0;
  const Real mu = 1.0;
  const Real Q = -1000;
  const Real Kxx = 1.0;
  const Real Kyy = 0.35;

  typedef SolutionFunction_TwoPhase2D_SingleWell_Peaceman<QInterpreter, TraitsSizeTwoPhase> SolutionType;
  typedef SolutionType::ArrayQ<Real> ArrayQ;

  PyDict soldict;
  soldict[SolutionType::ParamsType::params.pB] = pB;
  soldict[SolutionType::ParamsType::params.Rwellbore] = R_bore;
  soldict[SolutionType::ParamsType::params.Sw] = Sw;
  soldict[SolutionType::ParamsType::params.mu] = mu;
  soldict[SolutionType::ParamsType::params.Q] = Q;
  soldict[SolutionType::ParamsType::params.xcentroid] = 0.0;
  soldict[SolutionType::ParamsType::params.ycentroid] = 0.0;
  soldict[SolutionType::ParamsType::params.Kxx] = Kxx;
  soldict[SolutionType::ParamsType::params.Kyy] = Kyy;

  CapillaryModel pc(0.0);
  QInterpreter qInterpreter(pc);

  SolutionType sol(soldict, qInterpreter);

  const Real tol = 1e-13;

  Real Kxx_Kyy = Kxx/Kyy;
  Real Kyy_Kxx = Kyy/Kxx;

  {
    Real x = 20.215, y = 31.063, t = 1.0;

    Real du = pow(Kyy_Kxx, 0.25) * x;
    Real dv = pow(Kxx_Kyy, 0.25) * y;
    Real r_uv = sqrt(du*du + dv*dv);
    Real rw_bar = 0.5*R_bore*( pow(Kyy_Kxx, 0.25) + pow(Kxx_Kyy, 0.25) );

    Real p = pB - (mu*Q)/(2.0*PI*sqrt(Kxx*Kyy))*log(r_uv/rw_bar);
    ArrayQ q = sol(x, y, t);
    BOOST_CHECK_CLOSE( p, q[0], tol );
    BOOST_CHECK_CLOSE( Sw, q[1], tol );
  }

  {
    Real x = -100.415, y = 51.063, t = 1.0;

    Real du = pow(Kyy_Kxx, 0.25) * x;
    Real dv = pow(Kxx_Kyy, 0.25) * y;
    Real r_uv = sqrt(du*du + dv*dv);
    Real rw_bar = 0.5*R_bore*( pow(Kyy_Kxx, 0.25) + pow(Kxx_Kyy, 0.25) );

    Real p = pB - (mu*Q)/(2.0*PI*sqrt(Kxx*Kyy))*log(r_uv/rw_bar);
    ArrayQ q = sol(x, y, t);
    BOOST_CHECK_CLOSE( p, q[0], tol );
    BOOST_CHECK_CLOSE( Sw, q[1], tol );
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
