// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEBuckleyLeverett_ArtificialViscosity1D_btest
//
// test of 1-D BuckleyLeverett PDE class with artificial viscosity

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "pde/PorousMedia/TraitsBuckleyLeverettArtificialViscosity.h"
#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/PDEBuckleyLeverett_ArtificialViscosity1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"

//Explicitly instantiate the class so coverage information is correct
namespace SANS
{

typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel_PowerLaw, CapillaryModel_Linear> TraitsModelClass;

template class PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass>;
template class PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverettArtificialViscosity, TraitsModelClass>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEBuckleyLeverett_ArtificialViscosity1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test_SaturationWet )
{
  {
    typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;

    BOOST_CHECK( PDEClass::D == 1 );
    BOOST_CHECK( PDEClass::N == 1 );
  }

  {
    typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverettArtificialViscosity, TraitsModelClass> PDEClass;

    BOOST_CHECK( PDEClass::D == 1 );
    BOOST_CHECK( PDEClass::N == 2 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_SaturationWet )
{
  typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEAVClass;

  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.4;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real pc_max = 5.0;
  CapillaryModel_Linear pc_model(pc_max);
  const Real K = 0.25;

  const int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEAVClass pdeAV(order, hasSpaceTimeDiffusion, phi, uT, kr_model, mu_w, mu_n, K, pc_model);

  BOOST_CHECK_EQUAL(  phi, pdeAV.porosity());
  BOOST_CHECK_EQUAL(   uT, pdeAV.totalVelocity());
  BOOST_CHECK_EQUAL( mu_w, pdeAV.viscosityWet());
  BOOST_CHECK_EQUAL( mu_n, pdeAV.viscosityNonwet());
  BOOST_CHECK_EQUAL(    K, pdeAV.permeability());
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux_SaturationWet_SpatialDiffusion )
{
  typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEAVClass;
  typedef PDEAVClass::ArrayQ<Real> ArrayQ;
  typedef PDEAVClass::MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD1::D,Real> MatrixSym;
  typedef DLA::MatrixSymS<PhysD2::D,Real> MatrixSym_ST;
  typedef MakeTuple<ParamTuple, MatrixSym, Real>::type ParamType;
  typedef MakeTuple<ParamTuple, MatrixSym_ST, Real>::type ParamType_ST;

  const Real tol = 1.e-13;

  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.4;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real pc_max = 5.0;
  CapillaryModel_Linear pc_model(pc_max);
  const Real K = 0.25;

  int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEAVClass pdeAV(order, hasSpaceTimeDiffusion, phi, uT, kr_model, mu_w, mu_n, K, pc_model);

  // Static tests
  BOOST_CHECK( pdeAV.D == 1 );
  BOOST_CHECK( pdeAV.N == 1 );

  // Flux components
  BOOST_CHECK( pdeAV.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pdeAV.hasFluxAdvective() == true );
  BOOST_CHECK( pdeAV.hasFluxViscous() == true );
  BOOST_CHECK( pdeAV.hasSource() == false );
  BOOST_CHECK( pdeAV.hasForcingFunction() == false );

  BOOST_CHECK( pdeAV.needsSolutionGradientforSource() == false );

  // Function tests

  Real x, time;
  Real Sw, Swx, Swt;
//  Real Swxx;
  Real krw, krn;
  Real Lw, Ln, Lw_Sw, Ln_Sw;
  Real fw_true, fw_Sw_true;

  x = time = 0; // not actually used in functions
  Sw = 0.375;
  Swx = -0.63;
  Swt = -0.27;
//  Swxx = 1.93;
  krw = pow(Sw,2);
  krn = pow(1-Sw,2);
  Lw = krw/mu_w;
  Ln = krn/mu_n;
  fw_true = (Lw)/(Lw + Ln);

  Lw_Sw = (2*Sw)/mu_w;
  Ln_Sw = -2*(1-Sw)/mu_n;
  fw_Sw_true = (Lw_Sw*Ln - Lw*Ln_Sw)/pow(Lw + Ln, 2);


  // Set
  Real qDataPrim[1] = {Sw};
  string qNamePrim[1] = {"Sw"};
  ArrayQ qTrue = Sw;
  ArrayQ q;
  pdeAV.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );
  ArrayQ qt = Swt;
  ArrayQ qx = Swx;
//  ArrayQ qxx = Swxx;

  Real hxx = 0.1, htt = 0.2;
  MatrixSym logH = {log(hxx)};
  MatrixSym_ST logH_ST = {{log(hxx)},{0.0, log(htt)}};

  ParamType paramL(logH, 0.2); // grid spacing and sensor values
  ParamType_ST paramL_ST(logH_ST, 0.2);

  // Conservative flux
  ArrayQ fcons_true = Sw;
  ArrayQ fcons = 0;
  pdeAV.fluxAdvectiveTime( paramL, x, time, q, fcons );
  BOOST_CHECK_CLOSE( fcons_true, fcons, tol );

  // Master state
  ArrayQ ucons_true = Sw;
  ArrayQ ucons = 0;
  pdeAV.masterState( paramL, x, time, q, ucons );
  BOOST_CHECK_CLOSE( ucons_true, ucons, tol );

  // Strong form conservative flux
  ArrayQ strongPDE = 0;
  pdeAV.strongFluxAdvectiveTime( paramL, x, time, q, qt, strongPDE );
  BOOST_CHECK_CLOSE( qt, strongPDE, tol );

  // Advective flux
  ArrayQ fadv_true = uT*fw_true/phi;
  ArrayQ fadv = 0;
  pdeAV.fluxAdvective(paramL, x, time, q, fadv );
  BOOST_CHECK_CLOSE( fadv_true, fadv, tol );

  // Jacobian of advective flux
  MatrixQ fadv_u_true = uT*fw_Sw_true/phi;
  ArrayQ fadv_u = 0;
  pdeAV.jacobianFluxAdvective(paramL, x, time, q, fadv_u );
  BOOST_CHECK_CLOSE( fadv_u_true, fadv_u, tol );

  // Upwind advective flux (left to right)
  Real SwL = 0.3;
  Real SwR = 0.5;
  Real nx = 0.5;
  Real qLData[1] = {SwL};
  Real qRData[1] = {SwR};
  ArrayQ qL, qR;
  pdeAV.setDOFFrom( qL, qLData, qNamePrim, 1 );
  pdeAV.setDOFFrom( qR, qRData, qNamePrim, 1 );

  ArrayQ fupwind = 0, fadvL = 0, fadvR = 0;
  pdeAV.fluxAdvectiveUpwind(paramL, x, time, qL, qR, nx, fupwind);
  pdeAV.fluxAdvective(paramL, x, time, qL, fadvL );
  pdeAV.fluxAdvective(paramL, x, time, qR, fadvR );

  BOOST_CHECK_CLOSE( fadvL*nx, fupwind, tol );

  // Upwind advective flux (right to left)
  nx = -0.5;
  fupwind = 0;
  pdeAV.fluxAdvectiveUpwind(paramL, x, time, qL, qR, nx, fupwind);
  BOOST_CHECK_CLOSE( fadvR*nx, fupwind, tol );

  // Strong form advective flux
  strongPDE = 0;
  pdeAV.strongFluxAdvective(paramL, x, time, q, qx, strongPDE );
  BOOST_CHECK_CLOSE( fadv_u_true*qx, strongPDE, tol );

  // Characteristic speed
  Real lambda_true = fabs(fadv_u_true);
  Real lambda = 0;
  pdeAV.speedCharacteristic(paramL, x, time, q, lambda);
  BOOST_CHECK_CLOSE( lambda_true, lambda, tol );

  // Artificial viscosity with genH scale
  Real kxx_art_true = hxx/(Real(order)+1) * lambda_true * smoothabs0(paramL.right(), 1.0e-5);

  // Viscous diffusion matrix
  Real pc_Sw = -pc_max;
  Real kxx_true = -K*(Lw*Ln)/(Lw+Ln)*pc_Sw/phi + kxx_art_true;
  MatrixQ kxx = 0;
  pdeAV.diffusionViscous(paramL, x, time, q, qx, kxx );
  BOOST_CHECK_CLOSE( kxx_true, kxx, tol );

  // Viscous flux
  ArrayQ fvisc = 0;
  pdeAV.fluxViscous(paramL, x, time, q, qx, fvisc );
  BOOST_CHECK_CLOSE( -kxx_true*Swx, fvisc, tol );

#if 0 // Not implemented yet
  // Jacobian of viscous diffusion matrix
  pc_Sw = -pc_max;
  Real pc_Sw2 = 0;
  MatrixQ tmp = (Lw*Ln)/(Lw + Ln);
  MatrixQ tmp_Sw = ((Lw_Sw*Ln + Lw*Ln_Sw) - tmp*(Lw_Sw + Ln_Sw))/(Lw + Ln);
  MatrixQ kxx_u_true = -(K/phi)*(tmp_Sw*pc_Sw + tmp*pc_Sw2);
  MatrixQ kxx_u = 0;
  pdeAV.jacobianDiffusionViscous(paramL, x, time, q, kxx_u );
  BOOST_CHECK_CLOSE( kxx_u_true, kxx_u, tol );
#endif

  // Space-time artificial viscosity
  Real ktt_art_true = 0.0; //only spatial diffusion

  // Space-time viscous diffusion matrix
  Real ktt_true = ktt_art_true;
  kxx = 0;
  MatrixQ kxt = 0, ktx = 0, ktt = 0;
  pdeAV.diffusionViscousSpaceTime(paramL_ST, x, time, q, qx, qt, kxx, kxt, ktx, ktt );
  BOOST_CHECK_CLOSE( kxx_true, kxx, tol );
  BOOST_CHECK_CLOSE( 0.0, kxt, tol );
  BOOST_CHECK_CLOSE( 0.0, ktx, tol );
  BOOST_CHECK_CLOSE( ktt_true, ktt, tol );

  // Space-time viscous flux
  ArrayQ fvisc_x = 0;
  ArrayQ fvisc_t = 0;
  pdeAV.fluxViscousSpaceTime(paramL_ST, x, time, q, qx, qt, fvisc_x, fvisc_t );
  BOOST_CHECK_CLOSE( -(kxx_true*Swx + 0.0*Swt), fvisc_x, tol );
  BOOST_CHECK_CLOSE( -(0.0*Swx + ktt_true*Swt), fvisc_t, tol );

  //Forcing function
  ArrayQ forcing = 0;
  BOOST_CHECK_THROW( pdeAV.forcingFunction(paramL, x, time, forcing), DeveloperException);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux_SaturationWet_SpaceTimeDiffusion )
{
  typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEAVClass;
  typedef PDEAVClass::ArrayQ<Real> ArrayQ;
  typedef PDEAVClass::MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD1::D,Real> MatrixSym;
  typedef DLA::MatrixSymS<PhysD2::D,Real> MatrixSym_ST;
  typedef MakeTuple<ParamTuple, MatrixSym, Real>::type ParamType;
  typedef MakeTuple<ParamTuple, MatrixSym_ST, Real>::type ParamType_ST;

  const Real tol = 5e-13;

  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.4;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real pc_max = 5.0;
  CapillaryModel_Linear pc_model(pc_max);
  const Real K = 0.25;

  int order = 1;
  const bool hasSpaceTimeDiffusion = true;
  PDEAVClass pdeAV(order, hasSpaceTimeDiffusion, phi, uT, kr_model, mu_w, mu_n, K, pc_model);

  // Static tests
  BOOST_CHECK( pdeAV.D == 1 );
  BOOST_CHECK( pdeAV.N == 1 );

  // Flux components
  BOOST_CHECK( pdeAV.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pdeAV.hasFluxAdvective() == true );
  BOOST_CHECK( pdeAV.hasFluxViscous() == true );
  BOOST_CHECK( pdeAV.hasSource() == false );
  BOOST_CHECK( pdeAV.hasForcingFunction() == false );

  BOOST_CHECK( pdeAV.needsSolutionGradientforSource() == false );

  // Function tests

  Real x, time;
  Real Sw, Swx, Swt;
//  Real Swxx;
  Real krw, krn;
  Real Lw, Ln, Lw_Sw, Ln_Sw;
  Real fw_true, fw_Sw_true;

  x = time = 0; // not actually used in functions
  Sw = 0.375;
  Swx = -0.63;
  Swt = -0.27;
//  Swxx = 1.93;
  krw = pow(Sw,2);
  krn = pow(1-Sw,2);
  Lw = krw/mu_w;
  Ln = krn/mu_n;
  fw_true = (Lw)/(Lw + Ln);

  Lw_Sw = (2*Sw)/mu_w;
  Ln_Sw = -2*(1-Sw)/mu_n;
  fw_Sw_true = (Lw_Sw*Ln - Lw*Ln_Sw)/pow(Lw + Ln, 2);


  // Set
  Real qDataPrim[1] = {Sw};
  string qNamePrim[1] = {"Sw"};
  ArrayQ qTrue = Sw;
  ArrayQ q;
  pdeAV.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );
  ArrayQ qt = Swt;
  ArrayQ qx = Swx;
//  ArrayQ qxx = Swxx;

  Real hxx = 0.1, hxt = 0.04, htt = 0.2;
  MatrixSym logH = {log(hxx)};
  MatrixSym_ST H_ST = {{hxx},{hxt, htt}};
  MatrixSym_ST logH_ST = log(H_ST);

  ParamType paramL(logH, 0.2); // grid spacing and sensor values
  ParamType_ST paramL_ST(logH_ST, 0.2);

  // Conservative flux
  ArrayQ fcons_true = Sw;
  ArrayQ fcons = 0;
  pdeAV.masterState( paramL, x, time, q, fcons );
  BOOST_CHECK_CLOSE( fcons_true, fcons, tol );

  // Strong form conservative flux
  ArrayQ strongPDE = 0;
  pdeAV.strongFluxAdvectiveTime( paramL, x, time, q, qt, strongPDE );
  BOOST_CHECK_CLOSE( qt, strongPDE, tol );

  // Advective flux
  ArrayQ fadv_true = uT*fw_true/phi;
  ArrayQ fadv = 0;
  pdeAV.fluxAdvective(paramL, x, time, q, fadv );
  BOOST_CHECK_CLOSE( fadv_true, fadv, tol );

  // Jacobian of advective flux
  MatrixQ fadv_u_true = uT*fw_Sw_true/phi;
  ArrayQ fadv_u = 0;
  pdeAV.jacobianFluxAdvective(paramL, x, time, q, fadv_u );
  BOOST_CHECK_CLOSE( fadv_u_true, fadv_u, tol );

  // Upwind advective flux (left to right)
  Real SwL = 0.3;
  Real SwR = 0.5;
  Real nx = 0.5;
  Real qLData[1] = {SwL};
  Real qRData[1] = {SwR};
  ArrayQ qL, qR;
  pdeAV.setDOFFrom( qL, qLData, qNamePrim, 1 );
  pdeAV.setDOFFrom( qR, qRData, qNamePrim, 1 );

  ArrayQ fupwind = 0, fadvL = 0, fadvR = 0;
  pdeAV.fluxAdvectiveUpwind(paramL, x, time, qL, qR, nx, fupwind);
  pdeAV.fluxAdvective(paramL, x, time, qL, fadvL );
  pdeAV.fluxAdvective(paramL, x, time, qR, fadvR );

  BOOST_CHECK_CLOSE( fadvL*nx, fupwind, tol );

  // Upwind advective flux (right to left)
  nx = -0.5;
  fupwind = 0;
  pdeAV.fluxAdvectiveUpwind(paramL, x, time, qL, qR, nx, fupwind);
  BOOST_CHECK_CLOSE( fadvR*nx, fupwind, tol );

  // Strong form advective flux
  strongPDE = 0;
  pdeAV.strongFluxAdvective(paramL, x, time, q, qx, strongPDE );
  BOOST_CHECK_CLOSE( fadv_u_true*qx, strongPDE, tol );

  // Characteristic speed
  Real lambda_true = fabs(fadv_u_true);
  Real lambda = 0;
  pdeAV.speedCharacteristic(paramL, x, time, q, lambda);
  BOOST_CHECK_CLOSE( lambda_true, lambda, tol );

  // Artificial viscosity with genH scale
  Real kxx_art_true = hxx/(Real(order)+1) * lambda_true * smoothabs0(paramL.right(), 1.0e-5);

  // Viscous diffusion matrix
  Real pc_Sw = -pc_max;
  Real kxx_true = -K*(Lw*Ln)/(Lw+Ln)*pc_Sw/phi + kxx_art_true;
  MatrixQ kxx = 0;

  //Check for exception if we try to call spatial flux functions with hasSpaceTimeDiffusion = true
  BOOST_CHECK_THROW( pdeAV.diffusionViscous(paramL, x, time, q, qx, kxx ), AssertionException );

  // Viscous flux
  ArrayQ fvisc = 0;

  //Check for exception if we try to call spatial flux functions with hasSpaceTimeDiffusion = true
  BOOST_CHECK_THROW( pdeAV.fluxViscous(paramL, x, time, q, qx, fvisc ), AssertionException );

  // Space-time artificial viscosity
  Real kxt_art_true = hxt/(Real(order)+1) * sqrt(lambda_true) * smoothabs0(paramL_ST.right(), 1.0e-5);
  Real ktx_art_true = hxt/(Real(order)+1) * sqrt(lambda_true) * smoothabs0(paramL_ST.right(), 1.0e-5);
  Real ktt_art_true = htt/(Real(order)+1) * smoothabs0(paramL_ST.right(), 1.0e-5);

  // Space-time viscous diffusion matrix
  Real kxt_true = kxt_art_true;
  Real ktx_true = ktx_art_true;
  Real ktt_true = ktt_art_true;
  kxx = 0;
  MatrixQ kxt = 0, ktx = 0, ktt = 0;
  pdeAV.diffusionViscousSpaceTime(paramL_ST, x, time, q, qx, qt, kxx, kxt, ktx, ktt );
  BOOST_CHECK_CLOSE( kxx_true, kxx, tol );
  BOOST_CHECK_CLOSE( kxt_true, kxt, tol );
  BOOST_CHECK_CLOSE( ktx_true, ktx, tol );
  BOOST_CHECK_CLOSE( ktt_true, ktt, tol );

  // Space-time viscous flux
  ArrayQ fvisc_x = 0;
  ArrayQ fvisc_t = 0;
  pdeAV.fluxViscousSpaceTime(paramL_ST, x, time, q, qx, qt, fvisc_x, fvisc_t );
  BOOST_CHECK_CLOSE( -(kxx_true*Swx + kxt_true*Swt), fvisc_x, tol );
  BOOST_CHECK_CLOSE( -(ktx_true*Swx + ktt_true*Swt), fvisc_t, tol );

  //Forcing function
  ArrayQ forcing = 0;
  BOOST_CHECK_THROW( pdeAV.forcingFunction(paramL, x, time, forcing), DeveloperException);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux_SaturationWet_SpatialDiffusion_ExtendedState )
{
  typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverettArtificialViscosity, TraitsModelClass> PDEAVClass;
  typedef PDEAVClass::ArrayQ<Real> ArrayQ;
  typedef PDEAVClass::MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD1::D,Real> MatrixSym;
  typedef DLA::MatrixSymS<PhysD2::D,Real> MatrixSym_ST;
  typedef MatrixSym ParamType;
  typedef MatrixSym_ST ParamType_ST;

  const Real tol = 1.e-13;

  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.4;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real pc_max = 5.0;
  CapillaryModel_Linear pc_model(pc_max);
  const Real K = 0.25;

  int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEAVClass pdeAV(order, hasSpaceTimeDiffusion, phi, uT, kr_model, mu_w, mu_n, K, pc_model);

  // Static tests
  BOOST_CHECK( pdeAV.D == 1 );
  BOOST_CHECK( pdeAV.N == 2 );

  // Flux components
  BOOST_CHECK( pdeAV.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pdeAV.hasFluxAdvective() == true );
  BOOST_CHECK( pdeAV.hasFluxViscous() == true );
  BOOST_CHECK( pdeAV.hasSource() == false );
  BOOST_CHECK( pdeAV.hasForcingFunction() == false );

  BOOST_CHECK( pdeAV.needsSolutionGradientforSource() == false );

  // Function tests

  Real x, time;
  Real Sw, Swx, Swt;
  Real nu, nux, nut;
//  Real Swxx;
  Real krw, krn;
  Real Lw, Ln, Lw_Sw, Ln_Sw;
  Real fw_true, fw_Sw_true;

  x = time = 0; // not actually used in functions
  Sw = 0.375;
  Swx = -0.63;
  Swt = -0.27;

  nu = 0.1;
  nux = 0.45;
  nut = 0.08;

  krw = pow(Sw,2);
  krn = pow(1-Sw,2);
  Lw = krw/mu_w;
  Ln = krn/mu_n;
  fw_true = (Lw)/(Lw + Ln);

  Lw_Sw = (2*Sw)/mu_w;
  Ln_Sw = -2*(1-Sw)/mu_n;
  fw_Sw_true = (Lw_Sw*Ln - Lw*Ln_Sw)/pow(Lw + Ln, 2);


  // Set
  Real qDataPrim[2] = {Sw, nu};
  string qNamePrim[2] = {"Sw","nu"};
  ArrayQ qTrue = {Sw, nu};
  ArrayQ q;
  pdeAV.setDOFFrom( q, qDataPrim, qNamePrim, 2 );
  q(1) = nu;
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  ArrayQ qt = {Swt, nut};
  ArrayQ qx = {Swx, nux};
//  ArrayQ qxx = Swxx;

  Real hxx = 0.1, hxt = 0.04, htt = 0.2;
  MatrixSym logH = {log(hxx)};
  MatrixSym_ST H_ST = {{hxx},{hxt, htt}};
  MatrixSym_ST logH_ST = log(H_ST);

  ParamType paramL = logH;
  ParamType paramR = 0.5*logH;
  ParamType_ST paramL_ST = logH_ST;
  ParamType_ST paramR_ST = 0.5*logH_ST;

  // Conservative flux
  ArrayQ fcons_true = {Sw, 0.0};
  ArrayQ fcons = 0;
  pdeAV.masterState( paramL, x, time, q, fcons );
  BOOST_CHECK_CLOSE( fcons_true(0), fcons(0), tol );
  BOOST_CHECK_CLOSE( fcons_true(1), fcons(1), tol );


  // Strong form conservative flux
  ArrayQ strongPDE = 0;
  pdeAV.strongFluxAdvectiveTime( paramL, x, time, q, qt, strongPDE );
  BOOST_CHECK_CLOSE( qt(0), strongPDE(0), tol );
  BOOST_CHECK_CLOSE( 0.0, strongPDE(1), tol );

  // Advective flux
  ArrayQ fadv_true = {uT*fw_true/phi, 0.0};
  ArrayQ fadv = 0;
  pdeAV.fluxAdvective(paramL, x, time, q, fadv );
  BOOST_CHECK_CLOSE( fadv_true(0), fadv(0), tol );
  BOOST_CHECK_CLOSE( fadv_true(1), fadv(1), tol );

  // Jacobian of advective flux
  MatrixQ fadv_u_true = {{uT*fw_Sw_true/phi, 0}, {0, 0}};
  MatrixQ fadv_u = 0;
  pdeAV.jacobianFluxAdvective(paramL, x, time, q, fadv_u );
  BOOST_CHECK_CLOSE( fadv_u_true(0,0), fadv_u(0,0), tol );
  BOOST_CHECK_CLOSE( fadv_u_true(0,1), fadv_u(0,1), tol );
  BOOST_CHECK_CLOSE( fadv_u_true(1,0), fadv_u(1,0), tol );
  BOOST_CHECK_CLOSE( fadv_u_true(1,1), fadv_u(1,1), tol );


  // Upwind advective flux (left to right)
  Real SwL = 0.3;
  Real SwR = 0.5;
  Real nuL = 0.1, nuR = 0.23;
  Real nx = 0.5;
  Real qLData[2] = {SwL, nuL};
  Real qRData[2] = {SwR, nuR};
  ArrayQ qL, qR;
  pdeAV.setDOFFrom( qL, qLData, qNamePrim, 2 );
  pdeAV.setDOFFrom( qR, qRData, qNamePrim, 2 );
  qL(1) = nuL;
  qR(1) = nuR;

  ArrayQ fupwind = 0, fadvL = 0, fadvR = 0;
  pdeAV.fluxAdvectiveUpwind(paramL, x, time, qL, qR, nx, fupwind);
  pdeAV.fluxAdvective(paramL, x, time, qL, fadvL );
  pdeAV.fluxAdvective(paramL, x, time, qR, fadvR );

  BOOST_CHECK_CLOSE( fadvL(0)*nx, fupwind(0), tol );
  BOOST_CHECK_CLOSE( fadvL(1)*nx, fupwind(1), tol );

  // Upwind advective flux (right to left)
  nx = -0.5;
  fupwind = 0;
  pdeAV.fluxAdvectiveUpwind(paramL, x, time, qL, qR, nx, fupwind);
  BOOST_CHECK_CLOSE( fadvR(0)*nx, fupwind(0), tol );
  BOOST_CHECK_CLOSE( fadvR(1)*nx, fupwind(1), tol );


  // Strong form advective flux
  strongPDE = 0;
  pdeAV.strongFluxAdvective(paramL, x, time, q, qx, strongPDE );
  BOOST_CHECK_CLOSE( fadv_u_true(0,0)*qx(0), strongPDE(0), tol );
  BOOST_CHECK_CLOSE( 0.0, strongPDE(1), tol );

  // Characteristic speed
  Real lambda_true = fabs(fadv_u_true(0,0));
  Real lambda = 0;
  pdeAV.speedCharacteristic(paramL, x, time, q, lambda);
  BOOST_CHECK_CLOSE( lambda_true, lambda, tol );

  // Viscous diffusion matrix
  Real pc_Sw = -pc_max;
  Real kxx_true = -K*(Lw*Ln)/(Lw+Ln)*pc_Sw/phi;

  Real nu_mod = smoothmax(nu, 0.0, 40.0);
  ArrayQ fviscxTrue = {-(kxx_true + nu_mod)*Swx, 0};

  // Viscous flux
  ArrayQ fviscx = 0;
  pdeAV.fluxViscous(paramL, x, time, q, qx, fviscx);
  BOOST_CHECK_CLOSE( fviscxTrue(0), fviscx(0), tol );
  BOOST_CHECK_CLOSE( fviscxTrue(1), fviscx(1), tol );

  //Space-time viscous flux
  fviscx = 0;
  ArrayQ fvisct = 0;
  pdeAV.fluxViscousSpaceTime(paramL_ST, x, time, q, qx, qt, fviscx, fvisct );
  BOOST_CHECK_CLOSE( fviscxTrue(0), fviscx(0), tol );
  BOOST_CHECK_CLOSE( fviscxTrue(1), fviscx(1), tol );
  BOOST_CHECK_CLOSE( 0.0, fvisct(0), tol );
  BOOST_CHECK_CLOSE( 0.0, fvisct(1), tol );

  ArrayQ qxL = {0.17, 0.63};
  ArrayQ qxR = {-0.21, 0.91};
  ArrayQ qtL = {0.08, 0.43};
  ArrayQ qtR = {0.19, 0.51};

  nx = 0.157;
  Real nt = -0.713;

  ArrayQ fviscxL = 0, fviscxR = 0;
  pdeAV.fluxViscous(paramL, x, time, qL, qxL, fviscxL);
  pdeAV.fluxViscous(paramR, x, time, qR, qxR, fviscxR);
  fviscxTrue = 0.5*(fviscxL + fviscxR);
  ArrayQ fnTrue = fviscxTrue*nx;

  ArrayQ fn = 0;
  pdeAV.fluxViscous(paramL, paramR, x, time, qL, qxL, qR, qxR, nx, fn);
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );

  fn = 0;
  pdeAV.fluxViscousSpaceTime(paramL_ST, paramR_ST, x, time, qL, qxL, qtL, qR, qxR, qtR, nx, nt, fn);
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );

  MatrixSym_ST H_ST_sq = H_ST*H_ST;
  Real v_H = lambda_true*sqrt(H_ST_sq(0,0));
  Real Pe = 2.0;
  Real artviscMaxTrue = (v_H / order) / Pe;

  Real artviscMax = 0;
  pdeAV.artViscMax(paramL_ST, x, time, q, qx, artviscMax);
  BOOST_CHECK_CLOSE( artviscMaxTrue, artviscMax, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState_SaturationWet )
{
  typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEAVClass;
  typedef PDEAVClass::ArrayQ<Real> ArrayQ;

  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.4;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real pc_max = 5.0;
  CapillaryModel_Linear pc_model(pc_max);
  const Real K = 0.25;

  const int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEAVClass pdeAV(order, hasSpaceTimeDiffusion, phi, uT, kr_model, mu_w, mu_n, K, pc_model);

  ArrayQ q = 0.0;
  BOOST_CHECK( pdeAV.isValidState(q) == true );

  q = 0.35;
  BOOST_CHECK( pdeAV.isValidState(q) == true );

  q = 0.8;
  BOOST_CHECK( pdeAV.isValidState(q) == true );

  q = 1.0;
  BOOST_CHECK( pdeAV.isValidState(q) == true );

//  q = -0.1;
//  BOOST_CHECK( pde.isValidState(q) == false );
//
//  q = 1.05;
//  BOOST_CHECK( pde.isValidState(q) == false );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( residInterp )
{
  typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverettArtificialViscosity, TraitsModelClass> PDEAVClass;
  typedef PDEAVClass::ArrayQ<Real> ArrayQ;

  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.4;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real pc_max = 5.0;
  CapillaryModel_Linear pc_model(pc_max);
  const Real K = 0.25;

  const int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEAVClass pdeAV(order, hasSpaceTimeDiffusion, phi, uT, kr_model, mu_w, mu_n, K, pc_model);

  ArrayQ rsdIn = {0.23, 0.56};
  DLA::VectorD<Real> rsdOut = {0.0, 0.0};
  pdeAV.interpResidVariable(rsdIn, rsdOut);
  BOOST_CHECK_EQUAL( rsdOut[0], rsdIn[0] );
  BOOST_CHECK_EQUAL( rsdOut[1], rsdIn[1] );

  rsdIn = {0.32, -0.83};
  rsdOut = 0.0;
  pdeAV.interpResidGradient(rsdIn, rsdOut);
  BOOST_CHECK_EQUAL( rsdOut[0], rsdIn[0] );
  BOOST_CHECK_EQUAL( rsdOut[1], rsdIn[1] );

  rsdIn = {-0.82, 0.54};
  rsdOut = 0.0;
  pdeAV.interpResidBC(rsdIn, rsdOut);
  BOOST_CHECK_EQUAL( rsdOut[0], rsdIn[0] );
  BOOST_CHECK_EQUAL( rsdOut[1], rsdIn[1] );

  BOOST_CHECK_EQUAL( pdeAV.nMonitor(), 2 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/PDEBuckleyLeverett_ArtificialViscosity1D_pattern.txt", true );

  typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEAVClass;

  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.4;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real pc_max = 5.0;
  const Real K = 0.25;
  CapillaryModel_Linear pc_linear(pc_max);

  const int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEAVClass pdeAV(order, hasSpaceTimeDiffusion, phi, uT, kr_model, mu_w, mu_n, K, pc_linear);
  pdeAV.dump(2, output);

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
