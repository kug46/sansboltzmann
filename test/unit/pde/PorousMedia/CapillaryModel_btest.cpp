// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// CapillaryModel_btest
// testing of CapillaryModel classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/PorousMedia/CapillaryModel.h"


using namespace std;
using namespace SANS;

namespace SANS
{

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( CapillaryModel_None_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Constructor )
{
  const Real tol = 1.e-13;

  Real dummy_x = 0.0, dummy_y = 0.0, dummy_time = 0.0;
  Real Sn = 0.375;

  // PyDict constructor
  CapillaryModel_None pc_model;
  BOOST_CHECK_CLOSE( pc_model.capPressure(dummy_x, dummy_time, Sn), 0.0, tol );
  BOOST_CHECK_CLOSE( pc_model.capPressure(dummy_x, dummy_y, dummy_time, Sn), 0.0, tol );

  // copy constructor
  CapillaryModel_None pc_model2(pc_model);
  BOOST_CHECK_CLOSE( pc_model2.capPressure(dummy_x, dummy_time, Sn), 0.0, tol);
  BOOST_CHECK_CLOSE( pc_model2.capPressure(dummy_x, dummy_y, dummy_time, Sn), 0.0, tol);

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Evaluations )
{
//  const Real tol = 1.e-13;

  Real dummy_x = 0.0, dummy_y = 0.0, dummy_time = 0.0;
  Real Sn = 0.375;
  Real Snx = -0.75;
  Real Sny =  0.41;

  // PyDict constructor
  CapillaryModel_None pc_model;

  Real pc_Sn, pc_Sn2, pcx, pcy;

  //1D
  pc_model.capPressureJacobian(dummy_x, dummy_time, Sn, pc_Sn);
  pc_model.capPressureHessian(dummy_x, dummy_time, Sn, pc_Sn2);
  pc_model.capPressureGradient(dummy_x, dummy_time, Sn, Snx, pcx);
  BOOST_CHECK_EQUAL( pc_Sn, 0.0 );
  BOOST_CHECK_EQUAL( pc_Sn2, 0.0 );
  BOOST_CHECK_EQUAL( pcx, 0.0 );

  //2D
  pc_model.capPressureJacobian(dummy_x, dummy_y, dummy_time, Sn, pc_Sn);
  pc_model.capPressureHessian(dummy_x, dummy_y, dummy_time, Sn, pc_Sn2);
  pc_model.capPressureGradient(dummy_x, dummy_y, dummy_time, Sn, Snx, Sny, pcx, pcy);
  BOOST_CHECK_EQUAL( pc_Sn, 0.0 );
  BOOST_CHECK_EQUAL( pc_Sn2, 0.0 );
  BOOST_CHECK_EQUAL( pcx, 0.0 );
  BOOST_CHECK_EQUAL( pcy, 0.0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/CapillaryModel_None_pattern.txt", true );

  // PyDict constructor
  CapillaryModel_None pc_model;

  pc_model.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()


//############################################################################//
BOOST_AUTO_TEST_SUITE( CapillaryModel_Linear_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Constructor )
{
  const Real tol = 1.e-13;
  Real pc_max = 4.9;

  PyDict d;
  d[CapillaryModel_Linear_Params::params.pc_max] = pc_max;

  //Check dictionary
  CapillaryModel_Linear_Params::checkInputs(d);

  // PyDict constructor
  CapillaryModel_Linear pc_model(d);
  BOOST_CHECK_CLOSE( pc_model.pc_max(), pc_max, tol );
  BOOST_CHECK_EQUAL( pc_model.hasFluxViscous(), true);

  // copy constructor
  CapillaryModel_Linear pc_model2(pc_model);
  BOOST_CHECK_CLOSE( pc_model2.pc_max(), pc_max, tol );
  BOOST_CHECK_EQUAL( pc_model2.hasFluxViscous(), true);

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Evaluations )
{
  const Real tol = 1.e-13;
  Real pc_max = 4.9;

  PyDict d;
  d[CapillaryModel_Linear_Params::params.pc_max] = pc_max;

  //Check dictionary
  CapillaryModel_Linear_Params::checkInputs(d);

  // PyDict constructor
  CapillaryModel_Linear pc_model(d);

  Real dummy_x = 0.0, dummy_y = 0.0, dummy_time = 0.0;
  Real Sn = 0.375;
  Real Snx = -0.75;
  Real Sny =  0.41;

  Real pc_true = pc_max*Sn;
  Real pc_Sn_true = pc_max;
  Real pcx_true = pc_Sn_true*Snx;
  Real pcy_true = pc_Sn_true*Sny;

  Real pc, pc_Sn, pc_Sn2, pcx, pcy;

  //1D
  pc = pc_model.capPressure(dummy_x, dummy_time, Sn);
  pc_model.capPressureJacobian(dummy_x, dummy_time, Sn, pc_Sn);
  pc_model.capPressureHessian(dummy_x, dummy_time, Sn, pc_Sn2);
  pc_model.capPressureGradient(dummy_x, dummy_time, Sn, Snx, pcx);
  BOOST_CHECK_CLOSE( pc    , pc_true   , tol );
  BOOST_CHECK_CLOSE( pc_Sn , pc_Sn_true, tol );
  BOOST_CHECK_EQUAL( pc_Sn2, 0.0 );
  BOOST_CHECK_CLOSE( pcx   , pcx_true  , tol );

  //2D
  pc = pc_model.capPressure(dummy_x, dummy_y, dummy_time, Sn);
  pc_model.capPressureJacobian(dummy_x, dummy_y, dummy_time, Sn, pc_Sn);
  pc_model.capPressureHessian(dummy_x, dummy_y, dummy_time, Sn, pc_Sn2);
  pc_model.capPressureGradient(dummy_x, dummy_y, dummy_time, Sn, Snx, Sny, pcx, pcy);
  BOOST_CHECK_CLOSE( pc    , pc_true   , tol );
  BOOST_CHECK_CLOSE( pc_Sn , pc_Sn_true, tol );
  BOOST_CHECK_EQUAL( pc_Sn2, 0.0 );
  BOOST_CHECK_CLOSE( pcx   , pcx_true  , tol );
  BOOST_CHECK_CLOSE( pcy   , pcy_true  , tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/CapillaryModel_Linear_pattern.txt", true );

  Real pc_max = 4.9;

  PyDict d;
  d[CapillaryModel_Linear_Params::params.pc_max] = pc_max;

  // PyDict constructor
  CapillaryModel_Linear pc_model(d);

  pc_model.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
