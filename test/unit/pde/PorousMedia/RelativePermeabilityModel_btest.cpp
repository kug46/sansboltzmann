// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// RelativePermeabilityModel_btest
// testing of RelativePermeabilityModel classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/PorousMedia/RelPermModel_PowerLaw.h"

using namespace std;
using namespace SANS;

namespace SANS
{

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( RelPermModel_PowerLaw_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Constructor )
{
//  const Real tol = 1.e-13;
  int power = 2;

  PyDict d;
  d[RelPermModel_PowerLaw_Params::params.power] = power;

  //Check dictionary
  RelPermModel_PowerLaw_Params::checkInputs(d);

  // PyDict constructor
  RelPermModel_PowerLaw kr_model(d);
  BOOST_CHECK_EQUAL( power, kr_model.power());

  // copy constructor
  RelPermModel_PowerLaw kr_model2(kr_model);
  BOOST_CHECK_EQUAL( power, kr_model2.power());
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Jacobian )
{
  const Real tol = 1.e-13;

  PyDict d1, d2;
  d1[RelPermModel_PowerLaw_Params::params.power] = 2.0;
  d2[RelPermModel_PowerLaw_Params::params.power] = 4.0;

  // Check dictionaries
  RelPermModel_PowerLaw_Params::checkInputs(d1);
  RelPermModel_PowerLaw_Params::checkInputs(d2);

  // PyDict constructor
  RelPermModel_PowerLaw kr_model_quadratic(d1);
  RelPermModel_PowerLaw kr_model_quartic(d2);

  Real Sw = 0.378;          // saturation

  Real krw2 = pow(Sw,2);  //krw for quadratic
  Real krw4 = pow(Sw,4);  //krw for quartic

  Real kr2 = kr_model_quadratic.relativeperm(Sw);
  Real kr4 = kr_model_quartic  .relativeperm(Sw);

  BOOST_CHECK_CLOSE( krw2, kr2, tol );
  BOOST_CHECK_CLOSE( krw4, kr4, tol );

  Real krw2_Sw =  2*Sw;
  Real krw4_Sw =  4*pow(  Sw,3);

  Real kr2_Sw, kr4_Sw;
  kr_model_quadratic.relativepermJacobian(Sw, kr2_Sw);
  kr_model_quartic  .relativepermJacobian(Sw, kr4_Sw);

  BOOST_CHECK_CLOSE( krw2_Sw, kr2_Sw, tol );
  BOOST_CHECK_CLOSE( krw4_Sw, kr4_Sw, tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/RelPermModel_PowerLaw_pattern.txt", true );

  PyDict d1, d2;
  d1[RelPermModel_PowerLaw_Params::params.power] = 2.0;
  d2[RelPermModel_PowerLaw_Params::params.power] = 4.0;

  // Check dictionaries
  RelPermModel_PowerLaw_Params::checkInputs(d1);
  RelPermModel_PowerLaw_Params::checkInputs(d2);

  // PyDict constructor
  RelPermModel_PowerLaw kr_model_quadratic(d1);
  RelPermModel_PowerLaw kr_model_quartic(d2);

  kr_model_quadratic.dump( 2, output );
  kr_model_quartic  .dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
