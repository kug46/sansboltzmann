// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCTwoPhaseArtificialViscosity1D_btest
//
// test of TwoPhase AV BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "pde/PorousMedia/Q1DPrimitive_pnSw.h"
#include "pde/PorousMedia/BCTwoPhaseArtificialViscosity1D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsTwoPhaseArtificialViscosity.h"

#include "pde/ArtificialViscosity/AVVariable.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_Source1D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor1D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_Comp,
                            RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                            Real, CapillaryModel_Linear> TraitsModel_TwoPhase;

typedef PDETwoPhase_ArtificialViscosity1D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModel_TwoPhase> PDEBaseClass;

typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeTwoPhaseArtificialViscosity> SensorAdvectiveFlux;
typedef AVSensor_ViscousFlux1D_GenHScale<TraitsSizeTwoPhaseArtificialViscosity> SensorViscousFlux;
typedef AVSensor_Source1D_TwoPhase<PDEBaseClass> SensorSource;

typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
typedef PDEmitAVSensor1D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelAV> PDEClass;

template class BCmitAVSensor1D<BCTypeFlux_mitStateSpaceTime,
                               BCTwoPhase1D< BCTypeTimeIC, PDEClass >>;
template class BCmitAVSensor1D<BCTypeFlux_mitStateSpaceTime,
                               BCTwoPhase1D< BCTypeTimeIC_Function, PDEClass >>;
template class BCmitAVSensor1D<BCTypeFlux_mitState,
                               BCTwoPhase1D< BCTypeFullState, PDEClass >>;
template class BCmitAVSensor1D<BCTypeFlux_mitStateSpaceTime,
                               BCTwoPhase1D< BCTypeFullStateSpaceTime, PDEClass >>;

// Instantiate BCParameters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters< BCTwoPhaseArtificialViscosity1DVector<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelAV> >;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BCTwoPhaseArtificialViscosity1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDETwoPhase_ArtificialViscosity1D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModel_TwoPhase> PDEClass;

  {
    typedef BCNone<PhysD1,TraitsSizeTwoPhaseArtificialViscosity<PhysD1>::N> BCClass;
    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::NBC == 0 );
  }

  {
    typedef BCmitAVSensor1D< BCTypeFlux_mitStateSpaceTime, BCTwoPhase1D< BCTypeTimeIC, PDEClass>> BCClass;
    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::N == 3 );
    BOOST_CHECK( BCClass::NBC == 3 );
  }

  {
    typedef BCmitAVSensor1D< BCTypeFlux_mitStateSpaceTime, BCTwoPhase1D< BCTypeTimeIC_Function, PDEClass>> BCClass;
    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::N == 3 );
    BOOST_CHECK( BCClass::NBC == 3 );
  }

  {
    typedef BCmitAVSensor1D< BCTypeFlux_mitState, BCTwoPhase1D< BCTypeFullState, PDEClass>> BCClass;
    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::N == 3 );
    BOOST_CHECK( BCClass::NBC == 3 );
  }

  {
    typedef BCmitAVSensor1D< BCTypeFlux_mitStateSpaceTime, BCTwoPhase1D< BCTypeFullStateSpaceTime, PDEClass>> BCClass;
    BOOST_CHECK( BCClass::D == 1 );
    BOOST_CHECK( BCClass::N == 3 );
    BOOST_CHECK( BCClass::NBC == 3 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef Real RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel,
                              RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase_ArtificialViscosity1D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelClass> PDEClass;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  Real K = 1.1;

  CapillaryModel pc(5);

  const int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEClass pdeAV(order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCParameters< BCTwoPhaseArtificialViscosity1DVector<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelAV> > BCParams;

  PyDict BCList;

  {
    typedef BCNone<PhysD1,TraitsSizeTwoPhaseArtificialViscosity<PhysD1>::N> BCClass;

    //Pydict constructor
    PyDict BCNone;
    BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;
    BCClass bc0(pdeAV, BCNone);

    BCList["BC0"] = BCNone;
  }

  {
    typedef BCmitAVSensor1D<BCTypeFlux_mitStateSpaceTime, BCTwoPhase1D< BCTypeTimeIC, PDEClass>> BCClass;

    PressureNonWet_SaturationWet<Real> qdata(2534.5, 0.738);
    Real Cdiff = 3.0;
    BCClass bc0(Cdiff, pdeAV, qdata);

    PyDict d;
    d[TwoPhaseVariableTypeParams::params.StateVector.Variables] = TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
    d[PressureNonWet_SaturationWet_Params::params.pn] = 2534.5;
    d[PressureNonWet_SaturationWet_Params::params.Sw] = 0.738;

    PyDict BCTimeIC;
    BCTimeIC[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC;
    BCTimeIC[BCClass::ParamsType::params.StateVector] = d;
    BCTimeIC[BCClass::ParamsType::params.Cdiff] = Cdiff;
    BCClass bc1(pdeAV, BCTimeIC);

    BCList["BC1"] = BCTimeIC;
  }

  {
    typedef BCmitAVSensor1D<BCTypeFlux_mitStateSpaceTime, BCTwoPhase1D< BCTypeTimeIC_Function, PDEClass>> BCClass;
    typedef Q1D<QTypePrimitive_pnSw, CapillaryModel, TraitsSizeTwoPhase> QInterpreter;
    typedef SolutionFunction_TwoPhase1D_3ConstSaturation<QInterpreter, TraitsSizeTwoPhase> SolutionType;

    Real Cdiff = 3.0;

    PyDict initSoln;
    initSoln[BCClass::ParamsType::params.Function.Name] = BCClass::ParamsType::params.Function.ThreeConstSaturation;
    initSoln[SolutionType::ParamsType::params.pn] = 2500.0;
    initSoln[SolutionType::ParamsType::params.Sw_left] = 1.0;
    initSoln[SolutionType::ParamsType::params.Sw_mid] = 0.1;
    initSoln[SolutionType::ParamsType::params.Sw_right] = 1.0;
    initSoln[SolutionType::ParamsType::params.xLeftMid] = 100.0;
    initSoln[SolutionType::ParamsType::params.xMidRight] = 200.0;

    PyDict BCTimeIC_Function;
    BCTimeIC_Function[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC_Function;
    BCTimeIC_Function[BCClass::ParamsType::params.Function] = initSoln;
    BCTimeIC_Function[BCClass::ParamsType::params.Cdiff] = Cdiff;
    BCClass bc1(pdeAV, BCTimeIC_Function);

    BCList["BC2"] = BCTimeIC_Function;
  }

  {
    typedef BCmitAVSensor1D<BCTypeFlux_mitState, BCTwoPhase1D< BCTypeFullState, PDEClass>> BCClass;

    PressureNonWet_SaturationWet<Real> qdata(2534.5, 0.738);
    Real Cdiff = 3.0;
    BCClass bc0(Cdiff, pdeAV, qdata);

    PyDict d;
    d[TwoPhaseVariableTypeParams::params.StateVector.Variables] = TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
    d[PressureNonWet_SaturationWet_Params::params.pn] = 2534.5;
    d[PressureNonWet_SaturationWet_Params::params.Sw] = 0.738;

    PyDict BCFullState;
    BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
    BCFullState[BCClass::ParamsType::params.StateVector] = d;
    BCFullState[BCClass::ParamsType::params.Cdiff] = Cdiff;
    BCClass bc1(pdeAV, BCFullState);

    BCList["BC3"] = BCFullState;
  }

  {
    typedef BCmitAVSensor1D<BCTypeFlux_mitStateSpaceTime, BCTwoPhase1D< BCTypeFullStateSpaceTime, PDEClass>> BCClass;

    PressureNonWet_SaturationWet<Real> qdata(2534.5, 0.738);
    Real Cdiff = 3.0;
    BCClass bc0(Cdiff, pdeAV, qdata);

    PyDict d;
    d[TwoPhaseVariableTypeParams::params.StateVector.Variables] = TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
    d[PressureNonWet_SaturationWet_Params::params.pn] = 2534.5;
    d[PressureNonWet_SaturationWet_Params::params.Sw] = 0.738;

    PyDict BCFullState;
    BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
    BCFullState[BCClass::ParamsType::params.StateVector] = d;
    BCFullState[BCClass::ParamsType::params.Cdiff] = Cdiff;
    BCClass bc1(pdeAV, BCFullState);

    BCList["BC4"] = BCFullState;
  }

  //No exceptions should be thrown
  BCParams::checkInputs(BCList);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCNone_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef Real RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel,
                              RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase_ArtificialViscosity1D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelClass> PDEClass;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  Real K = 1.1;

  CapillaryModel pc(5);

  const int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEClass pdeAV(order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCNone<PhysD1,TraitsSizeTwoPhaseArtificialViscosity<PhysD1>::N> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  BCClass bc;
  ArrayQ qI = {2500, 0.8, 0.1};
  Real nx = 2;
  BOOST_CHECK(bc.isValidState(nx, qI) == true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeTimeIC_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef Real RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel,
                              RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase_ArtificialViscosity1D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelClass> PDEClass;

  typedef DLA::MatrixSymS<2, Real> ParamType;

  const Real tol = 1.0e-12;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 1e-5, pref);
  DensityModel rhon(52.1, 1e-5, pref);

  PorosityModel phi(0.3, 1e-5, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  Real K = 1.1;

  CapillaryModel pc(5);

  const int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEClass pdeAV(order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCmitAVSensor1D<BCTypeFlux_mitStateSpaceTime, BCTwoPhase1D< BCTypeTimeIC, PDEClass>> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  Real pnB = 2534.5;
  Real SwB = 0.738;
  PressureNonWet_SaturationWet<Real> qdata(pnB, SwB);
  Real Cdiff = 3.0;
  BCClass bc(Cdiff, pdeAV, qdata);

  Real x = 0;
  Real time = 0;
  Real nx = 0.8, nt = 0.3;

  ParamType H = {{0.1},{0.05, 0.3}}; // grid spacing
  ParamType param = log(H);

  ArrayQ qI = {2500, 0.8, 0.1};
  ArrayQ qIx = {1.35, -0.05, 0.23};
  ArrayQ qIt = {-0.21, 0.85,-0.41};
  ArrayQ qB = 0;
  bc.state(param, x, time, nx, nt, qI, qB);

  BOOST_CHECK_CLOSE( pnB, qB[0], tol );
  BOOST_CHECK_CLOSE( SwB, qB[1], tol );
  BOOST_CHECK_CLOSE( qI[2], qB[2], tol );

  ArrayQ uB = 0;
  pdeAV.masterState(param, x, time, qB, uB);
  ArrayQ Fn_true = uB*nt;

  ArrayQ fvx = 0, fvt = 0;
  pdeAV.fluxViscousSpaceTime(param, x, time, qB, qIx, qIt, fvx, fvt);
  Fn_true += fvx*nx + fvt*nt;

  DLA::VectorS<2, Real> nvec = {nx, nt};
  Real hn = dot(nvec, H*nvec);
  Fn_true[2] = sqrt(Cdiff)*hn*(qI(2) - 0.0);

  ArrayQ Fn = 0.1;
  bc.fluxNormalSpaceTime(param, x, time, nx, nt, qI, qIx, qIt, qB, Fn);
  BOOST_CHECK_CLOSE( Fn_true[0] + 0.1, Fn[0], tol );
  BOOST_CHECK_CLOSE( Fn_true[1] + 0.1, Fn[1], tol );
  BOOST_CHECK_CLOSE( Fn_true[2], Fn[2], tol );

  BOOST_CHECK(bc.isValidState(nx, nt, qI) == true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeTimeIC_Function_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef Real RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel,
                              RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase_ArtificialViscosity1D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelClass> PDEClass;

  typedef Q1D<QTypePrimitive_pnSw, CapillaryModel, TraitsSizeTwoPhaseArtificialViscosity> QInterpreter;
  typedef SolutionFunction_TwoPhase1D_3ConstSaturation<QInterpreter, TraitsSizeTwoPhaseArtificialViscosity> SolutionType;

  typedef DLA::MatrixSymS<2, Real> ParamType;

  const Real tol = 1.0e-12;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 1e-5, pref);
  DensityModel rhon(52.1, 1e-5, pref);

  PorosityModel phi(0.3, 1e-5, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  Real K = 1.1;

  CapillaryModel pc(5);

  const int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEClass pdeAV(order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCmitAVSensor1D<BCTypeFlux_mitStateSpaceTime, BCTwoPhase1D< BCTypeTimeIC_Function, PDEClass>> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  const Real xLeftMid = 1.0;
  const Real xMidRight = 2.0;
  const Real pnInit = 2500.0;
  const Real SwLeft = 0.1, SwMid = 0.6, SwRight = 1.0;

  std::shared_ptr<Function1DBase<ArrayQ>> functionPtr(new SolutionType(pnInit, SwLeft, SwMid, SwRight,
                                                                       xLeftMid, xMidRight, pdeAV.variableInterpreter()));

  Real Cdiff = 3.0;
  BCClass bc(Cdiff, pdeAV, functionPtr);

  Real x = 0;
  Real time = 0;
  Real nx = 0.8, nt = 0.3;

  ParamType H = {{0.1},{0.05, 0.3}}; // grid spacing
  ParamType param = log(H);

  ArrayQ qI = {2500, 0.8, 0.1};
  ArrayQ qIx = {1.35, -0.05, 0.23};
  ArrayQ qIt = {-0.21, 0.85,-0.41};
  ArrayQ qB = 0;
  bc.state(param, x, time, nx, nt, qI, qB);

  BOOST_CHECK_CLOSE( pnInit, qB[0], tol );
  BOOST_CHECK_CLOSE( SwLeft, qB[1], tol );
  BOOST_CHECK_CLOSE(  qI[2], qB[2], tol );

  ArrayQ uB = 0;
  pdeAV.masterState(param, x, time, qB, uB);
  ArrayQ Fn_true = uB*nt;

  ArrayQ fvx = 0, fvt = 0;
  pdeAV.fluxViscousSpaceTime(param, x, time, qB, qIx, qIt, fvx, fvt);
  Fn_true += fvx*nx + fvt*nt;

  DLA::VectorS<2, Real> nvec = {nx, nt};
  Real hn = dot(nvec, H*nvec);
  Fn_true[2] = sqrt(Cdiff)*hn*(qI(2) - 0.0);

  ArrayQ Fn = 0.1;
  bc.fluxNormalSpaceTime(param, x, time, nx, nt, qI, qIx, qIt, qB, Fn);
  BOOST_CHECK_CLOSE( Fn_true[0] + 0.1, Fn[0], tol );
  BOOST_CHECK_CLOSE( Fn_true[1] + 0.1, Fn[1], tol );
  BOOST_CHECK_CLOSE( Fn_true[2], Fn[2], tol );

  BOOST_CHECK(bc.isValidState(nx, nt, qI) == true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFullState_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef Real RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel,
                              RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase_ArtificialViscosity1D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelClass> PDEClass;

  typedef DLA::MatrixSymS<1, Real> ParamType;

  const Real tol = 1.0e-12;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 1e-5, pref);
  DensityModel rhon(52.1, 1e-5, pref);

  PorosityModel phi(0.3, 1e-5, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  Real K = 1.1;

  CapillaryModel pc(5);

  const int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEClass pdeAV(order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCmitAVSensor1D<BCTypeFlux_mitState, BCTwoPhase1D< BCTypeFullState, PDEClass>> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  Real pnB = 2534.5;
  Real SwB = 0.738;
  PressureNonWet_SaturationWet<Real> qdata(pnB, SwB);
  Real Cdiff = 3.0;
  BCClass bc(Cdiff, pdeAV, qdata);

  Real x = 0;
  Real time = 0;
  Real nx = 0.8;

  ParamType H = {{0.1}}; // grid spacing
  ParamType param = log(H);

  ArrayQ qI = {2500, 0.8, 0.1};
  ArrayQ qIx = {1.35, -0.05, 0.23};
  ArrayQ qB = 0;
  bc.state(param, x, time, nx, qI, qB);

  BOOST_CHECK_CLOSE( pnB, qB[0], tol );
  BOOST_CHECK_CLOSE( SwB, qB[1], tol );
  BOOST_CHECK_CLOSE( qI[2], qB[2], tol );

  ArrayQ Fn_true = 0;
  pdeAV.fluxAdvectiveUpwind(param, x, time, qI, qB, nx, Fn_true);

  ArrayQ fx_true = 0;
  pdeAV.fluxViscous(param, x, time, qB, qIx, fx_true);
  Fn_true += fx_true*nx;

  DLA::VectorS<1, Real> nvec = {nx};
  Real hn = dot(nvec, H*nvec);
  Fn_true[2] = sqrt(Cdiff)*hn*(qI(2) - 0.0);

  ArrayQ Fn = 0.1;
  bc.fluxNormal(param, x, time, nx, qI, qIx, qB, Fn);
  BOOST_CHECK_CLOSE( Fn_true[0] + 0.1, Fn[0], tol );
  BOOST_CHECK_CLOSE( Fn_true[1] + 0.1, Fn[1], tol );
  BOOST_CHECK_CLOSE( Fn_true[2], Fn[2], tol );

  BOOST_CHECK(bc.isValidState(nx, qI) == true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFullStateSpaceTime_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef Real RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel,
                              RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase_ArtificialViscosity1D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelClass> PDEClass;

  typedef DLA::MatrixSymS<2, Real> ParamType;

  const Real tol = 1.0e-12;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 1e-5, pref);
  DensityModel rhon(52.1, 1e-5, pref);

  PorosityModel phi(0.3, 1e-5, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  Real K = 1.1;

  CapillaryModel pc(5);

  const int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEClass pdeAV(order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCmitAVSensor1D<BCTypeFlux_mitStateSpaceTime, BCTwoPhase1D< BCTypeFullStateSpaceTime, PDEClass>> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  Real pnB = 2534.5;
  Real SwB = 0.738;
  PressureNonWet_SaturationWet<Real> qdata(pnB, SwB);
  Real Cdiff = 3.0;
  BCClass bc(Cdiff, pdeAV, qdata);

  Real x = 0;
  Real time = 0;
  Real nx = 0.8, nt = 0.3;

  ParamType H = {{0.1},{0.05, 0.3}}; // grid spacing
  ParamType param = log(H);

  ArrayQ qI = {2500, 0.8, 0.1};
  ArrayQ qIx = {1.35, -0.05, 0.23};
  ArrayQ qIt = {-0.21, 0.85,-0.41};
  ArrayQ qB = 0;
  bc.state(param, x, time, nx, nt, qI, qB);

  BOOST_CHECK_CLOSE( pnB, qB[0], tol );
  BOOST_CHECK_CLOSE( SwB, qB[1], tol );
  BOOST_CHECK_CLOSE( qI[2], qB[2], tol );

  ArrayQ Fn_true = 0.0;
  pdeAV.fluxAdvectiveUpwindSpaceTime(param, x, time, qI, qB, nx, nt, Fn_true);

  ArrayQ fvx = 0, fvt = 0;
  pdeAV.fluxViscousSpaceTime(param, x, time, qB, qIx, qIt, fvx, fvt);
  Fn_true += fvx*nx + fvt*nt;

  DLA::VectorS<2, Real> nvec = {nx, nt};
  Real hn = dot(nvec, H*nvec);
  Fn_true[2] = sqrt(Cdiff)*hn*(qI(2) - 0.0);

  ArrayQ Fn = 0.1;
  bc.fluxNormalSpaceTime(param, x, time, nx, nt, qI, qIx, qIt, qB, Fn);
  BOOST_CHECK_CLOSE( Fn_true[0] + 0.1, Fn[0], tol );
  BOOST_CHECK_CLOSE( Fn_true[1] + 0.1, Fn[1], tol );
  BOOST_CHECK_CLOSE( Fn_true[2], Fn[2], tol );

  BOOST_CHECK(bc.isValidState(nx, nt, qI) == true);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
