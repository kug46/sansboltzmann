// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// OutputTwoPhase_btest
//
// testing of 1D/2D output functionals for two phase PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/PorousMedia/TraitsTwoPhase.h"
#include "pde/PorousMedia/Q1DPrimitive_pnSw.h"
#include "pde/PorousMedia/OutputTwoPhase.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/CapillaryModel.h"

#include "pde/PorousMedia/Q2DPrimitive_pnSw.h"
#include "pde/PorousMedia/SourceTwoPhase2D.h"
#include "pde/PorousMedia/PermeabilityModel2D.h"

#include "pde/PorousMedia/PDETwoPhase1D.h"
//#include "pde/PorousMedia/PDETwoPhase2D.h"

//Explicitly instantiate the class so coverage information is correct
namespace SANS
{
typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_Comp,
                            RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                            Real, CapillaryModel_Linear> TraitsModel;

typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModel> PDEClass1D;
//typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModel> PDEClass2D;

template class OutputTwoPhase_SaturationPower<PDEClass1D>;

typedef SourceTwoPhase2D_FixedPressureOutflow<TraitsSizeTwoPhase, TraitsModel> SourceOutflowClass;
template class OutputTwoPhase2D_Flowrate<SourceOutflowClass>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( OutputTwoPhase_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( OutputTest_SaturationPower )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef Real RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real pref = 1.47;

  const Real rhow_ref = 62.4;
  const Real rhon_ref = 52.1;
  const Real Cw = 0.03;
  const Real Cn = 0.05;

  const Real phi_ref = 0.3;
  const Real Cr = 0.06;

  const Real muw = 1.0, mun = 2.0;
  const Real pcmax = 5.0;

  DensityModel rhow_model(rhow_ref, Cw, pref);
  DensityModel rhon_model(rhon_ref, Cn, pref);

  PorosityModel phi_model(phi_ref, Cr, pref);

  RelPermModel krw_model(2);
  RelPermModel krn_model(2);

  ViscModel muw_model(muw);
  ViscModel mun_model(mun);

  CapillaryModel pc_model(pcmax);
  const Real K = 0.25;

  PDEClass pde(rhow_model, rhon_model, phi_model, krw_model, krn_model, muw_model, mun_model, K, pc_model);

  double a = 1.35;
  OutputTwoPhase_SaturationPower<PDEClass> outputFcn1(pde, 0, 2, a);
  OutputTwoPhase_SaturationPower<PDEClass> outputFcn2(pde, 1, 3, a);

  // DOF set tests
  Real Sw = 0.375;
  Real pn = 25.34;
  Real Sn = 1.0 - Sw;

  // Set
  Real qDataPrim[2] = {pn,Sw};
  string qNamePrim[2] = {"pn","Sw"};
  ArrayQ qTrue = {pn,Sw};
  ArrayQ q;
  ArrayQ qx = {0.1, 0.5};
  ArrayQ qy = {0.2, 0.6};
  ArrayQ qz = {0.3, 0.7};
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 2 );
  BOOST_CHECK_CLOSE( qTrue[0], q[0], tol );
  BOOST_CHECK_CLOSE( qTrue[1], q[1], tol );

  //Output test
  BOOST_ASSERT( outputFcn1.needsSolutionGradient() == false );

  Real x = 0.5, y = 0.7, z = 0.9, time = 1.0;
  Real output;
  outputFcn1(x, time, q, qx, output);
  BOOST_CHECK_CLOSE( a*Sw*Sw, output, tol );

  outputFcn1(x, y, time, q, qx, qy, output);
  BOOST_CHECK_CLOSE( a*Sw*Sw, output, tol );

  outputFcn1(x, y, z, time, q, qx, qy, qz, output);
  BOOST_CHECK_CLOSE( a*Sw*Sw, output, tol );

  outputFcn2(x, time, q, qx, output);
  BOOST_CHECK_CLOSE( a*Sn*Sn*Sn, output, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( OutputTest_PressurePower )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef Real RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real pref = 1.47;

  const Real rhow_ref = 62.4;
  const Real rhon_ref = 52.1;
  const Real Cw = 0.03;
  const Real Cn = 0.05;

  const Real phi_ref = 0.3;
  const Real Cr = 0.06;

  const Real muw = 1.0, mun = 2.0;
  const Real pcmax = 5.0;

  DensityModel rhow_model(rhow_ref, Cw, pref);
  DensityModel rhon_model(rhon_ref, Cn, pref);

  PorosityModel phi_model(phi_ref, Cr, pref);

  RelPermModel krw_model(2);
  RelPermModel krn_model(2);

  ViscModel muw_model(muw);
  ViscModel mun_model(mun);

  CapillaryModel pc_model(pcmax);
  const Real K = 0.25;

  PDEClass pde(rhow_model, rhon_model, phi_model, krw_model, krn_model, muw_model, mun_model, K, pc_model);

  double a = 1.35;
  OutputTwoPhase_PressurePower<PDEClass> outputFcn1(pde, 0, 2, a);
  OutputTwoPhase_PressurePower<PDEClass> outputFcn2(pde, 1, 3, a);

  // DOF set tests
  Real Sw = 0.375;
  Real pn = 25.34;
  Real pc = pcmax*(1.0 - Sw);
  Real pw = pn - pc;

  // Set
  Real qDataPrim[2] = {pn,Sw};
  string qNamePrim[2] = {"pn","Sw"};
  ArrayQ qTrue = {pn,Sw};
  ArrayQ q;
  ArrayQ qx = {0.1, 0.5};
  ArrayQ qy = {0.2, 0.6};
  ArrayQ qz = {0.3, 0.7};
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 2 );
  BOOST_CHECK_CLOSE( qTrue[0], q[0], tol );
  BOOST_CHECK_CLOSE( qTrue[1], q[1], tol );

  //Output test
  BOOST_ASSERT( outputFcn1.needsSolutionGradient() == false );

  Real x = 0.5, y = 0.7, z = 0.9, time = 1.0;
  Real output;
  outputFcn1(x, time, q, qx, output);
  BOOST_CHECK_CLOSE( a*pw*pw, output, tol );

  outputFcn1(x, y, time, q, qx, qy, output);
  BOOST_CHECK_CLOSE( a*pw*pw, output, tol );

  outputFcn1(x, y, z, time, q, qx, qy, qz, output);
  BOOST_CHECK_CLOSE( a*pw*pw, output, tol );

  outputFcn2(x, time, q, qx, output);
  BOOST_CHECK_CLOSE( a*pn*pn*pn, output, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()


//############################################################################//
BOOST_AUTO_TEST_SUITE( OutputTwoPhase2D_test_suite )

Real evalGaussianModelFactor(int nParam, Real r, Real rw, Real R, Real pn, Real pB )
{
  //Set the Gaussian model parameters
  std::vector<Real> avec(nParam);

  if (nParam == 1)
  {
    avec[0] = 1.0;
  }
  else if (nParam == 3)
  {
    avec[0] =  1.6805486353473567929893462186086;
    avec[1] = -0.61235120144821138025328586331859;
    avec[2] =  0.23570005440147727558578930602427;
  }
  else
    SANS_DEVELOPER_EXCEPTION("evalGaussianModelFactor - Invalid parameter count for model.");

  Real sum_ai = 0.0;
  Real sum_2i_ai = 0.0;
  Real sum_ai_r_R_power = 0.0;
  Real sum_4i2_ai_r_R_power = 0.0;
  Real sum_2i_ai_r_R_power = 0.0;

  for (int i = 1; i <= nParam; i++)
  {
    sum_ai += avec[i-1];
    sum_2i_ai += 2*i*avec[i-1];
    sum_ai_r_R_power += avec[i-1] * pow(r/R, 2.0*i);
    sum_4i2_ai_r_R_power += 4*i*i * avec[i-1] / pow(R, 2.0*i) * pow(r, 2.0*i - 2.0);
    sum_2i_ai_r_R_power += 2*i * avec[i-1] / pow(R, 2.0*i) * pow(r, 2.0*i - 1.0);
  }

  Real expterm = exp(-sum_ai_r_R_power);
  Real dpndr2 = (pn - pB) / ( exp(-sum_ai) * (1.0 + sum_2i_ai*log(R/rw) ) - expterm )
               * expterm * (sum_4i2_ai_r_R_power - (sum_2i_ai_r_R_power*sum_2i_ai_r_R_power));

  return dpndr2;
}

//----------------------------------------------------------------------------//
// from SourceTwoPhase2D_btest: Source2D_FixedPressureOutflow_Gaussian_Volumetricflow_eval
BOOST_AUTO_TEST_CASE( OutputTest_FixedPressureOutflow_Gaussian_Volumetricflow_eval )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef QTypePrimitive_pnSw QType;
  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel,
                              CapillaryModel> TraitsModelClass;

  typedef SourceTwoPhase2D_FixedPressureOutflow<TraitsSizeTwoPhase, TraitsModelClass> SourceClass;
  typedef SourceClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  const Real pref = 1.47;
  const Real pB = 15.82;
  const Real Rw = 0.5;

  const Real rhow_ref = 62.4;
  const Real rhon_ref = 52.1;
  const Real Cw = 0.03;
  const Real Cn = 0.05;

  const Real muw = 1.0, mun = 2.0;
  const Real pcmax = 5.0;

  DensityModel rhow_model(rhow_ref, Cw, pref);
  DensityModel rhon_model(rhon_ref, Cn, pref);

  RelPermModel krw_model(2);
  RelPermModel krn_model(2);

  ViscModel muw_model(muw);
  ViscModel mun_model(mun);

  Real Kxx = 1.2, Kxy = 0.0, Kyx = 0.0, Kyy = 0.3; //matrix has to be diagonal
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc_model(pcmax);

  // Function tests
  Real Sw = 0.375;
  Real Swx = -0.63, Swy = 0.27;
  Real pn = 25.34;
  Real pnx = -0.52, pny = 0.81;
  Real Sn = 1.0 - Sw;

  Real krw = pow(Sw,2);
  Real krn = pow(Sn,2);

  Real lambda_w = krw/muw;
  Real lambda_n = krn/mun;

  ArrayQ q = {pn,Sw};
  ArrayQ qx = {pnx, Swx};
  ArrayQ qy = {pny, Swy};

  // coordinate transform parameters
  Real xscale = pow(Kyy/Kxx, 0.25);
  Real yscale = pow(Kxx/Kyy, 0.25);
  Real savg = 0.5*(xscale + yscale);
  xscale /= savg;
  yscale /= savg;
  Real volscale = xscale*yscale;

  for (int nParam = 1; nParam <= 3; nParam += 2)
  {
    // Set up PyDicts
    PyDict fixedwellpressure;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.pB] = pB;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.Rwellbore] = Rw;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.nParam] = nParam;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.WellModel] = "Gaussian";
    fixedwellpressure[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureOutflow;

    Real xc = 10.0, yc = 15.0, tc = 2.0;
    Real R = 5.0;
    Real smoothr = 1.0;
    Real smoothT = 0.2;

    PyDict well;
    well[SourceTwoPhase2DParam::params.Source] = fixedwellpressure;
    well[SourceTwoPhase2DParam::params.xcentroid] = xc;
    well[SourceTwoPhase2DParam::params.ycentroid] = yc;
    well[SourceTwoPhase2DParam::params.R] = R;
    well[SourceTwoPhase2DParam::params.Tmin] = tc - 1.0;
    well[SourceTwoPhase2DParam::params.Tmax] = tc + 1.0;
    well[SourceTwoPhase2DParam::params.smoothLr] = smoothr;
    well[SourceTwoPhase2DParam::params.smoothT] = smoothT;
    well[SourceTwoPhase2DParam::params.visibleAngle] = 2*PI;

    SourceTwoPhase2DParam::checkInputs(well);

    SourceClass source(well, rhow_model, rhon_model, krw_model, krn_model, muw_model, mun_model, K, pc_model, false);
    std::vector<SourceClass> sourceList1 = { source };

    OutputTwoPhase2D_Flowrate<SourceClass> outputFcn0(sourceList1, 0);   // wetting phase (water)
    OutputTwoPhase2D_Flowrate<SourceClass> outputFcn1(sourceList1, 1);   // non-wetting phase (oil)
    OutputTwoPhase2D_Flowrate<SourceClass> outputFcn2(sourceList1, 2);   // total: sum of wetting & non-wetting

    //------------------------------------------------------------

    Real x = xc - (R + 0.5*smoothr)/xscale, y = yc, t = tc;
    Real r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    Real dpndr2 = evalGaussianModelFactor(nParam, r, Rw, R, pn, pB);
    Real weight = 0.0;

    ArrayQ sourceTrue, sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*lambda_w*sqrt(Kxx*Kyy)*dpndr2;
    sourceTrue[1] = weight*volscale*lambda_n*sqrt(Kxx*Kyy)*dpndr2;

    Real out0 = 0, out1 = 0, out2 = 0;
    outputFcn0(x, y, t, q, qx, qy, out0);
    outputFcn1(x, y, t, q, qx, qy, out1);
    outputFcn2(x, y, t, q, qx, qy, out2);
    SANS_CHECK_CLOSE( sourceTrue[0], out0, small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], out1, small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[0] + sourceTrue[1], out2, small_tol, close_tol);

    //------------------------------------------------------------

    x = xc, y = yc, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    dpndr2 = evalGaussianModelFactor(nParam, r, Rw, R, pn, pB);
    weight = 1.0;

    sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*lambda_w*sqrt(Kxx*Kyy)*dpndr2; //special case of r = 0
    sourceTrue[1] = weight*volscale*lambda_n*sqrt(Kxx*Kyy)*dpndr2; //special case of r = 0

    out0 = 0; out1 = 0; out2 = 0;
    outputFcn0(x, y, t, q, qx, qy, out0);
    outputFcn1(x, y, t, q, qx, qy, out1);
    outputFcn2(x, y, t, q, qx, qy, out2);
    SANS_CHECK_CLOSE( sourceTrue[0], out0, small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], out1, small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[0] + sourceTrue[1], out2, small_tol, close_tol);

    //------------------------------------------------------------

    x = xc + R/xscale, y = yc, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    dpndr2 = evalGaussianModelFactor(nParam, r, Rw, R, pn, pB);
    weight = 0.5;

    sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*lambda_w*sqrt(Kxx*Kyy)*dpndr2;
    sourceTrue[1] = weight*volscale*lambda_n*sqrt(Kxx*Kyy)*dpndr2;

    out0 = 0; out1 = 0; out2 = 0;
    outputFcn0(x, y, t, q, qx, qy, out0);
    outputFcn1(x, y, t, q, qx, qy, out1);
    outputFcn2(x, y, t, q, qx, qy, out2);
    SANS_CHECK_CLOSE( sourceTrue[0], out0, small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], out1, small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[0] + sourceTrue[1], out2, small_tol, close_tol);

    //------------------------------------------------------------

    x = xc + 0.5*R/xscale, y = yc - 0.5*R/yscale, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    dpndr2 = evalGaussianModelFactor(nParam, r, Rw, R, pn, pB);
    weight = 1.0;

    sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*lambda_w*sqrt(Kxx*Kyy)*dpndr2;
    sourceTrue[1] = weight*volscale*lambda_n*sqrt(Kxx*Kyy)*dpndr2;

    out0 = 0; out1 = 0; out2 = 0;
    outputFcn0(x, y, t, q, qx, qy, out0);
    outputFcn1(x, y, t, q, qx, qy, out1);
    outputFcn2(x, y, t, q, qx, qy, out2);
    SANS_CHECK_CLOSE( sourceTrue[0], out0, small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], out1, small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[0] + sourceTrue[1], out2, small_tol, close_tol);

    //------------------------------------------------------------

    std::vector<SourceClass> sourceList2 = { source, source };

    OutputTwoPhase2D_Flowrate<SourceClass> outputFcnList0(sourceList2, 0);   // wetting phase (water)
    OutputTwoPhase2D_Flowrate<SourceClass> outputFcnList1(sourceList2, 1);   // non-wetting phase (oil)
    OutputTwoPhase2D_Flowrate<SourceClass> outputFcnList2(sourceList2, 2);   // total: sum of wetting & non-wetting

    out0 = 0; out1 = 0; out2 = 0;
    outputFcnList0(x, y, t, q, qx, qy, out0);
    outputFcnList1(x, y, t, q, qx, qy, out1);
    outputFcnList2(x, y, t, q, qx, qy, out2);
    SANS_CHECK_CLOSE( 2*sourceTrue[0], out0, small_tol, close_tol);
    SANS_CHECK_CLOSE( 2*sourceTrue[1], out1, small_tol, close_tol);
    SANS_CHECK_CLOSE( 2*(sourceTrue[0] + sourceTrue[1]), out2, small_tol, close_tol);
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
