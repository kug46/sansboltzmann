// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// SourceTwoPhase2D_btest
//
// testing of 2D two phase source class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/PorousMedia/Q2DPrimitive_pnSw.h"
#include "pde/PorousMedia/TraitsTwoPhase.h"
#include "pde/PorousMedia/SourceTwoPhase2D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/PermeabilityModel2D.h"
#include "pde/PorousMedia/CapillaryModel.h"

//Explicitly instantiate the class so coverage information is correct
namespace SANS
{

}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( SourceTwoPhase2D_test_suite )

Real evalGaussianModelFactor(int nParam, Real r, Real rw, Real R, Real pn, Real pB )
{
  //Set the Gaussian model parameters
  std::vector<Real> avec(nParam);

  if (nParam == 1)
  {
    avec[0] = 1.0;
  }
  else if (nParam == 3)
  {
    avec[0] =  1.6805486353473567929893462186086;
    avec[1] = -0.61235120144821138025328586331859;
    avec[2] =  0.23570005440147727558578930602427;
  }
  else
    SANS_DEVELOPER_EXCEPTION("evalGaussianModelFactor - Invalid parameter count for model.");

  Real sum_ai = 0.0;
  Real sum_2i_ai = 0.0;
  Real sum_ai_r_R_power = 0.0;
  Real sum_4i2_ai_r_R_power = 0.0;
  Real sum_2i_ai_r_R_power = 0.0;

  for (int i = 1; i <= nParam; i++)
  {
    sum_ai += avec[i-1];
    sum_2i_ai += 2*i*avec[i-1];
    sum_ai_r_R_power += avec[i-1] * pow(r/R, 2.0*i);
    sum_4i2_ai_r_R_power += 4*i*i * avec[i-1] / pow(R, 2.0*i) * pow(r, 2.0*i - 2.0);
    sum_2i_ai_r_R_power += 2*i * avec[i-1] / pow(R, 2.0*i) * pow(r, 2.0*i - 1.0);
  }

  Real expterm = exp(-sum_ai_r_R_power);
  Real dpndr2 = (pn - pB) / ( exp(-sum_ai) * (1.0 + sum_2i_ai*log(R/rw) ) - expterm )
               * expterm * (sum_4i2_ai_r_R_power - (sum_2i_ai_r_R_power*sum_2i_ai_r_R_power));

  return dpndr2;
}

Real evalPolyFlatModelFactor(int nParam, Real r, Real rw, Real R, Real pn, Real pB )
{
  const int order = 2*nParam;

  std::vector<Real> a(order + 1, 0.0);

  Real s0 = 0.0;
  Real s02 = s0*s0;
  Real s03 = s02*s0;
  Real s04 = s03*s0;
  Real s05 = s04*s0;
  Real s06 = s05*s0;

  if (order == 2)
  {
    Real denom = pow(1.0 - s0, 2.0);
    a[0] = s02 / denom;
    a[1] = -2.0*s0 / denom;
    a[2] = 1.0 / denom;
  }
  else if (order == 4)
  {
    Real denom = pow(1.0 - s0, 4.0) * (s0 - 7.0);
    a[0] =      s03*(s02 - 11.0*s0 + 16.0) / denom;
    a[1] = 24.0*s02*(s0 - 2.0) / denom;
    a[2] = -6.0*s0 *(s02 + s0 - 8.0) / denom;
    a[3] =  8.0*    (s02 - 2.0*s0 - 2.0) / denom;
    a[4] = -3.0*    (s0 - 3.0) / denom;
  }
  else if (order == 6)
  {
    Real denom = pow(1.0 - s0, 6.0) * (s02 - 8.0*s0 + 37.0);
    a[0] =        s04*(s04 - 14.0*s03 + 100.0*s02 - 252.0*s0 + 225.0) / denom;
    a[1] = -180.0*s03*(s02 - 4.0*s0 + 5.0) / denom;
    a[2] =   90.0*s02*(s03 - 2.0*s02 - 4.0*s0 + 15.0) / denom;
    a[3] =  -20.0*s0 *(s04 + 4.0*s03 - 26.0*s02 + 36.0*s0 + 45.0) / denom;
    a[4] =       (45.0*s04 - 90.0*s03 - 180.0*s02 + 900.0*s0 + 225) / denom;
    a[5] =      -(36.0*s03 - 144.0*s02 + 180.0*s0 + 288.0) / denom;
    a[6] =       (10.0*s02 - 50.0*s0 + 100.0) / denom;
  }
  else if (order == 8)
  {
    Real denom = pow(1.0 - s0, 8.0) * (3.0*s03 - 29.0*s02 + 139.0*s0 - 533.0);
    a[0] =         s05*(3.0*s06 -   53.0*s05 + 455.0*s04 - 2625.0*s03 + 8240.0*s02 - 13328.0*s0 + 9408.0) / denom;
    a[1] =  3360.0*s04*(    s03 -    6.0*s02 +  14.0*s0  - 14.0) / denom;
    a[2] =  -840.0*s03*(3.0*s04 -   13.0*s03 +  10.0*s02 + 42.0*s0 - 112.0) / denom;
    a[3] =  1120.0*s02*(    s05 -        s04 -  15.0*s03 + 50.0*s02 - 56.0*s0 - 84.0) / denom;
    a[4] =  -210.0*s0 *(    s06 +    9.0*s05 -  55.0*s04 + 65.0*s03 + 120.0*s02 - 616.0*s0 - 224.0) / denom;
    a[5] =           (672.0*s06 -  672.0*s05 - 10080.0*s04 + 33600.0*s03 - 47040.0*s02 - 84672.0*s0 - 9408) / denom;
    a[6] =          (-840.0*s05 + 3640.0*s04 -  2800.0*s03 - 11760.0*s02 + 50960.0*s0 + 19600.0) / denom;
    a[7] =           (480.0*s04 - 2880.0*s03 +  6720.0*s02 -  6720.0*s0  - 14400.0) / denom;
    a[8] =          (-105.0*s03 +  735.0*s02 -  2205.0*s0  +  3675.0) / denom;
  }
  else if (order == 10)
  {
    a[6]  =   88200.0/1627.0;
    a[7]  = -259200.0/1627.0;
    a[8]  =  297675.0/1627.0;
    a[9]  = -156800.0/1627.0;
    a[10] =   31752.0/1627.0;
  }
  else if (order == 12)
  {
    a[7]  =   3136320.0/18107.0;
    a[8]  = -12006225.0/18107.0;
    a[9]  =  18972800.0/18107.0;
    a[10] = -15367968.0/18107.0;
    a[11] =   6350400.0/18107.0;
    a[12] =  -1067220.0/18107.0;
  }
  else
    SANS_DEVELOPER_EXCEPTION("evalPolyFlatModelFactor - Invalid order.");

  Real s = r/R;

  Real f, df, d2f;
  Real f1, df1, d2f1;
  wellActivationFunctionPolynomial(a, s, s0, f, df, d2f);
  wellActivationFunctionPolynomial(a, 1.0, s0, f1, df1, d2f1); //evaluated at s = 1

  Real df_s;
  if (s == 0.0) //avoid division by zero
  {
    if (s0 == 0.0)
    {
      // f'(s) / s = a1/s + 2*a2 + 3*a3*s + ...
      BOOST_REQUIRE(a[1] == 0.0);
      df_s = 2.0*a[2];
    }
    else
    {
      df_s = 0.0;
    }
  }
  else
    df_s = df/s;

  Real dpndr2 = (pn - pB) / ((f - 1.0 + df1*log(R/rw))*R*R) * (df_s + d2f);
  return dpndr2;
}

Real evalPolyModelFactor(int Corder, Real r, Real rw, Real R, Real pn, Real pB )
{
  std::vector<Real> a;

  if (Corder == 1)
  {
    a = {-1.0/2.0, 0.0, 1.0/2.0};
  }
  else if (Corder == 2)
  {
    a = {-5.0/6.0, 0.0, 3.0/2.0, -2.0/3.0};
  }
  else if (Corder == 3)
  {
    a = {-19.0/20.0, 0.0, 5.0/3.0, 0.0, -5.0/4.0, 8.0/15.0};
  }
  else if (Corder == 4)
  {
    a = {-106.0/105.0, 0.0, 7.0/4.0, 0.0, 0.0, -14.0/5.0, 35.0/12.0, -6.0/7.0};
  }
  else if (Corder == 5)
  {
    a = {-527.0/504.0, 0.0, 9.0/5.0, 0.0, 0.0, 0.0, -7.0, 432.0/35.0, -63.0/8.0, 16.0/9.0};
  }
  else if (Corder == 6)
  {
    a = {-29657.0/27720.0, 0.0, 11.0/6.0, 0.0, 0.0, 0.0, 0.0,
         -132.0/7.0, 385.0/8.0, -440.0/9.0, 231.0/10.0, -140.0/33.0};
  }
  else if (Corder == 7)
  {
    a = {-55973.0/51480.0, 0.0, 13.0/7.0, 0.0, 0.0, 0.0, 0.0, 0.0,
         -429.0/8.0, 11440.0/63.0, -1287.0/5.0, 2080.0/11.0, -143.0/2.0, 144.0/13.0};
  }
  else
    SANS_DEVELOPER_EXCEPTION("evalPolyModelFactor - Invalid continuity order.");

  Real s = r/R;

  Real f, df, d2f;
  wellActivationFunctionPolynomial(a, s, 0.0, f, df, d2f);

  Real df_s;
  if (s == 0.0) //avoid division by zero
  {
    // f'(s) / s = a1/s + 2*a2 + 3*a3*s + ...
    BOOST_REQUIRE(a[1] == 0.0);
    df_s = 2.0*a[2];
  }
  else
    df_s = df/s;

  Real dpndr2 = (pn - pB) / ((f + log(R/rw))*R*R) * (df_s + d2f);
  return dpndr2;
}

void evalPolyModelFixedRateSource(int Corder, Real r, Real R, Real& f, Real& qhat)
{
  std::vector<Real> a = wellActivationFunctionPolyCoeff(Corder);

  Real s = r/R;

  Real df, d2f;
  wellActivationFunctionPolynomial(a, s, 0.0, f, df, d2f);

  Real df_s;
  if (s == 0.0) //avoid division by zero
  {
    // f'(s) / s = a1/s + 2*a2 + 3*a3*s + ...
    BOOST_REQUIRE(a[1] == 0.0);
    df_s = 2.0*a[2];
  }
  else
    df_s = df/s;

  qhat = df_s + d2f;
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Source2D_FixedPressure_ctor )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef QTypePrimitive_pnSw QType;
  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel,
                              CapillaryModel> TraitsModelClass;

  typedef SourceTwoPhase2D_FixedPressureInflow<TraitsSizeTwoPhase, TraitsModelClass> SourceClassPressureInflow;
  typedef SourceTwoPhase2D_FixedPressureOutflow<TraitsSizeTwoPhase, TraitsModelClass> SourceClassPressureOutflow;
  typedef SourceTwoPhase2D_FixedInflowRate<TraitsSizeTwoPhase, TraitsModelClass> SourceClassInflowRate;
  typedef SourceTwoPhase2D_FixedOutflowRate<TraitsSizeTwoPhase, TraitsModelClass> SourceClassOutflowRate;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  Real Kxx = 1.1, Kxy = 0.6, Kyx = 0.2, Kyy = 1.5;
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc(5);

  const Real tol = 1e-12;

  // Set up PyDicts
  PyDict fixedwellpressure1;
  fixedwellpressure1[SourceTwoPhase2DType_FixedPressureInflow_Param::params.pB] = 2500.0;
  fixedwellpressure1[SourceTwoPhase2DType_FixedPressureInflow_Param::params.Sw] = 1.0;
  fixedwellpressure1[SourceTwoPhase2DType_FixedPressureInflow_Param::params.Rwellbore] = 0.5;
  fixedwellpressure1[SourceTwoPhase2DType_FixedPressureInflow_Param::params.nParam] = 1;
  fixedwellpressure1[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.WellModel] = "Gaussian";
  fixedwellpressure1[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureInflow;

  PyDict fixedwellpressure2;
  fixedwellpressure2[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.pB] = 2350.0;
  fixedwellpressure2[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.Rwellbore] = 0.5;
  fixedwellpressure2[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.nParam] = 3;
  fixedwellpressure2[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.WellModel] = "Polynomial";
  fixedwellpressure2[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureOutflow;

  PyDict fixedrate1;
  fixedrate1[SourceTwoPhase2DType_FixedInflowRate_Param::params.Q] = 1.0;
  fixedrate1[SourceTwoPhase2DType_FixedInflowRate_Param::params.Sw] = 1.0;
  fixedrate1[SourceTwoPhase2DType_FixedInflowRate_Param::params.nParam] = 3;
  fixedrate1[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedInflowRate;

  PyDict fixedrate2;
  fixedrate2[SourceTwoPhase2DType_FixedOutflowRate_Param::params.Q] = 1.0;
  fixedrate2[SourceTwoPhase2DType_FixedOutflowRate_Param::params.nParam] = 3;
  fixedrate2[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedOutflowRate;

  Real xc1 = 10.0, yc1 = 15.0;
  Real xc2 = 25.0, yc2 = 50.0;
  Real R1 = 5.0, R2 = 2.0;

  PyDict well1;
  well1[SourceTwoPhase2DParam::params.Source] = fixedwellpressure1;
  well1[SourceTwoPhase2DParam::params.xcentroid] = xc1;
  well1[SourceTwoPhase2DParam::params.ycentroid] = yc1;
  well1[SourceTwoPhase2DParam::params.R] = R1;
  well1[SourceTwoPhase2DParam::params.Tmin] = 1.0;
  well1[SourceTwoPhase2DParam::params.Tmax] = 3.0;
  well1[SourceTwoPhase2DParam::params.smoothLr] = 1.0;
  well1[SourceTwoPhase2DParam::params.smoothT] = 0.2;
  well1[SourceTwoPhase2DParam::params.visibleAngle] = 2*PI;

  SourceClassPressureInflow source1(well1, rhow, rhon, krw, krn, muw, mun, K, pc);

  BOOST_CHECK_CLOSE( source1.getWellPressure(), 2500.0, tol);

  BOOST_CHECK_CLOSE( source1.getWeight(4.5, 15.0, 2.0), 0.0, tol);
  BOOST_CHECK_CLOSE( source1.getWeight(10.0, 15.0, 2.0), 1.0, tol);
  BOOST_CHECK_CLOSE( source1.getWeight(15.0, 15.0, 2.0), 0.5, tol);
  BOOST_CHECK_CLOSE( source1.getWeight(12.0, 16.0, 2.0), 1.0, tol);
  BOOST_CHECK_CLOSE( source1.getWeight(12.0, 16.0, 3.0), 0.5, tol);
  BOOST_CHECK_CLOSE( source1.getWeight(12.0, 16.0, 3.1), 0.0, tol);

  PyDict well2;
  well2[SourceTwoPhase2DParam::params.Source] = fixedwellpressure2;
  well2[SourceTwoPhase2DParam::params.xcentroid] = xc2;
  well2[SourceTwoPhase2DParam::params.ycentroid] = yc2;
  well2[SourceTwoPhase2DParam::params.R] = R2;
  well2[SourceTwoPhase2DParam::params.Tmin] = 5.0;
  well2[SourceTwoPhase2DParam::params.Tmax] = 10.0;
  well2[SourceTwoPhase2DParam::params.smoothLr] = 0.5;
  well2[SourceTwoPhase2DParam::params.smoothT] = 1.0;
  well2[SourceTwoPhase2DParam::params.visibleAngle] = 2*PI;

  SourceClassPressureOutflow source2(well2, rhow, rhon, krw, krn, muw, mun, K, pc);

  BOOST_CHECK_CLOSE( source2.getWellPressure(), 2350.0, tol);

  BOOST_CHECK_CLOSE( source2.getWeight(22.75, 50.0, 8.0), 0.0, tol);
  BOOST_CHECK_CLOSE( source2.getWeight(25.0, 50.0, 8.0), 1.0, tol);
  BOOST_CHECK_CLOSE( source2.getWeight(23.0, 50.0, 8.0), 0.5, tol);
  BOOST_CHECK_CLOSE( source2.getWeight(25.0, 52.0, 8.0), 0.5, tol);
  BOOST_CHECK_CLOSE( source2.getWeight(26.0, 51.0, 10.0), 0.5, tol);
  BOOST_CHECK_CLOSE( source2.getWeight(26.0, 51.0, 11.0), 0.0, tol);

  PyDict well3;
  well3[SourceTwoPhase2DParam::params.Source] = fixedrate1;
  well3[SourceTwoPhase2DParam::params.xcentroid] = xc1;
  well3[SourceTwoPhase2DParam::params.ycentroid] = yc1;
  well3[SourceTwoPhase2DParam::params.R] = R1;
  well3[SourceTwoPhase2DParam::params.Tmin] = 1.0;
  well3[SourceTwoPhase2DParam::params.Tmax] = 3.0;
  well3[SourceTwoPhase2DParam::params.smoothLr] = 1.0;
  well3[SourceTwoPhase2DParam::params.smoothT] = 0.2;
  well3[SourceTwoPhase2DParam::params.visibleAngle] = 2*PI;

  SourceClassInflowRate source3(well3, rhow, rhon, krw, krn, muw, mun, K, pc);

  BOOST_CHECK_CLOSE( source3.getWeight(4.5, 15.0, 2.0), 0.0, tol);
  BOOST_CHECK_CLOSE( source3.getWeight(10.0, 15.0, 2.0), 1.0, tol);
  BOOST_CHECK_CLOSE( source3.getWeight(15.0, 15.0, 2.0), 0.5, tol);
  BOOST_CHECK_CLOSE( source3.getWeight(12.0, 16.0, 2.0), 1.0, tol);
  BOOST_CHECK_CLOSE( source3.getWeight(12.0, 16.0, 3.0), 0.5, tol);
  BOOST_CHECK_CLOSE( source3.getWeight(12.0, 16.0, 3.1), 0.0, tol);

  PyDict well4;
  well4[SourceTwoPhase2DParam::params.Source] = fixedrate2;
  well4[SourceTwoPhase2DParam::params.xcentroid] = xc2;
  well4[SourceTwoPhase2DParam::params.ycentroid] = yc2;
  well4[SourceTwoPhase2DParam::params.R] = R2;
  well4[SourceTwoPhase2DParam::params.Tmin] = 5.0;
  well4[SourceTwoPhase2DParam::params.Tmax] = 10.0;
  well4[SourceTwoPhase2DParam::params.smoothLr] = 0.5;
  well4[SourceTwoPhase2DParam::params.smoothT] = 1.0;
  well4[SourceTwoPhase2DParam::params.visibleAngle] = 2*PI;

  SourceClassOutflowRate source4(well4, rhow, rhon, krw, krn, muw, mun, K, pc);

  BOOST_CHECK_CLOSE( source4.getWeight(22.75, 50.0, 8.0), 0.0, tol);
  BOOST_CHECK_CLOSE( source4.getWeight(25.0, 50.0, 8.0), 1.0, tol);
  BOOST_CHECK_CLOSE( source4.getWeight(23.0, 50.0, 8.0), 0.5, tol);
  BOOST_CHECK_CLOSE( source4.getWeight(25.0, 52.0, 8.0), 0.5, tol);
  BOOST_CHECK_CLOSE( source4.getWeight(26.0, 51.0, 10.0), 0.5, tol);
  BOOST_CHECK_CLOSE( source4.getWeight(26.0, 51.0, 11.0), 0.0, tol);

  PyDict sourceList;
  sourceList["well1"] = well1;
  sourceList["well2"] = well2;
  sourceList["well3"] = well3;
  sourceList["well4"] = well4;
  SourceTwoPhase2DListParam::checkInputs(sourceList);

  //An even number of parameters should fail for the Gaussian model
  fixedwellpressure1[SourceTwoPhase2DType_FixedPressureInflow_Param::params.nParam] = 2;

  BOOST_CHECK_THROW( SourceClassPressureInflow source5(well1, rhow, rhon, krw, krn, muw, mun, K, pc), DeveloperException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Source2D_FixedPressureInflow_Massflow_eval )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef QTypePrimitive_pnSw QType;
  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel,
                              CapillaryModel> TraitsModelClass;

  typedef SourceTwoPhase2D_FixedPressureInflow<TraitsSizeTwoPhase, TraitsModelClass> SourceClass;
  typedef SourceClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  const Real pref = 1.47;
  const Real pB = 15.82;
  const Real Sw_in = 0.9;
  const Real Rw = 0.5;

  const Real rhow_ref = 62.4;
  const Real rhon_ref = 52.1;
  const Real Cw = 0.03;
  const Real Cn = 0.05;

  const Real muw = 1.0, mun = 2.0;
  const Real pcmax = 5.0;

  DensityModel rhow_model(rhow_ref, Cw, pref);
  DensityModel rhon_model(rhon_ref, Cn, pref);

  RelPermModel krw_model(2);
  RelPermModel krn_model(2);

  ViscModel muw_model(muw);
  ViscModel mun_model(mun);

  Real Kxx = 1.2, Kxy = 0.0, Kyx = 0.0, Kyy = 0.3; //matrix has to be diagonal
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc_model(pcmax);

  // Function tests
  Real Sw = Sw_in;
  Real Swx = -0.63, Swy = 0.27;
  Real pn = 10.34;
  Real pnx = -0.52, pny = 0.81;

  Real Sn = 1.0 - Sw;
  Real pc = pcmax*Sn;
  Real pw = pn - pc;

  Real krw = pow(Sw,2);
  Real krn = pow(Sn,2);

  Real rhow = rhow_ref*exp(Cw*(pw - pref));
  Real rhon = rhon_ref*exp(Cn*(pn - pref));

  Real lambda_w = krw/muw;
  Real lambda_n = krn/mun;

  Sw = 0.375;
  ArrayQ q = {pn,Sw};
  ArrayQ qx = {pnx, Swx};
  ArrayQ qy = {pny, Swy};

  //Coordinate transform parameters
  Real xscale = pow(Kyy/Kxx, 0.25);
  Real yscale = pow(Kxx/Kyy, 0.25);
  Real savg = 0.5*(xscale + yscale);
  xscale /= savg;
  yscale /= savg;
  Real volscale = xscale*yscale;

  //Gaussian well model
  for (int nParam = 1; nParam <= 3; nParam += 2)
  {
    // Set up PyDicts
    PyDict fixedwellpressure;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureInflow_Param::params.pB] = pB;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureInflow_Param::params.Sw] = Sw_in;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureInflow_Param::params.Rwellbore] = Rw;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureInflow_Param::params.nParam] = nParam;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureInflow_Param::params.WellModel] = "Gaussian";
    fixedwellpressure[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureInflow;

    Real xc = 10.0, yc = 15.0, tc = 2.0;
    Real R = 5.0;
    Real smoothr = 1.0;
    Real smoothT = 0.2;

    PyDict well;
    well[SourceTwoPhase2DParam::params.Source] = fixedwellpressure;
    well[SourceTwoPhase2DParam::params.xcentroid] = xc;
    well[SourceTwoPhase2DParam::params.ycentroid] = yc;
    well[SourceTwoPhase2DParam::params.R] = R;
    well[SourceTwoPhase2DParam::params.Tmin] = tc - 1.0;
    well[SourceTwoPhase2DParam::params.Tmax] = tc + 1.0;
    well[SourceTwoPhase2DParam::params.smoothLr] = smoothr;
    well[SourceTwoPhase2DParam::params.smoothT] = smoothT;
    well[SourceTwoPhase2DParam::params.visibleAngle] = 2*PI;

    SourceTwoPhase2DParam::checkInputs(well);

    SourceClass source(well, rhow_model, rhon_model, krw_model, krn_model, muw_model, mun_model, K, pc_model);

    //------------------------------------------------------------

    Real x = xc - (R + 0.5*smoothr)/xscale, y = yc, t = tc;
    Real r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    Real dpndr2 = evalGaussianModelFactor(nParam, r, Rw, R, pn, pB);
    Real weight = 0.0;

    ArrayQ sourceTrue, sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*rhow*lambda_w*sqrt(Kxx*Kyy)*dpndr2;
    sourceTrue[1] = weight*volscale*rhon*lambda_n*sqrt(Kxx*Kyy)*dpndr2;

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc, y = yc, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    dpndr2 = evalGaussianModelFactor(nParam, r, Rw, R, pn, pB);
    weight = 1.0;

    sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*rhow*lambda_w*sqrt(Kxx*Kyy)*dpndr2; //special case of r = 0
    sourceTrue[1] = weight*volscale*rhon*lambda_n*sqrt(Kxx*Kyy)*dpndr2; //special case of r = 0

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc + R/xscale, y = yc, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    dpndr2 = evalGaussianModelFactor(nParam, r, Rw, R, pn, pB);
    weight = 0.5;

    sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*rhow*lambda_w*sqrt(Kxx*Kyy)*dpndr2;
    sourceTrue[1] = weight*volscale*rhon*lambda_n*sqrt(Kxx*Kyy)*dpndr2;

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc + 0.5*R/xscale, y = yc - 0.5*R/yscale, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    dpndr2 = evalGaussianModelFactor(nParam, r, Rw, R, pn, pB);
    weight = 1.0;

    sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*rhow*lambda_w*sqrt(Kxx*Kyy)*dpndr2;
    sourceTrue[1] = weight*volscale*rhon*lambda_n*sqrt(Kxx*Kyy)*dpndr2;

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);
  }

  //Polynomial well model - with zero derivatives at origin
  for (int nParam = 1; nParam <= 6; nParam ++)
  {
    // Set up PyDicts
    PyDict fixedwellpressure;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureInflow_Param::params.pB] = pB;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureInflow_Param::params.Sw] = Sw_in;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureInflow_Param::params.Rwellbore] = Rw;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureInflow_Param::params.nParam] = nParam;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureInflow_Param::params.WellModel] = "PolynomialFlat";
    fixedwellpressure[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureInflow;

    Real xc = 10.0, yc = 15.0, tc = 2.0;
    Real R = 5.0;
    Real smoothr = 1.0;
    Real smoothT = 0.2;

    PyDict well;
    well[SourceTwoPhase2DParam::params.Source] = fixedwellpressure;
    well[SourceTwoPhase2DParam::params.xcentroid] = xc;
    well[SourceTwoPhase2DParam::params.ycentroid] = yc;
    well[SourceTwoPhase2DParam::params.R] = R;
    well[SourceTwoPhase2DParam::params.Tmin] = tc - 1.0;
    well[SourceTwoPhase2DParam::params.Tmax] = tc + 1.0;
    well[SourceTwoPhase2DParam::params.smoothLr] = smoothr;
    well[SourceTwoPhase2DParam::params.smoothT] = smoothT;
    well[SourceTwoPhase2DParam::params.visibleAngle] = 2*PI;

    SourceTwoPhase2DParam::checkInputs(well);

    SourceClass source(well, rhow_model, rhon_model, krw_model, krn_model, muw_model, mun_model, K, pc_model);

    //------------------------------------------------------------

    Real x = xc - (R + 0.5*smoothr)/xscale, y = yc, t = tc;
    Real r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    Real dpndr2 = evalPolyFlatModelFactor(nParam, r, Rw, R, pn, pB);
    Real weight = 0.0;

    ArrayQ sourceTrue, sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*rhow*lambda_w*sqrt(Kxx*Kyy)*dpndr2;
    sourceTrue[1] = weight*volscale*rhon*lambda_n*sqrt(Kxx*Kyy)*dpndr2;

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc, y = yc, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    dpndr2 = evalPolyFlatModelFactor(nParam, r, Rw, R, pn, pB);
    weight = 1.0;

    sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*rhow*lambda_w*sqrt(Kxx*Kyy)*dpndr2; //special case of r = 0
    sourceTrue[1] = weight*volscale*rhon*lambda_n*sqrt(Kxx*Kyy)*dpndr2; //special case of r = 0

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc + R/xscale, y = yc, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    dpndr2 = evalPolyFlatModelFactor(nParam, r, Rw, R, pn, pB);
    weight = 0.5;

    sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*rhow*lambda_w*sqrt(Kxx*Kyy)*dpndr2;
    sourceTrue[1] = weight*volscale*rhon*lambda_n*sqrt(Kxx*Kyy)*dpndr2;

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc + 0.5*R/xscale, y = yc - 0.5*R/yscale, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    dpndr2 = evalPolyFlatModelFactor(nParam, r, Rw, R, pn, pB);
    weight = 1.0;

    sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*rhow*lambda_w*sqrt(Kxx*Kyy)*dpndr2;
    sourceTrue[1] = weight*volscale*rhon*lambda_n*sqrt(Kxx*Kyy)*dpndr2;

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Source2D_FixedPressureOutflow_Massflow_eval )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef QTypePrimitive_pnSw QType;
  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel,
                              CapillaryModel> TraitsModelClass;

  typedef SourceTwoPhase2D_FixedPressureOutflow<TraitsSizeTwoPhase, TraitsModelClass> SourceClass;
  typedef SourceClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  const Real pref = 1.47;
  const Real pB = 15.82;
  const Real Rw = 0.5;

  const Real rhow_ref = 62.4;
  const Real rhon_ref = 52.1;
  const Real Cw = 0.03;
  const Real Cn = 0.05;

  const Real muw = 1.0, mun = 2.0;
  const Real pcmax = 5.0;

  DensityModel rhow_model(rhow_ref, Cw, pref);
  DensityModel rhon_model(rhon_ref, Cn, pref);

  RelPermModel krw_model(2);
  RelPermModel krn_model(2);

  ViscModel muw_model(muw);
  ViscModel mun_model(mun);

  Real Kxx = 1.2, Kxy = 0.0, Kyx = 0.0, Kyy = 0.3; //matrix has to be diagonal
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc_model(pcmax);

  // Function tests
  Real Sw = 0.375;
  Real Swx = -0.63, Swy = 0.27;
  Real pn = 25.34;
  Real pnx = -0.52, pny = 0.81;

  Real Sn = 1.0 - Sw;
  Real pc = pcmax*Sn;
  Real pw = pn - pc;

  Real krw = pow(Sw,2);
  Real krn = pow(Sn,2);

  Real rhow = rhow_ref*exp(Cw*(pw - pref));
  Real rhon = rhon_ref*exp(Cn*(pn - pref));

  Real lambda_w = krw/muw;
  Real lambda_n = krn/mun;

  ArrayQ q = {pn,Sw};
  ArrayQ qx = {pnx, Swx};
  ArrayQ qy = {pny, Swy};

  //Coordinate transform parameters
  Real xscale = pow(Kyy/Kxx, 0.25);
  Real yscale = pow(Kxx/Kyy, 0.25);
  Real savg = 0.5*(xscale + yscale);
  xscale /= savg;
  yscale /= savg;
  Real volscale = xscale*yscale;

  //Gaussian well model
  for (int nParam = 1; nParam <= 3; nParam += 2)
  {
    // Set up PyDicts
    PyDict fixedwellpressure;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.pB] = pB;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.Rwellbore] = Rw;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.nParam] = nParam;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.WellModel] = "Gaussian";
    fixedwellpressure[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureOutflow;

    Real xc = 10.0, yc = 15.0, tc = 2.0;
    Real R = 5.0;
    Real smoothr = 1.0;
    Real smoothT = 0.2;

    PyDict well;
    well[SourceTwoPhase2DParam::params.Source] = fixedwellpressure;
    well[SourceTwoPhase2DParam::params.xcentroid] = xc;
    well[SourceTwoPhase2DParam::params.ycentroid] = yc;
    well[SourceTwoPhase2DParam::params.R] = R;
    well[SourceTwoPhase2DParam::params.Tmin] = tc - 1.0;
    well[SourceTwoPhase2DParam::params.Tmax] = tc + 1.0;
    well[SourceTwoPhase2DParam::params.smoothLr] = smoothr;
    well[SourceTwoPhase2DParam::params.smoothT] = smoothT;
    well[SourceTwoPhase2DParam::params.visibleAngle] = 2*PI;

    SourceTwoPhase2DParam::checkInputs(well);

    SourceClass source(well, rhow_model, rhon_model, krw_model, krn_model, muw_model, mun_model, K, pc_model, true);

    //------------------------------------------------------------

    Real x = xc - (R + 0.5*smoothr)/xscale, y = yc, t = tc;
    Real r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    Real dpndr2 = evalGaussianModelFactor(nParam, r, Rw, R, pn, pB);
    Real weight = 0.0;

    ArrayQ sourceTrue, sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*rhow*lambda_w*sqrt(Kxx*Kyy)*dpndr2;
    sourceTrue[1] = weight*volscale*rhon*lambda_n*sqrt(Kxx*Kyy)*dpndr2;

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc, y = yc, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    dpndr2 = evalGaussianModelFactor(nParam, r, Rw, R, pn, pB);
    weight = 1.0;

    sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*rhow*lambda_w*sqrt(Kxx*Kyy)*dpndr2; //special case of r = 0
    sourceTrue[1] = weight*volscale*rhon*lambda_n*sqrt(Kxx*Kyy)*dpndr2; //special case of r = 0

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc + R/xscale, y = yc, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    dpndr2 = evalGaussianModelFactor(nParam, r, Rw, R, pn, pB);
    weight = 0.5;

    sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*rhow*lambda_w*sqrt(Kxx*Kyy)*dpndr2;
    sourceTrue[1] = weight*volscale*rhon*lambda_n*sqrt(Kxx*Kyy)*dpndr2;

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc + 0.5*R/xscale, y = yc - 0.5*R/yscale, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    dpndr2 = evalGaussianModelFactor(nParam, r, Rw, R, pn, pB);
    weight = 1.0;

    sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*rhow*lambda_w*sqrt(Kxx*Kyy)*dpndr2;
    sourceTrue[1] = weight*volscale*rhon*lambda_n*sqrt(Kxx*Kyy)*dpndr2;

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);
  }

  //Polynomial well model - with zero derivatives at origin
  for (int nParam = 1; nParam <= 6; nParam++)
  {
    // Set up PyDicts
    PyDict fixedwellpressure;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.pB] = pB;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.Rwellbore] = Rw;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.nParam] = nParam;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.WellModel] = "PolynomialFlat";
    fixedwellpressure[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureOutflow;

    Real xc = 10.0, yc = 15.0, tc = 2.0;
    Real R = 5.0;
    Real smoothr = 1.0;
    Real smoothT = 0.2;

    PyDict well;
    well[SourceTwoPhase2DParam::params.Source] = fixedwellpressure;
    well[SourceTwoPhase2DParam::params.xcentroid] = xc;
    well[SourceTwoPhase2DParam::params.ycentroid] = yc;
    well[SourceTwoPhase2DParam::params.R] = R;
    well[SourceTwoPhase2DParam::params.Tmin] = tc - 1.0;
    well[SourceTwoPhase2DParam::params.Tmax] = tc + 1.0;
    well[SourceTwoPhase2DParam::params.smoothLr] = smoothr;
    well[SourceTwoPhase2DParam::params.smoothT] = smoothT;
    well[SourceTwoPhase2DParam::params.visibleAngle] = 2*PI;

    SourceTwoPhase2DParam::checkInputs(well);

    SourceClass source(well, rhow_model, rhon_model, krw_model, krn_model, muw_model, mun_model, K, pc_model, true);

    //------------------------------------------------------------

    Real x = xc - (R + 0.5*smoothr)/xscale, y = yc, t = tc;
    Real r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    Real dpndr2 = evalPolyFlatModelFactor(nParam, r, Rw, R, pn, pB);
    Real weight = 0.0;

    ArrayQ sourceTrue, sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*rhow*lambda_w*sqrt(Kxx*Kyy)*dpndr2;
    sourceTrue[1] = weight*volscale*rhon*lambda_n*sqrt(Kxx*Kyy)*dpndr2;

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc, y = yc, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    dpndr2 = evalPolyFlatModelFactor(nParam, r, Rw, R, pn, pB);
    weight = 1.0;

    sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*rhow*lambda_w*sqrt(Kxx*Kyy)*dpndr2; //special case of r = 0
    sourceTrue[1] = weight*volscale*rhon*lambda_n*sqrt(Kxx*Kyy)*dpndr2; //special case of r = 0

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc + R/xscale, y = yc, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    dpndr2 = evalPolyFlatModelFactor(nParam, r, Rw, R, pn, pB);
    weight = 0.5;

    sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*rhow*lambda_w*sqrt(Kxx*Kyy)*dpndr2;
    sourceTrue[1] = weight*volscale*rhon*lambda_n*sqrt(Kxx*Kyy)*dpndr2;

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc + 0.5*R/xscale, y = yc - 0.5*R/yscale, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    dpndr2 = evalPolyFlatModelFactor(nParam, r, Rw, R, pn, pB);
    weight = 1.0;

    sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*rhow*lambda_w*sqrt(Kxx*Kyy)*dpndr2;
    sourceTrue[1] = weight*volscale*rhon*lambda_n*sqrt(Kxx*Kyy)*dpndr2;

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);
  }

  //Polynomial well model - with free second derivative at origin
  for (int nParam = 1; nParam <= 7; nParam++)
  {
    // Set up PyDicts
    PyDict fixedwellpressure;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.pB] = pB;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.Rwellbore] = Rw;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.nParam] = nParam;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.WellModel] = "Polynomial";
    fixedwellpressure[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureOutflow;

    Real xc = 10.0, yc = 15.0, tc = 2.0;
    Real R = 5.0;
    Real smoothr = 1.0;
    Real smoothT = 0.2;

    PyDict well;
    well[SourceTwoPhase2DParam::params.Source] = fixedwellpressure;
    well[SourceTwoPhase2DParam::params.xcentroid] = xc;
    well[SourceTwoPhase2DParam::params.ycentroid] = yc;
    well[SourceTwoPhase2DParam::params.R] = R;
    well[SourceTwoPhase2DParam::params.Tmin] = tc - 1.0;
    well[SourceTwoPhase2DParam::params.Tmax] = tc + 1.0;
    well[SourceTwoPhase2DParam::params.smoothLr] = smoothr;
    well[SourceTwoPhase2DParam::params.smoothT] = smoothT;
    well[SourceTwoPhase2DParam::params.visibleAngle] = 2*PI;

    SourceTwoPhase2DParam::checkInputs(well);

    SourceClass source(well, rhow_model, rhon_model, krw_model, krn_model, muw_model, mun_model, K, pc_model, true);

    //------------------------------------------------------------

    Real x = xc - (R + 0.5*smoothr)/xscale, y = yc, t = tc;
    Real r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    Real dpndr2 = evalPolyModelFactor(nParam, r, Rw, R, pn, pB);
    Real weight = 0.0;

    ArrayQ sourceTrue, sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*rhow*lambda_w*sqrt(Kxx*Kyy)*dpndr2;
    sourceTrue[1] = weight*volscale*rhon*lambda_n*sqrt(Kxx*Kyy)*dpndr2;

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc, y = yc, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    dpndr2 = evalPolyModelFactor(nParam, r, Rw, R, pn, pB);
    weight = 1.0;

    sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*rhow*lambda_w*sqrt(Kxx*Kyy)*dpndr2; //special case of r = 0
    sourceTrue[1] = weight*volscale*rhon*lambda_n*sqrt(Kxx*Kyy)*dpndr2; //special case of r = 0

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc + R/xscale, y = yc, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    dpndr2 = evalPolyModelFactor(nParam, r, Rw, R, pn, pB);
    weight = 0.5;

    sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*rhow*lambda_w*sqrt(Kxx*Kyy)*dpndr2;
    sourceTrue[1] = weight*volscale*rhon*lambda_n*sqrt(Kxx*Kyy)*dpndr2;

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc + 0.5*R/xscale, y = yc - 0.5*R/yscale, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    dpndr2 = evalPolyModelFactor(nParam, r, Rw, R, pn, pB);
    weight = 1.0;

    sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*rhow*lambda_w*sqrt(Kxx*Kyy)*dpndr2;
    sourceTrue[1] = weight*volscale*rhon*lambda_n*sqrt(Kxx*Kyy)*dpndr2;

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Source2D_FixedPressureOutflow_Gaussian_Volumetricflow_eval )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef QTypePrimitive_pnSw QType;
  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel,
                              CapillaryModel> TraitsModelClass;

  typedef SourceTwoPhase2D_FixedPressureOutflow<TraitsSizeTwoPhase, TraitsModelClass> SourceClass;
  typedef SourceClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  const Real pref = 1.47;
  const Real pB = 15.82;
  const Real Rw = 0.5;

  const Real rhow_ref = 62.4;
  const Real rhon_ref = 52.1;
  const Real Cw = 0.03;
  const Real Cn = 0.05;

  const Real muw = 1.0, mun = 2.0;
  const Real pcmax = 5.0;

  DensityModel rhow_model(rhow_ref, Cw, pref);
  DensityModel rhon_model(rhon_ref, Cn, pref);

  RelPermModel krw_model(2);
  RelPermModel krn_model(2);

  ViscModel muw_model(muw);
  ViscModel mun_model(mun);

  Real Kxx = 1.2, Kxy = 0.0, Kyx = 0.0, Kyy = 0.3; //matrix has to be diagonal
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc_model(pcmax);

  // Function tests
  Real Sw = 0.375;
  Real Swx = -0.63, Swy = 0.27;
  Real pn = 25.34;
  Real pnx = -0.52, pny = 0.81;
  Real Sn = 1.0 - Sw;

  Real krw = pow(Sw,2);
  Real krn = pow(Sn,2);

  Real lambda_w = krw/muw;
  Real lambda_n = krn/mun;

  ArrayQ q = {pn,Sw};
  ArrayQ qx = {pnx, Swx};
  ArrayQ qy = {pny, Swy};

  //Coordinate transform parameters
  Real xscale = pow(Kyy/Kxx, 0.25);
  Real yscale = pow(Kxx/Kyy, 0.25);
  Real savg = 0.5*(xscale + yscale);
  xscale /= savg;
  yscale /= savg;
  Real volscale = xscale*yscale;

  for (int nParam = 1; nParam <= 3; nParam += 2)
  {
    // Set up PyDicts
    PyDict fixedwellpressure;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.pB] = pB;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.Rwellbore] = Rw;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.nParam] = nParam;
    fixedwellpressure[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.WellModel] = "Gaussian";
    fixedwellpressure[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureOutflow;

    Real xc = 10.0, yc = 15.0, tc = 2.0;
    Real R = 5.0;
    Real smoothr = 1.0;
    Real smoothT = 0.2;

    PyDict well;
    well[SourceTwoPhase2DParam::params.Source] = fixedwellpressure;
    well[SourceTwoPhase2DParam::params.xcentroid] = xc;
    well[SourceTwoPhase2DParam::params.ycentroid] = yc;
    well[SourceTwoPhase2DParam::params.R] = R;
    well[SourceTwoPhase2DParam::params.Tmin] = tc - 1.0;
    well[SourceTwoPhase2DParam::params.Tmax] = tc + 1.0;
    well[SourceTwoPhase2DParam::params.smoothLr] = smoothr;
    well[SourceTwoPhase2DParam::params.smoothT] = smoothT;
    well[SourceTwoPhase2DParam::params.visibleAngle] = 2*PI;

    SourceTwoPhase2DParam::checkInputs(well);

    SourceClass source(well, rhow_model, rhon_model, krw_model, krn_model, muw_model, mun_model, K, pc_model, false);

    //------------------------------------------------------------

    Real x = xc - (R + 0.5*smoothr)/xscale, y = yc, t = tc;
    Real r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    Real dpndr2 = evalGaussianModelFactor(nParam, r, Rw, R, pn, pB);
    Real weight = 0.0;

    ArrayQ sourceTrue, sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*lambda_w*sqrt(Kxx*Kyy)*dpndr2;
    sourceTrue[1] = weight*volscale*lambda_n*sqrt(Kxx*Kyy)*dpndr2;

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc, y = yc, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    dpndr2 = evalGaussianModelFactor(nParam, r, Rw, R, pn, pB);
    weight = 1.0;

    sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*lambda_w*sqrt(Kxx*Kyy)*dpndr2; //special case of r = 0
    sourceTrue[1] = weight*volscale*lambda_n*sqrt(Kxx*Kyy)*dpndr2; //special case of r = 0

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc + R/xscale, y = yc, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    dpndr2 = evalGaussianModelFactor(nParam, r, Rw, R, pn, pB);
    weight = 0.5;

    sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*lambda_w*sqrt(Kxx*Kyy)*dpndr2;
    sourceTrue[1] = weight*volscale*lambda_n*sqrt(Kxx*Kyy)*dpndr2;

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc + 0.5*R/xscale, y = yc - 0.5*R/yscale, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    dpndr2 = evalGaussianModelFactor(nParam, r, Rw, R, pn, pB);
    weight = 1.0;

    sourceVal = 0.0;
    sourceTrue[0] = weight*volscale*lambda_w*sqrt(Kxx*Kyy)*dpndr2;
    sourceTrue[1] = weight*volscale*lambda_n*sqrt(Kxx*Kyy)*dpndr2;

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Source2D_FixedInflowRate_eval )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef QTypePrimitive_pnSw QType;
  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel,
                              CapillaryModel> TraitsModelClass;

  typedef SourceTwoPhase2D_FixedInflowRate<TraitsSizeTwoPhase, TraitsModelClass> SourceClass;
  typedef SourceClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  const Real pref = 1.47;
  const Real SwIn = 0.9;

  const Real rhow_ref = 62.4;
  const Real rhon_ref = 52.1;
  const Real Cw = 0.03;
  const Real Cn = 0.05;

  const Real muw = 1.0, mun = 2.0;
  const Real pcmax = 5.0;

  DensityModel rhow_model(rhow_ref, Cw, pref);
  DensityModel rhon_model(rhon_ref, Cn, pref);

  RelPermModel krw_model(2);
  RelPermModel krn_model(2);

  ViscModel muw_model(muw);
  ViscModel mun_model(mun);

  Real Kxx = 1.2, Kxy = 0.0, Kyx = 0.0, Kyy = 0.3; //matrix has to be diagonal
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc_model(pcmax);

  Real Q = 0.6;

  // Function tests
  Real Sw = SwIn;
  Real Swx = -0.63, Swy = 0.27;
  Real pn = 10.34;
  Real pnx = -0.52, pny = 0.81;

  Real Sn = 1.0 - Sw;
  Real pc = pcmax*Sn;
  Real pw = pn - pc;

  Real krw = pow(Sw,2);
  Real krn = pow(Sn,2);

  Real rhow = rhow_ref*exp(Cw*(pw - pref));
  Real rhon = rhon_ref*exp(Cn*(pn - pref));

  Real lambda_w = krw/muw;
  Real lambda_n = krn/mun;
  Real lambda_t = lambda_w + lambda_n;

  Sw = 0.375;
  ArrayQ q = {pn, Sw};
  ArrayQ qx = {pnx, Swx};
  ArrayQ qy = {pny, Swy};

  //Coordinate transform parameters
  Real xscale = pow(Kyy/Kxx, 0.25);
  Real yscale = pow(Kxx/Kyy, 0.25);
  Real savg = 0.5*(xscale + yscale);
  xscale /= savg;
  yscale /= savg;
  Real volscale = xscale*yscale;

  for (int nParam = 1; nParam <= 7; nParam++)
  {
    // Set up PyDicts
    PyDict fixedrate;
    fixedrate[SourceTwoPhase2DType_FixedInflowRate_Param::params.Q] = Q;
    fixedrate[SourceTwoPhase2DType_FixedInflowRate_Param::params.Sw] = SwIn;
    fixedrate[SourceTwoPhase2DType_FixedInflowRate_Param::params.nParam] = nParam;
    fixedrate[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedInflowRate;

    Real xc = 10.0, yc = 15.0, tc = 2.0;
    Real R = 5.0;
    Real Rw = 0.5;
    Real smoothr = 1.0;
    Real smoothT = 0.2;

    std::vector<Real> coeff = wellActivationFunctionPolyCoeff(nParam);

    PyDict well;
    well[SourceTwoPhase2DParam::params.Source] = fixedrate;
    well[SourceTwoPhase2DParam::params.xcentroid] = xc;
    well[SourceTwoPhase2DParam::params.ycentroid] = yc;
    well[SourceTwoPhase2DParam::params.R] = R;
    well[SourceTwoPhase2DParam::params.Tmin] = tc - 1.0;
    well[SourceTwoPhase2DParam::params.Tmax] = tc + 1.0;
    well[SourceTwoPhase2DParam::params.smoothLr] = smoothr;
    well[SourceTwoPhase2DParam::params.smoothT] = smoothT;
    well[SourceTwoPhase2DParam::params.visibleAngle] = 2*PI;

    SourceTwoPhase2DParam::checkInputs(well);

    SourceClass source(well, rhow_model, rhon_model, krw_model, krn_model, muw_model, mun_model, K, pc_model);

    //------------------------------------------------------------

    Real x = xc - (R + 0.5*smoothr)/xscale, y = yc, t = tc;
    Real r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    Real weight = 0.0;
    Real f, qhat;
    evalPolyModelFixedRateSource(nParam, r, R, f, qhat);

    ArrayQ sourceTrue, sourceVal = 0.0;
    sourceTrue[0] = -weight*rhow*(lambda_w/lambda_t)*Q*qhat*volscale/(2*PI*R*R);
    sourceTrue[1] = -weight*rhon*(lambda_n/lambda_t)*Q*qhat*volscale/(2*PI*R*R);

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    DLA::VectorS<2,Real> bhpIntTrue, bhpInt = 0.0;
    Real tmp = lambda_t * sqrt(Kxx*Kyy) * volscale / ((f + log(R/Rw))*R*R) * qhat;
    bhpIntTrue[0] = tmp*pn;
    bhpIntTrue[1] = tmp;

    source.bottomHolePressureIntegrand(x, y, t, q, Rw, bhpInt);
    SANS_CHECK_CLOSE( bhpIntTrue[0], bhpInt[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( bhpIntTrue[1], bhpInt[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc, y = yc, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    weight = 1.0;
    evalPolyModelFixedRateSource(nParam, r, R, f, qhat);

    sourceVal = 0.0;
    sourceTrue[0] = -weight*rhow*(lambda_w/lambda_t)*Q*qhat*volscale/(2*PI*R*R);
    sourceTrue[1] = -weight*rhon*(lambda_n/lambda_t)*Q*qhat*volscale/(2*PI*R*R);

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    bhpInt = 0.0;
    tmp = lambda_t * sqrt(Kxx*Kyy) * volscale / ((f + log(R/Rw))*R*R) * qhat;
    bhpIntTrue[0] = tmp*pn;
    bhpIntTrue[1] = tmp;

    source.bottomHolePressureIntegrand(x, y, t, q, Rw, bhpInt);
    SANS_CHECK_CLOSE( bhpIntTrue[0], bhpInt[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( bhpIntTrue[1], bhpInt[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc + R/xscale, y = yc, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    weight = 0.5;
    evalPolyModelFixedRateSource(nParam, r, R, f, qhat);

    sourceVal = 0.0;
    sourceTrue[0] = -weight*rhow*(lambda_w/lambda_t)*Q*qhat*volscale/(2*PI*R*R);
    sourceTrue[1] = -weight*rhon*(lambda_n/lambda_t)*Q*qhat*volscale/(2*PI*R*R);

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    bhpInt = 0.0;
    tmp = lambda_t * sqrt(Kxx*Kyy) * volscale / ((f + log(R/Rw))*R*R) * qhat;
    bhpIntTrue[0] = tmp*pn;
    bhpIntTrue[1] = tmp;

    source.bottomHolePressureIntegrand(x, y, t, q, Rw, bhpInt);
    SANS_CHECK_CLOSE( bhpIntTrue[0], bhpInt[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( bhpIntTrue[1], bhpInt[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc + 0.5*R/xscale, y = yc - 0.5*R/yscale, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    weight = 1.0;
    evalPolyModelFixedRateSource(nParam, r, R, f, qhat);

    sourceVal = 0.0;
    sourceTrue[0] = -weight*rhow*(lambda_w/lambda_t)*Q*qhat*volscale/(2*PI*R*R);
    sourceTrue[1] = -weight*rhon*(lambda_n/lambda_t)*Q*qhat*volscale/(2*PI*R*R);

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    bhpInt = 0.0;
    tmp = lambda_t * sqrt(Kxx*Kyy) * volscale / ((f + log(R/Rw))*R*R) * qhat;
    bhpIntTrue[0] = tmp*pn;
    bhpIntTrue[1] = tmp;

    source.bottomHolePressureIntegrand(x, y, t, q, Rw, bhpInt);
    SANS_CHECK_CLOSE( bhpIntTrue[0], bhpInt[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( bhpIntTrue[1], bhpInt[1], small_tol, close_tol);

  } //nParam loop
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Source2D_FixedOutflowRate_eval )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef QTypePrimitive_pnSw QType;
  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel,
                              CapillaryModel> TraitsModelClass;

  typedef SourceTwoPhase2D_FixedOutflowRate<TraitsSizeTwoPhase, TraitsModelClass> SourceClass;
  typedef SourceClass::ArrayQ<Real> ArrayQ;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  const Real pref = 1.47;

  const Real rhow_ref = 62.4;
  const Real rhon_ref = 52.1;
  const Real Cw = 0.03;
  const Real Cn = 0.05;

  const Real muw = 1.0, mun = 2.0;
  const Real pcmax = 5.0;

  DensityModel rhow_model(rhow_ref, Cw, pref);
  DensityModel rhon_model(rhon_ref, Cn, pref);

  RelPermModel krw_model(2);
  RelPermModel krn_model(2);

  ViscModel muw_model(muw);
  ViscModel mun_model(mun);

  Real Kxx = 1.2, Kxy = 0.0, Kyx = 0.0, Kyy = 0.3; //matrix has to be diagonal
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc_model(pcmax);

  Real Q = 0.6;

  // Function tests
  Real Sw = 0.375;
  Real Swx = -0.63, Swy = 0.27;
  Real pn = 25.34;
  Real pnx = -0.52, pny = 0.81;

  Real Sn = 1.0 - Sw;
  Real pc = pcmax*Sn;
  Real pw = pn - pc;

  Real krw = pow(Sw,2);
  Real krn = pow(Sn,2);

  Real rhow = rhow_ref*exp(Cw*(pw - pref));
  Real rhon = rhon_ref*exp(Cn*(pn - pref));

  Real lambda_w = krw/muw;
  Real lambda_n = krn/mun;
  Real lambda_t = lambda_w + lambda_n;

  ArrayQ q = {pn, Sw};
  ArrayQ qx = {pnx, Swx};
  ArrayQ qy = {pny, Swy};

  //Coordinate transform parameters
  Real xscale = pow(Kyy/Kxx, 0.25);
  Real yscale = pow(Kxx/Kyy, 0.25);
  Real savg = 0.5*(xscale + yscale);
  xscale /= savg;
  yscale /= savg;
  Real volscale = xscale*yscale;

  for (int nParam = 1; nParam <= 7; nParam++)
  {
    // Set up PyDicts
    PyDict fixedrate;
    fixedrate[SourceTwoPhase2DType_FixedOutflowRate_Param::params.Q] = Q;
    fixedrate[SourceTwoPhase2DType_FixedOutflowRate_Param::params.nParam] = nParam;
    fixedrate[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedOutflowRate;

    Real xc = 10.0, yc = 15.0, tc = 2.0;
    Real R = 5.0;
    Real Rw = 0.5;
    Real smoothr = 1.0;
    Real smoothT = 0.2;

    PyDict well;
    well[SourceTwoPhase2DParam::params.Source] = fixedrate;
    well[SourceTwoPhase2DParam::params.xcentroid] = xc;
    well[SourceTwoPhase2DParam::params.ycentroid] = yc;
    well[SourceTwoPhase2DParam::params.R] = R;
    well[SourceTwoPhase2DParam::params.Tmin] = tc - 1.0;
    well[SourceTwoPhase2DParam::params.Tmax] = tc + 1.0;
    well[SourceTwoPhase2DParam::params.smoothLr] = smoothr;
    well[SourceTwoPhase2DParam::params.smoothT] = smoothT;
    well[SourceTwoPhase2DParam::params.visibleAngle] = 2*PI;

    SourceTwoPhase2DParam::checkInputs(well);

    SourceClass source(well, rhow_model, rhon_model, krw_model, krn_model, muw_model, mun_model, K, pc_model);

    //------------------------------------------------------------

    Real x = xc - (R + 0.5*smoothr)/xscale, y = yc, t = tc;
    Real r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    Real weight = 0.0;
    Real f, qhat;
    evalPolyModelFixedRateSource(nParam, r, R, f, qhat);

    ArrayQ sourceTrue, sourceVal = 0.0;
    sourceTrue[0] = weight*rhow*(lambda_w/lambda_t)*Q*qhat*volscale/(2*PI*R*R);
    sourceTrue[1] = weight*rhon*(lambda_n/lambda_t)*Q*qhat*volscale/(2*PI*R*R);

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    DLA::VectorS<2,Real> bhpIntTrue, bhpInt = 0.0;
    Real tmp = lambda_t * sqrt(Kxx*Kyy) * volscale / ((f + log(R/Rw))*R*R) * qhat;
    bhpIntTrue[0] = tmp*pn;
    bhpIntTrue[1] = tmp;

    source.bottomHolePressureIntegrand(x, y, t, q, Rw, bhpInt);
    SANS_CHECK_CLOSE( bhpIntTrue[0], bhpInt[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( bhpIntTrue[1], bhpInt[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc, y = yc, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    weight = 1.0;
    evalPolyModelFixedRateSource(nParam, r, R, f, qhat);

    sourceVal = 0.0;
    sourceTrue[0] = weight*rhow*(lambda_w/lambda_t)*Q*qhat*volscale/(2*PI*R*R);
    sourceTrue[1] = weight*rhon*(lambda_n/lambda_t)*Q*qhat*volscale/(2*PI*R*R);

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    bhpInt = 0.0;
    tmp = lambda_t * sqrt(Kxx*Kyy) * volscale / ((f + log(R/Rw))*R*R) * qhat;
    bhpIntTrue[0] = tmp*pn;
    bhpIntTrue[1] = tmp;

    source.bottomHolePressureIntegrand(x, y, t, q, Rw, bhpInt);
    SANS_CHECK_CLOSE( bhpIntTrue[0], bhpInt[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( bhpIntTrue[1], bhpInt[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc + R/xscale, y = yc, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    weight = 0.5;
    evalPolyModelFixedRateSource(nParam, r, R, f, qhat);

    sourceVal = 0.0;
    sourceTrue[0] = weight*rhow*(lambda_w/lambda_t)*Q*qhat*volscale/(2*PI*R*R);
    sourceTrue[1] = weight*rhon*(lambda_n/lambda_t)*Q*qhat*volscale/(2*PI*R*R);

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    bhpInt = 0.0;
    tmp = lambda_t * sqrt(Kxx*Kyy) * volscale / ((f + log(R/Rw))*R*R) * qhat;
    bhpIntTrue[0] = tmp*pn;
    bhpIntTrue[1] = tmp;

    source.bottomHolePressureIntegrand(x, y, t, q, Rw, bhpInt);
    SANS_CHECK_CLOSE( bhpIntTrue[0], bhpInt[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( bhpIntTrue[1], bhpInt[1], small_tol, close_tol);

    //------------------------------------------------------------

    x = xc + 0.5*R/xscale, y = yc - 0.5*R/yscale, t = tc;
    r = sqrt(pow(xscale*(x - xc), 2.0) +  pow(yscale*(y - yc), 2.0));
    weight = 1.0;
    evalPolyModelFixedRateSource(nParam, r, R, f, qhat);

    sourceVal = 0.0;
    sourceTrue[0] = weight*rhow*(lambda_w/lambda_t)*Q*qhat*volscale/(2*PI*R*R);
    sourceTrue[1] = weight*rhon*(lambda_n/lambda_t)*Q*qhat*volscale/(2*PI*R*R);

    source.source(x, y, t, q, qx, qy, sourceVal);
    SANS_CHECK_CLOSE( sourceTrue[0], sourceVal[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( sourceTrue[1], sourceVal[1], small_tol, close_tol);

    bhpInt = 0.0;
    tmp = lambda_t * sqrt(Kxx*Kyy) * volscale / ((f + log(R/Rw))*R*R) * qhat;
    bhpIntTrue[0] = tmp*pn;
    bhpIntTrue[1] = tmp;

    source.bottomHolePressureIntegrand(x, y, t, q, Rw, bhpInt);
    SANS_CHECK_CLOSE( bhpIntTrue[0], bhpInt[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( bhpIntTrue[1], bhpInt[1], small_tol, close_tol);

  } //nParam loop
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
