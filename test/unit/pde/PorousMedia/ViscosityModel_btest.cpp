// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ViscosityModel_btest
// testing of ViscosityModel class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/PorousMedia/ViscosityModel_Constant.h"

using namespace std;
using namespace SANS;

namespace SANS
{

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( ViscosityModel_Constant_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Constructor )
{
  const Real tol = 1.e-13;
  Real mu = 1.1;

  PyDict d;
  d[ViscosityModel_Constant_Params::params.mu] = mu;

  //Check dictionary
  ViscosityModel_Constant_Params::checkInputs(d);

  Real dummy_x = 0.0, dummy_y = 0.0, dummy_time = 0.0;

  // PyDict constructor
  ViscosityModel_Constant mu_model(d);
  BOOST_CHECK_CLOSE( mu_model.viscosity(dummy_x, dummy_time), mu, tol );
  BOOST_CHECK_CLOSE( mu_model.viscosity(dummy_x, dummy_y, dummy_time), mu, tol );

  // copy constructor
  ViscosityModel_Constant mu_model2(mu_model);
  BOOST_CHECK_CLOSE( mu_model2.viscosity(dummy_x, dummy_time), mu, tol );
  BOOST_CHECK_CLOSE( mu_model2.viscosity(dummy_x, dummy_y, dummy_time), mu, tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/ViscosityModel_Constant_pattern.txt", true );

  Real mu = 1.15;

  PyDict d;
  d[ViscosityModel_Constant_Params::params.mu] = mu;

  // PyDict constructor
  ViscosityModel_Constant mu_model(d);

  mu_model.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
