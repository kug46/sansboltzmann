// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCTwoPhaseArtificialViscosity2D_btest
//
// test of TwoPhase AV BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "pde/PorousMedia/Q2DPrimitive_pnSw.h"
#include "pde/PorousMedia/BCTwoPhaseArtificialViscosity2D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/PermeabilityModel2D.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsTwoPhaseArtificialViscosity.h"

#include "pde/ArtificialViscosity/AVVariable.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_Comp,
                            RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                            PermeabilityModel2D_Constant, CapillaryModel_Linear> TraitsModel_TwoPhase;

typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModel_TwoPhase> PDEBaseClass;

typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeTwoPhaseArtificialViscosity> SensorAdvectiveFlux;
typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeTwoPhaseArtificialViscosity> SensorViscousFlux;
typedef AVSensor_Source2D_TwoPhase<PDEBaseClass> SensorSource;

typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
typedef PDEmitAVSensor2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelAV> PDEClass;

template class BCmitAVSensor2D<BCTypeFlux_mitStateSpaceTime,
                               BCTwoPhase2D< BCTypeTimeIC, PDEClass >>;
template class BCmitAVSensor2D<BCTypeFlux_mitState,
                               BCTwoPhase2D< BCTypeFullState, PDEClass >>;
template class BCmitAVSensor2D<BCTypeFlux_mitState,
                               BCTwoPhase2D< BCTypeNoFlux, PDEClass >>;
template class BCmitAVSensor2D<BCTypeFlux_mitStateSpaceTime,
                               BCTwoPhase2D< BCTypeNoFluxSpaceTime, PDEClass >>;

// Instantiate BCParameters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters< BCTwoPhaseArtificialViscosity2DVector<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelAV> >;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BCTwoPhaseArtificialViscosity2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModel_TwoPhase> PDEClass;

  {
    typedef BCNone<PhysD2,TraitsSizeTwoPhaseArtificialViscosity<PhysD2>::N> BCClass;
    BOOST_CHECK( BCClass::D == 2 );
    BOOST_CHECK( BCClass::NBC == 0 );
  }

  {
    typedef BCmitAVSensor2D< BCTypeFlux_mitStateSpaceTime, BCTwoPhase2D< BCTypeTimeIC, PDEClass>> BCClass;
    BOOST_CHECK( BCClass::D == 2 );
    BOOST_CHECK( BCClass::N == 3 );
    BOOST_CHECK( BCClass::NBC == 3 );
  }

  {
    typedef BCmitAVSensor2D< BCTypeFlux_mitState, BCTwoPhase2D< BCTypeFullState, PDEClass>> BCClass;
    BOOST_CHECK( BCClass::D == 2 );
    BOOST_CHECK( BCClass::N == 3 );
    BOOST_CHECK( BCClass::NBC == 3 );
  }

  {
    typedef BCmitAVSensor2D< BCTypeFlux_mitState, BCTwoPhase2D< BCTypeNoFlux, PDEClass>> BCClass;
    BOOST_CHECK( BCClass::D == 2 );
    BOOST_CHECK( BCClass::N == 3 );
    BOOST_CHECK( BCClass::NBC == 3 );
  }

  {
    typedef BCmitAVSensor2D< BCTypeFlux_mitStateSpaceTime, BCTwoPhase2D< BCTypeNoFluxSpaceTime, PDEClass>> BCClass;
    BOOST_CHECK( BCClass::D == 2 );
    BOOST_CHECK( BCClass::N == 3 );
    BOOST_CHECK( BCClass::NBC == 3 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel,
                              RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelClass> PDEClass;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  Real Kxx = 1.1, Kxy = 0.6, Kyx = 0.2, Kyy = 1.5;
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc(5);

  const int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEClass pdeAV(order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCParameters< BCTwoPhaseArtificialViscosity2DVector<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelAV> > BCParams;

  PyDict BCList;

  {
    typedef BCNone<PhysD2,TraitsSizeTwoPhaseArtificialViscosity<PhysD2>::N> BCClass;

    //Pydict constructor
    PyDict BCNone;
    BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;
    BCClass bc0(pdeAV, BCNone);

    BCList["BC0"] = BCNone;
  }

  {
    typedef BCmitAVSensor2D<BCTypeFlux_mitStateSpaceTime, BCTwoPhase2D< BCTypeTimeIC, PDEClass>> BCClass;

    PressureNonWet_SaturationWet<Real> qdata(2534.5, 0.738);
    Real Cdiff = 3.0;
    BCClass bc0(Cdiff, pdeAV, qdata);

    PyDict d;
    d[TwoPhaseVariableTypeParams::params.StateVector.Variables] = TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
    d[PressureNonWet_SaturationWet_Params::params.pn] = 2534.5;
    d[PressureNonWet_SaturationWet_Params::params.Sw] = 0.738;

    PyDict BCTimeIC;
    BCTimeIC[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC;
    BCTimeIC[BCClass::ParamsType::params.StateVector] = d;
    BCTimeIC[BCClass::ParamsType::params.Cdiff] = Cdiff;
    BCClass bc1(pdeAV, BCTimeIC);

    BCList["BC1"] = BCTimeIC;
  }

  {
    typedef BCmitAVSensor2D<BCTypeFlux_mitState, BCTwoPhase2D< BCTypeFullState, PDEClass>> BCClass;

    PressureNonWet_SaturationWet<Real> qdata(2534.5, 0.738);
    Real Cdiff = 3.0;
    BCClass bc0(Cdiff, pdeAV, qdata);

    PyDict d;
    d[TwoPhaseVariableTypeParams::params.StateVector.Variables] = TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
    d[PressureNonWet_SaturationWet_Params::params.pn] = 2534.5;
    d[PressureNonWet_SaturationWet_Params::params.Sw] = 0.738;

    PyDict BCFullState;
    BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
    BCFullState[BCClass::ParamsType::params.StateVector] = d;
    BCFullState[BCClass::ParamsType::params.Cdiff] = Cdiff;
    BCClass bc1(pdeAV, BCFullState);

    BCList["BC2"] = BCFullState;
  }

  {
    typedef BCmitAVSensor2D<BCTypeFlux_mitState, BCTwoPhase2D< BCTypeNoFlux, PDEClass>> BCClass;

    Real Cdiff = 3.0;
    BCClass bc0(Cdiff, pdeAV);

    //Pydict constructor
    PyDict BCNoFlux;
    BCNoFlux[BCParams::params.BC.BCType] = BCParams::params.BC.NoFlux;
    BCNoFlux[BCClass::ParamsType::params.Cdiff] = Cdiff;
    BCClass bc1(pdeAV, BCNoFlux);

    BCList["BC3"] = BCNoFlux;
  }

  {
    typedef BCmitAVSensor2D<BCTypeFlux_mitStateSpaceTime, BCTwoPhase2D< BCTypeNoFluxSpaceTime, PDEClass>> BCClass;

    Real Cdiff = 3.0;
    BCClass bc0(Cdiff, pdeAV);

    //Pydict constructor
    PyDict BCNoFlux;
    BCNoFlux[BCParams::params.BC.BCType] = BCParams::params.BC.NoFluxSpaceTime;
    BCNoFlux[BCClass::ParamsType::params.Cdiff] = Cdiff;
    BCClass bc1(pdeAV, BCNoFlux);

    BCList["BC4"] = BCNoFlux;
  }

  //No exceptions should be thrown
  BCParams::checkInputs(BCList);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCNone_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel,
                              RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelClass> PDEClass;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  Real Kxx = 1.1, Kxy = 0.6, Kyx = 0.2, Kyy = 1.5;
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc(5);

  const int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEClass pdeAV(order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCNone<PhysD2,TraitsSizeTwoPhaseArtificialViscosity<PhysD2>::N> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  BCClass bc;
  ArrayQ qI = {2500, 0.8, 0.1};
  Real nx = 2, ny = 3;
  BOOST_CHECK(bc.isValidState(nx, ny, qI) == true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeTimeIC_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel,
                              RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelClass> PDEClass;

  typedef DLA::MatrixSymS<3, Real> ParamType;

  const Real tol = 5.0e-12;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 1e-5, pref);
  DensityModel rhon(52.1, 1e-5, pref);

  PorosityModel phi(0.3, 1e-5, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  Real Kxx = 1.1, Kxy = 0.6, Kyx = 0.2, Kyy = 1.5;
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc(5);

  const int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEClass pdeAV(order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCmitAVSensor2D<BCTypeFlux_mitStateSpaceTime, BCTwoPhase2D< BCTypeTimeIC, PDEClass>> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  Real pnB = 2534.5;
  Real SwB = 0.738;
  PressureNonWet_SaturationWet<Real> qdata(pnB, SwB);
  Real Cdiff = 3.0;
  BCClass bc(Cdiff, pdeAV, qdata);

  Real x = 0, y = 0;
  Real time = 0;
  Real nx = 0.8, ny = -0.2, nt = 0.3;

  ParamType H = {{0.1},{0.05, 0.3},{0.02, 0.06, 0.4}}; // grid spacing
  ParamType param = log(H);

  ArrayQ qI = {2500, 0.8, 0.1};
  ArrayQ qIx = {1.35, -0.05, 0.23};
  ArrayQ qIy = {-0.21, 0.85,-0.41};
  ArrayQ qIt = qIx;
  ArrayQ qB = 0;
  bc.state(param, x, y, time, nx, ny, nt, qI, qB);

  BOOST_CHECK_CLOSE( pnB, qB[0], tol );
  BOOST_CHECK_CLOSE( SwB, qB[1], tol );
  BOOST_CHECK_CLOSE( qI[2], qB[2], tol );

  ArrayQ uB = 0;
  pdeAV.masterState(param, x, y, time, qB, uB);
  ArrayQ Fn_true = uB*nt;

  ArrayQ fvx = 0, fvy = 0, fvt = 0;
  pdeAV.fluxViscousSpaceTime(param, x, y, time, qB, qIx, qIy, qIt, fvx, fvy, fvt);
  Fn_true += fvx*nx + fvy*ny + fvt*nt;

  DLA::VectorS<3, Real> nvec = {nx, ny, nt};
  Real hn = dot(nvec, H*nvec);
  Fn_true[2] = sqrt(Cdiff)*hn*(qI(2) - 0.0);

  ArrayQ Fn = 0.1;
  bc.fluxNormalSpaceTime(param, x, y, time, nx, ny, nt, qI, qIx, qIy, qIt, qB, Fn);
  BOOST_CHECK_CLOSE( Fn_true[0] + 0.1, Fn[0], tol );
  BOOST_CHECK_CLOSE( Fn_true[1] + 0.1, Fn[1], tol );
  BOOST_CHECK_CLOSE( Fn_true[2], Fn[2], tol );

  BOOST_CHECK(bc.isValidState(nx, ny, nt, qI) == true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFullState_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel,
                              RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelClass> PDEClass;

  typedef DLA::MatrixSymS<2, Real> ParamType;

  const Real tol = 1.0e-12;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 1e-5, pref);
  DensityModel rhon(52.1, 1e-5, pref);

  PorosityModel phi(0.3, 1e-5, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  Real Kxx = 1.1, Kxy = 0.6, Kyx = 0.2, Kyy = 1.5;
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc(5);

  const int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEClass pdeAV(order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCmitAVSensor2D<BCTypeFlux_mitState, BCTwoPhase2D< BCTypeFullState, PDEClass>> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  Real pnB = 2534.5;
  Real SwB = 0.738;
  PressureNonWet_SaturationWet<Real> qdata(pnB, SwB);
  Real Cdiff = 3.0;
  BCClass bc(Cdiff, pdeAV, qdata);

  Real x = 0, y = 0;
  Real time = 0;
  Real nx = 0.8, ny = -0.2;

  ParamType H = {{0.1},{0.05, 0.3}}; // grid spacing
  ParamType param = log(H);

  ArrayQ qI = {2500, 0.8, 0.1};
  ArrayQ qIx = {1.35, -0.05, 0.23};
  ArrayQ qIy = {-0.21, 0.85,-0.41};
  ArrayQ qB = 0;
  bc.state(param, x, y, time, nx, ny, qI, qB);

  BOOST_CHECK_CLOSE( pnB, qB[0], tol );
  BOOST_CHECK_CLOSE( SwB, qB[1], tol );
  BOOST_CHECK_CLOSE( qI[2], qB[2], tol );

  ArrayQ Fn_true = 0;
  pdeAV.fluxAdvectiveUpwind(param, x, y, time, qI, qB, nx, ny, Fn_true);

  ArrayQ fx_true = 0, fy_true = 0;
  pdeAV.fluxViscous(param, x, y, time, qB, qIx, qIy, fx_true, fy_true);
  Fn_true += fx_true*nx + fy_true*ny;

  DLA::VectorS<2, Real> nvec = {nx, ny};
  Real hn = dot(nvec, H*nvec);
  Fn_true[2] = sqrt(Cdiff)*hn*(qI(2) - 0.0);

  ArrayQ Fn = 0.1;
  bc.fluxNormal(param, x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);
  BOOST_CHECK_CLOSE( Fn_true[0] + 0.1, Fn[0], tol );
  BOOST_CHECK_CLOSE( Fn_true[1] + 0.1, Fn[1], tol );
  BOOST_CHECK_CLOSE( Fn_true[2], Fn[2], tol );

  BOOST_CHECK(bc.isValidState(nx, ny, qI) == true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeNoFlux_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel,
                              RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelClass> PDEClass;

  typedef DLA::MatrixSymS<2, Real> ParamType;

  const Real tol = 1.0e-12;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 1e-5, pref);
  DensityModel rhon(52.1, 1e-5, pref);

  PorosityModel phi(0.3, 1e-5, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  Real Kxx = 1.1, Kxy = 0.6, Kyx = 0.2, Kyy = 1.5;
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc(5);

  const int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEClass pdeAV(order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCmitAVSensor2D<BCTypeFlux_mitState, BCTwoPhase2D< BCTypeNoFlux, PDEClass>> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  Real Cdiff = 3.0;
  BCClass bc(Cdiff, pdeAV);

  Real x = 0, y = 0;
  Real time = 0;
  Real nx = 0.8, ny = -0.2;

  ParamType H = {{0.1},{0.05, 0.3}}; // grid spacing
  ParamType param = log(H);

  BOOST_CHECK_EQUAL( false, bc.hasFluxViscous() );

  ArrayQ qI = {2500, 0.8, 0.1};
  ArrayQ qIx = {1.35, -0.05, 0.23};
  ArrayQ qIy = {-0.21, 0.85,-0.41};
  ArrayQ qB = 0;

  bc.state(param, x, y, time, nx, ny, qI, qB);
  BOOST_CHECK_CLOSE( qI[0], qB[0], tol );
  BOOST_CHECK_CLOSE( qI[1], qB[1], tol );
  BOOST_CHECK_CLOSE( qI[2], qB[2], tol );

  ArrayQ Fn_true = 0;
  DLA::VectorS<2, Real> nvec = {nx, ny};
  Real hn = dot(nvec, H*nvec);
  Fn_true[2] = sqrt(Cdiff)*hn*(qI(2) - 0.0);

  ArrayQ Fn = 0.1;
  bc.fluxNormal(param, x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);
  BOOST_CHECK_CLOSE( Fn_true[0], Fn[0], tol );
  BOOST_CHECK_CLOSE( Fn_true[1], Fn[1], tol );
  BOOST_CHECK_CLOSE( Fn_true[2], Fn[2], tol );

  bool upwind = bc.useUpwind();
  BOOST_CHECK(upwind == false);

  BOOST_CHECK(bc.isValidState(nx, ny, qI) == true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeNoFluxSpaceTime_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel,
                              RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelClass> PDEClass;

  typedef DLA::MatrixSymS<3, Real> ParamType;

  const Real tol = 5.0e-12;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  Real Kxx = 1.1, Kxy = 0.6, Kyx = 0.2, Kyy = 1.5;
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc(5);

  const int order = 1;
  const bool hasSpaceTimeDiffusion = false;
  PDEClass pdeAV(order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCmitAVSensor2D<BCTypeFlux_mitStateSpaceTime, BCTwoPhase2D< BCTypeNoFluxSpaceTime, PDEClass>> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  Real Cdiff = 3.0;
  BCClass bc(Cdiff, pdeAV);

  Real x = 0, y = 0;
  Real time = 0;
  Real nx = 0.8, ny = -0.2, nt = 0.35;

  ParamType H = {{0.1},{0.05, 0.3},{0.02, 0.06, 0.4}}; // grid spacing
  ParamType param = log(H);

  BOOST_CHECK_EQUAL( false, bc.hasFluxViscous() );

  ArrayQ qI = {2500, 0.8, 0.1};
  ArrayQ qIx = {1.35, -0.05, 0.23};
  ArrayQ qIy = {-0.21, 0.85,-0.41};
  ArrayQ qIt = qIx;
  ArrayQ qB = 0;

  bc.state(param, x, y, time, nx, ny, nt, qI, qB);
  BOOST_CHECK_CLOSE( qI[0], qB[0], tol );
  BOOST_CHECK_CLOSE( qI[1], qB[1], tol );
  BOOST_CHECK_CLOSE( qI[2], qB[2], tol );

  ArrayQ Fn_true = 0;
  DLA::VectorS<3, Real> nvec = {nx, ny, nt};
  Real hn = dot(nvec, H*nvec);
  Fn_true[2] = sqrt(Cdiff)*hn*(qI(2) - 0.0);

  ArrayQ Fn = 0.1;
  bc.fluxNormalSpaceTime(param, x, y, time, nx, ny, nt, qI, qIx, qIy, qIt, qB, Fn);
  BOOST_CHECK_CLOSE( Fn_true[0], Fn[0], tol );
  BOOST_CHECK_CLOSE( Fn_true[1], Fn[1], tol );
  BOOST_CHECK_CLOSE( Fn_true[2], Fn[2], tol );

  bool upwind = bc.useUpwind();
  BOOST_CHECK(upwind == false);

  BOOST_CHECK(bc.isValidState(nx, ny, nt, qI) == true);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
