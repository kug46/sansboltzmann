// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEBuckleyLeverett1D_btest
//
// test of 1-D BuckleyLeverett PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverett.h"
#include "pde/PorousMedia/PDEBuckleyLeverett1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"

//Explicitly instantiate the class so coverage information is correct
namespace SANS
{
typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel_PowerLaw, CapillaryModel_None> TraitsModelClass_CapNone;
typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel_PowerLaw, CapillaryModel_Linear> TraitsModelClass_CapLinear;
template class PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass_CapNone>;
template class PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass_CapLinear>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEBuckleyLeverett1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test_SaturationWet )
{
  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass_CapNone> PDEClass_CapNone;

  BOOST_CHECK( PDEClass_CapNone::D == 1 );
  BOOST_CHECK( PDEClass_CapNone::N == 1 );

  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass_CapLinear> PDEClass_CapLinear;

  BOOST_CHECK( PDEClass_CapLinear::D == 1 );
  BOOST_CHECK( PDEClass_CapLinear::N == 1 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor_SaturationWet )
{
  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass_CapNone> PDEClass;
  Real tol = 1e-13;

  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.4;
  const Real mu_w = 1;
  const Real mu_n = 2;

  CapillaryModel_None pc_model;
  const Real K = 0.25;

  PDEClass pde(phi, uT, kr_model, mu_w, mu_n, K, pc_model);

  BOOST_CHECK_CLOSE( pde.porosity(), phi, tol);
  BOOST_CHECK_CLOSE( pde.totalVelocity(), uT, tol);
  BOOST_CHECK_CLOSE( pde.viscosityWet(), mu_w, tol);
  BOOST_CHECK_CLOSE( pde.viscosityNonwet(), mu_n, tol);
  BOOST_CHECK_CLOSE( pde.permeability(), K, tol);

  //Copy constructor
  PDEClass pde2(pde);
  BOOST_CHECK_CLOSE( pde2.porosity(), phi, tol);
  BOOST_CHECK_CLOSE( pde2.totalVelocity(), uT, tol);
  BOOST_CHECK_CLOSE( pde2.viscosityWet(), mu_w, tol);
  BOOST_CHECK_CLOSE( pde2.viscosityNonwet(), mu_n, tol);
  BOOST_CHECK_CLOSE( pde2.permeability(), K, tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux_SaturationWet )
{
  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass_CapLinear> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.4;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real pc_max = 5.0;
  CapillaryModel_Linear pc_model(pc_max);
  const Real K = 0.25;

  PDEClass pde(phi, uT, kr_model, mu_w, mu_n, K, pc_model);

  // Static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // Flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasSourceTrace() == false );
  BOOST_CHECK( pde.hasSourceLiftedQuantity() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // Function tests

  Real x, time;
  Real Sw, Swx, Swt, Swxx;
  Real krw, krn;
  Real Lw, Ln, Lw_Sw, Ln_Sw;
  Real fw_true, fw_Sw_true;

  x = time = 0; // not actually used in functions
  Sw = 0.375;
  Swx = -0.63;
  Swt = -0.27;
  Swxx = 1.93;
  krw = pow(Sw,2);
  krn = pow(1-Sw,2);
  Lw = krw/mu_w;
  Ln = krn/mu_n;
  fw_true = (Lw)/(Lw + Ln);

  Lw_Sw = (2*Sw)/mu_w;
  Ln_Sw = -2*(1-Sw)/mu_n;
  fw_Sw_true = (Lw_Sw*Ln - Lw*Ln_Sw)/pow(Lw + Ln, 2);


  // Set
  Real qDataPrim[1] = {Sw};
  string qNamePrim[1] = {"Sw"};
  ArrayQ qTrue = Sw;
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );
  ArrayQ qt = Swt;
  ArrayQ qx = Swx;
  ArrayQ qxx = Swxx;

  // Master state
  ArrayQ ucons_true = Sw;
  ArrayQ ucons = 0;
  pde.masterState( x, time, q, ucons );
  BOOST_CHECK_CLOSE( ucons_true, ucons, tol );

  // Conservative flux
  ArrayQ fcons_true = Sw;
  ArrayQ fcons = 0;
  pde.fluxAdvectiveTime( x, time, q, fcons );
  BOOST_CHECK_CLOSE( fcons_true, fcons, tol );

  // Strong form conservative flux
  ArrayQ strongPDE = 0;
  pde.strongFluxAdvectiveTime( x, time, q, qt, strongPDE );
  BOOST_CHECK_CLOSE( qt, strongPDE, tol );

  // Fractional flow function and Jacobian
  Real fw, fw_Sw;
  pde.fractionalFlow(Sw,1-Sw,fw);
  pde.fractionalFlowJacobian(Sw,1-Sw,fw_Sw);
  BOOST_CHECK_CLOSE( fw_true, fw, tol );
  BOOST_CHECK_CLOSE( fw_Sw_true, fw_Sw, tol );

  // Advective flux
  ArrayQ fadv_true = uT*fw_true/phi;
  ArrayQ fadv = 0;
  pde.fluxAdvective( x, time, q, fadv );
  BOOST_CHECK_CLOSE( fadv_true, fadv, tol );

  // Jacobian of advective flux
  MatrixQ fadv_u_true = uT*fw_Sw_true/phi;
  ArrayQ fadv_u = 0;
  pde.jacobianFluxAdvective( x, time, q, fadv_u );
  BOOST_CHECK_CLOSE( fadv_u_true, fadv_u, tol );

  // Upwind advective flux (left to right)
  Real SwL = 0.3;
  Real SwR = 0.5;
  Real nx = 0.5;
  Real qLData[1] = {SwL};
  Real qRData[1] = {SwR};
  ArrayQ qL, qR;
  pde.setDOFFrom( qL, qLData, qNamePrim, 1 );
  pde.setDOFFrom( qR, qRData, qNamePrim, 1 );

  ArrayQ fupwind = 0, fadvL = 0, fadvR = 0;
  pde.fluxAdvectiveUpwind(x, time, qL, qR, nx, fupwind);
  pde.fluxAdvective( x, time, qL, fadvL );
  pde.fluxAdvective( x, time, qR, fadvR );

  BOOST_CHECK_CLOSE( fadvL*nx, fupwind, tol );

  // Upwind advective flux (right to left)
  nx = -0.5;
  fupwind = 0;
  pde.fluxAdvectiveUpwind(x, time, qL, qR, nx, fupwind);
  BOOST_CHECK_CLOSE( fadvR*nx, fupwind, tol );

  // Strong form advective flux
  strongPDE = 0;
  pde.strongFluxAdvective( x, time, q, qx, strongPDE );
  BOOST_CHECK_CLOSE( fadv_u_true*qx, strongPDE, tol );

  // Characteristic speed
  Real lambda_true = fabs(fadv_u_true);
  Real lambda = 0;
  pde.speedCharacteristic(x, time, q, lambda);
  BOOST_CHECK_CLOSE( lambda_true, lambda, tol );

  // Viscous diffusion matrix
  Real pc_Sw = -pc_max;
  Real kxx_true = -K*(Lw*Ln)/(Lw+Ln)*pc_Sw/phi;
  MatrixQ kxx = 0;
  pde.diffusionViscous( x, time, q, qx, kxx );
  BOOST_CHECK_CLOSE( kxx_true, kxx, tol );

  // Space-time diffusion matrix
  kxx = 0;
  MatrixQ kxt = 0, ktx = 0, ktt = 0;
  pde.diffusionViscousSpaceTime( x, time, q, qx, qt, kxx, kxt, ktx, ktt );
  BOOST_CHECK_CLOSE( kxx_true, kxx, tol );
  BOOST_CHECK_CLOSE( 0.0, kxt, tol );
  BOOST_CHECK_CLOSE( 0.0, ktx, tol );
  BOOST_CHECK_CLOSE( 0.0, ktt, tol );

  // Viscous flux
  ArrayQ fvisc_x = 0;
  pde.fluxViscous( x, time, q, qx, fvisc_x );
  BOOST_CHECK_CLOSE( -kxx_true*Swx, fvisc_x, tol );

  // Space-time viscous flux
  fvisc_x = 0;
  ArrayQ fvisc_t = 0;
  pde.fluxViscousSpaceTime( x, time, q, qx, qt, fvisc_x, fvisc_t );
  BOOST_CHECK_CLOSE( -kxx_true*Swx, fvisc_x, tol );
  BOOST_CHECK_CLOSE( 0.0, fvisc_t, tol );

  // Jacobian of viscous diffusion matrix
  pc_Sw = -pc_max;
  Real pc_Sw2 = 0;
  MatrixQ tmp = (Lw*Ln)/(Lw + Ln);
  MatrixQ tmp_Sw = ((Lw_Sw*Ln + Lw*Ln_Sw) - tmp*(Lw_Sw + Ln_Sw))/(Lw + Ln);
  MatrixQ kxx_u_true = -(K/phi)*(tmp_Sw*pc_Sw + tmp*pc_Sw2);
  MatrixQ kxx_u = 0;
  pde.jacobianDiffusionViscous( x, time, q, kxx_u );
  BOOST_CHECK_CLOSE( kxx_u_true, kxx_u, tol );

  // Strong form viscous flux
  strongPDE = 0;
  pde.strongFluxViscous( x, time, q, qx, qxx, strongPDE );
  ArrayQ strong_viscousflux_true = -(kxx_u_true*(Swx*Swx) + kxx_true*Swxx);
  BOOST_CHECK_CLOSE( strong_viscousflux_true, strongPDE, tol );

  //Forcing function
  ArrayQ forcing = 0;
  BOOST_CHECK_THROW( pde.forcingFunction(x, time, forcing), DeveloperException);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState_SaturationWet )
{
  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass_CapLinear> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.4;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real pc_max = 5.0;
  CapillaryModel_Linear pc_model(pc_max);
  const Real K = 0.25;

  PDEClass pde(phi, uT, kr_model, mu_w, mu_n, K, pc_model);

  ArrayQ q = 0.0;
  BOOST_CHECK( pde.isValidState(q) == true );

  q = 0.35;
  BOOST_CHECK( pde.isValidState(q) == true );

  q = 0.8;
  BOOST_CHECK( pde.isValidState(q) == true );

  q = 1.0;
  BOOST_CHECK( pde.isValidState(q) == true );

//  q = -0.1;
//  BOOST_CHECK( pde.isValidState(q) == false );
//
//  q = 1.05;
//  BOOST_CHECK( pde.isValidState(q) == false );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( residInterp )
{
  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass_CapLinear> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.4;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real pc_max = 5.0;
  CapillaryModel_Linear pc_model(pc_max);
  const Real K = 0.25;

  PDEClass pde(phi, uT, kr_model, mu_w, mu_n, K, pc_model);

  ArrayQ rsdIn = 0.23;
  DLA::VectorD<ArrayQ> rsdOut = {0.0};
  pde.interpResidVariable(rsdIn, rsdOut);
  BOOST_CHECK_EQUAL( rsdOut[0], rsdIn );

  rsdIn = 0.32;
  rsdOut = 0.0;
  pde.interpResidGradient(rsdIn, rsdOut);
  BOOST_CHECK_EQUAL( rsdOut[0], rsdIn );

  rsdIn = -0.82;
  rsdOut = 0.0;
  pde.interpResidBC(rsdIn, rsdOut);
  BOOST_CHECK_EQUAL( rsdOut[0], rsdIn );

  BOOST_CHECK_EQUAL( pde.nMonitor(), 1 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/PDEBuckleyLeverett1D_pattern.txt", true );

  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass_CapNone> PDEClass_CapNone;
  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass_CapLinear> PDEClass_CapLinear;

  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.4;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real pc_max = 5.0;
  const Real K = 0.25;
  CapillaryModel_None pc_none;
  CapillaryModel_Linear pc_linear(pc_max);

  PDEClass_CapNone pde_capNone(phi, uT, kr_model, mu_w, mu_n, K, pc_none);
  PDEClass_CapLinear pde_capLinear(phi, uT, kr_model, mu_w, mu_n, K, pc_linear);

  pde_capNone.dump( 2, output );
  output << std::endl;
  pde_capLinear.dump(2, output);

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
