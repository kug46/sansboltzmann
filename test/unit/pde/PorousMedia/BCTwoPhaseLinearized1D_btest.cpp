// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCTwoPhaseLinearized1D_btest
//
// test of linearized two-phase BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/PorousMedia/Q1DPrimitive_pnSw.h"
#include "pde/PorousMedia/BCTwoPhaseLinearized1D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/CapillaryModel.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
typedef PDETwoPhaseLinearized1D<QTypePrimitive_pnSw, RelPermModel_PowerLaw, RelPermModel_PowerLaw,
                                CapillaryModel_Linear> PDE_RelPower_CapLinear;

template class BCTwoPhaseLinearized1D< BCTypeFullState, PDE_RelPower_CapLinear >;

// Instantiate BCParameters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters< BCTwoPhaseLinearized1DVector<PDE_RelPower_CapLinear> >;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BCTwoPhaseLinearized1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDETwoPhaseLinearized1D<QTypePrimitive_pnSw, RelPermModel_PowerLaw, RelPermModel_PowerLaw,
                                  CapillaryModel_Linear> PDEClass;

  {
  typedef BCNone<PhysD1,TraitsSizeTwoPhase<PhysD1>::N> BCClass;
  BOOST_CHECK( BCClass::D == 1 );
  BOOST_CHECK( BCClass::NBC == 0 );
  }

  {
  typedef BCTwoPhaseLinearized1D< BCTypeFullState, PDEClass> BCClass;
  BOOST_CHECK( BCClass::D == 1 );
  BOOST_CHECK( BCClass::N == 2 );
  BOOST_CHECK( BCClass::NBC == 2 );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor )
{
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef PDETwoPhaseLinearized1D<QTypePrimitive_pnSw, RelPermModel_PowerLaw, RelPermModel_PowerLaw,
                                  CapillaryModel_Linear> PDEClass;

  Real rhow = 52.4;
  Real rhon = 52.1;
  Real phi = 0.3;

  RelPermModel krw(2);
  RelPermModel krn(2);

  Real muw = 1;
  Real mun = 2;

  CapillaryModel pc(5);
  const Real K = 0.25;

  Real Swbar = 0.5;
  Real pxbar = -0.25;

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc, pxbar, Swbar);

  typedef BCParameters< BCTwoPhaseLinearized1DVector<PDEClass> > BCParams;

  PyDict BCList;

  {
    typedef BCNone<PhysD1,TraitsSizeTwoPhase<PhysD1>::N> BCClass;

    //Pydict constructor
    PyDict BCNone;
    BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;
    BCClass bc0(pde, BCNone);

    BCList["BC0"] = BCNone;
  }

  {
    typedef BCTwoPhaseLinearized1D< BCTypeFullState, PDEClass> BCClass;

    PressureNonWet_SaturationWet<Real> qdata(2534.5, 0.738);
    BCClass bc0(pde, qdata);

    PyDict d;
    d[TwoPhaseVariableTypeParams::params.StateVector.Variables] = TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
    d[PressureNonWet_SaturationWet_Params::params.pn] = 2534.5;
    d[PressureNonWet_SaturationWet_Params::params.Sw] = 0.738;

    PyDict BCFullState;
    BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
    BCFullState[BCClass::ParamsType::params.StateVector] = d;
    BCClass bc1(pde, BCFullState);

    BCList["BC1"] = BCFullState;
  }

  //No exceptions should be thrown
  BCParams::checkInputs(BCList);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCNone_test )
{
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef PDETwoPhaseLinearized1D<QTypePrimitive_pnSw, RelPermModel_PowerLaw, RelPermModel_PowerLaw,
                                  CapillaryModel_Linear> PDEClass;

  Real rhow = 52.4;
  Real rhon = 52.1;
  Real phi = 0.3;

  RelPermModel krw(2);
  RelPermModel krn(2);

  Real muw = 1;
  Real mun = 2;

  CapillaryModel pc(5);
  const Real K = 0.25;

  Real Swbar = 0.5;
  Real pxbar = -0.25;

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc, pxbar, Swbar);

  typedef BCNone<PhysD1,TraitsSizeTwoPhase<PhysD1>::N> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  BCClass bc;
  ArrayQ qI = {2500,0.8};
  Real nx = 2;
  BOOST_CHECK(bc.isValidState(nx, qI) == true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFullState_test )
{
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef PDETwoPhaseLinearized1D<QTypePrimitive_pnSw, RelPermModel_PowerLaw, RelPermModel_PowerLaw,
                                  CapillaryModel_Linear> PDEClass;

  Real rhow = 52.4;
  Real rhon = 52.1;
  Real phi = 0.3;

  RelPermModel krw(2);
  RelPermModel krn(2);

  Real muw = 1;
  Real mun = 2;

  CapillaryModel pc(5);
  const Real K = 0.25;

  Real Swbar = 0.5;
  Real pxbar = -0.25;

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc, pxbar, Swbar);

  typedef BCTwoPhaseLinearized1D< BCTypeFullState, PDEClass> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  Real pnB = 2534.5;
  Real SwB = 0.738;
  PressureNonWet_SaturationWet<Real> qdata(pnB, SwB);
  BCClass bc(pde, qdata);

  Real x = 0;
  Real time = 0;
  Real nx = 0.8;

  ArrayQ qI = {2500,0.8};
  ArrayQ qB = 0;
  bc.state(x, time, nx, qI, qB);

  Real tol = 1e-13;

  BOOST_CHECK_CLOSE( pnB, qB[0], tol );
  BOOST_CHECK_CLOSE( SwB, qB[1], tol );

  BOOST_CHECK(bc.isValidState(nx, qI) == true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/BCTwoPhaseLinearized1D_pattern.txt", true );

  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef PDETwoPhaseLinearized1D<QTypePrimitive_pnSw, RelPermModel_PowerLaw, RelPermModel_PowerLaw,
                                  CapillaryModel_Linear> PDEClass;

  Real rhow = 52.4;
  Real rhon = 52.1;
  Real phi = 0.3;

  RelPermModel krw(2);
  RelPermModel krn(2);

  Real muw = 1;
  Real mun = 2;

  CapillaryModel pc(5);
  const Real K = 0.25;

  Real Swbar = 0.5;
  Real pxbar = -0.25;

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc, pxbar, Swbar);

  {
    typedef BCNone<PhysD1,TraitsSizeTwoPhase<PhysD1>::N> BCClass;
    BCClass bc;
    bc.dump( 2, output );
  }

  {
    typedef BCTwoPhaseLinearized1D< BCTypeFullState, PDEClass> BCClass;

    Real pnB = 2534.5;
    Real SwB = 0.738;
    PressureNonWet_SaturationWet<Real> qdata(pnB, SwB);
    BCClass bc(pde, qdata);

    bc.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
