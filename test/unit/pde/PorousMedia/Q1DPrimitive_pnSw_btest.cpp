// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Q1DPrimitive_pnSw_btest
// testing of Q1D<QTypePrimitive_pnSw, CapillaryModel> class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <string>
#include <iostream>
#include "pde/PorousMedia/Q1DPrimitive_pnSw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsTwoPhase.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Q1DPrimitive_pnSw_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( test )
{
  const Real tol = 1.e-13;

  Real pc_max = 5.0;
  CapillaryModel_Linear pc_model(pc_max);

  typedef Q1D<QTypePrimitive_pnSw, CapillaryModel_Linear, TraitsSizeTwoPhase> QInterpret;
  BOOST_CHECK( QInterpret::D == 1 );
  BOOST_CHECK( QInterpret::N == 2 );

  typedef QInterpret::ArrayQ<Real> ArrayQ;

  // constructor
  QInterpret qInterpret(pc_model);

  // set/eval
  ArrayQ q;
  Real pw, pn, pc, Sw, Sn;
  Real pw1, pw2;
  Real pn1, pn2;
  Real Sw1, Sw2;
  Real Sn1, Sn2;

  pn = 2512.4;
  Sw = 0.375;

  Sn = 1-Sw;
  pc = pc_max*Sn;
  pw = pn - pc;

  Real qDataPrim[2] = {pn, Sw};
  string qNamePrim[2] = {"pn","Sw"};
  qInterpret.setFromPrimitive( q, qDataPrim, qNamePrim, 2 );
  qInterpret.eval( q, pw1, pn1, Sw1, Sn1 );
  BOOST_CHECK_CLOSE( pw1, pw, tol );
  BOOST_CHECK_CLOSE( pn1, pn, tol );
  BOOST_CHECK_CLOSE( Sw1, Sw, tol );
  BOOST_CHECK_CLOSE( Sn1, Sn, tol );

  pw1 = 0.0; pn1 = 0.0; Sw1 = 0.0; Sn1 = 0.0;
  qInterpret.eval_pw( q, pw1 );
  qInterpret.eval_pn( q, pn1 );
  qInterpret.eval_Sw( q, Sw1 );
  qInterpret.eval_Sn( q, Sn1 );
  BOOST_CHECK_CLOSE( pw1, pw, tol );
  BOOST_CHECK_CLOSE( pn1, pn, tol );
  BOOST_CHECK_CLOSE( Sw1, Sw, tol );
  BOOST_CHECK_CLOSE( Sn1, Sn, tol );

  // copy constructor
  QInterpret qInterpret2(qInterpret);

  qInterpret2.eval( q, pw2, pn2, Sw2, Sn2 );
  BOOST_CHECK_CLOSE( pw2, pw, tol );
  BOOST_CHECK_CLOSE( pn2, pn, tol );
  BOOST_CHECK_CLOSE( Sw2, Sw, tol );
  BOOST_CHECK_CLOSE( Sn2, Sn, tol );

  //jacobian
  ArrayQ pw_q, pn_q, Sw_q, Sn_q;
  qInterpret.jacobian_pw(q, pw_q);
  qInterpret.jacobian_pn(q, pn_q);
  qInterpret.jacobian_Sw(q, Sw_q);
  qInterpret.jacobian_Sn(q, Sn_q);
  BOOST_CHECK_CLOSE( pw_q[0], 1.0, tol );
  BOOST_CHECK_CLOSE( pw_q[1], pc_max, tol );
  BOOST_CHECK_CLOSE( pn_q[0], 1.0, tol );
  BOOST_CHECK_CLOSE( pn_q[1], 0.0, tol );
  BOOST_CHECK_CLOSE( Sw_q[0], 0.0, tol );
  BOOST_CHECK_CLOSE( Sw_q[1], 1.0, tol );
  BOOST_CHECK_CLOSE( Sn_q[0], 0.0, tol );
  BOOST_CHECK_CLOSE( Sn_q[1],-1.0, tol );

  // gradient
  Real pwx, pnx, pcx, Swx, Snx;
  Real pwx1, pwx2;
  Real pnx1, pnx2;
  Real Swx1, Swx2;
  Real Snx1, Snx2;

  pnx =  0.429;
  Swx = -0.673;

  Snx = -Swx;
  pcx = pc_max*Snx;
  pwx = pnx - pcx;

//  Real qxData[1] = {Swx};
  ArrayQ qx = {pnx, Swx};

  qInterpret.eval_pwGradient( q, qx, pwx1);
  qInterpret.eval_pnGradient( q, qx, pnx1);
  qInterpret.eval_SwGradient( q, qx, Swx1);
  qInterpret.eval_SnGradient( q, qx, Snx1);
  BOOST_CHECK_CLOSE( pwx1, pwx, tol );
  BOOST_CHECK_CLOSE( pnx1, pnx, tol );
  BOOST_CHECK_CLOSE( Swx1, Swx, tol );
  BOOST_CHECK_CLOSE( Snx1, Snx, tol );

  qInterpret2.eval_pwGradient( q, qx, pwx2);
  qInterpret2.eval_pnGradient( q, qx, pnx2);
  qInterpret2.eval_SwGradient( q, qx, Swx2);
  qInterpret2.eval_SnGradient( q, qx, Snx2);
  BOOST_CHECK_CLOSE( pwx2, pwx, tol );
  BOOST_CHECK_CLOSE( pnx2, pnx, tol );
  BOOST_CHECK_CLOSE( Swx2, Swx, tol );
  BOOST_CHECK_CLOSE( Snx2, Snx, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  CapillaryModel_Linear pc(5.0);

  typedef Q1D<QTypePrimitive_pnSw, CapillaryModel_Linear, TraitsSizeTwoPhase> QInterpret;
  typedef QInterpret::ArrayQ<Real> ArrayQ;

  QInterpret qInterpret(pc);
  ArrayQ q;

  q(0) = 2500.0;
  q(1) =  0.375;
  BOOST_CHECK( qInterpret.isValidState(q) == true );

  q(0) = 2500.0;
  q(1) =  0.739;
  BOOST_CHECK( qInterpret.isValidState(q) == true );

  q(0) = 2500.0;
  q(1) = -0.038;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  q(0) = 2500.0;
  q(1) =  1.01;
  BOOST_CHECK( qInterpret.isValidState(q) == false );

  q(0) = -0.5;
  q(1) =  0.5;
  BOOST_CHECK( qInterpret.isValidState(q) == false );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
