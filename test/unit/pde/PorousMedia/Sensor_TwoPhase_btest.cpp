// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Sensor_TwoPhase_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/PorousMedia/Sensor_TwoPhase.h"

#include "pde/PorousMedia/TraitsTwoPhase.h"
#include "pde/PorousMedia/Q2DPrimitive_pnSw.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/PermeabilityModel2D.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/PDETwoPhase2D.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{

template class TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_Comp,
                                   RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                                   PermeabilityModel2D_Constant, CapillaryModel_Linear>;

typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_Comp,
                            RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                            PermeabilityModel2D_Constant, CapillaryModel_Linear> TraitsModelClass;

typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDETwoPhase2DClass;

template class Sensor_TwoPhase<PDETwoPhase2DClass>;
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Sensor_TwoPhase_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jumpSensor )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef Sensor_TwoPhase<PDEClass> Sensor;
  typedef Sensor::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  RockPermModel K(1.1, 0.6, 0.2, 1.5);
  CapillaryModel pc(5);

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  Sensor sensor(pde);

  const ArrayQ jump = {0.258, -0.376};

  Real jumpQuantity = 0;
  Real jumpQuantityTrue = -0.376;
  sensor.jumpQuantity(jump, jumpQuantity);
  BOOST_CHECK_CLOSE( jumpQuantityTrue, jumpQuantity, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/Sensor_TwoPhase_pattern.txt", true );

  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef Sensor_TwoPhase<PDEClass> Sensor;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  RockPermModel K(1.1, 0.6, 0.2, 1.5);
  CapillaryModel pc(5);

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  Sensor sensor(pde);

  sensor.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
