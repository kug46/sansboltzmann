// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCTwoPhase2D_btest
//
// test of TwoPhase BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/PorousMedia/Q2DPrimitive_pnSw.h"
#include "pde/PorousMedia/BCTwoPhase2D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/PermeabilityModel2D.h"
#include "pde/PorousMedia/CapillaryModel.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_Comp,
                            RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                            PermeabilityModel2D_Constant, CapillaryModel_Linear> TraitsModel_RelPower_ViscConst_PermConst_CapLinear;

typedef PDETwoPhase2D<TraitsSizeTwoPhase,
                      TraitsModel_RelPower_ViscConst_PermConst_CapLinear> PDE_Comp_RelPower_ViscConst_PermConst_CapLinear;

template class BCTwoPhase2D< BCTypeTimeIC           , PDE_Comp_RelPower_ViscConst_PermConst_CapLinear >;
template class BCTwoPhase2D< BCTypeFullState        , PDE_Comp_RelPower_ViscConst_PermConst_CapLinear >;
template class BCTwoPhase2D< BCTypeNoFlux           , PDE_Comp_RelPower_ViscConst_PermConst_CapLinear >;
template class BCTwoPhase2D< BCTypeFunction_mitState, PDE_Comp_RelPower_ViscConst_PermConst_CapLinear >;

// Instantiate BCParameters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters< BCTwoPhase2DVector<PDE_Comp_RelPower_ViscConst_PermConst_CapLinear> >;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BCTwoPhase2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_Comp,
                              RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                              PermeabilityModel2D_Constant, CapillaryModel_Linear> TraitsModelClass;

  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  {
  typedef BCNone<PhysD2,TraitsSizeTwoPhase<PhysD2>::N> BCClass;
  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::NBC == 0 );
  }

  {
  typedef BCTwoPhase2D< BCTypeTimeIC, PDEClass> BCClass;
  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 2 );
  BOOST_CHECK( BCClass::NBC == 2 );
  }

  {
  typedef BCTwoPhase2D< BCTypeFullState, PDEClass> BCClass;
  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 2 );
  BOOST_CHECK( BCClass::NBC == 2 );
  }

  {
  typedef BCTwoPhase2D< BCTypeNoFlux, PDEClass> BCClass;
  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 2 );
  BOOST_CHECK( BCClass::NBC == 2 );
  }

  {
  typedef BCTwoPhase2D< BCTypeFunction_mitState, PDEClass> BCClass;
  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 2 );
  BOOST_CHECK( BCClass::NBC == 2 );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel,
                              RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  Real Kxx = 1.1, Kxy = 0.6, Kyx = 0.2, Kyy = 1.5;
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc(5);

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCParameters< BCTwoPhase2DVector<PDEClass> > BCParams;

  PyDict BCList;

  {
    typedef BCNone<PhysD2,TraitsSizeTwoPhase<PhysD2>::N> BCClass;

    //Pydict constructor
    PyDict BCNone;
    BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;
    BCClass bc0(pde, BCNone);

    BCList["BC0"] = BCNone;
  }

  {
    typedef BCTwoPhase2D< BCTypeTimeIC, PDEClass> BCClass;

    PressureNonWet_SaturationWet<Real> qdata(2534.5, 0.738);
    BCClass bc0(pde, qdata);

    PyDict d;
    d[TwoPhaseVariableTypeParams::params.StateVector.Variables] = TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
    d[PressureNonWet_SaturationWet_Params::params.pn] = 2534.5;
    d[PressureNonWet_SaturationWet_Params::params.Sw] = 0.738;

    PyDict BCFullState;
    BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC;
    BCFullState[BCClass::ParamsType::params.StateVector] = d;
    BCClass bc1(pde, BCFullState);

    BCList["BC1"] = BCFullState;
  }

  {
    typedef BCTwoPhase2D< BCTypeFullState, PDEClass> BCClass;

    PressureNonWet_SaturationWet<Real> qdata(2534.5, 0.738);
    BCClass bc0(pde, qdata);

    PyDict d;
    d[TwoPhaseVariableTypeParams::params.StateVector.Variables] = TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
    d[PressureNonWet_SaturationWet_Params::params.pn] = 2534.5;
    d[PressureNonWet_SaturationWet_Params::params.Sw] = 0.738;

    PyDict BCFullState;
    BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
    BCFullState[BCClass::ParamsType::params.StateVector] = d;
    BCClass bc1(pde, BCFullState);

    BCList["BC2"] = BCFullState;
  }

  {
    typedef BCTwoPhase2D< BCTypeNoFlux, PDEClass> BCClass;

    //Pydict constructor
    PyDict BCNoFlux;
    BCNoFlux[BCParams::params.BC.BCType] = BCParams::params.BC.NoFlux;
    BCClass bc2(pde, BCNoFlux);

    BCList["BC3"] = BCNoFlux;
  }

  {
    typedef BCTwoPhase2D< BCTypeFunction_mitState, PDEClass> BCClass;
    typedef Q2D<QTypePrimitive_pnSw, CapillaryModel, TraitsSizeTwoPhase> QInterpreter;
    typedef SolutionFunction_TwoPhase2D_SingleWell_Peaceman<QInterpreter, TraitsSizeTwoPhase> SolutionType;

    PyDict soldict;
    soldict[BCClass::ParamsType::params.Function.Name] = BCClass::ParamsType::params.Function.SingleWell_Peaceman;
    soldict[SolutionType::ParamsType::params.pB] = 2000;
    soldict[SolutionType::ParamsType::params.Rwellbore] = 1.0/6.0;
    soldict[SolutionType::ParamsType::params.Sw] = 1.0;
    soldict[SolutionType::ParamsType::params.mu] = 1.0;
    soldict[SolutionType::ParamsType::params.Q] = -100.0;
    soldict[SolutionType::ParamsType::params.xcentroid] = 0.0;
    soldict[SolutionType::ParamsType::params.ycentroid] = 0.0;
    soldict[SolutionType::ParamsType::params.Kxx] = 1.0;
    soldict[SolutionType::ParamsType::params.Kyy] = 0.5;

    PyDict BCSolution;
    BCSolution[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
    BCSolution[BCClass::ParamsType::params.Function] = soldict;
    BCClass bc3(pde, BCSolution);

    BCList["BC4"] = BCSolution;
  }

  //No exceptions should be thrown
  BCParams::checkInputs(BCList);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCNone_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel,
                              RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  Real Kxx = 1.1, Kxy = 0.6, Kyx = 0.2, Kyy = 1.5;
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc(5);

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCNone<PhysD2,TraitsSizeTwoPhase<PhysD2>::N> BCClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  BCClass bc;
  ArrayQ qI = {2500,0.8};
  Real nx = 2, ny = 3;
  BOOST_CHECK(bc.isValidState(nx, ny, qI) == true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeTimeIC_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel,
                              RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  const Real tol = 1.0e-12;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 1e-5, pref);
  DensityModel rhon(52.1, 1e-5, pref);

  PorosityModel phi(0.3, 1e-5, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  Real Kxx = 1.1, Kxy = 0.6, Kyx = 0.2, Kyy = 1.5;
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc(5);

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCTwoPhase2D< BCTypeTimeIC, PDEClass> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  Real pnB = 2534.5;
  Real SwB = 0.738;
  PressureNonWet_SaturationWet<Real> qdata(pnB, SwB);
  BCClass bc(pde, qdata);

  Real x = 0, y = 0;
  Real time = 0;
  Real nx = 0.8, ny = -0.2, nt = 0.3;

  ArrayQ qI = {2500,0.8};
  ArrayQ qIx = {1.35, -0.05};
  ArrayQ qIy = {-0.21, 0.85};
  ArrayQ qIt = qIx;
  ArrayQ qB = 0;
  bc.state(x, y, time, nx, ny, nt, qI, qB);

  BOOST_CHECK_CLOSE( pnB, qB[0], tol );
  BOOST_CHECK_CLOSE( SwB, qB[1], tol );

  ArrayQ uB = 0;
  pde.masterState(x, y, time, qB, uB);
  ArrayQ Fn_true = uB*nt;

  ArrayQ fvx = 0, fvy = 0, fvt = 0;
  pde.fluxViscousSpaceTime(x, y, time, qB, qIx, qIy, qIt, fvx, fvy, fvt);
  Fn_true += fvx*nx + fvy*ny + fvt*nt;

  ArrayQ Fn = 0;
  bc.fluxNormalSpaceTime(x, y, time, nx, ny, nt, qI, qIx, qIy, qIt, qB, Fn);
  BOOST_CHECK_CLOSE( Fn_true[0], Fn[0], tol );
  BOOST_CHECK_CLOSE( Fn_true[1], Fn[1], tol );

  BOOST_CHECK(bc.isValidState(nx, ny, nt, qI) == true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFullState_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel,
                              RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  const Real tol = 1.0e-12;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 1e-5, pref);
  DensityModel rhon(52.1, 1e-5, pref);

  PorosityModel phi(0.3, 1e-5, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  Real Kxx = 1.1, Kxy = 0.6, Kyx = 0.2, Kyy = 1.5;
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc(5);

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCTwoPhase2D< BCTypeFullState, PDEClass> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  Real pnB = 2534.5;
  Real SwB = 0.738;
  PressureNonWet_SaturationWet<Real> qdata(pnB, SwB);
  BCClass bc(pde, qdata);

  Real x = 0, y = 0;
  Real time = 0;
  Real nx = 0.8, ny = -0.2;

  ArrayQ qI = {2500,0.8};
  ArrayQ qIx = {1.35, -0.05};
  ArrayQ qIy = {-0.21, 0.85};
  ArrayQ qB = 0;
  bc.state(x, y, time, nx, ny, qI, qB);

  BOOST_CHECK_CLOSE( pnB, qB[0], tol );
  BOOST_CHECK_CLOSE( SwB, qB[1], tol );

  ArrayQ Fn_true = 0;
  pde.fluxAdvectiveUpwind(x, y, time, qI, qB, nx, ny, Fn_true);

  ArrayQ fx_true = 0, fy_true = 0;
  pde.fluxViscous(x, y, time, qB, qIx, qIy, fx_true, fy_true);
  Fn_true += fx_true*nx + fy_true*ny;

  ArrayQ Fn = 0;
  bc.fluxNormal(x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);
  BOOST_CHECK_CLOSE( Fn_true[0], Fn[0], tol );
  BOOST_CHECK_CLOSE( Fn_true[1], Fn[1], tol );

  BOOST_CHECK(bc.isValidState(nx, ny, qI) == true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeNoFlux_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel,
                              RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  const Real tol = 1.0e-12;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  Real Kxx = 1.1, Kxy = 0.6, Kyx = 0.2, Kyy = 1.5;
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc(5);

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCTwoPhase2D< BCTypeNoFlux, PDEClass> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  BCClass bc(pde);

  Real x = 0, y = 0;
  Real time = 0;
  Real nx = 0.8, ny = -0.2;

  BOOST_CHECK_EQUAL( false, bc.hasFluxViscous() );

  ArrayQ qI = {2500,0.8};
  ArrayQ qB = 0;
  ArrayQ qIx = {0.23,1.57};
  ArrayQ qIy = {-1.83,0.64};

  bc.state(x, y, time, nx, ny, qI, qB);
  BOOST_CHECK_CLOSE( qI[0], qB[0], tol );
  BOOST_CHECK_CLOSE( qI[1], qB[1], tol );

  ArrayQ Fn;
  bc.fluxNormal(x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);
  BOOST_CHECK_CLOSE( 0.0, Fn[0], tol );
  BOOST_CHECK_CLOSE( 0.0, Fn[1], tol );

  bool upwind = bc.useUpwind();
  BOOST_CHECK(upwind == false);

  BOOST_CHECK(bc.isValidState(nx, ny, qI) == true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeNoFluxSpaceTime_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel,
                              RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  const Real tol = 1.0e-12;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  Real Kxx = 1.1, Kxy = 0.6, Kyx = 0.2, Kyy = 1.5;
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc(5);

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  typedef BCTwoPhase2D< BCTypeNoFluxSpaceTime, PDEClass> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;

  BCClass bc(pde);

  Real x = 0, y = 0;
  Real time = 0;
  Real nx = 0.8, ny = -0.2, nt = 0.35;

  BOOST_CHECK_EQUAL( false, bc.hasFluxViscous() );

  ArrayQ qI = {2500,0.8};
  ArrayQ qB = 0;
  ArrayQ qIx = { 0.23,1.57};
  ArrayQ qIy = {-1.83,0.64};
  ArrayQ qIt = { 0.26,0.81};

  bc.state(x, y, time, nx, ny, nt, qI, qB);
  BOOST_CHECK_CLOSE( qI[0], qB[0], tol );
  BOOST_CHECK_CLOSE( qI[1], qB[1], tol );

  ArrayQ Fn;
  bc.fluxNormalSpaceTime(x, y, time, nx, ny, nt, qI, qIx, qIy, qIt, qB, Fn);
  BOOST_CHECK_CLOSE( 0.0, Fn[0], tol );
  BOOST_CHECK_CLOSE( 0.0, Fn[1], tol );

  bool upwind = bc.useUpwind();
  BOOST_CHECK(upwind == false);

  BOOST_CHECK(bc.isValidState(nx, ny, nt, qI) == true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFunction_mitState_test )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel,
                              RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef Q2D<QTypePrimitive_pnSw, CapillaryModel, TraitsSizeTwoPhase> QInterpreter;
  typedef BCTwoPhase2D< BCTypeFunction_mitState, PDEClass> BCClass;

  typedef BCParameters< BCTwoPhase2DVector<PDEClass> > BCParams;

  const Real pB = 2000.0;
  const Real R_bore = 1.0/6.0;
  const Real Sw = 1.0;
  const Real mu = 1.0;
  const Real Q = -1000;
  const Real Kxx = 1.0;
  const Real Kyy = 0.35;

  typedef SolutionFunction_TwoPhase2D_SingleWell_Peaceman<QInterpreter, TraitsSizeTwoPhase> SolutionType;
  typedef SolutionType::ArrayQ<Real> ArrayQ;

  const Real tol = 1.0e-12;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  RockPermModel K(Kxx, 0.0, 0.0, Kyy);

  CapillaryModel pc(0.0);

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  PyDict soldict;
  soldict[BCClass::ParamsType::params.Function.Name] = BCClass::ParamsType::params.Function.SingleWell_Peaceman;
  soldict[SolutionType::ParamsType::params.pB] = pB;
  soldict[SolutionType::ParamsType::params.Rwellbore] = R_bore;
  soldict[SolutionType::ParamsType::params.Sw] = Sw;
  soldict[SolutionType::ParamsType::params.mu] = mu;
  soldict[SolutionType::ParamsType::params.Q] = Q;
  soldict[SolutionType::ParamsType::params.xcentroid] = 0.0;
  soldict[SolutionType::ParamsType::params.ycentroid] = 0.0;
  soldict[SolutionType::ParamsType::params.Kxx] = Kxx;
  soldict[SolutionType::ParamsType::params.Kyy] = Kyy;

  PyDict BCSolution;
  BCSolution[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSolution[BCClass::ParamsType::params.Function] = soldict;
  BCClass bc(pde, BCSolution);

  Real x = 10.0, y = 20.0;
  Real time = 0;
  Real nx = 0.8, ny = 0.2;

  ArrayQ qI = {2500,0.8};
  ArrayQ qB = 0;
  bc.state(x, y, time, nx, ny, qI, qB);

  Real du = pow(Kyy/Kxx, 0.25) * x;
  Real dv = pow(Kxx/Kyy, 0.25) * y;
  Real r_uv = sqrt(du*du + dv*dv);
  Real rw_bar = 0.5*R_bore*( pow(Kyy/Kxx, 0.25) + pow(Kxx/Kyy, 0.25) );

  Real p = pB - (mu*Q)/(2.0*PI*sqrt(Kxx*Kyy))*log(r_uv/rw_bar);

  BOOST_CHECK_CLOSE( p, qB[0], tol );
  BOOST_CHECK_CLOSE( Sw, qB[1], tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PorousMedia/BCTwoPhase2D_pattern.txt", true );

  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef PermeabilityModel2D_Constant RockPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel,
                              RockPermModel, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  const Real pref = 14.7;

  DensityModel rhow(62.4, 0.1, pref);
  DensityModel rhon(52.1, 0.05, pref);

  PorosityModel phi(0.3, 0.06, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  Real Kxx = 1.1, Kxy = 0.6, Kyx = 0.2, Kyy = 1.5;
  RockPermModel K(Kxx, Kxy, Kyx, Kyy);

  CapillaryModel pc(5);

  PDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  {
    typedef BCNone<PhysD2,TraitsSizeTwoPhase<PhysD2>::N> BCClass;
    BCClass bc;
    bc.dump( 2, output );
  }

  {
    typedef BCTwoPhase2D< BCTypeFullState, PDEClass> BCClass;

    Real pnB = 2534.5;
    Real SwB = 0.738;
    PressureNonWet_SaturationWet<Real> qdata(pnB, SwB);
    BCClass bc(pde, qdata);

    bc.dump( 2, output );
  }

  {
    typedef BCTwoPhase2D< BCTypeNoFlux, PDEClass> BCClass;
    BCClass bc(pde);
    bc.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
