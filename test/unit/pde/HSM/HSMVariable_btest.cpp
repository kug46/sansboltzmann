// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// NSVariable_btest
// testing of NSVariable classes that represent a specific set of variables used to set the working variables

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/HSM/HSMVariable2D.h"
#include "pde/HSM/HSMVariable3D.h"

#include <string>
#include <iostream>

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( HSMVariable_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HSMVariable2D_test )
{
  const Real tol = 5.0e-12;

  // State vector
  Real rx, ry, lam3, f11, f22, f12;
  rx    = 3.263;
  ry    = -0.445;
  lam3  = 1./7.;
  f11   = 3.14;
  f22   = 42;
  f12   = 0.56;

  PositionLambdaForces2D var1(rx, ry, lam3, f11, f22, f12);

  BOOST_CHECK_CLOSE( rx, var1.rx, tol );
  BOOST_CHECK_CLOSE( ry, var1.ry, tol );
  BOOST_CHECK_CLOSE( lam3, var1.lam3, tol );
  BOOST_CHECK_CLOSE( f11, var1.f11, tol );
  BOOST_CHECK_CLOSE( f22, var1.f22, tol );
  BOOST_CHECK_CLOSE( f12, var1.f12, tol );

  PositionLambdaForces2D var2;

  var2 = {rx, ry, lam3, f11, f22, f12};

  BOOST_CHECK_CLOSE( rx, var2.rx, tol );
  BOOST_CHECK_CLOSE( ry, var2.ry, tol );
  BOOST_CHECK_CLOSE( lam3, var2.lam3, tol );
  BOOST_CHECK_CLOSE( f11, var2.f11, tol );
  BOOST_CHECK_CLOSE( f22, var2.f22, tol );
  BOOST_CHECK_CLOSE( f12, var2.f12, tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HSMVariable3D_test )
{
  const Real tol = 5.0e-12;

  // State vector
  Real rx, ry, rz, lam1, lam2, lam3, f11, f22, f12, m11, m22, m12;
  rx    = 3.263;
  ry    = -0.445;
  rz    = 19.82;
  lam1  = 1./5.;
  lam2  = 1./6.;
  lam3  = 1./7.;
  f11   = 3.14;
  f22   = 42;
  f12   = 0.56;
  m11   = 5.6;
  m22   = 4.9;
  m12   = 2.1;

  PositionLambdaForces3D var1(rx, ry, rz,
                              lam1, lam2, lam3,
                              f11, f22, f12,
                              m11, m22, m12);

  BOOST_CHECK_CLOSE( rx, var1.rx, tol );
  BOOST_CHECK_CLOSE( ry, var1.ry, tol );
  BOOST_CHECK_CLOSE( rz, var1.rz, tol );
  BOOST_CHECK_CLOSE( lam1, var1.lam1, tol );
  BOOST_CHECK_CLOSE( lam2, var1.lam2, tol );
  BOOST_CHECK_CLOSE( lam3, var1.lam3, tol );
  BOOST_CHECK_CLOSE( f11, var1.f11, tol );
  BOOST_CHECK_CLOSE( f22, var1.f22, tol );
  BOOST_CHECK_CLOSE( f12, var1.f12, tol );
  BOOST_CHECK_CLOSE( m11, var1.m11, tol );
  BOOST_CHECK_CLOSE( m22, var1.m22, tol );
  BOOST_CHECK_CLOSE( m12, var1.m12, tol );

  PositionLambdaForces3D var2;

  var2 = {rx, ry, rz,
          lam1, lam2, lam3,
          f11, f22, f12,
          m11, m22, m12};

  BOOST_CHECK_CLOSE( rx, var2.rx, tol );
  BOOST_CHECK_CLOSE( ry, var2.ry, tol );
  BOOST_CHECK_CLOSE( rz, var2.rz, tol );
  BOOST_CHECK_CLOSE( lam1, var2.lam1, tol );
  BOOST_CHECK_CLOSE( lam2, var2.lam2, tol );
  BOOST_CHECK_CLOSE( lam3, var2.lam3, tol );
  BOOST_CHECK_CLOSE( f11, var2.f11, tol );
  BOOST_CHECK_CLOSE( f22, var2.f22, tol );
  BOOST_CHECK_CLOSE( f12, var2.f12, tol );
  BOOST_CHECK_CLOSE( m11, var2.m11, tol );
  BOOST_CHECK_CLOSE( m22, var2.m22, tol );
  BOOST_CHECK_CLOSE( m12, var2.m12, tol );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
