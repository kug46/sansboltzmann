INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

GenerateUnitTests( pdeLib HSMLib DenseLinAlgLib PythonLib SurrealLib toolsLib )
