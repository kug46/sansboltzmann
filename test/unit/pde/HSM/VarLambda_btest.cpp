// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Var2DLambda_btest
//
// test of variable interpreter for 2-D Hybrid Shell Model (HSM) PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/HSM/Var2DLambda.h"
#include "pde/HSM/Var3DLambda.h"

#include <limits>

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}


using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( VarLambda_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( accessors2D )
{
  typedef VarInterpret2D<VarTypeLambda> VarInterp;
  typedef VarInterp::ArrayQ<Real> ArrayQ;
  typedef VarInterp::VectorX<Real> VectorX;
  typedef VarInterp::Vector3<Real> Vector3;
  typedef VarInterp::MatrixSymS2<Real> MatrixSymS2;

  BOOST_CHECK( VarInterp::D == 2 );
  BOOST_CHECK( VarInterp::N == 6 );

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  VarInterp varInterpObj;

  // State vector
  Real rx, rxxi, rxeta, rxx, rxy;
  Real ry, ryxi, ryeta, ryx, ryy;
  Real lam3, f11, f22, f12;
  rx    = 3.263;   rxxi = 1.32;   rxeta = -3.123;  rxx = -0.65;  rxy = -9.123;
  ry    = -0.445;  ryxi = -3.28;  ryeta = 8.6;     ryx = 153;    ryy = 0.0412;
  lam3  = 1./7.;
  f11   = 3.14;
  f22   = 42;
  f12   = 0.56;
  ArrayQ var    = {rx,    ry,    lam3, f11, f22, f12};
  ArrayQ varxi  = {rxxi,  ryxi,  0,    0,   0,   0};
  ArrayQ vareta = {rxeta, ryeta, 0,    0,   0,   0};
  ArrayQ varx   = {rxx,   ryx,   0,    0,   0,   0};
  ArrayQ vary   = {rxy,   ryy,   0,    0,   0,   0};

  // Get position
  VectorX r;
  varInterpObj.getPosition( var, r );
  //               True  Test
  SANS_CHECK_CLOSE( rx, r[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( ry, r[1], small_tol, close_tol );

  // Get position reference derivatives
  VectorX rxi, reta;
  varInterpObj.getPositionRefDerivs( var, varxi, vareta, rxi, reta );
  //                True    Test
  SANS_CHECK_CLOSE( rxxi , rxi[0] , small_tol, close_tol );
  SANS_CHECK_CLOSE( rxeta, reta[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( ryxi , rxi[1] , small_tol, close_tol );
  SANS_CHECK_CLOSE( ryeta, reta[1], small_tol, close_tol );

  // Get position gradient
  VectorX r_x, r_y;
  varInterpObj.getPositionGrad( var, varx, vary, r_x, r_y );
  //                True    Test
  SANS_CHECK_CLOSE( rxx, r_x[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rxy, r_y[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( ryx, r_x[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( ryy, r_y[1], small_tol, close_tol );

  // Get forces
  Vector3 floc;
  varInterpObj.getForces( var, floc );
  //                True  Test
  SANS_CHECK_CLOSE( f11, floc[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( f22, floc[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( f12, floc[2], small_tol, close_tol );

  MatrixSymS2 flocbar;
  varInterpObj.getForces( var, flocbar );
  //                True  Test
  SANS_CHECK_CLOSE( f11, flocbar(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( f12, flocbar(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( f22, flocbar(1,1), small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ehat_calcs2D )
{
  typedef VarInterpret2D<VarTypeLambda> VarInterp;
  typedef VarInterp::ArrayQ<Real> ArrayQ;
  typedef VarInterp::VectorX<Real> VectorX;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  VarInterp varInterpObj;

  // State vector
  Real rx, ry, lam3, f11, f22, f12;
  Real lam3x, lam3y;
  rx    = 3.263;
  ry    = -0.445;
  lam3  = 1./7.;   lam3x = 2./9.;  lam3y = -2./11.;
  f11   = 3.14;
  f22   = 42;
  f12   = 0.56;
  ArrayQ var    = {rx, ry, lam3, f11, f22, f12};
  ArrayQ varx   = { 0,  0, lam3x,  0,   0,   0};
  ArrayQ vary   = { 0,  0, lam3y,  0,   0,   0};

  // Calculate ehats
  // Calculate nonzero quaternion components and derivatives (Eq.6 in hsm2D)
  Real p0  =  cos( lam3 );
  Real p0x = -sin( lam3 )*lam3x;
  Real p0y = -sin( lam3 )*lam3y;

  Real p3  = sin( lam3 );
  Real p3x = cos( lam3 )*lam3x;
  Real p3y = cos( lam3 )*lam3y;

  // Deformed nodal basis vectors e1 and e2, and derivatives
  VectorX e1_True  = { 1-2*pow(p3,2)      , 2*p0*p3           };
  VectorX e1x_True = {  -4*    p3*p3x     , 2*(p0x*p3+p0*p3x) };
  VectorX e1y_True = {  -4*    p3*p3y     , 2*(p0y*p3+p0*p3y) };

  VectorX e2_True  = { -2*p0*p3           , 1-2*pow(p3,2)     };
  VectorX e2x_True = { -2*(p0x*p3+p0*p3x) ,  -4*    p3*p3x    };
  VectorX e2y_True = { -2*(p0y*p3+p0*p3y) ,  -4*    p3*p3y    };

  // Calculate ehats with varInterpret.calc_ehat
  VectorX e1, e2;
  varInterpObj.calc_ehat( var, e1, e2 );
  //                  True      Test
  SANS_CHECK_CLOSE( e1_True[0], e1[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1_True[1], e1[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2_True[0], e2[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2_True[1], e2[1], small_tol, close_tol );

  // Calculate ehats and derivatives with varInterpret.calc_ehat_derivs
  VectorX e1x, e1y, e2x, e2y;
  varInterpObj.calc_ehat_derivs( var, varx, vary, e1, e2, e1x, e1y, e2x, e2y );
  //                  True      Test
  SANS_CHECK_CLOSE( e1_True[0], e1[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1_True[1], e1[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2_True[0], e2[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2_True[1], e2[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( e1x_True[0], e1x[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1x_True[1], e1x[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2x_True[0], e2x[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2x_True[1], e2x[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( e1y_True[0], e1y[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1y_True[1], e1y[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2y_True[0], e2y[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2y_True[1], e2y[1], small_tol, close_tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( accessors3D )
{
  typedef VarInterpret3D<VarTypeLambda> VarInterp;
  typedef VarInterp::ArrayQ<Real> ArrayQ;
  typedef VarInterp::VectorX<Real> VectorX;
  typedef VarInterp::Vector3<Real> Vector3;
  typedef VarInterp::MatrixSymS3<Real> MatrixSymS3;

  BOOST_CHECK( VarInterp::D == 3 );
  BOOST_CHECK( VarInterp::N == 12 );

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  VarInterp varInterpObj;

  // State vector
  Real rx, rxxi, rxeta, rxx, rxy, rxz;
  Real ry, ryxi, ryeta, ryx, ryy, ryz;
  Real rz, rzxi, rzeta, rzx, rzy, rzz;
  Real lam1, lam2, lam3;
  Real f11, f22, f12;
  Real m11, m22, m12;
  rx   = 3.263;  rxxi = 1.32;  rxeta = -3.123; rxx = -0.65; rxy = -9.123; rxz = 1.23;
  ry   = -0.445; ryxi = -3.28; ryeta = 8.6;    ryx = 153;   ryy = 0.0412; ryz = 0.145;
  rz   = 1.78;   rzxi = 4.09;  rzeta = 7.82;   rzx = 9.4;   rzy = 0.678;  rzz = 978.2;
  lam1 = 1./5.;
  lam2 = 1./6.;
  lam3 = 1./7.;
  f11  = 3.14;
  f22  = 42;
  f12  = 0.56;
  m11   = 5.6;
  m22   = 4.9;
  m12   = 2.1;

  ArrayQ var    = {rx,    ry,    rz,    lam1, lam2, lam3, f11, f22, f12, m11, m22, m12};
  ArrayQ varxi  = {rxxi,  ryxi,  rzxi,     0,    0,    0,   0,   0,   0,   0,   0,   0};
  ArrayQ vareta = {rxeta, ryeta, rzeta,    0,    0,    0,   0,   0,   0,   0,   0,   0};
  ArrayQ varx   = {rxx,   ryx,   rzx,      0,    0,    0,   0,   0,   0,   0,   0,   0};
  ArrayQ vary   = {rxy,   ryy,   rzy,      0,    0,    0,   0,   0,   0,   0,   0,   0};
  ArrayQ varz   = {rxz,   ryz,   rzz,      0,    0,    0,   0,   0,   0,   0,   0,   0};

  // Get position
  VectorX r;
  varInterpObj.getPosition( var, r );
  //               True  Test
  SANS_CHECK_CLOSE( rx, r[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( ry, r[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rz, r[2], small_tol, close_tol );

  // Get position reference derivatives
  VectorX rxi, reta;
  varInterpObj.getPositionRefDerivs( var, varxi, vareta, rxi, reta );
  //                True    Test
  SANS_CHECK_CLOSE( rxxi , rxi[0] , small_tol, close_tol );
  SANS_CHECK_CLOSE( rxeta, reta[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( ryxi , rxi[1] , small_tol, close_tol );
  SANS_CHECK_CLOSE( ryeta, reta[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rzxi , rxi[2] , small_tol, close_tol );
  SANS_CHECK_CLOSE( rzeta, reta[2], small_tol, close_tol );

  // Get position gradient
  VectorX r_x, r_y, r_z;
  varInterpObj.getPositionGrad( var, varx, vary, varz, r_x, r_y, r_z );
  //                True    Test
  SANS_CHECK_CLOSE( rxx, r_x[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rxy, r_y[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rxz, r_z[0], small_tol, close_tol );

  SANS_CHECK_CLOSE( ryx, r_x[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( ryy, r_y[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( ryz, r_z[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rzx, r_x[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rzy, r_y[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rzz, r_z[2], small_tol, close_tol );

  // Get forces
  Vector3 floc;
  varInterpObj.getForces( var, floc );
  //                True  Test
  SANS_CHECK_CLOSE( f11, floc[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( f22, floc[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( f12, floc[2], small_tol, close_tol );

  MatrixSymS3 flocbar;
  varInterpObj.getForces( var, flocbar );
  //                True  Test
  SANS_CHECK_CLOSE( f11, flocbar(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( f12, flocbar(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( f22, flocbar(1,1), small_tol, close_tol );
  BOOST_CHECK_EQUAL( 0, flocbar(2,0) );
  BOOST_CHECK_EQUAL( 0, flocbar(2,1) );
  BOOST_CHECK_EQUAL( 0, flocbar(2,2) );

  // Get moments
  Vector3 mloc;
  varInterpObj.getMoments( var, mloc );
  //                True  Test
  SANS_CHECK_CLOSE( m11, mloc[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( m22, mloc[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( m12, mloc[2], small_tol, close_tol );

  // Get moments
  MatrixSymS3 mlocbar;
  varInterpObj.getMoments( var, mlocbar );
  //                True  Test
  SANS_CHECK_CLOSE( m11, mlocbar(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( m12, mlocbar(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( m22, mlocbar(1,1), small_tol, close_tol );
  BOOST_CHECK_EQUAL( 0, mlocbar(2,0) );
  BOOST_CHECK_EQUAL( 0, mlocbar(2,1) );
  BOOST_CHECK_EQUAL( 0, mlocbar(2,2) );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ehat_calcs3D )
{
  typedef VarInterpret3D<VarTypeLambda> VarInterp;
  typedef VarInterp::ArrayQ<Real> ArrayQ;
  typedef VarInterp::VectorX<Real> VectorX;

  const Real small_tol = 1.e-12;
  const Real close_tol = 1.e-12;

  VarInterp varInterpObj;

  // State vector
  Real rx, ry, rz;
  Real lam1, lam2, lam3;
  Real f11, f22, f12;
  Real m11, m22, m12;
  Real lam1x, lam1y, lam1z;
  Real lam2x, lam2y, lam2z;
  Real lam3x, lam3y, lam3z;
  rx    = 3.263;
  ry    = -0.445;
  rz   = 1.78;
  lam1  = 1./5.;   lam1x = 2./7.;  lam1y = -2./9. ; lam1z = 3./10.;
  lam2  = 1./6.;   lam2x = 2./8.;  lam2y = -2./10.; lam2z = 3./11.;
  lam3  = 1./7.;   lam3x = 2./9.;  lam3y = -2./11.; lam3z = 3./12.;
  f11   = 3.14;
  f22   = 42;
  f12   = 0.56;
  m11   = 5.6;
  m22   = 4.9;
  m12   = 2.1;
  ArrayQ var    = {rx, ry, rz, lam1,  lam2,  lam3, f11, f22, f12, m11, m22, m12};
  ArrayQ varx   = { 0,  0,  0, lam1x, lam2x, lam3x,  0,   0,   0,   0,   0,   0};
  ArrayQ vary   = { 0,  0,  0, lam1y, lam2y, lam3y,  0,   0,   0,   0,   0,   0};
  ArrayQ varz   = { 0,  0,  0, lam1z, lam2z, lam3z,  0,   0,   0,   0,   0,   0};

  Real mag2  =    lam1*lam1  + lam2*lam2  + lam3*lam3;
  Real mag2x = 2*(lam1x*lam1 + lam2x*lam2 + lam3x*lam3);
  Real mag2y = 2*(lam1y*lam1 + lam2y*lam2 + lam3y*lam3);
  Real mag2z = 2*(lam1z*lam1 + lam2z*lam2 + lam3z*lam3);

  Real mag  = sqrt(mag2);
  Real magx = mag2x/(0.5*mag);
  Real magy = mag2y/(0.5*mag);
  Real magz = mag2z/(0.5*mag);

  // Calculate ehats
  Real p0  =  cos( mag );
  Real p0x = -sin( mag )*magx;
  Real p0y = -sin( mag )*magy;
  Real p0z = -sin( mag )*magz;

  Real p1  = sin( mag )/mag*lam1;
  Real p1x = (cos( mag )*magx/mag - sin( mag )*magx/mag2)*lam1 + sin( mag )/mag*lam1x;
  Real p1y = (cos( mag )*magy/mag - sin( mag )*magy/mag2)*lam1 + sin( mag )/mag*lam1y;
  Real p1z = (cos( mag )*magz/mag - sin( mag )*magz/mag2)*lam1 + sin( mag )/mag*lam1z;

  Real p2  = sin( mag )/mag*lam2;
  Real p2x = (cos( mag )*magx/mag - sin( mag )*magx/mag2)*lam2 + sin( mag )/mag*lam2x;
  Real p2y = (cos( mag )*magy/mag - sin( mag )*magy/mag2)*lam2 + sin( mag )/mag*lam2y;
  Real p2z = (cos( mag )*magz/mag - sin( mag )*magz/mag2)*lam2 + sin( mag )/mag*lam2z;

  Real p3  = sin( mag )/mag*lam3;
  Real p3x = (cos( mag )*magx/mag - sin( mag )*magx/mag2)*lam3 + sin( mag )/mag*lam3x;
  Real p3y = (cos( mag )*magy/mag - sin( mag )*magy/mag2)*lam3 + sin( mag )/mag*lam3y;
  Real p3z = (cos( mag )*magz/mag - sin( mag )*magz/mag2)*lam3 + sin( mag )/mag*lam3z;

  // Deformed nodal basis vectors e1 and e2, and derivatives
  VectorX e1_True  = { 1-2*(p2*p2  + p3*p3 ), 2*(p1*p2         + p0*p3        ), 2*(p1*p3         - p0*p2        ) };
  VectorX e1x_True = {  -4*(p2x*p2 + p3x*p3), 2*(p1x*p2+p1*p2x + p0x*p3+p0*p3x), 2*(p1x*p3+p1*p3x - p0x*p2-p0*p2x) };
  VectorX e1y_True = {  -4*(p2y*p2 + p3y*p3), 2*(p1y*p2+p1*p2y + p0y*p3+p0*p3y), 2*(p1y*p3+p1*p3y - p0y*p2-p0*p2y) };
  VectorX e1z_True = {  -4*(p2z*p2 + p3z*p3), 2*(p1z*p2+p1*p2z + p0z*p3+p0*p3z), 2*(p1z*p3+p1*p3z - p0z*p2-p0*p2z) };

  VectorX e2_True  = { 2*(p1*p2         - p0*p3        ), 1-2*(p1*p1  + p3*p3 ), 2*(p2*p3         + p0*p1        ) };
  VectorX e2x_True = { 2*(p1x*p2+p1*p2x - p0x*p3-p0*p3x),  -4*(p1x*p1 + p3x*p3), 2*(p2x*p3+p2*p3x + p0x*p1+p0*p1x) };
  VectorX e2y_True = { 2*(p1y*p2+p1*p2y - p0y*p3-p0*p3y),  -4*(p1y*p1 + p3y*p3), 2*(p2y*p3+p2*p3y + p0y*p1+p0*p1y) };
  VectorX e2z_True = { 2*(p1z*p2+p1*p2z - p0z*p3-p0*p3z),  -4*(p1z*p1 + p3z*p3), 2*(p2z*p3+p2*p3z + p0z*p1+p0*p1z) };

  VectorX n_True   = { 2*(p1*p3         + p0*p2        ), 2*(p2*p3         - p0*p1        ), 1-2*(p1*p1  + p2*p2 ) };
  VectorX nx_True  = { 2*(p1x*p3+p1*p3x + p0x*p2+p0*p2x), 2*(p2x*p3+p2*p3x - p0x*p1-p0*p1x),  -4*(p1x*p1 + p2x*p2) };
  VectorX ny_True  = { 2*(p1y*p3+p1*p3y + p0y*p2+p0*p2y), 2*(p2y*p3+p2*p3y - p0y*p1-p0*p1y),  -4*(p1y*p1 + p2y*p2) };
  VectorX nz_True  = { 2*(p1z*p3+p1*p3z + p0z*p2+p0*p2z), 2*(p2z*p3+p2*p3z - p0z*p1-p0*p1z),  -4*(p1z*p1 + p2z*p2) };

  // Calculate ehats with varInterpret.calc_ehat
  VectorX e1, e2, n;
  varInterpObj.calc_ehat( var, e1, e2, n );
  //                  True      Test
  SANS_CHECK_CLOSE( e1_True[0], e1[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1_True[1], e1[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1_True[2], e1[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2_True[0], e2[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2_True[1], e2[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2_True[2], e2[2], small_tol, close_tol );
  SANS_CHECK_CLOSE(  n_True[0],  n[0], small_tol, close_tol );
  SANS_CHECK_CLOSE(  n_True[1],  n[1], small_tol, close_tol );
  SANS_CHECK_CLOSE(  n_True[2],  n[2], small_tol, close_tol );

  // Calculate ehats and derivatives with varInterpret.calc_ehat_derivs
  VectorX e1x, e1y, e1z, e2x, e2y, e2z, nx, ny, nz;
  varInterpObj.calc_ehat_derivs( var,
                                 varx, vary, varz,
                                 e1, e2, n,
                                 e1x, e1y, e1z,
                                 e2x, e2y, e2z,
                                 nx,  ny,  nz);
  //                  True      Test
  SANS_CHECK_CLOSE( e1_True[0], e1[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1_True[1], e1[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1_True[2], e1[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2_True[0], e2[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2_True[1], e2[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2_True[2], e2[2], small_tol, close_tol );
  SANS_CHECK_CLOSE(  n_True[0],  n[0], small_tol, close_tol );
  SANS_CHECK_CLOSE(  n_True[1],  n[1], small_tol, close_tol );
  SANS_CHECK_CLOSE(  n_True[2],  n[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( e1x_True[0], e1x[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1x_True[1], e1x[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1x_True[2], e1x[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2x_True[0], e2x[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2x_True[1], e2x[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2x_True[2], e2x[2], small_tol, close_tol );
  SANS_CHECK_CLOSE(  nx_True[0],  nx[0], small_tol, close_tol );
  SANS_CHECK_CLOSE(  nx_True[1],  nx[1], small_tol, close_tol );
  SANS_CHECK_CLOSE(  nx_True[2],  nx[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( e1y_True[0], e1y[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1y_True[1], e1y[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1y_True[2], e1y[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2y_True[0], e2y[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2y_True[1], e2y[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2y_True[2], e2y[2], small_tol, close_tol );
  SANS_CHECK_CLOSE(  ny_True[0],  ny[0], small_tol, close_tol );
  SANS_CHECK_CLOSE(  ny_True[1],  ny[1], small_tol, close_tol );
  SANS_CHECK_CLOSE(  ny_True[2],  ny[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( e1z_True[0], e1z[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1z_True[1], e1z[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1z_True[2], e1z[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2z_True[0], e2z[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2z_True[1], e2z[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2z_True[2], e2z[2], small_tol, close_tol );
  SANS_CHECK_CLOSE(  nz_True[0],  nz[0], small_tol, close_tol );
  SANS_CHECK_CLOSE(  nz_True[1],  nz[1], small_tol, close_tol );
  SANS_CHECK_CLOSE(  nz_True[2],  nz[2], small_tol, close_tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ehat_calcs3D_machineeps )
{
  typedef VarInterpret3D<VarTypeLambda> VarInterp;
  typedef VarInterp::ArrayQ<Real> ArrayQ;
  typedef VarInterp::VectorX<Real> VectorX;

  const Real small_tol = 1.e-12;
  const Real close_tol = 1.e-12;

  VarInterp varInterpObj;

  // State vector
  Real rx, ry, rz;
  Real lam1, lam2, lam3;
  Real f11, f22, f12;
  Real m11, m22, m12;
  Real lam1x, lam1y, lam1z;
  Real lam2x, lam2y, lam2z;
  Real lam3x, lam3y, lam3z;
  rx    = 3.263;
  ry    = -0.445;
  rz   = 1.78;
  lam1  = 1*std::numeric_limits<Real>::epsilon();   lam1x = 2./7.;  lam1y = -2./9. ; lam1z = 3./10.;
  lam2  = 2*std::numeric_limits<Real>::epsilon();   lam2x = 2./8.;  lam2y = -2./10.; lam2z = 3./11.;
  lam3  = 3*std::numeric_limits<Real>::epsilon();   lam3x = 2./9.;  lam3y = -2./11.; lam3z = 3./12.;
  f11   = 3.14;
  f22   = 42;
  f12   = 0.56;
  m11   = 5.6;
  m22   = 4.9;
  m12   = 2.1;
  ArrayQ var    = {rx, ry, rz, lam1,  lam2,  lam3, f11, f22, f12, m11, m22, m12};
  ArrayQ varx   = { 0,  0,  0, lam1x, lam2x, lam3x,  0,   0,   0,   0,   0,   0};
  ArrayQ vary   = { 0,  0,  0, lam1y, lam2y, lam3y,  0,   0,   0,   0,   0,   0};
  ArrayQ varz   = { 0,  0,  0, lam1z, lam2z, lam3z,  0,   0,   0,   0,   0,   0};

  Real mag2  =    lam1*lam1  + lam2*lam2  + lam3*lam3;
  Real mag2x = 2*(lam1x*lam1 + lam2x*lam2 + lam3x*lam3);
  Real mag2y = 2*(lam1y*lam1 + lam2y*lam2 + lam3y*lam3);
  Real mag2z = 2*(lam1z*lam1 + lam2z*lam2 + lam3z*lam3);

  // Make sure that we take the zero magnitude path
  BOOST_REQUIRE_LT(mag2, varInterpObj.machine_eps);

  // Calculate ehats
  Real p0  =  1 - 0.5*mag2;
  Real p0x = -0.5*mag2x;
  Real p0y = -0.5*mag2y;
  Real p0z = -0.5*mag2z;

  Real p1  = (1 - 1./6.*mag2)*lam1;
  Real p1x = (  - 1./6.*mag2x)*lam1 + (1 - 1./6.*mag2)*lam1x;
  Real p1y = (  - 1./6.*mag2y)*lam1 + (1 - 1./6.*mag2)*lam1y;
  Real p1z = (  - 1./6.*mag2z)*lam1 + (1 - 1./6.*mag2)*lam1z;

  Real p2  = (1 - 1./6.*mag2)*lam2;
  Real p2x = (  - 1./6.*mag2x)*lam2 + (1 - 1./6.*mag2)*lam2x;
  Real p2y = (  - 1./6.*mag2y)*lam2 + (1 - 1./6.*mag2)*lam2y;
  Real p2z = (  - 1./6.*mag2z)*lam2 + (1 - 1./6.*mag2)*lam2z;

  Real p3  = (1 - 1./6.*mag2)*lam3;
  Real p3x = (  - 1./6.*mag2x)*lam3 + (1 - 1./6.*mag2)*lam3x;
  Real p3y = (  - 1./6.*mag2y)*lam3 + (1 - 1./6.*mag2)*lam3y;
  Real p3z = (  - 1./6.*mag2z)*lam3 + (1 - 1./6.*mag2)*lam3z;

  // Deformed nodal basis vectors e1 and e2, and derivatives
  VectorX e1_True  = { 1-2*(p2*p2  + p3*p3 ), 2*(p1*p2         + p0*p3        ), 2*(p1*p3         - p0*p2        ) };
  VectorX e1x_True = {  -4*(p2x*p2 + p3x*p3), 2*(p1x*p2+p1*p2x + p0x*p3+p0*p3x), 2*(p1x*p3+p1*p3x - p0x*p2-p0*p2x) };
  VectorX e1y_True = {  -4*(p2y*p2 + p3y*p3), 2*(p1y*p2+p1*p2y + p0y*p3+p0*p3y), 2*(p1y*p3+p1*p3y - p0y*p2-p0*p2y) };
  VectorX e1z_True = {  -4*(p2z*p2 + p3z*p3), 2*(p1z*p2+p1*p2z + p0z*p3+p0*p3z), 2*(p1z*p3+p1*p3z - p0z*p2-p0*p2z) };

  VectorX e2_True  = { 2*(p1*p2         - p0*p3        ), 1-2*(p1*p1  + p3*p3 ), 2*(p2*p3         + p0*p1        ) };
  VectorX e2x_True = { 2*(p1x*p2+p1*p2x - p0x*p3-p0*p3x),  -4*(p1x*p1 + p3x*p3), 2*(p2x*p3+p2*p3x + p0x*p1+p0*p1x) };
  VectorX e2y_True = { 2*(p1y*p2+p1*p2y - p0y*p3-p0*p3y),  -4*(p1y*p1 + p3y*p3), 2*(p2y*p3+p2*p3y + p0y*p1+p0*p1y) };
  VectorX e2z_True = { 2*(p1z*p2+p1*p2z - p0z*p3-p0*p3z),  -4*(p1z*p1 + p3z*p3), 2*(p2z*p3+p2*p3z + p0z*p1+p0*p1z) };

  VectorX n_True   = { 2*(p1*p3         + p0*p2        ), 2*(p2*p3         - p0*p1        ), 1-2*(p1*p1  + p2*p2 ) };
  VectorX nx_True  = { 2*(p1x*p3+p1*p3x + p0x*p2+p0*p2x), 2*(p2x*p3+p2*p3x - p0x*p1-p0*p1x),  -4*(p1x*p1 + p2x*p2) };
  VectorX ny_True  = { 2*(p1y*p3+p1*p3y + p0y*p2+p0*p2y), 2*(p2y*p3+p2*p3y - p0y*p1-p0*p1y),  -4*(p1y*p1 + p2y*p2) };
  VectorX nz_True  = { 2*(p1z*p3+p1*p3z + p0z*p2+p0*p2z), 2*(p2z*p3+p2*p3z - p0z*p1-p0*p1z),  -4*(p1z*p1 + p2z*p2) };

  // Calculate ehats with varInterpret.calc_ehat
  VectorX e1, e2, n;
  varInterpObj.calc_ehat( var, e1, e2, n );
  //                  True      Test
  SANS_CHECK_CLOSE( e1_True[0], e1[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1_True[1], e1[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1_True[2], e1[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2_True[0], e2[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2_True[1], e2[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2_True[2], e2[2], small_tol, close_tol );
  SANS_CHECK_CLOSE(  n_True[0],  n[0], small_tol, close_tol );
  SANS_CHECK_CLOSE(  n_True[1],  n[1], small_tol, close_tol );
  SANS_CHECK_CLOSE(  n_True[2],  n[2], small_tol, close_tol );

  // Calculate ehats and derivatives with varInterpret.calc_ehat_derivs
  VectorX e1x, e1y, e1z, e2x, e2y, e2z, nx, ny, nz;
  varInterpObj.calc_ehat_derivs( var,
                                 varx, vary, varz,
                                 e1, e2, n,
                                 e1x, e1y, e1z,
                                 e2x, e2y, e2z,
                                 nx,  ny,  nz);
  //                  True      Test
  SANS_CHECK_CLOSE( e1_True[0], e1[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1_True[1], e1[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1_True[2], e1[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2_True[0], e2[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2_True[1], e2[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2_True[2], e2[2], small_tol, close_tol );
  SANS_CHECK_CLOSE(  n_True[0],  n[0], small_tol, close_tol );
  SANS_CHECK_CLOSE(  n_True[1],  n[1], small_tol, close_tol );
  SANS_CHECK_CLOSE(  n_True[2],  n[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( e1x_True[0], e1x[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1x_True[1], e1x[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1x_True[2], e1x[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2x_True[0], e2x[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2x_True[1], e2x[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2x_True[2], e2x[2], small_tol, close_tol );
  SANS_CHECK_CLOSE(  nx_True[0],  nx[0], small_tol, close_tol );
  SANS_CHECK_CLOSE(  nx_True[1],  nx[1], small_tol, close_tol );
  SANS_CHECK_CLOSE(  nx_True[2],  nx[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( e1y_True[0], e1y[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1y_True[1], e1y[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1y_True[2], e1y[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2y_True[0], e2y[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2y_True[1], e2y[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2y_True[2], e2y[2], small_tol, close_tol );
  SANS_CHECK_CLOSE(  ny_True[0],  ny[0], small_tol, close_tol );
  SANS_CHECK_CLOSE(  ny_True[1],  ny[1], small_tol, close_tol );
  SANS_CHECK_CLOSE(  ny_True[2],  ny[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( e1z_True[0], e1z[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1z_True[1], e1z[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e1z_True[2], e1z[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2z_True[0], e2z[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2z_True[1], e2z[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( e2z_True[2], e2z[2], small_tol, close_tol );
  SANS_CHECK_CLOSE(  nz_True[0],  nz[0], small_tol, close_tol );
  SANS_CHECK_CLOSE(  nz_True[1],  nz[1], small_tol, close_tol );
  SANS_CHECK_CLOSE(  nz_True[2],  nz[2], small_tol, close_tol );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
