// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEHSMKL_btest
//
// test of 2-D Hybrid Shell Model (HSM) PDE class

#include <boost/test/unit_test.hpp>
#include <boost/mpl/list.hpp>

#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/HSM/PDEHSMKL.h"
#include "pde/HSM/ComplianceMatrix.h"

#include "Surreal/SurrealS.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}


using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEHSMKL_test_suite )

template<class T_, class Tc_, class Tm_, class Tq_, class Tr_>
struct SurrealCombo
{
  typedef T_ T;
  typedef Tc_ Tc;
  typedef Tm_ Tm;
  typedef Tq_ Tq;
  typedef Tr_ Tr;
};

typedef boost::mpl::list< SurrealCombo<Real,Real,Real,Real,Real>,
                          SurrealCombo<SurrealS< PDEHSMKL<VarTypeLambda>::N >,Real,Real,Real,Real>,
                          SurrealCombo<Real,SurrealS<1>,SurrealS<1>,Real,Real>
                        > TTypes;

template<int N, class T>
T SurrealValue(const SurrealS<N,T>& s) { return s.value(); }

Real SurrealValue(const Real& s) { return s; }

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDEHSMKL<VarTypeLambda> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( PDEClass::D == 2 );
  BOOST_CHECK( PDEClass::N == 6 );
  BOOST_CHECK( ArrayQ::M == 6 );
  BOOST_CHECK( MatrixQ::M == 6 );
  BOOST_CHECK( MatrixQ::N == 6 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef PDEHSMKL<VarTypeLambda> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::VectorX<Real> VectorX;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  VectorX g = { 0.023, -9.81 };
  PDEClass hsm(g, PDEClass::HSM_ResidInterp_Separate);

  BOOST_CHECK( hsm.D == 2 );
  BOOST_CHECK( hsm.N == 6 );
  BOOST_CHECK( hsm.g_[0] == g[0] );
  BOOST_CHECK( hsm.g_[1] == g[1] );

  // State vector
  Real rx, ry, lam3, f11, f22, f12;
  rx   = 3.263;
  ry   = -0.445;
  lam3 = 1./4.;
  f11  = 3.14;
  f22  = 42;
  f12  = 0.56;
  ArrayQ varTrue = {rx, ry, lam3, f11, f22, f12};

  // Set DOFs with log-quaternion-based object (PositionLambdaForces2D)
  PositionLambdaForces2D varData;
  varData.rx    = rx;
  varData.ry    = ry;
  varData.lam3  = lam3;
  varData.f11   = f11;
  varData.f22   = f22;
  varData.f12   = f12;

  ArrayQ var1;
  hsm.setDOFFrom( var1, varData );
  for (int i = 0; i < hsm.N; i++)
  {
    SANS_CHECK_CLOSE( varTrue(i), var1(i), small_tol, close_tol );
  }

  ArrayQ var2 = hsm.setDOFFrom( varData );
  for (int i = 0; i < hsm.N; i++)
  {
    SANS_CHECK_CLOSE( varTrue(i), var2(i), small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( matrix_transformations )
{
  // Test protected matrix transformation functions
  typedef PDEHSMKL<VarTypeLambda> PDEClass;
  typedef PDEHSMKL<VarTypeLambda>::VectorX<Real> VectorX;
  typedef PDEHSMKL<VarTypeLambda>::MatrixX<Real> MatrixX;

  const Real small_tol = 1.5e-12;
  const Real close_tol = 1.5e-12;

  VectorX g = { 0.023, -9.81 };
  PDEClass hsm(g, PDEClass::HSM_ResidInterp_Separate);

  Real e1x =  3./5.;
  Real e1y =  4./5.;
  Real e2x = -4./5.;
  Real e2y =  3./5.;

  MatrixX e = {{ e1x, e2x },
               { e1y, e2y }};

  // Local to cartesian transformation
  Real f11 = 4.32;
  Real f22 = 0.37;
  Real f12 = -1.9;
  DLA::VectorS<3,Real> flocvec = { f11, f22, f12 };

  // Transformation matrix from {f11,f22,f12} to {fxx,fyy,fxy}
  DLA::MatrixS<3,3,Real> Tloc2glob;

  Tloc2glob = {{ pow(e1x,2), pow(e2x,2),    2*e1x*e2x    },
               { pow(e1y,2), pow(e2y,2),    2*e1y*e2y    },
               {  e1x*e1y  ,  e2x*e2y  , e1x*e2y+e1y*e2x }};

  DLA::VectorS<3,Real> fcartvec = Tloc2glob * flocvec;

  // Using HSM function
  MatrixX floc = {{ f11, f12 },
                  { f12, f22 }};
  MatrixX fcart;

  hsm.loc2cart( e, floc, fcart );

  //                  True      Test
  SANS_CHECK_CLOSE( fcartvec[0], fcart(0,0), small_tol, close_tol ); // fxx
  SANS_CHECK_CLOSE( fcartvec[1], fcart(1,1), small_tol, close_tol ); // fyy
  SANS_CHECK_CLOSE( fcartvec[2], fcart(1,0), small_tol, close_tol ); // fxy
  SANS_CHECK_CLOSE( fcartvec[2], fcart(0,1), small_tol, close_tol ); // fxy

  // Cartesian to local transformation
  Real fxx = 4.32;
  Real fyy = 0.37;
  Real fxy = -1.9;
  fcartvec = { fxx, fyy, fxy };

  // Transformation matrix from {fxx,fyy,fxy} to {f11,f22,f12}
  DLA::MatrixS<3,3,Real> Tglob2loc;

  Tglob2loc = {{ pow(e1x,2), pow(e1y,2),    2*e1x*e1y    },
               { pow(e2x,2), pow(e2y,2),    2*e2x*e2y    },
               {  e1x*e2x  ,  e1y*e2y  , e1x*e2y+e1y*e2x }};

  flocvec = Tglob2loc * fcartvec;

  // Using HSM function
  fcart = {{ fxx, fxy },
           { fxy, fyy }};

  hsm.cart2loc( e, fcart, floc );

  //                  True      Test
  SANS_CHECK_CLOSE( flocvec[0], floc(0,0), small_tol, close_tol ); // f11
  SANS_CHECK_CLOSE( flocvec[1], floc(1,1), small_tol, close_tol ); // f22
  SANS_CHECK_CLOSE( flocvec[2], floc(1,0), small_tol, close_tol ); // f12
  SANS_CHECK_CLOSE( flocvec[2], floc(0,1), small_tol, close_tol ); // f12

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fbar )
{
  // Test fbar calculations
  typedef PDEHSMKL<VarTypeLambda> PDEClass;
  typedef PDEHSMKL<VarTypeLambda>::ArrayQ<Real> ArrayQ;
  typedef PDEHSMKL<VarTypeLambda>::VectorX<Real> VectorX;
  typedef PDEHSMKL<VarTypeLambda>::MatrixX<Real> MatrixX;
  typedef PDEHSMKL<VarTypeLambda>::MatrixSymX<Real> MatrixSymX;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  VectorX g = { 0.023, -9.81 };
  PDEClass hsm(g, PDEClass::HSM_ResidInterp_Separate);

  Real e1x =  3./5.;
  Real e1y =  4./5.;
  Real e2x = -4./5.;
  Real e2y =  3./5.;

  MatrixX e = {{ e1x, e2x },
               { e1y, e2y }};

  // State vector
  Real rx, ry, lam3, f11, f22, f12;
  rx   = 3.263;
  ry   = -0.445;
  lam3 = 1./4.;
  f11  = 3.14;
  f22  = 42;
  f12  = 0.56;
  PositionLambdaForces2D varData = {rx, ry, lam3, f11, f22, f12};
  ArrayQ var = hsm.setDOFFrom( varData );

  MatrixSymX flocbar = {{ f11 },
                        { f12, f22 }};

  // Calculate true quantity (loc2cart tested above)
  MatrixSymX fbar_True;
  hsm.loc2cart( e, flocbar, fbar_True );

  // Use HSM method
  MatrixSymX fbar;
  hsm.calc_fbar( var, e, fbar );

  SANS_CHECK_CLOSE( fbar_True(0,0), fbar(0,0) , small_tol, close_tol ); // fxx
  SANS_CHECK_CLOSE( fbar_True(1,1), fbar(1,1) , small_tol, close_tol ); // fyy
  SANS_CHECK_CLOSE( fbar_True(1,0), fbar(1,0) , small_tol, close_tol ); // fxy

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( epsbar_force, Combo, TTypes )
{
  // Test calculation for strain as a function of the force resultants

  typedef typename Combo::T T;
  typedef typename Combo::Tc Tc;
  typedef typename Combo::Tm Tm;
  typedef typename Combo::Tq Tq;
  typedef typename Combo::Tr Tr;
  typedef typename promote_Surreal<T,Tc>::type Ti;

  typedef PDEHSMKL<VarTypeLambda> PDEClass;
  typedef typename PDEClass::template ArrayQ<T> ArrayQ;
  typedef typename PDEClass::template ParamsType<Tc, Tm, Tq> ParamsType;
  typedef typename PDEClass::template VectorX<Real> VectorX_R;
  typedef typename PDEClass::template VectorX<Tq> VectorX_Tq;
  typedef typename PDEClass::template VectorE<Tr> VectorE;
  typedef typename PDEClass::template MatrixX<Real> MatrixX_R;
  typedef typename PDEClass::template MatrixX<T > MatrixX_T;
  typedef typename PDEClass::template MatrixSymX<Ti> MatrixSymX_T;
  typedef typename PDEClass::template ComplianceMatrix<Tc> ComplianceMatrix;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  VectorX_R g = { 0.023, -9.81 };
  PDEClass hsm(g, PDEClass::HSM_ResidInterp_Separate);

  Real e1x =  3./5.;
  Real e1y =  4./5.;
  Real e2x = -4./5.;
  Real e2y =  3./5.;

  MatrixX_T e = {{ e1x, e2x },
                 { e1y, e2y }};

  // State vector
  Real rx, ry, lam3, f11, f22, f12;
  rx   = 3.263;
  ry   = -0.445;
  lam3 = 1./4.;
  f11  = 3.14;
  f22  = 42;
  f12  = 0.56;
  PositionLambdaForces2D varData = {rx, ry, lam3, f11, f22, f12};
  ArrayQ var = hsm.setDOFFrom( varData );

  // Parameters

  // Refernce basis vectors
  MatrixX_R e0 = {{ 1, 0 },
                  { 0, 1 }};

  // Material properties
  Real E = 70e9;
  Real nu = 0.33;
  Real t = 0.01;
  Real mu = 0.273;
  ComplianceMatrix AijInv = calcComplianceMatrix( E, nu, t );

  // Forces/accelerations
  VectorE qe    = { 1.3, -0.46 };
  VectorX_Tq qx = { 4.1,  8.24 };
  VectorX_Tq a  = { -2., 1./7. };

  // Insert into parameter fields
  ParamsType params( e0, AijInv, mu, qe, qx, a );

  // Force dependent local strain tensor
  MatrixSymX_T epsbar_force;

  // Compute epsloc_force
  hsm.calc_epsbar_force( params, var, e, epsbar_force );


  // Compute true epsloc_force
  DLA::VectorS<3,T> floc = { f11, f22, f12 };

  DLA::VectorS<3,Ti> epsloc_forceTrue = AijInv * floc;

  MatrixSymX_T epslocbarTrue = { {epsloc_forceTrue[0]},
                                 {epsloc_forceTrue[2], epsloc_forceTrue[1]} };

  MatrixSymX_T epsbar_forceTrue;
  hsm.loc2cart( e, epslocbarTrue, epsbar_forceTrue );

  SANS_CHECK_CLOSE( SurrealValue(epsbar_forceTrue(0,0)), SurrealValue(epsbar_force(0,0)), small_tol, close_tol );
  SANS_CHECK_CLOSE( SurrealValue(epsbar_forceTrue(1,1)), SurrealValue(epsbar_force(1,1)), small_tol, close_tol );
  SANS_CHECK_CLOSE( SurrealValue(epsbar_forceTrue(1,0)), SurrealValue(epsbar_force(1,0)), small_tol, close_tol );
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( epsloc_geom_metric )
{
  // Test calculation for strain as a function of the deformed geometry
  typedef PDEHSMKL<VarTypeLambda> PDEClass;
  typedef PDEHSMKL<VarTypeLambda>::VectorX<Real> VectorX;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  // Just test for VarTypeLambda, calculations the same as for VarTypeTheta
  PDEClass hsm;

  // Undeformed local basis vectors
  VectorX e01 = {4./5., 3./5.}, e02 = {-3./5., 4./5.};

  // Undeformed geometry local-coordinate-derivatives
  DLA::VectorS<2,Real> r0xi  = { 1.6, -3.2 };
  DLA::VectorS<2,Real> r0eta = { -0.3, 5.9 };

  // Metric tensor
  // Deformed
  DLA::MatrixSymS<2,Real> gij = {{ 2.13 },
                                 { -0.5, 1.75 }};
  // Undeformed and components
  DLA::MatrixSymS<2,Real> g0ij = {{ 1.39 },
                                  { 2.07, -0.9 }};
  Real a = g0ij(0,0);
  Real b = g0ij(1,0);
  Real d = g0ij(1,1);

  // Geometry dependent strain tensor
  DLA::VectorS<3,Real> epsij_geom;

  // Compute epsij_geom
  hsm.calc_epsij_geom(g0ij, gij, r0xi, r0eta, e01, e02, epsij_geom);
  // Components of epsij_force
  Real& eps11_geom = epsij_geom[0];
  Real& eps22_geom = epsij_geom[1];
  Real& eps12_geom = epsij_geom[2];

  // Undeformed metric tensor inverse
  Real InvDet_g0ij = 1.0 / (a*d-b*b);
  DLA::MatrixSymS<2,Real> g0ijAdj = {{ d },{ -b, a }};
  DLA::MatrixSymS<2,Real> g0ijInv = InvDet_g0ij * g0ijAdj;

  // Undeformed contravariant vectors (Eq. 5,6)
  DLA::VectorS<2,Real> bxi0  = g0ijInv * r0xi;
  DLA::VectorS<2,Real> beta0 = g0ijInv * r0eta;

  // Strain tensor associated with the covariant tangential basis vectors (Eq. 7)
  //   (parametric derivates of the shell geometry function)
  DLA::MatrixSymS<2,Real> epsbrv = 0.5 * (gij - g0ij);
  Real& epsbrv11 = epsbrv(0,0);
  Real& epsbrv21 = epsbrv(1,0);
  Real& epsbrv22 = epsbrv(1,1);

  // Convert to 12 basis (Eq. 8)
  Real bxi0_dot_e01 = dot(bxi0,e01);
  Real bxi0_dot_e02 = dot(bxi0,e02);
  Real beta0_dot_e01 = dot(beta0,e01);
  Real beta0_dot_e02 = dot(beta0,e02);

  // Compute true epsij_geom
  Real eps11_geomTrue = epsbrv11 * bxi0_dot_e01  * bxi0_dot_e01
                      + epsbrv21 * bxi0_dot_e01  * beta0_dot_e01
                      + epsbrv21 * beta0_dot_e01 * bxi0_dot_e01
                      + epsbrv22 * beta0_dot_e01 * beta0_dot_e01;
  Real eps12_geomTrue = epsbrv11 * bxi0_dot_e02  * bxi0_dot_e01
                      + epsbrv21 * bxi0_dot_e02  * beta0_dot_e01
                      + epsbrv21 * beta0_dot_e02 * bxi0_dot_e01
                      + epsbrv22 * beta0_dot_e02 * beta0_dot_e01;
  Real eps22_geomTrue = epsbrv11 * bxi0_dot_e02  * bxi0_dot_e02
                      + epsbrv21 * bxi0_dot_e02  * beta0_dot_e02
                      + epsbrv21 * beta0_dot_e02 * bxi0_dot_e02
                      + epsbrv22 * beta0_dot_e02 * beta0_dot_e02;

  SANS_CHECK_CLOSE( eps11_geomTrue , eps11_geom , small_tol, close_tol );
  SANS_CHECK_CLOSE( eps22_geomTrue , eps22_geom , small_tol, close_tol );
  SANS_CHECK_CLOSE( eps12_geomTrue , eps12_geom , small_tol, close_tol );
}

#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( epsbar_geom )
{
  // Test calculation for strain as a function of the deformed geometry
  typedef PDEHSMKL<VarTypeLambda> PDEClass;
  typedef PDEHSMKL<VarTypeLambda>::VectorX<Real> VectorX;
  typedef PDEHSMKL<VarTypeLambda>::MatrixX<Real> MatrixX;
  typedef PDEHSMKL<VarTypeLambda>::MatrixSymX<Real> MatrixSymX;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  VectorX g = { 0.023, -9.81 };
  PDEClass hsm(g, PDEClass::HSM_ResidInterp_Separate);

  // Local basis
  Real e1x =  3./5.;
  Real e1y =  4./5.;
  Real e2x = -4./5.;
  Real e2y =  3./5.;

  MatrixX e = {{ e1x, e2x },
               { e1y, e2y }};

  // Undeformed geometry local-coordinate-derivatives
  VectorX r0xi  = { 1.6, -3.2 };
  VectorX r0eta = { -0.3, 5.9 };

  // Deformed geometry local-coordinate-derivatives
  VectorX rxi  = { 1./6., -3./2. };
  VectorX reta = { -6./7., 5./9. };

  // Geometry parametric derivatives
  MatrixX dr0 = {{ r0xi[0] , r0xi[1]  },
                 { r0eta[0], r0eta[1] }};
  MatrixX dr  = {{ rxi[0]  , rxi[1]   },
                 { reta[0] , reta[1]  }};

#if 1
  // Lagrangian strain
  // ~> Deformed position surface gradient wrt undeformed geometry

  // Solve 2x2 system
  MatrixX dr0inv = DLA::InverseLUP::Inverse( dr0 );
  MatrixX deltld0r = dr0inv * dr;

  // Undeformed geometry surface gradient is identity by construction
  MatrixSymX deltld0r0 = DLA::Identity();

  // Calculate strains in cartesian coordinates
  MatrixSymX epsbar_geomTrue = deltld0r * Transpose(deltld0r);
             epsbar_geomTrue = 0.5*(epsbar_geomTrue - deltld0r0);

#else

  // Eulerian strain
  // ~> Undeformed position surface gradient wrt deformed geometry

  // Solve 2x2 system
  MatrixX drinv = DLA::InverseLUP::Inverse( dr );
  MatrixX deltldr0 = drinv * dr0;

  // Undeformed geometry surface gradient is identity by construction
  MatrixX deltldr = DLA::Identity();

  // Calculate strains in cartesian coordinates
  MatrixX epsbar_geomTrue = 0.5 * (deltldr - deltldr0 * Transpose(deltldr0));
#endif

  // Calculate strains with HSM function
  MatrixSymX epsbar_geom;
  hsm.calc_epsbar_geom( {r0xi, r0eta}, {rxi, reta}, epsbar_geom );

  SANS_CHECK_CLOSE( epsbar_geomTrue(0,0) , epsbar_geom(0,0) , small_tol, close_tol ); // epsxx
  SANS_CHECK_CLOSE( epsbar_geomTrue(1,1) , epsbar_geom(1,1) , small_tol, close_tol ); // epsyy
  SANS_CHECK_CLOSE( epsbar_geomTrue(1,0) , epsbar_geom(1,0) , small_tol, close_tol ); // epsxy
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  // Check that dump output remains consistent
  typedef PDEHSMKL<VarTypeLambda> PDEClass;
  typedef PDEHSMKL<VarTypeLambda>::VectorX<Real> VectorX;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/HSMKL_pattern.txt", true );

  VectorX g = { 0.023, -9.81 };
  PDEClass hsm(g, PDEClass::HSM_ResidInterp_Separate);

  // Dump IO
  hsm.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
