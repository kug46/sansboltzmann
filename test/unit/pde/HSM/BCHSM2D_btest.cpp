// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCHSM2D_btest
//
// test of PDEHSM2D BC classes

#include <boost/test/unit_test.hpp>
#include <boost/mpl/list.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "Surreal/SurrealS.h"

#include "pde/HSM/Var2DLambda.h"
#include "pde/HSM/PDEHSM2D.h"
#include "pde/HSM/BCHSM2D.h"
#include "pde/HSM/ComplianceMatrix.h"
#include "pde/BCParameters.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

//Explicitly instantiate classes so coverage information is correct
namespace SANS
{
template class BCHSM2D< BCTypeDisplacements, PDEHSM2D<VarTypeLambda> >;
template class BCHSM2D< BCTypeForces,        PDEHSM2D<VarTypeLambda> >;

// Instantiate BCParamaters here as they are only tested here and not needed in general without an ND convert class
template struct BCParameters< BCHSM2DVector<VarTypeLambda> >;

}


using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BCHSM2D_test_suite )

template<class T_, class Tc_, class Tm_, class Tq_, class Tr_>
struct SurrealCombo
{
  typedef T_ T;
  typedef Tc_ Tc;
  typedef Tm_ Tm;
  typedef Tq_ Tq;
  typedef Tr_ Tr;
};

typedef boost::mpl::list< SurrealCombo<Real,Real,Real,Real,Real>,
                          SurrealCombo<SurrealS< PDEHSM2D<VarTypeLambda>::N >,Real,Real,Real,Real>,
                          SurrealCombo<Real,SurrealS<1>,SurrealS<1>,Real,Real>
                        > TTypes;

template<int N, class T>
T SurrealValue(const SurrealS<N,T>& s) { return s.value(); }

Real SurrealValue(const Real& s) { return s; }

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDEHSM2D<VarTypeLambda> PDEClass;

  {
  typedef BCHSM2D<BCTypeDisplacements, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 6 );
  BOOST_CHECK( BCClass::NBC == 2 );
  BOOST_CHECK( ArrayQ::M == 6 );
  BOOST_CHECK( MatrixQ::M == 6 );
  BOOST_CHECK( MatrixQ::N == 6 );
  }

  {
  typedef BCHSM2D<BCTypeForces, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 6 );
  BOOST_CHECK( BCClass::NBC == 2 );
  BOOST_CHECK( ArrayQ::M == 6 );
  BOOST_CHECK( MatrixQ::M == 6 );
  BOOST_CHECK( MatrixQ::N == 6 );
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor )
{
  typedef PDEHSM2D<VarTypeLambda> PDEClass;

  {
    typedef BCHSM2D<BCTypeDisplacements, PDEClass> BCClass;
    typedef BCClass::VectorX<Real> VectorX;
    typedef BCParameters< BCHSM2DVector<VarTypeLambda> > BCParams;

    // Inputs for BCHSM2D
    VectorX g = { 0.023, -9.81 };
    PDEClass pde(g, PDEClass::HSM_ResidInterp_Separate);

    Real rshiftx = 0.21;
    Real rshifty = -0.043;

    // Standard constructor
    BCClass bc1(pde, rshiftx, rshifty);

    //Pydict constructor
    PyDict BCDisp;
    BCDisp[BCParams::params.BC.BCType] = BCParams::params.BC.Displacements;
    BCDisp[BCHSM2DParams<BCTypeDisplacements>::params.rshiftx] = rshiftx;
    BCDisp[BCHSM2DParams<BCTypeDisplacements>::params.rshifty] = rshifty;
    BCClass bc2(pde, BCDisp);

  }

  {
    typedef BCHSM2D<BCTypeForces, PDEClass> BCClass;
    typedef BCClass::VectorX<Real> VectorX;
    typedef BCClass::VectorE<Real> VectorE;
    typedef BCParameters< BCHSM2DVector<VarTypeLambda> > BCParams;

    // Inputs for BCHSM2D
    VectorX g = { 0.023, -9.81 };
    PDEClass pde(g, PDEClass::HSM_ResidInterp_Separate);

    VectorE flocBC = { -1.1, 2.43 };

    // Standard constructor
    BCClass bc1(pde, flocBC[0], flocBC[1]);

    //Pydict constructor
    PyDict BCForce;
    BCForce[BCParams::params.BC.BCType] = BCParams::params.BC.Forces;
    BCForce[BCHSM2DParams<BCTypeForces>::params.f1BC] = flocBC[0];
    BCForce[BCHSM2DParams<BCTypeForces>::params.f2BC] = flocBC[1];

    BCClass bc2(pde, BCForce);
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( functions_displacements, Combo, TTypes )
{
  typedef typename Combo::T T;
  typedef typename Combo::Tc Tc;
  typedef typename Combo::Tm Tm;
  typedef typename Combo::Tq Tq;
  typedef typename Combo::Tr Tr;
  typedef typename promote_Surreal<T,Tc,Tm,Tq,Tr>::type Ts;

  typedef PDEHSM2D<VarTypeLambda> PDEClass;
  typedef typename PDEClass::template ParamsType<Tc, Tm, Tq> ParamsType;
  typedef typename PDEClass::template VectorE<Tr> VectorE;
  typedef typename PDEClass::template ComplianceMatrix<Tc> ComplianceMatrix;

  typedef BCHSM2D<BCTypeDisplacements, PDEClass> BCClass;
  typedef typename BCClass::template ArrayQ<T> ArrayQ;
  typedef typename BCClass::template ArrayQ<Ts> ArrayQS;
  typedef typename BCClass::template MatrixQ<Ts> MatrixQS;
  typedef typename BCClass::template MatrixQ<Real> MatrixQ_R;
  typedef typename BCClass::template VectorX<Real> VectorX_R;
  typedef typename BCClass::template VectorX<Tq> VectorX_Tq;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  VectorX_R g = { 0.023, -9.81 };
  PDEClass pde(g, PDEClass::HSM_ResidInterp_Separate);

  Real rshiftx = 0.21;
  Real rshifty = -0.043;

  BCClass bc(pde, rshiftx, rshifty);

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 6 );
  BOOST_CHECK( bc.NBC == 2 );

  // BC inputs
  Real time;
  ArrayQ var, varx, vary, varxi, vareta;

  time = 0;   // not actually used in functions

  // Parameters

  // Material properties
  Real E = 70e9;
  Real nu = 0.33;
  Real t = 0.01;
  Real mu = 0.273;
  ComplianceMatrix AijInv = calcComplianceMatrix( E, nu, t );

  // Forces/accelerations
  VectorE qe = { 1.3, -0.46 };
  VectorX_Tq qx = { 4.1,  8.24 };
  VectorX_Tq a  = { -2., 1./7. };

  // Insert into parameter tuple
  ParamsType params( AijInv, mu, qe, qx, a );

  // Undeformed position and parametric derivs
  Real x0, y0, x0xi, y0xi, x0eta, y0eta;
  x0 = 0.329;
  y0 = 4.876;
  x0xi = 43./65.;
  y0xi = 0;
  x0eta = 0;
  y0eta = -81./40.;

  // Boundary unit normal
  Real nx, ny;
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1

  // State vector
  Real x, y, lam3, f11, f22, f12;
  x   = 3.263;
  y   = -0.445;
  lam3 =  1./6.;
  f11  = 3.14;
  f22  = 42;
  f12  = 0.56;
  var = {x, y, lam3, f11, f22, f12};

  // State vector x-coordinate derivatives
  Real xx, yx, lam3x, f11x, f22x, f12x;
  xx    = -2.1;
  yx    = 4.12;
  lam3x = 3./7.;
  f11x  = -0.32;
  f22x  = 5.3;
  f12x  = 6.2;
  varx = {xx, yx, lam3x, f11x, f22x, f12x};

  // State vector y-coordinate derivatives
  Real xy, yy, lam3y, f11y, f22y, f12y;
  xy   = 8.14;
  yy   = 4.21;
  lam3y = 2.2;
  f11y  = -4.7;
  f22y  = 0.75;
  f12y  = 2.11;
  vary = {xy, yy, lam3y, f11y, f22y, f12y};

  // State vector xi-coordinate derivatives
  Real xxi, yxi, lam3xi, f11xi, f22xi, f12xi;
  xxi = 4.36;
  yxi = -2.87;
  lam3xi = 9.45;
  f11xi = 27./5.;
  f22xi = 9.78;
  f12xi = -3.2;
  varxi = {xxi, yxi, lam3xi, f11xi, f22xi, f12xi};

  // State vector eta-coordinate derivatives
  Real xeta, yeta, lam3eta, f11eta, f22eta, f12eta;
  xeta = 13./45.;
  yeta = 2.459;
  lam3eta = 0.126;
  f11eta = -6.9;
  f22eta = -0.664;
  f12eta = 6.542;
  vareta = {xeta, yeta, lam3eta, f11eta, f22eta, f12eta};

  // Calculate strong-form BC
  ArrayQS strBCTrue = 0;
  strBCTrue[0] = x - (x0 + rshiftx);
  strBCTrue[1] = y - (y0 + rshifty);

  // Use bc object method to calculate strong-form BC
  ArrayQS strBC = 0;
  bc.strongBC( params,
               x0, y0, time,
               x0xi, x0eta,
               y0xi, y0eta,
               nx, ny,
               var,
               varx, vary,
               varxi, vareta,
               strBC );

  // Check results
  for (int n = 0; n < bc.N; n++)
  {
    SANS_CHECK_CLOSE( SurrealValue(strBCTrue(n)), SurrealValue(strBC(n)), small_tol, close_tol );
  }

  // Calculate BC weight
  Real eqax = -0.5223251367927014;
  Real eqay = -4.054796749475282;
  Real eqbx = 5.353205136792702;
  Real eqby = 1.6803967494752812;
  Real eqcx = -6.0122993940905385;
  Real eqcy = 1.343895654280944;
  Real eqdx = 0;
  Real eqdy = 0;
  Real eqex = 0;
  Real eqey = 0;
  Real eqfx = 1.133841204310926;
  Real eqfy = -14.142357113056558;

  MatrixQ_R wghtBCTrue = { {eqax, eqay, 0, 0, 0, 0},
                           {eqbx, eqby, 0, 0, 0, 0},
                           {eqcx, eqcy, 0, 0, 0, 0},
                           {eqdx, eqdy, 0, 0, 0, 0},
                           {eqex, eqey, 0, 0, 0, 0},
                           {eqfx, eqfy, 0, 0, 0, 0} };

  // string names[12] = { "eqax", "eqay",
  //                      "eqbx", "eqby",
  //                      "eqcx", "eqcy",
  //                      "eqdx", "eqdy",
  //                      "eqex", "eqey",
  //                      "eqfx", "eqfy" };

  // Use bc object method to calculate BC weight
  MatrixQS wghtBC = 0;
  bc.weightBC( params,
               x0, y0, time,
               x0xi, x0eta,
               y0xi, y0eta,
               nx, ny,
               var,
               varx, vary,
               varxi, vareta,
               wghtBC );

  // Check results
  for (int n = 0; n < bc.N; n++)
  {
    for (int m = 0; m < bc.N; m++)
    {
      // cout << n << "," << m;
      // if ( m < 2 )
      //   cout << " : " << names[n*2+m];
      // cout << endl;

      SANS_CHECK_CLOSE( wghtBCTrue(n,m), SurrealValue(wghtBC(n,m)), small_tol, close_tol );
    }
  }


}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functions_forces )
{
  typedef PDEHSM2D<VarTypeLambda> PDEClass;
  typedef PDEClass::ParamsType<Real,Real,Real> ParamsType;
  typedef BCHSM2D<BCTypeForces, PDEClass> BCClass;
  typedef BCClass::ArrayQ<Real> ArrayQ;
  typedef BCClass::MatrixQ<Real> MatrixQ;
  typedef BCClass::VectorX<Real> VectorX;
  typedef PDEClass::VectorE<Real> VectorE;
  typedef PDEClass::ComplianceMatrix<Real> ComplianceMatrix;


  const Real small_tol = 1.e-13;
  const Real close_tol = 5.e-13;

  VectorX g = { 0.023, -9.81 };
  PDEClass pde(g, PDEClass::HSM_ResidInterp_Separate);

  Real f1BC = -1.1;
  Real f2BC = 2.43;

  BCClass bc( pde, f1BC, f2BC );

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 6 );
  BOOST_CHECK( bc.NBC == 2 );

  // BC inputs
  Real time;
  ArrayQ var, varx, vary, varxi, vareta;

  time = 0;   // not actually used in functions

  // Parameters

  // Material properties
  Real E = 70e9;
  Real nu = 0.33;
  Real t = 0.01;
  Real mu = 0.273;
  ComplianceMatrix AijInv = calcComplianceMatrix( E, nu, t );

  // Forces/accelerations
  VectorE qe = { 1.3, -0.46 };
  VectorX qx = { 4.1,  8.24 };
  VectorX a  = { -2., 1./7. };

  // Insert into parameter tuple
  ParamsType params( AijInv, mu, qe, qx, a );

  // Undeformed position and parametric derivs
  Real x0, y0, x0xi, y0xi, x0eta, y0eta;
  x0 = 0.329;
  y0 = 4.876;
  x0xi = 43./65.;
  y0xi = 0;
  x0eta = 0;
  y0eta = -81./40.;

  // Boundary unit normal
  Real nx, ny;
  nx = -0.936;  ny = 0.352;   // Note: magnitude = 1

  // State vector
  Real x, y, lam3, f11, f22, f12;
  x   = 3.263;
  y   = -0.445;
  lam3 =  1./6.;
  f11  = 3.14;
  f22  = 42;
  f12  = 0.56;
  var = {x, y, lam3, f11, f22, f12};

  // State vector x-coordinate derivatives
  Real xx, yx, lam3x, f11x, f22x, f12x;
  xx   = -2.1;
  yx   = 4.12;
  lam3x = 3./7.;
  f11x  = -0.32;
  f22x  = 5.3;
  f12x  = 6.2;
  varx = {xx, yx, lam3x, f11x, f22x, f12x};

  // State vector y-coordinate derivatives
  Real xy, yy, lam3y, f11y, f22y, f12y;
  xy   = 8.14;
  yy   = 4.21;
  lam3y = 2.2;
  f11y  = -4.7;
  f22y  = 0.75;
  f12y  = 2.11;
  vary = {xy, yy, lam3y, f11y, f22y, f12y};

  // State vector xi-coordinate derivatives
  Real xxi, yxi, lam3xi, f11xi, f22xi, f12xi;
  xxi = 4.36;
  yxi = -2.87;
  lam3xi = 9.45;
  f11xi = 27./5.;
  f22xi = 9.78;
  f12xi = -3.2;
  varxi = {xxi, yxi, lam3xi, f11xi, f22xi, f12xi};

  // State vector eta-coordinate derivatives
  Real xeta, yeta, lam3eta, f11eta, f22eta, f12eta;
  xeta = 13./45.;
  yeta = 2.459;
  lam3eta = 0.126;
  f11eta = -6.9;
  f22eta = -0.664;
  f12eta = 6.542;
  vareta = {xeta, yeta, lam3eta, f11eta, f22eta, f12eta};

  // Calculate strong-form BC
  ArrayQ strBCTrue = 0;
  strBCTrue[0] = -0.9578522234917837;
  strBCTrue[1] = 23.972109400419548;

  // Use bc object method to calculate strong-form BC
  ArrayQ strBC = 0;
  bc.strongBC( params,
               x0, y0, time,
               x0xi, x0eta,
               y0xi, y0eta,
               nx, ny,
               var,
               varx, vary,
               varxi, vareta,
               strBC );

  // Check results
  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( strBCTrue(n), strBC(n), small_tol, close_tol );
  }

  // Calculate BC weight
  Real eqax = 0;
  Real eqay = 0;
  Real eqbx = 0;
  Real eqby = 0;
  Real eqcx = 0;
  Real eqcy = 0;
  Real eqdx = 1;
  Real eqdy = 0;
  Real eqex = 0;
  Real eqey = 1;
  Real eqfx = 0;
  Real eqfy = 0;

  Real wghtBCdata[36] = { eqax, eqay, 0, 0, 0, 0,
                          eqbx, eqby, 0, 0, 0, 0,
                          eqcx, eqcy, 0, 0, 0, 0,
                          eqdx, eqdy, 0, 0, 0, 0,
                          eqex, eqey, 0, 0, 0, 0,
                          eqfx, eqfy, 0, 0, 0, 0 };
  MatrixQ wghtBCTrue( wghtBCdata, 36 );

  // Use bc object method to calculate BC weight
  MatrixQ wghtBC = 0;
  bc.weightBC( params,
               x0, y0, time,
               x0xi, x0eta,
               y0xi, y0eta,
               nx, ny,
               var,
               varx, vary,
               varxi, vareta,
               wghtBC );

  // Check results
  for (int n = 0; n < pde.N; n++)
  {
    for (int m = 0; m < pde.N; m++)
    {
      SANS_CHECK_CLOSE( wghtBCTrue(n,m), wghtBC(n,m), small_tol, close_tol );
    }
  }

}


#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/NS/BCEuler2D_pattern.txt", true );

  typedef PDEHSM2D<VarType Theta> PDEClass;
  typedef BCClass::VectorX<Real> VectorX;

  VectorX g = { 0.023, -9.81 };
  PDEClass pde(g, PDEClass::HSM_ResidInterp_Separate);

  {
  typedef BCHSM2D<BCTypeDisplacements, PDEClass> BCClass;

  VectorX rshift = { 0.21, -0.043 };
  Real xmax = 9.32;
  Real ymax = 4.51;

  BCClass bc1(pde, rshift, xmax, ymax);

  bc.dump( 2, output );
  }

  {
  typedef BCHSM2D<BCTypeForces, PDEClass> BCClass;
  typedef BCClass::MatrixSymX<Real> MatrixSymX;

  MatrixSymX flocBC = {{ -1.1 },
                      {  2.43 , 3.88 }};
  Real xmax = 9.32;
  Real ymax = 4.51;

  BCClass bc1(pde, flocBC, xmax, ymax);

  bc.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
