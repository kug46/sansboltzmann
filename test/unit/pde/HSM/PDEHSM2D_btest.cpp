// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEHSM2D_btest
//
// test of 2-D Hybrid Shell Model (HSM) PDE class

#include <boost/test/unit_test.hpp>
#include <boost/mpl/list.hpp>

#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/HSM/PDEHSM2D.h"
#include "pde/HSM/ComplianceMatrix.h"

#include "Surreal/SurrealS.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}


using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEHSM2D_test_suite )

template<class T_, class Tc_, class Tm_, class Tq_, class Tr_>
struct SurrealCombo
{
  typedef T_ T;
  typedef Tc_ Tc;
  typedef Tm_ Tm;
  typedef Tq_ Tq;
  typedef Tr_ Tr;
};

typedef boost::mpl::list< SurrealCombo<Real,Real,Real,Real,Real>,
                          SurrealCombo<SurrealS< PDEHSM2D<VarTypeLambda>::N >,Real,Real,Real,Real>,
                          SurrealCombo<Real,SurrealS<1>,SurrealS<1>,Real,Real>
                        > TTypes;

template<int N, class T>
T SurrealValue(const SurrealS<N,T>& s) { return s.value(); }

Real SurrealValue(const Real& s) { return s; }

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDEHSM2D<VarTypeLambda> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( PDEClass::D == 2 );
  BOOST_CHECK( PDEClass::N == 6 );
  BOOST_CHECK( ArrayQ::M == 6 );
  BOOST_CHECK( MatrixQ::M == 6 );
  BOOST_CHECK( MatrixQ::N == 6 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  // Check constructor for Quaternion-templated HSM2D
  typedef PDEHSM2D<VarTypeLambda> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::VectorX<Real> VectorX;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  VectorX g = { 0.023, -9.81 };
  PDEClass hsm(g, PDEClass::HSM_ResidInterp_Separate);

  BOOST_CHECK( hsm.D == 2 );
  BOOST_CHECK( hsm.N == 6 );
  BOOST_CHECK( hsm.g_[0] == g[0] );
  BOOST_CHECK( hsm.g_[1] == g[1] );

  // State vector
  Real rx, ry, lam3, f11, f22, f12;
  rx   = 3.263;
  ry   = -0.445;
  lam3 = 1./4.;
  f11  = 3.14;
  f22  = 42;
  f12  = 0.56;
  ArrayQ varTrue = {rx, ry, lam3, f11, f22, f12};
  ArrayQ var;

  // Set DOFs with log-quaternion-based object (PositionLambdaForces2D)
  PositionLambdaForces2D varData;
  varData.rx    = rx;
  varData.ry    = ry;
  varData.lam3  = lam3;
  varData.f11   = f11;
  varData.f22   = f22;
  varData.f12   = f12;

  hsm.setDOFFrom( var, varData );
  for (int i = 0; i < 6; i++)
  {
    SANS_CHECK_CLOSE( varTrue(i), var(i), small_tol, close_tol );
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( matrix_transformations )
{
  // Test protected matrix transformation functions
  typedef PDEHSM2D<VarTypeLambda> PDEClass;
  typedef PDEHSM2D<VarTypeLambda>::VectorX<Real> VectorX;
  typedef PDEHSM2D<VarTypeLambda>::MatrixX<Real> MatrixX;

  const Real small_tol = 1.5e-12;
  const Real close_tol = 1.5e-12;

  VectorX g = { 0.023, -9.81 };
  PDEClass hsm(g, PDEClass::HSM_ResidInterp_Separate);

  Real e1x =  3./5.;
  Real e1y =  4./5.;
  Real e2x = -4./5.;
  Real e2y =  3./5.;

  MatrixX e = {{ e1x, e2x },{ e1y, e2y }};

  // Local to cartesian transformation
  Real f11 = 4.32;
  Real f22 = 0.37;
  Real f12 = -1.9;
  DLA::VectorS<3,Real> flocvec = { f11, f22, f12 };

  // Transformation matrix from {f11,f22,f12} to {fxx,fyy,fxy}
  DLA::MatrixS<3,3,Real> Tloc2glob;

  Tloc2glob = {{ pow(e1x,2), pow(e2x,2),    2*e1x*e2x    },
               { pow(e1y,2), pow(e2y,2),    2*e1y*e2y    },
               {  e1x*e1y  ,  e2x*e2y  , e1x*e2y+e1y*e2x }};

  DLA::VectorS<3,Real> fcartvec = Tloc2glob * flocvec;

  // Using HSM function
  MatrixX floc = {{ f11, f12 },{ f12, f22 }};
  MatrixX fcart;

  hsm.loc2cart( e, floc, fcart );

  //                  True      Test
  SANS_CHECK_CLOSE( fcartvec[0], fcart(0,0), small_tol, close_tol ); // fxx
  SANS_CHECK_CLOSE( fcartvec[1], fcart(1,1), small_tol, close_tol ); // fyy
  SANS_CHECK_CLOSE( fcartvec[2], fcart(1,0), small_tol, close_tol ); // fxy

  // Cartesian to local transformation
  Real fxx = 4.32;
  Real fyy = 0.37;
  Real fxy = -1.9;
  fcartvec = { fxx, fyy, fxy };

  // Transformation matrix from {fxx,fyy,fxy} to {f11,f22,f12}
  DLA::MatrixS<3,3,Real> Tglob2loc;

  Tglob2loc = {{ pow(e1x,2), pow(e1y,2),    2*e1x*e1y    },
               { pow(e2x,2), pow(e2y,2),    2*e2x*e2y    },
               {  e1x*e2x  ,  e1y*e2y  , e1x*e2y+e1y*e2x }};

  flocvec = Tglob2loc * fcartvec;

  // Using HSM function
  fcart = {{ fxx, fxy },{ fxy, fyy }};

  hsm.cart2loc( e, fcart, floc );

  //                  True      Test
  SANS_CHECK_CLOSE( flocvec[0], floc(0,0), small_tol, close_tol ); // f11
  SANS_CHECK_CLOSE( flocvec[1], floc(1,1), small_tol, close_tol ); // f22
  SANS_CHECK_CLOSE( flocvec[2], floc(1,0), small_tol, close_tol ); // f12

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fbar )
{
  // Test fbar calculations
  typedef PDEHSM2D<VarTypeLambda> PDEClass;
  typedef PDEHSM2D<VarTypeLambda>::ArrayQ<Real> ArrayQ;
  typedef PDEHSM2D<VarTypeLambda>::VectorX<Real> VectorX;
  typedef PDEHSM2D<VarTypeLambda>::MatrixX<Real> MatrixX;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  VectorX g = { 0.023, -9.81 };
  PDEClass hsm(g, PDEClass::HSM_ResidInterp_Separate);

  Real e1x =  3./5.;
  Real e1y =  4./5.;
  Real e2x = -4./5.;
  Real e2y =  3./5.;

  MatrixX e = {{ e1x, e2x },{ e1y, e2y }};

  // State vector
  Real rx, ry, lam3, f11, f22, f12;
  rx   = 3.263;
  ry   = -0.445;
  lam3 = 1./4.;
  f11  = 3.14;
  f22  = 42;
  f12  = 0.56;
  ArrayQ var = {rx, ry, lam3, f11, f22, f12};

  MatrixX flocbar = {{ f11, f12 },{ f12, f22 }};

  // Calculate true quantity (loc2cart tested above)
  MatrixX fbar_True;
  hsm.loc2cart( e, flocbar, fbar_True );

  // Use HSM method
  DLA::MatrixSymS<2,Real> fbar;
  hsm.calc_fbar( var, e, fbar );

  SANS_CHECK_CLOSE( fbar_True(0,0), fbar(0,0) , small_tol, close_tol ); // fxx
  SANS_CHECK_CLOSE( fbar_True(1,1), fbar(1,1) , small_tol, close_tol ); // fyy
  SANS_CHECK_CLOSE( fbar_True(1,0), fbar(1,0) , small_tol, close_tol ); // fxy

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( epsloc_force )
{
  // Test calculation for strain as a function of the force resultants
  typedef PDEHSM2D<VarTypeLambda> PDEClass;
  typedef PDEHSM2D<VarTypeLambda>::ArrayQ<Real> ArrayQ;
  typedef PDEHSM2D<VarTypeLambda>::ParamsType<Real,Real,Real> ParamsType;
  typedef PDEHSM2D<VarTypeLambda>::VectorX<Real> VectorX;
  typedef PDEHSM2D<VarTypeLambda>::VectorE<Real> VectorE;
  typedef PDEHSM2D<VarTypeLambda>::ComplianceMatrix<Real> ComplianceMatrix;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  VectorX g = { 0.023, -9.81 };
  PDEClass hsm(g, PDEClass::HSM_ResidInterp_Separate);

  // State vector quantities
  Real f11 = 4.32;
  Real f22 = 0.37;
  Real f12 = -1.9;

  // State vector
  ArrayQ var = 0;
  // Set force residuals in state vector
  var[3] = f11;
  var[4] = f22;
  var[5] = f12;
  DLA::VectorS<3,Real> floc = { f11, f22, f12 };

  // Parameters

  // Material properties
  Real E = 70e9;
  Real nu = 0.33;
  Real t = 0.01;
  Real mu = 0.273;
  ComplianceMatrix AijInv = calcComplianceMatrix( E, nu, t );

  // Forces/accelerations
  VectorE qe = { 1.3, -0.46 };
  VectorX qx = { 4.1,  8.24 };
  VectorX a  = { -2., 1./7. };

  // Insert into parameter fields
  ParamsType params( AijInv, mu, qe, qx, a );

  // Force dependent local strain tensor
  DLA::VectorS<3,Real> epsloc_force;

  // Compute epsloc_force
  hsm.calc_epsloc_force( params, var, epsloc_force );
  // Components of epsloc_force
  Real& eps11_force = epsloc_force[0];
  Real& eps22_force = epsloc_force[1];
  Real& eps12_force = epsloc_force[2];

  // Compute true epsloc_force
  DLA::VectorS<3,Real> epsloc_forceTrue = AijInv * floc;
  // Components of epsij_force
  Real& eps11_forceTrue = epsloc_forceTrue[0];
  Real& eps22_forceTrue = epsloc_forceTrue[1];
  Real& eps12_forceTrue = epsloc_forceTrue[2];

  SANS_CHECK_CLOSE( eps11_forceTrue , eps11_force , small_tol, close_tol );
  SANS_CHECK_CLOSE( eps22_forceTrue , eps22_force , small_tol, close_tol );
  SANS_CHECK_CLOSE( eps12_forceTrue , eps12_force , small_tol, close_tol );
}


#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( epsloc_geom_metric )
{
  // Test calculation for strain as a function of the deformed geometry
  typedef PDEHSM2D<VarTypeLambda> PDEClass;
  typedef PDEHSM2D<VarTypeLambda>::VectorX<Real> VectorX;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  // Just test for VarTypeLambda, calculations the same as for VarTypeTheta
  PDEClass hsm;

  // Undeformed local basis vectors
  VectorX e01 = {4./5., 3./5.}, e02 = {-3./5., 4./5.};

  // Undeformed geometry local-coordinate-derivatives
  DLA::VectorS<2,Real> r0xi  = { 1.6, -3.2 };
  DLA::VectorS<2,Real> r0eta = { -0.3, 5.9 };

  // Metric tensor
  // Deformed
  DLA::MatrixSymS<2,Real> gij = {{ 2.13 },
                                 { -0.5, 1.75 }};
  // Undeformed and components
  DLA::MatrixSymS<2,Real> g0ij = {{ 1.39 },
                                  { 2.07, -0.9 }};
  Real a = g0ij(0,0);
  Real b = g0ij(1,0);
  Real d = g0ij(1,1);

  // Geometry dependent strain tensor
  DLA::VectorS<3,Real> epsij_geom;

  // Compute epsij_geom
  hsm.calc_epsij_geom(g0ij, gij, r0xi, r0eta, e01, e02, epsij_geom);
  // Components of epsij_force
  Real& eps11_geom = epsij_geom[0];
  Real& eps22_geom = epsij_geom[1];
  Real& eps12_geom = epsij_geom[2];

  // Undeformed metric tensor inverse
  Real InvDet_g0ij = 1.0 / (a*d-b*b);
  DLA::MatrixSymS<2,Real> g0ijAdj = {{ d },{ -b, a }};
  DLA::MatrixSymS<2,Real> g0ijInv = InvDet_g0ij * g0ijAdj;

  // Undeformed contravariant vectors (Eq. 5,6)
  DLA::VectorS<2,Real> bxi0  = g0ijInv * r0xi;
  DLA::VectorS<2,Real> beta0 = g0ijInv * r0eta;

  // Strain tensor associated with the covariant tangential basis vectors (Eq. 7)
  //   (parametric derivates of the shell geometry function)
  DLA::MatrixSymS<2,Real> epsbrv = 0.5 * (gij - g0ij);
  Real& epsbrv11 = epsbrv(0,0);
  Real& epsbrv21 = epsbrv(1,0);
  Real& epsbrv22 = epsbrv(1,1);

  // Convert to 12 basis (Eq. 8)
  Real bxi0_dot_e01 = dot(bxi0,e01);
  Real bxi0_dot_e02 = dot(bxi0,e02);
  Real beta0_dot_e01 = dot(beta0,e01);
  Real beta0_dot_e02 = dot(beta0,e02);

  // Compute true epsij_geom
  Real eps11_geomTrue = epsbrv11 * bxi0_dot_e01  * bxi0_dot_e01
                      + epsbrv21 * bxi0_dot_e01  * beta0_dot_e01
                      + epsbrv21 * beta0_dot_e01 * bxi0_dot_e01
                      + epsbrv22 * beta0_dot_e01 * beta0_dot_e01;
  Real eps12_geomTrue = epsbrv11 * bxi0_dot_e02  * bxi0_dot_e01
                      + epsbrv21 * bxi0_dot_e02  * beta0_dot_e01
                      + epsbrv21 * beta0_dot_e02 * bxi0_dot_e01
                      + epsbrv22 * beta0_dot_e02 * beta0_dot_e01;
  Real eps22_geomTrue = epsbrv11 * bxi0_dot_e02  * bxi0_dot_e02
                      + epsbrv21 * bxi0_dot_e02  * beta0_dot_e02
                      + epsbrv21 * beta0_dot_e02 * bxi0_dot_e02
                      + epsbrv22 * beta0_dot_e02 * beta0_dot_e02;

  SANS_CHECK_CLOSE( eps11_geomTrue , eps11_geom , small_tol, close_tol );
  SANS_CHECK_CLOSE( eps22_geomTrue , eps22_geom , small_tol, close_tol );
  SANS_CHECK_CLOSE( eps12_geomTrue , eps12_geom , small_tol, close_tol );
}

#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( epsloc_geom_cart )
{
  // Test calculation for strain as a function of the deformed geometry
  typedef PDEHSM2D<VarTypeLambda> PDEClass;
  typedef PDEHSM2D<VarTypeLambda>::VectorX<Real> VectorX;
  typedef PDEHSM2D<VarTypeLambda>::MatrixX<Real> MatrixX;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  VectorX g = { 0.023, -9.81 };
  PDEClass hsm(g, PDEClass::HSM_ResidInterp_Separate);

  // Local basis
  Real e1x =  3./5.;
  Real e1y =  4./5.;
  Real e2x = -4./5.;
  Real e2y =  3./5.;
  MatrixX e = {{ e1x, e2x },{ e1y, e2y }};

  // Undeformed geometry local-coordinate-derivatives
  VectorX r0xi  = { 1.6, -3.2 };
  VectorX r0eta = { -0.3, 5.9 };

  // Deformed geometry local-coordinate-derivatives
  VectorX rxi  = { 1./6., -3./2. };
  VectorX reta = { -6./7., 5./9. };

  // Geometry parametric derivatives
  MatrixX dr0 = {{ r0xi[0] , r0xi[1]  },
                 { r0eta[0], r0eta[1] }};
  MatrixX dr  = {{ rxi[0]  , rxi[1]   },
                 { reta[0] , reta[1]  }};

#if 1
  // Lagrangian strain
  // ~> Deformed position surface gradient wrt undeformed geometry

  // Solve 2x2 system
  MatrixX dr0inv = DLA::InverseLUP::Inverse( dr0 );
  MatrixX deltld0r = dr0inv * dr;

  // Undeformed geometry surface gradient is identity by construction
  MatrixX deltld0r0 = DLA::Identity();

  // Calculate strains in cartesian coordinates
  MatrixX epscart = 0.5 * (deltld0r * Transpose(deltld0r) - deltld0r0);

#else

  // Eulerian strain
  // ~> Undeformed position surface gradient wrt deformed geometry

  // Solve 2x2 system
  MatrixX drinv = DLA::InverseLUP::Inverse( dr );
  MatrixX deltldr0 = drinv * dr0;

  // Undeformed geometry surface gradient is identity by construction
  MatrixX deltldr = DLA::Identity();

  // Calculate strains in cartesian coordinates
  MatrixX epscart = 0.5 * (deltldr - deltldr0 * Transpose(deltldr0));
#endif

  // Rotate to local frame for use in residual eqs (Eq.23)
  MatrixX epsloc;
  hsm.cart2loc( e, epscart, epsloc );

  // Return local strains as a 3-vector {eps11, eps22, eps12}
  DLA::VectorS<3,Real> epsloc_geom_True = { epsloc(0,0), epsloc(1,1), epsloc(1,0) };

  // Calculate strains with HSM function
  DLA::VectorS<3,Real> epsloc_geom;
  hsm.calc_epsloc_geom_cart( e, r0xi, r0eta, rxi, reta, epsloc_geom );

  SANS_CHECK_CLOSE( epsloc_geom_True(0) , epsloc_geom(0) , small_tol, close_tol ); // eps11
  SANS_CHECK_CLOSE( epsloc_geom_True(1) , epsloc_geom(1) , small_tol, close_tol ); // eps22
  SANS_CHECK_CLOSE( epsloc_geom_True(2) , epsloc_geom(2) , small_tol, close_tol ); // eps12

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux, Combo, TTypes )
{
  typedef typename Combo::T T;
  typedef typename Combo::Tc Tc;
  typedef typename Combo::Tm Tm;
  typedef typename Combo::Tq Tq;
  typedef typename Combo::Tr Tr;
  typedef typename promote_Surreal<T,Tc,Tm,Tq,Tr>::type Ts;

  // Test the advective flux integrand calculation
  typedef PDEHSM2D<VarTypeLambda> PDEClass;
  typedef typename PDEClass::template ArrayQ<T> ArrayQ;
  typedef typename PDEClass::template ArrayQ<Ts> ArrayQS;
  typedef typename PDEClass::template ParamsType<Tc, Tm, Tq> ParamsType;
  typedef typename PDEClass::template VectorX<Real> VectorX_R;
  typedef typename PDEClass::template VectorX<T > VectorX_T;
  typedef typename PDEClass::template VectorX<Tq> VectorX_Tq;
  typedef typename PDEClass::template VectorE<Tr> VectorE;
  typedef typename PDEClass::template MatrixX<T > MatrixX_T;
  typedef typename PDEClass::template ComplianceMatrix<Tc> ComplianceMatrix;

  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  VectorX_R g = { 0.023, -9.81 };
  PDEClass hsm(g, PDEClass::HSM_ResidInterp_Separate);

  // Static tests
  BOOST_CHECK( hsm.D == 2 );
  BOOST_CHECK( hsm.N == 6 );

  // Flux components
  BOOST_CHECK( hsm.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( hsm.hasFluxAdvective() == true );
  BOOST_CHECK( hsm.hasFluxViscous() == false );
  BOOST_CHECK( hsm.hasSource() == true );
  BOOST_CHECK( hsm.hasForcingFunction() == false );

  BOOST_CHECK( hsm.needsSolutionGradientforSource() == true );

  // Coordinates
  Real x0, y0, time;
  x0 = 0; y0 = 0; time = 0;

  // State vector
  Real rx, ry, lam3, f11, f22, f12;
  rx  =  3.263;
  ry  = -0.445;
  lam3  =  1./6.;
  f11 = 3.14;
  f22 = 42;
  f12 = 0.56;

  // Set variables in state vector
  ArrayQ varTrue = {rx, ry, lam3, f11, f22, f12};
  ArrayQ var     = {rx, ry, lam3, f11, f22, f12};

  // Parameters

  // Material properties
  Real E = 70e9;
  Real nu = 0.33;
  Real t = 0.01;
  Real mu = 0.273;
  ComplianceMatrix AijInv = calcComplianceMatrix( E, nu, t );

  // Forces/accelerations
  VectorE qe = { 1.3, -0.46 };
  VectorX_Tq qx = { 4.1,  8.24 };
  VectorX_Tq a  = { -2., 1./7. };

  // Insert into parameter fields
  ParamsType params( AijInv, mu, qe, qx, a );

  // MASTER STATE ---------------------------------------------------------
  ArrayQ uCons = 0;

  // Use HSM2D class function to calculate unsteady flux
  hsm.masterState( params, x0, y0, time, var, uCons );

  for (int n = 0; n < hsm.N; n++)
    SANS_CHECK_CLOSE( SurrealValue(varTrue[n]), SurrealValue(uCons[n]), small_tol, close_tol );

  // UNSTEADY FLUX ---------------------------------------------------------
  ArrayQ ftTrue = {rx, ry, lam3, f11, f22, f12};
  ArrayQ ft = 0;

  // Use HSM2D class function to calculate unsteady flux
  hsm.masterState( params, x0, y0, time, var, ft );

  for (int n = 0; n < hsm.N; n++)
    SANS_CHECK_CLOSE( SurrealValue(ftTrue[n]), SurrealValue(ft[n]), small_tol, close_tol );


  // ADVECTIVE FLUX --------------------------------------------------------

  // Deformed local basis vectors
  VectorX_T e1, e2;
  // Calculate ehat_1, ehat_2
  hsm.varInterpret_.calc_ehat( var, e1, e2 );
  MatrixX_T e = {{ e1[0], e2[0] },{ e1[1], e2[1] }};

  // Force resultant matrix
  DLA::MatrixSymS<2,T> fbar;
  // Calculate fbar
  hsm.calc_fbar( var, e, fbar );
  // Components of fbar
  T& fxx = fbar(0,0);
  T& fxy = fbar(1,0);
  T& fyy = fbar(1,1);

  // Vector-tensor dot in [13]
  // R^f1 flux terms
  T Rf1x = (e1[0]*fxx + e1[1]*fxy);  // Check that these match writeup
  T Rf1y = (e1[0]*fxy + e1[1]*fyy);
  // R^f2 flux terms
  T Rf2x = (e2[0]*fxx + e2[1]*fxy);
  T Rf2y = (e2[0]*fxy + e2[1]*fyy);

  ArrayQ fxTrue = {0, 0, 0, Rf1x, Rf2x, 0};
  ArrayQ fyTrue = {0, 0, 0, Rf1y, Rf2y, 0};

  ArrayQS fx = 0;
  ArrayQS fy = 0;

  // Use HSM2D class function to calculate advective flux
  // Proper order is ()1, ()2, ()1x, ()1y, ()2x, ()2y
  hsm.fluxAdvective( params, x0, y0, time, var, fx, fy );

  for (int n = 0; n < hsm.N; n++)
  {
    SANS_CHECK_CLOSE( SurrealValue(fxTrue(n)), SurrealValue(fx(n)), small_tol, close_tol );
    SANS_CHECK_CLOSE( SurrealValue(fyTrue(n)), SurrealValue(fy(n)), small_tol, close_tol );
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( source, Combo, TTypes )
{
  typedef typename Combo::T T;
  typedef typename Combo::Tc Tc;
  typedef typename Combo::Tm Tm;
  typedef typename Combo::Tq Tq;
  typedef typename Combo::Tr Tr;
  typedef typename promote_Surreal<T,Tc,Tm,Tq,Tr>::type Ts;

  // Test the source integrand calculation
  // Test the advective flux integrand calculation
  typedef PDEHSM2D<VarTypeLambda> PDEClass;
  typedef typename PDEClass::template ArrayQ<T> ArrayQ;
  typedef typename PDEClass::template ArrayQ<Ts> ArrayQS;
  typedef typename PDEClass::template ParamsType<Tc, Tm, Tq> ParamsType;
  typedef typename PDEClass::template VectorX<Real> VectorX_R;
  typedef typename PDEClass::template VectorX<T > VectorX_T;
  typedef typename PDEClass::template VectorX<Tr> VectorX_Tr;
  typedef typename PDEClass::template VectorX<Tq> VectorX_Tq;
  typedef typename PDEClass::template VectorE<Tr> VectorE;
  typedef typename PDEClass::template MatrixX<T > MatrixX_T;
  typedef typename PDEClass::template ComplianceMatrix<Tc> ComplianceMatrix;


  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;

  VectorX_R g = { 0.023, -9.81 };
  PDEClass hsm(g, PDEClass::HSM_ResidInterp_Separate);

  // Coordinates
  Real x0, y0, time;
  x0 = 0.0; y0 = 0.0; time = 0;

  // Coordinate local-coordinate-derivatives
  Real x0xi, x0eta, y0xi, y0eta;
  x0xi  = 3.21;
  x0eta = -0.12;
  y0xi  = 1.95;
  y0eta = 2.55;

  // State vector
  Real rx, ry, lam3, f11, f22, f12;
  rx   = 3.263;
  ry   = -0.445;
  lam3 =  1./6.;
  f11  = 3.14;
  f22  = 42;
  f12  = 0.56;
  ArrayQ var = {rx, ry, lam3, f11, f22, f12};

  // State vector x-coordinate derivatives
  Real rxx, ryx, lam3x, f11x, f22x, f12x;
  rxx   = -2.1;
  ryx   = 4.12;
  lam3x = 3./7.;
  f11x  = -0.32;
  f22x  = 5.3;
  f12x  = 6.2;
  ArrayQ varx = {rxx, ryx, lam3x, f11x, f22x, f12x};

  // State vector y-coordinate derivatives
  Real rxy, ryy, lam3y, f11y, f22y, f12y;
  rxy   = 8.14;
  ryy   = 4.21;
  lam3y = 2.2;
  f11y  = -4.7;
  f22y  = 0.75;
  f12y  = 2.11;
  ArrayQ vary = {rxy, ryy, lam3y, f11y, f22y, f12y};

  // State vector xi local-coordinate derivatives
  Real rxxi, ryxi, lam3xi, f11xi, f22xi, f12xi;
  rxxi   = 1.09;
  ryxi   = -3.2;
  lam3xi = -0.39;
  f11xi  = -6.4;
  f22xi  = 2.4;
  f12xi  = 12.8;
  ArrayQ varxi = {rxxi, ryxi, lam3xi, f11xi, f22xi, f12xi};

  // State vector eta local-coordinate derivatives
  Real rxeta, ryeta, lam3eta, f11eta, f22eta, f12eta;
  rxeta   = 0.453;
  ryeta   = 15.3;
  lam3eta = 1.75;
  f11eta  = 9.21;
  f22eta  = -4.99;
  f12eta  = 0.76;
  ArrayQ vareta = {rxeta, ryeta, lam3eta, f11eta, f22eta, f12eta};

  // Parameters

  // Material properties
  Real E = 70e9;
  Real nu = 0.33;
  Real t = 0.01;
  Real mu = 0.273;
  ComplianceMatrix AijInv = calcComplianceMatrix( E, nu, t );

  // Forces/accelerations
  VectorE qe = { 1.3, -0.46 };
  VectorX_Tq qx = { 4.1,  8.24 };
  VectorX_Tq a  = { -2., 1./7. };

  // Insert into parameter fields
  ParamsType params( AijInv, mu, qe, qx, a );


  // STRAIN-DISPLACEMENT CONSISTENCY -------------------------------------------------------

  // Deformed local basis vectors and derivs
  VectorX_T e1, e1x, e1y, e2, e2x, e2y;
  // Calculate ehat_1, ehat_2
  hsm.varInterpret_.calc_ehat_derivs(var, varx, vary,
                                     e1 , e2,
                                     e1x, e1y,
                                     e2x, e2y);
  // Local basis
  MatrixX_T e = {{ e1[0], e2[0] },
                 { e1[1], e2[1] }};

  // Postion derivatives w.r.t. geometry xi, eta
  // Deformed
  VectorX_T rxi   = { rxxi , ryxi };
  VectorX_T reta  = { rxeta, ryeta };
  // Undeformed
  VectorX_Tr r0xi   = { x0xi , y0xi };
  VectorX_Tr r0eta  = { x0eta, y0eta };

  // Two definitions of strain
  DLA::VectorS<3,Ts> epsloc_force;
  DLA::VectorS<3,T> epsloc_geom;
  // Force-dependent strains
  hsm.calc_epsloc_force( params, var, epsloc_force );
  // Geometry-dependent strains
  hsm.calc_epsloc_geom_cart( e, r0xi, r0eta, rxi, reta, epsloc_geom );

  // Break into components
  T& eps11r = epsloc_geom[0];
  T& eps22r = epsloc_geom[1];
  T& eps12r = epsloc_geom[2];
  Ts& eps11f = epsloc_force[0];
  Ts& eps22f = epsloc_force[1];
  Ts& eps12f = epsloc_force[2];

  // Inhabit source term corresponding to R^eps11
  Ts sourceTrue0 = eps11r - eps11f;
  // Inhabit source term corresponding to R^eps22
  Ts sourceTrue1 = eps22r - eps22f;
  // Inhabit source term corresponding to R^eps11
  Ts sourceTrue2 = eps12r - eps12f;

  ArrayQS sourceTrue = {sourceTrue0, sourceTrue1, sourceTrue2, 0, 0, 0};

  // IN-PLANE FORCE EQUILIBRIUM -----------------------------------------------------------

  // Force resultant matrix
  DLA::MatrixSymS<2,T> fbar;
  // Calculate fbar
  hsm.calc_fbar( var, e, fbar );
  // Components of fbar
  T& fxx = fbar(0,0);
  T& fxy = fbar(1,0);
  T& fyy = fbar(1,1);

  // Force-curvature (2nd) term in [13]
  // R^f1 source term
  T fbardele1 = fxx*e1x[0] + fxy*e1y[0]
              + fxy*e1x[1] + fyy*e1y[1];
  // R^f2 source term
  T fbardele2 = fxx*e2x[0] + fxy*e2y[0]
              + fxy*e2x[1] + fyy*e2y[1];

  // Calculate overall applied force in cartesian axes
  VectorX_T qecart = { e1[0]*qe[0]+e2[0]*qe[1], e1[1]*qe[0]+e2[1]*qe[1] };
  VectorX_T q = qx + qecart;

  // Inhabit source term corresponding to R^f1 (2nd + 3rd term)
  sourceTrue[3] = -fbardele1 + dot( q, e1 ) + dot( mu*(g-a), e1 );

  // Inhabit source term corresponding to R^f2 (2nd + 3rd term)
  sourceTrue[4] = -fbardele2 + dot( q, e2 ) + dot( mu*(g-a), e2 );

  // IN-PLANE ROTATION CONSTRAINT -------------------------------------------------------

  // Calculate material line vectors
  // Deformed (Eq.37-38)
  T s1tld = dot(e1,rxi);
  T s2tld = dot(e2,rxi);
  // Undeformed (Eq.39-40)
  Tr s01 = r0xi[0];
  Tr s02 = r0xi[1];

  // Use geometry-residual-defined strains in calculating s-tilde (fewer entries in matrix)
  VectorX_Tr s0 = { s01, s02 };
  DLA::MatrixSymS<2,T> epsbar  = {{ 1+eps11r },
                                  {  eps12r   , 1+eps22r }};
  VectorX_T s0tld = epsbar * s0;
  T& s01tld = s0tld[0];
  T& s02tld = s0tld[1];

  // Jacobian of element coordinates xi,eta, expressed as 1/J0^2
  Tr J0 = fabs( x0xi*y0eta - x0eta*y0xi );
  Tr J0invsq = pow( J0, -2 );

  // Inhabit source term corresponding to R^e3
  sourceTrue[5] += J0invsq * (s1tld*s02tld - s2tld*s01tld);

  // Use HSM2D class function to calculate source term
  ArrayQS source = 0;
  hsm.source( params, x0, y0, time,
              x0xi, x0eta, y0xi, y0eta,
              var, varx, vary, varxi, vareta, source );

  for (int n = 0; n < hsm.N; n++)
  {
    SANS_CHECK_CLOSE( SurrealValue(sourceTrue(n)), SurrealValue(source(n)), small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  // Check that dump output remains consistent
  typedef PDEHSM2D<VarTypeLambda> PDEClass;
  typedef PDEHSM2D<VarTypeLambda>::VectorX<Real> VectorX;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/HSM2D_pattern.txt", true );

  VectorX g = { 0.023, -9.81 };
  PDEClass hsm(g, PDEClass::HSM_ResidInterp_Separate);

  // Dump IO
  hsm.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
