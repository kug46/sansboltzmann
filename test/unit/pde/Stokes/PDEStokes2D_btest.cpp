// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEStokes2D_btest
//
// test of 2-D compressible Stokes PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "Topology/Dimension.h"
#include "pde/Stokes/TraitsStokes.h"
#include "pde/Stokes/PDEStokes2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"

namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
class QTypeEntropy {};

// Explicitly instantiate the class so coverage information is correct
template class TraitsSizeStokes<PhysD2>;
template class PDEStokes2D<TraitsSizeStokes>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEStokes2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDEStokes2D<TraitsSizeStokes> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  BOOST_REQUIRE( PDEClass::D == 2 );
  BOOST_REQUIRE( PDEClass::N == 3 );
  BOOST_REQUIRE( ArrayQ::M == 3 );
  BOOST_REQUIRE( MatrixQ::M == 3 );
  BOOST_REQUIRE( MatrixQ::N == 3 );

  const Real nu = 0.01;
  PDEClass pde(nu);

  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( setDOFFrom )
{
  typedef PDEStokes2D<TraitsSizeStokes> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;
  const Real nu = 0.01;
  PDEClass pde(nu);

  Real p = 1.137;
  Real u = 0.784;
  Real v = -0.231;

  // set
  Real qDataPrim[3] = {p, u, v};
  string qNamePrim[3] = {"Pressure", "VelocityX", "VelocityY"};
  ArrayQ qTrue = {p, u, v};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );
  BOOST_CHECK_CLOSE( qTrue(0), q(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), q(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), q(2), tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Advflux )
{
  typedef PDEStokes2D<TraitsSizeStokes> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-12;

  const Real nu = 0.01;
  PDEClass pde(nu);

  // function tests

  Real x, y, time;
  Real u, v, p;

  x = y = time = 0;   // not actually used in functions
  p = 1.137; u = 0.784; v = -0.231;

  // set
  ArrayQ q = {p, u, v};

  // conservative flux
  ArrayQ qTrue = {p, u, v};
  ArrayQ qCons = 0;
  pde.masterState( x, y, time, q, qCons );
  BOOST_CHECK_CLOSE( qTrue(0), qCons(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), qCons(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), qCons(2), tol );

  // Should not accumulate
  pde.masterState( x, y, time, q, qCons );
  BOOST_CHECK_CLOSE( qTrue(0), qCons(0), tol );
  BOOST_CHECK_CLOSE( qTrue(1), qCons(1), tol );
  BOOST_CHECK_CLOSE( qTrue(2), qCons(2), tol );

  // conservative flux
  ArrayQ ftTrue = {p, u, v};
  ArrayQ ft = 0;
  pde.fluxAdvectiveTime( x, y, time, q, ft );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );

  // Flux accumulate
  pde.fluxAdvectiveTime( x, y, time, q, ft );
  BOOST_CHECK_CLOSE( 2*ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( 2*ftTrue(2), ft(2), tol );

  // jacobianMasterState
  MatrixQ dudq = 0;
  MatrixQ dudqTrue = 0;

  dudqTrue(0,0) = 1;
  dudqTrue(0,1) = 0;
  dudqTrue(0,2) = 0;

  dudqTrue(1,0) = 0;
  dudqTrue(1,1) = 1;
  dudqTrue(1,2) = 0;

  dudqTrue(2,0) = 0;
  dudqTrue(2,1) = 0;
  dudqTrue(2,2) = 1;

  pde.jacobianMasterState(x, y, time, q, dudq);
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      BOOST_CHECK_CLOSE( dudq(i,j), dudqTrue(i,j), tol );

  // Should not accumulate
  pde.jacobianMasterState(x, y, time, q, dudq);
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      BOOST_CHECK_CLOSE( dudq(i,j), dudqTrue(i,j), tol );

  // advective flux
  ArrayQ fTrue = {u, p, 0};
  ArrayQ gTrue = {v, 0, p};
  ArrayQ f = 0, g = 0;

  pde.fluxAdvective( x, y, time, q, f, g );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( gTrue(1), g(1), tol );
  BOOST_CHECK_CLOSE( gTrue(2), g(2), tol );

  // Flux accumulate
  pde.fluxAdvective( x, y, time, q, f, g );
  BOOST_CHECK_CLOSE( 2*fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( 2*fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( 2*gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( 2*gTrue(1), g(1), tol );
  BOOST_CHECK_CLOSE( 2*gTrue(2), g(2), tol );

  // advective flux jacobian
  Real dfdudata[9] = {0,1,0,1,0,0,0,0,0};
  Real dgdudata[9] = {0,0,1,0,0,0,1,0,0};
  MatrixQ dfduTrue( dfdudata, 9 );
  MatrixQ dgduTrue( dgdudata, 9 );
  MatrixQ dfdu, dgdu;
  dfdu = 0;  dgdu = 0;
  pde.jacobianFluxAdvective( x, y, time, q, dfdu, dgdu );
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
    {
      BOOST_CHECK_CLOSE( dfduTrue(i,j), dfdu(i,j), tol );
      BOOST_CHECK_CLOSE( dgduTrue(i,j), dgdu(i,j), tol );
    }

  // Flux accumulate
  pde.jacobianFluxAdvective( x, y, time, q, dfdu, dgdu );
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
    {
      BOOST_CHECK_CLOSE( 2*dfduTrue(i,j), dfdu(i,j), tol );
      BOOST_CHECK_CLOSE( 2*dgduTrue(i,j), dgdu(i,j), tol );
    }

  Real px, py, ux, uy, vx, vy;
  px = -0.173; py = 0.236;
  ux = 0; vx = 0; uy = 0; vy=0;
  ArrayQ qx = {px, ux, vx};
  ArrayQ qy = {py, uy, vy};

  ArrayQ strongFTrue;
  strongFTrue(0) = ux + vy;
  strongFTrue(1) = px;
  strongFTrue(2) = py;

  ArrayQ strongF = 0;
  pde.strongFluxAdvective(x,y,time,q, qx, qy, strongF);
  BOOST_CHECK_CLOSE( strongFTrue(0), strongF(0), tol );
  BOOST_CHECK_CLOSE( strongFTrue(1), strongF(1), tol );
  BOOST_CHECK_CLOSE( strongFTrue(2), strongF(2), tol );
  // Flux accumulate
  pde.strongFluxAdvective(x,y,time,q, qx, qy, strongF);
  BOOST_CHECK_CLOSE( 2*strongFTrue(0), strongF(0), tol );
  BOOST_CHECK_CLOSE( 2*strongFTrue(1), strongF(1), tol );
  BOOST_CHECK_CLOSE( 2*strongFTrue(2), strongF(2), tol );

  Real nx = 1.0; Real ny = 0.0;
  Real speed = 0; Real speedTrue = 1.0;
  pde.speedCharacteristic(x, y, time, nx, ny, q, speed);
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  speed = 0;
  pde.speedCharacteristic(x, y, time, q, speed);
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );


  // Call these simply to make sure coverage is high
  ArrayQ qL, qR, qxL, qxR, qyL, qyR, source;
  Real dummy = 0;
  pde.source( dummy, dummy, dummy, q, qx, qy, source );
  //pde.forcingFunction( x, y, dummy, source );

  ArrayQ sourceL = 0 , sourceR = 0;
  pde.sourceTrace( dummy, dummy, dummy, dummy, dummy, qL, qxL, qyL, qR, qxR, qyR, sourceL, sourceR );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jacobianFluxAdvective )
{
  typedef PDEStokes2D<TraitsSizeStokes> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-12;

  const Real nu = 0.01;
  PDEClass pde(nu);

  // function tests

  Real x, y, time;
  Real u, v, p;

  x = y = time = 0;   // not actually used in functions
  p = 1.137; u = 0.784; v = -0.231;

  Real nx, ny;
  nx = 2.0/7.0; ny = sqrt(1.0-nx*nx);
  // set
  ArrayQ q = {p, u, v};

  MatrixQ mtx;
  mtx = 0;

  Real scale = 10.0;

  //Values taken from pde/NS/Roe.nb
  MatrixQ mtxTrue = {
    {1,0,0},
    {0, 1, 0},
    {0, 0, 1}};

  MatrixQ mtxTrue2 = scale*mtxTrue;

  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, time, q, scale*nx, scale*ny, mtx );
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      BOOST_CHECK_CLOSE( mtx(i,j), mtxTrue2(i,j), tol );

  // Flux accumulation
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, time, q, scale*nx, scale*ny, mtx );
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      BOOST_CHECK_CLOSE( mtx(i,j), 2*mtxTrue2(i,j), tol );

  const Real eps = 1.e-9;
  MatrixQ mtxTrue3 = eps*mtxTrue;

  mtx = 0;
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, time, q, eps*nx, eps*ny, mtx );
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      BOOST_CHECK_CLOSE( mtx(i,j), mtxTrue3(i,j), tol );

  nx = 0;  ny = 0;  // Note: magnitude == 0

  mtxTrue = 0;

  mtx = 0;
  const Real small_tol2 = 1.e-11;
  const Real close_tol = 1.e-11;
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, time, q, nx, ny, mtx );
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      SANS_CHECK_CLOSE( mtxTrue(i,j), mtx(i,j), small_tol2, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxRoe )
{
  typedef PDEStokes2D<TraitsSizeStokes> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-11;

  const Real nu = 0.01;
  PDEClass pde(nu);

  // function tests

  Real x, y, time;
  Real u, v, p, du, dv, dp;

  x = y = time = 0;   // not actually used in functions
  p = 1.137; u = 0.784; v = -0.231;
  dp = 0.003; du = 0.0078; dv = 0.0021;

  Real nx, ny;
  nx = 2.0/7.0; ny = sqrt(1.0-nx*nx);
  // set
  ArrayQ q = {p, u, v};
  ArrayQ qL = {p - 0.5*dp, u - 0.5*du, v - 0.5*dv};
  ArrayQ qR = {p + 0.5*dp, u + 0.5*du, v + 0.5*dv};
  ArrayQ dq = {dp, du, dv}; //R-L

  // advective normal flux (average)
  ArrayQ fAvg = {u, p, 0};
  ArrayQ gAvg = {v, 0, p};

  MatrixQ mtx = 0;
  pde.jacobianFluxAdvectiveAbsoluteValue(x, y, time, q, nx, ny, mtx );

  // Compute the average normal flux
  ArrayQ fnTrue = fAvg*nx + gAvg*ny - 0.5*mtx*dq;

  ArrayQ fn = 0;
  pde.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );

  // Flux accumulation
  pde.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( 2*fnTrue(2), fn(2), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DiffFlux )
{
  typedef PDEStokes2D<TraitsSizeStokes> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-12;

  const Real nu = 0.01;
  PDEClass pde(nu);

  // function tests

  Real x, y, time;
  Real u, v, p;

  x = y = time = 0;   // not actually used in functions
  p = 1.137; u = 0.784; v = -0.231;

  // set
  ArrayQ q = {p, u, v};

  Real px, py, ux, uy, vx, vy;
  px = -0.173; py = 0.236;
  ux = 0.101; vx = -0.035; uy = 0.184; vy=0.2273;
  ArrayQ qx = {px, ux, vx};
  ArrayQ qy = {py, uy, vy};

  ArrayQ fvTrue = {0, -nu*ux, -nu*vx};
  ArrayQ gvTrue= {0, -nu*uy, -nu*vy};

  ArrayQ fv = 0;
  ArrayQ gv = 0;
   pde.fluxViscous(x,y,time,q, qx, qy, fv, gv);
  BOOST_CHECK_CLOSE( fvTrue(0), fv(0), tol );
  BOOST_CHECK_CLOSE( fvTrue(1), fv(1), tol );
  BOOST_CHECK_CLOSE( fvTrue(2), fv(2), tol );
  BOOST_CHECK_CLOSE( gvTrue(0), gv(0), tol );
  BOOST_CHECK_CLOSE( gvTrue(1), gv(1), tol );
  BOOST_CHECK_CLOSE( gvTrue(2), gv(2), tol );

  //should accumulate
  pde.fluxViscous(x,y,time,q, qx, qy, fv, gv);
 BOOST_CHECK_CLOSE( 2.*fvTrue(0), fv(0), tol );
 BOOST_CHECK_CLOSE( 2.*fvTrue(1), fv(1), tol );
 BOOST_CHECK_CLOSE( 2.*fvTrue(2), fv(2), tol );
 BOOST_CHECK_CLOSE( 2.*gvTrue(0), gv(0), tol );
 BOOST_CHECK_CLOSE( 2.*gvTrue(1), gv(1), tol );
 BOOST_CHECK_CLOSE( 2.*gvTrue(2), gv(2), tol );

 MatrixQ kxx, kxy, kyx, kyy;
 kxx = 0; kxy = 0; kyx = 0; kyy = 0;
 MatrixQ kxxTrue = {{0,0,0},{0,nu,0},{0,0,nu}};
 MatrixQ kxyTrue = {{0,0,0},{0,0,0},{0,0,0}};
 MatrixQ kyxTrue = {{0,0,0},{0,0,0},{0,0,0}};
 MatrixQ kyyTrue = {{0,0,0},{0,nu,0},{0,0,nu}};

 pde.diffusionViscous(x,y,time,q, qx, qy, kxx, kxy, kyx, kyy);
 for (int i = 0; i < 3; i++)
   for (int j = 0; j < 3; j++)
   {
     BOOST_CHECK_CLOSE( kxxTrue(i,j), kxx(i,j), tol );
     BOOST_CHECK_CLOSE( kxyTrue(i,j), kxy(i,j), tol );
     BOOST_CHECK_CLOSE( kyxTrue(i,j), kyx(i,j), tol );
     BOOST_CHECK_CLOSE( kyyTrue(i,j), kyy(i,j), tol );
   }

 // Flux accumulate
 pde.diffusionViscous(x,y,time,q, qx, qy, kxx, kxy, kyx, kyy);
 for (int i = 0; i < 3; i++)
   for (int j = 0; j < 3; j++)
   {
     BOOST_CHECK_CLOSE( 2*kxxTrue(i,j), kxx(i,j), tol );
     BOOST_CHECK_CLOSE( 2*kxyTrue(i,j), kxy(i,j), tol );
     BOOST_CHECK_CLOSE( 2*kyxTrue(i,j), kyx(i,j), tol );
     BOOST_CHECK_CLOSE( 2*kyyTrue(i,j), kyy(i,j), tol );
   }

  Real pxx, pxy, pyy, uxx, uxy, uyy, vxx, vxy, vyy;
  pxx = 0.1234; pxy = 0.2345; pyy = 0.3456;
  uxx = 0.173; uxy = 0.731; uyy = 0.137;
  vxx = -0.236; vxy = -0.623, vyy = 3.427;

  ArrayQ qxx = {pxx, uxx, vxx};
  ArrayQ qxy = {pxy, uxy, vxy};
  ArrayQ qyy = {pyy, uyy, vyy};

  ArrayQ strongFTrue;
  strongFTrue(0) = 0;
  strongFTrue(1) = -nu*(uxx + uyy);
  strongFTrue(2) = -nu*(vxx + vyy);

  ArrayQ strongF = 0;
  pde.strongFluxViscous(x,y,time,q, qx, qy, qxx, qxy, qyy, strongF);
  BOOST_CHECK_CLOSE( strongFTrue(0), strongF(0), tol );
  BOOST_CHECK_CLOSE( strongFTrue(1), strongF(1), tol );
  BOOST_CHECK_CLOSE( strongFTrue(2), strongF(2), tol );
  // Flux accumulate
  pde.strongFluxViscous(x,y,time,q, qx, qy, qxx, qxy, qyy, strongF);
  BOOST_CHECK_CLOSE( 2*strongFTrue(0), strongF(0), tol );
  BOOST_CHECK_CLOSE( 2*strongFTrue(1), strongF(1), tol );
  BOOST_CHECK_CLOSE( 2*strongFTrue(2), strongF(2), tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef PDEStokes2D<TraitsSizeStokes> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real nu = 0.01;
  PDEClass pde(nu);

  // function tests

  Real u, v, p;

  p = 1.137; u = 0.784; v = -0.231;

  // set
  ArrayQ q1 = {p, u, v};
  BOOST_CHECK( pde.isValidState(q1) == true );

  p = -1;
  ArrayQ q2 = {p, u, v};
  BOOST_CHECK( pde.isValidState(q2) == true );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( interpResidBC )
{
  typedef PDEStokes2D<TraitsSizeStokes> PDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-12;

  const Real nu = 0.01;
  PDEClass pde(nu);

  // function tests

  Real u, v, p;

  p = 1.137; u = 0.784; v = -0.231;

  // set
  ArrayQ q = {p, u, v};

  BOOST_CHECK_EQUAL(pde.nMonitor(), 3);
  DLA::VectorD<Real> vecout1(3);
  pde.interpResidVariable(q, vecout1);
  for (int i=0; i<3; i++)
    SANS_CHECK_CLOSE( q(i), vecout1(i), tol, tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/Stokes/PDEStokes2D_pattern.txt", true );

  typedef PDEStokes2D<TraitsSizeStokes> PDEClass;

  const Real nu = 0.01;
  PDEClass pde(nu);

  pde.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
