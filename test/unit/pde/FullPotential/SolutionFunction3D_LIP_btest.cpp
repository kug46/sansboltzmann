// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// SolutionFunction3D_LIP_btest
//
// test of 3-D solution class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/FullPotential/SolutionFunction3D_LIP.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( SolutionFunction3D_LIP_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EllipticPotential_test )
{
  const Real small_tol = 1e-10;
  const Real close_tol = 1e-10;

  EllipticPotential phi;

  SANS_CHECK_CLOSE(  0.5, phi({0., 1e-12,  1e-12}), small_tol, close_tol );
  SANS_CHECK_CLOSE( -0.5, phi({0., 1e-12, -1e-12}), small_tol, close_tol );

  SANS_CHECK_CLOSE(  0.1513878188659974, phi({0., 1e-12,  1.5}), small_tol, close_tol );
  SANS_CHECK_CLOSE( -0.1513878188659974, phi({0., 1e-12, -1.5}), small_tol, close_tol );

  SANS_CHECK_CLOSE(  0.0636870798427297, phi({0.,  2., 1.5}), small_tol, close_tol );
  SANS_CHECK_CLOSE( -0.0636870798427297, phi({0., -2, -1.5}), small_tol, close_tol );

  Real q, qx, qy, qz, qxx, qxy, qxz, qyy, qyz, qzz;

  phi( 0., 1e-12, 1e-12, 0., q, qx, qy, qz, qxx, qxy, qxz, qyy, qyz, qzz);

  SANS_CHECK_CLOSE(  0.5, q, small_tol, close_tol );
  SANS_CHECK_CLOSE(  0.0, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE(  0.0, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE( -0.5, qz, small_tol, close_tol );

  phi( 0., 1e-12, -1e-12, 0., q, qx, qy, qz, qxx, qxy, qxz, qyy, qyz, qzz);

  SANS_CHECK_CLOSE( -0.5, q, small_tol, close_tol );
  SANS_CHECK_CLOSE(  0.0, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE(  0.0, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE( -0.5, qz, small_tol, close_tol );


  phi( 0., 1e-12, 1.5, 0., q, qx, qy, qz, qxx, qxy, qxz, qyy, qyz, qzz);

  SANS_CHECK_CLOSE(  0.1513878188659974, q, small_tol, close_tol );
  SANS_CHECK_CLOSE(                 0.0, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE(                 0.0, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE( -0.0839748528310781, qz, small_tol, close_tol );

  phi( 0., 1e-12, -1.5, 0., q, qx, qy, qz, qxx, qxy, qxz, qyy, qyz, qzz);

  SANS_CHECK_CLOSE( -0.1513878188659974, q, small_tol, close_tol );
  SANS_CHECK_CLOSE(                 0.0, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE(                 0.0, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE( -0.0839748528310781, qz, small_tol, close_tol );


  phi( 0., 2., 1.5, 0., q, qx, qy, qz, qxx, qxy, qxz, qyy, qyz, qzz);

  SANS_CHECK_CLOSE(  0.06368707984272970, q, small_tol, close_tol );
  SANS_CHECK_CLOSE(                  0.0, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( -0.04048142742447344, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.006721768917152371, qz, small_tol, close_tol );

  phi( 0.,-2., 1.5, 0., q, qx, qy, qz, qxx, qxy, qxz, qyy, qyz, qzz);

  SANS_CHECK_CLOSE(  0.06368707984272970, q, small_tol, close_tol );
  SANS_CHECK_CLOSE(                  0.0, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE(  0.04048142742447344, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.006721768917152371, qz, small_tol, close_tol );

  phi( 0., 2., -1.5, 0., q, qx, qy, qz, qxx, qxy, qxz, qyy, qyz, qzz);

  SANS_CHECK_CLOSE( -0.06368707984272970, q, small_tol, close_tol );
  SANS_CHECK_CLOSE(                  0.0, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE(  0.04048142742447344, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.006721768917152371, qz, small_tol, close_tol );

  phi( 0., -2., -1.5, 0., q, qx, qy, qz, qxx, qxy, qxz, qyy, qyz, qzz);

  SANS_CHECK_CLOSE( -0.06368707984272970, q, small_tol, close_tol );
  SANS_CHECK_CLOSE(                  0.0, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( -0.04048142742447344, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.006721768917152371, qz, small_tol, close_tol );


  DLA::VectorS<3,Real> X, gradQ;

  X[0] = 0;  X[1] = 2;  X[2] = 1.5;
  phi( X, gradQ );

  SANS_CHECK_CLOSE(                  0.0, gradQ[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( -0.04048142742447344, gradQ[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.006721768917152371, gradQ[2], small_tol, close_tol );

  X[0] = 0;  X[1] = -2;  X[2] = 1.5;
  phi( X, gradQ );

  SANS_CHECK_CLOSE(                  0.0, gradQ[0], small_tol, close_tol );
  SANS_CHECK_CLOSE(  0.04048142742447344, gradQ[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.006721768917152371, gradQ[2], small_tol, close_tol );

  X[0] = 0;  X[1] = 2;  X[2] = -1.5;
  phi( X, gradQ );

  SANS_CHECK_CLOSE(                  0.0, gradQ[0], small_tol, close_tol );
  SANS_CHECK_CLOSE(  0.04048142742447344, gradQ[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.006721768917152371, gradQ[2], small_tol, close_tol );

  X[0] = 0;  X[1] = -2;  X[2] = -1.5;
  phi( X, gradQ );

  SANS_CHECK_CLOSE(                  0.0, gradQ[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( -0.04048142742447344, gradQ[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.006721768917152371, gradQ[2], small_tol, close_tol );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
