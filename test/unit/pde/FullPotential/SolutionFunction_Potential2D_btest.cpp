// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// SolutionFunction_Potential2D_btest
//
// test of 2-D solution class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/FullPotential/SolutionFunction_Potential2D.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( SolutionFunction_Potential2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Const_test )
{
  typedef SolutionFunction_Potential2D_Const SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;

  const Real close_tol = 1e-13;
  const Real small_tol = 1e-13;

  Real a0 = 1;
  SolutionClass fcn( a0 );

  Real x = 1;
  Real y = 2;
  ArrayQ q, qx, qy, qxx, qxy, qyy;

  q = fcn(x, y, 0.);
  SANS_CHECK_CLOSE( 1, q, small_tol, close_tol );

  fcn(x, y, 0., q, qx, qy, qxx, qxy, qyy);
  SANS_CHECK_CLOSE( 1, q, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qxy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qyy, small_tol, close_tol );

  // PyDict constructor
  PyDict dict;
  dict[SolutionClass::ParamsType::params.constant] = a0;
  SolutionClass::ParamsType::checkInputs(dict);

  PyDict dictList;
  dictList["fcn"] = dict;
  //SolutionClass::ParamsType::checkInputs(dictList);   // causes error; why??

  SolutionClass fcn1( dict );

  q = fcn1(x, y, 0.);
  SANS_CHECK_CLOSE( 1, q, small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Linear_test )
{
  typedef SolutionFunction_Potential2D_Linear SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;

  const Real close_tol = 1e-13;
  const Real small_tol = 1e-13;

  Real a0 =  1;
  Real ax = -3;
  Real ay =  4;

  SolutionClass fcn( a0, ax, ay );

  Real x, y;
  ArrayQ q, qx, qy, qxx, qxy, qyy;

  x = 0;
  y = 0;
  q = fcn(x, y, 0.);
  SANS_CHECK_CLOSE( a0, q, small_tol, close_tol );

  fcn(x, y, 0., q, qx, qy, qxx, qxy, qyy);
  SANS_CHECK_CLOSE( a0, q, small_tol, close_tol );
  SANS_CHECK_CLOSE( ax, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( ay, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qxy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qyy, small_tol, close_tol );

  x = 1;
  y = 2;
  q = fcn(x, y, 0.);
  SANS_CHECK_CLOSE( a0 + ax*x + ay*y, q, small_tol, close_tol );

  fcn(x, y, 0., q, qx, qy, qxx, qxy, qyy);
  SANS_CHECK_CLOSE( a0 + ax*x + ay*y, q, small_tol, close_tol );
  SANS_CHECK_CLOSE( ax, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( ay, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qxy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qyy, small_tol, close_tol );

  // PyDict constructor
  PyDict dict;
  dict[SolutionClass::ParamsType::params.constant] = a0;
  dict[SolutionClass::ParamsType::params.gradx   ] = ax;
  dict[SolutionClass::ParamsType::params.grady   ] = ay;
  SolutionClass::ParamsType::checkInputs(dict);

  PyDict dictList;
  dictList["fcn"] = dict;
  //SolutionClass::ParamsType::checkInputs(dictList);   // causes error; why??

  SolutionClass fcn1( dict );

  q = fcn1(x, y, 0.);
  SANS_CHECK_CLOSE( a0 + ax*x + ay*y, q, small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SineSine_test )
{
  typedef SolutionFunction_Potential2D_SineSine SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;

  const Real close_tol = 1e-13;
  const Real small_tol = 1e-13;

  SolutionClass fcn;

  Real x, y;
  ArrayQ q, qx, qy, qxx, qxy, qyy;

  x = 0;
  y = 0;
  q = fcn(x, y, 0.);
  SANS_CHECK_CLOSE( 0, q, small_tol, close_tol );

  fcn(x, y, 0., q, qx, qy, qxx, qxy, qyy);
  SANS_CHECK_CLOSE( 0, q, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE( 4*PI*PI, qxy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qyy, small_tol, close_tol );

  x = 1;
  y = 1;
  q = fcn(x, y, 0.);
  SANS_CHECK_CLOSE( 0, q, small_tol, close_tol );

  fcn(x, y, 0., q, qx, qy, qxx, qxy, qyy);
  SANS_CHECK_CLOSE( 0, q, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE( 4*PI*PI, qxy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qyy, small_tol, close_tol );

  x = 0.5;
  y = 1;
  q = fcn(x, y, 0.);
  SANS_CHECK_CLOSE( 0, q, small_tol, close_tol );

  fcn(x, y, 0., q, qx, qy, qxx, qxy, qyy);
  SANS_CHECK_CLOSE( 0, q, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI, qxy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qyy, small_tol, close_tol );

  x = 0.25;
  y = 0.25;
  q = fcn(x, y, 0.);
  SANS_CHECK_CLOSE( 1, q, small_tol, close_tol );

  fcn(x, y, 0., q, qx, qy, qxx, qxy, qyy);
  SANS_CHECK_CLOSE( 1, q, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI, qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qxy, small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI, qyy, small_tol, close_tol );

  // PyDict constructor
  PyDict dict;
  SolutionClass::ParamsType::checkInputs(dict);

  PyDict dictList;
  dictList["fcn"] = dict;
  //SolutionClass::ParamsType::checkInputs(dictList);   // causes error; why??

  SolutionClass fcn1( dict );

  q = fcn1(x, y, 0.);
  SANS_CHECK_CLOSE( 1, q, small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CubicSourceBump_test )
{
  typedef SolutionFunction_Potential2D_CubicSourceBump SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;

  const Real close_tol = 1e-13;
  const Real small_tol = 1e-13;

  Real tau = 0.1;

  SolutionClass fcn( tau );

  Real x, y;
  Real p;
  ArrayQ q;

  x = 0;
  y = 0;
  p = -0.058268083616295029451674064301430;
  q = fcn( x, y, 0. );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );

  x = 1;
  y = 0;
  p = +0.058268083616295029451674064301430;
  q = fcn( x, y, 0. );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );

  x = 0.5;
  y = 0;
  p = 0;
  q = fcn( x, y, 0. );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );

  x = 0;
  y = 1;
  p = -0.0087660881433560155366196380166540;
  q = fcn( x, y, 0. );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );

  x = 1;
  y = 1;
  p = +0.0087660881433560155366196380166540;
  q = fcn( x, y, 0. );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );

  x = 0.5;
  y = 1;
  p = 0;
  q = fcn( x, y, 0. );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );

  // PyDict constructor
  PyDict dict;
  dict[SolutionClass::ParamsType::params.tau] = tau;
  SolutionClass::ParamsType::checkInputs(dict);

  PyDict dictList;
  dictList["fcn"] = dict;
  //SolutionClass::ParamsType::checkInputs(dictList);   // causes error; why??

  SolutionClass fcn1( dict );

  q = fcn1(x, y, 0.);
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Joukowski_test )
{
  typedef SolutionFunction_Potential2D_Joukowski SolutionClass;
  typedef SolutionClass::template ArrayQ<Real> ArrayQ;

  const Real close_tol = 1e-11;
  const Real small_tol = 1e-13;

  Real e = 0.1;
  Real aoa = 15;

  SolutionClass fcn( e, aoa );

  Real x, y;
  ArrayQ q, qx, qy, qxx, qxy, qyy, qt;
  Real p, px, py, pxx, pxy, pyy;

  x = 1; y = 1;
  p   = -0.58920444256397108029535110499700 + 58./121.;    // 58/121 from U*(xle - zeta0) shift
  px  =  0.093327264334358417293138366904870;
  py  = -0.071396738467994931538443069177670;
  pxx = -0.091257014304874294569935377726959;
  pxy = -0.029193934490294099283228208132641;
  pyy = -pxx;
  q = fcn( x, y, 0 );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );
  fcn( x, y, 0., q, qx, qy, qxx, qxy, qyy );
  SANS_CHECK_CLOSE( p,   q,   small_tol, close_tol );
  SANS_CHECK_CLOSE( px,  qx,  small_tol, close_tol );
  SANS_CHECK_CLOSE( py,  qy,  small_tol, close_tol );
  SANS_CHECK_CLOSE( pxx, qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE( pxy, qxy, small_tol, close_tol );
  SANS_CHECK_CLOSE( pyy, qyy, small_tol, close_tol );

  x = 1; y = -1;
  p   = -1.21318330086992919230004362554343 + 58./121.;
  px  = -0.085073334447847632869910960287526;
  py  = -0.056776417545814745059052593069822;
  pxx =  0.064594650556493521622581603833336;
  pxy = -0.028593633855465447618157869210762;
  pyy = -pxx;
  q = fcn( x, y, 0 );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );
  fcn( x, y, 0., q, qx, qy, qxx, qxy, qyy );
  SANS_CHECK_CLOSE( p,   q,   small_tol, close_tol );
  SANS_CHECK_CLOSE( px,  qx,  small_tol, close_tol );
  SANS_CHECK_CLOSE( py,  qy,  small_tol, close_tol );
  SANS_CHECK_CLOSE( pxx, qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE( pxy, qxy, small_tol, close_tol );
  SANS_CHECK_CLOSE( pyy, qyy, small_tol, close_tol );

  x = -1; y = 1;
  p   = -0.81469131994274930363399326947714 + 58./121.;
  px  =  0.056610709293132151766539703287421;
  py  =  0.072674144833788130592869118452175;
  pxx =  0.058579262881752827367582155454231;
  pxy =  0.013523874426064222422947179808326;
  pyy = -pxx;
  q = fcn( x, y, 0 );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );
  fcn( x, y, 0., q, qx, qy, qxx, qxy, qyy );
  SANS_CHECK_CLOSE( p,   q,   small_tol, close_tol );
  SANS_CHECK_CLOSE( px,  qx,  small_tol, close_tol );
  SANS_CHECK_CLOSE( py,  qy,  small_tol, close_tol );
  SANS_CHECK_CLOSE( pxx, qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE( pxy, qxy, small_tol, close_tol );
  SANS_CHECK_CLOSE( pyy, qyy, small_tol, close_tol );

  x = -1; y = -1;
  p   = -1.01016977280027913100774390089873 + 58./121.;
  px  = -0.058900512028353461599140154061055;
  py  =  0.064111616455402308691054157310479;
  pxx = -0.054304425783070587443922610599265;
  pxy =  0.0037763484505729272930279152454684;
  pyy = -pxx;
  q = fcn( x, y, 0 );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );
  fcn( x, y, 0., q, qx, qy, qxx, qxy, qyy );
  SANS_CHECK_CLOSE( p,   q,   small_tol, close_tol );
  SANS_CHECK_CLOSE( px,  qx,  small_tol, close_tol );
  SANS_CHECK_CLOSE( py,  qy,  small_tol, close_tol );
  SANS_CHECK_CLOSE( pxx, qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE( pxy, qxy, small_tol, close_tol );
  SANS_CHECK_CLOSE( pyy, qyy, small_tol, close_tol );

  x = 1; y = 0.01;
  p   = -0.44164404166038230041354127360481 + 58./121.;
  px  = -0.05501353882233457244903495070583;
  py  = -0.25793073603015143080283682066314;
  pxx = -0.18320842518134436103900688016874;
  pxy =  1.6605344036396646925663596812762;
  pyy = -pxx;
  q = fcn( x, y, 0 );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );
  fcn( x, y, 0., q, qx, qy, qxx, qxy, qyy );
  SANS_CHECK_CLOSE( p,   q,   small_tol, close_tol );
  SANS_CHECK_CLOSE( px,  qx,  small_tol, close_tol );
  SANS_CHECK_CLOSE( py,  qy,  small_tol, close_tol );
  SANS_CHECK_CLOSE( pxx, qxx, small_tol, 3*close_tol );
  SANS_CHECK_CLOSE( pxy, qxy, small_tol,   close_tol );
  SANS_CHECK_CLOSE( pyy, qyy, small_tol, 3*close_tol );

  x = 1; y = -0.01;
  p   = -1.32369367949818754021875970847945 + 58./121.;
  px  = -0.08672181634518794601815746297379;
  py  = -0.22909180194660185819141688829477;
  pxx =  1.3717214687583475896868990615580;
  pxy = -0.020503068760049491202174281408488;
  pyy = -pxx;
  q = fcn( x, y, 0 );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );
  fcn( x, y, 0., q, qx, qy, qxx, qxy, qyy );
  SANS_CHECK_CLOSE( p,   q,   small_tol, close_tol );
  SANS_CHECK_CLOSE( px,  qx,  small_tol, close_tol );
  SANS_CHECK_CLOSE( py,  qy,  small_tol, close_tol );
  SANS_CHECK_CLOSE( pxx, qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE( pxy, qxy, small_tol, 1500*close_tol );
  SANS_CHECK_CLOSE( pyy, qyy, small_tol, close_tol );

  qx = qy = 0;
  fcn.gradient( x, y, 0., q, qx, qy, qt );
  SANS_CHECK_CLOSE( px, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( py, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE(  0, qt, small_tol, close_tol );

  fcn.secondGradient( x, y, 0., q, qx, qy, qt, qxx, qxy, qyy );
  SANS_CHECK_CLOSE( pxx, qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE( pxy, qxy, small_tol, 1500*close_tol );
  SANS_CHECK_CLOSE( pyy, qyy, small_tol, close_tol );

  // PyDict constructor
  PyDict dict;
  dict[SolutionClass::ParamsType::params.e  ] = e;
  dict[SolutionClass::ParamsType::params.aoa] = aoa;
  SolutionClass::ParamsType::checkInputs(dict);

  PyDict dictList;
  dictList["fcn"] = dict;
  //SolutionClass::ParamsType::checkInputs(dictList);   // causes error; why??

  SolutionClass fcn1( dict );

  q = fcn1(x, y, 0.);
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
