// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCLinearizedIncompressiblePotential2D_sansLG_btest
//
// test of 2-D Linearized Incompressible Potential BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/FullPotential/BCLinearizedIncompressiblePotential2D_sansLG.h"
#include "pde/BCParameters.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct

template class SANS::BCLinearizedIncompressiblePotential2D<BCTypeDirichlet, Real>;
template class SANS::BCLinearizedIncompressiblePotential2D<BCTypeNeumann, Real>;
template class SANS::BCLinearizedIncompressiblePotential2D<BCTypeWall, Real>;
template class SANS::BCLinearizedIncompressiblePotential2D<BCTypeFunction, Real>;


//############################################################################//
BOOST_AUTO_TEST_SUITE( BCLinearizedIncompressiblePotential2D_sansLG_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeDirichlet_test )
{
  typedef BCTypeDirichlet BCType;
  typedef BCLinearizedIncompressiblePotential2D<BCType, Real> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;
  typedef BCClass::PhysDim PhysDim;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 1 );
  BOOST_CHECK( BCClass::NBC == 1 );

  BOOST_CHECK( PhysDim::D == 2 );

  const Real tol = 1.e-13;

  const Real bcdataTrue = 3;

  BCClass bc( bcdataTrue );

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  MatrixQ A;
  MatrixQ B;
  ArrayQ bcdata;

  Real x = 0;
  Real y = 0;
  Real nx = 0.8;
  Real ny = 0.6;

  A = 0;
  B = 0;
  bc.coefficients( x, y, 0, nx, ny, A, B );
  BOOST_CHECK_CLOSE( 1, A, tol );
  BOOST_CHECK_CLOSE( 0, B, tol );

  bcdata = 0;
  bc.data( x, y, 0, nx, ny, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata, tol );

  // Pydict constructors

  typedef BCParameters< BCLinearizedIncompressiblePotential2DVector<Real> > BCVectorParams;

  PyDict dictBC1;
  dictBC1[BCClass::ParamsType::params.q] = bcdataTrue;
  BCClass::ParamsType::checkInputs(dictBC1);

  const Real u = 1, v = 0.1;
  PDELinearizedIncompressiblePotential2D pde( u, v );

  BCClass bc1( pde, dictBC1 );

  A = 0;
  B = 0;
  bc1.coefficients( x, y, 0, nx, ny, A, B );
  BOOST_CHECK_CLOSE( 1, A, tol );
  BOOST_CHECK_CLOSE( 0, B, tol );

  bcdata = 0;
  bc1.data( x, y, 0, nx, ny, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata, tol );

  PyDict dictBC2;
  dictBC2[BCVectorParams::params.BC.BCType] = BCVectorParams::params.BC.Dirichlet;
  dictBC2[BCClass::ParamsType::params.q] = bcdataTrue;
  //BCVectorParams::checkInputs(dictBC2);   // causes error; why??
  //BCClass::ParamsType::checkInputs(dictBC2);   // causes error; why??

  PyDict dictBCList;
  dictBCList["BC1"] = dictBC2;
  BCVectorParams::checkInputs(dictBCList);

  BCClass bc2( pde, dictBC2 );

  bcdata = 0;
  bc2.data( x, y, 0, nx, ny, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata, tol );

#if 0   // why does this cause a checkInputs error?
  BCClass bc3( pde, dictBCList );

  bcdata = 0;
  bc3.data( x, y, 0, nx, ny, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata, tol );
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeNeumann_test )
{
  typedef BCTypeNeumann BCType;
  typedef BCLinearizedIncompressiblePotential2D<BCType, Real> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;
  typedef BCClass::PhysDim PhysDim;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 1 );
  BOOST_CHECK( BCClass::NBC == 1 );

  BOOST_CHECK( PhysDim::D == 2 );

  const Real tol = 1.e-13;

  const Real bcdataTrue = 3;

  BCClass bc( bcdataTrue );

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  MatrixQ A;
  MatrixQ B;
  ArrayQ bcdata;

  Real x = 0;
  Real y = 0;
  Real nx = 0.8;
  Real ny = 0.6;

  A = 0;
  B = 0;
  bc.coefficients( x, y, 0, nx, ny, A, B );
  BOOST_CHECK_CLOSE( 0, A, tol );
  BOOST_CHECK_CLOSE( 1, B, tol );

  bcdata = 0;
  bc.data( x, y, 0, nx, ny, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata, tol );

  // Pydict constructors

  typedef BCParameters< BCLinearizedIncompressiblePotential2DVector<Real> > BCVectorParams;

  PyDict dictBC;
  dictBC[BCVectorParams::params.BC.BCType] = BCVectorParams::params.BC.Neumann;
  dictBC[BCClass::ParamsType::params.gradqn] = bcdataTrue;
  //BCVectorParams::checkInputs(dictBC);      // why can't we do checkInputs here?
  //BCClass::ParamsType::checkInputs(dictBC);            // why can't we do checkInputs here?

  PyDict dictBCList;
  dictBCList["BC1"] = dictBC;
  BCVectorParams::checkInputs(dictBCList);

  const Real u = 1, v = 0.1;
  PDELinearizedIncompressiblePotential2D pde( u, v );

  BCClass bc1( pde, dictBC );

  A = 0;
  B = 0;
  bc1.coefficients( x, y, 0, nx, ny, A, B );
  BOOST_CHECK_CLOSE( 0, A, tol );
  BOOST_CHECK_CLOSE( 1, B, tol );

  bcdata = 0;
  bc1.data( x, y, 0, nx, ny, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata, tol );

#if 0   // why does this cause a checkInputs error?
  BCClass bc2( pde, dictBCList );

  bcdata = 0;
  bc2.data( x, y, 0, nx, ny, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata, tol );
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeWall_test )
{
  typedef BCTypeWall BCType;
  typedef BCLinearizedIncompressiblePotential2D<BCType, Real> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;
  typedef BCClass::PhysDim PhysDim;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 1 );
  BOOST_CHECK( BCClass::NBC == 1 );

  BOOST_CHECK( PhysDim::D == 2 );

  const Real tol = 1.e-13;

  const Real u =  0.95;
  const Real v = -0.21;

  BCClass bc( u, v );

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  MatrixQ A;
  MatrixQ B;
  ArrayQ bcdata;

  Real x = 0;
  Real y = 0;
  Real nx = 0.8;
  Real ny = 0.6;
  Real bcdataTrue = -(nx*u + ny*v);

  A = 0;
  B = 0;
  bc.coefficients( x, y, 0, nx, ny, A, B );
  BOOST_CHECK_CLOSE( 0, A, tol );
  BOOST_CHECK_CLOSE( 1, B, tol );

  bcdata = 0;
  bc.data( x, y, 0, nx, ny, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata, tol );

  // Pydict constructors

  typedef BCParameters< BCLinearizedIncompressiblePotential2DVector<Real> > BCVectorParams;

  PyDict dictBC;
  dictBC[BCVectorParams::params.BC.BCType] = BCVectorParams::params.BC.Wall;

  PyDict dictBCList;
  dictBCList["BC1"] = dictBC;
  BCVectorParams::checkInputs(dictBCList);

  PDELinearizedIncompressiblePotential2D pde( u, v );

  BCClass bc1( pde, dictBC );

  A = 0;
  B = 0;
  bc.coefficients( x, y, 0, nx, ny, A, B );
  BOOST_CHECK_CLOSE( 0, A, tol );
  BOOST_CHECK_CLOSE( 1, B, tol );

  bcdata = 0;
  bc.data( x, y, 0, nx, ny, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata, tol );

#if 0   // why does this cause a checkInputs error?
  BCClass bc2( pde, dictBCList );

  bcdata = 0;
  bc2.data( x, y, 0, nx, ny, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata, tol );
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFarfieldVortex_test )
{
  typedef BCTypeFarfieldVortex BCType;
  typedef BCLinearizedIncompressiblePotential2D<BCType, Real> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;
  typedef BCClass::PhysDim PhysDim;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 1 );
  BOOST_CHECK( BCClass::NBC == 1 );

  BOOST_CHECK( PhysDim::D == 2 );

  const Real tol = 1.e-13;

  Real x0 = -2;
  Real y0 = -3;
  Real circ = 0.1;

  BCClass bc( x0, y0, circ );

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  MatrixQ A;
  MatrixQ B;
  ArrayQ bcdata;

  Real x = 0;
  Real y = 0;
  Real nx = 0.8;
  Real ny = 0.6;

  A = 0;
  B = 0;
  bc.coefficients( x, y, 0, nx, ny, A, B );
  BOOST_CHECK_CLOSE( 1, A, tol );
  BOOST_CHECK_CLOSE( 0, B, tol );

  Real bcdataTrue = -circ * atan(1.5)/(2*PI);
  bcdata = 0;
  bc.data( x, y, 0, nx, ny, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata, tol );

  // Pydict constructors

  typedef BCParameters< BCLinearizedIncompressiblePotential2DVector<Real> > BCVectorParams;

  PyDict dictBC;
  dictBC[BCVectorParams::params.BC.BCType] = BCVectorParams::params.BC.FarfieldVortex;
  dictBC[BCClass::ParamsType::params.x0] = x0;
  dictBC[BCClass::ParamsType::params.y0] = y0;
  dictBC[BCClass::ParamsType::params.circ] = circ;

  PyDict dictBCList;
  dictBCList["BC1"] = dictBC;
  BCVectorParams::checkInputs(dictBCList);

  const Real u = 1, v = 0.1;
  PDELinearizedIncompressiblePotential2D pde( u, v );

  BCClass bc1( pde, dictBC );

  A = 0;
  B = 0;
  bc.coefficients( x, y, 0, nx, ny, A, B );
  BOOST_CHECK_CLOSE( 1, A, tol );
  BOOST_CHECK_CLOSE( 0, B, tol );

  bcdata = 0;
  bc.data( x, y, 0, nx, ny, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata, tol );

#if 0   // why does this cause a checkInputs error?
  BCClass bc2( pde, dictBCList );

  bcdata = 0;
  bc2.data( x, y, 0, nx, ny, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata, tol );
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFunction_test )
{
  typedef BCTypeFunction BCType;
  typedef BCLinearizedIncompressiblePotential2D<BCType, Real> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;
  typedef BCClass::PhysDim PhysDim;
  typedef SolutionFunction_Potential2D_CubicSourceBump SolutionClass;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 1 );
  BOOST_CHECK( BCClass::NBC == 1 );

  BOOST_CHECK( PhysDim::D == 2 );

  const Real tol = 1.e-13;

  Real tau = 0.1;
  BCClass::Function_ptr slnExact( new SolutionClass(tau) );
  BCClass bc( slnExact );

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  MatrixQ A;
  MatrixQ B;
  ArrayQ bcdata;
  Real bcdataTrue;

  Real x = 0;
  Real y = 0;
  Real nx = 0.8;
  Real ny = 0.6;

  A = 0;
  B = 0;
  bc.coefficients( x, y, 0, nx, ny, A, B );
  BOOST_CHECK_CLOSE( 1, A, tol );
  BOOST_CHECK_CLOSE( 0, B, tol );

  bcdataTrue = -0.058268083616295029451674064301430;
  bcdata = 0;
  bc.data( x, y, 0, nx, ny, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata, tol );

  // Pydict constructors

  typedef BCParameters< BCLinearizedIncompressiblePotential2DVector<Real> > BCVectorParams;

  PyDict dictFunction;
  dictFunction[BCClass::ParamsType::params.Function.SolutionFunctionName] = BCClass::ParamsType::params.Function.CubicSourceBump;
  dictFunction[SolutionClass::ParamsType::params.tau] = tau;

  PyDict dictBC;
  dictBC[BCVectorParams::params.BC.BCType] = BCVectorParams::params.BC.Function;
  dictBC[BCClass::ParamsType::params.Function] = dictFunction;

  PyDict dictBCList;
  dictBCList["BC1"] = dictBC;
  BCVectorParams::checkInputs(dictBCList);

  const Real u = 1, v = 0.1;
  PDELinearizedIncompressiblePotential2D pde( u, v );

  BCClass bc1( pde, dictBC );

  A = 0;
  B = 0;
  bc1.coefficients( x, y, 0, nx, ny, A, B );
  BOOST_CHECK_CLOSE( 1, A, tol );
  BOOST_CHECK_CLOSE( 0, B, tol );

  bcdata = 0;
  bc1.data( x, y, 0, nx, ny, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata, tol );

#if 0   // why does this cause a checkInputs error?
  BCClass bc2( pde, dictBCList );

  bcdata = 0;
  bc2.data( x, y, 0, nx, ny, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata, tol );
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/BCLinearizedIncompressiblePotential2D_sansLG_pattern.txt", true );

  {
  typedef BCTypeDirichlet BCType;
  typedef BCLinearizedIncompressiblePotential2D<BCType, Real> BCClass;

  Real bcdata = 3;

  BCClass bc( bcdata );
  bc.dump( 2, output );
  }

  {
  typedef BCTypeNeumann BCType;
  typedef BCLinearizedIncompressiblePotential2D<BCType, Real> BCClass;

  Real bcdata = 3;

  BCClass bc( bcdata );
  bc.dump( 2, output );
  }

  {
  typedef BCTypeWall BCType;
  typedef BCLinearizedIncompressiblePotential2D<BCType, Real> BCClass;

  Real u =  0.95;
  Real v = -0.21;

  BCClass bc( u, v );
  bc.dump( 2, output );
  }

  {
  typedef BCTypeFarfieldVortex BCType;
  typedef BCLinearizedIncompressiblePotential2D<BCType, Real> BCClass;

  Real x0 = -2;
  Real y0 = -3;
  Real circ = 0.1;

  BCClass bc( x0, y0, circ );
  bc.dump( 2, output );
  }

  {
  typedef BCTypeFunction BCType;
  typedef BCLinearizedIncompressiblePotential2D<BCType, Real> BCClass;
  typedef SolutionFunction_Potential2D_Const SolutionClass;

  BCClass::Function_ptr slnExact( new SolutionClass(1) );
  BCClass bc( slnExact );
  bc.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
