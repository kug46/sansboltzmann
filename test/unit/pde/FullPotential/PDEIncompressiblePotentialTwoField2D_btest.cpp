// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEIncompressiblePotentialTwoField2D_btest
//
// test of 2-D Linearized Incompressible Potential PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/FullPotential/PDEIncompressiblePotentialTwoField2D.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEIncompressiblePotentialTwoField2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDEIncompressiblePotentialTwoField2D PDEClass;
  typedef PDEClass::PhysDim PhysDim;

  BOOST_CHECK( PDEClass::D == 2 );
  BOOST_CHECK( PDEClass::N == 2 );

  BOOST_CHECK( PhysDim::D == 2 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef PDEIncompressiblePotentialTwoField2D PDEClass;

  const Real a = 1.1;
  const Real b = 0.2;

  const Real maskA = 0;     // airfoil weighted-norm
  const Real x0A = 1;
  const Real y0A = 0;
  const Real coefA = 30;

  const Real maskD = 0;     // domain weighted-norm
  const Real x0D = 1;
  const Real y0D = 0;
  const Real coefD = 30;

#undef USE_IPTF_NORM
#ifdef USE_IPTF_NORM
  const int normA = 2;
  const int normD = 2;

  PDEClass pde( a, b, x0A, y0A, coefA, maskA, normA, x0D, y0D, coefD, maskD, normD );
#else
  PDEClass pde( a, b, x0A, y0A, coefA, maskA, x0D, y0D, coefD, maskD );
#endif

  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 2 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == false );
  BOOST_CHECK( pde.hasFluxAdvective() == false );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );
}


#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxArea )
{
  typedef PDEIncompressiblePotentialTwoField2D PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real a = 1.1;
  Real b = 0.2;

  PDEClass pde( a, b );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == false );
  BOOST_CHECK( pde.hasFluxAdvective() == false );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // function tests

  Real x, y;
  Real phi, phix, phiy;

  x = 0; y = 0;
  phi  =  3.263;
  phix = -0.445;
  phiy =  1.741;

  // set
  Real qDataPrim[1] = {phi};
  string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = qDataPrim[0];
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  // set gradient
  qDataPrim[0] = phix;
  ArrayQ qxTrue = qDataPrim[0];
  ArrayQ qx;
  pde.setDOFFrom( qx, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxTrue, qx, tol );

  qDataPrim[0] = phiy;
  ArrayQ qyTrue = qDataPrim[0];
  ArrayQ qy;
  pde.setDOFFrom( qy, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyTrue, qy, tol );

  // diffusive flux
  ArrayQ fTrue, gTrue;
  ArrayQ f, g;
  fTrue = -phix;
  gTrue = -phiy;
  f = 0;
  g = 0;
  pde.fluxViscous( x, y, 0, q, qx, qy, f, g );
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( gTrue, g, tol );

  // diffusive flux jacobian
  MatrixQ kxx = 0, kxy = 0, kyx = 0, kyy = 0;
  pde.diffusionViscous( x, y, 0, q, qx, qy, kxx, kxy, kyx, kyy );
  BOOST_CHECK_CLOSE( 1, kxx, tol );
  BOOST_CHECK_CLOSE( 0, kxy, tol );
  BOOST_CHECK_CLOSE( 0, kyx, tol );
  BOOST_CHECK_CLOSE( 1, kyy, tol );

#if 0
  // forcing function

  source = 0;
  sourceTrue = -2*kxy*(4*PI*PI);
  pde.forcingFunction( x, y, source );
  BOOST_CHECK_CLOSE( sourceTrue, source, tol );
#endif

  // for coverage; functions are empty
  ArrayQ uCons = 0;
  MatrixQ dfdu = 0;
  MatrixQ dgdu = 0;
  ArrayQ source = 0;
  pde.fluxConservative( q, uCons );
  pde.fluxAdvective( x, y, 0, q, f, g );
  pde.jacobianFluxAdvective( x, y, 0, q, dfdu, dgdu );
  pde.sourceTrace( x, y, x, y, 0, q, qx, qy, q, qx, qy, source, source );
//  pde.forcingFunction( x, y, source );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxEdge )
{
  typedef PDEIncompressiblePotentialTwoField2D PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  Real a = 1.1;
  Real b = 0.2;

  PDEClass pde( a, b );

  // viscous flux function

  Real x, y;
  Real nx, ny;
  Real phiL, phiR;
  Real phixL, phiyL;
  Real phixR, phiyR;

  x = 0; y = 0;   // not actually used in functions
  nx = 1.22; ny = -0.432;
  phiL = 3.26; phiR = 1.79;
  phixL = 1.325;  phiyL = -0.457;
  phixR = 0.327;  phiyR =  3.421;

  // set
  Real qLDataPrim[1] = {phiL};
  string qLNamePrim[1] = {"Solution"};
  ArrayQ qLTrue = qLDataPrim[0];
  ArrayQ qL;
  pde.setDOFFrom( qL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qLTrue, qL, tol );

  Real qRDataPrim[1] = {phiR};
  string qRNamePrim[1] = {"Solution"};
  ArrayQ qRTrue = qRDataPrim[0];
  ArrayQ qR;
  pde.setDOFFrom( qR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qRTrue, qR, tol );

  // set gradient
  qLDataPrim[0] = phixL;
  ArrayQ qxLTrue = qLDataPrim[0];
  ArrayQ qxL;
  pde.setDOFFrom( qxL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxLTrue, qxL, tol );

  qLDataPrim[0] = phiyL;
  ArrayQ qyLTrue = qLDataPrim[0];
  ArrayQ qyL;
  pde.setDOFFrom( qyL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyLTrue, qyL, tol );

  qRDataPrim[0] = phixR;
  ArrayQ qxRTrue = qRDataPrim[0];
  ArrayQ qxR;
  pde.setDOFFrom( qxR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxRTrue, qxR, tol );

  qRDataPrim[0] = phiyR;
  ArrayQ qyRTrue = qRDataPrim[0];
  ArrayQ qyR;
  pde.setDOFFrom( qyR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyRTrue, qyR, tol );

  // viscous normal flux
  ArrayQ fnTrue = -0.5*(nx*(phixL + phixR) + ny*(phiyL + phiyR));
  ArrayQ fn = 0;
  pde.fluxViscous( x, y, 0, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );
  BOOST_CHECK_CLOSE( fnTrue, fn, tol );

  // for coverage; functions are empty
  pde.fluxAdvective( x, y, 0, qL, qR, nx, ny, fn );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic )
{
  typedef PDEIncompressiblePotentialTwoField2D PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  Real a = 1.1;
  Real b = 0.2;

  PDEClass pde( a, b );

  Real speed;
  Real x = 0;
  Real y = 0;
  Real dx = 0;
  Real dy = 0;
  ArrayQ q = 0;

  pde.speedCharacteristic( x, y, 0, dx, dy, q, speed );    // no check; empty function
  pde.speedCharacteristic( x, y, 0, q, speed );    // no check; empty function
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef PDEIncompressiblePotentialTwoField2D PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  Real a = 1.1;
  Real b = 0.2;

  PDEClass pde( a, b );

  ArrayQ q = 0;
  BOOST_CHECK( pde.isValidState(q) == true );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( interpResid_test )
{
  typedef PDEIncompressiblePotentialTwoField2D PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::ArrayD<Real> ArrayD;

  const Real tol = 1.e-13;

  Real a = 1.1;
  Real b = 0.2;

  PDEClass pde( a, b );

  BOOST_CHECK_EQUAL(pde.nMonitor(), 1);

  ArrayQ rsdIn = 2;
  ArrayD rsdOut(1);

  rsdOut = 0;
  pde.interpResidVariable( rsdIn, rsdOut );
  SANS_CHECK_CLOSE( rsdIn, rsdOut[0], tol, tol );

  rsdOut = 0;
  pde.interpResidGradient( rsdIn, rsdOut );
  SANS_CHECK_CLOSE( rsdIn, rsdOut[0], tol, tol );

  rsdOut = 0;
  pde.interpResidBC( rsdIn, rsdOut );
  SANS_CHECK_CLOSE( rsdIn, rsdOut[0], tol, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PDEIncompressiblePotentialTwoField2D_pattern.txt", true );

  typedef PDEIncompressiblePotentialTwoField2D PDEClass;

  Real a = 1.1;
  Real b = 0.2;

  PDEClass pde1( a, b );
  pde1.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
