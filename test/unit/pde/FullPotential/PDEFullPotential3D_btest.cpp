// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEFullPotential3D_btest
//
// test of 3-D Full Potential PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/FullPotential/PDEFullPotential3D.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEFullPotential3D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDEFullPotential3D<Real> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( PDEClass::N == 2 );
  BOOST_CHECK( (std::is_same<ArrayQ,DLA::VectorS<2,Real>>::value) );
  BOOST_CHECK( (std::is_same<MatrixQ,DLA::MatrixS<2,2,Real>>::value) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef PDEFullPotential3D<Real> PDEClass;

  Real a = 1.1;
  Real b = 0.2;
  Real c = 0.5;

  Real gamma = 1.4;
  Real Minf = 0.5;
  Real rhoinf = 1.2;
  Real pinf = 1.8;
  Real critVelFrac = 0.9;

  PDEClass pde0( a, b, c, gamma, Minf, rhoinf, pinf, critVelFrac );

  BOOST_CHECK( pde0.N == 2 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxVolume )
{
  typedef PDEFullPotential3D<Real> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real a = 1;
  Real b = 0.2;
  Real c = 0.5;

  Real gamma = 1.4;
  Real Minf = 0.5;
  Real rhoinf = 1.2;
  Real pinf = 1.8;
  Real critVelFrac = 0.9;

  PDEClass pde( a, b, c, gamma, Minf, rhoinf, pinf, critVelFrac );

  // static tests
  BOOST_CHECK( pde.N == 2 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == false );
  BOOST_CHECK( pde.hasFluxAdvective() == false );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // function tests

  Real x, y, z, h;
  Real phi, phix, phiy, phiz;
  Real rho, rhox, rhoy, rhoz;

  x = 0; y = 0; z = 0;
  phi  =  3.263;
  phix = -0.445;
  phiy =  1.741;
  phiz =  2.321;

  rho  = 1.346;
  rhox = 0.765;
  rhoy = 1.234;
  rhoz = 4.361;

  h = 0.467;

  // set
  Real qDataPrim[2] = {phi, rho};
  string qNamePrim[2] = {"Solution", "Density"};
  ArrayQ qTrue(qDataPrim,2);
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 2 );
  BOOST_CHECK_CLOSE( qTrue[0], q[0], tol );
  BOOST_CHECK_CLOSE( qTrue[1], q[1], tol );

  // set gradient
  qDataPrim[0] = phix;
  qDataPrim[1] = rhox;
  ArrayQ qxTrue(qDataPrim,2);
  ArrayQ qx;
  pde.setDOFFrom( qx, qDataPrim, qNamePrim, 2 );
  BOOST_CHECK_CLOSE( qxTrue[0], qx[0], tol );
  BOOST_CHECK_CLOSE( qxTrue[1], qx[1], tol );

  qDataPrim[0] = phiy;
  qDataPrim[1] = rhoy;
  ArrayQ qyTrue(qDataPrim,2);
  ArrayQ qy;
  pde.setDOFFrom( qy, qDataPrim, qNamePrim, 2 );
  BOOST_CHECK_CLOSE( qyTrue[0], qy[0], tol );
  BOOST_CHECK_CLOSE( qyTrue[1], qy[1], tol );

  qDataPrim[0] = phiz;
  qDataPrim[1] = rhoz;
  ArrayQ qzTrue(qDataPrim,2);
  ArrayQ qz;
  pde.setDOFFrom( qz, qDataPrim, qNamePrim, 2 );
  BOOST_CHECK_CLOSE( qzTrue[0], qz[0], tol );
  BOOST_CHECK_CLOSE( qzTrue[1], qz[1], tol );

  // diffusive flux
  ArrayQ fTrue = 0, gTrue = 0, hTrue = 0;
  ArrayQ F = 0, G = 0, H = 0;

  Real u = phix + a;
  Real v = phiy + b;
  Real w = phiz + c;

  Real V2 = u*u + v*v + w*w;
  Real V = sqrt(V2);

  Real M = pde.mach( q, qx, qy, qz );

  Real rhowind = rho - h*pde.mu(M)*(rhox*u + rhoy*v + rhoz*w)/V;


  fTrue[0] = -rhowind*u;
  gTrue[0] = -rhowind*v;
  hTrue[0] = -rhowind*w;

  pde.fluxViscous( h, x, y, z, 0, q, qx, qy, qz, F, G, H );
  BOOST_CHECK_CLOSE( fTrue[0], F[0], tol );
  BOOST_CHECK_CLOSE( fTrue[1], F[1], tol );
  BOOST_CHECK_CLOSE( gTrue[0], G[0], tol );
  BOOST_CHECK_CLOSE( gTrue[1], G[1], tol );
  BOOST_CHECK_CLOSE( hTrue[0], H[0], tol );
  BOOST_CHECK_CLOSE( hTrue[1], H[1], tol );

#if 0 // density upwinding means this does not work....
  // diffusive flux jacobian
  MatrixQ kxx = 0, kxy = 0, kxz = 0,
          kyx = 0, kyy = 0, kyz = 0,
          kzx = 0, kzy = 0, kzz = 0;
  pde.diffusionViscous( h, x, y, z, 0, q,
                        kxx, kxy, kxz,
                        kyx, kyy, kyz,
                        kzx, kzy, kzz);

  for (int i = 0; i < 2; i++)
  {
    for (int j = 0; j < 2; j++)
    {
      // First row
      if (i == 0 && j == 0)
        BOOST_CHECK_CLOSE( rho, kxx(i,j), tol );
      else
        BOOST_CHECK_EQUAL(   0, kxx(i,j) );
      BOOST_CHECK_EQUAL(   0, kxy(i,j) );
      BOOST_CHECK_EQUAL(   0, kxz(i,j) );

      // Middle row
      BOOST_CHECK_EQUAL(   0, kyx(i,j) );
      if (i == 0 && j == 0)
        BOOST_CHECK_CLOSE( rho, kyy(i,j), tol );
      else
        BOOST_CHECK_EQUAL(   0, kyy(i,j) );
      BOOST_CHECK_EQUAL(   0, kyz(i,j) );

      // Last row
      BOOST_CHECK_EQUAL(   0, kzx(i,j) );
      BOOST_CHECK_EQUAL(   0, kzy(i,j) );
      if (i == 0 && j == 0)
        BOOST_CHECK_CLOSE( rho, kzz(i,j), tol );
      else
        BOOST_CHECK_EQUAL(   0, kzz(i,j) );
    }
  }
#endif

  ArrayQ source = 0;
  pde.source( h, x, y, z, 0, q, qx, qy, qz, source );

  ArrayQ sourceTrue = 0;

  Real Vinf2 = a*a + b*b + c*c;
  sourceTrue[1] = q[1] - rhoinf*pow( 1 + (gamma-1.)/2.*Minf*Minf*(1 - V2/Vinf2), 1./(gamma-1.) );

  BOOST_CHECK_EQUAL( sourceTrue[0], source[0] );
  BOOST_CHECK_CLOSE( sourceTrue[1], source[1], tol );

  // check accumulation
  pde.source( h, x, y, z, 0, q, qx, qy, qz, source );

  BOOST_CHECK_EQUAL( 2*sourceTrue[0], source[0] );
  BOOST_CHECK_CLOSE( 2*sourceTrue[1], source[1], tol );

#if 0
  // forcing function

  source = 0;
  sourceTrue = -2*kxy*(4*PI*PI);
  pde.forcingFunction( x, y, source );
  BOOST_CHECK_CLOSE( sourceTrue, source, tol );
#endif

  // for coverage; functions are empty
  ArrayQ uCons = {1, 1};
  ArrayQ ft = 0;
  MatrixQ dfdu = 0;
  MatrixQ dgdu = 0;
  MatrixQ dhdu = 0;
  pde.masterState( h, x, y, z, 0, q, uCons );
  BOOST_CHECK_EQUAL( q[0], uCons[0] );
  BOOST_CHECK_EQUAL( q[1], uCons[1] );
  pde.fluxAdvectiveTime( h, x, y, z, 0, q, ft );
  pde.fluxAdvective( h, x, y, z, 0, q, F, G, H );
  pde.jacobianFluxAdvective( h, x, y, z, 0, q, dfdu, dgdu, dhdu );
  pde.sourceTrace( h, x, y, z, x, y, z, 0, q, qx, qy, qz, q, qx, qy, qz, source, source );
//  pde.forcingFunction( x, y, source );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxArea )
{
  typedef PDEFullPotential3D<Real> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  Real a = 1;
  Real b = 0.2;
  Real c = 0.5;

  Real gamma = 1.4;
  Real Minf = 0.5;
  Real rhoinf = 1.2;
  Real pinf = 1.8;
  Real critVelFrac = 0.9;

  PDEClass pde( a, b, c, gamma, Minf, rhoinf, pinf, critVelFrac );

  // viscous flux function

  Real h, x, y, z;
  Real nx, ny, nz;
  Real phiL, phiR;
  Real phixL, phiyL, phizL;
  Real phixR, phiyR, phizR;

  Real rhoL, rhoR;
  Real rhoxL, rhoyL, rhozL;
  Real rhoxR, rhoyR, rhozR;

  h = 0.467;

  x = 0; y = 0; z = 0;  // not actually used in functions
  nx = 1.22; ny = -0.432; nz = 2.561;
  phiL = 3.26; phiR = 1.79;
  phixL = 1.325;  phiyL = -0.457;  phizL = 1.432;
  phixR = 0.327;  phiyR =  3.421;  phizR = 2.731;

  rhoL = 2.13; rhoR = 4.27;
  rhoxL =  4.231;  rhoyL = -1.821;  rhozL =  2.894;
  rhoxR = -0.509;  rhoyR =  5.423;  rhozR = -3.091;

  // set
  Real qLDataPrim[2] = {phiL, rhoL};
  string qLNamePrim[2] = {"Solution", "Density"};
  ArrayQ qLTrue(qLDataPrim,2);
  ArrayQ qL;
  pde.setDOFFrom( qL, qLDataPrim, qLNamePrim, 2 );
  BOOST_CHECK_CLOSE( qLTrue[0], qL[0], tol );
  BOOST_CHECK_CLOSE( qLTrue[1], qL[1], tol );

  Real qRDataPrim[2] = {phiR, rhoR};
  string qRNamePrim[2] = {"Solution", "Density"};
  ArrayQ qRTrue(qRDataPrim,2);
  ArrayQ qR;
  pde.setDOFFrom( qR, qRDataPrim, qRNamePrim, 2 );
  BOOST_CHECK_CLOSE( qRTrue[0], qR[0], tol );
  BOOST_CHECK_CLOSE( qRTrue[1], qR[1], tol );

  // set gradient
  qLDataPrim[0] = phixL;
  qLDataPrim[1] = rhoxL;
  ArrayQ qxLTrue(qLDataPrim,2);
  ArrayQ qxL;
  pde.setDOFFrom( qxL, qLDataPrim, qLNamePrim, 2 );
  BOOST_CHECK_CLOSE( qxLTrue[0], qxL[0], tol );
  BOOST_CHECK_CLOSE( qxLTrue[1], qxL[1], tol );

  qLDataPrim[0] = phiyL;
  qLDataPrim[1] = rhoyL;
  ArrayQ qyLTrue(qLDataPrim,2);
  ArrayQ qyL;
  pde.setDOFFrom( qyL, qLDataPrim, qLNamePrim, 2 );
  BOOST_CHECK_CLOSE( qyLTrue[0], qyL[0], tol );
  BOOST_CHECK_CLOSE( qyLTrue[1], qyL[1], tol );

  qLDataPrim[0] = phizL;
  qLDataPrim[1] = rhozL;
  ArrayQ qzLTrue(qLDataPrim,2);
  ArrayQ qzL;
  pde.setDOFFrom( qzL, qLDataPrim, qLNamePrim, 2 );
  BOOST_CHECK_CLOSE( qzLTrue[0], qzL[0], tol );
  BOOST_CHECK_CLOSE( qzLTrue[1], qzL[1], tol );

  qRDataPrim[0] = phixR;
  qRDataPrim[1] = rhoxR;
  ArrayQ qxRTrue(qRDataPrim,2);
  ArrayQ qxR;
  pde.setDOFFrom( qxR, qRDataPrim, qRNamePrim, 2 );
  BOOST_CHECK_CLOSE( qxRTrue[0], qxR[0], tol );
  BOOST_CHECK_CLOSE( qxRTrue[1], qxR[1], tol );

  qRDataPrim[0] = phiyR;
  qRDataPrim[1] = rhoyR;
  ArrayQ qyRTrue(qRDataPrim,2);
  ArrayQ qyR;
  pde.setDOFFrom( qyR, qRDataPrim, qRNamePrim, 2 );
  BOOST_CHECK_CLOSE( qyRTrue[0], qyR[0], tol );
  BOOST_CHECK_CLOSE( qyRTrue[1], qyR[1], tol );

  qRDataPrim[0] = phizR;
  qRDataPrim[1] = rhozR;
  ArrayQ qzRTrue(qRDataPrim,2);
  ArrayQ qzR;
  pde.setDOFFrom( qzR, qRDataPrim, qRNamePrim, 2 );
  BOOST_CHECK_CLOSE( qzRTrue[0], qzR[0], tol );
  BOOST_CHECK_CLOSE( qzRTrue[1], qzR[1], tol );

  // viscous normal flux
  ArrayQ fnTrue = 0;
  fnTrue[0]= -0.5*(nx*(rhoL*phixL + rhoR*phixR) + ny*(rhoL*phiyL + rhoR*phiyR) + nz*(rhoL*phizL + rhoR*phizR));
  ArrayQ fn = 0;
  pde.fluxViscous( h, x, y, z, 0, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, nx, ny, nz, fn );
  BOOST_CHECK_CLOSE( fnTrue[0], fn[0], tol );
  BOOST_CHECK_EQUAL( fnTrue[1], fn[1] );

  // check accumulation
  pde.fluxViscous( h, x, y, z, 0, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, nx, ny, nz, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue[0], fn[0], tol );
  BOOST_CHECK_EQUAL( 2*fnTrue[1], fn[1] );

  // for coverage; functions are empty
  pde.fluxAdvective( h, x, y, z, 0, qL, qR, nx, ny, nz, fn );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic )
{
  typedef PDEFullPotential3D<Real> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  Real a = 1;
  Real b = 0.2;
  Real c = 0.5;

  Real gamma = 1.4;
  Real Minf = 0.5;
  Real rhoinf = 1.2;
  Real pinf = 1.8;
  Real critVelFrac = 0.9;

  PDEClass pde( a, b, c, gamma, Minf, rhoinf, pinf, critVelFrac );

  Real speed;
  Real x = 0;
  Real y = 0;
  Real z = 0;
  Real dx = 0;
  Real dy = 0;
  Real dz = 0;
  ArrayQ q = 0;

  pde.speedCharacteristic( x, y, z, 0, dx, dy, dz, q, speed );    // no check; empty function
  pde.speedCharacteristic( x, y, z, 0, q, speed );    // no check; empty function
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef PDEFullPotential3D<Real> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  Real a = 1;
  Real b = 0.2;
  Real c = 0.5;

  Real gamma = 1.4;
  Real Minf = 0.5;
  Real rhoinf = 1.2;
  Real pinf = 1.8;
  Real critVelFrac = 0.9;

  PDEClass pde( a, b, c, gamma, Minf, rhoinf, pinf, critVelFrac );

  ArrayQ q = 0;
  BOOST_CHECK( pde.isValidState(q) == false );

  q = 1;
  BOOST_CHECK( pde.isValidState(q) == true );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PDEFullPotential3D_pattern.txt", true );

  typedef PDEFullPotential3D<Real> PDEClass;

  Real a = 1;
  Real b = 0.2;
  Real c = 0.5;

  Real gamma = 1.4;
  Real Minf = 0.5;
  Real rhoinf = 1.2;
  Real pinf = 1.8;
  Real critVelFrac = 0.9;

  PDEClass pde( a, b, c, gamma, Minf, rhoinf, pinf, critVelFrac );
  pde.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
