// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// LinearizedIncompressiblePotential3D_Trefftz_btest
// testing of Trefftz plane integration for Lift (L), Drag (D), and Sideforce (Y)

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Surreal/SurrealS.h"

#include "pde/FullPotential/PDELinearizedIncompressiblePotential3D.h"
#include "pde/FullPotential/SolutionFunction3D_LIP.h"
#include "pde/FullPotential/BCLinearizedIncompressiblePotential3D.h"
#include "pde/FullPotential/LinearizedIncompressiblePotential3D_Trefftz.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "Meshing/EGADS/EGModel.h"
#include "Meshing/EGADS/extrude.h"
#include "Meshing/EGADS/solidBoolean.h"
#include "Meshing/EGADS/isSame.h"
#include "Meshing/EGADS/EGIntersection.h"

#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/output_Tecplot.h"
#include "Field/XFieldLine.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/Element/ElementVolume.h"
#include "Field/Element/ElementXFieldVolume.h"

#include "Meshing/TetGen/EGTetGen.h"
#include "Meshing/EGTess/EGTessModel.h"

using namespace std;
using namespace SANS;
using namespace EGADS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

// Un-named namespace makes these functions private to this file
namespace
{

EGModel<3> makeWakedBox(const EGContext& context,
                        const std::vector<double>& wakeParams)
{
  //Create the farfield box that is slightly larger than the Trefftz plane wake intersection
  // defined as x = 0.01, y in [-1,1], z = 0
  std::vector<double> Box = {0.00, -1.01, -0.05,
                             0.01,  2.02,  0.10};
  EGBody<3> box = context.makeSolidBody( BOX, Box );

  EGNode<3> nodeL(context, {-0.1,-1,0});
  EGNode<3> nodeR(context, {-0.1, 1,0});

  EGEdge<3> wakeedge(nodeL, nodeR);

  Real len = 5;
  DLA::VectorS<3,Real> dir = {1, 0., 0};

  //Nodes outside the box
  EGNode<3> outL(context, {-0.1, -2, 0.});
  EGNode<3> outR(context, {-0.1,  2, 0.});

  //Edges that extend from the wing tips outside the box
  EGEdge<3> edgeTipL (outL, nodeL);
  EGEdge<3> edgeTipR (nodeR, outR);

  EGLoop<3>::edge_vector wakeEdges;

  wakeEdges = {(edgeTipL,1),(wakeedge,1),(edgeTipR,1)};

  // Surface used to create the wake and cut the box
  EGBody<3> wakecutter = extrude( EGLoop<3>(context, wakeEdges, OPEN), len, dir );

  EGBody<3> boxscribed( EGIntersection<3>(box, wakecutter) );

  EGBody<3> wakelong = extrude( EGLoop<3>((wakeedge,1), OPEN), len, dir );

  EGBody<3> wake = intersect( EGModel<3>(wakelong), box ).getBodies()[0].clone();

  const int nSpan = std::max(2./wakeParams[0],2.);
  std::vector<double> rSpan(nSpan-2);
  for ( int i = 1; i < nSpan-1; i++ )
    rSpan[i-1] = i/Real(nSpan-1);
    //rSpan[i-1] = 1-0.5*(1+cos(PI*i/Real(nSpan-1)));

  //Set the tess resolution on the wake
  std::vector< EGFace<3> > wakefaces = wake.getFaces();
  for ( auto face = wakefaces.begin(); face != wakefaces.end(); face++ )
  {
    face->addAttribute(".tParams", wakeParams);
    face->addAttribute(EGTetGen::BCNAME, "Wing_Wake");
  }

  typedef EGNode<3>::CartCoord CartCoord;
  std::vector< EGEdge<3> > boxEdges = boxscribed.getEdges();
  std::vector< EGEdge<3> > WakeEdges = wake.getEdges();
  for ( auto edge = WakeEdges.begin(); edge != WakeEdges.end(); edge++ )
  {
    std::vector<EGNode<3>> nodes = edge->getNodes();

    if (nodes.size() == 1 ) continue;

    if ( ((CartCoord)nodes[0])[0] > Box[3]-1e-6 &&
         ((CartCoord)nodes[1])[0] > Box[3]-1e-6 )
    {
      for ( auto sedge = boxEdges.begin(); sedge != boxEdges.end(); sedge++ )
        if (isSame(*edge,*sedge))
          sedge->addAttribute(EGTetGen::TREFFTZ, "Wing_Wake"); // Mark the edge that is used for Treffetz plane integrals
    }
    else if ( ((CartCoord)nodes[0])[0] < Box[0]+1e-6 &&
              ((CartCoord)nodes[1])[0] < Box[0]+1e-6 )
    {
      for ( auto sedge = boxEdges.begin(); sedge != boxEdges.end(); sedge++ )
        if (isSame(*edge,*sedge))
          sedge->addAttribute(EGTetGen::WAKESHEET, "Wing_Wake"); // Mark the edge that is used for Kutta condition integrals
    }

    if ( fabs( ((CartCoord)nodes[0])[0] - ((CartCoord)nodes[1])[0] ) < 0.1 )
    {
      for ( auto sedge = boxEdges.begin(); sedge != boxEdges.end(); sedge++ )
        if (isSame(*edge,*sedge))
          sedge->addAttribute(".rPos", rSpan); //Span
    }
  }

  return EGModel<3>({wake, boxscribed});
}

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( LinearizedIncompressiblePotential_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Trefftz_test )
{
  typedef PDENDConvertSpace<PhysD3, PDELinearizedIncompressiblePotential3D<Real>> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  EGContext context((CreateContext()));

  // Refining the grid any more in the spanwise direction gives the incorrect integral
  // as the derivate of the elliptic potential field is singular at y = -1 and y = 1
  std::vector<double> wakeParams = {0.025,0.001,15.};
  std::vector<double> EGADSParams = {0.05*2.1, 0.001, 15.0};
  // TetGen mesh parameters
  Real maxRadiusEdgeRatio = 2.0;
  Real minDihedralAngle = 0;

  EGModel<3> model = makeWakedBox(context, wakeParams);
  //model.save("tmp/test.egads");

  EGTessModel tessModel(model, EGADSParams);

  EGTetGen xfld(tessModel, maxRadiusEdgeRatio, minDihedralAngle);

  // PDE
  Real U = 1, V = 0, W = 0;

  PDEClass pde( U, V, W );


  int order = 1;

  // solution: Hierarchical, C0
  Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);
  qfld = 0;

  Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical );
  lgfld = 0;

  EGTetGen::FieldCellGroupType<Tet>& xCellGroup = xfld.getCellGroup<Tet>(0);
  EGTetGen::FieldCellGroupType<Tet>::ElementType<> xCellElem( xCellGroup.basis() );

  Field_CG_Cell<PhysD3, TopoD3, ArrayQ>::FieldCellGroupType<Tet>& qCellGroup = qfld.getCellGroup<Tet>(0);
  Field_CG_Cell<PhysD3, TopoD3, ArrayQ>::FieldCellGroupType<Tet>::ElementType<> qCellElem( qCellGroup.basis() );

  EllipticPotential phi;

  for (int elem = 0; elem < xCellGroup.nElem(); elem++ )
  {
    xCellGroup.getElement(xCellElem, elem);
    qCellGroup.getElement(qCellElem, elem);

    for (int n = 0; n < xCellElem.nDOF(); n++)
      qCellElem.DOF(n) = phi(xCellElem.DOF(n));

    qCellGroup.setElement(qCellElem, elem);
  }

  std::vector<int> WakeFaces;

  {
    int BCFace = 0;
    std::vector< EGBody<3> > bodies = model.getBodies();
    for ( std::size_t ibody = 0; ibody < bodies.size(); ibody++ )
    {
      if (bodies[ibody].isSolid())
        BCFace += bodies[ibody].nFaces();
    }

    for ( std::size_t ibody = 0; ibody < bodies.size(); ibody++ ) //TODO: This should first go over solid bodies then sheet bodies...
    {
      if (bodies[ibody].isSolid()) continue;
      std::vector< EGFace<3> > faces = bodies[ibody].getFaces();
      for (std::size_t i = 0; i < faces.size(); i++)
      {
        if ( faces[i].hasAttribute(EGTetGen::BCNAME) )
        {
          WakeFaces.push_back(BCFace);
          BCFace++;
        }
      }
    }
  }

  BOOST_REQUIRE_EQUAL( WakeFaces.size(), 1 );

  EGTetGen::FieldTraceGroupType<Triangle>& xTraceGroup = xfld.getBoundaryTraceGroup<Triangle>(WakeFaces[0]);
  EGTetGen::FieldTraceGroupType<Triangle>::ElementType<> xTraceElem( xTraceGroup.basis() );

  // Traverse all elements on the wake, and perturb them so the solution from either
  // side of the wake is property initialized
  for (int elem = 0; elem < xTraceGroup.nElem(); elem++ )
  {
    int elemL = xTraceGroup.getElementLeft(elem);

    xCellGroup.getElement(xCellElem, elemL);
    qCellGroup.getElement(qCellElem, elemL);

    for (int n = 0; n < xCellElem.nDOF(); n++)
    {
      // Nudge the element so it's clearly below z = 0
      xCellElem.DOF(n)[2] -= 1e-12;
      qCellElem.DOF(n) = phi(xCellElem.DOF(n));
      //if (qCellElem.DOF(n) < 0)
      //std::cout << "lower: n = " << n << "  X = " << xCellElem.DOF(n) << "  phi = " << qCellElem.DOF(n) << std::endl;
    }

    qCellGroup.setElement(qCellElem, elemL);


    int elemR = xTraceGroup.getElementRight(elem);

    xCellGroup.getElement(xCellElem, elemR);
    qCellGroup.getElement(qCellElem, elemR);

    for (int n = 0; n < xCellElem.nDOF(); n++)
    {
      // Nudge the element so it's clearly above z = 0
      xCellElem.DOF(n)[2] += 1e-12;
      qCellElem.DOF(n) = phi(xCellElem.DOF(n));
      //if (qCellElem.DOF(n) > 0)
      //std::cout << "upper: n = " << n << "  X = " << xCellElem.DOF(n) << "  phi = " << qCellElem.DOF(n) << std::endl;
    }

    qCellGroup.setElement(qCellElem, elemR);
  }

#if 0
  string filename = "tmp/EllipticPotential_LIP.dat";
  cout << "calling output_Tecplot: filename = " << filename << endl;
  output_Tecplot_LIP( pde, qfld, filename );
#endif

  IntegrandBoundaryFrame3D_LIP_Trefftz<TrefftzInducedDrag, Real> fcnTrefftzDrag(pde, {1});
  IntegrandBoundaryFrame3D_LIP_Trefftz<TrefftzPlaneForce, Real>  fcnTrefftzLift(pde, {0,0,1}, {1});

  std::vector<int> quadratureOrder(xfld.nBoundaryFrameGroups(), -1);    // max quadrature

  Real Drag = 0;

  Real Gamma0 = 1; //Jump in potential at y = 0
  Real rho = 1;

  FunctionalTrefftz::integrate(fcnTrefftzDrag, xfld, qfld, quadratureOrder.data(), quadratureOrder.size(), Drag);

  const Real small_tol = 1e-12;
//  const Real close_tol = 1e-12;
  SANS_CHECK_CLOSE( PI/8*rho*Gamma0*Gamma0, Drag, small_tol, 8.8 );

  Real b = 2; //Span of the wing
  Real Lift = 0;

  FunctionalTrefftz::integrate(fcnTrefftzLift, xfld, qfld, quadratureOrder.data(), quadratureOrder.size(), Lift);

  SANS_CHECK_CLOSE( PI/4*rho*U*U*Gamma0*Gamma0*b, Lift, small_tol, 0.16 );

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
