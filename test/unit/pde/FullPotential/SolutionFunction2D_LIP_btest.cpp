// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// SolutionFunction2D_LIP_btest
//
// test of 2-D solution class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/FullPotential/SolutionFunction2D_LIP.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( SolutionFunction2D_LIP_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_Const )
{
  typedef SolutionFunction2D<1>::ArrayQ ArrayQ;

  SolutionFunction2D_Const<1> fcn( 1 );

  Real x = 1;
  Real y = 2;
  ArrayQ q, qx, qy, qxx, qxy, qyy;
  fcn(x, y, 0, q, qx, qy, qxx, qxy, qyy);

  const Real close_tol = 1e-13;
  const Real small_tol = 1e-13;

  SANS_CHECK_CLOSE( 1, q, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qxy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qyy, small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_Linear )
{
  typedef SolutionFunction2D<1>::ArrayQ ArrayQ;

  Real a0 =  1;
  Real ax = -3;
  Real ay =  4;

  SolutionFunction2D_Linear<1> fcn( a0, ax, ay );

  const Real close_tol = 1e-13;
  const Real small_tol = 1e-13;
  Real x, y;
  ArrayQ q, qx, qy, qxx, qxy, qyy;

  x = 0;
  y = 0;
  fcn(x, y, 0, q, qx, qy, qxx, qxy, qyy);
  SANS_CHECK_CLOSE( a0, q, small_tol, close_tol );
  SANS_CHECK_CLOSE( ax, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( ay, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qxy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qyy, small_tol, close_tol );

  x = 1;
  y = 2;
  fcn(x, y, 0, q, qx, qy, qxx, qxy, qyy);
  SANS_CHECK_CLOSE( a0 + ax*x + ay*y, q, small_tol, close_tol );
  SANS_CHECK_CLOSE( ax, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( ay, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qxy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qyy, small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_SineSine )
{
  typedef SolutionFunction2D<1>::ArrayQ ArrayQ;

  SolutionFunction2D_SineSine<1> fcn;

  const Real close_tol = 1e-13;
  const Real small_tol = 1e-13;
  Real x, y;
  ArrayQ q, qx, qy, qxx, qxy, qyy;

  x = 0;
  y = 0;
  fcn(x, y, 0, q, qx, qy, qxx, qxy, qyy);
  SANS_CHECK_CLOSE( 0, q, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE( 4*PI*PI, qxy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qyy, small_tol, close_tol );

  x = 1;
  y = 1;
  fcn(x, y, 0, q, qx, qy, qxx, qxy, qyy);
  SANS_CHECK_CLOSE( 0, q, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE( 4*PI*PI, qxy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qyy, small_tol, close_tol );

  x = 0.5;
  y = 1;
  fcn(x, y, 0, q, qx, qy, qxx, qxy, qyy);
  SANS_CHECK_CLOSE( 0, q, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI, qxy, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qyy, small_tol, close_tol );

  x = 0.25;
  y = 0.25;
  fcn(x, y, 0, q, qx, qy, qxx, qxy, qyy);
  SANS_CHECK_CLOSE( 1, q, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qy, small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI, qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, qxy, small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI, qyy, small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_CubicSourceBump )
{
  typedef SolutionFunction2D<1>::ArrayQ ArrayQ;

  Real tau = 0.1;

  SolutionFunction2D_CubicSourceBump<1> fcn( tau );

  const Real close_tol = 1e-13;
  const Real small_tol = 1e-13;
  Real x, y;
  ArrayQ q;
  Real p;

  x = 0;
  y = 0;
  p = -0.058268083616295029451674064301430;
  q = fcn( x, y, 0 );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );

  x = 1;
  y = 0;
  p = +0.058268083616295029451674064301430;
  q = fcn( x, y, 0 );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );

  x = 0.5;
  y = 0;
  p = 0;
  q = fcn( x, y, 0 );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );

  x = 0;
  y = 1;
  p = -0.0087660881433560155366196380166540;
  q = fcn( x, y, 0 );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );

  x = 1;
  y = 1;
  p = +0.0087660881433560155366196380166540;
  q = fcn( x, y, 0 );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );

  x = 0.5;
  y = 1;
  p = 0;
  q = fcn( x, y, 0 );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( functor_Joukowski )
{
  typedef SolutionFunction2D<1>::ArrayQ ArrayQ;

  Real e = 0.1;
  Real aoa = 15;

  SolutionFunction2D_Joukowski<1> fcn( e, aoa );

  const Real close_tol = 1e-11;
  const Real small_tol = 1e-13;
  Real x, y;
  ArrayQ q, qx, qy, qxx, qxy, qyy;
  Real p, px, py;

  x = 1; y = 1;
  p  = -0.58920444256397108029535110499700;
  px =  0.093327264334358417293138366904870;
  py = -0.071396738467994931538443069177670;
  q = fcn( x, y, 0 );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );
  fcn( x, y, 0, q, qx, qy, qxx, qxy, qyy );
  SANS_CHECK_CLOSE( p,  q,  small_tol, close_tol );
  SANS_CHECK_CLOSE( px, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( py, qy, small_tol, close_tol );

  x = 1; y = -1;
  p  = -1.21318330086992919230004362554343;
  px = -0.085073334447847632869910960287526;
  py = -0.056776417545814745059052593069822;
  q = fcn( x, y, 0 );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );
  fcn( x, y, 0, q, qx, qy, qxx, qxy, qyy );
  SANS_CHECK_CLOSE( p,  q,  small_tol, close_tol );
  SANS_CHECK_CLOSE( px, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( py, qy, small_tol, close_tol );

  x = -1; y = 1;
  p  = -0.81469131994274930363399326947714;
  px =  0.056610709293132151766539703287421;
  py =  0.072674144833788130592869118452175;
  q = fcn( x, y, 0 );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );
  fcn( x, y, 0, q, qx, qy, qxx, qxy, qyy );
  SANS_CHECK_CLOSE( p,  q,  small_tol, close_tol );
  SANS_CHECK_CLOSE( px, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( py, qy, small_tol, close_tol );

  x = -1; y = -1;
  p  = -1.01016977280027913100774390089873;
  px = -0.058900512028353461599140154061055;
  py =  0.064111616455402308691054157310479;
  q = fcn( x, y, 0 );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );
  fcn( x, y, 0, q, qx, qy, qxx, qxy, qyy );
  SANS_CHECK_CLOSE( p,  q,  small_tol, close_tol );
  SANS_CHECK_CLOSE( px, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( py, qy, small_tol, close_tol );

  x = 1; y = 0.01;
  p  = -0.44164404166038230041354127360481;
  px = -0.05501353882233457244903495070583;
  py = -0.25793073603015143080283682066314;
  q = fcn( x, y, 0 );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );
  fcn( x, y, 0, q, qx, qy, qxx, qxy, qyy );
  SANS_CHECK_CLOSE( p,  q,  small_tol, close_tol );
  SANS_CHECK_CLOSE( px, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( py, qy, small_tol, close_tol );

  x = 1; y = -0.01;
  p  = -1.32369367949818754021875970847945;
  px = -0.08672181634518794601815746297379;
  py = -0.22909180194660185819141688829477;
  q = fcn( x, y, 0 );
  SANS_CHECK_CLOSE( p, q, small_tol, close_tol );
  fcn( x, y, 0, q, qx, qy, qxx, qxy, qyy );
  SANS_CHECK_CLOSE( p,  q,  small_tol, close_tol );
  SANS_CHECK_CLOSE( px, qx, small_tol, close_tol );
  SANS_CHECK_CLOSE( py, qy, small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
