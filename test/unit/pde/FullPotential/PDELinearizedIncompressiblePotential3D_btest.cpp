// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDELinearizedIncompressiblePotential3D_btest
//
// test of 3-D Linearized Incompressible Potential PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/FullPotential/PDELinearizedIncompressiblePotential3D.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( PDELinearizedIncompressiblePotential3D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDELinearizedIncompressiblePotential3D<Real> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  BOOST_CHECK( PDEClass::N == 1 );
  BOOST_CHECK( (std::is_same<ArrayQ,Real>::value) );
  BOOST_CHECK( (std::is_same<MatrixQ,Real>::value) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef PDELinearizedIncompressiblePotential3D<Real> PDEClass;

  Real a = 1.1;
  Real b = 0.2;
  Real c = 0.5;

  PDEClass pde0( a, b, c );

  BOOST_CHECK( pde0.N == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxVolume )
{
  typedef PDELinearizedIncompressiblePotential3D<Real> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real a = 1;
  Real b = 0.2;
  Real c = 0.5;

  PDEClass pde( a, b, c );

  // static tests
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == false );
  BOOST_CHECK( pde.hasFluxAdvective() == false );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // function tests

  Real x, y, z;
  Real phi, phix, phiy, phiz;

  x = 0; y = 0; z = 0;
  phi  =  3.263;
  phix = -0.445;
  phiy =  1.741;
  phiz =  2.321;

  // set
  Real qDataPrim[1] = {phi};
  string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = qDataPrim[0];
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  // set gradient
  qDataPrim[0] = phix;
  ArrayQ qxTrue = qDataPrim[0];
  ArrayQ qx;
  pde.setDOFFrom( qx, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxTrue, qx, tol );

  qDataPrim[0] = phiy;
  ArrayQ qyTrue = qDataPrim[0];
  ArrayQ qy;
  pde.setDOFFrom( qy, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyTrue, qy, tol );

  qDataPrim[0] = phiz;
  ArrayQ qzTrue = qDataPrim[0];
  ArrayQ qz;
  pde.setDOFFrom( qz, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qzTrue, qz, tol );

  // diffusive flux
  ArrayQ fTrue, gTrue, hTrue;
  ArrayQ f, g, h;
  fTrue = -phix;
  gTrue = -phiy;
  hTrue = -phiz;
  f = 0;
  g = 0;
  h = 0;
  pde.fluxViscous( x, y, z, 0, q, qx, qy, qz, f, g, h );
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( gTrue, g, tol );
  BOOST_CHECK_CLOSE( hTrue, h, tol );

  // check accumulation
  pde.fluxViscous( x, y, z, 0, q, qx, qy, qz, f, g, h );
  BOOST_CHECK_CLOSE( 2*fTrue, f, tol );
  BOOST_CHECK_CLOSE( 2*gTrue, g, tol );
  BOOST_CHECK_CLOSE( 2*hTrue, h, tol );

  // diffusive flux jacobian
  MatrixQ kxx = 0, kxy = 0, kxz = 0,
          kyx = 0, kyy = 0, kyz = 0,
          kzx = 0, kzy = 0, kzz = 0;
  pde.diffusionViscous( x, y, z, 0,
                        q, qx, qy, qz,
                        kxx, kxy, kxz,
                        kyx, kyy, kyz,
                        kzx, kzy, kzz);

  BOOST_CHECK_CLOSE( 1, kxx, tol );
  BOOST_CHECK_CLOSE( 0, kxy, tol );
  BOOST_CHECK_CLOSE( 0, kxz, tol );

  BOOST_CHECK_CLOSE( 0, kyx, tol );
  BOOST_CHECK_CLOSE( 1, kyy, tol );
  BOOST_CHECK_CLOSE( 0, kyz, tol );

  BOOST_CHECK_CLOSE( 0, kzx, tol );
  BOOST_CHECK_CLOSE( 0, kzy, tol );
  BOOST_CHECK_CLOSE( 1, kzz, tol );

  // check accumulation
  pde.diffusionViscous( x, y, z, 0,
                        q, qx, qy, qz,
                        kxx, kxy, kxz,
                        kyx, kyy, kyz,
                        kzx, kzy, kzz);

  BOOST_CHECK_CLOSE( 2, kxx, tol );
  BOOST_CHECK_CLOSE( 0, kxy, tol );
  BOOST_CHECK_CLOSE( 0, kxz, tol );

  BOOST_CHECK_CLOSE( 0, kyx, tol );
  BOOST_CHECK_CLOSE( 2, kyy, tol );
  BOOST_CHECK_CLOSE( 0, kyz, tol );

  BOOST_CHECK_CLOSE( 0, kzx, tol );
  BOOST_CHECK_CLOSE( 0, kzy, tol );
  BOOST_CHECK_CLOSE( 2, kzz, tol );

#if 0
  // forcing function

  source = 0;
  sourceTrue = -2*kxy*(4*PI*PI);
  pde.forcingFunction( x, y, source );
  BOOST_CHECK_CLOSE( sourceTrue, source, tol );
#endif

  // for coverage; functions are empty
  ArrayQ uCons = 1.0;
  ArrayQ ft = 0;
  MatrixQ dfdu = 0;
  MatrixQ dgdu = 0;
  MatrixQ dhdu = 0;
  ArrayQ source = 0;
  pde.masterState( x, y, z, 0, q, uCons );
  BOOST_CHECK_EQUAL( q, uCons );
  pde.fluxAdvectiveTime( x, y, z, 0, q, ft );
  pde.fluxAdvective( x, y, z, 0, q, f, g, h );
  pde.jacobianFluxAdvective( x, y, z, 0, q, dfdu, dgdu, dhdu );
  pde.sourceTrace( x, y, z, x, y, z, 0, q, qx, qy, qz, q, qx, qy, qz, source, source );
//  pde.forcingFunction( x, y, source );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxArea )
{
  typedef PDELinearizedIncompressiblePotential3D<Real> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  Real a = 1;
  Real b = 0.2;
  Real c = 0.5;

  PDEClass pde( a, b, c );

  // viscous flux function

  Real x, y, z;
  Real nx, ny, nz;
  Real phiL, phiR;
  Real phixL, phiyL, phizL;
  Real phixR, phiyR, phizR;

  x = 0; y = 0; z = 0;  // not actually used in functions
  nx = 1.22; ny = -0.432; nz = 2.561;
  phiL = 3.26; phiR = 1.79;
  phixL = 1.325;  phiyL = -0.457;  phizL = 1.432;
  phixR = 0.327;  phiyR =  3.421;  phizR = 2.731;

  // set
  Real qLDataPrim[1] = {phiL};
  string qLNamePrim[1] = {"Solution"};
  ArrayQ qLTrue = qLDataPrim[0];
  ArrayQ qL;
  pde.setDOFFrom( qL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qLTrue, qL, tol );

  Real qRDataPrim[1] = {phiR};
  string qRNamePrim[1] = {"Solution"};
  ArrayQ qRTrue = qRDataPrim[0];
  ArrayQ qR;
  pde.setDOFFrom( qR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qRTrue, qR, tol );

  // set gradient
  qLDataPrim[0] = phixL;
  ArrayQ qxLTrue = qLDataPrim[0];
  ArrayQ qxL;
  pde.setDOFFrom( qxL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxLTrue, qxL, tol );

  qLDataPrim[0] = phiyL;
  ArrayQ qyLTrue = qLDataPrim[0];
  ArrayQ qyL;
  pde.setDOFFrom( qyL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyLTrue, qyL, tol );

  qLDataPrim[0] = phizL;
  ArrayQ qzLTrue = qLDataPrim[0];
  ArrayQ qzL;
  pde.setDOFFrom( qzL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qzLTrue, qzL, tol );

  qRDataPrim[0] = phixR;
  ArrayQ qxRTrue = qRDataPrim[0];
  ArrayQ qxR;
  pde.setDOFFrom( qxR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxRTrue, qxR, tol );

  qRDataPrim[0] = phiyR;
  ArrayQ qyRTrue = qRDataPrim[0];
  ArrayQ qyR;
  pde.setDOFFrom( qyR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyRTrue, qyR, tol );

  qRDataPrim[0] = phizR;
  ArrayQ qzRTrue = qRDataPrim[0];
  ArrayQ qzR;
  pde.setDOFFrom( qzR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qzRTrue, qzR, tol );

  // viscous normal flux
  ArrayQ fnTrue = -0.5*(nx*(phixL + phixR) + ny*(phiyL + phiyR) + nz*(phizL + phizR));
  ArrayQ fn = 0;
  pde.fluxViscous( x, y, z, 0, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, nx, ny, nz, fn );
  BOOST_CHECK_CLOSE( fnTrue, fn, tol );

  // check accumulation
  pde.fluxViscous( x, y, z, 0, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, nx, ny, nz, fn );
  BOOST_CHECK_CLOSE( 2*fnTrue, fn, tol );

  // for coverage; functions are empty
  pde.fluxAdvective( x, y, z, 0, qL, qR, nx, ny, nz, fn );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic )
{
  typedef PDELinearizedIncompressiblePotential3D<Real> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  Real a = 1;
  Real b = 0.2;
  Real c = 0.5;

  PDEClass pde( a, b, c );

  Real speed;
  Real x = 0;
  Real y = 0;
  Real z = 0;
  Real dx = 0;
  Real dy = 0;
  Real dz = 0;
  ArrayQ q = 0;

  pde.speedCharacteristic( x, y, z, 0, dx, dy, dz, q, speed );    // no check; empty function
  pde.speedCharacteristic( x, y, z, 0, q, speed );    // no check; empty function
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidState )
{
  typedef PDELinearizedIncompressiblePotential3D<Real> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  Real a = 1;
  Real b = 0.2;
  Real c = 0.5;

  PDEClass pde( a, b, c );

  ArrayQ q = 0;
  BOOST_CHECK( pde.isValidState(q) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/PDELinearizedIncompressiblePotential3D_pattern.txt", true );

  typedef PDELinearizedIncompressiblePotential3D<Real> PDEClass;

  Real a = 1;
  Real b = 0.2;
  Real c = 0.5;

  PDEClass pde( a, b, c );
  pde.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
