// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// SolutionFunction3D_LIP_btest
//
// test of 3-D solution class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/FullPotential/OutputLIP3D.h"
#include "pde/FullPotential/PDELinearizedIncompressiblePotential3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( OutputLIP3D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Force_test )
{
  typedef PDELinearizedIncompressiblePotential3D<Real> PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorX VectorX;
  typedef OutputLIP3D_Force<NDPDEClass> OutputType;
  typedef OutputType::template ArrayJ<Real> ArrayJ;

  Real a = 1;
  Real b = 0.2;
  Real c = 0.5;

  NDPDEClass pde( a, b, c );

  ArrayQ q  =  0.5;
  ArrayQ qx = -0.37;
  ArrayQ qy =  0.84;
  ArrayQ qz =  1.24;

  Real nx = 0.3, ny = 0.2, nz = 0.8;;

  VectorX N = {nx, ny, nz};

  Real gradq2 = qx*qx + qy*qy + qz*qz;

  // Force integrand
  VectorX forceTrue = (-0.5*gradq2 - (qx*a + qy*b + qz*c))*N;

  Real x = 0.2, y = 0.4, z = 0.5;;
  Real time = 1.0;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  { // x
    OutputType outForce(pde, 1., 0., 0.);

    BOOST_CHECK_EQUAL( true, outForce.needsSolutionGradient() );

    ArrayJ force;
    outForce(x, y, z, time, nx, ny, nz, q, qx, qy, qz, force);

    SANS_CHECK_CLOSE( forceTrue[0], force, small_tol, close_tol );
  }

  { // y
    OutputType outForce(pde, 0., 1., 0.);

    BOOST_CHECK_EQUAL( true, outForce.needsSolutionGradient() );

    ArrayJ force;
    outForce(x, y, z, time, nx, ny, nz, q, qx, qy, qz, force);

    SANS_CHECK_CLOSE( forceTrue[1], force, small_tol, close_tol );
  }

  { // z
    OutputType outForce(pde, 0., 0., 1.);

    BOOST_CHECK_EQUAL( true, outForce.needsSolutionGradient() );

    ArrayJ force;
    outForce(x, y, z, time, nx, ny, nz, q, qx, qy, qz, force);

    SANS_CHECK_CLOSE( forceTrue[2], force, small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Moment_test )
{
  typedef PDELinearizedIncompressiblePotential3D<Real> PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorX VectorX;
  typedef OutputLIP3D_Force<NDPDEClass> OutputForceType;
  typedef OutputLIP3D_Moment<NDPDEClass> OutputMomentType;
  typedef OutputForceType::template ArrayJ<Real> ArrayJ;

  Real a = 1;
  Real b = 0.2;
  Real c = 0.5;

  NDPDEClass pde( a, b, c );

  Real x0 = 0.3, y0 = 0.1, z0 = 1.5;

  pde.setRefLocation({x0, y0, z0});

  ArrayQ q  =  0.5;
  ArrayQ qx = -0.37;
  ArrayQ qy =  0.84;
  ArrayQ qz =  1.24;

  Real nx = 0.3, ny = 0.2, nz = 0.8;;
  Real x = 0.2, y = 0.4, z = 0.5;;
  Real time = 1.0;

  VectorX N = {nx, ny, nz};

  Real gradq2 = qx*qx + qy*qy + qz*qz;

  // Force integrand
  VectorX forceTrue = (-0.5*gradq2 - (qx*a + qy*b + qz*c))*N;

  VectorX dX = {(x - x0), (y - y0), (z - z0)};
  VectorX momentTrue = cross(dX, forceTrue);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  { // x
    OutputMomentType outMoment(pde, 1., 0., 0.);

    BOOST_CHECK_EQUAL( true, outMoment.needsSolutionGradient() );

    ArrayJ moment;
    outMoment(x, y, z, time, nx, ny, nz, q, qx, qy, qz, moment);

    SANS_CHECK_CLOSE( momentTrue[0], moment, small_tol, close_tol );
  }

  { // y
    OutputMomentType outMoment(pde, 0., 1., 0.);

    BOOST_CHECK_EQUAL( true, outMoment.needsSolutionGradient() );

    ArrayJ moment;
    outMoment(x, y, z, time, nx, ny, nz, q, qx, qy, qz, moment);

    SANS_CHECK_CLOSE( momentTrue[1], moment, small_tol, close_tol );
  }

  { // z
    OutputMomentType outMoment(pde, 0., 0., 1.);

    BOOST_CHECK_EQUAL( true, outMoment.needsSolutionGradient() );

    ArrayJ moment;
    outMoment(x, y, z, time, nx, ny, nz, q, qx, qy, qz, moment);

    SANS_CHECK_CLOSE( momentTrue[2], moment, small_tol, close_tol );
  }
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
