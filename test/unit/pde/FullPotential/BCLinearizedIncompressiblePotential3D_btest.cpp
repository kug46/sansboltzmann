// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCLinearizedIncompressiblePotential3D_btest
//
// test of 3-D Linearized Incompressible Potential BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/FullPotential/BCLinearizedIncompressiblePotential3D.h"
#include "pde/FullPotential/SolutionFunction3D_LIP.h"

#include "pde/BCParameters.h"

using namespace std;
using namespace SANS;


namespace SANS
{

}   // namespace SANS

using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct

template class SANS::BCLinearizedIncompressiblePotential3D<BCTypeDirichlet, Real>;
template class SANS::BCLinearizedIncompressiblePotential3D<BCTypeNeumann, Real>;
template class SANS::BCLinearizedIncompressiblePotential3D<BCTypeWall, Real>;
//template class SANS::BCLinearizedIncompressiblePotential3D<BCTypeFarfieldVortex, Real>;
//template class SANS::BCLinearizedIncompressiblePotential3D<BCTypeFunction<SolutionDummy>, Real >;


//############################################################################//
BOOST_AUTO_TEST_SUITE( BCLinearizedIncompressiblePotential3D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  {
  typedef BCLinearizedIncompressiblePotential3D<BCTypeDirichlet, Real> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 1 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( (std::is_same<ArrayQ,Real>::value) );
  BOOST_CHECK( (std::is_same<MatrixQ,Real>::value) );
  }

  {
  typedef BCLinearizedIncompressiblePotential3D<BCTypeNeumann, Real> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 1 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( (std::is_same<ArrayQ,Real>::value) );
  BOOST_CHECK( (std::is_same<MatrixQ,Real>::value) );
  }

  {
  typedef BCLinearizedIncompressiblePotential3D<BCTypeControl, Real> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 1 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( (std::is_same<ArrayQ,Real>::value) );
  BOOST_CHECK( (std::is_same<MatrixQ,Real>::value) );
  }
#if 0
  {
  typedef BCLinearizedIncompressiblePotential3D<BCTypeFarfieldVortex, Real> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 1 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( ArrayQ::N == 1 );
  BOOST_CHECK( MatrixQ::M == 1 );
  BOOST_CHECK( MatrixQ::N == 1 );
  }

  {
  typedef BCLinearizedIncompressiblePotential3D<BCTypeFunction<SolutionDummy>, Real > BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 1 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( (std::is_same<ArrayQ,Real>::value) );
  BOOST_CHECK( (std::is_same<MatrixQ,Real>::value) );
  }
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctor )
{
  typedef PDELinearizedIncompressiblePotential3D<Real> PDEClass;

  Real u =  0.95;
  Real v = -0.21;
  Real w =  1.55;

  PDEClass pde(u, v, w);

  {
  typedef BCLinearizedIncompressiblePotential3D<BCTypeDirichlet, Real> BCClass;

  typedef BCParameters< BCLinearizedIncompressiblePotential3DVector<Real> > BCParams;

  Real bcdata = 1.1;

  BCClass bc1(bcdata);

  //Pydict constructor
  PyDict BCDirichlet;
  BCDirichlet[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet;
  BCDirichlet[BCLinearizedIncompressiblePotential3DParams<BCTypeDirichlet>::params.q] = bcdata;

  PyDict BCList;
  BCList["BC1"] = BCDirichlet;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCDirichlet);
  }

  {
  typedef BCLinearizedIncompressiblePotential3D<BCTypeNeumann, Real> BCClass;

  typedef BCParameters< BCLinearizedIncompressiblePotential3DVector<Real> > BCParams;

  Real bcdata = 1.1;

  BCClass bc1(bcdata);

  //Pydict constructor
  PyDict BCNeumann;
  BCNeumann[BCParams::params.BC.BCType] = BCParams::params.BC.Neumann;
  BCNeumann[BCLinearizedIncompressiblePotential3DParams<BCTypeNeumann>::params.gradqn] = bcdata;

  PyDict BCList;
  BCList["BC1"] = BCNeumann;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCNeumann);
  }

  {
  typedef BCLinearizedIncompressiblePotential3D<BCTypeWall, Real> BCClass;

  typedef BCParameters< BCLinearizedIncompressiblePotential3DVector<Real> > BCParams;

  BCClass bc1(pde);

  //Pydict constructor
  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.Wall;

  PyDict BCList;
  BCList["BC1"] = BCWall;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCWall);
  }

  {
  typedef BCLinearizedIncompressiblePotential3D<BCTypeWall, Real> BCClass;

  typedef BCParameters< BCLinearizedIncompressiblePotential3DVector<Real> > BCParams;

  BCClass bc1(pde);

  //Pydict constructor
  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.Wall;

  PyDict BCList;
  BCList["BC1"] = BCWall;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCWall);
  }

  {
  typedef BCLinearizedIncompressiblePotential3D<BCTypeControl, Real> BCClass;

  typedef BCParameters< BCLinearizedIncompressiblePotential3DVector<Real> > BCParams;

  DLA::VectorS<3,Real> hinge = {1, 0, 0};
  Real deflect = 45;
  BCClass bc1(pde, hinge, deflect);

  //Pydict constructor
  PyDict BCControl;
  BCControl[BCParams::params.BC.BCType] = BCParams::params.BC.ControlSurface;
  BCControl[BCLinearizedIncompressiblePotential3DParams<BCTypeControl>::params.hinge] = {1, 0, 0};
  BCControl[BCLinearizedIncompressiblePotential3DParams<BCTypeControl>::params.deflect] = 45;

  PyDict BCList;
  BCList["BC1"] = BCControl;
  BCParams::checkInputs(BCList);

  BCClass bc2(pde, BCControl);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeDirichlet_test )
{
  typedef BCLinearizedIncompressiblePotential3D<BCTypeDirichlet, Real> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real bcdataTrue = 3;

  BCClass bc( bcdataTrue );

  // static tests
  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  MatrixQ A;
  MatrixQ B;
  ArrayQ bcdata;

  Real x = 0;
  Real y = 0;
  Real z = 0;
  Real nx = 0.8;
  Real ny = 0.6;
  Real nz = 0.7;

  A = 0;
  B = 0;
  bc.coefficients( x, y, z, 0, nx, ny, nz, A, B );
  BOOST_CHECK_CLOSE( 1, A, tol );
  BOOST_CHECK_CLOSE( 0, B, tol );

  bcdata = 0;
  bc.data( x, y, z, 0, nx, ny, nz, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeNeumann_test )
{
  typedef BCLinearizedIncompressiblePotential3D<BCTypeNeumann, Real> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real bcdataTrue = 3;

  BCClass bc( bcdataTrue );

  // static tests
  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  MatrixQ A;
  MatrixQ B;
  ArrayQ bcdata;

  Real x = 0;
  Real y = 0;
  Real z = 0;
  Real nx = 0.8;
  Real ny = 0.6;
  Real nz = 0.7;

  A = 0;
  B = 0;
  bc.coefficients( x, y, z, 0, nx, ny, nz, A, B );
  BOOST_CHECK_CLOSE( 0, A, tol );
  BOOST_CHECK_CLOSE( 1, B, tol );

  bcdata = 0;
  bc.data( x, y, z, 0, nx, ny, nz, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeWall_test )
{
  typedef BCLinearizedIncompressiblePotential3D<BCTypeWall, Real> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real u =  0.95;
  Real v = -0.21;
  Real w =  1.55;

  PDELinearizedIncompressiblePotential3D<Real> pde(u, v, w);

  BCClass bc( pde );

  // static tests
  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  MatrixQ A;
  MatrixQ B;
  ArrayQ bcdata;
  SANS::DLA::VectorD<Real> globalVar(1);

  Real x = 0;
  Real y = 0;
  Real z = 0;
  Real nx = 0.8;
  Real ny = 0.6;
  Real nz = 0.7;

  A = 0;
  B = 0;
  bc.coefficients( x, y, z, 0, nx, ny, nz, A, B );
  BOOST_CHECK_CLOSE( 0, A, tol );
  BOOST_CHECK_CLOSE( 1, B, tol );

  globalVar[0] = 0;
  bcdata = 0;
  bc.data( x, y, z, 0, nx, ny, nz, bcdata );
  BOOST_CHECK_CLOSE( -(nx*u + ny*v + nz*w), bcdata, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeWall_Sensitvity_test )
{
  typedef BCLinearizedIncompressiblePotential3D<BCTypeWall, SurrealS<1>> BCClass;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  SurrealS<1> u =  0.95;
  SurrealS<1> v = -0.21;
  SurrealS<1> w =  1.55;

  u.deriv() = 0.1;
  v.deriv() = 0.2;
  w.deriv() = 0.3;

  PDELinearizedIncompressiblePotential3D<SurrealS<1>> pde(u, v, w);

  BCClass bc( pde );

  // static tests
  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  MatrixQ A;
  MatrixQ B;
  SurrealS<1> bcdata;

  Real x = 0;
  Real y = 0;
  Real z = 0;
  Real nx = 0.8;
  Real ny = 0.6;
  Real nz = 0.7;

  A = 0;
  B = 0;
  bc.coefficients( x, y, z, 0, nx, ny, nz, A, B );
  BOOST_CHECK_CLOSE( 0, A, tol );
  BOOST_CHECK_CLOSE( 1, B, tol );

  bcdata = 0;
  bc.data( x, y, z, 0, nx, ny, nz, bcdata );
  BOOST_CHECK_CLOSE( -(nx*u.value() + ny*v.value() + nz*w.value()), bcdata.value(), tol );
  BOOST_CHECK_CLOSE( -(nx*u.deriv() + ny*v.deriv() + nz*w.deriv()), bcdata.deriv(), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeControlSurface_test )
{
  typedef BCLinearizedIncompressiblePotential3D<BCTypeControl, Real> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  MatrixQ A;
  MatrixQ B;
  ArrayQ bcdata;

  Real x = 0;
  Real y = 0;
  Real z = 0;
  Real time = 0;

  Real u =  0.95;
  Real v = -0.21;
  Real w =  1.55;

  PDELinearizedIncompressiblePotential3D<Real> pde(u, v, w);

  DLA::VectorS<3,Real> hinge = {0, 1, 0};
  Real deflect = 0;
  BCClass bc(pde, hinge, deflect);

  // function tests
  Real nx = 0.8;
  Real ny = 0.6;
  Real nz = 0.7;

  A = 0;
  B = 0;
  bc.coefficients( x, y, z, time, nx, ny, nz, A, B );
  BOOST_CHECK_CLOSE( 0, A, tol );
  BOOST_CHECK_CLOSE( 1, B, tol );

  bcdata = 0;
  bc.data( x, y, z, time, nx, ny, nz, bcdata );
  BOOST_CHECK_CLOSE( -(nx*u + ny*v + nz*w), bcdata, tol );

  {
    DLA::VectorS<3,Real> hinge = {0, 1, 0};
    Real deflect = 90;
    BCClass bc(pde, hinge, deflect);

    nx = 0; ny = 0; nz = 1;

    bcdata = 0;
    bc.data( x, y, z, time, nx, ny, nz, bcdata );

    // Rotate the normal consistent with the BC
    nx = 1; ny = 0; nz = 0;
    BOOST_CHECK_CLOSE( -(nx*u + ny*v + nz*w), bcdata, tol );
  }

  {
    DLA::VectorS<3,Real> hinge = {0, 0, 1};
    Real deflect = 90;
    BCClass bc(pde, hinge, deflect);

    nx = 0; ny = 1; nz = 0;

    bcdata = 0;
    bc.data( x, y, z, time, nx, ny, nz, bcdata );

    // Rotate the normal consistent with the BC
    nx = -1; ny = 0; nz = 0;
    BOOST_CHECK_CLOSE( -(nx*u + ny*v + nz*w), bcdata, tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/BCLinearizedIncompressiblePotential3D_pattern.txt", true );

  {
  typedef BCLinearizedIncompressiblePotential3D<BCTypeDirichlet, Real> BCClass;

  Real bcdata = 3;

  BCClass bc( bcdata );
  bc.dump( 2, output );
  }

  {
  typedef BCLinearizedIncompressiblePotential3D<BCTypeNeumann, Real> BCClass;

  Real bcdata = 3;

  BCClass bc( bcdata );
  bc.dump( 2, output );
  }

  {
  typedef BCLinearizedIncompressiblePotential3D<BCTypeWall, Real> BCClass;

  Real u =  0.95;
  Real v = -0.21;
  Real w =  1.55;

  PDELinearizedIncompressiblePotential3D<Real> pde(u, v, w);

  BCClass bc( pde );
  bc.dump( 2, output );
  }

  {
  typedef BCLinearizedIncompressiblePotential3D<BCTypeControl, Real> BCClass;

  Real u =  0.95;
  Real v = -0.21;
  Real w =  1.55;

  PDELinearizedIncompressiblePotential3D<Real> pde(u, v, w);

  DLA::VectorS<3,Real> hinge = {0.1, 0.7, -0.4};
  Real deflect = 30;
  BCClass bc(pde, hinge, deflect);

  bc.dump( 2, output );
  }

#if 0
  {
  typedef PDELinearizedIncompressiblePotential3D PDEClass;
  typedef BCLinearizedIncompressiblePotential3D<BCTypeFarfieldVortex> BCClass;

  Real x0 = -2;
  Real y0 = -3;

  BCClass bc( x0, y0 );
  bc.dump( 2, output );
  }

  {
  typedef PDELinearizedIncompressiblePotential3D PDEClass;
  typedef BCLinearizedIncompressiblePotential3D<BCTypeFunction<SolutionDummy> > BCClass;
  typedef SolutionFunction3D_Const<1> SolutionExact;

  SolutionExact sln( 1 );

  BCClass bc( &sln );
  bc.dump( 2, output );
  }
#endif
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
