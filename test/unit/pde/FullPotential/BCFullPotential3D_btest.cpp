// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCFullPotential3D_btest
//
// test of 3-D Linearized Incompressible Potential BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/FullPotential/BCFullPotential3D.h"
#include "pde/FullPotential/SolutionFunction3D_LIP.h"

using namespace std;
using namespace SANS;


namespace SANS
{

}   // namespace SANS

using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct

template class SANS::BCFullPotential3D<BCTypeInflow, Real>;
template class SANS::BCFullPotential3D<BCTypeOutflow, Real>;
template class SANS::BCFullPotential3D<BCTypeWall, Real>;
//template class SANS::BCFullPotential3D<BCTypeFarfieldVortex>;
//template class SANS::BCFullPotential3D<BCTypeFunction<SolutionDummy> >;


//############################################################################//
BOOST_AUTO_TEST_SUITE( BCFullPotential3D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  {
  typedef BCFullPotential3D<BCTypeInflow, Real> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 2 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( (std::is_same<ArrayQ, DLA::VectorS<2,Real>>::value) );
  BOOST_CHECK( (std::is_same<MatrixQ,DLA::MatrixS<2,2,Real>>::value) );
  }

  {
  typedef BCFullPotential3D<BCTypeOutflow, Real> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 2 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( (std::is_same<ArrayQ,DLA::VectorS<2,Real>>::value) );
  BOOST_CHECK( (std::is_same<MatrixQ,DLA::MatrixS<2,2,Real>>::value) );
  }
#if 0
  {
  typedef BCFullPotential3D<BCTypeFarfieldVortex> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 1 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( ArrayQ::N == 1 );
  BOOST_CHECK( MatrixQ::M == 1 );
  BOOST_CHECK( MatrixQ::N == 1 );
  }

  {
  typedef BCFullPotential3D<BCTypeFunction<SolutionDummy> > BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  BOOST_CHECK( BCClass::D == 3 );
  BOOST_CHECK( BCClass::N == 1 );
  BOOST_CHECK( BCClass::NBC == 1 );
  BOOST_CHECK( (std::is_same<ArrayQ,Real>::value) );
  BOOST_CHECK( (std::is_same<MatrixQ,Real>::value) );
  }
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeInflow_test )
{
  typedef BCFullPotential3D<BCTypeInflow, Real> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real bcdataTrue = 3;

  BCClass bc( bcdataTrue );

  // static tests
  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 2 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  MatrixQ A;
  MatrixQ B;
  ArrayQ bcdata;

  Real x = 0;
  Real y = 0;
  Real z = 0;
  Real nx = 0.8;
  Real ny = 0.6;
  Real nz = 0.7;

  A = 0;
  B = 0;
  bc.coefficients( x, y, z, 0, nx, ny, nz, A, B );
  BOOST_CHECK_CLOSE( 1, A(0,0), tol );
  BOOST_CHECK_CLOSE( 0, B(0,0), tol );

  bcdata = 0;
  bc.data( x, y, z, 0, nx, ny, nz, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata[0], tol );
  BOOST_CHECK_EQUAL( 0, bcdata[1] );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeOutflow_test )
{
  typedef BCFullPotential3D<BCTypeOutflow, Real> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  BCClass bc;

  // static tests
  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 2 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  MatrixQ A;
  MatrixQ B;
  ArrayQ bcdata;

  Real x = 0;
  Real y = 0;
  Real z = 0;
  Real nx = 0.8;
  Real ny = 0.6;
  Real nz = 0.7;

  A = 0;
  B = 0;
  bc.coefficients( x, y, z, 0, nx, ny, nz, A, B );
  BOOST_CHECK_CLOSE( 0, A(0,0), tol );
  BOOST_CHECK_CLOSE( 1, B(0,0), tol );

  bcdata = 0;
  bc.data( x, y, z, 0, nx, ny, nz, bcdata );
  BOOST_CHECK_CLOSE( 0, bcdata[0], tol );
  BOOST_CHECK_EQUAL( 0, bcdata[1] );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeWall_test )
{
  typedef BCFullPotential3D<BCTypeWall, Real> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real u =  0.95;
  Real v = -0.21;
  Real w =  1.55;

  Real gamma = 1.4;
  Real Minf = 0.5;
  Real rhoinf = 1.2;
  Real pinf = 1.8;
  Real critVelFrac = 0.9;

  PDEFullPotential3D<Real> pde( u, v, w, gamma, Minf, rhoinf, pinf, critVelFrac );

  BCClass bc( pde );

  // static tests
  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 2 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  MatrixQ A;
  MatrixQ B;
  ArrayQ bcdata;
  DLA::VectorD<Real> globalVar(2);

  Real x = 0;
  Real y = 0;
  Real z = 0;
  Real nx = 0.8;
  Real ny = 0.6;
  Real nz = 0.7;

  A = 0;
  B = 0;
  bc.coefficients( x, y, z, 0, nx, ny, nz, A, B );
  BOOST_CHECK_CLOSE( 0, A(0,0), tol );
  BOOST_CHECK_CLOSE( 1, B(0,0), tol );

  globalVar = 0;
  bcdata = 0;
  bc.data( x, y, z, 0, nx, ny, nz, bcdata );
  BOOST_CHECK_CLOSE( -(nx*u + ny*v + nz*w), bcdata[0], tol );
  BOOST_CHECK_EQUAL( 0, bcdata[1] );
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFarfieldVortex_test )
{
  typedef BCFullPotential3D<BCTypeFarfieldVortex> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real x0 = -2;
  Real y0 = -3;

  BCClass bc( x0, y0 );

  // static tests
  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  MatrixQ A;
  MatrixQ B;
  ArrayQ bcdata;
  SANS::DLA::VectorD<Real> globalVar(1);

  Real x = 0;
  Real y = 0;
  Real nx = 0.8;
  Real ny = 0.6;
  Real circ = 7;

  A = 0;
  B = 0;
  bc.coefficients( x, y, z, 0, nx, ny, nz, A, B );
  BOOST_CHECK_CLOSE( 1, A, tol );
  BOOST_CHECK_CLOSE( 0, B, tol );

  Real bcdataTrue = -circ * atan(1.5)/(2*PI);
  globalVar[0] = circ;
  bcdata = 0;
  bc.data( x, y, z, 0, nx, ny, nz, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata, tol );
}
#endif
#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFunction_test )
{
  typedef BCFullPotential3D<BCTypeFunction<SolutionDummy> > BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  SolutionFunction3D_SineSine<1> slnExact;

  BCClass bc( &slnExact );

  // static tests
  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  MatrixQ A;
  MatrixQ B;
  ArrayQ bcdata;
  SANS::DLA::VectorD<Real> globalVar(1);

  Real x = 0.75;
  Real y = 0.25;
  Real nx = 0.8;
  Real ny = 0.6;
  Real circ = 7;

  A = 0;
  B = 0;
  bc.coefficients( x, y, z, 0, nx, ny, nz, A, B );
  BOOST_CHECK_CLOSE( 1, A, tol );
  BOOST_CHECK_CLOSE( 0, B, tol );

  globalVar[0] = circ;
  bcdata = 0;
  bc.data( x, y, z, 0, nx, ny, nz, bcdata );
  BOOST_CHECK_CLOSE( -1, bcdata, tol );
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/BCFullPotential3D_pattern.txt", true );

  {
  typedef BCFullPotential3D<BCTypeInflow, Real> BCClass;

  Real bcdata = 3;

  BCClass bc( bcdata );
  bc.dump( 2, output );
  }

  {
  typedef BCFullPotential3D<BCTypeOutflow, Real> BCClass;

  BCClass bc;
  bc.dump( 2, output );
  }

  {
  typedef BCFullPotential3D<BCTypeWall, Real> BCClass;

  Real u =  0.95;
  Real v = -0.21;
  Real w =  1.55;

  Real gamma = 1.4;
  Real Minf = 0.5;
  Real rhoinf = 1.2;
  Real pinf = 1.8;
  Real critVelFrac = 0.9;

  PDEFullPotential3D<Real> pde( u, v, w, gamma, Minf, rhoinf, pinf, critVelFrac );

  BCClass bc( pde );
  bc.dump( 2, output );
  }
#if 0
  {
  typedef PDEFullPotential3D PDEClass;
  typedef BCFullPotential3D<BCTypeFarfieldVortex> BCClass;

  Real x0 = -2;
  Real y0 = -3;

  BCClass bc( x0, y0 );
  bc.dump( 2, output );
  }

  {
  typedef PDEFullPotential3D PDEClass;
  typedef BCFullPotential3D<BCTypeFunction<SolutionDummy> > BCClass;
  typedef SolutionFunction3D_Const<1> SolutionExact;

  SolutionExact sln( 1 );

  BCClass bc( &sln );
  bc.dump( 2, output );
  }
#endif
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
