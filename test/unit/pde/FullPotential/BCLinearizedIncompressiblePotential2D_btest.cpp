// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCLinearizedIncompressiblePotential2D_btest
//
// test of 2-D Linearized Incompressible Potential BC classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/FullPotential/BCLinearizedIncompressiblePotential2D.h"
#include "pde/FullPotential/SolutionFunction2D_LIP.h"

using namespace std;
using namespace SANS;


namespace SANS
{

//----------------------------------------------------------------------------//
// dummy BC for DeveloperException tests

class BCTypeDummy;

template <>
class BC<PDELinearizedIncompressiblePotential2D, BCTypeDummy> :
    public BCBase
{
public:
  BC() {}
  virtual ~BC() {}

  virtual const std::type_info& derivedTypeID() const { return typeid(*this); }

  virtual void dump( int indentSize, std::ostream& out = std::cout ) const {}
};

}   // namespace SANS


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( BCLinearizedIncompressiblePotential2D_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  {
  typedef BC<PDELinearizedIncompressiblePotential2D, BCTypeDirichlet> BCClass;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 1 );
  BOOST_CHECK( BCClass::NBC == 1 );
  }

  {
  typedef BC<PDELinearizedIncompressiblePotential2D, BCTypeNeumann> BCClass;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 1 );
  BOOST_CHECK( BCClass::NBC == 1 );
  }

  {
  typedef BC<PDELinearizedIncompressiblePotential2D, BCTypeFarfieldVortex> BCClass;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 1 );
  BOOST_CHECK( BCClass::NBC == 1 );
  }

  {
  typedef BC<PDELinearizedIncompressiblePotential2D, BCTypeFunction<SolutionDummy> > BCClass;

  BOOST_CHECK( BCClass::D == 2 );
  BOOST_CHECK( BCClass::N == 1 );
  BOOST_CHECK( BCClass::NBC == 1 );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeDirichlet_test )
{
  typedef BC<PDELinearizedIncompressiblePotential2D, BCTypeDirichlet> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real bcdataTrue = 3;

  BCClass bc( bcdataTrue );

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  MatrixQ A;
  MatrixQ B;
  ArrayQ bcdata;
  SANS::DLA::VectorD<Real> globalVar(1);

  Real x = 0;
  Real y = 0;
  Real nx = 0.8;
  Real ny = 0.6;

  A = 0;
  B = 0;
  bc.coefficients( x, y, 0, nx, ny, A, B );
  BOOST_CHECK_CLOSE( 1, A, tol );
  BOOST_CHECK_CLOSE( 0, B, tol );

  globalVar[0] = 0;
  bcdata = 0;
  bc.data( x, y, 0, nx, ny, globalVar, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata, tol );

  BOOST_CHECK( bc.derivedTypeID() == typeid(BCClass) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeNeumann_test )
{
  typedef BC<PDELinearizedIncompressiblePotential2D, BCTypeNeumann> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real bcdataTrue = 3;

  BCClass bc( bcdataTrue );

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  MatrixQ A;
  MatrixQ B;
  ArrayQ bcdata;
  SANS::DLA::VectorD<Real> globalVar(1);

  Real x = 0;
  Real y = 0;
  Real nx = 0.8;
  Real ny = 0.6;

  A = 0;
  B = 0;
  bc.coefficients( x, y, 0, nx, ny, A, B );
  BOOST_CHECK_CLOSE( 0, A, tol );
  BOOST_CHECK_CLOSE( 1, B, tol );

  globalVar[0] = 0;
  bcdata = 0;
  bc.data( x, y, 0, nx, ny, globalVar, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata, tol );

  BOOST_CHECK( bc.derivedTypeID() == typeid(BCClass) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeWall_test )
{
  typedef BC<PDELinearizedIncompressiblePotential2D, BCTypeWall> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real u =  0.95;
  Real v = -0.21;

  BCClass bc( u, v );

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  MatrixQ A;
  MatrixQ B;
  ArrayQ bcdata;
  SANS::DLA::VectorD<Real> globalVar(1);

  Real x = 0;
  Real y = 0;
  Real nx = 0.8;
  Real ny = 0.6;

  A = 0;
  B = 0;
  bc.coefficients( x, y, 0, nx, ny, A, B );
  BOOST_CHECK_CLOSE( 0, A, tol );
  BOOST_CHECK_CLOSE( 1, B, tol );

  globalVar[0] = 0;
  bcdata = 0;
  bc.data( x, y, 0, nx, ny, globalVar, bcdata );
  BOOST_CHECK_CLOSE( -(nx*u + ny*v), bcdata, tol );

  BOOST_CHECK( bc.derivedTypeID() == typeid(BCClass) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFarfieldVortex_test )
{
  typedef BC<PDELinearizedIncompressiblePotential2D, BCTypeFarfieldVortex> BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  Real x0 = -2;
  Real y0 = -3;

  BCClass bc( x0, y0 );

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  MatrixQ A;
  MatrixQ B;
  ArrayQ bcdata;
  SANS::DLA::VectorD<Real> globalVar(1);

  Real x = 0;
  Real y = 0;
  Real nx = 0.8;
  Real ny = 0.6;
  Real circ = 7;

  A = 0;
  B = 0;
  bc.coefficients( x, y, 0, nx, ny, A, B );
  BOOST_CHECK_CLOSE( 1, A, tol );
  BOOST_CHECK_CLOSE( 0, B, tol );

  Real bcdataTrue = -circ * atan(1.5)/(2*PI);
  globalVar[0] = circ;
  bcdata = 0;
  bc.data( x, y, 0, nx, ny, globalVar, bcdata );
  BOOST_CHECK_CLOSE( bcdataTrue, bcdata, tol );

  BOOST_CHECK( bc.derivedTypeID() == typeid(BCClass) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCTypeFunction_test )
{
  typedef BC<PDELinearizedIncompressiblePotential2D, BCTypeFunction<SolutionDummy> > BCClass;
  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;

  const Real tol = 1.e-13;

  SolutionFunction2D_SineSine<1> slnExact;

  BCClass bc( &slnExact );

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // function tests

  MatrixQ A;
  MatrixQ B;
  ArrayQ bcdata;
  SANS::DLA::VectorD<Real> globalVar(1);

  Real x = 0.75;
  Real y = 0.25;
  Real nx = 0.8;
  Real ny = 0.6;
  Real circ = 7;

  A = 0;
  B = 0;
  bc.coefficients( x, y, 0, nx, ny, A, B );
  BOOST_CHECK_CLOSE( 1, A, tol );
  BOOST_CHECK_CLOSE( 0, B, tol );

  globalVar[0] = circ;
  bcdata = 0;
  bc.data( x, y, 0, nx, ny, globalVar, bcdata );
  BOOST_CHECK_CLOSE( -1, bcdata, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/pde/BCLinearizedIncompressiblePotential2D_pattern.txt", true );

  {
  typedef PDELinearizedIncompressiblePotential2D PDEClass;
  typedef BC<PDEClass, BCTypeDirichlet> BCClass;

  Real bcdata = 3;

  BCClass bc( bcdata );
  bc.dump( 2, output );
  }

  {
  typedef PDELinearizedIncompressiblePotential2D PDEClass;
  typedef BC<PDEClass, BCTypeNeumann> BCClass;

  Real bcdata = 3;

  BCClass bc( bcdata );
  bc.dump( 2, output );
  }

  {
  typedef PDELinearizedIncompressiblePotential2D PDEClass;
  typedef BC<PDEClass, BCTypeWall> BCClass;

  Real u =  0.95;
  Real v = -0.21;

  BCClass bc( u, v );
  bc.dump( 2, output );
  }

  {
  typedef PDELinearizedIncompressiblePotential2D PDEClass;
  typedef BC<PDEClass, BCTypeFarfieldVortex> BCClass;

  Real x0 = -2;
  Real y0 = -3;

  BCClass bc( x0, y0 );
  bc.dump( 2, output );
  }

  {
  typedef PDELinearizedIncompressiblePotential2D PDEClass;
  typedef BC<PDEClass, BCTypeFunction<SolutionDummy> > BCClass;
  typedef SolutionFunction2D_Const<1> SolutionExact;

  SolutionExact sln( 1 );

  BCClass bc( &sln );
  bc.dump( 2, output );
  }

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
