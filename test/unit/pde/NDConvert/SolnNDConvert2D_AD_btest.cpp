// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// NDConvertScalarFunction_btest
//

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime2D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( SolnNDConvert2D_test_suite )

static const int D = PhysD2::D;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SineSineSineUnsteady )
{
  typedef ScalarFunction2D_SineSineSineUnsteady::template ArrayQ<Real> ArrayQ;
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_SineSineSineUnsteady> SolutionSpace;
  typedef SolnNDConvertSpaceTime<PhysD2, ScalarFunction2D_SineSineSineUnsteady> SolutionSpaceTime;
  typedef SolutionSpace::VectorX VectorX;
  typedef SolutionSpaceTime::VectorXT VectorXT;

  GlobalTime time(1./3.);
  SolutionSpace fcnSteady;
  SolutionSpace fcnUnsteady(time);
  SolutionSpaceTime fcnSpaceTime;

  Real x = 2./3;
  Real y = 3./4;
  VectorX X = {x, y};
  VectorXT XT = {x, y, time};
  ArrayQ slnSteady = fcnSteady(X);
  ArrayQ slnUnsteady = fcnUnsteady(X);
  ArrayQ slnSpaceTime = fcnSpaceTime(XT);


  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;
  SANS_CHECK_CLOSE( sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*0.)  , slnSteady, small_tol, close_tol );
  SANS_CHECK_CLOSE( sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time), slnUnsteady, small_tol, close_tol );
  SANS_CHECK_CLOSE( sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time), slnSpaceTime, small_tol, close_tol );

  ArrayQ q, qx, qy, qt;
  ArrayQ qxx,
         qyx, qyy;
  DLA::VectorS< D, ArrayQ > gradq;
  DLA::MatrixSymS< D, ArrayQ > hessianq;

  q = 0; gradq = 0; hessianq = 0;
  fcnSteady.secondGradient(X, q, gradq, hessianq);
  SANS_CHECK_CLOSE(          sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*0.), q            , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*cos(2*PI*x)*sin(2*PI*y)*sin(2*PI*0.), gradq[0]     , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*sin(2*PI*x)*cos(2*PI*y)*sin(2*PI*0.), gradq[1]     , small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI*sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*0.), hessianq(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE(  4*PI*PI*cos(2*PI*x)*cos(2*PI*y)*sin(2*PI*0.), hessianq(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI*sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*0.), hessianq(1,1), small_tol, close_tol );

  q = 0; qx = 0; qt = 0; qxx = 0; qyx = 0; qyy = 0;
  fcnSteady.secondGradient(x, y, time, q, qx, qy, qt, qxx, qyx, qyy);
  SANS_CHECK_CLOSE(          sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*0.), q  , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*cos(2*PI*x)*sin(2*PI*y)*sin(2*PI*0.), qx , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*sin(2*PI*x)*cos(2*PI*y)*sin(2*PI*0.), qy , small_tol, close_tol );
  SANS_CHECK_CLOSE(                                             0, qt , small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI*sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*0.), qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE(  4*PI*PI*cos(2*PI*x)*cos(2*PI*y)*sin(2*PI*0.), qyx, small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI*sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*0.), qyy, small_tol, close_tol );


  q = 0; gradq = 0; hessianq = 0;
  fcnUnsteady.secondGradient(X, q, gradq, hessianq);
  SANS_CHECK_CLOSE(          sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time), q            , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*cos(2*PI*x)*sin(2*PI*y)*sin(2*PI*time), gradq[0]     , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*sin(2*PI*x)*cos(2*PI*y)*sin(2*PI*time), gradq[1]     , small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI*sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time), hessianq(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE(  4*PI*PI*cos(2*PI*x)*cos(2*PI*y)*sin(2*PI*time), hessianq(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI*sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time), hessianq(1,1), small_tol, close_tol );

  q = 0; qx = 0; qt = 0; qxx = 0; qyx = 0; qyy = 0;
  fcnUnsteady.secondGradient(x, y, time, q, qx, qy, qt, qxx, qyx, qyy);
  SANS_CHECK_CLOSE(          sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time), q  , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*cos(2*PI*x)*sin(2*PI*y)*sin(2*PI*time), qx , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*sin(2*PI*x)*cos(2*PI*y)*sin(2*PI*time), qy , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*sin(2*PI*x)*sin(2*PI*y)*cos(2*PI*time), qt , small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI*sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time), qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE(  4*PI*PI*cos(2*PI*x)*cos(2*PI*y)*sin(2*PI*time), qyx, small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI*sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time), qyy, small_tol, close_tol );


  DLA::VectorS< D+1, ArrayQ > gradqST;
  DLA::MatrixSymS< D+1, ArrayQ > hessianqST;

  q = 0; gradqST = 0; hessianqST = 0;
  fcnSpaceTime.secondGradient(XT, q, gradqST, hessianqST);
  SANS_CHECK_CLOSE(          sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time), q              , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*cos(2*PI*x)*sin(2*PI*y)*sin(2*PI*time), gradqST[0]     , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*sin(2*PI*x)*cos(2*PI*y)*sin(2*PI*time), gradqST[1]     , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*sin(2*PI*x)*sin(2*PI*y)*cos(2*PI*time), gradqST[2]     , small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI*sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time), hessianqST(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE(  4*PI*PI*cos(2*PI*x)*cos(2*PI*y)*sin(2*PI*time), hessianqST(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI*sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time), hessianqST(1,1), small_tol, close_tol );

  q = 0; qx = 0; qt = 0; qxx = 0; qyx = 0; qyy = 0;
  fcnSpaceTime.secondGradient(x, y, time, q, qx, qy, qt, qxx, qyx, qyy);
  SANS_CHECK_CLOSE(          sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time), q  , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*cos(2*PI*x)*sin(2*PI*y)*sin(2*PI*time), qx , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*sin(2*PI*x)*cos(2*PI*y)*sin(2*PI*time), qy , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*sin(2*PI*x)*sin(2*PI*y)*cos(2*PI*time), qt , small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI*sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time), qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE(  4*PI*PI*cos(2*PI*x)*cos(2*PI*y)*sin(2*PI*time), qyx, small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI*sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time), qyy, small_tol, close_tol );


  // Test changing the time for unsteady
  time *= 2.5;

  q = 0; gradq = 0; hessianq = 0;
  fcnUnsteady.secondGradient(X, q, gradq, hessianq);
  SANS_CHECK_CLOSE(          sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time), q            , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*cos(2*PI*x)*sin(2*PI*y)*sin(2*PI*time), gradq[0]     , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*sin(2*PI*x)*cos(2*PI*y)*sin(2*PI*time), gradq[1]     , small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI*sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time), hessianq(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE(  4*PI*PI*cos(2*PI*x)*cos(2*PI*y)*sin(2*PI*time), hessianq(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI*sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time), hessianq(1,1), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Monomial )
{
  typedef ScalarFunction2D_Monomial::template ArrayQ<Real> ArrayQ;
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Monomial> SolutionSpace;
  typedef SolnNDConvertSpaceTime<PhysD2, ScalarFunction2D_Monomial> SolutionSpaceTime;
  typedef SolutionSpace::VectorX VectorX;
  typedef SolutionSpaceTime::VectorXT VectorXT;

  int i = 3;
  int j = 4;
  GlobalTime time(1./3.);
  SolutionSpace fcnSteady(i,j);
  SolutionSpace fcnUnsteady(time,i,j);
  SolutionSpaceTime fcnSpaceTime(i,j);

  Real x = 2./3;
  Real y = 3./4;
  VectorX X = {x, y};
  VectorXT XT = {x, y, time};
  ArrayQ slnSteady = fcnSteady(X);
  ArrayQ slnUnsteady = fcnUnsteady(X);
  ArrayQ slnSpaceTime = fcnSpaceTime(XT);


  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;
  SANS_CHECK_CLOSE( pow(x, i)*pow(y, j), slnSteady, small_tol, close_tol );
  SANS_CHECK_CLOSE( pow(x, i)*pow(y, j), slnUnsteady, small_tol, close_tol );
  SANS_CHECK_CLOSE( pow(x, i)*pow(y, j), slnSpaceTime, small_tol, close_tol );

  ArrayQ q, qx, qy, qt;
  ArrayQ qxx,
         qyx, qyy;
  DLA::VectorS< D, ArrayQ > gradq;
  DLA::MatrixSymS< D, ArrayQ > hessianq;

  q = 0; gradq = 0; hessianq = 0;
  fcnSteady.secondGradient(X, q, gradq, hessianq);
  SANS_CHECK_CLOSE(         pow(x, i  )*pow(y, j  ), q            , small_tol, close_tol );
  SANS_CHECK_CLOSE(       i*pow(x, i-1)*pow(y, j  ), gradq[0]     , small_tol, close_tol );
  SANS_CHECK_CLOSE(       j*pow(x, i  )*pow(y, j-1), gradq[1]     , small_tol, close_tol );
  SANS_CHECK_CLOSE( (i-1)*i*pow(x, i-2)*pow(y, j  ), hessianq(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE(     i*j*pow(x, i-1)*pow(y, j-1), hessianq(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( (j-1)*j*pow(x, i  )*pow(y, j-2), hessianq(1,1), small_tol, close_tol );

  q = 0; qx = 0; qt = 0; qxx = 0; qyx = 0; qyy = 0;
  fcnSteady.secondGradient(x, y, time, q, qx, qy, qt, qxx, qyx, qyy);
  SANS_CHECK_CLOSE(         pow(x, i  )*pow(y, j  ), q  , small_tol, close_tol );
  SANS_CHECK_CLOSE(       i*pow(x, i-1)*pow(y, j  ), qx , small_tol, close_tol );
  SANS_CHECK_CLOSE(       j*pow(x, i  )*pow(y, j-1), qy , small_tol, close_tol );
  SANS_CHECK_CLOSE(                               0, qt , small_tol, close_tol );
  SANS_CHECK_CLOSE( (i-1)*i*pow(x, i-2)*pow(y, j  ), qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE(     i*j*pow(x, i-1)*pow(y, j-1), qyx, small_tol, close_tol );
  SANS_CHECK_CLOSE( (j-1)*j*pow(x, i  )*pow(y, j-2), qyy, small_tol, close_tol );


  q = 0; gradq = 0; hessianq = 0;
  fcnUnsteady.secondGradient(X, q, gradq, hessianq);
  SANS_CHECK_CLOSE(         pow(x, i  )*pow(y, j  ), q            , small_tol, close_tol );
  SANS_CHECK_CLOSE(       i*pow(x, i-1)*pow(y, j  ), gradq[0]     , small_tol, close_tol );
  SANS_CHECK_CLOSE(       j*pow(x, i  )*pow(y, j-1), gradq[1]     , small_tol, close_tol );
  SANS_CHECK_CLOSE( (i-1)*i*pow(x, i-2)*pow(y, j  ), hessianq(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE(     i*j*pow(x, i-1)*pow(y, j-1), hessianq(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( (j-1)*j*pow(x, i  )*pow(y, j-2), hessianq(1,1), small_tol, close_tol );

  q = 0; qx = 0; qt = 0; qxx = 0; qyx = 0; qyy = 0;
  fcnUnsteady.secondGradient(x, y, time, q, qx, qy, qt, qxx, qyx, qyy);
  SANS_CHECK_CLOSE(         pow(x, i  )*pow(y, j  ), q  , small_tol, close_tol );
  SANS_CHECK_CLOSE(       i*pow(x, i-1)*pow(y, j  ), qx , small_tol, close_tol );
  SANS_CHECK_CLOSE(       j*pow(x, i  )*pow(y, j-1), qy , small_tol, close_tol );
  SANS_CHECK_CLOSE(                               0, qt , small_tol, close_tol );
  SANS_CHECK_CLOSE( (i-1)*i*pow(x, i-2)*pow(y, j  ), qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE(     i*j*pow(x, i-1)*pow(y, j-1), qyx, small_tol, close_tol );
  SANS_CHECK_CLOSE( (j-1)*j*pow(x, i  )*pow(y, j-2), qyy, small_tol, close_tol );


  DLA::VectorS< D+1, ArrayQ > gradqST;
  DLA::MatrixSymS< D+1, ArrayQ > hessianqST;

  q = 0; gradqST = 0; hessianqST = 0;
  fcnSpaceTime.secondGradient(XT, q, gradqST, hessianqST);
  SANS_CHECK_CLOSE(         pow(x, i  )*pow(y, j  ), q              , small_tol, close_tol );
  SANS_CHECK_CLOSE(       i*pow(x, i-1)*pow(y, j  ), gradqST[0]     , small_tol, close_tol );
  SANS_CHECK_CLOSE(       j*pow(x, i  )*pow(y, j-1), gradqST[1]     , small_tol, close_tol );
  SANS_CHECK_CLOSE(                               0, gradqST[2]     , small_tol, close_tol );
  SANS_CHECK_CLOSE( (i-1)*i*pow(x, i-2)*pow(y, j  ), hessianqST(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE(     i*j*pow(x, i-1)*pow(y, j-1), hessianqST(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( (j-1)*j*pow(x, i  )*pow(y, j-2), hessianqST(1,1), small_tol, close_tol );

  q = 0; qx = 0; qt = 0; qxx = 0; qyx = 0; qyy = 0;
  fcnSpaceTime.secondGradient(x, y, time, q, qx, qy, qt, qxx, qyx, qyy);
  SANS_CHECK_CLOSE(         pow(x, i  )*pow(y, j  ), q  , small_tol, close_tol );
  SANS_CHECK_CLOSE(       i*pow(x, i-1)*pow(y, j  ), qx , small_tol, close_tol );
  SANS_CHECK_CLOSE(       j*pow(x, i  )*pow(y, j-1), qy , small_tol, close_tol );
  SANS_CHECK_CLOSE(                               0, qt , small_tol, close_tol );
  SANS_CHECK_CLOSE( (i-1)*i*pow(x, i-2)*pow(y, j  ), qxx, small_tol, close_tol );
  SANS_CHECK_CLOSE(     i*j*pow(x, i-1)*pow(y, j-1), qyx, small_tol, close_tol );
  SANS_CHECK_CLOSE( (j-1)*j*pow(x, i  )*pow(y, j-2), qyy, small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
