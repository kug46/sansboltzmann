// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// NDConvert1D_btest
//
// test of 1-D NDConvert PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/ForcingFunction1D.h"
#include "pde/AdvectionDiffusion/ForcingFunction2D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpaceTime2D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"

#include "pde/ForcingFunction1D_MMS.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}


using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( NDConvert1D_ST_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpaceTime<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion1D::MatrixQ<Real> MatrixQ;
  typedef PDEClass::VectorXT VectorXT;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass2D;
  typedef SolnNDConvertSpaceTime<PhysD1, ScalarFunction1D_SineSineUnsteady> SolutionExact1D;
  typedef ForcingFunction2D_SineSine<PDEAdvectionDiffusion2D> ForcingFunction2D;

  const Real tol = 1.e-13;

  Real u = 1.1;
  Real v = 1.0; //temporal advection speed
  AdvectiveFlux1D_Uniform adv(u);
  AdvectiveFlux2D_Uniform adv2d(u, v);

  Real kxx = 2.123;
  Real kxy = 0.;
  Real kyy = 0.;
  ViscousFlux1D_Uniform visc(kxx);
  ViscousFlux2D_Uniform visc2d(kxx, kxy, kxy, kyy);


  ScalarFunction1D_SineSineUnsteady MMS_soln;
  typedef ForcingFunction1D_MMS<PDEAdvectionDiffusion1D> ForcingType1D;
  std::shared_ptr<ForcingType1D> forcingptr1( new ForcingType1D(MMS_soln) );
  Source1D_None source;

  Source2D_None source2d;
  std::shared_ptr<ForcingFunction2D> forcingptr2( new ForcingFunction2D( u, v, kxx, kxy, kxy, kyy ) );

  // BC

  SolutionExact1D solnExact1D;

  PDEClass pdespacetime(adv, visc, source, forcingptr1);
  PDEClass2D pde2d(adv2d, visc2d, source2d, forcingptr2);

  BOOST_CHECK( pdespacetime.D == 2 );
  BOOST_CHECK( pdespacetime.N == 1 );

  // flux components
  BOOST_CHECK( pdespacetime.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pdespacetime.hasFluxAdvective() == true );
  BOOST_CHECK( pdespacetime.hasFluxViscous() == true );
  BOOST_CHECK( pdespacetime.hasSource() == false );
  BOOST_CHECK( pdespacetime.hasForcingFunction() == true );

  BOOST_CHECK( pdespacetime.needsSolutionGradientforSource() == false );

  // function tests

  Real x, t;
  Real sln, slnx, slnt;

  x = 0; t = 0;
  sln  =  3.263;
  slnx = -0.445;
  slnt =  1.741;

  VectorXT XT = {x,t};

  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = qDataPrim[0];
  ArrayQ q;
  ArrayQ q2d;
  pdespacetime.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  pde2d.setDOFFrom( q2d, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );
  BOOST_CHECK_CLOSE( qTrue, q2d, tol );

  // unsteady flux
  ArrayQ uCons = 0;
  ArrayQ uCons2d = 0;
  pdespacetime.masterState( XT, q, uCons );
  pde2d.masterState( XT, q, uCons2d );
  BOOST_CHECK_CLOSE( uCons2d, uCons, tol );

  // advective flux
  Real fData[1] = {u*sln};
  Real gData[1] = {v*sln};
  ArrayQ fTrue = fData[0];
  ArrayQ gTrue = gData[0];
  DLA::VectorS<2,ArrayQ> F, F2d;

  F = 0; F2d = 0;
  pdespacetime.fluxAdvective( XT, q, F );
  pde2d.fluxAdvective(XT, q, F2d);
  BOOST_CHECK_CLOSE( fTrue, F[0], tol );
  BOOST_CHECK_CLOSE( gTrue, F[1], tol );
  BOOST_CHECK_CLOSE( F[0], F2d[0], tol );
  BOOST_CHECK_CLOSE( F[1], F2d[1], tol );

  // advective flux jacobian

  DLA::VectorS<2,MatrixQ> dFdu = 0;
  DLA::VectorS<2,MatrixQ> dFdu2d = 0;

  pdespacetime.jacobianFluxAdvective( XT, q, dFdu );
  pde2d.jacobianFluxAdvective(XT, q, dFdu2d);
  BOOST_CHECK_CLOSE( dFdu[0], dFdu2d[0], tol );

  // set gradient
  qDataPrim[0] = slnx;
  ArrayQ qxTrue = qDataPrim[0];
  ArrayQ qx;
  pdespacetime.setDOFFrom( qx, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxTrue, qx, tol );
  DLA::VectorS<2, ArrayQ> gradq;
  gradq[0] = qx; gradq[1] = slnt;

  // diffusive flux
  fTrue = -(kxx*slnx);

  F = 0; F2d = 0;
  pdespacetime.fluxViscous( XT, q, gradq, F );
  pde2d.fluxViscous( XT, q, gradq, F2d );
  BOOST_CHECK_CLOSE( fTrue, F[0], tol );
  BOOST_CHECK_CLOSE( F2d[0], F[0], tol );
  BOOST_CHECK_CLOSE( F2d[1], F[1], tol );

  // diffusive flux jacobian
  DLA::MatrixS<2,2,MatrixQ> KMtx = 0;
  DLA::MatrixS<2,2,MatrixQ> KMtx2d = 0;

  KMtx = 0;
  KMtx2d = 0;
  pdespacetime.diffusionViscous( XT, q, gradq, KMtx );
  pde2d.diffusionViscous( XT, q, gradq, KMtx2d );
  BOOST_CHECK_CLOSE( kxx, KMtx(0,0), tol );
  BOOST_CHECK_CLOSE( KMtx(0,0), KMtx2d(0,0), tol );
  BOOST_CHECK_CLOSE( KMtx(1,0), KMtx2d(1,0), tol );
  BOOST_CHECK_CLOSE( KMtx(0,1), KMtx2d(0,1), tol );
  BOOST_CHECK_CLOSE( KMtx(1,1), KMtx2d(1,1), tol );

  // forcing function
  ArrayQ forcing1 = 0;
  ArrayQ forcing2 = 0;
  ArrayQ src = 0;
  pdespacetime.forcingFunction( XT, forcing1 );
  pde2d.forcingFunction(XT, forcing2);
  //BOOST_CHECK_CLOSE( 1, forcing1d(0), tol );
  BOOST_CHECK_CLOSE( forcing1, forcing2, tol );

  // for coverage; functions are empty

  pdespacetime.source( XT, q, gradq, src );
  pdespacetime.sourceTrace( XT, XT, q, gradq, q, gradq, src, src );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxRoe )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpaceTime<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<1,Real> VectorX;
  typedef PDEClass::VectorXT VectorXT;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass2D;
  typedef SolnNDConvertSpaceTime<PhysD1, ScalarFunction1D_SineSineUnsteady> SolutionExact1D;
  typedef ForcingFunction2D_SineSine<PDEAdvectionDiffusion2D> ForcingFunction2D;

  const Real tol = 1.e-13;

  Real u = 1.1;
  Real v = 1.0; //temporal advection speed
  AdvectiveFlux1D_Uniform adv(u);
  AdvectiveFlux2D_Uniform adv2d(u, v);

  Real kxx = 2.123;
  Real kxy = 0.;
  Real kyy = 0.;
  ViscousFlux1D_Uniform visc(kxx);
  ViscousFlux2D_Uniform visc2d(kxx, kxy, kxy, kyy);



  Source1D_None source;

  Source2D_None source2d;


  std::shared_ptr<ForcingFunction2D> forcingptr2d( new ForcingFunction2D( u, v, kxx, kxy, kxy, kyy ) );

  // BC

  SolutionExact1D solnExact1D;

//  PDEClass pdespacetime(adv, visc, solnExact1D);
  PDEClass pdespacetime(adv, visc, source);
  PDEClass2D pde2d(adv2d, visc2d, source2d, forcingptr2d);

  // advective flux function

  Real x, time;
  Real nx, nt;
  Real slnL, slnR;

  x = 0;  // not actually used in functions
  time = 0;
  nx = 0.6;
  nt = 0.8;
  slnL = 3.26; slnR = 1.79;

  VectorX xvec;
  VectorXT XT;
  xvec[0] = x;
  XT[0] = x;
  XT[1] = time;

  VectorX N = {nx};
  VectorXT NT = {nx, nt};

  // set
  Real qLDataPrim[1] = {slnL};
  string qLNamePrim[1] = {"Solution"};
  ArrayQ qLTrue = qLDataPrim[0];
  ArrayQ qL;
  pdespacetime.setDOFFrom( qL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qLTrue, qL, tol );

  Real qRDataPrim[1] = {slnR};
  string qRNamePrim[1] = {"Solution"};
  ArrayQ qRTrue = qRDataPrim[0];
  ArrayQ qR;
  pdespacetime.setDOFFrom( qR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qRTrue, qR, tol );

  // advective normal flux
  Real fnData[1] = {(nx*u)*slnL + (nt*v)*slnL};
  ArrayQ fnTrue = fnData[0];
  ArrayQ fn = 0;
  ArrayQ fn2d = 0;

  pdespacetime.fluxAdvectiveUpwind( XT, qL, qR, NT, fn );
  pde2d.fluxAdvectiveUpwind(XT,qL,qR,NT, fn2d);
  BOOST_CHECK_CLOSE( fnTrue, fn, tol );
  BOOST_CHECK_CLOSE( fn2d, fn, tol );

  // viscous flux function

  Real slnxL;
  Real slnxR;

  slnxL = 1.325;
  slnxR = 0.327;

  // set gradient
  qLDataPrim[0] = slnxL;
  ArrayQ qxLTrue = qLDataPrim[0];
  ArrayQ qxL;
  pdespacetime.setDOFFrom( qxL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxLTrue, qxL, tol );
  DLA::VectorS<1, ArrayQ> gradqL = qxL;
  DLA::VectorS<2, ArrayQ> gradqLT = {qxL, 0};

  qRDataPrim[0] = slnxR;
  ArrayQ qxRTrue = qRDataPrim[0];
  ArrayQ qxR;
  pdespacetime.setDOFFrom( qxR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxRTrue, qxR, tol );
  DLA::VectorS<1, ArrayQ> gradqR = qxR;
  DLA::VectorS<2, ArrayQ> gradqRT = {qxR, 0};

  // viscous normal flux
  fnData[0] = -0.5*((nx*kxx)*(slnxL + slnxR));

  fn = 0;
  fn2d = 0;
  pdespacetime.fluxViscous( XT, XT, qL, gradqLT, qR, gradqRT, NT, fn );
  pde2d.fluxViscous( XT, XT, qL, gradqLT, qR, gradqRT, NT, fn2d );
  BOOST_CHECK_CLOSE( fnData[0], fn, tol );
  BOOST_CHECK_CLOSE( fn2d, fn, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpaceTime<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<1,Real> VectorX;
  typedef PDEClass::VectorXT VectorXT;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass2D;
  typedef SolnNDConvertSpaceTime<PhysD1, ScalarFunction1D_SineSineUnsteady> SolutionExact1D;
  typedef ForcingFunction2D_SineSine<PDEAdvectionDiffusion2D> ForcingFunction2D;

  const Real tol = 1.e-13;

  Real u = 1.1;
  Real v = 1.0; //temporal advection speed
  AdvectiveFlux1D_Uniform adv(u);
  AdvectiveFlux2D_Uniform adv2d(u, v);

  Real kxx = 2.123;
  Real kxy = 0.;
  Real kyy = 0.;
  ViscousFlux1D_Uniform visc(kxx);
  ViscousFlux2D_Uniform visc2d(kxx, kxy, kxy, kyy);


  Source1D_None source;

  Source2D_None source2d;
  std::shared_ptr<ForcingFunction2D> forcingptr2d( new ForcingFunction2D( u, v, kxx, kxy, kxy, kyy ) );

  // BC

  SolutionExact1D solnExact1D;

//  PDEClass pdespacetime(adv, visc, solnExact1D);
  PDEClass pdespacetime(adv, visc, source);
  PDEClass2D pde2d(adv2d, visc2d, source2d, forcingptr2d);

  Real x, time;
  Real dx;
  Real sln;
  Real speed, speedTrue, speed2d;

  x = 0;  // not actually used in functions
  time = 0;
  dx = 1.22;
  sln  =  3.263;

  VectorX xvec, dxvec;
  VectorXT XT, dxtvec;
  xvec[0] = x;
  XT[0] = x;
  XT[1] = time;
  dxvec[0] = dx;
  dxtvec[0] = dx;
  dxtvec[1] = 0;

  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = qDataPrim[0];
  ArrayQ q;
  pdespacetime.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  speedTrue = fabs(dx*u)/sqrt(dx*dx);

  pdespacetime.speedCharacteristic( XT, dxtvec, q, speed );
  pde2d.speedCharacteristic(XT, dxtvec, q, speed2d);
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );
  BOOST_CHECK_CLOSE( speed, speed2d, tol);
}



//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
