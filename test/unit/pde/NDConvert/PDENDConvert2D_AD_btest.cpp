// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// NDConvert2D_btest
//
// test of 2-D NDConvert class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpaceTime2D.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
namespace DLA
{
//template class VectorS<1,Real>;
//template class MatrixS<1,1,Real>;
}
}


using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( NDConvert2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> PDEClassSpace;
  typedef PDENDConvertSpaceTime<PhysD2, PDEClass> PDEClassSpaceTime;
  typedef PDEClassSpace::ArrayQ<Real> ArrayQ;
  typedef PDEClassSpace::MatrixQ<Real> MatrixQ;
  typedef PDEClassSpace::VectorX VectorX;
  typedef PDEClassSpaceTime::VectorXT VectorXT;

  // cppcheck-suppress unreadVariable
  static const int D = PhysD2::D;
  // cppcheck-suppress unreadVariable
  static const int DT = D+1;

  const Real tol = 1.e-13;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.724;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy,
                                 kyx, kyy);

  Real as = 1.1, bs = 2.4, cs = -1.6;
  Source2D_UniformGrad source(as, bs, cs);

  typedef ForcingFunction2D_Const<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType( 1 ) );

  Real time = 1.5;
  PDEClassSpace pdeSpace(adv, visc, source, forcingptr);
  PDEClassSpaceTime pdeSpaceTime(adv, visc, source, forcingptr);

  // function tests

  Real x, y;
  Real sln, slnx, slny, slnt, slnxx, slnyx, slnyy;

  x = 0; y = 0;
  sln  =  3.263;
  slnx = -0.445;
  slny =  1.741;
  slnt =  1.819;

  slnxx = -0.367;
  slnyx =  2.425;
  slnyy = -0.123;

  VectorX X = {x,y};
  VectorXT XT = {x,y,time};

  // set gradient
  ArrayQ qx = slnx;
  ArrayQ qy = slny;
  ArrayQ qt = slnt;

  DLA::VectorS<D, ArrayQ> gradq = {qx, qy};
  DLA::VectorS<DT, ArrayQ> gradqST = {qx, qy, qt};

  DLA::MatrixSymS<D, ArrayQ> hessq = { {slnxx},
                                       {slnyx, slnyy} };

  DLA::MatrixSymS<DT, ArrayQ> hessqST = { {slnxx},
                                          {slnyx, slnyy},
                                          {0,     0,        0} };

  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = qDataPrim[0];
  ArrayQ q;
  pdeSpace.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );


  // unsteady flux
  ArrayQ uCons = 0;
  pdeSpace.masterState( X, q, uCons );
  BOOST_CHECK_CLOSE( qTrue, uCons, tol );


  // advective flux
  ArrayQ fTrue = u*sln;
  ArrayQ gTrue = v*sln;
  ArrayQ tTrue = 1*sln;
  DLA::VectorS<D,ArrayQ> F;
  DLA::VectorS<DT,ArrayQ> FST;

  F = 0;
  pdeSpace.fluxAdvective( X, q, F );
  BOOST_CHECK_CLOSE( fTrue, F[0], tol );
  BOOST_CHECK_CLOSE( gTrue, F[1], tol );
  FST = 0;
  pdeSpaceTime.fluxAdvective( XT, q, FST );
  BOOST_CHECK_CLOSE( fTrue, FST[0], tol );
  BOOST_CHECK_CLOSE( gTrue, FST[1], tol );
  BOOST_CHECK_CLOSE( tTrue, FST[2], tol );


  // advective flux jacobian
  DLA::VectorS<D,MatrixQ> dFdu = 0;
  DLA::VectorS<DT,MatrixQ> dFduST = 0;
  dFdu = 0;
  pdeSpace.jacobianFluxAdvective( X, q, dFdu );
  BOOST_CHECK_CLOSE( u, dFdu[0], tol );
  BOOST_CHECK_CLOSE( v, dFdu[1], tol );
  dFduST = 0;
  pdeSpaceTime.jacobianFluxAdvective( XT, q, dFduST );
  BOOST_CHECK_CLOSE( u, dFduST[0], tol );
  BOOST_CHECK_CLOSE( v, dFduST[1], tol );
  BOOST_CHECK_CLOSE( 1, dFduST[2], tol );


  // strong advective flux
  ArrayQ strongAdvTrue   =        u*slnx + v*slny;
  ArrayQ strongAdvSTTrue = slnt + u*slnx + v*slny;
  ArrayQ strongPDE;

  strongPDE = 0;
  pdeSpace.strongFluxAdvective( X, q, gradq, strongPDE );
  BOOST_CHECK_CLOSE( strongAdvTrue, strongPDE, tol );
  strongPDE = 0;
  pdeSpaceTime.strongFluxAdvective(  XT, q, gradqST, strongPDE );
  BOOST_CHECK_CLOSE( strongAdvSTTrue, strongPDE, tol );


  // diffusive flux
  fTrue = -(kxx*slnx + kxy*slny);
  gTrue = -(kyx*slnx + kyy*slny);

  F=0;
  pdeSpace.fluxViscous( X, q, gradq, F );
  BOOST_CHECK_CLOSE( fTrue, F[0], tol );
  BOOST_CHECK_CLOSE( gTrue, F[1], tol );
  FST=0;
  pdeSpaceTime.fluxViscous( XT, q, gradqST, FST );
  BOOST_CHECK_CLOSE( fTrue, FST[0], tol );
  BOOST_CHECK_CLOSE( gTrue, FST[1], tol );
  BOOST_CHECK_SMALL(        FST[2], tol );


  // viscous diffusion matrix: d(Fv)/d(UX)
  DLA::MatrixS<D,D,MatrixQ> KMtx = 0;
  DLA::MatrixS<DT,DT,MatrixQ> KMtxST = 0;

  pdeSpace.diffusionViscous( X, q, gradq, KMtx );
  BOOST_CHECK_CLOSE( kxx, KMtx(0,0), tol );
  BOOST_CHECK_CLOSE( kxy, KMtx(0,1), tol );
  BOOST_CHECK_CLOSE( kyx, KMtx(1,0), tol );
  BOOST_CHECK_CLOSE( kyy, KMtx(1,1), tol );

  KMtxST = 0;
  pdeSpaceTime.diffusionViscous( XT, q, gradqST, KMtxST );
  BOOST_CHECK_CLOSE( kxx, KMtxST(0,0), tol );
  BOOST_CHECK_CLOSE( kxy, KMtxST(0,1), tol );
  BOOST_CHECK_SMALL(      KMtxST(0,2), tol );
  BOOST_CHECK_CLOSE( kyx, KMtxST(1,0), tol );
  BOOST_CHECK_CLOSE( kyy, KMtxST(1,1), tol );
  BOOST_CHECK_SMALL(      KMtxST(1,2), tol );
  BOOST_CHECK_SMALL(      KMtxST(2,1), tol );
  BOOST_CHECK_SMALL(      KMtxST(2,2), tol );
  BOOST_CHECK_SMALL(      KMtxST(2,2), tol );


  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  DLA::VectorS<D,MatrixQ> a = 0;
  DLA::VectorS<DT,MatrixQ> aST = 0;

  pdeSpace.jacobianFluxViscous( X, q, gradq, a );
  BOOST_CHECK_SMALL( a[0], tol );
  BOOST_CHECK_SMALL( a[1], tol );

  aST = 0;
  pdeSpaceTime.jacobianFluxViscous( XT, q, gradqST, aST );
  BOOST_CHECK_SMALL( aST[0], tol );
  BOOST_CHECK_SMALL( aST[1], tol );
  BOOST_CHECK_SMALL( aST[2], tol );


  // strong form viscous fluxes: div.(Fv)
  ArrayQ strongFvTrue = -(kxx*slnxx + kxy*slnyx) - (kyx*slnyx + kyy*slnyy) ;

  strongPDE = 0;
  pdeSpace.strongFluxViscous( X, q, gradq, hessq, strongPDE );
  BOOST_CHECK_CLOSE( strongFvTrue, strongPDE, tol );
  strongPDE = 0;
  pdeSpaceTime.strongFluxViscous(  XT, q, gradqST, hessqST, strongPDE );
  BOOST_CHECK_CLOSE( strongFvTrue, strongPDE, tol );


  // source
  ArrayQ src, srcTrue;

  ArrayQ qp = 0;
  DLA::VectorS<D, ArrayQ> gradqp = 0;
  DLA::VectorS<DT, ArrayQ> gradqpST = 0;

  srcTrue = as*q + bs*qx + cs*qy;

  src = 0;
  pdeSpace.source( X, q, gradq, src );
  BOOST_CHECK_CLOSE( srcTrue, src, tol );

  src = 0;
  pdeSpace.source( X, q, qp, gradq, gradqp, src );
  BOOST_CHECK_CLOSE( srcTrue, src, tol );

  src = 0;
  pdeSpaceTime.source( XT, q, gradqST, src );
  BOOST_CHECK_CLOSE( srcTrue, src, tol );

  src = 0;
  pdeSpaceTime.source( XT, q, qp, gradqST, gradqpST, src );
  BOOST_CHECK_CLOSE( srcTrue, src, tol );

  // forcing function
  ArrayQ force = 0;
  pdeSpace.forcingFunction( X, force );
  BOOST_CHECK_CLOSE( 1, force, tol );
  force = 0;
  pdeSpaceTime.forcingFunction( XT, force );
  BOOST_CHECK_CLOSE( 1, force, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxRoe )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> PDEClassSpace;
  typedef PDENDConvertSpaceTime<PhysD2, PDEClass> PDEClassSpaceTime;
  typedef PDEClassSpace::ArrayQ<Real> ArrayQ;
  typedef PDEClassSpace::MatrixQ<Real> MatrixQ;
  typedef PDEClassSpace::VectorX VectorX;
  typedef PDEClassSpaceTime::VectorXT VectorXT;

  const Real tol = 1.e-13;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.724;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy,
                                 kyx, kyy);

  Real as = 1.1, bs = 2.4, cs = -1.6;
  Source2D_UniformGrad source(as, bs, cs);


  typedef ForcingFunction2D_Const<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType( 1 ) );

  Real time = 0;
  PDEClassSpace pdeSpace(adv, visc, source, forcingptr);
  PDEClassSpaceTime pdeSpaceTime(adv, visc, source, forcingptr);

  // advective flux function

  Real x, y;
  Real nx, ny, nt;
  Real slnL, slnR;

  x = 0; y = 0;   // not actually used in functions
  nx = 1.22; ny = -0.432; nt = 1.2;
  slnL = 3.26; slnR = 1.79;

  VectorX X = {x, y};
  VectorX N = {nx, ny};
  VectorXT XT = {x, y, time};
  VectorXT NT = {nx, ny, nt};

  // set
  Real qLDataPrim[1] = {slnL};
  string qLNamePrim[1] = {"Solution"};
  ArrayQ qLTrue = qLDataPrim[0];
  ArrayQ qL;
  pdeSpace.setDOFFrom( qL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qLTrue, qL, tol );

  Real qRDataPrim[1] = {slnR};
  string qRNamePrim[1] = {"Solution"};
  ArrayQ qRTrue = qRDataPrim[0];
  ArrayQ qR;
  pdeSpace.setDOFFrom( qR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qRTrue, qR, tol );

  // advective normal flux
  ArrayQ fnTrue = (nx*u + ny*v)*slnL;
  ArrayQ fnSTTrue = (nt*1 + nx*u + ny*v)*slnL;
  ArrayQ fn;

  fn = 0;
  pdeSpace.fluxAdvectiveUpwind( X, qL, qR, N, fn );
  BOOST_CHECK_CLOSE( fnTrue, fn, tol );
  fn = 0;
  pdeSpaceTime.fluxAdvectiveUpwind( XT, qL, qR, NT, fn );
  BOOST_CHECK_CLOSE( fnSTTrue, fn, tol );


  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  MatrixQ aTrue = fabs(nx*u + ny*v);
  MatrixQ aSTTrue = fabs(nt*1 + nx*u + ny*v);
  MatrixQ a;

  a = 0;
  pdeSpace.jacobianFluxAdvectiveAbsoluteValue( X, qL, N, a );
  BOOST_CHECK_CLOSE( aTrue, a, tol );
  a = 0;
  pdeSpaceTime.jacobianFluxAdvectiveAbsoluteValue( XT, qL, NT, a );
  BOOST_CHECK_CLOSE( aSTTrue, a, tol );


  // viscous flux function

  Real slnxL, slnyL;
  Real slnxR, slnyR;

  slnxL = 1.325;  slnyL = -0.457;
  slnxR = 0.327;  slnyR =  3.421;

  // set gradient
  qLDataPrim[0] = slnxL;
  ArrayQ qxLTrue = qLDataPrim[0];
  ArrayQ qxL;
  pdeSpace.setDOFFrom( qxL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxLTrue, qxL, tol );

  qLDataPrim[0] = slnyL;
  ArrayQ qyLTrue = qLDataPrim[0];
  ArrayQ qyL;
  pdeSpace.setDOFFrom( qyL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyLTrue, qyL, tol );

  DLA::VectorS<2, ArrayQ> gradqL = {qxL, qyL};
  DLA::VectorS<3, ArrayQ> gradqLST = {qxL, qyL, 0};

  qRDataPrim[0] = slnxR;
  ArrayQ qxRTrue = qRDataPrim[0];
  ArrayQ qxR;
  pdeSpace.setDOFFrom( qxR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxRTrue, qxR, tol );

  qRDataPrim[0] = slnyR;
  ArrayQ qyRTrue = qRDataPrim[0];
  ArrayQ qyR;
  pdeSpace.setDOFFrom( qyR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qyRTrue, qyR, tol );

  DLA::VectorS<2, ArrayQ> gradqR = {qxR, qyR};
  DLA::VectorS<3, ArrayQ> gradqRST = {qxR, qyR, 0};


  // viscous normal flux
  ArrayQ Fvn = -0.5*((nx*kxx + kyx*ny)*(slnxL + slnxR) + (nx*kxy + ny*kyy)*(slnyL + slnyR));

  fn = 0;
  pdeSpace.fluxViscous( X, X, qL, gradqL, qR, gradqR, N, fn );
  BOOST_CHECK_CLOSE( Fvn, fn, tol );
  fn = 0;
  pdeSpaceTime.fluxViscous( XT, XT, qL, gradqLST, qR, gradqRST, NT, fn );
  BOOST_CHECK_CLOSE( Fvn, fn, tol );


  // source
  ArrayQ sourceL = 0, sourceR = 0;
  ArrayQ sourceTrueL, sourceTrueR;

  sourceTrueL = as*qL + bs*qxL + cs*qyL;
  sourceTrueR = as*qR + bs*qxR + cs*qyR;

  pdeSpace.sourceTrace( X, X, qL, gradqL, qR, gradqR, sourceL, sourceR );
  BOOST_CHECK_CLOSE( sourceTrueL, sourceL, tol );
  BOOST_CHECK_CLOSE( sourceTrueR, sourceR, tol );

  sourceL = 0, sourceR = 0;
  pdeSpaceTime.sourceTrace( XT, XT, qL, gradqLST, qR, gradqRST, sourceL, sourceR );
  BOOST_CHECK_CLOSE( sourceTrueL, sourceL, tol );
  BOOST_CHECK_CLOSE( sourceTrueR, sourceR, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> PDEClassSpace;
  typedef PDENDConvertSpaceTime<PhysD2, PDEClass> PDEClassSpaceTime;
  typedef PDEClassSpace::ArrayQ<Real> ArrayQ;
  typedef PDEClassSpace::VectorX VectorX;
  typedef PDEClassSpaceTime::VectorXT VectorXT;

  const Real tol = 1.e-13;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Real as = 1.1, bs = 2.4, cs = -1.6;
  Source2D_UniformGrad source(as, bs, cs);


  typedef ForcingFunction2D_Const<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType( 1 ) );

  Real time = 0;
  PDEClassSpace pdeSpace(adv, visc, source, forcingptr);
  PDEClassSpaceTime pdeSpaceTime(adv, visc, source, forcingptr);

  Real x, y;
  Real dx, dy;
  Real sln;
  Real speed, speedTrue;

  x = 0; y = 0;   // not actually used in functions
  dx = 1.22; dy = -0.432;
  sln  =  3.263;
  VectorX X, dX;
  X[0] = x; X[1] = y;
  dX[0] = dx; dX[1] = dy;

  VectorXT XT, dXT;
  XT[0] = x; XT[1] = y; XT[2] = time;
  dXT[0] = dx; dXT[1]= dy; dXT[2] = 0;

  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = qDataPrim[0];
  ArrayQ q;
  pdeSpace.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  speedTrue = fabs(dx*u + dy*v)/sqrt(dx*dx + dy*dy);

  pdeSpace.speedCharacteristic( X, dX, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );
  pdeSpaceTime.speedCharacteristic( XT, dXT, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
