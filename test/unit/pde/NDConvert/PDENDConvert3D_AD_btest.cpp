// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// NDConvert3D_btest
//
// test of 3-D NDConvert class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/PDENDConvertSpaceTime3D.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
namespace DLA
{
//template class VectorS<1,Real>;
//template class MatrixS<1,1,Real>;
}
}


using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( NDConvert3D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> PDEClassSpace;
  typedef PDENDConvertSpaceTime<PhysD3, PDEClass> PDEClassSpaceTime;
  typedef PDEClassSpace::ArrayQ<Real> ArrayQ;
  typedef PDEClassSpace::MatrixQ<Real> MatrixQ;
  typedef PDEClassSpace::VectorX VectorX;
  typedef PDEClassSpaceTime::VectorXT VectorXT;

  // cppcheck-suppress unreadVariable
  static const int D = PhysD3::D;
  // cppcheck-suppress unreadVariable
  static const int DT = D+1;

  const Real tol = 1.e-13;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123, kxy = 0.553, kxz = 0.643;
  Real kyx = 0.378, kyy = 1.007, kyz = 0.765;
  Real kzx = 1.642, kzy = 1.299, kzz = 1.234;

  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                              kyx, kyy, kyz,
                              kzx, kzy, kzz );

  Real as = 1.1, bs = 2.4, cs = -1.6, ds = 0.89;
  Source3D_UniformGrad source(as, bs, cs, ds);

  typedef ForcingFunction3D_Const<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType( 1 ) );

  Real time = 1.5;
  PDEClassSpace pdeSpace(adv, visc, source, forcingptr);
  PDEClassSpaceTime pdeSpaceTime(adv, visc, source, forcingptr);

  // function tests

  Real x, y, z;
  Real sln, slnx, slny, slnz, slnt;
  Real slnxx, slnyx, slnyy, slnzx, slnzy, slnzz;

  x = 0; y = 0; z = 0;
  sln  =  3.263;
  slnx = -0.445;
  slny =  1.741;
  slnz =  3.542;
  slnt =  1.819;

  slnxx = -0.367;
  slnyx =  2.425; slnyy = -0.123;
  slnzx =  1.256; slnzy = -2.387;  slnzz = 0.271;

  VectorX X = {x,y,z};
  VectorXT XT = {x,y,z,time};

  // set gradient
  ArrayQ qx = slnx;
  ArrayQ qy = slny;
  ArrayQ qz = slnz;
  ArrayQ qt = slnt;

  DLA::VectorS<D, ArrayQ> gradq = {qx, qy, qz};
  DLA::VectorS<DT, ArrayQ> gradqST = {qx, qy, qz, qt};

  DLA::MatrixSymS<D, ArrayQ> hessq = { {slnxx},
                                       {slnyx, slnyy},
                                       {slnzx, slnzy, slnzz} };

  DLA::MatrixSymS<DT, ArrayQ> hessqST = { {slnxx},
                                          {slnyx, slnyy},
                                          {slnzx, slnzy, slnzz},
                                          {0,     0,     0,     0} };

  // set
  ArrayQ q = sln;
  ArrayQ qTrue = sln;


  // unsteady flux
  ArrayQ uCons = 0;
  pdeSpace.masterState( X, q, uCons );
  BOOST_CHECK_CLOSE( qTrue, uCons, tol );


  // advective flux
  ArrayQ fTrue = u*sln;
  ArrayQ gTrue = v*sln;
  ArrayQ hTrue = w*sln;
  ArrayQ tTrue = 1*sln;
  DLA::VectorS<D,ArrayQ> F;
  DLA::VectorS<DT,ArrayQ> FST;

  F = 0;
  pdeSpace.fluxAdvective( X, q, F );
  BOOST_CHECK_CLOSE( fTrue, F[0], tol );
  BOOST_CHECK_CLOSE( gTrue, F[1], tol );
  BOOST_CHECK_CLOSE( hTrue, F[2], tol );
  FST = 0;
  pdeSpaceTime.fluxAdvective( XT, q, FST );
  BOOST_CHECK_CLOSE( fTrue, FST[0], tol );
  BOOST_CHECK_CLOSE( gTrue, FST[1], tol );
  BOOST_CHECK_CLOSE( hTrue, FST[2], tol );
  BOOST_CHECK_CLOSE( tTrue, FST[3], tol );


  // advective flux jacobian
  DLA::VectorS<D,MatrixQ> dFdu = 0;
  DLA::VectorS<DT,MatrixQ> dFduST = 0;
  dFdu = 0;
  pdeSpace.jacobianFluxAdvective( X, q, dFdu );
  BOOST_CHECK_CLOSE( u, dFdu[0], tol );
  BOOST_CHECK_CLOSE( v, dFdu[1], tol );
  BOOST_CHECK_CLOSE( w, dFdu[2], tol );
  dFduST = 0;
  pdeSpaceTime.jacobianFluxAdvective( XT, q, dFduST );
  BOOST_CHECK_CLOSE( u, dFduST[0], tol );
  BOOST_CHECK_CLOSE( v, dFduST[1], tol );
  BOOST_CHECK_CLOSE( w, dFduST[2], tol );
  BOOST_CHECK_CLOSE( 1, dFduST[3], tol );


  // strong advective flux
  ArrayQ strongAdvTrue   =        u*slnx + v*slny + w*slnz;
  ArrayQ strongAdvSTTrue = slnt + u*slnx + v*slny + w*slnz;
  ArrayQ strongPDE;

  strongPDE = 0;
  pdeSpace.strongFluxAdvective( X, q, gradq, strongPDE );
  BOOST_CHECK_CLOSE( strongAdvTrue, strongPDE, tol );
  strongPDE = 0;
  pdeSpaceTime.strongFluxAdvective(  XT, q, gradqST, strongPDE );
  BOOST_CHECK_CLOSE( strongAdvSTTrue, strongPDE, tol );


  // diffusive flux
  fTrue = -(kxx*slnx + kxy*slny + kxz*slnz);
  gTrue = -(kyx*slnx + kyy*slny + kyz*slnz);
  hTrue = -(kzx*slnx + kzy*slny + kzz*slnz);

  F=0;
  pdeSpace.fluxViscous( X, q, gradq, F );
  BOOST_CHECK_CLOSE( fTrue, F[0], tol );
  BOOST_CHECK_CLOSE( gTrue, F[1], tol );
  BOOST_CHECK_CLOSE( hTrue, F[2], tol );
  FST=0;
  pdeSpaceTime.fluxViscous( XT, q, gradqST, FST );
  BOOST_CHECK_CLOSE( fTrue, FST[0], tol );
  BOOST_CHECK_CLOSE( gTrue, FST[1], tol );
  BOOST_CHECK_CLOSE( hTrue, FST[2], tol );
  BOOST_CHECK_SMALL(        FST[3], tol );


  // viscous diffusion matrix: d(Fv)/d(UX)
  DLA::MatrixS<D,D,MatrixQ> KMtx = 0;
  DLA::MatrixS<DT,DT,MatrixQ> KMtxST = 0;

  pdeSpace.diffusionViscous( X, q, gradq, KMtx );
  BOOST_CHECK_CLOSE( kxx, KMtx(0,0), tol );
  BOOST_CHECK_CLOSE( kxy, KMtx(0,1), tol );
  BOOST_CHECK_CLOSE( kxz, KMtx(0,2), tol );
  BOOST_CHECK_CLOSE( kyx, KMtx(1,0), tol );
  BOOST_CHECK_CLOSE( kyy, KMtx(1,1), tol );
  BOOST_CHECK_CLOSE( kyz, KMtx(1,2), tol );
  BOOST_CHECK_CLOSE( kzx, KMtx(2,0), tol );
  BOOST_CHECK_CLOSE( kzy, KMtx(2,1), tol );
  BOOST_CHECK_CLOSE( kzz, KMtx(2,2), tol );

  KMtxST = 0;
  pdeSpaceTime.diffusionViscous( XT, q, gradqST, KMtxST );
  BOOST_CHECK_CLOSE( kxx, KMtxST(0,0), tol );
  BOOST_CHECK_CLOSE( kxy, KMtxST(0,1), tol );
  BOOST_CHECK_CLOSE( kxz, KMtxST(0,2), tol );
  BOOST_CHECK_SMALL(      KMtxST(0,3), tol );
  BOOST_CHECK_CLOSE( kyx, KMtxST(1,0), tol );
  BOOST_CHECK_CLOSE( kyy, KMtxST(1,1), tol );
  BOOST_CHECK_SMALL(      KMtxST(1,3), tol );
  BOOST_CHECK_CLOSE( kyz, KMtxST(1,2), tol );
  BOOST_CHECK_CLOSE( kzx, KMtxST(2,0), tol );
  BOOST_CHECK_CLOSE( kzy, KMtxST(2,1), tol );
  BOOST_CHECK_CLOSE( kzz, KMtxST(2,2), tol );
  BOOST_CHECK_SMALL(      KMtxST(3,0), tol );
  BOOST_CHECK_SMALL(      KMtxST(3,1), tol );
  BOOST_CHECK_SMALL(      KMtxST(3,2), tol );
  BOOST_CHECK_SMALL(      KMtxST(3,3), tol );


  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  DLA::VectorS<D,MatrixQ> a = 0;
  DLA::VectorS<DT,MatrixQ> aST = 0;

  pdeSpace.jacobianFluxViscous( X, q, gradq, a );
  BOOST_CHECK_SMALL( a[0], tol );
  BOOST_CHECK_SMALL( a[1], tol );
  BOOST_CHECK_SMALL( a[2], tol );

  aST = 0;
  pdeSpaceTime.jacobianFluxViscous( XT, q, gradqST, aST );
  BOOST_CHECK_SMALL( aST[0], tol );
  BOOST_CHECK_SMALL( aST[1], tol );
  BOOST_CHECK_SMALL( aST[2], tol );
  BOOST_CHECK_SMALL( aST[3], tol );


  // strong form viscous fluxes: div.(Fv)
  ArrayQ strongFvTrue = -(kxx*slnxx + kxy*slnyx + kxz*slnzx)
                        -(kyx*slnyx + kyy*slnyy + kyz*slnzy)
                        -(kzx*slnzx + kzy*slnzy + kzz*slnzz);

  strongPDE = 0;
  pdeSpace.strongFluxViscous( X, q, gradq, hessq, strongPDE );
  BOOST_CHECK_CLOSE( strongFvTrue, strongPDE, tol );
  strongPDE = 0;
  pdeSpaceTime.strongFluxViscous(  XT, q, gradqST, hessqST, strongPDE );
  BOOST_CHECK_CLOSE( strongFvTrue, strongPDE, tol );


  // source
  ArrayQ src, srcTrue;

  ArrayQ qp = 0;
  DLA::VectorS<D, ArrayQ> gradqp = 0;
  DLA::VectorS<DT, ArrayQ> gradqpST = 0;

  srcTrue = as*q + bs*qx + cs*qy + ds*qz;

  src = 0;
  pdeSpace.source( X, q, gradq, src );
  BOOST_CHECK_CLOSE( srcTrue, src, tol );

  src = 0;
  pdeSpace.source( X, q, qp, gradq, gradqp, src );
  BOOST_CHECK_CLOSE( srcTrue, src, tol );

  src = 0;
  pdeSpaceTime.source( XT, q, gradqST, src );
  BOOST_CHECK_CLOSE( srcTrue, src, tol );

  src = 0;
  pdeSpaceTime.source( XT, q, qp, gradqST, gradqpST, src );
  BOOST_CHECK_CLOSE( srcTrue, src, tol );

  // forcing function
  ArrayQ force = 0;
  pdeSpace.forcingFunction( X, force );
  BOOST_CHECK_CLOSE( 1, force, tol );
  force = 0;
  pdeSpaceTime.forcingFunction( XT, force );
  BOOST_CHECK_CLOSE( 1, force, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxUpwind )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> PDEClassSpace;
  typedef PDENDConvertSpaceTime<PhysD3, PDEClass> PDEClassSpaceTime;
  typedef PDEClassSpace::ArrayQ<Real> ArrayQ;
  typedef PDEClassSpace::MatrixQ<Real> MatrixQ;
  typedef PDEClassSpace::VectorX VectorX;
  typedef PDEClassSpaceTime::VectorXT VectorXT;

  // cppcheck-suppress unreadVariable
  static const int D = PhysD3::D;
  // cppcheck-suppress unreadVariable
  static const int DT = D+1;

  const Real tol = 1.e-13;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123, kxy = 0.553, kxz = 0.643;
  Real kyx = 0.378, kyy = 1.007, kyz = 0.765;
  Real kzx = 1.642, kzy = 1.299, kzz = 1.234;

  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                              kyx, kyy, kyz,
                              kzx, kzy, kzz );

  Real as = 1.1, bs = 2.4, cs = -1.6, ds = 0.89;
  Source3D_UniformGrad source(as, bs, cs, ds);

  typedef ForcingFunction3D_Const<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType( 1 ) );

  Real time = 1.5;
  PDEClassSpace pdeSpace(adv, visc, source, forcingptr);
  PDEClassSpaceTime pdeSpaceTime(adv, visc, source, forcingptr);

  // advective flux function

  Real x, y, z;
  Real nx, ny, nz, nt;
  Real slnL, slnR;

  x = 0; y = 0, z = 0;   // not actually used in functions
  nx = 1.22; ny = -0.432, nz = 0.234; nt = 1.2;
  slnL = 3.26; slnR = 1.79;

  VectorX X = {x, y, z};
  VectorX N = {nx, ny, nz};
  VectorXT XT = {x, y, z, time};
  VectorXT NT = {nx, ny, nz, nt};

  // set
  ArrayQ qL = slnL;
  ArrayQ qR = slnR;

  // advective normal flux
  ArrayQ fnTrue = (nx*u + ny*v + nz*w)*slnL;
  ArrayQ fnSTTrue = (nt*1 + nx*u + ny*v + nz*w)*slnL;
  ArrayQ fn;

  fn = 0;
  pdeSpace.fluxAdvectiveUpwind( X, qL, qR, N, fn );
  BOOST_CHECK_CLOSE( fnTrue, fn, tol );
  fn = 0;
  pdeSpaceTime.fluxAdvectiveUpwind( XT, qL, qR, NT, fn );
  BOOST_CHECK_CLOSE( fnSTTrue, fn, tol );


  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  MatrixQ aTrue = fabs(nx*u + ny*v + nz*w);
  MatrixQ aSTTrue = fabs(nt*1 + nx*u + ny*v + nz*w);
  MatrixQ a;

  a = 0;
  pdeSpace.jacobianFluxAdvectiveAbsoluteValue( X, qL, N, a );
  BOOST_CHECK_CLOSE( aTrue, a, tol );
  a = 0;
  pdeSpaceTime.jacobianFluxAdvectiveAbsoluteValue( XT, qL, NT, a );
  BOOST_CHECK_CLOSE( aSTTrue, a, tol );


  // viscous flux function

  Real slnxL, slnyL, slnzL;
  Real slnxR, slnyR, slnzR;

  slnxL = 1.325;  slnyL = -0.457;  slnzL = 0.123;
  slnxR = 0.327;  slnyR =  3.421;  slnzR =  4.291;


  // set gradient
  ArrayQ qxL = slnxL;
  ArrayQ qyL = slnyL;
  ArrayQ qzL = slnzL;

  DLA::VectorS<D , ArrayQ> gradqL = {slnxL, slnyL, slnzL};
  DLA::VectorS<DT, ArrayQ> gradqLST = {slnxL, slnyL, slnzL, 0};

  ArrayQ qxR = slnxR;
  ArrayQ qyR = slnyR;
  ArrayQ qzR = slnzR;

  DLA::VectorS<D , ArrayQ> gradqR = {qxR, qyR, qzR};
  DLA::VectorS<DT, ArrayQ> gradqRST = {qxR, qyR, qzR, 0};


  // viscous normal flux
  ArrayQ Fvn = -0.5*((nx*kxx + ny*kyx + nz*kzx)*(slnxL + slnxR)
                   + (nx*kxy + ny*kyy + nz*kzy)*(slnyL + slnyR)
                   + (nx*kxz + ny*kyz + nz*kzz)*(slnzL + slnzR));

  fn = 0;
  pdeSpace.fluxViscous( X, X, qL, gradqL, qR, gradqR, N, fn );
  BOOST_CHECK_CLOSE( Fvn, fn, tol );
  fn = 0;
  pdeSpaceTime.fluxViscous( XT, XT, qL, gradqLST, qR, gradqRST, NT, fn );
  BOOST_CHECK_CLOSE( Fvn, fn, tol );


  // source
  ArrayQ sourceL = 0, sourceR = 0;
  ArrayQ sourceTrueL, sourceTrueR;

  sourceTrueL = as*qL + bs*qxL + cs*qyL + ds*qzL;
  sourceTrueR = as*qR + bs*qxR + cs*qyR + ds*qzR;

  pdeSpace.sourceTrace( X, X, qL, gradqL, qR, gradqR, sourceL, sourceR );
  BOOST_CHECK_CLOSE( sourceTrueL, sourceL, tol );
  BOOST_CHECK_CLOSE( sourceTrueR, sourceR, tol );

  sourceL = 0, sourceR = 0;
  pdeSpaceTime.sourceTrace( XT, XT, qL, gradqLST, qR, gradqRST, sourceL, sourceR );
  BOOST_CHECK_CLOSE( sourceTrueL, sourceL, tol );
  BOOST_CHECK_CLOSE( sourceTrueR, sourceR, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> PDEClassSpace;
  typedef PDENDConvertSpaceTime<PhysD3, PDEClass> PDEClassSpaceTime;
  typedef PDEClassSpace::ArrayQ<Real> ArrayQ;
  typedef PDEClassSpace::VectorX VectorX;
  typedef PDEClassSpaceTime::VectorXT VectorXT;

  const Real tol = 1.e-13;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123, kxy = 0.553, kxz = 0.643;
  Real kyx = 0.378, kyy = 1.007, kyz = 0.765;
  Real kzx = 1.642, kzy = 1.299, kzz = 1.234;

  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                              kyx, kyy, kyz,
                              kzx, kzy, kzz );

  Real as = 1.1, bs = 2.4, cs = -1.6, ds = 0.89;
  Source3D_UniformGrad source(as, bs, cs, ds);

  typedef ForcingFunction3D_Const<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType( 1 ) );

  Real time = 1.5;
  PDEClassSpace pdeSpace(adv, visc, source, forcingptr);
  PDEClassSpaceTime pdeSpaceTime(adv, visc, source, forcingptr);

  Real x, y, z;
  Real dx, dy, dz;
  Real sln;
  Real speed, speedTrue;

  x = 0; y = 0; z = 0;  // not actually used in functions
  dx = 1.22; dy = -0.432; dz = 1.562;
  sln  =  3.263;
  VectorX X, dX;
  X[0] = x; X[1] = y; X[2] = z;
  dX[0] = dx; dX[1] = dy; dX[2] = dz;

  VectorXT XT, dXT;
  XT[0] = x; XT[1] = y; XT[2] = z; XT[3] = time;
  dXT[0] = dx; dXT[1]= dy; dXT[2]= dz; dXT[3] = 0;

  // set
  ArrayQ q = sln;

  speedTrue = fabs(dx*u + dy*v + dz*w)/sqrt(dx*dx + dy*dy + dz*dz);

  pdeSpace.speedCharacteristic( X, dX, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );
  pdeSpaceTime.speedCharacteristic( XT, dXT, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
