// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCNDConvert_btest
//

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"

#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime2D.h"

#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime3D.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{

}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( BCNDConvert_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCNDConvert1D_test )
{
  typedef BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_mitLG> BCClass;
  typedef BCNDConvertSpace<PhysD1, BCClass> BCClassSpace;
  typedef BCNDConvertSpaceTime<PhysD1, BCClass> BCClassSpaceTime;

  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;
  typedef BCClassSpace::VectorX VectorX;
  typedef BCClassSpaceTime::VectorXT VectorXT;

  const Real tol = 1.e-13;
  Real time = 0;

  Real A = 1;
  Real B = 0.2;
  Real bcdata = 3;

  BCClassSpace bcspace(A, B, bcdata );
  BCClassSpaceTime bcspacetime(A, B, bcdata );

  // function tests

  MatrixQ AMtx;
  MatrixQ BMtx;
  ArrayQ bcdataVec;

  Real x = 0;
  Real nx = 1;
  Real ntime = 0;

  VectorX X = {x};
  VectorXT XT = {x, time};
  VectorX N = {nx};
  VectorXT NT = {nx, ntime};

  AMtx = 0;
  BMtx = 0;
  bcspace.coefficients( X, N, AMtx, BMtx );
  BOOST_CHECK_CLOSE( A, AMtx, tol );
  BOOST_CHECK_CLOSE( B, BMtx, tol );

  AMtx = 0;
  BMtx = 0;
  bcspacetime.coefficients( XT, NT, AMtx, BMtx );
  BOOST_CHECK_CLOSE( A, AMtx, tol );
  BOOST_CHECK_CLOSE( B, BMtx, tol );

  bcdataVec = 0;
  bcspace.data( X, N, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );
  bcdataVec = 0;
  bcspacetime.data( XT, NT, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCNDConvert1D_None_test )
{
  typedef BCNone<PhysD1,AdvectionDiffusionTraits<PhysD1>::N> BCClass;
  typedef BCNDConvertSpace<PhysD1, BCClass> BCClassSpace;
  typedef BCNDConvertSpaceTime<PhysD1, BCClass> BCClassSpaceTime;

  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClassSpace::VectorX VectorX;
  typedef BCClassSpaceTime::VectorXT VectorXT;

  Real time = 0;

  BCClassSpace bcspace(true);
  BCClassSpaceTime bcspacetime;

  // function tests

  ArrayQ bcdataVec = 1.2;

  Real x = 0;
  Real nx = 1;
  Real ntime = 0;

  VectorX X = {x};
  VectorXT XT = {x, time};
  VectorX N = {nx};
  VectorXT NT = {nx, ntime};

  BOOST_CHECK_EQUAL( bcspace.isValidState(N,bcdataVec), true );
  BOOST_CHECK_EQUAL( bcspacetime.isValidState(NT,bcdataVec), true );
  BOOST_CHECK_EQUAL( bcspace.useFluxViscous(), true );
  BOOST_CHECK_EQUAL( bcspacetime.useFluxViscous(), false );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCNDConvert2D_test )
{
  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_mitLG> BCClass;
  typedef BCNDConvertSpace<PhysD2, BCClass> BCClassSpace;
  typedef BCNDConvertSpaceTime<PhysD2, BCClass> BCClassSpaceTime;

  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;
  typedef BCClassSpace::VectorX VectorX;
  typedef BCClassSpaceTime::VectorXT VectorXT;

  const Real tol = 1.e-13;
  Real time = 0;

  Real A = 1;
  Real B = 0.2;
  Real bcdata = 3;

  BCClassSpace bcspace(A, B, bcdata );
  BCClassSpaceTime bcspacetime(A, B, bcdata );

  // function tests

  MatrixQ AMtx;
  MatrixQ BMtx;
  ArrayQ bcdataVec;

  Real x = 0;
  Real y = 0;
  Real nx = 0.8;
  Real ny = 0.6;
  Real ntime = 0;

  VectorX X = {x, y};
  VectorXT XT = {x, y, time};
  VectorX N = {nx, ny};
  VectorXT NT = {nx, ny, ntime};

  AMtx = 0;
  BMtx = 0;
  bcspace.coefficients( X, N, AMtx, BMtx );
  BOOST_CHECK_CLOSE( A, AMtx, tol );
  BOOST_CHECK_CLOSE( B, BMtx, tol );

  AMtx = 0;
  BMtx = 0;
  bcspacetime.coefficients( XT, NT, AMtx, BMtx );
  BOOST_CHECK_CLOSE( A, AMtx, tol );
  BOOST_CHECK_CLOSE( B, BMtx, tol );

  bcdataVec = 0;
  bcspace.data( X, N, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );
  bcdataVec = 0;
  bcspacetime.data( XT, NT, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCNDConvert2D_None_test )
{
  typedef BCNone<PhysD2,AdvectionDiffusionTraits<PhysD2>::N> BCClass;
  typedef BCNDConvertSpace<PhysD2, BCClass> BCClassSpace;
  typedef BCNDConvertSpaceTime<PhysD2, BCClass> BCClassSpaceTime;

  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClassSpace::VectorX VectorX;
  typedef BCClassSpaceTime::VectorXT VectorXT;

  Real time = 0;

  BCClassSpace bcspace(true);
  BCClassSpaceTime bcspacetime;

  // function tests

  ArrayQ bcdataVec = 1.2;

  Real x = 0;
  Real y = 0;
  Real nx = 0.8;
  Real ny = 0.6;
  Real ntime = 0;

  VectorX X = {x, y};
  VectorXT XT = {x, y, time};
  VectorX N = {nx, ny};
  VectorXT NT = {nx, ny, ntime};

  BOOST_CHECK_EQUAL( bcspace.isValidState(N,bcdataVec), true );
  BOOST_CHECK_EQUAL( bcspacetime.isValidState(NT,bcdataVec), true );
  BOOST_CHECK_EQUAL( bcspace.useFluxViscous(), true );
  BOOST_CHECK_EQUAL( bcspacetime.useFluxViscous(), false );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCNDConvert3D_test )
{
  typedef BCAdvectionDiffusion<PhysD3, BCTypeLinearRobin_mitLG> BCClass;
  typedef BCNDConvertSpace<PhysD3, BCClass> BCClassSpace;
  typedef BCNDConvertSpaceTime<PhysD3, BCClass> BCClassSpaceTime;

  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClass::template MatrixQ<Real> MatrixQ;
  typedef BCClassSpace::VectorX VectorX;
  typedef BCClassSpaceTime::VectorXT VectorXT;

  const Real tol = 1.e-13;
  Real time = 0;

  Real A = 1;
  Real B = 0.2;
  Real bcdata = 3;

  BCClassSpace bcspace(A, B, bcdata );
  BCClassSpaceTime bcspacetime(A, B, bcdata );

  // function tests

  MatrixQ AMtx;
  MatrixQ BMtx;
  ArrayQ bcdataVec;

  Real x = 0;
  Real y = 0;
  Real z = 0;
  Real nx = 0.2;
  Real ny = 0.6;
  Real nz = sqrt(0.6);
  Real ntime = 0;

  VectorX X = {x, y, z};
  VectorXT XT = {x, y, z, time};
  VectorX N = {nx, ny, nz};
  VectorXT NT = {nx, ny, nz, ntime};

  AMtx = 0;
  BMtx = 0;
  bcspace.coefficients( X, N, AMtx, BMtx );
  BOOST_CHECK_CLOSE( A, AMtx, tol );
  BOOST_CHECK_CLOSE( B, BMtx, tol );

  AMtx = 0;
  BMtx = 0;
  bcspacetime.coefficients( XT, NT, AMtx, BMtx );
  BOOST_CHECK_CLOSE( A, AMtx, tol );
  BOOST_CHECK_CLOSE( B, BMtx, tol );

  bcdataVec = 0;
  bcspace.data( X, N, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );
  bcdataVec = 0;
  bcspacetime.data( XT, NT, bcdataVec );
  BOOST_CHECK_CLOSE( bcdata, bcdataVec, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BCNDConvert3D_None_test )
{
  typedef BCNone<PhysD3,AdvectionDiffusionTraits<PhysD3>::N> BCClass;
  typedef BCNDConvertSpace<PhysD3, BCClass> BCClassSpace;
  typedef BCNDConvertSpaceTime<PhysD3, BCClass> BCClassSpaceTime;

  typedef BCClass::template ArrayQ<Real> ArrayQ;
  typedef BCClassSpace::VectorX VectorX;
  typedef BCClassSpaceTime::VectorXT VectorXT;

  Real time = 0;

  BCClassSpace bcspace(true);
  BCClassSpaceTime bcspacetime;

  // function tests
  ArrayQ bcdataVec = 1.2;

  Real x = 0;
  Real y = 0;
  Real z = 0;
  Real nx = 0.2;
  Real ny = 0.6;
  Real nz = sqrt(0.6);
  Real ntime = 0;

  VectorX X = {x, y, z};
  VectorXT XT = {x, y, z, time};
  VectorX N = {nx, ny, nz};
  VectorXT NT = {nx, ny, nz, ntime};

  BOOST_CHECK_EQUAL( bcspace.isValidState(N,bcdataVec), true );
  BOOST_CHECK_EQUAL( bcspacetime.isValidState(NT,bcdataVec), true );
  BOOST_CHECK_EQUAL( bcspace.useFluxViscous(), true );
  BOOST_CHECK_EQUAL( bcspacetime.useFluxViscous(), false );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
