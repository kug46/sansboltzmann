// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// NDConvertScalarFunction_btest
//

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( SolnNDConvert1D_test_suite )


static const int D = PhysD1::D;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SineSineUnsteady )
{
  typedef ScalarFunction1D_SineSineUnsteady::template ArrayQ<Real> ArrayQ;
  typedef SolnNDConvertSpace<PhysD1, ScalarFunction1D_SineSineUnsteady> SolutionSteady;
  typedef SolnNDConvertSpaceTime<PhysD1, ScalarFunction1D_SineSineUnsteady> SolutionSpaceTime;
  typedef SolutionSteady::VectorX VectorX;
  typedef SolutionSpaceTime::VectorXT VectorXT;

  GlobalTime time(1./3);
  SolutionSteady fcnSteady;
  SolutionSteady fcnUnsteady(time);
  SolutionSpaceTime fcnSpaceTime;

  Real x = 2./3;
  VectorX X = {x};
  VectorXT XT = {x, time};
  ArrayQ slnSteady = fcnSteady(X);
  ArrayQ slnUnsteady = fcnUnsteady(X);
  ArrayQ slnSpaceTime = fcnSpaceTime(XT);


  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;
  SANS_CHECK_CLOSE( sin(2*PI*x)*sin(2*PI*0.)  , slnSteady, small_tol, close_tol );
  SANS_CHECK_CLOSE( sin(2*PI*x)*sin(2*PI*time), slnUnsteady, small_tol, close_tol );
  SANS_CHECK_CLOSE( sin(2*PI*x)*sin(2*PI*time), slnSpaceTime, small_tol, close_tol );

  ArrayQ q, qx, qt, qxx;
  DLA::VectorS< D, ArrayQ > gradq;
  DLA::MatrixSymS< D, ArrayQ > hessianq;

  q = 0; gradq = 0; hessianq = 0;
  fcnSteady.secondGradient(X, q, gradq, hessianq);
  SANS_CHECK_CLOSE(          sin(2*PI*x)*sin(2*PI*0.), q            , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*cos(2*PI*x)*sin(2*PI*0.), gradq[0]     , small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI*sin(2*PI*x)*sin(2*PI*0.), hessianq(0,0), small_tol, close_tol );

  q = 0; qx = 0; qt = 0; qxx = 0;
  fcnSteady.secondGradient(x, time, q, qx, qt, qxx);
  SANS_CHECK_CLOSE(          sin(2*PI*x)*sin(2*PI*0.), q  , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*cos(2*PI*x)*sin(2*PI*0.), qx , small_tol, close_tol );
  SANS_CHECK_CLOSE(                                 0, qt , small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI*sin(2*PI*x)*sin(2*PI*0.), qxx, small_tol, close_tol );


  q = 0; gradq = 0; hessianq = 0;
  fcnUnsteady.secondGradient(X, q, gradq, hessianq);
  SANS_CHECK_CLOSE(          sin(2*PI*x)*sin(2*PI*time), q            , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*cos(2*PI*x)*sin(2*PI*time), gradq[0]     , small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI*sin(2*PI*x)*sin(2*PI*time), hessianq(0,0), small_tol, close_tol );

  q = 0; qx = 0; qt = 0; qxx = 0;
  fcnUnsteady.secondGradient(x, time, q, qx, qt, qxx);
  SANS_CHECK_CLOSE(          sin(2*PI*x)*sin(2*PI*time), q  , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*cos(2*PI*x)*sin(2*PI*time), qx , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*sin(2*PI*x)*cos(2*PI*time), qt , small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI*sin(2*PI*x)*sin(2*PI*time), qxx, small_tol, close_tol );


  DLA::VectorS< D+1, ArrayQ > gradqST;
  DLA::MatrixSymS< D+1, ArrayQ > hessianqST;

  q = 0; gradqST = 0; hessianqST = 0;
  fcnSpaceTime.secondGradient(XT, q, gradqST, hessianqST);
  SANS_CHECK_CLOSE(          sin(2*PI*x)*sin(2*PI*time), q              , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*cos(2*PI*x)*sin(2*PI*time), gradqST[0]     , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*sin(2*PI*x)*cos(2*PI*time), gradqST[1]     , small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI*sin(2*PI*x)*sin(2*PI*time), hessianqST(0,0), small_tol, close_tol );

  q = 0; qx = 0; qt = 0; qxx = 0;
  fcnSpaceTime.secondGradient(x, time, q, qx, qt, qxx);
  SANS_CHECK_CLOSE(          sin(2*PI*x)*sin(2*PI*time), q  , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*cos(2*PI*x)*sin(2*PI*time), qx , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*sin(2*PI*x)*cos(2*PI*time), qt , small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI*sin(2*PI*x)*sin(2*PI*time), qxx, small_tol, close_tol );


  // Test changing the time for unsteady
  time *= 2.5;

  q = 0; gradq = 0; hessianq = 0;
  fcnUnsteady.secondGradient(X, q, gradq, hessianq);
  SANS_CHECK_CLOSE(          sin(2*PI*x)*sin(2*PI*time), q            , small_tol, close_tol );
  SANS_CHECK_CLOSE(     2*PI*cos(2*PI*x)*sin(2*PI*time), gradq[0]     , small_tol, close_tol );
  SANS_CHECK_CLOSE( -4*PI*PI*sin(2*PI*x)*sin(2*PI*time), hessianq(0,0), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Monomial )
{
  typedef ScalarFunction1D_Monomial::template ArrayQ<Real> ArrayQ;
  typedef SolnNDConvertSpace<PhysD1, ScalarFunction1D_Monomial> SolutionSteady;
  typedef SolnNDConvertSpaceTime<PhysD1, ScalarFunction1D_Monomial> SolutionSpaceTime;
  typedef SolutionSteady::VectorX VectorX;
  typedef SolutionSpaceTime::VectorXT VectorXT;

  int i = 3;
  GlobalTime time(1./3);
  SolutionSteady fcnSteady(i);
  SolutionSteady fcnUnsteady(time, i);
  SolutionSpaceTime fcnSpaceTime(i);

  Real x = 2./3;
  VectorX X = {x};
  VectorXT XT = {x, time};
  ArrayQ slnSteady = fcnSteady(X);
  ArrayQ slnUnsteady = fcnUnsteady(X);
  ArrayQ slnSpaceTime = fcnSpaceTime(XT);


  const Real small_tol = 1.e-13;
  const Real close_tol = 1.e-13;
  SANS_CHECK_CLOSE( pow(x, i), slnSteady, small_tol, close_tol );
  SANS_CHECK_CLOSE( pow(x, i), slnUnsteady, small_tol, close_tol );
  SANS_CHECK_CLOSE( pow(x, i), slnSpaceTime, small_tol, close_tol );

  ArrayQ q, qx, qt, qxx;
  DLA::VectorS< D, ArrayQ > gradq;
  DLA::MatrixSymS< D, ArrayQ > hessianq;

  q = 0; gradq = 0; hessianq = 0;
  fcnSteady.secondGradient(X, q, gradq, hessianq);
  SANS_CHECK_CLOSE(         pow(x, i  ), q            , small_tol, close_tol );
  SANS_CHECK_CLOSE(       i*pow(x, i-1), gradq[0]     , small_tol, close_tol );
  SANS_CHECK_CLOSE( (i-1)*i*pow(x, i-2), hessianq(0,0), small_tol, close_tol );

  q = 0; qx = 0; qt = 0; qxx = 0;
  fcnSteady.secondGradient(x, time, q, qx, qt, qxx);
  SANS_CHECK_CLOSE(         pow(x, i  ), q  , small_tol, close_tol );
  SANS_CHECK_CLOSE(       i*pow(x, i-1), qx , small_tol, close_tol );
  SANS_CHECK_CLOSE(                   0, qt , small_tol, close_tol );
  SANS_CHECK_CLOSE( (i-1)*i*pow(x, i-2), qxx, small_tol, close_tol );


  q = 0; gradq = 0; hessianq = 0;
  fcnUnsteady.secondGradient(X, q, gradq, hessianq);
  SANS_CHECK_CLOSE(         pow(x, i  ), q            , small_tol, close_tol );
  SANS_CHECK_CLOSE(       i*pow(x, i-1), gradq[0]     , small_tol, close_tol );
  SANS_CHECK_CLOSE( (i-1)*i*pow(x, i-2), hessianq(0,0), small_tol, close_tol );

  q = 0; qx = 0; qt = 0; qxx = 0;
  fcnUnsteady.secondGradient(x, time, q, qx, qt, qxx);
  SANS_CHECK_CLOSE(         pow(x, i  ), q  , small_tol, close_tol );
  SANS_CHECK_CLOSE(       i*pow(x, i-1), qx , small_tol, close_tol );
  SANS_CHECK_CLOSE(                   0, qt , small_tol, close_tol );
  SANS_CHECK_CLOSE( (i-1)*i*pow(x, i-2), qxx, small_tol, close_tol );


  DLA::VectorS< D+1, ArrayQ > gradqST;
  DLA::MatrixSymS< D+1, ArrayQ > hessianqST;

  q = 0; gradqST = 0; hessianqST = 0;
  fcnSpaceTime.secondGradient(XT, q, gradqST, hessianqST);
  SANS_CHECK_CLOSE(         pow(x, i  ), q            , small_tol, close_tol );
  SANS_CHECK_CLOSE(       i*pow(x, i-1), gradqST[0]     , small_tol, close_tol );
  SANS_CHECK_CLOSE(                   0, gradqST[1]     , small_tol, close_tol );
  SANS_CHECK_CLOSE( (i-1)*i*pow(x, i-2), hessianqST(0,0), small_tol, close_tol );

  q = 0; qx = 0; qt = 0; qxx = 0;
  fcnSpaceTime.secondGradient(x, time, q, qx, qt, qxx);
  SANS_CHECK_CLOSE(         pow(x, i  ), q  , small_tol, close_tol );
  SANS_CHECK_CLOSE(       i*pow(x, i-1), qx , small_tol, close_tol );
  SANS_CHECK_CLOSE(                   0, qt , small_tol, close_tol );
  SANS_CHECK_CLOSE( (i-1)*i*pow(x, i-2), qxx, small_tol, close_tol );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
