// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// OutputNDConvert_btest
//

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"

#include "pde/OutputCell_Solution.h"

#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime1D.h"

#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime2D.h"

#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime3D.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_None > PDEClass1D;
typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_None > PDEClass2D;
typedef PDEAdvectionDiffusion<PhysD3,
                              AdvectiveFlux3D_Uniform,
                              ViscousFlux3D_Uniform,
                              Source3D_None > PDEClass3D;

typedef OutputCell_Solution<PDEClass1D> OutputClass1D;
typedef OutputCell_Solution<PDEClass2D> OutputClass2D;
typedef OutputCell_Solution<PDEClass3D> OutputClass3D;

}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( OutputNDConvert_test_suite )

template <class PDE>
class DummyBoundaryOutputClass
{
public:

  typedef typename PDE::PhysDim PhysDim;
  static const int D = PhysDim::D;

  //Array of solution variables
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  // Array of outputs
  template<class T>
  using ArrayJ = typename PDE::template ArrayQ<T>;

  // Matrix required to represent the Jacobian of this output functional
  template<class T>
  using MatrixJ = typename PDE::template MatrixQ<T>;

  explicit DummyBoundaryOutputClass() {}

  bool needsSolutionGradient() const { return false; }

  template<class T>
  ArrayJ<T> operator()(const Real& x, const Real& time, const Real& nx,
                       const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& Fn ) const
  {
    return q + qx*nx + Fn;
  }

  template<class T>
  ArrayJ<T> operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                       const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& Fn ) const
  {
    return q + qx*nx + qy*ny + Fn;
  }

  template<class T>
  ArrayJ<T> operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                       const Real& nx, const Real& ny, const Real& nz,
                       const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz,
                       const ArrayQ<T>& Fn ) const
  {
    return q + qx*nx + qy*ny + qz*nz + Fn;
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CellOutputNDConvert1D_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEClass;

  typedef OutputCell_Solution<PDEClass> OutputClass;

  typedef OutputNDConvertSpace<PhysD1, OutputClass> OutputClassSpace;
  typedef OutputNDConvertSpaceTime<PhysD1, OutputClass> OutputClassSpaceTime;

  typedef OutputClassSpace::template ArrayQ<Real> ArrayQ;
  typedef OutputClassSpace::template VectorArrayQ<Real> VectorArrayQ;
  typedef OutputClassSpace::template ArrayJ<Real> ArrayJ;
  typedef OutputClassSpace::VectorX VectorX;

  typedef OutputClassSpaceTime::template VectorArrayQ<Real> VectorArrayQ_ST;
  typedef OutputClassSpaceTime::VectorXT VectorXT;

  const Real tol = 1e-13;

  // function tests
  ArrayQ q = 0.65;
  ArrayQ qx = -0.37;
  VectorArrayQ qxVec = {qx};
  VectorArrayQ_ST qxVecST = {qx, 0};
  ArrayJ J;

  Real x = 0.0;
  Real time = 1.0;
  VectorX xVec = {x};
  VectorXT XT = {x, time};

  OutputClass output;
  OutputClassSpace output_space;
  OutputClassSpaceTime output_spacetime;

  output(x, time, q, qx, J);
  BOOST_CHECK_CLOSE( J, q, tol );

  output_space(xVec, q, qxVec, J);
  BOOST_CHECK_CLOSE( J, q, tol );

  output_spacetime(XT, q, qxVecST, J);
  BOOST_CHECK_CLOSE( J, q, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CellOutputNDConvert2D_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None> PDEClass;

  typedef OutputCell_Solution<PDEClass> OutputClass;

  typedef OutputNDConvertSpace<PhysD2, OutputClass> OutputClassSpace;
  typedef OutputNDConvertSpaceTime<PhysD2, OutputClass> OutputClassSpaceTime;

  typedef OutputClassSpace::template ArrayQ<Real> ArrayQ;
  typedef OutputClassSpace::template VectorArrayQ<Real> VectorArrayQ;
  typedef OutputClassSpace::template ArrayJ<Real> ArrayJ;
  typedef OutputClassSpace::VectorX VectorX;

  typedef OutputClassSpaceTime::template VectorArrayQ<Real> VectorArrayQ_ST;
  typedef OutputClassSpaceTime::VectorXT VectorXT;

  const Real tol = 1e-13;

  // function tests
  ArrayQ q = 0.65;
  ArrayQ qx = -0.37;
  ArrayQ qy =  0.82;
  VectorArrayQ qxVec = {qx, qy};
  VectorArrayQ_ST qxVecST = {qx, qy, 0};
  ArrayJ J;

  Real x = 0.5, y = 0.5;
  Real time = 1.0;
  VectorX xVec = {x, y};
  VectorXT XT = {x, y, time};

  OutputClass output;
  OutputClassSpace output_space;
  OutputClassSpaceTime output_spacetime;

  output(x, y, time, q, qx, qy, J);
  BOOST_CHECK_CLOSE( J, q, tol );

  output_space(xVec, q, qxVec, J);
  BOOST_CHECK_CLOSE( J, q, tol );

  output_spacetime(XT, q, qxVecST, J);
  BOOST_CHECK_CLOSE( J, q, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CellOutputNDConvert3D_test )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEClass;

  typedef OutputCell_Solution<PDEClass> OutputClass;

  typedef OutputNDConvertSpace<PhysD3, OutputClass> OutputClassSpace;
  typedef OutputNDConvertSpaceTime<PhysD3, OutputClass> OutputClassSpaceTime;

  typedef OutputClassSpace::template ArrayQ<Real> ArrayQ;
  typedef OutputClassSpace::template VectorArrayQ<Real> VectorArrayQ;
  typedef OutputClassSpace::template ArrayJ<Real> ArrayJ;
  typedef OutputClassSpace::VectorX VectorX;

  typedef OutputClassSpaceTime::template VectorArrayQ<Real> VectorArrayQ_ST;
  typedef OutputClassSpaceTime::VectorXT VectorXT;

  const Real tol = 1e-13;

  // function tests
  ArrayQ q = 0.65;
  ArrayQ qx = -0.37;
  ArrayQ qy =  0.82;
  ArrayQ qz =  1.05;
  VectorArrayQ qxVec = {qx, qy, qz};
  VectorArrayQ_ST qxVecST = {qx, qy, qz, 0};
  ArrayJ J;

  Real x = 0.5, y = 0.5, z = 0.5;
  Real time = 1.0;
  VectorX xVec = {x, y, z};
  VectorXT XT = {x, y, z, time};

  OutputClass output;
  OutputClassSpace output_space;
  OutputClassSpaceTime output_spacetime;

  output(x, y, z, time, q, qx, qy, qz, J);
  BOOST_CHECK_CLOSE( J, q, tol );

  output_space(xVec, q, qxVec, J);
  BOOST_CHECK_CLOSE( J, q, tol );

  output_spacetime(XT, q, qxVecST, J);
  BOOST_CHECK_CLOSE( J, q, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
