// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// NDConvert1D_btest
//
// test of 1-D NDConvert PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"

#include "pde/ForcingFunction1D_MMS.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}


using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( NDConvert1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( flux )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> PDEClassSpace;
  typedef PDENDConvertSpaceTime<PhysD1, PDEClass> PDEClassSpaceTime;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;
  typedef PDEClassSpace::VectorX VectorX;
  typedef PDEClassSpaceTime::VectorXT VectorXT;

  const Real tol = 1.e-13;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);
  Real time = 0;

  Real a = 1.1, b = 2.4;
  Source1D_UniformGrad src(a, b);

  ScalarFunction1D_Quad solnExact;
  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );

  PDEClassSpace pdespace(adv, visc, src, forcingptr);
  PDEClassSpaceTime pdespacetime(adv, visc, src, forcingptr);

  // flux components
  BOOST_CHECK( pdespace.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pdespace.hasFluxAdvective() == true );
  BOOST_CHECK( pdespace.hasFluxViscous() == true );
  BOOST_CHECK( pdespace.hasSource() == true );
  BOOST_CHECK( pdespace.hasForcingFunction() == true );

  BOOST_CHECK( pdespace.needsSolutionGradientforSource() == true );

  BOOST_CHECK( pdespace.D == 1 );
  BOOST_CHECK( pdespace.N == 1 );

  // flux components
  BOOST_CHECK( pdespacetime.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pdespacetime.hasFluxAdvective() == true );
  BOOST_CHECK( pdespacetime.hasFluxViscous() == true );
  BOOST_CHECK( pdespacetime.hasSource() == true );
  BOOST_CHECK( pdespacetime.hasForcingFunction() == true );

  BOOST_CHECK( pdespacetime.needsSolutionGradientforSource() == true );

  BOOST_CHECK( pdespacetime.D == 2 );
  BOOST_CHECK( pdespacetime.N == 1 );

  // function tests

  Real x;
  Real sln, slnx;

  x = 0;
  VectorX xvec;
  VectorXT xtvec;
  xvec[0] = x;
  xtvec[0] = x;
  xtvec[1] = time;
  sln  =  3.263;
  slnx = -0.445;

  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = qDataPrim[0];
  ArrayQ q;
  pdespace.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  // unsteady flux
  ArrayQ uCons = 0;
  pdespace.masterState( xvec, q, uCons );
  BOOST_CHECK_CLOSE( qTrue, uCons, tol );

  // advective flux
  Real fData[1] = {u*sln};
  ArrayQ fTrue = fData[0];
  DLA::VectorS<1,ArrayQ> F;
  DLA::VectorS<2,ArrayQ> FT;

  F = 0;
  pdespace.fluxAdvective( xvec, q, F );
  BOOST_CHECK_CLOSE( fTrue, F[0], tol );
  FT = 0;
  pdespacetime.fluxAdvective( xtvec, q, FT );
  BOOST_CHECK_CLOSE( fTrue, FT[0], tol );

  // advective flux jacobian

  DLA::VectorS<1,MatrixQ> dFdu = 0;
  DLA::VectorS<2,MatrixQ> dFduT = 0;

  pdespace.jacobianFluxAdvective( xvec, q, dFdu );
  BOOST_CHECK_CLOSE( u, dFdu[0], tol );

  pdespacetime.jacobianFluxAdvective( xtvec, q, dFduT );
  BOOST_CHECK_CLOSE( u, dFduT[0], tol );

  // set gradient
  qDataPrim[0] = slnx;
  ArrayQ qxTrue = qDataPrim[0];
  ArrayQ qx;
  pdespace.setDOFFrom( qx, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxTrue, qx, tol );
  DLA::VectorS<1, ArrayQ> gradq = qx;
  DLA::VectorS<2, ArrayQ> gradqT;
  gradqT[0] = qx; gradqT[1] = 0;

  // diffusive flux
  fTrue = -(kxx*slnx);

  F = 0;
  pdespace.fluxViscous( xvec, q, gradq, F );
  BOOST_CHECK_CLOSE( fTrue, F[0], tol );
  FT = 0;
  pdespacetime.fluxViscous( xtvec, q, gradqT, FT );
  BOOST_CHECK_CLOSE( fTrue, FT[0], tol );
  BOOST_CHECK_CLOSE( 0.0, FT[1], tol );

  // diffusive flux jacobian

  DLA::MatrixS<1,1,MatrixQ> KMtx = 0;
  DLA::MatrixS<2,2,MatrixQ> KMtxT = 0;

  pdespace.diffusionViscous( xvec, q, gradq, KMtx );
  BOOST_CHECK_CLOSE( kxx, KMtx(0,0), tol );

  pdespacetime.diffusionViscous( xtvec, q, gradqT, KMtxT );
  BOOST_CHECK_CLOSE( kxx, KMtxT(0,0), tol );
  BOOST_CHECK_CLOSE( 0.0, KMtxT(0,1), tol );
  BOOST_CHECK_CLOSE( 0.0, KMtxT(1,0), tol );
  BOOST_CHECK_CLOSE( 0.0, KMtxT(1,1), tol );

  // source
  ArrayQ source, sourceTrue = 0;
  ArrayQ sourceL, sourceR;

  ArrayQ qp = 0;
  DLA::VectorS<1, ArrayQ> gradqp = 0;
  DLA::VectorS<2, ArrayQ> gradqpT = 0;

  sourceTrue = a*q + b*qx;

  source = 0;
  pdespace.source( xvec, q, gradq, source );
  BOOST_CHECK_CLOSE( sourceTrue, source, tol );

  source = 0;
  pdespace.source( xvec, q, qp, gradq, gradqp, source );
  BOOST_CHECK_CLOSE( sourceTrue, source, tol );

  sourceL = sourceR = 0;
  pdespace.sourceTrace( xvec, xvec, q, gradq, q, gradq, sourceL, sourceR );
  BOOST_CHECK_CLOSE( sourceTrue, sourceL, tol );
  BOOST_CHECK_CLOSE( sourceTrue, sourceR, tol );

  source = 0;
  pdespacetime.source( xtvec, q, gradqT, source );
  BOOST_CHECK_CLOSE( sourceTrue, source, tol );

  source = 0;
  pdespacetime.source( xtvec, q, qp, gradqT, gradqpT, source );
  BOOST_CHECK_CLOSE( sourceTrue, source, tol );

  sourceL = sourceR = 0;
  pdespacetime.sourceTrace( xtvec, xtvec, q, gradqT, q, gradqT, sourceL, sourceR );
  BOOST_CHECK_CLOSE( sourceTrue, sourceL, tol );
  BOOST_CHECK_CLOSE( sourceTrue, sourceR, tol );


  // forcing function
  ArrayQ force = 0;
  x = 0.5;
  xvec[0] = x;
  xtvec[0] = x;

  pdespace.forcingFunction( xvec, force );
  BOOST_CHECK_CLOSE( 27.126, force, tol );

  force = 0;
  pdespacetime.forcingFunction( xtvec, force );
  BOOST_CHECK_CLOSE( 27.126, force, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( fluxRoe )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> PDEClassSpace;
  typedef PDENDConvertSpaceTime<PhysD1, PDEClass> PDEClassSpaceTime;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClassSpace::VectorX VectorX;
  typedef PDEClassSpaceTime::VectorXT VectorXT;

  const Real tol = 1.e-13;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);
  Real time = 0;

  Real a = 1.1, b = 2.4;
  Source1D_UniformGrad src(a, b);

  ScalarFunction1D_Const solnExact( 0.0 );
  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );

  PDEClassSpace pdespace(adv, visc, src, forcingptr);
  PDEClassSpaceTime pdespacetime(adv, visc, src, forcingptr);

  // advective flux function

  Real x;
  Real nx, nt;
  Real slnL, slnR;

  x = 0;  // not actually used in functions
  nx = 1.22; nt = 2.1;
  slnL = 3.26; slnR = 1.79;

  VectorX xvec;
  VectorXT xtvec;
  xvec[0] = x;
  xtvec[0] = x;
  xtvec[1] = time;

  VectorX N = {nx};
  VectorXT NT = {nx, nt};

  // set
  Real qLDataPrim[1] = {slnL};
  string qLNamePrim[1] = {"Solution"};
  ArrayQ qLTrue = qLDataPrim[0];
  ArrayQ qL;
  pdespace.setDOFFrom( qL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qLTrue, qL, tol );

  Real qRDataPrim[1] = {slnR};
  string qRNamePrim[1] = {"Solution"};
  ArrayQ qRTrue = qRDataPrim[0];
  ArrayQ qR;
  pdespace.setDOFFrom( qR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qRTrue, qR, tol );

  // advective normal flux
  Real fnData[1] = {};
  ArrayQ fnTrue = (nx*u)*slnL;
  ArrayQ fnSTTrue = (nx*u + nt*1)*slnL;
  ArrayQ fn;

  fn = 0;
  pdespace.fluxAdvectiveUpwind( xvec, qL, qR, N, fn );
  BOOST_CHECK_CLOSE( fnTrue, fn, tol );
  fn = 0;
  pdespacetime.fluxAdvectiveUpwind( xtvec, qL, qR, NT, fn );
  BOOST_CHECK_CLOSE( fnSTTrue, fn, tol );

  // viscous flux function

  Real slnxL;
  Real slnxR;

  slnxL = 1.325;
  slnxR = 0.327;

  // set gradient
  qLDataPrim[0] = slnxL;
  ArrayQ qxLTrue = qLDataPrim[0];
  ArrayQ qxL;
  pdespace.setDOFFrom( qxL, qLDataPrim, qLNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxLTrue, qxL, tol );
  DLA::VectorS<1, ArrayQ> gradqL = qxL;
  DLA::VectorS<2, ArrayQ> gradqLT = {qxL, 0};

  qRDataPrim[0] = slnxR;
  ArrayQ qxRTrue = qRDataPrim[0];
  ArrayQ qxR;
  pdespace.setDOFFrom( qxR, qRDataPrim, qRNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxRTrue, qxR, tol );
  DLA::VectorS<1, ArrayQ> gradqR = qxR;
  DLA::VectorS<2, ArrayQ> gradqRT = {qxR, 0};

  // viscous normal flux
  fnData[0] = -0.5*((nx*kxx)*(slnxL + slnxR));

  fn = 0;
  pdespace.fluxViscous( xvec, xvec, qL, gradqL, qR, gradqR, N, fn );
  BOOST_CHECK_CLOSE( fnData[0], fn, tol );
  fn = 0;
  pdespacetime.fluxViscous( xtvec, xtvec, qL, gradqLT, qR, gradqRT, NT, fn );
  BOOST_CHECK_CLOSE( fnData[0], fn, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( speedCharacteristic )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> PDEClassSpace;
  typedef PDENDConvertSpaceTime<PhysD1, PDEClass> PDEClassSpaceTime;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClassSpace::VectorX VectorX;
  typedef PDEClassSpaceTime::VectorXT VectorXT;

  const Real tol = 1.e-13;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);
  Real time = 0;

  Real a = 1.1, b = 2.4;
  Source1D_UniformGrad src(a, b);

  ScalarFunction1D_Const solnExact( 0.0 );

  PDEClassSpace pdespace(adv, visc, src);
  PDEClassSpaceTime pdespacetime(adv, visc, src);

  Real x;
  Real dx;
  Real sln;
  Real speed, speedTrue;

  x = 0;  // not actually used in functions
  dx = 1.22;
  sln  =  3.263;

  VectorX xvec, dxvec;
  VectorXT xtvec, dxtvec;
  xvec[0] = x;
  xtvec[0] = x;
  xtvec[1] = time;
  dxvec[0] = dx;
  dxtvec[0] = dx;
  dxtvec[1] = 0;

  // set
  Real qDataPrim[1] = {sln};
  string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = qDataPrim[0];
  ArrayQ q;
  pdespace.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  speedTrue = fabs(dx*u)/sqrt(dx*dx);

  pdespace.speedCharacteristic( xvec, dxvec, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );
  pdespace.speedCharacteristic( xvec, dxvec, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );
  pdespacetime.speedCharacteristic( xtvec, dxtvec, q, speed );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );
}



//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
