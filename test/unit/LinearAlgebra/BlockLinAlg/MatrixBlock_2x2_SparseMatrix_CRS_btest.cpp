// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include <fstream>

#include "tools/SANSnumerics.h"
#include "tools/minmax.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Add.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"
#include "LinearAlgebra/SparseLinAlg/ScalarMatrix_CRS.h"

#include "unit/LinearAlgebra/TriDiagPattern_btest.h"
#include "../Heat1D_btest.h"

using namespace SANS::SLA;
using namespace SANS::BLA;
using namespace SANS;

//Explicitly instantiate templates to get correct coverage information
namespace SANS
{
namespace BLA
{
}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( BlockLinAlg_Sparse )

static const int M = 2;
static const int N = 2;

typedef MatrixBlock_2x2<
    DLA::MatrixD<SparseMatrix_CRS<DLA::MatrixS<M,N,Real>>>, DLA::MatrixD<SparseMatrix_CRS<DLA::MatrixS<M,1,Real>>>,
    DLA::MatrixD<SparseMatrix_CRS<DLA::MatrixS<1,N,Real>>>, DLA::MatrixD<SparseMatrix_CRS<Real>>                  > BlockMatrixType;

typedef MatrixBlock_2x2<
    DLA::MatrixD<SparseNonZeroPattern<DLA::MatrixS<M,N,Real>>>, DLA::MatrixD<SparseNonZeroPattern<DLA::MatrixS<M,1,Real>>>,
    DLA::MatrixD<SparseNonZeroPattern<DLA::MatrixS<1,N,Real>>>, DLA::MatrixD<SparseNonZeroPattern<Real>>                  > BlockMatrixNZType;

typedef VectorBlock_2< DLA::VectorD<SparseVector<DLA::VectorS<N,Real>>>, DLA::VectorD<SparseVector<Real>> > BlockVectorType;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Sparse_ctor )
{

  const int m = 3;
  const int n = 4;

#ifdef __INTEL_COMPILER
  // intel compiler just can't do initializer lists generically... sigh...
  DLA::MatrixD< SparseNonZeroPattern<DLA::MatrixS<M,N,Real>> > nz00 = {{ {m,m} }};
  DLA::MatrixD< SparseNonZeroPattern<DLA::MatrixS<M,1,Real>> > nz01 = {{ {m,n} }};

  DLA::MatrixD< SparseNonZeroPattern<DLA::MatrixS<1,N,Real>> > nz10 = {{ {n,m} }};
  DLA::MatrixD< SparseNonZeroPattern<Real                  > > nz11 = {{ {n,n} }};

  BlockMatrixNZType nz( nz00, nz01,
                        nz10, nz11 );
#else
  BlockMatrixNZType nz( {{ SparseMatrixSize(m,m) }}, {{ SparseMatrixSize(m,n) }},
                        {{ SparseMatrixSize(n,m) }}, {{ SparseMatrixSize(n,n) }} );
#endif

  BOOST_CHECK_EQUAL( 2, nz.m() );
  BOOST_CHECK_EQUAL( 2, nz.n() );

  TriDiagPattern(nz.m00(0,0));
  TriDiagPattern(nz.m01(0,0));
  TriDiagPattern(nz.m10(0,0));
  TriDiagPattern(nz.m11(0,0));

  BlockMatrixType A(nz);

  //Initialize as a '1D heat equation' matrix and check that is correct
  Heat1D< DLA::MatrixS<2,2,Real> >::init(A.m00(0,0));
  Heat1D< DLA::MatrixS<2,1,Real> >::init(A.m01(0,0));
  Heat1D< DLA::MatrixS<1,2,Real> >::init(A.m10(0,0));
  Heat1D< Real >::init(A.m11(0,0));


  BlockVectorType x( { SparseVectorSize(m) },
                     { SparseVectorSize(n) } );

  BOOST_CHECK_EQUAL( 2, x.m() );
  BOOST_CHECK_EQUAL( 1, x.n() );

#if 0
  ScalarMatrix_CRS<int> a(A);
  std::fstream file("tmp/block_2x2.mtx", std::ios::out);
  a.WriteMatrixMarketFile(file);
#endif
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Sparse_MulVec )
{
  const int m = 3;
  const int n = 4;

  BlockVectorType x( { SparseVectorSize(m) },
                     { SparseVectorSize(n) } );

  BlockVectorType b( { SparseVectorSize(m) },
                     { SparseVectorSize(n) } );

  BlockVectorType c( { SparseVectorSize(m) },
                     { SparseVectorSize(n+1) } ); //<- Wrong size on purpose

#ifdef __INTEL_COMPILER
  // intel compiler just can't do initializer lists generically... sigh...
  DLA::MatrixD< SparseNonZeroPattern<DLA::MatrixS<M,N,Real>> > nz00 = {{ {m,m} }};
  DLA::MatrixD< SparseNonZeroPattern<DLA::MatrixS<M,1,Real>> > nz01 = {{ {m,n} }};

  DLA::MatrixD< SparseNonZeroPattern<DLA::MatrixS<1,N,Real>> > nz10 = {{ {n,m} }};
  DLA::MatrixD< SparseNonZeroPattern<Real                  > > nz11 = {{ {n,n} }};

  BlockMatrixNZType nz( nz00, nz01,
                        nz10, nz11 );
#else
  BlockMatrixNZType nz( {{ SparseMatrixSize(m,m) }}, {{ SparseMatrixSize(m,n) }},
                        {{ SparseMatrixSize(n,m) }}, {{ SparseMatrixSize(n,n) }} );
#endif

  TriDiagPattern(nz.m00(0,0));
  TriDiagPattern(nz.m01(0,0));
  TriDiagPattern(nz.m10(0,0));
  TriDiagPattern(nz.m11(0,0));

  BlockMatrixType A(nz);

  //Initialize as a '1D heat equation' matrix and check that is correct
  Heat1D< DLA::MatrixS<2,2,Real> >::init(A.m00(0,0));
  Heat1D< DLA::MatrixS<2,1,Real> >::init(A.m01(0,0));
  Heat1D< DLA::MatrixS<1,2,Real> >::init(A.m10(0,0));
  Heat1D< Real >::init(A.m11(0,0));


  x = 1;
  x.v0[0][2] = 2;
  x.v1 = 3;

  // bTrue was constructed by dumping the scalar matrix above, printing x,
  // and then multiplying those with mathematica
  Real bTrue[] = {4, 1, -1, -1, 6, 3, 4, -1, 3, 1};
  //std::cout << x.v0 << ", " << x.v1 << std::endl;

  b = A*x;
  int t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(bTrue[t], b.v1[k][j]);
      t++;
    }


  b = +(A*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(bTrue[t], b.v1[k][j]);
      t++;
    }


  b += A*x;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*bTrue[t], b.v1[k][j]);
      t++;
    }


  b = -(A*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(-bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(-bTrue[t], b.v1[k][j]);
      t++;
    }


  b = -A*x;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(-bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(-bTrue[t], b.v1[k][j]);
      t++;
    }


  b = 2*(A*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*bTrue[t], b.v1[k][j]);
      t++;
    }


  b = +(2*(A*x));
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*bTrue[t], b.v1[k][j]);
      t++;
    }


  b += 2*(A*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(4*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(4*bTrue[t], b.v1[k][j]);
      t++;
    }


  b = -(2*(A*x));
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(-2*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(-2*bTrue[t], b.v1[k][j]);
      t++;
    }


  b = (A*x)*2;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*bTrue[t], b.v1[k][j]);
      t++;
    }


  b = (A*x)/2;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(bTrue[t]/2, b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(bTrue[t]/2, b.v1[k][j]);
      t++;
    }


  b = (2*(A*x))*2;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(4*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(4*bTrue[t], b.v1[k][j]);
      t++;
    }


  b = 2*((A*x)*2);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(4*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(4*bTrue[t], b.v1[k][j]);
      t++;
    }


  b = (2*(A*x))/2;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(bTrue[t], b.v1[k][j]);
      t++;
    }


  b = (2*A)*x;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*bTrue[t], b.v1[k][j]);
      t++;
    }

  b = +((2*A)*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*bTrue[t], b.v1[k][j]);
      t++;
    }


  b += (2*A)*x;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(4*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(4*bTrue[t], b.v1[k][j]);
      t++;
    }


  b = A*(x*2);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*bTrue[t], b.v1[k][j]);
      t++;
    }

  b = +(A*(x*2));
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*bTrue[t], b.v1[k][j]);
      t++;
    }

  b += A*(x*2);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(4*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(4*bTrue[t], b.v1[k][j]);
      t++;
    }


  BOOST_CHECK_THROW( b = A*c, AssertionException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Sparse_Expression )
{
  const int m = 3;
  const int n = 4;

  BlockVectorType x( { SparseVectorSize(m) },
                     { SparseVectorSize(n) } );

  BlockVectorType b( { SparseVectorSize(m) },
                     { SparseVectorSize(n) } );

  BlockVectorType r( { SparseVectorSize(m) },
                     { SparseVectorSize(n) } );

#ifdef __INTEL_COMPILER
  // intel compiler just can't do initializer lists generically... sigh...
  DLA::MatrixD< SparseNonZeroPattern<DLA::MatrixS<M,N,Real>> > nz00 = {{ {m,m} }};
  DLA::MatrixD< SparseNonZeroPattern<DLA::MatrixS<M,1,Real>> > nz01 = {{ {m,n} }};

  DLA::MatrixD< SparseNonZeroPattern<DLA::MatrixS<1,N,Real>> > nz10 = {{ {n,m} }};
  DLA::MatrixD< SparseNonZeroPattern<Real                  > > nz11 = {{ {n,n} }};

  BlockMatrixNZType nz( nz00, nz01,
                        nz10, nz11 );
#else
  BlockMatrixNZType nz( {{ SparseMatrixSize(m,m) }}, {{ SparseMatrixSize(m,n) }},
                        {{ SparseMatrixSize(n,m) }}, {{ SparseMatrixSize(n,n) }} );
#endif

  TriDiagPattern(nz.m00(0,0));
  TriDiagPattern(nz.m01(0,0));
  TriDiagPattern(nz.m10(0,0));
  TriDiagPattern(nz.m11(0,0));

  BlockMatrixType A(nz);

  //Initialize as a '1D heat equation' matrix and check that is correct
  Heat1D< DLA::MatrixS<2,2,Real> >::init(A.m00(0,0));
  Heat1D< DLA::MatrixS<2,1,Real> >::init(A.m01(0,0));
  Heat1D< DLA::MatrixS<1,2,Real> >::init(A.m10(0,0));
  Heat1D< Real >::init(A.m11(0,0));


  x = 1;
  x.v0[0][2] = 2;
  x.v1 = 3;

  b = 4;
  b.v0[0][2] = 2;
  b.v1 = 3;

  // bTrue was constructed by dumping the scalar matrix above, printing x,
  // and then multuplying those with mathematica
  Real AxTrue[] = {4, 1, -1, -1, 6, 3, 4, -1, 3, 1};
  //std::cout << x.v0 << ", " << x.v1 << std::endl;


  r = b + A*x;
  int t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(b.v0[k][j][i] + AxTrue[t], r.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(b.v1[k][j] + AxTrue[t], r.v1[k][j]);
      t++;
    }


  r = +(b + A*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(b.v0[k][j][i] + AxTrue[t], r.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(b.v1[k][j] + AxTrue[t], r.v1[k][j]);
      t++;
    }


  r += b + A*x;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*(b.v0[k][j][i] + AxTrue[t]), r.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*(b.v1[k][j] + AxTrue[t]), r.v1[k][j]);
      t++;
    }


  r = b - A*x;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(b.v0[k][j][i] - AxTrue[t], r.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(b.v1[k][j] - AxTrue[t], r.v1[k][j]);
      t++;
    }


  r = +(b - A*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(b.v0[k][j][i] - AxTrue[t], r.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(b.v1[k][j] - AxTrue[t], r.v1[k][j]);
      t++;
    }


  r += b - A*x;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*(b.v0[k][j][i] - AxTrue[t]), r.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*(b.v1[k][j] - AxTrue[t]), r.v1[k][j]);
      t++;
    }


  r = -A*x + b;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(b.v0[k][j][i] - AxTrue[t], r.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(b.v1[k][j] - AxTrue[t], r.v1[k][j]);
      t++;
    }


  r = 2*(b + A*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*(b.v0[k][j][i] + AxTrue[t]), r.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*(b.v1[k][j] + AxTrue[t]), r.v1[k][j]);
      t++;
    }


  r = (b + A*x)*2;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*(b.v0[k][j][i] + AxTrue[t]), r.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*(b.v1[k][j] + AxTrue[t]), r.v1[k][j]);
      t++;
    }

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
