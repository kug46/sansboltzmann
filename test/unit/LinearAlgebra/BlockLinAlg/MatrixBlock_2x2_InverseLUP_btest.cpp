// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/tools/SingularException.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"

#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/InverseLUP.h"

#include <iostream>
using namespace SANS::DLA;
using namespace SANS::BLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BlockLinAlg_Dense )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Dense_InverseLUP_Exceptions )
{
  typedef Real T;

  typedef MatrixBlock_2x2< MatrixD<T>, MatrixD<T>,
                           MatrixD<T>, MatrixD<T> > BlockMatrixType;

  {
    MatrixD<T> A00(2,2), A01(2,2), A10(2,2), A11(2,2);
    MatrixD<T> invA00(2,2), invA01(2,2), invA10(2,2), invA11(2,2);

    BlockMatrixType A(A00, A01, A10, A11);
    BlockMatrixType Ainv(invA00, invA01, invA10, invA11);
    A = 0;
    BOOST_CHECK_THROW( Ainv = InverseLUP::Inverse(A), SingularMatrixException );
  }

  {
    MatrixD<T> A00(2,3), A01(2,2), A10(3,3), A11(3,2);
    MatrixD<T> invA00(2,3), invA01(2,2), invA10(3,3), invA11(3,2);

    BlockMatrixType A(A00, A01, A10, A11);
    BlockMatrixType Ainv(invA00, invA01, invA10, invA11);
    A = Identity();
    BOOST_CHECK_THROW( Ainv = InverseLUP::Inverse(A), AssertionException );
  }

  {
    MatrixD<T> A00(2,2), A01(2,3), A10(3,2), A11(3,3);
    MatrixD<T> invA00(2,3), invA01(2,2), invA10(3,3), invA11(3,2);

    BlockMatrixType A(A00, A01, A10, A11);
    BlockMatrixType Ainv(invA00, invA01, invA10, invA11);
    A = Identity();
    BOOST_CHECK_THROW( Ainv = InverseLUP::Inverse(A), AssertionException );
  }

  {
    MatrixD<T> A00(2,2), A01(2,3), A10(3,2), A11(3,3);
    MatrixD<T> invA00(3,3), invA01(3,2), invA10(2,3), invA11(2,2);

    BlockMatrixType A(A00, A01, A10, A11);
    BlockMatrixType Ainv(invA00, invA01, invA10, invA11);
    A = Identity();
    BOOST_CHECK_THROW( Ainv = InverseLUP::Inverse(A), AssertionException );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Dense_InverseLUP_MatrixD_Rand )
{
  typedef Real T;
  static const int CacheItems = CACHE_LINE_SIZE/sizeof(Real);

  typedef MatrixBlock_2x2< MatrixD<T>, MatrixD<T>,
                           MatrixD<T>, MatrixD<T> > BlockMatrixType;

  const T small_tol = 1e-10;
  const T close_tol = 5e-10;

  for ( int m = 1; m <= CacheItems + CacheItems/2; ++m)
  {
    for ( int n = 1; n <= CacheItems; ++n)
    {
      MatrixD<T> A00(m,m), A01(m,n), A10(n,m), A11(n,n);
      MatrixD<T> I00(m,m), I01(m,n), I10(n,m), I11(n,n);
      I00 = Identity();
      I01 = Identity();
      I10 = Identity();
      I11 = Identity();

      for (int i = 0; i < m; i++)
      {
        for (int j = 0; j < m; j++)
          A00(i,j) = T(rand() % 101/100.) + 5*I00(i,j);

        for (int j = 0; j < n; j++)
          A01(i,j) = T(rand() % 101/100.) + 5*I01(i,j);
      }
      for (int i = 0; i < n; i++)
      {
        for (int j = 0; j < m; j++)
          A10(i,j) = T(rand() % 101/100.) + 5*I10(i,j);

        for (int j = 0; j < n; j++)
          A11(i,j) = T(rand() % 101/100.) + 5*I11(i,j);
      }

      BlockMatrixType A(A00, A01, A10, A11);

      BlockMatrixType Ainv(A00, A01, A10, A11);
      Ainv = InverseLUP::Inverse(A);

      BlockMatrixType I(A00, A01, A10, A11);
      I = Ainv*A;

      for (int i = 0; i < m; ++i)
        for (int j = 0; j < m; ++j)
        {
          if (i == j)
          {
            SANS_CHECK_CLOSE( I.m00(i,j), 1.0, small_tol, close_tol );
          }
          else
            SANS_CHECK_CLOSE( I.m00(i,j), 0.0, small_tol, close_tol );
        }

      for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
          SANS_CHECK_CLOSE( I.m01(i,j), 0.0, small_tol, close_tol );

      for (int i = 0; i < n; ++i)
        for (int j = 0; j < m; ++j)
          SANS_CHECK_CLOSE( I.m10(i,j), 0.0, small_tol, close_tol );

      for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
        {
          if (i == j)
          {
            SANS_CHECK_CLOSE( I.m11(i,j), 1.0, small_tol, close_tol );
          }
          else
            SANS_CHECK_CLOSE( I.m11(i,j), 0.0, small_tol, close_tol );
        }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Dense_SolveLUP_MatrixD_m1n1 )
{
  typedef Real T;

  typedef MatrixBlock_2x2< MatrixD<T>, MatrixD<T>,
                           MatrixD<T>, MatrixD<T> > BlockMatrixType;

  typedef VectorBlock_2< MatrixD<T>, MatrixD<T> > BlockVectorType;

  MatrixD<T> A00 = {{2}};
  MatrixD<T> A01 = {{1}};
  MatrixD<T> A10 = {{3}};
  MatrixD<T> A11 = {{2}};

  MatrixD<T> b0 = {{2}};
  MatrixD<T> b1 = {{3}};

  BlockMatrixType A(A00, A01, A10, A11);
  BlockVectorType b(b0, b1);

  BlockVectorType x(b0, b1);

  x = InverseLUP::Solve(A, b);

  BOOST_CHECK_CLOSE( A.m00(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A.m01(0,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A.m10(0,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A.m11(0,0), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x.v0(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( x.v1(0,0),        T(1e-12) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Dense_SolveLUP_MatrixD_m1n1_2b )
{
  //Check that b = !A*b works properly
  typedef Real T;

  typedef MatrixBlock_2x2< MatrixD<T>, MatrixD<T>,
                           MatrixD<T>, MatrixD<T> > BlockMatrixType;

  typedef VectorBlock_2< MatrixD<T>, MatrixD<T> > BlockVectorType;

  MatrixD<T> A00 = {{2}};
  MatrixD<T> A01 = {{1}};
  MatrixD<T> A10 = {{3}};
  MatrixD<T> A11 = {{2}};

  MatrixD<T> b0 = {{2}};
  MatrixD<T> b1 = {{3}};

  BlockMatrixType A(A00, A01, A10, A11);
  BlockVectorType b(b0, b1);

  b = InverseLUP::Solve(A, b);

  BOOST_CHECK_CLOSE( A.m00(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A.m01(0,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A.m10(0,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A.m11(0,0), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b.v0(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( b.v1(0,0),        T(1e-12) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Dense_SolveLUP_MatrixD_m1n1_Expressions )
{
  typedef Real T;

  typedef MatrixBlock_2x2< MatrixD<T>, MatrixD<T>,
                           MatrixD<T>, MatrixD<T> > BlockMatrixType;

  typedef VectorBlock_2< MatrixD<T>, MatrixD<T> > BlockVectorType;

  MatrixD<T> A00 = {{2}};
  MatrixD<T> A01 = {{1}};
  MatrixD<T> A10 = {{3}};
  MatrixD<T> A11 = {{2}};

  MatrixD<T> b0 = {{2}};
  MatrixD<T> b1 = {{3}};

  BlockMatrixType A(A00, A01, A10, A11);
  BlockVectorType b(b0, b1);

  BlockVectorType x(b0, b1);

  x = InverseLUP::Solve(A, b) + 2*b;

  BOOST_CHECK_CLOSE( A.m00(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A.m01(0,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A.m10(0,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A.m11(0,0), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x.v0(0,0), T(5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x.v1(0,0), T(6.), T(0.0001) );

  x = InverseLUP::Solve(A, 2*b + b);

  BOOST_CHECK_CLOSE( x.v0(0,0), T(3.), T(0.0001) );
  BOOST_CHECK_CLOSE( x.v1(0,0), T(0.), T(0.0001) );

  x = 3*InverseLUP::Solve(A, 2*b + b) - 2*b;

  BOOST_CHECK_CLOSE( x.v0(0,0), T( 5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x.v1(0,0), T(-6.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Dense_SolveLUP_MatrixD_m2n1 )
{
  typedef Real T;

  typedef MatrixBlock_2x2< MatrixD<T>, MatrixD<T>,
                           MatrixD<T>, MatrixD<T> > BlockMatrixType;

  typedef VectorBlock_2< MatrixD<T>, MatrixD<T> > BlockVectorType;

  MatrixD<T> A00 = {{4,2},{2,4}};
  MatrixD<T> A01 = {{2},{6}};
  MatrixD<T> A10 = {{2,6}};
  MatrixD<T> A11 = {{6}};

  MatrixD<T> b0 = {{1},{3}};
  MatrixD<T> b1 = {{2}};

  BlockMatrixType A(A00, A01, A10, A11);
  BlockVectorType b(b0, b1);

  BlockVectorType x(b0, b1);
  x = InverseLUP::Solve(A, b);

// The hard coded indicies confuses the analyzer
#ifndef __clang_analyzer__
  BOOST_CHECK_CLOSE( A.m00(0,0), 4., T(0.0001) );
  BOOST_CHECK_CLOSE( A.m00(0,1), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A.m00(1,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A.m00(1,1), 4., T(0.0001) );

  BOOST_CHECK_CLOSE( A.m01(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A.m01(1,0), 6., T(0.0001) );

  BOOST_CHECK_CLOSE( A.m10(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A.m10(0,1), 6., T(0.0001) );

  BOOST_CHECK_CLOSE( A.m11(0,0), 6., T(0.0001) );

  BOOST_CHECK_CLOSE( x.v0(0,0), T( 0.1), T(0.0001) );
  BOOST_CHECK_CLOSE( x.v0(1,0), T(-0.5), T(0.0001) );

  BOOST_CHECK_CLOSE( x.v1(0,0), T(4./5.), T(0.0001) );
#endif

  MatrixD<T> B00 = {{1,2},{2,3}};
  MatrixD<T> B01 = {{1},{1}};
  MatrixD<T> B10 = {{1,1}};
  MatrixD<T> B11 = {{2}};

  BlockMatrixType B(B00, B01, B10, B11);

  BlockMatrixType X(B00, B01, B10, B11);
  X = InverseLUP::Solve(A, B);

// The hard coded indicies confuses the analyzer
#ifndef __clang_analyzer__
  BOOST_CHECK_CLOSE( X.m00(0,0), T( 1./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( X.m00(0,1), T( 1./2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X.m00(1,0), T(-1./2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X.m00(1,1), T(-1.   ), T(0.0001) );

  BOOST_CHECK_CLOSE( X.m01(0,0), T(1./10.), T(0.0001) );
  BOOST_CHECK_CLOSE( X.m01(1,0), T( 1./2.), T(0.0001) );

  BOOST_CHECK_CLOSE( X.m10(0,0), T( 3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( X.m10(0,1), T( 1.   ), T(0.0001) );

  BOOST_CHECK_CLOSE( X.m11(0,0), T(-1./5.), T(0.0001) );
#endif
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Dense_SolveLUP_MatrixD_m2n1_zero_diagonals_1 )
{
  typedef Real T;

  typedef MatrixBlock_2x2< MatrixD<T>, MatrixD<T>,
      MatrixD<T>, MatrixD<T> > BlockMatrixType;

  typedef VectorBlock_2< MatrixD<T>, MatrixD<T> > BlockVectorType;

  MatrixD<T> A00 = {{  1.0000, -0.5000, -0.5000,       0},
                    { -0.5000,  1.0000,       0, -0.5000},
                    { -0.5000,       0,  1.0000, -0.5000},
                    {       0, -0.5000, -0.5000,  1.0000}};

  MatrixD<T> A01 = {{ 0.6667, 0.1667, 0.1667,      0},
                    { 0.1667, 0.6667,      0, 0.1667},
                    { 0.1667,      0, 0.6667, 0.1667},
                    {      0, 0.1667, 0.1667, 0.6667}};

  MatrixD<T> A10 = {{ 0.6667, 0.1667, 0.1667,      0},
                    { 0.1667, 0.6667,      0, 0.1667},
                    { 0.1667,      0, 0.6667, 0.1667},
                    {      0, 0.1667, 0.1667, 0.6667}};

  MatrixD<T> A11 = {{1, 0, 0, 0},
                    {0, 1, 0, 0},
                    {0, 0, 1, 0},
                    {0, 0, 0, 1}};


  //    MatrixD<T> b0 = {{6.66134e-16, 1.33227e-15, 7.77156e-16, 4.44089e-16}};
  //    MatrixD<T> b1 = {{1.70717e-17, 1.70717e-17, 1.0638e-16, 1.0638e-16}};

  VectorD<T> b0 = {0, 0, 0, 0};
  VectorD<T> b1 = {0, 0, 0, 0};

  A11 *= 100;
  BlockMatrixType A(A00, A01, A10, A11);
  BlockVectorType b(b0, b1);

  BlockVectorType x(b0, b1);
  x = InverseLUP::Solve(A, b); // Shouldn't through a singular exception, this is fine
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Dense_SolveLUP_MatrixD_m2n1_zero_diagonals_2 )
{
  typedef Real T;

  typedef MatrixBlock_2x2< MatrixD<T>, MatrixD<T>,
      MatrixD<T>, MatrixD<T> > BlockMatrixType;

  typedef VectorBlock_2< MatrixD<T>, MatrixD<T> > BlockVectorType;

  MatrixD<T> A00 = {{   1,    0, -0.5, -0.5},
                    {   0,    1, -0.5, -0.5},
                    {-0.5, -0.5,    1,    0},
                    {-0.5, -0.5,    0,    1}};

  MatrixD<T> A01 = {{0.666667,        0, 0.166667, 0.166667},
                    {       0, 0.666667, 0.166667, 0.166667},
                    {0.166667, 0.166667, 0.666667,        0},
                    {0.166667, 0.166667,        0, 0.666667}};

  MatrixD<T> A10 = {{0.666667,        0, 0.166667, 0.166667},
                    {       0, 0.666667, 0.166667, 0.166667},
                    {0.166667, 0.166667, 0.666667,        0},
                    {0.166667, 0.166667,        0, 0.666667}};

  MatrixD<T> A11 = {{1, 0, 0, 0},
                    {0, 1, 0, 0},
                    {0, 0, 1, 0},
                    {0, 0, 0, 1}};

  VectorD<T> b0 = {0, 0, 0, 0};
  VectorD<T> b1 = {0, 0, 0, 0};

  A11 *= 100;
  BlockMatrixType A(A00, A01, A10, A11);
  BlockVectorType b(b0, b1);

  BlockVectorType x(b0, b1);
  x = InverseLUP::Solve(A, b); // Shouldn't through a singular exception, this is fine
}

#endif


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Dense_SolveLUP_MatrixD_MatrixS22_and_VectorS2_m2n1 )
{
  typedef MatrixS<2,2,Real> T;

  typedef MatrixBlock_2x2< MatrixD<T>, MatrixD<T>,
                           MatrixD<T>, MatrixD<T> > BlockMatrixType;

  typedef VectorBlock_2< MatrixD<T>, MatrixD<T> > BlockVector_MatrixS22;
  typedef VectorBlock_2< MatrixD<VectorS<2,Real>>, MatrixD<VectorS<2,Real>> > BlockVector_VectorS2;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  T S00_00 = {{ 4, 2},{ 2, 4}};
  T S00_01 = {{-1, 0},{ 3, 2}};
  T S00_10 = {{ 1, 2},{-2,-1}};
  T S00_11 = {{ 6, 2},{ 4, 3}};

  T S01_00 = {{ 0, 1},{-3, 2}};
  T S01_10 = {{ 2,-1},{ 4, 3}};

  T S10_00 = {{ 3, 0},{ 5, 1}};
  T S10_01 = {{ 1, 2},{ 0, 3}};

  T S11_00 = {{ 4,-1},{ 2, 7}};

  MatrixD<T> A00 = {{S00_00, S00_01},{S00_10, S00_11}};
  MatrixD<T> A01 = {{S01_00},{S01_10}};
  MatrixD<T> A10 = {{S10_00, S10_01}};
  MatrixD<T> A11 = {{S11_00}};

  T b0_0 = {{ 1, 2},{ 0, 3}};
  T b0_1 = {{ 3,-1},{ 4, 2}};
  T b1_0 = {{-4, 5},{ 1, 1}};

  MatrixD<T> b0 = {{b0_0},{b0_1}};
  MatrixD<T> b1 = {{b1_0}};

  BlockMatrixType A(A00, A01, A10, A11);
  BlockVector_MatrixS22 b(b0, b1);

  BlockVector_MatrixS22 x(b0, b1);
  x = InverseLUP::Solve(A, b);

  BlockVector_MatrixS22 f(b0, b1);
  f = A*x;

// The hard coded indicies confuses the analyzer
#ifndef __clang_analyzer__
  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 2; j++)
    {
      SANS_CHECK_CLOSE( f.v0(0,0)(i,j), b.v0(0,0)(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( f.v0(1,0)(i,j), b.v0(1,0)(i,j), small_tol, close_tol );
      SANS_CHECK_CLOSE( f.v1(0,0)(i,j), b.v1(0,0)(i,j), small_tol, close_tol );
    }
#endif

  MatrixD<VectorS<2,Real>> bvec0 = {{{1,2}},{{3,4}}};
  MatrixD<VectorS<2,Real>> bvec1 = {{{5,6}}};

  BlockVector_VectorS2 bvec(bvec0, bvec1);
  BlockVector_VectorS2 xvec(bvec0, bvec1);

  xvec = InverseLUP::Solve(A, bvec);

  BlockVector_VectorS2 fvec(bvec0, bvec1);
  fvec = A*xvec;

// The hard coded indicies confuses the analyzer
#ifndef __clang_analyzer__
  for (int i = 0; i < 2; i++)
  {
    SANS_CHECK_CLOSE( fvec.v0(0,0)(i), bvec.v0(0,0)(i), small_tol, close_tol );
    SANS_CHECK_CLOSE( fvec.v0(1,0)(i), bvec.v0(1,0)(i), small_tol, close_tol );
    SANS_CHECK_CLOSE( fvec.v1(0,0)(i), bvec.v1(0,0)(i), small_tol, close_tol );
  }
#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
