// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include "LinearAlgebra/BlockLinAlg/BlockLinAlg_Add.h"
#include "LinearAlgebra/BlockLinAlg/BlockLinAlg_Mul.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_4.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

//#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

using namespace SANS;
using namespace SANS::DLA;
using namespace SANS::BLA;
using namespace SANS::SLA;

namespace SANS
{
typedef VectorS<4,Real> VectorS4;
typedef VectorS<3,Real> VectorS3;
typedef VectorS<2,Real> VectorS2;

typedef VectorD<VectorS4> DenseVector4;
typedef VectorD<VectorS3> DenseVector3;
typedef VectorD<VectorS2> DenseVector2;
typedef VectorD<Real> DenseVector1;

typedef VectorBlock_4<VectorD<DenseVector4>,
                      VectorD<DenseVector3>,
                      VectorD<DenseVector2>,
                      VectorD<DenseVector1>> VectorBlock_4_DenseType;
//typedef VectorBlock_4<DenseVectorSize,
//                      DenseVectorSize,
//                      DenseVectorSize,
//                      DenseVectorSize> VectorBlock_4_DenseSizeType;

//typedef SparseVector<VectorS4> SparseVector4;
//typedef SparseVector<VectorS3> SparseVector3;
//typedef SparseVector<VectorS2> SparseVector2;
//typedef SparseVector<Real> SparseVector1;
//
//typedef VectorBlock_4<VectorD<SparseVector4>,
//                      VectorD<SparseVector3>,
//                      VectorD<SparseVector2>,
//                      VectorD<SparseVector1>> VectorBlock_4_SparseType;

// Explicitly instantiate templates to get correct coverage information
namespace BLA
{
template class VectorBlock_4<VectorD<DenseVector4>,
                             VectorD<DenseVector3>,
                             VectorD<DenseVector2>,
                             VectorD<DenseVector1>>;
//template class VectorBlock_4<VectorD<SparseVector4>,
//                             VectorD<SparseVector3>,
//                             VectorD<SparseVector2>,
//                             VectorD<SparseVector1>>;
}

//----------------------------------------------------------------------------//
// Utility functions
//
VectorBlock_4_DenseType initializeVectorBlock4Dense(const Real& scalarValue)
{
  VectorS4 vs4(scalarValue);
  VectorS3 vs3(scalarValue);
  VectorS2 vs2(scalarValue);
  Real vs1 = scalarValue;

  // ctor: initializer lists
  std::initializer_list<DenseVector4> s0 = {DenseVector4(4,vs4)};
  std::initializer_list<DenseVector3> s1 = {DenseVector3(3,vs3)};
  std::initializer_list<DenseVector2> s2 {DenseVector2(2,vs2)};
  std::initializer_list<DenseVector1> s3 {DenseVector1(1,vs1)};

  return VectorBlock_4_DenseType(s0,s1,s2,s3);
}


void checkValue_VectorBlock4(const VectorBlock_4_DenseType& blockvec,
                             const VectorS4& vs4,
                             const VectorS3& vs3,
                             const VectorS2& vs2,
                             const Real& vs1)
{
  // blockvec.v0
  for (int i=0; i<blockvec.v0.m(); ++i)
  {
    for (int j=0; j<blockvec.v0[i].m(); ++j)
    {
      for (int k=0; k<VectorS4::M; ++k)
      {
        BOOST_CHECK_EQUAL(vs4[k],blockvec.v0[i][j][k]);
      }
    }
  }
  // blockvec.v1
  for (int i=0; i<blockvec.v1.m(); ++i)
  {
    for (int j=0; j<blockvec.v1[i].m(); ++j)
    {
      for (int k=0; k<VectorS3::M; ++k)
      {
        BOOST_CHECK_EQUAL(vs3[k],blockvec.v1[i][j][k]);
      }
    }
  }
  // blockvec.v2
  for (int i=0; i<blockvec.v2.m(); ++i)
  {
    for (int j=0; j<blockvec.v2[i].m(); ++j)
    {
      for (int k=0; k<VectorS2::M; ++k)
      {
        BOOST_CHECK_EQUAL(index(vs2,k),blockvec.v2[i][j][k]);
      }
    }
  }
  // blockvec.v3
  for (int i=0; i<blockvec.v3.m(); ++i)
  {
    for (int j=0; j<blockvec.v3[i].m(); ++j)
    {
      BOOST_CHECK_EQUAL(vs1,blockvec.v3[i][j]);
    }
  }
}

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( BlockLinAlg_Block_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  BOOST_CHECK(4==VectorBlock_4_DenseType::m());
  BOOST_CHECK(1==VectorBlock_4_DenseType::n());
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorBlock_4_Dense_ctor_test )
{
  VectorS4 vs4 = {7,8,9,10};
  VectorS3 vs3 = {4,5,6};
  VectorS2 vs2 = {2,3};
  Real vs1 = 1;

  // ctor: initializer lists
  std::initializer_list<DenseVector4> s0 = {DenseVector4(4,vs4)};
  std::initializer_list<DenseVector3> s1 = {DenseVector3(3,vs3)};
  std::initializer_list<DenseVector2> s2 {DenseVector2(2,vs2)};
  std::initializer_list<DenseVector1> s3 {DenseVector1(1,vs1)};

  VectorBlock_4_DenseType blockvec1(s0,s1,s2,s3);

  // ctor: assemble individual vector block components
  VectorD<DenseVector4> v0 = s0;
  VectorD<DenseVector3> v1 = s1;
  VectorD<DenseVector2> v2 = s2;
  VectorD<DenseVector1> v3 = s3;

  VectorBlock_4_DenseType blockvec2(v0,v1,v2,v3);

  // copy ctor: copy block vector
  VectorBlock_4_DenseType blockvec3(blockvec1);

  // --- check values --- //
  //
  checkValue_VectorBlock4(blockvec1, vs4, vs3, vs2, vs1);
  checkValue_VectorBlock4(blockvec2, vs4, vs3, vs2, vs1);
  checkValue_VectorBlock4(blockvec3, vs4, vs3, vs2, vs1);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorBlock_4_Dense_assignment_evaluation_test )
{
  const Real value_true = 16.6; // all components of the vector take this value

  // assignment operator =
  {
    VectorBlock_4_DenseType blockvec = initializeVectorBlock4Dense(0);
    VectorBlock_4_DenseType blockvec2 = initializeVectorBlock4Dense(0);
    VectorBlock_4_DenseType blockvec3 = initializeVectorBlock4Dense(0);

    blockvec = value_true;
    blockvec2 = blockvec;

    BlockLinAlgType<VectorBlock_4_DenseType>* blockvec_base = &blockvec;
    blockvec3 = *blockvec_base;

    const VectorS4 vs4(value_true);
    const VectorS3 vs3(value_true);
    const VectorS2 vs2(value_true);
    const Real vs1 = value_true;

    // --- check values --- //
    checkValue_VectorBlock4(blockvec3, vs4, vs3, vs2, vs1);
  }

  // lazy assignment +=
  {
    VectorBlock_4_DenseType blockvec = initializeVectorBlock4Dense(0);
    VectorBlock_4_DenseType blockvec2 = initializeVectorBlock4Dense(0);

    blockvec = value_true;
    blockvec2 += blockvec;

    const VectorS4 vs4(value_true);
    const VectorS3 vs3(value_true);
    const VectorS2 vs2(value_true);
    const Real vs1 = value_true;

    // --- check values --- //
    checkValue_VectorBlock4(blockvec2, vs4, vs3, vs2, vs1);
  }

  // lazy assignment -=
  {
    VectorBlock_4_DenseType blockvec = initializeVectorBlock4Dense(0);
    VectorBlock_4_DenseType blockvec2 = initializeVectorBlock4Dense(0);

    blockvec2 = 2*value_true;
    blockvec = value_true;

    blockvec2 -= blockvec;

    const VectorS4 vs4(value_true);
    const VectorS3 vs3(value_true);
    const VectorS2 vs2(value_true);
    const Real vs1 = value_true;

    // --- check values --- //
    checkValue_VectorBlock4(blockvec2, vs4, vs3, vs2, vs1);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorBlock_4_Dense_evaluation_test )
{
  const Real value_true = 16.6;

  // value(...) : sign = +1
  {
    VectorBlock_4_DenseType blockvec = initializeVectorBlock4Dense(value_true);
    VectorBlock_4_DenseType blockvec2 = initializeVectorBlock4Dense(0);

    blockvec.value(+1,blockvec2);

    const VectorS4 vs4(value_true);
    const VectorS3 vs3(value_true);
    const VectorS2 vs2(value_true);
    const Real vs1 = value_true;

    // --- check values --- //
    checkValue_VectorBlock4(blockvec2, vs4, vs3, vs2, vs1);
  }

  // value(...) : sign = -1
  {
    VectorBlock_4_DenseType blockvec = initializeVectorBlock4Dense(value_true);
    VectorBlock_4_DenseType blockvec2 = initializeVectorBlock4Dense(0);

    blockvec.value(-1,blockvec2);

    const VectorS4 vs4(-value_true);
    const VectorS3 vs3(-value_true);
    const VectorS2 vs2(-value_true);
    const Real vs1 = -value_true;

    // --- check values --- //
    checkValue_VectorBlock4(blockvec2, vs4, vs3, vs2, vs1);
  }

  // plus(...) : sign = +1
  {
    VectorBlock_4_DenseType blockvec = initializeVectorBlock4Dense(0.5*value_true);
    VectorBlock_4_DenseType blockvec2 = initializeVectorBlock4Dense(0.5*value_true);

    blockvec.plus(+1,blockvec2);

    const VectorS4 vs4(value_true);
    const VectorS3 vs3(value_true);
    const VectorS2 vs2(value_true);
    const Real vs1 = value_true;

    // --- check values --- //
    checkValue_VectorBlock4(blockvec2, vs4, vs3, vs2, vs1);
  }

  // plus(...) : sign = -1
  {
    VectorBlock_4_DenseType blockvec = initializeVectorBlock4Dense(value_true);
    VectorBlock_4_DenseType blockvec2 = initializeVectorBlock4Dense(2*value_true);

    blockvec.plus(-1,blockvec2);

    const VectorS4 vs4(value_true);
    const VectorS3 vs3(value_true);
    const VectorS2 vs2(value_true);
    const Real vs1 = value_true;

    // --- check values --- //
    checkValue_VectorBlock4(blockvec2, vs4, vs3, vs2, vs1);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorBlock_4_Dense_BlockLinAlg_Add_test )
{
  VectorS4 vs4 = {0,0,0,0};
  VectorS3 vs3 = {0,0,0};
  VectorS2 vs2 = {0,0};
  Real vs1 = 0;

  // ctor: initializer lists
  std::initializer_list<DenseVector4> s0 = {DenseVector4(4,vs4)};
  std::initializer_list<DenseVector3> s1 = {DenseVector3(3,vs3)};
  std::initializer_list<DenseVector2> s2 {DenseVector2(2,vs2)};
  std::initializer_list<DenseVector1> s3 {DenseVector1(1,vs1)};

  const Real value_true = 16.6;

  VectorBlock_4_DenseType blockvec(s0,s1,s2,s3);
  VectorBlock_4_DenseType blockvec2(s0,s1,s2,s3);

  // addition +
  {
    VectorBlock_4_DenseType blockvec = initializeVectorBlock4Dense(0.5*value_true);
    VectorBlock_4_DenseType blockvec2 = initializeVectorBlock4Dense(0);

    blockvec2 = blockvec + blockvec;

    // --- check values --- //
    const VectorS4 vs4(value_true);
    const VectorS3 vs3(value_true);
    const VectorS2 vs2(value_true);
    const Real vs1 = value_true;

    checkValue_VectorBlock4(blockvec2, vs4, vs3, vs2, vs1);
  }

  // subtraction -
  {
    VectorBlock_4_DenseType blockvec = initializeVectorBlock4Dense(0.5*value_true);
    VectorBlock_4_DenseType blockvec2 = initializeVectorBlock4Dense(0);

    blockvec2 = blockvec - blockvec;

    // --- check values --- //
    const VectorS4 vs4(0);
    const VectorS3 vs3(0);
    const VectorS2 vs2(0);
    const Real vs1 = 0;

    checkValue_VectorBlock4(blockvec2, vs4, vs3, vs2, vs1);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorBlock_4_Dense_BlockLinAlg_Mul_test )
{ // Tests scalar multiplication

  // M = s*(A+B) and M = (A+B)*s
  {
    const Real value_true = 16.6;
    VectorBlock_4_DenseType blockvec = initializeVectorBlock4Dense(value_true);

    VectorBlock_4_DenseType blockvec2 = initializeVectorBlock4Dense(0);

    std::vector<std::string> OpList
    = {"E*s", "E/s", "s*E", "-E", "(s1*E)*s2", "(s1*E)/s2", "s2*(s1*E)", "-(s1*E)"};

    for (std::string op : OpList)
    {
      if (op=="E*s")
      {
        blockvec2 = (blockvec+blockvec)*0.5;
      }
      else if (op=="E/s")
      {
        blockvec2 = (blockvec+blockvec)/2.0;
      }
      else if (op=="s*E")
      {
        blockvec2 = 0.5*(blockvec+blockvec);
      }
      else if (op=="-E")
      {
        blockvec2 = -(-blockvec);
      }
      else if (op=="(s1*E)*s2")
      {
        blockvec2 = 0.5*blockvec*2.0;
      }
      else if (op=="(s1*E)/s2")
      {
        blockvec2 = (2.0*blockvec)/2.0;
      }
      else if (op=="s2*(s1*E)")
      {
        blockvec2 = 0.5*(2.0*blockvec);
      }
      else if (op=="-(s1*E)")
      {
        blockvec2 = -((-1.0)*blockvec);
      }
      else
      {
        SANS_DEVELOPER_EXCEPTION("Unknown operation!");
      }

      // --- check values --- //
      const VectorS4 vs4(value_true);
      const VectorS3 vs3(value_true);
      const VectorS2 vs2(value_true);
      const Real vs1 = value_true;

      checkValue_VectorBlock4(blockvec2, vs4, vs3, vs2, vs1);
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
