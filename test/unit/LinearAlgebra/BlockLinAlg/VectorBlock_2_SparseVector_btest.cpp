// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include <fstream>

#include "tools/SANSnumerics.h"
#include "tools/minmax.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Add.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"

#include "unit/LinearAlgebra/TriDiagPattern_btest.h"
#include "../Heat1D_btest.h"

using namespace SANS::SLA;
using namespace SANS::BLA;
using namespace SANS;

//Explicitly instantiate templates to get correct coverage information
namespace SANS
{
namespace BLA
{
}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( BlockLinAlg_Sparse )

static const int N = 2;

typedef VectorBlock_2< DLA::VectorD<SparseVector<DLA::VectorS<N,Real>>>, DLA::VectorD<SparseVector<Real>> > BlockVectorType;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorBlock_2_Sparse_ctor )
{

  const int m = 3;
  const int n = 4;

  BlockVectorType x( { SparseVectorSize(m) },
                     { SparseVectorSize(n) } );

  BOOST_CHECK_EQUAL( 2, x.m() );
  BOOST_CHECK_EQUAL( 1, x.n() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorBlock_2x2_Sparse_MulVec )
{
  const int m = 3;
  const int n = 4;

  BlockVectorType x1( { SparseVectorSize(m) },
                     { SparseVectorSize(n) } );

  BlockVectorType x2( { SparseVectorSize(m) },
                     { SparseVectorSize(n) } );

  BlockVectorType b( { SparseVectorSize(m) },
                     { SparseVectorSize(n) } );

  BlockVectorType c( { SparseVectorSize(m) },
                     { SparseVectorSize(n+1) } ); //<- Wrong size on purpose

  x1 = 1;
  x1.v0[0][2] = 2;
  x1.v1 = 3;

  x2 = 1;
  x2.v0[0][2] = 2;
  x2.v1 = 4;

  Real x1True[] = {1,  1,  1,  1,  2,  2,  3,  3,  3,  3};
  Real x2True[] = {1,  1,  1,  1,  2,  2,  4,  4,  4,  4};

  // Test assignment
  b = x1;
  int t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(x1True[t], b.v0[k][j][i]);
        t++;
      }

  // test scalar multiplication
  b = 2.0*x1;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*x1True[t], b.v0[k][j][i]);
        t++;
      }

  // test addition
  b = x1 + x2;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(x1True[t] + x2True[t], b.v0[k][j][i]);
        t++;
      }

  // test addition
  b += x1;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2.0*x1True[t] + x2True[t], b.v0[k][j][i]);
        t++;
      }

  // test addition with scalar multiplication
  b += 2.0*x2;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2.0*x1True[t] + 3.0*x2True[t], b.v0[k][j][i]);
        t++;
      }

  // test scalar multiplication of summed vectors
  b = 2.0*(x1 + x2);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2.0*x1True[t] + 2.0*x2True[t], b.v0[k][j][i]);
        t++;
      }

  // Test assignment (repeated)
  b = x1;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(x1True[t], b.v0[k][j][i]);
        t++;
      }

  // test subtraction
  b = x1 - x2;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(x1True[t] - x2True[t], b.v0[k][j][i]);
        t++;
      }

  // test subtraction
  b -= x1;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(-1.0*x2True[t], b.v0[k][j][i]);
        t++;
      }

  // test subtraction with scalar multiplication
  b -= 2.0*x2;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(-3.0*x2True[t], b.v0[k][j][i]);
        t++;
      }

  // test scalar multiplication of summed vectors
  b = 2.0*(x1 - x2);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2.0*x1True[t] - 2.0*x2True[t], b.v0[k][j][i]);
        t++;
      }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
