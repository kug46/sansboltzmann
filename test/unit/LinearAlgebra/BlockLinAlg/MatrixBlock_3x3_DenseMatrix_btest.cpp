// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include <fstream>

#include "tools/SANSnumerics.h"
#include "tools/minmax.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_3x3.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_3.h"

#include "../Heat1D_btest.h"

using namespace SANS::DLA;
using namespace SANS::BLA;
using namespace SANS;

//Explicitly instantiate templates to get correct coverage information
namespace SANS
{
namespace BLA
{
}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( BlockLinAlg_Dense )

static const int L = 1;
static const int M = 3;
static const int N = 2;

typedef DLA::MatrixD<DLA::MatrixD<Real                  >> Matrix00;
typedef DLA::MatrixD<DLA::MatrixD<DLA::MatrixS<1,M,Real>>> Matrix01;
typedef DLA::MatrixD<DLA::MatrixD<DLA::MatrixS<1,N,Real>>> Matrix02;

typedef DLA::MatrixD<DLA::MatrixD<DLA::MatrixS<M,L,Real>>> Matrix10;
typedef DLA::MatrixD<DLA::MatrixD<DLA::MatrixS<M,M,Real>>> Matrix11;
typedef DLA::MatrixD<DLA::MatrixD<DLA::MatrixS<M,N,Real>>> Matrix12;

typedef DLA::MatrixD<DLA::MatrixD<DLA::MatrixS<N,L,Real>>> Matrix20;
typedef DLA::MatrixD<DLA::MatrixD<DLA::MatrixS<N,M,Real>>> Matrix21;
typedef DLA::MatrixD<DLA::MatrixD<DLA::MatrixS<N,N,Real>>> Matrix22;

typedef MatrixBlock_3x3<Matrix00, Matrix01, Matrix02,
                        Matrix10, Matrix11, Matrix12,
                        Matrix20, Matrix21, Matrix22> BlockMatrixType;

typedef DLA::MatrixD<DLA::DenseNonZeroPattern<Real                  >> NZ00;
typedef DLA::MatrixD<DLA::DenseNonZeroPattern<DLA::MatrixS<1,M,Real>>> NZ01;
typedef DLA::MatrixD<DLA::DenseNonZeroPattern<DLA::MatrixS<1,N,Real>>> NZ02;

typedef DLA::MatrixD<DLA::DenseNonZeroPattern<DLA::MatrixS<M,L,Real>>> NZ10;
typedef DLA::MatrixD<DLA::DenseNonZeroPattern<DLA::MatrixS<M,M,Real>>> NZ11;
typedef DLA::MatrixD<DLA::DenseNonZeroPattern<DLA::MatrixS<M,N,Real>>> NZ12;

typedef DLA::MatrixD<DLA::DenseNonZeroPattern<DLA::MatrixS<N,L,Real>>> NZ20;
typedef DLA::MatrixD<DLA::DenseNonZeroPattern<DLA::MatrixS<N,M,Real>>> NZ21;
typedef DLA::MatrixD<DLA::DenseNonZeroPattern<DLA::MatrixS<N,N,Real>>> NZ22;

typedef MatrixBlock_3x3<NZ00, NZ01, NZ02,
                        NZ10, NZ11, NZ12,
                        NZ20, NZ21, NZ22> BlockMatrixNZType;

typedef DLA::VectorD<DLA::VectorD<Real                >> Vector0;
typedef DLA::VectorD<DLA::VectorD<DLA::VectorS<M,Real>>> Vector1;
typedef DLA::VectorD<DLA::VectorD<DLA::VectorS<N,Real>>> Vector2;

typedef VectorBlock_3< Vector0, Vector1, Vector2 > BlockVectorType;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_3x3_Dense_ctor )
{
  const int m = 3;
  const int n = 4;
  const int k = 2;

  NZ00 nz00 = {{ {m,m} }};
  NZ01 nz01 = {{ {m,n} }};
  NZ02 nz02 = {{ {m,k} }};

  NZ10 nz10 = {{ {n,m} }};
  NZ11 nz11 = {{ {n,n} }};
  NZ12 nz12 = {{ {n,k} }};

  NZ20 nz20 = {{ {k,m} }};
  NZ21 nz21 = {{ {k,n} }};
  NZ22 nz22 = {{ {k,k} }};

  BlockMatrixNZType nz( nz00, nz01, nz02,
                        nz10, nz11, nz12,
                        nz20, nz21, nz22);

  BOOST_CHECK_EQUAL( 3, nz.m() );
  BOOST_CHECK_EQUAL( 3, nz.n() );

  BOOST_REQUIRE_EQUAL( 1, nz.m00.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m00.n() );
  BOOST_REQUIRE_EQUAL( 1, nz.m01.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m01.n() );
  BOOST_REQUIRE_EQUAL( 1, nz.m02.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m02.n() );

  BOOST_REQUIRE_EQUAL( 1, nz.m10.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m10.n() );
  BOOST_REQUIRE_EQUAL( 1, nz.m11.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m11.n() );
  BOOST_REQUIRE_EQUAL( 1, nz.m12.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m12.n() );

  BOOST_REQUIRE_EQUAL( 1, nz.m20.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m20.n() );
  BOOST_REQUIRE_EQUAL( 1, nz.m21.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m21.n() );
  BOOST_REQUIRE_EQUAL( 1, nz.m22.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m22.n() );

  BlockMatrixType A(nz);

#ifndef __clang_analyzer__
  //Initialize as a '1D heat equation' matrix and check that is correct
  Heat1D< Real >::init(A.m00(0,0));
  Heat1D< DLA::MatrixS<1,M,Real> >::init(A.m01(0,0));
  Heat1D< DLA::MatrixS<1,N,Real> >::init(A.m02(0,0));

  Heat1D< DLA::MatrixS<M,L,Real> >::init(A.m10(0,0));
  Heat1D< DLA::MatrixS<M,M,Real> >::init(A.m11(0,0));
  Heat1D< DLA::MatrixS<M,N,Real> >::init(A.m12(0,0));

  Heat1D< DLA::MatrixS<N,L,Real> >::init(A.m20(0,0));
  Heat1D< DLA::MatrixS<N,M,Real> >::init(A.m21(0,0));
  Heat1D< DLA::MatrixS<N,N,Real> >::init(A.m22(0,0));
#endif

  BlockVectorType x( { DenseVectorSize(m) },
                     { DenseVectorSize(n) },
                     { DenseVectorSize(k) });

  BOOST_CHECK_EQUAL( 3, x.m() );
  BOOST_CHECK_EQUAL( 1, x.n() );

#if 0
  ScalarMatrix_CRS<int> a(A);
  std::fstream file("tmp/block_3x3.mtx", std::ios::out);
  a.WriteMatrixMarketFile(file);
#endif
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_3x3_Dense_MulVec )
{
  const int m = 3;
  const int n = 4;
  const int k = 2;

  BlockVectorType x( { DenseVectorSize(m) },
                     { DenseVectorSize(n) },
                     { DenseVectorSize(k) });

  BlockVectorType b( { DenseVectorSize(m) },
                     { DenseVectorSize(n) },
                     { DenseVectorSize(k) });

  BlockVectorType c( { DenseVectorSize(m) },
                     { DenseVectorSize(n+1)}, //<- Wrong size on purpose
                     { DenseVectorSize(k) });

  NZ00 nz00 = {{ {m,m} }};
  NZ01 nz01 = {{ {m,n} }};
  NZ02 nz02 = {{ {m,k} }};

  NZ10 nz10 = {{ {n,m} }};
  NZ11 nz11 = {{ {n,n} }};
  NZ12 nz12 = {{ {n,k} }};

  NZ20 nz20 = {{ {k,m} }};
  NZ21 nz21 = {{ {k,n} }};
  NZ22 nz22 = {{ {k,k} }};

  BlockMatrixNZType nz( nz00, nz01, nz02,
                        nz10, nz11, nz12,
                        nz20, nz21, nz22);

  BOOST_REQUIRE_EQUAL( 1, nz.m00.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m00.n() );
  BOOST_REQUIRE_EQUAL( 1, nz.m01.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m01.n() );
  BOOST_REQUIRE_EQUAL( 1, nz.m02.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m02.n() );

  BOOST_REQUIRE_EQUAL( 1, nz.m10.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m10.n() );
  BOOST_REQUIRE_EQUAL( 1, nz.m11.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m11.n() );
  BOOST_REQUIRE_EQUAL( 1, nz.m12.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m12.n() );

  BOOST_REQUIRE_EQUAL( 1, nz.m20.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m20.n() );
  BOOST_REQUIRE_EQUAL( 1, nz.m21.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m21.n() );
  BOOST_REQUIRE_EQUAL( 1, nz.m22.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m22.n() );

  BlockMatrixType A(nz);

#ifndef __clang_analyzer__
  //Initialize as a '1D heat equation' matrix and check that is correct
  Heat1D< Real >::init(A.m00(0,0));
  Heat1D< DLA::MatrixS<1,M,Real> >::init(A.m01(0,0));
  Heat1D< DLA::MatrixS<1,N,Real> >::init(A.m02(0,0));

  Heat1D< DLA::MatrixS<M,L,Real> >::init(A.m10(0,0));
  Heat1D< DLA::MatrixS<M,M,Real> >::init(A.m11(0,0));
  Heat1D< DLA::MatrixS<M,N,Real> >::init(A.m12(0,0));

  Heat1D< DLA::MatrixS<N,L,Real> >::init(A.m20(0,0));
  Heat1D< DLA::MatrixS<N,M,Real> >::init(A.m21(0,0));
  Heat1D< DLA::MatrixS<N,N,Real> >::init(A.m22(0,0));
#endif

  x = 1;
  x.v0[0][2] = 2;
  x.v1 = 3;
  x.v2 = 4;
  x.v2[0][1] = 5;

  // bTrue was constructed by dumping the scalar matrix above, printing x,
  // and then multuplying those with mathematica
  Real bTrue[] = {7, 5, 1, 7, 6, 3, 5, 6, 0, -2, -5, 0, 1, 3, 3, 7, 6, 10, 9};
  //std::cout << x.v0 << ", " << x.v1 << ", " << x.v2 << std::endl;

  b = A*x;
  int t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(bTrue[t++], b.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(bTrue[t++], b.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(bTrue[t++], b.v2[k][j][i]);


  b = +(A*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(bTrue[t++], b.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(bTrue[t++], b.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(bTrue[t++], b.v2[k][j][i]);


  b += A*x;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(2*bTrue[t++], b.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(2*bTrue[t++], b.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(2*bTrue[t++], b.v2[k][j][i]);


  b = -(A*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(-bTrue[t++], b.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(-bTrue[t++], b.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(-bTrue[t++], b.v2[k][j][i]);


  b = -A*x;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(-bTrue[t++], b.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(-bTrue[t++], b.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(-bTrue[t++], b.v2[k][j][i]);


  b = 2*(A*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(2*bTrue[t++], b.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(2*bTrue[t++], b.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(2*bTrue[t++], b.v2[k][j][i]);


  b = +(2*(A*x));
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(2*bTrue[t++], b.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(2*bTrue[t++], b.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(2*bTrue[t++], b.v2[k][j][i]);


  b += 2*(A*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(4*bTrue[t++], b.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(4*bTrue[t++], b.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(4*bTrue[t++], b.v2[k][j][i]);


  b = -(2*(A*x));
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(-2*bTrue[t++], b.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(-2*bTrue[t++], b.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(-2*bTrue[t++], b.v2[k][j][i]);


  b = (A*x)*2;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(2*bTrue[t++], b.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(2*bTrue[t++], b.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(2*bTrue[t++], b.v2[k][j][i]);


  b = (A*x)/2;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(bTrue[t++]/2, b.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(bTrue[t++]/2, b.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(bTrue[t++]/2, b.v2[k][j][i]);


  b = (2*(A*x))*2;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(4*bTrue[t++], b.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(4*bTrue[t++], b.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(4*bTrue[t++], b.v2[k][j][i]);


  b = 2*((A*x)*2);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(4*bTrue[t++], b.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(4*bTrue[t++], b.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(4*bTrue[t++], b.v2[k][j][i]);


  b = (2*(A*x))/2;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(bTrue[t++], b.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(bTrue[t++], b.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(bTrue[t++], b.v2[k][j][i]);


  b = (2*A)*x;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(2*bTrue[t++], b.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(2*bTrue[t++], b.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(2*bTrue[t++], b.v2[k][j][i]);


  b = +((2*A)*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(2*bTrue[t++], b.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(2*bTrue[t++], b.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(2*bTrue[t++], b.v2[k][j][i]);


  b += (2*A)*x;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(4*bTrue[t++], b.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(4*bTrue[t++], b.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(4*bTrue[t++], b.v2[k][j][i]);


  b = A*(x*2);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(2*bTrue[t++], b.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(2*bTrue[t++], b.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(2*bTrue[t++], b.v2[k][j][i]);


  b = +(A*(x*2));
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(2*bTrue[t++], b.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(2*bTrue[t++], b.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(2*bTrue[t++], b.v2[k][j][i]);


  b += A*(x*2);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(4*bTrue[t++], b.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(4*bTrue[t++], b.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(4*bTrue[t++], b.v2[k][j][i]);


  BOOST_CHECK_THROW( b = A*c, AssertionException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_3x3_Dense_Expression )
{
  const int m = 3;
  const int n = 4;
  const int k = 2;

  BlockVectorType x( { DenseVectorSize(m) },
                     { DenseVectorSize(n) },
                     { DenseVectorSize(k) });

  BlockVectorType b( { DenseVectorSize(m) },
                     { DenseVectorSize(n) },
                     { DenseVectorSize(k) });

  BlockVectorType r( { DenseVectorSize(m) },
                     { DenseVectorSize(n) },
                     { DenseVectorSize(k) });

  NZ00 nz00 = {{ {m,m} }};
  NZ01 nz01 = {{ {m,n} }};
  NZ02 nz02 = {{ {m,k} }};

  NZ10 nz10 = {{ {n,m} }};
  NZ11 nz11 = {{ {n,n} }};
  NZ12 nz12 = {{ {n,k} }};

  NZ20 nz20 = {{ {k,m} }};
  NZ21 nz21 = {{ {k,n} }};
  NZ22 nz22 = {{ {k,k} }};

  BlockMatrixNZType nz( nz00, nz01, nz02,
                        nz10, nz11, nz12,
                        nz20, nz21, nz22);

  BOOST_REQUIRE_EQUAL( 1, nz.m00.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m00.n() );
  BOOST_REQUIRE_EQUAL( 1, nz.m01.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m01.n() );
  BOOST_REQUIRE_EQUAL( 1, nz.m02.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m02.n() );

  BOOST_REQUIRE_EQUAL( 1, nz.m10.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m10.n() );
  BOOST_REQUIRE_EQUAL( 1, nz.m11.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m11.n() );
  BOOST_REQUIRE_EQUAL( 1, nz.m12.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m12.n() );

  BOOST_REQUIRE_EQUAL( 1, nz.m20.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m20.n() );
  BOOST_REQUIRE_EQUAL( 1, nz.m21.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m21.n() );
  BOOST_REQUIRE_EQUAL( 1, nz.m22.m() ); BOOST_REQUIRE_EQUAL( 1, nz.m22.n() );

  BlockMatrixType A(nz);

  BOOST_REQUIRE_EQUAL( 1, A.m00.m() ); BOOST_REQUIRE_EQUAL( 1, A.m00.n() );
  BOOST_REQUIRE_EQUAL( 1, A.m01.m() ); BOOST_REQUIRE_EQUAL( 1, A.m01.n() );
  BOOST_REQUIRE_EQUAL( 1, A.m02.m() ); BOOST_REQUIRE_EQUAL( 1, A.m02.n() );

  BOOST_REQUIRE_EQUAL( 1, A.m10.m() ); BOOST_REQUIRE_EQUAL( 1, A.m10.n() );
  BOOST_REQUIRE_EQUAL( 1, A.m11.m() ); BOOST_REQUIRE_EQUAL( 1, A.m11.n() );
  BOOST_REQUIRE_EQUAL( 1, A.m12.m() ); BOOST_REQUIRE_EQUAL( 1, A.m12.n() );

  BOOST_REQUIRE_EQUAL( 1, A.m20.m() ); BOOST_REQUIRE_EQUAL( 1, A.m20.n() );
  BOOST_REQUIRE_EQUAL( 1, A.m21.m() ); BOOST_REQUIRE_EQUAL( 1, A.m21.n() );
  BOOST_REQUIRE_EQUAL( 1, A.m22.m() ); BOOST_REQUIRE_EQUAL( 1, A.m22.n() );

#ifndef __clang_analyzer__
  //Initialize as a '1D heat equation' matrix and check that is correct
  Heat1D< Real >::init(A.m00(0,0));
  Heat1D< DLA::MatrixS<1,M,Real> >::init(A.m01(0,0));
  Heat1D< DLA::MatrixS<1,N,Real> >::init(A.m02(0,0));

  Heat1D< DLA::MatrixS<M,L,Real> >::init(A.m10(0,0));
  Heat1D< DLA::MatrixS<M,M,Real> >::init(A.m11(0,0));
  Heat1D< DLA::MatrixS<M,N,Real> >::init(A.m12(0,0));

  Heat1D< DLA::MatrixS<N,L,Real> >::init(A.m20(0,0));
  Heat1D< DLA::MatrixS<N,M,Real> >::init(A.m21(0,0));
  Heat1D< DLA::MatrixS<N,N,Real> >::init(A.m22(0,0));
#endif

  x = 1;
  x.v0[0][2] = 2;
  x.v1 = 3;
  x.v2 = 4;
  x.v2[0][1] = 5;

  b = 4;
  b.v0[0][2] = 2;
  b.v1 = 3;
  b.v2 = 5;
  b.v2[0][1] = 4;

  // bTrue was constructed by dumping the scalar matrix above, printing x,
  // and then multuplying those with mathematica
  Real AxTrue[] = {7, 5, 1, 7, 6, 3, 5, 6, 0, -2, -5, 0, 1, 3, 3, 7, 6, 10, 9};
  //std::cout << x.v0 << ", " << x.v1 << ", " << x.v2 << std::endl;


  r = b + A*x;
  int t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(b.v0[k][j] + AxTrue[t++], r.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(b.v1[k][j][i] + AxTrue[t++], r.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(b.v2[k][j][i] + AxTrue[t++], r.v2[k][j][i]);


  r = +(b + A*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(b.v0[k][j] + AxTrue[t++], r.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(b.v1[k][j][i] + AxTrue[t++], r.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(b.v2[k][j][i] + AxTrue[t++], r.v2[k][j][i]);


  r += b + A*x;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(2*(b.v0[k][j] + AxTrue[t++]), r.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(2*(b.v1[k][j][i] + AxTrue[t++]), r.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(2*(b.v2[k][j][i] + AxTrue[t++]), r.v2[k][j][i]);


  r = b - A*x;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(b.v0[k][j] - AxTrue[t++], r.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(b.v1[k][j][i] - AxTrue[t++], r.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(b.v2[k][j][i] - AxTrue[t++], r.v2[k][j][i]);


  r = +(b - A*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(b.v0[k][j] - AxTrue[t++], r.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(b.v1[k][j][i] - AxTrue[t++], r.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(b.v2[k][j][i] - AxTrue[t++], r.v2[k][j][i]);


  r += b - A*x;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(2*(b.v0[k][j] - AxTrue[t++]), r.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(2*(b.v1[k][j][i] - AxTrue[t++]), r.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(2*(b.v2[k][j][i] - AxTrue[t++]), r.v2[k][j][i]);


  r = -A*x + b;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(b.v0[k][j] - AxTrue[t++], r.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(b.v1[k][j][i] - AxTrue[t++], r.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(b.v2[k][j][i] - AxTrue[t++], r.v2[k][j][i]);


  r = 2*(b + A*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(2*(b.v0[k][j] + AxTrue[t++]), r.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(2*(b.v1[k][j][i] + AxTrue[t++]), r.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(2*(b.v2[k][j][i] + AxTrue[t++]), r.v2[k][j][i]);


  r = (b + A*x)*2;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      BOOST_CHECK_EQUAL(2*(b.v0[k][j] + AxTrue[t++]), r.v0[k][j]);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M; i++ )
        BOOST_CHECK_EQUAL(2*(b.v1[k][j][i] + AxTrue[t++]), r.v1[k][j][i]);

  for ( int k = 0; k < b.v2.m(); k++ )
    for ( int j = 0; j < b.v2[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        BOOST_CHECK_EQUAL(2*(b.v2[k][j][i] + AxTrue[t++]), r.v2[k][j][i]);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
