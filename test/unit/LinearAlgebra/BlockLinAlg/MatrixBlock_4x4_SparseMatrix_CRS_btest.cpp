// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "SANS_btest.h"

#include <fstream>

#include "tools/SANSnumerics.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "LinearAlgebra/SparseLinAlg/ScalarMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_4x4.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_4.h"

#include "unit/LinearAlgebra/TriDiagPattern_btest.h"
#include "unit/LinearAlgebra/Heat1D_btest.h"

using namespace SANS::BLA;
using namespace SANS::DLA;
using namespace SANS::SLA;
using namespace SANS;

//Explicitly instantiate templates to get correct coverage information
namespace SANS
{
namespace BLA
{
}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( BlockLinAlg_Sparse )

//----------------------------------------------------------------------------//
// Utility stuff

static const int M0 = 4, M1 = 3, M2 = 2, M3 = 1;
static const int N0 = 4, N1 = 3, N2 = 2, N3 = 1;

typedef MatrixD<SparseMatrix_CRS<MatrixS<M0,N0,Real>>> Mblock00;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M0,N1,Real>>> Mblock01;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M0,N2,Real>>> Mblock02;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M0,N3,Real>>> Mblock03;

typedef MatrixD<SparseMatrix_CRS<MatrixS<M1,N0,Real>>> Mblock10;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M1,N1,Real>>> Mblock11;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M1,N2,Real>>> Mblock12;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M1,N3,Real>>> Mblock13;

typedef MatrixD<SparseMatrix_CRS<MatrixS<M2,N0,Real>>> Mblock20;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M2,N1,Real>>> Mblock21;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M2,N2,Real>>> Mblock22;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M2,N3,Real>>> Mblock23;

typedef MatrixD<SparseMatrix_CRS<MatrixS<M3,N0,Real>>> Mblock30;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M3,N1,Real>>> Mblock31;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M3,N2,Real>>> Mblock32;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M3,N3,Real>>> Mblock33;

typedef MatrixD<SparseNonZeroPattern<MatrixS<M0,N0,Real>>> MblockNZ00;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M0,N1,Real>>> MblockNZ01;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M0,N2,Real>>> MblockNZ02;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M0,N3,Real>>> MblockNZ03;

typedef MatrixD<SparseNonZeroPattern<MatrixS<M1,N0,Real>>> MblockNZ10;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M1,N1,Real>>> MblockNZ11;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M1,N2,Real>>> MblockNZ12;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M1,N3,Real>>> MblockNZ13;

typedef MatrixD<SparseNonZeroPattern<MatrixS<M2,N0,Real>>> MblockNZ20;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M2,N1,Real>>> MblockNZ21;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M2,N2,Real>>> MblockNZ22;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M2,N3,Real>>> MblockNZ23;

typedef MatrixD<SparseNonZeroPattern<MatrixS<M3,N0,Real>>> MblockNZ30;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M3,N1,Real>>> MblockNZ31;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M3,N2,Real>>> MblockNZ32;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M3,N3,Real>>> MblockNZ33;

typedef MatrixBlock_4x4<Mblock00, Mblock01, Mblock02, Mblock03,
                        Mblock10, Mblock11, Mblock12, Mblock13,
                        Mblock20, Mblock21, Mblock22, Mblock23,
                        Mblock30, Mblock31, Mblock32, Mblock33> BlockMatrixType;

typedef MatrixBlock_4x4<MblockNZ00, MblockNZ01, MblockNZ02, MblockNZ03,
                        MblockNZ10, MblockNZ11, MblockNZ12, MblockNZ13,
                        MblockNZ20, MblockNZ21, MblockNZ22, MblockNZ23,
                        MblockNZ30, MblockNZ31, MblockNZ32, MblockNZ33> BlockMatrixNZType;

typedef VectorBlock_4< VectorD<SparseVector<VectorS<N0,Real>>>,
                       VectorD<SparseVector<VectorS<N1,Real>>>,
                       VectorD<SparseVector<VectorS<N2,Real>>>,
                       VectorD<SparseVector<VectorS<N3,Real>>> > BlockVectorType;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_4x4_Sparse_ctor_test )
{
  const int m0 = 4, m1 = 3, m2 = 2, m3 = 1;

#ifdef __INTEL_COMPILER
  // intel compiler just can't do initializer lists generically... sigh...
  MblockNZ00 nz00 = {{ {m0,m0} }}; MblockNZ01 nz01 = {{ {m0,m1} }}; MblockNZ02 nz02 = {{ {m0,m2} }}; MblockNZ03 nz03 = {{ {m0,m3} }};
  MblockNZ10 nz10 = {{ {m1,m0} }}; MblockNZ11 nz11 = {{ {m1,m1} }}; MblockNZ12 nz12 = {{ {m1,m2} }}; MblockNZ13 nz13 = {{ {m1,m3} }};
  MblockNZ20 nz20 = {{ {m2,m0} }}; MblockNZ21 nz21 = {{ {m2,m1} }}; MblockNZ22 nz22 = {{ {m2,m2} }}; MblockNZ23 nz23 = {{ {m2,m3} }};
  MblockNZ30 nz30 = {{ {m3,m0} }}; MblockNZ31 nz31 = {{ {m3,m1} }}; MblockNZ32 nz32 = {{ {m3,m2} }}; MblockNZ33 nz33 = {{ {m3,m3} }};

  BlockMatrixNZType nz( nz00, nz01, nz02, nz03,
                        nz10, nz11, nz12, nz13,
                        nz20, nz21, nz22, nz23,
                        nz30, nz31, nz32, nz33);
#else
  // each block component is an initializer list of initializer list
  BlockMatrixNZType nz( {{ SparseMatrixSize(m0,m0) }}, {{ SparseMatrixSize(m0,m1) }}, {{ SparseMatrixSize(m0,m2) }}, {{ SparseMatrixSize(m0,m3) }},
                        {{ SparseMatrixSize(m1,m0) }}, {{ SparseMatrixSize(m1,m1) }}, {{ SparseMatrixSize(m1,m2) }}, {{ SparseMatrixSize(m1,m3) }},
                        {{ SparseMatrixSize(m2,m0) }}, {{ SparseMatrixSize(m2,m1) }}, {{ SparseMatrixSize(m2,m2) }}, {{ SparseMatrixSize(m2,m3) }},
                        {{ SparseMatrixSize(m3,m0) }}, {{ SparseMatrixSize(m3,m1) }}, {{ SparseMatrixSize(m3,m2) }}, {{ SparseMatrixSize(m3,m3) }} );
#endif

  BOOST_CHECK_EQUAL( 4, nz.m() );
  BOOST_CHECK_EQUAL( 4, nz.n() );

  TriDiagPattern(nz.m00(0,0)); TriDiagPattern(nz.m01(0,0)); TriDiagPattern(nz.m02(0,0)); TriDiagPattern(nz.m03(0,0));
  TriDiagPattern(nz.m10(0,0)); TriDiagPattern(nz.m11(0,0)); TriDiagPattern(nz.m12(0,0)); TriDiagPattern(nz.m13(0,0));
  TriDiagPattern(nz.m20(0,0)); TriDiagPattern(nz.m21(0,0)); TriDiagPattern(nz.m22(0,0)); TriDiagPattern(nz.m23(0,0));
  TriDiagPattern(nz.m30(0,0)); TriDiagPattern(nz.m31(0,0)); TriDiagPattern(nz.m32(0,0)); TriDiagPattern(nz.m33(0,0));

  BlockMatrixType A(nz);

  DLA::Identity I;
  A = I;

//  //Initialize as a '1D heat equation' matrix and check that is correct
  Heat1D< MatrixS<M0,N0,Real> >::init(A.m00(0,0));
  Heat1D< MatrixS<M0,N1,Real> >::init(A.m01(0,0));
  Heat1D< MatrixS<M0,N2,Real> >::init(A.m02(0,0));
//  Heat1D< MatrixS<M0,N3,Real> >::init(A.m03(0,0));

  Heat1D< MatrixS<M1,N0,Real> >::init(A.m10(0,0));
  Heat1D< MatrixS<M1,N1,Real> >::init(A.m11(0,0));
  Heat1D< MatrixS<M1,N2,Real> >::init(A.m12(0,0));
//  Heat1D< MatrixS<M1,N3,Real> >::init(A.m13(0,0));

  Heat1D< MatrixS<M2,N0,Real> >::init(A.m20(0,0));
  Heat1D< MatrixS<M2,N1,Real> >::init(A.m21(0,0));
  Heat1D< MatrixS<M2,N2,Real> >::init(A.m22(0,0));
//  Heat1D< MatrixS<M2,N3,Real> >::init(A.m23(0,0));

//  Heat1D< MatrixS<M3,N0,Real> >::init(A.m30(0,0));
//  Heat1D< MatrixS<M3,N1,Real> >::init(A.m31(0,0));
//  Heat1D< MatrixS<M3,N2,Real> >::init(A.m32(0,0));
//  Heat1D< MatrixS<M3,N3,Real> >::init(A.m33(0,0));

  // check sizes
  BOOST_CHECK_EQUAL( 1, A.m00.m() );  BOOST_CHECK_EQUAL( 1, A.m00.n() );
  BOOST_CHECK_EQUAL( 1, A.m11.m() );  BOOST_CHECK_EQUAL( 1, A.m11.n() );
  BOOST_CHECK_EQUAL( 1, A.m22.m() );  BOOST_CHECK_EQUAL( 1, A.m22.n() );
  BOOST_CHECK_EQUAL( 1, A.m33.m() );  BOOST_CHECK_EQUAL( 1, A.m33.n() );

  BOOST_CHECK_EQUAL( m0, A.m00(0,0).m() );  BOOST_CHECK_EQUAL( m0, A.m00(0,0).n() );
  BOOST_CHECK_EQUAL( m0, A.m01(0,0).m() );  BOOST_CHECK_EQUAL( m1, A.m01(0,0).n() );
  BOOST_CHECK_EQUAL( m0, A.m02(0,0).m() );  BOOST_CHECK_EQUAL( m2, A.m02(0,0).n() );
  BOOST_CHECK_EQUAL( m0, A.m03(0,0).m() );  BOOST_CHECK_EQUAL( m3, A.m03(0,0).n() );

  BOOST_CHECK_EQUAL( m1, A.m10(0,0).m() );  BOOST_CHECK_EQUAL( m0, A.m10(0,0).n() );
  BOOST_CHECK_EQUAL( m1, A.m11(0,0).m() );  BOOST_CHECK_EQUAL( m1, A.m11(0,0).n() );
  BOOST_CHECK_EQUAL( m1, A.m12(0,0).m() );  BOOST_CHECK_EQUAL( m2, A.m12(0,0).n() );
  BOOST_CHECK_EQUAL( m1, A.m13(0,0).m() );  BOOST_CHECK_EQUAL( m3, A.m13(0,0).n() );

  BOOST_CHECK_EQUAL( m2, A.m20(0,0).m() );  BOOST_CHECK_EQUAL( m0, A.m20(0,0).n() );
  BOOST_CHECK_EQUAL( m2, A.m21(0,0).m() );  BOOST_CHECK_EQUAL( m1, A.m21(0,0).n() );
  BOOST_CHECK_EQUAL( m2, A.m22(0,0).m() );  BOOST_CHECK_EQUAL( m2, A.m22(0,0).n() );
  BOOST_CHECK_EQUAL( m2, A.m23(0,0).m() );  BOOST_CHECK_EQUAL( m3, A.m23(0,0).n() );

  BOOST_CHECK_EQUAL( m3, A.m30(0,0).m() );  BOOST_CHECK_EQUAL( m0, A.m30(0,0).n() );
  BOOST_CHECK_EQUAL( m3, A.m31(0,0).m() );  BOOST_CHECK_EQUAL( m1, A.m31(0,0).n() );
  BOOST_CHECK_EQUAL( m3, A.m32(0,0).m() );  BOOST_CHECK_EQUAL( m2, A.m32(0,0).n() );
  BOOST_CHECK_EQUAL( m3, A.m33(0,0).m() );  BOOST_CHECK_EQUAL( m3, A.m33(0,0).n() );

  // write matrix to file  & check pattern
//  std::string filename_NonZeroPattern("IO/BlockLinAlg/MatrixBlock4x4_MatrixD_SparseNonZeroPattern_MatrixS.mm");
//  //Set the 2nd argument to false to regenerate the file
//  output_test_stream output_NonZeroPattern( filename_NonZeroPattern, true );
//  WriteMatrixMarketFile(nz, output_NonZeroPattern);
//  BOOST_CHECK( output_NonZeroPattern.match_pattern() );

  std::string filename_Matrix("IO/BlockLinAlg/MatrixBlock4x4_MatrixD_SparseMatrix_CRS_MatrixS.mm");
  //Set the 2nd argument to false to regenerate the file
  output_test_stream output_Matrix( filename_Matrix, true );
  WriteMatrixMarketFile(A, output_Matrix);
  BOOST_CHECK( output_Matrix.match_pattern() );

  // assignment operators: =
  {
    BlockMatrixType B(nz);
    B = 1;

    std::string filename_Matrix("IO/BlockLinAlg/MatrixBlock4x4_MatrixD_SparseMatrix_CRS_MatrixS_one.mm");
    //Set the 2nd argument to false to regenerate the file
    output_test_stream output_Matrix( filename_Matrix, true );
    WriteMatrixMarketFile(B, output_Matrix);
    BOOST_CHECK( output_Matrix.match_pattern() );
  }

  {
    BlockMatrixType B(nz);
    B = I;

    std::string filename_Matrix("IO/BlockLinAlg/MatrixBlock4x4_MatrixD_SparseMatrix_CRS_MatrixS_diag.mm");
    //Set the 2nd argument to false to regenerate the file
    output_test_stream output_Matrix( filename_Matrix, true );
    WriteMatrixMarketFile(B, output_Matrix);
    BOOST_CHECK( output_Matrix.match_pattern() );
  }
}

#if 0 // TODO
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_4x4_Sparse_MulVec )
{

}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
