// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/SingularException.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
//#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2_Decompose_LUP.h"

#include <iostream>
using namespace SANS::DLA;
using namespace SANS::BLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BlockLinAlg_Dense )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Dense_Decompose_LUP_Exceptions )
{
  typedef Real T;

  typedef MatrixBlock_2x2< MatrixD<T>, MatrixD<T>,
                           MatrixD<T>, MatrixD<T> > BlockMatrixType;

  const int m = 2;
  const int n = 3;
  MatrixD<T> A00(m,m), A01(m,n), A10(n,m), A11(n,n);
  MatrixD<T> LU00(m,m), LU01(m,n), LU10(n,m), LU11(n,n);

  BlockMatrixType A(A00, A01, A10, A11);
  BlockMatrixType LU(LU00, LU01, LU10, LU11);

  A = 0;
  BOOST_CHECK_THROW( Decompose_LUP(A, LU), SingularMatrixException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Dense_Decompose_LUP_m1n1 )
{
  typedef Real T;

  typedef MatrixBlock_2x2< MatrixD<T>, MatrixD<T>,
                           MatrixD<T>, MatrixD<T> > BlockMatrixType;

  const int m = 1;
  const int n = 1;
  MatrixD<T> A00(m,m), A01(m,n), A10(n,m), A11(n,n);
  MatrixD<T> LU00(m,m), LU01(m,n), LU10(n,m), LU11(n,n);

  A00 = 2;
  A01 = 1;
  A10 = 3;
  A11 = 2;

  BlockMatrixType A(A00, A01, A10, A11);
  BlockMatrixType LU(LU00, LU01, LU10, LU11);

  Decompose_LUP(A, LU);

  BOOST_CHECK_CLOSE( A.m00(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A.m01(0,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A.m10(0,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A.m11(0,0), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( LU.m00(0,0),    2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m01(0,0), 1./2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m10(0,0),    3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m11(0,0), 1./2., T(0.0001) );

  LU = 0;

  // cppcheck-suppress redundantAssignment
  LU = Decompose_LUP(A);

  BOOST_CHECK_CLOSE( A.m00(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A.m01(0,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A.m10(0,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A.m11(0,0), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( LU.m00(0,0),    2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m01(0,0), 1./2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m10(0,0),    3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m11(0,0), 1./2., T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Dense_Decompose_LUP_m1n1_AExpression )
{
  typedef Real T;

  typedef MatrixBlock_2x2< MatrixD<T>, MatrixD<T>,
                           MatrixD<T>, MatrixD<T> > BlockMatrixType;

  const int m = 1;
  const int n = 1;
  MatrixD<T> A00(m,m), A01(m,n), A10(n,m), A11(n,n);
  MatrixD<T> LU00(m,m), LU01(m,n), LU10(n,m), LU11(n,n);

  A00 = 1;
  A01 = 0;
  A10 = 2;
  A11 = 1;

  BlockMatrixType A1(A00, A01, A10, A11);
  BlockMatrixType A2(A00, A01, A10, A11);
  BlockMatrixType LU(LU00, LU01, LU10, LU11);

  A2 = 1;

  Decompose_LUP(A1 + A2 + A1 - A1, LU);

  BOOST_CHECK_CLOSE( LU.m00(0,0),    2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m01(0,0), 1./2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m10(0,0),    3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m11(0,0), 1./2., T(0.0001) );

  LU = 0;

  // cppcheck-suppress redundantAssignment
  LU = Decompose_LUP(A1 + A2);

  BOOST_CHECK_CLOSE( LU.m00(0,0),    2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m01(0,0), 1./2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m10(0,0),    3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m11(0,0), 1./2., T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Dense_Decompose_LUP_m2n1 )
{
  typedef Real T;

  typedef MatrixBlock_2x2< MatrixD<T>, MatrixD<T>,
                           MatrixD<T>, MatrixD<T> > BlockMatrixType;

//    Real Adata[] = {4, 2, 2,
//                    2, 4, 6,
//                    2, 6, 6};

  const int m = 2;
  const int n = 1;
  MatrixD<T> A00 = {{4,2},{2,4}};
  MatrixD<T> A01 = {{2},{6}};
  MatrixD<T> A10 = {{2,6}};
  MatrixD<T> A11 = {{6}};

  MatrixD<T> LU00(m,m), LU01(m,n), LU10(n,m), LU11(n,n);

  BlockMatrixType A(A00, A01, A10, A11);
  BlockMatrixType LU(LU00, LU01, LU10, LU11);

  Decompose_LUP(A, LU);

  BOOST_CHECK_CLOSE( LU.m00(0,0), T( 4.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m00(0,1), T( 2.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m01(0,0), T(-1./3.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m00(1,0), T( 2.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m00(1,1), T( 4.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m01(1,0), T( 5./3.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m10(0,0), T( 2.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m10(0,1), T( 6.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m11(0,0), T( -10./3. ), T(0.0001) );

  LU = 0;

  // cppcheck-suppress redundantAssignment
  LU = Decompose_LUP(A);

  BOOST_CHECK_CLOSE( LU.m00(0,0), T( 4.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m00(0,1), T( 2.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m01(0,0), T(-1./3.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m00(1,0), T( 2.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m00(1,1), T( 4.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m01(1,0), T( 5./3.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m10(0,0), T( 2.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m10(0,1), T( 6.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU.m11(0,0), T( -10./3. ), T(0.0001) );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
