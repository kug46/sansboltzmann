// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include <fstream>

#include "tools/SANSnumerics.h"
#include "tools/minmax.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"

#include "../Heat1D_btest.h"

using namespace SANS::DLA;
using namespace SANS::BLA;
using namespace SANS;

//Explicitly instantiate templates to get correct coverage information
namespace SANS
{
namespace BLA
{
}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( BlockLinAlg_Dense )

static const int M = 2;
static const int N = 2;

typedef MatrixBlock_2x2<
    DLA::MatrixD<DLA::MatrixD<DLA::MatrixS<M,N,Real>>>, DLA::MatrixD<DLA::MatrixD<DLA::MatrixS<M,1,Real>>>,
    DLA::MatrixD<DLA::MatrixD<DLA::MatrixS<1,N,Real>>>, DLA::MatrixD<DLA::MatrixD<Real>>                  > BlockMatrixType;

typedef MatrixBlock_2x2<
    DLA::MatrixD<DLA::DenseNonZeroPattern<DLA::MatrixS<M,N,Real>>>, DLA::MatrixD<DLA::DenseNonZeroPattern<DLA::MatrixS<M,1,Real>>>,
    DLA::MatrixD<DLA::DenseNonZeroPattern<DLA::MatrixS<1,N,Real>>>, DLA::MatrixD<DLA::DenseNonZeroPattern<Real>>                  > BlockMatrixNZType;

typedef VectorBlock_2< DLA::VectorD<DLA::VectorD<DLA::VectorS<N,Real>>>, DLA::VectorD<DLA::VectorD<Real>> > BlockVectorType;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Dense_ctor )
{

  const int m = 3;
  const int n = 4;

#ifdef __INTEL_COMPILER
  // intel compiler just can't do initializer lists generically... sigh...
  DLA::MatrixD< DLA::DenseNonZeroPattern<DLA::MatrixS<M,N,Real>> > nz00 = {{ {m,m} }};
  DLA::MatrixD< DLA::DenseNonZeroPattern<DLA::MatrixS<M,1,Real>> > nz01 = {{ {m,n} }};

  DLA::MatrixD< DLA::DenseNonZeroPattern<DLA::MatrixS<1,N,Real>> > nz10 = {{ {n,m} }};
  DLA::MatrixD< DLA::DenseNonZeroPattern<Real                  > > nz11 = {{ {n,n} }};

  BlockMatrixNZType nz( nz00, nz01,
                        nz10, nz11 );
#else
  BlockMatrixNZType nz( {{ DenseMatrixSize(m,m) }}, {{ DenseMatrixSize(m,n) }},
                        {{ DenseMatrixSize(n,m) }}, {{ DenseMatrixSize(n,n) }} );
#endif

  BOOST_CHECK_EQUAL( 2, nz.m() );
  BOOST_CHECK_EQUAL( 2, nz.n() );

  BlockMatrixType A(nz);

  //Initialize as a '1D heat equation' matrix and check that is correct
  Heat1D< DLA::MatrixS<M,N,Real> >::init(A.m00(0,0));
  Heat1D< DLA::MatrixS<M,1,Real> >::init(A.m01(0,0));
  Heat1D< DLA::MatrixS<1,N,Real> >::init(A.m10(0,0));
  Heat1D< Real >::init(A.m11(0,0));

  BlockVectorType x( { DenseVectorSize(m) },
                     { DenseVectorSize(n) } );

  BOOST_CHECK_EQUAL( 2, x.m() );
  BOOST_CHECK_EQUAL( 1, x.n() );

#if 0
  ScalarMatrix_CRS<int> a(A);
  std::fstream file("tmp/block_2x2.mtx", std::ios::out);
  a.WriteMatrixMarketFile(file);
#endif
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Dense_MulVec )
{
  const int m = 3;
  const int n = 4;

  BlockVectorType x( { DenseVectorSize(m) },
                     { DenseVectorSize(n) } );

  BlockVectorType b( { DenseVectorSize(m) },
                     { DenseVectorSize(n) } );

  BlockVectorType c( { DenseVectorSize(m) },
                     { DenseVectorSize(n+1) } ); //<- Wrong size on purpose

#ifdef __INTEL_COMPILER
  // intel compiler just can't do initializer lists generically... sigh...
  DLA::MatrixD< DLA::DenseNonZeroPattern<DLA::MatrixS<M,N,Real>> > nz00 = {{ {m,m} }};
  DLA::MatrixD< DLA::DenseNonZeroPattern<DLA::MatrixS<M,1,Real>> > nz01 = {{ {m,n} }};

  DLA::MatrixD< DLA::DenseNonZeroPattern<DLA::MatrixS<1,N,Real>> > nz10 = {{ {n,m} }};
  DLA::MatrixD< DLA::DenseNonZeroPattern<Real                  > > nz11 = {{ {n,n} }};

  BlockMatrixNZType nz( nz00, nz01,
                        nz10, nz11 );
#else
  BlockMatrixNZType nz( {{ DenseMatrixSize(m,m) }}, {{ DenseMatrixSize(m,n) }},
                        {{ DenseMatrixSize(n,m) }}, {{ DenseMatrixSize(n,n) }} );
#endif

  BlockMatrixType A(nz);

  //Initialize as a '1D heat equation' matrix and check that is correct
  Heat1D< DLA::MatrixS<M,N,Real> >::init(A.m00(0,0));
  Heat1D< DLA::MatrixS<M,1,Real> >::init(A.m01(0,0));
  Heat1D< DLA::MatrixS<1,N,Real> >::init(A.m10(0,0));
  Heat1D< Real >::init(A.m11(0,0));

  x = 1;
  x.v0[0][2] = 2;
  x.v1 = 3;

  // bTrue was constructed by dumping the scalar matrix above, printing x,
  // and then multiplying those with mathematica
  Real bTrue[] = {4, 1, -1, -1, 6, 3, 4, -1, 3, 1};
//  std::cout << x.v0 << ", " << x.v1 << std::endl;

  b = A*x;

  int t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(bTrue[t], b.v1[k][j]);
      t++;
    }


  b = +(A*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(bTrue[t], b.v1[k][j]);
      t++;
    }


  b += A*x;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*bTrue[t], b.v1[k][j]);
      t++;
    }


  b = -(A*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(-bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(-bTrue[t], b.v1[k][j]);
      t++;
    }


  b = -A*x;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(-bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(-bTrue[t], b.v1[k][j]);
      t++;
    }


  b = 2*(A*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*bTrue[t], b.v1[k][j]);
      t++;
    }


  b = +(2*(A*x));
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*bTrue[t], b.v1[k][j]);
      t++;
    }


  b += 2*(A*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(4*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(4*bTrue[t], b.v1[k][j]);
      t++;
    }


  b = -(2*(A*x));
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(-2*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(-2*bTrue[t], b.v1[k][j]);
      t++;
    }


  b = (A*x)*2;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*bTrue[t], b.v1[k][j]);
      t++;
    }


  b = (A*x)/2;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(bTrue[t]/2, b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(bTrue[t]/2, b.v1[k][j]);
      t++;
    }


  b = (2*(A*x))*2;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(4*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(4*bTrue[t], b.v1[k][j]);
      t++;
    }


  b = 2*((A*x)*2);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(4*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(4*bTrue[t], b.v1[k][j]);
      t++;
    }


  b = (2*(A*x))/2;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(bTrue[t], b.v1[k][j]);
      t++;
    }


  b = (2*A)*x;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*bTrue[t], b.v1[k][j]);
      t++;
    }

  b = +((2*A)*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*bTrue[t], b.v1[k][j]);
      t++;
    }


  b += (2*A)*x;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(4*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(4*bTrue[t], b.v1[k][j]);
      t++;
    }


  b = A*(x*2);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*bTrue[t], b.v1[k][j]);
      t++;
    }

  b = +(A*(x*2));
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*bTrue[t], b.v1[k][j]);
      t++;
    }

  b += A*(x*2);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(4*bTrue[t], b.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(4*bTrue[t], b.v1[k][j]);
      t++;
    }

  BOOST_CHECK_THROW( b = A*c, AssertionException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Dense_Expression )
{
  const int m = 3;
  const int n = 4;

  BlockVectorType x( { DenseVectorSize(m) },
                     { DenseVectorSize(n) } );

  BlockVectorType b( { DenseVectorSize(m) },
                     { DenseVectorSize(n) } );

  BlockVectorType r( { DenseVectorSize(m) },
                     { DenseVectorSize(n) } );

#ifdef __INTEL_COMPILER
  // intel compiler just can't do initializer lists generically... sigh...
  DLA::MatrixD< DLA::DenseNonZeroPattern<DLA::MatrixS<M,N,Real>> > nz00 = {{ {m,m} }};
  DLA::MatrixD< DLA::DenseNonZeroPattern<DLA::MatrixS<M,1,Real>> > nz01 = {{ {m,n} }};

  DLA::MatrixD< DLA::DenseNonZeroPattern<DLA::MatrixS<1,N,Real>> > nz10 = {{ {n,m} }};
  DLA::MatrixD< DLA::DenseNonZeroPattern<Real                  > > nz11 = {{ {n,n} }};

  BlockMatrixNZType nz( nz00, nz01,
                        nz10, nz11 );
#else
  BlockMatrixNZType nz( {{ DenseMatrixSize(m,m) }}, {{ DenseMatrixSize(m,n) }},
                        {{ DenseMatrixSize(n,m) }}, {{ DenseMatrixSize(n,n) }} );
#endif

  BlockMatrixType A(nz);

  //Initialize as a '1D heat equation' matrix and check that is correct
  Heat1D< DLA::MatrixS<2,2,Real> >::init(A.m00(0,0));
  Heat1D< DLA::MatrixS<2,1,Real> >::init(A.m01(0,0));
  Heat1D< DLA::MatrixS<1,2,Real> >::init(A.m10(0,0));
  Heat1D< Real >::init(A.m11(0,0));

  x = 1;
  x.v0[0][2] = 2;
  x.v1 = 3;

  b = 4;
  b.v0[0][2] = 2;
  b.v1 = 3;

  // bTrue was constructed by dumping the scalar matrix above, printing x,
  // and then multuplying those with mathematica
  Real AxTrue[] = {4, 1, -1, -1, 6, 3, 4, -1, 3, 1};
  //std::cout << x.v0 << ", " << x.v1 << std::endl;

  r = b + A*x;
  int t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(b.v0[k][j][i] + AxTrue[t], r.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(b.v1[k][j] + AxTrue[t], r.v1[k][j]);
      t++;
    }


  r = +(b + A*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(b.v0[k][j][i] + AxTrue[t], r.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(b.v1[k][j] + AxTrue[t], r.v1[k][j]);
      t++;
    }


  r += b + A*x;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*(b.v0[k][j][i] + AxTrue[t]), r.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*(b.v1[k][j] + AxTrue[t]), r.v1[k][j]);
      t++;
    }


  r = b - A*x;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(b.v0[k][j][i] - AxTrue[t], r.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(b.v1[k][j] - AxTrue[t], r.v1[k][j]);
      t++;
    }


  r = +(b - A*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(b.v0[k][j][i] - AxTrue[t], r.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(b.v1[k][j] - AxTrue[t], r.v1[k][j]);
      t++;
    }


  r += b - A*x;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*(b.v0[k][j][i] - AxTrue[t]), r.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*(b.v1[k][j] - AxTrue[t]), r.v1[k][j]);
      t++;
    }


  r = -A*x + b;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(b.v0[k][j][i] - AxTrue[t], r.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(b.v1[k][j] - AxTrue[t], r.v1[k][j]);
      t++;
    }


  r = 2*(b + A*x);
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*(b.v0[k][j][i] + AxTrue[t]), r.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*(b.v1[k][j] + AxTrue[t]), r.v1[k][j]);
      t++;
    }


  r = (b + A*x)*2;
  t = 0;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
      {
        BOOST_CHECK_EQUAL(2*(b.v0[k][j][i] + AxTrue[t]), r.v0[k][j][i]);
        t++;
      }

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
    {
      BOOST_CHECK_EQUAL(2*(b.v1[k][j] + AxTrue[t]), r.v1[k][j]);
      t++;
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Dense_MatMul )
{
  typedef Real T;

  typedef MatrixBlock_2x2< MatrixD<T>, MatrixD<T>,
                           MatrixD<T>, MatrixD<T> > BlockMatrixType;

  MatrixD<T> A00 = {{4,2},{2,4}};
  MatrixD<T> A01 = {{2},{6}};
  MatrixD<T> A10 = {{2,6}};
  MatrixD<T> A11 = {{6}};

  MatrixD<T> B00 = {{4,2,3},{2,4,7}};
  MatrixD<T> B01 = {{4,0},{3,1}};
  MatrixD<T> B10 = {{-2,6,7}};
  MatrixD<T> B11 = {{4,-3}};

  BlockMatrixType A(A00, A01, A10, A11);
  BlockMatrixType B(B00, B01, B10, B11);
  BlockMatrixType C(B00, B01, B10, B11);

  C = A*B;

  MatrixD<T> C00_true = {{16,28,40},{4,56,76}};
  MatrixD<T> C01_true = {{30,-4},{44,-14}};
  MatrixD<T> C10_true = {{8,64,90}};
  MatrixD<T> C11_true = {{50,-12}};

  const T small_tol = 1e-12;
  const T close_tol = 1e-12;

  BOOST_REQUIRE_EQUAL( C.m00.m(), C00_true.m() );
  BOOST_REQUIRE_EQUAL( C.m00.n(), C00_true.n() );
  BOOST_REQUIRE_EQUAL( C.m01.m(), C01_true.m() );
  BOOST_REQUIRE_EQUAL( C.m01.n(), C01_true.n() );
  BOOST_REQUIRE_EQUAL( C.m10.m(), C10_true.m() );
  BOOST_REQUIRE_EQUAL( C.m10.n(), C10_true.n() );
  BOOST_REQUIRE_EQUAL( C.m11.m(), C11_true.m() );
  BOOST_REQUIRE_EQUAL( C.m11.n(), C11_true.n() );

  for (int i = 0; i < C.m00.m(); i++)
    for (int j = 0; j < C.m00.n(); j++)
      SANS_CHECK_CLOSE(C.m00(i,j), C00_true(i,j), small_tol, close_tol);

  for (int i = 0; i < C.m01.m(); i++)
    for (int j = 0; j < C.m01.n(); j++)
      SANS_CHECK_CLOSE(C.m01(i,j), C01_true(i,j), small_tol, close_tol);

  for (int i = 0; i < C.m10.m(); i++)
    for (int j = 0; j < C.m10.n(); j++)
      SANS_CHECK_CLOSE(C.m10(i,j), C10_true(i,j), small_tol, close_tol);

  for (int i = 0; i < C.m11.m(); i++)
    for (int j = 0; j < C.m11.n(); j++)
      SANS_CHECK_CLOSE(C.m11(i,j), C11_true(i,j), small_tol, close_tol);

  C += A*B;

  for (int i = 0; i < C.m00.m(); i++)
    for (int j = 0; j < C.m00.n(); j++)
      SANS_CHECK_CLOSE(C.m00(i,j), 2*C00_true(i,j), small_tol, close_tol);

  for (int i = 0; i < C.m01.m(); i++)
    for (int j = 0; j < C.m01.n(); j++)
      SANS_CHECK_CLOSE(C.m01(i,j), 2*C01_true(i,j), small_tol, close_tol);

  for (int i = 0; i < C.m10.m(); i++)
    for (int j = 0; j < C.m10.n(); j++)
      SANS_CHECK_CLOSE(C.m10(i,j), 2*C10_true(i,j), small_tol, close_tol);

  for (int i = 0; i < C.m11.m(); i++)
    for (int j = 0; j < C.m11.n(); j++)
      SANS_CHECK_CLOSE(C.m11(i,j), 2*C11_true(i,j), small_tol, close_tol);

  C = C - A*B;

  for (int i = 0; i < C.m00.m(); i++)
    for (int j = 0; j < C.m00.n(); j++)
      SANS_CHECK_CLOSE(C.m00(i,j), C00_true(i,j), small_tol, close_tol);

  for (int i = 0; i < C.m01.m(); i++)
    for (int j = 0; j < C.m01.n(); j++)
      SANS_CHECK_CLOSE(C.m01(i,j), C01_true(i,j), small_tol, close_tol);

  for (int i = 0; i < C.m10.m(); i++)
    for (int j = 0; j < C.m10.n(); j++)
      SANS_CHECK_CLOSE(C.m10(i,j), C10_true(i,j), small_tol, close_tol);

  for (int i = 0; i < C.m11.m(); i++)
    for (int j = 0; j < C.m11.n(); j++)
      SANS_CHECK_CLOSE(C.m11(i,j), C11_true(i,j), small_tol, close_tol);

  BOOST_CHECK_THROW( C = A*A, AssertionException );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
