// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2_Transpose.h"

#include <iostream>
using namespace SANS::DLA;
using namespace SANS::BLA;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( BlockLinAlg_Dense )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Dense_Transpose_Exceptions )
{
  typedef Real T;

  typedef MatrixBlock_2x2< MatrixD<T>, MatrixD<T>,
                           MatrixD<T>, MatrixD<T> > BlockMatrixType;

  BlockMatrixType A(MatrixD<T>(2,2), MatrixD<T>(2,3),
                    MatrixD<T>(4,2), MatrixD<T>(4,3));
  A = 0;

  BlockMatrixType B0(MatrixD<T>(2,2), MatrixD<T>(2,3),
                     MatrixD<T>(4,2), MatrixD<T>(4,3));

  BlockMatrixType B1(MatrixD<T>(2,2), MatrixD<T>(4,2),
                     MatrixD<T>(2,3), MatrixD<T>(3,4));

  BOOST_CHECK_THROW( B0 = Transpose(A), AssertionException );
  BOOST_CHECK_THROW( B1 = Transpose(A), AssertionException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Dense_Transpose_MatrixD_m2n1 )
{
  typedef Real T;

  typedef MatrixBlock_2x2< MatrixD<T>, MatrixD<T>,
                           MatrixD<T>, MatrixD<T> > BlockMatrixType;

  MatrixD<T> A00 = {{4,1},{2,3}};
  MatrixD<T> A01 = {{2},{6}};
  MatrixD<T> A10 = {{5,7}};
  MatrixD<T> A11 = {{8}};

  BlockMatrixType A(A00, A01, A10, A11);
  BlockMatrixType B(A00, A01, A10, A11);

  B = Transpose(A);

  BOOST_CHECK_EQUAL( B.m00(0,0), 4 );
  BOOST_CHECK_EQUAL( B.m00(0,1), 2 );
  BOOST_CHECK_EQUAL( B.m00(1,0), 1 );
  BOOST_CHECK_EQUAL( B.m00(1,1), 3 );
  BOOST_CHECK_EQUAL( B.m01(0,0), 5 );
  BOOST_CHECK_EQUAL( B.m01(1,0), 7 );
  BOOST_CHECK_EQUAL( B.m10(0,0), 2 );
  BOOST_CHECK_EQUAL( B.m10(0,1), 6 );
  BOOST_CHECK_EQUAL( B.m11(0,0), 8 );

  B += Transpose(A);

  BOOST_CHECK_EQUAL( B.m00(0,0), 2*4 );
  BOOST_CHECK_EQUAL( B.m00(0,1), 2*2 );
  BOOST_CHECK_EQUAL( B.m00(1,0), 2*1 );
  BOOST_CHECK_EQUAL( B.m00(1,1), 2*3 );
  BOOST_CHECK_EQUAL( B.m01(0,0), 2*5 );
  BOOST_CHECK_EQUAL( B.m01(1,0), 2*7 );
  BOOST_CHECK_EQUAL( B.m10(0,0), 2*2 );
  BOOST_CHECK_EQUAL( B.m10(0,1), 2*6 );
  BOOST_CHECK_EQUAL( B.m11(0,0), 2*8 );

  B = A + Transpose(A);

  BOOST_CHECK_EQUAL( B.m00(0,0), 8 );
  BOOST_CHECK_EQUAL( B.m00(0,1), 3 );
  BOOST_CHECK_EQUAL( B.m00(1,0), 3 );
  BOOST_CHECK_EQUAL( B.m00(1,1), 6 );
  BOOST_CHECK_EQUAL( B.m01(0,0), 7 );
  BOOST_CHECK_EQUAL( B.m01(1,0), 13);
  BOOST_CHECK_EQUAL( B.m10(0,0), 7 );
  BOOST_CHECK_EQUAL( B.m10(0,1), 13);
  BOOST_CHECK_EQUAL( B.m11(0,0), 16);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixBlock_2x2_Dense_Transpose_MatrixD_NonSquare )
{
  typedef Real T;

  typedef MatrixBlock_2x2< MatrixD<T>, MatrixD<T>,
                           MatrixD<T>, MatrixD<T> > BlockMatrixType;

  MatrixD<T> A00 = {{ 1, 2, 3, 4},{ 5, 6, 7, 8}};
  MatrixD<T> A01 = {{9,10,11},{12,13,14}};
  MatrixD<T> A10 = {{15,16,17,18}};
  MatrixD<T> A11 = {{19,20,21}};

  BlockMatrixType A(A00, A01, A10, A11);
  BlockMatrixType B(MatrixD<T>(4,2), MatrixD<T>(4,1), MatrixD<T>(3,2), MatrixD<T>(3,1));

  B = Transpose(A);

  BOOST_CHECK_EQUAL( B.m00(0,0), 1 ); BOOST_CHECK_EQUAL( B.m00(0,1), 5 );
  BOOST_CHECK_EQUAL( B.m00(1,0), 2 ); BOOST_CHECK_EQUAL( B.m00(1,1), 6 );
  BOOST_CHECK_EQUAL( B.m00(2,0), 3 ); BOOST_CHECK_EQUAL( B.m00(2,1), 7 );
  BOOST_CHECK_EQUAL( B.m00(3,0), 4 ); BOOST_CHECK_EQUAL( B.m00(3,1), 8 );

  BOOST_CHECK_EQUAL( B.m01(0,0), 15 );
  BOOST_CHECK_EQUAL( B.m01(1,0), 16 );
  BOOST_CHECK_EQUAL( B.m01(2,0), 17 );
  BOOST_CHECK_EQUAL( B.m01(3,0), 18 );

  BOOST_CHECK_EQUAL( B.m10(0,0),  9 ); BOOST_CHECK_EQUAL( B.m10(0,1), 12 );
  BOOST_CHECK_EQUAL( B.m10(1,0), 10 ); BOOST_CHECK_EQUAL( B.m10(1,1), 13 );
  BOOST_CHECK_EQUAL( B.m10(2,0), 11 ); BOOST_CHECK_EQUAL( B.m10(2,1), 14 );

  BOOST_CHECK_EQUAL( B.m11(0,0), 19 );
  BOOST_CHECK_EQUAL( B.m11(1,0), 20 );
  BOOST_CHECK_EQUAL( B.m11(2,0), 21 );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
