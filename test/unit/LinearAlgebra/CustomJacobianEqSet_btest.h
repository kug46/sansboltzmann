// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef CUSTOMJACOBIANEQSET_BTEST_H
#define CUSTOMJACOBIANEQSET_BTEST_H

#include <map>
#include <array>

#include "TriDiagPattern_btest.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/Transpose.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/DenseLinAlg/tools/Identity.h"

// This is OK as this file is always only included in unit test cpp files
#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

template <class SystemMatrix>
class CustomJacobianEquationSet : public AlgebraicEquationSetBase<SystemMatrix>
{
public:
  typedef AlgebraicEquationSetBase<SystemMatrix> BaseType;

  typedef typename BaseType::SystemVector SystemVector;
  typedef typename BaseType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename BaseType::SystemMatrixView SystemMatrixView;
  typedef typename BaseType::SystemVectorView SystemVectorView;
  typedef typename BaseType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;

  typedef typename MatrixSizeType<SystemMatrix>::type MatrixSizeClass;
  typedef typename VectorSizeType<SystemVector>::type VectorSizeClass;

  CustomJacobianEquationSet(SystemVectorView& q) :
    comm_(nullptr), rstart_(-1), rend_(-1), N_(-1), q_(q), nz_(matrixSize())
  {}

  CustomJacobianEquationSet(std::shared_ptr<mpi::communicator> comm, const int rstart, const int rend, const int N, SystemVectorView& q) :
    comm_(comm), rstart_(rstart), rend_(rend), N_(N), q_(q), nz_(matrixSize())
  {}

  virtual ~CustomJacobianEquationSet() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  void setJacobianEntry(const int& i, const int& j, const Real& val)
  {
    if (rstart_ >= 0 && rend_ >= 0)
    {
      if (i >= rstart_ && i < rend_)
      {
        int modi = i - rstart_;

        int modj = 0;
        if (j  >= rstart_ && j < rend_)
        {
          modj = j - rstart_;
        }
        else if (j < rstart_)
        {
          modj = rend_ - rstart_ + j;
        }
        else if (j >= rend_)
        {
          modj = j;
        }
        SANS_ASSERT(modi >= 0 && modj >= 0);
        SANS_ASSERT(modi < N_ && modj < N_);

//        std::cout << "rank" << comm_->rank() << ": i,j=" << i << "," << j << ", rstart_= " << rstart_ << ", " << rend_
//                  << ", modi,modj= " << modi << "," << modj << ", val: " << val << std::endl;

        nz_.add(modi,modj);
        values_[{{modi,modj}}] = val;
      }
    }
    else
    {
      nz_.add(i,j);
      values_[{{i,j}}] = val;
    }
  }

  virtual void residual(SystemVectorView& rsd) override
  {
    SystemNonZeroPattern nz(matrixSize());
    jacobian(nz);
    SystemMatrix mtx(nz);
    jacobian(mtx);

    rsd = mtx*q_;
  }

  virtual void jacobian(SystemMatrixView& mtx) override
  {
//    std::cout << "jacobian, rank" << comm_->rank() << ", values.size=" << values_.size() << std::endl;

    for (auto it=values_.begin(); it!=values_.end(); ++it)
    {
//      std::cout << "rank" << comm_->rank() << ": i,j=" << it->first[0] << "," << it->first[1]<< ", val= " << it->second << std::endl;
      mtx.slowAccess(it->first[0], it->first[1]) = it->second;
    }
  }

  virtual void jacobian(SystemNonZeroPatternView& nz) override
  {
    nz = nz_;
  }

  virtual void jacobianTranspose(SystemMatrixView& mtx) override
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }
  virtual void jacobianTranspose(SystemNonZeroPatternView& nz) override
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }

  virtual std::vector<std::vector<Real>> residualNorm(const SystemVectorView& rsd) const override
  {
    std::vector<std::vector<Real>> rsdNorm(1, std::vector<Real>(1, 0));
    rsdNorm[0][0] = 0;
    for (int i = 0; i < rsd.m(); i++)
      rsdNorm[0][0] += dot(rsd[i], rsd[i]);

    rsdNorm[0][0] = sqrt( rsdNorm[0][0] );

    return rsdNorm;
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm) const override
  {
    //converged?
    return rsdNorm[0][0] < 1e-12;
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm, int iEq, int iMon) const override
  {
    SANS_ASSERT( iEq == 0 );
    SANS_ASSERT( iMon == 0 );

    //converged?
    return rsdNorm[0][0] < 1e-12;
  }

  // TODO:implement!
  virtual bool decreasedResidual(const std::vector<std::vector<Real>>& rsdNormOld,
                                 const std::vector<std::vector<Real>>& rsdNormNew) const override
  {

    //is the residual actually decreased or converged?
    int t = 0;

    if (rsdNormOld[0][0] < rsdNormNew[0][0] && rsdNormOld[0][0] > 1e-12)
      t++;

    return (t == 0);
  }

  //prints out a residual that could not be decreased and the convergence tolerances
  virtual void printDecreaseResidualFailure(const std::vector<std::vector<Real>>& rsdNorm, std::ostream& os = std::cout) const override {}

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override
  {
    q_ = q;
  }

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override
  {
    q = q_;
  }

  virtual VectorSizeClass vectorEqSize() const override    // vector for equations (rows in matrixSize)
  {
    if (rstart_ < 0 && rend_ < 0)
      return VectorSizeClass( q_.m() );
    else
      return VectorSizeClass( rend_ - rstart_ );
  }

  virtual VectorSizeClass vectorStateSize() const override // vector for state DOFs (columns in matrixSize)
  {
    return VectorSizeClass( q_.m() );
  }

  virtual MatrixSizeClass matrixSize() const override
  {
    // Create the size that represents the size of a sparse linear algebra matrix
    if (rstart_ < 0 && rend_ < 0)
      return { q_.m(), q_.m() };
    else
      return { rend_ - rstart_, q_.m() };
  }

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { return 0; }
  virtual int indexQ() const override { return 0; }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override
  {
    return true;
  }

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override
  {
    return 1;
  }

  // Converts processor local indexing to a processor global indexing
  virtual std::vector<GlobalContinuousMap> continuousGlobalMap() const override
  {
    SANS_ASSERT( comm_ != nullptr );

    std::vector<GlobalContinuousMap> continuousmap;
    continuousmap.emplace_back(comm_);

    int nghost = 0;
    if ( comm_->size() > 1 )
      nghost = N_ - (rend_ - rstart_);

    continuousmap[0].nDOFpossessed = rend_ - rstart_;
    continuousmap[0].nDOF_rank_offset = rstart_;
    continuousmap[0].remoteGhostIndex.resize(nghost);

    if ( comm_->size() > 1 )
    {
      int k = 0;
      for (int j = 0; j < N_; j++)
      {
        if (j < rstart_ || j >= rend_)
        {
          continuousmap[0].remoteGhostIndex[k] = j;
          k++;
        }
      }
      SANS_ASSERT(k == nghost);
    }

    return continuousmap;
  }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override { return comm_; }
  virtual void syncDOFs_MPI() override {}

  virtual bool updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                                         LinesearchData& pResData,
                                         LinesearchData& pStepData) const override { return true; };

  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override {};


  // Are we having machine precision issues - assume no issues
  virtual bool atMachinePrecision(const SystemVectorView& q, const std::vector<std::vector<Real>>& R0norm) override
  {
    return false;
  }

protected:
  std::shared_ptr<mpi::communicator> comm_;
  const int rstart_;
  const int rend_;
  const int N_;
  SystemVectorView& q_;
//  SystemMatrix jac_;
  SystemNonZeroPattern nz_;
  std::map<std::array<int,2>,Real> values_;
};

#if 1
template <class Matrix_type>
class CustomJacobianEquationSet< DLA::MatrixD<Matrix_type> > : public AlgebraicEquationSetBase<DLA::MatrixD<Matrix_type>>
{
public:
  typedef typename Matrix_type::Ttype TM;
  typedef DLA::MatrixD<Matrix_type> SystemMatrix;
  typedef AlgebraicEquationSetBase<SystemMatrix> BaseType;

  typedef typename BaseType::SystemVector SystemVector;
  typedef typename BaseType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename BaseType::SystemMatrixView SystemMatrixView;
  typedef typename BaseType::SystemVectorView SystemVectorView;
  typedef typename BaseType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;

  typedef typename MatrixSizeType<SystemMatrix>::type MatrixSizeClass;
  typedef typename VectorSizeType<SystemVector>::type VectorSizeClass;

  CustomJacobianEquationSet(SystemVectorView& q) :
    comm_(nullptr), rstart_(-1), rend_(-1), N_(-1), q_(q), nz_(matrixSize()) {}

  CustomJacobianEquationSet(std::shared_ptr<mpi::communicator> comm, const int rstart, const int rend, const int N, SystemVectorView& q) :
    comm_(comm), rstart_(rstart), rend_(rend), N_(N), q_(q), nz_(matrixSize()) {}
  virtual ~CustomJacobianEquationSet() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  //Currently only assigns val to (0,0) block of MatrixD
  void setJacobianEntry(const int& i, const int& j, const TM& val)
  {
    for (int i = 1; i < nz_.m(); i++ )
      for (int j = 0; j < nz_.n(); j++ )
        SANS_ASSERT( nz_(i,j).m() == 0 );

    if (rstart_ >= 0 && rend_ >= 0)
    {
      if (i >= rstart_ && i < rend_)
      {
        int modi = i - rstart_;

        int modj = 0;
        if (j  >= rstart_ && j < rend_)
        {
          modj = j - rstart_;
        }
        else if (j < rstart_)
        {
          modj = rend_ - rstart_ + j;
        }
        else if (j >= rend_)
        {
          modj = j;
        }
        SANS_ASSERT(modi >= 0 && modj >= 0);
        SANS_ASSERT(modi < N_ && modj < N_);

//        std::cout << "rank" << comm_->rank() << ": i,j=" << i << "," << j << ", rstart_= " << rstart_ << ", " << rend_
//                  << ", modi,modj= " << modi << "," << modj << ", val: " << val << std::endl;

        nz_(0,0).add(modi,modj);
        values_[{{modi,modj}}] = val;
      }
    }
    else
    {
      nz_(0,0).add(i,j);
      values_[{{i,j}}] = val;
    }
  }

  virtual void residual(SystemVectorView& rsd) override
  {
    SystemNonZeroPattern nz(matrixSize());
    jacobian(nz);
    SystemMatrix mtx(nz);
    jacobian(mtx);

    rsd = mtx*q_;
  }

  virtual void jacobian(SystemMatrixView& mtx) override
  {
    for (int i = 1; i < mtx.m(); i++ )
      for (int j = 0; j < mtx.n(); j++ )
        SANS_ASSERT( mtx(i,j).m() == 0 );

    for (auto it=values_.begin(); it!=values_.end(); ++it)
    {
//      std::cout << "rank" << comm_->rank() << ": i,j=" << it->first[0] << "," << it->first[1]<< ", val= " << it->second << std::endl;
      mtx(0,0).slowAccess(it->first[0], it->first[1]) = it->second;
    }
  }

  virtual void jacobian(SystemNonZeroPatternView& nz) override
  {
    nz = nz_;
  }

  virtual void jacobianTranspose(SystemMatrixView& mtx) override
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }
  virtual void jacobianTranspose(SystemNonZeroPatternView& nz) override
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }

  virtual std::vector<std::vector<Real>> residualNorm(const SystemVectorView& rsd) const override
  {
    std::vector<std::vector<Real>> rsdNorm(1, std::vector<Real>(1, 0));

    rsdNorm[0][0] = 0;
    for (int ii = 0; ii < rsd.m(); ii++)
      for (int i = 0; i < rsd[ii].m(); i++)
        rsdNorm[0][0] += dot(rsd[ii][i], rsd[ii][i]);

    rsdNorm[0][0] = sqrt( rsdNorm[0][0] );

    return rsdNorm;
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm) const override
  {
    //converged?
    return rsdNorm[0][0] < 1e-12;
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm, int iEq, int iMon) const override
  {
    SANS_ASSERT( iEq == 0 );
    SANS_ASSERT( iMon == 0 );

    //converged?
    return rsdNorm[0][0] < 1e-12;
  }

  // TODO:implement!
  virtual bool decreasedResidual(const std::vector<std::vector<Real>>& rsdNormOld,
                                 const std::vector<std::vector<Real>>& rsdNormNew) const override
  {
    //is the residual actually decreased or converged?
    int t = 0;

    if ( rsdNormOld[0][0] <= rsdNormNew[0][0] && !(convergedResidual(rsdNormNew)) )
      t++;

    return (t == 0);
  }

  //prints out a residual that could not be decreased and the convergence tolerances
  virtual void printDecreaseResidualFailure(const std::vector<std::vector<Real>>& rsdNorm,
                                            std::ostream& os = std::cout) const override
  {
    os << "CustomJacobianEquationSet::printDecreaseResidualFailure has not been implemented yet!" << std::endl;
  }

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override
  {
    q_ = q;
  }

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override
  {
    q = q_;
  }

  virtual VectorSizeClass vectorEqSize() const override    // vector for equations (rows in matrixSize)
  {
    if (rstart_ < 0 && rend_ < 0)
    {
      VectorSizeClass size(q_.m());
      for (int i = 0; i < size.m(); i++)
        size[i] = q_[i].m();

      return size;
    }
    else
    {
      VectorSizeClass size(q_.m());

      //TODO: This needs to be fixed for systems larger than 1x1 in MatrixD
      for (int i = 1; i < q_.m(); i++ )
        SANS_ASSERT( q_[i].m() == 0 );

      SANS_ASSERT( size.m() == 1 );
      int row = 0;
      for (int i = 0; i < size.m(); i++)
      {
        //if ( row >= rstart_ && row < rend_)
          //size[i] = { std::min(rend_, row+q_[i].m()) - row };
        size[i] = { rend_ - rstart_ };

        row += q_[i].m();
      }

      return size;
    }
  }

  virtual VectorSizeClass vectorStateSize() const override // vector for state DOFs (columns in matrixSize)
  {
    VectorSizeClass size(q_.m());
    for (int i = 0; i < size.m(); i++)
      size[i] = q_[i].m();

    return size;
  }

  virtual MatrixSizeClass matrixSize() const override
  {

    // Create the size that represents the size of a sparse linear algebra matrix
    if (rstart_ < 0 && rend_ < 0)
    {
      MatrixSizeClass size(q_.m(), q_.m());

      for (int i = 0; i < size.m(); i++)
        for (int j = 0; j < size.n(); j++)
          size(i,j) = { q_[i].m(), q_[j].m() };

       return size;
    }
    else
    {
      MatrixSizeClass size(q_.m(), q_.m());

      // TODO: The row size calculation only works for 1x1 MatrixD
      for (int i = 1; i < q_.m(); i++ )
        SANS_ASSERT( q_[i].m() == 0 );

      for (int i = 0; i < 1; i++ )//size.m(); i++)
      {
        for (int j = 0; j < size.n(); j++)
        {
          size(i,j) = { rend_ - rstart_,  q_[j].m() };
        }
      }

      return size;
    }
  }

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { return 0; }
  virtual int indexQ() const override { return 0; }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override
  {
    return true;
  }

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override
  {
    return 1;
  }

  // Converts processor local indexing to a processor global indexing
  virtual std::vector<GlobalContinuousMap> continuousGlobalMap() const override
  {
    SANS_ASSERT( comm_ != nullptr );

    std::vector<GlobalContinuousMap> continuousmap;
    continuousmap.emplace_back(comm_);
    continuousmap.emplace_back(comm_);

    int nghost = 0;
    if ( comm_->size() > 1 )
      nghost = N_ - (rend_ - rstart_);

    continuousmap[0].nDOFpossessed = rend_ - rstart_;
    continuousmap[0].nDOF_rank_offset = rstart_;
    continuousmap[0].remoteGhostIndex.resize(nghost);

    if ( comm_->size() > 1 )
    {
      int k = 0;
      for (int j = 0; j < N_; j++)
      {
        if (j < rstart_ || j >= rend_)
        {
          continuousmap[0].remoteGhostIndex[k] = j;
          k++;
        }
      }
      SANS_ASSERT(k == nghost);
    }

    return continuousmap;
  }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override { return comm_; }
  virtual void syncDOFs_MPI() override {}

  virtual bool updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                                         LinesearchData& pResData,
                                         LinesearchData& pStepData) const override { return true; };

  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override {};


  // Are we having machine precision issues - assume no issues
  virtual bool atMachinePrecision(const SystemVectorView& q, const std::vector<std::vector<Real>>& rsdNorm0) override
  {
    return false;
  }

protected:
  std::shared_ptr<mpi::communicator> comm_;
  const int rstart_;
  const int rend_;
  const int N_;
  SystemVectorView& q_;
  SystemNonZeroPattern nz_;
  std::map<std::array<int,2>, TM> values_;
};
#endif

}

#endif //CUSTOMJACOBIANEQSET_BTEST_H
