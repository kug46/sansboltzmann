// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ADVECTION1D_BTEST_H
#define ADVECTION1D_BTEST_H

#include "TriDiagPattern_btest.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/Transpose.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/DenseLinAlg/tools/Identity.h"

namespace SANS
{

template<class TM>
struct Advection1D
{
  static void init( SLA::SparseMatrix_CRS<TM>& A,
                    const int rstart=-1, const int rend=-1, const int N=-2 )
  {
    if (A.m() == 0) return; // nothing to do for an empty matrix

    int m = A.m() - 1;
    TM I = DLA::Identity();
    int rnz;

    if (rstart == 0 && A.m() == 1)
    {
      A.sparseRow(0,0) = 1*I;
      return;
    }
    else if (A.m() == 1)
    {
      rnz = A.rowNonZero(m);
      if ( rnz > 0 ) A.sparseRow(m,0) =  1*I;
      if ( rnz > 1 ) A.sparseRow(m,1) = -1*I; // ghost
      return;
    }

    rnz = A.rowNonZero(0);
    if ( rnz > 0 ) A.sparseRow(0,0) =  1*I;
    if ( rnz > 2 ) A.sparseRow(0,2) = -1*I; // ghost

    for ( int i = 1; i < A.m(); i++ )
    {
      rnz = A.rowNonZero(i);
      if ( rnz > 0 ) A.sparseRow(i,0) = -1*I;
      if ( rnz > 1 ) A.sparseRow(i,1) =  1*I;
    }
  }

  static void init_transpose( SLA::SparseMatrix_CRS<TM>& A )
  {
    SANS_ASSERT( A.getNumNonZero() > 0 );
    SANS_ASSERT( A.m() > 1 );

    int m = A.m() - 1;
    TM I = DLA::Identity();
    int rnz;

    rnz = A.rowNonZero(0);
    if ( rnz > 0 ) A.sparseRow(0,0) =  1*I;
    if ( rnz > 1 ) A.sparseRow(0,1) = -1*I;

    for ( int i = 1; i < A.m()-1; i++ )
    {
      rnz = A.rowNonZero(i);
      if ( rnz > 1 ) A.sparseRow(i,1) =  1*I;
      if ( rnz > 2 ) A.sparseRow(i,2) = -1*I;
    }

    rnz = A.rowNonZero(m);
    if ( rnz > 1 ) A.sparseRow(m,1) =  1*I;
    if ( rnz > 2 ) A.sparseRow(m,2) = -1*I; // ghost
  }
};


template<int N, class TM>
struct Advection1D< DLA::MatrixS<N,N,TM> >
{
  static void init( SLA::SparseMatrix_CRS< DLA::MatrixS<N,N,TM> >& A,
                    const int rstart=-1, const int rend=-1, const int M=-2 )
  {
    if (A.m() == 0) return; // nothing to do for an empty matrix

    DLA::MatrixS<N,N,TM> I = DLA::Identity();
    if ( N > 1 )
    {
      I(0,N-1) = 2;
      I(N-1,0) = 3;
    }

    int m = A.m() - 1;
    int rnz;

    if (rstart == 0 && A.m() == 1)
    {
      A.sparseRow(0,0) = 1*I;
      return;
    }
    else if (A.m() == 1)
    {
      rnz = A.rowNonZero(m);
      if ( rnz > 0 ) A.sparseRow(m,0) =  1*I;
      if ( rnz > 1 ) A.sparseRow(m,1) = -1*I; // ghost
      return;
    }

    rnz = A.rowNonZero(0);
    if ( rnz > 0 ) A.sparseRow(0,0) =  1*I;
    if ( rnz > 2 ) A.sparseRow(0,2) = -1*I; // ghost

    for ( int i = 1; i < A.m(); i++ )
    {
      rnz = A.rowNonZero(i);
      if ( rnz > 0 ) A.sparseRow(i,0) = -1*I;
      if ( rnz > 1 ) A.sparseRow(i,1) =  1*I;
    }
  }

  static void init_transpose( SLA::SparseMatrix_CRS< DLA::MatrixS<N,N,TM> >& A )
  {
    SANS_ASSERT( A.getNumNonZero() );

    DLA::MatrixS<N,N,TM> I = DLA::Identity();
    if ( N > 1 )
    {
      I(0,N-1) = 3;
      I(N-1,0) = 2;
    }

    A.sparseRow(0,0) =  1*I;
    A.sparseRow(0,1) = -1*I;
    for ( int i = 1; i < A.m()-1; i++ )
    {
      A.sparseRow(i,1) =  1*I;
      A.sparseRow(i,2) = -1*I;
    }
    int rnz = A.rowNonZero(A.m()-1);
    if ( rnz > 1 ) A.sparseRow(A.m()-1,1) =  1*I;
    if ( rnz > 2 ) A.sparseRow(A.m()-1,2) = -1*I; // ghost
  }
};


template<>
struct Advection1D< DLA::MatrixD<Real> >
{
  static void init( SLA::SparseMatrix_CRS< DLA::MatrixD<Real> >& A,
                    const int rstart=-1, const int rend=-1, const int M=-2 )
  {
    SANS_ASSERT( A.getNumNonZero() );
    SANS_ASSERT( A.m() > 1 );

    //Just assume all blocks are the same...
    const int block_m = A.block_m(0);
    DLA::MatrixD<Real> I(block_m, block_m);
    I = SANS::DLA::Identity();
    if ( block_m > 1 )
    {
      I(0,block_m-1) = 2;
      I(block_m-1,0) = 3;
    }

    A.sparseRow(0,0) =  1*I;
    for ( int i = 1; i < A.m()-1; i++ )
    {
      A.sparseRow(i,0) = -1*I;
      A.sparseRow(i,1) =  1*I;
    }
    A.sparseRow(A.m()-1,0) = -1*I;
    A.sparseRow(A.m()-1,1) =  1*I;
  }

  static void init_transpose( SLA::SparseMatrix_CRS< DLA::MatrixD<Real> >& A )
  {
    SANS_ASSERT( A.getNumNonZero() );

    const int block_m = A.block_m(0);
    DLA::MatrixD<Real> I(block_m, block_m);
    I = SANS::DLA::Identity();
    if ( block_m > 1 )
    {
      I(0,block_m-1) = 3;
      I(block_m-1,0) = 2;
    }

    A.sparseRow(0,0) =  1*I;
    A.sparseRow(0,1) = -1*I;
    for ( int i = 1; i < A.m()-1; i++ )
    {
      A.sparseRow(i,1) =  1*I;
      A.sparseRow(i,2) = -1*I;
    }
    A.sparseRow(A.m()-1,1) =  1*I;
  }
};


template<int N, class TM>
struct Advection1D< DLA::MatrixD< DLA::MatrixS<N,N,TM> > >
{
  static void init( SLA::SparseMatrix_CRS< DLA::MatrixD< DLA::MatrixS<N,N,TM> > >& A,
                    const int rstart=-1, const int rend=-1, const int M=-2  )
  {
    SANS_ASSERT( A.getNumNonZero() );
    SANS_ASSERT( A.m() > 1 );

    //Just assume all blocks are the same...
    const int block_m = A.block_m(0);
    DLA::MatrixD< DLA::MatrixS<N,N,TM> > I(block_m, block_m);
    I = SANS::DLA::Identity();

    A.sparseRow(0,0) =  1*I;
    for ( int i = 1; i < A.m()-1; i++ )
    {
      A.sparseRow(i,0) = -1*I;
      A.sparseRow(i,1) =  1*I;
    }
    A.sparseRow(A.m()-1,0) = -1*I;
    A.sparseRow(A.m()-1,1) =  1*I;
  }

  static void init_transpose( SLA::SparseMatrix_CRS< DLA::MatrixD< DLA::MatrixS<N,N,TM> > >& A )
  {
    SANS_ASSERT( A.getNumNonZero() );

    const int block_m = A.block_m(0);
    DLA::MatrixD< DLA::MatrixS<N,N,TM> > I(block_m, block_m);
    I = SANS::DLA::Identity();
    if ( block_m > 1 )
    {
      I(0,block_m-1) = 3;
      I(block_m-1,0) = 2;
      if ( N > 1 )
      {
        I(0,block_m-1)(0,N-1) = 5;
        I(block_m-1,0)(N-1,0) = 4;
      }
    }

    A.sparseRow(0,0) =  1*I;
    A.sparseRow(0,1) = -1*I;
    for ( int i = 1; i < A.m()-1; i++ )
    {
      A.sparseRow(i,1) =  1*I;
      A.sparseRow(i,2) = -1*I;
    }
    A.sparseRow(A.m()-1,1) =  1*I;
  }
};


template <class SystemMatrix>
class Advection1DEquationSet : public AlgebraicEquationSetBase<SystemMatrix>
{
public:
  typedef AlgebraicEquationSetBase<SystemMatrix> BaseType;

  typedef typename BaseType::SystemVector SystemVector;
  typedef typename BaseType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename BaseType::SystemMatrixView SystemMatrixView;
  typedef typename BaseType::SystemVectorView SystemVectorView;
  typedef typename BaseType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;

  typedef typename MatrixSizeType<SystemMatrix>::type MatrixSizeClass;
  typedef typename VectorSizeType<SystemVector>::type VectorSizeClass;

  explicit Advection1DEquationSet(SystemVectorView& q, bool singular = false) :
    comm_(nullptr), rstart_(-1), rend_(-1), N_(-1), b_(q), q_(q), singular_(singular) {}

  Advection1DEquationSet(std::shared_ptr<mpi::communicator> comm, const int rstart, const int rend, const int N,
                         SystemVectorView& b, SystemVectorView& q) :
    comm_(comm), rstart_(rstart), rend_(rend), N_(N), b_(b), q_(q), singular_(false) {}
  virtual ~Advection1DEquationSet() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  virtual void residual(SystemVectorView& rsd) override
  {
    SystemNonZeroPattern nz(matrixSize());
    jacobian(nz);
    SystemMatrix mtx(nz);
    jacobian(mtx);

    rsd = mtx*q_;
  }

  virtual void jacobian(SystemMatrixView& mtx) override
  {
    typedef typename SystemMatrix::Ttype TM ;

    if (singular_)
      mtx = 0;
    else
      Advection1D<TM>::init(mtx, rstart_, rend_, N_);
  }

  virtual void jacobian(SystemNonZeroPatternView& nz) override
  {
    if (rstart_ >= 0 && rend_ >= 0)
      TriDiagPattern(rstart_, rend_, 0, N_, N_, nz);
    else
      TriDiagPattern(nz, 2);
  }

  virtual void jacobianTranspose(SystemMatrixView& mtx) override
  {
    typedef typename SystemMatrix::Ttype TM ;

    if (singular_)
      mtx = 0;
    else
      Advection1D<TM>::init_transpose(mtx);
  }
  virtual void jacobianTranspose(SystemNonZeroPatternView& nz) override
  {
    jacobian(nz);
  }

  virtual std::vector<std::vector<Real>> residualNorm(const SystemVectorView& rsd) const override
  {
    std::vector<std::vector<Real>> rsdNorm(1, std::vector<Real>(1, 0));

    rsdNorm[0][0] = 0;
    for (int i = 0; i < rsd.m(); i++)
      rsdNorm[0][0] += dot(rsd[i], rsd[i]);

    rsdNorm[0][0] = sqrt( rsdNorm[0][0] );

    return rsdNorm;
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm) const override
  {
    //converged?
    return rsdNorm[0][0] < 1e-12;
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm, int iEq, int iMon) const override
  {
    SANS_ASSERT( iEq == 0 );
    SANS_ASSERT( iMon == 0 );

    //converged?
    return rsdNorm[0][0] < 1e-12;
  }

  // TODO:implement!
  virtual bool decreasedResidual(const std::vector<std::vector<Real>>& rsdNormOld,
                                 const std::vector<std::vector<Real>>& rsdNormNew) const override
  {
    //is the residual actually decreased or converged?
    int t = 0;

    if (rsdNormOld[0][0] < rsdNormNew[0][0] && rsdNormOld[0][0] > 1e-12)
      t++;

    return (t == 0);
  }

  //prints out a residual that could not be decreased and the convergence tolerances
  virtual void printDecreaseResidualFailure(const std::vector<std::vector<Real>>& rsdNorm, std::ostream& os = std::cout) const override {}

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override
  {
    q_ = q;
  }

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override
  {
    q = q_;
  }

  virtual VectorSizeClass vectorEqSize() const override    // vector for equations (rows in matrixSize)
  {
    if (rstart_ >= 0 && rend_ >= 0)
      SANS_ASSERT_MSG( b_.m() == rend_ - rstart_, "%d != %d", b_.m(), rend_ - rstart_);

    return VectorSizeClass( b_.m() );
  }

  virtual VectorSizeClass vectorStateSize() const override // vector for state DOFs (columns in matrixSize)
  {
    return VectorSizeClass( q_.m() );
  }

  virtual MatrixSizeClass matrixSize() const override
  {
    if (rstart_ >= 0 && rend_ >= 0)
      SANS_ASSERT_MSG( b_.m() == rend_ - rstart_, "%d != %d", b_.m(), rend_ - rstart_);

    // Create the size that represents the size of a sparse linear algebra matrix
    return { b_.m(), q_.m() };
  }

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { return 0; }
  virtual int indexQ() const override { return 0; }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override
  {
    return true;
  }

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override
  {
    return 1;
  }

  // Converts processor local indexing to a processor continuous indexing
  virtual std::vector<GlobalContinuousMap> continuousGlobalMap() const override
  {
    SANS_ASSERT( comm_ != nullptr );

    std::vector<GlobalContinuousMap> continuousmap;
    continuousmap.emplace_back(comm_);

    int nghost = 0;
    if ( comm_->size() > 1 && rstart_ != N_ )
      nghost = (rstart_ == 0 || rend_ == N_) ? 1 : 2;

    continuousmap[0].nDOFpossessed = rend_ - rstart_;
    continuousmap[0].nDOF_rank_offset = rstart_;
    continuousmap[0].remoteGhostIndex.resize(nghost);

    if ( comm_->size() > 1 && nghost > 0 )
    {
           if (rstart_ == 0 ) continuousmap[0].remoteGhostIndex[0] = rend_;
      else if (rend_   == N_) continuousmap[0].remoteGhostIndex[0] = rstart_-1;
      else
      {
        continuousmap[0].remoteGhostIndex[0] = rstart_-1;
        continuousmap[0].remoteGhostIndex[1] = rend_;
      }
    }

    return continuousmap;
  }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override { return comm_; }
  virtual void syncDOFs_MPI() override {}

  virtual bool updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                                         LinesearchData& pResData,
                                         LinesearchData& pStepData) const override { return true; };

  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override {};


  // Are we having machine precision issues - assume no issues
  virtual bool atMachinePrecision(const SystemVectorView& q, const std::vector<std::vector<Real>>& R0norm) override
  {
    return false;
  }

protected:
  std::shared_ptr<mpi::communicator> comm_;
  const int rstart_;
  const int rend_;
  const int N_;
  SystemVectorView& b_;
  SystemVectorView& q_;
  bool singular_;
};


template <class Matrix_type>
class Advection1DEquationSet< DLA::MatrixD<Matrix_type> > : public AlgebraicEquationSetBase<DLA::MatrixD<Matrix_type>>
{
public:
  typedef DLA::MatrixD<Matrix_type> SystemMatrix;
  typedef AlgebraicEquationSetBase<SystemMatrix> BaseType;

  typedef typename BaseType::SystemVector SystemVector;
  typedef typename BaseType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename BaseType::SystemMatrixView SystemMatrixView;
  typedef typename BaseType::SystemVectorView SystemVectorView;
  typedef typename BaseType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;

  typedef typename MatrixSizeType<SystemMatrix>::type MatrixSizeClass;
  typedef typename VectorSizeType<SystemVector>::type VectorSizeClass;

  explicit Advection1DEquationSet(SystemVectorView& q, bool singular = false) :
    comm_(nullptr), rstart_(-1), rend_(-1), N_{{-1,-1}}, b_(q), q_(q), singular_(singular) {}

  Advection1DEquationSet(std::shared_ptr<mpi::communicator> comm, const int rstart, const int rend,
                         const std::array<int,2>& N, SystemVectorView& b, SystemVectorView& q) :
    comm_(comm), rstart_(rstart), rend_(rend), N_(N), b_(b), q_(q), singular_(false)
  {
    SANS_ASSERT(b_.m() == q_.m());
  }
  virtual ~Advection1DEquationSet() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  virtual void residual(SystemVectorView& rsd) override
  {
    SystemNonZeroPattern nz(matrixSize());
    jacobian(nz);
    SystemMatrix mtx(nz);
    jacobian(mtx);

    rsd = mtx*q_;
  }

  virtual void jacobian(SystemMatrixView& mtx) override
  {
    typedef typename Matrix_type::Ttype TM ;

    TM I = DLA::Identity();

    if (singular_)
      mtx = 0;
    else
    {
      for (int i = 0; i < mtx.m(); i++)
        for (int j = std::max(0,i-1); j <= i; j++)
        {
          if (b_[i].m() == 0) continue;
          if (q_[j].m() == 0) continue;

          if ( i == j )
            Advection1D<TM>::init(mtx(i,j), rstart_, rend_, N_[0]+ N_[1]);
          else if ( j < i )
            mtx(i,j).sparseRow(0,0) = -1*I;
        }
    }
  }

  virtual void jacobian(SystemNonZeroPatternView& nz) override
  {
    int nEq = 0;
    for (int i = 0; i < nz.m(); i++)
    {
      for (int j = std::max(0,i-1); j <= i; j++)
      {
        if (b_[i].m() == 0) continue;
        if (q_[j].m() == 0) continue;

        if ( i == j )
          if (rstart_ >= 0 && rend_ >= 0)
            TriDiagPattern(rstart_, rend_, nEq, nEq+N_[i],  N_[0]+ N_[1], nz(i,j));
          else
            TriDiagPattern(nz(i,j), 2);
        else if ( j < i )
          nz(i,j).add(0,std::max(b_[j].m()-1,0));
      }

      nEq += N_[i];
    }
  }

  virtual void jacobianTranspose(SystemMatrixView& mtx) override
  {
    typedef typename Matrix_type::Ttype TM ;

    TM I = DLA::Identity();

    if (singular_)
      mtx = 0;
    else
    {
      for (int i = 0; i < mtx.m(); i++)
        for (int j = i; j < std::min(i+2,mtx.n()); j++)
        {
          if (mtx(i,j).m()*mtx(i,j).n() == 0) continue;

          if ( i == j )
            Advection1D<TM>::init_transpose(mtx(i,j));
          else if ( j > i )
            mtx(i,j).sparseRow(mtx(i,j).m()-1,0) = -1*I;
        }
    }
  }
  virtual void jacobianTranspose(SystemNonZeroPatternView& nz) override
  {
    int nEq = 0;
    for (int i = 0; i < nz.m(); i++)
    {
      for (int j = i; j < std::min(i+2,nz.n()); j++)
      {
        if (nz(i,j).m()*nz(i,j).n() == 0) continue;

        if ( i == j )
          if (rstart_ >= 0 && rend_ >= 0)
            TriDiagPattern(rstart_, rend_, nEq, nEq+N_[i],  N_[0]+ N_[1], nz(i,j));
          else
            TriDiagPattern(nz(i,j), 2);
        else if ( j > i )
          nz(i,j).add(nz(i,j).m()-1,0);
      }
      nEq += N_[i];
    }
  }

  virtual std::vector<std::vector<Real>> residualNorm(const SystemVectorView& rsd) const override
  {
    std::vector<std::vector<Real>> rsdNorm(1, std::vector<Real>(1, 0));

    rsdNorm[0][0] = 0;
    for (int ii = 0; ii < rsd.m(); ii++)
      for (int i = 0; i < rsd[ii].m(); i++)
        rsdNorm[0][0] += dot(rsd[ii][i], rsd[ii][i]);

    rsdNorm[0][0] = sqrt( rsdNorm[0][0] );

    return rsdNorm;
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm) const override
  {

    //converged?
    return rsdNorm[0][0] < 1e-12;
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm, int iEq, int iMon) const override
  {
    SANS_ASSERT( iEq == 0 );
    SANS_ASSERT( iMon == 0 );

    //converged?
    return rsdNorm[0][0] < 1e-12;
  }

  // TODO:implement!
  virtual bool decreasedResidual(const std::vector<std::vector<Real>>& rsdNormOld,
                                 const std::vector<std::vector<Real>>& rsdNormNew) const override
  {
    //is the residual actually decreased or converged?
    int t = 0;

    if (rsdNormOld[0][0] < rsdNormNew[0][0] && rsdNormOld[0][0] > 1e-12)
      t++;

    return (t == 0);
  }

  //prints out a residual that could not be decreased and the convergence tolerances
  virtual void printDecreaseResidualFailure(const std::vector<std::vector<Real>>& rsdNorm, std::ostream& os = std::cout) const override {}

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override
  {
    q_ = q;
  }

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override
  {
    q = q_;
  }

  virtual VectorSizeClass vectorEqSize() const override    // vector for equations (rows in matrixSize)
  {
    VectorSizeClass size(b_.m());
    int nrow = 0;
    for (int i = 0; i < size.m(); i++)
    {
      size[i] = b_[i].m();
      nrow += b_[i].m();
    }

    if (rstart_ >= 0 && rend_ >= 0)
      SANS_ASSERT( nrow == rend_ - rstart_ );

    return size;
  }

  virtual VectorSizeClass vectorStateSize() const override // vector for state DOFs (columns in matrixSize)
  {
    VectorSizeClass size(q_.m());
    for (int i = 0; i < size.m(); i++)
      size[i] = q_[i].m();

    return size;
  }

  virtual MatrixSizeClass matrixSize() const override
  {
    // Create the size that represents the size of a sparse linear algebra matrix
    MatrixSizeClass size(b_.m(), q_.m());

    int nrow = 0;
    for (int i = 0; i < size.m(); i++)
    {
      for (int j = 0; j < size.n(); j++)
        size(i,j) = { b_[i].m(), q_[j].m() };

      nrow += b_[i].m();
    }

    if (rstart_>= 0 && rend_ >= 0)
      SANS_ASSERT( nrow == rend_ - rstart_ );

    return size;
  }

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { return 0; }
  virtual int indexQ() const override { return 0; }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override
  {
    return true;
  }

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override
  {
    return 1;
  }

  // Converts processor local indexing to a processor continuous indexing
  virtual std::vector<GlobalContinuousMap> continuousGlobalMap() const override
  {
    SANS_ASSERT( comm_ != nullptr );

    std::vector<GlobalContinuousMap> continuousmap;
    continuousmap.emplace_back(comm_);
    continuousmap.emplace_back(comm_);

    SANS_ASSERT (b_.m() == 2);

    int nEq = 0;
    for (int i = 0; i < 2; i++)
    {
      int nghost = q_[i].m() - b_[i].m();

      continuousmap[i].nDOFpossessed = b_[i].m();
      continuousmap[i].nDOF_rank_offset = rstart_;
      continuousmap[i].remoteGhostIndex.resize(nghost);

      if ( comm_->size() > 1 && nghost > 0 )
      {
        nghost = 0;
        if ( rstart_ != 0 && rstart_ > nEq && rstart_ <= nEq + N_[i])
          continuousmap[i].remoteGhostIndex[nghost++] = rstart_-1;
        if ( rend_ != N_[0]+N_[1] && rend_ >= nEq && rend_ < nEq + N_[i])
          continuousmap[i].remoteGhostIndex[nghost++] = rend_;
      }
      nEq += N_[i];
    }

    return continuousmap;
  }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override { return comm_; }
  virtual void syncDOFs_MPI() override {}

  virtual bool updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                                         LinesearchData& pResData,
                                         LinesearchData& pStepData) const override { return true; };

  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override {};


  // Are we having machine precision issues - assume no issues
  virtual bool atMachinePrecision(const SystemVectorView& q, const std::vector<std::vector<Real>>& R0norm) override
  {
    return false;
  }

protected:
  std::shared_ptr<mpi::communicator> comm_;
  const int rstart_;
  const int rend_;
  const std::array<int,2> N_;
  SystemVectorView& b_;
  SystemVectorView& q_;
  bool singular_;
};

}

#endif //ADVECTION1D_BTEST_H
