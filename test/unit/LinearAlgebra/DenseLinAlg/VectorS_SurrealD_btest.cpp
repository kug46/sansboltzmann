// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// VectorS_SurrealB_btest
// testing of VectorS<N,T> class with T = SurrealD

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "Surreal/SurrealD.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
namespace DLA
{
template class VectorS<1, SurrealD>;
template class VectorS<4, SurrealD>;
}
}

using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( VectorS_SurrealB_test_suite )


typedef SurrealD Int;

typedef VectorS<1,Int> VectorS1;
typedef VectorS<4,Int> VectorS4;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( size1 )
{
  BOOST_CHECK( VectorS1::N == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( array_ops1 )
{
  const Int data = 3;
  VectorS1 a1(&data, 1);
  VectorS1 a2(a1);
  VectorS1 a3(1), a4, a5;

  // size
  BOOST_CHECK( a1.N == 1 );
  BOOST_CHECK( a2.N == 1 );
  BOOST_CHECK( a3.N == 1 );
  BOOST_CHECK( a4.N == 1 );
  BOOST_CHECK( a5.N == 1 );

  // ctors
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL(  1, a3[0] );
  BOOST_CHECK_EQUAL(  0, a4[0] );
  BOOST_CHECK_EQUAL(  0, a5[0] );

  // assignment
  a3 = a1;
  a4 = data;
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a3[0] );
  BOOST_CHECK_EQUAL(  3, a4[0] );

  a1 = a2 = a3 = data;
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL(  3, a3[0] );

  a4 = data;
  a1 = a2 = a3 = a4;
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL(  3, a3[0] );
  BOOST_CHECK_EQUAL(  3, a4[0] );

  // unary
  a2 = +a1;
  a3 = -a1;
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL( -3, a3[0] );

  // binary accumulation
  a2 = a1;
  a3 = a1;
  a4 = a1;
  a1 += data;
  a2 -= data;
  a3 *= data;
  a4 /= data;
  BOOST_CHECK_EQUAL(  6, a1[0] );
  BOOST_CHECK_EQUAL(  0, a2[0] );
  BOOST_CHECK_EQUAL(  9, a3[0] );
  BOOST_CHECK_EQUAL(  1, a4[0] );

  a1 = data;
  a2 = a1;
  a3 = a1;
  a2 += a1;
  a3 -= a1;
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  6, a2[0] );
  BOOST_CHECK_EQUAL(  0, a3[0] );

  // binary operators
  a1 = data;
//  a2 = a1 + data;
//  a3 = a1 - data;
  a4 = a1 * data;
  BOOST_CHECK_EQUAL(  3, a1[0] );
//  BOOST_CHECK_EQUAL(  6, a2[0] );
//  BOOST_CHECK_EQUAL(  0, a3[0] );
  BOOST_CHECK_EQUAL(  9, a4[0] );

//  a2 = data + a1;
//  a3 = data - a1;
  a4 = data * a1;
  BOOST_CHECK_EQUAL(  3, a1[0] );
//  BOOST_CHECK_EQUAL(  6, a2[0] );
//  BOOST_CHECK_EQUAL(  0, a3[0] );
  BOOST_CHECK_EQUAL(  9, a4[0] );

  a1 = a2 = data;
  a3 = a1 + a2;
  a4 = a1 - a2;
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL(  6, a3[0] );
  BOOST_CHECK_EQUAL(  0, a4[0] );

  // arithmetic combinations

  a1 = a2 = data;
  a3 = a1 + a2;
  a4 = a1 + a2 + a3;
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL(  6, a3[0] );
  BOOST_CHECK_EQUAL( 12, a4[0] );

  a2 += a1;
  a3 += a1 + a2;
  a4 += a1 + a2 + a3;
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  6, a2[0] );
  BOOST_CHECK_EQUAL( 15, a3[0] );
  BOOST_CHECK_EQUAL( 36, a4[0] );

  a3 = a1 - a2;
  a4 = a1 - a2 - a3;
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  6, a2[0] );
  BOOST_CHECK_EQUAL( -3, a3[0] );
  BOOST_CHECK_EQUAL(  0, a4[0] );

  a2 -= a1;
  a3 -= a1 - a2;
  a4 -= a1 - a2 - a3;
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL( -3, a3[0] );
  BOOST_CHECK_EQUAL( -3, a4[0] );

  a3 = a1 - a2;
  a4 = a1 + a2 - a3;
  a5 = a1 - a2 + a3;
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL(  0, a3[0] );
  BOOST_CHECK_EQUAL(  6, a4[0] );
  BOOST_CHECK_EQUAL(  0, a5[0] );

#ifndef __clang_analyzer__
  a5 = (a1 + a2) + (a3 + a4);
  BOOST_CHECK_EQUAL( 12, a5[0] );
  a5 = (a1 + a2) + (a3 - a4);
  BOOST_CHECK_EQUAL(  0, a5[0] );
  a5 = (a1 + a2) - (a3 + a4);
  BOOST_CHECK_EQUAL(  0, a5[0] );
  a5 = (a1 + a2) - (a3 - a4);
  BOOST_CHECK_EQUAL( 12, a5[0] );
  a5 = (a1 - a2) + (a3 + a4);
  BOOST_CHECK_EQUAL(  6, a5[0] );
  a5 = (a1 - a2) + (a3 - a4);
  BOOST_CHECK_EQUAL( -6, a5[0] );
  a5 = (a1 - a2) - (a3 + a4);
  BOOST_CHECK_EQUAL( -6, a5[0] );
  a5 = (a1 - a2) - (a3 - a4);
  BOOST_CHECK_EQUAL(  6, a5[0] );
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL(  0, a3[0] );
  BOOST_CHECK_EQUAL(  6, a4[0] );

  a5 += (a1 + a2) + (a3 + a4);
  a5 += (a1 + a2) + (a3 - a4);
  a5 += (a1 + a2) - (a3 + a4);
  a5 += (a1 + a2) - (a3 - a4);
  a5 += (a1 - a2) + (a3 + a4);
  a5 += (a1 - a2) + (a3 - a4);
  a5 += (a1 - a2) - (a3 + a4);
  a5 += (a1 - a2) - (a3 - a4);
  BOOST_CHECK_EQUAL( 30, a5[0] );
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL(  0, a3[0] );
  BOOST_CHECK_EQUAL(  6, a4[0] );

  a5 -= (a1 + a2) + (a3 + a4);
  a5 -= (a1 + a2) + (a3 - a4);
  a5 -= (a1 + a2) - (a3 + a4);
  a5 -= (a1 + a2) - (a3 - a4);
  a5 -= (a1 - a2) + (a3 + a4);
  a5 -= (a1 - a2) + (a3 - a4);
  a5 -= (a1 - a2) - (a3 + a4);
  a5 -= (a1 - a2) - (a3 - a4);
  BOOST_CHECK_EQUAL(  6, a5[0] );
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL(  0, a3[0] );
  BOOST_CHECK_EQUAL(  6, a4[0] );
#endif

  a1 = data;

  a2 = 4*a1;
  a3 = a2*7;
  BOOST_CHECK_EQUAL(   3, a1[0] );
  BOOST_CHECK_EQUAL(  12, a2[0] );
  BOOST_CHECK_EQUAL(  84, a3[0] );

  a2 += 4*a1;
  a3 += a2*7;
  BOOST_CHECK_EQUAL(   3, a1[0] );
  BOOST_CHECK_EQUAL(  24, a2[0] );
  BOOST_CHECK_EQUAL( 252, a3[0] );

  a2 -= 4*a1;
  a3 -= a2*7;
  BOOST_CHECK_EQUAL(   3, a1[0] );
  BOOST_CHECK_EQUAL(  12, a2[0] );
  BOOST_CHECK_EQUAL( 168, a3[0] );

#ifndef __clang_analyzer__
  a5 = 2*(a1 + a2) + (a3 + a4)*3;
  BOOST_CHECK_EQUAL(  552, a5[0] );
  a5 = 2*(a1 + a2) + (a3 - a4)*3;
  BOOST_CHECK_EQUAL(  516, a5[0] );
  a5 = 2*(a1 + a2) - (a3 + a4)*3;
  BOOST_CHECK_EQUAL( -492, a5[0] );
  a5 = 2*(a1 + a2) - (a3 - a4)*3;
  BOOST_CHECK_EQUAL( -456, a5[0] );
  a5 = 2*(a1 - a2) + (a3 + a4)*3;
  BOOST_CHECK_EQUAL(  504, a5[0] );
  a5 = 2*(a1 - a2) + (a3 - a4)*3;
  BOOST_CHECK_EQUAL(  468, a5[0] );
  a5 = 2*(a1 - a2) - (a3 + a4)*3;
  BOOST_CHECK_EQUAL( -540, a5[0] );
  a5 = 2*(a1 - a2) - (a3 - a4)*3;
  BOOST_CHECK_EQUAL( -504, a5[0] );
  BOOST_CHECK_EQUAL(    3, a1[0] );
  BOOST_CHECK_EQUAL(   12, a2[0] );
  BOOST_CHECK_EQUAL(  168, a3[0] );
  BOOST_CHECK_EQUAL(    6, a4[0] );

  a5 += 2*(a1 + a2) + (a3 + a4)*3;
  BOOST_CHECK_EQUAL(   48, a5[0] );
  a5 += 2*(a1 + a2) + (a3 - a4)*3;
  BOOST_CHECK_EQUAL(  564, a5[0] );
  a5 += 2*(a1 + a2) - (a3 + a4)*3;
  BOOST_CHECK_EQUAL(   72, a5[0] );
  a5 += 2*(a1 + a2) - (a3 - a4)*3;
  BOOST_CHECK_EQUAL( -384, a5[0] );
  a5 += 2*(a1 - a2) + (a3 + a4)*3;
  BOOST_CHECK_EQUAL(  120, a5[0] );
  a5 += 2*(a1 - a2) + (a3 - a4)*3;
  BOOST_CHECK_EQUAL(  588, a5[0] );
  a5 += 2*(a1 - a2) - (a3 + a4)*3;
  BOOST_CHECK_EQUAL(   48, a5[0] );
  a5 += 2*(a1 - a2) - (a3 - a4)*3;
  BOOST_CHECK_EQUAL( -456, a5[0] );
  BOOST_CHECK_EQUAL(    3, a1[0] );
  BOOST_CHECK_EQUAL(   12, a2[0] );
  BOOST_CHECK_EQUAL(  168, a3[0] );
  BOOST_CHECK_EQUAL(    6, a4[0] );

  a5 -= 2*(a1 + a2) + (a3 + a4)*3;
  BOOST_CHECK_EQUAL( -1008, a5[0] );
  a5 -= 2*(a1 + a2) + (a3 - a4)*3;
  BOOST_CHECK_EQUAL( -1524, a5[0] );
  a5 -= 2*(a1 + a2) - (a3 + a4)*3;
  BOOST_CHECK_EQUAL( -1032, a5[0] );
  a5 -= 2*(a1 + a2) - (a3 - a4)*3;
  BOOST_CHECK_EQUAL(  -576, a5[0] );
  a5 -= 2*(a1 - a2) + (a3 + a4)*3;
  BOOST_CHECK_EQUAL( -1080, a5[0] );
  a5 -= 2*(a1 - a2) + (a3 - a4)*3;
  BOOST_CHECK_EQUAL( -1548, a5[0] );
  a5 -= 2*(a1 - a2) - (a3 + a4)*3;
  BOOST_CHECK_EQUAL( -1008, a5[0] );
  a5 -= 2*(a1 - a2) - (a3 - a4)*3;
  BOOST_CHECK_EQUAL(  -504, a5[0] );
  BOOST_CHECK_EQUAL(     3, a1[0] );
  BOOST_CHECK_EQUAL(    12, a2[0] );
  BOOST_CHECK_EQUAL(   168, a3[0] );
  BOOST_CHECK_EQUAL(     6, a4[0] );
#endif

  a5 = 2*(a1 + a2)*3;
  BOOST_CHECK_EQUAL( 90, a5[0] );
  a5 = 2*3*(a1 + a2);
  BOOST_CHECK_EQUAL( 90, a5[0] );
  a5 = (a1 + a2)*2*3;
  BOOST_CHECK_EQUAL( 90, a5[0] );
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL( 12, a2[0] );

  a2 = +a1;
  BOOST_CHECK_EQUAL(  3, a2[0] );
  a3 = -a2;
  BOOST_CHECK_EQUAL( -3, a3[0] );
  a4 = +(a1 + a2);
  BOOST_CHECK_EQUAL(  6, a4[0] );
  a4 = +(a1 - a2);
  BOOST_CHECK_EQUAL(  0, a4[0] );
  a4 = -(a1 + a2);
  BOOST_CHECK_EQUAL( -6, a4[0] );
  a4 = -(a1 - a2);
  BOOST_CHECK_EQUAL(  0, a4[0] );
  a4 = +(a1 + a2) + a3;
  BOOST_CHECK_EQUAL(  3, a4[0] );
  a4 = -(a1 + a2) + a3;
  BOOST_CHECK_EQUAL( -9, a4[0] );
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL( -3, a3[0] );

  a4 = +5*a1;
  BOOST_CHECK_EQUAL(  15, a4[0] );
  a4 = -5*a1;
  BOOST_CHECK_EQUAL( -15, a4[0] );
  a4 = +a1*5;
  BOOST_CHECK_EQUAL(  15, a4[0] );
  a4 = -a1*5;
  BOOST_CHECK_EQUAL( -15, a4[0] );
  a4 = +(5*a1);
  BOOST_CHECK_EQUAL(  15, a4[0] );
  a4 = -(5*a1);
  BOOST_CHECK_EQUAL( -15, a4[0] );
  a4 = +(a1*5);
  BOOST_CHECK_EQUAL(  15, a4[0] );
  a4 = -(a1*5);
  BOOST_CHECK_EQUAL( -15, a4[0] );
  BOOST_CHECK_EQUAL(   3, a1[0] );
}


//----------------------------------------------------------------------------//
bool chkVectorS4( const VectorS4& z, Int a, Int b, Int c, Int d )
{
  bool isEqual = true;
  if ((z[0] != a) || (z[1] != b) || (z[2] != c) || (z[3] != d))
  {
    isEqual = false;
    cout << "actual (" << z << ")  expected "
         << "(" << a << " " << b << " " << c << " " << d << ")" << endl;
  }
  return isEqual;
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(size4)
{
  BOOST_CHECK( VectorS4::M == 4 );
  BOOST_CHECK( VectorS4::N == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(array_ops4)
{
  const Int vdata[4] = {1,2,3,4};
  VectorS4 a1(vdata, 4);
  VectorS4 a2(a1);
  VectorS4 a3, a4, a5;

  // size
  BOOST_CHECK( a1.M == 4 );
  BOOST_CHECK( a2.M == 4 );
  BOOST_CHECK( a3.M == 4 );
  BOOST_CHECK( a4.M == 4 );
  BOOST_CHECK( a5.M == 4 );

  // ctors
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a3, 0,0,0,0 ) );
  BOOST_CHECK( chkVectorS4( a4, 0,0,0,0 ) );
  BOOST_CHECK( chkVectorS4( a5, 0,0,0,0 ) );

  // assignment
  a3 = a1;
  a4 = 5;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a3, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a4, 5,5,5,5 ) );

  a2 = a3 = 3;
  BOOST_CHECK( chkVectorS4( a2, 3,3,3,3 ) );
  BOOST_CHECK( chkVectorS4( a3, 3,3,3,3 ) );

  // unary
  a2 = +a1;
  a3 = -a1;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a3, -1,-2,-3,-4 ) );

  // binary accumulation
  a2 = a1;
  a3 = a1;
  a4 = a1;
  a5 = a1;
  a2 += 5;
  a3 -= 5;
  a4 *= 5;
  a5 /= 1;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a2, 6,7,8,9 ) );
  BOOST_CHECK( chkVectorS4( a3, -4,-3,-2,-1 ) );
  BOOST_CHECK( chkVectorS4( a4, 5,10,15,20 ) );
  BOOST_CHECK( chkVectorS4( a5, 1,2,3,4 ) );

  a2 = 5;
  a3 = 5;
  a2 += a1;
  a3 -= a1;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a2, 6,7,8,9 ) );
  BOOST_CHECK( chkVectorS4( a3, 4,3,2,1 ) );

  // binary operators
//  a2 = a1 + 3;
//  a3 = a1 - 3;
  a4 = a1 * 3;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
//  BOOST_CHECK( chkVectorS4( a2, 4,5,6,7 ) );
//  BOOST_CHECK( chkVectorS4( a3, -2,-1,0,1 ) );
  BOOST_CHECK( chkVectorS4( a4, 3,6,9,12 ) );

//  a2 = 3 + a1;
//  a3 = 3 - a1;
  a4 = 3 * a1;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
//  BOOST_CHECK( chkVectorS4( a2, 4,5,6,7 ) );
//  BOOST_CHECK( chkVectorS4( a3, 2,1,0,-1 ) );
  BOOST_CHECK( chkVectorS4( a4, 3,6,9,12 ) );

  a2 = 3;
  a3 = a1 + a2;
  a4 = a1 - a2;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a2, 3,3,3,3 ) );
  BOOST_CHECK( chkVectorS4( a3, 4,5,6,7 ) );
  BOOST_CHECK( chkVectorS4( a4, -2,-1,0,1 ) );

  // arithmetic combinations

  a2 = a1;
  a3 = a1 + a2;
  a4 = a1 + a2 + a3;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a3, 2,4,6,8 ) );
  BOOST_CHECK( chkVectorS4( a4, 4,8,12,16 ) );

  a2 += a1;
  a3 += a1 + a2;
  a4 += a1 + a2 + a3;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a2, 2,4,6,8 ) );
  BOOST_CHECK( chkVectorS4( a3, 5,10,15,20 ) );
  BOOST_CHECK( chkVectorS4( a4, 12,24,36,48 ) );

  a3 = a1 - a2;
  a4 = a1 - a2 - a3;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a2, 2,4,6,8 ) );
  BOOST_CHECK( chkVectorS4( a3, -1,-2,-3,-4 ) );
  BOOST_CHECK( chkVectorS4( a4, 0,0,0,0 ) );

  a2 -= a1;
  a3 -= a1 - a2;
  a4 -= a1 - a2 - a3;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a3, -1,-2,-3,-4 ) );
  BOOST_CHECK( chkVectorS4( a4, -1,-2,-3,-4 ) );

  a3 = a1 - a2;
  a4 = a1 + a2 - a3;
  a5 = a1 - a2 + a3;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a3, 0,0,0,0 ) );
  BOOST_CHECK( chkVectorS4( a4, 2,4,6,8 ) );
  BOOST_CHECK( chkVectorS4( a5, 0,0,0,0 ) );

  a5 = (a1 + a2) + (a3 + a4);
  BOOST_CHECK( chkVectorS4( a5, 4,8,12,16 ) );
  a5 = (a1 + a2) + (a3 - a4);
  BOOST_CHECK( chkVectorS4( a5, 0,0,0,0 ) );
  a5 = (a1 + a2) - (a3 + a4);
  BOOST_CHECK( chkVectorS4( a5, 0,0,0,0 ) );
  a5 = (a1 + a2) - (a3 - a4);
  BOOST_CHECK( chkVectorS4( a5, 4,8,12,16 ) );
  a5 = (a1 - a2) + (a3 + a4);
  BOOST_CHECK( chkVectorS4( a5, 2,4,6,8 ) );
  a5 = (a1 - a2) + (a3 - a4);
  BOOST_CHECK( chkVectorS4( a5, -2,-4,-6,-8 ) );
  a5 = (a1 - a2) - (a3 + a4);
  BOOST_CHECK( chkVectorS4( a5, -2,-4,-6,-8 ) );
  a5 = (a1 - a2) - (a3 - a4);
  BOOST_CHECK( chkVectorS4( a5, 2,4,6,8 ) );
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a3, 0,0,0,0 ) );
  BOOST_CHECK( chkVectorS4( a4, 2,4,6,8 ) );

  a5 += (a1 + a2) + (a3 + a4);
  a5 += (a1 + a2) + (a3 - a4);
  a5 += (a1 + a2) - (a3 + a4);
  a5 += (a1 + a2) - (a3 - a4);
  a5 += (a1 - a2) + (a3 + a4);
  a5 += (a1 - a2) + (a3 - a4);
  a5 += (a1 - a2) - (a3 + a4);
  a5 += (a1 - a2) - (a3 - a4);
  BOOST_CHECK( chkVectorS4( a5, 10,20,30,40 ) );
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a3, 0,0,0,0 ) );
  BOOST_CHECK( chkVectorS4( a4, 2,4,6,8 ) );

  a5 -= (a1 + a2) + (a3 + a4);
  a5 -= (a1 + a2) + (a3 - a4);
  a5 -= (a1 + a2) - (a3 + a4);
  a5 -= (a1 + a2) - (a3 - a4);
  a5 -= (a1 - a2) + (a3 + a4);
  a5 -= (a1 - a2) + (a3 - a4);
  a5 -= (a1 - a2) - (a3 + a4);
  a5 -= (a1 - a2) - (a3 - a4);
  BOOST_CHECK( chkVectorS4( a5, 2,4,6,8 ) );
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a3, 0,0,0,0 ) );
  BOOST_CHECK( chkVectorS4( a4, 2,4,6,8 ) );

  a1(0) = 1;
  a1(1) = 2;
  a1(2) = 3;
  a1(3) = 4;

  a2 = 1*a1;
  a3 = a2*2;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a3, 2,4,6,8 ) );

  a2 += 1*a1;
  a3 += a2*2;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a2, 2,4,6,8 ) );
  BOOST_CHECK( chkVectorS4( a3, 6,12,18,24 ) );

  a2 -= 1*a1;
  a3 -= a2*2;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a3, 4,8,12,16 ) );

  a5 = 1*(a1 + a2) + (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 14,28,42,56 ) );
  a5 = 1*(a1 + a2) + (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 6,12,18,24 ) );
  a5 = 1*(a1 + a2) - (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -10,-20,-30,-40 ) );
  a5 = 1*(a1 + a2) - (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -2,-4,-6,-8 ) );
  a5 = 1*(a1 - a2) + (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 12,24,36,48 ) );
  a5 = 1*(a1 - a2) + (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 4,8,12,16 ) );
  a5 = 1*(a1 - a2) - (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -12,-24,-36,-48 ) );
  a5 = 1*(a1 - a2) - (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -4,-8,-12,-16 ) );
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a3, 4,8,12,16 ) );
  BOOST_CHECK( chkVectorS4( a4, 2,4,6,8 ) );

  a5 += 1*(a1 + a2) + (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 10,20,30,40 ) );
  a5 += 1*(a1 + a2) + (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 16,32,48,64 ) );
  a5 += 1*(a1 + a2) - (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 6,12,18,24 ) );
  a5 += 1*(a1 + a2) - (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 4,8,12,16 ) );
  a5 += 1*(a1 - a2) + (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 16,32,48,64 ) );
  a5 += 1*(a1 - a2) + (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 20,40,60,80 ) );
  a5 += 1*(a1 - a2) - (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 8,16,24,32 ) );
  a5 += 1*(a1 - a2) - (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 4,8,12,16 ) );
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a3, 4,8,12,16 ) );
  BOOST_CHECK( chkVectorS4( a4, 2,4,6,8 ) );

  a5 -= 1*(a1 + a2) + (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -10,-20,-30,-40 ) );
  a5 -= 1*(a1 + a2) + (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -16,-32,-48,-64 ) );
  a5 -= 1*(a1 + a2) - (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -6,-12,-18,-24 ) );
  a5 -= 1*(a1 + a2) - (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -4,-8,-12,-16 ) );
  a5 -= 1*(a1 - a2) + (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -16,-32,-48,-64 ) );
  a5 -= 1*(a1 - a2) + (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -20,-40,-60,-80 ) );
  a5 -= 1*(a1 - a2) - (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -8,-16,-24,-32 ) );
  a5 -= 1*(a1 - a2) - (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -4,-8,-12,-16 ) );
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a3, 4,8,12,16 ) );
  BOOST_CHECK( chkVectorS4( a4, 2,4,6,8 ) );

  a5 = 1*(a1 + a2)*2;
  BOOST_CHECK( chkVectorS4( a5, 4,8,12,16 ) );
  a5 = 1*2*(a1 + a2);
  BOOST_CHECK( chkVectorS4( a5, 4,8,12,16 ) );
  a5 = (a1 + a2)*1*2;
  BOOST_CHECK( chkVectorS4( a5, 4,8,12,16 ) );
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4 ) );

  a2 = +a1;
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4 ) );
  a3 = -a2;
  BOOST_CHECK( chkVectorS4( a3, -1,-2,-3,-4 ) );
  a4 = +(a1 + a2);
  BOOST_CHECK( chkVectorS4( a4, 2,4,6,8 ) );
  a4 = +(a1 - a2);
  BOOST_CHECK( chkVectorS4( a4, 0,0,0,0 ) );
  a4 = -(a1 + a2);
  BOOST_CHECK( chkVectorS4( a4, -2,-4,-6,-8 ) );
  a4 = -(a1 - a2);
  BOOST_CHECK( chkVectorS4( a4, 0,0,0,0 ) );
  a4 = +(a1 + a2) + a3;
  BOOST_CHECK( chkVectorS4( a4, 1,2,3,4 ) );
  a4 = -(a1 + a2) + a3;
  BOOST_CHECK( chkVectorS4( a4, -3,-6,-9,-12 ) );
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorS4( a3, -1,-2,-3,-4 ) );

  a4 = +1*a1;
  BOOST_CHECK( chkVectorS4( a4, 1,2,3,4 ) );
  a4 = -1*a1;
  BOOST_CHECK( chkVectorS4( a4, -1,-2,-3,-4 ) );
  a4 = +a1*1;
  BOOST_CHECK( chkVectorS4( a4, 1,2,3,4 ) );
  a4 = -a1*1;
  BOOST_CHECK( chkVectorS4( a4, -1,-2,-3,-4 ) );
  a4 = +(1*a1);
  BOOST_CHECK( chkVectorS4( a4, 1,2,3,4 ) );
  a4 = -(1*a1);
  BOOST_CHECK( chkVectorS4( a4, -1,-2,-3,-4 ) );
  a4 = +(a1*1);
  BOOST_CHECK( chkVectorS4( a4, 1,2,3,4 ) );
  a4 = -(a1*1);
  BOOST_CHECK( chkVectorS4( a4, -1,-2,-3,-4 ) );
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4 ) );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
