// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/range_c.hpp>
#include <boost/preprocessor/repetition/repeat_from_to.hpp>

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/InverseLUP.h"

#include <iostream>
using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_InverseLUP_Exceptions )
{
  MatrixS<2, 2, Real> A1, A1inv;
  A1 = 0;
  BOOST_CHECK_THROW( A1inv = InverseLUP::Inverse(A1), SingularMatrixException );
}

static const int CacheItems = CACHE_LINE_SIZE/sizeof(Real);
typedef boost::mpl::range_c<int, CacheItems/2, 2*CacheItems> MatrixSizes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MatrixS_InverseLUP_Rand, N, MatrixSizes )
{
  typedef Real T;
  static const int n = N::value;

  MatrixS<n,n,T> A(0), tmp, I, Ainv;

  I = Identity();

  for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++)
      A(i,j) = T(rand() % 101/100.) + 5*I(i,j);

  tmp = A;

  Ainv = InverseLUP::Inverse(A);

  I = Ainv*A;

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
    {
      BOOST_CHECK_CLOSE( A(i,j) , tmp(i,j), T(0.0001) );
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MatrixS_InverseLUP_Rand_Expression, N, MatrixSizes )
{
  typedef Real T;
  static const int n = N::value;

  MatrixS<n,n,T> A(0), tmp, I, Ainv;

  I = Identity();

  for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++)
      A(i,j) = T(rand() % 101/100.);

  tmp = A + 5*I;

  Ainv = InverseLUP::Inverse(A + 5*I);

  I = Ainv*tmp;

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_InverseLUP_Pivot_5x5_I )
{
  typedef Real T;

  //Pivoting check of Ainv = InverseLUP::Inverse(A)*I works properly
  MatrixS<5,5,T> Ainv, I, tmp;

  I = Identity();

  MatrixS<5,5,T> A = {{0, 0, 0, 0, 5},
                      {0, 0, 0, 5, 0},
                      {0, 0, 5, 0, 0},
                      {0, 5, 0, 0, 0},
                      {5, 0, 0, 0, 0}};

  tmp = A;
  Ainv = InverseLUP::Solve(tmp, I);
  I = Ainv*A;

  for (int i = 0; i < 5; ++i)
    for (int j = 0; j < 5; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_InverseLUP_Pivot_5x5 )
{
  typedef Real T;

  //Pivoting check of Ainv = InverseLUP::Inverse(A)*I works properly
  MatrixS<5,5,T> Ainv, I, tmp;

  I = Identity();

  MatrixS<5,5,T> A = {{5, 0, 0, 0, 0},
                      {0, 0, 5, 0, 0},
                      {0, 5, 0, 0, 0},
                      {0, 0, 0, 0, 5},
                      {0, 0, 0, 5, 0}};

  tmp = A;
  Ainv = InverseLUP::Solve(tmp, I);
  I = Ainv*A;

  for (int i = 0; i < 5; ++i)
    for (int j = 0; j < 5; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_InverseLUP_Pivot_7x7 )
{
  typedef Real T;

  //Pivoting check of Ainv = InverseLUP::Inverse(A)*I works properly
  MatrixS<7,7,T> Ainv, I, tmp;

  I = Identity();

  MatrixS<7,7,T> A = {{5, 0, 0, 0, 0, 0, 0},
                      {0, 0, 5, 0, 0, 0, 0},
                      {0, 0, 0, 5, 0, 0, 0},
                      {0, 0, 0, 0, 0, 0, 5},
                      {0, 5, 0, 0, 0, 0, 0},
                      {0, 0, 0, 0, 0, 5, 0},
                      {0, 0, 0, 0, 5, 0, 0}};

  tmp = A;
  Ainv = InverseLUP::Solve(tmp, I);
  I = Ainv*A;

  for (int i = 0; i < 7; ++i)
    for (int j = 0; j < 7; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveLUP_1x1 )
{
  typedef Real T;
  MatrixS<1,1,T> A;
  MatrixS<1,1,T> b;
  MatrixS<1,1,T> x;

  A = 2;

  b = 3;

  x = InverseLUP::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), T(2.)   , T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(3./2.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveLUP_2x2 )
{
  typedef Real T;

  MatrixS<2,2,T> A = {{2, 1},
                      {3, 2}};

  MatrixS<2,1,T> b = {{2},
                      {3}};

  MatrixS<2,1,T> x;

  x = InverseLUP::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(0.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveLUP_2x1_2b )
{
  //Check that b = InverseLUP::Inverse(A)*b works properly
  typedef Real T;

  MatrixS<2,2,T> A = {{2, 1},
                      {3, 2}};

  MatrixS<2,1,T> b = {{2},
                      {3}};

  b = InverseLUP::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( b(1,0),        T(1e-12) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveLUP_2x2_RHSExpression )
{
  typedef Real T;

  MatrixS<2,2,T> A = {{2, 1},
                      {3, 2}};

  MatrixS<2,1,T> b1 = {{1},
                       {2}};

  MatrixS<2,1,T> b2, x = 0;

  b2 = 1;

  x = InverseLUP::Solve(A, b1 + b2);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(0.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveLUP_2x2_InvExpression )
{
  typedef Real T;

  MatrixS<2,2,T> A1 = {{1, 0},
                       {2, 1}};

  MatrixS<2,1,T> b = {{2},
                      {3}};

  MatrixS<2,2,T> A2;
  MatrixS<2,1,T> x;

  A2 = 1;

  x = InverseLUP::Solve(A1 + A2, b);

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(0.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveLUP_2x2_InvExpression_2b )
{
  typedef Real T;

  MatrixS<2,2,T> A1 = {{1, 0},
                       {2, 1}};

  MatrixS<2,1,T> b = {{2},
                      {3}};

  MatrixS<2,2,T> A2;

  A2 = 1;

  b = InverseLUP::Solve(A1 + A2, b);

  BOOST_CHECK_CLOSE( b(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(0.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveLUP_2x2_Expressions )
{
  typedef Real T;

  MatrixS<2,2,T> A1 = {{1, 0},
                       {2, 1}};

  MatrixS<2,1,T> b1 = {{1},
                       {2}};

  MatrixS<2,2,T> A2;
  MatrixS<2,1,T> x, b2;

  A2 = 1;
  b2 = 1;

  x = InverseLUP::Solve(A1 + A2, b1 + b2);

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(0.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveLUP_2x2_Expressions_2 )
{
  typedef Real T;

  MatrixS<2,2,T> A1 = {{1, 0},
                       {2, 1}};

  MatrixS<2,1,T> b1 = {{1},
                       {2}};

  MatrixS<2,2,T> A2;
  MatrixS<2,1,T> x, b2;

  A2 = 1;
  b2 = 1;

  x = InverseLUP::Solve(A1 + A2, b1 + b2) + b2;

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(1.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveLUP_2x1_MulScal )
{
  typedef Real T;

  MatrixS<2,2,T> A = {{2, 1},
                      {3, 2}};

  MatrixS<2,1,T> b = {{2},
                      {3}};

  MatrixS<2,1,T> x;

  x = 0; // cppcheck-suppress redundantAssignment
  x = 2*InverseLUP::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = InverseLUP::Solve(A, b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = InverseLUP::Solve(A, b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -InverseLUP::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-1.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -InverseLUP::Solve(A, b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -InverseLUP::Solve(A, b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -(2*InverseLUP::Solve(A, b)) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-4.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveLUP_3x3 )
{
  typedef Real T;

  MatrixS<3,3,T> A = {{1, 2, 3},
                      {2, 4, 5},
                      {3, 5, 6}};

  MatrixS<3,1,T> b = {{1}, {3}, {2}};

  MatrixS<3,1,T> x = InverseLUP::Solve(A, b);

  BOOST_CHECK_CLOSE( x(0,0), T(-4.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0),  T(4.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(2,0), T(-1.), T(0.0001) );


  MatrixS<3,3,T> B = {{1, 2, 1},
                      {2, 3, 1},
                      {1, 1, 2}};

  MatrixS<3,3,T> X = InverseLUP::Solve(A, B);

  BOOST_CHECK_CLOSE( X(0,0), T(-3.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(0,1), T(-5.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(0,2), T( 2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,0), T( 2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,1), T( 2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,2), T(-2.), T(0.0001) );
  BOOST_CHECK_SMALL( X(2,0),         T(1e-12) );
  BOOST_CHECK_CLOSE( X(2,1), T( 1.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,2), T( 1.), T(0.0001) );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveLUP_2x2_NonSymVec )
{
  typedef Real T;

  MatrixS<2,2,T> A = {{1, 2},
                      {5, 6}};

  MatrixS<2,3,T> b = {{2, 4, 5},
                      {3, 2, 1}};

  MatrixS<2,3,T> x = InverseLUP::Solve(A, b);

  BOOST_CHECK_CLOSE( x(0,0), T(-1.50), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,1), T(-5.00), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,2), T(-7.00), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T( 1.75), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,1), T( 4.50), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,2), T( 6.00), T(0.0001) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveLUP_2x2_MixedTypes )
{
  typedef Real T;

  MatrixS<2,2,T> A = {{2, 1},
                      {3, 2}};
  MatrixS< 2,1, VectorS<1,T> > b = {{2},
                                    {3}};
  MatrixS< 2,1, VectorS<1,T> > x;

  x = InverseLUP::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0)[0], T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0)[0], T(0.), T(0.0001) );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MatrixS_InverseLUP_Rand_Factorize, N, MatrixSizes )
{
  typedef Real T;
  static const int n = N::value;

  MatrixS<n,n,T> A(0), tmp, I, Ainv;

  I = Identity();

  for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++)
      A(i,j) = T(rand() % 101/100.) + 5*I(i,j);

  tmp = A;

  auto LUP = InverseLUP::Factorize(A);
  Ainv = LUP.backsolve(I);

  I = Ainv*A;

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
    {
      BOOST_CHECK_CLOSE( A(i,j) , tmp(i,j), T(0.0001) );
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }

  I = Identity();

  // Solve the system a 2nd time and make sure it still works
  Ainv = 0;  // cppcheck-suppress redundantAssignment
  Ainv = LUP.backsolve(I);

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
    {
      BOOST_CHECK_CLOSE( A(i,j) , tmp(i,j), T(0.0001) );
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MatrixS_InverseLUP_Rand_Expression_Factorize, N, MatrixSizes )
{
  typedef Real T;
  static const int n = N::value;

  MatrixS<n,n,T> A, tmp, I, Ainv;

  I = Identity();

  for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++)
      A(i,j) = T(rand() % 101/100.);

  tmp = A + 5*I;

  auto LUP = InverseLUP::Factorize(A + 5*I);
  Ainv = LUP.backsolve(I);

  I = Ainv*tmp;

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }

  I = Identity();

  // Solve the system a 2nd time and make sure it still works
  Ainv = 0;  // cppcheck-suppress redundantAssignment
  Ainv = LUP.backsolve(I);

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }

  I = Identity();

  // Solve the system a 3rd time and make sure it still works
  Ainv = 0;  // cppcheck-suppress redundantAssignment
  Ainv = LUP.backsolve(I - I*I + I);

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeLUP_1x1 )
{
  typedef Real T;
  MatrixS<1,1,T> A;
  MatrixS<1,1,T> b;
  MatrixS<1,1,T> x;


  A = 2;

  b = 3;

  auto LUP = InverseLUP::Factorize(A);
  x = LUP.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), T(2.)   , T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(3./2.), T(0.0001) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeLUP_2x2 )
{
  typedef Real T;

  Real Avals[] = {2, 1,
                  3, 2};

  Real bvals[] = {2,
                  3};

  MatrixS<2,2,T> A(Avals, 2*2);
  MatrixS<2,1,T> b(bvals, 2*1);
  MatrixS<2,1,T> x;

  auto LUP = InverseLUP::Factorize(A);
  x = LUP.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(0.), T(0.0001) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeLUP_2x1_2b )
{
  //Check that b = LUP.backsolve(b) works properly
  typedef Real T;

  T Avals[] = {2, 1,
               3, 2};

  T bvals[] = {2,
               3};

  MatrixS<2,2,T> A(Avals, 2*2);
  MatrixS<2,1,T> b(bvals, 2*1);

  auto LUP = InverseLUP::Factorize(A);
  b = LUP.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( b(1,0),        T(1e-12) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeLUP_2x2_RHSExpression )
{
  typedef Real T;

  Real Avals[] = {2, 1,
                  3, 2};

  Real bvals[] = {1,
                  2};

  MatrixS<2,2,T> A(Avals, 2*2);
  MatrixS<2,1,T> b1(bvals, 2*1), b2;
  MatrixS<2,1,T> x;

  b2 = 1;

  auto LUP = InverseLUP::Factorize(A);
  x = LUP.backsolve( b1 + b2 );

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(0.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeLUP_2x2_InvExpression )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {2,
                  3};

  MatrixS<2,2,T> A1(Avals, 2*2), A2(Avals, 2*2);
  MatrixS<2,1,T> b(bvals, 2*1);
  MatrixS<2,1,T> x;

  A2 = 1;

  auto LUP = InverseLUP::Factorize(A1 + A2);
  x = LUP.backsolve(b);

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(0.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeLUP_2x2_InvExpression_2b )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {2,
                  3};

  MatrixS<2,2,T> A1(Avals, 2*2), A2(Avals, 2*2);
  MatrixS<2,1,T> b(bvals, 2*1);

  A2 = 1;

  auto LUP = InverseLUP::Factorize(A1 + A2);
  b = LUP.backsolve(b);

  BOOST_CHECK_CLOSE( b(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(0.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeLUP_2x2_Expressions )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {1,
                  2};

  MatrixS<2,2,T> A1(Avals, 2*2), A2(Avals, 2*2);
  MatrixS<2,1,T> b1(bvals, 2*1), b2;
  MatrixS<2,1,T> x;

  A2 = 1;
  b2 = 1;

  auto LUP = InverseLUP::Factorize(A1 + A2);
  x = LUP.backsolve(b1 + b2);

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(0.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeLUP_2x2_Expressions_2 )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {1,
                  2};

  MatrixS<2,2,T> A1(Avals, 2*2), A2(Avals, 2*2);
  MatrixS<2,1,T> b1(bvals, 2*1), b2;
  MatrixS<2,1,T> x;

  A2 = 1;
  b2 = 1;

  auto LUP = InverseLUP::Factorize(A1 + A2);
  x = LUP.backsolve(b1 + b2) + b2;

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(1.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeLUP_2x1_MulScal )
{
  //Check that b = !A*b works properly
  typedef Real T;

  T Avals[] = {2, 1,
               3, 2};

  T bvals[] = {2,
               3};

  MatrixS<2,2,T> A(Avals, 2*2);
  MatrixS<2,1,T> b(bvals, 2*1);
  MatrixS<2,1,T> x;

  auto LUP = InverseLUP::Factorize(A);

  x = 0; // cppcheck-suppress redundantAssignment
  x = 2*LUP.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = LUP.backsolve(b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = LUP.backsolve(b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -LUP.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-1.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -LUP.backsolve(b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -LUP.backsolve(b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = - 2 * LUP.backsolve(b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-4.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeLUP_3x3 )
{
  typedef Real T;

  Real Adata[] = {4, 2, 2,
                  2, 4, 6,
                  2, 6, 6};

  Real bdata[] = {1, 3, 2};

  MatrixS<3,3,T> A(Adata, 3*3);
  MatrixS<3,1,T> b(bdata, 3*1);

  auto LUP = InverseLUP::Factorize(A);

  MatrixS<3,1,T> x = LUP.backsolve(b);

  BOOST_CHECK_CLOSE( x(0,0),   T(0.1), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0),  T(-0.5), T(0.0001) );
  BOOST_CHECK_CLOSE( x(2,0), T(4./5.), T(0.0001) );


  Real Bdata[] = {1, 2, 1,
                  2, 3, 1,
                  1, 1, 2};

  MatrixS<3,3,T> B(Bdata, 3*3);

  MatrixS<3,3,T> X = LUP.backsolve(B);

  BOOST_CHECK_CLOSE( X(0,0), T( 1./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(0,1), T( 1./2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(0,2), T(1./10.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,0), T(-1./2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,1), T(-1.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,2), T( 1./2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,0), T( 3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,1), T( 1.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,2), T(-1./5.), T(0.0001) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeLUP_2x2_NonSymVec )
{
  typedef Real T;

  Real Adata[] = {1, 2,
                  5, 6};

  Real bdata[] = {2, 4, 5,
                  3, 2, 1};

  MatrixS<2,2,T> A(Adata, 4);
  MatrixS<2,3,T> b(bdata, 6);

  auto LUP = InverseLUP::Factorize(A);
  MatrixS<2,3,T> x = LUP.backsolve(b);

  BOOST_CHECK_CLOSE( x(0,0), T(-1.50), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,1), T(-5.00), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,2), T(-7.00), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T( 1.75), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,1), T( 4.50), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,2), T( 6.00), T(0.0001) );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
