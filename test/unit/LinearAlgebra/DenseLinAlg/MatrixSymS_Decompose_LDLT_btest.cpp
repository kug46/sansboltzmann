// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS_Decompose_LDLT.h"
#include "LinearAlgebra/DenseLinAlg/tools/SingularException.h"

#include <iostream>
using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_Decompose_LDLT_Exceptions )
{
  MatrixSymS<2,Real> A1, LL1;
  A1 = 0;
  BOOST_CHECK_THROW( Decompose_LDLT(A1, LL1), SingularMatrixException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_Decompose_LDLT_1x1 )
{
  typedef Real T;
  MatrixSymS<1,T> A;
  MatrixSymS<1,T> LD;

  A = 2;

  Decompose_LDLT(A, LD);

  BOOST_CHECK_CLOSE( A(0,0), T(2.), close_tol );

  BOOST_CHECK_CLOSE( LD(0,0), T(2.), close_tol );

#if 0
  //Set to zero so we know it gets filled properly
  LD = 0;

  // cppcheck-suppress redundantAssignment
  LD = Decompose_LDLT(A);

  BOOST_CHECK_CLOSE( A(0,0), T(2.), close_tol );

  BOOST_CHECK_CLOSE( LD(0,0), T(2.), close_tol );
#endif
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_Decompose_LDLT_2x2 )
{
  typedef Real T;
  const Real close_tol = 1e-10;

  MatrixSymS<2,T> A = {{1},
                       {1, 2}};
  MatrixSymS<2,T> LD;

  Decompose_LDLT(A, LD);

  BOOST_CHECK_CLOSE( A(0,0), 1., close_tol );
  BOOST_CHECK_CLOSE( A(0,1), 1., close_tol );
  BOOST_CHECK_CLOSE( A(1,0), 1., close_tol );
  BOOST_CHECK_CLOSE( A(1,1), 2., close_tol );

  BOOST_CHECK_CLOSE( LD(0,0), 1., close_tol );
  BOOST_CHECK_CLOSE( LD(0,1), 1., close_tol );
  BOOST_CHECK_CLOSE( LD(1,0), 1., close_tol );
  BOOST_CHECK_CLOSE( LD(1,1), 1., close_tol );


  // Hilbert matrix
  A = {{1.},
       {1./2., 1./3.}};

  Decompose_LDLT(A, LD);

  BOOST_CHECK_CLOSE( A(0,0),    1., close_tol );
  BOOST_CHECK_CLOSE( A(0,1), 1./2., close_tol );
  BOOST_CHECK_CLOSE( A(1,0), 1./2., close_tol );
  BOOST_CHECK_CLOSE( A(1,1), 1./3., close_tol );

  BOOST_CHECK_CLOSE( LD(0,0),    1., close_tol );
  BOOST_CHECK_CLOSE( LD(0,1),  1./2., close_tol );
  BOOST_CHECK_CLOSE( LD(1,0),  1./2., close_tol );
  BOOST_CHECK_CLOSE( LD(1,1), 1./12., close_tol );

#if 0
  LD = 0;

  // cppcheck-suppress redundantAssignment
  LD = Decompose_LDLT(A);

  BOOST_CHECK_CLOSE( A(0,0), 2., close_tol );
  BOOST_CHECK_CLOSE( A(0,1), 1., close_tol );
  BOOST_CHECK_CLOSE( A(1,0), 3., close_tol );
  BOOST_CHECK_CLOSE( A(1,1), 2., close_tol );

  BOOST_CHECK_CLOSE( LD(0,0),    2., close_tol );
  BOOST_CHECK_CLOSE( LD(0,1), 1./2., close_tol );
  BOOST_CHECK_CLOSE( LD(1,0),    3., close_tol );
  BOOST_CHECK_CLOSE( LD(1,1), 1./2., close_tol );
#endif
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_Decompose_LDLT_2x2_AExpression )
{
  typedef Real T;

  MatrixSymS<2,T> A1 = {{2},
                        {1, 3}};
  MatrixSymS<2,T> LD, A2;

  A2 = 1;

  Decompose_LDLT(A1 + A2, LD);

  BOOST_CHECK_CLOSE( LD(0,0),    sqrt(3.), close_tol );
  BOOST_CHECK_CLOSE( LD(0,1), 2./sqrt(3.), close_tol );
  BOOST_CHECK_CLOSE( LD(1,0), 2./sqrt(3.), close_tol );
  BOOST_CHECK_CLOSE( LD(1,1), sqrt(2./3.), close_tol );


  LD = 0;

  // cppcheck-suppress redundantAssignment
  LD = Decompose_LDLT(A1 + A2);

  BOOST_CHECK_CLOSE( LD(0,0),    2., close_tol );
  BOOST_CHECK_CLOSE( LD(0,1), 1./2., close_tol );
  BOOST_CHECK_CLOSE( LD(1,0),    3., close_tol );
  BOOST_CHECK_CLOSE( LD(1,1), 1./2., close_tol );

}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_Decompose_LDLT_3x3 )
{
  typedef Real T;
  const Real close_tol = 1e-10;

  // Hilbert matrix
  MatrixSymS<3,T> A = {{1,},
                       {1./2., 1./3.},
                       {1./3., 1./4., 1./5.}};
  MatrixSymS<3,T> LD;

  Decompose_LDLT(A, LD);

  BOOST_CHECK_CLOSE( LD(0,0), T( 1.      ), close_tol );
  BOOST_CHECK_CLOSE( LD(0,1), T( 1./2.   ), close_tol );
  BOOST_CHECK_CLOSE( LD(0,2), T( 1./3.   ), close_tol );
  BOOST_CHECK_CLOSE( LD(1,0), T( 1./2.   ), close_tol );
  BOOST_CHECK_CLOSE( LD(1,1), T( 1/12.   ), close_tol );
  BOOST_CHECK_CLOSE( LD(1,2), T( 1.      ), close_tol );
  BOOST_CHECK_CLOSE( LD(2,0), T( 1./3.   ), close_tol );
  BOOST_CHECK_CLOSE( LD(2,1), T( 1.      ), close_tol );
  BOOST_CHECK_CLOSE( LD(2,2), T( 1./180. ), close_tol );

  // not positive definite
  A = {{1,},
       {1., 2.},
       {1., 2., 1.}};

  Decompose_LDLT(A, LD);

  BOOST_CHECK_CLOSE( LD(0,0), T(  1. ), close_tol );
  BOOST_CHECK_CLOSE( LD(0,1), T(  1. ), close_tol );
  BOOST_CHECK_CLOSE( LD(0,2), T(  1. ), close_tol );
  BOOST_CHECK_CLOSE( LD(1,0), T(  1. ), close_tol );
  BOOST_CHECK_CLOSE( LD(1,1), T(  1. ), close_tol );
  BOOST_CHECK_CLOSE( LD(1,2), T(  1. ), close_tol );
  BOOST_CHECK_CLOSE( LD(2,0), T(  1. ), close_tol );
  BOOST_CHECK_CLOSE( LD(2,1), T(  1. ), close_tol );
  BOOST_CHECK_CLOSE( LD(2,2), T( -1. ), close_tol );

#if 0
  LD = 0;

  // cppcheck-suppress redundantAssignment
  LD = Decompose_LDLT(A);

  BOOST_CHECK_CLOSE( LD(0,0), T(  5. ), close_tol );
  BOOST_CHECK_CLOSE( LD(0,1), T(  3. ), close_tol );
  BOOST_CHECK_CLOSE( LD(0,2), T( -1. ), close_tol );
  BOOST_CHECK_CLOSE( LD(1,0), T(  3. ), close_tol );
  BOOST_CHECK_CLOSE( LD(1,1), T(  3. ), close_tol );
  BOOST_CHECK_CLOSE( LD(1,2), T(  1. ), close_tol );
  BOOST_CHECK_CLOSE( LD(2,0), T( -1. ), close_tol );
  BOOST_CHECK_CLOSE( LD(2,1), T(  1. ), close_tol );
  BOOST_CHECK_CLOSE( LD(2,2), T(  3. ), close_tol );
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_Decompose_LDLT_4x4 )
{
  typedef Real T;
  const Real close_tol = 1e-10;

  // Hilbert matrix
  MatrixSymS<4,T> A = {{1,},
                       {1./2., 1./3.},
                       {1./3., 1./4., 1./5.},
                       {1./4., 1./5., 1./6., 1./7.}};
  MatrixSymS<4,T> LD;

  Decompose_LDLT(A, LD);

  BOOST_CHECK_CLOSE( LD(0,0), T( 1.    ), close_tol );
  BOOST_CHECK_CLOSE( LD(0,1), T( 1./2. ), close_tol );
  BOOST_CHECK_CLOSE( LD(0,2), T( 1./3. ), close_tol );
  BOOST_CHECK_CLOSE( LD(0,3), T( 1./4. ), close_tol );

  BOOST_CHECK_CLOSE( LD(1,0), T( 1./2.  ), close_tol );
  BOOST_CHECK_CLOSE( LD(1,1), T( 1./12. ), close_tol );
  BOOST_CHECK_CLOSE( LD(1,2), T( 1.     ), close_tol );
  BOOST_CHECK_CLOSE( LD(1,3), T( 9./10. ), close_tol );

  BOOST_CHECK_CLOSE( LD(2,0), T( 1./3.   ), close_tol );
  BOOST_CHECK_CLOSE( LD(2,1), T( 1.      ), close_tol );
  BOOST_CHECK_CLOSE( LD(2,2), T( 1./180. ), close_tol );
  BOOST_CHECK_CLOSE( LD(2,3), T( 3./2.   ), close_tol );

  BOOST_CHECK_CLOSE( LD(3,0), T( 1./4.   ), close_tol );
  BOOST_CHECK_CLOSE( LD(3,1), T( 9./10.  ), close_tol );
  BOOST_CHECK_CLOSE( LD(3,2), T( 3./2.   ), close_tol );
  BOOST_CHECK_CLOSE( LD(3,3), T( 1./2800 ), close_tol );

#if 0
  LD = 0;

  // cppcheck-suppress redundantAssignment
  LD = Decompose_LDLT(A);

  BOOST_CHECK_CLOSE( LD(0,0), T(  3*sqrt(2.) ), close_tol );
  BOOST_CHECK_CLOSE( LD(0,1), T(  (11*sqrt(2.))/3 ), close_tol );
  BOOST_CHECK_CLOSE( LD(0,2), T( 9*sqrt(2.) ), close_tol );
  BOOST_CHECK_CLOSE( LD(0,3), T( 7*sqrt(2.) ), close_tol );

  BOOST_CHECK_CLOSE( LD(1,0), T(  (11*sqrt(2.))/3 ), close_tol );
  BOOST_CHECK_CLOSE( LD(1,1), T(  (2*sqrt(97.))/3 ), close_tol );
  BOOST_CHECK_CLOSE( LD(1,2), T( 30/sqrt(97.) ), close_tol );
  BOOST_CHECK_CLOSE( LD(1,3), T( 16/sqrt(97.) ), close_tol );

  BOOST_CHECK_CLOSE( LD(2,0), T( 9*sqrt(2.) ), close_tol );
  BOOST_CHECK_CLOSE( LD(2,1), T( 30/sqrt(97.) ), close_tol );
  BOOST_CHECK_CLOSE( LD(2,2), T( 2*sqrt(66./97.) ), close_tol );
  BOOST_CHECK_CLOSE( LD(2,3), T( 74*sqrt(2./3201.) ), close_tol );

  BOOST_CHECK_CLOSE( LD(3,0), T( 7*sqrt(2.) ), close_tol );
  BOOST_CHECK_CLOSE( LD(3,1), T( 16/sqrt(97.) ), close_tol );
  BOOST_CHECK_CLOSE( LD(3,2), T( 74*sqrt(2./3201.) ), close_tol );
  BOOST_CHECK_CLOSE( LD(3,3), T( 8/sqrt(33) ), close_tol );
#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
