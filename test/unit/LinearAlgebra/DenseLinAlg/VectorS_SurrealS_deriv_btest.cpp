// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// VectorS_SurrealS_deriv_btest
// testing of VectorS<N,T> class with T = SurrealS<1>
// tests of differentiation

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "Surreal/SurrealS.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
template class SurrealS<1>;
namespace SANS
{
namespace DLA
{
template class VectorS<1, SurrealS<1> >;
template class VectorS<4, SurrealS<1> >;
}
}

using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( VectorS_SurrealS_deriv_test_suite )


typedef SurrealS<1> Int;

typedef VectorS<1,Int> VectorS1;
typedef VectorS<4,Int> VectorS4;


//----------------------------------------------------------------------------//
bool chkVectorS1( const VectorS1& z, int v, int d )
{
  bool isEqual = true;
  if (z[0].value() != v)  isEqual = false;
  if (z[0].deriv() != d)  isEqual = false;
  if (!isEqual)
  {
    cout << "actual (" << z << ")  "
         << "expected (" << Int(v,d) << ")  "
         << "diff (" << z[0]-Int(v,d) << ")" << endl;
  }
  return isEqual;
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( size1 )
{
  BOOST_CHECK( VectorS1::M == 1 );
  BOOST_CHECK( VectorS1::N == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( array_ops1 )
{
  const Int data(3, 5);     // {value,deriv} = {3,5}
  VectorS1 a1(&data, 1);
  VectorS1 a2(a1);
  VectorS1 a3(1), a4, a5;

  // size
  BOOST_CHECK( a1.M == 1 );
  BOOST_CHECK( a2.M == 1 );
  BOOST_CHECK( a3.M == 1 );
  BOOST_CHECK( a4.M == 1 );
  BOOST_CHECK( a5.M == 1 );

  // ctors
  BOOST_CHECK( chkVectorS1( a1, 3, 5 ) );
  BOOST_CHECK( chkVectorS1( a2, 3, 5 ) );
  BOOST_CHECK( chkVectorS1( a3, 1, 0 ) );

  // assignment
  a3 = a1;
  a4 = data;
  BOOST_CHECK( chkVectorS1( a1, 3, 5 ) );
  BOOST_CHECK( chkVectorS1( a3, 3, 5 ) );
  BOOST_CHECK( chkVectorS1( a4, 3, 5 ) );

  a1 = a2 = a3 = data;
  BOOST_CHECK( chkVectorS1( a1, 3, 5 ) );
  BOOST_CHECK( chkVectorS1( a2, 3, 5 ) );
  BOOST_CHECK( chkVectorS1( a3, 3, 5 ) );

  a4 = data;
  a1 = a2 = a3 = a4;
  BOOST_CHECK( chkVectorS1( a1, 3, 5 ) );
  BOOST_CHECK( chkVectorS1( a2, 3, 5 ) );
  BOOST_CHECK( chkVectorS1( a3, 3, 5 ) );
  BOOST_CHECK( chkVectorS1( a4, 3, 5 ) );

  // unary
  a2 = +a1;
  a3 = -a1;
  BOOST_CHECK( chkVectorS1( a1, 3, 5 ) );
  BOOST_CHECK( chkVectorS1( a2, 3, 5 ) );
  BOOST_CHECK( chkVectorS1( a3, -3, -5 ) );

  // binary accumulation
  a2 = a1;
  a3 = a1;
  a1 += data;
  a2 -= data;
  a3 *= data;
  BOOST_CHECK( chkVectorS1( a1, 6, 10 ) );
  BOOST_CHECK( chkVectorS1( a2, 0,  0 ) );
  BOOST_CHECK( chkVectorS1( a3, 9, 30 ) );

  a1 = data;
  a2 = a1;
  a3 = a1;
  a2 += a1;
  a3 -= a1;
  BOOST_CHECK( chkVectorS1( a1, 3,  5 ) );
  BOOST_CHECK( chkVectorS1( a2, 6, 10 ) );
  BOOST_CHECK( chkVectorS1( a3, 0,  0 ) );

  // binary operators
  a1 = data;
//  a2 = a1 + data;
//  a3 = a1 - data;
  a4 = a1 * data;
  BOOST_CHECK( chkVectorS1( a1, 3,  5 ) );
//  BOOST_CHECK( chkVectorS1( a2, 6, 10 ) );
//  BOOST_CHECK( chkVectorS1( a3, 0,  0 ) );
  BOOST_CHECK( chkVectorS1( a4, 9, 30 ) );

//  a2 = data + a1;
//  a3 = data - a1;
  a4 = data * a1;
  BOOST_CHECK( chkVectorS1( a1, 3,  5 ) );
//  BOOST_CHECK( chkVectorS1( a2, 6, 10 ) );
//  BOOST_CHECK( chkVectorS1( a3, 0,  0 ) );
  BOOST_CHECK( chkVectorS1( a4, 9, 30 ) );

  a1 = a2 = data;
  a3 = a1 + a2;
  a4 = a1 - a2;
  BOOST_CHECK( chkVectorS1( a1, 3,  5 ) );
  BOOST_CHECK( chkVectorS1( a2, 3,  5 ) );
  BOOST_CHECK( chkVectorS1( a3, 6, 10 ) );
  BOOST_CHECK( chkVectorS1( a4, 0,  0 ) );

  // arithmetic combinations

  a1 = a2 = data;
  a3 = a1 + a2;
  a4 = a1 + a2 + a3;
  BOOST_CHECK( chkVectorS1( a1,  3,  5 ) );
  BOOST_CHECK( chkVectorS1( a2,  3,  5 ) );
  BOOST_CHECK( chkVectorS1( a3,  6, 10 ) );
  BOOST_CHECK( chkVectorS1( a4, 12, 20 ) );

  a2 += a1;
  a3 += a1 + a2;
  a4 += a1 + a2 + a3;
  BOOST_CHECK( chkVectorS1( a1,  3,  5 ) );
  BOOST_CHECK( chkVectorS1( a2,  6, 10 ) );
  BOOST_CHECK( chkVectorS1( a3, 15, 25 ) );
  BOOST_CHECK( chkVectorS1( a4, 36, 60 ) );

  a3 = a1 - a2;
  a4 = a1 - a2 - a3;
  BOOST_CHECK( chkVectorS1( a1,  3,  5 ) );
  BOOST_CHECK( chkVectorS1( a2,  6, 10 ) );
  BOOST_CHECK( chkVectorS1( a3, -3, -5 ) );
  BOOST_CHECK( chkVectorS1( a4,  0,  0 ) );

  a2 -= a1;
  a3 -= a1 - a2;
  a4 -= a1 - a2 - a3;
  BOOST_CHECK( chkVectorS1( a1,  3,  5 ) );
  BOOST_CHECK( chkVectorS1( a2,  3,  5 ) );
  BOOST_CHECK( chkVectorS1( a3, -3, -5 ) );
  BOOST_CHECK( chkVectorS1( a4, -3, -5 ) );

  a3 = a1 - a2;
  a4 = a1 + a2 - a3;
  a5 = a1 - a2 + a3;
  BOOST_CHECK( chkVectorS1( a1, 3,  5 ) );
  BOOST_CHECK( chkVectorS1( a2, 3,  5 ) );
  BOOST_CHECK( chkVectorS1( a3, 0,  0 ) );
  BOOST_CHECK( chkVectorS1( a4, 6, 10 ) );
  BOOST_CHECK( chkVectorS1( a5, 0,  0 ) );

  a5 = (a1 + a2) + (a3 + a4);
  BOOST_CHECK( chkVectorS1( a5, 12, 20 ) );
  a5 = (a1 + a2) + (a3 - a4);
  BOOST_CHECK( chkVectorS1( a5, 0,  0 ) );
  a5 = (a1 + a2) - (a3 + a4);
  BOOST_CHECK( chkVectorS1( a5, 0,  0 ) );
  a5 = (a1 + a2) - (a3 - a4);
  BOOST_CHECK( chkVectorS1( a5, 12, 20 ) );
  a5 = (a1 - a2) + (a3 + a4);
  BOOST_CHECK( chkVectorS1( a5, 6, 10 ) );
  a5 = (a1 - a2) + (a3 - a4);
  BOOST_CHECK( chkVectorS1( a5, -6, -10 ) );
  a5 = (a1 - a2) - (a3 + a4);
  BOOST_CHECK( chkVectorS1( a5, -6, -10 ) );
  a5 = (a1 - a2) - (a3 - a4);
  BOOST_CHECK( chkVectorS1( a5, 6, 10 ) );
  BOOST_CHECK( chkVectorS1( a1, 3,  5 ) );
  BOOST_CHECK( chkVectorS1( a2, 3,  5 ) );
  BOOST_CHECK( chkVectorS1( a3, 0,  0 ) );
  BOOST_CHECK( chkVectorS1( a4, 6, 10 ) );

  a5 += (a1 + a2) + (a3 + a4);
  a5 += (a1 + a2) + (a3 - a4);
  a5 += (a1 + a2) - (a3 + a4);
  a5 += (a1 + a2) - (a3 - a4);
  a5 += (a1 - a2) + (a3 + a4);
  a5 += (a1 - a2) + (a3 - a4);
  a5 += (a1 - a2) - (a3 + a4);
  a5 += (a1 - a2) - (a3 - a4);
  BOOST_CHECK( chkVectorS1( a5, 30, 50 ) );
  BOOST_CHECK( chkVectorS1( a1,  3,  5 ) );
  BOOST_CHECK( chkVectorS1( a2,  3,  5 ) );
  BOOST_CHECK( chkVectorS1( a3,  0,  0 ) );
  BOOST_CHECK( chkVectorS1( a4,  6, 10 ) );

  a5 -= (a1 + a2) + (a3 + a4);
  a5 -= (a1 + a2) + (a3 - a4);
  a5 -= (a1 + a2) - (a3 + a4);
  a5 -= (a1 + a2) - (a3 - a4);
  a5 -= (a1 - a2) + (a3 + a4);
  a5 -= (a1 - a2) + (a3 - a4);
  a5 -= (a1 - a2) - (a3 + a4);
  a5 -= (a1 - a2) - (a3 - a4);
  BOOST_CHECK( chkVectorS1( a5,  6, 10 ) );
  BOOST_CHECK( chkVectorS1( a1,  3,  5 ) );
  BOOST_CHECK( chkVectorS1( a2,  3,  5 ) );
  BOOST_CHECK( chkVectorS1( a3,  0,  0 ) );
  BOOST_CHECK( chkVectorS1( a4,  6, 10 ) );

  a1 = data;

  a2 = 4.*a1;
  a3 = a2*7;
  BOOST_CHECK( chkVectorS1( a1,  3,   5 ) );
  BOOST_CHECK( chkVectorS1( a2, 12,  20 ) );
  BOOST_CHECK( chkVectorS1( a3, 84, 140 ) );

  a2 += 4*a1;
  a3 += a2*7;
  BOOST_CHECK( chkVectorS1( a1,  3,    5 ) );
  BOOST_CHECK( chkVectorS1( a2, 24,   40 ) );
  BOOST_CHECK( chkVectorS1( a3, 252, 420 ) );

  a2 -= 4*a1;
  a3 -= a2*7;
  BOOST_CHECK( chkVectorS1( a1,  3,    5 ) );
  BOOST_CHECK( chkVectorS1( a2, 12,   20 ) );
  BOOST_CHECK( chkVectorS1( a3, 168, 280 ) );

  a5 = 2*(a1 + a2) + (a3 + a4)*3;
  BOOST_CHECK( chkVectorS1( a5,  552,  920 ) );
  a5 = 2*(a1 + a2) + (a3 - a4)*3;
  BOOST_CHECK( chkVectorS1( a5,  516,  860 ) );
  a5 = 2*(a1 + a2) - (a3 + a4)*3;
  BOOST_CHECK( chkVectorS1( a5, -492, -820 ) );
  a5 = 2*(a1 + a2) - (a3 - a4)*3;
  BOOST_CHECK( chkVectorS1( a5, -456, -760 ) );
  a5 = 2*(a1 - a2) + (a3 + a4)*3;
  BOOST_CHECK( chkVectorS1( a5,  504,  840 ) );
  a5 = 2*(a1 - a2) + (a3 - a4)*3;
  BOOST_CHECK( chkVectorS1( a5,  468,  780 ) );
  a5 = 2*(a1 - a2) - (a3 + a4)*3;
  BOOST_CHECK( chkVectorS1( a5, -540, -900 ) );
  a5 = 2*(a1 - a2) - (a3 - a4)*3;
  BOOST_CHECK( chkVectorS1( a5, -504, -840 ) );
  BOOST_CHECK( chkVectorS1( a1,    3,    5 ) );
  BOOST_CHECK( chkVectorS1( a2,   12,   20 ) );
  BOOST_CHECK( chkVectorS1( a3,  168,  280 ) );
  BOOST_CHECK( chkVectorS1( a4,    6,   10 ) );

  a5 += 2*(a1 + a2) + (a3 + a4)*3;
  BOOST_CHECK( chkVectorS1( a5,   48,   80 ) );
  a5 += 2*(a1 + a2) + (a3 - a4)*3;
  BOOST_CHECK( chkVectorS1( a5,  564,  940 ) );
  a5 += 2*(a1 + a2) - (a3 + a4)*3;
  BOOST_CHECK( chkVectorS1( a5,   72,  120 ) );
  a5 += 2*(a1 + a2) - (a3 - a4)*3;
  BOOST_CHECK( chkVectorS1( a5, -384, -640 ) );
  a5 += 2*(a1 - a2) + (a3 + a4)*3;
  BOOST_CHECK( chkVectorS1( a5,  120,  200 ) );
  a5 += 2*(a1 - a2) + (a3 - a4)*3;
  BOOST_CHECK( chkVectorS1( a5,  588,  980 ) );
  a5 += 2*(a1 - a2) - (a3 + a4)*3;
  BOOST_CHECK( chkVectorS1( a5,   48,   80 ) );
  a5 += 2*(a1 - a2) - (a3 - a4)*3;
  BOOST_CHECK( chkVectorS1( a5, -456, -760 ) );
  BOOST_CHECK( chkVectorS1( a1,    3,    5 ) );
  BOOST_CHECK( chkVectorS1( a2,   12,   20 ) );
  BOOST_CHECK( chkVectorS1( a3,  168,  280 ) );
  BOOST_CHECK( chkVectorS1( a4,    6,   10 ) );

  a5 -= 2*(a1 + a2) + (a3 + a4)*3;
  BOOST_CHECK( chkVectorS1( a5, -1008, -1680 ) );
  a5 -= 2*(a1 + a2) + (a3 - a4)*3;
  BOOST_CHECK( chkVectorS1( a5, -1524, -2540 ) );
  a5 -= 2*(a1 + a2) - (a3 + a4)*3;
  BOOST_CHECK( chkVectorS1( a5, -1032, -1720 ) );
  a5 -= 2*(a1 + a2) - (a3 - a4)*3;
  BOOST_CHECK( chkVectorS1( a5,  -576,  -960 ) );
  a5 -= 2*(a1 - a2) + (a3 + a4)*3;
  BOOST_CHECK( chkVectorS1( a5, -1080, -1800 ) );
  a5 -= 2*(a1 - a2) + (a3 - a4)*3;
  BOOST_CHECK( chkVectorS1( a5, -1548, -2580 ) );
  a5 -= 2*(a1 - a2) - (a3 + a4)*3;
  BOOST_CHECK( chkVectorS1( a5, -1008, -1680 ) );
  a5 -= 2*(a1 - a2) - (a3 - a4)*3;
  BOOST_CHECK( chkVectorS1( a5,  -504,  -840 ) );
  BOOST_CHECK( chkVectorS1( a1,     3,     5 ) );
  BOOST_CHECK( chkVectorS1( a2,    12,    20 ) );
  BOOST_CHECK( chkVectorS1( a3,   168,   280 ) );
  BOOST_CHECK( chkVectorS1( a4,     6,    10 ) );

  a5 = 2*(a1 + a2)*3;
  BOOST_CHECK( chkVectorS1( a5, 90, 150 ) );
  a5 = 2*3*(a1 + a2);
  BOOST_CHECK( chkVectorS1( a5, 90, 150 ) );
  a5 = (a1 + a2)*2*3;
  BOOST_CHECK( chkVectorS1( a5, 90, 150 ) );
  BOOST_CHECK( chkVectorS1( a1,  3,   5 ) );
  BOOST_CHECK( chkVectorS1( a2, 12,  20 ) );

  a2 = +a1;
  BOOST_CHECK( chkVectorS1( a2,  3,   5 ) );
  a3 = -a2;
  BOOST_CHECK( chkVectorS1( a3, -3,  -5 ) );
  a4 = +(a1 + a2);
  BOOST_CHECK( chkVectorS1( a4,  6,  10 ) );
  a4 = +(a1 - a2);
  BOOST_CHECK( chkVectorS1( a4,  0,   0 ) );
  a4 = -(a1 + a2);
  BOOST_CHECK( chkVectorS1( a4, -6, -10 ) );
  a4 = -(a1 - a2);
  BOOST_CHECK( chkVectorS1( a4,  0,   0 ) );
  a4 = +(a1 + a2) + a3;
  BOOST_CHECK( chkVectorS1( a4,  3,   5 ) );
  a4 = -(a1 + a2) + a3;
  BOOST_CHECK( chkVectorS1( a4, -9, -15 ) );
  BOOST_CHECK( chkVectorS1( a1,  3,   5 ) );
  BOOST_CHECK( chkVectorS1( a2,  3,   5 ) );
  BOOST_CHECK( chkVectorS1( a3, -3,  -5 ) );

  a4 = +5*a1;
  BOOST_CHECK( chkVectorS1( a4,  15,  25 ) );
  a4 = -5*a1;
  BOOST_CHECK( chkVectorS1( a4, -15, -25 ) );
  a4 = +a1*5;
  BOOST_CHECK( chkVectorS1( a4,  15,  25 ) );
  a4 = -a1*5;
  BOOST_CHECK( chkVectorS1( a4, -15, -25 ) );
  a4 = +(5*a1);
  BOOST_CHECK( chkVectorS1( a4,  15,  25 ) );
  a4 = -(5*a1);
  BOOST_CHECK( chkVectorS1( a4, -15, -25 ) );
  a4 = +(a1*5);
  BOOST_CHECK( chkVectorS1( a4,  15,  25 ) );
  a4 = -(a1*5);
  BOOST_CHECK( chkVectorS1( a4, -15, -25 ) );
  BOOST_CHECK( chkVectorS1( a1,   3,   5 ) );
}


//----------------------------------------------------------------------------//
bool chkVectorS4( const VectorS4& z,
                     int v0, int v1, int v2, int v3,
                     int d0, int d1, int d2, int d3 )
{
  bool isEqual = true;
  if (z[0].value() != v0)  isEqual = false;
  if (z[1].value() != v1)  isEqual = false;
  if (z[2].value() != v2)  isEqual = false;
  if (z[3].value() != v3)  isEqual = false;
  if (z[0].deriv() != d0)  isEqual = false;
  if (z[1].deriv() != d1)  isEqual = false;
  if (z[2].deriv() != d2)  isEqual = false;
  if (z[3].deriv() != d3)  isEqual = false;
  if (!isEqual)
  {
    cout << "actual (" << z << ")  "
         << "expected (" << Int(v0,d0) << ","
                         << Int(v1,d1) << ","
                         << Int(v2,d2) << ","
                         << Int(v3,d3) << ")  "
         << "diff (" << z[0]-Int(v0,d0) << ","
                     << z[1]-Int(v1,d1) << ","
                     << z[2]-Int(v2,d2) << ","
                     << z[3]-Int(v3,d3) << ")" << endl;
  }
  return isEqual;
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(size4)
{
  BOOST_CHECK( VectorS4::M == 4 );
  BOOST_CHECK( VectorS4::N == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(array_ops4)
{
  Int s1(1,5);   // {value,deriv} = {1,5}
  Int s2(2,6);
  Int s3(3,7);
  Int s4(4,8);
  Int vdata[4] = {s1, s2, s3, s4};
  VectorS4 a1(vdata, 4);
  VectorS4 a2(a1);
  VectorS4 a3, a4, a5;

  // size
  BOOST_CHECK( a1.M == 4 );
  BOOST_CHECK( a2.M == 4 );
  BOOST_CHECK( a3.M == 4 );
  BOOST_CHECK( a4.M == 4 );
  BOOST_CHECK( a5.M == 4 );

  // ctors
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4, 5,6,7,8 ) );

  // assignment
  a3 = a1;
  a4 = 5;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a3, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a4, 5,5,5,5, 0,0,0,0 ) );

  a2 = a3 = 3;
  BOOST_CHECK( chkVectorS4( a2, 3,3,3,3, 0,0,0,0 ) );
  BOOST_CHECK( chkVectorS4( a3, 3,3,3,3, 0,0,0,0 ) );

  // unary
  a2 = +a1;
  a3 = -a1;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a3, -1,-2,-3,-4, -5,-6,-7,-8 ) );

  // binary accumulation
  a2 = a1;
  a3 = a1;
  a4 = a1;
  a2 += 5;
  a3 -= 5;
  a4 *= 5;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a2, 6,7,8,9, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a3, -4,-3,-2,-1, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a4, 5,10,15,20, 25,30,35,40 ) );

  a2 = 5;
  a3 = 5;
  a2 += a1;
  a3 -= a1;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a2, 6,7,8,9, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a3, 4,3,2,1, -5,-6,-7,-8 ) );

  // binary operators
//  a2 = a1 + 3;
//  a3 = a1 - 3;
  a4 = a1 * 3;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
//  BOOST_CHECK( chkVectorS4( a2, 4,5,6,7, 5,6,7,8 ) );
//  BOOST_CHECK( chkVectorS4( a3, -2,-1,0,1, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a4, 3,6,9,12, 15,18,21,24 ) );

//  a2 = 3 + a1;
//  a3 = 3 - a1;
  a4 = 3 * a1;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
//  BOOST_CHECK( chkVectorS4( a2, 4,5,6,7, 5,6,7,8 ) );
//  BOOST_CHECK( chkVectorS4( a3, 2,1,0,-1, -5,-6,-7,-8 ) );
  BOOST_CHECK( chkVectorS4( a4, 3,6,9,12, 15,18,21,24 ) );

  a2 = 3;
  a3 = a1 + a2;
  a4 = a1 - a2;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a2, 3,3,3,3, 0,0,0,0 ) );
  BOOST_CHECK( chkVectorS4( a3, 4,5,6,7, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a4, -2,-1,0,1, 5,6,7,8 ) );

  // arithmetic combinations

  a2 = a1;
  a3 = a1 + a2;
  a4 = a1 + a2 + a3;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a3, 2,4,6,8, 10,12,14,16 ) );
  BOOST_CHECK( chkVectorS4( a4, 4,8,12,16, 20,24,28,32 ) );

  a2 += a1;
  a3 += a1 + a2;
  a4 += a1 + a2 + a3;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a2, 2,4,6,8, 10,12,14,16 ) );
  BOOST_CHECK( chkVectorS4( a3, 5,10,15,20, 25,30,35,40 ) );
  BOOST_CHECK( chkVectorS4( a4, 12,24,36,48, 60,72,84,96 ) );

  a3 = a1 - a2;
  a4 = a1 - a2 - a3;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a2, 2,4,6,8, 10,12,14,16 ) );
  BOOST_CHECK( chkVectorS4( a3, -1,-2,-3,-4, -5,-6,-7,-8 ) );
  BOOST_CHECK( chkVectorS4( a4, 0,0,0,0, 0,0,0,0 ) );

  a2 -= a1;
  a3 -= a1 - a2;
  a4 -= a1 - a2 - a3;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a3, -1,-2,-3,-4, -5,-6,-7,-8 ) );
  BOOST_CHECK( chkVectorS4( a4, -1,-2,-3,-4, -5,-6,-7,-8 ) );

  a3 = a1 - a2;
  a4 = a1 + a2 - a3;
  a5 = a1 - a2 + a3;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a3, 0,0,0,0, 0,0,0,0 ) );
  BOOST_CHECK( chkVectorS4( a4, 2,4,6,8, 10,12,14,16 ) );
  BOOST_CHECK( chkVectorS4( a5, 0,0,0,0, 0,0,0,0 ) );

  a5 = (a1 + a2) + (a3 + a4);
  BOOST_CHECK( chkVectorS4( a5, 4,8,12,16, 20,24,28,32 ) );
  a5 = (a1 + a2) + (a3 - a4);
  BOOST_CHECK( chkVectorS4( a5, 0,0,0,0, 0,0,0,0 ) );
  a5 = (a1 + a2) - (a3 + a4);
  BOOST_CHECK( chkVectorS4( a5, 0,0,0,0, 0,0,0,0 ) );
  a5 = (a1 + a2) - (a3 - a4);
  BOOST_CHECK( chkVectorS4( a5, 4,8,12,16, 20,24,28,32 ) );
  a5 = (a1 - a2) + (a3 + a4);
  BOOST_CHECK( chkVectorS4( a5, 2,4,6,8, 10,12,14,16 ) );
  a5 = (a1 - a2) + (a3 - a4);
  BOOST_CHECK( chkVectorS4( a5, -2,-4,-6,-8, -10,-12,-14,-16 ) );
  a5 = (a1 - a2) - (a3 + a4);
  BOOST_CHECK( chkVectorS4( a5, -2,-4,-6,-8, -10,-12,-14,-16 ) );
  a5 = (a1 - a2) - (a3 - a4);
  BOOST_CHECK( chkVectorS4( a5, 2,4,6,8, 10,12,14,16 ) );
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a3, 0,0,0,0, 0,0,0,0 ) );
  BOOST_CHECK( chkVectorS4( a4, 2,4,6,8, 10,12,14,16 ) );

  a5 += (a1 + a2) + (a3 + a4);
  a5 += (a1 + a2) + (a3 - a4);
  a5 += (a1 + a2) - (a3 + a4);
  a5 += (a1 + a2) - (a3 - a4);
  a5 += (a1 - a2) + (a3 + a4);
  a5 += (a1 - a2) + (a3 - a4);
  a5 += (a1 - a2) - (a3 + a4);
  a5 += (a1 - a2) - (a3 - a4);
  BOOST_CHECK( chkVectorS4( a5, 10,20,30,40, 50,60,70,80 ) );
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a3, 0,0,0,0, 0,0,0,0 ) );
  BOOST_CHECK( chkVectorS4( a4, 2,4,6,8, 10,12,14,16 ) );

  a5 -= (a1 + a2) + (a3 + a4);
  a5 -= (a1 + a2) + (a3 - a4);
  a5 -= (a1 + a2) - (a3 + a4);
  a5 -= (a1 + a2) - (a3 - a4);
  a5 -= (a1 - a2) + (a3 + a4);
  a5 -= (a1 - a2) + (a3 - a4);
  a5 -= (a1 - a2) - (a3 + a4);
  a5 -= (a1 - a2) - (a3 - a4);
  BOOST_CHECK( chkVectorS4( a5, 2,4,6,8, 10,12,14,16 ) );
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a3, 0,0,0,0, 0,0,0,0 ) );
  BOOST_CHECK( chkVectorS4( a4, 2,4,6,8, 10,12,14,16 ) );

  a1(0) = 1;
  a1(1) = 2;
  a1(2) = 3;
  a1(3) = 4;

  a1(0).deriv() = 5;
  a1(1).deriv() = 6;
  a1(2).deriv() = 7;
  a1(3).deriv() = 8;

  a2 = 1*a1;
  a3 = a2*2;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a3, 2,4,6,8, 10,12,14,16 ) );

  a2 += 1*a1;
  a3 += a2*2;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a2, 2,4,6,8, 10,12,14,16 ) );
  BOOST_CHECK( chkVectorS4( a3, 6,12,18,24, 30,36,42,48 ) );

  a2 -= 1*a1;
  a3 -= a2*2;
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a3, 4,8,12,16, 20,24,28,32 ) );

  a5 = 1*(a1 + a2) + (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 14,28,42,56, 70,84,98,112 ) );
  a5 = 1*(a1 + a2) + (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 6,12,18,24, 30,36,42,48 ) );
  a5 = 1*(a1 + a2) - (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -10,-20,-30,-40, -50,-60,-70,-80 ) );
  a5 = 1*(a1 + a2) - (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -2,-4,-6,-8, -10,-12,-14,-16 ) );
  a5 = 1*(a1 - a2) + (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 12,24,36,48, 60,72,84,96 ) );
  a5 = 1*(a1 - a2) + (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 4,8,12,16, 20,24,28,32 ) );
  a5 = 1*(a1 - a2) - (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -12,-24,-36,-48, -60,-72,-84,-96 ) );
  a5 = 1*(a1 - a2) - (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -4,-8,-12,-16, -20,-24,-28,-32 ) );
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a3, 4,8,12,16, 20,24,28,32 ) );
  BOOST_CHECK( chkVectorS4( a4, 2,4,6,8, 10,12,14,16 ) );

  a5 += 1*(a1 + a2) + (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 10,20,30,40, 50,60,70,80 ) );
  a5 += 1*(a1 + a2) + (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 16,32,48,64, 80,96,112,128 ) );
  a5 += 1*(a1 + a2) - (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 6,12,18,24, 30,36,42,48 ) );
  a5 += 1*(a1 + a2) - (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 4,8,12,16, 20,24,28,32 ) );
  a5 += 1*(a1 - a2) + (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 16,32,48,64, 80,96,112,128 ) );
  a5 += 1*(a1 - a2) + (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 20,40,60,80, 100,120,140,160 ) );
  a5 += 1*(a1 - a2) - (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 8,16,24,32, 40,48,56,64 ) );
  a5 += 1*(a1 - a2) - (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, 4,8,12,16, 20,24,28,32 ) );
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a3, 4,8,12,16, 20,24,28,32 ) );
  BOOST_CHECK( chkVectorS4( a4, 2,4,6,8, 10,12,14,16 ) );

  a5 -= 1*(a1 + a2) + (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -10,-20,-30,-40, -50,-60,-70,-80 ) );
  a5 -= 1*(a1 + a2) + (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -16,-32,-48,-64, -80,-96,-112,-128 ) );
  a5 -= 1*(a1 + a2) - (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -6,-12,-18,-24, -30,-36,-42,-48 ) );
  a5 -= 1*(a1 + a2) - (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -4,-8,-12,-16, -20,-24,-28,-32 ) );
  a5 -= 1*(a1 - a2) + (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -16,-32,-48,-64, -80,-96,-112,-128 ) );
  a5 -= 1*(a1 - a2) + (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -20,-40,-60,-80, -100,-120,-140,-160 ) );
  a5 -= 1*(a1 - a2) - (a3 + a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -8,-16,-24,-32, -40,-48,-56,-64 ) );
  a5 -= 1*(a1 - a2) - (a3 - a4)*2;
  BOOST_CHECK( chkVectorS4( a5, -4,-8,-12,-16, -20,-24,-28,-32 ) );
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a3, 4,8,12,16 , 20,24,28,32) );
  BOOST_CHECK( chkVectorS4( a4, 2,4,6,8, 10,12,14,16 ) );

  a5 = 1*(a1 + a2)*2;
  BOOST_CHECK( chkVectorS4( a5, 4,8,12,16, 20,24,28,32 ) );
  a5 = 1*2*(a1 + a2);
  BOOST_CHECK( chkVectorS4( a5, 4,8,12,16, 20,24,28,32 ) );
  a5 = (a1 + a2)*1*2;
  BOOST_CHECK( chkVectorS4( a5, 4,8,12,16, 20,24,28,32 ) );
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4, 5,6,7,8 ) );

  a2 = +a1;
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4, 5,6,7,8 ) );
  a3 = -a2;
  BOOST_CHECK( chkVectorS4( a3, -1,-2,-3,-4, -5,-6,-7,-8 ) );
  a4 = +(a1 + a2);
  BOOST_CHECK( chkVectorS4( a4, 2,4,6,8, 10,12,14,16 ) );
  a4 = +(a1 - a2);
  BOOST_CHECK( chkVectorS4( a4, 0,0,0,0, 0,0,0,0 ) );
  a4 = -(a1 + a2);
  BOOST_CHECK( chkVectorS4( a4, -2,-4,-6,-8, -10,-12,-14,-16 ) );
  a4 = -(a1 - a2);
  BOOST_CHECK( chkVectorS4( a4, 0,0,0,0, 0,0,0,0 ) );
  a4 = +(a1 + a2) + a3;
  BOOST_CHECK( chkVectorS4( a4, 1,2,3,4, 5,6,7,8 ) );
  a4 = -(a1 + a2) + a3;
  BOOST_CHECK( chkVectorS4( a4, -3,-6,-9,-12, -15,-18,-21,-24 ) );
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a2, 1,2,3,4, 5,6,7,8 ) );
  BOOST_CHECK( chkVectorS4( a3, -1,-2,-3,-4, -5,-6,-7,-8 ) );

  a4 = +1*a1;
  BOOST_CHECK( chkVectorS4( a4, 1,2,3,4, 5,6,7,8 ) );
  a4 = -1*a1;
  BOOST_CHECK( chkVectorS4( a4, -1,-2,-3,-4, -5,-6,-7,-8 ) );
  a4 = +a1*1;
  BOOST_CHECK( chkVectorS4( a4, 1,2,3,4, 5,6,7,8 ) );
  a4 = -a1*1;
  BOOST_CHECK( chkVectorS4( a4, -1,-2,-3,-4, -5,-6,-7,-8 ) );
  a4 = +(1*a1);
  BOOST_CHECK( chkVectorS4( a4, 1,2,3,4, 5,6,7,8 ) );
  a4 = -(1*a1);
  BOOST_CHECK( chkVectorS4( a4, -1,-2,-3,-4, -5,-6,-7,-8 ) );
  a4 = +(a1*1);
  BOOST_CHECK( chkVectorS4( a4, 1,2,3,4, 5,6,7,8 ) );
  a4 = -(a1*1);
  BOOST_CHECK( chkVectorS4( a4, -1,-2,-3,-4, -5,-6,-7,-8 ) );
  BOOST_CHECK( chkVectorS4( a1, 1,2,3,4, 5,6,7,8 ) );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
