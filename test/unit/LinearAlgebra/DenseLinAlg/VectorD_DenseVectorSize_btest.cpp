// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include <iostream>
using namespace SANS::DLA;


// Explicitly instantiate to create coverage information
namespace SANS
{
namespace DLA
{
template class DenseNonZeroPattern<Real>;
}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorD_VectorSize )
{
  typedef Real T;

  DenseVectorSize vs(3);

  VectorD<T> a(vs);

  BOOST_CHECK_EQUAL( 3, a.m() );

  VectorD< DenseVectorSize > vs2 = {2,
                                    3};

  BOOST_CHECK_EQUAL( 2, vs2[0].m() );
  BOOST_CHECK_EQUAL( 3, vs2[1].m() );

  //Copy constructor
  VectorD< DenseVectorSize > vs3(vs2);
  BOOST_CHECK_EQUAL( 2, vs3[0].m() );
  BOOST_CHECK_EQUAL( 3, vs3[1].m() );

  VectorD< DenseVectorSize > vs4(VectorD<DenseVectorSize>({3,4}));
  BOOST_CHECK_EQUAL( 3, vs4[0].m() );
  BOOST_CHECK_EQUAL( 4, vs4[1].m() );

  VectorD< VectorD<T> > b(vs2);

  BOOST_CHECK_EQUAL( 2, b[0].m() );
  BOOST_CHECK_EQUAL( 3, b[1].m() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorD_Copy )
{

  VectorD< DenseVectorSize > vs = {2,
                                   3};

  BOOST_CHECK_EQUAL( 2, vs[0].m() );
  BOOST_CHECK_EQUAL( 3, vs[1].m() );

  VectorD< DenseVectorSize > vs2 = {0,
                                    0,
                                    1};

  BOOST_CHECK_EQUAL( 0, vs2[0].m() );
  BOOST_CHECK_EQUAL( 0, vs2[1].m() );
  BOOST_CHECK_EQUAL( 1, vs2[2].m() );

  vs2[0] = vs[0];
  vs2[1] = vs[1];

  BOOST_CHECK_EQUAL( 2, vs2[0].m() );
  BOOST_CHECK_EQUAL( 3, vs2[1].m() );
  BOOST_CHECK_EQUAL( 1, vs2[2].m() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorD_CopyBlock )
{

  VectorD< DenseVectorSize > vs = {2,
                                   3};

  BOOST_CHECK_EQUAL( 2, vs[0].m() );
  BOOST_CHECK_EQUAL( 3, vs[1].m() );

  VectorD< DenseVectorSize > vs2 = {0,
                                    1};

  BOOST_CHECK_EQUAL( 0, vs2[0].m() );
  BOOST_CHECK_EQUAL( 1, vs2[1].m() );

  vs2 = vs;

  BOOST_CHECK_EQUAL( 2, vs2[0].m() );
  BOOST_CHECK_EQUAL( 3, vs2[1].m() );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
