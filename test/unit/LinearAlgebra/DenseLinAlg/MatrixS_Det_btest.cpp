// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include <iostream>
using namespace SANS::DLA;


//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Det_2x2_test )
{
  typedef Real T;

  T a = 2; T b = 3;
  T c = 6; T d = 9;

  Real Adata[] = {a, b,
                  c, d};

  MatrixS<2,2,T> A(Adata, 2*2);

  BOOST_CHECK_EQUAL( Det(A) , a*d - b*c );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_Det_2x2_test )
{
  typedef Real T;

  T a = 2;
  T c = 6; T d = 9;

  Real Adata[] = {a,
                  c, d};

  MatrixSymS<2,T> A(Adata, 3);

  BOOST_CHECK_EQUAL(A(0,0), a); BOOST_CHECK_EQUAL(A(0,1), c);
  BOOST_CHECK_EQUAL(A(1,0), c); BOOST_CHECK_EQUAL(A(1,1), d);

  BOOST_CHECK_EQUAL( Det(A) , a*d - c*c );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Det_3x3_test )
{
  typedef Real T;

  T a = 1; T b = 2; T c = 3;
  T d = 4; T e = 5; T f = 6;
  T g = 7; T h = 8; T i = 9;

  Real Adata[] = {a, b, c,
                  d, e, f,
                  g, h, i};

  MatrixS<3,3,T> A(Adata, 3*3);

  BOOST_CHECK_EQUAL( Det(A) , a*e*i + b*f*g + c*d*h - c*e*g - b*d*i - a*f*h );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_Det_3x3_test )
{
  typedef Real T;

  T a = 1; T b = 4; T c = 7;
  T d = 4; T e = 5; T f = 8;
  T g = 7; T h = 8; T i = 9;

  Real Adata[] = {a,
                  d, e,
                  g, h, i};

  MatrixSymS<3,T> A(Adata, 6);

  BOOST_CHECK_EQUAL(A(0,0), a); BOOST_CHECK_EQUAL(A(0,1), b); BOOST_CHECK_EQUAL(A(0,2), c);
  BOOST_CHECK_EQUAL(A(1,0), d); BOOST_CHECK_EQUAL(A(1,1), e); BOOST_CHECK_EQUAL(A(1,2), f);
  BOOST_CHECK_EQUAL(A(2,0), g); BOOST_CHECK_EQUAL(A(2,1), h); BOOST_CHECK_EQUAL(A(2,2), i);

  BOOST_CHECK_EQUAL( Det(A) , a*e*i + b*f*g + c*d*h - c*e*g - b*d*i - a*f*h );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Det_4x4_test )
{
  typedef Real T;

  T a = 1; T b = 4; T c = 7; T d = 3;
  T e = 4; T f = 5; T g = 8; T h = 2;
  T i = 7; T j = 8; T k = 9; T L = 1;
  T m = 3; T n = 2; T Q = 1; T p = 6;

  Real Adata[] = {a, b, c, d,
                  e, f, g, h,
                  i, j, k, L,
                  m, n, Q, p};

  MatrixS<4,4,T> A(Adata, 16);

  BOOST_CHECK_EQUAL( Det(A) , a*f*k*p - a*g*j*p - a*h*k*n - b*e*k*p + b*g*i*p +
                              b*h*k*m + c*e*j*p - c*f*i*p + c*h*i*n - c*h*j*m +
                              d*e*k*n - d*f*k*m - d*g*i*n + d*g*j*m - L*Q*a*f +
                              L*Q*b*e + L*a*g*n - L*b*g*m - L*c*e*n + L*c*f*m +
                              Q*a*h*j - Q*b*h*i - Q*d*e*j + Q*d*f*i );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_Det_4x4_test )
{
  typedef Real T;

  T a = 1; T b = 4; T c = 7; T d = 3;
  T e = 4; T f = 5; T g = 8; T h = 2;
  T i = 7; T j = 8; T k = 9; T L = 1;
  T m = 3; T n = 2; T Q = 1; T p = 6;

  Real Adata[] = {a,
                  e, f,
                  i, j, k,
                  m, n, Q, p};

  MatrixSymS<4,T> A(Adata, 10);

  BOOST_CHECK_EQUAL(A(0,0), a); BOOST_CHECK_EQUAL(A(0,1), b); BOOST_CHECK_EQUAL(A(0,2), c); BOOST_CHECK_EQUAL(A(0,3),d);
  BOOST_CHECK_EQUAL(A(1,0), e); BOOST_CHECK_EQUAL(A(1,1), f); BOOST_CHECK_EQUAL(A(1,2), g); BOOST_CHECK_EQUAL(A(1,3),h);
  BOOST_CHECK_EQUAL(A(2,0), i); BOOST_CHECK_EQUAL(A(2,1), j); BOOST_CHECK_EQUAL(A(2,2), k); BOOST_CHECK_EQUAL(A(2,3),L);
  BOOST_CHECK_EQUAL(A(3,0), m); BOOST_CHECK_EQUAL(A(3,1), n); BOOST_CHECK_EQUAL(A(3,2), Q); BOOST_CHECK_EQUAL(A(3,3),p);

  BOOST_CHECK_EQUAL( Det(A) , a*f*k*p - a*g*j*p - a*h*k*n - b*e*k*p + b*g*i*p +
                              b*h*k*m + c*e*j*p - c*f*i*p + c*h*i*n - c*h*j*m +
                              d*e*k*n - d*f*k*m - d*g*i*n + d*g*j*m - L*Q*a*f +
                              L*Q*b*e + L*a*g*n - L*b*g*m - L*c*e*n + L*c*f*m +
                              Q*a*h*j - Q*b*h*i - Q*d*e*j + Q*d*f*i );

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
