// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Det.h"

#include <iostream>
using namespace SANS::DLA;


//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_Det_Exceptions )
{
  typedef Real T;
  Real Adata[] = {1, 2};

  MatrixD<T> A(2,1, Adata);

  BOOST_CHECK_THROW( Det(A), DeveloperException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_Det_2x2_test )
{
  typedef Real T;

  T a = 2; T b = 3;
  T c = 6; T d = 9;

  Real Adata[] = {a, b,
                  c, d};

  MatrixD<T> A(2,2, Adata);

  BOOST_CHECK_EQUAL( Det(A) , a*d - b*c );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_Det_3x3_test )
{
  typedef Real T;

  T a = 1; T b = 2; T c = 3;
  T d = 4; T e = 5; T f = 6;
  T g = 7; T h = 8; T i = 9;

  Real Adata[] = {a, b, c,
                  d, e, f,
                  g, h, i};

  MatrixD<T> A(3, 3, Adata);

  BOOST_CHECK_EQUAL( Det(A) , a*e*i + b*f*g + c*d*h - c*e*g - b*d*i - a*f*h );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
