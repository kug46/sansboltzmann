// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// MatrixS_btest
// testing of MatrixS<M,N,T> class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "chkMatrixS_btest.h"

#include <iostream>
using namespace std;


//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
namespace DLA
{
template class MatrixS<1,1,int>;


typedef MatrixS<1,1,int> Mat;
typedef OpAddS< Mat, Mat, true, true > MatAddT;

template class OpMulS< Mat, Mat >;
template class OpMulS< MatAddT, Mat >;
template class OpMulS< Mat, MatAddT >;
template class OpMulS< MatAddT, MatAddT >;

template class OpAddS< Mat, Mat, true, true >;
template class OpSubS< Mat, Mat, true, true >;
template class OpMulSScalar< Mat, Real, true, true >;
template class OpMulSFactor< Mat, OpMulSScalar< Mat, Real, true, true >, true >;
template class OpMulSFactor< OpMulSScalar< Mat, Real, true, true >, Mat, true >;

typedef OpAddS< Mat, Mat, false, true > MatAddF;

template class OpAddS< Mat, Mat, false, true >;
template class OpSubS< Mat, Mat, false, true >;
template class OpMulSScalar< Mat, Real, false, true >;
template class OpMulSFactor< Mat, OpMulSScalar< Mat, Real, false, true >, true >;
template class OpMulSFactor< OpMulSScalar< Mat, Real, false, true >, Mat, true >;

}
}

using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( MatrixS_test_suite )

typedef int Int;

typedef VectorS<1,Int> VectorS1;
typedef MatrixS<1,1,Int> MatrixS1;

typedef VectorS<2,Int> VectorS2;
typedef MatrixS<2,2,Int> MatrixS2;


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( size1 )
{
  BOOST_CHECK( MatrixS1::M == 1 );
  BOOST_CHECK( MatrixS1::N == 1 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( matrix_ops1 )
{
  const Int data = 3;
  MatrixS1 m1(&data, 1);
  MatrixS1 m2(m1);
  MatrixS1 m3(1), m4{2}, m5;

  // size
  BOOST_CHECK( m1.M == 1 );
  BOOST_CHECK( m1.N == 1 );
  BOOST_CHECK( m2.M == 1 );
  BOOST_CHECK( m2.N == 1 );

  // ctors
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  1, m3(0,0) );
  BOOST_CHECK_EQUAL(  2, m4(0,0) );

  // assignment
  m3 = m1;
  m4 = data;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m3(0,0) );
  BOOST_CHECK_EQUAL(  3, m4(0,0) );

  m1 = m2 = m3 = data;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  3, m3(0,0) );

  m4 = data;
  m1 = m2 = m3 = m4;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  3, m3(0,0) );
  BOOST_CHECK_EQUAL(  3, m4(0,0) );

  // unary
  m2 = +m1;
  m3 = -m1;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL( -3, m3(0,0) );

  // binary accumulation
  m2 = m1;
  m3 = m1;
  m4 = m1;
  m1 += data;
  m2 -= data;
  m3 *= data;
  m4 /= data;
  BOOST_CHECK_EQUAL(  6, m1(0,0) );
  BOOST_CHECK_EQUAL(  0, m2(0,0) );
  BOOST_CHECK_EQUAL(  9, m3(0,0) );
  BOOST_CHECK_EQUAL(  1, m4(0,0) );

  m1 = data;
  m2 = m1;
  m3 = m1;
  m2 += m1;
  m3 -= m1;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  6, m2(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0) );

  // binary operators
  m1 = data;
//  m2 = m1 + data;
//  m3 = m1 - data;
  m4 = m1 * data;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
//  BOOST_CHECK_EQUAL(  6, m2(0,0) );
//  BOOST_CHECK_EQUAL(  0, m3(0,0) );
  BOOST_CHECK_EQUAL(  9, m4(0,0) );

//  m2 = data + m1;
//  m3 = data - m1;
  m4 = data * m1;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
//  BOOST_CHECK_EQUAL(  6, m2(0,0) );
//  BOOST_CHECK_EQUAL(  0, m3(0,0) );
  BOOST_CHECK_EQUAL(  9, m4(0,0) );

  m1 = m2 = data;
  m3 = m1 + m2;
  m4 = m1 - m2;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  6, m3(0,0) );
  BOOST_CHECK_EQUAL(  0, m4(0,0) );

  // arithmetic combinations

  m1 = m2 = data;
  m3 = m1 + m2;
  m4 = m1 + m2 + m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  6, m3(0,0) );
  BOOST_CHECK_EQUAL( 12, m4(0,0) );

  m2 += m1;
  m3 += m1 + m2;
  m4 += m1 + m2 + m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  6, m2(0,0) );
  BOOST_CHECK_EQUAL( 15, m3(0,0) );
  BOOST_CHECK_EQUAL( 36, m4(0,0) );

  m3 = m1 - m2;
  m4 = m1 - m2 - m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  6, m2(0,0) );
  BOOST_CHECK_EQUAL( -3, m3(0,0) );
  BOOST_CHECK_EQUAL(  0, m4(0,0) );

  m2 -= m1;
  m3 -= m1 - m2;
  m4 -= m1 - m2 - m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL( -3, m3(0,0) );
  BOOST_CHECK_EQUAL( -3, m4(0,0) );

  m3 = m1 - m2;
  m4 = m1 + m2 - m3;
  m5 = m1 - m2 + m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0) );
  BOOST_CHECK_EQUAL(  6, m4(0,0) );
  BOOST_CHECK_EQUAL(  0, m5(0,0) );

  m5 = (m1 + m2) + (m3 + m4);
  BOOST_CHECK_EQUAL( 12, m5(0,0) );
  m5 = (m1 + m2) + (m3 - m4);
  BOOST_CHECK_EQUAL(  0, m5(0,0) );
  m5 = (m1 + m2) - (m3 + m4);
  BOOST_CHECK_EQUAL(  0, m5(0,0) );
  m5 = (m1 + m2) - (m3 - m4);
  BOOST_CHECK_EQUAL( 12, m5(0,0) );
  m5 = (m1 - m2) + (m3 + m4);
  BOOST_CHECK_EQUAL(  6, m5(0,0) );
  m5 = (m1 - m2) + (m3 - m4);
  BOOST_CHECK_EQUAL( -6, m5(0,0) );
  m5 = (m1 - m2) - (m3 + m4);
  BOOST_CHECK_EQUAL( -6, m5(0,0) );
  m5 = (m1 - m2) - (m3 - m4);
  BOOST_CHECK_EQUAL(  6, m5(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0) );
  BOOST_CHECK_EQUAL(  6, m4(0,0) );

  m5 += (m1 + m2) + (m3 + m4);
  m5 += (m1 + m2) + (m3 - m4);
  m5 += (m1 + m2) - (m3 + m4);
  m5 += (m1 + m2) - (m3 - m4);
  m5 += (m1 - m2) + (m3 + m4);
  m5 += (m1 - m2) + (m3 - m4);
  m5 += (m1 - m2) - (m3 + m4);
  m5 += (m1 - m2) - (m3 - m4);
  BOOST_CHECK_EQUAL( 30, m5(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0) );
  BOOST_CHECK_EQUAL(  6, m4(0,0) );

  m5 -= (m1 + m2) + (m3 + m4);
  m5 -= (m1 + m2) + (m3 - m4);
  m5 -= (m1 + m2) - (m3 + m4);
  m5 -= (m1 + m2) - (m3 - m4);
  m5 -= (m1 - m2) + (m3 + m4);
  m5 -= (m1 - m2) + (m3 - m4);
  m5 -= (m1 - m2) - (m3 + m4);
  m5 -= (m1 - m2) - (m3 - m4);
  BOOST_CHECK_EQUAL(  6, m5(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0) );
  BOOST_CHECK_EQUAL(  6, m4(0,0) );

  m1 = data;

  m2 = 4*m1;
  m3 = m2*7;
  BOOST_CHECK_EQUAL(   3, m1(0,0) );
  BOOST_CHECK_EQUAL(  12, m2(0,0) );
  BOOST_CHECK_EQUAL(  84, m3(0,0) );

  m2 += 4*m1;
  m3 += m2*7;
  BOOST_CHECK_EQUAL(   3, m1(0,0) );
  BOOST_CHECK_EQUAL(  24, m2(0,0) );
  BOOST_CHECK_EQUAL( 252, m3(0,0) );

  m2 -= 4*m1;
  m3 -= m2*7;
  BOOST_CHECK_EQUAL(   3, m1(0,0) );
  BOOST_CHECK_EQUAL(  12, m2(0,0) );
  BOOST_CHECK_EQUAL( 168, m3(0,0) );

  m5 = 2*(m1 + m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL(  552, m5(0,0) );
  m5 = 2*(m1 + m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  516, m5(0,0) );
  m5 = 2*(m1 + m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -492, m5(0,0) );
  m5 = 2*(m1 + m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -456, m5(0,0) );
  m5 = 2*(m1 - m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL(  504, m5(0,0) );
  m5 = 2*(m1 - m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  468, m5(0,0) );
  m5 = 2*(m1 - m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -540, m5(0,0) );
  m5 = 2*(m1 - m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -504, m5(0,0) );
  BOOST_CHECK_EQUAL(    3, m1(0,0) );
  BOOST_CHECK_EQUAL(   12, m2(0,0) );
  BOOST_CHECK_EQUAL(  168, m3(0,0) );
  BOOST_CHECK_EQUAL(    6, m4(0,0) );

  m5 += 2*(m1 + m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL(   48, m5(0,0) );
  m5 += 2*(m1 + m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  564, m5(0,0) );
  m5 += 2*(m1 + m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL(   72, m5(0,0) );
  m5 += 2*(m1 + m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -384, m5(0,0) );
  m5 += 2*(m1 - m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL(  120, m5(0,0) );
  m5 += 2*(m1 - m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  588, m5(0,0) );
  m5 += 2*(m1 - m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL(   48, m5(0,0) );
  m5 += 2*(m1 - m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -456, m5(0,0) );
  BOOST_CHECK_EQUAL(    3, m1(0,0) );
  BOOST_CHECK_EQUAL(   12, m2(0,0) );
  BOOST_CHECK_EQUAL(  168, m3(0,0) );
  BOOST_CHECK_EQUAL(    6, m4(0,0) );

  m5 -= 2*(m1 + m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -1008, m5(0,0) );
  m5 -= 2*(m1 + m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -1524, m5(0,0) );
  m5 -= 2*(m1 + m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -1032, m5(0,0) );
  m5 -= 2*(m1 + m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  -576, m5(0,0) );
  m5 -= 2*(m1 - m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -1080, m5(0,0) );
  m5 -= 2*(m1 - m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -1548, m5(0,0) );
  m5 -= 2*(m1 - m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -1008, m5(0,0) );
  m5 -= 2*(m1 - m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  -504, m5(0,0) );
  BOOST_CHECK_EQUAL(     3, m1(0,0) );
  BOOST_CHECK_EQUAL(    12, m2(0,0) );
  BOOST_CHECK_EQUAL(   168, m3(0,0) );
  BOOST_CHECK_EQUAL(     6, m4(0,0) );

  m5 = 2*(m1 + m2)*3;
  BOOST_CHECK_EQUAL( 90, m5(0,0) );
  m5 = 2*3*(m1 + m2);
  BOOST_CHECK_EQUAL( 90, m5(0,0) );
  m5 = (m1 + m2)*2*3;
  BOOST_CHECK_EQUAL( 90, m5(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL( 12, m2(0,0) );

  m2 = +m1;
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  m3 = -m2;
  BOOST_CHECK_EQUAL( -3, m3(0,0) );
  m4 = +(m1 + m2);
  BOOST_CHECK_EQUAL(  6, m4(0,0) );
  m4 = +(m1 - m2);
  BOOST_CHECK_EQUAL(  0, m4(0,0) );
  m4 = -(m1 + m2);
  BOOST_CHECK_EQUAL( -6, m4(0,0) );
  m4 = -(m1 - m2);
  BOOST_CHECK_EQUAL(  0, m4(0,0) );
  m4 = +(m1 + m2) + m3;
  BOOST_CHECK_EQUAL(  3, m4(0,0) );
  m4 = -(m1 + m2) + m3;
  BOOST_CHECK_EQUAL( -9, m4(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL( -3, m3(0,0) );

  m4 = +5*m1;
  BOOST_CHECK_EQUAL(  15, m4(0,0) );
  m4 = -5*m1;
  BOOST_CHECK_EQUAL( -15, m4(0,0) );
  m4 = +m1*5;
  BOOST_CHECK_EQUAL(  15, m4(0,0) );
  m4 = -m1*5;
  BOOST_CHECK_EQUAL( -15, m4(0,0) );
  m4 = +(5*m1);
  BOOST_CHECK_EQUAL(  15, m4(0,0) );
  m4 = -(5*m1);
  BOOST_CHECK_EQUAL( -15, m4(0,0) );
  m4 = +(m1*5);
  BOOST_CHECK_EQUAL(  15, m4(0,0) );
  m4 = -(m1*5);
  BOOST_CHECK_EQUAL( -15, m4(0,0) );
  BOOST_CHECK_EQUAL(   3, m1(0,0) );

  m4 = {6};
  BOOST_CHECK_EQUAL( 6, m4(0,0) );


  Int i0 = m1*m1;
  BOOST_CHECK_EQUAL( 9, i0 );

  i0 += m1*m1;
  BOOST_CHECK_EQUAL( 2*9, i0 );

  i0 -= m1*m1;
  BOOST_CHECK_EQUAL( 9, i0 );


  Int i1 = (m1+m1)*m1;
  BOOST_CHECK_EQUAL( 18, i1 );

  i1 += (m1+m1)*m1;
  BOOST_CHECK_EQUAL( 2*18, i1 );

  i1 -= (m1+m1)*m1;
  BOOST_CHECK_EQUAL( 18, i1 );


  Int i2 = m1*(m1+m1);
  BOOST_CHECK_EQUAL( 18, i2 );

  i2 += m1*(m1+m1);
  BOOST_CHECK_EQUAL( 2*18, i2 );

  i2 -= m1*(m1+m1);
  BOOST_CHECK_EQUAL( 18, i2 );


  Int i3 = (m1+m1)*(m1+m1);
  BOOST_CHECK_EQUAL( 36, i3 );

  i3 += (m1+m1)*(m1+m1);
  BOOST_CHECK_EQUAL( 2*36, i3 );

  i3 -= (m1+m1)*(m1+m1);
  BOOST_CHECK_EQUAL( 36, i3 );

  Int i4 = 2*(m1*m1);
  BOOST_CHECK_EQUAL( 18, i4 );

  i4 += 2*(m1*m1);
  BOOST_CHECK_EQUAL( 2*18, i4 );

  i4 -= 2*(m1*m1);
  BOOST_CHECK_EQUAL( 18, i4 );


  i4 = (m1*m1)*2;
  BOOST_CHECK_EQUAL( 18, i4 );

  i4 += (m1*m1)*2;
  BOOST_CHECK_EQUAL( 2*18, i4 );

  i4 -= (m1*m1)*2;
  BOOST_CHECK_EQUAL( 18, i4 );


  i4 = (2*m1)*m1;
  BOOST_CHECK_EQUAL( 18, i4 );

  i4 += (2*m1)*m1;
  BOOST_CHECK_EQUAL( 2*18, i4 );

  i4 -= (2*m1)*m1;
  BOOST_CHECK_EQUAL( 18, i4 );


  i4 = m1*(2*m1);
  BOOST_CHECK_EQUAL( 18, i4 );

  i4 += m1*(2*m1);
  BOOST_CHECK_EQUAL( 2*18, i4 );

  i4 -= m1*(2*m1);
  BOOST_CHECK_EQUAL( 18, i4 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( size2 )
{
  BOOST_CHECK( VectorS2::M == 2 );
  BOOST_CHECK( VectorS2::N == 1 );
  BOOST_CHECK( MatrixS2::M == 2 );
  BOOST_CHECK( MatrixS2::N == 2 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( identity2 )
{
  MatrixS2 m1;
  m1 = Identity();

  BOOST_CHECK_EQUAL(  1, m1(0,0) );
  BOOST_CHECK_EQUAL(  0, m1(0,1) );
  BOOST_CHECK_EQUAL(  0, m1(1,0) );
  BOOST_CHECK_EQUAL(  1, m1(1,1) );

  MatrixS<2,3,int> m2;
  m2 = Identity();

  BOOST_CHECK_EQUAL( 1, m2(0,0) );
  BOOST_CHECK_EQUAL( 0, m2(0,1) );
  BOOST_CHECK_EQUAL( 0, m2(0,2) );
  BOOST_CHECK_EQUAL( 0, m2(1,0) );
  BOOST_CHECK_EQUAL( 1, m2(1,1) );
  BOOST_CHECK_EQUAL( 0, m2(1,2) );

  MatrixS2 m3 = Identity();

  BOOST_CHECK_EQUAL(  1, m3(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,1) );
  BOOST_CHECK_EQUAL(  0, m3(1,0) );
  BOOST_CHECK_EQUAL(  1, m3(1,1) );

  m3 += Identity();

  BOOST_CHECK_EQUAL(  2, m3(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,1) );
  BOOST_CHECK_EQUAL(  0, m3(1,0) );
  BOOST_CHECK_EQUAL(  2, m3(1,1) );

  m3 -= Identity();

  BOOST_CHECK_EQUAL(  1, m3(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,1) );
  BOOST_CHECK_EQUAL(  0, m3(1,0) );
  BOOST_CHECK_EQUAL(  1, m3(1,1) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( matrix_ops2 )
{
  MatrixS2 m1 = {{1,2},{3,4}};
  MatrixS2 m2(m1), m3, m4, m5;

  // size
  BOOST_CHECK( m1.M == 2 );
  BOOST_CHECK( m1.N == 2 );
  BOOST_CHECK( m2.M == 2 );
  BOOST_CHECK( m2.N == 2 );

  // ctors
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m2, 1,2,3,4 ) );

  // assignment
  m3 = m1;
  m4 = 5;
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m3, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m4, 5,5,5,5 ) );

  m2 = m3 = 3;
  BOOST_CHECK( chkMatrixS22( m2, 3,3,3,3 ) );
  BOOST_CHECK( chkMatrixS22( m3, 3,3,3,3 ) );

  // unary
  m2 = +m1;
  m3 = -m1;
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m3, -1,-2,-3,-4 ) );

  // binary accumulation
  m2 = m1;
  m3 = m1;
  m4 = m1;
  m2 += 5;
  m3 -= 5;
  m4 *= 5;
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m2, 6,7,8,9 ) );
  BOOST_CHECK( chkMatrixS22( m3, -4,-3,-2,-1 ) );
  BOOST_CHECK( chkMatrixS22( m4, 5,10,15,20 ) );

  m2 = 5;
  m3 = 5;
  m2 += m1;
  m3 -= m1;
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m2, 6,7,8,9 ) );
  BOOST_CHECK( chkMatrixS22( m3, 4,3,2,1 ) );

  // binary operators
//  m2 = m1 + 3;
//  m3 = m1 - 3;
  m4 = m1 * 3;
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
//  BOOST_CHECK( chkMatrixS22( m2, 4,5,6,7 ) );
//  BOOST_CHECK( chkMatrixS22( m3, -2,-1,0,1 ) );
  BOOST_CHECK( chkMatrixS22( m4, 3,6,9,12 ) );

//  m2 = 3 + m1;
//  m3 = 3 - m1;
  m4 = 3 * m1;
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
//  BOOST_CHECK( chkMatrixS22( m2, 4,5,6,7 ) );
//  BOOST_CHECK( chkMatrixS22( m3, 2,1,0,-1 ) );
  BOOST_CHECK( chkMatrixS22( m4, 3,6,9,12 ) );

  m2 = 3;
  m3 = m1 + m2;
  m4 = m1 - m2;
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m2, 3,3,3,3 ) );
  BOOST_CHECK( chkMatrixS22( m3, 4,5,6,7 ) );
  BOOST_CHECK( chkMatrixS22( m4, -2,-1,0,1 ) );

  // arithmetic combinations

  m2 = m1;
  m3 = m1 + m2;
  m4 = m1 + m2 + m3;
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m3, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixS22( m4, 4,8,12,16 ) );

  m2 += m1;
  m3 += m1 + m2;
  m4 += m1 + m2 + m3;
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m2, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixS22( m3, 5,10,15,20 ) );
  BOOST_CHECK( chkMatrixS22( m4, 12,24,36,48 ) );

  m3 = m1 - m2;
  m4 = m1 - m2 - m3;
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m2, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixS22( m3, -1,-2,-3,-4 ) );
  BOOST_CHECK( chkMatrixS22( m4, 0,0,0,0 ) );

  m2 -= m1;
  m3 -= m1 - m2;
  m4 -= m1 - m2 - m3;
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m3, -1,-2,-3,-4 ) );
  BOOST_CHECK( chkMatrixS22( m4, -1,-2,-3,-4 ) );

  m3 = m1 - m2;
  m4 = m1 + m2 - m3;
  m5 = m1 - m2 + m3;
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m3, 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixS22( m4, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixS22( m5, 0,0,0,0 ) );

  m5 = (m1 + m2) + (m3 + m4);
  BOOST_CHECK( chkMatrixS22( m5, 4,8,12,16 ) );
  m5 = (m1 + m2) + (m3 - m4);
  BOOST_CHECK( chkMatrixS22( m5, 0,0,0,0 ) );
  m5 = (m1 + m2) - (m3 + m4);
  BOOST_CHECK( chkMatrixS22( m5, 0,0,0,0 ) );
  m5 = (m1 + m2) - (m3 - m4);
  BOOST_CHECK( chkMatrixS22( m5, 4,8,12,16 ) );
  m5 = (m1 - m2) + (m3 + m4);
  BOOST_CHECK( chkMatrixS22( m5, 2,4,6,8 ) );
  m5 = (m1 - m2) + (m3 - m4);
  BOOST_CHECK( chkMatrixS22( m5, -2,-4,-6,-8 ) );
  m5 = (m1 - m2) - (m3 + m4);
  BOOST_CHECK( chkMatrixS22( m5, -2,-4,-6,-8 ) );
  m5 = (m1 - m2) - (m3 - m4);
  BOOST_CHECK( chkMatrixS22( m5, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m3, 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixS22( m4, 2,4,6,8 ) );

  m5 += (m1 + m2) + (m3 + m4);
  m5 += (m1 + m2) + (m3 - m4);
  m5 += (m1 + m2) - (m3 + m4);
  m5 += (m1 + m2) - (m3 - m4);
  m5 += (m1 - m2) + (m3 + m4);
  m5 += (m1 - m2) + (m3 - m4);
  m5 += (m1 - m2) - (m3 + m4);
  m5 += (m1 - m2) - (m3 - m4);
  BOOST_CHECK( chkMatrixS22( m5, 10,20,30,40 ) );
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m3, 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixS22( m4, 2,4,6,8 ) );

  m5 -= (m1 + m2) + (m3 + m4);
  m5 -= (m1 + m2) + (m3 - m4);
  m5 -= (m1 + m2) - (m3 + m4);
  m5 -= (m1 + m2) - (m3 - m4);
  m5 -= (m1 - m2) + (m3 + m4);
  m5 -= (m1 - m2) + (m3 - m4);
  m5 -= (m1 - m2) - (m3 + m4);
  m5 -= (m1 - m2) - (m3 - m4);
  BOOST_CHECK( chkMatrixS22( m5, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m3, 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixS22( m4, 2,4,6,8 ) );

  m2 = 1*m1;
  m3 = m2*2;
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m3, 2,4,6,8 ) );

  m2 += 1*m1;
  m3 += m2*2;
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m2, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixS22( m3, 6,12,18,24 ) );

  m2 -= 1*m1;
  m3 -= m2*2;
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m3, 4,8,12,16 ) );



  m3 = (2*m2)*2;
  BOOST_CHECK( chkMatrixS22( m3, 4,8,12,16 ) );

  m3 += (2*m2)*2;
  BOOST_CHECK( chkMatrixS22( m3, 2*4,2*8,2*12,2*16 ) );

  m3 = 2*(m2*2);
  BOOST_CHECK( chkMatrixS22( m3, 4,8,12,16 ) );

  m3 += 2*(m2*2);
  BOOST_CHECK( chkMatrixS22( m3, 2*4,2*8,2*12,2*16 ) );

  m3 = (4*m2)/2;
  BOOST_CHECK( chkMatrixS22( m3, 2,4,6,8 ) );

  m3 += (4*m2)/2;
  BOOST_CHECK( chkMatrixS22( m3, 2*2,2*4,2*6,2*8 ) );



  m5 = 1*(m1 + m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, 14,28,42,56 ) );
  m5 = 1*(m1 + m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, 6,12,18,24 ) );
  m5 = 1*(m1 + m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, -10,-20,-30,-40 ) );
  m5 = 1*(m1 + m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, -2,-4,-6,-8 ) );
  m5 = 1*(m1 - m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, 12,24,36,48 ) );
  m5 = 1*(m1 - m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, 4,8,12,16 ) );
  m5 = 1*(m1 - m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, -12,-24,-36,-48 ) );
  m5 = 1*(m1 - m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, -4,-8,-12,-16 ) );
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m3, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixS22( m4, 2,4,6,8 ) );

  m5 += 1*(m1 + m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, 10,20,30,40 ) );
  m5 += 1*(m1 + m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, 16,32,48,64 ) );
  m5 += 1*(m1 + m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, 6,12,18,24 ) );
  m5 += 1*(m1 + m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, 4,8,12,16 ) );
  m5 += 1*(m1 - m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, 16,32,48,64 ) );
  m5 += 1*(m1 - m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, 20,40,60,80 ) );
  m5 += 1*(m1 - m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, 8,16,24,32 ) );
  m5 += 1*(m1 - m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m3, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixS22( m4, 2,4,6,8 ) );

  m5 -= 1*(m1 + m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, -10,-20,-30,-40 ) );
  m5 -= 1*(m1 + m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, -16,-32,-48,-64 ) );
  m5 -= 1*(m1 + m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, -6,-12,-18,-24 ) );
  m5 -= 1*(m1 + m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, -4,-8,-12,-16 ) );
  m5 -= 1*(m1 - m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, -16,-32,-48,-64 ) );
  m5 -= 1*(m1 - m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, -20,-40,-60,-80 ) );
  m5 -= 1*(m1 - m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, -8,-16,-24,-32 ) );
  m5 -= 1*(m1 - m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS22( m5, -4,-8,-12,-16 ) );
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m3, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixS22( m4, 2,4,6,8 ) );

  m5 = 1*(m1 + m2)*2;
  BOOST_CHECK( chkMatrixS22( m5, 4,8,12,16 ) );
  m5 = 1*2*(m1 + m2);
  BOOST_CHECK( chkMatrixS22( m5, 4,8,12,16 ) );
  m5 = (m1 + m2)*1*2;
  BOOST_CHECK( chkMatrixS22( m5, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m2, 1,2,3,4 ) );

  //Need to divide by 1 because we are working with integers
  m5 = (m1 + m2)/1;
  BOOST_CHECK( chkMatrixS22( m5, 2,4,6,8 ) );

  m5 = +( 2*(m1 + m2) );
  BOOST_CHECK( chkMatrixS22( m5, 4,8,12,16 ) );

  m2 = +m1;
  BOOST_CHECK( chkMatrixS22( m2, 1,2,3,4 ) );
  m3 = -m2;
  BOOST_CHECK( chkMatrixS22( m3, -1,-2,-3,-4 ) );
  m4 = +(m1 + m2);
  BOOST_CHECK( chkMatrixS22( m4, 2,4,6,8 ) );
  m4 = +(m1 - m2);
  BOOST_CHECK( chkMatrixS22( m4, 0,0,0,0 ) );
  m4 = -(m1 + m2);
  BOOST_CHECK( chkMatrixS22( m4, -2,-4,-6,-8 ) );
  m4 = -(m1 - m2);
  BOOST_CHECK( chkMatrixS22( m4, 0,0,0,0 ) );
  m4 = +(m1 + m2) + m3;
  BOOST_CHECK( chkMatrixS22( m4, 1,2,3,4 ) );
  m4 = -(m1 + m2) + m3;
  BOOST_CHECK( chkMatrixS22( m4, -3,-6,-9,-12 ) );
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS22( m3, -1,-2,-3,-4 ) );

  m4 = +1*m1;
  BOOST_CHECK( chkMatrixS22( m4, 1,2,3,4 ) );
  m4 = -1*m1;
  BOOST_CHECK( chkMatrixS22( m4, -1,-2,-3,-4 ) );
  m4 = +m1*1;
  BOOST_CHECK( chkMatrixS22( m4, 1,2,3,4 ) );
  m4 = -m1*1;
  BOOST_CHECK( chkMatrixS22( m4, -1,-2,-3,-4 ) );
  m4 = +(1*m1);
  BOOST_CHECK( chkMatrixS22( m4, 1,2,3,4 ) );
  m4 = -(1*m1);
  BOOST_CHECK( chkMatrixS22( m4, -1,-2,-3,-4 ) );
  m4 = +(m1*1);
  BOOST_CHECK( chkMatrixS22( m4, 1,2,3,4 ) );
  m4 = -(m1*1);
  BOOST_CHECK( chkMatrixS22( m4, -1,-2,-3,-4 ) );
  BOOST_CHECK( chkMatrixS22( m1, 1,2,3,4 ) );
}


//----------------------------------------------------------------------------//
// matrix-vector multiply
BOOST_AUTO_TEST_CASE( matrix_vector_multiply2 )
{
  const Int adata[2] = {1,2};
  const Int mdata[4] = {3,4,5,6};
  MatrixS2 m1(mdata, 4);
  MatrixS2 m2(m1), m3;
  VectorS2 a1(adata, 2);
  VectorS2 a2(a1), a3;
  MatrixS<1,2,Int> row1(0);

  BOOST_CHECK( chkVectorS2( a1, 1,2 ) );
  BOOST_CHECK( chkVectorS2( a2, 1,2 ) );

  BOOST_CHECK( chkMatrixS22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS22( m2, 3,4,5,6 ) );

  a2 = m1*a1;
  BOOST_CHECK( chkVectorS2( a2, 11,17 ) );
  BOOST_CHECK( chkMatrixS22( m1, 3,4,5,6 ) );

  a2 = +(m1*a1);
  BOOST_CHECK( chkVectorS2( a2, 11,17 ) );

  a2 += m1*a1;
  BOOST_CHECK( chkVectorS2( a2, 2*11,2*17 ) );

  a2 -= m1*a1;
  BOOST_CHECK( chkVectorS2( a2, 11,17 ) );

  a2 += m1*a1 + m1*a1;
  BOOST_CHECK( chkVectorS2( a2, 3*11,3*17 ) );

  a2 += m1*a1 - m1*a1;
  BOOST_CHECK( chkVectorS2( a2, 3*11,3*17 ) );



  a2 = (2*m1)*a1;
  BOOST_CHECK( chkVectorS2( a2, 2*11,2*17 ) );

  a2 = +((2*m1)*a1);
  BOOST_CHECK( chkVectorS2( a2, 2*11,2*17 ) );

  a2 += (2*m1)*a1;
  BOOST_CHECK( chkVectorS2( a2, 4*11,4*17 ) );

  a2 = m1*(a1*2);
  BOOST_CHECK( chkVectorS2( a2, 2*11,2*17 ) );

  a2 = +(m1*(a1*2));
  BOOST_CHECK( chkVectorS2( a2, 2*11,2*17 ) );

  a2 += m1*(a1*2);
  BOOST_CHECK( chkVectorS2( a2, 4*11,4*17 ) );



  a2 = (m1 + m2)*a1;
  BOOST_CHECK( chkVectorS2( a2, 22,34 ) );
  BOOST_CHECK( chkMatrixS22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS22( m2, 3,4,5,6 ) );

  a2 = +((m1 + m2)*a1);
  BOOST_CHECK( chkVectorS2( a2, 22,34 ) );

  a2 += (m1 + m2)*a1;
  BOOST_CHECK( chkVectorS2( a2, 2*22,2*34 ) );

  a2 -= (m1 + m2)*a1;
  BOOST_CHECK( chkVectorS2( a2, 22,34 ) );

  a2 = (m1 - m2)*a1;
  BOOST_CHECK( chkVectorS2( a2, 0,0 ) );
  BOOST_CHECK( chkMatrixS22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS22( m2, 3,4,5,6 ) );


  a2 = a1;

  a3 = m1*(a1 + a2);
  BOOST_CHECK( chkVectorS2( a3, 2*11,2*17 ) );

  a3 = +(m1*(a1 + a2));
  BOOST_CHECK( chkVectorS2( a3, 2*11,2*17 ) );

  a3 += m1*(a1 + a2);
  BOOST_CHECK( chkVectorS2( a3, 4*11,4*17 ) );

  a3 -= m1*(a1 + a2);
  BOOST_CHECK( chkVectorS2( a3, 2*11,2*17 ) );


  a2 = a1;

  a3 = (m1 + m2)*(a1 + a2);
  BOOST_CHECK( chkVectorS2( a3, 44,68 ) );

  a3 += (m1 + m2)*(a1 + a2);
  BOOST_CHECK( chkVectorS2( a3, 2*44,2*68 ) );

  a3 -= (m1 + m2)*(a1 + a2);
  BOOST_CHECK( chkVectorS2( a3, 44,68 ) );

  a3 = +((m1 + m2)*(a1 + a2));
  BOOST_CHECK( chkVectorS2( a3, 44,68 ) );

  a3 = (m1 + m2)*(a1 - a2);
  BOOST_CHECK( chkVectorS2( a3, 0,0 ) );

  a3 = (m1 - m2)*(a1 + a2);
  BOOST_CHECK( chkVectorS2( a3, 0,0 ) );

  a3 = (m1 - m2)*(a1 - a2);
  BOOST_CHECK( chkVectorS2( a3, 0,0 ) );

  m3 = m1*m2;
  BOOST_CHECK( chkMatrixS22( m3, 29,36,45,56 ) );

  m3 = m1;
  m3 *= m2;
  BOOST_CHECK( chkMatrixS22( m3, 29,36,45,56 ) );


  //It is ok for what is on the left to be on the right as long as it is the first thing and not part of a multiplication
  a2 = a1;
  a2 = a2 + m1*a1;
  BOOST_CHECK( chkVectorS2( a2, 11+1,17+2 ) );

  //This is NOT ok because of lazy expressions the variable left of '=' cannot be part of a multiplication on the right of '='
  BOOST_CHECK_THROW( a2 = a1 + m1*a2;, AssertionException );
  BOOST_CHECK_THROW( a2 = m1*a1 + a2;, AssertionException );

  //Make sure that an exception is thrown when the same veriable is used on the left and right as part of a multiplication

  BOOST_CHECK_THROW( m2  = m2*m1, AssertionException );
  BOOST_CHECK_THROW( m2 += m2*m1, AssertionException );
  BOOST_CHECK_THROW( m2 -= m2*m1, AssertionException );

  BOOST_CHECK_THROW( m2  = m1*m2, AssertionException );
  BOOST_CHECK_THROW( m2 += m1*m2, AssertionException );
  BOOST_CHECK_THROW( m2 -= m1*m2, AssertionException );

  BOOST_CHECK_THROW( a1  = m1*a1, AssertionException );
  BOOST_CHECK_THROW( a1 += m1*a1, AssertionException );
  BOOST_CHECK_THROW( a1 -= m1*a1, AssertionException );

  BOOST_CHECK_THROW( row1  = row1*m1, AssertionException );
  BOOST_CHECK_THROW( row1 += row1*m1, AssertionException );
  BOOST_CHECK_THROW( row1 -= row1*m1, AssertionException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_sub_matrix )
{

  MatrixS<2,2,Real> M1 = {{1, 0}, {3, 2}};

  BOOST_CHECK_EQUAL( 1, M1(0,0) );
  BOOST_CHECK_EQUAL( 0, M1(0,1) );
  BOOST_CHECK_EQUAL( 3, M1(1,0) );
  BOOST_CHECK_EQUAL( 2, M1(1,1) );

  MatrixS<1,2,Real> row0 = M1.row(0);
  MatrixS<1,2,Real> row1 = M1.row(1);

  BOOST_CHECK_EQUAL( 1, row0(0,0) );
  BOOST_CHECK_EQUAL( 0, row0(0,1) );
  BOOST_CHECK_EQUAL( 3, row1(0,0) );
  BOOST_CHECK_EQUAL( 2, row1(0,1) );

  MatrixS<2,1,Real> col0 = M1.col(0);
  MatrixS<2,1,Real> col1 = M1.col(1);

  BOOST_CHECK_EQUAL( 1, col0(0,0) );
  BOOST_CHECK_EQUAL( 0, col1(0,0) );
  BOOST_CHECK_EQUAL( 3, col0(1,0) );
  BOOST_CHECK_EQUAL( 2, col1(1,0) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_row_col_swap )
{
  MatrixS<4,3,Real> M1 = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}, {9, 10, 11}};
  MatrixS<4,3,Real> Mr = {{0, 1, 2}, {9, 10, 11}, {6, 7, 8}, {3, 4, 5}};
  MatrixS<4,3,Real> Mc = {{2, 1, 0}, {11, 10, 9}, {8, 7, 6}, {5, 4, 3}};

  M1.swap_rows(1, 3);

  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 3; j++)
      BOOST_CHECK_EQUAL( Mr(i,j), M1(i,j) );

  M1.swap_cols(0, 2);

  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 3; j++)
      BOOST_CHECK_EQUAL( Mc(i,j), M1(i,j) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/DenseLinAlg/MatrixS_pattern.txt", true );

  MatrixS2 m = Identity();

  output << m << std::endl;
  BOOST_CHECK( output.match_pattern() );
  m.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );


}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
