// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include <iostream>
using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

typedef MatrixS<1,1,Real> MatrixS1;
typedef MatrixS<2,2,Real> MatrixS2;
typedef VectorS<1,Real> VectorS1;
typedef VectorS<2,Real> VectorS2;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_MatrixS2_2x2 )
{
  MatrixS<2,2,MatrixS2> M1;

  for (int i = 0; i < 2; ++i)
  {
    M1(0,i) = i+1;
    M1(1,i) = i+2;
  }

  M1.swap_rows(0,1);

  for (int i = 0; i < 2; ++i)
  {
    for (int j = 0; j < 2; ++j)
      for (int k = 0; k < 2; ++k)
      {
        BOOST_CHECK_EQUAL( M1(0,i)(j,k), i+2 );
        BOOST_CHECK_EQUAL( M1(1,i)(j,k), i+1 );
      }
  }
//
//
//  MatrixD<MatrixS1> M2(3,3);
//
//  for (int i = 0; i < 3; ++i)
//  {
//    M2(0,i) = i+1;
//    M2(1,i) = i+3;
//    M2(2,i) = i+6;
//  }
//
//  M2.swap_rows(0,2);
//
//  for (int i = 0; i < 3; ++i)
//  {
//    BOOST_CHECK_EQUAL( M2(0,i), i+6 );
//    BOOST_CHECK_EQUAL( M2(1,i), i+3 );
//    BOOST_CHECK_EQUAL( M2(2,i), i+1 );
//  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_MatrixS1_ctor )
{

  MatrixS<2,2,MatrixS1> M1;

  M1(0,0) = 1;
  M1(0,1) = 0;
  M1(1,0) = 3;
  M1(1,1) = 2;

  BOOST_CHECK_EQUAL( 1, M1(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 0, M1(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 3, M1(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 2, M1(1,1)(0,0) );

  MatrixS<2,2,MatrixS1> M2;
  M2 = 0;
  M2(0,0) = 1;
  M2(0,1) = 42;
  M2(1,0) = 1929;
  M2(1,1) = 2;

  BOOST_CHECK_EQUAL(    1, M2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(   42, M2(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 1929, M2(1,0)(0,0) );
  BOOST_CHECK_EQUAL(    2, M2(1,1)(0,0) );

  MatrixS<2,2,MatrixS1> M4 = {{MatrixS1(1), MatrixS1(42)}, {MatrixS1(1929), MatrixS1(2)}};

  BOOST_CHECK_EQUAL(    1, M4(0,0)(0,0) );
  BOOST_CHECK_EQUAL(   42, M4(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 1929, M4(1,0)(0,0) );
  BOOST_CHECK_EQUAL(    2, M4(1,1)(0,0) );

  MatrixS<2,2,MatrixS1> M5( M4 );

  BOOST_CHECK_EQUAL(    1, M5(0,0)(0,0) );
  BOOST_CHECK_EQUAL(   42, M5(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 1929, M5(1,0)(0,0) );
  BOOST_CHECK_EQUAL(    2, M5(1,1)(0,0) );

  MatrixS<2,2,MatrixS1> M6 = M5;

  BOOST_CHECK_EQUAL(    1, M6(0,0)(0,0) );
  BOOST_CHECK_EQUAL(   42, M6(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 1929, M6(1,0)(0,0) );
  BOOST_CHECK_EQUAL(    2, M6(1,1)(0,0) );

  MatrixS<2,2,MatrixS1> M7 = M4;

  BOOST_CHECK_EQUAL(    1, M7(0,0)(0,0) );
  BOOST_CHECK_EQUAL(   42, M7(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 1929, M7(1,0)(0,0) );
  BOOST_CHECK_EQUAL(    2, M7(1,1)(0,0) );

  M7 = Identity();

  BOOST_CHECK_EQUAL(  1, M7(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  0, M7(0,1)(0,0) );
  BOOST_CHECK_EQUAL(  0, M7(1,0)(0,0) );
  BOOST_CHECK_EQUAL(  1, M7(1,1)(0,0) );

  MatrixS<2,3,MatrixS1> M8;
  M8 = Identity();

  BOOST_CHECK_EQUAL( 1, M8(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 0, M8(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 0, M8(0,2)(0,0) );
  BOOST_CHECK_EQUAL( 0, M8(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 1, M8(1,1)(0,0) );
  BOOST_CHECK_EQUAL( 0, M8(1,2)(0,0) );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_MatrixS1_Assign )
{
  MatrixS<2,2,MatrixS1> M1;
  M1 = 0;
  M1(0,0) = 1;
  M1(1,0) = 3;
  M1(1,1) = 2;

  MatrixS<2,2,MatrixS1> M2;

  M2 = M1;

  BOOST_CHECK_EQUAL( 1, M2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 0, M2(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 3, M2(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 2, M2(1,1)(0,0) );

  M2 = -M1;

  BOOST_CHECK_EQUAL( -1, M2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  0, M2(0,1)(0,0) );
  BOOST_CHECK_EQUAL( -3, M2(1,0)(0,0) );
  BOOST_CHECK_EQUAL( -2, M2(1,1)(0,0) );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_MatrixS1_CompoundAssign )
{
  MatrixS<2,2,MatrixS1> M1, M2;
  M1 = 0;
  M1(0,0) = 1;
  M1(1,0) = 3;
  M1(1,1) = 2;

  M2 = 0;
  M2(0,0) = 3;
  M2(0,1) = 1;
  M2(1,0) = 2;
  M2(1,1) = 4;

  M2 += M1;

  BOOST_CHECK_EQUAL( 1, M1(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 0, M1(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 3, M1(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 2, M1(1,1)(0,0) );

  BOOST_CHECK_EQUAL( 4, M2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 1, M2(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 5, M2(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 6, M2(1,1)(0,0) );

  M1 -= M2;

  BOOST_CHECK_EQUAL( -3, M1(0,0)(0,0) );
  BOOST_CHECK_EQUAL( -1, M1(0,1)(0,0) );
  BOOST_CHECK_EQUAL( -2, M1(1,0)(0,0) );
  BOOST_CHECK_EQUAL( -4, M1(1,1)(0,0) );

  BOOST_CHECK_EQUAL( 4, M2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 1, M2(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 5, M2(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 6, M2(1,1)(0,0) );

  M2 *= 2;

  BOOST_CHECK_EQUAL(  8, M2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  2, M2(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 10, M2(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 12, M2(1,1)(0,0) );

  M2 /= 2;

  BOOST_CHECK_EQUAL( 4, M2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 1, M2(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 5, M2(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 6, M2(1,1)(0,0) );

  +M2; //This should do nothing

  BOOST_CHECK_EQUAL( 4, M2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 1, M2(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 5, M2(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 6, M2(1,1)(0,0) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_MatrixS1_Add )
{
  MatrixS<2,2,MatrixS1> M1, M2, M3;
  M1 = 0;
  M1(0,0) = 1;
  M1(1,0) = 3;
  M1(1,1) = 2;

  M2 = 0;
  M2(0,0) = 3;
  M2(0,1) = 1;
  M2(1,0) = 2;
  M2(1,1) = 4;

  M3 = M2 + M1;

  BOOST_CHECK_EQUAL( 1, M1(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 0, M1(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 3, M1(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 2, M1(1,1)(0,0) );

  BOOST_CHECK_EQUAL( 3, M2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 1, M2(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 2, M2(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 4, M2(1,1)(0,0) );

  BOOST_CHECK_EQUAL( 4, M3(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 1, M3(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 5, M3(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 6, M3(1,1)(0,0) );

  M3 += M2 + M1;

  BOOST_CHECK_EQUAL( 2*4, M3(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 2*1, M3(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 2*5, M3(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 2*6, M3(1,1)(0,0) );

  M3 = M1 - M2;

  BOOST_CHECK_EQUAL( -2, M3(0,0)(0,0) );
  BOOST_CHECK_EQUAL( -1, M3(0,1)(0,0) );
  BOOST_CHECK_EQUAL(  1, M3(1,0)(0,0) );
  BOOST_CHECK_EQUAL( -2, M3(1,1)(0,0) );

  M3 += M1 - M2;

  BOOST_CHECK_EQUAL( -2*2, M3(0,0)(0,0) );
  BOOST_CHECK_EQUAL( -1*2, M3(0,1)(0,0) );
  BOOST_CHECK_EQUAL(  1*2, M3(1,0)(0,0) );
  BOOST_CHECK_EQUAL( -2*2, M3(1,1)(0,0) );

  M3 -= M1 - M2;

  BOOST_CHECK_EQUAL( -2, M3(0,0)(0,0) );
  BOOST_CHECK_EQUAL( -1, M3(0,1)(0,0) );
  BOOST_CHECK_EQUAL(  1, M3(1,0)(0,0) );
  BOOST_CHECK_EQUAL( -2, M3(1,1)(0,0) );

  M1(0,0) = 1;
  M1(0,1) = 0;
  M1(1,0) = 3;
  M1(1,1) = 2;

  M2(0,0) = 3;
  M2(0,1) = 1;
  M2(1,0) = 2;
  M2(1,1) = 4;

  M3(0,0) = 4;
  M3(0,1) = 3;
  M3(1,0) = 2;
  M3(1,1) = 1;

  MatrixS<2,2,MatrixS1> M4;

  M4 = M3 + M2 + M1;

  BOOST_CHECK_EQUAL( 8, M4(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 4, M4(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 7, M4(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 7, M4(1,1)(0,0) );

  M4 = M3 - M2 + M1;

  BOOST_CHECK_EQUAL(  2, M4(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  2, M4(0,1)(0,0) );
  BOOST_CHECK_EQUAL(  3, M4(1,0)(0,0) );
  BOOST_CHECK_EQUAL( -1, M4(1,1)(0,0) );

  M4 = M3 + M2 - M1;

  BOOST_CHECK_EQUAL(  6, M4(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  4, M4(0,1)(0,0) );
  BOOST_CHECK_EQUAL(  1, M4(1,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, M4(1,1)(0,0) );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_MatrixS1_ops1 )
{
  const Real data = 3;
  MatrixS<1,1,MatrixS1> m1;
  MatrixS<1,1,MatrixS1> m2;
  MatrixS<1,1,MatrixS1> m3, m4, m5;

  m1 = data;
  m2 = data;

  // ctors
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );

  // assignment
  m3 = m1;
  m4 = data;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m4(0,0)(0,0) );

  m1 = m2 = m3 = data;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m3(0,0)(0,0) );

  m4 = data;
  m1 = m2 = m3 = m4;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m4(0,0)(0,0) );

  // unary
  m2 = +m1;
  m3 = -m1;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( -3, m3(0,0)(0,0) );

  // binary accumulation
  m3 = m1;
  m3 *= data;
  BOOST_CHECK_EQUAL(  9, m3(0,0)(0,0) );

  m3 = data;
  m3 *= m1;
  BOOST_CHECK_EQUAL(  9, m3(0,0)(0,0) );

  m1 = data;
  m2 = m1;
  m3 = m1;
  m2 += m1;
  m3 -= m1;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  6, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0)(0,0) );

  // binary operators
  m1 = data;
  //m2 = m1 + data;
  //m3 = m1 - data;
  m4 = m1 * data;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  //BOOST_CHECK_EQUAL(  6, m2(0,0) );
  //BOOST_CHECK_EQUAL(  0, m3(0,0) );
  BOOST_CHECK_EQUAL(  9, m4(0,0)(0,0) );

  //m2 = data + m1;
  //m3 = data - m1;
  m4 = data * m1;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  //BOOST_CHECK_EQUAL(  6, m2(0,0) );
  //BOOST_CHECK_EQUAL(  0, m3(0,0) );
  BOOST_CHECK_EQUAL(  9, m4(0,0)(0,0) );

  m1 = m2 = data;
  m3 = m1 + m2;
  m4 = m1 - m2;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  6, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  0, m4(0,0)(0,0) );

  // arithmetic combinations

  m1 = m2 = data;
  m3 = m1 + m2;
  m4 = m1 + m2 + m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  6, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 12, m4(0,0)(0,0) );

  m2 += m1;
  m3 += m1 + m2;
  m4 += m1 + m2 + m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  6, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 15, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 36, m4(0,0)(0,0) );

  m3 = m1 - m2;
  m4 = m1 - m2 - m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  6, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( -3, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  0, m4(0,0)(0,0) );

  m2 -= m1;
  m3 -= m1 - m2;
  m4 -= m1 - m2 - m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( -3, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL( -3, m4(0,0)(0,0) );

  m3 = m1 - m2;
  m4 = m1 + m2 - m3;
  m5 = m1 - m2 + m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  6, m4(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  0, m5(0,0)(0,0) );

  m5 = (m1 + m2) + (m3 + m4);
  BOOST_CHECK_EQUAL( 12, m5(0,0)(0,0) );
  m5 = (m1 + m2) + (m3 - m4);
  BOOST_CHECK_EQUAL(  0, m5(0,0)(0,0) );
  m5 = (m1 + m2) - (m3 + m4);
  BOOST_CHECK_EQUAL(  0, m5(0,0)(0,0) );
  m5 = (m1 + m2) - (m3 - m4);
  BOOST_CHECK_EQUAL( 12, m5(0,0)(0,0) );
  m5 = (m1 - m2) + (m3 + m4);
  BOOST_CHECK_EQUAL(  6, m5(0,0)(0,0) );
  m5 = (m1 - m2) + (m3 - m4);
  BOOST_CHECK_EQUAL( -6, m5(0,0)(0,0) );
  m5 = (m1 - m2) - (m3 + m4);
  BOOST_CHECK_EQUAL( -6, m5(0,0)(0,0) );
  m5 = (m1 - m2) - (m3 - m4);
  BOOST_CHECK_EQUAL(  6, m5(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  6, m4(0,0)(0,0) );

  m5 += (m1 + m2) + (m3 + m4);
  m5 += (m1 + m2) + (m3 - m4);
  m5 += (m1 + m2) - (m3 + m4);
  m5 += (m1 + m2) - (m3 - m4);
  m5 += (m1 - m2) + (m3 + m4);
  m5 += (m1 - m2) + (m3 - m4);
  m5 += (m1 - m2) - (m3 + m4);
  m5 += (m1 - m2) - (m3 - m4);
  BOOST_CHECK_EQUAL( 30, m5(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  6, m4(0,0)(0,0) );

  m5 -= (m1 + m2) + (m3 + m4);
  m5 -= (m1 + m2) + (m3 - m4);
  m5 -= (m1 + m2) - (m3 + m4);
  m5 -= (m1 + m2) - (m3 - m4);
  m5 -= (m1 - m2) + (m3 + m4);
  m5 -= (m1 - m2) + (m3 - m4);
  m5 -= (m1 - m2) - (m3 + m4);
  m5 -= (m1 - m2) - (m3 - m4);
  BOOST_CHECK_EQUAL(  6, m5(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  6, m4(0,0)(0,0) );

  m1 = data;

  m2 = 4*m1;
  m3 = m2*7;
  BOOST_CHECK_EQUAL(   3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  12, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  84, m3(0,0)(0,0) );

  m2 = m3/7;
  m1 = m2/4;
  BOOST_CHECK_EQUAL(   3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  12, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  84, m3(0,0)(0,0) );

  m2 += 4*m1;
  m3 += m2*7;
  BOOST_CHECK_EQUAL(   3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  24, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 252, m3(0,0)(0,0) );

  m2 -= 4*m1;
  m3 -= m2*7;
  BOOST_CHECK_EQUAL(   3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  12, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 168, m3(0,0)(0,0) );

  m5 = 2*(m1 + m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL(  552, m5(0,0)(0,0) );
  m5 = 2*(m1 + m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  516, m5(0,0)(0,0) );
  m5 = 2*(m1 + m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -492, m5(0,0)(0,0) );
  m5 = 2*(m1 + m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -456, m5(0,0)(0,0) );
  m5 = 2*(m1 - m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL(  504, m5(0,0)(0,0) );
  m5 = 2*(m1 - m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  468, m5(0,0)(0,0) );
  m5 = 2*(m1 - m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -540, m5(0,0)(0,0) );
  m5 = 2*(m1 - m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -504, m5(0,0)(0,0) );
  BOOST_CHECK_EQUAL(    3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(   12, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  168, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL(    6, m4(0,0)(0,0) );

  m5 += 2*(m1 + m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL(   48, m5(0,0)(0,0) );
  m5 += 2*(m1 + m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  564, m5(0,0)(0,0) );
  m5 += 2*(m1 + m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL(   72, m5(0,0)(0,0) );
  m5 += 2*(m1 + m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -384, m5(0,0)(0,0) );
  m5 += 2*(m1 - m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL(  120, m5(0,0)(0,0) );
  m5 += 2*(m1 - m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  588, m5(0,0)(0,0) );
  m5 += 2*(m1 - m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL(   48, m5(0,0)(0,0) );
  m5 += 2*(m1 - m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -456, m5(0,0)(0,0) );
  BOOST_CHECK_EQUAL(    3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(   12, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  168, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL(    6, m4(0,0)(0,0) );

  m5 -= 2*(m1 + m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -1008, m5(0,0)(0,0) );
  m5 -= 2*(m1 + m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -1524, m5(0,0)(0,0) );
  m5 -= 2*(m1 + m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -1032, m5(0,0)(0,0) );
  m5 -= 2*(m1 + m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  -576, m5(0,0)(0,0) );
  m5 -= 2*(m1 - m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -1080, m5(0,0)(0,0) );
  m5 -= 2*(m1 - m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -1548, m5(0,0)(0,0) );
  m5 -= 2*(m1 - m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -1008, m5(0,0)(0,0) );
  m5 -= 2*(m1 - m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  -504, m5(0,0)(0,0) );
  BOOST_CHECK_EQUAL(     3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(    12, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(   168, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL(     6, m4(0,0)(0,0) );

  m5 = 2*(m1 + m2)*3;
  BOOST_CHECK_EQUAL( 90, m5(0,0)(0,0) );
  m5 = 2*3*(m1 + m2);
  BOOST_CHECK_EQUAL( 90, m5(0,0)(0,0) );
  m5 = (m1 + m2)*2*3;
  BOOST_CHECK_EQUAL( 90, m5(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 12, m2(0,0)(0,0) );

  m2 = +m1;
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  m3 = -m2;
  BOOST_CHECK_EQUAL( -3, m3(0,0)(0,0) );
  m4 = +(m1 + m2);
  BOOST_CHECK_EQUAL(  6, m4(0,0)(0,0) );
  m4 = +(m1 - m2);
  BOOST_CHECK_EQUAL(  0, m4(0,0)(0,0) );
  m4 = -(m1 + m2);
  BOOST_CHECK_EQUAL( -6, m4(0,0)(0,0) );
  m4 = -(m1 - m2);
  BOOST_CHECK_EQUAL(  0, m4(0,0)(0,0) );
  m4 = +(m1 + m2) + m3;
  BOOST_CHECK_EQUAL(  3, m4(0,0)(0,0) );
  m4 = -(m1 + m2) + m3;
  BOOST_CHECK_EQUAL( -9, m4(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( -3, m3(0,0)(0,0) );

  m4 = +5*m1;
  BOOST_CHECK_EQUAL(  15, m4(0,0)(0,0) );
  m4 = -5*m1;
  BOOST_CHECK_EQUAL( -15, m4(0,0)(0,0) );
  m4 = +m1*5;
  BOOST_CHECK_EQUAL(  15, m4(0,0)(0,0) );
  m4 = -m1*5;
  BOOST_CHECK_EQUAL( -15, m4(0,0)(0,0) );
  m4 = +(5*m1);
  BOOST_CHECK_EQUAL(  15, m4(0,0)(0,0) );
  m4 = -(5*m1);
  BOOST_CHECK_EQUAL( -15, m4(0,0)(0,0) );
  m4 = +(m1*5);
  BOOST_CHECK_EQUAL(  15, m4(0,0)(0,0) );
  m4 = -(m1*5);
  BOOST_CHECK_EQUAL( -15, m4(0,0)(0,0) );
  BOOST_CHECK_EQUAL(   3, m1(0,0)(0,0) );

}


//----------------------------------------------------------------------------//
bool chkMatrixS_MatrixS1_22( const MatrixS<2,2,MatrixS1>& z, int m11, int m12, int m21, int m22 )
{
  bool isEqual = true;
  if (z(0,0)(0,0) != m11) isEqual = false;
  if (z(0,1)(0,0) != m12) isEqual = false;
  if (z(1,0)(0,0) != m21) isEqual = false;
  if (z(1,1)(0,0) != m22) isEqual = false;
  if (!isEqual)
  {
    std::cout << "actual ("
              << "(" << z(0,0)(0,0) << " " << z(0,1)(0,0) << ") "
              << "(" << z(1,0)(0,0) << " " << z(1,1)(0,0) << ") "
              << ")" << std::endl;
    std::cout << "  expected ("
              << "(" << m11 << " " << m12 << ") "
              << "(" << m21 << " " << m22 << ") "
              << ")" << std::endl;
  }
  return isEqual;
}

//----------------------------------------------------------------------------//
bool chkMatrixS_MatrixS1_21( const MatrixS<2,1,MatrixS1>& z, int m11, int m21 )
{
  bool isEqual = true;
  if (z(0,0)(0,0) != m11) isEqual = false;
  if (z(1,0)(0,0) != m21) isEqual = false;
  if (!isEqual)
  {
    std::cout << "actual ("
              << "        (" << z(0,0)(0,0) << ") " << std::endl
              << "        (" << z(1,0)(0,0) << ") "
              << ")" << std::endl;
    std::cout << "  expected ("
              << "            (" << m11 << ") " << std::endl
              << "            (" << m21 << ") "
              << ")" << std::endl;
  }
  return isEqual;
}

//----------------------------------------------------------------------------//
bool chkMatrixS_MatrixS1_12( const MatrixS<1,2,MatrixS1>& z, int m11, int m12 )
{
  bool isEqual = true;
  if (z(0,0)(0,0) != m11) isEqual = false;
  if (z(0,1)(0,0) != m12) isEqual = false;
  if (!isEqual)
  {
    std::cout << "actual ("
              << "       b(" << z(0,0)(0,0) << ") " << std::endl
              << "        (" << z(0,1)(0,0) << ") "
              << ")" << std::endl;
    std::cout << "  expected ("
              << "            (" << m11 << ") " << std::endl
              << "            (" << m12 << ") "
              << ")" << std::endl;
  }
  return isEqual;
}

//----------------------------------------------------------------------------//
bool chkMatrixS_MatrixS2_22( const MatrixS<2,2,MatrixS2>& z, int m11, int m12, int m21, int m22 )
{
  bool isEqual = true;
  for (int k = 0; k < 2; k++)
  {
    if (z(0,0)(k,k) != m11) isEqual = false;
    if (z(0,1)(k,k) != m12) isEqual = false;
    if (z(1,0)(k,k) != m21) isEqual = false;
    if (z(1,1)(k,k) != m22) isEqual = false;
  }

  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 2; j++)
      for (int k1 = 0; k1 < 2; k1++)
        for (int k2 = 0; k2 < 2; k2++)
          if ( (k1 != k2) && (z(i,j)(k1,k2) != 0) ) isEqual = false;

  if (!isEqual)
  {
    std::cout << "actual ("
              << "(" << z(0,0)(0,0) << " " << z(0,1)(0,0) << ") "
              << "(" << z(1,0)(0,0) << " " << z(1,1)(0,0) << ") "
              << ")" << std::endl;
    std::cout << "  expected ("
              << "(" << m11 << " " << m12 << ") "
              << "(" << m21 << " " << m22 << ") "
              << ")" << std::endl;
  }
  return isEqual;
}

//----------------------------------------------------------------------------//
bool chkMatrixS_MatrixS2_21( const MatrixS<2,1,MatrixS2>& z, int m11, int m21 )
{
  bool isEqual = true;
  for (int k = 0; k < 2; k++)
  {
    if (z(0,0)(k,k) != m11) isEqual = false;
    if (z(1,0)(k,k) != m21) isEqual = false;
  }

  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 1; j++)
      for (int k1 = 0; k1 < 2; k1++)
        for (int k2 = 0; k2 < 2; k2++)
          if ( (k1 != k2) && (z(i,j)(k1,k2) != 0) ) isEqual = false;

  if (!isEqual)
  {
    std::cout << "actual ("
              << "        (" << z(0,0)(0,0) << ") " << std::endl
              << "        (" << z(1,0)(0,0) << ") "
              << ")" << std::endl;
    std::cout << "  expected ("
              << "            (" << m11 << ") " << std::endl
              << "            (" << m21 << ") "
              << ")" << std::endl;
  }
  return isEqual;
}

//----------------------------------------------------------------------------//
bool chkMatrixS_MatrixS2_12( const MatrixS<1,2,MatrixS2>& z, int m11, int m12 )
{
  bool isEqual = true;
  for (int k = 0; k < 2; k++)
  {
    if (z(0,0)(k,k) != m11) isEqual = false;
    if (z(0,1)(k,k) != m12) isEqual = false;
  }

  for (int i = 0; i < 1; i++)
    for (int j = 0; j < 2; j++)
      for (int k1 = 0; k1 < 2; k1++)
        for (int k2 = 0; k2 < 2; k2++)
          if ( (k1 != k2) && (z(i,j)(k1,k2) != 0) ) isEqual = false;

  if (!isEqual)
  {
    std::cout << "actual ("
              << "       b(" << z(0,0)(0,0) << ") " << std::endl
              << "        (" << z(0,1)(0,0) << ") "
              << ")" << std::endl;
    std::cout << "  expected ("
              << "            (" << m11 << ") " << std::endl
              << "            (" << m12 << ") "
              << ")" << std::endl;
  }
  return isEqual;
}

//----------------------------------------------------------------------------//
bool chkMatrixS_VectorS2( const MatrixS<2,1,VectorS2>& z, int m11, int m21 )
{
  bool isEqual = true;
  for (int k = 0; k < 2; k++)
  {
    if (z(0,0)[k] != m11) isEqual = false;
    if (z(1,0)[k] != m21) isEqual = false;
  }

  if (!isEqual)
  {
    std::cout << "actual ("
              << "        (" << z(0,0)[0] << ") " << std::endl
              << "        (" << z(1,0)[0] << ") "
              << ")" << std::endl;
    std::cout << "  expected ("
              << "            (" << m11 << ") " << std::endl
              << "            (" << m21 << ") "
              << ")" << std::endl;
  }
  return isEqual;
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_MatrixS1_ops2 )
{

  MatrixS<2,2,MatrixS1> m1;
  MatrixS<2,2,MatrixS1> m2;
  MatrixS<2,2,MatrixS1> m3, m4, m5;

  m1(0,0) = 1; m1(0,1) = 2;
  m1(1,0) = 3; m1(1,1) = 4;

  m2 = m1;

  // ctors
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 1,2,3,4 ) );

  // assignment
  m3 = m1;
  m4 = 5;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, 5,5,5,5 ) );

  m2 = m3 = 3;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 3,3,3,3 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, 3,3,3,3 ) );

//  m3 = m2 = {1, 2,
//             3, 4};
//  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 1,2,3,4 ) );
//  BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, 1,2,3,4 ) );

  // unary
  m2 = +m1;
  m3 = -m1;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, -1,-2,-3,-4 ) );

  // binary accumulation
  m4 = m1;
  m4 *= 5;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, 5,10,15,20 ) );

  m2 = 5;
  m3 = 5;
  m2 += m1;
  m3 -= m1;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 6,7,8,9 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, 4,3,2,1 ) );

  // binary operators
  //m2 = m1 + 3;
  //m3 = m1 - 3;
  m4 = m1 * 3;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  //BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 4,5,6,7 ) );
  //BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, -2,-1,0,1 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, 3,6,9,12 ) );

  //m2 = 3 + m1;
  //m3 = 3 - m1;
  m4 = 3 * m1;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  //BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 4,5,6,7 ) );
  //BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, 2,1,0,-1 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, 3,6,9,12 ) );

  m2 = 3;
  m3 = m1 + m2;
  m4 = m1 - m2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 3,3,3,3 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, 4,5,6,7 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, -2,-1,0,1 ) );

  // arithmetic combinations

  m2 = m1;
  m3 = m1 + m2;
  m4 = m1 + m2 + m3;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, 4,8,12,16 ) );

  m2 += m1;
  m3 += m1 + m2;
  m4 += m1 + m2 + m3;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, 5,10,15,20 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, 12,24,36,48 ) );

  m3 = m1 - m2;
  m4 = m1 - m2 - m3;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, -1,-2,-3,-4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, 0,0,0,0 ) );

  m2 -= m1;
  m3 -= m1 - m2;
  m4 -= m1 - m2 - m3;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, -1,-2,-3,-4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, -1,-2,-3,-4 ) );

  m3 = m1 - m2;
  m4 = m1 + m2 - m3;
  m5 = m1 - m2 + m3;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 0,0,0,0 ) );

  m5 = (m1 + m2) + (m3 + m4);
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 4,8,12,16 ) );
  m5 = (m1 + m2) + (m3 - m4);
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 0,0,0,0 ) );
  m5 = (m1 + m2) - (m3 + m4);
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 0,0,0,0 ) );
  m5 = (m1 + m2) - (m3 - m4);
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 4,8,12,16 ) );
  m5 = (m1 - m2) + (m3 + m4);
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 2,4,6,8 ) );
  m5 = (m1 - m2) + (m3 - m4);
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, -2,-4,-6,-8 ) );
  m5 = (m1 - m2) - (m3 + m4);
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, -2,-4,-6,-8 ) );
  m5 = (m1 - m2) - (m3 - m4);
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, 2,4,6,8 ) );

  m5 += (m1 + m2) + (m3 + m4);
  m5 += (m1 + m2) + (m3 - m4);
  m5 += (m1 + m2) - (m3 + m4);
  m5 += (m1 + m2) - (m3 - m4);
  m5 += (m1 - m2) + (m3 + m4);
  m5 += (m1 - m2) + (m3 - m4);
  m5 += (m1 - m2) - (m3 + m4);
  m5 += (m1 - m2) - (m3 - m4);
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 10,20,30,40 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, 2,4,6,8 ) );

  m5 -= (m1 + m2) + (m3 + m4);
  m5 -= (m1 + m2) + (m3 - m4);
  m5 -= (m1 + m2) - (m3 + m4);
  m5 -= (m1 + m2) - (m3 - m4);
  m5 -= (m1 - m2) + (m3 + m4);
  m5 -= (m1 - m2) + (m3 - m4);
  m5 -= (m1 - m2) - (m3 + m4);
  m5 -= (m1 - m2) - (m3 - m4);
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, 2,4,6,8 ) );

  m2 = 1*m1;
  m3 = m2*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, 2,4,6,8 ) );

  m2 += 1*m1;
  m3 += m2*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, 6,12,18,24 ) );

  m2 -= 1*m1;
  m3 -= m2*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, 4,8,12,16 ) );

  m5 = 1*(m1 + m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 14,28,42,56 ) );
  m5 = 1*(m1 + m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 6,12,18,24 ) );
  m5 = 1*(m1 + m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, -10,-20,-30,-40 ) );
  m5 = 1*(m1 + m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, -2,-4,-6,-8 ) );
  m5 = 1*(m1 - m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 12,24,36,48 ) );
  m5 = 1*(m1 - m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 4,8,12,16 ) );
  m5 = 1*(m1 - m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, -12,-24,-36,-48 ) );
  m5 = 1*(m1 - m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, -4,-8,-12,-16 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, 2,4,6,8 ) );

  m5 += 1*(m1 + m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 10,20,30,40 ) );
  m5 += 1*(m1 + m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 16,32,48,64 ) );
  m5 += 1*(m1 + m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 6,12,18,24 ) );
  m5 += 1*(m1 + m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 4,8,12,16 ) );
  m5 += 1*(m1 - m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 16,32,48,64 ) );
  m5 += 1*(m1 - m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 20,40,60,80 ) );
  m5 += 1*(m1 - m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 8,16,24,32 ) );
  m5 += 1*(m1 - m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, 2,4,6,8 ) );

  m5 -= 1*(m1 + m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, -10,-20,-30,-40 ) );
  m5 -= 1*(m1 + m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, -16,-32,-48,-64 ) );
  m5 -= 1*(m1 + m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, -6,-12,-18,-24 ) );
  m5 -= 1*(m1 + m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, -4,-8,-12,-16 ) );
  m5 -= 1*(m1 - m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, -16,-32,-48,-64 ) );
  m5 -= 1*(m1 - m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, -20,-40,-60,-80 ) );
  m5 -= 1*(m1 - m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, -8,-16,-24,-32 ) );
  m5 -= 1*(m1 - m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, -4,-8,-12,-16 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, 2,4,6,8 ) );

  m5 = 1*(m1 + m2)*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 4,8,12,16 ) );
  m5 = 1*2*(m1 + m2);
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 4,8,12,16 ) );
  m5 = (m1 + m2)*1*2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m5, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 1,2,3,4 ) );

  m2 = +m1;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 1,2,3,4 ) );
  m3 = -m2;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, -1,-2,-3,-4 ) );
  m4 = +(m1 + m2);
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, 2,4,6,8 ) );
  m4 = +(m1 - m2);
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, 0,0,0,0 ) );
  m4 = -(m1 + m2);
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, -2,-4,-6,-8 ) );
  m4 = -(m1 - m2);
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, 0,0,0,0 ) );
  m4 = +(m1 + m2) + m3;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, 1,2,3,4 ) );
  m4 = -(m1 + m2) + m3;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, -3,-6,-9,-12 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m3, -1,-2,-3,-4 ) );

  m4 = +1*m1;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, 1,2,3,4 ) );
  m4 = -1*m1;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, -1,-2,-3,-4 ) );
  m4 = +m1*1;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, 1,2,3,4 ) );
  m4 = -m1*1;
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, -1,-2,-3,-4 ) );
  m4 = +(1*m1);
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, 1,2,3,4 ) );
  m4 = -(1*m1);
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, -1,-2,-3,-4 ) );
  m4 = +(m1*1);
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, 1,2,3,4 ) );
  m4 = -(m1*1);
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m4, -1,-2,-3,-4 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 1,2,3,4 ) );
}


//----------------------------------------------------------------------------//
// matrix-vector multiply
BOOST_AUTO_TEST_CASE( MatrixS_MatrixS1_Vector_Multiply2 )
{
  MatrixS<2,2,MatrixS1> m1;
  MatrixS<2,2,MatrixS1> m2;

  MatrixS<2,1,MatrixS1> col1;
  MatrixS<2,1,MatrixS1> col2;
  MatrixS<2,1,MatrixS1> col3;

  MatrixS<1,2,MatrixS1> row1;
  MatrixS<1,2,MatrixS1> row2;
  MatrixS<1,2,MatrixS1> row3;

  m1(0,0) = 3; m1(0,1) = 4;
  m1(1,0) = 5; m1(1,1) = 6;

  m2 = m1;

  col1(0,0) = 1;
  col1(1,0) = 2;

  col2 = col1;

  row1(0,0) = 1; row1(0,1) = 2;

  //Check column multiplication

  BOOST_CHECK( chkMatrixS_MatrixS1_21( col1, 1,2 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col2, 1,2 ) );

  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 3,4,5,6 ) );

  col2 = m1*col1;
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col2, 11,17 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 3,4,5,6 ) );

  col2 += m1*col1;
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col2, 22,34 ) );

  col2 -= m1*col1;
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col2, 11,17 ) );

  col2 = (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col2, 22,34 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 3,4,5,6 ) );

  col2 += (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col2, 2*22,2*34 ) );

  col2 -= (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col2, 22,34 ) );

  col2 = (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col2, 0,0 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 3,4,5,6 ) );


  m2(0,0) = 2; m2(0,1) = 3;
  m2(1,0) = 4; m2(1,1) = 5;

  col2(0,0) = 1;
  col2(1,0) = 2;

  col2 += (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col2, 4,5 ) );

  col2(0,0) = 7;
  col2(1,0) = 8;

  col2 -= (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col2, 4,5 ) );

  m2 = m1;
  col2 = col1;

  col3 = (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col3, 44,68 ) );

  col3 += (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col3, 2*44,2*68 ) );

  col3 -= (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col3, 44,68 ) );

  col3 = (m1 + m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col3, 0,0 ) );

  col3 = (m1 - m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col3, 0,0 ) );

  col3 = (m1 - m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col3, 0,0 ) );

  MatrixS<2,1,MatrixS1> col4 = (m1 - m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col4, 0,0 ) );

  //Check row multiplication
  row2 = row1*m1;
  BOOST_CHECK( chkMatrixS_MatrixS1_12( row2, 13,16 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 3,4,5,6 ) );

  row2 = row1*(m1 + m2);
  BOOST_CHECK( chkMatrixS_MatrixS1_12( row2, 26,32 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 3,4,5,6 ) );

  row2 += row1*(m1 + m2);
  BOOST_CHECK( chkMatrixS_MatrixS1_12( row2, 2*26,2*32 ) );

  row2 -= row1*(m1 + m2);
  BOOST_CHECK( chkMatrixS_MatrixS1_12( row2, 26,32 ) );

  row2 = row1*(m1 - m2);
  BOOST_CHECK( chkMatrixS_MatrixS1_12( row2, 0,0 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 3,4,5,6 ) );


  row2 = row1;

  row3 = (row1 + row2)*(m1 + m2);
  BOOST_CHECK( chkMatrixS_MatrixS1_12( row3, 52,64 ) );

  row3 = (row1 - row2)*(m1 + m2);
  BOOST_CHECK( chkMatrixS_MatrixS1_12( row3, 0,0 ) );

  row3 = (row1 + row2)*(m1 - m2);
  BOOST_CHECK( chkMatrixS_MatrixS1_12( row3, 0,0 ) );

  row3 = (row1 - row2)*(m1 - m2);
  BOOST_CHECK( chkMatrixS_MatrixS1_12( row3, 0,0 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_12( row1, 1,2 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_12( row2, 1,2 ) );

  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 3,4,5,6 ) );


  MatrixS<1,1,MatrixS1> val;

  val = row1*col1;
  BOOST_CHECK_EQUAL( val(0,0)(0,0), 5 );

  val = row1*m1*col1;
  BOOST_CHECK_EQUAL( val(0,0)(0,0), 45 );


  MatrixS1 val2;

  val2 = row1*col1;
  BOOST_CHECK_EQUAL( val2(0,0), 5 );

  val2 = row1*m1*col1;
  BOOST_CHECK_EQUAL( val2(0,0), 45 );
}

//----------------------------------------------------------------------------//
// matrix-vector multiply
BOOST_AUTO_TEST_CASE( MatrixS_VectorS1_Vector_Multiply2 )
{
  MatrixS<2,2,VectorS1> m1;
  MatrixS<2,2,VectorS1> m2;

  MatrixS<2,1,VectorS1> col1;
  MatrixS<2,1,VectorS1> col2;
  MatrixS<2,1,VectorS1> col3;

  MatrixS<1,2,VectorS1> row1;
  MatrixS<1,2,VectorS1> row2;
  MatrixS<1,2,VectorS1> row3;

  m1(0,0) = 3; m1(0,1) = 4;
  m1(1,0) = 5; m1(1,1) = 6;

  m2 = m1;

  col1(0,0) = 1;
  col1(1,0) = 2;

  col2 = col1;

  row1(0,0) = 1; row1(0,1) = 2;

  //Check column multiplication

  BOOST_CHECK( chkMatrixS_MatrixS1_21( col1, 1,2 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col2, 1,2 ) );

  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 3,4,5,6 ) );

  col2 = m1*col1;
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col2, 11,17 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 3,4,5,6 ) );

  col2 += m1*col1;
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col2, 22,34 ) );

  col2 -= m1*col1;
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col2, 11,17 ) );

  col2 = (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col2, 22,34 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 3,4,5,6 ) );

  col2 += (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col2, 2*22,2*34 ) );

  col2 -= (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col2, 22,34 ) );

  col2 = (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col2, 0,0 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 3,4,5,6 ) );


  m2(0,0) = 2; m2(0,1) = 3;
  m2(1,0) = 4; m2(1,1) = 5;

  col2(0,0) = 1;
  col2(1,0) = 2;

  col2 += (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col2, 4,5 ) );

  col2(0,0) = 7;
  col2(1,0) = 8;

  col2 -= (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col2, 4,5 ) );

  m2 = m1;
  col2 = col1;

  col3 = (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col3, 44,68 ) );

  col3 += (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col3, 2*44,2*68 ) );

  col3 -= (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col3, 44,68 ) );

  col3 = (m1 + m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col3, 0,0 ) );

  col3 = (m1 - m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col3, 0,0 ) );

  col3 = (m1 - m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col3, 0,0 ) );

  MatrixS<2,1,VectorS1> col4 = (m1 - m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixS_MatrixS1_21( col4, 0,0 ) );

  //Check row multiplication
  row2 = row1*m1;
  BOOST_CHECK( chkMatrixS_MatrixS1_12( row2, 13,16 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 3,4,5,6 ) );

  row2 = row1*(m1 + m2);
  BOOST_CHECK( chkMatrixS_MatrixS1_12( row2, 26,32 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 3,4,5,6 ) );

  row2 += row1*(m1 + m2);
  BOOST_CHECK( chkMatrixS_MatrixS1_12( row2, 2*26,2*32 ) );

  row2 -= row1*(m1 + m2);
  BOOST_CHECK( chkMatrixS_MatrixS1_12( row2, 26,32 ) );

  row2 = row1*(m1 - m2);
  BOOST_CHECK( chkMatrixS_MatrixS1_12( row2, 0,0 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 3,4,5,6 ) );


  row2 = row1;

  row3 = (row1 + row2)*(m1 + m2);
  BOOST_CHECK( chkMatrixS_MatrixS1_12( row3, 52,64 ) );

  row3 = (row1 - row2)*(m1 + m2);
  BOOST_CHECK( chkMatrixS_MatrixS1_12( row3, 0,0 ) );

  row3 = (row1 + row2)*(m1 - m2);
  BOOST_CHECK( chkMatrixS_MatrixS1_12( row3, 0,0 ) );

  row3 = (row1 - row2)*(m1 - m2);
  BOOST_CHECK( chkMatrixS_MatrixS1_12( row3, 0,0 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_12( row1, 1,2 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_12( row2, 1,2 ) );

  BOOST_CHECK( chkMatrixS_MatrixS1_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS_MatrixS1_22( m2, 3,4,5,6 ) );


  MatrixS<1,1,VectorS1> val;

  val = row1*col1;
  BOOST_CHECK_EQUAL( val(0,0)[0], 5 );

  val = row1*m1*col1;
  BOOST_CHECK_EQUAL( val(0,0)[0], 45 );


  VectorS1 val2;

  val2 = row1*col1;
  BOOST_CHECK_EQUAL( val2[0], 5 );

  val2 = row1*m1*col1;
  BOOST_CHECK_EQUAL( val2[0], 45 );
}


//----------------------------------------------------------------------------//
// matrix(of matrix)-vector(of matrix) multiply
BOOST_AUTO_TEST_CASE( MatrixS_MatrixS2_Vector_Multiply )
{
  MatrixS<2,2,MatrixS2> m1;
  MatrixS<2,2,MatrixS2> m2;

  MatrixS<2,1,MatrixS2> col1;
  MatrixS<2,1,MatrixS2> col2;
  MatrixS<2,1,MatrixS2> col3;

  MatrixS<1,2,MatrixS2> row1;
  MatrixS<1,2,MatrixS2> row2;
  MatrixS<1,2,MatrixS2> row3;

  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 2; j++)
    {
      m1(i,j) = 0;
      for (int k = 0; k < 2; k++)
        m1(i,j)(k,k) = 1;
    }

  m1(0,0) *= 3; m1(0,1) *= 4;
  m1(1,0) *= 5; m1(1,1) *= 6;

  m2 = m1;

  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 1; j++)
    {
      col1(i,j) = 0;
      for (int k = 0; k < 2; k++)
        col1(i,j)(k,k) = 1;
    }
  col1(0,0) *= 1;
  col1(1,0) *= 2;

  col2 = col1;

  for (int i = 0; i < 1; i++)
    for (int j = 0; j < 2; j++)
    {
      row1(i,j) = 0;
      for (int k = 0; k < 2; k++)
        row1(i,j)(k,k) = 1;
    }

  row1(0,0) *= 1; row1(0,1) *= 2;

  row2 = row1;

  //Check column multiplication

  BOOST_CHECK( chkMatrixS_MatrixS2_21( col1, 1,2 ) );
  BOOST_CHECK( chkMatrixS_MatrixS2_21( col2, 1,2 ) );

  BOOST_CHECK( chkMatrixS_MatrixS2_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS_MatrixS2_22( m2, 3,4,5,6 ) );

  col2 = m1*col1;
  BOOST_CHECK( chkMatrixS_MatrixS2_21( col2, 11,17 ) );
  BOOST_CHECK( chkMatrixS_MatrixS2_22( m1, 3,4,5,6 ) );

  col2 += m1*col1;
  BOOST_CHECK( chkMatrixS_MatrixS2_21( col2, 22,34 ) );

  col2 -= m1*col1;
  BOOST_CHECK( chkMatrixS_MatrixS2_21( col2, 11,17 ) );

  col2 = (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixS_MatrixS2_21( col2, 22,34 ) );
  BOOST_CHECK( chkMatrixS_MatrixS2_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS_MatrixS2_22( m2, 3,4,5,6 ) );

  col2 += (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixS_MatrixS2_21( col2, 2*22,2*34 ) );

  col2 -= (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixS_MatrixS2_21( col2, 22,34 ) );

  col2 = (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixS_MatrixS2_21( col2, 0,0 ) );
  BOOST_CHECK( chkMatrixS_MatrixS2_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS_MatrixS2_22( m2, 3,4,5,6 ) );


  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 2; j++)
    {
      m2(i,j) = 0;
      for (int k = 0; k < 2; k++)
        m2(i,j)(k,k) = 1;
    }

  m2(0,0) *= 2; m2(0,1) *= 3;
  m2(1,0) *= 4; m2(1,1) *= 5;

  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 1; j++)
    {
      col2(i,j) = 0;
      for (int k = 0; k < 2; k++)
        col2(i,j)(k,k) = 1;
    }

  col2(0,0) *= 1;
  col2(1,0) *= 2;

  col2 += (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixS_MatrixS2_21( col2, 4,5 ) );

  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 1; j++)
    {
      col2(i,j) = 0;
      for (int k = 0; k < 2; k++)
        col2(i,j)(k,k) = 1;
    }

  col2(0,0) *= 7;
  col2(1,0) *= 8;

  col2 -= (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixS_MatrixS2_21( col2, 4,5 ) );

  m2 = m1;
  col2 = col1;

  col3 = (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixS_MatrixS2_21( col3, 44,68 ) );

  col3 += (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixS_MatrixS2_21( col3, 2*44,2*68 ) );

  col3 -= (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixS_MatrixS2_21( col3, 44,68 ) );

  col3 = (m1 + m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixS_MatrixS2_21( col3, 0,0 ) );

  col3 = (m1 - m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixS_MatrixS2_21( col3, 0,0 ) );

  col3 = (m1 - m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixS_MatrixS2_21( col3, 0,0 ) );

  MatrixS<2,1,MatrixS2> col4 = (m1 - m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixS_MatrixS2_21( col4, 0,0 ) );

  //Check row multiplication
  row2 = row1*m1;
  BOOST_CHECK( chkMatrixS_MatrixS2_12( row2, 13,16 ) );
  BOOST_CHECK( chkMatrixS_MatrixS2_22( m1, 3,4,5,6 ) );

  row2 = row1*(m1 + m2);
  BOOST_CHECK( chkMatrixS_MatrixS2_12( row2, 26,32 ) );
  BOOST_CHECK( chkMatrixS_MatrixS2_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS_MatrixS2_22( m2, 3,4,5,6 ) );

  row2 += row1*(m1 + m2);
  BOOST_CHECK( chkMatrixS_MatrixS2_12( row2, 2*26,2*32 ) );

  row2 -= row1*(m1 + m2);
  BOOST_CHECK( chkMatrixS_MatrixS2_12( row2, 26,32 ) );

  row2 = row1*(m1 - m2);
  BOOST_CHECK( chkMatrixS_MatrixS2_12( row2, 0,0 ) );
  BOOST_CHECK( chkMatrixS_MatrixS2_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS_MatrixS2_22( m2, 3,4,5,6 ) );


  row2 = row1;

  row3 = (row1 + row2)*(m1 + m2);
  BOOST_CHECK( chkMatrixS_MatrixS2_12( row3, 52,64 ) );

  row3 = (row1 - row2)*(m1 + m2);
  BOOST_CHECK( chkMatrixS_MatrixS2_12( row3, 0,0 ) );

  row3 = (row1 + row2)*(m1 - m2);
  BOOST_CHECK( chkMatrixS_MatrixS2_12( row3, 0,0 ) );

  row3 = (row1 - row2)*(m1 - m2);
  BOOST_CHECK( chkMatrixS_MatrixS2_12( row3, 0,0 ) );
  BOOST_CHECK( chkMatrixS_MatrixS2_12( row1, 1,2 ) );
  BOOST_CHECK( chkMatrixS_MatrixS2_12( row2, 1,2 ) );

  BOOST_CHECK( chkMatrixS_MatrixS2_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS_MatrixS2_22( m2, 3,4,5,6 ) );


  MatrixS<1,1,MatrixS2> val;

  val = row1*col1;
  BOOST_CHECK_EQUAL( val(0,0)(0,0), 5 );
  BOOST_CHECK_EQUAL( val(0,0)(0,1), 0 );
  BOOST_CHECK_EQUAL( val(0,0)(1,0), 0 );
  BOOST_CHECK_EQUAL( val(0,0)(1,1), 5 );

  val = row1*m1*col1;
  BOOST_CHECK_EQUAL( val(0,0)(0,0), 45 );
  BOOST_CHECK_EQUAL( val(0,0)(0,1), 0 );
  BOOST_CHECK_EQUAL( val(0,0)(1,0), 0 );
  BOOST_CHECK_EQUAL( val(0,0)(1,1), 45 );

}

//----------------------------------------------------------------------------//
// matrix(of matrix)-vector(of vecor) multiply
BOOST_AUTO_TEST_CASE( MatrixS_MatrixS2_VectorS2_Multiply )
{
  MatrixS<2,2,MatrixS2> m1;
  MatrixS<2,2,MatrixS2> m2;

  MatrixS<2,1,VectorS2> col1;
  MatrixS<2,1,VectorS2> col2;
  MatrixS<2,1,VectorS2> col3;

  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 2; j++)
    {
      m1(i,j) = 0;
      for (int k = 0; k < 2; k++)
        m1(i,j)(k,k) = 1;
    }

  m1(0,0) *= 3; m1(0,1) *= 4;
  m1(1,0) *= 5; m1(1,1) *= 6;

  m2 = m1;

  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 1; j++)
    {
      col1(i,j) = 0;
      for (int k = 0; k < 2; k++)
        col1(i,j)[k] = 1;
    }

  col1(0,0) *= 1;
  col1(1,0) *= 2;

  col2 = col1;

  //Check column multiplication

  BOOST_CHECK( chkMatrixS_VectorS2( col1, 1,2 ) );
  BOOST_CHECK( chkMatrixS_VectorS2( col2, 1,2 ) );

  BOOST_CHECK( chkMatrixS_MatrixS2_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS_MatrixS2_22( m2, 3,4,5,6 ) );

  col2 = m1*col1;
  BOOST_CHECK( chkMatrixS_VectorS2( col2, 11,17 ) );
  BOOST_CHECK( chkMatrixS_MatrixS2_22( m1, 3,4,5,6 ) );

  col2 += m1*col1;
  BOOST_CHECK( chkMatrixS_VectorS2( col2, 22,34 ) );

  col2 -= m1*col1;
  BOOST_CHECK( chkMatrixS_VectorS2( col2, 11,17 ) );

  col2 = (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixS_VectorS2( col2, 22,34 ) );
  BOOST_CHECK( chkMatrixS_MatrixS2_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS_MatrixS2_22( m2, 3,4,5,6 ) );

  col2 += (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixS_VectorS2( col2, 2*22,2*34 ) );

  col2 -= (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixS_VectorS2( col2, 22,34 ) );

  col2 = (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixS_VectorS2( col2, 0,0 ) );
  BOOST_CHECK( chkMatrixS_MatrixS2_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS_MatrixS2_22( m2, 3,4,5,6 ) );


  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 2; j++)
    {
      m2(i,j) = 0;
      for (int k = 0; k < 2; k++)
        m2(i,j)(k,k) = 1;
    }

  m2(0,0) *= 2; m2(0,1) *= 3;
  m2(1,0) *= 4; m2(1,1) *= 5;

  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 1; j++)
    {
      col2(i,j) = 0;
      for (int k = 0; k < 2; k++)
        col2(i,j)[k] = 1;
    }

  col2(0,0) *= 1;
  col2(1,0) *= 2;

  col2 += (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixS_VectorS2( col2, 4,5 ) );

  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 1; j++)
    {
      col2(i,j) = 0;
      for (int k = 0; k < 2; k++)
        col2(i,j)[k] = 1;
    }

  col2(0,0) *= 7;
  col2(1,0) *= 8;

  col2 -= (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixS_VectorS2( col2, 4,5 ) );

  m2 = m1;
  col2 = col1;

  col3 = (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixS_VectorS2( col3, 44,68 ) );

  col3 += (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixS_VectorS2( col3, 2*44,2*68 ) );

  col3 -= (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixS_VectorS2( col3, 44,68 ) );

  col3 = (m1 + m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixS_VectorS2( col3, 0,0 ) );

  col3 = (m1 - m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixS_VectorS2( col3, 0,0 ) );

  col3 = (m1 - m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixS_VectorS2( col3, 0,0 ) );

  MatrixS<2,1,VectorS2> col4 = (m1 - m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixS_VectorS2( col4, 0,0 ) );


  MatrixS<1,2,MatrixS2> row5;
  MatrixS<2,1,MatrixS2> col5;
  VectorS<  2,MatrixS2> col6;

  MatrixS2 val;

  row5(0,0) = {{ 1,  2}, { 3,  4}}; row5(0,1) = {{ 5,  6}, { 7,  8}};

  col5(0,0) = {{10, 20}, {30, 40}};
  col5(1,0) = {{50, 60}, {70, 80}};

  val = row5*col5;
  BOOST_CHECK_EQUAL( val(0,0),  740 );
  BOOST_CHECK_EQUAL( val(0,1),  880 );
  BOOST_CHECK_EQUAL( val(1,0), 1060 );
  BOOST_CHECK_EQUAL( val(1,1), 1280 );

  val += row5*col5;
  BOOST_CHECK_EQUAL( val(0,0), 2*740 );
  BOOST_CHECK_EQUAL( val(0,1), 2*880 );
  BOOST_CHECK_EQUAL( val(1,0), 2*1060 );
  BOOST_CHECK_EQUAL( val(1,1), 2*1280 );

  val -= row5*col5;
  BOOST_CHECK_EQUAL( val(0,0),  740 );
  BOOST_CHECK_EQUAL( val(0,1),  880 );
  BOOST_CHECK_EQUAL( val(1,0), 1060 );
  BOOST_CHECK_EQUAL( val(1,1), 1280 );


  // Scalar multiplication of the matrix multiplication (without factorization)
  val = 3*(row5*col5);
  BOOST_CHECK_EQUAL( val(0,0),  3*740 );
  BOOST_CHECK_EQUAL( val(0,1),  3*880 );
  BOOST_CHECK_EQUAL( val(1,0), 3*1060 );
  BOOST_CHECK_EQUAL( val(1,1), 3*1280 );

  val += 3*(row5*col5);
  BOOST_CHECK_EQUAL( val(0,0), 2*3*740 );
  BOOST_CHECK_EQUAL( val(0,1), 2*3*880 );
  BOOST_CHECK_EQUAL( val(1,0), 2*3*1060 );
  BOOST_CHECK_EQUAL( val(1,1), 2*3*1280 );

  val -= 3*(row5*col5);
  BOOST_CHECK_EQUAL( val(0,0),  3*740 );
  BOOST_CHECK_EQUAL( val(0,1),  3*880 );
  BOOST_CHECK_EQUAL( val(1,0), 3*1060 );
  BOOST_CHECK_EQUAL( val(1,1), 3*1280 );


  // Scalar multiplication of the matrix multiplication (with factorization)
  val = 3*row5*col5;
  BOOST_CHECK_EQUAL( val(0,0),  3*740 );
  BOOST_CHECK_EQUAL( val(0,1),  3*880 );
  BOOST_CHECK_EQUAL( val(1,0), 3*1060 );
  BOOST_CHECK_EQUAL( val(1,1), 3*1280 );

  val += 3*row5*col5;
  BOOST_CHECK_EQUAL( val(0,0), 2*3*740 );
  BOOST_CHECK_EQUAL( val(0,1), 2*3*880 );
  BOOST_CHECK_EQUAL( val(1,0), 2*3*1060 );
  BOOST_CHECK_EQUAL( val(1,1), 2*3*1280 );

  val -= 3*row5*col5;
  BOOST_CHECK_EQUAL( val(0,0),  3*740 );
  BOOST_CHECK_EQUAL( val(0,1),  3*880 );
  BOOST_CHECK_EQUAL( val(1,0), 3*1060 );
  BOOST_CHECK_EQUAL( val(1,1), 3*1280 );


  // Multiplication with a vector
  val = 0;

  col6[0] = {{10, 20}, {30, 40}};
  col6[1] = {{50, 60}, {70, 80}};

  val = row5*col6;
  BOOST_CHECK_EQUAL( val(0,0),  740 );
  BOOST_CHECK_EQUAL( val(0,1),  880 );
  BOOST_CHECK_EQUAL( val(1,0), 1060 );
  BOOST_CHECK_EQUAL( val(1,1), 1280 );

  val += row5*col6;
  BOOST_CHECK_EQUAL( val(0,0), 2*740 );
  BOOST_CHECK_EQUAL( val(0,1), 2*880 );
  BOOST_CHECK_EQUAL( val(1,0), 2*1060 );
  BOOST_CHECK_EQUAL( val(1,1), 2*1280 );

  val -= row5*col6;
  BOOST_CHECK_EQUAL( val(0,0),  740 );
  BOOST_CHECK_EQUAL( val(0,1),  880 );
  BOOST_CHECK_EQUAL( val(1,0), 1060 );
  BOOST_CHECK_EQUAL( val(1,1), 1280 );


  // Triple multiplication
  val = row5*m1*col5;
  BOOST_CHECK_EQUAL( val(0,0), 6140 );
  BOOST_CHECK_EQUAL( val(0,1), 7560 );
  BOOST_CHECK_EQUAL( val(1,0), 9180 );
  BOOST_CHECK_EQUAL( val(1,1), 11320 );

  val += row5*m1*col5;
  BOOST_CHECK_EQUAL( val(0,0), 2*6140 );
  BOOST_CHECK_EQUAL( val(0,1), 2*7560 );
  BOOST_CHECK_EQUAL( val(1,0), 2*9180 );
  BOOST_CHECK_EQUAL( val(1,1), 2*11320 );

  val -= row5*m1*col5;
  BOOST_CHECK_EQUAL( val(0,0), 6140 );
  BOOST_CHECK_EQUAL( val(0,1), 7560 );
  BOOST_CHECK_EQUAL( val(1,0), 9180 );
  BOOST_CHECK_EQUAL( val(1,1), 11320 );
}

//----------------------------------------------------------------------------//
// matrix(of matrix)-vector(of Real) multiply
BOOST_AUTO_TEST_CASE( MatrixS_MatrixS2_VectorSReal_Multiply )
{
  MatrixS<1,3,MatrixS2> row;
  VectorS<  3,Real> col;

  row(0,0) = {{ 1,  2},
              { 3,  4}};
  row(0,1) = {{ 5,  6},
              { 7,  8}};
  row(0,2) = {{ 9, 10},
              {11, 12}};

  col = {10, 20, 30};

  MatrixS2 valTrue = {{ 1*10 + 5*20 +  9*30,  2*10 + 6*20 + 10*30},
                      { 3*10 + 7*20 + 11*30,  4*10 + 8*20 + 12*30}};

  MatrixS2 val = row*col;

  BOOST_CHECK_EQUAL( val(0,0), valTrue(0,0) );
  BOOST_CHECK_EQUAL( val(0,1), valTrue(0,1) );
  BOOST_CHECK_EQUAL( val(1,0), valTrue(1,0) );
  BOOST_CHECK_EQUAL( val(1,1), valTrue(1,1) );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
