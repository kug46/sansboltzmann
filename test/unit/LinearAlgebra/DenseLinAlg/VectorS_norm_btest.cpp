// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "LinearAlgebra/DenseLinAlg/tools/norm.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
namespace DLA
{
}
}

using namespace SANS;
using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( VectorS_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_norm_4 )
{
  typedef VectorS<4,Real> VectorS4;
  VectorS4 a = {1,2,3,4};

  unsigned int p = 2;

  Real L = norm(a,p);

  Real exact = 0;
  for (int i = 0; i < VectorS4::M; i++)
    exact += pow(a[i],p);

  BOOST_CHECK_EQUAL(L, pow(exact,1./p));
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_norm_4Surreal )
{
  typedef VectorS< 4,SurrealS<4> > VectorS4;
  VectorS4 a = {1,2,3,4};

  unsigned int p = 4;

  SurrealS<4> L = norm(a,p);

  SurrealS<4> exact = 0;
  for (int i = 0; i < VectorS4::M; i++)
    exact += pow(a[i],p);

  BOOST_CHECK_EQUAL(L.value(), pow(exact,1./p).value());
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
