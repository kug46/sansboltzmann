// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
namespace DLA
{
}
}

using namespace SANS;
using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( VectorD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorD_dot_Exception )
{
  VectorD<int> a({1,2,3,4});
  VectorD<int> b({2,3,4,5,6});

  BOOST_CHECK_THROW( dot(a,b), AssertionException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorD_dot_Int )
{
  VectorD<int> a = {1,2,3,4};
  VectorD<int> b = {2,3,4,5};

  int d = dot(a,b);

  int exact = 0;
  for (int i = 0; i < a.size(); i++)
    exact += a[i]*b[i];

  BOOST_CHECK_EQUAL(d, exact);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorD_dot_Surreal )
{
  VectorD< SurrealS<4> > a = {1,2,3,4};
  VectorD< SurrealS<4> > b = {2,3,4,5};

  SurrealS<4> d = dot(a,b);

  SurrealS<4> exact = 0;
  for (int i = 0; i < a.size(); i++)
    exact += a[i]*b[i];

  BOOST_CHECK_EQUAL(d.value(), exact.value());
}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorD_dot_SurrealReal )
{
  VectorD< SurrealS<4> > a = {1,2,3,4};
  VectorD< Real >        b = {2,3,4,5};

  SurrealS<4> d = dot(a,b);

  SurrealS<4> exact = 0;
  for (int i = 0; i < a.size(); i++)
    exact += a[i]*b[i];

  BOOST_CHECK_EQUAL(d.value(), exact.value());
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorD_dot_RealSurreal )
{
  VectorD< Real >        a = {1,2,3,4};
  VectorD< SurrealS<4> > b = {2,3,4,5};

  SurrealS<4> d = dot(a,b);

  SurrealS<4> exact = 0;
  for (int i = 0; i < a.size(); i++)
    exact += a[i]*b[i];

  BOOST_CHECK_EQUAL(d.value(), exact.value());
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorD_dot_VectorS )
{
  Real ae[] = {1,2,3,4};
  Real be[] = {2,3,4,5};

  VectorD< VectorS<2, Real> > a = {{1,2},{3,4}};
  VectorD< VectorS<2, Real> > b = {{2,3},{4,5}};

  Real d = dot(a,b);

  Real exact = 0;
  for (int i = 0; i < 4; i++)
    exact += ae[i]*be[i];

  BOOST_CHECK_EQUAL(d, exact);
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
