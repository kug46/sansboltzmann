// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Decompose_LU.h"
#include "LinearAlgebra/DenseLinAlg/tools/SingularException.h"

#include <iostream>
using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Decompose_LU_Exceptions )
{
  MatrixS<2,2,Real> A1, LU1;
  A1 = 0;
  BOOST_CHECK_THROW( Decompose_LU(A1, LU1), SingularMatrixException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Decompose_LU_1x1 )
{
  typedef Real T;
  MatrixS<1,1,T> A;
  MatrixS<1,1,T> LU;

  A = 2;

  Decompose_LU(A, LU);

  BOOST_CHECK_CLOSE( A(0,0), T(2.), T(0.0001) );

  BOOST_CHECK_CLOSE( LU(0,0), T(2.), T(0.0001) );

  //Set to zero so we know it gets filled properly
  LU = 0;

  // cppcheck-suppress redundantAssignment
  LU = Decompose_LU(A);

  BOOST_CHECK_CLOSE( A(0,0), T(2.), T(0.0001) );

  BOOST_CHECK_CLOSE( LU(0,0), T(2.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Decompose_LU_2x2 )
{
  typedef Real T;

  Real Avals[] = {2, 1,
                  3, 2};

  MatrixS<2,2,T> A(Avals, 4);
  MatrixS<2,2,T> LU;

  Decompose_LU(A, LU);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( LU(0,0),    2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1), 1./2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0),    3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), 1./2., T(0.0001) );


  LU = 0;

  // cppcheck-suppress redundantAssignment
  LU = Decompose_LU(A);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( LU(0,0),    2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1), 1./2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0),    3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), 1./2., T(0.0001) );

  // check scaling the matrix
  Real scale = 1e-20;
  A = scale*A;
  LU = 0;

  // cppcheck-suppress redundantAssignment
  LU = Decompose_LU(A);

  BOOST_CHECK_CLOSE( A(0,0), 2.*scale, T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1.*scale, T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3.*scale, T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2.*scale, T(0.0001) );

  BOOST_CHECK_CLOSE( LU(0,0),    2.*scale, T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1), 1./2.      , T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0),    3.*scale, T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), 1./2.*scale, T(0.0001) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Decompose_LU_2x2_AExpression )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  MatrixS<2,2,T> A1(Avals, 4), A2;
  MatrixS<2,2,T> LU;

  A2 = 1;

  Decompose_LU(A1 + A2, LU);

  BOOST_CHECK_CLOSE( LU(0,0),    2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1), 1./2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0),    3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), 1./2., T(0.0001) );


  LU = 0;

  // cppcheck-suppress redundantAssignment
  LU = Decompose_LU(A1 + A2);

  BOOST_CHECK_CLOSE( LU(0,0),    2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1), 1./2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0),    3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), 1./2., T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Decompose_LU_3x3 )
{
  typedef Real T;

  Real Adata[] = {4, 2, 2,
                  2, 4, 6,
                  2, 6, 6};

  MatrixS<3,3,T> A(Adata, 9);
  MatrixS<3,3,T> LU;

  Decompose_LU(A, LU);

  BOOST_CHECK_CLOSE( LU(0,0), T( 4.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1), T( 1./2.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,2), T( 1./2.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0), T( 2.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), T( 3.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,2), T( 5./3.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,0), T( 2.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,1), T( 5.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,2), T( -10./3. ), T(0.0001) );

  LU = 0;

  // cppcheck-suppress redundantAssignment
  LU = Decompose_LU(A);

  BOOST_CHECK_CLOSE( LU(0,0), T( 4.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1), T( 1./2.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,2), T( 1./2.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0), T( 2.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), T( 3.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,2), T( 5./3.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,0), T( 2.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,1), T( 5.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,2), T( -10./3. ), T(0.0001) );

  // check scaling the matrix

  Real scale = std::numeric_limits<Real>::min()*10;
  A = scale*A;
  LU = 0;

  // cppcheck-suppress redundantAssignment
  LU = Decompose_LU(A);

  BOOST_CHECK_CLOSE( LU(0,0), T( 4.      )*scale, T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1), T( 1./2.   )      , T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,2), T( 1./2.   )      , T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0), T( 2.      )*scale, T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), T( 3.      )*scale, T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,2), T( 5./3.   )      , T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,0), T( 2.      )*scale, T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,1), T( 5.      )*scale, T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,2), T( -10./3. )*scale, T(0.0001) );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
