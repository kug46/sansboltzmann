// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Diag.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"

#include <iostream>

namespace SANS
{
namespace DLA
{
  //Explicitly instantiate the class so that coverage information is correct
  template class MatrixSDiag<2,Real>;
}
}

using namespace SANS::DLA;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Diag_2x2_test )
{
  typedef Real T;

  VectorS<2,T> V = {1, 2};
  MatrixS<2,2,T> D;

  D = diag(V);

  BOOST_CHECK_EQUAL( D(0,0) , 1 );
  BOOST_CHECK_EQUAL( D(0,1) , 0 );
  BOOST_CHECK_EQUAL( D(1,0) , 0 );
  BOOST_CHECK_EQUAL( D(1,1) , 2 );

  D += diag(V);

  BOOST_CHECK_EQUAL( D(0,0) , 2*1 );
  BOOST_CHECK_EQUAL( D(0,1) ,   0 );
  BOOST_CHECK_EQUAL( D(1,0) ,   0 );
  BOOST_CHECK_EQUAL( D(1,1) , 2*2 );

  D -= diag(V);

  BOOST_CHECK_EQUAL( D(0,0) , 1 );
  BOOST_CHECK_EQUAL( D(0,1) , 0 );
  BOOST_CHECK_EQUAL( D(1,0) , 0 );
  BOOST_CHECK_EQUAL( D(1,1) , 2 );


  BOOST_CHECK_EQUAL( diag(V)(0,0) , 1 );
  BOOST_CHECK_EQUAL( diag(V)(0,1) , 0 );
  BOOST_CHECK_EQUAL( diag(V)(1,0) , 0 );
  BOOST_CHECK_EQUAL( diag(V)(1,1) , 2 );

  BOOST_CHECK_EQUAL( diag(V).diag(0) , 1 );
  BOOST_CHECK_EQUAL( diag(V).diag(1) , 2 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Diag_mul_2x2_test )
{
  typedef Real T;

  VectorS<2,T> V = {2, 3};
  MatrixS<2,2,T> M = {{3, 4},
                      {6, 7}};
  MatrixS<2,2,T> M2;

  M2 = diag(V)*M;

  BOOST_CHECK_EQUAL( M2(0,0) , 6 );
  BOOST_CHECK_EQUAL( M2(0,1) , 8 );
  BOOST_CHECK_EQUAL( M2(1,0) , 18 );
  BOOST_CHECK_EQUAL( M2(1,1) , 21 );

  M2 += diag(V)*M;

  BOOST_CHECK_EQUAL( M2(0,0) , 2*6 );
  BOOST_CHECK_EQUAL( M2(0,1) , 2*8 );
  BOOST_CHECK_EQUAL( M2(1,0) , 2*18 );
  BOOST_CHECK_EQUAL( M2(1,1) , 2*21 );

  M2 -= diag(V)*M;

  BOOST_CHECK_EQUAL( M2(0,0) , 6 );
  BOOST_CHECK_EQUAL( M2(0,1) , 8 );
  BOOST_CHECK_EQUAL( M2(1,0) , 18 );
  BOOST_CHECK_EQUAL( M2(1,1) , 21 );


  M2 = M*diag(V);

  BOOST_CHECK_EQUAL( M2(0,0) , 6 );
  BOOST_CHECK_EQUAL( M2(0,1) , 12 );
  BOOST_CHECK_EQUAL( M2(1,0) , 12 );
  BOOST_CHECK_EQUAL( M2(1,1) , 21 );

  M2 += M*diag(V);

  BOOST_CHECK_EQUAL( M2(0,0) , 2*6 );
  BOOST_CHECK_EQUAL( M2(0,1) , 2*12 );
  BOOST_CHECK_EQUAL( M2(1,0) , 2*12 );
  BOOST_CHECK_EQUAL( M2(1,1) , 2*21 );

  M2 -= M*diag(V);

  BOOST_CHECK_EQUAL( M2(0,0) , 6 );
  BOOST_CHECK_EQUAL( M2(0,1) , 12 );
  BOOST_CHECK_EQUAL( M2(1,0) , 12 );
  BOOST_CHECK_EQUAL( M2(1,1) , 21 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Diag_Transpose_mul_2x2_test )
{
  typedef Real T;

  VectorS<2,T> V = {2, 3};
  MatrixS<2,2,T> M = {{3, 4},
                      {6, 7}};
  MatrixS<2,2,T> M2;

  M2 = diag(V)*Transpose(M);

  BOOST_CHECK_EQUAL( M2(0,0) , 6 );
  BOOST_CHECK_EQUAL( M2(0,1) , 12 );
  BOOST_CHECK_EQUAL( M2(1,0) , 12 );
  BOOST_CHECK_EQUAL( M2(1,1) , 21 );

  M2 += diag(V)*Transpose(M);

  BOOST_CHECK_EQUAL( M2(0,0) , 2*6 );
  BOOST_CHECK_EQUAL( M2(0,1) , 2*12 );
  BOOST_CHECK_EQUAL( M2(1,0) , 2*12 );
  BOOST_CHECK_EQUAL( M2(1,1) , 2*21 );

  M2 -= diag(V)*Transpose(M);

  BOOST_CHECK_EQUAL( M2(0,0) , 6 );
  BOOST_CHECK_EQUAL( M2(0,1) , 12 );
  BOOST_CHECK_EQUAL( M2(1,0) , 12 );
  BOOST_CHECK_EQUAL( M2(1,1) , 21 );


  M2 = Transpose(M)*diag(V);

  BOOST_CHECK_EQUAL( M2(0,0) , 6 );
  BOOST_CHECK_EQUAL( M2(0,1) , 18 );
  BOOST_CHECK_EQUAL( M2(1,0) , 8 );
  BOOST_CHECK_EQUAL( M2(1,1) , 21 );

  M2 += Transpose(M)*diag(V);

  BOOST_CHECK_EQUAL( M2(0,0) , 2*6 );
  BOOST_CHECK_EQUAL( M2(0,1) , 2*18 );
  BOOST_CHECK_EQUAL( M2(1,0) , 2*8 );
  BOOST_CHECK_EQUAL( M2(1,1) , 2*21 );

  M2 -= Transpose(M)*diag(V);

  BOOST_CHECK_EQUAL( M2(0,0) , 6 );
  BOOST_CHECK_EQUAL( M2(0,1) , 18 );
  BOOST_CHECK_EQUAL( M2(1,0) , 8 );
  BOOST_CHECK_EQUAL( M2(1,1) , 21 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Diag_mul_2x3_test )
{
  typedef Real T;

  VectorS<2,T> V = {2, 3};
  MatrixS<2,3,T> A23 = {{3, 4, 2},
                        {6, 7, 5}};
  MatrixS<3,2,T> A32 = {{3, 4},
                        {6, 7},
                        {2, 5}};

  MatrixS<2,3,T> M23;
  MatrixS<3,2,T> M32;

  M23 = diag(V)*A23;

  BOOST_CHECK_EQUAL( M23(0,0) , 6 );
  BOOST_CHECK_EQUAL( M23(0,1) , 8 );
  BOOST_CHECK_EQUAL( M23(0,2) , 4 );
  BOOST_CHECK_EQUAL( M23(1,0) , 18 );
  BOOST_CHECK_EQUAL( M23(1,1) , 21 );
  BOOST_CHECK_EQUAL( M23(1,2) , 15 );

  M23 += diag(V)*A23;

  BOOST_CHECK_EQUAL( M23(0,0) , 2*6 );
  BOOST_CHECK_EQUAL( M23(0,1) , 2*8 );
  BOOST_CHECK_EQUAL( M23(0,2) , 2*4 );
  BOOST_CHECK_EQUAL( M23(1,0) , 2*18 );
  BOOST_CHECK_EQUAL( M23(1,1) , 2*21 );
  BOOST_CHECK_EQUAL( M23(1,2) , 2*15 );

  M23 -= diag(V)*A23;

  BOOST_CHECK_EQUAL( M23(0,0) , 6 );
  BOOST_CHECK_EQUAL( M23(0,1) , 8 );
  BOOST_CHECK_EQUAL( M23(0,2) , 4 );
  BOOST_CHECK_EQUAL( M23(1,0) , 18 );
  BOOST_CHECK_EQUAL( M23(1,1) , 21 );
  BOOST_CHECK_EQUAL( M23(1,2) , 15 );


  M32 = A32*diag(V);

  BOOST_CHECK_EQUAL( M32(0,0) , 6 );
  BOOST_CHECK_EQUAL( M32(0,1) , 12 );
  BOOST_CHECK_EQUAL( M32(1,0) , 12 );
  BOOST_CHECK_EQUAL( M32(1,1) , 21 );
  BOOST_CHECK_EQUAL( M32(2,0) , 4 );
  BOOST_CHECK_EQUAL( M32(2,1) , 15 );

  M32 += A32*diag(V);

  BOOST_CHECK_EQUAL( M32(0,0) , 2*6 );
  BOOST_CHECK_EQUAL( M32(0,1) , 2*12 );
  BOOST_CHECK_EQUAL( M32(1,0) , 2*12 );
  BOOST_CHECK_EQUAL( M32(1,1) , 2*21 );
  BOOST_CHECK_EQUAL( M32(2,0) , 2*4 );
  BOOST_CHECK_EQUAL( M32(2,1) , 2*15 );

  M32 -= A32*diag(V);

  BOOST_CHECK_EQUAL( M32(0,0) , 6 );
  BOOST_CHECK_EQUAL( M32(0,1) , 12 );
  BOOST_CHECK_EQUAL( M32(1,0) , 12 );
  BOOST_CHECK_EQUAL( M32(1,1) , 21 );
  BOOST_CHECK_EQUAL( M32(2,0) , 4 );
  BOOST_CHECK_EQUAL( M32(2,1) , 15 );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Diag_Transpose_mul_2x3_test )
{
  typedef Real T;

  VectorS<2,T> V = {2, 3};
  MatrixS<2,3,T> A23 = {{3, 4, 2},
                        {6, 7, 5}};
  MatrixS<3,2,T> A32 = {{3, 4},
                        {6, 7},
                        {2, 5}};

  MatrixS<2,3,T> M23;
  MatrixS<3,2,T> M32;

  M23 = diag(V)*Transpose(A32);

  BOOST_CHECK_EQUAL( M23(0,0) , 6 );
  BOOST_CHECK_EQUAL( M23(0,1) , 12 );
  BOOST_CHECK_EQUAL( M23(0,2) , 4 );
  BOOST_CHECK_EQUAL( M23(1,0) , 12 );
  BOOST_CHECK_EQUAL( M23(1,1) , 21 );
  BOOST_CHECK_EQUAL( M23(1,2) , 15 );

  M23 += diag(V)*Transpose(A32);

  BOOST_CHECK_EQUAL( M23(0,0) , 2*6 );
  BOOST_CHECK_EQUAL( M23(0,1) , 2*12 );
  BOOST_CHECK_EQUAL( M23(0,2) , 2*4 );
  BOOST_CHECK_EQUAL( M23(1,0) , 2*12 );
  BOOST_CHECK_EQUAL( M23(1,1) , 2*21 );
  BOOST_CHECK_EQUAL( M23(1,2) , 2*15 );

  M23 -= diag(V)*Transpose(A32);

  BOOST_CHECK_EQUAL( M23(0,0) , 6 );
  BOOST_CHECK_EQUAL( M23(0,1) , 12 );
  BOOST_CHECK_EQUAL( M23(0,2) , 4 );
  BOOST_CHECK_EQUAL( M23(1,0) , 12 );
  BOOST_CHECK_EQUAL( M23(1,1) , 21 );
  BOOST_CHECK_EQUAL( M23(1,2) , 15 );


  M32 = Transpose(A23)*diag(V);

  BOOST_CHECK_EQUAL( M32(0,0) , 6 );
  BOOST_CHECK_EQUAL( M32(0,1) , 18 );
  BOOST_CHECK_EQUAL( M32(1,0) , 8 );
  BOOST_CHECK_EQUAL( M32(1,1) , 21 );
  BOOST_CHECK_EQUAL( M32(2,0) , 4 );
  BOOST_CHECK_EQUAL( M32(2,1) , 15 );

  M32 += Transpose(A23)*diag(V);

  BOOST_CHECK_EQUAL( M32(0,0) , 2*6 );
  BOOST_CHECK_EQUAL( M32(0,1) , 2*18 );
  BOOST_CHECK_EQUAL( M32(1,0) , 2*8 );
  BOOST_CHECK_EQUAL( M32(1,1) , 2*21 );
  BOOST_CHECK_EQUAL( M32(2,0) , 2*4 );
  BOOST_CHECK_EQUAL( M32(2,1) , 2*15 );

  M32 -= Transpose(A23)*diag(V);

  BOOST_CHECK_EQUAL( M32(0,0) , 6 );
  BOOST_CHECK_EQUAL( M32(0,1) , 18 );
  BOOST_CHECK_EQUAL( M32(1,0) , 8 );
  BOOST_CHECK_EQUAL( M32(1,1) , 21 );
  BOOST_CHECK_EQUAL( M32(2,0) , 4 );
  BOOST_CHECK_EQUAL( M32(2,1) , 15 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Diag_3x3_test )
{
  typedef Real T;

  VectorS<3,T> V = {1, 2, 3};
  MatrixS<3,3,T> D;

  D = diag(V);

  BOOST_CHECK_EQUAL( D(0,0) , 1 );
  BOOST_CHECK_EQUAL( D(1,0) , 0 );
  BOOST_CHECK_EQUAL( D(2,0) , 0 );
  BOOST_CHECK_EQUAL( D(0,1) , 0 );
  BOOST_CHECK_EQUAL( D(1,1) , 2 );
  BOOST_CHECK_EQUAL( D(2,1) , 0 );
  BOOST_CHECK_EQUAL( D(0,2) , 0 );
  BOOST_CHECK_EQUAL( D(1,2) , 0 );
  BOOST_CHECK_EQUAL( D(2,2) , 3 );

  D += diag(V);

  BOOST_CHECK_EQUAL( D(0,0) , 2*1 );
  BOOST_CHECK_EQUAL( D(1,0) ,   0 );
  BOOST_CHECK_EQUAL( D(2,0) ,   0 );
  BOOST_CHECK_EQUAL( D(0,1) ,   0 );
  BOOST_CHECK_EQUAL( D(1,1) , 2*2 );
  BOOST_CHECK_EQUAL( D(2,1) ,   0 );
  BOOST_CHECK_EQUAL( D(0,2) ,   0 );
  BOOST_CHECK_EQUAL( D(1,2) ,   0 );
  BOOST_CHECK_EQUAL( D(2,2) , 2*3 );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
