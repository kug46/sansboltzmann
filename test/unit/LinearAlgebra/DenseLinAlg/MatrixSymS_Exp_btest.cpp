// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// MatrixS_btest
// testing of MatrixS<M,N,T> class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Exp.h"

#include "chkMatrixS_btest.h"

#include <iostream>
using namespace std;


//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
namespace DLA
{

}
}

using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( MatrixSymS_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Exp_MatrixSymS_2x2 )
{

  MatrixSymS<2,Real> A1 = { { 1.2},
                            {-0.3, 1.5} };
  MatrixSymS<2,Real> P;

  P = exp(A1);

  BOOST_CHECK_CLOSE( 3.486923521292471e+00, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE(-1.179048056608556e+00, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 4.665971577901029e+00, P(1,1), 1e-11 );

  MatrixSymS<2,Real> A2 = { {4.9},
                            {0.7, 2.3} };

  P = exp(A2);

  BOOST_CHECK_CLOSE( 1.511343235796655e+02, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( 3.599579203112186e+01, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 1.743566746406995e+01, P(1,1), 1e-11 );

  EigenSystemPair<2,Real> EL(A1);

  P = exp(EL);

  BOOST_CHECK_CLOSE( 3.486923521292471e+00, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE(-1.179048056608556e+00, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 4.665971577901029e+00, P(1,1), 1e-11 );

  EigenSystem(A2, EL);

  P = exp(EL);

  BOOST_CHECK_CLOSE( 1.511343235796655e+02, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( 3.599579203112186e+01, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 1.743566746406995e+01, P(1,1), 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Exp_MatrixSymS_3x3 )
{

  MatrixSymS<3,Real> A1 = { { 1.2},
                            {-0.3, 1.5},
                            { 0.2, 2.7, 4.6} };
  MatrixSymS<3,Real> P;

  P = exp(A1);

  BOOST_CHECK_CLOSE( 3.478287088903688e+00, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( 4.881454684009956e-01, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 1.199319523125021e+02, P(1,1), 1e-11 );
  BOOST_CHECK_CLOSE( 2.226573258674886e+00, P(2,0), 1e-11 );
  BOOST_CHECK_CLOSE( 2.054714550301101e+02, P(2,1), 1e-11 );
  BOOST_CHECK_CLOSE( 3.561271788549555e+02, P(2,2), 1e-11 );

  MatrixSymS<3,Real> A2 = { { 4.9},
                            { 0.7, 2.3},
                            {-0.1, 1.4,-3.9} };

  P = exp(A2);

  BOOST_CHECK_CLOSE( 1.512693467182085e+02, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( 3.740871096962268e+01, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 2.074609182518339e+01, P(1,1), 1e-11 );
  BOOST_CHECK_CLOSE( 3.962856226641679e+00, P(2,0), 1e-11 );
  BOOST_CHECK_CLOSE( 3.483713786031443e+00, P(2,1), 1e-11 );
  BOOST_CHECK_CLOSE( 6.647375901788216e-01, P(2,2), 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Exp_VectorS_3x1 )
{

  VectorS<3,Real> V = {0.1,1.3,10.4};
  VectorS<3,Real> P;

  P = exp(V);

  BOOST_CHECK_CLOSE( 1.105170918075648e+00, P[0], 1e-11 );
  BOOST_CHECK_CLOSE( 3.669296667619244e+00, P[1], 1e-11 );
  BOOST_CHECK_CLOSE( 3.285962567444333e+04, P[2], 1e-11 );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
