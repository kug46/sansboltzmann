// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// VectorD_btest
// testing of VectorD<Real> class

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Sub.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
namespace DLA
{
template class VectorDView<Real>;
template class VectorD<Real>;
}
}

using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( VectorD_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorDView_ctor )
{

  Real a1[4];
  VectorDView<Real> v1(a1, 4);
  v1 = 0;
  v1[0] = 1;
  v1[2] = 3;
  v1[3] = 2;

  BOOST_CHECK_EQUAL( 4, v1.size() );
  BOOST_CHECK_EQUAL( 4, v1.m() );
  BOOST_CHECK_EQUAL( 1, v1.n() );
  BOOST_CHECK_EQUAL( 1, v1.stride() );

  BOOST_CHECK_EQUAL( 1, v1[0] );
  BOOST_CHECK_EQUAL( 0, v1[1] );
  BOOST_CHECK_EQUAL( 3, v1[2] );
  BOOST_CHECK_EQUAL( 2, v1[3] );

  BOOST_CHECK_EQUAL( 1, const_cast<const VectorDView<Real>& >(v1)[0] );
  BOOST_CHECK_EQUAL( 0, const_cast<const VectorDView<Real>& >(v1)[1] );
  BOOST_CHECK_EQUAL( 3, const_cast<const VectorDView<Real>& >(v1)[2] );
  BOOST_CHECK_EQUAL( 2, const_cast<const VectorDView<Real>& >(v1)[3] );

  BOOST_CHECK_EQUAL( 1, v1(0) );
  BOOST_CHECK_EQUAL( 0, v1(1) );
  BOOST_CHECK_EQUAL( 3, v1(2) );
  BOOST_CHECK_EQUAL( 2, v1(3) );

  BOOST_CHECK_EQUAL( 1, const_cast<const VectorDView<Real>& >(v1)(0) );
  BOOST_CHECK_EQUAL( 0, const_cast<const VectorDView<Real>& >(v1)(1) );
  BOOST_CHECK_EQUAL( 3, const_cast<const VectorDView<Real>& >(v1)(2) );
  BOOST_CHECK_EQUAL( 2, const_cast<const VectorDView<Real>& >(v1)(3) );

  BOOST_CHECK_EQUAL( 1, a1[0] );
  BOOST_CHECK_EQUAL( 0, a1[1] );
  BOOST_CHECK_EQUAL( 3, a1[2] );
  BOOST_CHECK_EQUAL( 2, a1[3] );

  Real a2[6];
  a2[1] = 0;
  a2[2] = 42;
  a2[4] = 0;
  a2[5] = 1929;
  VectorDView<Real> v2(a2, 2, 3);
  v2 = 0;
  v2[0] = 1;
  v2[1] = 2;

  BOOST_CHECK_EQUAL( 2, v2.size() );
  BOOST_CHECK_EQUAL( 2, v2.m() );
  BOOST_CHECK_EQUAL( 1, v2.n() );
  BOOST_CHECK_EQUAL( 3, v2.stride() );

  BOOST_CHECK_EQUAL( 1, v2[0] );
  BOOST_CHECK_EQUAL( 2, v2[1] );

  BOOST_CHECK_EQUAL(    1, a2[0] );
  BOOST_CHECK_EQUAL(    0, a2[1] );
  BOOST_CHECK_EQUAL(   42, a2[2] );
  BOOST_CHECK_EQUAL(    2, a2[3] );
  BOOST_CHECK_EQUAL(    0, a2[4] );
  BOOST_CHECK_EQUAL( 1929, a2[5] );


  VectorDView<Real> v3(a2, 6);

  BOOST_CHECK_EQUAL( 6, v3.size() );
  BOOST_CHECK_EQUAL( 6, v3.m() );
  BOOST_CHECK_EQUAL( 1, v3.n() );
  BOOST_CHECK_EQUAL( 1, v3.stride() );

  BOOST_CHECK_EQUAL(    1, v3[0] );
  BOOST_CHECK_EQUAL(    0, v3[1] );
  BOOST_CHECK_EQUAL(   42, v3[2] );
  BOOST_CHECK_EQUAL(    2, v3[3] );
  BOOST_CHECK_EQUAL(    0, v3[4] );
  BOOST_CHECK_EQUAL( 1929, v3[5] );


  VectorD<Real> v4({1,2,3});
  BOOST_CHECK_EQUAL(    1, v4[0] );
  BOOST_CHECK_EQUAL(    2, v4[1] );
  BOOST_CHECK_EQUAL(    3, v4[2] );

  VectorD<Real> v5 = {42,2,3};
  BOOST_CHECK_EQUAL(   42, v5[0] );
  BOOST_CHECK_EQUAL(    2, v5[1] );
  BOOST_CHECK_EQUAL(    3, v5[2] );

  v5 = {1929,3,4};
  BOOST_CHECK_EQUAL( 1929, v5[0] );
  BOOST_CHECK_EQUAL(    3, v5[1] );
  BOOST_CHECK_EQUAL(    4, v5[2] );

  BOOST_CHECK_THROW( (v5 = {1929,3,4,6}), AssertionException );


  VectorD< VectorS<1,Real> > v6(3, 0.);
  BOOST_CHECK_EQUAL( 0, v6[0][0] );
  BOOST_CHECK_EQUAL( 0, v6[1][0] );
  BOOST_CHECK_EQUAL( 0, v6[2][0] );

  v6 = 1.;
  BOOST_CHECK_EQUAL( 1, v6[0][0] );
  BOOST_CHECK_EQUAL( 1, v6[1][0] );
  BOOST_CHECK_EQUAL( 1, v6[2][0] );

  static_cast< VectorDView< VectorS<1,Real> > >(v6) = 2.;
  BOOST_CHECK_EQUAL( 2, v6[0][0] );
  BOOST_CHECK_EQUAL( 2, v6[1][0] );
  BOOST_CHECK_EQUAL( 2, v6[2][0] );

  VectorD< VectorS<2, Real> > v7 = {{1,2},{3,4}};
  BOOST_CHECK_EQUAL( 1, v7[0][0] );
  BOOST_CHECK_EQUAL( 2, v7[0][1] );
  BOOST_CHECK_EQUAL( 3, v7[1][0] );
  BOOST_CHECK_EQUAL( 4, v7[1][1] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( array_ops1 )
{
  Real a0[1];
  const Real data = 3;
  VectorDView<Real> a1(a0, 1);
  a1 = data;
  VectorD<Real> a2(a1);
  VectorD<Real> a3(1, 1), a4(1), a5(1);

  // size
  BOOST_CHECK( a1.m() == 1 );
  BOOST_CHECK( a2.m() == 1 );
  BOOST_CHECK( a3.m() == 1 );
  BOOST_CHECK( a4.m() == 1 );
  BOOST_CHECK( a5.m() == 1 );

  // ctors
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL(  1, a3[0] );

  const VectorD<Real> a6(1, &data);
  BOOST_CHECK_EQUAL( data, a6[0] );
  BOOST_CHECK_EQUAL( data, a6(0) );

  // assignment
  a3 = a1;
  a4 = data;
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a3[0] );
  BOOST_CHECK_EQUAL(  3, a4[0] );

  a1 = a2 = a3 = data;
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL(  3, a3[0] );

  a4 = data;
  a1 = a2 = a3 = a4;
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL(  3, a3[0] );
  BOOST_CHECK_EQUAL(  3, a4[0] );

  // unary
  a2 = +a1;
  a3 = -a1;
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL( -3, a3[0] );

  // binary accumulation
  a1 = data;
  a2 = a1;
  a3 = a1;
  a2 += a1;
  a3 -= a1;
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  6, a2[0] );
  BOOST_CHECK_EQUAL(  0, a3[0] );

  // binary operators
  a1 = a2 = data;
  a3 = a1 + a2;
  a4 = a1 - a2;
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL(  6, a3[0] );
  BOOST_CHECK_EQUAL(  0, a4[0] );

  // arithmetic combinations

  a1 = a2 = data;
  a3 = a1 + a2;
  a4 = a1 + a2 + a3;
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL(  6, a3[0] );
  BOOST_CHECK_EQUAL( 12, a4[0] );

  a2 += a1;
  a3 += a1 + a2;
  a4 += a1 + a2 + a3;
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  6, a2[0] );
  BOOST_CHECK_EQUAL( 15, a3[0] );
  BOOST_CHECK_EQUAL( 36, a4[0] );

  a3 = a1 - a2;
  a4 = a1 - a2 - a3;
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  6, a2[0] );
  BOOST_CHECK_EQUAL( -3, a3[0] );
  BOOST_CHECK_EQUAL(  0, a4[0] );

  a2 -= a1;
  a3 -= a1 - a2;
  a4 -= a1 - a2 - a3;
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL( -3, a3[0] );
  BOOST_CHECK_EQUAL( -3, a4[0] );

  a3 = a1 - a2;
  a4 = a1 + a2 - a3;
  a5 = a1 - a2 + a3;
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL(  0, a3[0] );
  BOOST_CHECK_EQUAL(  6, a4[0] );
  BOOST_CHECK_EQUAL(  0, a5[0] );

  a5 = (a1 + a2) + (a3 + a4);
  BOOST_CHECK_EQUAL( 12, a5[0] );
  a5 = (a1 + a2) + (a3 - a4);
  BOOST_CHECK_EQUAL(  0, a5[0] );
  a5 = (a1 + a2) - (a3 + a4);
  BOOST_CHECK_EQUAL(  0, a5[0] );
  a5 = (a1 + a2) - (a3 - a4);
  BOOST_CHECK_EQUAL( 12, a5[0] );
  a5 = (a1 - a2) + (a3 + a4);
  BOOST_CHECK_EQUAL(  6, a5[0] );
  a5 = (a1 - a2) + (a3 - a4);
  BOOST_CHECK_EQUAL( -6, a5[0] );
  a5 = (a1 - a2) - (a3 + a4);
  BOOST_CHECK_EQUAL( -6, a5[0] );
  a5 = (a1 - a2) - (a3 - a4);
  BOOST_CHECK_EQUAL(  6, a5[0] );
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL(  0, a3[0] );
  BOOST_CHECK_EQUAL(  6, a4[0] );

  a5 += (a1 + a2) + (a3 + a4);
  a5 += (a1 + a2) + (a3 - a4);
  a5 += (a1 + a2) - (a3 + a4);
  a5 += (a1 + a2) - (a3 - a4);
  a5 += (a1 - a2) + (a3 + a4);
  a5 += (a1 - a2) + (a3 - a4);
  a5 += (a1 - a2) - (a3 + a4);
  a5 += (a1 - a2) - (a3 - a4);
  BOOST_CHECK_EQUAL( 30, a5[0] );
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL(  0, a3[0] );
  BOOST_CHECK_EQUAL(  6, a4[0] );

  a5 -= (a1 + a2) + (a3 + a4);
  a5 -= (a1 + a2) + (a3 - a4);
  a5 -= (a1 + a2) - (a3 + a4);
  a5 -= (a1 + a2) - (a3 - a4);
  a5 -= (a1 - a2) + (a3 + a4);
  a5 -= (a1 - a2) + (a3 - a4);
  a5 -= (a1 - a2) - (a3 + a4);
  a5 -= (a1 - a2) - (a3 - a4);
  BOOST_CHECK_EQUAL(  6, a5[0] );
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL(  0, a3[0] );
  BOOST_CHECK_EQUAL(  6, a4[0] );

  a1 = data;

  a2 = 4*a1;
  a3 = a2*7;
  BOOST_CHECK_EQUAL(   3, a1[0] );
  BOOST_CHECK_EQUAL(  12, a2[0] );
  BOOST_CHECK_EQUAL(  84, a3[0] );

  a2 += 4*a1;
  a3 += a2*7;
  BOOST_CHECK_EQUAL(   3, a1[0] );
  BOOST_CHECK_EQUAL(  24, a2[0] );
  BOOST_CHECK_EQUAL( 252, a3[0] );

  a2 -= 4*a1;
  a3 -= a2*7;
  BOOST_CHECK_EQUAL(   3, a1[0] );
  BOOST_CHECK_EQUAL(  12, a2[0] );
  BOOST_CHECK_EQUAL( 168, a3[0] );

  a5 = 2*(a1 + a2) + (a3 + a4)*3;
  BOOST_CHECK_EQUAL(  552, a5[0] );
  a5 = 2*(a1 + a2) + (a3 - a4)*3;
  BOOST_CHECK_EQUAL(  516, a5[0] );
  a5 = 2*(a1 + a2) - (a3 + a4)*3;
  BOOST_CHECK_EQUAL( -492, a5[0] );
  a5 = 2*(a1 + a2) - (a3 - a4)*3;
  BOOST_CHECK_EQUAL( -456, a5[0] );
  a5 = 2*(a1 - a2) + (a3 + a4)*3;
  BOOST_CHECK_EQUAL(  504, a5[0] );
  a5 = 2*(a1 - a2) + (a3 - a4)*3;
  BOOST_CHECK_EQUAL(  468, a5[0] );
  a5 = 2*(a1 - a2) - (a3 + a4)*3;
  BOOST_CHECK_EQUAL( -540, a5[0] );
  a5 = 2*(a1 - a2) - (a3 - a4)*3;
  BOOST_CHECK_EQUAL( -504, a5[0] );
  BOOST_CHECK_EQUAL(    3, a1[0] );
  BOOST_CHECK_EQUAL(   12, a2[0] );
  BOOST_CHECK_EQUAL(  168, a3[0] );
  BOOST_CHECK_EQUAL(    6, a4[0] );

  a5 += 2*(a1 + a2) + (a3 + a4)*3;
  BOOST_CHECK_EQUAL(   48, a5[0] );
  a5 += 2*(a1 + a2) + (a3 - a4)*3;
  BOOST_CHECK_EQUAL(  564, a5[0] );
  a5 += 2*(a1 + a2) - (a3 + a4)*3;
  BOOST_CHECK_EQUAL(   72, a5[0] );
  a5 += 2*(a1 + a2) - (a3 - a4)*3;
  BOOST_CHECK_EQUAL( -384, a5[0] );
  a5 += 2*(a1 - a2) + (a3 + a4)*3;
  BOOST_CHECK_EQUAL(  120, a5[0] );
  a5 += 2*(a1 - a2) + (a3 - a4)*3;
  BOOST_CHECK_EQUAL(  588, a5[0] );
  a5 += 2*(a1 - a2) - (a3 + a4)*3;
  BOOST_CHECK_EQUAL(   48, a5[0] );
  a5 += 2*(a1 - a2) - (a3 - a4)*3;
  BOOST_CHECK_EQUAL( -456, a5[0] );
  BOOST_CHECK_EQUAL(    3, a1[0] );
  BOOST_CHECK_EQUAL(   12, a2[0] );
  BOOST_CHECK_EQUAL(  168, a3[0] );
  BOOST_CHECK_EQUAL(    6, a4[0] );

  a5 -= 2*(a1 + a2) + (a3 + a4)*3;
  BOOST_CHECK_EQUAL( -1008, a5[0] );
  a5 -= 2*(a1 + a2) + (a3 - a4)*3;
  BOOST_CHECK_EQUAL( -1524, a5[0] );
  a5 -= 2*(a1 + a2) - (a3 + a4)*3;
  BOOST_CHECK_EQUAL( -1032, a5[0] );
  a5 -= 2*(a1 + a2) - (a3 - a4)*3;
  BOOST_CHECK_EQUAL(  -576, a5[0] );
  a5 -= 2*(a1 - a2) + (a3 + a4)*3;
  BOOST_CHECK_EQUAL( -1080, a5[0] );
  a5 -= 2*(a1 - a2) + (a3 - a4)*3;
  BOOST_CHECK_EQUAL( -1548, a5[0] );
  a5 -= 2*(a1 - a2) - (a3 + a4)*3;
  BOOST_CHECK_EQUAL( -1008, a5[0] );
  a5 -= 2*(a1 - a2) - (a3 - a4)*3;
  BOOST_CHECK_EQUAL(  -504, a5[0] );
  BOOST_CHECK_EQUAL(     3, a1[0] );
  BOOST_CHECK_EQUAL(    12, a2[0] );
  BOOST_CHECK_EQUAL(   168, a3[0] );
  BOOST_CHECK_EQUAL(     6, a4[0] );

  a5 = 2*(a1 + a2)*3;
  BOOST_CHECK_EQUAL( 90, a5[0] );
  a5 = 2*3*(a1 + a2);
  BOOST_CHECK_EQUAL( 90, a5[0] );
  a5 = (a1 + a2)*2*3;
  BOOST_CHECK_EQUAL( 90, a5[0] );
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL( 12, a2[0] );

  a2 = +a1;
  BOOST_CHECK_EQUAL(  3, a2[0] );
  a3 = -a2;
  BOOST_CHECK_EQUAL( -3, a3[0] );
  a4 = +(a1 + a2);
  BOOST_CHECK_EQUAL(  6, a4[0] );
  a4 = +(a1 - a2);
  BOOST_CHECK_EQUAL(  0, a4[0] );
  a4 = -(a1 + a2);
  BOOST_CHECK_EQUAL( -6, a4[0] );
  a4 = -(a1 - a2);
  BOOST_CHECK_EQUAL(  0, a4[0] );
  a4 = +(a1 + a2) + a3;
  BOOST_CHECK_EQUAL(  3, a4[0] );
  a4 = -(a1 + a2) + a3;
  BOOST_CHECK_EQUAL( -9, a4[0] );
  BOOST_CHECK_EQUAL(  3, a1[0] );
  BOOST_CHECK_EQUAL(  3, a2[0] );
  BOOST_CHECK_EQUAL( -3, a3[0] );

  a4 = +5*a1;
  BOOST_CHECK_EQUAL(  15, a4[0] );
  a4 = -5*a1;
  BOOST_CHECK_EQUAL( -15, a4[0] );
  a4 = +a1*5;
  BOOST_CHECK_EQUAL(  15, a4[0] );
  a4 = -a1*5;
  BOOST_CHECK_EQUAL( -15, a4[0] );
  a4 = +(5*a1);
  BOOST_CHECK_EQUAL(  15, a4[0] );
  a4 = -(5*a1);
  BOOST_CHECK_EQUAL( -15, a4[0] );
  a4 = +(a1*5);
  BOOST_CHECK_EQUAL(  15, a4[0] );
  a4 = -(a1*5);
  BOOST_CHECK_EQUAL( -15, a4[0] );
  BOOST_CHECK_EQUAL(   3, a1[0] );
}


//----------------------------------------------------------------------------//
bool chkVectorD4( const VectorD<Real>& z, Real a, Real b, Real c, Real d )
{
  bool isEqual = true;
  if ((z[0] != a) || (z[1] != b) || (z[2] != c) || (z[3] != d))
  {
    isEqual = false;
    std::cout << "actual (" << z << ")  expected "
              << "(" << a << " " << b << " " << c << " " << d << ")" << std::endl;
  }
  return isEqual;
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(array_ops4)
{
  const Real vdata[4] = {1,2,3,4};
  VectorD<Real> a1(4, vdata);
  VectorD<Real> a2(a1);
  VectorD<Real> a3(4), a4(4), a5(4);

  // size
  BOOST_CHECK( a1.m() == 4 );
  BOOST_CHECK( a2.m() == 4 );
  BOOST_CHECK( a3.m() == 4 );
  BOOST_CHECK( a4.m() == 4 );
  BOOST_CHECK( a5.m() == 4 );

  // ctors
  BOOST_CHECK( chkVectorD4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a2, 1,2,3,4 ) );

  // assignment
  a3 = a1;
  a4 = 5;
  BOOST_CHECK( chkVectorD4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a3, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a4, 5,5,5,5 ) );

  a2 = a3 = 3;
  BOOST_CHECK( chkVectorD4( a2, 3,3,3,3 ) );
  BOOST_CHECK( chkVectorD4( a3, 3,3,3,3 ) );

  // unary
  a2 = +a1;
  a3 = -a1;
  BOOST_CHECK( chkVectorD4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a3, -1,-2,-3,-4 ) );

  // binary accumulation
  a2 = 5;
  a3 = 5;
  a2 += a1;
  a3 -= a1;
  BOOST_CHECK( chkVectorD4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a2, 6,7,8,9 ) );
  BOOST_CHECK( chkVectorD4( a3, 4,3,2,1 ) );

  // binary operators
  a2 = 3;
  a3 = a1 + a2;
  a4 = a1 - a2;
  BOOST_CHECK( chkVectorD4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a2, 3,3,3,3 ) );
  BOOST_CHECK( chkVectorD4( a3, 4,5,6,7 ) );
  BOOST_CHECK( chkVectorD4( a4, -2,-1,0,1 ) );

  // arithmetic combinations

  a2 = a1;
  a3 = a1 + a2;
  a4 = a1 + a2 + a3;
  BOOST_CHECK( chkVectorD4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a3, 2,4,6,8 ) );
  BOOST_CHECK( chkVectorD4( a4, 4,8,12,16 ) );

  a2 += a1;
  a3 += a1 + a2;
  a4 += a1 + a2 + a3;
  BOOST_CHECK( chkVectorD4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a2, 2,4,6,8 ) );
  BOOST_CHECK( chkVectorD4( a3, 5,10,15,20 ) );
  BOOST_CHECK( chkVectorD4( a4, 12,24,36,48 ) );

  a3 = a1 - a2;
  a4 = a1 - a2 - a3;
  BOOST_CHECK( chkVectorD4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a2, 2,4,6,8 ) );
  BOOST_CHECK( chkVectorD4( a3, -1,-2,-3,-4 ) );
  BOOST_CHECK( chkVectorD4( a4, 0,0,0,0 ) );

  a2 -= a1;
  a3 -= a1 - a2;
  a4 -= a1 - a2 - a3;
  BOOST_CHECK( chkVectorD4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a3, -1,-2,-3,-4 ) );
  BOOST_CHECK( chkVectorD4( a4, -1,-2,-3,-4 ) );

  a3 = a1 - a2;
  a4 = a1 + a2 - a3;
  a5 = a1 - a2 + a3;
  BOOST_CHECK( chkVectorD4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a3, 0,0,0,0 ) );
  BOOST_CHECK( chkVectorD4( a4, 2,4,6,8 ) );
  BOOST_CHECK( chkVectorD4( a5, 0,0,0,0 ) );

  a5 = (a1 + a2) + (a3 + a4);
  BOOST_CHECK( chkVectorD4( a5, 4,8,12,16 ) );
  a5 = (a1 + a2) + (a3 - a4);
  BOOST_CHECK( chkVectorD4( a5, 0,0,0,0 ) );
  a5 = (a1 + a2) - (a3 + a4);
  BOOST_CHECK( chkVectorD4( a5, 0,0,0,0 ) );
  a5 = (a1 + a2) - (a3 - a4);
  BOOST_CHECK( chkVectorD4( a5, 4,8,12,16 ) );
  a5 = (a1 - a2) + (a3 + a4);
  BOOST_CHECK( chkVectorD4( a5, 2,4,6,8 ) );
  a5 = (a1 - a2) + (a3 - a4);
  BOOST_CHECK( chkVectorD4( a5, -2,-4,-6,-8 ) );
  a5 = (a1 - a2) - (a3 + a4);
  BOOST_CHECK( chkVectorD4( a5, -2,-4,-6,-8 ) );
  a5 = (a1 - a2) - (a3 - a4);
  BOOST_CHECK( chkVectorD4( a5, 2,4,6,8 ) );
  BOOST_CHECK( chkVectorD4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a3, 0,0,0,0 ) );
  BOOST_CHECK( chkVectorD4( a4, 2,4,6,8 ) );

  a5 += (a1 + a2) + (a3 + a4);
  a5 += (a1 + a2) + (a3 - a4);
  a5 += (a1 + a2) - (a3 + a4);
  a5 += (a1 + a2) - (a3 - a4);
  a5 += (a1 - a2) + (a3 + a4);
  a5 += (a1 - a2) + (a3 - a4);
  a5 += (a1 - a2) - (a3 + a4);
  a5 += (a1 - a2) - (a3 - a4);
  BOOST_CHECK( chkVectorD4( a5, 10,20,30,40 ) );
  BOOST_CHECK( chkVectorD4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a3, 0,0,0,0 ) );
  BOOST_CHECK( chkVectorD4( a4, 2,4,6,8 ) );

  a5 -= (a1 + a2) + (a3 + a4);
  a5 -= (a1 + a2) + (a3 - a4);
  a5 -= (a1 + a2) - (a3 + a4);
  a5 -= (a1 + a2) - (a3 - a4);
  a5 -= (a1 - a2) + (a3 + a4);
  a5 -= (a1 - a2) + (a3 - a4);
  a5 -= (a1 - a2) - (a3 + a4);
  a5 -= (a1 - a2) - (a3 - a4);
  BOOST_CHECK( chkVectorD4( a5, 2,4,6,8 ) );
  BOOST_CHECK( chkVectorD4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a3, 0,0,0,0 ) );
  BOOST_CHECK( chkVectorD4( a4, 2,4,6,8 ) );

  a1(0) = 1;
  a1(1) = 2;
  a1(2) = 3;
  a1(3) = 4;

  a2 = 1*a1;
  a3 = a2*2;
  BOOST_CHECK( chkVectorD4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a3, 2,4,6,8 ) );

  a2 += 1*a1;
  a3 += a2*2;
  BOOST_CHECK( chkVectorD4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a2, 2,4,6,8 ) );
  BOOST_CHECK( chkVectorD4( a3, 6,12,18,24 ) );

  a2 -= 1*a1;
  a3 -= a2*2;
  BOOST_CHECK( chkVectorD4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a3, 4,8,12,16 ) );

  a5 = 1*(a1 + a2) + (a3 + a4)*2;
  BOOST_CHECK( chkVectorD4( a5, 14,28,42,56 ) );
  a5 = 1*(a1 + a2) + (a3 - a4)*2;
  BOOST_CHECK( chkVectorD4( a5, 6,12,18,24 ) );
  a5 = 1*(a1 + a2) - (a3 + a4)*2;
  BOOST_CHECK( chkVectorD4( a5, -10,-20,-30,-40 ) );
  a5 = 1*(a1 + a2) - (a3 - a4)*2;
  BOOST_CHECK( chkVectorD4( a5, -2,-4,-6,-8 ) );
  a5 = 1*(a1 - a2) + (a3 + a4)*2;
  BOOST_CHECK( chkVectorD4( a5, 12,24,36,48 ) );
  a5 = 1*(a1 - a2) + (a3 - a4)*2;
  BOOST_CHECK( chkVectorD4( a5, 4,8,12,16 ) );
  a5 = 1*(a1 - a2) - (a3 + a4)*2;
  BOOST_CHECK( chkVectorD4( a5, -12,-24,-36,-48 ) );
  a5 = 1*(a1 - a2) - (a3 - a4)*2;
  BOOST_CHECK( chkVectorD4( a5, -4,-8,-12,-16 ) );
  BOOST_CHECK( chkVectorD4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a3, 4,8,12,16 ) );
  BOOST_CHECK( chkVectorD4( a4, 2,4,6,8 ) );

  a5 += 1*(a1 + a2) + (a3 + a4)*2;
  BOOST_CHECK( chkVectorD4( a5, 10,20,30,40 ) );
  a5 += 1*(a1 + a2) + (a3 - a4)*2;
  BOOST_CHECK( chkVectorD4( a5, 16,32,48,64 ) );
  a5 += 1*(a1 + a2) - (a3 + a4)*2;
  BOOST_CHECK( chkVectorD4( a5, 6,12,18,24 ) );
  a5 += 1*(a1 + a2) - (a3 - a4)*2;
  BOOST_CHECK( chkVectorD4( a5, 4,8,12,16 ) );
  a5 += 1*(a1 - a2) + (a3 + a4)*2;
  BOOST_CHECK( chkVectorD4( a5, 16,32,48,64 ) );
  a5 += 1*(a1 - a2) + (a3 - a4)*2;
  BOOST_CHECK( chkVectorD4( a5, 20,40,60,80 ) );
  a5 += 1*(a1 - a2) - (a3 + a4)*2;
  BOOST_CHECK( chkVectorD4( a5, 8,16,24,32 ) );
  a5 += 1*(a1 - a2) - (a3 - a4)*2;
  BOOST_CHECK( chkVectorD4( a5, 4,8,12,16 ) );
  BOOST_CHECK( chkVectorD4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a3, 4,8,12,16 ) );
  BOOST_CHECK( chkVectorD4( a4, 2,4,6,8 ) );

  a5 -= 1*(a1 + a2) + (a3 + a4)*2;
  BOOST_CHECK( chkVectorD4( a5, -10,-20,-30,-40 ) );
  a5 -= 1*(a1 + a2) + (a3 - a4)*2;
  BOOST_CHECK( chkVectorD4( a5, -16,-32,-48,-64 ) );
  a5 -= 1*(a1 + a2) - (a3 + a4)*2;
  BOOST_CHECK( chkVectorD4( a5, -6,-12,-18,-24 ) );
  a5 -= 1*(a1 + a2) - (a3 - a4)*2;
  BOOST_CHECK( chkVectorD4( a5, -4,-8,-12,-16 ) );
  a5 -= 1*(a1 - a2) + (a3 + a4)*2;
  BOOST_CHECK( chkVectorD4( a5, -16,-32,-48,-64 ) );
  a5 -= 1*(a1 - a2) + (a3 - a4)*2;
  BOOST_CHECK( chkVectorD4( a5, -20,-40,-60,-80 ) );
  a5 -= 1*(a1 - a2) - (a3 + a4)*2;
  BOOST_CHECK( chkVectorD4( a5, -8,-16,-24,-32 ) );
  a5 -= 1*(a1 - a2) - (a3 - a4)*2;
  BOOST_CHECK( chkVectorD4( a5, -4,-8,-12,-16 ) );
  BOOST_CHECK( chkVectorD4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a3, 4,8,12,16 ) );
  BOOST_CHECK( chkVectorD4( a4, 2,4,6,8 ) );

  a5 = 1*(a1 + a2)*2;
  BOOST_CHECK( chkVectorD4( a5, 4,8,12,16 ) );
  a5 = 1*2*(a1 + a2);
  BOOST_CHECK( chkVectorD4( a5, 4,8,12,16 ) );
  a5 = (a1 + a2)*1*2;
  BOOST_CHECK( chkVectorD4( a5, 4,8,12,16 ) );
  BOOST_CHECK( chkVectorD4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a2, 1,2,3,4 ) );

  a2 = +a1;
  BOOST_CHECK( chkVectorD4( a2, 1,2,3,4 ) );
  a3 = -a2;
  BOOST_CHECK( chkVectorD4( a3, -1,-2,-3,-4 ) );
  a4 = +(a1 + a2);
  BOOST_CHECK( chkVectorD4( a4, 2,4,6,8 ) );
  a4 = +(a1 - a2);
  BOOST_CHECK( chkVectorD4( a4, 0,0,0,0 ) );
  a4 = -(a1 + a2);
  BOOST_CHECK( chkVectorD4( a4, -2,-4,-6,-8 ) );
  a4 = -(a1 - a2);
  BOOST_CHECK( chkVectorD4( a4, 0,0,0,0 ) );
  a4 = +(a1 + a2) + a3;
  BOOST_CHECK( chkVectorD4( a4, 1,2,3,4 ) );
  a4 = -(a1 + a2) + a3;
  BOOST_CHECK( chkVectorD4( a4, -3,-6,-9,-12 ) );
  BOOST_CHECK( chkVectorD4( a1, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a2, 1,2,3,4 ) );
  BOOST_CHECK( chkVectorD4( a3, -1,-2,-3,-4 ) );

  a4 = +1*a1;
  BOOST_CHECK( chkVectorD4( a4, 1,2,3,4 ) );
  a4 = -1*a1;
  BOOST_CHECK( chkVectorD4( a4, -1,-2,-3,-4 ) );
  a4 = +a1*1;
  BOOST_CHECK( chkVectorD4( a4, 1,2,3,4 ) );
  a4 = -a1*1;
  BOOST_CHECK( chkVectorD4( a4, -1,-2,-3,-4 ) );
  a4 = +(1*a1);
  BOOST_CHECK( chkVectorD4( a4, 1,2,3,4 ) );
  a4 = -(1*a1);
  BOOST_CHECK( chkVectorD4( a4, -1,-2,-3,-4 ) );
  a4 = +(a1*1);
  BOOST_CHECK( chkVectorD4( a4, 1,2,3,4 ) );
  a4 = -(a1*1);
  BOOST_CHECK( chkVectorD4( a4, -1,-2,-3,-4 ) );
  BOOST_CHECK( chkVectorD4( a1, 1,2,3,4 ) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorD_sub_vector )
{
  VectorD<Real> v1 = {2, 3, 4, 5};
  VectorD<Real> sub(2);

  subVectorValue(v1, {0,2}, 2., sub);

  BOOST_CHECK_EQUAL( 2*2, sub[0] );
  BOOST_CHECK_EQUAL( 2*4, sub[1] );

  subVectorPlus(v1, {0,2}, 3., sub);

  BOOST_CHECK_EQUAL( 3*2+2*2, sub[0] );
  BOOST_CHECK_EQUAL( 3*4+2*4, sub[1] );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/DenseLinAlg/VectorD_pattern.txt", true );

  VectorD<Real> v(4);
  for ( int i = 0; i < 4; i++ ) v[i] = i;

  output << v << std::endl;
  BOOST_CHECK( output.match_pattern() );
  v.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
