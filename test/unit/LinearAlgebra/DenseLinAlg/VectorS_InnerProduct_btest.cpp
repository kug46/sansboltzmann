// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS_InnerProduct.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include <iostream>
using namespace SANS::DLA;


//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_InnerProduct_test )
{
  typedef VectorS<4,int> VectorS4;
  VectorS4 a = {1,2,3,4};
  VectorS4 b = {2,3,4,5};

  int d = InnerProduct(a,b);

  int exact = 0;
  for (int i = 0; i < VectorS4::M; i++)
    exact += a[i]*b[i];

  BOOST_CHECK_EQUAL(d, exact);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_InnerProduct_diagonal_test )
{
  typedef VectorS<4,int> VectorS4;
  VectorS4 a = {1,2,3,4};
  VectorS4 b = {2,3,4,5};
  // third vector - diagonal entries of a matrix in the inner product weight
  VectorS4 c = {3,4,5,6};

  int d = InnerProductDiag(a,b,c);

  int exact = 0;
  for (int i = 0; i < VectorS4::M; i++)
    exact += a[i]*b[i]*c[i];

  BOOST_CHECK_EQUAL(d, exact);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_SelfInnerProduct_diagonal_test )
{
  typedef VectorS<4,int> VectorS4;
  VectorS4 a = {1,2,3,4};
  VectorS4 b = {2,3,4,5};

  int d = InnerProductDiag(a,b);

  int exact = 0;
  for (int i = 0; i < VectorS4::M; i++)
    exact += a[i]*b[i]*a[i];

  BOOST_CHECK_EQUAL(d, exact);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_WeightedInnerProduct_MatrixS_test )
{
  typedef VectorS<4,int> VectorS4;
  VectorS4 a = {1,2,3,4};
  VectorS4 b = {2,3,4,5};

  typedef MatrixS<4,4,int> MatrixS4;
  MatrixS4 M = {{ 5,  4,  3,  2},
                { 9,  8,  7,  6},
                {-4, -3, -2, -1},
                {-9, -7, -5, -3}};

  int d = InnerProduct(a,b,M);

  int exact = 0;
  for (int i = 0; i < VectorS4::M; i++)
    for (int j = 0; j < VectorS4::M; j++)
      exact += a(i)*M(i,j)*b(j);

  BOOST_CHECK_EQUAL(d,exact);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_SelfWeightedInnerProduct_MatrixS_test )
{
  typedef VectorS<4,int> VectorS4;
  VectorS4 a = {1,2,3,4};

  typedef MatrixS<4,4,int> MatrixS4;
  MatrixS4 M = {{ 5,  4,  3,  2},
                { 9,  8,  7,  6},
                {-4, -3, -2, -1},
                {-9, -7, -5, -3}};

  int d = InnerProduct(a,M);

  int exact = 0;
  for (int i = 0; i < VectorS4::M; i++)
    for (int j = 0; j < VectorS4::M; j++)
      exact += a(i)*M(i,j)*a(j);

  BOOST_CHECK_EQUAL(d,exact);

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_WeightedInnerProduct_MatrixSymS_test )
{
  typedef VectorS<4,int> VectorS4;
  VectorS4 a = {1,2,3,4};
  VectorS4 b = {2,3,4,5};

  typedef MatrixSymS<4,int> MatrixSymS4;
  MatrixSymS4 M = {{ 5},
                   { 9,  8},
                   {-4, -3, -2},
                   {-9, -7, -5, -3}};

  int d = InnerProduct(a,b,M);

  int exact = 0;
  for (int i = 0; i < VectorS4::M; i++)
    for (int j = 0; j < VectorS4::M; j++)
      exact += a(i)*M(i,j)*b(j);

  BOOST_CHECK_EQUAL(d,exact);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_SelfWeightedInnerProduct_MatrixSymS_test )
{
  typedef VectorS<4,int> VectorS4;
  VectorS4 a = {1,2,3,4};

  typedef MatrixSymS<4,int> MatrixSymS4;
  MatrixSymS4 M = {{ 5},
                   { 9,  8},
                   {-4, -3, -2},
                   {-9, -7, -5, -3}};

  int d = InnerProduct(a,M);

  int exact = 0;
  for (int i = 0; i < VectorS4::M; i++)
    for (int j = 0; j < VectorS4::M; j++)
      exact += a(i)*M(i,j)*a(j);

  BOOST_CHECK_EQUAL(d,exact);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
