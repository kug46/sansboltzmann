// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_NonZeroPattern.h"

#include <iostream>


// Explicitly instantiate to create coverage information
namespace SANS
{
namespace DLA
{
template class DenseNonZeroPattern<Real>;
}
}

using namespace SANS::DLA;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_NonZeroPattern )
{
  typedef Real T;

  DenseNonZeroPattern<T> nz(3,2);

  BOOST_CHECK_EQUAL( 3, nz.m() );
  BOOST_CHECK_EQUAL( 2, nz.n() );

  nz.add(0,0);
  BOOST_CHECK_THROW( nz.add(4,4), AssertionException );

  BOOST_CHECK_EQUAL( 3*2, nz.nnz() );
  BOOST_CHECK_EQUAL(   2, nz.rowSize(0) );

  int rowMap[] = {0,1};
  int colMap[] = {1,0};

  // Nothing to check here...
  nz.scatterAdd(rowMap,2);
  nz.scatterAdd(rowMap,2,colMap,2);

  MatrixD<T> A(nz);

  BOOST_CHECK_EQUAL( 3, A.m() );
  BOOST_CHECK_EQUAL( 2, A.n() );

  BOOST_CHECK_EQUAL( 1, nz(0,0) );
  BOOST_CHECK_THROW( nz(4,4), AssertionException );

  MatrixD< DenseNonZeroPattern<T> > nz2 = {{{2,2},{2,3}},
                                           {{3,2},{3,3}}};

  MatrixD< MatrixD<T> > B(nz2);

  BOOST_CHECK_EQUAL( 2, B(0,0).m() );
  BOOST_CHECK_EQUAL( 2, B(0,0).n() );

  BOOST_CHECK_EQUAL( 2, B(0,1).m() );
  BOOST_CHECK_EQUAL( 3, B(0,1).n() );

  BOOST_CHECK_EQUAL( 3, B(1,0).m() );
  BOOST_CHECK_EQUAL( 2, B(1,0).n() );

  BOOST_CHECK_EQUAL( 3, B(1,1).m() );
  BOOST_CHECK_EQUAL( 3, B(1,1).n() );


  DenseNonZeroPattern<T> nz3(0,0);
  nz3 = nz;

  BOOST_CHECK_EQUAL( 3, nz3.m() );
  BOOST_CHECK_EQUAL( 2, nz3.n() );


  DenseNonZeroPattern<T> nz4 = {3,2};

  BOOST_CHECK_EQUAL( 3, nz4.m() );
  BOOST_CHECK_EQUAL( 2, nz4.n() );


  DenseNonZeroPattern<T> nz5(nz);

  BOOST_CHECK_EQUAL( 3, nz5.m() );
  BOOST_CHECK_EQUAL( 2, nz5.n() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_NonZeroPattern_MatrixSize )
{
  typedef Real T;

  DenseMatrixSize ms(3,2);

  DenseNonZeroPattern<T> nz(ms);

  MatrixD<T> A(nz);

  BOOST_CHECK_EQUAL( 3, A.m() );
  BOOST_CHECK_EQUAL( 2, A.n() );

  MatrixD< DenseMatrixSize > ms2 = {{{2,2},{2,3}},
                                    {{3,2},{3,3}}};

  BOOST_REQUIRE_EQUAL( 2, ms2.m() );
  BOOST_REQUIRE_EQUAL( 2, ms2.n() );

  MatrixD< DenseNonZeroPattern<T> > nz2(ms2);

  BOOST_REQUIRE_EQUAL( 2, nz2.m() );
  BOOST_REQUIRE_EQUAL( 2, nz2.n() );

  BOOST_CHECK_EQUAL( 2, nz2(0,0).m() );
  BOOST_CHECK_EQUAL( 2, nz2(0,0).n() );

  BOOST_CHECK_EQUAL( 2, nz2(0,1).m() );
  BOOST_CHECK_EQUAL( 3, nz2(0,1).n() );

  BOOST_CHECK_EQUAL( 3, nz2(1,0).m() );
  BOOST_CHECK_EQUAL( 2, nz2(1,0).n() );

  BOOST_CHECK_EQUAL( 3, nz2(1,1).m() );
  BOOST_CHECK_EQUAL( 3, nz2(1,1).n() );

  MatrixD< MatrixD<T> > B(nz2);

  BOOST_REQUIRE_EQUAL( 2, B.m() );
  BOOST_REQUIRE_EQUAL( 2, B.n() );

#ifndef __clang_analyzer__ // thinks we are accessing zero size memory
  BOOST_CHECK_EQUAL( 2, B(0,0).m() );
  BOOST_CHECK_EQUAL( 2, B(0,0).n() );

  BOOST_CHECK_EQUAL( 2, B(0,1).m() );
  BOOST_CHECK_EQUAL( 3, B(0,1).n() );

  BOOST_CHECK_EQUAL( 3, B(1,0).m() );
  BOOST_CHECK_EQUAL( 2, B(1,0).n() );

  BOOST_CHECK_EQUAL( 3, B(1,1).m() );
  BOOST_CHECK_EQUAL( 3, B(1,1).n() );
#endif
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_NonZeroPattern_Transpose )
{
  typedef Real T;

  DenseNonZeroPattern<T> nz(3,2);

  DenseNonZeroPattern<T> nzT = Transpose(nz);

  BOOST_CHECK_EQUAL( 3, nz.m() );
  BOOST_CHECK_EQUAL( 2, nz.n() );

  BOOST_CHECK_EQUAL( 2, nzT.m() );
  BOOST_CHECK_EQUAL( 3, nzT.n() );

  MatrixD< DenseNonZeroPattern<T> > nz2 = {{{2,2},{2,3}},
                                           {{4,2},{4,3}}};

  MatrixD< DenseNonZeroPattern<T> > nz2T = Transpose(nz2);

  BOOST_CHECK_EQUAL( 2, nz2(0,0).m() );
  BOOST_CHECK_EQUAL( 2, nz2(0,0).n() );

  BOOST_CHECK_EQUAL( 2, nz2(0,1).m() );
  BOOST_CHECK_EQUAL( 3, nz2(0,1).n() );

  BOOST_CHECK_EQUAL( 4, nz2(1,0).m() );
  BOOST_CHECK_EQUAL( 2, nz2(1,0).n() );

  BOOST_CHECK_EQUAL( 4, nz2(1,1).m() );
  BOOST_CHECK_EQUAL( 3, nz2(1,1).n() );


  BOOST_CHECK_EQUAL( 2, nz2T(0,0).m() );
  BOOST_CHECK_EQUAL( 2, nz2T(0,0).n() );

  BOOST_CHECK_EQUAL( 2, nz2T(0,1).m() );
  BOOST_CHECK_EQUAL( 4, nz2T(0,1).n() );

  BOOST_CHECK_EQUAL( 3, nz2T(1,0).m() );
  BOOST_CHECK_EQUAL( 2, nz2T(1,0).n() );

  BOOST_CHECK_EQUAL( 3, nz2T(1,1).m() );
  BOOST_CHECK_EQUAL( 4, nz2T(1,1).n() );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_MatrixSize )
{
  typedef Real T;

  DenseMatrixSize size(3,2);

  BOOST_CHECK_EQUAL( 3, size.m() );
  BOOST_CHECK_EQUAL( 2, size.n() );

  MatrixD<T> A(size);

  BOOST_CHECK_EQUAL( 3, A.m() );
  BOOST_CHECK_EQUAL( 2, A.n() );

  MatrixD< DenseMatrixSize > size2 = {{{2,2},{2,3}},
                                     {{3,2},{3,3}}};

  MatrixD< MatrixD<T> > B(size2);

  BOOST_CHECK_EQUAL( 2, B(0,0).m() );
  BOOST_CHECK_EQUAL( 2, B(0,0).n() );

  BOOST_CHECK_EQUAL( 2, B(0,1).m() );
  BOOST_CHECK_EQUAL( 3, B(0,1).n() );

  BOOST_CHECK_EQUAL( 3, B(1,0).m() );
  BOOST_CHECK_EQUAL( 2, B(1,0).n() );

  BOOST_CHECK_EQUAL( 3, B(1,1).m() );
  BOOST_CHECK_EQUAL( 3, B(1,1).n() );


  DenseMatrixSize size3(0,0);
  size3 = size;

  BOOST_CHECK_EQUAL( 3, size3.m() );
  BOOST_CHECK_EQUAL( 2, size3.n() );


  DenseMatrixSize size4 = {3,2};

  BOOST_CHECK_EQUAL( 3, size4.m() );
  BOOST_CHECK_EQUAL( 2, size4.n() );


  DenseMatrixSize size5(size);

  BOOST_CHECK_EQUAL( 3, size5.m() );
  BOOST_CHECK_EQUAL( 2, size5.n() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
