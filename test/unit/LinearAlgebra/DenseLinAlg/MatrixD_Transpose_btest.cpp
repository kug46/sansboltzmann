// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include <iostream>

namespace SANS
{
namespace DLA
{
  //Explicitly instantiate the class so that coverage information is correct
  template class MatrixDTranspose<Real>;
  template class MatrixDTranspose<MatrixS<3,2,Real>>;
}
}

using namespace SANS::DLA;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Transpose_Exceptions )
{
  typedef Real T;
  Real Adata[] = {1, 2,
                  3, 4};

  MatrixD<T> A(2,2, Adata);
  MatrixD<T> AT1(3,2), AT2(2,3);

  BOOST_CHECK_THROW( AT1 = Transpose(A), AssertionException );
  BOOST_CHECK_THROW( AT2 = Transpose(A), AssertionException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Transpose_2x2_test )
{
  typedef Real T;
  Real Adata[] = {1, 2,
                  3, 4};

  MatrixD<T> A(2,2, Adata);
  MatrixD<T> AT(2,2);

  AT = Transpose(A);

  BOOST_CHECK_EQUAL( AT(0,0) , 1 );
  BOOST_CHECK_EQUAL( AT(0,1) , 3 );
  BOOST_CHECK_EQUAL( AT(1,0) , 2 );
  BOOST_CHECK_EQUAL( AT(1,1) , 4 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Transpose_3x3_test )
{
  typedef Real T;
  Real Adata[] = {1, 2, 3,
                  4, 5, 6,
                  7, 8, 9};

  MatrixD<T> A(3,3, Adata);
  MatrixD<T> AT(3,3);

  AT = Transpose(A);

  BOOST_CHECK_EQUAL( AT(0,0) , 1 );
  BOOST_CHECK_EQUAL( AT(1,0) , 2 );
  BOOST_CHECK_EQUAL( AT(2,0) , 3 );
  BOOST_CHECK_EQUAL( AT(0,1) , 4 );
  BOOST_CHECK_EQUAL( AT(1,1) , 5 );
  BOOST_CHECK_EQUAL( AT(2,1) , 6 );
  BOOST_CHECK_EQUAL( AT(0,2) , 7 );
  BOOST_CHECK_EQUAL( AT(1,2) , 8 );
  BOOST_CHECK_EQUAL( AT(2,2) , 9 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Transpose_2x3_test )
{
  typedef Real T;
  Real Adata[] = {1, 2, 3,
                  4, 5, 6};

  MatrixD<T> A(2,3, Adata);
  MatrixD<T> AT(3,2);

  BOOST_CHECK_EQUAL( Transpose(A).size()  , 2*3 );
  BOOST_CHECK_EQUAL( Transpose(A).m()     , 3   );
  BOOST_CHECK_EQUAL( Transpose(A).n()     , 2   );
  BOOST_CHECK_EQUAL( Transpose(A).stride(), 3   );


  AT = Transpose(A);

  BOOST_CHECK_EQUAL( AT(0,0) , 1 );
  BOOST_CHECK_EQUAL( AT(1,0) , 2 );
  BOOST_CHECK_EQUAL( AT(2,0) , 3 );
  BOOST_CHECK_EQUAL( AT(0,1) , 4 );
  BOOST_CHECK_EQUAL( AT(1,1) , 5 );
  BOOST_CHECK_EQUAL( AT(2,1) , 6 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Transpose_2x2_add_test )
{
  typedef Real T;
  Real Adata[] = {1, 2,
                  3, 4};

  Real ATdata[] = {1, 2,
                   3, 4};

  MatrixD<T> A(2,2, Adata);
  MatrixD<T> AT(2,2, ATdata);

  AT += Transpose(A);

  BOOST_CHECK_EQUAL( AT(0,0) , 2 );
  BOOST_CHECK_EQUAL( AT(0,1) , 5 );
  BOOST_CHECK_EQUAL( AT(1,0) , 5 );
  BOOST_CHECK_EQUAL( AT(1,1) , 8 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Transpose_2x2_Sym_test )
{
  typedef Real T;
  Real Adata[] = {1, 2,
                  3, 4};

  MatrixD<T> A(2,2, Adata);
  MatrixD<T> Sym(2,2);

  Sym = Transpose(A)*A;

  BOOST_CHECK_EQUAL( Sym(0,0) , 10 );
  BOOST_CHECK_EQUAL( Sym(0,1) , 14 );
  BOOST_CHECK_EQUAL( Sym(1,0) , 14 );
  BOOST_CHECK_EQUAL( Sym(1,1) , 20 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Transpose_xTMx_test )
{
  typedef Real T;
  Real Mdata[] = {1, 2,
                  3, 4};

  Real xdata[] = {3, 4};

  MatrixD<T> M(2,2, Mdata);
  MatrixD<T> x(2,1, xdata);
  T xTMx;

  xTMx = Transpose(x)*M*x;

  BOOST_CHECK_EQUAL( xTMx, 133 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Transpose_xxT_2x2_test )
{
  typedef Real T;

  MatrixD<T> x = {{3},
                  {4}};
  MatrixD<T> xxT(2,2);

  xxT = 0.0;
  xxT = x*Transpose(x);

  BOOST_CHECK_EQUAL( xxT(0,0),  9 );
  BOOST_CHECK_EQUAL( xxT(0,1), 12 );
  BOOST_CHECK_EQUAL( xxT(1,0), 12 );
  BOOST_CHECK_EQUAL( xxT(1,1), 16 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Transpose_xxT_3x3_test )
{
  typedef Real T;

  MatrixD<T> x = {{3},
                  {4},
                  {5}};
  MatrixD<T> xxT(3,3);

  xxT = 0.0;
  xxT = x*Transpose(x);

  BOOST_CHECK_EQUAL( xxT(0,0),  9 );
  BOOST_CHECK_EQUAL( xxT(0,1), 12 );
  BOOST_CHECK_EQUAL( xxT(0,2), 15 );

  BOOST_CHECK_EQUAL( xxT(1,0), 12 );
  BOOST_CHECK_EQUAL( xxT(1,1), 16 );
  BOOST_CHECK_EQUAL( xxT(1,2), 20 );

  BOOST_CHECK_EQUAL( xxT(2,0), 15 );
  BOOST_CHECK_EQUAL( xxT(2,1), 20 );
  BOOST_CHECK_EQUAL( xxT(2,2), 25 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Transpose_2x3_MatrixS32_test )
{
  typedef MatrixS<3,2,Real> T;
  typedef MatrixS<2,3,Real> TT;

  //A is a 2x3 matrix of MatrixDs
  MatrixD<T> A = { { {{ 1, 2},{ 3, 4},{ 5, 6}}, {{ 7, 8},{ 9,10},{11,12}}, {{13,14},{15,16},{17,18}} },
                   { {{19,20},{21,22},{23,24}}, {{25,26},{27,28},{29,30}}, {{31,32},{33,34},{35,36}} } };

  MatrixD<TT> AT(3,2);

  AT = Transpose(A);

  BOOST_CHECK_EQUAL( Transpose(A).size()  , 2*3 );
  BOOST_CHECK_EQUAL( Transpose(A).m()     , 3   );
  BOOST_CHECK_EQUAL( Transpose(A).n()     , 2   );
  BOOST_CHECK_EQUAL( Transpose(A).stride(), 3   );

  BOOST_CHECK_EQUAL( AT(0,0)(0,0), 1 ); BOOST_CHECK_EQUAL( AT(0,0)(0,1), 3 ); BOOST_CHECK_EQUAL( AT(0,0)(0,2), 5 );
  BOOST_CHECK_EQUAL( AT(0,0)(1,0), 2 ); BOOST_CHECK_EQUAL( AT(0,0)(1,1), 4 ); BOOST_CHECK_EQUAL( AT(0,0)(1,2), 6 );

  BOOST_CHECK_EQUAL( AT(0,1)(0,0), 19 ); BOOST_CHECK_EQUAL( AT(0,1)(0,1), 21 ); BOOST_CHECK_EQUAL( AT(0,1)(0,2), 23 );
  BOOST_CHECK_EQUAL( AT(0,1)(1,0), 20 ); BOOST_CHECK_EQUAL( AT(0,1)(1,1), 22 ); BOOST_CHECK_EQUAL( AT(0,1)(1,2), 24 );

  BOOST_CHECK_EQUAL( AT(1,0)(0,0),  7 ); BOOST_CHECK_EQUAL( AT(1,0)(0,1),  9 ); BOOST_CHECK_EQUAL( AT(1,0)(0,2), 11 );
  BOOST_CHECK_EQUAL( AT(1,0)(1,0),  8 ); BOOST_CHECK_EQUAL( AT(1,0)(1,1), 10 ); BOOST_CHECK_EQUAL( AT(1,0)(1,2), 12 );

  BOOST_CHECK_EQUAL( AT(1,1)(0,0), 25 ); BOOST_CHECK_EQUAL( AT(1,1)(0,1), 27 ); BOOST_CHECK_EQUAL( AT(1,1)(0,2), 29 );
  BOOST_CHECK_EQUAL( AT(1,1)(1,0), 26 ); BOOST_CHECK_EQUAL( AT(1,1)(1,1), 28 ); BOOST_CHECK_EQUAL( AT(1,1)(1,2), 30 );

  BOOST_CHECK_EQUAL( AT(2,0)(0,0), 13 ); BOOST_CHECK_EQUAL( AT(2,0)(0,1), 15 ); BOOST_CHECK_EQUAL( AT(2,0)(0,2), 17 );
  BOOST_CHECK_EQUAL( AT(2,0)(1,0), 14 ); BOOST_CHECK_EQUAL( AT(2,0)(1,1), 16 ); BOOST_CHECK_EQUAL( AT(2,0)(1,2), 18 );

  BOOST_CHECK_EQUAL( AT(2,1)(0,0), 31 ); BOOST_CHECK_EQUAL( AT(2,1)(0,1), 33 ); BOOST_CHECK_EQUAL( AT(2,1)(0,2), 35 );
  BOOST_CHECK_EQUAL( AT(2,1)(1,0), 32 ); BOOST_CHECK_EQUAL( AT(2,1)(1,1), 34 ); BOOST_CHECK_EQUAL( AT(2,1)(1,2), 36 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Transpose_2x3_Vector2MatrixS23_test )
{
  typedef VectorS<2, MatrixS<2,3,Real>> T;
  typedef MatrixS<1, 2, MatrixS<3,2,Real>> TT;

  T A00 = { {{ 1,  2,  3},
             { 4,  5,  6}},
            {{ 7,  8,  9},
             {10, 11, 12}} };

  T A01 = { {{13, 14, 15},
             {16, 17, 18}},
            {{19, 20, 21},
             {22, 23, 24}} };

  T A02 = { {{25, 26, 27},
             {28, 29, 30}},
            {{31, 32, 33},
             {34, 35, 36}} };

  T A10 = { {{37, 38, 39},
             {40, 41, 42}},
            {{43, 44, 45},
             {46, 47, 48}} };

  T A11 = { {{49, 50, 51},
             {52, 53, 54}},
            {{55, 56, 57},
             {58, 59, 60}} };

  T A12 = { {{61, 62, 63},
             {64, 65, 66}},
            {{67, 68, 69},
             {70, 71, 72}} };

  //A is a 2x3 matrix of MatrixDs
  MatrixD<T> A = { {A00, A01, A02},
                   {A10, A11, A12} };

  MatrixD<TT> AT(3,2);

  AT = Transpose(A);

  BOOST_CHECK_EQUAL( Transpose(A).size()  , 2*3 );
  BOOST_CHECK_EQUAL( Transpose(A).m()     , 3   );
  BOOST_CHECK_EQUAL( Transpose(A).n()     , 2   );
  BOOST_CHECK_EQUAL( Transpose(A).stride(), 3   );

  int i = 0, j = 0;
  BOOST_CHECK_EQUAL( AT(j,i)(0,0)(0,0), 1 ); BOOST_CHECK_EQUAL( AT(j,i)(0,0)(0,1), 4 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,0)(1,0), 2 ); BOOST_CHECK_EQUAL( AT(j,i)(0,0)(1,1), 5 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,0)(2,0), 3 ); BOOST_CHECK_EQUAL( AT(j,i)(0,0)(2,1), 6 );

  BOOST_CHECK_EQUAL( AT(j,i)(0,1)(0,0), 7 ); BOOST_CHECK_EQUAL( AT(j,i)(0,1)(0,1), 10 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,1)(1,0), 8 ); BOOST_CHECK_EQUAL( AT(j,i)(0,1)(1,1), 11 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,1)(2,0), 9 ); BOOST_CHECK_EQUAL( AT(j,i)(0,1)(2,1), 12 );

  i = 0; j = 1;
  BOOST_CHECK_EQUAL( AT(j,i)(0,0)(0,0), 13 ); BOOST_CHECK_EQUAL( AT(j,i)(0,0)(0,1), 16 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,0)(1,0), 14 ); BOOST_CHECK_EQUAL( AT(j,i)(0,0)(1,1), 17 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,0)(2,0), 15 ); BOOST_CHECK_EQUAL( AT(j,i)(0,0)(2,1), 18 );

  BOOST_CHECK_EQUAL( AT(j,i)(0,1)(0,0), 19 ); BOOST_CHECK_EQUAL( AT(j,i)(0,1)(0,1), 22 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,1)(1,0), 20 ); BOOST_CHECK_EQUAL( AT(j,i)(0,1)(1,1), 23 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,1)(2,0), 21 ); BOOST_CHECK_EQUAL( AT(j,i)(0,1)(2,1), 24 );

  i = 0; j = 2;
  BOOST_CHECK_EQUAL( AT(j,i)(0,0)(0,0), 25 ); BOOST_CHECK_EQUAL( AT(j,i)(0,0)(0,1), 28 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,0)(1,0), 26 ); BOOST_CHECK_EQUAL( AT(j,i)(0,0)(1,1), 29 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,0)(2,0), 27 ); BOOST_CHECK_EQUAL( AT(j,i)(0,0)(2,1), 30 );

  BOOST_CHECK_EQUAL( AT(j,i)(0,1)(0,0), 31 ); BOOST_CHECK_EQUAL( AT(j,i)(0,1)(0,1), 34 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,1)(1,0), 32 ); BOOST_CHECK_EQUAL( AT(j,i)(0,1)(1,1), 35 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,1)(2,0), 33 ); BOOST_CHECK_EQUAL( AT(j,i)(0,1)(2,1), 36 );

  i = 1; j = 0;
  BOOST_CHECK_EQUAL( AT(j,i)(0,0)(0,0), 37 ); BOOST_CHECK_EQUAL( AT(j,i)(0,0)(0,1), 40 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,0)(1,0), 38 ); BOOST_CHECK_EQUAL( AT(j,i)(0,0)(1,1), 41 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,0)(2,0), 39 ); BOOST_CHECK_EQUAL( AT(j,i)(0,0)(2,1), 42 );

  BOOST_CHECK_EQUAL( AT(j,i)(0,1)(0,0), 43 ); BOOST_CHECK_EQUAL( AT(j,i)(0,1)(0,1), 46 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,1)(1,0), 44 ); BOOST_CHECK_EQUAL( AT(j,i)(0,1)(1,1), 47 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,1)(2,0), 45 ); BOOST_CHECK_EQUAL( AT(j,i)(0,1)(2,1), 48 );

  i = 1; j = 1;
  BOOST_CHECK_EQUAL( AT(j,i)(0,0)(0,0), 49 ); BOOST_CHECK_EQUAL( AT(j,i)(0,0)(0,1), 52 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,0)(1,0), 50 ); BOOST_CHECK_EQUAL( AT(j,i)(0,0)(1,1), 53 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,0)(2,0), 51 ); BOOST_CHECK_EQUAL( AT(j,i)(0,0)(2,1), 54 );

  BOOST_CHECK_EQUAL( AT(j,i)(0,1)(0,0), 55 ); BOOST_CHECK_EQUAL( AT(j,i)(0,1)(0,1), 58 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,1)(1,0), 56 ); BOOST_CHECK_EQUAL( AT(j,i)(0,1)(1,1), 59 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,1)(2,0), 57 ); BOOST_CHECK_EQUAL( AT(j,i)(0,1)(2,1), 60 );

  i = 1; j = 2;
  BOOST_CHECK_EQUAL( AT(j,i)(0,0)(0,0), 61 ); BOOST_CHECK_EQUAL( AT(j,i)(0,0)(0,1), 64 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,0)(1,0), 62 ); BOOST_CHECK_EQUAL( AT(j,i)(0,0)(1,1), 65 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,0)(2,0), 63 ); BOOST_CHECK_EQUAL( AT(j,i)(0,0)(2,1), 66 );

  BOOST_CHECK_EQUAL( AT(j,i)(0,1)(0,0), 67 ); BOOST_CHECK_EQUAL( AT(j,i)(0,1)(0,1), 70 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,1)(1,0), 68 ); BOOST_CHECK_EQUAL( AT(j,i)(0,1)(1,1), 71 );
  BOOST_CHECK_EQUAL( AT(j,i)(0,1)(2,0), 69 ); BOOST_CHECK_EQUAL( AT(j,i)(0,1)(2,1), 72 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Transpose_2x3_Vector2MatrixS23_MatMulVec_3x1_test )
{
  typedef VectorS<2, MatrixS<2,3,Real>> T;
  typedef VectorS<2, VectorS<2,Real>> Tvec;

  T A00 = { {{ 1,  2,  3},
             { 4,  5,  6}},
            {{ 7,  8,  9},
             {10, 11, 12}} };

  T A01 = { {{13, 14, 15},
             {16, 17, 18}},
            {{19, 20, 21},
             {22, 23, 24}} };

  T A02 = { {{25, 26, 27},
             {28, 29, 30}},
            {{31, 32, 33},
             {34, 35, 36}} };

  T A10 = { {{37, 38, 39},
             {40, 41, 42}},
            {{43, 44, 45},
             {46, 47, 48}} };

  T A11 = { {{49, 50, 51},
             {52, 53, 54}},
            {{55, 56, 57},
             {58, 59, 60}} };

  T A12 = { {{61, 62, 63},
             {64, 65, 66}},
            {{67, 68, 69},
             {70, 71, 72}} };

  //A is a 2x3 matrix of MatrixDs
  MatrixD<T> A = { {A00, A01, A02},
                   {A10, A11, A12} };

  Tvec b0 = {{1, 2},
             {3, 4}};
  Tvec b1 = {{5, 6},
             {7, 8}};
  VectorD<Tvec> b = {b0, b1};

  VectorD< VectorS<1, VectorS<3,Real>> > x(3);
  x = Transpose(A)*b;

  BOOST_CHECK_EQUAL( x(0)(0)(0), 1164 );
  BOOST_CHECK_EQUAL( x(0)(0)(1), 1200 );
  BOOST_CHECK_EQUAL( x(0)(0)(2), 1236 );

  BOOST_CHECK_EQUAL( x(1)(0)(0), 1596 );
  BOOST_CHECK_EQUAL( x(1)(0)(1), 1632 );
  BOOST_CHECK_EQUAL( x(1)(0)(2), 1668 );

  BOOST_CHECK_EQUAL( x(2)(0)(0), 2028 );
  BOOST_CHECK_EQUAL( x(2)(0)(1), 2064 );
  BOOST_CHECK_EQUAL( x(2)(0)(2), 2100 );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
