// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Diag.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_SVD.h"

#include <iostream>
using namespace SANS::DLA;

namespace SANS
{
namespace DLA
{

}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SVD_1x1_test )
{
  typedef Real T;

  const Real tol = 1e-12;
  MatrixS<1,1,T> A = {0.367};
  MatrixS<1,1,T> U = -1;
  VectorS<1,T> S = -1;
  MatrixS<1,1,T> VT = -1;

  SVD(A,U,S,VT);

  BOOST_CHECK_CLOSE(1.0, U(0,0), tol);

  BOOST_CHECK_CLOSE(0.367, S[0], tol);

  BOOST_CHECK_CLOSE(1.0, VT(0,0), tol);

  MatrixS<1,1,T> B = U*diag(S)*VT;

  BOOST_CHECK_CLOSE(A(0,0), B(0,0), tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_SVD_1x1_test )
{
  typedef Real T;

  const Real tol = 1e-12;
  MatrixSymS<1,T> A = {0.367};
  MatrixS<1,1,T> U = -1;
  VectorS<1,T> S = -1;
  MatrixS<1,1,T> VT = -1;

  SVD(A,U,S,VT);

  BOOST_CHECK_CLOSE(1.0, U(0,0), tol);

  BOOST_CHECK_CLOSE(0.367, S[0], tol);

  BOOST_CHECK_CLOSE(1.0, VT(0,0), tol);

  MatrixS<1,1,T> B = U*diag(S)*VT;

  BOOST_CHECK_CLOSE(A(0,0), B(0,0), tol);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SVD_2x2_test )
{
  typedef Real T;

  const Real tol = 1e-12;

  {
    MatrixS<2,2,T> A = {{1,2},
                        {2,1}};
    MatrixS<2,2,T> U = -1;
    VectorS<2,T> S = -1;
    MatrixS<2,2,T> VT = -1;

    SVD(A,U,S,VT);

    BOOST_CHECK_CLOSE( 1./sqrt(2.), U(0,0), tol);
    BOOST_CHECK_CLOSE( 1./sqrt(2.), U(0,1), tol);
    BOOST_CHECK_CLOSE( 1./sqrt(2.), U(1,0), tol);
    BOOST_CHECK_CLOSE(-1./sqrt(2.), U(1,1), tol);

    BOOST_CHECK_CLOSE( 3.0, S[0], tol);
    BOOST_CHECK_CLOSE( 1.0, S[1], tol);

    BOOST_CHECK_CLOSE( 1./sqrt(2.), VT(0,0), tol);
    BOOST_CHECK_CLOSE( 1./sqrt(2.), VT(0,1), tol);
    BOOST_CHECK_CLOSE(-1./sqrt(2.), VT(1,0), tol);
    BOOST_CHECK_CLOSE( 1./sqrt(2.), VT(1,1), tol);

    MatrixS<2,2,T> B = U*diag(S)*VT;

    BOOST_CHECK_CLOSE(A(0,0), B(0,0), tol);
    BOOST_CHECK_CLOSE(A(0,1), B(0,1), tol);
    BOOST_CHECK_CLOSE(A(1,0), B(1,0), tol);
    BOOST_CHECK_CLOSE(A(1,1), B(1,1), tol);
  }

  {
    MatrixS<2,2,T> A = {{1,2},
                        {1,2}};
    MatrixS<2,2,T> U = -1;
    VectorS<2,T> S = -1;
    MatrixS<2,2,T> VT = -1;

    SVD(A,U,S,VT);

    BOOST_CHECK_CLOSE( 1./sqrt(2.),U(0,0),tol);
    BOOST_CHECK_CLOSE( 1./sqrt(2.),U(0,1),tol);
    BOOST_CHECK_CLOSE( 1./sqrt(2.),U(1,0),tol);
    BOOST_CHECK_CLOSE(-1./sqrt(2.),U(1,1),tol);

    BOOST_CHECK_CLOSE(   sqrt(10.),S[0],tol);
    BOOST_CHECK_SMALL(             S[1],tol);

    BOOST_CHECK_CLOSE( 1./sqrt(5.),VT(0,0),tol);
    BOOST_CHECK_CLOSE( 2./sqrt(5.),VT(0,1),tol);
    BOOST_CHECK_CLOSE( 2./sqrt(5.),VT(1,0),tol);
    BOOST_CHECK_CLOSE(-1./sqrt(5.),VT(1,1),tol);

    MatrixS<2,2,T> B = U*diag(S)*VT;

    BOOST_CHECK_CLOSE(A(0,0), B(0,0), tol);
    BOOST_CHECK_CLOSE(A(0,1), B(0,1), tol);
    BOOST_CHECK_CLOSE(A(1,0), B(1,0), tol);
    BOOST_CHECK_CLOSE(A(1,1), B(1,1), tol);
  }

  {
    MatrixS<2,2,T> A = {{5,7},
                        {5,1}};
    MatrixS<2,2,T> U = -1;
    VectorS<2,T> S = -1;
    MatrixS<2,2,T> VT = -1;

    SVD(A,U,S,VT);

    BOOST_CHECK_CLOSE( 2./sqrt(5.),U(0,0),tol);
    BOOST_CHECK_CLOSE( 1./sqrt(5.),U(0,1),tol);
    BOOST_CHECK_CLOSE( 1./sqrt(5.),U(1,0),tol);
    BOOST_CHECK_CLOSE(-2./sqrt(5.),U(1,1),tol);

    BOOST_CHECK_CLOSE( 3.*sqrt(10.), S[0], tol);
    BOOST_CHECK_CLOSE(    sqrt(10.), S[1], tol);

    BOOST_CHECK_CLOSE( 1./sqrt(2.),VT(0,0),tol);
    BOOST_CHECK_CLOSE( 1./sqrt(2.),VT(0,1),tol);
    BOOST_CHECK_CLOSE(-1./sqrt(2.),VT(1,0),tol);
    BOOST_CHECK_CLOSE( 1./sqrt(2.),VT(1,1),tol);

    MatrixS<2,2,T> B = U*diag(S)*VT;

    BOOST_CHECK_CLOSE(A(0,0), B(0,0), tol);
    BOOST_CHECK_CLOSE(A(0,1), B(0,1), tol);
    BOOST_CHECK_CLOSE(A(1,0), B(1,0), tol);
    BOOST_CHECK_CLOSE(A(1,1), B(1,1), tol);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_SVD_2x2_test )
{
  typedef Real T;

  const Real tol = 1e-12;

  {
    MatrixSymS<2,T> A = {{1},
                         {2,1}};
    MatrixS<2,2,T> U = -1;
    VectorS<2,T> S = -1;
    MatrixS<2,2,T> VT = -1;

    SVD(A,U,S,VT);

    BOOST_CHECK_CLOSE( 1./sqrt(2.), U(0,0), tol);
    BOOST_CHECK_CLOSE( 1./sqrt(2.), U(0,1), tol);
    BOOST_CHECK_CLOSE( 1./sqrt(2.), U(1,0), tol);
    BOOST_CHECK_CLOSE(-1./sqrt(2.), U(1,1), tol);

    BOOST_CHECK_CLOSE( 3.0, S[0], tol);
    BOOST_CHECK_CLOSE( 1.0, S[1], tol);

    BOOST_CHECK_CLOSE( 1./sqrt(2.), VT(0,0), tol);
    BOOST_CHECK_CLOSE( 1./sqrt(2.), VT(0,1), tol);
    BOOST_CHECK_CLOSE(-1./sqrt(2.), VT(1,0), tol);
    BOOST_CHECK_CLOSE( 1./sqrt(2.), VT(1,1), tol);

    MatrixS<2,2,T> B = U*diag(S)*VT;

    BOOST_CHECK_CLOSE(A(0,0), B(0,0), tol);
    BOOST_CHECK_CLOSE(A(0,1), B(0,1), tol);
    BOOST_CHECK_CLOSE(A(1,0), B(1,0), tol);
    BOOST_CHECK_CLOSE(A(1,1), B(1,1), tol);
  }

  {
    MatrixS<2,2,T> A = {{3,7},
                        {7,-5}};
    MatrixS<2,2,T> U = -1;
    VectorS<2,T> S = -1;
    MatrixS<2,2,T> VT = -1;

    SVD(A,U,S,VT);

    BOOST_CHECK_CLOSE(  0.501926818193233, U(0,0), tol);
    BOOST_CHECK_CLOSE(  0.864910093118595, U(0,1), tol);
    BOOST_CHECK_CLOSE( -0.864910093118595, U(1,0), tol);
    BOOST_CHECK_CLOSE(  0.501926818193233, U(1,1), tol);

    BOOST_CHECK_CLOSE( 9.062257748298551, S[0], tol);
    BOOST_CHECK_CLOSE( 7.062257748298549, S[1], tol);

    BOOST_CHECK_CLOSE( -0.501926818193233, VT(0,0), tol);
    BOOST_CHECK_CLOSE(  0.864910093118595, VT(0,1), tol);
    BOOST_CHECK_CLOSE(  0.864910093118595, VT(1,0), tol);
    BOOST_CHECK_CLOSE(  0.501926818193233, VT(1,1), tol);

    MatrixS<2,2,T> B = U*diag(S)*VT;

    BOOST_CHECK_CLOSE(A(0,0), B(0,0), tol);
    BOOST_CHECK_CLOSE(A(0,1), B(0,1), tol);
    BOOST_CHECK_CLOSE(A(1,0), B(1,0), tol);
    BOOST_CHECK_CLOSE(A(1,1), B(1,1), tol);
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SVD_3x3_test )
{
  typedef Real T;

  const Real tol = 1e-12;

  {
    MatrixS<3,3,T> A = {{1,2,3},
                        {1,2,3},
                        {1,2,3}};
    MatrixS<3,3,T> U = -1;
    VectorS<3,T> S = -1;
    MatrixS<3,3,T> VT = -1;

    SVD(A,U,S,VT);

    BOOST_CHECK_CLOSE( 1./sqrt(3.), U(0,0),tol);
    BOOST_CHECK_CLOSE(-1./sqrt(2.), U(0,1),tol);
    BOOST_CHECK_CLOSE(-1./sqrt(6.), U(0,2),tol);

    BOOST_CHECK_CLOSE( 1./sqrt(3.), U(1,0),tol);
    BOOST_CHECK_CLOSE( 1./sqrt(2.), U(1,1),tol);
    BOOST_CHECK_CLOSE(-1./sqrt(6.), U(1,2),tol);

    BOOST_CHECK_CLOSE( 1./sqrt(3.), U(2,0),tol);
    BOOST_CHECK_SMALL(              U(2,1),tol);
    BOOST_CHECK_CLOSE( sqrt(2./3.), U(2,2),tol);


    BOOST_CHECK_CLOSE( sqrt(42.0),S[0],tol);
    BOOST_CHECK_SMALL(            S[1],tol);
    BOOST_CHECK_SMALL(            S[2],tol);

    MatrixS<3,3,T> B = U*diag(S)*VT;

    SANS_CHECK_CLOSE(A(0,0), B(0,0), tol, tol);
    SANS_CHECK_CLOSE(A(0,1), B(0,1), tol, tol);
    SANS_CHECK_CLOSE(A(0,2), B(0,2), tol, tol);

    SANS_CHECK_CLOSE(A(1,0), B(1,0), tol, tol);
    SANS_CHECK_CLOSE(A(1,1), B(1,1), tol, tol);
    SANS_CHECK_CLOSE(A(1,2), B(1,2), tol, tol);

    SANS_CHECK_CLOSE(A(2,0), B(2,0), tol, tol);
    SANS_CHECK_CLOSE(A(2,1), B(2,1), tol, tol);
    SANS_CHECK_CLOSE(A(2,2), B(2,2), tol, tol);
  }

  {
    MatrixS<3,3,T> A = {{ 1,0, 5},
                        { 7,2,-3},
                        {-1,4, 6}};
    MatrixS<3,3,T> U = -1;
    VectorS<3,T> S = -1;
    MatrixS<3,3,T> VT = -1;

    SVD(A,U,S,VT);

    BOOST_CHECK_CLOSE( 0.408257970646589, U(0,0), tol);
    BOOST_CHECK_CLOSE(-0.369952647085662, U(0,1), tol);
    BOOST_CHECK_CLOSE( 0.834542071029280, U(0,2), tol);
    BOOST_CHECK_CLOSE(-0.614332411218716, U(1,0), tol);
    BOOST_CHECK_CLOSE(-0.787549834591373, U(1,1), tol);
    BOOST_CHECK_CLOSE(-0.048589572557274, U(1,2), tol);
    BOOST_CHECK_CLOSE( 0.675219310986976, U(2,0), tol);
    BOOST_CHECK_CLOSE(-0.492849162472061, U(2,1), tol);
    BOOST_CHECK_CLOSE(-0.548797398974213, U(2,2), tol);

    BOOST_CHECK_CLOSE( 9.273689951305961, S[0], tol);
    BOOST_CHECK_CLOSE( 6.899520159164776, S[1], tol);
    BOOST_CHECK_CLOSE( 2.719429399768585, S[2], tol);

    BOOST_CHECK_CLOSE(-0.492499559814184, VT(0,0), tol);
    BOOST_CHECK_CLOSE( 0.158751524931362, VT(0,1), tol);
    BOOST_CHECK_CLOSE( 0.855711479948004, VT(0,2), tol);
    BOOST_CHECK_CLOSE(-0.781206837926783, VT(1,0), tol);
    BOOST_CHECK_CLOSE(-0.514020719884426, VT(1,1), tol);
    BOOST_CHECK_CLOSE(-0.354257781715424, VT(1,2), tol);
    BOOST_CHECK_CLOSE( 0.383614467870115, VT(2,0), tol);
    BOOST_CHECK_CLOSE(-0.842959460983423, VT(2,1), tol);
    BOOST_CHECK_CLOSE( 0.377172755086134, VT(2,2), tol);

    MatrixS<3,3,T> B = U*diag(S)*VT;

    SANS_CHECK_CLOSE(A(0,0), B(0,0), tol, tol);
    SANS_CHECK_CLOSE(A(0,1), B(0,1), tol, tol);
    SANS_CHECK_CLOSE(A(0,2), B(0,2), tol, tol);

    SANS_CHECK_CLOSE(A(1,0), B(1,0), tol, tol);
    SANS_CHECK_CLOSE(A(1,1), B(1,1), tol, tol);
    SANS_CHECK_CLOSE(A(1,2), B(1,2), tol, tol);

    SANS_CHECK_CLOSE(A(2,0), B(2,0), tol, tol);
    SANS_CHECK_CLOSE(A(2,1), B(2,1), tol, tol);
    SANS_CHECK_CLOSE(A(2,2), B(2,2), tol, tol);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_SVD_3x3_test )
{
  typedef Real T;

  const Real tol = 1e-12;
  MatrixSymS<3,T> A = {{1},
                       {-2,3},
                       {-1,4,5}};
  MatrixS<3,3,T> U = -1;
  VectorS<3,T> S = -1;
  MatrixS<3,3,T> VT = -1;

  SVD(A,U,S,VT);

  BOOST_CHECK_CLOSE(-0.258004444638195, U(0,0), tol);
  BOOST_CHECK_CLOSE( 0.776501881371954, U(0,1), tol);
  BOOST_CHECK_CLOSE( 0.574872624824624, U(0,2), tol);
  BOOST_CHECK_CLOSE( 0.616567158200162, U(1,0), tol);
  BOOST_CHECK_CLOSE(-0.325767273302780, U(1,1), tol);
  BOOST_CHECK_CLOSE( 0.716743066289342, U(1,2), tol);
  BOOST_CHECK_CLOSE( 0.743827026919507, U(2,0), tol);
  BOOST_CHECK_CLOSE( 0.539370477381445, U(2,1), tol);
  BOOST_CHECK_CLOSE(-0.394716153904802, U(2,2), tol);

  BOOST_CHECK_CLOSE( 8.662508861396413, S[0], tol);
  BOOST_CHECK_CLOSE( 1.144447904010149, S[1], tol);
  BOOST_CHECK_CLOSE( 0.806956765406561, S[2], tol);

  BOOST_CHECK_CLOSE(-0.258004444638195, VT(0,0), tol);
  BOOST_CHECK_CLOSE( 0.616567158200162, VT(0,1), tol);
  BOOST_CHECK_CLOSE( 0.743827026919507, VT(0,2), tol);
  BOOST_CHECK_CLOSE( 0.776501881371953, VT(1,0), tol);
  BOOST_CHECK_CLOSE(-0.325767273302782, VT(1,1), tol);
  BOOST_CHECK_CLOSE( 0.539370477381446, VT(1,2), tol);
  BOOST_CHECK_CLOSE(-0.574872624824625, VT(2,0), tol);
  BOOST_CHECK_CLOSE(-0.716743066289341, VT(2,1), tol);
  BOOST_CHECK_CLOSE( 0.394716153904801, VT(2,2), tol);

  MatrixS<3,3,T> B = U*diag(S)*VT;

  BOOST_CHECK_CLOSE(A(0,0), B(0,0), tol);
  BOOST_CHECK_CLOSE(A(0,1), B(0,1), tol);
  BOOST_CHECK_CLOSE(A(0,2), B(0,2), tol);

  BOOST_CHECK_CLOSE(A(1,0), B(1,0), tol);
  BOOST_CHECK_CLOSE(A(1,1), B(1,1), tol);
  BOOST_CHECK_CLOSE(A(1,2), B(1,2), tol);

  BOOST_CHECK_CLOSE(A(2,0), B(2,0), tol);
  BOOST_CHECK_CLOSE(A(2,1), B(2,1), tol);
  BOOST_CHECK_CLOSE(A(2,2), B(2,2), tol);
}

#ifdef DLA_LAPACK

////----------------------------------------------------------------------------//
//BOOST_AUTO_TEST_CASE( MatrixS_SVD_LAPACK_3x3_test )
//{
//  typedef Real T;
//
//  const Real tol = 1e-12;
//  MatrixS<3,3,T> A = {{1,2,3},
//                      {1,2,3},
//                      {1,2,3}};
//  MatrixS<3,3,T> U = -1;
//  VectorS<3,T> S = -1;
//  MatrixS<3,3,T> VT = -1;
//
//  SVD(A,U,S,VT);
//
//#if 0
//  // From Mathematica (with some sign changes and permutations...)
//  BOOST_CHECK_CLOSE(-1./sqrt(3.),U(0,0),tol);
//  BOOST_CHECK_CLOSE( sqrt(2./3.),U(0,1),tol);
//  BOOST_CHECK_SMALL(             U(0,2),tol);
//
//  BOOST_CHECK_CLOSE(-1./sqrt(3.),U(1,0),tol);
//  BOOST_CHECK_CLOSE(-1./sqrt(6.),U(1,1),tol);
//  BOOST_CHECK_CLOSE(-1./sqrt(2.),U(1,2),tol);
//
//  BOOST_CHECK_CLOSE(-1./sqrt(3.),U(2,0),tol);
//  BOOST_CHECK_CLOSE(-1./sqrt(6.),U(2,1),tol);
//  BOOST_CHECK_CLOSE( 1./sqrt(2.),U(2,2),tol);
//
//
//  BOOST_CHECK_CLOSE(   sqrt(42.),S[0],tol);
//  BOOST_CHECK_SMALL(             S[1],tol);
//  BOOST_CHECK_SMALL(             S[2],tol);
//
//
//  BOOST_CHECK_CLOSE(-1./sqrt(14.),VT(0,0),tol);
//  BOOST_CHECK_CLOSE( -sqrt(2./7.),VT(0,1),tol);
//  BOOST_CHECK_CLOSE(-3./sqrt(14.),VT(0,2),tol);
//
//  BOOST_CHECK_CLOSE( 3./sqrt(10.),VT(1,0),tol);
//  BOOST_CHECK_SMALL(              VT(1,1),tol);
//  BOOST_CHECK_CLOSE( 1./sqrt(10.),VT(1,2),tol);
//
//  BOOST_CHECK_CLOSE( -sqrt(5./7.),VT(2,0),tol);
//  BOOST_CHECK_CLOSE( 1./sqrt(10.),VT(2,1),tol);
//  BOOST_CHECK_CLOSE(-3./sqrt(35.),VT(2,2),tol);
//#endif
//
//  MatrixS<3,3,T> B = U*diag(S)*VT;
//
//  BOOST_CHECK_CLOSE(A(0,0), B(0,0), tol);
//  BOOST_CHECK_CLOSE(A(0,1), B(0,1), tol);
//  BOOST_CHECK_CLOSE(A(0,2), B(0,2), tol);
//
//  BOOST_CHECK_CLOSE(A(1,0), B(1,0), tol);
//  BOOST_CHECK_CLOSE(A(1,1), B(1,1), tol);
//  BOOST_CHECK_CLOSE(A(1,2), B(1,2), tol);
//
//  BOOST_CHECK_CLOSE(A(2,0), B(2,0), tol);
//  BOOST_CHECK_CLOSE(A(2,1), B(2,1), tol);
//  BOOST_CHECK_CLOSE(A(2,2), B(2,2), tol);
//
//}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SVD_LAPACK_3x2_test )
{
  typedef Real T;

  const Real tol = 1e-12;
  MatrixS<3,2,T> A = {{1,2},
                      {1,2},
                      {1,2}};
  MatrixS<3,3,T> U = -1;
  VectorS<2,T> S = -1;
  MatrixS<2,2,T> VT = -1;

  SVD(A,U,S,VT);

  MatrixS<3,2,T> Sig = 0;
  Sig(0,0) = S[0];
  Sig(1,1) = S[1];

  MatrixS<3,2,T> B = U*Sig*VT;

  BOOST_CHECK_CLOSE(A(0,0), B(0,0), tol);
  BOOST_CHECK_CLOSE(A(0,1), B(0,1), tol);

  BOOST_CHECK_CLOSE(A(1,0), B(1,0), tol);
  BOOST_CHECK_CLOSE(A(1,1), B(1,1), tol);

  BOOST_CHECK_CLOSE(A(2,0), B(2,0), tol);
  BOOST_CHECK_CLOSE(A(2,1), B(2,1), tol);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SVD_LAPACK_2x3_test )
{
  typedef Real T;

  const Real tol = 1e-12;
  MatrixS<2,3,T> A = {{1,2,3},
                      {1,2,3}};
  MatrixS<2,2,T> U = -1;
  VectorS<2,T> S = -1;
  MatrixS<3,3,T> VT = -1;

  SVD(A,U,S,VT);

  MatrixS<2,3,T> Sig = 0;
  Sig(0,0) = S[0];
  Sig(1,1) = S[1];

  MatrixS<2,3,T> B = U*Sig*VT;

  BOOST_CHECK_CLOSE(A(0,0), B(0,0), tol);
  BOOST_CHECK_CLOSE(A(0,1), B(0,1), tol);
  BOOST_CHECK_CLOSE(A(0,2), B(0,2), tol);

  BOOST_CHECK_CLOSE(A(1,0), B(1,0), tol);
  BOOST_CHECK_CLOSE(A(1,1), B(1,1), tol);
  BOOST_CHECK_CLOSE(A(1,2), B(1,2), tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SVD_LAPACK_3x4_test )
{
  typedef Real T;

  const Real tol = 1e-12;
  MatrixS<3,4,T> A = {{1,2,3,4},
                      {1,2,3,4},
                      {1,2,3,4}};
  MatrixS<3,3,T> U = -1;
  VectorS<3,T> S = -1;
  MatrixS<4,4,T> VT = -1;

  SVD(A,U,S,VT);

  MatrixS<3,4,T> Sfull(0);
  Sfull(0,0) = S(0);
  Sfull(1,1) = S(1);
  Sfull(2,2) = S(2);

  MatrixS<3,4,T> B = U*Sfull*VT;
  BOOST_CHECK_CLOSE(A(0,0), B(0,0), tol);
  BOOST_CHECK_CLOSE(A(0,1), B(0,1), tol);
  BOOST_CHECK_CLOSE(A(0,2), B(0,2), tol);
  BOOST_CHECK_CLOSE(A(0,3), B(0,3), tol);

  BOOST_CHECK_CLOSE(A(1,0), B(1,0), tol);
  BOOST_CHECK_CLOSE(A(1,1), B(1,1), tol);
  BOOST_CHECK_CLOSE(A(1,2), B(1,2), tol);
  BOOST_CHECK_CLOSE(A(1,3), B(1,3), tol);

  BOOST_CHECK_CLOSE(A(2,0), B(2,0), tol);
  BOOST_CHECK_CLOSE(A(2,1), B(2,1), tol);
  BOOST_CHECK_CLOSE(A(2,2), B(2,2), tol);
  BOOST_CHECK_CLOSE(A(2,3), B(2,3), tol);
}

#endif //DLA_LAPACK

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
