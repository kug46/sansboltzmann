// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/range_c.hpp>
#include <boost/preprocessor/repetition/repeat_from_to.hpp>

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Decompose_LUP.h"
#include "LinearAlgebra/DenseLinAlg/tools/SingularException.h"

#include <iostream>
using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Decompose_LUP_Exceptions )
{
  MatrixS<2, 2, Real> A1, LU1;
  VectorS<2, int> P1;
  A1 = 0;
  BOOST_CHECK_THROW( Decompose_LUP(A1, LU1, P1), SingularMatrixException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Decompose_LUP_Pivot_5x5_I )
{
  typedef Real T;

  MatrixS<5, 5, Real> Ainv, LU;
  VectorS<5, int> P;

  MatrixS<5, 5, Real> A = {{0, 0, 0, 0, 5},
                           {0, 0, 0, 5, 0},
                           {0, 0, 5, 0, 0},
                           {0, 5, 0, 0, 0},
                           {5, 0, 0, 0, 0}};

  Decompose_LUP(A, LU, P);

  BOOST_CHECK_EQUAL( P[0], 4 );
  BOOST_CHECK_EQUAL( P[1], 3 );
  BOOST_CHECK_EQUAL( P[2], 2 );
  BOOST_CHECK_EQUAL( P[3], 3 );
  BOOST_CHECK_EQUAL( P[4], 0 );

  for (int i = 0; i < 5; ++i)
    for (int j = 0; j < 5; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( LU(i,j), T(5), T(1e-10) );
      else
        BOOST_CHECK_SMALL( LU(i,j), T(1e-12) );
    }

  LU = 0; P = 0;

  (LU, P) = Decompose_LUP(A);

  BOOST_CHECK_EQUAL( P[0], 4 );
  BOOST_CHECK_EQUAL( P[1], 3 );
  BOOST_CHECK_EQUAL( P[2], 2 );
  BOOST_CHECK_EQUAL( P[3], 3 );
  BOOST_CHECK_EQUAL( P[4], 0 );

  for (int i = 0; i < 5; ++i)
    for (int j = 0; j < 5; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( LU(i,j), T(5), T(0.0001) );
      else
        BOOST_CHECK_SMALL( LU(i,j), T(1e-12) );
    }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Decompose_LUP_Pivot_5x5 )
{
  typedef Real T;

  MatrixS<5, 5, Real> LU;
  VectorS<5, int> P;

  MatrixS<5, 5, Real> A = {{5, 0, 0, 0, 0},
                           {0, 0, 5, 0, 0},
                           {0, 5, 0, 0, 0},
                           {0, 0, 0, 0, 5},
                           {0, 0, 0, 5, 0}};

  Decompose_LUP(A, LU, P);

  BOOST_CHECK_EQUAL( P[0], 0 );
  BOOST_CHECK_EQUAL( P[1], 2 );
  BOOST_CHECK_EQUAL( P[2], 2 );
  BOOST_CHECK_EQUAL( P[3], 4 );
  BOOST_CHECK_EQUAL( P[4], 0 );

  for (int i = 0; i < 5; ++i)
    for (int j = 0; j < 5; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( LU(i,j) , T(5), T(0.0001) );
      else
        BOOST_CHECK_SMALL( LU(i,j) , T(1e-12) );
    }

  LU = 0; P = 0;

  (LU, P) = Decompose_LUP(A);

  BOOST_CHECK_EQUAL( P[0], 0 );
  BOOST_CHECK_EQUAL( P[1], 2 );
  BOOST_CHECK_EQUAL( P[2], 2 );
  BOOST_CHECK_EQUAL( P[3], 4 );
  BOOST_CHECK_EQUAL( P[4], 0 );

  for (int i = 0; i < 5; ++i)
    for (int j = 0; j < 5; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( LU(i,j) , T(5), T(0.0001) );
      else
        BOOST_CHECK_SMALL( LU(i,j) , T(1e-12) );
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Decompose_LUP_Pivot_7x7 )
{
  typedef Real T;

  MatrixS<7, 7, Real> LU;
  VectorS<7, int> P;

  MatrixS<7, 7, Real> A = {{5, 0, 0, 0, 0, 0, 0},
                           {0, 0, 5, 0, 0, 0, 0},
                           {0, 0, 0, 5, 0, 0, 0},
                           {0, 0, 0, 0, 0, 0, 5},
                           {0, 5, 0, 0, 0, 0, 0},
                           {0, 0, 0, 0, 0, 5, 0},
                           {0, 0, 0, 0, 5, 0, 0}};

  Decompose_LUP(A, LU, P);

  BOOST_CHECK_EQUAL( P[0], 0 );
  BOOST_CHECK_EQUAL( P[1], 4 );
  BOOST_CHECK_EQUAL( P[2], 4 );
  BOOST_CHECK_EQUAL( P[3], 4 );
  BOOST_CHECK_EQUAL( P[4], 6 );
  BOOST_CHECK_EQUAL( P[5], 5 );
  BOOST_CHECK_EQUAL( P[6], 0 );

  for (int i = 0; i < 7; ++i)
    for (int j = 0; j < 7; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( LU(i,j) , T(5), T(0.0001) );
      else
        BOOST_CHECK_SMALL( LU(i,j) , T(1e-12) );
    }

  LU = 0; P = 0;

  (LU, P) = Decompose_LUP(A);

  BOOST_CHECK_EQUAL( P[0], 0 );
  BOOST_CHECK_EQUAL( P[1], 4 );
  BOOST_CHECK_EQUAL( P[2], 4 );
  BOOST_CHECK_EQUAL( P[3], 4 );
  BOOST_CHECK_EQUAL( P[4], 6 );
  BOOST_CHECK_EQUAL( P[5], 5 );
  BOOST_CHECK_EQUAL( P[6], 0 );

  for (int i = 0; i < 5; ++i)
    for (int j = 0; j < 5; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( LU(i,j) , T(5), T(0.0001) );
      else
        BOOST_CHECK_SMALL( LU(i,j) , T(1e-12) );
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Decompose_LUP_1x1 )
{
  typedef Real T;
  MatrixS<1, 1, Real> A;
  MatrixS<1, 1, Real> LU;
  VectorS<1, int> P;

  A = 2;

  Decompose_LUP(A, LU, P);

  BOOST_CHECK_CLOSE( A(0,0), T(2.), T(0.0001) );

  BOOST_CHECK_CLOSE( LU(0,0), T(2.), T(0.0001) );

  BOOST_CHECK_EQUAL( P[0], 0 );


  LU = 0; P = 0;

  (LU, P) = Decompose_LUP(A);

  BOOST_CHECK_CLOSE( A(0,0), T(2.), T(0.0001) );

  BOOST_CHECK_CLOSE( LU(0,0), T(2.), T(0.0001) );

  BOOST_CHECK_EQUAL( P[0], 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Decompose_LUP_2x2 )
{
  typedef Real T;

  MatrixS<2, 2, Real> A = {{2, 1},
                           {3, 2}};
  MatrixS<2, 2, Real> LU;
  VectorS<2, int> P;

  Decompose_LUP(A, LU, P);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( LU(0,0),     3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1),  2./3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0),     2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), -1./3., T(0.0001) );

  BOOST_CHECK_EQUAL( P[0], 1 );
  BOOST_CHECK_EQUAL( P[1], 0 );


  LU = 0; P = 0;

  (LU, P) = Decompose_LUP(A);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( LU(0,0),     3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1),  2./3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0),     2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), -1./3., T(0.0001) );

  BOOST_CHECK_EQUAL( P[0], 1 );
  BOOST_CHECK_EQUAL( P[1], 0 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Decompose_LUP_2x2_AExpression )
{
  typedef Real T;

  MatrixS<2, 2, Real> A1 = {{1, 0},
                            {2, 1}};
  MatrixS<2, 2, Real> LU, A2;
  VectorS<2, int> P;

  A2 = 1;

  Decompose_LUP(A1 + A2, LU, P);

  BOOST_CHECK_CLOSE( LU(0,0),     3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1),  2./3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0),     2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), -1./3., T(0.0001) );

  BOOST_CHECK_EQUAL( P[0], 1 );
  BOOST_CHECK_EQUAL( P[1], 0 );


  LU = 0; P = 0;

  (LU, P) = Decompose_LUP(A1 + A2);

  BOOST_CHECK_CLOSE( LU(0,0),     3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1),  2./3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0),     2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), -1./3., T(0.0001) );

  BOOST_CHECK_EQUAL( P[0], 1 );
  BOOST_CHECK_EQUAL( P[1], 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Decompose_LUP_3x3 )
{
  typedef Real T;

  MatrixS<3, 3, Real> A = {{1, 2, 3},
                           {2, 4, 5},
                           {3, 5, 6}};

  MatrixS<3, 3, Real> LU;
  VectorS<3, int> P;

  Decompose_LUP(A, LU, P);

  BOOST_CHECK_CLOSE( LU(0,0), T( 3.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1), T( 5./3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,2), T( 2.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0), T( 2.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), T( 2./3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,2), T( 3./2. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,0), T( 1.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,1), T( 1./3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,2), T( 1./2. ), T(0.0001) );


  (LU, P) = Decompose_LUP(A);

  BOOST_CHECK_CLOSE( LU(0,0), T( 3.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1), T( 5./3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,2), T( 2.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0), T( 2.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), T( 2./3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,2), T( 3./2. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,0), T( 1.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,1), T( 1./3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,2), T( 1./2. ), T(0.0001) );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
