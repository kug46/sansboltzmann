// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/InverseLUP.h"
#include "chkMatrixD_btest.h"

#include <iostream>
using namespace SANS::DLA;

namespace SANS
{
namespace DLA
{
//Explicitly instantiate the class to generate all the functions so that coverage information is correct
template class MatrixDTuple< MatrixDView<Real> >;

typedef OpAddD< MatrixDView<Real>, MatrixDView<Real>, true > MatAdd;
template class MatrixDExpressionTuple< MatAdd , MatrixDView<Real> >;
}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Tuple_Assign_1 )
{
  const int n = 1;
  MatrixD<Real> ML(n,n), MR(n,1), M(n,n+1);

  (ML, MR) = 1;

  BOOST_CHECK_EQUAL( 1, ML(0,0) );
  BOOST_CHECK_EQUAL( 1, MR(0,0) );

  (ML, MR) *= 2;

  BOOST_CHECK_EQUAL( 2, ML(0,0) );
  BOOST_CHECK_EQUAL( 2, MR(0,0) );

  (ML, MR)(0,0) = 3;
  (ML, MR)(0,1) = 1;

  BOOST_CHECK_EQUAL( 3, ML(0,0) );
  BOOST_CHECK_EQUAL( 1, MR(0,0) );

  M = 5;

  (ML, MR) = M;

  BOOST_CHECK_EQUAL( 5, ML(0,0) );
  BOOST_CHECK_EQUAL( 5, MR(0,0) );
  BOOST_CHECK_EQUAL( 5,  M(0,0) );
  BOOST_CHECK_EQUAL( 5,  M(0,1) );

  (ML, MR) += M;

  BOOST_CHECK_EQUAL( 2*5, ML(0,0) );
  BOOST_CHECK_EQUAL( 2*5, MR(0,0) );
  BOOST_CHECK_EQUAL(   5,  M(0,0) );
  BOOST_CHECK_EQUAL(   5,  M(0,1) );

  (ML, MR) -= M;

  BOOST_CHECK_EQUAL( 5, ML(0,0) );
  BOOST_CHECK_EQUAL( 5, MR(0,0) );
  BOOST_CHECK_EQUAL( 5,  M(0,0) );
  BOOST_CHECK_EQUAL( 5,  M(0,1) );

  ML = 2;
  MR = 3;

  M = (ML, MR);

  BOOST_CHECK_EQUAL( 2, ML(0,0) );
  BOOST_CHECK_EQUAL( 3, MR(0,0) );
  BOOST_CHECK_EQUAL( 2,  M(0,0) );
  BOOST_CHECK_EQUAL( 3,  M(0,1) );

  M += (ML, MR);

  BOOST_CHECK_EQUAL(   2, ML(0,0) );
  BOOST_CHECK_EQUAL(   3, MR(0,0) );
  BOOST_CHECK_EQUAL( 2*2,  M(0,0) );
  BOOST_CHECK_EQUAL( 2*3,  M(0,1) );

  M -= (ML, MR);

  BOOST_CHECK_EQUAL( 2, ML(0,0) );
  BOOST_CHECK_EQUAL( 3, MR(0,0) );
  BOOST_CHECK_EQUAL( 2,  M(0,0) );
  BOOST_CHECK_EQUAL( 3,  M(0,1) );

  MatrixD<Real> M1(2,1), M2(2,1);

  (M1, M2) = Identity();
  BOOST_CHECK_EQUAL( 1, M1(0,0) );
  BOOST_CHECK_EQUAL( 0, M1(1,0) );
  BOOST_CHECK_EQUAL( 0, M2(0,0) );
  BOOST_CHECK_EQUAL( 1, M2(1,0) );

  MatrixD<Real> M3 = (ML, MR);

  BOOST_CHECK_EQUAL( 2, ML(0,0) );
  BOOST_CHECK_EQUAL( 3, MR(0,0) );
  BOOST_CHECK_EQUAL( 2, M3(0,0) );
  BOOST_CHECK_EQUAL( 3, M3(0,1) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Tuple_Assign_2 )
{
  const int n = 1;
  MatrixD<Real> M1(n,n), M2(n,1), M3(n,2), M(n,n+3);

  (M1, M2, M3) = 1;

  BOOST_CHECK_EQUAL( 1, M1(0,0) );
  BOOST_CHECK_EQUAL( 1, M2(0,0) );
  BOOST_CHECK_EQUAL( 1, M3(0,0) );

  M = 5;

  (M1, M2, M3) = M;

  BOOST_CHECK_EQUAL( 5, M1(0,0) );
  BOOST_CHECK_EQUAL( 5, M2(0,0) );
  BOOST_CHECK_EQUAL( 5, M3(0,0) );
  BOOST_CHECK_EQUAL( 5,  M(0,0) );
  BOOST_CHECK_EQUAL( 5,  M(0,1) );
  BOOST_CHECK_EQUAL( 5,  M(0,2) );

  (M1, M2, M3) += M;

  BOOST_CHECK_EQUAL( 2*5, M1(0,0) );
  BOOST_CHECK_EQUAL( 2*5, M2(0,0) );
  BOOST_CHECK_EQUAL( 2*5, M3(0,0) );
  BOOST_CHECK_EQUAL(   5,  M(0,0) );
  BOOST_CHECK_EQUAL(   5,  M(0,1) );
  BOOST_CHECK_EQUAL(   5,  M(0,2) );

  (M1, M2, M3) -= M;

  BOOST_CHECK_EQUAL( 5, M1(0,0) );
  BOOST_CHECK_EQUAL( 5, M2(0,0) );
  BOOST_CHECK_EQUAL( 5, M3(0,0) );
  BOOST_CHECK_EQUAL( 5,  M(0,0) );
  BOOST_CHECK_EQUAL( 5,  M(0,1) );
  BOOST_CHECK_EQUAL( 5,  M(0,2) );

  M1 = 2;
  M2 = 3;
  M3 = 4;

  M = (M1, M2, M3);

  BOOST_CHECK_EQUAL( 2, M1(0,0) );
  BOOST_CHECK_EQUAL( 3, M2(0,0) );
  BOOST_CHECK_EQUAL( 4, M3(0,0) );
  BOOST_CHECK_EQUAL( 2,  M(0,0) );
  BOOST_CHECK_EQUAL( 3,  M(0,1) );
  BOOST_CHECK_EQUAL( 4,  M(0,2) );

  M += (M1, M2, M3);

  BOOST_CHECK_EQUAL(   2, M1(0,0) );
  BOOST_CHECK_EQUAL(   3, M2(0,0) );
  BOOST_CHECK_EQUAL(   4, M3(0,0) );
  BOOST_CHECK_EQUAL( 2*2,  M(0,0) );
  BOOST_CHECK_EQUAL( 2*3,  M(0,1) );
  BOOST_CHECK_EQUAL( 2*4,  M(0,2) );

  M -= (M1, M2, M3);

  BOOST_CHECK_EQUAL( 2, M1(0,0) );
  BOOST_CHECK_EQUAL( 3, M2(0,0) );
  BOOST_CHECK_EQUAL( 4, M3(0,0) );
  BOOST_CHECK_EQUAL( 2,  M(0,0) );
  BOOST_CHECK_EQUAL( 3,  M(0,1) );
  BOOST_CHECK_EQUAL( 4,  M(0,2) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Tuple_Addition )
{
  MatrixD<Real> A(1,3), B(1,3), M1(1,1), M2(1,2);

  A = 1;
  B = 2;

  (M1, M2) = A + B;

  BOOST_CHECK_EQUAL( 3, M1(0,0) );
  BOOST_CHECK_EQUAL( 3, M2(0,0) );
  BOOST_CHECK_EQUAL( 3, M2(0,1) );

  (M1, M2) += A + B;

  BOOST_CHECK_EQUAL( 6, M1(0,0) );
  BOOST_CHECK_EQUAL( 6, M2(0,0) );
  BOOST_CHECK_EQUAL( 6, M2(0,1) );

  (M1, M2) -= A + B;

  BOOST_CHECK_EQUAL( 3, M1(0,0) );
  BOOST_CHECK_EQUAL( 3, M2(0,0) );
  BOOST_CHECK_EQUAL( 3, M2(0,1) );

  (M1, M2) = A - B;

  BOOST_CHECK_EQUAL( -1, M1(0,0) );
  BOOST_CHECK_EQUAL( -1, M2(0,0) );
  BOOST_CHECK_EQUAL( -1, M2(0,1) );

  (M1, M2) += A - B;

  BOOST_CHECK_EQUAL( -2, M1(0,0) );
  BOOST_CHECK_EQUAL( -2, M2(0,0) );
  BOOST_CHECK_EQUAL( -2, M2(0,1) );

  (M1, M2) -= A - B;

  BOOST_CHECK_EQUAL( -1, M1(0,0) );
  BOOST_CHECK_EQUAL( -1, M2(0,0) );
  BOOST_CHECK_EQUAL( -1, M2(0,1) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Tuple_Expression )
{
  MatrixD<Real> M(1,3), M1(1,1), M2(1,2), M3(1,1), L1(1,1), L2(1,1), R1(1,2), R2(1,2);

  L1 = 2;
  L2 = 3;
  R1 = 4;
  R2 = 5;


  M = (L1, R1 + R2);

  BOOST_CHECK_EQUAL( 2, M(0,0) );
  BOOST_CHECK_EQUAL( 9, M(0,1) );
  BOOST_CHECK_EQUAL( 9, M(0,2) );

  M += (L1, R1 + R2);

  BOOST_CHECK_EQUAL( 2*2, M(0,0) );
  BOOST_CHECK_EQUAL( 2*9, M(0,1) );
  BOOST_CHECK_EQUAL( 2*9, M(0,2) );

  M -= (L1, R1 + R2);

  BOOST_CHECK_EQUAL( 2, M(0,0) );
  BOOST_CHECK_EQUAL( 9, M(0,1) );
  BOOST_CHECK_EQUAL( 9, M(0,2) );


  M = (L1 + L2, R1);

  BOOST_CHECK_EQUAL( 5, M(0,0) );
  BOOST_CHECK_EQUAL( 4, M(0,1) );
  BOOST_CHECK_EQUAL( 4, M(0,2) );

  M += (L1 + L2, R1);

  BOOST_CHECK_EQUAL( 2*5, M(0,0) );
  BOOST_CHECK_EQUAL( 2*4, M(0,1) );
  BOOST_CHECK_EQUAL( 2*4, M(0,2) );

  M -= (L1 + L2, R1);

  BOOST_CHECK_EQUAL( 5, M(0,0) );
  BOOST_CHECK_EQUAL( 4, M(0,1) );
  BOOST_CHECK_EQUAL( 4, M(0,2) );


  M = (L1 + L2, R1 + R2);

  BOOST_CHECK_EQUAL( 5, M(0,0) );
  BOOST_CHECK_EQUAL( 9, M(0,1) );
  BOOST_CHECK_EQUAL( 9, M(0,2) );

  M += (L1 + L2, R1 + R2);

  BOOST_CHECK_EQUAL( 2*5, M(0,0) );
  BOOST_CHECK_EQUAL( 2*9, M(0,1) );
  BOOST_CHECK_EQUAL( 2*9, M(0,2) );

  M -= (L1 + L2, R1 + R2);

  BOOST_CHECK_EQUAL( 5, M(0,0) );
  BOOST_CHECK_EQUAL( 9, M(0,1) );
  BOOST_CHECK_EQUAL( 9, M(0,2) );


  MatrixD<Real> A = (L1 + L2, R1 + R2);

  BOOST_CHECK_EQUAL( 5, A(0,0) );
  BOOST_CHECK_EQUAL( 9, A(0,1) );
  BOOST_CHECK_EQUAL( 9, A(0,2) );
  BOOST_CHECK_EQUAL( 1, A.m() );
  BOOST_CHECK_EQUAL( 3, A.n() );




  (M1, M2) = (L1, R1 + R2);

  BOOST_CHECK_EQUAL( 2, M1(0,0) );
  BOOST_CHECK_EQUAL( 9, M2(0,0) );
  BOOST_CHECK_EQUAL( 9, M2(0,1) );

  (M1, M2) += (L1, R1 + R2);

  BOOST_CHECK_EQUAL( 2*2, M1(0,0) );
  BOOST_CHECK_EQUAL( 2*9, M2(0,0) );
  BOOST_CHECK_EQUAL( 2*9, M2(0,1) );

  (M1, M2) -= (L1, R1 + R2);

  BOOST_CHECK_EQUAL( 2, M1(0,0) );
  BOOST_CHECK_EQUAL( 9, M2(0,0) );
  BOOST_CHECK_EQUAL( 9, M2(0,1) );

  (M1, M2) = (L1 + L2, R1);

  BOOST_CHECK_EQUAL( 5, M1(0,0) );
  BOOST_CHECK_EQUAL( 4, M2(0,0) );
  BOOST_CHECK_EQUAL( 4, M2(0,1) );

  (M1, M2) += (L1 + L2, R1);

  BOOST_CHECK_EQUAL( 2*5, M1(0,0) );
  BOOST_CHECK_EQUAL( 2*4, M2(0,0) );
  BOOST_CHECK_EQUAL( 2*4, M2(0,1) );

  (M1, M2) -= (L1 + L2, R1);

  BOOST_CHECK_EQUAL( 5, M1(0,0) );
  BOOST_CHECK_EQUAL( 4, M2(0,0) );
  BOOST_CHECK_EQUAL( 4, M2(0,1) );

  (M1, M2) = (L1 + L2, R1 + R2);

  BOOST_CHECK_EQUAL( 5, M1(0,0) );
  BOOST_CHECK_EQUAL( 9, M2(0,0) );
  BOOST_CHECK_EQUAL( 9, M2(0,1) );

  (M1, M2) += (L1 + L2, R1 + R2);

  BOOST_CHECK_EQUAL( 2*5, M1(0,0) );
  BOOST_CHECK_EQUAL( 2*9, M2(0,0) );
  BOOST_CHECK_EQUAL( 2*9, M2(0,1) );

  (M1, M2) -= (L1 + L2, R1 + R2);

  BOOST_CHECK_EQUAL( 5, M1(0,0) );
  BOOST_CHECK_EQUAL( 9, M2(0,0) );
  BOOST_CHECK_EQUAL( 9, M2(0,1) );


  (M1, M2, M3) = (L1 + L2, R1, L1 + L2);

  BOOST_CHECK_EQUAL( 5, M1(0,0) );
  BOOST_CHECK_EQUAL( 4, M2(0,0) );
  BOOST_CHECK_EQUAL( 4, M2(0,1) );
  BOOST_CHECK_EQUAL( 5, M3(0,0) );

  (M1, M2, M3) = (L1, R1 + R2, L1 + L2);

  BOOST_CHECK_EQUAL( 2, M1(0,0) );
  BOOST_CHECK_EQUAL( 9, M2(0,0) );
  BOOST_CHECK_EQUAL( 9, M2(0,1) );
  BOOST_CHECK_EQUAL( 5, M3(0,0) );

  (M1, M2, M3) = (L1 + L2, R1 + R2, L1);

  BOOST_CHECK_EQUAL( 5, M1(0,0) );
  BOOST_CHECK_EQUAL( 9, M2(0,0) );
  BOOST_CHECK_EQUAL( 9, M2(0,1) );
  BOOST_CHECK_EQUAL( 2, M3(0,0) );

  (M1, M2, M3) = (L1, R1, L1 + L2);

  BOOST_CHECK_EQUAL( 2, M1(0,0) );
  BOOST_CHECK_EQUAL( 4, M2(0,0) );
  BOOST_CHECK_EQUAL( 4, M2(0,1) );
  BOOST_CHECK_EQUAL( 5, M3(0,0) );

}

//----------------------------------------------------------------------------//
// matrix-vector multiply
BOOST_AUTO_TEST_CASE( Matrix_Tuple_Vector_Multiply2 )
{
  MatrixD<Real> m1(2, 2);
  MatrixD<Real> m2(2, 2);

  MatrixD<Real> col1(2, 1);
  MatrixD<Real> col2(2, 1);
  MatrixD<Real> col3(2, 1);
  MatrixD<Real> col4(2, 1);

  MatrixD<Real> row1(1, 2);
  MatrixD<Real> row2(1, 2);
  MatrixD<Real> row3(1, 2);
  MatrixD<Real> row4(1, 2);

  m1(0,0) = 3; m1(0,1) = 4;
  m1(1,0) = 5; m1(1,1) = 6;

  m2 = m1;

  col1(0,0) = 1;
  col1(1,0) = 2;

  col2 = col1;

  row1(0,0) = 1; row1(0,1) = 2;

  row2 = row1;

  //Check column multiplication

  BOOST_CHECK( chkMatrixD21( col1, 1,2 ) );
  BOOST_CHECK( chkMatrixD21( col2, 1,2 ) );

  BOOST_CHECK( chkMatrixD22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixD22( m2, 3,4,5,6 ) );

  m2 = m1*(col1, col2);
  BOOST_CHECK( chkMatrixD22( m2, 11,11,17,17 ) );
  BOOST_CHECK( chkMatrixD22( m1, 3,4,5,6 ) );

  (col3, col4) = m1*(col1, col2);
  BOOST_CHECK( chkMatrixD21( col3, 11,17 ) );
  BOOST_CHECK( chkMatrixD21( col4, 11,17 ) );
  BOOST_CHECK( chkMatrixD22( (col3, col4), 11, 11, 17, 17 ) );
  BOOST_CHECK( chkMatrixD22( m1, 3,4,5,6 ) );

  (col3, col4) += m1*(col1, col2);
  BOOST_CHECK( chkMatrixD21( col3, 2*11,2*17 ) );
  BOOST_CHECK( chkMatrixD21( col4, 2*11,2*17 ) );
  BOOST_CHECK( chkMatrixD22( m1, 3,4,5,6 ) );

  (col3, col4) -= m1*(col1, col2);
  BOOST_CHECK( chkMatrixD21( col3, 11,17 ) );
  BOOST_CHECK( chkMatrixD21( col4, 11,17 ) );
  BOOST_CHECK( chkMatrixD22( m1, 3,4,5,6 ) );

  (col3, col4) = (col1, col2)*m1;
  BOOST_CHECK( chkMatrixD21( col3, 8,16 ) );
  BOOST_CHECK( chkMatrixD21( col4, 10,20 ) );

  (col3, col4) += (col1, col2)*m1;
  BOOST_CHECK( chkMatrixD21( col3, 2*8,2*16 ) );
  BOOST_CHECK( chkMatrixD21( col4, 2*10,2*20 ) );

  (col3, col4) -= (col1, col2)*m1;
  BOOST_CHECK( chkMatrixD21( col3, 8,16 ) );
  BOOST_CHECK( chkMatrixD21( col4, 10,20 ) );

  m2 = m1;

  (col3, col4) = m2*m1;
  BOOST_CHECK( chkMatrixD21( col3, 29,45 ) );
  BOOST_CHECK( chkMatrixD21( col4, 36,56 ) );

  (col3, col4) += m2*m1;
  BOOST_CHECK( chkMatrixD21( col3, 2*29,2*45 ) );
  BOOST_CHECK( chkMatrixD21( col4, 2*36,2*56 ) );

  (col3, col4) -= m2*m1;
  BOOST_CHECK( chkMatrixD21( col3, 29,45 ) );
  BOOST_CHECK( chkMatrixD21( col4, 36,56 ) );

  (col3, col4) = (m2 + m1)*(m1 + m2);
  BOOST_CHECK( chkMatrixD21( col3, 4*29,4*45 ) );
  BOOST_CHECK( chkMatrixD21( col4, 4*36,4*56 ) );

  (col3, col4) += (m2 + m1)*(m1 + m2);
  BOOST_CHECK( chkMatrixD21( col3, 8*29,8*45 ) );
  BOOST_CHECK( chkMatrixD21( col4, 8*36,8*56 ) );

  (col3, col4) -= (m2 + m1)*(m1 + m2);
  BOOST_CHECK( chkMatrixD21( col3, 4*29,4*45 ) );
  BOOST_CHECK( chkMatrixD21( col4, 4*36,4*56 ) );

  //Make sure that an exception is thrown when dimensions do not match
  //or when the same veriable is used on the left and right as part of a multiplication

  BOOST_CHECK_THROW( (row1, row2)  = m1*(col1, col2), AssertionException );
  BOOST_CHECK_THROW( (row1, row2) += m1*(col1, col2), AssertionException );
  BOOST_CHECK_THROW( (row1, row2) -= m1*(col1, col2), AssertionException );

  BOOST_CHECK_THROW( (col1, col2)  = (row1, row2)*m1, AssertionException );
  BOOST_CHECK_THROW( (col1, col2) += (row1, row2)*m1, AssertionException );
  BOOST_CHECK_THROW( (col1, col2) -= (row1, row2)*m1, AssertionException );

  BOOST_CHECK_THROW( (row1, row2)  = (col1, col2)*m1, AssertionException );
  BOOST_CHECK_THROW( (row1, row2) += (col1, col2)*m1, AssertionException );
  BOOST_CHECK_THROW( (row1, row2) -= (col1, col2)*m1, AssertionException );

  BOOST_CHECK_THROW( (col1, col2)  = m1*(row1, row2), AssertionException );
  BOOST_CHECK_THROW( (col1, col2) += m1*(row1, row2), AssertionException );
  BOOST_CHECK_THROW( (col1, col2) -= m1*(row1, row2), AssertionException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Tuple_swap_rows )
{
  MatrixD<Real> M1(2,3), M2(2,3);

  for (int i = 0; i < 3; ++i)
  {
    M1(0,i) = i+1;
    M1(1,i) = i+3;

    M2(0,i) = i+2;
    M2(1,i) = i+4;
  }

  (M1, M2).swap_rows(0,1);

  for (int i = 0; i < 3; ++i)
  {
    BOOST_CHECK_EQUAL( M1(0,i), i+3 );
    BOOST_CHECK_EQUAL( M1(1,i), i+1 );

    BOOST_CHECK_EQUAL( M2(0,i), i+4 );
    BOOST_CHECK_EQUAL( M2(1,i), i+2 );
  }


  MatrixD<Real> M3(3,3), M4(3,3);

  for (int i = 0; i < 3; ++i)
  {
    M3(0,i) = i+1;
    M3(1,i) = i+3;
    M3(2,i) = i+5;

    M4(0,i) = i+2;
    M4(1,i) = i+4;
    M4(2,i) = i+6;
  }

  (M3, M4).swap_rows(0,2);

  for (int i = 0; i < 3; ++i)
  {
    BOOST_CHECK_EQUAL( M3(0,i), i+5 );
    BOOST_CHECK_EQUAL( M3(1,i), i+3 );
    BOOST_CHECK_EQUAL( M3(2,i), i+1 );

    BOOST_CHECK_EQUAL( M4(0,i), i+6 );
    BOOST_CHECK_EQUAL( M4(1,i), i+4 );
    BOOST_CHECK_EQUAL( M4(2,i), i+2 );
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Tuple_scale_row )
{

  MatrixD<Real> M1(3,3), M2(3,3);

  for (int i = 0; i < 3; ++i)
  {
    M1(0,i) = i+1;
    M1(1,i) = i+3;
    M1(2,i) = i+5;

    M2(0,i) = i+2;
    M2(1,i) = i+4;
    M2(2,i) = i+6;
  }

  (M1, M2).scale_row(1,2);

  for (int i = 0; i < 3; ++i)
  {
    BOOST_CHECK_EQUAL( M1(0,i), i+1 );
    BOOST_CHECK_EQUAL( M1(1,i), 2*(i+3) );
    BOOST_CHECK_EQUAL( M1(2,i), i+5 );

    BOOST_CHECK_EQUAL( M2(0,i), i+2 );
    BOOST_CHECK_EQUAL( M2(1,i), 2*(i+4) );
    BOOST_CHECK_EQUAL( M2(2,i), i+6 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Tuple_axpy )
{

  MatrixD<Real> M1(3,3), M2(3,3);

  for (int i = 0; i < 3; ++i)
  {
    M1(0,i) = i+1;
    M1(1,i) = i+3;
    M1(2,i) = i+5;

    M2(0,i) = i+2;
    M2(1,i) = i+4;
    M2(2,i) = i+6;
  }

  (M1, M2).axpy_rows(0,1,2);

  for (int i = 0; i < 3; ++i)
  {
    BOOST_CHECK_EQUAL( M1(0,i), i+1 );
    BOOST_CHECK_EQUAL( M1(1,i), i+3 + 2*(i+1) );
    BOOST_CHECK_EQUAL( M1(2,i), i+5 );

    BOOST_CHECK_EQUAL( M2(0,i), i+2 );
    BOOST_CHECK_EQUAL( M2(1,i), i+4 + 2*(i+2) );
    BOOST_CHECK_EQUAL( M2(2,i), i+6 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Tuple_InverseLUP_Rand )
{
  typedef Real T;
  static const int CacheItems = CACHE_LINE_SIZE/sizeof(Real);

  for ( int n = CacheItems/2; n <= 2*CacheItems; n += 2)
  {
    MatrixD<T> A(n,n), tmp(n,n), I(n,n), Ainv(n,n);
    MatrixD<T> L1(n,n/2), L2(n,n/2);

    I = Identity();

    for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
        A(i,j) = T(rand() % 101/100.) + 5*I(i,j);

    tmp = A;

    (L1, L2) = InverseLUP::Inverse(A);

    Ainv = (L1, L2);

    I = Ainv*A;

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
      {
        if (i == j)
          BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
        else
          BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
      }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Tuple_InverseLUP_Rand_RHSExpression )
{
  typedef Real T;
  static const int CacheItems = CACHE_LINE_SIZE/sizeof(Real);

  for ( int n = CacheItems/2; n <= 2*CacheItems; n += 2)
  {
    MatrixD<T> A(n,n), tmp(n,n), I(n,n), Ainv(n,n);
    MatrixD<T> R1(n,n/2), R2(n,n/2);
    MatrixD<T> L1(n,n/2), L2(n,n/2);

    I = Identity();

    for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
        A(i,j) = T(rand() % 101/100.) + 5*I(i,j);

    tmp = A;
    (R1, R2) = I;

    (L1, L2) = InverseLUP::Solve(A, (R1, R2));

    Ainv = (L1, L2);

    I = Ainv*A;

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
      {
        if (i == j)
          BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
        else
          BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
      }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Tuple_InverseLUP_Rand_RHSExpression2 )
{
  typedef Real T;
  static const int CacheItems = CACHE_LINE_SIZE/sizeof(Real);

  for ( int n = CacheItems/2; n <= 2*CacheItems; n += 2)
  {
    MatrixD<T> A(n,n), tmp(n,n), I(n,n), Ainv(n,n);
    MatrixD<T> R1(n,n/2), R2(n,n/2);
    MatrixD<T> L1(n,n/2), L2(n,n/2);
    MatrixD<T> C1(n,n/2), C2(n,n/2);

    I = Identity();

    for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
        A(i,j) = T(rand() % 101/100.) + 5*I(i,j);

    tmp = A;
    (R1, R2) = I;
    (C1, C2) = I;
    (C1, C2) *= 2;

    (L1, L2) = InverseLUP::Solve(A, (R1, R2)) + (C1, C2);

    Ainv = (L1, L2);

    Ainv -= (C1, C2);

    I = Ainv*A;

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
      {
        if (i == j)
          BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
        else
          BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
      }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Tuple_InverseLU_Rand )
{
  typedef Real T;
  static const int CacheItems = CACHE_LINE_SIZE/sizeof(Real);

  for ( int n = CacheItems/2; n <= 2*CacheItems; n += 2)
  {
    MatrixD<T> A(n,n), tmp(n,n), I(n,n), Ainv(n,n);
    MatrixD<T> L1(n,n/2), L2(n,n/2);

    I = Identity();

    for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
        A(i,j) = T(rand() % 101/100.) + 5*I(i,j);

    tmp = A;

    (L1, L2) = InverseLU::Inverse(A);

    Ainv = (L1, L2);

    I = Ainv*A;

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
      {
        if (i == j)
          BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
        else
          BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
      }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Tuple_InverseLU_Rand_RHSExpression )
{
  typedef Real T;
  static const int CacheItems = CACHE_LINE_SIZE/sizeof(Real);

  for ( int n = CacheItems/2; n <= 2*CacheItems; n += 2)
  {
    MatrixD<T> A(n,n), tmp(n,n), I(n,n), Ainv(n,n);
    MatrixD<T> R1(n,n/2), R2(n,n/2);
    MatrixD<T> L1(n,n/2), L2(n,n/2);

    I = Identity();

    for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
        A(i,j) = T(rand() % 101/100.) + 5*I(i,j);

    tmp = A;
    (R1, R2) = I;

    (L1, L2) = InverseLU::Solve(A, (R1, R2) );

    Ainv = (L1, L2);

    I = Ainv*A;

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
      {
        if (i == j)
          BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
        else
          BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
      }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BlockTriDiag )
{
  typedef Real T;

  Real Mdata1[] = {2, 1,
                   3, 2};

  Real Mdata2[] = {1, 4,
                   2, 3};

  Real Mdata3[] = {5, 2,
                   1, 6};

  Real Vdata1[] = {2, 3};

  Real Vdata2[] = {5, 1};

  MatrixD<T> Matrix1(2,2, Mdata1);
  MatrixD<T> Matrix2(2,2, Mdata2), Matrix3(2,2, Mdata3), Matrix4(2,2);
  MatrixD<T> Vector1(2,1,Vdata1), Vector2(2,1, Vdata2);

  MatrixD<T> M2(2,2, Mdata2);

  MatrixD<T> Vector3 = Vector1 - Matrix3*Vector2;

  (Matrix2, Vector1) = InverseLUP::Solve(Matrix1, (Matrix2, Vector1 - Matrix3*Vector2) );

  BOOST_CHECK_SMALL( Matrix2(0,0),  T(1e-12) );
  BOOST_CHECK_CLOSE( Matrix2(0,1),  5., T(0.0001) );
  BOOST_CHECK_CLOSE( Matrix2(1,0),  1., T(0.0001) );
  BOOST_CHECK_CLOSE( Matrix2(1,1), -6., T(0.0001) );

  BOOST_CHECK_CLOSE( Vector1(0,0), -42., T(0.0001) );
  BOOST_CHECK_CLOSE( Vector1(0,1),  59., T(0.0001) );

  Matrix2 = M2;

  Matrix4 = (Matrix1 - Matrix2*Matrix3);

  BOOST_CHECK_CLOSE( Matrix4(0,0),  -7., T(0.0001) );
  BOOST_CHECK_CLOSE( Matrix4(0,1), -25., T(0.0001) );
  BOOST_CHECK_CLOSE( Matrix4(1,0), -10., T(0.0001) );
  BOOST_CHECK_CLOSE( Matrix4(1,1), -20., T(0.0001) );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
