// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Decompose_LUP.h"
#include "LinearAlgebra/DenseLinAlg/tools/SingularException.h"

#include <iostream>
using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Decompose_LUP_Exceptions )
{
  MatrixD<Real> A1(2,2), LU1(2,2);
  VectorD<int> P1(2);
  A1 = 0;
  BOOST_CHECK_THROW( Decompose_LUP(A1, LU1, P1), SingularMatrixException );

  MatrixD<Real> A2(3,2);
  A2 = Identity();
  BOOST_CHECK_THROW( Decompose_LUP(A2, LU1, P1), AssertionException );

  MatrixD<Real> A3(2,3);
  A3 = Identity();
  BOOST_CHECK_THROW( Decompose_LUP(A2, LU1, P1), AssertionException );

  MatrixD<Real> LU2(3,2);
  A1 = Identity();
  BOOST_CHECK_THROW( Decompose_LUP(A1, LU2, P1), AssertionException );

  MatrixD<Real> LU3(2,3);
  A1 = Identity();
  BOOST_CHECK_THROW( Decompose_LUP(A1, LU3, P1), AssertionException );

  VectorD<int> P2(3);
  A1 = Identity();
  BOOST_CHECK_THROW( Decompose_LUP(A1, LU1, P2), AssertionException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Decompose_LUP_Pivot_5x5_I )
{
  typedef Real T;

  MatrixD<T> Ainv(5,5), LU(5,5);
  VectorD<int> P(5);

  Real vals[] = {0, 0, 0, 0, 5,
                 0, 0, 0, 5, 0,
                 0, 0, 5, 0, 0,
                 0, 5, 0, 0, 0,
                 5, 0, 0, 0, 0};

  MatrixD<T> A(5,5,vals);

  Decompose_LUP(A, LU, P);

  BOOST_CHECK_EQUAL( P[0], 4 );
  BOOST_CHECK_EQUAL( P[1], 3 );
  BOOST_CHECK_EQUAL( P[2], 2 );
  BOOST_CHECK_EQUAL( P[3], 3 );
  BOOST_CHECK_EQUAL( P[4], 0 );

  for (int i = 0; i < 5; ++i)
    for (int j = 0; j < 5; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( LU(i,j), T(5), T(1e-10) );
      else
        BOOST_CHECK_SMALL( LU(i,j), T(1e-12) );
    }

  LU = 0; P = 0;

  (LU, P) = Decompose_LUP(A);

  BOOST_CHECK_EQUAL( P[0], 4 );
  BOOST_CHECK_EQUAL( P[1], 3 );
  BOOST_CHECK_EQUAL( P[2], 2 );
  BOOST_CHECK_EQUAL( P[3], 3 );
  BOOST_CHECK_EQUAL( P[4], 0 );

  for (int i = 0; i < 5; ++i)
    for (int j = 0; j < 5; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( LU(i,j), T(5), T(0.0001) );
      else
        BOOST_CHECK_SMALL( LU(i,j), T(1e-12) );
    }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Decompose_LUP_Pivot_5x5 )
{
  typedef Real T;

  MatrixD<T> LU(5,5);
  VectorD<int> P(5);

  Real vals[] = {5, 0, 0, 0, 0,
                 0, 0, 5, 0, 0,
                 0, 5, 0, 0, 0,
                 0, 0, 0, 0, 5,
                 0, 0, 0, 5, 0};

  MatrixD<T> A(5,5,vals);

  Decompose_LUP(A, LU, P);

  BOOST_CHECK_EQUAL( P[0], 0 );
  BOOST_CHECK_EQUAL( P[1], 2 );
  BOOST_CHECK_EQUAL( P[2], 2 );
  BOOST_CHECK_EQUAL( P[3], 4 );
  BOOST_CHECK_EQUAL( P[4], 3 );

  for (int i = 0; i < 5; ++i)
    for (int j = 0; j < 5; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( LU(i,j) , T(5), T(0.0001) );
      else
        BOOST_CHECK_SMALL( LU(i,j) , T(1e-12) );
    }

  LU = 0; P = 0;

  (LU, P) = Decompose_LUP(A);

  BOOST_CHECK_EQUAL( P[0], 0 );
  BOOST_CHECK_EQUAL( P[1], 2 );
  BOOST_CHECK_EQUAL( P[2], 2 );
  BOOST_CHECK_EQUAL( P[3], 4 );
  BOOST_CHECK_EQUAL( P[4], 3 );

  for (int i = 0; i < 5; ++i)
    for (int j = 0; j < 5; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( LU(i,j) , T(5), T(0.0001) );
      else
        BOOST_CHECK_SMALL( LU(i,j) , T(1e-12) );
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Decompose_LUP_Pivot_7x7 )
{
  typedef Real T;

  MatrixD<T> LU(7,7);
  VectorD<int> P(7);

  Real vals[] = {5, 0, 0, 0, 0, 0, 0,
                 0, 0, 5, 0, 0, 0, 0,
                 0, 0, 0, 5, 0, 0, 0,
                 0, 0, 0, 0, 0, 0, 5,
                 0, 5, 0, 0, 0, 0, 0,
                 0, 0, 0, 0, 0, 5, 0,
                 0, 0, 0, 0, 5, 0, 0};

  MatrixD<T> A(7,7,vals);

  Decompose_LUP(A, LU, P);

  BOOST_CHECK_EQUAL( P[0], 0 );
  BOOST_CHECK_EQUAL( P[1], 4 );
  BOOST_CHECK_EQUAL( P[2], 4 );
  BOOST_CHECK_EQUAL( P[3], 4 );
  BOOST_CHECK_EQUAL( P[4], 6 );
  BOOST_CHECK_EQUAL( P[5], 5 );
  BOOST_CHECK_EQUAL( P[6], 3 );

  for (int i = 0; i < 7; ++i)
    for (int j = 0; j < 7; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( LU(i,j) , T(5), T(0.0001) );
      else
        BOOST_CHECK_SMALL( LU(i,j) , T(1e-12) );
    }

  LU = 0; P = 0;

  (LU, P) = Decompose_LUP(A);

  BOOST_CHECK_EQUAL( P[0], 0 );
  BOOST_CHECK_EQUAL( P[1], 4 );
  BOOST_CHECK_EQUAL( P[2], 4 );
  BOOST_CHECK_EQUAL( P[3], 4 );
  BOOST_CHECK_EQUAL( P[4], 6 );
  BOOST_CHECK_EQUAL( P[5], 5 );
  BOOST_CHECK_EQUAL( P[6], 3 );

  for (int i = 0; i < 5; ++i)
    for (int j = 0; j < 5; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( LU(i,j) , T(5), T(0.0001) );
      else
        BOOST_CHECK_SMALL( LU(i,j) , T(1e-12) );
    }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Decompose_LUP_1x1 )
{
  typedef Real T;
  MatrixD<T> A(1, 1);
  MatrixD<T> LU(1, 1);
  VectorD<int> P(1);

  A = 2;

  Decompose_LUP(A, LU, P);

  BOOST_CHECK_CLOSE( A(0,0), T(2.), T(0.0001) );

  BOOST_CHECK_CLOSE( LU(0,0), T(2.), T(0.0001) );

  BOOST_CHECK_EQUAL( P[0], 0 );


  LU = 0; P = 0;

  (LU, P) = Decompose_LUP(A);

  BOOST_CHECK_CLOSE( A(0,0), T(2.), T(0.0001) );

  BOOST_CHECK_CLOSE( LU(0,0), T(2.), T(0.0001) );

  BOOST_CHECK_EQUAL( P[0], 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Decompose_LUP_2x2 )
{
  typedef Real T;

  Real Avals[] = {2, 1,
                  3, 2};

  MatrixD<T> A(2,2,Avals);
  MatrixD<T> LU(2,2);
  VectorD<int> P(2);

  Decompose_LUP(A, LU, P);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( LU(0,0),     3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1),  2./3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0),     2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), -1./3., T(0.0001) );

  BOOST_CHECK_EQUAL( P[0], 1 );
  BOOST_CHECK_EQUAL( P[1], 0 );


  LU = 0; P = 0;

  (LU, P) = Decompose_LUP(A);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( LU(0,0),     3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1),  2./3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0),     2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), -1./3., T(0.0001) );

  BOOST_CHECK_EQUAL( P[0], 1 );
  BOOST_CHECK_EQUAL( P[1], 0 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Decompose_LUP_2x2_AExpression )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  MatrixD<T> A1(2,2,Avals), A2(2,2);
  MatrixD<T> LU(2,2);
  VectorD<int> P(2,1);

  A2 = 1;

  Decompose_LUP(A1 + A2, LU, P);

  BOOST_CHECK_CLOSE( LU(0,0),     3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1),  2./3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0),     2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), -1./3., T(0.0001) );

  BOOST_CHECK_EQUAL( P[0], 1 );
  BOOST_CHECK_EQUAL( P[1], 0 );


  LU = 0; P = 0;

  (LU, P) = Decompose_LUP(A1 + A2);

  BOOST_CHECK_CLOSE( LU(0,0),     3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1),  2./3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0),     2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), -1./3., T(0.0001) );

  BOOST_CHECK_EQUAL( P[0], 1 );
  BOOST_CHECK_EQUAL( P[1], 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Decompose_LUP_3x3 )
{
  typedef Real T;

  Real Adata[] = {1, 2, 3,
                  2, 4, 5,
                  3, 5, 6};

  MatrixD<T> A(3,3, Adata);
  MatrixD<T> LU(3,3);
  VectorD<int> P(3,1);

  Decompose_LUP(A, LU, P);

  BOOST_CHECK_CLOSE( LU(0,0), T( 3.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1), T( 5./3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,2), T( 2.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0), T( 2.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), T( 2./3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,2), T( 3./2. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,0), T( 1.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,1), T( 1./3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,2), T( 1./2. ), T(0.0001) );

  BOOST_CHECK_EQUAL( P[0], 2 );
  BOOST_CHECK_EQUAL( P[1], 1 );
  BOOST_CHECK_EQUAL( P[2], 0 );


  LU = 0;
  P = 0;
  // cppcheck-suppress redundantAssignment
  (LU, P) = Decompose_LUP(A);

  BOOST_CHECK_CLOSE( LU(0,0), T( 3.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1), T( 5./3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,2), T( 2.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0), T( 2.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), T( 2./3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,2), T( 3./2. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,0), T( 1.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,1), T( 1./3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,2), T( 1./2. ), T(0.0001) );

  BOOST_CHECK_EQUAL( P[0], 2 );
  BOOST_CHECK_EQUAL( P[1], 1 );
  BOOST_CHECK_EQUAL( P[2], 0 );


  MatrixD<T> L(3,3), U(3,3), PI(3,3);

  L = 0;
  U = 0;
  // cppcheck-suppress constStatement
  PI = 0;

  for (int i = 0; i < 3; i++)
  {
    for (int j = 0; j < i+1; j++)
    {
      L(i,j) = LU(i,j);
      U(2-i,2-j) = LU(2-i,2-j);
    }
    U(2-i,2-i) = 1;
    // cppcheck-suppress constStatement
    PI(i,P[i]) = 1;
  }
#if 0
  LU.dump();
  L.dump();
  U.dump();
  PI.dump();
#endif
  MatrixD<T> A2 = PI*L*U;

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      BOOST_CHECK_CLOSE( A(i,j), A2(i,j), T(0.0001) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Decompose_LUP_3x3_2 )
{
  typedef Real T;

  Real Adata[] = {4, 2, 2,
                  2, 4, 6,
                  2, 6, 6};

  MatrixD<T> A(3,3, Adata);
  MatrixD<T> LU(3,3);
  VectorD<int> P(3,1);

  Decompose_LUP(A, LU, P);
#if 0
  BOOST_CHECK_CLOSE( LU(0,0), T( 3.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1), T( 5./3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,2), T( 2.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0), T( 2.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), T( 2./3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,2), T( 3./2. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,0), T( 1.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,1), T( 1./3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,2), T( 1./2. ), T(0.0001) );


  LU = 0;
  P = 0;
  // cppcheck-suppress redundantAssignment
  (LU, P) = Decompose_LUP(A);

  BOOST_CHECK_CLOSE( LU(0,0), T( 3.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1), T( 5./3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,2), T( 2.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0), T( 2.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), T( 2./3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,2), T( 3./2. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,0), T( 1.    ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,1), T( 1./3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,2), T( 1./2. ), T(0.0001) );
#endif

  MatrixD<T> L(3,3), U(3,3), PI(3,3);

  L = 0;
  U = 0;
  // cppcheck-suppress constStatement
  PI = 0;

  for (int i = 0; i < 3; i++)
  {
    for (int j = 0; j < i+1; j++)
    {
      L(i,j) = LU(i,j);
      U(2-i,2-j) = LU(2-i,2-j);
    }
    U(2-i,2-i) = 1;
    // cppcheck-suppress constStatement
    PI(i,P[i]) = 1;
  }
#if 0
  LU.dump();
  L.dump();
  U.dump();
  PI.dump();
#endif
  MatrixD<T> A2 = PI*L*U;

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      BOOST_CHECK_CLOSE( A(i,j), A2(i,j), T(0.0001) );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
