// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/range_c.hpp>

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

//Including the _impl.h file here so that all the unit tests don't have to be
//explicitly instantiated. This should not be done in general, this unit test is a special case.
#define MATRIXS_MATMUL_NATIVE_INSTANTIATE
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatMul/MatrixS_MatMul_Native_impl.h"

#include <iostream>
using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( MatrixS_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatMul_Native_1x1x1 )
{
  MatrixS<1,1,Real> ML, MR, res;
  ML = 2;
  MR = 3;

  MatrixS_MatMul_Native<Real, Real, Real, Real>::value(ML, MR, 1, res);
  BOOST_CHECK_EQUAL( 6, res(0,0) );

  MatrixS_MatMul_Native<Real, Real, Real, Real>::plus(ML, MR, 1, res);
  BOOST_CHECK_EQUAL( 6*2, res(0,0) );

  MatrixS_MatMul_Native<Real, Real, Real, Real>::plus(ML, MR, -1, res);
  BOOST_CHECK_EQUAL( 6, res(0,0) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatMul_Native_2x2x1 )
{
  MatrixS<2,2,Real> ML;
  MatrixS<2,1,Real> MR, res;

  ML(0,0) = 1; ML(0,1) = 2;
  ML(1,0) = 4; ML(1,1) = 3;

  MR(0,0) = 2;
  MR(1,0) = 3;

  MatrixS_MatMul_Native<Real, Real, Real, Real>::value(ML, MR, 1, res);
  BOOST_CHECK_EQUAL(  8, res(0,0) );
  BOOST_CHECK_EQUAL( 17, res(1,0) );

  MatrixS_MatMul_Native<Real, Real, Real, Real>::plus(ML, MR, 1, res);
  BOOST_CHECK_EQUAL(  8*2, res(0,0) );
  BOOST_CHECK_EQUAL( 17*2, res(1,0) );

  MatrixS_MatMul_Native<Real, Real, Real, Real>::plus(ML, MR, -1, res);
  BOOST_CHECK_EQUAL(  8, res(0,0) );
  BOOST_CHECK_EQUAL( 17, res(1,0) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatMul_Native_4x4x1 )
{
  MatrixS<4,4,Real> ML, MR, res;
  ML(0,0) = 1; ML(0,1) = 2; ML(0,2) = 3; ML(0,3) = 4;
  ML(1,0) = 8; ML(1,1) = 7; ML(1,2) = 6; ML(1,3) = 5;
  ML(2,0) = 4; ML(2,1) = 3; ML(2,2) = 2; ML(2,3) = 1;
  ML(3,0) = 5; ML(3,1) = 6; ML(3,2) = 7; ML(3,3) = 8;

  MR = 0;
  MR(0,0) = 2;
  MR(1,0) = 3;
  MR(2,0) = 4;
  MR(3,0) = 5;

  MatrixS_MatMul_Native<Real, Real, Real, Real>::value(ML, MR, 1, res);
  BOOST_CHECK_EQUAL( 40, res(0,0) );
  BOOST_CHECK_EQUAL( 86, res(1,0) );
  BOOST_CHECK_EQUAL( 30, res(2,0) );
  BOOST_CHECK_EQUAL( 96, res(3,0) );

  MatrixS_MatMul_Native<Real, Real, Real, Real>::plus(ML, MR, 1, res);
  BOOST_CHECK_EQUAL( 40*2, res(0,0) );
  BOOST_CHECK_EQUAL( 86*2, res(1,0) );
  BOOST_CHECK_EQUAL( 30*2, res(2,0) );
  BOOST_CHECK_EQUAL( 96*2, res(3,0) );

  MatrixS_MatMul_Native<Real, Real, Real, Real>::plus(ML, MR, -1, res);
  BOOST_CHECK_EQUAL( 40, res(0,0) );
  BOOST_CHECK_EQUAL( 86, res(1,0) );
  BOOST_CHECK_EQUAL( 30, res(2,0) );
  BOOST_CHECK_EQUAL( 96, res(3,0) );
}


static const int CacheItems = CACHE_LINE_SIZE/sizeof(Real);
typedef boost::mpl::range_c<int, CacheItems/2, 2*CacheItems> MatrixSizes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MatMul_Native_nxnxn, N, MatrixSizes )
{
  static const int n = N::value;

  MatrixS<n,n,Real> ML, MR, res;

  ML = 1;
  MR = 1;

  MatrixS_MatMul_Native<Real, Real, Real, Real>::value(ML, MR, 1, res);

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
      BOOST_CHECK_EQUAL( res(i,j), n );

  MatrixS_MatMul_Native<Real, Real, Real, Real>::plus(ML, MR, 1, res);

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
      BOOST_CHECK_EQUAL( res(i,j), n*2 );

  MatrixS_MatMul_Native<Real, Real, Real, Real>::plus(ML, MR, -1, res);

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
      BOOST_CHECK_EQUAL( res(i,j), n );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MatMul_Native_nx2nxn, N, MatrixSizes )
{
  static const int n = N::value;

  MatrixS<  n,2*n,Real> ML;
  MatrixS<2*n,  n,Real> MR;
  MatrixS<  n,  n,Real> res;

  ML = 1;
  MR = 1;

  MatrixS_MatMul_Native<Real, Real, Real, Real>::value(ML, MR, 1, res);

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
      BOOST_CHECK_EQUAL( res(i,j), 2*n );

  MatrixS_MatMul_Native<Real, Real, Real, Real>::plus(ML, MR, 1, res);

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
      BOOST_CHECK_EQUAL( res(i,j), 2*n*2 );

  MatrixS_MatMul_Native<Real, Real, Real, Real>::plus(ML, MR, -1, res);

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
      BOOST_CHECK_EQUAL( res(i,j), 2*n );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MatMul_Native_nx3nxn, N, MatrixSizes )
{
  static const int n = N::value;

  MatrixS<  n,3*n,Real> ML;
  MatrixS<3*n,  n,Real> MR;
  MatrixS<  n,  n,Real> res;

  ML = 1;
  MR = 1;

  MatrixS_MatMul_Native<Real, Real, Real, Real>::value(ML, MR, 1, res);

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
      BOOST_CHECK_EQUAL( res(i,j), 3*n );

  MatrixS_MatMul_Native<Real, Real, Real, Real>::plus(ML, MR, 1, res);

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
      BOOST_CHECK_EQUAL( res(i,j), 3*n*2 );

  MatrixS_MatMul_Native<Real, Real, Real, Real>::plus(ML, MR, -1, res);

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
      BOOST_CHECK_EQUAL( res(i,j), 3*n );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
