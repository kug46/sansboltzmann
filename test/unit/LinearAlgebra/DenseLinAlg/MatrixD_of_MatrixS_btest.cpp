// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include <iostream>
using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

typedef MatrixS<1,1,Real> MatrixS1;
typedef MatrixS<2,2,Real> MatrixS2;
typedef VectorS<2,Real> VectorS2;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_MatrixS2_2x2 )
{
  MatrixD<MatrixS2> M1(2,2);

  for (int i = 0; i < 2; ++i)
  {
    M1(0,i) = i+1;
    M1(1,i) = i+2;
  }

  M1.swap_rows(0,1);

  for (int i = 0; i < 2; ++i)
  {
    for (int j = 0; j < 2; ++j)
      for (int k = 0; k < 2; ++k)
      {
        BOOST_CHECK_EQUAL( M1(0,i)(j,k), i+2 );
        BOOST_CHECK_EQUAL( M1(1,i)(j,k), i+1 );
      }
  }
//
//
//  MatrixD<MatrixS1> M2(3,3);
//
//  for (int i = 0; i < 3; ++i)
//  {
//    M2(0,i) = i+1;
//    M2(1,i) = i+3;
//    M2(2,i) = i+6;
//  }
//
//  M2.swap_rows(0,2);
//
//  for (int i = 0; i < 3; ++i)
//  {
//    BOOST_CHECK_EQUAL( M2(0,i), i+6 );
//    BOOST_CHECK_EQUAL( M2(1,i), i+3 );
//    BOOST_CHECK_EQUAL( M2(2,i), i+1 );
//  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_MatrixS1_ctor )
{

  MatrixD<MatrixS1> M1(2, 2);

  BOOST_CHECK_EQUAL( 4, M1.size() );
  BOOST_CHECK_EQUAL( 2, M1.m() );
  BOOST_CHECK_EQUAL( 2, M1.n() );
  BOOST_CHECK_EQUAL( 2, M1.stride() );

  M1(0,0) = 1;
  M1(0,1) = 0;
  M1(1,0) = 3;
  M1(1,1) = 2;

  BOOST_CHECK_EQUAL( 1, M1(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 0, M1(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 3, M1(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 2, M1(1,1)(0,0) );

  MatrixD<MatrixS1> M2(2, 2);
  M2 = 0;
  M2(0,0) = 1;
  M2(0,1) = 42;
  M2(1,0) = 1929;
  M2(1,1) = 2;

  BOOST_CHECK_EQUAL( 4, M2.size() );
  BOOST_CHECK_EQUAL( 2, M2.m() );
  BOOST_CHECK_EQUAL( 2, M2.n() );
  BOOST_CHECK_EQUAL( 2, M2.stride() );

  BOOST_CHECK_EQUAL(    1, M2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(   42, M2(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 1929, M2(1,0)(0,0) );
  BOOST_CHECK_EQUAL(    2, M2(1,1)(0,0) );

  MatrixS1 v3[4] = {MatrixS1(1), MatrixS1(42), MatrixS1(1929), MatrixS1(2)};
  MatrixDView<MatrixS1> M3(v3, 2, 2);
  MatrixD<MatrixS1> M4( M3 );

  BOOST_CHECK_EQUAL( 4, M4.size() );
  BOOST_CHECK_EQUAL( 2, M4.m() );
  BOOST_CHECK_EQUAL( 2, M4.n() );
  BOOST_CHECK_EQUAL( 2, M4.stride() );

  BOOST_CHECK_EQUAL(    1, M4(0,0)(0,0) );
  BOOST_CHECK_EQUAL(   42, M4(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 1929, M4(1,0)(0,0) );
  BOOST_CHECK_EQUAL(    2, M4(1,1)(0,0) );

  MatrixD<MatrixS1> M5( M4 );

  BOOST_CHECK_EQUAL( 4, M5.size() );
  BOOST_CHECK_EQUAL( 2, M5.m() );
  BOOST_CHECK_EQUAL( 2, M5.n() );
  BOOST_CHECK_EQUAL( 2, M5.stride() );

  BOOST_CHECK_EQUAL(    1, M5(0,0)(0,0) );
  BOOST_CHECK_EQUAL(   42, M5(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 1929, M5(1,0)(0,0) );
  BOOST_CHECK_EQUAL(    2, M5(1,1)(0,0) );

  MatrixD<MatrixS1> M6 = M5;

  BOOST_CHECK_EQUAL( 4, M6.size() );
  BOOST_CHECK_EQUAL( 2, M6.m() );
  BOOST_CHECK_EQUAL( 2, M6.n() );
  BOOST_CHECK_EQUAL( 2, M6.stride() );

  BOOST_CHECK_EQUAL(    1, M6(0,0)(0,0) );
  BOOST_CHECK_EQUAL(   42, M6(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 1929, M6(1,0)(0,0) );
  BOOST_CHECK_EQUAL(    2, M6(1,1)(0,0) );

  MatrixD<MatrixS1> M7 = M3;

  BOOST_CHECK_EQUAL( 4, M7.size() );
  BOOST_CHECK_EQUAL( 2, M7.m() );
  BOOST_CHECK_EQUAL( 2, M7.n() );
  BOOST_CHECK_EQUAL( 2, M7.stride() );

  BOOST_CHECK_EQUAL(    1, M7(0,0)(0,0) );
  BOOST_CHECK_EQUAL(   42, M7(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 1929, M7(1,0)(0,0) );
  BOOST_CHECK_EQUAL(    2, M7(1,1)(0,0) );

  M7 = Identity();

  BOOST_CHECK_EQUAL(  1, M7(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  0, M7(0,1)(0,0) );
  BOOST_CHECK_EQUAL(  0, M7(1,0)(0,0) );
  BOOST_CHECK_EQUAL(  1, M7(1,1)(0,0) );

  MatrixD<MatrixS1> M8(2,3);
  M8 = Identity();

  BOOST_CHECK_EQUAL( 1, M8(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 0, M8(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 0, M8(0,2)(0,0) );
  BOOST_CHECK_EQUAL( 0, M8(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 1, M8(1,1)(0,0) );
  BOOST_CHECK_EQUAL( 0, M8(1,2)(0,0) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_MatrixS1_Assign )
{
  MatrixD<MatrixS1> M1(2, 2);
  M1 = 0;
  M1(0,0) = 1;
  M1(1,0) = 3;
  M1(1,1) = 2;

  MatrixD<MatrixS1> M2(2, 2);

  M2 = M1;

  BOOST_CHECK_EQUAL( 1, M2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 0, M2(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 3, M2(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 2, M2(1,1)(0,0) );

  M2 = -M1;

  BOOST_CHECK_EQUAL( -1, M2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  0, M2(0,1)(0,0) );
  BOOST_CHECK_EQUAL( -3, M2(1,0)(0,0) );
  BOOST_CHECK_EQUAL( -2, M2(1,1)(0,0) );

  //Make sure that an exception is thrown when dimensions do not match
  MatrixD<MatrixS1> M3(2, 3);

  BOOST_CHECK_THROW( M3 = M1, AssertionException );

  MatrixD<MatrixS1> M4(3, 2);

  BOOST_CHECK_THROW( M4 = M1, AssertionException );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_MatrixS1_CompoundAssign )
{
  MatrixD<MatrixS1> M1(2, 2), M2(2, 2);
  M1 = 0;
  M1(0,0) = 1;
  M1(1,0) = 3;
  M1(1,1) = 2;

  M2 = 0;
  M2(0,0) = 3;
  M2(0,1) = 1;
  M2(1,0) = 2;
  M2(1,1) = 4;

  M2 += M1;

  BOOST_CHECK_EQUAL( 1, M1(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 0, M1(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 3, M1(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 2, M1(1,1)(0,0) );

  BOOST_CHECK_EQUAL( 4, M2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 1, M2(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 5, M2(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 6, M2(1,1)(0,0) );

  M1 -= M2;

  BOOST_CHECK_EQUAL( -3, M1(0,0)(0,0) );
  BOOST_CHECK_EQUAL( -1, M1(0,1)(0,0) );
  BOOST_CHECK_EQUAL( -2, M1(1,0)(0,0) );
  BOOST_CHECK_EQUAL( -4, M1(1,1)(0,0) );

  BOOST_CHECK_EQUAL( 4, M2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 1, M2(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 5, M2(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 6, M2(1,1)(0,0) );

  M2 *= 2;

  BOOST_CHECK_EQUAL(  8, M2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  2, M2(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 10, M2(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 12, M2(1,1)(0,0) );

  M2 /= 2;

  BOOST_CHECK_EQUAL( 4, M2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 1, M2(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 5, M2(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 6, M2(1,1)(0,0) );

  +M2; //This should do nothing

  BOOST_CHECK_EQUAL( 4, M2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 1, M2(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 5, M2(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 6, M2(1,1)(0,0) );


  //Make sure that an exception is thrown when dimensions do not match
  MatrixD<MatrixS1> M3(2, 3);
  MatrixD<MatrixS1> M4(3, 2);

  BOOST_CHECK_THROW( M3 += M1, AssertionException );
  BOOST_CHECK_THROW( M4 += M1, AssertionException );

  BOOST_CHECK_THROW( M3 -= M1, AssertionException );
  BOOST_CHECK_THROW( M4 -= M1, AssertionException );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_MatrixS1_Add )
{
  MatrixD<MatrixS1> M1(2, 2), M2(2, 2), M3(2, 2);
  M1 = 0;
  M1(0,0) = 1;
  M1(1,0) = 3;
  M1(1,1) = 2;

  M2 = 0;
  M2(0,0) = 3;
  M2(0,1) = 1;
  M2(1,0) = 2;
  M2(1,1) = 4;

  M3 = M2 + M1;

  BOOST_CHECK_EQUAL( 1, M1(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 0, M1(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 3, M1(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 2, M1(1,1)(0,0) );

  BOOST_CHECK_EQUAL( 3, M2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 1, M2(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 2, M2(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 4, M2(1,1)(0,0) );

  BOOST_CHECK_EQUAL( 4, M3(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 1, M3(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 5, M3(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 6, M3(1,1)(0,0) );

  M3 += M2 + M1;

  BOOST_CHECK_EQUAL( 2*4, M3(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 2*1, M3(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 2*5, M3(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 2*6, M3(1,1)(0,0) );

  M3 = M1 - M2;

  BOOST_CHECK_EQUAL( -2, M3(0,0)(0,0) );
  BOOST_CHECK_EQUAL( -1, M3(0,1)(0,0) );
  BOOST_CHECK_EQUAL(  1, M3(1,0)(0,0) );
  BOOST_CHECK_EQUAL( -2, M3(1,1)(0,0) );

  M3 += M1 - M2;

  BOOST_CHECK_EQUAL( -2*2, M3(0,0)(0,0) );
  BOOST_CHECK_EQUAL( -1*2, M3(0,1)(0,0) );
  BOOST_CHECK_EQUAL(  1*2, M3(1,0)(0,0) );
  BOOST_CHECK_EQUAL( -2*2, M3(1,1)(0,0) );

  M3 -= M1 - M2;

  BOOST_CHECK_EQUAL( -2, M3(0,0)(0,0) );
  BOOST_CHECK_EQUAL( -1, M3(0,1)(0,0) );
  BOOST_CHECK_EQUAL(  1, M3(1,0)(0,0) );
  BOOST_CHECK_EQUAL( -2, M3(1,1)(0,0) );

  M1(0,0) = 1;
  M1(0,1) = 0;
  M1(1,0) = 3;
  M1(1,1) = 2;

  M2(0,0) = 3;
  M2(0,1) = 1;
  M2(1,0) = 2;
  M2(1,1) = 4;

  M3(0,0) = 4;
  M3(0,1) = 3;
  M3(1,0) = 2;
  M3(1,1) = 1;

  MatrixD<MatrixS1> M4(2, 2);

  M4 = M3 + M2 + M1;

  BOOST_CHECK_EQUAL( 8, M4(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 4, M4(0,1)(0,0) );
  BOOST_CHECK_EQUAL( 7, M4(1,0)(0,0) );
  BOOST_CHECK_EQUAL( 7, M4(1,1)(0,0) );

  M4 = M3 - M2 + M1;

  BOOST_CHECK_EQUAL(  2, M4(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  2, M4(0,1)(0,0) );
  BOOST_CHECK_EQUAL(  3, M4(1,0)(0,0) );
  BOOST_CHECK_EQUAL( -1, M4(1,1)(0,0) );

  M4 = M3 + M2 - M1;

  BOOST_CHECK_EQUAL(  6, M4(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  4, M4(0,1)(0,0) );
  BOOST_CHECK_EQUAL(  1, M4(1,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, M4(1,1)(0,0) );

  //Make sure that an exception is thrown when dimensions do not match
  MatrixD<MatrixS1> M23(2, 3);
  MatrixD<MatrixS1> M32(3, 2);

  BOOST_CHECK_THROW( M23 =  M2 +  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 = M23 +  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 =  M1 + M23, AssertionException );

  BOOST_CHECK_THROW( M32 =  M2 +  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 = M32 +  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 =  M1 + M32, AssertionException );

  BOOST_CHECK_THROW( M23 =  M2 -  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 = M23 -  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 =  M1 - M23, AssertionException );

  BOOST_CHECK_THROW( M32 =  M2 -  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 = M32 -  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 =  M1 - M32, AssertionException );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_MatrixS1_ops1 )
{
  const Real data = 3;
  MatrixD<MatrixS1> m1(1, 1);
  MatrixD<MatrixS1> m2(1, 1);
  MatrixD<MatrixS1> m3(1, 1), m4(1, 1), m5(1, 1);

  m1 = data;
  m2 = data;

  // ctors
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );

  // assignment
  m3 = m1;
  m4 = data;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m4(0,0)(0,0) );

  m1 = m2 = m3 = data;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m3(0,0)(0,0) );

  m4 = data;
  m1 = m2 = m3 = m4;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m4(0,0)(0,0) );

  // unary
  m2 = +m1;
  m3 = -m1;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( -3, m3(0,0)(0,0) );

  // binary accumulation
  m3 = m1;
  m3 *= data;
  BOOST_CHECK_EQUAL(  9, m3(0,0)(0,0) );

  m3 = data;
  m3 *= m1;
  BOOST_CHECK_EQUAL(  9, m3(0,0)(0,0) );

  m1 = data;
  m2 = m1;
  m3 = m1;
  m2 += m1;
  m3 -= m1;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  6, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0)(0,0) );

  // binary operators
  m1 = data;
  //m2 = m1 + data;
  //m3 = m1 - data;
  m4 = m1 * data;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  //BOOST_CHECK_EQUAL(  6, m2(0,0) );
  //BOOST_CHECK_EQUAL(  0, m3(0,0) );
  BOOST_CHECK_EQUAL(  9, m4(0,0)(0,0) );

  //m2 = data + m1;
  //m3 = data - m1;
  m4 = data * m1;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  //BOOST_CHECK_EQUAL(  6, m2(0,0) );
  //BOOST_CHECK_EQUAL(  0, m3(0,0) );
  BOOST_CHECK_EQUAL(  9, m4(0,0)(0,0) );

  m1 = m2 = data;
  m3 = m1 + m2;
  m4 = m1 - m2;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  6, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  0, m4(0,0)(0,0) );

  // arithmetic combinations

  m1 = m2 = data;
  m3 = m1 + m2;
  m4 = m1 + m2 + m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  6, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 12, m4(0,0)(0,0) );

  m2 += m1;
  m3 += m1 + m2;
  m4 += m1 + m2 + m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  6, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 15, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 36, m4(0,0)(0,0) );

  m3 = m1 - m2;
  m4 = m1 - m2 - m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  6, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( -3, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  0, m4(0,0)(0,0) );

  m2 -= m1;
  m3 -= m1 - m2;
  m4 -= m1 - m2 - m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( -3, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL( -3, m4(0,0)(0,0) );

  m3 = m1 - m2;
  m4 = m1 + m2 - m3;
  m5 = m1 - m2 + m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  6, m4(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  0, m5(0,0)(0,0) );

  m5 = (m1 + m2) + (m3 + m4);
  BOOST_CHECK_EQUAL( 12, m5(0,0)(0,0) );
  m5 = (m1 + m2) + (m3 - m4);
  BOOST_CHECK_EQUAL(  0, m5(0,0)(0,0) );
  m5 = (m1 + m2) - (m3 + m4);
  BOOST_CHECK_EQUAL(  0, m5(0,0)(0,0) );
  m5 = (m1 + m2) - (m3 - m4);
  BOOST_CHECK_EQUAL( 12, m5(0,0)(0,0) );
  m5 = (m1 - m2) + (m3 + m4);
  BOOST_CHECK_EQUAL(  6, m5(0,0)(0,0) );
  m5 = (m1 - m2) + (m3 - m4);
  BOOST_CHECK_EQUAL( -6, m5(0,0)(0,0) );
  m5 = (m1 - m2) - (m3 + m4);
  BOOST_CHECK_EQUAL( -6, m5(0,0)(0,0) );
  m5 = (m1 - m2) - (m3 - m4);
  BOOST_CHECK_EQUAL(  6, m5(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  6, m4(0,0)(0,0) );

  m5 += (m1 + m2) + (m3 + m4);
  m5 += (m1 + m2) + (m3 - m4);
  m5 += (m1 + m2) - (m3 + m4);
  m5 += (m1 + m2) - (m3 - m4);
  m5 += (m1 - m2) + (m3 + m4);
  m5 += (m1 - m2) + (m3 - m4);
  m5 += (m1 - m2) - (m3 + m4);
  m5 += (m1 - m2) - (m3 - m4);
  BOOST_CHECK_EQUAL( 30, m5(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  6, m4(0,0)(0,0) );

  m5 -= (m1 + m2) + (m3 + m4);
  m5 -= (m1 + m2) + (m3 - m4);
  m5 -= (m1 + m2) - (m3 + m4);
  m5 -= (m1 + m2) - (m3 - m4);
  m5 -= (m1 - m2) + (m3 + m4);
  m5 -= (m1 - m2) + (m3 - m4);
  m5 -= (m1 - m2) - (m3 + m4);
  m5 -= (m1 - m2) - (m3 - m4);
  BOOST_CHECK_EQUAL(  6, m5(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  6, m4(0,0)(0,0) );

  m1 = data;

  m2 = 4*m1;
  m3 = m2*7;
  BOOST_CHECK_EQUAL(   3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  12, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  84, m3(0,0)(0,0) );

  m2 = m3/7;
  m1 = m2/4;
  BOOST_CHECK_EQUAL(   3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  12, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  84, m3(0,0)(0,0) );

  m2 += 4*m1;
  m3 += m2*7;
  BOOST_CHECK_EQUAL(   3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  24, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 252, m3(0,0)(0,0) );

  m2 -= 4*m1;
  m3 -= m2*7;
  BOOST_CHECK_EQUAL(   3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  12, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 168, m3(0,0)(0,0) );

  m5 = 2*(m1 + m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL(  552, m5(0,0)(0,0) );
  m5 = 2*(m1 + m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  516, m5(0,0)(0,0) );
  m5 = 2*(m1 + m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -492, m5(0,0)(0,0) );
  m5 = 2*(m1 + m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -456, m5(0,0)(0,0) );
  m5 = 2*(m1 - m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL(  504, m5(0,0)(0,0) );
  m5 = 2*(m1 - m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  468, m5(0,0)(0,0) );
  m5 = 2*(m1 - m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -540, m5(0,0)(0,0) );
  m5 = 2*(m1 - m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -504, m5(0,0)(0,0) );
  BOOST_CHECK_EQUAL(    3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(   12, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  168, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL(    6, m4(0,0)(0,0) );

  m5 += 2*(m1 + m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL(   48, m5(0,0)(0,0) );
  m5 += 2*(m1 + m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  564, m5(0,0)(0,0) );
  m5 += 2*(m1 + m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL(   72, m5(0,0)(0,0) );
  m5 += 2*(m1 + m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -384, m5(0,0)(0,0) );
  m5 += 2*(m1 - m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL(  120, m5(0,0)(0,0) );
  m5 += 2*(m1 - m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  588, m5(0,0)(0,0) );
  m5 += 2*(m1 - m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL(   48, m5(0,0)(0,0) );
  m5 += 2*(m1 - m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -456, m5(0,0)(0,0) );
  BOOST_CHECK_EQUAL(    3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(   12, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  168, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL(    6, m4(0,0)(0,0) );

  m5 -= 2*(m1 + m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -1008, m5(0,0)(0,0) );
  m5 -= 2*(m1 + m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -1524, m5(0,0)(0,0) );
  m5 -= 2*(m1 + m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -1032, m5(0,0)(0,0) );
  m5 -= 2*(m1 + m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  -576, m5(0,0)(0,0) );
  m5 -= 2*(m1 - m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -1080, m5(0,0)(0,0) );
  m5 -= 2*(m1 - m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -1548, m5(0,0)(0,0) );
  m5 -= 2*(m1 - m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -1008, m5(0,0)(0,0) );
  m5 -= 2*(m1 - m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  -504, m5(0,0)(0,0) );
  BOOST_CHECK_EQUAL(     3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(    12, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL(   168, m3(0,0)(0,0) );
  BOOST_CHECK_EQUAL(     6, m4(0,0)(0,0) );

  m5 = 2*(m1 + m2)*3;
  BOOST_CHECK_EQUAL( 90, m5(0,0)(0,0) );
  m5 = 2*3*(m1 + m2);
  BOOST_CHECK_EQUAL( 90, m5(0,0)(0,0) );
  m5 = (m1 + m2)*2*3;
  BOOST_CHECK_EQUAL( 90, m5(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL( 12, m2(0,0)(0,0) );

  m2 = +m1;
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  m3 = -m2;
  BOOST_CHECK_EQUAL( -3, m3(0,0)(0,0) );
  m4 = +(m1 + m2);
  BOOST_CHECK_EQUAL(  6, m4(0,0)(0,0) );
  m4 = +(m1 - m2);
  BOOST_CHECK_EQUAL(  0, m4(0,0)(0,0) );
  m4 = -(m1 + m2);
  BOOST_CHECK_EQUAL( -6, m4(0,0)(0,0) );
  m4 = -(m1 - m2);
  BOOST_CHECK_EQUAL(  0, m4(0,0)(0,0) );
  m4 = +(m1 + m2) + m3;
  BOOST_CHECK_EQUAL(  3, m4(0,0)(0,0) );
  m4 = -(m1 + m2) + m3;
  BOOST_CHECK_EQUAL( -9, m4(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0)(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0)(0,0) );
  BOOST_CHECK_EQUAL( -3, m3(0,0)(0,0) );

  m4 = +5*m1;
  BOOST_CHECK_EQUAL(  15, m4(0,0)(0,0) );
  m4 = -5*m1;
  BOOST_CHECK_EQUAL( -15, m4(0,0)(0,0) );
  m4 = +m1*5;
  BOOST_CHECK_EQUAL(  15, m4(0,0)(0,0) );
  m4 = -m1*5;
  BOOST_CHECK_EQUAL( -15, m4(0,0)(0,0) );
  m4 = +(5*m1);
  BOOST_CHECK_EQUAL(  15, m4(0,0)(0,0) );
  m4 = -(5*m1);
  BOOST_CHECK_EQUAL( -15, m4(0,0)(0,0) );
  m4 = +(m1*5);
  BOOST_CHECK_EQUAL(  15, m4(0,0)(0,0) );
  m4 = -(m1*5);
  BOOST_CHECK_EQUAL( -15, m4(0,0)(0,0) );
  BOOST_CHECK_EQUAL(   3, m1(0,0)(0,0) );

}


//----------------------------------------------------------------------------//
bool chkMatrixD_MatrixS1_22( const MatrixD<MatrixS1>& z, int m11, int m12, int m21, int m22 )
{
  bool isEqual = true;
  if (z(0,0)(0,0) != m11) isEqual = false;
  if (z(0,1)(0,0) != m12) isEqual = false;
  if (z(1,0)(0,0) != m21) isEqual = false;
  if (z(1,1)(0,0) != m22) isEqual = false;
  if (!isEqual)
  {
    std::cout << "actual ("
              << "(" << z(0,0)(0,0) << " " << z(0,1)(0,0) << ") "
              << "(" << z(1,0)(0,0) << " " << z(1,1)(0,0) << ") "
              << ")" << std::endl;
    std::cout << "  expected ("
              << "(" << m11 << " " << m12 << ") "
              << "(" << m21 << " " << m22 << ") "
              << ")" << std::endl;
  }
  return isEqual;
}

//----------------------------------------------------------------------------//
bool chkMatrixD_MatrixS1_21( const MatrixD<MatrixS1>& z, int m11, int m21 )
{
  bool isEqual = true;
  if (z(0,0)(0,0) != m11) isEqual = false;
  if (z(1,0)(0,0) != m21) isEqual = false;
  if (!isEqual)
  {
    std::cout << "actual ("
              << "        (" << z(0,0)(0,0) << ") " << std::endl
              << "        (" << z(1,0)(0,0) << ") "
              << ")" << std::endl;
    std::cout << "  expected ("
              << "            (" << m11 << ") " << std::endl
              << "            (" << m21 << ") "
              << ")" << std::endl;
  }
  return isEqual;
}

//----------------------------------------------------------------------------//
bool chkMatrixD_MatrixS1_12( const MatrixD<MatrixS1>& z, int m11, int m12 )
{
  bool isEqual = true;
  if (z(0,0)(0,0) != m11) isEqual = false;
  if (z(0,1)(0,0) != m12) isEqual = false;
  if (!isEqual)
  {
    std::cout << "actual ("
              << "       b(" << z(0,0)(0,0) << ") " << std::endl
              << "        (" << z(0,1)(0,0) << ") "
              << ")" << std::endl;
    std::cout << "  expected ("
              << "            (" << m11 << ") " << std::endl
              << "            (" << m12 << ") "
              << ")" << std::endl;
  }
  return isEqual;
}

//----------------------------------------------------------------------------//
bool chkMatrixD_MatrixS2_22( const MatrixD<MatrixS2>& z, int m11, int m12, int m21, int m22 )
{
  bool isEqual = true;
  for (int k = 0; k < 2; k++)
  {
    if (z(0,0)(k,k) != m11) isEqual = false;
    if (z(0,1)(k,k) != m12) isEqual = false;
    if (z(1,0)(k,k) != m21) isEqual = false;
    if (z(1,1)(k,k) != m22) isEqual = false;
  }

  for (int i = 0; i < z.m(); i++)
    for (int j = 0; j < z.n(); j++)
      for (int k1 = 0; k1 < 2; k1++)
        for (int k2 = 0; k2 < 2; k2++)
          if ( (k1 != k2) && (z(i,j)(k1,k2) != 0) ) isEqual = false;

  if (!isEqual)
  {
    std::cout << "actual ("
              << "(" << z(0,0)(0,0) << " " << z(0,1)(0,0) << ") "
              << "(" << z(1,0)(0,0) << " " << z(1,1)(0,0) << ") "
              << ")" << std::endl;
    std::cout << "  expected ("
              << "(" << m11 << " " << m12 << ") "
              << "(" << m21 << " " << m22 << ") "
              << ")" << std::endl;
  }
  return isEqual;
}

//----------------------------------------------------------------------------//
bool chkMatrixD_MatrixS2_21( const MatrixD<MatrixS2>& z, int m11, int m21 )
{
  bool isEqual = true;
  for (int k = 0; k < 2; k++)
  {
    if (z(0,0)(k,k) != m11) isEqual = false;
    if (z(1,0)(k,k) != m21) isEqual = false;
  }

  for (int i = 0; i < z.m(); i++)
    for (int j = 0; j < z.n(); j++)
      for (int k1 = 0; k1 < 2; k1++)
        for (int k2 = 0; k2 < 2; k2++)
          if ( (k1 != k2) && (z(i,j)(k1,k2) != 0) ) isEqual = false;

  if (!isEqual)
  {
    std::cout << "actual ("
              << "        (" << z(0,0)(0,0) << ") " << std::endl
              << "        (" << z(1,0)(0,0) << ") "
              << ")" << std::endl;
    std::cout << "  expected ("
              << "            (" << m11 << ") " << std::endl
              << "            (" << m21 << ") "
              << ")" << std::endl;
  }
  return isEqual;
}

//----------------------------------------------------------------------------//
bool chkMatrixD_MatrixS2_12( const MatrixD<MatrixS2>& z, int m11, int m12 )
{
  bool isEqual = true;
  for (int k = 0; k < 2; k++)
  {
    if (z(0,0)(k,k) != m11) isEqual = false;
    if (z(0,1)(k,k) != m12) isEqual = false;
  }

  for (int i = 0; i < z.m(); i++)
    for (int j = 0; j < z.n(); j++)
      for (int k1 = 0; k1 < 2; k1++)
        for (int k2 = 0; k2 < 2; k2++)
          if ( (k1 != k2) && (z(i,j)(k1,k2) != 0) ) isEqual = false;

  if (!isEqual)
  {
    std::cout << "actual ("
              << "       b(" << z(0,0)(0,0) << ") " << std::endl
              << "        (" << z(0,1)(0,0) << ") "
              << ")" << std::endl;
    std::cout << "  expected ("
              << "            (" << m11 << ") " << std::endl
              << "            (" << m12 << ") "
              << ")" << std::endl;
  }
  return isEqual;
}

//----------------------------------------------------------------------------//
bool chkMatrixD_VectorS2( const MatrixD<VectorS2>& z, int m11, int m21 )
{
  bool isEqual = true;
  for (int k = 0; k < 2; k++)
  {
    if (z(0,0)[k] != m11) isEqual = false;
    if (z(1,0)[k] != m21) isEqual = false;
  }

  if (!isEqual)
  {
    std::cout << "actual ("
              << "        (" << z(0,0)[0] << ") " << std::endl
              << "        (" << z(1,0)[0] << ") "
              << ")" << std::endl;
    std::cout << "  expected ("
              << "            (" << m11 << ") " << std::endl
              << "            (" << m21 << ") "
              << ")" << std::endl;
  }
  return isEqual;
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_MatrixS1_ops2 )
{

  MatrixD<MatrixS1> m1(2, 2);
  MatrixD<MatrixS1> m2(2, 2);
  MatrixD<MatrixS1> m3(2, 2), m4(2, 2), m5(2, 2);

  m1(0,0) = 1; m1(0,1) = 2;
  m1(1,0) = 3; m1(1,1) = 4;

  m2 = m1;

  // ctors
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 1,2,3,4 ) );

  // assignment
  m3 = m1;
  m4 = 5;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, 5,5,5,5 ) );

  m2 = m3 = 3;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 3,3,3,3 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, 3,3,3,3 ) );

//  m3 = m2 = {1, 2,
//             3, 4};
//  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 1,2,3,4 ) );
//  BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, 1,2,3,4 ) );

  // unary
  m2 = +m1;
  m3 = -m1;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, -1,-2,-3,-4 ) );

  // binary accumulation
  m4 = m1;
  m4 *= 5;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, 5,10,15,20 ) );

  m2 = 5;
  m3 = 5;
  m2 += m1;
  m3 -= m1;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 6,7,8,9 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, 4,3,2,1 ) );

  // binary operators
  //m2 = m1 + 3;
  //m3 = m1 - 3;
  m4 = m1 * 3;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  //BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 4,5,6,7 ) );
  //BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, -2,-1,0,1 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, 3,6,9,12 ) );

  //m2 = 3 + m1;
  //m3 = 3 - m1;
  m4 = 3 * m1;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  //BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 4,5,6,7 ) );
  //BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, 2,1,0,-1 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, 3,6,9,12 ) );

  m2 = 3;
  m3 = m1 + m2;
  m4 = m1 - m2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 3,3,3,3 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, 4,5,6,7 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, -2,-1,0,1 ) );

  // arithmetic combinations

  m2 = m1;
  m3 = m1 + m2;
  m4 = m1 + m2 + m3;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, 4,8,12,16 ) );

  m2 += m1;
  m3 += m1 + m2;
  m4 += m1 + m2 + m3;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, 5,10,15,20 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, 12,24,36,48 ) );

  m3 = m1 - m2;
  m4 = m1 - m2 - m3;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, -1,-2,-3,-4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, 0,0,0,0 ) );

  m2 -= m1;
  m3 -= m1 - m2;
  m4 -= m1 - m2 - m3;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, -1,-2,-3,-4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, -1,-2,-3,-4 ) );

  m3 = m1 - m2;
  m4 = m1 + m2 - m3;
  m5 = m1 - m2 + m3;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 0,0,0,0 ) );

  m5 = (m1 + m2) + (m3 + m4);
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 4,8,12,16 ) );
  m5 = (m1 + m2) + (m3 - m4);
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 0,0,0,0 ) );
  m5 = (m1 + m2) - (m3 + m4);
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 0,0,0,0 ) );
  m5 = (m1 + m2) - (m3 - m4);
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 4,8,12,16 ) );
  m5 = (m1 - m2) + (m3 + m4);
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 2,4,6,8 ) );
  m5 = (m1 - m2) + (m3 - m4);
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, -2,-4,-6,-8 ) );
  m5 = (m1 - m2) - (m3 + m4);
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, -2,-4,-6,-8 ) );
  m5 = (m1 - m2) - (m3 - m4);
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, 2,4,6,8 ) );

  m5 += (m1 + m2) + (m3 + m4);
  m5 += (m1 + m2) + (m3 - m4);
  m5 += (m1 + m2) - (m3 + m4);
  m5 += (m1 + m2) - (m3 - m4);
  m5 += (m1 - m2) + (m3 + m4);
  m5 += (m1 - m2) + (m3 - m4);
  m5 += (m1 - m2) - (m3 + m4);
  m5 += (m1 - m2) - (m3 - m4);
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 10,20,30,40 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, 2,4,6,8 ) );

  m5 -= (m1 + m2) + (m3 + m4);
  m5 -= (m1 + m2) + (m3 - m4);
  m5 -= (m1 + m2) - (m3 + m4);
  m5 -= (m1 + m2) - (m3 - m4);
  m5 -= (m1 - m2) + (m3 + m4);
  m5 -= (m1 - m2) + (m3 - m4);
  m5 -= (m1 - m2) - (m3 + m4);
  m5 -= (m1 - m2) - (m3 - m4);
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, 2,4,6,8 ) );

  m2 = 1*m1;
  m3 = m2*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, 2,4,6,8 ) );

  m2 += 1*m1;
  m3 += m2*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, 6,12,18,24 ) );

  m2 -= 1*m1;
  m3 -= m2*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, 4,8,12,16 ) );

  m5 = 1*(m1 + m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 14,28,42,56 ) );
  m5 = 1*(m1 + m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 6,12,18,24 ) );
  m5 = 1*(m1 + m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, -10,-20,-30,-40 ) );
  m5 = 1*(m1 + m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, -2,-4,-6,-8 ) );
  m5 = 1*(m1 - m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 12,24,36,48 ) );
  m5 = 1*(m1 - m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 4,8,12,16 ) );
  m5 = 1*(m1 - m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, -12,-24,-36,-48 ) );
  m5 = 1*(m1 - m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, -4,-8,-12,-16 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, 2,4,6,8 ) );

  m5 += 1*(m1 + m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 10,20,30,40 ) );
  m5 += 1*(m1 + m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 16,32,48,64 ) );
  m5 += 1*(m1 + m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 6,12,18,24 ) );
  m5 += 1*(m1 + m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 4,8,12,16 ) );
  m5 += 1*(m1 - m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 16,32,48,64 ) );
  m5 += 1*(m1 - m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 20,40,60,80 ) );
  m5 += 1*(m1 - m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 8,16,24,32 ) );
  m5 += 1*(m1 - m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, 2,4,6,8 ) );

  m5 -= 1*(m1 + m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, -10,-20,-30,-40 ) );
  m5 -= 1*(m1 + m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, -16,-32,-48,-64 ) );
  m5 -= 1*(m1 + m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, -6,-12,-18,-24 ) );
  m5 -= 1*(m1 + m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, -4,-8,-12,-16 ) );
  m5 -= 1*(m1 - m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, -16,-32,-48,-64 ) );
  m5 -= 1*(m1 - m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, -20,-40,-60,-80 ) );
  m5 -= 1*(m1 - m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, -8,-16,-24,-32 ) );
  m5 -= 1*(m1 - m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, -4,-8,-12,-16 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, 2,4,6,8 ) );

  m5 = 1*(m1 + m2)*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 4,8,12,16 ) );
  m5 = 1*2*(m1 + m2);
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 4,8,12,16 ) );
  m5 = (m1 + m2)*1*2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m5, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 1,2,3,4 ) );

  m2 = +m1;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 1,2,3,4 ) );
  m3 = -m2;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, -1,-2,-3,-4 ) );
  m4 = +(m1 + m2);
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, 2,4,6,8 ) );
  m4 = +(m1 - m2);
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, 0,0,0,0 ) );
  m4 = -(m1 + m2);
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, -2,-4,-6,-8 ) );
  m4 = -(m1 - m2);
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, 0,0,0,0 ) );
  m4 = +(m1 + m2) + m3;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, 1,2,3,4 ) );
  m4 = -(m1 + m2) + m3;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, -3,-6,-9,-12 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m3, -1,-2,-3,-4 ) );

  m4 = +1*m1;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, 1,2,3,4 ) );
  m4 = -1*m1;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, -1,-2,-3,-4 ) );
  m4 = +m1*1;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, 1,2,3,4 ) );
  m4 = -m1*1;
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, -1,-2,-3,-4 ) );
  m4 = +(1*m1);
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, 1,2,3,4 ) );
  m4 = -(1*m1);
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, -1,-2,-3,-4 ) );
  m4 = +(m1*1);
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, 1,2,3,4 ) );
  m4 = -(m1*1);
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m4, -1,-2,-3,-4 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 1,2,3,4 ) );
}


//----------------------------------------------------------------------------//
// matrix-vector multiply
BOOST_AUTO_TEST_CASE( MatrixD_MatrixS1_Vector_Multiply2 )
{
  MatrixD<MatrixS1> m1(2, 2);
  MatrixD<MatrixS1> m2(2, 2);

  MatrixD<MatrixS1> col1(2, 1);
  MatrixD<MatrixS1> col2(2, 1);
  MatrixD<MatrixS1> col3(2, 1);

  MatrixD<MatrixS1> row1(1, 2);
  MatrixD<MatrixS1> row2(1, 2);
  MatrixD<MatrixS1> row3(1, 2);

  m1(0,0) = 3; m1(0,1) = 4;
  m1(1,0) = 5; m1(1,1) = 6;

  m2 = m1;

  col1(0,0) = 1;
  col1(1,0) = 2;

  col2 = col1;

  row1(0,0) = 1; row1(0,1) = 2;

  //Check column multiplication

  BOOST_CHECK( chkMatrixD_MatrixS1_21( col1, 1,2 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_21( col2, 1,2 ) );

  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 3,4,5,6 ) );

  col2 = m1*col1;
  BOOST_CHECK( chkMatrixD_MatrixS1_21( col2, 11,17 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 3,4,5,6 ) );

  col2 += m1*col1;
  BOOST_CHECK( chkMatrixD_MatrixS1_21( col2, 22,34 ) );

  col2 -= m1*col1;
  BOOST_CHECK( chkMatrixD_MatrixS1_21( col2, 11,17 ) );

  col2 = (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixD_MatrixS1_21( col2, 22,34 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 3,4,5,6 ) );

  col2 += (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixD_MatrixS1_21( col2, 2*22,2*34 ) );

  col2 -= (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixD_MatrixS1_21( col2, 22,34 ) );

  col2 = (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixD_MatrixS1_21( col2, 0,0 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 3,4,5,6 ) );


  m2(0,0) = 2; m2(0,1) = 3;
  m2(1,0) = 4; m2(1,1) = 5;

  col2(0,0) = 1;
  col2(1,0) = 2;

  col2 += (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixD_MatrixS1_21( col2, 4,5 ) );

  col2(0,0) = 7;
  col2(1,0) = 8;

  col2 -= (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixD_MatrixS1_21( col2, 4,5 ) );

  m2 = m1;
  col2 = col1;

  col3 = (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixD_MatrixS1_21( col3, 44,68 ) );

  col3 += (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixD_MatrixS1_21( col3, 2*44,2*68 ) );

  col3 -= (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixD_MatrixS1_21( col3, 44,68 ) );

  col3 = (m1 + m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixD_MatrixS1_21( col3, 0,0 ) );

  col3 = (m1 - m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixD_MatrixS1_21( col3, 0,0 ) );

  col3 = (m1 - m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixD_MatrixS1_21( col3, 0,0 ) );

  MatrixD<MatrixS1> col4 = (m1 - m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixD_MatrixS1_21( col4, 0,0 ) );

  //Check row multiplication
  row2 = row1*m1;
  BOOST_CHECK( chkMatrixD_MatrixS1_12( row2, 13,16 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 3,4,5,6 ) );

  row2 = row1*(m1 + m2);
  BOOST_CHECK( chkMatrixD_MatrixS1_12( row2, 26,32 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 3,4,5,6 ) );

  row2 += row1*(m1 + m2);
  BOOST_CHECK( chkMatrixD_MatrixS1_12( row2, 2*26,2*32 ) );

  row2 -= row1*(m1 + m2);
  BOOST_CHECK( chkMatrixD_MatrixS1_12( row2, 26,32 ) );

  row2 = row1*(m1 - m2);
  BOOST_CHECK( chkMatrixD_MatrixS1_12( row2, 0,0 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 3,4,5,6 ) );


  row2 = row1;

  row3 = (row1 + row2)*(m1 + m2);
  BOOST_CHECK( chkMatrixD_MatrixS1_12( row3, 52,64 ) );

  row3 = (row1 - row2)*(m1 + m2);
  BOOST_CHECK( chkMatrixD_MatrixS1_12( row3, 0,0 ) );

  row3 = (row1 + row2)*(m1 - m2);
  BOOST_CHECK( chkMatrixD_MatrixS1_12( row3, 0,0 ) );

  row3 = (row1 - row2)*(m1 - m2);
  BOOST_CHECK( chkMatrixD_MatrixS1_12( row3, 0,0 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_12( row1, 1,2 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_12( row2, 1,2 ) );

  BOOST_CHECK( chkMatrixD_MatrixS1_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixD_MatrixS1_22( m2, 3,4,5,6 ) );


  MatrixD<MatrixS1> val(1, 1);

  val = row1*col1;
  BOOST_CHECK_EQUAL( val(0,0)(0,0), 5 );

  val = row1*m1*col1;
  BOOST_CHECK_EQUAL( val(0,0)(0,0), 45 );

  //Make sure that an exception is thrown when dimensions do not match
  //or when the same veriable is used on the left and right as part of a multiplication

  BOOST_CHECK_THROW( row2  = m1*col1;, AssertionException );
  BOOST_CHECK_THROW( row2 += m1*col1;, AssertionException );
  BOOST_CHECK_THROW( row2 -= m1*col1;, AssertionException );

  BOOST_CHECK_THROW( col2  = row1*m1;, AssertionException );
  BOOST_CHECK_THROW( col2 += row1*m1;, AssertionException );
  BOOST_CHECK_THROW( col2 -= row1*m1;, AssertionException );

  BOOST_CHECK_THROW( row2  = col1*m1;, AssertionException );
  BOOST_CHECK_THROW( row2 += col1*m1;, AssertionException );
  BOOST_CHECK_THROW( row2 -= col1*m1;, AssertionException );

  BOOST_CHECK_THROW( col2  = m1*row1;, AssertionException );
  BOOST_CHECK_THROW( col2 += m1*row1;, AssertionException );
  BOOST_CHECK_THROW( col2 -= m1*row1;, AssertionException );

  BOOST_CHECK_THROW( m2  = m2*m1;, AssertionException );
  BOOST_CHECK_THROW( m2 += m2*m1;, AssertionException );
  BOOST_CHECK_THROW( m2 -= m2*m1;, AssertionException );

  BOOST_CHECK_THROW( m2  = m1*m2;, AssertionException );
  BOOST_CHECK_THROW( m2 += m1*m2;, AssertionException );
  BOOST_CHECK_THROW( m2 -= m1*m2;, AssertionException );

  BOOST_CHECK_THROW( col1  = m1*col1;, AssertionException );
  BOOST_CHECK_THROW( col1 += m1*col1;, AssertionException );
  BOOST_CHECK_THROW( col1 -= m1*col1;, AssertionException );

  BOOST_CHECK_THROW( row1  = row1*m1;, AssertionException );
  BOOST_CHECK_THROW( row1 += row1*m1;, AssertionException );
  BOOST_CHECK_THROW( row1 -= row1*m1;, AssertionException );

}

//----------------------------------------------------------------------------//
// matrix(of matrix)-vector(of matrix) multiply
BOOST_AUTO_TEST_CASE( MatrixD_MatrixS2_Vector_Multiply )
{
  MatrixD<MatrixS2> m1(2, 2);
  MatrixD<MatrixS2> m2(2, 2);

  MatrixD<MatrixS2> col1(2, 1);
  MatrixD<MatrixS2> col2(2, 1);
  MatrixD<MatrixS2> col3(2, 1);

  MatrixD<MatrixS2> row1(1, 2);
  MatrixD<MatrixS2> row2(1, 2);
  MatrixD<MatrixS2> row3(1, 2);

  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 2; j++)
    {
      m1(i,j) = 0;
      for (int k = 0; k < 2; k++)
        m1(i,j)(k,k) = 1;
    }

  m1(0,0) *= 3; m1(0,1) *= 4;
  m1(1,0) *= 5; m1(1,1) *= 6;

  m2 = m1;

  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 1; j++)
    {
      col1(i,j) = 0;
      for (int k = 0; k < 2; k++)
        col1(i,j)(k,k) = 1;
    }
  col1(0,0) *= 1;
  col1(1,0) *= 2;

  col2 = col1;

  for (int i = 0; i < 1; i++)
    for (int j = 0; j < 2; j++)
    {
      row1(i,j) = 0;
      for (int k = 0; k < 2; k++)
        row1(i,j)(k,k) = 1;
    }

  row1(0,0) *= 1; row1(0,1) *= 2;

  row2 = row1;

  //Check column multiplication

  BOOST_CHECK( chkMatrixD_MatrixS2_21( col1, 1,2 ) );
  BOOST_CHECK( chkMatrixD_MatrixS2_21( col2, 1,2 ) );

  BOOST_CHECK( chkMatrixD_MatrixS2_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixD_MatrixS2_22( m2, 3,4,5,6 ) );

  col2 = m1*col1;
  BOOST_CHECK( chkMatrixD_MatrixS2_21( col2, 11,17 ) );
  BOOST_CHECK( chkMatrixD_MatrixS2_22( m1, 3,4,5,6 ) );

  col2 += m1*col1;
  BOOST_CHECK( chkMatrixD_MatrixS2_21( col2, 22,34 ) );

  col2 -= m1*col1;
  BOOST_CHECK( chkMatrixD_MatrixS2_21( col2, 11,17 ) );

  col2 = (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixD_MatrixS2_21( col2, 22,34 ) );
  BOOST_CHECK( chkMatrixD_MatrixS2_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixD_MatrixS2_22( m2, 3,4,5,6 ) );

  col2 += (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixD_MatrixS2_21( col2, 2*22,2*34 ) );

  col2 -= (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixD_MatrixS2_21( col2, 22,34 ) );

  col2 = (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixD_MatrixS2_21( col2, 0,0 ) );
  BOOST_CHECK( chkMatrixD_MatrixS2_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixD_MatrixS2_22( m2, 3,4,5,6 ) );


  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 2; j++)
    {
      m2(i,j) = 0;
      for (int k = 0; k < 2; k++)
        m2(i,j)(k,k) = 1;
    }

  m2(0,0) *= 2; m2(0,1) *= 3;
  m2(1,0) *= 4; m2(1,1) *= 5;

  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 1; j++)
    {
      col2(i,j) = 0;
      for (int k = 0; k < 2; k++)
        col2(i,j)(k,k) = 1;
    }

  col2(0,0) *= 1;
  col2(1,0) *= 2;

  col2 += (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixD_MatrixS2_21( col2, 4,5 ) );

  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 1; j++)
    {
      col2(i,j) = 0;
      for (int k = 0; k < 2; k++)
        col2(i,j)(k,k) = 1;
    }

  col2(0,0) *= 7;
  col2(1,0) *= 8;

  col2 -= (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixD_MatrixS2_21( col2, 4,5 ) );

  m2 = m1;
  col2 = col1;

  col3 = (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixD_MatrixS2_21( col3, 44,68 ) );

  col3 += (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixD_MatrixS2_21( col3, 2*44,2*68 ) );

  col3 -= (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixD_MatrixS2_21( col3, 44,68 ) );

  col3 = (m1 + m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixD_MatrixS2_21( col3, 0,0 ) );

  col3 = (m1 - m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixD_MatrixS2_21( col3, 0,0 ) );

  col3 = (m1 - m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixD_MatrixS2_21( col3, 0,0 ) );

  MatrixD<MatrixS2> col4 = (m1 - m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixD_MatrixS2_21( col4, 0,0 ) );

  //Check row multiplication
  row2 = row1*m1;
  BOOST_CHECK( chkMatrixD_MatrixS2_12( row2, 13,16 ) );
  BOOST_CHECK( chkMatrixD_MatrixS2_22( m1, 3,4,5,6 ) );

  row2 = row1*(m1 + m2);
  BOOST_CHECK( chkMatrixD_MatrixS2_12( row2, 26,32 ) );
  BOOST_CHECK( chkMatrixD_MatrixS2_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixD_MatrixS2_22( m2, 3,4,5,6 ) );

  row2 += row1*(m1 + m2);
  BOOST_CHECK( chkMatrixD_MatrixS2_12( row2, 2*26,2*32 ) );

  row2 -= row1*(m1 + m2);
  BOOST_CHECK( chkMatrixD_MatrixS2_12( row2, 26,32 ) );

  row2 = row1*(m1 - m2);
  BOOST_CHECK( chkMatrixD_MatrixS2_12( row2, 0,0 ) );
  BOOST_CHECK( chkMatrixD_MatrixS2_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixD_MatrixS2_22( m2, 3,4,5,6 ) );


  row2 = row1;

  row3 = (row1 + row2)*(m1 + m2);
  BOOST_CHECK( chkMatrixD_MatrixS2_12( row3, 52,64 ) );

  row3 = (row1 - row2)*(m1 + m2);
  BOOST_CHECK( chkMatrixD_MatrixS2_12( row3, 0,0 ) );

  row3 = (row1 + row2)*(m1 - m2);
  BOOST_CHECK( chkMatrixD_MatrixS2_12( row3, 0,0 ) );

  row3 = (row1 - row2)*(m1 - m2);
  BOOST_CHECK( chkMatrixD_MatrixS2_12( row3, 0,0 ) );
  BOOST_CHECK( chkMatrixD_MatrixS2_12( row1, 1,2 ) );
  BOOST_CHECK( chkMatrixD_MatrixS2_12( row2, 1,2 ) );

  BOOST_CHECK( chkMatrixD_MatrixS2_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixD_MatrixS2_22( m2, 3,4,5,6 ) );


  MatrixD<MatrixS2> val(1, 1);

  val = row1*col1;
  BOOST_CHECK_EQUAL( val(0,0)(0,0), 5 );
  BOOST_CHECK_EQUAL( val(0,0)(0,1), 0 );
  BOOST_CHECK_EQUAL( val(0,0)(1,0), 0 );
  BOOST_CHECK_EQUAL( val(0,0)(1,1), 5 );

  val = row1*m1*col1;
  BOOST_CHECK_EQUAL( val(0,0)(0,0), 45 );
  BOOST_CHECK_EQUAL( val(0,0)(0,1), 0 );
  BOOST_CHECK_EQUAL( val(0,0)(1,0), 0 );
  BOOST_CHECK_EQUAL( val(0,0)(1,1), 45 );

}

//----------------------------------------------------------------------------//
// matrix(of matrix)-vector(of vecor) multiply
BOOST_AUTO_TEST_CASE( MatrixD_MatrixS2_VectorS2_Multiply )
{
  MatrixD<MatrixS2> m1(2, 2);
  MatrixD<MatrixS2> m2(2, 2);

  MatrixD<VectorS2> col1(2, 1);
  MatrixD<VectorS2> col2(2, 1);
  MatrixD<VectorS2> col3(2, 1);

  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 2; j++)
    {
      m1(i,j) = 0;
      for (int k = 0; k < 2; k++)
        m1(i,j)(k,k) = 1;
    }

  m1(0,0) *= 3; m1(0,1) *= 4;
  m1(1,0) *= 5; m1(1,1) *= 6;

  m2 = m1;

  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 1; j++)
    {
      col1(i,j) = 0;
      for (int k = 0; k < 2; k++)
        col1(i,j)[k] = 1;
    }

  col1(0,0) *= 1;
  col1(1,0) *= 2;

  col2 = col1;

  //Check column multiplication

  BOOST_CHECK( chkMatrixD_VectorS2( col1, 1,2 ) );
  BOOST_CHECK( chkMatrixD_VectorS2( col2, 1,2 ) );

  BOOST_CHECK( chkMatrixD_MatrixS2_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixD_MatrixS2_22( m2, 3,4,5,6 ) );

  col2 = m1*col1;
  BOOST_CHECK( chkMatrixD_VectorS2( col2, 11,17 ) );
  BOOST_CHECK( chkMatrixD_MatrixS2_22( m1, 3,4,5,6 ) );

  col2 += m1*col1;
  BOOST_CHECK( chkMatrixD_VectorS2( col2, 22,34 ) );

  col2 -= m1*col1;
  BOOST_CHECK( chkMatrixD_VectorS2( col2, 11,17 ) );

  col2 = (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixD_VectorS2( col2, 22,34 ) );
  BOOST_CHECK( chkMatrixD_MatrixS2_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixD_MatrixS2_22( m2, 3,4,5,6 ) );

  col2 += (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixD_VectorS2( col2, 2*22,2*34 ) );

  col2 -= (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixD_VectorS2( col2, 22,34 ) );

  col2 = (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixD_VectorS2( col2, 0,0 ) );
  BOOST_CHECK( chkMatrixD_MatrixS2_22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixD_MatrixS2_22( m2, 3,4,5,6 ) );


  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 2; j++)
    {
      m2(i,j) = 0;
      for (int k = 0; k < 2; k++)
        m2(i,j)(k,k) = 1;
    }

  m2(0,0) *= 2; m2(0,1) *= 3;
  m2(1,0) *= 4; m2(1,1) *= 5;

  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 1; j++)
    {
      col2(i,j) = 0;
      for (int k = 0; k < 2; k++)
        col2(i,j)[k] = 1;
    }

  col2(0,0) *= 1;
  col2(1,0) *= 2;

  col2 += (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixD_VectorS2( col2, 4,5 ) );

  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 1; j++)
    {
      col2(i,j) = 0;
      for (int k = 0; k < 2; k++)
        col2(i,j)[k] = 1;
    }

  col2(0,0) *= 7;
  col2(1,0) *= 8;

  col2 -= (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixD_VectorS2( col2, 4,5 ) );

  m2 = m1;
  col2 = col1;

  col3 = (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixD_VectorS2( col3, 44,68 ) );

  col3 += (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixD_VectorS2( col3, 2*44,2*68 ) );

  col3 -= (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixD_VectorS2( col3, 44,68 ) );

  col3 = (m1 + m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixD_VectorS2( col3, 0,0 ) );

  col3 = (m1 - m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixD_VectorS2( col3, 0,0 ) );

  col3 = (m1 - m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixD_VectorS2( col3, 0,0 ) );

  MatrixD<VectorS2> col4 = (m1 - m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixD_VectorS2( col4, 0,0 ) );

}

//----------------------------------------------------------------------------//
// matrix(of matrix)-vector(of vecor) multiply
BOOST_AUTO_TEST_CASE( MatrixD_MatrixS12_MatrixS21_Multiply )
{
  MatrixD<MatrixS<1,2,Real>> m1(2, 2);
  MatrixD<MatrixS<1,2,Real>> m2(2, 2);

  MatrixD<VectorS<2,Real>> col1(2, 1);
  MatrixD<Real> col2(2, 1);
  //MatrixD<DLA::MatrixS<2,1,Real>> col3(2, 1);

  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 2; j++)
    {
      m1(i,j) = 0;
      for (int k = 0; k < 2; k++)
        m1(i,j)(0,k) = 1;
    }

  m1(0,0) *= 3; m1(0,1) *= 4;
  m1(1,0) *= 5; m1(1,1) *= 6;

  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 1; j++)
    {
      col1(i,j) = 0;
      for (int k = 0; k < 2; k++)
        col1(i,j)[k] = 1;
    }

  col1(0,0) *= 1;
  col1(1,0) *= 2;

  //Check column multiplication

  BOOST_CHECK_EQUAL( 3, m1(0,0)(0,0) ); BOOST_CHECK_EQUAL( 3, m1(0,0)(0,1) );
  BOOST_CHECK_EQUAL( 4, m1(0,1)(0,0) ); BOOST_CHECK_EQUAL( 4, m1(0,1)(0,1) );

  BOOST_CHECK_EQUAL( 5, m1(1,0)(0,0) ); BOOST_CHECK_EQUAL( 5, m1(1,0)(0,1) );
  BOOST_CHECK_EQUAL( 6, m1(1,1)(0,0) ); BOOST_CHECK_EQUAL( 6, m1(1,1)(0,1) );

  BOOST_CHECK_EQUAL( 1, col1(0,0)[0] ); BOOST_CHECK_EQUAL( 1, col1(0,0)[1] );
  BOOST_CHECK_EQUAL( 2, col1(0,1)[0] ); BOOST_CHECK_EQUAL( 2, col1(0,1)[1] );

  col2 = m1*col1;
  BOOST_CHECK_EQUAL( 22, col2(0,0) );
  BOOST_CHECK_EQUAL( 34, col2(0,1) );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
