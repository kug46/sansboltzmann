// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include <iostream>
using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatMul_Native_1x1x1 )
{
  const int n = 1;
  MatrixD<Real> ML(n,n), MR(n,1), res(n,1);
  ML = 2;
  MR = 3;

  MatMul_Native<Real, Real, Real>::value(ML, MR, 1, res);
  BOOST_CHECK_EQUAL( 6, res(0,0) );

  MatMul_Native<Real, Real, Real>::plus(ML, MR, 1, res);
  BOOST_CHECK_EQUAL( 6*2, res(0,0) );

  MatMul_Native<Real, Real, Real>::plus(ML, MR, -1, res);
  BOOST_CHECK_EQUAL( 6, res(0,0) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatMul_Native_2x2x1 )
{
  const int n = 2;
  MatrixD<Real> ML(n,n), MR(n,1), res(n,1);
  ML(0,0) = 1; ML(0,1) = 2;
  ML(1,0) = 4; ML(1,1) = 3;

  MR(0,0) = 2;
  MR(1,0) = 3;

  MatMul_Native<Real, Real, Real>::value(ML, MR, 1, res);
  BOOST_CHECK_EQUAL(  8, res(0,0) );
  BOOST_CHECK_EQUAL( 17, res(1,0) );

  MatMul_Native<Real, Real, Real>::plus(ML, MR, 1, res);
  BOOST_CHECK_EQUAL(  8*2, res(0,0) );
  BOOST_CHECK_EQUAL( 17*2, res(1,0) );

  MatMul_Native<Real, Real, Real>::plus(ML, MR, -1, res);
  BOOST_CHECK_EQUAL(  8, res(0,0) );
  BOOST_CHECK_EQUAL( 17, res(1,0) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatMul_Native_4x4x1 )
{
  const int n = 4;
  MatrixD<Real> ML(n,n), MR(n,1), res(n,1);
  ML(0,0) = 1; ML(0,1) = 2; ML(0,2) = 3; ML(0,3) = 4;
  ML(1,0) = 8; ML(1,1) = 7; ML(1,2) = 6; ML(1,3) = 5;
  ML(2,0) = 4; ML(2,1) = 3; ML(2,2) = 2; ML(2,3) = 1;
  ML(3,0) = 5; ML(3,1) = 6; ML(3,2) = 7; ML(3,3) = 8;

  MR(0,0) = 2;
  MR(1,0) = 3;
  MR(2,0) = 4;
  MR(3,0) = 5;

  MatMul_Native<Real, Real, Real>::value(ML, MR, 1, res);
  BOOST_CHECK_EQUAL( 40, res(0,0) );
  BOOST_CHECK_EQUAL( 86, res(1,0) );
  BOOST_CHECK_EQUAL( 30, res(2,0) );
  BOOST_CHECK_EQUAL( 96, res(3,0) );

  MatMul_Native<Real, Real, Real>::plus(ML, MR, 1, res);
  BOOST_CHECK_EQUAL( 40*2, res(0,0) );
  BOOST_CHECK_EQUAL( 86*2, res(1,0) );
  BOOST_CHECK_EQUAL( 30*2, res(2,0) );
  BOOST_CHECK_EQUAL( 96*2, res(3,0) );

  MatMul_Native<Real, Real, Real>::plus(ML, MR, -1, res);
  BOOST_CHECK_EQUAL( 40, res(0,0) );
  BOOST_CHECK_EQUAL( 86, res(1,0) );
  BOOST_CHECK_EQUAL( 30, res(2,0) );
  BOOST_CHECK_EQUAL( 96, res(3,0) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatMul_Native_nxnxn )
{
  static const int CacheItems = CACHE_LINE_SIZE/sizeof(Real);

  for ( int n = CacheItems/2; n < 2*CacheItems; n++)
  {
    MatrixD<Real> ML(n,n), MR(n,n), res(n,n);

    ML = 1;
    MR = 1;

    MatMul_Native<Real, Real, Real>::value(ML, MR, 1, res);

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
        BOOST_CHECK_EQUAL( res(i,j), n );

    MatMul_Native<Real, Real, Real>::plus(ML, MR, 1, res);

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
        BOOST_CHECK_EQUAL( res(i,j), n*2 );

    MatMul_Native<Real, Real, Real>::plus(ML, MR, -1, res);

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
        BOOST_CHECK_EQUAL( res(i,j), n );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatMul_Native_nx2nxn )
{
  static const int CacheItems = CACHE_LINE_SIZE/sizeof(Real);

  for ( int n = CacheItems/2; n < CacheItems*2; n++)
  {
    MatrixD<Real> ML(n,2*n), MR(2*n,n), res(n,n);

    ML = 1;
    MR = 1;

    MatMul_Native<Real, Real, Real>::value(ML, MR, 1, res);

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
        BOOST_CHECK_EQUAL( res(i,j), 2*n );

    MatMul_Native<Real, Real, Real>::plus(ML, MR, 1, res);

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
        BOOST_CHECK_EQUAL( res(i,j), 2*n*2 );

    MatMul_Native<Real, Real, Real>::plus(ML, MR, -1, res);

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
        BOOST_CHECK_EQUAL( res(i,j), 2*n );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatMul_Native_2nxnx2n )
{
  static const int CacheItems = CACHE_LINE_SIZE/sizeof(Real);

  for ( int n = CacheItems/2; n < CacheItems*2; n++)
  {
    MatrixD<Real> ML(2*n,n), MR(n,2*n), res(2*n,2*n);

    ML = 1;
    MR = 1;

    MatMul_Native<Real, Real, Real>::value(ML, MR, 1, res);

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
        BOOST_CHECK_EQUAL( res(i,j), n );

    MatMul_Native<Real, Real, Real>::plus(ML, MR, 1, res);

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
        BOOST_CHECK_EQUAL( res(i,j), n*2 );

    MatMul_Native<Real, Real, Real>::plus(ML, MR, -1, res);

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
        BOOST_CHECK_EQUAL( res(i,j), n );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatMul_Native_nx3nxn )
{
  static const int CacheItems = CACHE_LINE_SIZE/sizeof(Real);

  for ( int n = CacheItems/2; n < CacheItems*2; n++)
  {
    MatrixD<Real> ML(n,3*n), MR(3*n,n), res(n,n);

    ML = 1;
    MR = 1;

    MatMul_Native<Real, Real, Real>::value(ML, MR, 1, res);

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
        BOOST_CHECK_EQUAL( res(i,j), 3*n );

    MatMul_Native<Real, Real, Real>::plus(ML, MR, 1, res);

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
        BOOST_CHECK_EQUAL( res(i,j), 3*n*2 );

    MatMul_Native<Real, Real, Real>::plus(ML, MR, -1, res);

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
        BOOST_CHECK_EQUAL( res(i,j), 3*n );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatMul_Native_MatrixS )
{
  MatrixD< MatrixS<2,2,Real> > m1(1,1);
  VectorD< VectorS<2,Real> > a1(1,1), a2(1,1);

  m1(0,0)(0,0) = 3; m1(0,0)(0,1) = 4;
  m1(0,0)(1,0) = 5; m1(0,0)(1,1) = 6;

  a1[0][0] = 1;
  a1[0][1] = 2;

  MatMul_Native<MatrixS<2,2,Real>, VectorS<2,Real>, VectorS<2,Real> >::value(m1, a1, 1, a2);

  BOOST_CHECK_EQUAL( a2[0][0], 11 );
  BOOST_CHECK_EQUAL( a2[0][1], 17 );

  MatrixD< MatrixS<4,4,Real> > m2(1,1), m3(1,1), m4(1,1), m4True(1,1);

  m2(0,0)(0,0) = 3; m2(0,0)(0,1) = 4; m2(0,0)(0,2) = 7; m2(0,0)(0,3) = -4;
  m2(0,0)(1,0) = 5; m2(0,0)(1,1) = 6; m2(0,0)(1,2) = -7; m2(0,0)(1,3) = 1;
  m2(0,0)(2,0) = -2; m2(0,0)(2,1) = 1; m2(0,0)(2,2) = -3; m2(0,0)(2,3) = 3;
  m2(0,0)(3,0) = 5; m2(0,0)(3,1) = -1; m2(0,0)(3,2) = 0; m2(0,0)(3,3) = -2;

  m3(0,0)(0,0) = 3; m3(0,0)(0,1) = 4; m3(0,0)(0,2) = 7; m3(0,0)(0,3) = -4;
  m3(0,0)(1,0) = 5; m3(0,0)(1,1) = 6; m3(0,0)(1,2) = -7; m3(0,0)(1,3) = 1;
  m3(0,0)(2,0) = 5; m3(0,0)(2,1) = 6; m3(0,0)(2,2) = -7; m3(0,0)(2,3) = 1;
  m3(0,0)(3,0) = 5; m3(0,0)(3,1) = 6; m3(0,0)(3,2) = -7; m3(0,0)(3,3) = 1;

  MatMul_Native<MatrixS<4,4,Real>, MatrixS<4,4,Real>, MatrixS<4,4,Real> >::value(m2, m3, 1, m4);

  m4True(0,0)(0,0) = 44; m4True(0,0)(0,1) = 54; m4True(0,0)(0,2) = -28; m4True(0,0)(0,3) = -5;
  m4True(0,0)(1,0) = 15; m4True(0,0)(1,1) = 20; m4True(0,0)(1,2) = 35; m4True(0,0)(1,3) = -20;
  m4True(0,0)(2,0) = -1; m4True(0,0)(2,1) = -2; m4True(0,0)(2,2) = -21; m4True(0,0)(2,3) = 9;
  m4True(0,0)(3,0) = 0; m4True(0,0)(3,1) = 2; m4True(0,0)(3,2) = 56; m4True(0,0)(3,3) = -23;


  for (int i=0; i<4; i++)
    for (int j=0; j<4; j++)
      BOOST_CHECK_EQUAL( m4(0,0)(i,j), m4True(0,0)(i,j) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatMul_Native_MatrixS21_VectorS )
{
  MatrixD< MatrixS<1,2,Real> > m1(1,1);
  VectorD< VectorS<2,Real> > a1(1,1);
  VectorD< Real > a2(1,1);

  m1(0,0)(0,0) = 3; m1(0,0)(0,1) = 4;

  a1[0][0] = 1;
  a1[0][1] = 2;

  MatMul_Native<MatrixS<1,2,Real>, VectorS<2,Real>, Real >::value(m1, a1, 1, a2);

  BOOST_CHECK_EQUAL( a2[0], 11 );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
