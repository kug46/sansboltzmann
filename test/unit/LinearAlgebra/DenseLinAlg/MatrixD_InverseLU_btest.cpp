// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"

#include <iostream>
using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_InverseLU_Exceptions )
{
  MatrixD<Real> A1(2,2), A1inv(2,2);
  A1 = 0;
  BOOST_CHECK_THROW( A1inv = InverseLU::Inverse(A1), SingularMatrixException );

  MatrixD<Real> A2(3,2), A2inv(2,2);
  A2 = Identity();
  BOOST_CHECK_THROW( A2inv = InverseLU::Inverse(A2), AssertionException );

  MatrixD<Real> A3(2,3), A3inv(2,2);
  A3 = Identity();
  BOOST_CHECK_THROW( A3inv = InverseLU::Inverse(A3), AssertionException );

  MatrixD<Real> A4inv(3,2);
  A1 = Identity();
  BOOST_CHECK_THROW( A4inv = InverseLU::Inverse(A1), AssertionException );

  MatrixD<Real> A5inv(2,3);
  A1 = Identity();
  BOOST_CHECK_THROW( A5inv = InverseLU::Inverse(A1), AssertionException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_InverseLU_Rand )
{
  typedef Real T;
  static const int CacheItems = CACHE_LINE_SIZE/sizeof(Real);

  for ( int n = 1; n <= 2*CacheItems + CacheItems/2; ++n)
  {
    MatrixD<T> A(n,n), tmp(n,n), I(n,n), Ainv(n,n);

    I = Identity();

    for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
        A(i,j) = T(rand() % 101/100.) + 5*I(i,j);

    tmp = A;

    Ainv = InverseLU::Inverse(A);

    I = Ainv*A;

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
      {
        BOOST_CHECK_CLOSE( A(i,j) , tmp(i,j), T(0.0001) );
        if (i == j)
          BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
        else
          BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
      }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_InverseLU_Rand_Expression )
{
  typedef Real T;
  static const int CacheItems = CACHE_LINE_SIZE/sizeof(Real);

  for ( int n = 1; n <= 2*CacheItems + CacheItems/2; ++n)
  {
    MatrixD<T> A(n,n), tmp(n,n), I(n,n), Ainv(n,n);

    I = Identity();

    for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
        A(i,j) = T(rand() % 101/100.);

    tmp = A + 5*I;

    Ainv = InverseLU::Inverse(A + 5*I);

    I = Ainv*tmp;

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
      {
        if (i == j)
          BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
        else
          BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
      }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveLU_1x1 )
{
  typedef Real T;
  MatrixD<T> A(1,1);
  MatrixD<T> b(1, 1);
  MatrixD<T> x(1, 1);

  A = 2;

  b = 3;

  x = InverseLU::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), T(2.)   , T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(3./2.), T(0.0001) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveLU_2x2 )
{
  typedef Real T;

  Real Avals[] = {2, 1,
                  3, 2};

  Real bvals[] = {2,
                  3};

  MatrixD<T> A(2,2,Avals);
  MatrixD<T> b(2,1,bvals);
  MatrixD<T> x(2,1);

  x = InverseLU::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(0.), T(0.0001) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveLU_2x1_2b )
{
  //Check that b = !A*b works properly
  typedef Real T;

  T Avals[] = {2, 1,
               3, 2};

  T bvals[] = {2,
               3};

  MatrixD<T> A(2,2,Avals);
  MatrixD<T> b(2,1, bvals);

  b = InverseLU::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( b(1,0),        T(1e-12) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveLU_2x2_RHSExpression )
{
  typedef Real T;

  Real Avals[] = {2, 1,
                  3, 2};

  Real bvals[] = {1,
                  2};

  MatrixD<T> A(2,2,Avals);
  MatrixD<T> b1(2,1,bvals), b2(2,1);
  MatrixD<T> x(2,1);

  b2 = 1;

  x = InverseLU::Solve( A, (b1 + b2) );

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(0.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveLU_2x2_InvExpression )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {2,
                  3};

  MatrixD<T> A1(2,2,Avals), A2(2,2,Avals);
  MatrixD<T> b(2,1,bvals);
  MatrixD<T> x(2,1);

  A2 = 1;

  x = InverseLU::Solve(A1 + A2, b);

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(0.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveLU_2x2_InvExpression_2b )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {2,
                  3};

  MatrixD<T> A1(2,2,Avals), A2(2,2,Avals);
  MatrixD<T> b(2,1,bvals);

  A2 = 1;

  b = InverseLU::Solve(A1 + A2, b);

  BOOST_CHECK_CLOSE( b(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(0.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveLU_2x2_Expressions )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {1,
                  2};

  MatrixD<T> A1(2,2,Avals), A2(2,2,Avals);
  MatrixD<T> b1(2,1,bvals), b2(2,1);
  MatrixD<T> x(2,1);

  A2 = 1;
  b2 = 1;

  x = InverseLU::Solve(A1 + A2, b1 + b2);

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(0.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveLU_2x2_Expressions_2 )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {1,
                  2};

  MatrixD<T> A1(2,2,Avals), A2(2,2,Avals);
  MatrixD<T> b1(2,1,bvals), b2(2,1);
  MatrixD<T> x(2,1);

  A2 = 1;
  b2 = 1;

  x = InverseLU::Solve(A1 + A2, b1 + b2) + b2;

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(1.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveLU_2x1_MulScal )
{
  //Check that b = !A*b works properly
  typedef Real T;

  T Avals[] = {2, 1,
               3, 2};

  T bvals[] = {2,
               3};

  MatrixD<T> A(2,2,Avals);
  MatrixD<T> b(2,1, bvals);
  MatrixD<T> x(2,1);

  x = 0; // cppcheck-suppress redundantAssignment
  x = 2*InverseLU::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = InverseLU::Solve(A, b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = InverseLU::Solve(A, b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -InverseLU::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-1.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -InverseLU::Solve(A, b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -InverseLU::Solve(A, b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = - 2 * InverseLU::Solve(A, b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-4.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveLU_3x3 )
{
  typedef Real T;

  Real Adata[] = {4, 2, 2,
                  2, 4, 6,
                  2, 6, 6};

  Real bdata[] = {1, 3, 2};

  MatrixD<T> A(3,3, Adata);
  MatrixD<T> b(3,1, bdata);


  MatrixD<T> x = InverseLU::Solve(A, b);

  BOOST_CHECK_CLOSE( x(0,0),   T(0.1), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0),  T(-0.5), T(0.0001) );
  BOOST_CHECK_CLOSE( x(2,0), T(4./5.), T(0.0001) );


  Real Bdata[] = {1, 2, 1,
                  2, 3, 1,
                  1, 1, 2};

  MatrixD<T> B(3,3, Bdata);

  MatrixD<T> X = InverseLU::Solve(A, B);

  BOOST_CHECK_CLOSE( X(0,0), T( 1./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(0,1), T( 1./2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(0,2), T(1./10.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,0), T(-1./2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,1), T(-1.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,2), T( 1./2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,0), T( 3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,1), T( 1.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,2), T(-1./5.), T(0.0001) );

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      BOOST_CHECK_CLOSE( B(i,j), Bdata[3*i+j], T(0.0001) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveLU_2x2_NonSymVec )
{
  typedef Real T;

  Real Adata[] = {1, 2,
                  5, 6};

  Real bdata[] = {2, 4, 5,
                  3, 2, 1};

  MatrixD<T> A(2,2, Adata);
  MatrixD<T> b(2,3, bdata);

  MatrixD<T> x = InverseLU::Solve(A, b);

  BOOST_CHECK_CLOSE( x(0,0), T(-1.50), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,1), T(-5.00), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,2), T(-7.00), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T( 1.75), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,1), T( 4.50), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,2), T( 6.00), T(0.0001) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveLU_2x2_MixedTypes )
{
  typedef Real T;

  MatrixD<T> A = {{2, 1},
                  {3, 2}};
  MatrixD< VectorS<1,T> > b = {{2},
                               {3}};
  MatrixD< VectorS<1,T> > x(2,1);

  x = InverseLU::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0)[0], T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0)[0], T(0.), T(0.0001) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveLU_2x2_MatrixS_1x1 )
{
  typedef Real T;

  MatrixD< MatrixS<1,1,T> > A = {{2, 1},
                                 {3, 2}};
  MatrixD< VectorS<1,T> > b = {{2},
                               {3}};
  MatrixD< VectorS<1,T> > x(2,1);

  x = InverseLU::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0)(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1)(0,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0)(0,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1)(0,0), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0)[0], T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0)[0], T(0.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveLU_2x2_MatrixS_2x2 )
{
  typedef Real T;

  MatrixS<2,2,T> A11 = {{5, 1},
                        {3, 7}};

  MatrixS<2,2,T> A12 = {{1, 4},
                        {2, 6}};

  MatrixS<2,2,T> A21 = {{1, 2},
                        {2, 1}};

  MatrixS<2,2,T> A22 = {{8, 5},
                        {4, 9}};

  VectorS<2,T> b1 = { 2, 4 };
  VectorS<2,T> b2 = { 3, 5 };

  MatrixD< MatrixS<2,2,T> > A = {{A11, A12},
                                 {A21, A22}};
  MatrixD< VectorS<2,T> > b = {{b1},
                               {b2}};
  MatrixD< VectorS<2,T> > x(2,1);

  MatrixD< MatrixS<2,2,T> > Ainv(2,2), I(2,2);

  Ainv = InverseLU::Inverse(A);

  I = Ainv*A;

  for (int i = 0; i < 2; ++i)
    for (int j = 0; j < 2; ++j)
    {
      if (i == j)
        for (int ii = 0; ii < 2; ++ii)
          for (int jj = 0; jj < 2; ++jj)
          {
            if (ii == jj)
              BOOST_CHECK_CLOSE( I(i,j)(ii,jj), T(1), T(1e-12) );
            else
              BOOST_CHECK_SMALL( I(i,j)(ii,jj), T(1e-12) );
          }
      else
        for (int ii = 0; ii < 2; ++ii)
          for (int jj = 0; jj < 2; ++jj)
            BOOST_CHECK_SMALL( I(i,j)(ii,jj), T(1e-12) );
    }


  x = InverseLU::Solve(A, b);

  //Results from Mathematica
  BOOST_CHECK_CLOSE( x(0,0)[0], T(-23./330.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,0)[1], T( 41./330.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(1,0)[0], T(  1./165.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0)[1], T( 61./110.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveLU_2x2_MatrixD_2x2 )
{
  typedef Real T;

  MatrixD<T> A11 = {{5, 1},
                    {3, 7}};

  MatrixD<T> A12 = {{1, 4},
                    {2, 6}};

  MatrixD<T> A21 = {{1, 2},
                    {2, 1}};

  MatrixD<T> A22 = {{8, 5},
                    {4, 9}};

  VectorD<T> b1 = { 2, 4 };
  VectorD<T> b2 = { 3, 5 };

  MatrixD< MatrixD<T> > A = {{A11, A12},
                             {A21, A22}};
  VectorD< VectorD<T> > b = {b1,
                             b2};
  VectorD< VectorD<T> > x(b1.size());

  MatrixD< MatrixD<T> > Ainv(A.size()), I(A.size());

  Ainv = InverseLU::Inverse(A);

  I = Ainv*A;

  for (int i = 0; i < 2; ++i)
    for (int j = 0; j < 2; ++j)
    {
      if (i == j)
        for (int ii = 0; ii < 2; ++ii)
          for (int jj = 0; jj < 2; ++jj)
          {
            if (ii == jj)
              BOOST_CHECK_CLOSE( I(i,j)(ii,jj), T(1), T(1e-12) );
            else
              BOOST_CHECK_SMALL( I(i,j)(ii,jj), T(1e-12) );
          }
      else
        for (int ii = 0; ii < 2; ++ii)
          for (int jj = 0; jj < 2; ++jj)
            BOOST_CHECK_SMALL( I(i,j)(ii,jj), T(1e-12) );
    }


  x = InverseLU::Solve(A, b);

  //Results from Mathematica
  BOOST_CHECK_CLOSE( x[0][0], T(-23./330.), T(0.0001) );
  BOOST_CHECK_CLOSE( x[0][1], T( 41./330.), T(0.0001) );

  BOOST_CHECK_CLOSE( x[1][0], T(  1./165.), T(0.0001) );
  BOOST_CHECK_CLOSE( x[1][1], T( 61./110.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveLU_2x2_MatrixD_2x2_ZeroSize )
{
  typedef MatrixD<Real> T;

  Real Avals[] = {2, 1,
                  3, 2};

  Real bvals[] = {2,
                  3};

  MatrixD<Real> A11(2,2,Avals);
  MatrixD<Real> A12(2,0);
  MatrixD<Real> A21(0,2);
  MatrixD<Real> A22(0,0);

  MatrixD<T> A(2,2);
  A(0,0) = A11;
  A(0,1) = A12;
  A(1,0) = A21;
  A(1,1) = A22;

  MatrixD<Real> b1(2,1,bvals);
  MatrixD<Real> b2(0,1);

  MatrixD<T> b(2,1);
  b(0,0) = b1;
  b(1,0) = b2;

  MatrixD<T> x(2,1);

  x = InverseLU::Solve(A, b);

  Real tol = 1e-12;

  BOOST_CHECK_CLOSE( A(0,0)(0,0), 2., tol );
  BOOST_CHECK_CLOSE( A(0,0)(0,1), 1., tol );
  BOOST_CHECK_CLOSE( A(0,0)(1,0), 3., tol );
  BOOST_CHECK_CLOSE( A(0,0)(1,1), 2., tol );

  BOOST_CHECK_CLOSE( x(0,0)(0,0), 1.0, tol );
  BOOST_CHECK_CLOSE( x(0,0)(1,0), 0.0, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_InverseLU_Rand_Factorize )
{
  typedef Real T;
  static const int CacheItems = CACHE_LINE_SIZE/sizeof(Real);

  for ( int n = 1; n <= 2*CacheItems + CacheItems/2; ++n)
  {
    MatrixD<T> A(n,n), tmp(n,n), I(n,n), Ainv(n,n);

    I = Identity();

    for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
        A(i,j) = T(rand() % 101/100.) + 5*I(i,j);

    tmp = A;

    auto LU = InverseLU::Factorize(A);
    Ainv = LU.backsolve(I);

    I = Ainv*A;

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
      {
        BOOST_CHECK_CLOSE( A(i,j) , tmp(i,j), T(0.0001) );
        if (i == j)
          BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
        else
          BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
      }

    I = Identity();

    // Solve the system a 2nd time and make sure it still works
    Ainv = 0;  // cppcheck-suppress redundantAssignment
    Ainv = LU.backsolve(I);

    I = Ainv*A;

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
      {
        BOOST_CHECK_CLOSE( A(i,j) , tmp(i,j), T(0.0001) );
        if (i == j)
          BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
        else
          BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
      }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_InverseLU_Rand_Expression_Factorize )
{
  typedef Real T;
  static const int CacheItems = CACHE_LINE_SIZE/sizeof(Real);

  for ( int n = 1; n <= 2*CacheItems + CacheItems/2; ++n)
  {
    MatrixD<T> A(n,n), tmp(n,n), I(n,n), Ainv(n,n);

    I = Identity();

    for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
        A(i,j) = T(rand() % 101/100.);

    tmp = A + 5*I;

    auto LU = InverseLU::Factorize(A + 5*I);
    Ainv = LU.backsolve(I);

    I = Ainv*tmp;

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
      {
        if (i == j)
          BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
        else
          BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
      }

    I = Identity();

    // Solve the system a 2nd time and make sure it still works
    Ainv = 0;  // cppcheck-suppress redundantAssignment
    Ainv = LU.backsolve(I);

    I = Ainv*tmp;

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
      {
        if (i == j)
          BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
        else
          BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
      }

    I = Identity();

    // Solve the system a 3rd time and make sure it still works
    Ainv = 0;  // cppcheck-suppress redundantAssignment
    Ainv = LU.backsolve(I - I*I + I);

    I = Ainv*tmp;

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
      {
        if (i == j)
          BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
        else
          BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
      }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeLU_1x1 )
{
  typedef Real T;
  MatrixD<T> A(1,1);
  MatrixD<T> b(1, 1);
  MatrixD<T> x(1, 1);

  A = 2;

  b = 3;

  auto LU = InverseLU::Factorize(A);
  x = LU.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), T(2.)   , T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(3./2.), T(0.0001) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeLU_2x2 )
{
  typedef Real T;

  Real Avals[] = {2, 1,
                  3, 2};

  Real bvals[] = {2,
                  3};

  MatrixD<T> A(2,2,Avals);
  MatrixD<T> b(2,1,bvals);
  MatrixD<T> x(2,1);

  auto LU = InverseLU::Factorize(A);
  x = LU.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(0.), T(0.0001) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeLU_2x1_2b )
{
  //Check that b = LU.backsolve(b) works properly
  typedef Real T;

  T Avals[] = {2, 1,
               3, 2};

  T bvals[] = {2,
               3};

  MatrixD<T> A(2,2,Avals);
  MatrixD<T> b(2,1, bvals);

  auto LU = InverseLU::Factorize(A);
  b = LU.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( b(1,0),        T(1e-12) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeLU_2x2_RHSExpression )
{
  typedef Real T;

  Real Avals[] = {2, 1,
                  3, 2};

  Real bvals[] = {1,
                  2};

  MatrixD<T> A(2,2,Avals);
  MatrixD<T> b1(2,1,bvals), b2(2,1);
  MatrixD<T> x(2,1);

  b2 = 1;

  auto LU = InverseLU::Factorize(A);
  x = LU.backsolve( b1 + b2 );

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(0.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeLU_2x2_InvExpression )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {2,
                  3};

  MatrixD<T> A1(2,2,Avals), A2(2,2,Avals);
  MatrixD<T> b(2,1,bvals);
  MatrixD<T> x(2,1);

  A2 = 1;

  auto LU = InverseLU::Factorize(A1 + A2);
  x = LU.backsolve(b);

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(0.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeLU_2x2_InvExpression_2b )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {2,
                  3};

  MatrixD<T> A1(2,2,Avals), A2(2,2,Avals);
  MatrixD<T> b(2,1,bvals);

  A2 = 1;

  auto LU = InverseLU::Factorize(A1 + A2);
  b = LU.backsolve(b);

  BOOST_CHECK_CLOSE( b(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(0.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeLU_2x2_Expressions )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {1,
                  2};

  MatrixD<T> A1(2,2,Avals), A2(2,2,Avals);
  MatrixD<T> b1(2,1,bvals), b2(2,1);
  MatrixD<T> x(2,1);

  A2 = 1;
  b2 = 1;

  auto LU = InverseLU::Factorize(A1 + A2);
  x = LU.backsolve(b1 + b2);

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(0.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeLU_2x2_Expressions_2 )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {1,
                  2};

  MatrixD<T> A1(2,2,Avals), A2(2,2,Avals);
  MatrixD<T> b1(2,1,bvals), b2(2,1);
  MatrixD<T> x(2,1);

  A2 = 1;
  b2 = 1;

  auto LU = InverseLU::Factorize(A1 + A2);
  x = LU.backsolve(b1 + b2) + b2;

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(1.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeLU_2x1_MulScal )
{
  //Check that b = !A*b works properly
  typedef Real T;

  T Avals[] = {2, 1,
               3, 2};

  T bvals[] = {2,
               3};

  MatrixD<T> A(2,2,Avals);
  MatrixD<T> b(2,1, bvals);
  MatrixD<T> x(2,1);

  auto LU = InverseLU::Factorize(A);

  x = 0; // cppcheck-suppress redundantAssignment
  x = 2*LU.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = LU.backsolve(b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = LU.backsolve(b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -LU.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-1.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -LU.backsolve(b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -LU.backsolve(b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = - 2 * LU.backsolve(b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-4.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeLU_3x3 )
{
  typedef Real T;

  Real Adata[] = {4, 2, 2,
                  2, 4, 6,
                  2, 6, 6};

  Real bdata[] = {1, 3, 2};

  MatrixD<T> A(3,3, Adata);
  MatrixD<T> b(3,1, bdata);

  auto LU = InverseLU::Factorize(A);

  MatrixD<T> x = LU.backsolve(b);

  BOOST_CHECK_CLOSE( x(0,0),   T(0.1), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0),  T(-0.5), T(0.0001) );
  BOOST_CHECK_CLOSE( x(2,0), T(4./5.), T(0.0001) );


  Real Bdata[] = {1, 2, 1,
                  2, 3, 1,
                  1, 1, 2};

  MatrixD<T> B(3,3, Bdata);

  MatrixD<T> X = LU.backsolve(B);

  BOOST_CHECK_CLOSE( X(0,0), T( 1./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(0,1), T( 1./2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(0,2), T(1./10.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,0), T(-1./2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,1), T(-1.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,2), T( 1./2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,0), T( 3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,1), T( 1.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,2), T(-1./5.), T(0.0001) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeLU_2x2_NonSymVec )
{
  typedef Real T;

  Real Adata[] = {1, 2,
                  5, 6};

  Real bdata[] = {2, 4, 5,
                  3, 2, 1};

  MatrixD<T> A(2,2, Adata);
  MatrixD<T> b(2,3, bdata);

  auto LU = InverseLU::Factorize(A);
  MatrixD<T> x = LU.backsolve(b);

  BOOST_CHECK_CLOSE( x(0,0), T(-1.50), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,1), T(-5.00), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,2), T(-7.00), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T( 1.75), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,1), T( 4.50), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,2), T( 6.00), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeLU_2x2_MixedTypes )
{
  typedef Real T;

  MatrixD<T> A = {{2, 1},
                  {3, 2}};
  MatrixD< VectorS<1,T> > b = {{2},
                               {3}};
  MatrixD< VectorS<1,T> > x(2,1);

  auto LU = InverseLU::Factorize(A);
  x = LU.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0)[0], T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0)[0], T(0.), T(0.0001) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeLU_2x2_MatrixS_1x1 )
{
  typedef Real T;

  MatrixD< MatrixS<1,1,T> > A = {{2, 1},
                                 {3, 2}};
  MatrixD< VectorS<1,T> > b = {{2},
                                 {3}};
  MatrixD< VectorS<1,T> > x(2,1);

  auto LU = InverseLU::Factorize(A);
  x = LU.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0)(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1)(0,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0)(0,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1)(0,0), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0)[0], T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0)[0], T(0.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeLU_2x2_MatrixS_2x2 )
{
  typedef Real T;

  MatrixS<2,2,T> A11 = {{5, 1},
                        {3, 7}};

  MatrixS<2,2,T> A12 = {{1, 4},
                        {2, 6}};

  MatrixS<2,2,T> A21 = {{1, 2},
                        {2, 1}};

  MatrixS<2,2,T> A22 = {{8, 5},
                        {4, 9}};

  VectorS<2,T> b1 = { 2, 4 };
  VectorS<2,T> b2 = { 3, 5 };

  MatrixD< MatrixS<2,2,T> > A = {{A11, A12},
                                 {A21, A22}};
  MatrixD< VectorS<2,T> > b = {{b1},
                               {b2}};
  MatrixD< VectorS<2,T> > x(2,1);

  MatrixD< MatrixS<2,2,T> > Ainv(2,2), I(2,2);

  I = Identity();

  auto LU = InverseLU::Factorize(A);
  Ainv = LU.backsolve(I);

  I = Ainv*A;

  for (int i = 0; i < 2; ++i)
    for (int j = 0; j < 2; ++j)
    {
      if (i == j)
        for (int ii = 0; ii < 2; ++ii)
          for (int jj = 0; jj < 2; ++jj)
          {
            if (ii == jj)
              BOOST_CHECK_CLOSE( I(i,j)(ii,jj), T(1), T(1e-12) );
            else
              BOOST_CHECK_SMALL( I(i,j)(ii,jj), T(1e-12) );
          }
      else
        for (int ii = 0; ii < 2; ++ii)
          for (int jj = 0; jj < 2; ++jj)
            BOOST_CHECK_SMALL( I(i,j)(ii,jj), T(1e-12) );
    }


  x = LU.backsolve(b);

  //Results from Mathematica
  BOOST_CHECK_CLOSE( x(0,0)[0], T(-23./330.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,0)[1], T( 41./330.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(1,0)[0], T(  1./165.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0)[1], T( 61./110.), T(0.0001) );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
