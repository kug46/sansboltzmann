// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// MatrixS_btest
// testing of MatrixS<M,N,T> class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Pow.h"

#include "chkMatrixS_btest.h"

#include <iostream>
using namespace std;


//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
namespace DLA
{

}
}

using namespace SANS::DLA;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( MatrixSymS_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Pow_MatrixSymS_2x2_1 )
{

  MatrixSymS<2,Real> A1 = {{1.2},
                          {-0.3, 1.5}};
  MatrixSymS<2,Real> P;

  P = pow(A1,1.);

  BOOST_CHECK_CLOSE( A1(0,0), P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( A1(1,0), P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( A1(1,1), P(1,1), 1e-11 );

  MatrixSymS<2,Real> A2 = { {4.9},
                            {0.7, 2.3} };

  P = pow(A2,1.);

  BOOST_CHECK_CLOSE( A2(0,0), P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( A2(1,0), P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( A2(1,1), P(1,1), 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Pow_MatrixSymS_LE_2x2_1 )
{

  MatrixSymS<2,Real> A1 = {{1.2},
                          {-0.3, 1.5}};
  MatrixSymS<2,Real> P;

  P = pow(EigenSystemPair<2,Real>(A1),1.);

  BOOST_CHECK_CLOSE( A1(0,0), P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( A1(1,0), P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( A1(1,1), P(1,1), 1e-11 );

  MatrixSymS<2,Real> A2 = { {4.9},
                            {0.7, 2.3} };

  P = pow(EigenSystemPair<2,Real>(A2),1.);

  BOOST_CHECK_CLOSE( A2(0,0), P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( A2(1,0), P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( A2(1,1), P(1,1), 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Pow_MatrixSymS_2x2_2 )
{

  MatrixSymS<2,Real> A1 = {{1.2},
                          {-0.3, 1.5}};
  MatrixSymS<2,Real> P;

  P = pow(A1,-0.5);

  BOOST_CHECK_CLOSE( 9.312846793181335e-01, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( 9.950793196254012e-02, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 8.317767473555935e-01, P(1,1), 1e-11 );

  MatrixSymS<2,Real> A2 = { {4.9},
                            {0.7, 2.3} };

  P = pow(A2,-0.5);

  BOOST_CHECK_CLOSE( 4.583189579833333e-01, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE(-5.746131963329035e-02, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 6.717467166212689e-01, P(1,1), 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Pow_MatrixSymS_LE_2x2_2 )
{

  MatrixSymS<2,Real> A1 = {{1.2},
                          {-0.3, 1.5}};
  MatrixSymS<2,Real> P;

  P = pow(EigenSystemPair<2,Real>(A1),-0.5);

  BOOST_CHECK_CLOSE( 9.312846793181335e-01, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( 9.950793196254012e-02, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 8.317767473555935e-01, P(1,1), 1e-11 );

  MatrixSymS<2,Real> A2 = { {4.9},
                            {0.7, 2.3} };

  P = pow(EigenSystemPair<2,Real>(A2),-0.5);

  BOOST_CHECK_CLOSE( 4.583189579833333e-01, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE(-5.746131963329035e-02, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 6.717467166212689e-01, P(1,1), 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Pow_MatrixSymS_2x2_3 )
{

  MatrixSymS<2,Real> A1 = {{1.2},
                          {-0.3, 1.5}};
  MatrixSymS<2,Real> P;

  P = pow(A1,-3.);

  BOOST_CHECK_CLOSE( 7.505683260166423e-01, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( 3.347858720362001e-01, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 4.157824539804421e-01, P(1,1), 1e-11 );

  MatrixSymS<2,Real> A2 = { {4.9},
                            {0.7, 2.3} };

  P = pow(A2,-3.);

  BOOST_CHECK_CLOSE( 1.342830961245563e-02, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE(-2.294355456433241e-02, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 9.864722656569033e-02, P(1,1), 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Pow_MatrixSymS_LE_2x2_3 )
{

  MatrixSymS<2,Real> A1 = {{1.2},
                          {-0.3, 1.5}};
  MatrixSymS<2,Real> P;

  P = pow(EigenSystemPair<2,Real>(A1),-3.);

  BOOST_CHECK_CLOSE( 7.505683260166423e-01, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( 3.347858720362001e-01, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 4.157824539804421e-01, P(1,1), 1e-11 );

  MatrixSymS<2,Real> A2 = { {4.9},
                            {0.7, 2.3} };

  P = pow(EigenSystemPair<2,Real>(A2),-3.);

  BOOST_CHECK_CLOSE( 1.342830961245563e-02, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE(-2.294355456433241e-02, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 9.864722656569033e-02, P(1,1), 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Pow_MatrixSymS_3x3 )
{

  MatrixSymS<3,Real> A1 = { { 1.2},
                            {-0.3, 1.5},
                            { 0.2, 0.4, 4.6} };
  MatrixSymS<3,Real> P;

  P = pow(A1,-3.0);

  BOOST_CHECK_CLOSE( 8.079663079820394e-01, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( 3.949308421514928e-01, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 4.771359688900586e-01, P(1,1), 1e-11 );
  BOOST_CHECK_CLOSE(-8.594250296967894e-02, P(2,0), 1e-11 );
  BOOST_CHECK_CLOSE(-7.573425135008999e-02, P(2,1), 1e-11 );
  BOOST_CHECK_CLOSE( 2.320406477534843e-02, P(2,2), 1e-11 );

  MatrixSymS<3,Real> A2 = { { 4.9},
                            { 0.7, 2.3},
                            {-0.1, 1.4, 6.9} };

  P = pow(A2,-3.0);

  BOOST_CHECK_CLOSE( 1.617906825785062e-02, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE(-3.655298732242723e-02, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 1.652910435301443e-01, P(1,1), 1e-11 );
  BOOST_CHECK_CLOSE( 9.887497515738725e-03, P(2,0), 1e-11 );
  BOOST_CHECK_CLOSE(-4.501880869739474e-02, P(2,1), 1e-11 );
  BOOST_CHECK_CLOSE( 1.503927957529411e-02, P(2,2), 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Pow_MatrixSymS_LE_3x3 )
{

  MatrixSymS<3,Real> A1 = { { 1.2},
                            {-0.3, 1.5},
                            { 0.2, 0.4, 4.6} };
  MatrixSymS<3,Real> P;

  P = pow(EigenSystemPair<3,Real>(A1),-3.0);

  BOOST_CHECK_CLOSE( 8.079663079820394e-01, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( 3.949308421514928e-01, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 4.771359688900586e-01, P(1,1), 1e-11 );
  BOOST_CHECK_CLOSE(-8.594250296967894e-02, P(2,0), 1e-11 );
  BOOST_CHECK_CLOSE(-7.573425135008999e-02, P(2,1), 1e-11 );
  BOOST_CHECK_CLOSE( 2.320406477534843e-02, P(2,2), 1e-11 );

  MatrixSymS<3,Real> A2 = { { 4.9},
                            { 0.7, 2.3},
                            {-0.1, 1.4, 6.9} };

  P = pow(EigenSystemPair<3,Real>(A2),-3.0);

  BOOST_CHECK_CLOSE( 1.617906825785062e-02, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE(-3.655298732242723e-02, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 1.652910435301443e-01, P(1,1), 1e-11 );
  BOOST_CHECK_CLOSE( 9.887497515738725e-03, P(2,0), 1e-11 );
  BOOST_CHECK_CLOSE(-4.501880869739474e-02, P(2,1), 1e-11 );
  BOOST_CHECK_CLOSE( 1.503927957529411e-02, P(2,2), 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Sqrt_MatrixSymS_2x2 )
{

  MatrixSymS<2,Real> A1 = {{1.2},
                          {-0.3, 1.5}};
  MatrixSymS<2,Real> P, S;

  P = pow(A1,0.5);
  S = sqrt(A1);

  BOOST_CHECK_CLOSE( S(0,0), P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( S(1,0), P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( S(1,1), P(1,1), 1e-11 );

  MatrixSymS<2,Real> A2 = { {4.9},
                            {0.7, 2.3} };

  P = pow(A2,0.5);
  S = sqrt(A2);

  BOOST_CHECK_CLOSE( S(0,0), P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( S(1,0), P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( S(1,1), P(1,1), 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Sqrt_MatrixSymS_LE_2x2 )
{

  MatrixSymS<2,Real> A1 = {{1.2},
                          {-0.3, 1.5}};
  MatrixSymS<2,Real> P, S;

  P = pow(EigenSystemPair<2,Real>(A1),0.5);
  S = sqrt(EigenSystemPair<2,Real>(A1));

  BOOST_CHECK_CLOSE( S(0,0), P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( S(1,0), P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( S(1,1), P(1,1), 1e-11 );

  MatrixSymS<2,Real> A2 = { {4.9},
                            {0.7, 2.3} };

  P = pow(EigenSystemPair<2,Real>(A2),0.5);
  S = sqrt(EigenSystemPair<2,Real>(A2));

  BOOST_CHECK_CLOSE( S(0,0), P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( S(1,0), P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( S(1,1), P(1,1), 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Sqrt_MatrixSymS_3x3 )
{

  MatrixSymS<3,Real> A1 = { { 1.2},
                            {-0.3, 1.5},
                            { 0.2, 0.4, 4.6} };
  MatrixSymS<3,Real> P;

  P = sqrt(A1);

  BOOST_CHECK_CLOSE( 1.085121147984657e+00, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE(-1.342068620660059e-01, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 1.211237177087503e+00, P(1,1), 1e-11 );
  BOOST_CHECK_CLOSE( 6.708660351259538e-02, P(2,0), 1e-11 );
  BOOST_CHECK_CLOSE( 1.220369575804513e-01, P(2,1), 1e-11 );
  BOOST_CHECK_CLOSE( 2.140235119937444e+00, P(2,2), 1e-11 );

  MatrixSymS<3,Real> A2 = { { 4.9},
                            { 0.7, 2.3},
                            {-0.1, 1.4, 6.9} };

  P = sqrt(A2);

  BOOST_CHECK_CLOSE( 2.204795444278420e+00, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( 1.940851735548276e-01, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 1.463802546157899e+00, P(1,1), 1e-11 );
  BOOST_CHECK_CLOSE(-3.475621232691974e-02, P(2,0), 1e-11 );
  BOOST_CHECK_CLOSE( 3.458511981587489e-01, P(2,1), 1e-11 );
  BOOST_CHECK_CLOSE( 2.603685648160477e+00, P(2,2), 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Sqrt_MatrixSymS_LE_3x3 )
{

  MatrixSymS<3,Real> A1 = { { 1.2},
                            {-0.3, 1.5},
                            { 0.2, 0.4, 4.6} };
  MatrixSymS<3,Real> P;

  P = sqrt(EigenSystemPair<3,Real>(A1));

  BOOST_CHECK_CLOSE( 1.085121147984657e+00, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE(-1.342068620660059e-01, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 1.211237177087503e+00, P(1,1), 1e-11 );
  BOOST_CHECK_CLOSE( 6.708660351259538e-02, P(2,0), 1e-11 );
  BOOST_CHECK_CLOSE( 1.220369575804513e-01, P(2,1), 1e-11 );
  BOOST_CHECK_CLOSE( 2.140235119937444e+00, P(2,2), 1e-11 );

  MatrixSymS<3,Real> A2 = { { 4.9},
                            { 0.7, 2.3},
                            {-0.1, 1.4, 6.9} };

  P = sqrt(EigenSystemPair<3,Real>(A2));

  BOOST_CHECK_CLOSE( 2.204795444278420e+00, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( 1.940851735548276e-01, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 1.463802546157899e+00, P(1,1), 1e-11 );
  BOOST_CHECK_CLOSE(-3.475621232691974e-02, P(2,0), 1e-11 );
  BOOST_CHECK_CLOSE( 3.458511981587489e-01, P(2,1), 1e-11 );
  BOOST_CHECK_CLOSE( 2.603685648160477e+00, P(2,2), 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Metric_dist_2x2 )
{
  static const int Dim = 2;
  MatrixSymS<Dim,Real> M = {{1.2},
                            {-0.3, 1.5}};

  MatrixS<Dim,Dim, Real> E;
  VectorS<Dim, Real> L, dX = {0.1, -0.5};

  EigenSystem(M, L, E);

  for (int d = 0; d < Dim; d++ )
    L[d] = sqrt(L[d]);

  Real pow2 = 0;
  for ( int d = 0; d < Dim; d++)
    pow2 += pow(L[d]*Transpose(E.col(d))*dX, 2);

  Real dist2 = Transpose(dX)*M*dX;

  BOOST_CHECK_CLOSE( pow2, dist2, 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Metric_dist_3x3 )
{
  static const int Dim = 3;
  MatrixSymS<Dim,Real> M = {{ 1.2},
                            {-0.3, 1.5},
                            { 0.2, 0.4, 4.6}};

  MatrixS<Dim,Dim, Real> E;
  VectorS<Dim, Real> L, dX = {0.1, -0.5, 0.7};

  EigenSystem(M, L, E);

  for (int d = 0; d < Dim; d++ )
    L[d] = sqrt(L[d]);

  Real pow2 = 0;
  for ( int d = 0; d < Dim; d++)
    pow2 += pow(L[d]*Transpose(E.col(d))*dX, 2);

  Real dist2 = Transpose(dX)*M*dX;

  BOOST_CHECK_CLOSE( pow2, dist2, 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pow_VectorS_3x1 )
{

  VectorS<3,Real> V = {0.1,1.3,10.4};
  VectorS<3,Real> P;

  P = pow(V, 2);

  BOOST_CHECK_CLOSE( pow(V[0],2), P[0], 1e-11 );
  BOOST_CHECK_CLOSE( pow(V[1],2), P[1], 1e-11 );
  BOOST_CHECK_CLOSE( pow(V[2],2), P[2], 1e-11 );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
