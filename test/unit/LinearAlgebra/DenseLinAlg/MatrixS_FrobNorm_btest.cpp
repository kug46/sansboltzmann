// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_FrobNorm.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include <iostream>
using namespace SANS::DLA;


//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FrobNorm_2x2_test )
{
  typedef Real T;

  T a = 2; T b = 3;
  T c = 6; T d = 9;

  MatrixS<2,2,T> A = {{a, b},
                      {c, d}};

  BOOST_CHECK_EQUAL( FrobNorm(A) , sqrt(a*a + b*b + c*c + d*d) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FrobNorm_3x2_test )
{
  typedef Real T;

  T a = 2; T b = 3;
  T c = 6; T d = 9;
  T e =-1; T f = 4;

  MatrixS<3,2,T> A = {{a, b},
                      {c, d},
                      {e, f}};

  BOOST_CHECK_EQUAL( FrobNorm(A) , sqrt(a*a + b*b + c*c + d*d + e*e + f*f) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_FrobNorm_2x2_test )
{
  typedef Real T;

  T a = 2;
  T b = 6; T c = 9;

  MatrixSymS<2,T> A = {{a},
                       {b, c}};

  BOOST_CHECK_EQUAL(A(0,0), a); BOOST_CHECK_EQUAL(A(0,1), b);
  BOOST_CHECK_EQUAL(A(1,0), b); BOOST_CHECK_EQUAL(A(1,1), c);

  BOOST_CHECK_EQUAL( FrobNorm(A) , sqrt(a*a + 2*b*b + c*c) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_FrobNorm_3x3_test )
{
  typedef Real T;

  T a = 1;
  T b = 4; T c = 5;
  T d = 7; T e = 8; T f = 9;

  MatrixSymS<3,T> A = {{a},
                       {b, c},
                       {d, e, f}};

  BOOST_CHECK_EQUAL(A(0,0), a); BOOST_CHECK_EQUAL(A(0,1), b); BOOST_CHECK_EQUAL(A(0,2), d);
  BOOST_CHECK_EQUAL(A(1,0), b); BOOST_CHECK_EQUAL(A(1,1), c); BOOST_CHECK_EQUAL(A(1,2), e);
  BOOST_CHECK_EQUAL(A(2,0), d); BOOST_CHECK_EQUAL(A(2,1), e); BOOST_CHECK_EQUAL(A(2,2), f);

  BOOST_CHECK_EQUAL( FrobNorm(A) , sqrt(a*a + 2*b*b + c*c + 2*d*d + 2*e*e + f*f ) );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
