// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Trace.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include <iostream>
using namespace SANS::DLA;


//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Trace_2x2_test )
{
  typedef Real T;

  T a = 2; T b = 3;
  T c = 6; T d = 9;

  MatrixS<2,2,T> A = {{a, b},
                      {c, d}};

  BOOST_CHECK_EQUAL( tr(A) , a+d );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_Trace_2x2_test )
{
  typedef Real T;

  T a = 2;
  T c = 6; T d = 9;

  MatrixSymS<2,T> A = {{a},
                       {c, d}};

  BOOST_CHECK_EQUAL(A(0,0), a); BOOST_CHECK_EQUAL(A(0,1), c);
  BOOST_CHECK_EQUAL(A(1,0), c); BOOST_CHECK_EQUAL(A(1,1), d);

  BOOST_CHECK_EQUAL( tr(A) , a + d );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Mul_Trace_2x2_test )
{
  typedef Real T;

  T a = 2; T b = 3;
  T c = 6; T d = 9;

  MatrixS<2,2,T> A = {{a, b},
                      {c, d}};

  MatrixS<2,2,T> B = {{d, c},
                      {b, a}};

  MatrixS<2,2,T> C = A*B;

  BOOST_CHECK_EQUAL( tr(C)   , C(0,0) + C(1,1) );
  BOOST_CHECK_EQUAL( tr(A*B) , C(0,0) + C(1,1) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_Mul_Trace_2x2_test )
{
  typedef Real T;

  T a = 2;
  T c = 6; T d = 9;

  MatrixSymS<2,T> A = {{a},
                       {c, d}};

  MatrixSymS<2,T> B = {{d},
                       {c, a}};

  MatrixS<2,2,T> C = A*B;

  BOOST_CHECK_EQUAL( tr(C)   , C(0,0) + C(1,1) );
  BOOST_CHECK_EQUAL( tr(A*B) , C(0,0) + C(1,1) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Trace_3x3_test )
{
  typedef Real T;

  T a = 1; T b = 2; T c = 3;
  T d = 4; T e = 5; T f = 6;
  T g = 7; T h = 8; T i = 9;

  MatrixS<3,3,T> A = {{a, b, c},
                      {d, e, f},
                      {g, h, i}};

  BOOST_CHECK_EQUAL( tr(A) , a + e + i );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_Trace_3x3_test )
{
  typedef Real T;

  T a = 1; T b = 4; T c = 7;
  T d = 4; T e = 5; T f = 8;
  T g = 7; T h = 8; T i = 9;

  MatrixSymS<3,T> A = {{a},
                       {d, e},
                       {g, h, i}};

  BOOST_CHECK_EQUAL(A(0,0), a); BOOST_CHECK_EQUAL(A(0,1), b); BOOST_CHECK_EQUAL(A(0,2), c);
  BOOST_CHECK_EQUAL(A(1,0), d); BOOST_CHECK_EQUAL(A(1,1), e); BOOST_CHECK_EQUAL(A(1,2), f);
  BOOST_CHECK_EQUAL(A(2,0), g); BOOST_CHECK_EQUAL(A(2,1), h); BOOST_CHECK_EQUAL(A(2,2), i);

  BOOST_CHECK_EQUAL( tr(A) , a + e + i );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Mul_Trace_3x3_test )
{
  typedef Real T;

  T a = 1; T b = 2; T c = 3;
  T d = 4; T e = 5; T f = 6;
  T g = 7; T h = 8; T i = 9;

  MatrixS<3,3,T> A = {{a, b, c},
                      {d, e, f},
                      {g, h, i}};

  MatrixS<3,3,T> B = {{i, d, g},
                      {b, e, h},
                      {c, f, a}};

  MatrixS<3,3,T> C = A*B;

  BOOST_CHECK_EQUAL( tr(C)   , C(0,0) + C(1,1) + C(2,2) );
  BOOST_CHECK_EQUAL( tr(A*B) , C(0,0) + C(1,1) + C(2,2) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Mul_Trace_3x2_2x3_test )
{
  typedef Real T;

  T a = 1; T b = 2; T c = 3;
  T d = 4; T e = 5; T f = 6;

  MatrixS<3,2,T> A = {{a, b},
                      {c, d},
                      {e, f}};

  MatrixS<2,3,T> B = {{a, c, e},
                      {b, d, f}};

  MatrixS<3,3,T> C = A*B;

  BOOST_CHECK_EQUAL( tr(C)   , C(0,0) + C(1,1) + C(2,2) );
  BOOST_CHECK_EQUAL( tr(A*B) , C(0,0) + C(1,1) + C(2,2) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_Mul_Trace_3x3_test )
{
  typedef Real T;

  T a = 1;
  T d = 4; T e = 5;
  T g = 7; T h = 8; T i = 9;

  MatrixSymS<3,T> A = {{a},
                       {d, e},
                       {g, h, i}};

  MatrixSymS<3,T> B = {{i},
                       {h, e},
                       {g, d, a}};

  MatrixS<3,3,T> C = A*B;

  BOOST_CHECK_EQUAL( tr(C)   , C(0,0) + C(1,1) + C(2,2) );
  BOOST_CHECK_EQUAL( tr(A*B) , C(0,0) + C(1,1) + C(2,2) );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
