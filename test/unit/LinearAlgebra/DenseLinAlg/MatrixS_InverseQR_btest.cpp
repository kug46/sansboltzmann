// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/range_c.hpp>
#include <boost/preprocessor/repetition/repeat_from_to.hpp>

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/InverseQR.h"

#include <iostream>
using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( MatrixS_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_InverseQR_Exceptions )
{
  MatrixS<2, 2, Real> A1, A1inv;
  A1 = 0;
  BOOST_CHECK_THROW( A1inv = InverseQR::Inverse(A1), SingularMatrixException );
}

//static const int CacheItems = CACHE_LINE_SIZE/sizeof(Real);
typedef boost::mpl::range_c<int, 1, 8> MatrixSizes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MatrixS_InverseQR_Rand, N, MatrixSizes )
{
  typedef Real T;
  static const int n = N::value;

  MatrixS<n,n,T> A(0), tmp, I, Ainv;

  I = Identity();

  for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++)
      A(i,j) = T(rand() % 101/100.);

  A += 5*I;

  tmp = A;

  Ainv = InverseQR::Inverse(A);

  I = Ainv*A;

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
    {
      BOOST_CHECK_CLOSE( A(i,j) , tmp(i,j), T(0.0001) );
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MatrixS_InverseQR_Rand_Expression, N, MatrixSizes )
{
  typedef Real T;
  static const int n = N::value;

  MatrixS<n,n,T> A(0), tmp, I, Ainv;

  I = Identity();

  for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++)
      A(i,j) = T(rand() % 101/100.);

  tmp = A + 5*I;

  Ainv = InverseQR::Inverse(A + 5*I);

  I = Ainv*tmp;

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveQR_1x1 )
{
  typedef Real T;
  MatrixS<1,1,T> A;
  MatrixS<1,1,T> b;
  MatrixS<1,1,T> x;

  A = 2;

  b = 3;

  x = InverseQR::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), T(2.)   , T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(3./2.), T(0.0001) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveQR_2x2 )
{
  typedef Real T;

  Real Avals[] = {2, 1,
                  3, 2};

  Real bvals[] = {2,
                  3};

  MatrixS<2,2,T> A(Avals,4);
  MatrixS<2,1,T> b(bvals,2);
  MatrixS<2,1,T> x;

  x = InverseQR::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(1e-12) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveQR_2x1_2b )
{
  //Check that b = !A*b works properly
  typedef Real T;

  T Avals[] = {2, 1,
               3, 2};

  T bvals[] = {2,
               3};

  MatrixS<2,2,T> A(Avals,4);
  MatrixS<2,1,T> b(bvals,2);

  b = InverseQR::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(1.), T(1e-12) );
  BOOST_CHECK_SMALL( b(1,0),        T(1e-12) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveQR_2x2_RHSExpression )
{
  typedef Real T;

  Real Avals[] = {2, 1,
                  3, 2};

  Real bvals[] = {1,
                  2};

  MatrixS<2,2,T> A(Avals,4);
  MatrixS<2,1,T> b1(bvals,2), b2;
  MatrixS<2,1,T> x;

  b2 = 1;

  x = InverseQR::Solve(A, b1 + b2);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0)       , T(1e-12) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveQR_2x2_InvExpression )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {2,
                  3};

  MatrixS<2,2,T> A1(Avals,4), A2;
  MatrixS<2,1,T> b(bvals,2);
  MatrixS<2,1,T> x;

  A2 = 1;

  x = InverseQR::Solve(A1 + A2, b);

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveQR_2x2_InvExpression_2b )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {2,
                  3};

  MatrixS<2,2,T> A1(Avals,4), A2;
  MatrixS<2,1,T> b(bvals,2);

  A2 = 1;

  b = InverseQR::Solve(A1 + A2, b);

  BOOST_CHECK_CLOSE( b(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( b(1,0),        T(1e-12) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveQR_2x2_Expressions )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {1,
                  2};

  MatrixS<2,2,T> A1(Avals,4), A2;
  MatrixS<2,1,T> b1(bvals,2), b2;
  MatrixS<2,1,T> x;

  A2 = 1;
  b2 = 1;

  x = InverseQR::Solve(A1 + A2, b1 + b2);

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveQR_2x2_Expressions_2 )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {1,
                  2};

  MatrixS<2,2,T> A1(Avals,4), A2;
  MatrixS<2,1,T> b1(bvals,2), b2;
  MatrixS<2,1,T> x;

  A2 = 1;
  b2 = 1;

  x = InverseQR::Solve(A1 + A2, b1 + b2) + b2;

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(1.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveQR_2x1_MulScal )
{
  //Check that b = !A*b works properly
  typedef Real T;

  T Avals[] = {2, 1,
               3, 2};

  T bvals[] = {2,
               3};

  MatrixS<2,2,T> A(Avals,4);
  MatrixS<2,1,T> b(bvals,2);
  MatrixS<2,1,T> x;

  x = 0;
  x = 2*InverseQR::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = InverseQR::Solve(A, b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = InverseQR::Solve(A, b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -InverseQR::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-1.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -InverseQR::Solve(A, b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -InverseQR::Solve(A, b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -(2*InverseQR::Solve(A, b)) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-4.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveQR_3x3 )
{
  typedef Real T;

  Real Adata[] = {4, 2, 2,
                  2, 4, 6,
                  2, 6, 6};

  Real bdata[] = {1, 3, 2};

  MatrixS<3,3,T> A(Adata,9);
  MatrixS<3,1,T> b(bdata,3);

  MatrixS<3,1,T> x = InverseQR::Solve(A, b);

  BOOST_CHECK_CLOSE( x(0,0),   T(0.1), T(1e-12) );
  BOOST_CHECK_CLOSE( x(1,0),  T(-0.5), T(1e-12) );
  BOOST_CHECK_CLOSE( x(2,0), T(4./5.), T(1e-12) );


  Real Bdata[] = {1, 2, 1,
                  2, 3, 1,
                  1, 1, 2};

  MatrixS<3,3,T> B(Bdata, 9);

  MatrixS<3,3,T> X = InverseQR::Solve(A, B);

  BOOST_CHECK_CLOSE( X(0,0), T( 1./5.), T(1e-12) );
  BOOST_CHECK_CLOSE( X(0,1), T( 1./2.), T(1e-12) );
  BOOST_CHECK_CLOSE( X(0,2), T(1./10.), T(1e-12) );
  BOOST_CHECK_CLOSE( X(1,0), T(-1./2.), T(1e-12) );
  BOOST_CHECK_CLOSE( X(1,1), T(-1.   ), T(1e-12) );
  BOOST_CHECK_CLOSE( X(1,2), T( 1./2.), T(1e-12) );
  BOOST_CHECK_CLOSE( X(2,0), T( 3./5.), T(1e-12) );
  BOOST_CHECK_CLOSE( X(2,1), T( 1.   ), T(1e-12) );
  BOOST_CHECK_CLOSE( X(2,2), T(-1./5.), T(1e-12) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveQR_2x2_NonSymVec )
{
  typedef Real T;

  Real Adata[] = {1, 2,
                  5, 6};

  Real bdata[] = {2, 4, 5,
                  3, 2, 1};

  MatrixS<2,2,T> A(Adata, 4);
  MatrixS<2,3,T> b(bdata, 6);

  MatrixS<2,3,T> x = InverseQR::Solve(A, b);

  BOOST_CHECK_CLOSE( x(0,0), T(-1.50), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,1), T(-5.00), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,2), T(-7.00), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T( 1.75), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,1), T( 4.50), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,2), T( 6.00), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_SolveQR_3x2_LeastSquares )
{
  typedef Real T;

  MatrixS<3,2,T> A= {{2,1},
                     {3,2},
                     {6,9}};
  MatrixS<3,1,T> b = {{2},
                      {4},
                      {3}};

  MatrixS<2,1,T> x = InverseQR::Solve(A, b);

  //Results from mathematica
  BOOST_CHECK_CLOSE( x(0,0), T(63./37.), T(1e-12) );
  BOOST_CHECK_CLOSE( x(1,0), T(-59./74.), T(1e-12) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MatrixS_InverseQR_Rand_Factorize, N, MatrixSizes )
{
  typedef Real T;
  static const int n = N::value;

  MatrixS<n,n,T> A(0), tmp, I(0), Ainv;

  I = Identity();

  for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++)
    {
      I(i,j) = 0; if ( i == j ) I(i,j) = 1; // To suppress the clang analyzer
      A(i,j) = T(rand() % 101/100.) + 5*I(i,j);
    }

  tmp = A;

  auto QR = InverseQR::Factorize(A);
  Ainv = QR.backsolve(I);

  I = Ainv*A;

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
    {
      BOOST_CHECK_CLOSE( A(i,j) , tmp(i,j), T(0.0001) );
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }

  I = Identity();

  // Solve the system a 2nd time and make sure it still works
  Ainv = 0;  // cppcheck-suppress redundantAssignment
  Ainv = QR.backsolve(I);

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
    {
      BOOST_CHECK_CLOSE( A(i,j) , tmp(i,j), T(0.0001) );
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MatrixS_InverseQR_Rand_Expression_Factorize, N, MatrixSizes )
{
  typedef Real T;
  static const int n = N::value;

  MatrixS<n,n,T> A(0), tmp, I, Ainv;

  I = Identity();

  for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++)
      A(i,j) = T(rand() % 101/100.);

  tmp = A + 5*I;

  auto QR = InverseQR::Factorize(A + 5*I);
  Ainv = QR.backsolve(I);

  I = Ainv*tmp;

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }

  I = Identity();

  // Solve the system a 2nd time and make sure it still works
  Ainv = 0;  // cppcheck-suppress redundantAssignment
  Ainv = QR.backsolve(I);

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }

  I = Identity();

  // Solve the system a 3rd time and make sure it still works
  Ainv = 0;  // cppcheck-suppress redundantAssignment
  Ainv = QR.backsolve(I - I*I + I);

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeQR_1x1 )
{
  typedef Real T;
  MatrixS<1,1,T> A;
  MatrixS<1,1,T> b;
  MatrixS<1,1,T> x;


  A = 2;

  b = 3;

  auto QR = InverseQR::Factorize(A);
  x = QR.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), T(2.)   , T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(3./2.), T(0.0001) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeQR_2x2 )
{
  typedef Real T;

  Real Avals[] = {2, 1,
                  3, 2};

  Real bvals[] = {2,
                  3};

  MatrixS<2,2,T> A(Avals, 2*2);
  MatrixS<2,1,T> b(bvals, 2*1);
  MatrixS<2,1,T> x;

  auto QR = InverseQR::Factorize(A);
  x = QR.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeQR_2x1_2b )
{
  //Check that b = QR.backsolve(b) works properly
  typedef Real T;

  T Avals[] = {2, 1,
               3, 2};

  T bvals[] = {2,
               3};

  MatrixS<2,2,T> A(Avals, 2*2);
  MatrixS<2,1,T> b(bvals, 2*1);

  auto QR = InverseQR::Factorize(A);
  b = QR.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( b(1,0),        T(1e-12) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeQR_2x2_RHSExpression )
{
  typedef Real T;

  Real Avals[] = {2, 1,
                  3, 2};

  Real bvals[] = {1,
                  2};

  MatrixS<2,2,T> A(Avals, 2*2);
  MatrixS<2,1,T> b1(bvals, 2*1), b2;
  MatrixS<2,1,T> x;

  b2 = 1;

  auto QR = InverseQR::Factorize(A);
  x = QR.backsolve( b1 + b2 );

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeQR_2x2_InvExpression )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {2,
                  3};

  MatrixS<2,2,T> A1(Avals, 2*2), A2(Avals, 2*2);
  MatrixS<2,1,T> b(bvals, 2*1);
  MatrixS<2,1,T> x;

  A2 = 1;

  auto QR = InverseQR::Factorize(A1 + A2);
  x = QR.backsolve(b);

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeQR_2x2_InvExpression_2b )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {2,
                  3};

  MatrixS<2,2,T> A1(Avals, 2*2), A2(Avals, 2*2);
  MatrixS<2,1,T> b(bvals, 2*1);

  A2 = 1;

  auto QR = InverseQR::Factorize(A1 + A2);
  b = QR.backsolve(b);

  BOOST_CHECK_CLOSE( b(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( b(1,0),        T(1e-12) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeQR_2x2_Expressions )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {1,
                  2};

  MatrixS<2,2,T> A1(Avals, 2*2), A2(Avals, 2*2);
  MatrixS<2,1,T> b1(bvals, 2*1), b2;
  MatrixS<2,1,T> x;

  A2 = 1;
  b2 = 1;

  auto QR = InverseQR::Factorize(A1 + A2);
  x = QR.backsolve(b1 + b2);

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeQR_2x2_Expressions_2 )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {1,
                  2};

  MatrixS<2,2,T> A1(Avals, 2*2), A2(Avals, 2*2);
  MatrixS<2,1,T> b1(bvals, 2*1), b2;
  MatrixS<2,1,T> x;

  A2 = 1;
  b2 = 1;

  auto QR = InverseQR::Factorize(A1 + A2);
  x = QR.backsolve(b1 + b2) + b2;

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(1.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeQR_2x1_MulScal )
{
  //Check that b = !A*b works properly
  typedef Real T;

  T Avals[] = {2, 1,
               3, 2};

  T bvals[] = {2,
               3};

  MatrixS<2,2,T> A(Avals, 2*2);
  MatrixS<2,1,T> b(bvals, 2*1);
  MatrixS<2,1,T> x;

  auto QR = InverseQR::Factorize(A);

  x = 0; // cppcheck-suppress redundantAssignment
  x = 2*QR.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = QR.backsolve(b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = QR.backsolve(b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -QR.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-1.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -QR.backsolve(b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -QR.backsolve(b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = - 2 * QR.backsolve(b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-4.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeQR_3x3 )
{
  typedef Real T;

  Real Adata[] = {4, 2, 2,
                  2, 4, 6,
                  2, 6, 6};

  Real bdata[] = {1, 3, 2};

  MatrixS<3,3,T> A(Adata, 3*3);
  MatrixS<3,1,T> b(bdata, 3*1);

  auto QR = InverseQR::Factorize(A);

  MatrixS<3,1,T> x = QR.backsolve(b);

  BOOST_CHECK_CLOSE( x(0,0),   T(0.1), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0),  T(-0.5), T(0.0001) );
  BOOST_CHECK_CLOSE( x(2,0), T(4./5.), T(0.0001) );


  Real Bdata[] = {1, 2, 1,
                  2, 3, 1,
                  1, 1, 2};

  MatrixS<3,3,T> B(Bdata, 3*3);

  MatrixS<3,3,T> X = QR.backsolve(B);

  BOOST_CHECK_CLOSE( X(0,0), T( 1./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(0,1), T( 1./2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(0,2), T(1./10.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,0), T(-1./2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,1), T(-1.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,2), T( 1./2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,0), T( 3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,1), T( 1.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,2), T(-1./5.), T(0.0001) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeQR_2x2_NonSymVec )
{
  typedef Real T;

  Real Adata[] = {1, 2,
                  5, 6};

  Real bdata[] = {2, 4, 5,
                  3, 2, 1};

  MatrixS<2,2,T> A(Adata, 4);
  MatrixS<2,3,T> b(bdata, 6);

  auto QR = InverseQR::Factorize(A);
  MatrixS<2,3,T> x = QR.backsolve(b);

  BOOST_CHECK_CLOSE( x(0,0), T(-1.50), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,1), T(-5.00), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,2), T(-7.00), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T( 1.75), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,1), T( 4.50), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,2), T( 6.00), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_FactorizeQR_3x2_LeastSquares )
{
  typedef Real T;

  MatrixS<3,2,T> A= {{2,1},
                     {3,2},
                     {6,9}};
  MatrixS<3,1,T> b = {{2},
                      {4},
                      {3}};

  auto QR = InverseQR::Factorize(A);
  MatrixS<2,1,T> x = QR.backsolve(b);

  //Results from mathematica
  BOOST_CHECK_CLOSE( x(0,0), T(63./37.), T(1e-12) );
  BOOST_CHECK_CLOSE( x(1,0), T(-59./74.), T(1e-12) );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
