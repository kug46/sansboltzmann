// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"

#include <iostream>

namespace SANS
{
namespace DLA
{
  //Explicitly instantiate the class so that coverage information is correct
  template class MatrixSTranspose<2,2,Real>;
}
}

using namespace SANS::DLA;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Transpose_2x2_test )
{
  typedef Real T;
  Real Adata[] = {1, 2,
                  3, 4};

  MatrixS<2,2,T> A(Adata, 4);
  MatrixS<2,2,T> AT;

  AT = Transpose(A);

  BOOST_CHECK_EQUAL( AT(0,0) , 1 );
  BOOST_CHECK_EQUAL( AT(0,1) , 3 );
  BOOST_CHECK_EQUAL( AT(1,0) , 2 );
  BOOST_CHECK_EQUAL( AT(1,1) , 4 );

  MatrixS<2,2,T> AT2( Transpose(A) );

  BOOST_CHECK_EQUAL( AT2(0,0) , 1 );
  BOOST_CHECK_EQUAL( AT2(0,1) , 3 );
  BOOST_CHECK_EQUAL( AT2(1,0) , 2 );
  BOOST_CHECK_EQUAL( AT2(1,1) , 4 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Transpose_3x3_test )
{
  typedef Real T;
  Real Adata[] = {1, 2, 3,
                  4, 5, 6,
                  7, 8, 9};

  MatrixS<3,3,T> A(Adata,9);
  MatrixS<3,3,T> AT;

  AT = Transpose(A);

  BOOST_CHECK_EQUAL( AT(0,0) , 1 );
  BOOST_CHECK_EQUAL( AT(1,0) , 2 );
  BOOST_CHECK_EQUAL( AT(2,0) , 3 );
  BOOST_CHECK_EQUAL( AT(0,1) , 4 );
  BOOST_CHECK_EQUAL( AT(1,1) , 5 );
  BOOST_CHECK_EQUAL( AT(2,1) , 6 );
  BOOST_CHECK_EQUAL( AT(0,2) , 7 );
  BOOST_CHECK_EQUAL( AT(1,2) , 8 );
  BOOST_CHECK_EQUAL( AT(2,2) , 9 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Transpose_2x3_test )
{
  typedef Real T;
  Real Adata[] = {1, 2, 3,
                  4, 5, 6};

  MatrixS<2,3,T> A(Adata, 6);
  MatrixS<3,2,T> AT;

  AT = Transpose(A);

  BOOST_CHECK_EQUAL( AT(0,0) , 1 );
  BOOST_CHECK_EQUAL( AT(1,0) , 2 );
  BOOST_CHECK_EQUAL( AT(2,0) , 3 );
  BOOST_CHECK_EQUAL( AT(0,1) , 4 );
  BOOST_CHECK_EQUAL( AT(1,1) , 5 );
  BOOST_CHECK_EQUAL( AT(2,1) , 6 );

  MatrixS<3,2,T> AT2( Transpose(A) );

  BOOST_CHECK_EQUAL( AT2(0,0) , 1 );
  BOOST_CHECK_EQUAL( AT2(1,0) , 2 );
  BOOST_CHECK_EQUAL( AT2(2,0) , 3 );
  BOOST_CHECK_EQUAL( AT2(0,1) , 4 );
  BOOST_CHECK_EQUAL( AT2(1,1) , 5 );
  BOOST_CHECK_EQUAL( AT2(2,1) , 6 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Transpose_2x2_add_test )
{
  typedef Real T;
  Real Adata[] = {1, 2,
                  3, 4};

  Real ATdata[] = {1, 2,
                   3, 4};

  MatrixS<2,2,T> A(Adata, 4);
  MatrixS<2,2,T> AT(ATdata, 4);

  AT += Transpose(A);

  BOOST_CHECK_EQUAL( AT(0,0) , 2 );
  BOOST_CHECK_EQUAL( AT(0,1) , 5 );
  BOOST_CHECK_EQUAL( AT(1,0) , 5 );
  BOOST_CHECK_EQUAL( AT(1,1) , 8 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Transpose_2x2_Sym_test )
{
  typedef Real T;
  Real Adata[] = {1, 2,
                  3, 4};

  MatrixS<2,2,T> A(Adata, 4);
  MatrixS<2,2,T> Sym;

  Sym = Transpose(A)*A;

  BOOST_CHECK_EQUAL( Sym(0,0) , 10 );
  BOOST_CHECK_EQUAL( Sym(0,1) , 14 );
  BOOST_CHECK_EQUAL( Sym(1,0) , 14 );
  BOOST_CHECK_EQUAL( Sym(1,1) , 20 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Transpose_xTMx_test )
{
  typedef Real T;
  Real Mdata[] = {1, 2,
                  3, 4};

  Real xdata[] = {3, 4};

  MatrixS<2,2,T> M(Mdata, 4);
  MatrixS<2,1,T> x(xdata, 2);
  T xTMx;

  xTMx = Transpose(x)*M*x;

  BOOST_CHECK_EQUAL( xTMx , 133 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Vector2MatrixS34_Transpose_test )
{
  typedef Real T;

  MatrixS<3,4,T> A1 = {{1, 2, 3, 4},
                       {5, 6, 7, 8},
                       {9, 10, 11, 12}};

  MatrixS<3,4,T> A2 = {{13, 14, 15, 16},
                       {17, 18, 19, 20},
                       {21, 22, 23, 24}};

  VectorS<2, MatrixS<3,4,T>> vecA = {A1, A2};
  MatrixS<1, 2, MatrixS<4,3,T>> vecAT;

  vecAT = Transpose(vecA);

  BOOST_CHECK_EQUAL( vecAT(0,0)(0,0), 1 );
  BOOST_CHECK_EQUAL( vecAT(0,0)(0,1), 5 );
  BOOST_CHECK_EQUAL( vecAT(0,0)(0,2), 9 );
  BOOST_CHECK_EQUAL( vecAT(0,0)(1,0), 2 );
  BOOST_CHECK_EQUAL( vecAT(0,0)(1,1), 6 );
  BOOST_CHECK_EQUAL( vecAT(0,0)(1,2), 10 );
  BOOST_CHECK_EQUAL( vecAT(0,0)(2,0), 3 );
  BOOST_CHECK_EQUAL( vecAT(0,0)(2,1), 7 );
  BOOST_CHECK_EQUAL( vecAT(0,0)(2,2), 11 );
  BOOST_CHECK_EQUAL( vecAT(0,0)(3,0), 4 );
  BOOST_CHECK_EQUAL( vecAT(0,0)(3,1), 8 );
  BOOST_CHECK_EQUAL( vecAT(0,0)(3,2), 12 );

  BOOST_CHECK_EQUAL( vecAT(0,1)(0,0), 13 );
  BOOST_CHECK_EQUAL( vecAT(0,1)(0,1), 17 );
  BOOST_CHECK_EQUAL( vecAT(0,1)(0,2), 21 );
  BOOST_CHECK_EQUAL( vecAT(0,1)(1,0), 14 );
  BOOST_CHECK_EQUAL( vecAT(0,1)(1,1), 18 );
  BOOST_CHECK_EQUAL( vecAT(0,1)(1,2), 22 );
  BOOST_CHECK_EQUAL( vecAT(0,1)(2,0), 15 );
  BOOST_CHECK_EQUAL( vecAT(0,1)(2,1), 19 );
  BOOST_CHECK_EQUAL( vecAT(0,1)(2,2), 23 );
  BOOST_CHECK_EQUAL( vecAT(0,1)(3,0), 16 );
  BOOST_CHECK_EQUAL( vecAT(0,1)(3,1), 20 );
  BOOST_CHECK_EQUAL( vecAT(0,1)(3,2), 24 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Vector2MatrixS34_Transpose_MatMulVec_test )
{
  typedef Real T;

  MatrixS<3,4,T> A1 = {{1, 2, 3, 4},
                       {5, 6, 7, 8},
                       {9, 10, 11, 12}};

  MatrixS<3,4,T> A2 = {{13, 14, 15, 16},
                       {17, 18, 19, 20},
                       {21, 22, 23, 24}};

  VectorS<2, MatrixS<3,4,T>> A = {A1, A2};
  VectorS<2, VectorS<3, T>> b = {{1, 2, 3}, {4, 5, 6}};

  VectorS<1, VectorS<4, T>> x;

  x = Transpose(A)*b;

  BOOST_CHECK_EQUAL( x[0][0], 301 );
  BOOST_CHECK_EQUAL( x[0][1], 322 );
  BOOST_CHECK_EQUAL( x[0][2], 343 );
  BOOST_CHECK_EQUAL( x[0][3], 364 );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
