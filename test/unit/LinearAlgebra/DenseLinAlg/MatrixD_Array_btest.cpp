// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Array.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Array2D.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Array3D.h"

#ifdef B0
#undef B0
#endif

#include <iostream>
using namespace SANS::DLA;


//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixDView_Array_test )
{
  typedef Real T;
  Real data[] = {1, 2, 3, 4,
                 5, 6, 7, 8};

  MatrixDView_Array<T> array(data, 2,2, 2);

  BOOST_REQUIRE_EQUAL(2, array.size());
  BOOST_REQUIRE_EQUAL(2, array.m());
  BOOST_REQUIRE_EQUAL(2, array.n());

  MatrixDView<T> A = array[0];
  MatrixDView<T> B = array[1];

  BOOST_REQUIRE_EQUAL( 2, A.m());
  BOOST_REQUIRE_EQUAL( 2, A.n());

  BOOST_REQUIRE_EQUAL( 2, B.m());
  BOOST_REQUIRE_EQUAL( 2, B.n());

  BOOST_CHECK_EQUAL( data[0] , A(0,0) );
  BOOST_CHECK_EQUAL( data[1] , A(0,1) );
  BOOST_CHECK_EQUAL( data[2] , A(1,0) );
  BOOST_CHECK_EQUAL( data[3] , A(1,1) );

  BOOST_CHECK_EQUAL( data[4] , B(0,0) );
  BOOST_CHECK_EQUAL( data[5] , B(0,1) );
  BOOST_CHECK_EQUAL( data[6] , B(1,0) );
  BOOST_CHECK_EQUAL( data[7] , B(1,1) );

  const MatrixDView<T> cA = const_cast<const MatrixDView_Array<T>&>(array)[0];
  const MatrixDView<T> cB = const_cast<const MatrixDView_Array<T>&>(array)[1];

  BOOST_CHECK_EQUAL( data[0] , cA(0,0) );
  BOOST_CHECK_EQUAL( data[1] , cA(0,1) );
  BOOST_CHECK_EQUAL( data[2] , cA(1,0) );
  BOOST_CHECK_EQUAL( data[3] , cA(1,1) );

  BOOST_CHECK_EQUAL( data[4] , cB(0,0) );
  BOOST_CHECK_EQUAL( data[5] , cB(0,1) );
  BOOST_CHECK_EQUAL( data[6] , cB(1,0) );
  BOOST_CHECK_EQUAL( data[7] , cB(1,1) );

  MatrixDView_Array<T> array2(array);

  MatrixDView<T> A2 = array2[0];
  MatrixDView<T> B2 = array2[1];

  BOOST_REQUIRE_EQUAL( 2, A2.m() );
  BOOST_REQUIRE_EQUAL( 2, A2.n() );

  BOOST_REQUIRE_EQUAL( 2, B2.m() );
  BOOST_REQUIRE_EQUAL( 2, B2.n() );

  BOOST_CHECK_EQUAL( data[0] , A2(0,0) );
  BOOST_CHECK_EQUAL( data[1] , A2(0,1) );
  BOOST_CHECK_EQUAL( data[2] , A2(1,0) );
  BOOST_CHECK_EQUAL( data[3] , A2(1,1) );

  BOOST_CHECK_EQUAL( data[4] , B2(0,0) );
  BOOST_CHECK_EQUAL( data[5] , B2(0,1) );
  BOOST_CHECK_EQUAL( data[6] , B2(1,0) );
  BOOST_CHECK_EQUAL( data[7] , B2(1,1) );

  array = 0;
  for (int i = 0; i < 2*2*2; i++)
  {
    BOOST_CHECK_EQUAL( 0, array.value(i) );
    BOOST_CHECK_EQUAL( 0, data[i] );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_Array_test )
{
  typedef Real T;
  Real data[] = {1, 2, 3, 4,
                 5, 6, 7, 8};

  MatrixD_Array<T> array(2,2, 2);

  BOOST_REQUIRE_EQUAL(2, array.size());
  BOOST_REQUIRE_EQUAL(2, array.m());
  BOOST_REQUIRE_EQUAL(2, array.n());

  for (int i = 0; i < 2*2*2; i++)
  {
    array.value(i) = data[i];
    BOOST_CHECK_EQUAL( data[i], const_cast<const MatrixD_Array<T>&>(array).value(i) );
  }

  MatrixDView<T> A = array[0];
  MatrixDView<T> B = array[1];

  BOOST_CHECK_EQUAL( data[0] , A(0,0) );
  BOOST_CHECK_EQUAL( data[1] , A(0,1) );
  BOOST_CHECK_EQUAL( data[2] , A(1,0) );
  BOOST_CHECK_EQUAL( data[3] , A(1,1) );

  BOOST_CHECK_EQUAL( data[4] , B(0,0) );
  BOOST_CHECK_EQUAL( data[5] , B(0,1) );
  BOOST_CHECK_EQUAL( data[6] , B(1,0) );
  BOOST_CHECK_EQUAL( data[7] , B(1,1) );

  MatrixD_Array<T> array2(array);

  MatrixDView<T> A2 = array2[0];
  MatrixDView<T> B2 = array2[1];

  BOOST_CHECK_EQUAL( data[0] , A2(0,0) );
  BOOST_CHECK_EQUAL( data[1] , A2(0,1) );
  BOOST_CHECK_EQUAL( data[2] , A2(1,0) );
  BOOST_CHECK_EQUAL( data[3] , A2(1,1) );

  BOOST_CHECK_EQUAL( data[4] , B2(0,0) );
  BOOST_CHECK_EQUAL( data[5] , B2(0,1) );
  BOOST_CHECK_EQUAL( data[6] , B2(1,0) );
  BOOST_CHECK_EQUAL( data[7] , B2(1,1) );

  array = 0;
  for (int i = 0; i < 2*2*2; i++)
    BOOST_CHECK_EQUAL( 0, array.value(i) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixDView_Array2D_test )
{
  typedef Real T;
  // Data with a 2x3 array of 2x2 matrices
  Real data[] = {1, 2, 3, 4, 5, 6,
                 7, 8, 9, 1, 2, 3,
                 4, 5, 6, 7, 8, 9,
                 1, 2, 3, 4, 5, 6};

  MatrixDView_Array2D<T> array(data, 2,2, 2,3);

  BOOST_REQUIRE_EQUAL(2*3, array.size());
  BOOST_REQUIRE_EQUAL(2, array.m());
  BOOST_REQUIRE_EQUAL(2, array.n());

  for (int i = 0; i < 2*2*2*3; i++)
    array.value(i) = data[i];

  MatrixDView<T> A0 = array(0,0);
  MatrixDView<T> B0 = array(0,1);
  MatrixDView<T> C0 = array(0,2);

  MatrixDView<T> A1 = array(1,0);
  MatrixDView<T> B1 = array(1,1);
  MatrixDView<T> C1 = array(1,2);

  BOOST_CHECK_EQUAL( data[0] , A0(0,0) );
  BOOST_CHECK_EQUAL( data[1] , A0(0,1) );
  BOOST_CHECK_EQUAL( data[2] , A0(1,0) );
  BOOST_CHECK_EQUAL( data[3] , A0(1,1) );

  BOOST_CHECK_EQUAL( data[4] , B0(0,0) );
  BOOST_CHECK_EQUAL( data[5] , B0(0,1) );
  BOOST_CHECK_EQUAL( data[6] , B0(1,0) );
  BOOST_CHECK_EQUAL( data[7] , B0(1,1) );

  BOOST_CHECK_EQUAL( data[8 ] , C0(0,0) );
  BOOST_CHECK_EQUAL( data[9 ] , C0(0,1) );
  BOOST_CHECK_EQUAL( data[10] , C0(1,0) );
  BOOST_CHECK_EQUAL( data[11] , C0(1,1) );

  BOOST_CHECK_EQUAL( data[12] , A1(0,0) );
  BOOST_CHECK_EQUAL( data[13] , A1(0,1) );
  BOOST_CHECK_EQUAL( data[14] , A1(1,0) );
  BOOST_CHECK_EQUAL( data[15] , A1(1,1) );

  BOOST_CHECK_EQUAL( data[16] , B1(0,0) );
  BOOST_CHECK_EQUAL( data[17] , B1(0,1) );
  BOOST_CHECK_EQUAL( data[18] , B1(1,0) );
  BOOST_CHECK_EQUAL( data[19] , B1(1,1) );

  BOOST_CHECK_EQUAL( data[20] , C1(0,0) );
  BOOST_CHECK_EQUAL( data[21] , C1(0,1) );
  BOOST_CHECK_EQUAL( data[22] , C1(1,0) );
  BOOST_CHECK_EQUAL( data[23] , C1(1,1) );

  const MatrixDView<T> cA0 = const_cast<const MatrixDView_Array2D<T>&>(array)(0,0);
  const MatrixDView<T> cB0 = const_cast<const MatrixDView_Array2D<T>&>(array)(0,1);
  const MatrixDView<T> cC0 = const_cast<const MatrixDView_Array2D<T>&>(array)(0,2);

  const MatrixDView<T> cA1 = const_cast<const MatrixDView_Array2D<T>&>(array)(1,0);
  const MatrixDView<T> cB1 = const_cast<const MatrixDView_Array2D<T>&>(array)(1,1);
  const MatrixDView<T> cC1 = const_cast<const MatrixDView_Array2D<T>&>(array)(1,2);

  BOOST_CHECK_EQUAL( data[0] , cA0(0,0) );
  BOOST_CHECK_EQUAL( data[1] , cA0(0,1) );
  BOOST_CHECK_EQUAL( data[2] , cA0(1,0) );
  BOOST_CHECK_EQUAL( data[3] , cA0(1,1) );

  BOOST_CHECK_EQUAL( data[4] , cB0(0,0) );
  BOOST_CHECK_EQUAL( data[5] , cB0(0,1) );
  BOOST_CHECK_EQUAL( data[6] , cB0(1,0) );
  BOOST_CHECK_EQUAL( data[7] , cB0(1,1) );

  BOOST_CHECK_EQUAL( data[8 ] , cC0(0,0) );
  BOOST_CHECK_EQUAL( data[9 ] , cC0(0,1) );
  BOOST_CHECK_EQUAL( data[10] , cC0(1,0) );
  BOOST_CHECK_EQUAL( data[11] , cC0(1,1) );

  BOOST_CHECK_EQUAL( data[12] , cA1(0,0) );
  BOOST_CHECK_EQUAL( data[13] , cA1(0,1) );
  BOOST_CHECK_EQUAL( data[14] , cA1(1,0) );
  BOOST_CHECK_EQUAL( data[15] , cA1(1,1) );

  BOOST_CHECK_EQUAL( data[16] , cB1(0,0) );
  BOOST_CHECK_EQUAL( data[17] , cB1(0,1) );
  BOOST_CHECK_EQUAL( data[18] , cB1(1,0) );
  BOOST_CHECK_EQUAL( data[19] , cB1(1,1) );

  BOOST_CHECK_EQUAL( data[20] , cC1(0,0) );
  BOOST_CHECK_EQUAL( data[21] , cC1(0,1) );
  BOOST_CHECK_EQUAL( data[22] , cC1(1,0) );
  BOOST_CHECK_EQUAL( data[23] , cC1(1,1) );

  MatrixDView_Array2D<T> array2(array);

  int k = 0;
  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 3; j++)
    {
      MatrixDView<T> A = array2(i,j);

      BOOST_REQUIRE_EQUAL( 2, A.m() );
      BOOST_REQUIRE_EQUAL( 2, A.n() );

      BOOST_CHECK_EQUAL( data[k++], A(0,0) );
      BOOST_CHECK_EQUAL( data[k++], A(0,1) );
      BOOST_CHECK_EQUAL( data[k++], A(1,0) );
      BOOST_CHECK_EQUAL( data[k++], A(1,1) );
    }

  array = 0;
  for (int i = 0; i < 2*2*2*3; i++)
  {
    BOOST_CHECK_EQUAL( 0, array.value(i) );
    BOOST_CHECK_EQUAL( 0, data[i] );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_Array2D_test )
{
  typedef Real T;
  // Data with a 2x3 array of 2x2 matrices
  Real data[] = {1, 2, 3, 4, 5, 6,
                 7, 8, 9, 1, 2, 3,
                 4, 5, 6, 7, 8, 9,
                 1, 2, 3, 4, 5, 6};

  MatrixD_Array2D<T> array(2,2, 2,3);

  BOOST_REQUIRE_EQUAL(2*3, array.size());
  BOOST_REQUIRE_EQUAL(2, array.m());
  BOOST_REQUIRE_EQUAL(2, array.n());

  for (int i = 0; i < 2*2*2*3; i++)
  {
    array.value(i) = data[i];
    BOOST_CHECK_EQUAL( data[i], const_cast<const MatrixD_Array2D<T>&>(array).value(i) );
  }

  MatrixDView<T> A0 = array(0,0);
  MatrixDView<T> B0 = array(0,1);
  MatrixDView<T> C0 = array(0,2);

  MatrixDView<T> A1 = array(1,0);
  MatrixDView<T> B1 = array(1,1);
  MatrixDView<T> C1 = array(1,2);

  BOOST_CHECK_EQUAL( data[0] , A0(0,0) );
  BOOST_CHECK_EQUAL( data[1] , A0(0,1) );
  BOOST_CHECK_EQUAL( data[2] , A0(1,0) );
  BOOST_CHECK_EQUAL( data[3] , A0(1,1) );

  BOOST_CHECK_EQUAL( data[4] , B0(0,0) );
  BOOST_CHECK_EQUAL( data[5] , B0(0,1) );
  BOOST_CHECK_EQUAL( data[6] , B0(1,0) );
  BOOST_CHECK_EQUAL( data[7] , B0(1,1) );

  BOOST_CHECK_EQUAL( data[8 ] , C0(0,0) );
  BOOST_CHECK_EQUAL( data[9 ] , C0(0,1) );
  BOOST_CHECK_EQUAL( data[10] , C0(1,0) );
  BOOST_CHECK_EQUAL( data[11] , C0(1,1) );

  BOOST_CHECK_EQUAL( data[12] , A1(0,0) );
  BOOST_CHECK_EQUAL( data[13] , A1(0,1) );
  BOOST_CHECK_EQUAL( data[14] , A1(1,0) );
  BOOST_CHECK_EQUAL( data[15] , A1(1,1) );

  BOOST_CHECK_EQUAL( data[16] , B1(0,0) );
  BOOST_CHECK_EQUAL( data[17] , B1(0,1) );
  BOOST_CHECK_EQUAL( data[18] , B1(1,0) );
  BOOST_CHECK_EQUAL( data[19] , B1(1,1) );

  BOOST_CHECK_EQUAL( data[20] , C1(0,0) );
  BOOST_CHECK_EQUAL( data[21] , C1(0,1) );
  BOOST_CHECK_EQUAL( data[22] , C1(1,0) );
  BOOST_CHECK_EQUAL( data[23] , C1(1,1) );

  MatrixD_Array2D<T> array2(array);

  int k = 0;
  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 3; j++)
    {
      MatrixDView<T> A = array2(i,j);

      BOOST_REQUIRE_EQUAL( 2, A.m() );
      BOOST_REQUIRE_EQUAL( 2, A.n() );

      BOOST_CHECK_EQUAL( data[k++], A(0,0) );
      BOOST_CHECK_EQUAL( data[k++], A(0,1) );
      BOOST_CHECK_EQUAL( data[k++], A(1,0) );
      BOOST_CHECK_EQUAL( data[k++], A(1,1) );
    }

  array = 0;
  for (int i = 0; i < 2*2*2*3; i++)
    BOOST_CHECK_EQUAL( 0, array.value(i) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixDView_Array3D_test )
{
  typedef Real T;
  // Data with a 2x3x4 array of 2x3 matrices
  Real data[2*3*4 * 2*3];

  for (int i = 0; i < 2*3*4 * 2*3; i++)
    data[i] = i;

  MatrixDView_Array3D<T> array(data, 2,3, 2,3,4);

  BOOST_REQUIRE_EQUAL(2*3*4, array.size());
  BOOST_REQUIRE_EQUAL(2, array.m());
  BOOST_REQUIRE_EQUAL(3, array.n());

  int n = 0;
  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 3; j++)
      for (int k = 0; k < 4; k++)
      {
        MatrixDView<T> A = array(i,j,k);

        BOOST_REQUIRE_EQUAL( 2, A.m() );
        BOOST_REQUIRE_EQUAL( 3, A.n() );

        BOOST_CHECK_EQUAL( data[n++], A(0,0) );
        BOOST_CHECK_EQUAL( data[n++], A(0,1) );
        BOOST_CHECK_EQUAL( data[n++], A(0,2) );
        BOOST_CHECK_EQUAL( data[n++], A(1,0) );
        BOOST_CHECK_EQUAL( data[n++], A(1,1) );
        BOOST_CHECK_EQUAL( data[n++], A(1,2) );
      }

  n = 0;
  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 3; j++)
      for (int k = 0; k < 4; k++)
      {
        const MatrixDView<T> cA = const_cast<const MatrixDView_Array3D<T>&>(array)(i,j,k);

        BOOST_REQUIRE_EQUAL( 2, cA.m() );
        BOOST_REQUIRE_EQUAL( 3, cA.n() );

        BOOST_CHECK_EQUAL( data[n++], cA(0,0) );
        BOOST_CHECK_EQUAL( data[n++], cA(0,1) );
        BOOST_CHECK_EQUAL( data[n++], cA(0,2) );
        BOOST_CHECK_EQUAL( data[n++], cA(1,0) );
        BOOST_CHECK_EQUAL( data[n++], cA(1,1) );
        BOOST_CHECK_EQUAL( data[n++], cA(1,2) );
      }

  MatrixDView_Array3D<T> array2(array);

  n = 0;
  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 3; j++)
      for (int k = 0; k < 4; k++)
      {
        MatrixDView<T> A = array2(i,j,k);

        BOOST_REQUIRE_EQUAL( 2, A.m() );
        BOOST_REQUIRE_EQUAL( 3, A.n() );

        BOOST_CHECK_EQUAL( data[n++], A(0,0) );
        BOOST_CHECK_EQUAL( data[n++], A(0,1) );
        BOOST_CHECK_EQUAL( data[n++], A(0,2) );
        BOOST_CHECK_EQUAL( data[n++], A(1,0) );
        BOOST_CHECK_EQUAL( data[n++], A(1,1) );
        BOOST_CHECK_EQUAL( data[n++], A(1,2) );
      }

  array = 0;
  for (int i = 0; i < 2*3*4 * 2*3; i++)
  {
    BOOST_CHECK_EQUAL( 0, array.value(i) );
    BOOST_CHECK_EQUAL( 0, data[i] );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_Array3D_test )
{
  typedef Real T;
  // Data with a 2x3x4 array of 2x3 matrices
  Real data[2*3*4 * 2*3];

  for (int i = 0; i < 2*3*4 * 2*3; i++)
    data[i] = i;

  MatrixD_Array3D<T> array(2,3, 2,3,4);

  BOOST_REQUIRE_EQUAL(2*3*4, array.size());
  BOOST_REQUIRE_EQUAL(2, array.m());
  BOOST_REQUIRE_EQUAL(3, array.n());

  for (int i = 0; i < 2*3*4 * 2*3; i++)
  {
    array.value(i) = data[i];
    BOOST_CHECK_EQUAL( data[i], const_cast<const MatrixD_Array3D<T>&>(array).value(i) );
  }

  int n = 0;
  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 3; j++)
      for (int k = 0; k < 4; k++)
      {
        MatrixDView<T> A = array(i,j,k);

        BOOST_REQUIRE_EQUAL( 2, A.m() );
        BOOST_REQUIRE_EQUAL( 3, A.n() );

        BOOST_CHECK_EQUAL( data[n++], A(0,0) );
        BOOST_CHECK_EQUAL( data[n++], A(0,1) );
        BOOST_CHECK_EQUAL( data[n++], A(0,2) );
        BOOST_CHECK_EQUAL( data[n++], A(1,0) );
        BOOST_CHECK_EQUAL( data[n++], A(1,1) );
        BOOST_CHECK_EQUAL( data[n++], A(1,2) );
      }

  MatrixD_Array3D<T> array2(array);

  n = 0;
  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 3; j++)
      for (int k = 0; k < 4; k++)
      {
        MatrixDView<T> A = array2(i,j,k);

        BOOST_REQUIRE_EQUAL( 2, A.m() );
        BOOST_REQUIRE_EQUAL( 3, A.n() );

        BOOST_CHECK_EQUAL( data[n++], A(0,0) );
        BOOST_CHECK_EQUAL( data[n++], A(0,1) );
        BOOST_CHECK_EQUAL( data[n++], A(0,2) );
        BOOST_CHECK_EQUAL( data[n++], A(1,0) );
        BOOST_CHECK_EQUAL( data[n++], A(1,1) );
        BOOST_CHECK_EQUAL( data[n++], A(1,2) );
      }

  array = 0;
  for (int i = 0; i < 2*3*4 * 2*3; i++)
    BOOST_CHECK_EQUAL( 0, array.value(i) );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
