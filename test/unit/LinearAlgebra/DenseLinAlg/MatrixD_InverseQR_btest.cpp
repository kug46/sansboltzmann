// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/InverseQR.h"

#include <iostream>
using namespace SANS::DLA;


//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_InverseQR_Exceptions )
{
  MatrixD<Real> A1(2,2), A1inv(2,2);
  A1 = 0;
  BOOST_CHECK_THROW( A1inv = InverseQR::Inverse(A1), SingularMatrixException );

  MatrixD<Real> A2(3,2), A2inv(2,2);
  A2 = Identity();
  BOOST_CHECK_THROW( A2inv = InverseQR::Inverse(A2), AssertionException );

  MatrixD<Real> A3(2,3), A3inv(2,2);
  A3 = Identity();
  BOOST_CHECK_THROW( A3inv = InverseQR::Inverse(A3), AssertionException );

  MatrixD<Real> A4inv(3,2);
  A1 = Identity();
  BOOST_CHECK_THROW( A4inv = InverseQR::Inverse(A1), AssertionException );

  MatrixD<Real> A5inv(2,3);
  A1 = Identity();
  BOOST_CHECK_THROW( A5inv = InverseQR::Inverse(A1), AssertionException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_InverseQR_Rand )
{
  typedef Real T;
  static const int CacheItems = CACHE_LINE_SIZE/sizeof(Real);

  for ( int n = 1; n <= 2*CacheItems + CacheItems/2; ++n)
  {
    MatrixD<T> A(n,n), tmp(n,n), I(n,n), Ainv(n,n);

    I = Identity();

    for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
        A(i,j) = T(rand() % 101/100.) + 5*I(i,j);

    tmp = A;

    Ainv = InverseQR::Inverse(A);

    I = Ainv*A;

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
      {
        BOOST_CHECK_CLOSE( A(i,j) , tmp(i,j), T(0.0001) );
        if (i == j)
          BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
        else
          BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
      }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_InverseQR_Rand_Expression )
{
  typedef Real T;
  static const int CacheItems = CACHE_LINE_SIZE/sizeof(Real);

  for ( int n = 1; n <= 2*CacheItems + CacheItems/2; ++n)
  {
    MatrixD<T> A(n,n), tmp(n,n), I(n,n), Ainv(n,n);

    I = Identity();

    for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
        A(i,j) = T(rand() % 101/100.);

    tmp = A + 5*I;

    Ainv = InverseQR::Inverse(A + 5*I);

    I = Ainv*tmp;

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
      {
        if (i == j)
          BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
        else
          BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
      }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveQR_1x1 )
{
  typedef Real T;
  MatrixD<T> A(1,1);
  MatrixD<T> b(1, 1);
  MatrixD<T> x(1, 1);

  A = 2;

  b = 3;

  x = InverseQR::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), T(2.)   , T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(3./2.), T(0.0001) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveQR_2x2 )
{
  typedef Real T;

  Real Avals[] = {2, 1,
                  3, 2};

  Real bvals[] = {2,
                  3};

  MatrixD<T> A(2,2,Avals);
  MatrixD<T> b(2,1,bvals);
  MatrixD<T> x(2,1);

  x = InverseQR::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  SANS_CHECK_CLOSE( T(1.), x(0,0), T(1e-12), T(1e-12) );
  SANS_CHECK_CLOSE( T(0.), x(1,0), T(1e-12), T(1e-12) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveQR_2x1_2b )
{
  //Check that b = !A*b works properly
  typedef Real T;

  T Avals[] = {2, 1,
               3, 2};

  T bvals[] = {2,
               3};

  MatrixD<T> A(2,2,Avals);
  MatrixD<T> b(2,1, bvals);

  b = InverseQR::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  SANS_CHECK_CLOSE( T(1.), b(0,0), T(1e-12), T(1e-12) );
  SANS_CHECK_CLOSE( T(0.), b(1,0), T(1e-12), T(1e-12) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveQR_2x2_RHSExpression )
{
  typedef Real T;

  Real Avals[] = {2, 1,
                  3, 2};

  Real bvals[] = {1,
                  2};

  MatrixD<T> A(2,2,Avals);
  MatrixD<T> b1(2,1,bvals), b2(2,1);
  MatrixD<T> x(2,1);

  b2 = 1;

  x = InverseQR::Solve(A, b1 + b2);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  SANS_CHECK_CLOSE( T(1.), x(0,0), T(1e-12), T(1e-12) );
  SANS_CHECK_CLOSE( T(0.), x(1,0), T(1e-12), T(1e-12) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveQR_2x2_InvExpression )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {2,
                  3};

  MatrixD<T> A1(2,2,Avals), A2(2,2,Avals);
  MatrixD<T> b(2,1,bvals);
  MatrixD<T> x(2,1);

  A2 = 1;

  x = InverseQR::Solve(A1 + A2, b);

  SANS_CHECK_CLOSE( T(1.), x(0,0), T(1e-12), T(1e-12) );
  SANS_CHECK_CLOSE( T(0.), x(1,0), T(1e-12), T(1e-12) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveQR_2x2_InvExpression_2b )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {2,
                  3};

  MatrixD<T> A1(2,2,Avals), A2(2,2,Avals);
  MatrixD<T> b(2,1,bvals);

  A2 = 1;

  b = InverseQR::Solve(A1 + A2, b);

  SANS_CHECK_CLOSE( T(1.), b(0,0), T(1e-12), T(1e-12) );
  SANS_CHECK_CLOSE( T(0.), b(1,0), T(1e-12), T(1e-12) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveQR_2x2_Expressions )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {1,
                  2};

  MatrixD<T> A1(2,2,Avals), A2(2,2,Avals);
  MatrixD<T> b1(2,1,bvals), b2(2,1);
  MatrixD<T> x(2,1);

  A2 = 1;
  b2 = 1;

  x = InverseQR::Solve(A1 + A2, b1 + b2);

  SANS_CHECK_CLOSE( T(1.), x(0,0), T(1e-12), T(1e-12) );
  SANS_CHECK_CLOSE( T(0.), x(1,0), T(1e-12), T(1e-12) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveQR_2x2_Expressions_2 )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {1,
                  2};

  MatrixD<T> A1(2,2,Avals), A2(2,2,Avals);
  MatrixD<T> b1(2,1,bvals), b2(2,1);
  MatrixD<T> x(2,1);

  A2 = 1;
  b2 = 1;

  x = InverseQR::Solve(A1 + A2, b1 + b2) + b2;

  SANS_CHECK_CLOSE( T(2.), x(0,0), T(1e-12), T(1e-12) );
  SANS_CHECK_CLOSE( T(1.), x(1,0), T(1e-12), T(1e-12) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveQR_2x1_MulScal )
{
  typedef Real T;

  T Avals[] = {2, 1,
               3, 2};

  T bvals[] = {2,
               3};

  MatrixD<T> A(2,2,Avals);
  MatrixD<T> b(2,1, bvals);
  MatrixD<T> x(2,1);

  x = 0; // cppcheck-suppress redundantAssignment
  x = 2*InverseQR::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = InverseQR::Solve(A, b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = InverseQR::Solve(A, b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -InverseQR::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-1.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -InverseQR::Solve(A, b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -InverseQR::Solve(A, b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -(2*InverseQR::Solve(A, b)) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-4.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveQR_3x3 )
{
  typedef Real T;

  Real Adata[] = {4, 2, 2,
                  2, 4, 6,
                  2, 6, 6};

  Real bdata[] = {1, 3, 2};

  MatrixD<T> A(3,3, Adata);
  MatrixD<T> b(3,1, bdata);


  MatrixD<T> x = InverseQR::Solve(A, b);

  BOOST_CHECK_CLOSE( x(0,0),   T(0.1), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0),  T(-0.5), T(0.0001) );
  BOOST_CHECK_CLOSE( x(2,0), T(4./5.), T(0.0001) );


  Real Bdata[] = {1, 2, 1,
                  2, 3, 1,
                  1, 1, 2};

  MatrixD<T> B(3,3, Bdata);

  MatrixD<T> X = InverseQR::Solve(A, B);

  BOOST_CHECK_CLOSE( X(0,0), T( 1./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(0,1), T( 1./2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(0,2), T(1./10.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,0), T(-1./2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,1), T(-1.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,2), T( 1./2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,0), T( 3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,1), T( 1.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,2), T(-1./5.), T(0.0001) );

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      BOOST_CHECK_CLOSE( B(i,j), Bdata[3*i+j], T(0.0001) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveQR_2x2_NonSymVec )
{
  typedef Real T;

  Real Adata[] = {1, 2,
                  5, 6};

  Real bdata[] = {2, 4, 5,
                  3, 2, 1};

  MatrixD<T> A(2,2, Adata);
  MatrixD<T> b(2,3, bdata);

  MatrixD<T> x = InverseQR::Solve(A, b);

  BOOST_CHECK_CLOSE( x(0,0), T(-1.50), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,1), T(-5.00), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,2), T(-7.00), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T( 1.75), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,1), T( 4.50), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,2), T( 6.00), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveQR_2x2_MixedTypes )
{
  typedef Real T;

  MatrixD<T> A = {{2, 1},
                  {3, 2}};
  MatrixD< VectorS<1,T> > b = {{2},
                               {3}};
  MatrixD< VectorS<1,T> > x(2,1);

  x = InverseQR::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0)[0], T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0)[0],        T(1e-12) );

}
/*
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveQR_2x2_MatrixS_1x1 )
{
  typedef Real T;

  MatrixD< MatrixS<1,1,T> > A = {{2, 1},
                                 {3, 2}};
  MatrixD< VectorS<1,T> > b = {{2},
                                 {3}};
  MatrixD< VectorS<1,T> > x(2,1);

  x = InverseQR::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0)(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1)(0,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0)(0,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1)(0,0), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0)[0], T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0)[0], T(0.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveQR_2x2_MatrixS_2x2 )
{
  typedef Real T;

  MatrixS<2,2,T> A11 = {{5, 1},
                        {3, 7}};

  MatrixS<2,2,T> A12 = {{1, 4},
                        {2, 6}};

  MatrixS<2,2,T> A21 = {{1, 2},
                        {2, 1}};

  MatrixS<2,2,T> A22 = {{8, 5},
                        {4, 9}};

  VectorS<2,T> b1 = { 2, 4 };
  VectorS<2,T> b2 = { 3, 5 };

  MatrixD< MatrixS<2,2,T> > A = {{A11, A12},
                                 {A21, A22}};
  MatrixD< VectorS<2,T> > b = {{b1},
                               {b2}};
  MatrixD< VectorS<2,T> > x(2,1);

  MatrixD< MatrixS<2,2,T> > Ainv(2,2), I(2,2);

  Ainv = InverseQR::Inverse(A);

  I = Ainv*A;

  for (int i = 0; i < 2; ++i)
    for (int j = 0; j < 2; ++j)
    {
      if (i == j)
        for (int ii = 0; ii < 2; ++ii)
          for (int jj = 0; jj < 2; ++jj)
          {
            if (ii == jj)
              BOOST_CHECK_CLOSE( I(i,j)(ii,jj), T(1), T(1e-12) );
            else
              BOOST_CHECK_SMALL( I(i,j)(ii,jj), T(1e-12) );
          }
      else
        for (int ii = 0; ii < 2; ++ii)
          for (int jj = 0; jj < 2; ++jj)
            BOOST_CHECK_SMALL( I(i,j)(ii,jj), T(1e-12) );
    }


  x = InverseQR::Solve(A, b);

  //Results from Mathematica
  BOOST_CHECK_CLOSE( x(0,0)[0], T(-23./330.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,0)[1], T( 41./330.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(1,0)[0], T(  1./165.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0)[1], T( 61./110.), T(0.0001) );
}
*/
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SolveQR_3x2_LeastSquares )
{
  typedef Real T;

  MatrixD<T> A= {{2,1},
                 {3,2},
                 {6,9}};
  MatrixD<T> b = {{2},
                  {4},
                  {3}};

  MatrixD<T> x = InverseQR::Solve(A, b);

  BOOST_CHECK_EQUAL( x.m(), 2);
  BOOST_CHECK_EQUAL( x.n(), 1);

  //Results from mathematica
  BOOST_CHECK_CLOSE( x(0,0), T(63./37.), T(1e-12) );
  BOOST_CHECK_CLOSE( x(1,0), T(-59./74.), T(1e-12) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_InverseQR_Rand_Factorize )
{
  typedef Real T;
  static const int CacheItems = CACHE_LINE_SIZE/sizeof(Real);

  for ( int n = 1; n <= 2*CacheItems + CacheItems/2; ++n)
  {
    MatrixD<T> A(n,n), tmp(n,n), I(n,n), Ainv(n,n);

    I = Identity();

    for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
        A(i,j) = T(rand() % 101/100.) + 5*I(i,j);

    tmp = A;

    auto QR = InverseQR::Factorize(A);
    Ainv = QR.backsolve(I);

    I = Ainv*A;

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
      {
        BOOST_CHECK_CLOSE( A(i,j) , tmp(i,j), T(0.0001) );
        if (i == j)
          BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
        else
          BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
      }

    I = Identity();

    // Solve the system a 2nd time and make sure it still works
    Ainv = 0;  // cppcheck-suppress redundantAssignment
    Ainv = QR.backsolve(I);

    I = Ainv*A;

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
      {
        BOOST_CHECK_CLOSE( A(i,j) , tmp(i,j), T(0.0001) );
        if (i == j)
          BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
        else
          BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
      }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_InverseQR_Rand_Expression_Factorize )
{
  typedef Real T;
  static const int CacheItems = CACHE_LINE_SIZE/sizeof(Real);

  for ( int n = 1; n <= 2*CacheItems + CacheItems/2; ++n)
  {
    MatrixD<T> A(n,n), tmp(n,n), I(n,n), Ainv(n,n);

    I = Identity();

    for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
        A(i,j) = T(rand() % 101/100.);

    tmp = A + 5*I;

    auto QR = InverseQR::Factorize(A + 5*I);
    Ainv = QR.backsolve(I);

    I = Ainv*tmp;

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
      {
        if (i == j)
          BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
        else
          BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
      }

    I = Identity();

    // Solve the system a 2nd time and make sure it still works
    Ainv = 0;  // cppcheck-suppress redundantAssignment
    Ainv = QR.backsolve(I);

    I = Ainv*tmp;

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
      {
        if (i == j)
          BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
        else
          BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
      }

    I = Identity();

    // Solve the system a 3rd time and make sure it still works
    Ainv = 0;  // cppcheck-suppress redundantAssignment
    Ainv = QR.backsolve(I - I*I + I);

    I = Ainv*tmp;

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
      {
        if (i == j)
          BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
        else
          BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
      }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_FactorizeQR_Pivot_5x5_I )
{
  typedef Real T;

  //Pivoting check of Ainv = !A*I works properly
  MatrixD<T> Ainv(5,5), I(5,5), tmp(5,5);

  I = Identity();

  Real vals[] = {0, 0, 0, 0, 5,
                 0, 0, 0, 5, 0,
                 0, 0, 5, 0, 0,
                 0, 5, 0, 0, 0,
                 5, 0, 0, 0, 0};

  MatrixD<T> A(5,5,vals);

  tmp = A;
  auto QR = InverseQR::Factorize(A);
  Ainv = QR.backsolve(I);
  I = Ainv*A;

  for (int i = 0; i < 5; ++i)
    for (int j = 0; j < 5; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }
}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_FactorizeQR_Pivot_5x5 )
{
  typedef Real T;

  //Pivoting check of Ainv = !A*I works properly
  MatrixD<T> Ainv(5,5), I(5,5), tmp(5,5);

  I = Identity();

  Real vals[] = {5, 0, 0, 0, 0,
                 0, 0, 5, 0, 0,
                 0, 5, 0, 0, 0,
                 0, 0, 0, 0, 5,
                 0, 0, 0, 5, 0};

  MatrixD<T> A(5,5,vals);

  tmp = A;
  auto QR = InverseQR::Factorize(A);
  Ainv = QR.backsolve(I);
  I = Ainv*A;

  for (int i = 0; i < 5; ++i)
    for (int j = 0; j < 5; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_FactorizeQR_Pivot_7x7 )
{
  typedef Real T;

  //Pivoting check of Ainv = !A*I works properly
  MatrixD<T> Ainv(7,7), I(7,7), tmp(7,7);

  I = Identity();

  Real vals[] = {5, 0, 0, 0, 0, 0, 0,
                 0, 0, 5, 0, 0, 0, 0,
                 0, 0, 0, 5, 0, 0, 0,
                 0, 0, 0, 0, 0, 0, 5,
                 0, 5, 0, 0, 0, 0, 0,
                 0, 0, 0, 0, 0, 5, 0,
                 0, 0, 0, 0, 5, 0, 0};

  MatrixD<T> A(7,7,vals);

  tmp = A;
  auto QR = InverseQR::Factorize(A);
  Ainv = QR.backsolve(I);
  I = Ainv*A;

  for (int i = 0; i < 7; ++i)
    for (int j = 0; j < 7; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeQR_1x1 )
{
  typedef Real T;
  MatrixD<T> A(1,1);
  MatrixD<T> b(1, 1);
  MatrixD<T> x(1, 1);

  A = 2;

  b = 3;

  auto QR = InverseQR::Factorize(A);
  x = QR.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), T(2.)   , T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(3./2.), T(0.0001) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeQR_2x2 )
{
  typedef Real T;

  Real Avals[] = {2, 1,
                  3, 2};

  Real bvals[] = {2,
                  3};

  MatrixD<T> A(2,2,Avals);
  MatrixD<T> b(2,1,bvals);
  MatrixD<T> x(2,1);

  auto QR = InverseQR::Factorize(A);
  x = QR.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(0.0001) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeQR_2x1_2b )
{
  //Check that b = QR.backsolve(b) works properly
  typedef Real T;

  T Avals[] = {2, 1,
               3, 2};

  T bvals[] = {2,
               3};

  MatrixD<T> A(2,2,Avals);
  MatrixD<T> b(2,1, bvals);

  auto QR = InverseQR::Factorize(A);
  b = QR.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( b(1,0),        T(1e-12) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeQR_2x2_RHSExpression )
{
  typedef Real T;

  Real Avals[] = {2, 1,
                  3, 2};

  Real bvals[] = {1,
                  2};

  MatrixD<T> A(2,2,Avals);
  MatrixD<T> b1(2,1,bvals), b2(2,1);
  MatrixD<T> x(2,1);

  b2 = 1;

  auto QR = InverseQR::Factorize(A);
  x = QR.backsolve( b1 + b2 );

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeQR_2x2_InvExpression )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {2,
                  3};

  MatrixD<T> A1(2,2,Avals), A2(2,2,Avals);
  MatrixD<T> b(2,1,bvals);
  MatrixD<T> x(2,1);

  A2 = 1;

  auto QR = InverseQR::Factorize(A1 + A2);
  x = QR.backsolve(b);

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeQR_2x2_InvExpression_2b )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {2,
                  3};

  MatrixD<T> A1(2,2,Avals), A2(2,2,Avals);
  MatrixD<T> b(2,1,bvals);

  A2 = 1;

  auto QR = InverseQR::Factorize(A1 + A2);
  b = QR.backsolve(b);

  BOOST_CHECK_CLOSE( b(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( b(1,0),        T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeQR_2x2_Expressions )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {1,
                  2};

  MatrixD<T> A1(2,2,Avals), A2(2,2,Avals);
  MatrixD<T> b1(2,1,bvals), b2(2,1);
  MatrixD<T> x(2,1);

  A2 = 1;
  b2 = 1;

  auto QR = InverseQR::Factorize(A1 + A2);
  x = QR.backsolve(b1 + b2);

  BOOST_CHECK_CLOSE( x(0,0), T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeQR_2x2_Expressions_2 )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  Real bvals[] = {1,
                  2};

  MatrixD<T> A1(2,2,Avals), A2(2,2,Avals);
  MatrixD<T> b1(2,1,bvals), b2(2,1);
  MatrixD<T> x(2,1);

  A2 = 1;
  b2 = 1;

  auto QR = InverseQR::Factorize(A1 + A2);
  x = QR.backsolve(b1 + b2) + b2;

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(1.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeQR_2x1_MulScal )
{
  //Check that b = !A*b works properly
  typedef Real T;

  T Avals[] = {2, 1,
               3, 2};

  T bvals[] = {2,
               3};

  MatrixD<T> A(2,2,Avals);
  MatrixD<T> b(2,1, bvals);
  MatrixD<T> x(2,1);

  auto QR = InverseQR::Factorize(A);

  x = 0; // cppcheck-suppress redundantAssignment
  x = 2*QR.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = QR.backsolve(b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = QR.backsolve(b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),        T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -QR.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-1.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -QR.backsolve(b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -QR.backsolve(b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-2.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = - 2 * QR.backsolve(b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-4.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0),         T(1e-12) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeQR_3x3 )
{
  typedef Real T;

  Real Adata[] = {4, 2, 2,
                  2, 4, 6,
                  2, 6, 6};

  Real bdata[] = {1, 3, 2};

  MatrixD<T> A(3,3, Adata);
  MatrixD<T> b(3,1, bdata);

  auto QR = InverseQR::Factorize(A);

  MatrixD<T> x = QR.backsolve(b);

  BOOST_CHECK_CLOSE( x(0,0),   T(0.1), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0),  T(-0.5), T(0.0001) );
  BOOST_CHECK_CLOSE( x(2,0), T(4./5.), T(0.0001) );


  Real Bdata[] = {1, 2, 1,
                  2, 3, 1,
                  1, 1, 2};

  MatrixD<T> B(3,3, Bdata);

  MatrixD<T> X = QR.backsolve(B);

  BOOST_CHECK_CLOSE( X(0,0), T( 1./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(0,1), T( 1./2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(0,2), T(1./10.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,0), T(-1./2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,1), T(-1.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,2), T( 1./2.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,0), T( 3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,1), T( 1.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,2), T(-1./5.), T(0.0001) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeQR_2x2_NonSymVec )
{
  typedef Real T;

  Real Adata[] = {1, 2,
                  5, 6};

  Real bdata[] = {2, 4, 5,
                  3, 2, 1};

  MatrixD<T> A(2,2, Adata);
  MatrixD<T> b(2,3, bdata);

  auto QR = InverseQR::Factorize(A);
  MatrixD<T> x = QR.backsolve(b);

  BOOST_CHECK_CLOSE( x(0,0), T(-1.50), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,1), T(-5.00), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,2), T(-7.00), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T( 1.75), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,1), T( 4.50), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,2), T( 6.00), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeQR_2x2_MixedTypes )
{
  typedef Real T;

  MatrixD<T> A = {{2, 1},
                  {3, 2}};
  MatrixD< VectorS<1,T> > b = {{2},
                               {3}};
  MatrixD< VectorS<1,T> > x(2,1);

  auto QR = InverseQR::Factorize(A);
  x = QR.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0)[0], T(1.), T(0.0001) );
  BOOST_CHECK_SMALL( x(1,0)[0],        T(0.0001) );
}
#if 0 //TODO: These functions still need to be implemented
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeQR_2x2_MatrixS_1x1 )
{
  typedef Real T;

  MatrixD< MatrixS<1,1,T> > A = {{2, 1},
                                 {3, 2}};
  MatrixD< VectorS<1,T> > b = {{2},
                                 {3}};
  MatrixD< VectorS<1,T> > x(2,1);

  auto QR = InverseQR::Factorize(A);
  x = QR.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0)(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1)(0,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0)(0,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1)(0,0), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0)[0], T(1.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0)[0], T(0.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeQR_2x2_MatrixS_2x2 )
{
  typedef Real T;

  MatrixS<2,2,T> A11 = {{5, 1},
                        {3, 7}};

  MatrixS<2,2,T> A12 = {{1, 4},
                        {2, 6}};

  MatrixS<2,2,T> A21 = {{1, 2},
                        {2, 1}};

  MatrixS<2,2,T> A22 = {{8, 5},
                        {4, 9}};

  VectorS<2,T> b1 = { 2, 4 };
  VectorS<2,T> b2 = { 3, 5 };

  MatrixD< MatrixS<2,2,T> > A = {{A11, A12},
                                 {A21, A22}};
  MatrixD< VectorS<2,T> > b = {{b1},
                               {b2}};
  MatrixD< VectorS<2,T> > x(2,1);

  MatrixD< MatrixS<2,2,T> > Ainv(2,2), I(2,2);

  I = Identity();

  auto QR = InverseQR::Factorize(A);
  Ainv = QR.backsolve(I);

  I = Ainv*A;

  for (int i = 0; i < 2; ++i)
    for (int j = 0; j < 2; ++j)
    {
      if (i == j)
        for (int ii = 0; ii < 2; ++ii)
          for (int jj = 0; jj < 2; ++jj)
          {
            if (ii == jj)
              BOOST_CHECK_CLOSE( I(i,j)(ii,jj), T(1), T(1e-12) );
            else
              BOOST_CHECK_SMALL( I(i,j)(ii,jj), T(1e-12) );
          }
      else
        for (int ii = 0; ii < 2; ++ii)
          for (int jj = 0; jj < 2; ++jj)
            BOOST_CHECK_SMALL( I(i,j)(ii,jj), T(1e-12) );
    }


  x = QR.backsolve(b);

  //Results from Mathematica
  BOOST_CHECK_CLOSE( x(0,0)[0], T(-23./330.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,0)[1], T( 41./330.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(1,0)[0], T(  1./165.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0)[1], T( 61./110.), T(0.0001) );
}
#endif
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_FactorizeQR_3x2_LeastSquares )
{
  typedef Real T;

  MatrixD<T> A= {{2,1},
                 {3,2},
                 {6,9}};
  MatrixD<T> b = {{2},
                  {4},
                  {3}};

  auto QR = InverseQR::Factorize(A);
  MatrixD<T> x = QR.backsolve(b);

  BOOST_CHECK_EQUAL( x.m(), 2);
  BOOST_CHECK_EQUAL( x.n(), 1);

  //Results from mathematica
  BOOST_CHECK_CLOSE( x(0,0), T(63./37.), T(1e-12) );
  BOOST_CHECK_CLOSE( x(1,0), T(-59./74.), T(1e-12) );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
