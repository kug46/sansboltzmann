// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "chkMatrixD_btest.h"

#include <iostream>
using namespace SANS::DLA;

namespace SANS
{
namespace DLA
{
//Explicitly instantiate the classes to generate all the functions so that coverage information is correct
template class MatrixDView<Real>;

typedef MatrixDView<Real> Mat;
typedef OpAddD< Mat, Mat, true > MatAddT;

template class OpMulD< Mat, Mat >;
template class OpMulD< MatAddT, Mat >;
template class OpMulD< Mat, MatAddT >;
template class OpMulD< MatAddT, MatAddT >;

template class OpAddD< Mat, Mat, true >;
template class OpSubD< Mat, Mat, true >;
template class OpMulDScalar< Mat, true >;
template class OpMulDFactor< Mat, OpMulDScalar<Mat, true> >;
template class OpMulDFactor< OpMulDScalar<Mat, true>, Mat >;


template class OpAddD< Mat, Mat, false >;
template class OpSubD< Mat, Mat, false >;
template class OpMulDScalar< Mat, false >;
template class OpMulDFactor< Mat, OpMulDScalar<Mat, false> >;
template class OpMulDFactor< OpMulDScalar<Mat, false>, Mat >;

}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixDView_ctor )
{

  Real v1[4];
  MatrixDView<Real> M1(v1, 2, 2);
  M1 = 0;
  M1(0,0) = 1;
  M1(1,0) = 3;
  M1(1,1) = 2;

  BOOST_CHECK_EQUAL( 4, M1.size() );
  BOOST_CHECK_EQUAL( 2, M1.m() );
  BOOST_CHECK_EQUAL( 2, M1.n() );
  BOOST_CHECK_EQUAL( 2, M1.stride() );

  BOOST_CHECK_EQUAL( 1, M1(0,0) );
  BOOST_CHECK_EQUAL( 0, M1(0,1) );
  BOOST_CHECK_EQUAL( 3, M1(1,0) );
  BOOST_CHECK_EQUAL( 2, M1(1,1) );

  BOOST_CHECK_EQUAL( 1, const_cast<const MatrixDView<Real>& >(M1)(0,0) );
  BOOST_CHECK_EQUAL( 0, const_cast<const MatrixDView<Real>& >(M1)(0,1) );
  BOOST_CHECK_EQUAL( 3, const_cast<const MatrixDView<Real>& >(M1)(1,0) );
  BOOST_CHECK_EQUAL( 2, const_cast<const MatrixDView<Real>& >(M1)(1,1) );

  BOOST_CHECK_EQUAL( 1, v1[0] );
  BOOST_CHECK_EQUAL( 0, v1[1] );
  BOOST_CHECK_EQUAL( 3, v1[2] );
  BOOST_CHECK_EQUAL( 2, v1[3] );

  Real v2[6];
  v2[2] = 42;
  v2[5] = 1929;
  MatrixDView<Real> M2(v2, 2, 2, 3);
  M2 = 0;
  M2(0,0) = 1;
  M2(1,1) = 2;

  BOOST_CHECK_EQUAL( 4, M2.size() );
  BOOST_CHECK_EQUAL( 2, M2.m() );
  BOOST_CHECK_EQUAL( 2, M2.n() );
  BOOST_CHECK_EQUAL( 3, M2.stride() );

  BOOST_CHECK_EQUAL( 1, M2(0,0) );
  BOOST_CHECK_EQUAL( 0, M2(0,1) );
  BOOST_CHECK_EQUAL( 0, M2(1,0) );
  BOOST_CHECK_EQUAL( 2, M2(1,1) );

  BOOST_CHECK_EQUAL(    1, v2[0] );
  BOOST_CHECK_EQUAL(    0, v2[1] );
  BOOST_CHECK_EQUAL(   42, v2[2] );
  BOOST_CHECK_EQUAL(    0, v2[3] );
  BOOST_CHECK_EQUAL(    2, v2[4] );
  BOOST_CHECK_EQUAL( 1929, v2[5] );


  MatrixDView<Real> M3(v2, 2, 3);

  BOOST_CHECK_EQUAL( 6, M3.size() );
  BOOST_CHECK_EQUAL( 2, M3.m() );
  BOOST_CHECK_EQUAL( 3, M3.n() );
  BOOST_CHECK_EQUAL( 3, M3.stride() );

  BOOST_CHECK_EQUAL(    1, M3(0,0) );
  BOOST_CHECK_EQUAL(    0, M3(0,1) );
  BOOST_CHECK_EQUAL(   42, M3(0,2) );
  BOOST_CHECK_EQUAL(    0, M3(1,0) );
  BOOST_CHECK_EQUAL(    2, M3(1,1) );
  BOOST_CHECK_EQUAL( 1929, M3(1,2) );

  Real v4[4];
  MatrixDView<Real> M4(v4, 2, 2);

  BOOST_CHECK_EQUAL( 4, M4.size() );
  BOOST_CHECK_EQUAL( 2, M4.m() );
  BOOST_CHECK_EQUAL( 2, M4.n() );
  BOOST_CHECK_EQUAL( 2, M4.stride() );

  M4 = Identity();

  BOOST_CHECK_EQUAL(  1, M4(0,0) );
  BOOST_CHECK_EQUAL(  0, M4(0,1) );
  BOOST_CHECK_EQUAL(  0, M4(1,0) );
  BOOST_CHECK_EQUAL(  1, M4(1,1) );

  M3 = Identity();

  BOOST_CHECK_EQUAL( 1, M3(0,0) );
  BOOST_CHECK_EQUAL( 0, M3(0,1) );
  BOOST_CHECK_EQUAL( 0, M3(0,2) );
  BOOST_CHECK_EQUAL( 0, M3(1,0) );
  BOOST_CHECK_EQUAL( 1, M3(1,1) );
  BOOST_CHECK_EQUAL( 0, M3(1,2) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixDView_Assign )
{
  Real v1[4];
  MatrixDView<Real> M1(v1, 2, 2);
  M1 = 0;
  M1(0,0) = 1;
  M1(1,0) = 3;
  M1(1,1) = 2;

  Real v2[4];
  MatrixDView<Real> M2(v2, 2, 2);

  M2 = M1;

  BOOST_CHECK_EQUAL( 1, M2(0,0) );
  BOOST_CHECK_EQUAL( 0, M2(0,1) );
  BOOST_CHECK_EQUAL( 3, M2(1,0) );
  BOOST_CHECK_EQUAL( 2, M2(1,1) );

  M2 = -M1;

  BOOST_CHECK_EQUAL( -1, M2(0,0) );
  BOOST_CHECK_EQUAL(  0, M2(0,1) );
  BOOST_CHECK_EQUAL( -3, M2(1,0) );
  BOOST_CHECK_EQUAL( -2, M2(1,1) );

  M2 = {{1929,42},{-1,-3}};

  BOOST_CHECK_EQUAL( 1929, M2(0,0) );
  BOOST_CHECK_EQUAL(   42, M2(0,1) );
  BOOST_CHECK_EQUAL(   -1, M2(1,0) );
  BOOST_CHECK_EQUAL(   -3, M2(1,1) );

  //Make sure that an exception is thrown when dimensions do not match
  Real v3[6];
  MatrixDView<Real> M3(v3, 2, 3);

  BOOST_CHECK_THROW( M3 = M1, AssertionException );

  MatrixDView<Real> M4(v3, 3, 2);

  BOOST_CHECK_THROW( M4 = M1, AssertionException );

  BOOST_CHECK_THROW( (M2 = {{1929   },{-1,-3}}), AssertionException );
  BOOST_CHECK_THROW( (M2 = {{1929,42},{   -3}}), AssertionException );
  BOOST_CHECK_THROW( (M2 = {{1929,42}        }), AssertionException );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixDView_CompoundAssign )
{
  Real v1[4], v2[4];
  MatrixDView<Real> M1(v1, 2, 2), M2(v2, 2, 2);
  M1 = 0;
  M1(0,0) = 1;
  M1(1,0) = 3;
  M1(1,1) = 2;

  M2 = 0;
  M2(0,0) = 3;
  M2(0,1) = 1;
  M2(1,0) = 2;
  M2(1,1) = 4;

  M2 += M1;

  BOOST_CHECK_EQUAL( 1, M1(0,0) );
  BOOST_CHECK_EQUAL( 0, M1(0,1) );
  BOOST_CHECK_EQUAL( 3, M1(1,0) );
  BOOST_CHECK_EQUAL( 2, M1(1,1) );

  BOOST_CHECK_EQUAL( 4, M2(0,0) );
  BOOST_CHECK_EQUAL( 1, M2(0,1) );
  BOOST_CHECK_EQUAL( 5, M2(1,0) );
  BOOST_CHECK_EQUAL( 6, M2(1,1) );

  M1 -= M2;

  BOOST_CHECK_EQUAL( -3, M1(0,0) );
  BOOST_CHECK_EQUAL( -1, M1(0,1) );
  BOOST_CHECK_EQUAL( -2, M1(1,0) );
  BOOST_CHECK_EQUAL( -4, M1(1,1) );

  BOOST_CHECK_EQUAL( 4, M2(0,0) );
  BOOST_CHECK_EQUAL( 1, M2(0,1) );
  BOOST_CHECK_EQUAL( 5, M2(1,0) );
  BOOST_CHECK_EQUAL( 6, M2(1,1) );

  M2 *= 2;

  BOOST_CHECK_EQUAL(  8, M2(0,0) );
  BOOST_CHECK_EQUAL(  2, M2(0,1) );
  BOOST_CHECK_EQUAL( 10, M2(1,0) );
  BOOST_CHECK_EQUAL( 12, M2(1,1) );

  M2 /= 2;

  BOOST_CHECK_EQUAL( 4, M2(0,0) );
  BOOST_CHECK_EQUAL( 1, M2(0,1) );
  BOOST_CHECK_EQUAL( 5, M2(1,0) );
  BOOST_CHECK_EQUAL( 6, M2(1,1) );

  +M2; //This should do nothing

  BOOST_CHECK_EQUAL( 4, M2(0,0) );
  BOOST_CHECK_EQUAL( 1, M2(0,1) );
  BOOST_CHECK_EQUAL( 5, M2(1,0) );
  BOOST_CHECK_EQUAL( 6, M2(1,1) );


  //Make sure that an exception is thrown when dimensions do not match
  Real v3[6] = {0};
  MatrixDView<Real> M3(v3, 2, 3);
  MatrixDView<Real> M4(v3, 3, 2);

  BOOST_CHECK_THROW( M3 += M1, AssertionException );
  BOOST_CHECK_THROW( M4 += M1, AssertionException );

  BOOST_CHECK_THROW( M3 -= M1, AssertionException );
  BOOST_CHECK_THROW( M4 -= M1, AssertionException );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixDView_Add )
{
  Real v1[4], v2[4], v3[4];
  MatrixDView<Real> M1(v1, 2, 2), M2(v2, 2, 2), M3(v3, 2, 2);
  M1 = 0;
  M1(0,0) = 1;
  M1(1,0) = 3;
  M1(1,1) = 2;

  M2 = 0;
  M2(0,0) = 3;
  M2(0,1) = 1;
  M2(1,0) = 2;
  M2(1,1) = 4;

  M3 = M2 + M1;

  BOOST_CHECK_EQUAL( 1, M1(0,0) );
  BOOST_CHECK_EQUAL( 0, M1(0,1) );
  BOOST_CHECK_EQUAL( 3, M1(1,0) );
  BOOST_CHECK_EQUAL( 2, M1(1,1) );

  BOOST_CHECK_EQUAL( 3, M2(0,0) );
  BOOST_CHECK_EQUAL( 1, M2(0,1) );
  BOOST_CHECK_EQUAL( 2, M2(1,0) );
  BOOST_CHECK_EQUAL( 4, M2(1,1) );

  BOOST_CHECK_EQUAL( 4, M3(0,0) );
  BOOST_CHECK_EQUAL( 1, M3(0,1) );
  BOOST_CHECK_EQUAL( 5, M3(1,0) );
  BOOST_CHECK_EQUAL( 6, M3(1,1) );

  M3 += M2 + M1;

  BOOST_CHECK_EQUAL( 2*4, M3(0,0) );
  BOOST_CHECK_EQUAL( 2*1, M3(0,1) );
  BOOST_CHECK_EQUAL( 2*5, M3(1,0) );
  BOOST_CHECK_EQUAL( 2*6, M3(1,1) );

  M3 = M1 - M2;

  BOOST_CHECK_EQUAL( -2, M3(0,0) );
  BOOST_CHECK_EQUAL( -1, M3(0,1) );
  BOOST_CHECK_EQUAL(  1, M3(1,0) );
  BOOST_CHECK_EQUAL( -2, M3(1,1) );

  M3 += M1 - M2;

  BOOST_CHECK_EQUAL( -2*2, M3(0,0) );
  BOOST_CHECK_EQUAL( -1*2, M3(0,1) );
  BOOST_CHECK_EQUAL(  1*2, M3(1,0) );
  BOOST_CHECK_EQUAL( -2*2, M3(1,1) );

  M3 -= M1 - M2;

  BOOST_CHECK_EQUAL( -2, M3(0,0) );
  BOOST_CHECK_EQUAL( -1, M3(0,1) );
  BOOST_CHECK_EQUAL(  1, M3(1,0) );
  BOOST_CHECK_EQUAL( -2, M3(1,1) );

  M1(0,0) = 1;
  M1(0,1) = 0;
  M1(1,0) = 3;
  M1(1,1) = 2;

  M2(0,0) = 3;
  M2(0,1) = 1;
  M2(1,0) = 2;
  M2(1,1) = 4;

  M3(0,0) = 4;
  M3(0,1) = 3;
  M3(1,0) = 2;
  M3(1,1) = 1;

  Real v4[4];
  MatrixDView<Real> M4(v4, 2, 2);

  M4 = M3 + M2 + M1;

  BOOST_CHECK_EQUAL( 8, M4(0,0) );
  BOOST_CHECK_EQUAL( 4, M4(0,1) );
  BOOST_CHECK_EQUAL( 7, M4(1,0) );
  BOOST_CHECK_EQUAL( 7, M4(1,1) );

  M4 = M3 - M2 + M1;

  BOOST_CHECK_EQUAL(  2, M4(0,0) );
  BOOST_CHECK_EQUAL(  2, M4(0,1) );
  BOOST_CHECK_EQUAL(  3, M4(1,0) );
  BOOST_CHECK_EQUAL( -1, M4(1,1) );

  M4 = M3 + M2 - M1;

  BOOST_CHECK_EQUAL(  6, M4(0,0) );
  BOOST_CHECK_EQUAL(  4, M4(0,1) );
  BOOST_CHECK_EQUAL(  1, M4(1,0) );
  BOOST_CHECK_EQUAL(  3, M4(1,1) );

  //It is ok for what is on the left to be on the right as long as it is the first thing
  M4 = M4 + M3 + M2 - M1;
  BOOST_CHECK_EQUAL(  2*6, M4(0,0) );
  BOOST_CHECK_EQUAL(  2*4, M4(0,1) );
  BOOST_CHECK_EQUAL(  2*1, M4(1,0) );
  BOOST_CHECK_EQUAL(  2*3, M4(1,1) );

  //The same object can be on both left and right as long is only addition/subrtraction and scalar multiplication is involved
  M4 = M3 + M4;
  BOOST_CHECK_EQUAL(  2*6+4, M4(0,0) );
  BOOST_CHECK_EQUAL(  2*4+3, M4(0,1) );
  BOOST_CHECK_EQUAL(  2*1+2, M4(1,0) );
  BOOST_CHECK_EQUAL(  2*3+1, M4(1,1) );

  M4 = M3 - M4;
  BOOST_CHECK_EQUAL( -2*6, M4(0,0) );
  BOOST_CHECK_EQUAL( -2*4, M4(0,1) );
  BOOST_CHECK_EQUAL( -2*1, M4(1,0) );
  BOOST_CHECK_EQUAL( -2*3, M4(1,1) );

  //Make sure that an exception is thrown when dimensions do not match
  Real v6[6] = {0};
  MatrixDView<Real> M23(v6, 2, 3);
  MatrixDView<Real> M32(v6, 3, 2);

  BOOST_CHECK_THROW( M23 =  M2 +  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 = M23 +  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 =  M1 + M23, AssertionException );

  BOOST_CHECK_THROW( M23 +=  M2 +  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 += M23 +  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 +=  M1 + M23, AssertionException );

  BOOST_CHECK_THROW( M23 -=  M2 +  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 -= M23 +  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 -=  M1 + M23, AssertionException );

  BOOST_CHECK_THROW( M32 =  M2 +  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 = M32 +  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 =  M1 + M32, AssertionException );

  BOOST_CHECK_THROW( M32 +=  M2 +  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 += M32 +  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 +=  M1 + M32, AssertionException );

  BOOST_CHECK_THROW( M32 -=  M2 +  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 -= M32 +  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 -=  M1 + M32, AssertionException );

  BOOST_CHECK_THROW( M23 =  M2 -  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 = M23 -  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 =  M1 - M23, AssertionException );

  BOOST_CHECK_THROW( M23 +=  M2 -  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 += M23 -  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 +=  M1 - M23, AssertionException );

  BOOST_CHECK_THROW( M23 -=  M2 -  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 -= M23 -  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 -=  M1 - M23, AssertionException );

  BOOST_CHECK_THROW( M32 =  M2 -  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 = M32 -  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 =  M1 - M32, AssertionException );

  BOOST_CHECK_THROW( M32 +=  M2 -  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 += M32 -  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 +=  M1 - M32, AssertionException );

  BOOST_CHECK_THROW( M32 -=  M2 -  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 -= M32 -  M1, AssertionException );
  BOOST_CHECK_THROW(  M3 -=  M1 - M32, AssertionException );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixDView_ops1 )
{
  const Real data = 3;
  Real v1[1], v2[1], v3[1], v4[1], v5[1];
  MatrixDView<Real> m1(v1, 1, 1);
  MatrixDView<Real> m2(v2, 1, 1);
  MatrixDView<Real> m3(v3, 1, 1), m4(v4, 1, 1), m5(v5, 1, 1);

  m1 = data;
  m2 = data;

  // ctors
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );

  // assignment
  m3 = m1;
  m4 = data;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m3(0,0) );
  BOOST_CHECK_EQUAL(  3, m4(0,0) );

  m1 = m2 = m3 = data;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  3, m3(0,0) );

  m4 = data;
  m1 = m2 = m3 = m4;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  3, m3(0,0) );
  BOOST_CHECK_EQUAL(  3, m4(0,0) );

  // unary
  m2 = +m1;
  m3 = -m1;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL( -3, m3(0,0) );

  // binary accumulation
  m3 = m1;
  m3 *= data;
  BOOST_CHECK_EQUAL(  9, m3(0,0) );

  m3 = data;
  m3 *= m1;
  BOOST_CHECK_EQUAL(  9, m3(0,0) );

  m1 = data;
  m2 = m1;
  m3 = m1;
  m2 += m1;
  m3 -= m1;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  6, m2(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0) );

  // binary operators
  m1 = data;
  //m2 = m1 + data;
  //m3 = m1 - data;
  m4 = m1 * data;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  //BOOST_CHECK_EQUAL(  6, m2(0,0) );
  //BOOST_CHECK_EQUAL(  0, m3(0,0) );
  BOOST_CHECK_EQUAL(  9, m4(0,0) );

  //m2 = data + m1;
  //m3 = data - m1;
  m4 = data * m1;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  //BOOST_CHECK_EQUAL(  6, m2(0,0) );
  //BOOST_CHECK_EQUAL(  0, m3(0,0) );
  BOOST_CHECK_EQUAL(  9, m4(0,0) );

  m1 = m2 = data;
  m3 = m1 + m2;
  m4 = m1 - m2;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  6, m3(0,0) );
  BOOST_CHECK_EQUAL(  0, m4(0,0) );

  // arithmetic combinations

  m1 = m2 = data;
  m3 = m1 + m2;
  m4 = m1 + m2 + m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  6, m3(0,0) );
  BOOST_CHECK_EQUAL( 12, m4(0,0) );

  m2 += m1;
  m3 += m1 + m2;
  m4 += m1 + m2 + m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  6, m2(0,0) );
  BOOST_CHECK_EQUAL( 15, m3(0,0) );
  BOOST_CHECK_EQUAL( 36, m4(0,0) );

  m3 = m1 - m2;
  m4 = m1 - m2 - m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  6, m2(0,0) );
  BOOST_CHECK_EQUAL( -3, m3(0,0) );
  BOOST_CHECK_EQUAL(  0, m4(0,0) );

  m2 -= m1;
  m3 -= m1 - m2;
  m4 -= m1 - m2 - m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL( -3, m3(0,0) );
  BOOST_CHECK_EQUAL( -3, m4(0,0) );

  m3 = m1 - m2;
  m4 = m1 + m2 - m3;
  m5 = m1 - m2 + m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0) );
  BOOST_CHECK_EQUAL(  6, m4(0,0) );
  BOOST_CHECK_EQUAL(  0, m5(0,0) );

  m5 = (m1 + m2) + (m3 + m4);
  BOOST_CHECK_EQUAL( 12, m5(0,0) );
  m5 = (m1 + m2) + (m3 - m4);
  BOOST_CHECK_EQUAL(  0, m5(0,0) );
  m5 = (m1 + m2) - (m3 + m4);
  BOOST_CHECK_EQUAL(  0, m5(0,0) );
  m5 = (m1 + m2) - (m3 - m4);
  BOOST_CHECK_EQUAL( 12, m5(0,0) );
  m5 = (m1 - m2) + (m3 + m4);
  BOOST_CHECK_EQUAL(  6, m5(0,0) );
  m5 = (m1 - m2) + (m3 - m4);
  BOOST_CHECK_EQUAL( -6, m5(0,0) );
  m5 = (m1 - m2) - (m3 + m4);
  BOOST_CHECK_EQUAL( -6, m5(0,0) );
  m5 = (m1 - m2) - (m3 - m4);
  BOOST_CHECK_EQUAL(  6, m5(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0) );
  BOOST_CHECK_EQUAL(  6, m4(0,0) );

  m5 += (m1 + m2) + (m3 + m4);
  m5 += (m1 + m2) + (m3 - m4);
  m5 += (m1 + m2) - (m3 + m4);
  m5 += (m1 + m2) - (m3 - m4);
  m5 += (m1 - m2) + (m3 + m4);
  m5 += (m1 - m2) + (m3 - m4);
  m5 += (m1 - m2) - (m3 + m4);
  m5 += (m1 - m2) - (m3 - m4);
  BOOST_CHECK_EQUAL( 30, m5(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0) );
  BOOST_CHECK_EQUAL(  6, m4(0,0) );

  m5 -= (m1 + m2) + (m3 + m4);
  m5 -= (m1 + m2) + (m3 - m4);
  m5 -= (m1 + m2) - (m3 + m4);
  m5 -= (m1 + m2) - (m3 - m4);
  m5 -= (m1 - m2) + (m3 + m4);
  m5 -= (m1 - m2) + (m3 - m4);
  m5 -= (m1 - m2) - (m3 + m4);
  m5 -= (m1 - m2) - (m3 - m4);
  BOOST_CHECK_EQUAL(  6, m5(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0) );
  BOOST_CHECK_EQUAL(  6, m4(0,0) );

  m1 = data;

  m2 = 4*m1;
  m3 = m2*7;
  BOOST_CHECK_EQUAL(   3, m1(0,0) );
  BOOST_CHECK_EQUAL(  12, m2(0,0) );
  BOOST_CHECK_EQUAL(  84, m3(0,0) );

  m2 = m3/7;
  m1 = m2/4;
  BOOST_CHECK_EQUAL(   3, m1(0,0) );
  BOOST_CHECK_EQUAL(  12, m2(0,0) );
  BOOST_CHECK_EQUAL(  84, m3(0,0) );

  m2 += 4*m1;
  m3 += m2*7;
  BOOST_CHECK_EQUAL(   3, m1(0,0) );
  BOOST_CHECK_EQUAL(  24, m2(0,0) );
  BOOST_CHECK_EQUAL( 252, m3(0,0) );

  m2 -= 4*m1;
  m3 -= m2*7;
  BOOST_CHECK_EQUAL(   3, m1(0,0) );
  BOOST_CHECK_EQUAL(  12, m2(0,0) );
  BOOST_CHECK_EQUAL( 168, m3(0,0) );

  m2 = 2*m1/7;
  m3 = 2*(m2*4);
  BOOST_CHECK_EQUAL(             3, m1(0,0) );
  BOOST_CHECK_EQUAL(      2.*3./7., m2(0,0) );
  BOOST_CHECK_EQUAL( 2*2.*3./7.*4., m3(0,0) );

  //Check multiplication with 1x1 matricies
  m1 = 2;
  m2 = 3;
  m3 = m1*m2;
  BOOST_CHECK_EQUAL( 6, m3(0,0) );
  m3 += m1*m2;
  BOOST_CHECK_EQUAL( 2*6, m3(0,0) );
  m3 -= m1*m2;
  BOOST_CHECK_EQUAL( 6, m3(0,0) );

  m1 = data;
  m2 = 4*data;
  m3 = 168;

  m5 = 2*(m1 + m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL(  552, m5(0,0) );
  m5 = 2*(m1 + m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  516, m5(0,0) );
  m5 = 2*(m1 + m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -492, m5(0,0) );
  m5 = 2*(m1 + m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -456, m5(0,0) );
  m5 = 2*(m1 - m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL(  504, m5(0,0) );
  m5 = 2*(m1 - m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  468, m5(0,0) );
  m5 = 2*(m1 - m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -540, m5(0,0) );
  m5 = 2*(m1 - m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -504, m5(0,0) );
  BOOST_CHECK_EQUAL(    3, m1(0,0) );
  BOOST_CHECK_EQUAL(   12, m2(0,0) );
  BOOST_CHECK_EQUAL(  168, m3(0,0) );
  BOOST_CHECK_EQUAL(    6, m4(0,0) );

  m5 += 2*(m1 + m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL(   48, m5(0,0) );
  m5 += 2*(m1 + m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  564, m5(0,0) );
  m5 += 2*(m1 + m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL(   72, m5(0,0) );
  m5 += 2*(m1 + m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -384, m5(0,0) );
  m5 += 2*(m1 - m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL(  120, m5(0,0) );
  m5 += 2*(m1 - m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  588, m5(0,0) );
  m5 += 2*(m1 - m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL(   48, m5(0,0) );
  m5 += 2*(m1 - m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -456, m5(0,0) );
  BOOST_CHECK_EQUAL(    3, m1(0,0) );
  BOOST_CHECK_EQUAL(   12, m2(0,0) );
  BOOST_CHECK_EQUAL(  168, m3(0,0) );
  BOOST_CHECK_EQUAL(    6, m4(0,0) );

  m5 -= 2*(m1 + m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -1008, m5(0,0) );
  m5 -= 2*(m1 + m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -1524, m5(0,0) );
  m5 -= 2*(m1 + m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -1032, m5(0,0) );
  m5 -= 2*(m1 + m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  -576, m5(0,0) );
  m5 -= 2*(m1 - m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -1080, m5(0,0) );
  m5 -= 2*(m1 - m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -1548, m5(0,0) );
  m5 -= 2*(m1 - m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -1008, m5(0,0) );
  m5 -= 2*(m1 - m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  -504, m5(0,0) );
  BOOST_CHECK_EQUAL(     3, m1(0,0) );
  BOOST_CHECK_EQUAL(    12, m2(0,0) );
  BOOST_CHECK_EQUAL(   168, m3(0,0) );
  BOOST_CHECK_EQUAL(     6, m4(0,0) );

  m5 = 2*(m1 + m2)*3;
  BOOST_CHECK_EQUAL( 90, m5(0,0) );
  m5 = 2*3*(m1 + m2);
  BOOST_CHECK_EQUAL( 90, m5(0,0) );
  m5 = (m1 + m2)*2*3;
  BOOST_CHECK_EQUAL( 90, m5(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL( 12, m2(0,0) );

  m2 = +m1;
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  m3 = -m2;
  BOOST_CHECK_EQUAL( -3, m3(0,0) );
  m4 = +(m1 + m2);
  BOOST_CHECK_EQUAL(  6, m4(0,0) );
  m4 = +(m1 - m2);
  BOOST_CHECK_EQUAL(  0, m4(0,0) );
  m4 = -(m1 + m2);
  BOOST_CHECK_EQUAL( -6, m4(0,0) );
  m4 = -(m1 - m2);
  BOOST_CHECK_EQUAL(  0, m4(0,0) );
  m4 = +(m1 + m2) + m3;
  BOOST_CHECK_EQUAL(  3, m4(0,0) );
  m4 = -(m1 + m2) + m3;
  BOOST_CHECK_EQUAL( -9, m4(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL( -3, m3(0,0) );

  m4 = +5*m1;
  BOOST_CHECK_EQUAL(  15, m4(0,0) );
  m4 = -5*m1;
  BOOST_CHECK_EQUAL( -15, m4(0,0) );
  m4 = +m1*5;
  BOOST_CHECK_EQUAL(  15, m4(0,0) );
  m4 = -m1*5;
  BOOST_CHECK_EQUAL( -15, m4(0,0) );
  m4 = +(5*m1);
  BOOST_CHECK_EQUAL(  15, m4(0,0) );
  m4 = -(5*m1);
  BOOST_CHECK_EQUAL( -15, m4(0,0) );
  m4 = +(m1*5);
  BOOST_CHECK_EQUAL(  15, m4(0,0) );
  m4 = -(m1*5);
  BOOST_CHECK_EQUAL( -15, m4(0,0) );
  BOOST_CHECK_EQUAL(   3, m1(0,0) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixDView_ops2 )
{

  Real v1[4] = {1,2,3,4}, v2[4], v3[4], v4[4], v5[4];
  MatrixDView<Real> m1(v1, 2, 2);
  MatrixDView<Real> m2(v2, 2, 2);
  MatrixDView<Real> m3(v3, 2, 2), m4(v4, 2, 2), m5(v5, 2, 2);

  m2 = m1;

  // ctors
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m2, 1,2,3,4 ) );

  // assignment
  m3 = m1;
  m4 = 5;
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m3, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m4, 5,5,5,5 ) );

  m2 = m3 = 3;
  BOOST_CHECK( chkMatrixD22( m2, 3,3,3,3 ) );
  BOOST_CHECK( chkMatrixD22( m3, 3,3,3,3 ) );

//  m3 = m2 = {1, 2,
//             3, 4};
//  BOOST_CHECK( chkMatrixD22( m2, 1,2,3,4 ) );
//  BOOST_CHECK( chkMatrixD22( m3, 1,2,3,4 ) );

  // unary
  m2 = +m1;
  m3 = -m1;
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m3, -1,-2,-3,-4 ) );

  // binary accumulation
  m4 = m1;
  m4 *= 5;
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m4, 5,10,15,20 ) );

  m2 = 5;
  m3 = 5;
  m2 += m1;
  m3 -= m1;
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m2, 6,7,8,9 ) );
  BOOST_CHECK( chkMatrixD22( m3, 4,3,2,1 ) );

  // binary operators
  //m2 = m1 + 3;
  //m3 = m1 - 3;
  m4 = m1 * 3;
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  //BOOST_CHECK( chkMatrixD22( m2, 4,5,6,7 ) );
  //BOOST_CHECK( chkMatrixD22( m3, -2,-1,0,1 ) );
  BOOST_CHECK( chkMatrixD22( m4, 3,6,9,12 ) );

  //m2 = 3 + m1;
  //m3 = 3 - m1;
  m4 = 3 * m1;
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  //BOOST_CHECK( chkMatrixD22( m2, 4,5,6,7 ) );
  //BOOST_CHECK( chkMatrixD22( m3, 2,1,0,-1 ) );
  BOOST_CHECK( chkMatrixD22( m4, 3,6,9,12 ) );

  m2 = 3;
  m3 = m1 + m2;
  m4 = m1 - m2;
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m2, 3,3,3,3 ) );
  BOOST_CHECK( chkMatrixD22( m3, 4,5,6,7 ) );
  BOOST_CHECK( chkMatrixD22( m4, -2,-1,0,1 ) );

  // arithmetic combinations

  m2 = m1;
  m3 = m1 + m2;
  m4 = m1 + m2 + m3;
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m3, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixD22( m4, 4,8,12,16 ) );

  m2 += m1;
  m3 += m1 + m2;
  m4 += m1 + m2 + m3;
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m2, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixD22( m3, 5,10,15,20 ) );
  BOOST_CHECK( chkMatrixD22( m4, 12,24,36,48 ) );

  m3 = m1 - m2;
  m4 = m1 - m2 - m3;
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m2, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixD22( m3, -1,-2,-3,-4 ) );
  BOOST_CHECK( chkMatrixD22( m4, 0,0,0,0 ) );

  m2 -= m1;
  m3 -= m1 - m2;
  m4 -= m1 - m2 - m3;
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m3, -1,-2,-3,-4 ) );
  BOOST_CHECK( chkMatrixD22( m4, -1,-2,-3,-4 ) );

  m3 = m1 - m2;
  m4 = m1 + m2 - m3;
  m5 = m1 - m2 + m3;
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m3, 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixD22( m4, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixD22( m5, 0,0,0,0 ) );

  m5 = (m1 + m2) + (m3 + m4);
  BOOST_CHECK( chkMatrixD22( m5, 4,8,12,16 ) );
  m5 = (m1 + m2) + (m3 - m4);
  BOOST_CHECK( chkMatrixD22( m5, 0,0,0,0 ) );
  m5 = (m1 + m2) - (m3 + m4);
  BOOST_CHECK( chkMatrixD22( m5, 0,0,0,0 ) );
  m5 = (m1 + m2) - (m3 - m4);
  BOOST_CHECK( chkMatrixD22( m5, 4,8,12,16 ) );
  m5 = (m1 - m2) + (m3 + m4);
  BOOST_CHECK( chkMatrixD22( m5, 2,4,6,8 ) );
  m5 = (m1 - m2) + (m3 - m4);
  BOOST_CHECK( chkMatrixD22( m5, -2,-4,-6,-8 ) );
  m5 = (m1 - m2) - (m3 + m4);
  BOOST_CHECK( chkMatrixD22( m5, -2,-4,-6,-8 ) );
  m5 = (m1 - m2) - (m3 - m4);
  BOOST_CHECK( chkMatrixD22( m5, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m3, 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixD22( m4, 2,4,6,8 ) );

  m5 += (m1 + m2) + (m3 + m4);
  m5 += (m1 + m2) + (m3 - m4);
  m5 += (m1 + m2) - (m3 + m4);
  m5 += (m1 + m2) - (m3 - m4);
  m5 += (m1 - m2) + (m3 + m4);
  m5 += (m1 - m2) + (m3 - m4);
  m5 += (m1 - m2) - (m3 + m4);
  m5 += (m1 - m2) - (m3 - m4);
  BOOST_CHECK( chkMatrixD22( m5, 10,20,30,40 ) );
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m3, 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixD22( m4, 2,4,6,8 ) );

  m5 -= (m1 + m2) + (m3 + m4);
  m5 -= (m1 + m2) + (m3 - m4);
  m5 -= (m1 + m2) - (m3 + m4);
  m5 -= (m1 + m2) - (m3 - m4);
  m5 -= (m1 - m2) + (m3 + m4);
  m5 -= (m1 - m2) + (m3 - m4);
  m5 -= (m1 - m2) - (m3 + m4);
  m5 -= (m1 - m2) - (m3 - m4);
  BOOST_CHECK( chkMatrixD22( m5, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m3, 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixD22( m4, 2,4,6,8 ) );

  m2 = 1*m1;
  m3 = m2*2;
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m3, 2,4,6,8 ) );

  m2 += 1*m1;
  m3 += m2*2;
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m2, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixD22( m3, 6,12,18,24 ) );

  m2 -= 1*m1;
  m3 -= m2*2;
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m3, 4,8,12,16 ) );

  m5 = 1*(m1 + m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, 14,28,42,56 ) );
  m5 = 1*(m1 + m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, 6,12,18,24 ) );
  m5 = 1*(m1 + m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, -10,-20,-30,-40 ) );
  m5 = 1*(m1 + m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, -2,-4,-6,-8 ) );
  m5 = 1*(m1 - m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, 12,24,36,48 ) );
  m5 = 1*(m1 - m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, 4,8,12,16 ) );
  m5 = 1*(m1 - m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, -12,-24,-36,-48 ) );
  m5 = 1*(m1 - m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, -4,-8,-12,-16 ) );
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m3, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixD22( m4, 2,4,6,8 ) );

  m5 += 1*(m1 + m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, 10,20,30,40 ) );
  m5 += 1*(m1 + m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, 16,32,48,64 ) );
  m5 += 1*(m1 + m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, 6,12,18,24 ) );
  m5 += 1*(m1 + m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, 4,8,12,16 ) );
  m5 += 1*(m1 - m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, 16,32,48,64 ) );
  m5 += 1*(m1 - m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, 20,40,60,80 ) );
  m5 += 1*(m1 - m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, 8,16,24,32 ) );
  m5 += 1*(m1 - m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m3, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixD22( m4, 2,4,6,8 ) );

  m5 -= 1*(m1 + m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, -10,-20,-30,-40 ) );
  m5 -= 1*(m1 + m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, -16,-32,-48,-64 ) );
  m5 -= 1*(m1 + m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, -6,-12,-18,-24 ) );
  m5 -= 1*(m1 + m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, -4,-8,-12,-16 ) );
  m5 -= 1*(m1 - m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, -16,-32,-48,-64 ) );
  m5 -= 1*(m1 - m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, -20,-40,-60,-80 ) );
  m5 -= 1*(m1 - m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, -8,-16,-24,-32 ) );
  m5 -= 1*(m1 - m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixD22( m5, -4,-8,-12,-16 ) );
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m3, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixD22( m4, 2,4,6,8 ) );

  m5 = 1*(m1 + m2)*2;
  BOOST_CHECK( chkMatrixD22( m5, 4,8,12,16 ) );
  m5 = 1*2*(m1 + m2);
  BOOST_CHECK( chkMatrixD22( m5, 4,8,12,16 ) );
  m5 = (m1 + m2)*1*2;
  BOOST_CHECK( chkMatrixD22( m5, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m2, 1,2,3,4 ) );

  m2 = +m1;
  BOOST_CHECK( chkMatrixD22( m2, 1,2,3,4 ) );
  m3 = -m2;
  BOOST_CHECK( chkMatrixD22( m3, -1,-2,-3,-4 ) );
  m4 = +(m1 + m2);
  BOOST_CHECK( chkMatrixD22( m4, 2,4,6,8 ) );
  m4 = +(m1 - m2);
  BOOST_CHECK( chkMatrixD22( m4, 0,0,0,0 ) );
  m4 = -(m1 + m2);
  BOOST_CHECK( chkMatrixD22( m4, -2,-4,-6,-8 ) );
  m4 = -(m1 - m2);
  BOOST_CHECK( chkMatrixD22( m4, 0,0,0,0 ) );
  m4 = +(m1 + m2) + m3;
  BOOST_CHECK( chkMatrixD22( m4, 1,2,3,4 ) );
  m4 = -(m1 + m2) + m3;
  BOOST_CHECK( chkMatrixD22( m4, -3,-6,-9,-12 ) );
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixD22( m3, -1,-2,-3,-4 ) );

  m4 = +1*m1;
  BOOST_CHECK( chkMatrixD22( m4, 1,2,3,4 ) );
  m4 = -1*m1;
  BOOST_CHECK( chkMatrixD22( m4, -1,-2,-3,-4 ) );
  m4 = +m1*1;
  BOOST_CHECK( chkMatrixD22( m4, 1,2,3,4 ) );
  m4 = -m1*1;
  BOOST_CHECK( chkMatrixD22( m4, -1,-2,-3,-4 ) );
  m4 = +(1*m1);
  BOOST_CHECK( chkMatrixD22( m4, 1,2,3,4 ) );
  m4 = -(1*m1);
  BOOST_CHECK( chkMatrixD22( m4, -1,-2,-3,-4 ) );
  m4 = +(m1*1);
  BOOST_CHECK( chkMatrixD22( m4, 1,2,3,4 ) );
  m4 = -(m1*1);
  BOOST_CHECK( chkMatrixD22( m4, -1,-2,-3,-4 ) );
  BOOST_CHECK( chkMatrixD22( m1, 1,2,3,4 ) );
}


//----------------------------------------------------------------------------//
// matrix-vector multiply
BOOST_AUTO_TEST_CASE( MatrixDView_Vector_Multiply2 )
{
  Real mdata1[4] = {3,4,5,6};
  MatrixDView<Real> m1(mdata1, 2, 2);

  Real mdatcol2[4] = {3,4,5,6};
  MatrixDView<Real> m2(mdatcol2, 2, 2);

  Real coldata1[2] = {1,2};
  MatrixDView<Real> col1(coldata1, 2, 1);
  Real coldata2[2];
  MatrixDView<Real> col2(coldata2, 2, 1);
  Real coldata3[2];
  MatrixDView<Real> col3(coldata3, 2, 1);

  Real rowdata1[2] = {1,2};
  MatrixDView<Real> row1(rowdata1, 1, 2);
  Real rowdata2[2] = {1,2};
  MatrixDView<Real> row2(rowdata2, 1, 2);
  Real rowdata3[2];
  MatrixDView<Real> row3(rowdata3, 1, 2);


  //Check column multiplication
  col2 = col1;

  BOOST_CHECK( chkMatrixD21( col1, 1,2 ) );
  BOOST_CHECK( chkMatrixD21( col2, 1,2 ) );

  BOOST_CHECK( chkMatrixD22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixD22( m2, 3,4,5,6 ) );

  col2 = m1*col1;
  BOOST_CHECK( chkMatrixD21( col2, 11,17 ) );
  BOOST_CHECK( chkMatrixD22( m1, 3,4,5,6 ) );

  col2 += m1*col1;
  BOOST_CHECK( chkMatrixD21( col2, 22,34 ) );

  col2 -= m1*col1;
  BOOST_CHECK( chkMatrixD21( col2, 11,17 ) );


  col2 = (2*m1)*col1;
  BOOST_CHECK( chkMatrixD21( col2, 2*11,2*17 ) );

  BOOST_CHECK_EQUAL( ((2*m1)*col1).size(), 2 );

  col2 = +((2*m1)*col1);
  BOOST_CHECK( chkMatrixD21( col2, 2*11,2*17 ) );

  col2 += (2*m1)*col1;
  BOOST_CHECK( chkMatrixD21( col2, 4*11,4*17 ) );

  col2 = m1*(col1*2);
  BOOST_CHECK( chkMatrixD21( col2, 2*11,2*17 ) );

  BOOST_CHECK_EQUAL( (m1*(col1*2)).size(), 2 );

  col2 = +(m1*(col1*2));
  BOOST_CHECK( chkMatrixD21( col2, 2*11,2*17 ) );

  col2 += m1*(col1*2);
  BOOST_CHECK( chkMatrixD21( col2, 4*11,4*17 ) );



  col2 = (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixD21( col2, 22,34 ) );
  BOOST_CHECK( chkMatrixD22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixD22( m2, 3,4,5,6 ) );

  col2 += (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixD21( col2, 2*22,2*34 ) );

  col2 -= (m1 + m2)*col1;
  BOOST_CHECK( chkMatrixD21( col2, 22,34 ) );

  col2 = (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixD21( col2, 0,0 ) );
  BOOST_CHECK( chkMatrixD22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixD22( m2, 3,4,5,6 ) );

  col2 = m1*col1;
  col2 += m1*col1 + m1*col1;
  BOOST_CHECK( chkMatrixD21( col2, 3*11,3*17 ) );

  col2 += m1*col1 - m1*col1;
  BOOST_CHECK( chkMatrixD21( col2, 3*11,3*17 ) );

  col2 = m1*col1 + col1;
  BOOST_CHECK( chkMatrixD21( col2, 11+1,17+2 ) );



  m2(0,0) = 2; m2(0,1) = 3;
  m2(1,0) = 4; m2(1,1) = 5;

  col2(0,0) = 1;
  col2(1,0) = 2;

  col2 += (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixD21( col2, 4,5 ) );

  col2(0,0) = 7;
  col2(1,0) = 8;

  col2 -= (m1 - m2)*col1;
  BOOST_CHECK( chkMatrixD21( col2, 4,5 ) );

  m2 = m1;
  col2 = col1;

  col3 = (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixD21( col3, 44,68 ) );

  col3 += (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixD21( col3, 2*44,2*68 ) );

  col3 -= (m1 + m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixD21( col3, 44,68 ) );

  col3 = (m1 + m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixD21( col3, 0,0 ) );

  col3 = (m1 - m2)*(col1 + col2);
  BOOST_CHECK( chkMatrixD21( col3, 0,0 ) );

  col3 = (m1 - m2)*(col1 - col2);
  BOOST_CHECK( chkMatrixD21( col3, 0,0 ) );


  //Check row multiplication
  row2 = row1*m1;
  BOOST_CHECK( chkMatrixD12( row2, 13,16 ) );
  BOOST_CHECK( chkMatrixD22( m1, 3,4,5,6 ) );

  row2 = (2*row1)*m1;
  BOOST_CHECK( chkMatrixD12( row2, 2*13,2*16 ) );

  row2 = row1*(m1*2);
  BOOST_CHECK( chkMatrixD12( row2, 2*13,2*16 ) );

  row2 = row1*(m1 + m2);
  BOOST_CHECK( chkMatrixD12( row2, 26,32 ) );
  BOOST_CHECK( chkMatrixD22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixD22( m2, 3,4,5,6 ) );

  row2 += row1*(m1 + m2);
  BOOST_CHECK( chkMatrixD12( row2, 2*26,2*32 ) );

  row2 -= row1*(m1 + m2);
  BOOST_CHECK( chkMatrixD12( row2, 26,32 ) );

  row2 = row1*(m1 - m2);
  BOOST_CHECK( chkMatrixD12( row2, 0,0 ) );
  BOOST_CHECK( chkMatrixD22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixD22( m2, 3,4,5,6 ) );


  row2 = row1;

  row3 = (row1 + row2)*m1;
  BOOST_CHECK( chkMatrixD12( row3, 26,32 ) );

  row3 = (row1 + row2)*(m1 + m2);
  BOOST_CHECK( chkMatrixD12( row3, 52,64 ) );

  row3 = (row1 - row2)*(m1 + m2);
  BOOST_CHECK( chkMatrixD12( row3, 0,0 ) );

  row3 = (row1 + row2)*(m1 - m2);
  BOOST_CHECK( chkMatrixD12( row3, 0,0 ) );

  row3 = (row1 - row2)*(m1 - m2);
  BOOST_CHECK( chkMatrixD12( row3, 0,0 ) );
  BOOST_CHECK( chkMatrixD12( row1, 1,2 ) );
  BOOST_CHECK( chkMatrixD12( row2, 1,2 ) );

  BOOST_CHECK( chkMatrixD22( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixD22( m2, 3,4,5,6 ) );

  Real valdata1[1];
  MatrixDView<Real> val(valdata1, 1, 1);

  val = row1*col1;
  BOOST_CHECK_EQUAL( val(0,0), 5 );

  val = row1*m1*col1;
  BOOST_CHECK_EQUAL( val(0,0), 45 );

  //It is ok for what is on the left to be on the right as long as it is the first thing and not part of a multiplication
  col2 = col1;
  col2 = col2 + m1*col1;
  BOOST_CHECK( chkMatrixD21( col2, 11+1,17+2 ) );

  //This is not ok
  BOOST_CHECK_THROW( col2 = col1 + m1*col2;, AssertionException );

  //Make sure that an exception is thrown when dimensions do not match
  //or when the same veriable is used on the left and right as part of a multiplication

  BOOST_CHECK_THROW( row2  = m1*col1, AssertionException );
  BOOST_CHECK_THROW( row2 += m1*col1, AssertionException );
  BOOST_CHECK_THROW( row2 -= m1*col1, AssertionException );

  BOOST_CHECK_THROW( col2  = row1*m1, AssertionException );
  BOOST_CHECK_THROW( col2 += row1*m1, AssertionException );
  BOOST_CHECK_THROW( col2 -= row1*m1, AssertionException );

  BOOST_CHECK_THROW( row2  = col1*m1, AssertionException );
  BOOST_CHECK_THROW( row2 += col1*m1, AssertionException );
  BOOST_CHECK_THROW( row2 -= col1*m1, AssertionException );

  BOOST_CHECK_THROW( col2  = m1*row1, AssertionException );
  BOOST_CHECK_THROW( col2 += m1*row1, AssertionException );
  BOOST_CHECK_THROW( col2 -= m1*row1, AssertionException );

  BOOST_CHECK_THROW( m2  = m2*m1, AssertionException );
  BOOST_CHECK_THROW( m2 += m2*m1, AssertionException );
  BOOST_CHECK_THROW( m2 -= m2*m1, AssertionException );

  BOOST_CHECK_THROW( m2  = m1*m2, AssertionException );
  BOOST_CHECK_THROW( m2 += m1*m2, AssertionException );
  BOOST_CHECK_THROW( m2 -= m1*m2, AssertionException );

  BOOST_CHECK_THROW( col1  = m1*col1, AssertionException );
  BOOST_CHECK_THROW( col1 += m1*col1, AssertionException );
  BOOST_CHECK_THROW( col1 -= m1*col1, AssertionException );

  BOOST_CHECK_THROW( row1  = row1*m1, AssertionException );
  BOOST_CHECK_THROW( row1 += row1*m1, AssertionException );
  BOOST_CHECK_THROW( row1 -= row1*m1, AssertionException );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixDView_sub_matrix )
{

  Real v1[4];
  MatrixDView<Real> M1(v1, 2, 2);
  M1 = 0;
  M1(0,0) = 1;
  M1(1,0) = 3;
  M1(1,1) = 2;

  MatrixDView<Real> row0 = M1.row(0);
  MatrixDView<Real> row1 = M1.row(1);

  BOOST_CHECK_EQUAL( 2, row0.size() );
  BOOST_CHECK_EQUAL( 1, row0.m() );
  BOOST_CHECK_EQUAL( 2, row0.n() );
  BOOST_CHECK_EQUAL( 2, row0.stride() );

  BOOST_CHECK_EQUAL( 2, row1.size() );
  BOOST_CHECK_EQUAL( 1, row1.m() );
  BOOST_CHECK_EQUAL( 2, row1.n() );
  BOOST_CHECK_EQUAL( 2, row1.stride() );

  BOOST_CHECK_EQUAL( 1, row0(0,0) );
  BOOST_CHECK_EQUAL( 0, row0(0,1) );
  BOOST_CHECK_EQUAL( 3, row1(0,0) );
  BOOST_CHECK_EQUAL( 2, row1(0,1) );

  MatrixDView<Real> col0 = M1.col(0);
  MatrixDView<Real> col1 = M1.col(1);

  BOOST_CHECK_EQUAL( 2, col0.size() );
  BOOST_CHECK_EQUAL( 2, col0.m() );
  BOOST_CHECK_EQUAL( 1, col0.n() );
  BOOST_CHECK_EQUAL( 2, col0.stride() );

  BOOST_CHECK_EQUAL( 2, col1.size() );
  BOOST_CHECK_EQUAL( 2, col1.m() );
  BOOST_CHECK_EQUAL( 1, col1.n() );
  BOOST_CHECK_EQUAL( 2, col1.stride() );

  BOOST_CHECK_EQUAL( 1, col0(0,0) );
  BOOST_CHECK_EQUAL( 0, col1(0,0) );
  BOOST_CHECK_EQUAL( 3, col0(1,0) );
  BOOST_CHECK_EQUAL( 2, col1(1,0) );

  MatrixDView<Real> sub = M1.sub(1,1,1,1);

  BOOST_CHECK_EQUAL( 1, sub.size() );
  BOOST_CHECK_EQUAL( 1, sub.m() );
  BOOST_CHECK_EQUAL( 1, sub.n() );
  BOOST_CHECK_EQUAL( 2, col0.stride() );

  BOOST_CHECK_EQUAL( 2, sub(0,0) );


  VectorDView<Real> subcol0 = M1.subcol(1,0,1);
  VectorDView<Real> subcol1 = M1.subcol(1,1,1);

  BOOST_CHECK_EQUAL( 1, subcol0.size() );
  BOOST_CHECK_EQUAL( 1, subcol0.m() );
  BOOST_CHECK_EQUAL( 1, subcol0.n() );
  BOOST_CHECK_EQUAL( 2, subcol0.stride() );

  BOOST_CHECK_EQUAL( 1, subcol1.size() );
  BOOST_CHECK_EQUAL( 1, subcol1.m() );
  BOOST_CHECK_EQUAL( 1, subcol1.n() );
  BOOST_CHECK_EQUAL( 2, subcol1.stride() );

  BOOST_CHECK_EQUAL( 3, subcol0[0] );
  BOOST_CHECK_EQUAL( 2, subcol1[0] );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixDView_sub_matrix_const )
{

  Real v1[4];
  MatrixDView<Real> tmp(v1, 2, 2);
  const MatrixDView<Real>& M1 = tmp;
  tmp = 0;
  tmp(0,0) = 1;
  tmp(1,0) = 3;
  tmp(1,1) = 2;

  MatrixDView<Real> row0 = M1.row(0);
  MatrixDView<Real> row1 = M1.row(1);

  BOOST_CHECK_EQUAL( 2, row0.size() );
  BOOST_CHECK_EQUAL( 1, row0.m() );
  BOOST_CHECK_EQUAL( 2, row0.n() );
  BOOST_CHECK_EQUAL( 2, row0.stride() );

  BOOST_CHECK_EQUAL( 2, row1.size() );
  BOOST_CHECK_EQUAL( 1, row1.m() );
  BOOST_CHECK_EQUAL( 2, row1.n() );
  BOOST_CHECK_EQUAL( 2, row1.stride() );

  BOOST_CHECK_EQUAL( 1, row0(0,0) );
  BOOST_CHECK_EQUAL( 0, row0(0,1) );
  BOOST_CHECK_EQUAL( 3, row1(0,0) );
  BOOST_CHECK_EQUAL( 2, row1(0,1) );

  MatrixDView<Real> col0 = M1.col(0);
  MatrixDView<Real> col1 = M1.col(1);

  BOOST_CHECK_EQUAL( 2, col0.size() );
  BOOST_CHECK_EQUAL( 2, col0.m() );
  BOOST_CHECK_EQUAL( 1, col0.n() );
  BOOST_CHECK_EQUAL( 2, col0.stride() );

  BOOST_CHECK_EQUAL( 2, col1.size() );
  BOOST_CHECK_EQUAL( 2, col1.m() );
  BOOST_CHECK_EQUAL( 1, col1.n() );
  BOOST_CHECK_EQUAL( 2, col1.stride() );

  BOOST_CHECK_EQUAL( 1, col0(0,0) );
  BOOST_CHECK_EQUAL( 0, col1(0,0) );
  BOOST_CHECK_EQUAL( 3, col0(1,0) );
  BOOST_CHECK_EQUAL( 2, col1(1,0) );

  MatrixDView<Real> sub = M1.sub(1,1,1,1);

  BOOST_CHECK_EQUAL( 1, sub.size() );
  BOOST_CHECK_EQUAL( 1, sub.m() );
  BOOST_CHECK_EQUAL( 1, sub.n() );
  BOOST_CHECK_EQUAL( 2, col0.stride() );

  BOOST_CHECK_EQUAL( 2, sub(0,0) );


  VectorDView<Real> subcol0 = M1.subcol(1,0,1);
  VectorDView<Real> subcol1 = M1.subcol(1,1,1);

  BOOST_CHECK_EQUAL( 1, subcol0.size() );
  BOOST_CHECK_EQUAL( 1, subcol0.m() );
  BOOST_CHECK_EQUAL( 1, subcol0.n() );
  BOOST_CHECK_EQUAL( 2, subcol0.stride() );

  BOOST_CHECK_EQUAL( 1, subcol1.size() );
  BOOST_CHECK_EQUAL( 1, subcol1.m() );
  BOOST_CHECK_EQUAL( 1, subcol1.n() );
  BOOST_CHECK_EQUAL( 2, subcol1.stride() );

  BOOST_CHECK_EQUAL( 3, subcol0[0] );
  BOOST_CHECK_EQUAL( 2, subcol1[0] );

}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixDView_scatterAdd )
{
  Real v1[5*5], v2[5*5];
  MatrixDView<Real> A1(v1, 5, 5),  A2(v2, 5, 5);

  A1 = 0;
  A2 = 0;

  //Crete the reference matrix
  A1(0,0) =  2;
  A1(0,1) = -1;
  for ( int i = 1; i < A1.m()-1; i++ )
  {
    A1(i,i-1) = -1;
    A1(i,i  ) = 2;
    A1(i,i+1) = -1;
  }
  A1(A1.m()-1,A1.m()-2) = -1;
  A1(A1.m()-1,A1.m()-1) =  2;


  //Build the same tri-diagonal system using scatterAdd
  MatrixD< Real > mtxLocal( {{1,-1},{-1,1}} );

  for ( int i = 1; i < A2.m(); i++ )
  {
    int map[] = {i-1,i};
    A2.scatterAdd(mtxLocal,map,2);
  }

  MatrixD< Real > mtxBC( {{1}} );
  int mapBC[] = {0};
  A2.scatterAdd(mtxBC,mapBC,1);

  mapBC[0] = 4;
  A2.scatterAdd(mtxBC,mapBC,1);

  for ( int i = 0; i < A1.m(); i++ )
    for ( int j = 0; j < A1.n(); j++ )
      BOOST_CHECK_EQUAL(A1(i,j), A2(i,j));

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
