// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
namespace DLA
{
}
}

using namespace SANS;
using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( VectorS_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_dot_4Int )
{
  typedef VectorS<4,int> VectorS4;
  VectorS4 a = {1,2,3,4};
  VectorS4 b = {2,3,4,5};

  int d = dot(a,b);

  int exact = 0;
  for (int i = 0; i < VectorS4::M; i++)
    exact += a[i]*b[i];

  BOOST_CHECK_EQUAL(d, exact);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_dot_4Surreal )
{
  typedef VectorS< 4,SurrealS<4> > VectorS4;
  VectorS4 a = {1,2,3,4};
  VectorS4 b = {2,3,4,5};

  SurrealS<4> d = dot(a,b);

  SurrealS<4> exact = 0;
  for (int i = 0; i < VectorS4::M; i++)
    exact += a[i]*b[i];

  BOOST_CHECK_EQUAL(d.value(), exact.value());
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_dot_4SurrealReal )
{
  typedef VectorS< 4, SurrealS<4> > VectorS4;
  VectorS4 a = {1,2,3,4};
  VectorS< 4, Real > b = {2,3,4,5};

  SurrealS<4> d = dot(a,b);

  SurrealS<4> exact = 0;
  for (int i = 0; i < VectorS4::M; i++)
    exact += a[i]*b[i];

  BOOST_CHECK_EQUAL(d.value(), exact.value());
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_dot_4RealSurreal )
{
  typedef VectorS< 4, SurrealS<4> > VectorS4;
  VectorS< 4, Real > a = {1,2,3,4};
  VectorS4 b = {2,3,4,5};

  SurrealS<4> d = dot(a,b);

  SurrealS<4> exact = 0;
  for (int i = 0; i < VectorS4::M; i++)
    exact += a[i]*b[i];

  BOOST_CHECK_EQUAL(d.value(), exact.value());
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_dot_4RealSurrealVectorS )
{
  typedef VectorS< 4, SurrealS<4> > VectorS4;
  VectorS< 4, Real > a = {1,2,3,4};
  VectorS4 b = {2,3,4,5};
  VectorS< 4, Real > c = {3,2,5,4};

  VectorS4 d0 = dot(a,b)*c;
  VectorS4 d1 = c*dot(a,b);

  VectorS4 exact = 0;
  for (int j = 0; j < VectorS4::M; j++)
    for (int i = 0; i < VectorS4::M; i++)
      exact[j] += c[j]*a[i]*b[i];

  BOOST_CHECK_EQUAL(d0[0].value(), exact[0].value());
  BOOST_CHECK_EQUAL(d0[1].value(), exact[1].value());
  BOOST_CHECK_EQUAL(d0[2].value(), exact[2].value());
  BOOST_CHECK_EQUAL(d0[3].value(), exact[3].value());

  BOOST_CHECK_EQUAL(d1[0].value(), exact[0].value());
  BOOST_CHECK_EQUAL(d1[1].value(), exact[1].value());
  BOOST_CHECK_EQUAL(d1[2].value(), exact[2].value());
  BOOST_CHECK_EQUAL(d1[3].value(), exact[3].value());
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SurrealVectorS_dot_4RealSurrealVectorS )
{
  typedef VectorS< 4, SurrealS<4> > VectorS4;
  VectorS< 4, Real > a = {1,2,3,4};
  VectorS4 b = {2,3,4,5};
  VectorS< 4, Real > c = {3,2,5,4};

  VectorS4 d0 = dot(a,b)*c;
  VectorS4 d1 = c*dot(a,b);
  VectorS4 d2 = c*dot(b,a);
  VectorS4 d3 = dot(b,a)*c;

  VectorS4 exact = 0;
  for (int j = 0; j < VectorS4::M; j++)
    for (int i = 0; i < VectorS4::M; i++)
      exact[j] += c[j]*a[i]*b[i];

  BOOST_CHECK_EQUAL(d0[0].value(), exact[0].value());
  BOOST_CHECK_EQUAL(d0[1].value(), exact[1].value());
  BOOST_CHECK_EQUAL(d0[2].value(), exact[2].value());
  BOOST_CHECK_EQUAL(d0[3].value(), exact[3].value());

  BOOST_CHECK_EQUAL(d1[0].value(), exact[0].value());
  BOOST_CHECK_EQUAL(d1[1].value(), exact[1].value());
  BOOST_CHECK_EQUAL(d1[2].value(), exact[2].value());
  BOOST_CHECK_EQUAL(d1[3].value(), exact[3].value());

  BOOST_CHECK_EQUAL(d2[0].value(), exact[0].value());
  BOOST_CHECK_EQUAL(d2[1].value(), exact[1].value());
  BOOST_CHECK_EQUAL(d2[2].value(), exact[2].value());
  BOOST_CHECK_EQUAL(d2[3].value(), exact[3].value());

  BOOST_CHECK_EQUAL(d3[0].value(), exact[0].value());
  BOOST_CHECK_EQUAL(d3[1].value(), exact[1].value());
  BOOST_CHECK_EQUAL(d3[2].value(), exact[2].value());
  BOOST_CHECK_EQUAL(d3[3].value(), exact[3].value());
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_dot_Expr )
{
  typedef VectorS<4,Real> VectorS4;
  MatrixS<4,4,Real> I = Identity();
  VectorS4 a = {1,2,3,4};
  VectorS4 b = {2,3,4,5};
  VectorS4 c = {4,5,6,7};

  I *= 2;

  Real d = dot(a,I*b);

  Real exact = 0;
  for (int i = 0; i < VectorS4::M; i++)
    exact += 2*a[i]*b[i];

  BOOST_CHECK_EQUAL(d, exact);

  d = dot(I*a,b);

  BOOST_CHECK_EQUAL(d, exact);

  d = dot(I*a,I*b);

  exact = 0;
  for (int i = 0; i < VectorS4::M; i++)
    exact += 4*a[i]*b[i];

  BOOST_CHECK_EQUAL(d, exact);

  d = dot(I*a,b+c);

  exact = 0;
  for (int i = 0; i < VectorS4::M; i++)
    exact += 2*a[i]*(b[i]+c[i]);

  BOOST_CHECK_EQUAL(d, exact);

  d = dot(a+c,I*b);

  exact = 0;
  for (int i = 0; i < VectorS4::M; i++)
    exact += (a[i]+c[i])*2*b[i];

  BOOST_CHECK_EQUAL(d, exact);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_dot_Row_Col )
{
  typedef VectorS<4,Real> VectorS4;
  VectorS4 a = {1,2,3,4};
  MatrixS<1,4,Real> b = {{2,3,4,5}};
  MatrixS<1,4,Real> c = {{4,5,6,7}};

  Real exact = 0;
  for (int i = 0; i < VectorS4::M; i++)
    exact += a[i]*b(0,i);

  // column dot row
  Real d = dot(a, b);
  BOOST_CHECK_EQUAL(d, exact);

  // row dot column
  d = dot(b, a);
  BOOST_CHECK_EQUAL(d, exact);

  exact = 0;
  for (int i = 0; i < VectorS4::M; i++)
    exact += b(0,i)*c(0,i);

  // row dot row
  d = dot(b, c);
  BOOST_CHECK_EQUAL(d, exact);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_Real_dot_VectorS_VectorS )
{
  // This occurs when gradient of test function is dotted with fluxes, i.e.
  // PDE += dot(gradphi, F)

  typedef VectorS<2,Real> VectorS2;
  typedef VectorS<3,Real> VectorS3;
  typedef VectorS<3,VectorS2> VectorS3VectorS2;
  VectorS3         a = {1,2,3};
  VectorS3VectorS2 b = {{10,20},{30,40},{50,60}};

  VectorS2 exact = 0;
  for (int i = 0; i < 3; i++)
    exact += a[i]*b[i];

  // check the dot product
  VectorS2 d = dot(a, b);
  BOOST_CHECK_EQUAL(d[0], exact[0]);
  BOOST_CHECK_EQUAL(d[1], exact[1]);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorVectorS_Real_dot_VectorVectorS_Surreal )
{
  // This occurs when slift and dqn are dotted together in DG Field weighted Boundary Functional

  typedef VectorS<2, VectorS< 4, SurrealS<4> > >  SurrealVectorS4;
  typedef VectorS<2, VectorS< 4, Real > > VectorS4;
  typedef SurrealS<4> SurrealS4;

  SurrealVectorS4 a =  { {1,2,3,4}, {5,6,7,8} } ;
  VectorS4 b = { {10,20,30,40} , {50,60,70,80} };


  SurrealS4 exact = 0;
  for (int i = 0; i < 2; i++)
    for ( int j = 0; j < 4; j++)
    exact += a[i][j]*b[i][j];

  // check the dot product
  SurrealS4 c = dot(a, b);
  BOOST_CHECK_EQUAL(c.value(), exact.value());
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_VectorS_dot )
{
  typedef VectorS<4,VectorS<2,Real>> VectorS4VectorS2;
  VectorS4VectorS2 a = {{1,2},{3,4},{5,6},{7,8}};
  VectorS4VectorS2 b = {{10,20},{30,40},{50,60},{70,80}};

  Real exact = 0;
  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 2; j++)
    exact += a[i][j]*b[i][j];

  // check the dot product
  Real d = dot(a, b);
  BOOST_CHECK_EQUAL(d, exact);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_dot_MatrixSymS_Real )
{
  typedef VectorS<3,Real> VectorS4Real;
  typedef VectorS<3,MatrixSymS<2,Real>> VectorS4MatrixSymS22;
  VectorS4Real a = {1,2,3};

  MatrixSymS<2,Real> m1 = {{1.2},
                           {-0.3, 1.5}};

  MatrixSymS<2,Real> m2 = {{4.8},
                           {2.7, 0.8}};

  MatrixSymS<2,Real> m3 = {{6.2},
                           {1.9, -3.6}};

  VectorS4MatrixSymS22 b = {m1, m2, m3};

  MatrixSymS<2,Real> exact = 0;
  for (int i = 0; i < 3; i++)
      exact += a[i]*b[i];

  // check the dot product
  MatrixSymS<2,Real> d1 = dot(a, b);
  BOOST_CHECK_EQUAL(d1(0,0), exact(0,0));
  BOOST_CHECK_EQUAL(d1(1,0), exact(1,0));
  BOOST_CHECK_EQUAL(d1(1,1), exact(1,1));

  MatrixSymS<2,Real> d2 = dot(b, a);
  BOOST_CHECK_EQUAL(d2(0,0), exact(0,0));
  BOOST_CHECK_EQUAL(d2(1,0), exact(1,0));
  BOOST_CHECK_EQUAL(d2(1,1), exact(1,1));
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
