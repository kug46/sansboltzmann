// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// MatrixS_btest
// testing of MatrixS<M,N,T> class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_LpNorm.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"

#include "Surreal/SurrealS.h"

#include "chkMatrixS_btest.h"

#include <iostream>
using namespace std;


//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
namespace DLA
{

}
}

using namespace SANS::DLA;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( MatrixSymS_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LpNorm_1x1 )
{

  MatrixSymS<1,Real> A = {1.2};

  VectorS<1,Real> dX = {0.2};

  LpNorm<1,Real> norm(A);

  Real Lpnorm = norm.Lp(dX,2);

  BOOST_CHECK_CLOSE( pow( pow(sqrt(A(0,0))*dX[0],2), 1./2.), Lpnorm, 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LpNorm_2x2 )
{

  MatrixSymS<2,Real> A = {{1.2},
                          {-0.3, 1.5}};

  VectorS<2,Real> dX = {0.2, 1.8};
  Real Lpnorm, L2norm;

  LpNorm<2,Real> norm(A);

  Lpnorm = norm.Lp(dX,2);

  L2norm = sqrt(Transpose(dX)*A*dX);

  BOOST_CHECK_CLOSE( Lpnorm, L2norm, 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LpNorm_2x2_LpDeriv )
{
  typedef SurrealS<2> T;

  MatrixSymS<2,T> A = {{1.2},
                       {-0.3, 1.5}};

  VectorS<2,T> dX = {0.2, 1.8};
  T Lpnorm;

  dX[0].deriv(0) = 1;
  dX[1].deriv(1) = 1;

  LpNorm<2,T> norm(A);

  Lpnorm = norm.Lp(dX,2);


  MatrixSymS<2,Real> Ar = { {1.2},
                            {-0.3, 1.5} };

  VectorS<2,Real> dXr = {0.2, 1.8};

  LpNorm<2,Real> normr(Ar);

  VectorS<2,Real> dLpdXr = normr.dLpdX(dXr,2);

  BOOST_CHECK_CLOSE( Lpnorm.deriv(0), dLpdXr[0], 1e-11 );
  BOOST_CHECK_CLOSE( Lpnorm.deriv(1), dLpdXr[1], 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LpNorm_2x2_Lp2Deriv )
{
  typedef SurrealS<2> T;

  MatrixSymS<2,T> A = {{1.2},
                       {-0.3, 1.5}};

  VectorS<2,T> dX = {0.2, 1.8};
  T Lpnorm;

  dX[0].deriv(0) = 1;
  dX[1].deriv(1) = 1;

  LpNorm<2,T> norm(A);

  Lpnorm = norm.Lp2(dX,2);


  MatrixSymS<2,Real> Ar = { {1.2},
                            {-0.3, 1.5} };

  VectorS<2,Real> dXr = {0.2, 1.8};

  LpNorm<2,Real> normr(Ar);

  VectorS<2,Real> dLpdXr = normr.dLp2dX(dXr,2);

  BOOST_CHECK_CLOSE( Lpnorm.deriv(0), dLpdXr[0], 1e-11 );
  BOOST_CHECK_CLOSE( Lpnorm.deriv(1), dLpdXr[1], 1e-11 );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
