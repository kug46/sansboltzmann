// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// MatrixS_btest
// testing of MatrixS<M,N,T> class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "chkMatrixS_btest.h"

#include <iostream>
using namespace std;


//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
namespace DLA
{

}
}

using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( MatrixSymS_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Log_MatrixSymS_2x2 )
{

  MatrixSymS<2,Real> A1 = {{1.2},
                          {-0.3, 1.5}};
  MatrixSymS<2,Real> P;

  P = log(A1);

  BOOST_CHECK_CLOSE( 1.547607395672293e-01, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE(-2.269718913801098e-01, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 3.817326309473392e-01, P(1,1), 1e-11 );

  MatrixSymS<2,Real> A2 = { {4.9},
                            {0.7, 2.3} };

  P = log(A2);

  BOOST_CHECK_CLOSE( 1.572531181026974e+00, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( 2.065995606158336e-01, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 8.051613844538776e-01, P(1,1), 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Log_MatrixSymS_LE_2x2 )
{

  MatrixSymS<2,Real> A1 = {{1.2},
                          {-0.3, 1.5}};
  MatrixSymS<2,Real> P;

  P = log(EigenSystemPair<2,Real>(A1));

  BOOST_CHECK_CLOSE( 1.547607395672293e-01, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE(-2.269718913801098e-01, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 3.817326309473392e-01, P(1,1), 1e-11 );

  MatrixSymS<2,Real> A2 = { {4.9},
                            {0.7, 2.3} };

  P = log(EigenSystemPair<2,Real>(A2));

  BOOST_CHECK_CLOSE( 1.572531181026974e+00, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( 2.065995606158336e-01, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 8.051613844538776e-01, P(1,1), 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Log_MatrixSymS_3x3 )
{

  MatrixSymS<3,Real> A1 = { { 1.2},
                            {-0.3, 1.5},
                            { 0.2, 0.4, 4.6} };
  MatrixSymS<3,Real> P;

  P = log(A1);

  BOOST_CHECK_CLOSE( 1.459863523559461e-01, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE(-2.399071358855351e-01, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 3.624542414685893e-01, P(1,1), 1e-11 );
  BOOST_CHECK_CLOSE( 9.508950785202167e-02, P(2,0), 1e-11 );
  BOOST_CHECK_CLOSE( 1.550201455447050e-01, P(2,1), 1e-11 );
  BOOST_CHECK_CLOSE( 1.515223932386301e+00, P(2,2), 1e-11 );

  MatrixSymS<3,Real> A2 = { { 4.9},
                            { 0.7, 2.3},
                            {-0.1, 1.4, 6.9} };

  P = log(A2);

  BOOST_CHECK_CLOSE( 1.570378469084072e+00, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( 2.207743334143517e-01, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 7.098252248957918e-01, P(1,1), 1e-11 );
  BOOST_CHECK_CLOSE(-4.571201968580689e-02, P(2,0), 1e-11 );
  BOOST_CHECK_CLOSE( 3.562282350564878e-01, P(2,1), 1e-11 );
  BOOST_CHECK_CLOSE( 1.887375840394703e+00, P(2,2), 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Log_MatrixSymS_LE_3x3 )
{

  MatrixSymS<3,Real> A1 = { { 1.2},
                            {-0.3, 1.5},
                            { 0.2, 0.4, 4.6} };
  MatrixSymS<3,Real> P;

  P = log(EigenSystemPair<3,Real>(A1));

  BOOST_CHECK_CLOSE( 1.459863523559461e-01, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE(-2.399071358855351e-01, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 3.624542414685893e-01, P(1,1), 1e-11 );
  BOOST_CHECK_CLOSE( 9.508950785202167e-02, P(2,0), 1e-11 );
  BOOST_CHECK_CLOSE( 1.550201455447050e-01, P(2,1), 1e-11 );
  BOOST_CHECK_CLOSE( 1.515223932386301e+00, P(2,2), 1e-11 );

  MatrixSymS<3,Real> A2 = { { 4.9},
                            { 0.7, 2.3},
                            {-0.1, 1.4, 6.9} };

  P = log(EigenSystemPair<3,Real>(A2));

  BOOST_CHECK_CLOSE( 1.570378469084072e+00, P(0,0), 1e-11 );
  BOOST_CHECK_CLOSE( 2.207743334143517e-01, P(1,0), 1e-11 );
  BOOST_CHECK_CLOSE( 7.098252248957918e-01, P(1,1), 1e-11 );
  BOOST_CHECK_CLOSE(-4.571201968580689e-02, P(2,0), 1e-11 );
  BOOST_CHECK_CLOSE( 3.562282350564878e-01, P(2,1), 1e-11 );
  BOOST_CHECK_CLOSE( 1.887375840394703e+00, P(2,2), 1e-11 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Log_VectorS_3x1 )
{

  VectorS<3,Real> V = {1.2,20.3,173.5};
  VectorS<3,Real> P;

  P = log(V);

  BOOST_CHECK_CLOSE( 1.823215567939546e-01, P[0], 1e-11 );
  BOOST_CHECK_CLOSE( 3.010620886047742e+00, P[1], 1e-11 );
  BOOST_CHECK_CLOSE( 5.156177599386914e+00, P[2], 1e-11 );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
