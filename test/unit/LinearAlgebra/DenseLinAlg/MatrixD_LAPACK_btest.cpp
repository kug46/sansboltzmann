// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/lapack/Eigen.h"
#include "LinearAlgebra/DenseLinAlg/tools/DenseLinAlg_LAPACK.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/ElementaryReflector.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Decompose_QR.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Diag.h"
#include "LinearAlgebra/DenseLinAlg/InverseQR.h"

#include <iostream>
#include <limits>
using namespace SANS::DLA;

namespace SANS
{
namespace DLA
{
//Declare for tesing purposes. No need to use this anywhere else. Defined in MatrixD_InverseQR.cpp
template< class T, class MatrixType >
void ApplyQTrans( MatrixDView< T >& A, const VectorD<T>& tau, MatrixType& B );
}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

#ifdef DLA_LAPACK

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EigenValues_test )
{
  typedef Real T;
  Real Adata[] = {2, 1 ,3,
                  0, 1, 3,
                  0, 2, 1};

  MatrixD<T> A(3,3, Adata);
  VectorD<T> wr(3), wi(3);

  EigenValues(A, wr, wi);

  BOOST_CHECK_CLOSE( wr[0], T(1.+sqrt(6.)), T(0.0001) );
  BOOST_CHECK_CLOSE( wr[1], T(1.-sqrt(6.)), T(0.0001) );
  BOOST_CHECK_CLOSE( wr[2], T(2          ), T(0.0001) );

  BOOST_CHECK_SMALL( wi[0], T(1e-12) );
  BOOST_CHECK_SMALL( wi[1], T(1e-12) );
  BOOST_CHECK_SMALL( wi[2], T(1e-12) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EigenVectors_test )
{
  typedef Real T;
  Real Adata[] = {2, 1 ,3,
                  0, 1, 3,
                  0, 2, 1};

  MatrixD<T> A(3,3, Adata);
  MatrixD<T> vl(3,3), vr(3,3);

  EigenVectors(A, vl, vr);

  BOOST_CHECK_SMALL( vl(0,0) , T(1e-12) );
  BOOST_CHECK_CLOSE( vl(0,1) , T( (2*(1 + sqrt(6)))/sqrt(70 + 20*sqrt(6)) ), T(0.0001) );
  BOOST_CHECK_CLOSE( vl(0,2) , T( (6 + sqrt(6))/sqrt(70 + 20*sqrt(6))  ), T(0.0001) );

  BOOST_CHECK_SMALL( vl(1,0) , T(1e-12) );
  BOOST_CHECK_CLOSE( vl(1,1) , T( (2*(1 + sqrt(6)))/sqrt(70 + 20*sqrt(6)) ), T(0.0001) );
  BOOST_CHECK_CLOSE( vl(1,2) , T(-(6 + sqrt(6))/sqrt(70 + 20*sqrt(6)) ), T(0.0001) );

  BOOST_CHECK_CLOSE( vl(2,0) , T( sqrt(5/22.)), T(0.0001) );
  BOOST_CHECK_CLOSE( vl(2,1) , T(-7/sqrt(110)), T(0.0001) );
  BOOST_CHECK_CLOSE( vl(2,2) , T(-3*sqrt(2/55.)), T(0.0001) );


  BOOST_CHECK_CLOSE( vr(0,0) , T((6 + sqrt(6))/sqrt(2*(56 - 4*sqrt(6)))) , T(0.0001) );
  BOOST_CHECK_CLOSE( vr(1,0) , T((-1 + sqrt(6))*sqrt(3/(56 - 4*sqrt(6)))), T(0.0001) );
  BOOST_CHECK_CLOSE( vr(2,0) , T((-1 + sqrt(6))/sqrt(28 - 2*sqrt(6)))    , T(0.0001) );

  BOOST_CHECK_CLOSE( vr(0,1) , T((6 - sqrt(6))/sqrt(2*(56 + 4*sqrt(6))))    , T(0.0001) );
  BOOST_CHECK_CLOSE( vr(1,1) , T( 1/2.*(1 + sqrt(6))*sqrt(3/(14 + sqrt(6)))), T(0.0001) );
  BOOST_CHECK_CLOSE( vr(2,1) , T(-(1 + sqrt(6))/sqrt(2*(14 + sqrt(6))))     , T(0.0001) );

  BOOST_CHECK_CLOSE( vr(0,2) , T( 1.                 ), T(0.0001) );
  BOOST_CHECK_SMALL( vr(1,2) , T(1e-12) );
  BOOST_CHECK_SMALL( vr(2,2) , T(1e-12) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EigenSystem_test )
{
  typedef Real T;
  Real Adata[] = {2, 1 ,3,
                  0, 1, 3,
                  0, 2, 1};

  MatrixD<T> A(3,3, Adata);
  VectorD<T> wr(3), wi(3);
  MatrixD<T> vl(3,3), vr(3,3);

  EigenSystem(A, wr, wi, vl, vr);

  BOOST_CHECK_CLOSE( wr[0], T(1.+sqrt(6.)), T(0.0001) );
  BOOST_CHECK_CLOSE( wr[1], T(1.-sqrt(6.)), T(0.0001) );
  BOOST_CHECK_CLOSE( wr[2], T(2          ), T(0.0001) );

  BOOST_CHECK_SMALL( wi[0], T(1e-12) );
  BOOST_CHECK_SMALL( wi[1], T(1e-12) );
  BOOST_CHECK_SMALL( wi[2], T(1e-12) );



  BOOST_CHECK_SMALL( vl(0,0) , T(1e-12) );
  BOOST_CHECK_CLOSE( vl(0,1) , T( (2*(1 + sqrt(6)))/sqrt(70 + 20*sqrt(6)) ), T(0.0001) );
  BOOST_CHECK_CLOSE( vl(0,2) , T( (6 + sqrt(6))/sqrt(70 + 20*sqrt(6))  ), T(0.0001) );

  BOOST_CHECK_SMALL( vl(1,0) , T(1e-12) );
  BOOST_CHECK_CLOSE( vl(1,1) , T( (2*(1 + sqrt(6)))/sqrt(70 + 20*sqrt(6)) ), T(0.0001) );
  BOOST_CHECK_CLOSE( vl(1,2) , T(-(6 + sqrt(6))/sqrt(70 + 20*sqrt(6)) ), T(0.0001) );

  BOOST_CHECK_CLOSE( vl(2,0) , T( sqrt(5/22.)), T(0.0001) );
  BOOST_CHECK_CLOSE( vl(2,1) , T(-7/sqrt(110)), T(0.0001) );
  BOOST_CHECK_CLOSE( vl(2,2) , T(-3*sqrt(2/55.)), T(0.0001) );


  BOOST_CHECK_CLOSE( vr(0,0) , T((6 + sqrt(6))/sqrt(2*(56 - 4*sqrt(6)))) , T(0.0001) );
  BOOST_CHECK_CLOSE( vr(1,0) , T((-1 + sqrt(6))*sqrt(3/(56 - 4*sqrt(6)))), T(0.0001) );
  BOOST_CHECK_CLOSE( vr(2,0) , T((-1 + sqrt(6))/sqrt(28 - 2*sqrt(6)))    , T(0.0001) );

  BOOST_CHECK_CLOSE( vr(0,1) , T((6 - sqrt(6))/sqrt(2*(56 + 4*sqrt(6))))    , T(0.0001) );
  BOOST_CHECK_CLOSE( vr(1,1) , T( 1/2.*(1 + sqrt(6))*sqrt(3/(14 + sqrt(6)))), T(0.0001) );
  BOOST_CHECK_CLOSE( vr(2,1) , T(-(1 + sqrt(6))/sqrt(2*(14 + sqrt(6))))     , T(0.0001) );

  BOOST_CHECK_CLOSE( vr(0,2) , T( 1.                 ), T(0.0001) );
  BOOST_CHECK_SMALL( vr(1,2) , T(1e-12) );
  BOOST_CHECK_SMALL( vr(2,2) , T(1e-12) );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  MatrixD<T> tmp1 = vr*diag(wr);
  MatrixD<T> tmp2 = A*vr;

  for (int i = 0; i < 3; i++ )
    for (int j = 0; j < 3; j++ )
      SANS_CHECK_CLOSE( tmp2(i,j), tmp1(i,j), small_tol, close_tol );

  MatrixD<T> tmp3 = diag(wr)*vl;
  MatrixD<T> tmp4 = vl*A;

  for (int i = 0; i < 3; i++ )
    for (int j = 0; j < 3; j++ )
      SANS_CHECK_CLOSE( tmp4(i,j), tmp3(i,j), small_tol, close_tol );

}

#ifdef DLA_LAPACKE

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementaryReflector_test_1 )
{

  double la_x[] = {3, 5, 6};
  double la_tau = 0;
  double la_alpha = 1;
  lapack_int n = sizeof(la_x)/sizeof(double)+1;
  lapack_int incx = 1;

  VectorD<double> x(n-1, la_x);
  double tau = 0;
  double alpha = la_alpha;

  lapack_int INFO = LAPACKE_dlarfg( n, &la_alpha, la_x, incx, &la_tau );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  ElementaryReflector(alpha, x, tau);

  BOOST_CHECK_CLOSE( la_alpha, alpha, 1e-12 );
  BOOST_CHECK_CLOSE( la_tau, tau, 1e-12 );
  for (int i = 0; i < x.size(); i++)
    BOOST_CHECK_CLOSE( la_x[i], x[i], 1e-12 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementaryReflector_test_2 )
{

  double la_x[] = {std::numeric_limits<double>::min()/2, 0, 0};
  double la_tau = 0;
  double la_alpha = std::numeric_limits<double>::min()/2;
  lapack_int n = sizeof(la_x)/sizeof(double)+1;
  lapack_int incx = 1;

  VectorD<double> x(n-1, la_x);
  double tau = 0;
  double alpha = la_alpha;

  lapack_int INFO = LAPACKE_dlarfg( n, &la_alpha, la_x, incx, &la_tau );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  ElementaryReflector(alpha, x, tau);

  BOOST_CHECK_CLOSE( la_alpha, alpha, 1e-12 );
  BOOST_CHECK_CLOSE( la_tau, tau, 1e-12 );
  for (int i = 0; i < x.size(); i++)
    BOOST_CHECK_CLOSE( la_x[i], x[i], 1e-12 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementaryReflector_test_3 )
{

  double la_x[] = {0, 0, 0};
  double la_tau = 0;
  double la_alpha = 1;
  lapack_int n = sizeof(la_x)/sizeof(double)+1;
  lapack_int incx = 1;

  VectorD<double> x(n-1, la_x);
  double tau = 0;
  double alpha = la_alpha;

  lapack_int INFO = LAPACKE_dlarfg( n, &la_alpha, la_x, incx, &la_tau );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  ElementaryReflector(alpha, x, tau);

  BOOST_CHECK_EQUAL( la_alpha, alpha );
  BOOST_CHECK_EQUAL( la_tau, tau );
  for (int i = 0; i < x.size(); i++)
    BOOST_CHECK_EQUAL( la_x[i], x[i] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementaryReflector_test_4 )
{

  double la_x[] = {3, 5, 6};
  double la_tau = 0;
  double la_alpha = 0;
  lapack_int n = sizeof(la_x)/sizeof(double)+1;
  lapack_int incx = 1;

  VectorD<double> x(n-1, la_x);
  double tau = 0;
  double alpha = la_alpha;

  lapack_int INFO = LAPACKE_dlarfg( n, &la_alpha, la_x, incx, &la_tau );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  ElementaryReflector(alpha, x, tau);

  BOOST_CHECK_CLOSE( la_alpha, alpha, 1e-12 );
  BOOST_CHECK_CLOSE( la_tau, tau, 1e-12 );
  for (int i = 0; i < x.size(); i++)
    BOOST_CHECK_CLOSE( la_x[i], x[i], 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementaryReflector_test_5 )
{
  double la_x[] = {1.32, 0.12, 0.08};
  double la_tau = 0;
  double la_alpha = 0;
  lapack_int n = sizeof(la_x)/sizeof(double)+1;
  lapack_int incx = 1;

  VectorD<double> x(n-1, la_x);
  double tau = 0;
  double alpha = la_alpha;

  lapack_int INFO = LAPACKE_dlarfg( n, &la_alpha, la_x, incx, &la_tau );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  ElementaryReflector(alpha, x, tau);

  BOOST_CHECK_CLOSE( la_alpha, alpha, 1e-12 );
  BOOST_CHECK_CLOSE( la_tau, tau, 1e-12 );
  for (int i = 0; i < x.size(); i++)
    BOOST_CHECK_CLOSE( la_x[i], x[i], 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementaryReflector_test_6 )
{
  double la_x[] = {0.08, 0.65, 0.89, 0.38, 0.51};
  double la_tau = 0;
  double la_alpha = 1.32;
  lapack_int n = sizeof(la_x)/sizeof(double)+1;
  lapack_int incx = 1;

  VectorD<double> x(n-1, la_x);
  double tau = 0;
  double alpha = la_alpha;

  lapack_int INFO = LAPACKE_dlarfg( n, &la_alpha, la_x, incx, &la_tau );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  ElementaryReflector(alpha, x, tau);

  BOOST_CHECK_CLOSE( la_alpha, alpha, 1e-12 );
  BOOST_CHECK_CLOSE( la_tau, tau, 1e-12 );
  for (int i = 0; i < x.size(); i++)
    BOOST_CHECK_CLOSE( la_x[i], x[i], 1e-12 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ApplyElementaryReflector_test )
{

  double la_A[] = {2, 1 ,3,
                   1, 1, 3,
                   0, 2, 1,
                   0, 0, 0,  //Memory problem with lapacke
                   0, 0, 0}; //Memory problem with lapacke
  double la_tau = 0;
  double la_work[3] = {0};
  lapack_int incA = 3;

  MatrixD<double> A(3,3,la_A);
  double tau = 0;

  lapack_int INFO = LAPACKE_dlarfg( 3, &la_A[0], &la_A[3], incA, &la_tau );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  VectorDView<double> x = A.subcol(1,0,2);
  ElementaryReflector(A(0,0), x, tau);

  BOOST_CHECK_CLOSE( la_tau, tau, 1e-12 );
  for (int i = 0; i < A.m(); i++)
    BOOST_CHECK_CLOSE( la_A[i*incA + 0], A(i,0), 1e-12 );

  double la_A00 = la_A[0];
  la_A[0] = 1;
  double la_v[3] = {la_A[0], la_A[3], la_A[6]};
  INFO = LAPACKE_dlarfx( LAPACK_ROW_MAJOR, 'L', 3, 2, la_v, la_tau, &la_A[1], incA, la_work );
  la_A[0] = la_A00;

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  double A00 = A(0,0);
  A(0,0) = 1;
  VectorDView<double> v = A.subcol(0, 0, 3);
  MatrixDView<double> C = A.sub(0,1,3,2);
  ApplyElementaryReflector( v, tau, C );
  A(0,0) = A00;

  for (int i = 0; i < A.m(); i++)
    for (int j = 0; j < A.n(); j++)
      BOOST_CHECK_CLOSE( la_A[i*incA+j], A(i,j), 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_Decompose_QR_test )
{

  double la_A[] = {2, 1 ,3,
                   1, 1, 3,
                   0, 2, 1};
  double la_tau[3] = {0};
  lapack_int incA = 3;

  MatrixD<double> A(3,3,la_A);

  lapack_int INFO = LAPACKE_dgeqr2( LAPACK_ROW_MAJOR, 3, 3, la_A, incA, la_tau );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  VectorD<double> tau(3);
  MatrixD<double> QR(3,3);
  Decompose_QR(A, QR, tau);

  for (int i = 0; i < A.m(); i++)
  {
    BOOST_CHECK_CLOSE( la_tau[i], tau[i], 1e-12 );
    for (int j = 0; j < A.n(); j++)
      BOOST_CHECK_CLOSE( la_A[i*incA+j], QR(i,j), 1e-12 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_Decompose_QR_LeastSquare_test )
{

  double la_A[] = {2, 1,
                   1, 1,
                   3, 2};
  double la_tau[2] = {0};
  lapack_int incA = 2;

  MatrixD<double> A(3,2,la_A);

  lapack_int INFO = LAPACKE_dgeqr2( LAPACK_ROW_MAJOR, 3, 2, la_A, incA, la_tau );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  VectorD<double> tau(2);
  MatrixD<double> QR(3,2);

  Decompose_QR(A, QR, tau);

  for (int i = 0; i < tau.m(); i++)
    BOOST_CHECK_CLOSE( la_tau[i], tau[i], 1e-12 );

  for (int i = 0; i < QR.m(); i++)
    for (int j = 0; j < QR.n(); j++)
      BOOST_CHECK_CLOSE( la_A[i*incA+j], QR(i,j), 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ApplyQTrans_test_1x1 )
{

  double la_A[] = {2};
  double la_tau[2] = {0};
  double la_b[2] = {4};
  lapack_int incA = 1;

  MatrixD<double> A(1,1,la_A);
  VectorD<double> B(1,la_b);

  lapack_int INFO = LAPACKE_dgeqr2( LAPACK_ROW_MAJOR, 1, 1, la_A, incA, la_tau );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  INFO = LAPACKE_dormqr( LAPACK_ROW_MAJOR, 'L', 'T',
                         1, 1, 1,
                         la_A, incA, la_tau,
                         la_b, 1 );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  VectorD<double> tau(1);
  MatrixD<double> QR(1,1);
  Decompose_QR(A, QR, tau);

  for (int i = 0; i < A.m(); i++)
  {
    BOOST_CHECK_CLOSE( la_tau[i], tau[i], 1e-12 );
    for (int j = 0; j < A.n(); j++)
      BOOST_CHECK_CLOSE( la_A[i*incA+j], QR(i,j), 1e-12 );
  }

  ApplyQTrans( A, tau, static_cast< MatrixDView< double >& >(B) );

  for (int i = 0; i < B.m(); i++)
    BOOST_CHECK_CLOSE( la_b[i], B[i], 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ApplyQTrans_test_2x2 )
{

  double la_A[] = {2, 1,
                   1, 3};
  double la_tau[2] = {0};
  double la_b[2] = {2,4};
  lapack_int incA = 2;

  MatrixD<double> A(2,2,la_A);
  VectorD<double> B(2,la_b);

  lapack_int INFO = LAPACKE_dgeqrf( LAPACK_ROW_MAJOR, 2, 2, la_A, incA, la_tau );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  INFO = LAPACKE_dormqr( LAPACK_ROW_MAJOR, 'L', 'T',
                         2, 1, 2,
                         la_A, incA, la_tau,
                         la_b, 1 );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  VectorD<double> tau(2);
  MatrixD<double> QR(2,2);
  Decompose_QR(A, QR, tau);

  for (int i = 0; i < A.m(); i++)
  {
    BOOST_CHECK_CLOSE( la_tau[i], tau[i], 1e-12 );
    for (int j = 0; j < A.n(); j++)
      BOOST_CHECK_CLOSE( la_A[i*incA+j], QR(i,j), 1e-12 );
  }

  ApplyQTrans( QR, tau, static_cast< MatrixDView< double >& >(B) );

  for (int i = 0; i < B.m(); i++)
    BOOST_CHECK_CLOSE( la_b[i], B[i], 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ApplyQTrans_test_3x3 )
{

  double la_A[] = {1.32, 0.32 ,0.54,
                   0.12, 1.52, 0.56,
                   0.08, 0.30, 1.44};
  double la_tau[3] = {0};
  double la_b[3] = {2,4,9};
  lapack_int incA = 3;

  MatrixD<double> A(3,3,la_A);
  VectorD<double> B(3,la_b);
  VectorD<double> b(3,la_b);
  VectorD<double> la_x(3,la_b);

  lapack_int INFO = LAPACKE_dgeqr2( LAPACK_ROW_MAJOR, 3, 3, la_A, incA, la_tau );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  VectorD<double> tau(3);
  MatrixD<double> QR(3,3);
  Decompose_QR(A, QR, tau);

  for (int i = 0; i < A.m(); i++)
  {
    BOOST_CHECK_CLOSE( la_tau[i], tau[i], 1e-12 );
    for (int j = 0; j < A.n(); j++)
      BOOST_CHECK_CLOSE( la_A[i*incA+j], QR(i,j), 1e-12 );
  }


  INFO = LAPACKE_dormqr( LAPACK_ROW_MAJOR, 'L', 'T',
                         3, 1, 3,
                         la_A, incA, la_tau,
                         la_b, 1 );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  ApplyQTrans( QR, tau, static_cast< MatrixDView< double >& >(B) );

  for (int i = 0; i < B.m(); i++)
    BOOST_CHECK_CLOSE( la_b[i], B[i], 1e-12 );


  INFO = LAPACKE_dtrtrs( LAPACK_ROW_MAJOR, 'U', 'N', 'N',
                         3, 1, la_A,
                         3, la_b, 1 );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  VectorD<double> x(3);
  x = InverseQR::Solve(A, b);

  for (int i = 0; i < B.m(); i++)
    BOOST_CHECK_CLOSE( la_b[i], x[i], 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ApplyQTrans_test_3x2 )
{

  double la_A[] = {2, 1,
                   3, 2,
                   6, 9};
  double la_tau[2] = {0};
  double la_b[3] = {2,4,3};
  lapack_int incA = 2;

  MatrixD<double> A(3,2,la_A);
  VectorD<double> B(3,la_b);
  VectorD<double> b(3,la_b);
  VectorD<double> la_x(3,la_b);

  lapack_int INFO = LAPACKE_dgeqr2( LAPACK_ROW_MAJOR, 3, 2, la_A, incA, la_tau );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  VectorD<double> tau(2);
  MatrixD<double> QR(3,2);
  Decompose_QR(A, QR, tau);

  for (int i = 0; i < tau.m(); i++)
    BOOST_CHECK_CLOSE( la_tau[i], tau[i], 1e-12 );

  for (int i = 0; i < A.m(); i++)
     for (int j = 0; j < A.n(); j++)
      BOOST_CHECK_CLOSE( la_A[i*incA+j], QR(i,j), 1e-12 );


  INFO = LAPACKE_dormqr( LAPACK_ROW_MAJOR, 'L', 'T',
                         3, 1, 2,
                         la_A, incA, la_tau,
                         la_b, 1 );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  ApplyQTrans( QR, tau, static_cast< MatrixDView< double >& >(B) );

  for (int i = 0; i < B.m(); i++)
    BOOST_CHECK_CLOSE( la_b[i], B[i], 1e-12 );


  INFO = LAPACKE_dtrtrs( LAPACK_ROW_MAJOR, 'U', 'N', 'N',
                         2, 1, la_A, incA, la_b, 1 );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  VectorD<double> x(2);
  x = InverseQR::Solve(A, b);

  for (int i = 0; i < x.m(); i++)
    BOOST_CHECK_CLOSE( la_b[i], x[i], 1e-12 );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( InverseQR_test_3x3 )
{

  const int n = 3;
  double la_A[] = {1.32, 0.32 ,0.54,
                   0.12, 1.52, 0.56,
                   0.08, 0.30, 1.44};
  double la_b[3] = {2,4,9};

  MatrixD<double> A(3,3,la_A);
  VectorD<double> b(3,la_b);
  VectorD<double> x(3);

  lapack_int INFO = LAPACKE_dgels( LAPACK_ROW_MAJOR, 'N', n, n, 1, la_A, n, la_b, 1 );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  x = InverseQR::Solve(A, b);

  for (int i = 0; i < x.m(); i++)
    BOOST_CHECK_CLOSE( la_b[i], x[i], 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ApplyQTrans_test_6x6 )
{

  const int m = 6;
  const int n = 6;
  double la_A[] = {1.32, 0.32, 0.54, 0.12, 0.52, 0.56,
                   0.08, 1.30, 0.44, 0.94, 0.44, 0.39,
                   0.65, 0.19, 1.51, 0.91, 0.01, 0.05,
                   0.89, 0.34, 0.25, 1.58, 0.20, 0.51,
                   0.38, 0.65, 0.30, 0.07, 1.20, 0.10,
                   0.51, 0.18, 0.43, 0.71, 0.97, 1.61};
  double la_tau[m] = {0};
  double la_b[m*n] = {1, 0, 0, 0, 0, 0,
                      0, 1, 0, 0, 0, 0,
                      0, 0, 1, 0, 0, 0,
                      0, 0, 0, 1, 0, 0,
                      0, 0, 0, 0, 1, 0,
                      0, 0, 0, 0, 0, 1};
  lapack_int incA = m;

  MatrixD<double> A(m,m,la_A);
  MatrixD<double> B(m,n,la_b);
  MatrixD<double> b(m,n,la_b);
  MatrixD<double> la_x(m,n,la_b);

  lapack_int INFO = LAPACKE_dgeqr2( LAPACK_ROW_MAJOR, m, m, la_A, incA, la_tau );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  VectorD<double> tau(m);
  MatrixD<double> QR(m,m);
  Decompose_QR(A, QR, tau);

  for (int i = 0; i < A.m(); i++)
  {
    BOOST_CHECK_CLOSE( la_tau[i], tau[i], 1e-12 );
    for (int j = 0; j < A.n(); j++)
      BOOST_CHECK_CLOSE( la_A[i*incA+j], QR(i,j), 1e-12 );
  }


  INFO = LAPACKE_dormqr( LAPACK_ROW_MAJOR, 'L', 'T',
                             m, n, m,
                             la_A, incA, la_tau,
                             la_b, n );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  ApplyQTrans( QR, tau, static_cast< MatrixDView< double >& >(B) );

  for (int i = 0; i < B.m(); i++)
    for (int j = 0; j < B.n(); j++)
      BOOST_CHECK_CLOSE( la_b[i*n+j], B(i,j), 2e-12 );


  INFO = LAPACKE_dtrtrs( LAPACK_ROW_MAJOR, 'U', 'N', 'N',
                             m, n, la_A,
                             m, la_b, n );

  BOOST_REQUIRE_EQUAL( INFO, 0 );

  MatrixD<double> x(m,n);
  x = InverseQR::Solve(A, b);

  for (int i = 0; i < x.m(); i++)
    for (int j = 0; j < x.n(); j++)
      BOOST_CHECK_CLOSE( la_b[i*n+j], x(i,j), 1e-11 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( InverseQR_Rand )
{
  typedef double T;
  const T close_tol = 1.3e-8;
  const T small_tol = 1e-12;

  static const int CacheItems = CACHE_LINE_SIZE/sizeof(T);

  for ( int n = 1; n <= 2*CacheItems + CacheItems/2; ++n)
  //for ( int n = 6; n <= 6; ++n)
  {
    MatrixD<T> A(n,n), tmp(n,n), I(n,n), Ainv(n,n);
    MatrixD<T> la_A(n,n), la_Ainv(n,n);

    I = Identity();
    la_Ainv = I;

    for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
        la_A(i,j) = A(i,j) = T(rand() % 101/100.);

    tmp = A;

    Ainv = InverseQR::Inverse(A);

    lapack_int INFO = LAPACKE_dgels( LAPACK_ROW_MAJOR, 'N', n,
                              n, n, &la_A(0,0),
                              n, &la_Ainv(0,0), n );

    BOOST_REQUIRE_EQUAL( INFO, 0 );

    I = Ainv*A;

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
      {
        SANS_CHECK_CLOSE( tmp(i,j), A(i,j), small_tol, close_tol );
        SANS_CHECK_CLOSE( la_Ainv(i,j) , Ainv(i,j), small_tol, close_tol );
        if (i == j)
          BOOST_CHECK_CLOSE( I(i,j) , T(1), close_tol );
        else
          BOOST_CHECK_SMALL( I(i,j) , small_tol );
      }
  }
}
#endif

#else

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LAPACK_Empty )
{
  //This is an empty test to prevent the memcheck from complaining
  BOOST_CHECK( true );
}


#endif //DLA_LAPACK

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
