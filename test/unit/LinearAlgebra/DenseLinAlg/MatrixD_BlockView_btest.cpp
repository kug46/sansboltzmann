// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_BlockView.h"
#include "Surreal/SurrealS.h"
#include "chkMatrixD_btest.h"

#include <iostream>
using namespace SANS::DLA;

namespace SANS
{
namespace DLA
{
//Explicitly instantiate the class to generate all the functions so that coverage information is correct
template class DenseBlockMatrixView<Real>;
}
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BlockView_Exceptions )
{
  typedef Real T;

  Real v0[3*4];
  BOOST_CHECK_THROW( DenseBlockMatrixView<T> BM(v0, 3, 4, 2, 2), AssertionException );
  BOOST_CHECK_THROW( DenseBlockMatrixView<T> BM(v0, 4, 3, 2, 2), AssertionException );

  BOOST_CHECK_THROW( DenseBlockMatrixView<T> BM(v0, 3, 4, 3, 2, 2), AssertionException );
  BOOST_CHECK_THROW( DenseBlockMatrixView<T> BM(v0, 4, 3, 4, 2, 2), AssertionException );

  MatrixD<T> M1(3, 4), M2(4, 3);

  BOOST_CHECK_THROW( DenseBlockMatrixView<T> BM(M1, 2, 2), AssertionException );
  BOOST_CHECK_THROW( DenseBlockMatrixView<T> BM(M2, 2, 2), AssertionException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BlockView_ctor )
{
  typedef Real T;

  Real v0[4*4] = { 1, 1, 2, 2,
                   1, 1, 2, 2,
                   3, 3, 4, 4,
                   3, 3, 4, 4 };

  DenseBlockMatrixView<T> BM0(v0, 4, 4, 2, 2);

  BOOST_CHECK( chkMatrixD22( BM0(0,0), 1,1,1,1 ) );
  BOOST_CHECK( chkMatrixD22( BM0(0,1), 2,2,2,2 ) );
  BOOST_CHECK( chkMatrixD22( BM0(1,0), 3,3,3,3 ) );
  BOOST_CHECK( chkMatrixD22( BM0(1,1), 4,4,4,4 ) );

  BOOST_CHECK_EQUAL( 4, BM0.size() );
  BOOST_CHECK_EQUAL( 2, BM0.m() );
  BOOST_CHECK_EQUAL( 2, BM0.n() );
  BOOST_CHECK_EQUAL( 4, BM0.stride() );

  Real v1[4*6] = { 1, 1, 2, 2, 0, 0,
                   1, 1, 2, 2, 0, 0,
                   3, 3, 4, 4, 0, 0,
                   3, 3, 4, 4, 0, 0 };

  DenseBlockMatrixView<T> BM1(v1, 4, 4, 6, 2, 2);

  BOOST_CHECK( chkMatrixD22( BM1(0,0), 1,1,1,1 ) );
  BOOST_CHECK( chkMatrixD22( BM1(0,1), 2,2,2,2 ) );
  BOOST_CHECK( chkMatrixD22( BM1(1,0), 3,3,3,3 ) );
  BOOST_CHECK( chkMatrixD22( BM1(1,1), 4,4,4,4 ) );

  BOOST_CHECK_EQUAL( 4, BM1.size() );
  BOOST_CHECK_EQUAL( 2, BM1.m() );
  BOOST_CHECK_EQUAL( 2, BM1.n() );
  BOOST_CHECK_EQUAL( 6, BM1.stride() );

  MatrixDView<T> M2(v0, 4, 4);
  DenseBlockMatrixView<T> BM2(M2, 2, 2);

  BOOST_CHECK( chkMatrixD22( BM2(0,0), 1,1,1,1 ) );
  BOOST_CHECK( chkMatrixD22( BM2(0,1), 2,2,2,2 ) );
  BOOST_CHECK( chkMatrixD22( BM2(1,0), 3,3,3,3 ) );
  BOOST_CHECK( chkMatrixD22( BM2(1,1), 4,4,4,4 ) );

  BOOST_CHECK_EQUAL( 4, BM2.size() );
  BOOST_CHECK_EQUAL( 2, BM2.m() );
  BOOST_CHECK_EQUAL( 2, BM2.n() );
  BOOST_CHECK_EQUAL( 4, BM2.stride() );

  MatrixDView<T> M3(v1, 4, 4, 6);
  DenseBlockMatrixView<T> BM3(M3, 2, 2);

  BOOST_CHECK( chkMatrixD22( BM3(0,0), 1,1,1,1 ) );
  BOOST_CHECK( chkMatrixD22( BM3(0,1), 2,2,2,2 ) );
  BOOST_CHECK( chkMatrixD22( BM3(1,0), 3,3,3,3 ) );
  BOOST_CHECK( chkMatrixD22( BM3(1,1), 4,4,4,4 ) );

  BOOST_CHECK_EQUAL( 4, BM3.size() );
  BOOST_CHECK_EQUAL( 2, BM3.m() );
  BOOST_CHECK_EQUAL( 2, BM3.n() );
  BOOST_CHECK_EQUAL( 6, BM3.stride() );

  const DenseBlockMatrixView<T> BM4(M3, 2, 2);

  BOOST_CHECK( chkMatrixD22( BM4(0,0), 1,1,1,1 ) );
  BOOST_CHECK( chkMatrixD22( BM4(0,1), 2,2,2,2 ) );
  BOOST_CHECK( chkMatrixD22( BM4(1,0), 3,3,3,3 ) );
  BOOST_CHECK( chkMatrixD22( BM4(1,1), 4,4,4,4 ) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BlockView_assignment )
{
  typedef Real T;

  Real v0[4*4] = { 1, 1, 2, 2,
                   1, 1, 2, 2,
                   3, 3, 4, 4,
                   3, 3, 4, 4 };

  DenseBlockMatrixView<T> BM0(v0, 4, 4, 2, 2);

  BM0 = 5;

  BOOST_CHECK( chkMatrixD22( BM0(0,0), 5,5,5,5 ) );
  BOOST_CHECK( chkMatrixD22( BM0(0,1), 5,5,5,5 ) );
  BOOST_CHECK( chkMatrixD22( BM0(1,0), 5,5,5,5 ) );
  BOOST_CHECK( chkMatrixD22( BM0(1,1), 5,5,5,5 ) );

  +BM0;

  BOOST_CHECK( chkMatrixD22( BM0(0,0), 5,5,5,5 ) );
  BOOST_CHECK( chkMatrixD22( BM0(0,1), 5,5,5,5 ) );
  BOOST_CHECK( chkMatrixD22( BM0(1,0), 5,5,5,5 ) );
  BOOST_CHECK( chkMatrixD22( BM0(1,1), 5,5,5,5 ) );

  Real v1[4*6] = { 1, 1, 2, 2, 0, 0,
                   1, 1, 2, 2, 0, 0,
                   3, 3, 4, 4, 0, 0,
                   3, 3, 4, 4, 0, 0 };

  DenseBlockMatrixView<T> BM1(v1, 4, 4, 6, 2, 2);

  BM1 = 5;

  BOOST_CHECK( chkMatrixD22( BM1(0,0), 5,5,5,5 ) );
  BOOST_CHECK( chkMatrixD22( BM1(0,1), 5,5,5,5 ) );
  BOOST_CHECK( chkMatrixD22( BM1(1,0), 5,5,5,5 ) );
  BOOST_CHECK( chkMatrixD22( BM1(1,1), 5,5,5,5 ) );

  BOOST_CHECK_EQUAL( v1[0*6 + 4 + 0], 0 );
  BOOST_CHECK_EQUAL( v1[0*6 + 4 + 1], 0 );
  BOOST_CHECK_EQUAL( v1[1*6 + 4 + 0], 0 );
  BOOST_CHECK_EQUAL( v1[1*6 + 4 + 1], 0 );
  BOOST_CHECK_EQUAL( v1[2*6 + 4 + 0], 0 );
  BOOST_CHECK_EQUAL( v1[2*6 + 4 + 1], 0 );
  BOOST_CHECK_EQUAL( v1[3*6 + 4 + 0], 0 );
  BOOST_CHECK_EQUAL( v1[3*6 + 4 + 1], 0 );

  MatrixDView<T> M2(v0, 4, 4);
  DenseBlockMatrixView<T> BM2(M2, 2, 2);

  BM2 = Identity();

  BOOST_CHECK( chkMatrixD22( BM2(0,0), 1,0,0,1 ) );
  BOOST_CHECK( chkMatrixD22( BM2(0,1), 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixD22( BM2(1,0), 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixD22( BM2(1,1), 1,0,0,1 ) );

  MatrixDView<T> M3(v1, 4, 4, 6);
  DenseBlockMatrixView<T> BM3(M3, 2, 2);

  BM3 = Identity();

  BOOST_CHECK( chkMatrixD22( BM3(0,0), 1,0,0,1 ) );
  BOOST_CHECK( chkMatrixD22( BM3(0,1), 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixD22( BM3(1,0), 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixD22( BM3(1,1), 1,0,0,1 ) );

  BOOST_CHECK_EQUAL( v1[0*6 + 4 + 0], 0 );
  BOOST_CHECK_EQUAL( v1[0*6 + 4 + 1], 0 );
  BOOST_CHECK_EQUAL( v1[1*6 + 4 + 0], 0 );
  BOOST_CHECK_EQUAL( v1[1*6 + 4 + 1], 0 );
  BOOST_CHECK_EQUAL( v1[2*6 + 4 + 0], 0 );
  BOOST_CHECK_EQUAL( v1[2*6 + 4 + 1], 0 );
  BOOST_CHECK_EQUAL( v1[3*6 + 4 + 0], 0 );
  BOOST_CHECK_EQUAL( v1[3*6 + 4 + 1], 0 );

  MatrixD<T> M4(4, 4);
  DenseBlockMatrixView<T> BM4(M4, 2, 2);

  BM4 = Identity();

  BOOST_CHECK( chkMatrixD22( BM4(0,0), 1,0,0,1 ) );
  BOOST_CHECK( chkMatrixD22( BM4(0,1), 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixD22( BM4(1,0), 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixD22( BM4(1,1), 1,0,0,1 ) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BlockView_operators )
{
  typedef Real T;

  T v0[4*4] = { 1, 1, 2, 2,
                1, 1, 2, 2,
                3, 3, 4, 4,
                3, 3, 4, 4 };

  DenseBlockMatrixView<T> BM0(v0, 4, 4, 2, 2);

  BM0 *= 2;

  BOOST_CHECK( chkMatrixD22( BM0(0,0), 2,2,2,2 ) );
  BOOST_CHECK( chkMatrixD22( BM0(0,1), 4,4,4,4 ) );
  BOOST_CHECK( chkMatrixD22( BM0(1,0), 6,6,6,6 ) );
  BOOST_CHECK( chkMatrixD22( BM0(1,1), 8,8,8,8 ) );

  T v1[4*6] = { 1, 1, 2, 2, 0, 0,
                1, 1, 2, 2, 0, 0,
                3, 3, 4, 4, 0, 0,
                3, 3, 4, 4, 0, 0 };

  DenseBlockMatrixView<T> BM1(v1, 4, 4, 6, 2, 2);

  BM1 *= 2;

  BOOST_CHECK( chkMatrixD22( BM1(0,0), 2,2,2,2 ) );
  BOOST_CHECK( chkMatrixD22( BM1(0,1), 4,4,4,4 ) );
  BOOST_CHECK( chkMatrixD22( BM1(1,0), 6,6,6,6 ) );
  BOOST_CHECK( chkMatrixD22( BM1(1,1), 8,8,8,8 ) );

  MatrixDView<T> M2(v0, 4, 4);
  DenseBlockMatrixView<T> BM2(M2, 2, 2);

  BM2 /= 2;

  BOOST_CHECK( chkMatrixD22( BM2(0,0), 1,1,1,1 ) );
  BOOST_CHECK( chkMatrixD22( BM2(0,1), 2,2,2,2 ) );
  BOOST_CHECK( chkMatrixD22( BM2(1,0), 3,3,3,3 ) );
  BOOST_CHECK( chkMatrixD22( BM2(1,1), 4,4,4,4 ) );

  MatrixDView<T> M3(v1, 4, 4, 6);
  DenseBlockMatrixView<T> BM3(M3, 2, 2);

  BM3 /= 2;

  BOOST_CHECK( chkMatrixD22( BM3(0,0), 1,1,1,1 ) );
  BOOST_CHECK( chkMatrixD22( BM3(0,1), 2,2,2,2 ) );
  BOOST_CHECK( chkMatrixD22( BM3(1,0), 3,3,3,3 ) );
  BOOST_CHECK( chkMatrixD22( BM3(1,1), 4,4,4,4 ) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BlockView_operators_Surreal )
{
  typedef SurrealS<1> T;

  T v0[4*4] = { 1, 1, 2, 2,
                1, 1, 2, 2,
                3, 3, 4, 4,
                3, 3, 4, 4 };

  DenseBlockMatrixView<T> BM0(v0, 4, 4, 2, 2);

  BM0 *= 2;

  BOOST_CHECK( chkMatrixD22( BM0(0,0), 2,2,2,2 ) );
  BOOST_CHECK( chkMatrixD22( BM0(0,1), 4,4,4,4 ) );
  BOOST_CHECK( chkMatrixD22( BM0(1,0), 6,6,6,6 ) );
  BOOST_CHECK( chkMatrixD22( BM0(1,1), 8,8,8,8 ) );

  T v1[4*6] = { 1, 1, 2, 2, 0, 0,
                1, 1, 2, 2, 0, 0,
                3, 3, 4, 4, 0, 0,
                3, 3, 4, 4, 0, 0 };

  DenseBlockMatrixView<T> BM1(v1, 4, 4, 6, 2, 2);

  BM1 *= 2;

  BOOST_CHECK( chkMatrixD22( BM1(0,0), 2,2,2,2 ) );
  BOOST_CHECK( chkMatrixD22( BM1(0,1), 4,4,4,4 ) );
  BOOST_CHECK( chkMatrixD22( BM1(1,0), 6,6,6,6 ) );
  BOOST_CHECK( chkMatrixD22( BM1(1,1), 8,8,8,8 ) );

  MatrixDView<T> M2(v0, 4, 4);
  DenseBlockMatrixView<T> BM2(M2, 2, 2);

  BM2 /= 2;

  BOOST_CHECK( chkMatrixD22( BM2(0,0), 1,1,1,1 ) );
  BOOST_CHECK( chkMatrixD22( BM2(0,1), 2,2,2,2 ) );
  BOOST_CHECK( chkMatrixD22( BM2(1,0), 3,3,3,3 ) );
  BOOST_CHECK( chkMatrixD22( BM2(1,1), 4,4,4,4 ) );

  MatrixDView<T> M3(v1, 4, 4, 6);
  DenseBlockMatrixView<T> BM3(M3, 2, 2);

  BM3 /= 2;

  BOOST_CHECK( chkMatrixD22( BM3(0,0), 1,1,1,1 ) );
  BOOST_CHECK( chkMatrixD22( BM3(0,1), 2,2,2,2 ) );
  BOOST_CHECK( chkMatrixD22( BM3(1,0), 3,3,3,3 ) );
  BOOST_CHECK( chkMatrixD22( BM3(1,1), 4,4,4,4 ) );

  BM3 = 5;

  BOOST_CHECK( chkMatrixD22( BM3(0,0), 5,5,5,5 ) );
  BOOST_CHECK( chkMatrixD22( BM3(0,1), 5,5,5,5 ) );
  BOOST_CHECK( chkMatrixD22( BM3(1,0), 5,5,5,5 ) );
  BOOST_CHECK( chkMatrixD22( BM3(1,1), 5,5,5,5 ) );


}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
