// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/ElementaryReflector.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Decompose_QR.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/ElementaryReflector.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Decompose_QR.h"
#include "LinearAlgebra/DenseLinAlg/InverseQR.h"

#include <iostream>
#include <limits>
using namespace SANS::DLA;

namespace SANS
{
namespace DLA
{
//Declare for tesing purposes. No need to use this anywhere else. Defined in MatrixD_InverseQR.cpp
template< class T, class MatrixType >
void ApplyQTrans( MatrixDView< T >& A, const VectorD<T>& tau, MatrixType& B );
}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_ElementaryReflector_test_1 )
{

  VectorD<Real> xD = {3, 5, 6};
  Real tauD = 0;
  Real alphaD = 1;

  VectorS<3,Real> xS(&xD[0], xD.m());
  Real tauS = tauD;
  Real alphaS = alphaD;


  ElementaryReflector(alphaD, xD, tauD);
  ElementaryReflector(alphaS, xS, tauS);

  BOOST_CHECK_CLOSE( alphaD, alphaS, 1e-12 );
  BOOST_CHECK_CLOSE( tauD, tauS, 1e-12 );
  for (int i = 0; i < xD.size(); i++)
    BOOST_CHECK_CLOSE( xD[i], xS[i], 1e-12 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_ElementaryReflector_test_2 )
{

  VectorD<Real> xD = {std::numeric_limits<double>::min()/2, 0., 0.};
  Real tauD = 0;
  Real alphaD = std::numeric_limits<Real>::min()/2;

  VectorS<3,Real> xS(&xD[0], xD.m());
  Real tauS = tauD;
  Real alphaS = alphaD;


  ElementaryReflector(alphaD, xD, tauD);
  ElementaryReflector(alphaS, xS, tauS);

  BOOST_CHECK_CLOSE( alphaD, alphaS, 1e-12 );
  BOOST_CHECK_CLOSE( tauD, tauS, 1e-12 );
  for (int i = 0; i < xD.size(); i++)
    BOOST_CHECK_CLOSE( xD[i], xS[i], 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_ElementaryReflector_test_3 )
{
  VectorD<Real> xD = {0, 0, 0};
  Real tauD = 0;
  Real alphaD = 1;

  VectorS<3,Real> xS(&xD[0], xD.m());
  Real tauS = tauD;
  Real alphaS = alphaD;


  ElementaryReflector(alphaD, xD, tauD);
  ElementaryReflector(alphaS, xS, tauS);

  BOOST_CHECK_CLOSE( alphaD, alphaS, 1e-12 );
  BOOST_CHECK_CLOSE( tauD, tauS, 1e-12 );
  for (int i = 0; i < xD.size(); i++)
    BOOST_CHECK_CLOSE( xD[i], xS[i], 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_ElementaryReflector_test_4 )
{
  VectorD<Real> xD = {3, 5, 6};
  Real tauD = 0;
  Real alphaD = 0;

  VectorS<3,Real> xS(&xD[0], xD.m());
  Real tauS = tauD;
  Real alphaS = alphaD;


  ElementaryReflector(alphaD, xD, tauD);
  ElementaryReflector(alphaS, xS, tauS);

  BOOST_CHECK_CLOSE( alphaD, alphaS, 1e-12 );
  BOOST_CHECK_CLOSE( tauD, tauS, 1e-12 );
  for (int i = 0; i < xD.size(); i++)
    BOOST_CHECK_CLOSE( xD[i], xS[i], 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_ElementaryReflector_test_5 )
{
  VectorD<Real> xD = {1.32, 0.12, 0.08};
  Real tauD = 0;
  Real alphaD = 0;

  VectorS<3,Real> xS(&xD[0], xD.m());
  Real tauS = tauD;
  Real alphaS = alphaD;


  ElementaryReflector(alphaD, xD, tauD);
  ElementaryReflector(alphaS, xS, tauS);

  BOOST_CHECK_CLOSE( alphaD, alphaS, 1e-12 );
  BOOST_CHECK_CLOSE( tauD, tauS, 1e-12 );
  for (int i = 0; i < xD.size(); i++)
    BOOST_CHECK_CLOSE( xD[i], xS[i], 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_ElementaryReflector_test_6 )
{
  VectorD<Real> xD = {0.08, 0.65, 0.89, 0.38, 0.51};
  Real tauD = 0;
  Real alphaD = 1.32;

  VectorS<5,Real> xS(&xD[0], xD.m());
  Real tauS = tauD;
  Real alphaS = alphaD;


  ElementaryReflector(alphaD, xD, tauD);
  ElementaryReflector(alphaS, xS, tauS);

  BOOST_CHECK_CLOSE( alphaD, alphaS, 1e-12 );
  BOOST_CHECK_CLOSE( tauD, tauS, 1e-12 );
  for (int i = 0; i < xD.size(); i++)
    BOOST_CHECK_CLOSE( xD[i], xS[i], 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Decompose_QR_test )
{

  MatrixD<Real> AD = {{2, 1 ,3},
                      {1, 1, 3},
                      {0, 2, 1}};
  VectorD<Real> tauD(3);

  MatrixS<3,3,Real> AS(&AD(0,0),9);
  VectorS<3,Real> tauS;

  MatrixD<Real> QRD(3,3);
  Decompose_QR(AD, QRD, tauD);

  MatrixS<3,3,Real> QRS;
  Decompose_QR(AS, QRS, tauS);

  for (int i = 0; i < AD.m(); i++)
  {
    //std::cout << "tauD[" << i << "] = " << tauD[i] << "  tauS[" << i << "] = " << tauS[i] << std::endl;
    BOOST_CHECK_CLOSE( tauD[i], tauS[i], 1e-12 );
    for (int j = 0; j < AD.n(); j++)
      BOOST_CHECK_CLOSE( QRD(i,j), QRS(i,j), 1e-12 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Decompose_QR_LeastSquare_test )
{

  MatrixD<Real> AD = {{2, 1},
                      {1, 1},
                      {3, 2}};
  VectorD<Real> tauD(2);

  MatrixS<3,2,Real> AS(&AD(0,0),6);
  VectorS<2,Real> tauS;

  MatrixD<Real> QRD(3,2);
  Decompose_QR(AD, QRD, tauD);

  MatrixS<3,2,Real> QRS;
  Decompose_QR(AS, QRS, tauS);

  for (int i = 0; i < tauD.m(); i++)
  {
    //std::cout << "tauD[" << i << "] = " << tauD[i] << "  tauS[" << i << "] = " << tauS[i] << std::endl;
    BOOST_CHECK_CLOSE( tauD[i], tauS[i], 1e-12 );
  }

  for (int i = 0; i < QRD.m(); i++)
    for (int j = 0; j < QRD.n(); j++)
      BOOST_CHECK_CLOSE( QRD(i,j), QRS(i,j), 1e-12 );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
