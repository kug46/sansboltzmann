// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

#include <iostream>
using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Util_Swap )
{
  MatrixS<2,3,Real> M1;

  for (int i = 0; i < 3; ++i)
  {
    M1(0,i) = i+1;
    M1(1,i) = i+3;
  }

  M1.swap_rows(0,1);

  for (int i = 0; i < 3; ++i)
  {
    BOOST_CHECK_EQUAL( M1(0,i), i+3 );
    BOOST_CHECK_EQUAL( M1(1,i), i+1 );
  }


  MatrixS<3,3,Real> M2;

  for (int i = 0; i < 3; ++i)
  {
    M2(0,i) = i+1;
    M2(1,i) = i+3;
    M2(2,i) = i+5;
  }

  M2.swap_rows(0,2);

  for (int i = 0; i < 3; ++i)
  {
    BOOST_CHECK_EQUAL( M2(0,i), i+5 );
    BOOST_CHECK_EQUAL( M2(1,i), i+3 );
    BOOST_CHECK_EQUAL( M2(2,i), i+1 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Util_Scale )
{

  MatrixS<3,3,Real> M2;

  for (int i = 0; i < 3; ++i)
  {
    M2(0,i) = i+1;
    M2(1,i) = i+3;
    M2(2,i) = i+5;
  }

  M2.scale_row(1,2);

  for (int i = 0; i < 3; ++i)
  {
    BOOST_CHECK_EQUAL( M2(0,i), i+1 );
    BOOST_CHECK_EQUAL( M2(1,i), 2*(i+3) );
    BOOST_CHECK_EQUAL( M2(2,i), i+5 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Util_axpy )
{

  MatrixS<3,3,Real> M2;

  for (int i = 0; i < 3; ++i)
  {
    M2(0,i) = i+1;
    M2(1,i) = i+3;
    M2(2,i) = i+5;
  }

  M2.axpy_rows(0,1,2);

  for (int i = 0; i < 3; ++i)
  {
    BOOST_CHECK_EQUAL( M2(0,i), i+1 );
    BOOST_CHECK_EQUAL( M2(1,i), i+3 + 2*(i+1) );
    BOOST_CHECK_EQUAL( M2(2,i), i+5 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixS_Util_max_row_in_col )
{

  MatrixS<3,3,Real> M2;

  for (int i = 0; i < 3; ++i)
  {
    M2(0,i) = 4-i;
    M2(1,i) = pow(i,3);
    M2(2,i) = i+1;
  }

  BOOST_CHECK_EQUAL( M2.max_row_in_col(0), 0);
  BOOST_CHECK_EQUAL( M2.max_row_in_col(1), 0);
  BOOST_CHECK_EQUAL( M2.max_row_in_col(2), 1);

  BOOST_CHECK_EQUAL( M2.max_row_in_col(0,0), 0);
  BOOST_CHECK_EQUAL( M2.max_row_in_col(1,1), 2);
  BOOST_CHECK_EQUAL( M2.max_row_in_col(2,2), 2);

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
