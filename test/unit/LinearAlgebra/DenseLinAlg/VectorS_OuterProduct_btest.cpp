// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS_OuterProduct.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include <iostream>
using namespace SANS::DLA;


//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_OuterProduct_MatrixS_test )
{
  typedef VectorS<4,int> VectorS4;
  typedef MatrixS<4,4,int> MatrixS4;
  VectorS4 a = {1,2,3,4};
  VectorS4 b = {2,3,4,5};

  MatrixS4 M = OuterProduct(a,b);
  MatrixS4 Mexact = { {2, 3, 4, 5},
                      {4, 6, 8,10},
                      {6, 9,12,15},
                      {8,12,16,20} };

  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++)
      BOOST_CHECK_EQUAL(M(i,j), Mexact(i,j));

  M = OuterProduct(b,a);
  Mexact = {{2, 4, 6, 8},
            {3, 6, 9,12},
            {4, 8,12,16},
            {5,10,15,20} };

  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++)
      BOOST_CHECK_EQUAL(M(i,j), Mexact(i,j));
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_OuterProduct_MatrixSymS_test )
{
  typedef VectorS<4,int> VectorS4;
  typedef MatrixSymS<4,int> MatrixSymS4;
  VectorS4 a = {2,3,4,5};

  MatrixSymS4 M = OuterProduct(a);

  MatrixSymS4 Mexact = { {4},
                         {6,9},
                         {8,12,16},
                         {10,15,20,25} };

  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++)
      BOOST_CHECK_EQUAL(M(i,j), Mexact(i,j));

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
