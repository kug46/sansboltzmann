// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// MatrixS_SurrealB_btest
// testing of MatrixS<M,N,T> class with T = SurrealD

#include <iostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "Surreal/SurrealD.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
namespace DLA
{
template class MatrixS<2,2, SurrealD>;
}
}

using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( MatrixS_SurrealB_test_suite )

typedef SurrealD Int;

typedef VectorS<1,Int> VectorS1;
typedef MatrixS<1,1,Int> MatrixS1;

typedef VectorS<2,Int> VectorS2;
typedef MatrixS<2,2,Int> MatrixS2;


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( size1 )
{
  BOOST_CHECK( MatrixS1::M == 1 );
  BOOST_CHECK( MatrixS1::N == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( matrix_ops1 )
{
  const Int data = 3;
  MatrixS1 m1(&data, 1);
  MatrixS1 m2(m1);
  MatrixS1 m3(1), m4, m5;

  // size
  BOOST_CHECK( m1.M == 1 );
  BOOST_CHECK( m1.N == 1 );
  BOOST_CHECK( m2.M == 1 );
  BOOST_CHECK( m2.N == 1 );

  // ctors
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  1, m3(0,0) );

  // assignment
  m3 = m1;
  m4 = data;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m3(0,0) );
  BOOST_CHECK_EQUAL(  3, m4(0,0) );

  m1 = m2 = m3 = data;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  3, m3(0,0) );

  m4 = data;
  m1 = m2 = m3 = m4;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  3, m3(0,0) );
  BOOST_CHECK_EQUAL(  3, m4(0,0) );

  // unary
  m2 = +m1;
  m3 = -m1;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL( -3, m3(0,0) );

  // binary accumulation
  m2 = m1;
  m3 = m1;
  m1 += data;
  m2 -= data;
  m3 *= data;
  BOOST_CHECK_EQUAL(  6, m1(0,0) );
  BOOST_CHECK_EQUAL(  0, m2(0,0) );
  BOOST_CHECK_EQUAL(  9, m3(0,0) );

  m1 = data;
  m2 = m1;
  m3 = m1;
  m2 += m1;
  m3 -= m1;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  6, m2(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0) );

  // binary operators
  m1 = data;
//  m2 = m1 + data;
//  m3 = m1 - data;
  m4 = m1 * data;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
//  BOOST_CHECK_EQUAL(  6, m2(0,0) );
//  BOOST_CHECK_EQUAL(  0, m3(0,0) );
  BOOST_CHECK_EQUAL(  9, m4(0,0) );

//  m2 = data + m1;
//  m3 = data - m1;
  m4 = data * m1;
//  BOOST_CHECK_EQUAL(  3, m1(0,0) );
//  BOOST_CHECK_EQUAL(  6, m2(0,0) );
//  BOOST_CHECK_EQUAL(  0, m3(0,0) );
  BOOST_CHECK_EQUAL(  9, m4(0,0) );

  m1 = m2 = data;
  m3 = m1 + m2;
  m4 = m1 - m2;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  6, m3(0,0) );
  BOOST_CHECK_EQUAL(  0, m4(0,0) );

  // arithmetic combinations

  m1 = m2 = data;
  m3 = m1 + m2;
  m4 = m1 + m2 + m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  6, m3(0,0) );
  BOOST_CHECK_EQUAL( 12, m4(0,0) );

  m2 += m1;
  m3 += m1 + m2;
  m4 += m1 + m2 + m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  6, m2(0,0) );
  BOOST_CHECK_EQUAL( 15, m3(0,0) );
  BOOST_CHECK_EQUAL( 36, m4(0,0) );

  m3 = m1 - m2;
  m4 = m1 - m2 - m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  6, m2(0,0) );
  BOOST_CHECK_EQUAL( -3, m3(0,0) );
  BOOST_CHECK_EQUAL(  0, m4(0,0) );

  m2 -= m1;
  m3 -= m1 - m2;
  m4 -= m1 - m2 - m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL( -3, m3(0,0) );
  BOOST_CHECK_EQUAL( -3, m4(0,0) );

  m3 = m1 - m2;
  m4 = m1 + m2 - m3;
  m5 = m1 - m2 + m3;
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0) );
  BOOST_CHECK_EQUAL(  6, m4(0,0) );
  BOOST_CHECK_EQUAL(  0, m5(0,0) );

#ifndef __clang_analyzer__
  m5 = (m1 + m2) + (m3 + m4);
  BOOST_CHECK_EQUAL( 12, m5(0,0) );
  m5 = (m1 + m2) + (m3 - m4);
  BOOST_CHECK_EQUAL(  0, m5(0,0) );
  m5 = (m1 + m2) - (m3 + m4);
  BOOST_CHECK_EQUAL(  0, m5(0,0) );
  m5 = (m1 + m2) - (m3 - m4);
  BOOST_CHECK_EQUAL( 12, m5(0,0) );
  m5 = (m1 - m2) + (m3 + m4);
  BOOST_CHECK_EQUAL(  6, m5(0,0) );
  m5 = (m1 - m2) + (m3 - m4);
  BOOST_CHECK_EQUAL( -6, m5(0,0) );
  m5 = (m1 - m2) - (m3 + m4);
  BOOST_CHECK_EQUAL( -6, m5(0,0) );
  m5 = (m1 - m2) - (m3 - m4);
  BOOST_CHECK_EQUAL(  6, m5(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0) );
  BOOST_CHECK_EQUAL(  6, m4(0,0) );

  m5 += (m1 + m2) + (m3 + m4);
  m5 += (m1 + m2) + (m3 - m4);
  m5 += (m1 + m2) - (m3 + m4);
  m5 += (m1 + m2) - (m3 - m4);
  m5 += (m1 - m2) + (m3 + m4);
  m5 += (m1 - m2) + (m3 - m4);
  m5 += (m1 - m2) - (m3 + m4);
  m5 += (m1 - m2) - (m3 - m4);
  BOOST_CHECK_EQUAL( 30, m5(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0) );
  BOOST_CHECK_EQUAL(  6, m4(0,0) );

  m5 -= (m1 + m2) + (m3 + m4);
  m5 -= (m1 + m2) + (m3 - m4);
  m5 -= (m1 + m2) - (m3 + m4);
  m5 -= (m1 + m2) - (m3 - m4);
  m5 -= (m1 - m2) + (m3 + m4);
  m5 -= (m1 - m2) + (m3 - m4);
  m5 -= (m1 - m2) - (m3 + m4);
  m5 -= (m1 - m2) - (m3 - m4);
  BOOST_CHECK_EQUAL(  6, m5(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL(  0, m3(0,0) );
  BOOST_CHECK_EQUAL(  6, m4(0,0) );
#endif

  m1 = data;

  m2 = 4*m1;
  m3 = m2*7;
  BOOST_CHECK_EQUAL(   3, m1(0,0) );
  BOOST_CHECK_EQUAL(  12, m2(0,0) );
  BOOST_CHECK_EQUAL(  84, m3(0,0) );

  m2 += 4*m1;
  m3 += m2*7;
  BOOST_CHECK_EQUAL(   3, m1(0,0) );
  BOOST_CHECK_EQUAL(  24, m2(0,0) );
  BOOST_CHECK_EQUAL( 252, m3(0,0) );

  m2 -= 4*m1;
  m3 -= m2*7;
  BOOST_CHECK_EQUAL(   3, m1(0,0) );
  BOOST_CHECK_EQUAL(  12, m2(0,0) );
  BOOST_CHECK_EQUAL( 168, m3(0,0) );

  m5 = 2*(m1 + m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL(  552, m5(0,0) );
  m5 = 2*(m1 + m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  516, m5(0,0) );
  m5 = 2*(m1 + m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -492, m5(0,0) );
  m5 = 2*(m1 + m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -456, m5(0,0) );
  m5 = 2*(m1 - m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL(  504, m5(0,0) );
  m5 = 2*(m1 - m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  468, m5(0,0) );
  m5 = 2*(m1 - m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -540, m5(0,0) );
  m5 = 2*(m1 - m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -504, m5(0,0) );
  BOOST_CHECK_EQUAL(    3, m1(0,0) );
  BOOST_CHECK_EQUAL(   12, m2(0,0) );
  BOOST_CHECK_EQUAL(  168, m3(0,0) );
  BOOST_CHECK_EQUAL(    6, m4(0,0) );

#ifndef __clang_analyzer__
  m5 += 2*(m1 + m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL(   48, m5(0,0) );
  m5 += 2*(m1 + m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  564, m5(0,0) );
  m5 += 2*(m1 + m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL(   72, m5(0,0) );
  m5 += 2*(m1 + m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -384, m5(0,0) );
  m5 += 2*(m1 - m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL(  120, m5(0,0) );
  m5 += 2*(m1 - m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  588, m5(0,0) );
  m5 += 2*(m1 - m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL(   48, m5(0,0) );
  m5 += 2*(m1 - m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -456, m5(0,0) );
  BOOST_CHECK_EQUAL(    3, m1(0,0) );
  BOOST_CHECK_EQUAL(   12, m2(0,0) );
  BOOST_CHECK_EQUAL(  168, m3(0,0) );
  BOOST_CHECK_EQUAL(    6, m4(0,0) );

  m5 -= 2*(m1 + m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -1008, m5(0,0) );
  m5 -= 2*(m1 + m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -1524, m5(0,0) );
  m5 -= 2*(m1 + m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -1032, m5(0,0) );
  m5 -= 2*(m1 + m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  -576, m5(0,0) );
  m5 -= 2*(m1 - m2) + (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -1080, m5(0,0) );
  m5 -= 2*(m1 - m2) + (m3 - m4)*3;
  BOOST_CHECK_EQUAL( -1548, m5(0,0) );
  m5 -= 2*(m1 - m2) - (m3 + m4)*3;
  BOOST_CHECK_EQUAL( -1008, m5(0,0) );
  m5 -= 2*(m1 - m2) - (m3 - m4)*3;
  BOOST_CHECK_EQUAL(  -504, m5(0,0) );
  BOOST_CHECK_EQUAL(     3, m1(0,0) );
  BOOST_CHECK_EQUAL(    12, m2(0,0) );
  BOOST_CHECK_EQUAL(   168, m3(0,0) );
  BOOST_CHECK_EQUAL(     6, m4(0,0) );
#endif

  m5 = 2*(m1 + m2)*3;
  BOOST_CHECK_EQUAL( 90, m5(0,0) );
  m5 = 2*3*(m1 + m2);
  BOOST_CHECK_EQUAL( 90, m5(0,0) );
  m5 = (m1 + m2)*2*3;
  BOOST_CHECK_EQUAL( 90, m5(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL( 12, m2(0,0) );

  m2 = +m1;
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  m3 = -m2;
  BOOST_CHECK_EQUAL( -3, m3(0,0) );
  m4 = +(m1 + m2);
  BOOST_CHECK_EQUAL(  6, m4(0,0) );
  m4 = +(m1 - m2);
  BOOST_CHECK_EQUAL(  0, m4(0,0) );
  m4 = -(m1 + m2);
  BOOST_CHECK_EQUAL( -6, m4(0,0) );
  m4 = -(m1 - m2);
  BOOST_CHECK_EQUAL(  0, m4(0,0) );
  m4 = +(m1 + m2) + m3;
  BOOST_CHECK_EQUAL(  3, m4(0,0) );
  m4 = -(m1 + m2) + m3;
  BOOST_CHECK_EQUAL( -9, m4(0,0) );
  BOOST_CHECK_EQUAL(  3, m1(0,0) );
  BOOST_CHECK_EQUAL(  3, m2(0,0) );
  BOOST_CHECK_EQUAL( -3, m3(0,0) );

  m4 = +5*m1;
  BOOST_CHECK_EQUAL(  15, m4(0,0) );
  m4 = -5*m1;
  BOOST_CHECK_EQUAL( -15, m4(0,0) );
  m4 = +m1*5;
  BOOST_CHECK_EQUAL(  15, m4(0,0) );
  m4 = -m1*5;
  BOOST_CHECK_EQUAL( -15, m4(0,0) );
  m4 = +(5*m1);
  BOOST_CHECK_EQUAL(  15, m4(0,0) );
  m4 = -(5*m1);
  BOOST_CHECK_EQUAL( -15, m4(0,0) );
  m4 = +(m1*5);
  BOOST_CHECK_EQUAL(  15, m4(0,0) );
  m4 = -(m1*5);
  BOOST_CHECK_EQUAL( -15, m4(0,0) );
  BOOST_CHECK_EQUAL(   3, m1(0,0) );
}


//----------------------------------------------------------------------------//
bool chkVectorS2( const VectorS2& z, Int a, Int b )
{
  bool isEqual = true;
  if ((z[0] != a) || (z[1] != b))
  {
    isEqual = false;
    cout << "actual (" << z << ")  expected ("
         << "(" << a << " " << b << "))" << endl;
  }
  return isEqual;
}


//----------------------------------------------------------------------------//
bool chkMatrixS2( const MatrixS2& z, Int m00, Int m01, Int m10, Int m11 )
{
  bool isEqual = true;
  if (z(0,0) != m00) isEqual = false;
  if (z(0,1) != m01) isEqual = false;
  if (z(1,0) != m10) isEqual = false;
  if (z(1,1) != m11) isEqual = false;
  if (!isEqual)
  {
    cout << "actual ("
         << "(" << z(0,0) << " " << z(0,1) << ") "
         << "(" << z(1,0) << " " << z(1,1) << ") "
         << ")" << endl;
    cout << "  expected ("
         << "(" << m00 << " " << m01 << ") "
         << "(" << m10 << " " << m11 << ") "
         << ")" << endl;
  }
  return isEqual;
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( size2 )
{
  BOOST_CHECK( VectorS2::M == 2 );
  BOOST_CHECK( VectorS2::N == 1 );
  BOOST_CHECK( MatrixS2::M == 2 );
  BOOST_CHECK( MatrixS2::N == 2 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( matrix_ops2 )
{
  const Int data[4] = {1,2,3,4};
  MatrixS2 m1(data, 4);
  MatrixS2 m2(m1), m3, m4, m5;

  // size
  BOOST_CHECK( m1.M == 2 );
  BOOST_CHECK( m1.N == 2 );
  BOOST_CHECK( m2.M == 2 );
  BOOST_CHECK( m2.N == 2 );

  // ctors
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m2, 1,2,3,4 ) );

  // assignment
  m3 = m1;
  m4 = 5;
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m3, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m4, 5,5,5,5 ) );

  m2 = m3 = 3;
  BOOST_CHECK( chkMatrixS2( m2, 3,3,3,3 ) );
  BOOST_CHECK( chkMatrixS2( m3, 3,3,3,3 ) );

  // unary
  m2 = +m1;
  m3 = -m1;
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m3, -1,-2,-3,-4 ) );

  // binary accumulation
  m2 = m1;
  m3 = m1;
  m4 = m1;
  m2 += 5;
  m3 -= 5;
  m4 *= 5;
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m2, 6,7,8,9 ) );
  BOOST_CHECK( chkMatrixS2( m3, -4,-3,-2,-1 ) );
  BOOST_CHECK( chkMatrixS2( m4, 5,10,15,20 ) );

  m2 = 5;
  m3 = 5;
  m2 += m1;
  m3 -= m1;
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m2, 6,7,8,9 ) );
  BOOST_CHECK( chkMatrixS2( m3, 4,3,2,1 ) );

  // binary operators
//  m2 = m1 + 3;
//  m3 = m1 - 3;
  m4 = m1 * 3;
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
//  BOOST_CHECK( chkMatrixS2( m2, 4,5,6,7 ) );
//  BOOST_CHECK( chkMatrixS2( m3, -2,-1,0,1 ) );
  BOOST_CHECK( chkMatrixS2( m4, 3,6,9,12 ) );

//  m2 = 3 + m1;
//  m3 = 3 - m1;
  m4 = 3 * m1;
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
//  BOOST_CHECK( chkMatrixS2( m2, 4,5,6,7 ) );
//  BOOST_CHECK( chkMatrixS2( m3, 2,1,0,-1 ) );
  BOOST_CHECK( chkMatrixS2( m4, 3,6,9,12 ) );

  m2 = 3;
  m3 = m1 + m2;
  m4 = m1 - m2;
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m2, 3,3,3,3 ) );
  BOOST_CHECK( chkMatrixS2( m3, 4,5,6,7 ) );
  BOOST_CHECK( chkMatrixS2( m4, -2,-1,0,1 ) );

  // arithmetic combinations

  m2 = m1;
  m3 = m1 + m2;
  m4 = m1 + m2 + m3;
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m3, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixS2( m4, 4,8,12,16 ) );

  m2 += m1;
  m3 += m1 + m2;
  m4 += m1 + m2 + m3;
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m2, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixS2( m3, 5,10,15,20 ) );
  BOOST_CHECK( chkMatrixS2( m4, 12,24,36,48 ) );

  m3 = m1 - m2;
  m4 = m1 - m2 - m3;
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m2, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixS2( m3, -1,-2,-3,-4 ) );
  BOOST_CHECK( chkMatrixS2( m4, 0,0,0,0 ) );

  m2 -= m1;
  m3 -= m1 - m2;
  m4 -= m1 - m2 - m3;
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m3, -1,-2,-3,-4 ) );
  BOOST_CHECK( chkMatrixS2( m4, -1,-2,-3,-4 ) );

  m3 = m1 - m2;
  m4 = m1 + m2 - m3;
  m5 = m1 - m2 + m3;
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m3, 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixS2( m4, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixS2( m5, 0,0,0,0 ) );

  m5 = (m1 + m2) + (m3 + m4);
  BOOST_CHECK( chkMatrixS2( m5, 4,8,12,16 ) );
  m5 = (m1 + m2) + (m3 - m4);
  BOOST_CHECK( chkMatrixS2( m5, 0,0,0,0 ) );
  m5 = (m1 + m2) - (m3 + m4);
  BOOST_CHECK( chkMatrixS2( m5, 0,0,0,0 ) );
  m5 = (m1 + m2) - (m3 - m4);
  BOOST_CHECK( chkMatrixS2( m5, 4,8,12,16 ) );
  m5 = (m1 - m2) + (m3 + m4);
  BOOST_CHECK( chkMatrixS2( m5, 2,4,6,8 ) );
  m5 = (m1 - m2) + (m3 - m4);
  BOOST_CHECK( chkMatrixS2( m5, -2,-4,-6,-8 ) );
  m5 = (m1 - m2) - (m3 + m4);
  BOOST_CHECK( chkMatrixS2( m5, -2,-4,-6,-8 ) );
  m5 = (m1 - m2) - (m3 - m4);
  BOOST_CHECK( chkMatrixS2( m5, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m3, 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixS2( m4, 2,4,6,8 ) );

  m5 += (m1 + m2) + (m3 + m4);
  m5 += (m1 + m2) + (m3 - m4);
  m5 += (m1 + m2) - (m3 + m4);
  m5 += (m1 + m2) - (m3 - m4);
  m5 += (m1 - m2) + (m3 + m4);
  m5 += (m1 - m2) + (m3 - m4);
  m5 += (m1 - m2) - (m3 + m4);
  m5 += (m1 - m2) - (m3 - m4);
  BOOST_CHECK( chkMatrixS2( m5, 10,20,30,40 ) );
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m3, 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixS2( m4, 2,4,6,8 ) );

  m5 -= (m1 + m2) + (m3 + m4);
  m5 -= (m1 + m2) + (m3 - m4);
  m5 -= (m1 + m2) - (m3 + m4);
  m5 -= (m1 + m2) - (m3 - m4);
  m5 -= (m1 - m2) + (m3 + m4);
  m5 -= (m1 - m2) + (m3 - m4);
  m5 -= (m1 - m2) - (m3 + m4);
  m5 -= (m1 - m2) - (m3 - m4);
  BOOST_CHECK( chkMatrixS2( m5, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m3, 0,0,0,0 ) );
  BOOST_CHECK( chkMatrixS2( m4, 2,4,6,8 ) );

  m2 = 1*m1;
  m3 = m2*2;
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m3, 2,4,6,8 ) );

  m2 += 1*m1;
  m3 += m2*2;
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m2, 2,4,6,8 ) );
  BOOST_CHECK( chkMatrixS2( m3, 6,12,18,24 ) );

  m2 -= 1*m1;
  m3 -= m2*2;
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m3, 4,8,12,16 ) );

  m5 = 1*(m1 + m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, 14,28,42,56 ) );
  m5 = 1*(m1 + m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, 6,12,18,24 ) );
  m5 = 1*(m1 + m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, -10,-20,-30,-40 ) );
  m5 = 1*(m1 + m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, -2,-4,-6,-8 ) );
  m5 = 1*(m1 - m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, 12,24,36,48 ) );
  m5 = 1*(m1 - m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, 4,8,12,16 ) );
  m5 = 1*(m1 - m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, -12,-24,-36,-48 ) );
  m5 = 1*(m1 - m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, -4,-8,-12,-16 ) );
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m3, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixS2( m4, 2,4,6,8 ) );

  m5 += 1*(m1 + m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, 10,20,30,40 ) );
  m5 += 1*(m1 + m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, 16,32,48,64 ) );
  m5 += 1*(m1 + m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, 6,12,18,24 ) );
  m5 += 1*(m1 + m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, 4,8,12,16 ) );
  m5 += 1*(m1 - m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, 16,32,48,64 ) );
  m5 += 1*(m1 - m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, 20,40,60,80 ) );
  m5 += 1*(m1 - m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, 8,16,24,32 ) );
  m5 += 1*(m1 - m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m3, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixS2( m4, 2,4,6,8 ) );

  m5 -= 1*(m1 + m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, -10,-20,-30,-40 ) );
  m5 -= 1*(m1 + m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, -16,-32,-48,-64 ) );
  m5 -= 1*(m1 + m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, -6,-12,-18,-24 ) );
  m5 -= 1*(m1 + m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, -4,-8,-12,-16 ) );
  m5 -= 1*(m1 - m2) + (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, -16,-32,-48,-64 ) );
  m5 -= 1*(m1 - m2) + (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, -20,-40,-60,-80 ) );
  m5 -= 1*(m1 - m2) - (m3 + m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, -8,-16,-24,-32 ) );
  m5 -= 1*(m1 - m2) - (m3 - m4)*2;
  BOOST_CHECK( chkMatrixS2( m5, -4,-8,-12,-16 ) );
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m3, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixS2( m4, 2,4,6,8 ) );

  m5 = 1*(m1 + m2)*2;
  BOOST_CHECK( chkMatrixS2( m5, 4,8,12,16 ) );
  m5 = 1*2*(m1 + m2);
  BOOST_CHECK( chkMatrixS2( m5, 4,8,12,16 ) );
  m5 = (m1 + m2)*1*2;
  BOOST_CHECK( chkMatrixS2( m5, 4,8,12,16 ) );
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m2, 1,2,3,4 ) );

  m2 = +m1;
  BOOST_CHECK( chkMatrixS2( m2, 1,2,3,4 ) );
  m3 = -m2;
  BOOST_CHECK( chkMatrixS2( m3, -1,-2,-3,-4 ) );
  m4 = +(m1 + m2);
  BOOST_CHECK( chkMatrixS2( m4, 2,4,6,8 ) );
  m4 = +(m1 - m2);
  BOOST_CHECK( chkMatrixS2( m4, 0,0,0,0 ) );
  m4 = -(m1 + m2);
  BOOST_CHECK( chkMatrixS2( m4, -2,-4,-6,-8 ) );
  m4 = -(m1 - m2);
  BOOST_CHECK( chkMatrixS2( m4, 0,0,0,0 ) );
  m4 = +(m1 + m2) + m3;
  BOOST_CHECK( chkMatrixS2( m4, 1,2,3,4 ) );
  m4 = -(m1 + m2) + m3;
  BOOST_CHECK( chkMatrixS2( m4, -3,-6,-9,-12 ) );
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m2, 1,2,3,4 ) );
  BOOST_CHECK( chkMatrixS2( m3, -1,-2,-3,-4 ) );

  m4 = +1*m1;
  BOOST_CHECK( chkMatrixS2( m4, 1,2,3,4 ) );
  m4 = -1*m1;
  BOOST_CHECK( chkMatrixS2( m4, -1,-2,-3,-4 ) );
  m4 = +m1*1;
  BOOST_CHECK( chkMatrixS2( m4, 1,2,3,4 ) );
  m4 = -m1*1;
  BOOST_CHECK( chkMatrixS2( m4, -1,-2,-3,-4 ) );
  m4 = +(1*m1);
  BOOST_CHECK( chkMatrixS2( m4, 1,2,3,4 ) );
  m4 = -(1*m1);
  BOOST_CHECK( chkMatrixS2( m4, -1,-2,-3,-4 ) );
  m4 = +(m1*1);
  BOOST_CHECK( chkMatrixS2( m4, 1,2,3,4 ) );
  m4 = -(m1*1);
  BOOST_CHECK( chkMatrixS2( m4, -1,-2,-3,-4 ) );
  BOOST_CHECK( chkMatrixS2( m1, 1,2,3,4 ) );
}


//----------------------------------------------------------------------------//
// matrix-vector multiply
BOOST_AUTO_TEST_CASE( matrix_vector_multiply2 )
{
  const Int adata[2] = {1,2};
  const Int mdata[4] = {3,4,5,6};
  MatrixS2 m1(mdata, 4);
  MatrixS2 m2(m1), m3;
  VectorS2 a1(adata, 2);
  VectorS2 a2(a1), a3;

  BOOST_CHECK( chkVectorS2( a1, 1,2 ) );
  BOOST_CHECK( chkVectorS2( a2, 1,2 ) );

  BOOST_CHECK( chkMatrixS2( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS2( m2, 3,4,5,6 ) );

  a2 = m1*a1;
  BOOST_CHECK( chkVectorS2( a2, 11,17 ) );
  BOOST_CHECK( chkMatrixS2( m1, 3,4,5,6 ) );

//  a2 = a1*m1;
//  BOOST_CHECK( chkVectorS2( a2, 13,16 ) );
//  BOOST_CHECK( chkMatrixS2( m1, 3,4,5,6 ) );

  a2 = (m1 + m2)*a1;
  BOOST_CHECK( chkVectorS2( a2, 22,34 ) );
  BOOST_CHECK( chkMatrixS2( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS2( m2, 3,4,5,6 ) );

//  a2 = a1*(m1 + m2);
//  BOOST_CHECK( chkVectorS2( a2, 26,32 ) );
//  BOOST_CHECK( chkMatrixS2( m1, 3,4,5,6 ) );
//  BOOST_CHECK( chkMatrixS2( m2, 3,4,5,6 ) );

  a2 = (m1 - m2)*a1;
  BOOST_CHECK( chkVectorS2( a2, 0,0 ) );
  BOOST_CHECK( chkMatrixS2( m1, 3,4,5,6 ) );
  BOOST_CHECK( chkMatrixS2( m2, 3,4,5,6 ) );

//  a2 = a1*(m1 - m2);
//  BOOST_CHECK( chkVectorS2( a2, 0,0 ) );
//  BOOST_CHECK( chkMatrixS2( m1, 3,4,5,6 ) );
//  BOOST_CHECK( chkMatrixS2( m2, 3,4,5,6 ) );

  a2 = a1;

  a3 = (m1 + m2)*(a1 + a2);
  BOOST_CHECK( chkVectorS2( a3, 44,68 ) );

//  a3 = (a1 + a2)*(m1 + m2);
//  BOOST_CHECK( chkVectorS2( a3, 52,64 ) );

  a3 = (m1 + m2)*(a1 - a2);
  BOOST_CHECK( chkVectorS2( a3, 0,0 ) );

//  a3 = (a1 - a2)*(m1 + m2);
//  BOOST_CHECK( chkVectorS2( a3, 0,0 ) );

  a3 = (m1 - m2)*(a1 + a2);
  BOOST_CHECK( chkVectorS2( a3, 0,0 ) );

//  a3 = (a1 + a2)*(m1 - m2);
//  BOOST_CHECK( chkVectorS2( a3, 0,0 ) );

  a3 = (m1 - m2)*(a1 - a2);
  BOOST_CHECK( chkVectorS2( a3, 0,0 ) );

//  a3 = (a1 - a2)*(m1 - m2);
//  BOOST_CHECK( chkVectorS2( a3, 0,0 ) );
//  BOOST_CHECK( chkVectorS2( a1, 1,2 ) );
//  BOOST_CHECK( chkVectorS2( a2, 1,2 ) );
//  BOOST_CHECK( chkMatrixS2( m1, 3,4,5,6 ) );
//  BOOST_CHECK( chkMatrixS2( m2, 3,4,5,6 ) );

  m3 = m1*m2;
  BOOST_CHECK( chkMatrixS2( m3, 29,36,45,56 ) );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
