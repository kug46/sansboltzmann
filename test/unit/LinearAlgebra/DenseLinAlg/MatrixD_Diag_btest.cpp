// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Diag.h"

#include <iostream>
using namespace SANS::DLA;

namespace SANS
{
namespace DLA
{
  //Explicitly instantiate the class so that coverage information is correct
  template class MatrixDDiag<Real>;
  template class OpMulD< MatrixDDiag<Real>, MatrixDView<Real> >;
  template class OpMulD< MatrixDView<Real>, MatrixDDiag<Real> >;
}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_Diag_2x2_test )
{
  typedef Real T;

  VectorD<T> V = {1, 2};
  MatrixD<T> D(2,2);

  D = diag(V);

  BOOST_CHECK_EQUAL( D(0,0) , 1 );
  BOOST_CHECK_EQUAL( D(0,1) , 0 );
  BOOST_CHECK_EQUAL( D(1,0) , 0 );
  BOOST_CHECK_EQUAL( D(1,1) , 2 );

  D += diag(V);

  BOOST_CHECK_EQUAL( D(0,0) , 2*1 );
  BOOST_CHECK_EQUAL( D(0,1) ,   0 );
  BOOST_CHECK_EQUAL( D(1,0) ,   0 );
  BOOST_CHECK_EQUAL( D(1,1) , 2*2 );

  D -= diag(V);

  BOOST_CHECK_EQUAL( D(0,0) , 1 );
  BOOST_CHECK_EQUAL( D(0,1) , 0 );
  BOOST_CHECK_EQUAL( D(1,0) , 0 );
  BOOST_CHECK_EQUAL( D(1,1) , 2 );


  BOOST_CHECK_EQUAL( diag(V)(0,0) , 1 );
  BOOST_CHECK_EQUAL( diag(V)(0,1) , 0 );
  BOOST_CHECK_EQUAL( diag(V)(1,0) , 0 );
  BOOST_CHECK_EQUAL( diag(V)(1,1) , 2 );

  BOOST_CHECK_EQUAL( diag(V).diag(0) , 1 );
  BOOST_CHECK_EQUAL( diag(V).diag(1) , 2 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_Diag_right_mul_2x2_test )
{
  typedef Real T;

  VectorD<T> V = {2, 3};
  MatrixD<T> M = {{3, 4},
                  {6, 7}};
  MatrixD<T> M2(2,2);

  M2 = diag(V)*M;

  BOOST_CHECK_EQUAL( M2(0,0) , 6 );
  BOOST_CHECK_EQUAL( M2(0,1) , 8 );
  BOOST_CHECK_EQUAL( M2(1,0) , 18 );
  BOOST_CHECK_EQUAL( M2(1,1) , 21 );

  M2 += diag(V)*M;

  BOOST_CHECK_EQUAL( M2(0,0) , 2*6 );
  BOOST_CHECK_EQUAL( M2(0,1) , 2*8 );
  BOOST_CHECK_EQUAL( M2(1,0) , 2*18 );
  BOOST_CHECK_EQUAL( M2(1,1) , 2*21 );

  M2 -= diag(V)*M;

  BOOST_CHECK_EQUAL( M2(0,0) , 6 );
  BOOST_CHECK_EQUAL( M2(0,1) , 8 );
  BOOST_CHECK_EQUAL( M2(1,0) , 18 );
  BOOST_CHECK_EQUAL( M2(1,1) , 21 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_Diag_left_mul_2x2_test )
{
  typedef Real T;

  VectorD<T> V = {2, 3};
  MatrixD<T> M = {{3, 4},
                  {6, 7}};
  MatrixD<T> M2(2,2);

  M2 = M*diag(V);

  BOOST_CHECK_EQUAL( M2(0,0) , 6 );
  BOOST_CHECK_EQUAL( M2(0,1) , 12 );
  BOOST_CHECK_EQUAL( M2(1,0) , 12 );
  BOOST_CHECK_EQUAL( M2(1,1) , 21 );

  M2 += M*diag(V);

  BOOST_CHECK_EQUAL( M2(0,0) , 2*6 );
  BOOST_CHECK_EQUAL( M2(0,1) , 2*12 );
  BOOST_CHECK_EQUAL( M2(1,0) , 2*12 );
  BOOST_CHECK_EQUAL( M2(1,1) , 2*21 );

  M2 -= M*diag(V);

  BOOST_CHECK_EQUAL( M2(0,0) , 6 );
  BOOST_CHECK_EQUAL( M2(0,1) , 12 );
  BOOST_CHECK_EQUAL( M2(1,0) , 12 );
  BOOST_CHECK_EQUAL( M2(1,1) , 21 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_Diag_3x3_test )
{
  typedef Real T;

  VectorD<T> V = {1, 2, 3};
  MatrixD<T> D(3,3);

  D = diag(V);

  BOOST_CHECK_EQUAL( D(0,0) , 1 );
  BOOST_CHECK_EQUAL( D(1,0) , 0 );
  BOOST_CHECK_EQUAL( D(2,0) , 0 );
  BOOST_CHECK_EQUAL( D(0,1) , 0 );
  BOOST_CHECK_EQUAL( D(1,1) , 2 );
  BOOST_CHECK_EQUAL( D(2,1) , 0 );
  BOOST_CHECK_EQUAL( D(0,2) , 0 );
  BOOST_CHECK_EQUAL( D(1,2) , 0 );
  BOOST_CHECK_EQUAL( D(2,2) , 3 );

  D += diag(V);

  BOOST_CHECK_EQUAL( D(0,0) , 2*1 );
  BOOST_CHECK_EQUAL( D(1,0) ,   0 );
  BOOST_CHECK_EQUAL( D(2,0) ,   0 );
  BOOST_CHECK_EQUAL( D(0,1) ,   0 );
  BOOST_CHECK_EQUAL( D(1,1) , 2*2 );
  BOOST_CHECK_EQUAL( D(2,1) ,   0 );
  BOOST_CHECK_EQUAL( D(0,2) ,   0 );
  BOOST_CHECK_EQUAL( D(1,2) ,   0 );
  BOOST_CHECK_EQUAL( D(2,2) , 2*3 );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
