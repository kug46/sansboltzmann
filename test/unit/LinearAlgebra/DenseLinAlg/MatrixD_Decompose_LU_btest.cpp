// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Decompose_LU.h"
#include "LinearAlgebra/DenseLinAlg/tools/SingularException.h"

#include <iostream>
using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Decompose_LU_Exceptions )
{
  MatrixD<Real> A1(2,2), LU1(2,2);
  A1 = 0;
  BOOST_CHECK_THROW( Decompose_LU(A1, LU1), SingularMatrixException );

  MatrixD<Real> A2(3,2);
  A2 = Identity();
  BOOST_CHECK_THROW( Decompose_LU(A2, LU1), AssertionException );

  MatrixD<Real> A3(2,3);
  A3 = Identity();
  BOOST_CHECK_THROW( Decompose_LU(A2, LU1), AssertionException );

  MatrixD<Real> LU2(3,2);
  A1 = Identity();
  BOOST_CHECK_THROW( Decompose_LU(A1, LU2), AssertionException );

  MatrixD<Real> LU3(2,3);
  A1 = Identity();
  BOOST_CHECK_THROW( Decompose_LU(A1, LU3), AssertionException );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Decompose_LU_1x1 )
{
  typedef Real T;
  MatrixD<T> A(1, 1);
  MatrixD<T> LU(1, 1);

  A = 2;

  Decompose_LU(A, LU);

  BOOST_CHECK_CLOSE( A(0,0), T(2.), T(0.0001) );

  BOOST_CHECK_CLOSE( LU(0,0), T(2.), T(0.0001) );


  LU = 0;
  // cppcheck-suppress redundantAssignment
  LU = Decompose_LU(A);

  BOOST_CHECK_CLOSE( A(0,0), T(2.), T(0.0001) );

  BOOST_CHECK_CLOSE( LU(0,0), T(2.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Decompose_LU_2x2 )
{
  typedef Real T;

  Real Avals[] = {2, 1,
                  3, 2};

  MatrixD<T> A(2,2,Avals);
  MatrixD<T> LU(2,2);

  Decompose_LU(A, LU);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( LU(0,0),    2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1), 1./2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0),    3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), 1./2., T(0.0001) );


  LU = 0;
  // cppcheck-suppress redundantAssignment
  LU = Decompose_LU(A);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( LU(0,0),    2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1), 1./2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0),    3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), 1./2., T(0.0001) );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Decompose_LU_2x2_AExpression )
{
  typedef Real T;

  Real Avals[] = {1, 0,
                  2, 1};

  MatrixD<T> A1(2,2,Avals), A2(2,2);
  MatrixD<T> LU(2,2);

  A2 = 1;

  Decompose_LU(A1 + A2, LU);

  BOOST_CHECK_CLOSE( LU(0,0),    2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1), 1./2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0),    3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), 1./2., T(0.0001) );


  LU = 0;
  // cppcheck-suppress redundantAssignment
  LU = Decompose_LU(A1 + A2);

  BOOST_CHECK_CLOSE( LU(0,0),    2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1), 1./2., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0),    3., T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), 1./2., T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Matrix_Decompose_LU_3x3 )
{
  typedef Real T;

  Real Adata[] = {4, 2, 2,
                  2, 4, 6,
                  2, 6, 6};

  MatrixD<T> A(3,3, Adata);
  MatrixD<T> LU(3,3);

  Decompose_LU(A, LU);

  BOOST_CHECK_CLOSE( LU(0,0), T( 4.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1), T( 1./2.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,2), T( 1./2.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0), T( 2.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), T( 3.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,2), T( 5./3.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,0), T( 2.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,1), T( 5.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,2), T( -10./3. ), T(0.0001) );

  LU = 0;
  // cppcheck-suppress redundantAssignment
  LU = Decompose_LU(A);

  BOOST_CHECK_CLOSE( LU(0,0), T( 4.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,1), T( 1./2.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(0,2), T( 1./2.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,0), T( 2.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,1), T( 3.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(1,2), T( 5./3.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,0), T( 2.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,1), T( 5.      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LU(2,2), T( -10./3. ), T(0.0001) );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
