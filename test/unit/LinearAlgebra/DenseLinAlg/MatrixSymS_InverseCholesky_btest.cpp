// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/range_c.hpp>
#include <boost/preprocessor/repetition/repeat_from_to.hpp>

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/InverseCholesky.h"
#include "LinearAlgebra/DenseLinAlg/tools/SingularException.h"

#include <iostream>
using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( MatrixSymS_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_InverseCholesky_Exceptions )
{
  MatrixSymS<2, Real> A1, A1inv;
  A1 = 0;
  BOOST_CHECK_THROW( A1inv = InverseCholesky::Inverse(A1), SingularMatrixException );

  // not positive definite
  MatrixSymS<3,Real> A2 = {{1,},
                           {1., 2.},
                           {1., 2., 1.}};
  MatrixSymS<3,Real> A2inv;

  BOOST_CHECK_THROW( A2inv = InverseCholesky::Inverse(A2), AssertionException );
}

static const int CacheItems = CACHE_LINE_SIZE/sizeof(Real);
typedef boost::mpl::range_c<int, CacheItems/2, 2*CacheItems> MatrixSizes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MatrixSymS_InverseCholesky_Rand, N, MatrixSizes )
{
  typedef Real T;
  static const int n = N::value;

  MatrixSymS<n,T> A(0), tmp;
  MatrixS<n, n, T> I, Ainv;

  I = Identity();

  for (int i = 0; i < n; i++)
    for (int j = 0; j < i+1; j++)
      A(i,j) = T(rand() % 101/100.) + 5*I(i,j);

  tmp = A;

  Ainv = InverseCholesky::Inverse(A);

  I = Ainv*A;

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
    {
      BOOST_CHECK_CLOSE( A(i,j) , tmp(i,j), T(0.0001) );
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MatrixSymS_InverseCholesky_Rand_Expression, N, MatrixSizes )
{
  typedef Real T;
  static const int n = N::value;

  MatrixSymS<n,T> A(0), tmp, ISym;
  MatrixS<n, n, T> I, Ainv;

  I = Identity();
  ISym = Identity();

  for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++)
      A(i,j) = T(rand() % 101/100.);

  tmp = A + 5*ISym;

  Ainv = InverseCholesky::Inverse(A + 5*ISym);

  I = Ainv*tmp;

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_SolveLL_1x1 )
{
  typedef Real T;
  MatrixSymS<1,T> A = 2;
  MatrixS<1,1,T> b = 3;
  MatrixS<1,1,T> x;

  x = InverseCholesky::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), T(2.)   , T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(3./2.), T(0.0001) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_SolveLL_2x2 )
{
  typedef Real T;

  MatrixSymS<2,T> A = {{2},
                       {1, 3}};
  MatrixS<2,1,T> b = {{2},
                      {3}};
  MatrixS<2,1,T> x;

  x = InverseCholesky::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 3., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(4./5.), T(0.0001) );

  MatrixS<2,2,T> B = {{3, 4},
                      {4, 5}};

  MatrixS<2,2,T> X = InverseCholesky::Solve(A, B);

  BOOST_CHECK_CLOSE( X(0,0), T(1.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( X(0,1), T(7./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,0), T(1.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,1), T(6./5.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_SolveLL_2x1_2b )
{
  typedef Real T;

  MatrixSymS<2,T> A = {{2},
                       {1, 3}};
  MatrixS<2,1,T> b = {{2},
                      {3}};

  b = InverseCholesky::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 3., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(4./5.), T(0.0001) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_SolveLL_2x2_RHSExpression )
{
  typedef Real T;

  MatrixSymS<2,T> A = {{2},
                       {1, 3}};
  MatrixS<2,1,T> b1 = {{2},
                       {3}};
  MatrixS<2,1,T> b2 = 2;
  MatrixS<2,1,T> x;

  x = InverseCholesky::Solve(A, b1 + b2);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 3., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(7./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(6./5.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_SolveLL_2x2_InvExpression )
{
  typedef Real T;

  MatrixSymS<2,T> A1 = {{1},
                        {0, 2}};
  MatrixSymS<2,T> A2 = 1;
  MatrixS<2,1,T> b = {{2},
                      {3}};
  MatrixS<2,1,T> x;

  x = InverseCholesky::Solve(A1 + A2, b);

  BOOST_CHECK_CLOSE( x(0,0), T(3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(4./5.), T(0.0001) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_SolveLL_2x2_InvExpression_2b )
{
  typedef Real T;

  MatrixSymS<2,T> A1 = {{1},
                        {0, 2}};
  MatrixSymS<2,T> A2 = 1;
  MatrixS<2,1,T> b = {{2},
                      {3}};

  b = InverseCholesky::Solve(A1 + A2, b);

  BOOST_CHECK_CLOSE( b(0,0), T(3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(4./5.), T(0.0001) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_SolveLL_2x2_Expressions )
{
  typedef Real T;

  MatrixSymS<2,T> A1 = {{1},
                        {0, 2}};
  MatrixSymS<2,T> A2 = 1;
  MatrixS<2,1,T> b1 = {{2},
                       {3}};
  MatrixS<2,1,T> b2 = 2;
  MatrixS<2,1,T> x;

  x = InverseCholesky::Solve(A1 + A2, b1 + b2);

  BOOST_CHECK_CLOSE( x(0,0), T(7./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(6./5.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_SolveLL_2x2_Expressions_2 )
{
  typedef Real T;

  MatrixSymS<2,T> A1 = {{1},
                        {0, 2}};
  MatrixSymS<2,T> A2 = 1;
  MatrixS<2,1,T> b1 = {{2},
                       {3}};
  MatrixS<2,1,T> b2 = 2;
  MatrixS<2,1,T> x;

  x = InverseCholesky::Solve(A1 + A2, b1 + b2) + b2;

  BOOST_CHECK_CLOSE( x(0,0), T(7./5. + 2), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(6./5. + 2), T(0.0001) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_SolveLL_2x1_MulScal )
{
  typedef Real T;

  MatrixSymS<2,T> A = {{2},
                       {1, 3}};
  MatrixS<2,1,T> b = {{2},
                      {3}};

  MatrixS<2,1,T> x;

  x = 0;
  x = 2*InverseCholesky::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 3., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2*3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(2*4./5.), T(0.0001) );


  x = 0; // cppcheck-suppress redundantAssignment
  x = InverseCholesky::Solve(A, b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 3., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(3./5.*2), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(4./5.*2), T(0.0001) );


  x = 0; // cppcheck-suppress redundantAssignment
  x = -InverseCholesky::Solve(A, b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 3., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(-4./5.), T(0.0001) );


  x = 0; // cppcheck-suppress redundantAssignment
  x = -InverseCholesky::Solve(A, b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 3., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-2*3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(-2*4./5.), T(0.0001) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = -(2*InverseCholesky::Solve(A, b)) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 3., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-2*3./5.*2), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(-2*4./5.*2), T(0.0001) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_SolveLL_3x3 )
{
  typedef Real T;

  MatrixSymS<3,T> A = {{25,},
                       {15, 18},
                       {-5,  0, 11}};

  MatrixS<3,1,T> b = {{1},
                      {3},
                      {2}};

  MatrixS<3,1,T> x = InverseCholesky::Solve(A, b);

  BOOST_CHECK_CLOSE( x(0,0), T(-13./225.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T( 29./135.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(2,0), T(  7./45. ), T(0.0001) );


  MatrixS<3,3,T> B = {{1, 2, 1},
                      {2, 3, 1},
                      {1, 1, 2}};

  MatrixS<3,3,T> X = InverseCholesky::Solve(A, B);

  BOOST_CHECK_CLOSE( X(0,0), T(-14./675.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(0,1), T( -1./225.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(0,2), T( 71./675.), T(0.0001) );

  BOOST_CHECK_CLOSE( X(1,0), T( 52./405.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,1), T( 23./135.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,2), T(-13./405.), T(0.0001) );

  BOOST_CHECK_CLOSE( X(2,0), T( 11./135.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,1), T(  4./45. ), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,2), T( 31./135.), T(0.0001) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_SolveLL_2x2_NonSymVec )
{
  typedef Real T;

  MatrixSymS<2,T> A = {{2},
                       {1, 3}};
  MatrixS<2,3,T> b = {{2, 4, 5},
                      {3, 1, 2}};

  MatrixS<2,3,T> x = InverseCholesky::Solve(A, b);

  BOOST_CHECK_CLOSE( x(0,0), T( 3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,1), T(11./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,2), T(13./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T( 4./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,1), T(-2./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,2), T(-1./5.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MatrixSymS_InverseCholesky_Rand_Factorize, N, MatrixSizes )
{
  typedef Real T;
  static const int n = N::value;

  MatrixSymS<n,T> A(0), tmp;
  MatrixS<n,n,T> I, Ainv;

  I = Identity();

  for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++)
      A(i,j) = T(rand() % 101/100.) + 5*I(i,j);

  tmp = A;

  auto LL = InverseCholesky::Factorize(A);
  Ainv = LL.backsolve(I);

  I = Ainv*A;

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
    {
      BOOST_CHECK_CLOSE( A(i,j) , tmp(i,j), T(0.0001) );
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }

  I = Identity();

  // Solve the system a 2nd time and make sure it still works
  Ainv = 0;  // cppcheck-suppress redundantAssignment
  Ainv = LL.backsolve(I);

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
    {
      BOOST_CHECK_CLOSE( A(i,j) , tmp(i,j), T(0.0001) );
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MatrixSymS_InverseCholesky_Rand_Expression_Factorize, N, MatrixSizes )
{
  typedef Real T;
  static const int n = N::value;

  MatrixSymS<n,T> A(0), tmp, ISym;
  MatrixS<n,n,T> I, Ainv;

  I = Identity();
  ISym = Identity();

  for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++)
      A(i,j) = T(rand() % 101/100.);

  tmp = A + 5*ISym;

  auto LL = InverseCholesky::Factorize(A + 5*ISym);
  Ainv = LL.backsolve(I);

  I = Ainv*tmp;

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }

  I = Identity();

  // Solve the system a 2nd time and make sure it still works
  Ainv = 0;  // cppcheck-suppress redundantAssignment
  Ainv = LL.backsolve(I);

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }

  I = Identity();

  // Solve the system a 3rd time and make sure it still works
  Ainv = 0;  // cppcheck-suppress redundantAssignment
  Ainv = LL.backsolve(I - I*I + I);

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( I(i,j) , T(1), T(0.0001) );
      else
        BOOST_CHECK_SMALL( I(i,j) , T(1e-12) );
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_FactorizeLL_1x1 )
{
  typedef Real T;
  MatrixSymS<1,T> A = 2;
  MatrixS<1,1,T> b = 3;
  MatrixS<1,1,T> x;

  auto LL = InverseCholesky::Factorize(A);
  x = LL.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), T(2.)   , T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(3./2.), T(0.0001) );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_FactorizeLL_2x2 )
{
  typedef Real T;

  MatrixSymS<2,T> A = {{2},
                       {1, 3}};
  MatrixS<2,1,T> b = {{2},
                      {3}};
  MatrixS<2,1,T> x;

  auto LL = InverseCholesky::Factorize(A);
  x = LL.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 3., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(4./5.), T(0.0001) );


  MatrixS<2,2,T> B = {{3, 4},
                      {4, 5}};

  MatrixS<2,2,T> X = LL.backsolve(B);

  BOOST_CHECK_CLOSE( X(0,0), T(1.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( X(0,1), T(7./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,0), T(1.   ), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,1), T(6./5.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_FactorizeLL_2x1_2b )
{
  //Check that b = LL.backsolve(b) works properly
  typedef Real T;

  MatrixSymS<2,T> A = {{2},
                       {1, 3}};
  MatrixS<2,1,T> b = {{2},
                      {3}};

  auto LL = InverseCholesky::Factorize(A);
  b = LL.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 3., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(4./5.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_FactorizeLL_2x2_RHSExpression )
{
  typedef Real T;

  MatrixSymS<2,T> A = {{2},
                       {1, 3}};
  MatrixS<2,1,T> b1 = {{2},
                       {3}};
  MatrixS<2,1,T> b2 = 2;
  MatrixS<2,1,T> x;

  auto LL = InverseCholesky::Factorize(A);
  x = LL.backsolve( b1 + b2 );

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 3., T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(7./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(6./5.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_FactorizeLL_2x2_InvExpression )
{
  typedef Real T;

  MatrixSymS<2,T> A1 = {{1},
                        {0, 2}};
  MatrixSymS<2,T> A2 = 1;
  MatrixS<2,1,T> b = {{2},
                      {3}};
  MatrixS<2,1,T> x;

  auto LL = InverseCholesky::Factorize(A1 + A2);
  x = LL.backsolve(b);

  BOOST_CHECK_CLOSE( x(0,0), T(3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(4./5.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_FactorizeLL_2x2_InvExpression_2b )
{
  typedef Real T;

  MatrixSymS<2,T> A1 = {{1},
                        {0, 2}};
  MatrixSymS<2,T> A2 = 1;
  MatrixS<2,1,T> b = {{2},
                      {3}};

  auto LL = InverseCholesky::Factorize(A1 + A2);
  b = LL.backsolve(b);

  BOOST_CHECK_CLOSE( b(0,0), T(3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(4./5.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_FactorizeLL_2x2_Expressions )
{
  typedef Real T;

  MatrixSymS<2,T> A1 = {{1},
                        {0, 2}};
  MatrixSymS<2,T> A2 = 1;
  MatrixS<2,1,T> b1 = {{2},
                       {3}};
  MatrixS<2,1,T> b2 = 2;
  MatrixS<2,1,T> x;

  auto LL = InverseCholesky::Factorize(A1 + A2);
  x = LL.backsolve(b1 + b2);

  BOOST_CHECK_CLOSE( x(0,0), T(7./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(6./5.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_FactorizeLL_2x2_Expressions_2 )
{
  typedef Real T;

  MatrixSymS<2,T> A1 = {{1},
                        {0, 2}};
  MatrixSymS<2,T> A2 = 1;
  MatrixS<2,1,T> b1 = {{2},
                       {3}};
  MatrixS<2,1,T> b2 = 2;
  MatrixS<2,1,T> x;

  auto LL = InverseCholesky::Factorize(A1 + A2);
  x = LL.backsolve(b1 + b2) + b2;

  BOOST_CHECK_CLOSE( x(0,0), T(7./5. + 2), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(6./5. + 2), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_FactorizeLL_2x1_MulScal )
{
  typedef Real T;

  MatrixSymS<2,T> A = {{2},
                       {1, 3}};
  MatrixS<2,1,T> b = {{2},
                      {3}};

  MatrixS<2,1,T> x;

  auto LL = InverseCholesky::Factorize(A);

  x = 0; // cppcheck-suppress redundantAssignment
  x = 2*LL.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 3., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(2*3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(2*4./5.), T(0.0001) );


  x = 0; // cppcheck-suppress redundantAssignment
  x = LL.backsolve(b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 3., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(3./5.*2), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(4./5.*2), T(0.0001) );


  x = 0; // cppcheck-suppress redundantAssignment
  x = -LL.backsolve(b);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 3., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(-4./5.), T(0.0001) );


  x = 0; // cppcheck-suppress redundantAssignment
  x = -LL.backsolve(b) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 3., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-2*3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(-2*4./5.), T(0.0001) );

  x = 0; // cppcheck-suppress redundantAssignment
  x = - (2 * LL.backsolve(b)) * 2;

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 3., T(0.0001) );

  BOOST_CHECK_CLOSE( b(0,0), T(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( b(1,0), T(3.), T(0.0001) );

  BOOST_CHECK_CLOSE( x(0,0), T(-2*3./5.*2), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T(-2*4./5.*2), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_FactorizeLL_3x3 )
{
  typedef Real T;

  MatrixSymS<3,T> A = {{25,},
                       {15, 18},
                       {-5,  0, 11}};

  MatrixS<3,1,T> b = {{1},
                      {3},
                      {2}};

  auto LL = InverseCholesky::Factorize(A);

  MatrixS<3,1,T> x = LL.backsolve(b);

  BOOST_CHECK_CLOSE( x(0,0), T(-13./225.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T( 29./135.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(2,0), T(  7./45. ), T(0.0001) );


  MatrixS<3,3,T> B = {{1, 2, 1},
                      {2, 3, 1},
                      {1, 1, 2}};

  MatrixS<3,3,T> X = LL.backsolve(B);

  BOOST_CHECK_CLOSE( X(0,0), T(-14./675.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(0,1), T( -1./225.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(0,2), T( 71./675.), T(0.0001) );

  BOOST_CHECK_CLOSE( X(1,0), T( 52./405.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,1), T( 23./135.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(1,2), T(-13./405.), T(0.0001) );

  BOOST_CHECK_CLOSE( X(2,0), T( 11./135.), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,1), T(  4./45. ), T(0.0001) );
  BOOST_CHECK_CLOSE( X(2,2), T( 31./135.), T(0.0001) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_FactorizeLL_2x2_NonSymVec )
{
  typedef Real T;

  MatrixSymS<2,T> A = {{2},
                       {1, 3}};
  MatrixS<2,3,T> b = {{2, 4, 5},
                      {3, 1, 2}};

  auto LL = InverseCholesky::Factorize(A);
  MatrixS<2,3,T> x = LL.backsolve(b);

  BOOST_CHECK_CLOSE( x(0,0), T( 3./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,1), T(11./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(0,2), T(13./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,0), T( 4./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,1), T(-2./5.), T(0.0001) );
  BOOST_CHECK_CLOSE( x(1,2), T(-1./5.), T(0.0001) );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
