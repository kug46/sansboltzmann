// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "LinearAlgebra/DenseLinAlg/tools/cross.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
namespace DLA
{
}
}

using namespace SANS;
using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( VectorS_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_cross_2 )
{
  typedef VectorS<2,Real> VectorS2;
  VectorS2 a = {1,0};
  VectorS2 b = {0,1};

  Real c = cross(a,b);

  BOOST_CHECK_EQUAL(c, 1.);

  a = {1.1,0.2};
  b = {2.0,1.5};

  c = cross(a,b);

  BOOST_CHECK_CLOSE(c, a[0]*b[1] - a[1]*b[0], 1e-12);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_cross_2_Surreal )
{
  typedef VectorS<2, SurrealS<4> > VectorS2;
  VectorS2 a = {1,0};
  VectorS2 b = {0,1};

  SurrealS<4> c = cross(a,b);

  BOOST_CHECK_EQUAL(c.value(), 1.);

  a = {1.1,0.2};
  b = {2.0,1.5};

  c = cross(a,b);

  BOOST_CHECK_CLOSE(c.value(), (a[0]*b[1] - a[1]*b[0]).value(), 1e-12);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_cross_3 )
{
  typedef VectorS<3,Real> VectorS3;
  VectorS3 a(0), b(0), c(0);

  a = {1,0,0};
  b = {0,1,0};
  c = cross(a,b);

  BOOST_CHECK_EQUAL(c[0], 0.);
  BOOST_CHECK_EQUAL(c[1], 0.);
  BOOST_CHECK_EQUAL(c[2], 1.);

  a = {0,0,1};
  b = {1,0,0};
  c = cross(a,b);

  BOOST_CHECK_EQUAL(c[0], 0.);
  BOOST_CHECK_EQUAL(c[1], 1.);
  BOOST_CHECK_EQUAL(c[2], 0.);

  a = {0,1,0};
  b = {0,0,1};
  c = cross(a,b);

  BOOST_CHECK_EQUAL(c[0], 1.);
  BOOST_CHECK_EQUAL(c[1], 0.);
  BOOST_CHECK_EQUAL(c[2], 0.);

  a = {0,1,0};
  b = {1,0,0};
  c = cross(a,b);

  BOOST_CHECK_EQUAL(c[0],  0.);
  BOOST_CHECK_EQUAL(c[1],  0.);
  BOOST_CHECK_EQUAL(c[2], -1.);

  a = {1,0,0};
  b = {0,0,1};
  c = cross(a,b);

  BOOST_CHECK_EQUAL(c[0],  0.);
  BOOST_CHECK_EQUAL(c[1], -1.);
  BOOST_CHECK_EQUAL(c[2],  0.);

  a = {0,0,1};
  b = {0,1,0};
  c = cross(a,b);

  BOOST_CHECK_EQUAL(c[0], -1.);
  BOOST_CHECK_EQUAL(c[1],  0.);
  BOOST_CHECK_EQUAL(c[2],  0.);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorS_cross_3_Surreal )
{
  typedef VectorS<3, SurrealS<4> > VectorS3;
  VectorS3 a, b, c;

  a = {1,0,0};
  b = {0,1,0};
  c = cross(a,b);

  BOOST_CHECK_EQUAL(c[0].value(), 0.);
  BOOST_CHECK_EQUAL(c[1].value(), 0.);
  BOOST_CHECK_EQUAL(c[2].value(), 1.);

  a = {0,0,1};
  b = {1,0,0};
  c = cross(a,b);

  BOOST_CHECK_EQUAL(c[0].value(), 0.);
  BOOST_CHECK_EQUAL(c[1].value(), 1.);
  BOOST_CHECK_EQUAL(c[2].value(), 0.);

  a = {0,1,0};
  b = {0,0,1};
  c = cross(a,b);

  BOOST_CHECK_EQUAL(c[0].value(), 1.);
  BOOST_CHECK_EQUAL(c[1].value(), 0.);
  BOOST_CHECK_EQUAL(c[2].value(), 0.);

  a = {0,1,0};
  b = {1,0,0};
  c = cross(a,b);

  BOOST_CHECK_EQUAL(c[0].value(),  0.);
  BOOST_CHECK_EQUAL(c[1].value(),  0.);
  BOOST_CHECK_EQUAL(c[2].value(), -1.);

  a = {1,0,0};
  b = {0,0,1};
  c = cross(a,b);

  BOOST_CHECK_EQUAL(c[0].value(),  0.);
  BOOST_CHECK_EQUAL(c[1].value(), -1.);
  BOOST_CHECK_EQUAL(c[2].value(),  0.);

  a = {0,0,1};
  b = {0,1,0};
  c = cross(a,b);

  BOOST_CHECK_EQUAL(c[0].value(), -1.);
  BOOST_CHECK_EQUAL(c[1].value(),  0.);
  BOOST_CHECK_EQUAL(c[2].value(),  0.);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
