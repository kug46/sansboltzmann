// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS_Decompose_Cholesky.h"
#include "LinearAlgebra/DenseLinAlg/tools/SingularException.h"

#include <iostream>
using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( DenseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_Decompose_Cholesky_Exceptions )
{
  MatrixSymS<2,Real> A1, LL1;
  A1 = 0;
  BOOST_CHECK_THROW( Decompose_Cholesky(A1, LL1), SingularMatrixException );

  // not positive definite
  MatrixSymS<3,Real> A2 = {{1,},
                           {1., 2.},
                           {1., 2., 1.}};
  MatrixSymS<3,Real> LL2;

  BOOST_CHECK_THROW( Decompose_Cholesky(A2, LL2), AssertionException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_Decompose_Cholesky_1x1 )
{
  typedef Real T;
  MatrixSymS<1,T> A;
  MatrixSymS<1,T> LL;

  A = 2;

  Decompose_Cholesky(A, LL);

  BOOST_CHECK_CLOSE( A(0,0), T(2.), T(0.0001) );

  BOOST_CHECK_CLOSE( LL(0,0), T(sqrt(2.)), T(0.0001) );

#if 0
  //Set to zero so we know it gets filled properly
  LL = 0;

  // cppcheck-suppress redundantAssignment
  LL = Decompose_Cholesky(A);

  BOOST_CHECK_CLOSE( A(0,0), T(2.), T(0.0001) );

  BOOST_CHECK_CLOSE( LL(0,0), T(2.), T(0.0001) );
#endif
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_Decompose_Cholesky_2x2 )
{
  typedef Real T;

  MatrixSymS<2,T> A = {{2},
                       {1, 3}};
  MatrixSymS<2,T> LL;

  Decompose_Cholesky(A, LL);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 3., T(0.0001) );

  BOOST_CHECK_CLOSE( LL(0,0),    sqrt(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(0,1), 1./sqrt(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(1,0), 1./sqrt(2.), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(1,1), sqrt(5./2.), T(0.0001) );

#if 0
  LL = 0;

  // cppcheck-suppress redundantAssignment
  LL = Decompose_Cholesky(A);

  BOOST_CHECK_CLOSE( A(0,0), 2., T(0.0001) );
  BOOST_CHECK_CLOSE( A(0,1), 1., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,0), 3., T(0.0001) );
  BOOST_CHECK_CLOSE( A(1,1), 2., T(0.0001) );

  BOOST_CHECK_CLOSE( LL(0,0),    2., T(0.0001) );
  BOOST_CHECK_CLOSE( LL(0,1), 1./2., T(0.0001) );
  BOOST_CHECK_CLOSE( LL(1,0),    3., T(0.0001) );
  BOOST_CHECK_CLOSE( LL(1,1), 1./2., T(0.0001) );
#endif
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_Decompose_Cholesky_2x2_AExpression )
{
  typedef Real T;

  MatrixSymS<2,T> A1 = {{2},
                        {1, 3}};
  MatrixSymS<2,T> LL, A2;

  A2 = 1;

  Decompose_Cholesky(A1 + A2, LL);

  BOOST_CHECK_CLOSE( LL(0,0),    sqrt(3.), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(0,1), 2./sqrt(3.), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(1,0), 2./sqrt(3.), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(1,1), sqrt(2./3.), T(0.0001) );


  LL = 0;

  // cppcheck-suppress redundantAssignment
  LL = Decompose_Cholesky(A1 + A2);

  BOOST_CHECK_CLOSE( LL(0,0),    2., T(0.0001) );
  BOOST_CHECK_CLOSE( LL(0,1), 1./2., T(0.0001) );
  BOOST_CHECK_CLOSE( LL(1,0),    3., T(0.0001) );
  BOOST_CHECK_CLOSE( LL(1,1), 1./2., T(0.0001) );

}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_Decompose_Cholesky_3x3 )
{
  typedef Real T;

  MatrixSymS<3,T> A = {{25,},
                       {15, 18},
                       {-5,  0, 11}};
  MatrixSymS<3,T> LL;

  Decompose_Cholesky(A, LL);

  BOOST_CHECK_CLOSE( LL(0,0), T(  5. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(0,1), T(  3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(0,2), T( -1. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(1,0), T(  3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(1,1), T(  3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(1,2), T(  1. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(2,0), T( -1. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(2,1), T(  1. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(2,2), T(  3. ), T(0.0001) );

#if 0
  LL = 0;

  // cppcheck-suppress redundantAssignment
  LL = Decompose_Cholesky(A);

  BOOST_CHECK_CLOSE( LL(0,0), T(  5. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(0,1), T(  3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(0,2), T( -1. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(1,0), T(  3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(1,1), T(  3. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(1,2), T(  1. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(2,0), T( -1. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(2,1), T(  1. ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(2,2), T(  3. ), T(0.0001) );
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixSymS_Decompose_Cholesky_4x4 )
{
  typedef Real T;

  MatrixSymS<4,T> A = {{18},
                       {22, 70},
                       {54, 86, 174},
                       {42, 62, 134, 106}};
  MatrixSymS<4,T> LL;

  Decompose_Cholesky(A, LL);

  BOOST_CHECK_CLOSE( LL(0,0), T(  3*sqrt(2.)   ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(0,1), T( 11*sqrt(2.)/3 ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(0,2), T(  9*sqrt(2.)   ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(0,3), T(  7*sqrt(2.)   ), T(0.0001) );

  BOOST_CHECK_CLOSE( LL(1,0), T( 11*sqrt(2.)/3  ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(1,1), T(  2*sqrt(97.)/3 ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(1,2), T( 30/sqrt(97.)   ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(1,3), T( 16/sqrt(97.)   ), T(0.0001) );

  BOOST_CHECK_CLOSE( LL(2,0), T(  9*sqrt(2.)       ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(2,1), T( 30/sqrt(97.)      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(2,2), T(  2*sqrt(66./97.)  ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(2,3), T( 74*sqrt(2./3201.) ), T(0.0001) );

  BOOST_CHECK_CLOSE( LL(3,0), T(  7*sqrt(2.)       ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(3,1), T( 16/sqrt(97.)      ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(3,2), T( 74*sqrt(2./3201.) ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(3,3), T(  8/sqrt(33)       ), T(0.0001) );

#if 0
  LL = 0;

  // cppcheck-suppress redundantAssignment
  LL = Decompose_Cholesky(A);

  BOOST_CHECK_CLOSE( LL(0,0), T(  3*sqrt(2.) ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(0,1), T(  (11*sqrt(2.))/3 ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(0,2), T( 9*sqrt(2.) ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(0,3), T( 7*sqrt(2.) ), T(0.0001) );

  BOOST_CHECK_CLOSE( LL(1,0), T(  (11*sqrt(2.))/3 ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(1,1), T(  (2*sqrt(97.))/3 ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(1,2), T( 30/sqrt(97.) ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(1,3), T( 16/sqrt(97.) ), T(0.0001) );

  BOOST_CHECK_CLOSE( LL(2,0), T( 9*sqrt(2.) ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(2,1), T( 30/sqrt(97.) ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(2,2), T( 2*sqrt(66./97.) ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(2,3), T( 74*sqrt(2./3201.) ), T(0.0001) );

  BOOST_CHECK_CLOSE( LL(3,0), T( 7*sqrt(2.) ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(3,1), T( 16/sqrt(97.) ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(3,2), T( 74*sqrt(2./3201.) ), T(0.0001) );
  BOOST_CHECK_CLOSE( LL(3,3), T( 8/sqrt(33) ), T(0.0001) );
#endif
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
