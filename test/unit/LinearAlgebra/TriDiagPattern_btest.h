// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef TRIDIAGPATTERN_H
#define TRIDIAGPATTERN_H

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Type.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"

namespace SANS
{
namespace SLA
{

// Parallel matrix partitioned across rows with ghost entries
template<class TM>
void TriDiagPattern(int rstart, int rend, const int nzs, const int nze, const int N, SparseNonZeroPattern<TM>& nz)
{
  const int m = std::min(nz.m(), rend - rstart);
  const int n = std::min(nz.n(), rend - rstart);

  // nothing to do for partition outside the size of the matrix
  if (n == 0) return;

  //SANS_ASSERT_MSG( m <= nz.m(), "%d > %d", m, nz.m() );
  //SANS_ASSERT_MSG( n <= nz.n(), "%d > %d", n, nz.n() );

  int nghost = 0;
  if ( rstart != 0 && rstart > nzs && rstart < nze)
  {
    SANS_ASSERT_MSG( nz.m()+nghost < nz.n(), "rs=%d, re=%d, N=%d, nz.m()=%d, nghost=%d, nz.n()=%d", rstart, rend, N, nz.m(), nghost, nz.n() );

    nz.add(0,nz.m()+nghost); // ghost index (ghost DOF are always last)
    nghost++;
  }

  if ( rend != N && rend > nzs && rend < nze)
  {
    SANS_ASSERT_MSG( nz.m()+nghost == nz.n()-1, "rs=%d, re=%d, N=%d, nz.m()=%d, nghost=%d, nz.n()=%d", rstart, rend, N, nz.m(), nghost, nz.n() );

    nz.add(nz.m()-1,nz.m()+nghost); // ghost index  (ghost DOF are always last)
  }

  for ( int i = 0; i < m; i++ )
    for ( int j = i-1; j < i+2; j++ )
      if ( j >= 0 && j < n )
        nz.add(i,j);
}


template<class TM>
void TriDiagPattern(SparseNonZeroPattern<TM>& nz, const int m_block = 0)
{
  const int m = nz.m();
  const int n = nz.n();

  //All the entries even if the matrix isn't square
  for ( int i = 0; i < m; i++ )
    for ( int j = i-1; j < i+2; j++ )
      if ( j >= 0 && j < std::min(m,n) )
        nz.add(i,j);
}


template<class TM>
void TriDiagPattern( SparseNonZeroPattern< DLA::MatrixD<TM> >& nz, const int m_block )
{
  const int m = nz.m();
  const int n = nz.n();

  //All the entries even if the matrix isn't square
  for ( int i = 0; i < m; i++ )
    for ( int j = i-1; j < i+2; j++ )
      if ( j >= 0 && j < n )
        nz.add(i,j,m_block,m_block);
}

} //namespace SLA
} //namespace SANS

#endif //TRIDIAGPATTERN_H
