// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include "tools/SANSnumerics.h"

#include "../TriDiagPattern_btest.h"
#include "../Heat1D_btest.h"

#include "LinearAlgebra/SparseLinAlg/Krylov/FGMRES.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"

using namespace SANS;
using namespace SANS::SLA;


//############################################################################//
BOOST_AUTO_TEST_SUITE( SparseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FGMRES_ctor )
{
  PyDict d, Precond;

  Precond[FGMRESParam::params.Preconditioner.Name] = FGMRESParam::params.Preconditioner.Identity;

  d[FGMRESParam::params.nInner] = 10;
  d[FGMRESParam::params.nOuter] = 3;
  d[FGMRESParam::params.tol] = 1e-10;
  d[FGMRESParam::params.PrintCovergence] = true;
  d[FGMRESParam::params.Preconditioner] = Precond;

  FGMRESParam::checkInputs(d);

  typedef SparseMatrix_CRS<Real> Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  Vector_type q(5);
  Heat1DEquationSet<Matrix_type> f(q);

  //This returns a shared pointer so no memory is lost
  FGMRESParam::newSolver< SparseMatrix_CRS<Real> >(d, f);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FGMRES_Solve )
{
  PyDict d, Precond;

  Precond[FGMRESParam::params.Preconditioner.Name] = FGMRESParam::params.Preconditioner.Identity;

  d[FGMRESParam::params.nInner] = 10;
  d[FGMRESParam::params.nOuter] = 3;
  d[FGMRESParam::params.tol] = 1e-10;
  d[FGMRESParam::params.PrintCovergence] = false;
  d[FGMRESParam::params.Preconditioner] = Precond;

  FGMRESParam::checkInputs(d);

  typedef SparseMatrix_CRS<Real> Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;
  typedef FGMRES<Matrix_type>::Solver_ptr Solver_ptr;

  Vector_type q(5);
  Heat1DEquationSet<Matrix_type> f(q);

  FGMRES<Matrix_type> Solver(d, Solver_ptr( new Identity<Matrix_type>(f) ) );
  FGMRES<Matrix_type> Solver2(d, Solver_ptr( new Identity<Matrix_type>(f) ) );

  Vector_type x(5), b(5), b2(5);

  //Create a residual vector
  b[0] = 0.5;
  b[1] = 1;
  b[2] = 2;
  b[3] = 1;
  b[4] = 0.5;

  //Solve the linear system. x here is the initial guess
  x = 1;
  Solver.solve(b, x);

  //Compute the residual vector from the solution
  b2 = Solver.A()*x;

  //The residuals should now be the same!
  for (int i = 0; i < b.m(); i++)
    BOOST_CHECK_CLOSE( b[i], b2[i], 1e-8 );

  //Temporary output just to check what is going on
  //for (int i = 0; i < b.size(); i++)
  //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;

  //Set the matrix for the solver
  Solver2.factorize();

  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    //Solve the linear system. x here is the initial guess
    x = 1;
    Solver2.backsolve(b, x);

    //Compute the residual vector from the solution
    b2 = Solver2.A()*x;

    //The residuals should now be the same!
    for (int i = 0; i < b.m(); i++)
      BOOST_CHECK_CLOSE( b[i], b2[i], 1e-8 );
  }
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
