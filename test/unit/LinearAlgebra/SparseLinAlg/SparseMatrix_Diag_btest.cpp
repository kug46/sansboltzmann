// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include "tools/SANSnumerics.h"
#include "tools/minmax.h"

#include "../TriDiagPattern_btest.h"
#include "../Heat1D_btest.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_Diag.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

using namespace SANS::SLA;
using namespace SANS;

//Explicitly instantiate templates to get correct coverage information
namespace SANS
{
namespace SLA
{
template class SparseMatrix_Diag<Real>;
template class SparseMatrix_Diag< DLA::MatrixD<Real> >;
template class SparseMatrix_Diag< DLA::MatrixS<2,2,Real> >;
template class SparseMatrix_Diag< DLA::MatrixD< DLA::MatrixS<2,2,Real> > >;
}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( SparseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_Diag_Real )
{
  const int m = 5;
  typedef Real Block_type;
  typedef SparseMatrix_CRS<Block_type> Matrix_type;
  typedef SparseMatrix_Diag<Block_type> Diag_type;
  SparseNonZeroPattern<Block_type> nz(m,m);
  TriDiagPattern(nz);
  Matrix_type A( nz );

  Diag_type D;

  BOOST_CHECK_EQUAL( 0, D.m() );
  BOOST_CHECK_EQUAL( 0, D.n() );

  D.resize(A);

  BOOST_CHECK_EQUAL( A.m(), D.m() );
  BOOST_CHECK_EQUAL( A.n(), D.n() );

  BOOST_CHECK_EQUAL( D.m(), m );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_Diag_Block )
{
  const int m = 5;
  typedef DLA::MatrixD<Real> Block_type;
  typedef SparseMatrix_CRS< Block_type > Matrix_type;
  typedef SparseMatrix_Diag<Block_type> Diag_type;
  SparseNonZeroPattern<Block_type> nz(m,m);
  TriDiagPattern(nz,2);
  Matrix_type A( nz );

  Diag_type D;

  BOOST_CHECK_EQUAL( 0, D.m() );
  BOOST_CHECK_EQUAL( 0, D.n() );

  D.resize(A);

  BOOST_CHECK_EQUAL( A.m(), D.m() );
  BOOST_CHECK_EQUAL( A.n(), D.n() );

  for (int i = 0; i < D.m(); i++ )
  {
    BOOST_CHECK_EQUAL( A.block_m(i), D[i].m() );
    BOOST_CHECK_EQUAL( A.block_n(i), D[i].n() );

    BOOST_CHECK_EQUAL( A.block_m(i), D.block_m(i) );
    BOOST_CHECK_EQUAL( A.block_n(i), D.block_n(i) );

    BOOST_CHECK_EQUAL( i*4, D.block_i(i) );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_Diag_Static )
{
  const int m = 5;
  typedef DLA::MatrixS<2,2,Real> Block_type;
  typedef SparseMatrix_CRS<Block_type> Matrix_type;
  typedef SparseMatrix_Diag<Block_type> Diag_type;
  SparseNonZeroPattern<Block_type> nz(m,m);
  TriDiagPattern(nz);
  Matrix_type A( nz );

  Diag_type D;

  BOOST_CHECK_EQUAL( 0, D.m() );
  BOOST_CHECK_EQUAL( 0, D.n() );

  D.resize(A);

  BOOST_CHECK_EQUAL( A.m(), D.m() );
  BOOST_CHECK_EQUAL( A.n(), D.n() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_Diag_Block_Static )
{
  const int m = 5;
  typedef DLA::MatrixD<Real> Block_type;
  typedef SparseMatrix_CRS< Block_type > Matrix_type;
  typedef SparseMatrix_Diag<Block_type> Diag_type;
  SparseNonZeroPattern<Block_type> nz(m,m);
  TriDiagPattern(nz,2);
  Matrix_type A( nz );

  Diag_type D;

  BOOST_CHECK_EQUAL( 0, D.m() );
  BOOST_CHECK_EQUAL( 0, D.n() );

  D.resize(A);

  BOOST_CHECK_EQUAL( A.m(), D.m() );
  BOOST_CHECK_EQUAL( A.n(), D.n() );

  for (int i = 0; i < D.m(); i++ )
  {
    BOOST_CHECK_EQUAL( A.block_m(i), D[i].m() );
    BOOST_CHECK_EQUAL( A.block_n(i), D[i].n() );

    BOOST_CHECK_EQUAL( A.block_m(i), D.block_m(i) );
    BOOST_CHECK_EQUAL( A.block_n(i), D.block_n(i) );

    BOOST_CHECK_EQUAL( i*4, D.block_i(i) );
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
