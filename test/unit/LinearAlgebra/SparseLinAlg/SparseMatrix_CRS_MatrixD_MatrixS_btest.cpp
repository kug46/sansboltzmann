// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include "tools/SANSnumerics.h"
#include "tools/minmax.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Add.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"

#include "../TriDiagPattern_btest.h"
#include "../Heat1D_btest.h"
#include "../Advection1D_btest.h"

#include "unit/LinearAlgebra/DenseLinAlg/chkMatrixS_btest.h"

using namespace SANS::SLA;
using namespace SANS;

//Explicitly instantiate templates to get correct coverage information
namespace SANS
{
namespace SLA
{
typedef SparseVector< DLA::VectorD< DLA::VectorS<1,Real> > > Vec_type;
typedef SparseMatrix_CRS< DLA::MatrixD< DLA::MatrixS<1,1,Real> > > Mat_type;
typedef OpMul<Mat_type, Vec_type> MulVec_type;
template class SparseMatrix_CRS< DLA::MatrixD<Real> >;
template class OpMul<Mat_type, Vec_type>;
template class OpAdd<Vec_type, MulVec_type, true>;
template class OpSub<Vec_type, MulVec_type, true>;
template class OpMulScalar<MulVec_type, true>;
template class OpAdd<Vec_type, Vec_type, false>;
template class OpSub<Vec_type, Vec_type, false>;
template class OpMulScalar<Vec_type, false>;
}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( SparseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_CRS_Block_Static_ctor )
{
  const int m = 5;
  typedef DLA::MatrixD< DLA::MatrixS<2,2,Real> > Block_type;
  typedef SparseMatrix_CRS< Block_type > Matrix_type;
  SparseNonZeroPattern< Block_type > nz(m,m);
  TriDiagPattern(nz,2);
  Matrix_type A( nz );

  const Matrix_type& cA = A;

  //Check that all tri-diag mappings are correct

  BOOST_CHECK_EQUAL(A.getNumNonZero(), 3*(m-2) + 4 );
  for ( int i = 0; i < A.m(); i++ )
    for ( int k = 0; k < A.rowNonZero(i); k++)
      for ( int bi = 0; bi < 2; bi++ )
        for ( int bj = 0; bj < 2; bj++ )
          BOOST_CHECK( chkMatrixS22( A.sparseRow(i,k)(bi,bj), 0, 0, 0, 0) );

  int *row_ptr = A.get_row_ptr();
  int *col_ind = A.get_col_ind();

  BOOST_CHECK_EQUAL( A.get_values(), &A[0](0,0));

  //First row in the matrix
  BOOST_CHECK_EQUAL(row_ptr[0], 0);
  BOOST_CHECK_EQUAL(row_ptr[1], 2);
  BOOST_CHECK_EQUAL(col_ind[0], 0);
  BOOST_CHECK_EQUAL(col_ind[1], 1);

  //All the entries on the interior rows
  for ( int i = 0; i < m-2; i++ )
  {
    BOOST_CHECK_EQUAL(row_ptr[i+2], 3*i + 3 + 2);
    BOOST_CHECK_EQUAL(col_ind[3*i + 0 + 2], i + 0);
    BOOST_CHECK_EQUAL(col_ind[3*i + 1 + 2], i + 1);
    BOOST_CHECK_EQUAL(col_ind[3*i + 2 + 2], i + 2);
  }

  //Last row in the matrix
  BOOST_CHECK_EQUAL(row_ptr[m], 3*(m-2) + 4);
  BOOST_CHECK_EQUAL(col_ind[3*(m-2) + 2], (m-2) + 0);
  BOOST_CHECK_EQUAL(col_ind[3*(m-2) + 3], (m-2) + 1);

  //Check that the row_ptr and col_ind work the way they are intended to be used
  for ( int i = 0; i < m; i++ )
    for ( int k = row_ptr[i]; k < row_ptr[i+1]; k++)
    {
      for ( int bi = 0; bi < 2; bi++ )
        for ( int bj = 0; bj < 2; bj++ )
        {
          BOOST_CHECK( chkMatrixS22(A[k](bi,bj), 0, 0, 0, 0) );
          BOOST_CHECK( chkMatrixS22(cA[k](bi,bj), 0, 0, 0, 0) );
        }

      BOOST_CHECK_EQUAL(col_ind[k], max(i-1,0) + k - row_ptr[i]);
    }

  //Test isNonZero operator

  BOOST_CHECK(cA.isNonZero(0,0));
  BOOST_CHECK(cA.isNonZero(0,1));

  for ( int j = 2; j < cA.n(); j++ )
    BOOST_CHECK(!cA.isNonZero(0,j));

  for ( int i = 1; i < cA.m()-1; i++ )
  {
    for ( int j = 0; j < i-1; j++ )
      BOOST_CHECK(!cA.isNonZero(i,j));

    BOOST_CHECK(cA.isNonZero(i,i-1));
    BOOST_CHECK(cA.isNonZero(i,i));
    BOOST_CHECK(cA.isNonZero(i,i+1));

    for ( int j = i+2; j < cA.n(); j++ )
      BOOST_CHECK(!cA.isNonZero(i,j));
  }

  for ( int j = 0; j < cA.n()-2; j++ )
    BOOST_CHECK(!cA.isNonZero(A.m()-1,j));

  BOOST_CHECK(cA.isNonZero(A.m()-1,cA.n()-2));
  BOOST_CHECK(cA.isNonZero(A.m()-1,cA.n()-1));

  //Initialize as a '1D heat equation' matrix and check that is correct
  Heat1D< Block_type >::init(A);

  BOOST_CHECK( chkMatrixS22(cA.sparseRow(0,0)(0,0),  2, 0, 0, 2) );
  BOOST_CHECK( chkMatrixS22(cA.sparseRow(0,0)(0,1),  0, 0, 0, 0) );
  BOOST_CHECK( chkMatrixS22(cA.sparseRow(0,0)(1,0),  0, 0, 0, 0) );
  BOOST_CHECK( chkMatrixS22(cA.sparseRow(0,0)(1,1),  2, 0, 0, 2) );

  BOOST_CHECK( chkMatrixS22(cA.sparseRow(0,1)(0,0), -1, 0, 0, -1) );
  BOOST_CHECK( chkMatrixS22(cA.sparseRow(0,1)(0,1),  0, 0, 0,  0) );
  BOOST_CHECK( chkMatrixS22(cA.sparseRow(0,1)(1,0),  0, 0, 0,  0) );
  BOOST_CHECK( chkMatrixS22(cA.sparseRow(0,1)(1,1), -1, 0, 0, -1) );

  for ( int i = 1; i < cA.m()-1; i++ )
  {
    BOOST_CHECK( chkMatrixS22(cA.sparseRow(i,0)(0,0), -1, 0, 0, -1) );
    BOOST_CHECK( chkMatrixS22(cA.sparseRow(i,0)(0,1),  0, 0, 0,  0) );
    BOOST_CHECK( chkMatrixS22(cA.sparseRow(i,0)(1,0),  0, 0, 0,  0) );
    BOOST_CHECK( chkMatrixS22(cA.sparseRow(i,0)(1,1), -1, 0, 0, -1) );

    BOOST_CHECK( chkMatrixS22(cA.sparseRow(i,1)(0,0),  2, 0, 0, 2) );
    BOOST_CHECK( chkMatrixS22(cA.sparseRow(i,1)(0,1),  0, 0, 0, 0) );
    BOOST_CHECK( chkMatrixS22(cA.sparseRow(i,1)(1,0),  0, 0, 0, 0) );
    BOOST_CHECK( chkMatrixS22(cA.sparseRow(i,1)(1,1),  2, 0, 0, 2) );

    BOOST_CHECK( chkMatrixS22(cA.sparseRow(i,2)(0,0), -1, 0, 0, -1) );
    BOOST_CHECK( chkMatrixS22(cA.sparseRow(i,2)(0,1),  0, 0, 0,  0) );
    BOOST_CHECK( chkMatrixS22(cA.sparseRow(i,2)(1,0),  0, 0, 0,  0) );
    BOOST_CHECK( chkMatrixS22(cA.sparseRow(i,2)(1,1), -1, 0, 0, -1) );
  }
  BOOST_CHECK( chkMatrixS22(cA.sparseRow(A.m()-1,0)(0,0), -1, 0, 0, -1) );
  BOOST_CHECK( chkMatrixS22(cA.sparseRow(A.m()-1,0)(0,1),  0, 0, 0,  0) );
  BOOST_CHECK( chkMatrixS22(cA.sparseRow(A.m()-1,0)(1,0),  0, 0, 0,  0) );
  BOOST_CHECK( chkMatrixS22(cA.sparseRow(A.m()-1,0)(1,1), -1, 0, 0, -1) );

  BOOST_CHECK( chkMatrixS22(cA.sparseRow(A.m()-1,1)(0,0),  2, 0, 0, 2) );
  BOOST_CHECK( chkMatrixS22(cA.sparseRow(A.m()-1,1)(0,1),  0, 0, 0, 0) );
  BOOST_CHECK( chkMatrixS22(cA.sparseRow(A.m()-1,1)(1,0),  0, 0, 0, 0) );
  BOOST_CHECK( chkMatrixS22(cA.sparseRow(A.m()-1,1)(1,1),  2, 0, 0, 2) );


  //Test dense matrix accessor
  BOOST_CHECK( chkMatrixS22(cA(0,0)(0,0),  2, 0, 0, 2) );
  BOOST_CHECK( chkMatrixS22(cA(0,0)(0,1),  0, 0, 0, 0) );
  BOOST_CHECK( chkMatrixS22(cA(0,0)(1,0),  0, 0, 0, 0) );
  BOOST_CHECK( chkMatrixS22(cA(0,0)(1,1),  2, 0, 0, 2) );

  BOOST_CHECK( chkMatrixS22(cA(0,1)(0,0), -1, 0, 0, -1) );
  BOOST_CHECK( chkMatrixS22(cA(0,1)(0,1),  0, 0, 0,  0) );
  BOOST_CHECK( chkMatrixS22(cA(0,1)(1,0),  0, 0, 0,  0) );
  BOOST_CHECK( chkMatrixS22(cA(0,1)(1,1), -1, 0, 0, -1) );

  for ( int i = 1; i < cA.m()-1; i++ )
  {
    BOOST_CHECK( chkMatrixS22(cA(i,i-1)(0,0), -1, 0, 0, -1) );
    BOOST_CHECK( chkMatrixS22(cA(i,i-1)(0,1),  0, 0, 0,  0) );
    BOOST_CHECK( chkMatrixS22(cA(i,i-1)(1,0),  0, 0, 0,  0) );
    BOOST_CHECK( chkMatrixS22(cA(i,i-1)(1,1), -1, 0, 0, -1) );

    BOOST_CHECK( chkMatrixS22(cA(i,i  )(0,0),  2, 0, 0, 2) );
    BOOST_CHECK( chkMatrixS22(cA(i,i  )(0,1),  0, 0, 0, 0) );
    BOOST_CHECK( chkMatrixS22(cA(i,i  )(1,0),  0, 0, 0, 0) );
    BOOST_CHECK( chkMatrixS22(cA(i,i  )(1,1),  2, 0, 0, 2) );

    BOOST_CHECK( chkMatrixS22(cA(i,i+1)(0,0), -1, 0, 0, -1) );
    BOOST_CHECK( chkMatrixS22(cA(i,i+1)(0,1),  0, 0, 0,  0) );
    BOOST_CHECK( chkMatrixS22(cA(i,i+1)(1,0),  0, 0, 0,  0) );
    BOOST_CHECK( chkMatrixS22(cA(i,i+1)(1,1), -1, 0, 0, -1) );
  }
  BOOST_CHECK( chkMatrixS22(cA(A.m()-1,A.m()-2)(0,0), -1, 0, 0, -1) );
  BOOST_CHECK( chkMatrixS22(cA(A.m()-1,A.m()-2)(0,1),  0, 0, 0,  0) );
  BOOST_CHECK( chkMatrixS22(cA(A.m()-1,A.m()-2)(1,0),  0, 0, 0,  0) );
  BOOST_CHECK( chkMatrixS22(cA(A.m()-1,A.m()-2)(1,1), -1, 0, 0, -1) );

  BOOST_CHECK( chkMatrixS22(cA(A.m()-1,A.m()-1)(0,0),  2, 0, 0, 2) );
  BOOST_CHECK( chkMatrixS22(cA(A.m()-1,A.m()-1)(0,1),  0, 0, 0, 0) );
  BOOST_CHECK( chkMatrixS22(cA(A.m()-1,A.m()-1)(1,0),  0, 0, 0, 0) );
  BOOST_CHECK( chkMatrixS22(cA(A.m()-1,A.m()-1)(1,1),  2, 0, 0, 2) );

  BOOST_CHECK_THROW( cA(0,2), AssertionException );
  BOOST_CHECK_THROW( cA(A.m()-1,0), AssertionException );
  BOOST_CHECK_THROW( cA(-1,0), AssertionException );
  BOOST_CHECK_THROW( cA(0,-1), AssertionException );

  //Test assignement to the dense matrix accessor
  A.slowAccess(0,0) = 3;
  BOOST_CHECK( chkMatrixS22(cA(0,0)(0,0),  3, 3, 3, 3) );
  BOOST_CHECK( chkMatrixS22(cA(0,0)(0,1),  3, 3, 3, 3) );
  BOOST_CHECK( chkMatrixS22(cA(0,0)(1,0),  3, 3, 3, 3) );
  BOOST_CHECK( chkMatrixS22(cA(0,0)(1,1),  3, 3, 3, 3) );

  //Test dense matrix accessor on pattern
  BOOST_CHECK( chkMatrixS22(nz(0,0)(0,0),  1, 1, 1, 1) );
  BOOST_CHECK( chkMatrixS22(nz(0,0)(0,1),  1, 1, 1, 1) );
  BOOST_CHECK( chkMatrixS22(nz(0,0)(1,0),  1, 1, 1, 1) );
  BOOST_CHECK( chkMatrixS22(nz(0,0)(1,1),  1, 1, 1, 1) );

  BOOST_CHECK( chkMatrixS22(nz(0,1)(0,0), 1, 1, 1, 1) );
  BOOST_CHECK( chkMatrixS22(nz(0,1)(0,1), 1, 1, 1, 1) );
  BOOST_CHECK( chkMatrixS22(nz(0,1)(1,0), 1, 1, 1, 1) );
  BOOST_CHECK( chkMatrixS22(nz(0,1)(1,1), 1, 1, 1, 1) );

  for ( int i = 1; i < cA.m()-1; i++ )
  {
    BOOST_CHECK( chkMatrixS22(nz(i,i-1)(0,0), 1, 1, 1, 1) );
    BOOST_CHECK( chkMatrixS22(nz(i,i-1)(0,1), 1, 1, 1, 1) );
    BOOST_CHECK( chkMatrixS22(nz(i,i-1)(1,0), 1, 1, 1, 1) );
    BOOST_CHECK( chkMatrixS22(nz(i,i-1)(1,1), 1, 1, 1, 1) );

    BOOST_CHECK( chkMatrixS22(nz(i,i  )(0,0),  1, 1, 1, 1) );
    BOOST_CHECK( chkMatrixS22(nz(i,i  )(0,1),  1, 1, 1, 1) );
    BOOST_CHECK( chkMatrixS22(nz(i,i  )(1,0),  1, 1, 1, 1) );
    BOOST_CHECK( chkMatrixS22(nz(i,i  )(1,1),  1, 1, 1, 1) );

    BOOST_CHECK( chkMatrixS22(nz(i,i+1)(0,0), 1, 1, 1, 1) );
    BOOST_CHECK( chkMatrixS22(nz(i,i+1)(0,1), 1, 1, 1, 1) );
    BOOST_CHECK( chkMatrixS22(nz(i,i+1)(1,0), 1, 1, 1, 1) );
    BOOST_CHECK( chkMatrixS22(nz(i,i+1)(1,1), 1, 1, 1, 1) );
  }
  BOOST_CHECK( chkMatrixS22(nz(A.m()-1,A.m()-2)(0,0), 1, 1, 1, 1) );
  BOOST_CHECK( chkMatrixS22(nz(A.m()-1,A.m()-2)(0,1), 1, 1, 1, 1) );
  BOOST_CHECK( chkMatrixS22(nz(A.m()-1,A.m()-2)(1,0), 1, 1, 1, 1) );
  BOOST_CHECK( chkMatrixS22(nz(A.m()-1,A.m()-2)(1,1), 1, 1, 1, 1) );

  BOOST_CHECK( chkMatrixS22(nz(A.m()-1,A.m()-1)(0,0),  1, 1, 1, 1) );
  BOOST_CHECK( chkMatrixS22(nz(A.m()-1,A.m()-1)(0,1),  1, 1, 1, 1) );
  BOOST_CHECK( chkMatrixS22(nz(A.m()-1,A.m()-1)(1,0),  1, 1, 1, 1) );
  BOOST_CHECK( chkMatrixS22(nz(A.m()-1,A.m()-1)(1,1),  1, 1, 1, 1) );

  BOOST_CHECK( chkMatrixS22(nz(0,2)(0,0),  0, 0, 0, 0) );
  BOOST_CHECK( chkMatrixS22(nz(0,2)(0,1),  0, 0, 0, 0) );
  BOOST_CHECK( chkMatrixS22(nz(0,2)(1,0),  0, 0, 0, 0) );
  BOOST_CHECK( chkMatrixS22(nz(0,2)(1,1),  0, 0, 0, 0) );

  BOOST_CHECK( chkMatrixS22(nz(A.m()-1,0)(0,0), 0, 0, 0, 0) );
  BOOST_CHECK( chkMatrixS22(nz(A.m()-1,0)(0,1), 0, 0, 0, 0) );
  BOOST_CHECK( chkMatrixS22(nz(A.m()-1,0)(1,0), 0, 0, 0, 0) );
  BOOST_CHECK( chkMatrixS22(nz(A.m()-1,0)(1,1), 0, 0, 0, 0) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_CRS_Empty_Block_Static_MulVec )
{
  typedef DLA::VectorD< DLA::VectorS<2,Real> > BlockVec_type;
  typedef DLA::MatrixD< DLA::MatrixS<2,2,Real> > Block_type;

  SparseVector< BlockVec_type > x(5,2), b(5,2);
  SparseNonZeroPattern< Block_type > Null(5,5);

  //Create an empty matrix, which acts like a 'zero' matrix
  SparseMatrix_CRS< Block_type > A(Null);

  x = 1;
  x[2] = 2;

  b = A*x;
  for ( int j = 0; j < b.m(); j++)
    for ( int i = 0; i < 2; i++ )
      for ( int k = 0; k < 2; k++ )
        BOOST_CHECK_EQUAL(b[j][i][k], 0);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_CRS_Block_Static_MulVec )
{
  typedef DLA::VectorD< DLA::VectorS<2,Real> > BlockVec_type;
  typedef DLA::MatrixD< DLA::MatrixS<2,2,Real> > Block_type;

  SparseVector< BlockVec_type > x(5,2), b(5,2), c(3,2);
  SparseNonZeroPattern< Block_type > nz(5,5);
  TriDiagPattern(nz,2);
  SparseMatrix_CRS< Block_type > A( nz );

  Heat1D< Block_type >::init(A);

  x = 1;
  x[2] = 2;

  b = A*x;
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(b[0][i][j], 1);
      BOOST_CHECK_EQUAL(b[1][i][j],-1);
      BOOST_CHECK_EQUAL(b[2][i][j], 2);
      BOOST_CHECK_EQUAL(b[3][i][j],-1);
      BOOST_CHECK_EQUAL(b[4][i][j], 1);
    }

  b = +(A*x);
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(b[0][i][j], 1);
      BOOST_CHECK_EQUAL(b[1][i][j],-1);
      BOOST_CHECK_EQUAL(b[2][i][j], 2);
      BOOST_CHECK_EQUAL(b[3][i][j],-1);
      BOOST_CHECK_EQUAL(b[4][i][j], 1);
    }

  b += A*x;
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(b[0][i][j], 2);
      BOOST_CHECK_EQUAL(b[1][i][j],-2);
      BOOST_CHECK_EQUAL(b[2][i][j], 4);
      BOOST_CHECK_EQUAL(b[3][i][j],-2);
      BOOST_CHECK_EQUAL(b[4][i][j], 2);
    }


  b = 2*(A*x);
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(b[0][i][j], 2);
      BOOST_CHECK_EQUAL(b[1][i][j],-2);
      BOOST_CHECK_EQUAL(b[2][i][j], 4);
      BOOST_CHECK_EQUAL(b[3][i][j],-2);
      BOOST_CHECK_EQUAL(b[4][i][j], 2);
    }

  b = +(2*(A*x));
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(b[0][i][j], 2);
      BOOST_CHECK_EQUAL(b[1][i][j],-2);
      BOOST_CHECK_EQUAL(b[2][i][j], 4);
      BOOST_CHECK_EQUAL(b[3][i][j],-2);
      BOOST_CHECK_EQUAL(b[4][i][j], 2);
    }

  b += 2*(A*x);
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(b[0][i][j], 4);
      BOOST_CHECK_EQUAL(b[1][i][j],-4);
      BOOST_CHECK_EQUAL(b[2][i][j], 8);
      BOOST_CHECK_EQUAL(b[3][i][j],-4);
      BOOST_CHECK_EQUAL(b[4][i][j], 4);
    }

  b = -(2*(A*x));
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(b[0][i][j],-2);
      BOOST_CHECK_EQUAL(b[1][i][j], 2);
      BOOST_CHECK_EQUAL(b[2][i][j],-4);
      BOOST_CHECK_EQUAL(b[3][i][j], 2);
      BOOST_CHECK_EQUAL(b[4][i][j],-2);
    }


  b = (A*x)*2;
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(b[0][i][j], 2);
      BOOST_CHECK_EQUAL(b[1][i][j],-2);
      BOOST_CHECK_EQUAL(b[2][i][j], 4);
      BOOST_CHECK_EQUAL(b[3][i][j],-2);
      BOOST_CHECK_EQUAL(b[4][i][j], 2);
    }

  b = (A*x)/2;
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(b[0][i][j], 0.5);
      BOOST_CHECK_EQUAL(b[1][i][j],-0.5);
      BOOST_CHECK_EQUAL(b[2][i][j], 1);
      BOOST_CHECK_EQUAL(b[3][i][j],-0.5);
      BOOST_CHECK_EQUAL(b[4][i][j], 0.5);
    }



  b = (2*(A*x))*2;
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(b[0][i][j], 4);
      BOOST_CHECK_EQUAL(b[1][i][j],-4);
      BOOST_CHECK_EQUAL(b[2][i][j], 8);
      BOOST_CHECK_EQUAL(b[3][i][j],-4);
      BOOST_CHECK_EQUAL(b[4][i][j], 4);
    }

  b = 2*((A*x)*2);
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(b[0][i][j], 4);
      BOOST_CHECK_EQUAL(b[1][i][j],-4);
      BOOST_CHECK_EQUAL(b[2][i][j], 8);
      BOOST_CHECK_EQUAL(b[3][i][j],-4);
      BOOST_CHECK_EQUAL(b[4][i][j], 4);
    }

  b = (2*(A*x))/2;
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(b[0][i][j], 1);
      BOOST_CHECK_EQUAL(b[1][i][j],-1);
      BOOST_CHECK_EQUAL(b[2][i][j], 2);
      BOOST_CHECK_EQUAL(b[3][i][j],-1);
      BOOST_CHECK_EQUAL(b[4][i][j], 1);
    }


  b = (2*A)*x;
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(b[0][i][j], 2);
      BOOST_CHECK_EQUAL(b[1][i][j],-2);
      BOOST_CHECK_EQUAL(b[2][i][j], 4);
      BOOST_CHECK_EQUAL(b[3][i][j],-2);
      BOOST_CHECK_EQUAL(b[4][i][j], 2);
    }

  b = +((2*A)*x);
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(b[0][i][j], 2);
      BOOST_CHECK_EQUAL(b[1][i][j],-2);
      BOOST_CHECK_EQUAL(b[2][i][j], 4);
      BOOST_CHECK_EQUAL(b[3][i][j],-2);
      BOOST_CHECK_EQUAL(b[4][i][j], 2);
    }

  b += (2*A)*x;
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(b[0][i][j], 4);
      BOOST_CHECK_EQUAL(b[1][i][j],-4);
      BOOST_CHECK_EQUAL(b[2][i][j], 8);
      BOOST_CHECK_EQUAL(b[3][i][j],-4);
      BOOST_CHECK_EQUAL(b[4][i][j], 4);
    }


  b = A*(x*2);
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(b[0][i][j], 2);
      BOOST_CHECK_EQUAL(b[1][i][j],-2);
      BOOST_CHECK_EQUAL(b[2][i][j], 4);
      BOOST_CHECK_EQUAL(b[3][i][j],-2);
      BOOST_CHECK_EQUAL(b[4][i][j], 2);
    }

  b = +(A*(x*2));
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(b[0][i][j], 2);
      BOOST_CHECK_EQUAL(b[1][i][j],-2);
      BOOST_CHECK_EQUAL(b[2][i][j], 4);
      BOOST_CHECK_EQUAL(b[3][i][j],-2);
      BOOST_CHECK_EQUAL(b[4][i][j], 2);
    }

  b += A*(x*2);
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(b[0][i][j], 4);
      BOOST_CHECK_EQUAL(b[1][i][j],-4);
      BOOST_CHECK_EQUAL(b[2][i][j], 8);
      BOOST_CHECK_EQUAL(b[3][i][j],-4);
      BOOST_CHECK_EQUAL(b[4][i][j], 4);
    }

  BOOST_CHECK_THROW( b = A*c, AssertionException );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_CRS_Block_Static_Expression )
{
  typedef DLA::VectorD< DLA::VectorS<2,Real> > BlockVec_type;
  typedef DLA::MatrixD< DLA::MatrixS<2,2,Real> > Block_type;

  SparseVector< BlockVec_type > x(5,2), b(5,2), r(5,2), c(3,2);

  SparseNonZeroPattern< Block_type > nz(5,5);
  TriDiagPattern(nz,2);
  SparseMatrix_CRS< Block_type > A( nz );

  Heat1D< Block_type >::init(A);

  x = 1;
  b = 2;
  x[2] = 2;

  r = b + A*x;
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(r[0][i][j], 3);
      BOOST_CHECK_EQUAL(r[1][i][j], 1);
      BOOST_CHECK_EQUAL(r[2][i][j], 4);
      BOOST_CHECK_EQUAL(r[3][i][j], 1);
      BOOST_CHECK_EQUAL(r[4][i][j], 3);
    }

  r = +(b + A*x);
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(r[0][i][j], 3);
      BOOST_CHECK_EQUAL(r[1][i][j], 1);
      BOOST_CHECK_EQUAL(r[2][i][j], 4);
      BOOST_CHECK_EQUAL(r[3][i][j], 1);
      BOOST_CHECK_EQUAL(r[4][i][j], 3);
    }

  r += b + A*x;
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(r[0][i][j], 6);
      BOOST_CHECK_EQUAL(r[1][i][j], 2);
      BOOST_CHECK_EQUAL(r[2][i][j], 8);
      BOOST_CHECK_EQUAL(r[3][i][j], 2);
      BOOST_CHECK_EQUAL(r[4][i][j], 6);
    }

  r = b - A*x;
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(r[0][i][j], 1);
      BOOST_CHECK_EQUAL(r[1][i][j], 3);
      BOOST_CHECK_EQUAL(r[2][i][j], 0);
      BOOST_CHECK_EQUAL(r[3][i][j], 3);
      BOOST_CHECK_EQUAL(r[4][i][j], 1);
    }

  r = +(b - A*x);
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(r[0][i][j], 1);
      BOOST_CHECK_EQUAL(r[1][i][j], 3);
      BOOST_CHECK_EQUAL(r[2][i][j], 0);
      BOOST_CHECK_EQUAL(r[3][i][j], 3);
      BOOST_CHECK_EQUAL(r[4][i][j], 1);
    }

  r += b - A*x;
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(r[0][i][j], 2);
      BOOST_CHECK_EQUAL(r[1][i][j], 6);
      BOOST_CHECK_EQUAL(r[2][i][j], 0);
      BOOST_CHECK_EQUAL(r[3][i][j], 6);
      BOOST_CHECK_EQUAL(r[4][i][j], 2);
    }

  r = -A*x + b;
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(r[0][i][j], 1);
      BOOST_CHECK_EQUAL(r[1][i][j], 3);
      BOOST_CHECK_EQUAL(r[2][i][j], 0);
      BOOST_CHECK_EQUAL(r[3][i][j], 3);
      BOOST_CHECK_EQUAL(r[4][i][j], 1);
    }

  r = 2*(b + A*x);
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(r[0][i][j], 6);
      BOOST_CHECK_EQUAL(r[1][i][j], 2);
      BOOST_CHECK_EQUAL(r[2][i][j], 8);
      BOOST_CHECK_EQUAL(r[3][i][j], 2);
      BOOST_CHECK_EQUAL(r[4][i][j], 6);
    }

  r = (b + A*x)*2;
  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(r[0][i][j], 6);
      BOOST_CHECK_EQUAL(r[1][i][j], 2);
      BOOST_CHECK_EQUAL(r[2][i][j], 8);
      BOOST_CHECK_EQUAL(r[3][i][j], 2);
      BOOST_CHECK_EQUAL(r[4][i][j], 6);
    }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_CRS_Block_Static_scatterAdd_NonZeroPattern )
{
  typedef DLA::MatrixD< DLA::MatrixS<2,2,Real> > Block_type;

  SparseNonZeroPattern<Block_type> Pattern(5,5);

  Block_type I(2,2);
  I = DLA::Identity();

  Block_type mtxLocal(2,2);

  for ( int i = 1; i < (int)Pattern.m(); i++ )
  {
    Pattern.scatterAdd( mtxLocal =  1*I,i-1,i-1);
    Pattern.scatterAdd( mtxLocal = -1*I,i-1,i);
    Pattern.scatterAdd( mtxLocal = -1*I,i,i-1);
    Pattern.scatterAdd( mtxLocal =  1*I,i,i);
  }

  Pattern.scatterAdd(I,0,0);

  Pattern.scatterAdd(I,4,4);

  SparseNonZeroPattern< Block_type > nz(5,5);
  TriDiagPattern(nz,2);
  SparseMatrix_CRS< Block_type > A1( nz ), A2( Pattern );

  //Initialize as a '1D heat equation' matrix and check that is correct
  Heat1D< Block_type >::init(A1);

  BOOST_REQUIRE_EQUAL(A1.m(), A2.m());

  for ( int i = 1; i < (int)A2.m(); i++ )
  {
    A2.scatterAdd( mtxLocal =  1*I,i-1,i-1);
    A2.scatterAdd( mtxLocal = -1*I,i-1,i);
    A2.scatterAdd( mtxLocal = -1*I,i,i-1);
    A2.scatterAdd( mtxLocal =  1*I,i,i);
  }

  A2.scatterAdd(I,0,0);

  A2.scatterAdd(I,4,4);

  for ( int i = 0; i < A1.m(); i++ )
  {
    BOOST_REQUIRE_EQUAL(A1.rowNonZero(i), A2.rowNonZero(i));
    for ( int j = 0; j < A1.rowNonZero(i); j++ )
      for ( int ib = 0; ib < 2; ib++ )
        for ( int jb = 0; jb < 2; jb++ )
          for ( int is = 0; is < 2; is++ )
            for ( int js = 0; js < 2; js++ )
              BOOST_CHECK_EQUAL(A1.sparseRow(i,j)(ib,jb)(is,js), A2.sparseRow(i,j)(ib,jb)(is,js));
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_CRS_Block_Static_scatterAdd_Advection )
{
  typedef DLA::MatrixD< DLA::MatrixS<2,2,Real> > Block_type;

  SparseNonZeroPattern<Block_type> Pattern(5,5);

  Block_type I(2,2);
  I = DLA::Identity();

  Block_type mtxLocal(2,2);

  for ( int i = 1; i < (int)Pattern.m(); i++ )
  {
    Pattern.scatterAdd( mtxLocal =  1*I,i-1,i-1);
    Pattern.scatterAdd( mtxLocal = -1*I,i-1,i);
    Pattern.scatterAdd( mtxLocal = -1*I,i,i-1);
    Pattern.scatterAdd( mtxLocal =  1*I,i,i);
  }

  Pattern.scatterAdd(I,0,0);

  Pattern.scatterAdd(I,4,4);

  SparseNonZeroPattern< Block_type > nz(5,5);
  TriDiagPattern(nz,2);
  SparseMatrix_CRS< Block_type > A1( nz ), A2( Pattern );

  //Initialize as a '1D heat equation' matrix and check that is correct
  Advection1D< Block_type >::init(A1);

  BOOST_REQUIRE_EQUAL(A1.m(), A2.m());

  for ( int i = 1; i < (int)A2.m(); i++ )
  {
    A2.scatterAdd( mtxLocal =  1*I,i-1,i-1);
    A2.scatterAdd( mtxLocal = -1*I,i,i-1);
  }

  A2.scatterAdd(I,4,4);

  for ( int i = 0; i < A1.m(); i++ )
  {
    BOOST_REQUIRE_EQUAL(A1.rowNonZero(i), A2.rowNonZero(i));
    for ( int j = 0; j < A1.rowNonZero(i); j++ )
      for ( int ib = 0; ib < 2; ib++ )
        for ( int jb = 0; jb < 2; jb++ )
          for ( int is = 0; is < 2; is++ )
            for ( int js = 0; js < 2; js++ )
              BOOST_CHECK_EQUAL(A1.sparseRow(i,j)(ib,jb)(is,js), A2.sparseRow(i,j)(ib,jb)(is,js));
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_CRS_Block_Static_scatterAdd_Transpose )
{
  typedef DLA::MatrixD< DLA::MatrixS<2,2,Real> > Block_type;

  SparseNonZeroPattern<Block_type> Pattern(5,5);

  Block_type I(2,2);
  I = DLA::Identity();
  I(0,1) = 2;
  I(1,0) = 3;
  I(0,1)(0,1) = 4;
  I(1,0)(1,0) = 5;

  Block_type mtxLocal(2,2);

  for ( int i = 1; i < (int)Pattern.m(); i++ )
  {
    Pattern.scatterAdd( mtxLocal =  1*I,i-1,i-1);
    Pattern.scatterAdd( mtxLocal = -1*I,i-1,i);
    Pattern.scatterAdd( mtxLocal = -1*I,i,i-1);
    Pattern.scatterAdd( mtxLocal =  1*I,i,i);
  }

  Pattern.scatterAdd(I,0,0);

  Pattern.scatterAdd(I,4,4);

  SparseNonZeroPattern< Block_type > nz(5,5);
  TriDiagPattern(nz,2);
  SparseMatrix_CRS< Block_type > A1( nz ), A2( Pattern );

  //Initialize as a '1D heat equation' matrix and check that is correct
  Advection1D< Block_type >::init_transpose(A1);

  //Transpose the scatter add operator
  SparseMatrix_CRS_Transpose<Block_type> A2T = Transpose(A2);

  BOOST_REQUIRE_EQUAL(A1.m(), A2T.m());

  for ( int i = 1; i < (int)A2T.m(); i++ )
  {
    A2T.scatterAdd( mtxLocal =  1*I,i-1,i-1);
    A2T.scatterAdd( mtxLocal = -1*I,i,i-1);
  }

  A2T.scatterAdd(I,(int)A2.m()-1,(int)A2.m()-1);

  //Check that A2 is transposed
  for ( int i = 0; i < A1.m(); i++ )
  {
    BOOST_REQUIRE_EQUAL(A1.rowNonZero(i), A2.rowNonZero(i));
    for ( int j = 0; j < A1.rowNonZero(i); j++ )
      for ( int ib = 0; ib < 2; ib++ )
        for ( int jb = 0; jb < 2; jb++ )
          for ( int is = 0; is < 2; is++ )
            for ( int js = 0; js < 2; js++ )
              BOOST_CHECK_EQUAL(A1.sparseRow(i,j)(ib,jb)(is,js), A2.sparseRow(i,j)(ib,jb)(is,js));
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_CRS_Block_Static_scatterAdd_Transpose_NonSquare )
{
// Suppressing clang analyzer warnings
#ifndef __clang_analyzer__

  const int m = 2;
  const int n = 3;
  typedef DLA::MatrixD< DLA::MatrixS<m,n,Real> > Block_type;
  typedef DLA::MatrixD< DLA::MatrixS<n,m,Real> > BlockT_type;

  SparseNonZeroPattern<BlockT_type> nz_A1(3,4);

  Block_type B(2,2);
  B = DLA::Identity();
  B(0,1) = 2;
  B(0,1)(0,1) = 4;
  B(1,0) = 3;
  B(1,0)(1,0) = 5;

  BlockT_type BT = Transpose(B);

//  B.dump(3,std::cout);
//  BT.dump(3,std::cout);

  Block_type mtxLocal(2,2);
  BlockT_type mtxTLocal(2,2);

  nz_A1.scatterAdd(mtxTLocal =    BT, 0, 0);
  nz_A1.scatterAdd(mtxTLocal = -2*BT, 1, 0);
  nz_A1.scatterAdd(mtxTLocal =    BT, 1, 2);
  nz_A1.scatterAdd(mtxTLocal =  2*BT, 2, 0);
  nz_A1.scatterAdd(mtxTLocal =   -BT, 2, 2);
  nz_A1.scatterAdd(mtxTLocal =  3*BT, 2, 3);

  //To please clang analyzer
  BOOST_REQUIRE(nz_A1.nnz() > 0);

  SparseMatrix_CRS< BlockT_type > A1(nz_A1);
  SparseMatrix_CRS< BlockT_type > A2(nz_A1);

  A2.scatterAdd(mtxTLocal =    BT, 0, 0);
  A2.scatterAdd(mtxTLocal = -2*BT, 1, 0);
  A2.scatterAdd(mtxTLocal =    BT, 1, 2);
  A2.scatterAdd(mtxTLocal =  2*BT, 2, 0);
  A2.scatterAdd(mtxTLocal =   -BT, 2, 2);
  A2.scatterAdd(mtxTLocal =  3*BT, 2, 3);

  //Transpose the scatter add operator
  SparseMatrix_CRS_Transpose<BlockT_type> A1T = Transpose(A1);

  BOOST_REQUIRE_EQUAL(A1T.m(), A2.n());
  BOOST_REQUIRE_EQUAL(A1T.n(), A2.m());

  A1T.scatterAdd(mtxLocal =    B, 0, 0);
  A1T.scatterAdd(mtxLocal = -2*B, 0, 1);
  A1T.scatterAdd(mtxLocal =    B, 2, 1);
  A1T.scatterAdd(mtxLocal =  2*B, 0, 2);
  A1T.scatterAdd(mtxLocal =   -B, 2, 2);
  A1T.scatterAdd(mtxLocal =  3*B, 3, 2);

  //Check that A1 is transposed
  for ( int i = 0; i < A1.m(); i++ )
  {
//    std::cout << i << ", " << A1.rowNonZero(i) << std::endl;
    BOOST_REQUIRE_EQUAL(A1.rowNonZero(i), A2.rowNonZero(i));
    for ( int j = 0; j < A1.rowNonZero(i); j++ )
      for ( int ib = 0; ib < 2; ib++ )
        for ( int jb = 0; jb < 2; jb++ )
          for ( int is = 0; is < A1.sparseRow(i,j)(ib,jb).M; is++ )
            for ( int js = 0; js < A1.sparseRow(i,j)(ib,jb).N; js++ )
              BOOST_CHECK_EQUAL(A1.sparseRow(i,j)(ib,jb)(is,js), A2.sparseRow(i,j)(ib,jb)(is,js));
  }
#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
