// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include "tools/SANSnumerics.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Add.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"

#include "LinearAlgebra/DenseLinAlg/tools/norm.h"
#include "LinearAlgebra/SparseLinAlg/tools/norm.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/SparseLinAlg/tools/dot.h"

//Explicitly instantiate templates to get correct coverage information
namespace SANS
{
namespace DLA
{
typedef VectorD< SLA::SparseVector<Real> > Vec_type;
template class VectorD< SLA::SparseVector<Real> >;
}
}

using namespace SANS;
using namespace SANS::SLA;
using namespace SANS::DLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( SparseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorD_SparseVector_Static_ctor )
{
  typedef VectorD< SparseVector< DLA::VectorS<2, Real> > > VecorType;
  VecorType x = {{1}};

  BOOST_CHECK_EQUAL(x[0].m(), 1);

  x[0] = 1;
  BOOST_CHECK_EQUAL(x[0][0][0], 1);
  BOOST_CHECK_EQUAL(x[0][0][1], 1);

  VecorType x2(x);
  BOOST_CHECK_EQUAL(x2[0][0][0], 1);
  BOOST_CHECK_EQUAL(x2[0][0][1], 1);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorD_SparseVector_Static_assign )
{
  typedef VectorD< SparseVector< DLA::VectorS<2, Real> > > VecorType;
  VecorType x{{2}}, b{{2}}, c{{3}};

  x = 0;
  BOOST_CHECK_EQUAL(x[0][0][0], 0);
  BOOST_CHECK_EQUAL(x[0][0][1], 0);
  BOOST_CHECK_EQUAL(x[0][1][0], 0);
  BOOST_CHECK_EQUAL(x[0][1][1], 0);

  b = x;
  BOOST_CHECK_EQUAL(x[0][0][0], 0);
  BOOST_CHECK_EQUAL(x[0][0][1], 0);
  BOOST_CHECK_EQUAL(x[0][1][0], 0);
  BOOST_CHECK_EQUAL(x[0][1][1], 0);
  BOOST_CHECK_EQUAL(b[0][0][0], 0);
  BOOST_CHECK_EQUAL(b[0][0][1], 0);
  BOOST_CHECK_EQUAL(b[0][1][0], 0);
  BOOST_CHECK_EQUAL(b[0][1][1], 0);

  b[0][1] = 1;
  BOOST_CHECK_EQUAL(x[0][0][0], 0);
  BOOST_CHECK_EQUAL(x[0][0][1], 0);
  BOOST_CHECK_EQUAL(x[0][1][0], 0);
  BOOST_CHECK_EQUAL(x[0][1][1], 0);
  BOOST_CHECK_EQUAL(b[0][0][0], 0);
  BOOST_CHECK_EQUAL(b[0][0][1], 0);
  BOOST_CHECK_EQUAL(b[0][1][0], 1);
  BOOST_CHECK_EQUAL(b[0][1][1], 1);

  x = b;
  BOOST_CHECK_EQUAL(x[0][0][0], 0);
  BOOST_CHECK_EQUAL(x[0][0][1], 0);
  BOOST_CHECK_EQUAL(x[0][1][0], 1);
  BOOST_CHECK_EQUAL(x[0][1][1], 1);
  BOOST_CHECK_EQUAL(b[0][0][0], 0);
  BOOST_CHECK_EQUAL(b[0][0][1], 0);
  BOOST_CHECK_EQUAL(b[0][1][0], 1);
  BOOST_CHECK_EQUAL(b[0][1][1], 1);

  BOOST_CHECK_THROW( c = x, AssertionException );
  BOOST_CHECK_THROW( x = c, AssertionException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorD_SparseVector_Static_unary )
{
  typedef VectorD< SparseVector< DLA::VectorS<2, Real> > > VecorType;
  VecorType x{{2}};

  x = 2;
  x /= 2;
  BOOST_CHECK_EQUAL(x[0][0][0], 1);
  BOOST_CHECK_EQUAL(x[0][0][1], 1);
  BOOST_CHECK_EQUAL(x[0][1][0], 1);
  BOOST_CHECK_EQUAL(x[0][1][1], 1);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorD_SparseVector_Static_norm )
{
  typedef VectorD< SparseVector< DLA::VectorS<2, Real> > > VecorType;
  VecorType x{{2},{2}};

  x = 2;
  BOOST_CHECK_EQUAL(norm(x, 2), sqrt(32));
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorD_SparseVector_Static_dot )
{
  typedef VectorD< SparseVector< DLA::VectorS<2, Real> > > VecorType;
  VecorType x{{2},{2}}, x2{{2},{2}}, b{{2},{2}}, c{{2},{2},{2}};

  x = 2;
  b = 3;
  BOOST_CHECK_EQUAL(dot(x, b), 48);
  BOOST_CHECK_THROW(dot(x, c), AssertionException );

  x2 = 3;
  BOOST_CHECK_EQUAL(dot(x+x2, b), 120);
  BOOST_CHECK_EQUAL(dot(b, x+x2), 120);

  typedef VectorD< SparseVector< DLA::MatrixS<1, 2, Real> > > VecorMatrixType;
  VecorMatrixType x3{{2},{2}};

  x3 = 3;
  BOOST_CHECK_EQUAL(dot(x, x3), 48);
  BOOST_CHECK_EQUAL(dot(x3, x), 48);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorD_SparseVector_Static_LazyExpression )
{
  typedef VectorD< SparseVector< DLA::VectorS<2, Real> > > VecorType;
  VecorType x{{2}}, b{{2}}, c{{3}};

  x = 2;
  x.value(1, b);
  BOOST_CHECK_EQUAL(b[0][0][0], 2);
  BOOST_CHECK_EQUAL(b[0][0][1], 2);
  BOOST_CHECK_EQUAL(b[0][1][0], 2);
  BOOST_CHECK_EQUAL(b[0][1][1], 2);

  x.value(-1, b);
  BOOST_CHECK_EQUAL(b[0][0][0],-2);
  BOOST_CHECK_EQUAL(b[0][0][1],-2);
  BOOST_CHECK_EQUAL(b[0][1][0],-2);
  BOOST_CHECK_EQUAL(b[0][1][1],-2);

  x.plus(1, b);
  BOOST_CHECK_EQUAL(b[0][0][0], 0);
  BOOST_CHECK_EQUAL(b[0][0][1], 0);
  BOOST_CHECK_EQUAL(b[0][1][0], 0);
  BOOST_CHECK_EQUAL(b[0][1][1], 0);

  x.plus(-1, b);
  BOOST_CHECK_EQUAL(b[0][0][0],-2);
  BOOST_CHECK_EQUAL(b[0][0][1],-2);
  BOOST_CHECK_EQUAL(b[0][1][0],-2);
  BOOST_CHECK_EQUAL(b[0][1][1],-2);

  BOOST_CHECK_THROW( x.value(1, c), AssertionException );
  BOOST_CHECK_THROW( x.plus(1, c), AssertionException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VectorD_SparseVector_Static_Expression )
{
  typedef VectorD< SparseVector< DLA::VectorS<2, Real> > > VecorType;
  VecorType x1{{2}}, x2{{2}}, x3{{2}};

  x1 = 1;
  x2 = 2;

  x3 = -x1;
  BOOST_CHECK_EQUAL(x3[0][0][0],-1);
  BOOST_CHECK_EQUAL(x3[0][0][1],-1);
  BOOST_CHECK_EQUAL(x3[0][1][0],-1);
  BOOST_CHECK_EQUAL(x3[0][1][1],-1);

  x3 = -(-x1);
  BOOST_CHECK_EQUAL(x3[0][0][0], 1);
  BOOST_CHECK_EQUAL(x3[0][0][1], 1);
  BOOST_CHECK_EQUAL(x3[0][1][0], 1);
  BOOST_CHECK_EQUAL(x3[0][1][1], 1);

  x3 = x1 + x2;
  BOOST_CHECK_EQUAL(x3[0][0][0], 3);
  BOOST_CHECK_EQUAL(x3[0][0][1], 3);
  BOOST_CHECK_EQUAL(x3[0][1][0], 3);
  BOOST_CHECK_EQUAL(x3[0][1][1], 3);

  x3 = +(x1 + x2);
  BOOST_CHECK_EQUAL(x3[0][0][0], 3);
  BOOST_CHECK_EQUAL(x3[0][0][1], 3);
  BOOST_CHECK_EQUAL(x3[0][1][0], 3);
  BOOST_CHECK_EQUAL(x3[0][1][1], 3);

  x3 += x1 + x2;
  BOOST_CHECK_EQUAL(x3[0][0][0], 6);
  BOOST_CHECK_EQUAL(x3[0][0][1], 6);
  BOOST_CHECK_EQUAL(x3[0][1][0], 6);
  BOOST_CHECK_EQUAL(x3[0][1][1], 6);

  x3 -= x1 + x2;
  BOOST_CHECK_EQUAL(x3[0][0][0], 3);
  BOOST_CHECK_EQUAL(x3[0][0][1], 3);
  BOOST_CHECK_EQUAL(x3[0][1][0], 3);
  BOOST_CHECK_EQUAL(x3[0][1][1], 3);


  x3 = x1 - x2;
  BOOST_CHECK_EQUAL(x3[0][0][0],-1);
  BOOST_CHECK_EQUAL(x3[0][0][1],-1);
  BOOST_CHECK_EQUAL(x3[0][1][0],-1);
  BOOST_CHECK_EQUAL(x3[0][1][1],-1);

  x3 = +(x1 - x2);
  BOOST_CHECK_EQUAL(x3[0][0][0],-1);
  BOOST_CHECK_EQUAL(x3[0][0][1],-1);
  BOOST_CHECK_EQUAL(x3[0][1][0],-1);
  BOOST_CHECK_EQUAL(x3[0][1][1],-1);

  x3 += x1 - x2;
  BOOST_CHECK_EQUAL(x3[0][0][0],-2);
  BOOST_CHECK_EQUAL(x3[0][0][1],-2);
  BOOST_CHECK_EQUAL(x3[0][1][0],-2);
  BOOST_CHECK_EQUAL(x3[0][1][1],-2);

  x3 -= x1 - x2;
  BOOST_CHECK_EQUAL(x3[0][0][0],-1);
  BOOST_CHECK_EQUAL(x3[0][0][1],-1);
  BOOST_CHECK_EQUAL(x3[0][1][0],-1);
  BOOST_CHECK_EQUAL(x3[0][1][1],-1);


  x3 = 2*x1;
  BOOST_CHECK_EQUAL(x3[0][0][0], 2);
  BOOST_CHECK_EQUAL(x3[0][0][1], 2);
  BOOST_CHECK_EQUAL(x3[0][1][0], 2);
  BOOST_CHECK_EQUAL(x3[0][1][1], 2);

  x3 = +(2*x1);
  BOOST_CHECK_EQUAL(x3[0][0][0], 2);
  BOOST_CHECK_EQUAL(x3[0][0][1], 2);
  BOOST_CHECK_EQUAL(x3[0][1][0], 2);
  BOOST_CHECK_EQUAL(x3[0][1][1], 2);

  x3 = x1*2;
  BOOST_CHECK_EQUAL(x3[0][0][0], 2);
  BOOST_CHECK_EQUAL(x3[0][0][1], 2);
  BOOST_CHECK_EQUAL(x3[0][1][0], 2);
  BOOST_CHECK_EQUAL(x3[0][1][1], 2);

  x3 = x2/2;
  BOOST_CHECK_EQUAL(x3[0][0][0], 1);
  BOOST_CHECK_EQUAL(x3[0][0][1], 1);
  BOOST_CHECK_EQUAL(x3[0][1][0], 1);
  BOOST_CHECK_EQUAL(x3[0][1][1], 1);

  x3 = 2*x1*2;
  BOOST_CHECK_EQUAL(x3[0][0][0], 4);
  BOOST_CHECK_EQUAL(x3[0][0][1], 4);
  BOOST_CHECK_EQUAL(x3[0][1][0], 4);
  BOOST_CHECK_EQUAL(x3[0][1][1], 4);

  x3 = 2*(x1*2);
  BOOST_CHECK_EQUAL(x3[0][0][0], 4);
  BOOST_CHECK_EQUAL(x3[0][0][1], 4);
  BOOST_CHECK_EQUAL(x3[0][1][0], 4);
  BOOST_CHECK_EQUAL(x3[0][1][1], 4);

  x3 = (2*x1)/2;
  BOOST_CHECK_EQUAL(x3[0][0][0], 1);
  BOOST_CHECK_EQUAL(x3[0][0][1], 1);
  BOOST_CHECK_EQUAL(x3[0][1][0], 1);
  BOOST_CHECK_EQUAL(x3[0][1][1], 1);

  x3 += x1*2;
  BOOST_CHECK_EQUAL(x3[0][0][0], 3);
  BOOST_CHECK_EQUAL(x3[0][0][1], 3);
  BOOST_CHECK_EQUAL(x3[0][1][0], 3);
  BOOST_CHECK_EQUAL(x3[0][1][1], 3);

  x3 -= x1*2;
  BOOST_CHECK_EQUAL(x3[0][0][0], 1);
  BOOST_CHECK_EQUAL(x3[0][0][1], 1);
  BOOST_CHECK_EQUAL(x3[0][1][0], 1);
  BOOST_CHECK_EQUAL(x3[0][1][1], 1);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
