// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include "tools/SANSnumerics.h"

#include "../TriDiagPattern_btest.h"
#include "../Heat1D_btest.h"

#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_LinearSolver.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"

using namespace SANS;
using namespace SANS::SLA;


//############################################################################//
BOOST_AUTO_TEST_SUITE( SparseLinAlg )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LinearSolver_FGMRES )
{
  PyDict LinSolver, FGMRES, IdentityPrecond;

  IdentityPrecond[FGMRESParam::params.Preconditioner.Name] = FGMRESParam::params.Preconditioner.Identity;

  FGMRES[LinearSolverParam::params.LinearSolver.Solver] = LinearSolverParam::params.LinearSolver.FGMRES;
  FGMRES[FGMRESParam::params.nInner] = 10;
  FGMRES[FGMRESParam::params.nOuter] = 3;
  FGMRES[FGMRESParam::params.tol] = 1e-10;
  FGMRES[FGMRESParam::params.PrintCovergence] = false;
  FGMRES[FGMRESParam::params.Preconditioner] = IdentityPrecond;


  LinSolver[LinearSolverParam::params.LinearSolver] = FGMRES;

  LinearSolverParam::checkInputs(LinSolver);

  typedef SparseMatrix_CRS<Real> Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  Vector_type q(5);
  Heat1DEquationSet<Matrix_type> f(q);

  LinearSolver< Matrix_type > Solver(LinSolver, f);
  LinearSolver< Matrix_type > Solver2(LinSolver, f);

  Vector_type x(5), b(5), b2(5);

  //Create a residual vector
  b[0] = 0.5;
  b[1] = 1;
  b[2] = 2;
  b[3] = 1;
  b[4] = 0.5;

  //Solve the linear system. x here is the initial guess
  x = 1;
  Solver.solve(b, x);

  //Compute the residual vector from the solution
  b2 = Solver.A()*x;

  //The residuals should now be the same!
  for (int i = 0; i < b.m(); i++)
    BOOST_CHECK_CLOSE( b[i], b2[i], 1e-12 );

  //Temporary output just to check what is going on
  //for (int i = 0; i < b.m(); i++)
  //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;

  //The matrix 'solve' can be split into separate steps so that the same linear system can
  //be solved multiple times without invoking initialization repeatedly
  Solver2.factorize();

  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    //Solve the linear system (possibly multiple times). x here is the initial guess
    x = 1;
    Solver2.backsolve(b, x);

    //Compute the residual vector from the solution
    b2 = Solver2.A()*x;

    //The residuals should now be the same!
    for (int i = 0; i < b.m(); i++)
      BOOST_CHECK_CLOSE( b[i], b2[i], 1e-12 );
  }
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LinearSolver_FGMRES_Block )
{
  PyDict LinSolver, FGMRES, IdentityPrecond;

  IdentityPrecond[FGMRESParam::params.Preconditioner.Name] = FGMRESParam::params.Preconditioner.Identity;

  FGMRES[LinearSolverParam::params.LinearSolver.Solver] = LinearSolverParam::params.LinearSolver.FGMRES;
  FGMRES[FGMRESParam::params.nInner] = 10;
  FGMRES[FGMRESParam::params.nOuter] = 3;
  FGMRES[FGMRESParam::params.tol] = 1e-10;
  FGMRES[FGMRESParam::params.PrintCovergence] = false;
  FGMRES[FGMRESParam::params.Preconditioner] = IdentityPrecond;


  LinSolver[LinearSolverParam::params.LinearSolver] = FGMRES;

  LinearSolverParam::checkInputs(LinSolver);

  typedef DLA::MatrixD<Real> Block_type;
  typedef SparseMatrix_CRS< Block_type > Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  Vector_type q(5, 2);
  Heat1DEquationSet<Matrix_type> f(q);

  LinearSolver< Matrix_type > Solver(LinSolver, f);
  LinearSolver< Matrix_type > Solver2(LinSolver, f);

  Vector_type x(5,2), b(5,2), b2(5,2);

  //Create a residual vector
  b[0] = 0.5;
  b[1] = 1;
  b[2] = 2;
  b[3] = 1;
  b[4] = 0.5;

  //Solve the linear system. x here is the initial guess
  x = 1;
  Solver.solve(b, x);

  //Compute the residual vector from the solution
  b2 = Solver.A()*x;

  //The residuals should now be the same!
  for (int i = 0; i < b.m(); i++)
    for (int j = 0; j < b.block_m(i); j++)
      BOOST_CHECK_CLOSE( b[i][j], b2[i][j], 1e-12 );

  //Temporary output just to check what is going on
  //for (int i = 0; i < b.m(); i++)
  //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;

  //The matrix 'solve' can be split into separate steps so that the same linear system can
  //be solved multiple times without invoking initialization repeatedly
  Solver2.factorize();

  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    //Solve the linear system (possibly multiple times). x here is the initial guess
    x = 1;
    Solver2.backsolve(b, x);

    //Compute the residual vector from the solution
    b2 = A*x;

    //The residuals should now be the same!
    for (int i = 0; i < b.m(); i++)
      for (int j = 0; j < b.block_m(i); j++)
        BOOST_CHECK_CLOSE( b[i][j], b2[i][j], 1e-12 );
  }
}
#endif


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LinearSolver_UMFPACK )
{
  PyDict LinSolver, UMFPACK;

  UMFPACK[LinearSolverParam::params.LinearSolver.Solver] = LinearSolverParam::params.LinearSolver.UMFPACK;
  UMFPACK[UMFPACKParam::params.Timing] = true;

  LinSolver[LinearSolverParam::params.LinearSolver] = UMFPACK;

  LinearSolverParam::checkInputs(LinSolver);

  typedef SparseMatrix_CRS<Real> Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  const int nRow = 5;

  Vector_type q(nRow);
  Heat1DEquationSet<Matrix_type> f(q);

  LinearSolver< Matrix_type > Solver(LinSolver, f);
  LinearSolver< Matrix_type > Solver2(LinSolver, f);

  Vector_type x(nRow), b(nRow), b2(nRow);

  //Create a residual vector
  b[0] = 0.5;
  b[1] = 1;
  b[2] = 2;
  b[3] = 1;
  b[4] = 0.5;

  //Solve the linear system. x here is the initial guess
  x = 1;
  Solver.solve(b, x);

  //Compute the residual vector from the solution
  b2 = Solver.A()*x;

  //The residuals should now be the same!
  for (int i = 0; i < b.m(); i++)
    BOOST_CHECK_CLOSE( b[i], b2[i], 1e-12 );

  //Temporary output just to check what is going on
  //for (int i = 0; i < b.m(); i++)
  //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;

  //The matrix 'solve' can be split into separate steps so that the same linear system can
  //be solved multiple times without invoking initialization repeatedly
  Solver2.factorize();

  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    //Solve the linear system (possibly multiple times). x here is the initial guess
    x = 1;
    Solver2.backsolve(b, x);

    //Compute the residual vector from the solution
    b2 = Solver2.A()*x;

    //The residuals should now be the same!
    for (int i = 0; i < b.m(); i++)
      BOOST_CHECK_CLOSE( b[i], b2[i], 1e-12 );
  }
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LinearSolver_UMFPACK_Block )
{
  PyDict LinSolver, UMFPACK;

  UMFPACK[LinearSolverParam::params.LinearSolver.Solver] = LinearSolverParam::params.LinearSolver.UMFPACK;

  LinSolver[LinearSolverParam::params.LinearSolver] = UMFPACK;

  LinearSolverParam::checkInputs(LinSolver);

  typedef DLA::MatrixD<Real> Block_type;
  typedef SparseMatrix_CRS< Block_type > Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  LinearSolver< Matrix_type > Solver(LinSolver);

  SparseNonZeroPattern<Block_type> nz(5,5);
  TriDiagPattern(nz,2);

  Matrix_type A( nz );
  Vector_type x(5, 2), b(5, 2), b2(5, 2);

  //Initialize the matrix
  Heat1D<Block_type>::init(A);

  //Create a residual vector
  b[0] = 0.5;
  b[1] = 1;
  b[2] = 2;
  b[3] = 1;
  b[4] = 0.5;

  //Solve the linear system. x here is the initial guess
  x = 1;
  x = Solver.inverse(A)*b;

  //Compute the residual vector from the solution
  b2 = A*x;

  //The residuals should now be the same!
  for (int i = 0; i < b.m(); i++)
    for (int j = 0; j < b.block_m(i); j++)
      BOOST_CHECK_CLOSE( b[i][j], b2[i][j], 1e-12 );

  //Temporary output just to check what is going on
  //for (int i = 0; i < b.m(); i++)
  //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;

  //The matrix 'solve' can be split into separate steps so that the same linear system can
  //be solved multiple times without invoking initialization repeatedly
  Solver.factorize(A);

  //Solve the linear system (possibly multiple times). x here is the initial guess
  x = 1;
  x = Solver.backsolve(A)*b;

  //Compute the residual vector from the solution
  b2 = A*x;

  //The residuals should now be the same!
  for (int i = 0; i < b.m(); i++)
    for (int j = 0; j < b.block_m(i); j++)
      BOOST_CHECK_CLOSE( b[i][j], b2[i][j], 1e-12 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LinearSolver_UMFPACK_Block_Static )
{
  PyDict LinSolver, UMFPACK;

  UMFPACK[LinearSolverParam::params.LinearSolver.Solver] = LinearSolverParam::params.LinearSolver.UMFPACK;

  LinSolver[LinearSolverParam::params.LinearSolver] = UMFPACK;

  LinearSolverParam::checkInputs(LinSolver);

  typedef DLA::MatrixD< DLA::MatrixS<2,2,Real> > Block_type;
  typedef SparseMatrix_CRS<Block_type> Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  LinearSolver< Matrix_type > Solver(LinSolver);

  SparseNonZeroPattern<Block_type> nz(5,5);
  TriDiagPattern(nz,2);

  Matrix_type A( nz );
  Vector_type x(5, 2), b(5, 2), b2(5, 2);

  //Initialize the matrix
  Heat1D<Block_type>::init(A);

  //Create a residual vector
  b[0] = 0.5;
  b[1] = 1;
  b[2] = 2;
  b[3] = 1;
  b[4] = 0.5;

  //Solve the linear system. x here is the initial guess
  x = 1;
  x = Solver.inverse(A)*b;

  //Compute the residual vector from the solution
  b2 = A*x;

  //The residuals should now be the same!
  for (int i = 0; i < b.m(); i++)
    for (int j = 0; j < b.block_m(i); j++)
      for (int k = 0; k < 2; k++)
        BOOST_CHECK_CLOSE( b[i][j][k], b2[i][j][k], 1e-12 );

  //Temporary output just to check what is going on
  /*
  for (int i = 0; i < b.m(); i++)
    for (int j = 0; j < b.block_m(); j++)
      for (int k = 0; k < 2; k++)
        std::cout <<    "b[" << i << "][" << j << "][" << k << "] = " << b[i][j][k]
                  << "\tb2[" << i << "][" << j << "][" << k << "] = " << b2[i][j][k]
                  <<  "\tx[" << i << "][" << j << "][" << k << "] = " << x[i][j][k] << std::endl;
*/

  //The matrix 'solve' can be split into separate steps so that the same linear system can
  //be solved multiple times without invoking initialization repeatedly
  Solver.factorize(A);

  //Solve the linear system (possibly multiple times). x here is the initial guess
  x = 1;
  x = Solver.backsolve(A)*b;

  //Compute the residual vector from the solution
  b2 = A*x;

  //The residuals should now be the same!
  for (int i = 0; i < b.m(); i++)
    for (int j = 0; j < b.block_m(i); j++)
      for (int k = 0; k < 2; k++)
        BOOST_CHECK_CLOSE( b[i][j][k], b2[i][j][k], 1e-12 );

}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LinearSolver_UMFPACK_Static )
{
  PyDict LinSolver, UMFPACK;

  UMFPACK[LinearSolverParam::params.LinearSolver.Solver] = LinearSolverParam::params.LinearSolver.UMFPACK;

  LinSolver[LinearSolverParam::params.LinearSolver] = UMFPACK;

  LinearSolverParam::checkInputs(LinSolver);

  typedef DLA::MatrixS<2,2,Real> Block_type;
  typedef SparseMatrix_CRS< Block_type > Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  Vector_type q(5);
  Heat1DEquationSet<Matrix_type> f(q);

  LinearSolver< Matrix_type > Solver(LinSolver, f);
  LinearSolver< Matrix_type > Solver2(LinSolver, f);

  Vector_type x(5), b(5), b2(5);

  //Create a residual vector
  b[0] = 0.5;
  b[1] = 1;
  b[2] = 2;
  b[3] = 1;
  b[4] = 0.5;

  //Solve the linear system. x here is the initial guess
  x = 1;
  Solver.solve(b, x);

  //Compute the residual vector from the solution
  b2 = Solver.A()*x;

  //The residuals should now be the same!
  for (int i = 0; i < b.m(); i++)
    for (int j = 0; j < Matrix_type::Ttype::M; j++)
      BOOST_CHECK_CLOSE( b[i][j], b2[i][j], 1e-12 );

  //Temporary output just to check what is going on
  //for (int i = 0; i < b.m(); i++)
  //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;

  //The matrix 'solve' can be split into separate steps so that the same linear system can
  //be solved multiple times without invoking initialization repeatedly
  Solver2.factorize();

  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    //Solve the linear system (possibly multiple times). x here is the initial guess
    x = 1;
    Solver2.backsolve(b, x);

    //Compute the residual vector from the solution
    b2 = Solver2.A()*x;

    //The residuals should now be the same!
    for (int i = 0; i < b.m(); i++)
      for (int j = 0; j < Matrix_type::Ttype::M; j++)
        BOOST_CHECK_CLOSE( b[i][j], b2[i][j], 1e-12 );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LinearSolver_UMFPACK_Solve_MatrixD_CRS_Static )
{
  PyDict LinSolver, UMFPACK;

  UMFPACK[LinearSolverParam::params.LinearSolver.Solver] = LinearSolverParam::params.LinearSolver.UMFPACK;

  LinSolver[LinearSolverParam::params.LinearSolver] = UMFPACK;

  LinearSolverParam::checkInputs(LinSolver);

  typedef DLA::MatrixS<2,2,Real> Block_type;
  typedef DLA::MatrixD< SparseMatrix_CRS<Block_type> > Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  Vector_type q = {{5},{5}};
  Heat1DEquationSet<Matrix_type> f(q);

  LinearSolver< Matrix_type > Solver(LinSolver, f);
  LinearSolver< Matrix_type > Solver2(LinSolver, f);

  Vector_type x = {{5},{5}}, b = {{5},{5}}, b2 = {{5},{5}};

  //Create a residual vector
  for (int i = 0; i < 2; i++)
  {
    b[i][0] = 0.5;
    b[i][1] = 1;
    b[i][2] = 2;
    b[i][3] = 1;
    b[i][4] = 0.5;
  }

  //Solve the linear system. x here is the initial guess
  x = 1;
  Solver.solve(b, x);

  //Compute the residual vector from the solution
  b2 = Solver.A()*x;

  //The residuals should now be the same!
  for (int k = 0; k < b.m(); k++)
    for (int i = 0; i < b[k].m(); i++)
      for (int j = 0; j < 2; j++)
        BOOST_CHECK_CLOSE( b[k][i][j], b2[k][i][j], 1e-11 );

  //Temporary output just to check what is going on
  //for (int i = 0; i < b.size(); i++)
  //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;

  //The matrix 'solve' can be split into separate steps so that the same linear system can
  //be solved multiple times without invoking initialization repeatedly
  Solver2.factorize();

  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    //Solve the linear system (possibly multiple times). x here is the initial guess
    x = 1;
    Solver2.backsolve(b, x);

    //Compute the residual vector from the solution
    b2 = Solver2.A()*x;

    //The residuals should now be the same!
    for (int k = 0; k < b.m(); k++)
      for (int i = 0; i < b[k].m(); i++)
        for (int j = 0; j < 2; j++)
          BOOST_CHECK_CLOSE( b[k][i][j], b2[k][i][j], 1e-11 );
  }
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
