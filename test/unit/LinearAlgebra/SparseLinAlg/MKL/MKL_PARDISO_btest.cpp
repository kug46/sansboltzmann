// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "SANS_CHECK_CLOSE_btest.h"
#include "tools/SANSnumerics.h"

#include "../../TriDiagPattern_btest.h"
#include "../../Heat1D_btest.h"
#include "../../Advection1D_btest.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/Direct/MKL_PARDISO/MKL_PARDISOSolver.h"
#include "LinearAlgebra/SparseLinAlg/ScalarMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"
#include "LinearAlgebra/BlockLinAlg/BlockLinAlg_Mul.h"


using namespace SANS;
using namespace SANS::SLA;


//############################################################################//
BOOST_AUTO_TEST_SUITE( SparseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MKL_PARDISO_ctor )
{
  typedef SparseMatrix_CRS<Real> Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  PyDict d;

  MKL_PARDISOParam::checkInputs(d);

  Vector_type q(2);
  Heat1DEquationSet<Matrix_type> f(q);

  //This returns a shared pointer so no memory is lost
  MKL_PARDISOParam::newSolver< SparseMatrix_CRS<Real> >(d, f);
}


namespace CRS_Real
{
typedef SparseMatrix_CRS<Real> Matrix_type;
typedef VectorType<Matrix_type>::type Vector_type;

typedef boost::mpl::list< Heat1DEquationSet<Matrix_type>, Advection1DEquationSet<Matrix_type> > AlgEqSet_types;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MKL_PARDISO_Solve, AlgEqSet, AlgEqSet_types )
{

  PyDict d;

  MKL_PARDISOParam::checkInputs(d);

  Vector_type q(5);
  AlgEqSet f(q);

  MKL_PARDISO<Matrix_type> Solver(d, f);
  MKL_PARDISO<Matrix_type> Solver2(d, f);

  Vector_type x(5), b(5), b2(5);

  //Create a residual vector
  b[0] = 0.5;
  b[1] = 1;
  b[2] = 2;
  b[3] = 1;
  b[4] = 0.5;

  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    // solve the linear system.
    Solver.solve(b, x);

    //Compute the residual vector from the solution
    b2 = Solver.A()*x;

    //The residuals should now be the same!
    for (int i = 0; i < b.m(); i++)
      BOOST_CHECK_CLOSE( b[i], b2[i], 1e-12 );
  }

  // factorize the matrix
  Solver2.factorize();

  //Test solving the same matrix twice, the numeric factorization is only done once
  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    //Solve the linear system.
    Solver2.backsolve(b, x);

    //Compute the residual vector from the solution
    b2 = Solver2.A()*x;

    //The residuals should now be the same!
    for (int i = 0; i < b.m(); i++)
      BOOST_CHECK_CLOSE( b[i], b2[i], 1e-12 );

    //Temporary output just to check what is going on
    //for (int i = 0; i < b.size(); i++)
    //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MKL_PARDISO_Solve_Transpose, AlgEqSet, AlgEqSet_types )
{
  typedef NonZeroPatternType<Matrix_type>::type NonZeroPattern;

  PyDict d;

  MKL_PARDISOParam::checkInputs(d);

  Vector_type q(5);
  AlgEqSet f(q);

  // construct the transposed matrix
  NonZeroPattern nzt(f.matrixSize()); f.jacobianTranspose(nzt);
  Matrix_type At(nzt);                f.jacobianTranspose(At);

  MKL_PARDISO<Matrix_type> Solver(d, f, SLA::TransposeSolve);
  MKL_PARDISO<Matrix_type> Solver2(d, f, SLA::TransposeSolve);

  Vector_type x(5), b(5), b2(5);

  //Create a residual vector
  b[0] = 0.5;
  b[1] = 1;
  b[2] = 2;
  b[3] = 1;
  b[4] = 0.5;

  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    // solve the linear system.
    Solver.solve(b, x);

    //Compute the residual vector from the solution
    b2 = At*x;

    //The residuals should now be the same!
    for (int i = 0; i < b.m(); i++)
      BOOST_CHECK_CLOSE( b[i], b2[i], 1e-12 );
  }

  // factorize the matrix
  Solver2.factorize();

  //Test solving the same matrix twice, the numeric factorization is only done once
  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    //Solve the linear system.
    Solver2.backsolve(b, x);

    //Compute the residual vector from the solution
    b2 = At*x;

    //The residuals should now be the same!
    for (int i = 0; i < b.m(); i++)
      BOOST_CHECK_CLOSE( b[i], b2[i], 1e-12 );

    //Temporary output just to check what is going on
    //for (int i = 0; i < b.size(); i++)
    //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;
  }
}
}


#if 0
typedef boost::mpl::list< Heat1D< DLA::MatrixD<Real> >, Advection1D< DLA::MatrixD<Real> > > MatrixInit_Block_types;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MKL_PARDISO_Solve_Block, MatrixInit, MatrixInit_Block_types )
{
  PyDict d;

  MKL_PARDISOParam::checkInputs(d);

  typedef DLA::MatrixD<Real> Block_type;
  typedef SparseMatrix_CRS<Block_type> Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  MKL_PARDISO<Matrix_type> Solver(d);

  SparseNonZeroPattern<Block_type> nz(5,5);
  TriDiagPattern(nz,2);

  Matrix_type A( nz );
  Vector_type x(5, 2), b(5, 2), b2(5, 2);

  //Initialize the matrix here when it is a real matrix
  MatrixInit::init(A);

  //Create a residual vector
  b[0] = 0.5;
  b[1] = 1;
  b[2] = 2;
  b[3] = 1;
  b[4] = 0.5;

  //Set the matrix for the solver
  Solver.factorize(A);

  //Solve the linear system.
  x = Solver.backsolve(A)*b;

  //Compute the residual vector from the solution
  b2 = A*x;

  //The residuals should now be the same!
  for (int i = 0; i < b.m(); i++)
    for (int j = 0; j < b[i].m(); j++)
      BOOST_CHECK_CLOSE( b[i][j], b2[i][j], 1e-12 );

  //Temporary output just to check what is going on
  //for (int i = 0; i < b.size(); i++)
  //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;
}
#endif


namespace CRS_MatrixS
{
typedef DLA::MatrixS<2,2,Real> Static_type;
typedef Static_type Block_type;
typedef SparseMatrix_CRS<Block_type> Matrix_type;
typedef VectorType<Matrix_type>::type Vector_type;

typedef boost::mpl::list< Heat1DEquationSet<Matrix_type>, Advection1DEquationSet<Matrix_type> > AlgEqSet_types;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MKL_PARDISO_Solve_Static, AlgEqSet, AlgEqSet_types )
{
  PyDict d;

  MKL_PARDISOParam::checkInputs(d);

  Vector_type q(5);
  AlgEqSet f(q);

  MKL_PARDISO<Matrix_type> Solver(d, f);
  MKL_PARDISO<Matrix_type> Solver2(d, f);

  Vector_type x(5), b(5), b2(5);

  //Create a residual vector
  b[0] = 0.5;
  b[1] = 1;
  b[2] = 2;
  b[3] = 1;
  b[4] = 0.5;

  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    // solve the linear system.
    Solver.solve(b, x);

    //Compute the residual vector from the solution
    b2 = Solver.A()*x;

    //The residuals should now be the same!
    for (int i = 0; i < b.m(); i++)
      for (int j = 0; j < Block_type::M; j++)
        BOOST_CHECK_CLOSE( b[i][j], b2[i][j], 1e-12 );
  }

  // factorize the matrix
  Solver2.factorize();

  //Test solving the same matrix twice, the numeric factorization is only done once
  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    //Solve the linear system.
    Solver2.backsolve(b, x);

    //Compute the residual vector from the solution
    b2 = Solver2.A()*x;

    //The residuals should now be the same!
    for (int i = 0; i < b.m(); i++)
      for (int j = 0; j < Block_type::M; j++)
        BOOST_CHECK_CLOSE( b[i][j], b2[i][j], 1e-12 );

    //Temporary output just to check what is going on
    //for (int i = 0; i < b.size(); i++)
    //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MKL_PARDISO_Solve_Static_Transpose, AlgEqSet, AlgEqSet_types )
{
  typedef NonZeroPatternType<Matrix_type>::type NonZeroPattern;

  PyDict d;

  MKL_PARDISOParam::checkInputs(d);

  Vector_type q(5);
  AlgEqSet f(q);

  // construct the transposed matrix
  NonZeroPattern nzt(f.matrixSize()); f.jacobianTranspose(nzt);
  Matrix_type At(nzt);                f.jacobianTranspose(At);

  MKL_PARDISO<Matrix_type> Solver(d, f, SLA::TransposeSolve);
  MKL_PARDISO<Matrix_type> Solver2(d, f, SLA::TransposeSolve);

  Vector_type x(5), b(5), b2(5);

  //Create a residual vector
  b[0] = 0.5;
  b[1] = 1;
  b[2] = 2;
  b[3] = 1;
  b[4] = 0.5;

  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    // solve the linear system.
    Solver.solve(b, x);

    //Compute the residual vector from the solution
    b2 = At*x;

    //The residuals should now be the same!
    for (int i = 0; i < b.m(); i++)
      for (int j = 0; j < Block_type::M; j++)
        BOOST_CHECK_CLOSE( b[i][j], b2[i][j], 1e-12 );
  }

  // factorize the matrix
  Solver2.factorize();

  //Test solving the same matrix twice, the numeric factorization is only done once
  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    //Solve the linear system.
    Solver2.backsolve(b, x);

    //Compute the residual vector from the solution
    b2 = At*x;

    //The residuals should now be the same!
    for (int i = 0; i < b.m(); i++)
      for (int j = 0; j < Block_type::M; j++)
        BOOST_CHECK_CLOSE( b[i][j], b2[i][j], 1e-12 );

    //Temporary output just to check what is going on
    //for (int i = 0; i < b.size(); i++)
    //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;
  }
}
}

#if 0
typedef DLA::MatrixD< DLA::MatrixS<2,2,Real> > Block_Static_type;
typedef boost::mpl::list< Heat1D<Block_Static_type>, Advection1D<Block_Static_type> > MatrixInit_Block_Static_types;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MKL_PARDISO_Solve_Block_Static, MatrixInit, MatrixInit_Block_Static_types )
{
  PyDict d;

  MKL_PARDISOParam::checkInputs(d);

  typedef DLA::MatrixD< DLA::MatrixS<2,2,Real> > Block_type;
  typedef SparseMatrix_CRS<Block_type> Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  MKL_PARDISO<Matrix_type> Solver(d);

  SparseNonZeroPattern<Block_type> nz(5,5);
  TriDiagPattern(nz,2);

  Matrix_type A( nz );
  Vector_type x(5, 2), b(5, 2), b2(5, 2);

  //Initialize the matrix here when it is a real matrix
  MatrixInit::init(A);

  //Create a residual vector
  b[0] = 0.5;
  b[1] = 1;
  b[2] = 2;
  b[3] = 1;
  b[4] = 0.5;

  //Set the matrix for the solver
  Solver.factorize(A);

  //Solve the linear system.
  x = Solver.backsolve(A)*b;

  //Compute the residual vector from the solution
  b2 = A*x;

  //The residuals should now be the same!
  for (int i = 0; i < b.m(); i++)
    for (int j = 0; j < b.block_m(i); j++)
      for (int k = 0; k < 2; k++)
        BOOST_CHECK_CLOSE( b[i][j][k], b2[i][j][k], 1e-12 );

  //Temporary output just to check what is going on
  //for (int i = 0; i < b.size(); i++)
  //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MKL_PARDISO_Solve_MatrixD_CRS, MatrixInit, MatrixInit_types )
{
  PyDict d;

  MKL_PARDISOParam::checkInputs(d);

  typedef DLA::MatrixD< SparseMatrix_CRS<Real> > Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  MKL_PARDISO<Matrix_type> Solver(d);

  DLA::MatrixD< SparseNonZeroPattern<Real> > nz = {{ {5,5}, {5,5} },
                                                   { {5,5}, {5,5} }};

  Vector_type x = {{5},{5}}, b = {{5},{5}}, b2 = {{5},{5}};

  for (int i = 0; i < 2; i++)
    TriDiagPattern(nz(i,i));

  Matrix_type A( nz );

  //Initialize the matrix here when it is a real matrix
  for (int i = 0; i < 2; i++)
    MatrixInit::init(A(i,i));

  //Create a residual vector
  for (int i = 0; i < 2; i++)
  {
    b[i][0] = 0.5;
    b[i][1] = 1;
    b[i][2] = 2;
    b[i][3] = 1;
    b[i][4] = 0.5;
  }

  //Set the matrix for the solver
  Solver.factorize(A);

  //Solve the linear system.
  x = Solver.backsolve(A)*b;

  //Compute the residual vector from the solution
  b2 = A*x;

  //The residuals should now be the same!
  for (int i = 0; i < 2; i++)
    for (int j = 0; j < b[i].m(); j++)
      BOOST_CHECK_CLOSE( b[i][j], b2[i][j], 1e-12 );

  //Temporary output just to check what is going on
  //for (int i = 0; i < b.size(); i++)
  //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;


  //Create off diagonal sparse matrices
  nz(0,1).add(4,0);
  nz(1,0).add(0,4);

  Matrix_type A2( nz );

  //Initialize the matrix here when it is a real matrix
  for (int i = 0; i < 2; i++)
    MatrixInit::init(A2(i,i));

  A2(0,1).sparseRow(4,0) = -1;
  if (boost::is_same<Heat1D<Real>,MatrixInit>::value)
    A2(1,0).sparseRow(0,0) = -1;

  //Set the matrix for the solver
  Solver.factorize(A2);

  //Solve the linear system.
  x = Solver.backsolve(A2)*b;

  //Compute the residual vector from the solution
  b2 = A2*x;

  //The residuals should now be the same!
  for (int i = 0; i < 2; i++)
    for (int j = 0; j < b[i].m(); j++)
      BOOST_CHECK_CLOSE( b[i][j], b2[i][j], 2e-12 );

}
#endif


namespace MatrixD_CRS_MatrixS
{
typedef DLA::MatrixS<2,2,Real> Block_type;
typedef DLA::MatrixD< SparseMatrix_CRS<Block_type> > Matrix_type;
typedef VectorType<Matrix_type>::type Vector_type;

typedef boost::mpl::list< Heat1DEquationSet<Matrix_type>, Advection1DEquationSet<Matrix_type> > AlgEqSet_types;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MKL_PARDISO_Solve_MatrixD_CRS_Static, AlgEqSet, AlgEqSet_types )
{
  PyDict d;

  MKL_PARDISOParam::checkInputs(d);

  Vector_type q = {{5},{5}};
  AlgEqSet f(q);

  MKL_PARDISO<Matrix_type> Solver(d, f);
  MKL_PARDISO<Matrix_type> Solver2(d, f);

  Vector_type x = {{5},{5}}, b = {{5},{5}}, b2 = {{5},{5}};


  //Create a residual vector
  for (int i = 0; i < 2; i++)
  {
    b[i][0] = 0.5;
    b[i][1] = 1;
    b[i][2] = 2;
    b[i][3] = 1;
    b[i][4] = 0.5;
  }

  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    // solve the linear system.
    Solver.solve(b, x);

    //Compute the residual vector from the solution
    b2 = Solver.A()*x;

    //The residuals should now be the same!
    for (int k = 0; k < b.m(); k++)
      for (int i = 0; i < b[k].m(); i++)
        for (int j = 0; j < 2; j++)
          BOOST_CHECK_CLOSE( b[k][i][j], b2[k][i][j], 1e-11 );
  }

#if 0
  //Temporary output just to check what is going on
  for (int k = 0; k < b.m(); k++)
    for (int i = 0; i < b[k].m(); i++)
      for (int j = 0; j < 2; j++)
        std::cout << "b[" << k << "][" << i << "][" << j << "] = " << b[k][i][j]
                  << "\t\tb2[" << k << "][" << i << "][" << j << "] = " << b2[k][i][j]
                  << "\t\tx[" << k << "][" << i << "][" << j << "] = "  << x[k][i][j] << std::endl;
#endif

  // factorize the matrix
  Solver2.factorize();

  //Test solving the same matrix twice, the numeric factorization is only done once
  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    //Solve the linear system.
    Solver2.backsolve(b, x);

    //Compute the residual vector from the solution
    b2 = Solver2.A()*x;

    //The residuals should now be the same!
    for (int k = 0; k < b.m(); k++)
      for (int i = 0; i < b[k].m(); i++)
        for (int j = 0; j < 2; j++)
          BOOST_CHECK_CLOSE( b[k][i][j], b2[k][i][j], 1e-11 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MKL_PARDISO_Solve_MatrixD_CRS_Static_Transpose, AlgEqSet, AlgEqSet_types )
{
  typedef NonZeroPatternType<Matrix_type>::type NonZeroPattern;

  PyDict d;

  MKL_PARDISOParam::checkInputs(d);

  Vector_type q = {{5},{5}};
  AlgEqSet f(q);

  // construct the transposed matrix
  NonZeroPattern nzt(f.matrixSize()); f.jacobianTranspose(nzt);
  Matrix_type At(nzt);                f.jacobianTranspose(At);

  MKL_PARDISO<Matrix_type> Solver(d, f, SLA::TransposeSolve);
  MKL_PARDISO<Matrix_type> Solver2(d, f, SLA::TransposeSolve);

  Vector_type x = {{5},{5}}, b = {{5},{5}}, b2 = {{5},{5}};


  //Create a residual vector
  for (int i = 0; i < 2; i++)
  {
    b[i][0] = 0.5;
    b[i][1] = 1;
    b[i][2] = 2;
    b[i][3] = 1;
    b[i][4] = 0.5;
  }

  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    // solve the linear system.
    Solver.solve(b, x);

    //Compute the residual vector from the solution
    b2 = At*x;

    //The residuals should now be the same!
    for (int k = 0; k < b.m(); k++)
      for (int i = 0; i < b[k].m(); i++)
        for (int j = 0; j < 2; j++)
          BOOST_CHECK_CLOSE( b[k][i][j], b2[k][i][j], 1e-11 );
  }

#if 0
  //Temporary output just to check what is going on
  for (int k = 0; k < b.m(); k++)
    for (int i = 0; i < b[k].m(); i++)
      for (int j = 0; j < 2; j++)
        std::cout << "b[" << k << "][" << i << "][" << j << "] = " << b[k][i][j]
                  << "\t\tb2[" << k << "][" << i << "][" << j << "] = " << b2[k][i][j]
                  << "\t\tx[" << k << "][" << i << "][" << j << "] = "  << x[k][i][j] << std::endl;
#endif

  // factorize the matrix
  Solver2.factorize();

  //Test solving the same matrix twice, the numeric factorization is only done once
  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    //Solve the linear system.
    Solver2.backsolve(b, x);

    //Compute the residual vector from the solution
    b2 = At*x;

    //The residuals should now be the same!
    for (int k = 0; k < b.m(); k++)
      for (int i = 0; i < b[k].m(); i++)
        for (int j = 0; j < 2; j++)
          BOOST_CHECK_CLOSE( b[k][i][j], b2[k][i][j], 1e-11 );
  }
}
}


#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MKL_PARDISO_Solve_MatrixD_CRS_Block, MatrixInit, MatrixInit_Block_types )
{
  PyDict d;

  MKL_PARDISOParam::checkInputs(d);

  typedef DLA::MatrixD<Real> Block_type;
  typedef DLA::MatrixD< SparseMatrix_CRS<Block_type> > Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  MKL_PARDISO<Matrix_type> Solver(d);

  DLA::MatrixD< SparseNonZeroPattern<Block_type> > nz = {{ {5,5}, {5,5} },
                                                         { {5,5}, {5,5} }};

  Vector_type x = {{5,2},{5,2}}, b = {{5,2},{5,2}}, b2 = {{5,2},{5,2}};

  for (int i = 0; i < 2; i++)
    TriDiagPattern(nz(i,i),2);

  Matrix_type A( nz );

  //Initialize the matrix here when it is a real matrix
  for (int i = 0; i < 2; i++)
    MatrixInit::init(A(i,i));

  //Set the matrix for the solver
  Solver.factorize(A);

  //Create a residual vector
  for (int i = 0; i < 2; i++)
  {
    b[i][0] = 0.5;
    b[i][1] = 1;
    b[i][2] = 2;
    b[i][3] = 1;
    b[i][4] = 0.5;
  }

  //Solve the linear system.
  x = Solver.backsolve(A)*b;

  //Compute the residual vector from the solution
  b2 = A*x;

  //The residuals should now be the same!
  for (int k = 0; k < b.m(); k++)
    for (int i = 0; i < b[k].m(); i++)
      for (int j = 0; j < b[k][i].m(); j++)
        BOOST_CHECK_CLOSE( b[k][i][j], b2[k][i][j], 1e-12 );

  //Temporary output just to check what is going on
  //for (int i = 0; i < b.size(); i++)
  //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;


  //Create the off diagonal sparse matrices
  nz(0,1).add(4,0,2,2);
  nz(1,0).add(0,4,2,2);

  Matrix_type A2( nz );

  //Initialize the matrix here when it is a real matrix
  for (int i = 0; i < 2; i++)
    MatrixInit::init(A2(i,i));

  A2(0,1).sparseRow(4,0) = -1;
  if (boost::is_same<Heat1D<Real>,MatrixInit>::value)
    A2(1,0).sparseRow(0,0) = -1;

  //Set the matrix for the solver
  Solver.factorize(A2);

  //Solve the linear system.
  x = Solver.backsolve(A2)*b;

  //Compute the residual vector from the solution
  b2 = A2*x;

  //The residuals should now be the same!
  for (int k = 0; k < b.m(); k++)
    for (int i = 0; i < b[k].m(); i++)
      for (int j = 0; j < b[k][i].m(); j++)
        BOOST_CHECK_CLOSE( b[k][i][j], b2[k][i][j], 2e-12 );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( MKL_PARDISO_Solve_MatrixD_CRS_Block_Static, MatrixInit, MatrixInit_Block_Static_types )
{
  PyDict d;

  MKL_PARDISOParam::checkInputs(d);

  typedef DLA::MatrixD< DLA::MatrixS<2,2,Real> > Block_type;
  typedef DLA::MatrixD< SparseMatrix_CRS<Block_type> > Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  MKL_PARDISO<Matrix_type> Solver(d);

  DLA::MatrixD< SparseNonZeroPattern<Block_type> > nz = {{ {5,5}, {5,5} },
                                                         { {5,5}, {5,5} }};

  Vector_type x = {{5,2},{5,2}}, b = {{5,2},{5,2}}, b2 = {{5,2},{5,2}};

  for (int i = 0; i < 2; i++)
    TriDiagPattern(nz(i,i),2);

  Matrix_type A( nz );

  //Initialize the matrix here when it is a real matrix
  for (int i = 0; i < 2; i++)
    MatrixInit::init(A(i,i));

  //Create a residual vector
  for (int i = 0; i < 2; i++)
  {
    b[i][0] = 0.5;
    b[i][1] = 1;
    b[i][2] = 2;
    b[i][3] = 1;
    b[i][4] = 0.5;
  }

  //Set the matrix for the solver
  Solver.factorize(A);

  //Solve the linear system.
  x = Solver.backsolve(A)*b;

  //Compute the residual vector from the solution
  b2 = A*x;

  //The residuals should now be the same!
  for (int jd = 0; jd < b.m(); jd++)
    for (int k = 0; k < b[jd].m(); k++)
      for (int i = 0; i < b[jd][k].m(); i++)
        for (int j = 0; j < 2; j++)
          BOOST_CHECK_CLOSE( b[jd][k][i][j], b2[jd][k][i][j], 1e-12 );

  //Temporary output just to check what is going on
  //for (int i = 0; i < b.size(); i++)
  //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;


  //Create the off diagonal sparse matrices
  nz(0,1).add(4,0,2,2);
  nz(1,0).add(0,4,2,2);

  Matrix_type A2( nz );

  //Initialize the matrix here when it is a real matrix
  for (int i = 0; i < 2; i++)
    MatrixInit::init(A2(i,i));

  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      A2(0,1).sparseRow(4,0)(i,i)(j,j) = -1;
      if (boost::is_same<Heat1D<Block_type>,MatrixInit>::value)
        A2(1,0).sparseRow(0,0)(i,i)(j,j) = -1;
    }

  //Set the matrix for the solver
  Solver.factorize(A2);

  //Solve the linear system.
  x = Solver.backsolve(A2)*b;

  //Compute the residual vector from the solution
  b2 = A2*x;

  //The residuals should now be the same!
  for (int jd = 0; jd < b.m(); jd++)
    for (int k = 0; k < b[jd].m(); k++)
      for (int i = 0; i < b[jd][k].m(); i++)
        for (int j = 0; j < 2; j++)
          BOOST_CHECK_CLOSE( b[jd][k][i][j], b2[jd][k][i][j], 2e-12 );

}
#endif


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MKL_PARDISO_Solve_MatrixBlock_Real_2x2_test )
{
  PyDict d;

  MKL_PARDISOParam::checkInputs(d);

  typedef DLA::MatrixD<SparseMatrix_CRS<Real>> M00;
  typedef DLA::MatrixD<SparseMatrix_CRS<Real>> M01;
  typedef DLA::MatrixD<SparseMatrix_CRS<Real>> M10;
  typedef DLA::MatrixD<SparseMatrix_CRS<Real>> M11;

  typedef BLA::MatrixBlock_2x2<M00, M01,
                               M10, M11 > Matrix_type;

  typedef VectorType<Matrix_type>::type Vector_type;

  const int m = 3;
  const int n = 4;

  VectorType<M00>::type q0({ SparseVectorSize(m) });
  Heat1DEquationSet<M00> f0(q0);

  VectorType<M11>::type q1({ SparseVectorSize(n) });
  Heat1DEquationSet<M11> f1(q1);


  Vector_type q( { SparseVectorSize(m) },
                 { SparseVectorSize(n) } );

  Heat1DEquationSet_Block2x2< M00, M01,
                              M10, M11 > f(f0, f1, q);

  MKL_PARDISO<Matrix_type> Solver(d, f);
  MKL_PARDISO<Matrix_type> Solver2(d, f);

  Vector_type x( { SparseVectorSize(m) },
                 { SparseVectorSize(n) } );

  Vector_type b( { SparseVectorSize(m) },
                 { SparseVectorSize(n) } );

  Vector_type b2( { SparseVectorSize(m) },
                  { SparseVectorSize(n) } );

#if 0
  ScalarMatrix_CRS<int> a(A);
  std::fstream file("tmp/block_2x2.mtx", std::ios::out);
  a.WriteMatrixMarketFile(file);
#endif

  //Create a residual vector
  b = 4;
  b.v0[0][2] = 2;
  b.v1 = 3;
  b.v1[0][1] = 1;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    //Solve the linear system.
    Solver.solve(b, x);

    //Compute the residual vector from the solution
    b2 = Solver.A()*x;

    //The residuals should now be the same!
    for ( int k = 0; k < b.v0.m(); k++ )
      for ( int j = 0; j < b.v0[k].m(); j++ )
        SANS_CHECK_CLOSE(b2.v0[k][j], b.v0[k][j], small_tol, close_tol);

    for ( int k = 0; k < b.v1.m(); k++ )
      for ( int j = 0; j < b.v1[k].m(); j++ )
        SANS_CHECK_CLOSE(b2.v1[k][j], b.v1[k][j], small_tol, close_tol);
  }

  //Factorize the matrix
  Solver2.factorize();

  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    //Solve the linear system.
    Solver2.backsolve(b, x);

    //Compute the residual vector from the solution
    b2 = Solver2.A()*x;

    //The residuals should now be the same!
    for ( int k = 0; k < b.v0.m(); k++ )
      for ( int j = 0; j < b.v0[k].m(); j++ )
        SANS_CHECK_CLOSE(b2.v0[k][j], b.v0[k][j], small_tol, close_tol);

    for ( int k = 0; k < b.v1.m(); k++ )
      for ( int j = 0; j < b.v1[k].m(); j++ )
        SANS_CHECK_CLOSE(b2.v1[k][j], b.v1[k][j], small_tol, close_tol);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MKL_PARDISO_Solve_MatrixBlock_2x2_MatrixAndReal_test )
{
  PyDict d;

  MKL_PARDISOParam::checkInputs(d);

  static const int M = 2;
  static const int N = 2;

  typedef DLA::MatrixD<SparseMatrix_CRS<DLA::MatrixS<M,N,Real>>> M00;
  typedef DLA::MatrixD<SparseMatrix_CRS<DLA::MatrixS<M,1,Real>>> M01;

  typedef DLA::MatrixD<SparseMatrix_CRS<DLA::MatrixS<1,N,Real>>> M10;
  typedef DLA::MatrixD<SparseMatrix_CRS<Real>> M11;

  typedef BLA::MatrixBlock_2x2<M00, M01,
                               M10, M11 > Matrix_type;

  typedef VectorType<Matrix_type>::type Vector_type;

  const int m = 3;
  const int n = 4;

  VectorType<M00>::type q0({ SparseVectorSize(m) });
  Heat1DEquationSet<M00> f0(q0);

  VectorType<M11>::type q1({ SparseVectorSize(n) });
  Heat1DEquationSet<M11> f1(q1);


  Vector_type q( { SparseVectorSize(m) },
                 { SparseVectorSize(n) } );

  Heat1DEquationSet_Block2x2< M00, M01,
                              M10, M11 > f(f0, f1, q);

  MKL_PARDISO<Matrix_type> Solver(d, f);
  MKL_PARDISO<Matrix_type> Solver2(d, f);

  Vector_type x( { SparseVectorSize(m) },
                 { SparseVectorSize(n) } );

  Vector_type b( { SparseVectorSize(m) },
                 { SparseVectorSize(n) } );

  Vector_type b2( { SparseVectorSize(m) },
                  { SparseVectorSize(n) } );

#if 0
  ScalarMatrix_CRS<int> a(A);
  std::fstream file("tmp/block_2x2.mtx", std::ios::out);
  a.WriteMatrixMarketFile(file);
#endif

  //Create a residual vector
  b = 4;
  b.v0[0][2] = 2;
  b.v1 = 3;
  b.v1[0][1] = 1;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    //Solve the linear system.
    Solver.solve(b, x);

    //Compute the residual vector from the solution
    b2 = Solver.A()*x;

    //The residuals should now be the same!
    for ( int k = 0; k < b.v0.m(); k++ )
      for ( int j = 0; j < b.v0[k].m(); j++ )
        for ( int i = 0; i < N; i++ )
          SANS_CHECK_CLOSE(b2.v0[k][j][i], b.v0[k][j][i], small_tol, close_tol);

    for ( int k = 0; k < b.v1.m(); k++ )
      for ( int j = 0; j < b.v1[k].m(); j++ )
        SANS_CHECK_CLOSE(b2.v1[k][j], b.v1[k][j], small_tol, close_tol);
  }

  //Factorize the matrix
  Solver2.factorize();

  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    //Solve the linear system.
    Solver2.backsolve(b, x);

    //Compute the residual vector from the solution
    b2 = Solver2.A()*x;

    //The residuals should now be the same!
    for ( int k = 0; k < b.v0.m(); k++ )
      for ( int j = 0; j < b.v0[k].m(); j++ )
        for ( int i = 0; i < N; i++ )
          SANS_CHECK_CLOSE(b2.v0[k][j][i], b.v0[k][j][i], small_tol, close_tol);

    for ( int k = 0; k < b.v1.m(); k++ )
      for ( int j = 0; j < b.v1[k].m(); j++ )
        SANS_CHECK_CLOSE(b2.v1[k][j], b.v1[k][j], small_tol, close_tol);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MKL_PARDISO_Solve_MatrixBlock_2x2_Matrices_test )
{
  PyDict d;

  MKL_PARDISOParam::checkInputs(d);

  static const int M0 = 8;
  static const int M1 = 2;

  typedef DLA::MatrixD<SparseMatrix_CRS<DLA::MatrixS<M0,M0,Real>>> M00;
  typedef DLA::MatrixD<SparseMatrix_CRS<DLA::MatrixS<M0,M1,Real>>> M01;

  typedef DLA::MatrixD<SparseMatrix_CRS<DLA::MatrixS<M1,M0,Real>>> M10;
  typedef DLA::MatrixD<SparseMatrix_CRS<DLA::MatrixS<M1,M1,Real>>> M11;

  typedef BLA::MatrixBlock_2x2<M00, M01,
                               M10, M11 > Matrix_type;

  typedef VectorType<Matrix_type>::type Vector_type;

  const int m = 3;
  const int n = 4;

  VectorType<M00>::type q0({ SparseVectorSize(m) });
  Heat1DEquationSet<M00> f0(q0);

  VectorType<M11>::type q1({ SparseVectorSize(n) });
  Heat1DEquationSet<M11> f1(q1);


  Vector_type q( { SparseVectorSize(m) },
                 { SparseVectorSize(n) } );

  Heat1DEquationSet_Block2x2< M00, M01,
                              M10, M11 > f(f0, f1, q);

  MKL_PARDISO<Matrix_type> Solver(d, f);
  MKL_PARDISO<Matrix_type> Solver2(d, f);

  Vector_type x( { SparseVectorSize(m) },
                 { SparseVectorSize(n) } );

  Vector_type b( { SparseVectorSize(m) },
                 { SparseVectorSize(n) } );

  Vector_type b2( { SparseVectorSize(m) },
                  { SparseVectorSize(n) } );

#if 0
  ScalarMatrix_CRS<int> a(A);
  std::fstream file("tmp/block_2x2.mtx", std::ios::out);
  a.WriteMatrixMarketFile(file);
#endif

  //Create a residual vector
  b = 4;
  b.v0[0][2] = 2;
  b.v1 = 3;
  b.v1[0][1] = 1;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    //Solve the linear system.
    Solver.solve(b, x);

    //Compute the residual vector from the solution
    b2 = Solver.A()*x;

    //The residuals should now be the same!
    for ( int k = 0; k < b.v0.m(); k++ )
      for ( int j = 0; j < b.v0[k].m(); j++ )
        for ( int i = 0; i < M0; i++ )
          SANS_CHECK_CLOSE(b2.v0[k][j][i], b.v0[k][j][i], small_tol, close_tol);

    for ( int k = 0; k < b.v1.m(); k++ )
      for ( int j = 0; j < b.v1[k].m(); j++ )
        for ( int i = 0; i < M1; i++ )
          SANS_CHECK_CLOSE(b2.v1[k][j][i], b.v1[k][j][i], small_tol, close_tol);
  }

  //Factorize the matrix
  Solver2.factorize();

  for (int nsolve = 0; nsolve < 2; nsolve++)
  {
    //Solve the linear system.
    Solver2.backsolve(b, x);

    //Compute the residual vector from the solution
    b2 = Solver2.A()*x;

    //The residuals should now be the same!
    for ( int k = 0; k < b.v0.m(); k++ )
      for ( int j = 0; j < b.v0[k].m(); j++ )
        for ( int i = 0; i < M0; i++ )
          SANS_CHECK_CLOSE(b2.v0[k][j][i], b.v0[k][j][i], small_tol, close_tol);

    for ( int k = 0; k < b.v1.m(); k++ )
      for ( int j = 0; j < b.v1[k].m(); j++ )
        for ( int i = 0; i < M1; i++ )
          SANS_CHECK_CLOSE(b2.v1[k][j][i], b.v1[k][j][i], small_tol, close_tol);
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
