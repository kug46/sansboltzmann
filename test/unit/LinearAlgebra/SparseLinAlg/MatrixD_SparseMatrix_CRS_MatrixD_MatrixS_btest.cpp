// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include "tools/SANSnumerics.h"
#include "tools/minmax.h"

#include "../TriDiagPattern_btest.h"
#include "../Heat1D_btest.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Add.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "unit/LinearAlgebra/DenseLinAlg/chkMatrixS_btest.h"

using namespace SANS::SLA;
using namespace SANS;

//Explicitly instantiate templates to get correct coverage information
namespace SANS
{
namespace SLA
{

}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( SparseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SparseMatrix_CRS_Block_Static_ctor )
{
  const int m = 5;
  typedef DLA::MatrixD< DLA::MatrixS<2,2,Real> > Block_type;
  typedef DLA::MatrixD< SparseMatrix_CRS< Block_type > > Matrix_type;
  DLA::MatrixD< SparseNonZeroPattern<Block_type> > nz = {{ {m,m} }};
  TriDiagPattern(nz(0,0),2);
  Matrix_type A( nz );

  const Matrix_type& cA = A;

  //Check that all tri-diag mappings are correct

  BOOST_CHECK_EQUAL(A(0,0).getNumNonZero(), 3*(m-2) + 4 );
  for ( int i = 0; i < A(0,0).m(); i++ )
    for ( int k = 0; k < A(0,0).rowNonZero(i); k++)
      for ( int bi = 0; bi < 2; bi++ )
        for ( int bj = 0; bj < 2; bj++ )
          BOOST_CHECK( chkMatrixS22( A(0,0).sparseRow(i,k)(bi,bj), 0, 0, 0, 0) );

  int *row_ptr = A(0,0).get_row_ptr();
  int *col_ind = A(0,0).get_col_ind();

  BOOST_CHECK_EQUAL( A(0,0).get_values(), &A(0,0)[0](0,0));

  //First row in the matrix
  BOOST_CHECK_EQUAL(row_ptr[0], 0);
  BOOST_CHECK_EQUAL(row_ptr[1], 2);
  BOOST_CHECK_EQUAL(col_ind[0], 0);
  BOOST_CHECK_EQUAL(col_ind[1], 1);

  //All the entries on the interior rows
  for ( int i = 0; i < m-2; i++ )
  {
    BOOST_CHECK_EQUAL(row_ptr[i+2], 3*i + 3 + 2);
    BOOST_CHECK_EQUAL(col_ind[3*i + 0 + 2], i + 0);
    BOOST_CHECK_EQUAL(col_ind[3*i + 1 + 2], i + 1);
    BOOST_CHECK_EQUAL(col_ind[3*i + 2 + 2], i + 2);
  }

  //Last row in the matrix
  BOOST_CHECK_EQUAL(row_ptr[m], 3*(m-2) + 4);
  BOOST_CHECK_EQUAL(col_ind[3*(m-2) + 2], (m-2) + 0);
  BOOST_CHECK_EQUAL(col_ind[3*(m-2) + 3], (m-2) + 1);

  //Check that the row_ptr and col_ind work the way they are intended to be used
  for ( int i = 0; i < m; i++ )
    for ( int k = row_ptr[i]; k < row_ptr[i+1]; k++)
    {
      for ( int bi = 0; bi < 2; bi++ )
        for ( int bj = 0; bj < 2; bj++ )
        {
          BOOST_CHECK( chkMatrixS22(A(0,0)[k](bi,bj), 0, 0, 0, 0) );
          BOOST_CHECK( chkMatrixS22(cA(0,0)[k](bi,bj), 0, 0, 0, 0) );
        }

      BOOST_CHECK_EQUAL(col_ind[k], max(i-1,0) + k - row_ptr[i]);
    }


  //Initialize as a '1D heat equation' matrix and check that is correct
  Heat1D< Block_type >::init(A(0,0));

  BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(0,0)(0,0),  2, 0, 0, 2) );
  BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(0,0)(0,1),  0, 0, 0, 0) );
  BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(0,0)(1,0),  0, 0, 0, 0) );
  BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(0,0)(1,1),  2, 0, 0, 2) );

  BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(0,1)(0,0), -1, 0, 0, -1) );
  BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(0,1)(0,1),  0, 0, 0,  0) );
  BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(0,1)(1,0),  0, 0, 0,  0) );
  BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(0,1)(1,1), -1, 0, 0, -1) );

  for ( int i = 1; i < cA(0,0).m()-1; i++ )
  {
    BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(i,0)(0,0), -1, 0, 0, -1) );
    BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(i,0)(0,1),  0, 0, 0,  0) );
    BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(i,0)(1,0),  0, 0, 0,  0) );
    BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(i,0)(1,1), -1, 0, 0, -1) );

    BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(i,1)(0,0),  2, 0, 0, 2) );
    BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(i,1)(0,1),  0, 0, 0, 0) );
    BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(i,1)(1,0),  0, 0, 0, 0) );
    BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(i,1)(1,1),  2, 0, 0, 2) );

    BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(i,2)(0,0), -1, 0, 0, -1) );
    BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(i,2)(0,1),  0, 0, 0,  0) );
    BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(i,2)(1,0),  0, 0, 0,  0) );
    BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(i,2)(1,1), -1, 0, 0, -1) );
  }
  BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(A(0,0).m()-1,0)(0,0), -1, 0, 0, -1) );
  BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(A(0,0).m()-1,0)(0,1),  0, 0, 0,  0) );
  BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(A(0,0).m()-1,0)(1,0),  0, 0, 0,  0) );
  BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(A(0,0).m()-1,0)(1,1), -1, 0, 0, -1) );

  BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(A(0,0).m()-1,1)(0,0),  2, 0, 0, 2) );
  BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(A(0,0).m()-1,1)(0,1),  0, 0, 0, 0) );
  BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(A(0,0).m()-1,1)(1,0),  0, 0, 0, 0) );
  BOOST_CHECK( chkMatrixS22(cA(0,0).sparseRow(A(0,0).m()-1,1)(1,1),  2, 0, 0, 2) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SparseMatrix_CRS_Empty_Block_Static_MulVec )
{
  typedef DLA::VectorD< DLA::VectorS<2,Real> > Vec_type;
  typedef DLA::MatrixD< DLA::MatrixS<2,2,Real> > Block_type;

  DLA::VectorD< SparseVector<Vec_type> > x = { {5,2},
                                               {5,2} };
  DLA::VectorD< SparseVector<Vec_type> > b = { {5,2},
                                               {5,2} };

  //Create a non-zero pattern, but don't fill it with anything
  DLA::MatrixD< SparseNonZeroPattern<Block_type> > nz = {{ {5,5}, {5,5} },
                                                         { {5,5}, {5,5} }};

  //Create an empty matrix, which acts like a 'zero' matrix
  DLA::MatrixD< SparseMatrix_CRS< Block_type > > A( nz );

  x = 1;
  x[0][2] = 2;

  b = A*x;
  for ( int jd = 0; jd < b.m(); jd++)
    for ( int j = 0; j < b[jd].m(); j++)
      for ( int i = 0; i < 2; i++ )
        for ( int k = 0; k < 2; k++ )
          BOOST_CHECK_EQUAL(b[jd][j][i][k], 0);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SparseMatrix_CRS_Block_Static_MulVec )
{
  typedef DLA::VectorD< DLA::VectorS<2,Real> > Vec_type;
  typedef DLA::MatrixD< DLA::MatrixS<2,2,Real> > Block_type;

  DLA::VectorD< SparseVector<Vec_type> > x = { {5,2},
                                               {5,2} };
  DLA::VectorD< SparseVector<Vec_type> > b = { {5,2},
                                               {5,2} };
  DLA::VectorD< SparseVector<Vec_type> > c = { {5,2},
                                               {5,1} }; //<- Wrong size on purpose
  DLA::MatrixD< SparseNonZeroPattern<Block_type> > nz = {{ {5,5}, {5,5} },
                                                         { {5,5}, {5,5} }};

  for (int i = 0; i < 2; i++)
    TriDiagPattern( nz(i,i),2 );

  DLA::MatrixD< SparseMatrix_CRS<Block_type> > A(nz);

  for (int i = 0; i < 2; i++)
    Heat1D<Block_type>::init(A(i,i));

  x = 1;
  x[0][2] = 2;
  x[1][2] = 2;

  b = A*x;
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(b[k][0][i][j], 1);
        BOOST_CHECK_EQUAL(b[k][1][i][j],-1);
        BOOST_CHECK_EQUAL(b[k][2][i][j], 2);
        BOOST_CHECK_EQUAL(b[k][3][i][j],-1);
        BOOST_CHECK_EQUAL(b[k][4][i][j], 1);
      }

  b = +(A*x);
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(b[k][0][i][j], 1);
        BOOST_CHECK_EQUAL(b[k][1][i][j],-1);
        BOOST_CHECK_EQUAL(b[k][2][i][j], 2);
        BOOST_CHECK_EQUAL(b[k][3][i][j],-1);
        BOOST_CHECK_EQUAL(b[k][4][i][j], 1);
      }

  b += A*x;
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(b[k][0][i][j], 2);
        BOOST_CHECK_EQUAL(b[k][1][i][j],-2);
        BOOST_CHECK_EQUAL(b[k][2][i][j], 4);
        BOOST_CHECK_EQUAL(b[k][3][i][j],-2);
        BOOST_CHECK_EQUAL(b[k][4][i][j], 2);
      }


  b = 2*(A*x);
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(b[k][0][i][j], 2);
        BOOST_CHECK_EQUAL(b[k][1][i][j],-2);
        BOOST_CHECK_EQUAL(b[k][2][i][j], 4);
        BOOST_CHECK_EQUAL(b[k][3][i][j],-2);
        BOOST_CHECK_EQUAL(b[k][4][i][j], 2);
      }

  b = +(2*(A*x));
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(b[k][0][i][j], 2);
        BOOST_CHECK_EQUAL(b[k][1][i][j],-2);
        BOOST_CHECK_EQUAL(b[k][2][i][j], 4);
        BOOST_CHECK_EQUAL(b[k][3][i][j],-2);
        BOOST_CHECK_EQUAL(b[k][4][i][j], 2);
      }

  b += 2*(A*x);
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(b[k][0][i][j], 4);
        BOOST_CHECK_EQUAL(b[k][1][i][j],-4);
        BOOST_CHECK_EQUAL(b[k][2][i][j], 8);
        BOOST_CHECK_EQUAL(b[k][3][i][j],-4);
        BOOST_CHECK_EQUAL(b[k][4][i][j], 4);
      }

  b = -(2*(A*x));
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(b[k][0][i][j],-2);
        BOOST_CHECK_EQUAL(b[k][1][i][j], 2);
        BOOST_CHECK_EQUAL(b[k][2][i][j],-4);
        BOOST_CHECK_EQUAL(b[k][3][i][j], 2);
        BOOST_CHECK_EQUAL(b[k][4][i][j],-2);
      }


  b = (A*x)*2;
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(b[k][0][i][j], 2);
        BOOST_CHECK_EQUAL(b[k][1][i][j],-2);
        BOOST_CHECK_EQUAL(b[k][2][i][j], 4);
        BOOST_CHECK_EQUAL(b[k][3][i][j],-2);
        BOOST_CHECK_EQUAL(b[k][4][i][j], 2);
      }

  b = (A*x)/2;
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(b[k][0][i][j], 0.5);
        BOOST_CHECK_EQUAL(b[k][1][i][j],-0.5);
        BOOST_CHECK_EQUAL(b[k][2][i][j], 1);
        BOOST_CHECK_EQUAL(b[k][3][i][j],-0.5);
        BOOST_CHECK_EQUAL(b[k][4][i][j], 0.5);
      }



  b = (2*(A*x))*2;
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(b[k][0][i][j], 4);
        BOOST_CHECK_EQUAL(b[k][1][i][j],-4);
        BOOST_CHECK_EQUAL(b[k][2][i][j], 8);
        BOOST_CHECK_EQUAL(b[k][3][i][j],-4);
        BOOST_CHECK_EQUAL(b[k][4][i][j], 4);
      }

  b = 2*((A*x)*2);
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(b[k][0][i][j], 4);
        BOOST_CHECK_EQUAL(b[k][1][i][j],-4);
        BOOST_CHECK_EQUAL(b[k][2][i][j], 8);
        BOOST_CHECK_EQUAL(b[k][3][i][j],-4);
        BOOST_CHECK_EQUAL(b[k][4][i][j], 4);
      }

  b = (2*(A*x))/2;
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(b[k][0][i][j], 1);
        BOOST_CHECK_EQUAL(b[k][1][i][j],-1);
        BOOST_CHECK_EQUAL(b[k][2][i][j], 2);
        BOOST_CHECK_EQUAL(b[k][3][i][j],-1);
        BOOST_CHECK_EQUAL(b[k][4][i][j], 1);
      }


  b = (2*A)*x;
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(b[k][0][i][j], 2);
        BOOST_CHECK_EQUAL(b[k][1][i][j],-2);
        BOOST_CHECK_EQUAL(b[k][2][i][j], 4);
        BOOST_CHECK_EQUAL(b[k][3][i][j],-2);
        BOOST_CHECK_EQUAL(b[k][4][i][j], 2);
      }

  b = +((2*A)*x);
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(b[k][0][i][j], 2);
        BOOST_CHECK_EQUAL(b[k][1][i][j],-2);
        BOOST_CHECK_EQUAL(b[k][2][i][j], 4);
        BOOST_CHECK_EQUAL(b[k][3][i][j],-2);
        BOOST_CHECK_EQUAL(b[k][4][i][j], 2);
      }

  b += (2*A)*x;
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(b[k][0][i][j], 4);
        BOOST_CHECK_EQUAL(b[k][1][i][j],-4);
        BOOST_CHECK_EQUAL(b[k][2][i][j], 8);
        BOOST_CHECK_EQUAL(b[k][3][i][j],-4);
        BOOST_CHECK_EQUAL(b[k][4][i][j], 4);
      }


  b = A*(x*2);
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(b[k][0][i][j], 2);
        BOOST_CHECK_EQUAL(b[k][1][i][j],-2);
        BOOST_CHECK_EQUAL(b[k][2][i][j], 4);
        BOOST_CHECK_EQUAL(b[k][3][i][j],-2);
        BOOST_CHECK_EQUAL(b[k][4][i][j], 2);
      }

  b = +(A*(x*2));
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(b[k][0][i][j], 2);
        BOOST_CHECK_EQUAL(b[k][1][i][j],-2);
        BOOST_CHECK_EQUAL(b[k][2][i][j], 4);
        BOOST_CHECK_EQUAL(b[k][3][i][j],-2);
        BOOST_CHECK_EQUAL(b[k][4][i][j], 2);
      }

  b += A*(x*2);
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(b[k][0][i][j], 4);
        BOOST_CHECK_EQUAL(b[k][1][i][j],-4);
        BOOST_CHECK_EQUAL(b[k][2][i][j], 8);
        BOOST_CHECK_EQUAL(b[k][3][i][j],-4);
        BOOST_CHECK_EQUAL(b[k][4][i][j], 4);
      }

  BOOST_CHECK_THROW( b = A*c, AssertionException );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SparseMatrix_CRS_Block_Static_Expression )
{
  typedef DLA::VectorD< DLA::VectorS<2,Real> > Vec_type;
  typedef DLA::MatrixD< DLA::MatrixS<2,2,Real> > Block_type;

  DLA::VectorD< SparseVector<Vec_type> > x = { {5,2},
                                               {5,2} };
  DLA::VectorD< SparseVector<Vec_type> > b = { {5,2},
                                               {5,2} };
  DLA::VectorD< SparseVector<Vec_type> > r = { {5,2},
                                               {5,2} };
  DLA::MatrixD< SparseNonZeroPattern<Block_type> > nz = {{ {5,5}, {5,5} },
                                                         { {5,5}, {5,5} }};

  for (int i = 0; i < 2; i++)
    TriDiagPattern( nz(i,i),2 );

  DLA::MatrixD< SparseMatrix_CRS<Block_type> > A(nz);

  for (int i = 0; i < 2; i++)
    Heat1D<Block_type>::init(A(i,i));

  x = 1;
  b = 2;
  x[0][2] = 2;
  x[1][2] = 2;


  r = b + A*x;
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(r[k][0][i][j], 3);
        BOOST_CHECK_EQUAL(r[k][1][i][j], 1);
        BOOST_CHECK_EQUAL(r[k][2][i][j], 4);
        BOOST_CHECK_EQUAL(r[k][3][i][j], 1);
        BOOST_CHECK_EQUAL(r[k][4][i][j], 3);
      }

  r = +(b + A*x);
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(r[k][0][i][j], 3);
        BOOST_CHECK_EQUAL(r[k][1][i][j], 1);
        BOOST_CHECK_EQUAL(r[k][2][i][j], 4);
        BOOST_CHECK_EQUAL(r[k][3][i][j], 1);
        BOOST_CHECK_EQUAL(r[k][4][i][j], 3);
      }

  r += b + A*x;
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(r[k][0][i][j], 6);
        BOOST_CHECK_EQUAL(r[k][1][i][j], 2);
        BOOST_CHECK_EQUAL(r[k][2][i][j], 8);
        BOOST_CHECK_EQUAL(r[k][3][i][j], 2);
        BOOST_CHECK_EQUAL(r[k][4][i][j], 6);
      }

  r = b - A*x;
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(r[k][0][i][j], 1);
        BOOST_CHECK_EQUAL(r[k][1][i][j], 3);
        BOOST_CHECK_EQUAL(r[k][2][i][j], 0);
        BOOST_CHECK_EQUAL(r[k][3][i][j], 3);
        BOOST_CHECK_EQUAL(r[k][4][i][j], 1);
      }

  r = +(b - A*x);
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(r[k][0][i][j], 1);
        BOOST_CHECK_EQUAL(r[k][1][i][j], 3);
        BOOST_CHECK_EQUAL(r[k][2][i][j], 0);
        BOOST_CHECK_EQUAL(r[k][3][i][j], 3);
        BOOST_CHECK_EQUAL(r[k][4][i][j], 1);
      }

  r += b - A*x;
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(r[k][0][i][j], 2);
        BOOST_CHECK_EQUAL(r[k][1][i][j], 6);
        BOOST_CHECK_EQUAL(r[k][2][i][j], 0);
        BOOST_CHECK_EQUAL(r[k][3][i][j], 6);
        BOOST_CHECK_EQUAL(r[k][4][i][j], 2);
      }

  r = -A*x + b;
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(r[k][0][i][j], 1);
        BOOST_CHECK_EQUAL(r[k][1][i][j], 3);
        BOOST_CHECK_EQUAL(r[k][2][i][j], 0);
        BOOST_CHECK_EQUAL(r[k][3][i][j], 3);
        BOOST_CHECK_EQUAL(r[k][4][i][j], 1);
      }

  r = 2*(b + A*x);
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(r[k][0][i][j], 6);
        BOOST_CHECK_EQUAL(r[k][1][i][j], 2);
        BOOST_CHECK_EQUAL(r[k][2][i][j], 8);
        BOOST_CHECK_EQUAL(r[k][3][i][j], 2);
        BOOST_CHECK_EQUAL(r[k][4][i][j], 6);
      }

  r = (b + A*x)*2;
  for ( int k = 0; k < 2; k++ )
    for ( int i = 0; i < 2; i++ )
      for ( int j = 0; j < 2; j++ )
      {
        BOOST_CHECK_EQUAL(r[k][0][i][j], 6);
        BOOST_CHECK_EQUAL(r[k][1][i][j], 2);
        BOOST_CHECK_EQUAL(r[k][2][i][j], 8);
        BOOST_CHECK_EQUAL(r[k][3][i][j], 2);
        BOOST_CHECK_EQUAL(r[k][4][i][j], 6);
      }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
