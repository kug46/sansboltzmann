// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include "tools/SANSnumerics.h"

#include "../TriDiagPattern_btest.h"
#include "../Heat1D_btest.h"

#include "LinearAlgebra/SparseLinAlg/Preconditioners/LU_SGS.h"
#include "LinearAlgebra/SparseLinAlg/Krylov/FGMRES.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"

using namespace SANS;
using namespace SANS::SLA;


//############################################################################//
BOOST_AUTO_TEST_SUITE( SparseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LU_SGS_Solve )
{
  PyDict d;

  LU_SGSParam::checkInputs(d);

  typedef SparseMatrix_CRS<Real> Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  Vector_type q(10);
  Heat1DEquationSet<Matrix_type> f(q);

  LU_SGS< Matrix_type > Precond(d, f);

  Vector_type x(10), b(10);

  //Create a residual vector
  for ( int i = 0; i < 10; i++ )
    b[i] = i + 1;

  //Approximately solve the linear system
  Precond.solve(b, x);

  //Exact numbers computed using Mathematica by creating M = (L+D)D^-1(D+U) and taking it's inverse
  BOOST_CHECK_CLOSE( 1386837./524288., x[0], 1e-10 );
  BOOST_CHECK_CLOSE( 1124693./262144., x[1], 1e-10 );
  BOOST_CHECK_CLOSE( 797013./131072., x[2], 1e-10 );
  BOOST_CHECK_CLOSE( 518485./65536., x[3], 1e-10 );
  BOOST_CHECK_CLOSE( 317781./32768., x[4], 1e-10 );
  BOOST_CHECK_CLOSE( 185685./16384., x[5], 1e-10 );
  BOOST_CHECK_CLOSE( 103509./8192., x[6], 1e-10 );
  BOOST_CHECK_CLOSE( 54293./4096., x[7], 1e-10 );
  BOOST_CHECK_CLOSE( 25605./2048., x[8], 1e-10 );
  BOOST_CHECK_CLOSE( 9217./1024., x[9], 1e-10 );
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LU_SGS_Solve_Block )
{
  PyDict d;

  LU_SGSParam::checkInputs(d);

  typedef DLA::MatrixD<Real> TM;
  typedef SparseMatrix_CRS< TM > Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  LU_SGS< Matrix_type > Precond(d);

  SparseNonZeroPattern<TM> nz(5,5);
  TriDiagPattern(nz,2);

  Matrix_type A( nz );
  Vector_type x(5,2), b(5,2);

  //Initialize the matrix here when it is a real matrix
  Heat1D<TM>::init(A);

  //Create a residual vector
  for ( int i = 0; i < 5; i++ )
    b[i] = i + 1;

  //Approximately solve the linear system
  x = Precond.inverse(A)*b;

  //Exact numbers computed using Mathematica by creating M = (L+D)D^-1(D+U) and taking it's inverse
  BOOST_CHECK_CLOSE( 1173./512., x[0][0], 1e-10 );
  BOOST_CHECK_CLOSE( 1173./512., x[0][1], 1e-10 );
  BOOST_CHECK_CLOSE( 917./256., x[1][0], 1e-10 );
  BOOST_CHECK_CLOSE( 917./256., x[1][1], 1e-10 );
  BOOST_CHECK_CLOSE( 597./128., x[2][0], 1e-10 );
  BOOST_CHECK_CLOSE( 597./128., x[2][1], 1e-10 );
  BOOST_CHECK_CLOSE( 325./64., x[3][0], 1e-10 );
  BOOST_CHECK_CLOSE( 325./64., x[3][1], 1e-10 );
  BOOST_CHECK_CLOSE( 129./32., x[4][0], 1e-10 );
  BOOST_CHECK_CLOSE( 129./32., x[4][1], 1e-10 );
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LU_SGS_Solve_Static )
{
  PyDict d;

  LU_SGSParam::checkInputs(d);

  typedef DLA::MatrixS<2,2,Real> TM;
  typedef SparseMatrix_CRS< TM > Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  Vector_type q(5);
  Heat1DEquationSet<Matrix_type> f(q);

  LU_SGS< Matrix_type > Precond(d, f);

  Vector_type x(5), b(5);

  //Create a residual vector
  for ( int i = 0; i < 5; i++ )
    b[i] = i + 1;

  //Approximately solve the linear system
  Precond.solve(b, x);

  //Exact numbers computed using Mathematica by creating M = (L+D)D^-1(D+U) and taking it's inverse
  BOOST_CHECK_CLOSE( 1173./512., x[0][0], 1e-10 );
  BOOST_CHECK_CLOSE( 1173./512., x[0][1], 1e-10 );
  BOOST_CHECK_CLOSE( 917./256., x[1][0], 1e-10 );
  BOOST_CHECK_CLOSE( 917./256., x[1][1], 1e-10 );
  BOOST_CHECK_CLOSE( 597./128., x[2][0], 1e-10 );
  BOOST_CHECK_CLOSE( 597./128., x[2][1], 1e-10 );
  BOOST_CHECK_CLOSE( 325./64., x[3][0], 1e-10 );
  BOOST_CHECK_CLOSE( 325./64., x[3][1], 1e-10 );
  BOOST_CHECK_CLOSE( 129./32., x[4][0], 1e-10 );
  BOOST_CHECK_CLOSE( 129./32., x[4][1], 1e-10 );
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LU_SGS_Solve_Block_Static )
{
  PyDict d;

  LU_SGSParam::checkInputs(d);

  typedef DLA::MatrixD< DLA::MatrixS<2,2,Real> > TM;
  typedef SparseMatrix_CRS< TM > Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  LU_SGS< Matrix_type > Precond(d);

  SparseNonZeroPattern<TM> nz(3,3);
  TriDiagPattern(nz,2);

  Matrix_type A( nz );
  Vector_type x(3,2), b(3,2);

  //Initialize the matrix here when it is a real matrix
  Heat1D<TM>::init(A);

  //Create a residual vector
  for ( int i = 0; i < 3; i++ )
    b[i] = i + 1;

  //Approximately solve the linear system
  x = Precond.inverse(A)*b;

  //Exact numbers computed using Mathematica by creating M = (L+D)D^-1(D+U) and taking it's inverse
  BOOST_CHECK_CLOSE( 53./32., x[0][0][0], 1e-10 );
  BOOST_CHECK_CLOSE( 53./32., x[0][0][1], 1e-10 );
  BOOST_CHECK_CLOSE( 53./32., x[0][1][0], 1e-10 );
  BOOST_CHECK_CLOSE( 53./32., x[0][1][1], 1e-10 );

  BOOST_CHECK_CLOSE( 37./16., x[1][0][0], 1e-10 );
  BOOST_CHECK_CLOSE( 37./16., x[1][0][1], 1e-10 );
  BOOST_CHECK_CLOSE( 37./16., x[1][1][0], 1e-10 );
  BOOST_CHECK_CLOSE( 37./16., x[1][1][1], 1e-10 );

  BOOST_CHECK_CLOSE( 17./8., x[2][0][0], 1e-10 );
  BOOST_CHECK_CLOSE( 17./8., x[2][0][1], 1e-10 );
  BOOST_CHECK_CLOSE( 17./8., x[2][1][0], 1e-10 );
  BOOST_CHECK_CLOSE( 17./8., x[2][1][1], 1e-10 );

}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LU_SGS_Solve_MatrixD_CRS_Static )
{
  PyDict d;

  LU_SGSParam::checkInputs(d);

  typedef DLA::MatrixS<2,2,Real> TM;
  typedef DLA::MatrixD< SparseMatrix_CRS< TM > > Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  Vector_type q = {{2},{2}};
  Heat1DEquationSet<Matrix_type> f(q);

  LU_SGS< Matrix_type > Precond(d, f);

  Vector_type x = {{2},{2}}, b = {{2},{2}};

  //Create a residual vector
  for (int j = 0; j < 2; j++)
    for ( int i = 0; i < 2; i++ )
      b[j][i] = 2*j + i + 1;

  //std::fstream fout( "tmp/A.mtx", std::fstream::out );
  //std::cout << "btest: global jac" << std::endl;  WriteMatrixMarketFile( Precond.A(), fout );

  //Approximately solve the linear system
  Precond.solve(b, x);

  //Exact numbers computed using Mathematica by creating M = (L+D)D^-1(D+U) and taking it's inverse
  BOOST_CHECK_CLOSE( 261./128., x[0][0][0], 1e-10 );
  BOOST_CHECK_CLOSE( 261./128., x[0][0][1], 1e-10 );
  BOOST_CHECK_CLOSE( 197./64., x[0][1][0], 1e-10 );
  BOOST_CHECK_CLOSE( 197./64., x[0][1][1], 1e-10 );
  BOOST_CHECK_CLOSE( 117./32., x[1][0][0], 1e-10 );
  BOOST_CHECK_CLOSE( 117./32., x[1][0][1], 1e-10 );
  BOOST_CHECK_CLOSE( 49./16., x[1][1][0], 1e-10 );
  BOOST_CHECK_CLOSE( 49./16., x[1][1][1], 1e-10 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FGMRES_LU_SGS_Solve )
{
  PyDict d, Precond;

  Precond[FGMRESParam::params.Preconditioner.Name] = FGMRESParam::params.Preconditioner.LU_SGS;

  d[FGMRESParam::params.nInner] = 10;
  d[FGMRESParam::params.nOuter] = 3;
  d[FGMRESParam::params.tol] = 1e-10;
  d[FGMRESParam::params.PrintCovergence] = false;
  d[FGMRESParam::params.Preconditioner] = Precond;

  FGMRESParam::checkInputs(d);

  typedef SparseMatrix_CRS<Real> Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  Vector_type q(5);
  Heat1DEquationSet<Matrix_type> f(q);

  std::shared_ptr< LinearSolverBase< Matrix_type > > Solver =
      FGMRESParam::newSolver< Matrix_type >(d, f);

  Vector_type x(5), b(5), b2(5);

  //Set the matrix for the solver
  Solver->factorize();

  //Create a residual vector
  b[0] = 0.5;
  b[1] = 1;
  b[2] = 2;
  b[3] = 1;
  b[4] = 0.5;

  //Solve the linear system. x here is the initial guess
  x = 1;
  Solver->backsolve(b, x);

  //Compute the residual vector from the solution
  b2 = Solver->A()*x;

  //The residuals should now be the same!
  for (int i = 0; i < b.m(); i++)
    BOOST_CHECK_CLOSE( b[i], b2[i], 1e-8 );

  //Temporary output just to check what is going on
  //for (int i = 0; i < b.size(); i++)
  //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FGMRES_LU_SGS_Solve_Block )
{
  PyDict d, Precond;

  Precond[FGMRESParam::params.Preconditioner.Name] = FGMRESParam::params.Preconditioner.LU_SGS;

  d[FGMRESParam::params.nInner] = 10;
  d[FGMRESParam::params.nOuter] = 3;
  d[FGMRESParam::params.tol] = 1e-10;
  d[FGMRESParam::params.PrintCovergence] = false;
  d[FGMRESParam::params.Preconditioner] = Precond;

  FGMRESParam::checkInputs(d);

  typedef DLA::MatrixD<Real> Block_type;
  typedef SparseMatrix_CRS< Block_type > Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  std::shared_ptr< LinearSolverBase< Matrix_type > > Solver =
      FGMRESParam::newSolver< Matrix_type >(d);

  SparseNonZeroPattern<Block_type> nz(5,5);
  TriDiagPattern(nz,2);

  Matrix_type A( nz );
  Vector_type x(5,2), b(5,2), b2(5,2);

  //Initialize the matrix
  Heat1D<Block_type>::init(A);

  //Set the matrix for the solver
  Solver->factorize(A);

  //Create a residual vector
  b[0] = 0.5;
  b[1] = 1;
  b[2] = 2;
  b[3] = 1;
  b[4] = 0.5;

  //Solve the linear system. x here is the initial guess
  x = 1;
  Solver->backsolve(b, 1, x);

  //Compute the residual vector from the solution
  b2 = A*x;

  //The residuals should now be the same!
  for (int i = 0; i < b.m(); i++)
    for (int j = 0; j < b.block_m(i); j++)
      BOOST_CHECK_CLOSE( b[i][j], b2[i][j], 1e-12 );

  //Temporary output just to check what is going on
  //for (int i = 0; i < b.m(); i++)
  //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FGMRES_LU_SGS_Solve_Static )
{
  PyDict d, Precond;

  Precond[FGMRESParam::params.Preconditioner.Name] = FGMRESParam::params.Preconditioner.LU_SGS;

  d[FGMRESParam::params.nInner] = 10;
  d[FGMRESParam::params.nOuter] = 3;
  d[FGMRESParam::params.tol] = 1e-10;
  d[FGMRESParam::params.PrintCovergence] = false;
  d[FGMRESParam::params.Preconditioner] = Precond;

  FGMRESParam::checkInputs(d);

  typedef DLA::MatrixS<2,2,Real> Block_type;
  typedef SparseMatrix_CRS< Block_type > Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  Vector_type q(5);
  Heat1DEquationSet<Matrix_type> f(q);

  std::shared_ptr< LinearSolverBase< Matrix_type > > Solver =
      FGMRESParam::newSolver< Matrix_type >(d, f);

  Vector_type x(5), b(5), b2(5);

  // factorize the matrix for the solver
  Solver->factorize();

  //Create a residual vector
  b[0] = 0.5;
  b[1] = 1;
  b[2] = 2;
  b[3] = 1;
  b[4] = 0.5;

  //Solve the linear system. x here is the initial guess
  x = 1;
  Solver->backsolve(b, x);

  //Compute the residual vector from the solution
  b2 = Solver->A()*x;

  //The residuals should now be the same!
  for (int i = 0; i < b.m(); i++)
    for (int j = 0; j < Block_type::M; j++)
      BOOST_CHECK_CLOSE( b[i][j], b2[i][j], 1e-12 );

  //Temporary output just to check what is going on
  //for (int i = 0; i < b.m(); i++)
  //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FGMRES_LU_SGS_Solve_Block_Static )
{
  PyDict d, Precond;

  Precond[FGMRESParam::params.Preconditioner.Name] = FGMRESParam::params.Preconditioner.LU_SGS;

  d[FGMRESParam::params.nInner] = 10;
  d[FGMRESParam::params.nOuter] = 3;
  d[FGMRESParam::params.tol] = 1e-10;
  d[FGMRESParam::params.PrintCovergence] = false;
  d[FGMRESParam::params.Preconditioner] = Precond;

  FGMRESParam::checkInputs(d);

  typedef DLA::MatrixD< DLA::MatrixS<2,2,Real> > Block_type;
  typedef SparseMatrix_CRS< Block_type > Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  std::shared_ptr< LinearSolverBase< Matrix_type > > Solver =
      FGMRESParam::newSolver< Matrix_type >(d);

  SparseNonZeroPattern<Block_type> nz(5,5);
  TriDiagPattern(nz,2);

  Matrix_type A( nz );
  Vector_type x(5,2), b(5,2), b2(5,2);

  //Initialize the matrix
  Heat1D<Block_type>::init(A);

  //Set the matrix for the solver
  Solver->factorize(A);

  //Create a residual vector
  b[0] = 0.5;
  b[1] = 1;
  b[2] = 2;
  b[3] = 1;
  b[4] = 0.5;

  //Solve the linear system. x here is the initial guess
  x = 1;
  Solver->backsolve(b, 1, x);

  //Compute the residual vector from the solution
  b2 = A*x;

  //The residuals should now be the same!
  for (int i = 0; i < b.m(); i++)
    for (int j = 0; j < b.block_m(i); j++)
      for (int k = 0; k < 2; k++)
        BOOST_CHECK_CLOSE( b[i][j][k], b2[i][j][k], 1e-12 );

  //Temporary output just to check what is going on
  //for (int i = 0; i < b.m(); i++)
  //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
