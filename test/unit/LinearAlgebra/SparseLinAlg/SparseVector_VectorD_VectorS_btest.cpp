// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include "tools/SANSnumerics.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Add.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"


using namespace SANS;
using namespace SANS::SLA;


//Explicitly instantiate templates to get correct coverage information
namespace SANS
{
namespace SLA
{
typedef SparseVector< DLA::VectorD< DLA::VectorS<1,Real> > > Vec_type;
template class SparseVector<Real>;
template class OpAdd<Vec_type, Vec_type, false>;
template class OpSub<Vec_type, Vec_type, false>;
template class OpMulScalar<Vec_type, false>;
}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( SparseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseVector_Block_Static_ctor )
{
  typedef SparseVector< DLA::VectorD< DLA::VectorS<1,Real> > > Vector_type;
  Vector_type x(1,1);

  BOOST_CHECK_EQUAL(x.m(), 1);
  BOOST_CHECK_EQUAL(x.block_m(0), 1);

  x[0] = 1;
  BOOST_CHECK_EQUAL(x[0][0][0], 1);
  BOOST_CHECK_EQUAL(const_cast<const Vector_type&>(x)[0][0][0], 1);

  Vector_type x2(x);
  BOOST_CHECK_EQUAL(x2[0][0][0], 1);

  Vector_type x3(x.size());
  BOOST_CHECK_EQUAL(x3.m(), 1);
  BOOST_CHECK_EQUAL(x3.block_m(0), 1);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseVector_Block_Static_assign )
{
  typedef SparseVector< DLA::VectorD< DLA::VectorS<1,Real> > > Vector_type;
  Vector_type x(2,1), b(2,1), c(3,1);

  x = 0;
  BOOST_CHECK_EQUAL(x[0][0][0], 0);
  BOOST_CHECK_EQUAL(x[1][0][0], 0);

  b = x;
  BOOST_CHECK_EQUAL(x[0][0][0], 0);
  BOOST_CHECK_EQUAL(x[1][0][0], 0);
  BOOST_CHECK_EQUAL(b[0][0][0], 0);
  BOOST_CHECK_EQUAL(b[1][0][0], 0);

  b[1] = 1;
  BOOST_CHECK_EQUAL(x[0][0][0], 0);
  BOOST_CHECK_EQUAL(x[1][0][0], 0);
  BOOST_CHECK_EQUAL(b[0][0][0], 0);
  BOOST_CHECK_EQUAL(b[1][0][0], 1);

  x = b;
  BOOST_CHECK_EQUAL(x[0][0][0], 0);
  BOOST_CHECK_EQUAL(x[1][0][0], 1);
  BOOST_CHECK_EQUAL(b[0][0][0], 0);
  BOOST_CHECK_EQUAL(b[1][0][0], 1);

  BOOST_CHECK_THROW( c = x, AssertionException );
  BOOST_CHECK_THROW( x = c, AssertionException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseVector_Block_Static_unary )
{
  typedef SparseVector< DLA::VectorD< DLA::VectorS<2,Real> > > Vector_type;
  Vector_type x(2,2);

  x = 2;
  for (int i = 0; i < 2; i++ )
  {
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(x[0][i][j], 2);
      BOOST_CHECK_EQUAL(x[1][i][j], 2);
    }
  }

  x /= 2;
  for (int i = 0; i < 2; i++ )
  {
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(x[0][i][j], 1);
      BOOST_CHECK_EQUAL(x[1][i][j], 1);
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseVector_Block_Static_norm )
{
  typedef SparseVector< DLA::VectorD< DLA::VectorS<4,Real> > > Vector_type;
  Vector_type x(4,4);

  x = 2;
  BOOST_CHECK_EQUAL(x.norm(), 16);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseVector_Block_Static_dot )
{
  typedef SparseVector< DLA::VectorD< DLA::VectorS<2,Real> > > Vector_type;
  Vector_type x(2,2), b(2,2), c(3,2), x2(2,2);

  x = 2;
  b = 3;
  BOOST_CHECK_EQUAL(x.dot(b), 48);
  BOOST_CHECK_THROW( x.dot(c), AssertionException );

  x2 = 3;
  BOOST_CHECK_EQUAL(dot(x+x2, b), 120);
  BOOST_CHECK_EQUAL(dot(b, x+x2), 120);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseVector_Block_Static_LazyExpression )
{
  typedef SparseVector< DLA::VectorD< DLA::VectorS<2,Real> > > Vector_type;
  Vector_type x(2,2), b(2,2), c(3,2);

  x = 2;
  x.value(1, b);
  for (int i = 0; i < 2; i++ )
  {
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(b[0][i][j], 2);
      BOOST_CHECK_EQUAL(b[1][i][j], 2);
    }
  }

  x.value(-1, b);
  for (int i = 0; i < 2; i++ )
  {
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(b[0][i][j],-2);
      BOOST_CHECK_EQUAL(b[1][i][j],-2);
    }
  }

  x.plus(1, b);
  for (int i = 0; i < 2; i++ )
  {
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(b[0][i][j], 0);
      BOOST_CHECK_EQUAL(b[1][i][j], 0);
    }
  }

  x.plus(-1, b);
  for (int i = 0; i < 2; i++ )
  {
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(b[0][i][j],-2);
      BOOST_CHECK_EQUAL(b[1][i][j],-2);
    }
  }

  BOOST_CHECK_THROW( x.value(1, c), AssertionException );
  BOOST_CHECK_THROW( x.plus(1, c), AssertionException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseVector_Block_Static_Expression )
{
  typedef SparseVector< DLA::VectorD< DLA::VectorS<2,Real> > > Vector_type;
  Vector_type x1(2,2), x2(2,2), x3(2,2);

  x1 = 1;
  x2 = 2;

  x3 = -x1;
  for (int i = 0; i < 2; i++ )
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(x3[0][i][j],-1);
      BOOST_CHECK_EQUAL(x3[1][i][j],-1);
    }


  x3 = -(-x1);
  for (int i = 0; i < 2; i++ )
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(x3[0][i][j], 1);
      BOOST_CHECK_EQUAL(x3[1][i][j], 1);
    }

  x3 = x1 + x2;
  for (int i = 0; i < 2; i++ )
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(x3[0][i][j], 3);
      BOOST_CHECK_EQUAL(x3[1][i][j], 3);
    }

  x3 = +(x1 + x2);
  for (int i = 0; i < 2; i++ )
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(x3[0][i][j], 3);
      BOOST_CHECK_EQUAL(x3[1][i][j], 3);
    }

  x3 += x1 + x2;
  for (int i = 0; i < 2; i++ )
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(x3[0][i][j], 6);
      BOOST_CHECK_EQUAL(x3[1][i][j], 6);
    }

  x3 -= x1 + x2;
  for (int i = 0; i < 2; i++ )
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(x3[0][i][j], 3);
      BOOST_CHECK_EQUAL(x3[1][i][j], 3);
    }


  x3 = x1 - x2;
  for (int i = 0; i < 2; i++ )
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(x3[0][i][j],-1);
      BOOST_CHECK_EQUAL(x3[1][i][j],-1);
    }

  x3 = +(x1 - x2);
  for (int i = 0; i < 2; i++ )
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(x3[0][i][j],-1);
      BOOST_CHECK_EQUAL(x3[1][i][j],-1);
    }

  x3 += x1 - x2;
  for (int i = 0; i < 2; i++ )
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(x3[0][i][j],-2);
      BOOST_CHECK_EQUAL(x3[1][i][j],-2);
    }

  x3 -= x1 - x2;
  for (int i = 0; i < 2; i++ )
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(x3[0][i][j],-1);
      BOOST_CHECK_EQUAL(x3[1][i][j],-1);
    }


  x3 = 2*x1;
  for (int i = 0; i < 2; i++ )
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(x3[0][i][j], 2);
      BOOST_CHECK_EQUAL(x3[1][i][j], 2);
    }

  x3 = +(2*x1);
  for (int i = 0; i < 2; i++ )
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(x3[0][i][j], 2);
      BOOST_CHECK_EQUAL(x3[1][i][j], 2);
    }

  x3 = x1*2;
  for (int i = 0; i < 2; i++ )
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(x3[0][i][j], 2);
      BOOST_CHECK_EQUAL(x3[1][i][j], 2);
    }

  x3 = x2/2;
  for (int i = 0; i < 2; i++ )
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(x3[0][i][j], 1);
      BOOST_CHECK_EQUAL(x3[1][i][j], 1);
    }

  x3 = 2*x1*2;
  for (int i = 0; i < 2; i++ )
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(x3[0][i][j], 4);
      BOOST_CHECK_EQUAL(x3[1][i][j], 4);
    }

  x3 = 2*(x1*2);
  for (int i = 0; i < 2; i++ )
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(x3[0][i][j], 4);
      BOOST_CHECK_EQUAL(x3[1][i][j], 4);
    }

  x3 = (2*x1)/2;
  for (int i = 0; i < 2; i++ )
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(x3[0][i][j], 1);
      BOOST_CHECK_EQUAL(x3[1][i][j], 1);
    }

  x3 += x1*2;
  for (int i = 0; i < 2; i++ )
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(x3[0][i][j], 3);
      BOOST_CHECK_EQUAL(x3[1][i][j], 3);
    }

  x3 -= x1*2;
  for (int i = 0; i < 2; i++ )
    for (int j = 0; j < 2; j++ )
    {
      BOOST_CHECK_EQUAL(x3[0][i][j], 1);
      BOOST_CHECK_EQUAL(x3[1][i][j], 1);
    }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
