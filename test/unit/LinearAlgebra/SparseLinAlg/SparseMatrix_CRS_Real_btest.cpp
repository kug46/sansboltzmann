// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"
#include "tools/minmax.h"

#include "../TriDiagPattern_btest.h"
#include "../Heat1D_btest.h"
#include "../Advection1D_btest.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Add.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/ScalarMatrix_CRS.h"

using namespace SANS::SLA;
using namespace SANS;

//Explicitly instantiate templates to get correct coverage information
namespace SANS
{
namespace SLA
{
typedef SparseVector<Real> Vec_type;
typedef SparseMatrix_CRS<Real> Mat_type;
typedef OpMul<Mat_type, Vec_type> MulVec_type;
template class SparseMatrix_CRS<Real>;
template class OpMul<Mat_type, Vec_type>;
template class OpAdd<Vec_type, MulVec_type, true>;
template class OpSub<Vec_type, MulVec_type, true>;
template class OpMulScalar<MulVec_type, true>;
template class OpAdd<Vec_type, Vec_type, false>;
template class OpSub<Vec_type, Vec_type, false>;
template class OpMulScalar<Vec_type, false>;

template class SparseNonZeroPattern<Real>;
}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( SparseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_CRS_ctor )
{
  const int m = 5;
  typedef SparseMatrix_CRS<Real> Matrix_type;
  SparseNonZeroPattern<Real> nz(m,m);
  TriDiagPattern(nz);
  Matrix_type A( nz );

  const Matrix_type& cA = A;

  //Check that all tri-diag mappings are correct

  BOOST_CHECK_EQUAL(A.getNumNonZero(), 3*(m-2) + 4 );
  for ( int i = 0; i < A.m(); i++ )
    for ( int k = 0; k < A.rowNonZero(i); k++)
      BOOST_CHECK_EQUAL( A.sparseRow(i,k), 0 );

  int *row_ptr = A.get_row_ptr();
  int *col_ind = A.get_col_ind();

  BOOST_CHECK_EQUAL( A.get_values(), &A[0]);

  //First row in the matrix
  BOOST_CHECK_EQUAL(row_ptr[0], 0);
  BOOST_CHECK_EQUAL(row_ptr[1], 2);
  BOOST_CHECK_EQUAL(col_ind[0], 0);
  BOOST_CHECK_EQUAL(col_ind[1], 1);

  //All the entries on the interior rows
  for ( int i = 0; i < m-2; i++ )
  {
    BOOST_CHECK_EQUAL(row_ptr[i+2], 3*i + 3 + 2);
    BOOST_CHECK_EQUAL(col_ind[3*i + 0 + 2], i + 0);
    BOOST_CHECK_EQUAL(col_ind[3*i + 1 + 2], i + 1);
    BOOST_CHECK_EQUAL(col_ind[3*i + 2 + 2], i + 2);
  }

  //Last row in the matrix
  BOOST_CHECK_EQUAL(row_ptr[m], 3*(m-2) + 4);
  BOOST_CHECK_EQUAL(col_ind[3*(m-2) + 2], (m-2) + 0);
  BOOST_CHECK_EQUAL(col_ind[3*(m-2) + 3], (m-2) + 1);

  //Check that the row_ptr and col_ind work the way they are intended to be used
  for ( int i = 0; i < m; i++ )
    for ( int k = row_ptr[i]; k < row_ptr[i+1]; k++)
    {
      BOOST_CHECK_EQUAL(A[k], 0);
      BOOST_CHECK_EQUAL(cA[k], 0);
      BOOST_CHECK_EQUAL(col_ind[k], max(i-1,0) + k - row_ptr[i]);
    }

  //Test isNonZero operator

  BOOST_CHECK(cA.isNonZero(0,0));
  BOOST_CHECK(cA.isNonZero(0,1));

  for ( int j = 2; j < cA.n(); j++ )
    BOOST_CHECK(!cA.isNonZero(0,j));

  for ( int i = 1; i < cA.m()-1; i++ )
  {
    for ( int j = 0; j < i-1; j++ )
      BOOST_CHECK(!cA.isNonZero(i,j));

    BOOST_CHECK(cA.isNonZero(i,i-1));
    BOOST_CHECK(cA.isNonZero(i,i));
    BOOST_CHECK(cA.isNonZero(i,i+1));

    for ( int j = i+2; j < cA.n(); j++ )
      BOOST_CHECK(!cA.isNonZero(i,j));
  }

  for ( int j = 0; j < cA.n()-2; j++ )
    BOOST_CHECK(!cA.isNonZero(A.m()-1,j));

  BOOST_CHECK(cA.isNonZero(A.m()-1,cA.n()-2));
  BOOST_CHECK(cA.isNonZero(A.m()-1,cA.n()-1));


  //Test the simple assignemnt operator
  A = 5;

  BOOST_CHECK_EQUAL(cA.sparseRow(0,0),  5);
  BOOST_CHECK_EQUAL(cA.sparseRow(0,1),  5);
  for ( int i = 1; i < cA.m()-1; i++ )
  {
    BOOST_CHECK_EQUAL(cA.sparseRow(i,0),  5);
    BOOST_CHECK_EQUAL(cA.sparseRow(i,1),  5);
    BOOST_CHECK_EQUAL(cA.sparseRow(i,2),  5);
  }
  BOOST_CHECK_EQUAL(cA.sparseRow(A.m()-1,0),  5);
  BOOST_CHECK_EQUAL(cA.sparseRow(A.m()-1,1),  5);

  //Initialize as a '1D heat equation' matrix and check that is correct
  Heat1D<Real>::init(A);


  BOOST_CHECK_EQUAL(cA.sparseRow(0,0),  2);
  BOOST_CHECK_EQUAL(cA.sparseRow(0,1), -1);
  for ( int i = 1; i < cA.m()-1; i++ )
  {
    BOOST_CHECK_EQUAL(cA.sparseRow(i,0), -1);
    BOOST_CHECK_EQUAL(cA.sparseRow(i,1),  2);
    BOOST_CHECK_EQUAL(cA.sparseRow(i,2), -1);
  }
  BOOST_CHECK_EQUAL(cA.sparseRow(A.m()-1,0), -1);
  BOOST_CHECK_EQUAL(cA.sparseRow(A.m()-1,1),  2);

  //Test the dense matrix accessor
  BOOST_CHECK_EQUAL(cA(0,0),  2);
  BOOST_CHECK_EQUAL(cA(0,1), -1);
  for ( int i = 1; i < cA.m()-1; i++ )
  {
    BOOST_CHECK_EQUAL(cA(i,i-1), -1);
    BOOST_CHECK_EQUAL(cA(i,i  ),  2);
    BOOST_CHECK_EQUAL(cA(i,i+1), -1);
  }
  BOOST_CHECK_EQUAL(cA(A.m()-1,A.m()-2), -1);
  BOOST_CHECK_EQUAL(cA(A.m()-1,A.m()-1),  2);

  BOOST_CHECK_THROW( cA(0,2), AssertionException );
  BOOST_CHECK_THROW( cA(A.m()-1,0), AssertionException );
  BOOST_CHECK_THROW( cA(-1,0), AssertionException );
  BOOST_CHECK_THROW( cA(0,-1), AssertionException );

  //Test assignement to the dense matrix accessor
  A.slowAccess(0,0) = 3;
  BOOST_CHECK_EQUAL(cA(0,0),  3);

  //Test the dense matrix accessor on pattern
  BOOST_CHECK_EQUAL(nz(0,0), 1);
  BOOST_CHECK_EQUAL(nz(0,1), 1);
  for ( int i = 1; i < cA.m()-1; i++ )
  {
    BOOST_CHECK_EQUAL(nz(i,i-1), 1);
    BOOST_CHECK_EQUAL(nz(i,i  ), 1);
    BOOST_CHECK_EQUAL(nz(i,i+1), 1);
  }
  BOOST_CHECK_EQUAL(nz(A.m()-1,A.m()-2), 1);
  BOOST_CHECK_EQUAL(nz(A.m()-1,A.m()-1), 1);

  BOOST_CHECK_EQUAL(nz(0,2), 0 );
  BOOST_CHECK_EQUAL(nz(A.m()-1,0), 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_CRS_Empty_MulVec )
{
  SparseVector<Real> x(5), b(5);
  SparseNonZeroPattern<Real> Null(5,5);

  //Create an empty matrix, which acts like a 'zero' matrix
  SparseMatrix_CRS<Real> A(Null);

  x = 1;
  x[2] = 2;

  b = A*x;
  for ( int i = 0; i < b.m(); i++)
    BOOST_CHECK_EQUAL(b[i], 0);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_CRS_MulVec )
{
  SparseVector<Real> x(5), b(5), c(3);
  SparseNonZeroPattern<Real> nz(5,5);
  TriDiagPattern(nz);
  SparseMatrix_CRS<Real> A(nz);

  Heat1D<Real>::init(A);

  x = 1;
  x[2] = 2;

  b = A*x;
  BOOST_CHECK_EQUAL(b[0], 1);
  BOOST_CHECK_EQUAL(b[1],-1);
  BOOST_CHECK_EQUAL(b[2], 2);
  BOOST_CHECK_EQUAL(b[3],-1);
  BOOST_CHECK_EQUAL(b[4], 1);

  b = +(A*x);
  BOOST_CHECK_EQUAL(b[0], 1);
  BOOST_CHECK_EQUAL(b[1],-1);
  BOOST_CHECK_EQUAL(b[2], 2);
  BOOST_CHECK_EQUAL(b[3],-1);
  BOOST_CHECK_EQUAL(b[4], 1);

  b += A*x;
  BOOST_CHECK_EQUAL(b[0], 2);
  BOOST_CHECK_EQUAL(b[1],-2);
  BOOST_CHECK_EQUAL(b[2], 4);
  BOOST_CHECK_EQUAL(b[3],-2);
  BOOST_CHECK_EQUAL(b[4], 2);



  b = 2*(A*x);
  BOOST_CHECK_EQUAL(b[0], 2);
  BOOST_CHECK_EQUAL(b[1],-2);
  BOOST_CHECK_EQUAL(b[2], 4);
  BOOST_CHECK_EQUAL(b[3],-2);
  BOOST_CHECK_EQUAL(b[4], 2);

  b = +(2*(A*x));
  BOOST_CHECK_EQUAL(b[0], 2);
  BOOST_CHECK_EQUAL(b[1],-2);
  BOOST_CHECK_EQUAL(b[2], 4);
  BOOST_CHECK_EQUAL(b[3],-2);
  BOOST_CHECK_EQUAL(b[4], 2);

  b += 2*(A*x);
  BOOST_CHECK_EQUAL(b[0], 4);
  BOOST_CHECK_EQUAL(b[1],-4);
  BOOST_CHECK_EQUAL(b[2], 8);
  BOOST_CHECK_EQUAL(b[3],-4);
  BOOST_CHECK_EQUAL(b[4], 4);

  b = -(2*(A*x));
  BOOST_CHECK_EQUAL(b[0],-2);
  BOOST_CHECK_EQUAL(b[1], 2);
  BOOST_CHECK_EQUAL(b[2],-4);
  BOOST_CHECK_EQUAL(b[3], 2);
  BOOST_CHECK_EQUAL(b[4],-2);



  b = (A*x)*2;
  BOOST_CHECK_EQUAL(b[0], 2);
  BOOST_CHECK_EQUAL(b[1],-2);
  BOOST_CHECK_EQUAL(b[2], 4);
  BOOST_CHECK_EQUAL(b[3],-2);
  BOOST_CHECK_EQUAL(b[4], 2);

  b = (A*x)/2;
  BOOST_CHECK_EQUAL(b[0], 0.5);
  BOOST_CHECK_EQUAL(b[1],-0.5);
  BOOST_CHECK_EQUAL(b[2], 1);
  BOOST_CHECK_EQUAL(b[3],-0.5);
  BOOST_CHECK_EQUAL(b[4], 0.5);



  b = (2*(A*x))*2;
  BOOST_CHECK_EQUAL(b[0], 4);
  BOOST_CHECK_EQUAL(b[1],-4);
  BOOST_CHECK_EQUAL(b[2], 8);
  BOOST_CHECK_EQUAL(b[3],-4);
  BOOST_CHECK_EQUAL(b[4], 4);

  b = 2*((A*x)*2);
  BOOST_CHECK_EQUAL(b[0], 4);
  BOOST_CHECK_EQUAL(b[1],-4);
  BOOST_CHECK_EQUAL(b[2], 8);
  BOOST_CHECK_EQUAL(b[3],-4);
  BOOST_CHECK_EQUAL(b[4], 4);

  b = (2*(A*x))/2;
  BOOST_CHECK_EQUAL(b[0], 1);
  BOOST_CHECK_EQUAL(b[1],-1);
  BOOST_CHECK_EQUAL(b[2], 2);
  BOOST_CHECK_EQUAL(b[3],-1);
  BOOST_CHECK_EQUAL(b[4], 1);



  b = (2*A)*x;
  BOOST_CHECK_EQUAL(b[0], 2);
  BOOST_CHECK_EQUAL(b[1],-2);
  BOOST_CHECK_EQUAL(b[2], 4);
  BOOST_CHECK_EQUAL(b[3],-2);
  BOOST_CHECK_EQUAL(b[4], 2);

  b = +((2*A)*x);
  BOOST_CHECK_EQUAL(b[0], 2);
  BOOST_CHECK_EQUAL(b[1],-2);
  BOOST_CHECK_EQUAL(b[2], 4);
  BOOST_CHECK_EQUAL(b[3],-2);
  BOOST_CHECK_EQUAL(b[4], 2);

  b += (2*A)*x;
  BOOST_CHECK_EQUAL(b[0], 4);
  BOOST_CHECK_EQUAL(b[1],-4);
  BOOST_CHECK_EQUAL(b[2], 8);
  BOOST_CHECK_EQUAL(b[3],-4);
  BOOST_CHECK_EQUAL(b[4], 4);



  b = A*(x*2);
  BOOST_CHECK_EQUAL(b[0], 2);
  BOOST_CHECK_EQUAL(b[1],-2);
  BOOST_CHECK_EQUAL(b[2], 4);
  BOOST_CHECK_EQUAL(b[3],-2);
  BOOST_CHECK_EQUAL(b[4], 2);

  b = +(A*(x*2));
  BOOST_CHECK_EQUAL(b[0], 2);
  BOOST_CHECK_EQUAL(b[1],-2);
  BOOST_CHECK_EQUAL(b[2], 4);
  BOOST_CHECK_EQUAL(b[3],-2);
  BOOST_CHECK_EQUAL(b[4], 2);

  b += A*(x*2);
  BOOST_CHECK_EQUAL(b[0], 4);
  BOOST_CHECK_EQUAL(b[1],-4);
  BOOST_CHECK_EQUAL(b[2], 8);
  BOOST_CHECK_EQUAL(b[3],-4);
  BOOST_CHECK_EQUAL(b[4], 4);



  BOOST_CHECK_THROW( b = A*c, AssertionException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_CRS_Expression )
{
  SparseVector<Real> x(5), b(5), r(5), c(3);
  SparseNonZeroPattern<Real> nz(5,5);
  TriDiagPattern(nz);
  SparseMatrix_CRS<Real> A(nz);

  Heat1D<Real>::init(A);

  x = 1;
  b = 2;
  x[2] = 2;

  r = b + A*x;
  BOOST_CHECK_EQUAL(r[0], 3);
  BOOST_CHECK_EQUAL(r[1], 1);
  BOOST_CHECK_EQUAL(r[2], 4);
  BOOST_CHECK_EQUAL(r[3], 1);
  BOOST_CHECK_EQUAL(r[4], 3);

  r = +(b + A*x);
  BOOST_CHECK_EQUAL(r[0], 3);
  BOOST_CHECK_EQUAL(r[1], 1);
  BOOST_CHECK_EQUAL(r[2], 4);
  BOOST_CHECK_EQUAL(r[3], 1);
  BOOST_CHECK_EQUAL(r[4], 3);

  r += b + A*x;
  BOOST_CHECK_EQUAL(r[0], 6);
  BOOST_CHECK_EQUAL(r[1], 2);
  BOOST_CHECK_EQUAL(r[2], 8);
  BOOST_CHECK_EQUAL(r[3], 2);
  BOOST_CHECK_EQUAL(r[4], 6);

  r = b - A*x;
  BOOST_CHECK_EQUAL(r[0], 1);
  BOOST_CHECK_EQUAL(r[1], 3);
  BOOST_CHECK_EQUAL(r[2], 0);
  BOOST_CHECK_EQUAL(r[3], 3);
  BOOST_CHECK_EQUAL(r[4], 1);

  r = +(b - A*x);
  BOOST_CHECK_EQUAL(r[0], 1);
  BOOST_CHECK_EQUAL(r[1], 3);
  BOOST_CHECK_EQUAL(r[2], 0);
  BOOST_CHECK_EQUAL(r[3], 3);
  BOOST_CHECK_EQUAL(r[4], 1);

  r += b - A*x;
  BOOST_CHECK_EQUAL(r[0], 2);
  BOOST_CHECK_EQUAL(r[1], 6);
  BOOST_CHECK_EQUAL(r[2], 0);
  BOOST_CHECK_EQUAL(r[3], 6);
  BOOST_CHECK_EQUAL(r[4], 2);

  r = -A*x + b;
  BOOST_CHECK_EQUAL(r[0], 1);
  BOOST_CHECK_EQUAL(r[1], 3);
  BOOST_CHECK_EQUAL(r[2], 0);
  BOOST_CHECK_EQUAL(r[3], 3);
  BOOST_CHECK_EQUAL(r[4], 1);

  r = 2*(b + A*x);
  BOOST_CHECK_EQUAL(r[0], 6);
  BOOST_CHECK_EQUAL(r[1], 2);
  BOOST_CHECK_EQUAL(r[2], 8);
  BOOST_CHECK_EQUAL(r[3], 2);
  BOOST_CHECK_EQUAL(r[4], 6);

  r = (b + A*x)*2;
  BOOST_CHECK_EQUAL(r[0], 6);
  BOOST_CHECK_EQUAL(r[1], 2);
  BOOST_CHECK_EQUAL(r[2], 8);
  BOOST_CHECK_EQUAL(r[3], 2);
  BOOST_CHECK_EQUAL(r[4], 6);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_CRS_scatterAdd_NonZeroPattern )
{

  SparseNonZeroPattern<Real> Pattern(5,5);

  DLA::MatrixD< Real > mtxLocal( {{1,-1},{-1,1}} );

  for ( int i = 1; i < 5; i++ )
  {
    int map[] = {i-1,i};
    Pattern.scatterAdd(mtxLocal,map,2);
  }

  SparseNonZeroPattern<Real> nz(5,5);
  TriDiagPattern(nz);
  SparseMatrix_CRS<Real> A1( nz ), A2( Pattern );

  //Initialize as a '1D heat equation' matrix
  Heat1D<Real>::init(A1);

  BOOST_REQUIRE_EQUAL(A1.m(), A2.m());

  for ( int i = 1; i < (int)A2.m()-1; i++ )
  {
    int map[] = {i-1,i};
    A2.scatterAdd(mtxLocal,map,2);
  }

  //Add the last map is reversed in order to tests a non-sorted map
  int map[] = {(int)A2.m()-1,(int)A2.m()-2};
  A2.scatterAdd(mtxLocal,map,2);

  DLA::MatrixD< Real > mtxBC( {{1}} );
  int mapBC[] = {0};
  A2.scatterAdd(mtxBC,mapBC,1);

  mapBC[0] = 4;
  A2.scatterAdd(mtxBC,mapBC,1);

  // check that A2 is correct based on comparison to A1
  for ( int i = 0; i < A1.m(); i++ )
  {
    BOOST_REQUIRE_EQUAL(A1.rowNonZero(i), A2.rowNonZero(i));
    for ( int j = 0; j < A1.rowNonZero(i); j++ )
      BOOST_CHECK_EQUAL(A1.sparseRow(i,j), A2.sparseRow(i,j));
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_CRS_scatterAdd_Heat )
{
  SparseNonZeroPattern<Real> nz(5,5);
  TriDiagPattern(nz);
  SparseMatrix_CRS<Real> A1( nz ), A2( nz );

  //Initialize as a '1D heat equation' matrix and check that is correct
  Heat1D<Real>::init(A1);

  BOOST_REQUIRE_EQUAL(A1.m(), A2.m());

  DLA::MatrixD< Real > mtxLocal( {{1,-1},{-1,1}} );

  for ( int i = 1; i < (int)A2.m()-1; i++ )
  {
    int map[] = {i-1,i};
    A2.scatterAdd(mtxLocal,map,2);
  }

  //Add the last map is reversed in order to tests a non-sorted map
  int map[] = {(int)A2.m()-1,(int)A2.m()-2};
  A2.scatterAdd(mtxLocal,map,2);

  DLA::MatrixD< Real > mtxBC( {{1}} );
  int mapBC[] = {0};
  A2.scatterAdd(mtxBC,mapBC,1);

  mapBC[0] = 4;
  A2.scatterAdd(mtxBC,mapBC,1);

  for ( int i = 0; i < A1.m(); i++ )
  {
    BOOST_REQUIRE_EQUAL(A1.rowNonZero(i), A2.rowNonZero(i));
    for ( int j = 0; j < A1.rowNonZero(i); j++ )
      BOOST_CHECK_EQUAL(A1.sparseRow(i,j), A2.sparseRow(i,j));
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_CRS_scatterAdd_Advection )
{

  SparseNonZeroPattern<Real> Pattern(5,5);

  DLA::MatrixD< Real > mtxLocal( {{1,0},{-1,0}} );

  for ( int i = 1; i < 5; i++ )
  {
    int map[] = {i-1,i};
    Pattern.scatterAdd(mtxLocal,map,2);
  }

  SparseNonZeroPattern<Real> nz(5,5);
  TriDiagPattern(nz);
  SparseMatrix_CRS<Real> A1( nz ), A2( Pattern );

  //Initialize as a '1D advection equation' matrix and check that is correct
  Advection1D<Real>::init(A1);

  BOOST_REQUIRE_EQUAL(A1.m(), A2.m());

  for ( int i = 1; i < (int)A2.m(); i++ )
  {
    int map[] = {i-1,i};
    A2.scatterAdd(mtxLocal,map,2);
  }

  DLA::MatrixD< Real > mtxBC( {{1}} );
  int mapBC[] = {(int)A2.m()-1};
  A2.scatterAdd(mtxBC,mapBC,1);

  for ( int i = 0; i < A1.m(); i++ )
  {
    BOOST_REQUIRE_EQUAL(A1.rowNonZero(i), A2.rowNonZero(i));
    for ( int j = 0; j < A1.rowNonZero(i); j++ )
      BOOST_CHECK_EQUAL(A1.sparseRow(i,j), A2.sparseRow(i,j));
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_CRS_scatterAdd_Transpose )
{

  SparseNonZeroPattern<Real> Pattern(5,5);

  DLA::MatrixD< Real > mtxLocal( {{1,0},{-1,0}} );

  for ( int i = 1; i < 5; i++ )
  {
    int map[] = {i-1,i};
    Pattern.scatterAdd(mtxLocal,map,2);
  }

  SparseNonZeroPattern<Real> nz(5,5);
  TriDiagPattern(nz);
  SparseMatrix_CRS<Real> A1( nz ), A2( Pattern );

  //Initialize as a '1D heat equation' matrix and check that is correct
  Advection1D<Real>::init_transpose(A1);

  //Transpose the scatter add operator
  //A2.transposeScatterAdd(true);
  SparseMatrix_CRS_Transpose< Real > A2T = Transpose(A2);

  BOOST_REQUIRE_EQUAL(A1.m(), A2T.m());

  for ( int i = 1; i < (int)A2T.m(); i++ )
  {
    int map[] = {i-1,i};
    A2T.scatterAdd(mtxLocal,map,2);
  }

  DLA::MatrixD< Real > mtxBC( {{1}} );
  int mapBC[] = {(int)A2.m()-1};
  A2T.scatterAdd(mtxBC,mapBC,1);

  //Check that A2 is transposed
  for ( int i = 0; i < A1.m(); i++ )
  {
    BOOST_REQUIRE_EQUAL(A1.rowNonZero(i), A2.rowNonZero(i));
    for ( int j = 0; j < A1.rowNonZero(i); j++ )
      BOOST_CHECK_EQUAL(A1.sparseRow(i,j), A2.sparseRow(i,j));
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_CRS_scatterGet_NonZeroPattern )
{

  SparseNonZeroPattern<int> Pattern(5,5);

  DLA::MatrixD< int > mtxLocal( {{1,-1},{-1,1}} );

  for ( int i = 1; i < 5; i++ )
  {
    int map[] = {i-1,i};
    Pattern.scatterAdd(mtxLocal,map,2);
  }

  // --------------------------------- //
  // scatterGet rectangular matrix
  // --------------------------------- //

  DLA::MatrixD< int > mtxLocalGet1(5,1);

  for (int i = 0; i < 5; i++)
  {
    int rowMap[] = {0,1,2,3,4};
    int colMap[] = {i};
    Pattern.scatterGet(mtxLocalGet1, rowMap, 5, colMap, 1);

    for (int j = 0; j < 5; j++)
    {
      if ( (j == (i-1)) || (j == i) || (j == (i+1)) )
        BOOST_CHECK_EQUAL(1, mtxLocalGet1(j,0));
      else
        BOOST_CHECK_EQUAL(0, mtxLocalGet1(j,0));
    }
  }

  // --------------------------------- //
  // scatterGet square matrix
  // --------------------------------- //

  DLA::MatrixD< int > mtxLocalGet2(5,5);

  {
    int rowMap[] = {0,1,2,3,4};
    int colMap[] = {0,1,2,3,4};
    Pattern.scatterGet(mtxLocalGet2, rowMap, 5, colMap, 5);

    for (int i = 0; i < 5; i++)
    {
      for (int j = 0; j < 5; j++)
      {
        if ( (j == (i-1)) || (j == i) || (j == (i+1)) )
          BOOST_CHECK_EQUAL(1, mtxLocalGet2(j,i));
        else
          BOOST_CHECK_EQUAL(0, mtxLocalGet2(j,i));
      }
    }
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_CRS_scatterGet_Heat )
{
  SparseNonZeroPattern<Real> nz(5,5);
  TriDiagPattern(nz);
  SparseMatrix_CRS<Real> A1( nz );

  //Initialize as a '1D heat equation' matrix
  Heat1D<Real>::init(A1);

  // scale A1 by some factor
  for (int i = 0; i < A1.getNumNonZero(); i++)
    A1[i] += 0.6;

  // --------------------------------- //
  // scatterGet rectangular matrix
  // --------------------------------- //

  DLA::MatrixD< Real > mtxLocalGet1(5,1);

  for (int i = 0; i < 5; i++)
  {
    int rowMap[] = {0,1,2,3,4};
    int colMap[] = {i};
    A1.scatterGet(mtxLocalGet1, rowMap, 5, colMap, 1);

#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << "Column = "<<i<< std::endl;
#endif

    for (int j = 0; j < 5; j++)
    {
      if ( (j == (i-1)) || (j == (i+1)) )
        BOOST_CHECK_EQUAL(-0.4, mtxLocalGet1(j,0));
      else if ( j == i )
        BOOST_CHECK_EQUAL(2.6, mtxLocalGet1(j,0));
      else
        BOOST_CHECK_EQUAL(0, mtxLocalGet1(j,0));
#ifdef DISPLAY_FOR_DEBUGGING
      std::cout << mtxLocalGet1(j,0) << std::endl;
#endif
    }
  }

  // --------------------------------- //
  // scatterGet square matrix
  // --------------------------------- //

  DLA::MatrixD< Real > mtxLocalGet2(5,5);

  {
    int rowMap[] = {0,1,2,3,4};
    int colMap[] = {0,1,2,3,4};
    A1.scatterGet(mtxLocalGet2, rowMap, 5, colMap, 5);

    for (int i = 0; i < 5; i++)
    {
#ifdef DISPLAY_FOR_DEBUGGING
      std::cout << "Column = "<<i<< std::endl;
#endif

      for (int j = 0; j < 5; j++)
      {
        if ( (j == (i-1)) || (j == (i+1)) )
          BOOST_CHECK_EQUAL(-0.4, mtxLocalGet2(j,i));
        else if ( j == i )
          BOOST_CHECK_EQUAL(2.6, mtxLocalGet2(j,i));
        else
          BOOST_CHECK_EQUAL(0, mtxLocalGet2(j,i));

#ifdef DISPLAY_FOR_DEBUGGING
        std::cout << mtxLocalGet1(j,0) << std::endl;
#endif
      }
    }
  }

  // --------------------------------- //
  // scatterGet unordered row/col mapping
  // --------------------------------- //

  {
    const int nrow = 3, ncol = 2;
    DLA::MatrixD< Real > mtxLocalGet3(nrow,ncol);

    int rowMap[] = {0,2,1};
    int colMap[] = {1,0};
    A1.scatterGet(mtxLocalGet3, rowMap, nrow, colMap, ncol);

    BOOST_CHECK_EQUAL(-0.4, mtxLocalGet3(0,0));    BOOST_CHECK_EQUAL( 2.6, mtxLocalGet3(0,1));
    BOOST_CHECK_EQUAL(-0.4, mtxLocalGet3(1,0));    BOOST_CHECK_EQUAL( 0.0, mtxLocalGet3(1,1));
    BOOST_CHECK_EQUAL( 2.6, mtxLocalGet3(2,0));    BOOST_CHECK_EQUAL(-0.4, mtxLocalGet3(2,1));

#ifdef DISPLAY_FOR_DEBUGGING
    for (int i = 0; i < nrow; i++)
      for (int j = 0; j < ncol; j++)
        std::cout << "A1["<<rowMap[i]<<", "<<colMap[j]<<"] = " << mtxLocalGet3(i,j) << std::endl;
#endif
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_CRS_To_MatrixD )
{


  SparseNonZeroPattern<Real> nz(5,5);
  TriDiagPattern(nz);
  SparseMatrix_CRS<Real> A1( nz );

  //Initialize as a '1D heat equation' matrix and check that is correct
  Advection1D<Real>::init(A1);

  // Create a dense matrix out of the sparse matrix
  DLA::MatrixD< Real > A2( A1 );

  BOOST_REQUIRE_EQUAL(A1.m(), A2.m());
  BOOST_REQUIRE_EQUAL(A1.n(), A2.n());

  for ( int i = 0; i < A1.m(); i++ )
  {
    int row_ptr = A1.get_row_ptr()[i];
    for ( int j = 0; j < A1.rowNonZero(i); j++ )
      BOOST_CHECK_EQUAL( A1.sparseRow(i,j), A2( i, A1.get_col_ind()[row_ptr+j] ) );
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseMatrix_CRS_ScalarMatrix )
{
  SparseNonZeroPattern<Real> nz(5,5);
  TriDiagPattern(nz);
  SparseMatrix_CRS<Real> A( nz );

  //Initialize as a '1D heat equation' matrix and check that is correct
  Heat1D<Real>::init(A);

  ScalarMatrix_CRS<int> sM( A );

  std::string SparseMatrix_CRS_Real("IO/SparseLinAlg/SparseMatrix_CRS_Real.mm");

  //Set the 2nd argument to false to regenerate the file
  output_test_stream output( SparseMatrix_CRS_Real, true );

  sM.WriteMatrixMarketFile(output);

  BOOST_CHECK( output.match_pattern() );

  //Set the 2nd argument to false to regenerate the file
  output_test_stream output2( SparseMatrix_CRS_Real, true );

  WriteMatrixMarketFile(A, output2);

  BOOST_CHECK( output2.match_pattern() );

  //Set the 2nd argument to false to regenerate the file
  output_test_stream output3( "IO/SparseLinAlg/SparseNonZeroPattern_Real.mm", true );

  WriteMatrixMarketFile(nz, output3);

  BOOST_CHECK( output3.match_pattern() );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
