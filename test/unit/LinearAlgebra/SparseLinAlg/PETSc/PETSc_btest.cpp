// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

//#define SLEEP_FOR_OUTPUT
#ifdef SLEEP_FOR_OUTPUT
#define SLEEP_MILLISECONDS 500
#include <chrono>
#include <thread>
#endif

#include <petscsys.h>

#include "SANS_CHECK_CLOSE_btest.h"
#include "tools/SANSnumerics.h"

#include "../../TriDiagPattern_btest.h"
#include "../../Heat1D_btest.h"
#include "../../Advection1D_btest.h"
#include "../../CustomJacobianEqSet_btest.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/ScalarMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"
#include "LinearAlgebra/BlockLinAlg/BlockLinAlg_Mul.h"

#include "LinearAlgebra/SparseLinAlg/PETSc/PETScSolver.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"


#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace SANS;
using namespace SANS::SLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( SparseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PETSc_ctor )
{
  typedef SparseMatrix_CRS<Real> Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  mpi::communicator world(PETSC_COMM_WORLD, mpi::comm_attach);

  PyDict d;

  PETScSolverParam::checkInputs(d);

  PetscInt N = 9;
  Vector_type q(N);

  Heat1DEquationSet<Matrix_type> f(std::make_shared<mpi::communicator>(world), 0, N, N, q, q);

  //This returns a shared pointer so no memory is lost
  PETScSolverParam::newSolver< Matrix_type >(d, f);

  {
    PyDict PreconditionerDict;
    PyDict PreconditionerILU;
    PyDict PETScDict;

    PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
    PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
    PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
    PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;

    PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
    PreconditionerDict[SLA::PreconditionerASMParam::params.Overlap] = 2;
    PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;
    PreconditionerDict[SLA::PreconditionerASMParam::params.PreconditionerSide] = SLA::PreconditionerASMParam::params.PreconditionerSide.Left;

    PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
    PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-9;
    PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 2000;
    PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
    PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
    PETScDict[SLA::PETScSolverParam::params.Verbose] = false;
    PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
    PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";

    PETScSolverParam::checkInputs(PETScDict);

    PETScSolver<Matrix_type> Solver(PETScDict, f);

    KSP ksp = Solver.getKSP();

    PCSide side;
    PETSc_STATUS( KSPGetPCSide(ksp, &side) );
    BOOST_CHECK_EQUAL(PC_LEFT, side);

    // Get linear solve parameters
    PetscReal RelativeTolerance, AbsoluteTolerance, DivergenceTolerance;
    PetscInt MaxIterations;
    PETSc_STATUS( KSPGetTolerances(ksp, &RelativeTolerance, &AbsoluteTolerance, &DivergenceTolerance, &MaxIterations) );
    BOOST_CHECK_EQUAL(1e-5, RelativeTolerance);
    BOOST_CHECK_EQUAL(1e-9, AbsoluteTolerance);
    BOOST_CHECK_EQUAL(2000, MaxIterations);

    // Get restart value
    PetscInt GMRES_restart;
    PETSc_STATUS( KSPGMRESGetRestart(ksp, &GMRES_restart) );
    BOOST_CHECK_EQUAL(300, GMRES_restart);


    // Get the preconditioner for the KSP
    PC pc;
    PETSc_STATUS( KSPGetPC(ksp, &pc) );

    // check the correct PC is set
    PCType pctype;
    PETSc_STATUS( PCGetType(pc, &pctype) );
    BOOST_CHECK_EQUAL(std::string(PCASM), std::string(pctype));

    //TODO: PETSc does not have PCASMGetOverlap
    //PetscInt overlap;
    //PETSc_STATUS( PCASMGetOverlap(pc, &overlap) );
    //BOOST_CHECK_EQUAL(2, overlap);

    PetscInt nlocal,first;
    KSP *subksp;     // array of local KSP contexts on this processor

    // Extract the array of KSP contexts for the local blocks
    PETSc_STATUS( PCASMGetSubKSP(pc, &nlocal, &first, &subksp) );

    // Loop over the local blocks, setting various KSP options for each block.
    for (PetscInt i = 0; i < nlocal; i++)
    {
      PCSide side;
      PETSc_STATUS( KSPGetPCSide(subksp[i], &side) );
      BOOST_CHECK_EQUAL(PC_RIGHT, side);

      PC subpc;
      PETSc_STATUS( KSPGetPC(subksp[i], &subpc) );
      PCType pctype;
      PETSc_STATUS( PCGetType(subpc, &pctype) );
      BOOST_CHECK_EQUAL(std::string(PCILU), std::string(pctype));

      PetscInt fill_level;
      PETSc_STATUS( PCFactorGetLevels(subpc, &fill_level) );
      BOOST_CHECK_EQUAL(4, fill_level);

      //TODO: PETSc does not have a function to get the MatOrdering
      //PETSc_STATUS( PCFactorSetMatOrderingType(subpc, MATORDERINGQMD) );
      //BOOST_CHECK_EQUAL(std::string(MATORDERINGQMD), std::string(pctype));
    }

  }
}

namespace CRS_Real
{
typedef SparseMatrix_CRS<Real> Matrix_type;
typedef VectorType<Matrix_type>::type Vector_type;

typedef boost::mpl::list< Heat1DEquationSet<Matrix_type>, Advection1DEquationSet<Matrix_type> > AlgEqSet_types;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( PETSc_Solve, AlgEqSet, AlgEqSet_types )
{
  mpi::communicator world(PETSC_COMM_WORLD, mpi::comm_attach);

  PetscInt N = 9;
  PetscInt n = PETSC_DECIDE;
  PetscInt nghost = 0;

  PyDict PreconditionerILU;
  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.K;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 1;

  PyDict PreconditionerDict;
  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.PreconditionerSide] = SLA::PreconditionerASMParam::params.PreconditionerSide.Left;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PyDict PETScDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = false;
  PETScDict[SLA::PETScSolverParam::params.Timing] = false;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-16;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-12;

  PETScSolverParam::checkInputs(PETScDict);

  Vector_type x_global(N);

  //-------------------------------------------------------------------//
  //  Solve the global problem on all processors as a truth reference  //
  //-------------------------------------------------------------------//
  {
    mpi::communicator comm = world.split(world.rank());

    Vector_type q(N);

    AlgEqSet f(std::make_shared<mpi::communicator>(comm), 0, N, N, q, q);

    PETScSolver<Matrix_type> Solver(PETScDict, f);

    Vector_type b(N), b2(N);

    //Create a residual vector
    for (int i = 0; i < b.m(); i++)
      b[i] = i + 1;

    // Set initial guess
    x_global = 0;

    //Solve the linear system.
    Solver.solve(b, x_global);

    //Compute the residual vector from the solution
    b2 = Solver.A()*x_global;

    //The residuals should now be the same!
    for (int i = 0; i < b2.m(); i++)
      BOOST_CHECK_CLOSE( b[i], b2[i], 1e-11 );
  }

  //----------------------------------------------------------------------//
  //  Solve the global problem in parallel distributed across processors  //
  //----------------------------------------------------------------------//

  for (int comm_size = 1; comm_size <= world.size(); comm_size++)
  {
    int color = world.rank() < comm_size ? 0 : 1;
    mpi::communicator comm = world.split(color);

    if (color == 1) continue;

    // Use PETSc to crate a partitioned system of equations
    PetscInt rstart, rend;
    Vec v;
    PETSc_STATUS( VecCreateMPI(comm, PETSC_DECIDE, N, &v) );
    PETSc_STATUS( VecGetLocalSize(v, &n) );
    PETSc_STATUS( VecGetOwnershipRange(v, &rstart, &rend) );
    PETSc_STATUS( VecDestroy(&v) );

    nghost = 0;
    if ( comm.size() > 1 && n > 0 )
      nghost = (rstart == 0 || rend == N) ? 1 : 2;

    Vector_type x(n+nghost), b(n), b2(n);

    Vector_type q(n+nghost);
    AlgEqSet f(std::make_shared<mpi::communicator>(comm), rstart, rend, N, b, q);

    PETScSolver<Matrix_type> Solver(PETScDict, f);

    //Set the matrix for the solver
    Solver.factorize();

#if 0 // For debugging
    if (comm.rank() == 0) std::cout << std::string(80, '*') << std::endl;
    for (int rank = 0; rank < comm.size(); rank++)
    {
      std::cout << std::flush;
      comm.barrier();
      if (rank != comm.rank()) continue;

      std::cout << std::setprecision(5);
      std::cout << std::string(80, '-') << std::endl;
      std::cout << "rank " << rank << " rstart=" << rstart << " rend=" << rend
                << " nghost=" << nghost << std::endl;
      std::cout << std::string(80, '-') << std::endl;

      //Temporary output just to check what is going on
      for (int i = 0; i < Solver.A().m(); i++)
      {
        int rnz = Solver.A().rowNonZero(i);
        for (int j = 0; j < rnz; j++)
          std::cout << Solver.A().sparseRow(i,j) << " ";
        std::cout << std::endl;
      }
      std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
    }
    std::cout << std::flush;
    comm.barrier();

    Solver.view();
#endif

    //Create a residual vector
    for (int i = 0; i < b.m(); i++)
      b[i] = rstart + i + 1;

    // set initial guess
    x = 0;

    //Test solving the same matrix twice, the symbolic factorization is only done once
    for (int solve = 0; solve < 2; solve++)
    {
      //Solve the linear system.
      Solver.backsolve(b, x);

      //Compute the residual vector from the solution
      b2 = Solver.A()*x;

      //The residuals should now be the same!
      for (int i = 0; i < b2.m(); i++)
        BOOST_CHECK_CLOSE( b[i], b2[i], 1e-11 );

      //Check that the parallel solution matches the global solution
      for (int i = rstart; i < rend; i++)
        BOOST_CHECK_CLOSE( x_global[i], x[i-rstart], 1e-11 );

#if 0 // For debugging
      for (int rank = 0; rank < comm.size(); rank++)
      {
        std::cout << std::flush;
        comm.barrier();
        if (rank != comm.rank()) continue;

        std::cout << std::setprecision(5);
        std::cout << std::string(80, '-') << std::endl;
        std::cout << "rank " << rank << std::endl;
        std::cout << std::string(80, '-') << std::endl;

        //Temporary output just to check what is going on
        for (int i = 0; i < b.m(); i++)
          std::cout << "x[" << i << "] = " << x[i]
                    << "\tx_global[" << i+rstart << "] = " << x_global[i+rstart]
                    << "\tb[" << i << "] = " << b[i]
                    << "\tb2[" << i << "] = "  << b2[i] << std::endl;

        // show the ghost values
        std::cout << std::string(10, '+') << std::endl;
        for (int i = n; i < n+nghost; i++)
          std::cout << "x[" << i << "] = " << x[i] << std::endl;
        std::cout << std::flush;
      }
      std::cout << std::flush;
      comm.barrier();
#endif
    }
  } // comm_size
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( PETSc_Solve_Transpose, AlgEqSet, AlgEqSet_types )
{
  typedef NonZeroPatternType<Matrix_type>::type NonZeroPattern;

  mpi::communicator world(PETSC_COMM_WORLD, mpi::comm_attach);

  PetscInt N = 9;
  PetscInt n = PETSC_DECIDE;
  PetscInt nghost = 0;

  PyDict PETScDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = false;

  PETScSolverParam::checkInputs(PETScDict);

  Vector_type x_global(N);

  //-------------------------------------------------------------------//
  //  Solve the global problem on all processors as a truth reference  //
  //-------------------------------------------------------------------//
  {
    mpi::communicator comm = world.split(world.rank());

    Vector_type q(N);

    AlgEqSet f(std::make_shared<mpi::communicator>(comm), 0, N, N, q, q);

    // construct the transposed matrix
    NonZeroPattern nzt(f.matrixSize()); f.jacobianTranspose(nzt);
    Matrix_type At(nzt);                f.jacobianTranspose(At);

    PETScSolver<Matrix_type> Solver(PETScDict, f, SLA::TransposeSolve);

    Vector_type b(N), b2(N);

    // Set initial guess
    x_global = 0;

    //Create a residual vector
    for (int i = 0; i < b.m(); i++)
      b[i] = i + 1;

    //Solve the linear system.
    Solver.solve(b, x_global);

    //Compute the residual vector from the solution
    b2 = At*x_global;

    //The residuals should now be the same!
    for (int i = 0; i < b2.m(); i++)
      BOOST_CHECK_CLOSE( b[i], b2[i], 1e-12 );
  }

  //----------------------------------------------------------------------//
  //  Solve the global problem in parallel distributed across processors  //
  //----------------------------------------------------------------------//

  for (int comm_size = 1; comm_size <= world.size(); comm_size++)
  {
    int color = world.rank() < comm_size ? 0 : 1;
    mpi::communicator comm = world.split(color);

    if (color == 1) continue;

    // Use PETSc to crate a partitioned system of equations
    PetscInt rstart, rend;
    Vec v;
    PETSc_STATUS( VecCreateMPI(comm, PETSC_DECIDE, N, &v) );
    PETSc_STATUS( VecGetLocalSize(v, &n) );
    PETSc_STATUS( VecGetOwnershipRange(v, &rstart, &rend) );
    PETSc_STATUS( VecDestroy(&v) );

    nghost = 0;
    if ( comm.size() > 1 && n > 0 )
      nghost = (rstart == 0 || rend == N) ? 1 : 2;

    Vector_type x(n+nghost), b(n), b2(n);

    Vector_type q(n+nghost);
    AlgEqSet f(std::make_shared<mpi::communicator>(comm), rstart, rend, N, b, q);

    PETScSolver<Matrix_type> Solver(PETScDict, f, SLA::TransposeSolve);

    //Set the matrix for the solver
    Solver.factorize();

#if 0 // For debugging
    for (int rank = 0; rank < comm.size(); rank++)
    {
      std::cout << std::flush;
      comm.barrier();
      if (rank != comm.rank()) continue;

      std::cout << std::setprecision(5);
      std::cout << std::string(80, '-') << std::endl;
      std::cout << "rank " << rank << " rstart=" << rstart << " rend=" << rend
                << " nghost=" << nghost << std::endl;
      std::cout << std::string(80, '-') << std::endl;

      //Temporary output just to check what is going on
      for (int i = 0; i < Solver.A().m(); i++)
      {
        int rnz = Solver.A().rowNonZero(i);
        for (int j = 0; j < rnz; j++)
          std::cout << Solver.A().sparseRow(i,j) << " ";
        std::cout << std::endl;
      }
      std::cout << std::flush;
    }
    std::cout << std::flush;
    comm.barrier();

    Solver.view();
#endif

    //Create a residual vector
    for (int i = 0; i < b.m(); i++)
      b[i] = rstart + i + 1;

    // set initial guess
    x = 0;

    //Test solving the same matrix twice, the symbolic factorization is only done once
    for (int solve = 0; solve < 2; solve++)
    {
      //Solve the linear system.
      Solver.backsolve(b, x);

      // TODO: Don't have transposed mat mul in parallel yet...
#if 0
      //Compute the residual vector from the solution
      b2 = Solver.A()*x;

      //The residuals should now be the same!
      for (int i = 0; i < b2.m(); i++)
        BOOST_CHECK_CLOSE( b[i], b2[i], 1e-12 );
#endif

      //Check that the parallel solution matches the global solution
      for (int i = rstart; i < rend; i++)
        BOOST_CHECK_CLOSE( x_global[i], x[i-rstart], 1e-12 );

#if 0 // For debugging
      for (int rank = 0; rank < comm.size(); rank++)
      {
        std::cout << std::flush;
        comm.barrier();
        if (rank != comm.rank()) continue;

        std::cout << std::setprecision(5);
        std::cout << std::string(80, '-') << std::endl;
        std::cout << "rank " << rank << std::endl;
        std::cout << std::string(80, '-') << std::endl;

        //Temporary output just to check what is going on
        for (int i = 0; i < b.m(); i++)
          std::cout << "x[" << i << "] = " << x[i]
                    << "\tx_global[" << i+rstart << "] = " << x_global[i+rstart]
                    << "\tb[" << i << "] = " << b[i]
                    << "\tb2[" << i << "] = "  << b2[i] << std::endl;

        // show the ghost values
        std::cout << std::string(10, '+') << std::endl;
        for (int i = n; i < n+nghost; i++)
          std::cout << "x[" << i << "] = " << x[i] << std::endl;
        std::cout << std::flush;
      }
      std::cout << std::flush;
      comm.barrier();
#endif
    }
  } // comm_size
}

}

#if 0
typedef boost::mpl::list< Heat1D< DLA::MatrixD<Real> >, Advection1D< DLA::MatrixD<Real> > > MatrixInit_Block_types;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( PETSc_Solve_Block, MatrixInit, MatrixInit_Block_types )
{
  PyDict d;

  PETScSolverParam::checkInputs(d);

  typedef DLA::MatrixD<Real> Block_type;
  typedef SparseMatrix_CRS<Block_type> Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  PETScSolver<Matrix_type> Solver(d);

  SparseNonZeroPattern<Block_type> nz(5,5);
  TriDiagPattern(nz,2);

  Matrix_type A( nz );
  Vector_type x(5, 2), b(5, 2), b2(5, 2);

  // set the initial guess
  x = 0;

  //Initialize the matrix here when it is a real matrix
  MatrixInit::init(A);

  //Create a residual vector
  b[0] = 0.5;
  b[1] = 1;
  b[2] = 2;
  b[3] = 1;
  b[4] = 0.5;

  //Set the matrix for the solver
  Solver.factorize(A);

  //Solve the linear system.
  x = Solver.backsolve(A)*b;

  //Compute the residual vector from the solution
  b2 = A*x;

  //The residuals should now be the same!
  for (int i = 0; i < b.m(); i++)
    for (int j = 0; j < b[i].m(); j++)
      BOOST_CHECK_CLOSE( b[i][j], b2[i][j], 1e-12 );

  //Temporary output just to check what is going on
  //for (int i = 0; i < b.size(); i++)
  //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;
}
#endif

#if 1

namespace CSR_MatrixS
{
typedef DLA::MatrixS<2,2,Real> MatrixS_type;
typedef SparseMatrix_CRS<MatrixS_type> Matrix_type;

typedef boost::mpl::list< Heat1DEquationSet<Matrix_type>, Advection1DEquationSet<Matrix_type> > AlgEqSet_types;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( PETSc_Solve_Static, AlgEqSet, AlgEqSet_types )
{
  typedef VectorType<Matrix_type>::type Vector_type;

  mpi::communicator world(PETSC_COMM_WORLD, mpi::comm_attach);

  PetscInt N = 9;
  PetscInt n = PETSC_DECIDE;
  PetscInt nghost = 0;

  PyDict PreconditionerILU;
  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.K;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 1;

  PyDict PreconditionerDict;
  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.PreconditionerSide] = SLA::PreconditionerASMParam::params.PreconditionerSide.Left;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PyDict PETScDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = false;
  PETScDict[SLA::PETScSolverParam::params.Timing] = false;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-16;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-12;

  PETScSolverParam::checkInputs(PETScDict);


  Vector_type x_global(N);

  //-------------------------------------------------------------------//
  //  Solve the global problem on all processors as a truth reference  //
  //-------------------------------------------------------------------//
  {
    mpi::communicator comm = world.split(world.rank());

    Vector_type q(N);

    AlgEqSet f(std::make_shared<mpi::communicator>(comm), 0, N, N, q, q);

    PETScSolver<Matrix_type> Solver(PETScDict, f);

    Vector_type b(N), b2(N);

    //Create a residual vector
    for (int i = 0; i < b.m(); i++)
    {
      //b[i] = i + 1;
      for (int j = 0; j < MatrixS_type::M; j++)
        b[i][j] = MatrixS_type::M*i + j + 1;
    }

    // Set initial guess
    x_global = 0;

    //Solve the linear system.
    Solver.solve(b, x_global);

    //Compute the residual vector from the solution
    b2 = Solver.A()*x_global;

    //The residuals should now be the same!
    for (int i = 0; i < b2.m(); i++)
      for (int j = 0; j < MatrixS_type::M; j++)
        BOOST_CHECK_CLOSE( b[i][j], b2[i][j], 1e-11 );
  }


  //-----------------------------------------------------------------------//
  //  Solve the global problem in parallel distributed across processors  //
  //-----------------------------------------------------------------------//

  for (int comm_size = 1; comm_size <= world.size(); comm_size++)
  {
    int color = world.rank() < comm_size ? 0 : 1;
    mpi::communicator comm = world.split(color);

    if (color == 1) continue;

    // Use PETSc to crate a partitioned system of equations
    PetscInt rstart, rend;
    Vec v;
    PETSc_STATUS( VecCreateMPI(comm, PETSC_DECIDE, N, &v) );
    PETSc_STATUS( VecGetLocalSize(v, &n) );
    PETSc_STATUS( VecGetOwnershipRange(v, &rstart, &rend) );
    PETSc_STATUS( VecDestroy(&v) );

    nghost = 0;
    if ( comm.size() > 1 && n > 0 )
      nghost = (rstart == 0 || rend == N) ? 1 : 2;

    Vector_type x(n+nghost), b(n), b2(n);

    Vector_type q(n+nghost);
    AlgEqSet f(std::make_shared<mpi::communicator>(comm), rstart, rend, N, b, q);

    PETScSolver<Matrix_type> Solver(PETScDict, f);

    //Set the matrix for the solver
    Solver.factorize();

    //Create a residual vector
    for (int i = 0; i < b.m(); i++)
    {
      //b[i] = (rstart + i) + 1;
      for (int j = 0; j < MatrixS_type::M; j++)
        b[i][j] = MatrixS_type::M*(rstart + i) + j + 1;
    }

    // set initial guess
    x = 0;

    //Test solving the same matrix twice, the symbolic factorization is only done once
    for (int solve = 0; solve < 2; solve++)
    {
      //Solve the linear system.
      Solver.backsolve(b, x);

      //Compute the residual vector from the solution
      b2 = Solver.A()*x;

      //The residuals should now be the same!
      for (int i = 0; i < b2.m(); i++)
        for (int j = 0; j < MatrixS_type::M; j++)
          BOOST_CHECK_CLOSE( b[i][j], b2[i][j], 1e-11 );

      //Check that the parallel solution matches the global solution
      for (int i = rstart; i < rend; i++)
        for (int j = 0; j < MatrixS_type::M; j++)
          BOOST_CHECK_CLOSE( x_global[i][j], x[i-rstart][j], 1e-11 );

#if 0 // For debugging
      for (int rank = 0; rank < comm.size(); rank++)
      {
        std::cout << std::flush;
        comm.barrier();
        std::cout << std::flush;
        comm.barrier();
        std::cout << std::flush;
        comm.barrier();
        std::cout << std::flush;
        comm.barrier();
        std::cout << std::flush;
        comm.barrier();
        std::cout << std::flush;
        comm.barrier();
        if (rank != comm.rank()) continue;

        std::cout << std::setprecision(3);
        std::cout << std::string(80, '-') << std::endl;
        std::cout << "rank " << rank << std::endl;
        std::cout << std::string(80, '-') << std::endl;

        //Temporary output just to check what is going on
        for (int i = 0; i < b.m(); i++)
          for (int j = 0; j < MatrixS_type::M; j++)
            std::cout << "x[" << i << "][" << j << "] = " << x[i][j]
                      << "\t\tx_global[" << i+rstart << "][" << j << "] = " << x_global[i+rstart][j]
                      << "\t\tb[" << i << "][" << j << "] = " << b[i][j]
                      << "\t\tb2[" << i << "] = "  << b2[i][j] << std::endl;

        // show the ghost values
        std::cout << std::string(10, '+') << std::endl;
        for (int i = n; i < n+nghost; i++)
          for (int j = 0; j < MatrixS_type::M; j++)
            std::cout << "x[" << i << "][" << j << "] = " << x[i][j] << std::endl;
        std::cout << std::flush;
        std::cout << std::flush;
        std::cout << std::flush;
        std::cout << std::flush;
      }
      std::cout << std::flush;
      comm.barrier();
#endif
    }
  } // comm_size

}
} // namespace CRS_MatrixS
#endif

#if 0
typedef DLA::MatrixD< DLA::MatrixS<2,2,Real> > Block_Static_type;
typedef boost::mpl::list< Heat1D<Block_Static_type>, Advection1D<Block_Static_type> > MatrixInit_Block_Static_types;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( PETSc_Solve_Block_Static, MatrixInit, MatrixInit_Block_Static_types )
{
  PyDict d;

  PETScSolverParam::checkInputs(d);

  typedef DLA::MatrixD< DLA::MatrixS<2,2,Real> > Block_type;
  typedef SparseMatrix_CRS<Block_type> Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  PETScSolver<Matrix_type> Solver(d);

  SparseNonZeroPattern<Block_type> nz(5,5);
  TriDiagPattern(nz,2);

  Matrix_type A( nz );
  Vector_type x(5, 2), b(5, 2), b2(5, 2);

  // set the initial guess
  x = 0;

  //Initialize the matrix here when it is a real matrix
  MatrixInit::init(A);

  //Create a residual vector
  b[0] = 0.5;
  b[1] = 1;
  b[2] = 2;
  b[3] = 1;
  b[4] = 0.5;

  //Set the matrix for the solver
  Solver.factorize(A);

  //Solve the linear system.
  x = Solver.backsolve(A)*b;

  //Compute the residual vector from the solution
  b2 = A*x;

  //The residuals should now be the same!
  for (int i = 0; i < b.m(); i++)
    for (int j = 0; j < b.block_m(i); j++)
      for (int k = 0; k < 2; k++)
        BOOST_CHECK_CLOSE( b[i][j][k], b2[i][j][k], 1e-12 );

  //Temporary output just to check what is going on
  //for (int i = 0; i < b.size(); i++)
  //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;
}

#endif

namespace MatrixD_CSR_MatrixS
{
typedef DLA::MatrixS<2,2,Real> MatrixS_type;
typedef DLA::MatrixD< SparseMatrix_CRS< MatrixS_type > > Matrix_type;

typedef boost::mpl::list< Heat1DEquationSet<Matrix_type>, Advection1DEquationSet<Matrix_type> > AlgEqSet_types;
//typedef boost::mpl::list< Heat1DEquationSet<Matrix_type> > AlgEqSet_types;
//typedef boost::mpl::list< Advection1DEquationSet<Matrix_type> > AlgEqSet_types;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( PETSc_Solve_MatrixD_CRS_MatrixS, AlgEqSet, AlgEqSet_types )
{
  typedef VectorType<Matrix_type>::type Vector_type;

  mpi::communicator world(PETSC_COMM_WORLD, mpi::comm_attach);

  PetscInt N = 9;
  PetscInt n = PETSC_DECIDE;
  PetscInt nghost = 0;

  PyDict DirectLU;
  DirectLU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
  DirectLU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;

  PyDict PreconditionerDict;
  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = DirectLU;

  PyDict PETScDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;

  PETScSolverParam::checkInputs(PETScDict);

  Vector_type x_global({{N}, {0}});

  //-------------------------------------------------------------------//
  //  Solve the global problem on all processors as a truth reference  //
  //-------------------------------------------------------------------//
  {
    mpi::communicator comm = world.split(world.rank());

    Vector_type q({{N}, {0}});

    AlgEqSet f(std::make_shared<mpi::communicator>(comm), 0, N, {{N,0}}, q, q);

    PETScSolver<Matrix_type> Solver(PETScDict, f);

    Vector_type b({{N}, {0}}), b2({{N}, {0}});

    //Create a residual vector
    for (int k = 0; k < b.m(); k++)
      for (int i = 0; i < b[k].m(); i++)
      {
        for (int j = 0; j < MatrixS_type::M; j++)
          b[k][i][j] = MatrixS_type::M*i + j + k + 1;
      }

    // Set initial guess
    x_global = 0;

    //Solve the linear system.
    Solver.solve(b, x_global);

    //Compute the residual vector from the solution
    b2 = Solver.A()*x_global;

    //The residuals should now be the same!
    for (int k = 0; k < b2.m(); k++)
      for (int i = 0; i < b2[k].m(); i++)
        for (int j = 0; j < MatrixS_type::M; j++)
          BOOST_CHECK_CLOSE( b[k][i][j], b2[k][i][j], 1e-11 );
  }

  //-----------------------------------------------------------------------//
  //  Solve the global problem in parallel distributed across processors  //
  //-----------------------------------------------------------------------//

  for (int comm_size = 1; comm_size <= world.size(); comm_size++)
  {
    int color = world.rank() < comm_size ? 0 : 1;
    mpi::communicator comm = world.split(color);

    if (color == 1) continue;

    // Use PETSc to crate a partitioned system of equations
    PetscInt rstart, rend;
    Vec v;
    PETSc_STATUS( VecCreateMPI(comm, PETSC_DECIDE, N, &v) );
    PETSc_STATUS( VecGetLocalSize(v, &n) );
    PETSc_STATUS( VecGetOwnershipRange(v, &rstart, &rend) );
    PETSc_STATUS( VecDestroy(&v) );

    nghost = 0;
    if ( comm.size() > 1 && n > 0 )
      nghost = (rstart == 0 || rend == N) ? 1 : 2;

    Vector_type x({{n+nghost}, {0}}), b({{n}, {0}}), b2({{n}, {0}});

    Vector_type q({{n+nghost}, {0}});
    AlgEqSet f(std::make_shared<mpi::communicator>(comm), rstart, rend, {{N,0}}, b, q);

    PETScSolver<Matrix_type> Solver(PETScDict, f);

    //Set the matrix for the solver
    Solver.factorize();

#if 0 // For debugging
    if (comm.rank() == 0) std::cout << std::string(80, '*') << std::endl;
    for (int rank = 0; rank < comm.size(); rank++)
    {
      std::cout << std::flush;
      comm.barrier();
      if (rank != comm.rank()) continue;

      std::cout << std::setprecision(5);
      std::cout << std::string(80, '-') << std::endl;
      std::cout << "rank " << rank << " rstart=" << rstart << " rend=" << rend
                << " nghost=" << nghost << std::endl;
      std::cout << std::string(80, '-') << std::endl;

      //Temporary output just to check what is going on
      for (int md = 0; md < Solver.A().m(); md++)
        for (int nd = 0; nd < Solver.A().n(); nd++)
        {
          if (Solver.A()(md,nd).getNumNonZero() == 0) continue;
          for (int i = 0; i < Solver.A()(md,nd).m(); i++)
          {
            std::cout << std::string(10, '+') << std::endl;
            int rnz = Solver.A()(md,nd).rowNonZero(i);
            for (int j = 0; j < rnz; j++)
              std::cout << Solver.A()(md,nd).sparseRow(i,j) << std::endl;
          }
        }
      std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
    }
    std::cout << std::flush;
    comm.barrier();

    Solver.view();
#endif

    //Create a residual vector
    for (int k = 0; k < b.m(); k++)
      for (int i = 0; i < b[k].m(); i++)
        for (int j = 0; j < MatrixS_type::M; j++)
          b[k][i][j] = MatrixS_type::M*(rstart + i) + j + k + 1;

    // set initial guess
    x = 0;

    //Test solving the same matrix twice, the symbolic factorization is only done once
    for (int solve = 0; solve < 2; solve++)
    {
      //Solve the linear system.
      Solver.backsolve(b, x);

      //Compute the residual vector from the solution
      b2 = Solver.A()*x;

      //The residuals should now be the same!
      for (int k = 0; k < b2.m(); k++)
        for (int i = 0; i < b2[k].m(); i++)
          for (int j = 0; j < MatrixS_type::M; j++)
            BOOST_CHECK_CLOSE( b[k][i][j], b2[k][i][j], 1e-11 );

      // TOOD: parallel check below is not yet properly setup
      BOOST_REQUIRE_EQUAL(2, x.m());
      BOOST_REQUIRE_EQUAL(0, x[1].m());

      //Check that the parallel solution matches the global solution
      for (int i = rstart; i < rend; i++)
        for (int j = 0; j < MatrixS_type::M; j++)
          BOOST_CHECK_CLOSE( x_global[0][i][j], x[0][i-rstart][j], 1e-11 );

#if 0 // For debugging
      for (int rank = 0; rank < comm.size(); rank++)
      {
        std::cout << std::flush;
        comm.barrier();
        std::cout << std::flush;
        comm.barrier();
        std::cout << std::flush;
        comm.barrier();
        std::cout << std::flush;
        comm.barrier();
        std::cout << std::flush;
        comm.barrier();
        std::cout << std::flush;
        comm.barrier();
        if (rank != comm.rank()) continue;

        std::cout << std::setprecision(3);
        std::cout << std::string(80, '-') << std::endl;
        std::cout << "rank " << rank << std::endl;
        std::cout << std::string(80, '-') << std::endl;

        //Temporary output just to check what is going on
        for (int k = 0; k < b.m(); k++)
          for (int i = 0; i < b[k].m(); i++)
            for (int j = 0; j < MatrixS_type::M; j++)
              std::cout << "x[" << k << "][" << i << "][" << j << "] = " << x[k][i][j]
                        << "\t\tx_global[" << k << "][" << i+rstart << "][" << j << "] = " << x_global[k][i+rstart][j]
                        << "\t\tb[" << k << "][" << i << "][" << j << "] = " << b[k][i][j]
                        << "\t\tb2[" << k << "][" << i << "] = "  << b2[k][i][j] << std::endl;

        // show the ghost values
        std::cout << std::string(10, '+') << std::endl;
        for (int i = n; i < n+nghost; i++)
          for (int j = 0; j < MatrixS_type::M; j++)
            std::cout << "x[0][" << i << "][" << j << "] = " << x[0][i][j] << std::endl;
        std::cout << std::flush;
        std::cout << std::flush;
        std::cout << std::flush;
        std::cout << std::flush;
      }
      std::cout << std::flush;
      comm.barrier();
#endif
    }
  } // comm_size

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( PETSc_Solve_MatrixD_2x2_CRS_MatrixS, AlgEqSet, AlgEqSet_types )
{
  typedef VectorType<Matrix_type>::type Vector_type;

  mpi::communicator world(PETSC_COMM_WORLD, mpi::comm_attach);

  PetscInt N0 = 4;
  PetscInt N1 = 6;
  PetscInt n = 0, n0 = -1, n1 = -1;

  PyDict PETScDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = false;

  PETScSolverParam::checkInputs(PETScDict);

  Vector_type x_global({{N0}, {N1}});

  //-------------------------------------------------------------------//
  //  Solve the global problem on all processors as a truth reference  //
  //-------------------------------------------------------------------//
  {
    mpi::communicator comm = world.split(world.rank());

    Vector_type q({{N0}, {N1}}), b({{N0}, {N1}}), b2({{N0}, {N1}});

    AlgEqSet f(std::make_shared<mpi::communicator>(comm), 0, N0+N1, {{N0,N1}}, q, q);

    PETScSolver<Matrix_type> Solver(PETScDict, f);

    //Create a residual vector
    int k = 0;
    for (int i = 0; i < b[k].m(); i++)
      for (int j = 0; j < MatrixS_type::M; j++)
        b[k][i][j] = MatrixS_type::M*i + j + 1;

    k = 1;
    for (int i = 0; i < b[k].m(); i++)
      for (int j = 0; j < MatrixS_type::M; j++)
        b[k][i][j] = MatrixS_type::M*(N0+i) + j + 1;

    // Set initial guess
    x_global = 0;

    //Solve the linear system.
    Solver.solve(b, x_global);

    //Compute the residual vector from the solution
    b2 = Solver.A()*x_global;

    //The residuals should now be the same!
    for (int k = 0; k < b2.m(); k++)
      for (int i = 0; i < b2[k].m(); i++)
        for (int j = 0; j < MatrixS_type::M; j++)
          BOOST_CHECK_CLOSE( b[k][i][j], b2[k][i][j], 1e-11 );
  }

  //-----------------------------------------------------------------------//
  //  Solve the global problem in parallel distributed across processors  //
  //-----------------------------------------------------------------------//

  for (int comm_size = 1; comm_size <= world.size(); comm_size++)
  {
    int color = world.rank() < comm_size ? 0 : 1;
    mpi::communicator comm = world.split(color);

    if (color == 1) continue;

    // Use PETSc to crate a partitioned system of equations
    PetscInt rstart, rend;
    Vec v;
    PETSc_STATUS( VecCreateMPI(comm, PETSC_DECIDE, N0+N1, &v) );
    PETSc_STATUS( VecGetLocalSize(v, &n) );
    PETSc_STATUS( VecGetOwnershipRange(v, &rstart, &rend) );
    PETSc_STATUS( VecDestroy(&v) );

    PetscInt n0ghost = 0, n1ghost = 0;

    n0 = n1 = 0;
    if (rstart < N0 && rend <= N0)
    {
      n0 = rend - rstart;
      n1 = 0;

      if (comm.size() > 1 && n > 0)
      {
        n0ghost = (rstart == 0) ? 1 : 2;
        if ( rend == N0 )
        {
          n1ghost = 1;
          n0ghost--;
        }
      }
    }
    else if (rstart >= N0)
    {
      n0 = 0;
      n1 = rend - rstart;

      if (comm.size() > 1 && n > 0)
      {
        n1ghost = (rend == N0+N1) ? 1 : 2;
        if ( rstart == N0 )
        {
          n0ghost = 1;
          n1ghost--;
        }
      }
    }
    else
    {
      n0 = N0 - rstart;
      n1 = rend - N0;
      if (comm.size() > 1 && n > 0) n0ghost = (rstart == 0  ) ? 0 : 1;
      if (comm.size() > 1 && n > 0) n1ghost = (rend == N0+N1) ? 0 : 1;
    }

    BOOST_REQUIRE_EQUAL(n0+n1, rend - rstart);

    Vector_type x({{n0+n0ghost}, {n1+n1ghost}}), b({{n0}, {n1}}), b2({{n0}, {n1}});

    Vector_type q({{n0+n0ghost}, {n1+n1ghost}});
    AlgEqSet f(std::make_shared<mpi::communicator>(comm), rstart, rend, {{N0,N1}}, b, q);

    PETScSolver<Matrix_type> Solver(PETScDict, f);

    //Set the matrix for the solver
    Solver.factorize();

#if 0 // For debugging
    if (comm.rank() == 0) std::cout << std::string(80, '*') << std::endl;
    for (int rank = 0; rank < comm.size(); rank++)
    {
      std::cout << std::flush;
      comm.barrier();
      if (rank != comm.rank()) continue;

      std::cout << std::setprecision(5);
      std::cout << std::string(80, '-') << std::endl;
      std::cout << "rank " << rank << " rstart=" << rstart << " rend=" << rend
                << " n0=" << n0 << " n0ghost=" << n0ghost
                << " n1=" << n1 << " n1ghost=" << n1ghost << std::endl;
      std::cout << std::string(80, '-') << std::endl;

      //Temporary output just to check what is going on
      for (int md = 0; md < Solver.A().m(); md++)
        for (int nd = 0; nd < Solver.A().n(); nd++)
        {
          if (Solver.A()(md,nd).getNumNonZero() == 0) continue;
          for (int i = 0; i < Solver.A()(md,nd).m(); i++)
          {
            std::cout << std::string(10, '+') << std::endl;
            int rnz = Solver.A()(md,nd).rowNonZero(i);
            for (int j = 0; j < rnz; j++)
              std::cout << Solver.A()(md,nd).sparseRow(i,j) << std::endl;
          }
        }
      std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
    }
    std::cout << std::flush;
    comm.barrier();

    Solver.view();
#endif

    //Create a residual vector
    if (rstart < N0 && rend <= N0)
    {
      int k = 0;
      for (int i = 0; i < b[k].m(); i++)
        for (int j = 0; j < MatrixS_type::M; j++)
          b[k][i][j] = MatrixS_type::M*(rstart + i) + j + 1;
    }
    else if (rstart >= N0)
    {
      int k = 1;
      for (int i = 0; i < b[k].m(); i++)
        for (int j = 0; j < MatrixS_type::M; j++)
          b[k][i][j] = MatrixS_type::M*(rstart + i) + j + 1;
    }
    else
    {
      int k = 0;
      for (int i = 0; i < b[k].m(); i++)
        for (int j = 0; j < MatrixS_type::M; j++)
          b[k][i][j] = MatrixS_type::M*(rstart + i) + j + 1;

      k = 1;
      for (int i = 0; i < b[k].m(); i++)
        for (int j = 0; j < MatrixS_type::M; j++)
          b[k][i][j] = MatrixS_type::M*(N0 + i) + j + 1;
    }

    // set initial guess
    x = 0;

    //Test solving the same matrix twice, the symbolic factorization is only done once
    for (int solve = 0; solve < 2; solve++)
    {
      //Solve the linear system.
      Solver.backsolve(b, x);

      //Compute the residual vector from the solution
      b2 = Solver.A()*x;

      //The residuals should now be the same!
      for (int k = 0; k < b2.m(); k++)
        for (int i = 0; i < b2[k].m(); i++)
          for (int j = 0; j < MatrixS_type::M; j++)
            BOOST_CHECK_CLOSE( b[k][i][j], b2[k][i][j], 1e-11 );

      //Check that the parallel solution matches the global solution
      if (rstart < N0 && rend <= N0)
      {
        for (int i = rstart; i < rend; i++)
          for (int j = 0; j < MatrixS_type::M; j++)
            BOOST_CHECK_CLOSE( x_global[0][i][j], x[0][i-rstart][j], 1e-11 );
      }
      else if (rstart >= N0)
      {
        for (int i = rstart; i < rend; i++)
          for (int j = 0; j < MatrixS_type::M; j++)
            BOOST_CHECK_CLOSE( x_global[1][i-N0][j], x[1][i-rstart][j], 1e-11 );
      }
      else
      {
        for (int i = rstart; i < N0; i++)
          for (int j = 0; j < MatrixS_type::M; j++)
            BOOST_CHECK_CLOSE( x_global[0][i][j], x[0][i-rstart][j], 1e-11 );

        for (int i = 0; i < n1; i++)
          for (int j = 0; j < MatrixS_type::M; j++)
            BOOST_CHECK_CLOSE( x_global[1][i][j], x[1][i][j], 1e-11 );
      }


#if 0 // For debugging
      for (int rank = 0; rank < comm.size(); rank++)
      {
        std::cout << std::flush;
        comm.barrier();
        std::cout << std::flush;
        comm.barrier();
        std::cout << std::flush;
        comm.barrier();
        std::cout << std::flush;
        comm.barrier();
        std::cout << std::flush;
        comm.barrier();
        std::cout << std::flush;
        comm.barrier();
        if (rank != comm.rank()) continue;

        std::cout << std::setprecision(3);
        std::cout << std::string(80, '-') << std::endl;
        std::cout << "rank " << rank << std::endl;
        std::cout << std::string(80, '-') << std::endl;

        if (rstart < N0 && rend <= N0)
        {
          int k = 0;
          for (int i = 0; i < b[k].m(); i++)
            for (int j = 0; j < MatrixS_type::M; j++)
              std::cout << "x[" << k << "][" << i << "][" << j << "] = " << x[k][i][j]
                        << "\t\tx_global[" << k << "][" << i+rstart << "][" << j << "] = " << x_global[k][i+rstart][j]
                        << "\t\tb[" << k << "][" << i << "][" << j << "] = " << b[k][i][j]
                        << "\t\tb2[" << k << "][" << i << "] = "  << b2[k][i][j] << std::endl;
        }
        else if (rstart >= N0)
        {
          int k = 1;
          for (int i = 0; i < b[k].m(); i++)
            for (int j = 0; j < MatrixS_type::M; j++)
              std::cout << "x[" << k << "][" << i << "][" << j << "] = " << x[k][i][j]
                        << "\t\tx_global[" << k << "][" << i+rstart-N0 << "][" << j << "] = " << x_global[k][i+rstart-N0][j]
                        << "\t\tb[" << k << "][" << i << "][" << j << "] = " << b[k][i][j]
                        << "\t\tb2[" << k << "][" << i << "] = "  << b2[k][i][j] << std::endl;
        }
        else
        {
          //Temporary output just to check what is going on
          int k = 0;
          for (int i = 0; i < b[k].m(); i++)
            for (int j = 0; j < MatrixS_type::M; j++)
              std::cout << "x[" << k << "][" << i << "][" << j << "] = " << x[k][i][j]
                        << "\t\tx_global[" << k << "][" << i+rstart << "][" << j << "] = " << x_global[k][i+rstart][j]
                        << "\t\tb[" << k << "][" << i << "][" << j << "] = " << b[k][i][j]
                        << "\t\tb2[" << k << "][" << i << "] = "  << b2[k][i][j] << std::endl;
          k = 1;
          for (int i = 0; i < b[k].m(); i++)
            for (int j = 0; j < MatrixS_type::M; j++)
              std::cout << "x[" << k << "][" << i << "][" << j << "] = " << x[k][i][j]
                        << "\t\tx_global[" << k << "][" << i << "][" << j << "] = " << x_global[k][i][j]
                        << "\t\tb[" << k << "][" << i << "][" << j << "] = " << b[k][i][j]
                        << "\t\tb2[" << k << "][" << i << "] = "  << b2[k][i][j] << std::endl;
        }

        // show the ghost values
        //std::cout << std::string(10, '+') << std::endl;
        //for (int i = n; i < n+nghost; i++)
        //  for (int j = 0; j < MatrixS_type::M; j++)
        //    std::cout << "x[0][" << i << "][" << j << "] = " << x[0][i][j] << std::endl;
        std::cout << std::flush;
        std::cout << std::flush;
        std::cout << std::flush;
        std::cout << std::flush;
      }
      std::cout << std::flush;
      comm.barrier();
#endif
    }
  } // comm_size

}

} // namespace MatrixD_CSR_MatrixS

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( PETSc_Solve_MatrixD_CRS_Static, MatrixInit, MatrixInit_Static_types )
{
  PyDict d;

  PETScSolverParam::checkInputs(d);

  typedef DLA::MatrixS<2,2,Real> Block_type;
  typedef DLA::MatrixD< SparseMatrix_CRS<Block_type> > Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  PETScSolver<Matrix_type> Solver(d);

  DLA::MatrixD< SparseNonZeroPattern<Block_type> > nz = {{ {5,5}, {5,5} },
                                                         { {5,5}, {5,5} }};

  Vector_type x = {{5},{5}}, b = {{5},{5}}, b2 = {{5},{5}};

  // set the initial guess
  x = 0;

  for (int i = 0; i < 2; i++)
    TriDiagPattern(nz(i,i));

  Matrix_type A( nz );

  //Initialize the matrix here when it is a real matrix
  for (int i = 0; i < 2; i++)
    MatrixInit::init(A(i,i));

  //Set the matrix for the solver
  Solver.factorize(A);

  //Create a residual vector
  for (int i = 0; i < 2; i++)
  {
    b[i][0] = 0.5;
    b[i][1] = 1;
    b[i][2] = 2;
    b[i][3] = 1;
    b[i][4] = 0.5;
  }

  //Solve the linear system.
  x = Solver.backsolve(A)*b;

  //Compute the residual vector from the solution
  b2 = A*x;

  //The residuals should now be the same!
  for (int k = 0; k < b.m(); k++)
    for (int i = 0; i < b[k].m(); i++)
      for (int j = 0; j < 2; j++)
        BOOST_CHECK_CLOSE( b[k][i][j], b2[k][i][j], 1e-12 );

  //Temporary output just to check what is going on
  //for (int i = 0; i < b.size(); i++)
  //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;


  //Create the off diagonal sparse matrices
  nz(0,1).add(4,0);
  nz(1,0).add(0,4);

  Matrix_type A2( nz );

  //Initialize the matrix here when it is a real matrix
  for (int i = 0; i < 2; i++)
    MatrixInit::init(A2(i,i));

  for ( int i = 0; i < 2; i++ )
  {
    A2(0,1).sparseRow(4,0)(i,i) = -1;
    if (boost::is_same<Heat1D<Block_type>,MatrixInit>::value)
      A2(1,0).sparseRow(0,0)(i,i) = -1;
  }

  //Set the matrix for the solver
  Solver.factorize(A2);

  //Solve the linear system.
  x = Solver.backsolve(A2)*b;

  //Compute the residual vector from the solution
  b2 = A2*x;

  //The residuals should now be the same!
  for (int k = 0; k < b.m(); k++)
    for (int i = 0; i < b[k].m(); i++)
      for (int j = 0; j < 2; j++)
        BOOST_CHECK_CLOSE( b[k][i][j], b2[k][i][j], 2e-12 );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( PETSc_Solve_MatrixD_CRS_Block, MatrixInit, MatrixInit_Block_types )
{
  PyDict d;

  PETScSolverParam::checkInputs(d);

  typedef DLA::MatrixD<Real> Block_type;
  typedef DLA::MatrixD< SparseMatrix_CRS<Block_type> > Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  PETScSolver<Matrix_type> Solver(d);

  DLA::MatrixD< SparseNonZeroPattern<Block_type> > nz = {{ {5,5}, {5,5} },
                                                         { {5,5}, {5,5} }};

  Vector_type x = {{5,2},{5,2}}, b = {{5,2},{5,2}}, b2 = {{5,2},{5,2}};

  // set the initial guess
  x = 0;

  for (int i = 0; i < 2; i++)
    TriDiagPattern(nz(i,i),2);

  Matrix_type A( nz );

  //Initialize the matrix here when it is a real matrix
  for (int i = 0; i < 2; i++)
    MatrixInit::init(A(i,i));

  //Set the matrix for the solver
  Solver.factorize(A);

  //Create a residual vector
  for (int i = 0; i < 2; i++)
  {
    b[i][0] = 0.5;
    b[i][1] = 1;
    b[i][2] = 2;
    b[i][3] = 1;
    b[i][4] = 0.5;
  }

  //Solve the linear system.
  x = Solver.backsolve(A)*b;

  //Compute the residual vector from the solution
  b2 = A*x;

  //The residuals should now be the same!
  for (int k = 0; k < b.m(); k++)
    for (int i = 0; i < b[k].m(); i++)
      for (int j = 0; j < b[k][i].m(); j++)
        BOOST_CHECK_CLOSE( b[k][i][j], b2[k][i][j], 1e-12 );

  //Temporary output just to check what is going on
  //for (int i = 0; i < b.size(); i++)
  //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;


  //Create the off diagonal sparse matrices
  nz(0,1).add(4,0,2,2);
  nz(1,0).add(0,4,2,2);

  Matrix_type A2( nz );

  //Initialize the matrix here when it is a real matrix
  for (int i = 0; i < 2; i++)
    MatrixInit::init(A2(i,i));

  A2(0,1).sparseRow(4,0) = -1;
  if (boost::is_same<Heat1D<Real>,MatrixInit>::value)
    A2(1,0).sparseRow(0,0) = -1;

  //Set the matrix for the solver
  Solver.factorize(A2);

  //Solve the linear system.
  x = Solver.backsolve(A2)*b;

  //Compute the residual vector from the solution
  b2 = A2*x;

  //The residuals should now be the same!
  for (int k = 0; k < b.m(); k++)
    for (int i = 0; i < b[k].m(); i++)
      for (int j = 0; j < b[k][i].m(); j++)
        BOOST_CHECK_CLOSE( b[k][i][j], b2[k][i][j], 2e-12 );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( PETSc_Solve_MatrixD_CRS_Block_Static, MatrixInit, MatrixInit_Block_Static_types )
{
  PyDict d;

  PETScSolverParam::checkInputs(d);

  typedef DLA::MatrixD< DLA::MatrixS<2,2,Real> > Block_type;
  typedef DLA::MatrixD< SparseMatrix_CRS<Block_type> > Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  PETScSolver<Matrix_type> Solver(d);

  DLA::MatrixD< SparseNonZeroPattern<Block_type> > nz = {{ {5,5}, {5,5} },
                                                         { {5,5}, {5,5} }};

  Vector_type x = {{5,2},{5,2}}, b = {{5,2},{5,2}}, b2 = {{5,2},{5,2}};

  // set the initial guess
  x = 0;

  for (int i = 0; i < 2; i++)
    TriDiagPattern(nz(i,i),2);

  Matrix_type A( nz );

  //Initialize the matrix here when it is a real matrix
  for (int i = 0; i < 2; i++)
    MatrixInit::init(A(i,i));

  //Create a residual vector
  for (int i = 0; i < 2; i++)
  {
    b[i][0] = 0.5;
    b[i][1] = 1;
    b[i][2] = 2;
    b[i][3] = 1;
    b[i][4] = 0.5;
  }

  //Set the matrix for the solver
  Solver.factorize(A);

  //Solve the linear system.
  x = Solver.backsolve(A)*b;

  //Compute the residual vector from the solution
  b2 = A*x;

  //The residuals should now be the same!
  for (int jd = 0; jd < b.m(); jd++)
    for (int k = 0; k < b[jd].m(); k++)
      for (int i = 0; i < b[jd][k].m(); i++)
        for (int j = 0; j < 2; j++)
          BOOST_CHECK_CLOSE( b[jd][k][i][j], b2[jd][k][i][j], 1e-12 );

  //Temporary output just to check what is going on
  //for (int i = 0; i < b.size(); i++)
  //  std::cout << "b[" << i << "] = " << b[i] << "\tb2[" << i << "] = " << b2[i] << "\tx[" << i << "] = "  << x[i] << std::endl;


  //Create the off diagonal sparse matrices
  nz(0,1).add(4,0,2,2);
  nz(1,0).add(0,4,2,2);

  Matrix_type A2( nz );

  //Initialize the matrix here when it is a real matrix
  for (int i = 0; i < 2; i++)
    MatrixInit::init(A2(i,i));

  for ( int i = 0; i < 2; i++ )
    for ( int j = 0; j < 2; j++ )
    {
      A2(0,1).sparseRow(4,0)(i,i)(j,j) = -1;
      if (boost::is_same<Heat1D<Block_type>,MatrixInit>::value)
        A2(1,0).sparseRow(0,0)(i,i)(j,j) = -1;
    }

  //Set the matrix for the solver
  Solver.factorize(A2);

  //Solve the linear system.
  x = Solver.backsolve(A2)*b;

  //Compute the residual vector from the solution
  b2 = A2*x;

  //The residuals should now be the same!
  for (int jd = 0; jd < b.m(); jd++)
    for (int k = 0; k < b[jd].m(); k++)
      for (int i = 0; i < b[jd][k].m(); i++)
        for (int j = 0; j < 2; j++)
          BOOST_CHECK_CLOSE( b[jd][k][i][j], b2[jd][k][i][j], 2e-12 );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PETSc_Solve_MatrixBlock_Real_2x2 )
{
  PyDict d;

  PETScSolverParam::checkInputs(d);

  typedef BLA::MatrixBlock_2x2<
      DLA::MatrixD<SparseMatrix_CRS<Real>>, DLA::MatrixD<SparseMatrix_CRS<Real>>,
      DLA::MatrixD<SparseMatrix_CRS<Real>>, DLA::MatrixD<SparseMatrix_CRS<Real>> > Matrix_type;

  typedef BLA::MatrixBlock_2x2<
      DLA::MatrixD<SparseNonZeroPattern<Real>>, DLA::MatrixD<SparseNonZeroPattern<Real>>,
      DLA::MatrixD<SparseNonZeroPattern<Real>>, DLA::MatrixD<SparseNonZeroPattern<Real>> > BlockMatrixNZType;

  typedef VectorType<Matrix_type>::type Vector_type;

  PETScSolver<Matrix_type> Solver(d);

  const int m = 3;
  const int n = 4;

  DLA::MatrixD< SparseNonZeroPattern<Real> > nz00 = {{ {m,m} }};
  DLA::MatrixD< SparseNonZeroPattern<Real> > nz01 = {{ {m,n} }};

  DLA::MatrixD< SparseNonZeroPattern<Real> > nz10 = {{ {n,m} }};
  DLA::MatrixD< SparseNonZeroPattern<Real> > nz11 = {{ {n,n} }};

  BlockMatrixNZType nz( nz00, nz01,
                        nz10, nz11 );

  Vector_type x( { SparseVectorSize(m) },
                 { SparseVectorSize(n) } );

  Vector_type b( { SparseVectorSize(m) },
                 { SparseVectorSize(n) } );

  Vector_type b2( { SparseVectorSize(m) },
                  { SparseVectorSize(n) } );

  // set the initial guess
  x = 0;

  TriDiagPattern(nz.m00(0,0));
  nz.m01(0,0).add(m-1,0);
  nz.m10(0,0).add(0,m-1);
  TriDiagPattern(nz.m11(0,0));

  Matrix_type A( nz );

  //Initialize the matrix here when it is a real matrix
  Heat1D< Real >::init(A.m00(0,0));
  A.m01(0,0).slowAccess(m-1,0) = -1;
  A.m10(0,0).slowAccess(0,m-1) = -1;
  Heat1D< Real >::init(A.m11(0,0));

#if 0
  ScalarMatrix_CRS<int> a(A);
  std::fstream file("tmp/block_2x2.mtx", std::ios::out);
  a.WriteMatrixMarketFile(file);
#endif

  //Create a residual vector
  b = 4;
  b.v0[0][2] = 2;
  b.v1 = 3;

  //Set the matrix for the solver
  Solver.factorize(A);

  //Solve the linear system.
  x = Solver.backsolve(A)*b;

  //Compute the residual vector from the solution
  b2 = A*x;

  //The residuals should now be the same!
  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      SANS_CHECK_CLOSE(b2.v0[k][j], b.v0[k][j], small_tol, close_tol);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      SANS_CHECK_CLOSE(b2.v1[k][j], b.v1[k][j], small_tol, close_tol);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PETSc_Solve_MatrixBlock_2x2 )
{
  PyDict d;

  PETScSolverParam::checkInputs(d);

  static const int M = 2;
  static const int N = 2;

  typedef BLA::MatrixBlock_2x2<
      DLA::MatrixD<SparseMatrix_CRS<DLA::MatrixS<M,N,Real>>>, DLA::MatrixD<SparseMatrix_CRS<DLA::MatrixS<M,1,Real>>>,
      DLA::MatrixD<SparseMatrix_CRS<DLA::MatrixS<1,N,Real>>>, DLA::MatrixD<SparseMatrix_CRS<Real>>                  > Matrix_type;

  typedef BLA::MatrixBlock_2x2<
      DLA::MatrixD<SparseNonZeroPattern<DLA::MatrixS<M,N,Real>>>, DLA::MatrixD<SparseNonZeroPattern<DLA::MatrixS<M,1,Real>>>,
      DLA::MatrixD<SparseNonZeroPattern<DLA::MatrixS<1,N,Real>>>, DLA::MatrixD<SparseNonZeroPattern<Real>>                  > BlockMatrixNZType;

  typedef VectorType<Matrix_type>::type Vector_type;

  PETScSolver<Matrix_type> Solver(d);

  const int m = 3;
  const int n = 4;

  DLA::MatrixD< SparseNonZeroPattern<DLA::MatrixS<M,N,Real>> > nz00 = {{ {m,m} }};
  DLA::MatrixD< SparseNonZeroPattern<DLA::MatrixS<M,1,Real>> > nz01 = {{ {m,n} }};

  DLA::MatrixD< SparseNonZeroPattern<DLA::MatrixS<1,N,Real>> > nz10 = {{ {n,m} }};
  DLA::MatrixD< SparseNonZeroPattern<Real                  > > nz11 = {{ {n,n} }};

  BlockMatrixNZType nz( nz00, nz01,
                        nz10, nz11 );

  Vector_type x( { SparseVectorSize(m) },
                 { SparseVectorSize(n) } );

  Vector_type b( { SparseVectorSize(m) },
                 { SparseVectorSize(n) } );

  Vector_type b2( { SparseVectorSize(m) },
                  { SparseVectorSize(n) } );

  // set the initial guess
  x = 0;

  TriDiagPattern(nz.m00(0,0));
  nz.m01(0,0).add(m-1,0);
  nz.m10(0,0).add(0,m-1);
  TriDiagPattern(nz.m11(0,0));

  Matrix_type A( nz );

  //Initialize the matrix here when it is a real matrix
  Heat1D< DLA::MatrixS<M,N,Real> >::init(A.m00(0,0));
  A.m01(0,0).slowAccess(m-1,0)(1,0) = -1;
  A.m10(0,0).slowAccess(0,m-1)(0,1) = -1;
  Heat1D< Real >::init(A.m11(0,0));

#if 0
  ScalarMatrix_CRS<int> a(A);
  std::fstream file("tmp/block_2x2.mtx", std::ios::out);
  a.WriteMatrixMarketFile(file);
#endif

  //Create a residual vector
  b = 4;
  b.v0[0][2] = 2;
  b.v1 = 3;

  //Set the matrix for the solver
  Solver.factorize(A);

  //Solve the linear system.
  x = Solver.backsolve(A)*b;

  //Compute the residual vector from the solution
  b2 = A*x;

  //The residuals should now be the same!
  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < N; i++ )
        SANS_CHECK_CLOSE(b2.v0[k][j][i], b.v0[k][j][i], small_tol, close_tol);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      SANS_CHECK_CLOSE(b2.v1[k][j], b.v1[k][j], small_tol, close_tol);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PETSc_Solve_MatrixBlock_2x2_Matrices_test )
{
  PyDict d;

  PETScSolverParam::checkInputs(d);

  static const int M0 = 8;
  static const int M1 = 2;

  typedef BLA::MatrixBlock_2x2<
      DLA::MatrixD<SparseMatrix_CRS<DLA::MatrixS<M0,M0,Real>>>, DLA::MatrixD<SparseMatrix_CRS<DLA::MatrixS<M0,M1,Real>>>,
      DLA::MatrixD<SparseMatrix_CRS<DLA::MatrixS<M1,M0,Real>>>, DLA::MatrixD<SparseMatrix_CRS<DLA::MatrixS<M1,M1,Real>>>  > Matrix_type;

  typedef BLA::MatrixBlock_2x2<
      DLA::MatrixD<SparseNonZeroPattern<DLA::MatrixS<M0,M0,Real>>>, DLA::MatrixD<SparseNonZeroPattern<DLA::MatrixS<M0,M1,Real>>>,
      DLA::MatrixD<SparseNonZeroPattern<DLA::MatrixS<M1,M0,Real>>>, DLA::MatrixD<SparseNonZeroPattern<DLA::MatrixS<M1,M1,Real>>>  > BlockMatrixNZType;

  typedef VectorType<Matrix_type>::type Vector_type;

  PETScSolver<Matrix_type> Solver(d);

  const int m = 4;
  const int n = 3;

  DLA::MatrixD< SparseNonZeroPattern<DLA::MatrixS<M0,M0,Real>> > nz00 = {{ {m,m} }};
  DLA::MatrixD< SparseNonZeroPattern<DLA::MatrixS<M0,M1,Real>> > nz01 = {{ {m,n} }};
  DLA::MatrixD< SparseNonZeroPattern<DLA::MatrixS<M1,M0,Real>> > nz10 = {{ {n,m} }};
  DLA::MatrixD< SparseNonZeroPattern<DLA::MatrixS<M1,M1,Real>> > nz11 = {{ {n,n} }};

  BlockMatrixNZType nz( nz00, nz01,
                        nz10, nz11 );
  // matrixSize for nz is:
  //  {{ {m,m}, {m,n} },
  //   { {n,m}, {n,n} }};

  Vector_type x( { SparseVectorSize(m) },
                 { SparseVectorSize(n) } );

  Vector_type b( { SparseVectorSize(m) },
                 { SparseVectorSize(n) } );

  Vector_type b2( { SparseVectorSize(m) },
                  { SparseVectorSize(n) } );

  // set the initial guess
  x = 0;

  TriDiagPattern(nz.m00(0,0));
  nz.m01(0,0).add(m-1,0);
  nz.m10(0,0).add(0,m-1);
  TriDiagPattern(nz.m11(0,0));

  Matrix_type A( nz );

  //Initialize the matrix here when it is a real matrix
  Heat1D< DLA::MatrixS<M0,M0,Real> >::init(A.m00(0,0));
  A.m01(0,0).slowAccess(m-1,0)(1,0) = -1;
  A.m10(0,0).slowAccess(0,m-1)(0,1) = -2;
  Heat1D< DLA::MatrixS<M1,M1,Real> >::init(A.m11(0,0));

#if 0
  ScalarMatrix_CRS<int> a(A);
  std::fstream file("tmp/block_2x2.mtx", std::ios::out);
  a.WriteMatrixMarketFile(file);
#endif

  //Create a residual vector
  b = 4;
  b.v0[0][1] = 2;
  b.v1 = 3;

  //Set the matrix for the solver
  Solver.factorize(A);

  //Solve the linear system.
  x = Solver.backsolve(A)*b;

  //Compute the residual vector from the solution
  b2 = A*x;

  //The residuals should now be the same!
  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for ( int k = 0; k < b.v0.m(); k++ )
    for ( int j = 0; j < b.v0[k].m(); j++ )
      for ( int i = 0; i < M0; i++ )
        SANS_CHECK_CLOSE(b2.v0[k][j][i], b.v0[k][j][i], small_tol, close_tol);

  for ( int k = 0; k < b.v1.m(); k++ )
    for ( int j = 0; j < b.v1[k].m(); j++ )
      for ( int i = 0; i < M1; i++ )
        SANS_CHECK_CLOSE(b2.v1[k][j][i], b.v1[k][j][i], small_tol, close_tol);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PETSc_ScalarMatrix_MatrixD_CRS_Static )
{

  typedef DLA::MatrixS<2,2,Real> Block_type;
  typedef DLA::MatrixD< SparseMatrix_CRS<Block_type> > Matrix_type;

  DLA::MatrixD< SparseNonZeroPattern<Block_type> > nz = {{ {5,5}, {5,5} },
                                                         { {5,5}, {5,5} }};

  for (int i = 0; i < 2; i++)
    TriDiagPattern(nz(i,i));

  nz(0,1).add(4,0);
  nz(1,0).add(0,4);

  Matrix_type A( nz );

  //Initialize the matrix here when it is a real matrix
  Heat1D<Block_type>::init(A(0,0));
  Heat1D<Block_type>::init(A(1,1));

  //Create the off diagonal sparse matrices
  for ( int i = 0; i < 2; i++ )
  {
    A(0,1).sparseRow(4,0)(i,i) = -1;
    A(1,0).sparseRow(0,0)(i,i) = -1;
  }

  ScalarMatrix_CRS<PetscInt> sM( A );

  std::string MatrixD_CRS_Static("IO/SparseLinAlg/PETSc/MatrixD_CRS_Static.mm");

  //Set the 2nd argument to false to regenerate the file
  output_test_stream output( MatrixD_CRS_Static, true );

  sM.WriteMatrixMarketFile(output);

  BOOST_CHECK( output.match_pattern() );

  //Set the 2nd argument to false to regenerate the file
  output_test_stream output2( MatrixD_CRS_Static, true );

  WriteMatrixMarketFile(A, output2);

  BOOST_CHECK( output2.match_pattern() );

  //Set the 2nd argument to false to regenerate the file
  output_test_stream output3( "IO/SparseLinAlg/PETSc/MatrixD_SparseNonZeroPattern_Static.mm", true );

  WriteMatrixMarketFile(nz, output3);

  BOOST_CHECK( output3.match_pattern() );

}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
