// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include <petscsys.h>

#include "SANS_CHECK_CLOSE_btest.h"
#include "tools/SANSnumerics.h"

#include "../../CustomJacobianEqSet_btest.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/ScalarMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"
#include "LinearAlgebra/BlockLinAlg/BlockLinAlg_Mul.h"

#include "LinearAlgebra/SparseLinAlg/PETSc/PETScSolver.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace SANS;
using namespace SANS::SLA;

//############################################################################//
BOOST_AUTO_TEST_SUITE( SparseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PETSc_CRS_Real_MDF_Ordering )
{
  typedef SparseMatrix_CRS<Real> Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;
  typedef CustomJacobianEquationSet<Matrix_type> AlgEqSet;

  mpi::communicator world(PETSC_COMM_WORLD, mpi::comm_attach);

  PyDict PreconditionerILU;
  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.MDF;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 0;

  PyDict PreconditionerDict;
  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PyDict PETScDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.Timing] = false;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-16;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-11;

  PETScSolverParam::checkInputs(PETScDict);

  //Test matrix 1
  {
    PetscInt N = 5;
    std::vector<std::vector<Real>> testMat = {{9, 5, 0, 0, 3},
                                              {4, 1, 2, 0, 0},
                                              {0, 8, 2, 7, 6},
                                              {0, 0, 3, 5, 0},
                                              {1, 0, 8, 0, 3}};

    std::vector<int> trueOrdering = {3, 4, 0, 1, 2};
    BOOST_REQUIRE((int) testMat.size() == N);

    mpi::communicator comm = world.split(world.rank());

    Vector_type q(N);

    AlgEqSet f(std::make_shared<mpi::communicator>(comm), 0, N, N, q);

    for (int i = 0; i < N; i++)
    {
      BOOST_REQUIRE((int) testMat[i].size() == N);
      for (int j = 0; j < N; j++)
      {
        if (testMat[i][j] != 0)
          f.setJacobianEntry(i,j,testMat[i][j]); //only fill nonzeros
      }
    }

#if 0
    AlgEqSet::SystemNonZeroPattern nz(f.matrixSize());
    f.jacobian(nz);

    // construct the matrix
    Matrix_type A(nz);
    f.jacobian(A);
#endif

    PETScSolver<Matrix_type> Solver(PETScDict, f);
    Solver.factorize();

    IS rperm, cperm; /* row and column permutations */
    Solver.getOrderingIndex("mdf", &rperm, &cperm);

    const PetscInt *row_idx, *col_idx;
    ISGetIndices(rperm, &row_idx);
    ISGetIndices(cperm, &col_idx);

    for (int i = 0; i < N; i++)
    {
      BOOST_CHECK_EQUAL( trueOrdering[i], row_idx[i] );
      BOOST_CHECK_EQUAL( trueOrdering[i], col_idx[i] );
//      std::cout << i << ": " << row_idx[i] << ", " << col_idx[i] << std::endl;
    }

    ISRestoreIndices(rperm, &row_idx);
    ISRestoreIndices(cperm, &col_idx);
    ISDestroy(&rperm);
    ISDestroy(&cperm);
  }

  //Test matrix 2
  {
    PetscInt N = 8;
    std::vector<std::vector<Real>> testMat = {{1, 5, 0, 0, 6, 4, 4, 8},
                                              {2, 1, 0, 0, 0, 0, 7, 2},
                                              {0, 0, 2, 4, 1, 0, 0, 0},
                                              {0, 0, 9, 1, 2, 7, 0, 4},
                                              {3, 0, 5, 9, 1, 0, 0, 0},
                                              {5, 0, 0, 2, 0, 1, 0, 0},
                                              {4, 2, 0, 0, 0, 0, 8, 3},
                                              {3, 6, 0, 5, 0, 0, 1, 2}};

    std::vector<int> trueOrdering = {1, 2, 6, 7, 0, 4, 3, 5};
    BOOST_REQUIRE((int) testMat.size() == N);

    mpi::communicator comm = world.split(world.rank());

    Vector_type q(N);

    AlgEqSet f(std::make_shared<mpi::communicator>(comm), 0, N, N, q);

    for (int i = 0; i < N; i++)
    {
      BOOST_REQUIRE((int) testMat[i].size() == N);
      for (int j = 0; j < N; j++)
      {
        if (testMat[i][j] != 0)
          f.setJacobianEntry(i,j,testMat[i][j]); //only fill nonzeros
      }
    }

#if 0
    AlgEqSet::SystemNonZeroPattern nz(f.matrixSize());
    f.jacobian(nz);

    // construct the matrix
    Matrix_type A(nz);
    f.jacobian(A);
#endif

    PETScSolver<Matrix_type> Solver(PETScDict, f);
    Solver.factorize();

    IS rperm, cperm; /* row and column permutations */
    Solver.getOrderingIndex("mdf", &rperm, &cperm);

    const PetscInt *row_idx, *col_idx;
    ISGetIndices(rperm, &row_idx);
    ISGetIndices(cperm, &col_idx);

    for (int i = 0; i < N; i++)
    {
      BOOST_CHECK_EQUAL( trueOrdering[i], row_idx[i] );
      BOOST_CHECK_EQUAL( trueOrdering[i], col_idx[i] );
//      std::cout << i << ": " << row_idx[i] << ", " << col_idx[i] << std::endl;
    }

    ISRestoreIndices(rperm, &row_idx);
    ISRestoreIndices(cperm, &col_idx);
    ISDestroy(&rperm);
    ISDestroy(&cperm);
  }

#ifdef SANS_MPI
  std::cout << "Fix Parallel test in " << __FILE__ << ":" << __LINE__ << std::endl;
#endif

#ifndef SANS_MPI //Parallel
  {
    int color = world.rank() < 3 ? 0 : 1;
    mpi::communicator comm = world.split(color);

    if (color == 0 && comm.size() == 3) //this test only works with 3 MPI processes
    {
      PetscInt N = 8;
      std::vector<std::vector<Real>> testMat = {{1, 5, 0, 0, 6, 4, 4, 8},
                                                {2, 1, 0, 0, 0, 0, 7, 2},
                                                {0, 0, 2, 4, 1, 0, 0, 0},
                                                {0, 0, 9, 1, 2, 7, 0, 4},
                                                {3, 0, 5, 9, 1, 0, 0, 0},
                                                {5, 0, 0, 2, 0, 1, 0, 0},
                                                {4, 2, 0, 0, 0, 0, 8, 3},
                                                {3, 6, 0, 5, 0, 0, 1, 2}};

      std::vector<int> trueOrdering0 = {0, 1, 2}; //3 rows on rank 0
      std::vector<int> trueOrdering1 = {4, 3, 5, 6}; //4 rows on rank 1
      std::vector<int> trueOrdering2 = {7}; //1 row on rank 2

      // Use PETSc to crate a partitioned system of equations
      PetscInt rstart, rend;
      PetscInt n = 0, nghost = 0;

      //Set partitioning
      if (comm.rank() == 0)
        n = 3;
      else if (comm.rank() == 1)
        n = 4;
      else if (comm.rank() == 2)
        n = 1;

      Vec v;
//      PETSc_STATUS( VecCreateMPI(comm, PETSC_DECIDE, N, &v) );
      PETSc_STATUS( VecCreateMPI(comm, n, PETSC_DETERMINE, &v) );
      PETSc_STATUS( VecGetLocalSize(v, &n) );
      PETSc_STATUS( VecGetOwnershipRange(v, &rstart, &rend) );
      PETSc_STATUS( VecDestroy(&v) );

      if ( comm.size() > 1 )
        nghost = N - (rend - rstart);

//      std::cout << "rank" << comm.rank() << " : rstart = " << rstart << ", rend = " << rend << ", nghost = " << nghost << std::endl;

      Vector_type q(n+nghost);
      AlgEqSet f(std::make_shared<mpi::communicator>(comm), rstart, rend, N, q);

      for (int i = 0; i < N; i++)
      {
        BOOST_REQUIRE((int) testMat[i].size() == N);
        for (int j = 0; j < N; j++)
        {
          if (testMat[i][j] != 0)
            f.setJacobianEntry(i,j,testMat[i][j]); //only fill nonzeros
        }
      }

      PETScSolver<Matrix_type> Solver(PETScDict, f);
      Solver.factorize();

      IS rperm, cperm; /* row and column permutations */
      Solver.getOrderingIndex("mdf", &rperm, &cperm);

      const PetscInt *row_idx, *col_idx;
      ISGetIndices(rperm, &row_idx);
      ISGetIndices(cperm, &col_idx);

      for (int i = 0; i < n; i++)
      {
//        std::cout << "rank" << comm.rank() << ", " << i << ": " << row_idx[i] << ", " << col_idx[i] << std::endl;
        if (comm.rank() == 0)
        {
          BOOST_CHECK_EQUAL( trueOrdering0[i], row_idx[i] );
          BOOST_CHECK_EQUAL( trueOrdering0[i], col_idx[i] );
        }
        else if (comm.rank() == 1)
        {
          BOOST_CHECK_EQUAL( trueOrdering1[i], row_idx[i] );
          BOOST_CHECK_EQUAL( trueOrdering1[i], col_idx[i] );
        }
        else if (comm.rank() == 2)
        {
          BOOST_CHECK_EQUAL( trueOrdering2[i], row_idx[i] );
          BOOST_CHECK_EQUAL( trueOrdering2[i], col_idx[i] );
        }
      }

      ISRestoreIndices(rperm, &row_idx);
      ISRestoreIndices(cperm, &col_idx);
      ISDestroy(&rperm);
      ISDestroy(&cperm);
    }

    comm.barrier();
  }
#endif
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PETSc_MatrixD_CRS_MatrixS_MDF_Ordering )
{
  typedef DLA::MatrixS<2,2,Real> MatrixS_type;
  typedef DLA::MatrixD< SparseMatrix_CRS< MatrixS_type > > Matrix_type;

  typedef CustomJacobianEquationSet<Matrix_type> AlgEqSet;
  typedef VectorType<Matrix_type>::type Vector_type;

  mpi::communicator world(PETSC_COMM_WORLD, mpi::comm_attach);
  mpi::communicator comm = world.split(world.rank());

  PetscInt N = 6;

  MatrixS_type A00 = {{1,5},{0,1}};
  MatrixS_type A02 = {{6,4},{4,3}};
  MatrixS_type A03 = {{4,8},{7,2}};
  MatrixS_type A11 = {{2,4},{9,1}};
  MatrixS_type A12 = {{1,6},{2,7}};
  MatrixS_type A15 = {{6,3},{3,0}};
  MatrixS_type A20 = {{3,0},{5,0}};
  MatrixS_type A21 = {{5,9},{0,2}};
  MatrixS_type A22 = {{1,0},{0,1}};
  MatrixS_type A30 = {{4,2},{3,6}};
  MatrixS_type A33 = {{8,3},{1,2}};
  MatrixS_type A44 = {{7,2},{0,1}};
  MatrixS_type A51 = {{1,0},{0,2}};
  MatrixS_type A55 = {{3,0},{9,4}};

  Vector_type q({{N}, {0}});
  AlgEqSet f(std::make_shared<mpi::communicator>(comm), 0, N, N, q);

  //Only sets entries in the MatrixD(0,0) block
  f.setJacobianEntry(0, 0, A00);
  f.setJacobianEntry(0, 2, A02);
  f.setJacobianEntry(0, 3, A03);

  f.setJacobianEntry(1, 1, A11);
  f.setJacobianEntry(1, 2, A12);
  f.setJacobianEntry(1, 5, A15);

  f.setJacobianEntry(2, 0, A20);
  f.setJacobianEntry(2, 1, A21);
  f.setJacobianEntry(2, 2, A22);

  f.setJacobianEntry(3, 0, A30);
  f.setJacobianEntry(3, 3, A33);

  f.setJacobianEntry(4, 4, A44);

  f.setJacobianEntry(5, 1, A51);
  f.setJacobianEntry(5, 5, A55);

  std::vector<int> trueOrdering_outerbsize1 = {3, 0, 2, 1, 4, 5};
  std::vector<int> trueOrdering_outerbsize2 = {2, 3, 0, 1, 4, 5};

  //Case where outer_block_size = 1 (i.e. single MatrixS block is reduced to a scalar)
  {
    PyDict PreconditionerILU;
    PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
    PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
    PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.MDF;
    PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 0;
    PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = 1;

    PyDict PreconditionerDict;
    PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
    PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

    PyDict PETScDict;
    PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
    PETScDict[SLA::PETScSolverParam::params.Timing] = false;
    PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
    PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-16;
    PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-11;

    PETScSolverParam::checkInputs(PETScDict);

#if 0
    AlgEqSet::SystemNonZeroPattern nz(f.matrixSize());
    f.jacobian(nz);

    // construct the matrix
    Matrix_type A(nz);
    f.jacobian(A);
    std::cout << A.m() << ", " << A.n() << std::endl;
    std::cout << A(0,0).m() << ", " << A(0,0).n() << std::endl;
    std::cout << A(0,1).m() << ", " << A(0,1).n() << std::endl;
    std::cout << A(0,0)(0,0) << std::endl;
#endif

    PETScSolver<Matrix_type> Solver(PETScDict, f);
    Solver.factorize();

    IS rperm, cperm; /* row and column permutations */
    Solver.getOrderingIndex("mdf", &rperm, &cperm);

    const PetscInt *row_idx, *col_idx;
    ISGetIndices(rperm, &row_idx);
    ISGetIndices(cperm, &col_idx);

    for (int i = 0; i < N; i++)
    {
      BOOST_CHECK_EQUAL( trueOrdering_outerbsize1[i], row_idx[i] );
      BOOST_CHECK_EQUAL( trueOrdering_outerbsize1[i], col_idx[i] );
    }

    ISRestoreIndices(rperm, &row_idx);
    ISRestoreIndices(cperm, &col_idx);
    ISDestroy(&rperm);
    ISDestroy(&cperm);
  }

  //Case where outer_block_size = 2 (i.e. 2x2 MatrixS blocks are reduced to scalars)
  {
    PyDict PreconditionerILU;
    PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
    PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
    PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.MDF;
    PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 0;
    PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = 2;

    PyDict PreconditionerDict;
    PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
    PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

    PyDict PETScDict;
    PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
    PETScDict[SLA::PETScSolverParam::params.Timing] = false;
    PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
    PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-16;
    PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-11;

    PETScSolverParam::checkInputs(PETScDict);

    PETScSolver<Matrix_type> Solver(PETScDict, f);
    Solver.factorize();

    IS rperm, cperm; /* row and column permutations */
    Solver.getOrderingIndex("mdf", &rperm, &cperm);

    const PetscInt *row_idx, *col_idx;
    ISGetIndices(rperm, &row_idx);
    ISGetIndices(cperm, &col_idx);

    for (int i = 0; i < N; i++)
    {
      BOOST_CHECK_EQUAL( trueOrdering_outerbsize2[i], row_idx[i] );
      BOOST_CHECK_EQUAL( trueOrdering_outerbsize2[i], col_idx[i] );
    }

    ISRestoreIndices(rperm, &row_idx);
    ISRestoreIndices(cperm, &col_idx);
    ISDestroy(&rperm);
    ISDestroy(&cperm);
  }

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
