// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include "tools/SANSnumerics.h"
#include "tools/minmax.h"

#include "../TriDiagPattern_btest.h"
#include "../Heat1D_btest.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Add.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"

using namespace SANS::SLA;
using namespace SANS;

//Explicitly instantiate templates to get correct coverage information
namespace SANS
{
namespace SLA
{
}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( SparseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SparseMatrix_CRS_ctor )
{
  const int m = 5;
  typedef DLA::MatrixD< SparseMatrix_CRS<Real> > Matrix_type;
  DLA::MatrixD< SparseNonZeroPattern<Real> > nz = {{ {m,m} }};
  TriDiagPattern(nz(0,0));
  Matrix_type A( nz );

  const Matrix_type& cA = A;

  //Check that all tri-diag mappings are correct

  BOOST_CHECK_EQUAL(A(0,0).getNumNonZero(), 3*(m-2) + 4 );
  for ( int i = 0; i < A(0,0).m(); i++ )
    for ( int k = 0; k < A(0,0).rowNonZero(i); k++)
      BOOST_CHECK_EQUAL( A(0,0).sparseRow(i,k), 0 );

  int *row_ptr = A(0,0).get_row_ptr();
  int *col_ind = A(0,0).get_col_ind();

  BOOST_CHECK_EQUAL( A(0,0).get_values(), &A(0,0)[0]);

  //First row in the matrix
  BOOST_CHECK_EQUAL(row_ptr[0], 0);
  BOOST_CHECK_EQUAL(row_ptr[1], 2);
  BOOST_CHECK_EQUAL(col_ind[0], 0);
  BOOST_CHECK_EQUAL(col_ind[1], 1);

  //All the entries on the interior rows
  for ( int i = 0; i < m-2; i++ )
  {
    BOOST_CHECK_EQUAL(row_ptr[i+2], 3*i + 3 + 2);
    BOOST_CHECK_EQUAL(col_ind[3*i + 0 + 2], i + 0);
    BOOST_CHECK_EQUAL(col_ind[3*i + 1 + 2], i + 1);
    BOOST_CHECK_EQUAL(col_ind[3*i + 2 + 2], i + 2);
  }

  //Last row in the matrix
  BOOST_CHECK_EQUAL(row_ptr[m], 3*(m-2) + 4);
  BOOST_CHECK_EQUAL(col_ind[3*(m-2) + 2], (m-2) + 0);
  BOOST_CHECK_EQUAL(col_ind[3*(m-2) + 3], (m-2) + 1);

  //Check that the row_ptr and col_ind work the way they are intended to be used
  for ( int i = 0; i < m; i++ )
    for ( int k = row_ptr[i]; k < row_ptr[i+1]; k++)
    {
      BOOST_CHECK_EQUAL(A(0,0)[k], 0);
      BOOST_CHECK_EQUAL(cA(0,0)[k], 0);
      BOOST_CHECK_EQUAL(col_ind[k], max(i-1,0) + k - row_ptr[i]);
    }

  //Initialize as a '1D heat equation' matrix and check that is correct
  Heat1D<Real>::init(A(0,0));


  BOOST_CHECK_EQUAL(cA(0,0).sparseRow(0,0),  2);
  BOOST_CHECK_EQUAL(cA(0,0).sparseRow(0,1), -1);
  for ( int i = 1; i < cA(0,0).m()-1; i++ )
  {
    BOOST_CHECK_EQUAL(cA(0,0).sparseRow(i,0), -1);
    BOOST_CHECK_EQUAL(cA(0,0).sparseRow(i,1),  2);
    BOOST_CHECK_EQUAL(cA(0,0).sparseRow(i,2), -1);
  }
  BOOST_CHECK_EQUAL(cA(0,0).sparseRow(A(0,0).m()-1,0), -1);
  BOOST_CHECK_EQUAL(cA(0,0).sparseRow(A(0,0).m()-1,1),  2);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SparseMatrix_CRS_Empty_MulVec )
{
  DLA::VectorD< SparseVector<Real> > x = { {5},
                                           {5} };
  DLA::VectorD< SparseVector<Real> > b = { {5},
                                           {5} };

  //Create a non-zero pattern, but don't fill it with anything
  DLA::MatrixD< SparseNonZeroPattern<Real> > nz = {{ {5,5}, {5,5} },
                                                   { {5,5}, {5,5} }};

  //Create an empty matrix, which acts like a 'zero' matrix
  DLA::MatrixD< SparseMatrix_CRS< Real > > A( nz );

  x = 1;
  x[0][2] = 2;

  b = A*x;
  for ( int j = 0; j < b.m(); j++)
    for ( int i = 0; i < b[j].m(); i++ )
      BOOST_CHECK_EQUAL(b[j][i], 0);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SparseMatrix_CRS_MulVec )
{
  DLA::VectorD< SparseVector<Real> > x = { {5},
                                           {5} };
  DLA::VectorD< SparseVector<Real> > b = { {5},
                                           {5} };
  DLA::VectorD< SparseVector<Real> > c = { {5},
                                           {3} }; //<- Wrong size on purpose
  DLA::MatrixD< SparseNonZeroPattern<Real> > nz = {{ {5,5}, {5,5} },
                                                   { {5,5}, {5,5} }};

  for (int i = 0; i < 2; i++)
    TriDiagPattern( nz(i,i) );

  DLA::MatrixD< SparseMatrix_CRS<Real> > A(nz);

  for (int i = 0; i < 2; i++)
    Heat1D<Real>::init(A(i,i));

  x = 1;
  x[0][2] = 2;
  x[1][2] = 2;

  b = A*x;
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(b[i][0], 1);
    BOOST_CHECK_EQUAL(b[i][1],-1);
    BOOST_CHECK_EQUAL(b[i][2], 2);
    BOOST_CHECK_EQUAL(b[i][3],-1);
    BOOST_CHECK_EQUAL(b[i][4], 1);
  }

  b = +(A*x);
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(b[i][0], 1);
    BOOST_CHECK_EQUAL(b[i][1],-1);
    BOOST_CHECK_EQUAL(b[i][2], 2);
    BOOST_CHECK_EQUAL(b[i][3],-1);
    BOOST_CHECK_EQUAL(b[i][4], 1);
  }

  b += A*x;
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(b[i][0], 2);
    BOOST_CHECK_EQUAL(b[i][1],-2);
    BOOST_CHECK_EQUAL(b[i][2], 4);
    BOOST_CHECK_EQUAL(b[i][3],-2);
    BOOST_CHECK_EQUAL(b[i][4], 2);
  }


  b = 2*(A*x);
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(b[i][0], 2);
    BOOST_CHECK_EQUAL(b[i][1],-2);
    BOOST_CHECK_EQUAL(b[i][2], 4);
    BOOST_CHECK_EQUAL(b[i][3],-2);
    BOOST_CHECK_EQUAL(b[i][4], 2);
  }

  b = +(2*(A*x));
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(b[i][0], 2);
    BOOST_CHECK_EQUAL(b[i][1],-2);
    BOOST_CHECK_EQUAL(b[i][2], 4);
    BOOST_CHECK_EQUAL(b[i][3],-2);
    BOOST_CHECK_EQUAL(b[i][4], 2);
  }

  b += 2*(A*x);
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(b[i][0], 4);
    BOOST_CHECK_EQUAL(b[i][1],-4);
    BOOST_CHECK_EQUAL(b[i][2], 8);
    BOOST_CHECK_EQUAL(b[i][3],-4);
    BOOST_CHECK_EQUAL(b[i][4], 4);
  }

  b = -(2*(A*x));
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(b[i][0],-2);
    BOOST_CHECK_EQUAL(b[i][1], 2);
    BOOST_CHECK_EQUAL(b[i][2],-4);
    BOOST_CHECK_EQUAL(b[i][3], 2);
    BOOST_CHECK_EQUAL(b[i][4],-2);
  }


  b = (A*x)*2;
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(b[i][0], 2);
    BOOST_CHECK_EQUAL(b[i][1],-2);
    BOOST_CHECK_EQUAL(b[i][2], 4);
    BOOST_CHECK_EQUAL(b[i][3],-2);
    BOOST_CHECK_EQUAL(b[i][4], 2);
  }

  b = (A*x)/2;
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(b[i][0], 0.5);
    BOOST_CHECK_EQUAL(b[i][1],-0.5);
    BOOST_CHECK_EQUAL(b[i][2], 1);
    BOOST_CHECK_EQUAL(b[i][3],-0.5);
    BOOST_CHECK_EQUAL(b[i][4], 0.5);
  }


  b = (2*(A*x))*2;
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(b[i][0], 4);
    BOOST_CHECK_EQUAL(b[i][1],-4);
    BOOST_CHECK_EQUAL(b[i][2], 8);
    BOOST_CHECK_EQUAL(b[i][3],-4);
    BOOST_CHECK_EQUAL(b[i][4], 4);
  }

  b = 2*((A*x)*2);
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(b[i][0], 4);
    BOOST_CHECK_EQUAL(b[i][1],-4);
    BOOST_CHECK_EQUAL(b[i][2], 8);
    BOOST_CHECK_EQUAL(b[i][3],-4);
    BOOST_CHECK_EQUAL(b[i][4], 4);
  }

  b = (2*(A*x))/2;
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(b[i][0], 1);
    BOOST_CHECK_EQUAL(b[i][1],-1);
    BOOST_CHECK_EQUAL(b[i][2], 2);
    BOOST_CHECK_EQUAL(b[i][3],-1);
    BOOST_CHECK_EQUAL(b[i][4], 1);
  }


  b = (2*A)*x;
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(b[i][0], 2);
    BOOST_CHECK_EQUAL(b[i][1],-2);
    BOOST_CHECK_EQUAL(b[i][2], 4);
    BOOST_CHECK_EQUAL(b[i][3],-2);
    BOOST_CHECK_EQUAL(b[i][4], 2);
  }

  b = +((2*A)*x);
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(b[i][0], 2);
    BOOST_CHECK_EQUAL(b[i][1],-2);
    BOOST_CHECK_EQUAL(b[i][2], 4);
    BOOST_CHECK_EQUAL(b[i][3],-2);
    BOOST_CHECK_EQUAL(b[i][4], 2);
  }

  b += (2*A)*x;
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(b[i][0], 4);
    BOOST_CHECK_EQUAL(b[i][1],-4);
    BOOST_CHECK_EQUAL(b[i][2], 8);
    BOOST_CHECK_EQUAL(b[i][3],-4);
    BOOST_CHECK_EQUAL(b[i][4], 4);
  }


  b = A*(x*2);
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(b[i][0], 2);
    BOOST_CHECK_EQUAL(b[i][1],-2);
    BOOST_CHECK_EQUAL(b[i][2], 4);
    BOOST_CHECK_EQUAL(b[i][3],-2);
    BOOST_CHECK_EQUAL(b[i][4], 2);
  }

  b = +(A*(x*2));
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(b[i][0], 2);
    BOOST_CHECK_EQUAL(b[i][1],-2);
    BOOST_CHECK_EQUAL(b[i][2], 4);
    BOOST_CHECK_EQUAL(b[i][3],-2);
    BOOST_CHECK_EQUAL(b[i][4], 2);
  }

  b += A*(x*2);
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(b[i][0], 4);
    BOOST_CHECK_EQUAL(b[i][1],-4);
    BOOST_CHECK_EQUAL(b[i][2], 8);
    BOOST_CHECK_EQUAL(b[i][3],-4);
    BOOST_CHECK_EQUAL(b[i][4], 4);
  }


  BOOST_CHECK_THROW( b = A*c, AssertionException );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SparseMatrix_CRS_Expression )
{
  DLA::VectorD< SparseVector<Real> > x = { {5},
                                           {5} };
  DLA::VectorD< SparseVector<Real> > b = { {5},
                                           {5} };
  DLA::VectorD< SparseVector<Real> > r = { {5},
                                           {5} };
  DLA::MatrixD< SparseNonZeroPattern<Real> > nz = {{ {5,5}, {5,5} },
                                                   { {5,5}, {5,5} }};

  for (int i = 0; i < 2; i++)
    TriDiagPattern( nz(i,i) );

  DLA::MatrixD< SparseMatrix_CRS<Real> > A(nz);

  for (int i = 0; i < 2; i++)
    Heat1D<Real>::init(A(i,i));

  x = 1;
  b = 2;
  x[0][2] = 2;
  x[1][2] = 2;

  r = b + A*x;
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(r[i][0], 3);
    BOOST_CHECK_EQUAL(r[i][1], 1);
    BOOST_CHECK_EQUAL(r[i][2], 4);
    BOOST_CHECK_EQUAL(r[i][3], 1);
    BOOST_CHECK_EQUAL(r[i][4], 3);
  }

  r = +(b + A*x);
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(r[i][0], 3);
    BOOST_CHECK_EQUAL(r[i][1], 1);
    BOOST_CHECK_EQUAL(r[i][2], 4);
    BOOST_CHECK_EQUAL(r[i][3], 1);
    BOOST_CHECK_EQUAL(r[i][4], 3);
  }

  r += b + A*x;
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(r[i][0], 6);
    BOOST_CHECK_EQUAL(r[i][1], 2);
    BOOST_CHECK_EQUAL(r[i][2], 8);
    BOOST_CHECK_EQUAL(r[i][3], 2);
    BOOST_CHECK_EQUAL(r[i][4], 6);
  }

  r = b - A*x;
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(r[i][0], 1);
    BOOST_CHECK_EQUAL(r[i][1], 3);
    BOOST_CHECK_EQUAL(r[i][2], 0);
    BOOST_CHECK_EQUAL(r[i][3], 3);
    BOOST_CHECK_EQUAL(r[i][4], 1);
  }

  r = +(b - A*x);
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(r[i][0], 1);
    BOOST_CHECK_EQUAL(r[i][1], 3);
    BOOST_CHECK_EQUAL(r[i][2], 0);
    BOOST_CHECK_EQUAL(r[i][3], 3);
    BOOST_CHECK_EQUAL(r[i][4], 1);
  }

  r += b - A*x;
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(r[i][0], 2);
    BOOST_CHECK_EQUAL(r[i][1], 6);
    BOOST_CHECK_EQUAL(r[i][2], 0);
    BOOST_CHECK_EQUAL(r[i][3], 6);
    BOOST_CHECK_EQUAL(r[i][4], 2);
  }

  r = -A*x + b;
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(r[i][0], 1);
    BOOST_CHECK_EQUAL(r[i][1], 3);
    BOOST_CHECK_EQUAL(r[i][2], 0);
    BOOST_CHECK_EQUAL(r[i][3], 3);
    BOOST_CHECK_EQUAL(r[i][4], 1);
  }

  r = 2*(b + A*x);
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(r[i][0], 6);
    BOOST_CHECK_EQUAL(r[i][1], 2);
    BOOST_CHECK_EQUAL(r[i][2], 8);
    BOOST_CHECK_EQUAL(r[i][3], 2);
    BOOST_CHECK_EQUAL(r[i][4], 6);
  }

  r = (b + A*x)*2;
  for ( int i = 0; i < b.m(); i++)
  {
    BOOST_CHECK_EQUAL(r[i][0], 6);
    BOOST_CHECK_EQUAL(r[i][1], 2);
    BOOST_CHECK_EQUAL(r[i][2], 8);
    BOOST_CHECK_EQUAL(r[i][3], 2);
    BOOST_CHECK_EQUAL(r[i][4], 6);
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MatrixD_SparseNonZeroPattern )
{
  int m0 = 3;
  int m1 = 2;
  DLA::MatrixD< SparseNonZeroPattern<Real> >
    pA = {{ {m0,m0}, {m0,m1} },
          { {m1,m0}, {m1,m1} }};

  //Initialize the non-zero pattern
  pA(0,0).add(0,0); pA(0,0).add(0,1);
  pA(0,0).add(1,0); pA(0,0).add(1,1); pA(0,0).add(1,2);
                    pA(0,0).add(2,1); pA(0,0).add(2,2);

  pA(0,1).add(0,0); pA(0,1).add(0,1);
  pA(0,1).add(1,0);
                    pA(0,1).add(2,1);

  pA(1,0).add(0,0); pA(1,0).add(0,1);
                    pA(1,0).add(1,1); pA(1,0).add(1,2);

  //pA(1,1) is a zero matrix

  //Create the block matrix system
  DLA::MatrixD< SparseMatrix_CRS<Real> > A(pA);

  //Create the vectors
  DLA::VectorD< SparseVector<Real> > x = {{m0},{m1}}, b = {{m0},{m1}};

  //Initialize
  x = 1;
  A = 1;

  //Compute the matrix-vector multiplication
  b = A*x;

  BOOST_CHECK_EQUAL(b[0][0], 4);
  BOOST_CHECK_EQUAL(b[0][1], 4);
  BOOST_CHECK_EQUAL(b[0][2], 3);

  BOOST_CHECK_EQUAL(b[1][0], 2);
  BOOST_CHECK_EQUAL(b[1][1], 2);

#ifdef __clang_analyzer__
  return; // false errors
#endif

  //test non-zero pattern copy
  SparseNonZeroPattern<Real> AA(pA(0,1).m(),pA(0,1).n());
  AA = pA(0,1);
  for (int ii=0; ii<pA(0,1).m(); ii++)
    for (int jj=0; jj<pA(0,1).n(); jj++ )
      BOOST_CHECK_EQUAL(AA(ii,jj),AA(ii,jj));


  // test matrixD non-zero pattern copy
  DLA::MatrixD< SparseNonZeroPattern<Real> > pB(pA.m(),pA.n());
  BOOST_CHECK_EQUAL(1,1);
  pB = pA;
  BOOST_CHECK_EQUAL(1,1);

  for (int i=0; i<2; i++)
    for (int j=0; j<2; j++)
      for (int ii=0; ii<pA(i,j).m(); ii++)
        for (int jj=0; jj<pA(i,j).n(); jj++ )
          BOOST_CHECK_EQUAL(pA(i,j)(ii,jj),pB(i,j)(ii,jj));

}



BOOST_AUTO_TEST_CASE( MatrixD_SparseMatrixSize )
{
  // same as MatrixD_SparseNonZeroPattern Test but initialized with SparseMatrix Size

  typedef SLA::SparseNonZeroPattern<Real> NonZeroPatternClass;
  typedef SLA::SparseMatrixSize SparseMatrixSizeClass;
  int ntimes = 2;

//
  //declare matrix sizes
  DLA::MatrixD<SparseMatrixSizeClass> szA(ntimes,ntimes); // N x N of nDOFPDE x nDOFPDE

  BOOST_CHECK_EQUAL(szA.m(),2);
  BOOST_CHECK_EQUAL(szA.n(),2);

  int m0 = 3;
  int m1 = 2;
  //resize sparsematrixsizes inside the individual matrices
  szA(0,0).resize(m0,m0);
  szA(0,1).resize(m0,m1);
  szA(1,0).resize(m1,m0);
  szA(1,1).resize(m1,m1);

  BOOST_CHECK_EQUAL(szA(0,0).m(),m0);
  BOOST_CHECK_EQUAL(szA(0,0).n(),m0);
  BOOST_CHECK_EQUAL(szA(0,1).m(),m0);
  BOOST_CHECK_EQUAL(szA(0,1).n(),m1);

  // test constructor for single sparsematrix size
  NonZeroPatternClass nz1(szA(1,0));
  BOOST_CHECK_EQUAL(nz1.m(),m1);
  BOOST_CHECK_EQUAL(nz1.n(),m0);


  DLA::MatrixD< NonZeroPatternClass > pA(szA);

  BOOST_CHECK_EQUAL(pA(0,0).m(),m0);
  BOOST_CHECK_EQUAL(pA(0,0).n(),m0);

  //Initialize the non-zero pattern
  pA(0,0).add(0,0); pA(0,0).add(0,1);
  pA(0,0).add(1,0); pA(0,0).add(1,1); pA(0,0).add(1,2);
                    pA(0,0).add(2,1); pA(0,0).add(2,2);

  pA(0,1).add(0,0); pA(0,1).add(0,1);
  pA(0,1).add(1,0);
                    pA(0,1).add(2,1);

  pA(1,0).add(0,0); pA(1,0).add(0,1);
                    pA(1,0).add(1,1); pA(1,0).add(1,2);


  //pA(1,1) is a zero matrix

  //Create the block matrix system
  DLA::MatrixD< SparseMatrix_CRS<Real> > A(pA);

  //Create the vectors
  DLA::VectorD< SparseVector<Real> > x = {{m0},{m1}}, b = {{m0},{m1}};

  //Initialize
  x = 1;
  A = 1;

  //Compute the matrix-vector multiplication
  b = A*x;

#ifdef __clang_analyzer__
  return;
#endif

  BOOST_CHECK_EQUAL(b[0][0], 4);
  BOOST_CHECK_EQUAL(b[0][1], 4);
  BOOST_CHECK_EQUAL(b[0][2], 3);

  BOOST_CHECK_EQUAL(b[1][0], 2);
  BOOST_CHECK_EQUAL(b[1][1], 2);
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
