// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include "tools/SANSnumerics.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Add.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/tools/dot.h"

using namespace SANS;
using namespace SANS::SLA;


//Explicitly instantiate templates to get correct coverage information
namespace SANS
{
namespace SLA
{
typedef SparseVector<Real> Vec_type;
template class SparseVector<Real>;
template class OpAdd<Vec_type, Vec_type, false>;
template class OpSub<Vec_type, Vec_type, false>;
template class OpMulScalar<Vec_type, false>;
}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( SparseLinAlg )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseVector_ctor )
{
  SparseVector<Real> x(1);

  BOOST_CHECK_EQUAL(x.m(), 1);

  x[0] = 1;
  BOOST_CHECK_EQUAL(x[0], 1);
  BOOST_CHECK_EQUAL(const_cast<const SparseVector<Real>&>(x)[0], 1);

  SparseVector<Real> x2(x);
  BOOST_CHECK_EQUAL(x2[0], 1);

  SparseVector<Real> x3(x.size());
  BOOST_CHECK_EQUAL(x3.m(), 1);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseVector_assign )
{
  SparseVector<Real> x(2), b(2), c(3);

  x = 0;
  BOOST_CHECK_EQUAL(x[0], 0);
  BOOST_CHECK_EQUAL(x[1], 0);

  b = x;
  BOOST_CHECK_EQUAL(x[0], 0);
  BOOST_CHECK_EQUAL(x[1], 0);
  BOOST_CHECK_EQUAL(b[0], 0);
  BOOST_CHECK_EQUAL(b[1], 0);

  b[1] = 1;
  BOOST_CHECK_EQUAL(x[0], 0);
  BOOST_CHECK_EQUAL(x[1], 0);
  BOOST_CHECK_EQUAL(b[0], 0);
  BOOST_CHECK_EQUAL(b[1], 1);

  b(0) = 2;
  BOOST_CHECK_EQUAL(x[0], 0);
  BOOST_CHECK_EQUAL(x[1], 0);
  BOOST_CHECK_EQUAL(b[0], 2);
  BOOST_CHECK_EQUAL(b[1], 1);

  x = b;
  BOOST_CHECK_EQUAL(x[0], 2);
  BOOST_CHECK_EQUAL(x[1], 1);
  BOOST_CHECK_EQUAL(b[0], 2);
  BOOST_CHECK_EQUAL(b[1], 1);

  BOOST_CHECK_THROW( c = x, AssertionException );
  BOOST_CHECK_THROW( x = c, AssertionException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseVector_unary )
{
  SparseVector<Real> x(2);

  x = 2;
  x /= 2;
  BOOST_CHECK_EQUAL(x[0], 1);
  BOOST_CHECK_EQUAL(x[1], 1);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseVector_norm )
{
  SparseVector<Real> x(4);

  x = 2;
  BOOST_CHECK_EQUAL(x.norm(), 4);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseVector_dot )
{
  SparseVector<Real> x(2), b(2), c(3), x2(2);

  x = 2;
  b = 3;
  BOOST_CHECK_EQUAL(x.dot(b), 12);
  BOOST_CHECK_THROW( x.dot(c), AssertionException );

  x2 = 3;
  BOOST_CHECK_EQUAL(dot(x+x2, b), 30);
  BOOST_CHECK_EQUAL(dot(b, x+x2), 30);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseVector_LazyExpression )
{
  SparseVector<Real> x(2), b(2), c(3);

  x = 2;
  x.value(1, b);
  BOOST_CHECK_EQUAL(b[0], 2);
  BOOST_CHECK_EQUAL(b[1], 2);

  x.value(-1, b);
  BOOST_CHECK_EQUAL(b[0],-2);
  BOOST_CHECK_EQUAL(b[1],-2);

  x.plus(1, b);
  BOOST_CHECK_EQUAL(b[0], 0);
  BOOST_CHECK_EQUAL(b[1], 0);

  x.plus(-1, b);
  BOOST_CHECK_EQUAL(b[0],-2);
  BOOST_CHECK_EQUAL(b[1],-2);

  BOOST_CHECK_THROW( x.value(1, c), AssertionException );
  BOOST_CHECK_THROW( x.plus(1, c), AssertionException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseVector_Expression )
{
  SparseVector<Real> x1(2), x2(2), x3(2);

  x1 = 1;
  x2 = 2;

  x3 = -x1;
  BOOST_CHECK_EQUAL(x3[0],-1);
  BOOST_CHECK_EQUAL(x3[1],-1);

  x3 = -(-x1);
  BOOST_CHECK_EQUAL(x3[0], 1);
  BOOST_CHECK_EQUAL(x3[1], 1);

  x3 = x1 + x2;
  BOOST_CHECK_EQUAL(x3[0], 3);
  BOOST_CHECK_EQUAL(x3[1], 3);

  x3 = +(x1 + x2);
  BOOST_CHECK_EQUAL(x3[0], 3);
  BOOST_CHECK_EQUAL(x3[1], 3);

  x3 += x1 + x2;
  BOOST_CHECK_EQUAL(x3[0], 6);
  BOOST_CHECK_EQUAL(x3[1], 6);

  x3 -= x1 + x2;
  BOOST_CHECK_EQUAL(x3[0], 3);
  BOOST_CHECK_EQUAL(x3[1], 3);


  x3 = x1 - x2;
  BOOST_CHECK_EQUAL(x3[0],-1);
  BOOST_CHECK_EQUAL(x3[1],-1);

  x3 = +(x1 - x2);
  BOOST_CHECK_EQUAL(x3[0],-1);
  BOOST_CHECK_EQUAL(x3[1],-1);

  x3 += x1 - x2;
  BOOST_CHECK_EQUAL(x3[0],-2);
  BOOST_CHECK_EQUAL(x3[1],-2);

  x3 -= x1 - x2;
  BOOST_CHECK_EQUAL(x3[0],-1);
  BOOST_CHECK_EQUAL(x3[1],-1);


  x3 = 2*x1;
  BOOST_CHECK_EQUAL(x3[0], 2);
  BOOST_CHECK_EQUAL(x3[1], 2);

  x3 = +(2*x1);
  BOOST_CHECK_EQUAL(x3[0], 2);
  BOOST_CHECK_EQUAL(x3[1], 2);

  x3 = x1*2;
  BOOST_CHECK_EQUAL(x3[0], 2);
  BOOST_CHECK_EQUAL(x3[1], 2);

  x3 = x2/2;
  BOOST_CHECK_EQUAL(x3[0], 1);
  BOOST_CHECK_EQUAL(x3[1], 1);

  x3 = 2*x1*2;
  BOOST_CHECK_EQUAL(x3[0], 4);
  BOOST_CHECK_EQUAL(x3[1], 4);

  x3 = 2*(x1*2);
  BOOST_CHECK_EQUAL(x3[0], 4);
  BOOST_CHECK_EQUAL(x3[1], 4);

  x3 = (2*x1)/2;
  BOOST_CHECK_EQUAL(x3[0], 1);
  BOOST_CHECK_EQUAL(x3[1], 1);

  x3 += x1*2;
  BOOST_CHECK_EQUAL(x3[0], 3);
  BOOST_CHECK_EQUAL(x3[1], 3);

  x3 -= x1*2;
  BOOST_CHECK_EQUAL(x3[0], 1);
  BOOST_CHECK_EQUAL(x3[1], 1);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
