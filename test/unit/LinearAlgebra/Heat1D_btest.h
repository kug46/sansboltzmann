// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef HEAT1D_BTEST_H
#define HEAT1D_BTEST_H

#include "TriDiagPattern_btest.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/Transpose.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/DenseLinAlg/tools/Identity.h"

// This is OK as this file is always only included in unit test cpp files
#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

template<class TM>
struct Heat1D
{
  static void init( SLA::SparseMatrix_CRS<TM>& A )
  {
    if (A.getNumNonZero() == 0) return; // nothing to do for an empty matrix

    TM I = DLA::Identity();
    int rnz;

    rnz = A.rowNonZero(0);
    if ( rnz > 0 ) A.sparseRow(0,0) =  2*I;
    if ( rnz > 1 ) A.sparseRow(0,1) = -1*I; // ghost if A.m() == 1
    if ( rnz > 2 ) A.sparseRow(0,2) = -1*I; // ghost

    if (A.m() == 1) return; // done if just one row

    for ( int i = 1; i < A.m()-1; i++ )
    {
      rnz = A.rowNonZero(i);
      if ( rnz > 0 ) A.sparseRow(i,0) = -1*I;
      if ( rnz > 1 ) A.sparseRow(i,1) =  2*I;
      if ( rnz > 2 ) A.sparseRow(i,2) = -1*I;
    }

    rnz = A.rowNonZero(A.m()-1);
    if ( rnz > 0 ) A.sparseRow(A.m()-1,0) = -1*I;
    if ( rnz > 1 ) A.sparseRow(A.m()-1,1) =  2*I;
    if ( rnz > 2 ) A.sparseRow(A.m()-1,2) = -1*I; // ghost
  }

  static void init( DLA::MatrixD<TM>& A )
  {
    TM I = DLA::Identity();

    int min_size = min(A.m(), A.n());
    A = 0;

    for (int i = 0; i < min_size; i++)
    {
      A(i,i) = 2*I;
      if (i > 0) A(i, i-1) = -I;
      if (i < min_size-1 ) A(i, i+1) = -I;
    }

    if (A.m() > min_size) A(min_size, min_size - 1) = -I;
//    if (A.n() > min_size) A(min_size - 1, min_size) = -I;
  }
};

template<class TM>
struct Heat1D< DLA::MatrixD<TM> >
{
  static void init( SLA::SparseMatrix_CRS< DLA::MatrixD<TM> >& A )
  {
    SANS_ASSERT( A.getNumNonZero() > 0 );
    SANS_ASSERT( A.m() > 1 );

    //Just assume all blocks are the same...
    const int block_m = A.block_m(0);
    DLA::MatrixD<TM> I(block_m, block_m);
    I = SANS::DLA::Identity();

    int rnz = A.rowNonZero(0);

    if ( rnz > 0 ) A.sparseRow(0,0) =  2*I;
    if ( rnz > 1 ) A.sparseRow(0,1) = -1*I;
    for ( int i = 1; i < A.m()-1; i++ )
    {
      rnz = A.rowNonZero(i);
      if ( rnz > 0 ) A.sparseRow(i,0) = -1*I;
      if ( rnz > 1 ) A.sparseRow(i,1) =  2*I;
      if ( rnz > 2 ) A.sparseRow(i,2) = -1*I;
    }
    rnz = A.rowNonZero(A.m()-1);
    if ( rnz > 0 ) A.sparseRow(A.m()-1,0) = -1*I;
    if ( rnz > 1 ) A.sparseRow(A.m()-1,1) =  2*I;
  }
};



template <class SystemMatrix>
class Heat1DEquationSet : public AlgebraicEquationSetBase<SystemMatrix>
{
public:
  typedef AlgebraicEquationSetBase<SystemMatrix> BaseType;

  typedef typename BaseType::SystemVector SystemVector;
  typedef typename BaseType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename BaseType::SystemMatrixView SystemMatrixView;
  typedef typename BaseType::SystemVectorView SystemVectorView;
  typedef typename BaseType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;

  typedef typename MatrixSizeType<SystemMatrix>::type MatrixSizeClass;
  typedef typename VectorSizeType<SystemVector>::type VectorSizeClass;

  explicit Heat1DEquationSet(SystemVectorView& q, bool singular = false) :
    comm_(nullptr), rstart_(-1), rend_(-1), N_(-1), b_(q), q_(q), singular_(singular) {}

  Heat1DEquationSet(std::shared_ptr<mpi::communicator> comm, const int rstart, const int rend, const int N,
                    SystemVectorView& b, SystemVectorView& q) :
    comm_(comm), rstart_(rstart), rend_(rend), N_(N), b_(b), q_(q), singular_(false) {}

  virtual ~Heat1DEquationSet() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  virtual void residual(SystemVectorView& rsd) override
  {
    SystemNonZeroPattern nz(matrixSize());
    jacobian(nz);
    SystemMatrix mtx(nz);
    jacobian(mtx);

    rsd = mtx*q_;
  }

  virtual void jacobian(SystemMatrixView& mtx) override
  {
    typedef typename SystemMatrix::Ttype TM ;

    if (singular_)
      mtx = 0;
    else
      Heat1D<TM>::init(mtx);
  }

  virtual void jacobian(SystemNonZeroPatternView& nz) override
  {
    if (rstart_ >= 0 && rend_ >= 0)
      TriDiagPattern(rstart_, rend_, 0, N_, N_, nz);
    else
      TriDiagPattern(nz, 2);
  }

  virtual void jacobianTranspose(SystemMatrixView& mtx) override
  {
    // Heat equation is symmetric
    jacobian(mtx);
  }
  virtual void jacobianTranspose(SystemNonZeroPatternView& nz) override
  {
    // Heat equation is symmetric
    jacobian(nz);
  }

  virtual std::vector<std::vector<Real>> residualNorm(const SystemVectorView& rsd) const override
  {
    std::vector<std::vector<Real>> rsdNorm(1, std::vector<Real>(1, 0));

    rsdNorm[0][0] = 0;
    for (int i = 0; i < rsd.m(); i++)
      rsdNorm[0][0] += dot(rsd[i], rsd[i]);

    rsdNorm[0][0] = sqrt( rsdNorm[0][0] );

    return rsdNorm;
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm) const override
  {
    //converged?
    return rsdNorm[0][0] < 1e-12;
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm, int iEq, int iMon) const override
  {
    SANS_ASSERT( iEq == 0 );
    SANS_ASSERT( iMon == 0 );

    //converged?
    return rsdNorm[0][0] < 1e-12;
  }

  // TODO:implement!
  virtual bool decreasedResidual(const std::vector<std::vector<Real>>& rsdNormOld,
                                 const std::vector<std::vector<Real>>& rsdNormNew) const override
  {
    //is the residual actually decreased or converged?
    int t = 0;

    if (rsdNormOld[0][0] < rsdNormNew[0][0] && rsdNormOld[0][0] > 1e-12)
      t++;

    return (t == 0);
  }

  //prints out a residual that could not be decreased and the convergence tolerances
  virtual void printDecreaseResidualFailure(const std::vector<std::vector<Real>>& rsdNorm, std::ostream& os = std::cout) const override {}

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override
  {
    q_ = q;
  }

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override
  {
    q = q_;
  }

  virtual VectorSizeClass vectorEqSize() const override    // vector for equations (rows in matrixSize)
  {
    if (rstart_ >= 0 && rend_ >= 0)
      SANS_ASSERT_MSG( b_.m() == rend_ - rstart_, "%d != %d", b_.m(), rend_ - rstart_);

    return VectorSizeClass( b_.m() );
  }

  virtual VectorSizeClass vectorStateSize() const override // vector for state DOFs (columns in matrixSize)
  {
    return VectorSizeClass( q_.m() );
  }

  virtual MatrixSizeClass matrixSize() const override
  {
    if (rstart_ >= 0 && rend_ >= 0)
      SANS_ASSERT_MSG( b_.m() == rend_ - rstart_, "%d != %d", b_.m(), rend_ - rstart_);

    // Create the size that represents the size of a sparse linear algebra matrix
    return { b_.m(), q_.m() };
  }

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { return 0; }
  virtual int indexQ() const override { return 0; }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override
  {
    return true;
  }

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override
  {
    return 1;
  }

  // Converts processor local indexing to a processor global indexing
  virtual std::vector<GlobalContinuousMap> continuousGlobalMap() const override
  {
    SANS_ASSERT( comm_ != nullptr );

    std::vector<GlobalContinuousMap> continuousmap;
    continuousmap.emplace_back(comm_);

    int nghost = 0;
    if ( comm_->size() > 1 && rstart_ != N_ )
      nghost = (rstart_ == 0 || rend_ == N_) ? 1 : 2;

    continuousmap[0].nDOFpossessed = rend_ - rstart_;
    continuousmap[0].nDOF_rank_offset = rstart_;
    continuousmap[0].remoteGhostIndex.resize(nghost);

    if ( comm_->size() > 1 && nghost > 0 )
    {
           if (rstart_ == 0 ) continuousmap[0].remoteGhostIndex[0] = rend_;
      else if (rend_   == N_) continuousmap[0].remoteGhostIndex[0] = rstart_-1;
      else
      {
        continuousmap[0].remoteGhostIndex[0] = rstart_-1;
        continuousmap[0].remoteGhostIndex[1] = rend_;
      }
    }

    return continuousmap;
  }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override { return comm_; }
  virtual void syncDOFs_MPI() override {}

  virtual bool updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                                         LinesearchData& pResData,
                                         LinesearchData& pStepData) const override { return true; };

  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override {};


  // Are we having machine precision issues - assume no issues
  virtual bool atMachinePrecision(const SystemVectorView& q, const std::vector<std::vector<Real>>& R0norm) override
  {
    return false;
  }

protected:
  std::shared_ptr<mpi::communicator> comm_;
  const int rstart_;
  const int rend_;
  const int N_;
  SystemVectorView& b_;
  SystemVectorView& q_;
  bool singular_;
};


template <class Matrix_type>
class Heat1DEquationSet< DLA::MatrixD<Matrix_type> > : public AlgebraicEquationSetBase<DLA::MatrixD<Matrix_type>>
{
public:
  typedef DLA::MatrixD<Matrix_type> SystemMatrix;
  typedef AlgebraicEquationSetBase<SystemMatrix> BaseType;

  typedef typename BaseType::SystemVector SystemVector;
  typedef typename BaseType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename BaseType::SystemMatrixView SystemMatrixView;
  typedef typename BaseType::SystemVectorView SystemVectorView;
  typedef typename BaseType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;

  typedef typename MatrixSizeType<SystemMatrix>::type MatrixSizeClass;
  typedef typename VectorSizeType<SystemVector>::type VectorSizeClass;

  explicit Heat1DEquationSet(SystemVectorView& q, bool singular = false) :
    comm_(nullptr), rstart_(-1), rend_(-1), N_{{-1,-1}}, b_(q), q_(q), singular_(singular) {}

  Heat1DEquationSet(std::shared_ptr<mpi::communicator> comm, const int rstart, const int rend,
                    const std::array<int,2>& N, SystemVectorView& b, SystemVectorView& q) :
    comm_(comm), rstart_(rstart), rend_(rend), N_(N), b_(b), q_(q), singular_(false)
  {
    SANS_ASSERT(b_.m() == q_.m());
  }

  virtual ~Heat1DEquationSet() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  virtual void residual(SystemVectorView& rsd) override
  {
    SystemNonZeroPattern nz(matrixSize());
    jacobian(nz);
    SystemMatrix mtx(nz);
    jacobian(mtx);

    rsd = mtx*q_;
  }

  virtual void jacobian(SystemMatrixView& mtx) override
  {
    typedef typename Matrix_type::Ttype TM ;

    TM I = DLA::Identity();

    if (singular_)
      mtx = 0;
    else
    {
      for (int i = 0; i < mtx.m(); i++)
      {

        for (int j = 0; j < mtx.n(); j++)
        {
          if (b_[i].m() == 0) continue;
          if (q_[j].m() == 0) continue;

          if ( i == j )
            Heat1D<TM>::init(mtx(i,j));
          else if ( i < j)
            mtx(i,j).sparseRow(mtx(i,j).m()-1,0) = -1*I;
          else if ( i > j)
            mtx(i,j).sparseRow(0,0) = -1*I;
        }
      }
    }
  }

  virtual void jacobian(SystemNonZeroPatternView& nz) override
  {
    int nEq = 0;
    for (int i = 0; i < nz.m(); i++)
    {
      for (int j = 0; j < nz.n(); j++)
      {
        if (b_[i].m() == 0) continue;
        if (q_[j].m() == 0) continue;

        if ( i == j )
          if (rstart_ >= 0 && rend_ >= 0)
            TriDiagPattern(rstart_, rend_, nEq, nEq+N_[i],  N_[0]+ N_[1], nz(i,j));
          else
            TriDiagPattern(nz(i,j), 2);
        else if ( i < j )
        {
          nz(i,j).add(nz(i,j).m()-1,0);
        }
        else if ( i > j )
        {
          nz(i,j).add(0,std::max(b_[j].m()-1,0));
        }
      }

      nEq += N_[i];
    }
  }

  virtual void jacobianTranspose(SystemMatrixView& mtx) override
  {
    // symmetric
    jacobian(mtx);
  }
  virtual void jacobianTranspose(SystemNonZeroPatternView& nz) override
  {
    // symmetric
    jacobian(nz);
  }

  virtual std::vector<std::vector<Real>> residualNorm(const SystemVectorView& rsd) const override
  {
    std::vector<std::vector<Real>> rsdNorm(1, std::vector<Real>(1, 0));

    rsdNorm[0][0] = 0;
    for (int ii = 0; ii < rsd.m(); ii++)
      for (int i = 0; i < rsd[ii].m(); i++)
        rsdNorm[0][0] += dot(rsd[ii][i], rsd[ii][i]);

    rsdNorm[0][0] = sqrt( rsdNorm[0][0] );

    return rsdNorm;
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm) const override
  {
    //HACKED Simple L2 norm
    //TODO: Add a residual convergence checking class

    //converged?
    return rsdNorm[0][0] < 1e-12;
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm, int iEq, int iMon) const override
  {
    SANS_ASSERT( iEq == 0 );
    SANS_ASSERT( iMon == 0 );

    //converged?
    return rsdNorm[0][0] < 1e-12;
  }

  // TODO:implement!
  virtual bool decreasedResidual(const std::vector<std::vector<Real>>& rsdNormOld,
                                 const std::vector<std::vector<Real>>& rsdNormNew) const override
  {
    //is the residual actually decreased or converged?
    int t = 0;

    if ( rsdNormOld[0][0] <= rsdNormNew[0][0] && !(convergedResidual(rsdNormNew)) )
      t++;

    return (t == 0);
  }

  //prints out a residual that could not be decreased and the convergence tolerances
  virtual void printDecreaseResidualFailure(const std::vector<std::vector<Real>>& rsdNorm,
                                            std::ostream& os = std::cout) const override
  {
    os << "Heat1DEquationSet::printDecreaseResidualFailure has not been implemented yet!" << std::endl;
  }

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override
  {
    q_ = q;
  }

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override
  {
    q = q_;
  }

  virtual VectorSizeClass vectorEqSize() const override    // vector for equations (rows in matrixSize)
  {
    VectorSizeClass size(b_.m());
    int nrow = 0;
    for (int i = 0; i < size.m(); i++)
    {
      size[i] = b_[i].m();
      nrow += b_[i].m();
    }

    if (rstart_ >= 0 && rend_ >= 0)
      SANS_ASSERT( nrow == rend_ - rstart_ );

    return size;
  }

  virtual VectorSizeClass vectorStateSize() const override // vector for state DOFs (columns in matrixSize)
  {
    VectorSizeClass size(q_.m());
    for (int i = 0; i < size.m(); i++)
      size[i] = q_[i].m();

    return size;
  }

  virtual MatrixSizeClass matrixSize() const override
  {
    // Create the size that represents the size of a sparse linear algebra matrix
    MatrixSizeClass size(b_.m(), q_.m());

    int nrow = 0;
    for (int i = 0; i < size.m(); i++)
    {
      for (int j = 0; j < size.n(); j++)
        size(i,j) = { b_[i].m(), q_[j].m() };

      nrow += b_[i].m();
    }

    if (rstart_>= 0 && rend_ >= 0)
      SANS_ASSERT( nrow == rend_ - rstart_ );

    return size;
  }

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { return 0; }
  virtual int indexQ() const override { return 0; }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override
  {
    return true;
  }

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override
  {
    return 1;
  }

  // Converts processor local indexing to a processor global indexing
  virtual std::vector<GlobalContinuousMap> continuousGlobalMap() const override
  {
    SANS_ASSERT( comm_ != nullptr );

    std::vector<GlobalContinuousMap> continuousmap;
    continuousmap.emplace_back(comm_);
    continuousmap.emplace_back(comm_);

    SANS_ASSERT (b_.m() == 2);

    int nEq = 0;
    for (int i = 0; i < 2; i++)
    {
      int nghost = q_[i].m() - b_[i].m();

      continuousmap[i].nDOFpossessed = b_[i].m();
      continuousmap[i].nDOF_rank_offset = rstart_;
      continuousmap[i].remoteGhostIndex.resize(nghost);

      if ( comm_->size() > 1 && nghost > 0 )
      {
        nghost = 0;
        if ( rstart_ != 0 && rstart_ > nEq && rstart_ <= nEq + N_[i])
          continuousmap[i].remoteGhostIndex[nghost++] = rstart_-1;
        if ( rend_ != N_[0]+N_[1] && rend_ >= nEq && rend_ < nEq + N_[i])
          continuousmap[i].remoteGhostIndex[nghost++] = rend_;
      }
      nEq += N_[i];
    }

    return continuousmap;
  }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override { return comm_; }
  virtual void syncDOFs_MPI() override {}

  virtual bool updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                                         LinesearchData& pResData,
                                         LinesearchData& pStepData) const override { return true; };

  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override {};


  // Are we having machine precision issues - assume no issues
  virtual bool atMachinePrecision(const SystemVectorView& q, const std::vector<std::vector<Real>>& R0norm) override
  {
    return false;
  }

protected:
  std::shared_ptr<mpi::communicator> comm_;
  const int rstart_;
  const int rend_;
  const std::array<int,2> N_;
  SystemVectorView& b_;
  SystemVectorView& q_;
  bool singular_;
};

//----------------------------------------------------------------------------//
// Abbreviations:
// SM = system matrix
//
template<class SM00, class SM01,
         class SM10, class SM11>
class Heat1DEquationSet_Block2x2
    : public AlgebraicEquationSetBase< BLA::MatrixBlock_2x2< SM00, SM01,
                                                             SM10, SM11 > >
{
public:
  typedef BLA::MatrixBlock_2x2< SM00, SM01,
                                SM10, SM11 > Matrix_type;

  typedef AlgebraicEquationSetBase< Matrix_type > BaseType;

  typedef typename BaseType::SystemMatrix SystemMatrix;
  typedef typename BaseType::SystemVector SystemVector;
  typedef typename BaseType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename BaseType::SystemMatrixView SystemMatrixView;
  typedef typename BaseType::SystemVectorView SystemVectorView;
  typedef typename BaseType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef AlgebraicEquationSetBase< SM00 > BaseType00;
  typedef AlgebraicEquationSetBase< SM11 > BaseType11;

  typedef typename MatrixSizeType<SystemMatrix>::type MatrixSizeClass;
  typedef typename VectorSizeType<SystemVector>::type VectorSizeClass;

  typedef typename BaseType::LinesearchData LinesearchData;

  Heat1DEquationSet_Block2x2( BaseType00& AES00,
                              BaseType11& AES11,
                              SystemVectorView& q) :
    AES00_(AES00), AES11_(AES11), q_(q)
  {
    // Nothing
  }
  virtual ~Heat1DEquationSet_Block2x2() {}


  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  virtual void residual(SystemVectorView& rsd) override
  {
    SystemNonZeroPattern nz(matrixSize());
    jacobian(nz);
    SystemMatrix mtx(nz);
    jacobian(mtx);

    rsd = mtx*q_;
  }

  virtual void jacobian(SystemMatrixView& mtx) override
  {
    AES00_.jacobian(mtx.m00);
    AES11_.jacobian(mtx.m11);

    typedef typename SM01::node_type::Ttype TM01;
    typedef typename SM10::node_type::Ttype TM10;

    TM01 I01 = DLA::Identity();
    TM10 I10 = DLA::Identity();

    mtx.m01(0,0).slowAccess(mtx.m01.m()-1,0) = -1*I01;
    mtx.m10(0,0).slowAccess(0,mtx.m10.n()-1) = -1*I10;
  }

  virtual void jacobian(SystemNonZeroPatternView& nz) override
  {
    AES00_.jacobian(nz.m00);
    AES11_.jacobian(nz.m11);

    nz.m01(0,0).add(nz.m01.m()-1,0);
    nz.m10(0,0).add(0,nz.m01.n()-1);
  }

  virtual void jacobianTranspose(SystemMatrixView& mtx) override
  {
    // symmetric
    jacobian(mtx);
  }
  virtual void jacobianTranspose(SystemNonZeroPatternView& nz) override
  {
    // symmetric
    jacobian(nz);
  }

  virtual std::vector<std::vector<Real>> residualNorm(const SystemVectorView& rsd) const override
  {
    std::vector<std::vector<Real>> rsdNorm(1, std::vector<Real>(1, 0));

    rsdNorm[0][0] = 0;
    for (int ii = 0; ii < rsd.v0.m(); ii++)
      for (int i = 0; i < rsd.v0[ii].m(); i++)
        rsdNorm[0][0] += dot(rsd.v0[ii][i], rsd.v0[ii][i]);

    for (int ii = 0; ii < rsd.v1.m(); ii++)
      for (int i = 0; i < rsd.v1[ii].m(); i++)
        rsdNorm[0][0] += dot(rsd.v1[ii][i], rsd.v1[ii][i]);

    rsdNorm[0][0] = sqrt( rsdNorm[0][0] );

    return rsdNorm;
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm) const override
  {
    //converged?
    return rsdNorm[0][0] < 1e-12;
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm, int iEq, int iMon) const override
  {
    SANS_ASSERT( iEq == 0 );
    SANS_ASSERT( iMon == 0 );

    //converged?
    return rsdNorm[0][0] < 1e-12;
  }

  // TODO:implement!
  virtual bool decreasedResidual(const std::vector<std::vector<Real>>& rsdNormOld,
                                 const std::vector<std::vector<Real>>& rsdNormNew) const override
  {
    //is the residual actually decreased or converged?
    int t = 0;

    if (rsdNormOld[0][0] < rsdNormNew[0][0] && rsdNormOld[0][0] > 1e-12)
      t++;

    return (t == 0);
  }

  //prints out a residual that could not be decreased and the convergence tolerances
  virtual void printDecreaseResidualFailure(const std::vector<std::vector<Real>>& rsdNorm, std::ostream& os = std::cout) const override {}

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override
  {
    AES00_.setSolutionField(q.v0);
    AES11_.setSolutionField(q.v1);
  }

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override
  {
    AES00_.fillSystemVector(q.v0);
    AES11_.fillSystemVector(q.v1);
  }

  virtual VectorSizeClass vectorEqSize() const override    // vector for equations (rows in matrixSize)
  {
    return VectorSizeClass(AES00_.vectorEqSize(), AES11_.vectorEqSize());
  }

  virtual VectorSizeClass vectorStateSize() const override // vector for state DOFs (columns in matrixSize)
  {
    return VectorSizeClass(AES00_.vectorStateSize(), AES11_.vectorStateSize());
  }

  virtual MatrixSizeClass matrixSize() const override
  {
    // Build up the off diagonal sizes from the diagonals
    typename MatrixSizeType<SM00>::type size00( AES00_.matrixSize() );
    typename MatrixSizeType<SM11>::type size11( AES11_.matrixSize() );

    typename MatrixSizeType<SM01>::type size01(size00.m(), size11.n());
    typename MatrixSizeType<SM10>::type size10(size11.m(), size00.n());

    // TODO: Not sure that this logic is 100% robust... might need some functions
    for (int i = 0; i < size00.m(); i++)
      for (int j = 0; j < size11.n(); j++)
        size01(i,j).resize(size00(i,0).m(), size11(0,j).n());

    for (int i = 0; i < size11.m(); i++)
      for (int j = 0; j < size00.n(); j++)
        size10(i,j).resize(size11(i,0).m(), size00(0,j).n());

    MatrixSizeClass size(size00, size01,
                         size10, size11);

    return size;
  }

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { return 0; }
  virtual int indexQ() const override { return 0; }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override
  {
    return true;
  }

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override
  {
    return 1;
  }

  // Converts processor local indexing to a processor global indexing
  virtual std::vector<GlobalContinuousMap> continuousGlobalMap() const override
  {
    //SANS_ASSERT( comm_ != nullptr );
    SANS_DEVELOPER_EXCEPTION("Not implemented yet");

    std::vector<GlobalContinuousMap> continuousmap;

    return continuousmap;
  }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override { return AES00_.comm(); }
  virtual void syncDOFs_MPI() override {}

  virtual bool updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                                         LinesearchData& pResData,
                                         LinesearchData& pStepData) const override { return true; };

  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override {};


  // Are we having machine precision issues - assume no issues
  virtual bool atMachinePrecision(const SystemVectorView& q, const std::vector<std::vector<Real>>& R0norm) override
  {
    return false;
  }

protected:
  BaseType00& AES00_;
  BaseType11& AES11_;
  SystemVectorView& q_;
};

}

#endif //HEAT1D_BTEST_H
