// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/python/extract.hpp>

#include "Surreal/SurrealS.h"

#include "pde/FullPotential/PDELinearizedIncompressiblePotential3D.h"
#include "pde/FullPotential/SolutionFunction3D_LIP.h"
#include "pde/FullPotential/BCLinearizedIncompressiblePotential3D.h"
#include "pde/FullPotential/LinearizedIncompressiblePotential3D_Trefftz.h"
#include "pde/FullPotential/IntegrandBoundary3D_LIP_Force.h"
#include "pde/FullPotential/IntegrandFunctorBoundary3D_LIP_Vn2.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_BoundaryFrame.h"

#include "Field/output_Tecplot.h"

#include "unit/UnitGrids/XField3D_Box_Tet_X1_WakeCut.h"
#include "unit/UnitGrids/XField3D_Box_Hex_X1_WakeCut.h"

#include "UserInterfaces/LIPSolver.h"

#include <iostream>

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UserInterfaces_test_suite )

bool checkRate(const std::vector<Real>& err_vec, const std::vector<Real>& step_vec,
               const std::vector<Real>& rate_range, const Real& small_tol)
{
  if (err_vec[0] > small_tol && err_vec[1] > small_tol)
  {
    Real rate = log(err_vec[1]/err_vec[0])/log(step_vec[1]/step_vec[0]);

    bool rateCheck = (rate >= rate_range[0] && rate <= rate_range[1]);
    if ( !rateCheck )
      std::cout << "Rate check failed : rate = " << rate <<
                   ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" << std::endl;

    return rateCheck;
  }
  else
    return true;
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Sensitivity_test )
{
  Real span = 4;

  const int nChord = 4;
  const int nSpan  = span*nChord+1;
  const int nWake  = 4; //nChord;

  std::shared_ptr<XField3D_Wake> pxfld =
      std::make_shared<XField3D_Box_Hex_X1_WakeCut>( nChord, nSpan, span, nWake );

  Real theta   = 45;

  Real ct = cos(theta*PI/180.);
  Real st = sin(theta*PI/180.);

  DLA::MatrixS<3,3,Real> Tx = {{ 1,   0,  0},
                               { 0,  ct, st},
                               { 0, -st, ct}};

  //Rotate the grid so we have non-zero forces around alpha=0, beta=0
  for (int n = 0; n < pxfld->nDOF(); n++)
  {
    DLA::VectorS<3,Real> X = pxfld->DOF(n);
    pxfld->DOF(n) = Tx*X;
  }


  std::map<std::string, std::vector<int> > BCFaces;

  BCFaces["Inflow"] = {0};
  BCFaces["Outflow"] = {1,2,3,4,5};
  BCFaces["Wall"] = {6,7};
  std::vector<int> WakeFaces = {8};
  BCFaces["Wing_Wake"] = WakeFaces;

  std::vector<int> TrefftzFrames = {1};

  Real Sref = 10;
  Real Cref = 2;
  Real Bref = 5;

  Real  Xref = 0, Yref = span/10., Zref = 0.;


  PyDict caseParams;

  caseParams[LIPSolver::LIPCaseParams::params.Sref] = Sref;
  caseParams[LIPSolver::LIPCaseParams::params.Cref] = Cref;
  caseParams[LIPSolver::LIPCaseParams::params.Bref] = Bref;

  caseParams[LIPSolver::LIPCaseParams::params.Xref] = Xref;
  caseParams[LIPSolver::LIPCaseParams::params.Yref] = Yref;
  caseParams[LIPSolver::LIPCaseParams::params.Zref] = Zref;

  typedef LIPSolver::BCParams BCParams;
  typedef BCLinearizedIncompressiblePotential3DParams<BCTypeNeumann> BCNeumannParamsType;
  typedef BCLinearizedIncompressiblePotential3DParams<BCTypeDirichlet> BCDirichletParamsType;

  // BC
  PyDict BCWallDict;
  BCWallDict[BCParams::params.BC.BCType] = BCParams::params.BC.Wall;

  PyDict BCOutflowDict;
  BCOutflowDict[BCParams::params.BC.BCType] = BCParams::params.BC.Neumann;
  BCOutflowDict[BCNeumannParamsType::params.gradqn] = 0;

  PyDict BCInflowDict;
  BCInflowDict[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet;
  BCInflowDict[BCDirichletParamsType::params.q] = 0;

  PyDict PyBCList;
  PyBCList["Wall"] = BCWallDict;
  PyBCList["Inflow"] = BCInflowDict;
  PyBCList["Outflow"] = BCOutflowDict;

  caseParams[LIPSolver::LIPCaseParams::params.BCDict] = PyBCList;

  LIPSolver solver(pxfld, BCFaces, WakeFaces, TrefftzFrames, caseParams);

  SANS::PyDict solveparams;

  double alpha0 = 0;
  double beta0 = 0;
  double roll0 = 0;
  double pitch0 = 0;
  double yaw0 = 0;
  solveparams[SANS::LIPSolver::LIPSolveParams::params.alpha]     = alpha0;
  solveparams[SANS::LIPSolver::LIPSolveParams::params.beta]      = beta0;
  solveparams[SANS::LIPSolver::LIPSolveParams::params.rollRate]  = roll0;
  solveparams[SANS::LIPSolver::LIPSolveParams::params.pitchRate] = pitch0;
  solveparams[SANS::LIPSolver::LIPSolveParams::params.yawRate]   = yaw0;

  PyDict outputs0 = solver.solve(solveparams);

  //solver.writeTecplot("tmp/LIPsolver.dat");

  // Compute stability derivatives
  solver.StabilityDerivatives(outputs0);

#if 0
  double CL0 = boost::python::extract<double>(outputs0["CLtot"]);
  double CY0 = boost::python::extract<double>(outputs0["CYtot"]);
  double cl0 = boost::python::extract<double>(outputs0["Cl'tot"]);
  double cm0 = boost::python::extract<double>(outputs0["Cm'tot"]);
  double cn0 = boost::python::extract<double>(outputs0["Cn'tot"]);
#endif

  double CLa = boost::python::extract<double>(outputs0["CLa"]);
  double CYa = boost::python::extract<double>(outputs0["CYa"]);
  double cla = boost::python::extract<double>(outputs0["Cl'a"]);
  double cma = boost::python::extract<double>(outputs0["Cm'a"]);
  double cna = boost::python::extract<double>(outputs0["Cn'a"]);

  double CLb = boost::python::extract<double>(outputs0["CLb"]);
  double CYb = boost::python::extract<double>(outputs0["CYb"]);
  double clb = boost::python::extract<double>(outputs0["Cl'b"]);
  double cmb = boost::python::extract<double>(outputs0["Cm'b"]);
  double cnb = boost::python::extract<double>(outputs0["Cn'b"]);

  double CXp = boost::python::extract<double>(outputs0["CXp"]);
  double CYp = boost::python::extract<double>(outputs0["CYp"]);
  double CZp = boost::python::extract<double>(outputs0["CZp"]);
  double Clp = boost::python::extract<double>(outputs0["Clp"]);
  double Cmp = boost::python::extract<double>(outputs0["Cmp"]);
  double Cnp = boost::python::extract<double>(outputs0["Cnp"]);

  double CXq = boost::python::extract<double>(outputs0["CXq"]);
  double CYq = boost::python::extract<double>(outputs0["CYq"]);
  double CZq = boost::python::extract<double>(outputs0["CZq"]);
  double Clq = boost::python::extract<double>(outputs0["Clq"]);
  double Cmq = boost::python::extract<double>(outputs0["Cmq"]);
  double Cnq = boost::python::extract<double>(outputs0["Cnq"]);

  double CXr = boost::python::extract<double>(outputs0["CXr"]);
  double CYr = boost::python::extract<double>(outputs0["CYr"]);
  double CZr = boost::python::extract<double>(outputs0["CZr"]);
  double Clr = boost::python::extract<double>(outputs0["Clr"]);
  double Cmr = boost::python::extract<double>(outputs0["Cmr"]);
  double Cnr = boost::python::extract<double>(outputs0["Cnr"]);


  std::vector<Real> dalpha = {2e-1, 1e-1};

  std::vector<Real> CLaErr(2);
  std::vector<Real> CYaErr(2);
  std::vector<Real> claErr(2);
  std::vector<Real> cmaErr(2);
  std::vector<Real> cnaErr(2);

  for (std::size_t i = 0; i < dalpha.size(); i++)
  {
    const Real da = dalpha[i];

    solveparams[SANS::LIPSolver::LIPSolveParams::params.alpha]     = alpha0 - da;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.beta]      = beta0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.rollRate]  = roll0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.pitchRate] = pitch0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.yawRate]   = yaw0;

    PyDict outputsm = solver.solve(solveparams);

    double CL1m = boost::python::extract<double>(outputsm["CLtot"]);
    double CY1m = boost::python::extract<double>(outputsm["CYtot"]);
    double cl1m = boost::python::extract<double>(outputsm["Cl'tot"]);
    double cm1m = boost::python::extract<double>(outputsm["Cm'tot"]);
    double cn1m = boost::python::extract<double>(outputsm["Cn'tot"]);

    solveparams[SANS::LIPSolver::LIPSolveParams::params.alpha]     = alpha0 + da;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.beta]      = beta0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.rollRate]  = roll0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.pitchRate] = pitch0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.yawRate]   = yaw0;

    PyDict outputsp = solver.solve(solveparams);

    double CL1p = boost::python::extract<double>(outputsp["CLtot"]);
    double CY1p = boost::python::extract<double>(outputsp["CYtot"]);
    double cl1p = boost::python::extract<double>(outputsp["Cl'tot"]);
    double cm1p = boost::python::extract<double>(outputsp["Cm'tot"]);
    double cn1p = boost::python::extract<double>(outputsp["Cn'tot"]);

    //solver.writeTecplot("tmp/LIPsolver.dat");

    // Check that the finite difference matches the analytic
    CLaErr[i] = fabs( (CL1p - CL1m)/(2*da) - CLa );
    CYaErr[i] = fabs( (CY1p - CY1m)/(2*da) - CYa );
    claErr[i] = fabs( (cl1p - cl1m)/(2*da) - cla );
    cmaErr[i] = fabs( (cm1p - cm1m)/(2*da) - cma );
    cnaErr[i] = fabs( (cn1p - cn1m)/(2*da) - cna );
  }

  Real small_tol = 1e-11;

  BOOST_CHECK( checkRate(CLaErr, dalpha, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(CYaErr, dalpha, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(claErr, dalpha, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(cmaErr, dalpha, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(cnaErr, dalpha, {1.9, 2.1}, small_tol) );



  std::vector<Real> dbeta = {2e-1, 1e-1};

  std::vector<Real> CLbErr(2);
  std::vector<Real> CYbErr(2);
  std::vector<Real> clbErr(2);
  std::vector<Real> cmbErr(2);
  std::vector<Real> cnbErr(2);

  for (std::size_t i = 0; i < dbeta.size(); i++)
  {
    const Real db = dbeta[i];

    solveparams[SANS::LIPSolver::LIPSolveParams::params.alpha]     = alpha0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.beta]      = beta0 - db;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.rollRate]  = roll0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.pitchRate] = pitch0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.yawRate]   = yaw0;

    PyDict outputsm = solver.solve(solveparams);

    double CL1m = boost::python::extract<double>(outputsm["CLtot"]);
    double CY1m = boost::python::extract<double>(outputsm["CYtot"]);
    double cl1m = boost::python::extract<double>(outputsm["Cl'tot"]);
    double cm1m = boost::python::extract<double>(outputsm["Cm'tot"]);
    double cn1m = boost::python::extract<double>(outputsm["Cn'tot"]);

    solveparams[SANS::LIPSolver::LIPSolveParams::params.alpha]     = alpha0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.beta]      = beta0 + db;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.rollRate]  = roll0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.pitchRate] = pitch0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.yawRate]   = yaw0;

    PyDict outputsp = solver.solve(solveparams);

    double CL1p = boost::python::extract<double>(outputsp["CLtot"]);
    double CY1p = boost::python::extract<double>(outputsp["CYtot"]);
    double cl1p = boost::python::extract<double>(outputsp["Cl'tot"]);
    double cm1p = boost::python::extract<double>(outputsp["Cm'tot"]);
    double cn1p = boost::python::extract<double>(outputsp["Cn'tot"]);

    //solver.writeTecplot("tmp/LIPsolver.dat");

    // Check that the finite difference matches the analytic
    CLbErr[i] = fabs( (CL1p - CL1m)/(2*db) - CLb );
    CYbErr[i] = fabs( (CY1p - CY1m)/(2*db) - CYb );
    clbErr[i] = fabs( (cl1p - cl1m)/(2*db) - clb );
    cmbErr[i] = fabs( (cm1p - cm1m)/(2*db) - cmb );
    cnbErr[i] = fabs( (cn1p - cn1m)/(2*db) - cnb );
  }

  small_tol = 1e-11;

  BOOST_CHECK( checkRate(CLbErr, dbeta, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(CYbErr, dbeta, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(clbErr, dbeta, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(cmbErr, dbeta, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(cnbErr, dbeta, {1.9, 2.1}, small_tol) );



  std::vector<Real> droll = {2e-1, 1e-1};

  std::vector<Real> CXpErr(2);
  std::vector<Real> CYpErr(2);
  std::vector<Real> CZpErr(2);
  std::vector<Real> ClpErr(2);
  std::vector<Real> CmpErr(2);
  std::vector<Real> CnpErr(2);

  for (std::size_t i = 0; i < droll.size(); i++)
  {
    const Real dp = droll[i];

    solveparams[SANS::LIPSolver::LIPSolveParams::params.alpha]     = alpha0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.beta]      = beta0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.rollRate]  = roll0 - dp;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.pitchRate] = pitch0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.yawRate]   = yaw0;

    PyDict outputsm = solver.solve(solveparams);

    double CX1m = boost::python::extract<double>(outputsm["CXtot"]);
    double CY1m = boost::python::extract<double>(outputsm["CYtot"]);
    double CZ1m = boost::python::extract<double>(outputsm["CZtot"]);
    double Cl1m = boost::python::extract<double>(outputsm["Cltot"]);
    double Cm1m = boost::python::extract<double>(outputsm["Cmtot"]);
    double Cn1m = boost::python::extract<double>(outputsm["Cntot"]);

    solveparams[SANS::LIPSolver::LIPSolveParams::params.alpha]     = alpha0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.beta]      = beta0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.rollRate]  = roll0 + dp;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.pitchRate] = pitch0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.yawRate]   = yaw0;

    PyDict outputsp = solver.solve(solveparams);

    double CX1p = boost::python::extract<double>(outputsp["CXtot"]);
    double CY1p = boost::python::extract<double>(outputsp["CYtot"]);
    double CZ1p = boost::python::extract<double>(outputsp["CZtot"]);
    double Cl1p = boost::python::extract<double>(outputsp["Cltot"]);
    double Cm1p = boost::python::extract<double>(outputsp["Cmtot"]);
    double Cn1p = boost::python::extract<double>(outputsp["Cntot"]);

    //solver.writeTecplot("tmp/LIPsolver.dat");

    // Check that the finite difference matches the analytic
    CXpErr[i] = fabs( (CX1p - CX1m)/(2*dp) - CXp );
    CYpErr[i] = fabs( (CY1p - CY1m)/(2*dp) - CYp );
    CZpErr[i] = fabs( (CZ1p - CZ1m)/(2*dp) - CZp );
    ClpErr[i] = fabs( (Cl1p - Cl1m)/(2*dp) - Clp );
    CmpErr[i] = fabs( (Cm1p - Cm1m)/(2*dp) - Cmp );
    CnpErr[i] = fabs( (Cn1p - Cn1m)/(2*dp) - Cnp );
  }

  small_tol = 1e-10;

  BOOST_CHECK( checkRate(CXpErr, droll, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(CYpErr, droll, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(CZpErr, droll, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(ClpErr, droll, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(CmpErr, droll, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(CnpErr, droll, {1.9, 2.1}, small_tol) );



  std::vector<Real> dpitch = {2e-1, 1e-1};

  std::vector<Real> CXqErr(2);
  std::vector<Real> CYqErr(2);
  std::vector<Real> CZqErr(2);
  std::vector<Real> ClqErr(2);
  std::vector<Real> CmqErr(2);
  std::vector<Real> CnqErr(2);

  for (std::size_t i = 0; i < dpitch.size(); i++)
  {
    const Real dq = dpitch[i];

    solveparams[SANS::LIPSolver::LIPSolveParams::params.alpha]     = alpha0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.beta]      = beta0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.rollRate]  = roll0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.pitchRate] = pitch0 - dq;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.yawRate]   = yaw0;

    PyDict outputsm = solver.solve(solveparams);

    double CX1m = boost::python::extract<double>(outputsm["CXtot"]);
    double CY1m = boost::python::extract<double>(outputsm["CYtot"]);
    double CZ1m = boost::python::extract<double>(outputsm["CZtot"]);
    double Cl1m = boost::python::extract<double>(outputsm["Cltot"]);
    double Cm1m = boost::python::extract<double>(outputsm["Cmtot"]);
    double Cn1m = boost::python::extract<double>(outputsm["Cntot"]);

    solveparams[SANS::LIPSolver::LIPSolveParams::params.alpha]     = alpha0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.beta]      = beta0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.rollRate]  = roll0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.pitchRate] = pitch0 + dq;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.yawRate]   = yaw0;

    PyDict outputsp = solver.solve(solveparams);

    double CX1p = boost::python::extract<double>(outputsp["CXtot"]);
    double CY1p = boost::python::extract<double>(outputsp["CYtot"]);
    double CZ1p = boost::python::extract<double>(outputsp["CZtot"]);
    double Cl1p = boost::python::extract<double>(outputsp["Cltot"]);
    double Cm1p = boost::python::extract<double>(outputsp["Cmtot"]);
    double Cn1p = boost::python::extract<double>(outputsp["Cntot"]);

    //solver.writeTecplot("tmp/LIPsolver.dat");

    // Check that the finite difference matches the analytic
    CXqErr[i] = fabs( (CX1p - CX1m)/(2*dq) - CXq );
    CYqErr[i] = fabs( (CY1p - CY1m)/(2*dq) - CYq );
    CZqErr[i] = fabs( (CZ1p - CZ1m)/(2*dq) - CZq );
    ClqErr[i] = fabs( (Cl1p - Cl1m)/(2*dq) - Clq );
    CmqErr[i] = fabs( (Cm1p - Cm1m)/(2*dq) - Cmq );
    CnqErr[i] = fabs( (Cn1p - Cn1m)/(2*dq) - Cnq );
  }

  small_tol = 1e-10;

  BOOST_CHECK( checkRate(CXqErr, dpitch, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(CYqErr, dpitch, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(CZqErr, dpitch, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(ClqErr, dpitch, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(CmqErr, dpitch, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(CnqErr, dpitch, {1.9, 2.1}, small_tol) );


  std::vector<Real> dyaw = {2e-1, 1e-1};

  std::vector<Real> CXrErr(2);
  std::vector<Real> CYrErr(2);
  std::vector<Real> CZrErr(2);
  std::vector<Real> ClrErr(2);
  std::vector<Real> CmrErr(2);
  std::vector<Real> CnrErr(2);

  for (std::size_t i = 0; i < dyaw.size(); i++)
  {
    const Real dr = dyaw[i];

    solveparams[SANS::LIPSolver::LIPSolveParams::params.alpha]     = alpha0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.beta]      = beta0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.rollRate]  = roll0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.pitchRate] = pitch0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.yawRate]   = yaw0 - dr;

    PyDict outputsm = solver.solve(solveparams);

    double CX1m = boost::python::extract<double>(outputsm["CXtot"]);
    double CY1m = boost::python::extract<double>(outputsm["CYtot"]);
    double CZ1m = boost::python::extract<double>(outputsm["CZtot"]);
    double Cl1m = boost::python::extract<double>(outputsm["Cltot"]);
    double Cm1m = boost::python::extract<double>(outputsm["Cmtot"]);
    double Cn1m = boost::python::extract<double>(outputsm["Cntot"]);

    solveparams[SANS::LIPSolver::LIPSolveParams::params.alpha]     = alpha0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.beta]      = beta0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.rollRate]  = roll0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.pitchRate] = pitch0;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.yawRate]   = yaw0 + dr;

    PyDict outputsp = solver.solve(solveparams);

    double CX1p = boost::python::extract<double>(outputsp["CXtot"]);
    double CY1p = boost::python::extract<double>(outputsp["CYtot"]);
    double CZ1p = boost::python::extract<double>(outputsp["CZtot"]);
    double Cl1p = boost::python::extract<double>(outputsp["Cltot"]);
    double Cm1p = boost::python::extract<double>(outputsp["Cmtot"]);
    double Cn1p = boost::python::extract<double>(outputsp["Cntot"]);

    //solver.writeTecplot("tmp/LIPsolver.dat");

    // Check that the finite difference matches the analytic
    CXrErr[i] = fabs( (CX1p - CX1m)/(2*dr) - CXr );
    CYrErr[i] = fabs( (CY1p - CY1m)/(2*dr) - CYr );
    CZrErr[i] = fabs( (CZ1p - CZ1m)/(2*dr) - CZr );
    ClrErr[i] = fabs( (Cl1p - Cl1m)/(2*dr) - Clr );
    CmrErr[i] = fabs( (Cm1p - Cm1m)/(2*dr) - Cmr );
    CnrErr[i] = fabs( (Cn1p - Cn1m)/(2*dr) - Cnr );
  }

  small_tol = 1e-10;

  BOOST_CHECK( checkRate(CXrErr, dyaw, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(CYrErr, dyaw, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(CZrErr, dyaw, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(ClrErr, dyaw, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(CmrErr, dyaw, {1.9, 2.1}, small_tol) );
  BOOST_CHECK( checkRate(CnrErr, dyaw, {1.9, 2.1}, small_tol) );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
