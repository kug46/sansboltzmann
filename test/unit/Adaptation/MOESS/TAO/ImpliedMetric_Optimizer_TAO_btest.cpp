// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// ImpliedMetric_Optimizer_TAO_btest
// testing of the implied metric optimizer class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>
#include <random>

#include "Adaptation/MOESS/MOESS.h"
#include "Adaptation/MOESS/TAO/ImpliedMetric_Optimizer_TAO.h"
#include "Adaptation/MOESS/TAO/TAOException.h"

#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldSpacetime_CG_Cell.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
  typedef ImpliedMetric_Optimizer<PhysD1, TopoD1, TAO> ImpliedMetricOptimizer1D;
  typedef ImpliedMetric_Optimizer<PhysD2, TopoD2, TAO> ImpliedMetricOptimizer2D;
  typedef ImpliedMetric_Optimizer<PhysD3, TopoD3, TAO> ImpliedMetricOptimizer3D;
}

using namespace SANS;

#define SANS_CHECK_PING_ORDER( err_vec, delta_vec, small_tol ) \
    if (err_vec[1] > small_tol){ \
      Real rate = log(err_vec[1]/err_vec[0])/log(delta_vec[1]/delta_vec[0]); \
      BOOST_CHECK_MESSAGE( rate >= 1.7 && rate <= 3.7, "Rate check failed: rate = " << rate << \
                           ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" ); \
    }

namespace  //no-name namespace local to this file
{

template <class PhysDim, class TopoDim>
class DummyProblem : public SolverInterfaceBase<PhysDim, TopoDim>
{

public:

  explicit DummyProblem(int order) : order_(order) {}

  virtual ~DummyProblem() {}

  virtual void solveGlobalPrimalProblem() override {}
  virtual void solveGlobalAdjointProblem() override {}
  virtual void computeErrorEstimates() override {}

  virtual Real getElementalErrorEstimate(int cellgroup, int elem) const override { return 1.0; }
  virtual Real getGlobalErrorEstimate() const override { return 1.0; }
  virtual Real getGlobalErrorIndicator() const override { return 1.0; }

  virtual LocalSolveStatus solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& local_xfld, std::vector<Real>& local_error) const override
  {
    local_error[0] = 0.5;
    return LocalSolveStatus(true, 1);
  }

  virtual Real getnDOFperCell(const int cellgroup) const override
  {
    int nDOF = 0;

         if (PhysDim::D == 1)
      nDOF = order_ + 1;
    else if (PhysDim::D == 2)
      nDOF = (order_ + 1)*(order_ + 2)/2;
    else if (PhysDim::D == 3)
      nDOF = (order_ + 1)*(order_ + 2)*(order_ + 3)/6;

    return nDOF;
  }

  virtual int getSolutionOrder(const int cellgroup) const override { return order_; }

  virtual Real getOutput() const override { return 0; }
  virtual void output_EField(const std::string& filename) const override {}

  virtual int getCost() const override {return 0;}
  SpaceType spaceType() const override {return SpaceType::Discontinuous; }

protected:
  int order_;
};

} // namespace

//############################################################################//
BOOST_AUTO_TEST_SUITE( MOESS_TAO_test_suite )

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
void ping_ImpliedMetric_Optimizer(mpi::communicator& comm,
                                  const std::vector<Real>& x,
                                  const Real small_tol,
                                  ImpliedMetric_Optimizer<PhysDim, TopoDim, TAO>& optimizer)
{
  std::size_t nDim = x.size();

  std::vector<Real> grad_obj(nDim, 0.0);
  std::vector<Real> grad_dummy(nDim, 0.0);
  Real delta[2] = {1e-2, 1e-3};

  // perturbed x values
  std::vector<Real> xp = x;
  std::vector<Real> xm = x;

  Tao tao=nullptr;
  Vec X, G;
  Vec Xp, Xm, Gdummy;
  PetscReal f;
  Real err_obj[2];

  TAO_STATUS( VecCreateSeqWithArray(comm, 1, nDim, x.data(), &X) );
  TAO_STATUS( VecCreateSeqWithArray(comm, 1, nDim, grad_obj.data(), &G) );
  TAO_STATUS( VecCreateSeqWithArray(comm, 1, nDim, xp.data(), &Xp) );
  TAO_STATUS( VecCreateSeqWithArray(comm, 1, nDim, xm.data(), &Xm) );
  TAO_STATUS( VecCreateSeqWithArray(comm, 1, nDim, grad_dummy.data(), &Gdummy) );

  optimizer.FormFunctionGradient(tao, X, &f, G, (void*) &optimizer);

  for (std::size_t i = 0; i < nDim; i++)
  {
    for (int j = 0; j < 2; j++)
    {
      PetscReal fp, fm, grad_approx;

      // perturb
      xp[i] += delta[j];
      xm[i] -= delta[j];

      //Ping objective function
      optimizer.FormFunctionGradient(tao, Xp, &fp, Gdummy, (void*) &optimizer);
      optimizer.FormFunctionGradient(tao, Xm, &fm, Gdummy, (void*) &optimizer);

      // de-perturb
      xp[i] -= delta[j];
      xm[i] += delta[j];

      grad_approx = (fp - fm)/(2.0*delta[j]);
      err_obj[j] = fabs(grad_approx - grad_obj[i]);
    }

    SANS_CHECK_PING_ORDER(err_obj, delta, small_tol);
  }

  TAO_STATUS( VecDestroy(&X) );
  TAO_STATUS( VecDestroy(&G) );
  TAO_STATUS( VecDestroy(&Xp) );
  TAO_STATUS( VecDestroy(&Xm) );
  TAO_STATUS( VecDestroy(&Gdummy) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Ping_ImpliedMetric_Optimizer_1D_Line )
{
  typedef PhysD1 PhysDim;
  typedef TopoD1 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  const Real small_tol = 1e-10;

  XField1D xfld(4);
  // Change the coordinates so the grid is not uniform
  xfld.DOF(1) = 0.1;
  xfld.DOF(2) = 0.3;
  xfld.DOF(3) = 0.6;

  std::vector<int> cellGroups = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  MOESSParams::checkInputs(paramsDict);

  NodalMetrics<PhysDim, TopoDim> nodalMetrics(xfld, cellGroups, dummyProblem, paramsDict);

  int nDim = xfld.nDOF()*MatrixSym::SIZE;
  ImpliedMetric_Optimizer<PhysDim, TopoDim, TAO> optimizer(nodalMetrics);

  std::vector<PetscScalar> x = {0.1, -0.2, 0.5, 1.1, -0.7};

  BOOST_REQUIRE_EQUAL( nDim, x.size() );

  ping_ImpliedMetric_Optimizer(*xfld.comm(), x, small_tol, optimizer);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ImpliedMetric_Optimizer_1D_Line )
{
  typedef PhysD1 PhysDim;
  typedef TopoD1 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  XField1D xfld(10);
  // Change the coordinates so the grid is not uniform
  for (int i = 0; i < xfld.nDOF(); i++)
    xfld.DOF(i) = i*i*i/pow((xfld.nDOF()-1),3);

  std::vector<int> cellGroups = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  MOESSParams::checkInputs(paramsDict);

  NodalMetrics<PhysDim, TopoDim> nodalMetrics(xfld, cellGroups, dummyProblem, paramsDict);

  int nDim = xfld.nDOF()*MatrixSym::SIZE;
  ImpliedMetric_Optimizer<PhysDim, TopoDim, TAO> optimizer(nodalMetrics);

  const NodalMetrics<PhysDim, TopoDim>::MatrixSymFieldType& logMfld = optimizer.logMfld();

  Real complexityTrue =  nodalMetrics.getComplexityTrue();

  std::vector<Real> x(nDim, 0);
  std::vector<Real> grad_obj(nDim, 0.0);
  std::vector<MatrixSym> dcomplexity_dlogM;

  for (int node = 0; node < xfld.nDOF(); node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      x[node*MatrixSym::SIZE + ind] = logMfld.DOF(node).value(ind);

  Tao tao=nullptr;
  Vec X, G;
  PetscReal f;

  TAO_STATUS( VecCreateSeqWithArray(*nodalMetrics.comm(), 1, nDim, x.data(), &X) );
  TAO_STATUS( VecCreateSeqWithArray(*nodalMetrics.comm(), 1, nDim, grad_obj.data(), &G) );

  optimizer.FormFunctionGradient(tao, X, &f, G, (void*) &optimizer);

  //Compute the global complexity
  Real complexity;
  nodalMetrics.computeComplexity(logMfld, complexity, dcomplexity_dlogM);

  BOOST_CHECK_CLOSE( complexityTrue, complexity, 5e-2 );
  BOOST_CHECK_SMALL( f, 1e-7 );

  TAO_STATUS( VecDestroy(&X) );
  TAO_STATUS( VecDestroy(&G) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Ping_ImpliedMetric_Optimizer_2D_Triangle )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  const Real small_tol = 5e-9;

  XField2D_4Triangle_X1_1Group xfld;
  // Change the coordinates so the grid is not uniform
  xfld.DOF(3) = { 1.5, 2.0};
  xfld.DOF(4) = {-0.6, 0.4};
  xfld.DOF(5) = { 0.7,-0.3};

  std::vector<int> cellGroups = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  MOESSParams::checkInputs(paramsDict);

  NodalMetrics<PhysDim, TopoDim> nodalMetrics(xfld, cellGroups, dummyProblem, paramsDict);

  int nDim = xfld.nDOF()*MatrixSym::SIZE;
  ImpliedMetric_Optimizer<PhysDim, TopoDim, TAO> optimizer(nodalMetrics);

  std::vector<Real> x = { 0.1, -0.2,  0.5,
                          0.3, -0.7,  0.4,
                         -0.2,  0.3,  0.6,
                         -0.4,  0.6,  0.5,
                          0.4, -0.1,  0.3,
                         -0.6,  0.5,  0.7};

  BOOST_REQUIRE_EQUAL( nDim, (int) x.size() );

  ping_ImpliedMetric_Optimizer(*xfld.comm(), x, small_tol, optimizer);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ImpliedMetric_Optimizer_2D_Triangle )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  // Grids are intentionally not uniform

  int ii = 5;
  int jj = 4;
  std::vector<Real> xvec(ii+1);
  std::vector<Real> yvec(jj+1);

  // construct vectors
  for (int i = 0; i < ii+1; i++)
    xvec[i] = i*i/Real(ii*ii);

  for (int j = 0; j < jj+1; j++)
    yvec[j] = 2*j*j/Real(jj*jj);

  // create identical global mesh on each processors
  XField2D_Box_Triangle_Lagrange_X1 xfld(comm_local, xvec, yvec);

  std::vector<int> cellGroups = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  MOESSParams::checkInputs(paramsDict);

  NodalMetrics<PhysDim, TopoDim> nodalMetrics(xfld, cellGroups, dummyProblem, paramsDict);

  int nDim = xfld.nDOF()*MatrixSym::SIZE;
  ImpliedMetric_Optimizer<PhysDim, TopoDim, TAO> optimizer(nodalMetrics);

  const NodalMetrics<PhysDim, TopoDim>::MatrixSymFieldType& logMfld = optimizer.logMfld();

  Real complexityTrue =  nodalMetrics.getComplexityTrue();

  std::vector<Real> x(nDim, 0);
  std::vector<Real> grad_obj(nDim, 0.0);
  std::vector<MatrixSym> dcomplexity_dlogM;

  for (int node = 0; node < xfld.nDOF(); node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      x[node*MatrixSym::SIZE + ind] = logMfld.DOF(node).value(ind);

  Tao tao=nullptr;
  Vec X, G;
  PetscReal f;

  TAO_STATUS( VecCreateSeqWithArray(*nodalMetrics.comm(), 1, nDim, x.data(), &X) );
  TAO_STATUS( VecCreateSeqWithArray(*nodalMetrics.comm(), 1, nDim, grad_obj.data(), &G) );

  optimizer.FormFunctionGradient(tao, X, &f, G, (void*) &optimizer);

  //Compute the global complexity
  Real complexity;
  nodalMetrics.computeComplexity(logMfld, complexity, dcomplexity_dlogM);

  BOOST_CHECK_CLOSE( complexityTrue, complexity, 5e-2 );
  BOOST_CHECK_SMALL( f, 1e-7 );

  TAO_STATUS( VecDestroy(&X) );
  TAO_STATUS( VecDestroy(&G) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Ping_ImpliedMetric_Optimizer_3D_Tet )
{
  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  Real small_tol = 1e-10;

  XField3D_6Tet_X1_1Group xfld;
  // Change the coordinates so the grid is not uniform
  xfld.DOF(0) = {-0.1, 0.0, 0.2};
  xfld.DOF(3) = { 1.2, 0.9,-0.2};
  xfld.DOF(5) = { 1.3, 0.0,-0.1};

  std::vector<int> cellGroups = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  MOESSParams::checkInputs(paramsDict);

  NodalMetrics<PhysDim, TopoDim> nodalMetrics(xfld, cellGroups, dummyProblem, paramsDict);

  int nDim = xfld.nDOF()*MatrixSym::SIZE;
  ImpliedMetric_Optimizer<PhysDim, TopoDim, TAO> optimizer(nodalMetrics);

  std::vector<Real> x = { 0.1, -0.2,  0.5,  0.3, -0.4,  0.8,
                          0.3, -0.7,  0.4, -0.1,  0.9, -0.5,
                         -0.2,  0.3,  0.6,  0.0,  0.1,  0.9,
                         -0.4,  0.6,  0.7, -0.6,  0.3,  0.2,
                          0.4,  0.0,  0.3,  0.8,  0.7, -0.5,
                         -0.6,  0.5,  0.7, -0.2,  0.8,  0.3,
                          0.5,  0.4, -0.2,  0.7,  0.2,  0.0,
                          0.7,  0.0,  0.1, -0.5,  0.4,  0.6 };

  BOOST_REQUIRE_EQUAL( nDim, (int) x.size() );

  ping_ImpliedMetric_Optimizer(*xfld.comm(), x, small_tol, optimizer);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ImpliedMetric_Optimizer_3D_Tet )
{
  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  // Grids are intentionally not uniform

  int ii = 2;
  int jj = 3;
  int kk = 4;
  std::vector<Real> xvec(ii+1);
  std::vector<Real> yvec(jj+1);
  std::vector<Real> zvec(kk+1);

  // construct vectors
  for (int i = 0; i < ii+1; i++)
    xvec[i] = i*i/Real(ii*ii);

  for (int j = 0; j < jj+1; j++)
    yvec[j] = 2*j*j/Real(jj*jj);

  for (int k = 0; k < kk+1; k++)
    zvec[k] = 3*k*k/Real(kk*kk);

  // create identical global mesh on each processors
  XField3D_Box_Tet_X1 xfld(comm_local, xvec, yvec, zvec);

  std::vector<int> cellGroups = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  MOESSParams::checkInputs(paramsDict);

  NodalMetrics<PhysDim, TopoDim> nodalMetrics(xfld, cellGroups, dummyProblem, paramsDict);

  int nDim = xfld.nDOF()*MatrixSym::SIZE;
  ImpliedMetric_Optimizer<PhysDim, TopoDim, TAO> optimizer(nodalMetrics);

  const NodalMetrics<PhysDim, TopoDim>::MatrixSymFieldType& logMfld = optimizer.logMfld();

  Real complexityTrue =  nodalMetrics.getComplexityTrue();

  std::vector<Real> x(nDim, 0);
  std::vector<Real> grad_obj(nDim, 0.0);
  std::vector<MatrixSym> dcomplexity_dlogM;

  for (int node = 0; node < xfld.nDOF(); node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      x[node*MatrixSym::SIZE + ind] = logMfld.DOF(node).value(ind);

  Tao tao=nullptr;
  Vec X, G;
  PetscReal f;

  TAO_STATUS( VecCreateSeqWithArray(*nodalMetrics.comm(), 1, nDim, x.data(), &X) );
  TAO_STATUS( VecCreateSeqWithArray(*nodalMetrics.comm(), 1, nDim, grad_obj.data(), &G) );

  optimizer.FormFunctionGradient(tao, X, &f, G, (void*) &optimizer);

  //Compute the global complexity
  Real complexity;
  nodalMetrics.computeComplexity(logMfld, complexity, dcomplexity_dlogM);

  BOOST_CHECK_CLOSE( complexityTrue, complexity, 5e-2 );
  BOOST_CHECK_SMALL( f, 1e-7 );

  TAO_STATUS( VecDestroy(&X) );
  TAO_STATUS( VecDestroy(&G) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Serial_Parallel_Equivalency_TAO )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-10;

  // parallel and serial communicator
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  // use a non-uniform mesh for better testing
  std::vector<Real> xvec = {0, 0.25, 0.75, 1};
  std::vector<Real> yvec = {0, 0.25, 0.75, 1};

  XField2D_Box_Triangle_Lagrange_X1 xfld_parallel( world, xvec, yvec ); // partitioned system
  XField2D_Box_Triangle_Lagrange_X1 xfld_serial( comm, xvec, yvec );  // complete system on all processors

  std::vector<int> cellGroups = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  MOESSParams::checkInputs(paramsDict);

  NodalMetrics<PhysDim, TopoDim> nodalMetrics_parallel(xfld_parallel, cellGroups, dummyProblem, paramsDict);
  NodalMetrics<PhysDim, TopoDim> nodalMetrics_serial(xfld_serial, cellGroups, dummyProblem, paramsDict);

  // check that the global complexity is consistent
  BOOST_CHECK_CLOSE(nodalMetrics_serial.getComplexityTrue(), nodalMetrics_parallel.getComplexityTrue(), 1e-9);

  ImpliedMetric_Optimizer<PhysDim, TopoDim, TAO> optimizer_parallel(nodalMetrics_parallel);
  ImpliedMetric_Optimizer<PhysDim, TopoDim, TAO> optimizer_serial(nodalMetrics_serial);

  const NodalMetrics<PhysDim, TopoDim>::MatrixSymFieldType& logMfld_parallel = optimizer_parallel.logMfld();
  const NodalMetrics<PhysDim, TopoDim>::MatrixSymFieldType& logMfld_serial = optimizer_serial.logMfld();

  int nDim_parallel = logMfld_parallel.nDOFpossessed()*MatrixSym::SIZE;
  int nDim_serial   = logMfld_serial.nDOFpossessed()*MatrixSym::SIZE;

  // initialize a uniform distribution between -2*log(2) and 2*log(2)
  std::mt19937_64 rng;
  std::uniform_real_distribution<double> unif(-2*log(2), 2*log(2));

  // initialize the serial vector perturbed with random numbers
  std::vector<Real> x_serial(nDim_serial);
  for ( int i = 0; i < logMfld_serial.nDOF(); i++ )
    for ( int j = 0; j < MatrixSym::SIZE; j++ )
      x_serial[i*MatrixSym::SIZE+j] = logMfld_serial.DOF(i).value(j) + unif(rng);

  // copy over the serial portion to the equivalent parallel vector
  std::vector<Real> x_parallel(nDim_parallel);
  for ( int i = 0; i < logMfld_parallel.nDOFpossessed(); i++ )
    for ( int j = 0; j < MatrixSym::SIZE; j++ )
    {
      int i_serial = logMfld_parallel.local2nativeDOFmap(i);
      int k_serial = i_serial*MatrixSym::SIZE+j;
      int k_parallel = i*MatrixSym::SIZE+j;
      x_parallel[k_parallel] = x_serial[k_serial];
    }

  std::vector<Real> grad_obj_parallel(nDim_parallel, 0.0);
  std::vector<Real> grad_obj_serial(nDim_serial, 0.0);

  Tao tao=nullptr;
  Vec X_parallel, G_parallel;
  Vec X_serial, G_serial;
  PetscReal f_parallel, f_serial;

  TAO_STATUS( VecCreateMPIWithArray(world, 1, nDim_parallel, PETSC_DECIDE, x_parallel.data(), &X_parallel) );
  TAO_STATUS( VecCreateMPIWithArray(world, 1, nDim_parallel, PETSC_DECIDE, grad_obj_parallel.data(), &G_parallel) );

  TAO_STATUS( VecCreateMPIWithArray(comm, 1, nDim_serial, PETSC_DECIDE, x_serial.data(), &X_serial) );
  TAO_STATUS( VecCreateMPIWithArray(comm, 1, nDim_serial, PETSC_DECIDE, grad_obj_serial.data(), &G_serial) );

  optimizer_parallel.FormFunctionGradient(tao, X_parallel, &f_parallel, G_parallel, (void*) &optimizer_parallel);
  optimizer_serial.FormFunctionGradient(tao, X_serial, &f_serial, G_serial, (void*) &optimizer_serial);

  // Check that the logM fields are consistent after the call to FormFunctionGradient
  for ( int i = 0; i < logMfld_parallel.nDOF(); i++ )
  {
    int i_serial = logMfld_parallel.local2nativeDOFmap(i);
    for ( int j = 0; j < MatrixSym::SIZE; j++ )
      SANS_CHECK_CLOSE( logMfld_serial.DOF(i_serial).value(j), logMfld_parallel.DOF(i).value(j), small_tol, close_tol );
  }

  // Check that the implied metrics are consistent
  const Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& implied_metric_parallel = nodalMetrics_parallel.impliedMetrics();
  const Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& implied_metric_serial = nodalMetrics_serial.impliedMetrics();
  for ( int i = 0; i < implied_metric_parallel.nDOF(); i++ )
  {
    int i_serial = implied_metric_parallel.local2nativeDOFmap(i);
    for ( int j = 0; j < MatrixSym::SIZE; j++ )
      SANS_CHECK_CLOSE( implied_metric_serial.DOF(i_serial).value(j), implied_metric_parallel.DOF(i).value(j), small_tol, close_tol );
  }

  // Check that the objective value is the same on all processors
  SANS_CHECK_CLOSE( f_serial, f_parallel, small_tol, close_tol );

  for (int rank = 0; rank < world.size(); rank++)
  {
    world.barrier();
    if (rank != world.rank()) continue;

    // check that the gradient is consistent between the serial and parallel calculations
    for ( int i = 0; i < logMfld_parallel.nDOFpossessed(); i++ )
    {
      //std::cout << rank << " is " << logMfld_parallel.local2nativeDOFmap(i) << " ip " << i << std::endl;
      for ( int j = 0; j < MatrixSym::SIZE; j++ )
      {
        int i_serial = logMfld_parallel.local2nativeDOFmap(i);
        int k_serial = i_serial*MatrixSym::SIZE+j;
        int k_parallel = i*MatrixSym::SIZE+j;
        SANS_CHECK_CLOSE( grad_obj_serial[k_serial], grad_obj_parallel[k_parallel], small_tol, close_tol );
      }
    }
  }

  TAO_STATUS( VecDestroy(&X_parallel) );
  TAO_STATUS( VecDestroy(&G_parallel) );
  TAO_STATUS( VecDestroy(&X_serial) );
  TAO_STATUS( VecDestroy(&G_serial) );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
