// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// MOESS_SANS_Optimizer_TAO_btest
// testing of the MOESS TAO optimizer class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>
#include <array>
#include <random>

#include "tools/linspace.h"

#include "Adaptation/MOESS/SolverInterfaceBase.h"
#include "Adaptation/MOESS/TAO/MOESS_SANS_Optimizer_TAO.h"
#include "Adaptation/MOESS/TAO/TAOException.h"

#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldSpacetime_CG_Cell.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
}

using namespace SANS;

#define SANS_CHECK_PING_ORDER( err_vec, delta_vec, small_tol ) \
    if (std::abs(err_vec[1]) > small_tol) \
    { \
      Real rate = log(err_vec[1]/err_vec[0])/log(delta_vec[1]/delta_vec[0]); \
      BOOST_CHECK_MESSAGE( rate >= 1.9 && rate <= 3.7, "Rate check failed: rate = " << rate << \
                           ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" ); \
    }

template <class PhysDim, class TopoDim>
class DummyProblem : public SolverInterfaceBase<PhysDim, TopoDim>
{
public:

  explicit DummyProblem(const XField<PhysDim, TopoDim>& xfld, const int order) : order_(order), xfld_(xfld) {}

  virtual ~DummyProblem() {}

  virtual void solveGlobalPrimalProblem() override {}
  virtual void solveGlobalAdjointProblem() override {}
  virtual void computeErrorEstimates() override {}

  virtual Real getElementalErrorEstimate(int cellgroup, int elem) const override { return 1.0; }
  virtual Real getGlobalErrorEstimate() const override { return 1.0; }
  virtual Real getGlobalErrorIndicator() const override { return 1.0; }

  virtual LocalSolveStatus solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& local_xfld, std::vector<Real>& local_error) const override
  {
    std::pair<int,int> cellmap = local_xfld.getGlobalCellMap({0, 0});
    int global_cellgrp  = cellmap.first;
    int global_cellelem = cellmap.second;

    int cellID = xfld_.cellIDs(global_cellgrp)[global_cellelem];

    // make sure the error varies "randomly" throughout the grid so parallel test is more rigorous
    local_error[0] = 0.5 + cellID;

    return LocalSolveStatus(true, 1);
  }

  virtual Real getnDOFperCell(const int cellgroup) const override
  {
    int nDOF = 0;

         if (PhysDim::D == 1)
      nDOF = order_ + 1;
    else if (PhysDim::D == 2)
      nDOF = (order_ + 1)*(order_ + 2)/2;
    else if (PhysDim::D == 3)
      nDOF = (order_ + 1)*(order_ + 2)*(order_ + 3)/6;

    return nDOF;
  }

  virtual int getSolutionOrder(const int cellgroup) const override { return order_; }

  virtual Real getOutput() const override { return 0; }
  virtual void output_EField(const std::string& filename ) const override {}

  virtual int getCost() const override {return 0;}
  SpaceType spaceType() const override {return SpaceType::Discontinuous; }

protected:
  const int order_;
  const XField<PhysDim, TopoDim>& xfld_;
};


//############################################################################//
BOOST_AUTO_TEST_SUITE( MOESS_TAO_test_suite )

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
void ping_MOESS_SANS_Optimizer(mpi::communicator& comm,
                               const std::vector<Real>& x,
                               const Real small_tol,
                               MOESS_SANS_Optimizer<PhysDim, TopoDim, TAO>& optimizer)
{
  PetscInt nDim = x.size();

  // perturbed x values
  std::vector<Real> xp = x;
  std::vector<Real> xm = x;

  std::vector<Real> grad_auglag(nDim, 0.0);
  std::vector<Real> grad_obj(nDim, 0.0);
  std::vector<Real> grad_cost(nDim, 0.0);
  std::vector<Real> grad_dummy(nDim, 0.0);
  PetscReal ce[2] = {0, 0};
  PetscReal cep[2] = {0, 0};
  PetscReal cem[2] = {0, 0};
  Real delta[2] = {1e-2, 1e-3};

  Tao tao=nullptr;
  Vec X, G, Gauglag, G_dummy, CE, CEp, CEm;
  Vec Xp, Xm;
  Mat JE;
  PetscReal f;

  TAO_STATUS( VecCreateSeqWithArray(comm, 1, nDim, x.data(), &X) );
  TAO_STATUS( VecCreateSeqWithArray(comm, 1, nDim, grad_auglag.data(), &Gauglag) );
  TAO_STATUS( VecCreateSeqWithArray(comm, 1, nDim, grad_obj.data(), &G) );
  TAO_STATUS( VecCreateSeqWithArray(comm, 1, nDim, grad_dummy.data(), &G_dummy) );
  TAO_STATUS( VecCreateSeqWithArray(comm, 1, nDim, xp.data(), &Xp) );
  TAO_STATUS( VecCreateSeqWithArray(comm, 1, nDim, xm.data(), &Xm) );

  TAO_STATUS( VecCreateSeqWithArray(PETSC_COMM_SELF, 1, 1, ce, &CE) );
  TAO_STATUS( VecCreateSeqWithArray(PETSC_COMM_SELF, 1, 1, cep, &CEp) );
  TAO_STATUS( VecCreateSeqWithArray(PETSC_COMM_SELF, 1, 1, cem, &CEm) );
  TAO_STATUS( MatCreateDense(comm,PETSC_DECIDE,nDim,1,PETSC_DECIDE,NULL,&JE) );

  optimizer.FormFunctionGradientAugLag(tao, X, &f, Gauglag, (void*) &optimizer);
#if 0
  optimizer.FormFunctionGradient(tao, X, &f, G, (void*) &optimizer);
  optimizer.FormEqualityConstraints(tao, X, CE, (void*) &optimizer);
  optimizer.FormEqualityJacobian(tao, X, JE, JE, (void*) &optimizer);
#endif

  PetscInt Cost = 0;

  std::vector<PetscInt> cols = linspace(0, nDim-1);

  TAO_STATUS( MatGetValues(JE, 1, &Cost, cols.size(), cols.data(), grad_cost.data()) );

  PetscReal err_auglag[2] = {0,0}; //, err_obj[2] = {0,0}, err_cost[2] = {0,0};
  for (PetscInt i = 0; i < nDim; i++)
  {
    for (int j = 0; j < 2; j++)
    {
      PetscReal fp, fm, grad_approx;

      // perturb
      xp[i] += delta[j];
      xm[i] -= delta[j];

      //Ping auglag objective function
      optimizer.FormFunctionGradientAugLag(tao, Xp, &fp, G_dummy, (void*) &optimizer);
      optimizer.FormFunctionGradientAugLag(tao, Xm, &fm, G_dummy, (void*) &optimizer);
      grad_approx = (fp - fm)/(2.0*delta[j]);
      err_auglag[j] = fabs(grad_approx - grad_auglag[i]);

#if 0
      //Ping objective function
      optimizer.FormFunctionGradient(tao, Xp, &fp, G_dummy, (void*) &optimizer);
      optimizer.FormFunctionGradient(tao, Xm, &fm, G_dummy, (void*) &optimizer);
      grad_approx = (fp - fm)/(2.0*delta[j]);
      err_obj[j] = fabs(grad_approx - grad_obj[i]);

      // Equality constraints
      optimizer.FormEqualityConstraints(tao, Xp, CEp, (void*) &optimizer);
      optimizer.FormEqualityConstraints(tao, Xm, CEm, (void*) &optimizer);

      //Ping cost constraint
      fp = cep[Cost];
      fm = cem[Cost];
      grad_approx = (fp - fm)/(2.0*delta[j]);
      err_cost[j] = fabs(grad_approx - grad_cost[i]);
#endif
      // de-perturb
      xp[i] -= delta[j];
      xm[i] += delta[j];
    }

    SANS_CHECK_PING_ORDER(err_auglag, delta, small_tol);
    //SANS_CHECK_PING_ORDER(err_obj, delta, small_tol);
    //SANS_CHECK_PING_ORDER(err_cost, delta, small_tol);
  }

  TAO_STATUS( VecDestroy(&X) );
  TAO_STATUS( VecDestroy(&Gauglag) );
  TAO_STATUS( VecDestroy(&G) );
  TAO_STATUS( VecDestroy(&G_dummy) );
  TAO_STATUS( VecDestroy(&CE) );
  TAO_STATUS( VecDestroy(&CEp) );
  TAO_STATUS( VecDestroy(&CEm) );
  TAO_STATUS( VecDestroy(&Xp) );
  TAO_STATUS( VecDestroy(&Xm) );
  TAO_STATUS( MatDestroy(&JE) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Ping_MOESS_SANS_Optimizer_1D_Line )
{
  typedef PhysD1 PhysDim;
  typedef TopoD1 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;
  typedef ErrorModel<PhysDim,TopoDim> ErrorModelType;

  const Real small_tol = 1e-10;

  XField1D xfld(4);
  const Real h_domain_max = 1;

  std::vector<int> cellGroups = {0};

  Real targetCost = 16;

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(xfld, order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.None;
  MOESSParams::checkInputs(paramsDict);

  // construct an error model and perform the synthesis
  std::shared_ptr<ErrorModelType> errorModel = std::make_shared<ErrorModelType>(xfld, cellGroups, dummyProblem, paramsDict);
  errorModel->synthesize(xfld);

  MOESS_SANS_Optimizer<PhysDim, TopoDim, TAO> optimizer(targetCost, h_domain_max, errorModel, xfld, cellGroups, dummyProblem, paramsDict);

  int nNode = xfld.nDOF();
  int nDim = nNode*MatrixSym::SIZE;

  std::vector<Real> x = {0.1, -0.2, 0.5, 1.1, -0.7};

  BOOST_REQUIRE_EQUAL( nDim, x.size() );

  ping_MOESS_SANS_Optimizer(*xfld.comm(), x, small_tol, optimizer);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Ping_MOESS_SANS_Optimizer_2D_Triangle )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;
  typedef ErrorModel<PhysDim,TopoDim> ErrorModelType;

  const Real small_tol = 1e-10;

  XField2D_4Triangle_X1_1Group xfld;
  const Real h_domain_max = 2;

  std::vector<int> cellGroups = {0};

  Real targetCost = 24;

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(xfld, order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.None;
  MOESSParams::checkInputs(paramsDict);

  // construct an error model and perform the synthesis
  std::shared_ptr<ErrorModelType> errorModel = std::make_shared<ErrorModelType>(xfld, cellGroups, dummyProblem, paramsDict);
  errorModel->synthesize(xfld);

  MOESS_SANS_Optimizer<PhysDim, TopoDim, TAO> optimizer(targetCost, h_domain_max, errorModel, xfld, cellGroups, dummyProblem, paramsDict);

  int nNode = xfld.nDOF();
  int nDim = nNode*MatrixSym::SIZE;

  std::vector<Real> x = { 0.1, -0.2,  0.5,
                          0.3, -0.7,  0.4,
                         -0.2,  0.3,  0.6,
                         -0.4,  0.6,  0.5,
                          0.4,  0.0,  0.3,
                         -0.6,  0.5,  0.7};

  BOOST_REQUIRE_EQUAL( nDim, x.size() );

  ping_MOESS_SANS_Optimizer(*xfld.comm(), x, small_tol, optimizer);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Ping_MOESS_SANS_Optimizer_3D_Tet )
{
  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;
  typedef ErrorModel<PhysDim,TopoDim> ErrorModelType;

  Real small_tol = 1e-10;

  XField3D_6Tet_X1_1Group xfld;
  const Real h_domain_max = 1;

  std::vector<int> cellGroups = {0};

  Real targetCost = 48;

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(xfld, order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.None;
  MOESSParams::checkInputs(paramsDict);

  // construct an error model and perform the synthesis
  std::shared_ptr<ErrorModelType> errorModel = std::make_shared<ErrorModelType>(xfld, cellGroups, dummyProblem, paramsDict);
  errorModel->synthesize(xfld);

  MOESS_SANS_Optimizer<PhysDim, TopoDim, TAO> optimizer(targetCost, h_domain_max, errorModel, xfld, cellGroups, dummyProblem, paramsDict);

  int nNode = xfld.nDOF();
  int nDim = nNode*MatrixSym::SIZE;

  std::vector<Real> x = { 0.1, -0.2,  0.5,  0.3, -0.4,  0.8,
                          0.3, -0.7,  0.4, -0.1,  0.9, -0.5,
                         -0.2,  0.3,  0.6,  0.0,  0.1,  0.9,
                         -0.1,  0.6,  0.5, -0.6,  0.3,  0.2,
                          0.4,  0.0,  0.3,  0.8,  0.7, -0.5,
                         -0.6,  0.5,  0.4, -0.2,  0.8,  0.3,
                          0.5,  0.4, -0.2,  0.7,  0.2,  0.0,
                          0.7,  0.0,  0.1, -0.5,  0.4,  0.6 };

  BOOST_REQUIRE_EQUAL( nDim, x.size() );

  ping_MOESS_SANS_Optimizer(*xfld.comm(), x, small_tol, optimizer);
}

#if 0 // TODO: This test is not reliable. It randomly fails on different machines...
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Parallel_Serial_Equiv_SANS_2D_Triangle )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;
  typedef ErrorModel<PhysDim,TopoDim> ErrorModelType;
  typedef NodalMetrics<PhysDim,TopoDim> NodalMetricsType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-6;

  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  int ii = 4, jj = 5;
  XField2D_Box_Triangle_Lagrange_X1 xfld_parallel(world, ii, jj);

  XField2D_Box_Triangle_Lagrange_X1 xfld_serial(comm, ii, jj);

  Real targetCost = ii*jj*2*3* 1.5; // increase cost by %50
  Real h_domain_max = 1;

  std::vector<int> cellGroups = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem_parallel(xfld_parallel, order);
  DummyProblem<PhysDim,TopoDim> dummyProblem_serial(xfld_serial, order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  MOESSParams::checkInputs(paramsDict);

  //--------
  // construct a parallel error model and perform the synthesis
  std::shared_ptr<ErrorModelType> errorModel_parallel = std::make_shared<ErrorModelType>(xfld_parallel, cellGroups,
                                                                                         dummyProblem_parallel, paramsDict);
  errorModel_parallel->synthesize(xfld_parallel);

  MOESS_SANS_Optimizer<PhysDim, TopoDim, TAO> optimizer_parallel(targetCost, h_domain_max,
                                                                 errorModel_parallel,
                                                                 xfld_parallel, cellGroups, dummyProblem_parallel, paramsDict);

  std::shared_ptr<NodalMetricsType> nodalMetrics_parallel = optimizer_parallel.nodalMetrics();

  //Get nodal metric request for parallelized mesh
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> metric_req_parallel(xfld_parallel, 1, BasisFunctionCategory_Lagrange, cellGroups);
  nodalMetrics_parallel->getNodalMetricRequestField(metric_req_parallel);
  //--------

  world.barrier();

  //--------
  // construct a serial error model and perform the synthesis
  std::shared_ptr<ErrorModelType> errorModel_serial = std::make_shared<ErrorModelType>(xfld_serial, cellGroups,
                                                                                       dummyProblem_serial, paramsDict);
  errorModel_serial->synthesize(xfld_serial);

  MOESS_SANS_Optimizer<PhysDim, TopoDim, TAO> optimizer_serial(targetCost, h_domain_max,
                                                               errorModel_serial,
                                                               xfld_serial, cellGroups, dummyProblem_serial, paramsDict);

  std::shared_ptr<NodalMetricsType> nodalMetrics_serial = optimizer_serial.nodalMetrics();

  // Field on all ranks (all elements on all ranks)
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> metric_req_serial(xfld_serial, 1, BasisFunctionCategory_Hierarchical, cellGroups);
  nodalMetrics_serial->getNodalMetricRequestField(metric_req_serial);
  //--------

  int nNode = metric_req_parallel.nDOF();

  for (int rank = 0; rank < world.size(); rank++)
  {
    world.barrier();
    if (rank != world.rank()) continue;

    // check that the parallelized metrics match the serial metrics (full mesh)
    for (int node = 0; node < nNode; node++)
      for (int i = 0; i < MatrixSym::SIZE; i++)
      {
        int native_node = metric_req_parallel.local2nativeDOFmap(node);

        //std::cout << rank << " " << native_node
        //          << " serial " << metric_req_serial.DOF(native_node).value(i)
        //          << " parallel " << metric_req_parallel.DOF(node).value(i) << std::endl;
#if defined(__INTEL_COMPILER) || defined(INTEL_MKL)
        // intel optimization really changes the serial .vs parallel behaviour. Not sure why...
        const Real close_tol = 5000;
#else
        const Real close_tol = 1;
#endif
        SANS_CHECK_CLOSE( metric_req_serial.DOF(native_node).value(i),
                          metric_req_parallel.DOF(node).value(i), small_tol, close_tol );
      }
  }


  int nDim_parallel = metric_req_parallel.nDOFpossessed()*MatrixSym::SIZE;
  int nDim_serial   = metric_req_serial.nDOFpossessed()*MatrixSym::SIZE;

  // initialize a uniform distribution between -2*log(2) and 2*log(2)
  std::mt19937_64 rng;
  std::uniform_real_distribution<double> unif(-2*log(2), 2*log(2));

  // initialize the serial vector perturbed with random numbers
  std::vector<Real> x_serial(nDim_serial);
  for ( int i = 0; i < metric_req_serial.nDOF(); i++ )
    for ( int j = 0; j < MatrixSym::SIZE; j++ )
      x_serial[i*MatrixSym::SIZE+j] = unif(rng);

  // copy over the serial portion to the equivalent parallel vector
  std::vector<Real> x_parallel(nDim_parallel);
  for ( int i = 0; i < metric_req_parallel.nDOFpossessed(); i++ )
    for ( int j = 0; j < MatrixSym::SIZE; j++ )
    {
      int i_serial = metric_req_parallel.local2nativeDOFmap(i);
      int k_serial = i_serial*MatrixSym::SIZE+j;
      int k_parallel = i*MatrixSym::SIZE+j;
      x_parallel[k_parallel] = x_serial[k_serial];
    }

  std::vector<Real> grad_obj_parallel(nDim_parallel, 0.0);
  std::vector<Real> grad_obj_serial(nDim_serial, 0.0);

  Tao tao=nullptr;
  Vec X_parallel, G_parallel;
  Vec X_serial, G_serial;
  PetscReal f_parallel, f_serial;

  TAO_STATUS( VecCreateMPIWithArray(world, 1, nDim_parallel, PETSC_DECIDE, x_parallel.data(), &X_parallel) );
  TAO_STATUS( VecCreateMPIWithArray(world, 1, nDim_parallel, PETSC_DECIDE, grad_obj_parallel.data(), &G_parallel) );

  TAO_STATUS( VecCreateMPIWithArray(comm, 1, nDim_serial, PETSC_DECIDE, x_serial.data(), &X_serial) );
  TAO_STATUS( VecCreateMPIWithArray(comm, 1, nDim_serial, PETSC_DECIDE, grad_obj_serial.data(), &G_serial) );

  optimizer_parallel.FormFunctionGradientAugLag(tao, X_parallel, &f_parallel, G_parallel, (void*) &optimizer_parallel);
  optimizer_serial.FormFunctionGradientAugLag(tao, X_serial, &f_serial, G_serial, (void*) &optimizer_serial);

  // Check that the objective value is the same on all processors
  SANS_CHECK_CLOSE( f_serial, f_parallel, small_tol, close_tol );

  for (int rank = 0; rank < world.size(); rank++)
  {
    world.barrier();
    if (rank != world.rank()) continue;

    // check that the gradient is consistent between the serial and parallel calculations
    for ( int i = 0; i < metric_req_parallel.nDOFpossessed(); i++ )
    {
      //std::cout << rank << " is " << logMfld_parallel.local2nativeDOFmap(i) << " ip " << i << std::endl;
      for ( int j = 0; j < MatrixSym::SIZE; j++ )
      {
        int i_serial = metric_req_parallel.local2nativeDOFmap(i);
        int k_serial = i_serial*MatrixSym::SIZE+j;
        int k_parallel = i*MatrixSym::SIZE+j;
        SANS_CHECK_CLOSE( grad_obj_serial[k_serial], grad_obj_parallel[k_parallel], small_tol, close_tol );
      }
    }
  }

  TAO_STATUS( VecDestroy(&X_parallel) );
  TAO_STATUS( VecDestroy(&G_parallel) );
  TAO_STATUS( VecDestroy(&X_serial) );
  TAO_STATUS( VecDestroy(&G_serial) );
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
