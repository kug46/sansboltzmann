// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

function ReferenceStepMatrix_btest

clear;clc;close all;

% topological number of the simplex
n = 4;

% coordinates of the reference simplex
X = unitCoordinates(n);

% number of edges
ne = n*(n+1)/2;

computeStepMatrix = @(m0,m1) logm(inv(sqrtm(m0)) * m1 *inv(sqrtm(m0)));

mk0 = impliedMetric( X );

X0 = X;

% loop through all splits and compute the implied metric of each split elem
for i=1:n+1

    for j=i+1:n+1

        fprintf('splitting edge (%d,%d)\n',i,j);

        % compute the coordinates of the split vertex
        xs = 0.5*( X(i,:) +X(j,:) );

        % assign the first edge vertex as the split one
        X(i,:) = xs;
        mk1 = impliedMetric( X );

        % assign the second edge vertex as the split one
        X = X0;
        X(j,:) = xs;
        mk2 = impliedMetric( X );

        % restore the coordinates
        X = X0;

        % compute the affine invariant average
        st = 0.5*computeStepMatrix(mk1,mk2);
        mk = sqrtm(mk1) * expm(st) * sqrtm(mk1);

        % compute the step matrix
        sk = computeStepMatrix( mk0 , mk );

        for ii=1:n
            for jj=ii:n
                fprintf('SANS_CHECK_CLOSE( %1.16e , S(%d,%d) , tol , tol );\n',sk(ii,jj),ii-1,jj-1);
            end
        end
    end

end

end

function mk = impliedMetric( X )

n = size(X,1) -1;

% unit n-simplex coordinates
X0 = unitCoordinates(n);
J  = X0(2:n+1,:) -X0(1:n,:);
J  = J';

J = vpa(J);
invJeq = inv(J);

J = X(2:n+1,:) -X(1:n,:);
J = J';

J = J*invJeq;

mk = inv( J*J' );

end

function X0 = unitCoordinates( n )

if n==1
    X0 = [0;1];
elseif n==2
    % triangle
    x0 = [0,0];
    x1 = [1,0];
    x2 = [1/2, sqrt(sym(3))/2];
    X0 = [x0;x1;x2];
elseif n==3

    % tetrahedron
    x0 = [0,0,0];
    x1 = [1,0,0];
    x2 = [1/2,sqrt(sym(3))/2,0];
    x3 = [1/2,sqrt(sym(3))/6,sqrt(sym(2)/sym(3))];
    X0 = [x0;x1;x2;x3];
elseif n==4

    % pentatope: edge lengths are unit if we multiply be the following factor
    fac = sqrt(sym(4)/sym(10)); % edge length between any two vertices below
    x0 = [   1   0,                    0,                    0                  ]*fac;
    x1 = [-1/4,  sqrt(sym(15))/4,      0,                    0                  ]*fac;
    x2 = [-1/4, -sqrt(sym(5)/sym(48)), sqrt(sym(5)/sym(6)),  0                  ]*fac;
    x3 = [-1/4,- sqrt(sym(5)/sym(48)),-sqrt(sym(5)/sym(24)), sqrt(sym(5)/sym(8))]*fac;
    x4 = [-1/4,- sqrt(sym(5)/sym(48)),-sqrt(sym(5)/sym(24)),-sqrt(sym(5)/sym(8))]*fac;
    X0 = [x0;x1;x2;x3;x4];
end

end
