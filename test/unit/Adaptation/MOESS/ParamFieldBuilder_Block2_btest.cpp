// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// ParamFieldBuilder_Block2_btest
// testing of the ParamFieldBuilder_Block2 class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Adaptation/MOESS/ParamFieldBuilder_Block2.h"

#include "Field/Local/XField_LocalPatch.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ParamFieldBuilder_Block2_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ParamFieldBuilder_Block2_None_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  int ii = 3;
  int jj = 3;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  typedef Field_DG_Cell<PhysD2, TopoD2, Real> QFieldType0;
  typedef Field_DG_Cell<PhysD2, TopoD2, Real> QFieldType1;

  QFieldType0 qfld(xfld, 1, BasisFunctionCategory_Legendre);
  QFieldType1 sfld(xfld, 0, BasisFunctionCategory_Legendre);

  typedef ParamFieldBuilder_Block2<ParamType_None, PhysD2, TopoD2, QFieldType0, QFieldType1> ParamFieldBuilder;

  ParamFieldBuilder builder(xfld, qfld, sfld);

  BOOST_CHECK_EQUAL( &builder.fld0, &xfld);
  BOOST_CHECK_EQUAL( &builder.fld1, &xfld);

  //Create local param field

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh
  const int main_group = 0;
  const int main_cell = 1;
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local, connectivity, main_group, main_cell);

  int split_edge_index = 0;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_construct,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  QFieldType0 qfld_local(xfld_split_local, 1, BasisFunctionCategory_Legendre);
  QFieldType1 sfld_local(xfld_split_local, 0, BasisFunctionCategory_Legendre);

  ParamFieldBuilder builder_local(xfld_split_local, builder, qfld_local, sfld_local);

  BOOST_CHECK_EQUAL( &builder_local.fld0.getXField(), &xfld_split_local);
  BOOST_CHECK_EQUAL( &builder_local.fld1.getXField(), &xfld_split_local);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ParamFieldBuilder_Block2_ArtificialViscosity_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  int ii = 3;
  int jj = 3;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  typedef Field_DG_Cell<PhysD2, TopoD2, Real> QFieldType0;
  typedef Field_DG_Cell<PhysD2, TopoD2, Real> QFieldType1;

  QFieldType0 qfld(xfld, 1, BasisFunctionCategory_Legendre);
  QFieldType1 sfld(xfld, 0, BasisFunctionCategory_Legendre);

  typedef ParamFieldBuilder_Block2<ParamType_ArtificialViscosity, PhysD2, TopoD2, QFieldType0, QFieldType1> ParamFieldBuilder;

  ParamFieldBuilder builder(xfld, qfld, sfld);

  BOOST_CHECK_EQUAL( &builder.fld0.getXField(), &xfld);
  BOOST_CHECK_EQUAL( &builder.fld1.getXField(), &xfld);

  BOOST_CHECK_EQUAL( &get<0>(builder.fld0).getXField(), &xfld); //hfld
  BOOST_CHECK_EQUAL( &get<1>(builder.fld0), &sfld);
  BOOST_CHECK_EQUAL( &get<2>(builder.fld0), &xfld);

  BOOST_CHECK_EQUAL( &get<0>(builder.fld1).getXField(), &xfld); //hfld
  BOOST_CHECK_EQUAL( &get<1>(builder.fld1), &qfld);
  BOOST_CHECK_EQUAL( &get<2>(builder.fld1), &xfld);

  //Create local param field

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh
  const int main_group = 0;
  const int main_cell = 1;
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local, connectivity, main_group, main_cell);

  int split_edge_index = 0;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_construct,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  QFieldType0 qfld_local(xfld_split_local, 1, BasisFunctionCategory_Legendre);
  QFieldType1 sfld_local(xfld_split_local, 0, BasisFunctionCategory_Legendre);

  ParamFieldBuilder builder_local(xfld_split_local, builder, qfld_local, sfld_local);

  BOOST_CHECK_EQUAL( &builder_local.fld0.getXField(), &xfld_split_local);
  BOOST_CHECK_EQUAL( &builder_local.fld1.getXField(), &xfld_split_local);

  BOOST_CHECK_EQUAL( &get<0>(builder_local.fld0).getXField(), &xfld_split_local); //hfld
  BOOST_CHECK_EQUAL( &get<1>(builder_local.fld0), &sfld_local);
  BOOST_CHECK_EQUAL( &get<2>(builder_local.fld0), &xfld_split_local);

  BOOST_CHECK_EQUAL( &get<0>(builder_local.fld1).getXField(), &xfld_split_local); //hfld
  BOOST_CHECK_EQUAL( &get<1>(builder_local.fld1), &qfld_local);
  BOOST_CHECK_EQUAL( &get<2>(builder_local.fld1), &xfld_split_local);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
