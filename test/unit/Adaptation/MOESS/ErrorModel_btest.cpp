// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// ErrorModel_btest
// testing of the NodalMetrics class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>
#include <random>

// for the old synthesize edge function
#include <unordered_map>
#include <unordered_set>
#include <boost/functional/hash.hpp>

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_SVD.h"

#include "Adaptation/MOESS/ReferenceStepMatrix.h"
#include "Adaptation/MOESS/LocalSplitConfig.h"
#include "Adaptation/MOESS/ErrorModel.h"

#include "Field/Element/ElementXFieldJacobianEquilateral.h"

#include "Field/Local/XField_LocalPatch.h"

#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldSpacetime_CG_Cell.h"

#include "Field/FieldArea_DG_Cell.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField2D_Box_Quad_X1.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"
#include "unit/UnitGrids/XField_KuhnFreudenthal.h"

#include "Meshing/EPIC/XField_PX.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
  typedef ErrorModel<PhysD1, TopoD1> ErrorModel1D;
  typedef ErrorModel<PhysD2, TopoD2> ErrorModel2D;
  typedef ErrorModel<PhysD3, TopoD3> ErrorModel3D;
}

using namespace SANS;

namespace  //no-name namespace local to this file
{

template <class PhysDim, class TopoDim>
class DummyProblem : public SolverInterfaceBase<PhysDim, TopoDim>
{
public:

  explicit DummyProblem(const XField<PhysDim, TopoDim>& xfld, const int order) : order_(order), xfld_(xfld) {}

  virtual ~DummyProblem() {}

  virtual void solveGlobalPrimalProblem() override {}
  virtual void solveGlobalAdjointProblem() override {}
  virtual void computeErrorEstimates() override {}

  virtual Real getElementalErrorEstimate(int cellgroup, int elem) const override { return 1.0; }
  virtual Real getGlobalErrorEstimate() const override { return 1.0; }
  virtual Real getGlobalErrorIndicator() const override { return 1.0; }

  virtual LocalSolveStatus solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& local_xfld, std::vector<Real>& local_error) const override
  {
    std::pair<int,int> cellmap = local_xfld.getGlobalCellMap({0, 0});
    int global_cellgrp  = cellmap.first;
    int global_cellelem = cellmap.second;

    int cellID = xfld_.cellIDs(global_cellgrp)[global_cellelem];

    // make sure the error varies "randomly" throughout the grid so parallel test is more rigorous
    local_error[0] = 0.5 + cellID;

    return LocalSolveStatus(true, 1);
  }

  virtual Real getnDOFperCell(const int cellgroup) const override
  {
    int nDOF = 0;

         if (PhysDim::D == 1)
      nDOF = order_ + 1;
    else if (PhysDim::D == 2)
      nDOF = (order_ + 1)*(order_ + 2)/2;
    else if (PhysDim::D == 3)
      nDOF = (order_ + 1)*(order_ + 2)*(order_ + 3)/6;

    return nDOF;
  }

  virtual int getSolutionOrder(const int cellgroup) const override { return order_; }

  virtual Real getOutput() const override { return 0; }
  virtual void output_EField(const std::string& filename ) const override {}

  virtual int getCost() const override {return 0;}

  SpaceType spaceType() const override {return SpaceType::Discontinuous; }

protected:
  const int order_;
  const XField<PhysDim, TopoDim>& xfld_;
};

// Edge Problems need access to a pointer to a P1 efld
template <class PhysDim, class TopoDim>
class DebugEdgeProblem : public SolverInterfaceBase<PhysDim, TopoDim>
{
public:

  explicit DebugEdgeProblem(const XField<PhysDim, TopoDim>& xfld, const int order) : order_(order), xfld_(xfld),
  ifld_dum(xfld,1,BasisFunctionCategory_Lagrange)
  {
    ifld_dum = 1;
    for (int dof = 0; dof < ifld_dum.nDOFpossessed(); dof++)
    {
      int nativeDOF = ifld_dum.local2nativeDOFmap(dof);
      ifld_dum.DOF(dof) = fabs(1*(-nativeDOF+1%3)-1); // junk function
    }
  }

  virtual ~DebugEdgeProblem() {}

  virtual void solveGlobalPrimalProblem() override {}
  virtual void solveGlobalAdjointProblem() override {}
  virtual void computeErrorEstimates() override {}

  virtual Real getElementalErrorEstimate(int cellgroup, int elem) const override { BOOST_CHECK(cellgroup ==0); return ifld_dum.DOF(elem); }
  virtual Real getGlobalErrorEstimate() const override { return 1.0; }
  virtual Real getGlobalErrorIndicator() const override { return 1.0; }

  virtual LocalSolveStatus solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& local_xfld, std::vector<Real>& local_error) const override
  {
    std::vector<int> commonNodes, localCommonNodes;

    typedef typename Simplex<TopoDim>::type Topology; // Local Split Patch assumes purely simplicial mesh

    if (local_xfld.derivedTypeID() == typeid(XField_LocalPatch<PhysDim,Topology>))
    {
      // get the pointer
      const XField_LocalPatch<PhysDim,Topology>* plocal_xfld = static_cast<const XField_LocalPatch<PhysDim,Topology>*>(&local_xfld);

      // check that it is being run in edge mode
      BOOST_REQUIRE( plocal_xfld->isEdgeMode() );

      commonNodes = plocal_xfld->getLinearCommonNodes(); // nodes on processor (GLOBAL numbering)
      localCommonNodes = plocal_xfld->getLocalLinearCommonNodes(); // nodes on patch
    }
    else
      BOOST_REQUIRE_MESSAGE( 1 == 0, "Shouldn't be possible!");

    local_error.resize(commonNodes.size());
    // In edge solve, the only guarantees from parallel to serial are the native numbering for the first two nodes.
    int nativeDOF0 = xfld_.local2nativeDOFmap(commonNodes[0]);
    int nativeDOF1 = xfld_.local2nativeDOFmap(commonNodes[1]);
    int CantorNumber = (nativeDOF0 + nativeDOF1)*(nativeDOF0+nativeDOF1+1)/2 + nativeDOF1;

    // 0.5 + Cantor's pairing function (maps NxN \to N uniquely )
    for (std::size_t i = 0; i < local_error.size(); i++)
      local_error[i] = 0.5 + CantorNumber;

    return LocalSolveStatus(true, 1);
  }

  virtual Real getnDOFperCell(const int cellgroup) const override
  {
    int nDOF = 0;

         if (PhysDim::D == 1)
      nDOF = order_ + 1;
    else if (PhysDim::D == 2)
      nDOF = (order_ + 1)*(order_ + 2)/2;
    else if (PhysDim::D == 3)
      nDOF = (order_ + 1)*(order_ + 2)*(order_ + 3)/6;

    return nDOF;
  }

  virtual int getSolutionOrder(const int cellgroup) const override { return order_; }

  virtual Real getOutput() const override { return 0; }
  virtual void output_EField(const std::string& filename ) const override {}
  virtual const Field<PhysDim,TopoDim,Real>* pifld() const override { return &ifld_dum; };

  virtual int getCost() const override {return 0;}

  SpaceType spaceType() const override {return SpaceType::Continuous; }

protected:
  const int order_;
  const XField<PhysDim, TopoDim>& xfld_;
  Field_CG_Cell<PhysDim,TopoDim,Real> ifld_dum;
};

template <class PhysDim, class TopoDim>
class DebugEdgeErrorModel : public ErrorModel<PhysDim,TopoDim>
{
public:
  typedef ErrorModel<PhysDim,TopoDim> BaseType;
  DebugEdgeErrorModel(const XField<PhysDim,TopoDim>& xfld_linear,
             const std::vector<int>& cellgroup_list,
             const SolverInterfaceBase<PhysDim,TopoDim>& problem,
             const PyDict& paramsDict) :
   BaseType( xfld_linear, cellgroup_list, problem, paramsDict ) {}

   using BaseType::errorModels_;
   using BaseType::xfld_linear_;
   using BaseType::LocalSolve_;
   using BaseType::cellgroup_list_;
   using BaseType::problem_;
   using BaseType::D;
   using BaseType::uniform_refinement_;

   typedef typename BaseType::ParamsType ParamsType;
   typedef typename BaseType::Matrix Matrix;
   typedef typename BaseType::MatrixSym MatrixSym;

   struct ErrorStepPair
   {
     // typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

     ErrorStepPair() {} // Not initializing data
     ErrorStepPair(const Real& error, const MatrixSym& step) : error(error), step(step) {}

     Real error;
     MatrixSym step;

     // for debugging
     void dump() const { std::cout << "error = " << error << std::endl << "step = " << std::endl << step; }
   };

   //===========================================================================//
   void synthesize_debug(const XField<PhysDim, TopoDim>& xfld_curved)
   {
     errorModels_.resize(xfld_linear_.nCellGroups());

     const XField_CellToTrace<PhysDim, TopoDim> xfld_connectivity(xfld_curved); //build connectivity of curved mesh, as required for local solves

     std::vector<Real> time_breakdown(8, 0.0); //stores the times taken for each part of the synthesis process

     BOOST_REQUIRE( LocalSolve_ == ParamsType::params.LocalSolve.Edge );

     // use the old school synthesize edge
     Field_NodalView nodalView(xfld_linear_, cellgroup_list_);

     synthesize_edge_debug(xfld_connectivity,time_breakdown,nodalView);
   }

   // the original working serial edge solve - use to make comparisons to test against
   void synthesize_edge_debug( const XField_CellToTrace<PhysDim, TopoDim>& xfld_connectivity,
                               std::vector<Real>& time_breakdown,
                               const Field_NodalView& nodalView )
   {
     // get the rank of the current processor
     const int comm_rank = xfld_linear_.comm()->rank();
     mpi::communicator comm_local = xfld_linear_.comm()->split(comm_rank);

     typedef typename Simplex<TopoDim>::type Topology; // MOESS assumes purely simplicial meshes

     //Cell Group Types
     typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
     typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

     // Linear element for calculating Jacobians
     ElementXFieldClass xfldElem( xfld_linear_.template getCellGroupGlobal<Topology>(0).basis() );

     // Assuming constant polynomial order solutions
     const int order = problem_.getSolutionOrder(0); //Get solution order
     BOOST_REQUIRE( order >= 0 );

     // -------------------------------------------------------
     // Constructing the edge_list
     // -------------------------------------------------------
     const int nEdge = Topology::NEdge; // Number of different edges of simplex

     typedef typename std::vector<std::array<int,Line::NNode>> EdgeList;
     EdgeList edge_list;

     const int (*EdgeNodes)[Line::NNode] = ElementEdges<Topology>::EdgeNodes;
     std::array<int,Topology::NNode> nodeMap;

     for ( int group = 0; group < xfld_linear_.nCellGroups(); group++ )
     {
       const XFieldCellGroupType& xfldCellGroup = xfld_linear_.template getCellGroupGlobal<Topology>(group);
       const int nElem = xfldCellGroup.nElem();
       for ( int elem = 0; elem < nElem; elem++ )
       {
         xfldCellGroup.associativity(elem).getNodeGlobalMapping( nodeMap.data(), nodeMap.size() );

         for ( int edge = 0; edge < nEdge; edge++ )
         {
           // loop over the edges
           std::array<int,Line::NNode> newEdge;
           newEdge[0] = std::min(nodeMap[EdgeNodes[edge][0]],nodeMap[EdgeNodes[edge][1]]);
           newEdge[1] = std::max(nodeMap[EdgeNodes[edge][0]],nodeMap[EdgeNodes[edge][1]]);

           edge_list.push_back(newEdge);
         }
       }
     }

     {
       // unordered_set is likely quicker for unique-ing according to StackOverflow discussions
       // every edge is duplicated approx x2 for 2D, x5 for 3D, x20 for 4D
       // Probably over-kill, but I was learning a lot about how these things work -- Hugh
       std::unordered_set<std::array<int,Line::NNode>,boost::hash<std::array<int,Line::NNode>>> uSetEdges;
       uSetEdges.reserve(edge_list.size());

       for (auto const& edge : edge_list)
         uSetEdges.insert(edge);
       edge_list.assign( uSetEdges.begin(), uSetEdges.end() );

       edge_list.shrink_to_fit(); // free up the memory that was taken by all the duplicates.
     }

     // -------------------------------------------------------------------- //
     //            Collect the Element Geometry Data
     // -------------------------------------------------------------------- //

     // unordered_map because we need unique, but we don't actually care about ordered
     // just need the key to find the right value. unordered_map uses a hash function
     // and we need only generate the hash table once because we know the number of entries in advance!

     int nElem = 0; // This will be used later to reserve additional maps
     for (int group = 0; group < xfld_linear_.nCellGroups(); group++)
       nElem += xfld_linear_.getCellGroupBaseGlobal(group).nElem();

     std::unordered_map<GroupElemIndex,Matrix,boost::hash<GroupElemIndex>> UVT_map;
     UVT_map.reserve(nElem); // reserve enough space in the hash table for all the elements

     Matrix J, Jref; // Jacobians for element, reference element and regular to right angle reference
     Matrix U, VT; // matrices for storing left and right singular vectors
     DLA::VectorS<D,Real> sigma; // for storing the singular values
     const Matrix& invJeq = JacobianEquilateralTransform<PhysDim,TopoDim,Topology>::invJeq;

     // The keys here are global group and elem
     for (int group = 0; group < xfld_linear_.nCellGroups(); group++)
     {
       // all MOESS cell groups have the same Topology
       const XFieldCellGroupType& xfldCellGroup = xfld_linear_.template getCellGroupGlobal<Topology>(group);
       for (int elem = 0; elem < xfldCellGroup.nElem(); elem++)
       {
         xfldCellGroup.getElement( xfldElem, elem );
         xfldElem.jacobian(Jref);

         J = Jref*invJeq;

         SVD( J, U, sigma, VT );

         // The hash table is already reserved so this is a quick insertion
         UVT_map[GroupElemIndex(group,elem)] = U*VT; // place into the map at the relevant location
       }
     }
     // UVT_vec[GroupElemIndex] now contains the relevant rotation matrices for the rotations

     // -------------------------------------------------------
     // Operating on the edge_list
     // -------------------------------------------------------
     typedef typename Field_NodalView::IndexVector IndexVector;

     // struct for storing data for an error model: an initial error and a map from edges to (error,Step) pairs.
     struct ErrorModelData
     {
       ErrorModelData() : error0(), edgeMap() {}
       ErrorModelData(Real error0) : error0(error0), edgeMap() {}
       Real error0; // initial error
       // map from edges (in global coord) to new vertex error
       std::map< std::array<int,2>, ErrorStepPair > edgeMap; // needs to be a map so can create by keying in
     };

     std::vector<ErrorModelData> modelVector; // just index by dofs. Don't need to use maps
     modelVector.reserve(xfld_linear_.nDOF()); // This only works in serial
     for (int dof = 0; dof < xfld_linear_.nDOF(); dof++)
       modelVector.push_back( ErrorModelData(problem_.getElementalErrorEstimate(0,dof)) );

     BOOST_REQUIRE_MESSAGE(uniform_refinement_ == false, "Uniform Refinement is incompatible with edge split " );

     std::vector<LocalSplitConfig> split_config_list = LocalSplitConfigList<Topology>::get(uniform_refinement_);

     ReferenceStepMatrix<PhysDim, TopoDim> RefStepMatrixList(comm_local, split_config_list); // no isotropic split allowed

     // loops over the elements in edge_list
     std::vector<Real> split_elem_error; // vector for storing error estimates from the re-solve

     IndexVector nodeGroup0, nodeGroup1; // group elem sets attached to node 0 and node 1
     IndexVector attachedCells; // set of cells attached to the edge
     std::vector<int> attachedNodes; // vector of global numbering nodes in and opposite the edge

     // map from local GroupElemIndex pairs to array of sub-cell steps and whether the edge is reversed
     // use unordered_map as we don't need sort, only unique. Also uses the boost hashing function
     // as works with the GroupElemIndex struct.

     std::unordered_map<GroupElemIndex, MatrixSym, boost::hash<GroupElemIndex> > stepmatrix_map;
     // --------------------------------------------------------------------------//
     //                                EDGE LOOP
     // --------------------------------------------------------------------------//
     for ( const std::array<int,2>& edge : edge_list)
     {
       XField_LocalPatch<PhysDim,Topology> xfld_local( comm_local, xfld_connectivity, edge, problem_.spaceType(), &nodalView );

       // nodes that make up the edge, and the first two are node 0 and node 1 from the edge
       // The attached nodes came from when the local grid was generated
       attachedNodes = xfld_local.getLinearCommonNodes(); // These are in GLOBAL numbering
       split_elem_error.resize(attachedNodes.size()); // These are vertex errors, the first two share the new dof

       LocalSolveStatus status = problem_.solveLocalProblem(xfld_local, split_elem_error);

       // find the intersection of the two sets
       // these are group and cell from the global grid
       nodeGroup0 = nodalView.getCellList( edge[0] );
       nodeGroup1 = nodalView.getCellList( edge[1] );
       attachedCells.clear();
       std::set_intersection( nodeGroup0.begin(), nodeGroup0.end(), nodeGroup1.begin(), nodeGroup1.end(),
                              std::back_inserter( attachedCells ) );

       // Loop over the attached elements and extract the reference sub-cell steps
       // Also store whether the edge is split relative to the canonical edge.
       // This is necessary to ensure the correct step is used for vertex 0 and 1
       // This map is based on GLOBAL CELL ELEMENT pairs
       stepmatrix_map.clear(); // clear but don't de-allocate memory
       stepmatrix_map.reserve(attachedCells.size()); // reserve more if necessary

       for ( auto const& cell : attachedCells ) // cell and element from the global grid
       {
         // Nodes from the global grid
         const XFieldCellGroupType& xfldCellGroup = xfld_linear_.template getCellGroupGlobal<Topology>(cell.group);
         xfldCellGroup.associativity(cell.elem).getNodeGlobalMapping(nodeMap.data(), nodeMap.size());

         std::array<int,2> local_edge; // these are global coordinates. edge is also.

         for (int canon_edge = 0; canon_edge < Topology::NEdge; canon_edge++)
         {
           // find the canonical Edge for the current edge
           local_edge[0] = nodeMap[EdgeNodes[canon_edge][0]];
           local_edge[1] = nodeMap[EdgeNodes[canon_edge][1]];

           const bool firstNode0 = (local_edge[0] == edge[0]);
           const bool secondNode0 = (local_edge[1] == edge[0]);
           const bool firstNode1 = (local_edge[0] == edge[1]);
           const bool secondNode1 = (local_edge[1] == edge[1]);
           const bool containsNode0 = ( firstNode0 || secondNode0 );
           const bool containsNode1 = ( firstNode1 || secondNode1 );

           if ( containsNode0 && containsNode1 )
           {
             // compute the edge step and rotate it appropriately
             const MatrixSym Selem = RefStepMatrixList.getStepMatrix(canon_edge);

             stepmatrix_map[cell] = Metric::rotateRefStepMatrix( Selem, UVT_map[cell] );
             break; // found the canonical edge, don't need to look at the rest
           }
         }
       }

       // Need to figure out which canonical edge the nodes make up for each attached cell
       // and then insert the error into the mapped set corresponding
       BOOST_REQUIRE( split_elem_error.size() == attachedNodes.size() );

       // loop over the computed nodal error estimates.
       // dispatch them, with their correct Step, to the modelVector indexed by the node
       for (std::size_t iN = 0; iN < split_elem_error.size(); iN++)
       {
         const int node = attachedNodes[iN]; // actual node from the global grid
         const Real error = split_elem_error[iN]; // indexed using iN, but correspond to attachedNodes

         IndexVector nodeAttachedCells = nodalView.getCellList(node);

         // get the relevant sub-cell steps - averaging if edge is opposite
         MatrixSym S = 0;
         // int nStepsToAverage = 0;
         for ( auto const& cell : attachedCells ) // all of the cells attached to the edge
         {
           // is this cell attached to the node we're currently looking at?
           const bool attachedToNode = std::find(nodeAttachedCells.begin(),nodeAttachedCells.end(), cell) != nodeAttachedCells.end();

           if ( !attachedToNode ) continue; // skip a cell whose step doesn't contribute to this node

           S += stepmatrix_map[cell];
         }
         // This step is now the average of all those involved in the split.
         // To get the Log-Euclidean vertex interpolation, need to take the mean
         S /= nodeAttachedCells.size();

         // insert the error, step pair into the map for the node.
         // checking that this edge has not been computed already
         ErrorStepPair errorPair(error,S);

         auto map_ret = modelVector[node].edgeMap.insert(std::pair<std::array<int,2>,ErrorStepPair>(edge,errorPair));
         BOOST_CHECK_MESSAGE( map_ret.second == true, "Edge split data was already present, this shouldn't be possible" );
       }
     }

     // -------------------------------------------------------
     // Constructing the nodal error models
     // -------------------------------------------------------

     // resize and reserve
     errorModels_.resize(1); // Monolithic nodal group - Treat all nodes at the same time.
     errorModels_[0].reserve( xfld_linear_.nDOF() ); // number of P1 nodes in grid

     // Need to loop over the vector and convert the maps into vectors to pass to ErrorModel_Local
     std::vector<Real> error1_vec;
     std::vector<MatrixSym> stepmatrix_vec;
     for (int node = 0; node < xfld_linear_.nDOF(); node++)
     {
       // extract the vectors from the maps
       error1_vec.clear();
       error1_vec.reserve(modelVector[node].edgeMap.size());
       stepmatrix_vec.clear();
       stepmatrix_vec.reserve(modelVector[node].edgeMap.size());

       for (auto const& keyVal: modelVector[node].edgeMap ) // key through the map
       {
         auto const& val = keyVal.second;
         stepmatrix_vec.push_back(val.step);
         error1_vec.push_back(val.error);
       }
       errorModels_[0].push_back(ErrorModel_Local<PhysDim>(error1_vec,stepmatrix_vec,modelVector[node].error0,order));
     }
   }
};

#define SANS_CHECK_PING_ORDER( err_vec, delta_vec, small_tol ) \
    if (err_vec[1] > small_tol){ \
      Real rate = log(err_vec[1]/err_vec[0])/log(delta_vec[1]/delta_vec[0]); \
      BOOST_CHECK_MESSAGE( rate >= 1.8 && rate <= 3.7, "Rate check failed: rate = " << rate << \
                           ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" ); \
    }

} // namespace

//############################################################################//
BOOST_AUTO_TEST_SUITE( MOESS_test_suite )

template <class PhysDim, class TopoDim, class Topology>
void pingErrorModel(const XField<PhysDim,TopoDim>& xfld)
{
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  const Real small_tol = 1e-10;

  std::vector<int> cellGroups = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(xfld, order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.None;
  MOESSParams::checkInputs(paramsDict);

  ErrorModel<PhysDim, TopoDim> errorModel(xfld, cellGroups, dummyProblem, paramsDict);
  errorModel.synthesize(xfld);

  //Step matrices at each node
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> Sfld(xfld, 1, BasisFunctionCategory_Lagrange, cellGroups);

  // initialize a uniform distribution between -2*log(2) and 2*log(2)
  std::mt19937_64 rng;
  std::uniform_real_distribution<double> unif(-2*log(2), 2*log(2));

  // initialize the serial vector to "random" numbers
  const int nNode = Sfld.nDOF();
  for ( int node = 0; node < nNode; node++ )
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      Sfld.DOF(node).value(ind) = unif(rng);

  // Perturbed step matrix fields
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> Sfld_p(Sfld, FieldCopy());
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> Sfld_m(Sfld, FieldCopy());

  Real Error0;
  std::vector<MatrixSym> dError_dS0(nNode, 0.0);

  errorModel.computeError(Sfld, Error0, dError_dS0);

  std::vector<MatrixSym> dError_dS_dummy;
  Real delta[2] = {1e-2, 1e-3};
  Real err_approx[2];

  for (int node = 0; node < nNode; node++)
  {
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
    {
      for (int j = 0; j < 2; j++)
      {
        // perturb
        Sfld_p.DOF(node).value(ind) += delta[j];
        Sfld_m.DOF(node).value(ind) -= delta[j];

        Real Errorp, Errorm;
        errorModel.computeError(Sfld_p, Errorp, dError_dS_dummy);
        errorModel.computeError(Sfld_m, Errorm, dError_dS_dummy);

        // de-perturb
        Sfld_p.DOF(node).value(ind) -= delta[j];
        Sfld_m.DOF(node).value(ind) += delta[j];

        Real grad_approx = (Errorp - Errorm)/(2.0*delta[j]);
        err_approx[j] = fabs(grad_approx - dError_dS0[node].value(ind));
      }

      SANS_CHECK_PING_ORDER(err_approx, delta, small_tol);
    }
  }
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingErrorModel_1D_Line )
{
  XField1D xfld(4);
  pingErrorModel<PhysD1, TopoD1, Line>(xfld);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingErrorModel_2D_4Triangle )
{
  XField2D_4Triangle_X1_1Group xfld;
  pingErrorModel<PhysD2, TopoD2, Triangle>(xfld);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingErrorModel_2D_UnionJack_Triangle )
{
  XField2D_Box_UnionJack_Triangle_X1 xfld(2,2);
  pingErrorModel<PhysD2, TopoD2, Triangle>(xfld);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingErrorModel_3D_6Tet )
{
  XField3D_6Tet_X1_1Group xfld;
  pingErrorModel<PhysD3, TopoD3, Tet>(xfld);
}
#endif

#if defined(SANS_AVRO) && defined(DLA_LAPACK)
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingErrorModel_4D )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());
  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld(comm,{2,2,2,2});
  pingErrorModel<PhysD4, TopoD4, Pentatope>(xfld);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Serialize_ErrorModel_2D_Triangle )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;
  typedef DLA::VectorS<MatrixSym::SIZE, Real> ArrayMatSym; //Data-type to represent symmetric matrix as vector

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-6;

  // parallel and serial communicator
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  int ii = 3, jj = 4;
  XField2D_Box_Triangle_Lagrange_X1 xfld_parallel(world, ii, jj);
  XField2D_Box_Triangle_Lagrange_X1 xfld_serial(comm, ii, jj);

  typedef XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef XFieldCellGroupType::ElementType<> ElementXFieldClass;

  const XFieldCellGroupType& xfldCellGroup = xfld_parallel.getCellGroup<Triangle>(0);
  ElementXFieldClass xfldElem(xfldCellGroup.basis() );
  xfldCellGroup.getElement(xfldElem,0);

  // Implied metric of one element
  MatrixSym M0;
  xfldElem.impliedMetric(M0);

  std::vector<int> cellGroups = {0};

  int order = 1;

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  MOESSParams::checkInputs(paramsDict);

  //--------
  // Construct a parallel setup and serialize it
  DummyProblem<PhysDim,TopoDim> dummyProblem_parallel(xfld_parallel, order);

  // construct the error model and synthesize in parallel
  ErrorModel<PhysDim, TopoDim> errorModel_parallel(xfld_parallel, cellGroups, dummyProblem_parallel, paramsDict);
  errorModel_parallel.synthesize(xfld_parallel);

  // create a serialize grid on processor 0
  XField<PhysDim,TopoDim> xfld_rank0(xfld_parallel, XFieldBalance::Serial);

  // construct the error model on rank0 and serialize
  ErrorModel<PhysDim, TopoDim> errorModel_serialize(xfld_rank0, cellGroups, dummyProblem_parallel, paramsDict);
  errorModel_serialize.serialize(errorModel_parallel);
  //--------

  //--------
  // Construct a serial problem
  DummyProblem<PhysDim,TopoDim> dummyProblem_serial(xfld_serial, order);

  // construct the serial error model and synthesize as the truth reference
  ErrorModel<PhysDim, TopoDim> errorModel_serial(xfld_serial, cellGroups, dummyProblem_serial, paramsDict);
  errorModel_serial.synthesize(xfld_serial);
  //--------

  Field_DG_Cell<PhysDim,TopoDim,ArrayMatSym> rate_fld_serialize(xfld_rank0, 0, BasisFunctionCategory_Legendre, cellGroups);
  Field_DG_Cell<PhysDim,TopoDim,ArrayMatSym> rate_fld_serial(xfld_serial, 0, BasisFunctionCategory_Legendre, cellGroups);

  if (world.rank() == 0)
  {
    //Get rate field for the serialized mesh
    errorModel_serialize.getRateMatrixField(rate_fld_serialize);

    //Get rate field for the global mesh
    errorModel_serial.getRateMatrixField(rate_fld_serial);

    BOOST_REQUIRE_EQUAL(rate_fld_serial.nDOF(), rate_fld_serialize.nDOF());

    // check that the parallelized metrics match the global metrics (full mesh)
    for (int node = 0; node < rate_fld_serial.nDOF(); node++)
      for (int i = 0; i < MatrixSym::SIZE; i++)
        SANS_CHECK_CLOSE( rate_fld_serial.DOF(node)[i], rate_fld_serialize.DOF(node)[i], small_tol, close_tol );
  }
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorModel_Element_Serial_Parallel_Equivalency )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;
  typedef DLA::VectorS<MatrixSym::SIZE, Real> ArrayMatSym; //Data-type to represent symmetric matrix as vector
  typedef Field_CG_Cell<PhysDim,TopoDim,MatrixSym> MatrixSymFieldType;

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-10;

  // parallel and serial communicator
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  // use a non-uniform mesh for better testing
  std::vector<Real> xvec = {0, 0.25, 0.75, 1};
  std::vector<Real> yvec = {0, 0.25, 0.75, 1};

  XField2D_Box_Triangle_Lagrange_X1 xfld_parallel( world, xvec, yvec ); // partitioned system
  XField2D_Box_Triangle_Lagrange_X1 xfld_serial( comm, xvec, yvec );  // complete system on all processors

  std::vector<int> cellGroups = {0};

  int order = 1;

  PyDict paramsDict;
  paramsDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Element;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  MOESSParams::checkInputs(paramsDict);

  //--------
  // Construct a parallel error model
  DummyProblem<PhysDim,TopoDim> dummyProblem_parallel(xfld_parallel, order);

  // construct the error model and synthesize in parallel
  ErrorModel<PhysDim, TopoDim> errorModel_parallel(xfld_parallel, cellGroups, dummyProblem_parallel, paramsDict);
  errorModel_parallel.synthesize(xfld_parallel);

  Field_DG_Cell<PhysDim,TopoDim,ArrayMatSym> rate_fld_parallel(xfld_parallel, 0, BasisFunctionCategory_Legendre, cellGroups);

  //Get rate field for the parallel mesh
  errorModel_parallel.getRateMatrixField(rate_fld_parallel);
  //--------

  //--------
  // Construct a serial problem
  DummyProblem<PhysDim,TopoDim> dummyProblem_serial(xfld_serial, order);

  // construct the serial error model and synthesize as the truth reference
  ErrorModel<PhysDim, TopoDim> errorModel_serial(xfld_serial, cellGroups, dummyProblem_serial, paramsDict);
  errorModel_serial.synthesize(xfld_serial);

  Field_DG_Cell<PhysDim,TopoDim,ArrayMatSym> rate_fld_serial(xfld_serial, 0, BasisFunctionCategory_Legendre, cellGroups);

  //Get rate field for the serial global mesh
  errorModel_serial.getRateMatrixField(rate_fld_serial);
  //--------

  // check that the parallelized rates match the global serial rates (full mesh)
  for (int i = 0; i < rate_fld_parallel.nDOF(); i++)
  {
    int i_serial = rate_fld_parallel.local2nativeDOFmap(i);
    for (int j = 0; j < MatrixSym::SIZE; j++)
      SANS_CHECK_CLOSE( rate_fld_serial.DOF(i_serial)[j], rate_fld_parallel.DOF(i)[j], small_tol, close_tol );
  }

  MatrixSymFieldType Sfld_parallel(xfld_parallel, 1, BasisFunctionCategory_Lagrange, cellGroups);
  MatrixSymFieldType Sfld_serial(xfld_serial, 1, BasisFunctionCategory_Lagrange, cellGroups);

  // initialize a uniform distribution between -2*log(2) and 2*log(2)
  std::mt19937_64 rng;
  std::uniform_real_distribution<double> unif(-2*log(2), 2*log(2));

  // initialize the serial step matrix field with random numbers
  for ( int node = 0; node < Sfld_serial.nDOF(); node++ )
    for ( int j = 0; j < MatrixSym::SIZE; j++ )
      Sfld_serial.DOF(node).value(j) = unif(rng);

  // copy over the serial portion to the equivalent parallel vector
  for ( int node = 0; node < Sfld_parallel.nDOF(); node++ )
  {
    int node_serial = Sfld_parallel.local2nativeDOFmap(node);
    for ( int j = 0; j < MatrixSym::SIZE; j++ )
      Sfld_parallel.DOF(node) = Sfld_serial.DOF(node_serial);
  }

  std::vector<MatrixSym> dError_dS_serial(Sfld_serial.nDOF(), 0);
  Real err_serial = 0;
  errorModel_serial.computeError(Sfld_serial, err_serial, dError_dS_serial);

  std::vector<MatrixSym> dError_dS_parallel(Sfld_parallel.nDOF(), 0);
  Real err_parallel = 0;
  errorModel_parallel.computeError(Sfld_parallel, err_parallel, dError_dS_parallel);

  // check that the global error is consistent
  BOOST_CHECK_CLOSE(err_serial, err_parallel, 1e-9);

  for (int rank = 0; rank < world.size(); rank++)
  {
    world.barrier();
    if (rank != world.rank()) continue;

    // check that the gradient is consistent between the serial and parallel calculations
    for ( int node = 0; node < Sfld_parallel.nDOFpossessed(); node++ )
    {
      int node_serial = Sfld_parallel.local2nativeDOFmap(node);
      //std::cout << rank << " is " << Sfld_parallel.local2nativeDOFmap(node) << " ip " << node << std::endl;
      for ( int j = 0; j < MatrixSym::SIZE; j++ )
        SANS_CHECK_CLOSE( dError_dS_serial[node_serial].value(j), dError_dS_parallel[node].value(j), small_tol, close_tol );
    }
  }
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorModel_EdgeElement_Serial_Parallel_Equivalency )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;
  typedef DLA::VectorS<MatrixSym::SIZE, Real> ArrayMatSym; //Data-type to represent symmetric matrix as vector
  typedef Field_CG_Cell<PhysDim,TopoDim,MatrixSym> MatrixSymFieldType;

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-10;

  // parallel and serial communicator
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  // use a non-uniform mesh for better testing
  std::vector<Real> xvec = {0, 0.25, 0.75, 1};
  std::vector<Real> yvec = {0, 0.25, 0.75, 1};

  XField2D_Box_Triangle_Lagrange_X1 xfld_parallel( world, xvec, yvec ); // partitioned system
  XField2D_Box_Triangle_Lagrange_X1 xfld_serial( comm, xvec, yvec );  // complete system on all processors

  std::vector<int> cellGroups = {0};

  int order = 1;

  PyDict paramsDict;
  paramsDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.EdgeElement;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  MOESSParams::checkInputs(paramsDict);

  //--------
  // Construct a parallel error model
  DebugEdgeProblem<PhysDim,TopoDim> dummyProblem_parallel(xfld_parallel, order);

  // construct the error model and synthesize in parallel
  DebugEdgeErrorModel<PhysDim, TopoDim> errorModel_parallel(xfld_parallel, cellGroups, dummyProblem_parallel, paramsDict);
  errorModel_parallel.synthesize(xfld_parallel);

  Field_DG_Cell<PhysDim,TopoDim,ArrayMatSym> rate_fld_parallel(xfld_parallel, 0, BasisFunctionCategory_Legendre, cellGroups);

  //Get rate field for the parallel mesh
  errorModel_parallel.getRateMatrixField(rate_fld_parallel);
  //--------

  //--------
  // Construct a serial problem
  DebugEdgeProblem<PhysDim,TopoDim> dummyProblem_serial(xfld_serial, order);

  // construct the serial error model and synthesize as the truth reference
  DebugEdgeErrorModel<PhysDim, TopoDim> errorModel_serial(xfld_serial, cellGroups, dummyProblem_serial, paramsDict);
  errorModel_serial.synthesize(xfld_serial);

  Field_DG_Cell<PhysDim,TopoDim,ArrayMatSym> rate_fld_serial(xfld_serial, 0, BasisFunctionCategory_Legendre, cellGroups);

  //Get rate field for the serial global mesh
  errorModel_serial.getRateMatrixField(rate_fld_serial);
  //--------

  // check that the parallelized rates match the global serial rates (full mesh)
  for (int i = 0; i < rate_fld_parallel.nDOF(); i++)
  {
    int i_serial = rate_fld_parallel.local2nativeDOFmap(i);
    for (int j = 0; j < MatrixSym::SIZE; j++)
      SANS_CHECK_CLOSE( rate_fld_serial.DOF(i_serial)[j], rate_fld_parallel.DOF(i)[j], small_tol, close_tol );
  }

  MatrixSymFieldType Sfld_parallel(xfld_parallel, 1, BasisFunctionCategory_Lagrange, cellGroups);
  MatrixSymFieldType Sfld_serial(xfld_serial, 1, BasisFunctionCategory_Lagrange, cellGroups);

  // initialize a uniform distribution between -2*log(2) and 2*log(2)
  std::mt19937_64 rng;
  std::uniform_real_distribution<double> unif(-2*log(2), 2*log(2));

  // initialize the serial step matrix field with random numbers
  for ( int node = 0; node < Sfld_serial.nDOF(); node++ )
    for ( int j = 0; j < MatrixSym::SIZE; j++ )
      Sfld_serial.DOF(node).value(j) = unif(rng);

  // copy over the serial portion to the equivalent parallel vector
  for ( int node = 0; node < Sfld_parallel.nDOF(); node++ )
  {
    int node_serial = Sfld_parallel.local2nativeDOFmap(node);
    for ( int j = 0; j < MatrixSym::SIZE; j++ )
      Sfld_parallel.DOF(node) = Sfld_serial.DOF(node_serial);
  }

  std::vector<MatrixSym> dError_dS_serial(Sfld_serial.nDOF(), 0);
  Real err_serial = 0;
  errorModel_serial.computeError(Sfld_serial, err_serial, dError_dS_serial);

  std::vector<MatrixSym> dError_dS_parallel(Sfld_parallel.nDOF(), 0);
  Real err_parallel = 0;
  errorModel_parallel.computeError(Sfld_parallel, err_parallel, dError_dS_parallel);

  // check that the global error is consistent
  BOOST_CHECK_CLOSE(err_serial, err_parallel, 1e-9);

  for (int rank = 0; rank < world.size(); rank++)
  {
    world.barrier();
    if (rank != world.rank()) continue;

    // check that the gradient is consistent between the serial and parallel calculations
    for ( int node = 0; node < Sfld_parallel.nDOFpossessed(); node++ )
    {
      int node_serial = Sfld_parallel.local2nativeDOFmap(node);
      //std::cout << rank << " is " << Sfld_parallel.local2nativeDOFmap(node) << " ip " << node << std::endl;
      for ( int j = 0; j < MatrixSym::SIZE; j++ )
        SANS_CHECK_CLOSE( dError_dS_serial[node_serial].value(j), dError_dS_parallel[node].value(j), small_tol, close_tol );
    }
  }
}
#endif

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class Topology>
void pingEdgeErrorModel(const XField<PhysDim,TopoDim>& xfld, const Real small_tol)
{
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  std::vector<int> cellGroups = {0};

  int order = 1;
  DebugEdgeProblem<PhysDim,TopoDim> dummyProblem(xfld, order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Edge;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.None;
  MOESSParams::checkInputs(paramsDict);

  ErrorModel<PhysDim, TopoDim> errorModel(xfld, cellGroups, dummyProblem, paramsDict);
  errorModel.synthesize(xfld);

  //Step matrices at each node
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> Sfld(xfld, 1, BasisFunctionCategory_Lagrange, cellGroups);

  // initialize a uniform distribution between -2*log(2) and 2*log(2)
  std::mt19937_64 rng;
  std::uniform_real_distribution<double> unif(-2*log(2), 2*log(2));

  // initialize the serial vector to "random" numbers
  const int nNode = Sfld.nDOFpossessed();
  for ( int node = 0; node < nNode; node++ )
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      Sfld.DOF(node).value(ind) = unif(rng);

  // Perturbed step matrix fields
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> Sfld_p(Sfld, FieldCopy());
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> Sfld_m(Sfld, FieldCopy());

  Real Error0;
  std::vector<MatrixSym> dError_dS0(nNode, 0.0);

  errorModel.computeError(Sfld, Error0, dError_dS0);

  std::vector<MatrixSym> dError_dS_dummy;
  Real delta[2] = {1e-1, 1e-2};
  Real err_approx[2];

  for (int node = 0; node < nNode; node++)
  {
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
    {
      for (int j = 0; j < 2; j++)
      {
        // perturb
        Sfld_p.DOF(node).value(ind) += delta[j];
        Sfld_m.DOF(node).value(ind) -= delta[j];

        Real Errorp, Errorm;
        errorModel.computeError(Sfld_p, Errorp, dError_dS_dummy);
        errorModel.computeError(Sfld_m, Errorm, dError_dS_dummy);

        // de-perturb
        Sfld_p.DOF(node).value(ind) -= delta[j];
        Sfld_m.DOF(node).value(ind) += delta[j];

        Real grad_approx = (Errorp - Errorm)/(2.0*delta[j]);
        err_approx[j] = fabs(grad_approx - dError_dS0[node].value(ind));
      }

      SANS_CHECK_PING_ORDER(err_approx, delta, small_tol);
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingEdgeErrorModel_1D_Line )
{
  XField1D xfld(4);
  pingEdgeErrorModel<PhysD1, TopoD1, Line>(xfld, 1e-10);
}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingEdgeErrorModel_2D_4Triangle )
{
  XField2D_4Triangle_X1_1Group xfld;
  pingEdgeErrorModel<PhysD2, TopoD2, Triangle>(xfld, 1e-10);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingEdgeErrorModel_2D_UnionJack_Triangle )
{
  XField2D_Box_UnionJack_Triangle_X1 xfld(2,2);
  pingEdgeErrorModel<PhysD2, TopoD2, Triangle>(xfld, 1e-10);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingEdgeErrorModel_3D_6Tet )
{
  XField3D_6Tet_X1_1Group xfld;
  pingEdgeErrorModel<PhysD3, TopoD3, Tet>(xfld, 1e-10);
}

#if defined(SANS_AVRO) && defined(DLA_LAPACK)
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingEdgeErrorModel_4D )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());
  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld(comm,{2,2,2,2});
  pingEdgeErrorModel<PhysD4, TopoD4, Pentatope>(xfld, 1e-8);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Serialize_EdgeErrorModel_2D_Triangle )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;
  typedef DLA::VectorS<MatrixSym::SIZE, Real> ArrayMatSym; //Data-type to represent symmetric matrix as vector

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-6;

  // parallel and serial communicator
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  int ii = 3, jj = 4;
  XField2D_Box_Triangle_Lagrange_X1 xfld_parallel(world, ii, jj);
  XField2D_Box_Triangle_Lagrange_X1 xfld_serial(comm, ii, jj);

  typedef XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef XFieldCellGroupType::ElementType<> ElementXFieldClass;

  const XFieldCellGroupType& xfldCellGroup = xfld_parallel.getCellGroup<Triangle>(0);
  ElementXFieldClass xfldElem(xfldCellGroup.basis() );
  xfldCellGroup.getElement(xfldElem,0);

  // Implied metric of one element
  MatrixSym M0;
  xfldElem.impliedMetric(M0);

  std::vector<int> cellGroups = {0};

  int order = 1;

  PyDict paramsDict;
  paramsDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Edge;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  MOESSParams::checkInputs(paramsDict);

  //--------
  // Construct a parallel setup and serialize it
  DebugEdgeProblem<PhysDim,TopoDim> dummyProblem_parallel(xfld_parallel, order);

  // construct the error model and synthesize in parallel
  ErrorModel<PhysDim, TopoDim> errorModel_parallel(xfld_parallel, cellGroups, dummyProblem_parallel, paramsDict);
  errorModel_parallel.synthesize(xfld_parallel);

  // create a serialize grid on processor 0
  XField<PhysDim,TopoDim> xfld_rank0(xfld_parallel, XFieldBalance::Serial);

  // construct the error model on rank0 and serialize
  ErrorModel<PhysDim, TopoDim> errorModel_serialize(xfld_rank0, cellGroups, dummyProblem_parallel, paramsDict);
  errorModel_serialize.serialize(errorModel_parallel);

  //--------
  // Construct a serial problem
  DebugEdgeProblem<PhysDim,TopoDim> dummyProblem_serial(xfld_serial, order);

  // construct the serial error model and synthesize as the truth reference
  ErrorModel<PhysDim, TopoDim> errorModel_serial(xfld_serial, cellGroups, dummyProblem_serial, paramsDict);
  errorModel_serial.synthesize(xfld_serial);
  //--------

  Field_CG_Cell<PhysDim,TopoDim,ArrayMatSym> rate_fld_serialize(xfld_rank0, 1, BasisFunctionCategory_Lagrange, cellGroups);
  Field_CG_Cell<PhysDim,TopoDim,ArrayMatSym> rate_fld_serial(xfld_serial, 1, BasisFunctionCategory_Lagrange, cellGroups);

  if (world.rank() == 0)
  {
    //Get rate field for the serialized mesh
    errorModel_serialize.getRateMatrixField(rate_fld_serialize);

    //Get rate field for the global mesh
    errorModel_serial.getRateMatrixField(rate_fld_serial);

    BOOST_REQUIRE_EQUAL(rate_fld_serial.nDOF(), rate_fld_serialize.nDOF());

    // check that the parallelized metrics match the global metrics (full mesh)
    for (int node = 0; node < rate_fld_serial.nDOF(); node++)
      for (int i = 0; i < MatrixSym::SIZE; i++)
        SANS_CHECK_CLOSE( rate_fld_serial.DOF(node)[i], rate_fld_serialize.DOF(node)[i], small_tol, close_tol );
  }
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeErrorModel_Serial_Parallel_Equivalency )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;
  typedef DLA::VectorS<MatrixSym::SIZE, Real> ArrayMatSym; //Data-type to represent symmetric matrix as vector
  typedef Field_CG_Cell<PhysDim,TopoDim,MatrixSym> MatrixSymFieldType;

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-10;

  // parallel and serial communicator
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  // use a non-uniform mesh for better testing
  std::vector<Real> xvec = {0, 0.25, 0.75, 1};
  std::vector<Real> yvec = {0, 0.25, 0.75, 1};

  XField2D_Box_Triangle_Lagrange_X1 xfld_parallel( world, xvec, yvec ); // partitioned system
  XField2D_Box_Triangle_Lagrange_X1 xfld_serial( comm, xvec, yvec );  // complete system on all processors

  std::vector<int> cellGroups = {0};

  int order = 1;

  PyDict paramsDict;
  paramsDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Edge;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  MOESSParams::checkInputs(paramsDict);

  //--------
  // Construct a parallel error model
  DebugEdgeProblem<PhysDim,TopoDim> dummyProblem_parallel(xfld_parallel, order);

  // construct the error model and synthesize in parallel
  DebugEdgeErrorModel<PhysDim, TopoDim> errorModel_parallel(xfld_parallel, cellGroups, dummyProblem_parallel, paramsDict);
  errorModel_parallel.synthesize(xfld_parallel);

  Field_CG_Cell<PhysDim,TopoDim,ArrayMatSym> rate_fld_parallel(xfld_parallel, 1, BasisFunctionCategory_Lagrange, cellGroups);

  //Get rate field for the parallel mesh
  errorModel_parallel.getRateMatrixField(rate_fld_parallel);
  //--------

  //--------
  // Construct a serial problem
  DebugEdgeProblem<PhysDim,TopoDim> dummyProblem_serial(xfld_serial, order);

  // construct the serial error model and synthesize as the truth reference
  DebugEdgeErrorModel<PhysDim, TopoDim> errorModel_serial(xfld_serial, cellGroups, dummyProblem_serial, paramsDict);
  errorModel_serial.synthesize(xfld_serial);

  Field_CG_Cell<PhysDim,TopoDim,ArrayMatSym> rate_fld_serial(xfld_serial, 1, BasisFunctionCategory_Lagrange, cellGroups);

  //Get rate field for the serial global mesh
  errorModel_serial.getRateMatrixField(rate_fld_serial);
  //--------

  for (int i = 0; i < rate_fld_parallel.nDOFpossessed(); i++)
  {
    int i_serial = rate_fld_parallel.local2nativeDOFmap(i);
    // check that the resulting rate matrices match up for all the dofs
    for (int j = 0; j < MatrixSym::SIZE; j++)
      SANS_CHECK_CLOSE( rate_fld_serial.DOF(i_serial)[j], rate_fld_parallel.DOF(i)[j], small_tol, close_tol );

    // check that the data used to generate the rate matrices are the same
    const std::vector<Real>& error1_list_serial   = errorModel_serial.errorModels_[0][i_serial].error1_list();
    const std::vector<Real>& error1_list_parallel = errorModel_parallel.errorModels_[0][i].error1_list();

    const std::vector<MatrixSym>& step_matrix_list_serial = errorModel_serial.errorModels_[0][i_serial].step_matrix_list();
    const std::vector<MatrixSym>& step_matrix_list_parallel = errorModel_parallel.errorModels_[0][i].step_matrix_list();

    BOOST_CHECK( error1_list_serial.size() > 0 );
    BOOST_CHECK( step_matrix_list_serial.size() > 0 );
    BOOST_CHECK_EQUAL( error1_list_serial.size(), error1_list_parallel.size() );
    BOOST_CHECK_EQUAL( step_matrix_list_serial.size(), step_matrix_list_parallel.size() );
    // for (std::size_t ind = 0; ind < error1_list_serial.size(); ind++)
    // {
    //   std::cout << "rank = " << world.rank() << ": " << error1_list_serial[ind] << ", " <<  error1_list_parallel[ind] << std::endl;
    // }
    for (std::size_t j = 0; j < error1_list_serial.size(); j++)
    {
      SANS_CHECK_CLOSE( error1_list_serial[j], error1_list_parallel[j], small_tol, close_tol );

      // check that the step matrices used to build the models match
      for (int ii = 0; ii < PhysDim::D; ii++)
        for (int jj = ii; jj < PhysDim::D; jj++)
          SANS_CHECK_CLOSE( step_matrix_list_serial[j](ii,jj), step_matrix_list_parallel[j](ii,jj), small_tol, close_tol );
    }
  }

  MatrixSymFieldType Sfld_parallel(xfld_parallel, 1, BasisFunctionCategory_Lagrange, cellGroups);
  MatrixSymFieldType Sfld_serial(xfld_serial, 1, BasisFunctionCategory_Lagrange, cellGroups);

  // initialize a uniform distribution between -2*log(2) and 2*log(2)
  std::mt19937_64 rng;
  std::uniform_real_distribution<double> unif(-2*log(2), 2*log(2));

  // initialize the serial step matrix field with random numbers
  for ( int node = 0; node < Sfld_serial.nDOF(); node++ )
    for ( int j = 0; j < MatrixSym::SIZE; j++ )
      Sfld_serial.DOF(node).value(j) = unif(rng);

  // copy over the serial portion to the equivalent parallel vector
  for ( int node = 0; node < Sfld_parallel.nDOFpossessed(); node++ )
  {
    int node_serial = Sfld_parallel.local2nativeDOFmap(node);
    for ( int j = 0; j < MatrixSym::SIZE; j++ )
      Sfld_parallel.DOF(node) = Sfld_serial.DOF(node_serial);
  }

  std::vector<MatrixSym> dError_dS_serial(Sfld_serial.nDOF(), 0);
  Real err_serial = 0;
  errorModel_serial.computeError(Sfld_serial, err_serial, dError_dS_serial);

  std::vector<MatrixSym> dError_dS_parallel(Sfld_parallel.nDOF(), 0);
  Real err_parallel = 0;
  errorModel_parallel.computeError(Sfld_parallel, err_parallel, dError_dS_parallel);

  // check that the global error is consistent
  BOOST_CHECK_CLOSE(err_serial, err_parallel, 1e-9);

  for (int rank = 0; rank < world.size(); rank++)
  {
    world.barrier();
    if (rank != world.rank()) continue;

    // check that the gradient is consistent between the serial and parallel calculations
    for ( int node = 0; node < Sfld_parallel.nDOFpossessed(); node++ )
    {
      int node_serial = Sfld_parallel.local2nativeDOFmap(node);
      //std::cout << rank << " is " << Sfld_parallel.local2nativeDOFmap(node) << " ip " << node << std::endl;
      for ( int j = 0; j < MatrixSym::SIZE; j++ )
        SANS_CHECK_CLOSE( dError_dS_serial[node_serial].value(j), dError_dS_parallel[node].value(j), small_tol, close_tol );
    }
  }
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeErrorModel_BuildAtNode_vs_BuildFromMap )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;
  typedef DLA::VectorS<MatrixSym::SIZE, Real> ArrayMatSym; //Data-type to represent symmetric matrix as vector

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-10;

  // parallel and serial communicator
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  // use a non-uniform mesh for better testing
  std::vector<Real> xvec = {1, 1.1, 1.45, 1.62, 2};
  std::vector<Real> yvec = {0, 0.55, 0.72, 1};

  // needs to be a serial grid
  XField2D_Box_Triangle_Lagrange_X1 xfld_serial( comm, xvec, yvec );  // complete system on all processors

  std::vector<int> cellGroups = {0};

  int order = 1;

  PyDict paramsDict;
  paramsDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Edge;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  MOESSParams::checkInputs(paramsDict);

  // Construct a serial problem
  DebugEdgeProblem<PhysDim,TopoDim> dummyProblem_serial(xfld_serial, order);

  // construct the serial error model and synthesize as the truth reference
  DebugEdgeErrorModel<PhysDim, TopoDim> errorModel_serial(xfld_serial, cellGroups, dummyProblem_serial, paramsDict);
  errorModel_serial.synthesize(xfld_serial);

  // construct the serial error model directly, without the use of maps
  DebugEdgeErrorModel<PhysDim,TopoDim> errorModel_dummy_serial(xfld_serial, cellGroups, dummyProblem_serial, paramsDict);
  errorModel_dummy_serial.synthesize_debug(xfld_serial);

  Field_CG_Cell<PhysDim,TopoDim,ArrayMatSym> rate_fld_serial(xfld_serial, 1, BasisFunctionCategory_Lagrange, cellGroups);
  Field_CG_Cell<PhysDim,TopoDim,ArrayMatSym> rate_fld_dummy_serial(xfld_serial, 1, BasisFunctionCategory_Lagrange, cellGroups);

  //Get rate fields
  errorModel_serial.getRateMatrixField(rate_fld_serial);
  errorModel_dummy_serial.getRateMatrixField(rate_fld_dummy_serial);

  BOOST_CHECK_EQUAL( rate_fld_dummy_serial.nDOF(), rate_fld_serial.nDOF() );
  BOOST_CHECK_EQUAL( errorModel_serial.errorModels_[0].size(), rate_fld_serial.nDOF() );
  BOOST_CHECK_EQUAL( errorModel_dummy_serial.errorModels_[0].size(), rate_fld_dummy_serial.nDOF() );

  for (int i = 0; i < rate_fld_serial.nDOF(); i++)
  {
    // check that the resulting rate matrices match up for all dofs
    for (int j = 0; j < MatrixSym::SIZE; j++)
      SANS_CHECK_CLOSE( rate_fld_serial.DOF(i)[j], rate_fld_dummy_serial.DOF(i)[j], small_tol, close_tol );

    // check that the data used to generate the rate matrices are the same
    const std::vector<Real>& error1_list_serial = errorModel_serial.errorModels_[0][i].error1_list();
    const std::vector<Real>& error1_list_dummy_serial = errorModel_dummy_serial.errorModels_[0][i].error1_list();

    const std::vector<MatrixSym>& step_matrix_list_serial = errorModel_serial.errorModels_[0][i].step_matrix_list();
    const std::vector<MatrixSym>& step_matrix_list_dummy_serial = errorModel_dummy_serial.errorModels_[0][i].step_matrix_list();

    BOOST_CHECK( error1_list_serial.size() > 0 );
    BOOST_CHECK( step_matrix_list_dummy_serial.size() > 0 );
    BOOST_CHECK_EQUAL( error1_list_serial.size(), error1_list_dummy_serial.size() );
    BOOST_CHECK_EQUAL( step_matrix_list_serial.size(), step_matrix_list_dummy_serial.size() );
    for (std::size_t j = 0; j < error1_list_serial.size(); j++)
    {
      SANS_CHECK_CLOSE( error1_list_serial[j], error1_list_dummy_serial[j], small_tol, close_tol );

      // check that the step matrices used to build the models match
      for (int ii = 0; ii < PhysDim::D; ii++)
        for (int jj = ii; jj < PhysDim::D; jj++)
          SANS_CHECK_CLOSE( step_matrix_list_serial[j](ii,jj), step_matrix_list_dummy_serial[j](ii,jj), small_tol, close_tol );
    }
  }

}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeErrorModel_BuildAtNode_vs_BuildFromMap_UnstructuredGrid )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;
  typedef DLA::VectorS<MatrixSym::SIZE, Real> ArrayMatSym; //Data-type to represent symmetric matrix as vector

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-10;

  // parallel and serial communicator
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  // A grid generated in an AD adapt at one point.
  std::string filename = "IO/Adaptation/XField2D_UnstructuredBox_X1.grm";
  XField_PX<PhysD2,TopoD2> xfld_serial( comm, filename );  // complete system on all processors

  std::vector<int> cellGroups = {0};

  int order = 1;

  PyDict paramsDict;
  paramsDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Edge;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  MOESSParams::checkInputs(paramsDict);

  // Construct a serial problem
  DebugEdgeProblem<PhysDim,TopoDim> dummyProblem_serial(xfld_serial, order);

  // construct the serial error model and synthesize as the truth reference
  DebugEdgeErrorModel<PhysDim, TopoDim> errorModel_serial(xfld_serial, cellGroups, dummyProblem_serial, paramsDict);
  errorModel_serial.synthesize(xfld_serial);

  // construct the serial error model directly, without the use of maps
  DebugEdgeErrorModel<PhysDim,TopoDim> errorModel_dummy_serial(xfld_serial, cellGroups, dummyProblem_serial, paramsDict);
  errorModel_dummy_serial.synthesize_debug(xfld_serial);

  Field_CG_Cell<PhysDim,TopoDim,ArrayMatSym> rate_fld_serial(xfld_serial, 1, BasisFunctionCategory_Lagrange, cellGroups);
  Field_CG_Cell<PhysDim,TopoDim,ArrayMatSym> rate_fld_dummy_serial(xfld_serial, 1, BasisFunctionCategory_Lagrange, cellGroups);

  //Get rate fields
  errorModel_serial.getRateMatrixField(rate_fld_serial);
  errorModel_dummy_serial.getRateMatrixField(rate_fld_dummy_serial);

  BOOST_CHECK( rate_fld_dummy_serial.nDOF() > 0 );
  BOOST_CHECK_EQUAL( rate_fld_dummy_serial.nDOF(), rate_fld_serial.nDOF() );
  BOOST_CHECK_EQUAL( errorModel_serial.errorModels_[0].size(), rate_fld_serial.nDOF() );
  BOOST_CHECK_EQUAL( errorModel_dummy_serial.errorModels_[0].size(), rate_fld_dummy_serial.nDOF() );

  for (int i = 0; i < rate_fld_serial.nDOF(); i++)
  {
    // check that the resulting rate matrices match up for all dofs
    for (int j = 0; j < MatrixSym::SIZE; j++)
      SANS_CHECK_CLOSE( rate_fld_serial.DOF(i)[j], rate_fld_dummy_serial.DOF(i)[j], small_tol, close_tol );

    // check that the data used to generate the rate matrices are the same
    const std::vector<Real>& error1_list_serial = errorModel_serial.errorModels_[0][i].error1_list();
    const std::vector<Real>& error1_list_dummy_serial = errorModel_dummy_serial.errorModels_[0][i].error1_list();

    const std::vector<MatrixSym>& step_matrix_list_serial = errorModel_serial.errorModels_[0][i].step_matrix_list();
    const std::vector<MatrixSym>& step_matrix_list_dummy_serial = errorModel_dummy_serial.errorModels_[0][i].step_matrix_list();

    BOOST_CHECK( error1_list_serial.size() > 0 );
    BOOST_CHECK( step_matrix_list_dummy_serial.size() > 0 );
    BOOST_CHECK_EQUAL( error1_list_serial.size(), error1_list_dummy_serial.size() );
    BOOST_CHECK_EQUAL( step_matrix_list_serial.size(), step_matrix_list_dummy_serial.size() );
    for (std::size_t j = 0; j < error1_list_serial.size(); j++)
    {
      SANS_CHECK_CLOSE( error1_list_serial[j], error1_list_dummy_serial[j], small_tol, close_tol );

      // check that the step matrices used to build the models match
      for (int ii = 0; ii < PhysDim::D; ii++)
        for (int jj = ii; jj < PhysDim::D; jj++)
          SANS_CHECK_CLOSE( step_matrix_list_serial[j](ii,jj), step_matrix_list_dummy_serial[j](ii,jj), small_tol, close_tol );
    }
  }

}
#endif



//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
