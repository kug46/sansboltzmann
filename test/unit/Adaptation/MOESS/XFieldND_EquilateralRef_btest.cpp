// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XFieldND_EquilateralRef_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Adaptation/MOESS/XFieldND_EquilateralRef.h"

#include "Field/XFieldLine.h"
#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"
#include "Field/XFieldSpacetime.h"

#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"
#include "Field/Element/ElementXFieldSpacetime.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )

int
factorial( int i )
{
  if (i<=1) return 1;
  return i*factorial(i-1);
}

Real
unitVolume( int n )
{
  return std::sqrt( Real(n+1) )/( factorial(n)*std::sqrt(std::pow(2.,n)) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XFieldND_EquilateralRef_Line_test )
{
  typedef XFieldND_EquilateralRef<PhysD1, TopoD1, Line> XFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XFieldClass xfld(comm_local);

  BOOST_CHECK_EQUAL( xfld.nDOF(), 2 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 );

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XFieldClass::FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);
  BOOST_CHECK_EQUAL( xfldCell.nElem(), 1 );

  // ensure the length = 1
  ElementXField<PhysD1,TopoD1,Line> xfldElem( xfldCell.basis() );
  xfldCell.getElement( xfldElem , 0 );
  SANS_CHECK_CLOSE( xfldElem.length() , unitVolume(1) , 1e-12 , 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XFieldND_EquilateralRef_Triangle_test )
{
  typedef XFieldND_EquilateralRef<PhysD2, TopoD2, Triangle> XFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XFieldClass xfld(comm_local);

  BOOST_CHECK_EQUAL( xfld.nDOF(), 3 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );   BOOST_CHECK_EQUAL( xfld.DOF(0)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 );   BOOST_CHECK_EQUAL( xfld.DOF(1)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 0.5 ); BOOST_CHECK_EQUAL( xfld.DOF(2)[1], sqrt(3.0)/2.0 );

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XFieldClass::FieldCellGroupType<Triangle>& xfldCell = xfld.getCellGroup<Triangle>(0);
  BOOST_CHECK_EQUAL( xfldCell.nElem(), 1 );

  // ensure the area = sqrt(3)/4
  ElementXField<PhysD2,TopoD2,Triangle> xfldElem( xfldCell.basis() );
  xfldCell.getElement( xfldElem , 0 );
  SANS_CHECK_CLOSE( xfldElem.area() , unitVolume( PhysD2::D ) , 1e-12 , 1e-12 );
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XFieldND_EquilateralRef_Quad_test )
{
  typedef XFieldND_EquilateralRef<PhysD2, TopoD2, Quad> XFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XFieldClass xfld(comm_local);

  BOOST_CHECK_EQUAL( xfld.nDOF(), 4 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 ); BOOST_CHECK_EQUAL( xfld.DOF(0)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 ); BOOST_CHECK_EQUAL( xfld.DOF(1)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 1 ); BOOST_CHECK_EQUAL( xfld.DOF(2)[1], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0], 0 ); BOOST_CHECK_EQUAL( xfld.DOF(3)[1], 1 );

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Quad) );

  const XFieldClass::FieldCellGroupType<Quad>& xfldCell = xfld.getCellGroup<Quad>(0);
  BOOST_CHECK_EQUAL( xfldCell.nElem(), 1 );

  // ensure area = 1
  ElementXField<PhysD2,TopoD2,Quad> xfldElem( xfldCell.basis() );
  xfldCell.getElement( xfldElem , 0 );
  SANS_CHECK_CLOSE( xfldElem.area() , 1. , 1e-12 , 1e-12 );
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XFieldND_EquilateralRef_Tet_test )
{
  typedef XFieldND_EquilateralRef<PhysD3, TopoD3, Tet> XFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XFieldClass xfld(comm_local);

  BOOST_CHECK_EQUAL( xfld.nDOF(), 4 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(0)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(0)[2], 0 );

  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[2], 0 );

  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 0.5 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[1], sqrt(3.0)/2.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[2], 0 );

  BOOST_CHECK_EQUAL( xfld.DOF(3)[0], 0.5 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[1], sqrt(3.0)/6.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[2], sqrt(2.0/3.0) );

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  const XFieldClass::FieldCellGroupType<Tet>& xfldCell = xfld.getCellGroup<Tet>(0);
  BOOST_CHECK_EQUAL( xfldCell.nElem(), 1 );

  // ensure volume of unit equilateral tet
  ElementXField<PhysD3,TopoD3,Tet> xfldElem( xfldCell.basis() );
  xfldCell.getElement( xfldElem , 0 );
  SANS_CHECK_CLOSE( xfldElem.volume() , unitVolume( PhysD3::D ) , 1e-12 , 1e-12 );
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XFieldND_EquilateralRef_Hex_test )
{
  typedef XFieldND_EquilateralRef<PhysD3, TopoD3, Hex> XFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XFieldClass xfld(comm_local);

  BOOST_CHECK_EQUAL( xfld.nDOF(), 8 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(0)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(0)[2], 0 );

  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[2], 0 );

  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[1], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[2], 0 );

  BOOST_CHECK_EQUAL( xfld.DOF(3)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[1], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[2], 0 );

  BOOST_CHECK_EQUAL( xfld.DOF(4)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[2], 1 );

  BOOST_CHECK_EQUAL( xfld.DOF(5)[0], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(5)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(5)[2], 1 );

  BOOST_CHECK_EQUAL( xfld.DOF(6)[0], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(6)[1], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(6)[2], 1 );

  BOOST_CHECK_EQUAL( xfld.DOF(7)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(7)[1], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(7)[2], 1 );

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Hex) );

  const XFieldClass::FieldCellGroupType<Hex>& xfldCell = xfld.getCellGroup<Hex>(0);
  BOOST_CHECK_EQUAL( xfldCell.nElem(), 1 );

  // ensure area = 1
  ElementXField<PhysD3,TopoD3,Hex> xfldElem( xfldCell.basis() );
  xfldCell.getElement( xfldElem , 0 );
  SANS_CHECK_CLOSE( xfldElem.volume() , 1. , 1e-12 , 1e-12 );
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XFieldND_EquilateralRef_Pentatope_test )
{
  typedef XFieldND_EquilateralRef<PhysD4, TopoD4, Pentatope> XFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XFieldClass xfld(comm_local);

  BOOST_CHECK_EQUAL( xfld.nDOF(), 5 );

  //Check the node values
  Real fac = sqrt(4./10);
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 1*fac );
  BOOST_CHECK_EQUAL( xfld.DOF(0)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(0)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(0)[3], 0 );

  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], -0.25*fac );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[1], sqrt(15.)/4.*fac );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[3], 0 );

  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], -0.25*fac );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[1], -sqrt(5./48.)*fac );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[2], sqrt(5./6.)*fac );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[3], 0 );

  BOOST_CHECK_EQUAL( xfld.DOF(3)[0], -0.25*fac );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[1], -sqrt(5./48.)*fac );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[2], -sqrt(5./24.)*fac );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[3],  sqrt(5./8.)*fac );

  BOOST_CHECK_EQUAL( xfld.DOF(4)[0], -0.25*fac );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[1], -sqrt(5./48.)*fac );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[2], -sqrt(5./24.)*fac );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[3], -sqrt(5./8.)*fac );

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Pentatope) );

  const XFieldClass::FieldCellGroupType<Pentatope>& xfldCell = xfld.getCellGroup<Pentatope>(0);
  BOOST_CHECK_EQUAL( xfldCell.nElem(), 1 );

  // ensure volume of unit equilateral pentatope
  ElementXField<PhysD4,TopoD4,Pentatope> xfldElem( xfldCell.basis() );
  xfldCell.getElement( xfldElem , 0 );
  SANS_CHECK_CLOSE( xfldElem.volume() , unitVolume( PhysD4::D ) , 1e-12 , 1e-12 );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
