// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// ParamFieldBuilder_btest
// testing of the ParamFieldBuilder class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Adaptation/MOESS/ParamFieldBuilder.h"
#include "Field/Local/XField_LocalPatch.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"

#include "Field/DistanceFunction/DistanceFunction.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ParamFieldBuilder_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ParamFieldBuilder_None_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  int ii = 3;
  int jj = 3;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  typedef Field_CG_Cell<PhysD2, TopoD2, Real> QFieldType;

  QFieldType qfld(xfld, 1, BasisFunctionCategory_Lagrange);

  typedef ParamFieldBuilder<ParamType_None, PhysD2, TopoD2, QFieldType> ParamFieldBuilder;
  typedef ParamFieldBuilder_Local<ParamType_None, PhysD2, TopoD2, QFieldType> ParamFieldBuilderLocal;

  PyDict dict;

  ParamFieldBuilder builder(xfld, qfld, dict);

  BOOST_CHECK_EQUAL( &builder.fld, &xfld);

  //Create local param field

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh
  const int main_group = 0;
  const int main_cell = 1;
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local, connectivity, main_group, main_cell);

  int split_edge_index = 0;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_construct,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  QFieldType qfld_local(xfld_split_local, 1, BasisFunctionCategory_Lagrange);

  ParamFieldBuilderLocal builder_local(xfld_split_local, builder, qfld_local);

  BOOST_CHECK_EQUAL( &builder_local.fld.getXField(), &xfld_split_local);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ParamFieldBuilder_Distance_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  int ii = 3;
  int jj = 3;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  typedef Field_CG_Cell<PhysD2, TopoD2, Real> QFieldType;

  QFieldType qfld(xfld, 1, BasisFunctionCategory_Lagrange);

  typedef ParamFieldBuilder<ParamType_Distance, PhysD2, TopoD2, QFieldType> ParamFieldBuilder;
  typedef ParamFieldBuilder_Local<ParamType_Distance, PhysD2, TopoD2, QFieldType> ParamFieldBuilderLocal;

  //Compute distance field
  Field_CG_Cell<PhysD2, TopoD2, Real> distfld(xfld, 1, BasisFunctionCategory_Lagrange);
  DistanceFunction(distfld, {xfld.iLeft, xfld.iTop});

  PyDict d;

  ParamFieldBuilder builder((distfld, xfld), qfld, d);

  BOOST_CHECK_EQUAL( &builder.fld.getXField(), &xfld);
  BOOST_CHECK_EQUAL( &get<0>(builder.fld).getXField(), &xfld); //distance field
  BOOST_CHECK_EQUAL( &get<1>(builder.fld), &xfld);

  //Create local param field

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh
  const int main_group = 0;
  const int main_cell = 8;
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local, connectivity, main_group, main_cell);

  int split_edge_index = 0;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_construct,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  QFieldType qfld_local(xfld_split_local, 1, BasisFunctionCategory_Lagrange);

  ParamFieldBuilderLocal builder_local(xfld_split_local, builder, qfld_local);

  BOOST_CHECK_EQUAL( &builder_local.fld.getXField(), &xfld_split_local);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ParamFieldBuilder_GenH_CG_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  int ii = 3;
  int jj = 3;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  typedef Field_CG_Cell<PhysD2, TopoD2, Real> QFieldType;

  QFieldType qfld(xfld, 1, BasisFunctionCategory_Lagrange);

  typedef ParamFieldBuilder<ParamType_GenH_CG, PhysD2, TopoD2, QFieldType> ParamFieldBuilder;
  typedef ParamFieldBuilder_Local<ParamType_GenH_CG, PhysD2, TopoD2, QFieldType> ParamFieldBuilderLocal;

  //Create generalized H-tensor field
  GenHField_CG<PhysD2, TopoD2> Hfld(xfld);

  PyDict dict;

  ParamFieldBuilder builder((Hfld, xfld), qfld, dict);

  BOOST_CHECK_EQUAL( &builder.fld.getXField(), &xfld);
  BOOST_CHECK_EQUAL( &get<0>(builder.fld), &Hfld); //gen-H field
  BOOST_CHECK_EQUAL( &get<0>(builder.fld).getXField(), &xfld); //gen-H field
  BOOST_CHECK_EQUAL( &get<1>(builder.fld), &xfld);

  //Create local param field

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh
  const int main_group = 0;
  const int main_cell = 8;
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local, connectivity, main_group, main_cell);

  int split_edge_index = 0;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_construct,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  QFieldType qfld_local(xfld_split_local, 1, BasisFunctionCategory_Lagrange);

  ParamFieldBuilderLocal builder_local(xfld_split_local, builder, qfld_local);

  BOOST_CHECK_EQUAL( &builder_local.fld.getXField(), &xfld_split_local);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ParamFieldBuilder_GenH_DG_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  int ii = 3;
  int jj = 3;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  typedef Field_CG_Cell<PhysD2, TopoD2, Real> QFieldType;

  QFieldType qfld(xfld, 1, BasisFunctionCategory_Lagrange);

  typedef ParamFieldBuilder<ParamType_GenH_DG, PhysD2, TopoD2, QFieldType> ParamFieldBuilder;
  typedef ParamFieldBuilder_Local<ParamType_GenH_DG, PhysD2, TopoD2, QFieldType> ParamFieldBuilderLocal;

  //Create generalized H-tensor field
  GenHField_DG<PhysD2, TopoD2> Hfld(xfld);

  PyDict dict;

  ParamFieldBuilder builder((Hfld, xfld), qfld, dict);

  BOOST_CHECK_EQUAL( &builder.fld.getXField(), &xfld);
  BOOST_CHECK_EQUAL( &get<0>(builder.fld), &Hfld); //gen-H field
  BOOST_CHECK_EQUAL( &get<0>(builder.fld).getXField(), &xfld); //gen-H field
  BOOST_CHECK_EQUAL( &get<1>(builder.fld), &xfld);

  //Create local param field

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh
  const int main_group = 0;
  const int main_cell = 8;
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local, connectivity, main_group, main_cell);

  int split_edge_index = 0;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_construct,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  QFieldType qfld_local(xfld_split_local, 1, BasisFunctionCategory_Lagrange);

  ParamFieldBuilderLocal builder_local(xfld_split_local, builder, qfld_local);

  BOOST_CHECK_EQUAL( &builder_local.fld.getXField(), &xfld_split_local);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ParamFieldBuilder_GenH_CG_Distance_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  int ii = 3;
  int jj = 3;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  typedef Field_CG_Cell<PhysD2, TopoD2, Real> QFieldType;

  QFieldType qfld(xfld, 1, BasisFunctionCategory_Lagrange);

  typedef ParamFieldBuilder<ParamType_GenH_CG_Distance, PhysD2, TopoD2, QFieldType> ParamFieldBuilder;
  typedef ParamFieldBuilder_Local<ParamType_GenH_CG_Distance, PhysD2, TopoD2, QFieldType> ParamFieldBuilderLocal;

  //Create generalized H-tensor field
  GenHField_CG<PhysD2, TopoD2> Hfld(xfld);

  //Compute distance field
  Field_CG_Cell<PhysD2, TopoD2, Real> distfld(xfld, 1, BasisFunctionCategory_Lagrange);
  DistanceFunction(distfld, {xfld.iLeft, xfld.iTop});

  PyDict dict;

  ParamFieldBuilder builder((Hfld, distfld, xfld), qfld, dict);

  BOOST_CHECK_EQUAL( &builder.fld.getXField(), &xfld);
  BOOST_CHECK_EQUAL( &get<0>(builder.fld), &Hfld); //gen-H field
  BOOST_CHECK_EQUAL( &get<1>(builder.fld), &distfld); //gen-H field
  BOOST_CHECK_EQUAL( &get<0>(builder.fld).getXField(), &xfld); //gen-H field
  BOOST_CHECK_EQUAL( &get<2>(builder.fld), &xfld);

  //Create local param field

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh
  const int main_group = 0;
  const int main_cell = 8;
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local, connectivity, main_group, main_cell);

  int split_edge_index = 0;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_construct,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  QFieldType qfld_local(xfld_split_local, 1, BasisFunctionCategory_Lagrange);

  ParamFieldBuilderLocal builder_local(xfld_split_local, builder, qfld_local);

  BOOST_CHECK_EQUAL( &builder_local.fld.getXField(), &xfld_split_local);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ParamFieldBuilder_GenH_DG_Distance_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  int ii = 3;
  int jj = 3;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  typedef Field_CG_Cell<PhysD2, TopoD2, Real> QFieldType;

  QFieldType qfld(xfld, 1, BasisFunctionCategory_Lagrange);

  typedef ParamFieldBuilder<ParamType_GenH_DG_Distance, PhysD2, TopoD2, QFieldType> ParamFieldBuilder;
  typedef ParamFieldBuilder_Local<ParamType_GenH_DG_Distance, PhysD2, TopoD2, QFieldType> ParamFieldBuilderLocal;

  //Create generalized H-tensor field
  GenHField_CG<PhysD2, TopoD2> Hfld(xfld);

  //Compute distance field
  Field_CG_Cell<PhysD2, TopoD2, Real> distfld(xfld, 1, BasisFunctionCategory_Lagrange);
  DistanceFunction(distfld, {xfld.iLeft, xfld.iTop});

  PyDict dict;

  ParamFieldBuilder builder((Hfld, distfld, xfld), qfld, dict);

  BOOST_CHECK_EQUAL( &builder.fld.getXField(), &xfld);
  BOOST_CHECK_EQUAL( &get<0>(builder.fld), &Hfld); //gen-H field
  BOOST_CHECK_EQUAL( &get<1>(builder.fld), &distfld); //gen-H field
  BOOST_CHECK_EQUAL( &get<0>(builder.fld).getXField(), &xfld); //gen-H field
  BOOST_CHECK_EQUAL( &get<2>(builder.fld), &xfld);

  //Create local param field

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh
  const int main_group = 0;
  const int main_cell = 8;
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local, connectivity, main_group, main_cell);

  int split_edge_index = 0;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_construct,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  QFieldType qfld_local(xfld_split_local, 1, BasisFunctionCategory_Lagrange);

  ParamFieldBuilderLocal builder_local(xfld_split_local, builder, qfld_local);

  BOOST_CHECK_EQUAL( &builder_local.fld.getXField(), &xfld_split_local);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
