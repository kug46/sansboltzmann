// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// ReferenceStepMatrix_btest
// testing of the ReferenceStepMatrix class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Adaptation/MOESS/ReferenceStepMatrix.h"

#include "BasisFunction/BasisFunction_RefElement_Split.h"

#include "Field/XFieldLine.h"
#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"
#include "Field/XFieldSpacetime.h"

// Local Patch still does not support isotropic splits...
#define LOCAL_SPLIT_PATCH 1

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include <iostream>
using namespace std;


//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ReferenceStepMatrix_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( RefStepMatrix1D_Line )
{
  typedef PhysD1 PhysDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> StepMatrix;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  bool uniform_refinement = true;

  std::vector<LocalSplitConfig> split_config_list = LocalSplitConfigList<Line>::get(uniform_refinement);

  ReferenceStepMatrix<PhysD1, TopoD1> builder(comm_local, split_config_list);

  Real tol = 1e-13;

  StepMatrix S = builder.getStepMatrix(0);
  SANS_CHECK_CLOSE( S(0,0), log(4.0), tol, tol );

  // sub indexing
  S = builder.getStepMatrix(0,0);
  SANS_CHECK_CLOSE( S(0,0), log(4.0), tol, tol );
  S = builder.getStepMatrix(0,1);
  SANS_CHECK_CLOSE( S(0,0), log(4.0), tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( RefStepMatrix2D_Triangle )
{
  typedef PhysD2 PhysDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> StepMatrix;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

#if LOCAL_SPLIT_PATCH
  bool uniform_refinement = false;
#else
  bool uniform_refinement = true;
#endif
  std::vector<LocalSplitConfig> split_config_list = LocalSplitConfigList<Triangle>::get(uniform_refinement);

  ReferenceStepMatrix<PhysD2, TopoD2> builder(comm_local,split_config_list);

  Real tol = 1e-11;

  StepMatrix S = builder.getStepMatrix(0);
  SANS_CHECK_CLOSE( S(0,0), 0.418494108392918, tol, tol );
  SANS_CHECK_CLOSE( S(1,0),-0.475713075448173, tol, tol );
  SANS_CHECK_CLOSE( S(1,1), 0.967800252726973, tol, tol );

  S = builder.getStepMatrix(1);
  SANS_CHECK_CLOSE( S(0,0), 0.418494108392918, tol, tol );
  SANS_CHECK_CLOSE( S(1,0), 0.475713075448173, tol, tol );
  SANS_CHECK_CLOSE( S(1,1), 0.967800252726973, tol, tol );

  S = builder.getStepMatrix(2);
  SANS_CHECK_CLOSE( S(0,0), log(2.0*sqrt(3.0)), tol, tol );
  SANS_CHECK_CLOSE( S(1,0), 0.0, tol, tol );
  SANS_CHECK_CLOSE( S(1,1), log(2.0*sqrt(3.0)/3.0), tol, tol );

#if LOCAL_SPLIT_PATCH
  // isotropic splits not supported with local split patch
#else
  S = builder.getStepMatrix(3);
  SANS_CHECK_CLOSE( S(0,0), log(4.0), tol, tol );
  SANS_CHECK_CLOSE( S(1,0), 0.0, tol, tol );
  SANS_CHECK_CLOSE( S(1,1), log(4.0), tol, tol );
#endif

  // sub-cell indices
  int i,j;
  i = 0, j = 0;
  S = builder.getStepMatrix(i,j);
  SANS_CHECK_CLOSE( S(0,0),0.8434571242605627, tol, tol );
  SANS_CHECK_CLOSE( S(1,0),-0.7810333781168599, tol, tol );
  SANS_CHECK_CLOSE( S(1,1),0.5428372368593279, tol, tol );

  i = 1, j = 0;
  S = builder.getStepMatrix(i,j);
  SANS_CHECK_CLOSE( S(0,0),-0.05840253794314158, tol, tol );
  SANS_CHECK_CLOSE( S(1,0),0.2603444593722866, tol, tol );
  SANS_CHECK_CLOSE( S(1,1),1.444696899063031, tol, tol );

  i = 0, j = 1;
  S = builder.getStepMatrix(i,j);
  SANS_CHECK_CLOSE( S(0,0),-0.05840253794314093, tol, tol );
  SANS_CHECK_CLOSE( S(1,0),-0.2603444593722869, tol, tol );
  SANS_CHECK_CLOSE( S(1,1),1.44469689906303, tol, tol );

  i = 1, j = 1;
  S = builder.getStepMatrix(i,j);
  SANS_CHECK_CLOSE( S(0,0),0.8434571242605613, tol, tol );
  SANS_CHECK_CLOSE( S(1,0),0.7810333781168599, tol, tol );
  SANS_CHECK_CLOSE( S(1,1),0.5428372368593277, tol, tol );

  i = 2, j = 0;
  S = builder.getStepMatrix(i,j);
  SANS_CHECK_CLOSE( S(0,0),1.294386955362413, tol, tol );
  SANS_CHECK_CLOSE( S(1,0),0.5206889187445746, tol, tol );
  SANS_CHECK_CLOSE( S(1,1),0.09190740575747657, tol, tol );

  i = 2, j = 1;
  S = builder.getStepMatrix(i,j);
  SANS_CHECK_CLOSE( S(0,0),1.294386955362415, tol, tol );
  SANS_CHECK_CLOSE( S(1,0),-0.5206889187445719, tol, tol );
  SANS_CHECK_CLOSE( S(1,1),0.09190740575747548, tol, tol );

#if LOCAL_SPLIT_PATCH
  // isotropic splits not supported with local split patch
#else
  i = 3;
  for (int k = 0; k < 4; k++)
  {
    S = builder.getStepMatrix(i,k);
    SANS_CHECK_CLOSE( S(0,0), 2.0*log(2), tol, tol );
    SANS_CHECK_CLOSE( S(1,0), 0.0, tol, tol );
    SANS_CHECK_CLOSE( S(1,1), 2.0*log(2), tol, tol );
  }
#endif

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( RefStepMatrix2D_Quad )
{
  BOOST_CHECK_THROW( LocalSplitConfigList<Quad>::get(true), DeveloperException);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( RefStepMatrix3D_Tet )
{
  typedef PhysD3 PhysDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> StepMatrix;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

#if LOCAL_SPLIT_PATCH
  bool uniform_refinement = false;
#else
  bool uniform_refinement = true;
#endif
  std::vector<LocalSplitConfig> split_config_list = LocalSplitConfigList<Tet>::get(uniform_refinement);

  ReferenceStepMatrix<PhysD3, TopoD3> builder(comm_local,split_config_list);

  Real tol = 1e-11;

  StepMatrix S = builder.getStepMatrix(0);
  SANS_CHECK_CLOSE(                   0.0, S(0,0), tol, tol );
  SANS_CHECK_CLOSE(                   0.0, S(1,0), tol, tol );
  SANS_CHECK_CLOSE( 5.296756383913240e-01, S(1,1), tol, tol );
  SANS_CHECK_CLOSE(                   0.0, S(2,0), tol, tol );
  SANS_CHECK_CLOSE(-4.623673439938185e-01, S(2,1), tol, tol );
  SANS_CHECK_CLOSE( 8.566187227285654e-01, S(2,2), tol, tol );

  S = builder.getStepMatrix(1);
  SANS_CHECK_CLOSE( 3.972567287934924e-01, S(0,0), tol, tol );
  SANS_CHECK_CLOSE(-2.293562793063132e-01, S(1,0), tol, tol );
  SANS_CHECK_CLOSE( 1.324189095978296e-01, S(1,1), tol, tol );
  SANS_CHECK_CLOSE(-4.004218657789844e-01, S(2,0), tol, tol );
  SANS_CHECK_CLOSE( 2.311836719969094e-01, S(2,1), tol, tol );
  SANS_CHECK_CLOSE( 8.566187227285661e-01, S(2,2), tol, tol );

  S = builder.getStepMatrix(2);
  SANS_CHECK_CLOSE( 3.972567287934949e-01, S(0,0), tol, tol );
  SANS_CHECK_CLOSE(-4.539734486056918e-01, S(1,0), tol, tol );
  SANS_CHECK_CLOSE( 9.214601143083720e-01, S(1,1), tol, tol );
  SANS_CHECK_CLOSE(-8.276521861395229e-02, S(2,0), tol, tol );
  SANS_CHECK_CLOSE(-4.778452124630309e-02, S(2,1), tol, tol );
  SANS_CHECK_CLOSE( 6.757751801802712e-02, S(2,2), tol, tol );

  S = builder.getStepMatrix(3);
  SANS_CHECK_CLOSE( 3.972567287934942e-01, S(0,0), tol, tol );
  SANS_CHECK_CLOSE( 4.539734486056917e-01, S(1,0), tol, tol );
  SANS_CHECK_CLOSE( 9.214601143083709e-01, S(1,1), tol, tol );
  SANS_CHECK_CLOSE( 8.276521861395218e-02, S(2,0), tol, tol );
  SANS_CHECK_CLOSE(-4.778452124630363e-02, S(2,1), tol, tol );
  SANS_CHECK_CLOSE( 6.757751801802708e-02, S(2,2), tol, tol );

  S = builder.getStepMatrix(4);
  SANS_CHECK_CLOSE( 3.972567287934931e-01, S(0,0), tol, tol );
  SANS_CHECK_CLOSE( 2.293562793063132e-01, S(1,0), tol, tol );
  SANS_CHECK_CLOSE( 1.324189095978310e-01, S(1,1), tol, tol );
  SANS_CHECK_CLOSE( 4.004218657789851e-01, S(2,0), tol, tol );
  SANS_CHECK_CLOSE( 2.311836719969092e-01, S(2,1), tol, tol );
  SANS_CHECK_CLOSE( 8.566187227285661e-01, S(2,2), tol, tol );

  S = builder.getStepMatrix(5);
  SANS_CHECK_CLOSE( 1.183561807065809e+00, S(0,0), tol, tol );
  SANS_CHECK_CLOSE(                   0.0, S(1,0), tol, tol );
  SANS_CHECK_CLOSE( 1.351550360360550e-01, S(1,1), tol, tol );
  SANS_CHECK_CLOSE(                   0.0, S(2,0), tol, tol );
  SANS_CHECK_CLOSE( 9.556904249260745e-02, S(2,1), tol, tol );
  SANS_CHECK_CLOSE( 6.757751801802669e-02, S(2,2), tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( RefStepMatrix3D_Hex )
{
  BOOST_CHECK_THROW( LocalSplitConfigList<Hex>::get(true), DeveloperException);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( RefStepMatrix4D_Pentatope )
{
  typedef PhysD4 PhysDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> StepMatrix;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  std::vector<LocalSplitConfig> split_config_list = LocalSplitConfigList<Pentatope>::get(false);

  ReferenceStepMatrix<PhysD4, TopoD4> builder(comm_local,split_config_list);

  Real tol = 1e-11;

  StepMatrix S = builder.getStepMatrix(0);
  SANS_CHECK_CLOSE( 8.0768352204421467e-01 , S(0,0) , tol , tol );
  SANS_CHECK_CLOSE( -4.4359734310410104e-01 , S(0,1) , tol , tol );
  SANS_CHECK_CLOSE( 0.0000000000000000e+00 , S(0,2) , tol , tol );
  SANS_CHECK_CLOSE( 0.0000000000000000e+00 , S(0,3) , tol , tol );
  SANS_CHECK_CLOSE( 5.7861083907567590e-01 , S(1,1) , tol , tol );
  SANS_CHECK_CLOSE( 0.0000000000000000e+00 , S(1,2) , tol , tol );
  SANS_CHECK_CLOSE( 0.0000000000000000e+00 , S(1,3) , tol , tol );
  SANS_CHECK_CLOSE( 0.0000000000000000e+00 , S(2,2) , tol , tol );
  SANS_CHECK_CLOSE( 0.0000000000000000e+00 , S(2,3) , tol , tol );
  SANS_CHECK_CLOSE( 0.0000000000000000e+00 , S(3,3) , tol , tol );

  S = builder.getStepMatrix(1);
  SANS_CHECK_CLOSE( 8.0768352204421467e-01 , S(0,0) , tol , tol );
  SANS_CHECK_CLOSE( 1.4786578103470033e-01 , S(0,1) , tol , tol );
  SANS_CHECK_CLOSE( -4.1822758590032721e-01 , S(0,2) , tol , tol );
  SANS_CHECK_CLOSE( 2.6330578883200234e-40 , S(0,3) , tol , tol );
  SANS_CHECK_CLOSE( 6.4290093230630660e-02 , S(1,1) , tol , tol );
  SANS_CHECK_CLOSE( -1.8183984354597718e-01 , S(1,2) , tol , tol );
  SANS_CHECK_CLOSE( 9.7575439510921502e-41 , S(1,3) , tol , tol );
  SANS_CHECK_CLOSE( 5.1432074584504528e-01 , S(2,2) , tol , tol );
  SANS_CHECK_CLOSE( -3.2978100796381048e-40 , S(2,3) , tol , tol );
  SANS_CHECK_CLOSE( -6.1675145632502339e-43 , S(3,3) , tol , tol );

  S = builder.getStepMatrix(2);
  SANS_CHECK_CLOSE( 8.0768352204421467e-01 , S(0,0) , tol , tol );
  SANS_CHECK_CLOSE( 1.4786578103470033e-01 , S(0,1) , tol , tol );
  SANS_CHECK_CLOSE( 2.0911379295016361e-01 , S(0,2) , tol , tol );
  SANS_CHECK_CLOSE( -3.6219571395312189e-01 , S(0,3) , tol , tol );
  SANS_CHECK_CLOSE( 6.4290093230630660e-02 , S(1,1) , tol , tol );
  SANS_CHECK_CLOSE( 9.0919921772988591e-02 , S(1,2) , tol , tol );
  SANS_CHECK_CLOSE( -1.5747792393100404e-01 , S(1,3) , tol , tol );
  SANS_CHECK_CLOSE( 1.2858018646126132e-01 , S(2,2) , tol , tol );
  SANS_CHECK_CLOSE( -2.2270741579758449e-01 , S(2,3) , tol , tol );
  SANS_CHECK_CLOSE( 3.8574055938378393e-01 , S(3,3) , tol , tol );

  S = builder.getStepMatrix(3);
  SANS_CHECK_CLOSE( 8.0768352204421467e-01 , S(0,0) , tol , tol );
  SANS_CHECK_CLOSE( 1.4786578103470033e-01 , S(0,1) , tol , tol );
  SANS_CHECK_CLOSE( 2.0911379295016361e-01 , S(0,2) , tol , tol );
  SANS_CHECK_CLOSE( 3.6219571395312189e-01 , S(0,3) , tol , tol );
  SANS_CHECK_CLOSE( 6.4290093230630660e-02 , S(1,1) , tol , tol );
  SANS_CHECK_CLOSE( 9.0919921772988591e-02 , S(1,2) , tol , tol );
  SANS_CHECK_CLOSE( 1.5747792393100404e-01 , S(1,3) , tol , tol );
  SANS_CHECK_CLOSE( 1.2858018646126132e-01 , S(2,2) , tol , tol );
  SANS_CHECK_CLOSE( 2.2270741579758449e-01 , S(2,3) , tol , tol );
  SANS_CHECK_CLOSE( 3.8574055938378393e-01 , S(3,3) , tol , tol );

  S = builder.getStepMatrix(4);
  SANS_CHECK_CLOSE( 3.9166969103811296e-02 , S(0,0) , tol , tol );
  SANS_CHECK_CLOSE( -5.0564339686827192e-02 , S(0,1) , tol , tol );
  SANS_CHECK_CLOSE( -7.1508774957551161e-02 , S(0,2) , tol , tol );
  SANS_CHECK_CLOSE( 1.6687625269498085e-41 , S(0,3) , tol , tol );
  SANS_CHECK_CLOSE( 8.3280664617103406e-01 , S(1,1) , tol , tol );
  SANS_CHECK_CLOSE( -4.5040707966561910e-01 , S(1,2) , tol , tol );
  SANS_CHECK_CLOSE( 5.2139167819116496e-41 , S(1,3) , tol , tol );
  SANS_CHECK_CLOSE( 5.1432074584504528e-01 , S(2,2) , tol , tol );
  SANS_CHECK_CLOSE( 1.8355182325828383e-40 , S(2,3) , tol , tol );
  SANS_CHECK_CLOSE( 0.0000000000000000e+00 , S(3,3) , tol , tol );

  S = builder.getStepMatrix(5);
  SANS_CHECK_CLOSE( 3.9166969103811296e-02 , S(0,0) , tol , tol );
  SANS_CHECK_CLOSE( -5.0564339686827192e-02 , S(0,1) , tol , tol );
  SANS_CHECK_CLOSE( 3.5754387478775580e-02 , S(0,2) , tol , tol );
  SANS_CHECK_CLOSE( -6.1928415706743796e-02 , S(0,3) , tol , tol );
  SANS_CHECK_CLOSE( 8.3280664617103406e-01 , S(1,1) , tol , tol );
  SANS_CHECK_CLOSE( 2.2520353983280955e-01 , S(1,2) , tol , tol );
  SANS_CHECK_CLOSE( -3.9006397303478757e-01 , S(1,3) , tol , tol );
  SANS_CHECK_CLOSE( 1.2858018646126132e-01 , S(2,2) , tol , tol );
  SANS_CHECK_CLOSE( -2.2270741579758449e-01 , S(2,3) , tol , tol );
  SANS_CHECK_CLOSE( 3.8574055938378393e-01 , S(3,3) , tol , tol );

  S = builder.getStepMatrix(6);
  SANS_CHECK_CLOSE( 3.9166969103811296e-02 , S(0,0) , tol , tol );
  SANS_CHECK_CLOSE( -5.0564339686827192e-02 , S(0,1) , tol , tol );
  SANS_CHECK_CLOSE( 3.5754387478775580e-02 , S(0,2) , tol , tol );
  SANS_CHECK_CLOSE( 6.1928415706743796e-02 , S(0,3) , tol , tol );
  SANS_CHECK_CLOSE( 8.3280664617103406e-01 , S(1,1) , tol , tol );
  SANS_CHECK_CLOSE( 2.2520353983280955e-01 , S(1,2) , tol , tol );
  SANS_CHECK_CLOSE( 3.9006397303478757e-01 , S(1,3) , tol , tol );
  SANS_CHECK_CLOSE( 1.2858018646126132e-01 , S(2,2) , tol , tol );
  SANS_CHECK_CLOSE( 2.2270741579758449e-01 , S(2,3) , tol , tol );
  SANS_CHECK_CLOSE( 3.8574055938378393e-01 , S(3,3) , tol , tol );

  S = builder.getStepMatrix(7);
  SANS_CHECK_CLOSE( 3.9166969103811296e-02 , S(0,0) , tol , tol );
  SANS_CHECK_CLOSE( 5.0564339686827192e-02 , S(0,1) , tol , tol );
  SANS_CHECK_CLOSE( -3.5754387478775580e-02 , S(0,2) , tol , tol );
  SANS_CHECK_CLOSE( -6.1928415706743796e-02 , S(0,3) , tol , tol );
  SANS_CHECK_CLOSE( 6.5278281839685495e-02 , S(1,1) , tol , tol );
  SANS_CHECK_CLOSE( -4.6158715753048273e-02 , S(1,2) , tol , tol );
  SANS_CHECK_CLOSE( -7.9949240896409507e-02 , S(1,3) , tol , tol );
  SANS_CHECK_CLOSE( 8.9610855079260987e-01 , S(2,2) , tol , tol );
  SANS_CHECK_CLOSE( -4.4199164583848138e-01 , S(2,3) , tol , tol );
  SANS_CHECK_CLOSE( 3.8574055938378393e-01 , S(3,3) , tol , tol );

  S = builder.getStepMatrix(8);
  SANS_CHECK_CLOSE( 3.9166969103811296e-02 , S(0,0) , tol , tol );
  SANS_CHECK_CLOSE( 5.0564339686827192e-02 , S(0,1) , tol , tol );
  SANS_CHECK_CLOSE( -3.5754387478775580e-02 , S(0,2) , tol , tol );
  SANS_CHECK_CLOSE( 6.1928415706743796e-02 , S(0,3) , tol , tol );
  SANS_CHECK_CLOSE( 6.5278281839685495e-02 , S(1,1) , tol , tol );
  SANS_CHECK_CLOSE( -4.6158715753048273e-02 , S(1,2) , tol , tol );
  SANS_CHECK_CLOSE( 7.9949240896409507e-02 , S(1,3) , tol , tol );
  SANS_CHECK_CLOSE( 8.9610855079260987e-01 , S(2,2) , tol , tol );
  SANS_CHECK_CLOSE( 4.4199164583848138e-01 , S(2,3) , tol , tol );
  SANS_CHECK_CLOSE( 3.8574055938378393e-01 , S(3,3) , tol , tol );

  S = builder.getStepMatrix(9);
  SANS_CHECK_CLOSE( 3.9166969103811296e-02 , S(0,0) , tol , tol );
  SANS_CHECK_CLOSE( 5.0564339686827192e-02 , S(0,1) , tol , tol );
  SANS_CHECK_CLOSE( 7.1508774957551161e-02 , S(0,2) , tol , tol );
  SANS_CHECK_CLOSE( 0.0000000000000000e+00 , S(0,3) , tol , tol );
  SANS_CHECK_CLOSE( 6.5278281839685495e-02 , S(1,1) , tol , tol );
  SANS_CHECK_CLOSE( 9.2317431506096545e-02 , S(1,2) , tol , tol );
  SANS_CHECK_CLOSE( 0.0000000000000000e+00 , S(1,3) , tol , tol );
  SANS_CHECK_CLOSE( 1.3055656367937099e-01 , S(2,2) , tol , tol );
  SANS_CHECK_CLOSE( 0.0000000000000000e+00 , S(2,3) , tol , tol );
  SANS_CHECK_CLOSE( 1.1512925464970230e+00 , S(3,3) , tol , tol );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
