// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// CostModel_LogEuclidean_btest
// testing of the CostModel_LogEuclidean class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>
#include <random>

#include "Field/Field_NodalView.h"
#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldSpacetime_CG_Cell.h"

#include "Meshing/Metric/MetricOps.h"

#include "Adaptation/MOESS/CostModel_LogEuclidean.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Quad_X1.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"
#include "unit/UnitGrids/XField_KuhnFreudenthal.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;


//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
  typedef CostModel_LogEuclidean<PhysD1, TopoD1> CostModel1D;
  typedef CostModel_LogEuclidean<PhysD2, TopoD2> CostModel2D;
  typedef CostModel_LogEuclidean<PhysD3, TopoD3> CostModel3D;
  typedef CostModel_LogEuclidean<PhysD4, TopoD4> CostModel4D;
}

#define SANS_CHECK_PING_ORDER( err_vec, delta_vec, small_tol ) \
    if (err_vec[1] > small_tol){ \
      Real rate = log(err_vec[1]/err_vec[0])/log(delta_vec[1]/delta_vec[0]); \
      BOOST_CHECK_MESSAGE( rate >= 1.8 && rate <= 3.7, "Rate check failed: rate = " << rate << \
                           ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" ); \
    }

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( CostModel_LogEuclidean_test_suite )

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class Topology>
void pingCostModel_S(const XField<PhysDim,TopoDim>& xfld)
{
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  Real small_tol = 1e-10;
  Real close_tol = 1e-6;

  int nElem = xfld.nElem();

  std::vector<int> cellgroup_list = {0};

  //Metrics at each node
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> implied_metric(xfld, 1, BasisFunctionCategory_Lagrange, cellgroup_list);
  Field_NodalView nodalview(implied_metric, cellgroup_list);

  const int nNode = implied_metric.nDOF();

  for (int node = 0; node < nNode; node++)
  {
    //Get the list of cells around this node (each returned pair contains cell_group and cell_elem)
    Field_NodalView::IndexVector cell_list = nodalview.getCellList(node);

    if ((int) cell_list.size() == 1) //Only one cell at this node -> nothing to average
    {
      int cellgrp = cell_list[0].group;
      int elem = cell_list[0].elem;
      const XFieldCellGroupType& xfldCellGroup = xfld.template getCellGroup<Topology>(cellgrp);
      ElementXFieldClass xfldElem(xfldCellGroup.basis() );
      xfldCellGroup.getElement(xfldElem,elem);

      xfldElem.impliedMetric(implied_metric.DOF(node));
    }
    else //more than one cell around this node
    {
      int nCells = (int) cell_list.size();
      std::vector<MatrixSym> metric_list(nCells);
      std::vector<Real> weight_list(nCells, 1.0/((Real)nCells));

      for (int i = 0; i < nCells; i++)
      {
        int cellgrp = cell_list[i].group;
        int elem = cell_list[i].elem;
        const XFieldCellGroupType& xfldCellGroup = xfld.template getCellGroup<Topology>(cellgrp);
        ElementXFieldClass xfldElem(xfldCellGroup.basis() );
        xfldCellGroup.getElement(xfldElem,elem);

        xfldElem.impliedMetric(metric_list[i]);
      }

      implied_metric.DOF(node) = Metric::computeLogEuclideanAvg(metric_list, weight_list);
    }
  } //loop over nodes

  int nDOF0 = 1;
  CostModel_LogEuclidean<PhysDim, TopoDim> costmodel(implied_metric, cellgroup_list);

  //Step matrices at each node
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> Sfld(xfld, 1, BasisFunctionCategory_Lagrange, cellgroup_list);

  Real Cost0;
  std::vector<MatrixSym> dCost_dS0;

  // compute the cost with a zero step matrix to make sure it matches
  // the element count
  Sfld = 0;
  costmodel.computeCost_S(nDOF0, Sfld, Cost0, dCost_dS0);

  SANS_CHECK_CLOSE( Cost0, (Real) nElem*nDOF0, small_tol, close_tol );

  // initialize a uniform distribution between -2*log(2) and 2*log(2)
  std::mt19937_64 rng;
  std::uniform_real_distribution<double> unif(-2*log(2), 2*log(2));

  // initialize the serial vector to "random" numbers
  for ( int node = 0; node < nNode; node++ )
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      Sfld.DOF(node).value(ind) = unif(rng);

  std::vector<MatrixSym> dCost_dS_dummy;
  Real delta[2] = {1e-2, 1e-3};
  Real err_appox[2];

  // recompue the cost with the random Sfld
  dCost_dS0.assign(nNode, 0.0);
  costmodel.computeCost_S(nDOF0, Sfld, Cost0, dCost_dS0);

  // Perturbed step matrix fields
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> Sfld_p(Sfld, FieldCopy());
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> Sfld_m(Sfld, FieldCopy());

  for (int node = 0; node < nNode; node++)
  {
    for (int d = 0; d < PhysDim::D; d++)
    {
      for (int j = 0; j < 2; j++)
      {
        // perturb
        Sfld_p.DOF(node)(d,d) += delta[j];
        Sfld_m.DOF(node)(d,d) -= delta[j];

        Real Costp, Costm;
        costmodel.computeCost_S(nDOF0, Sfld_p, Costp, dCost_dS_dummy);
        costmodel.computeCost_S(nDOF0, Sfld_m, Costm, dCost_dS_dummy);

        // de-perturb
        Sfld_p.DOF(node)(d,d) -= delta[j];
        Sfld_m.DOF(node)(d,d) += delta[j];

        Real grad_approx = (Costp - Costm)/(2.0*delta[j]);
        err_appox[j] = fabs(grad_approx - dCost_dS0[node](d,d));
      }

      SANS_CHECK_PING_ORDER(err_appox, delta, small_tol);
    }

    //Off-diagonals should be zero
    if (PhysDim::D == 2)
    {
      BOOST_CHECK_EQUAL( 0.0, dCost_dS0[node](1,0) );
    }
    else if (PhysDim::D == 3)
    {
      BOOST_CHECK_EQUAL( 0.0, dCost_dS0[node](1,0) );
      BOOST_CHECK_EQUAL( 0.0, dCost_dS0[node](2,0) );
      BOOST_CHECK_EQUAL( 0.0, dCost_dS0[node](2,1) );
    }
    else if (PhysDim::D == 4)
    {
      BOOST_CHECK_EQUAL( 0.0, dCost_dS0[node](1,0) );
      BOOST_CHECK_EQUAL( 0.0, dCost_dS0[node](2,0) );
      BOOST_CHECK_EQUAL( 0.0, dCost_dS0[node](2,1) );
      BOOST_CHECK_EQUAL( 0.0, dCost_dS0[node](3,0) );
      BOOST_CHECK_EQUAL( 0.0, dCost_dS0[node](3,1) );
      BOOST_CHECK_EQUAL( 0.0, dCost_dS0[node](3,2) );
    }
  }
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class Topology>
void pingCostModel_logM(const XField<PhysDim,TopoDim>& xfld)
{
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  Real small_tol = 1e-10;
  Real close_tol = 1e-7;

  int nElem = xfld.nElem();

  std::vector<int> cellgroup_list = {0};

  //Metrics at each node
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> implied_metric(xfld, 1, BasisFunctionCategory_Lagrange, cellgroup_list);
  Field_NodalView nodalview(implied_metric, cellgroup_list);

  const int nNode = implied_metric.nDOF();

  //log matrices at each node
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> logMfld(xfld, 1, BasisFunctionCategory_Lagrange, cellgroup_list);

  for (int node = 0; node < nNode; node++)
  {
    //Get the list of cells around this node (each returned pair contains cell_group and cell_elem)
    Field_NodalView::IndexVector cell_list = nodalview.getCellList(node);

    if ((int) cell_list.size() == 1) //Only one cell at this node -> nothing to average
    {
      int cellgrp = cell_list[0].group;
      int elem = cell_list[0].elem;
      const XFieldCellGroupType& xfldCellGroup = xfld.template getCellGroup<Topology>(cellgrp);
      ElementXFieldClass xfldElem(xfldCellGroup.basis() );
      xfldCellGroup.getElement(xfldElem,elem);

      xfldElem.impliedMetric(implied_metric.DOF(node));
    }
    else //more than one cell around this node
    {
      int nCells = (int) cell_list.size();
      std::vector<MatrixSym> metric_list(nCells);
      std::vector<Real> weight_list(nCells, 1.0/((Real)nCells));

      for (int i = 0; i < nCells; i++)
      {
        int cellgrp = cell_list[i].group;
        int elem = cell_list[i].elem;
        const XFieldCellGroupType& xfldCellGroup = xfld.template getCellGroup<Topology>(cellgrp);
        ElementXFieldClass xfldElem(xfldCellGroup.basis() );
        xfldCellGroup.getElement(xfldElem,elem);

        xfldElem.impliedMetric(metric_list[i]);
      }

      implied_metric.DOF(node) = Metric::computeLogEuclideanAvg(metric_list, weight_list);
    }

    // compute the log of the metric
    logMfld.DOF(node) = log(implied_metric.DOF(node));
  } //loop over nodes

  int nDOF0 = 1;
  CostModel_LogEuclidean<PhysDim, TopoDim> costmodel(implied_metric, cellgroup_list);

  Real Cost0;
  std::vector<MatrixSym> dCost_dlogM0(nNode);

  costmodel.computeCost_logM(nDOF0, logMfld, Cost0, dCost_dlogM0);

  SANS_CHECK_CLOSE( Cost0, (Real) nElem*nDOF0, small_tol, close_tol );

  std::vector<MatrixSym> dCost_dlogM_dummy;
  Real delta[2] = {1e-2, 1e-3};
  Real err_appox[2];

  // Perturbed log metric fields
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> logMfld_p(logMfld, FieldCopy());
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> logMfld_m(logMfld, FieldCopy());

  for (int node = 0; node < nNode; node++)
  {
    for (int d = 0; d < PhysDim::D; d++)
    {
      for (int j = 0; j < 2; j++)
      {
        // perturb
        logMfld_p.DOF(node)(d,d) += delta[j];
        logMfld_m.DOF(node)(d,d) -= delta[j];

        Real Costp, Costm;
        costmodel.computeCost_logM(nDOF0, logMfld_p, Costp, dCost_dlogM_dummy);
        costmodel.computeCost_logM(nDOF0, logMfld_m, Costm, dCost_dlogM_dummy);

        // de-perturb
        logMfld_p.DOF(node)(d,d) -= delta[j];
        logMfld_m.DOF(node)(d,d) += delta[j];

        Real grad_approx = (Costp - Costm)/(2.0*delta[j]);
        err_appox[j] = fabs(grad_approx - dCost_dlogM0[node](d,d));
      }
      SANS_CHECK_PING_ORDER(err_appox, delta, small_tol);
    }

    //Off-diagonals should be zero
    if (PhysDim::D == 2)
    {
      BOOST_CHECK_EQUAL( 0.0, dCost_dlogM0[node](1,0) );
    }
    else if (PhysDim::D == 3)
    {
      BOOST_CHECK_EQUAL( 0.0, dCost_dlogM0[node](1,0) );
      BOOST_CHECK_EQUAL( 0.0, dCost_dlogM0[node](2,0) );
      BOOST_CHECK_EQUAL( 0.0, dCost_dlogM0[node](2,1) );
    }
    else if (PhysDim::D == 4)
    {
      BOOST_CHECK_EQUAL( 0.0, dCost_dlogM0[node](1,0) );
      BOOST_CHECK_EQUAL( 0.0, dCost_dlogM0[node](2,0) );
      BOOST_CHECK_EQUAL( 0.0, dCost_dlogM0[node](2,1) );
      BOOST_CHECK_EQUAL( 0.0, dCost_dlogM0[node](3,0) );
      BOOST_CHECK_EQUAL( 0.0, dCost_dlogM0[node](3,1) );
      BOOST_CHECK_EQUAL( 0.0, dCost_dlogM0[node](3,2) );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CostModel_LogEuclidean_1D_Line )
{
  XField1D xfld(4);
  pingCostModel_S<PhysD1, TopoD1, Line>(xfld);
  pingCostModel_logM<PhysD1, TopoD1, Line>(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CostModel_LogEuclidean_2D_4Triangle )
{
  XField2D_4Triangle_X1_1Group xfld;
  pingCostModel_S<PhysD2, TopoD2, Triangle>(xfld);
  pingCostModel_logM<PhysD2, TopoD2, Triangle>(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CostModel_LogEuclidean_2D_UnionJack_Triangle )
{
  XField2D_Box_UnionJack_Triangle_X1 xfld(2,2);
  pingCostModel_S<PhysD2, TopoD2, Triangle>(xfld);
  pingCostModel_logM<PhysD2, TopoD2, Triangle>(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CostModel_LogEuclidean_2D_Box_Quad )
{
  XField2D_Box_Quad_X1 xfld(2,3);
  pingCostModel_S<PhysD2, TopoD2, Quad>(xfld);
  pingCostModel_logM<PhysD2, TopoD2, Quad>(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CostModel_LogEuclidean_3D_6Tet )
{
  XField3D_6Tet_X1_1Group xfld;
  pingCostModel_S<PhysD3, TopoD3, Tet>(xfld);
  pingCostModel_logM<PhysD3, TopoD3, Tet>(xfld);
}

#ifdef SANS_AVRO
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CostModel_LogEuclidean_4D )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());
  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld(comm,{2,2,2,2});
  pingCostModel_S<PhysD4, TopoD4, Pentatope>(xfld);
  pingCostModel_logM<PhysD4, TopoD4, Pentatope>(xfld);
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
