// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// MOESS_SANS_Optimizer_MMA_btest
// testing of the home grown MOESS MMA optimizer class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>
#include <array>

#include "tools/linspace.h"

#include "Adaptation/MOESS/SolverInterfaceBase.h"
#include "Adaptation/MOESS/MMA/MOESS_SANS_Optimizer_MMA.h"

// Use NLopt as the verification as the MMA algorithm is shamelessly stolen from there...
#include "Adaptation/MOESS/NLOPT/MOESS_SANS_Optimizer_NLOPT.h"

#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldSpacetime_CG_Cell.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
}

using namespace SANS;

#define SANS_CHECK_PING_ORDER( err_vec, delta_vec, small_tol ) \
    if (std::abs(err_vec[1]) > small_tol) \
    { \
      Real rate = log(err_vec[1]/err_vec[0])/log(delta_vec[1]/delta_vec[0]); \
      BOOST_CHECK_MESSAGE( rate >= 1.9 && rate <= 3.7, "Rate check failed: rate = " << rate << \
                           ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" ); \
    }

template <class PhysDim, class TopoDim>
class DummyProblem : public SolverInterfaceBase<PhysDim, TopoDim>
{
public:

  explicit DummyProblem(const XField<PhysDim, TopoDim>& xfld, const int order) : order_(order), xfld_(xfld) {}

  virtual ~DummyProblem() {}

  virtual void solveGlobalPrimalProblem() override {}
  virtual void solveGlobalAdjointProblem() override {}
  virtual void computeErrorEstimates() override {}

  virtual Real getElementalErrorEstimate(int cellgroup, int elem) const override { return 1.0; }
  virtual Real getGlobalErrorEstimate() const override { return 1.0; }
  virtual Real getGlobalErrorIndicator() const override { return 1.0; }

  virtual LocalSolveStatus solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& local_xfld, std::vector<Real>& local_error) const override
  {
    std::pair<int,int> cellmap = local_xfld.getGlobalCellMap({0, 0});
    int global_cellgrp  = cellmap.first;
    int global_cellelem = cellmap.second;

    int cellID = xfld_.cellIDs(global_cellgrp)[global_cellelem];

    // make sure the error varies "randomly" throughout the grid so parallel test is more rigorous
    local_error[0] = 0.5 + cellID;

    return LocalSolveStatus(true, 1);
  }

  virtual Real getnDOFperCell(const int cellgroup) const override
  {
    int nDOF = 0;

         if (PhysDim::D == 1)
      nDOF = order_ + 1;
    else if (PhysDim::D == 2)
      nDOF = (order_ + 1)*(order_ + 2)/2;
    else if (PhysDim::D == 3)
      nDOF = (order_ + 1)*(order_ + 2)*(order_ + 3)/6;

    return nDOF;
  }

  virtual int getSolutionOrder(const int cellgroup) const override { return order_; }

  virtual Real getOutput() const override { return 0; }
  virtual void output_EField(const std::string& filename ) const override {}

  virtual int getCost() const override {return 0;}
  SpaceType spaceType() const override {return SpaceType::Discontinuous; }

protected:
  const int order_;
  const XField<PhysDim, TopoDim>& xfld_;
};


//############################################################################//
BOOST_AUTO_TEST_SUITE( MOESS_MMA_test_suite )

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
void MMA_NLopt_Equiv(MOESS_SANS_Optimizer<PhysDim, TopoDim, MMA>& optimizer_MMA,
                     MOESS_SANS_Optimizer<PhysDim, TopoDim, NLOPT>& optimizer_NLopt,
                     const Real close_tol,
                     const Real small_tol)
{
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;
  typedef NodalMetrics<PhysDim,TopoDim> NodalMetricsType;

  //--------
  std::shared_ptr<NodalMetricsType> nodalMetrics_MMA = optimizer_MMA.nodalMetrics();

  //Get nodal metric request for parallelized mesh
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> metric_req_MMA(nodalMetrics_MMA->xfld_linear(), 1, BasisFunctionCategory_Lagrange);
  nodalMetrics_MMA->getNodalMetricRequestField(metric_req_MMA);
  //--------

  //--------
  std::shared_ptr<NodalMetricsType> nodalMetrics_NLopt = optimizer_NLopt.nodalMetrics();

  // Field on all ranks (all elements on all ranks)
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> metric_req_NLopt(nodalMetrics_NLopt->xfld_linear(), 1, BasisFunctionCategory_Hierarchical);
  nodalMetrics_NLopt->getNodalMetricRequestField(metric_req_NLopt);
  //--------

  BOOST_REQUIRE_EQUAL(metric_req_MMA.nDOF(), metric_req_NLopt.nDOF());
  int nNode = metric_req_MMA.nDOF();

  // check that the parallelized metrics match the serial metrics (full mesh)
  for (int node = 0; node < nNode; node++)
    for (int i = 0; i < MatrixSym::SIZE; i++)
    {
      //std::cout << rank << " " << native_node
      //          << " serial " << metric_req_MMA.DOF(node).value(i)
      //          << " parallel " << optimizer_NLopt.DOF(node).value(i) << std::endl;

      SANS_CHECK_CLOSE( metric_req_MMA.DOF(node).value(i),
                        metric_req_NLopt.DOF(node).value(i), small_tol, close_tol );
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Ping_MOESS_SANS_Optimizer_1D_Line )
{
  typedef PhysD1 PhysDim;
  typedef TopoD1 TopoDim;
  typedef ErrorModel<PhysDim,TopoDim> ErrorModelType;

  const Real close_tol = 0.1;
  const Real small_tol = 1e-10;

  XField1D xfld(4);
  const Real h_domain_max = 1;

  std::vector<int> cellGroups = {0};

  Real targetCost = 16;

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(xfld, order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.None;
  MOESSParams::checkInputs(paramsDict);

  // construct an error model and perform the synthesis
  std::shared_ptr<ErrorModelType> errorModel = std::make_shared<ErrorModelType>(xfld, cellGroups, dummyProblem, paramsDict);
  errorModel->synthesize(xfld);

  MOESS_SANS_Optimizer<PhysDim, TopoDim, MMA> optimizer_MMA(targetCost, h_domain_max, errorModel, xfld, cellGroups, dummyProblem, paramsDict);

  MOESS_SANS_Optimizer<PhysDim, TopoDim, NLOPT> optimizer_NLopt(targetCost, h_domain_max, errorModel, xfld, cellGroups, dummyProblem, paramsDict);

  MMA_NLopt_Equiv(optimizer_MMA, optimizer_NLopt, close_tol, small_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Ping_MOESS_SANS_Optimizer_2D_Triangle )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef ErrorModel<PhysDim,TopoDim> ErrorModelType;

  const Real close_tol = 0.18;
  const Real small_tol = 1e-10;

  XField2D_4Triangle_X1_1Group xfld;
  const Real h_domain_max = 2;

  std::vector<int> cellGroups = {0};

  Real targetCost = 24;

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(xfld, order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.None;
  MOESSParams::checkInputs(paramsDict);

  // construct an error model and perform the synthesis
  std::shared_ptr<ErrorModelType> errorModel = std::make_shared<ErrorModelType>(xfld, cellGroups, dummyProblem, paramsDict);
  errorModel->synthesize(xfld);

  MOESS_SANS_Optimizer<PhysDim, TopoDim, MMA> optimizer_MMA(targetCost, h_domain_max, errorModel, xfld, cellGroups, dummyProblem, paramsDict);

  MOESS_SANS_Optimizer<PhysDim, TopoDim, NLOPT> optimizer_NLopt(targetCost, h_domain_max, errorModel, xfld, cellGroups, dummyProblem, paramsDict);

  MMA_NLopt_Equiv(optimizer_MMA, optimizer_NLopt, close_tol, small_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Ping_MOESS_SANS_Optimizer_3D_Tet )
{
  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;
  typedef ErrorModel<PhysDim,TopoDim> ErrorModelType;

  const Real close_tol = 9e-2;
  const Real small_tol = 1e-10;

  XField3D_6Tet_X1_1Group xfld;
  const Real h_domain_max = 1;

  std::vector<int> cellGroups = {0};

  Real targetCost = 48;

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(xfld, order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.None;
  MOESSParams::checkInputs(paramsDict);

  // construct an error model and perform the synthesis
  std::shared_ptr<ErrorModelType> errorModel = std::make_shared<ErrorModelType>(xfld, cellGroups, dummyProblem, paramsDict);
  errorModel->synthesize(xfld);

  MOESS_SANS_Optimizer<PhysDim, TopoDim, MMA> optimizer_MMA(targetCost, h_domain_max, errorModel, xfld, cellGroups, dummyProblem, paramsDict);

  MOESS_SANS_Optimizer<PhysDim, TopoDim, NLOPT> optimizer_NLopt(targetCost, h_domain_max, errorModel, xfld, cellGroups, dummyProblem, paramsDict);

  MMA_NLopt_Equiv(optimizer_MMA, optimizer_NLopt, close_tol, small_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Parallel_Serial_Equiv_SANS_2D_Triangle )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;
  typedef ErrorModel<PhysDim,TopoDim> ErrorModelType;
  typedef NodalMetrics<PhysDim,TopoDim> NodalMetricsType;

  const Real close_tol = 0.2;
  const Real small_tol = 1e-12;

  mpi::communicator world;

  int ii = 4, jj = 5;
  XField2D_Box_Triangle_Lagrange_X1 xfld_parallel(world, ii, jj);

  XField2D_Box_Triangle_Lagrange_X1 xfld_serial(world.split(world.rank()), ii, jj);

  Real targetCost = ii*jj*2*3* 1.5; // increase cost by %50
  Real h_domain_max = 1;

  std::vector<int> cellGroups = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem_parallel(xfld_parallel, order);
  DummyProblem<PhysDim,TopoDim> dummyProblem_serial(xfld_serial, order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  MOESSParams::checkInputs(paramsDict);

  //--------
  // construct a parallel error model and perform the synthesis
  std::shared_ptr<ErrorModelType> errorModel_parallel = std::make_shared<ErrorModelType>(xfld_parallel, cellGroups,
                                                                                         dummyProblem_parallel, paramsDict);
  errorModel_parallel->synthesize(xfld_parallel);

  MOESS_SANS_Optimizer<PhysDim, TopoDim, MMA> optimizer_parallel(targetCost, h_domain_max,
                                                                 errorModel_parallel,
                                                                 xfld_parallel, cellGroups, dummyProblem_parallel, paramsDict);

  std::shared_ptr<NodalMetricsType> nodalMetrics_parallel = optimizer_parallel.nodalMetrics();

  //Get nodal metric request for parallelized mesh
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> metric_req_parallel(xfld_parallel, 1, BasisFunctionCategory_Lagrange, cellGroups);
  nodalMetrics_parallel->getNodalMetricRequestField(metric_req_parallel);
  //--------

  world.barrier();

  //--------
  // construct a serial error model and perform the synthesis
  std::shared_ptr<ErrorModelType> errorModel_serial = std::make_shared<ErrorModelType>(xfld_serial, cellGroups,
                                                                                       dummyProblem_serial, paramsDict);
  errorModel_serial->synthesize(xfld_serial);

  MOESS_SANS_Optimizer<PhysDim, TopoDim, MMA> optimizer_serial(targetCost, h_domain_max,
                                                               errorModel_serial,
                                                               xfld_serial, cellGroups, dummyProblem_serial, paramsDict);

  std::shared_ptr<NodalMetricsType> nodalMetrics_serial = optimizer_serial.nodalMetrics();

  // Field on all ranks (all elements on all ranks)
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> metric_req_serial(xfld_serial, 1, BasisFunctionCategory_Hierarchical, cellGroups);
  nodalMetrics_serial->getNodalMetricRequestField(metric_req_serial);
  //--------

  int nNode = metric_req_parallel.nDOF();

  for (int rank = 0; rank < world.size(); rank++)
  {
    world.barrier();
    if (rank != world.rank()) continue;

    // check that the parallelized metrics match the serial metrics (full mesh)
    for (int node = 0; node < nNode; node++)
      for (int i = 0; i < MatrixSym::SIZE; i++)
      {
        int native_node = metric_req_parallel.local2nativeDOFmap(node);

        //std::cout << rank << " " << native_node
        //          << " serial " << metric_req_serial.DOF(native_node).value(i)
        //          << " parallel " << metric_req_parallel.DOF(node).value(i) << std::endl;

        SANS_CHECK_CLOSE( metric_req_serial.DOF(native_node).value(i),
                          metric_req_parallel.DOF(node).value(i), small_tol, close_tol );
      }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
