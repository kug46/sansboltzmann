// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// ErrorModel_Local_btest
// testing of the ErrorModel_Local class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Surreal/SurrealS.h"

#include "Adaptation/MOESS/ErrorModel_Local.h"

#include "Adaptation/MOESS/ReferenceStepMatrix.h"

#include "Field/XFieldLine.h"
#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"
#include "Field/XFieldSpacetime.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include <iostream>
using namespace std;


//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
  typedef ErrorModel_Local<PhysD1> ErrorModel1D;
  typedef ErrorModel_Local<PhysD2> ErrorModel2D;
  typedef ErrorModel_Local<PhysD3> ErrorModel3D;
  typedef ErrorModel_Local<PhysD4> ErrorModel4D;
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorModel_Local_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorModel_Local_1D )
{
  typedef PhysD1 PhysDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> SymMatrix;

  Real tol = 1e-13;

  SymMatrix trueRateMatrix0 = {-0.35};
  SymMatrix trueRateMatrix1 = {-1.74};

  int Nconfig = 4;
  Real error0 = 0.2;
  int order = 3;  //Fairly large order to avoid hitting theoretical limit

  //Some test step matrices
  std::vector<SymMatrix> test_stepmatrix_vec(Nconfig);
  test_stepmatrix_vec[0] = {1.30};
  test_stepmatrix_vec[1] = {0.67};
  test_stepmatrix_vec[2] = {0.98};
  test_stepmatrix_vec[3] = {0.14};

  std::vector<Real> test_error_vec(Nconfig);

  // Manufacture error according to the "true" rate matrices
  test_error_vec[0] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[0]));
  test_error_vec[1] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[1]));
  test_error_vec[2] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[2]));
  test_error_vec[3] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[3]));

  //Estimate rate matrix from error data
  ErrorModel_Local<PhysDim> model0(test_error_vec, test_stepmatrix_vec, error0, order);

  // check that negative error0 throws
  BOOST_CHECK_THROW( ErrorModel1D model0(test_error_vec, test_stepmatrix_vec, -1.0, order), AssertionException );

  SymMatrix RateMatrix0 = model0.getRateMatrix();

  SANS_CHECK_CLOSE( RateMatrix0(0,0), trueRateMatrix0(0,0), tol, tol );

  // Manufacture error ratios according to the "true" rate matrices
  test_error_vec[0] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[0]));
  test_error_vec[1] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[1]));
  test_error_vec[2] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[2]));
  test_error_vec[3] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[3]));

  //Estimate rate matrix from error data
  ErrorModel_Local<PhysDim> model1(test_error_vec, test_stepmatrix_vec, error0, order);
  SymMatrix RateMatrix1 = model1.getRateMatrix();

  SANS_CHECK_CLOSE( RateMatrix1(0,0), trueRateMatrix1(0,0), tol, tol );


  //Get error estimate derivative
  const int size = SymMatrix::SIZE;
  SymMatrix Sval = {-0.84};
  DLA::MatrixSymS<PhysDim::D,SurrealS<size> > S = Sval;
  S(0,0).deriv(0) = 1.0;

  SurrealS<size> error_est;
  model0.getErrorEstimate(S, error_est);

  Real trueErrorEst = error0*exp(DLA::tr(trueRateMatrix0*Sval));
  SymMatrix true_ErrorEst_S = trueErrorEst*trueRateMatrix0;

  SANS_CHECK_CLOSE( error_est.value(), trueErrorEst, tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(0), true_ErrorEst_S(0,0), tol, tol );


  // check with zero error0
  error0 = 0;

  //Estimate rate matrix from error data - for error0 = 0 case (fall back to isotropic rate matrix)
  ErrorModel_Local<PhysDim> model2(test_error_vec, test_stepmatrix_vec, error0, order);
  SymMatrix RateMatrix2 = model2.getRateMatrix();

  SANS_CHECK_CLOSE( RateMatrix2(0,0), 0.0, tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorModel_Local_2D )
{
  typedef PhysD2 PhysDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> SymMatrix;

  Real tol = 1e-12;

  SymMatrix trueRateMatrix0 = {{-0.35},{-0.56, -1.27}};
  SymMatrix trueRateMatrix1 = {{-1.74},{-1.06, -0.75}};

  Real error0 = 0.2;
  int order = 4; //Fairly large order to avoid hitting theoretical limit

  //Some test step matrices
  std::vector<SymMatrix> test_stepmatrix_vec(4);
  test_stepmatrix_vec[0] = {{ 1.30},{ 0.43, 2.30}};
  test_stepmatrix_vec[1] = {{ 0.67},{ 1.20, 0.41}};
  test_stepmatrix_vec[2] = {{ 0.98},{-0.85, 1.10}};
  test_stepmatrix_vec[3] = {{ 0.14},{-0.03, 0.92}};

  std::vector<Real> test_error_vec(4);

  // Manufacture error ratios according to the "true" rate matrices
  test_error_vec[0] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[0]));
  test_error_vec[1] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[1]));
  test_error_vec[2] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[2]));
  test_error_vec[3] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[3]));

  //Estimate rate matrix from error data
  ErrorModel_Local<PhysDim> model0(test_error_vec, test_stepmatrix_vec, error0, order);

  // check that negative error0 throws
  BOOST_CHECK_THROW( ErrorModel2D model0(test_error_vec, test_stepmatrix_vec, -1.0, order), AssertionException );

  SymMatrix RateMatrix0 = model0.getRateMatrix();

  SANS_CHECK_CLOSE( RateMatrix0(0,0), trueRateMatrix0(0,0), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix0(1,0), trueRateMatrix0(1,0), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix0(1,1), trueRateMatrix0(1,1), tol, tol );

  // Manufacture error ratios according to the "true" rate matrices
  test_error_vec[0] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[0]));
  test_error_vec[1] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[1]));
  test_error_vec[2] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[2]));
  test_error_vec[3] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[3]));

  //Estimate rate matrix from error data
  ErrorModel_Local<PhysDim> model1(test_error_vec, test_stepmatrix_vec, error0, order);
  SymMatrix RateMatrix1 = model1.getRateMatrix();

  SANS_CHECK_CLOSE( RateMatrix1(0,0), trueRateMatrix1(0,0), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix1(1,0), trueRateMatrix1(1,0), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix1(1,1), trueRateMatrix1(1,1), tol, tol );


  //Get error estimate derivative
  const int size = SymMatrix::SIZE;
  SymMatrix Sval = {{-0.84},{0.36, 1.57}};
  DLA::MatrixSymS<PhysDim::D,SurrealS<size> > S = Sval;
  S(0,0).deriv(0) = 1.0;
  S(1,0).deriv(1) = 1.0;
  S(1,1).deriv(2) = 1.0;

  SurrealS<size> error_est;
  model0.getErrorEstimate(S, error_est);

  Real trueErrorEst = error0*exp(DLA::tr(trueRateMatrix0*Sval));
  SymMatrix true_ErrorEst_S = trueErrorEst*trueRateMatrix0;
  true_ErrorEst_S(1,0) *= 2; //Off-diagonal terms are multiplied by 2 due to symmetry

  SANS_CHECK_CLOSE( error_est.value() , trueErrorEst        , tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(0), true_ErrorEst_S(0,0), tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(1), true_ErrorEst_S(1,0), tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(2), true_ErrorEst_S(1,1), tol, tol );

  // error estimate with frobeneous norm of the difference in the trace free step matrix
  Real dSt2val = 1.2;
  SurrealS<size> dSt2 = dSt2val;
  model0.getErrorEstimate(S, dSt2, error_est);

  trueErrorEst = error0*exp(DLA::tr(trueRateMatrix0*Sval) + DLA::FrobNorm(trueRateMatrix0)*dSt2val);
  true_ErrorEst_S = trueErrorEst*trueRateMatrix0;
  true_ErrorEst_S(1,0) *= 2; //Off-diagonal terms are multiplied by 2 due to symmetry

  SANS_CHECK_CLOSE( error_est.value() , trueErrorEst        , tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(0), true_ErrorEst_S(0,0), tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(1), true_ErrorEst_S(1,0), tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(2), true_ErrorEst_S(1,1), tol, tol );


  // check with zero error0
  error0 = 0;

  //Estimate rate matrix from error data - for error0 = 0 case (fall back to isotropic rate matrix)
  ErrorModel_Local<PhysDim> model2(test_error_vec, test_stepmatrix_vec, error0, order);
  SymMatrix RateMatrix2 = model2.getRateMatrix();

  SANS_CHECK_CLOSE( RateMatrix2(0,0), 0.0, tol, tol );
  SANS_CHECK_CLOSE( RateMatrix2(1,0), 0.0, tol, tol );
  SANS_CHECK_CLOSE( RateMatrix2(1,1), 0.0, tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorModel_Local_3D )
{
  typedef PhysD3 PhysDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> SymMatrix;

  Real tol = 1e-12;

  //SymMatrix trueRateMatrix0 = {{-0.35},{-0.56, -1.27},{-0.26, -0.72, -0.10}};
  SymMatrix trueRateMatrix0 = {{-0.35},{0.56, -1.27},{-0.26, -0.72, 0.25}};
  SymMatrix trueRateMatrix1 = {{-1.74},{-0.82, -0.75},{-0.34,  0.57, -0.23}};

  Real error0 = 0.2;
  int order = 5; //Fairly large order to avoid hitting theoretical limit

  //Some test step matrices
  std::vector<SymMatrix> test_stepmatrix_vec(7);
  //test_stepmatrix_vec[0] = {{1.30},{ 0.43, 2.30},{ 2.10, 0.10, 1.80}};
  test_stepmatrix_vec[0] = {{1.30},{ 0.43, 2.30},{ 2.10, 0.10, 2.80}};
  test_stepmatrix_vec[1] = {{0.67},{ 1.20, 0.41},{ 0.48, 0.66, 2.30}};
  test_stepmatrix_vec[2] = {{0.98},{-0.85, 1.10},{ 0.00,-0.23, 1.70}};
  test_stepmatrix_vec[3] = {{0.14},{-0.03, 0.92},{ 0.71, 0.99, 0.30}};
  test_stepmatrix_vec[4] = {{2.30},{ 0.51, 1.00},{-0.22, 0.47, 0.33}};
  test_stepmatrix_vec[5] = {{1.13},{ 0.63, 0.32},{ 0.05, 1.39, 1.85}};
  test_stepmatrix_vec[6] = {{0.74},{ 0.83, 2.47},{ 1.31, 0.19, 0.69}};

  std::vector<Real> test_error_vec(7);

  // Manufacture error ratios according to the "true" rate matrices
  test_error_vec[0] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[0]));
  test_error_vec[1] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[1]));
  test_error_vec[2] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[2]));
  test_error_vec[3] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[3]));
  test_error_vec[4] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[4]));
  test_error_vec[5] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[5]));
  test_error_vec[6] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[6]));

  //Estimate rate matrix from error data
  ErrorModel_Local<PhysDim> model0(test_error_vec, test_stepmatrix_vec, error0, order);

  // check that negative error0 throws
  BOOST_CHECK_THROW( ErrorModel3D model0(test_error_vec, test_stepmatrix_vec, -1.0, order), AssertionException );

  SymMatrix RateMatrix0 = model0.getRateMatrix();

  SANS_CHECK_CLOSE( RateMatrix0(0,0), trueRateMatrix0(0,0), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix0(1,0), trueRateMatrix0(1,0), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix0(1,1), trueRateMatrix0(1,1), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix0(2,0), trueRateMatrix0(2,0), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix0(2,1), trueRateMatrix0(2,1), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix0(2,2), trueRateMatrix0(2,2), tol, tol );

  // Manufacture error ratios according to the "true" rate matrices
  test_error_vec[0] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[0]));
  test_error_vec[1] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[1]));
  test_error_vec[2] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[2]));
  test_error_vec[3] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[3]));
  test_error_vec[4] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[4]));
  test_error_vec[5] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[5]));
  test_error_vec[6] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[6]));

  //Estimate rate matrix from error data
  ErrorModel_Local<PhysDim> model1(test_error_vec, test_stepmatrix_vec, error0, order);
  SymMatrix RateMatrix1 = model1.getRateMatrix();

  SANS_CHECK_CLOSE( RateMatrix1(0,0), trueRateMatrix1(0,0), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix1(1,0), trueRateMatrix1(1,0), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix1(1,1), trueRateMatrix1(1,1), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix1(2,0), trueRateMatrix1(2,0), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix1(2,1), trueRateMatrix1(2,1), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix1(2,2), trueRateMatrix1(2,2), tol, tol );


  //Get error estimate derivative
  const int size = SymMatrix::SIZE;
  SymMatrix Sval = {{-0.84},{0.36, 1.57},{1.92,-0.61, 0.22}};
  DLA::MatrixSymS<PhysDim::D,SurrealS<size> > S = Sval;
  S(0,0).deriv(0) = 1.0;
  S(1,0).deriv(1) = 1.0;
  S(1,1).deriv(2) = 1.0;
  S(2,0).deriv(3) = 1.0;
  S(2,1).deriv(4) = 1.0;
  S(2,2).deriv(5) = 1.0;

  SurrealS<size> error_est;
  model0.getErrorEstimate(S, error_est);

  Real trueErrorEst = error0*exp(DLA::tr(trueRateMatrix0*Sval));
  SymMatrix true_ErrorEst_S = trueErrorEst*trueRateMatrix0;
  true_ErrorEst_S(1,0) *= 2; //Off-diagonal terms are multiplied by 2 due to symmetry
  true_ErrorEst_S(2,0) *= 2; //Off-diagonal terms are multiplied by 2 due to symmetry
  true_ErrorEst_S(2,1) *= 2; //Off-diagonal terms are multiplied by 2 due to symmetry

  SANS_CHECK_CLOSE( error_est.value(), trueErrorEst, tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(0), true_ErrorEst_S(0,0), tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(1), true_ErrorEst_S(1,0), tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(2), true_ErrorEst_S(1,1), tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(3), true_ErrorEst_S(2,0), tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(4), true_ErrorEst_S(2,1), tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(5), true_ErrorEst_S(2,2), tol, tol );


  // check with zero error0
  error0 = 0;

  //Estimate rate matrix from error data - for error0 = 0 case (fall back to isotropic rate matrix)
  ErrorModel_Local<PhysDim> model2(test_error_vec, test_stepmatrix_vec, error0, order);
  SymMatrix RateMatrix2 = model2.getRateMatrix();

  SANS_CHECK_CLOSE( RateMatrix2(0,0), 0.0, tol, tol );
  SANS_CHECK_CLOSE( RateMatrix2(1,0), 0.0, tol, tol );
  SANS_CHECK_CLOSE( RateMatrix2(1,1), 0.0, tol, tol );
  SANS_CHECK_CLOSE( RateMatrix2(2,0), 0.0, tol, tol );
  SANS_CHECK_CLOSE( RateMatrix2(2,1), 0.0, tol, tol );
  SANS_CHECK_CLOSE( RateMatrix2(2,2), 0.0, tol, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorModel_Local_4D )
{
  typedef PhysD4 PhysDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> SymMatrix;

  Real tol = 1e-11;

  // negative-definite rate matrices formed by: X = rand(4,4); A = -( X'*X +eye(4) )
  SymMatrix trueRateMatrix1 = {{-2.61},{-0.96, -1.72},{-1.25, -0.87, -2.43}, {-1.34,-0.94,-1.26,-2.37}};
  SymMatrix trueRateMatrix0 = {{-1.76},{-0.33, -2.49},{-0.87,  -1.18, -2.56}, {-0.65,-0.73,-1.12,-1.89}};

  Real error0 = 0.2;
  int order = 5; //Fairly large order to avoid hitting theoretical limit

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  std::vector<LocalSplitConfig> split_config_list = LocalSplitConfigList<Pentatope>::get(false);

  ReferenceStepMatrix<PhysD4, TopoD4> builder(comm_local,split_config_list);

  // test step matrices formed by the splits of the reference element
  std::vector<SymMatrix> test_stepmatrix_vec(10);
  for (std::size_t i=0;i<split_config_list.size();i++)
    test_stepmatrix_vec[i] = builder.getStepMatrix(i);

  std::vector<Real> test_error_vec(10);

  // Manufacture error ratios according to the "true" rate matrices
  test_error_vec[0] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[0]));
  test_error_vec[1] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[1]));
  test_error_vec[2] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[2]));
  test_error_vec[3] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[3]));
  test_error_vec[4] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[4]));
  test_error_vec[5] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[5]));
  test_error_vec[6] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[6]));
  test_error_vec[7] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[7]));
  test_error_vec[8] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[8]));
  test_error_vec[9] = error0*exp(DLA::tr(trueRateMatrix0*test_stepmatrix_vec[9]));

  //Estimate rate matrix from error data
  ErrorModel_Local<PhysDim> model0(test_error_vec, test_stepmatrix_vec, error0, order);

  // check that negative error0 throws
  BOOST_CHECK_THROW( ErrorModel4D model0(test_error_vec, test_stepmatrix_vec, -1.0, order), AssertionException );

  SymMatrix RateMatrix0 = model0.getRateMatrix();

  SANS_CHECK_CLOSE( RateMatrix0(0,0), trueRateMatrix0(0,0), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix0(1,0), trueRateMatrix0(1,0), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix0(1,1), trueRateMatrix0(1,1), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix0(2,0), trueRateMatrix0(2,0), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix0(2,1), trueRateMatrix0(2,1), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix0(2,2), trueRateMatrix0(2,2), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix0(3,0), trueRateMatrix0(3,0), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix0(3,1), trueRateMatrix0(3,1), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix0(3,2), trueRateMatrix0(3,2), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix0(3,3), trueRateMatrix0(3,3), tol, tol );

  // Manufacture error ratios according to the "true" rate matrices
  test_error_vec[0] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[0]));
  test_error_vec[1] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[1]));
  test_error_vec[2] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[2]));
  test_error_vec[3] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[3]));
  test_error_vec[4] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[4]));
  test_error_vec[5] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[5]));
  test_error_vec[6] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[6]));
  test_error_vec[7] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[7]));
  test_error_vec[8] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[8]));
  test_error_vec[9] = error0*exp(DLA::tr(trueRateMatrix1*test_stepmatrix_vec[9]));

  //Estimate rate matrix from error data
  ErrorModel_Local<PhysDim> model1(test_error_vec, test_stepmatrix_vec, error0, order);
  SymMatrix RateMatrix1 = model1.getRateMatrix();

  SANS_CHECK_CLOSE( RateMatrix1(0,0), trueRateMatrix1(0,0), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix1(1,0), trueRateMatrix1(1,0), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix1(1,1), trueRateMatrix1(1,1), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix1(2,0), trueRateMatrix1(2,0), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix1(2,1), trueRateMatrix1(2,1), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix1(2,2), trueRateMatrix1(2,2), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix1(3,0), trueRateMatrix1(3,0), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix1(3,1), trueRateMatrix1(3,1), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix1(3,2), trueRateMatrix1(3,2), tol, tol );
  SANS_CHECK_CLOSE( RateMatrix1(3,3), trueRateMatrix1(3,3), tol, tol );


  //Get error estimate derivative
  const int size = SymMatrix::SIZE;

  // some random step matrix
  SymMatrix Sval = {{-0.84},{0.36, 1.57},{1.92,-0.61, 0.22},{0.11,0.54,0.23,0.89}};
  DLA::MatrixSymS<PhysDim::D,SurrealS<size> > S = Sval;
  S(0,0).deriv(0) = 1.0;
  S(1,0).deriv(1) = 1.0;
  S(1,1).deriv(2) = 1.0;
  S(2,0).deriv(3) = 1.0;
  S(2,1).deriv(4) = 1.0;
  S(2,2).deriv(5) = 1.0;
  S(3,0).deriv(6) = 1.0;
  S(3,1).deriv(7) = 1.0;
  S(3,2).deriv(8) = 1.0;
  S(3,3).deriv(9) = 1.0;

  SurrealS<size> error_est;
  model0.getErrorEstimate(S, error_est);

  Real trueErrorEst = error0*exp(DLA::tr(trueRateMatrix0*Sval));
  SymMatrix true_ErrorEst_S = trueErrorEst*trueRateMatrix0;
  true_ErrorEst_S(1,0) *= 2; //Off-diagonal terms are multiplied by 2 due to symmetry
  true_ErrorEst_S(2,0) *= 2; //Off-diagonal terms are multiplied by 2 due to symmetry
  true_ErrorEst_S(2,1) *= 2; //Off-diagonal terms are multiplied by 2 due to symmetry
  true_ErrorEst_S(3,0) *= 2; //Off-diagonal terms are multiplied by 2 due to symmetry
  true_ErrorEst_S(3,1) *= 2; //Off-diagonal terms are multiplied by 2 due to symmetry
  true_ErrorEst_S(3,2) *= 2; //Off-diagonal terms are multiplied by 2 due to symmetry

  SANS_CHECK_CLOSE( error_est.value(), trueErrorEst, tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(0), true_ErrorEst_S(0,0), tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(1), true_ErrorEst_S(1,0), tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(2), true_ErrorEst_S(1,1), tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(3), true_ErrorEst_S(2,0), tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(4), true_ErrorEst_S(2,1), tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(5), true_ErrorEst_S(2,2), tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(6), true_ErrorEst_S(3,0), tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(7), true_ErrorEst_S(3,1), tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(8), true_ErrorEst_S(3,2), tol, tol );
  SANS_CHECK_CLOSE( error_est.deriv(9), true_ErrorEst_S(3,3), tol, tol );



  // check with zero error0
  error0 = 0;

  //Estimate rate matrix from error data - for error0 = 0 case (fall back to isotropic rate matrix)
  ErrorModel_Local<PhysDim> model2(test_error_vec, test_stepmatrix_vec, error0, order);
  SymMatrix RateMatrix2 = model2.getRateMatrix();

  SANS_CHECK_CLOSE( RateMatrix2(0,0), 0.0, tol, tol );
  SANS_CHECK_CLOSE( RateMatrix2(1,0), 0.0, tol, tol );
  SANS_CHECK_CLOSE( RateMatrix2(1,1), 0.0, tol, tol );
  SANS_CHECK_CLOSE( RateMatrix2(2,0), 0.0, tol, tol );
  SANS_CHECK_CLOSE( RateMatrix2(2,1), 0.0, tol, tol );
  SANS_CHECK_CLOSE( RateMatrix2(2,2), 0.0, tol, tol );
  SANS_CHECK_CLOSE( RateMatrix2(3,0), 0.0, tol, tol );
  SANS_CHECK_CLOSE( RateMatrix2(3,1), 0.0, tol, tol );
  SANS_CHECK_CLOSE( RateMatrix2(3,2), 0.0, tol, tol );
  SANS_CHECK_CLOSE( RateMatrix2(3,3), 0.0, tol, tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
