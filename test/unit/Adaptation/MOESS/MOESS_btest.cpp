// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// MOESS_btest
// testing of the MOESS main class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>
#include <fstream>

#include "Adaptation/MOESS/MOESS.h"

#include "Field/FieldArea_CG_Cell.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
  typedef MOESS<PhysD1, TopoD1> MOESS1D;
  typedef MOESS<PhysD2, TopoD2> MOESS2D;
  typedef MOESS<PhysD3, TopoD3> MOESS3D;
  typedef MOESS<PhysD4, TopoD4> MOESS4D;
}

using namespace SANS;

namespace  //no-name namespace local to this file
{

template <class PhysDim, class TopoDim>
class DummyProblem : public SolverInterfaceBase<PhysDim, TopoDim>
{

public:

  explicit DummyProblem(int order) : order_(order) {}

  virtual ~DummyProblem() {}

  virtual void solveGlobalPrimalProblem() override {}
  virtual void solveGlobalAdjointProblem() override {}
  virtual void computeErrorEstimates() override {}

  virtual Real getElementalErrorEstimate(int cellgroup, int elem) const override { return 1.0; }
  virtual Real getGlobalErrorEstimate() const override { return 1.0; }
  virtual Real getGlobalErrorIndicator() const override { return 1.0; }

  virtual LocalSolveStatus solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& local_xfld, std::vector<Real>& local_error) const override
  {
    local_error[0] = 0.5;
    return LocalSolveStatus(true, 1);
  }

  virtual Real getnDOFperCell(const int cellgroup) const override
  {
    int nDOF = 0;

         if (PhysDim::D == 1)
      nDOF = order_ + 1;
    else if (PhysDim::D == 2)
      nDOF = (order_ + 1)*(order_ + 2)/2;
    else if (PhysDim::D == 3)
      nDOF = (order_ + 1)*(order_ + 2)*(order_ + 3)/6;
    else if (PhysDim::D == 3)
      nDOF = (order_ + 1)*(order_ + 2)*(order_ + 3)*(order_ + 4)/24;
    else
      SANS_DEVELOPER_EXCEPTION("not ready for D > 4.");

    return nDOF;
  }

  virtual int getSolutionOrder(const int cellgroup) const override { return order_; }

  virtual Real getOutput() const override { return 0; }
  virtual void output_EField(const std::string& filename ) const override {}

  virtual int getCost() const override {return 0;}

  virtual SpaceType spaceType() const override { return SpaceType::Discontinuous; }
protected:
  int order_;
};


template <class PhysDim, class TopoDim>
class DummyEdgeProblem : public SolverInterfaceBase<PhysDim, TopoDim>
{

public:

  explicit DummyEdgeProblem(int order) : order_(order) {}

  virtual ~DummyEdgeProblem() {}

  virtual void solveGlobalPrimalProblem() override {}
  virtual void solveGlobalAdjointProblem() override {}
  virtual void computeErrorEstimates() override {}

  virtual Real getElementalErrorEstimate(int cellgroup, int elem) const override { return 1.0; }
  virtual Real getGlobalErrorEstimate() const override { return 1.0; }
  virtual Real getGlobalErrorIndicator() const override { return 1.0; }

  virtual LocalSolveStatus solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& local_xfld, std::vector<Real>& local_error) const override
  {
    for (std::size_t i = 0; i < local_error.size(); i++)
      local_error[i] = 0.5 + 0.1*(int)i;
    return LocalSolveStatus(true, 1);
  }

  virtual Real getnDOFperCell(const int cellgroup) const override
  {
    int nDOF = 0;

    if (PhysDim::D == 1)
      nDOF = order_ + 1;
    else if (PhysDim::D == 2)
      nDOF = (order_ + 1)*(order_ + 2)/2;
    else if (PhysDim::D == 3)
      nDOF = (order_ + 1)*(order_ + 2)*(order_ + 3)/6;
    else if (PhysDim::D == 4)
      nDOF = (order_ + 1)*(order_ + 2)*(order_ + 3)*(order_ + 4)/24;
    else
      SANS_DEVELOPER_EXCEPTION("not ready for D > 4.");

    return nDOF;
  }

  virtual int getSolutionOrder(const int cellgroup) const override { return order_; }

  virtual Real getOutput() const override { return 0; }
  virtual void output_EField(const std::string& filename ) const override {}

  virtual int getCost() const override {return 0;}

  virtual SpaceType spaceType() const override { return SpaceType::Discontinuous; }
protected:
  int order_;
};

} //namespace

//############################################################################//
BOOST_AUTO_TEST_SUITE( MOESS_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MOESS_VolmeWeighted_ImpliedMetric_2D_Triangle )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-6;

  XField2D_4Triangle_X1_1Group xfld;

  typedef XField2D_4Triangle_X1_1Group::FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef XFieldCellGroupType::ElementType<> ElementXFieldClass;

  const XFieldCellGroupType& xfldCellGroup = xfld.getCellGroup<Triangle>(0);
  ElementXFieldClass xfldElem(xfldCellGroup.basis() );
  xfldCellGroup.getElement(xfldElem,0);

  // Implied metric of one element
  MatrixSym M0;
  xfldElem.impliedMetric(M0);

  std::vector<int> cellGroups = {0};

  Real dummy_targetCost = 1;

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  paramsDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.None;
  MOESSParams::checkInputs(paramsDict);

  MOESS<PhysDim, TopoDim> MOESSClass(xfld, cellGroups, dummyProblem, dummy_targetCost, paramsDict);

  //Get nodal metric request for new mesh
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> metric_req(xfld, 1, BasisFunctionCategory_Hierarchical, cellGroups);
  MOESSClass.getNodalMetricRequestField(metric_req);

  int nNode = xfld.nDOF();

  // all metrics should be identical for a uniform mesh
  for (int node = 0; node < nNode; node++)
    for (int i = 0; i < MatrixSym::SIZE; i++)
      SANS_CHECK_CLOSE( M0.value(i), metric_req.DOF(node).value(i), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MOESS_Optimized_ImpliedMetric_2D_Triangle )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-6;

  XField2D_4Triangle_X1_1Group xfld;

  typedef XField2D_4Triangle_X1_1Group::FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef XFieldCellGroupType::ElementType<> ElementXFieldClass;

  const XFieldCellGroupType& xfldCellGroup = xfld.getCellGroup<Triangle>(0);
  ElementXFieldClass xfldElem(xfldCellGroup.basis() );
  xfldCellGroup.getElement(xfldElem,0);

  // Implied metric of one element
  MatrixSym M0;
  xfldElem.impliedMetric(M0);

  std::vector<int> cellGroups = {0};

  Real dummy_targetCost = 1;

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;
  paramsDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.None;
  MOESSParams::checkInputs(paramsDict);

  MOESS<PhysDim, TopoDim> MOESSClass(xfld, cellGroups, dummyProblem, dummy_targetCost, paramsDict);

  //Get nodal metric request for new mesh
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> metric_req(xfld, 1, BasisFunctionCategory_Hierarchical, cellGroups);
  MOESSClass.getNodalMetricRequestField(metric_req);

  int nNode = xfld.nDOF();

  // all metrics should be identical for a uniform mesh
  for (int node = 0; node < nNode; node++)
    for (int i = 0; i < MatrixSym::SIZE; i++)
      SANS_CHECK_CLOSE( M0.value(i), metric_req.DOF(node).value(i), small_tol, close_tol );
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MOESS_GradationPenalty_1D_Line )
{
  typedef PhysD1 PhysDim;
  typedef TopoD1 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  const Real small_tol = 1e-12;
  const Real close_tol = 6e-7;

  XField1D xfld(4);
  // Change the coordinates so the grid is not uniform
  xfld.DOF(1) = 0.1;
  xfld.DOF(2) = 0.3;
  xfld.DOF(3) = 0.6;

  std::vector<int> cellGroups = {0};

  Real dummy_targetCost = 1;

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  // Use a small gradation factor so the penalty kicks in
  paramsDict[MOESSParams::params.GradationFactor] = 1.01;
  paramsDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.None;
  MOESSParams::checkInputs(paramsDict);

  MOESS<PhysDim, TopoDim> MOESSClass(xfld, cellGroups, dummyProblem, dummy_targetCost, paramsDict);

  int nNode = xfld.nDOF();

  MatrixSym S0 = {{0.26}};

  //Step matrices at each node
  std::vector<MatrixSym> nodalStepMatrices(nNode, S0);

  Real penalty0 = 0;
  std::vector<MatrixSym> dpenalty_dS0(nNode, 0.0);

  MOESSClass.computeGradationPenalty(nodalStepMatrices, penalty0, dpenalty_dS0);

  std::vector<MatrixSym> dpenalty_dS_dummy(nNode, 0.0);
  Real delta = 1e-7;

  for (int node = 0; node < nNode; node++)
  {
    for (int k = 0; k < MatrixSym::SIZE; k++)
    {
      std::vector<MatrixSym> Svec_p(nNode, S0);
      std::vector<MatrixSym> Svec_m(nNode, S0);

      Svec_p[node].value(k) += delta;
      Svec_m[node].value(k) -= delta;

      Real penaltyp = 0, penaltym = 0;
      MOESSClass.computeGradationPenalty(Svec_p, penaltyp, dpenalty_dS_dummy);
      MOESSClass.computeGradationPenalty(Svec_m, penaltym, dpenalty_dS_dummy);

      Real derivative_approx = (penaltyp - penaltym)/(2.0*delta);
      SANS_CHECK_CLOSE( derivative_approx, dpenalty_dS0[node].value(k), small_tol, close_tol );
      //std::cout << "node " << node << ", "<< derivative_approx << ", " << dpenalty_dS0[node].value(k) << std::endl;
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MOESS_GradationPenalty_2D_Triangle )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  const Real small_tol = 1e-12;
  const Real close_tol = 0.005;

  XField2D_4Triangle_X1_1Group xfld;
  // Change the coordinates so the grid is not uniform
  xfld.DOF(3) = { 1.5, 2.0};
  xfld.DOF(4) = {-0.6, 0.4};
  xfld.DOF(5) = { 0.7,-0.3};

  std::vector<int> cellGroups = {0};

  Real dummy_targetCost = 10;

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  // Use a small gradation factor so the penalty kicks in
  paramsDict[MOESSParams::params.GradationFactor] = 1.1;
  paramsDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.None;
  MOESSParams::checkInputs(paramsDict);

  MOESS<PhysDim, TopoDim> MOESSClass(xfld, cellGroups, dummyProblem, dummy_targetCost, paramsDict);

  int nNode = xfld.nDOF();

  MatrixSym S0 = {{0.23},{-0.47, 1.38}};

  //Step matrices at each node
  std::vector<MatrixSym> nodalStepMatrices(nNode, S0);

  Real penalty0 = 0;
  std::vector<MatrixSym> dpenalty_dS0(nNode, 0.0);

  MOESSClass.computeGradationPenalty(nodalStepMatrices, penalty0, dpenalty_dS0);

  std::vector<MatrixSym> dpenalty_dS_dummy(nNode, 0.0);
  Real delta = 1e-6;

  for (int node = 0; node < nNode; node++)
  {
    for (int k = 0; k < MatrixSym::SIZE; k++)
    {
      std::vector<MatrixSym> Svec_p(nNode, S0);
      std::vector<MatrixSym> Svec_m(nNode, S0);

      Svec_p[node].value(k) += delta;
      Svec_m[node].value(k) -= delta;

      Real penaltyp = 0, penaltym = 0;
      MOESSClass.computeGradationPenalty(Svec_p, penaltyp, dpenalty_dS_dummy);
      MOESSClass.computeGradationPenalty(Svec_m, penaltym, dpenalty_dS_dummy);

      Real derivative_approx = (penaltyp - penaltym)/(2.0*delta);
      SANS_CHECK_CLOSE( derivative_approx, dpenalty_dS0[node].value(k), small_tol, close_tol );
      //std::cout << derivative_approx << ", " << dpenalty_dS0[node].value(k) << std::endl<<std::endl;
    }
  }
}
#endif


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MOESS_2D_Triangle_Parallel_FrobNorm )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  mpi::communicator world;

  XField2D_Box_Triangle_Lagrange_X1 xfld(world, 3, 3);

  std::vector<int> cellGroups = {0};

  Real dummy_targetCost = 40;

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.FrobNorm;
  MOESSParams::checkInputs(paramsDict);

  MOESS<PhysDim, TopoDim> MOESSClass(xfld, cellGroups, dummyProblem, dummy_targetCost, paramsDict);

  //Get nodal metric request for new mesh
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> metric_req(xfld, 1, BasisFunctionCategory_Hierarchical, cellGroups);
  MOESSClass.getNodalMetricRequestField(metric_req);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MOESS_2D_Triangle_Parallel_SANSparallel )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  mpi::communicator world;

  XField2D_Box_Triangle_Lagrange_X1 xfld(world, 3, 3);

  std::vector<int> cellGroups = {0};

  Real dummy_targetCost = 40;

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.Progressbar;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;
  paramsDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.SANSparallel;
  MOESSParams::checkInputs(paramsDict);

  MOESS<PhysDim, TopoDim> MOESSClass(xfld, cellGroups, dummyProblem, dummy_targetCost, paramsDict);

  //Get nodal metric request for new mesh
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> metric_req(xfld, 1, BasisFunctionCategory_Hierarchical, cellGroups);
  MOESSClass.getNodalMetricRequestField(metric_req);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MOESS_PrintInfo )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;

  Real dummy_targetCost = 500;

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.Progressbar;
  paramsDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.None;
  MOESSParams::checkInputs(paramsDict);

  std::string adapthist_filename = "tmp/test.adapthist";
  std::fstream fadapthist( adapthist_filename, fstream::out );

  std::string objhist_filename = "tmp/optinfo.dat";
  std::fstream fobjhist( objhist_filename, fstream::out );

  for (int iter = 0; iter < 3; iter++)
  {
    int ii = iter + 2;
    XField2D_Box_Triangle_X1 xfld(ii, ii);

    std::vector<int> cellGroups = {0};

    MOESS<PhysDim, TopoDim> MOESSClass(xfld, cellGroups, dummyProblem, dummy_targetCost, paramsDict);

    MOESSClass.printAdaptInfo(fadapthist, iter, dummyProblem.getGlobalErrorIndicator(), dummyProblem.getGlobalErrorEstimate(), 1.0, 100.0);
    MOESSClass.printOptimizationInfo(fobjhist);
  }

  fadapthist.close();
  fobjhist.close();

  //Delete the written files
  std::remove(adapthist_filename.c_str());
  std::remove(objhist_filename.c_str());
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
