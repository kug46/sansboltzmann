// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// MOESS_Gradation_Optimizer_NLOPT_btest
// testing of the MOESS NLOPT optimizer class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

//#include "Adaptation/MOESS/NLOPT/MOESS_Gradation_Optimizer_NLOPT.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
}

using namespace SANS;

#define SANS_CHECK_PING_ORDER( err_vec, delta_vec, small_tol ) \
    if (std::abs(err_vec[1]) > small_tol) \
    { \
      Real rate = log(err_vec[1]/err_vec[0])/log(delta_vec[1]/delta_vec[0]); \
      BOOST_CHECK_MESSAGE( rate >= 1.8 && rate <= 5.0, "Rate check failed: rate = " << rate << \
                           ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" ); \
    }

namespace  //no-name namespace local to this file
{

#if 0
template <class PhysDim, class TopoDim>
class DummyProblem : public SolverInterfaceBase<PhysDim, TopoDim>
{

public:

  explicit DummyProblem(int order) : order_(order) {}

  virtual ~DummyProblem() {}

  virtual void solveGlobalPrimalProblem() override {}
  virtual void solveGlobalAdjointProblem() override {}
  virtual void computeErrorEstimates() override {}

  virtual Real getElementalErrorEstimate(int cellgroup, int elem) const override { return 1.0; }
  virtual Real getGlobalErrorEstimate() const override { return 1.0; }
  virtual Real getGlobalErrorIndicator() const override { return 1.0; }

  virtual LocalSolveStatus solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& local_xfld, std::vector<Real>& local_error) const override
  {
    local_error[0] = 0.5;
    return LocalSolveStatus(true, 1);
  }

  virtual Real getnDOFperCell(const int cellgroup) const override
  {
    int nDOF = 0;

         if (PhysDim::D == 1)
      nDOF = order_ + 1;
    else if (PhysDim::D == 2)
      nDOF = (order_ + 1)*(order_ + 2)/2;
    else if (PhysDim::D == 3)
      nDOF = (order_ + 1)*(order_ + 2)*(order_ + 3)/6;

    return nDOF;
  }

  virtual int getSolutionOrder(const int cellgroup) const override { return order_; }

  virtual Real getOutput() const override { return 0; }
  virtual void output_EField(const std::string& filename ) const override {}

  virtual int getCost() const override {return 0;}
  SpaceType spaceType() const override {return SpaceType::Discontinuous; }

protected:
  int order_;
};
#endif

} // namespace

//############################################################################//
BOOST_AUTO_TEST_SUITE( MOESS_test_suite )

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Ping_MOESS_Gradation_Optimizer_1D_Line )
{
  typedef PhysD1 PhysDim;
  typedef TopoD1 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  const Real small_tol = 1e-10;

  XField1D xfld(4);
  // Change the coordinates so the grid is not uniform
  xfld.DOF(1) = 0.1;
  xfld.DOF(2) = 0.3;
  xfld.DOF(3) = 0.6;

  std::vector<int> cellGroups = {0};

  Real targetCost = 16;

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.Gradation;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  // Use a small gradation factor so the penalty kicks in
  paramsDict[MOESSParams::params.GradationFactor] = 1.1;
  MOESSParams::checkInputs(paramsDict);

  MOESS<PhysDim, TopoDim> MOESSClass(xfld, cellGroups, dummyProblem, targetCost, paramsDict);

  int nNode = xfld.nDOF();
  int nDim = nNode*MatrixSym::SIZE;
  MOESS_Gradation_Optimizer<MOESS<PhysDim, TopoDim>, NLOPT> optimizer(MOESSClass, nNode);

  std::vector<MatrixSym> Svec(xfld.nDOF(), 0);
  optimizer.optimize(Svec);

  std::vector<Real> x = {0.1, -0.2, 0.5, 1.1, -0.7};

  BOOST_REQUIRE_EQUAL( nDim, x.size() );

  std::vector<Real> grad_obj(nDim, 0.0);
  std::vector<Real> grad_cost(nDim, 0.0);
  //std::vector<Real> grad_eig(nDim, 0.0);
  std::vector<Real> grad_gradation(nDim, 0.0);
  std::vector<Real> grad_frobnorm(nDim, 0.0);
  Real delta[2] = {1e-2, 1e-3};

  optimizer.NLOPT_Objective(nDim, x.data(), grad_obj.data(), (void*) &optimizer);
  optimizer.NLOPT_CostConstraint(nDim, x.data(), grad_cost.data(), (void*) &optimizer);
  //optimizer.NLOPT_EigenValueConstraint(nDim, x.data(), grad_eig.data(), (void*) &optimizer);
  optimizer.NLOPT_GradationConstraint(nDim, x.data(), grad_gradation.data(), (void*) &optimizer);
  optimizer.NLOPT_FrobNormSqSumConstraint(nDim, x.data(), grad_frobnorm.data(), (void*) &optimizer);

  for (int i = 0; i < nDim; i++)
  {
    Real err_obj[2], err_cost[2], err_gradation[2], err_frobnorm[2]; //, err_eig[2]

    for (int j = 0; j < 2; j++)
    {
      std::vector<Real> xp = x;
      std::vector<Real> xm = x;

      Real fp, fm, grad_approx;

      xp[i] += delta[j];
      xm[i] -= delta[j];

      //Ping objective function
      fp = optimizer.NLOPT_Objective(nDim, xp.data(), nullptr, (void*) &optimizer);
      fm = optimizer.NLOPT_Objective(nDim, xm.data(), nullptr, (void*) &optimizer);
      grad_approx = (fp - fm)/(2.0*delta[j]);
      err_obj[j] = fabs(grad_approx - grad_obj[i]);

      //Ping cost constraint
      fp = optimizer.NLOPT_CostConstraint(nDim, xp.data(), nullptr, (void*) &optimizer);
      fm = optimizer.NLOPT_CostConstraint(nDim, xm.data(), nullptr, (void*) &optimizer);
      grad_approx = (fp - fm)/(2.0*delta[j]);
      err_cost[j] = fabs(grad_approx - grad_cost[i]);

      //Ping eigenvalue constraint
      //fp = optimizer.NLOPT_EigenValueConstraint(nDim, xp.data(), nullptr, (void*) &optimizer);
      //fm = optimizer.NLOPT_EigenValueConstraint(nDim, xm.data(), nullptr, (void*) &optimizer);
      //grad_approx = (fp - fm)/(2.0*delta[j]);
      //err_eig[j] = fabs(grad_approx - grad_eig[i]);

      //Ping gradation constraint
      fp = optimizer.NLOPT_GradationConstraint(nDim, xp.data(), nullptr, (void*) &optimizer);
      fm = optimizer.NLOPT_GradationConstraint(nDim, xm.data(), nullptr, (void*) &optimizer);
      grad_approx = (fp - fm)/(2.0*delta[j]);
      err_gradation[j] = fabs(grad_approx - grad_gradation[i]);

      //Ping Frobenius norm constraint
      fp = optimizer.NLOPT_FrobNormSqSumConstraint(nDim, xp.data(), nullptr, (void*) &optimizer);
      fm = optimizer.NLOPT_FrobNormSqSumConstraint(nDim, xm.data(), nullptr, (void*) &optimizer);
      grad_approx = (fp - fm)/(2.0*delta[j]);
      err_frobnorm[j] = fabs(grad_approx - grad_frobnorm[i]);
    }

    SANS_CHECK_PING_ORDER(err_obj, delta, small_tol);
    SANS_CHECK_PING_ORDER(err_cost, delta, small_tol);
    //SANS_CHECK_PING_ORDER(err_eig, delta, small_tol);
    SANS_CHECK_PING_ORDER(err_gradation, delta, small_tol);
    SANS_CHECK_PING_ORDER(err_frobnorm, delta, small_tol);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Ping_MOESS_Gradation_Optimizer_2D_Triangle )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  const Real small_tol = 5e-9;

  XField2D_4Triangle_X1_1Group xfld;
  // Change the coordinates so the grid is not uniform
  xfld.DOF(3) = { 1.5, 2.0};
  xfld.DOF(4) = {-0.6, 0.4};
  xfld.DOF(5) = { 0.7,-0.3};

  std::vector<int> cellGroups = {0};

  Real targetCost = 24;

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.Gradation;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  // Use a small gradation factor so the penalty kicks in
  paramsDict[MOESSParams::params.GradationFactor] = 1.1;
  MOESSParams::checkInputs(paramsDict);

  MOESS<PhysDim, TopoDim> MOESSClass(xfld, cellGroups, dummyProblem, targetCost, paramsDict);

  int nNode = xfld.nDOF();
  int nDim = nNode*MatrixSym::SIZE;
  MOESS_Gradation_Optimizer<MOESS<PhysDim, TopoDim>, NLOPT> optimizer(MOESSClass, nNode);

  std::vector<MatrixSym> Svec(xfld.nDOF(), 0);
  optimizer.optimize(Svec);

  std::vector<Real> x = { 0.1, -0.2,  0.5,
                          0.3, -0.7,  0.4,
                         -0.2,  0.3,  0.6,
                         -0.4,  0.6,  0.5,
                          0.4, -0.1,  0.3,
                         -0.6,  0.5,  0.7};

  BOOST_REQUIRE_EQUAL( nDim, x.size() );

  std::vector<Real> grad_obj(nDim, 0.0);
  std::vector<Real> grad_cost(nDim, 0.0);
  //std::vector<Real> grad_eig(nDim, 0.0);
  std::vector<Real> grad_gradation(nDim, 0.0);
  std::vector<Real> grad_frobnorm(nDim, 0.0);
  Real delta[2] = {1e-2, 1e-3};

  optimizer.NLOPT_Objective(nDim, x.data(), grad_obj.data(), (void*) &optimizer);
  optimizer.NLOPT_CostConstraint(nDim, x.data(), grad_cost.data(), (void*) &optimizer);
  //optimizer.NLOPT_EigenValueConstraint(nDim, x.data(), grad_eig.data(), (void*) &optimizer);
  optimizer.NLOPT_GradationConstraint(nDim, x.data(), grad_gradation.data(), (void*) &optimizer);
  optimizer.NLOPT_FrobNormSqSumConstraint(nDim, x.data(), grad_frobnorm.data(), (void*) &optimizer);

  for (int i = 0; i < nDim; i++)
  {
    Real err_obj[2], err_cost[2], err_gradation[2], err_frobnorm[2]; //, err_eig[2]

    for (int j = 0; j < 2; j++)
    {
      std::vector<Real> xp = x;
      std::vector<Real> xm = x;

      Real fp, fm, grad_approx;

      xp[i] += delta[j];
      xm[i] -= delta[j];

      //Ping objective function
      fp = optimizer.NLOPT_Objective(nDim, xp.data(), nullptr, (void*) &optimizer);
      fm = optimizer.NLOPT_Objective(nDim, xm.data(), nullptr, (void*) &optimizer);
      grad_approx = (fp - fm)/(2.0*delta[j]);
      err_obj[j] = fabs(grad_approx - grad_obj[i]);

      //Ping cost constraint
      fp = optimizer.NLOPT_CostConstraint(nDim, xp.data(), nullptr, (void*) &optimizer);
      fm = optimizer.NLOPT_CostConstraint(nDim, xm.data(), nullptr, (void*) &optimizer);
      grad_approx = (fp - fm)/(2.0*delta[j]);
      err_cost[j] = fabs(grad_approx - grad_cost[i]);

      //Ping eigenvalue constraint
      //fp = optimizer.NLOPT_EigenValueConstraint(nDim, xp.data(), nullptr, (void*) &optimizer);
      //fm = optimizer.NLOPT_EigenValueConstraint(nDim, xm.data(), nullptr, (void*) &optimizer);
      //grad_approx = (fp - fm)/(2.0*delta[j]);
      //err_eig[j] = fabs(grad_approx - grad_eig[i]);

      //Ping gradation constraint
      fp = optimizer.NLOPT_GradationConstraint(nDim, xp.data(), nullptr, (void*) &optimizer);
      fm = optimizer.NLOPT_GradationConstraint(nDim, xm.data(), nullptr, (void*) &optimizer);
      grad_approx = (fp - fm)/(2.0*delta[j]);
      err_gradation[j] = fabs(grad_approx - grad_gradation[i]);

      //Ping Frobenius norm constraint
      fp = optimizer.NLOPT_FrobNormSqSumConstraint(nDim, xp.data(), nullptr, (void*) &optimizer);
      fm = optimizer.NLOPT_FrobNormSqSumConstraint(nDim, xm.data(), nullptr, (void*) &optimizer);
      grad_approx = (fp - fm)/(2.0*delta[j]);
      err_frobnorm[j] = fabs(grad_approx - grad_frobnorm[i]);
    }

    SANS_CHECK_PING_ORDER(err_obj, delta, small_tol);
    SANS_CHECK_PING_ORDER(err_cost, delta, small_tol);
    //SANS_CHECK_PING_ORDER(err_eig, delta, small_tol);
    SANS_CHECK_PING_ORDER(err_gradation, delta, small_tol);
    SANS_CHECK_PING_ORDER(err_frobnorm, delta, small_tol);
  }
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Ping_MOESS_Gradation_Optimizer_3D_Tet )
{
  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  Real small_tol = 1e-10;

  XField3D_6Tet_X1_1Group xfld;
  // Change the coordinates so the grid is not uniform
  xfld.DOF(0) = {-0.1, 0.0, 0.2};
  xfld.DOF(3) = { 1.2, 0.9,-0.2};
  xfld.DOF(5) = { 1.3, 0.0,-0.1};

  std::vector<int> cellGroups = {0};

  Real targetCost = 48;

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.Gradation;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  // Use a small gradation factor so the penalty kicks in
  paramsDict[MOESSParams::params.GradationFactor] = 2;
  MOESSParams::checkInputs(paramsDict);

  MOESS<PhysDim, TopoDim> MOESSClass(xfld, cellGroups, dummyProblem, targetCost, paramsDict);

  int nNode = xfld.nDOF();
  int nDim = nNode*MatrixSym::SIZE;
  MOESS_Gradation_Optimizer<MOESS<PhysDim, TopoDim>, NLOPT> optimizer(MOESSClass, nNode);

  std::vector<MatrixSym> Svec(xfld.nDOF(), 0);
  optimizer.optimize(Svec);

  std::vector<Real> x = { 0.1, -0.2,  0.7,  0.3, -0.4,  0.8,
                          0.3, -0.7,  0.4, -0.1,  0.9, -0.5,
                         -0.2,  0.3,  0.6,  0.0,  0.1,  0.9,
                         -0.4,  0.6,  0.7, -0.6,  0.3,  0.2,
                          0.4,  0.1,  0.3,  0.8,  0.7, -0.5,
                         -0.6,  0.5,  0.7, -0.2,  0.8,  0.3,
                          0.5,  0.4, -0.2,  0.7,  0.2,  0.0,
                          0.7,  0.3,  0.1, -0.5,  0.4,  0.6 };

  BOOST_REQUIRE_EQUAL( nDim, x.size() );

  std::vector<Real> grad_obj(nDim, 0.0);
  std::vector<Real> grad_cost(nDim, 0.0);
  //std::vector<Real> grad_eig(nDim, 0.0);
  std::vector<Real> grad_gradation(nDim, 0.0);
  std::vector<Real> grad_frobnorm(nDim, 0.0);
  Real delta[2] = {1e-1, 1e-2};

  optimizer.NLOPT_Objective(nDim, x.data(), grad_obj.data(), (void*) &optimizer);
  optimizer.NLOPT_CostConstraint(nDim, x.data(), grad_cost.data(), (void*) &optimizer);
  //optimizer.NLOPT_EigenValueConstraint(nDim, x.data(), grad_eig.data(), (void*) &optimizer);
  optimizer.NLOPT_GradationConstraint(nDim, x.data(), grad_gradation.data(), (void*) &optimizer);
  optimizer.NLOPT_FrobNormSqSumConstraint(nDim, x.data(), grad_frobnorm.data(), (void*) &optimizer);

  for (int i = 0; i < nDim; i++)
  {
    Real err_obj[2], err_cost[2], err_gradation[2], err_frobnorm[2]; //, err_eig[2]

    for (int j = 0; j < 2; j++)
    {
      std::vector<Real> xp = x;
      std::vector<Real> xm = x;

      Real fp, fm, grad_approx;

      xp[i] += delta[j];
      xm[i] -= delta[j];

      //Ping objective function
      fp = optimizer.NLOPT_Objective(nDim, xp.data(), nullptr, (void*) &optimizer);
      fm = optimizer.NLOPT_Objective(nDim, xm.data(), nullptr, (void*) &optimizer);
      grad_approx = (fp - fm)/(2.0*delta[j]);
      err_obj[j] = fabs(grad_approx - grad_obj[i]);

      //Ping cost constraint
      fp = optimizer.NLOPT_CostConstraint(nDim, xp.data(), nullptr, (void*) &optimizer);
      fm = optimizer.NLOPT_CostConstraint(nDim, xm.data(), nullptr, (void*) &optimizer);
      grad_approx = (fp - fm)/(2.0*delta[j]);
      err_cost[j] = fabs(grad_approx - grad_cost[i]);

      //Ping eigenvalue constraint
      //fp = optimizer.NLOPT_EigenValueConstraint(nDim, xp.data(), nullptr, (void*) &optimizer);
      //fm = optimizer.NLOPT_EigenValueConstraint(nDim, xm.data(), nullptr, (void*) &optimizer);
      //grad_approx = (fp - fm)/(2.0*delta[j]);
      //err_eig[j] = fabs(grad_approx - grad_eig[i]);

      //Ping gradation constraint
      fp = optimizer.NLOPT_GradationConstraint(nDim, xp.data(), nullptr, (void*) &optimizer);
      fm = optimizer.NLOPT_GradationConstraint(nDim, xm.data(), nullptr, (void*) &optimizer);
      grad_approx = (fp - fm)/(2.0*delta[j]);
      err_gradation[j] = fabs(grad_approx - grad_gradation[i]);

      //Ping Frobenius norm constraint
      fp = optimizer.NLOPT_FrobNormSqSumConstraint(nDim, xp.data(), nullptr, (void*) &optimizer);
      fm = optimizer.NLOPT_FrobNormSqSumConstraint(nDim, xm.data(), nullptr, (void*) &optimizer);
      grad_approx = (fp - fm)/(2.0*delta[j]);
      err_frobnorm[j] = fabs(grad_approx - grad_frobnorm[i]);
    }

    //std::cout << "i = " << i <<std::endl;
    SANS_CHECK_PING_ORDER(err_obj, delta, small_tol);
    SANS_CHECK_PING_ORDER(err_cost, delta, small_tol);
    //SANS_CHECK_PING_ORDER(err_eig, delta, small_tol);
    SANS_CHECK_PING_ORDER(err_gradation, delta, small_tol);
    SANS_CHECK_PING_ORDER(err_frobnorm, delta, small_tol);
  }
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
