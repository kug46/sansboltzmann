// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// ImpliedMetric_Optimizer_NLOPT_btest
// testing of the implied metric optimizer class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>
#include <random> // for the initial condition testing

#include "Adaptation/MOESS/NLOPT/ImpliedMetric_Optimizer_NLOPT.h"

#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldSpacetime_CG_Cell.h"

#include "Meshing/libMeshb/XField_libMeshb.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"
#include "unit/UnitGrids/XField4D_24Ptope_X1_1Group.h"

#include "Meshing/EPIC/XField_PX.h"

#include "Meshing/Metric/MetricOps.h"
// #include "Field/output_Tecplot.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
}

using namespace SANS;

#define SANS_CHECK_PING_ORDER( err_vec, delta_vec, small_tol ) \
    if (err_vec[1] > small_tol){ \
      Real rate = log(err_vec[1]/err_vec[0])/log(delta_vec[1]/delta_vec[0]); \
      BOOST_CHECK_MESSAGE( rate >= 1.8 && rate <= 3.7, "Rate check failed: rate = " << rate << \
                           ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" ); \
    }

namespace  //no-name namespace local to this file
{

template <class PhysDim, class TopoDim>
class DummyProblem : public SolverInterfaceBase<PhysDim, TopoDim>
{

public:

  explicit DummyProblem(int order) : order_(order) {}

  virtual ~DummyProblem() {}

  virtual void solveGlobalPrimalProblem() override {}
  virtual void solveGlobalAdjointProblem() override {}
  virtual void computeErrorEstimates() override {}

  virtual Real getElementalErrorEstimate(int cellgroup, int elem) const override { return 1.0; }
  virtual Real getGlobalErrorEstimate() const override { return 1.0; }
  virtual Real getGlobalErrorIndicator() const override { return 1.0; }

  virtual LocalSolveStatus solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& local_xfld, std::vector<Real>& local_error) const override
  {
    local_error[0] = 0.5;
    return LocalSolveStatus(true, 1);
  }

  virtual Real getnDOFperCell(const int cellgroup) const override
  {
    int nDOF = 0;

         if (PhysDim::D == 1)
      nDOF = order_ + 1;
    else if (PhysDim::D == 2)
      nDOF = (order_ + 1)*(order_ + 2)/2;
    else if (PhysDim::D == 3)
      nDOF = (order_ + 1)*(order_ + 2)*(order_ + 3)/6;

    return nDOF;
  }

  virtual int getSolutionOrder(const int cellgroup) const override { return order_; }

  virtual Real getOutput() const override { return 0; }
  virtual void output_EField(const std::string& filename ) const override {}

  virtual int getCost() const override {return 0;}
  SpaceType spaceType() const override {return SpaceType::Discontinuous; }

protected:
  int order_;
};

// an exposed class for unit testing purposes
// want to be able to change the initial condition for the optimizer to test single point convergence
template <class PhysDim, class TopoDim>
struct DummyNodalMetrics : public NodalMetrics<PhysDim,TopoDim>
{
  typedef NodalMetrics<PhysDim,TopoDim> BaseType;
  DummyNodalMetrics( const XField<PhysDim,TopoDim>& xfld_linear,
                    const std::vector<int>& cellgroup_list,
                    const SolverInterfaceBase<PhysDim,TopoDim>& problem,
                    const PyDict& paramsDict ) : BaseType(xfld_linear,cellgroup_list,problem,paramsDict) {}

  using BaseType::implied_metric_; // expose the implied metric
};


} // namespace

//############################################################################//
BOOST_AUTO_TEST_SUITE( MOESS_test_suite )

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
void ping_ImpliedMetric_Optimizer(const std::vector<Real>& x,
                                  const Real small_tol,
                                  ImpliedMetric_Optimizer<PhysDim, TopoDim, NLOPT>& optimizer)
{
  std::size_t nDim = x.size();

  std::vector<Real> grad_obj(nDim, 0.0);
  Real delta[2] = {1e-3, 1e-4};

  optimizer.NLOPT_Objective(nDim, x.data(), grad_obj.data(), (void*) &optimizer);

  // perturbed x values
  std::vector<Real> xp = x;
  std::vector<Real> xm = x;

  Real err_obj[2];
  for (std::size_t i = 0; i < nDim; i++)
  {
    for (int j = 0; j < 2; j++)
    {
      Real fp, fm, grad_approx;

      // perturb
      xp[i] += delta[j];
      xm[i] -= delta[j];

      //Ping objective function
      fp = optimizer.NLOPT_Objective(nDim, xp.data(), nullptr, (void*) &optimizer);
      fm = optimizer.NLOPT_Objective(nDim, xm.data(), nullptr, (void*) &optimizer);

      // de-perturb
      xp[i] -= delta[j];
      xm[i] += delta[j];

      grad_approx = (fp - fm)/(2.0*delta[j]);
      err_obj[j] = fabs(grad_approx - grad_obj[i]);
    }
    SANS_CHECK_PING_ORDER(err_obj, delta, small_tol);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Ping_ImpliedMetric_Optimizer_1D_Line )
{
  typedef PhysD1 PhysDim;
  typedef TopoD1 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  const Real small_tol = 1e-10;

  XField1D xfld(4);
  // Change the coordinates so the grid is not uniform
  xfld.DOF(1) = 0.1;
  xfld.DOF(2) = 0.3;
  xfld.DOF(3) = 0.6;

  std::vector<int> cellGroups = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.ImpliedMetric_XTol_Rel] = 1e-7;
  paramsDict[MOESSParams::params.ImpliedMetric_FTol_Rel] = 1e-7;
  paramsDict[MOESSParams::params.ImpliedMetric_MaxEval] = 500;
  MOESSParams::checkInputs(paramsDict);

  NodalMetrics<PhysDim, TopoDim> nodalMetrics(xfld, cellGroups, dummyProblem, paramsDict);

  int nDim = xfld.nDOF()*MatrixSym::SIZE;
  ImpliedMetric_Optimizer<PhysDim, TopoDim, NLOPT> optimizer(nodalMetrics);

  std::vector<Real> x = {0.1, -0.2, 0.5, 1.1, -0.7};

  BOOST_REQUIRE_EQUAL( nDim, x.size() );

  ping_ImpliedMetric_Optimizer(x, small_tol, optimizer);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ImpliedMetric_Optimizer_1D_Line )
{
  typedef PhysD1 PhysDim;
  typedef TopoD1 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  XField1D xfld(10);
  // Change the coordinates so the grid is not uniform
  for (int i = 0; i < xfld.nDOF(); i++)
    xfld.DOF(i) = i*i*i/pow((xfld.nDOF()-1),3);

  std::vector<int> cellGroups = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.ImpliedMetric_XTol_Rel] = 1e-7;
  paramsDict[MOESSParams::params.ImpliedMetric_FTol_Rel] = 1e-7;
  paramsDict[MOESSParams::params.ImpliedMetric_MaxEval] = 500;
  MOESSParams::checkInputs(paramsDict);

  NodalMetrics<PhysDim, TopoDim> nodalMetrics(xfld, cellGroups, dummyProblem, paramsDict);

  int nDim = xfld.nDOF()*MatrixSym::SIZE;
  ImpliedMetric_Optimizer<PhysDim, TopoDim, NLOPT> optimizer(nodalMetrics);

  const NodalMetrics<PhysDim, TopoDim>::MatrixSymFieldType& logMfld = optimizer.logMfld();

  std::vector<Real> x(nDim, 0);
  std::vector<MatrixSym> dcomplexity_dlogM;

  for (int node = 0; node < xfld.nDOF(); node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      x[node*MatrixSym::SIZE + ind] = logMfld.DOF(node).value(ind);

  Real objective = optimizer.NLOPT_Objective(nDim, x.data(), nullptr, (void*) &optimizer);

  BOOST_CHECK_SMALL( objective, 1e-7 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Ping_ImpliedMetric_Optimizer_2D_Triangle )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  const Real small_tol = 5e-9;

  XField2D_4Triangle_X1_1Group xfld;
  // Change the coordinates so the grid is not uniform
  xfld.DOF(0) = {-0.1, 0.1};
  xfld.DOF(1) = { 2.0,-0.1};
  xfld.DOF(2) = { 0.1, 1.1};
  xfld.DOF(3) = { 1.5, 2.0};
  xfld.DOF(4) = {-0.6, 0.4};
  xfld.DOF(5) = { 0.7,-1.3};

  // // Grids are intentionally not uniform
  // int ii = 2;
  // int jj = 1;
  // std::vector<Real> xvec(ii+1);
  // std::vector<Real> yvec(jj+1);
  //
  // // construct vectors
  // for (int i = 0; i < ii+1; i++)
  //   xvec[i] = i*i/Real(ii*ii);
  //
  // for (int j = 0; j < jj+1; j++)
  //   yvec[j] = 2*j*j/Real(jj*jj);
  //
  // mpi::communicator world;
  // mpi::communicator comm_local = world.split(world.rank());
  // // create identical global mesh on each processors
  // XField2D_Box_Triangle_Lagrange_X1 xfld(comm_local, xvec, yvec);

  std::vector<int> cellGroups = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.Detailed;
  paramsDict[MOESSParams::params.ImpliedMetric_XTol_Rel] = 1e-7;
  paramsDict[MOESSParams::params.ImpliedMetric_FTol_Rel] = 1e-7;
  paramsDict[MOESSParams::params.ImpliedMetric_MaxEval] = 500;
  MOESSParams::checkInputs(paramsDict);

  NodalMetrics<PhysDim, TopoDim> nodalMetrics(xfld, cellGroups, dummyProblem, paramsDict);

  int nDim = xfld.nDOF()*MatrixSym::SIZE;
  ImpliedMetric_Optimizer<PhysDim, TopoDim, NLOPT> optimizer(nodalMetrics);

  std::vector<Real> x = { 0.1, -0.2,  0.5,
                          0.3, -0.7,  0.4,
                         -0.2,  0.3,  0.6,
                         -0.4,  0.6,  0.5,
                          0.4, -0.1,  0.3,
                         -0.6,  0.5,  0.7};

  BOOST_REQUIRE_EQUAL( nDim, (int) x.size() );

  ping_ImpliedMetric_Optimizer(x, small_tol, optimizer);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ImpliedMetric_Optimizer_2D_Triangle )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  // Grids are intentionally not uniform

  int ii = 5;
  int jj = 4;
  std::vector<Real> xvec(ii+1);
  std::vector<Real> yvec(jj+1);

  // construct vectors
  for (int i = 0; i < ii+1; i++)
    xvec[i] = i*i/Real(ii*ii);

  for (int j = 0; j < jj+1; j++)
    yvec[j] = 2*j*j/Real(jj*jj);

  // create identical global mesh on each processors
  XField2D_Box_Triangle_Lagrange_X1 xfld(comm_local, xvec, yvec);

  std::vector<int> cellGroups = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.ImpliedMetric_XTol_Rel] = 1e-7;
  paramsDict[MOESSParams::params.ImpliedMetric_FTol_Rel] = 1e-7;
  paramsDict[MOESSParams::params.ImpliedMetric_MaxEval] = 500;
  MOESSParams::checkInputs(paramsDict);

  DummyNodalMetrics<PhysDim, TopoDim> nodalMetrics(xfld, cellGroups, dummyProblem, paramsDict);

  int nDim = xfld.nDOF()*MatrixSym::SIZE;
  ImpliedMetric_Optimizer<PhysDim, TopoDim, NLOPT> optimizer(nodalMetrics);

  const NodalMetrics<PhysDim, TopoDim>::MatrixSymFieldType& logMfld = optimizer.logMfld();

  // Real complexityTrue =  nodalMetrics.getComplexityTrue();

  std::vector<Real> x(nDim, 0);

  for (int node = 0; node < xfld.nDOF(); node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      x[node*MatrixSym::SIZE + ind] = logMfld.DOF(node).value(ind);

  Real objective = optimizer.NLOPT_Objective(nDim, x.data(), nullptr, (void*) &optimizer);

  // can't exactly get avg quality 1. Thus 1e-2 is hand wavey here
  // BOOST_CHECK_SMALL( 1+objective, 1e-2 );

  // re start the optimization with random initial conditions, then check that the resulting metrics are the same
  // do this by generating random steps and applying them at each node
  std::mt19937_64 rng;
  std::uniform_real_distribution<double> unif(-0.5*log(2),0.5*log(2));

  const Real small_tol = 1e-3;
  const Real close_tol = 1e-3;

  const int nShuffle = 1;
  for (int shuffle = 0; shuffle < nShuffle; shuffle++)
  {
    MatrixSym Srand = 0;
    for (int dof = 0; dof < nodalMetrics.implied_metric_.nDOF(); dof++)
    {
      // compute a random step matrix
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        Srand.value(ind) = unif(rng);

      nodalMetrics.implied_metric_.DOF(dof) = Metric::computeExponentialMap(nodalMetrics.implied_metric_.DOF(dof),Srand);
    }

    // run the optimization using this different initial condition
    ImpliedMetric_Optimizer<PhysDim, TopoDim, NLOPT> optimizer_shuffle(nodalMetrics);

    // now compare the final solution to the original solution
    const NodalMetrics<PhysDim,TopoDim>::MatrixSymFieldType& logMfld_shuffle = optimizer_shuffle.logMfld();

    for (int node = 0; node < xfld.nDOF(); node++)
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        x[node*MatrixSym::SIZE + ind] = logMfld_shuffle.DOF(node).value(ind);

    Real objective_shuffle = optimizer_shuffle.NLOPT_Objective(nDim, x.data(), nullptr, (void*) &optimizer);

    SANS_CHECK_CLOSE(objective,objective_shuffle,small_tol,close_tol);
  }
}



//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ImpliedMetric_Optimizer_ExactSolution_2D_Triangle )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  mpi::communicator world;

  // Grids are intentionally not uniform
  // Can't use because there are more dofs than equations.
  // You can nail the implied, thus the gradient is zero occasionally.
  XField2D_4Triangle_X1_1Group xfld;
  // Change the coordinates so the grid is not uniform
  xfld.DOF(0) = {-0.1, 0.1};
  xfld.DOF(1) = { 2.0,-0.1};
  xfld.DOF(2) = { 0.1, 1.1};
  xfld.DOF(3) = { 1.5, 2.0};
  xfld.DOF(4) = {-0.6, 0.4};
  xfld.DOF(5) = { 0.7,-1.3};


  std::vector<int> cellGroups = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.ImpliedMetric_XTol_Rel] = 1e-7;
  paramsDict[MOESSParams::params.ImpliedMetric_FTol_Rel] = 1e-7;
  paramsDict[MOESSParams::params.ImpliedMetric_MaxEval] = 500;
  MOESSParams::checkInputs(paramsDict);

  NodalMetrics<PhysDim, TopoDim> nodalMetrics(xfld, cellGroups, dummyProblem, paramsDict);

  int nDim = xfld.nDOF()*MatrixSym::SIZE;
  ImpliedMetric_Optimizer<PhysDim, TopoDim, NLOPT> optimizer(nodalMetrics);

  const NodalMetrics<PhysDim, TopoDim>::MatrixSymFieldType& logMfld = optimizer.logMfld();

  // Real complexityTrue =  nodalMetrics.getComplexityTrue();

  std::vector<Real> x(nDim, 0);
  std::vector<MatrixSym> dcomplexity_dlogM;

  for (int node = 0; node < xfld.nDOF(); node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      x[node*MatrixSym::SIZE + ind] = logMfld.DOF(node).value(ind);

  Real objective = optimizer.NLOPT_Objective(nDim, x.data(), nullptr, (void*) &optimizer);

  //Compute the global complexity
  Real complexity;
  nodalMetrics.computeComplexity(logMfld, complexity, dcomplexity_dlogM);

  BOOST_CHECK_SMALL( objective, 1e-7 );
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Ping_ImpliedMetric_Optimizer_3D_Tet )
{
  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  Real small_tol = 1e-10;

  XField3D_6Tet_X1_1Group xfld;
  // Change the coordinates so the grid is not uniform
  xfld.DOF(0) = {-0.1, 0.0, 0.2};
  xfld.DOF(3) = { 1.2, 0.9,-0.2};
  xfld.DOF(5) = { 1.3, 0.0,-0.1};

  std::vector<int> cellGroups = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.ImpliedMetric_XTol_Rel] = 1e-7;
  paramsDict[MOESSParams::params.ImpliedMetric_FTol_Rel] = 1e-7;
  paramsDict[MOESSParams::params.ImpliedMetric_MaxEval] = 500;
  MOESSParams::checkInputs(paramsDict);

  NodalMetrics<PhysDim, TopoDim> nodalMetrics(xfld, cellGroups, dummyProblem, paramsDict);

  int nDim = xfld.nDOF()*MatrixSym::SIZE;
  ImpliedMetric_Optimizer<PhysDim, TopoDim, NLOPT> optimizer(nodalMetrics);

  std::vector<Real> x = { 0.1, -0.2,  0.5,  0.3, -0.4,  0.8,
                          0.3, -0.7,  0.4, -0.1,  0.9, -0.5,
                         -0.2,  0.3,  0.6,  0.0,  0.1,  0.9,
                         -0.4,  0.6,  0.7, -0.6,  0.3,  0.2,
                          0.4,  0.0,  0.3,  0.8,  0.7, -0.5,
                         -0.6,  0.5,  0.7, -0.2,  0.8,  0.3,
                          0.5,  0.4, -0.2,  0.7,  0.2,  0.0,
                          0.7,  0.0,  0.1, -0.5,  0.4,  0.6 };

  BOOST_REQUIRE_EQUAL( nDim, (int) x.size() );

  ping_ImpliedMetric_Optimizer(x, small_tol, optimizer);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ImpliedMetric_Optimizer_3D_Tet )
{
  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  // Grids are intentionally not uniform

  int ii = 2;
  int jj = 3;
  int kk = 4;
  std::vector<Real> xvec(ii+1);
  std::vector<Real> yvec(jj+1);
  std::vector<Real> zvec(kk+1);

  // construct vectors
  for (int i = 0; i < ii+1; i++)
    xvec[i] = i*i/Real(ii*ii);

  for (int j = 0; j < jj+1; j++)
    yvec[j] = 1.5*j*j/Real(jj*jj);

  for (int k = 0; k < kk+1; k++)
    zvec[k] = 2.1*k*k/Real(kk*kk);

  // create identical global mesh on each processors
  XField3D_Box_Tet_X1 xfld(comm_local, xvec, yvec, zvec);

  std::vector<int> cellGroups = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.ImpliedMetric_XTol_Rel] = 1e-7;
  paramsDict[MOESSParams::params.ImpliedMetric_FTol_Rel] = 1e-7;
  paramsDict[MOESSParams::params.ImpliedMetric_MaxEval] = 500;
  MOESSParams::checkInputs(paramsDict);

  DummyNodalMetrics<PhysDim, TopoDim> nodalMetrics(xfld, cellGroups, dummyProblem, paramsDict);

  int nDim = xfld.nDOF()*MatrixSym::SIZE;
  ImpliedMetric_Optimizer<PhysDim, TopoDim, NLOPT> optimizer(nodalMetrics);

  const NodalMetrics<PhysDim, TopoDim>::MatrixSymFieldType& logMfld = optimizer.logMfld();

  // Real complexityTrue =  nodalMetrics.getComplexityTrue();

  std::vector<Real> x(nDim, 0);

  for (int node = 0; node < xfld.nDOF(); node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      x[node*MatrixSym::SIZE + ind] = logMfld.DOF(node).value(ind);

  Real objective = optimizer.NLOPT_Objective(nDim, x.data(), nullptr, (void*) &optimizer);

  // re start the optimization with random initial conditions, then check that the resulting metrics are the same
  // do this by generating random steps and applying them at each node
  std::mt19937_64 rng;
  std::uniform_real_distribution<double> unif(-log(2),log(2));

  const Real small_tol = 1e-3;
  const Real close_tol = 1e-3;

  const int nShuffle = 1;
  for (int shuffle = 0; shuffle < nShuffle; shuffle++)
  {
    MatrixSym Srand = 0;
    for (int dof = 0; dof < nodalMetrics.implied_metric_.nDOF(); dof++)
    {
      // compute a random step matrix
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        Srand.value(ind) = unif(rng);

      nodalMetrics.implied_metric_.DOF(dof) = Metric::computeExponentialMap(nodalMetrics.implied_metric_.DOF(dof),Srand);
    }

    // run the optimization using this different initial condition
    ImpliedMetric_Optimizer<PhysDim, TopoDim, NLOPT> optimizer_shuffle(nodalMetrics);

    // now compare the final solution to the original solution
    const NodalMetrics<PhysDim,TopoDim>::MatrixSymFieldType& logMfld_shuffle = optimizer_shuffle.logMfld();

    for (int node = 0; node < xfld.nDOF(); node++)
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        x[node*MatrixSym::SIZE + ind] = logMfld_shuffle.DOF(node).value(ind);

    Real objective_shuffle = optimizer_shuffle.NLOPT_Objective(nDim, x.data(), nullptr, (void*) &optimizer);

    SANS_CHECK_CLOSE(objective,objective_shuffle,small_tol,close_tol);
  }
}

#endif

#if 0 //def DLA_LAPACK // SVD in error model currently requires lapack
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Ping_ImpliedMetric_Optimizer_4D_Tet )
{
  typedef PhysD4 PhysDim;
  typedef TopoD4 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  Real small_tol = 1e-10;

  XField4D_24Ptope_X1_1Group xfld;
  // Change the coordinates so the grid is not uniform
  xfld.DOF(0)  = {-0.1,  0.0, 0.2, -0.15};
  xfld.DOF(3)  = { 0.1, -0.1, 1.1,  0.9 };
  xfld.DOF(7)  = { 0.0,  1.1, 1.2,  1.3 };
  xfld.DOF(15) = { 1.1,  1.0, 1.1,  1.2 };

  std::vector<int> cellGroups = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  MOESSParams::checkInputs(paramsDict);

  NodalMetrics<PhysDim, TopoDim> nodalMetrics(xfld, cellGroups, dummyProblem, paramsDict);

  int nDim = xfld.nDOF()*MatrixSym::SIZE;
  ImpliedMetric_Optimizer<NodalMetrics<PhysDim, TopoDim>, NLOPT> optimizer(nodalMetrics, nDim);

  std::vector<MatrixSym> logMvec;
  optimizer.optimize(logMvec);

  std::vector<Real> x = { 0.1, -0.2,  0.5,  0.3, -0.4,  0.8,  0.3, -0.7,  0.4, -0.1,
                          0.3, -0.7,  0.4, -0.1,  0.9, -0.5, -0.4,  0.6,  0.7, -0.6,
                         -0.2,  0.3,  0.6,  0.0,  0.1,  0.9,  0.7, -0.6,  0.3,  0.2,
                         -0.4,  0.6,  0.7, -0.6,  0.3,  0.2,  0.7, -0.2,  0.8,  0.3,
                          0.4,  0.0,  0.3,  0.8,  0.7, -0.5,  0.0,  0.1, -0.5,  0.4,
                         -0.6,  0.5,  0.7, -0.2,  0.8,  0.3, -0.2,  0.5,  0.3, -0.4,
                          0.5,  0.4, -0.2,  0.7,  0.2,  0.0,  0.6,  0.0,  0.1,  0.9,
                          0.7,  0.0,  0.1, -0.5,  0.4,  0.6, -0.4,  0.6,  0.7, -0.6,
                          0.1, -0.2,  0.5,  0.3, -0.4,  0.8,  0.3, -0.7,  0.4, -0.1,
                          0.3, -0.7,  0.4, -0.1,  0.9, -0.5, -0.4,  0.6,  0.7, -0.6,
                         -0.2,  0.3,  0.6,  0.0,  0.1,  0.9,  0.7, -0.6,  0.3,  0.2,
                         -0.4,  0.6,  0.7, -0.6,  0.3,  0.2,  0.7, -0.2,  0.8,  0.3,
                          0.4,  0.0,  0.3,  0.8,  0.7, -0.5,  0.0,  0.1, -0.5,  0.4,
                         -0.6,  0.5,  0.7, -0.2,  0.8,  0.3, -0.2,  0.5,  0.3, -0.4,
                          0.5,  0.4, -0.2,  0.7,  0.2,  0.0,  0.6,  0.0,  0.1,  0.9,
                          0.7,  0.0,  0.1, -0.5,  0.4,  0.6, -0.4,  0.6,  0.7, -0.6 };
  std::vector<Real> xp = x;
  std::vector<Real> xm = x;

  BOOST_REQUIRE_EQUAL( nDim, (int) x.size() );

  std::vector<Real> grad_obj(nDim, 0.0);
  Real delta[2] = {1e-1, 1e-2};

  optimizer.NLOPT_Objective(nDim, x.data(), grad_obj.data(), (void*) &optimizer);

  for (int i = 0; i < nDim; i++)
  {
    Real err_obj[2];

    for (int j = 0; j < 2; j++)
    {
      Real fp, fm, grad_approx;

      // perturb
      xp[i] += delta[j];
      xm[i] -= delta[j];

      //Ping objective function
      fp = optimizer.NLOPT_Objective(nDim, xp.data(), nullptr, (void*) &optimizer);
      fm = optimizer.NLOPT_Objective(nDim, xm.data(), nullptr, (void*) &optimizer);

      // de-perturb
      xp[i] -= delta[j];
      xm[i] += delta[j];

      grad_approx = (fp - fm)/(2.0*delta[j]);
      err_obj[j] = fabs(grad_approx - grad_obj[i]);
    }

    SANS_CHECK_PING_ORDER(err_obj, delta, small_tol);
  }
}
#endif // DLA_LAPACK

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()


#if 0 // Used to debug history for metric optimization

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

//############################################################################//
BOOST_AUTO_TEST_SUITE( MOESS_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Ping_ImpliedMetric_Optimizer_3D_Tet )
{
  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  mpi::communicator world;

  XField_PX<PhysD3, TopoD3> xfld(world, "IO/hchf3.q1.grm" );

  std::vector<int> cellGroups = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.ImpliedMetric_MaxEval] = 1000;
  MOESSParams::checkInputs(paramsDict);

  NodalMetrics<PhysDim, TopoDim> nodalMetrics(xfld, cellGroups, dummyProblem, paramsDict);

  int nDim = xfld.nDOF()*MatrixSym::SIZE;
  ImpliedMetric_Optimizer<NodalMetrics<PhysDim, TopoDim>, NLOPT> optimizer(nodalMetrics, nDim);

  optimizer.optimize();
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()

#endif
