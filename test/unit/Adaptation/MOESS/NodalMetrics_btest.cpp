// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// NodalMetrics_btest
// testing of the NodalMetrics class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "Adaptation/MOESS/NodalMetrics.h"

#include "Field/Field_NodalView.h"
#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldSpacetime_CG_Cell.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Quad_X1.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"
#include "unit/UnitGrids/XField_KuhnFreudenthal.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
  typedef NodalMetrics<PhysD1, TopoD1> NodalMetrics1D;
  typedef NodalMetrics<PhysD2, TopoD2> NodalMetrics2D;
  typedef NodalMetrics<PhysD3, TopoD3> NodalMetrics3D;
}

using namespace SANS;

namespace  //no-name namespace local to this file
{

template <class PhysDim, class TopoDim>
class DummyProblem : public SolverInterfaceBase<PhysDim, TopoDim>
{

public:

  explicit DummyProblem(int order) : order_(order) {}

  virtual ~DummyProblem() {}

  virtual void solveGlobalPrimalProblem() override {}
  virtual void solveGlobalAdjointProblem() override {}
  virtual void computeErrorEstimates() override {}

  virtual Real getElementalErrorEstimate(int cellgroup, int elem) const override { return 1.0; }
  virtual Real getGlobalErrorEstimate() const override { return 1.0; }
  virtual Real getGlobalErrorIndicator() const override { return 1.0; }

  virtual LocalSolveStatus solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& local_xfld, std::vector<Real>& local_error) const override
  {
    local_error[0] = 0.5;
    return LocalSolveStatus(true, 1);
  }

  virtual Real getnDOFperCell(const int cellgroup) const override
  {
    int nDOF = 0;

         if (PhysDim::D == 1)
      nDOF = order_ + 1;
    else if (PhysDim::D == 2)
      nDOF = (order_ + 1)*(order_ + 2)/2;
    else if (PhysDim::D == 3)
      nDOF = (order_ + 1)*(order_ + 2)*(order_ + 3)/6;

    return nDOF;
  }

  virtual int getSolutionOrder(const int cellgroup) const override { return order_; }

  virtual Real getOutput() const override { return 0; }
  virtual void output_EField(const std::string& filename ) const override {}

  virtual int getCost() const override {return 0;}

protected:
  int order_;
};

#define SANS_CHECK_PING_ORDER( err_vec, delta_vec, small_tol ) \
    if (err_vec[1] > small_tol){ \
      Real rate = log(err_vec[1]/err_vec[0])/log(delta_vec[1]/delta_vec[0]); \
      BOOST_CHECK_MESSAGE( rate >= 1.8 && rate <= 3.7, "Rate check failed: rate = " << rate << \
                           ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" ); \
    }

} // namespace

//############################################################################//
BOOST_AUTO_TEST_SUITE( MOESS_test_suite )


//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
void pingEdgeLengthDeviation(const XField<PhysDim,TopoDim>& xfld)
{
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  Real small_tol = 1e-10;

  std::vector<int> cellgroup_list = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  MOESSParams::checkInputs(paramsDict);

  NodalMetrics<PhysDim, TopoDim> nodalMetrics(xfld, cellgroup_list, dummyProblem, paramsDict);

  //Metrics at each node
  const Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& implied_metric = nodalMetrics.impliedMetrics();

  //log matrices at each node
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> logMfld(xfld, 1, BasisFunctionCategory_Lagrange, cellgroup_list);

  // compute the log of the metric
  const int nNode = logMfld.nDOF();
  for (int node = 0; node < nNode; node++)
    logMfld.DOF(node) = log(implied_metric.DOF(node));

  std::vector<MatrixSym> dEdgeLengthDeviation_dlogM(nNode, 0.0);
  std::vector<MatrixSym> dEdgeLengthDeviation_dlogM_dummy;
  Real delta[2] = {1e-2, 1e-3};
  Real err_appox[2];

  Real EdgeLenDev = 0;
  nodalMetrics.computeEdgeLengthDeviation(logMfld, EdgeLenDev, dEdgeLengthDeviation_dlogM);

  Real EdgeLenDev2 = 0;
  nodalMetrics.computeEdgeLengthDeviation(logMfld, EdgeLenDev2, dEdgeLengthDeviation_dlogM_dummy);

  // make sure the edge legnth calculation is the same witout gradient
  BOOST_CHECK_CLOSE(EdgeLenDev, EdgeLenDev2, 1e-10);

  // Perturbed log metric fields
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> logMfld_p(logMfld, FieldCopy());
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> logMfld_m(logMfld, FieldCopy());

  for (int node = 0; node < nNode; node++)
  {
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
    {
      for (int j = 0; j < 2; j++)
      {
        // perturb
        logMfld_p.DOF(node).value(ind) += delta[j];
        logMfld_m.DOF(node).value(ind) -= delta[j];

        Real EdgeLenDevp = 0, EdgeLenDevm = 0;
        nodalMetrics.computeEdgeLengthDeviation(logMfld_p, EdgeLenDevp, dEdgeLengthDeviation_dlogM_dummy);
        nodalMetrics.computeEdgeLengthDeviation(logMfld_m, EdgeLenDevm, dEdgeLengthDeviation_dlogM_dummy);

        // de-perturb
        logMfld_p.DOF(node).value(ind) -= delta[j];
        logMfld_m.DOF(node).value(ind) += delta[j];

        Real grad_approx = (EdgeLenDevp - EdgeLenDevm)/(2.0*delta[j]);
        err_appox[j] = fabs(grad_approx - dEdgeLengthDeviation_dlogM[node].value(ind));
      }
      SANS_CHECK_PING_ORDER(err_appox, delta, small_tol);
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingEdgeLengthDeviation_1D_Line )
{
  XField1D xfld(4);
  xfld.DOF(1) += 0.005;
  xfld.DOF(2) -= 0.01;
  pingEdgeLengthDeviation<PhysD1, TopoD1>(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingEdgeLengthDeviation_2D_4Triangle )
{
  XField2D_4Triangle_X1_1Group xfld;
  xfld.DOF(0)[0] -= 0.2;
  xfld.DOF(1)[0] += 0.1;
  xfld.DOF(2)[1] += 0.2;
  xfld.DOF(5)[1] -= 0.5;
  pingEdgeLengthDeviation<PhysD2, TopoD2>(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingEdgeLengthDeviation_2D_UnionJack_Triangle )
{
  XField2D_Box_UnionJack_Triangle_X1 xfld(2,2);
  pingEdgeLengthDeviation<PhysD2, TopoD2>(xfld);
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingEdgeLengthDeviation_2D_Box_Quad )
{
  XField2D_Box_Quad_X1 xfld(2,3);
  pingEdgeLengthDeviation<PhysD2, TopoD2>(xfld);
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingEdgeLengthDeviation_3D_6Tet )
{
  XField3D_6Tet_X1_1Group xfld;
  pingEdgeLengthDeviation<PhysD3, TopoD3>(xfld);
}

#if 0 //def SANS_AVRO
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingEdgeLengthDeviation_4D )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());
  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld(comm,{2,2,2,2});
  pingEdgeLengthDeviation<PhysD4, TopoD4, Pentatope>(xfld);
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( NodalMetrics_EdgeLengthDeviation_2D_Triangle )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  typedef XField<PhysDim, TopoDim>::template FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  XField2D_1Triangle_X1_1Group xfld;

  std::vector<int> cellGroups = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  MOESSParams::checkInputs(paramsDict);

  NodalMetrics<PhysDim, TopoDim> nodalMetrics(xfld, cellGroups, dummyProblem, paramsDict);

  NodalMetrics<PhysDim, TopoDim>::MatrixSymFieldType logMfld(xfld, 1, BasisFunctionCategory_Lagrange, cellGroups);

  const XFieldCellGroupType& xfldCellGroup = xfld.template getCellGroup<Triangle>(0);
  ElementXFieldClass xfldElem(xfldCellGroup.basis() );
  xfldCellGroup.getElement(xfldElem,0);

  MatrixSym M0;
  xfldElem.impliedMetric(M0);

  for (int node = 0; node < logMfld.nDOF(); node++) logMfld.DOF(node) = log(M0);

  Real EdgeLenDev = 0;
  std::vector<MatrixSym> dEdgeLenDev_dlogM;
  nodalMetrics.computeEdgeLengthDeviation(logMfld, EdgeLenDev, dEdgeLenDev_dlogM);

  BOOST_CHECK_EQUAL(0, EdgeLenDev);

  xfld.DOF(1) = {2, 0};

  EdgeLenDev = 0;
  nodalMetrics.computeEdgeLengthDeviation(logMfld, EdgeLenDev, dEdgeLenDev_dlogM);

  // not analytic, just what I got one time running this case...
  BOOST_CHECK_CLOSE(0.44416626494126299, EdgeLenDev, 1e-10);
}


//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
void pingGradationPenalty_logM(const XField<PhysDim,TopoDim>& xfld)
{
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  Real small_tol = 1e-10;

  std::vector<int> cellgroup_list = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  paramsDict[MOESSParams::params.GradationFactor] = 1.5; // tight so pings are non-zero
  MOESSParams::checkInputs(paramsDict);

  NodalMetrics<PhysDim, TopoDim> nodalMetrics(xfld, cellgroup_list, dummyProblem, paramsDict);

  //Metrics at each node
  const Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& implied_metric = nodalMetrics.impliedMetrics();

  //log matrices at each node
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> logMfld(xfld, 1, BasisFunctionCategory_Lagrange, cellgroup_list);

  // compute the log of the metric
  const int nNode = logMfld.nDOF();
  for (int node = 0; node < nNode; node++)
    logMfld.DOF(node) = log(implied_metric.DOF(node));

  std::vector<MatrixSym> dgradation_dlogM(nNode, 0.0);
  std::vector<MatrixSym> dgradation_dlogM_dummy;
  Real delta[2] = {1e-2, 1e-3};
  Real err_appox[2];

  Real gradation = 0;
  nodalMetrics.computeGradationPenalty_logM(logMfld, gradation, dgradation_dlogM);

  Real gradation2 = 0;
  nodalMetrics.computeGradationPenalty_logM(logMfld, gradation2, dgradation_dlogM_dummy);

  // make sure the edge calculation is the same witout gradient
  BOOST_CHECK_CLOSE(gradation, gradation2, 1e-10);

  // Perturbed log metric fields
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> logMfld_p(logMfld, FieldCopy());
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> logMfld_m(logMfld, FieldCopy());

  for (int node = 0; node < nNode; node++)
  {
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
    {
      for (int j = 0; j < 2; j++)
      {
        // perturb
        logMfld_p.DOF(node).value(ind) += delta[j];
        logMfld_m.DOF(node).value(ind) -= delta[j];

        Real gradation_p = 0, gradation_m = 0;
        nodalMetrics.computeGradationPenalty_logM(logMfld_p, gradation_p, dgradation_dlogM_dummy);
        nodalMetrics.computeGradationPenalty_logM(logMfld_m, gradation_m, dgradation_dlogM_dummy);

        // de-perturb
        logMfld_p.DOF(node).value(ind) -= delta[j];
        logMfld_m.DOF(node).value(ind) += delta[j];

        Real grad_approx = (gradation_p - gradation_m)/(2.0*delta[j]);
        err_appox[j] = fabs(grad_approx - dgradation_dlogM[node].value(ind));
      }
      SANS_CHECK_PING_ORDER(err_appox, delta, small_tol);
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingGradationPenalty_logM_1D_Line )
{
  XField1D xfld(4);
  xfld.DOF(1) += 0.05;
  xfld.DOF(2) -= 0.1;
  pingGradationPenalty_logM<PhysD1, TopoD1>(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingGradationPenalty_logM_2D_4Triangle )
{
  XField2D_4Triangle_X1_1Group xfld;
  xfld.DOF(0)[0] -= 0.2;
  xfld.DOF(1)[0] += 0.1;
  xfld.DOF(2)[1] += 0.2;
  xfld.DOF(5)[1] -= 0.5;
  pingGradationPenalty_logM<PhysD2, TopoD2>(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingGradationPenalty_logM_2D_UnionJack_Triangle )
{
  XField2D_Box_UnionJack_Triangle_X1 xfld(2,2);
  xfld.DOF(4)[0] -= 0.2;
  xfld.DOF(4)[1] += 0.2;
  pingGradationPenalty_logM<PhysD2, TopoD2>(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingGradationPenalty_logM_3D_6Tet )
{
  XField3D_6Tet_X1_1Group xfld;
  xfld.DOF(0)[0] -= 0.1;
  xfld.DOF(0)[1] -= 0.12;
  xfld.DOF(0)[2] -= 0.15;
  xfld.DOF(3)[0] += 0.14;
  xfld.DOF(3)[1] += 0.16;
  xfld.DOF(3)[2] -= 0.18;
  xfld.DOF(4)[0] -= 0.11;
  xfld.DOF(4)[1] -= 0.15;
  xfld.DOF(4)[2] += 0.105;
  xfld.DOF(7)[0] += 0.13;
  xfld.DOF(7)[1] += 0.14;
  xfld.DOF(7)[2] += 0.1;
  pingGradationPenalty_logM<PhysD3, TopoD3>(xfld);
}

#if 0 //def SANS_AVRO
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingGradationPenalty_logM_4D )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());
  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld(comm,{2,2,2,2});
  pingGradationPenalty_logM<PhysD4, TopoD4, Pentatope>(xfld);
}
#endif


//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
void pingGradationPenalty_S(const XField<PhysDim,TopoDim>& xfld)
{
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  Real small_tol = 1e-10;

  std::vector<int> cellgroup_list = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  paramsDict[MOESSParams::params.GradationFactor] = 1.5; // tight so pings are non-zero
  MOESSParams::checkInputs(paramsDict);

  NodalMetrics<PhysDim, TopoDim> nodalMetrics(xfld, cellgroup_list, dummyProblem, paramsDict);

  //log matrices at each node
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> Sfld(xfld, 1, BasisFunctionCategory_Lagrange, cellgroup_list);

  //Metrics at each node
  const Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& implied_metric = nodalMetrics.impliedMetrics();

  // compute the log of the metric
  const int nNode = Sfld.nDOF();
  for (int node = 0; node < nNode; node++)
    Sfld.DOF(node) = log(implied_metric.DOF(node))/10.; // "random" value to ping about

  std::vector<MatrixSym> dgradation_dS(nNode, 0.0);
  std::vector<MatrixSym> dgradation_dS_dummy;
  Real delta[2] = {1e-2, 1e-3};
  Real err_appox[2];

  Real gradation = 0;
  nodalMetrics.computeGradationPenalty_S(Sfld, gradation, dgradation_dS);

  Real gradation2 = 0;
  nodalMetrics.computeGradationPenalty_S(Sfld, gradation2, dgradation_dS_dummy);

  // make sure the edge calculation is the same witout gradient
  BOOST_CHECK_CLOSE(gradation, gradation2, 1e-10);

  // Perturbed log metric fields
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> Sfld_p(Sfld, FieldCopy());
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> Sfld_m(Sfld, FieldCopy());

  for (int node = 0; node < nNode; node++)
  {
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
    {
      for (int j = 0; j < 2; j++)
      {
        // perturb
        Sfld_p.DOF(node).value(ind) += delta[j];
        Sfld_m.DOF(node).value(ind) -= delta[j];

        Real gradation_p = 0, gradation_m = 0;
        nodalMetrics.computeGradationPenalty_S(Sfld_p, gradation_p, dgradation_dS_dummy);
        nodalMetrics.computeGradationPenalty_S(Sfld_m, gradation_m, dgradation_dS_dummy);

        // de-perturb
        Sfld_p.DOF(node).value(ind) -= delta[j];
        Sfld_m.DOF(node).value(ind) += delta[j];

        Real grad_approx = (gradation_p - gradation_m)/(2.0*delta[j]);
        err_appox[j] = fabs(grad_approx - dgradation_dS[node].value(ind));
      }
      SANS_CHECK_PING_ORDER(err_appox, delta, small_tol);
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingGradationPenalty_S_1D_Line )
{
  XField1D xfld(4);
  xfld.DOF(1) += 0.05;
  xfld.DOF(2) -= 0.1;
  pingGradationPenalty_S<PhysD1, TopoD1>(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingGradationPenalty_S_2D_4Triangle )
{
  XField2D_4Triangle_X1_1Group xfld;
  xfld.DOF(0)[0] -= 0.2;
  xfld.DOF(1)[0] += 0.1;
  xfld.DOF(2)[1] += 0.2;
  xfld.DOF(5)[1] -= 0.5;
  pingGradationPenalty_S<PhysD2, TopoD2>(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingGradationPenalty_S_2D_UnionJack_Triangle )
{
  XField2D_Box_UnionJack_Triangle_X1 xfld(2,2);
  xfld.DOF(4)[0] -= 0.2;
  xfld.DOF(4)[1] += 0.2;
  pingGradationPenalty_S<PhysD2, TopoD2>(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingGradationPenalty_S_3D_6Tet )
{
  XField3D_6Tet_X1_1Group xfld;
  xfld.DOF(0)[0] -= 0.1;
  xfld.DOF(0)[1] -= 0.12;
  xfld.DOF(0)[2] -= 0.15;
  xfld.DOF(3)[0] += 0.14;
  xfld.DOF(3)[1] += 0.16;
  xfld.DOF(3)[2] -= 0.18;
  xfld.DOF(4)[0] -= 0.11;
  xfld.DOF(4)[1] -= 0.15;
  xfld.DOF(4)[2] += 0.105;
  xfld.DOF(7)[0] += 0.13;
  xfld.DOF(7)[1] += 0.14;
  xfld.DOF(7)[2] += 0.1;
  pingGradationPenalty_S<PhysD3, TopoD3>(xfld);
}

#if 0 //def SANS_AVRO
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingGradationPenalty_S_4D )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());
  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld(comm,{2,2,2,2});
  pingGradationPenalty_S<PhysD4, TopoD4, Pentatope>(xfld);
}
#endif


//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
void pingStepDeviation_logM(const XField<PhysDim,TopoDim>& xfld)
{
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  Real small_tol = 1e-10;

  std::vector<int> cellgroup_list = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  paramsDict[MOESSParams::params.GradationFactor] = 1.5; // tight so pings are non-zero
  MOESSParams::checkInputs(paramsDict);

  NodalMetrics<PhysDim, TopoDim> nodalMetrics(xfld, cellgroup_list, dummyProblem, paramsDict);

  //Metrics at each node
  const Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& implied_metric = nodalMetrics.impliedMetrics();

  //log matrices at each node
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> logMfld(xfld, 1, BasisFunctionCategory_Lagrange, cellgroup_list);

  // compute the log of the metric
  const int nNode = logMfld.nDOF();
  for (int node = 0; node < nNode; node++)
    logMfld.DOF(node) = log(implied_metric.DOF(node));

  std::vector<MatrixSym> dgradation_dlogM(nNode, 0.0);
  std::vector<MatrixSym> dgradation_dlogM_dummy;
  Real delta[2] = {1e-2, 1e-3};
  Real err_appox[2];

  Real StepDev = 0;
  nodalMetrics.computeStepDeviation_logM(logMfld, StepDev, dgradation_dlogM);

  Real StepDev2 = 0;
  nodalMetrics.computeStepDeviation_logM(logMfld, StepDev2, dgradation_dlogM_dummy);

  // make sure the edge calculation is the same witout gradient
  BOOST_CHECK_CLOSE(StepDev, StepDev2, 1e-10);

  // Perturbed log metric fields
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> logMfld_p(logMfld, FieldCopy());
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> logMfld_m(logMfld, FieldCopy());

  for (int node = 0; node < nNode; node++)
  {
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
    {
      for (int j = 0; j < 2; j++)
      {
        // perturb
        logMfld_p.DOF(node).value(ind) += delta[j];
        logMfld_m.DOF(node).value(ind) -= delta[j];

        Real StepDev_p = 0, StepDev_m = 0;
        nodalMetrics.computeStepDeviation_logM(logMfld_p, StepDev_p, dgradation_dlogM_dummy);
        nodalMetrics.computeStepDeviation_logM(logMfld_m, StepDev_m, dgradation_dlogM_dummy);

        // de-perturb
        logMfld_p.DOF(node).value(ind) -= delta[j];
        logMfld_m.DOF(node).value(ind) += delta[j];

        Real grad_approx = (StepDev_p - StepDev_m)/(2.0*delta[j]);
        err_appox[j] = fabs(grad_approx - dgradation_dlogM[node].value(ind));
      }
      SANS_CHECK_PING_ORDER(err_appox, delta, small_tol);
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingStepDeviation_logM_1D_Line )
{
  XField1D xfld(4);
  xfld.DOF(1) += 0.05;
  xfld.DOF(2) -= 0.1;
  pingStepDeviation_logM<PhysD1, TopoD1>(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingStepDeviation_logM_2D_4Triangle )
{
  XField2D_4Triangle_X1_1Group xfld;
  xfld.DOF(0)[0] -= 0.2;
  xfld.DOF(1)[0] += 0.1;
  xfld.DOF(2)[1] += 0.2;
  xfld.DOF(5)[1] -= 0.5;
  pingStepDeviation_logM<PhysD2, TopoD2>(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingStepDeviation_logM_2D_UnionJack_Triangle )
{
  XField2D_Box_UnionJack_Triangle_X1 xfld(2,2);
  xfld.DOF(4)[0] -= 0.2;
  xfld.DOF(4)[1] += 0.2;
  pingStepDeviation_logM<PhysD2, TopoD2>(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingStepDeviation_logM_3D_6Tet )
{
  XField3D_6Tet_X1_1Group xfld;
  xfld.DOF(0)[0] -= 0.1;
  xfld.DOF(0)[1] -= 0.12;
  xfld.DOF(0)[2] -= 0.15;
  xfld.DOF(3)[0] += 0.14;
  xfld.DOF(3)[1] += 0.16;
  xfld.DOF(3)[2] -= 0.18;
  xfld.DOF(4)[0] -= 0.11;
  xfld.DOF(4)[1] -= 0.15;
  xfld.DOF(4)[2] += 0.105;
  xfld.DOF(7)[0] += 0.13;
  xfld.DOF(7)[1] += 0.14;
  xfld.DOF(7)[2] += 0.1;
  pingStepDeviation_logM<PhysD3, TopoD3>(xfld);
}

#if 0 //def SANS_AVRO
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pingStepDeviation_logM_4D )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());
  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld(comm,{2,2,2,2});
  pingStepDeviation_logM<PhysD4, TopoD4, Pentatope>(xfld);
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Parallelized_NodalMetrics_2D_Triangle )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-6;

  mpi::communicator world;

  int ii = 4, jj = 5;
  XField2D_Box_Triangle_Lagrange_X1 xfld_parallel(world, ii, jj);

  XField2D_Box_Triangle_Lagrange_X1 xfld_serial(world.split(world.rank()), ii, jj);

  std::vector<int> cellGroups = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  MOESSParams::checkInputs(paramsDict);

  //--------
  // create a serialize grid on processor 0
  XField<PhysDim,TopoDim> xfld_rank0(xfld_parallel, XFieldBalance::Serial);

  NodalMetrics<PhysDim, TopoDim> nodalMetrics_rank0(xfld_rank0, cellGroups, dummyProblem, paramsDict);
  //--------

  //--------
  // create serial nodal metrics on all processors
  NodalMetrics<PhysDim, TopoDim> nodalMetrics_serial(xfld_serial, cellGroups, dummyProblem, paramsDict);
  //--------

  MatrixSym S0 = {{1}, {-0.1, 2}};
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> Sfld(xfld_serial, 1, BasisFunctionCategory_Lagrange, cellGroups);
  for (int node = 0; node < Sfld.nDOF(); node++)
    Sfld.DOF(node) = S0 * (Real)node/(Real)(Sfld.nDOF()-1);

  nodalMetrics_serial.setNodalStepMatrix(Sfld);
  if (world.rank() == 0) // only set the serialized Svec on rank 0
    nodalMetrics_rank0.setNodalStepMatrix(Sfld);

  // Field on rank0 (all elements on rank0, no elements on other ranks)
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> metric_req_rank0(xfld_rank0, 1, BasisFunctionCategory_Hierarchical, cellGroups);

  // Field on all ranks (all elements on all ranks)
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> metric_req_serial(xfld_serial, 1, BasisFunctionCategory_Hierarchical, cellGroups);

  // extract the requested metric on all processors
  nodalMetrics_serial.getNodalMetricRequestField(metric_req_serial);

  // parallelize the serialized nodal metric request
  NodalMetrics<PhysDim, TopoDim> nodalMetrics_parallel(xfld_parallel, cellGroups, dummyProblem, paramsDict, nodalMetrics_rank0);

  //Get nodal metric request for parallelized mesh
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> metric_req_parallel(xfld_parallel, 1, BasisFunctionCategory_Lagrange, cellGroups);
  nodalMetrics_parallel.getNodalMetricRequestField(metric_req_parallel);

  int nNode = metric_req_parallel.nDOF();

  for (int rank = 0; rank < world.size(); rank++)
  {
    world.barrier();
    if (rank != world.rank()) continue;

    // check that the parallelized metrics match the serial metrics (full mesh)
    for (int node = 0; node < nNode; node++)
      for (int i = 0; i < MatrixSym::SIZE; i++)
      {
        int native_node = metric_req_parallel.local2nativeDOFmap(node);

        //std::cout << rank << " " << native_node
        //          << " serial " << metric_req_serial.DOF(native_node).value(i)
        //          << " parallel " << metric_req_parallel.DOF(node).value(i) << std::endl;

        SANS_CHECK_CLOSE( metric_req_serial.DOF(native_node).value(i),
                          metric_req_parallel.DOF(node).value(i), small_tol, close_tol );
      }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ImpliedMetric_VolumeWeighted_Serial_Parallel_Equivalency )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-10;

  // parallel and serial communicator
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  // use a non-uniform mesh for better testing
  std::vector<Real> xvec = {0, 0.25, 0.75, 1};
  std::vector<Real> yvec = {0, 0.25, 0.75, 1};

  XField2D_Box_Triangle_Lagrange_X1 xfld_parallel( world, xvec, yvec ); // partitioned system
  XField2D_Box_Triangle_Lagrange_X1 xfld_serial( comm, xvec, yvec );  // complete system on all processors

  std::vector<int> cellGroups = {0};

  int order = 1;
  DummyProblem<PhysDim,TopoDim> dummyProblem(order);

  PyDict paramsDict;
  paramsDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.VolumeWeighted;
  paramsDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  paramsDict[MOESSParams::params.Verbosity] = MOESSParams::params.Verbosity.None;
  MOESSParams::checkInputs(paramsDict);

  NodalMetrics<PhysDim, TopoDim> nodalMetrics_parallel(xfld_parallel, cellGroups, dummyProblem, paramsDict);
  NodalMetrics<PhysDim, TopoDim> nodalMetrics_serial(xfld_serial, cellGroups, dummyProblem, paramsDict);

  // Check that the implied metrics are consistent
  const Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& implied_metric_parallel = nodalMetrics_parallel.impliedMetrics();
  const Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& implied_metric_serial = nodalMetrics_serial.impliedMetrics();
  for ( int i = 0; i < implied_metric_parallel.nDOF(); i++ )
  {
    int i_serial = implied_metric_parallel.local2nativeDOFmap(i);
    for ( int j = 0; j < MatrixSym::SIZE; j++ )
      SANS_CHECK_CLOSE( implied_metric_serial.DOF(i_serial).value(j), implied_metric_parallel.DOF(i).value(j), small_tol, close_tol );
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
