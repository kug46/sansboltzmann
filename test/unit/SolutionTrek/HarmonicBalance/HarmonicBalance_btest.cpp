// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PseudoTime_btest
// testing of PseudoTime_btest classes

#include "Python/PyDict.h"

#include <boost/test/unit_test.hpp>
#include <memory> //unique_ptr
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/ForcingFunction1D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"

#include "SolutionTrek/TimeMarching/BackwardsDifference/BDF.h"
#include "SolutionTrek/HarmonicBalance/AlgebraicEquationSet_HarmonicBalance.h"

#include "Field/Field.h"
#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/XField1D/XField1D.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( HB_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HB_test )
{

  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ScalarFunction1D_ConstCosUnsteady SolutionExact;
  //typedef ScalarFunction1D_ConstSineUnsteady SolutionExact;
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;

  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD1, TopoD1>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

//  typedef BDF<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> BDFClass;
  typedef AlgebraicEquationSet_HarmonicBalance<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> HBEqSetClass;

  GlobalTime time(0);

  // PDE
  AdvectiveFlux1D_Uniform adv( 0.1 );

  Real nu = 1;
  ViscousFlux1D_Uniform visc( nu );

  Source1D_UniformGrad source(0.1, 0.2);

  PyDict ConstCosUnsteady;
  ConstCosUnsteady[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.ConstCosUnsteady;
//  SineSineUnsteady[ScalarFunction1D_SineSineUnsteady::ParamsType::params.a] = 1;
//  SineSineUnsteady[ScalarFunction1D_SineSineUnsteady::ParamsType::params.b] = 0.5;

  NDSolutionExact solnExact(time, ConstCosUnsteady);

  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  NDPDEClass pde(time, adv, visc, source, forcingptr);

  // BC

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function] = ConstCosUnsteady;

  PyDict PyBCList;
  PyBCList["TheBCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TheBCName"] = {0, 1};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpace<PhysD1, ErrorClass> NDErrorClass;
  typedef IntegrandCell_DGBR2_Output<NDErrorClass> ErrorIntegrandClass;

  // integrands
  NDErrorClass fcnError(time, solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});


  // Set up Newton Solver
  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  PyDict NonLinearSolverDict;
  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
  NonLinearSolverParam::checkInputs(NonLinearSolverDict);

  int ntimes = 3;
  Real period = 1.0;


  int order = 1;
  int ii = 2; //grid size - using timesteps = grid size

  // grid:
  XField1D xfld( ii );

  typedef Field_DG_Cell<PhysD1, TopoD1, ArrayQ> QfldClass;
  typedef FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> RfldClass;
  typedef Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> LGfldClass;

  typedef std::unique_ptr<PrimalEquationSetClass::BaseType> SpatialEqSetPtr;

  std::vector< SpatialEqSetPtr > spatialeqsets;

  FieldSequence<PhysD1, TopoD1, ArrayQ>
  qflds(ntimes, FieldConstructor<Field_DG_Cell>(), xfld, order, BasisFunctionCategory_Legendre);
  FieldSequence<PhysD1, TopoD1, VectorArrayQ, FieldLift>
  rflds(ntimes, FieldConstructor<FieldLift_DG_Cell>(), xfld, order, BasisFunctionCategory_Legendre);
  FieldSequence<PhysD1, TopoD1, ArrayQ>
  lgflds(ntimes, FieldConstructor<Field_DG_BoundaryTrace>(), xfld, order, BasisFunctionCategory_Legendre);


  std::vector<Real> tol = {1e-10, 1e-10};

  std::vector<Real> tol2;
  for (int i=0; i< ntimes; i++)
    for (int j=0; j < static_cast<int>(tol.size()); j++)
      tol2.push_back(tol[j]);

  QuadratureOrder quadratureOrder( xfld, 3*order+1 );

  for (int j=0; j<ntimes; j++)
  {
    spatialeqsets.push_back(SpatialEqSetPtr(
        new PrimalEquationSetClass(xfld,
                                   static_cast<QfldClass&>(qflds[j]),
                                   static_cast<RfldClass&>(rflds[j]),
                                   static_cast<LGfldClass&>(lgflds[j]),
                                   pde, disc, quadratureOrder, ResidualNorm_Default,
                                   tol, {0}, {0}, PyBCList, BCBoundaryGroups, time) )    );
  }

  qflds = 0;
  rflds = 0;
  lgflds = 0;

  HBEqSetClass hbeqset(xfld, qflds, pde, quadratureOrder, {0}, period, ntimes, time, tol2, spatialeqsets);

  typedef HBEqSetClass::SystemMatrix SystemMatrixClass;
  typedef HBEqSetClass::SystemVector SystemVectorClass;
  NewtonSolver<SystemMatrixClass> Solver( hbeqset, NewtonSolverDict );

  SystemVectorClass ini(hbeqset.vectorStateSize());
  SystemVectorClass sln(hbeqset.vectorStateSize());
  SystemVectorClass slnchk(hbeqset.vectorStateSize());
  SystemVectorClass rsd(hbeqset.vectorEqSize());
  rsd = 0;

  hbeqset.fillSystemVector(ini);
  sln = ini;

  Solver.solve(ini,sln);

  hbeqset.setSolutionField(sln);

  ArrayQ SquareError1 = 0;
  ArrayQ SquareError2 = 0;
  ArrayQ SquareError3 = 0;

  time = 0;
  IntegrateCellGroups<TopoD1>::integrate(
      FunctionalCell_DGBR2( errorIntegrand, SquareError1 ),
      xfld, (qflds[0], rflds[0]), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

  time = period/(Real)(ntimes);
  IntegrateCellGroups<TopoD1>::integrate(
      FunctionalCell_DGBR2( errorIntegrand, SquareError2 ),
      xfld, (qflds[1], rflds[1]), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

  time = 2.*period/(Real)(ntimes);
  IntegrateCellGroups<TopoD1>::integrate(
      FunctionalCell_DGBR2( errorIntegrand, SquareError3 ),
      xfld, (qflds[2], rflds[2]), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

  Real smallTol = 1.0e-11;

  BOOST_CHECK_SMALL(SquareError1, smallTol);
  BOOST_CHECK_SMALL(SquareError2, smallTol);
  BOOST_CHECK_SMALL(SquareError3, smallTol);



}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
