INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

RUN_WITH_MPIEXEC( 1 )

GenerateUnitTests( AdvectionDiffusionLib
                   AnalyticFunctionLib
                   pdeLib
                   DGBR2ADLib
                   DGBR2Lib
                   NonLinearSolverLib
                   UnitGridsLib
                   XField1DLib
                   FieldLib
                   SparseLinAlgLib
                   DenseLinAlgLib
                   BasisFunctionLib
                   QuadratureLib
                   TopologyLib
                   PythonLib
                   toolsLib )
