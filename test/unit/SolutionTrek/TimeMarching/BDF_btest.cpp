// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PseudoTime_btest
// testing of PseudoTime_btest classes

#include "Python/PyDict.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "pde/BCParameters.h" //Neded because a custom BC vector is created here

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"

#include "SolutionTrek/TimeMarching/BackwardsDifference/BDF.h"

#include "Meshing/XField1D/XField1D.h"

// This is only here because RiccatiEquation is special
#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

// This is only here because RiccatiEquation is special
#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"

#include "unit/SolutionTrek/Ricatti_btest.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BDF_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BDF_test )
{
  PyDict NonLinearSolverDict, NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NonLinearSolverParam::params.NonLinearSolver.Solver] = NonLinearSolverParam::params.NonLinearSolver.Newton;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
  NonLinearSolverParam::checkInputs(NonLinearSolverDict);

  typedef SANS::RiccatiEquation RiccatiEquation;

  typedef PDENDConvertSpace<PhysD1, RiccatiEquation> NDPDEClass;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, boost::mpl::vector<>, AlgEqSetTraits_Sparse, DGAdv,
                                           XField<PhysD1, TopoD1>> AlgebraicEquationSet_DGClass;

  typedef BDF<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> BDFClass;

  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::MatrixQ<Real> MatrixQ;

  GlobalTime time(0);
  Real a = 2, b = 0; // Set b to 0 so the equation is linear
  NDPDEClass pde(time, a, b);

  XField1D xfld( 1 );

  // Using a P0 DG solution field, as we don't have a 0D ODE solver framework

  // solution
  int order = 0;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

  qfld = 1;

  // lagrange multipliers not actually used either
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre, {} );
  lgfld = 0;

  // empty boundary condition information
  PyDict PyBCList;
  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {1e-12, 1e-12};

  // Create the spatial discretization
  AlgebraicEquationSet_DGClass AlgEqSetSpace(xfld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_Default, tol,
                                             {0}, { }, PyBCList, BCBoundaryGroups);

  Real dt = 0.1;

  ArrayQ qPast0, qPast1, qPast2, qPast3;
  ArrayQ q, force, rsd, dq;
  MatrixQ A;

  const Real close_tol = 1e-11;
  {
    time = 0;
    int BDForder = 1;
    std::vector<Real> weights = {1/dt, -1/dt};

    // The BDF class
    BDFClass BDF( BDForder, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {0}, AlgEqSetSpace );

    // solution can't be zero
    qPast0 = 1;
    qfld = qPast0;
    BDF.setqfldPast(0, qfld); // set time - 0*dt
    BOOST_CHECK_CLOSE(qPast0, BDF.qfldpast(0).DOF(0), close_tol);

    // Compute the forcing function
    force = weights[1]*qPast0;

    // solution can't be zero
    q = 1.1;
    qfld = q;

    // Manually advance the solution
    rsd = (q*weights[0] - a*q + force);
    A   = weights[0] - a;
    dq  = rsd/A;
    q  -= dq;

    // Advance the solution one step
    BDF.march(1);

    BOOST_CHECK_CLOSE(q, qfld.DOF(0), close_tol);
    BOOST_CHECK_EQUAL(dt, time);

    // rotate the solution
    qPast0 = q;

    BOOST_CHECK_CLOSE(qPast0, BDF.qfldpast(0).DOF(0), close_tol);

    // recompute the force
    force = weights[1]*qPast0;

    // Manually advance the solution
    rsd = (q*weights[0] - a*q + force);
    A   = weights[0] - a;
    dq  = rsd/A;
    q  -= dq;

    // Advance the solution one step again
    BDF.march(1);

    BOOST_CHECK_CLOSE(q, qfld.DOF(0), close_tol);
    BOOST_CHECK_EQUAL(2*dt, time);

    // rotate the solution
    qPast0 = q;

    BOOST_CHECK_CLOSE(qPast0, BDF.qfldpast(0).DOF(0), close_tol);
  }

  {
    time = 0;
    int BDForder = 2;
    std::vector<Real> weights = {1.5/dt, -2/dt, 0.5/dt};

    // The BDF class
    BDFClass BDF( BDForder, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {0}, AlgEqSetSpace );

    // solution can't be zero
    qPast1 = 1;
    qfld = qPast1;
    BDF.setqfldPast(1, qfld); // set time - 1*dt
    BOOST_CHECK_CLOSE(qPast1, BDF.qfldpast(1).DOF(0), close_tol);

    // solution can't be zero
    qPast0 = 1.1;
    qfld = qPast0;
    BDF.setqfldPast(0, qfld); // set time - 0*dt
    BOOST_CHECK_CLOSE(qPast0, BDF.qfldpast(0).DOF(0), close_tol);

    // solution can't be zero
    q = 1.2;
    qfld = q;

    for (int step = 0; step < 3; step++)
    {
      // Compute the forcing function
      force = weights[1]*qPast0 + weights[2]*qPast1;

      // Manually advance the solution
      rsd = (q*weights[0] - a*q + force);
      A   = weights[0] - a;
      dq  = rsd/A;
      q  -= dq;

      // Advance the solution one step
      BDF.march(1);

      BOOST_CHECK_CLOSE(q, qfld.DOF(0), close_tol);
      BOOST_CHECK_EQUAL(Real(step+1)*dt, time);

      // rotate the solution
      qPast1 = qPast0;
      qPast0 = q;

      BOOST_CHECK_CLOSE(qPast1, BDF.qfldpast(1).DOF(0), close_tol);
      BOOST_CHECK_CLOSE(qPast0, BDF.qfldpast(0).DOF(0), close_tol);
    }
  }


  {
    time = 0;
    int BDForder = 3;
    std::vector<Real> weights = {11./6./dt, -3./dt, 9./6./dt, -1./3./dt};

    // The BDF class
    BDFClass BDF( BDForder, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {0}, AlgEqSetSpace );

    // solution can't be zero
    qPast2 = 1;
    qfld = qPast2;
    BDF.setqfldPast(2, qfld); // set time - 2*dt
    BOOST_CHECK_CLOSE(qPast2, BDF.qfldpast(2).DOF(0), close_tol);

    // solution can't be zero
    qPast1 = 1.1;
    qfld = qPast1;
    BDF.setqfldPast(1, qfld); // set time - 1*dt
    BOOST_CHECK_CLOSE(qPast1, BDF.qfldpast(1).DOF(0), close_tol);

    // solution can't be zero
    qPast0 = 1.2;
    qfld = qPast0;
    BDF.setqfldPast(0, qfld); // set time - 0*dt
    BOOST_CHECK_CLOSE(qPast0, BDF.qfldpast(0).DOF(0), close_tol);

    // solution can't be zero
    q = 1.3;
    qfld = q;

    for (int step = 0; step < 4; step++)
    {
      // Compute the forcing function
      force = weights[1]*qPast0 + weights[2]*qPast1 + weights[3]*qPast2;

      // Manually advance the solution
      rsd = (q*weights[0] - a*q + force);
      A   = weights[0] - a;
      dq  = rsd/A;
      q  -= dq;

      // Advance the solution one step
      BDF.march(1);

      BOOST_CHECK_CLOSE(q, qfld.DOF(0), close_tol);
      BOOST_CHECK_EQUAL(Real(step+1)*dt, time);

      // rotate the solution
      qPast2 = qPast1;
      qPast1 = qPast0;
      qPast0 = q;

      BOOST_CHECK_CLOSE(qPast2, BDF.qfldpast(2).DOF(0), close_tol);
      BOOST_CHECK_CLOSE(qPast1, BDF.qfldpast(1).DOF(0), close_tol);
      BOOST_CHECK_CLOSE(qPast0, BDF.qfldpast(0).DOF(0), close_tol);
    }
  }

  {
    time = 0;
    int BDForder = 4;
    std::vector<Real> weights = {25./12./dt, -4./dt, 3./dt, -4./3./dt, 1./4./dt};

    // The BDF class
    BDFClass BDF( BDForder, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {0}, AlgEqSetSpace );

    // solution can't be zero
    qPast3 = 1;
    qfld = qPast3;
    BDF.setqfldPast(3, qfld); // set time - 3*dt
    BOOST_CHECK_CLOSE(qPast3, BDF.qfldpast(3).DOF(0), close_tol);

    // solution can't be zero
    qPast2 = 1.1;
    qfld = qPast2;
    BDF.setqfldPast(2, qfld); // set time - 2*dt
    BOOST_CHECK_CLOSE(qPast2, BDF.qfldpast(2).DOF(0), close_tol);

    // solution can't be zero
    qPast1 = 1.2;
    qfld = qPast1;
    BDF.setqfldPast(1, qfld); // set time - 1*dt
    BOOST_CHECK_CLOSE(qPast1, BDF.qfldpast(1).DOF(0), close_tol);

    // solution can't be zero
    qPast0 = 1.3;
    qfld = qPast0;
    BDF.setqfldPast(0, qfld); // set time - 0*dt
    BOOST_CHECK_CLOSE(qPast0, BDF.qfldpast(0).DOF(0), close_tol);

    // solution can't be zero
    q = 1.4;
    qfld = q;

    for (int step = 0; step < 5; step++)
    {
      // Compute the forcing function
      force = weights[1]*qPast0 + weights[2]*qPast1 + weights[3]*qPast2 + weights[4]*qPast3;

      // Manually advance the solution
      rsd = (q*weights[0] - a*q + force);
      A   = weights[0] - a;
      dq  = rsd/A;
      q  -= dq;

      // Advance the solution one step
      BDF.march(1);

      BOOST_CHECK_CLOSE(q, qfld.DOF(0), close_tol);
      BOOST_CHECK_EQUAL(Real(step+1)*dt, time);

      // rotate the solution
      qPast3 = qPast2;
      qPast2 = qPast1;
      qPast1 = qPast0;
      qPast0 = q;

      BOOST_CHECK_CLOSE(qPast3, BDF.qfldpast(3).DOF(0), close_tol);
      BOOST_CHECK_CLOSE(qPast2, BDF.qfldpast(2).DOF(0), close_tol);
      BOOST_CHECK_CLOSE(qPast1, BDF.qfldpast(1).DOF(0), close_tol);
      BOOST_CHECK_CLOSE(qPast0, BDF.qfldpast(0).DOF(0), close_tol);
    }
  }

  int BDForder;
  BOOST_CHECK_THROW( BDFClass BDF( BDForder=0, dt,time,xfld,qfld,NonLinearSolverDict,pde,quadratureOrder,{0},AlgEqSetSpace ), AssertionException );
  BOOST_CHECK_THROW( BDFClass BDF( BDForder=5, dt,time,xfld,qfld,NonLinearSolverDict,pde,quadratureOrder,{0},AlgEqSetSpace ), DeveloperException );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
