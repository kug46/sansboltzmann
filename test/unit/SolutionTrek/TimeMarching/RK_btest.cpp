// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PseudoTime_btest
// testing of PseudoTime_btest classes

#include "Python/PyDict.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/SurrealS.h"


#include "Field/XFieldLine.h"
#include "Field/XFieldArea.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/Element/ElementSequence.h"

#include "pde/BCParameters.h" //Neded because a custom BC vector is created here
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "SolutionTrek/TimeMarching/RungeKutta/RungeKutta.h"

#include "Meshing/XField1D/XField1D.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#ifdef SANS_PETSC
#include "LinearAlgebra/SparseLinAlg/PETSc/PETScSolver.h"
#endif

// This is only here because RiccatiEquation is special
#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

// This is only here because RiccatiEquation is special
#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"

#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#include "unit/SolutionTrek/Ricatti_btest.h"
#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( RK_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( RK_test )
{
  PyDict NonLinearSolverDict, NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NonLinearSolverParam::params.NonLinearSolver.Solver] = NonLinearSolverParam::params.NonLinearSolver.Newton;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
  NonLinearSolverParam::checkInputs(NonLinearSolverDict);

  typedef SANS::RiccatiEquation RiccatiEquation;

  typedef PDENDConvertSpace<PhysD1, RiccatiEquation> NDPDEClass;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, boost::mpl::vector<>, AlgEqSetTraits_Sparse, DGAdv,
                                           XField<PhysD1, TopoD1>> AlgebraicEquationSet_DGClass;

  typedef RK<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> RKClass;

  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::MatrixQ<Real> MatrixQ;

  GlobalTime time(0);
  Real a = 2, b = 0; // Set b to 0 so the equation is linear
  NDPDEClass pde(time, a, b);

  XField1D xfld( 1 );

  // Using a P0 DG solution field, as we don't have a 0D ODE solver framework

  // solution
  int order = 0;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

  qfld = 1;

  // lagrange multipliers not actually used either
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre, {} );
  lgfld = 0;

  // empty boundary condition information
  PyDict PyBCList;
  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {1e-12, 1e-12};

  // Create the spatial discretization
  AlgebraicEquationSet_DGClass AlgEqSetSpace(xfld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_Default, tol,
                                             {0}, { }, PyBCList, BCBoundaryGroups);

  Real dt;
  Real dt_ref = 0.01;
  Real tperiod = 0.5;
  int nsteps;

  ArrayQ qtemp, q1, q2, q3, q4, qn, qnp;
  ArrayQ rsd, force, dq;
  MatrixQ A;

  Real clamb;
  Real qRK1_coarse, qRK1_fine;
  Real qRK2_coarse, qRK2_fine;
  Real qRK3_coarse, qRK3_fine;
  Real qRK4_coarse, qRK4_fine;

  std::vector<std::vector<Real>> weightsA;
  std::vector<Real> weightsB;
  std::vector<Real> weightsC;

  weightsA.resize(10, std::vector<Real>(10, 0.));
  weightsB.resize(10, 0.);
  weightsC.resize(10, 0.);

  const Real close_tol = 1e-11;
  int stype = 0; //Algebraically stable RK


  //RK1
  {
    int order = 1;
    int stages = 1;
    time = 0;
    dt = dt_ref;

    clamb = 1.;

    weightsA[0][0] = clamb;
    weightsB[0] = clamb;
    weightsC[0] = clamb;

    // The RK class
    RKClass RK( order, stages, stype, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {tol[0]}, {0}, AlgEqSetSpace );

    // Set current solution
    qtemp = 1;
    qfld = qtemp;
    q1 = qtemp;
    nsteps = int (tperiod/dt);

    // Advance the solution
    RK.march(nsteps);

    // Set current solution
    qn = qtemp;
    for (int i=0; i<nsteps; i++)
    {
      //stage 1
      force = - qn*(1./(dt*weightsA[0][0]));
      rsd = q1*((1./(dt*weightsA[0][0])) - a) + force;
      A = (1./(dt*weightsA[0][0])) - a;
      dq = rsd/A;
      q1 -= dq;

      qn = q1;
    }

    qnp = q1;
    BOOST_CHECK_CLOSE(qnp, qfld.DOF(0), close_tol);
    qRK1_coarse = qfld.DOF(0);

    //debugging
    //std::cout << std::setprecision(15) << qnp << std::endl;
    //std::cout << std::setprecision(15) << qfld.DOF(0) << std::endl;
  }


  //RK1 (fine)
  {
    int order = 1;
    int stages = 1;
    time = 0;
    dt = dt_ref/2.;

    // The RK class
    RKClass RK( order, stages, stype, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {tol[0]}, {0}, AlgEqSetSpace );

    // Set current solution
    qtemp = 1;
    qfld = qtemp;
    nsteps = int (tperiod/dt);

    // Advance the solution
    RK.march(nsteps);

    qRK1_fine = qfld.DOF(0);
  }


  //RK2
  {
    int order = 2;
    int stages = 2;
    time = 0;
    dt = dt_ref;

    clamb = (2.-sqrt(2.))/2.;

    weightsA[0][0] = clamb;
    weightsA[0][1] = 0.;
    weightsA[1][0] = 1.-2.*clamb;
    weightsA[1][1] = clamb;

    weightsB[0] = 1./2.;
    weightsB[1] = 1./2.;

    weightsC[0] = clamb;
    weightsC[1] = 1.-clamb;

    // The RK class
    RKClass RK( order, stages, stype, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {tol[0]}, {0}, AlgEqSetSpace );

    // Set current solution
    qtemp = 1;
    qfld = qtemp;
    q1 = qtemp;
    q2 = qtemp;
    nsteps = int (tperiod/dt);

    // Advance the solution
    RK.march(nsteps);

    // Set current solution
    qn = qtemp;
    for (int i=0; i<nsteps; i++)
    {
      //stage 1
      force = - qn*(1./(dt*weightsA[0][0]));
      rsd = q1*((1./(dt*weightsA[0][0])) - a) + force;
      A = (1./(dt*weightsA[0][0])) - a;
      dq = rsd/A;
      q1 -= dq;

      //stage 2
      force = - qn*(1./(dt*weightsA[1][1])) - (weightsA[1][0]/weightsA[1][1])*a*q1;
      rsd = q2*((1./(dt*weightsA[1][1])) - a) + force;
      A = (1./(dt*weightsA[1][1])) - a;
      dq = rsd/A;
      q2 -= dq;

      qnp = qn + dt*(weightsB[0]*a*q1 + weightsB[1]*a*q2);
      qn = qnp;
    }

    BOOST_CHECK_CLOSE(qnp, qfld.DOF(0), close_tol);
    qRK2_coarse = qfld.DOF(0);

    //debugging
    //std::cout << std::setprecision(15) << qnp << std::endl;
    //std::cout << std::setprecision(15) << qfld.DOF(0) << std::endl;
  }


  //RK2 (fine)
  {
    int order = 2;
    int stages = 2;
    time = 0;
    dt = dt_ref/2.;

    // The RK class
    RKClass RK( order, stages, stype, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {tol[0]}, {0}, AlgEqSetSpace );

    // Set current solution
    qtemp = 1;
    qfld = qtemp;
    nsteps = int (tperiod/dt);

    // Advance the solution
    RK.march(nsteps);

    qRK2_fine = qfld.DOF(0);
  }


  //RK3
  {
    int order = 3;
    int stages = 3;
    time = 0;
    dt = dt_ref;

    clamb = 0.435866521508459;

    Real d1, d2;
    Real c2, c3;
    Real b1, b2, b3;
    Real a21, a31, a32;

    d1 = 2.; //Must be greater than 1.77429424707279

    b1 = (d1*(1.-2.*clamb) + (1./6.))/((2.*clamb - 1.)*(2.*clamb - 1. -2.*d1));
    b2 = (clamb*clamb - clamb + (1./6.))/((clamb-0.5)*(clamb-0.5)-d1*d1);
    b3 = (d1*(2.*clamb-1.) + (1./6.))/((2.*clamb - 1.)*(2.*clamb - 1. +2.*d1));

    d2 = (0.5-clamb-d1)*(b2/b3);

    a21 = 0.5-clamb+d1;
    a31 = 1. - 2.*clamb -d2;
    a32 = d2;

    c2 = 0.5+d1;
    c3 = 1.-clamb;

    weightsA[0][0] = clamb;
    weightsA[0][1] = 0.;
    weightsA[0][2] = 0.;

    weightsA[1][0] = a21;
    weightsA[1][1] = clamb;
    weightsA[1][2] = 0.;

    weightsA[2][0] = a31;
    weightsA[2][1] = a32;
    weightsA[2][2] = clamb;

    weightsB[0] = b1;
    weightsB[1] = b2;
    weightsB[2] = b3;

    weightsC[0] = clamb;
    weightsC[1] = c2;
    weightsC[2] = c3;

    // The RK class
    RKClass RK( order, stages, stype, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {tol[0]}, {0}, AlgEqSetSpace );

    // Set current solution
    qtemp = 1;
    qfld = qtemp;
    q1 = qtemp;
    q2 = qtemp;
    q3 = qtemp;
    nsteps = int (tperiod/dt);

    // Advance the solution
    RK.march(nsteps);

    // Set current solution
    qn = qtemp;
    for (int i=0; i<nsteps; i++)
    {
      //stage 1
      force = - qn*(1./(dt*weightsA[0][0]));
      rsd = q1*((1./(dt*weightsA[0][0])) - a) + force;
      A = (1./(dt*weightsA[0][0])) - a;
      dq = rsd/A;
      q1 -= dq;

      //stage 2
      force = - qn*(1./(dt*weightsA[1][1])) - (weightsA[1][0]/weightsA[1][1])*a*q1;
      rsd = q2*((1./(dt*weightsA[1][1])) - a) + force;
      A = (1./(dt*weightsA[1][1])) - a;
      dq = rsd/A;
      q2 -= dq;

      //stage 3
      force = - qn*(1./(dt*weightsA[2][2])) - (weightsA[2][0]/weightsA[2][2])*a*q1 - (weightsA[2][1]/weightsA[2][2])*a*q2;
      rsd = q3*((1./(dt*weightsA[2][2])) - a) + force;
      A = (1./(dt*weightsA[2][2])) - a;
      dq = rsd/A;
      q3 -= dq;

      qnp = qn + dt*(weightsB[0]*a*q1 + weightsB[1]*a*q2 + weightsB[2]*a*q3);
      qn = qnp;
    }

    BOOST_CHECK_CLOSE(qnp, qfld.DOF(0), close_tol);
    qRK3_coarse = qfld.DOF(0);

    //debugging
    //std::cout << std::setprecision(15) << qnp << std::endl;
    //std::cout << std::setprecision(15) << qfld.DOF(0) << std::endl;
  }


  //RK3 (fine)
  {
    int order = 3;
    int stages = 3;
    time = 0;
    dt = dt_ref/2.;

    // The RK class
    RKClass RK( order, stages, stype, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {tol[0]}, {0}, AlgEqSetSpace );

    // Set current solution
    qtemp = 1;
    qfld = qtemp;
    nsteps = int (tperiod/dt);

    // Advance the solution
    RK.march(nsteps);

    qRK3_fine = qfld.DOF(0);
  }


  //RK4
  {
    int order = 4;
    int stages = 4;
    time = 0;
    dt = dt_ref;

    clamb = 0.5728160624821349;

    Real d1, d2, d3;

    Real a21, a31, a32;
    Real a41, a42, a43;
    Real b2, c2, c3;

    d2 = -(clamb*(clamb - (1./3.)))/(2.*clamb*clamb - clamb + (1./6.));

    d1 = clamb*clamb - (clamb/2.) + (1./12.);
    d3 = clamb*clamb*clamb - (3./2.)*clamb*clamb + (1./2.)*clamb - (1./24.);

    b2 = (d1*d1)/(2*clamb*(clamb - (1./3.))*(clamb - (1./4.)));

    a21 = clamb*((1./3.)-clamb)/(2*d1);

    a31 = (((1./2.)-clamb-(d3/d1))*(clamb*(clamb - (1./3.))) + 8.*d3*(clamb - (1./4.)))/(clamb*(clamb - (1./3.)));
    a32 = -(8.*d3*(clamb - (1./4.)))/(clamb*(clamb-(1./3.)));

    a42 = ((clamb*clamb - clamb + (1./6.))*.5 + (d3/d2))/(d2*((1./2.)-b2));
    a41 = 1.-2.*clamb-a42-(d1/((4.*clamb-1.)*(b2-(1./2.))));
    a43 = d1/((4.*clamb -1.)*(b2-(1./2.)));

    c2 = clamb*(clamb-(1./2.))*(clamb-(1./2.))/d1;
    c3 = (1./2.)-(d3/d1);

    weightsA[0][0] = clamb;
    weightsA[0][1] = 0.;
    weightsA[0][2] = 0.;
    weightsA[0][3] = 0.;

    weightsA[1][0] = a21;
    weightsA[1][1] = clamb;
    weightsA[1][2] = 0.;
    weightsA[1][3] = 0.;

    weightsA[2][0] = a31;
    weightsA[2][1] = a32;
    weightsA[2][2] = clamb;
    weightsA[2][3] = 0.;

    weightsA[3][0] = a41;
    weightsA[3][1] = a42;
    weightsA[3][2] = a43;
    weightsA[3][3] = clamb;

    weightsB[0] = 0.5-b2;
    weightsB[1] = b2;
    weightsB[2] = b2;
    weightsB[3] = 0.5-b2;

    weightsC[0] = clamb;
    weightsC[1] = c2;
    weightsC[2] = c3;
    weightsC[3] = 1.-clamb;

    // The RK class
    RKClass RK( order, stages, stype, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {tol[0]}, {0}, AlgEqSetSpace );

    // Set current solution
    qtemp = 1;
    qfld = qtemp;
    q1 = qtemp;
    q2 = qtemp;
    q3 = qtemp;
    q4 = qtemp;
    nsteps = int (tperiod/dt);

    // Advance the solution
    RK.march(nsteps);

    // Set current solution
    qn = qtemp;
    for (int i=0; i<nsteps; i++)
    {
      //stage 1
      force = - qn*(1./(dt*weightsA[0][0]));
      rsd = q1*((1./(dt*weightsA[0][0])) - a) + force;
      A = (1./(dt*weightsA[0][0])) - a;
      dq = rsd/A;
      q1 -= dq;

      //stage 2
      force = - qn*(1./(dt*weightsA[1][1])) - (weightsA[1][0]/weightsA[1][1])*a*q1;
      rsd = q2*((1./(dt*weightsA[1][1])) - a) + force;
      A = (1./(dt*weightsA[1][1])) - a;
      dq = rsd/A;
      q2 -= dq;

      //stage 3
      force = - qn*(1./(dt*weightsA[2][2])) - (weightsA[2][0]/weightsA[2][2])*a*q1 - (weightsA[2][1]/weightsA[2][2])*a*q2;
      rsd = q3*((1./(dt*weightsA[2][2])) - a) + force;
      A = (1./(dt*weightsA[2][2])) - a;
      dq = rsd/A;
      q3 -= dq;

      //stage 4
      force = - qn*(1./(dt*weightsA[3][3])) - (weightsA[3][0]/weightsA[3][3])*a*q1;
      force +=- (weightsA[3][1]/weightsA[3][3])*a*q2 - (weightsA[3][2]/weightsA[3][3])*a*q3;
      rsd = q4*((1./(dt*weightsA[3][3])) - a) + force;
      A = (1./(dt*weightsA[3][3])) - a;
      dq = rsd/A;
      q4 -= dq;

      qnp = qn + dt*(weightsB[0]*a*q1 + weightsB[1]*a*q2 + weightsB[2]*a*q3 + weightsB[3]*a*q4);
      qn = qnp;
    }

    BOOST_CHECK_CLOSE(qnp, qfld.DOF(0), close_tol);
    qRK4_coarse = qfld.DOF(0);

    //debugging
    //std::cout << std::setprecision(15) << qnp << std::endl;
    //std::cout << std::setprecision(15) << qfld.DOF(0) << std::endl;
  }


  //RK4 (fine)
  {
    int order = 4;
    int stages = 4;
    time = 0;
    dt = dt_ref/2.;

    // The RK class
    RKClass RK( order, stages, stype, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {tol[0]}, {0}, AlgEqSetSpace );

    // Set current solution
    qtemp = 1;
    qfld = qtemp;
    nsteps = int (tperiod/dt);

    // Advance the solution
    RK.march(nsteps);

    qRK4_fine = qfld.DOF(0);
  }

  //Check order of accuracy
  Real eval_1, eval_2, eval_3, eval_4;
  Real qexact = exp(a*tperiod);
  const Real error_tol = 5.0;

  eval_1 = log(fabs((qRK1_fine-qexact)/(qRK1_coarse-qexact)))/log(1./2.);
  BOOST_CHECK_CLOSE(eval_1, 1.0, error_tol);
  eval_2 = log(fabs((qRK2_fine-qexact)/(qRK2_coarse-qexact)))/log(1./2.);
  BOOST_CHECK_CLOSE(eval_2, 2.0, error_tol);
  eval_3 = log(fabs((qRK3_fine-qexact)/(qRK3_coarse-qexact)))/log(1./2.);
  BOOST_CHECK_CLOSE(eval_3, 3.0, error_tol);
  eval_4 = log(fabs((qRK4_fine-qexact)/(qRK4_coarse-qexact)))/log(1./2.);
  BOOST_CHECK_CLOSE(eval_4, 4.0, error_tol);


  stype = 1; //L-stable and stiffly accurate

  //RK1
  {
    int order = 1;
    int stages = 1;
    time = 0;
    dt = dt_ref;

    // The RK class
    RKClass RK( order, stages, stype, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {tol[0]}, {0}, AlgEqSetSpace );

    // Set current solution
    qtemp = 1;
    qfld = qtemp;
    nsteps = int (tperiod/dt);

    // Advance the solution
    RK.march(nsteps);
    qRK1_coarse = qfld.DOF(0);
  }


  //RK1 (fine)
  {
    int order = 1;
    int stages = 1;
    time = 0;
    dt = dt_ref/2.;

    // The RK class
    RKClass RK( order, stages, stype, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {tol[0]}, {0}, AlgEqSetSpace );

    // Set current solution
    qtemp = 1;
    qfld = qtemp;
    nsteps = int (tperiod/dt);

    // Advance the solution
    RK.march(nsteps);
    qRK1_fine = qfld.DOF(0);
  }


  //RK2
  {
    int order = 2;
    int stages = 2;
    time = 0;
    dt = dt_ref;

    // The RK class
    RKClass RK( order, stages, stype, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {tol[0]}, {0}, AlgEqSetSpace );

    // Set current solution
    qtemp = 1;
    qfld = qtemp;
    nsteps = int (tperiod/dt);

    // Advance the solution
    RK.march(nsteps);
    qRK2_coarse = qfld.DOF(0);

  }


  //RK2 (fine)
  {
    int order = 2;
    int stages = 2;
    time = 0;
    dt = dt_ref/2.;

    // The RK class
    RKClass RK( order, stages, stype, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {tol[0]}, {0}, AlgEqSetSpace );

    // Set current solution
    qtemp = 1;
    qfld = qtemp;
    nsteps = int (tperiod/dt);

    // Advance the solution
    RK.march(nsteps);
    qRK2_fine = qfld.DOF(0);
  }


  //RK3
  {
    int order = 3;
    int stages = 3;
    time = 0;
    dt = dt_ref;

    // The RK class
    RKClass RK( order, stages, stype, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {tol[0]}, {0}, AlgEqSetSpace );

    // Set current solution
    qtemp = 1;
    qfld = qtemp;
    nsteps = int (tperiod/dt);

    // Advance the solution
    RK.march(nsteps);
    qRK3_coarse = qfld.DOF(0);
  }


  //RK3 (fine)
  {
    int order = 3;
    int stages = 3;
    time = 0;
    dt = dt_ref/2.;

    // The RK class
    RKClass RK( order, stages, stype, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {tol[0]}, {0}, AlgEqSetSpace );

    // Set current solution
    qtemp = 1;
    qfld = qtemp;
    nsteps = int (tperiod/dt);

    // Advance the solution
    RK.march(nsteps);
    qRK3_fine = qfld.DOF(0);
  }

  //Check order of accuracy
  eval_1 = log(fabs((qRK1_fine-qexact)/(qRK1_coarse-qexact)))/log(1./2.);
  BOOST_CHECK_CLOSE(eval_1, 1.0, error_tol);
  eval_2 = log(fabs((qRK2_fine-qexact)/(qRK2_coarse-qexact)))/log(1./2.);
  BOOST_CHECK_CLOSE(eval_2, 2.0, error_tol);
  eval_3 = log(fabs((qRK3_fine-qexact)/(qRK3_coarse-qexact)))/log(1./2.);
  BOOST_CHECK_CLOSE(eval_3, 3.0, error_tol);
}

BOOST_AUTO_TEST_CASE( Serial_Parallel_RK_Compile)
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass2;
  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector2;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass2, BCNDConvertSpace, BCVector2,
                                   AlgEqSetTraits_Sparse, DGBR2, XField<PhysD2, TopoD2>> PrimalEquationSetSparse;
  typedef PrimalEquationSetSparse::BCParams BCParams;

  typedef NDPDEClass2::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass2::template VectorArrayQ<Real> VectorArrayQ;

  typedef RK<NDPDEClass2, AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> RKClass;

  // global communicator
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  Real u = 0.1;
  Real v = 0.0;

  AdvectiveFlux2D_Uniform adv(u,v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Real a = 1.4, b = 3.14, c = 0.5;
  Source2D_UniformGrad source(a,b,c);

  NDPDEClass2 pde( adv, visc, source);

  // BC
  PyDict SineSine;
  SineSine[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.SineSine;

  PyDict BCSoln_sansLG;
  BCSoln_sansLG[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln_sansLG[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = SineSine;
  //BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCSoln_sansLG"] = BCSoln_sansLG;

  std::map<std::string, std::vector<int>> BCBoundaryGroups2;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups2["BCSoln_sansLG"] = {XField2D_Box_Triangle_X1::iLeft,
                                 XField2D_Box_Triangle_X1::iBottom,
                                 XField2D_Box_Triangle_X1::iRight,
                                 XField2D_Box_Triangle_X1::iTop};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);


  // grid: HierarchicalP1 (aka X1)

  int ii = 3;
  int jj = 3;

  XField2D_Box_Triangle_Lagrange_X1 xfld_global( comm, ii, jj ); // complete system on all processors
  XField2D_Box_Triangle_Lagrange_X1 xfld_local( world, ii, jj ); // partitioned system


  // Set up Solver for the RK stepping
  PyDict LineUpdateDict, NewtonSolverDict, NonLinearSolverDict;
#ifdef SANS_MPI
#ifndef SANS_PETSC
#error "SANS must be configured with USE_PETSC=ON if USE_MPI=ON"
#endif
  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 0;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
//PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-11;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = false;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor] = 1.1;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1000;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;
  NewtonSolverDict[NewtonSolverParam::params.Timing] =  false;

  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  // solution: P1 (aka Q1)
  int order = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_global(xfld_global, order, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_local (xfld_local , order, BasisFunctionCategory_Legendre);
  qfld_global = 0;
  qfld_local  = 0;

  // lifting operator
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld_global(xfld_global, order, BasisFunctionCategory_Legendre);
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld_local (xfld_local , order, BasisFunctionCategory_Legendre);
  rfld_global = 0;
  rfld_local  = 0;

  // Lagrange multiplier: Legendre P1
  std::vector<int> mitLG_bcgroups = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups2);
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_global( xfld_global, order, BasisFunctionCategory_Legendre, mitLG_bcgroups );
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_local ( xfld_local , order, BasisFunctionCategory_Legendre, mitLG_bcgroups );
  lgfld_global = 0;
  lgfld_local  = 0;

  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  QuadratureOrder quadratureOrder( xfld_global, 2*order + 1 );
  QuadratureOrder quadratureOrder2( xfld_local, 2*order + 1 );

  std::vector<Real> tol = {1e-12, 1e-12};

  PrimalEquationSetSparse PrimalEqSet_global(xfld_global, qfld_global, rfld_global, lgfld_global, pde, disc, quadratureOrder,
                                             ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups2);
  PrimalEquationSetSparse PrimalEqSet_local (xfld_local, qfld_local, rfld_local, lgfld_local, pde, disc, quadratureOrder2,
                                            ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups2);

  // RK Set up

  int RKorder = 4;
  int RKtype = 0;
  int RKstages = RKorder;

  GlobalTime time(0);
  int Nsteps = 2;
  Real dt = 0.1;

  BOOST_TEST_CHECKPOINT("Serial RK Initialization");
  RKClass RK_global(RKorder, RKstages, RKtype, dt, time, xfld_global, qfld_global, NonLinearSolverDict, pde,
                    quadratureOrder, { tol[0] }, { 0 },PrimalEqSet_global);
  BOOST_TEST_CHECKPOINT("Parallel RK Initialization");
  RKClass RK_local(RKorder, RKstages, RKtype, dt, time, xfld_local, qfld_local, NonLinearSolverDict, pde,
                    quadratureOrder2, { tol[0] }, { 0 },PrimalEqSet_local);

  for (int step = 0; step < Nsteps; step++)
  {
    RK_global.march(1);
    BOOST_TEST_CHECKPOINT("Serial Step");
    RK_local.march(1);
    BOOST_TEST_CHECKPOINT("Parallel Step");
  }

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  // check that the parallel solution matches the serial
  for ( int i = 0; i < qfld_local.nDOF(); i++ )
  {
    int in = qfld_local.local2nativeDOFmap(i);
    SANS_CHECK_CLOSE( qfld_local.DOF(i), qfld_global.DOF(in),  small_tol, close_tol );
  }

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
