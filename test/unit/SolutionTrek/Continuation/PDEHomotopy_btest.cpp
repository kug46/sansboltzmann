// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEHomotopy_btest
// testing of PDEHomotopy_btest classes

#include "Python/PyDict.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "SolutionTrek/Continuation/Homotopy/PDEHomotopy.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"

#include "Meshing/XField1D/XField1D.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEHomotopy_test_suite )

BOOST_AUTO_TEST_CASE( static_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PrimaryPDE;
  typedef PDENDConvertSpace<PhysD1, PrimaryPDE> NDPrimaryPDE;
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_None,
                                ViscousFlux1D_Uniform,
                                Source1D_None> AuxiliaryPDE;
  typedef PDENDConvertSpace<PhysD1, AuxiliaryPDE> NDAuxiliaryPDE;

  typedef PDEHomotopy<NDPrimaryPDE, NDAuxiliaryPDE> NDPDEClass;

  BOOST_CHECK( NDPDEClass::D == 1 );
  BOOST_CHECK( NDPDEClass::N == 1 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors_steady_1D )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PrimaryPDE;
  typedef PDENDConvertSpace<PhysD1, PrimaryPDE> NDPrimaryPDE;
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_None,
                                ViscousFlux1D_Uniform,
                                Source1D_None> AuxiliaryPDE;
  typedef PDENDConvertSpace<PhysD1, AuxiliaryPDE> NDAuxiliaryPDE;

  typedef PDEHomotopy<NDPrimaryPDE, NDAuxiliaryPDE> NDPDEClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);
  AdvectiveFlux1D_None NoAdv;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  NDPrimaryPDE primarypde(adv, visc, source);
  NDAuxiliaryPDE auxiliarypde(NoAdv, visc, source);

  Real lambda = 0.0;
  NDPDEClass pde(lambda, primarypde, auxiliarypde);

  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( set_data_steady_1D )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PrimaryPDE;
  typedef PDENDConvertSpace<PhysD1, PrimaryPDE> NDPrimaryPDE;
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_None,
                                ViscousFlux1D_Uniform,
                                Source1D_None> AuxiliaryPDE;
  typedef PDENDConvertSpace<PhysD1, AuxiliaryPDE> NDAuxiliaryPDE;

  typedef PDEHomotopy<NDPrimaryPDE, NDAuxiliaryPDE> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  const Real tol = 1.e-13;

  Real u = 1.0;
  AdvectiveFlux1D_Uniform adv(u);
  AdvectiveFlux1D_None NoAdv;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  NDPrimaryPDE primarypde(adv, visc, source);
  NDAuxiliaryPDE auxiliarypde(NoAdv, visc, source);

  Real lambda = 0.0;
  NDPDEClass pde(lambda, primarypde, auxiliarypde);

  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  pde.setMergeMode(ResidualMode);

  Real sln  =  3.263;

  // Set data
  Real qDataPrim[1] = {sln};
  std::string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = sln;
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( advective_flux_steady_1D )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PrimaryPDE;
  typedef PDENDConvertSpace<PhysD1, PrimaryPDE> NDPrimaryPDE;
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_None,
                                ViscousFlux1D_Uniform,
                                Source1D_None> AuxiliaryPDE;
  typedef PDENDConvertSpace<PhysD1, AuxiliaryPDE> NDAuxiliaryPDE;

  typedef PDEHomotopy<NDPrimaryPDE, NDAuxiliaryPDE> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorX VectorX;

  const Real tol = 1.e-13;

  Real u = 1.0;
  AdvectiveFlux1D_Uniform adv(u);
  AdvectiveFlux1D_None NoAdv;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  NDPrimaryPDE primarypde(adv, visc, source);
  NDAuxiliaryPDE auxiliarypde(NoAdv, visc, source);

  Real lambda = 0.0;
  NDPDEClass pde(lambda, primarypde, auxiliarypde);

  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  pde.setMergeMode(ResidualMode);

  VectorX xvec;
  xvec[0] = 0.0;
  Real sln  =  3.263;

  // Set data
  Real qDataPrim[1] = {sln};
  std::string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = sln;
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  // Test lambda in 0.05 increments
  lambda = 0.0;
  while ( lambda <= 1.0)
  {
    ArrayQ fTrue = {lambda * (u*sln) + (1. - lambda) * (0.0*sln)};
    DLA::VectorS<1,ArrayQ> F;
    F = 0.0;
    pde.fluxAdvective( xvec, q, F );
    BOOST_CHECK_CLOSE( fTrue, F[0], tol );
    lambda += 0.05;
  }

  pde.setMergeMode(JacobianMode);

  // Test lambda in 0.05 increments
  lambda = 0.0;
  while ( lambda <= 1.0)
  {
    ArrayQ fTrue = {(u*sln) - (0.0*sln)};
    DLA::VectorS<1,ArrayQ> F;
    F = 0.0;
    pde.fluxAdvective( xvec, q, F );
    BOOST_CHECK_CLOSE( fTrue, F[0], tol );
    lambda += 0.05;
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( advective_jacobian_steady_1D )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PrimaryPDE;
  typedef PDENDConvertSpace<PhysD1, PrimaryPDE> NDPrimaryPDE;
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_None,
                                ViscousFlux1D_Uniform,
                                Source1D_None> AuxiliaryPDE;
  typedef PDENDConvertSpace<PhysD1, AuxiliaryPDE> NDAuxiliaryPDE;

  typedef PDEHomotopy<NDPrimaryPDE, NDAuxiliaryPDE> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::MatrixQ<Real> MatrixQ;
  typedef NDPDEClass::VectorX VectorX;

  const Real tol = 1.e-13;

  Real u = 1.0;
  AdvectiveFlux1D_Uniform adv(u);
  AdvectiveFlux1D_None NoAdv;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  NDPrimaryPDE primarypde(adv, visc, source);
  NDAuxiliaryPDE auxiliarypde(NoAdv, visc, source);

  Real lambda = 0.0;
  NDPDEClass pde(lambda, primarypde, auxiliarypde);

  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  pde.setMergeMode(ResidualMode);

  VectorX xvec;
  xvec[0] = 0.0;
  Real sln  =  3.263;

  // Set data
  Real qDataPrim[1] = {sln};
  std::string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = sln;
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  // Test lambda in 0.05 increments
  lambda = 0.0;
  while ( lambda <= 1.0)
  {
    DLA::VectorS<1,MatrixQ> dFdu = 0;
    pde.jacobianFluxAdvective( xvec, q, dFdu );
    BOOST_CHECK_CLOSE( u*lambda, dFdu[0], tol );
    lambda += 0.05;
  }

  pde.setMergeMode(JacobianMode);

  // Test lambda in 0.05 increments
  lambda = 0.0;
  while ( lambda <= 1.0)
  {
    DLA::VectorS<1,MatrixQ> dFdu = 0;
    pde.jacobianFluxAdvective( xvec, q, dFdu );
    BOOST_CHECK_CLOSE( u - 0.0, dFdu[0], tol );
    lambda += 0.05;
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( diffusive_flux_steady_1D )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PrimaryPDE;
  typedef PDENDConvertSpace<PhysD1, PrimaryPDE> NDPrimaryPDE;
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> AuxiliaryPDE;
  typedef PDENDConvertSpace<PhysD1, AuxiliaryPDE> NDAuxiliaryPDE;

  typedef PDEHomotopy<NDPrimaryPDE, NDAuxiliaryPDE> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorX VectorX;

  const Real tol = 1.e-13;

  Real u = 1.0;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx1 = 2.123;
  ViscousFlux1D_Uniform visc1(kxx1);

  Real kxx2 = 2.0*2.123;
  ViscousFlux1D_Uniform visc2(kxx2);

  Source1D_None source;

  NDPrimaryPDE primarypde(adv, visc1, source);
  NDAuxiliaryPDE auxiliarypde(adv, visc2, source);

  Real lambda = 0.0;
  NDPDEClass pde(lambda, primarypde, auxiliarypde);

  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  pde.setMergeMode(ResidualMode);

  VectorX xvec;
  xvec[0] = 0.0;
  Real sln   =  3.263;
  Real slnx  = -0.445;

  // Set data
  Real qDataPrim[1] = {sln};
  std::string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = sln;
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );

  qDataPrim[0] = slnx;
  ArrayQ qxTrue = qDataPrim[0];
  ArrayQ qx;
  pde.setDOFFrom( qx, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxTrue, qx, tol );
  DLA::VectorS<1, ArrayQ> gradq = qx;
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  // Test lambda in 0.05 increments
  lambda = 0.0;
  while ( lambda <= 1.0)
  {
    ArrayQ fTrue = -(lambda*kxx1*slnx + (1. - lambda)*kxx2*slnx);
    DLA::VectorS<1,ArrayQ> F;
    F = 0.0;
    pde.fluxViscous( xvec, q, gradq, F );
    BOOST_CHECK_CLOSE( fTrue, F[0], tol );
    lambda += 0.05;
  }

  pde.setMergeMode(JacobianMode);

  // Test lambda in 0.05 increments
  lambda = 0.0;
  while ( lambda <= 1.0)
  {
    ArrayQ fTrue = -(kxx1*slnx - kxx2*slnx);
    DLA::VectorS<1,ArrayQ> F;
    F = 0.0;
    pde.fluxViscous( xvec, q, gradq, F );
    BOOST_CHECK_CLOSE( fTrue, F[0], tol );
    lambda += 0.05;
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( diffusive_jacobian_steady_1D )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PrimaryPDE;
  typedef PDENDConvertSpace<PhysD1, PrimaryPDE> NDPrimaryPDE;
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> AuxiliaryPDE;
  typedef PDENDConvertSpace<PhysD1, AuxiliaryPDE> NDAuxiliaryPDE;

  typedef PDEHomotopy<NDPrimaryPDE, NDAuxiliaryPDE> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::MatrixQ<Real> MatrixQ;
  typedef NDPDEClass::VectorX VectorX;

  const Real tol = 1.e-13;

  Real u = 1.0;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx1 = 2.123;
  ViscousFlux1D_Uniform visc1(kxx1);

  Real kxx2 = 2.0*2.123;
  ViscousFlux1D_Uniform visc2(kxx2);

  Source1D_None source;

  NDPrimaryPDE primarypde(adv, visc1, source);
  NDAuxiliaryPDE auxiliarypde(adv, visc2, source);

  Real lambda = 0.0;
  NDPDEClass pde(lambda, primarypde, auxiliarypde);

  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  pde.setMergeMode(ResidualMode);

  VectorX xvec;
  xvec[0] = 0.0;
  Real sln   =  3.263;
  Real slnx  = -0.445;

  // Set data
  Real qDataPrim[1] = {sln};
  std::string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = sln;
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );

  qDataPrim[0] = slnx;
  ArrayQ qxTrue = qDataPrim[0];
  ArrayQ qx;
  pde.setDOFFrom( qx, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxTrue, qx, tol );
  DLA::VectorS<1, ArrayQ> gradq = qx;
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  // Test lambda in 0.05 increments
  lambda = 0.0;
  while ( lambda <= 1.0)
  {
    ArrayQ fTrue = lambda*kxx1 + (1. - lambda)*kxx2;
    DLA::MatrixS<1,1,MatrixQ> KMtx = 0;
    KMtx = 0.0;
    pde.diffusionViscous( xvec, q, gradq, KMtx );
    BOOST_CHECK_CLOSE( fTrue, KMtx(0,0), tol );
    lambda += 0.05;
  }

  pde.setMergeMode(JacobianMode);

  // Test lambda in 0.05 increments
  lambda = 0.0;
  while ( lambda <= 1.0)
  {
    ArrayQ fTrue = kxx1 - kxx2;
    DLA::MatrixS<1,1,MatrixQ> KMtx = 0;
    KMtx = 0.0;
    pde.diffusionViscous( xvec, q, gradq, KMtx );
    BOOST_CHECK_CLOSE( fTrue, KMtx(0,0), tol );
    lambda += 0.05;
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( source_steady_1D )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform> PrimaryPDE;
  typedef PDENDConvertSpace<PhysD1, PrimaryPDE> NDPrimaryPDE;
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> AuxiliaryPDE;
  typedef PDENDConvertSpace<PhysD1, AuxiliaryPDE> NDAuxiliaryPDE;

  typedef PDEHomotopy<NDPrimaryPDE, NDAuxiliaryPDE> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorX VectorX;

  const Real tol = 1.e-13;

  Real u = 1.0;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 1.0;
  Source1D_Uniform source1(a);
  Source1D_None source2;

  NDPrimaryPDE primarypde(adv, visc, source1);
  NDAuxiliaryPDE auxiliarypde(adv, visc, source2);

  Real lambda = 0.0;
  NDPDEClass pde(lambda, primarypde, auxiliarypde);

  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  pde.setMergeMode(ResidualMode);

  VectorX xvec;
  xvec[0] = 0.0;
  Real sln  =  3.263;
  Real slnx = -0.445;

  // Set data
  Real qDataPrim[1] = {sln};
  std::string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = sln;
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  qDataPrim[0] = slnx;
  ArrayQ qxTrue = qDataPrim[0];
  ArrayQ qx;
  pde.setDOFFrom( qx, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxTrue, qx, tol );
  DLA::VectorS<1, ArrayQ> gradq = qx;
  BOOST_CHECK_CLOSE( qTrue, q, tol );


  // Test lambda in 0.05 increments
  lambda = 0.0;
  while ( lambda <= 1.0)
  {
    ArrayQ fTrue = {lambda * a * sln + (1. - lambda) * 0.0 * sln};
    Real S = 0;
    pde.source( xvec, q, gradq, S );
    BOOST_CHECK_CLOSE( fTrue, S, tol );
    lambda += 0.05;
  }

  pde.setMergeMode(JacobianMode);

  // Test lambda in 0.05 increments
  lambda = 0.0;
  while ( lambda <= 1.0)
  {
    ArrayQ fTrue = {a * sln - 0.0 * sln};
    Real S = 0;
    pde.source( xvec, q, gradq, S );
    BOOST_CHECK_CLOSE( fTrue, S, tol );
    lambda += 0.05;
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( source_jacobian_steady_1D )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform> PrimaryPDE;
  typedef PDENDConvertSpace<PhysD1, PrimaryPDE> NDPrimaryPDE;
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> AuxiliaryPDE;
  typedef PDENDConvertSpace<PhysD1, AuxiliaryPDE> NDAuxiliaryPDE;

  typedef PDEHomotopy<NDPrimaryPDE, NDAuxiliaryPDE> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorX VectorX;

  const Real tol = 1.e-13;

  Real u = 1.0;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 1.0;
  Source1D_Uniform source1(a);
  Source1D_None source2;

  NDPrimaryPDE primarypde(adv, visc, source1);
  NDAuxiliaryPDE auxiliarypde(adv, visc, source2);

  Real lambda = 0.0;
  NDPDEClass pde(lambda, primarypde, auxiliarypde);

  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  pde.setMergeMode(ResidualMode);

  VectorX xvec;
  xvec[0] = 0.0;
  Real sln  =  3.263;
  Real slnx = -0.445;

  // Set data
  Real qDataPrim[1] = {sln};
  std::string qNamePrim[1] = {"Solution"};
  ArrayQ qTrue = sln;
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qTrue, q, tol );

  qDataPrim[0] = slnx;
  ArrayQ qxTrue = qDataPrim[0];
  ArrayQ qx;
  pde.setDOFFrom( qx, qDataPrim, qNamePrim, 1 );
  BOOST_CHECK_CLOSE( qxTrue, qx, tol );
  DLA::VectorS<1, ArrayQ> gradq = qx;
  BOOST_CHECK_CLOSE( qTrue, q, tol );


  // Test lambda in 0.05 increments
  lambda = 0.0;
  while ( lambda <= 1.0)
  {
    ArrayQ fTrue = {lambda * a + (1. - lambda) * 0.0};
    Real S = 0;
    pde.jacobianSource( xvec, q, gradq, S );
    BOOST_CHECK_CLOSE( fTrue, S, tol );
    lambda += 0.05;
  }

  pde.setMergeMode(JacobianMode);

  // Test lambda in 0.05 increments
  lambda = 0.0;
  while ( lambda <= 1.0)
  {
    ArrayQ fTrue = {a - 0.0};
    Real S = 0;
    pde.jacobianSource( xvec, q, gradq, S );
    BOOST_CHECK_CLOSE( fTrue, S, tol );
    lambda += 0.05;
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( forcing_steady_1D )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PrimaryPDE;
  typedef PDENDConvertSpace<PhysD1, PrimaryPDE> NDPrimaryPDE;
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> AuxiliaryPDE;
  typedef PDENDConvertSpace<PhysD1, AuxiliaryPDE> NDAuxiliaryPDE;

  typedef PDEHomotopy<NDPrimaryPDE, NDAuxiliaryPDE> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorX VectorX;

  const Real tol = 1.e-13;

  Real u = 1.0;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real b = 1.0;

  typedef ForcingFunction1D_Const<PrimaryPDE> ForcingType1;
  std::shared_ptr<ForcingType1> forcingptr1( new ForcingType1(b) );

  Source1D_None source;

  NDPrimaryPDE primarypde(adv, visc, source, forcingptr1);
  NDAuxiliaryPDE auxiliarypde(adv, visc, source);

  Real lambda = 0.0;
  NDPDEClass pde(lambda, primarypde, auxiliarypde);

  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  pde.setMergeMode(ResidualMode);

  VectorX xvec;
  xvec[0] = 0.0;

  // Test lambda in 0.05 increments
  lambda = 0.0;
  while ( lambda <= 1.0)
  {
    ArrayQ fTrue = {lambda * b + (1. - lambda) * 0.0};
    Real F = 0;
    pde.forcingFunction( xvec, F );
    BOOST_CHECK_CLOSE( fTrue, F, tol );
    lambda += 0.05;
  }

  pde.setMergeMode(JacobianMode);

  // Test lambda in 0.05 increments
  lambda = 0.0;
  while ( lambda <= 1.0)
  {
    ArrayQ fTrue = {b - 0.0};
    Real F = 0;
    pde.forcingFunction( xvec, F );
    BOOST_CHECK_CLOSE( fTrue, F, tol );
    lambda += 0.05;
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
