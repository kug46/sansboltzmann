// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ImplicitResidualSmoothing_btest
// testing of ImplicitResidualSmoothing_btest classes

#include "Python/PyDict.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <limits>

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/SparseLinAlg/tools/norm.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern_Transpose.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "Field/FieldArea_DG_Cell.h"

#include "pde/BCParameters.h" //Neded because a custom BC vector is created here

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"

#include "SolutionTrek/Continuation/PseudoTime/SetLocalTimeStep.h"

#include "SolutionTrek/Continuation/ImplicitResidualSmoothing/ImplicitResidualSmoothing.h"
#include "SolutionTrek/Continuation/ImplicitResidualSmoothing/AlgebraicEquationSet_IRS.h"

#include "Meshing/XField1D/XField1D.h"

// This is only here because RiccatiEquation is special
#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

// This is only here because RiccatiEquation is special
#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"

#include "unit/SolutionTrek/Ricatti_btest.h"

using namespace SANS;

namespace SANS
{

// A derived class that inherits from ImplicitResidualSmoothing to expose its protected members/methods for testing
// NOTE: intended ONLY for unit testing!
template<class SystemMatrix>
class ImplicitResidualSmoothing_UnitTest : public ImplicitResidualSmoothing<SystemMatrix>
{
public:
  typedef ImplicitResidualSmoothing<SystemMatrix> BaseType;

  using BaseType::ImplicitResidualSmoothing; // inherit constructors

  // Expose protected members and methods for testing
  using BaseType::updateCFL;

  using BaseType::invCFL_max_;
  using BaseType::invCFL_;
  using BaseType::invCFL_min_;
  using BaseType::CFLDecreaseFactor_;
  using BaseType::CFLIncreaseFactor_;

  using BaseType::algEqSetIRS_;
  using BaseType::spatial_;

  using BaseType::verbose_;
};

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( ImplicitResidualSmoothing_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SetLocalTimeStep_Spatial_test )
{
  typedef PDENDConvertSpace<PhysD1, RiccatiEquation> NDPDEClass;

  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::MatrixQ<Real> MatrixQ;

  Real a = 2, b = 3;
  NDPDEClass pde(a, b);

  Real h = 1.75;
  XField1D xfld( 1, 0, 2*h );

  // Using a P0 DG solution field, as we don't have a 0D ODE solver framework

  // solution
  int order = 0;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

  // grid size field
  HField_DG<PhysD1, TopoD1> hfld(xfld);

  // initial guess can't be zero
  qfld = 3;

  ArrayQ q = qfld.DOF(0);

  Real x, time, speed;
  static_cast<RiccatiEquation&>(pde).speedCharacteristic(x, time, q, speed);

  ArrayQ qx;
  MatrixQ dsdu = 0;
  static_cast<RiccatiEquation&>(pde).jacobianSource( x, time, q, qx, dsdu );

  Real dtisource = dsdu;

  Real invCFL = 12;

  // exact dti
  Real dti = invCFL * (speed / h + dtisource);

  QuadratureOrder quadratureOrder( xfld, -1 );

  {
    // DG inverse time step field
    Field_DG_Cell<PhysD1, TopoD1, ArrayQ> dtifld(xfld, 0, BasisFunctionCategory_Legendre);

    // Initialize all DOF to zero for accumulation
    for (int n = 0; n < dtifld.nDOF(); n++)
      dtifld.DOF(n) = 0;

    for_each_CellGroup<TopoD1>::apply(
        SetLocalTimeStep<NDPDEClass, XField<PhysD1, TopoD1>>(pde, quadratureOrder.cellOrders, {0}, invCFL),
        (xfld, (qfld, hfld, dtifld)));

    BOOST_CHECK_CLOSE(dti, dtifld.DOF(0), 1e-12);
  }

  {
    // CG inverse time step field
    Field_CG_Cell<PhysD1, TopoD1, ArrayQ> dtifld(xfld, 1, BasisFunctionCategory_Lagrange);

    // Initialize all DOF to zero for accumulation
    for (int n = 0; n < dtifld.nDOF(); n++)
      dtifld.DOF(n) = 0;

    for_each_CellGroup<TopoD1>::apply(
        SetLocalTimeStep<NDPDEClass, XField<PhysD1, TopoD1>>(pde, quadratureOrder.cellOrders, {0}, invCFL),
        (xfld, (qfld, hfld, dtifld)));

    for (int n = 0; n < dtifld.nDOF(); n++)
      BOOST_CHECK_CLOSE(dti, dtifld.DOF(n), 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SetLocalTimeStep_test )
{
  typedef PDENDConvertSpace<PhysD1, RiccatiEquation> NDPDEClass;

  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::MatrixQ<Real> MatrixQ;

  Real a = 2, b = 3;
  NDPDEClass pde(a, b);

  Real h = 1.75;
  XField1D xfld( 1, 0, 2*h );

  // Using a P0 DG solution field, as we don't have a 0D ODE solver framework

  // solution
  int order = 0;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

  // grid size field
  HField_DG<PhysD1, TopoD1> hfld(xfld);

  // initial guess can't be zero
  qfld = 3;

  ArrayQ q = qfld.DOF(0);

  Real x, time, speed;
  static_cast<RiccatiEquation&>(pde).speedCharacteristic(x, time, q, speed);

  ArrayQ qx;
  MatrixQ dsdu = 0;
  static_cast<RiccatiEquation&>(pde).jacobianSource( x, time, q, qx, dsdu );

  Real dtisource = dsdu;

  Real invCFL = 12;

  // exact dti
  Real dti = invCFL * (speed / h + dtisource);

  // inverse time step field
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> dtifld(xfld, 0, BasisFunctionCategory_Legendre);

  for (int n = 0; n < dtifld.nDOF(); n++)
    dtifld.DOF(n) = 0;

  QuadratureOrder quadratureOrder( xfld, -1 );

  for_each_CellGroup<TopoD1>::apply(
      SetLocalTimeStep<NDPDEClass, XField<PhysD1, TopoD1>>(pde, quadratureOrder.cellOrders, {0}, invCFL),
      (xfld, (qfld, hfld, dtifld)));

  BOOST_CHECK_CLOSE(dti, dtifld.DOF(0), 1e-12);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PythonDictionaryConstructor_updateCFL_test )
{
  PyDict ImplicitResidualSmoothingDict, NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NonLinearSolverParam::params.NonLinearSolver.Solver] = NonLinearSolverParam::params.NonLinearSolver.Newton;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  ImplicitResidualSmoothingDict[ImplicitResidualSmoothingParam::params.NonLinearSolver] = NewtonSolverDict;
  ImplicitResidualSmoothingDict[ImplicitResidualSmoothingParam::params.invCFL] = 10;
  ImplicitResidualSmoothingDict[ImplicitResidualSmoothingParam::params.invCFL_max] = 100;
  ImplicitResidualSmoothingDict[ImplicitResidualSmoothingParam::params.invCFL_min] = 0.1;

  ImplicitResidualSmoothingDict[ImplicitResidualSmoothingParam::params.CFLDecreaseFactor] = 0.5;
  ImplicitResidualSmoothingDict[ImplicitResidualSmoothingParam::params.CFLIncreaseFactor] = 10;

  // Check the inputs before they are extracted in the class
  ImplicitResidualSmoothingParam::checkInputs(ImplicitResidualSmoothingDict);

  typedef SANS::RiccatiEquation RiccatiEquation;

  typedef PDENDConvertSpace<PhysD1, RiccatiEquation> NDPDEClass;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, boost::mpl::vector<>, AlgEqSetTraits_Sparse, DGAdv,
                                           XField<PhysD1, TopoD1>> AlgebraicEquationSet_DGClass;
  typedef AlgebraicEquationSet_IRS<NDPDEClass, AlgEqSetTraits_Sparse, IRS, XField<PhysD1, TopoD1>> AlgebraicEquationSet_IRSClass;

  typedef AlgebraicEquationSet_DGClass::SystemMatrix SystemMatrix;

  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  Real a = 2, b = 3;
  NDPDEClass pde(a, b);

  XField1D xfld( 1 );

  // Using a P0 DG solution field, as we don't have a 0D ODE solver framework

  // solution
  int order = 0;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

  // initial guess can't be zero
  qfld = 1;

  // lagrange multipliers not actually used
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre, {} );

  lgfld = 0;

  // empty boundary condition information
  PyDict PyBCList;
  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {1e-12, 1e-12};

  // Create the spatial discretization
  AlgebraicEquationSet_DGClass AlgEqSet(xfld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_Default, tol,
                                        {0}, { }, PyBCList, BCBoundaryGroups);
  AlgebraicEquationSet_IRSClass AlgEqSetIRS(xfld, qfld, pde, quadratureOrder, {0}, AlgEqSet);

  // Create the pseudo time continuation class
  ImplicitResidualSmoothing_UnitTest< SystemMatrix > IRS(ImplicitResidualSmoothingDict, AlgEqSetIRS);

  { // check constructor
    BOOST_CHECK( IRS.invCFL_max_ == ImplicitResidualSmoothingDict.get(ImplicitResidualSmoothingParam::params.invCFL_max) );
    BOOST_CHECK( IRS.invCFL_ == ImplicitResidualSmoothingDict.get(ImplicitResidualSmoothingParam::params.invCFL) );
    BOOST_CHECK( IRS.invCFL_min_ == ImplicitResidualSmoothingDict.get(ImplicitResidualSmoothingParam::params.invCFL_min) );

    BOOST_CHECK( IRS.CFLDecreaseFactor_ == ImplicitResidualSmoothingDict.get(ImplicitResidualSmoothingParam::params.CFLDecreaseFactor) );
    BOOST_CHECK( IRS.CFLIncreaseFactor_ == ImplicitResidualSmoothingDict.get(ImplicitResidualSmoothingParam::params.CFLIncreaseFactor) );

    BOOST_CHECK( &IRS.algEqSetIRS_ == &AlgEqSetIRS );
    BOOST_CHECK( &IRS.spatial_ == &AlgEqSetIRS.getSpatialAlgebraicEquationSet() );

    BOOST_CHECK( IRS.verbose_ == false );
  }

  // iterate the pseudo time to convergence
  BOOST_CHECK( IRS.iterate(20) );

  // check the answer is correct
  BOOST_CHECK_CLOSE( a/b, qfld.DOF(0), 1e-8 );

  // solve logic woth the pseudo time to convergence
  BOOST_CHECK( IRS.solve() );

  // check the answer is correct
  BOOST_CHECK_CLOSE( a/b, qfld.DOF(0), 1e-8 );

  //----------------------------//
  // ----- Test updateCFL ----- //
  //----------------------------//
  SolveStatus status;
  Real invCFL_currentTrue;

  const Real invCFL_max_original = IRS.invCFL_max_;

  {
    status.linesearch.status = LineSearchStatus::FullStep;

    invCFL_currentTrue = IRS.invCFL_min_*0.9; // smaller than IRS.invCFL_min_
    IRS.invCFL_ = invCFL_currentTrue;
    IRS.updateCFL(status);
    BOOST_CHECK_EQUAL( IRS.invCFL_min_, IRS.invCFL_ );

    invCFL_currentTrue = IRS.invCFL_min_*IRS.CFLIncreaseFactor_*1.1; // greater than IRS.invCFL_min_*IRS.CFLIncreaseFactor_
    IRS.invCFL_ = invCFL_currentTrue;
    IRS.updateCFL(status);
    BOOST_CHECK_EQUAL( invCFL_currentTrue/IRS.CFLIncreaseFactor_, IRS.invCFL_ );
  }

  {
    status.linesearch.status = LineSearchStatus::PartialStep;

    invCFL_currentTrue = IRS.invCFL_max_*0.9*1.1; // greater than IRS.invCFL_max_*0.9
    IRS.invCFL_ = invCFL_currentTrue;
    IRS.updateCFL(status);
    BOOST_CHECK_EQUAL( IRS.invCFL_max_, IRS.invCFL_ );

    invCFL_currentTrue = IRS.invCFL_max_*0.9*0.9; // smaller than IRS.invCFL_max_*0.9
    IRS.invCFL_ = invCFL_currentTrue;
    IRS.updateCFL(status);
    BOOST_CHECK_EQUAL( invCFL_currentTrue/0.9, IRS.invCFL_ );
  }

  {
    status.linesearch.status = LineSearchStatus::LimitedStep;

    invCFL_currentTrue = IRS.invCFL_max_*0.9*1.1; // greater than IRS.invCFL_max_*0.9
    IRS.invCFL_ = invCFL_currentTrue;
    IRS.updateCFL(status);
    //BOOST_CHECK_EQUAL( IRS.invCFL_max_, IRS.invCFL_ );
    BOOST_CHECK_EQUAL( invCFL_currentTrue, IRS.invCFL_ );

    invCFL_currentTrue = IRS.invCFL_max_*0.9*0.9; // smaller than IRS.invCFL_max_*0.9
    IRS.invCFL_ = invCFL_currentTrue;
    IRS.updateCFL(status);
    //BOOST_CHECK_EQUAL( invCFL_currentTrue/0.9, IRS.invCFL_ );
    BOOST_CHECK_EQUAL( invCFL_currentTrue, IRS.invCFL_ );
  }

  {
    status.linesearch.status = LineSearchStatus::Failure;

    invCFL_currentTrue = IRS.invCFL_max_*IRS.CFLDecreaseFactor_*1.1; // greater than IRS.invCFL_max_*IRS.CFLDecreaseFactor_
    IRS.invCFL_ = invCFL_currentTrue;
    IRS.updateCFL(status);
    BOOST_CHECK_EQUAL( IRS.invCFL_max_, IRS.invCFL_ );

    invCFL_currentTrue = IRS.invCFL_max_*IRS.CFLDecreaseFactor_*0.9; // smaller than IRS.invCFL_max_*IRS.CFLDecreaseFactor_
    IRS.invCFL_ = invCFL_currentTrue;
    IRS.updateCFL(status);
    BOOST_CHECK_EQUAL( invCFL_currentTrue/IRS.CFLDecreaseFactor_, IRS.invCFL_ );


    invCFL_currentTrue = 0; // zero
    const Real tol = sqrt(std::numeric_limits<Real>::epsilon())/IRS.CFLDecreaseFactor_;

    IRS.invCFL_max_ = tol + 1;
    IRS.invCFL_ = invCFL_currentTrue;
    IRS.updateCFL(status);
    BOOST_CHECK_EQUAL( tol, IRS.invCFL_ );

    IRS.invCFL_max_ = 0;
    IRS.invCFL_ = invCFL_currentTrue;
    IRS.updateCFL(status);
    BOOST_CHECK_EQUAL( IRS.invCFL_max_, IRS.invCFL_ );

    IRS.invCFL_max_ = invCFL_max_original; // reset invCFL_max_ to the original value in case it gets used later
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Riccati_Solve )
{
  PyDict NonLinearSolverDict, NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NonLinearSolverParam::params.NonLinearSolver.Solver] = NonLinearSolverParam::params.NonLinearSolver.Newton;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  //std::cout << NonLinearSolverDict << std::endl;

  typedef SANS::RiccatiEquation RiccatiEquation;

  NonLinearSolverParam::checkInputs(NonLinearSolverDict);

  typedef PDENDConvertSpace<PhysD1, RiccatiEquation> NDPDEClass;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, boost::mpl::vector<>, AlgEqSetTraits_Sparse, DGAdv,
                                           XField<PhysD1, TopoD1>> AlgebraicEquationSet_DGClass;
  typedef AlgebraicEquationSet_IRS<NDPDEClass, AlgEqSetTraits_Sparse, IRS, XField<PhysD1, TopoD1>> AlgebraicEquationSet_IRSClass;

  typedef AlgebraicEquationSet_DGClass::SystemMatrix SystemMatrix;

  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  Real a = 2, b = 3;
  NDPDEClass pde(a, b);

  XField1D xfld( 1 );

  // Using a P0 DG solution field, as we don't have a 0D ODE solver framework

  // solution
  int order = 0;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

  // initial guess can't be zero
  qfld = 1;

  // lagrange multipliers not actually used either
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre, {} );

  lgfld = 0;

  // empty boundary condition information
  PyDict PyBCList;
  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {1e-11, 1e-11};

  // Create the spatial discretization
  AlgebraicEquationSet_DGClass AlgEqSet(xfld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_Default, tol,
                                        {0}, { }, PyBCList, BCBoundaryGroups);
  AlgebraicEquationSet_IRSClass AlgEqSetIRS(xfld, qfld, pde, quadratureOrder, {0}, AlgEqSet);

  Real invCFL = 0.1;
  Real invCFL_min = 0.0;
  Real invCFL_max = 1000.0;
  Real CFLDecreaseFactor = 0.9;
  Real CFLIncreaseFactor = 1.1;
  Real CFLPartialStepDecreaseFactor = 1.0;

  // Create the pseudo time continuation class
  ImplicitResidualSmoothing_UnitTest< SystemMatrix >
  IRS(invCFL, invCFL_max, invCFL_min, CFLDecreaseFactor, CFLIncreaseFactor, CFLPartialStepDecreaseFactor,
      NonLinearSolverDict, AlgEqSetIRS, true);

  { // check constructor
    BOOST_CHECK( IRS.invCFL_max_ == invCFL_max );
    BOOST_CHECK( IRS.invCFL_ == invCFL );
    BOOST_CHECK( IRS.invCFL_min_ == invCFL_min );

    BOOST_CHECK( IRS.CFLDecreaseFactor_ == CFLDecreaseFactor );
    BOOST_CHECK( IRS.CFLIncreaseFactor_ == CFLIncreaseFactor );

    BOOST_CHECK( &IRS.algEqSetIRS_ == &AlgEqSetIRS );
    BOOST_CHECK( &IRS.spatial_ == &AlgEqSetIRS.getSpatialAlgebraicEquationSet() );

    BOOST_CHECK( IRS.verbose_ == true );
  }

  // iterat the pseudo time to convergence
  BOOST_CHECK( IRS.iterate(20) );

  // check the answer is correct
  BOOST_CHECK_CLOSE( a/b, qfld.DOF(0), 1e-8 );

  // solve logic woth the pseudo time to convergence
  BOOST_CHECK( IRS.solve() );

  // check the answer is correct
  BOOST_CHECK_CLOSE( a/b, qfld.DOF(0), 1e-8 );

  // call the empty dump function to suppress an intel warning saying it's never used
  pde.dump(0);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Jacobian_test )
{
  typedef PDENDConvertSpace<PhysD1, RiccatiEquation> NDPDEClass;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, boost::mpl::vector<>, AlgEqSetTraits_Dense, DGAdv,
                                           XField<PhysD1, TopoD1>> AlgebraicEquationSet_DGClass;
  typedef AlgebraicEquationSet_IRS<NDPDEClass, AlgEqSetTraits_Dense, IRS, XField<PhysD1, TopoD1>> AlgebraicEquationSet_IRSClass;
  typedef AlgebraicEquationSet_IRSClass::SystemVector SystemVector;
  typedef AlgebraicEquationSet_IRSClass::SystemMatrix SystemMatrix;
  typedef AlgebraicEquationSet_IRSClass::SystemNonZeroPattern SystemNonZeroPattern;

  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  Real a = 2, b = 0; // b = 0 -> linear equation
  NDPDEClass pde(a, b);

  XField1D xfld( 1, 0, 1.25 );

  // Using a P0 DG solution field, as we don't have a 0D ODE solver framework

  // solution
  int order = 0;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

  // initial guess can't be zero
  qfld = 1;

  // lagrange multipliers not actually used either
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre, {} );
  lgfld = 0;

  // empty boundary condition information
  PyDict PyBCList;
  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {1e-11, 1e-11};

  // Create the spatial discretization
  AlgebraicEquationSet_DGClass AlgEqSet(xfld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_Default, tol,
                                        {0}, { }, PyBCList, BCBoundaryGroups);
  AlgebraicEquationSet_IRSClass AlgEqSetIRS(xfld, qfld, pde, quadratureOrder, {0}, AlgEqSet);

  Real invCFL = 10;
  AlgEqSetIRS.setLocalTimeStepField(invCFL);

  SystemVector rsd0(AlgEqSetIRS.vectorEqSize());
  SystemVector rsd1(AlgEqSetIRS.vectorEqSize());

  SystemNonZeroPattern nz(AlgEqSetIRS.matrixSize());
  SystemMatrix mtx(nz);

  mtx = 0;
  AlgEqSetIRS.jacobian(mtx);

  rsd0 = 0;
  AlgEqSetIRS.residual(rsd0);

  qfld.DOF(0) += 1;

  rsd1 = 0;
  AlgEqSetIRS.residual(rsd1);

  Real diff = rsd1[0][0] - rsd0[0][0];

  BOOST_CHECK_CLOSE(diff, mtx(0,0)(0,0), 1e-12);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
