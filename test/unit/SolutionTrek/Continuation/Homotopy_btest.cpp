// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Homotopy_btest
// testing of Homotopy_btest classes

//#define SANS_VERBOSE

#include "Python/PyDict.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/SparseLinAlg/tools/norm.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern_Transpose.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h" // HACK: I need to work out how to sort out homotopy based stuff

#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE // HACK: I need to work out how to sort out homotopy based stuff
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"

#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"
#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"

#include "SolutionTrek/Continuation/Homotopy/Homotopy.h"

#include "Meshing/XField1D/XField1D.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Homotopy_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AD_Solve )
{
#if 1
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PrimaryPDE;
  typedef PDENDConvertSpace<PhysD1, PrimaryPDE> NDPrimaryPDE;
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_None,
                                ViscousFlux1D_Uniform,
                                Source1D_None> AuxiliaryPDE;
  typedef PDENDConvertSpace<PhysD1, AuxiliaryPDE> NDAuxiliaryPDE;

  typedef PDEHomotopy<NDPrimaryPDE, NDAuxiliaryPDE> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCTypeFunctionLinearRobin_mitLG BCType;
  typedef BCAdvectionDiffusion<PhysD1,BCType> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;

  typedef BCHomotopy<NDPrimaryPDE, NDAuxiliaryPDE, BCClass, BCClass> NDBC;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertHomotopy, boost::mpl::vector<NDBC>,
                                     AlgEqSetTraits_Sparse, DGBR2,
                                     XField<PhysD1, TopoD1>> AlgebraicEquationSet_DGClass;
  typedef AlgebraicEquationSet_DGClass::BCParams BCParams;

  PyDict NonLinearSolverDict, NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;

  NewtonSolverDict[NonLinearSolverParam::params.NonLinearSolver.Solver] = NonLinearSolverParam::params.NonLinearSolver.Newton;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  //std::cout << NonLinearSolverDict << std::endl;

  NonLinearSolverParam::checkInputs(NonLinearSolverDict);

  // Generate PDEs
  Real u = 5.0;
  AdvectiveFlux1D_Uniform adv(u);
  AdvectiveFlux1D_None NoAdv;

  Real kxx = 0.5;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  NDPrimaryPDE primarypde(adv, visc, source);
  NDAuxiliaryPDE auxiliarypde(NoAdv, visc, source);

  // Set up our Homotopy pde
  GlobalTime lambda(0);
  NDPDEClass pde(lambda.time, primarypde, auxiliarypde);

  Real a0 = 0.0;
  Real ax = 1.0;
  // Create a Solution dictionary
  PyDict SolnDict;
  SolnDict[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Linear;
  SolnDict["a0"] = a0;
  SolnDict["ax"] = ax;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function] = SolnDict;

  PyDict PyBCList;
  PyBCList["TheBCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TheBCName"] = {0, 1};

  const std::vector<int> BoundaryGroups = {0,1};

  // Grid
  XField1D xfld( 9 );

  // solution
  int order = 0;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

  // initial guess
  qfld = 0;

  // lifting operators (not needed, but not optional yet)
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Legendre);
  for (int i = 0; i < rfld.nDOF(); i++)
    rfld.DOF(i) = 0;

  // lagrange multipliers not actually used either
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld( xfld, order , BasisFunctionCategory_Legendre, BoundaryGroups);

  lgfld = 0;

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {1e-12, 1e-12};
  // Create the spatial discretization
  AlgebraicEquationSet_DGClass AlgEqSet(xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder, ResidualNorm_Default, tol,
                                        {0}, {0}, PyBCList, BCBoundaryGroups, lambda);

  // Create the Homotopy continuation class
  Homotopy<PhysD1, NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>>
     HTC(xfld, qfld, NonLinearSolverDict, pde, {0}, AlgEqSet, lambda.time);

  // Iterate the homotopy to convergence
//  BOOST_CHECK( HTC.iterate(300) );

  // Did we get to the final PDE
//  BOOST_CHECK_CLOSE( 1.0, lambda.time, 1e-12 );

#ifdef SANS_VERBOSE
  // Tecplot dump
  std::string filename = "tmp/slnHDG.plt";
  output_Tecplot( qfld, filename );
#endif

#endif
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
