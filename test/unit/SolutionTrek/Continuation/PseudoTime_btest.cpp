// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PseudoTime_btest
// testing of PseudoTime_btest classes

#include "Python/PyDict.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <limits>

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/SparseLinAlg/tools/norm.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern_Transpose.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "Field/FieldArea_DG_Cell.h"

#include "pde/BCParameters.h" //Neded because a custom BC vector is created here

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"

#include "SolutionTrek/Continuation/PseudoTime/SetLocalTimeStep.h"
#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"

#include "Meshing/XField1D/XField1D.h"

// This is only here because RiccatiEquation is special
#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

// This is only here because RiccatiEquation is special
#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"

#include "unit/SolutionTrek/Ricatti_btest.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

using namespace SANS;

namespace SANS
{

// A derived class that inherits from PseudoTime to expose its protected members/methods for testing
// NOTE: intended ONLY for unit testing!
template<class SystemMatrix>
class PseudoTime_UnitTest : public PseudoTime<SystemMatrix>
{
public:
  typedef PseudoTime<SystemMatrix> BaseType;

  using BaseType::PseudoTime; // inherit constructors

  // Expose protected members and methods for testing
  using BaseType::updateCFL;

  using BaseType::invCFL_max_;
  using BaseType::invCFL_;
  using BaseType::invCFL_min_;
  using BaseType::CFLDecreaseFactor_;
  using BaseType::CFLIncreaseFactor_;
  using BaseType::CFLPartialStepDecreaseFactor_;

  using BaseType::algEqSetPTC_;
  using BaseType::spatial_;

  using BaseType::verbose_;
};

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( PseudoTime_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SetLocalTimeStep_Spatial_test )
{
  typedef PDENDConvertSpace<PhysD1, RiccatiEquation> NDPDEClass;

  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::MatrixQ<Real> MatrixQ;

  Real a = 2, b = 3;
  NDPDEClass pde(a, b);

  Real h = 1.75;
  XField1D xfld( 1, 0, 2*h );

  // Using a P0 DG solution field, as we don't have a 0D ODE solver framework

  // solution
  int order = 0;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

  // grid size field
  HField_DG<PhysD1, TopoD1> hfld(xfld);

  // initial guess can't be zero
  qfld = 3;

  ArrayQ q = qfld.DOF(0);

  Real x, time, speed;
  static_cast<RiccatiEquation&>(pde).speedCharacteristic(x, time, q, speed);

  ArrayQ qx;
  MatrixQ dsdu = 0;
  static_cast<RiccatiEquation&>(pde).jacobianSource( x, time, q, qx, dsdu );

  Real dtisource = dsdu;

  Real invCFL = 12;

  // exact dti
  Real dti = invCFL * (speed / h + dtisource);

  QuadratureOrder quadratureOrder( xfld, -1 );

  {
    // DG inverse time step field
    Field_DG_Cell<PhysD1, TopoD1, ArrayQ> dtifld(xfld, 0, BasisFunctionCategory_Legendre);

    // Initialize all DOF to zero for accumulation
    for (int n = 0; n < dtifld.nDOF(); n++)
      dtifld.DOF(n) = 0;

    for_each_CellGroup<TopoD1>::apply(
        SetLocalTimeStep<NDPDEClass, XField<PhysD1, TopoD1>>(pde, quadratureOrder.cellOrders, {0}, invCFL),
        (xfld, (qfld, hfld, dtifld)));

    BOOST_CHECK_CLOSE(dti, dtifld.DOF(0), 1e-12);
  }

  {
    // CG inverse time step field
    Field_CG_Cell<PhysD1, TopoD1, ArrayQ> dtifld(xfld, 1, BasisFunctionCategory_Lagrange);

    // Initialize all DOF to zero for accumulation
    for (int n = 0; n < dtifld.nDOF(); n++)
      dtifld.DOF(n) = 0;

    for_each_CellGroup<TopoD1>::apply(
        SetLocalTimeStep<NDPDEClass, XField<PhysD1, TopoD1>>(pde, quadratureOrder.cellOrders, {0}, invCFL),
        (xfld, (qfld, hfld, dtifld)));

    for (int n = 0; n < dtifld.nDOF(); n++)
      BOOST_CHECK_CLOSE(dti, dtifld.DOF(n), 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SetLocalTimeStep_SpaceTime_test )
{
  typedef PDENDConvertSpaceTime<PhysD1, RiccatiEquation> NDPDEClass;

  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  Real a = 2, b = 3;
  NDPDEClass pde(a, b);

  Real dx = 1.75;
  XField2D_Box_Triangle_X1 xfld( 1, 1, 0, dx, 0, dx );

  // Using a P0 DG solution field, as we don't have a 0D ODE solver framework

  // solution
  int order = 0;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

  // grid size field
  HField_DG<PhysD2, TopoD2> hfld(xfld);

  // initial guess can't be zero
  qfld = 3;

  Real invCFL = 12;

  // exact dti
  Real perimeter = dx * (1 + 1 + sqrt(2.0));
  Real area = 0.5*dx*dx;
  Real h = area / perimeter;
  Real dti = invCFL / h;

  QuadratureOrder quadratureOrder( xfld, -1 );

  {
    // DG inverse time step field
    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> dtifld(xfld, 0, BasisFunctionCategory_Legendre);

    // Initialize all DOF to zero for accumulation
    for (int n = 0; n < dtifld.nDOF(); n++)
      dtifld.DOF(n) = 0;

    for_each_CellGroup<TopoD2>::apply(
        SetLocalTimeStep<NDPDEClass, XField<PhysD2, TopoD2>>(pde, quadratureOrder.cellOrders, {0}, invCFL),
        (xfld, (qfld, hfld, dtifld)));

    BOOST_CHECK_CLOSE(dti, dtifld.DOF(0), 1e-12);
    BOOST_CHECK_CLOSE(dti, dtifld.DOF(1), 1e-12);
  }

  {
    // CG inverse DG time step field
    Field_CG_Cell<PhysD2, TopoD2, ArrayQ> dtifld(xfld, 1, BasisFunctionCategory_Lagrange);

    // Initialize all DOF to zero for accumulation
    for (int n = 0; n < dtifld.nDOF(); n++)
      dtifld.DOF(n) = 0;

    for_each_CellGroup<TopoD2>::apply(
        SetLocalTimeStep<NDPDEClass, XField<PhysD2, TopoD2>>(pde, quadratureOrder.cellOrders, {0}, invCFL),
        (xfld, (qfld, hfld, dtifld)));

    for (int n = 0; n < dtifld.nDOF(); n++)
      BOOST_CHECK_CLOSE(dti, dtifld.DOF(n), 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PythonDictionaryConstructor_updateCFL_test )
{
  PyDict PseudoTimeDict, NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NonLinearSolverParam::params.NonLinearSolver.Solver] = NonLinearSolverParam::params.NonLinearSolver.Newton;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  PseudoTimeDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  PseudoTimeDict[PseudoTimeParam::params.invCFL] = 10;
  PseudoTimeDict[PseudoTimeParam::params.invCFL_max] = 100;
  PseudoTimeDict[PseudoTimeParam::params.invCFL_min] = 0.1;

  PseudoTimeDict[PseudoTimeParam::params.CFLDecreaseFactor] = 0.5;
  PseudoTimeDict[PseudoTimeParam::params.CFLIncreaseFactor] = 10;
  PseudoTimeDict[PseudoTimeParam::params.CFLPartialStepDecreaseFactor] = 0.6;

  // Check the inputs before they are extracted in the class
  PseudoTimeParam::checkInputs(PseudoTimeDict);

  typedef SANS::RiccatiEquation RiccatiEquation;

  typedef PDENDConvertSpace<PhysD1, RiccatiEquation> NDPDEClass;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, boost::mpl::vector<>, AlgEqSetTraits_Sparse, DGAdv,
                                           XField<PhysD1, TopoD1>> AlgebraicEquationSet_DGClass;
  typedef AlgebraicEquationSet_PTC<NDPDEClass, AlgEqSetTraits_Sparse, PTC, XField<PhysD1, TopoD1>> AlgebraicEquationSet_PTCClass;

  typedef AlgebraicEquationSet_DGClass::SystemMatrix SystemMatrix;

  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  Real a = 2, b = 3;
  NDPDEClass pde(a, b);

  XField1D xfld( 1 );

  // Using a P0 DG solution field, as we don't have a 0D ODE solver framework

  // solution
  int order = 0;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

  // initial guess can't be zero
  qfld = 1;

  // lagrange multipliers not actually used
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre, {} );

  lgfld = 0;

  // empty boundary condition information
  PyDict PyBCList;
  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {1e-12, 1e-12};

  // Create the spatial discretization
  AlgebraicEquationSet_DGClass AlgEqSet(xfld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_Default, tol,
                                        {0}, { }, PyBCList, BCBoundaryGroups);
  AlgebraicEquationSet_PTCClass AlgEqSetPTC(xfld, qfld, pde, quadratureOrder, {0}, AlgEqSet);

  // Create the pseudo time continuation class
  PseudoTime_UnitTest< SystemMatrix > PTC(PseudoTimeDict, AlgEqSetPTC);

  { // check constructor
    BOOST_CHECK( PTC.invCFL_max_ == PseudoTimeDict.get(PseudoTimeParam::params.invCFL_max) );
    BOOST_CHECK( PTC.invCFL_ == PseudoTimeDict.get(PseudoTimeParam::params.invCFL) );
    BOOST_CHECK( PTC.invCFL_min_ == PseudoTimeDict.get(PseudoTimeParam::params.invCFL_min) );

    BOOST_CHECK( PTC.CFLDecreaseFactor_ == PseudoTimeDict.get(PseudoTimeParam::params.CFLDecreaseFactor) );
    BOOST_CHECK( PTC.CFLIncreaseFactor_ == PseudoTimeDict.get(PseudoTimeParam::params.CFLIncreaseFactor) );
    BOOST_CHECK( PTC.CFLPartialStepDecreaseFactor_ == PseudoTimeDict.get(PseudoTimeParam::params.CFLPartialStepDecreaseFactor) );

    BOOST_CHECK( &PTC.algEqSetPTC_ == &AlgEqSetPTC );
    BOOST_CHECK( &PTC.spatial_ == &AlgEqSetPTC.getSpatialAlgebraicEquationSet() );

    BOOST_CHECK( PTC.verbose_ == false );
  }

  // iterate the pseudo time to convergence
  BOOST_CHECK( PTC.iterate(20) );

  // check the answer is correct
  BOOST_CHECK_CLOSE( a/b, qfld.DOF(0), 1e-8 );

  //----------------------------//
  // ----- Test updateCFL ----- //
  //----------------------------//
  SolveStatus status;
  Real invCFL_currentTrue;

  const Real invCFL_max_original = PTC.invCFL_max_;

  {
    status.linesearch.status = LineSearchStatus::FullStep;

    invCFL_currentTrue = PTC.invCFL_min_*0.9; // smaller than PTC.invCFL_min_
    PTC.invCFL_ = invCFL_currentTrue;
    PTC.updateCFL(status);
    BOOST_CHECK_EQUAL( PTC.invCFL_min_, PTC.invCFL_ );

    invCFL_currentTrue = PTC.invCFL_min_*PTC.CFLIncreaseFactor_*1.1; // greater than PTC.invCFL_min_*PTC.CFLIncreaseFactor_
    PTC.invCFL_ = invCFL_currentTrue;
    PTC.updateCFL(status);
    BOOST_CHECK_EQUAL( invCFL_currentTrue/PTC.CFLIncreaseFactor_, PTC.invCFL_ );
  }

  {
    status.linesearch.status = LineSearchStatus::PartialStep;

    invCFL_currentTrue = PTC.invCFL_max_*0.9*1.1; // greater than PTC.invCFL_max_*0.9
    PTC.invCFL_ = invCFL_currentTrue;
    PTC.updateCFL(status);
    BOOST_CHECK_EQUAL( PTC.invCFL_max_, PTC.invCFL_ );

    invCFL_currentTrue = PTC.invCFL_max_*0.9*PTC.CFLPartialStepDecreaseFactor_; // smaller than PTC.invCFL_max_*0.9
    PTC.invCFL_ = invCFL_currentTrue;
    PTC.updateCFL(status);
    BOOST_CHECK_EQUAL( invCFL_currentTrue/PTC.CFLPartialStepDecreaseFactor_, PTC.invCFL_ );
  }

  {
    status.linesearch.status = LineSearchStatus::LimitedStep;

    invCFL_currentTrue = PTC.invCFL_max_*0.9*1.1; // greater than PTC.invCFL_max_*0.9
    PTC.invCFL_ = invCFL_currentTrue;
    PTC.updateCFL(status);
    //BOOST_CHECK_EQUAL( PTC.invCFL_max_, PTC.invCFL_ );
    BOOST_CHECK_EQUAL( invCFL_currentTrue, PTC.invCFL_ );

    invCFL_currentTrue = PTC.invCFL_max_*0.9*PTC.CFLPartialStepDecreaseFactor_; // smaller than PTC.invCFL_max_*0.9
    PTC.invCFL_ = invCFL_currentTrue;
    PTC.updateCFL(status);
    //BOOST_CHECK_EQUAL( invCFL_currentTrue/0.9, PTC.invCFL_ );
    BOOST_CHECK_EQUAL( invCFL_currentTrue, PTC.invCFL_ );
  }

  {
    status.linesearch.status = LineSearchStatus::Failure;

    invCFL_currentTrue = PTC.invCFL_max_*PTC.CFLDecreaseFactor_*1.1; // greater than PTC.invCFL_max_*PTC.CFLDecreaseFactor_
    PTC.invCFL_ = invCFL_currentTrue;
    PTC.updateCFL(status);
    BOOST_CHECK_EQUAL( PTC.invCFL_max_, PTC.invCFL_ );

    invCFL_currentTrue = PTC.invCFL_max_*PTC.CFLDecreaseFactor_*0.9; // smaller than PTC.invCFL_max_*PTC.CFLDecreaseFactor_
    PTC.invCFL_ = invCFL_currentTrue;
    PTC.updateCFL(status);
    BOOST_CHECK_EQUAL( invCFL_currentTrue/PTC.CFLDecreaseFactor_, PTC.invCFL_ );


    invCFL_currentTrue = 0; // zero
    const Real tol = sqrt(std::numeric_limits<Real>::epsilon())/PTC.CFLDecreaseFactor_;

    PTC.invCFL_max_ = tol + 1;
    PTC.invCFL_ = invCFL_currentTrue;
    PTC.updateCFL(status);
    BOOST_CHECK_EQUAL( tol, PTC.invCFL_ );

    PTC.invCFL_max_ = 0;
    PTC.invCFL_ = invCFL_currentTrue;
    PTC.updateCFL(status);
    BOOST_CHECK_EQUAL( PTC.invCFL_max_, PTC.invCFL_ );

    PTC.invCFL_max_ = invCFL_max_original; // reset invCFL_max_ to the original value in case it gets used later
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Riccati_Solve )
{
  PyDict NonLinearSolverDict, NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NonLinearSolverParam::params.NonLinearSolver.Solver] = NonLinearSolverParam::params.NonLinearSolver.Newton;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  //std::cout << NonLinearSolverDict << std::endl;

  typedef SANS::RiccatiEquation RiccatiEquation;

  NonLinearSolverParam::checkInputs(NonLinearSolverDict);

  typedef PDENDConvertSpace<PhysD1, RiccatiEquation> NDPDEClass;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, boost::mpl::vector<>, AlgEqSetTraits_Sparse, DGAdv,
                                           XField<PhysD1, TopoD1>> AlgebraicEquationSet_DGClass;
  typedef AlgebraicEquationSet_PTC<NDPDEClass, AlgEqSetTraits_Sparse, PTC, XField<PhysD1, TopoD1>> AlgebraicEquationSet_PTCClass;

  typedef AlgebraicEquationSet_DGClass::SystemMatrix SystemMatrix;

  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  Real a = 2, b = 3;
  NDPDEClass pde(a, b);

  XField1D xfld( 1 );

  // Using a P0 DG solution field, as we don't have a 0D ODE solver framework

  // solution
  int order = 0;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

  // initial guess can't be zero
  qfld = 1;

  // lagrange multipliers not actually used either
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre, {} );

  lgfld = 0;

  // empty boundary condition information
  PyDict PyBCList;
  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {1e-11, 1e-11};

  // Create the spatial discretization
  AlgebraicEquationSet_DGClass AlgEqSet(xfld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_Default, tol,
                                        {0}, { }, PyBCList, BCBoundaryGroups);
  AlgebraicEquationSet_PTCClass AlgEqSetPTC(xfld, qfld, pde, quadratureOrder, {0}, AlgEqSet);

  Real invCFL = 0.1;
  Real invCFL_min = 0.0;
  Real invCFL_max = 1000.0;
  Real CFLDecreaseFactor = 0.9;
  Real CFLIncreaseFactor = 1.1;
  Real CFLPartialStepDecreaseFactor = 0.9;

  // Create the pseudo time continuation class
  PseudoTime_UnitTest< SystemMatrix >
     PTC(invCFL, invCFL_max, invCFL_min, CFLDecreaseFactor, CFLIncreaseFactor, CFLPartialStepDecreaseFactor,
         NonLinearSolverDict, AlgEqSetPTC, true);

  { // check constructor
    BOOST_CHECK( PTC.invCFL_max_ == invCFL_max );
    BOOST_CHECK( PTC.invCFL_ == invCFL );
    BOOST_CHECK( PTC.invCFL_min_ == invCFL_min );

    BOOST_CHECK( PTC.CFLDecreaseFactor_ == CFLDecreaseFactor );
    BOOST_CHECK( PTC.CFLIncreaseFactor_ == CFLIncreaseFactor );

    BOOST_CHECK( &PTC.algEqSetPTC_ == &AlgEqSetPTC );
    BOOST_CHECK( &PTC.spatial_ == &AlgEqSetPTC.getSpatialAlgebraicEquationSet() );

    BOOST_CHECK( PTC.verbose_ == true );
  }

  // iterate the pseudo time to convergence
  BOOST_CHECK( PTC.iterate(20) );

  // check the answer is correct
  BOOST_CHECK_CLOSE( a/b, qfld.DOF(0), 1e-8 );

  // iterate the pseudo time to convergence using solve
  qfld = 1;
  BOOST_CHECK( PTC.solve() );

  // check the answer is correct
  BOOST_CHECK_CLOSE( a/b, qfld.DOF(0), 1e-8 );

  // call the empty dump function to suppress an intel warning saying it's never used
  pde.dump(0);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Jacobian_test )
{
  typedef PDENDConvertSpace<PhysD1, RiccatiEquation> NDPDEClass;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, boost::mpl::vector<>, AlgEqSetTraits_Dense, DGAdv,
                                           XField<PhysD1, TopoD1>> AlgebraicEquationSet_DGClass;
  typedef AlgebraicEquationSet_PTC<NDPDEClass, AlgEqSetTraits_Dense, PTC, XField<PhysD1, TopoD1>> AlgebraicEquationSet_PTCClass;
  typedef AlgebraicEquationSet_PTCClass::SystemVector SystemVector;
  typedef AlgebraicEquationSet_PTCClass::SystemMatrix SystemMatrix;
  typedef AlgebraicEquationSet_PTCClass::SystemNonZeroPattern SystemNonZeroPattern;

  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  Real a = 2, b = 0; // b = 0 -> linear equation
  NDPDEClass pde(a, b);

  XField1D xfld( 1, 0, 1.25 );

  // Using a P0 DG solution field, as we don't have a 0D ODE solver framework

  // solution
  int order = 0;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

  // initial guess can't be zero
  qfld = 1;

  // lagrange multipliers not actually used either
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre, {} );
  lgfld = 0;

  // empty boundary condition information
  PyDict PyBCList;
  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {1e-11, 1e-11};

  // Create the spatial discretization
  AlgebraicEquationSet_DGClass AlgEqSet(xfld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_Default, tol,
                                        {0}, { }, PyBCList, BCBoundaryGroups);
  AlgebraicEquationSet_PTCClass AlgEqSetPTC(xfld, qfld, pde, quadratureOrder, {0}, AlgEqSet);

  Real invCFL = 10;
  AlgEqSetPTC.setLocalTimeStepField(invCFL);

  SystemVector rsd0(AlgEqSetPTC.vectorEqSize());
  SystemVector rsd1(AlgEqSetPTC.vectorEqSize());

  SystemNonZeroPattern nz(AlgEqSetPTC.matrixSize());
  SystemMatrix mtx(nz);

  mtx = 0;
  AlgEqSetPTC.jacobian(mtx);

  rsd0 = 0;
  AlgEqSetPTC.residual(rsd0);

  qfld.DOF(0) += 1;

  rsd1 = 0;
  AlgEqSetPTC.residual(rsd1);

  Real diff = rsd1[0][0] - rsd0[0][0];

  BOOST_CHECK_CLOSE(diff, mtx(0,0)(0,0), 1e-12);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
