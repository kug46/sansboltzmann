INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

ADD_SUBDIRECTORY( Continuation )
ADD_SUBDIRECTORY( HarmonicBalance )
ADD_SUBDIRECTORY( TimeMarching )
