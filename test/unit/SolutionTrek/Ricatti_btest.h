// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RICCATI_H
#define RICCATI_H

// The non-linear Riccati equation,
//
// dq/dt = -a*q + b*q*q
//
// where a and b are constants, is useful for debugging time-marching and continuation methods

namespace SANS
{

class RiccatiEquation
{
public:
  typedef PhysD1 PhysDim;
  static const int D = 1;   // physical dimensions
  static const int N = 1;   // total solution variables

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using MatrixQ = T; // matrices

  typedef DLA::VectorS<D,Real> VectorX;

  RiccatiEquation( const Real a, const Real b ) : a_(a), b_(b) {}
  ~RiccatiEquation() {}

  RiccatiEquation( const RiccatiEquation& ) = delete;
  RiccatiEquation& operator=( const RiccatiEquation& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return false; }
  bool hasFluxViscous() const { return false; }
  bool hasSource() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return false; }

  bool needsSolutionGradientforSource() const { return false; }

  // unsteady conservative flux: U(Q)
  template <class T>
  void fluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& ft ) const
  {
    ft += q;
  }

  // master state: U(Q)
  template <class T>
  void masterState(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& uCons ) const
  {
    uCons = q;
  }

  // jacobian of master state wrt q: dU(Q)/dQ
  template<class T>
  void jacobianMasterState(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
  {
    dudq = DLA::Identity();
  }

  // unsteady conservative flux: d(U)/d(t)
  template <class T>
  void strongFluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& dudt ) const
  {
    dudt += qt;
  }

  // advective flux: F(Q)
  template <class Tq, class Tf>
  void fluxAdvective(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, ArrayQ<Tf>& f ) const {}

  template <class Tq, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, ArrayQ<Tf>& f ) const {}

  template <class T>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& nt, ArrayQ<T>& f ) const {}

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T>
  void jacobianFluxAdvective(
      const Real& x,  const Real& time, const ArrayQ<T>& q,
      MatrixQ<T>& ax ) const {}


  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const Real& nx,
      MatrixQ<T>& a ) const {}

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& time, const ArrayQ<T>& q, const Real& nx, const Real& nt,
      MatrixQ<T>& a ) const {}

  // strong form advective fluxes
  template <class T>
  void strongFluxAdvective(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      ArrayQ<T>& strongPDE ) const {}

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Tf>& f ) const {}

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      const Real& nx, ArrayQ<Tf>& f ) const {}

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Tk>& kxx) const {}

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class T>
  void jacobianFluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& ax ) const {}

  // strong form viscous fluxes
  template <class T>
  void strongFluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      const ArrayQ<T>& qxx,
      ArrayQ<T>& strongPDE ) const {}

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg>
  void source(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ< typename promote_Surreal<Tq,Tg>::type >& source ) const
  {
    source += -a_*q + b_*q*q;
  }

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T>
  void jacobianSource(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      MatrixQ<T>& dsdu ) const
  {
    dsdu += -a_ + 2*b_*q;
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T>
  void jacobianSourceAbsoluteValue(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      MatrixQ<T>& dsdu ) const
  {
    dsdu += fabs(-a_ + 2*b_*q);
  }

  // right-hand-side forcing function
  template <class T>
  void forcingFunction( const Real& x, const Real& time, ArrayQ<T>& source ) const {}

  // characteristic speed (needed for timestep)
  template <class T>
  void speedCharacteristic(
      const Real& x, const Real& time,
      const Real& dx, const ArrayQ<T>& q, Real& speed ) const { speed = 1; }

  // characteristic speed (needed for timestep)
  template <class T>
  void speedCharacteristic(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, Real& speed ) const { speed = 1; }

  // is state physically valid
  template <class T>
  bool isValidState( const ArrayQ<T>& q ) const { return true; }

  // update fraction needed for physically valid state
  template <class T>
  void updateFraction(
      const Real& x, const Real& time, const ArrayQ<T>& q, const ArrayQ<T>& dq,
      Real maxChangeFraction, Real& updateFraction ) const { updateFraction = 1; }

  // set from primitive variable array
  template <class T>
  void setDOFFrom(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const {}

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
  {
    SANS_ASSERT(rsdPDEOut.m() == 1);
    rsdPDEOut[0] = rsdPDEIn;
  }

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
  {
    SANS_ASSERT(rsdAuxOut.m() == 1);
    rsdAuxOut[0] = rsdAuxIn;
  }

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
  {
    SANS_ASSERT(rsdBCOut.m() == 1);
    rsdBCOut[0] = rsdBCIn;
  }

  // how many residual equations are we dealing with
  int nMonitor() const
  {
    return 1;
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const {}

private:
  const Real a_, b_;
};

}

#endif //RICCATI_H
