// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// test of XfoilPanel

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>
#include <type_traits>

#include "PanelMethod/XfoilPanel.h"

#include "unit/UnitGrids/XField2D_Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Line_X1_2Group_AirfoilWithWake.h"

//----------------------------------------------------------------------------//

using namespace std;

namespace SANS
{
typedef XfoilPanel PanelType;
typedef typename PanelType::VectorX VectorX;
typedef typename PanelType::GamFieldType<Real> GamFieldType;
typedef typename PanelType::LamFieldType<Real> LamFieldType;
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( XfoilPanel_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  // static

  const int PhysDimTrue = 2;

  BOOST_CHECK_EQUAL(0, PanelType::domainName::airfoil);
  BOOST_CHECK_EQUAL(1, PanelType::domainName::wake);

  BOOST_CHECK( (std::is_same<PhysD2,PanelType::PhysDim>::value) );
  BOOST_CHECK( (std::is_same<TopoD1,PanelType::TopoDim>::value) );

  BOOST_CHECK(PhysDimTrue == PanelType::D);

  BOOST_CHECK(1 == PanelType::order_xfld);
  BOOST_CHECK(1 == PanelType::order_gam);
  BOOST_CHECK(0 == PanelType::order_lam);

  BOOST_CHECK( (std::is_same<Real,PanelType::ArrayQ<Real>>::value) );
  BOOST_CHECK( (std::is_same<Real,PanelType::MatrixQ<Real>>::value) );
  BOOST_CHECK( (std::is_same<DLA::VectorS<PhysDimTrue,Real>,PanelType::VectorX>::value) );

  BOOST_CHECK( (std::is_same<XField<PhysD2,TopoD1>,PanelType::XFieldType>::value) );
  BOOST_CHECK( (std::is_same<Field_CG_Cell<PhysD2,TopoD1,Real>,typename PanelType::GamFieldType<Real>>::value) );
  BOOST_CHECK( (std::is_same<Field_DG_Cell<PhysD2,TopoD1,Real>,typename PanelType::LamFieldType<Real>>::value) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( NodePanelCount_test )
{
  // Parameters
  const VectorX Vinf = {1.0,0.5};
  const Real p0 = 1.e5, T0 = 300;

  // Panel grid
  std::vector<VectorX> coordinates_a = { {1.0,  0.5E-2},
                                         {0.5,  0.1E-1},
                                         {0.,   0.},
                                         {0.5, -0.1E-1},
                                         {1.0, -0.5E-2}};
  // node & panel counts
  const int nnode_a = coordinates_a.size(); // total airfoil nodes
  const int npanel_a = nnode_a - 1;         // total airfoil panels

  { // without wake
    XField2D_Line_X1_1Group xfld(coordinates_a);
    std::vector<int> CellGroups = {0};

    const int nnode_w = 0;  // total wake nodes
    const int npanel_w = 0; // total wake panels

    PanelType panel(Vinf,p0,T0,xfld,CellGroups);

    BOOST_CHECK_EQUAL( nnode_a, panel.nNodeAirfoil() );
    BOOST_CHECK_EQUAL( nnode_w, panel.nNodeWake() );

    BOOST_CHECK_EQUAL( npanel_a, panel.nPanelAirfoil() );
    BOOST_CHECK_EQUAL( npanel_w, panel.nPanelWake() );
  }

  { // with wake
    std::vector<VectorX> coordinates_w = { {1.0001, 0.0},
                                           {1.2,    0.0},
                                           {1.4,    0.0} };
    XField2D_Line_X1_2Group_AirfoilWithWake xfld(coordinates_a,coordinates_w);
    std::vector<int> CellGroups = {0,1};

    const int nnode_w = coordinates_w.size(); // total wake nodes
    const int npanel_w = nnode_w - 1;         // total wake panels

    PanelType panel(Vinf,p0,T0,xfld,CellGroups);

    BOOST_CHECK_EQUAL( nnode_a, panel.nNodeAirfoil() );
    BOOST_CHECK_EQUAL( nnode_w, panel.nNodeWake() );

    BOOST_CHECK_EQUAL( npanel_a, panel.nPanelAirfoil() );
    BOOST_CHECK_EQUAL( npanel_w, panel.nPanelWake() );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FiniteTE_AirfoilAndWake_test )
{
  const Real tol = 1.5e-12;

  // Parameters
  const VectorX Vinf = {1.0,0.5};
  const Real p0 = 1.e5, T0 = 300;

  // Panel grid
  std::vector<VectorX> coordinates_a = { {1.0,  0.5E-2},
                                         {0.5,  0.1E-1},
                                         {0.,   0.},
                                         {0.5, -0.1E-1},
                                         {1.0, -0.5E-2}};
  std::vector<VectorX> coordinates_w = { {1.0001, 0.0},
                                         {1.2,    0.0},
                                         {1.4,    0.0} };
  XField2D_Line_X1_2Group_AirfoilWithWake xfld(coordinates_a,coordinates_w);
  std::vector<int> CellGroups = {0,1};

  PanelType panel(Vinf,p0,T0,xfld,CellGroups);

  const int nnode_a = panel.nNodeAirfoil(); // total airfoil nodes
  const int nnode_w = panel.nNodeWake();    // total wake nodes

  // member accessors
  BOOST_CHECK( (&xfld == &panel.getXfield()) ); // member access "." takes precedence over address-of "&"

  std::vector<int> CellGroups_actual = panel.getPanelCellGroups();

  BOOST_CHECK_EQUAL(2, CellGroups_actual.size());
  BOOST_CHECK_EQUAL(0, CellGroups_actual.at(0));
  BOOST_CHECK_EQUAL(1, CellGroups_actual.at(1));

  SANS_CHECK_CLOSE(Vinf[0], (panel.getVinf())[0], tol, tol);
  SANS_CHECK_CLOSE(Vinf[1], (panel.getVinf())[1], tol, tol);
  SANS_CHECK_CLOSE(p0, panel.getp0(), tol, tol);
  SANS_CHECK_CLOSE(T0, panel.getT0(), tol, tol);

  // TE gap check
  BOOST_CHECK_EQUAL(true, panel.isFiniteTE());

  // bisector
  VectorX bisectorTrue = {1.0, 0.0};
  VectorX bisector = panel.bisectorTE();
  SANS_CHECK_CLOSE(bisectorTrue[0], bisector[0], tol, tol);
  SANS_CHECK_CLOSE(bisectorTrue[1], bisector[1], tol, tol);

  // streamfunction calculations
  GamFieldType gamfld(xfld, PanelType::order_gam, BasisFunctionCategory_Hierarchical,{0});
  for (int i = 0; i < gamfld.nDOF(); i++)
    gamfld.DOF(i) = 1+i;

  LamFieldType lamfld(xfld, PanelType::order_lam, BasisFunctionCategory_Legendre);
  for (int i = 0; i < lamfld.nDOF(); i++)
    lamfld.DOF(i) = 1+i;

  // Psi on airfoil & Psi_nep on airfoil and wake
  std::vector<Real> PsiTrue = { -1.3152360718463099e+00,
                                -9.4588937468547640e-01,
                                 5.4257613643632574e-02,
                                -1.2146197952582525e+00,
                                -1.3994134487274228e+00 };
  std::vector<Real> Psi_nepTrue = { 6.6528039683205282e+00,
                                    1.3401706751429447e+01,
                                    4.7991919537879474e-01,
                                   -1.3510365443000511e+01,
                                   -5.4102752426404246e+00,
                                   11.157998318425735,
                                   6.5320295504919192e+00,
                                   4.7125557942070113e+00 };
  for (int i = 0; i < nnode_a; ++i)
  {
    VectorX Xep = coordinates_a.at(i);

    Real Psi, Psi_nep;
    panel.Psicalc(gamfld,lamfld,Xep,i,Psi,Psi_nep);

    SANS_CHECK_CLOSE(PsiTrue[i], Psi, tol, tol);
    SANS_CHECK_CLOSE(Psi_nepTrue[i], Psi_nep, tol, tol);
  }

  for (int i = 0; i < nnode_w; ++i)
  {
    VectorX Xep = coordinates_w.at(i);
    const int inode = i + nnode_a;

    Real Psi_dummy, Psi_nep;
    panel.Psicalc(gamfld,lamfld,Xep,inode,Psi_dummy,Psi_nep);

//    SANS_CHECK_CLOSE(PsiTrue[inode], Psi_dummy, tol, tol); //TODO: this is not useful and it's also cubersome to test
    SANS_CHECK_CLOSE(Psi_nepTrue[inode], Psi_nep, tol, tol);
  }

  // Uecalc

  // Ue on airfoil
  for (int i = 0; i < nnode_a; ++i)
  {
    VectorX Xep = coordinates_a.at(i);

    Real Ue = 0;
    panel.Uecalc(gamfld,lamfld,Xep,i,Ue);

    SANS_CHECK_CLOSE(-gamfld.DOF(i), Ue, tol, tol);
  }

  // Ue on wake
  for (int i = 0; i < nnode_w; i++)
  {
    VectorX Xep = coordinates_w.at(i);
    const int inode = i + nnode_a;

    Real Ue = 0;
    panel.Uecalc(gamfld,lamfld,Xep,inode,Ue);

    if (inode==nnode_a)
    {
      SANS_CHECK_CLOSE(0.5*(gamfld.DOF(0)-gamfld.DOF(nnode_a-1)), Ue, tol, tol);
    }
    else
    {
      SANS_CHECK_CLOSE(Psi_nepTrue[inode], Ue, tol, tol);
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SharpTE_AirfoilAndWake_test )
{
  const Real tol = 1.5e-12;

  // Parameters
  const VectorX Vinf = {1.0,0.5};
  const Real p0 = 1.e5, T0 = 300;

  // Panel grid
  std::vector<VectorX> coordinates_a = { {1.0,  0},
                                         {0.5,  0.1E-1},
                                         {0.,   0.},
                                         {0.5, -0.1E-1},
                                         {1.0,  0}};
  std::vector<VectorX> coordinates_w = { {1.0001, 0.0},
                                         {1.2,    0.0},
                                         {1.4,    0.0} };
  XField2D_Line_X1_2Group_AirfoilWithWake xfld(coordinates_a,coordinates_w);
  std::vector<int> CellGroups = {0,1};

  PanelType panel(Vinf,p0,T0,xfld,CellGroups);

  const int nnode_a = panel.nNodeAirfoil(); // total airfoil nodes
  const int nnode_w = panel.nNodeWake();    // total wake nodes

  // TE gap check
  BOOST_CHECK_EQUAL(false, panel.isFiniteTE());

  // bisector
  VectorX bisectorTrue = {1.0, 0.0};
  VectorX bisector = panel.bisectorTE();
  SANS_CHECK_CLOSE(bisectorTrue[0], bisector[0], tol, tol);
  SANS_CHECK_CLOSE(bisectorTrue[1], bisector[1], tol, tol);

  // streamfunction calculations
  GamFieldType gamfld(xfld, PanelType::order_gam, BasisFunctionCategory_Hierarchical);
  for (int i = 0; i < gamfld.nDOF(); i++)
    gamfld.DOF(i) = 1+i;

  LamFieldType lamfld(xfld, PanelType::order_lam, BasisFunctionCategory_Legendre);
  for (int i = 0; i < lamfld.nDOF(); i++)
    lamfld.DOF(i) = 1+i;

  // Psi on airfoil & Psi_nep on airfoil and wake
  std::vector<Real> PsiTrue = { -1.3715374334735166e+00,
                                -9.4156576165621120e-01,
                                 5.7809267901907324e-02,
                                -1.2096618707717997e+00,
                                -1.3715374334735166e+00 };
  std::vector<Real> Psi_nepTrue = { 5.6054117790617108e-01,
                                    1.3355233834267862e+01,
                                    4.7916882378742648e-01,
                                   -1.3506671246124446e+01,
                                   -6.1146372005419569e-01,
                                   14.373065262907279,
                                   6.5412548444663869e+00,
                                   4.7182366140389673e+00 };
  for (int i = 0; i < nnode_a; ++i)
  {
    VectorX Xep = coordinates_a.at(i);

    Real Psi, Psi_nep;
    panel.Psicalc(gamfld,lamfld,Xep,i,Psi,Psi_nep);

    SANS_CHECK_CLOSE(PsiTrue[i], Psi, tol, tol);
    SANS_CHECK_CLOSE(Psi_nepTrue[i], Psi_nep, tol, tol);
  }

  for (int i = 0; i < nnode_w; ++i)
  {
    VectorX Xep = coordinates_w.at(i);
    const int inode = i + nnode_a;

    Real Psi_dummy, Psi_nep;
    panel.Psicalc(gamfld,lamfld,Xep,inode,Psi_dummy,Psi_nep);

    //    SANS_CHECK_CLOSE(PsiTrue[inode], Psi_dummy, tol, tol); //TODO: this is not useful and it's also cubersome to test
    SANS_CHECK_CLOSE(Psi_nepTrue[inode], Psi_nep, tol, tol);
  }

  // Uecalc

  // Ue on airfoil
  for (int i = 0; i < nnode_a; ++i)
  {
    VectorX Xep = coordinates_a.at(i);

    Real Ue = 0;
    panel.Uecalc(gamfld,lamfld,Xep,i,Ue);

    SANS_CHECK_CLOSE(-gamfld.DOF(i), Ue, tol, tol);
  }

  // Ue on wake
  for (int i = 0; i < nnode_w; i++)
  {
    VectorX Xep = coordinates_w.at(i);
    const int inode = i + nnode_a;

    Real Ue = 0;
    panel.Uecalc(gamfld,lamfld,Xep,inode,Ue);

    if (inode==nnode_a)
    {
      SANS_CHECK_CLOSE(0.5*(gamfld.DOF(0)-gamfld.DOF(nnode_a-1)), Ue, tol, tol);
    }
    else
    {
      SANS_CHECK_CLOSE(Psi_nepTrue[inode], Ue, tol, tol);
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( interpResidualStuff_test )
{
  const Real tol = 1.5e-12;

  // Parameters
  const VectorX Vinf = {1.0,0.5};
  const Real p0 = 1.e5, T0 = 300;

  // Panel grid
  std::vector<VectorX> coordinates_a = { {1.0,  0},
                                         {0.5,  0.1E-1},
                                         {0.,   0.},
                                         {0.5, -0.1E-1},
                                         {1.0,  0}};
  std::vector<VectorX> coordinates_w = { {1.0001, 0.0},
                                         {1.2,    0.0},
                                         {1.4,    0.0} };
  XField2D_Line_X1_2Group_AirfoilWithWake xfld(coordinates_a,coordinates_w);
  std::vector<int> CellGroups = {0,1};

  PanelType panel(Vinf,p0,T0,xfld,CellGroups);

  BOOST_CHECK( panel.nMonitor() == 1 );

  // PDE
  Real rsdPDEIn = 1.0;

  {
    DLA::VectorD<Real> rsdPDEOut(1);
    panel.interpResidVariable(rsdPDEIn, rsdPDEOut);

    SANS_CHECK_CLOSE(rsdPDEIn, rsdPDEOut[0], tol, tol);

    // no cumulation
    panel.interpResidVariable(rsdPDEIn, rsdPDEOut);
    SANS_CHECK_CLOSE(rsdPDEIn, rsdPDEOut[0], tol, tol);

  }

  {
    DLA::VectorD<Real> rsdPDEOut(2);
    BOOST_CHECK_THROW(panel.interpResidVariable(rsdPDEIn, rsdPDEOut);, SANSException);
  }

  // Gradient
  Real rsdGradientIn = 1.0;

  {
    DLA::VectorD<Real> rsdGradientOut(1);
    panel.interpResidGradient(rsdGradientIn, rsdGradientOut);

    SANS_CHECK_CLOSE(rsdGradientIn, rsdGradientOut[0], tol, tol);

  }

  {
    DLA::VectorD<Real> rsdGradientOut(2);
    BOOST_CHECK_THROW(panel.interpResidGradient(rsdGradientIn, rsdGradientOut);, SANSException);
  }

  // BC
  Real rsdBCIn = 1.0;

  {
    DLA::VectorD<Real> rsdBCOut(1);
    panel.interpResidBC(rsdBCIn, rsdBCOut);

    SANS_CHECK_CLOSE(rsdBCIn, rsdBCOut[0], tol, tol);

  }

  {
    DLA::VectorD<Real> rsdBCOut(2);
    BOOST_CHECK_THROW(panel.interpResidBC(rsdBCIn, rsdBCOut);, SANSException);
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
