// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//#define DISPLAY_FOR_DEBUG

//----------------------------------------------------------------------------//
// test of XfoilPanelEquationSet

//Python must be included first
#include "Python/PyDict.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>
#include <type_traits>
#include <limits>

#include "NonLinearSolver/NewtonSolver.h"

#include "PanelMethod/XfoilPanel.h"
#include "PanelMethod/XfoilPanelEquationSet.h"

#include "unit/UnitGrids/XField2D_Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Line_X1_2Group_AirfoilWithWake.h"

#include "unit/Discretization/jacobianPingTest_btest.h"

//----------------------------------------------------------------------------//

using namespace std;

namespace SANS
{
typedef XfoilPanel PanelType;

typedef XfoilPanelEquationSet<PanelType,AlgEqSetTraits_Sparse> PanelEquationClass;

typedef PanelEquationClass::SystemMatrix SystemMatrixClass;
typedef PanelEquationClass::SystemVector SystemVectorClass;
typedef PanelEquationClass::SystemNonZeroPattern SystemNonZeroPattern;

typedef PanelEquationClass::VectorX VectorX;
typedef PanelEquationClass::XFieldType XFieldType;
typedef PanelEquationClass::GamFieldType<Real> GamFieldType;
typedef PanelEquationClass::LamFieldType<Real> LamFieldType;

typedef typename PanelEquationClass::ArrayQ<Real> ArrayQ;
typedef typename PanelEquationClass::MatrixQ<Real> MatrixQ;
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( XfoilPanelEquationSet_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  BOOST_CHECK(PanelType::D == PanelEquationClass::D);
  BOOST_CHECK(PanelType::N == PanelEquationClass::N);
  BOOST_CHECK(1 == PanelEquationClass::Nauxi);

  BOOST_CHECK(PanelType::order_xfld == PanelEquationClass::order_xfld);
  BOOST_CHECK(PanelType::order_gam == PanelEquationClass::order_gam);
  BOOST_CHECK(PanelType::order_lam == PanelEquationClass::order_lam);

  BOOST_CHECK(3 == PanelEquationClass::nsubeqn);

  BOOST_CHECK(0 == PanelEquationClass::iProj);
  BOOST_CHECK(1 == PanelEquationClass::iPanel);
  BOOST_CHECK(2 == PanelEquationClass::iKutta);

  BOOST_CHECK(0 == PanelEquationClass::ilam);
  BOOST_CHECK(1 == PanelEquationClass::igam);
  BOOST_CHECK(2 == PanelEquationClass::iPsi0);

  BOOST_CHECK(0 == PanelEquationClass::iParam);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( constructor_miscellaneous_test )
{
  // Parameters
  const VectorX Vinf = {1.0,0.5};
  const Real p0 = 1.e5, T0 = 300;

  // Panel grid
  std::vector<VectorX> coordinates_a = { {1.0,  0.5E-2},
                                         {0.5,  0.1E-1},
                                         {0.,   0.},
                                         {0.5, -0.1E-1},
                                         {1.0, -0.5E-2}};
  std::vector<VectorX> coordinates_w = { {1.0001, 0.0},
                                         {1.2,    0.0},
                                         {1.4,    0.0} };
  XField2D_Line_X1_2Group_AirfoilWithWake xfld(coordinates_a,coordinates_w);
  std::vector<int> CellGroups = {0,1};

  PanelType panel(Vinf,p0,T0,xfld,CellGroups);

  // constructor

  GamFieldType gamfld(xfld, PanelType::order_gam, BasisFunctionCategory_Hierarchical,{0});
  Real Psi0 = 0;

  LamFieldType lamfld(xfld, PanelType::order_lam, BasisFunctionCategory_Legendre);
  LamFieldType qauxifld(xfld, PanelType::order_lam, BasisFunctionCategory_Legendre);

  const Real tol_eqn = 1.e-12;
  PanelEquationClass panelEqSet(panel, gamfld, Psi0, lamfld, qauxifld, tol_eqn);

  // miscellaneous member functions
  BOOST_CHECK_THROW(panelEqSet.indexPDE();,SANSException);
  BOOST_CHECK_THROW(panelEqSet.indexQ();,  SANSException);

  SystemVectorClass q(panelEqSet.vectorStateSize());
  BOOST_CHECK(panelEqSet.isValidStateSystemVector(q));

  BOOST_CHECK(3 == panelEqSet.nResidNorm());

  // accessor functions
  BOOST_CHECK(&xfld == &panelEqSet.getxfld());
  BOOST_CHECK(&gamfld == &panelEqSet.getgamfld());
  BOOST_CHECK(&Psi0 == &panelEqSet.getPsi0());
  BOOST_CHECK(&lamfld == &panelEqSet.getlamfld());
  BOOST_CHECK(&qauxifld == &panelEqSet.getqauxifld());
  BOOST_CHECK(&panel == &panelEqSet.getpanel());

  Real real_eps = std::numeric_limits<Real>::epsilon();
  SANS_CHECK_CLOSE(Vinf[0], panelEqSet.getVinf()[0], real_eps, real_eps);
  SANS_CHECK_CLOSE(Vinf[1], panelEqSet.getVinf()[1], real_eps, real_eps);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( jacobianPing_test )
{
  // Parameters
  const VectorX Vinf = {1.0, 0.5};
  const Real p0 = 1.e5, T0 = 300;

  // Panel grid
  std::vector<VectorX> coordinates_a = { {1.0,  0.5E-2},
                                         {0.5,  0.1E-1},
                                         {0.,   0.},
                                         {0.5, -0.1E-1},
                                         {1.0, -0.5E-2}};
  std::vector<VectorX> coordinates_w = { {1.0001, 0.0},
                                         {1.2,    0.0},
                                         {1.4,    0.0} };
  XField2D_Line_X1_2Group_AirfoilWithWake xfld(coordinates_a,coordinates_w);
  std::vector<int> CellGroups = {0,1};

  PanelType panel(Vinf,p0,T0,xfld,CellGroups);

  // constructor
  GamFieldType gamfld(xfld, PanelType::order_gam, BasisFunctionCategory_Hierarchical,{0});
  for (int i = 0; i < gamfld.nDOF(); ++i)
    gamfld.DOF(i) = 1;

  Real Psi0 = 1;

  LamFieldType lamfld(xfld, PanelType::order_lam, BasisFunctionCategory_Legendre);
  for (int i = 0; i < lamfld.nDOF(); ++i)
    lamfld.DOF(i) = 2;

  LamFieldType qauxifld(xfld, PanelEquationClass::order_lam, BasisFunctionCategory_Legendre);
  for (int i = 0; i < qauxifld.nDOF(); i++)
    qauxifld.DOF(i) = 3;

  const Real tol_eqn = 1.e-12;
  PanelEquationClass panelEqSet(panel,gamfld,Psi0,lamfld,qauxifld,tol_eqn);

  // residual
  SystemVectorClass q(panelEqSet.vectorStateSize());
  panelEqSet.fillSystemVector(q);

  BOOST_CHECK(PanelEquationClass::nsubeqn == q.m());

  SystemVectorClass rsd(panelEqSet.vectorEqSize());
  SystemVectorClass rsdp(panelEqSet.vectorEqSize());
  SystemVectorClass rsdm(panelEqSet.vectorEqSize());

  // jacobian nonzero pattern
  SystemNonZeroPattern nz(panelEqSet.matrixSize());
  panelEqSet.jacobian(nz);

  // jacobian
  SystemMatrixClass jac(nz);
  panelEqSet.jacobian(jac);

  BOOST_CHECK(PanelEquationClass::nsubeqn == jac.m());
  BOOST_CHECK(PanelEquationClass::nsubeqn == jac.n());

  // The kutta condition should only have 2 non-zero entries for gamma
  BOOST_CHECK_EQUAL(2, jac(PanelEquationClass::iKutta,PanelEquationClass::igam).getNumNonZero());
  BOOST_CHECK_EQUAL(0, jac(PanelEquationClass::iKutta,PanelEquationClass::ilam).getNumNonZero());
  BOOST_CHECK_EQUAL(0, jac(PanelEquationClass::iKutta,PanelEquationClass::iPsi0).getNumNonZero());

  // [empty] jacobianTranspose and jacobianTranspose nonzero pattern
  BOOST_CHECK_THROW(panelEqSet.jacobianTranspose(jac);, DeveloperException);
  BOOST_CHECK_THROW(panelEqSet.jacobianTranspose(nz);,  DeveloperException);

#ifdef DISPLAY_FOR_DEBUGGING
  const bool verbose = true;
#else
  const bool verbose = false;
#endif

  const std::vector<Real> step_vec = {2e-3, 1e-3};
  const std::vector<Real>& rate_range = {2.0, 2.0}; // linear system: jacobian should be recovered up to numerical exactness
  const Real nonzero_tol = 1e-13;
  const Real small_tol = 1e-12;

  const ArrayQ qArrayQscale = {1.0};

#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(0,0) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<MatrixQ>(jac, rsdp, rsdm, rsdp, rsdm, q, q, panelEqSet,
                        step_vec, qArrayQscale, rate_range, verbose, nonzero_tol, small_tol);

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( residual_SharpTE_test )
{
  // Parameters
  const VectorX Vinf = {1.0,0.5};
  const Real p0 = 1.e5, T0 = 300;

  // Panel grid
  std::vector<VectorX> coordinates_a = { {1.0,  0},
                                         {0.5,  0.1E-1},
                                         {0.,   0.},
                                         {0.5, -0.1E-1},
                                         {1.0, -0}};
  std::vector<VectorX> coordinates_w = { {1.0001, 0.0},
                                         {1.2,    0.0},
                                         {1.4,    0.0} };
  XField2D_Line_X1_2Group_AirfoilWithWake xfld(coordinates_a,coordinates_w);
  std::vector<int> CellGroups = {0,1};

  PanelType panel(Vinf,p0,T0,xfld,CellGroups);

  // constructor
#if PanelSharpTETreatment_isXfoilPaper
  const std::vector<Real> gamData
  = { 2.6337166803069575e+01,
      1.4191855842557578e+01,
      8.9798712366713982e+00,
      -1.2145310960511997e+01,
      -2.6337166803069575e+01 };
#else
  const std::vector<Real> gamData
  = { 8.3024502018675364e+00,
      1.4542065239963360e+01,
      8.9798712366713946e+00,
      -1.2495520357917782e+01,
      -8.3024502018675364e+00 };
#endif

  GamFieldType gamfld(xfld, PanelType::order_gam, BasisFunctionCategory_Hierarchical,{0});
  BOOST_CHECK(gamfld.nDOF() >= (int)gamData.size());

  for (std::size_t i = 0; i < gamData.size(); ++i)
    gamfld.DOF(i) = gamData[i];

  Real Psi0 = -6.8597799373171109e-01;

  LamFieldType lamfld(xfld, PanelType::order_lam, BasisFunctionCategory_Legendre);
  for (int i = 0; i < lamfld.nDOF(); ++i)
    lamfld.DOF(i) = 1+i;

  LamFieldType qauxifld(xfld, PanelEquationClass::order_lam, BasisFunctionCategory_Legendre);
  for (int i = 0; i < qauxifld.nDOF(); i++)
  {
    qauxifld.DOF(i) = lamfld.DOF(i);
#ifdef DISPLAY_FOR_DEBUG
    cout << "qauxifld.DOF("<<i<<") = " <<qauxifld.DOF(i) << endl;
#endif
  }

  const Real tol_eqn = 1.e-12;
  PanelEquationClass panelEqSet(panel,gamfld,Psi0,lamfld,qauxifld,tol_eqn);

  // residual
  SystemVectorClass q(panelEqSet.vectorStateSize());
  panelEqSet.fillSystemVector(q);

  SystemVectorClass rsd(panelEqSet.vectorEqSize());
  BOOST_REQUIRE_EQUAL(3, rsd.m());
  rsd = 0;
  panelEqSet.residual(rsd);

  // check residual
#if PanelSharpTETreatment_isXfoilPaper
  const std::vector<Real> rsdTrue
  = { 0.0000000000000000e+00,
      0.0000000000000000e+00,
      0.0000000000000000e+00,
      0.0000000000000000e+00,
      0.0000000000000000e+00,
      0.0000000000000000e+00,
      5.0922713916418960e+00,
      -8.8761848424589873e-01,
      -1.4801151582058668e+02,
      -1.6217594847975823e+02,
      4.6991522242293382e+02,
      3.6013449933908348e+01 };
#else
  const std::vector<Real> rsdTrue_Panel
  = { 0.0000000000000000e+00,
      0.0000000000000000e+00,
      0.0000000000000000e+00,
      0.0000000000000000e+00,
      0.0000000000000000e+00 };

  const std::vector<Real> rsdTrue_Kutta
  = { 0.0000000000000000e+00 };

  const std::vector<Real> rsdTrue_Proj
  = { 0.0000000000000000e+00,
      0.0000000000000000e+00,
      0.0000000000000000e+00,
      0.0000000000000000e+00,
      0.0000000000000000e+00,
      0.0000000000000000e+00 };
#endif
  BOOST_REQUIRE_EQUAL(rsd(PanelEquationClass::iPanel).m(), (int)rsdTrue_Panel.size());
  BOOST_REQUIRE_EQUAL(rsd(PanelEquationClass::iKutta).m(), (int)rsdTrue_Kutta.size());
  BOOST_REQUIRE_EQUAL(rsd(PanelEquationClass::iProj).m(), (int)rsdTrue_Proj.size());

  for (int i = 0; i < rsd(PanelEquationClass::iPanel).m(); ++i)
  {
#ifdef DISPLAY_FOR_DEBUG
    cout << "rsd["<<i<<"] = " << rsd(PanelEquationClass::iPanel)[i] << endl;
#endif
    SANS_CHECK_CLOSE(rsdTrue_Panel[i], rsd(PanelEquationClass::iPanel)[i], tol_eqn, tol_eqn);
  }

  for (int i = 0; i < rsd(PanelEquationClass::iKutta).m(); ++i)
  {
#ifdef DISPLAY_FOR_DEBUG
    cout << "rsd["<<i<<"] = " << rsd(PanelEquationClass::iKutta)[i] << endl;
#endif
    SANS_CHECK_CLOSE(rsdTrue_Kutta[i], rsd(PanelEquationClass::iKutta)[i], tol_eqn, tol_eqn);
  }

  for (int i = 0; i < rsd(PanelEquationClass::iProj).m(); ++i)
  {
#ifdef DISPLAY_FOR_DEBUG
    cout << "rsd["<<i<<"] = " << rsd(PanelEquationClass::iProj)[i] << endl;
#endif
    SANS_CHECK_CLOSE(rsdTrue_Proj[i], rsd(PanelEquationClass::iProj)[i], tol_eqn, tol_eqn);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( residual_FiniteTE_test )
{
  // Parameters
  const VectorX Vinf = {1.0,0.5};
  const Real p0 = 1.e5, T0 = 300;

  // Panel grid
  std::vector<VectorX> coordinates_a = { {1.0,  0.5E-2},
                                         {0.5,  0.1E-1},
                                         {0.,   0.},
                                         {0.5, -0.1E-1},
                                         {1.0, -0.5E-2}};
  std::vector<VectorX> coordinates_w = { {1.0001, 0.0},
                                         {1.2,    0.0},
                                         {1.4,    0.0} };
  XField2D_Line_X1_2Group_AirfoilWithWake xfld(coordinates_a,coordinates_w);
  std::vector<int> CellGroups = {0,1};

  PanelType panel(Vinf,p0,T0,xfld,CellGroups);

  // constructor
  const std::vector<Real> gamData
  = { 6.2145956442753674e+00,
      1.4415334106912901e+01,
      8.9158330795686105e+00,
      -1.2429709541780587e+01,
      -6.2145956442753674e+00 };
  GamFieldType gamfld(xfld, PanelType::order_gam, BasisFunctionCategory_Hierarchical,{0});
  BOOST_CHECK(gamfld.nDOF() >= (int)gamData.size());

  for (int i = 0; i < (int)gamData.size(); ++i)
    gamfld.DOF(i) = gamData[i];

  Real Psi0 = -6.5392961664772775e-01;

  LamFieldType lamfld(xfld, PanelType::order_lam, BasisFunctionCategory_Legendre);
  for (int i = 0; i < lamfld.nDOF(); ++i)
    lamfld.DOF(i) = 1+i;

  LamFieldType qauxifld(xfld, PanelType::order_lam, BasisFunctionCategory_Legendre);
  for (int i = 0; i < qauxifld.nDOF(); i++)
  {
    qauxifld.DOF(i) = lamfld.DOF(i);
#ifdef DISPLAY_FOR_DEBUG
    cout << "qauxifld.DOF("<<i<<") = " <<qauxifld.DOF(i) << endl;
#endif
  }

  const Real tol_eqn = 1.e-12;
  PanelEquationClass panelEqSet(panel,gamfld,Psi0,lamfld,qauxifld,tol_eqn);

  // residual
  SystemVectorClass q(panelEqSet.vectorStateSize());
  BOOST_CHECK(3==q.m());
  panelEqSet.fillSystemVector(q);

  SystemVectorClass rsd(panelEqSet.vectorEqSize());
  BOOST_CHECK(3 == rsd.m());
  rsd = 0;
  panelEqSet.residual(rsd);

  // check residual
  const std::vector<Real> rsdTrue_Panel
  = { 0.0000000000000000e+00,
      0.0000000000000000e+00,
      0.0000000000000000e+00,
      0.0000000000000000e+00,
      0.0000000000000000e+00 };

  const std::vector<Real> rsdTrue_Kutta
  = { 0.0000000000000000e+00 };

  const std::vector<Real> rsdTrue_Proj
  = { 0.0000000000000000e+00,
      0.0000000000000000e+00,
      0.0000000000000000e+00,
      0.0000000000000000e+00,
      0.0000000000000000e+00,
      0.0000000000000000e+00 };

  BOOST_REQUIRE_EQUAL(rsd(PanelEquationClass::iPanel).m(), (int)rsdTrue_Panel.size());
  BOOST_REQUIRE_EQUAL(rsd(PanelEquationClass::iKutta).m(), (int)rsdTrue_Kutta.size());
  BOOST_REQUIRE_EQUAL(rsd(PanelEquationClass::iProj).m(), (int)rsdTrue_Proj.size());

  for (int i = 0; i < rsd(PanelEquationClass::iPanel).m(); ++i)
  {
#ifdef DISPLAY_FOR_DEBUG
    cout << "rsd["<<i<<"] = " << rsd(PanelEquationClass::iPanel)[i] << endl;
#endif
    SANS_CHECK_CLOSE(rsdTrue_Panel[i], rsd(PanelEquationClass::iPanel)[i], tol_eqn, tol_eqn);
  }

  for (int i = 0; i < rsd(PanelEquationClass::iKutta).m(); ++i)
  {
#ifdef DISPLAY_FOR_DEBUG
    cout << "rsd["<<i<<"] = " << rsd(PanelEquationClass::iKutta)[i] << endl;
#endif
    SANS_CHECK_CLOSE(rsdTrue_Kutta[i], rsd(PanelEquationClass::iKutta)[i], tol_eqn, tol_eqn);
  }

  for (int i = 0; i < rsd(PanelEquationClass::iProj).m(); ++i)
  {
#ifdef DISPLAY_FOR_DEBUG
    cout << "rsd["<<i<<"] = " << rsd(PanelEquationClass::iProj)[i] << endl;
#endif
    SANS_CHECK_CLOSE(rsdTrue_Proj[i], rsd(PanelEquationClass::iProj)[i], tol_eqn, tol_eqn);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( residualInfo_Norm_test )
{
  // Parameters
  const VectorX Vinf = {1.0,0.5};
  const Real p0 = 1.e5, T0 = 300;

  // Panel grid
  std::vector<VectorX> coordinates_a = { {1.0,  0.5E-2},
                                         {0.5,  0.1E-1},
                                         {0.,   0.},
                                         {0.5, -0.1E-1},
                                         {1.0, -0.5E-2}};
  XField2D_Line_X1_1Group xfld(coordinates_a);
  std::vector<int> CellGroups = {0};

  PanelType panel(Vinf,p0,T0,xfld,CellGroups);

  // constructor

  GamFieldType gamfld(xfld, PanelType::order_gam, BasisFunctionCategory_Hierarchical,{0});
  Real Psi0 = 0;
  LamFieldType lamfld(xfld, PanelType::order_lam, BasisFunctionCategory_Legendre);
  LamFieldType qauxifld(xfld, PanelType::order_lam, BasisFunctionCategory_Legendre);

  const Real tol_eqn = 1.e-12;
  PanelEquationClass panelEqSet(panel, gamfld, Psi0, lamfld, qauxifld, tol_eqn);

  std::vector<std::string> titles;
  std::vector<int> idx;

  panelEqSet.residualInfo(titles, idx);

  BOOST_CHECK((int) titles.size() == PanelEquationClass::nsubeqn);
  BOOST_CHECK((int) idx.size() == PanelEquationClass::nsubeqn);

  BOOST_CHECK(titles.at(PanelEquationClass::iProj)  == "Source Projection : ");
  BOOST_CHECK(titles.at(PanelEquationClass::iPanel) == "Panel equation    : ");
  BOOST_CHECK(titles.at(PanelEquationClass::iKutta) == "Kutta condition   : ");

  SystemVectorClass rsd(panelEqSet.vectorEqSize());
  BOOST_CHECK(3 == rsd.m());
  rsd = 0.0;

  for (int i = 0; i < rsd(PanelEquationClass::iProj).m(); ++i)
  {
    rsd(PanelEquationClass::iProj)[i] = 1.0;
#ifdef DISPLAY_FOR_DEBUG
    cout << "rsd["<<i<<"] = " << rsd(PanelEquationClass::iProj)[i] << endl;
#endif
  }

  for (int i = 0; i < rsd(PanelEquationClass::iPanel).m(); ++i)
  {
    rsd(PanelEquationClass::iPanel)[i] = 2.0;
#ifdef DISPLAY_FOR_DEBUG
    cout << "rsd["<<i<<"] = " << rsd(PanelEquationClass::iPanel)[i] << endl;
#endif
  }

  for (int i = 0; i < rsd(PanelEquationClass::iKutta).m(); ++i)
  {
    rsd(PanelEquationClass::iKutta)[i] = 3.0;
#ifdef DISPLAY_FOR_DEBUG
    cout << "rsd["<<i<<"] = " << rsd(PanelEquationClass::iKutta)[i] << endl;
#endif
  }

  auto rsdNorm = panelEqSet.residualNorm(rsd);

  // check
  BOOST_CHECK((int) rsdNorm.size() == panelEqSet.nResidNorm());
  BOOST_CHECK(panel.nMonitor() == 1);

  BOOST_CHECK( rsdNorm[PanelEquationClass::iProj][0] == 1.0*sqrt((Real)rsd(PanelEquationClass::iProj).m()));
  BOOST_CHECK( rsdNorm[PanelEquationClass::iPanel][0] == 2.0*sqrt((Real)rsd(PanelEquationClass::iPanel).m()));
  BOOST_CHECK( rsdNorm[PanelEquationClass::iKutta][0] == 3.0*sqrt((Real)rsd(PanelEquationClass::iKutta).m()));

}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
