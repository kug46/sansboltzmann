// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "pde/IBL/PDEIBLSplitAmpLag2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "PanelMethod/ProjectionToQauxi_IBL.h"
#include "PanelMethod/XfoilPanel.h"
#include "PanelMethod/XfoilPanelEquationSet.h"

using namespace std;
using namespace SANS;

namespace SANS
{
typedef XfoilPanel PanelType;
typedef XfoilPanelEquationSet<PanelType,AlgEqSetTraits_Sparse> PanelEquationClass;

typedef PhysD2 PhysDim;
typedef VarTypeDsThNGsplit VarType;
typedef PDEIBLSplitAmpLag<PhysDim,VarType> PDEClass;

typedef ProjectionToQauxi<PanelEquationClass,PDEClass> ProjectToQauxiClass;
typedef typename ProjectToQauxiClass::template ArrayQIBL<Real> ArrayQIBL;
typedef typename ProjectToQauxiClass::template ArrayQ<Real> ArrayQauxi;
typedef typename ProjectToQauxiClass::template ArrayQauxv<Real> ArrayQauxv;
typedef typename ProjectToQauxiClass::template VectorX<Real> VectorX;
typedef typename ProjectToQauxiClass::template ParamType<Real,Real> ParamType;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( ProjectionToQauxiPanelEquation_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( static_test )
{
  BOOST_CHECK(ProjectToQauxiClass::N == PanelEquationClass::N);
  BOOST_CHECK(ProjectToQauxiClass::NQIBL == PDEClass::N);
  BOOST_CHECK(ProjectToQauxiClass::NQauxv == PDEClass::Nparam);

  BOOST_CHECK(ProjectToQauxiClass::iParamQIBL == 0);
  BOOST_CHECK(ProjectToQauxiClass::iParamQauxv == 1);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( miscellaneous_test )
{
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.6;
  gasModelDict[GasModelParamsIBL::params.R] = 66.0;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelIBL gasModel(gasModelDict);

  const Real mue = 7.0E-5;
  typename PDEClass::ViscosityModelType viscosityModel(mue);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.ntinit] = 0.0;
  transitionModelDict[TransitionModelParams::params.ntcrit] = 9.0;
  transitionModelDict[TransitionModelParams::params.isTransitionActive] = true;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const typename PDEClass::TransitionModelType transitionModel(transitionModelDict);

  PDEClass pdeIBL(gasModel, viscosityModel, transitionModel);

  ProjectToQauxiClass projToQauxi(pdeIBL);

  BOOST_CHECK(projToQauxi.hasFluxAdvectiveTime() == false);
  BOOST_CHECK(projToQauxi.hasFluxAdvective() == true);
  BOOST_CHECK(projToQauxi.hasFluxViscous() == false);
  BOOST_CHECK(projToQauxi.hasSource() == false);
  BOOST_CHECK(projToQauxi.hasSourceTrace() == false);
  BOOST_CHECK(projToQauxi.hasForcingFunction() == false);

  BOOST_CHECK(projToQauxi.needsSolutionGradientforSource() == false);

  // flux calculations
  const Real small_tol = 6.E-14;
  const Real close_tol = 7.6E-14;

  const Real x = 0.1;
  const Real z = 3.8;
  const Real t = 0.3;

  { // fluxAdvective
    const Real delta1s = 2e-2, theta11 = 5e-3, nt = 1.3, G = -5.0;
    const Real qx = 4.0, qz = 3.0, p0e = 28.125, T0e = 4.6875;
    const Real qx_x = 0, qx_z = 0, qz_x = 0, qz_z = 0;

    const ArrayQIBL qIBL = pdeIBL.setDOFFrom( VarData2DDsThNGsplit(delta1s,theta11,nt,G) );

    const ArrayQauxv qauxv =
        pdeIBL.getParamInterpreter().setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0e, T0e);

    const ParamType paramsQuaxi(qIBL,qauxv);

    const VectorX e1 = { 0.6, 0.8 };

    const VectorX q1 = pdeIBL.getParamInterpreter().getq1(qauxv);

    const ArrayQauxi fxTrue = -delta1s*q1[0];
    const ArrayQauxi fzTrue = -delta1s*q1[1];

    const ArrayQauxi qauxi = 0;
    ArrayQauxi fx = 0, fz = 0;

    projToQauxi.fluxAdvectiveBoundary(paramsQuaxi,e1,x,z,t,qauxi,fx,fz);
    for ( int i = 0; i < ProjectToQauxiClass::N; ++i )
    {
      SANS_CHECK_CLOSE( DLA::index(fxTrue,i), DLA::index(fx,i), small_tol, close_tol );
      SANS_CHECK_CLOSE( DLA::index(fzTrue,i), DLA::index(fz,i), small_tol, close_tol );
    }

    projToQauxi.fluxAdvectiveBoundary(paramsQuaxi,e1,x,z,t,qauxi,fx,fz);
    for ( int i = 0; i < ProjectToQauxiClass::N; ++i )
    {
      SANS_CHECK_CLOSE( 2.0*DLA::index(fxTrue,i), DLA::index(fx,i), small_tol, close_tol );
      SANS_CHECK_CLOSE( 2.0*DLA::index(fzTrue,i), DLA::index(fz,i), small_tol, close_tol );
    }
  }

  { // fluxAdvectiveUpwind
    const Real delta1sL = 2e-2, theta11L = 5e-3, ntL = 1.3, GL = -5.0;
    const Real delta1sR = 3e-2, theta11R = 6e-3, ntR = 1.3, GR = -5.0;

    const Real qxL =  4.0, qzL = 3.0, p0eL = 1.2e5, T0eL = 300;
    const Real qxR = -2.3, qzR = 0.9, p0eR = 0.3e5, T0eR = 400;

    const Real qx_xL = 0, qx_zL = 0, qz_xL = 0, qz_zL = 0;
    const Real qx_xR = 0, qx_zR = 0, qz_xR = 0, qz_zR = 0;

    const ArrayQIBL qIBLL = pdeIBL.setDOFFrom( VarData2DDsThNGsplit(delta1sL,theta11L,ntL,GL) );
    const ArrayQIBL qIBLR = pdeIBL.setDOFFrom( VarData2DDsThNGsplit(delta1sR,theta11R,ntR,GR) );

    const ArrayQauxv qauxvL =
        pdeIBL.getParamInterpreter().setDOFFrom(qxL, qzL, qx_xL, qx_zL, qz_xL, qz_zL, p0eL, T0eL);
    const ArrayQauxv qauxvR =
        pdeIBL.getParamInterpreter().setDOFFrom(qxR, qzR, qx_xR, qx_zR, qz_xR, qz_zR, p0eR, T0eR);

    const ParamType paramsQuaxiL(qIBLL, qauxvL);
    const ParamType paramsQuaxiR(qIBLR, qauxvR);

    const VectorX e1L = { 0.6, 0.8 };
    const VectorX e1R = { -0.8, 0.6 };
    const VectorX nrm = { sqrt(0.5), sqrt(0.5) };

    const ArrayQauxi qauxiL = 0, qauxiR = 0;

    const Real qnL = qxL*nrm[0] + qzL*nrm[1], qnR = qxR*nrm[0] + qzR*nrm[1];
    const ArrayQauxi fLTrue = (qnL > 0) ?  -delta1sL*qnL : -delta1sR*qnR,
                     fRTrue = (qnL > 0) ?  -delta1sL*qnL : -delta1sR*qnR;

    ArrayQauxi fL = 0.0, fR = 0.0;

    projToQauxi.fluxAdvectiveUpwind(paramsQuaxiL, paramsQuaxiR, e1L, e1R, x, z, t, qauxiL, qauxiR, nrm[0], nrm[1], nrm[0], nrm[1], fL, fR);
    for ( int i = 0; i < ProjectToQauxiClass::N; ++i )
    {
      SANS_CHECK_CLOSE( DLA::index(fLTrue,i), DLA::index(fL,i), small_tol, close_tol );
      SANS_CHECK_CLOSE( DLA::index(fRTrue,i), DLA::index(fR,i), small_tol, close_tol );
    }

    projToQauxi.fluxAdvectiveUpwind(paramsQuaxiL, paramsQuaxiR, e1L, e1R, x, z, t, qauxiL, qauxiR, nrm[0], nrm[1], nrm[0], nrm[1], fL, fR);
    for ( int i = 0; i < ProjectToQauxiClass::N; ++i )
    {
      SANS_CHECK_CLOSE( 2.0*DLA::index(fLTrue,i), DLA::index(fL,i), small_tol, close_tol );
      SANS_CHECK_CLOSE( 2.0*DLA::index(fRTrue,i), DLA::index(fR,i), small_tol, close_tol );
    }
  }

  // dummy functions
  {
    const ArrayQauxv qauxv = 0;
    const ArrayQauxi qauxi = 0, forcing = 0;

    projToQauxi.forcingFunction(qauxv, x, z, t, forcing);

    BOOST_CHECK(projToQauxi.isValidState(qauxi) == true);

    projToQauxi.dump(2);
    projToQauxi.setDOFFrom(qauxi);
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
