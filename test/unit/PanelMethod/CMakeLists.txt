INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

RUN_WITH_MPIEXEC( 1 )

# The library dependencies should included downstream in the following list
GenerateUnitTests( PanelMethodLib
                   IBL_Lib
                   pdeLib
                   UnitGridsLib
                   FieldLib
                   BasisFunctionLib
                   QuadratureLib
                   TopologyLib
                   NonLinearSolverLib
                   SparseLinAlgLib
                   DenseLinAlgLib
                   PythonLib
                   SurrealLib
                   toolsLib
                 )
