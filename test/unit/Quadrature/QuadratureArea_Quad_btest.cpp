// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// QuadratureArea_Quad_btest
// testing of QuadratureArea<Quad> class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "Topology/ElementTopology.h"
#include "Quadrature/QuadratureArea.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( QuadratureArea_Quad_test_suite )

//----------------------------------------------------------------------------//
// monomials of given degree
static int
getMonomialSize( int degree )
{
  //This assumes tensor product quadrature
  return degree;
}

static int
getMonomial( int n, int degree, int& xexp, int& yexp )
{
  if (n >= getMonomialSize(degree))
  {
    printf( "Error in %s: n = %d >= getMonomialSize(%d) = %d\n", __func__,
      n, degree, getMonomialSize(degree) );
    return 1;
  }

  // This assumes tenspor product quadrature
  xexp = yexp = n;

  return 0;
}

static int
getMonomialIntegral( int n, int degree, Real& f )
{
  if (n >= getMonomialSize(degree))
    return 1;

  int xexp, yexp;

  // This assumes a tensor product
  xexp = yexp = n;

  f = 1./static_cast<Real>((1 + xexp)*(1 + yexp));

  return 0;
}


//----------------------------------------------------------------------------//
// monomial class:  f(x) = a * x^m * y^n

class Fcn2D
{
public:
  Fcn2D( int orderx, int ordery, Real coef ) : orderx_(orderx), ordery_(ordery), coef_(coef) {}
  ~Fcn2D() {}

  Real operator()( Real xy[] ) const;
  Real operator()( Real x, Real y ) const;

private:
  int orderx_, ordery_;
  Real coef_;
};

Real
Fcn2D::operator()( Real xy[] ) const
{
  Real f;
  f = coef_ * pow(xy[0], orderx_) * pow(xy[1], ordery_);

  return f;
}

Real
Fcn2D::operator()( Real x, Real y ) const
{
  Real f;
  f = coef_ * pow(x, orderx_) * pow(y, ordery_);

  return f;
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orderReqd )
{
  QuadratureArea<Quad> quadm1(-1);
  BOOST_CHECK_EQUAL( 39, quadm1.order() );
  BOOST_CHECK_EQUAL( 400, quadm1.nQuadrature() );

  QuadratureArea<Quad> quad0(0);
  BOOST_CHECK_EQUAL( 1, quad0.order() );
  BOOST_CHECK_EQUAL( 1, quad0.nQuadrature() );

  QuadratureArea<Quad> quad1(1);
  BOOST_CHECK_EQUAL( 1, quad1.order() );
  BOOST_CHECK_EQUAL( 1, quad1.nQuadrature() );

  QuadratureArea<Quad> quad2(2);
  BOOST_CHECK_EQUAL( 3, quad2.order() );
  BOOST_CHECK_EQUAL( 4, quad2.nQuadrature() );

  QuadratureArea<Quad> quad3(3);
  BOOST_CHECK_EQUAL( 3, quad3.order() );
  BOOST_CHECK_EQUAL( 4, quad3.nQuadrature() );

  QuadratureArea<Quad> quad4(4);
  BOOST_CHECK_EQUAL( 5, quad4.order() );
  BOOST_CHECK_EQUAL( 9, quad4.nQuadrature() );

  QuadratureArea<Quad> quad5(5);
  BOOST_CHECK_EQUAL( 5, quad5.order() );
  BOOST_CHECK_EQUAL( 9, quad5.nQuadrature() );

  QuadratureArea<Quad> quad6(6);
  BOOST_CHECK_EQUAL( 7, quad6.order() );
  BOOST_CHECK_EQUAL( 16, quad6.nQuadrature() );

  QuadratureArea<Quad> quad7(7);
  BOOST_CHECK_EQUAL( 7, quad7.order() );
  BOOST_CHECK_EQUAL( 16, quad7.nQuadrature() );

  QuadratureArea<Quad> quad8(8);
  BOOST_CHECK_EQUAL( 9, quad8.order() );
  BOOST_CHECK_EQUAL( 25, quad8.nQuadrature() );

  QuadratureArea<Quad> quad9(9);
  BOOST_CHECK_EQUAL( 9, quad9.order() );
  BOOST_CHECK_EQUAL( 25, quad9.nQuadrature() );

  QuadratureArea<Quad> quad10(10);
  BOOST_CHECK_EQUAL( 11, quad10.order() );
  BOOST_CHECK_EQUAL( 36, quad10.nQuadrature() );

  QuadratureArea<Quad> quad11(11);
  BOOST_CHECK_EQUAL( 11, quad11.order() );
  BOOST_CHECK_EQUAL( 36, quad11.nQuadrature() );

  QuadratureArea<Quad> quad12(12);
  BOOST_CHECK_EQUAL( 13, quad12.order() );
  BOOST_CHECK_EQUAL( 49, quad12.nQuadrature() );

  QuadratureArea<Quad> quad13(13);
  BOOST_CHECK_EQUAL( 13, quad13.order() );
  BOOST_CHECK_EQUAL( 49, quad13.nQuadrature() );

  QuadratureArea<Quad> quad14(14);
  BOOST_CHECK_EQUAL( 15, quad14.order() );
  BOOST_CHECK_EQUAL( 64, quad14.nQuadrature() );

  QuadratureArea<Quad> quad15(15);
  BOOST_CHECK_EQUAL( 15, quad15.order() );
  BOOST_CHECK_EQUAL( 64, quad15.nQuadrature() );

  QuadratureArea<Quad> quad16(16);
  BOOST_CHECK_EQUAL( 17, quad16.order() );
  BOOST_CHECK_EQUAL( 81, quad16.nQuadrature() );

  QuadratureArea<Quad> quad17(17);
  BOOST_CHECK_EQUAL( 17, quad17.order() );
  BOOST_CHECK_EQUAL( 81, quad17.nQuadrature() );

  QuadratureArea<Quad> quad18(18);
  BOOST_CHECK_EQUAL( 19, quad18.order() );
  BOOST_CHECK_EQUAL( 100, quad18.nQuadrature() );

  QuadratureArea<Quad> quad19(19);
  BOOST_CHECK_EQUAL( 19, quad19.order() );
  BOOST_CHECK_EQUAL( 100, quad19.nQuadrature() );

  QuadratureArea<Quad> quad20(20);
  BOOST_CHECK_EQUAL( 21, quad20.order() );
  BOOST_CHECK_EQUAL( 121, quad20.nQuadrature() );

  QuadratureArea<Quad> quad21(21);
  BOOST_CHECK_EQUAL( 21, quad21.order() );
  BOOST_CHECK_EQUAL( 121, quad21.nQuadrature() );

  QuadratureArea<Quad> quad22(22);
  BOOST_CHECK_EQUAL( 23, quad22.order() );
  BOOST_CHECK_EQUAL( 144, quad22.nQuadrature() );

  QuadratureArea<Quad> quad23(23);
  BOOST_CHECK_EQUAL( 23, quad23.order() );
  BOOST_CHECK_EQUAL( 144, quad23.nQuadrature() );

  QuadratureArea<Quad> quad39(39);
  BOOST_CHECK_EQUAL( 39, quad39.order() );
  BOOST_CHECK_EQUAL( 400, quad39.nQuadrature() );

  BOOST_CHECK_THROW( QuadratureArea<Quad> quad40(40), DeveloperException );


  BOOST_CHECK( QuadratureArea<Quad>::nOrderIdx == 13);
  for (int orderidx = 0; orderidx < QuadratureArea<Quad>::nOrderIdx; orderidx++)
  {
    // Check that the orderidx -> order -> orderidx
    BOOST_CHECK_EQUAL( orderidx, QuadratureArea<Quad>::getOrderIndex( QuadratureArea<Quad>::getOrderFromIndex(orderidx) ) );
  }

  BOOST_CHECK_EQUAL( 0, QuadratureArea<Quad>::getOrderIndex(0) );
  BOOST_CHECK_EQUAL( 0, QuadratureArea<Quad>::getOrderIndex(1) );
  BOOST_CHECK_EQUAL( 1, QuadratureArea<Quad>::getOrderIndex(2) );
  BOOST_CHECK_EQUAL( 1, QuadratureArea<Quad>::getOrderIndex(3) );
  BOOST_CHECK_EQUAL( 2, QuadratureArea<Quad>::getOrderIndex(4) );
  BOOST_CHECK_EQUAL( 2, QuadratureArea<Quad>::getOrderIndex(5) );
  BOOST_CHECK_EQUAL( 3, QuadratureArea<Quad>::getOrderIndex(6) );
  BOOST_CHECK_EQUAL( 3, QuadratureArea<Quad>::getOrderIndex(7) );
  BOOST_CHECK_EQUAL( 4, QuadratureArea<Quad>::getOrderIndex(8) );
  BOOST_CHECK_EQUAL( 4, QuadratureArea<Quad>::getOrderIndex(9) );
  BOOST_CHECK_EQUAL( 5, QuadratureArea<Quad>::getOrderIndex(10) );
  BOOST_CHECK_EQUAL( 5, QuadratureArea<Quad>::getOrderIndex(11) );
  BOOST_CHECK_EQUAL( 6, QuadratureArea<Quad>::getOrderIndex(12) );
  BOOST_CHECK_EQUAL( 6, QuadratureArea<Quad>::getOrderIndex(13) );
  BOOST_CHECK_EQUAL( 7, QuadratureArea<Quad>::getOrderIndex(14) );
  BOOST_CHECK_EQUAL( 7, QuadratureArea<Quad>::getOrderIndex(15) );
  BOOST_CHECK_EQUAL( 8, QuadratureArea<Quad>::getOrderIndex(16) );
  BOOST_CHECK_EQUAL( 8, QuadratureArea<Quad>::getOrderIndex(17) );
  BOOST_CHECK_EQUAL( 9, QuadratureArea<Quad>::getOrderIndex(18) );
  BOOST_CHECK_EQUAL( 9, QuadratureArea<Quad>::getOrderIndex(19) );
  BOOST_CHECK_EQUAL(10, QuadratureArea<Quad>::getOrderIndex(20) );
  BOOST_CHECK_EQUAL(10, QuadratureArea<Quad>::getOrderIndex(21) );
  BOOST_CHECK_EQUAL(11, QuadratureArea<Quad>::getOrderIndex(22) );
  BOOST_CHECK_EQUAL(11, QuadratureArea<Quad>::getOrderIndex(23) );
  BOOST_CHECK_EQUAL(12, QuadratureArea<Quad>::getOrderIndex(24) );
  BOOST_CHECK_EQUAL(12, QuadratureArea<Quad>::getOrderIndex(39) );
  BOOST_CHECK_EQUAL(12, QuadratureArea<Quad>::getOrderIndex(-1) );
  BOOST_CHECK_THROW( QuadratureArea<Quad>::getOrderIndex(40), DeveloperException );
}


//----------------------------------------------------------------------------//
// degree 1 polynomial; 1 point
BOOST_AUTO_TEST_CASE( _1a )
{
  QuadratureArea<Quad> quad(1);
  BOOST_CHECK_EQUAL( 1, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad;
  Real f, fTrue;
  Real xy[2], w, wsum;

  nquad = quad.nQuadrature();

  // f(x) = 1
  Fcn2D fcn0(0, 0, 1);
  fTrue = 1;
  f = 0;
  wsum = 0;
  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, xy);
    quad.weight(n, w);
    f += w*fcn0(xy);
    wsum += w;
  }
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( 1, wsum, tol );

  // f(x) = x
  Fcn2D fcn1(1, 0, 1);
  fTrue = 0.5;
  f = 0;
  wsum = 0;
  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, xy);
    quad.weight(n, w);
    f += w*fcn1(xy);
    wsum += w;
  }
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( 1, wsum, tol );

  // f(x) = y
  Fcn2D fcn2(0, 1, 1);
  fTrue = 0.5;
  f = 0;
  wsum = 0;
  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, xy);
    quad.weight(n, w);
    f += w*fcn2(xy);
    wsum += w;
  }
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( 1, wsum, tol );
}


//----------------------------------------------------------------------------//
// degree 1 polynomial; 1 point
BOOST_AUTO_TEST_CASE( _1 )
{
  const int degree = 1;
  const int npts = 1;

  QuadratureArea<Quad> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad, nmonomial, orderx=-1, ordery=-1, ierr;
  Real f, fTrue;
  Real xy[2], w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 1: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, xy);
      quad.weight(n, w);
      f += w*fcn(xy);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 3 polynomial; 4 points
BOOST_AUTO_TEST_CASE( _3 )
{
  const int degree = 3;
  const int npts = 4;

  QuadratureArea<Quad> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad, nmonomial, orderx=-1, ordery=-1, ierr;
  Real f, fTrue = 0;
  Real xy[2], w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
//    printf( "deg 3: i = %d\n", i );
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 3: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, xy);
      quad.weight(n, w);
      f += w*fcn(xy);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 5 polynomial; 9 points
BOOST_AUTO_TEST_CASE( _5 )
{
  const int degree = 5;
  const int npts = 9;

  QuadratureArea<Quad> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad, nmonomial, orderx=-1, ordery=-1, ierr;
  Real f, fTrue = 0;
  Real xy[2], w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 5: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, xy);
      quad.weight(n, w);
      f += w*fcn(xy);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 5 polynomial; 9 points
// alternate .coordinates() member function
BOOST_AUTO_TEST_CASE( _5_coord )
{
  const int degree = 5;
  const int npts = 9;

  QuadratureArea<Quad> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad, nmonomial, orderx=-1, ordery=-1, ierr;
  Real f, fTrue = 0;
  Real x, y, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 5: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y);
      quad.weight(n, w);
      f += w*fcn(x, y);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 7 polynomial; 16 points
BOOST_AUTO_TEST_CASE( _7 )
{
  const int degree = 7;
  const int npts = 16;

  QuadratureArea<Quad> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad, nmonomial, orderx=-1, ordery=-1, ierr;
  Real f, fTrue = 0;
  Real xy[2], w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 7: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, xy);
      quad.weight(n, w);
      f += w*fcn(xy);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 7 polynomial; 16 points
// alternate .coordinates() member function
BOOST_AUTO_TEST_CASE( _7_coord )
{
  const int degree = 7;
  const int npts = 16;

  QuadratureArea<Quad> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad, nmonomial, orderx=-1, ordery=-1, ierr;
  Real f, fTrue = 0;
  Real x, y, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 7: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y);
      quad.weight(n, w);
      f += w*fcn(x, y);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 9 polynomial; 25 points
// alternate .coordinates() member function
BOOST_AUTO_TEST_CASE( _9_coord )
{
  const int degree = 9;
  const int npts = 25;

  QuadratureArea<Quad> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-12;
  int nquad, nmonomial, orderx=-1, ordery=-1, ierr;
  Real f, fTrue = 0;
  Real x, y, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y);
      quad.weight(n, w);
      f += w*fcn(x, y);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 11 polynomial; 36 points
// alternate .coordinates() member function
BOOST_AUTO_TEST_CASE( _11_coord )
{
  const int degree = 11;
  const int npts = 36;

  QuadratureArea<Quad> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-12;
  int nquad, nmonomial, orderx=-1, ordery=-1, ierr;
  Real f, fTrue = 0;
  Real x, y, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y);
      quad.weight(n, w);
      f += w*fcn(x, y);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 13 polynomial; 49 points
// alternate .coordinates() member function
BOOST_AUTO_TEST_CASE( _13_coord )
{
  const int degree = 13;
  const int npts = 49;

  QuadratureArea<Quad> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-12;
  int nquad, nmonomial, orderx=-1, ordery=-1, ierr;
  Real f, fTrue = 0;
  Real x, y, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y);
      quad.weight(n, w);
      f += w*fcn(x, y);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 15 polynomial; 64 points
// alternate .coordinates() member function
BOOST_AUTO_TEST_CASE( _15_coord )
{
  const int degree = 15;
  const int npts = 64;

  QuadratureArea<Quad> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-12;
  int nquad, nmonomial, orderx=-1, ordery=-1, ierr;
  Real f, fTrue = 0;
  Real x, y, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y);
      quad.weight(n, w);
      f += w*fcn(x, y);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 17 polynomial; 81 points
// alternate .coordinates() member function
BOOST_AUTO_TEST_CASE( _17_coord )
{
  const int degree = 17;
  const int npts = 81;

  QuadratureArea<Quad> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-12;
  int nquad, nmonomial, orderx=-1, ordery=-1, ierr;
  Real f, fTrue = 0;
  Real x, y, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y);
      quad.weight(n, w);
      f += w*fcn(x, y);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 19 polynomial; 100 points
// alternate .coordinates() member function
BOOST_AUTO_TEST_CASE( _19_coord )
{
  const int degree = 19;
  const int npts = 100;

  QuadratureArea<Quad> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-12;
  int nquad, nmonomial, orderx=-1, ordery=-1, ierr;
  Real f, fTrue = 0;
  Real x, y, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y);
      quad.weight(n, w);
      f += w*fcn(x, y);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 21 polynomial; 121 points
// alternate .coordinates() member function
BOOST_AUTO_TEST_CASE( _21_coord )
{
  const int degree = 21;
  const int npts = 121;

  QuadratureArea<Quad> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-12;
  int nquad, nmonomial, orderx=-1, ordery=-1, ierr;
  Real f, fTrue = 0;
  Real x, y, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y);
      quad.weight(n, w);
      f += w*fcn(x, y);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 23 polynomial; 144 points
// alternate .coordinates() member function
BOOST_AUTO_TEST_CASE( _23_coord )
{
  const int degree = 23;
  const int npts = 144;

  QuadratureArea<Quad> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-12;
  int nquad, nmonomial, orderx=-1, ordery=-1, ierr;
  Real f, fTrue = 0;
  Real x, y, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y);
      quad.weight(n, w);
      f += w*fcn(x, y);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 39 polynomial; 400 points
// alternate .coordinates() member function
BOOST_AUTO_TEST_CASE( _39_coord )
{
  const int degree = 39;
  const int npts = 400;

  QuadratureArea<Quad> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-12;
  int nquad, nmonomial, orderx=-1, ordery=-1, ierr;
  Real f, fTrue = 0;
  Real x, y, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y);
      quad.weight(n, w);
      f += w*fcn(x, y);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Quadrature/QuadratureArea_Quad_pattern.txt", true );

  QuadratureArea<Quad> quadm1(-1);
  output << quadm1 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quadm1.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Quad> quad1(1);
  output << quad1 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad1.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Quad> quad3(3);
  output << quad3 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad3.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Quad> quad5(5);
  output << quad5 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad5.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Quad> quad7(7);
  output << quad7 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad7.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Quad> quad9(9);
  output << quad9 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad9.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Quad> quad11(11);
  output << quad11 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad11.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Quad> quad13(13);
  output << quad13 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad13.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Quad> quad15(15);
  output << quad15 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad15.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Quad> quad17(17);
  output << quad17 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad17.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Quad> quad19(19);
  output << quad19 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad19.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Quad> quad21(21);
  output << quad21 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad21.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Quad> quad23(23);
  output << quad23 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad23.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Quad> quad39(39);
  output << quad39 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad39.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
