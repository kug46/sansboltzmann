// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "Quadrature/QuadratureSpacetime.h"
#include "Quadrature/Quadrature_Simplex_StroudConical.h"
#include "Topology/ElementTopology.h"

using namespace SANS;

// local definitions
namespace
{

BOOST_AUTO_TEST_SUITE(QuadratureSpacetime_Pentatope_Stroud_test_suite)

BOOST_AUTO_TEST_CASE(TestStroud_2D_OrderOne)
{
  // constant integral should be exactly evaluated with order one quadrature

  double tol= 1e-12;

  double *x;
  double *w;

  int nDim= 2;
  int order= 1;
  int nPoints= order*order;

  x= new double[nPoints*nDim];
  w= new double[nPoints];

  calculateStroudQuadrature(nDim, order, x, w);

  {
    // test integral on unit simplex over f(x, y, z)= 1.0

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./2.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= 1.0;
      If+= w[i]*f[i]/2.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  delete [] x;
  delete [] w;

}

BOOST_AUTO_TEST_CASE(TestStroud_2D_OrderTwo)
{
  // constant integral should be exactly evaluated with order one quadrature

  double tol= 1e-12;

  double *x;
  double *w;

  int nDim= 2;
  int order= 2;
  int nPoints= order*order;

  x= new double[nPoints*nDim];
  w= new double[nPoints];

  calculateStroudQuadrature(nDim, order, x, w);

  {
    // test integral on unit simplex over f(x, y, z)= x

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./6.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 0];
      If+= w[i]*f[i]/2.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z)= y

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./6.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 1];
      If+= w[i]*f[i]/2.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z)= xy

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./24.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 0]*x[nDim*i + 1];
      If+= w[i]*f[i]/2.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  delete [] x;
  delete [] w;

}

BOOST_AUTO_TEST_CASE(TestStroud_3D_OrderOne)
{
  // constant integral should be exactly evaluated with order one quadrature

  double tol= 1e-12;

  double *x;
  double *w;

  int nDim= 3;
  int order= 1;
  int nPoints= order*order*order;

  x= new double[nPoints*nDim];
  w= new double[nPoints];

  calculateStroudQuadrature(nDim, order, x, w);

  {
    // test integral on unit simplex over f(x, y, z)= 1.0

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./6.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= 1.0;
      If+= w[i]*f[i]/6.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  delete [] x;
  delete [] w;

}

BOOST_AUTO_TEST_CASE(TestStroud_3D_OrderTwo)
{
  // constant integral should be exactly evaluated with order one quadrature

  double tol= 1e-12;

  double *x;
  double *w;

  int nDim= 3;
  int order= 2;
  int nPoints= order*order*order;

  x= new double[nPoints*nDim];
  w= new double[nPoints];

  calculateStroudQuadrature(nDim, order, x, w);

  {
    // test integral on unit simplex over f(x, y, z)= x

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./24.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 0];
      If+= w[i]*f[i]/6.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z)= y

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./24.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 1];
      If+= w[i]*f[i]/6.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z)= z

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./24.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 2];
      If+= w[i]*f[i]/6.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z)= xy

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./120.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 0]*x[nDim*i + 1];
      If+= w[i]*f[i]/6.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z)= xz

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./120.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 0]*x[nDim*i + 2];
      If+= w[i]*f[i]/6.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z)= yz

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./120.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 1]*x[nDim*i + 2];
      If+= w[i]*f[i]/6.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  delete [] x;
  delete [] w;

}

BOOST_AUTO_TEST_CASE(TestStroud_4D_OrderOne)
{
  // constant integral should be exactly evaluated with order one quadrature

  double tol= 1e-12;

  double *x;
  double *w;

  int nDim= 4;
  int order= 1;
  int nPoints= order*order*order*order;

  x= new double[nPoints*nDim];
  w= new double[nPoints];

  calculateStroudQuadrature(nDim, order, x, w);

  {
    // test integral on unit simplex over f(x, y, z, w)= 1.0

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./24.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= 1.0;
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  delete [] x;
  delete [] w;

}

BOOST_AUTO_TEST_CASE(TestStroud_4D_OrderTwo)
{
  // polynomial functions that are linear in any given direction should be
  // exactly evaluated with order two quadrature

  double tol= 1e-12;

  double *x;
  double *w;

  int nDim= 4;
  int order= 2;
  int nPoints= order*order*order*order;

  x= new double[nPoints*nDim];
  w= new double[nPoints];

  calculateStroudQuadrature(nDim, order, x, w);

  {
    // test integral on unit simplex over f(x, y, z, w)= x

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./120.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 0];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z, w)= y

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./120.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 1];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z, w)= z

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./120.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 2];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z, w)= w

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./120.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 3];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z, w)= xy

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./720.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 0]*x[nDim*i + 1];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z, w)= xz

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./720.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 0]*x[nDim*i + 2];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z, w)= xw

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./720.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 0]*x[nDim*i + 3];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z, w)= yz

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./720.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 1]*x[nDim*i + 2];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z, w)= yw

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./720.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 1]*x[nDim*i + 3];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z, w)= zw

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./720.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 2]*x[nDim*i + 3];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  delete [] x;
  delete [] w;

}

BOOST_AUTO_TEST_CASE(TestStroud_4D_OrderThree)
{
  // polynomial functions that are quadratic in any given direction should be
  // exactly evaluated with order three quadrature

  double tol= 1e-12;

  double *x;
  double *w;

  int nDim= 4;
  int order= 3;
  int nPoints= order*order*order*order;

  x= new double[nPoints*nDim];
  w= new double[nPoints];

  calculateStroudQuadrature(nDim, order, x, w);

  {
    // test integral on unit simplex over f(x, y, z, w)= x^2

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./360.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 0]*x[nDim*i + 0];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z, w)= x^2 y^2

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./10080.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 0]*x[nDim*i + 0]*x[nDim*i + 1]*x[nDim*i + 1];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z, w)= x^2 z^2

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./10080.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 0]*x[nDim*i + 0]*x[nDim*i + 2]*x[nDim*i + 2];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z, w)= x^2 w^2

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./10080.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 0]*x[nDim*i + 0]*x[nDim*i + 3]*x[nDim*i + 3];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z, w)= y^2

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./360.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 1]*x[nDim*i + 1];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z, w)= y^2 z^2

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./10080.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 1]*x[nDim*i + 1]*x[nDim*i + 2]*x[nDim*i + 2];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z, w)= y^2 w^2

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./10080.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 1]*x[nDim*i + 1]*x[nDim*i + 3]*x[nDim*i + 3];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z, w)= z^2

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./360.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 2]*x[nDim*i + 2];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z, w)= z^2 w^2

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./10080.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 2]*x[nDim*i + 2]*x[nDim*i + 3]*x[nDim*i + 3];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  {
    // test integral on unit simplex over f(x, y, z, w)= w^2

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./360.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 2]*x[nDim*i + 2];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;

  }

  delete [] x;
  delete [] w;

}

BOOST_AUTO_TEST_CASE(TestStroud_4D_OrderFourPlus)
{
  // constant integral should be exactly evaluated with order one quadrature

  double tol= 1e-12;

  double *x;
  double *w;

  int nDim= 4;

  {
    int order= 4;
    int nPoints= order*order*order*order;

    x= new double[nPoints*nDim];
    w= new double[nPoints];

    calculateStroudQuadrature(nDim, order, x, w);

    // test integral on unit simplex over f(x, y, z, w)= x*x*x

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./840.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 0]*x[nDim*i + 0]*x[nDim*i + 0];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;
    delete [] x;
    delete [] w;
  }

  {
    int order= 5;
    int nPoints= order*order*order*order;

    x= new double[nPoints*nDim];
    w= new double[nPoints];

    calculateStroudQuadrature(nDim, order, x, w);

    // test integral on unit simplex over f(x, y, z, w)= x*x*x*x

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./1680.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 0]*x[nDim*i + 0]*x[nDim*i + 0]*x[nDim*i + 0];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;
    delete [] x;
    delete [] w;
  }

  {
    int order= 6;
    int nPoints= order*order*order*order;

    x= new double[nPoints*nDim];
    w= new double[nPoints];

    calculateStroudQuadrature(nDim, order, x, w);

    // test integral on unit simplex over f(x, y, z, w)= x^5

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./3024.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 0]*x[nDim*i + 0]*x[nDim*i + 0]*x[nDim*i + 0]*x[nDim*i + 0];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;
    delete [] x;
    delete [] w;
  }

  {
    int order= 7;
    int nPoints= order*order*order*order;

    x= new double[nPoints*nDim];
    w= new double[nPoints];

    calculateStroudQuadrature(nDim, order, x, w);

    // test integral on unit simplex over f(x, y, z, w)= x^6

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./5040.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 0]*x[nDim*i + 0]*x[nDim*i + 0]*x[nDim*i + 0]*x[nDim*i + 0]*x[nDim*i + 0];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;
    delete [] x;
    delete [] w;
  }

  {
    int order= 8;
    int nPoints= order*order*order*order;

    x= new double[nPoints*nDim];
    w= new double[nPoints];

    calculateStroudQuadrature(nDim, order, x, w);

    // test integral on unit simplex over f(x, y, z, w)= x^7

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./7920.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 0]*x[nDim*i + 0]*x[nDim*i + 0]*x[nDim*i + 0]
                *x[nDim*i + 0]*x[nDim*i + 0]*x[nDim*i + 0];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;
    delete [] x;
    delete [] w;
  }

  {
    int order= 9;
    int nPoints= order*order*order*order;

    x= new double[nPoints*nDim];
    w= new double[nPoints];

    calculateStroudQuadrature(nDim, order, x, w);

    // test integral on unit simplex over f(x, y, z, w)= x^8

    double *f= new double[nPoints];
    double If= 0.0;
    double Iexact= 1./11880.;

    for (int i= 0; i < nPoints; i++)
    {
      f[i]= x[nDim*i + 0]*x[nDim*i + 0]*x[nDim*i + 0]*x[nDim*i + 0]
                *x[nDim*i + 0]*x[nDim*i + 0]*x[nDim*i + 0]*x[nDim*i + 0];
      If+= w[i]*f[i]/24.0;
    }

    BOOST_CHECK_CLOSE(If, Iexact, tol);

    delete [] f;
    delete [] x;
    delete [] w;
  }

}

BOOST_AUTO_TEST_CASE(TestJacobi_Main_Jacobi)
{
  // here, we check out the Gauss-Jacobi quadrature rule for a bunch of random values of alpha and beta.
  // these are checked against values from a third-party Jacobi rule found on MATLAB Central.

  double alpha;
  double beta;

  double *x;
  double *w;

  int order;

  double tol= 1e-10;

  {
    alpha= 3;
    beta= 7;      // random integer less than 10
    order= 5;     // random integer less than 10
    x= new double[order];
    w= new double[order];

    double xExact[]= {2.68842116656955454346e-01,
                      4.44625621649959223980e-01,
                      6.16339084381115842604e-01,
                      7.71783635298533776670e-01,
                      8.98409542013435702401e-01};

    double wExact[]= {6.96310404191467363915e-06,
                      1.03259620030380521762e-04,
                      3.16333291981013933238e-04,
                      2.77140832818798671378e-04,
                      5.38789087036495057282e-05};

    calculateJacobiQuadrature(order, alpha, beta, x, w);

    for (int i= 0; i < order; i++)
    {
      BOOST_CHECK_CLOSE(x[i], xExact[i], tol);
      BOOST_CHECK_CLOSE(w[i], wExact[i], tol);
    }

    delete [] x;
    delete [] w;

  }

  {
    alpha= 7;     // random integer less than 10
    beta= 2;      // random integer less than 10
    order= 5;
    x= new double[order];
    w= new double[order];

    double xExact[]= {7.38017200643051873854e-02,
                      1.91129467720554835353e-01,
                      3.45462794018753793779e-01,
                      5.22760528017332415374e-01,
                      7.08950753336948569228e-01};

    double wExact[]= {3.02257324150055324899e-04,
                      1.14174756879341387540e-03,
                      1.03388336752044591484e-03,
                      2.83194748292544206357e-04,
                      1.66947690213163464569e-05};

    calculateJacobiQuadrature(order, alpha, beta, x, w);

    for (int i= 0; i < order; i++)
    {
      BOOST_CHECK_CLOSE(x[i], xExact[i], tol);
      BOOST_CHECK_CLOSE(w[i], wExact[i], tol);
    }

    delete [] x;
    delete [] w;

  }

  {
    alpha= 2;     // random integer less than 10
    beta= 5;      // random integer less than 10
    order= 5;
    x= new double[order];
    w= new double[order];

    double xExact[]= {2.27505458877667821049e-01,
                      4.15078360603830076592e-01,
                      6.05897843263564772620e-01,
                      7.78882078247648990121e-01,
                      9.13812729595523620674e-01};

    double wExact[]= {6.55433519568723979608e-05,
                      8.11499927323016366498e-04,
                      2.35267483318269896020e-03,
                      2.20096862681726668926e-03,
                      5.21694213101099222261e-04};

    calculateJacobiQuadrature(order, alpha, beta, x, w);

    for (int i= 0; i < order; i++)
    {
      BOOST_CHECK_CLOSE(x[i], xExact[i], tol);
      BOOST_CHECK_CLOSE(w[i], wExact[i], tol);
    }

    delete [] x;
    delete [] w;

  }

  {
    alpha= 10;
    beta= 4;      // random integer less than 10
    order= 10;    // random integer less than 10
    x= new double[order];
    w= new double[order];

    double xExact[]= {5.06575786516313963936e-02,
                      1.05820919117908640583e-01,
                      1.74496895323383749421e-01,
                      2.54794927521122871461e-01,
                      3.44252969638660699658e-01,
                      4.40100140766182312824e-01,
                      5.39405981285284719107e-01,
                      6.39256509591722421604e-01,
                      7.37078858892115551527e-01,
                      8.31782278035516853265e-01};

    double wExact[]= {1.88332315558317717026e-07,
                      2.54708405048263735043e-06,
                      1.02001460056987580167e-05,
                      1.89919030799951418060e-05,
                      1.92298565741824184136e-05,
                      1.11390513717925972035e-05,
                      3.63971084261199804240e-06,
                      6.18351942087722575273e-07,
                      4.48134207552319868068e-08,
                      8.16996901688760787876e-10};

    calculateJacobiQuadrature(order, alpha, beta, x, w);

    for (int i= 0; i < order; i++)
    {
      BOOST_CHECK_CLOSE(x[i], xExact[i], tol);
      BOOST_CHECK_CLOSE(w[i], wExact[i], tol);
    }

    delete [] x;
    delete [] w;

  }

  {
    alpha= 6;
    beta= 3;      // random integer less than 10
    order= 10;    // random integer less than 10
    x= new double[order];
    w= new double[order];

    double xExact[]= {4.65631764129550429487e-02,
      1.06792468333246182155e-01,
      1.84673120191578665761e-01,
      2.76979352131668021819e-01,
      3.79731844471207558023e-01,
      4.88480907848221956868e-01,
      5.98530879792247949567e-01,
      7.05182388971074658457e-01,
      8.04046826899715849635e-01,
      8.91777655637739030681e-01};

    double wExact[]= {3.84119535170392115440e-06,
      4.29900026578738600220e-05,
      1.58567032687616422688e-04,
      2.98216332673760682864e-04,
      3.32155162452985085533e-04,
      2.30070199184536917621e-04,
      9.79670476761052434322e-05,
      2.38189201526852252662e-05,
      2.75706289615665653636e-06,
      9.32347427660660252635e-08};

    calculateJacobiQuadrature(order, alpha, beta, x, w);

    for (int i= 0; i < order; i++)
    {
      BOOST_CHECK_CLOSE(x[i], xExact[i], tol);
      BOOST_CHECK_CLOSE(w[i], wExact[i], tol);
    }

    delete [] x;
    delete [] w;

  }

  {
    alpha= 8;   // random integer less than 10
    beta= 3;    // random integer less than 10
    order= 10;
    x= new double[order];
    w= new double[order];

    double xExact[]= {4.19772221725302485673e-02,
      9.65186413033416457097e-02,
      1.67488063765132810801e-01,
      2.52328501394966431270e-01,
      3.47845973030256994107e-01,
      4.50435162612192852016e-01,
      5.56255217151364500516e-01,
      6.61436349073315277280e-01,
      7.62405005923330270079e-01,
      8.56858250670343379873e-01};

    double wExact[]= {2.40123201342172411102e-06,
      2.51932376823836870169e-05,
      8.49882622178397065249e-05,
      1.42376547408768779459e-04,
      1.37268224141342219929e-04,
      7.97329046344906505099e-05,
      2.74630267225979007059e-05,
      5.17644409275404775134e-06,
      4.40480361708094579796e-07,
      1.01457751984798850284e-08};

    calculateJacobiQuadrature(order, alpha, beta, x, w);

    for (int i= 0; i < order; i++)
    {
      BOOST_CHECK_CLOSE(x[i], xExact[i], tol);
      BOOST_CHECK_CLOSE(w[i], wExact[i], tol);
    }

    delete [] x;
    delete [] w;

  }

}

BOOST_AUTO_TEST_CASE(TestJacobi_Main_ZeroBeta)
{
  // here, we check out the Gauss-Jacobi quadrature rule for a bunch of systematic values of alpha with beta= 0.
  // these are checked against values from a third-party Jacobi rule found on MATLAB Central.

  double alpha= 0.0;
  double beta= 0.0;

  double *x;
  double *w;

  int order;

  double tol= 1e-12;

  {
    alpha= 1.0;
    order= 3;
    x= new double[order];
    w= new double[order];

    // exact legendre quadrature values from matlab
    double xExact[]= {88.5879595127039e-003,
                      409.466864440735e-003,
                      787.659461760847e-003};

    double wExact[]= {200.931913738959e-003,
                      229.241106359586e-003,
                      69.8269799014541e-003};

    calculateJacobiQuadrature(order, alpha, beta, x, w);

    for (int i= 0; i < order; i++)
    {
      BOOST_CHECK_CLOSE(x[i], xExact[i], tol);
      BOOST_CHECK_CLOSE(w[i], wExact[i], tol);
    }

    delete [] x;
    delete [] w;

  }

  {
    alpha= 2.0;
    order= 3;
    x= new double[order];
    w= new double[order];

    // exact legendre quadrature values from matlab
    double xExact[]= {72.9940240731496e-003,
                      347.003766038352e-003,
                      705.002209888498e-003};

    double wExact[]= {157.136361064887e-003,
                      146.246269259866e-003,
                      29.9507030085807e-003};

    calculateJacobiQuadrature(order, alpha, beta, x, w);

    for (int i= 0; i < order; i++)
    {
      BOOST_CHECK_CLOSE(x[i], xExact[i], tol);
      BOOST_CHECK_CLOSE(w[i], wExact[i], tol);
    }

    delete [] x;
    delete [] w;

  }

  {
    alpha= 3.0;
    order= 3;
    x= new double[order];
    w= new double[order];

    // exact legendre quadrature values from matlab
    double xExact[]= {62.0758993801255e-003,
                      301.188730836386e-003,
                      636.735369783488e-003};

    double wExact[]= {128.921043160522e-003,
                      104.599897556807e-003,
                      16.4790592826717e-003};

    calculateJacobiQuadrature(order, alpha, beta, x, w);

    for (int i= 0; i < order; i++)
    {
      BOOST_CHECK_CLOSE(x[i], xExact[i], tol);
      BOOST_CHECK_CLOSE(w[i], wExact[i], tol);
    }

    delete [] x;
    delete [] w;

  }

  {
    alpha= 1.0;
    order= 5;
    x= new double[order];
    w= new double[order];

    // exact legendre quadrature values from matlab
    double xExact[]= {39.8098570514686e-003,
                      198.013417873608e-003,
                      437.974810247386e-003,
                      695.464273353636e-003,
                      901.464914201174e-003};

    double wExact[]= {96.7815902266515e-003,
                      167.174638094370e-003,
                      146.386987084670e-003,
                      73.9088700726167e-003,
                      15.7479145216923e-003};

    calculateJacobiQuadrature(order, alpha, beta, x, w);

    for (int i= 0; i < order; i++)
    {
      BOOST_CHECK_CLOSE(x[i], xExact[i], tol);
      BOOST_CHECK_CLOSE(w[i], wExact[i], tol);
    }

    delete [] x;
    delete [] w;

  }

  {
    alpha= 2.0;
    order= 5;
    x= new double[order];
    w= new double[order];

    // exact legendre quadrature values from matlab
    double xExact[]= {34.5789399182151e-003,
                      173.480320771696e-003,
                      389.886387065519e-003,
                      634.333472630887e-003,
                      851.054212947016e-003};

    double wExact[]= {81.7647842857708e-003,
                      126.198961899912e-003,
                      89.2001612215900e-003,
                      32.0556007229619e-003,
                      4.11382520309902e-003};

    calculateJacobiQuadrature(order, alpha, beta, x, w);

    for (int i= 0; i < order; i++)
    {
      BOOST_CHECK_CLOSE(x[i], xExact[i], tol);
      BOOST_CHECK_CLOSE(w[i], wExact[i], tol);
    }

    delete [] x;
    delete [] w;

  }

  {
    alpha= 3.0;
    order= 5;
    x= new double[order];
    w= new double[order];

    // exact legendre quadrature values from matlab
    double xExact[]= {30.5642965067328e-003,
                      154.394850026162e-003,
                      351.429995762939e-003,
                      582.899788177799e-003,
                      803.787992603290e-003};

    double wExact[]= {70.7605280967551e-003,
                      100.316504464958e-003,
                      60.4449532037250e-003,
                      16.9573248632718e-003,
                      1.52068937129008e-003};

    calculateJacobiQuadrature(order, alpha, beta, x, w);

    for (int i= 0; i < order; i++)
    {
      BOOST_CHECK_CLOSE(x[i], xExact[i], tol);
      BOOST_CHECK_CLOSE(w[i], wExact[i], tol);
    }

    delete [] x;
    delete [] w;

  }

  {
    alpha= 4.0;
    order= 5;
    x= new double[order];
    w= new double[order];

    // exact legendre quadrature values from matlab
    double xExact[]= {27.3855814657346e-003,
                      139.113656323557e-003,
                      319.940767258990e-003,
                      539.066325468485e-003,
                      760.207955197519e-003};

    double wExact[]= {62.3552985892800e-003,
                      82.7127131019398e-003,
                      44.0244695053829e-003,
                      10.2105417254722e-003,
                      696.977077925095e-006};

    calculateJacobiQuadrature(order, alpha, beta, x, w);

    for (int i= 0; i < order; i++)
    {
      BOOST_CHECK_CLOSE(x[i], xExact[i], tol);
      BOOST_CHECK_CLOSE(w[i], wExact[i], tol);
    }

    delete [] x;
    delete [] w;

  }

  {
    alpha= 5.0;
    order= 5;
    x= new double[order];
    w= new double[order];

    // exact legendre quadrature values from matlab
    double xExact[]= {24.8061653019448e-003,
                      126.597272127742e-003,
                      293.666181079218e-003,
                      501.290172970008e-003,
                      720.306875187755e-003};

    double wExact[]= {55.7281760749163e-003,
                      70.0713397046102e-003,
                      33.7677449583775e-003,
                      6.72969043000753e-003,
                      369.715498755104e-006};

    calculateJacobiQuadrature(order, alpha, beta, x, w);

    for (int i= 0; i < order; i++)
    {
      BOOST_CHECK_CLOSE(x[i], xExact[i], tol);
      BOOST_CHECK_CLOSE(w[i], wExact[i], tol);
    }

    delete [] x;
    delete [] w;

  }

}

BOOST_AUTO_TEST_CASE(TestJacobi_Main_Legendre)
{
  // here, we check out the Gauss-Jacobi quadrature rule for a bunch of zero values of alpha and beta
  // at many orders. this is the specialization of Gauss-Jacobi to Gauss-Legendre quadrature points.
  // these are checked against values from a third-party Jacobi rule found on MATLAB Central.

  double alpha= 0.0;
  double beta= 0.0;

  double *x;
  double *w;

  int order;

  double tol= 1e-12;

  {
    order= 3;
    x= new double[order];
    w= new double[order];

    // exact legendre quadrature values from matlab
    double xExact[]= {112.701665379258e-003,
                      500.000000000000e-003,
                      887.298334620742e-003};

    double wExact[]= {277.777777777778e-003,
                      444.444444444444e-003,
                      277.777777777778e-003};

    calculateJacobiQuadrature(order, alpha, beta, x, w);

    for (int i= 0; i < order; i++)
    {
      BOOST_CHECK_CLOSE(x[i], xExact[i], tol);
      BOOST_CHECK_CLOSE(w[i], wExact[i], tol);
    }

    delete [] x;
    delete [] w;

  }

  {
    order= 5;
    x= new double[order];
    w= new double[order];

    // exact legendre quadrature values from matlab
    double xExact[]= {46.9100770306681e-003,
                      230.765344947158e-003,
                      500.000000000000e-003,
                      769.234655052842e-003,
                      953.089922969332e-003};

    double wExact[]= {118.463442528094e-003,
                      239.314335249683e-003,
                      284.444444444445e-003,
                      239.314335249683e-003,
                      118.463442528094e-003};

    calculateJacobiQuadrature(order, alpha, beta, x, w);

    for (int i= 0; i < order; i++)
    {
      BOOST_CHECK_CLOSE(x[i], xExact[i], tol);
      BOOST_CHECK_CLOSE(w[i], wExact[i], tol);
    }

    delete [] x;
    delete [] w;

  }

  {
    order= 10;
    x= new double[order];
    w= new double[order];

    // exact legendre quadrature values from matlab
    double xExact[]= {13.0467357414142e-003,
                      67.4683166555078e-003,
                      160.295215850488e-003,
                      283.302302935376e-003,
                      425.562830509184e-003,
                      574.437169490816e-003,
                      716.697697064624e-003,
                      839.704784149512e-003,
                      932.531683344492e-003,
                      986.953264258586e-003};
    double wExact[]= {33.3356721543442e-003,
                      74.7256745752904e-003,
                      109.543181257991e-003,
                      134.633359654998e-003,
                      147.762112357376e-003,
                      147.762112357376e-003,
                      134.633359654998e-003,
                      109.543181257991e-003,
                      74.7256745752905e-003,
                      33.3356721543442e-003};

    calculateJacobiQuadrature(order, alpha, beta, x, w);

    for (int i= 0; i < order; i++)
    {
      BOOST_CHECK_CLOSE(x[i], xExact[i], tol);
      BOOST_CHECK_CLOSE(w[i], wExact[i], tol);
    }

    delete [] x;
    delete [] w;

  }

}

BOOST_AUTO_TEST_CASE(TestJacobi_ParameterChecker)
{
  // there was a built-in parameter checker in some code I stole, so I'm gonna make sure it works.

  int kind= 8;

  // make sure that the parameter checks work
  BOOST_CHECK_THROW(parchk(-2, 0, 0.0, 0.0), DeveloperException);
  BOOST_CHECK_THROW(parchk(5, 0, -5.0, 0.0), DeveloperException);
  BOOST_CHECK_THROW(parchk(4, 0, 5.0, -5.0), DeveloperException);
  BOOST_CHECK_THROW(parchk(kind, 0, -5.0, 4.0), DeveloperException);


}


BOOST_AUTO_TEST_CASE(TestJacobi_ResizerUnusedCases)
{
  // for coverage/unit testing sanity, I'm not checking out the values spit out by some stolen code that
  // I definitely don't need. with that said, I'm making sure that parts of the code I'm not testing throw
  // exceptions.

  int nt= 1;
  double *t= new double[nt];
  int *mlt= new int[nt];
  double *wts= new double[nt];
  int nwts= nt;
  int *ndx= new int[nwts];
  double *swts= new double[nwts];
  double *st= new double[nt];
  int kind;
  double alpha= 1.0;
  double beta= 1.0;
  double a= 0;
  double b= 1;

  kind= 1; BOOST_CHECK_THROW(scqf(nt, t, mlt, wts, nwts, ndx, swts, st, kind, alpha, beta, a, b), DeveloperException);
  kind= 2; BOOST_CHECK_THROW(scqf(nt, t, mlt, wts, nwts, ndx, swts, st, kind, alpha, beta, a, b), DeveloperException);
  kind= 3; BOOST_CHECK_THROW(scqf(nt, t, mlt, wts, nwts, ndx, swts, st, kind, alpha, beta, a, b), DeveloperException);
  kind= 5; BOOST_CHECK_THROW(scqf(nt, t, mlt, wts, nwts, ndx, swts, st, kind, alpha, beta, a, b), DeveloperException);
  kind= 6; BOOST_CHECK_THROW(scqf(nt, t, mlt, wts, nwts, ndx, swts, st, kind, alpha, beta, a, b), DeveloperException);
  kind= 7; BOOST_CHECK_THROW(scqf(nt, t, mlt, wts, nwts, ndx, swts, st, kind, alpha, beta, a, b), DeveloperException);
  kind= 8; BOOST_CHECK_THROW(scqf(nt, t, mlt, wts, nwts, ndx, swts, st, kind, alpha, beta, a, b), DeveloperException);
  kind= 9; BOOST_CHECK_THROW(scqf(nt, t, mlt, wts, nwts, ndx, swts, st, kind, alpha, beta, a, b), DeveloperException);

  delete [] t;
  delete [] mlt;
  delete [] wts;
  delete [] ndx;
  delete [] swts;
  delete [] st;

}


BOOST_AUTO_TEST_CASE(TestJacobi_ClassMatrixUnusedCases)
{
  // for coverage/unit testing sanity, I'm not checking out the values spit out by some stolen code that
  // I definitely don't need. with that said, I'm making sure that parts of the code I'm not testing throw
  // exceptions.

  int kind;
  int m= 1;
  double alpha= 1.0;
  double beta= 1.0;
  double *aj= new double[m];
  double *bj= new double[m];

  kind= 1; BOOST_CHECK_THROW(class_matrix(kind, m, alpha, beta, aj, bj), DeveloperException);
  kind= 2; BOOST_CHECK_THROW(class_matrix(kind, m, alpha, beta, aj, bj), DeveloperException);
  kind= 3; BOOST_CHECK_THROW(class_matrix(kind, m, alpha, beta, aj, bj), DeveloperException);
  kind= 5; BOOST_CHECK_THROW(class_matrix(kind, m, alpha, beta, aj, bj), DeveloperException);
  kind= 6; BOOST_CHECK_THROW(class_matrix(kind, m, alpha, beta, aj, bj), DeveloperException);
  kind= 7; BOOST_CHECK_THROW(class_matrix(kind, m, alpha, beta, aj, bj), DeveloperException);
  kind= 8; BOOST_CHECK_THROW(class_matrix(kind, m, alpha, beta, aj, bj), DeveloperException);
  kind= 9; BOOST_CHECK_THROW(class_matrix(kind, m, alpha, beta, aj, bj), DeveloperException);
  kind= 10; BOOST_CHECK_THROW(class_matrix(kind, m, alpha, beta, aj, bj), DeveloperException);

  delete [] aj;
  delete [] bj;

}

BOOST_AUTO_TEST_SUITE_END()

}
