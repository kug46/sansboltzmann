// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// QuadratureArea_Triangle_btest
// testing of QuadratureArea<Triangle> class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "Quadrature/QuadratureArea.h"
#include "Topology/ElementTopology.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( QuadratureArea_Triangle_test_suite )

//----------------------------------------------------------------------------//
static long long
factorial( long long n )
{
  long long m;
  if (n <= 1)
    m = 1;
  else
    m = n * factorial(n - 1);

  return m;
}


//----------------------------------------------------------------------------//
// monomials of given degree
static int
getMonomialSize( int degree )
{
  int size;
  if (degree == 0)
    size = 1;
  else if (degree == 1)
    size = 3;
  else if (degree == 2)
    size = 6;
  else if (degree == 3)
    size = 10;
  else if (degree == 4)
    size = 15;
  else if (degree == 5)
    size = 21;
  else if (degree == 6)
    size = 28;
  else if (degree == 7)
    size = 36;
  else if (degree == 8)
    size = 45;
  else if (degree == 9)
    size = 55;
  else if (degree == 10)
    size = 66;
  else if (degree == 11)
    size = 78;
  else if (degree == 12)
    size = 90;
  else if (degree == 13)
    size = 103;
  else if (degree == 14)
    size = 117;
  else
    SANS_DEVELOPER_EXCEPTION( "not coded for degree %d", degree );

  return size;
}

static int
getMonomial( int n, int degree, int& xexp, int& yexp )
{
  if (n >= getMonomialSize(degree))
  {
    printf( "Error in %s: n = %d >= getMonomialSize(%d) = %d\n", __func__,
      n, degree, getMonomialSize(degree) );
    return 1;
  }

  if (n == 0)                // const
  {
    xexp = yexp = 0;
  }
  else if (n < 3)            // linear
  {
    yexp = n - 1;
    xexp = 1 - yexp;
  }
  else if (n < 6)            // quadratic
  {
    yexp = n - 3;
    xexp = 2 - yexp;
  }
  else if (n < 10)           // cubic
  {
    yexp = n - 6;
    xexp = 3 - yexp;
  }
  else if (n < 15)           // quartic
  {
    yexp = n - 10;
    xexp = 4 - yexp;
  }
  else if (n < 21)           // quintic
  {
    yexp = n - 15;
    xexp = 5 - yexp;
  }
  else if (n < 28)           // degree 6
  {
    yexp = n - 21;
    xexp = 6 - yexp;
  }
  else if (n < 36)           // degree 7
  {
    yexp = n - 28;
    xexp = 7 - yexp;
  }
  else if (n < 45)           // degree 8
  {
    yexp = n - 36;
    xexp = 8 - yexp;
  }
  else if (n < 55)           // degree 9
  {
    yexp = n - 45;
    xexp = 9 - yexp;
  }
  else if (n < 66)           // degree 10
  {
    yexp = n - 55;
    xexp = 10 - yexp;
  }
  else if (n < 78)           // degree 11
  {
    yexp = n - 66;
    xexp = 11 - yexp;
  }
  else if (n < 90)           // degree 12
  {
    yexp = n - 78;
    xexp = 12 - yexp;
  }
  else if (n < 103)          // degree 13
  {
    yexp = n - 90;
    xexp = 13 - yexp;
  }
  else if (n < 117)          // degree 14
  {
    yexp = n - 103;
    xexp = 14 - yexp;
  }
  else
    SANS_DEVELOPER_EXCEPTION( "not coded for degree %d of %d", n, degree );

  return 0;
}

static int
getMonomialIntegral( int n, int degree, Real& f )
{
  if (n >= getMonomialSize(degree))
    return 1;

  int xexp, yexp;

  if (n == 0)                // const
  {
    xexp = yexp = 0;
  }
  else if (n < 3)            // linear
  {
    yexp = n - 1;
    xexp = 1 - yexp;
  }
  else if (n < 6)            // quadratic
  {
    yexp = n - 3;
    xexp = 2 - yexp;
  }
  else if (n < 10)           // cubic
  {
    yexp = n - 6;
    xexp = 3 - yexp;
  }
  else if (n < 15)           // quartic
  {
    yexp = n - 10;
    xexp = 4 - yexp;
  }
  else if (n < 21)           // quintic
  {
    yexp = n - 15;
    xexp = 5 - yexp;
  }
  else if (n < 28)           // degree 6
  {
    yexp = n - 21;
    xexp = 6 - yexp;
  }
  else if (n < 36)           // degree 7
  {
    yexp = n - 28;
    xexp = 7 - yexp;
  }
  else if (n < 45)           // degree 8
  {
    yexp = n - 36;
    xexp = 8 - yexp;
  }
  else if (n < 55)           // degree 9
  {
    yexp = n - 45;
    xexp = 9 - yexp;
  }
  else if (n < 66)           // degree 10
  {
    yexp = n - 55;
    xexp = 10 - yexp;
  }
  else if (n < 78)           // degree 11
  {
    yexp = n - 66;
    xexp = 11 - yexp;
  }
  else if (n < 90)           // degree 12
  {
    yexp = n - 78;
    xexp = 12 - yexp;
  }
  else if (n < 103)          // degree 13
  {
    yexp = n - 90;
    xexp = 13 - yexp;
  }
  else if (n < 117)          // degree 14
  {
    yexp = n - 103;
    xexp = 14 - yexp;
  }
  else
    SANS_DEVELOPER_EXCEPTION( "not coded for degree %d of %d", n, degree );

  f = 2*factorial(xexp)*factorial(yexp)/static_cast<Real>(factorial(xexp + yexp + 2));

  return 0;
}


//----------------------------------------------------------------------------//
// monomial class:  f(x) = a * x^m * y^n

class Fcn2D
{
public:
  Fcn2D( int orderx, int ordery, Real coef ) : orderx_(orderx), ordery_(ordery), coef_(coef) {}
  ~Fcn2D() {}

  Real operator()( Real xy[] ) const;
  Real operator()( Real x, Real y ) const;

private:
  int orderx_, ordery_;
  int coef_;
};

Real
Fcn2D::operator()( Real xy[] ) const
{
  Real f;
  f = coef_ * pow(xy[0], orderx_) * pow(xy[1], ordery_);

  return f;
}

Real
Fcn2D::operator()( Real x, Real y ) const
{
  Real f;
  f = coef_ * pow(x, orderx_) * pow(y, ordery_);

  return f;
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orderReqd )
{
  QuadratureArea<Triangle> quadm1(-1);
  BOOST_CHECK_EQUAL(  30, quadm1.order() );
  BOOST_CHECK_EQUAL( 175, quadm1.nQuadrature() );

  QuadratureArea<Triangle> quad0(0);
  BOOST_CHECK_EQUAL( 1, quad0.order() );
  BOOST_CHECK_EQUAL( 1, quad0.nQuadrature() );

  QuadratureArea<Triangle> quad1(1);
  BOOST_CHECK_EQUAL( 1, quad1.order() );
  BOOST_CHECK_EQUAL( 1, quad1.nQuadrature() );

  QuadratureArea<Triangle> quad2(2);
  BOOST_CHECK_EQUAL( 2, quad2.order() );
  BOOST_CHECK_EQUAL( 3, quad2.nQuadrature() );

  QuadratureArea<Triangle> quad3(3);
  BOOST_CHECK_EQUAL( 3, quad3.order() );
  BOOST_CHECK_EQUAL( 6, quad3.nQuadrature() );

  QuadratureArea<Triangle> quad4(4);
  BOOST_CHECK_EQUAL( 4, quad4.order() );
  BOOST_CHECK_EQUAL( 6, quad4.nQuadrature() );

  QuadratureArea<Triangle> quad5(5);
  BOOST_CHECK_EQUAL( 5, quad5.order() );
  BOOST_CHECK_EQUAL( 7, quad5.nQuadrature() );

  QuadratureArea<Triangle> quad8(8);
  BOOST_CHECK_EQUAL(  8, quad8.order() );
  BOOST_CHECK_EQUAL( 16, quad8.nQuadrature() );

  QuadratureArea<Triangle> quad10(10);
  BOOST_CHECK_EQUAL( 10, quad10.order() );
  BOOST_CHECK_EQUAL( 25, quad10.nQuadrature() );

  QuadratureArea<Triangle> quad11(11);
  BOOST_CHECK_EQUAL( 11, quad11.order() );
  BOOST_CHECK_EQUAL( 28, quad11.nQuadrature() );

  QuadratureArea<Triangle> quad12(12);
  BOOST_CHECK_EQUAL( 12, quad12.order() );
  BOOST_CHECK_EQUAL( 33, quad12.nQuadrature() );

  QuadratureArea<Triangle> quad13(13);
  BOOST_CHECK_EQUAL( 13, quad13.order() );
  BOOST_CHECK_EQUAL( 37, quad13.nQuadrature() );

  QuadratureArea<Triangle> quad14(14);
  BOOST_CHECK_EQUAL( 14, quad14.order() );
  BOOST_CHECK_EQUAL( 46, quad14.nQuadrature() );

  QuadratureArea<Triangle> quad30(30);
  BOOST_CHECK_EQUAL(  30, quad30.order() );
  BOOST_CHECK_EQUAL( 175, quad30.nQuadrature() );

  BOOST_CHECK_THROW( QuadratureArea<Triangle> quad31(31), DeveloperException );

  BOOST_CHECK( QuadratureArea<Triangle>::nOrderIdx == 12);

  for (int orderidx = 0; orderidx < QuadratureArea<Triangle>::nOrderIdx; orderidx++)
  {
    // Check that the orderidx -> order -> orderidx
    BOOST_CHECK_EQUAL( orderidx, QuadratureArea<Triangle>::getOrderIndex( QuadratureArea<Triangle>::getOrderFromIndex(orderidx) ) );
  }

  BOOST_CHECK_EQUAL( 0, QuadratureArea<Triangle>::getOrderIndex(0) );
  BOOST_CHECK_EQUAL( 0, QuadratureArea<Triangle>::getOrderIndex(1) );
  BOOST_CHECK_EQUAL( 1, QuadratureArea<Triangle>::getOrderIndex(2) );
  BOOST_CHECK_EQUAL( 2, QuadratureArea<Triangle>::getOrderIndex(3) );
  BOOST_CHECK_EQUAL( 3, QuadratureArea<Triangle>::getOrderIndex(4) );
  BOOST_CHECK_EQUAL( 4, QuadratureArea<Triangle>::getOrderIndex(5) );
  BOOST_CHECK_EQUAL( 5, QuadratureArea<Triangle>::getOrderIndex(6) );
  BOOST_CHECK_EQUAL( 5, QuadratureArea<Triangle>::getOrderIndex(7) );
  BOOST_CHECK_EQUAL( 5, QuadratureArea<Triangle>::getOrderIndex(8) );
  BOOST_CHECK_EQUAL( 6, QuadratureArea<Triangle>::getOrderIndex(9) );
  BOOST_CHECK_EQUAL( 6, QuadratureArea<Triangle>::getOrderIndex(10) );
  BOOST_CHECK_EQUAL( 7, QuadratureArea<Triangle>::getOrderIndex(11) );
  BOOST_CHECK_EQUAL( 8, QuadratureArea<Triangle>::getOrderIndex(12) );
  BOOST_CHECK_EQUAL( 9, QuadratureArea<Triangle>::getOrderIndex(13) );
  BOOST_CHECK_EQUAL(10, QuadratureArea<Triangle>::getOrderIndex(14) );
  BOOST_CHECK_EQUAL(11, QuadratureArea<Triangle>::getOrderIndex(30) );
  BOOST_CHECK_EQUAL(11, QuadratureArea<Triangle>::getOrderIndex(-1) );

  BOOST_CHECK_THROW( QuadratureArea<Triangle>::getOrderIndex(31), DeveloperException );
}


//----------------------------------------------------------------------------//
// degree 1 polynomial; 1 point
BOOST_AUTO_TEST_CASE( _1a )
{
  QuadratureArea<Triangle> quad(1);
  BOOST_CHECK_EQUAL( 1, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad;
  Real f, fTrue;
  Real xy[2], w, wsum;

  nquad = quad.nQuadrature();

  // f(x) = 1
  Fcn2D fcn0(0, 0, 1);
  fTrue = 1.0;
  f = 0;
  wsum = 0;
  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, xy);
    quad.weight(n, w);
    f += w*fcn0(xy);
    wsum += w;
  }
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( 1.0, wsum, tol );

  // f(x) = x
  Fcn2D fcn1(1, 0, 1);
  fTrue = 1./3.;
  f = 0;
  wsum = 0;
  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, xy);
    quad.weight(n, w);
    f += w*fcn1(xy);
    wsum += w;
  }
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( 1.0, wsum, tol );

  // f(x) = y
  Fcn2D fcn2(0, 1, 1);
  fTrue = 1./3.;
  f = 0;
  wsum = 0;
  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, xy);
    quad.weight(n, w);
    f += w*fcn2(xy);
    wsum += w;
  }
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( 1.0, wsum, tol );
}


//----------------------------------------------------------------------------//
// degree 1 polynomial; 1 point
BOOST_AUTO_TEST_CASE( _1 )
{
  const int degree = 1;
  const int npts = 1;

  QuadratureArea<Triangle> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad, nmonomial, orderx, ordery, ierr;
  Real f, fTrue;
  Real xy[2], w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 1: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, xy);
      quad.weight(n, w);
      f += w*fcn(xy);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 2 polynomial; 3 points
BOOST_AUTO_TEST_CASE( _2 )
{
  const int degree = 2;
  const int npts = 3;

  QuadratureArea<Triangle> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad, nmonomial, orderx, ordery, ierr;
  Real f, fTrue;
  Real xy[2], w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    //printf( "deg 2: i = %d\n", i );
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 2: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, xy);
      quad.weight(n, w);
      f += w*fcn(xy);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 3 polynomial; 6 points
BOOST_AUTO_TEST_CASE( _3 )
{
  const int degree = 3;
  const int npts = 6;

  QuadratureArea<Triangle> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad, nmonomial, orderx, ordery, ierr;
  Real f, fTrue;
  Real xy[2], w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 3: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, xy);
      quad.weight(n, w);
      f += w*fcn(xy);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 3 polynomial; 6 points
// alternate .coordinates member function
BOOST_AUTO_TEST_CASE( _3_coord )
{
  const int degree = 3;
  const int npts = 6;

  QuadratureArea<Triangle> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad, nmonomial, orderx, ordery, ierr;
  Real f, fTrue;
  Real x, y, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 3: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y);
      quad.weight(n, w);
      f += w*fcn(x, y);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 4 polynomial; 6 points
BOOST_AUTO_TEST_CASE( _4_coord )
{
  const int degree = 4;
  const int npts = 6;

  QuadratureArea<Triangle> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad, nmonomial, orderx, ordery, ierr;
  Real f, fTrue;
  Real x, y, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 4: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y);
      quad.weight(n, w);
      f += w*fcn(x, y);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 5 polynomial; 7 points
BOOST_AUTO_TEST_CASE( _5_coord )
{
  const int degree = 5;
  const int npts = 7;

  QuadratureArea<Triangle> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad, nmonomial, orderx = -1, ordery = -1, ierr;
  Real f, fTrue = 0;
  Real x, y, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 5: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y);
      quad.weight(n, w);
      f += w*fcn(x, y);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 8 polynomial; 16 points
BOOST_AUTO_TEST_CASE( _8_coord )
{
  const int degree = 8;
  const int npts = 16;

  QuadratureArea<Triangle> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad, nmonomial, orderx = -1, ordery = -1, ierr;
  Real f, fTrue = 0;
  Real x, y, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 8: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y);
      quad.weight(n, w);
      f += w*fcn(x, y);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 10 polynomial; 25 points
BOOST_AUTO_TEST_CASE( _10_coord )
{
  const int degree = 10;
  const int npts = 25;

  QuadratureArea<Triangle> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-11;
  int nquad, nmonomial, orderx = -1, ordery = -1, ierr;
  Real f, fTrue = 0;
  Real x, y, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 8: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y);
      quad.weight(n, w);
      f += w*fcn(x, y);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 11 polynomial; 25 points
BOOST_AUTO_TEST_CASE( _11_coord )
{
  const int degree = 11;
  const int npts = 28;

  QuadratureArea<Triangle> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-11;
  int nquad, nmonomial, orderx = -1, ordery = -1, ierr;
  Real f, fTrue = 0;
  Real x, y, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 11: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y);
      quad.weight(n, w);
      f += w*fcn(x, y);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 12 polynomial; 33 points
BOOST_AUTO_TEST_CASE( _12_coord )
{
  const int degree = 12;
  const int npts = 33;

  QuadratureArea<Triangle> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-11;
  int nquad, nmonomial, orderx = -1, ordery = -1, ierr;
  Real f, fTrue = 0;
  Real x, y, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 12: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y);
      quad.weight(n, w);
      f += w*fcn(x, y);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 13 polynomial; 37 points
BOOST_AUTO_TEST_CASE( _13_coord )
{
  const int degree = 13;
  const int npts = 37;

  QuadratureArea<Triangle> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-11;
  int nquad, nmonomial, orderx = -1, ordery = -1, ierr;
  Real f, fTrue = 0;
  Real x, y, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 13: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y);
      quad.weight(n, w);
      f += w*fcn(x, y);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 14 polynomial; 46 points
BOOST_AUTO_TEST_CASE( _14_coord )
{
  const int degree = 14;
  const int npts = 46;

  QuadratureArea<Triangle> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-11;
  int nquad, nmonomial, orderx = -1, ordery = -1, ierr;
  Real f, fTrue = 0;
  Real x, y, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 13: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y);
      quad.weight(n, w);
      f += w*fcn(x, y);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 30 polynomial; 175 points
BOOST_AUTO_TEST_CASE( _30_coord )
{
  const int degree = 30;
  const int npts = 175;

  const int degreecheck = 14;

  QuadratureArea<Triangle> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-11;
  int nquad, nmonomial, orderx = -1, ordery = -1, ierr;
  Real f, fTrue = 0;
  Real x, y, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degreecheck);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degreecheck, orderx, ordery);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degreecheck, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 8: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn2D fcn(orderx, ordery, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y);
      quad.weight(n, w);
      f += w*fcn(x, y);
      wsum += w;
    }

    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Quadrature/QuadratureArea_Triangle_pattern.txt", true );

  QuadratureArea<Triangle> quad1(1);
  output << quad1 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad1.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Triangle> quad2(2);
  output << quad2 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad2.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Triangle> quad3(3);
  output << quad3 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad3.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Triangle> quad4(4);
  output << quad4 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad4.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Triangle> quad5(5);
  output << quad5 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad5.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Triangle> quad8(8);
  output << quad8 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad8.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Triangle> quad10(10);
  output << quad10 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad10.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Triangle> quad11(11);
  output << quad11 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad11.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Triangle> quad12(12);
  output << quad12 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad12.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Triangle> quad13(13);
  output << quad13 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad13.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Triangle> quad14(14);
  output << quad14 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad14.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureArea<Triangle> quad30(30);
  output << quad30 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad30.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
