// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// QuadratureVolume_Tetrahedron_btest
// testing of QuadratureVolume<Tet> class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "Quadrature/QuadratureSpacetime.h"
#include "Topology/ElementTopology.h"

using namespace SANS;

// local definitions
namespace
{

//----------------------------------------------------------------------------//
// monomial exponents:  x^i * y^j * z^k * t^m
struct MonomialExponents
{
  int i, j, k, m;
};


//----------------------------------------------------------------------------//
// exponents for all monomials of degree n or less
// Note: ordering not particularly elegant, but they're all included
void
getMonomialList( int n, std::vector<MonomialExponents>& list )
{
  MonomialExponents ijkm;

  int cnt = 0;
  for (int i = 0; i <= n; i++)
  {
    for (int j = 0; j <= n - i; j++)
    {
      for (int k = 0; k <= n - i - j; k++)
      {
        for (int m = 0; m <= n - i - j - k; m++)
        {
          ijkm.i = i;
          ijkm.j = j;
          ijkm.k = k;
          ijkm.m = m;

          list[cnt] = ijkm;
          cnt++;
        }
      }
    }
  }
}

//----------------------------------------------------------------------------//
long long
factorial( long long n )
{
  long long m;
  if (n <= 1)
    m = 1;
  else
    m = n * factorial(n - 1);

  return m;
}

#ifdef USE_CONICAL
//----------------------------------------------------------------------------//
long long
ipow(long long thing, long long power)
{
  long long value= 1;
  for (int i= 0; i < power; i++)
    value*= thing;

  return value;
}
#endif

//----------------------------------------------------------------------------//
// analytic integral for monomial
Real
getMonomialIntegral( MonomialExponents& ijkm )
{
  int i = ijkm.i;
  int j = ijkm.j;
  int k = ijkm.k;
  int m = ijkm.m;

#if 1
  Real f = factorial(i)*factorial(j)*factorial(k)*factorial(m) /
                static_cast<Real>(factorial(i + j + k + m + 4));
#else

  int kk = 0;
  Real f = 1.;
  int E[4] = {i,j,k,m};
  for (int ii=0;ii<4;ii++)
  for (int jj=1;jj<=E[ii];jj++)
  {
    kk++;
    f *= Real(jj)/Real(kk);
  }

  for (int ii=0;ii<4;ii++)
  {
    kk++;
    f = f/Real(kk);
  }
#endif
  //std::cout << "getMonomialIntegral: ijk = " << i << j << k << "  f = " << f << std::endl;

  return f;
}


//----------------------------------------------------------------------------//
// monomial class:  f(x) = a * x^i * y^j * z^k * t^m

class Monomial
{
public:
  explicit Monomial( MonomialExponents& ijkm ) :
    i_(ijkm.i), j_(ijkm.j), k_(ijkm.k), m_(ijkm.m) {}
  Monomial( int i, int j, int k, int m ) :
    i_(i), j_(j), k_(k), m_(m) {}
  ~Monomial() {}

  Real operator()( Real xyzt[] ) const;
  Real operator()( Real x, Real y, Real z, Real t) const;

private:
  int i_, j_, k_, m_;
};

Real
Monomial::operator()( Real xyzt[] ) const
{
  Real f;
  f = pow(xyzt[0], i_) * pow(xyzt[1], j_) * pow(xyzt[2], k_) * pow(xyzt[3],m_);

  return f;
}

Real
Monomial::operator()( Real x, Real y, Real z, Real t ) const
{
  Real f;
  f = pow(x, i_) * pow(y, j_) * pow(z, k_) * pow(t, m_);

  return f;
}

}  // local definitions


//############################################################################//
BOOST_AUTO_TEST_SUITE( QuadratureSpacetime_Pentatope_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orderReqd )
{

}


//----------------------------------------------------------------------------//
// degree 1 polynomial; 1 point
BOOST_AUTO_TEST_CASE( _1 )
{
  const int degree = 1;
  const int npts = 1;
  const Real tol = 1e-13;

  QuadratureSpacetime<Pentatope> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  //quad.dump(1);

  Real f0, f1, fTrue;
  Real x, y, z, t, xyzt[4], w, wsum;
  Real vol = 1./factorial(4); // volume of the right 4-simplex

  int size = (degree + 1)*(degree + 2)*(degree + 3)*(degree + 4)/24;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijkm;

//  printf("vol = %g\n",vol);

  for (int m = 0; m < size; m++)
  {
    ijkm = monomialList[m];

    fTrue = getMonomialIntegral( ijkm );

    Monomial fcn( ijkm );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyzt);
      quad.coordinates(n, x, y, z, t);
      quad.weight(n, w);
      f0 += w*fcn(xyzt);
      f1 += w*fcn(x, y, z, t);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;

    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }

}

//----------------------------------------------------------------------------//
// degree 2 polynomial; 6 points
BOOST_AUTO_TEST_CASE( _2 )
{
  const int degree = 2;
#ifdef USE_SPARSE
  const int npts= 5;
#elif USE_CONICAL
  const int npts= ipow(degree, 4);
#else
  const int npts = 6;
#endif
  const Real tol = 1e-13;

  QuadratureSpacetime<Pentatope> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  //quad.dump(1);

  Real f0, f1, fTrue;
  Real x, y, z, t, xyzt[4], w, wsum;
  Real vol = 1./factorial(4);

  int size = (degree + 1)*(degree + 2)*(degree + 3)*(degree + 4)/24;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijkm;

  for (int m = 0; m < size; m++)
  {
    ijkm = monomialList[m];

    fTrue = getMonomialIntegral( ijkm );

    Monomial fcn( ijkm );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyzt);
      quad.coordinates(n, x, y, z, t);
      quad.weight(n, w);
      f0 += w*fcn(xyzt);
      f1 += w*fcn(x, y, z, t);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }

}

//----------------------------------------------------------------------------//
// degree 3 polynomial; 6 points
BOOST_AUTO_TEST_CASE( _3 )
{
  const int degree = 3;
#ifdef USE_SPARSE
  const int npts= 15;
#elif USE_CONICAL
  const int npts= ipow(degree, 4);
#else
  const int npts = 6;
#endif
  const Real tol = 1e-12;

  QuadratureSpacetime<Pentatope> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  //quad.dump(1);

  Real f0, f1, fTrue;
  Real x, y, z, t, xyzt[4], w, wsum;
  Real vol = 1./factorial(4);

  int size = (degree + 1)*(degree + 2)*(degree + 3)*(degree + 4)/24;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijkm;

  for (int m = 0; m < size; m++)
  {
    ijkm = monomialList[m];

    fTrue = getMonomialIntegral( ijkm );

    Monomial fcn( ijkm );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyzt);
      quad.coordinates(n, x, y, z, t);
      quad.weight(n, w);
      f0 += w*fcn(xyzt);
      f1 += w*fcn(x, y, z, t);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }

}

//----------------------------------------------------------------------------//
// degree 5 polynomial; 21 points
BOOST_AUTO_TEST_CASE( _5 )
{
  const int degree = 5;
#ifdef USE_SPARSE
  const int npts= 35;
#elif USE_CONICAL
  const int npts= ipow(degree, 4);
#else
  const int npts = 21;
#endif
  const Real tol = 1e-10;

  QuadratureSpacetime<Pentatope> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  //quad.dump(1);

  Real f0, f1, fTrue;
  Real x, y, z, t, xyzt[4], w, wsum;
  Real vol = 1./factorial(4);

  int size = (degree + 1)*(degree + 2)*(degree + 3)*(degree + 4)/24;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijkm;

  for (int m = 0; m < size; m++)
  {
    ijkm = monomialList[m];

    fTrue = getMonomialIntegral( ijkm );

    Monomial fcn( ijkm );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyzt);
      quad.coordinates(n, x, y, z, t);
      quad.weight(n, w);
      f0 += w*fcn(xyzt);
      f1 += w*fcn(x, y, z, t);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }

}

//----------------------------------------------------------------------------//
// degree 6 polynomial; 56 points
BOOST_AUTO_TEST_CASE( _6 )
{
  const int degree = 6;
#ifdef USE_SPARSE
  const int npts= 70;
#elif USE_CONICAL
  const int npts= ipow(degree, 4);
#else
  const int npts = 56;
#endif
  const Real tol = 1e-10;

  QuadratureSpacetime<Pentatope> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  //quad.dump(1);

  Real f0, f1, fTrue;
  Real x, y, z, t, xyzt[4], w, wsum;
  Real vol = 1./factorial(4);

  int size = (degree + 1)*(degree + 2)*(degree + 3)*(degree + 4)/24;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijkm;

  for (int m = 0; m < size; m++)
  {
    ijkm = monomialList[m];

    fTrue = getMonomialIntegral( ijkm );

    Monomial fcn( ijkm );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyzt);
      quad.coordinates(n, x, y, z, t);
      quad.weight(n, w);
      f0 += w*fcn(xyzt);
      f1 += w*fcn(x, y, z, t);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }

}

//----------------------------------------------------------------------------//
// degree 7 polynomial; 35 points
BOOST_AUTO_TEST_CASE( _7 )
{
  const int degree = 7;
#ifdef USE_SPARSE
  const int npts= 126;
#elif USE_CONICAL
  const int npts= ipow(degree, 4);
#else
  const int npts = 56;
#endif
  const Real tol = 1e-09;

  QuadratureSpacetime<Pentatope> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  //quad.dump(1);

  Real f0, f1, fTrue;
  Real x, y, z, t, xyzt[4], w, wsum;
  Real vol = 1./factorial(4);

  int size = (degree + 1)*(degree + 2)*(degree + 3)*(degree + 4)/24;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijkm;

  for (int m = 0; m < size; m++)
  {
    ijkm = monomialList[m];

    fTrue = getMonomialIntegral( ijkm );

    Monomial fcn( ijkm );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyzt);
      quad.coordinates(n, x, y, z, t);
      quad.weight(n, w);
      f0 += w*fcn(xyzt);
      f1 += w*fcn(x, y, z, t);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }

}

//----------------------------------------------------------------------------//
// degree 8 polynomial; 126 points
BOOST_AUTO_TEST_CASE( _8 )
{
  const int degree = 8;
#ifdef USE_SPARSE
  const int npts= 126;
  const Real tol = 1e-08;
#elif USE_CONICAL
  const int npts= ipow(degree, 4);
  const Real tol = 1e-11;
#else
  const int npts = 126;
  const Real tol = 1e-11;
#endif

  QuadratureSpacetime<Pentatope> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  //quad.dump(1);

  Real f0, f1, fTrue;
  Real x, y, z, t, xyzt[4], w, wsum;
  Real vol = 1./factorial(4);

  int size = (degree + 1)*(degree + 2)*(degree + 3)*(degree + 4)/24;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijkm;

  for (int m = 0; m < size; m++)
  {
    ijkm = monomialList[m];

    fTrue = getMonomialIntegral( ijkm );

    Monomial fcn( ijkm );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyzt);
      quad.coordinates(n, x, y, z, t);
      quad.weight(n, w);
      f0 += w*fcn(xyzt);
      f1 += w*fcn(x, y, z, t);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }

}

#if !(defined(USE_SPARSE) || defined(USE_CONICAL))

//----------------------------------------------------------------------------//
// degree 9 polynomial; 126 points
BOOST_AUTO_TEST_CASE( _9 )
{
  const int degree = 9;
#ifdef USE_CONICAL
  const int npts= ipow(degree, 4);
#else
  const int npts = 126;
#endif
  const Real tol = 1e-11;

  QuadratureSpacetime<Pentatope> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  //quad.dump(1);

  Real f0, f1, fTrue;
  Real x, y, z, t, xyzt[4], w, wsum;
  Real vol = 1./factorial(4);

  int size = (degree + 1)*(degree + 2)*(degree + 3)*(degree + 4)/24;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijkm;

  for (int m = 0; m < size; m++)
  {
    ijkm = monomialList[m];

    fTrue = getMonomialIntegral( ijkm );

    Monomial fcn( ijkm );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyzt);
      quad.coordinates(n, x, y, z, t);
      quad.weight(n, w);
      f0 += w*fcn(xyzt);
      f1 += w*fcn(x, y, z, t);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }

}

//----------------------------------------------------------------------------//
// degree 10 polynomial; 252 points
BOOST_AUTO_TEST_CASE( _10 )
{
  const int degree = 10;
#ifdef USE_CONICAL
  const int npts= ipow(degree, 4);
#else
  const int npts = 252;
#endif
  const Real tol = 1e-11;

  QuadratureSpacetime<Pentatope> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  //quad.dump(1);

  Real f0, f1, fTrue;
  Real x, y, z, t, xyzt[4], w, wsum;
  Real vol = 1./factorial(4);

  int size = (degree + 1)*(degree + 2)*(degree + 3)*(degree + 4)/24;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijkm;

  for (int m = 0; m < size; m++)
  {
    ijkm = monomialList[m];

    fTrue = getMonomialIntegral( ijkm );

    Monomial fcn( ijkm );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyzt);
      quad.coordinates(n, x, y, z, t);
      quad.weight(n, w);
      f0 += w*fcn(xyzt);
      f1 += w*fcn(x, y, z, t);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 11 polynomial; 252 points
BOOST_AUTO_TEST_CASE( _11 )
{
  const int degree = 11;
#ifdef USE_CONICAL
  const int npts= ipow(degree, 4);
#else
  const int npts = 252;
#endif
  const Real tol = 1e-11;

  QuadratureSpacetime<Pentatope> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  //quad.dump(1);

  Real f0, f1, fTrue;
  Real x, y, z, t, xyzt[4], w, wsum;
  Real vol = 1./factorial(4);

  int size = (degree + 1)*(degree + 2)*(degree + 3)*(degree + 4)/24;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijkm;

  for (int m = 0; m < size; m++)
  {
    ijkm = monomialList[m];

    fTrue = getMonomialIntegral( ijkm );

    Monomial fcn( ijkm );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyzt);
      quad.coordinates(n, x, y, z, t);
      quad.weight(n, w);
      f0 += w*fcn(xyzt);
      f1 += w*fcn(x, y, z, t);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }

}

//----------------------------------------------------------------------------//
// degree 12 polynomial; 140 points
BOOST_AUTO_TEST_CASE( _12 )
{
  const int degree = 12;
#ifdef USE_CONICAL
  const int npts= ipow(degree, 4);
#else
  const int npts = 462;
#endif
  const Real tol = 1e-10;

  QuadratureSpacetime<Pentatope> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  //quad.dump(1);

  Real f0, f1, fTrue;
  Real x, y, z, t, xyzt[4], w, wsum;
  Real vol = 1./factorial(4);

  int size = (degree + 1)*(degree + 2)*(degree + 3)*(degree + 4)/24;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijkm;

  for (int m = 0; m < size; m++)
  {
    ijkm = monomialList[m];

    fTrue = getMonomialIntegral( ijkm );

    Monomial fcn( ijkm );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyzt);
      quad.coordinates(n, x, y, z, t);
      quad.weight(n, w);
      f0 += w*fcn(xyzt);
      f1 += w*fcn(x, y, z, t);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }

}

//----------------------------------------------------------------------------//
// degree 13 polynomial; 171 points
BOOST_AUTO_TEST_CASE( _13 )
{
  const int degree = 13;
#ifdef USE_CONICAL
  const int npts= ipow(degree, 4);
#else
  const int npts = 462;
#endif
  const Real tol = 1e-10;

  QuadratureSpacetime<Pentatope> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  //quad.dump(1);

  Real f0, f1, fTrue;
  Real x, y, z, t, xyzt[4], w, wsum;
  Real vol = 1./factorial(4);

  int size = (degree + 1)*(degree + 2)*(degree + 3)*(degree + 4)/24;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijkm;

  for (int m = 0; m < size; m++)
  {
    ijkm = monomialList[m];

    fTrue = getMonomialIntegral( ijkm );

    Monomial fcn( ijkm );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyzt);
      quad.coordinates(n, x, y, z, t);
      quad.weight(n, w);
      f0 += w*fcn(xyzt);
      f1 += w*fcn(x, y, z, t);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }


}

//----------------------------------------------------------------------------//
// degree 14 polynomial; 236 points
BOOST_AUTO_TEST_CASE( _14 )
{

  const int degree = 14;
#ifdef USE_CONICAL
  const int npts= ipow(degree, 4);
#else
  const int npts = 792;
#endif
  const Real tol = 1e-10;

  QuadratureSpacetime<Pentatope> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  //quad.dump(1);

  Real f0, f1, fTrue;
  Real x, y, z, t, xyzt[4], w, wsum;
  Real vol = 1./factorial(4);

  int size = (degree + 1)*(degree + 2)*(degree + 3)*(degree + 4)/24;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijkm;

  for (int m = 0; m < size; m++)
  {
    ijkm = monomialList[m];

    fTrue = getMonomialIntegral( ijkm );

    Monomial fcn( ijkm );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyzt);
      quad.coordinates(n, x, y, z, t);
      quad.weight(n, w);
      f0 += w*fcn(xyzt);
      f1 += w*fcn(x, y, z, t);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }


}

#endif // !USE_SPARSE AND !USE_CONICAL

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( getCacheIndexFromOrder )
{
  int orderReqd;
  int orderIdx;

#ifdef USE_SPARSE
  orderReqd= -1;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == QuadratureSpacetime<Pentatope>::nOrderIdx - 1);

  orderReqd= 0;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 0);

  orderReqd= 1;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 0);

  orderReqd= 2;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 1);

  orderReqd= 3;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 2);

  orderReqd= 4;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 3);

  orderReqd= 5;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 3);

  orderReqd= 6;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 4);

  orderReqd= 7;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 5);

  orderReqd= 8;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 6);

  orderReqd= 9;
  BOOST_CHECK_THROW(QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd), DeveloperException);

  // orderReqd= 8;
  // BOOST_CHECK_THROW(QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd), DeveloperException);
#elif USE_CONICAL
  orderReqd= -1;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == QuadratureSpacetime<Pentatope>::nOrderIdx - 1);

  orderReqd= 0;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 0);

  orderReqd= 1;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 0);

  orderReqd= 2;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 1);

  orderReqd= 3;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 2);

  orderReqd= 4;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 3);

  orderReqd= 5;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 4);

  orderReqd= 6;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 5);

  orderReqd= 7;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 6);

  orderReqd= 8;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 7);

  orderReqd= 9;
  BOOST_CHECK_THROW(QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd), DeveloperException);
#else
  orderReqd= -1;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == QuadratureSpacetime<Pentatope>::nOrderIdx - 1);

  orderReqd= 0;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 0);

  orderReqd= 1;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 0);

  orderReqd= 2;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 1);

  orderReqd= 3;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 2);

  orderReqd= 4;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 3);

  orderReqd= 5;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 3);

  orderReqd= 6;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 4);

  orderReqd= 7;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 5);

  orderReqd= 8;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 6);

  orderReqd= 9;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 7);

  orderReqd= 10;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 8);

  orderReqd= 11;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 9);

  orderReqd= 12;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 10);

  orderReqd= 13;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 11);

  orderReqd= 14;
  orderIdx= QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd);
  BOOST_CHECK(orderIdx == 12);

  orderReqd= 15;
  BOOST_CHECK_THROW(QuadratureSpacetime<Pentatope>::getOrderIndex(orderReqd), DeveloperException);
#endif

}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( getCacheOrderFromIndex )
{
  int orderIdx;
  int orderReqd;

#ifdef USE_SPARSE
  orderIdx= -1;
  BOOST_CHECK_THROW(QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx), DeveloperException);

  orderIdx= 0;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 1);

  orderIdx= 1;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 2);

  orderIdx= 2;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 3);

  orderIdx= 3;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 5);

  orderIdx= 4;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 6);

  orderIdx= 5;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 7);

  orderIdx= 6;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 8);

  orderIdx= 7;
  BOOST_CHECK_THROW(QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx), DeveloperException);
#elif USE_CONICAL
  orderIdx= -1;
  BOOST_CHECK_THROW(QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx), DeveloperException);

  orderIdx= 0;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 1);

  orderIdx= 1;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 2);

  orderIdx= 2;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 3);

  orderIdx= 3;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 4);

  orderIdx= 4;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 5);

  orderIdx= 5;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 6);

  orderIdx= 6;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 7);

  orderIdx= 7;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 8);

  orderIdx= 8;
  BOOST_CHECK_THROW(QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx), DeveloperException);
#else
  orderIdx= -1;
  BOOST_CHECK_THROW(QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx), DeveloperException);

  orderIdx= 0;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 1);

  orderIdx= 1;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 2);

  orderIdx= 2;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 3);

  orderIdx= 3;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 5);

  orderIdx= 4;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 6);

  orderIdx= 5;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 7);

  orderIdx= 6;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 8);

  orderIdx= 7;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 9);

  orderIdx= 8;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 10);

  orderIdx= 9;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 11);

  orderIdx= 10;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 12);

  orderIdx= 11;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 13);

  orderIdx= 12;
  orderReqd= QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx);
  BOOST_CHECK(orderReqd == 14);

  orderIdx= 13;
  BOOST_CHECK_THROW(QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderIdx), DeveloperException);
#endif

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
