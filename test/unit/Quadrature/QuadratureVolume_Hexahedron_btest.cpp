// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// QuadratureVolume_Hexahedron_btest
// testing of QuadratureVolume<Hex> class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"
#include "Quadrature/QuadratureVolume.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( QuadratureVolume_Hexahedron_test_suite )

//----------------------------------------------------------------------------//
// monomials of given degree
static int
getMonomialSize( int degree )
{
  //This assumes tensor product quadrature
  return degree;
#if 0
  int size;
  if (degree == 0)
    size = 1;
  else if (degree == 1)
    size = 3;
  else if (degree == 2)
    size = 6;
  else if (degree == 3)
    size = 10;
  else if (degree == 4)
    size = 15;
  else if (degree == 5)
    size = 21;
  else
    size = -1;

  return size;
#endif
}

static int
getMonomial( int n, int degree, int& xexp, int& yexp, int& zexp )
{
  if (n >= getMonomialSize(degree))
  {
    printf( "Error in %s: n = %d >= getMonomialSize(%d) = %d\n", __func__,
      n, degree, getMonomialSize(degree) );
    return 1;
  }

  // This assumes tenspor product quadrature
  xexp = yexp = zexp = n;

#if 0
  if (n == 0)                // const
  {
    xexp = yexp = zexp = 0;
  }
  else if (n < 3)            // linear
  {
    yexp = n - 1;
    xexp = 1 - yexp;
  }
  else if (n < 6)            // quadratic
  {
    yexp = n - 3;
    xexp = 2 - yexp;
  }
  else if (n < 10)           // cubic
  {
    yexp = n - 6;
    xexp = 3 - yexp;
  }
  else if (n < 15)           // quartic
  {
    yexp = n - 10;
    xexp = 4 - yexp;
  }
  else if (n < 21)           // quintic
  {
    yexp = n - 15;
    xexp = 5 - yexp;
  }
  else
    BOOST_ERROR("Developer error");
#endif

  return 0;
}

static int
getMonomialIntegral( int n, int degree, Real& f )
{
  if (n >= getMonomialSize(degree))
    return 1;


  int xexp, yexp, zexp;

  // This assumes a tensor product
  xexp = yexp = zexp = n;
#if 0
  if (n == 0)                // const
  {
    xexp = yexp = 0;
  }
  else if (n < 3)            // linear
  {
    yexp = n - 1;
    xexp = 1 - yexp;
  }
  else if (n < 6)            // quadratic
  {
    yexp = n - 3;
    xexp = 2 - yexp;
  }
  else if (n < 10)           // cubic
  {
    yexp = n - 6;
    xexp = 3 - yexp;
  }
  else if (n < 15)           // quartic
  {
    yexp = n - 10;
    xexp = 4 - yexp;
  }
  else if (n < 21)           // quintic
  {
    yexp = n - 15;
    xexp = 5 - yexp;
  }
  else
  {
    yexp = 0;
    xexp = 0;
    BOOST_ERROR("Developer error");
  }
#endif

  f = 1./static_cast<Real>((1 + xexp)*(1 + yexp)*(1 + zexp));

  return 0;
}


//----------------------------------------------------------------------------//
// monomial class:  f(x) = a * x^i * y^j * z^k

class Fcn3D
{
public:
  Fcn3D( int orderx, int ordery, int orderz, Real coef ) : orderx_(orderx), ordery_(ordery), orderz_(orderz), coef_(coef) {}
  ~Fcn3D() {}

  Real operator()( Real xyz[] ) const;
  Real operator()( Real x, Real y, Real z ) const;

private:
  int orderx_, ordery_, orderz_;
  Real coef_;
};

Real
Fcn3D::operator()( Real xyz[] ) const
{
  Real f;
  f = coef_ * pow(xyz[0], orderx_) * pow(xyz[1], ordery_) * pow(xyz[2], orderz_);

  return f;
}

Real
Fcn3D::operator()( Real x, Real y, Real z ) const
{
  Real f;
  f = coef_ * pow(x, orderx_) * pow(y, ordery_) * pow(z, orderz_);

  return f;
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orderReqd )
{
  QuadratureVolume<Hex> quadm1(-1);
  BOOST_CHECK_EQUAL( 39, quadm1.order() );
  BOOST_CHECK_EQUAL( 8000, quadm1.nQuadrature() );

  QuadratureVolume<Hex> quad0(0);
  BOOST_CHECK_EQUAL( 1, quad0.order() );
  BOOST_CHECK_EQUAL( 1, quad0.nQuadrature() );

  QuadratureVolume<Hex> quad1(1);
  BOOST_CHECK_EQUAL( 1, quad1.order() );
  BOOST_CHECK_EQUAL( 1, quad1.nQuadrature() );

  QuadratureVolume<Hex> quad2(2);
  BOOST_CHECK_EQUAL( 3, quad2.order() );
  BOOST_CHECK_EQUAL( 8, quad2.nQuadrature() );

  QuadratureVolume<Hex> quad3(3);
  BOOST_CHECK_EQUAL( 3, quad3.order() );
  BOOST_CHECK_EQUAL( 8, quad3.nQuadrature() );

  QuadratureVolume<Hex> quad4(4);
  BOOST_CHECK_EQUAL( 5, quad4.order() );
  BOOST_CHECK_EQUAL( 27, quad4.nQuadrature() );

  QuadratureVolume<Hex> quad5(5);
  BOOST_CHECK_EQUAL( 5, quad5.order() );
  BOOST_CHECK_EQUAL( 27, quad5.nQuadrature() );

  QuadratureVolume<Hex> quad6(6);
  BOOST_CHECK_EQUAL( 7, quad6.order() );
  BOOST_CHECK_EQUAL( 64, quad6.nQuadrature() );

  QuadratureVolume<Hex> quad7(7);
  BOOST_CHECK_EQUAL( 7, quad7.order() );
  BOOST_CHECK_EQUAL( 64, quad7.nQuadrature() );

  QuadratureVolume<Hex> quad8(8);
  BOOST_CHECK_EQUAL( 9, quad8.order() );
  BOOST_CHECK_EQUAL( 125, quad8.nQuadrature() );

  QuadratureVolume<Hex> quad9(9);
  BOOST_CHECK_EQUAL( 9, quad9.order() );
  BOOST_CHECK_EQUAL( 125, quad9.nQuadrature() );

  QuadratureVolume<Hex> quad10(10);
  BOOST_CHECK_EQUAL( 11, quad10.order() );
  BOOST_CHECK_EQUAL( 6*6*6, quad10.nQuadrature() );

  QuadratureVolume<Hex> quad11(11);
  BOOST_CHECK_EQUAL( 11, quad11.order() );
  BOOST_CHECK_EQUAL( 6*6*6, quad11.nQuadrature() );

  QuadratureVolume<Hex> quad12(12);
  BOOST_CHECK_EQUAL( 13, quad12.order() );
  BOOST_CHECK_EQUAL( 7*7*7, quad12.nQuadrature() );

  QuadratureVolume<Hex> quad13(13);
  BOOST_CHECK_EQUAL( 13, quad13.order() );
  BOOST_CHECK_EQUAL( 7*7*7, quad13.nQuadrature() );

  QuadratureVolume<Hex> quad14(14);
  BOOST_CHECK_EQUAL( 15, quad14.order() );
  BOOST_CHECK_EQUAL( 8*8*8, quad14.nQuadrature() );

  QuadratureVolume<Hex> quad15(15);
  BOOST_CHECK_EQUAL( 15, quad15.order() );
  BOOST_CHECK_EQUAL( 8*8*8, quad15.nQuadrature() );

  QuadratureVolume<Hex> quad16(16);
  BOOST_CHECK_EQUAL( 17, quad16.order() );
  BOOST_CHECK_EQUAL( 9*9*9, quad16.nQuadrature() );

  QuadratureVolume<Hex> quad17(17);
  BOOST_CHECK_EQUAL( 17, quad17.order() );
  BOOST_CHECK_EQUAL( 9*9*9, quad17.nQuadrature() );

  QuadratureVolume<Hex> quad18(18);
  BOOST_CHECK_EQUAL( 19, quad18.order() );
  BOOST_CHECK_EQUAL( 10*10*10, quad18.nQuadrature() );

  QuadratureVolume<Hex> quad19(19);
  BOOST_CHECK_EQUAL( 19, quad19.order() );
  BOOST_CHECK_EQUAL( 10*10*10, quad19.nQuadrature() );

  QuadratureVolume<Hex> quad20(20);
  BOOST_CHECK_EQUAL( 21, quad20.order() );
  BOOST_CHECK_EQUAL( 11*11*11, quad20.nQuadrature() );

  QuadratureVolume<Hex> quad21(21);
  BOOST_CHECK_EQUAL( 21, quad21.order() );
  BOOST_CHECK_EQUAL( 11*11*11, quad21.nQuadrature() );

  QuadratureVolume<Hex> quad22(22);
  BOOST_CHECK_EQUAL( 23, quad22.order() );
  BOOST_CHECK_EQUAL( 12*12*12, quad22.nQuadrature() );

  QuadratureVolume<Hex> quad23(23);
  BOOST_CHECK_EQUAL( 23, quad23.order() );
  BOOST_CHECK_EQUAL( 12*12*12, quad23.nQuadrature() );

  QuadratureVolume<Hex> quad39(39);
  BOOST_CHECK_EQUAL( 39, quad39.order() );
  BOOST_CHECK_EQUAL( 20*20*20, quad39.nQuadrature() );

  BOOST_CHECK_THROW( QuadratureVolume<Hex> quad40(40), DeveloperException );

  BOOST_CHECK( QuadratureVolume<Hex>::nOrderIdx == 13); // should match QuadratureLine

  for (int orderidx = 0; orderidx < QuadratureVolume<Hex>::nOrderIdx; orderidx++)
  {
    // Check that the orderidx -> order -> orderidx
    BOOST_CHECK_EQUAL( orderidx, QuadratureVolume<Hex>::getOrderIndex( QuadratureVolume<Hex>::getOrderFromIndex(orderidx) ) );
  }

  BOOST_CHECK_EQUAL( 0, QuadratureVolume<Hex>::getOrderIndex(0) );
  BOOST_CHECK_EQUAL( 0, QuadratureVolume<Hex>::getOrderIndex(1) );
  BOOST_CHECK_EQUAL( 1, QuadratureVolume<Hex>::getOrderIndex(2) );
  BOOST_CHECK_EQUAL( 1, QuadratureVolume<Hex>::getOrderIndex(3) );
  BOOST_CHECK_EQUAL( 2, QuadratureVolume<Hex>::getOrderIndex(4) );
  BOOST_CHECK_EQUAL( 2, QuadratureVolume<Hex>::getOrderIndex(5) );
  BOOST_CHECK_EQUAL( 3, QuadratureVolume<Hex>::getOrderIndex(6) );
  BOOST_CHECK_EQUAL( 3, QuadratureVolume<Hex>::getOrderIndex(7) );
  BOOST_CHECK_EQUAL( 4, QuadratureVolume<Hex>::getOrderIndex(8) );
  BOOST_CHECK_EQUAL( 4, QuadratureVolume<Hex>::getOrderIndex(9) );
  BOOST_CHECK_EQUAL( 5, QuadratureVolume<Hex>::getOrderIndex(10) );
  BOOST_CHECK_EQUAL( 5, QuadratureVolume<Hex>::getOrderIndex(11) );
  BOOST_CHECK_EQUAL( 6, QuadratureVolume<Hex>::getOrderIndex(12) );
  BOOST_CHECK_EQUAL( 6, QuadratureVolume<Hex>::getOrderIndex(13) );
  BOOST_CHECK_EQUAL( 7, QuadratureVolume<Hex>::getOrderIndex(14) );
  BOOST_CHECK_EQUAL( 7, QuadratureVolume<Hex>::getOrderIndex(15) );
  BOOST_CHECK_EQUAL( 8, QuadratureVolume<Hex>::getOrderIndex(16) );
  BOOST_CHECK_EQUAL( 8, QuadratureVolume<Hex>::getOrderIndex(17) );
  BOOST_CHECK_EQUAL( 9, QuadratureVolume<Hex>::getOrderIndex(18) );
  BOOST_CHECK_EQUAL( 9, QuadratureVolume<Hex>::getOrderIndex(19) );
  BOOST_CHECK_EQUAL(10, QuadratureVolume<Hex>::getOrderIndex(20) );
  BOOST_CHECK_EQUAL(10, QuadratureVolume<Hex>::getOrderIndex(21) );
  BOOST_CHECK_EQUAL(11, QuadratureVolume<Hex>::getOrderIndex(22) );
  BOOST_CHECK_EQUAL(11, QuadratureVolume<Hex>::getOrderIndex(23) );
  BOOST_CHECK_EQUAL(12, QuadratureVolume<Hex>::getOrderIndex(24) );
  BOOST_CHECK_EQUAL(12, QuadratureVolume<Hex>::getOrderIndex(39) );
  BOOST_CHECK_EQUAL(12, QuadratureVolume<Hex>::getOrderIndex(-1) );
  BOOST_CHECK_THROW( QuadratureVolume<Hex>::getOrderIndex(40), DeveloperException );
}


//----------------------------------------------------------------------------//
// degree 1 polynomial; 1 point
BOOST_AUTO_TEST_CASE( _1a )
{
  QuadratureVolume<Hex> quad(1);
  BOOST_CHECK_EQUAL( 1, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad;
  Real f, fTrue;
  Real xyz[3], w, wsum;

  nquad = quad.nQuadrature();

  // f(x) = 1
  Fcn3D fcn0(0, 0, 0, 1);
  fTrue = 1;
  f = 0;
  wsum = 0;
  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, xyz);
    quad.weight(n, w);
    f += w*fcn0(xyz);
    wsum += w;
  }
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( 1, wsum, tol );

  // f(x) = x
  Fcn3D fcn1(1, 0, 0, 1);
  fTrue = 0.5;
  f = 0;
  wsum = 0;
  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, xyz);
    quad.weight(n, w);
    f += w*fcn1(xyz);
    wsum += w;
  }
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( 1, wsum, tol );

  // f(x) = y
  Fcn3D fcn2(0, 1, 0, 1);
  fTrue = 0.5;
  f = 0;
  wsum = 0;
  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, xyz);
    quad.weight(n, w);
    f += w*fcn2(xyz);
    wsum += w;
  }
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( 1, wsum, tol );

  // f(x) = y
  Fcn3D fcn3(0, 0, 1, 1);
  fTrue = 0.5;
  f = 0;
  wsum = 0;
  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, xyz);
    quad.weight(n, w);
    f += w*fcn3(xyz);
    wsum += w;
  }
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( 1, wsum, tol );

}


//----------------------------------------------------------------------------//
// degree 1 polynomial;
BOOST_AUTO_TEST_CASE( _1 )
{
  const int degree = 1;
  const int npts = 1;

  QuadratureVolume<Hex> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad, nmonomial, orderx, ordery, orderz, ierr;
  Real f, fTrue;
  Real xyz[3], w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery, orderz);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 1: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn3D fcn(orderx, ordery, orderz, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, xyz);
      quad.weight(n, w);
      f += w*fcn(xyz);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 3 polynomial;
BOOST_AUTO_TEST_CASE( _3 )
{
  const int degree = 3;
  const int npts = 8;

  QuadratureVolume<Hex> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad, nmonomial, orderx, ordery, orderz, ierr;
  Real f, fTrue;
  Real xyz[3], w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
//    printf( "deg 3: i = %d\n", i );
    ierr = getMonomial(i, degree, orderx, ordery, orderz);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 3: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn3D fcn(orderx, ordery, orderz, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, xyz);
      quad.weight(n, w);
      f += w*fcn(xyz);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 5 polynomial;
BOOST_AUTO_TEST_CASE( _5 )
{
  const int degree = 5;
  const int npts = 27;

  QuadratureVolume<Hex> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad, nmonomial, orderx = -1, ordery = -1, orderz = -1, ierr;
  Real f, fTrue = 0;
  Real xyz[3], w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery, orderz);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 5: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn3D fcn(orderx, ordery, orderz, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, xyz);
      quad.weight(n, w);
      f += w*fcn(xyz);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 5 polynomial;
// alternate .coordinates() member function
BOOST_AUTO_TEST_CASE( _5_coord )
{
  const int degree = 5;
  const int npts = 27;

  QuadratureVolume<Hex> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad, nmonomial, orderx = -1, ordery = -1, orderz = -1, ierr;
  Real f, fTrue = 0;
  Real x, y, z, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery, orderz);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 5: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn3D fcn(orderx, ordery, orderz, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f += w*fcn(x, y, z);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 7 polynomial;
// alternate .coordinates() member function
BOOST_AUTO_TEST_CASE( _7_coord )
{
  const int degree = 7;
  const int npts = 4*4*4;

  QuadratureVolume<Hex> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 5e-13;
  int nquad, nmonomial, orderx = -1, ordery = -1, orderz = -1, ierr;
  Real f, fTrue = 0;
  Real x, y, z, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery, orderz);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 5: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn3D fcn(orderx, ordery, orderz, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f += w*fcn(x, y, z);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 9 polynomial;
// alternate .coordinates() member function
BOOST_AUTO_TEST_CASE( _9_coord )
{
  const int degree = 9;
  const int npts = 5*5*5;

  QuadratureVolume<Hex> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 5e-13;
  int nquad, nmonomial, orderx = -1, ordery = -1, orderz = -1, ierr;
  Real f, fTrue = 0;
  Real x, y, z, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery, orderz);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 5: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn3D fcn(orderx, ordery, orderz, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f += w*fcn(x, y, z);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 11 polynomial;
// alternate .coordinates() member function
BOOST_AUTO_TEST_CASE( _11_coord )
{
  const int degree = 11;
  const int npts = 6*6*6;

  QuadratureVolume<Hex> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 5e-13;
  int nquad, nmonomial, orderx = -1, ordery = -1, orderz = -1, ierr;
  Real f, fTrue = 0;
  Real x, y, z, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery, orderz);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 5: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn3D fcn(orderx, ordery, orderz, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f += w*fcn(x, y, z);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 13 polynomial;
// alternate .coordinates() member function
BOOST_AUTO_TEST_CASE( _13_coord )
{
  const int degree = 13;
  const int npts = 7*7*7;

  QuadratureVolume<Hex> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 5e-13;
  int nquad, nmonomial, orderx = -1, ordery = -1, orderz = -1, ierr;
  Real f, fTrue = 0;
  Real x, y, z, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery, orderz);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 5: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn3D fcn(orderx, ordery, orderz, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f += w*fcn(x, y, z);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 15 polynomial;
// alternate .coordinates() member function
BOOST_AUTO_TEST_CASE( _15_coord )
{
  const int degree = 15;
  const int npts = 8*8*8;

  QuadratureVolume<Hex> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 5e-13;
  int nquad, nmonomial, orderx = -1, ordery = -1, orderz = -1, ierr;
  Real f, fTrue = 0;
  Real x, y, z, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery, orderz);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 5: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn3D fcn(orderx, ordery, orderz, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f += w*fcn(x, y, z);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 17 polynomial;
// alternate .coordinates() member function
BOOST_AUTO_TEST_CASE( _17_coord )
{
  const int degree = 17;
  const int npts = 9*9*9;

  QuadratureVolume<Hex> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 5e-13;
  int nquad, nmonomial, orderx = -1, ordery = -1, orderz = -1, ierr;
  Real f, fTrue = 0;
  Real x, y, z, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery, orderz);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 5: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn3D fcn(orderx, ordery, orderz, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f += w*fcn(x, y, z);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 19 polynomial;
// alternate .coordinates() member function
BOOST_AUTO_TEST_CASE( _19_coord )
{
  const int degree = 19;
  const int npts = 10*10*10;

  QuadratureVolume<Hex> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 5e-13;
  int nquad, nmonomial, orderx = -1, ordery = -1, orderz = -1, ierr;
  Real f, fTrue = 0;
  Real x, y, z, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery, orderz);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 5: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn3D fcn(orderx, ordery, orderz, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f += w*fcn(x, y, z);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 21 polynomial;
// alternate .coordinates() member function
BOOST_AUTO_TEST_CASE( _21_coord )
{
  const int degree = 21;
  const int npts = 11*11*11;

  QuadratureVolume<Hex> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 5e-13;
  int nquad, nmonomial, orderx = -1, ordery = -1, orderz = -1, ierr;
  Real f, fTrue = 0;
  Real x, y, z, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery, orderz);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 5: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn3D fcn(orderx, ordery, orderz, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f += w*fcn(x, y, z);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 23 polynomial;
// alternate .coordinates() member function
BOOST_AUTO_TEST_CASE( _23_coord )
{
  const int degree = 23;
  const int npts = 12*12*12;

  QuadratureVolume<Hex> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 5e-13;
  int nquad, nmonomial, orderx = -1, ordery = -1, orderz = -1, ierr;
  Real f, fTrue = 0;
  Real x, y, z, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery, orderz);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 5: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn3D fcn(orderx, ordery, orderz, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f += w*fcn(x, y, z);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 39 polynomial;
// alternate .coordinates() member function
BOOST_AUTO_TEST_CASE( _39_coord )
{
  const int degree = 39;
  const int npts = 20*20*20;

  QuadratureVolume<Hex> quad(degree);
  BOOST_REQUIRE_EQUAL( npts, quad.nQuadrature() );

  const Real tol = 5e-13;
  int nquad, nmonomial, orderx = -1, ordery = -1, orderz = -1, ierr;
  Real f, fTrue = 0;
  Real x, y, z, w, wsum;

  nquad = quad.nQuadrature();

  nmonomial = getMonomialSize(degree);

  for (int i = 0; i < nmonomial; i++)
  {
    ierr = getMonomial(i, degree, orderx, ordery, orderz);
    BOOST_REQUIRE_EQUAL( 0, ierr );
    ierr = getMonomialIntegral(i, degree, fTrue);
    BOOST_REQUIRE_EQUAL( 0, ierr );

    //printf( "deg 5: n = %d  orderx = %d  ordery = %d\n", i, orderx, ordery );
    Fcn3D fcn(orderx, ordery, orderz, 1);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f += w*fcn(x, y, z);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Quadrature/QuadratureVolume_Hex_pattern.txt", true );

  QuadratureVolume<Hex> quad1(1);
  output << quad1 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad1.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Hex> quad3(3);
  output << quad3 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad3.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Hex> quad5(5);
  output << quad5 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad5.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Hex> quad7(7);
  output << quad7 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad7.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Hex> quad9(9);
  output << quad9 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad9.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Hex> quad11(11);
  output << quad11 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad11.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Hex> quad13(13);
  output << quad13 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad13.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Hex> quad15(15);
  output << quad15 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad15.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Hex> quad17(17);
  output << quad17 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad17.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Hex> quad19(19);
  output << quad19 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad19.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Hex> quad21(21);
  output << quad21 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad21.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Hex> quad23(23);
  output << quad23 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad23.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Hex> quad39(39);
  output << quad39 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad39.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
