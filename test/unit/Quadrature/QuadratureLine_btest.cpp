// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// QuadratureLine_gtest
// testing of QuadratureLine class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "Quadrature/QuadratureLine.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( QuadratureLine_test_suite )

//----------------------------------------------------------------------------//
// monomial class:  f(x) = a*x^n

class Fcn1D
{
public:
  Fcn1D( int order, Real coef ) : order_(order), coef_(coef) {}
  ~Fcn1D() {}

  Real operator()( Real x ) const;

private:
  int order_;
  int coef_;
};

Real
Fcn1D::operator()( Real x ) const
{
  Real f;
  switch (order_)
  {
  case 0 :
    f = coef_;
    break;
  case 1 :
    f = coef_*x;
    break;
  case 2 :
    f = coef_*x*x;
    break;
  case 3 :
    f = coef_*x*x*x;
    break;
  case 4 :
    f = coef_*x*x*x*x;
    break;
  case 5 :
    f = coef_*x*x*x*x*x;
    break;
  case 6 :
    f = coef_*x*x*x*x*x*x;
    break;
  case 7 :
    f = coef_*x*x*x*x*x*x*x;
    break;
  case 8 :
    f = coef_*x*x*x*x*x*x*x*x;
    break;
  case 9 :
    f = coef_*x*x*x*x*x*x*x*x*x;
    break;
  default:
    f = coef_*pow(x, order_);
    break;
  }

  return f;
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orderReqd )
{
  QuadratureLine quadm1(-1);
  BOOST_CHECK_EQUAL( 39, quadm1.order() );
  BOOST_CHECK_EQUAL( 20, quadm1.nQuadrature() );

  QuadratureLine quad0(0);
  BOOST_CHECK_EQUAL( 1, quad0.order() );
  BOOST_CHECK_EQUAL( 1, quad0.nQuadrature() );

  QuadratureLine quad1(1);
  BOOST_CHECK_EQUAL( 1, quad1.order() );
  BOOST_CHECK_EQUAL( 1, quad1.nQuadrature() );

  QuadratureLine quad2(2);
  BOOST_CHECK_EQUAL( 3, quad2.order() );
  BOOST_CHECK_EQUAL( 2, quad2.nQuadrature() );

  QuadratureLine quad3(3);
  BOOST_CHECK_EQUAL( 3, quad3.order() );
  BOOST_CHECK_EQUAL( 2, quad3.nQuadrature() );

  QuadratureLine quad4(4);
  BOOST_CHECK_EQUAL( 5, quad4.order() );
  BOOST_CHECK_EQUAL( 3, quad4.nQuadrature() );

  QuadratureLine quad5(5);
  BOOST_CHECK_EQUAL( 5, quad5.order() );
  BOOST_CHECK_EQUAL( 3, quad5.nQuadrature() );

  QuadratureLine quad6(6);
  BOOST_CHECK_EQUAL( 7, quad6.order() );
  BOOST_CHECK_EQUAL( 4, quad6.nQuadrature() );

  QuadratureLine quad7(7);
  BOOST_CHECK_EQUAL( 7, quad7.order() );
  BOOST_CHECK_EQUAL( 4, quad7.nQuadrature() );

  QuadratureLine quad8(8);
  BOOST_CHECK_EQUAL( 9, quad8.order() );
  BOOST_CHECK_EQUAL( 5, quad8.nQuadrature() );

  QuadratureLine quad9(9);
  BOOST_CHECK_EQUAL( 9, quad9.order() );
  BOOST_CHECK_EQUAL( 5, quad9.nQuadrature() );

  QuadratureLine quad10(10);
  BOOST_CHECK_EQUAL( 11, quad10.order() );
  BOOST_CHECK_EQUAL( 6, quad10.nQuadrature() );

  QuadratureLine quad11(11);
  BOOST_CHECK_EQUAL( 11, quad11.order() );
  BOOST_CHECK_EQUAL( 6, quad11.nQuadrature() );

  QuadratureLine quad12(12);
  BOOST_CHECK_EQUAL( 13, quad12.order() );
  BOOST_CHECK_EQUAL( 7, quad12.nQuadrature() );

  QuadratureLine quad13(13);
  BOOST_CHECK_EQUAL( 13, quad13.order() );
  BOOST_CHECK_EQUAL( 7, quad13.nQuadrature() );

  QuadratureLine quad14(14);
  BOOST_CHECK_EQUAL( 15, quad14.order() );
  BOOST_CHECK_EQUAL( 8, quad14.nQuadrature() );

  QuadratureLine quad15(15);
  BOOST_CHECK_EQUAL( 15, quad15.order() );
  BOOST_CHECK_EQUAL( 8, quad15.nQuadrature() );

  QuadratureLine quad16(16);
  BOOST_CHECK_EQUAL( 17, quad16.order() );
  BOOST_CHECK_EQUAL( 9, quad16.nQuadrature() );

  QuadratureLine quad17(17);
  BOOST_CHECK_EQUAL( 17, quad17.order() );
  BOOST_CHECK_EQUAL( 9, quad17.nQuadrature() );

  QuadratureLine quad18(18);
  BOOST_CHECK_EQUAL( 19, quad18.order() );
  BOOST_CHECK_EQUAL( 10, quad18.nQuadrature() );

  QuadratureLine quad19(19);
  BOOST_CHECK_EQUAL( 19, quad19.order() );
  BOOST_CHECK_EQUAL( 10, quad19.nQuadrature() );

  QuadratureLine quad20(20);
  BOOST_CHECK_EQUAL( 21, quad20.order() );
  BOOST_CHECK_EQUAL( 11, quad20.nQuadrature() );

  QuadratureLine quad21(21);
  BOOST_CHECK_EQUAL( 21, quad21.order() );
  BOOST_CHECK_EQUAL( 11, quad21.nQuadrature() );

  QuadratureLine quad22(22);
  BOOST_CHECK_EQUAL( 23, quad22.order() );
  BOOST_CHECK_EQUAL( 12, quad22.nQuadrature() );

  QuadratureLine quad23(23);
  BOOST_CHECK_EQUAL( 23, quad23.order() );
  BOOST_CHECK_EQUAL( 12, quad23.nQuadrature() );

  QuadratureLine quad39(39);
  BOOST_CHECK_EQUAL( 39, quad39.order() );
  BOOST_CHECK_EQUAL( 20, quad39.nQuadrature() );

  BOOST_CHECK_THROW( QuadratureLine quad40(40), DeveloperException );

  BOOST_CHECK( QuadratureLine::nOrderIdx == 13);

  for (int orderidx = 0; orderidx < QuadratureLine::nOrderIdx; orderidx++)
  {
    // Check that the orderidx -> order -> orderidx
    BOOST_CHECK_EQUAL( orderidx, QuadratureLine::getOrderIndex( QuadratureLine::getOrderFromIndex(orderidx) ) );
  }

  BOOST_CHECK_EQUAL( 0, QuadratureLine::getOrderIndex(0) );
  BOOST_CHECK_EQUAL( 0, QuadratureLine::getOrderIndex(1) );
  BOOST_CHECK_EQUAL( 1, QuadratureLine::getOrderIndex(2) );
  BOOST_CHECK_EQUAL( 1, QuadratureLine::getOrderIndex(3) );
  BOOST_CHECK_EQUAL( 2, QuadratureLine::getOrderIndex(4) );
  BOOST_CHECK_EQUAL( 2, QuadratureLine::getOrderIndex(5) );
  BOOST_CHECK_EQUAL( 3, QuadratureLine::getOrderIndex(6) );
  BOOST_CHECK_EQUAL( 3, QuadratureLine::getOrderIndex(7) );
  BOOST_CHECK_EQUAL( 4, QuadratureLine::getOrderIndex(8) );
  BOOST_CHECK_EQUAL( 4, QuadratureLine::getOrderIndex(9) );
  BOOST_CHECK_EQUAL( 5, QuadratureLine::getOrderIndex(10) );
  BOOST_CHECK_EQUAL( 5, QuadratureLine::getOrderIndex(11) );
  BOOST_CHECK_EQUAL( 6, QuadratureLine::getOrderIndex(12) );
  BOOST_CHECK_EQUAL( 6, QuadratureLine::getOrderIndex(13) );
  BOOST_CHECK_EQUAL( 7, QuadratureLine::getOrderIndex(14) );
  BOOST_CHECK_EQUAL( 7, QuadratureLine::getOrderIndex(15) );
  BOOST_CHECK_EQUAL( 8, QuadratureLine::getOrderIndex(16) );
  BOOST_CHECK_EQUAL( 8, QuadratureLine::getOrderIndex(17) );
  BOOST_CHECK_EQUAL( 9, QuadratureLine::getOrderIndex(18) );
  BOOST_CHECK_EQUAL( 9, QuadratureLine::getOrderIndex(19) );
  BOOST_CHECK_EQUAL(10, QuadratureLine::getOrderIndex(20) );
  BOOST_CHECK_EQUAL(10, QuadratureLine::getOrderIndex(21) );
  BOOST_CHECK_EQUAL(11, QuadratureLine::getOrderIndex(22) );
  BOOST_CHECK_EQUAL(11, QuadratureLine::getOrderIndex(23) );
  BOOST_CHECK_EQUAL(12, QuadratureLine::getOrderIndex(24) );
  BOOST_CHECK_EQUAL(12, QuadratureLine::getOrderIndex(39) );
  BOOST_CHECK_EQUAL(12, QuadratureLine::getOrderIndex(-1) );
  BOOST_CHECK_THROW( QuadratureLine::getOrderIndex(40), DeveloperException );
}


//----------------------------------------------------------------------------//
// degree 1 polynomial
BOOST_AUTO_TEST_CASE( _1a )
{
  QuadratureLine quad(1);
  BOOST_CHECK_EQUAL( 1, quad.nQuadrature() );

  const Real tol = 1e-14;
  int nquad;
  Real f, fTrue;
  Real x, w, wsum;

  nquad = quad.nQuadrature();

  // f(x) = 1
  Fcn1D fcn0(0, 1);
  fTrue = 1;
  f = 0;
  wsum = 0;
  for (int n = 0; n < nquad; n++)
  {
    quad.coordinate(n, x);
    quad.weight(n, w);
    f += w*fcn0(x);
    wsum += w;
  }
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( 1, wsum, tol );

  // f(x) = x
  Fcn1D fcn1(1, 1);
  fTrue = 0.5;
  f = 0;
  wsum = 0;
  for (int n = 0; n < nquad; n++)
  {
    quad.coordinate(n, x);
    quad.weight(n, w);
    f += w*fcn1(x);
    wsum += w;
  }
  BOOST_CHECK_CLOSE( fTrue, f, tol );
  BOOST_CHECK_CLOSE( 1, wsum, tol );
}


//----------------------------------------------------------------------------//
// degree 1 polynomial
BOOST_AUTO_TEST_CASE( _1 )
{
  QuadratureLine quad(1);
  BOOST_REQUIRE_EQUAL( 1, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad;
  Real f, fTrue;
  Real x, w, wsum;

  nquad = quad.nQuadrature();

  for (int order = 0; order <= quad.order(); order++)
  {
//    printf( "%s: order = %d\n", __func__, order );
    Fcn1D fcn(order, 1);
//    fTrue = (1 + pow(-1, order))/static_cast<Real>(1 + order);    // x in [-1,1]
    fTrue = 1./static_cast<Real>(1 + order);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinate(n, x);
      quad.weight(n, w);
      f += w*fcn(x);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 3 polynomial
BOOST_AUTO_TEST_CASE( _3 )
{
  QuadratureLine quad(3);
  BOOST_REQUIRE_EQUAL( 2, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad;
  Real f, fTrue;
  Real x, w, wsum;

  nquad = quad.nQuadrature();

  for (int order = 0; order <= quad.order(); order++)
  {
//    printf( "%s: order = %d\n", __func__, order );
    Fcn1D fcn(order, 1);
    fTrue = 1./static_cast<Real>(1 + order);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinate(n, x);
      quad.weight(n, w);
      f += w*fcn(x);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 5 polynomial
BOOST_AUTO_TEST_CASE( _5 )
{
  QuadratureLine quad(5);
  BOOST_REQUIRE_EQUAL( 3, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad;
  Real f, fTrue;
  Real x, w, wsum;

  nquad = quad.nQuadrature();

  for (int order = 0; order <= quad.order(); order++)
  {
//    printf( "%s: order = %d\n", __func__, order );
    Fcn1D fcn(order, 1);
    fTrue = 1./static_cast<Real>(1 + order);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinate(n, x);
      quad.weight(n, w);
      f += w*fcn(x);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 7 polynomial
BOOST_AUTO_TEST_CASE( _7 )
{
  QuadratureLine quad(7);
  BOOST_REQUIRE_EQUAL( 4, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad;
  Real f, fTrue;
  Real x, w, wsum;

  nquad = quad.nQuadrature();

  for (int order = 0; order <= quad.order(); order++)
  {
//    printf( "%s: order = %d\n", __func__, order );
    Fcn1D fcn(order, 1);
    fTrue = 1./static_cast<Real>(1 + order);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinate(n, x);
      quad.weight(n, w);
      f += w*fcn(x);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}


//----------------------------------------------------------------------------//
// degree 9 polynomial
BOOST_AUTO_TEST_CASE( _9 )
{
  QuadratureLine quad(9);
  BOOST_REQUIRE_EQUAL( 5, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad;
  Real f, fTrue;
  Real x, w, wsum;

  nquad = quad.nQuadrature();

  for (int order = 0; order <= quad.order(); order++)
  {
//    printf( "%s: order = %d\n", __func__, order );
    Fcn1D fcn(order, 1);
    fTrue = 1./static_cast<Real>(1 + order);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinate(n, x);
      quad.weight(n, w);
      f += w*fcn(x);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 11 polynomial
BOOST_AUTO_TEST_CASE( _11 )
{
  QuadratureLine quad(11);
  BOOST_REQUIRE_EQUAL( 6, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad;
  Real f, fTrue;
  Real x, w, wsum;

  nquad = quad.nQuadrature();

  for (int order = 0; order <= quad.order(); order++)
  {
//    printf( "%s: order = %d\n", __func__, order );
    Fcn1D fcn(order, 1);
    fTrue = 1./static_cast<Real>(1 + order);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinate(n, x);
      quad.weight(n, w);
      f += w*fcn(x);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 13 polynomial
BOOST_AUTO_TEST_CASE( _13 )
{
  QuadratureLine quad(13);
  BOOST_REQUIRE_EQUAL( 7, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad;
  Real f, fTrue;
  Real x, w, wsum;

  nquad = quad.nQuadrature();

  for (int order = 0; order <= quad.order(); order++)
  {
//    printf( "%s: order = %d\n", __func__, order );
    Fcn1D fcn(order, 1);
    fTrue = 1./static_cast<Real>(1 + order);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinate(n, x);
      quad.weight(n, w);
      f += w*fcn(x);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 15 polynomial
BOOST_AUTO_TEST_CASE( _15 )
{
  QuadratureLine quad(15);
  BOOST_REQUIRE_EQUAL( 8, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad;
  Real f, fTrue;
  Real x, w, wsum;

  nquad = quad.nQuadrature();

  for (int order = 0; order <= quad.order(); order++)
  {
//    printf( "%s: order = %d\n", __func__, order );
    Fcn1D fcn(order, 1);
    fTrue = 1./static_cast<Real>(1 + order);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinate(n, x);
      quad.weight(n, w);
      f += w*fcn(x);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 17 polynomial
BOOST_AUTO_TEST_CASE( _17 )
{
  QuadratureLine quad(17);
  BOOST_REQUIRE_EQUAL( 9, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad;
  Real f, fTrue;
  Real x, w, wsum;

  nquad = quad.nQuadrature();

  for (int order = 0; order <= quad.order(); order++)
  {
//    printf( "%s: order = %d\n", __func__, order );
    Fcn1D fcn(order, 1);
    fTrue = 1./static_cast<Real>(1 + order);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinate(n, x);
      quad.weight(n, w);
      f += w*fcn(x);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 19 polynomial
BOOST_AUTO_TEST_CASE( _19 )
{
  QuadratureLine quad(19);
  BOOST_REQUIRE_EQUAL( 10, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad;
  Real f, fTrue;
  Real x, w, wsum;

  nquad = quad.nQuadrature();

  for (int order = 0; order <= quad.order(); order++)
  {
//    printf( "%s: order = %d\n", __func__, order );
    Fcn1D fcn(order, 1);
    fTrue = 1./static_cast<Real>(1 + order);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinate(n, x);
      quad.weight(n, w);
      f += w*fcn(x);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 21 polynomial
BOOST_AUTO_TEST_CASE( _21 )
{
  QuadratureLine quad(21);
  BOOST_REQUIRE_EQUAL( 11, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad;
  Real f, fTrue;
  Real x, w, wsum;

  nquad = quad.nQuadrature();

  for (int order = 0; order <= quad.order(); order++)
  {
//    printf( "%s: order = %d\n", __func__, order );
    Fcn1D fcn(order, 1);
    fTrue = 1./static_cast<Real>(1 + order);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinate(n, x);
      quad.weight(n, w);
      f += w*fcn(x);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 23 polynomial
BOOST_AUTO_TEST_CASE( _23 )
{
  QuadratureLine quad(23);
  BOOST_REQUIRE_EQUAL( 12, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad;
  Real f, fTrue;
  Real x, w, wsum;

  nquad = quad.nQuadrature();

  for (int order = 0; order <= quad.order(); order++)
  {
//    printf( "%s: order = %d\n", __func__, order );
    Fcn1D fcn(order, 1);
    fTrue = 1./static_cast<Real>(1 + order);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinate(n, x);
      quad.weight(n, w);
      f += w*fcn(x);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 39 polynomial
BOOST_AUTO_TEST_CASE( _39 )
{
  QuadratureLine quad(39);
  BOOST_REQUIRE_EQUAL( 20, quad.nQuadrature() );

  const Real tol = 1e-13;
  int nquad;
  Real f, fTrue;
  Real x, w, wsum;

  nquad = quad.nQuadrature();

  for (int order = 0; order < 10; order++)
  {
//    printf( "%s: order = %d\n", __func__, order );
    Fcn1D fcn(order, 1);
    fTrue = 1./static_cast<Real>(1 + order);
    f = 0;
    wsum = 0;
    for (int n = 0; n < nquad; n++)
    {
      quad.coordinate(n, x);
      quad.weight(n, w);
      f += w*fcn(x);
      wsum += w;
    }
    BOOST_CHECK_CLOSE( fTrue, f, tol );
    BOOST_CHECK_CLOSE( 1, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Quadrature/QuadratureLine_pattern.txt", true );

  QuadratureLine quad1(1);
  output << quad1 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad1.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureLine quad3(3);
  output << quad3 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad3.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureLine quad5(5);
  output << quad5 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad5.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureLine quad7(7);
  output << quad7 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad7.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureLine quad9(9);
  output << quad9 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad9.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureLine quad11(11);
  output << quad11 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad11.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureLine quad13(13);
  output << quad13 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad13.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureLine quad15(15);
  output << quad15 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad15.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureLine quad17(17);
  output << quad17 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad17.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureLine quad19(19);
  output << quad19 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad19.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureLine quad21(21);
  output << quad21 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad21.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureLine quad23(23);
  output << quad23 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad23.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureLine quad39(39);
  output << quad39 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad39.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
