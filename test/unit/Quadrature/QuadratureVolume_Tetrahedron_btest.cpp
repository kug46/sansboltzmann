// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// QuadratureVolume_Tetrahedron_btest
// testing of QuadratureVolume<Tet> class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "Quadrature/QuadratureVolume.h"
#include "Topology/ElementTopology.h"

using namespace SANS;

// local definitions
namespace
{

//----------------------------------------------------------------------------//
// monomial exponents:  x^i * y^j * z^k
struct MonomialExponents
{
  int i, j, k;
};


//----------------------------------------------------------------------------//
// exponents for all monomials of degree n or less
// Note: ordering not particularly elegant, but they're all included
void
getMonomialList( int n, std::vector<MonomialExponents>& list )
{
  MonomialExponents ijk;

  int cnt = 0;
  for (int i = 0; i <= n; i++)
  {
    for (int j = 0; j <= n - i; j++)
    {
      for (int k = 0; k <= n - i - j; k++)
      {
        ijk.i = i;
        ijk.j = j;
        ijk.k = k;

        list[cnt] = ijk;
        cnt++;
      }
    }
  }
}

//----------------------------------------------------------------------------//
long long
factorial( long long n )
{
  long long m;
  if (n <= 1)
    m = 1;
  else
    m = n * factorial(n - 1);

  return m;
}


//----------------------------------------------------------------------------//
// analytic integral for monomial
Real
getMonomialIntegral( MonomialExponents& ijk )
{
  int i = ijk.i;
  int j = ijk.j;
  int k = ijk.k;

  Real f = factorial(i)*factorial(j)*factorial(k)/static_cast<Real>(factorial(i + j + k + 3));

  //std::cout << "getMonomialIntegral: ijk = " << i << j << k << "  f = " << f << std::endl;

  return f;
}


//----------------------------------------------------------------------------//
// monomial class:  f(x) = a * x^i * y^j * z^k

class Monomial
{
public:
  explicit Monomial( MonomialExponents& ijk ) : i_(ijk.i), j_(ijk.j), k_(ijk.k) {}
  Monomial( int i, int j, int k ) : i_(i), j_(j), k_(k) {}
  ~Monomial() {}

  Real operator()( Real xyz[] ) const;
  Real operator()( Real x, Real y, Real z ) const;

private:
  int i_, j_, k_;
};

Real
Monomial::operator()( Real xyz[] ) const
{
  Real f;
  f = pow(xyz[0], i_) * pow(xyz[1], j_) * pow(xyz[2], k_);

  return f;
}

Real
Monomial::operator()( Real x, Real y, Real z ) const
{
  Real f;
  f = pow(x, i_) * pow(y, j_) * pow(z, k_);

  return f;
}

}  // local definitions


//############################################################################//
BOOST_AUTO_TEST_SUITE( QuadratureVolume_Tetrahedron_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orderReqd )
{
  QuadratureVolume<Tet> quad0(0);
  BOOST_CHECK_EQUAL( 1, quad0.order() );
  BOOST_CHECK_EQUAL( 1, quad0.nQuadrature() );

  QuadratureVolume<Tet> quad1(1);
  BOOST_CHECK_EQUAL( 1, quad1.order() );
  BOOST_CHECK_EQUAL( 1, quad1.nQuadrature() );

  QuadratureVolume<Tet> quad2(2);
  BOOST_CHECK_EQUAL( 2, quad2.order() );
  BOOST_CHECK_EQUAL( 4, quad2.nQuadrature() );

  QuadratureVolume<Tet> quad3(3);
  BOOST_CHECK_EQUAL( 3, quad3.order() );
  BOOST_CHECK_EQUAL( 8, quad3.nQuadrature() );

  QuadratureVolume<Tet> quad5(5);
  BOOST_CHECK_EQUAL(  5, quad5.order() );
  BOOST_CHECK_EQUAL( 14, quad5.nQuadrature() );

  QuadratureVolume<Tet> quad6(6);
  BOOST_CHECK_EQUAL(  6, quad6.order() );
  BOOST_CHECK_EQUAL( 24, quad6.nQuadrature() );

  QuadratureVolume<Tet> quad7(7);
  BOOST_CHECK_EQUAL(  7, quad7.order() );
  BOOST_CHECK_EQUAL( 35, quad7.nQuadrature() );

  QuadratureVolume<Tet> quad8(8);
  BOOST_CHECK_EQUAL(  8, quad8.order() );
  BOOST_CHECK_EQUAL( 46, quad8.nQuadrature() );

  QuadratureVolume<Tet> quad9(9);
  BOOST_CHECK_EQUAL(  9, quad9.order() );
  BOOST_CHECK_EQUAL( 61, quad9.nQuadrature() );

  QuadratureVolume<Tet> quad10(10);
  BOOST_CHECK_EQUAL( 10, quad10.order() );
  BOOST_CHECK_EQUAL( 81, quad10.nQuadrature() );

  QuadratureVolume<Tet> quad11(11);
  BOOST_CHECK_EQUAL(  11, quad11.order() );
  BOOST_CHECK_EQUAL( 109, quad11.nQuadrature() );

  QuadratureVolume<Tet> quad12(12);
  BOOST_CHECK_EQUAL(  12, quad12.order() );
  BOOST_CHECK_EQUAL( 140, quad12.nQuadrature() );

  QuadratureVolume<Tet> quad13(13);
  BOOST_CHECK_EQUAL(  13, quad13.order() );
  BOOST_CHECK_EQUAL( 171, quad13.nQuadrature() );

  QuadratureVolume<Tet> quad14(14);
  BOOST_CHECK_EQUAL(  14, quad14.order() );
  BOOST_CHECK_EQUAL( 236, quad14.nQuadrature() );

  QuadratureVolume<Tet> quadm1(-1);
  BOOST_CHECK_EQUAL(  14, quadm1.order() );
  BOOST_CHECK_EQUAL( 236, quadm1.nQuadrature() );


  BOOST_CHECK_THROW( QuadratureVolume<Tet> quad15(15), DeveloperException );


  BOOST_CHECK( QuadratureVolume<Tet>::nOrderIdx == 13);
  for (int orderidx = 0; orderidx < QuadratureVolume<Tet>::nOrderIdx; orderidx++)
  {
    // Check that the orderidx -> order -> orderidx
    BOOST_CHECK_EQUAL( orderidx, QuadratureVolume<Tet>::getOrderIndex( QuadratureVolume<Tet>::getOrderFromIndex(orderidx) ) );
  }

  BOOST_CHECK_EQUAL( 0, QuadratureVolume<Tet>::getOrderIndex(0) );
  BOOST_CHECK_EQUAL( 0, QuadratureVolume<Tet>::getOrderIndex(1) );
  BOOST_CHECK_EQUAL( 1, QuadratureVolume<Tet>::getOrderIndex(2) );
  BOOST_CHECK_EQUAL( 2, QuadratureVolume<Tet>::getOrderIndex(3) );
  BOOST_CHECK_EQUAL( 3, QuadratureVolume<Tet>::getOrderIndex(4) );
  BOOST_CHECK_EQUAL( 3, QuadratureVolume<Tet>::getOrderIndex(5) );
  BOOST_CHECK_EQUAL( 4, QuadratureVolume<Tet>::getOrderIndex(6) );
  BOOST_CHECK_EQUAL( 5, QuadratureVolume<Tet>::getOrderIndex(7) );
  BOOST_CHECK_EQUAL( 6, QuadratureVolume<Tet>::getOrderIndex(8) );
  BOOST_CHECK_EQUAL( 7, QuadratureVolume<Tet>::getOrderIndex(9) );
  BOOST_CHECK_EQUAL( 8, QuadratureVolume<Tet>::getOrderIndex(10) );
  BOOST_CHECK_EQUAL( 9, QuadratureVolume<Tet>::getOrderIndex(11) );
  BOOST_CHECK_EQUAL( 10, QuadratureVolume<Tet>::getOrderIndex(12) );
  BOOST_CHECK_EQUAL( 11, QuadratureVolume<Tet>::getOrderIndex(13) );
  BOOST_CHECK_EQUAL( 12, QuadratureVolume<Tet>::getOrderIndex(14) );
  BOOST_CHECK_EQUAL( 12, QuadratureVolume<Tet>::getOrderIndex(-1) );

  BOOST_CHECK_THROW( QuadratureVolume<Tet>::getOrderIndex(15), DeveloperException );
}


//----------------------------------------------------------------------------//
// degree 1 polynomial; 1 point
BOOST_AUTO_TEST_CASE( _1 )
{
  const int degree = 1;
  const int npts = 1;
  const Real tol = 1e-13;

  QuadratureVolume<Tet> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  Real f0, f1, fTrue;
  Real x, y, z, xyz[3], w, wsum;
  Real vol = 1./6.;

  int size = (degree + 1)*(degree + 2)*(degree + 3)/6;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijk;

  for (int m = 0; m < size; m++)
  {
    ijk = monomialList[m];

    fTrue = getMonomialIntegral( ijk );

    Monomial fcn( ijk );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyz);
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f0 += w*fcn(xyz);
      f1 += w*fcn(x, y, z);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 2 polynomial; 4 points
BOOST_AUTO_TEST_CASE( _2 )
{
  const int degree = 2;
  const int npts = 4;
  const Real tol = 1e-13;

  QuadratureVolume<Tet> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  Real f0, f1, fTrue;
  Real x, y, z, xyz[3], w, wsum;
  Real vol = 1./6.;

  int size = (degree + 1)*(degree + 2)*(degree + 3)/6;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijk;

  for (int m = 0; m < size; m++)
  {
    ijk = monomialList[m];

    fTrue = getMonomialIntegral( ijk );

    Monomial fcn( ijk );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyz);
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f0 += w*fcn(xyz);
      f1 += w*fcn(x, y, z);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 3 polynomial; 8 points
BOOST_AUTO_TEST_CASE( _3 )
{
  const int degree = 3;
  const int npts = 8;
  const Real tol = 1e-13;

  QuadratureVolume<Tet> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  Real f0, f1, fTrue;
  Real x, y, z, xyz[3], w, wsum;
  Real vol = 1./6.;

  int size = (degree + 1)*(degree + 2)*(degree + 3)/6;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijk;

  for (int m = 0; m < size; m++)
  {
    ijk = monomialList[m];

    fTrue = getMonomialIntegral( ijk );

    Monomial fcn( ijk );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyz);
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f0 += w*fcn(xyz);
      f1 += w*fcn(x, y, z);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 5 polynomial; 14 points
BOOST_AUTO_TEST_CASE( _5 )
{
  const int degree = 5;
  const int npts = 14;
  const Real tol = 1e-13;

  QuadratureVolume<Tet> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  Real f0, f1, fTrue;
  Real x, y, z, xyz[3], w, wsum;
  Real vol = 1./6.;

  int size = (degree + 1)*(degree + 2)*(degree + 3)/6;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijk;

  for (int m = 0; m < size; m++)
  {
    ijk = monomialList[m];

    fTrue = getMonomialIntegral( ijk );

    Monomial fcn( ijk );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyz);
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f0 += w*fcn(xyz);
      f1 += w*fcn(x, y, z);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 6 polynomial; 24 points
BOOST_AUTO_TEST_CASE( _6 )
{
  const int degree = 6;
  const int npts = 24;
  const Real tol = 1e-13;

  QuadratureVolume<Tet> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  Real f0, f1, fTrue;
  Real x, y, z, xyz[3], w, wsum;
  Real vol = 1./6.;

  int size = (degree + 1)*(degree + 2)*(degree + 3)/6;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijk;

  for (int m = 0; m < size; m++)
  {
    ijk = monomialList[m];

    fTrue = getMonomialIntegral( ijk );

    Monomial fcn( ijk );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyz);
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f0 += w*fcn(xyz);
      f1 += w*fcn(x, y, z);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 7 polynomial; 35 points
BOOST_AUTO_TEST_CASE( _7 )
{
  const int degree = 7;
  const int npts = 35;
  const Real tol = 1e-13;

  QuadratureVolume<Tet> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  Real f0, f1, fTrue;
  Real x, y, z, xyz[3], w, wsum;
  Real vol = 1./6.;

  int size = (degree + 1)*(degree + 2)*(degree + 3)/6;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijk;

  for (int m = 0; m < size; m++)
  {
    ijk = monomialList[m];

    fTrue = getMonomialIntegral( ijk );

    Monomial fcn( ijk );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyz);
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f0 += w*fcn(xyz);
      f1 += w*fcn(x, y, z);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 8 polynomial; 46 points
BOOST_AUTO_TEST_CASE( _8 )
{
  const int degree = 8;
  const int npts = 46;
  const Real tol = 5e-13;

  QuadratureVolume<Tet> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  Real f0, f1, fTrue;
  Real x, y, z, xyz[3], w, wsum;
  Real vol = 1./6.;

  int size = (degree + 1)*(degree + 2)*(degree + 3)/6;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijk;

  for (int m = 0; m < size; m++)
  {
    ijk = monomialList[m];

    fTrue = getMonomialIntegral( ijk );

    Monomial fcn( ijk );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyz);
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f0 += w*fcn(xyz);
      f1 += w*fcn(x, y, z);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 9 polynomial; 61 points
BOOST_AUTO_TEST_CASE( _9 )
{
  const int degree = 9;
  const int npts = 61;
  const Real tol = 5e-13;

  QuadratureVolume<Tet> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  Real f0, f1, fTrue;
  Real x, y, z, xyz[3], w, wsum;
  Real vol = 1./6.;

  int size = (degree + 1)*(degree + 2)*(degree + 3)/6;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijk;

  for (int m = 0; m < size; m++)
  {
    ijk = monomialList[m];

    fTrue = getMonomialIntegral( ijk );

    Monomial fcn( ijk );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyz);
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f0 += w*fcn(xyz);
      f1 += w*fcn(x, y, z);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 10 polynomial; 81 points
BOOST_AUTO_TEST_CASE( _10 )
{
  const int degree = 10;
  const int npts = 81;
  const Real tol = 5e-13;

  QuadratureVolume<Tet> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  Real f0, f1, fTrue;
  Real x, y, z, xyz[3], w, wsum;
  Real vol = 1./6.;

  int size = (degree + 1)*(degree + 2)*(degree + 3)/6;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijk;

  for (int m = 0; m < size; m++)
  {
    ijk = monomialList[m];

    fTrue = getMonomialIntegral( ijk );

    Monomial fcn( ijk );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyz);
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f0 += w*fcn(xyz);
      f1 += w*fcn(x, y, z);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 11 polynomial; 109 points
BOOST_AUTO_TEST_CASE( _11 )
{
  const int degree = 11;
  const int npts = 109;
  const Real tol = 5e-13;

  QuadratureVolume<Tet> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  Real f0, f1, fTrue;
  Real x, y, z, xyz[3], w, wsum;
  Real vol = 1./6.;

  int size = (degree + 1)*(degree + 2)*(degree + 3)/6;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijk;

  for (int m = 0; m < size; m++)
  {
    ijk = monomialList[m];

    fTrue = getMonomialIntegral( ijk );

    Monomial fcn( ijk );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyz);
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f0 += w*fcn(xyz);
      f1 += w*fcn(x, y, z);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 12 polynomial; 140 points
BOOST_AUTO_TEST_CASE( _12 )
{
  const int degree = 12;
  const int npts = 140;
  const Real tol = 5e-13;

  QuadratureVolume<Tet> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  Real f0, f1, fTrue;
  Real x, y, z, xyz[3], w, wsum;
  Real vol = 1./6.;

  int size = (degree + 1)*(degree + 2)*(degree + 3)/6;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijk;

  for (int m = 0; m < size; m++)
  {
    ijk = monomialList[m];

    fTrue = getMonomialIntegral( ijk );

    Monomial fcn( ijk );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyz);
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f0 += w*fcn(xyz);
      f1 += w*fcn(x, y, z);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 13 polynomial; 171 points
BOOST_AUTO_TEST_CASE( _13 )
{
  const int degree = 13;
  const int npts = 171;
  const Real tol = 5e-13;

  QuadratureVolume<Tet> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  Real f0, f1, fTrue;
  Real x, y, z, xyz[3], w, wsum;
  Real vol = 1./6.;

  int size = (degree + 1)*(degree + 2)*(degree + 3)/6;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijk;

  for (int m = 0; m < size; m++)
  {
    ijk = monomialList[m];

    fTrue = getMonomialIntegral( ijk );

    Monomial fcn( ijk );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyz);
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f0 += w*fcn(xyz);
      f1 += w*fcn(x, y, z);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
// degree 14 polynomial; 236 points
BOOST_AUTO_TEST_CASE( _14 )
{
  const int degree = 14;
  const int npts = 236;
  const Real tol = 5e-13;

  QuadratureVolume<Tet> quad( degree );
  BOOST_CHECK_EQUAL( npts, quad.nQuadrature() );

  Real f0, f1, fTrue;
  Real x, y, z, xyz[3], w, wsum;
  Real vol = 1./6.;

  int size = (degree + 1)*(degree + 2)*(degree + 3)/6;
  std::vector<MonomialExponents> monomialList( size );

  getMonomialList( degree, monomialList );
  size = monomialList.size();

  MonomialExponents ijk;

  for (int m = 0; m < size; m++)
  {
    ijk = monomialList[m];

    fTrue = getMonomialIntegral( ijk );

    Monomial fcn( ijk );
    f0 = 0;
    f1 = 0;
    wsum = 0;
    for (int n = 0; n < quad.nQuadrature(); n++)
    {
      quad.coordinates(n, xyz);
      quad.coordinates(n, x, y, z);
      quad.weight(n, w);
      f0 += w*fcn(xyz);
      f1 += w*fcn(x, y, z);
      wsum += w;
    }
    f0 *= vol;
    f1 *= vol;
    BOOST_CHECK_CLOSE( fTrue, f0, tol );
    BOOST_CHECK_CLOSE( fTrue, f1, tol );
    BOOST_CHECK_CLOSE( 1.0, wsum, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Quadrature/QuadratureVolume_Tetrahedron_pattern.txt", true );

  QuadratureVolume<Tet> quadm1(-1);
  output << quadm1 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quadm1.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Tet> quad0(0);
  output << quad0 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad0.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Tet> quad1(1);
  output << quad1 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad1.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Tet> quad2(2);
  output << quad2 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad2.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Tet> quad3(3);
  output << quad3 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad3.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Tet> quad5(5);
  output << quad5 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad5.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Tet> quad6(6);
  output << quad6 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad6.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Tet> quad7(7);
  output << quad7 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad7.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Tet> quad8(8);
  output << quad8 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad8.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Tet> quad9(9);
  output << quad9 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad9.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Tet> quad10(10);
  output << quad10 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad10.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Tet> quad11(11);
  output << quad11 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad11.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Tet> quad12(12);
  output << quad12 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad12.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Tet> quad13(13);
  output << quad13 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad13.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

  QuadratureVolume<Tet> quad14(14);
  output << quad14 << std::endl;
  BOOST_CHECK( output.match_pattern() );
  quad14.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
