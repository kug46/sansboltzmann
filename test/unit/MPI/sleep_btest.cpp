// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"
#include "MPI/MPI_sleep.h"

#include <iostream>
#include <chrono>
#include <thread>

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( MPI_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( sleep_test )
{
  boost::mpi::communicator world;

  // have rank zero sleep for a bit to test the sleep function
  if (world.rank() == 0)
  {
    std::this_thread::sleep_for( std::chrono::milliseconds(2000) );
  }

  MPI_sleep(world, 0, 1000);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
