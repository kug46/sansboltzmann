INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

# MPI is optional
IF( USE_MPI )

RUN_WITH_MPIEXEC( ${MPIEXEC_MAX_NUMPROCS} )

GenerateUnitTests( MPILib )

ENDIF()