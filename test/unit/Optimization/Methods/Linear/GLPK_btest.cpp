// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "SANS_CHECK_CLOSE_btest.h"
#include "tools/SANSnumerics.h"

#include "Optimization/Methods/Linear/LOptSolver.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"

//This is only needed for the UMFPACK_ERROR_* exception testing
#include <glpk.h>

using namespace SANS;
using namespace SANS::SLA;
using namespace SANS::LOPT;


//############################################################################//
BOOST_AUTO_TEST_SUITE( LinearOpt )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( GLPK_ctor )
{
  PyDict d, Simplex_dict;

  // This is necessary because default for solutionmethod is not working currently
  Simplex_dict[GLPKParam::params.SolutionMethod.Name]  = GLPKParam::params.SolutionMethod.Simplex;
  Simplex_dict[GLPKParam::SimplexParam::params.msg_lev] = 0;

  d[GLPKParam::params.SolutionMethod] = Simplex_dict;

  GLPKParam::checkInputs(d);

  //This returns a shared pointer so no memory is lost
  GLPKParam::newSolver< SparseMatrix_CRS<Real>, SparseVector<Real> >(d);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( GLPK_Exception )
{
  //So there isn't really any checking going on here. But simply instantiating the exception class
  //helps get the coverage numbers up. This is much simpler than actually trying to get UMFPACK
  //to generate all the possible errors.

  /* cppcheck-suppress unusedScopedObject */ GLPKException(GLP_EBADB);
  /* cppcheck-suppress unusedScopedObject */ GLPKException(GLP_ESING);
  /* cppcheck-suppress unusedScopedObject */ GLPKException(GLP_EBOUND);
  /* cppcheck-suppress unusedScopedObject */ GLPKException(GLP_EFAIL);
  /* cppcheck-suppress unusedScopedObject */ GLPKException(GLP_EITLIM);
  /* cppcheck-suppress unusedScopedObject */ GLPKException(GLP_ETMLIM);
  /* cppcheck-suppress unusedScopedObject */ GLPKException(GLP_ENOCVG);
  /* cppcheck-suppress unusedScopedObject */ GLPKException(GLP_EINSTAB);

  PyDict GLPK_dict, Simplex_dict;

  typedef SparseMatrix_CRS<Real> Matrix_type;

  Simplex_dict[GLPKParam::params.SolutionMethod.Name]  = GLPKParam::params.SolutionMethod.Simplex;
  Simplex_dict[GLPKParam::SimplexParam::params.msg_lev] = 0;

  GLPK_dict[GLPKParam::params.SolutionMethod] = Simplex_dict;

  GLPKParam::checkInputs(GLPK_dict);

  typedef SparseMatrix_CRS<Real> Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  GLPK<Matrix_type, Vector_type> Solver( GLPK_dict );

  const int nRow = 3;

  SparseNonZeroPattern<Real> nz(nRow,nRow);
  nz.add(0,0);
  nz.add(0,1);
  nz.add(0,2);
  nz.add(1,0);
  nz.add(1,1);
  nz.add(1,2);
  nz.add(2,0);
  nz.add(2,1);
  nz.add(2,2);

  Matrix_type A( nz   );
  Vector_type b( nRow - 1 ), c( nRow), x( nRow );
  // double z;

  BOOST_REQUIRE( A.get_row_ptr() != NULL ); // suppress clang analyzer warning

  A.sparseRow(0,0) =  1.0;
  A.sparseRow(0,1) =  1.0;
  A.sparseRow(0,2) =  1.0;
  A.sparseRow(1,0) = 10.0;
  A.sparseRow(1,1) =  4.0;
  A.sparseRow(1,2) =  5.0;
  A.sparseRow(2,0) =  2.0;
  A.sparseRow(2,1) =  2.0;
  A.sparseRow(2,2) =  6.0;

  c[0] = 1.0;

  BOOST_CHECK_THROW( Solver.setConstraints( A, b, false ), AssertionException );
  // Solver.setConstraints( A, b );
  // Solver.setObjectiveVector( c );

  // BOOST_CHECK_THROW( Solver.solve( x, z ), AssertionException );
}

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( GLPK_Solve_small )
{

  PyDict GLPK_dict, Simplex_dict;

  typedef SparseMatrix_CRS<Real> Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  Simplex_dict[GLPKParam::params.SolutionMethod.Name]  = GLPKParam::params.SolutionMethod.Simplex;
  Simplex_dict[GLPKParam::SimplexParam::params.msg_lev] = 0;

  GLPK_dict[GLPKParam::params.SolutionMethod] = Simplex_dict;

  GLPKParam::checkInputs( GLPK_dict );

  GLPK<Matrix_type, Vector_type> Solver( GLPK_dict );

  const int nRow = 3;

  SparseNonZeroPattern<Real> nz(nRow,nRow);
  nz.add(0,0);
  nz.add(0,1);
  nz.add(0,2);
  nz.add(1,0);
  nz.add(1,1);
  nz.add(1,2);
  nz.add(2,0);
  nz.add(2,1);
  nz.add(2,2);

  Matrix_type A( nz );

  BOOST_REQUIRE( A.get_row_ptr() != NULL ); // suppress clang analyzer warning

  A.sparseRow(0,0) =  1.0;
  A.sparseRow(0,1) =  1.0;
  A.sparseRow(0,2) =  1.0;
  A.sparseRow(1,0) = 10.0;
  A.sparseRow(1,1) =  4.0;
  A.sparseRow(1,2) =  5.0;
  A.sparseRow(2,0) =  2.0;
  A.sparseRow(2,1) =  2.0;
  A.sparseRow(2,2) =  6.0;

  Vector_type x(nRow), b(nRow), x2(nRow), c(nRow);
  double z;

  b[0] = 100.0;
  b[1] = 600.0;
  b[2] = 300.0;

  c[0] = -10.0;
  c[1] =  -6.0;
  c[2] =  -4.0;

  // correct solution
  x2[0] = 100./3.;
  x2[1] = 2.*100./3.;
  x2[2] = 0.0;

  //Solve the linear program
  Solver.setConstraints(A, b, false);
  Solver.solve( c, x, z );

  //The solution should be equal to x2!
  for (int i = 0; i < b.m(); i++)
    BOOST_CHECK_CLOSE( x[i], x2[i], 1e-12 );

  // And the objective value should be correct
  BOOST_CHECK_CLOSE( z, -(700. + 100./3.), 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( GLPK_Solve_small_CRS_matrix )
{

  PyDict GLPK_dict, Simplex_dict;

  typedef SparseMatrix_CRS< DLA::MatrixS<2, 1, Real> > Matrix_type;

  typedef SparseVector< Real > Vector_DV_type;
  typedef SparseVector< DLA::VectorS<2,Real> > Vector_C_type;


  Simplex_dict[GLPKParam::params.SolutionMethod.Name]  = GLPKParam::params.SolutionMethod.Simplex;
  Simplex_dict[GLPKParam::SimplexParam::params.msg_lev] = 0;

  GLPK_dict[GLPKParam::params.SolutionMethod] = Simplex_dict;

  GLPKParam::checkInputs( GLPK_dict );

  GLPK<Matrix_type, Vector_C_type> Solver( GLPK_dict );

  const int nRow = 2;
  const int nVar = 3;

  SparseNonZeroPattern< DLA::MatrixS<2, 1, Real> > nz(nRow,nVar);
  nz.add(0,0);
  nz.add(0,1);
  nz.add(0,2);
  nz.add(1,0);
  nz.add(1,1);
  nz.add(1,2);
  // nz.add(2,0);
  // nz.add(2,1);
  // nz.add(2,2);

  Matrix_type A( nz );

  BOOST_REQUIRE( A.get_row_ptr() != NULL ); // suppress clang analyzer warning

  A.sparseRow(0,0)(0,0) =  1.0;
  A.sparseRow(0,1)(0,0) =  1.0;
  A.sparseRow(0,2)(0,0) =  1.0;
  A.sparseRow(0,0)(1,0) = 10.0;
  A.sparseRow(0,1)(1,0) =  4.0;
  A.sparseRow(0,2)(1,0) =  5.0;
  A.sparseRow(1,0)(0,0) =  2.0;
  A.sparseRow(1,1)(0,0) =  2.0;
  A.sparseRow(1,2)(0,0) =  6.0;
  A.sparseRow(1,0)(1,0) = -1.0;
  A.sparseRow(1,1)(1,0) = -1.0;
  A.sparseRow(1,2)(1,0) =  0.0;

  Vector_DV_type x(nVar), x2(nVar), c(nVar);
  Vector_C_type b(nRow);
  double z;

  b[0][0] = 100.0;
  b[0][1] = 600.0;
  b[1][0] = 300.0;
  b[1][1] =  50.0;

  c[0] = -10.0;
  c[1] =  -6.0;
  c[2] =  -4.0;

  // correct solution
  x2[0] = 100./3.;
  x2[1] = 2.*100./3.;
  x2[2] = 0.0;

  //Solve the linear program
  Solver.setConstraints(A, b, false);
  Solver.solve( c, x, z );

  //The solution should be equal to x2!
  for (int i = 0; i < b.m(); i++)
    BOOST_CHECK_CLOSE( x[i], x2[i], 1e-12 );

  // And the objective value should be correct
  BOOST_CHECK_CLOSE( z, -(700. + 100./3.), 1e-12 );
}

// typedef boost::mpl::list< Heat1D< DLA::MatrixD<Real> >, Advection1D< DLA::MatrixD<Real> > > MatrixInit_Block_types;


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
