// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"

#include "Optimization/Methods/Linear/LOptSolver.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"

using namespace SANS;
using namespace SANS::LOPT;
using namespace SANS::SLA;


//############################################################################//
BOOST_AUTO_TEST_SUITE( LinearOpt )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LOptSolver_GLPK_Simplex )
{
  PyDict LOptSolver_dict, GLPK, Simplex_dict, SolutionMethod;

  Simplex_dict[GLPKParam::params.SolutionMethod.Name]  = GLPKParam::params.SolutionMethod.Simplex;
  Simplex_dict[GLPKParam::SimplexParam::params.msg_lev] = 0;

  GLPK[LOptSolverParam::params.LOptSolver.Solver] = LOptSolverParam::params.LOptSolver.GLPK;
  GLPK[GLPKParam::params.SolutionMethod] = Simplex_dict;

  LOptSolver_dict[LOptSolverParam::params.LOptSolver] = GLPK;

  LOptSolverParam::checkInputs(LOptSolver_dict);

  typedef SparseMatrix_CRS<Real> Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  LOptSolver< Matrix_type, Vector_type > Solver(LOptSolver_dict);

  // This is the example from the GLPK documentation

  const int nRow = 3;

  SparseNonZeroPattern<Real> nz(nRow,nRow);
  nz.add(0,0);
  nz.add(0,1);
  nz.add(0,2);
  nz.add(1,0);
  nz.add(1,1);
  nz.add(1,2);
  nz.add(2,0);
  nz.add(2,1);
  nz.add(2,2);

  Matrix_type A( nz );

  A.sparseRow(0,0) =  1.0;
  A.sparseRow(0,1) =  1.0;
  A.sparseRow(0,2) =  1.0;
  A.sparseRow(1,0) = 10.0;
  A.sparseRow(1,1) =  4.0;
  A.sparseRow(1,2) =  5.0;
  A.sparseRow(2,0) =  2.0;
  A.sparseRow(2,1) =  2.0;
  A.sparseRow(2,2) =  6.0;

  Vector_type x(nRow), b(nRow), x2(nRow), c(nRow);

  b[0] = 100.0;
  b[1] = 600.0;
  b[2] = 300.0;

  c[0] = -10.0;
  c[1] =  -6.0;
  c[2] =  -4.0;

  // correct solution
  x2[0] = 100./3.;
  x2[1] = 2.*100./3.;
  x2[2] = 0.0;

  double z;

  //Solve the linear program
  Solver.setConstraints(A, b, false);
  Solver.solve( c, x, z );

  //The solution should be equal to x2!
  for (int i = 0; i < b.m(); i++)
    BOOST_CHECK_CLOSE( x[i], x2[i], 1e-12 );

  // And the objective value should be correct
  BOOST_CHECK_CLOSE( z, -(700. + 100./3.), 1e-12 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LOptSolver_GLPK_InteriorPoint )
{
  PyDict LOptSolver_dict, GLPK, IP_dict, SolutionMethod;

  IP_dict[GLPKParam::params.SolutionMethod.Name]  = GLPKParam::params.SolutionMethod.InteriorPoint;
  IP_dict[GLPKParam::InteriorPointParam::params.msg_lev] = 1;

  GLPK[LOptSolverParam::params.LOptSolver.Solver] = LOptSolverParam::params.LOptSolver.GLPK;
  GLPK[GLPKParam::params.SolutionMethod] = IP_dict;

  LOptSolver_dict[LOptSolverParam::params.LOptSolver] = GLPK;

  LOptSolverParam::checkInputs(LOptSolver_dict);

  typedef SparseMatrix_CRS<Real> Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  LOptSolver< Matrix_type, Vector_type > Solver(LOptSolver_dict);

  // This is the example from the GLPK documentation

  const int nRow = 3;

  SparseNonZeroPattern<Real> nz(nRow,nRow);
  nz.add(0,0);
  nz.add(0,1);
  nz.add(0,2);
  nz.add(1,0);
  nz.add(1,1);
  nz.add(1,2);
  nz.add(2,0);
  nz.add(2,1);
  nz.add(2,2);

  Matrix_type A( nz );

  A.sparseRow(0,0) =  1.0;
  A.sparseRow(0,1) =  1.0;
  A.sparseRow(0,2) =  1.0;
  A.sparseRow(1,0) = 10.0;
  A.sparseRow(1,1) =  4.0;
  A.sparseRow(1,2) =  5.0;
  A.sparseRow(2,0) =  2.0;
  A.sparseRow(2,1) =  2.0;
  A.sparseRow(2,2) =  6.0;

  Vector_type x(nRow), b(nRow), x2(nRow), c(nRow);

  b[0] = 100.0;
  b[1] = 600.0;
  b[2] = 300.0;

  c[0] = -10.0;
  c[1] =  -6.0;
  c[2] =  -4.0;

  // correct solution
  x2[0] = 100./3.;
  x2[1] = 2.*100./3.;
  x2[2] = 0.0;

  double z;

  //Solve the linear program
  Solver.setConstraints(A, b, false);
  Solver.solve( c, x, z );

  //The solution should be equal to x2!
  for (int i = 0; i < b.m(); i++)
    if (x2[i] != 0.0) BOOST_CHECK_CLOSE( x[i], x2[i], 1e-6 ); // INTERIOR POINT METHOD LESS ACCURATE
    // The 0.0 is a problem for relative checks

  // And the objective value should be correct
  BOOST_CHECK_CLOSE( z, -(700. + 100./3.), 1e-6 ); // INTERIOR POINT METHOD LESS ACCURATE

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LOptSolver_GLPK_InteriorPoint_MatrixS )
{
  PyDict LOptSolver_dict, GLPK, IP_dict, SolutionMethod;

  IP_dict[GLPKParam::params.SolutionMethod.Name]  = GLPKParam::params.SolutionMethod.InteriorPoint;
  IP_dict[GLPKParam::InteriorPointParam::params.msg_lev] = 1;

  GLPK[LOptSolverParam::params.LOptSolver.Solver] = LOptSolverParam::params.LOptSolver.GLPK;
  GLPK[GLPKParam::params.SolutionMethod] = IP_dict;

  LOptSolver_dict[LOptSolverParam::params.LOptSolver] = GLPK;

  LOptSolverParam::checkInputs(LOptSolver_dict);

  typedef SparseMatrix_CRS< DLA::MatrixS<2, 1, Real> > Matrix_type;
  typedef SparseVector< DLA::VectorS<2,Real> > Vector_C_type;
  typedef SparseVector< Real > Vector_DV_type;

  LOptSolver< Matrix_type, Vector_C_type > Solver(LOptSolver_dict);

  // This is the example from the GLPK documentation

  const int nRow = 2;
  const int nVar = 3;

  SparseNonZeroPattern< DLA::MatrixS<2, 1, Real> > nz(nRow,nVar);
  nz.add(0,0);
  nz.add(0,1);
  nz.add(0,2);
  nz.add(1,0);
  nz.add(1,1);
  nz.add(1,2);

  Matrix_type A( nz );

  BOOST_REQUIRE( A.get_row_ptr() != NULL ); // suppress clang analyzer warning

  A.sparseRow(0,0)(0,0) =  1.0;
  A.sparseRow(0,1)(0,0) =  1.0;
  A.sparseRow(0,2)(0,0) =  1.0;
  A.sparseRow(0,0)(1,0) = 10.0;
  A.sparseRow(0,1)(1,0) =  4.0;
  A.sparseRow(0,2)(1,0) =  5.0;
  A.sparseRow(1,0)(0,0) =  2.0;
  A.sparseRow(1,1)(0,0) =  2.0;
  A.sparseRow(1,2)(0,0) =  6.0;
  A.sparseRow(1,0)(1,0) = -1.0;
  A.sparseRow(1,1)(1,0) = -1.0;
  A.sparseRow(1,2)(1,0) =  0.0;

  Vector_DV_type x(nVar), x2(nVar), c(nVar);
  Vector_C_type b(nRow);

  b[0][0] = 100.0;
  b[0][1] = 600.0;
  b[1][0] = 300.0;
  b[1][1] =  50.0;

  c[0] = -10.0;
  c[1] =  -6.0;
  c[2] =  -4.0;

  // correct solution
  x2[0] = 100./3.;
  x2[1] = 2.*100./3.;
  x2[2] = 0.0;

  double z;

  //Solve the linear program
  Solver.setConstraints(A, b, false);
  Solver.solve( c, x, z );

  //The solution should be equal to x2!
  for (int i = 0; i < b.m(); i++)
    SANS_CHECK_CLOSE( x[i], x2[i], 1e-6, 1e-6 ); // INTERIOR POINT METHOD LESS ACCURATE
    // The 0.0 is a problem for relative checks

  // And the objective value should be correct
  BOOST_CHECK_CLOSE( z, -(700. + 100./3.), 1e-6 ); // INTERIOR POINT METHOD LESS ACCURATE

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LOptSolver_GLPK_InteriorPoint_MatrixD_CRS_MatrixS )
{
  PyDict LOptSolver_dict, GLPK, IP_dict, SolutionMethod;

  IP_dict[GLPKParam::params.SolutionMethod.Name]  = GLPKParam::params.SolutionMethod.InteriorPoint;
  IP_dict[GLPKParam::InteriorPointParam::params.msg_lev] = 1;

  GLPK[LOptSolverParam::params.LOptSolver.Solver] = LOptSolverParam::params.LOptSolver.GLPK;
  GLPK[GLPKParam::params.SolutionMethod] = IP_dict;

  LOptSolver_dict[LOptSolverParam::params.LOptSolver] = GLPK;

  LOptSolverParam::checkInputs(LOptSolver_dict);

  typedef DLA::MatrixD< SparseMatrix_CRS< DLA::MatrixS<2, 1, Real> > > Matrix_type;
  typedef DLA::VectorD< SparseVector< DLA::VectorS<2,Real> > > Vector_C_type;
  typedef DLA::VectorD< SparseVector< Real > > Vector_DV_type;

  LOptSolver< Matrix_type, Vector_C_type > Solver(LOptSolver_dict);

  // This is the example from the GLPK documentation

  const int nRow = 2;
  const int nVar0 = 2;
  const int nVar1 = 1;

  DLA::MatrixD< SparseNonZeroPattern< DLA::MatrixS<2, 1, Real> > > nz = {{ {nRow,nVar0} }, { {nRow,nVar1} } };
  nz(0,0).add(0,0);
  nz(0,0).add(0,1);
  nz(0,0).add(1,0);
  nz(0,0).add(1,1);

  nz(1,0).add(0,0);
  nz(1,0).add(1,0);

  Matrix_type A( nz );

  BOOST_REQUIRE( A(0,0).get_row_ptr() != NULL ); // suppress clang analyzer warning
  BOOST_REQUIRE( A(1,0).get_row_ptr() != NULL ); // suppress clang analyzer warning

  A(0,0).sparseRow(0,0)(0,0) =  1.0;
  A(0,0).sparseRow(0,1)(0,0) =  1.0;
  A(1,0).sparseRow(0,0)(0,0) =  1.0;
  A(0,0).sparseRow(0,0)(1,0) = 10.0;
  A(0,0).sparseRow(0,1)(1,0) =  4.0;
  A(1,0).sparseRow(0,0)(1,0) =  5.0;
  A(0,0).sparseRow(1,0)(0,0) =  2.0;
  A(0,0).sparseRow(1,1)(0,0) =  2.0;
  A(1,0).sparseRow(1,0)(0,0) =  6.0;
  A(0,0).sparseRow(1,0)(1,0) = -1.0;
  A(0,0).sparseRow(1,1)(1,0) = -1.0;
  A(1,0).sparseRow(1,0)(1,0) =  0.0;

  Vector_DV_type x  = { {nVar0}, {nVar1}};
  Vector_DV_type x2 = { {nVar0}, {nVar1}};
  Vector_DV_type c  = { {nVar0}, {nVar1}};

  Vector_C_type b = { {nRow} };

  b[0][0][0] = 100.0;
  b[0][0][1] = 600.0;
  b[0][1][0] = 300.0;
  b[0][1][1] =  50.0;

  c[0][0] = -10.0;
  c[0][1] =  -6.0;
  c[1][0] =  -4.0;

  // correct solution
  x2[0][0] = 100./3.;
  x2[0][1] = 2.*100./3.;
  x2[1][0] = 0.0;

  double z;

  //Solve the linear program
  Solver.setConstraints(A, b, false);
  Solver.solve( c, x, z );

  // x[1][0] = x[1][0] + 1e9;
  // x[1][0] = x[1][0] - 1e9;

  //The solution should be equal to x2!
  SANS_CHECK_CLOSE( x[0][0], x2[0][0], 1e-6, 1e-6 ); // INTERIOR POINT METHOD LESS ACCURATE
  SANS_CHECK_CLOSE( x[0][1], x2[0][1], 1e-6, 1e-6 ); // INTERIOR POINT METHOD LESS ACCURATE
  SANS_CHECK_CLOSE( x[1][0], x2[1][0], 1e-6, 1e-6 ); // INTERIOR POINT METHOD LESS ACCURATE
  // The 0.0 is a problem for relative checks

  // And the objective value should be correct
  SANS_CHECK_CLOSE( z, -(700. + 100./3.), 1e-6, 1e-6 ); // INTERIOR POINT METHOD LESS ACCURATE

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LOptSolver_GLPK_Simplex_MatrixD_CRS_MatrixS )
{
  PyDict LOptSolver_dict, GLPK, Simplex_dict, SolutionMethod;

  Simplex_dict[GLPKParam::params.SolutionMethod.Name]  = GLPKParam::params.SolutionMethod.Simplex;
  Simplex_dict[GLPKParam::SimplexParam::params.msg_lev] = 0;

  GLPK[LOptSolverParam::params.LOptSolver.Solver] = LOptSolverParam::params.LOptSolver.GLPK;
  GLPK[GLPKParam::params.SolutionMethod] = Simplex_dict;

  LOptSolver_dict[LOptSolverParam::params.LOptSolver] = GLPK;

  LOptSolverParam::checkInputs(LOptSolver_dict);

  typedef DLA::MatrixD< SparseMatrix_CRS< DLA::MatrixS<2, 1, Real> > > Matrix_type;
  typedef DLA::VectorD< SparseVector< DLA::VectorS<2,Real> > > Vector_C_type;
  typedef DLA::VectorD< SparseVector< Real > > Vector_DV_type;

  LOptSolver< Matrix_type, Vector_C_type > Solver(LOptSolver_dict);

  // This is the example from the GLPK documentation

  const int nRow = 2;
  const int nVar0 = 2;
  const int nVar1 = 1;

  DLA::MatrixD< SparseNonZeroPattern< DLA::MatrixS<2, 1, Real> > > nz = {{ {nRow,nVar0} }, { {nRow,nVar1} } };
  nz(0,0).add(0,0);
  nz(0,0).add(0,1);
  nz(0,0).add(1,0);
  nz(0,0).add(1,1);

  nz(1,0).add(0,0);
  nz(1,0).add(1,0);

  Matrix_type A( nz );

  BOOST_REQUIRE( A(0,0).get_row_ptr() != NULL ); // suppress clang analyzer warning
  BOOST_REQUIRE( A(1,0).get_row_ptr() != NULL ); // suppress clang analyzer warning

  A(0,0).sparseRow(0,0)(0,0) =  1.0;
  A(0,0).sparseRow(0,1)(0,0) =  1.0;
  A(1,0).sparseRow(0,0)(0,0) =  1.0;
  A(0,0).sparseRow(0,0)(1,0) = 10.0;
  A(0,0).sparseRow(0,1)(1,0) =  4.0;
  A(1,0).sparseRow(0,0)(1,0) =  5.0;
  A(0,0).sparseRow(1,0)(0,0) =  2.0;
  A(0,0).sparseRow(1,1)(0,0) =  2.0;
  A(1,0).sparseRow(1,0)(0,0) =  6.0;
  A(0,0).sparseRow(1,0)(1,0) = -1.0;
  A(0,0).sparseRow(1,1)(1,0) = -1.0;
  A(1,0).sparseRow(1,0)(1,0) =  0.0;

  Vector_DV_type x  = { {nVar0}, {nVar1}};
  Vector_DV_type x2 = { {nVar0}, {nVar1}};
  Vector_DV_type c  = { {nVar0}, {nVar1}};

  Vector_C_type b = { {nRow} };

  b[0][0][0] = 100.0;
  b[0][0][1] = 600.0;
  b[0][1][0] = 300.0;
  b[0][1][1] =  50.0;

  c[0][0] = -10.0;
  c[0][1] =  -6.0;
  c[1][0] =  -4.0;

  // correct solution
  x2[0][0] = 100./3.;
  x2[0][1] = 2.*100./3.;
  x2[1][0] = 0.0;

  double z;

  //Solve the linear program
  Solver.setConstraints(A, b, false);
  Solver.solve( c, x, z );

  // x[1][0] = x[1][0] + 1e9;
  // x[1][0] = x[1][0] - 1e9;

  //The solution should be equal to x2!
  SANS_CHECK_CLOSE( x[0][0], x2[0][0], 1e-12, 1e-12 );
  SANS_CHECK_CLOSE( x[0][1], x2[0][1], 1e-12, 1e-12 );
  SANS_CHECK_CLOSE( x[1][0], x2[1][0], 1e-12, 1e-12 );
  // The 0.0 is a problem for relative checks

  // And the objective value should be correct
  SANS_CHECK_CLOSE( z, -(700. + 100./3.), 1e-12, 1e-12 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LOptSolver_GLPK_Simplex_Equality )
{
  PyDict LOptSolver_dict, GLPK, Simplex_dict, SolutionMethod;

  Simplex_dict[GLPKParam::params.SolutionMethod.Name]  = GLPKParam::params.SolutionMethod.Simplex;
  Simplex_dict[GLPKParam::SimplexParam::params.msg_lev] = 0;

  GLPK[LOptSolverParam::params.LOptSolver.Solver] = LOptSolverParam::params.LOptSolver.GLPK;
  GLPK[GLPKParam::params.SolutionMethod] = Simplex_dict;

  LOptSolver_dict[LOptSolverParam::params.LOptSolver] = GLPK;

  LOptSolverParam::checkInputs(LOptSolver_dict);

  typedef SparseMatrix_CRS<Real> Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  LOptSolver< Matrix_type, Vector_type > Solver(LOptSolver_dict);

  // This is a modified example from the GLPK documentation

  const int nRow = 3;

  // Inequality constraint matrix
  SparseNonZeroPattern<Real> nz(nRow,nRow);
  nz.add(0,0);
  nz.add(0,1);
  nz.add(0,2);
  nz.add(1,0);
  nz.add(1,1);
  nz.add(1,2);
  nz.add(2,0);
  nz.add(2,1);
  nz.add(2,2);

  Matrix_type A( nz );

  A.sparseRow(0,0) =  1.0;
  A.sparseRow(0,1) =  1.0;
  A.sparseRow(0,2) =  1.0;
  A.sparseRow(1,0) = 10.0;
  A.sparseRow(1,1) =  4.0;
  A.sparseRow(1,2) =  5.0;
  A.sparseRow(2,0) =  2.0;
  A.sparseRow(2,1) =  2.0;
  A.sparseRow(2,2) =  6.0;

  // Equality constraint matrix
  SparseNonZeroPattern<Real> nz2(1,nRow);
  nz2.add(0,0);
  nz2.add(0,1);
  nz2.add(0,2);

  Matrix_type A2( nz2 );

  A2.sparseRow(0,0) =  1.0;
  A2.sparseRow(0,1) =  0.0;
  A2.sparseRow(0,2) =  0.0;

  Vector_type x(nRow), b(nRow), b2(1), x2(nRow), c(nRow);

  b[0] = 100.0;
  b[1] = 600.0;
  b[2] = 300.0;

  b2[0] = 0.0;

  c[0] = -10.0;
  c[1] =  -6.0;
  c[2] =  -4.0;

  // correct solution
  x2[0] = 0.0;
  x2[1] = 100.0;
  x2[2] = 0.0;

  double z;

  //Solve the linear program
  Solver.setConstraints(A, A2, b, b2);
  Solver.solve( c, x, z );

  //The solution should be equal to x2!
  for (int i = 0; i < b.m(); i++)
    BOOST_CHECK_CLOSE( x[i], x2[i], 1e-12 );

  // And the objective value should be correct
  BOOST_CHECK_CLOSE( z, -600.0, 1e-12 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LOptSolver_GLPK_Simplex_Equality_MatrixS )
{
  PyDict LOptSolver_dict, GLPK, Simplex_dict, SolutionMethod;

  Simplex_dict[GLPKParam::params.SolutionMethod.Name]  = GLPKParam::params.SolutionMethod.Simplex;
  Simplex_dict[GLPKParam::SimplexParam::params.msg_lev] = 0;

  GLPK[LOptSolverParam::params.LOptSolver.Solver] = LOptSolverParam::params.LOptSolver.GLPK;
  GLPK[GLPKParam::params.SolutionMethod] = Simplex_dict;

  LOptSolver_dict[LOptSolverParam::params.LOptSolver] = GLPK;

  LOptSolverParam::checkInputs(LOptSolver_dict);

  typedef SparseMatrix_CRS< DLA::MatrixS<2, 1, Real> > Matrix_type;
  typedef SparseVector< DLA::VectorS<2,Real> > Vector_C_type;
  typedef SparseVector< Real > Vector_DV_type;

  LOptSolver< Matrix_type, Vector_C_type > Solver(LOptSolver_dict);

  // This is a modified example from the GLPK documentation

  const int nRow = 1;
  const int nVar = 3;

  // Inequality constraint matrix
  SparseNonZeroPattern< DLA::MatrixS<2, 1, Real> > nz(nRow,nVar);
  nz.add(0,0);
  nz.add(0,1);
  nz.add(0,2);

  Matrix_type A( nz );

  A.sparseRow(0,0)(0,0) =  1.0;
  A.sparseRow(0,0)(1,0) = 10.0;
  A.sparseRow(0,1)(0,0) =  1.0;
  A.sparseRow(0,1)(1,0) =  4.0;
  A.sparseRow(0,2)(0,0) =  1.0;
  A.sparseRow(0,2)(1,0) =  5.0;

  // Equality constraint matrix
  SparseNonZeroPattern< DLA::MatrixS<2, 1, Real> > nz2(nRow,nVar);
  nz2.add(0,0);
  nz2.add(0,1);
  nz2.add(0,2);

  Matrix_type A2( nz2 );

  A2.sparseRow(0,0)(0,0) =  1.0;
  A2.sparseRow(0,0)(1,0) =  0.0;
  A2.sparseRow(0,1)(0,0) =  0.0;
  A2.sparseRow(0,1)(1,0) =  1.0;
  A2.sparseRow(0,2)(0,0) =  0.0;
  A2.sparseRow(0,2)(1,0) =  0.0;

  Vector_C_type b(nRow), b2(nRow);
  Vector_DV_type x(nVar), x2(nVar), c(nVar);

  b[0][0] = 100.0;
  b[0][1] = 600.0;

  b2[0][0] =  0.0;
  b2[0][1] = 50.0;

  c[0] = -10.0;
  c[1] =  -6.0;
  c[2] =  -4.0;

  // correct solution
  x2[0] =  0.0;
  x2[1] = 50.0;
  x2[2] = 50.0;

  double z;

  //Solve the linear program
  Solver.setConstraints(A, A2, b, b2);
  Solver.solve( c, x, z );

  //The solution should be equal to x2!
  BOOST_CHECK_CLOSE( x2[0], x[0], 1e-12 );
  BOOST_CHECK_CLOSE( x2[1], x[1], 1e-12 );
  BOOST_CHECK_CLOSE( x2[2], x[2], 1e-12 );

  // And the objective value should be correct
  BOOST_CHECK_CLOSE( z, -500.0, 1e-12 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LOptSolver_GLPK_Simplex_Equality_MatrixD_CRS_MatrixS )
{
  PyDict LOptSolver_dict, GLPK, Simplex_dict, SolutionMethod;

  Simplex_dict[GLPKParam::params.SolutionMethod.Name]  = GLPKParam::params.SolutionMethod.Simplex;
  Simplex_dict[GLPKParam::SimplexParam::params.msg_lev] = 0;

  GLPK[LOptSolverParam::params.LOptSolver.Solver] = LOptSolverParam::params.LOptSolver.GLPK;
  GLPK[GLPKParam::params.SolutionMethod] = Simplex_dict;

  LOptSolver_dict[LOptSolverParam::params.LOptSolver] = GLPK;

  LOptSolverParam::checkInputs(LOptSolver_dict);

  typedef DLA::MatrixD< SparseMatrix_CRS< DLA::MatrixS<2, 1, Real> > > Matrix_type;
  typedef DLA::VectorD< SparseVector< DLA::VectorS<2,Real> > > Vector_C_type;
  typedef DLA::VectorD< SparseVector< Real > > Vector_DV_type;

  LOptSolver< Matrix_type, Vector_C_type > Solver(LOptSolver_dict);

  // This is a modified example from the GLPK documentation

  const int nRow  = 1;
  const int nVar0 = 2;
  const int nVar1 = 1;

  // Inequality constraint matrix
  DLA::MatrixD< SparseNonZeroPattern< DLA::MatrixS<2, 1, Real> > > nz = {{ {nRow,nVar0} }, { {nRow,nVar1} } };

  nz(0,0).add(0,0);
  nz(0,0).add(0,1);

  nz(1,0).add(0,0);

  Matrix_type A( nz );

  A(0,0).sparseRow(0,0)(0,0) =  1.0;
  A(0,0).sparseRow(0,0)(1,0) = 10.0;
  A(0,0).sparseRow(0,1)(0,0) =  1.0;
  A(0,0).sparseRow(0,1)(1,0) =  4.0;
  A(1,0).sparseRow(0,0)(0,0) =  1.0;
  A(1,0).sparseRow(0,0)(1,0) =  5.0;

  // Equality constraint matrix
  DLA::MatrixD< SparseNonZeroPattern< DLA::MatrixS<2, 1, Real> > > nz2 = {{ {nRow,nVar0} }, { {nRow,nVar1} } };
  nz2(0,0).add(0,0);
  nz2(0,0).add(0,1);

  nz2(1,0).add(0,0);

  Matrix_type A2( nz2 );

  A2(0,0).sparseRow(0,0)(0,0) =  1.0;
  A2(0,0).sparseRow(0,0)(1,0) =  0.0;
  A2(0,0).sparseRow(0,1)(0,0) =  0.0;
  A2(0,0).sparseRow(0,1)(1,0) =  1.0;
  A2(1,0).sparseRow(0,0)(0,0) =  0.0;
  A2(1,0).sparseRow(0,0)(1,0) =  0.0;

  Vector_C_type b = { {nRow} }, b2 = { {nRow} };

  Vector_DV_type x  = { {nVar0}, {nVar1} };
  Vector_DV_type x2 = { {nVar0}, {nVar1} };
  Vector_DV_type c  = { {nVar0}, {nVar1} };

  b[0][0][0] = 100.0;
  b[0][0][1] = 600.0;

  b2[0][0][0] =  0.0;
  b2[0][0][1] = 50.0;

  c[0][0] = -10.0;
  c[0][1] =  -6.0;
  c[1][0] =  -4.0;

  // correct solution
  x2[0][0] =  0.0;
  x2[0][1] = 50.0;
  x2[1][0] = 50.0;

  double z;

  //Solve the linear program
  Solver.setConstraints(A, A2, b, b2);
  Solver.solve( c, x, z );

  //The solution should be equal to x2!
  BOOST_CHECK_CLOSE( x2[0][0], x[0][0], 1e-12 );
  BOOST_CHECK_CLOSE( x2[0][1], x[0][1], 1e-12 );
  BOOST_CHECK_CLOSE( x2[1][0], x[1][0], 1e-12 );

  // And the objective value should be correct
  BOOST_CHECK_CLOSE( z, -500.0, 1e-12 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LOptSolver_GLPK_Simplex_Equality_Only )
{
  PyDict LOptSolver_dict, GLPK, Simplex_dict, SolutionMethod;

  Simplex_dict[GLPKParam::params.SolutionMethod.Name]  = GLPKParam::params.SolutionMethod.Simplex;
  Simplex_dict[GLPKParam::SimplexParam::params.msg_lev] = 0;

  GLPK[LOptSolverParam::params.LOptSolver.Solver] = LOptSolverParam::params.LOptSolver.GLPK;
  GLPK[GLPKParam::params.SolutionMethod] = Simplex_dict;

  LOptSolver_dict[LOptSolverParam::params.LOptSolver] = GLPK;

  LOptSolverParam::checkInputs(LOptSolver_dict);

  typedef SparseMatrix_CRS<Real> Matrix_type;
  typedef VectorType<Matrix_type>::type Vector_type;

  LOptSolver< Matrix_type, Vector_type > Solver(LOptSolver_dict);

  // This is a modified example from the GLPK documentation

  const int nRow = 3;

  // Inequality constraint matrix
  SparseNonZeroPattern<Real> nz(nRow-1,nRow);
  nz.add(0,0);
  nz.add(1,0);
  nz.add(1,1);
  nz.add(1,2);

  Matrix_type A( nz );

  BOOST_REQUIRE( A.get_row_ptr() != NULL ); // suppress clang analyzer

  A.sparseRow(0,0) =  1.0;
  A.sparseRow(1,0) = 10.0;
  A.sparseRow(1,1) =  4.0;
  A.sparseRow(1,2) =  5.0;

  Vector_type x(nRow), b(nRow-1), x2(nRow), c(nRow);

  b[0] = 1.0;
  b[1] = 25.0;

  c[0] = -10.0;
  c[1] =  -6.0;
  c[2] =  -4.0;

  // correct solution
  x2[0] = 1.0;
  x2[1] = 3.75;
  x2[2] = 0.0;

  double z;

  //Solve the linear program
  Solver.setConstraints(A, b, true);
  Solver.solve( c, x, z );

  //The solution should be equal to x2!
  for (int i = 0; i < b.m(); i++)
    BOOST_CHECK_CLOSE( x[i], x2[i], 1e-12 );

  // And the objective value should be correct
  BOOST_CHECK_CLOSE( z, -32.5, 1e-12 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LOptSolver_GLPK_Simplex_Equality_Only_MatrixS )
{
  PyDict LOptSolver_dict, GLPK, Simplex_dict, SolutionMethod;

  Simplex_dict[GLPKParam::params.SolutionMethod.Name]  = GLPKParam::params.SolutionMethod.Simplex;
  Simplex_dict[GLPKParam::SimplexParam::params.msg_lev] = 0;

  GLPK[LOptSolverParam::params.LOptSolver.Solver] = LOptSolverParam::params.LOptSolver.GLPK;
  GLPK[GLPKParam::params.SolutionMethod] = Simplex_dict;

  LOptSolver_dict[LOptSolverParam::params.LOptSolver] = GLPK;

  LOptSolverParam::checkInputs(LOptSolver_dict);

  typedef SparseMatrix_CRS< DLA::MatrixS<2, 1, Real> > Matrix_type;
  typedef SparseVector< DLA::VectorS<2,Real> > Vector_C_type;
  typedef SparseVector< Real > Vector_DV_type;

  LOptSolver< Matrix_type, Vector_C_type > Solver(LOptSolver_dict);

  // This is a modified example from the GLPK documentation

  const int nRow = 1;
  const int nVar = 3;

  // Inequality constraint matrix
  SparseNonZeroPattern< DLA::MatrixS<2, 1, Real> > nz(nRow,nVar);
  nz.add(0,0);
  nz.add(0,1);
  nz.add(0,2);

  Matrix_type A( nz );

  BOOST_REQUIRE( A.get_row_ptr() != NULL ); // suppress clang analyzer

  A.sparseRow(0,0)(0,0) =  1.0;
  A.sparseRow(0,0)(1,0) = 10.0;
  A.sparseRow(0,1)(0,0) =  0.0;
  A.sparseRow(0,1)(1,0) =  4.0;
  A.sparseRow(0,2)(0,0) =  0.0;
  A.sparseRow(0,2)(1,0) =  5.0;

  Vector_C_type b(nRow);
  Vector_DV_type x(nVar), x2(nVar), c(nVar);
  b[0][0] = 1.0;
  b[0][1] = 25.0;

  c[0] = -10.0;
  c[1] =  -6.0;
  c[2] =  -4.0;

  // correct solution
  x2[0] = 1.0;
  x2[1] = 3.75;
  x2[2] = 0.0;

  double z;

  //Solve the linear program
  Solver.setConstraints(A, b, true);
  Solver.solve( c, x, z );

  //The solution should be equal to x2!
  for (int i = 0; i < b.m(); i++)
    BOOST_CHECK_CLOSE( x[i], x2[i], 1e-12 );

  // And the objective value should be correct
  BOOST_CHECK_CLOSE( z, -32.5, 1e-12 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LOptSolver_GLPK_Simplex_Equality_Only_MatrixD_CRS_MatrixS )
{
  PyDict LOptSolver_dict, GLPK, Simplex_dict, SolutionMethod;

  Simplex_dict[GLPKParam::params.SolutionMethod.Name]  = GLPKParam::params.SolutionMethod.Simplex;
  Simplex_dict[GLPKParam::SimplexParam::params.msg_lev] = 0;

  GLPK[LOptSolverParam::params.LOptSolver.Solver] = LOptSolverParam::params.LOptSolver.GLPK;
  GLPK[GLPKParam::params.SolutionMethod] = Simplex_dict;

  LOptSolver_dict[LOptSolverParam::params.LOptSolver] = GLPK;

  LOptSolverParam::checkInputs(LOptSolver_dict);

  typedef DLA::MatrixD< SparseMatrix_CRS< DLA::MatrixS<2, 1, Real> > > Matrix_type;
  typedef DLA::VectorD< SparseVector< DLA::VectorS<2,Real> > > Vector_C_type;
  typedef DLA::VectorD< SparseVector< Real > > Vector_DV_type;

  LOptSolver< Matrix_type, Vector_C_type > Solver(LOptSolver_dict);

  // This is a modified example from the GLPK documentation

  const int nRow  = 1;
  const int nVar0 = 2;
  const int nVar1 = 1;

  // Equality constraint matrix
  DLA::MatrixD< SparseNonZeroPattern< DLA::MatrixS<2, 1, Real> > > nz = {{ {nRow,nVar0} }, { {nRow,nVar1} } };
  nz(0,0).add(0,0);
  nz(0,0).add(0,1);
  nz(1,0).add(0,0);

  Matrix_type A( nz );

  BOOST_REQUIRE( A(0,0).get_row_ptr() != NULL ); // suppress clang analyzer
  BOOST_REQUIRE( A(1,0).get_row_ptr() != NULL ); // suppress clang analyzer

  A(0,0).sparseRow(0,0)(0,0) =  1.0;
  A(0,0).sparseRow(0,0)(1,0) = 10.0;
  A(0,0).sparseRow(0,1)(0,0) =  0.0;
  A(0,0).sparseRow(0,1)(1,0) =  4.0;
  A(1,0).sparseRow(0,0)(0,0) =  0.0;
  A(1,0).sparseRow(0,0)(1,0) =  5.0;

  Vector_C_type b = { {nRow} };

  Vector_DV_type x  = { {nVar0}, {nVar1} };
  Vector_DV_type x2 = { {nVar0}, {nVar1} };
  Vector_DV_type c  = { {nVar0}, {nVar1} };

  b[0][0][0] = 1.0;
  b[0][0][1] = 25.0;

  c[0][0] = -10.0;
  c[0][1] =  -6.0;
  c[1][0] =  -4.0;

  // correct solution
  x2[0][0] = 1.0;
  x2[0][1] = 3.75;
  x2[1][0] = 0.0;

  double z;

  //Solve the linear program
  Solver.setConstraints(A, b, true);
  Solver.solve( c, x, z );

  //The solution should be equal to x2!
  SANS_CHECK_CLOSE( x[0][0], x2[0][0], 1e-12, 1e-12 );
  SANS_CHECK_CLOSE( x[0][1], x2[0][1], 1e-12, 1e-12 );
  SANS_CHECK_CLOSE( x[1][0], x2[1][0], 1e-12, 1e-12 );

  // And the objective value should be correct
  BOOST_CHECK_CLOSE( z, -32.5, 1e-12 );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
