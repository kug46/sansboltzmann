// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include <boost/version.hpp>

#include <glpk.h>

//---------------------------------------------------------------------------//
//
// A fixture to initialize and terminate GLPK in unit tests
//
//---------------------------------------------------------------------------//

struct LOpt_unit_fixture
{

  LOpt_unit_fixture()
  {
  }

  ~LOpt_unit_fixture()
  {
    glp_free_env();
  }

};


BOOST_GLOBAL_FIXTURE( LOpt_unit_fixture )
// Boost 1.59 removed the ';' in the above macro, but simply adding it for all versions gives compiler warnings for ';;'
#if BOOST_VERSION >= 105900
;
#endif
