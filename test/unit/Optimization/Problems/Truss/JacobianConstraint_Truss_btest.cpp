// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianInteriorTrace_HDG_AD_btest
// testing of HDG interior trace-integral jacobian: advection-diffusions

#include <boost/mpl/list.hpp>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_Trace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/tools/for_each_InteriorFieldTraceGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup.h"
#include "Field/XFieldArea.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"

#include "Optimization/Problems/Truss/ConstraintFrame_Truss.h"
#include "Optimization/Problems/Truss/JacobianConstraint_Truss.h"
#include "Optimization/Problems/Truss/JacobianConstraint_Truss_Boundary.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianConstraint_Truss_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_Surreal_InteriorEdge )
{

  typedef ConstraintFrame_Truss<PhysD2> ConstraintClass;

  typedef ConstraintClass::ArrayQ<Real> ArrayQ;
  typedef ConstraintClass::MatrixQ<Real> MatrixQ;

  typedef SurrealS<1> SurrealClass;

  // grid
  XField2D_2Triangle_X1_1Group xfld;

  // interface solution: P1 (aka Q1)
  Field_DG_InteriorTrace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, 0, BasisFunctionCategory_Legendre);

  BOOST_CHECK( qIfld.nDOF() == 1 );

  // line trace data
  qIfld.DOF(0) =  5.;

  // constraint
  ConstraintClass con( {0}, {0} );

  // jacobian true

  DLA::MatrixD<MatrixQ> jac(4,1);

  jac = 0;

  // jacobian via finite difference
  jac(0,0)(0,0) =  0.;           // node 0, x
  jac(0,0)(1,0) =  0.;           // node 0, y
  jac(1,0)(0,0) = -0.5*sqrt(2);  // node 1, x
  jac(1,0)(1,0) =  0.5*sqrt(2);  // node 1, y
  jac(2,0)(0,0) =  0.5*sqrt(2);  // node 2, x
  jac(2,0)(1,0) = -0.5*sqrt(2);  // node 2, y
  jac(3,0)(0,0) =  0.;           // node 3, x
  jac(3,0)(1,0) =  0.;           // node 3, y

  // jacobian via Surreal

  const int uIDOF = qIfld.nDOF();

  BOOST_CHECK_EQUAL( 1, uIDOF );

  DLA::MatrixD<MatrixQ> mtxCon(xfld.nDOF(), uIDOF);

  mtxCon = 0;

  for_each_InteriorFieldTraceGroup<TopoD2>::apply(
      JacobianConstraint_Truss<SurrealClass>(con, mtxCon),
      (xfld, qIfld) );

  // check whether jacobians are the same
  for (int i = 0; i < xfld.nDOF(); i++)
  {
    for (int j = 0; j < qIfld.nDOF(); j++)
    {
      for (int d = 0; d < PhysD2::D; d++)
        BOOST_CHECK_CLOSE( jac(i,j)(d,0), mtxCon(i,j)(d,0), 1e-12 );
    }
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_Surreal_Edge )
{

  typedef ConstraintFrame_Truss<PhysD2> ConstraintClass;

  typedef ConstraintClass::ArrayQ<Real> ArrayQ;
  typedef ConstraintClass::MatrixQ<Real> MatrixQ;

  typedef SurrealS<1> SurrealClass;

  // grid
  XField2D_2Triangle_X1_1Group xfld;

  // interface solution: P1 (aka Q1)
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, 0, BasisFunctionCategory_Legendre);

  BOOST_CHECK( qIfld.nDOF() == 5 );

  // line trace data -- random because doesn't matter
  qIfld.DOF(0) =  1.;
  qIfld.DOF(1) =  5.;
  qIfld.DOF(2) =  3.;
  qIfld.DOF(3) =  6.;
  qIfld.DOF(4) =  4.;

  // constraint
  ConstraintClass con( {0}, {0} );

  // jacobian true

  DLA::MatrixD<MatrixQ> jac(4,5);

  jac = 0;

  // jacobian via finite difference
  //  unknown 0 (diagonal)
  jac(0,0)(0,0) =  0.;           // node 0, x
  jac(0,0)(1,0) =  0.;           // node 0, y
  jac(1,0)(0,0) = -0.5*sqrt(2);  // node 1, x
  jac(1,0)(1,0) =  0.5*sqrt(2);  // node 1, y
  jac(2,0)(0,0) =  0.5*sqrt(2);  // node 2, x
  jac(2,0)(1,0) = -0.5*sqrt(2);  // node 2, y
  jac(3,0)(0,0) =  0.;           // node 3, x
  jac(3,0)(1,0) =  0.;           // node 3, y
  //  unknown 1 (bottom)
  jac(0,1)(0,0) =  1.;           // node 0, x
  jac(1,1)(0,0) = -1.;           // node 1, x
  //  unknown 2 (right)
  jac(1,2)(1,0) =  1.;           // node 1, y
  jac(3,2)(1,0) = -1.;           // node 3, y
  //  unknown 3 (top)
  jac(2,3)(0,0) =  1.;           // node 2, x
  jac(3,3)(0,0) = -1.;           // node 3, x
  //  unknown 4 (left)
  jac(0,4)(1,0) =  1.;           // node 0, y
  jac(2,4)(1,0) = -1.;           // node 2, y

  // jacobian via Surreal

  const int uIDOF = qIfld.nDOF();

  BOOST_CHECK_EQUAL( 5, uIDOF );

  DLA::MatrixD<MatrixQ> mtxCon(xfld.nDOF(), uIDOF);

  mtxCon = 0;

  for_each_InteriorFieldTraceGroup<TopoD2>::apply(
      JacobianConstraint_Truss<SurrealClass>(con, mtxCon),
      (xfld, qIfld) );
  for_each_BoundaryTraceGroup<TopoD2>::apply(
      JacobianConstraint_Truss_Boundary<SurrealClass>(con, mtxCon),
      (xfld, qIfld) );


  for (int i = 0; i < xfld.nDOF(); i++)
  {
    for (int j = 0; j < qIfld.nDOF(); j++)
    {
      for (int d = 0; d < PhysD2::D; d++)
        BOOST_CHECK_CLOSE( jac(i,j)(d,0), mtxCon(i,j)(d,0), 1e-12 );
    }
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_Surreal_Edge_SparseMatrix )
{

  typedef ConstraintFrame_Truss<PhysD2> ConstraintClass;

  typedef ConstraintClass::ArrayQ<Real> ArrayQ;
  typedef ConstraintClass::MatrixQ<Real> MatrixQ;

  typedef SurrealS<1> SurrealClass;

  // grid
  XField2D_2Triangle_X1_1Group xfld;

  // interface solution: P1 (aka Q1)
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, 0, BasisFunctionCategory_Legendre);

  BOOST_CHECK( qIfld.nDOF() == 5 );

  // line trace data -- random because doesn't matter
  qIfld.DOF(0) =  1.;
  qIfld.DOF(1) =  5.;
  qIfld.DOF(2) =  3.;
  qIfld.DOF(3) =  6.;
  qIfld.DOF(4) =  4.;

  // constraint
  ConstraintClass con( {0}, {0} );

  // jacobian true

  DLA::MatrixD<MatrixQ> jac( 4,5 );

  jac = 0;

  // jacobian via finite difference
  //  unknown 0 (diagonal)
  jac(0,0)(0,0) =  0.;           // node 0, x
  jac(0,0)(1,0) =  0.;           // node 0, y
  jac(1,0)(0,0) = -0.5*sqrt(2);  // node 1, x
  jac(1,0)(1,0) =  0.5*sqrt(2);  // node 1, y
  jac(2,0)(0,0) =  0.5*sqrt(2);  // node 2, x
  jac(2,0)(1,0) = -0.5*sqrt(2);  // node 2, y
  jac(3,0)(0,0) =  0.;           // node 3, x
  jac(3,0)(1,0) =  0.;           // node 3, y
  //  unknown 1 (bottom)
  jac(0,1)(0,0) =  1.;           // node 0, x
  jac(1,1)(0,0) = -1.;           // node 1, x
  //  unknown 2 (right)
  jac(1,2)(1,0) =  1.;           // node 1, y
  jac(3,2)(1,0) = -1.;           // node 3, y
  //  unknown 3 (top)
  jac(2,3)(0,0) =  1.;           // node 2, x
  jac(3,3)(0,0) = -1.;           // node 3, x
  //  unknown 4 (left)
  jac(0,4)(1,0) =  1.;           // node 0, y
  jac(2,4)(1,0) = -1.;           // node 2, y

  // jacobian via Surreal

  const int uIDOF = qIfld.nDOF();

  BOOST_CHECK_EQUAL( 5, uIDOF );

  SLA::SparseNonZeroPattern < MatrixQ > nz_mtxCon( xfld.nDOF(), uIDOF );

  // mapping
  //    unknown 0 (diagonal)
  nz_mtxCon.add(1,0);           // node 1
  nz_mtxCon.add(2,0);           // node 2
  //      unknown 1 (bottom)
  nz_mtxCon.add(0,1);           // node 0
  nz_mtxCon.add(1,1);           // node 1
  //      unknown 2 (right)
  nz_mtxCon.add(1,2);           // node 1
  nz_mtxCon.add(3,2);           // node 3
  //      unknown 3 (top)
  nz_mtxCon.add(2,3);           // node 2
  nz_mtxCon.add(3,3);           // node 3
  //      unknown 4 (left)
  nz_mtxCon.add(0,4);           // node 0
  nz_mtxCon.add(2,4);           // node 2

  SLA::SparseMatrix_CRS < MatrixQ > mtxCon( nz_mtxCon );

  mtxCon = 0;

  for_each_InteriorFieldTraceGroup<TopoD2>::apply(
      JacobianConstraint_Truss<SurrealClass>(con, mtxCon),
      (xfld, qIfld) );
  for_each_BoundaryTraceGroup<TopoD2>::apply(
      JacobianConstraint_Truss_Boundary<SurrealClass>(con, mtxCon),
      (xfld, qIfld) );

  // unknown 0 (diagonal)
  SANS_CHECK_CLOSE( jac(1,0)(0,0), mtxCon(1,0)(0,0), 1e-12, 1e-12 );           // node 1, x
  SANS_CHECK_CLOSE( jac(1,0)(1,0), mtxCon(1,0)(1,0), 1e-12, 1e-12 );           // node 1, y
  SANS_CHECK_CLOSE( jac(2,0)(0,0), mtxCon(2,0)(0,0), 1e-12, 1e-12 );           // node 2, x
  SANS_CHECK_CLOSE( jac(2,0)(1,0), mtxCon(2,0)(1,0), 1e-12, 1e-12 );           // node 2, y
  //  unknown 1 (bottom)
  SANS_CHECK_CLOSE( jac(0,1)(0,0), mtxCon(0,1)(0,0), 1e-12, 1e-12 );           // node 0, x
  SANS_CHECK_CLOSE( jac(1,1)(0,0), mtxCon(1,1)(0,0), 1e-12, 1e-12 );           // node 1, x
  //  unknown 2 (right)
  SANS_CHECK_CLOSE( jac(1,2)(1,0), mtxCon(1,2)(1,0), 1e-12, 1e-12 );           // node 1, y
  SANS_CHECK_CLOSE( jac(3,2)(1,0), mtxCon(3,2)(1,0), 1e-12, 1e-12 );           // node 3, y
  //  unknown 3 (top)
  SANS_CHECK_CLOSE( jac(2,3)(0,0), mtxCon(2,3)(0,0), 1e-12, 1e-12 );           // node 2, x
  SANS_CHECK_CLOSE( jac(3,3)(0,0), mtxCon(3,3)(0,0), 1e-12, 1e-12 );           // node 3, x
  //  unknown 4 (left)
  SANS_CHECK_CLOSE( jac(0,4)(1,0), mtxCon(0,4)(1,0), 1e-12, 1e-12 );           // node 0, y
  SANS_CHECK_CLOSE( jac(2,4)(1,0), mtxCon(2,4)(1,0), 1e-12, 1e-12 );           // node 2, y

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
