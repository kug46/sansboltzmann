// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianInteriorTrace_HDG_AD_btest
// testing of HDG interior trace-integral jacobian: advection-diffusions

#include <boost/mpl/list.hpp>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_Trace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/tools/for_each_InteriorFieldTraceGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup.h"
#include "Field/XFieldArea.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"

#include "Optimization/Methods/Linear/LOptSolver.h"

#include "Optimization/Problems/Truss/LengthFrame_Truss.h"
#include "Optimization/Problems/Truss/LengthObjective_Truss.h"
#include "Optimization/Problems/Truss/LengthObjective_Truss_Boundary.h"

#include "Optimization/Problems/Truss/ConstraintFrame_Truss.h"
#include "Optimization/Problems/Truss/JacobianConstraint_Truss.h"
#include "Optimization/Problems/Truss/JacobianConstraint_Truss_Boundary.h"

using namespace std;
using namespace SANS;
using namespace SANS::LOPT;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Truss_Optimization_without_adapt_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_Surreal_Edge )
{

  typedef ConstraintFrame_Truss<PhysD2> ConstraintClass;

  typedef LengthFrame_Truss<PhysD2> LengthClass;

  typedef LengthClass::ArrayQ<Real> ArrayQ;

  typedef SurrealS<1> SurrealClass;

  typedef DLA::MatrixD< SLA::SparseMatrix_CRS< DLA::MatrixS<2, 1, Real> > > Matrix_type;
  typedef DLA::VectorD< SLA::SparseVector< DLA::VectorS<2,Real> > > Vector_C_type;
  //typedef DLA::VectorD< SLA::SparseVector<Real> > Vector_DV_type;

  // grid
  XField2D_2Triangle_X1_1Group xfld;

  // interface solution: P1 (aka Q1)
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, 0, BasisFunctionCategory_Legendre);

  // line trace data
  qIfld.DOF(0) =  5.;

  // constraint
  LengthClass len( {0}, {0} );
  ConstraintClass con( {0}, {0} );

  // max stress, tensile and compressive
  Real sig_tens = 50.0;
  Real sig_comp = 50.0;

  // Force applied at node 1
  Real Fx =  250.0;
  Real Fy = -500.0;

  // compute lengths

  const int uIDOF = qIfld.nDOF();

  BOOST_CHECK_EQUAL( 5, uIDOF );

  DLA::VectorD< SLA::SparseVector<ArrayQ> > lGlobal = { {uIDOF}, {uIDOF} };
  DLA::VectorD< SLA::SparseVector<ArrayQ> > sol_f   = { {uIDOF}, {uIDOF} };

  lGlobal = 0;

  for_each_InteriorFieldTraceGroup<TopoD2>::apply(
      LengthObjective_Truss(len, lGlobal[0]),
      (xfld, qIfld) );
  for_each_BoundaryTraceGroup<TopoD2>::apply(
      LengthObjective_Truss_Boundary(len, lGlobal[0]),
      (xfld, qIfld) );

  // Get Jacobian

  DLA::MatrixD< SLA::SparseNonZeroPattern< DLA::MatrixS<2, 1, Real> > > nzFin = {{ {xfld.nDOF(), uIDOF} }, { {xfld.nDOF(), uIDOF} }};

  // nzFin(0,0).add(1,0);
  // nzFin(0,0).add(2,0);
  // nzFin(0,0).add(0,1);
  // nzFin(0,0).add(1,1);
  // nzFin(0,0).add(1,2);
  // nzFin(0,0).add(3,2);
  // nzFin(0,0).add(2,3);
  // nzFin(0,0).add(3,3);
  // nzFin(0,0).add(0,4);
  // nzFin(0,0).add(2,4);
  //
  // nzFin(1,0).add(1,0);
  // nzFin(1,0).add(2,0);
  // nzFin(1,0).add(0,1);
  // nzFin(1,0).add(1,1);
  // nzFin(1,0).add(1,2);
  // nzFin(1,0).add(3,2);
  // nzFin(1,0).add(2,3);
  // nzFin(1,0).add(3,3);
  // nzFin(1,0).add(0,4);
  // nzFin(1,0).add(2,4);

  for_each_InteriorFieldTraceGroup<TopoD2>::apply(
      JacobianConstraint_Truss<SurrealClass>(con, nzFin(0,0) ),
      (xfld, qIfld) );
  for_each_BoundaryTraceGroup<TopoD2>::apply(
      JacobianConstraint_Truss_Boundary<SurrealClass>(con, nzFin(0,0) ),
      (xfld, qIfld) );

  DLA::MatrixD< SLA::SparseMatrix_CRS< DLA::MatrixS<2, 1, Real> > > mtxConFin( nzFin );

  for_each_InteriorFieldTraceGroup<TopoD2>::apply(
      JacobianConstraint_Truss<SurrealClass>(con, mtxConFin(0,0) ),
      (xfld, qIfld) );
  for_each_BoundaryTraceGroup<TopoD2>::apply(
      JacobianConstraint_Truss_Boundary<SurrealClass>(con, mtxConFin(0,0) ),
      (xfld, qIfld) );



  mtxConFin(1,0).copy_from( mtxConFin(0,0) );

  int nnz                        = mtxConFin(1,0).getNumNonZero();
  DLA::MatrixS<2, 1, double> *Rx = mtxConFin(1,0).get_values();
  for (int jj = 0; jj < nnz; jj++)
  {
    // for (int ii = 0; ii < 2; ii++)
    // {
    //   Rx[jj](ii,0) = - Rx[jj](ii,0);
    // }
    Rx[jj] = - Rx[jj];
  }

  Real tmp;
  for (int ii = 0; ii < uIDOF; ii++)
  {
    tmp = lGlobal[0][ii];
    lGlobal[0][ii] = tmp / sig_tens;
    lGlobal[1][ii] = tmp / sig_comp;
  }

  DLA::VectorD< SLA::SparseVector< DLA::VectorS<2,Real> > > fVec = { {xfld.nDOF()} };

  fVec = 0;

  // need these minus signs because the force is pulled to the other side of the equilibrium equation
  fVec[0][0][0] =  - ( -(Fx + Fy) );
  fVec[0][0][1] =  - ( - Fy   );

  fVec[0][1][0] =  - (   Fx  );
  fVec[0][1][1] =  - (   Fy  );

  fVec[0][2][0] =  - (   Fy  );

  // Linear solver

  PyDict LOptSolver_dict, GLPK, Simplex_dict, SolutionMethod;

  Simplex_dict[GLPKParam::params.SolutionMethod.Name]  = GLPKParam::params.SolutionMethod.Simplex;
  Simplex_dict[GLPKParam::SimplexParam::params.msg_lev] = 0;

  GLPK[LOptSolverParam::params.LOptSolver.Solver] = LOptSolverParam::params.LOptSolver.GLPK;
  GLPK[GLPKParam::params.SolutionMethod] = Simplex_dict;

  LOptSolver_dict[LOptSolverParam::params.LOptSolver] = GLPK;

  LOptSolverParam::checkInputs(LOptSolver_dict);

  LOptSolver< Matrix_type, Vector_C_type > Solver(LOptSolver_dict);

  // solve
  double z;

  Solver.setConstraints(mtxConFin, fVec, true);
  Solver.solve( lGlobal, sol_f, z );

  SLA::SparseVector<ArrayQ> forc_corr(uIDOF);
  forc_corr[0] = -Fy*sqrt(2.0);
  forc_corr[1] =  Fx + Fy;
  forc_corr[2] =  0.0;
  forc_corr[3] =  0.0;
  forc_corr[4] =  Fy;

  SLA::SparseVector<ArrayQ> area_corr(uIDOF);
  for (int ii = 0; ii < uIDOF; ii++)
  {
    if (forc_corr[ii] < 0)
    {
      area_corr[ii] = -forc_corr[ii]/sig_comp;
    }
    else
    {
      area_corr[ii] =  forc_corr[ii]/sig_tens;
    }
  }

  // std::cout << "result" << std::endl;
  for (int ii = 0; ii < sol_f[0].m(); ii++)
  {
    SANS_CHECK_CLOSE(sol_f[0][ii]/sig_tens + sol_f[1][ii]/sig_comp, area_corr[ii], 1e-12, 1e-12);
  }
  for (int ii = 0; ii < sol_f[0].m(); ii++)
  {
    SANS_CHECK_CLOSE(sol_f[0][ii] - sol_f[1][ii], forc_corr[ii], 1e-12, 1e-12);
  }


}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
