// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandInteroirTrace_HDG_AD_btest
// testing of residual integrands for HDG: Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Optimization/Problems/Truss/ConstraintFrame_Truss.h"

using namespace std;
using namespace SANS;


#if 0
// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_None> PDEAdvectionDiffusion1D;
typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldLine;
template class IntegrandInteriorTrace_HDG<PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D>>::
BasisWeighted<Real,TopoD0,Node,TopoD1,Line,Line,ElementXFieldLine,ElementXFieldLine>;
template class IntegrandInteriorTrace_HDG<PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D>>::
FieldWeighted<Real,TopoD0,Node,TopoD1,Line,Line,ElementXFieldLine,ElementXFieldLine>;

typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_None > PDEAdvectionDiffusion2D;
typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldTri;
template class IntegrandInteriorTrace_HDG<PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D>>::
BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementXFieldTri,ElementXFieldTri>;
template class IntegrandInteriorTrace_HDG<PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D>>::
FieldWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementXFieldTri,ElementXFieldTri>;
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE( ConstraintFrame_Truss_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_Test_2D_Ones )
{

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;

  typedef Real ArrayQ;

  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldTrace;

  typedef ConstraintFrame_Truss<PhysD2> ConstraintClass;

  // grid

  int order = 1;

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, y1, y2;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 1;

  xedge.DOF(0) = {x1, y1};
  xedge.DOF(1) = {x2, y2};

  // interface solution
  ElementQFieldTrace qedge(0, BasisFunctionCategory_Legendre);
  qedge.DOF(0) = 1.0;

  // static tests
  BOOST_CHECK_EQUAL( 0, qedge.order() );
  BOOST_CHECK_EQUAL( 1, qedge.nDOF() );

  // constraint
  ConstraintClass con( {0}, {0} );

  std::vector< DLA::VectorS<2, Real> > rsdElemTrace(2);
  // constraint functor
  con.constraint( xedge, qedge, rsdElemTrace);

  // check outputs
  BOOST_CHECK_CLOSE( rsdElemTrace[0](0),  0.5*sqrt(2), 1e-12 );
  BOOST_CHECK_CLOSE( rsdElemTrace[0](1),  0.5*sqrt(2), 1e-12 );
  BOOST_CHECK_CLOSE( rsdElemTrace[1](0), -0.5*sqrt(2), 1e-12 );
  BOOST_CHECK_CLOSE( rsdElemTrace[1](1), -0.5*sqrt(2), 1e-12 );

  // Second case
  qedge.DOF(0) = 500.0;
  x2 =  872.0;
  y2 = -1.0;
  xedge.DOF(1) = {x2, y2};

  con.constraint( xedge, qedge, rsdElemTrace);

  // check outputs second case
  BOOST_CHECK_CLOSE( rsdElemTrace[0](0),  872.0*500.0/sqrt( pow(872.0,2) + 1 ), 1e-12 );
  BOOST_CHECK_CLOSE( rsdElemTrace[0](1),   -1.0*500.0/sqrt( pow(872.0,2) + 1 ), 1e-12 );
  BOOST_CHECK_CLOSE( rsdElemTrace[1](0), -872.0*500.0/sqrt( pow(872.0,2) + 1 ), 1e-12 );
  BOOST_CHECK_CLOSE( rsdElemTrace[1](1),    1.0*500.0/sqrt( pow(872.0,2) + 1 ), 1e-12 );

  // Third case
  qedge.DOF(0) = -500.0;
  x2 = - 1.0;
  y2 = -10.0;
  xedge.DOF(1) = {x2, y2};

  con.constraint( xedge, qedge, rsdElemTrace);

  // check outputs third case
  BOOST_CHECK_CLOSE( rsdElemTrace[0](0),  -500.0 * -  1.0 / sqrt( 101.0 ), 1e-12 );
  BOOST_CHECK_CLOSE( rsdElemTrace[0](1),  -500.0 * - 10.0 / sqrt( 101.0 ), 1e-12 );
  BOOST_CHECK_CLOSE( rsdElemTrace[1](0),  -500.0 *    1.0 / sqrt( 101.0 ), 1e-12 );
  BOOST_CHECK_CLOSE( rsdElemTrace[1](1),  -500.0 *   10.0 / sqrt( 101.0 ), 1e-12 );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
