// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianInteriorTrace_HDG_AD_btest
// testing of HDG interior trace-integral jacobian: advection-diffusions

#include <boost/mpl/list.hpp>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_Trace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/tools/for_each_InteriorFieldTraceGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup.h"
#include "Field/XFieldArea.h"

#include "Surreal/SurrealS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"

#include "Optimization/Problems/Truss/LengthFrame_Truss.h"
#include "Optimization/Problems/Truss/LengthObjective_Truss.h"
#include "Optimization/Problems/Truss/LengthObjective_Truss_Boundary.h"
// #include "Optimization/Problems/Truss/JacobianConstraint_Truss_Boundary.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( LengthObjective_Truss_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_InteriorEdge )
{

  typedef LengthFrame_Truss<PhysD2> LengthClass;

  typedef LengthClass::ArrayQ<Real> ArrayQ;

  // grid
  XField2D_2Triangle_X1_1Group xfld;

  // interface solution: P1 (aka Q1)
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, 0, BasisFunctionCategory_Legendre);

  // line trace data
  qIfld.DOF(0) =  5.;

  // constraint
  LengthClass len( {0}, {0} );

  // true lengths

  DLA::VectorD<ArrayQ> l_corr(5);

  l_corr = 0;

  // correct lengths
  l_corr[0] =  sqrt(2.0);   // edge 0
  l_corr[1] =  0.;          // edge 1 -- these are zero because we're only checking internal edges
  l_corr[2] =  0.;          // edge 2 -- these are zero because we're only checking internal edges
  l_corr[3] =  0.;          // edge 3 -- these are zero because we're only checking internal edges
  l_corr[4] =  0.;          // edge 4 -- these are zero because we're only checking internal edges

  // computed lengths

  const int uIDOF = qIfld.nDOF();

  BOOST_CHECK_EQUAL( 5, uIDOF );

  DLA::VectorD<ArrayQ> lGlobal(uIDOF);

  lGlobal = 0;

  for_each_InteriorFieldTraceGroup<TopoD2>::apply(
      LengthObjective_Truss(len, lGlobal),
      (xfld, qIfld) );

  // check whether jacobians are the same

    for (int j = 0; j < qIfld.nDOF(); j++)
    {
        BOOST_CHECK_CLOSE( l_corr(j), lGlobal(j), 1e-12 );
    }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_Edge )
{

  typedef LengthFrame_Truss<PhysD2> LengthClass;

  typedef LengthClass::ArrayQ<Real> ArrayQ;

  // grid
  XField2D_2Triangle_X1_1Group xfld;

  // interface solution: P1 (aka Q1)
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, 0, BasisFunctionCategory_Legendre);

  // line trace data
  qIfld.DOF(0) =  5.;

  // constraint
  LengthClass len( {0}, {0} );

  // true lengths

  DLA::VectorD<ArrayQ> l_corr(5);

  l_corr = 0;

  // correct lengths
  l_corr[0] =  sqrt(2.0);   // edge 0
  l_corr[1] =  1.;          // edge 1
  l_corr[2] =  1.;          // edge 2
  l_corr[3] =  1.;          // edge 3
  l_corr[4] =  1.;          // edge 4

  // computed lengths

  const int uIDOF = qIfld.nDOF();

  BOOST_CHECK_EQUAL( 5, uIDOF );

  DLA::VectorD<ArrayQ> lGlobal(uIDOF);

  lGlobal = 0;

  for_each_InteriorFieldTraceGroup<TopoD2>::apply(
      LengthObjective_Truss(len, lGlobal),
      (xfld, qIfld) );
  for_each_BoundaryTraceGroup<TopoD2>::apply(
      LengthObjective_Truss_Boundary(len, lGlobal),
      (xfld, qIfld) );

  // check whether jacobians are the same

    for (int j = 0; j < qIfld.nDOF(); j++)
    {
        BOOST_CHECK_CLOSE( l_corr(j), lGlobal(j), 1e-12 );
    }

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
