// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEstimate_DGBR2_AD_btest
// testing of Error Estimate umbrella function for DG BR2 with Advection-Diffusion

#include <boost/test/unit_test.hpp>

// #include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin_StrongForm.h"
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
//#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
//#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
//#include "pde/NDConvert/PDENDConvertSpace3D.h"
//#include "pde/NDConvert/BCNDConvertSpace3D.h"

// for the p0 eflds
// #include "Field/FieldLine_DG_Cell.h"
// #include "Field/FieldLine_DG_BoundaryTrace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

// #include "Field/FieldLine_CG_Cell.h"
// #include "Field/FieldLine_CG_BoundaryTrace.h"

#include "Field/FieldArea_CG_Cell.h"
// #include "Field/FieldArea_CG_BoundaryTrace.h"

#include "Field/Tuple/FieldTuple.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin.h"
// #include "Discretization/IntegrateCellGroups.h"
// #include "Discretization/IntegrateBoundaryTraceGroups.h"
// #include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
// #include "Discretization/IntegrateInteriorTraceGroups.h"

// #include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
//#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"

// #include "Discretization/Galerkin/IntegrandCell_Galerkin_Stabilized.h"
// #include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"
// #include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"
// #include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"

#include "ErrorEstimate/ErrorEstimate_Common.h"
// #include "ErrorEstimate/Galerkin/ErrorEstimateCell_Galerkin.h"
// #include "ErrorEstimate/Galerkin/ErrorEstimateInteriorTrace_Galerkin.h"
//
// #include "ErrorEstimate/Galerkin/ErrorEstimateBoundaryTrace_FieldTrace_Galerkin.h"
// #include "ErrorEstimate/Galerkin/ErrorEstimateBoundaryTrace_Galerkin.h"

#include "Field/XField_CellToTrace.h"
#include "Field/Local/XField_LocalPatch.h"
#include "Field/Field_NodalView.h"


#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

// Suite of tests for some of the functions in NodalBase_Galerkin, particularly those used in the adaptation

//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEstimateNodalBase_Galerkin_AD_test_suite )


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ConstructDOFSet_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  // For testing that the DOF Map for use in the local solves in constructed correctly. Make sure that a
  std::vector<std::set<int>> elementDOFSupportMap;

  XField2D_4Triangle_X1_1Group xfld; // Unsplit global grid

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  {
    // Construct a local unsplit grid
    const int group = 0, elem = 0;
    XField_LocalPatchConstructor<PhysD2,Triangle> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);

    // Split the grid using alternate constructor
    const int edge = 0;
    XField_LocalPatch<PhysD2,Triangle> xfld_local(xfld_construct, ElementSplitType::Edge, edge);

    Field_CG_Cell<PhysD2, TopoD2, Real> nodal_fld(xfld_local, 1, BasisFunctionCategory_Hierarchical);

    for_each_CellGroup<TopoD2>::apply( ConstructDOFSet<PhysD2>(elementDOFSupportMap,{0}), nodal_fld  );

    std::vector<int> correctDOFs = { 0, 1, 2, 6 };
    std::vector<int> computeDOFs(elementDOFSupportMap[0].begin(),elementDOFSupportMap[0].end());

    BOOST_CHECK_EQUAL( correctDOFs.size(), computeDOFs.size() );
    for (std::size_t i = 0; i < computeDOFs.size(); i++)
      BOOST_CHECK_EQUAL( correctDOFs[i], computeDOFs[i] );
  }

  {
    // Construct a local unsplit grid
    const int group = 0, elem = 1;
    XField_LocalPatchConstructor<PhysD2,Triangle> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);

    // Split the grid using alternate constructor
    const int edge = 0;
    XField_LocalPatch<PhysD2,Triangle> xfld_local(xfld_construct, ElementSplitType::Edge, edge);

    Field_CG_Cell<PhysD2, TopoD2, Real> nodal_fld(xfld_local, 1, BasisFunctionCategory_Hierarchical);

    for_each_CellGroup<TopoD2>::apply( ConstructDOFSet<PhysD2>(elementDOFSupportMap,{0}), nodal_fld  );

    std::vector<int> correctDOFs = { 0, 1, 2, 6 }; // still the same, because 0 1 2 is always the first
    std::vector<int> computeDOFs(elementDOFSupportMap[0].begin(),elementDOFSupportMap[0].end());

    BOOST_CHECK_EQUAL( correctDOFs.size(), computeDOFs.size() );
    for (std::size_t i = 0; i < computeDOFs.size(); i++)
      BOOST_CHECK_EQUAL( correctDOFs[i], computeDOFs[i] );
  }
}
#endif


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
