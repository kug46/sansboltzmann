% AD PDE Integrand MMS
clear all
close all
if 0
    % 1D - Cell Integrands
    disp('1D CELL 1 Line')
    syms x y z f q
    
    conv = 1.1;
    visc = 2.123;
    
    y = -x.^2*(x - 1)*(x.^3 - x.^2 + x - 1); % weighting function
    
    % classic unit test
    ui = [1,4];
    qi = [2,3];
    u = ui(1)*(1-x) + ui(2)*x; % solution
    q = qi(1)*(1-x) + qi(2)*x;
    
%     % AWError test
%     ui = [ 0.0815792, 0.0470998];
%     qi = [ 0.870178, 0.941995];
%     u = ui(1)*1 + ui(2)*sqrt(3)*(2*x-1);
%     q = qi(1)*1 + qi(2)*sqrt(3)*(2*x-1);

    inv = -diff(y)*conv*u;
    vis = +diff(y)*visc*q;
    f = -diff(y)*(conv*u - visc*q); % advective + diffusive flux
    F = matlabFunction(f);
    Finv = matlabFunction(inv);
    Fvis = matlabFunction(vis);
    
    au = -visc*diff(y)*(q- diff(u));
    Au = matlabFunction(au);
    
    if 0
        disp('PT')
        % Point Val
        xval = [0, 0.15, 0.3, 0.5, 0.75, 0.9];
        PDET = F(xval);
        PDEi = Finv(xval);
        PDEv = Fvis(xval);
        AuT = Au(xval);
        
    else
        disp('INT')
        % Integral Val
        PDET = int(f,[0 1]);
        PDEi = int(inv,[0 1]);
        PDEv = int(vis,[0 1]);
        AuT  = int(au,[0 1]);
        xval = 0;
    end
    
    disp([PDET',PDEi', PDEv', AuT', xval'])
    
    % 
    
elseif 0
    % 1D - Trace Integrand - two length 1 elements
    disp('1D Cell 2 Line')
    clear all
    syms x y z f q
    
    conv = 1.1;
    visc = 2.123;
    
    lengthL = 1;
    lengthR = 1;
    
    y = -x.^2*(x - 2)*(x.^3 - x.^2 + x - 1); % weighting function
    
    tauL = conv + visc/lengthL;
    tauR = conv + visc/lengthR;
    
    uiL = [1,4]; uiR = [7,9];
    qiL = [2,7]; qiR = [9,-1];
    uL = uiL(1)*(1-x) + uiL(2)*x; % L solution
    qL = qiL(1)*(1-x) + qiL(2)*x;
    uR = uiR(1)*(2-x) + uiR(2)*(x-1); % R solution
    qR = qiR(1)*(2-x) + qiR(2)*(x-1);
    %basis functions are R ele in global
    % 2 ele, 0-1, 1-2
    
    % LEFT
    invL = -diff(y)*conv*uL;
    visL = +diff(y)*visc*qL;
    fL = -diff(y)*(conv*uL - visc*qL); % advective + diffusive flux
    FL = matlabFunction(fL);
    FinvL = matlabFunction(invL);
    FvisL = matlabFunction(visL);
    
    auL = -visc*diff(y)*(qL- diff(uL));
    AuL = matlabFunction(auL);
    
    QL = matlabFunction(qL);
    GUL= matlabFunction(diff(uL));
    
    %RIGHT
    invR = -diff(y)*conv*uR;
    visR = +diff(y)*visc*qR;
    fR = -diff(y)*(conv*uR - visc*qR); % advective + diffusive flux
    FR = matlabFunction(fR);
    FinvR = matlabFunction(invR);
    FvisR = matlabFunction(visR);
    
    auR = -visc*diff(y)*(qR- diff(uR));
    AuR = matlabFunction(auR);
    
    QR = matlabFunction(qR);
    GUR= matlabFunction(diff(uR));
    
    Psi = matlabFunction(y);
    if 0
        disp('PT')
        % Point Val
        xval = [0.0469101,0.230765, 0.5, 0.769235, 0.95309 ];
        PSILT = Psi(xval);
        PDELT = FL(xval);
        PDELi = FinvL(xval);
        PDELv = FvisL(xval);
        AuLT = AuL(xval);
        eQL = QL(xval);
        disp('LEFT')
        disp([xval', eQL', AuLT']);
        
        xval = [1.0469101,1.230765, 1.5, 1.769235, 1.95309 ];
        PSIRT = Psi(xval);
        PDERT = FR(xval);
        PDERi = FinvR(xval);
        PDERv = FvisR(xval);
        AuRT = AuR(xval);
        eQR = QR(xval);
        disp('RIGHT')
        disp([xval', eQR', AuRT']);
        
    else
        disp('INT L')
        % Integral Val
        PDELT = int(fL,[0 1]);
        PDELi = int(invL,[0 1]);
        PDELv = int(visL,[0 1]);
        AuLT  = int(auL,[0 1]);
        xval = 0;
        
        disp([PDELi', PDELv', AuLT']);

        disp('INT R')
        % Integral Val
        PDERT = int(fR,[1 2]);
        PDERi = int(invR,[1 2]);
        PDERv = int(visR,[1 2]);
        AuRT  = int(auR,[1 2]);
        
        disp([PDERi', PDERv', AuRT']);
    end
    
    
elseif 0
    % 1D - Trace Integrand - two length 1 elements
    disp('1D TRACE')
    clear all
    syms x y z f q
    
    conv = 1.1;
    visc = 2.123;
    
    lengthL = 1;
    lengthR = 1;
    
    tauL = conv + visc/lengthL;
    tauR = conv + visc/lengthR;
    
    uiL = [1,4]; uiR = [7,9];
    qiL = [2,7]; qiR = [9,-1];
    uL = uiL(1)*(1-x) + uiL(2)*x; % L solution
    qL = qiL(1)*(1-x) + qiL(2)*x;
    uR = uiR(1)*(1-x) + uiR(2)*x; % R solution
    qR = qiR(1)*(1-x) + qiR(2)*x;
    uI = 8; % T Solution
    
    finv    = uI*conv;
    fvisL   = -visc*qL;
    fvisR   = -visc*qR;
    
    FvisL = matlabFunction(fvisL);
    FvisR = matlabFunction(fvisR);
    
    stabL = matlabFunction(tauL*(uL-uI));
    stabR = matlabFunction(tauL*(uR-uI));
    
    PDEi = [finv, -finv, 0];
    PDEv = [FvisL(1), -FvisR(0), FvisL(1)-FvisR(0)];
    PDEs = [stabL(1), -stabR(0), stabL(1)-stabR(0)];
    
    disp([PDEi', PDEv', PDEs']);
    
elseif 0
    % 2D Cell Integrand
    disp('2D CELL')
    
    syms x y z f b
    
    C = [1.1, 0.2]';
    V = [2.123, 0.553;
        0.553, 1.007];
    
    ui = [1,3,4];
    qxi = [2,7,-1];
    qyi = [-3,8,-5];
    w = 27*x*y*(1-x-y); % analytic weight function
    %w = 1-x-y;
    b(1) = 1-x-y;
    b(2) = x;
    b(3) = y;
    
    u = sum(ui.*b); % u from basis
    gradu = [diff(u,x); diff(u,y)];
    qx = sum(qxi.*b);
    qy = sum(qyi.*b);
    q = [qx;qy]; % q from basis
    
    gradw = [diff(w,x), diff(w,y)];
    
    rinv = -gradw*C*u;
    rvis = gradw*V*q;
    rPDE = rinv + rvis;
    
    AuRES = (V*(q-gradu));
    au = -[diff(w,x)*AuRES(1); diff(w,y)*AuRES(2)];
    
    Rinv = matlabFunction(rinv);
    Rvis = matlabFunction(rvis);
    RPDE = matlabFunction(rPDE);
    
    Au = matlabFunction(au);
    
    if 1
        disp('PT')
        % Point Val
        xval = [0.3, 0.75];
        PDET = RPDE(xval(1),xval(2));
        PDEi = Rinv(xval(1),xval(2));
        PDEv = Rvis(xval(1),xval(2));
        AuT = Au(xval(1),xval(2));
    else
        disp('INT')
        % Integral Val
        PDET = int(int(rPDE,y,[0 1-x]),x,[0 1]);
        PDEi = int(int(rinv,y,[0 1-x]),x,[0 1]);
        PDEv = int(int(rvis,y,[0 1-x]),x,[0 1]);
        AuT  = int(int(au,y,[0 1-x]),x,[0 1]);
    end
    
    disp([PDET, PDEi, PDEv])
    disp([AuT'])
    
elseif 1
    % 2D Trace Integrand
    disp('2D TRACE')
    
    syms x y s
    % x y global coords, s t coords in ref ele
    % r coords along the face
    
    C = [1.1, 0.2]';
    V = [2.123, 0.553;
        0.553, 1.007]';
    nL= [1 1];  nL = nL/norm(nL);
    nR= -[1 1]; nR = nR/norm(nR);
    
    uiL = [1,3,4];      uiR = [7,2,9];
    qxiL = [2,7,-1];    qxiR = [9,-1,2];
    qyiL = [-3,8,-5];   qyiR = [6,3,-3];
    uT = [8,-1];
    
    % L basis in glob coords
    bL(1) = 1-x-y;
    bL(2) = x;
    bL(3) = y;
    % R basis in glob coords
    bR(1) = 1 - (1-x) - (1-y);
    bR(2) = 1-x;
    bR(3) = 1-y;
    % F basis in glob coords
    bT(1) = 1-y;
    bT(2) = 1-x;
    
    uL = sum(uiL.*bL); uR = sum(uiR.*bR); uI = sum(uT.*bT);
    qL = [sum(qxiL.*bL);sum(qyiL.*bL)];
    qR = [sum(qxiR.*bR);sum(qyiR.*bR)];
    UL = matlabFunction(uL);
    UR = matlabFunction(uR);
    QL = matlabFunction(qL);
    QR = matlabFunction(qR);
    
    % Weight Function - in glob coords
    w = 1.2 + 0.8*x + 0.3*y; % 1st basis on trace
    dw = [diff(w,x);diff(w,y)];
    %w = y; % 2nd basis on trace
    
    finv = w*uI*C; Finv = matlabFunction(finv);
    
    fvisL = -w*V*qL; FvisL = matlabFunction(fvisL);
    fvisR = -w*V*qR; FvisR = matlabFunction(fvisR);
    
    hL = 0.5/sqrt(2); hR = 0.5/sqrt(2);
    tauL = abs(nL*C)+ nL*V*nL'/hL; tauR = abs(nR*C)+ nR*V*nR'/hR; %+ nL*V*nL'/hL + nR*V*nR'/hR
    stabL = w*tauL*(uL-uI); StabL = matlabFunction(stabL);
    stabR = w*tauR*(uR-uI); StabR = matlabFunction(stabR);
    
    AuRESL = V*nL' *(uL-uI);
    AuRESR = V*nR' *(uR-uI);
    auL = -[dw(1) * AuRESL(1); dw(2) * AuRESL(2)];
    auR = -[dw(1) * AuRESR(1); dw(2) * AuRESR(2)];
    AuL = matlabFunction(auL);
    AuR = matlabFunction(auR);
    
    % bc business
    % Robin
     A = 1;
     B = 2;
    % Flux
%     A = nL*C;
%     B = -1;
    
    bc= 3;
    if 0
        disp('PT')
        % point values
        sRef = [0 0.5 1.0];
        for i = 1:3
            xf = 1-sRef(i);
            yf = sRef(i);
            
            %disp([nL*Finv(xf,yf), nR*Finv(xf,yf), (nL +nR)*Finv(xf,yf)]) % matches vanilla integrand Functor
            %disp([nL*FvisL(xf,yf), nR*FvisR(xf,yf), nL*FvisL(xf,yf)+ nR*FvisR(xf,yf)]) % matches vanilla integrand Functor
            %disp([StabL(xf,yf), StabR(xf,yf), StabL(xf,yf) + StabR(xf,yf)]) % matches vanilla integrand Functor
            
            disp('PDE')
            sRef(i)
            disp([nL*Finv(xf,yf), nL*FvisL(xf,yf), StabL(xf,yf)])
            disp([nR*Finv(xf,yf), nR*FvisR(xf,yf), StabR(xf,yf)])
            disp([(nL +nR)*Finv(xf,yf), nL*FvisL(xf,yf)+ nR*FvisR(xf,yf), StabL(xf,yf) + StabR(xf,yf)])
            disp('Au')
            disp([AuL(xf,yf), AuR(xf,yf)])
        end
        %disp('lagrange multiplier')
        cn = nL*C;
        mu = ((B - A*cn)*w - A*(V*nL')'*dw)/(A^2 +B^2);
        Mu = matlabFunction(mu);
        rBC = (A*uL + B*(V*nL')'*qL) - bc;
        RBC = matlabFunction(rBC);
        %disp(Mu(1-sRef,sRef))
        %disp(RBC(1-sRef,sRef))
    else
        disp('INT')
        % integral along face
        RinvL = int(nL*Finv(1-s/sqrt(2),s/sqrt(2)),s,0,sqrt(2));
        RinvR = int(nR*Finv(1-s/sqrt(2),s/sqrt(2)),s,0,sqrt(2));
        RvisL = int(nL*FvisL(1-s/sqrt(2),s/sqrt(2)),s,0,sqrt(2));
        RvisR = int(nR*FvisR(1-s/sqrt(2),s/sqrt(2)),s,0,sqrt(2));
        RstaL = int(StabL(1-s/sqrt(2),s/sqrt(2)),s,0,sqrt(2));
        RstaR = int(StabR(1-s/sqrt(2),s/sqrt(2)),s,0,sqrt(2));
        
        RauL = int(AuL(1-s/sqrt(2),s/sqrt(2)),s,0,sqrt(2));
        RauR = int(AuR(1-s/sqrt(2),s/sqrt(2)),s,0,sqrt(2));
        
        disp([RinvL, RvisL, RstaL, RinvL+RvisL+RstaL])
        disp([RinvR, RvisR, RstaR, RinvR+RvisR+RstaR])
        disp([RinvL+RinvR, RvisL+RvisR, RstaL+RstaR, RinvL+RinvR+RvisL+RvisR+RstaL+RstaR ])
        
        
        cn = nL*C;
        mu = ((B - A*cn)*w - A*(V*nL')'*dw)/(A^2 +B^2);
        rBC = (A*uL + B*(V*nL')'*qL) - bc;
        rBCMU = matlabFunction(mu*rBC);
        RBCMU = int(rBCMU(1-s/sqrt(2),s/sqrt(2)),s,0,sqrt(2));
        
        disp(RauL)
        disp(RauR)
        
        disp(RBCMU)
        
    end
end