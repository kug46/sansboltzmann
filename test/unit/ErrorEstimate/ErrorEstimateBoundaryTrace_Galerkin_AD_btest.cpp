// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEstimateBoundaryTrace_AD_btest
// testing of boundary integral residual functions with Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
//#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
//#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
//#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
//#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"

//#include "pde/NDConvert/PDENDConvertSpace1D.h"
//#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
//#include "pde/NDConvert/PDENDConvertSpace3D.h"
//#include "pde/NDConvert/BCNDConvertSpace3D.h"

//#include "Field/FieldLine_DG_Cell.h"
//#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/Tuple/FieldTuple.h"

#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"

#include "ErrorEstimate/Galerkin/ErrorEstimateBoundaryTrace_FieldTrace_Galerkin.h"
#include "ErrorEstimate/Galerkin/ErrorEstimateBoundaryTrace_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

//#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
//#include "unit/UnitGrids/XField2D_1Quad_X1_1Group.h"
//#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
//#include "unit/UnitGrids/XField3D_1Hex_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEstimateBoundaryTrace_Galerkin_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_LinearRobin_mitLG_2D_1Triangle_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.321;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC

  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( qfld.nDOF() == 3 );

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;


  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( lgfld.nDOF() == 6 );

  // Lagrange multiplier DOF data - Second face, thus DOF 2 and 3
  lgfld.DOF(2) =  2;
  lgfld.DOF(3) = -3;

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( wfld.nDOF() == 6 );

  // triangle solution (left)
  wfld.DOF(0) = -2;
  wfld.DOF(1) =  4;
  wfld.DOF(2) =  3;
  wfld.DOF(3) =  2;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> mufld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( mufld.nDOF() == 9 );

  // Lagrange multiplier DOF data
  mufld.DOF(3) = -3;
  mufld.DOF(4) =  2;
  mufld.DOF(5) = -1;


  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> efld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK( efld.nDOF() == 1 );

  efld = 0;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, Real> eBfld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK( eBfld.nDOF() == 3 );

  // error estimate field
  eBfld = 0;

  // integrand
  IntegrandClass fcn( pde, bc, {1} );

  // quadrature rule
  int quadratureorder[3] = {2,2,2};

  Real rsdPDETrue, rsdBCTrue;   // Basis function

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ErrorEstimateBoundaryTrace_FieldTrace_Galerkin(fcn),
                                                      xfld, (qfld,wfld,efld), (lgfld,mufld,eBfld), quadratureorder, 3 );

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  //Taken from IntegrandBoundaryTrace_LinearScalar_Galerkin_AD_btest
  //PDE residual: (advective) + (viscous) + (lagrange)
  rsdPDETrue = ( 1313./60. ) + ( -53041./1000. )
             + ( (153336118.+3728125.*sqrt(2.))/(1875000.*sqrt(2.)) );   // Basis function

  //BC residual:  (lagrange)
  rsdBCTrue = ( -(500.+38409.*sqrt(2.))/(1500.*sqrt(2.)) );   // Basis function

  SANS_CHECK_CLOSE( rsdPDETrue, efld.DOF(0), small_tol, close_tol );

  SANS_CHECK_CLOSE( 0,         eBfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue, eBfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,         eBfld.DOF(2), small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_None_2D_1Triangle_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef BCNone<PhysD2,AdvectionDiffusionTraits<PhysD2>::N> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.321;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC

  BCClass bc;

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 0 );

  // grid: P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( qfld.nDOF() == 3 );

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( wfld.nDOF() == 6 );

  // triangle solution (left)
  wfld.DOF(0) = -2;
  wfld.DOF(1) =  4;
  wfld.DOF(2) =  3;
  wfld.DOF(3) =  2;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> efld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK( efld.nDOF() == 1 );

  efld.DOF(0) = 0;

  // integrand
  IntegrandClass fcn( pde, bc, {1} );

  // quadrature rule
  int quadratureorder[3] = {2,2,2};

  Real rsdPDETrue;   // Basis function

  IntegrateBoundaryTraceGroups<TopoD2>::integrate( ErrorEstimateBoundaryTrace_Galerkin(fcn),
                                                   xfld, (qfld,wfld,efld), quadratureorder, 3 );

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;


  //=======================================================
  // Viscous terms zeroed out per request by Prof. Darmofal
  //=======================================================

  //Taken from IntegrandBoundaryTrace_LinearScalar_Galerkin_AD_btest
  //PDE residual: (advective) + (viscous) + (lagrange)
  rsdPDETrue = ( 1313./60. ) + ( -53041./1000. )*0
             + ( 0 );   // Basis function

  SANS_CHECK_CLOSE( rsdPDETrue, efld.DOF(0), small_tol, close_tol );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
