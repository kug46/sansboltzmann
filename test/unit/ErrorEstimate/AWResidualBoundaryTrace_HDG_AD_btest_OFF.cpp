// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AWResidualBoundary_HDG_Triangle_AD_btest
// testing of boundary trace-integral residual functions for HDG
// with Advection-Diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Discretization/HDG/AWIntegrandBoundaryFunctor_HDG.h"
#include "Discretization/HDG/AWIntegrandBCFunctor_HDG.h"
#include "Discretization/HDG/AWResidualBoundaryTrace_HDG.h"
#include "Discretization/HDG/AWResidualBCTrace_HDG.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( AWResidualBoundaryTrace_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AWHDG_1D_1Line_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;

  typedef SolnNDConvertSpace<PhysD1, ScalarFunction1D_Const> WeightFunction;

  typedef AWIntegrandBoundary_HDG<PDEClass, WeightFunction> BoundaryIntegrandClass;
  typedef AWIntegrandBC_HDG<PDEClass, WeightFunction, BCClass> BCIntegrandClass;

  // PDE

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC

  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> ufld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( ufld.nDOF(), 2 );

  // solution data (left)
  ufld.DOF(0) = 1;
  ufld.DOF(1) = 4;

  // auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( qfld.nDOF(), 2 );

  // auxiliary variable data (left)
  qfld.DOF(0) = { 7};
  qfld.DOF(1) = { 9};

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( qfld.nDOF(), 2 );

  // Lagrange multiplier DOF data
  lgfld.DOF(0) =  0; //Left domain boundary
  lgfld.DOF(1) =  7; //Right domain boundary

  // quadrature rule
  int quadratureorder[2] = {0,0};

  // PDE residuals (left): (boundary flux) + (Lagrange)
  Real rsd1PDE = (0)      + (0);
  Real rsd2PDE = (22./5.) + (-19107./1000.) + (92214./3125.);
  Real rsdTPDE = rsd1PDE + rsd2PDE;

  // Auxiliary residuals: (Lagrange)
  Real rsdTAux = -0.;

  // BC residuals
  Real rsd1BC = 0;
  Real rsd2BC = 19607./500.;

  // PDE/BC global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(xfld.nElem());
  SLA::SparseVector<ArrayQ> rsdAuGlobal(PhysD1::D*xfld.nElem());
  SLA::SparseVector<ArrayQ> rsdBCGlobal(2);

  rsdPDEGlobal = 0;
  rsdAuGlobal = 0;
  rsdBCGlobal = 0;

  // integrand

  const std::vector<int> BoundaryGroups = {1};
  DiscretizationHDG<PDEClass> disc( pde, Global, AugGradient );

  // Using the Analytical Weight
  // Weight Function
  Real a0=1.1; // avoiding 1.0
  WeightFunction wfcn(a0);

  Real mu = a0*(B-A*u)/(A*A + B*B);

  // integrands
  BoundaryIntegrandClass fcnbound( pde, wfcn, disc, BoundaryGroups );
  BCIntegrandClass fcnbc( pde, wfcn, disc, bc, BoundaryGroups );

  AWResidualBoundaryTrace_HDG<TopoD1>::integrate( fcnbound, ufld, qfld, quadratureorder, 2, rsdPDEGlobal, rsdAuGlobal);
  AWResidualBCTrace_HDG<TopoD1>::integrate( fcnbc, ufld, qfld, lgfld, quadratureorder, 2, rsdPDEGlobal, rsdAuGlobal, rsdBCGlobal);

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  SANS_CHECK_CLOSE( a0*rsdTPDE, rsdPDEGlobal[0], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdTAux, rsdAuGlobal[0], small_tol, close_tol );

  SANS_CHECK_CLOSE( mu*rsd1BC, rsdBCGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( mu*rsd2BC, rsdBCGlobal[1], small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AWHDG_2D_1Triangle_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;

  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Linear> WeightFunction;

  typedef AWIntegrandBoundary_HDG<PDEClass, WeightFunction> BoundaryIntegrandClass;
  typedef AWIntegrandBC_HDG<PDEClass, WeightFunction, BCClass> BCIntegrandClass;

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC

  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> ufld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle solution data (left)
  ufld.DOF(0) = 1;
  ufld.DOF(1) = 3;
  ufld.DOF(2) = 4;

  // auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle auxiliary variable data (left)
  qfld.DOF(0) = { 2, -3};
  qfld.DOF(1) = { 7,  8};
  qfld.DOF(2) = {-1, -5};

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // Lagrange multiplier DOF data
  lgfld.DOF(0) =  0;
  lgfld.DOF(1) =  0;
  lgfld.DOF(2) =  7;
  lgfld.DOF(3) = -9;
  lgfld.DOF(4) =  0;
  lgfld.DOF(5) =  0;

  // quadrature rule
  int quadratureorder[3] = {2,2,2};

  // PDE/BC global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(xfld.nElem());
  SLA::SparseVector<ArrayQ> rsdAuGlobal(PhysD2::D*xfld.nElem());
  SLA::SparseVector<ArrayQ> rsdBCGlobal(3);

  rsdPDEGlobal = 0;
  rsdAuGlobal = 0;
  rsdBCGlobal = 0;

  // integrand
  const std::vector<int> BoundaryGroups = {1};
  DiscretizationHDG<PDEClass> disc( pde, Global, AugGradient );

  Real a0=1.2,a1=0.8,a2=-0.3;
  WeightFunction wfcn(a0,a1,a2);

  // integrands
  BoundaryIntegrandClass fcnbound( pde, wfcn, disc, BoundaryGroups );
  BCIntegrandClass fcnbc( pde, wfcn, disc, bc, BoundaryGroups );

  // base interface
  AWResidualBoundaryTrace_HDG<TopoD2>::integrate( fcnbound, ufld, qfld, quadratureorder, 3, rsdPDEGlobal, rsdAuGlobal);
  AWResidualBCTrace_HDG<TopoD2>::integrate( fcnbc, ufld, qfld, lgfld, quadratureorder, 3, rsdPDEGlobal, rsdAuGlobal, rsdBCGlobal);

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  // PDE residuals (left): (boundary flux) + (Lagrange)
  Real rsd1PDE = (0)           + (0);
  Real rsd2PDE = (-3329./500.) + (5329./2500. + 21043*sqrt(2)/9375.);
  Real rsd3PDE = (   49./100.) + (-2909./500. - 4043*sqrt(2)/1875.);
  Real rsdTPDE = a0*rsd1PDE + (a1+a0)*rsd2PDE + (a2+a0)*rsd3PDE;

  // Auxiliary residuals: (Lagrange)
  Real rsd1Aux = 0;
  Real rsd2Aux = -223./250. - 2896101*sqrt(2)/625000.;
  Real rsd3Aux = 17171./1250. - 114399*sqrt(2)/125000.;
  Real rsd1Auy = 0;
  Real rsd2Auy = -13./25. - 168831*sqrt(2)/62500.;
  Real rsd3Auy = 1001./125. - 6669*sqrt(2)/12500.;
  Real rsdTAux = -a1*rsd1Aux - (a1)*rsd2Aux - (a1)*rsd3Aux;
  Real rsdTAuy = -a2*rsd1Auy - (a2)*rsd2Auy - (a2)*rsd3Auy;

  // BC residuals
  Real rsdTBC = 3.817880070565693; // AD_PDE_Integrand_MMS.m

  SANS_CHECK_CLOSE( rsdTPDE, rsdPDEGlobal[0], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdTAux, rsdAuGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTAuy, rsdAuGlobal[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdTBC, rsdBCGlobal[1], small_tol, close_tol );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
