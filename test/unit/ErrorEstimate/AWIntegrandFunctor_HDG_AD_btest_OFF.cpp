// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AWIntegrandFunctor_HDG_AD_btest
// testing of residual integrands for HDG: Advection-Diffusion

#include "SANS_btest.h"

#include <boost/test/unit_test.hpp>
#include "tools/SANSnumerics.h"     // Real

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"

#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"

#include "Discretization/HDG/AWIntegrandFunctor_HDG.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( AWIntegrandFunctor_HDG_AD_test_suite )

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AWIntegrandCellFunctor_HDG_1D_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef SolnNDConvertSpace<PhysD1, ScalarFunction1D_Hex> WeightFunction;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;

  typedef Element<ArrayQ,TopoD1,Line> CellUFieldClass;
  typedef Element<DLA::VectorS<1,ArrayQ>,TopoD1,Line> ElementQFieldClass;

  typedef AWIntegrandCell_HDG<PDEClass,WeightFunction> IntegrandClass;
  typedef IntegrandClass::Functor<Real,TopoD1,Line> FunctorClass;

  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = {x1};
  xfldElem.DOF(1) = {x2};

  // solution
  CellUFieldClass ufldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, ufldElem.order() );
  BOOST_CHECK_EQUAL( 2, ufldElem.nDOF() );

  // line solution
  ufldElem.DOF(0) = 1;
  ufldElem.DOF(1) = 4;

  // auxiliary variable
  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  qfldElem.DOF(0) = {2};
  qfldElem.DOF(1) = {3};

  // HDG discretization (not used)
  DiscretizationClass disc( pde );

  // Using the Analytical Weight
  // Weight Function
  WeightFunction wfcn;

  // integrand
  IntegrandClass fcnint( pde, wfcn, disc );

  // integrand functor
  FunctorClass fcn = fcnint.integrand(xfldElem, ufldElem, qfldElem );

  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  Real sRef;
  FunctorClass::IntegrandType integrand;

  /*      PDET               PDEi               PDEv                  Au            x
                   0                   0                   0                   0   0
  -0.556454515031250   0.298892034375000  -0.855346549406250   0.338160263718750   0.15
  -0.585335982000000   0.438022200000000  -1.023358182000000   0.311456838000000   0.30
  -0.159843750000000   0.171875000000000  -0.331718750000000   0.066343750000000   0.50
   0.543710449218750  -0.858837890625000   1.402548339843750  -0.127504394531250   0.75
   0.513453402000000  -1.001464200000000   1.514917602000000  -0.052238538000000   0.90

   Taken from AD_PDE_Integrand_MMS.m
   */
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  sRef = 0.0;
  fcn( {sRef}, &integrand, 1);
  Real PDETrue  = 0;
  Real AuTrue   = -0;
  SANS_CHECK_CLOSE( PDETrue, integrand.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( AuTrue, integrand.Au[0], small_tol, close_tol );

  sRef = 0.15;
  fcn( {sRef}, &integrand, 1);
  PDETrue  = 0.298892034375000 + -0.855346549406250;
  AuTrue   = -0.338160263718750;
  SANS_CHECK_CLOSE( PDETrue, integrand.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( AuTrue, integrand.Au[0], small_tol, close_tol );

  sRef = 0.3;
  fcn( {sRef}, &integrand, 1 );
  PDETrue  = 0.438022200000000 + -1.023358182000000;
  AuTrue   = -0.311456838000000;
  SANS_CHECK_CLOSE( PDETrue, integrand.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( AuTrue, integrand.Au[0], small_tol, close_tol );

  sRef = 0.5;
  fcn( {sRef}, &integrand, 1 );
  PDETrue  = 0.171875000000000 + -0.331718750000000;
  AuTrue   = -0.066343750000000;
  SANS_CHECK_CLOSE( PDETrue, integrand.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( AuTrue, integrand.Au[0], small_tol, close_tol );

  sRef = 0.75;
  fcn( {sRef}, &integrand, 1 );
  PDETrue  = -0.858837890625000 +  1.402548339843750;
  AuTrue   = 0.127504394531250;
  SANS_CHECK_CLOSE( PDETrue, integrand.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( AuTrue, integrand.Au[0], small_tol, close_tol );

  sRef = 0.9;
  fcn( {sRef}, &integrand, 1 );
  PDETrue  = -1.001464200000000 + 1.514917602000000;
  AuTrue   = 0.052238538000000;
  SANS_CHECK_CLOSE( PDETrue, integrand.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( AuTrue, integrand.Au[0], small_tol, close_tol );
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AWIntegrandCellFunctor_HDG_1D_Line_test_composite )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef SolnNDConvertSpace<PhysD1, ScalarFunction1D_Linear> WeightFunction;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;

  typedef Element<ArrayQ,TopoD1,Line> CellUFieldClass;
  typedef Element<DLA::VectorS<1,ArrayQ>,TopoD1,Line> ElementQFieldClass;

  typedef AWIntegrandCell_HDG<PDEClass,WeightFunction> IntegrandClass;
  typedef IntegrandClass::Functor<Real,TopoD1,Line> FunctorClass;

  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = {x1};
  xfldElem.DOF(1) = {x2};

  // solution
  CellUFieldClass ufldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, ufldElem.order() );
  BOOST_CHECK_EQUAL( 2, ufldElem.nDOF() );

  // line solution
  ufldElem.DOF(0) = 1;
  ufldElem.DOF(1) = 4;

  // auxiliary variable
  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  qfldElem.DOF(0) = {2};
  qfldElem.DOF(1) = {3};

  // HDG discretization (not used)
  DiscretizationClass disc( pde );

  // Using the Analytical Weight
  // Weight Function
  Real a0=1.2, a1=-0.2;
  WeightFunction wfcn(a0,a1);

  // integrand
  IntegrandClass fcnint( pde, wfcn, disc );

  // integrand functor
  FunctorClass fcn = fcnint.integrand(xfldElem, ufldElem, qfldElem );

  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  FunctorClass::IntegrandType integrand;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  Real sRef;
  Real integrandPDETrue[2], integrandAuxTrue[2];

  // PDE residuals: (advective) + (viscous)
  integrandPDETrue[0] = ( 11./10.) + (-2123./500.);
  integrandPDETrue[1] = (-11./10.) + ( 2123./500.);

  // auxiliary variable definition
  integrandAuxTrue[0] = -2123./1000.;
  integrandAuxTrue[1] = 0;

  sRef = 0;
  fcn( {sRef}, &integrand, 1 );
  Real PDET = a0*integrandPDETrue[0] + (a0+a1)*integrandPDETrue[1];
  Real AuxT = -a1*integrandAuxTrue[0] - (a1)*integrandAuxTrue[1];

  SANS_CHECK_CLOSE( PDET, integrand.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( AuxT, integrand.Au[0], small_tol, close_tol );

  // PDE residuals: (advective) + (viscous)
  integrandPDETrue[0] = ( 22./5.) + (-6369./1000.);
  integrandPDETrue[1] = (-22./5.) + ( 6369./1000.);

  // auxiliary variable definition
  integrandAuxTrue[0] = 0;
  integrandAuxTrue[1] = 0;

  sRef = 1;
  fcn( {sRef}, &integrand, 1 );
  PDET = a0*integrandPDETrue[0] + (a0+a1)*integrandPDETrue[1];
  AuxT = -a0*integrandAuxTrue[0] - (a0+a1)*integrandAuxTrue[1];

  SANS_CHECK_CLOSE( PDET, integrand.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( AuxT, integrand.Au[0], small_tol, close_tol );

  // PDE residuals: (advective) + (viscous)
  integrandPDETrue[0] = ( 11./4.) + (-2123./400.);
  integrandPDETrue[1] = (-11./4.) + ( 2123./400.);

  // auxiliary variable definition
  integrandAuxTrue[0] = -2123./4000.;
  integrandAuxTrue[1] = -2123./4000.;

  sRef = 0.5;
  fcn( {sRef}, &integrand, 1 );
  PDET = a0*integrandPDETrue[0] + (a0+a1)*integrandPDETrue[1];
  AuxT = -a1*integrandAuxTrue[0] - (a1)*integrandAuxTrue[1];

  SANS_CHECK_CLOSE( PDET, integrand.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( AuxT, integrand.Au[0], small_tol, close_tol );
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AWIntegrandTraceFunctor_HDG_1D_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef SolnNDConvertSpace<PhysD1, ScalarFunction1D_Const> WeightFunction;

  typedef ElementXField<PhysD1,TopoD0,Node> TraceXFieldClass;
  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;

  typedef Element<ArrayQ,TopoD1,Line> CellUFieldClass;
  typedef Element<ArrayQ,TopoD0,Node> TraceUFieldClass;
  typedef Element<DLA::VectorS<1,ArrayQ>,TopoD1,Line> ElementQFieldClass;

  typedef AWIntegrandTrace_HDG<PDEClass,WeightFunction> IntegrandClass;
  typedef IntegrandClass::Functor<Real,TopoD0,Node, TopoD1, Line, Line> FunctorClass;
  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  int order = 1;
  ElementXFieldClass xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldClass xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemR.nDOF() );


  TraceXFieldClass xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent elements grid
  Real x1, x2, x3;

  x1 = 0;
  x2 = 1;
  x3 = 2;

  xfldElemL.DOF(0) = {x1};
  xfldElemL.DOF(1) = {x2};

  xfldElemR.DOF(0) = {x2};
  xfldElemR.DOF(1) = {x3};

  xnode.DOF(0) = {x2};
  xnode.normalSignL() = 1;

  // solution

  order = 1;
  CellUFieldClass ufldElemL(order, BasisFunctionCategory_Hierarchical);
  CellUFieldClass ufldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, ufldElemL.order() );
  BOOST_CHECK_EQUAL( 2, ufldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, ufldElemR.order() );
  BOOST_CHECK_EQUAL( 2, ufldElemR.nDOF() );

  // line solution (left)
  ufldElemL.DOF(0) = 1;
  ufldElemL.DOF(1) = 4;

  // line solution (right)
  ufldElemR.DOF(0) = 7;
  ufldElemR.DOF(1) = 9;

  // auxiliary variable

  ElementQFieldClass qfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldClass qfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, qfldElemL.nDOF() );

  qfldElemL.DOF(0) = { 2};
  qfldElemL.DOF(1) = { 7};

  qfldElemR.DOF(0) = { 9};
  qfldElemR.DOF(1) = {-1};

  // interface solution

  TraceUFieldClass uedge(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, uedge.order() );
  BOOST_CHECK_EQUAL( 1, uedge.nDOF() );

  uedge.DOF(0) =  8;

  // HDG discretization (not used)
  DiscretizationClass disc( pde, Local ); // HDG(h)

  // Using the Analytical Weight
  // Weight Function
  Real a0=1.1; // avoiding 1.0
  WeightFunction wfcn(a0);

  // integrand
  IntegrandClass fcnint( pde, wfcn, disc );

  // integrand functor
  FunctorClass fcn = fcnint.integrand( xnode, uedge, CanonicalTraceToCell(0,0), CanonicalTraceToCell(1,0),
                                       xfldElemL, ufldElemL, qfldElemL,
                                       xfldElemR, ufldElemR, qfldElemR );

  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOFLeft() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOFRight() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  Real sRef;
  FunctorClass::IntegrandCellType integrandCellL, integrandCellR;
  ArrayQ integrandPDEI;

  sRef = 0.0;
  fcn( sRef, &integrandCellL, 1,
             &integrandCellR, 1,
             &integrandPDEI, 1 );

  const Real small_tol = 1e-13;
  const Real close_tol = 2e-12;
  Real LPDETrue = a0*((22/5.)  + (-14861./1000.) + (-2123./250.));
  Real RPDETrue = a0*((-88/10.) + (19107./1000.)  + (-2123./1000.));

  //    Taken from IntegrandInteriorTrace_HDG, scaled by a0
  Real IPDETrue = a0*((-4.4)        + (2123./500.) + (-2123./200.));
  SANS_CHECK_CLOSE( LPDETrue, integrandCellL.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( RPDETrue, integrandCellR.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( IPDETrue, integrandPDEI, small_tol, close_tol );

  Real LAuTrue = 0.;
  Real RAuTrue = 0.;
  SANS_CHECK_CLOSE( LAuTrue, integrandCellL.Au[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( RAuTrue, integrandCellR.Au[0], small_tol, close_tol);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AWIntegrandCellFunctor_HDG_2D_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;

  typedef Element<ArrayQ,TopoD2,Triangle> CellUFieldClass;
  typedef Element<DLA::VectorS<2,ArrayQ>,TopoD2,Triangle> ElementQFieldClass;

  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Cubic> WeightFunction;
  //typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Linear> WeightFunction;

  typedef AWIntegrandCell_HDG<PDEClass,WeightFunction> IntegrandClass;
  typedef IntegrandClass::Functor<Real,TopoD2,Triangle> FunctorClass;

  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  // solution

  CellUFieldClass ufldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, ufldElem.order() );
  BOOST_CHECK_EQUAL( 3, ufldElem.nDOF() );

  // triangle solution
  ufldElem.DOF(0) = 1;
  ufldElem.DOF(1) = 3;
  ufldElem.DOF(2) = 4;

  // auxiliary variable

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  qfldElem.DOF(0) = { 2, -3};
  qfldElem.DOF(1) = { 7,  8};
  qfldElem.DOF(2) = {-1, -5};

  // HDG discretization (not used)
  DiscretizationClass disc( pde, Local );

  // Using the Analytical Weight
  // Weight Function
  WeightFunction wfcn;

  // integrand
  IntegrandClass fcnint( pde, wfcn, disc );

  // integrand functor
  FunctorClass fcn = fcnint.integrand(xfldElem, ufldElem, qfldElem );

  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  Real sRef, tRef;
  Real integrandPDETrue, integrandAuxTrue, integrandAuyTrue;
  FunctorClass::IntegrandType integrand;


  sRef = 0.2;  tRef = 0.2;
  fcn( {sRef, tRef}, &integrand, 1 );

  // AD_PDE_Integrand_MMS.m
  integrandPDETrue = (-5616./1000) + (9828864./1000000);
  integrandAuxTrue = (3.182543999999999);
  integrandAuyTrue = (8.657711999999997);

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuxTrue, integrand.Au[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuyTrue, integrand.Au[1], small_tol, close_tol );

  sRef = 0.3;  tRef = 0.75;
  fcn( {sRef, tRef}, &integrand, 1 );

  // AD_PDE_Integrand_MMS.m
  integrandPDETrue = (35.0051625) + (-10.754056125);
  integrandAuxTrue = (-27.746499374999996);
  integrandAuyTrue = (-30.094091999999996);

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuxTrue, integrand.Au[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuyTrue, integrand.Au[1], small_tol, close_tol );

  sRef = 0.6;  tRef = 0.25;
  fcn( {sRef, tRef}, &integrand, 1 );

  // AD_PDE_Integrand_MMS.m
  integrandPDETrue = (10.8124875) + (-41.478348375);
  integrandAuxTrue = (14.677351874999999);
  integrandAuyTrue = (2.178818999999998);

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuxTrue, integrand.Au[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuyTrue, integrand.Au[1], small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AWIntegrandCellFunctor_HDG_2D_Triangle_test_composite )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;

  typedef Element<ArrayQ,TopoD2,Triangle> CellUFieldClass;
  typedef Element<DLA::VectorS<2,ArrayQ>,TopoD2,Triangle> ElementQFieldClass;

  //typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Cubic> WeightFunction;
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Linear> WeightFunction;

  typedef AWIntegrandCell_HDG<PDEClass,WeightFunction> IntegrandClass;
  typedef IntegrandClass::Functor<Real,TopoD2,Triangle> FunctorClass;

  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  Real u = 1.;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  // solution

  CellUFieldClass ufldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, ufldElem.order() );
  BOOST_CHECK_EQUAL( 3, ufldElem.nDOF() );

  // triangle solution
  ufldElem.DOF(0) = 1;
  ufldElem.DOF(1) = 3;
  ufldElem.DOF(2) = 4;

  // auxiliary variable

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  qfldElem.DOF(0) = { 2, -3};
  qfldElem.DOF(1) = { 7,  8};
  qfldElem.DOF(2) = {-1, -5};

  // HDG discretization (not used)
  DiscretizationClass disc( pde );

  // Using the Analytical Weight
  // Weight Function
  Real a0=1.2, a1=0.8, a2=0.3;
  WeightFunction wfcn(a0,a1,a2);

  // integrand
  IntegrandClass fcnint( pde, wfcn, disc );

  // integrand functor
  FunctorClass fcn = fcnint.integrand(xfldElem, ufldElem, qfldElem );

  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  Real sRef, tRef;
  Real PDETrue[3], AuxTrue[3], AuyTrue[3];
  FunctorClass::IntegrandType integrand;

  sRef = 0;  tRef = 0;
  fcn( {sRef, tRef}, &integrand, 1 );

  // PDE residuals: (advective) + (viscous)
  PDETrue[0] = ( 1.2) + ( -84./125.);
  PDETrue[1] = (-1)   + (2587./1000.);
  PDETrue[2] = (-0.2) + (-383./200.);

  // auxiliary variable definition
  AuxTrue[0] = -1659./500.;
  AuxTrue[1] = 0;
  AuxTrue[2] = 0;
  AuyTrue[0] = -3021./500.;
  AuyTrue[1] = 0;
  AuyTrue[2] = 0;

  Real PDETTrue = a0*PDETrue[0] + (a0+a1)*PDETrue[1] + (a0+a2)*PDETrue[2];
  Real AuxTTrue = -a1*AuxTrue[0] - (a1)*AuxTrue[1] - (a1)*AuxTrue[2];
  Real AuyTTrue = -a2*AuyTrue[0] - (a2)*AuyTrue[1] - (a2)*AuyTrue[2];

  SANS_CHECK_CLOSE( PDETTrue, integrand.PDE, small_tol, close_tol );

  SANS_CHECK_CLOSE( AuxTTrue, integrand.Au[0], small_tol, close_tol );

  SANS_CHECK_CLOSE( AuyTTrue, integrand.Au[1], small_tol, close_tol );

  sRef = 1;  tRef = 0;
  fcn( {sRef, tRef}, &integrand, 1 );

  // PDE residuals: (advective) + (viscous)
  PDETrue[0] = ( 3.6) + (-7803./250.);
  PDETrue[1] = (-3)   + ( 3857./200.);
  PDETrue[2] = (-0.6) + (11927./1000.);

  // auxiliary variable definition
  AuxTrue[0] = 0;
  AuxTrue[1] = 669./50.;
  AuxTrue[2] = 0;
  AuyTrue[0] = 0;
  AuyTrue[1] = 39./5.;
  AuyTrue[2] = 0;

  PDETTrue = a0*PDETrue[0] + (a0+a1)*PDETrue[1] + (a0+a2)*PDETrue[2];
  AuxTTrue = -a1*AuxTrue[0] - (a1)*AuxTrue[1] - (a1)*AuxTrue[2];
  AuyTTrue = -a2*AuyTrue[0] - (a2)*AuyTrue[1] - (a2)*AuyTrue[2];

  SANS_CHECK_CLOSE( PDETTrue, integrand.PDE, small_tol, close_tol );

  SANS_CHECK_CLOSE( AuxTTrue, integrand.Au[0], small_tol, close_tol );

  SANS_CHECK_CLOSE( AuyTTrue, integrand.Au[1], small_tol, close_tol );

  sRef = 0;  tRef = 1;
  fcn( {sRef, tRef}, &integrand, 1 );

  // PDE residuals: (advective) + (viscous)
  PDETrue[0] = ( 4.8) + ( 2619./250.);
  PDETrue[1] = (-4)   + ( -611./125.);
  PDETrue[2] = (-0.8) + (-1397./250.);

  // auxiliary variable definition
  AuxTrue[0] = 0;
  AuxTrue[1] = 0;
  AuxTrue[2] = -10793./1000.;
  AuyTrue[0] = 0;
  AuyTrue[1] = 0;
  AuyTrue[2] = -1943./200.;

  PDETTrue = a0*PDETrue[0] + (a0+a1)*PDETrue[1] + (a0+a2)*PDETrue[2];
  AuxTTrue = -a1*AuxTrue[0] - (a1)*AuxTrue[1] - (a1)*AuxTrue[2];
  AuyTTrue = -a2*AuyTrue[0] - (a2)*AuyTrue[1] - (a2)*AuyTrue[2];

  SANS_CHECK_CLOSE( PDETTrue, integrand.PDE, small_tol, close_tol );

  SANS_CHECK_CLOSE( AuxTTrue, integrand.Au[0], small_tol, close_tol );

  SANS_CHECK_CLOSE( AuyTTrue, integrand.Au[1], small_tol, close_tol );

  sRef = 1./3.;  tRef = 1./3.;
  fcn( {sRef, tRef}, &integrand, 1 );

  // PDE residuals: (advective) + (viscous)
  PDETrue[0] = ( 3.2)    + ( -892./125.);
  PDETrue[1] = (-8./3.)  + ( 2123./375.);
  PDETrue[2] = (-8./15.) + (  553./375.);

  // auxiliary variable definition
  AuxTrue[0] =  -731./9000.;
  AuxTrue[1] =  -731./9000.;
  AuxTrue[2] =  -731./9000.;
  AuyTrue[0] = -7957./9000.;
  AuyTrue[1] = -7957./9000.;
  AuyTrue[2] = -7957./9000.;

  PDETTrue = a0*PDETrue[0] + (a0+a1)*PDETrue[1] + (a0+a2)*PDETrue[2];
  AuxTTrue = -a1*AuxTrue[0] - (a1)*AuxTrue[1] - (a1)*AuxTrue[2];
  AuyTTrue = -a2*AuyTrue[0] - (a2)*AuyTrue[1] - (a2)*AuyTrue[2];

  SANS_CHECK_CLOSE( PDETTrue, integrand.PDE, small_tol, close_tol );

  SANS_CHECK_CLOSE( AuxTTrue, integrand.Au[0], small_tol, close_tol );

  SANS_CHECK_CLOSE( AuyTTrue, integrand.Au[1], small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AWIntegrandTraceFunctor_HDG_2D_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Linear> WeightFunction;

  typedef ElementXField<PhysD2,TopoD1,Line> TraceXFieldClass;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;

  typedef Element<ArrayQ,TopoD2,Triangle> CellUFieldClass;
  typedef Element<ArrayQ,TopoD1,Line> TraceUFieldClass;
  typedef Element<DLA::VectorS<2,ArrayQ>,TopoD2,Triangle> ElementQFieldClass;

  typedef AWIntegrandTrace_HDG<PDEClass,WeightFunction> IntegrandClass;
  typedef IntegrandClass::Functor<Real, TopoD1, Line, TopoD2, Triangle, Triangle> FunctorClass;

  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldClass xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldClass xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemR.nDOF() );

  TraceXFieldClass xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;
  x4 = 1;  y4 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xfldElemR.DOF(0) = {x4, y4};
  xfldElemR.DOF(1) = {x3, y3};
  xfldElemR.DOF(2) = {x2, y2};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution

  order = 1;
  CellUFieldClass ufldElemL(order, BasisFunctionCategory_Hierarchical);
  CellUFieldClass ufldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, ufldElemL.order() );
  BOOST_CHECK_EQUAL( 3, ufldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, ufldElemR.order() );
  BOOST_CHECK_EQUAL( 3, ufldElemR.nDOF() );

  // triangle solution (left)
  ufldElemL.DOF(0) = 1;
  ufldElemL.DOF(1) = 3;
  ufldElemL.DOF(2) = 4;

  // triangle solution (right)
  ufldElemR.DOF(0) = 7;
  ufldElemR.DOF(1) = 2;
  ufldElemR.DOF(2) = 9;

  // auxiliary variable

  ElementQFieldClass qfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldClass qfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemL.nDOF() );

  qfldElemL.DOF(0) = { 2, -3};
  qfldElemL.DOF(1) = { 7,  8};
  qfldElemL.DOF(2) = {-1, -5};

  qfldElemR.DOF(0) = { 9,  6};
  qfldElemR.DOF(1) = {-1,  3};
  qfldElemR.DOF(2) = { 2, -3};

  // interface solution

  TraceUFieldClass uedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, uedge.order() );
  BOOST_CHECK_EQUAL( 2, uedge.nDOF() );

  uedge.DOF(0) =  8;
  uedge.DOF(1) = -1;

  // HDG discretization
  DiscretizationClass disc( pde, Local );

  // Using the Analytical Weight
  // Weight Function
  Real a0=1.2, a1=0.8, a2=0.3; // avoiding 1.0
  WeightFunction wfcn(a0,a1,a2);

  // integrand
  IntegrandClass fcnint( pde, wfcn, disc );

  // integrand functor
  FunctorClass fcn = fcnint.integrand( xedge, uedge, CanonicalTraceToCell(0,1), CanonicalTraceToCell(0,-1),
                                       xfldElemL, ufldElemL, qfldElemL,
                                       xfldElemR, ufldElemR, qfldElemR );

  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOFLeft() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOFRight() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  Real sRef;
  Real integrandCellLTrue, integrandCellRTrue, integrandPDEITrue;
  Real integrandAuxLTrue, integrandAuyLTrue;
  Real integrandAuxRTrue, integrandAuyRTrue;
  FunctorClass::IntegrandCellType integrandCellL, integrandCellR;
  ArrayQ integrandPDEI;

  sRef = 0;
  fcn( sRef, &integrandCellL, 1,
             &integrandCellR, 1,
             &integrandPDEI, 1 );

  // 2.0*results from IntegrandInteriorTrace_HDG_AD_btest
  integrandCellLTrue = 2.0*( (39/(10.*sqrt(2)))  + (-7803./(250*sqrt(2))) + (-1059./(25.*sqrt(2))) );
  integrandCellRTrue = 2.0*( (-26*sqrt(2)/5.) + (42*sqrt(2)/125.) + (1059./(125.*sqrt(2))) );
  integrandPDEITrue  =  2.0*( (-13/(2.*sqrt(2))) + (-1527./(50.*sqrt(2))) + (-2118.*sqrt(2)/125.) );

  SANS_CHECK_CLOSE( integrandCellLTrue, integrandCellL.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandCellRTrue, integrandCellR.PDE, small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDEITrue, integrandPDEI, small_tol, close_tol );

  integrandAuxLTrue = 7.568870985820804;
  integrandAuyLTrue = 1.654629867976521;
  integrandAuxRTrue = 1.513774197164161;
  integrandAuyRTrue = 0.330925973595304;
  /*
   -7.568870985820804  -1.513774197164161
   -1.654629867976521   0.330925973595304
   */
  SANS_CHECK_CLOSE( integrandAuxLTrue, integrandCellL.Au[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuyLTrue, integrandCellL.Au[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuxRTrue, integrandCellR.Au[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuyRTrue, integrandCellR.Au[1], small_tol, close_tol );

  sRef = 0.5;
  fcn( sRef, &integrandCellL, 1,
             &integrandCellR, 1,
             &integrandPDEI, 1 );

  // 1.75*results from IntegrandInteriorTrace_HDG_AD_btest
  integrandCellLTrue = 2.*1.75*(( 91./(40.*sqrt(2))) + (-324*sqrt(2)/125.) + (0));
  integrandCellRTrue = 2.*1.75*((-91./(40*sqrt(2))) + (669./(1000*sqrt(2))) + (1059/(125.*sqrt(2))));
  integrandPDEITrue  = 2.*1.75*(( 0 ) + ( -903/(200.*sqrt(2)) )+(1059./(125.*sqrt(2))));

  SANS_CHECK_CLOSE( integrandCellLTrue, integrandCellL.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandCellRTrue, integrandCellR.PDE, small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDEITrue, integrandPDEI, small_tol, close_tol );

  integrandAuxLTrue = -0;
  integrandAuyLTrue = -0;
  integrandAuxRTrue = 3.027548394328322;
  integrandAuyRTrue = 0.661851947190609;
  /*
                   0  -3.027548394328322
                  -0  -0.661851947190609
   */
  SANS_CHECK_CLOSE( integrandAuxLTrue, integrandCellL.Au[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuyLTrue, integrandCellL.Au[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuxRTrue, integrandCellR.Au[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuyRTrue, integrandCellR.Au[1], small_tol, close_tol );

  sRef = 1.0;
  fcn( sRef, &integrandCellL, 1,
             &integrandCellR, 1,
             &integrandPDEI, 1 );

  // 1.5* results from IntegrandInteriorTrace_HDG_AD_btest
  integrandCellLTrue = 1.5*((13.*sqrt(2)/5.) + (2619./(250*sqrt(2))) + (1059/(25.*sqrt(2))));
  integrandCellRTrue = 1.5*(( 13./(10.*sqrt(2))) + (501./(250*sqrt(2))) + (3177/(125.*sqrt(2))));
  integrandPDEITrue  = 1.5*( ( 13/(2.*sqrt(2)) ) + ( (156*sqrt(2))/25. ) + ((4236.*sqrt(2))/125.) );

  SANS_CHECK_CLOSE( integrandCellLTrue, integrandCellL.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandCellRTrue, integrandCellR.PDE, small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDEITrue, integrandPDEI, small_tol, close_tol );

  integrandAuxLTrue = -7.568870985820805;
  integrandAuyLTrue = -1.654629867976521;
  integrandAuxRTrue = 4.541322591492483;
  integrandAuyRTrue = 0.992777920785913;
  /*
   7.568870985820805  -4.541322591492483
   1.654629867976521  -0.992777920785913
   */
  SANS_CHECK_CLOSE( integrandAuxLTrue, integrandCellL.Au[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuyLTrue, integrandCellL.Au[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuxRTrue, integrandCellR.Au[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuyRTrue, integrandCellR.Au[1], small_tol, close_tol );

}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
