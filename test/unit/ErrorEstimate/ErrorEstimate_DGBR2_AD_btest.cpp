// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEstimate_DGBR2_AD_btest
// testing of Error Estimate umbrella function for DG BR2 with Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
//#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
//#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
//#include "pde/NDConvert/PDENDConvertSpace3D.h"
//#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/Tuple/FieldTuple.h"

#include "Discretization/DG/DiscretizationDGBR2.h"
#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"

#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
//#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"

// Interior Integrands
#include "Discretization/DG/IntegrandCell_DGBR2.h"
#include "Discretization/DG/IntegrandInteriorTrace_DGBR2.h"

// Boundary Integrands
#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_LinearScalar_sansLG_DGBR2.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"

// Estimation classes
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2.h"
#include "ErrorEstimate/DG/ErrorEstimateCell_DGBR2.h"
#include "ErrorEstimate/DG/ErrorEstimateInteriorTrace_DGBR2.h"

#include "ErrorEstimate/DG/ErrorEstimateBoundaryTrace_DGBR2.h"
#include "ErrorEstimate/Galerkin/ErrorEstimateBoundaryTrace_FieldTrace_Galerkin.h"
#include "ErrorEstimate/Galerkin/ErrorEstimateBoundaryTrace_Galerkin.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEstimate_DGBR2_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimate_DGBR2_LinearScalar_mitLG_BC_2Line_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
          AdvectiveFlux1D_Uniform,
          ViscousFlux1D_Uniform,
          Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD1::D, ArrayQ> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandInteriorTraceClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandBoundaryTraceClass;

  // Algebraic Eq set things
  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_DGBR2<PDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Dense, DGBR2, XField<PhysD1, TopoD1>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef ErrorEstimate_DGBR2<PDEClass, BCNDConvertSpace, BCVector,XField<PhysD1,TopoD1>> ErrorEstimateClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 0.4, b = -0.3;
  Source1D_UniformGrad source(a, b);

  PDEClass pde( adv, visc, source );

  // BCs

  Real A = 1.0,B = 2.0,bcdata = 3.0; // Robin
  BCClass bc(A, B, bcdata);

  PyDict BCRobin;
  BCRobin[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BCRobin[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_mitLG>::params.A] = A;
  BCRobin[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_mitLG>::params.B] = B;
  BCRobin[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_mitLG>::params.bcdata] = bcdata;

  PyDict BCList;
  BCList["RobinL"] = BCRobin;
  BCList["RobinR"] = BCRobin;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  //Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["RobinL"] = {0};
  BCBoundaryGroups["RobinR"] = {1};

  // Check the BC dictionary
  BCParams::checkInputs(BCList);

  // grid: single triangle, P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 2, xfld.nElem() );

  // solution: single line, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution data (left)
  qfld.DOF(0) = 2;
  qfld.DOF(1) = 3;
  // solution data (right)
  qfld.DOF(2) = 7;
  qfld.DOF(3) = 4;

  BOOST_CHECK_EQUAL( 4, qfld.nDOF() );
  BOOST_CHECK( qfld.D == pde.D);

  // lifting operator: single line, P1 (aka Q1)
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  rfld.DOF(0) = -1;  rfld.DOF(1) = 5;
  rfld.DOF(2) =  3;  rfld.DOF(3) = 2;

  // lifting operators in left element
  rfld.DOF(0) =  3; //right edge
  rfld.DOF(1) =  2; //right edge
  rfld.DOF(2) = -1; //left edge
  rfld.DOF(3) =  5; //left edge

  // lifting operators in right element
  rfld.DOF(4) = -2; //right edge
  rfld.DOF(5) =  6; //right edge
  rfld.DOF(6) =  3; //left edge
  rfld.DOF(7) =  4; //left edge

  BOOST_CHECK_EQUAL( 8, rfld.nDOF() );
  BOOST_CHECK( rfld.D == pde.D);

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld(xfld, 0, BasisFunctionCategory_Legendre);

  // Lagrange Multiplier DOF data
  lgfld.DOF(0) =  5;
  lgfld.DOF(1) = -7;

  BOOST_CHECK_EQUAL(2, lgfld.nDOF() );

  // weight: single line, P2 (aka Q2)
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  // line solution data (left)
  wfld.DOF(0) = 3;
  wfld.DOF(1) = 4;
  wfld.DOF(2) = 5;

  // line solution data (right)
  wfld.DOF(3) =  5;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  BOOST_CHECK_EQUAL( 6, wfld.nDOF() );
  BOOST_CHECK( wfld.D == pde.D);

  // lifting operator: single line, P2 (aka Q2)
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> sfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  // lifting operators in left element
  sfld.DOF(0) = -5;  sfld.DOF(1) = 3;  sfld.DOF(2) = 2;
  sfld.DOF(3) =  2;  sfld.DOF(4) = 9;  sfld.DOF(5) = 3;

  // lifting operators in right element
  sfld.DOF(6)  = -2; //right edge
  sfld.DOF(7)  =  1; //right edge
  sfld.DOF(8)  =  4; //right edge
  sfld.DOF(9)  =  3; //left edge
  sfld.DOF(10) =  2; //left edge
  sfld.DOF(11) =  4; //left edge

  BOOST_CHECK_EQUAL( 12, sfld.nDOF() );
  BOOST_CHECK( sfld.D == pde.D);

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> mufld(xfld, 0, BasisFunctionCategory_Legendre);

  // Lagrange Multiplier DOF data
  mufld.DOF(0) =  3;
  mufld.DOF(1) =  2;

  BOOST_CHECK_EQUAL(2, mufld.nDOF() );

  // estimate fields
  Field_DG_Cell<PhysD1, TopoD1, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysD1, TopoD1, Real> eSfld(xfld, 0, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysD1, TopoD1, Real> ifld(xfld, 0, BasisFunctionCategory_Legendre);

  // solution data
  efld = 0; eSfld = 0; ifld = 0;

  BOOST_CHECK_EQUAL( 2, eSfld.nElem() );
  BOOST_CHECK_EQUAL( 2, eSfld.nDOF() );

  // lifting operator: single line, P0 (aka Q0)
  FieldLift_DG_Cell<PhysD1, TopoD1, Real> eLfld(xfld, 0, BasisFunctionCategory_Legendre);

  eLfld = 0;

  BOOST_CHECK_EQUAL( 2, eLfld.nElem() );
  BOOST_CHECK_EQUAL( 4, eLfld.nDOF() );
  BOOST_CHECK( eLfld.D == pde.D);

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD1, TopoD1, Real> eBfld(xfld, 0, BasisFunctionCategory_Legendre);

  // Lagrange Multiplier DOF data
  eBfld =  0;

  BOOST_CHECK_EQUAL(2, eBfld.nDOF() );

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // cubic rule
  int quadratureOrder = 3;
  int quadratureOrderMin[2]={0,0};

  // integrand
  IntegrandCellClass fcnCellint( pde, disc, {0} );
  IntegrandInteriorTraceClass fcnIntTraceint( pde, disc, {0} );
  IntegrandBoundaryTraceClass fcnBouTraceint( pde, bc, {0,1});

  IntegrateCellGroups<TopoD1>::integrate( ErrorEstimateCell_DGBR2(fcnCellint),
                                          xfld,
                                          (qfld, rfld, wfld, sfld, efld, eLfld),
                                          &quadratureOrder, 1 );

  IntegrateInteriorTraceGroups<TopoD1>::integrate(ErrorEstimateInteriorTrace_DGBR2(fcnIntTraceint),
                                                  xfld,
                                                  (qfld, rfld, wfld, sfld, efld, eLfld),
                                                  quadratureOrderMin, 1);

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ErrorEstimateBoundaryTrace_FieldTrace_Galerkin(fcnBouTraceint),
                                                              xfld,
                                                              (qfld,wfld,efld),
                                                              (lgfld,mufld,eBfld),
                                                              quadratureOrderMin, 2 );

  for (int i = 0; i < efld.nDOF(); i++)
  {
    eSfld.DOF(i) = efld.DOF(i);
    ifld.DOF(i) = fabs(efld.DOF(i));
  }

  // Distribute the eLfld to efld by looping over elements in 1 cell group
  typedef typename Field< PhysD1,TopoD1,Real   >::FieldCellGroupType<Line> EFieldCellGroupType;
  typedef typename FieldLift<PhysD1,TopoD1,Real>::FieldCellGroupType<Line> ELFieldCellGroupType;
  typedef typename EFieldCellGroupType::ElementType<> ElementEFieldClass;
  typedef ElementLift<Real, TopoD1, Line>          ElementELFieldClass;

  EFieldCellGroupType& eSfldCell = eSfld.getCellGroup<Line>(0);
  EFieldCellGroupType& ifldCell = ifld.getCellGroup<Line>(0);
  ELFieldCellGroupType& eLfldCell = eLfld.getCellGroup<Line>(0);

  ElementEFieldClass eSfldElem(eSfldCell.basis() );
  ElementEFieldClass ifldElem(ifldCell.basis() );
  ElementELFieldClass eLfldElem(eLfldCell.basis() );

  for (int elem = 0; elem <eSfldCell.nElem(); elem++)
  {
    eSfldCell.getElement(eSfldElem,elem);
    ifldCell.getElement(ifldElem,elem);
    eLfldCell.getElement(eLfldElem,elem);

    for (int trace=0;trace<Line::NTrace;trace++)
    {
       eSfldElem.DOF(0) += eLfldElem[trace].DOF(0);
       ifldElem.DOF(0)  += fabs(eLfldElem[trace].DOF(0));
    }
    eSfldCell.setElement(eSfldElem,elem);
    ifldCell.setElement(ifldElem,elem);
  }

  // Distribute the eBfld to efld by looping over elements of boundary trace groups

  // boundary terms
  typedef typename XField<PhysD1, TopoD1>::FieldTraceGroupType<Node> XFieldTraceGroupType;
  typedef typename Field<PhysD1,TopoD1,Real>::FieldTraceGroupType<Node> EBFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::ElementType<> ElementXFieldTraceClass;
  typedef typename EBFieldTraceGroupType::ElementType<> ElementEBFieldTraceClass;

  for (int group = 0; group< xfld.nBoundaryTraceGroups(); group++)
  {
    const XFieldTraceGroupType xfldBoundaryTrace = xfld.getBoundaryTraceGroup<Node>(group);
    ElementXFieldTraceClass xfldTraceElem(xfldBoundaryTrace.basis() );

    EBFieldTraceGroupType& eBfldTrace = eBfld.getBoundaryTraceGroup<Node>(group);
    ElementEBFieldTraceClass eBfldElem(eBfldTrace.basis() );

    // loop over elements in group
    for (int elem = 0; elem< xfldBoundaryTrace.nElem(); elem++)
    {
      int elemL = xfldBoundaryTrace.getElementLeft(elem);

      eSfldCell.getElement( eSfldElem, elemL );
      ifldCell. getElement( ifldElem, elemL );

      eBfldTrace.getElement( eBfldElem, elem );
      eSfldElem.DOF(0) += eBfldElem.DOF(0);
      ifldElem.DOF(0)  += fabs(eBfldElem.DOF(0));

      eSfldCell.setElement( eSfldElem, elemL );
      ifldCell. setElement( ifldElem, elemL );
    }
  }

  QuadratureOrder quadOrder(xfld, -1);

  // Construct ErrorEstimateClass
  ErrorEstimateClass ErrorEstimate(xfld,
                                   qfld, rfld, lgfld, wfld, sfld, mufld,
                                   pde, disc, quadOrder, {0}, {0},
                                   BCList, BCBoundaryGroups);
  const Field<PhysD1,TopoD1,Real>& efld2 = ErrorEstimate.getEField();
  const Field<PhysD1,TopoD1,Real>& eSfld2 = ErrorEstimate.getESField();
  const Field<PhysD1,TopoD1,Real>& ifld2 = ErrorEstimate.getIField();

  std::vector<std::vector<Real>> errorArray;
  ErrorEstimate.fillEArray(errorArray); // savithru's slightly more fragile interface

  BOOST_CHECK_EQUAL( 2, efld2.nElem() );
  BOOST_CHECK_EQUAL( 2, eSfld2.nElem() );
  BOOST_CHECK_EQUAL( 2, ifld2.nDOF() );

  Real globalEstimate=0,trueSum=0;
  Real globalAggregate=0,trueAgg=0;
  ErrorEstimate.getErrorEstimate(globalEstimate);
  ErrorEstimate.getErrorIndicator(globalAggregate);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int dof = 0; dof < efld.nDOF(); dof++)
  {
    SANS_CHECK_CLOSE( efld.DOF(dof), efld2.DOF(dof), small_tol, close_tol );
    SANS_CHECK_CLOSE( eSfld.DOF(dof), eSfld2.DOF(dof), small_tol, close_tol );
    SANS_CHECK_CLOSE( ifld.DOF(dof), ifld2.DOF(dof), small_tol, close_tol );
    trueSum += eSfld2.DOF(dof);
    trueAgg += fabs(ifld2.DOF(dof));
  }

  SANS_CHECK_CLOSE( trueSum, globalEstimate, small_tol, close_tol );
  SANS_CHECK_CLOSE( trueAgg, globalAggregate, small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimate_DGBR2_Dirichlet_mitState_sansLG_BC_2Line_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
      AdvectiveFlux1D_Uniform,
      ViscousFlux1D_Uniform,
      Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD1::D, ArrayQ> VectorArrayQ;

  typedef BCTypeDirichlet_mitState<AdvectiveFlux1D_Uniform,ViscousFlux1D_Uniform> BCTypeDirichlet_mitState1D;

  typedef BCNDConvertSpace<PhysD1,BCAdvectionDiffusion<PhysD1,BCTypeDirichlet_mitState1D>  >BCClass;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandInteriorTraceClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandBoundaryTraceClass;

  // Algebraic Eq set things
  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_DGBR2<PDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Dense, DGBR2, XField<PhysD1, TopoD1>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef ErrorEstimate_DGBR2<PDEClass, BCNDConvertSpace, BCVector,XField<PhysD1,TopoD1>> ErrorEstimateClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 0.4, b = -0.3;
  Source1D_UniformGrad source(a, b);

  PDEClass pde( adv, visc, source );

  // BC
  Real uB = 12./5.;
  BCClass bc(pde, uB);

  PyDict BCDirichlet;
  BCDirichlet[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichlet[BCAdvectionDiffusionParams<PhysD1, BCTypeDirichlet_mitStateParam>::params.qB] = uB;

  PyDict BCList;
  BCList["DirichletL"] = BCDirichlet;
  BCList["DirichletR"] = BCDirichlet;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  //Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["DirichletL"] = {0};
  BCBoundaryGroups["DirichletR"] = {1};

  // Check the BC dictionary
  BCParams::checkInputs(BCList);

  // grid: single triangle, P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 2, xfld.nElem() );

  // solution: single line, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution data (left)
  qfld.DOF(0) = 2;
  qfld.DOF(1) = 3;
  // solution data (right)
  qfld.DOF(2) = 7;
  qfld.DOF(3) = 4;

  BOOST_CHECK_EQUAL( 4, qfld.nDOF() );
  BOOST_CHECK( qfld.D == pde.D);

  // lifting operator: single line, P1 (aka Q1)
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  rfld.DOF(0) = -1;  rfld.DOF(1) = 5;
  rfld.DOF(2) =  3;  rfld.DOF(3) = 2;

  // lifting operators in left element
  rfld.DOF(0) =  3; //right edge
  rfld.DOF(1) =  2; //right edge
  rfld.DOF(2) = -1; //left edge
  rfld.DOF(3) =  5; //left edge

  // lifting operators in right element
  rfld.DOF(4) = -2; //right edge
  rfld.DOF(5) =  6; //right edge
  rfld.DOF(6) =  3; //left edge
  rfld.DOF(7) =  4; //left edge

  BOOST_CHECK_EQUAL( 8, rfld.nDOF() );
  BOOST_CHECK( rfld.D == pde.D);

  // weight: single line, P2 (aka Q2)
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  // line solution data (left)
  wfld.DOF(0) = 3;
  wfld.DOF(1) = 4;
  wfld.DOF(2) = 5;

  // line solution data (right)
  wfld.DOF(3) =  5;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  BOOST_CHECK_EQUAL( 6, wfld.nDOF() );
  BOOST_CHECK( wfld.D == pde.D);

  // lifting operator: single line, P2 (aka Q2)
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> sfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  // lifting operators in left element
  sfld.DOF(0) = -5;  sfld.DOF(1) = 3;  sfld.DOF(2) = 2;
  sfld.DOF(3) =  2;  sfld.DOF(4) = 9;  sfld.DOF(5) = 3;

  // lifting operators in right element
  sfld.DOF(6)  = -2; //right edge
  sfld.DOF(7)  =  1; //right edge
  sfld.DOF(8)  =  4; //right edge
  sfld.DOF(9)  =  3; //left edge
  sfld.DOF(10) =  2; //left edge
  sfld.DOF(11) =  4; //left edge

  BOOST_CHECK_EQUAL( 12, sfld.nDOF() );
  BOOST_CHECK( sfld.D == pde.D);

  // estimate fields
  Field_DG_Cell<PhysD1, TopoD1, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysD1, TopoD1, Real> eSfld(xfld, 0, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysD1, TopoD1, Real> ifld(xfld, 0, BasisFunctionCategory_Legendre);

  // solution data
  efld = 0; eSfld = 0; ifld = 0;

  BOOST_CHECK_EQUAL( 2, efld.nElem() );
  BOOST_CHECK_EQUAL( 2, efld.nDOF() );

  // lifting operator: single line, P0 (aka Q0)
  FieldLift_DG_Cell<PhysD1, TopoD1, Real> eLfld(xfld, 0, BasisFunctionCategory_Legendre);

  eLfld = 0;

  BOOST_CHECK_EQUAL( 2, eLfld.nElem() );
  BOOST_CHECK_EQUAL( 4, eLfld.nDOF() );
  BOOST_CHECK( eLfld.D == pde.D);

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // cubic rule
  int quadratureOrder = 3;
  int quadratureOrderMin[2]={0,0};

  // integrand
  IntegrandCellClass fcnCellint( pde, disc, {0} );
  IntegrandInteriorTraceClass fcnIntTraceint( pde, disc, {0} );
  IntegrandBoundaryTraceClass fcnBouTraceint( pde, bc, {0,1}, disc);

  IntegrateCellGroups<TopoD1>::integrate( ErrorEstimateCell_DGBR2(fcnCellint),
                                          xfld,
                                          (qfld, rfld, wfld, sfld, efld, eLfld),
                                          &quadratureOrder, 1 );

  IntegrateInteriorTraceGroups<TopoD1>::integrate(ErrorEstimateInteriorTrace_DGBR2(fcnIntTraceint),
                                                  xfld,
                                                  (qfld, rfld, wfld, sfld, efld, eLfld),
                                                  quadratureOrderMin, 1);

  IntegrateBoundaryTraceGroups<TopoD1>::integrate( ErrorEstimateBoundaryTrace_DGBR2(fcnBouTraceint),
                                                   xfld,
                                                   (qfld,rfld,wfld,sfld,efld,eLfld),
                                                   quadratureOrderMin, 2 );


  for (int i = 0; i < efld.nDOF(); i++)
  {
   eSfld.DOF(i) = efld.DOF(i);
   ifld.DOF(i) = fabs(efld.DOF(i));
  }

  // Distribute the eLfld to efld by looping over elements in 1 cell group
  typedef typename XField<PhysD1, TopoD1       >::FieldCellGroupType<Line> XFieldCellGroupType;
  typedef typename Field< PhysD1,TopoD1,Real   >::FieldCellGroupType<Line> EFieldCellGroupType;
  typedef typename FieldLift<PhysD1,TopoD1,Real>::FieldCellGroupType<Line> ELFieldCellGroupType;
  typedef typename XFieldCellGroupType::ElementType<> ElementXFieldClass;
  typedef typename EFieldCellGroupType::ElementType<> ElementEFieldClass;
  typedef ElementLift<Real, TopoD1, Line>          ElementELFieldClass;

  XFieldCellGroupType& xfldCell = xfld.getCellGroup<Line>(0);
  EFieldCellGroupType& eSfldCell = eSfld.getCellGroup<Line>(0);
  EFieldCellGroupType& ifldCell = ifld.getCellGroup<Line>(0);
  ELFieldCellGroupType& eLfldCell = eLfld.getCellGroup<Line>(0);

  ElementXFieldClass xfldElem(xfldCell.basis() );
  ElementEFieldClass eSfldElem(eSfldCell.basis() );
  ElementEFieldClass ifldElem(ifldCell.basis() );
  ElementELFieldClass eLfldElem(eLfldCell.basis() );

  for (int elem = 0; elem <xfldCell.nElem(); elem++)
  {
    eSfldCell.getElement(eSfldElem,elem);
    ifldCell.getElement(ifldElem,elem);
    eLfldCell.getElement(eLfldElem,elem);
    for (int trace=0;trace<Line::NTrace;trace++)
    {
      eSfldElem.DOF(0) += eLfldElem[trace].DOF(0);
      ifldElem.DOF(0) += fabs(eLfldElem[trace].DOF(0));
    }
    eSfldCell.setElement(eSfldElem,elem);
    ifldCell.setElement(ifldElem,elem);
  }

  // Needed to get past constructor, really want to get rid of
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> dumBfld1(xfld, 0, BasisFunctionCategory_Legendre,
                                                          BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups) );
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> dumBfld2(xfld, 0, BasisFunctionCategory_Legendre,
                                                          BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups));
  dumBfld1=0; dumBfld2=0;

  QuadratureOrder quadOrder(xfld, -1);

  // Construct ErrorEstimateClass
  ErrorEstimateClass ErrorEstimate(xfld,
                                   qfld, rfld, dumBfld1, wfld, sfld, dumBfld2,
                                   pde, disc, quadOrder, {0}, {0},
                                   BCList, BCBoundaryGroups);
  const Field<PhysD1,TopoD1,Real>& efld2 = ErrorEstimate.getEField();
  const Field<PhysD1,TopoD1,Real>& eSfld2 = ErrorEstimate.getESField();
  const Field<PhysD1,TopoD1,Real>& ifld2 = ErrorEstimate.getIField();

  std::vector<std::vector<Real>> errorArray;
  ErrorEstimate.fillEArray(errorArray); // savithru's slightly more fragile interface

  BOOST_CHECK_EQUAL( 2, efld2.nElem() );
  BOOST_CHECK_EQUAL( 2, eSfld2.nElem() );
  BOOST_CHECK_EQUAL( 2, ifld2.nDOF() );

  Real globalEstimate=0,trueSum=0;
  Real globalAggregate=0,trueAgg=0;
  ErrorEstimate.getErrorEstimate(globalEstimate);
  ErrorEstimate.getErrorIndicator(globalAggregate);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int dof = 0; dof < efld.nDOF(); dof++)
  {
    SANS_CHECK_CLOSE( efld.DOF(dof), efld2.DOF(dof), small_tol, close_tol );
    SANS_CHECK_CLOSE( eSfld.DOF(dof), eSfld2.DOF(dof), small_tol, close_tol );
    SANS_CHECK_CLOSE( ifld.DOF(dof), ifld2.DOF(dof), small_tol, close_tol );
    trueSum += eSfld2.DOF(dof);
    trueAgg += fabs(ifld2.DOF(dof));
  }

  SANS_CHECK_CLOSE( trueSum, globalEstimate, small_tol, close_tol );
  SANS_CHECK_CLOSE( trueAgg, globalAggregate, small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimate_DGBR2_LinearScalar_FieldTrace_BC_2Triangle_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
      AdvectiveFlux2D_Uniform,
      ViscousFlux2D_Uniform,
      Source2D_UniformGrad> PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD2::D, ArrayQ> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandInteriorTraceClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandBoundaryTraceClass;

  // Algebraic Eq set things
  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef AlgebraicEquationSet_DGBR2<PDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Dense, DGBR2, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef ErrorEstimate_DGBR2<PDEClass, BCNDConvertSpace, BCVector,XField<PhysD2,TopoD2>> ErrorEstimateClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Real a = 0.35, b = 1.82, c = -2.49;
  Source2D_UniformGrad source(a, b, c);

  PDEClass pde( adv, visc, source );

  // BCs
  Real A=2.1,B=3.1,bcdata=4.1;
  BCClass bc(A,B,bcdata);

  PyDict BCRobin;
  BCRobin[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BCRobin[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_mitLG>::params.A] = A;
  BCRobin[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_mitLG>::params.B] = B;
  BCRobin[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_mitLG>::params.bcdata] = bcdata;

  PyDict BCList;
  BCList["Boundary"] = BCRobin;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  BCBoundaryGroups["Boundary"] = {0};

  // Check the BC dictionary
  BCParams::checkInputs(BCList);

  // grid: single triangle, P1 (aka X1)
  XField2D_2Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 4, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 2, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;
  // triangle solution data (right)
  qfld.DOF(3) = 7;
  qfld.DOF(4) = 2;
  qfld.DOF(5) = 9;

  BOOST_CHECK_EQUAL( 6, qfld.nDOF() );

  // lifting operator: single triangle, P1 (aka Q1)
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // Left
  rfld.DOF(0) = { 2, -3};  rfld.DOF(1) = { 7,  8};  rfld.DOF(2) = {-1,  7};
  rfld.DOF(3) = { 9,  6};  rfld.DOF(4) = {-1,  3};  rfld.DOF(5) = { 2,  3};
  rfld.DOF(6) = {-2,  1};  rfld.DOF(7) = { 4, -4};  rfld.DOF(8) = {-9, -5};

  // Right
  rfld.DOF( 9) = { 9,  6};  rfld.DOF(10) = {-1,  3};  rfld.DOF(11) = { 2,  3};
  rfld.DOF(12) = {-2,  1};  rfld.DOF(13) = { 4, -4};  rfld.DOF(14) = {-9, -5};
  rfld.DOF(15) = { 2, -3};  rfld.DOF(16) = { 7,  8};  rfld.DOF(17) = {-1,  7};

  BOOST_CHECK_EQUAL( 18, rfld.nDOF() );
  BOOST_CHECK( rfld.D == pde.D);

  // Lagrange Multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  for (int i = 0; i<lgfld.nDOF();i++)
    lgfld.DOF(i) = i; // the values don't really matter

  BOOST_CHECK_EQUAL(8, lgfld.nDOF());

  // weight: single triangle, P2 (aka Q1)
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  // triangle solution data (left)
  wfld.DOF(0) = -2;
  wfld.DOF(1) =  4;
  wfld.DOF(2) =  3;
  wfld.DOF(3) =  2;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  // triangle solution data (right)
  wfld.DOF(6)  =  7;
  wfld.DOF(7)  =  3;
  wfld.DOF(8)  =  2;
  wfld.DOF(9)  = -1;
  wfld.DOF(10) =  5;
  wfld.DOF(11) = -3;

  // lifting operator: single triangle, P1 (aka Q1)
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> sfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  // Triangle auxilliary variables (left)
  sfld.DOF(0)  = { 1,  5};  sfld.DOF(1)  = { 3,  6};  sfld.DOF(2)  = {-1,  7};
  sfld.DOF(3)  = { 3,  1};  sfld.DOF(4)  = { 2,  2};  sfld.DOF(5)  = { 4,  3};

  sfld.DOF(6)  = { 5,  6};  sfld.DOF(7)  = { 4,  5};  sfld.DOF(8)  = { 3,  4};
  sfld.DOF(9)  = { 4,  3};  sfld.DOF(10) = { 3,  2};  sfld.DOF(11) = { 2,  1};

  sfld.DOF(12) = {-2, -1};  sfld.DOF(13) = {-3,  2};  sfld.DOF(14) = {-4, -3};
  sfld.DOF(15) = {-1, -3};  sfld.DOF(16) = {-2, -2};  sfld.DOF(17) = {-3, -1};

  // Triangle auxilliary variables (right)

  sfld.DOF(18) = { 5,  6};  sfld.DOF(19) = { 4,  5};  sfld.DOF(20) = { 3,  4};
  sfld.DOF(21) = { 4,  3};  sfld.DOF(22) = { 3,  2};  sfld.DOF(23) = { 2,  1};

  sfld.DOF(24) = {-2, -1};  sfld.DOF(25) = {-3,  2};  sfld.DOF(26) = {-4, -3};
  sfld.DOF(27) = {-1, -3};  sfld.DOF(28) = {-2, -2};  sfld.DOF(29) = {-3, -1};

  sfld.DOF(30) = { 1,  5};  sfld.DOF(31) = { 3,  6};  sfld.DOF(32) = {-1,  7};
  sfld.DOF(33) = { 3,  1};  sfld.DOF(34) = { 2,  2};  sfld.DOF(35) = { 4,  3};

  BOOST_CHECK_EQUAL(36, sfld.nDOF() );

  // Lagrange Multiplier
  Field_DG_BoundaryTrace<PhysD2,TopoD2,ArrayQ> mufld( xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  for (int i=0;i<mufld.nDOF();i++)
    mufld.DOF(i) = pow(-1,i)*i;

  BOOST_CHECK_EQUAL(12, mufld.nDOF() );

  // estimate fields
  Field_DG_Cell<PhysD2, TopoD2, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysD2, TopoD2, Real> eSfld(xfld, 0, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysD2, TopoD2, Real> ifld(xfld, 0, BasisFunctionCategory_Legendre);

  // solution data
  efld = 0; eSfld = 0; ifld = 0;

  BOOST_CHECK_EQUAL( 2, efld.nElem() );
  BOOST_CHECK_EQUAL( 2, efld.nDOF() );

  // lifting operator: single line, P2 (aka Q2)
  FieldLift_DG_Cell<PhysD2, TopoD2, Real> eLfld(xfld, 0, BasisFunctionCategory_Legendre);

  eLfld=0;

  BOOST_CHECK_EQUAL( 2, eLfld.nElem() );
  BOOST_CHECK_EQUAL( 6, eLfld.nDOF() );
  BOOST_CHECK( eLfld.D == pde.D);

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, Real> eBfld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK_EQUAL( 4, eBfld.nDOF() );

  // error estimate field
  eBfld = 0;

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  int quadratureOrder = 3;

  // integrand
  IntegrandCellClass fcnCellint( pde, disc, {0} );
  IntegrandInteriorTraceClass fcnIntTraceint( pde, disc, {0} );
  IntegrandBoundaryTraceClass fcnBouTraceint( pde, bc, {0} );

  IntegrateCellGroups<TopoD2>::integrate( ErrorEstimateCell_DGBR2(fcnCellint),
                                          xfld,
                                          (qfld, rfld, wfld, sfld, efld, eLfld),
                                          &quadratureOrder, 1 );

  IntegrateInteriorTraceGroups<TopoD2>::integrate(ErrorEstimateInteriorTrace_DGBR2(fcnIntTraceint),
                                                  xfld,
                                                  (qfld, rfld, wfld, sfld, efld, eLfld),
                                                  &quadratureOrder, 1);

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ErrorEstimateBoundaryTrace_FieldTrace_Galerkin(fcnBouTraceint),
                                                              xfld,
                                                              (qfld,wfld,efld),
                                                              (lgfld,mufld,eBfld),
                                                              &quadratureOrder, 1 );

  for (int i = 0; i < efld.nDOF(); i++)
  {
    eSfld.DOF(i) = efld.DOF(i);
    ifld.DOF(i) = fabs(efld.DOF(i));
  }

  // Distribute the eLfld to efld by looping over elements in 1 cell group
  typedef typename XField<PhysD2, TopoD2       >::FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef typename Field< PhysD2,TopoD2,Real   >::FieldCellGroupType<Triangle> EFieldCellGroupType;
  typedef typename FieldLift<PhysD2,TopoD2,Real>::FieldCellGroupType<Triangle> ELFieldCellGroupType;
  typedef typename XFieldCellGroupType::ElementType<> ElementXFieldClass;
  typedef typename EFieldCellGroupType::ElementType<> ElementEFieldClass;
  typedef ElementLift<Real, TopoD2, Triangle>      ElementELFieldClass;

  XFieldCellGroupType& xfldCell = xfld.getCellGroup<Triangle>(0);
  EFieldCellGroupType& eSfldCell = eSfld.getCellGroup<Triangle>(0);
  EFieldCellGroupType& ifldCell = ifld.getCellGroup<Triangle>(0);
  ELFieldCellGroupType& eLfldCell = eLfld.getCellGroup<Triangle>(0);

  ElementXFieldClass xfldElem(xfldCell.basis() );
  ElementEFieldClass eSfldElem(eSfldCell.basis() );
  ElementEFieldClass ifldElem(ifldCell.basis() );
  ElementELFieldClass eLfldElem(eLfldCell.basis() );

  for (int elem = 0; elem <xfldCell.nElem(); elem++)
  {
    eSfldCell.getElement(eSfldElem,elem);
    ifldCell.getElement(ifldElem,elem);
    eLfldCell.getElement(eLfldElem,elem);
    for (int trace=0;trace<Triangle::NTrace;trace++)
    {
      eSfldElem.DOF(0) += eLfldElem[trace].DOF(0);
      ifldElem.DOF(0) += fabs(eLfldElem[trace].DOF(0));
    }
    eSfldCell.setElement(eSfldElem,elem);
    ifldCell.setElement(ifldElem,elem);
  }


  // Distribute the eBfld to efld by looping over elements of boundary trace groups

  // boundary terms
  typedef typename XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>      XFieldTraceGroupType;
  typedef typename Field<PhysD2, TopoD2, Real>::FieldTraceGroupType<Line> EBFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::ElementType<>                    ElementXFieldTraceClass;
  typedef typename EBFieldTraceGroupType::ElementType<>                   ElementEBFieldTraceClass;

  for (int group = 0; group< xfld.nBoundaryTraceGroups(); group++)
  {
    const XFieldTraceGroupType xfldBoundaryTrace = xfld.getBoundaryTraceGroup<Line>(group);
    ElementXFieldTraceClass xfldTraceElem(xfldBoundaryTrace.basis() );

    EBFieldTraceGroupType& eBfldTrace = eBfld.getBoundaryTraceGroup<Line>(group);
    ElementEBFieldTraceClass eBfldElem(eBfldTrace.basis() );

    // loop over elements in group
    for (int elem = 0; elem< xfldBoundaryTrace.nElem(); elem++)
    {
      int elemL = xfldBoundaryTrace.getElementLeft(elem);

      eSfldCell.getElement( eSfldElem, elemL );
      ifldCell.getElement( ifldElem, elemL );
      eBfldTrace.getElement( eBfldElem, elem );
      eSfldElem.DOF(0) +=  eBfldElem.DOF(0);
      ifldElem.DOF(0) +=  fabs(eBfldElem.DOF(0));
      eSfldCell.setElement( eSfldElem, elemL );
      ifldCell.setElement( ifldElem, elemL );
    }
  }

  QuadratureOrder quadOrder(xfld, -1);

  // Construct ErrorEstimateClass
  ErrorEstimateClass ErrorEstimate(xfld,
                                   qfld, rfld, lgfld, wfld, sfld, mufld,
                                   pde, disc, quadOrder, {0}, {0},
                                   BCList, BCBoundaryGroups);

  const Field<PhysD2,TopoD2,Real>& efld2 = ErrorEstimate.getEField();
  const Field<PhysD2,TopoD2,Real>& eSfld2 = ErrorEstimate.getESField();
  const Field<PhysD2,TopoD2,Real>& ifld2 = ErrorEstimate.getIField();

  std::vector<std::vector<Real>> errorArray;
  ErrorEstimate.fillEArray(errorArray); // savithru's slightly more fragile interface

  BOOST_CHECK_EQUAL( 2, efld2.nElem() );
  BOOST_CHECK_EQUAL( 2, efld2.nDOF() );

  Real globalEstimate=0,trueSum=0;
  Real globalAggregate=0,trueAgg=0;
  ErrorEstimate.getErrorEstimate(globalEstimate);
  ErrorEstimate.getErrorIndicator(globalAggregate);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int dof = 0; dof < efld.nDOF(); dof++)
  {
    SANS_CHECK_CLOSE( efld.DOF(dof), efld2.DOF(dof), small_tol, close_tol );
    SANS_CHECK_CLOSE( eSfld.DOF(dof), eSfld2.DOF(dof), small_tol, close_tol );
    SANS_CHECK_CLOSE( ifld.DOF(dof), ifld2.DOF(dof), small_tol, close_tol );
    trueSum += eSfld2.DOF(dof);
    trueAgg += fabs(ifld2.DOF(dof));
  }

  SANS_CHECK_CLOSE( trueSum, globalEstimate, small_tol, close_tol );
  SANS_CHECK_CLOSE( trueAgg, globalAggregate, small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimate_DGBR2_Dirichlet_mitState_sansLG_BC_2Triangle_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad> PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD2::D, ArrayQ> VectorArrayQ;

  typedef BCTypeDirichlet_mitState<AdvectiveFlux2D_Uniform,ViscousFlux2D_Uniform> BCTypeDirichlet_mitState2D;

  typedef BCNDConvertSpace<PhysD2,BCAdvectionDiffusion<PhysD2,BCTypeDirichlet_mitState2D> >BCClass;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandInteriorTraceClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandBoundaryTraceClass;

  // Algebraic Eq set things
  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef AlgebraicEquationSet_DGBR2<PDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Dense, DGBR2, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef ErrorEstimate_DGBR2<PDEClass, BCNDConvertSpace, BCVector,XField<PhysD2,TopoD2>> ErrorEstimateClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Real a = 0.35, b = 1.82, c = -2.49;
  Source2D_UniformGrad source(a, b, c);

  PDEClass pde( adv, visc, source );

  // BC
  Real uB = 12./5.;
  BCClass bc(pde, uB);

  PyDict BCDirichlet;
  BCDirichlet[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichlet[BCAdvectionDiffusionParams<PhysD1, BCTypeDirichlet_mitStateParam>::params.qB] = uB;

  PyDict BCList;
  BCList["Boundary"] = BCDirichlet;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  BCBoundaryGroups["Boundary"] = {0};

  // Check the BC dictionary
  BCParams::checkInputs(BCList);

  // grid: single triangle, P1 (aka X1)
  XField2D_2Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 4, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 2, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;
  // triangle solution data (right)
  qfld.DOF(3) = 7;
  qfld.DOF(4) = 2;
  qfld.DOF(5) = 9;

  BOOST_CHECK_EQUAL( 6, qfld.nDOF() );

  // lifting operator: single triangle, P1 (aka Q1)
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // Left
  rfld.DOF(0) = { 2, -3};  rfld.DOF(1) = { 7,  8};  rfld.DOF(2) = {-1,  7};
  rfld.DOF(3) = { 9,  6};  rfld.DOF(4) = {-1,  3};  rfld.DOF(5) = { 2,  3};
  rfld.DOF(6) = {-2,  1};  rfld.DOF(7) = { 4, -4};  rfld.DOF(8) = {-9, -5};

  // Right
  rfld.DOF( 9) = { 9,  6};  rfld.DOF(10) = {-1,  3};  rfld.DOF(11) = { 2,  3};
  rfld.DOF(12) = {-2,  1};  rfld.DOF(13) = { 4, -4};  rfld.DOF(14) = {-9, -5};
  rfld.DOF(15) = { 2, -3};  rfld.DOF(16) = { 7,  8};  rfld.DOF(17) = {-1,  7};

  BOOST_CHECK_EQUAL( 18, rfld.nDOF() );
  BOOST_CHECK( rfld.D == pde.D);

  // weight: single triangle, P2 (aka Q1)
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  // triangle solution data (left)
  wfld.DOF(0) = -2;
  wfld.DOF(1) =  4;
  wfld.DOF(2) =  3;
  wfld.DOF(3) =  2;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  // triangle solution data (right)
  wfld.DOF(6)  =  7;
  wfld.DOF(7)  =  3;
  wfld.DOF(8)  =  2;
  wfld.DOF(9)  = -1;
  wfld.DOF(10) =  5;
  wfld.DOF(11) = -3;

  // lifting operator: single triangle, P1 (aka Q1)
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> sfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  // Triangle auxilliary variables (left)
  sfld.DOF(0)  = { 1,  5};  sfld.DOF(1)  = { 3,  6};  sfld.DOF(2)  = {-1,  7};
  sfld.DOF(3)  = { 3,  1};  sfld.DOF(4)  = { 2,  2};  sfld.DOF(5)  = { 4,  3};

  sfld.DOF(6)  = { 5,  6};  sfld.DOF(7)  = { 4,  5};  sfld.DOF(8)  = { 3,  4};
  sfld.DOF(9)  = { 4,  3};  sfld.DOF(10) = { 3,  2};  sfld.DOF(11) = { 2,  1};

  sfld.DOF(12) = {-2, -1};  sfld.DOF(13) = {-3,  2};  sfld.DOF(14) = {-4, -3};
  sfld.DOF(15) = {-1, -3};  sfld.DOF(16) = {-2, -2};  sfld.DOF(17) = {-3, -1};

  // Triangle auxilliary variables (right)

  sfld.DOF(18) = { 5,  6};  sfld.DOF(19) = { 4,  5};  sfld.DOF(20) = { 3,  4};
  sfld.DOF(21) = { 4,  3};  sfld.DOF(22) = { 3,  2};  sfld.DOF(23) = { 2,  1};

  sfld.DOF(24) = {-2, -1};  sfld.DOF(25) = {-3,  2};  sfld.DOF(26) = {-4, -3};
  sfld.DOF(27) = {-1, -3};  sfld.DOF(28) = {-2, -2};  sfld.DOF(29) = {-3, -1};

  sfld.DOF(30) = { 1,  5};  sfld.DOF(31) = { 3,  6};  sfld.DOF(32) = {-1,  7};
  sfld.DOF(33) = { 3,  1};  sfld.DOF(34) = { 2,  2};  sfld.DOF(35) = { 4,  3};

  BOOST_CHECK_EQUAL(36, sfld.nDOF() );



  // estimate fields
  Field_DG_Cell<PhysD2, TopoD2, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysD2, TopoD2, Real> eSfld(xfld, 0, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysD2, TopoD2, Real> ifld(xfld, 0, BasisFunctionCategory_Legendre);

  // solution data
  efld = 0; eSfld = 0; ifld = 0;

  BOOST_CHECK_EQUAL( 2, efld.nElem() );
  BOOST_CHECK_EQUAL( 2, efld.nDOF() );

  // lifting operator: single line, P2 (aka Q2)
  FieldLift_DG_Cell<PhysD2, TopoD2, Real> eLfld(xfld, 0, BasisFunctionCategory_Legendre);

  eLfld=0;

  BOOST_CHECK_EQUAL( 2, eLfld.nElem() );
  BOOST_CHECK_EQUAL( 6, eLfld.nDOF() );
  BOOST_CHECK( eLfld.D == pde.D);

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // Field Bundles for the Alternative Constructor

  FieldBundle_DGBR2<PhysD2,TopoD2,ArrayQ> primal(xfld, qorder, BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical,
                                                 BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups) );

  BOOST_CHECK_EQUAL( primal.qfld.nDOF(), qfld.nDOF() );
  BOOST_CHECK_EQUAL( primal.rfld.nDOF(), rfld.nDOF() );
  for (int i = 0; i < primal.qfld.nDOF(); i++)
    primal.qfld.DOF(i) = qfld.DOF(i);
  for (int i = 0; i < primal.rfld.nDOF(); i++)
    primal.rfld.DOF(i) = rfld.DOF(i);
  primal.lgfld = 0;

  FieldBundle_DGBR2<PhysD2,TopoD2,ArrayQ> adjoint(xfld, qorder+1, BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical,
                                                  BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups) );

  BOOST_CHECK_EQUAL( adjoint.qfld.nDOF(), wfld.nDOF() );
  BOOST_CHECK_EQUAL( adjoint.rfld.nDOF(), sfld.nDOF() );
  for (int i = 0; i < adjoint.qfld.nDOF(); i++)
    adjoint.qfld.DOF(i) = wfld.DOF(i);
  for (int i = 0; i < adjoint.rfld.nDOF(); i++)
    adjoint.rfld.DOF(i) = sfld.DOF(i);
  adjoint.lgfld = 0;

  int quadratureOrder = 3;

  // integrand
  IntegrandCellClass fcnCellint( pde, disc, {0} );
  IntegrandInteriorTraceClass fcnIntTraceint( pde, disc, {0} );
  IntegrandBoundaryTraceClass fcnBouTraceint( pde, bc, {0}, disc);

  IntegrateCellGroups<TopoD2>::integrate( ErrorEstimateCell_DGBR2(fcnCellint),
                                          xfld,
                                          (qfld, rfld, wfld, sfld, efld, eLfld),
                                          &quadratureOrder, 1 );

  IntegrateInteriorTraceGroups<TopoD2>::integrate(ErrorEstimateInteriorTrace_DGBR2(fcnIntTraceint),
                                                  xfld,
                                                  (qfld, rfld, wfld, sfld, efld, eLfld),
                                                  &quadratureOrder, 1);

  IntegrateBoundaryTraceGroups<TopoD2>::integrate( ErrorEstimateBoundaryTrace_DGBR2(fcnBouTraceint),
                                                         xfld,
                                                         (qfld,rfld,wfld,sfld,efld,eLfld),
                                                         &quadratureOrder, 1 );
  // hardcoded distribution
  for (int i = 0; i < efld.nDOF(); i++)
  {
    eSfld.DOF(i) = efld.DOF(i);
    ifld.DOF(i) = fabs(efld.DOF(i));
  }


  // Distribute the eLfld to efld by looping over elements in 1 cell group
  typedef typename XField<PhysD2, TopoD2       >::FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef typename Field< PhysD2,TopoD2,Real   >::FieldCellGroupType<Triangle> EFieldCellGroupType;
  typedef typename FieldLift<PhysD2,TopoD2,Real>::FieldCellGroupType<Triangle> ELFieldCellGroupType;
  typedef typename XFieldCellGroupType::ElementType<> ElementXFieldClass;
  typedef typename EFieldCellGroupType::ElementType<> ElementEFieldClass;
  typedef ElementLift<Real, TopoD2, Triangle>      ElementELFieldClass;

  XFieldCellGroupType& xfldCell = xfld.getCellGroup<Triangle>(0);
  EFieldCellGroupType& eSfldCell = eSfld.getCellGroup<Triangle>(0);
  EFieldCellGroupType& ifldCell = ifld.getCellGroup<Triangle>(0);
  ELFieldCellGroupType& eLfldCell = eLfld.getCellGroup<Triangle>(0);

  ElementXFieldClass xfldElem(xfldCell.basis() );
  ElementEFieldClass eSfldElem(eSfldCell.basis() );
  ElementEFieldClass ifldElem(ifldCell.basis() );
  ElementELFieldClass eLfldElem(eLfldCell.basis() );

  for (int elem = 0; elem <xfldCell.nElem(); elem++)
  {
    eSfldCell.getElement(eSfldElem,elem);
    ifldCell.getElement(ifldElem,elem);
    eLfldCell.getElement(eLfldElem,elem);
    for (int trace=0;trace<Triangle::NTrace;trace++)
    {
      eSfldElem.DOF(0) += eLfldElem[trace].DOF(0);
      ifldElem.DOF(0) += fabs(eLfldElem[trace].DOF(0));
    }
    eSfldCell.setElement(eSfldElem,elem);
    ifldCell.setElement(ifldElem,elem);
  }

  // Needed to get past constructor, really want to get rid of
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> dumBfld1(xfld, 0, BasisFunctionCategory_Legendre,
                                                          BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups) );
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> dumBfld2(xfld, 0, BasisFunctionCategory_Legendre,
                                                          BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups));
  dumBfld1=0; dumBfld2=0;


  QuadratureOrder quadOrder(xfld, -1);

  // Construct ErrorEstimateClass
  ErrorEstimateClass ErrorEstimate(xfld,
                                   qfld, rfld, dumBfld1, wfld, sfld, dumBfld2,
                                   pde, disc, quadOrder, {0}, {0},
                                   BCList, BCBoundaryGroups);

  // Construct ErrorEstimateClass
  ErrorEstimateClass ErrorEstimate_alt( xfld, primal, adjoint, {},
                                        pde, disc, quadOrder, {0}, {0},
                                        BCList, BCBoundaryGroups);

  const Field<PhysD2,TopoD2,Real>& efld2 = ErrorEstimate.getEField();
  const Field<PhysD2,TopoD2,Real>& eSfld2 = ErrorEstimate.getESField();
  const Field<PhysD2,TopoD2,Real>& ifld2 = ErrorEstimate.getIField();

  const Field<PhysD2,TopoD2,Real>& efld2_alt = ErrorEstimate_alt.getEField();
  const Field<PhysD2,TopoD2,Real>& eSfld2_alt = ErrorEstimate_alt.getESField();
  const Field<PhysD2,TopoD2,Real>& ifld2_alt = ErrorEstimate_alt.getIField();

  std::vector<std::vector<Real>> errorArray;
  ErrorEstimate.fillEArray(errorArray); // savithru's slightly more fragile interface

  BOOST_CHECK_EQUAL( 2, efld2.nElem() );
  BOOST_CHECK_EQUAL( 2, efld2.nDOF() );

  Real globalEstimate=0,trueSum=0;
  Real globalAggregate=0,trueAgg=0;
  ErrorEstimate.getErrorEstimate(globalEstimate);
  ErrorEstimate.getErrorIndicator(globalAggregate);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int dof = 0; dof < efld.nDOF(); dof++)
  {
    // Vanila constructor
    SANS_CHECK_CLOSE( efld.DOF(dof), efld2.DOF(dof), small_tol, close_tol );
    SANS_CHECK_CLOSE( eSfld.DOF(dof), eSfld2.DOF(dof), small_tol, close_tol );
    SANS_CHECK_CLOSE( ifld.DOF(dof), ifld2.DOF(dof), small_tol, close_tol );

    // Agnostic constructor
    SANS_CHECK_CLOSE( efld.DOF(dof), efld2_alt.DOF(dof), small_tol, close_tol );
    SANS_CHECK_CLOSE( eSfld.DOF(dof), eSfld2_alt.DOF(dof), small_tol, close_tol );
    SANS_CHECK_CLOSE( ifld.DOF(dof), ifld2_alt.DOF(dof), small_tol, close_tol );

    trueSum += eSfld2.DOF(dof);
    trueAgg += fabs(ifld2.DOF(dof));
  }

  SANS_CHECK_CLOSE( trueSum, globalEstimate, small_tol, close_tol );
  SANS_CHECK_CLOSE( trueAgg, globalAggregate, small_tol, close_tol );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
