// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEstimateCell_DGBR2_LiftedQuantity_TwoPhaseAV_btest
// testing of residual functions for DG BR2 with lifted quantity sources - two-phase

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/PorousMedia/Q2DPrimitive_pnSw.h"
#include "pde/PorousMedia/TraitsTwoPhaseArtificialViscosity.h"
#include "pde/PorousMedia/PDETwoPhase_ArtificialViscosity2D.h"
#include "pde/PorousMedia/BCTwoPhaseArtificialViscosity2D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/PermeabilityModel2D.h"
#include "pde/PorousMedia/CapillaryModel.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/HField/GenHFieldArea_CG.h"

#include "Field/Tuple/FieldTuple.h"

#include "Discretization/DG/IntegrandCell_DGBR2.h"
#include "Discretization/DG/DiscretizationDGBR2.h"
#include "Discretization/IntegrateCellGroups.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "ErrorEstimate/DG/ErrorEstimateCell_DGBR2.h"

#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEstimateCell_DGBR2_LiftedQuantity_TwoPhaseAV_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateCell_DGBR2_LiftedQuantity_1Triangle_X1Q1 )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef PermeabilityModel2D_QuadBlock RockPermModel;
  typedef QTypePrimitive_pnSw QType;

  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel,
                              CapillaryModel> TraitsModelTwoPhaseClass;

  typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity,
                                            TraitsModelTwoPhaseClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeTwoPhaseArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeTwoPhaseArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_TwoPhase<PDEBaseClass> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandCell_DGBR2<NDPDEClass> IntegrandClass;

  int Ntrace = 3;
  int N = 3;
  int nDOF = 4;

  // PDE
  const Real pref = 14.7;

  DensityModel rhow(62.4, 5.0e-6, pref);
  DensityModel rhon(52.1, 1.5e-5, pref);

  PorosityModel phi(0.3, 3.0e-6, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  const Real ft3_per_bbl = 5.61458;
  const Real conversion = (1.127e-3)*ft3_per_bbl; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real Kref = conversion*200; //mD

  DLA::MatrixS<2,2,Real> Kref_mat = {{Kref, 0.0}, {0.0, Kref}};

  RockPermModel K(Kref_mat);

  CapillaryModel pc(0.0);

  // TwoPhase PDE with AV
  int order = 1; //hard-coded to 1 so that the artificial viscosity is added instead of being just zero
  bool hasSpaceTimeDiffusion = false;
  PDEBaseClass pdeTwoPhaseAV(order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

//  Sensor sensor(pdeTwoPhaseAV);
  SensorAdvectiveFlux sensor_adv(0.0, 0.0);
  SensorViscousFlux sensor_visc(3.0);
  SensorSource sensor_source(pdeTwoPhaseAV);

  // AV PDE with sensor equation
  bool isSteady = false;
  NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteady,
                 order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == N );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid
  XField2D_4Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 6, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 4, xfld.nElem() );

  //Compute generalized log H-tensor field
  GenHField_CG<PhysD2, TopoD2> Hfld(xfld);

  // solution
  int qorder = 0;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  //P0 solutions in each cell
  Real pn[4] = {2500.0, 2520.0, 2513.0, 2527.0};
  Real Sw[4] = {0.26, 0.30, 0.20, 0.47};
  Real nu[4] = {0.15, 0.04, 0.23, 0.36};

  // triangle solution data
  for (int i = 0; i < 4; i++)
    qfld.DOF(i) = {pn[i], Sw[i], nu[i]};

  BOOST_CHECK_EQUAL( nDOF, qfld.nDOF() );

  // lifting operator: single triangle, P1 (aka Q1)
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Legendre);

  rfld.DOF(0) = { 2, -3};  rfld.DOF(1) = { 7,  8};  rfld.DOF(2) = {-1,  7};
  rfld.DOF(3) = { 9,  6};  rfld.DOF(4) = {-1,  3};  rfld.DOF(5) = { 2,  3};
  rfld.DOF(6) = {-2,  1};  rfld.DOF(7) = { 4, -4};  rfld.DOF(8) = {-9, -5};
  rfld.DOF(9) = { 3,  4};  rfld.DOF(10) = { 1, 2};  rfld.DOF(11) = {3,  6};

  BOOST_CHECK_EQUAL( nDOF*Ntrace, rfld.nDOF() );
  BOOST_CHECK( rfld.D == pde.D);

  //lifted quantity field
  Field_DG_Cell<PhysD2, TopoD2, Real> lqfld(xfld, qorder, BasisFunctionCategory_Legendre);

  Real lifted_quantity[4] = {0.21, 0.35, 0.07, 0.16};

  for (int i = 0; i < 4; i++)
    lqfld.DOF(i) = lifted_quantity[i];


  // weight
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, qorder, BasisFunctionCategory_Legendre);

  Real w_wet[4] = {0.1, 0.2, 0.3, 0.4};
  Real w_nonwet[4] = {1.6, 1.7, 1.8, 1.9};
  Real w_artvisc[4] = {0.3, -0.4, 0.2, 0.7};

  // triangle solution
  for (int i = 0; i < 4; i++)
    wfld.DOF(i) = {w_wet[i], w_nonwet[i], w_artvisc[i]};

  // lifting operator weight
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> sfld(xfld, qorder, BasisFunctionCategory_Legendre);

  sfld.DOF(0) = { 1, -2};  sfld.DOF(1) = { 7,  9};  sfld.DOF(2) = {-3,  7};
  sfld.DOF(3) = { 7,  3};  sfld.DOF(4) = {-1,  2};  sfld.DOF(5) = { 2,  4};
  sfld.DOF(6) = { 5,  2};  sfld.DOF(7) = { 5, -4};  sfld.DOF(8) = {-8, -5};
  sfld.DOF(9) = { 9,  4};  sfld.DOF(10) = { 1, 6};  sfld.DOF(11) = {3,  0};

  // estimate fields
  Field_DG_Cell<PhysD2, TopoD2, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);

  // solution data
  efld = 0;

  BOOST_CHECK_EQUAL( 4, efld.nElem() );
  BOOST_CHECK_EQUAL( 4, efld.nDOF() );

  // lifting operator: single line, P2 (aka Q2)
  FieldLift_DG_Cell<PhysD2, TopoD2, Real> eLfld(xfld, 0, BasisFunctionCategory_Legendre);

  eLfld = 0;

  BOOST_CHECK_EQUAL( 4, eLfld.nElem() );
  BOOST_CHECK_EQUAL( 4*Ntrace, eLfld.nDOF() );
  BOOST_CHECK( eLfld.D == pde.D);

  Real triArea = 0.5;
  DLA::MatrixSymS<PhysD2::D,Real> H = Hfld.DOF(0); //Mesh is isotropic, so all DOFs have same H-tensor
  Real x = 0, y = 0, time = 0;
  VectorArrayQ gradq = 0; //P0 solution has zero gradients

  ArrayQ rsdPDETrueCell; //cell residual
  Real eTrue[4]; //error estimate for each cell

  for (int cell = 0; cell < 4; cell++)
  {
    Real Sk = log10(lifted_quantity[cell] + 1e-16);
    Real Sk_min = -4;
    Real Sk_max = -1;
    Real xi = (Sk - Sk_min)/(Sk_max - Sk_min);

//    Real sensor = smoothActivation_tanh(xi);
    Real alpha = 10;
    Real sensor = smoothActivation_exp(xi, alpha);

    VectorArrayQ R = 0; //sum of lifting operators on each cell

    for (int trace = 0; trace < Ntrace; trace++)
      R += rfld.DOF(Ntrace*cell + trace);

    VectorArrayQ gradq_lifted = gradq + R;

    Real nu_max;
    pdeTwoPhaseAV.artViscMax(H, x, y, time, qfld.DOF(cell), gradq_lifted[0], gradq_lifted[1], nu_max);

    //PDE residual: (source)  (advective + diffusion residuals are zero since gradq = 0)
    rsdPDETrueCell[0] = 0.0; //Basis function 1
    rsdPDETrueCell[1] = 0.0; //Basis function 2
    rsdPDETrueCell[2] = (nu[cell] - sensor*nu_max)*triArea; //Basis function 3

    eTrue[cell] = dot(rsdPDETrueCell, wfld.DOF(cell));
  }

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  int quadratureOrder = 3;

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  IntegrateCellGroups<TopoD2>::integrate( ErrorEstimateCell_DGBR2(fcnint), (Hfld, xfld),
                                          (qfld, rfld, wfld, sfld, lqfld, efld, eLfld), &quadratureOrder, 1 );

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-10;

  SANS_CHECK_CLOSE( eTrue[0], efld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( eTrue[1], efld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( eTrue[2], efld.DOF(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( eTrue[3], efld.DOF(3), small_tol, close_tol );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
