// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AWResiDualDLine_HDG_Triangle_AD_btest
// testing of 2-D line residual functions for HDG with Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "Discretization/HDG/AWIntegrandFunctor_HDG.h"
#include "Discretization/HDG/AWResidualInteriorTrace_HDG.h"
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( AWResidualInteriorTrace_HDG_AD_test_suite )

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AWResidualInteriorTrace_1D_HDG_2Line_X1Q1R1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef SolnNDConvertSpace<PhysD1, ScalarFunction1D_Const> WeightFunction;
  typedef AWIntegrandTrace_HDG<PDEClass,WeightFunction> IntegrandClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: 2 lines @ P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> ufld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // line solution data (left)
  ufld.DOF(0) = 1;
  ufld.DOF(1) = 4;

  // line solution data (right)
  ufld.DOF(2) = 7;
  ufld.DOF(3) = 9;

  // auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // line auxiliary variable (left)
  qfld.DOF(0) = { 2};
  qfld.DOF(1) = { 7};

  // line auxiliary variable (right)
  qfld.DOF(2) = { 9};
  qfld.DOF(3) = {-1};

  // interface solution: P1 (aka Q1)
  Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> uIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_REQUIRE_EQUAL( uIfld.nInteriorTraceGroups(), 1 );

  // node trace data
  uIfld.DOF(0) =  8;

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde , Local);

  // Using the Analytical Weight
  // Weight Function
  Real a0=1.1; // avoiding 1.0
  WeightFunction wfcn(a0);

  // integrand
  IntegrandClass fcnint( pde, wfcn, disc );

  // quadrature rule: quadratic (basis & flux both linear)
  int quadratureorder = 0;

  // PDE residuals (left): (advective) + (viscous) + (stabilization)
  Real LPDETrue = a0*(( 44./10.) + (-14861./1000.) + (-2123./250.) );
  Real RPDETrue = a0*((-44./5.) + ( 19107./1000.) + (-2123./1000.));

  // interface residuals
  Real IPDETrue = LPDETrue + RPDETrue;

  // auxiliary variable defn (left)
  Real AuxLTrue = 0.;
  Real AuxRTrue = 0.;

  // PDE, interface, aux var defn global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(2);
  SLA::SparseVector<ArrayQ> rsdAuGlobal(2);
  SLA::SparseVector<ArrayQ> rsdIntGlobal(1);

  rsdPDEGlobal = 0;
  rsdAuGlobal = 0;
  rsdIntGlobal = 0;

  // topology-specific single group interface

  AWResidualInteriorTrace_HDG<TopoD1>::integrate(
      fcnint, ufld, qfld, uIfld, &quadratureorder, 1,
      rsdPDEGlobal, rsdAuGlobal, rsdIntGlobal);

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( LPDETrue, rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( RPDETrue, rsdPDEGlobal[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( IPDETrue, rsdIntGlobal[0], small_tol, close_tol );

  SANS_CHECK_CLOSE( AuxLTrue, rsdAuGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( AuxRTrue, rsdAuGlobal[1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AWResidualInteriorTrace_2D_HDG_2Triangle_X1Q1R1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Linear> WeightFunction;
  typedef AWIntegrandTrace_HDG<PDEClass,WeightFunction> IntegrandClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: 2 triangles @ P1 (aka X1)
  XField2D_2Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> ufld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle solution data (left)
  ufld.DOF(0) = 1;
  ufld.DOF(1) = 3;
  ufld.DOF(2) = 4;

  // triangle solution data (right)
  ufld.DOF(3) = 7;
  ufld.DOF(4) = 2;
  ufld.DOF(5) = 9;

  // auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle auxiliary variable (left)
  qfld.DOF(0) = { 2, -3};
  qfld.DOF(1) = { 7,  8};
  qfld.DOF(2) = {-1, -5};

  // triangle auxiliary variable (right)
  qfld.DOF(3) = { 9,  6};
  qfld.DOF(4) = {-1,  3};
  qfld.DOF(5) = { 2, -3};

  // interface solution: P1 (aka Q1)
  Field_DG_InteriorTrace<PhysD2, TopoD2, ArrayQ> uIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // line trace data
  uIfld.DOF(0) =  8;
  uIfld.DOF(1) = -1;

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Local );

  // Using the Analytical Weight
  // Weight Function
  Real a0=1.2, a1=0.8, a2=0.3; // a0 + a1*x + a2*y
  WeightFunction wfcn(a0,a1,a2);

  // integrand
  IntegrandClass fcnint( pde, wfcn, disc );

  // quadrature rule: quadratic (basis & flux both linear)
  int quadratureorder = -1;

  Real rsdPDEL1 = ( 0)     + (0)           + ( 0);
  Real rsdPDEL2 = ( 13./6.)     + (-4329./500.) + (-353./50.);
  Real rsdPDEL3 = ( 143./60.) + ( -171./100.) + ( 353./50.);
  Real LPDETrue = a0*rsdPDEL1 + (a0+a1)*rsdPDEL2 + (a0+a2)*rsdPDEL3;

  // PDE residuals (right): (advective) + (viscous) + (stabilization)
  Real rsdPDER1 = ( 0)     + (0)         + (0);
  Real rsdPDER2 = (-13./10.) + ( 39./50.)  + (2471./250.);
  Real rsdPDER3 = (-13./4.)     + (279./500.) + (353./50.);
  Real RPDETrue = a0*rsdPDER1 + (a0+a2)*rsdPDER2 + (a0+a1)*rsdPDER3;

  // interface residuals
  Real rsdInt1 = ( -13./12. )+( -81./10. ) + (0);
  Real rsdInt2 = ( 13./12. )+( -93./100. ) + (2118./125.);
  Real IPDETrue = (a1+a0)*rsdInt1 + (a2+a0)*rsdInt2;

  /*
    (3*2^(1/2))/9007199254740992
                            0

 -(6817432910271587*2^(1/2))/2251799813685248
                                     -117/125
   */
  // auxiliary variable defn (left)
  Real LAuxTrue = -(3.*sqrt(2))/9007199254740992.;
  Real LAuyTrue = 0.;
  Real RAuxTrue = (6817432910271587.*sqrt(2))/2251799813685248.;
  Real RAuyTrue = 117./125.;

  // PDE, interface, aux var defn global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(2);
  SLA::SparseVector<ArrayQ> rsdAuGlobal(4);
  SLA::SparseVector<ArrayQ> rsdIntGlobal(1);

  rsdPDEGlobal = 0;
  rsdAuGlobal = 0;
  rsdIntGlobal = 0;

  AWResidualInteriorTrace_HDG<TopoD2>::integrate( fcnint, ufld, qfld, uIfld, &quadratureorder, 1, rsdPDEGlobal, rsdAuGlobal, rsdIntGlobal);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( LPDETrue, rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( RPDETrue, rsdPDEGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( IPDETrue, rsdIntGlobal[0], small_tol, close_tol );

  SANS_CHECK_CLOSE( LAuxTrue, rsdAuGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( LAuyTrue, rsdAuGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( RAuxTrue, rsdAuGlobal[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( RAuyTrue, rsdAuGlobal[3], small_tol, close_tol );

}
#endif
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AWResidualInteriorTrace_2D_HDG_2Triangle_X1Q1R1_composite )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Linear> WeightFunction;
  typedef AWIntegrandTrace_HDG<PDEClass,WeightFunction> IntegrandClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: 2 triangles @ P1 (aka X1)
  XField2D_2Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> ufld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle solution data (left)
  ufld.DOF(0) = 1;
  ufld.DOF(1) = 3;
  ufld.DOF(2) = 4;

  // triangle solution data (right)
  ufld.DOF(3) = 7;
  ufld.DOF(4) = 2;
  ufld.DOF(5) = 9;

  // auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle auxiliary variable (left)
  qfld.DOF(0) = { 2, -3};
  qfld.DOF(1) = { 7,  8};
  qfld.DOF(2) = {-1, -5};

  // triangle auxiliary variable (right)
  qfld.DOF(3) = { 9,  6};
  qfld.DOF(4) = {-1,  3};
  qfld.DOF(5) = { 2, -3};

  // interface solution: P1 (aka Q1)
  Field_DG_InteriorTrace<PhysD2, TopoD2, ArrayQ> uIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // line trace data
  uIfld.DOF(0) =  8;
  uIfld.DOF(1) = -1;

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Local );

  // Using the Analytical Weight
  // Weight Function
  Real a0=1.3, a1=0.8, a2=-0.3; // a0 + a1*x + a2*y
  WeightFunction wfcn(a0,a1,a2);

  // integrand
  IntegrandClass fcnint( pde, wfcn, disc );

  // quadrature rule: quadratic (basis & flux both linear)
  int quadratureorder = 2;

  // PDE residuals (left): (advective) + (viscous) + (stabilization)
  Real rsdPDEL1 = ( 0)     + (0)           + ( 0);
  Real rsdPDEL2 = ( 13./6.)     + (-4329./500.) + (-353./50.);
  Real rsdPDEL3 = ( 143./60.) + ( -171./100.) + ( 353./50.);
  Real rsdPDELT = a0*rsdPDEL1 + (a0+a1)*rsdPDEL2 + (a0+a2)*rsdPDEL3;

  // PDE residuals (right): (advective) + (viscous) + (stabilization)
  Real rsdPDER1 = ( 0)     + (0)         + (0);
  Real rsdPDER2 = (-13./10.) + ( 39./50.)  + (2471./250.);
  Real rsdPDER3 = (-13./4.)     + (279./500.) + (353./50.);
  Real rsdPDERT = a0*rsdPDER1 + (a0+a2)*rsdPDER2 + (a0+a1)*rsdPDER3;

  // interface residuals
  Real rsdInt1 = ( -13./12. )+( -81./10. ) + (0);
  Real rsdInt2 = ( 13./12. )+( -93./100. ) + (2118./125.);
  Real rsdIntT = (a1+a0)*rsdInt1 + (a2+a0)*rsdInt2;

  // auxiliary variable defn (left)
  Real rsdAuxL1 =  0;
  Real rsdAuxL2 = -223./100.;
  Real rsdAuxL3 =  223./100.;
  Real rsdAuxLT = -a1*rsdAuxL1 - a1*rsdAuxL2 - a1*rsdAuxL3;

  Real rsdAuyL1 =  0;
  Real rsdAuyL2 = -13./10.;
  Real rsdAuyL3 =  13./10.;
  Real rsdAuyLT = -a2*rsdAuyL1 - a2*rsdAuyL2 - a2*rsdAuyL3;

  // auxiliary variable defn (right)
  Real rsdAuxR1 =  0;
  Real rsdAuxR2 = -1561./500.;
  Real rsdAuxR3 =  -223./100.;
  Real rsdAuxRT = -a1*rsdAuxR1 - a1*rsdAuxR2 - a1*rsdAuxR3;

  Real rsdAuyR1 =  0;
  Real rsdAuyR2 = -91./50.;
  Real rsdAuyR3 = -13./10.;
  Real rsdAuyRT = -a2*rsdAuyR1 - a2*rsdAuyR2 - a2*rsdAuyR3;

  // PDE, inteface, aux var defn global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(2);
  SLA::SparseVector<ArrayQ> rsdAuGlobal(4);
  SLA::SparseVector<ArrayQ> rsdIntGlobal(1);

  // topology-specific single group interface

  rsdPDEGlobal = 0;
  rsdAuGlobal = 0;
  rsdIntGlobal = 0;

  AWResidualInteriorTrace_HDG<TopoD2>::integrate( fcnint, ufld, qfld, uIfld, &quadratureorder, 1, rsdPDEGlobal, rsdAuGlobal, rsdIntGlobal);

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( rsdPDELT, rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERT, rsdPDEGlobal[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdIntT, rsdIntGlobal[0], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdAuxLT, rsdAuGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAuyLT, rsdAuGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAuxRT, rsdAuGlobal[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAuyRT, rsdAuGlobal[3], small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
