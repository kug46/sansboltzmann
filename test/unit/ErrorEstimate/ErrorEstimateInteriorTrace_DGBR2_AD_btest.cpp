// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEstimateInteriorTrace_DGBR2_AD_btest
// testing of trace residual functions for DG BR2 with Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/Tuple/FieldTuple.h"

#include "Discretization/DG/DiscretizationDGBR2.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"

#include "Discretization/DG/IntegrandInteriorTrace_DGBR2.h"
#include "ErrorEstimate/DG/ErrorEstimateInteriorTrace_DGBR2.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEstimateInteriorTrace_DGBR2_AD_test_suite )

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateInteriorTrace_DGBR2_2Line_X1Q1R1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 4);

  // line solution data (left)
  qfld.DOF(0) = 2;
  qfld.DOF(1) = 3;

  // line solution data (right)
  qfld.DOF(2) = 7;
  qfld.DOF(3) = 4;

  // lifting-operator: P1 (aka R1)
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( rfld.nDOF(), 8 );

  // lifting operators in left element
  rfld.DOF(0) = 3; //right edge
  rfld.DOF(1) = 2; //right edge
  rfld.DOF(2) = 0; //left edge
  rfld.DOF(3) = 0; //left edge

  // lifting operators in right element
  rfld.DOF(4) = 0; //right edge
  rfld.DOF(5) = 0; //right edge
  rfld.DOF(6) = 5; //left edge
  rfld.DOF(7) = 4; //left edge

  // weighting: P2 (akak Q2)
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( wfld.nDOF(), 6);

  // line solution data (left)
  wfld.DOF(0) = 3;
  wfld.DOF(1) = 4;
  wfld.DOF(2) = 5;

  // line solution data (right)
  wfld.DOF(3) =  5;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  // lifting-operator: P1 (aka R1)
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> sfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( sfld.nDOF(), 12 );

  // lifting operators in left element
  sfld.DOF(0) = 2; //right edge
  sfld.DOF(1) = 9; //right edge
  sfld.DOF(2) = 3; //right edge
  sfld.DOF(3) = 0; //left edge
  sfld.DOF(4) = 0; //left edge
  sfld.DOF(5) = 0; //left edge

  // lifting operators in right element
  sfld.DOF(6)  = 0; //right edge
  sfld.DOF(7)  = 0; //right edge
  sfld.DOF(8)  = 0; //right edge
  sfld.DOF(9)  = 3; //left edge
  sfld.DOF(10) = 2; //left edge
  sfld.DOF(11) = 4; //left edge

  // estimate fields
  Field_DG_Cell<PhysD1, TopoD1, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);

  // solution data
  efld=0;

  BOOST_CHECK_EQUAL( 2, efld.nElem() );
  BOOST_CHECK_EQUAL( 2, efld.nDOF() );

  // lifting operator: single line, P0 (aka Q0)
  FieldLift_DG_Cell<PhysD1, TopoD1, Real> eLfld(xfld, 0, BasisFunctionCategory_Legendre);

  eLfld=0;

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // quadrature rule (no quadrature required: value at a node)
  int quadratureOrder = 0;

  // integrand
  IntegrandClass fcn( pde, disc, {0} );

  // base interface
  IntegrateInteriorTraceGroups<TopoD1>::integrate(ErrorEstimateInteriorTrace_DGBR2(fcn),
                                                  xfld, (qfld, rfld, wfld, sfld, efld, eLfld), &quadratureOrder, 1);

  Real rsdPDELTrue,rsdPDERTrue;
  Real rsdLiftL,rsdLiftR;

  //PDE residuals (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  rsdPDELTrue = ( 66./5. ) + ( 2123./250. ) + ( -40337./500. ) + ( -14861./250. ); // Weight function
  rsdPDERTrue = ( -33./2. ) + ( -2123./200. ) + ( -2123./100. ) + ( 14861./200. ); // Weight function

  //PDE residual: (lifting)
  rsdLiftL = ( -18 );   // Basis function
  rsdLiftR = ( -6 );   // Basis function

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( rsdPDELTrue, efld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue, efld.DOF(1), small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdLiftL, eLfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,        eLfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,        eLfld.DOF(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftR, eLfld.DOF(3), small_tol, close_tol );


}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateInteriorTrace_DGBR2_2Triangle_X1Q1R1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: P1 (aka X1)
  XField2D_2Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 6 );

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  // triangle solution data (right)
  qfld.DOF(3) = 7;
  qfld.DOF(4) = 2;
  qfld.DOF(5) = 9;

  // lifting-operator: P1 (aka R1)
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( rfld.nDOF(), 18 );

  rfld = 0;
  // triangle auxiliary variable (left)
  rfld.DOF(0) = { 2, -3};
  rfld.DOF(1) = { 7,  8};
  rfld.DOF(2) = {-1,  7};

  // triangle auxiliary variable (right)
  rfld.DOF( 9) = { 8, -2};
  rfld.DOF(10) = {-5,  7};
  rfld.DOF(11) = { 3,  9};

  // weighting P2 (aka Q2)
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( wfld.nDOF(), 12 );

  // triangle solution data (left)
  wfld.DOF(0) = -2;
  wfld.DOF(1) =  4;
  wfld.DOF(2) =  3;
  wfld.DOF(3) =  2;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  // triangle solution data (right)
  wfld.DOF(6)  =  7;
  wfld.DOF(7)  =  3;
  wfld.DOF(8)  =  2;
  wfld.DOF(9)  = -1;
  wfld.DOF(10) =  5;
  wfld.DOF(11) = -3;

  // lifting-operator: P1 (aka R1)
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> sfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( sfld.nDOF(), 36 );

  sfld = 0;

  // triangle auxiliary variable (left)
  sfld.DOF(0) = { 1,  5};
  sfld.DOF(1) = { 3,  6};
  sfld.DOF(2) = {-1,  7};
  sfld.DOF(3) = { 3,  1};
  sfld.DOF(4) = { 2,  2};
  sfld.DOF(5) = { 4,  3};

  // triangle auxiliary variable (right)
  // numbering is reversed because canonical trace right is 0, -1 below
  sfld.DOF(18) = { 5,  6};
  sfld.DOF(19) = { 4,  5};
  sfld.DOF(20) = { 3,  4};
  sfld.DOF(21) = { 4,  3};
  sfld.DOF(22) = { 3,  2};
  sfld.DOF(23) = { 2,  1};

  // estimate fields
  Field_DG_Cell<PhysD2, TopoD2, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);

  efld = 0;

  BOOST_CHECK_EQUAL( 2, efld.nElem() );
  BOOST_CHECK_EQUAL( 2, efld.nDOF() );

  // lifting operator: single line, P0 (aka Q0)
  FieldLift_DG_Cell<PhysD2, TopoD2, Real> eLfld(xfld, 0, BasisFunctionCategory_Legendre);

  // left element
  eLfld=0;

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // quadrature rule (quadratic: basis & flux both linear)
  int quadratureOrder = 3;

  // integrand
  IntegrandClass fcn( pde, disc, {0} );

  // base interface
  IntegrateInteriorTraceGroups<TopoD2>::integrate(ErrorEstimateInteriorTrace_DGBR2(fcn),
                                                  xfld, (qfld, rfld, wfld, sfld, efld, eLfld), &quadratureOrder, 1);

  Real rsdPDELTrue,rsdPDERTrue;
  Real rsdLiftLTrue,rsdLiftRTrue;

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  //PDE residuals (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  rsdPDELTrue = ( 1313/60. ) + ( -79547/1500. ) + ( 32669/750. ) + ( -111969/250. );   // Basis function
  rsdPDERTrue = ( -169/20. ) + ( 30173/1500. ) + ( 71543/750. ) + ( 38051/250. );   // Basis function

  //LO residuals (left): (lifting-operator)
  rsdLiftLTrue = ( -67/6. ); // Weight function

  //LO residuals (right): (lifting-operator)
  rsdLiftRTrue = ( -12 ); // Weight function

  SANS_CHECK_CLOSE( rsdPDELTrue, efld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue, efld.DOF(1), small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdLiftLTrue, eLfld.DOF(0), small_tol, close_tol );
  for (int i=1;i<3;i++)
    SANS_CHECK_CLOSE( 0, eLfld.DOF(i),small_tol,close_tol);

  SANS_CHECK_CLOSE( rsdLiftRTrue, eLfld.DOF(3), small_tol, close_tol );
  for (int i=4;i<6;i++)
    SANS_CHECK_CLOSE( 0, eLfld.DOF(i),small_tol,close_tol);

}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
