// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AWResidualCell_HDG_AD_btest
// testing of cell residual functions for HDG with Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "Discretization/HDG/AWResidualCell_HDG.h"
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/ForcingFunction1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( AWResidualCell_HDG_AD_test_suite )

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AWResidualPDE_1D_HDG_1Line_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef SolnNDConvertSpace<PhysD1, ScalarFunction1D_Hex> WeightFunction;
  typedef AWIntegrandCell_HDG<PDEClass,WeightFunction> IntegrandClass;
  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single line, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single line, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> ufld(xfld, qorder, BasisFunctionCategory_Hierarchical);

   // solution data
  ufld.DOF(0) = 1;
  ufld.DOF(1) = 4;

  // auxiliary variable: single line, P1 (aka Q1)
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // auxiliary variable data
  qfld.DOF(0) = { 2};
  qfld.DOF(1) = { 3};

  // HDG discretization (not used)
  DiscretizationClass disc( pde , Local);

  // quadrature rule: p8 - Weighting is p6, solution p1,
  int quadratureOrder = 8;

  // PDE residuals: (advective) + (viscous)
  Real rsdPDE = (-99./700.) +  (6369./70000.);

  // auxiliary variable defn
  Real rsdAux = -6369./70000.;

  const Real tol = 1e-12;

  SLA::SparseVector<ArrayQ> rsdPDEGlobal(ufld.nElem());
  SLA::SparseVector<ArrayQ> rsdAuGlobal(ufld.nElem()*qfld.D);

  rsdPDEGlobal = 0;
  rsdAuGlobal = 0;

  // Weight Function
  WeightFunction wfcn;

  IntegrandClass fcnint( pde, wfcn, disc);

  AWResidualCell_HDG<TopoD1>::integrate( fcnint, ufld, qfld,
                                       &quadratureOrder, 1,
                                       rsdPDEGlobal, rsdAuGlobal);

  BOOST_CHECK_CLOSE( rsdPDE, rsdPDEGlobal[0], tol );

  BOOST_CHECK_CLOSE( rsdAux, rsdAuGlobal[0], tol );
}
#endif

//----------------------------------------------------------------------------//
// 1D 2 Line
BOOST_AUTO_TEST_CASE( AWResidualCell_1D_HDG_2Line_X1Q1R1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef SolnNDConvertSpace<PhysD1, ScalarFunction1D_Hex2> WeightFunction;
  typedef AWIntegrandCell_HDG<PDEClass,WeightFunction> IntegrandClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: 2 lines @ P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> ufld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // line solution data (left)
  ufld.DOF(0) = 1;
  ufld.DOF(1) = 4;

  // line solution data (right)
  ufld.DOF(2) = 7;
  ufld.DOF(3) = 9;

  // auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // line auxiliary variable (left)
  qfld.DOF(0) = { 2};
  qfld.DOF(1) = { 7};

  // line auxiliary variable (right)
  qfld.DOF(2) = { 9};
  qfld.DOF(3) = {-1};

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde , Local);

  // quadrature rule: p8 - Weighting is p6, solution p1,
  int quadratureOrder = 8;

  // Using the Analytical Weight
  // Weight Function
  WeightFunction wfcn;

  // integrand
  IntegrandClass fcnint( pde, wfcn, disc );

  /*
   * inv, vis, aux
    INT L
    [ -737/1400, 142241/84000, 142241/84000]

    INT R
    [ 6193/2100, 1195249/42000, 1195249/42000]
   */

  // PDE residuals (left): (advective) + (viscous)
  Real LPDETrue = -737./1400. + 142241./84000.;
  Real RPDETrue = 6193./2100. + 1195249./42000.;

  // auxiliary variable defn (left)
  Real AuxLTrue = -142241./84000.;
  Real AuxRTrue = -1195249./42000.;

  // PDE, interface, aux var defn global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(ufld.nElem());
  SLA::SparseVector<ArrayQ> rsdAuGlobal(ufld.nElem()*qfld.D);

  rsdPDEGlobal = 0;
  rsdAuGlobal = 0;

  // topology-specific single group interface

  AWResidualCell_HDG<TopoD1>::integrate( fcnint, ufld, qfld, &quadratureOrder, 1,
      rsdPDEGlobal, rsdAuGlobal);

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( LPDETrue, rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( RPDETrue, rsdPDEGlobal[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( AuxLTrue, rsdAuGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( AuxRTrue, rsdAuGlobal[1], small_tol, close_tol );
}


#if 1

//----------------------------------------------------------------------------//
// 2D 1 Triangle
BOOST_AUTO_TEST_CASE( AWResidualPDE_2D_HDG_1Triangle_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Cubic> WeightFunction;
  typedef AWIntegrandCell_HDG<PDEClass,WeightFunction> IntegrandClass;
  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single line, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single line, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> ufld(xfld, qorder, BasisFunctionCategory_Hierarchical);

   // solution data
  ufld.DOF(0) = 1;
  ufld.DOF(1) = 3;
  ufld.DOF(2) = 4;

  // auxiliary variable: single line, P1 (aka Q1)
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // auxiliary variable data
  qfld.DOF(0) = { 2, -3};
  qfld.DOF(1) = { 7,  8};
  qfld.DOF(2) = {-1, -5};


  // HDG discretization (not used)
  DiscretizationClass disc( pde , Local);

  // quadrature rule: quadratic (aux var defn has linear basis & linear solution)
  int quadratureOrder = -1;

  const Real tol = 1e-11;

  SLA::SparseVector<ArrayQ> rsdPDEGlobal(ufld.nElem());
  SLA::SparseVector<ArrayQ> rsdAuGlobal(ufld.nElem()*qfld.D);

  rsdPDEGlobal = 0;
  rsdAuGlobal = 0;

  // Weight Function
  WeightFunction wfcn;
  IntegrandClass fcnint( pde, wfcn, disc);

  /*
   INT
    [ -3681/1600, 63/100, -4689/1600]

    [ -75141/20000, 33057/40000]
   */

  // PDE residuals: (advective) + (viscous)
  Real rsdPDE = 63./100. + -4689./1600.;
  // auxiliary variable defn
  Real rsdAux =  75141./20000.;
  Real rsdAuy =  -33057./40000.;

  AWResidualCell_HDG<TopoD2>::integrate( fcnint, ufld, qfld,
                                       &quadratureOrder, 1,
                                       rsdPDEGlobal, rsdAuGlobal);

  BOOST_CHECK_CLOSE( rsdPDE, rsdPDEGlobal[0], tol );
  BOOST_CHECK_CLOSE( rsdAux, rsdAuGlobal[0], tol );
  BOOST_CHECK_CLOSE( rsdAuy, rsdAuGlobal[1], tol );

}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
