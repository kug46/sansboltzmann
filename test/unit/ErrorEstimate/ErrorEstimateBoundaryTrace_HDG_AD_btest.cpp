// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEstimateBoundaryTrace_AD_btest
// testing of boundary integral residual functions with Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
//#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
//#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
//#include "pde/NDConvert/PDENDConvertSpace3D.h"
//#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_Trace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Field/Tuple/FieldTuple.h"

#include "ErrorEstimate/HDG/ErrorEstimateBoundaryTrace_FieldTrace_HDG.h"
#include "ErrorEstimate/HDG/ErrorEstimateBoundaryTrace_HDG.h"

#include "Discretization/HDG/IntegrandBoundaryTrace_Flux_mitState_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_mitLG_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_sansLG_HDG.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
//#include "unit/UnitGrids/XField2D_1Quad_X1_1Group.h"
//#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
//#include "unit/UnitGrids/XField3D_1Hex_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEstimateBoundaryTrace_HDG_AD_test_suite )

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_LinearRobin_mitLG_1D_1Line_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandClass;

  // PDE
  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC

  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( qfld.nDOF() == 2 );

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 4;

  // lifting operator: single triangle, P1 (aka Q1)
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // auxiliary variable data
  afld.DOF(0) = {2};
  afld.DOF(1) = {7};

  BOOST_CHECK_EQUAL( 2, afld.nDOF() );
  BOOST_CHECK( afld.D == pde.D);

  // Lagrange multiplier
  Field_DG_Trace<PhysD1, TopoD1, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( qIfld.nDOF() == 2 );
  qIfld = 0;

  // node trace data
  qIfld.DOF(1) =  8; // left face

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( lgfld.nDOF() == 2 );

  lgfld.DOF(1) =  7;

  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( wfld.nDOF() == 3 );

  // line solution data (left)
  wfld.DOF(0) = 3;
  wfld.DOF(1) = 4;
  wfld.DOF(2) = 5;

  // auxilliary variable: single triangle, P1 (aka Q1)
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> bfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  bfld.DOF(0) = -5;
  bfld.DOF(1) =  3;
  bfld.DOF(2) =  2;

  // Lagrange multiplier
  Field_DG_Trace<PhysD1, TopoD1, ArrayQ> wIfld(xfld, 0, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( wIfld.nDOF() == 2 );

  // node trace data
  wIfld.DOF(1) =  -2;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> mufld(xfld, 0, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( mufld.nDOF() == 2 );

  // Lagrange multiplier DOF data
  mufld.DOF(1) = 3;

  Field_DG_Cell<PhysD1, TopoD1, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK( efld.nDOF() == 1 );

  efld = 0;

  // lifting operator: single line, P2 (aka Q2)
  Field_DG_Cell<PhysD1, TopoD1, Real> eAfld(xfld, 0, BasisFunctionCategory_Legendre);

  eAfld = 0;

  BOOST_CHECK_EQUAL( 1, eAfld.nElem() );
  BOOST_CHECK_EQUAL( 1, eAfld.nDOF() );
  BOOST_CHECK( eAfld.D == pde.D);

  // Lagrange multiplier
  Field_DG_Trace<PhysD1, TopoD1, Real> eIfld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK( eIfld.nDOF() == 2 );

  // error estimate field
  eIfld = 0;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD1, TopoD1, Real> eBfld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK( eBfld.nDOF() == 2 );

  // error estimate field
  eBfld = 0;

  // integrand

  DiscretizationHDG<PDEClass> disc( pde, Local, Gradient );
  IntegrandClass fcn( pde, bc, {1}, disc );

  // quadrature rule
  int quadratureorder[3] = {0,0,0};

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ErrorEstimateBoundaryTrace_FieldTrace_HDG(fcn),
                                                              xfld, (qfld,afld,wfld,bfld,efld,eAfld),
                                                              (qIfld,lgfld,wIfld,mufld,eIfld,eBfld), quadratureorder, 2 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  //Taken from IntegrandBoundaryTrace_LinearScalar_HDG_AD_btest
  Real rsdPDETrue, rsdAuxTrue, rsdPDEITrue, rsdBCTrue;

  //PDE residuals (left): (advective) + (viscous) + (bc)
  rsdPDETrue = ( 88/5. ) +  ( -14861/250. ) +  ( 2563133/16250. ); // Basis function
  rsdAuxTrue = ( -435249/6500. ); // Basis function
  rsdPDEITrue = ( -2123/125. ); // Basis function
  rsdBCTrue = ( 166749/1000. ); // Basis function

  SANS_CHECK_CLOSE( rsdPDETrue, efld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAuxTrue, eAfld.DOF(0), small_tol, close_tol );

  SANS_CHECK_CLOSE( 0,           eIfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDEITrue, eIfld.DOF(1), small_tol, close_tol );

  SANS_CHECK_CLOSE( 0,         eBfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue, eBfld.DOF(1), small_tol, close_tol );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_LinearRobin_sansLG_1D_1Line_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_sansLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandClass;

  // PDE
  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC

  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( qfld.nDOF() == 2 );

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 4;

  // lifting operator: single triangle, P1 (aka Q1)
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // auxiliary variable data
  afld.DOF(0) = {2};
  afld.DOF(1) = {7};

  BOOST_CHECK_EQUAL( 2, afld.nDOF() );
  BOOST_CHECK( afld.D == pde.D);

  // Lagrange multiplier
  Field_DG_Trace<PhysD1, TopoD1, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( qIfld.nDOF() == 2 );
  qIfld = 0;

  // node trace data
  qIfld.DOF(1) =  8; // left face

  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( wfld.nDOF() == 3 );

  // line solution data (left)
  wfld.DOF(0) = 3;
  wfld.DOF(1) = 4;
  wfld.DOF(2) = 5;

  // auxilliary variable: single triangle, P1 (aka Q1)
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> bfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  bfld.DOF(0) = -5;
  bfld.DOF(1) =  3;
  bfld.DOF(2) =  2;

  // Lagrange multiplier
  Field_DG_Trace<PhysD1, TopoD1, ArrayQ> wIfld(xfld, 0, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( wIfld.nDOF() == 2 );

  // node trace data
  wIfld.DOF(1) =  -2;

  Field_DG_Cell<PhysD1, TopoD1, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK( efld.nDOF() == 1 );

  efld = 0;

  // lifting operator: single line, P2 (aka Q2)
  Field_DG_Cell<PhysD1, TopoD1, Real> eAfld(xfld, 0, BasisFunctionCategory_Legendre);

  eAfld = 0;

  BOOST_CHECK_EQUAL( 1, eAfld.nElem() );
  BOOST_CHECK_EQUAL( 1, eAfld.nDOF() );
  BOOST_CHECK( eAfld.D == pde.D);

  // Lagrange multiplier
  Field_DG_Trace<PhysD1, TopoD1, Real> eIfld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK( eIfld.nDOF() == 2 );

  // error estimate field
  eIfld = 0;

  // integrand

  DiscretizationHDG<PDEClass> disc( pde, Local, Gradient );

  IntegrandClass fcn( pde, bc, {1}, disc );

  // quadrature rule
  int quadratureorder[2] = {0,0};

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ErrorEstimateBoundaryTrace_HDG(fcn),
                                                              xfld, (qfld,afld,wfld,bfld,efld,eAfld),
                                                              (qIfld,wIfld,eIfld), quadratureorder, 2 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  //Taken from IntegrandBoundaryTrace_LinearScalar_HDG_AD_btest
  Real rsdPDETrue, rsdAUXTrue, rsdINTTrue;

  //PDE residuals: (advective) + (viscous) + (stabilization) + (bc)
  rsdPDETrue = ( 88/5. ) +  ( -14861/250. ) + ( -4246/125. ) + ( 111166/8125. ); // Basis function

  rsdAUXTrue = ( 10749/6500. ); // Basis function

  rsdINTTrue = ( -2123/125. ); // Basis function

  SANS_CHECK_CLOSE( rsdPDETrue, efld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXTrue, eAfld.DOF(0), small_tol, close_tol );

  SANS_CHECK_CLOSE( 0,           eIfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdINTTrue, eIfld.DOF(1), small_tol, close_tol );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_Dirichlet_mitState_1D_1Line_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCTypeDirichlet_mitState<AdvectiveFlux1D_Uniform,ViscousFlux1D_Uniform> BCTypeDirichlet_mitState1D;

  typedef BCAdvectionDiffusion<PhysD1,BCTypeDirichlet_mitState1D> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandClass;

  // PDE
  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC
  Real uB = 12./5.;
  BCClass bc(pde, uB);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( qfld.nDOF() == 2 );

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 4;

  // lifting operator: single triangle, P1 (aka Q1)
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // auxiliary variable data
  afld.DOF(0) = {2};
  afld.DOF(1) = {7};

  BOOST_CHECK_EQUAL( 2, afld.nDOF() );
  BOOST_CHECK( afld.D == pde.D);

  // Lagrange multiplier
  Field_DG_Trace<PhysD1, TopoD1, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( qIfld.nDOF() == 2 );
  qIfld = 0;

  // node trace data
  qIfld.DOF(1) =  8; // left face

  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( wfld.nDOF() == 3 );

  // line solution data (left)
  wfld.DOF(0) = 3;
  wfld.DOF(1) = 4;
  wfld.DOF(2) = 5;

  // auxilliary variable: single triangle, P1 (aka Q1)
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> bfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  bfld.DOF(0) = -5;
  bfld.DOF(1) =  3;
  bfld.DOF(2) =  2;

  // Lagrange multiplier
  Field_DG_Trace<PhysD1, TopoD1, ArrayQ> wIfld(xfld, 0, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( wIfld.nDOF() == 2 );

  // node trace data
  wIfld.DOF(1) =  -2;

  Field_DG_Cell<PhysD1, TopoD1, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK( efld.nDOF() == 1 );

  efld = 0;

  // lifting operator: single line, P2 (aka Q2)
  Field_DG_Cell<PhysD1, TopoD1, Real> eAfld(xfld, 0, BasisFunctionCategory_Legendre);

  eAfld = 0;

  BOOST_CHECK_EQUAL( 1, eAfld.nElem() );
  BOOST_CHECK_EQUAL( 1, eAfld.nDOF() );
  BOOST_CHECK( eAfld.D == pde.D);

  // Lagrange multiplier
  Field_DG_Trace<PhysD1, TopoD1, Real> eIfld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK( eIfld.nDOF() == 2 );

  // error estimate field
  eIfld = 0;

  // integrand

  DiscretizationHDG<PDEClass> disc( pde, Local, Gradient );
  IntegrandClass fcn( pde, bc, {1}, disc );

  // quadrature rule
  int quadratureorder[2] = {0,0};

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ErrorEstimateBoundaryTrace_HDG(fcn),
                                                              xfld, (qfld,afld,wfld,bfld,efld,eAfld),
                                                              (qIfld,wIfld,eIfld), quadratureorder, 2 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  //Taken from IntegrandBoundaryTrace_LinearScalar_HDG_AD_btest
  Real rsdPDETrue, rsdAUXTrue, rsdINTTrue;

  //PDE residuals: (advective) + (viscous) + (stabilization)
  rsdPDETrue = ( 88/5. ) + ( -14861/250. ) + ( -4246/125. );
  rsdAUXTrue = ( -36/5. );
  rsdINTTrue = ( -30976/625. );

  SANS_CHECK_CLOSE( rsdPDETrue, efld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXTrue, eAfld.DOF(0), small_tol, close_tol );

  SANS_CHECK_CLOSE( 0,           eIfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdINTTrue, eIfld.DOF(1), small_tol, close_tol );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_RobinFunction_mitState_1D_1Line_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCTypeFunction_mitState<AdvectiveFlux1D_Uniform,ViscousFlux1D_Uniform> BCTypeFunction_mitState1D;
  typedef BCAdvectionDiffusion<PhysD1,BCTypeFunction_mitState1D> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandClass;

  // PDE
  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC
  Real qB = 12./5.;
  Real qBx = -4./5.;
  BCClass::Function_ptr uexact(new ScalarFunction1D_Linear( qB, qBx ));
  bool upwind = false;
  BCClass bc(uexact, adv, visc, "Robin", upwind);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( qfld.nDOF() == 2 );

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 4;

  // lifting operator: single triangle, P1 (aka Q1)
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // auxiliary variable data
  afld.DOF(0) = {2};
  afld.DOF(1) = {7};

  BOOST_CHECK_EQUAL( 2, afld.nDOF() );
  BOOST_CHECK( afld.D == pde.D);

  // Lagrange multiplier
  Field_DG_Trace<PhysD1, TopoD1, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( qIfld.nDOF() == 2 );
  qIfld = 0;

  // node trace data
  qIfld.DOF(1) =  8; // left face

  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( wfld.nDOF() == 3 );

  // line solution data (left)
  wfld.DOF(0) = 3;
  wfld.DOF(1) = 4;
  wfld.DOF(2) = 5;

  // auxilliary variable: single triangle, P1 (aka Q1)
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> bfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  bfld.DOF(0) = -5;
  bfld.DOF(1) =  3;
  bfld.DOF(2) =  2;

  // Lagrange multiplier
  Field_DG_Trace<PhysD1, TopoD1, ArrayQ> wIfld(xfld, 0, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( wIfld.nDOF() == 2 );

  // node trace data
  wIfld.DOF(1) =  -2;

  Field_DG_Cell<PhysD1, TopoD1, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK( efld.nDOF() == 1 );

  efld = 0;

  // lifting operator: single line, P2 (aka Q2)
  Field_DG_Cell<PhysD1, TopoD1, Real> eAfld(xfld, 0, BasisFunctionCategory_Legendre);

  eAfld = 0;

  BOOST_CHECK_EQUAL( 1, eAfld.nElem() );
  BOOST_CHECK_EQUAL( 1, eAfld.nDOF() );
  BOOST_CHECK( eAfld.D == pde.D);

  // Lagrange multiplier
  Field_DG_Trace<PhysD1, TopoD1, Real> eIfld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK( eIfld.nDOF() == 2 );

  // error estimate field
  eIfld = 0;

  // integrand

  DiscretizationHDG<PDEClass> disc( pde, Local, Gradient );
  IntegrandClass fcn( pde, bc, {1}, disc );

  // quadrature rule
  int quadratureorder[2] = {0,0};

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ErrorEstimateBoundaryTrace_HDG(fcn),
                                                              xfld, (qfld,afld,wfld,bfld,efld,eAfld),
                                                              (qIfld,wIfld,eIfld), quadratureorder, 2 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  //Taken from IntegrandBoundaryTrace_LinearScalar_HDG_AD_btest
  Real rsdPDETrue, rsdAUXTrue, rsdTraceTrue;

  //PDE residuals: (advective) + (viscous) + (stabilization)
  rsdPDETrue = ( 88/5. ) + ( -14861/250. ) + ( -4246/125. );
  rsdAUXTrue = ( -24/5. );
  rsdTraceTrue = ( -179993/2500. );

  SANS_CHECK_CLOSE( rsdPDETrue, efld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXTrue, eAfld.DOF(0), small_tol, close_tol );

  SANS_CHECK_CLOSE( 0,           eIfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTraceTrue, eIfld.DOF(1), small_tol, close_tol );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_LinearRobin_mitLG_2D_1Triangle_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandClass;

  // PDE
  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC
  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( qfld.nDOF() == 3 );

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  // lifting operator: single triangle, P1 (aka Q1)
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  afld.DOF(0) = { 2, -3};
  afld.DOF(1) = { 7,  8};
  afld.DOF(2) = {-1, -5};

  BOOST_CHECK_EQUAL( 3, afld.nDOF() );
  BOOST_CHECK( afld.D == pde.D);

  // Lagrange multiplier
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( qIfld.nDOF() == 6 );

  // Lagrange multiplier DOF data - Second face, thus DOF 2 and 3
  qIfld.DOF(2) =  8;
  qIfld.DOF(3) = -1;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( lgfld.nDOF() == 6 );

  // Lagrange multiplier DOF data - Second face, thus DOF 2 and 3
  lgfld.DOF(2) =  7;
  lgfld.DOF(3) = -9;

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( wfld.nDOF() == 6 );

  // triangle solution (left)
  wfld.DOF(0) = -2;
  wfld.DOF(1) =  4;
  wfld.DOF(2) =  3;
  wfld.DOF(3) =  2;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  // lifting operator: single triangle, P1 (aka Q1)
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> bfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  bfld.DOF(0) = { 1,  5};
  bfld.DOF(1) = { 3,  6};
  bfld.DOF(2) = {-1,  7};
  bfld.DOF(3) = { 3,  1};
  bfld.DOF(4) = { 2,  2};
  bfld.DOF(5) = { 4,  3};

  // Lagrange multiplier
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> wIfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( wIfld.nDOF() == 9 );

  // Lagrange multiplier DOF data
  wIfld.DOF(3) =  2;
  wIfld.DOF(4) = -1;
  wIfld.DOF(5) =  3;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> mufld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( mufld.nDOF() == 9 );

  // Lagrange multiplier DOF data
  mufld.DOF(3) = -3;
  mufld.DOF(4) =  2;
  mufld.DOF(5) = -1;

  Field_DG_Cell<PhysD2, TopoD2, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK( efld.nDOF() == 1 );

  efld = 0;

  // lifting operator: single line, P2 (aka Q2)
  Field_DG_Cell<PhysD2, TopoD2, Real> eAfld(xfld, 0, BasisFunctionCategory_Legendre);

  eAfld = 0;

  BOOST_CHECK_EQUAL( 1, eAfld.nElem() );
  BOOST_CHECK_EQUAL( 1, eAfld.nDOF() );
  BOOST_CHECK( eAfld.D == pde.D);

  // Lagrange multiplier
  Field_DG_Trace<PhysD2, TopoD2, Real> eIfld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK( eIfld.nDOF() == 3 );

  // error estimate field
  eIfld = 0;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, Real> eBfld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK( eBfld.nDOF() == 3 );

  // error estimate field
  eBfld = 0;

  // integrand

  DiscretizationHDG<PDEClass> disc( pde, Local, Gradient );
  IntegrandClass fcn( pde, bc, {1}, disc );

  // quadrature rule
  int quadratureorder[3] = {3,3,3};

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ErrorEstimateBoundaryTrace_FieldTrace_HDG(fcn),
                                                              xfld, (qfld,afld,wfld,bfld,efld,eAfld),
                                                              (qIfld,lgfld,wIfld,mufld,eIfld,eBfld), quadratureorder, 3 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  //Taken from IntegrandBoundaryTrace_LinearScalar_HDG_AD_btest
  Real rsdPDETrue, rsdAUXTrue, rsdPDEITrue, rsdBCTrue;

  //PDE residuals (left): (advective) + (viscous) + (bc)
  rsdPDETrue = ( 1313/60. ) + ( -21437/375. )
               + ( (1/97500.)*((pow(2,-1/2.))*((40+(39)*(sqrt(2)))*(-73875+(42874)*(sqrt(2))))) ); // Weight function
  rsdAUXTrue = ( (1/13.)*((pow(2,-1/2.))*(-741+(581)*(sqrt(2)))) ); // Weight function
  rsdPDEITrue = ( 559/25. ); // Weight function
  rsdBCTrue = ( (-1/750.)*((pow(2,-1/2.))*(14750+(69927)*(sqrt(2)))) ); // Weight function

  SANS_CHECK_CLOSE( rsdPDETrue, efld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXTrue, eAfld.DOF(0), small_tol, close_tol );

  SANS_CHECK_CLOSE( 0,           eIfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDEITrue, eIfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,           eIfld.DOF(2), small_tol, close_tol );

  SANS_CHECK_CLOSE( 0,         eBfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue, eBfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,         eBfld.DOF(2), small_tol, close_tol );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_LinearRobin_sansLG_2D_1Triangle_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_sansLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandClass;

  // PDE
  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC
  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( qfld.nDOF() == 3 );

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  // lifting operator: single triangle, P1 (aka Q1)
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  afld.DOF(0) = { 2, -3};
  afld.DOF(1) = { 7,  8};
  afld.DOF(2) = {-1, -5};

  BOOST_CHECK_EQUAL( 3, afld.nDOF() );
  BOOST_CHECK( afld.D == pde.D);

  // interface solution
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( qIfld.nDOF() == 6 );

  qIfld = 0;
  // DOF data - Second face, thus DOF 2 and 3
  qIfld.DOF(2) =  8;
  qIfld.DOF(3) = -1;

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( wfld.nDOF() == 6 );

  // triangle solution (left)
  wfld.DOF(0) = -2;
  wfld.DOF(1) =  4;
  wfld.DOF(2) =  3;
  wfld.DOF(3) =  2;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  // lifting operator: single triangle, P1 (aka Q1)
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> bfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  bfld.DOF(0) = { 1,  5};
  bfld.DOF(1) = { 3,  6};
  bfld.DOF(2) = {-1,  7};
  bfld.DOF(3) = { 3,  1};
  bfld.DOF(4) = { 2,  2};
  bfld.DOF(5) = { 4,  3};

  // interface weight
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> wIfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( wIfld.nDOF() == 9 );

  wIfld = 0;
  // DOF data
  wIfld.DOF(3) =  2;
  wIfld.DOF(4) = -1;
  wIfld.DOF(5) =  3;

  Field_DG_Cell<PhysD2, TopoD2, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK( efld.nDOF() == 1 );

  efld = 0;

  // lifting operator: single line, P2 (aka Q2)
  Field_DG_Cell<PhysD2, TopoD2, Real> eAfld(xfld, 0, BasisFunctionCategory_Legendre);

  eAfld = 0;

  BOOST_CHECK_EQUAL( 1, eAfld.nElem() );
  BOOST_CHECK_EQUAL( 1, eAfld.nDOF() );
  BOOST_CHECK( eAfld.D == pde.D);

  // Lagrange multiplier
  Field_DG_Trace<PhysD2, TopoD2, Real> eIfld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK( eIfld.nDOF() == 3 );

  // error estimate field
  eIfld = 0;

  // integrand

  DiscretizationHDG<PDEClass> disc( pde, Local, Gradient );
  IntegrandClass fcn( pde, bc, {1}, disc );

  // quadrature rule
  int quadratureorder[3] = {3,3,3};

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ErrorEstimateBoundaryTrace_HDG(fcn),
                                                              xfld, (qfld,afld,wfld,bfld,efld,eAfld),
                                                              (qIfld,wIfld,eIfld), quadratureorder, 3 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  //Taken from IntegrandBoundaryTrace_LinearScalar_HDG_AD_btest
  Real rsdPDETrue, rsdAUXTrue, rsdTraceTrue;

  //PDE residuals: (advective) + (diffusive) + (bc)
  rsdPDETrue = ( 1313/60. ) + ( -21437/375. ) + ( -559/75. )
               + ( (-1/48750.)*((pow(2,-1/2.))*((-30+(13)*(sqrt(2)))*(8375+(64311)*(sqrt(2))))) ); // Basis function
  rsdAUXTrue = ( (-1/78.)*((pow(2,-1/2.))*(-4446+(2653)*(sqrt(2)))) ); // Basis function
  rsdTraceTrue = ( 559/25. ); // Basis function

  SANS_CHECK_CLOSE( rsdPDETrue, efld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXTrue, eAfld.DOF(0), small_tol, close_tol );

  SANS_CHECK_CLOSE( 0,           eIfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTraceTrue, eIfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,           eIfld.DOF(2), small_tol, close_tol );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_Dirichlet_mitState_2D_1Triangle_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCTypeDirichlet_mitState<AdvectiveFlux2D_Uniform,ViscousFlux2D_Uniform> BCTypeDirichlet_mitState2D;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeDirichlet_mitState2D> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandClass;

  // PDE
  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC
  Real uB = 12./5.;
  BCClass bc(pde, uB);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( qfld.nDOF() == 3 );

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  // lifting operator: single triangle, P1 (aka Q1)
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  afld.DOF(0) = { 2, -3};
  afld.DOF(1) = { 7,  8};
  afld.DOF(2) = {-1, -5};

  BOOST_CHECK_EQUAL( 3, afld.nDOF() );
  BOOST_CHECK( afld.D == pde.D);

  // interface solution
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( qIfld.nDOF() == 6 );

  qIfld = 0;
  // DOF data - Second face, thus DOF 2 and 3
  qIfld.DOF(2) =  8;
  qIfld.DOF(3) = -1;

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( wfld.nDOF() == 6 );

  // triangle solution (left)
  wfld.DOF(0) = -2;
  wfld.DOF(1) =  4;
  wfld.DOF(2) =  3;
  wfld.DOF(3) =  2;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  // lifting operator: single triangle, P1 (aka Q1)
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> bfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  bfld.DOF(0) = { 1,  5};
  bfld.DOF(1) = { 3,  6};
  bfld.DOF(2) = {-1,  7};
  bfld.DOF(3) = { 3,  1};
  bfld.DOF(4) = { 2,  2};
  bfld.DOF(5) = { 4,  3};

  // interface weight
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> wIfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( wIfld.nDOF() == 9 );

  wIfld = 0;
  // DOF data
  wIfld.DOF(3) =  2;
  wIfld.DOF(4) = -1;
  wIfld.DOF(5) =  3;

  Field_DG_Cell<PhysD2, TopoD2, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK( efld.nDOF() == 1 );

  efld = 0;

  // lifting operator: single line, P2 (aka Q2)
  Field_DG_Cell<PhysD2, TopoD2, Real> eAfld(xfld, 0, BasisFunctionCategory_Legendre);

  eAfld = 0;

  BOOST_CHECK_EQUAL( 1, eAfld.nElem() );
  BOOST_CHECK_EQUAL( 1, eAfld.nDOF() );
  BOOST_CHECK( eAfld.D == pde.D);

  // Lagrange multiplier
  Field_DG_Trace<PhysD2, TopoD2, Real> eIfld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK( eIfld.nDOF() == 3 );

  // error estimate field
  eIfld = 0;

  // integrand

  DiscretizationHDG<PDEClass> disc( pde, Local, Gradient );
  IntegrandClass fcn( pde, bc, {1}, disc );

  // quadrature rule
  int quadratureorder[3] = {3,3,3};

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ErrorEstimateBoundaryTrace_HDG(fcn),
                                                              xfld, (qfld,afld,wfld,bfld,efld,eAfld),
                                                              (qIfld,wIfld,eIfld), quadratureorder, 3 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  //Taken from IntegrandBoundaryTrace_LinearScalar_HDG_AD_btest
  Real rsdPDETrue, rsdAUXTrue, rsdTraceTrue;

  //PDE residuals: (advective) + (diffusive) + (stabilization)
  rsdPDETrue = ( 1313/60. ) + ( -21437/375. ) + ( -559/75. );
  rsdAUXTrue = ( -122/5. );
  rsdTraceTrue = ( 7033/100. );

  SANS_CHECK_CLOSE( rsdPDETrue, efld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXTrue, eAfld.DOF(0), small_tol, close_tol );

  SANS_CHECK_CLOSE( 0,            eIfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTraceTrue, eIfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,            eIfld.DOF(2), small_tol, close_tol );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_RobinFunction_mitState_2D_1Triangle_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCTypeFunction_mitState<AdvectiveFlux2D_Uniform,ViscousFlux2D_Uniform> BCTypeFunction_mitState2D;
  typedef BCAdvectionDiffusion<PhysD2,BCTypeFunction_mitState2D> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandClass;

  // PDE
  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC
  Real qB = 12./5.;
  Real qBx = -4./5.;
  Real qBy =  7./2.;
  BCClass::Function_ptr uexact(new ScalarFunction2D_Linear( qB, qBx, qBy ));
  bool upwind = false;
  BCClass bc(uexact, adv, visc, "Robin", upwind);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( qfld.nDOF() == 3 );

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  // lifting operator: single triangle, P1 (aka Q1)
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  afld.DOF(0) = { 2, -3};
  afld.DOF(1) = { 7,  8};
  afld.DOF(2) = {-1, -5};

  BOOST_CHECK_EQUAL( 3, afld.nDOF() );
  BOOST_CHECK( afld.D == pde.D);

  // interface solution
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( qIfld.nDOF() == 6 );

  qIfld = 0;
  // DOF data - Second face, thus DOF 2 and 3
  qIfld.DOF(2) =  8;
  qIfld.DOF(3) = -1;

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( wfld.nDOF() == 6 );

  // triangle solution (left)
  wfld.DOF(0) = -2;
  wfld.DOF(1) =  4;
  wfld.DOF(2) =  3;
  wfld.DOF(3) =  2;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  // lifting operator: single triangle, P1 (aka Q1)
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> bfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  bfld.DOF(0) = { 1,  5};
  bfld.DOF(1) = { 3,  6};
  bfld.DOF(2) = {-1,  7};
  bfld.DOF(3) = { 3,  1};
  bfld.DOF(4) = { 2,  2};
  bfld.DOF(5) = { 4,  3};

  // interface weight
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> wIfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( wIfld.nDOF() == 9 );

  wIfld = 0;
  // DOF data
  wIfld.DOF(3) =  2;
  wIfld.DOF(4) = -1;
  wIfld.DOF(5) =  3;

  Field_DG_Cell<PhysD2, TopoD2, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK( efld.nDOF() == 1 );

  efld = 0;

  // lifting operator: single line, P2 (aka Q2)
  Field_DG_Cell<PhysD2, TopoD2, Real> eAfld(xfld, 0, BasisFunctionCategory_Legendre);

  eAfld = 0;

  BOOST_CHECK_EQUAL( 1, eAfld.nElem() );
  BOOST_CHECK_EQUAL( 1, eAfld.nDOF() );
  BOOST_CHECK( eAfld.D == pde.D);

  // Lagrange multiplier
  Field_DG_Trace<PhysD2, TopoD2, Real> eIfld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK( eIfld.nDOF() == 3 );

  // error estimate field
  eIfld = 0;

  // integrand

  DiscretizationHDG<PDEClass> disc( pde, Local, Gradient );
  IntegrandClass fcn( pde, bc, {1}, disc );

  // quadrature rule
  int quadratureorder[3] = {3,3,3};

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ErrorEstimateBoundaryTrace_HDG(fcn),
                                                              xfld, (qfld,afld,wfld,bfld,efld,eAfld),
                                                              (qIfld,wIfld,eIfld), quadratureorder, 3 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  //Taken from IntegrandBoundaryTrace_LinearScalar_HDG_AD_btest
  Real rsdPDETrue, rsdAUXTrue, rsdTraceTrue;

  //PDE residuals: (advective) + (diffusive) + (stabilization)
  rsdPDETrue = ( 1313/60. ) + ( -21437/375. ) + ( -559/75. );
  rsdAUXTrue = ( -741/20. );
  rsdTraceTrue = ( 192517/2500. );

  SANS_CHECK_CLOSE( rsdPDETrue, efld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXTrue, eAfld.DOF(0), small_tol, close_tol );

  SANS_CHECK_CLOSE( 0,            eIfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTraceTrue, eIfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0,            eIfld.DOF(2), small_tol, close_tol );
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
