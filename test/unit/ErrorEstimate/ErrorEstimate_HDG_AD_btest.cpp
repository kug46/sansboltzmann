// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEstimate_DG_AD_btest
// testing of Error Estimate umbrella function for DG  with Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
//#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
//#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
//#include "pde/NDConvert/PDENDConvertSpace3D.h"
//#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_Trace.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Field/Tuple/FieldTuple.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateInteriorTraceGroups_FieldTrace.h"

#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
//#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"

#include "Discretization/HDG/IntegrandCell_HDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_Flux_mitState_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_mitLG_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_sansLG_HDG.h"

#include "ErrorEstimate/HDG/ErrorEstimate_HDG.h"
#include "ErrorEstimate/HDG/ErrorEstimateCell_HDG.h"
//#include "ErrorEstimate/HDG/ErrorEstimateInteriorTrace_HDG.h"
#include "ErrorEstimate/HDG/ErrorEstimateBoundaryTrace_FieldTrace_HDG.h"
#include "ErrorEstimate/HDG/ErrorEstimateBoundaryTrace_HDG.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEstimate_HDG_AD_test_suite )


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimate_HDG_LinearScalar_mitLG_BC_2Line_X1Q1 )
{
  typedef TopoD1 TopoDim;
  typedef PhysD1 PhysDim;
  typedef Line Topology;
  typedef Node TopologyTrace;

  typedef PDEAdvectionDiffusion<PhysDim,
          AdvectiveFlux1D_Uniform,
          ViscousFlux1D_Uniform,
          Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysDim, PDEAdvectionDiffusion1D> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysDim::D, ArrayQ> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysDim,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysDim, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandInteriorTraceClass;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandBoundaryTraceClass;

  // Algebraic Eq set things
  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_HDG<PDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Dense, XField<PhysDim, TopoDim>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef ErrorEstimate_HDG<PDEClass, BCNDConvertSpace, BCVector,XField<PhysDim,TopoDim>> ErrorEstimateClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BCs

  Real A = 1.0,B = 2.0,bcdata = 3.0; // Robin
  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  PyDict BCRobin;
  BCRobin[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BCRobin[BCAdvectionDiffusionParams<PhysDim, BCTypeLinearRobin_mitLG>::params.A] = A;
  BCRobin[BCAdvectionDiffusionParams<PhysDim, BCTypeLinearRobin_mitLG>::params.B] = B;
  BCRobin[BCAdvectionDiffusionParams<PhysDim, BCTypeLinearRobin_mitLG>::params.bcdata] = bcdata;

  PyDict BCList;
  BCList["RobinL"] = BCRobin;
  BCList["RobinR"] = BCRobin;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  //Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["RobinL"] = {0};
  BCBoundaryGroups["RobinR"] = {1};

  // Check the BC dictionary
  BCParams::checkInputs(BCList);

  // grid: single triangle, P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 2, xfld.nElem() );
  BOOST_CHECK_EQUAL( 2, xfld.nBoundaryTraceGroups() );

  XField_CellToTrace<PhysDim, TopoDim> connectivity(xfld);

  // solution: two line, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 4;
  // solution data (right)
  qfld.DOF(2) = 7;
  qfld.DOF(3) = 4;

  BOOST_CHECK_EQUAL( 4, qfld.nDOF() );
  BOOST_CHECK( qfld.D == pde.D);

  // Auxilliary variable: single line, P1 (aka Q1)
  Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // lifting operators in left element
  afld.DOF(0) =  2;
  afld.DOF(1) =  7;

  // lifting operators in right element
  afld.DOF(2) =  9;
  afld.DOF(3) = -1;

  BOOST_CHECK_EQUAL( 4, afld.nDOF() );
  BOOST_CHECK( afld.D == pde.D);

  // interface solution: P0 (aka Q1)
  Field_DG_Trace<PhysDim, TopoDim, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( qIfld.nDOF(), 3);
  BOOST_CHECK_EQUAL( qIfld.nInteriorTraceGroups(), 1 );
  BOOST_CHECK_EQUAL( qIfld.nBoundaryTraceGroups(), 2 );

  // node trace data
  qIfld.DOF(0) = -1;
  qIfld.DOF(1) =  8;
  qIfld.DOF(2) =  3;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> lgfld(xfld, 0, BasisFunctionCategory_Legendre);

  // Lagrange Multiplier DOF data
  lgfld.DOF(0) =  5;
  lgfld.DOF(1) = -7;

  BOOST_CHECK_EQUAL(2, lgfld.nDOF() );

  // weight: single line, P2 (aka Q2)
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  // line solution data (left)
  wfld.DOF(0) = 3;
  wfld.DOF(1) = 4;
  wfld.DOF(2) = 5;

  // line solution data (right)
  wfld.DOF(3) =  5;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  BOOST_CHECK_EQUAL( 6, wfld.nDOF() );
  BOOST_CHECK( wfld.D == pde.D);

  // Auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ> bfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( bfld.nDOF(), 6 );

  // lifting operators in left element
  bfld.DOF(0) = -5;
  bfld.DOF(1) =  3;
  bfld.DOF(2) =  2;

  // lifting operators in right element
  bfld.DOF(3) =  3;
  bfld.DOF(4) =  2;
  bfld.DOF(5) =  4;

  // interface solution: P1 (aka Q1)
  Field_DG_Trace<PhysDim, TopoDim, ArrayQ> wIfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( wIfld.nDOF(), 3);
  BOOST_CHECK_EQUAL( wIfld.nInteriorTraceGroups(), 1 );
  BOOST_CHECK_EQUAL( wIfld.nBoundaryTraceGroups(), 2 );

  // node trace data
  wIfld.DOF(0) =   3;
  wIfld.DOF(1) =  -2;
  wIfld.DOF(2) =   5;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> mufld(xfld, 0, BasisFunctionCategory_Legendre);

  // Lagrange Multiplier DOF data
  mufld.DOF(0) =  3;
  mufld.DOF(1) =  2;

  BOOST_CHECK_EQUAL(2, mufld.nDOF() );

  // estimate fields
  Field_DG_Cell<PhysDim, TopoDim, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysDim, TopoDim, Real> eSfld(xfld, 0, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysDim, TopoDim, Real> ifld(xfld, 0, BasisFunctionCategory_Legendre);

  // solution data
  efld = 0; eSfld = 0; ifld = 0;

  BOOST_CHECK_EQUAL( 2, efld.nElem() );
  BOOST_CHECK_EQUAL( 2, efld.nDOF() );

  // lifting operator: single line, P0 (aka Q0)
  Field_DG_Cell<PhysDim, TopoDim, Real> eAfld(xfld, 0, BasisFunctionCategory_Legendre);

  eAfld = 0;

  BOOST_CHECK_EQUAL( 2, eAfld.nElem() );
  BOOST_CHECK_EQUAL( 2, eAfld.nDOF() );

  // interface solution: P1 (aka Q1)
  Field_DG_Trace<PhysDim, TopoDim, Real> eIfld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK_EQUAL( eIfld.nDOF(), 3);
  BOOST_CHECK_EQUAL( eIfld.nInteriorTraceGroups(), 1 );
  BOOST_CHECK_EQUAL( eIfld.nBoundaryTraceGroups(), 2 );

  eIfld = 0;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysDim, TopoDim, Real> eBfld(xfld, 0, BasisFunctionCategory_Legendre);

  // Lagrange Multiplier DOF data
  eBfld = 0;

  BOOST_CHECK_EQUAL(2, eBfld.nDOF() );

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Local, Gradient );

  // cubic rule
  QuadratureOrder quadOrder(xfld, 3);

  // integrand
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandInteriorTraceClass fcnITrace( pde, disc, {0} );
  IntegrandBoundaryTraceClass fcnBTrace( pde, bc, {0,1}, disc);

  IntegrateCellGroups<TopoDim>::integrate( ErrorEstimateCell_HDG( fcnCell, fcnITrace, connectivity,
                                                                 xfld, qfld, afld, qIfld, wfld, bfld, wIfld,
                                                                 efld, eAfld, eIfld,
                                                                 quadOrder.interiorTraceOrders.data(),
                                                                 quadOrder.interiorTraceOrders.size() ),
                                          xfld, (qfld, afld, wfld, bfld, efld, eAfld),
                                          quadOrder.cellOrders.data(), quadOrder.cellOrders.size());

  IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate( ErrorEstimateBoundaryTrace_FieldTrace_HDG(fcnBTrace),
                                                               xfld, (qfld, afld, wfld, bfld, efld, eAfld),
                                                               (qIfld, lgfld, wIfld, mufld, eIfld, eBfld),
                                                               quadOrder.boundaryTraceOrders.data(),
                                                               quadOrder.boundaryTraceOrders.size() );

  for (int i = 0; i < efld.nDOF(); i++)
  {
    eSfld.DOF(i) = efld.DOF(i);
    ifld.DOF(i) = fabs(efld.DOF(i));
  }

  // Distribute the eAfld to efld by looping over elements in 1 cell group
  typedef typename XField<PhysDim, TopoDim    >::FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename Field< PhysDim,TopoDim,Real>::FieldCellGroupType<Topology> EFieldCellGroupType;
  typedef typename Field<PhysDim,TopoDim,Real >::FieldCellGroupType<Topology> EAFieldCellGroupType;
  typedef typename XFieldCellGroupType::ElementType<>  ElementXFieldClass;
  typedef typename EFieldCellGroupType::ElementType<>  ElementEFieldClass;
  typedef typename EAFieldCellGroupType::ElementType<> ElementEAFieldClass;

  XFieldCellGroupType& xfldCell = xfld.getCellGroup<Topology>(0);
  EFieldCellGroupType& eSfldCell = eSfld.getCellGroup<Topology>(0);
  EFieldCellGroupType& ifldCell = ifld.getCellGroup<Topology>(0);
  EAFieldCellGroupType& eAfldCell = eAfld.getCellGroup<Topology>(0);

  ElementXFieldClass xfldElem(xfldCell.basis() );
  ElementEFieldClass eSfldElem(eSfldCell.basis() );
  ElementEFieldClass ifldElem(ifldCell.basis() );
  ElementEAFieldClass eAfldElem(eAfldCell.basis() );

  for (int elem = 0; elem <xfldCell.nElem(); elem++)
  {
    eSfldCell.getElement( eSfldElem, elem);
    ifldCell.getElement( ifldElem, elem);
    eAfldCell.getElement( eAfldElem, elem);

    eSfldElem.DOF(0) += eAfldElem.DOF(0);
    ifldElem.DOF(0)  += fabs(eAfldElem.DOF(0));

    eSfldCell.setElement(eSfldElem,elem);
    ifldCell.setElement(ifldElem,elem);
  }

  // Distribute the eIfld to efld by looping over interior trace elements
  typedef typename XField<PhysDim,TopoDim>::FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename Field<PhysDim,TopoDim,Real>::FieldTraceGroupType<TopologyTrace> EIFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::ElementType<> ElementXFieldTraceClass;
  typedef typename EIFieldTraceGroupType::ElementType<> ElementEIFieldTraceClass;

  for (int group = 0; group< xfld.nInteriorTraceGroups(); group ++)
  {
    const XFieldTraceGroupType& xfldInteriorTrace = xfld.getInteriorTraceGroup<TopologyTrace>(group);
    EIFieldTraceGroupType& eIfldInteriorTrace = eIfld.getInteriorTraceGroup<TopologyTrace>(group);

    ElementXFieldTraceClass xfldTraceElem( xfldInteriorTrace.basis() );
    ElementEIFieldTraceClass eIfldTraceElem( eIfldInteriorTrace.basis() );
    for (int elem = 0; elem < xfldInteriorTrace.nElem(); elem++)
    {
      eIfldInteriorTrace.getElement( eIfldTraceElem, elem);

      int elemL  = xfldInteriorTrace.getElementLeft( elem );
      eSfldCell.getElement( eSfldElem, elemL );
      eSfldElem.DOF(0) += 0.5* eIfldTraceElem.DOF(0);
      eSfldCell.setElement( eSfldElem, elemL );

      ifldCell.getElement( ifldElem, elemL );
      ifldElem.DOF(0) += fabs(0.5* eIfldTraceElem.DOF(0));
      ifldCell.setElement( ifldElem, elemL );

      int elemR  = xfldInteriorTrace.getElementRight( elem );
      eSfldCell.getElement( eSfldElem, elemR );
      eSfldElem.DOF(0) += 0.5* eIfldTraceElem.DOF(0);
      eSfldCell.setElement( eSfldElem, elemR );

      ifldCell.getElement( ifldElem, elemR );
      ifldElem.DOF(0) += fabs(0.5* eIfldTraceElem.DOF(0));
      ifldCell.setElement( ifldElem, elemR );
    }
  }

  // Distribute the boundary fields to efld by looping over elements of boundary trace groups

  // boundary terms
  typedef typename XField<PhysDim, TopoDim>::FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename Field<PhysDim,TopoDim,Real>::FieldTraceGroupType<TopologyTrace> EBFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::ElementType<> ElementXFieldTraceClass;
  typedef typename EBFieldTraceGroupType::ElementType<> ElementEBFieldTraceClass;

  for (int group = 0; group< xfld.nBoundaryTraceGroups(); group++)
  {
    const XFieldTraceGroupType xfldBoundaryTrace = xfld.getBoundaryTraceGroup<TopologyTrace>(group);
    ElementXFieldTraceClass xfldTraceElem(xfldBoundaryTrace.basis() );

    EBFieldTraceGroupType& eBfldTrace = eBfld.getBoundaryTraceGroup<TopologyTrace>(group);
    EBFieldTraceGroupType& eIfldTrace = eIfld.getBoundaryTraceGroup<TopologyTrace>(group);
    ElementEBFieldTraceClass eBfldElem(eBfldTrace.basis() );
    ElementEBFieldTraceClass eIfldElem(eIfldTrace.basis() );

    // loop over elements in group
    for (int elem = 0; elem< xfldBoundaryTrace.nElem(); elem++)
    {
      eBfldTrace.getElement( eBfldElem, elem );
      eIfldTrace.getElement( eIfldElem, elem );

      int elemL = xfldBoundaryTrace.getElementLeft(elem);
      eSfldCell.getElement( eSfldElem, elemL );
      eSfldElem.DOF(0) +=  eBfldElem.DOF(0) + eIfldElem.DOF(0);
      eSfldCell.setElement( eSfldElem, elemL );

      ifldCell.getElement( ifldElem, elemL );
      ifldElem.DOF(0) += fabs(eIfldElem.DOF(0)) + fabs(eBfldElem.DOF(0));
      ifldCell.setElement( ifldElem, elemL );
    }
  }

  // Construct ErrorEstimateClass
  ErrorEstimateClass ErrorEstimate(xfld,
                                   qfld, afld, qIfld, lgfld,
                                   wfld, bfld, wIfld, mufld,
                                   pde, disc, quadOrder, {0}, {0},
                                   BCList, BCBoundaryGroups);
  const Field_DG_Cell<PhysDim,TopoDim,Real>& eSfld2 = ErrorEstimate.getESField();
  const Field_DG_Cell<PhysDim,TopoDim,Real>& ifld2 = ErrorEstimate.getIField();

  std::vector<std::vector<Real>> errorArray;
  ErrorEstimate.fillEArray(errorArray); // savithru's slightly more fragile interface

  BOOST_CHECK_EQUAL( xfld.nElem(), eSfld2.nElem() );
  BOOST_CHECK_EQUAL( xfld.nElem(), eSfld2.nDOF() );

  Real globalEstimate=0,trueSum=0;
  Real globalAggregate=0,trueAgg=0;
  ErrorEstimate.getErrorEstimate(globalEstimate);
  ErrorEstimate.getErrorIndicator(globalAggregate);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int dof = 0; dof < eSfld.nDOF(); dof++)
  {
    SANS_CHECK_CLOSE( eSfld.DOF(dof), eSfld2.DOF(dof), small_tol, close_tol );
    SANS_CHECK_CLOSE( ifld.DOF(dof),  ifld2.DOF(dof), small_tol, close_tol );
    trueSum += eSfld.DOF(dof);
    trueAgg += fabs(ifld.DOF(dof));
  }

  SANS_CHECK_CLOSE( trueSum, globalEstimate, small_tol, close_tol );
  SANS_CHECK_CLOSE( trueAgg, globalAggregate, small_tol, close_tol );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimate_HDG_LinearScalar_sansLG_mitState_BC_2Line_X1Q1 )
{
  typedef TopoD1 TopoDim;
  typedef PhysD1 PhysDim;
  typedef Line Topology;
  typedef Node TopologyTrace;

  typedef PDEAdvectionDiffusion<PhysDim,
      AdvectiveFlux1D_Uniform,
      ViscousFlux1D_Uniform,
      Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysDim, PDEAdvectionDiffusion1D> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysDim::D, ArrayQ> VectorArrayQ;

  typedef BCNDConvertSpace<PhysDim,BCAdvectionDiffusion<PhysDim,BCTypeLinearRobin_sansLG>>BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandInteriorTraceClass;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandBoundaryTraceClass;

  // Algebraic Eq set things
  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_HDG<PDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Dense, XField<PhysDim, TopoDim>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef ErrorEstimate_HDG<PDEClass, BCNDConvertSpace, BCVector,XField<PhysDim,TopoDim>> ErrorEstimateClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BCs
  Real A = 1.0,B = 2.0,bcdata = 3.0; // Robin
  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  PyDict BCRobin;
  BCRobin[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCRobin[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_mitLG>::params.A] = A;
  BCRobin[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_mitLG>::params.B] = B;
  BCRobin[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_mitLG>::params.bcdata] = bcdata;

  PyDict BCList;
  BCList["RobinL"] = BCRobin;
  BCList["RobinR"] = BCRobin;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  //Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["RobinL"] = {0};
  BCBoundaryGroups["RobinR"] = {1};

  // Check the BC dictionary
  BCParams::checkInputs(BCList);

  // grid: single triangle, P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 2, xfld.nElem() );
  BOOST_CHECK_EQUAL( 2, xfld.nBoundaryTraceGroups() );

  XField_CellToTrace<PhysDim, TopoDim> connectivity(xfld);

  // solution: single line, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution data (left)
  qfld.DOF(0) = 2;
  qfld.DOF(1) = 3;
  // solution data (right)
  qfld.DOF(2) = 7;
  qfld.DOF(3) = 4;

  BOOST_CHECK_EQUAL( 4, qfld.nDOF() );
  BOOST_CHECK( qfld.D == pde.D);

  // lifting operator: single line, P1 (aka Q1)
  Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // lifting operators in left element
  afld.DOF(0) =  2;
  afld.DOF(1) =  7;

  // lifting operators in right element
  afld.DOF(2) =  9;
  afld.DOF(3) = -1;

  BOOST_CHECK_EQUAL( 4, afld.nDOF() );
  BOOST_CHECK( afld.D == pde.D);

  // interface solution: P0 (aka Q1)
  Field_DG_Trace<PhysDim, TopoDim, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( qIfld.nDOF(), 3);
  BOOST_CHECK_EQUAL( qIfld.nInteriorTraceGroups(), 1 );
  BOOST_CHECK_EQUAL( qIfld.nBoundaryTraceGroups(), 2 );

  // node trace data
  qIfld.DOF(0) = -1;
  qIfld.DOF(1) =  8;
  qIfld.DOF(2) =  3;

  // weight: single line, P2 (aka Q2)
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  // line solution data (left)
  wfld.DOF(0) = 3;
  wfld.DOF(1) = 4;
  wfld.DOF(2) = 5;

  // line solution data (right)
  wfld.DOF(3) =  5;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  BOOST_CHECK_EQUAL( 6, wfld.nDOF() );
  BOOST_CHECK( wfld.D == pde.D);

  // Auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ> bfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( bfld.nDOF(), 6 );

  // lifting operators in left element
  bfld.DOF(0) = -5;
  bfld.DOF(1) =  3;
  bfld.DOF(2) =  2;

  // lifting operators in right element
  bfld.DOF(3) =  3;
  bfld.DOF(4) =  2;
  bfld.DOF(5) =  4;

  // interface solution: P1 (aka Q1)
  Field_DG_Trace<PhysDim, TopoDim, ArrayQ> wIfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( wIfld.nDOF(), 3);
  BOOST_CHECK_EQUAL( wIfld.nInteriorTraceGroups(), 1 );
  BOOST_CHECK_EQUAL( wIfld.nBoundaryTraceGroups(), 2 );

  // node trace data
  wIfld.DOF(0) =   3;
  wIfld.DOF(1) =  -2;
  wIfld.DOF(2) =   5;

  // estimate fields
  Field_DG_Cell<PhysDim, TopoDim, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysDim, TopoDim, Real> eSfld(xfld, 0, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysDim, TopoDim, Real> ifld(xfld, 0, BasisFunctionCategory_Legendre);

  // solution data
  efld = 0; eSfld = 0; ifld = 0;

  BOOST_CHECK_EQUAL( 2, efld.nElem() );
  BOOST_CHECK_EQUAL( 2, efld.nDOF() );

  // lifting operator: single line, P0 (aka Q0)
  Field_DG_Cell<PhysDim, TopoDim, Real> eAfld(xfld, 0, BasisFunctionCategory_Legendre);

  eAfld = 0;

  BOOST_CHECK_EQUAL( 2, eAfld.nElem() );
  BOOST_CHECK_EQUAL( 2, eAfld.nDOF() );

  // interface solution: P1 (aka Q1)
  Field_DG_Trace<PhysDim, TopoDim, Real> eIfld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK_EQUAL( eIfld.nDOF(), 3);
  BOOST_CHECK_EQUAL( eIfld.nInteriorTraceGroups(), 1 );

  eIfld = 0;

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Local );

  // cubic rule
  QuadratureOrder quadOrder(xfld, 3);

  // integrand
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandInteriorTraceClass fcnITrace( pde, disc, {0} );
  IntegrandBoundaryTraceClass fcnBTrace( pde, bc, {0,1}, disc);

  IntegrateCellGroups<TopoDim>::integrate( ErrorEstimateCell_HDG( fcnCell, fcnITrace, connectivity,
                                                                 xfld, qfld, afld, qIfld, wfld, bfld, wIfld,
                                                                 efld, eAfld, eIfld,
                                                                 quadOrder.interiorTraceOrders.data(),
                                                                 quadOrder.interiorTraceOrders.size() ),
                                          xfld, (qfld, afld, wfld, bfld, efld, eAfld),
                                          quadOrder.cellOrders.data(), quadOrder.cellOrders.size());

  IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate( ErrorEstimateBoundaryTrace_HDG(fcnBTrace),
                                                               xfld, (qfld, afld, wfld, bfld, efld, eAfld),
                                                               (qIfld, wIfld, eIfld),
                                                               quadOrder.boundaryTraceOrders.data(),
                                                               quadOrder.boundaryTraceOrders.size() );

  for (int i = 0; i < efld.nDOF(); i++)
  {
    eSfld.DOF(i) = efld.DOF(i);
    ifld.DOF(i) = fabs(efld.DOF(i));
  }

  // Distribute the eAfld to efld by looping over elements in 1 cell group
  typedef typename XField<PhysDim, TopoDim      >::FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename Field< PhysDim,TopoDim,Real  >::FieldCellGroupType<Topology> EFieldCellGroupType;
  typedef typename Field<PhysDim,TopoDim,Real>::FieldCellGroupType<Topology> EAFieldCellGroupType;
  typedef typename XFieldCellGroupType::ElementType<>  ElementXFieldClass;
  typedef typename EFieldCellGroupType::ElementType<>  ElementEFieldClass;
  typedef typename EAFieldCellGroupType::ElementType<> ElementEAFieldClass;

  XFieldCellGroupType& xfldCell = xfld.getCellGroup<Topology>(0);
  EFieldCellGroupType& eSfldCell = eSfld.getCellGroup<Topology>(0);
  EFieldCellGroupType& ifldCell = ifld.getCellGroup<Topology>(0);
  EAFieldCellGroupType& eAfldCell = eAfld.getCellGroup<Topology>(0);

  ElementXFieldClass xfldElem(xfldCell.basis() );
  ElementEFieldClass eSfldElem(eSfldCell.basis() );
  ElementEFieldClass ifldElem(ifldCell.basis() );
  ElementEAFieldClass eAfldElem(eAfldCell.basis() );

  for (int elem = 0; elem <xfldCell.nElem(); elem++)
  {
    eSfldCell.getElement( eSfldElem, elem);
    ifldCell.getElement( ifldElem, elem);
    eAfldCell.getElement( eAfldElem, elem);

    eSfldElem.DOF(0) += eAfldElem.DOF(0);
    ifldElem.DOF(0)  += fabs(eAfldElem.DOF(0));

    eSfldCell.setElement(eSfldElem,elem);
    ifldCell.setElement(ifldElem,elem);
  }
  // Distribute the eIfld to efld by looping over interior trace elements
  typedef typename XField<PhysDim,TopoDim>::FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename Field<PhysDim,TopoDim,Real>::FieldTraceGroupType<TopologyTrace> EIFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::ElementType<> ElementXFieldTraceClass;
  typedef typename EIFieldTraceGroupType::ElementType<> ElementEIFieldTraceClass;

  for (int group = 0; group< xfld.nInteriorTraceGroups(); group ++)
  {
    const XFieldTraceGroupType& xfldInteriorTrace = xfld.getInteriorTraceGroup<TopologyTrace>(group);
    EIFieldTraceGroupType& eIfldInteriorTrace = eIfld.getInteriorTraceGroup<TopologyTrace>(group);

    ElementXFieldTraceClass xfldTraceElem( xfldInteriorTrace.basis() );
    ElementEIFieldTraceClass eIfldTraceElem( eIfldInteriorTrace.basis() );

    for (int elem = 0; elem < xfldInteriorTrace.nElem(); elem++)
    {
      eIfldInteriorTrace.getElement( eIfldTraceElem, elem);

      int elemL  = xfldInteriorTrace.getElementLeft( elem );
      eSfldCell.getElement( eSfldElem, elemL );
      eSfldElem.DOF(0) += 0.5* eIfldTraceElem.DOF(0);
      eSfldCell.setElement( eSfldElem, elemL );

      ifldCell.getElement( ifldElem, elemL );
      ifldElem.DOF(0) += fabs(0.5* eIfldTraceElem.DOF(0));
      ifldCell.setElement( ifldElem, elemL );

      int elemR  = xfldInteriorTrace.getElementRight( elem );
      eSfldCell.getElement( eSfldElem, elemR );
      eSfldElem.DOF(0) += 0.5* eIfldTraceElem.DOF(0);
      eSfldCell.setElement( eSfldElem, elemR );

      ifldCell.getElement( ifldElem, elemR );
      ifldElem.DOF(0) += fabs(0.5* eIfldTraceElem.DOF(0));
      ifldCell.setElement( ifldElem, elemR );
    }
  }

  // Distribute the boundary fields to efld by looping over elements of boundary trace groups

  // boundary terms
  typedef typename XField<PhysDim, TopoDim>::FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename Field<PhysDim,TopoDim,Real>::FieldTraceGroupType<TopologyTrace> EBFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::ElementType<> ElementXFieldTraceClass;
  typedef typename EBFieldTraceGroupType::ElementType<> ElementEBFieldTraceClass;

  for (int group = 0; group< xfld.nBoundaryTraceGroups(); group++)
  {
    const XFieldTraceGroupType xfldBoundaryTrace = xfld.getBoundaryTraceGroup<TopologyTrace>(group);
    ElementXFieldTraceClass xfldTraceElem(xfldBoundaryTrace.basis() );

    EBFieldTraceGroupType& eIfldTrace = eIfld.getBoundaryTraceGroup<TopologyTrace>(group);
    ElementEBFieldTraceClass eIfldElem(eIfldTrace.basis() );

    // loop over elements in group
    for (int elem = 0; elem< xfldBoundaryTrace.nElem(); elem++)
    {
      eIfldTrace.getElement( eIfldElem, elem );

      int elemL = xfldBoundaryTrace.getElementLeft(elem);
      eSfldCell.getElement( eSfldElem, elemL );
      eSfldElem.DOF(0) +=   eIfldElem.DOF(0);
      eSfldCell.setElement( eSfldElem, elemL );

      ifldCell.getElement( ifldElem, elemL );
      ifldElem.DOF(0) += fabs(eIfldElem.DOF(0));
      ifldCell.setElement( ifldElem, elemL );
    }
  }

  // Needed to get past constructor, really want to get rid of
  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> dumBfld1(xfld, 0, BasisFunctionCategory_Legendre,
                                                          BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups) );
  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> dumBfld2(xfld, 0, BasisFunctionCategory_Legendre,
                                                          BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups));
  dumBfld1=0; dumBfld2=0;

  // Construct ErrorEstimateClass
  ErrorEstimateClass ErrorEstimate(xfld,
                                   qfld, afld, qIfld, dumBfld1,
                                   wfld, bfld, wIfld, dumBfld2,
                                   pde, disc, quadOrder, {0},{0},
                                   BCList, BCBoundaryGroups);
  const Field_DG_Cell<PhysDim,TopoDim,Real>& eSfld2 = ErrorEstimate.getESField();
  const Field_DG_Cell<PhysDim,TopoDim,Real>& ifld2 = ErrorEstimate.getIField();

  std::vector<std::vector<Real>> errorArray;
  ErrorEstimate.fillEArray(errorArray); // savithru's slightly more fragile interface

  BOOST_CHECK_EQUAL( xfld.nElem(), eSfld2.nElem() );
  BOOST_CHECK_EQUAL( xfld.nElem(), eSfld2.nDOF() );

  Real globalEstimate=0,trueSum=0;
  Real globalAggregate=0,trueAgg=0;
  ErrorEstimate.getErrorEstimate(globalEstimate);
  ErrorEstimate.getErrorIndicator(globalAggregate);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int dof = 0; dof < eSfld.nDOF(); dof++)
  {
    SANS_CHECK_CLOSE( eSfld.DOF(dof), eSfld2.DOF(dof), small_tol, close_tol );
    SANS_CHECK_CLOSE( ifld.DOF(dof),  ifld2.DOF(dof), small_tol, close_tol );
    trueSum += eSfld.DOF(dof);
    trueAgg += fabs(ifld.DOF(dof));
  }

  SANS_CHECK_CLOSE( trueSum, globalEstimate, small_tol, close_tol );
  SANS_CHECK_CLOSE( trueAgg, globalAggregate, small_tol, close_tol );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimate_HDG_Dirichlet_mitState_BC_2Line_X1Q1 )
{
  typedef TopoD1 TopoDim;
  typedef PhysD1 PhysDim;
  typedef Line Topology;
  typedef Node TopologyTrace;

  typedef PDEAdvectionDiffusion<PhysDim,
      AdvectiveFlux1D_Uniform,
      ViscousFlux1D_Uniform,
      Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysDim, PDEAdvectionDiffusion1D> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysDim::D, ArrayQ> VectorArrayQ;

  typedef BCTypeDirichlet_mitState<AdvectiveFlux1D_Uniform,ViscousFlux1D_Uniform> BCTypeDirichlet_mitState1D;

  typedef BCNDConvertSpace<PhysDim,BCAdvectionDiffusion<PhysDim,BCTypeDirichlet_mitState1D>>BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandInteriorTraceClass;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandBoundaryTraceClass;

  // Algebraic Eq set things
  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_HDG<PDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Dense, XField<PhysDim, TopoDim>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef ErrorEstimate_HDG<PDEClass, BCNDConvertSpace, BCVector,XField<PhysDim,TopoDim>> ErrorEstimateClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC
  Real uB = 12./5.;
  BCClass bc(pde, uB);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  PyDict BCDirichlet;
  BCDirichlet[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichlet[BCAdvectionDiffusionParams<PhysDim, BCTypeDirichlet_mitStateParam>::params.qB] = uB;

  PyDict BCList;
  BCList["DirichletL"] = BCDirichlet;
  BCList["DirichletR"] = BCDirichlet;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  //Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["DirichletL"] = {0};
  BCBoundaryGroups["DirichletR"] = {1};

  // Check the BC dictionary
  BCParams::checkInputs(BCList);

  // grid: single triangle, P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 2, xfld.nElem() );
  BOOST_CHECK_EQUAL( 2, xfld.nBoundaryTraceGroups() );

  XField_CellToTrace<PhysDim, TopoDim> connectivity(xfld);

  // solution: single line, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution data (left)
  qfld.DOF(0) = 2;
  qfld.DOF(1) = 3;
  // solution data (right)
  qfld.DOF(2) = 7;
  qfld.DOF(3) = 4;

  BOOST_CHECK_EQUAL( 4, qfld.nDOF() );
  BOOST_CHECK( qfld.D == pde.D);

  // lifting operator: single line, P1 (aka Q1)
  Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // lifting operators in left element
  afld.DOF(0) =  2;
  afld.DOF(1) =  7;

  // lifting operators in right element
  afld.DOF(2) =  9;
  afld.DOF(3) = -1;

  BOOST_CHECK_EQUAL( 4, afld.nDOF() );
  BOOST_CHECK( afld.D == pde.D);

  // interface solution: P0 (aka Q1)
  Field_DG_Trace<PhysDim, TopoDim, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( qIfld.nDOF(), 3);
  BOOST_CHECK_EQUAL( qIfld.nInteriorTraceGroups(), 1 );
  BOOST_CHECK_EQUAL( qIfld.nBoundaryTraceGroups(), 2 );

  // node trace data
  qIfld.DOF(0) = -1;
  qIfld.DOF(1) =  8;
  qIfld.DOF(2) =  3;

  // weight: single line, P2 (aka Q2)
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  // line solution data (left)
  wfld.DOF(0) = 3;
  wfld.DOF(1) = 4;
  wfld.DOF(2) = 5;

  // line solution data (right)
  wfld.DOF(3) =  5;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  BOOST_CHECK_EQUAL( 6, wfld.nDOF() );
  BOOST_CHECK( wfld.D == pde.D);

  // Auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ> bfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( bfld.nDOF(), 6 );

  // lifting operators in left element
  bfld.DOF(0) = -5;
  bfld.DOF(1) =  3;
  bfld.DOF(2) =  2;

  // lifting operators in right element
  bfld.DOF(3) =  3;
  bfld.DOF(4) =  2;
  bfld.DOF(5) =  4;

  // interface solution: P1 (aka Q1)
  Field_DG_Trace<PhysDim, TopoDim, ArrayQ> wIfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( wIfld.nDOF(), 3);
  BOOST_CHECK_EQUAL( wIfld.nInteriorTraceGroups(), 1 );
  BOOST_CHECK_EQUAL( wIfld.nBoundaryTraceGroups(), 2 );

  // node trace data
  wIfld.DOF(0) =   3;
  wIfld.DOF(1) =  -2;
  wIfld.DOF(2) =   5;

  // estimate fields
  Field_DG_Cell<PhysDim, TopoDim, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysDim, TopoDim, Real> eSfld(xfld, 0, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysDim, TopoDim, Real> ifld(xfld, 0, BasisFunctionCategory_Legendre);

  // solution data
  efld = 0; eSfld = 0; ifld = 0;

  BOOST_CHECK_EQUAL( 2, efld.nElem() );
  BOOST_CHECK_EQUAL( 2, efld.nDOF() );

  // lifting operator: single line, P0 (aka Q0)
  Field_DG_Cell<PhysDim, TopoDim, Real> eAfld(xfld, 0, BasisFunctionCategory_Legendre);

  eAfld = 0;

  BOOST_CHECK_EQUAL( 2, eAfld.nElem() );
  BOOST_CHECK_EQUAL( 2, eAfld.nDOF() );

  // interface solution: P1 (aka Q1)
  Field_DG_Trace<PhysDim, TopoDim, Real> eIfld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK_EQUAL( eIfld.nDOF(), 3);
  BOOST_CHECK_EQUAL( eIfld.nInteriorTraceGroups(), 1 );

  eIfld = 0;

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Local );

  // cubic rule
  QuadratureOrder quadOrder(xfld, 3);

  // integrand
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandInteriorTraceClass fcnITrace( pde, disc, {0} );
  IntegrandBoundaryTraceClass fcnBTrace( pde, bc, {0,1}, disc);

  IntegrateCellGroups<TopoDim>::integrate( ErrorEstimateCell_HDG( fcnCell, fcnITrace, connectivity,
                                                                 xfld, qfld, afld, qIfld, wfld, bfld, wIfld,
                                                                 efld, eAfld, eIfld,
                                                                 quadOrder.interiorTraceOrders.data(),
                                                                 quadOrder.interiorTraceOrders.size() ),
                                          xfld, (qfld, afld, wfld, bfld, efld, eAfld),
                                          quadOrder.cellOrders.data(), quadOrder.cellOrders.size());

  IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate( ErrorEstimateBoundaryTrace_HDG(fcnBTrace),
                                                               xfld, (qfld, afld, wfld, bfld, efld, eAfld),
                                                               (qIfld, wIfld, eIfld),
                                                               quadOrder.boundaryTraceOrders.data(),
                                                               quadOrder.boundaryTraceOrders.size() );


  for (int i = 0; i < efld.nDOF(); i++)
  {
    eSfld.DOF(i) = efld.DOF(i);
    ifld.DOF(i) = fabs(efld.DOF(i));
  }

  // Distribute the eAfld to efld by looping over elements in 1 cell group
  typedef typename XField<PhysDim, TopoDim      >::FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename Field< PhysDim,TopoDim,Real  >::FieldCellGroupType<Topology> EFieldCellGroupType;
  typedef typename Field<PhysDim,TopoDim,Real>::FieldCellGroupType<Topology> EAFieldCellGroupType;
  typedef typename XFieldCellGroupType::ElementType<>  ElementXFieldClass;
  typedef typename EFieldCellGroupType::ElementType<>  ElementEFieldClass;
  typedef typename EAFieldCellGroupType::ElementType<> ElementEAFieldClass;

  XFieldCellGroupType& xfldCell = xfld.getCellGroup<Topology>(0);
  EFieldCellGroupType& eSfldCell = eSfld.getCellGroup<Topology>(0);
  EFieldCellGroupType& ifldCell = ifld.getCellGroup<Topology>(0);
  EAFieldCellGroupType& eAfldCell = eAfld.getCellGroup<Topology>(0);

  ElementXFieldClass xfldElem(xfldCell.basis() );
  ElementEFieldClass eSfldElem(eSfldCell.basis() );
  ElementEFieldClass ifldElem(ifldCell.basis() );
  ElementEAFieldClass eAfldElem(eAfldCell.basis() );

  for (int elem = 0; elem <xfldCell.nElem(); elem++)
  {
    eSfldCell.getElement( eSfldElem, elem);
    ifldCell.getElement( ifldElem, elem);
    eAfldCell.getElement( eAfldElem, elem);

    eSfldElem.DOF(0) += eAfldElem.DOF(0);
    ifldElem.DOF(0)  += fabs(eAfldElem.DOF(0));

    eSfldCell.setElement(eSfldElem,elem);
    ifldCell.setElement(ifldElem,elem);
  }
  // Distribute the eIfld to efld by looping over interior trace elements
  typedef typename XField<PhysDim,TopoDim>::FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename Field<PhysDim,TopoDim,Real>::FieldTraceGroupType<TopologyTrace> EIFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::ElementType<> ElementXFieldTraceClass;
  typedef typename EIFieldTraceGroupType::ElementType<> ElementEIFieldTraceClass;

  for (int group = 0; group< xfld.nInteriorTraceGroups(); group ++)
  {
    const XFieldTraceGroupType& xfldInteriorTrace = xfld.getInteriorTraceGroup<TopologyTrace>(group);
    EIFieldTraceGroupType& eIfldInteriorTrace = eIfld.getInteriorTraceGroup<TopologyTrace>(group);

    ElementXFieldTraceClass xfldTraceElem( xfldInteriorTrace.basis() );
    ElementEIFieldTraceClass eIfldTraceElem( eIfldInteriorTrace.basis() );

    for (int elem = 0; elem < xfldInteriorTrace.nElem(); elem++)
    {
      eIfldInteriorTrace.getElement( eIfldTraceElem, elem);

      int elemL  = xfldInteriorTrace.getElementLeft( elem );
      eSfldCell.getElement( eSfldElem, elemL );
      eSfldElem.DOF(0) += 0.5* eIfldTraceElem.DOF(0);
      eSfldCell.setElement( eSfldElem, elemL );

      ifldCell.getElement( ifldElem, elemL );
      ifldElem.DOF(0) += fabs(0.5* eIfldTraceElem.DOF(0));
      ifldCell.setElement( ifldElem, elemL );

      int elemR  = xfldInteriorTrace.getElementRight( elem );
      eSfldCell.getElement( eSfldElem, elemR );
      eSfldElem.DOF(0) += 0.5* eIfldTraceElem.DOF(0);
      eSfldCell.setElement( eSfldElem, elemR );

      ifldCell.getElement( ifldElem, elemR );
      ifldElem.DOF(0) += fabs(0.5* eIfldTraceElem.DOF(0));
      ifldCell.setElement( ifldElem, elemR );
    }
  }

  // Distribute the boundary fields to efld by looping over elements of boundary trace groups

  // boundary terms
  typedef typename XField<PhysDim, TopoDim>::FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename Field<PhysDim,TopoDim,Real>::FieldTraceGroupType<TopologyTrace> EBFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::ElementType<> ElementXFieldTraceClass;
  typedef typename EBFieldTraceGroupType::ElementType<> ElementEBFieldTraceClass;

  for (int group = 0; group< xfld.nBoundaryTraceGroups(); group++)
  {
    const XFieldTraceGroupType xfldBoundaryTrace = xfld.getBoundaryTraceGroup<TopologyTrace>(group);
    ElementXFieldTraceClass xfldTraceElem(xfldBoundaryTrace.basis() );

    EBFieldTraceGroupType& eIfldTrace = eIfld.getBoundaryTraceGroup<TopologyTrace>(group);
    ElementEBFieldTraceClass eIfldElem(eIfldTrace.basis() );

    // loop over elements in group
    for (int elem = 0; elem< xfldBoundaryTrace.nElem(); elem++)
    {
      eIfldTrace.getElement( eIfldElem, elem );

      int elemL = xfldBoundaryTrace.getElementLeft(elem);
      eSfldCell.getElement( eSfldElem, elemL );
      eSfldElem.DOF(0) +=   eIfldElem.DOF(0);
      eSfldCell.setElement( eSfldElem, elemL );

      ifldCell.getElement( ifldElem, elemL );
      ifldElem.DOF(0) += fabs(eIfldElem.DOF(0));
      ifldCell.setElement( ifldElem, elemL );
    }
  }

  // Needed to get past constructor, really want to get rid of
  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> dumBfld1(xfld, 0, BasisFunctionCategory_Legendre,
                                                          BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups) );
  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> dumBfld2(xfld, 0, BasisFunctionCategory_Legendre,
                                                          BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups));
  dumBfld1=0; dumBfld2=0;

  // Construct ErrorEstimateClass
  ErrorEstimateClass ErrorEstimate(xfld,
                                   qfld, afld, qIfld, dumBfld1,
                                   wfld, bfld, wIfld, dumBfld2,
                                   pde, disc, quadOrder, {0},{0},
                                   BCList, BCBoundaryGroups);
  const Field_DG_Cell<PhysDim,TopoDim,Real>& eSfld2 = ErrorEstimate.getESField();
  const Field_DG_Cell<PhysDim,TopoDim,Real>& ifld2 = ErrorEstimate.getIField();

  std::vector<std::vector<Real>> errorArray;
  ErrorEstimate.fillEArray(errorArray); // savithru's slightly more fragile interface

  BOOST_CHECK_EQUAL( xfld.nElem(), eSfld2.nElem() );
  BOOST_CHECK_EQUAL( xfld.nElem(), eSfld2.nDOF() );

  Real globalEstimate=0,trueSum=0;
  Real globalAggregate=0,trueAgg=0;
  ErrorEstimate.getErrorEstimate(globalEstimate);
  ErrorEstimate.getErrorIndicator(globalAggregate);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int dof = 0; dof < eSfld.nDOF(); dof++)
  {
    SANS_CHECK_CLOSE( eSfld.DOF(dof), eSfld2.DOF(dof), small_tol, close_tol );
    SANS_CHECK_CLOSE( ifld.DOF(dof),  ifld2.DOF(dof), small_tol, close_tol );
    trueSum += eSfld.DOF(dof);
    trueAgg += fabs(ifld.DOF(dof));
  }

  SANS_CHECK_CLOSE( trueSum, globalEstimate, small_tol, close_tol );
  SANS_CHECK_CLOSE( trueAgg, globalAggregate, small_tol, close_tol );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimate_HDG_LinearScalar_mitLG_BC_2Triangle_X1Q1 )
{
  typedef TopoD2 TopoDim;
  typedef PhysD2 PhysDim;
  typedef Triangle Topology;
  typedef Line TopologyTrace;

  typedef PDEAdvectionDiffusion<PhysDim,
      AdvectiveFlux2D_Uniform,
      ViscousFlux2D_Uniform,
      Source2D_None> PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysDim::D, ArrayQ> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysDim,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysDim, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandInteriorTraceClass;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandBoundaryTraceClass;

  // Algebraic Eq set things
  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef AlgebraicEquationSet_HDG<PDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Dense, XField<PhysDim, TopoDim>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef ErrorEstimate_HDG<PDEClass, BCNDConvertSpace, BCVector,XField<PhysDim,TopoDim>> ErrorEstimateClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BCs
  Real A=2.1,B=3.1,bcdata=4.1;
  BCClass bc(A,B,bcdata);

  PyDict BCRobin;
  BCRobin[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BCRobin[BCAdvectionDiffusionParams<PhysDim, BCTypeLinearRobin_mitLG>::params.A] = A;
  BCRobin[BCAdvectionDiffusionParams<PhysDim, BCTypeLinearRobin_mitLG>::params.B] = B;
  BCRobin[BCAdvectionDiffusionParams<PhysDim, BCTypeLinearRobin_mitLG>::params.bcdata] = bcdata;

  PyDict BCList;
  BCList["Boundary"] = BCRobin;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  BCBoundaryGroups["Boundary"] = {0};

  // Check the BC dictionary
  BCParams::checkInputs(BCList);

  // grid: single triangle, P1 (aka X1)
  XField2D_2Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 4, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 2, xfld.nElem() );

  XField_CellToTrace<PhysDim, TopoDim> connectivity(xfld);

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;
  // triangle solution data (right)
  qfld.DOF(3) = 7;
  qfld.DOF(4) = 2;
  qfld.DOF(5) = 9;

  BOOST_CHECK_EQUAL( 6, qfld.nDOF() );

  // Auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( afld.nDOF(), 6 );

  // triangle auxiliary variable (left)
  afld.DOF(0) = { 2, -3};
  afld.DOF(1) = { 7,  8};
  afld.DOF(2) = {-1, -5};

  // triangle auxiliary variable (right)
  afld.DOF(3) = { 9,  6};
  afld.DOF(4) = {-1,  3};
  afld.DOF(5) = { 2, -3};

  // interface solution
  Field_DG_Trace<PhysDim, TopoDim, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( qIfld.nDOF(), 5*2);
  BOOST_CHECK_EQUAL( qIfld.nInteriorTraceGroups(), 1 );
  BOOST_CHECK_EQUAL( qIfld.nBoundaryTraceGroups(), 1 );

  // node trace data
  for (int k = 0; k < qIfld.nDOF(); k++)
    qIfld.DOF(k) = pow(-1,k)*k;

  // Lagrange Multiplier
  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> lgfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  for (int i = 0; i<lgfld.nDOF();i++)
    lgfld.DOF(i) = i; // the values don't really matter

  BOOST_CHECK_EQUAL(8, lgfld.nDOF());

  // weight: single triangle, P2 (aka Q1)
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  // triangle solution data (left)
  wfld.DOF(0) = -2;
  wfld.DOF(1) =  4;
  wfld.DOF(2) =  3;
  wfld.DOF(3) =  2;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  // triangle solution data (right)
  wfld.DOF(6)  =  7;
  wfld.DOF(7)  =  3;
  wfld.DOF(8)  =  2;
  wfld.DOF(9)  = -1;
  wfld.DOF(10) =  5;
  wfld.DOF(11) = -3;

  // Auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ> bfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( bfld.nDOF(), 12 );

  // triangle auxiliary variable (left)
  bfld.DOF(0) = { 1,  5};
  bfld.DOF(1) = { 3,  6};
  bfld.DOF(2) = {-1,  7};
  bfld.DOF(3) = { 3,  1};
  bfld.DOF(4) = { 2,  2};
  bfld.DOF(5) = { 4,  3};

  // triangle auxiliary variable (right)
  bfld.DOF(6)  = { 5,  6};
  bfld.DOF(7)  = { 4,  5};
  bfld.DOF(8)  = { 3,  4};
  bfld.DOF(9)  = { 4,  3};
  bfld.DOF(10) = { 3,  2};
  bfld.DOF(11) = { 2,  1};

  // interface solution
  Field_DG_Trace<PhysDim, TopoDim, ArrayQ> wIfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( wIfld.nDOF(), 5*3);
  BOOST_CHECK_EQUAL( wIfld.nInteriorTraceGroups(), 1 );
  BOOST_CHECK_EQUAL( wIfld.nBoundaryTraceGroups(), 1 );

  // node trace data
  for (int k = 0; k < wIfld.nDOF(); k++)
    wIfld.DOF(k) = pow(-1,k)*k;

  // Lagrange Multiplier
  Field_DG_BoundaryTrace<PhysDim,TopoDim,ArrayQ> mufld( xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  for (int i=0;i<mufld.nDOF();i++)
    mufld.DOF(i) = pow(-1,i)*i;

  BOOST_CHECK_EQUAL(12, mufld.nDOF() );

  // estimate fields
  Field_DG_Cell<PhysDim, TopoDim, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysDim, TopoDim, Real> eSfld(xfld, 0, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysDim, TopoDim, Real> ifld(xfld, 0, BasisFunctionCategory_Legendre);

  // solution data
  efld = 0; eSfld = 0; ifld = 0;

  BOOST_CHECK_EQUAL( 2, efld.nElem() );
  BOOST_CHECK_EQUAL( 2, efld.nDOF() );

  // lifting operator: single line, P0 (aka Q0)
  Field_DG_Cell<PhysDim, TopoDim, Real> eAfld(xfld, 0, BasisFunctionCategory_Legendre);

  eAfld=0;

  // interface solution
  Field_DG_Trace<PhysDim, TopoDim, ArrayQ> eIfld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK_EQUAL( eIfld.nDOF(), 5);
  BOOST_CHECK_EQUAL( eIfld.nInteriorTraceGroups(), 1 );
  BOOST_CHECK_EQUAL( eIfld.nBoundaryTraceGroups(), 1 );

  eIfld = 0;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysDim, TopoDim, Real> eBfld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK_EQUAL( 4, eBfld.nDOF() );

  // error estimate field
  eBfld = 0;

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Local );

  QuadratureOrder quadOrder(xfld, 3);

  // integrand
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandInteriorTraceClass fcnITrace( pde, disc, {0} );
  IntegrandBoundaryTraceClass fcnBTrace( pde, bc, {0}, disc );

  IntegrateCellGroups<TopoDim>::integrate( ErrorEstimateCell_HDG( fcnCell, fcnITrace, connectivity,
                                                                 xfld, qfld, afld, qIfld, wfld, bfld, wIfld,
                                                                 efld, eAfld, eIfld,
                                                                 quadOrder.interiorTraceOrders.data(),
                                                                 quadOrder.interiorTraceOrders.size() ),
                                          xfld, (qfld, afld, wfld, bfld, efld, eAfld),
                                          quadOrder.cellOrders.data(), quadOrder.cellOrders.size());

  IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate( ErrorEstimateBoundaryTrace_FieldTrace_HDG(fcnBTrace),
                                                               xfld, (qfld, afld, wfld, bfld, efld, eAfld),
                                                               (qIfld, lgfld, wIfld, mufld, eIfld, eBfld),
                                                               quadOrder.boundaryTraceOrders.data(),
                                                               quadOrder.boundaryTraceOrders.size() );

  // hardcoded distribution

  for (int i = 0; i < efld.nDOF(); i++)
  {
    eSfld.DOF(i) = efld.DOF(i);
    ifld.DOF(i) = fabs(efld.DOF(i));
  }

  // Distribute the eAfld to efld by looping over elements in 1 cell group
  typedef typename XField<PhysDim, TopoDim    >::FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename Field< PhysDim,TopoDim,Real>::FieldCellGroupType<Topology> EFieldCellGroupType;
  typedef typename Field<PhysDim,TopoDim,Real >::FieldCellGroupType<Topology> EAFieldCellGroupType;
  typedef typename XFieldCellGroupType::ElementType<>  ElementXFieldClass;
  typedef typename EFieldCellGroupType::ElementType<>  ElementEFieldClass;
  typedef typename EAFieldCellGroupType::ElementType<> ElementEAFieldClass;

  XFieldCellGroupType& xfldCell = xfld.getCellGroup<Topology>(0);
  EFieldCellGroupType& eSfldCell = eSfld.getCellGroup<Topology>(0);
  EFieldCellGroupType& ifldCell = ifld.getCellGroup<Topology>(0);
  EAFieldCellGroupType& eAfldCell = eAfld.getCellGroup<Topology>(0);

  ElementXFieldClass xfldElem(xfldCell.basis() );
  ElementEFieldClass eSfldElem(eSfldCell.basis() );
  ElementEFieldClass ifldElem(ifldCell.basis() );
  ElementEAFieldClass eAfldElem(eAfldCell.basis() );

  for (int elem = 0; elem <xfldCell.nElem(); elem++)
  {
    eSfldCell.getElement( eSfldElem, elem);
    ifldCell.getElement( ifldElem, elem);
    eAfldCell.getElement( eAfldElem, elem);

    eSfldElem.DOF(0) += eAfldElem.DOF(0);
    ifldElem.DOF(0)  += fabs(eAfldElem.DOF(0));

    eSfldCell.setElement(eSfldElem,elem);
    ifldCell.setElement(ifldElem,elem);
  }

  // Distribute the eIfld to efld by looping over interior trace elements
  typedef typename XField<PhysDim,TopoDim>::FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename Field<PhysDim,TopoDim,Real>::FieldTraceGroupType<TopologyTrace> EIFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::ElementType<> ElementXFieldTraceClass;
  typedef typename EIFieldTraceGroupType::ElementType<> ElementEIFieldTraceClass;

  for (int group = 0; group< xfld.nInteriorTraceGroups(); group ++)
  {
    const XFieldTraceGroupType& xfldInteriorTrace = xfld.getInteriorTraceGroup<TopologyTrace>(group);
    EIFieldTraceGroupType& eIfldInteriorTrace = eIfld.getInteriorTraceGroup<TopologyTrace>(group);

    ElementXFieldTraceClass xfldTraceElem( xfldInteriorTrace.basis() );
    ElementEIFieldTraceClass eIfldTraceElem( eIfldInteriorTrace.basis() );
    for (int elem = 0; elem < xfldInteriorTrace.nElem(); elem++)
    {
      eIfldInteriorTrace.getElement( eIfldTraceElem, elem);

      int elemL  = xfldInteriorTrace.getElementLeft( elem );
      eSfldCell.getElement( eSfldElem, elemL );
      eSfldElem.DOF(0) += 0.5* eIfldTraceElem.DOF(0);
      eSfldCell.setElement( eSfldElem, elemL );

      ifldCell.getElement( ifldElem, elemL );
      ifldElem.DOF(0) += fabs(0.5* eIfldTraceElem.DOF(0));
      ifldCell.setElement( ifldElem, elemL );

      int elemR  = xfldInteriorTrace.getElementRight( elem );
      eSfldCell.getElement( eSfldElem, elemR );
      eSfldElem.DOF(0) += 0.5* eIfldTraceElem.DOF(0);
      eSfldCell.setElement( eSfldElem, elemR );

      ifldCell.getElement( ifldElem, elemR );
      ifldElem.DOF(0) += fabs(0.5* eIfldTraceElem.DOF(0));
      ifldCell.setElement( ifldElem, elemR );
    }
  }

  // Distribute the boundary fields to efld by looping over elements of boundary trace groups

  // boundary terms
  typedef typename XField<PhysDim, TopoDim>::FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename Field<PhysDim,TopoDim,Real>::FieldTraceGroupType<TopologyTrace> EBFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::ElementType<> ElementXFieldTraceClass;
  typedef typename EBFieldTraceGroupType::ElementType<> ElementEBFieldTraceClass;

  for (int group = 0; group< xfld.nBoundaryTraceGroups(); group++)
  {
    const XFieldTraceGroupType xfldBoundaryTrace = xfld.getBoundaryTraceGroup<TopologyTrace>(group);
    ElementXFieldTraceClass xfldTraceElem(xfldBoundaryTrace.basis() );

    EBFieldTraceGroupType& eBfldTrace = eBfld.getBoundaryTraceGroup<TopologyTrace>(group);
    EBFieldTraceGroupType& eIfldTrace = eIfld.getBoundaryTraceGroup<TopologyTrace>(group);
    ElementEBFieldTraceClass eBfldElem(eBfldTrace.basis() );
    ElementEBFieldTraceClass eIfldElem(eIfldTrace.basis() );

    // loop over elements in group
    for (int elem = 0; elem< xfldBoundaryTrace.nElem(); elem++)
    {
      eBfldTrace.getElement( eBfldElem, elem );
      eIfldTrace.getElement( eIfldElem, elem );

      int elemL = xfldBoundaryTrace.getElementLeft(elem);
      eSfldCell.getElement( eSfldElem, elemL );
      eSfldElem.DOF(0) +=  eBfldElem.DOF(0) + eIfldElem.DOF(0);
      eSfldCell.setElement( eSfldElem, elemL );

      ifldCell.getElement( ifldElem, elemL );
      ifldElem.DOF(0) += fabs(eIfldElem.DOF(0)) + fabs(eBfldElem.DOF(0));
      ifldCell.setElement( ifldElem, elemL );
    }
  }

  // Construct ErrorEstimateClass
  ErrorEstimateClass ErrorEstimate(xfld,
                                   qfld, afld, qIfld, lgfld,
                                   wfld, bfld, wIfld, mufld,
                                   pde, disc, quadOrder, {0}, {0},
                                   BCList, BCBoundaryGroups);
  const Field_DG_Cell<PhysDim,TopoDim,Real>& eSfld2 = ErrorEstimate.getESField();
  const Field_DG_Cell<PhysDim,TopoDim,Real>& ifld2 = ErrorEstimate.getIField();

  std::vector<std::vector<Real>> errorArray;
  ErrorEstimate.fillEArray(errorArray); // savithru's slightly more fragile interface

  BOOST_CHECK_EQUAL( xfld.nElem(), eSfld2.nElem() );
  BOOST_CHECK_EQUAL( xfld.nElem(), eSfld2.nDOF() );

  Real globalEstimate=0,trueSum=0;
  Real globalAggregate=0,trueAgg=0;
  ErrorEstimate.getErrorEstimate(globalEstimate);
  ErrorEstimate.getErrorIndicator(globalAggregate);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int dof = 0; dof < eSfld.nDOF(); dof++)
  {
    SANS_CHECK_CLOSE( eSfld.DOF(dof), eSfld2.DOF(dof), small_tol, close_tol );
    SANS_CHECK_CLOSE( ifld.DOF(dof),  ifld2.DOF(dof), small_tol, close_tol );
    trueSum += eSfld.DOF(dof);
    trueAgg += fabs(ifld.DOF(dof));
  }

  SANS_CHECK_CLOSE( trueSum, globalEstimate, small_tol, close_tol );
  SANS_CHECK_CLOSE( trueAgg, globalAggregate, small_tol, close_tol );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimate_HDG_LinearScalar_sansLG_BC_2Triangle_X1Q1 )
{
  typedef TopoD2 TopoDim;
  typedef PhysD2 PhysDim;
  typedef Triangle Topology;
  typedef Line TopologyTrace;

  typedef PDEAdvectionDiffusion<PhysDim,
      AdvectiveFlux2D_Uniform,
      ViscousFlux2D_Uniform,
      Source2D_None> PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysDim::D, ArrayQ> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysDim,BCTypeLinearRobin_sansLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysDim, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandInteriorTraceClass;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandBoundaryTraceClass;

  // Algebraic Eq set things
  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef AlgebraicEquationSet_HDG<PDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Dense, XField<PhysDim, TopoDim>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef ErrorEstimate_HDG<PDEClass, BCNDConvertSpace, BCVector,XField<PhysDim,TopoDim>> ErrorEstimateClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BCs
  Real A=2.1,B=3.1,bcdata=4.1;
  BCClass bc(A,B,bcdata);

  PyDict BCRobin;
  BCRobin[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCRobin[BCAdvectionDiffusionParams<PhysDim, BCTypeLinearRobin_sansLG>::params.A] = A;
  BCRobin[BCAdvectionDiffusionParams<PhysDim, BCTypeLinearRobin_sansLG>::params.B] = B;
  BCRobin[BCAdvectionDiffusionParams<PhysDim, BCTypeLinearRobin_sansLG>::params.bcdata] = bcdata;

  PyDict BCList;
  BCList["Boundary"] = BCRobin;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  BCBoundaryGroups["Boundary"] = {0};

  // Check the BC dictionary
  BCParams::checkInputs(BCList);

  // grid: single triangle, P1 (aka X1)
  XField2D_2Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 4, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 2, xfld.nElem() );

  XField_CellToTrace<PhysDim, TopoDim> connectivity(xfld);

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;
  // triangle solution data (right)
  qfld.DOF(3) = 7;
  qfld.DOF(4) = 2;
  qfld.DOF(5) = 9;

  BOOST_CHECK_EQUAL( 6, qfld.nDOF() );

  // Auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( afld.nDOF(), 6 );

  // triangle auxiliary variable (left)
  afld.DOF(0) = { 2, -3};
  afld.DOF(1) = { 7,  8};
  afld.DOF(2) = {-1, -5};

  // triangle auxiliary variable (right)
  afld.DOF(3) = { 9,  6};
  afld.DOF(4) = {-1,  3};
  afld.DOF(5) = { 2, -3};

  // interface solution
  Field_DG_Trace<PhysDim, TopoDim, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( qIfld.nDOF(), 5*2);
  BOOST_CHECK_EQUAL( qIfld.nInteriorTraceGroups(), 1 );
  BOOST_CHECK_EQUAL( qIfld.nBoundaryTraceGroups(), 1 );

  // node trace data
  for (int k = 0; k < qIfld.nDOF(); k++)
    qIfld.DOF(k) = pow(-1,k)*k;

  // weight: single triangle, P2 (aka Q1)
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  // triangle solution data (left)
  wfld.DOF(0) = -2;
  wfld.DOF(1) =  4;
  wfld.DOF(2) =  3;
  wfld.DOF(3) =  2;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  // triangle solution data (right)
  wfld.DOF(6)  =  7;
  wfld.DOF(7)  =  3;
  wfld.DOF(8)  =  2;
  wfld.DOF(9)  = -1;
  wfld.DOF(10) =  5;
  wfld.DOF(11) = -3;

  // Auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ> bfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( bfld.nDOF(), 12 );

  // triangle auxiliary variable (left)
  bfld.DOF(0) = { 1,  5};
  bfld.DOF(1) = { 3,  6};
  bfld.DOF(2) = {-1,  7};
  bfld.DOF(3) = { 3,  1};
  bfld.DOF(4) = { 2,  2};
  bfld.DOF(5) = { 4,  3};

  // triangle auxiliary variable (right)
  bfld.DOF(6)  = { 5,  6};
  bfld.DOF(7)  = { 4,  5};
  bfld.DOF(8)  = { 3,  4};
  bfld.DOF(9)  = { 4,  3};
  bfld.DOF(10) = { 3,  2};
  bfld.DOF(11) = { 2,  1};

  // interface solution
  Field_DG_Trace<PhysDim, TopoDim, ArrayQ> wIfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( wIfld.nDOF(), 5*3);
  BOOST_CHECK_EQUAL( wIfld.nInteriorTraceGroups(), 1 );
  BOOST_CHECK_EQUAL( wIfld.nBoundaryTraceGroups(), 1 );

  // node trace data
  for (int k = 0; k < wIfld.nDOF(); k++)
    wIfld.DOF(k) = pow(-1,k)*k;

  // estimate fields
  Field_DG_Cell<PhysDim, TopoDim, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysDim, TopoDim, Real> eSfld(xfld, 0, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysDim, TopoDim, Real> ifld(xfld, 0, BasisFunctionCategory_Legendre);

  // solution data
  efld = 0; eSfld = 0; ifld = 0;

  BOOST_CHECK_EQUAL( 2, efld.nElem() );
  BOOST_CHECK_EQUAL( 2, efld.nDOF() );

  // lifting operator: single line, P0 (aka Q0)
  Field_DG_Cell<PhysDim, TopoDim, Real> eAfld(xfld, 0, BasisFunctionCategory_Legendre);

  eAfld=0;

  // interface solution
  Field_DG_Trace<PhysDim, TopoDim, ArrayQ> eIfld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK_EQUAL( eIfld.nDOF(), 5);
  BOOST_CHECK_EQUAL( eIfld.nInteriorTraceGroups(), 1 );
  BOOST_CHECK_EQUAL( eIfld.nBoundaryTraceGroups(), 1 );

  eIfld = 0;

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Local );

  QuadratureOrder quadOrder(xfld, 3);

  // integrand
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandInteriorTraceClass fcnITrace( pde, disc, {0} );
  IntegrandBoundaryTraceClass fcnBTrace( pde, bc, {0}, disc );

  IntegrateCellGroups<TopoDim>::integrate( ErrorEstimateCell_HDG( fcnCell, fcnITrace, connectivity,
                                                                 xfld, qfld, afld, qIfld, wfld, bfld, wIfld,
                                                                 efld, eAfld, eIfld,
                                                                 quadOrder.interiorTraceOrders.data(),
                                                                 quadOrder.interiorTraceOrders.size() ),
                                          xfld, (qfld, afld, wfld, bfld, efld, eAfld),
                                          quadOrder.cellOrders.data(), quadOrder.cellOrders.size());

  IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate( ErrorEstimateBoundaryTrace_HDG(fcnBTrace),
                                                               xfld, (qfld, afld, wfld, bfld, efld, eAfld),
                                                               (qIfld, wIfld, eIfld),
                                                               quadOrder.boundaryTraceOrders.data(),
                                                               quadOrder.boundaryTraceOrders.size() );

  // hardcoded distribution
  for (int i = 0; i < efld.nDOF(); i++)
  {
    eSfld.DOF(i) = efld.DOF(i);
    ifld.DOF(i) = fabs(efld.DOF(i));
  }

  // Distribute the eAfld to efld by looping over elements in 1 cell group
  typedef typename XField<PhysDim, TopoDim      >::FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename Field< PhysDim,TopoDim,Real  >::FieldCellGroupType<Topology> EFieldCellGroupType;
  typedef typename Field<PhysDim,TopoDim,Real>::FieldCellGroupType<Topology> EAFieldCellGroupType;
  typedef typename XFieldCellGroupType::ElementType<>  ElementXFieldClass;
  typedef typename EFieldCellGroupType::ElementType<>  ElementEFieldClass;
  typedef typename EAFieldCellGroupType::ElementType<> ElementEAFieldClass;

  XFieldCellGroupType& xfldCell = xfld.getCellGroup<Topology>(0);
  EFieldCellGroupType& eSfldCell = eSfld.getCellGroup<Topology>(0);
  EFieldCellGroupType& ifldCell = ifld.getCellGroup<Topology>(0);
  EAFieldCellGroupType& eAfldCell = eAfld.getCellGroup<Topology>(0);

  ElementXFieldClass xfldElem(xfldCell.basis() );
  ElementEFieldClass eSfldElem(eSfldCell.basis() );
  ElementEFieldClass ifldElem(ifldCell.basis() );
  ElementEAFieldClass eAfldElem(eAfldCell.basis() );

  for (int elem = 0; elem <xfldCell.nElem(); elem++)
  {
    eSfldCell.getElement( eSfldElem, elem);
    ifldCell.getElement( ifldElem, elem);
    eAfldCell.getElement( eAfldElem, elem);

    eSfldElem.DOF(0) += eAfldElem.DOF(0);
    ifldElem.DOF(0)  += fabs(eAfldElem.DOF(0));

    eSfldCell.setElement(eSfldElem,elem);
    ifldCell.setElement(ifldElem,elem);
  }
  // Distribute the eIfld to efld by looping over interior trace elements
  typedef typename XField<PhysDim,TopoDim>::FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename Field<PhysDim,TopoDim,Real>::FieldTraceGroupType<TopologyTrace> EIFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::ElementType<> ElementXFieldTraceClass;
  typedef typename EIFieldTraceGroupType::ElementType<> ElementEIFieldTraceClass;

  for (int group = 0; group< xfld.nInteriorTraceGroups(); group ++)
  {
    const XFieldTraceGroupType& xfldInteriorTrace = xfld.getInteriorTraceGroup<TopologyTrace>(group);
    EIFieldTraceGroupType& eIfldInteriorTrace = eIfld.getInteriorTraceGroup<TopologyTrace>(group);

    ElementXFieldTraceClass xfldTraceElem( xfldInteriorTrace.basis() );
    ElementEIFieldTraceClass eIfldTraceElem( eIfldInteriorTrace.basis() );

    for (int elem = 0; elem < xfldInteriorTrace.nElem(); elem++)
    {
      eIfldInteriorTrace.getElement( eIfldTraceElem, elem);

      int elemL  = xfldInteriorTrace.getElementLeft( elem );
      eSfldCell.getElement( eSfldElem, elemL );
      eSfldElem.DOF(0) += 0.5* eIfldTraceElem.DOF(0);
      eSfldCell.setElement( eSfldElem, elemL );

      ifldCell.getElement( ifldElem, elemL );
      ifldElem.DOF(0) += fabs(0.5* eIfldTraceElem.DOF(0));
      ifldCell.setElement( ifldElem, elemL );

      int elemR  = xfldInteriorTrace.getElementRight( elem );
      eSfldCell.getElement( eSfldElem, elemR );
      eSfldElem.DOF(0) += 0.5* eIfldTraceElem.DOF(0);
      eSfldCell.setElement( eSfldElem, elemR );

      ifldCell.getElement( ifldElem, elemR );
      ifldElem.DOF(0) += fabs(0.5* eIfldTraceElem.DOF(0));
      ifldCell.setElement( ifldElem, elemR );
    }
  }

  // Distribute the boundary fields to efld by looping over elements of boundary trace groups

  // boundary terms
  typedef typename XField<PhysDim, TopoDim>::FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename Field<PhysDim,TopoDim,Real>::FieldTraceGroupType<TopologyTrace> EBFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::ElementType<> ElementXFieldTraceClass;
  typedef typename EBFieldTraceGroupType::ElementType<> ElementEBFieldTraceClass;

  for (int group = 0; group< xfld.nBoundaryTraceGroups(); group++)
  {
    const XFieldTraceGroupType xfldBoundaryTrace = xfld.getBoundaryTraceGroup<TopologyTrace>(group);
    ElementXFieldTraceClass xfldTraceElem(xfldBoundaryTrace.basis() );

    EBFieldTraceGroupType& eIfldTrace = eIfld.getBoundaryTraceGroup<TopologyTrace>(group);
    ElementEBFieldTraceClass eIfldElem(eIfldTrace.basis() );

    // loop over elements in group
    for (int elem = 0; elem< xfldBoundaryTrace.nElem(); elem++)
    {
      eIfldTrace.getElement( eIfldElem, elem );

      int elemL = xfldBoundaryTrace.getElementLeft(elem);
      eSfldCell.getElement( eSfldElem, elemL );
      eSfldElem.DOF(0) +=   eIfldElem.DOF(0);
      eSfldCell.setElement( eSfldElem, elemL );

      ifldCell.getElement( ifldElem, elemL );
      ifldElem.DOF(0) += fabs(eIfldElem.DOF(0));
      ifldCell.setElement( ifldElem, elemL );
    }
  }

  // Needed to get past constructor, really want to get rid of
  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> dumBfld1(xfld, 0, BasisFunctionCategory_Legendre,
                                                          BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups) );
  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> dumBfld2(xfld, 0, BasisFunctionCategory_Legendre,
                                                          BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups));
  dumBfld1=0; dumBfld2=0;

  // Construct ErrorEstimateClass
  ErrorEstimateClass ErrorEstimate(xfld,
                                   qfld, afld, qIfld, dumBfld1,
                                   wfld, bfld, wIfld, dumBfld2,
                                   pde, disc, quadOrder, {0},{0},
                                   BCList, BCBoundaryGroups);
  const Field_DG_Cell<PhysDim,TopoDim,Real>& eSfld2 = ErrorEstimate.getESField();
  const Field_DG_Cell<PhysDim,TopoDim,Real>& ifld2 = ErrorEstimate.getIField();

  std::vector<std::vector<Real>> errorArray;
  ErrorEstimate.fillEArray(errorArray); // savithru's slightly more fragile interface

  BOOST_CHECK_EQUAL( xfld.nElem(), eSfld2.nElem() );
  BOOST_CHECK_EQUAL( xfld.nElem(), eSfld2.nDOF() );

  Real globalEstimate=0,trueSum=0;
  Real globalAggregate=0,trueAgg=0;
  ErrorEstimate.getErrorEstimate(globalEstimate);
  ErrorEstimate.getErrorIndicator(globalAggregate);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int dof = 0; dof < eSfld.nDOF(); dof++)
  {
    SANS_CHECK_CLOSE( eSfld.DOF(dof), eSfld2.DOF(dof), small_tol, close_tol );
    SANS_CHECK_CLOSE( ifld.DOF(dof),  ifld2.DOF(dof), small_tol, close_tol );
    trueSum += eSfld.DOF(dof);
    trueAgg += fabs(ifld.DOF(dof));
  }

  SANS_CHECK_CLOSE( trueSum, globalEstimate, small_tol, close_tol );
  SANS_CHECK_CLOSE( trueAgg, globalAggregate, small_tol, close_tol );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimate_HDG_Dirichlet_mitState_BC_2Triangle_X1Q1 )
{
  typedef TopoD2 TopoDim;
  typedef PhysD2 PhysDim;
  typedef Triangle Topology;
  typedef Line TopologyTrace;

  typedef PDEAdvectionDiffusion<PhysDim,
      AdvectiveFlux2D_Uniform,
      ViscousFlux2D_Uniform,
      Source2D_None> PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysDim::D, ArrayQ> VectorArrayQ;

  typedef BCTypeDirichlet_mitState<AdvectiveFlux2D_Uniform,ViscousFlux2D_Uniform> BCTypeDirichlet_mitState2D;

  typedef BCAdvectionDiffusion<PhysDim,BCTypeDirichlet_mitState2D> BCClassRaw;
  typedef BCNDConvertSpace<PhysDim, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandInteriorTraceClass;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandBoundaryTraceClass;

  // Algebraic Eq set things
  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef AlgebraicEquationSet_HDG<PDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Dense, XField<PhysDim, TopoDim>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef ErrorEstimate_HDG<PDEClass, BCNDConvertSpace, BCVector,XField<PhysDim,TopoDim>> ErrorEstimateClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC
  Real uB = 12./5.;
  BCClass bc(pde, uB);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  PyDict BCDirichlet;
  BCDirichlet[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichlet[BCAdvectionDiffusionParams<PhysD1, BCTypeDirichlet_mitStateParam>::params.qB] = uB;

  PyDict BCList;
  BCList["Boundary"] = BCDirichlet;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  BCBoundaryGroups["Boundary"] = {0};

  // Check the BC dictionary
  BCParams::checkInputs(BCList);

  // grid: single triangle, P1 (aka X1)
  XField2D_2Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 4, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 2, xfld.nElem() );

  XField_CellToTrace<PhysDim, TopoDim> connectivity(xfld);

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;
  // triangle solution data (right)
  qfld.DOF(3) = 7;
  qfld.DOF(4) = 2;
  qfld.DOF(5) = 9;

  BOOST_CHECK_EQUAL( 6, qfld.nDOF() );

  // Auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( afld.nDOF(), 6 );

  // triangle auxiliary variable (left)
  afld.DOF(0) = { 2, -3};
  afld.DOF(1) = { 7,  8};
  afld.DOF(2) = {-1, -5};

  // triangle auxiliary variable (right)
  afld.DOF(3) = { 9,  6};
  afld.DOF(4) = {-1,  3};
  afld.DOF(5) = { 2, -3};

  // interface solution
  Field_DG_Trace<PhysDim, TopoDim, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( qIfld.nDOF(), 5*2);
  BOOST_CHECK_EQUAL( qIfld.nInteriorTraceGroups(), 1 );
  BOOST_CHECK_EQUAL( qIfld.nBoundaryTraceGroups(), 1 );

  // node trace data
  for (int k = 0; k < qIfld.nDOF(); k++)
    qIfld.DOF(k) = pow(-1,k)*k;

  // weight: single triangle, P2 (aka Q1)
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  // triangle solution data (left)
  wfld.DOF(0) = -2;
  wfld.DOF(1) =  4;
  wfld.DOF(2) =  3;
  wfld.DOF(3) =  2;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  // triangle solution data (right)
  wfld.DOF(6)  =  7;
  wfld.DOF(7)  =  3;
  wfld.DOF(8)  =  2;
  wfld.DOF(9)  = -1;
  wfld.DOF(10) =  5;
  wfld.DOF(11) = -3;

  // Auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ> bfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( bfld.nDOF(), 12 );

  // triangle auxiliary variable (left)
  bfld.DOF(0) = { 1,  5};
  bfld.DOF(1) = { 3,  6};
  bfld.DOF(2) = {-1,  7};
  bfld.DOF(3) = { 3,  1};
  bfld.DOF(4) = { 2,  2};
  bfld.DOF(5) = { 4,  3};

  // triangle auxiliary variable (right)
  bfld.DOF(6)  = { 5,  6};
  bfld.DOF(7)  = { 4,  5};
  bfld.DOF(8)  = { 3,  4};
  bfld.DOF(9)  = { 4,  3};
  bfld.DOF(10) = { 3,  2};
  bfld.DOF(11) = { 2,  1};

  // interface solution
  Field_DG_Trace<PhysDim, TopoDim, ArrayQ> wIfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( wIfld.nDOF(), 5*3);
  BOOST_CHECK_EQUAL( wIfld.nInteriorTraceGroups(), 1 );
  BOOST_CHECK_EQUAL( wIfld.nBoundaryTraceGroups(), 1 );

  // node trace data
  for (int k = 0; k < wIfld.nDOF(); k++)
    wIfld.DOF(k) = pow(-1,k)*k;

  // estimate fields
  Field_DG_Cell<PhysDim, TopoDim, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysDim, TopoDim, Real> eSfld(xfld, 0, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysDim, TopoDim, Real> ifld(xfld, 0, BasisFunctionCategory_Legendre);

  // solution data
  efld = 0; eSfld = 0; ifld = 0;

  BOOST_CHECK_EQUAL( 2, efld.nElem() );
  BOOST_CHECK_EQUAL( 2, efld.nDOF() );

  // lifting operator: single line, P0 (aka Q0)
  Field_DG_Cell<PhysDim, TopoDim, Real> eAfld(xfld, 0, BasisFunctionCategory_Legendre);

  eAfld=0;

  // interface solution
  Field_DG_Trace<PhysDim, TopoDim, ArrayQ> eIfld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_CHECK_EQUAL( eIfld.nDOF(), 5);
  BOOST_CHECK_EQUAL( eIfld.nInteriorTraceGroups(), 1 );
  BOOST_CHECK_EQUAL( eIfld.nBoundaryTraceGroups(), 1 );

  eIfld = 0;

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Local );

  QuadratureOrder quadOrder(xfld, 3);

  // integrand
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandInteriorTraceClass fcnITrace( pde, disc, {0} );
  IntegrandBoundaryTraceClass fcnBTrace( pde, bc, {0}, disc );

  IntegrateCellGroups<TopoDim>::integrate( ErrorEstimateCell_HDG( fcnCell, fcnITrace, connectivity,
                                                                 xfld, qfld, afld, qIfld, wfld, bfld, wIfld,
                                                                 efld, eAfld, eIfld,
                                                                 quadOrder.interiorTraceOrders.data(),
                                                                 quadOrder.interiorTraceOrders.size() ),
                                          xfld, (qfld, afld, wfld, bfld, efld, eAfld),
                                          quadOrder.cellOrders.data(), quadOrder.cellOrders.size());

  IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate( ErrorEstimateBoundaryTrace_HDG(fcnBTrace),
                                                               xfld, (qfld, afld, wfld, bfld, efld, eAfld),
                                                               (qIfld, wIfld, eIfld),
                                                               quadOrder.boundaryTraceOrders.data(),
                                                               quadOrder.boundaryTraceOrders.size() );

  // hardcoded distribution

  for (int i = 0; i < efld.nDOF(); i++)
  {
    eSfld.DOF(i) = efld.DOF(i);
    ifld.DOF(i) = fabs(efld.DOF(i));
  }

  // Distribute the eAfld to efld by looping over elements in 1 cell group
  typedef typename XField<PhysDim, TopoDim      >::FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename Field< PhysDim,TopoDim,Real  >::FieldCellGroupType<Topology> EFieldCellGroupType;
  typedef typename Field<PhysDim,TopoDim,Real>::FieldCellGroupType<Topology> EAFieldCellGroupType;
  typedef typename XFieldCellGroupType::ElementType<>  ElementXFieldClass;
  typedef typename EFieldCellGroupType::ElementType<>  ElementEFieldClass;
  typedef typename EAFieldCellGroupType::ElementType<> ElementEAFieldClass;

  XFieldCellGroupType& xfldCell = xfld.getCellGroup<Topology>(0);
  EFieldCellGroupType& eSfldCell = eSfld.getCellGroup<Topology>(0);
  EFieldCellGroupType& ifldCell = ifld.getCellGroup<Topology>(0);
  EAFieldCellGroupType& eAfldCell = eAfld.getCellGroup<Topology>(0);

  ElementXFieldClass xfldElem(xfldCell.basis() );
  ElementEFieldClass eSfldElem(eSfldCell.basis() );
  ElementEFieldClass ifldElem(ifldCell.basis() );
  ElementEAFieldClass eAfldElem(eAfldCell.basis() );

  for (int elem = 0; elem <xfldCell.nElem(); elem++)
  {
    eSfldCell.getElement( eSfldElem, elem);
    ifldCell.getElement( ifldElem, elem);
    eAfldCell.getElement( eAfldElem, elem);

    eSfldElem.DOF(0) += eAfldElem.DOF(0);
    ifldElem.DOF(0)  += fabs(eAfldElem.DOF(0));

    eSfldCell.setElement(eSfldElem,elem);
    ifldCell.setElement(ifldElem,elem);
  }
  // Distribute the eIfld to efld by looping over interior trace elements
  typedef typename XField<PhysDim,TopoDim>::FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename Field<PhysDim,TopoDim,Real>::FieldTraceGroupType<TopologyTrace> EIFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::ElementType<> ElementXFieldTraceClass;
  typedef typename EIFieldTraceGroupType::ElementType<> ElementEIFieldTraceClass;

  for (int group = 0; group< xfld.nInteriorTraceGroups(); group ++)
  {
    const XFieldTraceGroupType& xfldInteriorTrace = xfld.getInteriorTraceGroup<TopologyTrace>(group);
    EIFieldTraceGroupType& eIfldInteriorTrace = eIfld.getInteriorTraceGroup<TopologyTrace>(group);

    ElementXFieldTraceClass xfldTraceElem( xfldInteriorTrace.basis() );
    ElementEIFieldTraceClass eIfldTraceElem( eIfldInteriorTrace.basis() );

    for (int elem = 0; elem < xfldInteriorTrace.nElem(); elem++)
    {
      eIfldInteriorTrace.getElement( eIfldTraceElem, elem);

      int elemL  = xfldInteriorTrace.getElementLeft( elem );
      eSfldCell.getElement( eSfldElem, elemL );
      eSfldElem.DOF(0) += 0.5* eIfldTraceElem.DOF(0);
      eSfldCell.setElement( eSfldElem, elemL );

      ifldCell.getElement( ifldElem, elemL );
      ifldElem.DOF(0) += fabs(0.5* eIfldTraceElem.DOF(0));
      ifldCell.setElement( ifldElem, elemL );

      int elemR  = xfldInteriorTrace.getElementRight( elem );
      eSfldCell.getElement( eSfldElem, elemR );
      eSfldElem.DOF(0) += 0.5* eIfldTraceElem.DOF(0);
      eSfldCell.setElement( eSfldElem, elemR );

      ifldCell.getElement( ifldElem, elemR );
      ifldElem.DOF(0) += fabs(0.5* eIfldTraceElem.DOF(0));
      ifldCell.setElement( ifldElem, elemR );
    }
  }

  // Distribute the boundary fields to efld by looping over elements of boundary trace groups

  // boundary terms
  typedef typename XField<PhysDim, TopoDim>::FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename Field<PhysDim,TopoDim,Real>::FieldTraceGroupType<TopologyTrace> EBFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::ElementType<> ElementXFieldTraceClass;
  typedef typename EBFieldTraceGroupType::ElementType<> ElementEBFieldTraceClass;

  for (int group = 0; group< xfld.nBoundaryTraceGroups(); group++)
  {
    const XFieldTraceGroupType xfldBoundaryTrace = xfld.getBoundaryTraceGroup<TopologyTrace>(group);
    ElementXFieldTraceClass xfldTraceElem(xfldBoundaryTrace.basis() );

    EBFieldTraceGroupType& eIfldTrace = eIfld.getBoundaryTraceGroup<TopologyTrace>(group);
    ElementEBFieldTraceClass eIfldElem(eIfldTrace.basis() );

    // loop over elements in group
    for (int elem = 0; elem< xfldBoundaryTrace.nElem(); elem++)
    {
      eIfldTrace.getElement( eIfldElem, elem );

      int elemL = xfldBoundaryTrace.getElementLeft(elem);
      eSfldCell.getElement( eSfldElem, elemL );
      eSfldElem.DOF(0) +=   eIfldElem.DOF(0);
      eSfldCell.setElement( eSfldElem, elemL );

      ifldCell.getElement( ifldElem, elemL );
      ifldElem.DOF(0) += fabs(eIfldElem.DOF(0));
      ifldCell.setElement( ifldElem, elemL );
    }
  }

  // Needed to get past constructor, really want to get rid of
  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> dumBfld1(xfld, 0, BasisFunctionCategory_Legendre,
                                                          BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups) );
  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> dumBfld2(xfld, 0, BasisFunctionCategory_Legendre,
                                                          BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups));
  dumBfld1=0; dumBfld2=0;

  // Construct ErrorEstimateClass
  ErrorEstimateClass ErrorEstimate(xfld,
                                   qfld, afld, qIfld, dumBfld1,
                                   wfld, bfld, wIfld, dumBfld2,
                                   pde, disc, quadOrder, {0},{0},
                                   BCList, BCBoundaryGroups);
  const Field_DG_Cell<PhysDim,TopoDim,Real>& eSfld2 = ErrorEstimate.getESField();
  const Field_DG_Cell<PhysDim,TopoDim,Real>& ifld2 = ErrorEstimate.getIField();

  std::vector<std::vector<Real>> errorArray;
  ErrorEstimate.fillEArray(errorArray); // savithru's slightly more fragile interface

  BOOST_CHECK_EQUAL( xfld.nElem(), eSfld2.nElem() );
  BOOST_CHECK_EQUAL( xfld.nElem(), eSfld2.nDOF() );

  Real globalEstimate=0,trueSum=0;
  Real globalAggregate=0,trueAgg=0;
  ErrorEstimate.getErrorEstimate(globalEstimate);
  ErrorEstimate.getErrorIndicator(globalAggregate);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int dof = 0; dof < eSfld.nDOF(); dof++)
  {
    SANS_CHECK_CLOSE( eSfld.DOF(dof), eSfld2.DOF(dof), small_tol, close_tol );
    SANS_CHECK_CLOSE( ifld.DOF(dof),  ifld2.DOF(dof), small_tol, close_tol );
    trueSum += eSfld.DOF(dof);
    trueAgg += fabs(ifld.DOF(dof));
  }

  SANS_CHECK_CLOSE( trueSum, globalEstimate, small_tol, close_tol );
  SANS_CHECK_CLOSE( trueAgg, globalAggregate, small_tol, close_tol );

}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
