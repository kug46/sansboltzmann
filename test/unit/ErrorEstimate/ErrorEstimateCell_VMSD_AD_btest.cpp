// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEstimateCell_Galerkin_AD_btest
// testing of cell residual functions for Galerkin with Advection-Diffusion

#include <boost/test/unit_test.hpp>
#include "../../../src/Discretization/VMSD/IntegrandCell_VMSD.h"
#include "../../../src/Discretization/VMSD/IntegrandTrace_VMSD.h"
#include "../../../src/ErrorEstimate/VMSD/ErrorEstimateCell_VMSD.h"
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
//#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
//#include "pde/NDConvert/PDENDConvertSpace3D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
// #include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
// #include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/XField_CellToTrace.h"

#include "Field/Tuple/FieldTuple.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/QuadratureOrder.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
//#include "unit/UnitGrids/XField2D_1Quad_X1_1Group.h"
//#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
//#include "unit/UnitGrids/XField3D_1Hex_X1_1Group.h"


using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEstimateCell_VMSD_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateCell_VMSD_1D_CG_P2P3 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_VMSD<PDEClass> IntegrandClass;
  typedef IntegrandTrace_VMSD<PDEClass> IntegrandTraceClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single triangle, P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;
  XField_CellToTrace<PhysD1, TopoD1> xfldCellToTrace(xfld);

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 2, xfld.nElem() );

  // solution: single line, P1 (aka Q1)
  int qorder = 1;
  Field_CG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution data
  qfld.DOF(0) =  5;
  qfld.DOF(1) = -4;
  qfld.DOF(2) =  3;

  BOOST_CHECK_EQUAL( 2, qfld.nElem() );
  BOOST_CHECK_EQUAL( 3, qfld.nDOF() );

  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qpfld(xfld, 1, BasisFunctionCategory_Hierarchical);
  // solution data
  qpfld.DOF(0) =  0.2;
  qpfld.DOF(1) = 0.3;
  qpfld.DOF(2) =  -0.05;
  qpfld.DOF(3) = 0.017;

  Field_CG_Cell<PhysD1, TopoD1, ArrayQ> wfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  // solution data
  wfld.DOF(0) = 3;
  wfld.DOF(1) = 4;
  wfld.DOF(2) = 5;

  BOOST_CHECK_EQUAL( 2, wfld.nElem() );
  BOOST_CHECK_EQUAL( 3, wfld.nDOF() );

  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> wpfld(xfld, 1, BasisFunctionCategory_Hierarchical);

  // solution data
  wpfld.DOF(0) =  -0.2;
  wpfld.DOF(1) = -0.3;
  wpfld.DOF(2) =  -0.15;
  wpfld.DOF(3) = -0.22;

  Field_CG_Cell<PhysD1, TopoD1, Real> efld(xfld, 1, BasisFunctionCategory_Hierarchical);

  // solution data
  efld.DOF(0) = 0;
  efld.DOF(1) = 0;
  efld.DOF(2) = 0;

  Field_CG_Cell<PhysD1, TopoD1, Real> epfld(xfld, 1, BasisFunctionCategory_Hierarchical);

  // solution data
  epfld.DOF(0) = 0;
  epfld.DOF(1) = 0;
  epfld.DOF(2) = 0;

  BOOST_CHECK_EQUAL( 2, efld.nElem() );
  BOOST_CHECK_EQUAL( 3, efld.nDOF() );

  // quadrature rule (p2*p3)
  int quadOrder = 5;

  QuadratureOrder quadratureOrder( xfld, quadOrder );

  const Real small_tol = 2e-13;
  const Real close_tol = 2e-13;

  // integrand
  StabilizationNitsche stab(1, 2.5);
  IntegrandClass fcnint( pde , {0} );
  IntegrandTraceClass fcnITrace (pde, stab, {0});
  IntegrandTraceClass fcnBTrace (pde, stab, {0,1});

  IntegrateCellGroups<TopoD1>::integrate( ErrorEstimateCell_VMSD(fcnint, fcnITrace, fcnBTrace,
                                                                xfldCellToTrace, xfld,
                                                                qfld, qpfld, wfld, wpfld,
                                                                quadratureOrder, efld, epfld),
                                           xfld, (qfld, qpfld, wfld, wpfld, efld, epfld), &quadOrder, 1 );


  Real efldTRUE[3];
  //cell
  efldTRUE[0] = 57.52743333333333 - 3.780773333333333;
  efldTRUE[1] = -138.2370806666667 + 7.997758816666667;
  efldTRUE[2] = 76.56133833333333 - 3.335012853333333;

  efldTRUE[0] += 1.6984 + 3.83713;
  efldTRUE[1] += -1.411795 - 8.94771317;
  efldTRUE[2] += -0.641146 + 2.51758771;

  Real epfldTRUE[3];
  epfldTRUE[0] = 0;
  epfldTRUE[1] = 0;
  epfldTRUE[2] = 0;

  //COMPARISON TO FIELDWEIGHTED
  //PDE residual: (advective) + (viscous)
  SANS_CHECK_CLOSE( efldTRUE[0], efld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( epfldTRUE[0], epfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( efldTRUE[1], efld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( epfldTRUE[1], epfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( efldTRUE[2], efld.DOF(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( epfldTRUE[2], epfld.DOF(2), small_tol, close_tol );


}

#if 0

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateCell_Galerkin_2D_CG_1Triangle_P2P3 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin_StrongForm<PDEClass> IntegrandClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.321;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P2 (aka Q2)
  int qorder = 2;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle solution
  qfld.DOF(0) =  1;
  qfld.DOF(1) =  3;
  qfld.DOF(2) =  4;
  qfld.DOF(3) =  2;
  qfld.DOF(4) =  3;
  qfld.DOF(5) =  4;

  BOOST_CHECK_EQUAL( 6, qfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld.nElem() );

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  // triangle solution
  wfld.DOF(0) = -2;
  wfld.DOF(1) =  4;
  wfld.DOF(2) =  3;
  wfld.DOF(3) =  2;
  wfld.DOF(4) = -4;
  wfld.DOF(5) =  4;
  wfld.DOF(6) = -3;
  wfld.DOF(7) = -1;
  wfld.DOF(8) = -2;
  wfld.DOF(9) = -1;

  BOOST_CHECK_EQUAL( 10, wfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, wfld.nElem() );

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> efld(xfld, 0, BasisFunctionCategory_Legendre);

  // solution data
  efld.DOF(0) = 0;

  BOOST_CHECK_EQUAL( 1, efld.nElem() );
  BOOST_CHECK_EQUAL( 1, efld.nDOF() );

  // quadrature rule
  int quadratureOrder = 5;

  Real trueEstimate;
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  // integrand
  const StabilizationMatrix stab(StabilizationType::Unstabilized, TauType::Glasby, qorder+1);
  IntegrandClass fcnint( pde , {0}, stab );

  IntegrateCellGroups<TopoD2>::integrate( ErrorEstimateCell_Galerkin(fcnint), xfld, (qfld, wfld, efld), &quadratureOrder, 1 );

  // Taken from IntegrandCellGalerkinStrong_FW_AD_btest
  //PDE residual: (advection) + (diffusion)
  trueEstimate = ((-174.+43.*sqrt(3.))/50.) + (131653./750.);   // Weight Function

  SANS_CHECK_CLOSE( trueEstimate, efld.DOF(0), small_tol, close_tol );


}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateCell_Galerkin_2D_Nodal_1Triangle_P1P2 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin_Stabilized<PDEClass> IntegrandClass;

  Real u = 11./10.;
  Real v = 2./10.;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2123./1000.;
  Real kxy = 553./1000.;
  Real kyx = 478./1000.;
  Real kyy = 1007./1000.;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real a = 35./100., b = 182./100., c = 249./100.;
  Source2D_UniformGrad source(a, b, c);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P2 (aka Q2)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle solution
  qfld.DOF(0) =  1;
  qfld.DOF(1) =  3;
  qfld.DOF(2) =  4;

  BOOST_CHECK_EQUAL( 3, qfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld.nElem() );

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  // triangle solution
  wfld.DOF(0) = -2;
  wfld.DOF(1) =  4;
  wfld.DOF(2) =  3;
  wfld.DOF(3) =  2;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  BOOST_CHECK_EQUAL( 6, wfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, wfld.nElem() );

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> efld( xfld, 1, BasisFunctionCategory_Hierarchical );

  // solution data
  for (int i = 0; i < efld.nDOF(); i++)
    efld.DOF(i) = 0;

  BOOST_CHECK_EQUAL( 1, efld.nElem() );
  BOOST_REQUIRE_EQUAL( 3, efld.nDOF() );

  // quadrature rule
  int quadratureOrder = -1;

  Real trueEstimate[3] = {0,0,0};
  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // integrand
  const StabilizationMatrix stab(StabilizationType::Adjoint, TauType::Constant, 1, 10.0 );
  IntegrandClass fcnint( pde , {0}, stab );

  IntegrateCellGroups<TopoD2>::integrate( ErrorEstimateCell_Galerkin(fcnint), xfld, (qfld, wfld, efld), &quadratureOrder, 1 );

  // Taken from IntegrandCell_Galerkin_Stabilized_AD_btest
  //PDE residual: (advection) + (diffusion) + (source) + (stabilization)
  trueEstimate[0] = (2617/600.) + (-11127/2000.) + (84923/18000.) + (182126281/360000.);   // Basis function 1
  trueEstimate[1] = (-1063/120.) + (66161/3000.) + (125681/18000.) + (209491591/360000.);   // Basis function 2
  trueEstimate[2] = (-691/300.) + (1406/125.) + (153671/18000.) + (-20044597/180000.);   // Basis function 3

  for (int i = 0; i < 3; i++)
    SANS_CHECK_CLOSE( trueEstimate[i], efld.DOF(i), small_tol, close_tol );


}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
