// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEstimateBoundaryTrace_AD_btest
// testing of boundary integral residual functions with Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
//#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
//#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
//#include "pde/NDConvert/PDENDConvertSpace3D.h"
//#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"


#include "Field/Tuple/FieldTuple.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "ErrorEstimate/DG/ErrorEstimateBoundaryTrace_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
//#include "unit/UnitGrids/XField2D_1Quad_X1_1Group.h"
//#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
//#include "unit/UnitGrids/XField3D_1Hex_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEstimateBoundaryTrace_DGBR2_AD_test_suite )

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_Dirichlet_mitState_1D_1Line_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD1::D,ArrayQ> VectorArrayQ;

  typedef BCTypeDirichlet_mitState<AdvectiveFlux1D_Uniform,ViscousFlux1D_Uniform> BCTypeDirichlet_mitState1D;

  typedef BCNDConvertSpace<PhysD1, BCAdvectionDiffusion<PhysD1,BCTypeDirichlet_mitState1D> > BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC
  Real uB = 12./5.;
  BCClass bc(pde, uB);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: single triangle, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single line, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

   // solution data
  qfld.DOF(0) = 2;
  qfld.DOF(1) = 3;

  BOOST_CHECK_EQUAL( 2, qfld.nDOF() );

  // lifting operator: single line, P1 (aka Q1)
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  rfld.DOF(0) = -1;  rfld.DOF(1) = 5; // right boundary
  rfld.DOF(2) =  0;  rfld.DOF(3) = 0; // left boundary

  BOOST_CHECK_EQUAL( 4, rfld.nDOF() );
  BOOST_CHECK( rfld.D == pde.D);

  // weight: single line, P2 (aka Q2)
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

   // solution data
  wfld.DOF(0) = 3;
  wfld.DOF(1) = 4;
  wfld.DOF(2) = 5;

  BOOST_CHECK_EQUAL( 3, wfld.nDOF() );

  // lifting operator: single line, P2 (aka Q2)
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> sfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  sfld.DOF(0) = -5;  sfld.DOF(1) = 3;  sfld.DOF(2) = 2; // right boundary
  sfld.DOF(3) =  0;  sfld.DOF(4) = 0;  sfld.DOF(5) = 0; // left boundary

  BOOST_CHECK_EQUAL( 6, sfld.nDOF() );
  BOOST_CHECK( sfld.D == pde.D);

  // estimate fields
  Field_DG_Cell<PhysD1, TopoD1, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);

  // solution data
  efld.DOF(0) = 0;

  BOOST_CHECK_EQUAL( 1, efld.nElem() );
  BOOST_CHECK_EQUAL( 1, efld.nDOF() );

  // lifting operator: single line, P0 (aka Q0)
  FieldLift_DG_Cell<PhysD1, TopoD1, Real> eLfld(xfld, 0, BasisFunctionCategory_Legendre);

  eLfld.DOF(0) = 0;
  eLfld.DOF(1) = 0;

  BOOST_CHECK_EQUAL( 1, eLfld.nElem() );
  BOOST_CHECK_EQUAL( 2, eLfld.nDOF() );
  BOOST_CHECK( eLfld.D == pde.D);

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // cubic rule
  int quadratureOrder[2] = {0,0};

  // integrand
  IntegrandClass fcnint( pde, bc, {1}, disc); // integrate right edge, thus group 1

  IntegrateBoundaryTraceGroups<TopoD1>::integrate( ErrorEstimateBoundaryTrace_DGBR2(fcnint),
                                                   xfld,
                                                   (qfld,rfld,wfld,sfld,efld,eLfld),
                                                   quadratureOrder, 2);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  // Taken from IntegrandBoundaryTrace_Dirichlet_mitState_DGBR2_FW_AD_btest

  Real rsdPDETrue,rsdLiftTrue;

  //PDE residuals (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  rsdPDETrue = ( 66./5. ) + ( -2123./250. ) + ( 121011./5000. ) + ( -2123./25. ); // Basis function

  //PDE residual: (lifting)
  rsdLiftTrue = ( 9./5. );   // Basis function

  SANS_CHECK_CLOSE( rsdPDETrue, efld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftTrue, eLfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, eLfld.DOF(1), small_tol, close_tol );
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_Dirichlet_mitState_2D_1Triangle_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD2::D, ArrayQ> VectorArrayQ;

  typedef BCTypeDirichlet_mitState<AdvectiveFlux2D_Uniform,ViscousFlux2D_Uniform> BCTypeDirichlet_mitState2D;

  typedef BCNDConvertSpace<PhysD2, BCAdvectionDiffusion<PhysD2,BCTypeDirichlet_mitState2D> > BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC
  Real uB = 12./5.;
  BCClass bc(pde, uB);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( qfld.nDOF() == 3 );

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  // lifting-operator: P1 (aka R1)
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK( rfld.nDOF() == 9 );

  rfld = 0;
  // triangle auxiliary variable (left)
  rfld.DOF(0) = { 2, -3};   rfld.DOF(1) = { 7,  8};   rfld.DOF(2) = {-1,  7};

  // weighting : P2
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK( wfld.nDOF() == 6 );

  // triangle solution (left)
  wfld.DOF(0) = -2;
  wfld.DOF(1) =  4;
  wfld.DOF(2) =  3;
  wfld.DOF(3) =  2;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  // lifting-operator: P1 (aka R1)
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> sfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( sfld.nDOF(), 18 );

  sfld = 0;
  // triangle auxiliary variable (left)
  sfld.DOF(0) = { 1,  5};   sfld.DOF(1) = { 3,  6};   sfld.DOF(2) = {-1,  7};
  sfld.DOF(3) = { 3,  1};   sfld.DOF(4) = { 2,  2};   sfld.DOF(5) = { 4,  3};

  // Estimate Fields
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> efld(xfld, 0, BasisFunctionCategory_Legendre);

  efld = 0;

  BOOST_CHECK( 1 == efld.nElem() );
  BOOST_CHECK( 1 == efld.nDOF() );

  // lifting operator: single line, P0 (aka Q0)
  FieldLift_DG_Cell<PhysD2, TopoD2, Real> eLfld(xfld, 0, BasisFunctionCategory_Legendre);

  // left element
  eLfld=0;

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcn( pde, bc, {1}, disc );

  // quadrature rule
  int quadratureorder[3] = {3,3,3};

  IntegrateBoundaryTraceGroups<TopoD2>::integrate( ErrorEstimateBoundaryTrace_DGBR2(fcn),
                                                   xfld,
                                                   (qfld,rfld,wfld,sfld,efld,eLfld),
                                                   quadratureorder, 3 );

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  // Taken from IntegrandBoundaryTrace_Dirichlet_mitState_DGBR2_FW_AD_btest

  Real rsdPDETrue,rsdLiftTrue;

  //PDE residuals (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  rsdPDETrue = ( 1313/60. ) + ( -38077/750. ) + ( -81259/7500. ) + ( -75634/125. );   // Weight function

  //LO residuals (left): (lifting-operator)
  rsdLiftTrue = ( 164/15. ); // Weight function

  SANS_CHECK_CLOSE( rsdPDETrue, efld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftTrue, eLfld.DOF(0), small_tol, close_tol );

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
