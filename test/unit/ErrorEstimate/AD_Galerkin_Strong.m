% AD_Galerkin_Strong_Unit_Tests
close all
clear all

% 1D
if 0
    disp('1D Cell');
    syms u x
    ui = [ 5; -4; 3; -2 ];
    w = [1-x, x, 4*x*(1-x), 6*sqrt(3)*x*(1-x)*(1-2*x)];
    u = w*ui;
    
    K = 2123/1000;
    C = 11/10;
    
    ux = diff(u,x);
    uxx = diff(ux,x);
    
    integrand = w*(C*ux - K*uxx);
    subs(integrand, x, 0)
    subs(integrand, x, 1/3)
    subs(integrand, x, 1/2)
    subs(integrand, x, 9/10)
    
elseif 0
    disp('1D Trace');
    syms u x
    uiL = [ 5; -3;  2; -4 ];
    uiR = [-3;  2; -4;  5 ];
    wL = [1-x, x, 4*x*(1-x), 6*sqrt(3)*x*(1-x)*(1-2*x)];
    uL = wL*uiL;
    wR = subs(wL,x,x-1); % shifting all to the right by 1
    uR = wR*uiR;
    
    UL = matlabFunction(uL);
    UR = matlabFunction(uR);
    xplot = linspace(0,1,10000);
    plot(xplot,UL(xplot));
    hold on
    plot(xplot+1,UR(xplot+1)); % checking that they're c0
    
    K = 2123/1000;
    C = 11/10;
    
    uxL = diff(uL, x);
    uxR = diff(uR, x);
    
    nL = 1;nR = -1;
    
    integrandL = -0.5*wL*(-K*(uxL-uxR));
    subs(integrandL,x,1)
    integrandR = -0.5*wR*(-K*(uxL-uxR));
    subs(integrandR,x,1)
    
elseif 0
    disp('2D Cell')
    
    syms u s t
    sgn = [1,1,1];
    w = [u, s, t, ...
        4*s*t, 6*sqrt(3)*s*t*(t-s)*sgn(1), ...
        4*t*u, 6*sqrt(3)*t*u*(u-t)*sgn(2), ...
        4*u*s, 6*sqrt(3)*u*s*(s-u)*sgn(3), ...
        27*s*t*u];
    w = subs(w,u,1-s-t);
    ui = [1,-2,3,-4,5,-1,2,-3,4,-5];
    C = [1.1,0.2];
    K = [ 2.123, 0.553; 0.553,1.321];
    
    u = sum(ui.*w);
    
    ux = diff(u,s);
    uy = diff(u,t);
    gradu = [ux;uy];
    Finv = C*gradu;
    
    uxx = diff(ux,s);
    uxy = diff(ux,t);
    uyy = diff(uy,t);
    Fvis = -K(1,1)*uxx - (K(2,1)+K(1,2))*uxy - K(2,2)*uyy;
    
    integrand = w*(Finv+Fvis);
    subs(integrand,[s,t],[0,0])
    subs(integrand,[s,t],[0,1])
    subs(integrand,[s,t],[1,0])
    subs(integrand,[s,t],[1/2,1/2])
    subs(integrand,[s,t],[1/3,1/3])
    subs(integrand,[s,t],[1/10,2/3])
    subs(integrand,[s,t],[1/8,1/5])
    
elseif 1
    disp('2D Trace')
    
    syms u s t
    sgn = [1,1,1];
    nL = [1/sqrt(2),1/sqrt(2)];
    wL = [u, s, t, ...
        4*s*t, 6*sqrt(3)*s*t*(t-s)*sgn(1), ...
        4*t*u, 6*sqrt(3)*t*u*(u-t)*sgn(2), ...
        4*u*s, 6*sqrt(3)*u*s*(s-u)*sgn(3), ...
        27*s*t*u];
    wL = subs(wL,u,1-s-t);
    wR = subs(wL,[s,t],[1-s,1-t]);
    
    uiL = [1,-2,3,-4,5,-1,2,-3,4,-5];
    uiR = [5,-4,3,-2,1,-5,4,-3,2,-1];
    K = [ 2.123, 0.553; 0.553,1.321];
    
    uL = sum(uiL.*wL);
    uR = sum(uiR.*wR);
    
    uxL = diff(uL,s);
    uyL = diff(uL,t);
    graduL = [uxL;uyL];
    
    uxR = diff(uR,s);
    uyR = diff(uR,t);
    graduR = [uxR;uyR];
    
    FvisL = -K*graduL;
    FvisR = -K*graduR;
    
    FvisJ = nL*(FvisL-FvisR);
    integrandL = -0.5*wL*FvisJ;
    integrandR = -0.5*wR*FvisJ;
    
    sFace = 0;
    subs(integrandL,[s,t],[1-sFace,sFace])
    subs(integrandR,[s,t],[1-sFace,sFace])
    
    sFace = 1/2;
    subs(integrandL,[s,t],[1-sFace,sFace])
    subs(integrandR,[s,t],[1-sFace,sFace])
    
    sFace = 1;
    subs(integrandL,[s,t],[1-sFace,sFace])
    subs(integrandR,[s,t],[1-sFace,sFace])
    
    sFace = 1/8;
    subs(integrandL,[s,t],[1-sFace,sFace])
    subs(integrandR,[s,t],[1-sFace,sFace])
    
end

