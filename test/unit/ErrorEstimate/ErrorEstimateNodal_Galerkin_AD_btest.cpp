// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEstimate_DGBR2_AD_btest
// testing of Error Estimate umbrella function for DG BR2 with Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin_StrongForm.h"
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
// #include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
// #include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"


#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/BCParameters.h"


#include "pde/BCParameters.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
// #include "pde/NDConvert/PDENDConvertSpace2D.h"
// #include "pde/NDConvert/BCNDConvertSpace2D.h"

// for the p0 eflds
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"

// #include "Field/FieldArea_DG_Cell.h"
// #include "Field/FieldArea_DG_BoundaryTrace.h"
// #include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"

// #include "Field/FieldArea_CG_Cell.h"
// #include "Field/FieldArea_CG_BoundaryTrace.h"

#include "Field/Tuple/FieldTuple.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"

#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
//#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"

#include "ErrorEstimate/Galerkin/ErrorEstimateNodal_Galerkin.h"
#include "ErrorEstimate/Galerkin/ErrorEstimateCell_Galerkin.h"
#include "ErrorEstimate/Galerkin/ErrorEstimateInteriorTrace_Galerkin.h"

#include "ErrorEstimate/Galerkin/ErrorEstimateBoundaryTrace_FieldTrace_Galerkin.h"
#include "ErrorEstimate/Galerkin/ErrorEstimateBoundaryTrace_Galerkin.h"

#include "ErrorEstimate/ErrorEstimate_Common.h"

#include "Field/Local/XField_LocalPatch.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEstimateNodal_Galerkin_AD_test_suite )

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateNodal_Galerkin_LinearScalar_mitLG_BC_2Line_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
          AdvectiveFlux1D_Uniform,
          ViscousFlux1D_Uniform,
          Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;

  typedef IntegrandCell_Galerkin_Stabilized<PDEClass> IntegrandCellClass;
  // typedef IntegrandInteriorTrace_Galerkin_StrongForm<PDEClass> IntegrandInteriorTraceClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandBoundaryTraceClass;

  // // For subtracting off the weak bc values
  // typedef BCNDConvertSpace<PhysD1, BCNoneClassRaw> BCNoneClass;
  // typedef NDVectorCategory<boost::mpl::vector1<BCNoneClass>, BCNoneClass::Category> NDNoneBCVecCat;
  // typedef IntegrandBoundaryTrace<PDEClass, NDNoneBCVecCat, Galerkin> IntegrandBoundaryTraceNoneClass;

  // Algebraic Eq set things
  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_Galerkin<PDEClass, BCNDConvertSpace, BCVector,
                                        AlgEqSetTraits_Dense, XField<PhysD1, TopoD1>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef ErrorEstimateNodal_Galerkin<PDEClass, BCNDConvertSpace, BCVector, XField<PhysD1,TopoD1>> ErrorEstimateClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BCs

  Real A = 1.0,B = 2.0,bcdata = 3.0; // Robin
  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  PyDict BCRobin;
  BCRobin[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BCRobin[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_mitLG>::params.A] = A;
  BCRobin[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_mitLG>::params.B] = B;
  BCRobin[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_mitLG>::params.bcdata] = bcdata;

  PyDict BCList;
  BCList["RobinL"] = BCRobin;
  BCList["RobinR"] = BCRobin;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  //Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["RobinL"] = {0};
  BCBoundaryGroups["RobinR"] = {1};

  // Check the BC dictionary
  BCParams::checkInputs(BCList);

  // grid: single triangle, P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 2, xfld.nElem() );


  std::vector<int> active_LG_boundaries{0,1};

  // solution: single line, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution data (left)
  qfld.DOF(0) = 2;
  qfld.DOF(1) = 3;
  // solution data (right)
  qfld.DOF(2) = 7;
  qfld.DOF(3) = 4;

  BOOST_CHECK_EQUAL( 4, qfld.nDOF() );
  BOOST_CHECK( qfld.D == pde.D);

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld(xfld, 0, BasisFunctionCategory_Legendre, active_LG_boundaries);

  // Lagrange Multiplier DOF data
  lgfld.DOF(0) = 0;
  lgfld.DOF(1) = 0;

  BOOST_CHECK_EQUAL(2, lgfld.nDOF() );

  // weight: single line, P2 (aka Q2)
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  // line solution data (left)
  wfld.DOF(0) = 3;
  wfld.DOF(1) = 4;
  wfld.DOF(2) = 5;

  // line solution data (right)
  wfld.DOF(3) =  5;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  BOOST_CHECK_EQUAL( 6, wfld.nDOF() );
  BOOST_CHECK( wfld.D == pde.D);

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> mufld(xfld, 0, BasisFunctionCategory_Legendre, active_LG_boundaries);

  // Lagrange Multiplier DOF data
  mufld.DOF(0) =  0;
  mufld.DOF(1) =  0;

  BOOST_CHECK_EQUAL(2, mufld.nDOF() );

  // estimate fields
  // Field_DG_Cell<PhysD1, TopoD1, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);
  // Field_DG_Cell<PhysD1, TopoD1, Real> eSfld(xfld, 0, BasisFunctionCategory_Legendre);
  // Field_DG_Cell<PhysD1, TopoD1, Real> ifld(xfld, 0, BasisFunctionCategory_Legendre);

  Field_CG_Cell<PhysD1, TopoD1, Real> efld_nodal(xfld, 1, BasisFunctionCategory_Hierarchical);
  Field_CG_Cell<PhysD1, TopoD1, Real> eSfld_nodal(xfld, 1, BasisFunctionCategory_Hierarchical);
  Field_CG_Cell<PhysD1, TopoD1, Real> ifld_nodal(xfld, 1, BasisFunctionCategory_Hierarchical);

  // solution data
  // efld = 0; eSfld = 0; ifld = 0;
  efld_nodal = 0; eSfld_nodal = 0; ifld_nodal = 0;

  BOOST_CHECK_EQUAL( 2, efld_nodal.nElem() );
  BOOST_CHECK_EQUAL( 3, efld_nodal.nDOF() );

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD1, TopoD1, Real> eBfld_nodal(xfld, 1, BasisFunctionCategory_Hierarchical, active_LG_boundaries);

  // Lagrange Multiplier DOF data
  eBfld_nodal = 0;

  BOOST_CHECK_EQUAL(2, eBfld_nodal.nDOF() );

  // cubic rule
  int quadratureOrder = 3;
  int quadratureOrderMin[2]={0,0};

  // Stabilization
  const StabilizationMatrix stab(StabilizationType::Unstabilized, TauType::Glasby, qorder+1);

  // integrand
  IntegrandCellClass fcnCellint( pde, {0}, stab );
  IntegrandBoundaryTraceClass fcnBouTraceint( pde, bc, active_LG_boundaries);

  IntegrateCellGroups<TopoD1>::integrate( ErrorEstimateCell_Galerkin(fcnCellint),
                                          xfld,
                                          (qfld, wfld, efld_nodal),
                                          &quadratureOrder, 1 );

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ErrorEstimateBoundaryTrace_FieldTrace_Galerkin(fcnBouTraceint),
                                                              xfld,
                                                              (qfld,wfld,efld_nodal),
                                                              (lgfld,mufld,eBfld_nodal),
                                                              quadratureOrderMin, 2 );

  for (int i = 0; i < efld_nodal.nDOF(); i++)
  {
    eSfld_nodal.DOF(i) = efld_nodal.DOF(i);
    ifld_nodal.DOF(i) = fabs(efld_nodal.DOF(i));
  }


  QuadratureOrder quadOrder(xfld, -1);

  // Construct ErrorEstimateClass
  ErrorEstimateClass ErrorEstimate(xfld,
                                   qfld, lgfld, wfld, mufld,
                                   pde, stab, quadOrder, {0}, {0},
                                   BCList, BCBoundaryGroups);
  const Field<PhysD1,TopoD1,Real>& efld2 = ErrorEstimate.getEField();
  const Field<PhysD1,TopoD1,Real>& eSfld2 = ErrorEstimate.getESField();
  const Field<PhysD1,TopoD1,Real>& ifld2 = ErrorEstimate.getIField();

  std::vector<std::vector<Real>> errorArray;
  ErrorEstimate.fillEArray(errorArray); // savithru's slightly more fragile interface

  BOOST_CHECK_EQUAL( 3, efld2.nDOF() );
  BOOST_CHECK_EQUAL( 3, eSfld2.nDOF() );
  BOOST_CHECK_EQUAL( 3, ifld2.nDOF() );

  Real globalEstimate=0,trueSum=0;
  Real globalAggregate=0,trueAgg=0;
  ErrorEstimate.getErrorEstimate(globalEstimate);
  ErrorEstimate.getErrorIndicator(globalAggregate);

  // KahanSum<Real> tmp = 0;
  // for_each_CellGroup<TopoD1>::apply( FieldSum<PhysD1>(tmp,{0},true),efld2);
  // trueAgg = static_cast<Real>(tmp);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int dof = 0; dof < efld_nodal.nDOF(); dof++)
  {
    SANS_CHECK_CLOSE( efld_nodal.DOF(dof), efld2.DOF(dof), small_tol, close_tol );
    SANS_CHECK_CLOSE( eSfld_nodal.DOF(dof), eSfld2.DOF(dof), small_tol, close_tol );
    SANS_CHECK_CLOSE( ifld_nodal.DOF(dof), ifld2.DOF(dof), small_tol, close_tol );
    trueAgg += ifld_nodal.DOF(dof);
    trueSum += eSfld2.DOF(dof);
  }

  SANS_CHECK_CLOSE( trueSum, globalEstimate, small_tol, close_tol );
  SANS_CHECK_CLOSE( trueAgg, globalAggregate, small_tol, close_tol );

}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateNodal_Galerkin_FieldBundleConstructor_LinearScalar_mitLG_BC_2Line_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
          AdvectiveFlux1D_Uniform,
          ViscousFlux1D_Uniform,
          Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;

  typedef IntegrandCell_Galerkin_Stabilized<PDEClass> IntegrandCellClass;
  // typedef IntegrandInteriorTrace_Galerkin_StrongForm<PDEClass> IntegrandInteriorTraceClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandBoundaryTraceClass;

  // // For subtracting off the weak bc values
  // typedef BCNDConvertSpace<PhysD1, BCNoneClassRaw> BCNoneClass;
  // typedef NDVectorCategory<boost::mpl::vector1<BCNoneClass>, BCNoneClass::Category> NDNoneBCVecCat;
  // typedef IntegrandBoundaryTrace<PDEClass, NDNoneBCVecCat, Galerkin> IntegrandBoundaryTraceNoneClass;

  // Algebraic Eq set things
  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_Galerkin<PDEClass, BCNDConvertSpace, BCVector,
                                        AlgEqSetTraits_Dense, XField<PhysD1, TopoD1>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef ErrorEstimateNodal_Galerkin<PDEClass, BCNDConvertSpace, BCVector, XField<PhysD1,TopoD1>> ErrorEstimateClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BCs

  Real A = 1.0,B = 2.0,bcdata = 3.0; // Robin
  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  PyDict BCRobin;
  BCRobin[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BCRobin[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_mitLG>::params.A] = A;
  BCRobin[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_mitLG>::params.B] = B;
  BCRobin[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_mitLG>::params.bcdata] = bcdata;

  PyDict BCList;
  BCList["RobinL"] = BCRobin;
  BCList["RobinR"] = BCRobin;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  //Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["RobinL"] = {0};
  BCBoundaryGroups["RobinR"] = {1};

  // Check the BC dictionary
  BCParams::checkInputs(BCList);

  // grid: single triangle, P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 2, xfld.nElem() );

  int qorder = 1;

  std::vector<int> active_LG_boundaries{0,1};

  // solution: single line, P1 (aka Q1)
  FieldBundle_Galerkin<PhysD1,TopoD1,ArrayQ> primal(xfld, qorder, BasisFunctionCategory_Hierarchical,
                                                                  BasisFunctionCategory_Hierarchical, active_LG_boundaries );
  // weight: single line, P2 (aka Q2)
  FieldBundle_Galerkin<PhysD1,TopoD1,ArrayQ> adjoint(xfld, qorder+1, BasisFunctionCategory_Hierarchical,
                                                                     BasisFunctionCategory_Hierarchical, active_LG_boundaries );

  // solution data (left)
  primal.qfld.DOF(0) = 2;
  primal.qfld.DOF(1) = 3;
  // solution data (right)
  primal.qfld.DOF(2) = 7;

  primal.lgfld = 0;

  BOOST_CHECK_EQUAL( 3, primal.qfld.nDOF() );
  BOOST_CHECK( primal.qfld.D == pde.D);

  // line solution data (left)
  adjoint.qfld.DOF(0) = 3;
  adjoint.qfld.DOF(1) = 4;
  adjoint.qfld.DOF(2) = 5;

  // line solution data (right)
  adjoint.qfld.DOF(3) =  5;
  adjoint.qfld.DOF(4) =  4;

  adjoint.lgfld = 0;

  BOOST_CHECK_EQUAL( 5, adjoint.qfld.nDOF() );
  BOOST_CHECK( adjoint.qfld.D == pde.D);


  Field_CG_Cell<PhysD1, TopoD1, Real> efld_nodal(xfld, 1, BasisFunctionCategory_Hierarchical);
  Field_CG_Cell<PhysD1, TopoD1, Real> eSfld_nodal(xfld, 1, BasisFunctionCategory_Hierarchical);
  Field_CG_Cell<PhysD1, TopoD1, Real> ifld_nodal(xfld, 1, BasisFunctionCategory_Hierarchical);

  // solution data
  // efld = 0; eSfld = 0; ifld = 0;
  efld_nodal = 0; eSfld_nodal = 0; ifld_nodal = 0;

  BOOST_CHECK_EQUAL( 2, efld_nodal.nElem() );
  BOOST_CHECK_EQUAL( 3, efld_nodal.nDOF() );

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD1, TopoD1, Real> eBfld_nodal(xfld, 1, BasisFunctionCategory_Hierarchical, active_LG_boundaries);

  // Lagrange Multiplier DOF data
  eBfld_nodal = 0;

  BOOST_CHECK_EQUAL(2, eBfld_nodal.nDOF() );

  // cubic rule
  int quadratureOrder = 3;
  int quadratureOrderMin[2]={0,0};

  // Stabilization
  const StabilizationMatrix stab(StabilizationType::Unstabilized, TauType::Glasby, qorder+1);

  // integrand
  IntegrandCellClass fcnCellint( pde, {0}, stab );
  IntegrandBoundaryTraceClass fcnBouTraceint( pde, bc, active_LG_boundaries);

  IntegrateCellGroups<TopoD1>::integrate( ErrorEstimateCell_Galerkin(fcnCellint),
                                          xfld,
                                          (primal.qfld, adjoint.qfld, efld_nodal),
                                          &quadratureOrder, 1 );

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ErrorEstimateBoundaryTrace_FieldTrace_Galerkin(fcnBouTraceint),
                                                              xfld,
                                                              (primal.qfld,adjoint.qfld,efld_nodal),
                                                              (primal.lgfld,adjoint.lgfld,eBfld_nodal),
                                                              quadratureOrderMin, 2 );

  for (int i = 0; i < efld_nodal.nDOF(); i++)
  {
    eSfld_nodal.DOF(i) = efld_nodal.DOF(i);
    ifld_nodal.DOF(i) = fabs(efld_nodal.DOF(i));
  }

  QuadratureOrder quadOrder(xfld, -1);

  BOOST_CHECK_EQUAL(2, eBfld_nodal.nDOF() );

  // Construct ErrorEstimateClass
  ErrorEstimateClass ErrorEstimate(xfld,
                                   primal, adjoint, nullptr,
                                   pde, stab, quadOrder, {0}, {0},
                                   BCList, BCBoundaryGroups);

  BOOST_CHECK_EQUAL(2, eBfld_nodal.nDOF() );

  const Field<PhysD1,TopoD1,Real>& efld2 = ErrorEstimate.getEField();
  const Field<PhysD1,TopoD1,Real>& eSfld2 = ErrorEstimate.getESField();
  const Field<PhysD1,TopoD1,Real>& ifld2 = ErrorEstimate.getIField();

  std::vector<std::vector<Real>> errorArray;
  ErrorEstimate.fillEArray(errorArray); // savithru's slightly more fragile interface

  BOOST_CHECK_EQUAL( 3, efld2.nDOF() );
  BOOST_CHECK_EQUAL( 3, eSfld2.nDOF() );
  BOOST_CHECK_EQUAL( 3, ifld2.nDOF() );

  Real globalEstimate=0,trueSum=0;
  Real globalAggregate=0,trueAgg=0;
  ErrorEstimate.getErrorEstimate(globalEstimate);
  ErrorEstimate.getErrorIndicator(globalAggregate);

  // KahanSum<Real> tmp = 0;
  // for_each_CellGroup<TopoD1>::apply( FieldSum<PhysD1>(tmp,{0},true),efld2);
  // trueAgg = static_cast<Real>(tmp);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int dof = 0; dof < efld_nodal.nDOF(); dof++)
  {
    SANS_CHECK_CLOSE( efld_nodal.DOF(dof), efld2.DOF(dof), small_tol, close_tol );
    SANS_CHECK_CLOSE( eSfld_nodal.DOF(dof), eSfld2.DOF(dof), small_tol, close_tol );
    SANS_CHECK_CLOSE( ifld_nodal.DOF(dof), ifld2.DOF(dof), small_tol, close_tol );
    trueAgg += ifld_nodal.DOF(dof);
    trueSum += eSfld2.DOF(dof);
  }

  SANS_CHECK_CLOSE( trueSum, globalEstimate, small_tol, close_tol );
  SANS_CHECK_CLOSE( trueAgg, globalAggregate, small_tol, close_tol );


}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateNodal_Galerkin_FieldBundles_mitState_2Line_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
          AdvectiveFlux1D_Uniform,
          ViscousFlux1D_Uniform,
          Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion<PhysD1,BCTypeDirichlet_mitState<AdvectiveFlux1D_Uniform,ViscousFlux1D_Uniform>> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;

  typedef IntegrandCell_Galerkin_Stabilized<PDEClass> IntegrandCellClass;
  // typedef IntegrandInteriorTrace_Galerkin_StrongForm<PDEClass> IntegrandInteriorTraceClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandBoundaryTraceClass;


  // Algebraic Eq set things
  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_Galerkin<PDEClass, BCNDConvertSpace, BCVector,
                                        AlgEqSetTraits_Dense, XField<PhysD1, TopoD1>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef ErrorEstimateNodal_Galerkin<PDEClass, BCNDConvertSpace, BCVector, XField<PhysD1,TopoD1>> ErrorEstimateClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BCs

  Real bcdata = 3.0; // Robin
  BCClass bc(pde,bcdata);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );


  PyDict BCDirichlet;
  BCDirichlet[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichlet[BCAdvectionDiffusionParams<PhysD1,BCTypeDirichlet_mitStateParam>::params.qB] = bcdata;

  PyDict BCList;
  BCList["DirichletL"] = BCDirichlet;
  BCList["DirichletR"] = BCDirichlet;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  //Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["DirichletL"] = {0};
  BCBoundaryGroups["DirichletR"] = {1};

  // Check the BC dictionary
  BCParams::checkInputs(BCList);

  // grid: single triangle, P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 2, xfld.nElem() );

  int qorder = 1;

  std::vector<int> active_LG_boundaries{};

  // solution: single line, P1 (aka Q1)
  FieldBundle_Galerkin<PhysD1,TopoD1,ArrayQ> primal(xfld, qorder, BasisFunctionCategory_Hierarchical,
                                                                  BasisFunctionCategory_Hierarchical, active_LG_boundaries );
  // weight: single line, P2 (aka Q2)
  FieldBundle_Galerkin<PhysD1,TopoD1,ArrayQ> adjoint(xfld, qorder+1, BasisFunctionCategory_Hierarchical,
                                                                     BasisFunctionCategory_Hierarchical, active_LG_boundaries );

  // solution data (left)
  primal.qfld.DOF(0) = 2;
  primal.qfld.DOF(1) = 3;
  // solution data (right)
  primal.qfld.DOF(2) = 7;

  primal.lgfld = 0;

  BOOST_CHECK_EQUAL( 3, primal.qfld.nDOF() );
  BOOST_CHECK( primal.qfld.D == pde.D);

  // line solution data (left)
  adjoint.qfld.DOF(0) = 3;
  adjoint.qfld.DOF(1) = 4;
  adjoint.qfld.DOF(2) = 5;

  // line solution data (right)
  adjoint.qfld.DOF(3) =  5;
  adjoint.qfld.DOF(4) =  4;

  adjoint.lgfld = 0;

  BOOST_CHECK_EQUAL( 5, adjoint.qfld.nDOF() );
  BOOST_CHECK( adjoint.qfld.D == pde.D);


  Field_CG_Cell<PhysD1, TopoD1, Real> efld_nodal(xfld, 1, BasisFunctionCategory_Hierarchical);
  Field_CG_Cell<PhysD1, TopoD1, Real> eSfld_nodal(xfld, 1, BasisFunctionCategory_Hierarchical);
  Field_CG_Cell<PhysD1, TopoD1, Real> ifld_nodal(xfld, 1, BasisFunctionCategory_Hierarchical);

  // solution data
  // efld = 0; eSfld = 0; ifld = 0;
  efld_nodal = 0; eSfld_nodal = 0; ifld_nodal = 0;

  BOOST_CHECK_EQUAL( 2, efld_nodal.nElem() );
  BOOST_CHECK_EQUAL( 3, efld_nodal.nDOF() );


  // cubic rule
  int quadratureOrder = 3;
  int quadratureOrderMin[2]={0,0};

  // Stabilization
  const StabilizationMatrix stab(StabilizationType::Unstabilized, TauType::Glasby, qorder+1);

  // integrand
  IntegrandCellClass fcnCellint( pde, {0}, stab );
  IntegrandBoundaryTraceClass fcnBouTraceint( pde, bc, {0,1}, stab);

  IntegrateCellGroups<TopoD1>::integrate( ErrorEstimateCell_Galerkin(fcnCellint),
                                          xfld,
                                          (primal.qfld, adjoint.qfld, efld_nodal),
                                          &quadratureOrder, 1 );

  IntegrateBoundaryTraceGroups<TopoD1>::integrate( ErrorEstimateBoundaryTrace_Galerkin(fcnBouTraceint),
                                                   xfld,
                                                   (primal.qfld,  adjoint.qfld,  efld_nodal),
                                                   // (primal.lgfld, adjoint.lgfld, eBfld_nodal),
                                                   quadratureOrderMin, 2 );

  for (int i = 0; i < efld_nodal.nDOF(); i++)
  {
    eSfld_nodal.DOF(i) = efld_nodal.DOF(i);
    ifld_nodal.DOF(i) = fabs(efld_nodal.DOF(i));
  }

  QuadratureOrder quadOrder(xfld, -1);

  // Construct ErrorEstimateClass
  ErrorEstimateClass ErrorEstimate(xfld,
                                   primal, adjoint, nullptr,
                                   pde, stab, quadOrder, {0}, {0},
                                   BCList, BCBoundaryGroups);

  const Field<PhysD1,TopoD1,Real>& efld2 = ErrorEstimate.getEField();
  const Field<PhysD1,TopoD1,Real>& eSfld2 = ErrorEstimate.getESField();
  const Field<PhysD1,TopoD1,Real>& ifld2 = ErrorEstimate.getIField();

  std::vector<std::vector<Real>> errorArray;
  ErrorEstimate.fillEArray(errorArray); // savithru's slightly more fragile interface

  BOOST_CHECK_EQUAL( 3, efld2.nDOF() );
  BOOST_CHECK_EQUAL( 3, eSfld2.nDOF() );
  BOOST_CHECK_EQUAL( 3, ifld2.nDOF() );

  Real globalEstimate=0,trueSum=0;
  Real globalAggregate=0,trueAgg=0;
  ErrorEstimate.getErrorEstimate(globalEstimate);
  ErrorEstimate.getErrorIndicator(globalAggregate);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int dof = 0; dof < efld_nodal.nDOF(); dof++)
  {
    SANS_CHECK_CLOSE( efld_nodal.DOF(dof), efld2.DOF(dof), small_tol, close_tol );
    SANS_CHECK_CLOSE( eSfld_nodal.DOF(dof), eSfld2.DOF(dof), small_tol, close_tol );
    SANS_CHECK_CLOSE( ifld_nodal.DOF(dof), ifld2.DOF(dof), small_tol, close_tol );
    trueAgg += ifld_nodal.DOF(dof);
    trueSum += eSfld2.DOF(dof);
  }

  SANS_CHECK_CLOSE( trueSum, globalEstimate, small_tol, close_tol );
  SANS_CHECK_CLOSE( trueAgg, globalAggregate, small_tol, close_tol );


}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateNodal_Galerkin_LocalFieldBundles_mitState_2Line_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
          AdvectiveFlux1D_Uniform,
          ViscousFlux1D_Uniform,
          Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;

  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  // Algebraic Eq set things
  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_Galerkin<PDEClass, BCNDConvertSpace, BCVector,
                                        AlgEqSetTraits_Dense, XField<PhysD1, TopoD1>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef ErrorEstimateNodal_Galerkin<PDEClass, BCNDConvertSpace, BCVector, XField<PhysD1,TopoD1>> ErrorEstimateClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BCs
  Real bcdata = 3.0;

  PyDict BCDirichlet;
  BCDirichlet[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichlet[BCAdvectionDiffusionParams<PhysD1,BCTypeDirichlet_mitStateParam>::params.qB] = bcdata;

  PyDict BCList;
  BCList["DirichletL"] = BCDirichlet;
  BCList["DirichletR"] = BCDirichlet;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  //Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["DirichletL"] = {0};
  BCBoundaryGroups["DirichletR"] = {1};

  // Check the BC dictionary
  BCParams::checkInputs(BCList);

  // grid: single triangle, P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 2, xfld.nElem() );

  int qorder = 1;

  // solution: single line, P1 (aka Q1)
  FieldBundle_Galerkin<PhysD1,TopoD1,ArrayQ> primal(xfld, qorder, BasisFunctionCategory_Hierarchical,
                                                                  BasisFunctionCategory_Hierarchical, {} );
  // weight: single line, P2 (aka Q2)
  FieldBundle_Galerkin<PhysD1,TopoD1,ArrayQ> adjoint(xfld, qorder+1, BasisFunctionCategory_Hierarchical,
                                                                     BasisFunctionCategory_Hierarchical, {} );

  // solution data (left)
  primal.qfld.DOF(0) = 2;
  primal.qfld.DOF(1) = 3;
  // solution data (right)
  primal.qfld.DOF(2) = 7;

  primal.lgfld = 0;

  BOOST_CHECK_EQUAL( 3, primal.qfld.nDOF() );
  BOOST_CHECK( primal.qfld.D == pde.D);

  // line solution data (left)
  adjoint.qfld.DOF(0) = 3;
  adjoint.qfld.DOF(1) = 4;
  adjoint.qfld.DOF(2) = 5;

  // line solution data (right)
  adjoint.qfld.DOF(3) =  5;
  adjoint.qfld.DOF(4) =  4;

  adjoint.lgfld = 0;

  BOOST_CHECK_EQUAL( 5, adjoint.qfld.nDOF() );
  BOOST_CHECK( adjoint.qfld.D == pde.D);

  QuadratureOrder quadOrder(xfld, -1);

  // Stabilization
  const StabilizationMatrix stab(StabilizationType::Unstabilized, TauType::Glasby, qorder+1);

  // Construct ErrorEstimateClass
  ErrorEstimateClass ErrorEstimate(xfld,
                                   primal, adjoint, nullptr,
                                   pde, stab, quadOrder, {0}, {0},
                                   BCList, BCBoundaryGroups);

  // construct an unsplit local grid
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);
  Field_NodalView nodalview(xfld,{0});
  std::array<int,2> edge{{1,2}};
  XField_LocalPatchConstructor<PhysD1,Line> xfld_construct( comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);
  XField_LocalPatch<PhysD1,Line> xfld_local(xfld_construct);

  std::vector<int> active_LG_boundaries{};
  FieldBundle_Galerkin_Local<PhysD1,TopoD1,ArrayQ> primal_local( xfld_local, primal, primal.order, active_LG_boundaries );
  FieldBundle_Galerkin_Local<PhysD1,TopoD1,ArrayQ> adjoint_local( xfld_local, adjoint, adjoint.order, active_LG_boundaries );

  // create a local dg estimate field from the global one
  std::unique_ptr<Field_DG_Cell<PhysD1,TopoD1,Real>> up_local_DG_est_fld
    = SANS::make_unique<Field_Local<Field_DG_Cell<PhysD1,TopoD1,Real>>>
        ( xfld_local, ErrorEstimate.efld_DG_nodal(), 1, BasisFunctionCategory_Lagrange);


  QuadratureOrder quadOrder_local(xfld_local, -1);
  ErrorEstimateClass errorEstimate_local( xfld_local,
                                          primal_local, adjoint_local,
                                          up_local_DG_est_fld, nullptr,
                                          pde, stab, quadOrder_local, {0}, {},
                                          BCList, BCBoundaryGroups);


}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
