// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEstimateInteriorTrace_HDG_AD_btest
// testing of trace residual functions for HDG with Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_Trace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"

#include "Field/Tuple/FieldTuple.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/QuadratureOrder.h"

#include "ErrorEstimate/HDG/ErrorEstimateCell_HDG.h"

#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"

#include "unit/Discretization/HDG/IntegrandTest_HDG.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEstimateInteriorTrace_HDG_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateInteriorTrace_HDG_2Line_X1Q1R1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef testspace::IntegrandCell_HDG_None<PDEClass> IntegrandCellClass; //dummy integrand for testing
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandTraceClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;

  XField_CellToTrace<PhysD1, TopoD1> connectivity(xfld);

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 4);

  // line solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 4;

  // line solution data (right)
  qfld.DOF(2) = 7;
  qfld.DOF(3) = 9;

  // lifting-operator: P1 (aka R1)
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( afld.nDOF(), 4 );

  // lifting operators in left element
  afld.DOF(0) =  2;
  afld.DOF(1) =  7;

  // lifting operators in right element
  afld.DOF(2) =  9;
  afld.DOF(3) = -1;

  // interface solution: P0 (aka Q1)
  Field_DG_Trace<PhysD1, TopoD1, ArrayQ> qIfld(xfld, 0, BasisFunctionCategory_Hierarchical); // nodes, can only be P0
  BOOST_REQUIRE_EQUAL( qIfld.nDOF(), 3);
  BOOST_REQUIRE_EQUAL( qIfld.nInteriorTraceGroups(), 1 );

  // node trace data
  qIfld.DOF(0) =  8;

  // weighting: P2 (akak Q2)
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( wfld.nDOF(), 6);

  // line solution data (left)
  wfld.DOF(0) = 3;
  wfld.DOF(1) = 4;
  wfld.DOF(2) = 5;

  // line solution data (right)
  wfld.DOF(3) =  5;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  // lifting-operator: P1 (aka R1)
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> bfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( bfld.nDOF(), 6 );

  // lifting operators in left element
  bfld.DOF(0) = -5;
  bfld.DOF(1) =  3;
  bfld.DOF(2) =  2;

  // lifting operators in right element
  bfld.DOF(3) =  3;
  bfld.DOF(4) =  2;
  bfld.DOF(5) =  4;

  // interface solution: P1 (aka Q1)
  Field_DG_Trace<PhysD1, TopoD1, ArrayQ> wIfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_REQUIRE_EQUAL( wIfld.nDOF(), 3);
  BOOST_REQUIRE_EQUAL( wIfld.nInteriorTraceGroups(), 1 );

  // node trace data
  wIfld.DOF(0) =  -2;

  // estimate fields
  Field_DG_Cell<PhysD1, TopoD1, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);

  // solution data
  efld = 0;

  BOOST_CHECK_EQUAL( 2, efld.nElem() );
  BOOST_CHECK_EQUAL( 2, efld.nDOF() );

  // lifting operator: single line, P0 (aka Q0)
  Field_DG_Cell<PhysD1, TopoD1, Real> eAfld(xfld, 0, BasisFunctionCategory_Legendre);

  eAfld = 0;

  // interface solution: P1 (aka Q1)
  Field_DG_Trace<PhysD1, TopoD1, Real> eIfld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_REQUIRE_EQUAL( eIfld.nDOF(), 3);
  BOOST_REQUIRE_EQUAL( eIfld.nInteriorTraceGroups(), 1 );

  eIfld = 0;

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Local );

  QuadratureOrder quadOrder(xfld, 3);

  // integrand
  IntegrandCellClass fcnCell({0});
  IntegrandTraceClass fcnTrace( pde, disc, {0} );

  IntegrateCellGroups<TopoD1>::integrate( ErrorEstimateCell_HDG( fcnCell, fcnTrace,
                                                                 connectivity, xfld,
                                                                 qfld, afld, qIfld, wfld, bfld, wIfld,
                                                                 efld, eAfld, eIfld,
                                                                 quadOrder.interiorTraceOrders.data(),
                                                                 quadOrder.interiorTraceOrders.size() ),
                                          xfld, (qfld, afld, wfld, bfld, efld, eAfld),
                                          quadOrder.cellOrders.data(), quadOrder.cellOrders.size());

  Real rsdPDELTrue, rsdPDERTrue, rsdAUXLTrue, rsdAUXRTrue, rsdITrue;

  //PDE residuals (left): (advective) + (viscous) + (stabilization)
  rsdPDELTrue = ( 17.6 ) + ( -14861./250. ) + ( -4246./125. );   // Weight function
  rsdPDERTrue = ( -44. ) + ( 19107./200. ) + ( -2123./200. );   // Weight function

  //AUX residuals (left): (Auxiliary)
  rsdAUXLTrue = ( -24 );   // Weight function

  //AUX residuals (right): (Auxiliary)
  rsdAUXRTrue = ( 24 );   // Weight function

  //Int residual integrands (left): (advective) + (viscous)
  rsdITrue = ( -44/5. ) + ( 2123/250. ) + ( -2123/100. );  // Weight function

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  SANS_CHECK_CLOSE( rsdPDELTrue, efld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue, efld.DOF(1), small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdAUXLTrue, eAfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXRTrue, eAfld.DOF(1), small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdITrue, eIfld.DOF(0), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateInteriorTrace_HDG_2Triangle_X1Q1R1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef testspace::IntegrandCell_HDG_None<PDEClass> IntegrandCellClass; //dummy integrand for testing
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandTraceClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: P1 (aka X1)
  XField2D_2Triangle_X1_1Group xfld;

  XField_CellToTrace<PhysD2, TopoD2> connectivity(xfld);

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 6 );

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  // triangle solution data (right)
  qfld.DOF(3) = 7;
  qfld.DOF(4) = 2;
  qfld.DOF(5) = 9;

  // lifting-operator: P1 (aka R1)
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( afld.nDOF(), 6 );

  // triangle auxiliary variable (left)
  afld.DOF(0) = { 2, -3};
  afld.DOF(1) = { 7,  8};
  afld.DOF(2) = {-1, -5};

  // triangle auxiliary variable (right)
  afld.DOF(3) = { 9,  6};
  afld.DOF(4) = {-1,  3};
  afld.DOF(5) = { 2, -3};

  // interface solution
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_REQUIRE_EQUAL( qIfld.nDOF(), 10);
  BOOST_REQUIRE_EQUAL( qIfld.nInteriorTraceGroups(), 1 );

  // node trace data
  qIfld.DOF(0) =  8;
  qIfld.DOF(1) = -1;

  // weighting P2 (aka Q2)
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( wfld.nDOF(), 12 );

  // triangle solution data (left)
  wfld.DOF(0) = -2;
  wfld.DOF(1) =  4;
  wfld.DOF(2) =  3;
  wfld.DOF(3) =  2;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  // triangle solution data (right)
  wfld.DOF(6)  =  7;
  wfld.DOF(7)  =  3;
  wfld.DOF(8)  =  2;
  wfld.DOF(9)  = -1;
  wfld.DOF(10) =  5;
  wfld.DOF(11) = -3;

  // lifting-operator: P1 (aka R1)
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> bfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( bfld.nDOF(), 12 );

  // triangle auxiliary variable (left)
  bfld.DOF(0) = { 1,  5};
  bfld.DOF(1) = { 3,  6};
  bfld.DOF(2) = {-1,  7};
  bfld.DOF(3) = { 3,  1};
  bfld.DOF(4) = { 2,  2};
  bfld.DOF(5) = { 4,  3};

  // triangle auxiliary variable (right)
  bfld.DOF(6)  = { 5,  6};
  bfld.DOF(7)  = { 4,  5};
  bfld.DOF(8)  = { 3,  4};
  bfld.DOF(9)  = { 4,  3};
  bfld.DOF(10) = { 3,  2};
  bfld.DOF(11) = { 2,  1};

  // interface solution
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> wIfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  BOOST_REQUIRE_EQUAL( wIfld.nDOF(), 15);
  BOOST_REQUIRE_EQUAL( wIfld.nInteriorTraceGroups(), 1 );

  // node trace data
  wIfld.DOF(0) =  2;
  wIfld.DOF(1) = -1;
  wIfld.DOF(2) =  3;

  // estimate fields
  Field_DG_Cell<PhysD2, TopoD2, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);

  efld = 0;

  BOOST_CHECK_EQUAL( 2, efld.nElem() );
  BOOST_CHECK_EQUAL( 2, efld.nDOF() );

  // lifting operator: single line, P0 (aka Q0)
  Field_DG_Cell<PhysD2, TopoD2, Real> eAfld(xfld, 0, BasisFunctionCategory_Legendre);

  eAfld=0;

  // interface solution
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> eIfld(xfld, 0, BasisFunctionCategory_Legendre);
  BOOST_REQUIRE_EQUAL( eIfld.nDOF(), 5);
  BOOST_REQUIRE_EQUAL( wIfld.nInteriorTraceGroups(), 1 );

  eIfld = 0;

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Local );

  // quadrature rule (quadratic: basis & flux both linear)
  QuadratureOrder quadOrder(xfld, 3);

  // integrand
  IntegrandCellClass fcnCell({0});
  IntegrandTraceClass fcnTrace( pde, disc, {0} );

  // base interface
  IntegrateCellGroups<TopoD2>::integrate( ErrorEstimateCell_HDG( fcnCell, fcnTrace,
                                                                 connectivity, xfld,
                                                                 qfld, afld, qIfld, wfld, bfld, wIfld,
                                                                 efld, eAfld, eIfld,
                                                                 quadOrder.interiorTraceOrders.data(),
                                                                 quadOrder.interiorTraceOrders.size() ),
                                          xfld, (qfld, afld, wfld, bfld, efld, eAfld),
                                          quadOrder.cellOrders.data(), quadOrder.cellOrders.size());

  Real rsdPDELTrue, rsdPDERTrue;
  Real rsdAUXLTrue, rsdAUXRTrue;
  Real rsdPDEITrue;
  //PDE residuals (left): (advective) + (viscous) + (stabilization)
  rsdPDELTrue = ( 1313/60. ) + ( -21437/375. ) + ( -559/75. ); // Weight function

  //PDE residuals (right): (advective) + (viscous) + (stabilization)
  rsdPDERTrue = ( -221/30. ) + ( 2041/750. )+ ( 12857/375. ); // Weight function

  //Aux residuals (left):
  rsdAUXLTrue = ( -227/6. );   // Basis function

  //AUX residuals (right):
  rsdAUXRTrue = ( 257/6. );   // Basis function

  //INT residuals: (advective) + (viscous) + (stabilization)
  rsdPDEITrue = ( 13/4. )+( 351/10. ) + ( -2236/125. );   // Weight function

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( rsdPDELTrue, efld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue, efld.DOF(1), small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdAUXLTrue, eAfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXRTrue, eAfld.DOF(1), small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdPDEITrue, eIfld.DOF(0), small_tol, close_tol );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
