// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandBoundaryFunctor_HDG_AD_btest
// testing of boundary integrands: HDG Advection-Diffusion on Triangles

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "Discretization/HDG/AWIntegrandBoundaryFunctor_HDG.h"
#include "Discretization/HDG/AWIntegrandBCFunctor_HDG.h"

#include "Field/FieldArea_CG_BoundaryTrace.h"

#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( AWIntegrandBoundaryFunctor_HDG_AD_btest_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AWIntegrand2DBoundaryFunctor_HDG_BCTypeLinearRobin_X1Q1LG1_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;

  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Linear> WeightFunction;

  typedef AWIntegrandBoundary_HDG<PDEClass, WeightFunction> IntegrandBoundaryClass;
  typedef AWIntegrandBC_HDG<PDEClass, WeightFunction, BCClass> IntegrandBCClass;
  typedef IntegrandBoundaryClass::Functor<Real,TopoD1,Line,TopoD2,Triangle> BoundaryFunctorClass;
  typedef IntegrandBCClass::Functor<Real,TopoD1,Line,TopoD2,Triangle> BCFunctorClass;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef Element<ArrayQ,TopoD1,Line> ElementUFieldTrace;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementUFieldCell;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementQFieldCell;

  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC

  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // grid coordinates
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution

  order = 1;
  ElementUFieldCell ufldElemL(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, ufldElemL.order() );
  BOOST_CHECK_EQUAL( 3, ufldElemL.nDOF() );

  // triangle solution
  ufldElemL.DOF(0) = 1;
  ufldElemL.DOF(1) = 3;
  ufldElemL.DOF(2) = 4;

  // auxiliary variable

  ElementQFieldCell qfldElemL(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemL.nDOF() );

  qfldElemL.DOF(0) = { 2, -3};
  qfldElemL.DOF(1) = { 7,  8};
  qfldElemL.DOF(2) = {-1, -5};

  // Lagrange multiplier

  order = 1;
  ElementUFieldTrace lgedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, lgedge.order() );
  BOOST_CHECK_EQUAL( 2, lgedge.nDOF() );

  lgedge.DOF(0) =  7;
  lgedge.DOF(1) = -9;

  // Using the Analytical Weight
  // Weight Function
  Real a0=1.2, a1=0.8, a2=0.3; // a0 + a1*x + a2*y
  WeightFunction wfcn(a0,a1,a2);

  // integrand
  DiscretizationHDG<PDEClass> disc( pde, Global, AugGradient );
  const std::vector<int> BoundaryGroups = {1};
  IntegrandBoundaryClass fcnbound( pde, wfcn, disc, BoundaryGroups );
  IntegrandBCClass fcnbc( pde, wfcn, disc, bc, BoundaryGroups );

  // integrand functor
  BoundaryFunctorClass fcn = fcnbound.integrand( xedge, CanonicalTraceToCell(0, 1), xfldElemL, ufldElemL, qfldElemL );
  BCFunctorClass fcn2 = fcnbc.integrand( xedge, CanonicalTraceToCell(0, 1), xfldElemL, ufldElemL, qfldElemL, lgedge );

  BOOST_CHECK_EQUAL( 3, fcn.nDOFCell() );
  BOOST_CHECK_EQUAL( 2, fcn2.nDOFTrace() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandPDE1, integrandPDE2, integrandPDE3; // the totals gives the quick
  Real integrandAux1, integrandAux2, integrandAux3; // and dirty unit test form
  Real integrandAuy1, integrandAuy2, integrandAuy3;
  BCFunctorClass::IntegrandCellType integrandL;
  ArrayQ integrandBC;
  BCFunctorClass::IntegrandCellType integrandLbc;
  ArrayQ integrandBCbc;

  sRef = 0;

  fcn( sRef, &integrandL, 1, &integrandBC, 1 );
  fcn2( sRef, &integrandLbc, 1, &integrandBCbc, 1 );

  // PDE residuals: (boundary flux) + (Lagrange)
  integrandPDE1 = (0)                    + (0);
  integrandPDE2 = (-6903./(250*sqrt(2))) + ((166136 + 126015*sqrt(2))/12500.);
  integrandPDE3 = (0)                    + (0);

  // Auxiliary residuals: (Lagrange)
  integrandAux1 = 0;
  integrandAux2 = -669*(7803 + 7250*sqrt(2))/312500.;
  integrandAux3 = 0;
  integrandAuy1 = 0;
  integrandAuy2 =  -39*(7803 + 7250*sqrt(2))/31250.;
  integrandAuy3 = 0;

  Real integrandPDET, integrandAuxT, integrandAuyT, integrandBCT;
  // combine
  integrandPDET = a0*integrandPDE1 + (a1+a0)*integrandPDE2 + (a2+a0)*integrandPDE3; // separate weight
  integrandAuxT = -a1*integrandAux1 - (a1)*integrandAux2 - (a1)*integrandAux3; // function into
  integrandAuyT = -a2*integrandAuy1 - (a2)*integrandAuy2 - (a2)*integrandAuy3; // basis functions

  integrandBCT = 0.091648710878564*44.140433708789047; // mu*rBC
  // AD_PDE_Integrand_MMS.m

  SANS_CHECK_CLOSE( integrandPDET, integrandL.PDE+integrandLbc.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuxT, integrandLbc.Au[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuyT, integrandLbc.Au[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCT, integrandBCbc, small_tol, close_tol );

  sRef = 1;
  fcn( sRef, &integrandL, 1, &integrandBC, 1 );
  fcn2( sRef, &integrandLbc, 1, &integrandBCbc, 1 );

  // PDE residuals: (boundary flux) + (Lagrange)
  integrandPDE1 = (0)                   + (0);
  integrandPDE2 = (0)                   + (0);
  integrandPDE3 = (3819./(250*sqrt(2))) + (-(163928 + 172095*sqrt(2))/12500.);

  // Auxiliary residuals: (Lagrange)
  integrandAux1 = 0;
  integrandAux2 = 0;
  integrandAux3 = 669*(2619 + 13250*sqrt(2))/312500.;
  integrandAuy1 = 0;
  integrandAuy2 = 0;
  integrandAuy3 = 39*(2619 + 13250*sqrt(2))/31250.;


  // combine
  integrandPDET = a0*integrandPDE1 + (a1+a0)*integrandPDE2 + (a2+a0)*integrandPDE3; // separate weight
  integrandAuxT = -a1*integrandAux1 - (a1)*integrandAux2 - (a1)*integrandAux3; // function into
  integrandAuyT = -a2*integrandAuy1 - (a2)*integrandAuy2 - (a2)*integrandAuy3; // basis functions
  integrandBCT  = -0.023498475379050*-13.815301279420545; // mu*rBC

  SANS_CHECK_CLOSE( integrandPDET, integrandL.PDE+integrandLbc.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuxT, integrandLbc.Au[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuyT, integrandLbc.Au[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCT, integrandBCbc, small_tol, close_tol );


  sRef = 0.5;
  fcn( sRef, &integrandL, 1, &integrandBC, 1 );
  fcn2( sRef, &integrandLbc, 1, &integrandBCbc, 1 );

  // PDE residuals: (boundary flux) + (Lagrange)
  integrandPDE1 = (0)                   + (0);
  integrandPDE2 = (-771./(250*sqrt(2))) + (6*(23 - 480*sqrt(2))/3125.);
  integrandPDE3 = (-771./(250*sqrt(2))) + (6*(23 - 480*sqrt(2))/3125.);

  // Auxiliary residuals: (Lagrange)
  integrandAux1 = 0;
  integrandAux2 = -2007*(108 - 125*sqrt(2))/78125.;
  integrandAux3 = -2007*(108 - 125*sqrt(2))/78125.;
  integrandAuy1 = 0;
  integrandAuy2 = -234*(108 - 125*sqrt(2))/15625.;
  integrandAuy3 = -234*(108 - 125*sqrt(2))/15625.;

  // combine
  integrandPDET = a0*integrandPDE1 + (a1+a0)*integrandPDE2 + (a2+a0)*integrandPDE3; // separate weight
  integrandAuxT = -a1*integrandAux1 - (a1)*integrandAux2 - (a1)*integrandAux3; // function into
  integrandAuyT = -a2*integrandAuy1 - (a2)*integrandAuy2 - (a2)*integrandAuy3; // basis functions
  integrandBCT  = 0.034075117749757*15.162566214684249;

  SANS_CHECK_CLOSE( integrandPDET, integrandL.PDE+integrandLbc.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuxT, integrandLbc.Au[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuyT, integrandLbc.Au[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCT, integrandBCbc, small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AWIntegrandBoundaryFunctor_HDG_BCTypeFlux_X1Q1LG1_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion< PhysD2, BCTypeFlux<PDEAdvectionDiffusion2D> > BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;

  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Linear> WeightFunction;

  typedef AWIntegrandBoundary_HDG<PDEClass, WeightFunction> IntegrandBoundaryClass;
  typedef AWIntegrandBC_HDG<PDEClass, WeightFunction, BCClass> IntegrandBCClass;
  typedef IntegrandBoundaryClass::Functor<Real,TopoD1,Line,TopoD2,Triangle> BoundaryFunctorClass;
  typedef IntegrandBCClass::Functor<Real,TopoD1,Line,TopoD2,Triangle> BCFunctorClass;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef Element<ArrayQ,TopoD1,Line> ElementUFieldTrace;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementUFieldCell;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementQFieldCell;

  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC

  Real bcdata = 3;
  ArrayQ bcdataVec = bcdata;

  BCClass bc(pde, bcdataVec);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // grid coordinates
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution

  order = 1;
  ElementUFieldCell ufldElemL(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, ufldElemL.order() );
  BOOST_CHECK_EQUAL( 3, ufldElemL.nDOF() );

  // triangle solution
  ufldElemL.DOF(0) = 1;
  ufldElemL.DOF(1) = 3;
  ufldElemL.DOF(2) = 4;

  // auxiliary variable

  ElementQFieldCell qfldElemL(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemL.nDOF() );

  qfldElemL.DOF(0) = { 2, -3};
  qfldElemL.DOF(1) = { 7,  8};
  qfldElemL.DOF(2) = {-1, -5};

  // Lagrange multiplier

  order = 1;
  ElementUFieldTrace lgedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, lgedge.order() );
  BOOST_CHECK_EQUAL( 2, lgedge.nDOF() );

  lgedge.DOF(0) =  7;
  lgedge.DOF(1) = -9;

  // Using the Analytical Weight
  // Weight Function
  Real a0=1.2, a1=0.8, a2=0.3; // a0 + a1*x + a2*y
  WeightFunction wfcn(a0,a1,a2);

  // integrand
  DiscretizationHDG<PDEClass> disc( pde, Global, AugGradient );
  const std::vector<int> BoundaryGroups = {1};
  IntegrandBoundaryClass fcnbound( pde, wfcn, disc, BoundaryGroups );
  IntegrandBCClass fcnbc( pde, wfcn, disc, bc, BoundaryGroups );

  // integrand functor
  BoundaryFunctorClass fcn = fcnbound.integrand( xedge, CanonicalTraceToCell(0, 1), xfldElemL, ufldElemL, qfldElemL );
  BCFunctorClass fcn2 = fcnbc.integrand( xedge, CanonicalTraceToCell(0, 1), xfldElemL, ufldElemL, qfldElemL, lgedge );

  BOOST_CHECK_EQUAL( 3, fcn.nDOFCell() );
  BOOST_CHECK_EQUAL( 2, fcn2.nDOFTrace() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandPDE1, integrandPDE2, integrandPDE3;
  Real integrandAux1, integrandAux2, integrandAux3;
  Real integrandAuy1, integrandAuy2, integrandAuy3;
  //Real integrandBC1, integrandBC2;
  BCFunctorClass::IntegrandCellType integrandL;
  ArrayQ integrandBC;
  BCFunctorClass::IntegrandCellType integrandLbc;
  ArrayQ integrandBCbc;

  sRef = 0;
  fcn( sRef, &integrandL, 1, &integrandBC, 1 );
  fcn2( sRef, &integrandLbc, 1, &integrandBCbc, 1 );

  // PDE residuals: (boundary flux) + (Lagrange)
  integrandPDE1 = (0)                    + (0);
  integrandPDE2 = (-6903./(250*sqrt(2))) + (0);
  integrandPDE3 = (0)                    + (0);

  // Auxiliary residuals: (Lagrange)
  integrandAux1 = 0;
  integrandAux2 = 28237821./(537500*sqrt(2));
  integrandAux3 = 0;
  integrandAuy1 = 0;
  integrandAuy2 = 1646151./(53750*sqrt(2));
  integrandAuy3 = 0;


  Real integrandPDET, integrandAuxT, integrandAuyT, integrandBCT;
  // combine
  integrandPDET = a0*integrandPDE1 + (a1+a0)*integrandPDE2 + (a2+a0)*integrandPDE3; // separate weight
  integrandAuxT = -a1*integrandAux1 - (a1)*integrandAux2 - (a1)*integrandAux3; // function into
  integrandAuyT = -a2*integrandAuy1 - (a2)*integrandAuy2 - (a2)*integrandAuy3; // basis functions

  integrandBCT = -2.910046511627907*-22.524632442122950; // mu*rBC
  // AD_PDE_Integrand_MMS.m

  SANS_CHECK_CLOSE( integrandPDET, integrandL.PDE+integrandLbc.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuxT, integrandLbc.Au[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuyT, integrandLbc.Au[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCT, integrandBCbc, small_tol, close_tol );

  sRef = 1;
  fcn( sRef, &integrandL, 1, &integrandBC, 1 );
  fcn2( sRef, &integrandLbc, 1, &integrandBCbc, 1 );

  // PDE residuals: (boundary flux) + (Lagrange)
  integrandPDE1 = (0)                   + (0);
  integrandPDE2 = (0)                   + (0);
  integrandPDE3 = (3819./(250*sqrt(2))) + (0);

  // Auxiliary residuals: (Lagrange)
  integrandAux1 = 0;
  integrandAux2 = 0;
  integrandAux3 = -14856483./(537500*sqrt(2));
  integrandAuy1 = 0;
  integrandAuy2 = 0;
  integrandAuy3 = -866073/(53750*sqrt(2));


  // combine
  integrandPDET = a0*integrandPDE1 + (a1+a0)*integrandPDE2 + (a2+a0)*integrandPDE3; // separate weight
  integrandAuxT = -a1*integrandAux1 - (a1)*integrandAux2 - (a1)*integrandAux3; // function into
  integrandAuyT = -a2*integrandAuy1 - (a2)*integrandAuy2 - (a2)*integrandAuy3; // basis functions

  integrandBCT = -2.410046511627907*7.801763189405700; // mu*rBC
  // AD_PDE_Integrand_MMS.m

  SANS_CHECK_CLOSE( integrandPDET, integrandL.PDE+integrandLbc.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuxT, integrandLbc.Au[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuyT, integrandLbc.Au[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCT, integrandBCbc, small_tol, close_tol );


  sRef = 0.5;
  fcn( sRef, &integrandL, 1, &integrandBC, 1 );
  fcn2( sRef, &integrandLbc, 1, &integrandBCbc, 1 );

  // PDE residuals: (boundary flux) + (Lagrange)
  integrandPDE1 = (0)                   + (0);
  integrandPDE2 = (-771./(250*sqrt(2))) + (0);
  integrandPDE3 = (-771./(250*sqrt(2))) + (0);

  // Auxiliary residuals: (Lagrange)
  integrandAux1 = 0;
  integrandAux2 = 6690669./(1075000*sqrt(2));
  integrandAux3 = 6690669./(1075000*sqrt(2));
  integrandAuy1 = 0;
  integrandAuy2 = 390039./(107500*sqrt(2));
  integrandAuy3 = 390039./(107500*sqrt(2));

  // BC residuals
  //integrandBC1 = -3*(250 + 257*sqrt(2))/500.;
  //integrandBC2 = -3*(250 + 257*sqrt(2))/500.;

  // combine
  integrandPDET = a0*integrandPDE1 + (a1+a0)*integrandPDE2 + (a2+a0)*integrandPDE3; // separate weight
  integrandAuxT = -a1*integrandAux1 - (a1)*integrandAux2 - (a1)*integrandAux3; // function into
  integrandAuyT = -a2*integrandAuy1 - (a2)*integrandAuy2 - (a2)*integrandAuy3; // basis functions

  integrandBCT = -2.660046511627907*-7.361434626358625; // mu*rBC
  // AD_PDE_Integrand_MMS.m

  SANS_CHECK_CLOSE( integrandPDET, integrandL.PDE+integrandLbc.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuxT, integrandLbc.Au[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAuyT, integrandLbc.Au[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCT, integrandBCbc, small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
