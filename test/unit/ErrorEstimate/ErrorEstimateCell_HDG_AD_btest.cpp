// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEstimateCell_HDG_AD_btest
// testing of residual functions for HDG with Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_Trace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"

#include "Field/Tuple/FieldTuple.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/HDG/IntegrandCell_HDG.h"
#include "Discretization/QuadratureOrder.h"

#include "ErrorEstimate/HDG/ErrorEstimateCell_HDG.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"

#include "unit/Discretization/HDG/IntegrandTest_HDG.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEstimateCell_HDG_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateCell_HDG_1Line_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef testspace::IntegrandInteriorTrace_HDG_None<PDEClass> IntegrandTraceClass; //dummy integrand for testing

  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_UniformGrad source(1.0/4.0, 3.0/5.0);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid: single triangle, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  XField_CellToTrace<PhysD1, TopoD1> connectivity(xfld);

  // solution: single line, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

   // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 4;

  // auxiliary variable: single line, P1 (aka Q1)
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // auxiliary variable data
  afld.DOF(0) = {2};
  afld.DOF(1) = {7};

  Field_DG_Trace<PhysD1, TopoD1, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  qIfld = 0;

  // solution: single line, P1 (aka Q1)
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

 // weight data
  wfld.DOF(0) = 3;
  wfld.DOF(1) = 4;
  wfld.DOF(2) = 5;

  // auxiliary variable: single line, P1 (aka Q1)
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> bfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  // auxiliary variable data
  bfld.DOF(0) = {-5};
  bfld.DOF(1) = {3};
  bfld.DOF(2) = {2};

  Field_DG_Trace<PhysD1, TopoD1, ArrayQ> wIfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  wIfld = 0;

  // estimate fields
  Field_DG_Cell<PhysD1, TopoD1, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);
  efld = 0;

  BOOST_CHECK_EQUAL( 1, efld.nElem() );
  BOOST_CHECK_EQUAL( 1, efld.nDOF() );

  // lifting operator: single line, P0 (aka Q0)
  Field_DG_Cell<PhysD1, TopoD1, Real> eAfld(xfld, 0, BasisFunctionCategory_Legendre);
  eAfld = 0;

  BOOST_CHECK_EQUAL( 1, eAfld.nElem() );
  BOOST_CHECK_EQUAL( 1, eAfld.nDOF() );
  BOOST_CHECK( eAfld.D == pde.D);

  Field_DG_Trace<PhysD1, TopoD1, Real> eIfld(xfld, 0, BasisFunctionCategory_Legendre);
  eIfld = 0;

  // HDG discretization (not used)
  DiscretizationClass disc( pde );

  // quadrature rule
  QuadratureOrder quadOrder(xfld, 3);

  // integrand
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandTraceClass fcnTrace( {} );

  IntegrateCellGroups<TopoD1>::integrate( ErrorEstimateCell_HDG( fcnCell, fcnTrace,
                                                                 connectivity, xfld,
                                                                 qfld, afld, qIfld, wfld, bfld, wIfld,
                                                                 efld, eAfld, eIfld,
                                                                 quadOrder.interiorTraceOrders.data(),
                                                                 quadOrder.interiorTraceOrders.size() ),
                                          xfld, (qfld, afld, wfld, bfld, efld, eAfld),
                                          quadOrder.cellOrders.data(), quadOrder.cellOrders.size());

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-10;

  // Taken from Integrand FW test
  Real rsdPDETrue, rsdAUXTrue;
  //PDE residual: (advective) + (viscous)
  rsdPDETrue = ( 33/4. ) + ( -154979/6000. ) + ( 691/30. );   // Weight function

  //Auxilliary Variable residual:
  rsdAUXTrue =  ( 125/6. );   // Basis function

  SANS_CHECK_CLOSE( rsdPDETrue, efld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXTrue, eAfld.DOF(0), small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateCell_HDG_1Triangle_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                  AdvectiveFlux2D_Uniform,
                                  ViscousFlux2D_Uniform,
                                  Source2D_UniformGrad> PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef testspace::IntegrandInteriorTrace_HDG_None<PDEClass> IntegrandTraceClass; //dummy integrand for testing

  int nDOF = 3;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_UniformGrad source(0.25, 0.6, -1.5);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  XField_CellToTrace<PhysD2, TopoD2> connectivity(xfld);

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

   // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  BOOST_CHECK_EQUAL( nDOF, qfld.nDOF() );

  // lifting operator: single triangle, P1 (aka Q1)
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  afld.DOF(0) = { 2, -3};
  afld.DOF(1) = { 7,  8};
  afld.DOF(2) = {-1, -5};

  BOOST_CHECK_EQUAL( 3, afld.nDOF() );
  BOOST_CHECK( afld.D == pde.D);

  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  qIfld = 0.5;

  // weight: single triangle, P2 (aka Q1)
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  // triangle solution
  wfld.DOF(0) = -2;
  wfld.DOF(1) =  4;
  wfld.DOF(2) =  3;
  wfld.DOF(3) =  2;
  wfld.DOF(4) =  4;
  wfld.DOF(5) = -1;

  // lifting operator: single triangle, P1 (aka Q1)
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> bfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  bfld.DOF(0) = { 1,  5};
  bfld.DOF(1) = { 3,  6};
  bfld.DOF(2) = {-1,  7};
  bfld.DOF(3) = { 3,  1};
  bfld.DOF(4) = { 2,  2};
  bfld.DOF(5) = { 4,  3};

  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> wIfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);
  wIfld = 0;

  // estimate fields
  Field_DG_Cell<PhysD2, TopoD2, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);
  efld = 0;

  BOOST_CHECK_EQUAL( 1, efld.nElem() );
  BOOST_CHECK_EQUAL( 1, efld.nDOF() );

  // lifting operator: single line, P2 (aka Q2)
  Field_DG_Cell<PhysD2, TopoD2, Real> eAfld(xfld, 0, BasisFunctionCategory_Legendre);
  eAfld = 0;

  BOOST_CHECK_EQUAL( 1, eAfld.nElem() );
  BOOST_CHECK_EQUAL( 1, eAfld.nDOF() );
  BOOST_CHECK( eAfld.D == pde.D);

  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> eIfld(xfld, 0, BasisFunctionCategory_Legendre);
  eIfld = 0;

  // HDG discretization (not used)
  DiscretizationHDG<PDEClass> disc( pde );

  QuadratureOrder quadOrder(xfld, 3);

  // integrand
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandTraceClass fcnTrace( {} );

  IntegrateCellGroups<TopoD2>::integrate( ErrorEstimateCell_HDG( fcnCell, fcnTrace,
                                                                 connectivity, xfld,
                                                                 qfld, afld, qIfld, wfld, bfld, wIfld,
                                                                 efld, eAfld, eIfld,
                                                                 quadOrder.interiorTraceOrders.data(),
                                                                 quadOrder.interiorTraceOrders.size() ),
                                          xfld, (qfld, afld, wfld, bfld, efld, eAfld),
                                          quadOrder.cellOrders.data(), quadOrder.cellOrders.size());

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real rsdPDETrue, rsdAUXTrue;

  //PDE residual: (advection) + (diffusion) + (source)
  rsdPDETrue = (-34/5.) + (203167/6000.) + (571/150.);   // Weight function

  //AUX residual:
  rsdAUXTrue = (91/12.);   // Weight function

  SANS_CHECK_CLOSE( rsdPDETrue, efld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXTrue, eAfld.DOF(0), small_tol, close_tol );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
