// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ResidualInteriorTrace_Triangle_AD_btest
// testing of interior trace residual functions with Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/Tuple/FieldTuple.h"

#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"

#include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin_StrongForm.h"
#include "ErrorEstimate/Galerkin/ErrorEstimateInteriorTrace_Galerkin.h"
#include "ErrorEstimate/Galerkin/ErrorEstimateInteriorTrace_PrimalStrongForm_Galerkin.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEstimateInteriorTrace_PrimalStrongForm_Galerkin_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateInteriorTrace_PrimalStrongForm_Galerkin_1D )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef IntegrandInteriorTrace_Galerkin_StrongForm<PDEClass> IntegrandClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // grid: P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK( qfld.nDOF() == 4 );

  // line solution data (left)
  qfld.DOF(0) =  5;
  qfld.DOF(1) = -4;

  // line solution data (right)
  qfld.DOF(2) = -4;
  qfld.DOF(3) =  2;

  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> wfld(xfld, 0, BasisFunctionCategory_Legendre);

  BOOST_CHECK( wfld.nDOF() == 2 );

  // line solution (left)
  wfld.DOF(0) =  1;

  // line solution (right)
  wfld.DOF(1) =  1;

  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> efld(xfld, 0, BasisFunctionCategory_Legendre);

  BOOST_CHECK( efld.nDOF() == 2 );

  // error estimate field
  efld = 0;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // quadrature rule: quadratic (basis & flux both linear)
  int quadratureOrder = 0;

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  // general interface
  Real rsdTrue0=0, rsdTrue1=0;

  IntegrateInteriorTraceGroups<TopoD1>::integrate( ErrorEstimateInteriorTrace_Galerkin(fcnint),
                                                   xfld, (qfld,wfld,efld) , &quadratureOrder, 1);

  rsdTrue0 = pow(efld.DOF(0),2);
  rsdTrue1 = pow(efld.DOF(1),2);

  efld = 0;

  IntegrateInteriorTraceGroups<TopoD1>::integrate( ErrorEstimateInteriorTrace_PrimalStrongForm_Galerkin(fcnint),
                                                   xfld, (qfld, efld) , &quadratureOrder, 1);

  SANS_CHECK_CLOSE( rsdTrue0, efld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue1, efld.DOF(1), small_tol, close_tol );

}
#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateInteriorTrace_PrimalStrongForm_Galerkin_2D_2Triangle_X1Q1_1Group )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef IntegrandInteriorTrace_Galerkin_StrongForm<PDEClass> IntegrandClass;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.321;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // grid: P1 (aka X1)
  XField2D_2Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK( qfld.nDOF() == 6 );

  // triangle solution (left)
  qfld.DOF(0) =  1;
  qfld.DOF(1) =  3;
  qfld.DOF(2) =  4;

  // triangle solution (right)
  qfld.DOF(3) = -4;
  qfld.DOF(4) =  4;
  qfld.DOF(5) =  3;

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, 0, BasisFunctionCategory_Legendre);

  // triangle solution (left)
  wfld.DOF(0) = 1;

  // triangle solution (right)
  wfld.DOF(1) = 1;

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> efld(xfld, 0, BasisFunctionCategory_Legendre);

  BOOST_CHECK( efld.nDOF() == 2 );

  // error estimate field
  efld.DOF(0) = 0;
  efld.DOF(1) = 0;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // quadrature rule: quadratic (basis & flux both linear)
  int quadratureOrder = 3;

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  // general interface
  Real rsdTrue0 = 0, rsdTrue1 = 0;

  IntegrateInteriorTraceGroups<TopoD2>::integrate( ErrorEstimateInteriorTrace_Galerkin(fcnint),
                                                   xfld, (qfld,wfld,efld) , &quadratureOrder, 1);

  rsdTrue0 = pow(efld.DOF(0),2);
  rsdTrue1 = pow(efld.DOF(1),2);

  efld = 0;

  IntegrateInteriorTraceGroups<TopoD2>::integrate( ErrorEstimateInteriorTrace_PrimalStrongForm_Galerkin(fcnint),
                                                   xfld, (qfld, efld) , &quadratureOrder, 1);

  SANS_CHECK_CLOSE( rsdTrue0, efld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue1, efld.DOF(1), small_tol, close_tol );

}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
