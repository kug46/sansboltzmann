// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEstimateCell_StrongForm_Galerkin_AD_btest
// testing of cell residual functions for Galerkin with Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
//#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
//#include "pde/NDConvert/PDENDConvertSpace3D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/Tuple/FieldTuple.h"

#include "Discretization/IntegrateCellGroups.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
//#include "unit/UnitGrids/XField2D_1Quad_X1_1Group.h"
//#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
//#include "unit/UnitGrids/XField3D_1Hex_X1_1Group.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_StrongForm.h"
#include "ErrorEstimate/Galerkin/ErrorEstimateCell_Galerkin.h"
#include "ErrorEstimate/Galerkin/ErrorEstimateCell_PrimalStrongForm_Galerkin.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEstimateCell_PrimalStrongForm_Galerkin_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateCell_PrimalStrongForm_Galerkin_1D )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin_StrongForm<PDEClass> IntegrandClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single triangle, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single line, P1 (aka Q1)
  int qorder = 2;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( 1, qfld.nElem() );
  BOOST_REQUIRE_EQUAL( 3, qfld.nDOF() );

  // solution data
  qfld.DOF(0) =  5;
  qfld.DOF(1) = -4;
  qfld.DOF(2) =  3;

  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> wfld(xfld, 0, BasisFunctionCategory_Legendre);

  BOOST_REQUIRE_EQUAL( 1, wfld.nElem() );
  BOOST_REQUIRE_EQUAL( 1, wfld.nDOF() );

  // weight data
  wfld.DOF(0) =  1;

  Field_DG_Cell<PhysD1, TopoD1, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);

  BOOST_REQUIRE_EQUAL( 1, efld.nElem() );
  BOOST_REQUIRE_EQUAL( 1, efld.nDOF() );

  // solution data
  efld = 0;

  // quadrature rule (p2*p3)
  int quadratureOrder = 5;

  Real trueEstimate;
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  // integrand
  const StabilizationMatrix stab(StabilizationType::Adjoint, TauType::Glasby, qorder+1);
  IntegrandClass fcnint( pde , {0}, stab );

  IntegrateCellGroups<TopoD1>::integrate( ErrorEstimateCell_Galerkin(fcnint), xfld, (qfld, wfld, efld), &quadratureOrder, 1 );

  trueEstimate = pow(efld.DOF(0),2); // strong form should be the equivalent to the square of the unity weighted AD estimate

  efld = 0;

  IntegrateCellGroups<TopoD1>::integrate( ErrorEstimateCell_PrimalStrongForm_Galerkin(fcnint), xfld, (qfld, efld), &quadratureOrder, 1 );

  SANS_CHECK_CLOSE( trueEstimate, efld.DOF(0), small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateCell_PrimalStrongForm_Galerkin_2D_CG_1Triangle_P2P3 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin_StrongForm<PDEClass> IntegrandClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.321;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P2 (aka Q2)
  int qorder = 2;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle solution
  qfld.DOF(0) =  1;
  qfld.DOF(1) =  3;
  qfld.DOF(2) =  4;
  qfld.DOF(3) =  2;
  qfld.DOF(4) =  3;
  qfld.DOF(5) =  4;

  BOOST_CHECK_EQUAL( 6, qfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld.nElem() );

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, 0, BasisFunctionCategory_Legendre);

  // triangle solution
  wfld.DOF(0) = 1;

  BOOST_CHECK_EQUAL( 1, wfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, wfld.nElem() );

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> efld(xfld, 0, BasisFunctionCategory_Legendre);

  // solution data
  efld.DOF(0) = 0;

  BOOST_CHECK_EQUAL( 1, efld.nElem() );
  BOOST_CHECK_EQUAL( 1, efld.nDOF() );

  // quadrature rule
  int quadratureOrder = 5;

  Real trueEstimate;
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  // integrand
  const StabilizationMatrix stab(StabilizationType::Adjoint, TauType::Glasby, qorder+1);
  IntegrandClass fcnint( pde , {0}, stab );

  IntegrateCellGroups<TopoD2>::integrate( ErrorEstimateCell_Galerkin(fcnint), xfld, (qfld, wfld, efld), &quadratureOrder, 1 );

  trueEstimate = pow(efld.DOF(0),2); // strong form should be the equivalent to the square of the unity weighted AD estimate

  efld = 0;

  IntegrateCellGroups<TopoD2>::integrate( ErrorEstimateCell_PrimalStrongForm_Galerkin(fcnint), xfld, (qfld, efld), &quadratureOrder, 1 );

  SANS_CHECK_CLOSE( trueEstimate, efld.DOF(0), small_tol, close_tol );

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
