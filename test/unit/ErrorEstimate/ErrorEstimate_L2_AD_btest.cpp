// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEstimate_DGBR2_AD_btest
// testing of Error Estimate umbrella function for DG BR2 with Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/AnalyticFunction/ScalarFunction4D.h"

#include "pde/NDConvert/FunctionNDConvertSpace1D.h"
#include "pde/NDConvert/FunctionNDConvertSpace2D.h"
#include "pde/NDConvert/FunctionNDConvertSpace3D.h"
#include "pde/NDConvert/FunctionNDConvertSpace4D.h"
// #include "pde/NDConvert/FunctionNDConvertSpaceTime3D.h"
// #include "pde/NDConvert/FunctionNDConvertSpaceTime1D.h"
// #include "pde/NDConvert/FunctionNDConvertSpaceTime2D.h"
// #include "pde/NDConvert/FunctionNDConvertSpaceTime3D.h"
// #include "pde/NDConvert/FunctionNDConvertSpaceTime4D.h"

// for the p0 eflds
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldSpacetime_DG_Cell.h"

#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldSpacetime_CG_Cell.h"

#include "Field/Tuple/FieldTuple.h"

#include "Discretization/IntegrateCellGroups.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
#include "unit/UnitGrids/XField4D_1Ptope_X1_1Group.h"

#include "ErrorEstimate/ErrorEstimate_L2.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include "Field/Local/XField_LocalPatch.h"
#include "Field/Local/Field_Local.h"
#include "Field/Field_NodalView.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEstimate_L2_AD_test_suite )

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimate_L2_Line )
{
  typedef Real ArrayQ;

  typedef ScalarFunction1D_Quad SolutionExact;
  typedef FunctionNDConvertSpace<PhysD1, Real> Function1DND;
  typedef ErrorEstimate_L2< Function1DND, PhysD1, TopoD1, ArrayQ > ErrorEstimateClass;

  // grid
  XField1D_1Line_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 2 );

  // triangle solution data (left)
  qfld.DOF(0) = 6;
  qfld.DOF(1) = 4;

  const QuadratureOrder quadOrder(xfld, 5);
  const std::vector<int> cellGroups{0};

  const Real small_tol = 1e-12, close_tol = small_tol;

  SolutionExact solnExact;
  Function1DND fcnND(solnExact);
  {
    const int estimateOrder = 0;
    ErrorEstimateClass errorEstimate( xfld, qfld, fcnND, quadOrder, estimateOrder, cellGroups );

    Real estimate=0.0;
    errorEstimate.getErrorEstimate(estimate);

    const Field<PhysD1,TopoD1,ArrayQ>& efld = errorEstimate.getEField();

    const Real rsdTrue[1] = {248./15};
    SANS_CHECK_CLOSE( efld.DOF(0), rsdTrue[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( estimate,    rsdTrue[0], small_tol, close_tol);
  }

  {
    const int estimateOrder = 1;
    ErrorEstimateClass errorEstimate( xfld, qfld, fcnND, quadOrder, estimateOrder, cellGroups );

    Real estimate=0.0;
    errorEstimate.getErrorEstimate(estimate);

    const Field<PhysD1,TopoD1,ArrayQ>& efld = errorEstimate.getEField();

    const Real rsdTrue[2] = {146./15, 34./5};
    for (int n = 0; n < 2; n++)
      SANS_CHECK_CLOSE( efld.DOF(n), rsdTrue[n], small_tol, close_tol);
    SANS_CHECK_CLOSE( estimate, rsdTrue[0]+rsdTrue[1], small_tol, close_tol);
  }
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimate_L2_Triangle )
{
  typedef Real ArrayQ;

  typedef ScalarFunction2D_Cubic SolutionExact;
  typedef FunctionNDConvertSpace<PhysD2, Real> Function2DND;
  typedef ErrorEstimate_L2< Function2DND, PhysD2, TopoD2, ArrayQ > ErrorEstimateClass;

  // grid
  XField2D_1Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 3 );

  // triangle solution data (left)
  qfld.DOF(0) = 5;
  qfld.DOF(1) = 2;
  qfld.DOF(2) = 7;

  const QuadratureOrder quadOrder(xfld, 6);

  const Real small_tol = 1e-12, close_tol = small_tol;

  SolutionExact solnExact;
  Function2DND fcnND(solnExact);
  {
    const int estimateOrder = 0;
    const std::vector<int> cellGroups = {0};
    ErrorEstimateClass errorEstimate( xfld, qfld, fcnND, quadOrder, estimateOrder, cellGroups );

    Real estimate=0.0;
    errorEstimate.getErrorEstimate(estimate);

    const Field<PhysD2,TopoD2,ArrayQ>& efld = errorEstimate.getEField();

    const Real rsdTrue[1] = {3179./336};
    SANS_CHECK_CLOSE( efld.DOF(0), rsdTrue[0], small_tol, close_tol);
    SANS_CHECK_CLOSE( estimate,    rsdTrue[0], small_tol, close_tol);
  }

  {
    const int estimateOrder = 1;
    const std::vector<int> cellGroups = linspace(0,xfld.nCellGroups()-1);
    ErrorEstimateClass errorEstimate( xfld, qfld, fcnND, quadOrder, estimateOrder, cellGroups );

    Real estimate=0.0;
    errorEstimate.getErrorEstimate(estimate);

    const Field<PhysD2,TopoD2,ArrayQ>& efld = errorEstimate.getEField();

    const Real rsdTrue[3] = {5389./1680, 3733./1680, 6773./1680};
    for (int n = 0; n < 3; n++)
      SANS_CHECK_CLOSE( efld.DOF(n), rsdTrue[n], small_tol, close_tol);
    SANS_CHECK_CLOSE( estimate, rsdTrue[0]+rsdTrue[1]+rsdTrue[2], small_tol, close_tol);
  }
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimate_L2_Triangle_LocalPatch )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  typedef Real ArrayQ;

  typedef ScalarFunction2D_Cubic SolutionExact;
  typedef FunctionNDConvertSpace<PhysD2, Real> Function2DND;
  typedef ErrorEstimate_L2< Function2DND, PhysD2, TopoD2, ArrayQ > ErrorEstimateClass;

  // grid
  XField2D_1Triangle_X1_1Group xfld_linear;

  // higher order grid
  int xorder = 2;
  XField<PhysD2,TopoD2> xfld_global(xfld_linear, xorder, BasisFunctionCategory_Lagrange);

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld_global);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld_global,{0});

  // edge focused on for patch
  std::array<int,2> edge{{4,5}};

  {
    // make an unsplit grid
    XField_LocalPatchConstructor<PhysD2,Triangle> xfld_construct(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);
    XField_LocalPatch<PhysD2,Triangle> xfld_local(xfld_construct);

    // solution: P1 (aka Q1)
    int qorder = 1;
    Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld_local, qorder, BasisFunctionCategory_Hierarchical);

    BOOST_REQUIRE_EQUAL( qfld.nDOF(), 3 );

    // triangle solution data (left)
    qfld.DOF(0) = 5;
    qfld.DOF(1) = 2;
    qfld.DOF(2) = 7;
    // qfld.DOF(3) = (qfld.DOF(1) + qfld.DOF(2))/2; // half way between the two dofs

    const QuadratureOrder quadOrder(xfld_local, 6);

    const Real small_tol = 1e-12, close_tol = small_tol;

    SolutionExact solnExact;
    Function2DND fcnND(solnExact);

    {
      const int estimateOrder = 0;
      const std::vector<int> cellGroups{0};
      ErrorEstimateClass errorEstimate( xfld_local, qfld, fcnND, quadOrder, estimateOrder, cellGroups );

      Real estimate=0.0;
      errorEstimate.getErrorEstimate(estimate);

      const Field<PhysD2,TopoD2,ArrayQ>& efld = errorEstimate.getEField();

      const Real rsdTrue[1] = {3179./336};
      SANS_CHECK_CLOSE( efld.DOF(0), rsdTrue[0], small_tol, close_tol);
      SANS_CHECK_CLOSE( estimate,    rsdTrue[0], small_tol, close_tol);
    }

    {
      const int estimateOrder = 1;
      const std::vector<int> cellGroups = linspace(0,xfld_local.nCellGroups()-1);
      ErrorEstimateClass errorEstimate( xfld_local, qfld, fcnND, quadOrder, estimateOrder, cellGroups );

      Real estimate=0.0;
      errorEstimate.getErrorEstimate(estimate);

      const Field<PhysD2,TopoD2,ArrayQ>& efld = errorEstimate.getEField();

      const Real rsdTrue[3] = {5389./1680, 3733./1680, 6773./1680};
      for (int n = 0; n < 3; n++)
        SANS_CHECK_CLOSE( efld.DOF(n), rsdTrue[n], small_tol, close_tol);
      SANS_CHECK_CLOSE( estimate, rsdTrue[0]+rsdTrue[1]+rsdTrue[2], small_tol, close_tol);

      std::vector<Real> localEstimate{};
      errorEstimate.getLocalSolveErrorIndicator(localEstimate);

      BOOST_CHECK_EQUAL( localEstimate.size(), 3 );

      for (int n = 0; n < 3; n++)
        SANS_CHECK_CLOSE( localEstimate[n], rsdTrue[n], small_tol, close_tol);
    }
  }

  {
    // make a split grid
    XField_LocalPatch<PhysD2,Triangle> xfld_local( comm_local, connectivity, edge, SpaceType::Continuous, &nodalview );

    BOOST_CHECK_EQUAL( xfld_local.nDOF(), 9 );

    // solution: P1 (aka Q1)
    int qorder = 1;
    Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld_local, qorder, BasisFunctionCategory_Hierarchical);

    BOOST_REQUIRE_EQUAL( qfld.nDOF(), 4 );

    // triangle solution data (left)
    qfld.DOF(0) = 5;
    qfld.DOF(1) = 2;
    qfld.DOF(2) = 7;
    // get the new linear DOF
    std::vector<int> newLinearNodeDOFs = xfld_local.getNewLinearNodeDOFs();
    std::vector<int> localLinearAttachedNodes = xfld_local.getLocalLinearCommonNodes();
    // half way between the two dofs
    qfld.DOF(newLinearNodeDOFs[0]) = (qfld.DOF(localLinearAttachedNodes[0]) + qfld.DOF(localLinearAttachedNodes[1]))/2;

    BOOST_CHECK_EQUAL( localLinearAttachedNodes[0], 0 );
    BOOST_CHECK_EQUAL( localLinearAttachedNodes[1], 1 );

    const QuadratureOrder quadOrder(xfld_local, 6);

    const Real small_tol = 1e-12, close_tol = small_tol;

    SolutionExact solnExact;
    Function2DND fcnND(solnExact);
    {
      const int estimateOrder = 0;
      const std::vector<int> cellGroups{0};
      ErrorEstimateClass errorEstimate( xfld_local, qfld, fcnND, quadOrder, estimateOrder, cellGroups );

      Real estimate=0.0;
      errorEstimate.getErrorEstimate(estimate);

      const Field<PhysD2,TopoD2,ArrayQ>& efld = errorEstimate.getEField();

      const Real rsdTrue[1] = {3179./336};
      SANS_CHECK_CLOSE( efld.DOF(0) + efld.DOF(1), rsdTrue[0], small_tol, close_tol);
      SANS_CHECK_CLOSE( estimate,    rsdTrue[0], small_tol, close_tol);

      std::vector<Real> localEstimate{};
      errorEstimate.getLocalSolveErrorIndicator(localEstimate);

      BOOST_CHECK_EQUAL( localEstimate.size(), 1 );
      SANS_CHECK_CLOSE( localEstimate[0], rsdTrue[0], small_tol, close_tol );
    }

    {
      const int estimateOrder = 1;
      const std::vector<int> cellGroups = linspace(0,xfld_local.nCellGroups()-1);
      ErrorEstimateClass errorEstimate( xfld_local, qfld, fcnND, quadOrder, estimateOrder, cellGroups );

      Real estimate=0.0;
      errorEstimate.getErrorEstimate(estimate);

      const Field<PhysD2,TopoD2,ArrayQ>& efld = errorEstimate.getEField();

      const Real rsdTrue[3] = {5389./1680, 3733./1680, 6773./1680};
      // first and second node make up the edge, and receive half of the new nodes estimate
      SANS_CHECK_CLOSE( efld.DOF(0) + efld.DOF(3)/2, rsdTrue[0], small_tol, close_tol);
      SANS_CHECK_CLOSE( efld.DOF(1) + efld.DOF(3)/2, rsdTrue[1], small_tol, close_tol);
      SANS_CHECK_CLOSE( efld.DOF(2), rsdTrue[2], small_tol, close_tol);
      SANS_CHECK_CLOSE( estimate, rsdTrue[0]+rsdTrue[1]+rsdTrue[2], small_tol, close_tol);

      std::vector<Real> localEstimate{};
      errorEstimate.getLocalSolveErrorIndicator(localEstimate);

      BOOST_CHECK_EQUAL( localEstimate.size(), 3 );

      for (int n = 0; n < 3; n++)
        SANS_CHECK_CLOSE( localEstimate[n], rsdTrue[n], small_tol, close_tol);
    }
  }
}
#endif

#if 1
BOOST_AUTO_TEST_CASE(ErrorEstimate_L2_Pentatope)
{
  typedef Real ArrayQ;

  typedef ScalarFunction4D_Monomial SolutionExact;
  // typedef ScalarFunction4D_SineSineSineSineUnsteady SolutionExact;
  typedef FunctionNDConvertSpace<PhysD4, Real> Function4DND;
  typedef ErrorEstimate_L2<Function4DND, PhysD4, TopoD4, ArrayQ> ErrorEstimateClass;

  // grid
  XField4D_1Ptope_X1_1Group xfld;

  // solution: P1
  int qorder= 1;
  Field_DG_Cell<PhysD4, TopoD4, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Lagrange);

  BOOST_REQUIRE_EQUAL(qfld.nDOF(), 5);

  // pentatope solution data
  qfld.DOF(0)= 4;
  qfld.DOF(1)= 1;
  qfld.DOF(2)= 3;
  qfld.DOF(3)= 2;
  qfld.DOF(4)= 5;

  const QuadratureOrder quadOrder(xfld, 8);

  const Real small_tol= 1e-8;
  const Real close_tol= 1e-8;

  const Real constant= 0.0;
  const std::vector<Real> coeffs= {1., 2., 3., 4.};
  const std::vector<int> exponents= {1, 2, 1, 2};
  SolutionExact solnExact(constant, coeffs, exponents);
  Function4DND fcnND(solnExact);

  {
    const int estimateOrder= 0;
    const std::vector<int> cellGroups = {0};
    ErrorEstimateClass errorEstimate(xfld, qfld, fcnND, quadOrder, estimateOrder, cellGroups);

    Real estimate= 0.0;
    errorEstimate.getErrorEstimate(estimate);

    const Field<PhysD4, TopoD4, ArrayQ> &efld= errorEstimate.getEField();

    const Real rsdTrue[1]= {0.1587301587301589};

    BOOST_CHECK_EQUAL(efld.nDOF(), 1);
    SANS_CHECK_CLOSE(estimate, rsdTrue[0], small_tol, close_tol);
    SANS_CHECK_CLOSE(efld.DOF(0), rsdTrue[0], small_tol, close_tol);
  }

  {
    const int estimateOrder= 1;
    const std::vector<int> cellGroups = linspace(0,xfld.nCellGroups()-1);
    ErrorEstimateClass errorEstimate(xfld, qfld, fcnND, quadOrder, estimateOrder, cellGroups);

    Real estimate= 0.0;
    errorEstimate.getErrorEstimate(estimate);

    const Field<PhysD4, TopoD4, ArrayQ> &efld= errorEstimate.getEField();

    Real estimateTrue= 0.0;
    const std::vector<Real> rsdTrue= {0.0453483245149911,
                                      0.0239197530864197,
                                      0.0322751322751322,
                                      0.0205467372134039,
                                      0.03664021164021160};

    BOOST_CHECK_EQUAL(efld.nDOF(), 5);

    for (int i= 0; i < efld.nDOF(); i++)
    {
      estimateTrue += rsdTrue[i];
      SANS_CHECK_CLOSE(efld.DOF(i), rsdTrue[i], small_tol, close_tol);
    }

    SANS_CHECK_CLOSE(estimate, estimateTrue, small_tol, close_tol);
  }
}
#endif

#if 1
BOOST_AUTO_TEST_CASE(ErrorEstimate_L2_Pentatope_LocalPatch)
{
  mpi::communicator world;
  mpi::communicator comm_local= world.split(world.rank());

  typedef Real ArrayQ;

  typedef ScalarFunction4D_Monomial SolutionExact;
  // typedef ScalarFunction4D_SineSineSineSineUnsteady SolutionExact;
  typedef FunctionNDConvertSpace<PhysD4, Real> Function4DND;
  typedef ErrorEstimate_L2<Function4DND, PhysD4, TopoD4, ArrayQ> ErrorEstimateClass;

  // grid
  XField4D_1Ptope_X1_1Group xfld_linear;

  // higher order grid
  // int xorder= 1; // DEBUG!!! switch when bug worked out to stretch out higher order grids
  int xorder= 2;
  XField<PhysD4, TopoD4> xfld_global(xfld_linear, xorder, BasisFunctionCategory_Lagrange);

  // build cell to trace connectivity structure
  XField_CellToTrace<PhysD4, TopoD4> connectivity(xfld_global);

  // build node to cell connectivity structure
  Field_NodalView nodalview(xfld_global, {0});

  // edge focused on for patch
  // std::array<int, 2> edge{{0, 2}}; // DEBUG!!! switch when bug worked out to stretch out higher order grids
  std::array<int, 2> edge{{10, 12}};

  {
    // global solution: p1
    int qorder= 1;
    Field_CG_Cell<PhysD4, TopoD4, ArrayQ> qfld_global(xfld_global, qorder, BasisFunctionCategory_Lagrange);

    // pentatope solution data
    qfld_global.DOF(0)= 4;
    qfld_global.DOF(1)= 1;
    qfld_global.DOF(2)= 3;
    qfld_global.DOF(3)= 2;
    qfld_global.DOF(4)= 5;

    // make an unsplit grid
    XField_LocalPatchConstructor<PhysD4, Pentatope> xfld_construct(comm_local, connectivity, edge, SpaceType::Continuous, &nodalview);
    XField_LocalPatch<PhysD4, Pentatope> xfld_local(xfld_construct);

    // local field
    Field_Local<Field_CG_Cell<PhysD4, TopoD4, ArrayQ>> qfld_local(xfld_local, qfld_global, qorder, BasisFunctionCategory_Lagrange);

    BOOST_REQUIRE_EQUAL(qfld_local.nDOF(), 5);

    const QuadratureOrder quadOrder(xfld_local, 8);

    const Real small_tol= 1e-8;
    const Real close_tol= 1e-8;

    const Real constant= 0.0;
    const std::vector<Real> coeffs= {1., 2., 3., 4.};
    const std::vector<int> exponents= {1, 2, 1, 2};
    SolutionExact solnExact(constant, coeffs, exponents);
    Function4DND fcnND(solnExact);

    {
      const int estimateOrder= 0;
      const std::vector<int> cellGroups{0,1};
      ErrorEstimateClass errorEstimate(xfld_local, qfld_local, fcnND, quadOrder, estimateOrder, cellGroups);

      Real estimate= 0.0;
      errorEstimate.getErrorEstimate(estimate);

      const Field<PhysD4, TopoD4, ArrayQ> &efld= errorEstimate.getEField();

      const Real rsdTrue[1]= {0.1587301587301589};

      BOOST_CHECK_EQUAL(efld.nDOF(), 1);
      SANS_CHECK_CLOSE(estimate, rsdTrue[0], small_tol, close_tol);
      SANS_CHECK_CLOSE(efld.DOF(0), rsdTrue[0], small_tol, close_tol);
    }

    {
      const int estimateOrder= 1;
      const std::vector<int> cellGroups = linspace(0,xfld_local.nCellGroups()-1);
      ErrorEstimateClass errorEstimate(xfld_local, qfld_local, fcnND, quadOrder, estimateOrder, cellGroups);

      Real estimate= 0.0;
      errorEstimate.getErrorEstimate(estimate);

      const Field<PhysD4, TopoD4, ArrayQ> &efld= errorEstimate.getEField();

      Real estimateTrue= 0.0;
      const std::vector<Real> rsdTrue= {0.0453483245149911,
                                        0.0322751322751322, // local patch on this edge reverses DOFs
                                        0.0239197530864197, // local patch on this edge reverses DOFs
                                        0.0205467372134039,
                                        0.03664021164021160};

      BOOST_CHECK_EQUAL(efld.nDOF(), 5);

      for (int i= 0; i < efld.nDOF(); i++)
      {
        estimateTrue += rsdTrue[i];
        SANS_CHECK_CLOSE(efld.DOF(i), rsdTrue[i], small_tol, close_tol);
      }

      SANS_CHECK_CLOSE(estimate, estimateTrue, small_tol, close_tol);
    }
  }

  {
    // global solution: p1
    int qorder= 1;
    Field_CG_Cell<PhysD4, TopoD4, ArrayQ> qfld_global(xfld_global, qorder, BasisFunctionCategory_Lagrange);

    // pentatope solution data
    qfld_global.DOF(0)= 4;
    qfld_global.DOF(1)= 1;
    qfld_global.DOF(2)= 3;
    qfld_global.DOF(3)= 2;
    qfld_global.DOF(4)= 5;

    // make a split grid
    XField_LocalPatch<PhysD4, Pentatope> xfld_local(comm_local, connectivity, edge, SpaceType::Continuous, &nodalview);

    // local solution: p1
    Field_Local<Field_CG_Cell<PhysD4, TopoD4, ArrayQ>> qfld_local(xfld_local, qfld_global, qorder, BasisFunctionCategory_Lagrange);

    BOOST_REQUIRE_EQUAL(qfld_local.nDOF(), 6);

    const QuadratureOrder quadOrder(xfld_local, 8);

    const Real small_tol= 1e-8;
    const Real close_tol= 1e-8;

    const Real constant= 0.0;
    const std::vector<Real> coeffs= {1., 2., 3., 4.};
    const std::vector<int> exponents= {1, 2, 1, 2};
    SolutionExact solnExact(constant, coeffs, exponents);
    Function4DND fcnND(solnExact);

    {
      const int estimateOrder= 0;
      const std::vector<int> cellGroups{0};
      ErrorEstimateClass errorEstimate(xfld_local, qfld_local, fcnND, quadOrder, estimateOrder, cellGroups);

      Real estimate= 0.0;
      errorEstimate.getErrorEstimate(estimate);

      const Field<PhysD4, TopoD4, ArrayQ> &efld= errorEstimate.getEField();

      const Real rsdTrue[1]= {0.1587301587301589};

      BOOST_CHECK_EQUAL(efld.nDOF(), 2);
      SANS_CHECK_CLOSE(estimate, rsdTrue[0], small_tol, close_tol);
      SANS_CHECK_CLOSE(efld.DOF(0) + efld.DOF(1), rsdTrue[0], small_tol, close_tol);

      std::vector<Real> localEstimate{};
      errorEstimate.getLocalSolveErrorIndicator(localEstimate);

      BOOST_CHECK_EQUAL(localEstimate.size(), 1);
      SANS_CHECK_CLOSE(localEstimate[0], rsdTrue[0], small_tol, close_tol);
    }

    {
      const int estimateOrder= 1;
      const std::vector<int> cellGroups = linspace(0,xfld_local.nCellGroups()-1);
      ErrorEstimateClass errorEstimate(xfld_local, qfld_local, fcnND, quadOrder, estimateOrder, cellGroups);

      Real estimate= 0.0;
      errorEstimate.getErrorEstimate(estimate);

      const Field<PhysD4, TopoD4, ArrayQ> &efld= errorEstimate.getEField();

      Real estimateTrue= 0.0;
      const std::vector<Real> rsdTrue= {0.0453483245149911,
                                        0.0322751322751322, // local patch on this edge reverses DOFs
                                        0.0239197530864197, // local patch on this edge reverses DOFs
                                        0.0205467372134039,
                                        0.03664021164021160};

      BOOST_CHECK_EQUAL(efld.nDOF(), 6);

      for (int i= 0; i < 5; i++)
        estimateTrue += rsdTrue[i];

      SANS_CHECK_CLOSE(estimate, estimateTrue, small_tol, close_tol);

      // first and second node make up the edge and therefore recieve half the new node's estimate
      SANS_CHECK_CLOSE(efld.DOF(0) + 0.5*efld.DOF(5), rsdTrue[0], small_tol, close_tol);
      SANS_CHECK_CLOSE(efld.DOF(1) + 0.5*efld.DOF(5), rsdTrue[1], small_tol, close_tol);
      SANS_CHECK_CLOSE(efld.DOF(2), rsdTrue[2], small_tol, close_tol);
      SANS_CHECK_CLOSE(efld.DOF(3), rsdTrue[3], small_tol, close_tol);
      SANS_CHECK_CLOSE(efld.DOF(4), rsdTrue[4], small_tol, close_tol);

      std::vector<Real> localEstimate{};
      errorEstimate.getLocalSolveErrorIndicator(localEstimate);

      BOOST_CHECK_EQUAL(localEstimate.size(), 5);

      for (int n= 0; n < 5; n++)
        SANS_CHECK_CLOSE(localEstimate[n], rsdTrue[n], small_tol, close_tol);
    }
  }
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
