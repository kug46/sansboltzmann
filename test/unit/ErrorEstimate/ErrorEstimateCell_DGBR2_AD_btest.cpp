// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEstimateCell_DGBR2_AD_btest
// testing of residual functions for DG BR2 with Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
//#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
//#include "pde/NDConvert/PDENDConvertSpace3D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/Tuple/FieldTuple.h"

#include "Discretization/DG/IntegrandCell_DGBR2.h"
#include "Discretization/DG/DiscretizationDGBR2.h"
#include "Discretization/IntegrateCellGroups.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "ErrorEstimate/DG/ErrorEstimateCell_DGBR2.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
//#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEstimateCell_DGBR2_AD_test_suite )

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateCell_DGBR2_1Line_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandClass;

  int nDOF = 2;
  int Ntrace = 2;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single triangle, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single line, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

   // solution data
  qfld.DOF(0) = 2;
  qfld.DOF(1) = 3;

  BOOST_CHECK_EQUAL( nDOF, qfld.nDOF() );

  // lifting operator: single line, P1 (aka Q1)
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  rfld.DOF(0) = -1;  rfld.DOF(1) = 5;
  rfld.DOF(2) =  3;  rfld.DOF(3) = 2;

  BOOST_CHECK_EQUAL( nDOF*Ntrace, rfld.nDOF() );
  BOOST_CHECK( rfld.D == pde.D);

  // weight: single line, P2 (aka Q2)
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

   // solution data
  wfld.DOF(0) = 3;
  wfld.DOF(1) = 4;
  wfld.DOF(2) = 5;

  BOOST_CHECK_EQUAL( 3, wfld.nDOF() );

  // lifting operator: single line, P2 (aka Q2)
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> sfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  sfld.DOF(0) = -5;  sfld.DOF(1) = 3;  sfld.DOF(2) = 2;
  sfld.DOF(3) =  2;  sfld.DOF(4) = 9;  sfld.DOF(5) = 3;

  BOOST_CHECK_EQUAL( 3*Ntrace, sfld.nDOF() );
  BOOST_CHECK( sfld.D == pde.D);

  // estimate fields
  Field_DG_Cell<PhysD1, TopoD1, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);

  // solution data
  efld.DOF(0) = 0;

  BOOST_CHECK_EQUAL( 1, efld.nElem() );
  BOOST_CHECK_EQUAL( 1, efld.nDOF() );

  // lifting operator: single line, P0 (aka Q0)
  FieldLift_DG_Cell<PhysD1, TopoD1, Real> eLfld(xfld, 0, BasisFunctionCategory_Legendre);

  eLfld.DOF(0) = 0;
  eLfld.DOF(1) = 0;

  BOOST_CHECK_EQUAL( 1, eLfld.nElem() );
  BOOST_CHECK_EQUAL( 1*Ntrace, eLfld.nDOF() );
  BOOST_CHECK( eLfld.D == pde.D);

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // cubic rule
  int quadratureOrder = 3;

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  IntegrateCellGroups<TopoD1>::integrate( ErrorEstimateCell_DGBR2(fcnint), xfld,
                                          (qfld, rfld, wfld, sfld, efld, eLfld), &quadratureOrder, 1 );

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-10;
  Real rsdPDE, rsdLO[2];

  //PDE residual: (advective) + (viscous)
  rsdPDE = ( 11./12. ) + ( 2123./1000. );   // Basis function

  SANS_CHECK_CLOSE( rsdPDE, efld.DOF(0), small_tol, close_tol );

  //Lifting Operator residual:
  rsdLO[0] = ( 14./3. ); // Lifting Operator
  rsdLO[1] = ( 109./6. ); // Lifting Operator

  SANS_CHECK_CLOSE( rsdLO[0], eLfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLO[1], eLfld.DOF(1), small_tol, close_tol );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateCell_DGBR2_1Triangle_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                  AdvectiveFlux2D_Uniform,
                                  ViscousFlux2D_Uniform,
                                  Source2D_UniformGrad> PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandClass;

  int nDOF = 3;
  int Ntrace = 3;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real s0 = 0.25, sx = 0.6, sy = -1.5;
  Source2D_UniformGrad source(s0, sx, sy);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

   // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  BOOST_CHECK_EQUAL( nDOF, qfld.nDOF() );

  // lifting operator: single triangle, P1 (aka Q1)
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  rfld.DOF(0) = { 2, -3};  rfld.DOF(1) = { 7,  8};  rfld.DOF(2) = {-1,  7};
  rfld.DOF(3) = { 9,  6};  rfld.DOF(4) = {-1,  3};  rfld.DOF(5) = { 2,  3};
  rfld.DOF(6) = {-2,  1};  rfld.DOF(7) = { 4, -4};  rfld.DOF(8) = {-9, -5};

  BOOST_CHECK_EQUAL( nDOF*Ntrace, rfld.nDOF() );
  BOOST_CHECK( rfld.D == pde.D);

  // weight: single triangle, P2 (aka Q1)
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  // triangle solution
  wfld.DOF(0) = -2;
  wfld.DOF(1) = 4;
  wfld.DOF(2) = 3;
  wfld.DOF(3) = 2;
  wfld.DOF(4) = 4;
  wfld.DOF(5) = -1;

  // lifting operator: single triangle, P1 (aka Q1)
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> sfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  sfld.DOF(0)  = { 1,  5};  sfld.DOF(1)  = { 3,  6};  sfld.DOF(2)  = {-1,  7};
  sfld.DOF(3)  = { 3,  1};  sfld.DOF(4)  = { 2,  2};  sfld.DOF(5)  = { 4,  3};

  sfld.DOF(6)  = { 5,  6};  sfld.DOF(7)  = { 4,  5};  sfld.DOF(8)  = { 3,  4};
  sfld.DOF(9)  = { 4,  3};  sfld.DOF(10) = { 3,  2};  sfld.DOF(11) = { 2,  1};

  sfld.DOF(12) = {-2, -1};  sfld.DOF(13) = {-3,  2};  sfld.DOF(14) = {-4, -3};
  sfld.DOF(15) = {-1, -3};  sfld.DOF(16) = {-2, -2};  sfld.DOF(17) = {-3, -1};

  // estimate fields
  Field_DG_Cell<PhysD2, TopoD2, Real> efld(xfld, 0, BasisFunctionCategory_Legendre);

  // solution data
  efld = 0;

  BOOST_CHECK_EQUAL( 1, efld.nElem() );
  BOOST_CHECK_EQUAL( 1, efld.nDOF() );

  // lifting operator: single line, P2 (aka Q2)
  FieldLift_DG_Cell<PhysD2, TopoD2, Real> eLfld(xfld, 0, BasisFunctionCategory_Legendre);

  eLfld = 0;

  BOOST_CHECK_EQUAL( 1, eLfld.nElem() );
  BOOST_CHECK_EQUAL( 1*Ntrace, eLfld.nDOF() );
  BOOST_CHECK( eLfld.D == pde.D);

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  int quadratureOrder = 3;

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  IntegrateCellGroups<TopoD2>::integrate( ErrorEstimateCell_DGBR2(fcnint), xfld,
                                          (qfld, rfld, wfld, sfld, efld, eLfld), &quadratureOrder, 1 );

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-10;

  Real rsdPDETrue, rsdLiftTrue[3];

  //PDE residual: (advection) + (diffusion) + (source)
  rsdPDETrue = (-34/5.) + (183223/6000.) + (-4711/300.);

  SANS_CHECK_CLOSE( rsdPDETrue, efld.DOF(0), small_tol, close_tol );

  // Lifting Operator residual:
  rsdLiftTrue[0] = (447/20.);   // Weight function
  rsdLiftTrue[1] = (103/4.);   // Weight function
  rsdLiftTrue[2] = (387/40.);   // Weight function

  for (int edge =0; edge < 3; edge++)
      SANS_CHECK_CLOSE( rsdLiftTrue[edge], eLfld.DOF(edge), small_tol, close_tol );

}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
