! Auto-generated script for communication between SANS and EPIC

lua ORIGNAME = "IO/Adaptation/case_a0.cgd"
lua INGRM = "IO/Adaptation/mesh_a0_in.grm"
lua OUTQ1GRM = "IO/Adaptation/mesh_a0_outQ1.grm"
file import grm $INGRM$ cgd $ORIGNAME$ 'INPUTOBJ'
file open cgd $ORIGNAME$
file set working cgd $ORIGNAME$
om set file cgd $ORIGNAME$
file set units inches file cgd $ORIGNAME$

lua OUTQ2GRM_NOPROJ = "IO/Adaptation/mesh_a0_outQ2_noproj.grm"
lua OUTQ2GRM = "IO/Adaptation/mesh_a0_outQ2.grm"
lua CSFFILE = "caseGeom.csf"
lua IGSFILE = "case.igs"
lua SHELLNAME = "Shell"
lua BONAME = "BoundaryObject"

lua SYMSURF0 = "U1"
lua SYMSURF1 = "U2"
lua SLIPSURF0 = "U3"
lua SLIPSURF1 = "U4"
lua VISCSURF0 = "U5"
lua VISCSURF1 = "U6"
lua VISCSURF2 = "U7"

file set working cgd $ORIGNAME$
om set file cgd $ORIGNAME$
ol set cell vertex
bc couple mode bilinear best
bc file name $ORIGNAME$
ol file name $ORIGNAME$
bc zone 1
bc group start guigroup
bc boundary $SYMSURF0$
bc boundary $SYMSURF1$
bc group end
bc set from_type UNDEFINED to REFLECTION
bc update
bc zone 1
bc group start guigroup
bc boundary $SLIPSURF0$
bc boundary $SLIPSURF1$
bc group end
bc set from_type UNDEFINED to INVISCID WALL
bc update
bc zone 1
bc group start guigroup
bc boundary $VISCSURF0$
bc boundary $VISCSURF1$
bc boundary $VISCSURF2$
bc group end
bc set from_type UNDEFINED to VISCOUS WALL
bc update

file open igs $IGSFILE$ mode apptopen
file open csf $CSFFILE$

om select clear
om select usurface object $BONAME$ file csf $CSFFILE$
geom register boundarydata
om select clear
geom autoproject on
om select clear
om select usurface object $SHELLNAME$ file csf $CSFFILE$
geom register associatedmesh

! Adaptation parameters
GGU ADAPT NCELL_REQUEST 0
GGU ADAPT SET_ERROR_PARAM METHOD FILE
GGU ADAPT SET_ERROR_PARAM FILE "IO/Adaptation/metric_request_a0.bnmetric"
GGU ADAPT SET_METRIC_PARAM HMIN -1.00000000e+00 HMAX -1.00000000e+00
GGU ADAPT SET_METRIC_PARAM MAXANISOTROPY 1.00000000e+06
GGU ADAPT SET_METRIC_PARAM TRANSFER AVERAGE AVERAGE LOG
GGU ADAPT SET_METRIC_PARAM GROWTH_LIMIT 0.00000000e+00
GGU ADAPT SET_METRIC_PARAM GROWTH_LIMIT_INITIAL 0.00000000e+00
GGU ADAPT SET_METRIC_PARAM GROWTH_NLAYER 0
GGU ADAPT SET_METRIC_PARAM GROWTH_NITER 0
GGU ADAPT SET_METRIC_PARAM GEOM_CURVELIMIT_NPTS 24 GEOM_CURVELIMIT_MIN -1.00000000e+00 GEOM_CURVELIMIT_MAX -1.00000000e+00
GGU ADAPT SET_METRIC_PARAM GEOM_CURVELIMIT_NORMSIZE -1.00000000e+00
GGU ADAPT SET_EPIC_PARAM PROJECT_MODE Elastic
GGU ADAPT SET_EPIC_PARAM PROJECT_TO_GEOM IGES
GGU ADAPT SET_EPIC_PARAM COARSENREFINE_BITMAP 247
GGU ADAPT SET_EPIC_PARAM COARSEN_MODE 1
GGU ADAPT SET_EPIC_PARAM NITER 25
GGU ADAPT SET_EPIC_PARAM MESSAGE_LEVEL 4
GGU ADAPT SET_EPIC_PARAM BACKGROUND_MESH INITIAL
GGU ADAPT SET_EPIC_QUALPARAM NORM 2
GGU ADAPT SET_EPIC_QUALPARAM QUALMODE Step
GGU ADAPT SET_EPIC_QUALPARAM QUALCOMBMODE ObjFunc
GGU ADAPT SET_EPIC_QUALPARAM CELLQUALITY_RATIO 1.00000000e+00
GGU ADAPT SET_EPIC_QUALPARAM INITIAL_QUALTOL -1.00000000e+00
GGU ADAPT SET_EPIC_QUALPARAM FACEQUALITY_RATIO -1.00000000e+00
GGU ADAPT SET_EPIC_QUALPARAM FOLDED_FACEANGLE 7.00000000e+01
GGU ADAPT SET_EPIC_QUALPARAM CORNER_EDGEANGLE 7.00000000e+01
GGU ADAPT SET_EPIC_QUALPARAM REMOVETRAPPED ON
GGU ADAPT SET_EPIC_BLAYERPARAM NORMSIZE -1.00000000e+00
GGU ADAPT SET_EPIC_BLAYERPARAM ANISOCUTOFF 5.00000000e+00
GGU ADAPT SET_EPIC_BLAYERPARAM NLAYER1 2 GROW1 1.00000000e+00
GGU ADAPT SET_EPIC_BLAYERPARAM NLAYER2 0 GROW2 0
GGU ADAPT SET_EPIC_BLAYERPARAM EDGEQUALMODE REINSERT RECONNECT

! Select object to be adapted
om select clear
om select usurface object $SHELLNAME$ file csf $CSFFILE$
om select uzone object 'ZONE   1' file cgd $ORIGNAME$
manip umergeoptdata
om select $result

! Metric preprocessing
GGU ADAPT ATTACH_TARGET_SIZE

! Adaptation
om select $result
GGU ADAPT EXECUTE USING EPIC
file save name 'ADAPTED' overwrite file cgd $ORIGNAME$

om select clear
om select usurface object $SHELLNAME$ file csf $CSFFILE$
om select uzone object 'ZONE   2' file cgd $ORIGNAME$
manip umergeoptdata
om select $result
manip deform solver cg precond sgs geometry
file save name 'ADAPTED PROJECTED' overwrite file cgd $ORIGNAME$
om select $result
file export to file grm $OUTQ1GRM$ overwrite optionalarg
om select clear
om select usurface object $SHELLNAME$ file csf $CSFFILE$
om select uzone object 'ZONE   2' file cgd $ORIGNAME$
manip umergeoptdata
om select $result
ggu higherorder qorder 2 projectdelta 'IO/Adaptation/projectdeform_a0.disp'
file save name 'ADAPTED Q2 NOPROJ' overwrite file cgd $ORIGNAME$
om select $result
file export to file grm $OUTQ2GRM_NOPROJ$ overwrite optionalarg
manip deform solver local model quality geometry
file save name 'ADAPTED Q2 PROJECTED' overwrite file cgd $ORIGNAME$
om select $result
file export to file grm $OUTQ2GRM$ overwrite optionalarg

