  FieldBase<FieldTraits>:  nDOF_ = 9  nCellGroups_ = 1  nInteriorTraceGroups_ = 0  nBoundaryTraceGroups_ = 1
    DOF_[] = (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) 
    local2nativeDOFmap_[] = (0, 0) (1, 1) (2, 2) (3, 3) (4, 4) (5, 5) (6, 6) (7, 7) (8, 8) 
  elemCellGroups_[0] = 
    FieldAssociativity:  nElem_ = 2  nDOF_ = 9
      DOF_[] =(0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) 
      assoc_[0]:
        ElementAssociativity<TopoD2>: order_ = 2  nNode_ = 3  nEdge_ = 3  nCell_ = 0  edgeSign_ = 1 1 1
                nodeList = 5 6 7
                edgeList = 2 1 0
      assoc_[1]:
        ElementAssociativity<TopoD2>: order_ = 2  nNode_ = 3  nEdge_ = 3  nCell_ = 0  edgeSign_ = -1 1 1
                nodeList = 8 7 6
                edgeList = 2 3 4
  elemBoundaryTraceGroups_[0] = 
    FieldAssociativity:  nElem_ = 4  nDOF_ = 9
      DOF_[] =(0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) 
      assoc_[0]:
        ElementAssociativity<TopoD1>: order_ = 2  nNode_ = 2  nEdge_ = 1
                nodeList = 5 6 
                edgeList = 0 
      assoc_[1]:
        ElementAssociativity<TopoD1>: order_ = 2  nNode_ = 2  nEdge_ = 1
                nodeList = 6 8 
                edgeList = 3 
      assoc_[2]:
        ElementAssociativity<TopoD1>: order_ = 2  nNode_ = 2  nEdge_ = 1
                nodeList = 8 7 
                edgeList = 4 
      assoc_[3]:
        ElementAssociativity<TopoD1>: order_ = 2  nNode_ = 2  nEdge_ = 1
                nodeList = 7 5 
                edgeList = 1 
  FieldBase<FieldTraits>:  nDOF_ = 9  nCellGroups_ = 0  nInteriorTraceGroups_ = 1  nBoundaryTraceGroups_ = 1
    DOF_[] = (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) 
    local2nativeDOFmap_[] = (0, 0) (1, 1) (2, 2) (3, 3) (4, 4) (5, 5) (6, 6) (7, 7) (8, 8) 
  elemInteriorTraceGroups_[0] = 
    FieldAssociativity:  nElem_ = 1  nDOF_ = 9
      DOF_[] =(0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) 
      assoc_[0]:
        ElementAssociativity<TopoD1>: order_ = 2  nNode_ = 2  nEdge_ = 1
                nodeList = 6 7 
                edgeList = 2 
  elemBoundaryTraceGroups_[0] = 
    FieldAssociativity:  nElem_ = 4  nDOF_ = 9
      DOF_[] =(0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) 
      assoc_[0]:
        ElementAssociativity<TopoD1>: order_ = 2  nNode_ = 2  nEdge_ = 1
                nodeList = 5 6 
                edgeList = 0 
      assoc_[1]:
        ElementAssociativity<TopoD1>: order_ = 2  nNode_ = 2  nEdge_ = 1
                nodeList = 6 8 
                edgeList = 3 
      assoc_[2]:
        ElementAssociativity<TopoD1>: order_ = 2  nNode_ = 2  nEdge_ = 1
                nodeList = 8 7 
                edgeList = 4 
      assoc_[3]:
        ElementAssociativity<TopoD1>: order_ = 2  nNode_ = 2  nEdge_ = 1
                nodeList = 7 5 
                edgeList = 1 
  FieldBase<FieldTraits>:  nDOF_ = 3  nCellGroups_ = 0  nInteriorTraceGroups_ = 1  nBoundaryTraceGroups_ = 0
    DOF_[] = (0, 0) (0, 0) (0, 0) 
    local2nativeDOFmap_[] = (0, 0) (1, 1) (2, 2) 
  elemInteriorTraceGroups_[0] = 
    FieldAssociativity:  nElem_ = 1  nDOF_ = 3
      DOF_[] =(0, 0) (0, 0) (0, 0) 
      assoc_[0]:
        ElementAssociativity<TopoD1>: order_ = 2  nNode_ = 2  nEdge_ = 1
                nodeList = 1 2 
                edgeList = 0 
  FieldBase<FieldTraits>:  nDOF_ = 8  nCellGroups_ = 0  nInteriorTraceGroups_ = 0  nBoundaryTraceGroups_ = 1
    DOF_[] = (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) 
    local2nativeDOFmap_[] = (0, 0) (1, 1) (2, 2) (3, 3) (4, 4) (5, 5) (6, 6) (7, 7) 
  elemBoundaryTraceGroups_[0] = 
    FieldAssociativity:  nElem_ = 4  nDOF_ = 8
      DOF_[] =(0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) 
      assoc_[0]:
        ElementAssociativity<TopoD1>: order_ = 2  nNode_ = 2  nEdge_ = 1
                nodeList = 4 5 
                edgeList = 0 
      assoc_[1]:
        ElementAssociativity<TopoD1>: order_ = 2  nNode_ = 2  nEdge_ = 1
                nodeList = 5 7 
                edgeList = 2 
      assoc_[2]:
        ElementAssociativity<TopoD1>: order_ = 2  nNode_ = 2  nEdge_ = 1
                nodeList = 7 6 
                edgeList = 3 
      assoc_[3]:
        ElementAssociativity<TopoD1>: order_ = 2  nNode_ = 2  nEdge_ = 1
                nodeList = 6 4 
                edgeList = 1 
