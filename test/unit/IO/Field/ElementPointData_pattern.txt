  ElementNode:  order_ = 0  nDOF_ = 1
    DOF_ = (1, 2) 
    basis_:
    BasisFunctionNode: order = 0  nBasis = 1
  ElementNode:  order_ = 1  nDOF_ = 2
    DOF_ = (1, 2) (3, 4) 
    basis_:
    BasisFunctionLine: order = 1  nBasis = 2
  ElementNode:  order_ = 1  nDOF_ = 3
    DOF_ = (1, 2) (3, 4) (5, 6) 
    basis_:
    BasisFunctionArea<...>: order = 1  nBasis = 3  nBasisNode = 3  nBasisEdge = 0  nBasisCell = 0
  ElementNode:  order_ = 1  nDOF_ = 4
    DOF_ = (1, 2) (3, 4) (5, 6) (7, 8) 
    basis_:
    BasisFunctionVolume<...>: order = 1  nBasis = 4  nBasisNode = 4  nBasisEdge = 0  nBasisCell = 0
