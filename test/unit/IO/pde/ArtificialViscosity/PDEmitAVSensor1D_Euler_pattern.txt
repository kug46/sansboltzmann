  PDEmitAVSensor1D: adv_ =
    AVSensor_AdvectiveFlux1D_Uniform:  u_ = 1
  PDEmitAVSensor1D: visc_ =
    AVSensor_DiffusionMatrix1D_Uniform:  kxx_ = 0.8
  PDEmitAVSensor1D: source_ =
    AVSensor_Source1D_Uniform:
      a_ = 0.8
  PDEmitAVSensor1D: pde_ =
    PDEEulermitAVDiffusion1D1D: pde_ =
      PDEEuler1D<PDETraitsSize, PDETraitsModel>: N = 4
      PDEEuler1D<PDETraitsSize, PDETraitsModel>: qInterpret_ = 
        Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 4
        Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
          GasModel<T>:
            gamma_ = 1.4
            R_ = 0.4
      PDEEuler1D<PDETraitsSize, PDETraitsModel>: gas_ = 
        GasModel<T>:
          gamma_ = 1.4
          R_ = 0.4
