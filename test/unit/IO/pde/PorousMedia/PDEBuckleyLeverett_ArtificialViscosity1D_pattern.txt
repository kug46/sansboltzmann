  PDEBuckleyLeverett_ArtificialViscosity1D:
    order_ = 1
    hasSpaceTimeDiffusion_ = 0
    C_artvisc_ = 1
    pde_ = 
      PDEBuckleyLeverett1D<QType, RelPermModel>:
        qInterpret_ = 
          Q1D<QTypePrimitive_Sw>
        kr_ = 
          RelPermModel_PowerLaw_Params<T>:
            power_ = 2
        phi_ = 0.3
        uT_ = 0.4
        mu_w_ = 1
        mu_n_ = 2
        K_ = 0.25
        pc_ = 
          CapillaryModel_Linear<T>:
            pc_max_ = 5
        force_ = 
