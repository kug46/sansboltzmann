  BC<PDELinearizedIncompressiblePotential2D, BCTypeDirichlet>:
    bcdata_ = 3
  BC<PDELinearizedIncompressiblePotential2D, BCTypeNeumann>:
    bcdata_ = 3
  BC<PDELinearizedIncompressiblePotential2D, BCTypeWall>:
    (u_, v_) = (0.95, -0.21)
  BC<PDELinearizedIncompressiblePotential2D, BCTypeFarfieldVortex>:
    (x0_, y0_) = (-2, -3)
  BC<PDELinearizedIncompressiblePotential2D, BCTypeFunction<SolutionDummy> >:
