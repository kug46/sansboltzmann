  PDENavierStokes2D<PDETraitsSize, PDETraitsModel>: N = 4
  PDENavierStokes2D<PDETraitsSize, PDETraitsModel>: qInterpret_ = 
    Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 4
    Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
      GasModel<T>:
        gamma_ = 1.4
        R_ = 287.04
  PDENavierStokes2D<PDETraitsSize, PDETraitsModel>: gas_ = 
    GasModel<T>:
      gamma_ = 1.4
      R_ = 287.04
  PDENavierStokes2D<PDETraitsSize, PDETraitsModel>: visc_ = 
    ViscosityModel<T>:
      tSuth_ = 110
      tRef_ = 300
      muRef_ = 1.789e-05
  PDENavierStokes2D<PDETraitsSize, PDETraitsModel>: tcond_ = 
    ThermalConductivityModel<T>:
      Prandtl_ = 0.72
      Cp_ = 1004.64
