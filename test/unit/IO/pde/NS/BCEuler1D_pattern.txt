  BCEuler1D<BCTypeTimeIC, PDE>: pde_ = 
    PDEEuler1D<PDETraitsSize, PDETraitsModel>: N = 3
    PDEEuler1D<PDETraitsSize, PDETraitsModel>: qInterpret_ = 
      Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 3
      Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
        GasModel<T>:
          gamma_ = 1.4
          R_ = 0.4
    PDEEuler1D<PDETraitsSize, PDETraitsModel>: gas_ = 
      GasModel<T>:
        gamma_ = 1.4
        R_ = 0.4
  BCEuler1D<BCTypeTimeIC, PDE>: qB_ = 
    VectorS<3,T>:
      data = 1.1 1.3 5.45 
  BCEuler1D<BCTypeReflect, PDE>: pde_ = 
    PDEEuler1D<PDETraitsSize, PDETraitsModel>: N = 3
    PDEEuler1D<PDETraitsSize, PDETraitsModel>: qInterpret_ = 
      Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 3
      Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
        GasModel<T>:
          gamma_ = 1.4
          R_ = 0.4
    PDEEuler1D<PDETraitsSize, PDETraitsModel>: gas_ = 
      GasModel<T>:
        gamma_ = 1.4
        R_ = 0.4
  BCEuler1D<BCTypeReflect, PDE>: qInterpret_ = 
    Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 3
    Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
      GasModel<T>:
        gamma_ = 1.4
        R_ = 0.4
  BCEuler1D<BCTypeReflect, PDE>: gas_ = 
    GasModel<T>:
      gamma_ = 1.4
      R_ = 0.4
  BCEuler1D<BCTypeReflect, PDE>: bcdata_ = 
    VectorS<3,T>:
      data = 0 0 0 
  BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<PDETraitsSize, PDETraitsModel>>: pde_ = 
    PDEEuler1D<PDETraitsSize, PDETraitsModel>: N = 3
    PDEEuler1D<PDETraitsSize, PDETraitsModel>: qInterpret_ = 
      Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 3
      Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
        GasModel<T>:
          gamma_ = 1.4
          R_ = 0.4
    PDEEuler1D<PDETraitsSize, PDETraitsModel>: gas_ = 
      GasModel<T>:
        gamma_ = 1.4
        R_ = 0.4
  BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<PDETraitsSize, PDETraitsModel>>: qInterpret_ = 
    Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 3
    Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
      GasModel<T>:
        gamma_ = 1.4
        R_ = 0.4
  BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<PDETraitsSize, PDETraitsModel>>: gas_ = 
    GasModel<T>:
      gamma_ = 1.4
      R_ = 0.4
  BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<PDETraitsSize, PDETraitsModel>>: bcdata_ = 
    VectorS<3,T>:
      data = 0 0 0 
  BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>>: pde_ = 
    PDEEuler1D<PDETraitsSize, PDETraitsModel>: N = 3
    PDEEuler1D<PDETraitsSize, PDETraitsModel>: qInterpret_ = 
      Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 3
      Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
        GasModel<T>:
          gamma_ = 1.4
          R_ = 0.4
    PDEEuler1D<PDETraitsSize, PDETraitsModel>: gas_ = 
      GasModel<T>:
        gamma_ = 1.4
        R_ = 0.4
  BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>>: qInterpret_ = 
    Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 3
    Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
      GasModel<T>:
        gamma_ = 1.4
        R_ = 0.4
  BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>>: gas_ = 
    GasModel<T>:
      gamma_ = 1.4
      R_ = 0.4
  BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>>: bcdata_ = 
    VectorS<3,T>:
      data = 0 0 0 
  BCEuler1D<BCTypeFunction_mitState, PDEEuler1D<PDETraitsSize, PDETraitsModel>>: pde_ = 
    PDEEuler1D<PDETraitsSize, PDETraitsModel>: N = 3
    PDEEuler1D<PDETraitsSize, PDETraitsModel>: qInterpret_ = 
      Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 3
      Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
        GasModel<T>:
          gamma_ = 1.4
          R_ = 0.4
    PDEEuler1D<PDETraitsSize, PDETraitsModel>: gas_ = 
      GasModel<T>:
        gamma_ = 1.4
        R_ = 0.4
  BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDE>: pde_ = 
    PDEEuler1D<PDETraitsSize, PDETraitsModel>: N = 3
    PDEEuler1D<PDETraitsSize, PDETraitsModel>: qInterpret_ = 
      Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 3
      Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
        GasModel<T>:
          gamma_ = 1.4
          R_ = 0.4
    PDEEuler1D<PDETraitsSize, PDETraitsModel>: gas_ = 
      GasModel<T>:
        gamma_ = 1.4
        R_ = 0.4
  BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDE>: qInterpret_ = 
    Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 3
    Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
      GasModel<T>:
        gamma_ = 1.4
        R_ = 0.4
  BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDE>: gas_ = 
    GasModel<T>:
      gamma_ = 1.4
      R_ = 0.4
  BCEuler1D<BCTypeTimeIC_Function, PDE>: pde_ = 
    PDEEuler1D<PDETraitsSize, PDETraitsModel>: N = 3
    PDEEuler1D<PDETraitsSize, PDETraitsModel>: qInterpret_ = 
      Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 3
      Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
        GasModel<T>:
          gamma_ = 1.4
          R_ = 0.4
    PDEEuler1D<PDETraitsSize, PDETraitsModel>: gas_ = 
      GasModel<T>:
        gamma_ = 1.4
        R_ = 0.4
