  BCEuler2D_ArtificialViscosity<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: pde_ = 
    PDEEulermitAVDiffusion2D2D: pde_ =
      PDEEuler2D<PDETraitsSize, PDETraitsModel>: N = 4
      PDEEuler2D<PDETraitsSize, PDETraitsModel>: qInterpret_ = 
        Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 4
        Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
          GasModel<T>:
            gamma_ = 1.4
            R_ = 0.4
      PDEEuler2D<PDETraitsSize, PDETraitsModel>: gas_ = 
        GasModel<T>:
          gamma_ = 1.4
          R_ = 0.4
  BCEuler2D_ArtificialViscosity<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: qInterpret_ = 
    Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 4
    Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
      GasModel<T>:
        gamma_ = 1.4
        R_ = 0.4
  BCEuler2D_ArtificialViscosity<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: gas_ = 
    GasModel<T>:
      gamma_ = 1.4
      R_ = 0.4
  BCEuler2D_ArtificialViscosity<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: bcdata_ = 
    VectorS<4,T>:
      data = -7 0 0 0 
  BCEuler2D_ArtificialViscosity<BCTypeInflowSupersonic, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: pde_ = 
    PDEEulermitAVDiffusion2D2D: pde_ =
      PDEEuler2D<PDETraitsSize, PDETraitsModel>: N = 4
      PDEEuler2D<PDETraitsSize, PDETraitsModel>: qInterpret_ = 
        Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 4
        Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
          GasModel<T>:
            gamma_ = 1.4
            R_ = 0.4
      PDEEuler2D<PDETraitsSize, PDETraitsModel>: gas_ = 
        GasModel<T>:
          gamma_ = 1.4
          R_ = 0.4
  BCEuler2D_ArtificialViscosity<BCTypeInflowSupersonic, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: qInterpret_ = 
    Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 4
    Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
      GasModel<T>:
        gamma_ = 1.4
        R_ = 0.4
  BCEuler2D_ArtificialViscosity<BCTypeInflowSupersonic, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: gas_ = 
    GasModel<T>:
      gamma_ = 1.4
      R_ = 0.4
  BCEuler2D_ArtificialViscosity<BCTypeInflowSupersonic, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: bcdata_ = 
    VectorS<4,T>:
      data = 7 3.551 -4.993 2.997 
  BCEuler2D_ArtificialViscosity<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: pde_ = 
    PDEEulermitAVDiffusion2D2D: pde_ =
      PDEEuler2D<PDETraitsSize, PDETraitsModel>: N = 4
      PDEEuler2D<PDETraitsSize, PDETraitsModel>: qInterpret_ = 
        Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 4
        Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
          GasModel<T>:
            gamma_ = 1.4
            R_ = 0.4
      PDEEuler2D<PDETraitsSize, PDETraitsModel>: gas_ = 
        GasModel<T>:
          gamma_ = 1.4
          R_ = 0.4
  BCEuler2D_ArtificialViscosity<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: qInterpret_ = 
    Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 4
    Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
      GasModel<T>:
        gamma_ = 1.4
        R_ = 0.4
  BCEuler2D_ArtificialViscosity<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: gas_ = 
    GasModel<T>:
      gamma_ = 1.4
      R_ = 0.4
  BCEuler2D_ArtificialViscosity<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: bcdata_ = 
    VectorS<4,T>:
      data = -7.153 3.551 4.993 0 
  BCEuler2D_ArtificialViscosity<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: pde_ = 
    PDEEulermitAVDiffusion2D2D: pde_ =
      PDEEuler2D<PDETraitsSize, PDETraitsModel>: N = 4
      PDEEuler2D<PDETraitsSize, PDETraitsModel>: qInterpret_ = 
        Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 4
        Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
          GasModel<T>:
            gamma_ = 1.4
            R_ = 0.4
      PDEEuler2D<PDETraitsSize, PDETraitsModel>: gas_ = 
        GasModel<T>:
          gamma_ = 1.4
          R_ = 0.4
  BCEuler2D_ArtificialViscosity<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: qInterpret_ = 
    Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 4
    Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
      GasModel<T>:
        gamma_ = 1.4
        R_ = 0.4
  BCEuler2D_ArtificialViscosity<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: gas_ = 
    GasModel<T>:
      gamma_ = 1.4
      R_ = 0.4
  BCEuler2D_ArtificialViscosity<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: bcdata_ = 
    VectorS<4,T>:
      data = -7 0 0 0 
  BCEuler2D_ArtificialViscosity<BCTypeOutflowSubsonic_Pressure_mitState, PDE>: pde_ = 
    PDEEulermitAVDiffusion2D2D: pde_ =
      PDEEuler2D<PDETraitsSize, PDETraitsModel>: N = 4
      PDEEuler2D<PDETraitsSize, PDETraitsModel>: qInterpret_ = 
        Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 4
        Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
          GasModel<T>:
            gamma_ = 1.4
            R_ = 0.4
      PDEEuler2D<PDETraitsSize, PDETraitsModel>: gas_ = 
        GasModel<T>:
          gamma_ = 1.4
          R_ = 0.4
  BCEuler2D_ArtificialViscosity<BCTypeOutflowSubsonic_Pressure_mitState, PDE>: qInterpret_ = 
    Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 4
    Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
      GasModel<T>:
        gamma_ = 1.4
        R_ = 0.4
  BCEuler2D_ArtificialViscosity<BCTypeOutflowSubsonic_Pressure_mitState, PDE>: gas_ = 
    GasModel<T>:
      gamma_ = 1.4
      R_ = 0.4
  BCEuler2D_ArtificialViscosity<BCTypeOutflowSubsonic_Pressure_mitState, PDE>: pSpec_ = 2.3
  BCEuler2D_ArtificialViscosity<BCTypeFullState_mitState, PDE>: pde_ = 
    PDEEulermitAVDiffusion2D2D: pde_ =
      PDEEuler2D<PDETraitsSize, PDETraitsModel>: N = 4
      PDEEuler2D<PDETraitsSize, PDETraitsModel>: qInterpret_ = 
        Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 4
        Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
          GasModel<T>:
            gamma_ = 1.4
            R_ = 0.4
      PDEEuler2D<PDETraitsSize, PDETraitsModel>: gas_ = 
        GasModel<T>:
          gamma_ = 1.4
          R_ = 0.4
  BCEuler2D_ArtificialViscosity<BCTypeFullState_mitState, PDE>: qInterpret_ = 
    Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 4
    Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
      GasModel<T>:
        gamma_ = 1.4
        R_ = 0.4
  BCEuler2D_ArtificialViscosity<BCTypeInflowSubsonic_PtTta_mitState, PDE>: pde_ = 
    PDEEulermitAVDiffusion2D2D: pde_ =
      PDEEuler2D<PDETraitsSize, PDETraitsModel>: N = 4
      PDEEuler2D<PDETraitsSize, PDETraitsModel>: qInterpret_ = 
        Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 4
        Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
          GasModel<T>:
            gamma_ = 1.4
            R_ = 0.4
      PDEEuler2D<PDETraitsSize, PDETraitsModel>: gas_ = 
        GasModel<T>:
          gamma_ = 1.4
          R_ = 0.4
  BCEuler2D_ArtificialViscosity<BCTypeInflowSubsonic_PtTta_mitState, PDE>: qInterpret_ = 
    Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = 4
    Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = 
      GasModel<T>:
        gamma_ = 1.4
        R_ = 0.4
  BCEuler2D_ArtificialViscosity<BCTypeInflowSubsonic_PtTta_mitState, PDE>: gas_ = 
    GasModel<T>:
      gamma_ = 1.4
      R_ = 0.4
