// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ResidualCell_VMSD_AD_btest
// testing of cell residual functions for VMSD with Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/XField_CellToTrace.h"

#include "Discretization/QuadratureOrder.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Discretization/VMSD/IntegrandCell_VMSD.h"
#include "Discretization/VMSD/IntegrandTrace_VMSD.h"
#include "Discretization/VMSD/ResidualCell_VMSD.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ResidualCell_VMSD_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualCell_VMSD_1D_1Line_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef IntegrandCell_VMSD<PDEClass> IntegrandCellClass;
  typedef IntegrandTrace_VMSD<PDEClass> IntegrandTraceClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_UniformGrad source(1.0/4.0, 3.0/5.0);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid: single line, P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 2, xfld.nElem() );

  XField_CellToTrace<PhysD1, TopoD1> connectivity(xfld);

  // solution: single line, P1 (aka Q1)
  int qorder = 1;
  Field_CG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

   // solution data
  qfld.DOF(0) = 2;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 2.5;

  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qpfld(xfld, qorder+1, BasisFunctionCategory_Hierarchical);

  qpfld.DOF(0) = 0.11;
  qpfld.DOF(1) = 0.07;
  qpfld.DOF(2) = -0.03;

  qpfld.DOF(3) = -0.02;
  qpfld.DOF(4) = 0.04;
  qpfld.DOF(5) = 0.015;

  // quadrature rule
  QuadratureOrder quadOrder(xfld, 4);

  Real rsdPDETrue[3], rsdPDEPTrue[6];

  //CELL 1 INTEGRAND
  rsdPDETrue[0] = ( 2827/1000. ) + ( -6369/3125. ) + ( 2309/4000. ) ;   // Basis function 1
  rsdPDETrue[1] = ( -2827/1000. ) + ( 6369/3125. ) + ( 513/800. ) ;   // Basis function 2

  rsdPDEPTrue[0] = ( 2827/1000. ) + ( -6369/3125. ) + ( 2309/4000. ) ;   // Basis function 1
  rsdPDEPTrue[1] = ( -2827/1000. ) + ( 6369/3125. ) + ( 513/800. ) ;   // Basis function 2
  rsdPDEPTrue[2] = ( 88/125. ) + ( -2123/6250. ) + ( 487/600. ) ;   // Basis function 3

  //CELL 2 INTEGRAND
  rsdPDETrue[1] += ( 3047/1000. ) + ( 23353/25000. ) + ( 2753/12000. ) ;   // Basis function 1
  rsdPDETrue[2] = ( -3047/1000. ) + ( -23353/25000. ) + ( 2389/12000. ) ;   // Basis function 2

  rsdPDEPTrue[3] = ( 3047/1000. ) + ( 23353/25000. ) + ( 2753/12000. ) ;   // Basis function 1
  rsdPDEPTrue[4] = ( -3047/1000. ) + ( -23353/25000. ) + ( 2389/12000. ) ;   // Basis function 2
  rsdPDEPTrue[5] = ( -121/375. ) + ( 2123/12500. ) + ( 143/500. ) ;   // Basis function 3

  //CELL 1, LEFT TRACE INTEGRAND
  rsdPDETrue[0] += ( 0 ) + ( 0 ) + ( -23353/100000. );  // Basis function 1
  rsdPDETrue[1] += ( 0 ) + ( 0 ) + ( 23353/100000. );  // Basis function 2

  //PDE residual integrands (right): (advective) + (viscous) + (dual-consistent)
  rsdPDEPTrue[0] += ( -2079/1000. ) + ( 44583/25000. ) + ( 23353/10000. ) + ( -23353/100000. ) ;  // Basis function 1
  rsdPDEPTrue[1] += ( 0 ) + ( 0 ) + ( 0 ) + ( 23353/100000. ) ;  // Basis function 2
  rsdPDEPTrue[2] += ( 0 ) + ( 0 ) + ( 0 ) + ( 23353/25000. ) ;  // Basis function 3



  //CELL 1, RIGHT TRACE INTEGRAND
  rsdPDETrue[0] += ( 0 ) + ( 0 ) + ( 14861/100000. );  // Basis function 1
  rsdPDETrue[1] += ( 0 ) + ( 0 ) + ( -14861/100000. );  // Basis function 2

  //PDE residual integrands (right): (advective) + (viscous) + (dual-consistent)
  rsdPDEPTrue[0] += ( 0 ) + ( 0 ) + ( 0 ) + ( 14861/100000. ) ;  // Basis function 1
  rsdPDEPTrue[1] += ( 3377/1000. ) + ( -57321/25000. ) + ( 14861/10000. ) + ( -14861/100000. ) ;  // Basis function 2
  rsdPDEPTrue[2] += ( 0 ) + ( 0 ) + ( 0 ) + ( 14861/25000. ) ;  // Basis function 3

  //CELL 2, LEFT TRACE INTEGRAND
  rsdPDETrue[1] += ( 0 ) + ( 0 ) + ( 2123/50000. );  // Basis function 1
  rsdPDETrue[2] += ( 0 ) + ( 0 ) + ( -2123/50000. );  // Basis function 2

  //PDE residual integrands (right): (advective) + (viscous) + (dual-consistent)
  rsdPDEPTrue[3] += ( -1661/500. ) + ( -40337/50000. ) + ( -2123/5000. ) + ( 2123/50000. );  // Basis function 1
  rsdPDEPTrue[4] += ( 0 ) + ( 0 ) + ( 0 ) + ( -2123/50000. ) ;  // Basis function 2
  rsdPDEPTrue[5] += ( 0 ) + ( 0 ) + ( 0 ) + ( -2123/12500. ) ;  // Basis function 3

  //CELL 2, RIGHT TRACE INTEGRAND
  rsdPDETrue[1] += ( 0 ) + ( 0 ) + ( 2123/25000. );  // Basis function 1
  rsdPDETrue[2] += ( 0 ) + ( 0 ) + ( -2123/25000. );  // Basis function 2

  //PDE residual integrands (right): (advective) + (viscous) + (dual-consistent)
  rsdPDEPTrue[3] += ( 0 ) + ( 0 ) + ( 0 ) + ( 2123/25000. ) ;  // Basis function 1
  rsdPDEPTrue[4] += ( 1397/500. ) + ( 2123/2000. ) + ( 2123/2500. ) + ( -2123/25000. ) ;  // Basis function 2
  rsdPDEPTrue[5] += ( 0 ) + ( 0 ) + ( 0 ) + ( 2123/6250. ) ;  // Basis function 3


  const Real tol = 1e-12;

  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());
  SLA::SparseVector<ArrayQ> rsdPDEpGlobal(qpfld.nDOF());
  rsdPDEGlobal = 0;
  rsdPDEpGlobal = 0;

  // integrand
  StabilizationNitsche stab(1, 2.5);
  IntegrandCellClass fcnCell( pde, {0} );
  IntegrandTraceClass fcnITrace( pde, stab, {0} );
  IntegrandTraceClass fcnBTrace( pde, stab, {0,1} );

  //Compute cell PDE residual without solving for auxiliary variables
  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_VMSD(fcnCell, fcnITrace, fcnBTrace, connectivity,
                                                           xfld, qfld, qpfld, quadOrder,
                                                           rsdPDEGlobal, rsdPDEpGlobal),
                                          xfld, (qfld, qpfld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

  BOOST_CHECK_CLOSE( rsdPDETrue[0], rsdPDEGlobal[0], tol );
  BOOST_CHECK_CLOSE( rsdPDETrue[1], rsdPDEGlobal[1], tol );
  BOOST_CHECK_CLOSE( rsdPDETrue[2], rsdPDEGlobal[2], tol );

  BOOST_CHECK_CLOSE( rsdPDEPTrue[0], rsdPDEpGlobal[0], tol );
  BOOST_CHECK_CLOSE( rsdPDEPTrue[1], rsdPDEpGlobal[1], tol );
  BOOST_CHECK_CLOSE( rsdPDEPTrue[2], rsdPDEpGlobal[2], tol );
  BOOST_CHECK_CLOSE( rsdPDEPTrue[3], rsdPDEpGlobal[3], tol );
  BOOST_CHECK_CLOSE( rsdPDEPTrue[4], rsdPDEpGlobal[4], tol );
  BOOST_CHECK_CLOSE( rsdPDEPTrue[5], rsdPDEpGlobal[5], tol );
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualCell_HDG_2D_1Triangle_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef testspace::IntegrandInteriorTrace_HDG_None<PDEClass> IntegrandTraceClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_UniformGrad source(0.25, 0.6, -1.5);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  XField_CellToTrace<PhysD2, TopoD2> connectivity(xfld);

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

   // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  // auxiliary variable: single triangle, P1 (aka Q1)
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // auxiliary variable data
  afld.DOF(0) = { 2, -3};
  afld.DOF(1) = { 7,  8};
  afld.DOF(2) = {-1, -5};

  // interface solution: P1 (aka Q1)
  Field_DG_InteriorTrace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // HDG discretization (not used)
  DiscretizationHDG<PDEClass> disc( pde );

  // quadrature rule: quadratic (aux var defn has linear basis & linear solution)
  QuadratureOrder quadOrder(xfld, 2);

  Real rsdPDETrue[3];

  //PDE residual: (advection) + (diffusion) + (source)
  rsdPDETrue[0] = (26/15.) + (-1456/375.) + (17/32.);   // Basis function 1
  rsdPDETrue[1] = (-22/15.) + (2123/750.) + (-1/96.);   // Basis function 2
  rsdPDETrue[2] = (-4/15.) + (263/250.) + (49/80.);   // Basis function 3

  const Real tol = 1e-12;

  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());
  SLA::SparseVector<ArrayQ> rsdINTGlobal(qIfld.nDOF());
  rsdPDEGlobal = 0;
  rsdINTGlobal = 0;

  // integrand
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandTraceClass fcnTrace( {} );

  // base interface
  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_HDG(fcnCell, fcnTrace, connectivity,
                                                           xfld, qfld, afld, qIfld,
                                                           quadOrder.interiorTraceOrders.data(),
                                                           quadOrder.interiorTraceOrders.size(),
                                                           rsdPDEGlobal, rsdINTGlobal),
                                          xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

  BOOST_CHECK_CLOSE( rsdPDETrue[0], rsdPDEGlobal[0], tol );
  BOOST_CHECK_CLOSE( rsdPDETrue[1], rsdPDEGlobal[1], tol );
  BOOST_CHECK_CLOSE( rsdPDETrue[2], rsdPDEGlobal[2], tol );
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
