// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_HDG_AD_btest
// testing of cell residual integrands for HDG: Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/Element/ElementArea.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"

#include "Discretization/VMSD/IntegrandCell_VMSD_NL.h"
#include "Discretization/VMSD/JacobianCell_VMSD_Element.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_UniformGrad> PDEAdvectionDiffusion1D;
typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass1D;
typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldLine;
template class IntegrandCell_VMSD_NL<PDEClass1D>::BasisWeighted<Real,TopoD1,Line,ElementXFieldLine>;
template class IntegrandCell_VMSD_NL<PDEClass1D>::FieldWeighted<Real,TopoD1,Line,ElementXFieldLine>;

//typedef PDEAdvectionDiffusion<PhysD2,
//                              AdvectiveFlux2D_Uniform,
//                              ViscousFlux2D_Uniform,
//                              Source2D_UniformGrad > PDEAdvectionDiffusion2D;
//typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass2D;
//typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldTri;
//template class IntegrandCell_HDG<PDEClass2D>::BasisWeighted_PDE<Real,Real,TopoD2,Triangle,ElementXFieldTri>;
//template class IntegrandCell_HDG<PDEClass2D>::BasisWeighted_AUX<Real,Real,TopoD2,Triangle>;
//template class IntegrandCell_HDG<PDEClass2D>::FieldWeighted<Real,TopoD2,Triangle,ElementXFieldTri>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_VMSD_NL_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VMSD_BasisWeighted_1D_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD1,Line>       ElementQFieldClass;

  typedef ElementXFieldClass::RefCoordType RefCoordType;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  typedef IntegrandCell_VMSD_NL<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line, ElementXFieldClass> BasisWeightedCoarseClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_UniformGrad source(1.0/4.0, 3.0/5.0);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = {x1};
  xfldElem.DOF(1) = {x2};

  // solution

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldClass qpfldElem(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // line solution
  qfldElem.DOF(0) = 2;
  qfldElem.DOF(1) = 3;

  // line solution
  qpfldElem.DOF(0) = 0.11;
  qpfldElem.DOF(1) = 0.07;
  qpfldElem.DOF(2) = -0.03;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // integrand functor
  BasisWeightedCoarseClass fcnCoarse = fcnint.integrand(xfldElem, qfldElem, qpfldElem );

  BOOST_CHECK_EQUAL( 1, fcnCoarse.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcnCoarse.nDOF() );
  BOOST_CHECK_EQUAL( 3, fcnCoarse.nDOFp() );
  BOOST_CHECK( fcnCoarse.needsEvaluation() == true );
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  const int nDOF = 2;
  const int nDOFp = 3;

  RefCoordType sRef;
  Real integrandCoarseTrue[nDOF], integrandFineTrue[nDOFp];

  Real integrandCoarse[nDOF];
  Real integrandFine[nDOFp];

  sRef = {0.5};
  fcnCoarse( sRef, integrandCoarse, 2, integrandFine, 3 );

  //PDE residual integrands:
  integrandCoarseTrue[0] = ( 352./125. ) + ( -6369./3125. )  + ( 76./125. );   // Basis function 1
  integrandCoarseTrue[1] = ( -352./125. ) + ( 6369./3125. )  + ( 76./125. );   // Basis function 2

  SANS_CHECK_CLOSE( integrandCoarseTrue[0], integrandCoarse[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandCoarseTrue[1], integrandCoarse[1], small_tol, close_tol );


  //PDE residual integrands:
  integrandFineTrue[0] = ( 352./125. ) + ( -6369./3125. )  + ( 76./125. );   // Basis function 1
  integrandFineTrue[1] = ( -352./125. ) + ( 6369./3125. )  + ( 76./125. );   // Basis function 2
  integrandFineTrue[2] = ( 0 ) + ( 0 )  + ( 152/125. );   // Basis function 3


  SANS_CHECK_CLOSE( integrandFineTrue[0], integrandFine[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandFineTrue[1], integrandFine[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandFineTrue[2], integrandFine[2], small_tol, close_tol );



//
//  sRef = {1};
//  fcnPDE( sRef, integrandPDE, 2 );
//  fcnAUX( sRef, integrandAUX, 2 );
//
//  //PDE residual integrands: (advective) + (viscous) + (source)
//  integrandPDETrue[0] = ( 22/5. ) + ( -14861/1000. ) + ( 0 );   // Basis function 1
//  integrandPDETrue[1] = ( -22/5. ) + ( 14861/1000. ) + ( 26/5. );   // Basis function 2
//
//  //Auxiliary residual integrands:
//  integrandAUXTrue[0] =  ( -4 );   // Basis function 1
//  integrandAUXTrue[1] =  ( 11 );   // Basis function 2
//
//  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
//
//  SANS_CHECK_CLOSE( integrandAUXTrue[0], integrandAUX[0][0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAUXTrue[1], integrandAUX[1][0], small_tol, close_tol );
//
//
//  sRef = {1./2.};
//  fcnPDE( sRef, integrandPDE, 2 );
//  fcnAUX( sRef, integrandAUX, 2 );
//
//  //PDE residual integrands: (advective) + (viscous) + (source)
//  integrandPDETrue[0] = ( 11/4. ) + ( -19107/2000. ) + ( 133/80. );   // Basis function 1
//  integrandPDETrue[1] = ( -11/4. ) + ( 19107/2000. ) + ( 133/80. );   // Basis function 2
//
//  //Auxiliary residual integrands:
//  integrandAUXTrue[0] =  ( -1/4. );   // Basis function 1
//  integrandAUXTrue[1] =  ( 19/4. );   // Basis function 2
//
//  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
//
//  SANS_CHECK_CLOSE( integrandAUXTrue[0], integrandAUX[0][0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAUXTrue[1], integrandAUX[1][0], small_tol, close_tol );
//
//
//  // test the element integral of the functor
//
//  int quadratureorder = 2;
//  BOOST_REQUIRE_EQUAL(nDOF, qfldElem.nDOF());
//  const int nIntegrand = nDOF;
//  GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedPDEClass::IntegrandType> integralPDE(quadratureorder, nIntegrand);
//  GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedAUXClass::IntegrandType> integralAUX(quadratureorder, nIntegrand);
//
//  BasisWeightedPDEClass::IntegrandType rsdPDEElem[nDOF] = {0,0};
//  BasisWeightedAUXClass::IntegrandType rsdAUXElem[nDOF] = {0,0};
//  Real rsdPDETrue[nDOF], rsdAUXTrue[nDOF];
//
//  // cell integration for canonical element
//  integralPDE( fcnPDE, xfldElem, rsdPDEElem, nIntegrand );
//  integralAUX( fcnAUX, xfldElem, rsdAUXElem, nIntegrand );
//
//  //PDE residual: (advective) + (viscous) + (source)
//  rsdPDETrue[0] = ( 11/4. ) + ( -19107/2000. ) + ( 27/20. );   // Basis function 1
//  rsdPDETrue[1] = ( -11/4. ) + ( 19107/2000. ) + ( 79/40. );   // Basis function 2
//
//  //Auxiliary Variable residual:
//  rsdAUXTrue[0] =  ( -2/3. );   // Basis function 1
//  rsdAUXTrue[1] =  ( 31/6. );   // Basis function 2
//
//  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDEElem[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDEElem[1], small_tol, close_tol );
//
//  SANS_CHECK_CLOSE( rsdAUXTrue[0], rsdAUXElem[0][0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( rsdAUXTrue[1], rsdAUXElem[1][0], small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VMSD_1D_Line_Jacobian_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion1D::template MatrixQ<Real> MatrixQ;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;
  typedef IntegrandCell_VMSD_NL<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line, ElementXFieldClass > BasisWeightedCoarseClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;


  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 0.25, b = 0.6;
  Source1D_UniformGrad source(a, b);

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  IntegrandClass fcnint( pde, {0} );

  for (int qorder = 1; qorder <= 4; qorder++)
  {
    ElementQFieldClass qfldElem(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldClass qpfldElem(qorder+1, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, qfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder+1, qpfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+2, qpfldElem.nDOF() );

    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    for (int dof = 0; dof < qpfldElem.nDOF();dof++)
      qpfldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    BasisWeightedCoarseClass fcnCoarse = fcnint.integrand( xfldElem, qfldElem, qpfldElem );

    const Real small_tol = 1e-11;
    const Real close_tol = 5e-11;
    RefCoordType sRef = Line::centerRef;
    DLA::VectorD<ArrayQ> integrand0(qfldElem.nDOF()), integrand1(qfldElem.nDOF());
    DLA::VectorD<ArrayQ> integrandp0(qpfldElem.nDOF()), integrandp1(qpfldElem.nDOF());

    JacobianElemCell_VMSD<PhysD1,MatrixQ> mtxElem(qfldElem.nDOF(), qpfldElem.nDOF());
    JacobianElemCell_VMSD<PhysD1,MatrixQ> mtxElemTrue(qfldElem.nDOF(), qpfldElem.nDOF());

    // compute jacobians via finite differenec (exact for linear PDE)
    fcnCoarse(sRef, &integrand0[0], integrand0.m(), &integrandp0[0], integrandp0.m());

    for (int i = 0; i < qfldElem.nDOF(); i++)
    {
      qfldElem.DOF(i) += 1;
      fcnCoarse(sRef, &integrand1[0], integrand1.m(), &integrandp1[0], integrandp1.m());
      qfldElem.DOF(i) -= 1;

      for (int j = 0; j < qfldElem.nDOF(); j++)
        mtxElemTrue.PDE_q(j,i) = integrand1[j] - integrand0[j];

      for (int j = 0; j < qpfldElem.nDOF(); j++)
        mtxElemTrue.PDEp_q(j,i) = integrandp1[j] - integrandp0[j];
    }

    for (int i = 0; i < qpfldElem.nDOF(); i++)
    {
      qpfldElem.DOF(i) += 1;
      fcnCoarse(sRef, &integrand1[0], integrand1.m(), &integrandp1[0], integrandp1.m());
      qpfldElem.DOF(i) -= 1;

      for (int j = 0; j < qfldElem.nDOF(); j++)
        mtxElemTrue.PDE_qp(j,i) = integrand1[j] - integrand0[j];

      for (int j = 0; j < qpfldElem.nDOF(); j++)
        mtxElemTrue.PDEp_qp(j,i) = integrandp1[j] - integrandp0[j];
    }


    mtxElem = 0;

    // accumulate the jacobian via Surreal
    fcnCoarse(1., sRef, mtxElem);

    for (int i = 0; i < qfldElem.nDOF(); i++)
      for (int j = 0; j < qfldElem.nDOF(); j++)
        SANS_CHECK_CLOSE( mtxElemTrue.PDE_q(i,j), mtxElem.PDE_q(i,j), small_tol, close_tol );

    for (int i = 0; i < qfldElem.nDOF(); i++)
      for (int j = 0; j < qpfldElem.nDOF(); j++)
        SANS_CHECK_CLOSE( mtxElemTrue.PDE_qp(i,j), mtxElem.PDE_qp(i,j), small_tol, close_tol );

    for (int i = 0; i < qpfldElem.nDOF(); i++)
      for (int j = 0; j < qfldElem.nDOF(); j++)
        SANS_CHECK_CLOSE( mtxElemTrue.PDEp_q(i,j), mtxElem.PDEp_q(i,j), small_tol, close_tol );

    for (int i = 0; i < qpfldElem.nDOF(); i++)
      for (int j = 0; j < qpfldElem.nDOF(); j++)
        SANS_CHECK_CLOSE( mtxElemTrue.PDEp_qp(i,j), mtxElem.PDEp_qp(i,j), small_tol, close_tol );
  }
}



//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VMSD_Basis_Field_Weight_Equal_1D_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
//  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD1,Line>       ElementQFieldClass;

  typedef IntegrandCell_VMSD_NL<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line, ElementXFieldClass > BasisWeightedCoarseClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD1,Line,ElementXFieldClass> FieldWeightedCoarseClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3;
  Source1D_Uniform source(a);

  PDEClass pde( adv, visc , source );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  Element<Real,TopoD1,Line> efldElem( 0, BasisFunctionCategory_Legendre );
  Element<Real,TopoD1,Line> epfldElem( 0, BasisFunctionCategory_Legendre );

  for (int qorder = 1; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldClass qfldElem( qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldClass qpfldElem(1, BasisFunctionCategory_Hierarchical);
    ElementQFieldClass wfldElem( qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldClass wpfldElem(1, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder,   qfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   wfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, wfldElem.nDOF() );
    BOOST_CHECK_EQUAL( 1,   qpfldElem.order() );
    BOOST_CHECK_EQUAL( 2, qpfldElem.nDOF() );
    BOOST_CHECK_EQUAL( 1,   wpfldElem.order() );
    BOOST_CHECK_EQUAL( 2, wpfldElem.nDOF() );

    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
    {
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);
      wfldElem.DOF(dof) = 0;
    }

    for (int dof = 0; dof < qpfldElem.nDOF();dof++)
    {
      qpfldElem.DOF(dof) = (dof+1)*pow(-1,dof);
      wpfldElem.DOF(dof) = 0;
    }

    IntegrandClass fcnint( pde, {0} );

    BasisWeightedCoarseClass fcnB_Coarse = fcnint.integrand( xfldElem, qfldElem, qpfldElem );

    FieldWeightedCoarseClass fcnW_Coarse = fcnint.integrand( xfldElem, qfldElem, qpfldElem, wfldElem, wpfldElem, efldElem );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;
    std::vector<ArrayQ> rsdPDEElemB(qfldElem.nDOF(), 0);
    std::vector<ArrayQ> rsdPDEpElemB(qpfldElem.nDOF(), 0);

    std::vector<Real> rsdPDEElemW(efldElem.nDOF(), 0);

    //COARSE SCALE FIRST
    int quadratureorder = 2;
    int nIntegrand = qfldElem.nDOF(); int npIntegrand = qpfldElem.nDOF();
    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integralB(quadratureorder, nIntegrand, npIntegrand);

    // cell integration for canonical element
    integralB( fcnB_Coarse, xfldElem, rsdPDEElemB.data(), nIntegrand, rsdPDEpElemB.data(), npIntegrand );

    GalerkinWeightedIntegral<TopoD1, Line, Real> integralW(quadratureorder, efldElem.nDOF() );

    for (int i = 0; i < wfldElem.nDOF(); i++)
    {
      wfldElem.DOF(i) = 1;

      rsdPDEElemW[0] = 0;
      integralW( fcnW_Coarse, xfldElem, rsdPDEElemW.data(), rsdPDEElemW.size() );

      // efld same order as qfld with a wfld of 1 gives the basis weighted function
      SANS_CHECK_CLOSE( rsdPDEElemW[0], rsdPDEElemB[i], small_tol, close_tol );

      wfldElem.DOF(i) = 0;

    }

    for (int i = 0; i < wpfldElem.nDOF(); i++)
    {
      wpfldElem.DOF(i) = 1;

      rsdPDEElemW[0] = 0;
      integralW( fcnW_Coarse, xfldElem, rsdPDEElemW.data(), rsdPDEElemW.size() );

      // efld same order as qfld with a wfld of 1 gives the basis weighted function
      SANS_CHECK_CLOSE( rsdPDEElemW[0], rsdPDEpElemB[i], small_tol, close_tol );

      wpfldElem.DOF(i) = 0;

    }

  }

}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_BasisWeighted_2D_Triangle_test )
{
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementAFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_HDG<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,Real,TopoD2,Triangle,ElementParam> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_AUX<Real,Real,TopoD2,Triangle> BasisWeightedAUXClass;

  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_UniformGrad source(0.25, 0.6, -1.5);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  // solution

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  // auxiliary variable

  ElementAFieldClass afldElem(order, BasisFunctionCategory_Hierarchical);

  afldElem.DOF(0) = { 2, -3};
  afldElem.DOF(1) = { 7,  8};
  afldElem.DOF(2) = {-1, -5};

  // HDG discretization (not used)
  DiscretizationClass disc( pde );

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  BasisWeightedPDEClass fcnPDE = fcnint.integrand_PDE( xfldElem, qfldElem, afldElem );
  BasisWeightedAUXClass fcnAUX = fcnint.integrand_AUX( xfldElem, qfldElem, afldElem );

  BOOST_CHECK_EQUAL( 1, fcnPDE.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcnPDE.nDOF() );
  BOOST_CHECK( fcnPDE.needsEvaluation() == true );
  BOOST_CHECK_EQUAL( 1, fcnAUX.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcnAUX.nDOF() );
  BOOST_CHECK( fcnAUX.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordType sRef;

  const int nDOF = 3;
  Real integrandPDETrue[nDOF], integrandAUXxTrue[nDOF], integrandAUXyTrue[nDOF];
  BasisWeightedPDEClass::IntegrandType integrandPDE[nDOF];
  BasisWeightedAUXClass::IntegrandType integrandAUX[nDOF];

  // Test at {0, 0}
  sRef = {0, 0};
  fcnPDE( sRef, integrandPDE, 3 );
  fcnAUX( sRef, integrandAUX, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 13/10. ) + ( -143/125. ) + ( 119/20. );   // Basis function 1
  integrandPDETrue[1] = ( -11/10. ) + ( 2587/1000. ) + ( 0 );   // Basis function 2
  integrandPDETrue[2] = ( -1/5. ) + ( -1443/1000. ) + ( 0 );   // Basis function 3

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );

  //AUX-x residual integrands:
  integrandAUXxTrue[0] = ( 1 );   // Basis function 1
  integrandAUXxTrue[1] = ( 1 );   // Basis function 2
  integrandAUXxTrue[2] = ( 0 );   // Basis function 3

  //AUX-y residual integrands:
  integrandAUXyTrue[0] = ( -4 );   // Basis function 1
  integrandAUXyTrue[1] = ( 0 );   // Basis function 2
  integrandAUXyTrue[2] = ( 1 );   // Basis function 3

  SANS_CHECK_CLOSE( integrandAUXxTrue[0], integrandAUX[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[1], integrandAUX[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[2], integrandAUX[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[0], integrandAUX[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[1], integrandAUX[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[2], integrandAUX[2][1], small_tol, close_tol );


  // Test at {0, 1}
  sRef = {0, 1};
  fcnPDE( sRef, integrandPDE, 3 );
  fcnAUX( sRef, integrandAUX, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 26/5. ) + ( 1339/125. ) + ( 0 );   // Basis function 1
  integrandPDETrue[1] = ( -22/5. ) + ( -611/125. ) + ( 0 );   // Basis function 2
  integrandPDETrue[2] = ( -4/5. ) + ( -728/125. ) + ( 79/10. );   // Basis function 3

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );

  //AUX-x residual integrands:
  integrandAUXxTrue[0] = ( -4 );   // Basis function 1
  integrandAUXxTrue[1] = ( 4 );   // Basis function 2
  integrandAUXxTrue[2] = ( -1 );   // Basis function 3

  //AUX-y residual integrands:
  integrandAUXyTrue[0] = ( -4 );   // Basis function 1
  integrandAUXyTrue[1] = ( 0 );   // Basis function 2
  integrandAUXyTrue[2] = ( -1 );   // Basis function 3

  SANS_CHECK_CLOSE( integrandAUXxTrue[0], integrandAUX[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[1], integrandAUX[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[2], integrandAUX[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[0], integrandAUX[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[1], integrandAUX[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[2], integrandAUX[2][1], small_tol, close_tol );


  // Test at {1, 0}
  sRef = {1, 0};
  fcnPDE( sRef, integrandPDE, 3 );
  fcnAUX( sRef, integrandAUX, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 39/10. ) + ( -4108/125. ) + ( 0 );   // Basis function 1
  integrandPDETrue[1] = ( -33/10. ) + ( 3857/200. ) + ( -141/20. );   // Basis function 2
  integrandPDETrue[2] = ( -3/5. ) + ( 13579/1000. ) + ( 0 );   // Basis function 3

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );

  //AUX-x residual integrands:
  integrandAUXxTrue[0] = ( -3 );   // Basis function 1
  integrandAUXxTrue[1] = ( 10 );   // Basis function 2
  integrandAUXxTrue[2] = ( 0 );   // Basis function 3

  //AUX-y residual integrands:
  integrandAUXyTrue[0] = ( -3 );   // Basis function 1
  integrandAUXyTrue[1] = ( 8 );   // Basis function 2
  integrandAUXyTrue[2] = ( 3 );   // Basis function 3

  SANS_CHECK_CLOSE( integrandAUXxTrue[0], integrandAUX[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[1], integrandAUX[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[2], integrandAUX[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[0], integrandAUX[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[1], integrandAUX[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[2], integrandAUX[2][1], small_tol, close_tol );


  // Test at {1/3, 1/3}
  sRef = {1/3., 1/3.};
  fcnPDE( sRef, integrandPDE, 3 );
  fcnAUX( sRef, integrandAUX, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 52/15. ) + ( -2912/375. ) + ( 34/45. );   // Basis function 1
  integrandPDETrue[1] = ( -44/15. ) + ( 2123/375. ) + ( 34/45. );   // Basis function 2
  integrandPDETrue[2] = ( -8/15. ) + ( 263/125. ) + ( 34/45. );   // Basis function 3

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );

  //AUX-x residual integrands:
  integrandAUXxTrue[0] = ( -16/9. );   // Basis function 1
  integrandAUXxTrue[1] = ( 32/9. );   // Basis function 2
  integrandAUXxTrue[2] = ( 8/9. );   // Basis function 3

  //AUX-y residual integrands:
  integrandAUXyTrue[0] = ( -8/3. );   // Basis function 1
  integrandAUXyTrue[1] = ( 0 );   // Basis function 2
  integrandAUXyTrue[2] = ( 8/3. );   // Basis function 3

  SANS_CHECK_CLOSE( integrandAUXxTrue[0], integrandAUX[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[1], integrandAUX[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[2], integrandAUX[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[0], integrandAUX[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[1], integrandAUX[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[2], integrandAUX[2][1], small_tol, close_tol );

  // test the element integral of the functor

  int quadratureorder = 2;
  BOOST_REQUIRE_EQUAL(nDOF, qfldElem.nDOF());
  const int nIntegrand = nDOF;
  GalerkinWeightedIntegral<TopoD2, Triangle, BasisWeightedPDEClass::IntegrandType> integralPDE(quadratureorder, nIntegrand);
  GalerkinWeightedIntegral<TopoD2, Triangle, BasisWeightedAUXClass::IntegrandType> integralAUX(quadratureorder, nIntegrand);

  BasisWeightedPDEClass::IntegrandType rsdPDEElem[nDOF] = {0,0,0};
  BasisWeightedAUXClass::IntegrandType rsdAUXElem[nDOF] = {0,0,0};

  // cell integration for canonical element
  integralPDE( fcnPDE, xfldElem, rsdPDEElem, nIntegrand );
  integralAUX( fcnAUX, xfldElem, rsdAUXElem, nIntegrand );

  Real rsdPDE[nDOF], rsdAUXx[nDOF], rsdAUXy[nDOF];

  //PDE residual: (advection) + (diffusion) + (source)
  rsdPDE[0] = (26/15.) + (-1456/375.) + (17/32.);   // Basis function 1
  rsdPDE[1] = (-22/15.) + (2123/750.) + (-1/96.);   // Basis function 2
  rsdPDE[2] = (-4/15.) + (263/250.) + (49/80.);   // Basis function 3

  SANS_CHECK_CLOSE( rsdPDE[0], rsdPDEElem[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDE[1], rsdPDEElem[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDE[2], rsdPDEElem[2], small_tol, close_tol );

  //AUX-x residual:
  rsdAUXx[0] = (-11/12.);   // Basis function 1
  rsdAUXx[1] = (47/24.);   // Basis function 2
  rsdAUXx[2] = (7/24.);   // Basis function 3

  //AUX-y residual:
  rsdAUXy[0] = (-35/24.);   // Basis function 1
  rsdAUXy[1] = (1/3.);   // Basis function 2
  rsdAUXy[2] = (9/8.);   // Basis function 3

  SANS_CHECK_CLOSE( rsdAUXx[0], rsdAUXElem[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXx[1], rsdAUXElem[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXx[2], rsdAUXElem[2][0], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdAUXy[0], rsdAUXElem[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXy[1], rsdAUXElem[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXy[2], rsdAUXElem[2][1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_FieldWeighted_2D_Triangle_test )
{
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;

  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementAFieldClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_HDG<PDEClass> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD2,Triangle,ElementParam> FieldWeightedClass;

  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_UniformGrad source(0.25, 0.6, -1.5);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  // solution

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  // auxiliary variable

  ElementAFieldClass afldElem(order, BasisFunctionCategory_Hierarchical);

  afldElem.DOF(0) = { 2, -3};
  afldElem.DOF(1) = { 7,  8};
  afldElem.DOF(2) = {-1, -5};

  // weight

  ElementQFieldClass wfldElem(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElem.order() );
  BOOST_CHECK_EQUAL( 6, wfldElem.nDOF() );

  // triangle solution
  wfldElem.DOF(0) = -2;
  wfldElem.DOF(1) =  4;
  wfldElem.DOF(2) =  3;
  wfldElem.DOF(3) =  2;
  wfldElem.DOF(4) =  4;
  wfldElem.DOF(5) = -1;

  // auxiliary variable

  ElementAFieldClass bfldElem(order+1, BasisFunctionCategory_Hierarchical);

  bfldElem.DOF(0) = { 1,  5};
  bfldElem.DOF(1) = { 3,  6};
  bfldElem.DOF(2) = {-1,  7};
  bfldElem.DOF(3) = { 3,  1};
  bfldElem.DOF(4) = { 2,  2};
  bfldElem.DOF(5) = { 4,  3};

  // HDG discretization (not used)
  DiscretizationClass disc( pde );

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  FieldWeightedClass fcn = fcnint.integrand(xfldElem, qfldElem, afldElem, wfldElem, bfldElem );

  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordType sRef;
  Real integrandPDETrue, integrandAUXTrue;
  FieldWeightedClass::IntegrandType integrand;

  // Test at {0, 0}
  sRef = {0, 0};
  fcn( sRef, integrand );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue = ( -32/5. ) + ( -25129/1000. ) + ( -119/10. );   // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //AUX residual integrands:
  integrandAUXTrue = ( 15 );   // Weight function

  SANS_CHECK_CLOSE( integrandAUXTrue, integrand.Au, small_tol, close_tol );

  // Test at {1, 0}
  sRef = {1, 0};
  fcn( sRef, integrand );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue = ( -216/5. ) + ( 423693/1000. ) + ( -141/5. );   // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //AUX residual integrands:
  integrandAUXTrue = ( 9 );   // Weight function

  SANS_CHECK_CLOSE( integrandAUXTrue, integrand.Au, small_tol, close_tol );

  // Test at {0, 1}
  sRef = {0, 1};
  fcn( sRef, integrand );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue = ( 88/5. ) + ( 1846/25. ) + ( 237/10. );   // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //AUX residual integrands:
  integrandAUXTrue = ( -34 );   // Weight function

  SANS_CHECK_CLOSE( integrandAUXTrue, integrand.Au, small_tol, close_tol );

  // Test at {1/3, 1/3}
  sRef = {1./3., 1./3.};
  fcn( sRef, integrand );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue = ( -656/45. ) + ( 42533/1125. ) + ( 238/27. );   // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //AUX residual integrands:
  integrandAUXTrue = ( 184/9. );   // Weight function

  SANS_CHECK_CLOSE( integrandAUXTrue, integrand.Au, small_tol, close_tol );

  // Test at {1/5, 1/2}
  sRef = {1./5., 1./2.};
  fcn( sRef, integrand );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue = ( -377/50. ) + ( 43017/50000. ) + ( 40309/2000. );   // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //AUX residual integrands:
  integrandAUXTrue = ( 383/125. );   // Weight function

  SANS_CHECK_CLOSE( integrandAUXTrue, integrand.Au, small_tol, close_tol );

  // Test at {1/3, 1/4}
  sRef = {1./3., 1./4.};
  fcn( sRef, integrand );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue = ( -5539/360. ) + ( 233317/4500. ) + ( 11009/1728. );   // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //AUX residual integrands:
  integrandAUXTrue = ( 1553/54. );   // Weight function

  SANS_CHECK_CLOSE( integrandAUXTrue, integrand.Au, small_tol, close_tol );

  // test the element integral of the functor

  int quadratureorder = 4;
  ElementIntegral<TopoD2, Triangle, FieldWeightedClass::IntegrandType> integral(quadratureorder);

  FieldWeightedClass::IntegrandType rsdElem;

  // cell integration for canonical element
  integral( fcn, xfldElem, rsdElem );

  Real rsdPDETrue, rsdAUXTrue;

  //PDE residual: (advection) + (diffusion) + (source)
  rsdPDETrue = (-34/5.) + (203167/6000.) + (571/150.);   // Weight function

  SANS_CHECK_CLOSE( rsdPDETrue, rsdElem.PDE, small_tol, close_tol );

  //AUX residual:
  rsdAUXTrue = (91/12.);   // Weight function

  SANS_CHECK_CLOSE( rsdAUXTrue, rsdElem.Au, small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_Basis_Field_Weight_Equal_2D_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;

  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementAFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_HDG<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,Real,TopoD2,Triangle,ElementParam> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_AUX<Real,Real,TopoD2,Triangle> BasisWeightedAUXClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD2,Triangle,ElementParam> FieldWeightedClass;

  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_UniformGrad source(0.25, 0.6, -1.5);

  PDEClass pde( adv, visc, source );

  // grid

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = -0.1;  y1 = -0.05;
  x2 =    1;  y2 =     0;
  x3 =  0.2;  y3 =     1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  for (int qorder = 2; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldClass qfldElem( qorder, BasisFunctionCategory_Hierarchical);
    ElementAFieldClass afldElem(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldClass wfldElem( qorder, BasisFunctionCategory_Hierarchical);
    ElementAFieldClass bfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qfldElem.nDOF(), afldElem.nDOF() );
    BOOST_CHECK_EQUAL( qfldElem.nDOF(), wfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qfldElem.nDOF(), bfldElem.nDOF() );

    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
    {
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);
      wfldElem.DOF(dof) = 0;
      afldElem.DOF(dof) = (dof+1)*pow(-1,dof);
      bfldElem.DOF(dof) = 0;
    }

    // HDG discretization (not used)
    DiscretizationClass disc( pde );

    IntegrandClass fcnint( pde, disc, {0} );

    BasisWeightedPDEClass fcnB_PDE = fcnint.integrand_PDE( xfldElem, qfldElem, afldElem );
    BasisWeightedAUXClass fcnB_AUX = fcnint.integrand_AUX( xfldElem, qfldElem, afldElem );
    FieldWeightedClass fcnW = fcnint.integrand( xfldElem, qfldElem, afldElem, wfldElem, bfldElem );

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;
    const int nIntegrand = qfldElem.nDOF();
    FieldWeightedClass::IntegrandType rsdElemW = 0;
    std::vector<BasisWeightedPDEClass::IntegrandType> rsdElemB_PDE(nIntegrand,0); // trickery to get round the POD warning
    std::vector<BasisWeightedAUXClass::IntegrandType> rsdElemB_AUX(nIntegrand,0); // trickery to get round the POD warning

    int quadratureorder = 2*qorder;
    GalerkinWeightedIntegral<TopoD2, Triangle, BasisWeightedPDEClass::IntegrandType> integralB_PDE(quadratureorder, nIntegrand);
    GalerkinWeightedIntegral<TopoD2, Triangle, BasisWeightedAUXClass::IntegrandType> integralB_AUX(quadratureorder, nIntegrand);

    // cell integration for canonical element
    integralB_PDE( fcnB_PDE, xfldElem, rsdElemB_PDE.data(), nIntegrand );
    integralB_AUX( fcnB_AUX, xfldElem, rsdElemB_AUX.data(), nIntegrand );

    ElementIntegral<TopoD2, Triangle, FieldWeightedClass::IntegrandType> integralW(quadratureorder);

    for (int i = 0; i < wfldElem.nDOF(); i++) // testing the wfld
    {
      // set just one of the weights to one
      wfldElem.DOF(i) = 1;
      bfldElem.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemW = 0;
      integralW( fcnW, xfldElem, rsdElemW );

      // test the the two integrands are identical
      SANS_CHECK_CLOSE( rsdElemW.PDE, rsdElemB_PDE[i], small_tol, close_tol );

      Real tmp = 0;
      for (int D = 0; D < PhysD2::D; D++ )
        tmp += rsdElemB_AUX[i][D];

      SANS_CHECK_CLOSE( rsdElemW.Au, tmp, small_tol, close_tol )

      // reset to zero
      wfldElem.DOF(i) = 0;
      bfldElem.DOF(i) = 0;
    }
  }
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
