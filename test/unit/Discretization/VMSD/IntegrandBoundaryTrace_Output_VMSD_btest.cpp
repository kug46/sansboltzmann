// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandBoundaryTrace_DGBR2_sansLG_Output_btest
// testing of boundary output integrand routines for DGBR2 sansLG

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_EG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/BCEuler2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/OutputEuler2D.h"

#include "Discretization/VMSD/IntegrandBoundaryTrace_Flux_mitState_VMSD.h"
#include "Discretization/VMSD/IntegrandBoundaryTrace_OutputWeightRsd_VMSD.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandBoundaryTrace_OutputWeightRsd_VMSD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IntegrandTest_Euler_Drag_P1_2D_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template ArrayJ<Real> ArrayJ;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)

  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Raw );

  //Elements
  typedef ElementXField<PhysD2, TopoD2, Triangle> ElementXFieldCell;
  typedef ElementXField<PhysD2, TopoD1, Line> ElementXFieldTrace;
  typedef Element<ArrayQ, TopoD2, Triangle> ElementQFieldType;
  typedef ElementXFieldTrace::RefCoordType RefCoordType;

  // WALL BC INTEGRAND
  typedef BCEuler2D<BCTypeSymmetry_mitState, PDEClass> BCClass;
  typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, VMSD> BCIntegrandBoundaryTrace;

  NDBCClass bc(pde);

  int order = 1;
  DiscretizationVMSD stab;
  BCIntegrandBoundaryTrace bcintegrand( pde, bc, {1}, stab );

  // DRAG INTEGRAND
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, VMSD> IntegrandOutputClass;

  NDOutputClass outputFcn(pde, 1., 0.);
  IntegrandOutputClass output_integrand( outputFcn, {1} );


  typedef IntegrandOutputClass::Functor<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell,BCIntegrandBoundaryTrace> FunctorClass;

  // grid

  ElementXFieldCell xfldElemCell(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemCell.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemCell.nDOF() );

  // triangle grid
  xfldElemCell.DOF(0) = {0, 0};
  xfldElemCell.DOF(1) = {1, 0};
  xfldElemCell.DOF(2) = {0, 1};

  ElementXFieldTrace xfldElemTrace(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemTrace.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemTrace.nDOF() );

  // trace grid
  xfldElemTrace.DOF(0) = {1, 0};
  xfldElemTrace.DOF(1) = {0, 1};

  CanonicalTraceToCell canonicalTrace(0,1);

  // solution: P1

  order = 1;
  ElementQFieldType qfldElem(order, BasisFunctionCategory_Hierarchical);

  order = 1;
  ElementQFieldType qpfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = {1.0, 0.0, 0.0, 1.0};
  qfldElem.DOF(1) = {1.0, 0.0, 0.0, 1.0};
  qfldElem.DOF(2) = {1.0, 0.0, 0.0, 1.0};

  // triangle solution
  qpfldElem.DOF(0) = {0.0, 0.0, 0.0, 0.0};
  qpfldElem.DOF(1) = {0.0, 0.0, 0.0, 0.0};
  qpfldElem.DOF(2) = {0.0, 0.0, 0.0, 0.0};

  //Weigts
  ElementQFieldType wfldElem( 0, BasisFunctionCategory_Legendre );
  wfldElem.DOF(0) = 1;

  FunctorClass fcn = output_integrand.integrand(bcintegrand,
                                                xfldElemTrace, canonicalTrace,
                                                xfldElemCell, qfldElem, qpfldElem);

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordType sRef;
  ArrayJ integrandTrue;
  ArrayJ integrand =0;

  sRef = 0;
  integrandTrue = sqrt(2)/2;
  fcn( sRef, integrand );
  SANS_CHECK_CLOSE( integrandTrue, integrand, small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
