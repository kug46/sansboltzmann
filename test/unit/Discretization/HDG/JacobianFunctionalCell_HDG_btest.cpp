// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianFunctionalCell_HDG_btest
// testing of 2-D functional area-integral jacobian

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/list.hpp>

#include "tools/SANSnumerics.h"     // Real

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime1D.h"
#include "pde/OutputCell_WeightedSolution.h"

#include "pde/PorousMedia/TraitsTwoPhase.h"
#include "pde/PorousMedia/Q1DPrimitive_pnSw.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/OutputTwoPhase.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/HDG/IntegrandCell_HDG_Output.h"
#include "Discretization/HDG/FunctionalCell_HDG.h"
#include "Discretization/HDG/JacobianFunctionalCell_HDG.h"

#include "Surreal/SurrealS.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianFunctionalCell_HDG_test_suite )

template<class PhysDim_>
struct DummyPDEReal
{
  typedef PhysDim_ PhysDim;

  template<class T>
  using ArrayQ = T;

  template<class T>
  using MatrixQ = T;
};

template<class PhysDim_>
struct DummyPDEVector
{
  typedef PhysDim_ PhysDim;

  template<class T>
  using ArrayQ = DLA::VectorS<2,T>;

  template<class T>
  using MatrixQ = DLA::MatrixS<2,2,T>;
};


typedef boost::mpl::list< DummyPDEReal<PhysD1>,
                          DummyPDEVector<PhysD1> >  DummyPDEs_PhysD1;

typedef boost::mpl::list< DummyPDEReal<PhysD2>,
                          DummyPDEVector<PhysD2> >  DummyPDEs_PhysD2;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Line_P0_Const_test, DummyPDE, DummyPDEs_PhysD1 )
{
  typedef SurrealS<1> SurrealClass;

  typedef ScalarFunction1D_Const WeightFcn; // Change this to change weighting type
  typedef OutputCell_WeightedSolution<DummyPDE,WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDOutputClass::template ArrayJ<Real> ArrayJ;
  typedef typename NDOutputClass::template MatrixJ<Real> MatrixJ;

  typedef IntegrandCell_HDG_Output<NDOutputClass> IntegrandClass;

  Real a0 = 1.123;//,a1=1.2,a2=1.3;
  WeightFcn weightFcn(a0);

  // grid: single triangle, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 0;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 1, qfld.nDOF() );

  // solution data
  qfld.DOF(0) = 1.1;

  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Legendre);

  afld.DOF(0) = -1;

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder = 1;

  // integrand
  NDOutputClass fcnOutput(weightFcn);
  IntegrandClass integrand( fcnOutput, {0} );

  // Via Surreals

  DLA::VectorD<MatrixJ> jacFunctional( qfld.nDOF() );
  jacFunctional=0;

  IntegrateCellGroups<TopoD1>::integrate(
      JacobianFunctionalCell_HDG<SurrealClass>( integrand, jacFunctional ),
      xfld, (qfld, afld), &quadratureOrder, 1 );

  ArrayJ functional0 = 0;
  IntegrateCellGroups<TopoD1>::integrate(
      FunctionalCell_HDG( integrand, functional0 ), xfld, (qfld, afld), &quadratureOrder, 1 );

  // Via FD
  for (int i = 0; i < qfld.nDOF(); i++)
  {
    MatrixJ fdjacFunctional = 0;

    for (int n = 0; n < DLA::VectorSize<ArrayQ>::M; n++)
    {
      DLA::index(qfld.DOF(i),n) += 1;

      ArrayJ functional1 = 0;
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell_HDG( integrand, functional1 ), xfld, (qfld, afld), &quadratureOrder, 1 );

      DLA::index(qfld.DOF(i),n) -= 1;

      for (int m = 0; m < DLA::VectorSize<ArrayJ>::M; m++)
          DLA::index(fdjacFunctional,m,n) = DLA::index(functional1,m) - DLA::index(functional0,m);
    }

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;
    for (int m = 0; m < DLA::VectorSize<ArrayJ>::M; m++)
      for (int n = 0; n < DLA::VectorSize<ArrayQ>::M; n++)
        SANS_CHECK_CLOSE( DLA::index(fdjacFunctional,m,n), DLA::index(jacFunctional[i],m,n), small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Triangle_P0_Linear_test, DummyPDE, DummyPDEs_PhysD2 )
{
  typedef SurrealS<1> SurrealClass;

  typedef ScalarFunction2D_Linear WeightFcn; // Change this to change weighting type
  typedef OutputCell_WeightedSolution<DummyPDE,WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDOutputClass::template ArrayJ<Real> ArrayJ;
  typedef typename NDOutputClass::template MatrixJ<Real> MatrixJ;

  typedef IntegrandCell_HDG_Output<NDOutputClass> IntegrandClass;

  Real a0 = 1.123, a1 = 1.231, a2 = 1.312;
  WeightFcn weightFcn(a0,a1,a2);

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 0;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 1, qfld.nDOF() );

  // solution data
  qfld.DOF(0) = 1.0;

  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Legendre);

  afld.DOF(0) = -1;

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder = 1;

  // integrand
  NDOutputClass fcnOutput(weightFcn);
  IntegrandClass integrand( fcnOutput, {0} );

  // Via Surreals

  DLA::VectorD<MatrixJ> jacFunctional( qfld.nDOF() );
  jacFunctional = 0;

  IntegrateCellGroups<TopoD2>::integrate(
      JacobianFunctionalCell_HDG<SurrealClass>( integrand, jacFunctional ), xfld, (qfld, afld), &quadratureOrder, 1 );

  // Via FD
  ArrayJ functional0 = 0;
  IntegrateCellGroups<TopoD2>::integrate(
      FunctionalCell_HDG( integrand, functional0 ), xfld, (qfld, afld), &quadratureOrder, 1 );

  for (int i = 0; i < qfld.nDOF(); i++)
  {
    MatrixJ fdjacFunctional = 0;

    for (int n = 0; n < DLA::VectorSize<ArrayQ>::M; n++)
    {
      DLA::index(qfld.DOF(i),n) += 1;

      ArrayJ functional1 = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_HDG( integrand, functional1 ), xfld, (qfld, afld), &quadratureOrder, 1 );

      DLA::index(qfld.DOF(i),n) -= 1;

      for (int m = 0; m < DLA::VectorSize<ArrayJ>::M; m++)
          DLA::index(fdjacFunctional,m,n) = DLA::index(functional1,m) - DLA::index(functional0,m);
    }

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;
    for (int m = 0; m < DLA::VectorSize<ArrayJ>::M; m++)
      for (int n = 0; n < DLA::VectorSize<ArrayQ>::M; n++)
        SANS_CHECK_CLOSE( DLA::index(fdjacFunctional,m,n), DLA::index(jacFunctional[i],m,n), small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Triangle_P1_Linear_test, DummyPDE, DummyPDEs_PhysD2 )
{
  typedef SurrealS<1> SurrealClass;

  typedef ScalarFunction2D_Linear WeightFcn; // Change this to change weighting type
  typedef OutputCell_WeightedSolution<DummyPDE,WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDOutputClass::template ArrayJ<Real> ArrayJ;
  typedef typename NDOutputClass::template MatrixJ<Real> MatrixJ;

  typedef IntegrandCell_HDG_Output<NDOutputClass> IntegrandClass;

  Real a0 = 1.4, a1 = 0.0, a2 = 0.0;
  WeightFcn weightFcn(a0,a1,a2);

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 3, qfld.nDOF() );

  // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  afld.DOF(0) = -1;  afld.DOF(1) = 5;  afld.DOF(2) = 2;

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder = 2;

  // integrand
  NDOutputClass fcnOutput(weightFcn);
  IntegrandClass integrand( fcnOutput, {0} );

  // Via Surreals

  DLA::VectorD<MatrixJ> jacFunctional( qfld.nDOF() );
  jacFunctional=0;

  IntegrateCellGroups<TopoD2>::integrate(
      JacobianFunctionalCell_HDG<SurrealClass>( integrand, jacFunctional ), xfld, (qfld, afld), &quadratureOrder, 1 );

  // Via FD
  MatrixJ fdjacFunctional[3] = {};
  ArrayJ functional0 = 0;

  IntegrateCellGroups<TopoD2>::integrate(
      FunctionalCell_HDG( integrand, functional0 ), xfld, (qfld, afld), &quadratureOrder, 1 );

  for (int i = 0; i < 3; i++)
  {
    for (int n = 0; n < DLA::VectorSize<ArrayQ>::M; n++)
    {
      DLA::index(qfld.DOF(i),n) += 1;  // increment DOF by 1

      ArrayJ functional1 = 0.;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_HDG( integrand, functional1 ), xfld, (qfld, afld), &quadratureOrder, 1 );

      DLA::index(qfld.DOF(i),n) -= 1;  // resetting the DOF

      // (f(x + dx) - f(x))/dx where dx = 1
      for (int m = 0; m < DLA::VectorSize<ArrayJ>::M; m++)
        DLA::index(fdjacFunctional[i],m,n) = DLA::index(functional1,m) - DLA::index(functional0,m);
    }
  }

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int i = 0; i < 3; i++)
  {
    for (int m = 0; m < DLA::VectorSize<ArrayJ>::M; m++)
    {
      for (int n = 0; n < DLA::VectorSize<ArrayQ>::M; n++)
      {
        if ( m == n)
        {
          SANS_CHECK_CLOSE( a0/6, DLA::index(jacFunctional[i],m,n),  small_tol, close_tol );
          SANS_CHECK_CLOSE( a0/6, DLA::index(fdjacFunctional[i],m,n), small_tol, close_tol );
        }
        SANS_CHECK_CLOSE( DLA::index(fdjacFunctional[i],m,n), DLA::index(jacFunctional[i],m,n), small_tol, close_tol );
      }
    }
  }
}
// Need PDE available in the output functional
#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_P1_Nonlinear_test )
{
  typedef SurrealS<1> SurrealClass;

  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef Q1D<QTypePrimitive_pnSw, CapillaryModel, TraitsSizeTwoPhase> QInterpreter;

  typedef OutputTwoPhase1D_FixedWellPressure<QInterpreter, DensityModel, DensityModel,
                                             RelPermModel, RelPermModel, ViscModel, ViscModel, CapillaryModel> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_HDG_Output<NDOutputClass> OutputIntegrandClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDOutputClass::template ArrayJ<Real> ArrayJ;
  typedef typename NDOutputClass::template MatrixJ<Real> MatrixJ;

  const Real L = 2000.0; //ft
  const Real T = 1000.0; //days
  const Real Ls = 10.0;  //ft - well size

  // PDE
  const Real pref = 14.7;

  DensityModel rhow(62.4, 5.0e-6, pref);
  DensityModel rhon(52.1, 1.5e-5, pref);

  PorosityModel phi(0.3, 3.0e-6, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  CapillaryModel pc(5);
  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;

  // Set up source term PyDicts
  PyDict well_fixedpressure;
  well_fixedpressure[SourceTwoPhase1DType_FixedWellPressure_Param::params.pB] = 2350.0;
  well_fixedpressure[SourceTwoPhase1DParam::params.Source.SourceType] = SourceTwoPhase1DParam::params.Source.FixedPressure;

  PyDict source_well;
  source_well[SourceTwoPhase1DParam::params.Source] = well_fixedpressure;
  source_well[SourceTwoPhase1DParam::params.xmin] = 0.5*(L - Ls);
  source_well[SourceTwoPhase1DParam::params.xmax] = 0.5*(L + Ls);
  source_well[SourceTwoPhase1DParam::params.Tmin] = 0;
  source_well[SourceTwoPhase1DParam::params.Tmax] = T;
  source_well[SourceTwoPhase1DParam::params.smoothLx] = 5.0;
  source_well[SourceTwoPhase1DParam::params.smoothT] = 0.0;

  NDOutputClass fcnOutput(source_well, rhow, rhon, krw, krn, muw, mun, K, pc);
  OutputIntegrandClass integrand(fcnOutput, {0});

  // grid:
  std::vector<Real> xvec = {0, 500, 995, 1000, 1005, 1500, 2000};
  std::vector<Real> yvec = {0, 500, 1000};

  XField2D_Box_Triangle_X1 xfld(xvec, yvec, true);

  BOOST_CHECK_EQUAL( 7*3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 24, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 72, qfld.nDOF() );

  // solution data
  ArrayQ qinit = {2400, 0.5};
  qfld = qinit;

  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  afld = 0.0;

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder = 2;

  DLA::VectorD<MatrixJ> jacFunctional( qfld.nDOF() );
  jacFunctional = 0;

  IntegrateCellGroups<TopoD2>::integrate(
      JacobianFunctionalCell_HDG<SurrealClass>( integrand, jacFunctional ), xfld, (qfld, afld), &quadratureOrder, 1 );

  // Via FD
  Real step = 0.01;

  for (int i = 0; i < qfld.nDOF(); i++)
  {
    MatrixJ fdjacFunctional = 0;

    for (int n = 0; n < DLA::VectorSize<ArrayQ>::M; n++)
    {
      qfld.DOF(i)[n] += step;

      ArrayJ functionalp = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_HDG( integrand, functionalp ), xfld, (qfld, afld), &quadratureOrder, 1 );

      qfld.DOF(i)[n] -= 2*step;

      ArrayJ functionalm = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_HDG( integrand, functionalm ), xfld, (qfld, afld), &quadratureOrder, 1 );

      qfld.DOF(i)[n] += step;

      fdjacFunctional[n] = (functionalp - functionalm)/(2*step);
    }

    const Real small_tol = 1e-10;
    const Real close_tol = 1e-8;
    for (int n = 0; n < DLA::VectorSize<ArrayQ>::M; n++)
      SANS_CHECK_CLOSE( fdjacFunctional[n], jacFunctional[i][n], small_tol, close_tol );
  }
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
