// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// FunctionalBoundaryTrace_HDG_sansLG_btest
// testing of boundary output integrand routines for : HDG NS

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_Flux_mitState_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_OutputWeightRsd_HDG.h"
#include "Discretization/HDG/FunctionalBoundaryTrace_HDG_sansLG.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"

#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/OutputEuler2D.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( FunctionalBoundaryTrace_HDG_sansLG_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FunctionalTest_2D_P1_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::PhysDim PhysDim;

  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef DiscretizationHDG<NDPDEClass> DiscretizationClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDOutputClass::template ArrayJ<Real> ArrayJ;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
//  const Real tSutherland = 110;     // K
//  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // HDG discretization
  DiscretizationClass disc(pde, Local, Gradient );

  // static
  BOOST_CHECK( PhysDim::D == 2 );

  // WALL BC INTEGRAND
  typedef BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass> BCClass;
  typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, HDG> BCIntegrandBoundaryTrace;

  NDBCClass bc(pde);
  BCIntegrandBoundaryTrace bcintegrand( pde, bc, {1}, disc );

  // DRAG INTEGRAND
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, HDG> IntegrandOutputClass;

  NDOutputClass outputFcn(pde, 1., 0.);
  IntegrandOutputClass output_integrand( outputFcn, {1} );

  //GRID

  int qorder = 1;
  XField2D_1Triangle_X1_1Group xfld;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution: P1

  // triangle solution
  qfld.DOF(0) = {1.0, 0.0, 0.0, 1.0};
  qfld.DOF(1) = {1.0, 0.0, 0.0, 1.0};
  qfld.DOF(2) = {1.0, 0.0, 0.0, 1.0};

  afld.DOF(0) = 0.0;
  afld.DOF(1) = 0.0;
  afld.DOF(2) = 0.0;

  qIfld.DOF(2) = {1.0, 0.0, 0.0, 1.0};  // second face
  qIfld.DOF(3) = {1.0, 0.0, 0.0, 1.0};

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  ArrayJ Drag = 0;
  // int quadratureOrder[3] = {-1, -1, -1};    // max
  int quadratureOrder[3] = {0,0,0}; // min
  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      FunctionalBoundaryTrace_HDG_sansLG( output_integrand, bcintegrand, Drag ), xfld, (qfld, afld), qIfld, quadratureOrder, 3 );

  SANS_CHECK_CLOSE( Drag, 1.0, small_tol, close_tol );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
