// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANTEST_HDG_H
#define JACOBIANTEST_HDG_H

#include "tools/SANSnumerics.h"     // Real
#include "tools/call_with_derived.h"
#include "Field/Element/ElementIntegral.h"

namespace SANS
{
namespace testspace
{

template<class Surreal, class IntegrandCell>
class JacobianCell_HDG_Test_impl :
    public GroupIntegralCellType< JacobianCell_HDG_Test_impl<Surreal, IntegrandCell> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCell::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename IntegrandCell::template MatrixQ<Real> MatrixQ;

  typedef typename IntegrandCell::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename IntegrandCell::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  // Save off the boundary trace integrand and the residual vectors
  JacobianCell_HDG_Test_impl( const IntegrandCell& fcn,
                              MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                              MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_a,
                              MatrixScatterAdd<MatrixQ>& mtxGlobalAu_q,
                              MatrixScatterAdd<MatrixQ>& mtxGlobalAu_a ) :
    fcn_(fcn), mtxGlobalPDE_q_(mtxGlobalPDE_q), mtxGlobalPDE_a_(mtxGlobalPDE_a),
               mtxGlobalAu_q_(mtxGlobalAu_q),   mtxGlobalAu_a_(mtxGlobalAu_a){}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>&     qfld = get<0>(flds);
    const Field<PhysDim, TopoDim, VectorArrayQ>& afld = get<1>(flds);

    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalPDE_a_.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_a_.n() == afld.nDOF()*PhysDim::D );

    SANS_ASSERT( mtxGlobalAu_q_.m() == afld.nDOF()*PhysDim::D );
    SANS_ASSERT( mtxGlobalAu_q_.n() == qfld.nDOF() );

    SANS_ASSERT( mtxGlobalAu_a_.m() == afld.nDOF()*PhysDim::D );
    SANS_ASSERT( mtxGlobalAu_a_.n() == afld.nDOF()*PhysDim::D );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                            template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {
    typedef typename XFieldType                          ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ    >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> AFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<Surreal> ElementQFieldSurrealClass;
    typedef typename AFieldCellGroupType::template ElementType<Surreal> ElementAFieldSurrealClass;

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const AFieldCellGroupType& afldCell = get<1>(fldsCell);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldSurrealClass qfldElemSurreal( qfldCell.basis() );
    ElementAFieldSurrealClass afldElemSurreal( afldCell.basis() );

    // DOFs per element
    const int nDOF = qfldElemSurreal.nDOF();

    // variables/equations per DOF
    const int nEqn = IntegrandCell::N;

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_q(nDOF, -1);
    std::vector<int> mapDOFGlobal_a(nDOF*PhysDim::D, -1);

    // element integral
    GalerkinWeightedIntegral<TopoDim, Topology, ArrayQSurreal> integralPDE(quadratureorder, nDOF);
    GalerkinWeightedIntegral<TopoDim, Topology, VectorArrayQSurreal> integralAUX(quadratureorder, nDOF);

    // element integrand/residual
    DLA::VectorD<ArrayQSurreal> rsdElemPDE( nDOF );

    // auxiliary variable residuals
    DLA::VectorD<VectorArrayQSurreal> rsdElemAUX( nDOF );

    // element jacobian matrices
    MatrixElemClass mtxElemPDE_q(nDOF, nDOF);
    MatrixElemClass mtxElemAu_q(PhysDim::D*nDOF, nDOF);

    MatrixElemClass mtxElemPDE_a(nDOF, PhysDim::D*nDOF);
    MatrixElemClass mtxElemAu_a(PhysDim::D*nDOF, PhysDim::D*nDOF);

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero element Jacobians
      mtxElemPDE_q = 0;  mtxElemPDE_a = 0;
      mtxElemAu_q  = 0;  mtxElemAu_a  = 0;

      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElemSurreal, elem );
      afldCell.getElement( afldElemSurreal, elem );

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*((PhysDim::D+1)*nDOF); nchunk += nDeriv)
      {

        // associate derivative slots with solution & lifting-operator variables

        int slot, slotOffset;
        for (int j = 0; j < nDOF; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qfldElemSurreal.DOF(j),n).deriv(k) = 0;

            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemSurreal.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset = nEqn*nDOF;

        for (int d = 0; d < PhysDim::D; d++)
        {
          for (int j = 0; j < nDOF; j++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              for (int k = 0; k < nDeriv; k++)
                DLA::index(afldElemSurreal.DOF(j)[d],n).deriv(k) = 0;

              slot = slotOffset + nEqn*j + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(afldElemSurreal.DOF(j)[d],n).deriv(slot - nchunk) = 1;
            }
          }
          slotOffset += nEqn*nDOF;
        }

        for (int n = 0; n < nDOF; n++) rsdElemPDE[n] = 0;
        for (int n = 0; n < nDOF; n++) rsdElemAUX[n] = 0;

        // cell integration for canonical element

        integralPDE( fcn_.integrand_PDE(xfldElem, qfldElemSurreal, afldElemSurreal),
                     xfldElem, rsdElemPDE.data(), nDOF );

        integralAUX( fcn_.integrand_AUX(xfldElem, qfldElemSurreal, afldElemSurreal),
                     xfldElem, rsdElemAUX.data(), nDOF );

        // accumulate derivatives into element jacobians

        for (int j = 0; j < nDOF; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOF; i++)
              {
                for (int m = 0; m < nEqn; m++)
                {
                  DLA::index(mtxElemPDE_q(i,j),m,n) = DLA::index(rsdElemPDE[i],m).deriv(slot - nchunk);

                  for (int d = 0; d < PhysDim::D; d++)
                    DLA::index(mtxElemAu_q(PhysDim::D*i+d,j),m,n) = DLA::index(rsdElemAUX[i][d],m).deriv(slot - nchunk);
                }
              }
            }
          }
        }
        slotOffset = nEqn*nDOF;

        for (int d1 = 0; d1 < PhysDim::D; d1++)
        {
          for (int j = 0; j < nDOF; j++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*j + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                for (int i = 0; i < nDOF; i++)
                {
                  for (int m = 0; m < nEqn; m++)
                  {
                    DLA::index(mtxElemPDE_a(i,PhysDim::D*j+d1),m,n) = DLA::index(rsdElemPDE[i],m).deriv(slot - nchunk);

                    for (int d2 = 0; d2 < PhysDim::D; d2++)
                      DLA::index(mtxElemAu_a(PhysDim::D*i+d1,PhysDim::D*j+d2),m,n) = DLA::index(rsdElemAUX[i][d2],m).deriv(slot - nchunk);
                  }
                }
              }
            }
          }
          slotOffset += nEqn*nDOF;
        }
      }   // nchunk

      // scatter-add element jacobian to global
      scatterAdd(
          qfldCell, afldCell, elem, nDOF,
          mapDOFGlobal_q.data(), mapDOFGlobal_a.data(),
          mtxElemPDE_q, mtxElemPDE_a,
          mtxElemAu_q, mtxElemAu_a,
          mtxGlobalPDE_q_, mtxGlobalPDE_a_,
          mtxGlobalAu_q_, mtxGlobalAu_a_ );
    } // elem
  }

//----------------------------------------------------------------------------//
  template <class QFieldCellGroupType, class AFieldCellGroupType, template <class> class SparseMatrixType>
  void
  scatterAdd(
      const QFieldCellGroupType& qfld,
      const AFieldCellGroupType& afld,
      const int elem, const int nDOF,
      int mapDOFGlobal_q[], int mapDOFGlobal_a[],
      const SANS::DLA::MatrixD<MatrixQ>& mtxElemPDE_q,
      const SANS::DLA::MatrixD<MatrixQ>& mtxElemPDE_a,
      const SANS::DLA::MatrixD<MatrixQ>& mtxElemAu_q,
      const SANS::DLA::MatrixD<MatrixQ>& mtxElemAu_a,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_q,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_a,
      SparseMatrixType<MatrixQ>& mtxGlobalAu_q,
      SparseMatrixType<MatrixQ>& mtxGlobalAu_a )
  {

    // jacobian wrt u
    qfld.associativity( elem ).getGlobalMapping( mapDOFGlobal_q, nDOF );

    for (int n = 0; n < nDOF; n++)
      for (int d = 0; d < PhysDim::D; d++)
        mapDOFGlobal_a[PhysDim::D*n+d] = mapDOFGlobal_q[n]*PhysDim::D+d;

    mtxGlobalPDE_q.scatterAdd( mtxElemPDE_q, mapDOFGlobal_q, nDOF );
     mtxGlobalAu_q.scatterAdd( mtxElemAu_q , mapDOFGlobal_a, nDOF*PhysDim::D, mapDOFGlobal_q, nDOF );

    mtxGlobalPDE_a.scatterAdd( mtxElemPDE_a, mapDOFGlobal_q, nDOF, mapDOFGlobal_a, nDOF*PhysDim::D );
     mtxGlobalAu_a.scatterAdd( mtxElemAu_a , mapDOFGlobal_a, nDOF*PhysDim::D );
  }

protected:
  const IntegrandCell& fcn_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_a_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalAu_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalAu_a_;
};

// Factory function

template<class Surreal, class IntegrandCell, class MatrixQ>
JacobianCell_HDG_Test_impl<Surreal, IntegrandCell>
JacobianCell_HDG_Test( const IntegrandCellType<IntegrandCell>& fcn,
                       MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                       MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_a,
                       MatrixScatterAdd<MatrixQ>& mtxGlobalAu_q,
                       MatrixScatterAdd<MatrixQ>& mtxGlobalAu_a)
{
  return { fcn.cast(), mtxGlobalPDE_q, mtxGlobalPDE_a,
                       mtxGlobalAu_q, mtxGlobalAu_a    };
}


//----------------------------------------------------------------------------//
//  HDG interior-trace integral
//
template<class Surreal, class IntegrandInteriorTrace>
class JacobianInteriorTrace_HDG_Test_impl :
    public GroupIntegralInteriorTraceType< JacobianInteriorTrace_HDG_Test_impl<Surreal, IntegrandInteriorTrace> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandInteriorTrace::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename IntegrandInteriorTrace::template MatrixQ<Real> MatrixQ;

  typedef typename IntegrandInteriorTrace::template ArrayQ<Surreal> ArrayQSurreal;
  typedef DLA::VectorS< PhysDim::D, ArrayQSurreal > VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  // Save off the boundary trace integrand and the residual vectors
  JacobianInteriorTrace_HDG_Test_impl( const IntegrandInteriorTrace& fcn,
                                       MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                       MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_a,
                                       MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
                                       MatrixScatterAdd<MatrixQ>& mtxGlobalAu_q,
                                       MatrixScatterAdd<MatrixQ>& mtxGlobalAu_qI,
                                       MatrixScatterAdd<MatrixQ>& mtxGlobalInt_q,
                                       MatrixScatterAdd<MatrixQ>& mtxGlobalInt_a,
                                       MatrixScatterAdd<MatrixQ>& mtxGlobalInt_qI) :
    fcn_(fcn), mtxGlobalPDE_q_(mtxGlobalPDE_q), mtxGlobalPDE_a_(mtxGlobalPDE_a), mtxGlobalPDE_qI_(mtxGlobalPDE_qI),
               mtxGlobalAu_q_(mtxGlobalAu_q),                                    mtxGlobalAu_qI_(mtxGlobalAu_qI),
               mtxGlobalInt_q_(mtxGlobalInt_q), mtxGlobalInt_a_(mtxGlobalInt_a), mtxGlobalInt_qI_(mtxGlobalInt_qI) {}

  std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
  std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfld ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>&     qfld = get<0>(flds);
    const Field<PhysDim, TopoDim, VectorArrayQ>& afld = get<1>(flds);

    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalPDE_a_.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_a_.n() == afld.nDOF()*PhysDim::D );

    SANS_ASSERT( mtxGlobalPDE_qI_.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_qI_.n() == qIfld.nDOF() );


    SANS_ASSERT( mtxGlobalAu_q_.m() == afld.nDOF()*PhysDim::D );
    SANS_ASSERT( mtxGlobalAu_q_.n() == qfld.nDOF() );

    SANS_ASSERT( mtxGlobalAu_qI_.m() == afld.nDOF()*PhysDim::D );
    SANS_ASSERT( mtxGlobalAu_qI_.n() == qIfld.nDOF() );


    SANS_ASSERT( mtxGlobalInt_q_.m() == qIfld.nDOF() );
    SANS_ASSERT( mtxGlobalInt_q_.n() == qfld.nDOF() );

    SANS_ASSERT( mtxGlobalInt_a_.m() == qIfld.nDOF() );
    SANS_ASSERT( mtxGlobalInt_a_.n() == afld.nDOF()*PhysDim::D );

    SANS_ASSERT( mtxGlobalInt_qI_.m() == qIfld.nDOF() );
    SANS_ASSERT( mtxGlobalInt_qI_.n() == qIfld.nDOF() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                       template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                       template FieldCellGroupType<TopologyL>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> AFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldSurrealClassL;
    typedef typename AFieldCellGroupTypeL::template ElementType<Surreal> ElementAFieldSurrealClassL;

    //Right types
    typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyR> AFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<Surreal> ElementQFieldSurrealClassR;
    typedef typename AFieldCellGroupTypeR::template ElementType<Surreal> ElementAFieldSurrealClassR;

    //Trace types
    typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<Surreal> ElementQFieldSurrealTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const int D = PhysDim::D;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const AFieldCellGroupTypeL& afldCellL = get<1>(fldsCellL);

    const QFieldCellGroupTypeL& qfldCellR = get<0>(fldsCellR);
    const AFieldCellGroupTypeL& afldCellR = get<1>(fldsCellR);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldSurrealClassL qfldElemL( qfldCellL.basis() );
    ElementAFieldSurrealClassL afldElemL( afldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldSurrealClassR qfldElemR( qfldCellR.basis() );
    ElementAFieldSurrealClassR afldElemR( afldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldSurrealTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();
    const int nDOFI = qIfldElemTrace.nDOF();

    // variables/equations per DOF
    const int nEqn = IntegrandInteriorTrace::N;

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_qL(nDOFL, -1);
    std::vector<int> mapDOFGlobal_aL(nDOFL*D, -1);

    std::vector<int> mapDOFGlobal_qR(nDOFR, -1);
    std::vector<int> mapDOFGlobal_aR(nDOFR*D, -1);

    std::vector<int> mapDOFGlobal_qI(nDOFI, -1);

    // element integral
//    typedef IntegrandHDG<ArrayQSurreal,VectorArrayQSurreal> CellIntegrandType;
//    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, CellIntegrandType, CellIntegrandType, ArrayQSurreal>
//      integral(quadratureorder, nDOFL, nDOFR, nDOFI);

    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQSurreal, ArrayQSurreal>
      integralPDEL(quadratureorder, nDOFL, nDOFI);

    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQSurreal, ArrayQSurreal>
      integralPDER(quadratureorder, nDOFR, nDOFI);

    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, VectorArrayQSurreal> integralAUXL(quadratureorder, nDOFL);
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, VectorArrayQSurreal> integralAUXR(quadratureorder, nDOFR);

    // element integrand/residual
    DLA::VectorD< ArrayQSurreal > rsdElemPDEL( nDOFL );
    DLA::VectorD< ArrayQSurreal > rsdElemPDER( nDOFR );
    DLA::VectorD< ArrayQSurreal > rsdElemINTL( nDOFI );
    DLA::VectorD< ArrayQSurreal > rsdElemINTR( nDOFI );
    DLA::VectorD< ArrayQSurreal > rsdElemINT( nDOFI );

    DLA::VectorD< VectorArrayQSurreal > rsdElemAUXL( nDOFL );
    DLA::VectorD< VectorArrayQSurreal > rsdElemAUXR( nDOFR );

    // element jacobian matrices
    // Left
    MatrixElemClass mtxElemPDEL_qL(nDOFL, nDOFL);
    MatrixElemClass mtxElemPDEL_aL(nDOFL, D*nDOFL);
    MatrixElemClass mtxElemPDEL_qI(nDOFL, nDOFI);

    MatrixElemClass mtxElemAuL_qL(D*nDOFL, nDOFL);
    MatrixElemClass mtxElemAuL_qI(D*nDOFL, nDOFI);

    //right
    MatrixElemClass mtxElemPDER_qR(nDOFR, nDOFR);
    MatrixElemClass mtxElemPDER_aR(nDOFR, D*nDOFR);
    MatrixElemClass mtxElemPDER_qI(nDOFR, nDOFI);

    MatrixElemClass mtxElemAuR_qR(D*nDOFR, nDOFR);
    MatrixElemClass mtxElemAuR_qI(D*nDOFR, nDOFI);

    //trace
    MatrixElemClass mtxElemInt_qL(nDOFI, nDOFL);
    MatrixElemClass mtxElemInt_aL(nDOFI, D*nDOFL);

    MatrixElemClass mtxElemInt_qR(nDOFI, nDOFR);
    MatrixElemClass mtxElemInt_aR(nDOFI, D*nDOFR);

    MatrixElemClass mtxElemInt_qI(nDOFI, nDOFI);


    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    // loop over elements within group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero element Jacobians
      mtxElemPDEL_qL = 0; mtxElemPDEL_aL = 0; mtxElemPDEL_qI = 0;
      mtxElemAuL_qL  = 0;                     mtxElemAuL_qI  = 0;
      mtxElemInt_qL  = 0; mtxElemInt_aL  = 0;

      mtxElemPDER_qR = 0;  mtxElemPDER_aR = 0; mtxElemPDER_qI = 0;
      mtxElemAuR_qR  = 0;                      mtxElemAuR_qI  = 0;
      mtxElemInt_qR  = 0;  mtxElemInt_aR  = 0;

      mtxElemInt_qI = 0;

      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      afldCellL.getElement( afldElemL, elemL );

      xfldCellR.getElement( xfldElemR, elemR );
      qfldCellR.getElement( qfldElemR, elemR );
      afldCellR.getElement( afldElemR, elemR );

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*((D+1)*(nDOFL+nDOFR) + nDOFI); nchunk += nDeriv)
      {

        // associate derivative slots

        // wrt uL
        int slot, slotOffset = 0;
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qfldElemL.DOF(j),n).deriv(k) = 0;

            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemL.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nEqn*nDOFL;

        // wrt uR
        for (int j = 0; j < nDOFR; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qfldElemR.DOF(j),n).deriv(k) = 0;

            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemR.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nEqn*nDOFR;

        // wrt qL
        for (int d = 0; d < D; d++)
        {
          for (int j = 0; j < nDOFL; j++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              for (int k = 0; k < nDeriv; k++)
                DLA::index(afldElemL.DOF(j)[d],n).deriv(k) = 0;

              slot = slotOffset + nEqn*j + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(afldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 1;
            }
          }
          slotOffset += nEqn*nDOFL;
        }

        // wrt qR
        for (int d = 0; d < D; d++)
        {
          for (int j = 0; j < nDOFR; j++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              for (int k = 0; k < nDeriv; k++)
                DLA::index(afldElemR.DOF(j)[d],n).deriv(k) = 0;

              slot = slotOffset + nEqn*j + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(afldElemR.DOF(j)[d],n).deriv(slot - nchunk) = 1;
            }
          }
          slotOffset += nEqn*nDOFR;
        }

        // wrt qI
        for (int j = 0; j < nDOFI; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qIfldElemTrace.DOF(j),n).deriv(k) = 0;

            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qIfldElemTrace.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nEqn*nDOFI;

        for (int n = 0; n < nDOFL; n++) rsdElemPDEL[n] = 0;
        for (int n = 0; n < nDOFR; n++) rsdElemPDER[n] = 0;
        for (int n = 0; n < nDOFI; n++)
        {
          rsdElemINTL[n] = 0;
          rsdElemINTR[n] = 0;
        }

        for (int n = 0; n < nDOFL; n++) rsdElemAUXL[n] = 0;
        for (int n = 0; n < nDOFR; n++) rsdElemAUXR[n] = 0;

        // PDE trace integration for canonical element
        integralPDEL( fcn_.integrand(xfldElemTrace, qIfldElemTrace, canonicalTraceL, +1,
                                     xfldElemL, qfldElemL, afldElemL),
                      xfldElemTrace,
                      rsdElemPDEL.data(), nDOFL,
                      rsdElemINTL.data(), nDOFI);

        integralPDER( fcn_.integrand(xfldElemTrace, qIfldElemTrace, canonicalTraceR, -1,
                                     xfldElemR, qfldElemR, afldElemR),
                      xfldElemTrace,
                      rsdElemPDER.data(), nDOFR,
                      rsdElemINTR.data(), nDOFI);

        for (int n = 0; n < nDOFI; n++) rsdElemINT[n] = rsdElemINTL[n] + rsdElemINTR[n];

        integralAUXL( fcn_.integrand_AUX(xfldElemTrace, qIfldElemTrace, canonicalTraceL, +1,
                                         xfldElemL, qfldElemL),
                      xfldElemTrace, rsdElemAUXL.data(), nDOFL);

        integralAUXR( fcn_.integrand_AUX(xfldElemTrace, qIfldElemTrace, canonicalTraceR, -1,
                                         xfldElemR, qfldElemR),
                      xfldElemTrace, rsdElemAUXR.data(), nDOFR);


        // accumulate derivatives into element jacobians
        slotOffset = 0;
        // wrt uL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                {
                  DLA::index(mtxElemPDEL_qL(i,j),m,n) = DLA::index(rsdElemPDEL[i],m).deriv(slot - nchunk);

                  for (int d = 0; d < D; d++)
                    DLA::index(mtxElemAuL_qL(D*i+d,j),m,n) = DLA::index(rsdElemAUXL[i][d],m).deriv(slot - nchunk);
                }

              for (int i = 0; i < nDOFI; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemInt_qL(i,j),m,n) = DLA::index(rsdElemINT[i],m).deriv(slot - nchunk);
            }
          }
        }
        slotOffset += nEqn*nDOFL;

        // wrt uR
        for (int j = 0; j < nDOFR; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFR; i++)
                for (int m = 0; m < nEqn; m++)
                {
                  DLA::index(mtxElemPDER_qR(i,j),m,n) = DLA::index(rsdElemPDER[i],m).deriv(slot - nchunk);

                  for (int d = 0; d < D; d++)
                    DLA::index(mtxElemAuR_qR(D*i+d,j),m,n) = DLA::index(rsdElemAUXR[i][d],m).deriv(slot - nchunk);
                }

              for (int i = 0; i < nDOFI; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemInt_qR(i,j),m,n) = DLA::index(rsdElemINT[i],m).deriv(slot - nchunk);
            }
          }
        }
        slotOffset += nEqn*nDOFR;

        // wrt qL
        for (int d = 0; d < D; d++)
        {
          for (int j = 0; j < nDOFL; j++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*j + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                for (int i = 0; i < nDOFL; i++)
                  for (int m = 0; m < nEqn; m++)
                    DLA::index(mtxElemPDEL_aL(i,D*j+d),m,n) = DLA::index(rsdElemPDEL[i],m).deriv(slot - nchunk);

                for (int i = 0; i < nDOFI; i++)
                  for (int m = 0; m < nEqn; m++)
                    DLA::index(mtxElemInt_aL(i,D*j+d),m,n) = DLA::index(rsdElemINT[i],m).deriv(slot - nchunk);
              }
            }
          }
          slotOffset += nEqn*nDOFL;
        }

        // wrt qR
        for (int d = 0; d < D; d++)
        {
          for (int j = 0; j < nDOFR; j++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*j + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                for (int i = 0; i < nDOFR; i++)
                  for (int m = 0; m < nEqn; m++)
                    DLA::index(mtxElemPDER_aR(i,D*j+d),m,n) = DLA::index(rsdElemPDER[i],m).deriv(slot - nchunk);

                for (int i = 0; i < nDOFI; i++)
                  for (int m = 0; m < nEqn; m++)
                    DLA::index(mtxElemInt_aR(i,D*j+d),m,n) = DLA::index(rsdElemINT[i],m).deriv(slot - nchunk);
              }
            }
          }
          slotOffset += nEqn*nDOFR;
        }

        // wrt uI
        for (int j = 0; j < nDOFI; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                {
                  DLA::index(mtxElemPDEL_qI(i,j),m,n) = DLA::index(rsdElemPDEL[i],m).deriv(slot - nchunk);
                  for (int d = 0; d < D; d++)
                    DLA::index(mtxElemAuL_qI(D*i+d,j),m,n) = DLA::index(rsdElemAUXL[i][d],m).deriv(slot - nchunk);
                }

              for (int i = 0; i < nDOFR; i++)
                for (int m = 0; m < nEqn; m++)
                {
                  DLA::index(mtxElemPDER_qI(i,j),m,n) = DLA::index(rsdElemPDER[i],m).deriv(slot - nchunk);
                  for (int d = 0; d < D; d++)
                    DLA::index(mtxElemAuR_qI(D*i+d,j),m,n) = DLA::index(rsdElemAUXR[i][d],m).deriv(slot - nchunk);
                }

              for (int i = 0; i < nDOFI; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemInt_qI(i,j),m,n) = DLA::index(rsdElemINT[i],m).deriv(slot - nchunk);
            }
          }
        }
        slotOffset += nEqn*nDOFI;

      }   // nchunk


      // scatter-add element jacobian to global

      scatterAdd(
          qIfldTrace, qfldCellL, afldCellL, qfldCellR, afldCellR,
          elem, elemL, elemR,
          mapDOFGlobal_qL.data(), mapDOFGlobal_aL.data(), nDOFL,
          mapDOFGlobal_qR.data(), mapDOFGlobal_aR.data(), nDOFR,
          mapDOFGlobal_qI.data(), mapDOFGlobal_qI.size(),

          mtxElemPDEL_qL, mtxElemPDEL_aL, mtxElemPDEL_qI,
          mtxElemAuL_qL ,                 mtxElemAuL_qI ,
          mtxElemInt_qL , mtxElemInt_aL ,

          mtxElemPDER_qR, mtxElemPDER_aR, mtxElemPDER_qI,
          mtxElemAuR_qR ,                  mtxElemAuR_qI ,
          mtxElemInt_qR , mtxElemInt_aR ,

          mtxElemInt_qI,

          mtxGlobalPDE_q_, mtxGlobalPDE_a_, mtxGlobalPDE_qI_,
          mtxGlobalAu_q_ ,                  mtxGlobalAu_qI_ ,
          mtxGlobalInt_q_, mtxGlobalInt_a_, mtxGlobalInt_qI_ );
    }
  }

  //----------------------------------------------------------------------------//
  template <class QFieldTraceGroupType,
            class QFieldCellGroupTypeL, class AFieldCellGroupTypeL,
            class QFieldCellGroupTypeR, class AFieldCellGroupTypeR,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const QFieldTraceGroupType& qfldTrace,
      const QFieldCellGroupTypeL& qfldL,
      const AFieldCellGroupTypeL& afldL,
      const QFieldCellGroupTypeR& qfldR,
      const AFieldCellGroupTypeR& afldR,
      const int elem, const int elemL, const int elemR,
      int mapDOFGlobal_qL[], int mapDOFGlobal_aL[], const int nDOFL,
      int mapDOFGlobal_qR[], int mapDOFGlobal_aR[], const int nDOFR,
      int mapDOFGlobal_qI[], const int nDOFI,

      const SANS::DLA::MatrixD<MatrixQ>& mtxElemPDEL_qL,
      const SANS::DLA::MatrixD<MatrixQ>& mtxElemPDEL_aL,
      const SANS::DLA::MatrixD<MatrixQ>& mtxElemPDEL_qI,
      const SANS::DLA::MatrixD<MatrixQ>& mtxElemAuL_qL,
      const SANS::DLA::MatrixD<MatrixQ>& mtxElemAuL_qI ,
      const SANS::DLA::MatrixD<MatrixQ>& mtxElemInt_qL,
      const SANS::DLA::MatrixD<MatrixQ>& mtxElemInt_aL ,

      const SANS::DLA::MatrixD<MatrixQ>& mtxElemPDER_qR,
      const SANS::DLA::MatrixD<MatrixQ>& mtxElemPDER_aR,
      const SANS::DLA::MatrixD<MatrixQ>& mtxElemPDER_qI,
      const SANS::DLA::MatrixD<MatrixQ>& mtxElemAuR_qR,
      const SANS::DLA::MatrixD<MatrixQ>& mtxElemAuR_qI,
      const SANS::DLA::MatrixD<MatrixQ>& mtxElemInt_qR,
      const SANS::DLA::MatrixD<MatrixQ>& mtxElemInt_aR,

      const SANS::DLA::MatrixD<MatrixQ>& mtxElemInt_qI,

      SparseMatrixType<MatrixQ>& mtxGlobalPDE_q,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_a,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_qI,
      SparseMatrixType<MatrixQ>& mtxGlobalAu_q,
      SparseMatrixType<MatrixQ>& mtxGlobalAu_qI,
      SparseMatrixType<MatrixQ>& mtxGlobalInt_q,
      SparseMatrixType<MatrixQ>& mtxGlobalInt_a,
      SparseMatrixType<MatrixQ>& mtxGlobalInt_qI)
  {

    // global mapping for qL
    qfldL.associativity( elemL ).getGlobalMapping( mapDOFGlobal_qL, nDOFL );

    // global mapping for qR
    qfldR.associativity( elemR ).getGlobalMapping( mapDOFGlobal_qR, nDOFR );

    // global mapping for aL
    for (int n = 0; n < nDOFL; n++)
      for (int d = 0; d < PhysDim::D; d++)
        mapDOFGlobal_aL[PhysDim::D*n+d] = mapDOFGlobal_qL[n]*PhysDim::D+d;

    // global mapping for aR
    for (int n = 0; n < nDOFR; n++)
      for (int d = 0; d < PhysDim::D; d++)
        mapDOFGlobal_aR[PhysDim::D*n+d] = mapDOFGlobal_qR[n]*PhysDim::D+d;

    // global mapping for qI
    qfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobal_qI, nDOFI );

    // jacobian wrt qL
    mtxGlobalPDE_q.scatterAdd( mtxElemPDEL_qL, mapDOFGlobal_qL, nDOFL );
     mtxGlobalAu_q.scatterAdd( mtxElemAuL_qL , mapDOFGlobal_aL, nDOFL*PhysDim::D, mapDOFGlobal_qL, nDOFL );
    mtxGlobalInt_q.scatterAdd( mtxElemInt_qL , mapDOFGlobal_qI, nDOFI           , mapDOFGlobal_qL, nDOFL );

    // jacobian wrt qR
    mtxGlobalPDE_q.scatterAdd( mtxElemPDER_qR, mapDOFGlobal_qR, nDOFR );
     mtxGlobalAu_q.scatterAdd( mtxElemAuR_qR , mapDOFGlobal_aR, nDOFR*PhysDim::D, mapDOFGlobal_qR, nDOFR );
    mtxGlobalInt_q.scatterAdd( mtxElemInt_qR , mapDOFGlobal_qI, nDOFI           , mapDOFGlobal_qR, nDOFR );

    // jacobian wrt aL
    mtxGlobalPDE_a.scatterAdd( mtxElemPDEL_aL, mapDOFGlobal_qL, nDOFL, mapDOFGlobal_aL, nDOFL*PhysDim::D );
    mtxGlobalInt_a.scatterAdd( mtxElemInt_aL , mapDOFGlobal_qI, nDOFI, mapDOFGlobal_aL, nDOFL*PhysDim::D );

    // jacobian wrt aR
    mtxGlobalPDE_a.scatterAdd( mtxElemPDER_aR, mapDOFGlobal_qR, nDOFR, mapDOFGlobal_aR, nDOFR*PhysDim::D );
    mtxGlobalInt_a.scatterAdd( mtxElemInt_aR , mapDOFGlobal_qI, nDOFI, mapDOFGlobal_aR, nDOFR*PhysDim::D );

    // jacobian wrt qI
    mtxGlobalPDE_qI.scatterAdd( mtxElemPDEL_qI, mapDOFGlobal_qL, nDOFL           , mapDOFGlobal_qI, nDOFI );
     mtxGlobalAu_qI.scatterAdd( mtxElemAuL_qI , mapDOFGlobal_aL, nDOFL*PhysDim::D, mapDOFGlobal_qI, nDOFI );
    mtxGlobalPDE_qI.scatterAdd( mtxElemPDER_qI, mapDOFGlobal_qR, nDOFR           , mapDOFGlobal_qI, nDOFI );
     mtxGlobalAu_qI.scatterAdd( mtxElemAuR_qI , mapDOFGlobal_aR, nDOFR*PhysDim::D, mapDOFGlobal_qI, nDOFI );
    mtxGlobalInt_qI.scatterAdd( mtxElemInt_qI , mapDOFGlobal_qI, nDOFI );
  }

protected:
  const IntegrandInteriorTrace& fcn_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_a_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalAu_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalAu_qI_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalInt_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalInt_a_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalInt_qI_;
};

// Factory function
template<class Surreal, class IntegrandInteriorTrace, class MatrixQ>
JacobianInteriorTrace_HDG_Test_impl<Surreal, IntegrandInteriorTrace>
JacobianInteriorTrace_HDG_Test( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                                MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_a,
                                MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
                                MatrixScatterAdd<MatrixQ>& mtxGlobalAu_q,
                                MatrixScatterAdd<MatrixQ>& mtxGlobalAu_qI,
                                MatrixScatterAdd<MatrixQ>& mtxGlobalInt_q,
                                MatrixScatterAdd<MatrixQ>& mtxGlobalInt_a,
                                MatrixScatterAdd<MatrixQ>& mtxGlobalInt_qI)
{
  return { fcn.cast(), mtxGlobalPDE_q, mtxGlobalPDE_a, mtxGlobalPDE_qI,
                       mtxGlobalAu_q ,                 mtxGlobalAu_qI ,
                       mtxGlobalInt_q, mtxGlobalInt_a, mtxGlobalInt_qI };
}

//----------------------------------------------------------------------------//
//  HDG boundary trace group integral without lagrange multipliers
//

template<class Surreal, class IntegrandBoundaryTrace>
class JacobianBoundaryTrace_HDG_Test_impl :
    public GroupIntegralBoundaryTraceType< JacobianBoundaryTrace_HDG_Test_impl<Surreal, IntegrandBoundaryTrace> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTrace::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename IntegrandBoundaryTrace::template MatrixQ<Real> MatrixQ;

  typedef typename IntegrandBoundaryTrace::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename IntegrandBoundaryTrace::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  // Save off the boundary trace integrand and the residual vectors
  JacobianBoundaryTrace_HDG_Test_impl( const IntegrandBoundaryTrace& fcn,
                                       MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                       MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_a,
                                       MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
                                       MatrixScatterAdd<MatrixQ>& mtxGlobalAu_q,
                                       MatrixScatterAdd<MatrixQ>& mtxGlobalAu_a,
                                       MatrixScatterAdd<MatrixQ>& mtxGlobalAu_qI,
                                       MatrixScatterAdd<MatrixQ>& mtxGlobalInt_q,
                                       MatrixScatterAdd<MatrixQ>& mtxGlobalInt_a,
                                       MatrixScatterAdd<MatrixQ>& mtxGlobalInt_qI ) :
    fcn_(fcn), mtxGlobalPDE_q_(mtxGlobalPDE_q), mtxGlobalPDE_a_(mtxGlobalPDE_a), mtxGlobalPDE_qI_(mtxGlobalPDE_qI),
               mtxGlobalAu_q_(mtxGlobalAu_q),   mtxGlobalAu_a_(mtxGlobalAu_a),   mtxGlobalAu_qI_(mtxGlobalAu_qI),
               mtxGlobalInt_q_(mtxGlobalInt_q), mtxGlobalInt_a_(mtxGlobalInt_a), mtxGlobalInt_qI_(mtxGlobalInt_qI) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the matricies
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfld ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>&     qfld = get<0>(flds);
    const Field<PhysDim, TopoDim, VectorArrayQ>& afld = get<1>(flds);

    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalPDE_a_.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_a_.n() == afld.nDOF()*PhysDim::D );

    SANS_ASSERT( mtxGlobalPDE_qI_.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_qI_.n() == qIfld.nDOF() );


    SANS_ASSERT( mtxGlobalAu_q_.m() == afld.nDOF()*PhysDim::D );
    SANS_ASSERT( mtxGlobalAu_q_.n() == qfld.nDOF() );

    SANS_ASSERT( mtxGlobalAu_a_.m() == afld.nDOF()*PhysDim::D );
    SANS_ASSERT( mtxGlobalAu_a_.n() == afld.nDOF()*PhysDim::D );

    SANS_ASSERT( mtxGlobalAu_qI_.m() == afld.nDOF()*PhysDim::D );
    SANS_ASSERT( mtxGlobalAu_qI_.n() == qIfld.nDOF() );


    SANS_ASSERT( mtxGlobalInt_q_.m() == qIfld.nDOF() );
    SANS_ASSERT( mtxGlobalInt_q_.n() == qfld.nDOF() );

    SANS_ASSERT( mtxGlobalInt_a_.m() == qIfld.nDOF() );
    SANS_ASSERT( mtxGlobalInt_a_.n() == afld.nDOF()*PhysDim::D );

    SANS_ASSERT( mtxGlobalInt_qI_.m() == qIfld.nDOF() );
    SANS_ASSERT( mtxGlobalInt_qI_.n() == qIfld.nDOF() );
  }


//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                       template FieldCellGroupType<TopologyL>& fldsCellL,
      const int traceGroupGlobal,
      const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> AFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<>        ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldSurrealClassL;
    typedef typename AFieldCellGroupTypeL::template ElementType<Surreal> ElementAFieldSurrealClassL;

    //Trace types
    typedef typename XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<>        ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<Surreal> ElementQFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const int D = PhysDim::D;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const AFieldCellGroupTypeL& afldCellL = get<1>(fldsCellL);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldSurrealClassL qfldElemL( qfldCellL.basis() );
    ElementAFieldSurrealClassL afldElemL( afldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFI = qIfldElemTrace.nDOF();

    // variables/equations per DOF
    const int nEqn = IntegrandBoundaryTrace::N;

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_qL(nDOFL, -1);
    std::vector<int> mapDOFGlobal_aL(nDOFL*D, -1);
    std::vector<int> mapDOFGlobalTrace(nDOFI, -1);

    // element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQSurreal, ArrayQSurreal>
      integralPDE(quadratureorder, nDOFL, nDOFI);

    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, VectorArrayQSurreal> integralAUX(quadratureorder, nDOFL);

    // element integrand/residual
    std::vector<ArrayQSurreal> rsdElemPDEL( nDOFL );
    std::vector<ArrayQSurreal> rsdElemINT( nDOFI );

    // auxiliary variable residuals
    DLA::VectorD<VectorArrayQSurreal> rsdElemAUXL( nDOFL );

    // element jacobian matrices
    // Left
    MatrixElemClass mtxElemPDEL_qL(nDOFL, nDOFL);
    MatrixElemClass mtxElemPDEL_aL(nDOFL, D*nDOFL);
    MatrixElemClass mtxElemPDEL_qI(nDOFL, nDOFI);

    MatrixElemClass mtxElemAuL_qL(D*nDOFL, nDOFL);
    MatrixElemClass mtxElemAuL_aL(D*nDOFL, D*nDOFL);
    MatrixElemClass mtxElemAuL_qI(D*nDOFL, nDOFI);

    MatrixElemClass mtxElemInt_qL(nDOFI, nDOFL);
    MatrixElemClass mtxElemInt_aL(nDOFI, D*nDOFL);
    MatrixElemClass mtxElemInt_qI(nDOFI, nDOFI);

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    // loop over elements within group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero element Jacobians
      mtxElemPDEL_qL = 0; mtxElemPDEL_aL = 0;
      mtxElemAuL_qL  = 0; mtxElemAuL_aL  = 0;

      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      afldCellL.getElement( afldElemL, elemL );

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*((PhysDim::D+1)*nDOFL + nDOFI); nchunk += nDeriv)
      {

        // associate derivative slots

        // wrt qL
        int slot, slotOffset = 0;
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemL.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nEqn*nDOFL;

        // wrt aL
        for (int d = 0; d < D; d++)
        {
          for (int j = 0; j < nDOFL; j++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*j + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(afldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 1;
            }
          }
          slotOffset += nEqn*nDOFL;
        }

        // wrt qI
        for (int j = 0; j < nDOFI; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qIfldElemTrace.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nEqn*nDOFI;

        //------------------------------------------------------------------------------------------------

        for (int n = 0; n < nDOFL; n++) rsdElemPDEL[n] = 0;
        for (int n = 0; n < nDOFI; n++) rsdElemINT[n] = 0;
        for (int n = 0; n < nDOFL; n++) rsdElemAUXL[n] = 0;

        // trace integration
        integralPDE( fcn_.integrand(xfldElemTrace, canonicalTraceL,
                                    xfldElemL, qfldElemL, afldElemL, qIfldElemTrace),
                     xfldElemTrace,
                     rsdElemPDEL.data(), nDOFL,
                     rsdElemINT.data(), nDOFI);

        // AUX trace integration for canonical element
        integralAUX( fcn_.integrand_AUX(xfldElemTrace, canonicalTraceL, xfldElemL,
                                        qfldElemL, afldElemL, qIfldElemTrace),
                     xfldElemTrace, rsdElemAUXL.data(), nDOFL);

        //------------------------------------------------------------------------------------------------

        // accumulate derivatives into element jacobians
        slotOffset = 0;
        // wrt aL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qfldElemL.DOF(j),n).deriv(slot - nchunk) = 0; // Reset the derivative

              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                {
                  DLA::index(mtxElemPDEL_qL(i,j),m,n) = DLA::index(rsdElemPDEL[i],m).deriv(slot - nchunk);

                  for (int d = 0; d < D; d++)
                    DLA::index(mtxElemAuL_qL(D*i+d,j),m,n) = DLA::index(rsdElemAUXL[i][d],m).deriv(slot - nchunk);
                }

              for (int i = 0; i < nDOFI; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemInt_qL(i,j),m,n) = DLA::index(rsdElemINT[i],m).deriv(slot - nchunk);
            }
          }
        }
        slotOffset += nEqn*nDOFL;

        // wrt aL
        for (int d = 0; d < D; d++)
        {
          for (int j = 0; j < nDOFL; j++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*j + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                DLA::index(afldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 0;  // Reset the derivative

                for (int i = 0; i < nDOFL; i++)
                  for (int m = 0; m < nEqn; m++)
                  {
                    DLA::index(mtxElemPDEL_aL(i,D*j+d),m,n) = DLA::index(rsdElemPDEL[i],m).deriv(slot - nchunk);

                    for (int di = 0; di < D; di++)
                      DLA::index(mtxElemAuL_aL(D*i+di,D*j+d),m,n) = DLA::index(rsdElemAUXL[i][di],m).deriv(slot - nchunk);
                  }

                for (int i = 0; i < nDOFI; i++)
                  for (int m = 0; m < nEqn; m++)
                    DLA::index(mtxElemInt_aL(i,D*j+d),m,n) = DLA::index(rsdElemINT[i],m).deriv(slot - nchunk);
              }
            }
          }
          slotOffset += nEqn*nDOFL;
        }

        // wrt qI
        for (int j = 0; j < nDOFI; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qIfldElemTrace.DOF(j),n).deriv(slot - nchunk) = 0;  // Reset the derivative

              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                {
                  DLA::index(mtxElemPDEL_qI(i,j),m,n) = DLA::index(rsdElemPDEL[i],m).deriv(slot - nchunk);
                  for (int d = 0; d < D; d++)
                    DLA::index(mtxElemAuL_qI(D*i+d,j),m,n) = DLA::index(rsdElemAUXL[i][d],m).deriv(slot - nchunk);
                }

              for (int i = 0; i < nDOFI; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemInt_qI(i,j),m,n) = DLA::index(rsdElemINT[i],m).deriv(slot - nchunk);
            }
          }
        }
        slotOffset += nEqn*nDOFI;
      }   // nchunk


      // scatter-add element jacobian to global

      scatterAdd(
          qIfldTrace,
          qfldCellL,
          elem, elemL,
          mapDOFGlobal_qL.data(), mapDOFGlobal_aL.data(), nDOFL,
          mapDOFGlobalTrace.data(), nDOFI,

          mtxElemPDEL_qL, mtxElemPDEL_aL, mtxElemPDEL_qI,
          mtxElemAuL_qL , mtxElemAuL_aL , mtxElemAuL_qI,
          mtxElemInt_qL , mtxElemInt_aL , mtxElemInt_qI,

          mtxGlobalPDE_q_, mtxGlobalPDE_a_, mtxGlobalPDE_qI_,
          mtxGlobalAu_q_ , mtxGlobalAu_a_ , mtxGlobalAu_qI_,
          mtxGlobalInt_q_, mtxGlobalInt_a_, mtxGlobalInt_qI_);
    }
  }


//----------------------------------------------------------------------------//
  template <class QFieldTraceGroupType, class QFieldCellGroupTypeL,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const QFieldTraceGroupType& qIfldTrace,
      const QFieldCellGroupTypeL& qfldCellL,
      const int elem, const int elemL,
      int mapDOFGlobal_qL[], int mapDOFGlobal_aL[], const int nDOFL,
      int mapDOFGlobal_qI[], const int nDOFI,

      const MatrixElemClass& mtxElemPDEL_qL,
      const MatrixElemClass& mtxElemPDEL_aL,
      const MatrixElemClass& mtxElemPDEL_qI,
      const MatrixElemClass& mtxElemAuL_qL,
      const MatrixElemClass& mtxElemAuL_aL,
      const MatrixElemClass& mtxElemAuL_qI,
      const MatrixElemClass& mtxElemInt_qL,
      const MatrixElemClass& mtxElemInt_aL,
      const MatrixElemClass& mtxElemInt_qI,

      SparseMatrixType<MatrixQ>& mtxGlobalPDE_q,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_a,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_qI,
      SparseMatrixType<MatrixQ>& mtxGlobalAu_q,
      SparseMatrixType<MatrixQ>& mtxGlobalAu_a,
      SparseMatrixType<MatrixQ>& mtxGlobalAu_qI,
      SparseMatrixType<MatrixQ>& mtxGlobalInt_q,
      SparseMatrixType<MatrixQ>& mtxGlobalInt_a,
      SparseMatrixType<MatrixQ>& mtxGlobalInt_qI)
  {
    // global mapping for qL
    qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobal_qL, nDOFL );

    // global mapping for aL
    for (int n = 0; n < nDOFL; n++)
      for (int d = 0; d < PhysDim::D; d++)
        mapDOFGlobal_aL[PhysDim::D*n+d] = mapDOFGlobal_qL[n]*PhysDim::D+d;

    // global mapping for qI
    qIfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobal_qI, nDOFI );

    // jacobian wrt qL
    mtxGlobalPDE_q.scatterAdd( mtxElemPDEL_qL, mapDOFGlobal_qL, nDOFL );
     mtxGlobalAu_q.scatterAdd( mtxElemAuL_qL , mapDOFGlobal_aL, nDOFL*PhysDim::D, mapDOFGlobal_qL, nDOFL );
    mtxGlobalInt_q.scatterAdd( mtxElemInt_qL , mapDOFGlobal_qI, nDOFI           , mapDOFGlobal_qL, nDOFL );

    // jacobian wrt aL
    mtxGlobalPDE_a.scatterAdd( mtxElemPDEL_aL, mapDOFGlobal_qL, nDOFL, mapDOFGlobal_aL, nDOFL*PhysDim::D );
     mtxGlobalAu_a.scatterAdd( mtxElemAuL_aL , mapDOFGlobal_aL, nDOFL*PhysDim::D );
    mtxGlobalInt_a.scatterAdd( mtxElemInt_aL , mapDOFGlobal_qI, nDOFI, mapDOFGlobal_aL, nDOFL*PhysDim::D );

    // jacobian wrt qI
    mtxGlobalPDE_qI.scatterAdd( mtxElemPDEL_qI, mapDOFGlobal_qL, nDOFL           , mapDOFGlobal_qI, nDOFI );
     mtxGlobalAu_qI.scatterAdd( mtxElemAuL_qI , mapDOFGlobal_aL, nDOFL*PhysDim::D, mapDOFGlobal_qI, nDOFI );
    mtxGlobalInt_qI.scatterAdd( mtxElemInt_qI , mapDOFGlobal_qI, nDOFI );
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_a_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalAu_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalAu_a_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalAu_qI_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalInt_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalInt_a_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalInt_qI_;
};

// Factory function
template<class Surreal, class IntegrandBoundaryTrace, class MatrixQ>
JacobianBoundaryTrace_HDG_Test_impl<Surreal, IntegrandBoundaryTrace>
JacobianBoundaryTrace_HDG_Test( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_a,
                                MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
                                MatrixScatterAdd<MatrixQ>& mtxGlobalAu_q,
                                MatrixScatterAdd<MatrixQ>& mtxGlobalAu_a,
                                MatrixScatterAdd<MatrixQ>& mtxGlobalAu_qI,
                                MatrixScatterAdd<MatrixQ>& mtxGlobalInt_q,
                                MatrixScatterAdd<MatrixQ>& mtxGlobalInt_a,
                                MatrixScatterAdd<MatrixQ>& mtxGlobalInt_qI)
{
  return { fcn.cast(), mtxGlobalPDE_q, mtxGlobalPDE_a, mtxGlobalPDE_qI,
                       mtxGlobalAu_q,  mtxGlobalAu_a,  mtxGlobalAu_qI,
                       mtxGlobalInt_q, mtxGlobalInt_a, mtxGlobalInt_qI };
}

//----------------------------------------------------------------------------//
//  Functional boundary-trace integral without Lagrange multipliers
//

template<class Surreal, class FunctionalIntegrandBoundaryTrace, class BCIntegrandBoundaryTrace, template<class> class Vector>
class JacobianFunctionalBoundaryTrace_HDG_Test_impl :
    public GroupIntegralBoundaryTraceType<
    JacobianFunctionalBoundaryTrace_HDG_Test_impl<Surreal, FunctionalIntegrandBoundaryTrace, BCIntegrandBoundaryTrace, Vector> >
{
public:

  typedef typename FunctionalIntegrandBoundaryTrace::PhysDim PhysDim;
  static const int D = PhysDim::D;

  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename FunctionalIntegrandBoundaryTrace::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayJ<Real> ArrayJ;
  typedef typename FunctionalIntegrandBoundaryTrace::template MatrixJ<Real> MatrixJ;
  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayJ<Surreal> ArrayJSurreal;
  typedef DLA::VectorS<D, MatrixJ> VectorMatrixJ;


  typedef typename BCIntegrandBoundaryTrace::PDE PDE;
  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,D,MatrixQ> RowMatrixQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixAElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixRElemClass;

  JacobianFunctionalBoundaryTrace_HDG_Test_impl(const FunctionalIntegrandBoundaryTrace& fcnJ,
                                                const BCIntegrandBoundaryTrace& fcnBC,
                                                Vector<MatrixJ>& jacFunctional_qL,
                                                Vector<MatrixJ>& jacFunctional_aL,
                                                Vector<MatrixJ>& jacFunctional_qI ) :
    fcnJ_(fcnJ),
    fcnBC_(fcnBC),
    jacFunctional_qL_(jacFunctional_qL),
    jacFunctional_aL_(jacFunctional_aL),
    jacFunctional_qI_(jacFunctional_qI)
  {
    // Find all groups that are common between the BC and functional
    for (std::size_t i = 0; i < fcnJ_.nBoundaryGroups(); i++)
    {
      for (std::size_t j = 0; j < fcnBC_.nBoundaryGroups(); j++)
      {
        std::size_t iBoundaryGroupJ = fcnJ_.boundaryGroup(i);
        if (iBoundaryGroupJ == fcnBC_.boundaryGroup(j))
          boundaryTraceGroups_.push_back(iBoundaryGroupJ);
      }
    }
  }

  std::size_t nBoundaryGroups() const { return boundaryTraceGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return boundaryTraceGroups_[n]; }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfldTrace ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    const Field<PhysDim, TopoDim, VectorArrayQ>& afld = get<1>(flds);

    SANS_ASSERT_MSG(qfld.nDOF() == jacFunctional_qL_.m(), "qfld.nDOF() = %d, jacFunctional_qL_.m() = %d", qfld.nDOF(), jacFunctional_qL_.m());
    SANS_ASSERT_MSG(D*afld.nDOF() == jacFunctional_aL_.m(), "D*afld.nDOF() = %d, jacFunctional_aL_.m() = %d", D*afld.nDOF(), jacFunctional_aL_.m());
    SANS_ASSERT_MSG(qIfldTrace.nDOF() == jacFunctional_qI_.m(), "qIfldTrace.nDOF() = %d, jacFunctional_qI_.m() = %d",
                    qIfldTrace.nDOF(), jacFunctional_qI_.m());
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType, class XFieldTraceGroupType>
  void
  integrate( const int cellGroupGloalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                              template FieldCellGroupType<TopologyL>& fldsCellL,
             const int traceGroupGlobal,
             const XFieldTraceGroupType& xfldTrace,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
             int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>       QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> AFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>  QFieldTraceGroupType;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldClassL;
    typedef typename AFieldCellGroupTypeL::template ElementType<Surreal> ElementAFieldClassL;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<Surreal> ElementQFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const AFieldCellGroupTypeL& afldCellL = get<1>(fldsCellL);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementAFieldClassL afldElemL( afldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace(  xfldTrace.basis() );
    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    // total number of entries in ArrayQ
    const int nVar = DLA::VectorSize<ArrayQ>::M;

    // total number of outputs in the functional
    const int nJEqn = DLA::VectorSize<ArrayJ>::M;
    static_assert( nJEqn == 1 , "number of output functionals must be 1");

    // variables/equations per DOF
    // const int nJEqn = IntegrandBoundaryTrace::N;

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFTrace = qIfldElemTrace.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_qL(nDOFL, -1);
    std::vector<int> mapDOFGlobal_qI(nDOFTrace, -1);

    // trace element integral
    ElementIntegral<TopoDimTrace, TopologyTrace, ArrayJSurreal> integralJ(quadratureorder);

    // element jacobian matrices
    // Left
    // element functional jacobian vector
    DLA::VectorD<MatrixJ> jacFunctionalElem_qL(nDOFL);
    DLA::VectorD<MatrixJ> jacFunctionalElem_aL(D*nDOFL);
    DLA::VectorD<MatrixJ> jacFunctionalElem_qI(nDOFTrace);

    // loop over elements within group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero element Jacobians
      jacFunctionalElem_qL = 0;
      jacFunctionalElem_aL = 0;
      jacFunctionalElem_qI = 0;

      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      afldCellL.getElement( afldElemL, elemL );

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );


      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nVar*((D+1)*nDOFL + nDOFTrace); nchunk += nDeriv)
      {

        // associate derivative slots

        // wrt qL
        int slot, slotOffset = 0;
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = slotOffset + nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemL.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nVar*nDOFL;


        // wrt aL
        for (int d = 0; d < D; d++)
        {
          for (int j = 0; j < nDOFL; j++)
          {
            for (int n = 0; n < nVar; n++)
            {
              slot = slotOffset + nVar*j + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(afldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 1;
            }
          }
          slotOffset += nVar*nDOFL;
        }


        // wrt qI
        for (int j = 0; j < nDOFTrace; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = slotOffset + nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qIfldElemTrace.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nVar*nDOFTrace;

        // trace integration for canonical element
        ArrayJSurreal functional = 0;
        integralJ( fcnJ_.integrand( fcnBC_,
                                    xfldElemTrace, canonicalTraceL,
                                    xfldElemL, qfldElemL, afldElemL, qIfldElemTrace),
                   get<-1>(xfldElemTrace),
                   functional);

        // accumulate derivatives into element jacobians
        slotOffset = 0;
        // wrt qL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = slotOffset + nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qfldElemL.DOF(j),n).deriv(slot - nchunk) = 0; // Reset the derivative

              DLA::index(jacFunctionalElem_qL[j], n) = DLA::index(functional, n).deriv(slot - nchunk);
            }
          }
        }
        slotOffset += nVar*nDOFL;


        // wrt aL
        for (int d = 0; d < D; d++)
        {
          for (int j = 0; j < nDOFL; j++)
          {
            for (int n = 0; n < nVar; n++)
            {
              slot = slotOffset + nVar*j + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                DLA::index(afldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 0;  // Reset the derivative

                DLA::index(jacFunctionalElem_aL[D*j+d], n) = DLA::index(functional, n).deriv(slot - nchunk);
              }
            }
          }
          slotOffset += nVar*nDOFL;
        }


        // wrt qI
        for (int j = 0; j < nDOFTrace; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = slotOffset + nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qIfldElemTrace.DOF(j),n).deriv(slot - nchunk) = 0;  // Reset the derivative

              DLA::index(jacFunctionalElem_qI[j], n) = DLA::index(functional, n).deriv(slot - nchunk);
            }
          }
        }
        slotOffset += nVar*nDOFTrace;
      }   // nchunk

      // global mapping for qL
      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobal_qL.data(), nDOFL );

      // global mapping for qI
      qIfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobal_qI.data(), nDOFTrace );

      // scatter-add element jacobian to global
      for (int i = 0; i < nDOFL; i++)
        jacFunctional_qL_[mapDOFGlobal_qL[i]] += jacFunctionalElem_qL[i];

      for (int i = 0; i < nDOFL; i++)
        for (int d = 0; d < D; d++)
            jacFunctional_aL_[D*mapDOFGlobal_qL[i] + d] += jacFunctionalElem_aL[D*i + d];

      for (int i = 0; i < nDOFTrace; i++)
        jacFunctional_qI_[mapDOFGlobal_qI[i]] += jacFunctionalElem_qI[i];

    }
  }


protected:
  const FunctionalIntegrandBoundaryTrace& fcnJ_;
  const BCIntegrandBoundaryTrace& fcnBC_;
  std::vector<int> boundaryTraceGroups_;
  Vector<MatrixJ>& jacFunctional_qL_;
  Vector<MatrixJ>& jacFunctional_aL_;
  Vector<MatrixJ>& jacFunctional_qI_;
};

// Factory function

template<class Surreal, class FunctionalIntegrandBoundaryTrace, class BCIntegrandBoundaryTrace, template<class> class Vector, class MatrixJ>
JacobianFunctionalBoundaryTrace_HDG_Test_impl<Surreal, FunctionalIntegrandBoundaryTrace, BCIntegrandBoundaryTrace, Vector>
JacobianFunctionalBoundaryTrace_HDG_Test( const IntegrandBoundaryTraceType<FunctionalIntegrandBoundaryTrace>& fcnJ,
                                          const IntegrandBoundaryTraceType<BCIntegrandBoundaryTrace>& fcnBC,
                                          Vector< MatrixJ >& func_qL,
                                          Vector< MatrixJ >& func_aL,
                                          Vector< MatrixJ >& func_qI)
{
  typedef typename Scalar<MatrixJ>::type T;
  static_assert( std::is_same<typename FunctionalIntegrandBoundaryTrace::template MatrixJ<T>, MatrixJ>::value, "These should be the same");
  return JacobianFunctionalBoundaryTrace_HDG_Test_impl<Surreal, FunctionalIntegrandBoundaryTrace, BCIntegrandBoundaryTrace, Vector>(
      fcnJ.cast(), fcnBC.cast(), func_qL, func_aL, func_qI );
}



} //testspace
}

#endif  // JACOBIANTEST_HDG_H
