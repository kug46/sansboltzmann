// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandInteriorTrace_HDG_NS_btest
// testing of residual integrands for HDG: Navier-Stokes

#include <boost/test/unit_test.hpp>

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementArea.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"

#include "Discretization/HDG/IntegrandCell_HDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandInteriorTrace_HDG_NS_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_2D_Triangle_Triangle_test )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;

  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldTrace;
  typedef Element<DLA::VectorS<2,ArrayQ>,TopoD2,Triangle> ElementAFieldCell;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_HDG<NDPDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementParam> BasisWeightedClass;
  typedef IntegrandClass::BasisWeighted_AUX<Real,TopoD1,Line,TopoD2,Triangle,ElementParam> BasisWeightedAUXClass;

  typedef DiscretizationHDG<NDPDEClass> DiscretizationClass;

  // PDE
  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)

//  const Real muRef = 0.0;      // kg/(m s) // FOR RE = 10
  const Real muRef = 0.1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef); //single argument for constant viscosity
  ThermalConductivityModel tcond(Prandtl, Cp);

  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemR.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;
  x4 = 1;  y4 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xfldElemR.DOF(0) = {x4, y4};
  xfldElemR.DOF(1) = {x3, y3};
  xfldElemR.DOF(2) = {x2, y2};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution

  order = 1;
  ElementQFieldCell qfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell qfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemR.nDOF() );

  // triangle solution (left)
  qfldElemL.DOF(0) = {967./1000., 1./4., 32./100., 107./100.};
  qfldElemL.DOF(1) = {103./100., 23./100., 32./100., 108./100.};
  qfldElemL.DOF(2) = {11./10., 21./100., 36./100., 105./100.};

  // triangle solution (right)
  qfldElemR.DOF(0) = {987./1000., 274./1000., 318./1000., 1064./1000.};
  qfldElemR.DOF(1) = {101./100., 21./100., 37./100., 112./100.};
  qfldElemR.DOF(2) = {12./10., 17./100., 41./100., 98./100.};

  // auxiliary variable

  ElementAFieldCell afldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementAFieldCell afldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, afldElemL.order() );
  BOOST_CHECK_EQUAL( 3, afldElemL.nDOF() );

  afldElemL.DOF(0)(0) = { 8./100., -24./1000., -8./100., -23./1000.};
  afldElemL.DOF(1)(0) = {6./100., 14./1000., 6./100., 23./1000.};
  afldElemL.DOF(2)(0) = {-5./100., -13./1000., -33./1000., 34./1000.};

  afldElemL.DOF(0)(1) = {7./100., 8./100., 12./100., 56./1000.};
  afldElemL.DOF(1)(1) = {-2./100., 8./1000., 5./100., 17./1000.};
  afldElemL.DOF(2)(1) = {2./100., 22./1000., 23./1000., 8./100.};

  afldElemR.DOF(0)(0) = {12./100., 5./100., 17./100., 64./1000.};
  afldElemR.DOF(1)(0) = {-35./1000., 73./1000., 65./1000., 19./1000.};
  afldElemR.DOF(2)(0) = {-4./100., -19./1000., -52./1000., 23./1000.};

  afldElemR.DOF(0)(1) = {2./100., 12./100., 9./100., 68./1000.};
  afldElemR.DOF(1)(1) = {-2./100., 8./1000., 5./100., 17./1000.};
  afldElemR.DOF(2)(1) = {16./1000., 2./100., 29./1000., 56./1000.};

  // interface solution

  ElementQFieldTrace qedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qedge.order() );
  BOOST_CHECK_EQUAL( 2, qedge.nDOF() );

  qedge.DOF(0) =  {11./10., 20./100., 37./100., 103./100.};
  qedge.DOF(1) = {106./100., 21./100., 365./1000., 109./100.};

  // HDG discretization: a = gradq; no stabilization
  DiscretizationClass disc( pde, Nothing, Gradient );

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  BasisWeightedClass fcnPDEL = fcnint.integrand( xedge, qedge, CanonicalTraceToCell(0,1), +1,
                                                 xfldElemL, qfldElemL, afldElemL );

  BasisWeightedClass fcnPDER = fcnint.integrand( xedge, qedge, CanonicalTraceToCell(0,-1), -1,
                                                 xfldElemR, qfldElemR, afldElemR );

  BasisWeightedAUXClass fcnAUXL = fcnint.integrand_AUX( xedge, qedge, CanonicalTraceToCell(0,1), +1,
                                                        xfldElemL, qfldElemL );

  BasisWeightedAUXClass fcnAUXR = fcnint.integrand_AUX( xedge, qedge, CanonicalTraceToCell(0,-1), -1,
                                                        xfldElemR, qfldElemR );

  BOOST_CHECK_EQUAL( 4, fcnPDEL.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcnPDEL.nDOFLeft() );
  BOOST_CHECK( fcnPDEL.needsEvaluation() == true );

  BOOST_CHECK_EQUAL( 4, fcnPDER.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcnPDER.nDOFLeft() );
  BOOST_CHECK( fcnPDER.needsEvaluation() == true );

  BOOST_CHECK_EQUAL( 4, fcnAUXL.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcnAUXL.nDOFLeft() );
  BOOST_CHECK( fcnAUXL.needsEvaluation() == true );

  BOOST_CHECK_EQUAL( 4, fcnAUXR.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcnAUXR.nDOFLeft() );
  BOOST_CHECK( fcnAUXR.needsEvaluation() == true );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-11;
  Real sRef;
  Real integrandPDELTrue[3][4], integrandPDERTrue[3][4], integrandPDEITrue[2][4];
  Real integrandAuxLTrue[3][4], integrandAuyLTrue[3][4];
  Real integrandAuxRTrue[3][4], integrandAuyRTrue[3][4];

  BasisWeightedClass::IntegrandCellType integrandPDEL[3], integrandPDER[3];
  BasisWeightedClass::IntegrandTraceType integrandPDEIL[2], integrandPDEIR[2], integrandPDEI[2];
  BasisWeightedAUXClass::IntegrandType integrandAUXL[3], integrandAUXR[3];

  sRef = 0;
  fcnPDEL( sRef, integrandPDEL, 3, integrandPDEIL, 2 );
  fcnPDER( sRef, integrandPDER, 3, integrandPDEIR, 2 );

  fcnAUXL( sRef, integrandAUXL, 3 );
  fcnAUXR( sRef, integrandAUXR, 3 );

  integrandPDEI[0] = integrandPDEIL[0] + integrandPDEIR[0];
  integrandPDEI[1] = integrandPDEIL[1] + integrandPDEIR[1];

  //PDE residual integrands (left): (advective) + (viscous) + (stabilization)
  integrandPDELTrue[0][0] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
  integrandPDELTrue[0][1] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
  integrandPDELTrue[0][2] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
  integrandPDELTrue[0][3] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
  integrandPDELTrue[1][0] = ( 0.41965731459986344 ) + ( 0 ) + ( 0 ) ;  // Basis function 2
  integrandPDELTrue[1][1] = ( 0.8440077681772281 ) + ( -sqrt(2)/375. ) + ( 0 ) ;  // Basis function 2
  integrandPDELTrue[1][2] = ( 0.8825527245164043 ) + ( -47/(3750.*sqrt(2)) ) + ( 0 ) ;  // Basis function 2
  integrandPDELTrue[1][3] = ( 1.5600798141946257 ) + ( -6446341/(1.491890625e9*sqrt(2)) ) + ( 0 ) ;  // Basis function 2
  integrandPDELTrue[2][0] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 3
  integrandPDELTrue[2][1] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 3
  integrandPDELTrue[2][2] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 3
  integrandPDELTrue[2][3] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 3

  //PDE residual integrands (Right): (advective) + (viscous) + (stabilization)
  integrandPDERTrue[0][0] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
  integrandPDERTrue[0][1] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
  integrandPDERTrue[0][2] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
  integrandPDERTrue[0][3] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
  integrandPDERTrue[1][0] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 2
  integrandPDERTrue[1][1] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 2
  integrandPDERTrue[1][2] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 2
  integrandPDERTrue[1][3] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 2
  integrandPDERTrue[2][0] = ( -0.46021315843020916 ) + ( 0 ) + ( 0 ) ;  // Basis function 3
  integrandPDERTrue[2][1] = ( -0.8069091257375015 ) + ( -23/(3000.*sqrt(2)) ) + ( 0 ) ;  // Basis function 3
  integrandPDERTrue[2][2] = ( -0.885748197105269 ) + ( 29/(15000.*sqrt(2)) ) + ( 0 ) ;  // Basis function 3
  integrandPDERTrue[2][3] = ( -1.5378399273851246 ) + ( 2129299/(5.4e7*sqrt(2)) ) + ( 0 ) ;  // Basis function 3

  //Aux residual integrands (Left): x
  integrandAuxLTrue[0][0] = ( 0 );  // Basis function 1
  integrandAuxLTrue[0][1] = ( 0 );  // Basis function 1
  integrandAuxLTrue[0][2] = ( 0 );  // Basis function 1
  integrandAuxLTrue[0][3] = ( 0 );  // Basis function 1
  integrandAuxLTrue[1][0] = ( -11/(10.*sqrt(2)) );  // Basis function 2
  integrandAuxLTrue[1][1] = ( -1/(5.*sqrt(2)) );  // Basis function 2
  integrandAuxLTrue[1][2] = ( -37/(100.*sqrt(2)) );  // Basis function 2
  integrandAuxLTrue[1][3] = ( -103/(100.*sqrt(2)) );  // Basis function 2
  integrandAuxLTrue[2][0] = ( 0 );  // Basis function 3
  integrandAuxLTrue[2][1] = ( 0 );  // Basis function 3
  integrandAuxLTrue[2][2] = ( 0 );  // Basis function 3
  integrandAuxLTrue[2][3] = ( 0 );  // Basis function 3

  //Aux residual integrands (Left): y
  integrandAuyLTrue[0][0] = ( 0 );  // Basis function 1
  integrandAuyLTrue[0][1] = ( 0 );  // Basis function 1
  integrandAuyLTrue[0][2] = ( 0 );  // Basis function 1
  integrandAuyLTrue[0][3] = ( 0 );  // Basis function 1
  integrandAuyLTrue[1][0] = ( -11/(10.*sqrt(2)) );  // Basis function 2
  integrandAuyLTrue[1][1] = ( -1/(5.*sqrt(2)) );  // Basis function 2
  integrandAuyLTrue[1][2] = ( -37/(100.*sqrt(2)) );  // Basis function 2
  integrandAuyLTrue[1][3] = ( -103/(100.*sqrt(2)) );  // Basis function 2
  integrandAuyLTrue[2][0] = ( 0 );  // Basis function 3
  integrandAuyLTrue[2][1] = ( 0 );  // Basis function 3
  integrandAuyLTrue[2][2] = ( 0 );  // Basis function 3
  integrandAuyLTrue[2][3] = ( 0 );  // Basis function 3

  //Aux residual integrands (Right): x
  integrandAuxRTrue[0][0] = ( 0 );  // Basis function 1
  integrandAuxRTrue[0][1] = ( 0 );  // Basis function 1
  integrandAuxRTrue[0][2] = ( 0 );  // Basis function 1
  integrandAuxRTrue[0][3] = ( 0 );  // Basis function 1
  integrandAuxRTrue[1][0] = ( 0 );  // Basis function 2
  integrandAuxRTrue[1][1] = ( 0 );  // Basis function 2
  integrandAuxRTrue[1][2] = ( 0 );  // Basis function 2
  integrandAuxRTrue[1][3] = ( 0 );  // Basis function 2
  integrandAuxRTrue[2][0] = ( 11/(10.*sqrt(2)) );  // Basis function 3
  integrandAuxRTrue[2][1] = ( 1/(5.*sqrt(2)) );  // Basis function 3
  integrandAuxRTrue[2][2] = ( 37/(100.*sqrt(2)) );  // Basis function 3
  integrandAuxRTrue[2][3] = ( 103/(100.*sqrt(2)) );  // Basis function 3

  //Aux residual integrands (Left): y
  integrandAuyRTrue[0][0] = ( 0 );  // Basis function 1
  integrandAuyRTrue[0][1] = ( 0 );  // Basis function 1
  integrandAuyRTrue[0][2] = ( 0 );  // Basis function 1
  integrandAuyRTrue[0][3] = ( 0 );  // Basis function 1
  integrandAuyRTrue[1][0] = ( 0 );  // Basis function 2
  integrandAuyRTrue[1][1] = ( 0 );  // Basis function 2
  integrandAuyRTrue[1][2] = ( 0 );  // Basis function 2
  integrandAuyRTrue[1][3] = ( 0 );  // Basis function 2
  integrandAuyRTrue[2][0] = ( 11/(10.*sqrt(2)) );  // Basis function 3
  integrandAuyRTrue[2][1] = ( 1/(5.*sqrt(2)) );  // Basis function 3
  integrandAuyRTrue[2][2] = ( 37/(100.*sqrt(2)) );  // Basis function 3
  integrandAuyRTrue[2][3] = ( 103/(100.*sqrt(2)) );  // Basis function 3

  // Interface Residuals
  integrandPDEITrue[0][0] = -( -0.04055584383034566 );  // Basis function 1
  integrandPDEITrue[0][1] = -( 0.02790625428430153 );  // Basis function 1
  integrandPDEITrue[0][2] = -( -0.010690804469441956 );  // Basis function 1
  integrandPDEITrue[0][3] = -( 0.04706678937444923 );  // Basis function 1
  integrandPDEITrue[1][0] = -( 0. );  // Basis function 2
  integrandPDEITrue[1][1] = -( 0. );  // Basis function 2
  integrandPDEITrue[1][2] = -( 0. );  // Basis function 2
  integrandPDEITrue[1][3] = -( 0. );  // Basis function 2

  for (int i=0; i< qfldElemL.nDOF(); i++)
    for (int j=0; j< fcnPDEL.nEqn(); j++)
    {
      SANS_CHECK_CLOSE( integrandPDELTrue[i][j], integrandPDEL[i][j], small_tol, close_tol );
      SANS_CHECK_CLOSE( integrandPDERTrue[i][j], integrandPDER[i][j], small_tol, close_tol );
    }

  for (int i=0; i< qfldElemL.nDOF(); i++)
    for (int j=0; j< fcnAUXL.nEqn(); j++)
    {
      SANS_CHECK_CLOSE( integrandAuxLTrue[i][j], integrandAUXL[i][0][j], small_tol, close_tol );
      SANS_CHECK_CLOSE( integrandAuyLTrue[i][j], integrandAUXL[i][1][j], small_tol, close_tol );

      SANS_CHECK_CLOSE( integrandAuxRTrue[i][j], integrandAUXR[i][0][j], small_tol, close_tol );
      SANS_CHECK_CLOSE( integrandAuyRTrue[i][j], integrandAUXR[i][1][j], small_tol, close_tol );
    }

  for (int i=0; i< qedge.nDOF(); i++)
    for (int j=0; j< fcnPDEL.nEqn(); j++)
    {
      SANS_CHECK_CLOSE( integrandPDEITrue[i][j], integrandPDEI[i][j], small_tol, close_tol );
    }

//////

  sRef = 1;
  fcnPDEL( sRef, integrandPDEL, 3, integrandPDEIL, 2 );
  fcnPDER( sRef, integrandPDER, 3, integrandPDEIR, 2 );

  fcnAUXL( sRef, integrandAUXL, 3 );
  fcnAUXR( sRef, integrandAUXR, 3 );

  integrandPDEI[0] = integrandPDEIL[0] + integrandPDEIR[0];
  integrandPDEI[1] = integrandPDEIL[1] + integrandPDEIR[1];

  //PDE residual integrands (left): (advective) + (viscous) + (stabilization)
  integrandPDELTrue[0][0] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
  integrandPDELTrue[0][1] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
  integrandPDELTrue[0][2] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
  integrandPDELTrue[0][3] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
  integrandPDELTrue[1][0] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 2
  integrandPDELTrue[1][1] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 2
  integrandPDELTrue[1][2] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 2
  integrandPDELTrue[1][3] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 2
  integrandPDELTrue[2][0] = ( 0.43346478563182006 ) + ( 0 ) + ( 0 ) ;  // Basis function 3
  integrandPDELTrue[2][1] = ( 0.841728289737883 ) + ( 131/(30000.*sqrt(2)) ) + ( 0 ) ;  // Basis function 3
  integrandPDELTrue[2][2] = ( 0.9067235086490387 ) + ( -17/(6000.*sqrt(2)) ) + ( 0 ) ;  // Basis function 3
  integrandPDELTrue[2][3] = ( 1.4892554983764934 ) + ( -22918639/(3.63e8*sqrt(2)) ) + ( 0 ) ;  // Basis function 3

  //PDE residual integrands (Right): (advective) + (viscous) + (stabilization)
  integrandPDERTrue[0][0] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
  integrandPDERTrue[0][1] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
  integrandPDERTrue[0][2] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
  integrandPDERTrue[0][3] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
  integrandPDERTrue[1][0] = ( -0.42402949124808165 ) + ( 0 ) + ( 0 ) ;  // Basis function 2
  integrandPDERTrue[1][1] = ( -0.8658039709490782 ) + ( 137/(10000.*sqrt(2)) ) + ( 0 ) ;  // Basis function 2
  integrandPDERTrue[1][2] = ( -0.9315113717996001 ) + ( 91/(10000.*sqrt(2)) ) + ( 0 ) ;  // Basis function 2
  integrandPDERTrue[1][3] = ( -1.5661978832595382 ) + ( 1214751349/(2.295225e10*sqrt(2)) ) + ( 0 ) ;  // Basis function 2
  integrandPDERTrue[2][0] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 3
  integrandPDERTrue[2][1] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 3
  integrandPDERTrue[2][2] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 3
  integrandPDERTrue[2][3] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 3

  //Aux residual integrands (Left): x
  integrandAuxLTrue[0][0] = ( 0 );  // Basis function 1
  integrandAuxLTrue[0][1] = ( 0 );  // Basis function 1
  integrandAuxLTrue[0][2] = ( 0 );  // Basis function 1
  integrandAuxLTrue[0][3] = ( 0 );  // Basis function 1
  integrandAuxLTrue[1][0] = ( 0 );  // Basis function 2
  integrandAuxLTrue[1][1] = ( 0 );  // Basis function 2
  integrandAuxLTrue[1][2] = ( 0 );  // Basis function 2
  integrandAuxLTrue[1][3] = ( 0 );  // Basis function 2
  integrandAuxLTrue[2][0] = ( -53/(50.*sqrt(2)) );  // Basis function 3
  integrandAuxLTrue[2][1] = ( -21/(100.*sqrt(2)) );  // Basis function 3
  integrandAuxLTrue[2][2] = ( -73/(200.*sqrt(2)) );  // Basis function 3
  integrandAuxLTrue[2][3] = ( -109/(100.*sqrt(2)) );  // Basis function 3

  //Aux residual integrands (Left): y
  integrandAuyLTrue[0][0] = ( 0 );  // Basis function 1
  integrandAuyLTrue[0][1] = ( 0 );  // Basis function 1
  integrandAuyLTrue[0][2] = ( 0 );  // Basis function 1
  integrandAuyLTrue[0][3] = ( 0 );  // Basis function 1
  integrandAuyLTrue[1][0] = ( 0 );  // Basis function 2
  integrandAuyLTrue[1][1] = ( 0 );  // Basis function 2
  integrandAuyLTrue[1][2] = ( 0 );  // Basis function 2
  integrandAuyLTrue[1][3] = ( 0 );  // Basis function 2
  integrandAuyLTrue[2][0] = ( -53/(50.*sqrt(2)) );  // Basis function 3
  integrandAuyLTrue[2][1] = ( -21/(100.*sqrt(2)) );  // Basis function 3
  integrandAuyLTrue[2][2] = ( -73/(200.*sqrt(2)) );  // Basis function 3
  integrandAuyLTrue[2][3] = ( -109/(100.*sqrt(2)) );  // Basis function 3

  //Aux residual integrands (Right): x
  integrandAuxRTrue[0][0] = ( 0 );  // Basis function 1
  integrandAuxRTrue[0][1] = ( 0 );  // Basis function 1
  integrandAuxRTrue[0][2] = ( 0 );  // Basis function 1
  integrandAuxRTrue[0][3] = ( 0 );  // Basis function 1
  integrandAuxRTrue[1][0] = ( 53/(50.*sqrt(2)) );  // Basis function 2
  integrandAuxRTrue[1][1] = ( 21/(100.*sqrt(2)) );  // Basis function 2
  integrandAuxRTrue[1][2] = ( 73/(200.*sqrt(2)) );  // Basis function 2
  integrandAuxRTrue[1][3] = ( 109/(100.*sqrt(2)) );  // Basis function 2
  integrandAuxRTrue[2][0] = ( 0 );  // Basis function 3
  integrandAuxRTrue[2][1] = ( 0 );  // Basis function 3
  integrandAuxRTrue[2][2] = ( 0 );  // Basis function 3
  integrandAuxRTrue[2][3] = ( 0 );  // Basis function 3

  //Aux residual integrands (Left): y
  integrandAuyRTrue[0][0] = ( 0 );  // Basis function 1
  integrandAuyRTrue[0][1] = ( 0 );  // Basis function 1
  integrandAuyRTrue[0][2] = ( 0 );  // Basis function 1
  integrandAuyRTrue[0][3] = ( 0 );  // Basis function 1
  integrandAuyRTrue[1][0] = ( 53/(50.*sqrt(2)) );  // Basis function 2
  integrandAuyRTrue[1][1] = ( 21/(100.*sqrt(2)) );  // Basis function 2
  integrandAuyRTrue[1][2] = ( 73/(200.*sqrt(2)) );  // Basis function 2
  integrandAuyRTrue[1][3] = ( 109/(100.*sqrt(2)) );  // Basis function 2
  integrandAuyRTrue[2][0] = ( 0 );  // Basis function 3
  integrandAuyRTrue[2][1] = ( 0 );  // Basis function 3
  integrandAuyRTrue[2][2] = ( 0 );  // Basis function 3
  integrandAuyRTrue[2][3] = ( 0 );  // Basis function 3

  //Interior Trace Integrands
  integrandPDEITrue[0][0] = -( 0. );  // Basis function 1
  integrandPDEITrue[0][1] = -( 0. );  // Basis function 1
  integrandPDEITrue[0][2] = -( 0. );  // Basis function 1
  integrandPDEITrue[0][3] = -( 0. );  // Basis function 1
  integrandPDEITrue[1][0] = -( 0.009435294383738399 );  // Basis function 2
  integrandPDEITrue[1][1] = -( -0.011300618697758169 );  // Basis function 2
  integrandPDEITrue[1][2] = -( -0.020356660655125543 );  // Basis function 2
  integrandPDEITrue[1][3] = -( -0.08416307174698437 );  // Basis function 2

  for (int i=0; i< qfldElemL.nDOF(); i++)
    for (int j=0; j< fcnPDEL.nEqn(); j++)
    {
      SANS_CHECK_CLOSE( integrandPDELTrue[i][j], integrandPDEL[i][j], small_tol, close_tol );
      SANS_CHECK_CLOSE( integrandPDERTrue[i][j], integrandPDER[i][j], small_tol, close_tol );
    }

  for (int i=0; i< qfldElemL.nDOF(); i++)
    for (int j=0; j< fcnAUXL.nEqn(); j++)
    {
      SANS_CHECK_CLOSE( integrandAuxLTrue[i][j], integrandAUXL[i][0][j], small_tol, close_tol );
      SANS_CHECK_CLOSE( integrandAuyLTrue[i][j], integrandAUXL[i][1][j], small_tol, close_tol );

      SANS_CHECK_CLOSE( integrandAuxRTrue[i][j], integrandAUXR[i][0][j], small_tol, close_tol );
      SANS_CHECK_CLOSE( integrandAuyRTrue[i][j], integrandAUXR[i][1][j], small_tol, close_tol );
    }

  for (int i=0; i< qedge.nDOF(); i++)
    for (int j=0; j< fcnPDEL.nEqn(); j++)
    {
      SANS_CHECK_CLOSE( integrandPDEITrue[i][j], integrandPDEI[i][j], small_tol, close_tol );
    }

  //////

    sRef = 0.5;
    fcnPDEL( sRef, integrandPDEL, 3, integrandPDEIL, 2 );
    fcnPDER( sRef, integrandPDER, 3, integrandPDEIR, 2 );

    fcnAUXL( sRef, integrandAUXL, 3 );
    fcnAUXR( sRef, integrandAUXR, 3 );

    integrandPDEI[0] = integrandPDEIL[0] + integrandPDEIR[0];
    integrandPDEI[1] = integrandPDEIL[1] + integrandPDEIR[1];

    //PDE residual integrands (Left): (advective) + (viscous) + (stabilization)
    integrandPDELTrue[0][0] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
    integrandPDELTrue[0][1] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
    integrandPDELTrue[0][2] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
    integrandPDELTrue[0][3] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
    integrandPDELTrue[1][0] = ( 0.2131238845355758 ) + ( 0 ) + ( 0 ) ;  // Basis function 2
    integrandPDELTrue[1][1] = ( 0.42151862539965523 ) + ( -29/(120000.*sqrt(2)) ) + ( 0 ) ;  // Basis function 2
    integrandPDELTrue[1][2] = ( 0.44714178346119154 ) + ( -461/(120000.*sqrt(2)) ) + ( 0 ) ;  // Basis function 2
    integrandPDELTrue[1][3] = ( 0.7622755804642669 ) + ( -1894769/(1.065e8*sqrt(2)) ) + ( 0 ) ;  // Basis function 2
    integrandPDELTrue[2][0] = ( 0.2131238845355758 ) + ( 0 ) + ( 0 ) ;  // Basis function 3
    integrandPDELTrue[2][1] = ( 0.42151862539965523 ) + ( -29/(120000.*sqrt(2)) ) + ( 0 ) ;  // Basis function 3
    integrandPDELTrue[2][2] = ( 0.44714178346119154 ) + ( -461/(120000.*sqrt(2)) ) + ( 0 ) ;  // Basis function 3
    integrandPDELTrue[2][3] = ( 0.7622755804642669 ) + ( -1894769/(1.065e8*sqrt(2)) ) + ( 0 ) ;  // Basis function 3

    //PDE residual integrands (Right): (advective) + (viscous) + (stabilization)
    integrandPDERTrue[0][0] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
    integrandPDERTrue[0][1] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
    integrandPDERTrue[0][2] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
    integrandPDERTrue[0][3] = ( 0. ) + ( 0 ) + ( 0 ) ;  // Basis function 1
    integrandPDERTrue[1][0] = ( -0.22093942592450258 ) + ( 0 ) + ( 0 ) ;  // Basis function 2
    integrandPDERTrue[1][1] = ( -0.418119707057531 ) + ( 181/(120000.*sqrt(2)) ) + ( 0 ) ;  // Basis function 2
    integrandPDERTrue[1][2] = ( -0.4540664400816798 ) + ( 331/(120000.*sqrt(2)) ) + ( 0 ) ;  // Basis function 2
    integrandPDERTrue[1][3] = ( -0.7773004136611313 ) + ( 9787433251/(4.39569e11*sqrt(2)) ) + ( 0 ) ;  // Basis function 2
    integrandPDERTrue[2][0] = ( -0.22093942592450258 ) + ( 0 ) + ( 0 ) ;  // Basis function 3
    integrandPDERTrue[2][1] = ( -0.418119707057531 ) + ( 181/(120000.*sqrt(2)) ) + ( 0 ) ;  // Basis function 3
    integrandPDERTrue[2][2] = ( -0.4540664400816798 ) + ( 331/(120000.*sqrt(2)) ) + ( 0 ) ;  // Basis function 3
    integrandPDERTrue[2][3] = ( -0.7773004136611313 ) + ( 9787433251/(4.39569e11*sqrt(2)) ) + ( 0 ) ;  // Basis function 3

    //Aux residual integrands (Left): x
    integrandAuxLTrue[0][0] = ( 0 );  // Basis function 1
    integrandAuxLTrue[0][1] = ( 0 );  // Basis function 1
    integrandAuxLTrue[0][2] = ( 0 );  // Basis function 1
    integrandAuxLTrue[0][3] = ( 0 );  // Basis function 1
    integrandAuxLTrue[1][0] = ( -27/(50.*sqrt(2)) );  // Basis function 2
    integrandAuxLTrue[1][1] = ( -41/(400.*sqrt(2)) );  // Basis function 2
    integrandAuxLTrue[1][2] = ( -147/(800.*sqrt(2)) );  // Basis function 2
    integrandAuxLTrue[1][3] = ( -53/(100.*sqrt(2)) );  // Basis function 2
    integrandAuxLTrue[2][0] = ( -27/(50.*sqrt(2)) );  // Basis function 3
    integrandAuxLTrue[2][1] = ( -41/(400.*sqrt(2)) );  // Basis function 3
    integrandAuxLTrue[2][2] = ( -147/(800.*sqrt(2)) );  // Basis function 3
    integrandAuxLTrue[2][3] = ( -53/(100.*sqrt(2)) );  // Basis function 3

    //Aux residual integrands (Left): y
    integrandAuyLTrue[0][0] = ( 0 );  // Basis function 1
    integrandAuyLTrue[0][1] = ( 0 );  // Basis function 1
    integrandAuyLTrue[0][2] = ( 0 );  // Basis function 1
    integrandAuyLTrue[0][3] = ( 0 );  // Basis function 1
    integrandAuyLTrue[1][0] = ( -27/(50.*sqrt(2)) );  // Basis function 2
    integrandAuyLTrue[1][1] = ( -41/(400.*sqrt(2)) );  // Basis function 2
    integrandAuyLTrue[1][2] = ( -147/(800.*sqrt(2)) );  // Basis function 2
    integrandAuyLTrue[1][3] = ( -53/(100.*sqrt(2)) );  // Basis function 2
    integrandAuyLTrue[2][0] = ( -27/(50.*sqrt(2)) );  // Basis function 3
    integrandAuyLTrue[2][1] = ( -41/(400.*sqrt(2)) );  // Basis function 3
    integrandAuyLTrue[2][2] = ( -147/(800.*sqrt(2)) );  // Basis function 3
    integrandAuyLTrue[2][3] = ( -53/(100.*sqrt(2)) );  // Basis function 3

    //Aux residual integrands (Right): x
    integrandAuxRTrue[0][0] = ( 0 );  // Basis function 1
    integrandAuxRTrue[0][1] = ( 0 );  // Basis function 1
    integrandAuxRTrue[0][2] = ( 0 );  // Basis function 1
    integrandAuxRTrue[0][3] = ( 0 );  // Basis function 1
    integrandAuxRTrue[1][0] = ( 27/(50.*sqrt(2)) );  // Basis function 2
    integrandAuxRTrue[1][1] = ( 41/(400.*sqrt(2)) );  // Basis function 2
    integrandAuxRTrue[1][2] = ( 147/(800.*sqrt(2)) );  // Basis function 2
    integrandAuxRTrue[1][3] = ( 53/(100.*sqrt(2)) );  // Basis function 2
    integrandAuxRTrue[2][0] = ( 27/(50.*sqrt(2)) );  // Basis function 3
    integrandAuxRTrue[2][1] = ( 41/(400.*sqrt(2)) );  // Basis function 3
    integrandAuxRTrue[2][2] = ( 147/(800.*sqrt(2)) );  // Basis function 3
    integrandAuxRTrue[2][3] = ( 53/(100.*sqrt(2)) );  // Basis function 3

    //Aux residual integrands (Left): y
    integrandAuyRTrue[0][0] = ( 0 );  // Basis function 1
    integrandAuyRTrue[0][1] = ( 0 );  // Basis function 1
    integrandAuyRTrue[0][2] = ( 0 );  // Basis function 1
    integrandAuyRTrue[0][3] = ( 0 );  // Basis function 1
    integrandAuyRTrue[1][0] = ( 27/(50.*sqrt(2)) );  // Basis function 2
    integrandAuyRTrue[1][1] = ( 41/(400.*sqrt(2)) );  // Basis function 2
    integrandAuyRTrue[1][2] = ( 147/(800.*sqrt(2)) );  // Basis function 2
    integrandAuyRTrue[1][3] = ( 53/(100.*sqrt(2)) );  // Basis function 2
    integrandAuyRTrue[2][0] = ( 27/(50.*sqrt(2)) );  // Basis function 3
    integrandAuyRTrue[2][1] = ( 41/(400.*sqrt(2)) );  // Basis function 3
    integrandAuyRTrue[2][2] = ( 147/(800.*sqrt(2)) );  // Basis function 3
    integrandAuyRTrue[2][3] = ( 53/(100.*sqrt(2)) );  // Basis function 3

    //Interior Trace Integrands
    integrandPDEITrue[0][0] = -( -0.007815541388926785 );  // Basis function 1
    integrandPDEITrue[0][1] = -( 0.004294586931627202 );  // Basis function 1
    integrandPDEITrue[0][2] = -( -0.007690688966773606 );  // Basis function 1
    integrandPDEITrue[0][3] = -( -0.011860729189368504 );  // Basis function 1
    integrandPDEITrue[1][0] = -( -0.007815541388926785 );  // Basis function 2
    integrandPDEITrue[1][1] = -( 0.004294586931627202 );  // Basis function 2
    integrandPDEITrue[1][2] = -( -0.007690688966773606 );  // Basis function 2
    integrandPDEITrue[1][3] = -( -0.011860729189368504 );  // Basis function 2

    for (int i=0; i< qfldElemL.nDOF(); i++)
      for (int j=0; j< fcnPDEL.nEqn(); j++)
      {
        SANS_CHECK_CLOSE( integrandPDELTrue[i][j], integrandPDEL[i][j], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandPDERTrue[i][j], integrandPDER[i][j], small_tol, close_tol );
      }

    for (int i=0; i< qfldElemL.nDOF(); i++)
      for (int j=0; j< fcnAUXL.nEqn(); j++)
      {
        SANS_CHECK_CLOSE( integrandAuxLTrue[i][j], integrandAUXL[i][0][j], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandAuyLTrue[i][j], integrandAUXL[i][1][j], small_tol, close_tol );

        SANS_CHECK_CLOSE( integrandAuxRTrue[i][j], integrandAUXR[i][0][j], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandAuyRTrue[i][j], integrandAUXR[i][1][j], small_tol, close_tol );
      }

    for (int i=0; i< qedge.nDOF(); i++)
      for (int j=0; j< fcnPDEL.nEqn(); j++)
      {
        SANS_CHECK_CLOSE( integrandPDEITrue[i][j], integrandPDEI[i][j], small_tol, close_tol );
      }

    //
    //
    //  // test the element integral of the functor
    //
    int quadratureorder = -1;
    int nIntegrandL = qfldElemL.nDOF();
    int nIntegrandR = qfldElemR.nDOF();
    int nIntegrandTrace = qedge.nDOF();
    typedef BasisWeightedClass::IntegrandCellType CellIntegrandType;
    typedef BasisWeightedClass::IntegrandTraceType TraceIntegrandType;
    typedef BasisWeightedAUXClass::IntegrandType AUXIntegrandType;

    GalerkinWeightedIntegral<TopoD1, Line, CellIntegrandType, TraceIntegrandType>
      integralPDEL(quadratureorder, nIntegrandL, nIntegrandTrace);

    GalerkinWeightedIntegral<TopoD1, Line, CellIntegrandType, TraceIntegrandType>
      integralPDER(quadratureorder, nIntegrandR, nIntegrandTrace);

    GalerkinWeightedIntegral<TopoD1, Line, AUXIntegrandType> integralAUXL(quadratureorder, nIntegrandL);
    GalerkinWeightedIntegral<TopoD1, Line, AUXIntegrandType> integralAUXR(quadratureorder, nIntegrandR);

    CellIntegrandType rsdPDEElemL[3], rsdPDEElemR[3];
    TraceIntegrandType rsdPDEElemTraceL[2], rsdPDEElemTraceR[2], rsdPDEElemTrace[2];
    AUXIntegrandType rsdAUXElemL[3], rsdAUXElemR[3];

    Real rsdPDETrueL[3][4];
    Real rsdPDETrueR[3][4];
    Real rsdIntTrue[2][4];
    Real rsdAuxTrueL[3][4];
    Real rsdAuyTrueL[3][4];
    Real rsdAuxTrueR[3][4];
    Real rsdAuyTrueR[3][4];

      // cell integration for canonical element
    integralPDEL( fcnPDEL, xedge, rsdPDEElemL, nIntegrandL, rsdPDEElemTraceL, nIntegrandTrace );
    integralPDER( fcnPDER, xedge, rsdPDEElemR, nIntegrandR, rsdPDEElemTraceR, nIntegrandTrace );

    integralAUXL( fcnAUXL, xedge, rsdAUXElemL, nIntegrandL );
    integralAUXR( fcnAUXR, xedge, rsdAUXElemR, nIntegrandR );

    rsdPDEElemTrace[0] = rsdPDEElemTraceL[0] + rsdPDEElemTraceR[0];
    rsdPDEElemTrace[1] = rsdPDEElemTraceL[1] + rsdPDEElemTraceR[1];

    //PDEL residual: (advection) + (diffusion)
    rsdPDETrueL[0][0] = ( 0. ) + ( 0 )  + ( 0 );
    rsdPDETrueL[0][1] = ( 0. ) + ( 0 )  + ( 0 );
    rsdPDETrueL[0][2] = ( 0. ) + ( 0 )  + ( 0 );
    rsdPDETrueL[0][3] = ( 0. ) + ( 0 )  + ( 0 );
    rsdPDETrueL[1][0] = ( 0.2998495944572331 ) + ( 0 )  + ( 0 );
    rsdPDETrueL[1][1] = ( 0.5963461835769586 ) + ( -21./20000. )  + ( 0 );
    rsdPDETrueL[1][2] = ( 0.6295895313459524 ) + ( -93./20000. )  + ( 0 );
    rsdPDETrueL[1][3] = ( 1.0863932416950084 ) + ( -0.012586842908285 )  + ( 0 );
    rsdPDETrueL[2][0] = ( 0.3031034795780546 ) + ( 0 )  + ( 0 );
    rsdPDETrueL[2][1] = ( 0.5958087658373886 ) + ( 17./30000. )  + ( 0 );
    rsdPDETrueL[2][2] = ( 0.635285549766673 ) + ( -91./30000. )  + ( 0 );
    rsdPDETrueL[2][3] = ( 1.0697025387106398 ) + ( -0.022378256784125 )  + ( 0 );

    //PDER residual: (advection) + (diffusion)
    rsdPDETrueR[0][0] = ( 0. ) + ( 0 )  + ( 0 );
    rsdPDETrueR[0][1] = ( 0. ) + ( 0 )  + ( 0 );
    rsdPDETrueR[0][2] = ( 0. ) + ( 0 )  + ( 0 );
    rsdPDETrueR[0][3] = ( 0. ) + ( 0 )  + ( 0 );
    rsdPDETrueR[1][0] = ( -0.30824727649658934 ) + ( 0 )  + ( 0 );
    rsdPDETrueR[1][1] = ( -0.5982786388892393 ) + ( 37./11250. )  + ( 0 );
    rsdPDETrueR[1][2] = ( -0.6476576499522043 ) + ( 151./45000. )  + ( 0 );
    rsdPDETrueR[1][3] = ( -1.102013196804855 ) + ( 0.023647217042196 )  + ( 0 );
    rsdPDETrueR[2][0] = ( -0.3167781016677913 ) + ( 0 )  + ( 0 );
    rsdPDETrueR[2][1] = ( -0.5843976107861967 ) + ( -49./180000. )  + ( 0 );
    rsdPDETrueR[2][2] = ( -0.6368704120030171 ) + ( 389./180000. )  + ( 0 );
    rsdPDETrueR[2][3] = ( -1.0953085174421242 ) + ( 0.021429625012999 )  + ( 0 );

    //AUXL residual: (advection) + (diffusion)
    rsdAuxTrueL[0][0] = ( 0 );
    rsdAuxTrueL[0][1] = ( 0 );
    rsdAuxTrueL[0][2] = ( 0 );
    rsdAuxTrueL[0][3] = ( 0 );
    rsdAuxTrueL[1][0] = ( -163./300. );
    rsdAuxTrueL[1][1] = ( -61./600. );
    rsdAuxTrueL[1][2] = ( -221./1200. );
    rsdAuxTrueL[1][3] = ( -21./40. );
    rsdAuxTrueL[2][0] = ( -161./300. );
    rsdAuxTrueL[2][1] = ( -31./300. );
    rsdAuxTrueL[2][2] = ( -11./60. );
    rsdAuxTrueL[2][3] = ( -107./200. );

    rsdAuyTrueL[0][0] = ( 0 );
    rsdAuyTrueL[0][1] = ( 0 );
    rsdAuyTrueL[0][2] = ( 0 );
    rsdAuyTrueL[0][3] = ( 0 );
    rsdAuyTrueL[1][0] = ( -163./300. );
    rsdAuyTrueL[1][1] = ( -61./600. );
    rsdAuyTrueL[1][2] = ( -221./1200. );
    rsdAuyTrueL[1][3] = ( -21./40. );
    rsdAuyTrueL[2][0] = ( -161./300. );
    rsdAuyTrueL[2][1] = ( -31./300. );
    rsdAuyTrueL[2][2] = ( -11./60. );
    rsdAuyTrueL[2][3] = ( -107./200. );

    //AUXR residual: (advection) + (diffusion)
    rsdAuxTrueR[0][0] = ( 0 );
    rsdAuxTrueR[0][1] = ( 0 );
    rsdAuxTrueR[0][2] = ( 0 );
    rsdAuxTrueR[0][3] = ( 0 );
    rsdAuxTrueR[1][0] = ( 161./300. );
    rsdAuxTrueR[1][1] = ( 31./300. );
    rsdAuxTrueR[1][2] = ( 11./60. );
    rsdAuxTrueR[1][3] = ( 107./200. );
    rsdAuxTrueR[2][0] = ( 163./300. );
    rsdAuxTrueR[2][1] = ( 61./600. );
    rsdAuxTrueR[2][2] = ( 221./1200. );
    rsdAuxTrueR[2][3] = ( 21./40. );

    rsdAuyTrueR[0][0] = ( 0 );
    rsdAuyTrueR[0][1] = ( 0 );
    rsdAuyTrueR[0][2] = ( 0 );
    rsdAuyTrueR[0][3] = ( 0 );
    rsdAuyTrueR[1][0] = ( 161./300. );
    rsdAuyTrueR[1][1] = ( 31./300. );
    rsdAuyTrueR[1][2] = ( 11./60. );
    rsdAuyTrueR[1][3] = ( 107./200. );
    rsdAuyTrueR[2][0] = ( 163./300. );
    rsdAuyTrueR[2][1] = ( 61./600. );
    rsdAuyTrueR[2][2] = ( 221./1200. );
    rsdAuyTrueR[2][3] = ( 21./40. );

    rsdIntTrue[0][0] = -( -0.016928507210558212 );
    rsdIntTrue[0][1] = -( 0.010626350568539743 );
    rsdIntTrue[0][2] = -( -0.009769769545953644 );
    rsdIntTrue[0][3] = -( -7.24936423999591e-05 );
    rsdIntTrue[1][0] = -( -0.005143796918534765 );
    rsdIntTrue[1][1] = -( 0.0013856825037045829 );
    rsdIntTrue[1][2] = -( -0.012049877963309051 );
    rsdIntTrue[1][3] = -( -0.031041697836147734 );

    const Real small_tol_rsd = 1e-11;
    const Real close_tol_rsd = 1e-9;

    for (int i=0; i< fcnPDEL.nDOFLeft(); i++)
      for (int j=0; j< fcnPDEL.nEqn(); j++)
      {
        SANS_CHECK_CLOSE( rsdPDETrueL[i][j], rsdPDEElemL[i][j], small_tol_rsd, close_tol_rsd );
        SANS_CHECK_CLOSE( rsdPDETrueR[i][j], rsdPDEElemR[i][j], small_tol_rsd, close_tol_rsd );
      }

    for (int i=0; i< fcnAUXL.nDOFLeft(); i++)
      for (int j=0; j< fcnAUXL.nEqn(); j++)
      {
        SANS_CHECK_CLOSE( rsdAuxTrueL[i][j], rsdAUXElemL[i][0][j], small_tol_rsd, close_tol_rsd );
        SANS_CHECK_CLOSE( rsdAuyTrueL[i][j], rsdAUXElemL[i][1][j], small_tol_rsd, close_tol_rsd );

        SANS_CHECK_CLOSE( rsdAuxTrueR[i][j], rsdAUXElemR[i][0][j], small_tol_rsd, close_tol_rsd );
        SANS_CHECK_CLOSE( rsdAuyTrueR[i][j], rsdAUXElemR[i][1][j], small_tol_rsd, close_tol_rsd );
      }

    for (int i=0; i< fcnPDEL.nDOFTrace(); i++)
      for (int j=0; j< fcnPDEL.nEqn(); j++)
      {
        SANS_CHECK_CLOSE( rsdIntTrue[i][j], rsdPDEElemTrace[i][j], small_tol_rsd, close_tol_rsd );
      }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
