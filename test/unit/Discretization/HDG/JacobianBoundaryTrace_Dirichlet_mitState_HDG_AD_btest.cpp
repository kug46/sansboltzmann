// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianBoundaryTrace_HDG_AD_btest
// testing of HDG boundary trace-integral jacobian: advection-diffusion

#include <boost/mpl/list.hpp>

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_Trace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"

#include "Field/XField_CellToTrace.h"

#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_Flux_mitState_HDG.h"
#include "Discretization/HDG/SetFieldCell_HDG_AuxiliaryVariable.h"
#include "Discretization/HDG/SetFieldBoundaryTrace_HDG_AuxiliaryVariable.h"

#include "Discretization/HDG/ResidualCell_HDG.h"
#include "Discretization/HDG/JacobianCell_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_HDG_AuxiliaryVariable.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/QuadratureOrder.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#include "IntegrandTest_HDG.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct
namespace           // local definition
{
template<class SurrealClass, class IntegrandCell, class IntegrandITrace, class IntegrandBTrace,
         class JacAUXClass,
         class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
void computeAuxiliaryVariables(const IntegrandCell& fcnCell, const IntegrandITrace& fcnITrace, const IntegrandBTrace& fcnBTrace,
                               const FieldDataInvMassMatrix_Cell& mmfld, JacAUXClass& jacAUX_a_bcell,
                               const XField_CellToTrace<PhysDim, TopoDim>& connectivity,
                               const XField<PhysDim, TopoDim>& xfld, const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                               const Field<PhysDim, TopoDim, VectorArrayQ>& afld, const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                               const QuadratureOrder& quadOrder)
{
  jacAUX_a_bcell = 0.0;

  FieldDataVectorD_BoundaryCell<VectorArrayQ> rsdAUX_bcell(qfld);
  rsdAUX_bcell = 0.0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
      SetFieldBoundaryTrace_HDG_AuxiliaryVariable( fcnBTrace, rsdAUX_bcell, jacAUX_a_bcell ),
      xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

  std::vector<int> cellgroupsAux(fcnCell.nCellGroups());
  for (std::size_t i = 0; i < fcnCell.nCellGroups(); i++)
    cellgroupsAux[i] = fcnCell.cellGroup(i);

  //Note: jacAUX_a_bcell gets completed and inverted inside SetFieldCell_HDG_AuxiliaryVariable
  IntegrateCellGroups<TopoDim>::integrate(
      SetFieldCell_HDG_AuxiliaryVariable( fcnCell, fcnITrace, mmfld, connectivity,
                                          xfld, qfld, afld, qIfld,
                                          quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                          rsdAUX_bcell, cellgroupsAux, true, jacAUX_a_bcell),
      xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );
}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianBoundaryTrace_Dirichlet_mitState_HDG_AD_test_suite )

typedef boost::mpl::list< SurrealS<1>, SurrealS<2> > Surreals;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( HDG_1D_Dirichlet_mitState_Line, SurrealClass, Surreals )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef PDEClass::template TensorMatrixQ<Real> TensorMatrixQ;

  typedef BCTypeDirichlet_mitState<AdvectiveFlux1D_Uniform,ViscousFlux1D_Uniform> BCTypeDirichlet_mitState1D;

  typedef BCNDConvertSpace<PhysD1, BCAdvectionDiffusion<PhysD1,BCTypeDirichlet_mitState1D>> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef testspace::IntegrandCell_HDG_None<PDEClass> IntegrandCellClass; //dummy integrand for testing
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandITraceClass;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandBTraceClass;

  // PDE
  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_UniformGrad source(0.25, 0.6);

  PDEClass pde( adv, visc, source );

  // BC
  Real uB = 2.4;
  BCClass bc(pde, uB);

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Local );

  // integrand
  std::vector<int> cellgroups = {0};
  IntegrandCellClass fcnCell( cellgroups );
  IntegrandITraceClass fcnITrace( pde, disc, {0} );
  IntegrandBTraceClass fcnBTrace( pde, bc, {0,1}, disc );

  // grid: X1
  XField1D xfld(4);

  XField_CellToTrace<PhysD1, TopoD1> connectivity(xfld);

  // solution: P1
  for (int qorder = 0; qorder <= 3; qorder++ )
  {
    Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
    const int qDOF = qfld.nDOF();

    // solution data
    for (int i = 0; i < qDOF; i++)
      qfld.DOF(i) = sin(PI*i/((Real)qDOF));

    // auxiliary variable:
    Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Legendre);
    const int aDOF = afld.nDOF();

    // auxiliary data
    for (int i = 0; i < aDOF; i++)
      afld.DOF(i) = { -sin(PI*i/((Real)aDOF)) };

    // trace variable
    Field_DG_Trace<PhysD1, TopoD1, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Legendre);
    const int qIDOF = qIfld.nDOF();

    // auxiliary data
    for (int i = 0; i < qIDOF; i++)
      qIfld.DOF(i) = cos(PI*i/((Real)qIDOF));

    // Compute the field of inverse mass matrices
    FieldDataInvMassMatrix_Cell mmfld(afld);

    // quadrature rule
    QuadratureOrder quadOrder(xfld, 2*qorder);

    // jacobian via FD w/ residual operator; assumes scalar PDE
    // topology-specific single group interface

    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
    SLA::SparseVector<ArrayQ> rsdINTGlobal0(qIDOF), rsdINTGlobal1(qIDOF);
    DLA::MatrixD<Real> jacPDE_q(qDOF ,qDOF), jacPDE_qI(qDOF ,qIDOF);
    DLA::MatrixD<Real> jacINT_q(qIDOF,qDOF), jacINT_qI(qIDOF,qIDOF);

    rsdPDEGlobal0 = 0;
    rsdINTGlobal0 = 0;

    FieldDataMatrixD_BoundaryCell<TensorMatrixQ> jacAUX_a_bcell(qfld);

    computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                            xfld, qfld, afld, qIfld, quadOrder);

    //Compute the residuals at initial state
    IntegrateCellGroups<TopoD1>::integrate(
        ResidualCell_HDG( fcnCell, fcnITrace, connectivity,
                          xfld, qfld, afld, qIfld,
                          quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                         rsdPDEGlobal0, rsdINTGlobal0 ),
        xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_HDG(fcnBTrace, rsdPDEGlobal0, rsdINTGlobal0),
                                                                xfld, (qfld, afld), qIfld,
                                                                quadOrder.boundaryTraceOrders.data(),
                                                                quadOrder.boundaryTraceOrders.size() );

    // jacobian wrt q
    for (int j = 0; j < qDOF; j++)
    {
      qfld.DOF(j) += 1;

      // compute the perturbed BC residual
      rsdPDEGlobal1 = 0;
      rsdINTGlobal1 = 0;

      computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                              xfld, qfld, afld, qIfld, quadOrder);

      //Compute the residuals at perturbed state
      IntegrateCellGroups<TopoD1>::integrate(
          ResidualCell_HDG( fcnCell, fcnITrace, connectivity,
                            xfld, qfld, afld, qIfld,
                            quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                           rsdPDEGlobal1, rsdINTGlobal1 ),
          xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_HDG(fcnBTrace, rsdPDEGlobal1, rsdINTGlobal1),
                                                                  xfld, (qfld, afld), qIfld,
                                                                  quadOrder.boundaryTraceOrders.data(),
                                                                  quadOrder.boundaryTraceOrders.size() );

      qfld.DOF(j) -= 1;

      for (int i = 0; i < qDOF; i++)
        jacPDE_q(i,j) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];

      for (int i = 0; i < qIDOF; i++)
        jacINT_q(i,j) = rsdINTGlobal1[i] - rsdINTGlobal0[i];
    }

    // jacobian wrt qI
    for (int j = 0; j < qIDOF; j++)
    {
      qIfld.DOF(j) += 1;

      // compute the perturbed BC residual
      rsdPDEGlobal1 = 0;
      rsdINTGlobal1 = 0;

      computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                              xfld, qfld, afld, qIfld, quadOrder);

      //Compute the residuals at perturbed state
      IntegrateCellGroups<TopoD1>::integrate(
          ResidualCell_HDG( fcnCell, fcnITrace, connectivity,
                            xfld, qfld, afld, qIfld,
                            quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                           rsdPDEGlobal1, rsdINTGlobal1 ),
          xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_HDG(fcnBTrace, rsdPDEGlobal1, rsdINTGlobal1),
                                                                  xfld, (qfld, afld), qIfld,
                                                                  quadOrder.boundaryTraceOrders.data(),
                                                                  quadOrder.boundaryTraceOrders.size() );

      qIfld.DOF(j) -= 1;

      for (int i = 0; i < qDOF; i++)
        jacPDE_qI(i,j) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];

      for (int i = 0; i < qIDOF; i++)
        jacINT_qI(i,j) = rsdINTGlobal1[i] - rsdINTGlobal0[i];
    }

    // jacobian via Surreal
    DLA::MatrixD<MatrixQ> jacPDEGlob_q(qDOF,qDOF);
    DLA::MatrixD<MatrixQ> jacPDEGlob_qI(qDOF,qIDOF);
    DLA::MatrixD<MatrixQ> jacINTGlob_q(qIDOF,qDOF);
    DLA::MatrixD<MatrixQ> jacINTGlob_qI(qIDOF,qIDOF);
    jacPDEGlob_q = 0;
    jacPDEGlob_qI = 0;
    jacINTGlob_q = 0;
    jacINTGlob_qI = 0;

    computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                            xfld, qfld, afld, qIfld, quadOrder);

    FieldDataMatrixD_BoundaryCell<VectorMatrixQ> jacAUX_q_bcell(qfld);
    FieldDataMatrixD_BoundaryTrace<VectorMatrixQ> jacAUX_qI_btrace(qfld, qIfld);
    jacAUX_q_bcell = 0.0;
    jacAUX_qI_btrace = 0.0;

    std::vector<std::vector<std::vector<int>>> mapDOFGlobal_qI(xfld.nBoundaryTraceGroups());

    // Compute Jacobians via Surreals
    IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
        JacobianBoundaryTrace_HDG_AuxiliaryVariable<SurrealClass>( fcnBTrace, jacAUX_q_bcell, jacAUX_qI_btrace, mapDOFGlobal_qI ),
        xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

    IntegrateCellGroups<TopoD1>::integrate(
        JacobianCell_HDG<SurrealClass>( fcnCell, fcnITrace,
                                        mmfld, jacAUX_a_bcell, jacAUX_q_bcell, jacAUX_qI_btrace,
                                        mapDOFGlobal_qI, connectivity,
                                        xfld, qfld, afld, qIfld,
                                        quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                        cellgroups,
                                        jacPDEGlob_q, jacPDEGlob_qI,
                                        jacINTGlob_q, jacINTGlob_qI ),
         xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
        JacobianBoundaryTrace_HDG<SurrealClass>( fcnCell, fcnITrace, fcnBTrace,
                                                 jacAUX_a_bcell, jacAUX_q_bcell, jacAUX_qI_btrace,
                                                 mapDOFGlobal_qI, connectivity,
                                                 xfld, qfld, afld, qIfld,
                                                 quadOrder.cellOrders.data(), quadOrder.cellOrders.size(),
                                                 quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                                 jacPDEGlob_q, jacPDEGlob_qI,
                                                 jacINTGlob_q, jacINTGlob_qI),
        xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

    const Real small_tol = 1e-11;
    const Real close_tol = 1e-10;

    for (int i = 0; i < qDOF; i++)
      for (int j = 0; j < qDOF; j++)
      {
//        std::cout << "jacPDE_q(" << i << "," << j << ") = "
//                  << jacPDE_q(i,j) << ", " << jacPDEGlob_q(i,j) << ", " << jacPDE_q(i,j) - jacPDEGlob_q(i,j) << std::endl;

        SANS_CHECK_CLOSE( jacPDE_q(i,j), jacPDEGlob_q(i,j), small_tol, close_tol );
      }
//    std::cout << std::endl << std::endl;

    for (int i = 0; i < qDOF; i++)
      for (int j = 0; j < qIDOF; j++)
      {
//        std::cout << "jacPDE_qI(" << i << "," << j << ") = "
//                  << jacPDE_qI(i,j) << ", " << jacPDEGlob_qI(i,j) << ", " << jacPDE_qI(i,j) - jacPDEGlob_qI(i,j) << std::endl;

        SANS_CHECK_CLOSE( jacPDE_qI(i,j), jacPDEGlob_qI(i,j), small_tol, close_tol );
      }
//    std::cout << std::endl << std::endl;

    for (int i = 0; i < qIDOF; i++)
      for (int j = 0; j < qDOF; j++)
      {
//        std::cout << "jacINT_q(" << i << "," << j << ") = "
//                  << jacINT_q(i,j) << ", " << jacINTGlob_q(i,j) << ", " << jacINT_q(i,j) - jacINTGlob_q(i,j) << std::endl;

        SANS_CHECK_CLOSE( jacINT_q(i,j), jacINTGlob_q(i,j), small_tol, close_tol );
      }
//    std::cout << std::endl << std::endl;

    for (int i = 0; i < qIDOF; i++)
      for (int j = 0; j < qIDOF; j++)
      {
//        std::cout << "jacINT_qI(" << i << "," << j << ") = "
//                  << jacINT_qI(i,j) << ", " << jacINTGlob_qI(i,j) << ", " << jacINT_qI(i,j) - jacINTGlob_qI(i,j) << std::endl;

        SANS_CHECK_CLOSE( jacINT_qI(i,j), jacINTGlob_qI(i,j), small_tol, close_tol );
      }
  }
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( HDG_2D_Dirichlet_mitState_Triangle, SurrealClass, Surreals )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None> PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef PDEClass::template TensorMatrixQ<Real> TensorMatrixQ;

  typedef BCTypeDirichlet_mitState<AdvectiveFlux2D_Uniform,ViscousFlux2D_Uniform> BCTypeDirichlet_mitState2D;

  typedef BCNDConvertSpace<PhysD2, BCAdvectionDiffusion<PhysD2,BCTypeDirichlet_mitState2D>> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef testspace::IntegrandCell_HDG_None<PDEClass> IntegrandCellClass; //dummy integrand for testing
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandITraceClass;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandBTraceClass;

  // PDE
  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // BC
  Real uB = 12./5.;
  BCClass bc(pde, uB);

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Local );

  // integrand
  std::vector<int> cellgroups = {0};
  IntegrandCellClass fcnCell( cellgroups );
  IntegrandITraceClass fcnITrace( pde, disc, {0,1,2} );
  IntegrandBTraceClass fcnBTrace( pde, bc, {0,1,2,3}, disc );

  // grid: X1
  XField2D_Box_UnionJack_Triangle_X1 xfld(2, 2);

  XField_CellToTrace<PhysD2, TopoD2> connectivity(xfld);

  // solution: P1
  for (int qorder = 0; qorder <= 3; qorder++ )
  {
    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
    const int qDOF = qfld.nDOF();

    // solution data
    for (int i = 0; i < qDOF; i++)
      qfld.DOF(i) = sin(PI*i/((Real)qDOF));

    // auxiliary variable:
    Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Legendre);
    const int aDOF = afld.nDOF();

    // auxiliary data
    for (int i = 0; i < aDOF; i++)
      afld.DOF(i) = { -sin(PI*i/((Real)aDOF)), -cos(PI*i/((Real)aDOF)) };

    // trace variable
    Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Legendre);
    const int qIDOF = qIfld.nDOF();

    // auxiliary data
    for (int i = 0; i < qIDOF; i++)
      qIfld.DOF(i) = cos(PI*i/((Real)qIDOF));

    // Compute the field of inverse mass matrices
    FieldDataInvMassMatrix_Cell mmfld(afld);

    // quadrature rule
    QuadratureOrder quadOrder(xfld, 2*qorder);

    // jacobian via FD w/ residual operator; assumes scalar PDE
    // topology-specific single group interface

    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
    SLA::SparseVector<ArrayQ> rsdINTGlobal0(qIDOF), rsdINTGlobal1(qIDOF);
    DLA::MatrixD<Real> jacPDE_q(qDOF ,qDOF), jacPDE_qI(qDOF ,qIDOF);
    DLA::MatrixD<Real> jacINT_q(qIDOF,qDOF), jacINT_qI(qIDOF,qIDOF);

    rsdPDEGlobal0 = 0;
    rsdINTGlobal0 = 0;

    FieldDataMatrixD_BoundaryCell<TensorMatrixQ> jacAUX_a_bcell(qfld);

    computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                            xfld, qfld, afld, qIfld, quadOrder);

    //Compute the residuals at initial state
    IntegrateCellGroups<TopoD2>::integrate(
        ResidualCell_HDG( fcnCell, fcnITrace, connectivity,
                          xfld, qfld, afld, qIfld,
                          quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                         rsdPDEGlobal0, rsdINTGlobal0 ),
        xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_HDG(fcnBTrace, rsdPDEGlobal0, rsdINTGlobal0),
                                                                xfld, (qfld, afld), qIfld,
                                                                quadOrder.boundaryTraceOrders.data(),
                                                                quadOrder.boundaryTraceOrders.size() );

    // jacobian wrt q
    for (int j = 0; j < qDOF; j++)
    {
      qfld.DOF(j) += 1;

      // compute the perturbed BC residual
      rsdPDEGlobal1 = 0;
      rsdINTGlobal1 = 0;

      computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                              xfld, qfld, afld, qIfld, quadOrder);

      //Compute the residuals at perturbed state
      IntegrateCellGroups<TopoD2>::integrate(
          ResidualCell_HDG( fcnCell, fcnITrace, connectivity,
                            xfld, qfld, afld, qIfld,
                            quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                           rsdPDEGlobal1, rsdINTGlobal1 ),
          xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_HDG(fcnBTrace, rsdPDEGlobal1, rsdINTGlobal1),
                                                                  xfld, (qfld, afld), qIfld,
                                                                  quadOrder.boundaryTraceOrders.data(),
                                                                  quadOrder.boundaryTraceOrders.size() );

      qfld.DOF(j) -= 1;

      for (int i = 0; i < qDOF; i++)
        jacPDE_q(i,j) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];

      for (int i = 0; i < qIDOF; i++)
        jacINT_q(i,j) = rsdINTGlobal1[i] - rsdINTGlobal0[i];
    }

    // jacobian wrt qI
    for (int j = 0; j < qIDOF; j++)
    {
      qIfld.DOF(j) += 1;

      // compute the perturbed BC residual
      rsdPDEGlobal1 = 0;
      rsdINTGlobal1 = 0;

      computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                              xfld, qfld, afld, qIfld, quadOrder);

      //Compute the residuals at perturbed state
      IntegrateCellGroups<TopoD2>::integrate(
          ResidualCell_HDG( fcnCell, fcnITrace, connectivity,
                            xfld, qfld, afld, qIfld,
                            quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                           rsdPDEGlobal1, rsdINTGlobal1 ),
          xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_HDG(fcnBTrace, rsdPDEGlobal1, rsdINTGlobal1),
                                                                  xfld, (qfld, afld), qIfld,
                                                                  quadOrder.boundaryTraceOrders.data(),
                                                                  quadOrder.boundaryTraceOrders.size() );

      qIfld.DOF(j) -= 1;

      for (int i = 0; i < qDOF; i++)
        jacPDE_qI(i,j) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];

      for (int i = 0; i < qIDOF; i++)
        jacINT_qI(i,j) = rsdINTGlobal1[i] - rsdINTGlobal0[i];
    }

    // jacobian via Surreal
    DLA::MatrixD<MatrixQ> jacPDEGlob_q(qDOF,qDOF);
    DLA::MatrixD<MatrixQ> jacPDEGlob_qI(qDOF,qIDOF);
    DLA::MatrixD<MatrixQ> jacINTGlob_q(qIDOF,qDOF);
    DLA::MatrixD<MatrixQ> jacINTGlob_qI(qIDOF,qIDOF);
    jacPDEGlob_q = 0;
    jacPDEGlob_qI = 0;
    jacINTGlob_q = 0;
    jacINTGlob_qI = 0;

    computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                            xfld, qfld, afld, qIfld, quadOrder);

    // Compute Jacobians via Surreals
    FieldDataMatrixD_BoundaryCell<VectorMatrixQ> jacAUX_q_bcell(qfld);
    FieldDataMatrixD_BoundaryTrace<VectorMatrixQ> jacAUX_qI_btrace(qfld, qIfld);
    jacAUX_q_bcell = 0.0;
    jacAUX_qI_btrace = 0.0;

    std::vector<std::vector<std::vector<int>>> mapDOFGlobal_qI(xfld.nBoundaryTraceGroups());

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
        JacobianBoundaryTrace_HDG_AuxiliaryVariable<SurrealClass>( fcnBTrace, jacAUX_q_bcell, jacAUX_qI_btrace, mapDOFGlobal_qI ),
        xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

    IntegrateCellGroups<TopoD2>::integrate(
        JacobianCell_HDG<SurrealClass>( fcnCell, fcnITrace,
                                        mmfld, jacAUX_a_bcell, jacAUX_q_bcell, jacAUX_qI_btrace,
                                        mapDOFGlobal_qI, connectivity,
                                        xfld, qfld, afld, qIfld,
                                        quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                        cellgroups,
                                        jacPDEGlob_q, jacPDEGlob_qI,
                                        jacINTGlob_q, jacINTGlob_qI ),
         xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
        JacobianBoundaryTrace_HDG<SurrealClass>( fcnCell, fcnITrace, fcnBTrace,
                                                 jacAUX_a_bcell, jacAUX_q_bcell, jacAUX_qI_btrace,
                                                 mapDOFGlobal_qI, connectivity,
                                                 xfld, qfld, afld, qIfld,
                                                 quadOrder.cellOrders.data(), quadOrder.cellOrders.size(),
                                                 quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                                 jacPDEGlob_q, jacPDEGlob_qI,
                                                 jacINTGlob_q, jacINTGlob_qI),
        xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

    const Real small_tol = 1e-11;
    const Real close_tol = 3e-10;

    for (int i = 0; i <  qDOF; i++)
      for (int j = 0; j < qDOF; j++)
        SANS_CHECK_CLOSE( jacPDE_q(i,j), jacPDEGlob_q(i,j), small_tol, close_tol );

    for (int i = 0; i <  qDOF; i++)
      for (int j = 0; j < qIDOF; j++)
        SANS_CHECK_CLOSE( jacPDE_qI(i,j), jacPDEGlob_qI(i,j), small_tol, close_tol );


    for (int i = 0; i <  qIDOF; i++)
      for (int j = 0; j < qDOF; j++)
        SANS_CHECK_CLOSE( jacINT_q(i,j), jacINTGlob_q(i,j), small_tol, close_tol );

    for (int i = 0; i <  qIDOF; i++)
      for (int j = 0; j < qIDOF; j++)
        SANS_CHECK_CLOSE( jacINT_qI(i,j), jacINTGlob_qI(i,j), small_tol, close_tol );
  }
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
