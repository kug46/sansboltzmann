// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AlgebraicEquationSet_HDG_Triangle_AD_btest
// testing AlgebraicEquationSet_HDG

//#define DISPLAY_FOR_DEBUGGING

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Surreal/SurrealS.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"

#include "pde/BCParameters.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/Element/ElementSequence.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( AlgebraicEquationSet_HDG_Triangle_AD_test_suite )


template<class MatrixQ>
void checkDenseSparseEquality(DLA::MatrixD<MatrixQ>& djac, const SLA::SparseMatrix_CRS<MatrixQ>& sjac )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  if (sjac.getNumNonZero() > 0)
  {
    const int *col_ind = sjac.get_col_ind();
    const int *row_ptr = sjac.get_row_ptr();

    for (int i = 0; i < sjac.m(); i++)
      for (int j = row_ptr[i]; j < row_ptr[i+1]; j++)
      {
        SANS_CHECK_CLOSE( djac(i,col_ind[j]), sjac[j], small_tol, close_tol );

        // Zero out the non-zero entry so the next loop can look for any non-zero values missed
        djac(i,col_ind[j]) = 0;
      }
  }

  // Check that all non-zero values have been cleared
  for (int i = 0; i < djac.m(); i++)
    for (int j = 0; j < djac.n(); j++)
      BOOST_CHECK_EQUAL(djac(i,j), 0);
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseSystem )
{
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_SineSine> SolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;
//  typedef BCTypeFunctionLinearRobin_sansLG BCType;
  typedef BCTypeFunction_mitStateParam BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  SolutionExact solnExact;

  Source2D_UniformGrad source(0.3, 0.2, 0.5);

  typedef ForcingFunction2D_MMS<PDEAdvectionDiffusion2D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict SineSine;
  SineSine[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.SineSine;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = SineSine;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
         BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Upwind] = true;

  PyDict PyBCList;
  PyBCList["BCNameSolution"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNameSolution"] = {0, 1, 2, 3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);


  // grid: HierarchicalP1 (aka X1)

  int ii = 1;
  int jj = 1;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  std::vector<int> cellGroups = {0};
  std::vector<int> interorTraceGroups = {0};

  // solution: P1 (aka Q1)

  int order = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0;

  // auxiliary operator
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
  afld = 0;

  // interface
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);
  qIfld = 0;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre,
                                                        BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
  lgfld = 0;

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Global, Gradient );

  QuadratureOrder quadratureOrder( xfld, -1 );
  std::vector<Real> tol = {1e-12, 1e-12, 1e-12};

  PrimalEquationSetClass PrimalEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                                     cellGroups, interorTraceGroups, PyBCList, BCBoundaryGroups);

  BOOST_CHECK_EQUAL(0, PrimalEqSet.indexPDE());
  BOOST_CHECK_EQUAL(0, PrimalEqSet.indexQ());

  SystemVectorClass q(PrimalEqSet.vectorStateSize());
  SystemVectorClass rsd(PrimalEqSet.vectorEqSize()), rsdnew(rsd.size());

  PrimalEqSet.fillSystemVector(q);

  rsd = 0;
  rsdnew = 0;

  PrimalEqSet.residual(q, rsd);
  std::vector<std::vector<Real>> rsdNrm( PrimalEqSet.residualNorm(rsd) );
//  rsd = 0;
//  PrimalEqSet.residual(q, rsd);

  // jacobian nonzero pattern
  SystemNonZeroPattern nz(PrimalEqSet.matrixSize());
  PrimalEqSet.jacobian(q, nz);

  // jacobian
  SystemMatrixClass jac(nz);
  jac = 0;
  PrimalEqSet.jacobian(q, jac);

  BOOST_REQUIRE_EQUAL(jac.m(), 3);
  BOOST_REQUIRE_EQUAL(jac.n(), 3);

#if __clang_analyzer__
  return; // clang things we are accessing zero size memory below... sigh...
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-10;

  for ( int ii = 0; ii < jac.m(); ii++)
  {
#ifdef DISPLAY_FOR_DEBUGGING
      std::cout << "=================" << std::endl;
      std::cout << "block row = " << ii << std::endl;
#endif
    for ( int i = 0; i < jac(ii,0).m(); i++ )
    {
      std::vector<Real> AJac;
      std::vector<int> iAJac;

      for ( int jj = 0; jj < jac.n(); jj++)
      {
        if (jac(ii,jj).getNumNonZero() == 0) continue;
        for ( int k = 0; k < jac(ii,jj).rowNonZero(i); k++)
          if ( abs(jac(ii,jj).sparseRow(i,k)) > small_tol)
          {
            // Save the non-zero value and column index
            AJac.push_back(jac(ii,jj).sparseRow(i,k));
            iAJac.push_back(jac(ii,jj).get_col_ind()[jac(ii,jj).get_row_ptr()[i] + k]);
          }
      }

#ifdef DISPLAY_FOR_DEBUGGING
      std::cout << "-----------------" << std::endl;
      std::cout << "row = " << i << std::endl;
      std::cout << "jacA" << std::endl;
      for ( std::size_t k = 0; k < AJac.size(); k++)
        std::cout << AJac[k] << ", ";
      std::cout << std::endl;

      for ( std::size_t k = 0; k < iAJac.size(); k++)
        std::cout << iAJac[k] << ", ";
      std::cout << std::endl;
#endif

      std::vector<Real> diffJac;
      std::vector<int> idiffJac;
      for ( int kk = 0; kk < q.m(); kk++)
      {
        if (jac(ii,kk).getNumNonZero() == 0) continue;
        for ( int k = 0; k < q[kk].m(); k++)
        {
          // Use finite difference to compute an exact Jacobian, which works for a Linear PDE
          rsdnew = 0;
          q[kk][k] += 1;

          PrimalEqSet.residual(q, rsdnew);

          q[kk][k] -= 1;

          Real diff = rsdnew[ii][i] - rsd[ii][i];

          // Save of only non-zero jacobian entries
          if ( abs(diff) > close_tol )
          {
            diffJac.push_back(diff);
            idiffJac.push_back(k);
          }
        }
      }
#ifdef DISPLAY_FOR_DEBUGGING
      std::cout << "jacFD" << std::endl;
      for ( std::size_t k = 0; k < diffJac.size(); k++)
        std::cout << diffJac[k] << ", ";
      std::cout << std::endl;
      for ( std::size_t k = 0; k < diffJac.size(); k++)
        std::cout << idiffJac[k] << ", ";
      std::cout << std::endl;
      std::cout << "jacA-jacFD" << std::endl;
#endif
      bool error = false;
      BOOST_REQUIRE_EQUAL(AJac.size(), diffJac.size());
      for ( std::size_t k = 0; k < diffJac.size(); k++)
      {
        if ( abs(AJac[k]-diffJac[k]) > close_tol)
        {
          error = true;
          std::cout << "jacA[" << k << "]=" << AJac[k]
                    << ", jacFD[" << k << "]=" << diffJac[k] << std::endl;
        }
      }
      BOOST_REQUIRE(!error);
    }
  }

  // Test residual assessment functions
  std::vector<std::vector<Real>> rsdNorm = PrimalEqSet.residualNorm(rsd);
  BOOST_CHECK(PrimalEqSet.convergedResidual(rsdNorm) == false);
  BOOST_CHECK(PrimalEqSet.isValidStateSystemVector(q) == true);

  std::vector<std::vector<Real>> dummyrsdNorm = PrimalEqSet.residualNorm(rsd);
  for (unsigned int i = 0; i < dummyrsdNorm.size(); i++)
    for (unsigned int j = 0; j < dummyrsdNorm[i].size(); j++)
        dummyrsdNorm[i][j] = 0;
  BOOST_CHECK(PrimalEqSet.decreasedResidual(rsdNorm, dummyrsdNorm) == true);
  for (unsigned int i = 0; i < dummyrsdNorm.size(); i++)
    for (unsigned int j = 0; j < dummyrsdNorm[i].size(); j++)
        dummyrsdNorm[i][j] = 100;
  BOOST_CHECK(PrimalEqSet.decreasedResidual(rsdNorm, dummyrsdNorm) == false);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DenseSystem )
{
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_SineSine> SolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;
  typedef BCTypeFunction_mitStateParam BCType;
//  typedef BCTypeFunctionLinearRobin_sansLG BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Dense, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  SolutionExact solnExact;

  Source2D_UniformGrad source(0.3, 0.2, 0.5);

  typedef ForcingFunction2D_MMS<PDEAdvectionDiffusion2D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict SineSine;
  SineSine[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.SineSine;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = SineSine;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
         BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Upwind] = true;

  PyDict PyBCList;
  PyBCList["BCNameSolution"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNameSolution"] = {0, 1, 2, 3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);


  // grid: HierarchicalP1 (aka X1)

  int ii = 1;
  int jj = 1;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  std::vector<int> cellGroups = {0};
  std::vector<int> interorTraceGroups = {0};

  // solution: P1 (aka Q1)

  int order = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0;

  // auxiliary operator
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
  afld = 0;

  // interface
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);
  qIfld = 0;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre,
                                                        BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
  lgfld = 0;

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Global, Gradient );

  QuadratureOrder quadratureOrder( xfld, -1 );
  std::vector<Real> tol = {1e-12, 1e-12, 1e-12};

  PrimalEquationSetClass PrimalEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                                     cellGroups, interorTraceGroups, PyBCList, BCBoundaryGroups);

  SystemVectorClass q(PrimalEqSet.vectorStateSize());
  SystemVectorClass rsd(PrimalEqSet.vectorEqSize()), rsdnew(rsd.size());

  PrimalEqSet.fillSystemVector(q);

  rsd = 0;
  rsdnew = 0;

  PrimalEqSet.residual(q, rsd);

  // jacobian nonzero pattern

  SystemNonZeroPattern nz(PrimalEqSet.matrixSize());

  BOOST_REQUIRE_EQUAL(nz.m(), 3);
  BOOST_REQUIRE_EQUAL(nz.n(), 3);

  PrimalEqSet.jacobian(q, nz);

  // jacobian

  SystemMatrixClass jac(nz);
  jac = 0;

  BOOST_REQUIRE_EQUAL(jac.m(), 3);
  BOOST_REQUIRE_EQUAL(jac.n(), 3);

#if __clang_analyzer__
  return; // clang thinks we are accessing zero size memory below... sigh...
#endif

  afld = 0;
  PrimalEqSet.fillSystemVector(q);

  PrimalEqSet.jacobian(q, jac);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-10;

  for ( int ii = 0; ii < jac.m(); ii++)
  {
#ifdef DISPLAY_FOR_DEBUGGING
      std::cout << "=================" << std::endl;
      std::cout << "block row = " << ii << std::endl;
#endif
    for ( int i = 0; i < jac(ii,0).m(); i++ )
    {
      std::vector<Real> AJac;
      std::vector<int> iAJac;

      for ( int jj = 0; jj < jac.n(); jj++)
      {
        for ( int k = 0; k < jac(ii,jj).n(); k++)
          if ( abs(jac(ii,jj)(i,k)) > small_tol)
          {
            // Save the non-zero value and column index
            AJac.push_back(jac(ii,jj)(i,k));
            iAJac.push_back(k);
          }
      }

#ifdef DISPLAY_FOR_DEBUGGING
      std::cout << "-----------------" << std::endl;
      std::cout << "row = " << i << std::endl;
      std::cout << "jacA" << std::endl;
      for ( std::size_t k = 0; k < AJac.size(); k++)
        std::cout << AJac[k] << ", ";
      std::cout << std::endl;
      for ( std::size_t k = 0; k < iAJac.size(); k++)
        std::cout << iAJac[k] << ", ";
      std::cout << std::endl;
#endif

      std::vector<Real> diffJac;
      std::vector<int> idiffJac;
      for ( int kk = 0; kk < q.m(); kk++)
      {
        for ( int k = 0; k < q[kk].m(); k++)
        {
          // Use finite difference to compute an exact Jacobian, which works for a Linear PDE
          rsdnew = 0;
          q[kk][k] += 1;

          PrimalEqSet.residual(q, rsdnew);

          q[kk][k] -= 1;

          Real diff = rsdnew[ii][i] - rsd[ii][i];

          // Save of only non-zero jacobian entries
          if ( abs(diff) > close_tol )
          {
            diffJac.push_back(diff);
            idiffJac.push_back(k);
          }
        }
      }
#ifdef DISPLAY_FOR_DEBUGGING
      std::cout << "jacFD" << std::endl;
      for ( std::size_t k = 0; k < diffJac.size(); k++)
        std::cout << diffJac[k] << ", ";
      std::cout << std::endl;
      for ( std::size_t k = 0; k < diffJac.size(); k++)
        std::cout << idiffJac[k] << ", ";
      std::cout << std::endl;
      std::cout << "jacA - jacFD" << std::endl;
#endif
      bool error = false;
      BOOST_REQUIRE_EQUAL(AJac.size(), diffJac.size());
      for ( std::size_t k = 0; k < diffJac.size(); k++)
      {
        if ( abs(AJac[k]-diffJac[k]) > close_tol )
        {
          error = true;
          std::cout << "jacA[" << k << "]=" << AJac[k]
                    << ", jacFD[" << k << "]=" << diffJac[k] << std::endl;
        }
      }
      BOOST_REQUIRE(!error);
    }
  }

  // Test residual assessment functions
  std::vector<std::vector<Real>> rsdNorm = PrimalEqSet.residualNorm(rsd);
  BOOST_CHECK(PrimalEqSet.convergedResidual(rsdNorm) == false);
  BOOST_CHECK(PrimalEqSet.isValidStateSystemVector(q) == true);

  std::vector<std::vector<Real>> dummyrsdNorm = PrimalEqSet.residualNorm(rsd);
  for (unsigned int i = 0; i < dummyrsdNorm.size(); i++)
    for (unsigned int j = 0; j < dummyrsdNorm[i].size(); j++)
        dummyrsdNorm[i][j] = 0;
  BOOST_CHECK(PrimalEqSet.decreasedResidual(rsdNorm, dummyrsdNorm) == true);
  for (unsigned int i = 0; i < dummyrsdNorm.size(); i++)
    for (unsigned int j = 0; j < dummyrsdNorm[i].size(); j++)
        dummyrsdNorm[i][j] = 100;
  BOOST_CHECK(PrimalEqSet.decreasedResidual(rsdNorm, dummyrsdNorm) == false);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Sparse_Dense_Equality )
{
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_SineSine> SolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;
  typedef BCTypeFunctionLinearRobin_sansLG BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> PrimalEquationSetSparse;
  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Dense, XField<PhysD2, TopoD2>> PrimalEquationSetDense;
  typedef PrimalEquationSetSparse::BCParams BCParams;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef PrimalEquationSetSparse::SystemMatrix SparseSystemMatrixClass;
  typedef PrimalEquationSetSparse::SystemVector SparseSystemVectorClass;
  typedef PrimalEquationSetSparse::SystemNonZeroPattern SparseSystemNonZeroPattern;

  typedef PrimalEquationSetDense::SystemMatrix DenseSystemMatrixClass;
  typedef PrimalEquationSetDense::SystemVector DenseSystemVectorClass;
  typedef PrimalEquationSetDense::SystemNonZeroPattern DenseSystemNonZeroPattern;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  SolutionExact solnExact;

  Source2D_UniformGrad source(0.3, 0.2, 0.5);

  typedef ForcingFunction2D_MMS<PDEAdvectionDiffusion2D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict SineSine;
  SineSine[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.SineSine;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = SineSine;

  PyDict PyBCList;
  PyBCList["BCNameSolution"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNameSolution"] = {0, 1, 2, 3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);


  // grid: HierarchicalP1 (aka X1)

  int ii = 2;
  int jj = 2;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  std::vector<int> cellGroups = {0};
  std::vector<int> interorTraceGroups = {0,1,2};

  // solution: P1 (aka Q1)

  int order = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0;

  // auxiliary operator
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
  afld = 0;

  // interface
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);
  qIfld = 0;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre,
                                                        BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
  lgfld = 0;

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Global, Gradient );

  QuadratureOrder quadratureOrder( xfld, -1 );
  std::vector<Real> tol = {1e-12, 1e-12, 1e-12};

  PrimalEquationSetSparse PrimalEqSetSparse(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                                            cellGroups, interorTraceGroups, PyBCList, BCBoundaryGroups);
  PrimalEquationSetDense PrimalEqSetDense(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                                          cellGroups, interorTraceGroups, PyBCList, BCBoundaryGroups);

  SparseSystemVectorClass q_s(PrimalEqSetSparse.vectorStateSize());
  DenseSystemVectorClass q_d(PrimalEqSetDense.vectorStateSize());

  SparseSystemVectorClass rsd_s(PrimalEqSetSparse.vectorEqSize());
  DenseSystemVectorClass rsd_d(PrimalEqSetDense.vectorEqSize());

  PrimalEqSetSparse.fillSystemVector(q_s);
  PrimalEqSetDense.fillSystemVector(q_d);

  rsd_s = 0;
  rsd_d = 0;

  PrimalEqSetSparse.residual(q_s, rsd_s);
  PrimalEqSetDense.residual(q_d, rsd_d);

  BOOST_REQUIRE_EQUAL( q_s.m(), q_d.m() );
  BOOST_REQUIRE_EQUAL( rsd_s.m(), rsd_d.m() );


  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  for ( int ii = 0; ii < q_s.m(); ii++ )
    for ( int i = 0; i < q_s[ii].m(); i++ )
      SANS_CHECK_CLOSE( q_s[ii][i], q_d[ii][i], small_tol, close_tol );

  // jacobian nonzero pattern

  SparseSystemNonZeroPattern nz_s(PrimalEqSetSparse.matrixSize());
  DenseSystemNonZeroPattern nz_d(PrimalEqSetDense.matrixSize());

  PrimalEqSetSparse.jacobian(q_s, nz_s);
  PrimalEqSetDense.jacobian(q_d, nz_d);

  // jacobian

  SparseSystemMatrixClass jac_s(nz_s);
  DenseSystemMatrixClass jac_d(nz_d);
  jac_s = 0;
  jac_d = 0;

  PrimalEqSetSparse.jacobian(q_s, jac_s);
  PrimalEqSetDense.jacobian(q_d, jac_d);


  BOOST_REQUIRE_EQUAL(jac_s.m(), 3);
  BOOST_REQUIRE_EQUAL(jac_s.n(), 3);

  BOOST_REQUIRE_EQUAL(jac_d.m(), 3);
  BOOST_REQUIRE_EQUAL(jac_d.n(), 3);

#if __clang_analyzer__
  return; // clang thinks we are accessing zero size memory below... sigh...
#endif

  BOOST_TEST_CHECKPOINT( "PDE_q" );
  checkDenseSparseEquality(jac_d(0,0), jac_s(0,0));

  BOOST_TEST_CHECKPOINT( "PDE_qI" );
  checkDenseSparseEquality(jac_d(0,1), jac_s(0,1));

  BOOST_TEST_CHECKPOINT( "PDE_lg" );
  checkDenseSparseEquality(jac_d(0,2), jac_s(0,2));


  BOOST_TEST_CHECKPOINT( "INT_q" );
  checkDenseSparseEquality(jac_d(1,0), jac_s(1,0));

  BOOST_TEST_CHECKPOINT( "INT_qI" );
  checkDenseSparseEquality(jac_d(1,1), jac_s(1,1));

  BOOST_TEST_CHECKPOINT( "INT_lg" );
  checkDenseSparseEquality(jac_d(1,2), jac_s(1,2));


  BOOST_TEST_CHECKPOINT( "BC_q" );
  checkDenseSparseEquality(jac_d(2,0), jac_s(2,0));

  BOOST_TEST_CHECKPOINT( "BC_qI" );
  checkDenseSparseEquality(jac_d(2,1), jac_s(2,1));

  BOOST_TEST_CHECKPOINT( "BC_lg" );
  checkDenseSparseEquality(jac_d(2,2), jac_s(2,2));
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Residual_test )
{
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_SineSine> SolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;
  typedef BCTypeFunctionLinearRobin_sansLG BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  SolutionExact solnExact;

  Source2D_UniformGrad source(0.3, 0.2, 0.5);

  typedef ForcingFunction2D_MMS<PDEAdvectionDiffusion2D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict SineSine;
  SineSine[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.SineSine;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = SineSine;

  PyDict PyBCList;
  PyBCList["BCNameSolution"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNameSolution"] = {0, 1, 2, 3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);


  // grid: HierarchicalP1 (aka X1)

  int ii = 1;
  int jj = 1;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  std::vector<int> cellGroups = {0};
  std::vector<int> interorTraceGroups = {0};

  // solution: P1 (aka Q1)

  int order = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0;

  // auxiliary operator
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
  afld = 0;

  // interface
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);
  qIfld = 0;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre,
                                                        BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
  lgfld = 0;

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Global, Gradient );

  QuadratureOrder quadratureOrder( xfld, -1 );
  std::vector<Real> tol = {1e-12, 2e-12, 3e-12};

  PrimalEquationSetClass PrimalEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                                     cellGroups, interorTraceGroups, PyBCList, BCBoundaryGroups);

  SystemVectorClass rsd(PrimalEqSet.vectorEqSize());

  rsd[PrimalEqSet.iPDE] = 1;
  rsd[PrimalEqSet.iINT] = 2;
  rsd[PrimalEqSet.iBC] = 3;

  std::vector<std::vector<Real>> rsdNrm( PrimalEqSet.residualNorm(rsd) );

  const Real close_tol = 1e-12;

  // Check the currently assumed L2-norm
  BOOST_CHECK_CLOSE( sqrt(qfld.nDOF()*1*1) , rsdNrm[PrimalEqSet.iPDE][0], close_tol );
  BOOST_CHECK_CLOSE( sqrt(qIfld.nDOF()*2*2), rsdNrm[PrimalEqSet.iINT][0], close_tol );
  BOOST_CHECK_CLOSE( sqrt(lgfld.nDOF()*3*3), rsdNrm[PrimalEqSet.iBC][0] , close_tol );


  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Discretization/AlgebraicEquationSet_HDG_Triangle_AD_pattern.txt", true );

  PrimalEqSet.printDecreaseResidualFailure(rsdNrm, output);
  BOOST_CHECK( output.match_pattern() );
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
