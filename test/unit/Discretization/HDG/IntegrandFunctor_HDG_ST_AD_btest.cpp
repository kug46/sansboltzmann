// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandFunctor_HDG_ST_AD_btest
// testing of residual integrands for HDG: Advection-Diffusion on a spacetime grid

#include <boost/test/unit_test.hpp>

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"

#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"

#include "Discretization/HDG/IntegrandCell_HDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_None> PDEAdvectionDiffusion1D;
typedef PDENDConvertSpaceTime<PhysD1, PDEAdvectionDiffusion1D> PDEClass1D;
typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldTri;
template class IntegrandCell_HDG<PDEClass1D>::BasisWeighted_PDE<Real,Real,TopoD2,Triangle,ElementXFieldTri>;
template class IntegrandInteriorTrace_HDG<PDEClass1D>::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldTri>;

typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_None > PDEAdvectionDiffusion2D;
typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass2D;
template class IntegrandCell_HDG<PDEClass2D>::BasisWeighted_PDE<Real,Real,TopoD2,Triangle,ElementXFieldTri>;
template class IntegrandInteriorTrace_HDG<PDEClass2D>::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldTri>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandFunctor_HDG_ST_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IntegrandCell_HDG_ST_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpaceTime<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;

  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<DLA::VectorS<2,ArrayQ>,TopoD2,Triangle> ElementAFieldCell;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandCell_HDG<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,Real,TopoD2,Triangle,ElementParam> BasisWeightedPDEClass;
  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // line grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  // solution

  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  // auxiliary variable

  ElementAFieldCell afldElem(order, BasisFunctionCategory_Hierarchical);

  afldElem.DOF(0) = { 2, -3};
  afldElem.DOF(1) = { 7,  8};
  afldElem.DOF(2) = {-1, -5};

  // HDG discretization (not used)
  DiscretizationClass disc( pde );

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  BasisWeightedPDEClass fcn = fcnint.integrand_PDE(xfldElem, qfldElem, afldElem );

  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  Real sRef, tRef;
  Real integrandPDETrue[3];
//  Real integrandAuxTrue[3], integrandAuyTrue[3];
  BasisWeightedPDEClass::IntegrandType integrand[3];

  sRef = 0, tRef = 0;
  fcn( {sRef, tRef}, integrand, 3 );

  // PDE residuals: (advective) + (viscous)
  integrandPDETrue[0] = ( 2.1) + ( -2123./500.);
  integrandPDETrue[1] = (-1.1)   + (2123./500.);
  integrandPDETrue[2] = (-1.) + 0;

  // auxiliary variable definition
//  integrandAuxTrue[0] = 0;
//  integrandAuxTrue[1] = 0;
//  integrandAuxTrue[2] = 0;
//  integrandAuyTrue[0] = 0;
//  integrandAuyTrue[1] = 0;
//  integrandAuyTrue[2] = 0;

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

//  SANS_CHECK_CLOSE( integrandAuxTrue[0], integrand[0].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuxTrue[1], integrand[1].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuxTrue[2], integrand[2].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyTrue[0], integrand[0].Au[1], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyTrue[1], integrand[1].Au[1], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyTrue[2], integrand[2].Au[1], small_tol, close_tol );

  sRef = 1./3., tRef = 2./3.;
  fcn( {sRef, tRef}, integrand, 3 );

  // PDE residuals: (advective) + (viscous)
  integrandPDETrue[0] = ( 77./10.) + ( -2123./600.);
  integrandPDETrue[1] = (-121./30.)   + (2123./600.);
  integrandPDETrue[2] = (-11./3.) + 0;

  // auxiliary variable definition
//  integrandAuxTrue[0] = 0;
//  integrandAuxTrue[1] = -2123./9000.;
//  integrandAuxTrue[2] = -2123./4500.;
//  integrandAuyTrue[0] = 0;
//  integrandAuyTrue[1] = 0;
//  integrandAuyTrue[2] = 0;

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

//  SANS_CHECK_CLOSE( integrandAuxTrue[0], integrand[0].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuxTrue[1], integrand[1].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuxTrue[2], integrand[2].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyTrue[0], integrand[0].Au[1], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyTrue[1], integrand[1].Au[1], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyTrue[2], integrand[2].Au[1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IntegrandTrace_HDG_ST_1D_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpaceTime<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;

  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldTrace;
  typedef Element<DLA::VectorS<2,ArrayQ>,TopoD2,Triangle> ElementAFieldCell;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementParam> BasisWeightedClass;
  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );


  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemR.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;
  x4 = 1;  y4 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xfldElemR.DOF(0) = {x4, y4};
  xfldElemR.DOF(1) = {x3, y3};
  xfldElemR.DOF(2) = {x2, y2};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution

  order = 1;
  ElementQFieldCell qfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell qfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemR.nDOF() );

  // triangle solution (left)
  qfldElemL.DOF(0) = 1;
  qfldElemL.DOF(1) = 3;
  qfldElemL.DOF(2) = 4;

  // triangle solution (right)
  qfldElemR.DOF(0) = 7;
  qfldElemR.DOF(1) = 2;
  qfldElemR.DOF(2) = 9;

  // auxiliary variable

  ElementAFieldCell afldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementAFieldCell afldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, afldElemL.order() );
  BOOST_CHECK_EQUAL( 3, afldElemL.nDOF() );

  afldElemL.DOF(0) = { 2, -3};
  afldElemL.DOF(1) = { 7,  8};
  afldElemL.DOF(2) = {-1, -5};

  afldElemR.DOF(0) = { 9,  6};
  afldElemR.DOF(1) = {-1,  3};
  afldElemR.DOF(2) = { 2, -3};

  // interface solution

  ElementQFieldTrace uedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, uedge.order() );
  BOOST_CHECK_EQUAL( 2, uedge.nDOF() );

  uedge.DOF(0) =  8;
  uedge.DOF(1) = -1;

  // HDG discretization
  DiscretizationClass disc( pde );

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  BasisWeightedClass fcnPDEL = fcnint.integrand( xedge, uedge, CanonicalTraceToCell(0,1), +1,
                                                 xfldElemL, qfldElemL, afldElemL );

  BasisWeightedClass fcnPDER = fcnint.integrand( xedge, uedge, CanonicalTraceToCell(0,-1), -1,
                                                 xfldElemR, qfldElemR, afldElemR );

  BOOST_CHECK_EQUAL( 1, fcnPDEL.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcnPDEL.nDOFLeft() );
  BOOST_CHECK( fcnPDEL.needsEvaluation() == true );

  BOOST_CHECK_EQUAL( 1, fcnPDER.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcnPDER.nDOFLeft() );
  BOOST_CHECK( fcnPDER.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  Real sRef;
  Real integrandCellLTrue[3], integrandCellRTrue[3], integrandPDEITrue[2];
  BasisWeightedClass::IntegrandCellType integrandCellL[3], integrandCellR[3];
  ArrayQ integrandPDEIL[2], integrandPDEIR[2], integrandPDEI[2];

  sRef = 0;
  fcnPDEL( {sRef}, integrandCellL, 3, integrandPDEIL, 2 );
  fcnPDER( {sRef}, integrandCellR, 3, integrandPDEIR, 2 );

  integrandPDEI[0] = integrandPDEIL[0] + integrandPDEIR[0];
  integrandPDEI[1] = integrandPDEIL[1] + integrandPDEIR[1];

  // PDE residuals (left): (advective) + (viscous) + (stablization)
  integrandCellLTrue[0] = (0)              + (0)                    + (0);
  integrandCellLTrue[1] = ( 63./(10.*sqrt(2)) )  + ( -14861./(1000.*sqrt(2)) ) + (0);
  integrandCellLTrue[2] = (0)              + (0)                    + (0);

  // PDE residuals (right)
  integrandCellRTrue[0] = (0)              + (0)               + (0);
  integrandCellRTrue[1] = (0)              + (0)               + (0);
  integrandCellRTrue[2] = (-42.*sqrt(2.)/5.) + (2123./(500*sqrt(2))) + (0);

  // interface residuals
  integrandPDEITrue[0] = (21./(2.*sqrt(2.))) + (2123./(200*sqrt(2))) + (0);
  integrandPDEITrue[1] = (0) + (0)                   + (0);

  // auxiliary  variable definition
//  integrandAuxLTrue[0] = 0;
//  integrandAuxLTrue[1] = -2123./(200*sqrt(2));
//  integrandAuxLTrue[2] = 0;
//  integrandAuyLTrue[0] = 0;
//  integrandAuyLTrue[1] = 0;
//  integrandAuyLTrue[2] = 0;
//
//  integrandAuxRTrue[0] = 0;
//  integrandAuxRTrue[1] = 0;
//  integrandAuxRTrue[2] = -2123./(1000*sqrt(2));
//  integrandAuyRTrue[0] = 0;
//  integrandAuyRTrue[1] = 0;
//  integrandAuyRTrue[2] = 0;

  SANS_CHECK_CLOSE( integrandCellLTrue[0], integrandCellL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandCellLTrue[1], integrandCellL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandCellLTrue[2], integrandCellL[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandCellRTrue[0], integrandCellR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandCellRTrue[1], integrandCellR[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandCellRTrue[2], integrandCellR[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDEITrue[0], integrandPDEI[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDEITrue[1], integrandPDEI[1], small_tol, close_tol );

//  SANS_CHECK_CLOSE( integrandAuxLTrue[0], integrandCellL[0].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuxLTrue[1], integrandCellL[1].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuxLTrue[2], integrandCellL[2].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyLTrue[0], integrandCellL[0].Au[1], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyLTrue[1], integrandCellL[1].Au[1], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyLTrue[2], integrandCellL[2].Au[1], small_tol, close_tol );
//
//  SANS_CHECK_CLOSE( integrandAuxRTrue[0], integrandCellR[0].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuxRTrue[1], integrandCellR[1].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuxRTrue[2], integrandCellR[2].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyRTrue[0], integrandCellR[0].Au[1], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyRTrue[1], integrandCellR[1].Au[1], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyRTrue[2], integrandCellR[2].Au[1], small_tol, close_tol );


  sRef = 1;
  fcnPDEL( {sRef}, integrandCellL, 3, integrandPDEIL, 2 );
  fcnPDER( {sRef}, integrandCellR, 3, integrandPDEIR, 2 );

  integrandPDEI[0] = integrandPDEIL[0] + integrandPDEIR[0];
  integrandPDEI[1] = integrandPDEIL[1] + integrandPDEIR[1];

  // PDE residuals (left): (advective) + (viscous) + (stablization)
  integrandCellLTrue[0] = (0)             + (0)                   + (0);
  integrandCellLTrue[1] = (0)             + (0)                   + (0);
  integrandCellLTrue[2] = (21*sqrt(2)/5.) + (2123./(1000*sqrt(2))) + (0);

  // PDE residuals (right)
  integrandCellRTrue[0] = (0)             + (0)                  + (0);
  integrandCellRTrue[1] = ( 21./(10*sqrt(2))) - (2123./(1000*sqrt(2))) + (0);
  integrandCellRTrue[2] = (0)             + (0)                  + (0);

  // interface residuals
  integrandPDEITrue[0] = (0) + (0)               + (0);
  integrandPDEITrue[1] = (-21./(2*sqrt(2))) + (0) + (0);

  // auxiliary variable definition
//  integrandAuxLTrue[0] = 0;
//  integrandAuxLTrue[1] = 0;
//  integrandAuxLTrue[2] = 2123./(200*sqrt(2));
//  integrandAuyLTrue[0] = 0;
//  integrandAuyLTrue[1] = 0;
//  integrandAuyLTrue[2] = 0;
//
//  integrandAuxRTrue[0] = 0;
//  integrandAuxRTrue[1] = -6369./(1000*sqrt(2));
//  integrandAuxRTrue[2] = 0;
//  integrandAuyRTrue[0] = 0;
//  integrandAuyRTrue[1] = 0;
//  integrandAuyRTrue[2] = 0;

  SANS_CHECK_CLOSE( integrandCellLTrue[0], integrandCellL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandCellLTrue[1], integrandCellL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandCellLTrue[2], integrandCellL[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandCellRTrue[0], integrandCellR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandCellRTrue[1], integrandCellR[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandCellRTrue[2], integrandCellR[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDEITrue[0], integrandPDEI[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDEITrue[1], integrandPDEI[1], small_tol, close_tol );

//  SANS_CHECK_CLOSE( integrandAuxLTrue[0], integrandCellL[0].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuxLTrue[1], integrandCellL[1].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuxLTrue[2], integrandCellL[2].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyLTrue[0], integrandCellL[0].Au[1], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyLTrue[1], integrandCellL[1].Au[1], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyLTrue[2], integrandCellL[2].Au[1], small_tol, close_tol );
//
//  SANS_CHECK_CLOSE( integrandAuxRTrue[0], integrandCellR[0].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuxRTrue[1], integrandCellR[1].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuxRTrue[2], integrandCellR[2].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyRTrue[0], integrandCellR[0].Au[1], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyRTrue[1], integrandCellR[1].Au[1], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyRTrue[2], integrandCellR[2].Au[1], small_tol, close_tol );


  sRef = 0.5;
  fcnPDEL( {sRef}, integrandCellL, 3, integrandPDEIL, 2 );
  fcnPDER( {sRef}, integrandCellR, 3, integrandPDEIR, 2 );

  integrandPDEI[0] = integrandPDEIL[0] + integrandPDEIR[0];
  integrandPDEI[1] = integrandPDEIL[1] + integrandPDEIR[1];

  // PDE residuals (left): (advective) + (viscous) + (stablization)
  integrandCellLTrue[0] = (0)                 + (0)                 + (0);
  integrandCellLTrue[1] = ( 147./(40*sqrt(2))) + (-6369./(2000*sqrt(2))) + (0);
  integrandCellLTrue[2] = ( 147./(40*sqrt(2))) + (-6369./(2000*sqrt(2))) + (0);

  // PDE residuals (right)
  integrandCellRTrue[0] = (0)                 + (0)                   + (0);
  integrandCellRTrue[1] = (-147./(40*sqrt(2))) + (2123./(4000*sqrt(2))) + (0);
  integrandCellRTrue[2] = (-147./(40*sqrt(2))) + (2123./(4000*sqrt(2))) + (0);

  // interface residuals
  integrandPDEITrue[0] = (0) + (2123./(800*sqrt(2))) + (0);
  integrandPDEITrue[1] = (0) + (2123./(800*sqrt(2))) + (0);

  // auxiliary variable definition
//  integrandAuxLTrue[0] = 0;
//  integrandAuxLTrue[1] = 0;
//  integrandAuxLTrue[2] = 0;
//  integrandAuyLTrue[0] = 0;
//  integrandAuyLTrue[1] = 0;
//  integrandAuyLTrue[2] = 0;
//
//  integrandAuxRTrue[0] = 0;
//  integrandAuxRTrue[1] = -2123./(1000*sqrt(2));
//  integrandAuxRTrue[2] = -2123./(1000*sqrt(2));
//  integrandAuyRTrue[0] = 0;
//  integrandAuyRTrue[1] = 0;
//  integrandAuyRTrue[2] = 0;

  SANS_CHECK_CLOSE( integrandCellLTrue[0], integrandCellL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandCellLTrue[1], integrandCellL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandCellLTrue[2], integrandCellL[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandCellRTrue[0], integrandCellR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandCellRTrue[1], integrandCellR[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandCellRTrue[2], integrandCellR[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDEITrue[0], integrandPDEI[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDEITrue[1], integrandPDEI[1], small_tol, close_tol );

//  SANS_CHECK_CLOSE( integrandAuxLTrue[0], integrandCellL[0].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuxLTrue[1], integrandCellL[1].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuxLTrue[2], integrandCellL[2].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyLTrue[0], integrandCellL[0].Au[1], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyLTrue[1], integrandCellL[1].Au[1], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyLTrue[2], integrandCellL[2].Au[1], small_tol, close_tol );
//
//  SANS_CHECK_CLOSE( integrandAuxRTrue[0], integrandCellR[0].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuxRTrue[1], integrandCellR[1].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuxRTrue[2], integrandCellR[2].Au[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyRTrue[0], integrandCellR[0].Au[1], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyRTrue[1], integrandCellR[1].Au[1], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandAuyRTrue[2], integrandCellR[2].Au[1], small_tol, close_tol );

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
