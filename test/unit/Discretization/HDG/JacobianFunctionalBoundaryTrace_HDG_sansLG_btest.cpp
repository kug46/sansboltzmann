// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianFunctionalBoundaryTrace_HDG_sansLG_btest
// testing of 2-D functional boundary - output integral jacobian

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/list.hpp>

#include "tools/SANSnumerics.h"     // Real

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Field/XField_CellToTrace.h"

#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "Surreal/SurrealS.h"

#include "Discretization/HDG/IntegrandCell_HDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"

#include "Discretization/HDG/SetFieldCell_HDG_AuxiliaryVariable.h"
#include "Discretization/HDG/SetFieldBoundaryTrace_HDG_AuxiliaryVariable.h"

#include "Discretization/HDG/IntegrandBoundaryTrace_Flux_mitState_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_OutputWeightRsd_HDG.h"
#include "Discretization/HDG/FunctionalBoundaryTrace_HDG_sansLG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_HDG_AuxiliaryVariable.h"
#include "Discretization/HDG/JacobianFunctionalBoundaryTrace_HDG_sansLG.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/QuadratureOrder.h"

#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#include "IntegrandTest_HDG.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

#define SANS_CHECK_PING_ORDER( err_vec, delta_vec, small_tol ) \
    if (fabs(err_vec[1]) > small_tol) \
    { \
      Real rate = log(err_vec[1]/err_vec[0])/log(delta_vec[1]/delta_vec[0]); \
      BOOST_CHECK_MESSAGE( rate >= 1.9 && rate <= 4.0, "Rate check failed: rate = " << rate << \
                           ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" ); \
    }

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

namespace           // local definition
{

template<class SurrealClass, class IntegrandCell, class IntegrandITrace, class IntegrandBTrace,
         class JacAUXClass,
         class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
void computeAuxiliaryVariables(const IntegrandCell& fcnCell, const IntegrandITrace& fcnITrace, const IntegrandBTrace& fcnBTrace,
                               const FieldDataInvMassMatrix_Cell& mmfld, JacAUXClass& jacAUX_a_bcell,
                               const XField_CellToTrace<PhysDim, TopoDim>& connectivity,
                               const XField<PhysDim, TopoDim>& xfld, const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                               const Field<PhysDim, TopoDim, VectorArrayQ>& afld, const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                               const QuadratureOrder& quadOrder)
{
  jacAUX_a_bcell = 0.0;

  FieldDataVectorD_BoundaryCell<VectorArrayQ> rsdAUX_bcell(qfld);
  rsdAUX_bcell = 0.0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
      SetFieldBoundaryTrace_HDG_AuxiliaryVariable( fcnBTrace, rsdAUX_bcell, jacAUX_a_bcell ),
      xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

  std::vector<int> cellgroupsAux(fcnCell.nCellGroups());
  for (std::size_t i = 0; i < fcnCell.nCellGroups(); i++)
    cellgroupsAux[i] = fcnCell.cellGroup(i);

  //Note: jacAUX_a_bcell gets completed and inverted inside SetFieldCell_HDG_AuxiliaryVariable
  IntegrateCellGroups<TopoDim>::integrate(
      SetFieldCell_HDG_AuxiliaryVariable( fcnCell, fcnITrace, mmfld, connectivity,
                                          xfld, qfld, afld, qIfld,
                                          quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                          rsdAUX_bcell, cellgroupsAux, true, jacAUX_a_bcell),
      xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );
}

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianFunctionalBoundaryTrace_HDG_sansLG_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_P1_Linear_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef SurrealS<4> SurrealClass;

  // BC INTEGRAND
  typedef BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass> BCClass;
  typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;

  typedef IntegrandCell_HDG<NDPDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<NDPDEClass> IntegrandITrace;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, HDG> IntegrandBCClass;

  // DRAG INTEGRAND
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;

  typedef DiscretizationHDG<NDPDEClass> DiscretizationClass;

  typedef NDPDEClass::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef NDPDEClass::template TensorMatrixQ<Real> TensorMatrixQ;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDOutputClass::template ArrayJ<Real> ArrayJ;
  typedef typename NDOutputClass::template MatrixJ<Real> MatrixJ;

  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, HDG> IntegrandOutputClass;

  // pde class
  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)
  const Real muRef = 1.0;        // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  DiscretizationClass disc(pde, Global, Gradient );

  // integrand
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandITrace fcnITrace( pde, disc, {} );

  NDBCClass bc(pde);
  IntegrandBCClass fcnBTrace( pde, bc, {0,1,2}, disc );

  NDOutputClass outputFcn(pde, 1., 0.);
  IntegrandOutputClass output_integrand( outputFcn, {0,1,2} );

  //GRID: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;
//  XField2D_Box_Triangle_X1 xfld(2,2);

  XField_CellToTrace<PhysD2, TopoD2> connectivity(xfld);

  int qorder = 2;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> bfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  bfld = 0;

  // quadrature rule
  QuadratureOrder quadOrder(xfld, -1);

  // solution: P2

  BOOST_REQUIRE( qfld.nDOF() == 6 );

  qfld.DOF(0) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(1.0, 0.5, 0.6, 1.0));
  qfld.DOF(1) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(1.1, 0.3, 0.4, 2.0));
  qfld.DOF(2) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(1.2, 0.1, 0.2, 3.0));

  qfld.DOF(3) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(0.1, 0.3, 0.6, 0.1));
  qfld.DOF(4) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(0.2, 0.2, 0.4, 0.2));
  qfld.DOF(5) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(0.3, 0.5, 0.2, 0.4));

  BOOST_REQUIRE( afld.nDOF() == 6 );

  afld.DOF(0) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(1.1, 0.6, 0.7, 1.1));
  afld.DOF(1) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(1.2, 0.4, 0.5, 2.2));
  afld.DOF(2) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(1.3, 0.2, 0.3, 3.3));

  afld.DOF(3) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(0.3, 0.2, 0.7, 0.2));
  afld.DOF(4) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(0.1, 0.1, 0.5, 0.3));
  afld.DOF(5) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(0.4, 0.4, 0.3, 0.5));

  BOOST_REQUIRE( qIfld.nDOF() == 9 );

  qIfld.DOF(0) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(1.1, 0.6, 0.5, 1.1));
  qIfld.DOF(1) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(1.0, 0.5, 0.6, 1.0));
  qIfld.DOF(2) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(1.2, 0.1, 0.2, 3.0));

  qIfld.DOF(3) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(1.1, 0.6, 0.5, 1.1));
  qIfld.DOF(4) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(1.0, 0.5, 0.6, 1.0));
  qIfld.DOF(5) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(1.2, 0.1, 0.2, 3.0));

  qIfld.DOF(6) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(1.1, 0.6, 0.5, 1.1));
  qIfld.DOF(7) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(1.0, 0.5, 0.6, 1.0));
  qIfld.DOF(8) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(1.2, 0.1, 0.2, 3.0));

  // Compute the field of inverse mass matrices
  FieldDataInvMassMatrix_Cell mmfld(qfld);

  FieldDataMatrixD_BoundaryCell<TensorMatrixQ> jacAUX_a_bcell(qfld);

  // Via Surreals
  DLA::VectorD<MatrixJ> jacFunctional_qL( qfld.nDOF() );
  DLA::VectorD<MatrixJ> jacFunctional_aL( PhysD2::D*afld.nDOF() );
  DLA::VectorD<MatrixJ> jacFunctional_qI( qIfld.nDOF() );
  jacFunctional_qL = 0;
  jacFunctional_aL = 0;
  jacFunctional_qI = 0;

  computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                          xfld, qfld, afld, qIfld, quadOrder);

  FieldDataMatrixD_BoundaryCell<VectorMatrixQ> jacAUX_q_bcell(qfld);
  FieldDataMatrixD_BoundaryTrace<VectorMatrixQ> jacAUX_qI_btrace(qfld, qIfld);
  jacAUX_q_bcell = 0.0;
  jacAUX_qI_btrace = 0.0;

  std::vector<std::vector<std::vector<int>>> mapDOFGlobal_qI(xfld.nBoundaryTraceGroups());

  // Compute Jacobians via Surreals
  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      JacobianBoundaryTrace_HDG_AuxiliaryVariable<SurrealClass>( fcnBTrace, jacAUX_q_bcell, jacAUX_qI_btrace, mapDOFGlobal_qI ),
      xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      JacobianFunctionalBoundaryTrace_HDG_sansLG<SurrealClass>( output_integrand, fcnBTrace, fcnCell, fcnITrace,
                                                                jacAUX_a_bcell, jacAUX_q_bcell, jacAUX_qI_btrace,
                                                                mapDOFGlobal_qI, connectivity,
                                                                xfld, qfld, afld, qIfld,
                                                                quadOrder.cellOrders.data(), quadOrder.cellOrders.size(),
                                                                quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                                                jacFunctional_qL, jacFunctional_qI ),
      xfld, (qfld, afld, bfld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

  // Via Finite Difference
  ArrayJ functional0, functional1;

  const Real small_tol = 5e-10;
  const Real close_tol = 1e-5;

  const Real eps = 1e-4;

  Real delta[2] = {10*eps, eps};

  for (int i = 0; i < qfld.nDOF(); i++)
    for (int n = 0; n < ArrayQ::M; n++)
    {
      Real err_Functional_qL[2];
      Real fdFunctional_qL = 0;
      for (int j = 0; j < 2; j++)
      {
        // Positive pertubation
        qfld.DOF(i)[n] += delta[j];

        computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                                xfld, qfld, afld, qIfld, quadOrder);

        functional1 = 0;
        IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
            FunctionalBoundaryTrace_HDG_sansLG( output_integrand, fcnBTrace, functional1 ),
            xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

        // Negative pertubation
        qfld.DOF(i)[n] -= 2*delta[j];

        computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                                xfld, qfld, afld, qIfld, quadOrder);

        functional0 = 0;
        IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
            FunctionalBoundaryTrace_HDG_sansLG( output_integrand, fcnBTrace, functional0 ),
            xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

        // Restore the perturbation
        qfld.DOF(i)[n] += delta[j];

        // functional is a Real
        fdFunctional_qL = (functional1 - functional0)/(2*delta[j]);
        err_Functional_qL[j] = fabs(fdFunctional_qL - jacFunctional_qL[i][n]);
      }

      SANS_CHECK_PING_ORDER( err_Functional_qL, delta, small_tol )
      SANS_CHECK_CLOSE( fdFunctional_qL, jacFunctional_qL[i][n], small_tol, close_tol );
    }

  for (int i = 0; i < qIfld.nDOF(); i++)
    for (int n = 0; n < ArrayQ::M; n++)
    {
      Real err_Functional_qI[2];
      Real fdFunctional_qI = 0;

      for (int j = 0; j < 2; j++)
      {
        // Positive pertubation
        qIfld.DOF(i)[n] += delta[j];

        computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                                xfld, qfld, afld, qIfld, quadOrder);

        functional1 = 0;
        IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
            FunctionalBoundaryTrace_HDG_sansLG( output_integrand, fcnBTrace, functional1 ),
            xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

        // Negative pertubation
        qIfld.DOF(i)[n] -= 2*delta[j];

        computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                                xfld, qfld, afld, qIfld, quadOrder);
        functional0 = 0;
        IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
            FunctionalBoundaryTrace_HDG_sansLG( output_integrand, fcnBTrace, functional0 ),
            xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

        // Restore the perturbation
        qIfld.DOF(i)[n] += delta[j];

        // functional is a Real
        fdFunctional_qI = (functional1 - functional0)/(2*delta[j]);
        err_Functional_qI[j] = fabs(fdFunctional_qI - jacFunctional_qI[i][n]);
      }

      SANS_CHECK_PING_ORDER( err_Functional_qI, delta, small_tol )
      SANS_CHECK_CLOSE( fdFunctional_qI, jacFunctional_qI[i][n], small_tol, close_tol );
    }
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
