// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AlgebraicEquationSet_Local_HDG_Line_AD_btest
// testing of 1-D HDG local solves with Advection-Diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/ForcingFunction1D_MMS.h"

#include "pde/BCParameters.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"
#include "Discretization/HDG/AlgebraicEquationSet_Local_HDG.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_Trace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "Field/Local/Field_Local.h"
#include "Field/Local/XField_LocalPatch.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/XField1D/XField1D.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( AlgebraicEquationSet_Local_HDG_Line_AD_test_suite )

void localsolve(const XField<PhysD1, TopoD1>& xfld, const int order, const int main_group, const int main_cell)
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                  AdvectiveFlux1D_Uniform,
                                  ViscousFlux1D_Uniform,
                                  Source1D_UniformGrad > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef Field_DG_Cell<PhysD1, TopoD1, ArrayQ> QField1D_DG_Cell;
  typedef Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> AField1D_DG_Cell;
  typedef Field_DG_Trace<PhysD1, TopoD1, ArrayQ> QIField1D_DG_Trace;
  typedef Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> LGField1D_DG_Trace;

  typedef ScalarFunction1D_Sine SolutionExact;
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;

  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> PrimalEquationSetClass_Sparse;
  typedef AlgebraicEquationSet_Local_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                         AlgEqSetTraits_Dense, XField<PhysD1, TopoD1>> PrimalEquationSetClass_Dense;
  typedef PrimalEquationSetClass_Sparse::BCParams BCParams;

  typedef PrimalEquationSetClass_Sparse::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass_Sparse::SystemVector SystemVectorClass;

  typedef PrimalEquationSetClass_Dense::SystemMatrix SystemMatrixClassD;
  typedef PrimalEquationSetClass_Dense::SystemVector SystemVectorClassD;
  typedef PrimalEquationSetClass_Dense::SystemNonZeroPattern SystemNonZeroPatternD;

  // PDE
  AdvectiveFlux1D_Uniform adv( 1.1 );

  Real nu = 2.123;
  ViscousFlux1D_Uniform visc( nu );

  ScalarFunction1D_Sine MMS_soln(1);

  Source1D_UniformGrad source(0., 0.);

  typedef ForcingFunction1D_MMS<PDEAdvectionDiffusion1D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(MMS_soln));

  //Exact solution
  NDSolutionExact solnExact;

  NDPDEClass pde(adv, visc, source, forcingptr);

  // BC

  // Create a BC dictionary
  PyDict Sine;
  Sine[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Function.Sine;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Function] = Sine;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
         BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.SolutionBCType.Robin;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Upwind] = true;

  PyDict PyBCList;
  PyBCList["BCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCName"] = {0, 1};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Global, Gradient );

  // solution: Legendre
  QField1D_DG_Cell qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0;

  // auxiliary operator
  AField1D_DG_Cell afld(xfld, order, BasisFunctionCategory_Legendre);
  afld = 0;

  // interface
  QIField1D_DG_Trace qIfld(xfld, order, BasisFunctionCategory_Legendre);
  qIfld = 0;

  // Lagrange multiplier
  std::vector<int> active_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
  LGField1D_DG_Trace lgfld( xfld, order-1, BasisFunctionCategory_Legendre, active_BGroup_list );
  lgfld = 0;

  QuadratureOrder quadratureOrder( xfld, -1 );
  std::vector<Real> eqnsettol = {1e-9, 1e-9, 1e-9};

  PrimalEquationSetClass_Sparse PrimalEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, eqnsettol,
                                            {0}, {0}, PyBCList, BCBoundaryGroups);

  // residual
  SystemVectorClass q(PrimalEqSet.vectorStateSize());
  SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
  PrimalEqSet.fillSystemVector(q);

  rsd = 0;
  PrimalEqSet.residual(q, rsd);
  std::vector<std::vector<Real>> rsdNorm = PrimalEqSet.residualNorm(rsd);

#if 0
  typedef PrimalEquationSetClass_Sparse::SystemNonZeroPattern SystemNonZeroPattern;

  // jacobian nonzero pattern
  SystemNonZeroPattern nz(PrimalEqSet.matrixSize());
  PrimalEqSet.jacobian(nz);

  // jacobian
  SystemMatrixClass jac(nz);
  jac = 0;
  PrimalEqSet.jacobian(jac);

  cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, "tmp/jac0.mtx" );
#endif

#if 0 //Print residual vector
  cout <<endl << "PDE residual:" <<endl;
  for (int k = 0; k < rsd[0].m(); k++) cout << k << "\t" << rsd[0][k] <<endl;

  cout <<endl << "INT residual:" <<endl;
  for (int k = 0; k < rsd[1].m(); k++) cout << k << "\t" << rsd[1][k] <<endl;

  cout <<endl << "Lagrange multiplier residual:" <<endl;
  for (int k = 0; k < rsd[2].m(); k++) cout << k << "\t" << rsd[2][k] << endl;
#endif

  // solve
  SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);

  SystemVectorClass dq(PrimalEqSet.vectorStateSize());
  solver.solve(rsd, dq);

  // update solution
  q -= dq;
  PrimalEqSet.setSolutionField(q);

  rsd = 0;
  PrimalEqSet.residual(rsd);

#if 0 //Print residual vector
  cout <<endl << "PDE residual:" <<endl;
  for (int k = 0; k < rsd[0].m(); k++) cout << k << "\t" << rsd[0][k] <<endl;

  cout <<endl << "INT residual:" <<endl;
  for (int k = 0; k < rsd[1].m(); k++) cout << k << "\t" << rsd[1][k] <<endl;

  cout <<endl << "Lagrange multiplier residual:" <<endl;
  for (int k = 0; k < rsd[2].m(); k++) cout << k << "\t" << rsd[2][k] << endl;

  std::cout << "Solution:" << std::endl;
  for (int i = 0; i < qfld.nDOF(); i++)
    std::cout << qfld.DOF(i) << std::endl;

  std::cout << std::endl;
  for (int i = 0; i < afld.nDOF(); i++)
    std::cout << afld.DOF(i) << std::endl;

  std::cout << std::endl;
  for (int i = 0; i < qIfld.nDOF(); i++)
    std::cout << qIfld.DOF(i) << std::endl;
#endif

  //Check if residual is zero for global solution
  rsdNorm = PrimalEqSet.residualNorm(rsd);
  BOOST_REQUIRE( PrimalEqSet.convergedResidual(rsdNorm) );

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  mpi::communicator comm_local = xfld.comm()->split(xfld.comm()->rank());

  //Extract the local mesh for the given element
  XField_LocalPatchConstructor<PhysD1,Line> xfld_constructor(comm_local, connectivity, main_group, main_cell, SpaceType::Discontinuous);

  // create an split patch
  XField_LocalPatch<PhysD1,Line> xfld_local( xfld_constructor, ElementSplitType::Edge, 0);

  std::vector<int> active_BGroup_list_local = {}; //Using mitState - so lgfld should be empty

  int nLocalInteriorTraceGroups = std::min(xfld_local.nInteriorTraceGroups(), 2);
  std::vector<int> interiorTraceGroups(nLocalInteriorTraceGroups);
  for (int i = 0; i < nLocalInteriorTraceGroups; i++)
    interiorTraceGroups[i] = i;

  int nLocalBoundaryTraceGroups = xfld_local.nBoundaryTraceGroups();
  std::vector<int> boundaryTraceGroups(nLocalBoundaryTraceGroups);
  for (int i = 0; i < nLocalBoundaryTraceGroups; i++)
    boundaryTraceGroups[i] = i;

  Field_Local<QField1D_DG_Cell> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);
  Field_Local<AField1D_DG_Cell> afld_local(xfld_local, afld, order, BasisFunctionCategory_Legendre);
  Field_Local<QIField1D_DG_Trace> qIfld_local(xfld_local, qIfld, order, BasisFunctionCategory_Legendre, interiorTraceGroups, boundaryTraceGroups);
  Field_Local<LGField1D_DG_Trace> lgfld_local(xfld_local, lgfld, order-1, BasisFunctionCategory_Legendre, active_BGroup_list_local);

  int nMainBTraces = 0;
  std::vector<int> MainBTraceGroup_list;

  for (int i = 0; i < qIfld_local.nBoundaryTraceGroups(); i++)
  {
    MainBTraceGroup_list.push_back(i);
    nMainBTraces += qIfld_local.getBoundaryTraceGroupBase(i).nElem();
  }

  int nMainITraces = 0;
  for (int i = 0; i < qIfld_local.nInteriorTraceGroups(); i++)
    nMainITraces += qIfld_local.getInteriorTraceGroupBase(i).nElem();

  std::map<std::string, std::vector<int>> BCBoundaryGroups_local;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups_local["BCName"] = MainBTraceGroup_list;

#if 0
  xfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  xfld_local.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
#endif

#if 0
//   qfld.dump(3,std::cout);
//   std::cout<<std::endl<<std::endl;
//   qfld_local.dump(3,std::cout);
//   std::cout<<std::endl<<std::endl;

   qIfld.dump(3,std::cout);
   std::cout<<std::endl<<std::endl;
   qIfld_local.dump(3,std::cout);
   std::cout<<std::endl<<std::endl;

//   lgfld.dump(3,std::cout);
//   std::cout<<std::endl<<std::endl;
//   lgfld_local.dump(3,std::cout);
#endif

   QuadratureOrder quadratureOrder_local( xfld_local, -1 );

   std::vector<int> localCellGroups = {0,1};

  PrimalEquationSetClass_Dense PrimalEqSet_Local(xfld_local, qfld_local, afld_local, qIfld_local, lgfld_local,
                                                 pde, disc, quadratureOrder_local, eqnsettol, localCellGroups, {0},
                                                 PyBCList, BCBoundaryGroups_local);

  SystemVectorClassD q_local(PrimalEqSet_Local.vectorStateSize());
  PrimalEqSet_Local.fillSystemVector(q_local);

  // Local residual
  SystemVectorClassD rsd_local(PrimalEqSet_Local.vectorEqSize());
  SystemVectorClassD rsd_local0(PrimalEqSet_Local.vectorEqSize());
  rsd_local0 = 0;
  PrimalEqSet_Local.residual(q_local, rsd_local0);

#if 0 //Print local residual vector
  cout <<endl << "PDE residual:" <<endl;
  for (int k = 0; k < rsd_local0[0].m(); k++) cout << k << "\t" << rsd_local0[0][k] <<endl;

  cout <<endl << "INT residual:" <<endl;
  for (int k = 0; k < rsd_local0[1].m(); k++) cout << k << "\t" << rsd_local0[1][k] <<endl;

  cout <<endl << "Lagrange multiplier residual:" <<endl;
  for (int k = 0; k < rsd_local0[2].m(); k++) cout << k << "\t" << rsd_local0[2][k] << endl;
#endif

  //Jacobian
  SystemNonZeroPatternD nz_local(PrimalEqSet_Local.matrixSize());
  SystemMatrixClassD jac_local(nz_local);
  jac_local = 0;
  PrimalEqSet_Local.jacobian(jac_local);

  int iPDE = PrimalEqSet.iPDE;
  int iINT = PrimalEqSet.iINT;

  int iq = PrimalEqSet.iq;
  int iqI = PrimalEqSet.iqI;

  const Real small_tol = 1e-10;
  const Real close_tol = 5e-9;

  for (int j = 0; j < q_local[iq].m(); j++)
  {
    q_local[iq][j] += 1.0;

    rsd_local = 0;
    PrimalEqSet_Local.residual(q_local, rsd_local);

    q_local[iq][j] -= 1.0;

    for (int i = 0; i < rsd_local[iPDE].m(); i++)
    {
//      std::cout << i << ", " << j << ": " << jac_local(iPDE,iq)(i,j) << ", " << rsd_local[iPDE][i] - rsd_local0[iPDE][i] << std::endl;
      SANS_CHECK_CLOSE(jac_local(iPDE,iq)(i,j), rsd_local[iPDE][i] - rsd_local0[iPDE][i], small_tol, close_tol);
    }

    for (int i = 0; i < rsd_local[iINT].m(); i++)
    {
//      std::cout << i << ", " << j << ": " << jac_local(iINT,iq)(i,j) << ", " << rsd_local[iINT][i] - rsd_local0[iINT][i] << std::endl;
      SANS_CHECK_CLOSE(jac_local(iINT,iq)(i,j), rsd_local[iINT][i] - rsd_local0[iINT][i], small_tol, close_tol);
    }
  }
//  std::cout << std::endl;

  for (int j = 0; j < q_local[iqI].m(); j++)
  {
    q_local[iqI][j] += 1.0;

    rsd_local = 0;
    PrimalEqSet_Local.residual(q_local, rsd_local);

    q_local[iqI][j] -= 1.0;

    for (int i = 0; i < rsd_local[iPDE].m(); i++)
    {
//      std::cout << i << ", " << j << ": " << jac_local(iPDE,iqI)(i,j) << ", " << rsd_local[iPDE][i] - rsd_local0[iPDE][i] << std::endl;
      SANS_CHECK_CLOSE(jac_local(iPDE,iqI)(i,j), rsd_local[iPDE][i] - rsd_local0[iPDE][i], small_tol, close_tol);
    }

    for (int i = 0; i < rsd_local[iINT].m(); i++)
    {
//      std::cout << i << ", " << j << ": " << jac_local(iINT,iqI)(i,j) << ", " << rsd_local[iINT][i] - rsd_local0[iINT][i] << std::endl;
      SANS_CHECK_CLOSE(jac_local(iINT,iqI)(i,j), rsd_local[iINT][i] - rsd_local0[iINT][i], small_tol, close_tol);
    }
  }
//  std::cout << std::endl;

  rsd_local = 0;
  PrimalEqSet_Local.residual(q_local, rsd_local);

  //Local solve - with extracted sub-system
  SystemVectorClassD dq_local(PrimalEqSet_Local.vectorStateSize());
  dq_local = DLA::InverseLU::Solve(jac_local,rsd_local);

  //Update local solution fields
  q_local -= dq_local;
  PrimalEqSet_Local.setSolutionField(q_local);

#if 0
  std::cout << "solution:" << std::endl;
  for (int i = 0; i < qfld_local.nDOF(); i++)
    std::cout << qfld_local.DOF(i) << std::endl;

  std::cout << std::endl;
  for (int i = 0; i < afld_local.nDOF(); i++)
    std::cout << afld_local.DOF(i) << std::endl;

  std::cout << std::endl;
  for (int i = 0; i < qIfld_local.nDOF(); i++)
    std::cout << qIfld_local.DOF(i) << std::endl;
#endif

  rsd_local = 0;
  PrimalEqSet_Local.residual(rsd_local);

#if 0 //Print local residual vector
  cout <<endl << "PDE residual:" <<endl;
  for (int k = 0; k < rsd_local[0].m(); k++) cout << k << "\t" << rsd_local[0][k] <<endl;

  cout <<endl << "INT residual:" <<endl;
  for (int k = 0; k < rsd_local[1].m(); k++) cout << k << "\t" << rsd_local[1][k] <<endl;

  cout <<endl << "Lagrange multiplier residual:" <<endl;
  for (int k = 0; k < rsd_local[2].m(); k++) cout << k << "\t" << rsd_local[2][k] << endl;
#endif

  rsdNorm = PrimalEqSet_Local.residualNorm(rsd_local);
  PrimalEqSet_Local.printDecreaseResidualFailure(rsdNorm, std::cout);

  BOOST_REQUIRE( PrimalEqSet_Local.convergedResidual(rsdNorm) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AlgEqnSet_Local_HDG_Line_AD_LeftBoundaryCell_Target_P2_ii3 )
{
  int ii = 3;
  XField1D xfld( ii, 0, ii );
  int order = 2;

  int main_group = 0;
  int main_cell = 0;
  localsolve(xfld, order, main_group, main_cell);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AlgEqnSet_Local_HDG_Line_AD_InteriorCell_Target_P2_ii3 )
{
  int ii = 3;
  XField1D xfld( ii, 0, ii );
  int order = 2;

  int main_group = 0;
  int main_cell = 1;
  localsolve(xfld, order, main_group, main_cell);
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
