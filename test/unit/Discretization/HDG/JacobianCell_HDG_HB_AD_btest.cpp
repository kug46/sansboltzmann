// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianCell_HDG_AD_btest
// testing of HDG cell-integral jacobian: advection-diffusions

#include <boost/mpl/list.hpp>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
//#include "Field/FieldVolume_DG_Cell.h"

#include "Discretization/HDG/IntegrandFunctor_HDG_HB.h"
#include "Discretization/HDG/JacobianCell_HDG_HB.h"
#include "Discretization/HDG/ResidualCell_HDG_HB.h"
#include "Surreal/SurrealS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct
namespace           // local definition
{

//----------------------------------------------------------------------------//
// jacobian dumps
template <int N>
void
dump( Real jac[][N], int m, int n )
{
  cout << "{";
  for (int i = 0; i < m; i++)
  {
    cout << "{";
    for (int j = 0; j < n; j++)
    {
      cout << jac[i][j];
      if (j < n-1) cout << ", ";
    }
    cout << "}";
    if (i < m-1) cout << ", ";
  }
  cout << "}" << std::endl;
}

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianCell_HDG_HB_Triangle_AD_test_suite )

typedef boost::mpl::list< SurrealS<1>, SurrealS<2> > Surreals;


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_HB_1D_1Line_X1Q1_Surreal1 )
{
  typedef SurrealS<1> SurrealClass;

  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandCell_HDG_HB<PDEClass> IntegrandClass;

  Real u = 1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // grid: line
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single line, P0 (aka Q0)
  int qorder = 1;

  // solution data
  typedef Field<PhysD1, TopoD1, ArrayQ> FieldType;
  typedef std::unique_ptr< FieldType > FieldPtr;

  std::vector< FieldPtr > qflds;

  int ntimes = 3;
  Real period = 1.;

  for (int j=0; j<ntimes; j++)
    qflds.push_back( FieldPtr(new Field_DG_Cell<PhysD1, TopoD1, ArrayQ>(xfld, qorder, BasisFunctionCategory_Hierarchical)) );

  qflds[0]->DOF(0) = 0;
  qflds[0]->DOF(1) = 0;
  qflds[1]->DOF(0) = 0;
  qflds[1]->DOF(1) = 0;
  qflds[2]->DOF(0) = 0;
  qflds[2]->DOF(1) = 0;

  int nDOF = qflds[0]->nDOF();
  BOOST_CHECK_EQUAL( nDOF, 2);

  // auxiliary variable
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // quadrature rule
  int quadratureOrder = -1;

  // HDG discretization (not used)
  DiscretizationHDG<PDEClass> disc( pde, Global, Gradient );

  // Time integration parameters

  // integrand
  IntegrandClass fcnint( pde, disc, period, ntimes );

  // initialize Residual Vector of ntimes residual vectors
  typedef SLA::SparseVector<ArrayQ> SparseVectorClass;
  typedef DLA::VectorD< SparseVectorClass > HBVectorClass;

  HBVectorClass rsdPDEGlobalVec0(ntimes);
  HBVectorClass rsdPDEGlobalVec1(ntimes);
  for (int i=0; i<ntimes; i++)
  {
    rsdPDEGlobalVec0(i).resize( nDOF ); //set size of individual residual vectors in rsdHB
    rsdPDEGlobalVec1(i).resize( nDOF );
    for (int j=0; j<nDOF; j++)
    {
      rsdPDEGlobalVec0(i)(j) = 0;
      rsdPDEGlobalVec1(i)(j) = 0;
    }
  }

  // jacobian via FD w/ residual operator; assumes scalar PDE

  Real jacPDE_qVec[3][3][2][2];
  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  for (int t1 = 0; t1 < ntimes; t1++)
    ResidualCell_HDG_HB<TopoD1>::integrate( fcnint, t1, qflds, &quadratureOrder, 1, rsdPDEGlobalVec0(t1) );


  for (int t2 = 0; t2 < ntimes; t2++)
    for (int j = 0; j < nDOF; j++)
    {
      qflds[t2]->DOF(j) += 1;
      for (int t1 =0; t1 < ntimes; t1++)
      {
        rsdPDEGlobalVec1(t1) = 0;

        ResidualCell_HDG_HB<TopoD1>::integrate( fcnint, t1, qflds, &quadratureOrder, 1, rsdPDEGlobalVec1(t1) );
        for (int i = 0; i < nDOF; i++)
          jacPDE_qVec[t1][t2][i][j] = rsdPDEGlobalVec1(t1)(i) - rsdPDEGlobalVec0(t1)(i);
      }
      qflds[t2]->DOF(j) -= 1;
    }

#if 0
  std::cout << "jacPDE_q = " << std::endl; dump(jacPDE_q,1,1);
  std::cout << "jacPDE_a = " << std::endl; dump(jacPDE_a,1,2);
  std::cout << "jacAu_q = " << std::endl; dump(jacAu_q,2,1);
  std::cout << "jacAu_a = " << std::endl; dump(jacAu_a,2,2);
#endif

  // jacobian via Surreal

  // initialize Jacobian matrix
  typedef DLA::MatrixD<MatrixQ> MatrixClass;
  typedef DLA::MatrixD<MatrixClass> HBMatrixClass;

  HBMatrixClass mtxPDEGlob_q(ntimes,ntimes);
  for (int t1=0; t1<ntimes; t1++)
    for (int t2=0; t2<ntimes; t2++)
    {
      mtxPDEGlob_q(t1,t2) = {nDOF,nDOF};
      for (int i = 0; i < nDOF; i++)
        for (int j = 0; j < nDOF; j++)
          mtxPDEGlob_q(t1,t2)(i,j) = 0;
    }

  // compute Jacobian
  for (int t1=0; t1<ntimes; t1++)
    for (int t2=0; t2<ntimes; t2++)
    {
      //std::cout << "t1: " << t1 << " t2: " << t2 << "\n";

      JacobianCell_HDG_HB<SurrealClass,TopoD1>::integrate( fcnint, t1, t2, qflds, &quadratureOrder, 1, mtxPDEGlob_q(t1,t2) );

      for (int i = 0; i < nDOF; i++)
        for (int j = 0; j < nDOF; j++)
          SANS_CHECK_CLOSE( jacPDE_qVec[t1][t2][i][j], mtxPDEGlob_q(t1,t2)(i,j), small_tol, close_tol );

    }

  //matrix dump to screen
#if 1
  for (int t1 = 0; t1 < ntimes; t1++)
    for (int i=0; i<nDOF; i++)
    {
      for (int t2=0; t2<ntimes; t2++)
        for (int j=0; j<nDOF; j++)
        {
          std::cout << jacPDE_qVec[t1][t2][i][j] << " " ;
        }

      std::cout << "\n";
    }
#endif




}




////----------------------------------------------------------------------------//
//BOOST_AUTO_TEST_CASE( HGD_HB_2D_1Triangle_X1Q0_Surreal1 )
//{
//  typedef SurrealS<1> SurrealClass;
//
//  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
//  typedef PDEClass::template ArrayQ<Real> ArrayQ;
//  typedef PDEClass::template MatrixQ<Real> MatrixQ;
//  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
//
//  typedef IntegrandCell_HDG_HB<PDEClass> IntegrandClass;
//
//  Real u = 1;
//  Real v = 0.2;
//  AdvectiveFlux2D_Uniform adv(u, v);
//
//  Real kxx = 2.123;
//  Real kxy = 0.553;
//  Real kyy = 1.007;
//  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);
//
//  GlobalTime time(0);
//  PDEClass pde(time, adv, visc);
//
//
//  // static tests
//  BOOST_CHECK( pde.D == 2 );
//  BOOST_CHECK( pde.N == 1 );
//
//  // grid: single triangle, P1 (aka X1)
//  XField2D_1Triangle_X1_1Group xfld;
//
//  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
//  BOOST_CHECK_EQUAL( 1, xfld.nElem() );
//
//  // solution: single triangle, P1 (aka Q1)
//  int qorder = 0;
//  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
//  // solution data
//  qfld.DOF(0) = 1;
//
//  typedef Field<PhysD2, TopoD2, ArrayQ> FieldType;
//  typedef std::unique_ptr< FieldType > FieldPtr;
//
//  std::vector< FieldPtr > qflds;
//
//  int ntimes = 2;
//
//  for (int j=0; j<ntimes-1; j++)
//  {
//    qflds.push_back( FieldPtr(new Field_DG_Cell<PhysD2, TopoD2, ArrayQ>(xfld, qorder, BasisFunctionCategory_Legendre)) );
//  }
//
//  qflds[0]->DOF(0) = 0;
//
//  // auxiliary variable
//  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Legendre);
//
//  // auxiliary variable
//  afld.DOF(0) = { 2, -3};
//
//  // quadrature rule (linear: basis is linear, solution is const)
//  int quadratureOrder = 1;
//
//  // HDG discretization (not used)
//  DiscretizationHDG<PDEClass> disc( pde, 1 );
//
//  //temporal discretization
//  Real dt = 13.;
//  std::vector<Real> weights {1., -1.}; //HB1 weights
//
//  // integrand
//  IntegrandClass fcnint( pde, disc, dt, weights );
//
//  // jacobian via FD w/ residual operator; assumes scalar PDE
//
//  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(1), rsdPDEGlobal1(1);
//  Real jacPDE_q[1][1];
//
//  rsdPDEGlobal0 = 0;
//
//  ResidualCell_HDG_HB<TopoD2>::integrate( fcnint, qfld, afld, qflds, &quadratureOrder, 1, rsdPDEGlobal0 );
//
//  qfld.DOF(0) += 1;
//  rsdPDEGlobal1 = 0;
//
//  ResidualCell_HDG_HB<TopoD2>::integrate( fcnint, qfld, afld, qflds, &quadratureOrder, 1, rsdPDEGlobal1 );
//
//  jacPDE_q[0][0] = rsdPDEGlobal1[0](0) - rsdPDEGlobal0[0](0);
//
//  qfld.DOF(0) -= 1;
//
//#if 0
//  std::cout << "jacPDE_q = " << std::endl; dump(jacPDE_q,1,1);
//  std::cout << "jacPDE_a = " << std::endl; dump(jacPDE_a,1,2);
//  std::cout << "jacAu_q = " << std::endl; dump(jacAu_q,2,1);
//  std::cout << "jacAu_a = " << std::endl; dump(jacAu_a,2,2);
//#endif
//
//  // jacobian via Surreal
//
//  DLA::MatrixD<MatrixQ> mtxPDEGlob_q(1,1);
//  mtxPDEGlob_q = 0;
//
//  JacobianCell_HDG_HB<SurrealClass,TopoD2>::integrate( fcnint, qfld, afld, qflds, &quadratureOrder, 1, mtxPDEGlob_q );
//
//  const Real small_tol = 1e-12;
//  const Real close_tol = 1e-12;
//  SANS_CHECK_CLOSE( jacPDE_q[0][0], mtxPDEGlob_q(0,0)(0,0), small_tol, close_tol );
//
//}
//
//
//////----------------------------------------------------------------------------//
//BOOST_AUTO_TEST_CASE_TEMPLATE( HDG_HB_Galerkin_1Triangle_X1Q1_Surreal, SurrealClass, Surreals )
//{
//  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
//  typedef PDEClass::template ArrayQ<Real> ArrayQ;
//  typedef PDEClass::template MatrixQ<Real> MatrixQ;
//  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
//
//  typedef IntegrandCell_HDG_HB<PDEClass> IntegrandClass;
//
//  Real u = 1;
//  Real v = 0.2;
//  AdvectiveFlux2D_Uniform adv(u, v);
//
//  Real kxx = 2.123;
//  Real kxy = 0.553;
//  Real kyy = 1.007;
//  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);
//
//  GlobalTime time(0);
//
//  PDEClass pde(time, adv, visc);
//
//  // static tests
//  BOOST_CHECK( pde.D == 2 );
//  BOOST_CHECK( pde.N == 1 );
//
//  // grid: single triangle, P1 (aka X1)
//  XField2D_1Triangle_X1_1Group xfld;
//
//  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
//  BOOST_CHECK_EQUAL( 1, xfld.nElem() );
//
//  // solution: single triangle, P1 (aka Q1)
//  int qorder = 1;
//  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
//
//  // solution data
//  qfld.DOF(0) = 1;
//  qfld.DOF(1) = 3;
//  qfld.DOF(2) = 4;
//
//  typedef Field<PhysD2, TopoD2, ArrayQ> FieldType;
//  typedef std::unique_ptr< FieldType > FieldPtr;
//
//  std::vector< FieldPtr > qflds;
//
//  int ntimes = 3;
//
//  for (int j=0; j<ntimes-1; j++)
//  {
//    qflds.push_back( FieldPtr(new Field_DG_Cell<PhysD2, TopoD2, ArrayQ>(xfld, qorder, BasisFunctionCategory_Legendre)) );
//  }
//
//  qflds[0]->DOF(0) = 0;
//  qflds[0]->DOF(1) = 0;
//  qflds[0]->DOF(2) = 0;
//
//  qflds[1]->DOF(0) = -1./3.;
//  qflds[1]->DOF(1) = -4./9.;
//  qflds[1]->DOF(2) = -5.;
//
//  // auxiliary variable
//  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Legendre);
//
//  // auxiliary variable
//  afld.DOF(0) = { 2, -3};
//  afld.DOF(1) = { 7,  8};
//  afld.DOF(2) = {-1, -5};
//
//  // quadrature rule (linear: basis is linear, solution is const)
//  int quadratureOrder = 1;
//
//  // HDG discretization (not used)
//  DiscretizationHDG<PDEClass> disc( pde, 1 );
//
//  // Temporal discretization
//  Real dt = 13.;
//  std::vector<Real> weights {1.5, -2., 0.5}; //HB2 weights
//
//  // integrand
//  IntegrandClass fcnint( pde, disc, dt, weights );
//
//  // jacobian via FD w/ residual operator; assumes scalar PDE
//  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(3), rsdPDEGlobal1(3);
//  Real jacPDE_q[3][3];
//
//  rsdPDEGlobal0 = 0;
//  ResidualCell_HDG_HB<TopoD2>::integrate( fcnint, qfld, afld, qflds, &quadratureOrder, 1, rsdPDEGlobal0 );
//
//  for (int j = 0; j < 3; j++)
//  {
//    qfld.DOF(j) += 1;
//
//    rsdPDEGlobal1 = 0;
//    ResidualCell_HDG_HB<TopoD2>::integrate( fcnint, qfld, afld, qflds, &quadratureOrder, 1, rsdPDEGlobal1 );
//
//    qfld.DOF(j) -= 1;
//
//    for (int i = 0; i < 3; i++)
//      jacPDE_q[i][j]     = rsdPDEGlobal1[i](0)    - rsdPDEGlobal0[i](0);
//  }
//
//#if 0
//  std::cout << "jacPDE_q = " << std::endl; dump(jacPDE_q,3,3);
//  std::cout << "jacPDE_a = " << std::endl; dump(jacPDE_a,3,6);
//  std::cout << "jacAu_q = " << std::endl; dump(jacAu_q,6,3);
//  std::cout << "jacAu_a = " << std::endl; dump(jacAu_a,6,6);
//#endif
//
//  // jacobian via Surreal
//
//  DLA::MatrixD<MatrixQ> mtxPDEGlob_q(3,3);
//  mtxPDEGlob_q = 0;
//
//  JacobianCell_HDG_HB<SurrealClass,TopoD2>::integrate( fcnint, qfld, afld, qflds, &quadratureOrder, 1, mtxPDEGlob_q );
//
//#if 0
//  std::cout << "mtxPDEGlob_q " << std::endl; mtxPDEGlob_q.dump();
//  std::cout << "mtxPDEGlob_a " << std::endl; mtxPDEGlob_a.dump();
//  std::cout << "mtxAuGlob_q " << std::endl; mtxAuGlob_q.dump();
//  std::cout << "mtxAuGlob_a " << std::endl; mtxAuGlob_a.dump();
//#endif
//
//  const Real small_tol = 1e-12;
//  const Real close_tol = 1e-12;
//  for (int i = 0; i < 3; i++)
//    for (int j = 0; j < 3; j++)
//      SANS_CHECK_CLOSE( jacPDE_q[i][j], mtxPDEGlob_q(i,j)(0,0), small_tol, close_tol );
//
//}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
