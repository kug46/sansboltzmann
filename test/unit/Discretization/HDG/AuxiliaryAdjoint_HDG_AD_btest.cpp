// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AuxiliaryAdjoint_HDG_AD_btest
// testing computing the auxiliary adjoint of HDG with Advection-Diffusion
// Constructs the complete PDE, AUX and INT system and compares with statically condensed solves

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/SurrealS.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "Discretization/HDG/IntegrandCell_HDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_sansLG_HDG.h"

#include "Discretization/HDG/SetFieldCell_HDG_AuxiliaryVariable.h"
#include "Discretization/HDG/SetFieldCell_HDG_AuxiliaryAdjoint.h"
#include "Discretization/HDG/SetFieldBoundaryTrace_HDG_AuxiliaryVariable.h"
#include "Discretization/HDG/SetFieldBoundaryTrace_HDG_AuxiliaryAdjoint.h"
#include "Discretization/HDG/ResidualCell_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_HDG.h"
#include "Discretization/HDG/JacobianCell_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_HDG_AuxiliaryVariable.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/QuadratureOrder.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_Trace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Field/XField_CellToTrace.h"
#include "Field/tools/for_each_CellGroup.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

#include "JacobianTest_HDG.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;
using namespace SANS::testspace;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

namespace // no-name namespace keep things private to this file
{
template<class SurrealClass, class IntegrandCell, class IntegrandITrace, class IntegrandBTrace,
         class JacAUXClass,
         class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
void computeAuxiliaryVariables(const IntegrandCell& fcnCell, const IntegrandITrace& fcnITrace, const IntegrandBTrace& fcnBTrace,
                               const FieldDataInvMassMatrix_Cell& mmfld, JacAUXClass& jacAUX_a_bcell,
                               const XField_CellToTrace<PhysDim, TopoDim>& connectivity,
                               const XField<PhysDim, TopoDim>& xfld, const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                               const Field<PhysDim, TopoDim, VectorArrayQ>& afld, const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                               const QuadratureOrder& quadOrder)
{
  jacAUX_a_bcell = 0.0;

  FieldDataVectorD_BoundaryCell<VectorArrayQ> rsdAUX_bcell(qfld);
  rsdAUX_bcell = 0.0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
      SetFieldBoundaryTrace_HDG_AuxiliaryVariable( fcnBTrace, rsdAUX_bcell, jacAUX_a_bcell ),
      xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

  std::vector<int> cellgroupsAux(fcnCell.nCellGroups());
  for (std::size_t i = 0; i < fcnCell.nCellGroups(); i++)
    cellgroupsAux[i] = fcnCell.cellGroup(i);

  //Note: jacAUX_a_bcell gets completed and inverted inside SetFieldCell_HDG_AuxiliaryVariable
  IntegrateCellGroups<TopoDim>::integrate(
      SetFieldCell_HDG_AuxiliaryVariable( fcnCell, fcnITrace, mmfld, connectivity,
                                          xfld, qfld, afld, qIfld,
                                          quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                          rsdAUX_bcell, cellgroupsAux, true, jacAUX_a_bcell),
      xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );
}
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( AuxiliaryAdjoint_HDG_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AuxiliaryAdjoint_HDG_AD_Line )
{
  const int D = PhysD1::D;

  typedef SurrealS<1> SurrealClass;

  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template MatrixQ<Real> MatrixQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef NDPDEClass::template TensorMatrixQ<Real> TensorMatrixQ;

  typedef ScalarFunction1D_Sine SolutionExact;

  typedef BCTypeFunctionLinearRobin_sansLG BCType;
  typedef BCNDConvertSpace<PhysD1, BCAdvectionDiffusion<PhysD1,BCType>> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandCell_HDG<NDPDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<NDPDEClass> IntegrandITraceClass;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, HDG> IntegrandBTraceClass;

  // PDE
  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv( u );

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 0.38, b = -0.67;
  Source1D_UniformGrad source(a, b);

  NDPDEClass pde( adv, visc, source );

  // BC
  std::shared_ptr<SolutionExact> solnExact( new SolutionExact );

  Real A = 0.64;
  Real B = 0.32;
  BCClass bc( solnExact, visc, A, B );

  // grid: HierarchicalP1 (aka X1)

  int ii = 4;

  XField1D xfld( ii, 0, ii );

  XField_CellToTrace<PhysD1, TopoD1> connectivity(xfld);

  // solution
  int order = 2;

  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0;

  // Auxiliary variable
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
  afld = 0;

  // Interface variable
  Field_DG_Trace<PhysD1, TopoD1, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);
  qIfld = 0;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld( xfld, order-1, BasisFunctionCategory_Legendre, {} );
  lgfld = 0;

  const int nDOFPDE = qfld.nDOF();
  const int nDOFAUX = D*afld.nDOF();
  const int nDOFINT = qIfld.nDOF();
  const int nDOFBC = lgfld.nDOF();

  // Compute the field of inverse mass matrices
  FieldDataInvMassMatrix_Cell mmfld(afld);

  // quadrature rule
  QuadratureOrder quadOrder(xfld, 2*order);

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Local, Gradient );

  // integrands
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandITraceClass fcnITrace( pde, disc, {0} );
  IntegrandBTraceClass fcnBTrace( pde, bc, {0,1}, disc );

  // linear system setup

  // complete jacobian nonzero pattern without static condensation of auxiliary variables
  //
  //        q   a  qI  lg
  //   PDE  X   X   X   X
  //   AUX  X   X   X   X
  //   IINT X   X   X   0
  //   BC   X   X   0   0

  DLA::MatrixD<DLA::MatrixD<MatrixQ>> jacT =
  {{ {nDOFPDE, nDOFPDE}, {nDOFPDE, nDOFAUX}, {nDOFPDE, nDOFINT}, {nDOFPDE, nDOFBC} },
   { {nDOFAUX, nDOFPDE}, {nDOFAUX, nDOFAUX}, {nDOFAUX, nDOFINT}, {nDOFAUX, nDOFBC} },
   { {nDOFINT, nDOFPDE}, {nDOFINT, nDOFAUX}, {nDOFINT, nDOFINT}, {nDOFINT, nDOFBC} },
   { {nDOFBC , nDOFPDE}, {nDOFBC , nDOFAUX}, {nDOFBC , nDOFINT}, {nDOFBC , nDOFBC} }};

  DLA::VectorD<DLA::VectorD<ArrayQ>> rhs = DLA::VectorD< DLA::DenseVectorSize >({ nDOFPDE, nDOFAUX, nDOFINT, nDOFBC });
  DLA::VectorD<DLA::VectorD<ArrayQ>> adj = DLA::VectorD< DLA::DenseVectorSize >({ nDOFPDE, nDOFAUX, nDOFINT, nDOFBC });

  // Create a dummy right hand side for the adjoint
  for (int i = 0; i < nDOFPDE; i++)
    rhs[0][i] = cos(2*PI*i/nDOFPDE);

  for (int i = 0; i < nDOFAUX; i++)
    rhs[1][i] = 0; // must be zero

  for (int i = 0; i < nDOFINT; i++)
    rhs[2][i] = sin(2*PI*i/nDOFINT);

  for (int i = 0; i < nDOFBC; i++)
    rhs[3][i] = tan(2*PI*i/nDOFBC);

  jacT = 0;

  // jacobian transpose
  auto jacTPDE_q  = Transpose(jacT)(0,0);
  auto jacTPDE_a  = Transpose(jacT)(0,1);
  auto jacTPDE_qI = Transpose(jacT)(0,2);
//auto jacTPDE_lg = Transpose(jacT)(0,3);

  auto jacTAUX_q  = Transpose(jacT)(1,0);
  auto jacTAUX_a  = Transpose(jacT)(1,1);
  auto jacTAUX_qI = Transpose(jacT)(1,2);
//auto jacTAUX_lg = Transpose(jacT)(1,3);

  auto jacTINT_q  = Transpose(jacT)(2,0);
  auto jacTINT_a  = Transpose(jacT)(2,1);
  auto jacTINT_qI = Transpose(jacT)(2,2);
//auto jacTINT_lg = Transpose(jacT)(2,3);

  auto jacTBC_q  = Transpose(jacT)(3,0);
  auto jacTBC_a  = Transpose(jacT)(3,1);
  auto jacTBC_qI = Transpose(jacT)(3,2);
//auto jacTBC_lg = Transpose(jacT)(3,3);


  IntegrateCellGroups<TopoD1>::integrate( JacobianCell_HDG_Test<SurrealClass>(fcnCell, jacTPDE_q, jacTPDE_a,
                                                                                       jacTAUX_q, jacTAUX_a),
                                          xfld, (qfld, afld),
                                          quadOrder.cellOrders.data(),
                                          quadOrder.cellOrders.size() );


  IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate(
      JacobianInteriorTrace_HDG_Test<SurrealClass>(fcnITrace, jacTPDE_q, jacTPDE_a, jacTPDE_qI,
                                                              jacTAUX_q,            jacTAUX_qI,
                                                              jacTINT_q, jacTINT_a, jacTINT_qI),
      xfld, (qfld, afld), qIfld,
      quadOrder.interiorTraceOrders.data(),
      quadOrder.interiorTraceOrders.size() );


  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
      JacobianBoundaryTrace_HDG_Test<SurrealClass>( fcnBTrace, jacTPDE_q, jacTPDE_a, jacTPDE_qI,
                                                               jacTAUX_q, jacTAUX_a, jacTAUX_qI,
                                                               jacTINT_q, jacTINT_a, jacTINT_qI),
      xfld, (qfld, afld), qIfld,
      quadOrder.boundaryTraceOrders.data(),
      quadOrder.boundaryTraceOrders.size() );

  // Compute the adjoint
  adj = DLA::InverseLU::Solve(jacT, rhs);

  // Adjoint fields
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> wfld(xfld, order, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> bfld(xfld, order, BasisFunctionCategory_Legendre);
  Field_DG_Trace<PhysD1, TopoD1, ArrayQ> wIfld(xfld, order, BasisFunctionCategory_Legendre);
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> mufld( xfld, order-1, BasisFunctionCategory_Legendre, {} );

  // Copy the solution from the linear algebra vector to the adjoint field variables
  BOOST_CHECK( nDOFPDE == wfld.nDOF() );
  BOOST_CHECK( nDOFPDE == adj[0].m() );
  for (int k = 0; k < nDOFPDE; k++)
    wfld.DOF(k) = adj[0][k];

  BOOST_CHECK( nDOFINT == wIfld.nDOF() );
  BOOST_CHECK( nDOFINT == adj[2].m() );
  for (int k = 0; k < nDOFINT; k++)
    wIfld.DOF(k) = adj[2][k];

  BOOST_CHECK( nDOFBC == mufld.nDOF() );
  BOOST_CHECK( nDOFBC == adj[3].m() );
  for (int k = 0; k < nDOFBC; k++)
    mufld.DOF(k) = adj[3][k];

  FieldDataMatrixD_BoundaryCell<TensorMatrixQ> invJacAUX_a_bcell(qfld);

  computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, invJacAUX_a_bcell, connectivity,
                                          xfld, qfld, afld, qIfld, quadOrder);

  bfld = 0;
  IntegrateCellGroups<TopoD1>::integrate(
      SetFieldCell_HDG_AuxiliaryAdjoint<SurrealClass>( fcnCell, fcnITrace,
                                                       mmfld, invJacAUX_a_bcell, connectivity,
                                                       xfld, qfld, afld, qIfld, wfld, bfld, wIfld,
                                                       quadOrder.interiorTraceOrders.data(),
                                                       quadOrder.interiorTraceOrders.size() ),
      xfld, (qfld, afld, wfld, bfld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
      SetFieldBoundaryTrace_HDG_AuxiliaryAdjoint<SurrealClass>( fcnBTrace, invJacAUX_a_bcell),
      xfld, (qfld, afld, wfld, bfld), (qIfld, wIfld),
      quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );


  const Real small_tol = 1e-10;
  const Real close_tol = 1e-10;

  for ( int i = 0; i < bfld.nDOF(); i++ )
    for (int d = 0; d < D; d++)
    {
//      std::cout << "[" << i << "][" << d << "] " << bfld.DOF(i)[d] << " == " << adj[1][D*i + d] << std::endl;
      SANS_CHECK_CLOSE( bfld.DOF(i)[d], adj[1][D*i + d], small_tol, close_tol );
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AuxiliaryAdjoint_HDG_AD_Triangle )
{
  const int D = PhysD2::D;

  typedef SurrealS<1> SurrealClass;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template MatrixQ<Real> MatrixQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef NDPDEClass::template TensorMatrixQ<Real> TensorMatrixQ;

  typedef ScalarFunction2D_SineSine SolutionExact;

  typedef BCTypeFunctionLinearRobin_sansLG BCType;
  typedef BCNDConvertSpace<PhysD2, BCAdvectionDiffusion<PhysD2,BCType>> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandCell_HDG<NDPDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<NDPDEClass> IntegrandITraceClass;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, HDG> IntegrandBTraceClass;

  // PDE
  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.348;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real a = 0.38, b = -0.67, c = -0.91;
  Source2D_UniformGrad source(a, b, c);

  NDPDEClass pde( adv, visc, source );

  // BC
  std::shared_ptr<SolutionExact> solnExact( new SolutionExact );

  Real A = 0.64;
  Real B = 0.32;
  BCClass bc( solnExact, visc, A, B );

  // grid: HierarchicalP1 (aka X1)

  int ii = 2;
  int jj = 2;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  XField_CellToTrace<PhysD2, TopoD2> connectivity(xfld);

  // solution
  int order = 2;

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0;

  // Auxiliary variable
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
  afld = 0;

  // Interface variable
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);
  qIfld = 0;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order-1, BasisFunctionCategory_Legendre, {} );
  lgfld = 0;

  const int nDOFPDE = qfld.nDOF();
  const int nDOFAUX = D*afld.nDOF();
  const int nDOFINT = qIfld.nDOF();
  const int nDOFBC = lgfld.nDOF();

  // Compute the field of inverse mass matrices
  FieldDataInvMassMatrix_Cell mmfld(afld);

  // quadrature rule
  QuadratureOrder quadOrder(xfld, 2*order);

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Local, Gradient );

  // integrands
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandITraceClass fcnITrace( pde, disc, {0,1,2} );
  IntegrandBTraceClass fcnBTrace( pde, bc, {0,1,2,3}, disc );

  // linear system setup

  // complete jacobian nonzero pattern without static condensation of auxiliary variables
  //
  //        q   a  qI  lg
  //   PDE  X   X   X   X
  //   AUX  X   X   X   X
  //   IINT X   X   X   0
  //   BC   X   X   0   0

  DLA::MatrixD<DLA::MatrixD<MatrixQ>> jacT =
  {{ {nDOFPDE, nDOFPDE}, {nDOFPDE, nDOFAUX}, {nDOFPDE, nDOFINT}, {nDOFPDE, nDOFBC} },
   { {nDOFAUX, nDOFPDE}, {nDOFAUX, nDOFAUX}, {nDOFAUX, nDOFINT}, {nDOFAUX, nDOFBC} },
   { {nDOFINT, nDOFPDE}, {nDOFINT, nDOFAUX}, {nDOFINT, nDOFINT}, {nDOFINT, nDOFBC} },
   { {nDOFBC , nDOFPDE}, {nDOFBC , nDOFAUX}, {nDOFBC , nDOFINT}, {nDOFBC , nDOFBC} }};

  DLA::VectorD<DLA::VectorD<ArrayQ>> rhs = DLA::VectorD< DLA::DenseVectorSize >({ nDOFPDE, nDOFAUX, nDOFINT, nDOFBC });
  DLA::VectorD<DLA::VectorD<ArrayQ>> adj = DLA::VectorD< DLA::DenseVectorSize >({ nDOFPDE, nDOFAUX, nDOFINT, nDOFBC });

  // Create a dummy right hand side for the adjoint
  for (int i = 0; i < nDOFPDE; i++)
    rhs[0][i] = cos(2*PI*i/nDOFPDE);

  for (int i = 0; i < nDOFAUX; i++)
    rhs[1][i] = 0; // must be zero

  for (int i = 0; i < nDOFINT; i++)
    rhs[2][i] = sin(2*PI*i/nDOFINT);

  for (int i = 0; i < nDOFBC; i++)
    rhs[3][i] = tan(2*PI*i/nDOFBC);

  jacT = 0;

  // jacobian transpose
  auto jacTPDE_q  = Transpose(jacT)(0,0);
  auto jacTPDE_a  = Transpose(jacT)(0,1);
  auto jacTPDE_qI = Transpose(jacT)(0,2);
//auto jacTPDE_lg = Transpose(jacT)(0,3);

  auto jacTAUX_q  = Transpose(jacT)(1,0);
  auto jacTAUX_a  = Transpose(jacT)(1,1);
  auto jacTAUX_qI = Transpose(jacT)(1,2);
//auto jacTAUX_lg = Transpose(jacT)(1,3);

  auto jacTINT_q  = Transpose(jacT)(2,0);
  auto jacTINT_a  = Transpose(jacT)(2,1);
  auto jacTINT_qI = Transpose(jacT)(2,2);
//auto jacTINT_lg = Transpose(jacT)(2,3);

  auto jacTBC_q  = Transpose(jacT)(3,0);
  auto jacTBC_a  = Transpose(jacT)(3,1);
  auto jacTBC_qI = Transpose(jacT)(3,2);
//auto jacTBC_lg = Transpose(jacT)(3,3);


  IntegrateCellGroups<TopoD2>::integrate( JacobianCell_HDG_Test<SurrealClass>(fcnCell, jacTPDE_q, jacTPDE_a,
                                                                                       jacTAUX_q, jacTAUX_a),
                                          xfld, (qfld, afld),
                                          quadOrder.cellOrders.data(),
                                          quadOrder.cellOrders.size() );


  IntegrateInteriorTraceGroups_FieldTrace<TopoD2>::integrate(
      JacobianInteriorTrace_HDG_Test<SurrealClass>(fcnITrace, jacTPDE_q, jacTPDE_a, jacTPDE_qI,
                                                              jacTAUX_q,            jacTAUX_qI,
                                                              jacTINT_q, jacTINT_a, jacTINT_qI),
      xfld, (qfld, afld), qIfld,
      quadOrder.interiorTraceOrders.data(),
      quadOrder.interiorTraceOrders.size() );


  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      JacobianBoundaryTrace_HDG_Test<SurrealClass>( fcnBTrace, jacTPDE_q, jacTPDE_a, jacTPDE_qI,
                                                               jacTAUX_q, jacTAUX_a, jacTAUX_qI,
                                                               jacTINT_q, jacTINT_a, jacTINT_qI),
      xfld, (qfld, afld), qIfld,
      quadOrder.boundaryTraceOrders.data(),
      quadOrder.boundaryTraceOrders.size() );

  // Compute the adjoint
  adj = DLA::InverseLU::Solve(jacT, rhs);

  // Adjoint fields
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, order, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> bfld(xfld, order, BasisFunctionCategory_Legendre);
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> wIfld(xfld, order, BasisFunctionCategory_Legendre);
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> mufld( xfld, order-1, BasisFunctionCategory_Legendre, {} );

  // Copy the solution from the linear algebra vector to the adjoint field variables
  BOOST_CHECK( nDOFPDE == wfld.nDOF() );
  BOOST_CHECK( nDOFPDE == adj[0].m() );
  for (int k = 0; k < nDOFPDE; k++)
    wfld.DOF(k) = adj[0][k];

  BOOST_CHECK( nDOFINT == wIfld.nDOF() );
  BOOST_CHECK( nDOFINT == adj[2].m() );
  for (int k = 0; k < nDOFINT; k++)
    wIfld.DOF(k) = adj[2][k];

  BOOST_CHECK( nDOFBC == mufld.nDOF() );
  BOOST_CHECK( nDOFBC == adj[3].m() );
  for (int k = 0; k < nDOFBC; k++)
    mufld.DOF(k) = adj[3][k];

  FieldDataMatrixD_BoundaryCell<TensorMatrixQ> invJacAUX_a_bcell(qfld);

  computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, invJacAUX_a_bcell, connectivity,
                                          xfld, qfld, afld, qIfld, quadOrder);

  bfld = 0;
  IntegrateCellGroups<TopoD2>::integrate(
      SetFieldCell_HDG_AuxiliaryAdjoint<SurrealClass>( fcnCell, fcnITrace,
                                                       mmfld, invJacAUX_a_bcell, connectivity,
                                                       xfld, qfld, afld, qIfld, wfld, bfld, wIfld,
                                                       quadOrder.interiorTraceOrders.data(),
                                                       quadOrder.interiorTraceOrders.size() ),
      xfld, (qfld, afld, wfld, bfld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      SetFieldBoundaryTrace_HDG_AuxiliaryAdjoint<SurrealClass>( fcnBTrace, invJacAUX_a_bcell),
      xfld, (qfld, afld, wfld, bfld), (qIfld, wIfld),
      quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

  const Real small_tol = 1e-10;
  const Real close_tol = 5e-10;

  for ( int i = 0; i < bfld.nDOF(); i++ )
    for (int d = 0; d < D; d++)
    {
//      std::cout << "[" << i << "][" << d << "] " << bfld.DOF(i)[d] << " == " << adj[1][D*i + d] << std::endl;
      SANS_CHECK_CLOSE( bfld.DOF(i)[d], adj[1][D*i + d], small_tol, close_tol );
    }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
