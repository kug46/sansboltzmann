// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ResidualCell_HDG_HB_AD_btest
// testing of cell residual functions for HDG with harmonic balance for Advection-Diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/HDG/IntegrandFunctor_HDG_HB.h"
#include "Discretization/HDG/ResidualCell_HDG_HB.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"


#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ResidualCell_HDG_HB_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualPDE_1D_HDG_HB_1Line_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_HDG_HB<PDEClass> IntegrandClass;
  typedef Field_DG_Cell<PhysD1, TopoD1, ArrayQ> UField_DG_CellType;
  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single line, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single line, P1 (aka Q1)
  int qorder = 1;
  int ntimes = 3;

  typedef Field<PhysD1, TopoD1, ArrayQ> FieldType;
  typedef std::unique_ptr< FieldType > FieldPtr;
  std::vector< FieldPtr > qflds;

  //create array of HB fields
  for (int j=0; j<ntimes; j++)
  {
    qflds.push_back( FieldPtr(new UField_DG_CellType(xfld, qorder, BasisFunctionCategory_Hierarchical)) );
  }

  // populate solution data
  qflds[0]->DOF(0) = 1;
  qflds[0]->DOF(1) = 4;

  qflds[1]->DOF(0) = 0;
  qflds[1]->DOF(1) = 0;

  qflds[2]->DOF(0) = 2;
  qflds[2]->DOF(1) = 5;

  // HDG discretization
  DiscretizationClass disc( pde, Global, Gradient );

  // quadrature rule: quadratic (aux var defn has linear basis & linear solution)
  int quadratureOrder = -1;
  BOOST_CHECK_EQUAL( -1, quadratureOrder );
  int nDOF = qflds[0]->nDOF();


  BOOST_CHECK( 1 == 1);

  typedef SLA::SparseVector<ArrayQ> SparseVectorClass;
  typedef DLA::VectorD< SparseVectorClass > HBVectorClass;
  //SparseVectorClass rsd(nDOF);
  HBVectorClass rsdHB(ntimes);
  for (int i=0; i<ntimes; i++)
  {

    BOOST_CHECK( 1 == 1);
    rsdHB(i).resize( nDOF ); //set size of individual residual vectors in rsdHB

    BOOST_CHECK( 1 == 1);
    for (int j=0; j<nDOF; j++)
      rsdHB(i)(j) = 0;
  }


  // Time integration parameters
  Real period = 1.;

  // integrand
  IntegrandClass fcnint( pde, disc, period, ntimes );

  for (int i=0; i<ntimes; i++)
    ResidualCell_HDG_HB<TopoD1>::integrate( fcnint, i, qflds, &quadratureOrder, 1, rsdHB(i) );

#if 0 //dump residual
  for (int j=0; j<ntimes; j++)
    for (int k=0; k<2; k++ )
    {
      Real tmp = (*rsdPDEGlobalVec[j])[k];
      cout << "HBrsd: " << tmp << "\n";
    }
#endif

  Real rsdPDETrue[3][2];
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  // PDE residuals: HB
  rsdPDETrue[0][0] = -sqrt(3.)*PI;
  rsdPDETrue[0][1] = -4.*PI/sqrt(3.);
  rsdPDETrue[1][0] = PI/sqrt(3.);
  rsdPDETrue[1][1] = PI/sqrt(3.);
  rsdPDETrue[2][0] = 2.*PI/sqrt(3.);
  rsdPDETrue[2][1] = sqrt(3.)*PI;

  for (int i=0; i<ntimes; i++)
    for (int j=0; j<nDOF; j++)
    {
      SANS_CHECK_CLOSE( rsdPDETrue[i][j], rsdHB(i)[j], small_tol, close_tol );
//      SANS_CHECK_CLOSE( rsdPDETrue[i][j],  rsdPDETrue[i][j], small_tol, close_tol );
//      std::cout << "rsd_" << i << "_" << j << ": " << rsdHB(i)[j] <<"\n";
    }



}

//----------------------------------------------------------------------------//
//BOOST_AUTO_TEST_CASE( ResidualPDE_2D_HDG_HB_1Triangle_X1Q1 )
//{
//  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
//  typedef PDEClass::template ArrayQ<Real> ArrayQ;
//  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
//
//  typedef IntegrandCell_HDG_HB<PDEClass> IntegrandClass;
//
//  Real u = 1;
//  Real v = 0.2;
//  AdvectiveFlux2D_Uniform adv(u, v);
//
//  Real kxx = 2.123;
//  Real kxy = 0.553;
//  Real kyy = 1.007;
//  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);
//
//  PDEClass pde(adv, visc);
//
//  // static tests
//  BOOST_CHECK( pde.D == 2 );
//  BOOST_CHECK( pde.N == 1 );
//
//  // flux components
//  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
//  BOOST_CHECK( pde.hasFluxAdvective() == true );
//  BOOST_CHECK( pde.hasFluxViscous() == true );
//  BOOST_CHECK( pde.hasSource() == false );
//  BOOST_CHECK( pde.hasForcingFunction() == false );
//
//  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );
//
//  // grid: single triangle, P1 (aka X1)
//  XField2D_1Triangle_X1_1Group xfld;
//
//  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
//  BOOST_CHECK_EQUAL( 1, xfld.nElem() );
//
//  // solution: single triangle, P1 (aka Q1)
//  int qorder = 1;
//  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
//
//   // solution data
//  qfld.DOF(0) = 1;
//  qfld.DOF(1) = 3;
//  qfld.DOF(2) = 4;
//
//  int ntimes = 2;
//
//  typedef Field<PhysD2, TopoD2, ArrayQ> FieldType;
//  typedef std::shared_ptr< FieldType > FieldPtr;
//
//  std::vector< FieldPtr > qfldspast;
//
//  for (int j=0; j<ntimes-1; j++)
//  {
//    qfldspast.push_back( FieldPtr(new Field_DG_Cell<PhysD2, TopoD2, ArrayQ>(xfld, qorder, BasisFunctionCategory_Hierarchical)) );
//  }
//
//  // previous solution data
//  qfldspast[0]->DOF(0) = 0;
//  qfldspast[0]->DOF(1) = 0;
//  qfldspast[0]->DOF(2) = 0;
//
//  // auxiliary variable: single triangle, P1 (aka Q1)
//  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);
//
//
//  // HDG discretization (not used)
//  DiscretizationHDG<PDEClass> disc( pde );
//
//  // quadrature rule: quadratic (aux var defn has linear basis & linear solution)
//  int quadratureOrder = 2;
//
//  // PDE residuals: (advective) + (viscous)
//  Real rsdPDE1 = 3./104.;
//  Real rsdPDE2 = 11./312.;
//  Real rsdPDE3 = 1./26.;
//
//  const Real tol = 1e-12;
//  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());
//
//  rsdPDEGlobal = 0;
//
//  // Time integration parameters
//  Real dt = 13.;
//  std::vector<Real> weights {1., -1.}; //HB1 weights
//
//  // integrand
//  IntegrandClass fcnint( pde, disc, dt, weights );
//
//  ResidualCell_HDG_HB<TopoD2>::integrate( fcnint, qfld, afld, qfldspast,
//                                           &quadratureOrder, 1,
//                                           rsdPDEGlobal );
//
//  BOOST_CHECK_CLOSE( rsdPDE1, rsdPDEGlobal[0], tol );
//  BOOST_CHECK_CLOSE( rsdPDE2, rsdPDEGlobal[1], tol );
//  BOOST_CHECK_CLOSE( rsdPDE3, rsdPDEGlobal[2], tol );
//
//}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
