// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandFunctor_HDG_HB_AD_btest
// testing of residual integrands for HDG: Advection-Diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Discretization/HDG/IntegrandFunctor_HDG_HB.h"

#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_None> PDEAdvectionDiffusion1D;
template class IntegrandCell_HDG_HB<PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D>>::Functor<Real, TopoD1, Line>;

typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_None > PDEAdvectionDiffusion2D;
template class IntegrandCell_HDG_HB<PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D>>::Functor<Real, TopoD2, Triangle>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandFunctor_HDG_HB_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IntegrandCellFunctor_HDG_HB_1D_Line_test )
{
    typedef PDEAdvectionDiffusion<PhysD1,
                                  AdvectiveFlux1D_Uniform,
                                  ViscousFlux1D_Uniform,
                                  Source1D_None> PDEAdvectionDiffusion1D;
    typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
    typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

    typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;

    typedef Element<ArrayQ,TopoD1,Line> CellQFieldClass;

    typedef IntegrandCell_HDG_HB<PDEClass> IntegrandClass;
    typedef IntegrandClass::Functor<Real,TopoD1,Line> FunctorClass;
    typedef DiscretizationHDG<PDEClass> DiscretizationClass;

    Real u = 1.1;
    AdvectiveFlux1D_Uniform adv(u);

    Real kxx = 2.123;
    ViscousFlux1D_Uniform visc(kxx);

    Source1D_None source;

    GlobalTime time(0);

    PDEClass pde( time, adv, visc, source );

    // static tests
    BOOST_CHECK( pde.D == 1 );
    BOOST_CHECK( pde.N == 1 );

    // flux components
    BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
    BOOST_CHECK( pde.hasFluxAdvective() == true );
    BOOST_CHECK( pde.hasFluxViscous() == true );
    BOOST_CHECK( pde.hasSource() == false );
    BOOST_CHECK( pde.hasForcingFunction() == false );

    BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

    // grid

    int order = 1;
    ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( 1, xfldElem.order() );
    BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

    // line grid
    Real x1, x2;

    x1 = 0;
    x2 = 1;

    xfldElem.DOF(0) = {x1};
    xfldElem.DOF(1) = {x2};

    // timestep
    Real period = 1.;
    int ntimes = 3;

    // solution at two timesteps

    CellQFieldClass qfldElem1(order, BasisFunctionCategory_Hierarchical); // current timestep
    CellQFieldClass qfldElem2(order, BasisFunctionCategory_Hierarchical); // previous timestep
    CellQFieldClass qfldElem3(order, BasisFunctionCategory_Hierarchical); // previous timestep

    int nDOF = qfldElem3.nDOF();

    BOOST_CHECK_EQUAL( 1, qfldElem1.order() );
    BOOST_CHECK_EQUAL( 2, qfldElem2.nDOF() );
    BOOST_CHECK_EQUAL( 2, qfldElem3.nDOF() );

//    Real test;
//    int n=1;
//    int j=2;
//    test = (PI / period) * ( pow(-1, n-j) / sin( PI*(n-j)/ntimes ) );
//    cout << test << "\n";

//
//    // HDG discretization (not used)
    DiscretizationClass disc( pde, Global, Gradient );
//
//    // integrand
    IntegrandClass fcnint( pde, disc, period, ntimes );
//
//    //////////////////////////////////
//    // Case 1: du/dt = 0; Set previous timestep equal to current timestep
//    //////////////////////////////////

    // line solutions
    qfldElem1.DOF(0) = 1;
    qfldElem1.DOF(1) = 4;

    qfldElem2.DOF(0) = 0;
    qfldElem2.DOF(1) = 0;

    qfldElem3.DOF(0) = 2;
    qfldElem3.DOF(1) = 5;
//
    std::vector <CellQFieldClass> qfldElemVec;
    qfldElemVec.push_back(qfldElem1);
    qfldElemVec.push_back(qfldElem2);
    qfldElemVec.push_back(qfldElem3);


    const Real small_tol = 1e-13;
    const Real close_tol = 1e-12;

    //SANS_CHECK_CLOSE( qfldElemVec[2].DOF(1), 4., small_tol, close_tol );

    Real sRef;
    Real integrandPDETrue[3][2];
    FunctorClass::IntegrandType integrand[3][2];


//
//    // integrand functor
    for (int i=0; i<ntimes; i++)
    {
      for (int k=0; k<nDOF; k++)
          integrand[i][k] = 0;

      FunctorClass fcn = fcnint.integrand( xfldElem, qfldElemVec, i);

      BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
      BOOST_CHECK_EQUAL( 2, fcn.nDOF() );
      BOOST_CHECK( fcn.needsEvaluation() == true );

      // x=0
      sRef = 0;
      fcn( {sRef}, integrand[i], 2 );
    }

    integrandPDETrue[0][0] = -4.*PI / (sqrt(3.));
    integrandPDETrue[0][1] = 0;
    integrandPDETrue[1][0] = 2.*PI / sqrt(3.);
    integrandPDETrue[1][1] = 0;
    integrandPDETrue[2][0] = 2.*PI / sqrt(3.);
    integrandPDETrue[2][1] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0][0], integrand[0][0].PDE, small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[0][1], integrand[0][1].PDE, small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1][0], integrand[1][0].PDE, small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1][1], integrand[1][1].PDE, small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2][0], integrand[2][0].PDE, small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2][1], integrand[2][1].PDE, small_tol, close_tol );

    // x=1
    sRef = 1;
    for (int i=0; i<ntimes; i++)
    {
      for (int k=0; k<nDOF; k++)
        integrand[i][k] = 0;

      FunctorClass fcn = fcnint.integrand( xfldElem, qfldElemVec, i);

      BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
      BOOST_CHECK_EQUAL( 2, fcn.nDOF() );
      BOOST_CHECK( fcn.needsEvaluation() == true );

      // x=0
      fcn( {sRef}, integrand[i], 2 );
    }

    integrandPDETrue[0][0] = 0;
    integrandPDETrue[0][1] = -10.*PI / sqrt(3.);
    integrandPDETrue[1][0] = 0;
    integrandPDETrue[1][1] = 2.*PI/sqrt(3.);
    integrandPDETrue[2][0] = 0;
    integrandPDETrue[2][1] = 8.*PI/sqrt(3.);

    SANS_CHECK_CLOSE( integrandPDETrue[0][0], integrand[0][0].PDE, small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[0][1], integrand[0][1].PDE, small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1][0], integrand[1][0].PDE, small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1][1], integrand[1][1].PDE, small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2][0], integrand[2][0].PDE, small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2][1], integrand[2][1].PDE, small_tol, close_tol );

    // x=0.5
    sRef = 1./2.;
    for (int i=0; i<ntimes; i++)
    {
      for (int k=0; k<nDOF; k++)
              integrand[i][k] = 0;

      FunctorClass fcn = fcnint.integrand( xfldElem, qfldElemVec, i);

      BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
      BOOST_CHECK_EQUAL( 2, fcn.nDOF() );
      BOOST_CHECK( fcn.needsEvaluation() == true );

      // x=0
      fcn( {sRef}, integrand[i], 2 );
    }

    integrandPDETrue[0][0] = -7.*PI/(2*sqrt(3.));
    integrandPDETrue[0][1] = -7.*PI/(2*sqrt(3.));
    integrandPDETrue[1][0] = PI / sqrt(3.);
    integrandPDETrue[1][1] = PI / sqrt(3.);
    integrandPDETrue[2][0] = 5.*PI / (2.*sqrt(3.));
    integrandPDETrue[2][1] = 5.*PI / (2.*sqrt(3.));

    SANS_CHECK_CLOSE( integrandPDETrue[0][0], integrand[0][0].PDE, small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[0][1], integrand[0][1].PDE, small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1][0], integrand[1][0].PDE, small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1][1], integrand[1][1].PDE, small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2][0], integrand[2][0].PDE, small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2][1], integrand[2][1].PDE, small_tol, close_tol );

    //////////////////////////////////
    // Case 2: HB1 for non-zero nonzero du/dt; Change solution at t=n-1;
    //////////////////////////////////
//
//    qfldElem1.DOF(0) = 0;
//    qfldElem1.DOF(1) = 0;
//    qfldElemVec[0] = qfldElem1;
//    BOOST_CHECK_EQUAL(qfldElemVec.size(), 2); //to get around cppcheck
//
//    //x=0
//    sRef = 0;
//    fcn( {sRef}, integrand, 4 );
//
//    integrandPDETrue[0] = 1./13.;
//    integrandPDETrue[1] = 0;
//
//    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1].PDE, small_tol, close_tol );
//
//    //x=1
//    sRef = 1;
//    fcn( sRef, integrand, 4 );
//
//    integrandPDETrue[0] = 0;
//    integrandPDETrue[1] = 4./13.;
//
//    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1].PDE, small_tol, close_tol );
//
//    //x=0.5
//    sRef = 1./3.;
//    fcn( sRef, integrand, 4 );
//
//    integrandPDETrue[0] = 4./39.;
//    integrandPDETrue[1] = 2./39.;
//
//    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1].PDE, small_tol, close_tol );
//

}


//----------------------------------------------------------------------------//
//BOOST_AUTO_TEST_CASE( IntegrandCellFunctor_HDG_HB_2D_Triangle_test )
//{
//    typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
//    typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
//
//    typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
//
//    typedef Element<ArrayQ,TopoD2,Triangle> CellQFieldClass;
//    typedef Element<DLA::VectorS<2,ArrayQ>,TopoD2,Triangle> ElementAFieldClass;
//
//    typedef IntegrandCell_HDG_HB<PDEClass> IntegrandClass;
//    typedef IntegrandClass::Functor<Real,TopoD2,Triangle> FunctorClass;
//    typedef DiscretizationHDG<PDEClass> DiscretizationClass;
//
//    Real u = 1;
//    Real v = 0.2;
//    AdvectiveFlux2D_Uniform adv(u, v);
//
//    Real kxx = 2.123;
//    Real kxy = 0.553;
//    Real kyy = 1.007;
//    ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);
//
//    GlobalTime time(0);
//    PDEClass pde(time, adv, visc);
//
//    // static tests
//    BOOST_CHECK( pde.D == 2 );
//    BOOST_CHECK( pde.N == 1 );
//
//    // flux components
//    BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
//    BOOST_CHECK( pde.hasFluxAdvective() == true );
//    BOOST_CHECK( pde.hasFluxViscous() == true );
//    BOOST_CHECK( pde.hasSource() == false );
//    BOOST_CHECK( pde.hasForcingFunction() == false );
//
//    BOOST_CHECK( pde.needsSolutionGradientforSource() == false );
//
//    // grid
//
//    int order = 1;
//    ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);
//
//    BOOST_CHECK_EQUAL( 1, xfldElem.order() );
//    BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );
//
//    // timestep + HB1 weights
//    Real dt = 13;
//    std::vector <Real> weights = {1., -1.};
//
//    // triangle grid
//    Real x1, x2, x3, y1, y2, y3;
//
//    x1 = 0;  y1 = 0;
//    x2 = 1;  y2 = 0;
//    x3 = 0;  y3 = 1;
//
//    xfldElem.DOF(0) = {x1, y1};
//    xfldElem.DOF(1) = {x2, y2};
//    xfldElem.DOF(2) = {x3, y3};
//
//    // solution
//
//    CellQFieldClass qfldElem_t0(order, BasisFunctionCategory_Hierarchical); // current timestep
//    CellQFieldClass qfldElem_tm1(order, BasisFunctionCategory_Hierarchical); // previous timestep
//
//    BOOST_CHECK_EQUAL( 1, qfldElem_t0.order() );
//    BOOST_CHECK_EQUAL( 3, qfldElem_t0.nDOF() );
//
//    // triangle solution
//    qfldElem_t0.DOF(0) = 1;
//    qfldElem_t0.DOF(1) = 3;
//    qfldElem_t0.DOF(2) = 4;
//
//    // HDG discretization (not used)
//    DiscretizationClass disc( pde );
//
//    // integrand
//    IntegrandClass fcnint( pde, disc, dt, weights  );
//
//    //////////////////////////////////
//    // Case 1: HB1, du/dt = 0; Set previous timestep equal to current timestep
//    //////////////////////////////////
//    qfldElem_tm1 = qfldElem_t0;
//
//    std::vector <CellQFieldClass> qfldElemVec {qfldElem_tm1};
//
//    // integrand functor
//    FunctorClass fcn = fcnint.integrand(xfldElem, qfldElem_t0, qfldElemVec);
//
//    BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
//    BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
//    BOOST_CHECK( fcn.needsEvaluation() == true );
//
//    const Real small_tol = 1e-13;
//    const Real close_tol = 1e-12;
//    Real sRef, tRef;
//    Real integrandPDETrue[3];
//    FunctorClass::IntegrandType integrand[3];
//
//    //sref =0; tref = 0;
//    sRef = 0;  tRef = 0;
//    fcn( {sRef, tRef}, integrand, 3 );
//
//    integrandPDETrue[0] = 0;
//    integrandPDETrue[1] = 0;
//    integrandPDETrue[2] = 0;
//
//    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2].PDE, small_tol, close_tol );
//
//    //sref =1; tref = 0;
//    sRef = 1;  tRef = 0;
//    fcn( {sRef, tRef}, integrand, 3 );
//
//    integrandPDETrue[0] = 0;
//    integrandPDETrue[1] = 0;
//    integrandPDETrue[2] = 0;
//
//    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2].PDE, small_tol, close_tol );
//
//    //sref =0; tref = 1;
//    sRef = 0;  tRef = 1;
//    fcn( {sRef, tRef}, integrand, 3 );
//
//    integrandPDETrue[0] = 0;
//    integrandPDETrue[1] = 0;
//    integrandPDETrue[2] = 0;
//
//    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2].PDE, small_tol, close_tol );
//
//    //sref = 0.5; tref = 0.5;
//    sRef = 0.5;  tRef = 0.5;
//    fcn( {sRef, tRef}, integrand, 3 );
//
//    integrandPDETrue[0] = 0;
//    integrandPDETrue[1] = 0;
//    integrandPDETrue[2] = 0;
//
//    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2].PDE, small_tol, close_tol );
//
//    //////////////////////////////////
//    // Case 2: HB1, du/dt nonzero; Set previous timestep equal to zero
//    //////////////////////////////////
//    qfldElem_tm1.DOF(0) = 0;
//    qfldElem_tm1.DOF(1) = 0;
//    qfldElem_tm1.DOF(2) = 0;
//
//    qfldElemVec[0] = qfldElem_tm1;
//    BOOST_CHECK_EQUAL(qfldElemVec.size(), 1);
//
//    //sref =0; tref = 0;
//    sRef = 0;  tRef = 0;
//    fcn( {sRef, tRef}, integrand, 3 );
//
//    integrandPDETrue[0] = 1./13;
//    integrandPDETrue[1] = 0;
//    integrandPDETrue[2] = 0;
//
//    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2].PDE, small_tol, close_tol );
//
//    //sref =1; tref = 0;
//    sRef = 1;  tRef = 0;
//    fcn( {sRef, tRef}, integrand, 3 );
//
//    integrandPDETrue[0] = 0;
//    integrandPDETrue[1] = 3./13;
//    integrandPDETrue[2] = 0;
//
//    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2].PDE, small_tol, close_tol );
//
//    //sref =0; tref = 1;
//    sRef = 0;  tRef = 1;
//    fcn( {sRef, tRef}, integrand, 3 );
//
//    integrandPDETrue[0] = 0;
//    integrandPDETrue[1] = 0;
//    integrandPDETrue[2] = 4./13;
//
//    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2].PDE, small_tol, close_tol );
//
//    //sref = 0.5; tref = 0.5;
//    sRef = 1./3.;  tRef = 2./3.;
//    fcn( {sRef, tRef}, integrand, 3 );
//
//    integrandPDETrue[0] = 0;
//    integrandPDETrue[1] = 11./117;
//    integrandPDETrue[2] = 22./117;
//
//    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2].PDE, small_tol, close_tol );
//
//    //////////////////////////////////
//    // Case 3: HB2, du/dt nonzero; constant slope
//    //////////////////////////////////
//    std::vector<Real> weights2 = {1.5, -2., 0.5};
//    BOOST_CHECK_EQUAL(weights2.size(), 3); //to get around cppcheck
//
//    IntegrandClass fcnint2( pde, disc, dt, weights2 );
//
//    CellQFieldClass qfldElem_tm2(order, BasisFunctionCategory_Hierarchical); // previous timestep
//    qfldElem_tm2.DOF(0) = -1./3.;
//    qfldElem_tm2.DOF(1) = -4./9.;
//    qfldElem_tm2.DOF(2) = -5.;
//
//    std::vector <CellQFieldClass> qfldElemVec2 {qfldElem_tm1, qfldElem_tm2};
//    BOOST_CHECK_EQUAL(qfldElemVec2.size(), 2); //to get around cppcheck
//
//    // integrand functor
//    FunctorClass fcn2 = fcnint2.integrand( xfldElem, qfldElem_t0, qfldElemVec2);
//
//    //sref =0; tref = 0;
//    sRef = 0;  tRef = 0;
//    fcn2( {sRef, tRef}, integrand, 3 );
//
//    integrandPDETrue[0] = 4./39;
//    integrandPDETrue[1] = 0;
//    integrandPDETrue[2] = 0;
//
//    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2].PDE, small_tol, close_tol );
//
//    //sref =1; tref = 0;
//    sRef = 1;  tRef = 0;
//    fcn2( {sRef, tRef}, integrand, 3 );
//
//    integrandPDETrue[0] = 0;
//    integrandPDETrue[1] = 77./234;
//    integrandPDETrue[2] = 0;
//
//    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2].PDE, small_tol, close_tol );
//
//    //sref =0; tref = 1;
//    sRef = 0;  tRef = 1;
//    fcn2( {sRef, tRef}, integrand, 3 );
//
//    integrandPDETrue[0] = 0;
//    integrandPDETrue[1] = 0;
//    integrandPDETrue[2] = 7./26;
//
//    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2].PDE, small_tol, close_tol );
//
//    //sref = 0.5; tref = 0.5;
//    sRef = 1./3;  tRef = 2./3;
//    fcn2( {sRef, tRef}, integrand, 3 );
//
//    integrandPDETrue[0] = 0;
//    integrandPDETrue[1] = 203./2106;
//    integrandPDETrue[2] = 203./1053;
//
//    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1].PDE, small_tol, close_tol );
//    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2].PDE, small_tol, close_tol );
//
//}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
