// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// FunctionalCell_HDG_btest
// testing of 2-D functional area-integral with HDG

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/list.hpp>

#include "tools/SANSnumerics.h"     // Real

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "pde/OutputCell_WeightedSolution.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/HDG/FunctionalCell_HDG.h"
#include "Discretization/HDG/IntegrandCell_HDG_Output.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( FunctionalCell_HDG_WeightedSolution_test_suite )

template<class PhysDim_>
struct DummyPDEReal
{
  typedef PhysDim_ PhysDim;

  template<class T>
  using ArrayQ = T;

  template<class T>
  using MatrixQ = T;
};

template<class PhysDim_>
struct DummyPDEVector
{
  typedef PhysDim_ PhysDim;

  template<class T>
  using ArrayQ = DLA::VectorS<2,T>;

  template<class T>
  using MatrixQ = DLA::MatrixS<2,2,T>;
};


typedef boost::mpl::list< DummyPDEReal<PhysD1>,
                          DummyPDEVector<PhysD1> >  DummyPDEs_PhysD1;

typedef boost::mpl::list< DummyPDEReal<PhysD2>,
                          DummyPDEVector<PhysD2> >  DummyPDEs_PhysD2;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Line_P0_Const_test, DummyPDE, DummyPDEs_PhysD1 )
{
  typedef ScalarFunction1D_Const WeightFcn; // Change this to change weighting type
  typedef OutputCell_WeightedSolution<DummyPDE,WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDOutputClass::template ArrayJ<Real> ArrayJ;

  typedef IntegrandCell_HDG_Output<NDOutputClass> IntegrandClass;

  Real a0 = 1.4;
  WeightFcn weightFcn(a0);

  // grid: single triangle, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P0 (aka Q0)
  int qorder = 0;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
  BOOST_CHECK_EQUAL( 1, qfld.nDOF() );

  // solution data
  qfld.DOF(0) = 1.0;

  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Legendre);

  afld.DOF(0) = -1;

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder = 1;

  // integrand
  NDOutputClass fcnOutput(weightFcn);
  IntegrandClass integrand( fcnOutput, {0} );

  ArrayJ functional = 0;
  IntegrateCellGroups<TopoD1>::integrate(
      FunctionalCell_HDG( integrand, functional ), xfld, (qfld, afld), &quadratureOrder, 1 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int m = 0; m < DLA::VectorSize<ArrayJ>::M; m++)
    SANS_CHECK_CLOSE( a0, DLA::index(functional,m), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Line_P1_Const_test, DummyPDE, DummyPDEs_PhysD1 )
{
  typedef ScalarFunction1D_Const WeightFcn; // Change this to change weighting type
  typedef OutputCell_WeightedSolution<DummyPDE,WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDOutputClass::template ArrayJ<Real> ArrayJ;

  typedef IntegrandCell_HDG_Output<NDOutputClass> IntegrandClass;

  Real a0=1.4;
  WeightFcn weightFcn(a0);

  // grid: single triangle, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( 2, qfld.nDOF() );

  // solution data
  qfld.DOF(0) = 1.0; // phi_0 = 1-x;
  qfld.DOF(1) = 2.0; // phi_1 = x;

  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  afld.DOF(0) = -1;  afld.DOF(1) = 5;

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder = 1;

  // integrand
  NDOutputClass fcnOutput(weightFcn);
  IntegrandClass integrand( fcnOutput, {0} );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  ArrayJ functional = 0;
  IntegrateCellGroups<TopoD1>::integrate(
      FunctionalCell_HDG( integrand, functional ), xfld, (qfld, afld), &quadratureOrder, 1 );

  for (int m = 0; m < DLA::VectorSize<ArrayJ>::M; m++)
    SANS_CHECK_CLOSE( a0*(3./2), DLA::index(functional,m), small_tol, close_tol );
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Triangle_P0_Linear_test, DummyPDE, DummyPDEs_PhysD2 )
{
  typedef ScalarFunction2D_Linear WeightFcn; // Change this to change weighting type
  typedef OutputCell_WeightedSolution<DummyPDE,WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDOutputClass::template ArrayJ<Real> ArrayJ;

  typedef IntegrandCell_HDG_Output<NDOutputClass> IntegrandClass;

  Real a0=1.4,a1=0.0,a2=0.0;
  WeightFcn weightFcn(a0,a1,a2);

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P0 (aka Q0)
  int qorder = 0;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 1, qfld.nDOF() );

  // solution data
  qfld.DOF(0) = 1.0;

  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Legendre);

  afld.DOF(0) = -1;

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder = 1;

  // integrand
  NDOutputClass fcnOutput(weightFcn);
  IntegrandClass integrand( fcnOutput, {0} );

  ArrayJ functional = 0;
  IntegrateCellGroups<TopoD2>::integrate(
      FunctionalCell_HDG( integrand, functional ), xfld, (qfld, afld), &quadratureOrder, 1 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int m = 0; m < DLA::VectorSize<ArrayJ>::M; m++)
    SANS_CHECK_CLOSE( a0/2, DLA::index(functional,m), small_tol, close_tol );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Triangle_P1_Linear_test, DummyPDE, DummyPDEs_PhysD2 )
{
  typedef ScalarFunction2D_Linear WeightFcn; // Change this to change weighting type
  typedef OutputCell_WeightedSolution<DummyPDE,WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDOutputClass::template ArrayJ<Real> ArrayJ;

  typedef IntegrandCell_HDG_Output<NDOutputClass> IntegrandClass;

  Real a0=1.4,a1=0.0,a2=0.0;
  WeightFcn weightFcn(a0,a1,a2);

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 3, qfld.nDOF() );

  // solution data
  qfld.DOF(0) = 1.0;
  qfld.DOF(1) = 2.0;
  qfld.DOF(2) = 3.0;

  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Legendre);

  // afld = 2.0;
  afld.DOF(0) = -1;  afld.DOF(1) = 5;  afld.DOF(2) = 2;

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder = 2;

  // integrand
  NDOutputClass fcnOutput(weightFcn);
  IntegrandClass integrand( fcnOutput, {0} );

  ArrayJ functional = 0;
  IntegrateCellGroups<TopoD2>::integrate(
      FunctionalCell_HDG( integrand, functional ), xfld, (qfld, afld), &quadratureOrder, 1 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int m = 0; m < DLA::VectorSize<ArrayJ>::M; m++)
    SANS_CHECK_CLOSE( a0/2, DLA::index(functional,m), small_tol, close_tol );
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
