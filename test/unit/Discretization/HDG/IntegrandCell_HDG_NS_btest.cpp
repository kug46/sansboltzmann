// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_HDG_AD_btest
// testing of cell residual integrands for HDG: Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementArea.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"

#include "Discretization/HDG/IntegrandCell_HDG.h"

using namespace std;
using namespace SANS;

namespace SANS
{
typedef QTypePrimitiveRhoPressure QType;
typedef ViscosityModel_Const ViscosityModelType;
typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
typedef PDENDConvertSpace<PhysD2, PDEClass> PDEClass2D;

typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldTri;
template class IntegrandCell_HDG<PDEClass2D>::BasisWeighted_PDE<Real,Real,TopoD2,Triangle,ElementXFieldTri>;
template class IntegrandCell_HDG<PDEClass2D>::BasisWeighted_AUX<Real,Real,TopoD2,Triangle>;
template class IntegrandCell_HDG<PDEClass2D>::FieldWeighted<Real,TopoD2,Triangle,ElementXFieldTri>;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_HDG_NS_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_2D_Triangle_test )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;

  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementAFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_HDG<NDPDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,Real,TopoD2,Triangle,ElementParam> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_AUX<Real,Real,TopoD2,Triangle> BasisWeightedAUXClass;

  typedef DiscretizationHDG<NDPDEClass> DiscretizationClass;
  // PDE
  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)

//  const Real muRef = 0.0;      // kg/(m s) // FOR RE = 10
  const Real muRef = 0.1;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef); //single argument for constant viscosity
  ThermalConductivityModel tcond(Prandtl, Cp);

  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 4 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  // solution

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = {967./1000., 1./4., 32./100., 107./100.};
  qfldElem.DOF(1) = {103./100., 23./100., 32./100., 108./100.};
  qfldElem.DOF(2) = {11./10., 21./100., 36./100., 105./100.};

  // auxiliary variable

  ElementAFieldClass afldElem(order, BasisFunctionCategory_Hierarchical);

  afldElem.DOF(0)(0) = { 8./100., -24./1000., -8./100., -23./1000.};
  afldElem.DOF(1)(0) = {6./100., 14./1000., 6./100., 23./1000.};
  afldElem.DOF(2)(0) = {-5./100., -13./1000., -33./1000., 34./1000.};

  afldElem.DOF(0)(1) = {7./100., 8./100., 12./100., 56./1000.};
  afldElem.DOF(1)(1) = {-2./100., 8./1000., 5./100., 17./1000.};
  afldElem.DOF(2)(1) = {2./100., 22./1000., 23./1000., 8./100.};

  // HDG discretization (not used)
  DiscretizationClass disc( pde, Local, Gradient );

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  BasisWeightedPDEClass fcnPDE = fcnint.integrand_PDE(xfldElem, qfldElem, afldElem );
  BasisWeightedAUXClass fcnAUX = fcnint.integrand_AUX(xfldElem, qfldElem, afldElem );

  BOOST_CHECK_EQUAL( 4, fcnPDE.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcnPDE.nDOF() );
  BOOST_CHECK( fcnPDE.needsEvaluation() == true );
  BOOST_CHECK_EQUAL( 4, fcnAUX.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcnAUX.nDOF() );
  BOOST_CHECK( fcnAUX.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  Real sRef, tRef;
  Real integrandPDETrue[3][4], integrandAuxTrue[3][4], integrandAuyTrue[3][4];
  BasisWeightedPDEClass::IntegrandType integrandPDE[3];
  BasisWeightedAUXClass::IntegrandType integrandAUX[3];

  sRef = 0;  tRef = 0;
  fcnPDE( {sRef, tRef}, integrandPDE, 3 );
  fcnAUX( {sRef, tRef}, integrandAUX, 3 );

  // PDE residuals: (advective) + (viscous)
  integrandPDETrue[0][0] = ( 55119./100000. ) + ( 0 );
  integrandPDETrue[0][1] = ( 483119./400000. ) + ( 7./625. );
  integrandPDETrue[0][2] = ( 194747./156250. ) + ( -11./625. );
  integrandPDETrue[0][3] = ( 4360191231./2000000000. ) + ( 22447638607./350658375000. );
  integrandPDETrue[1][0] = ( -967./4000. ) + ( 0 );
  integrandPDETrue[1][1] = ( -18087./16000. ) + ( -7./625. );
  integrandPDETrue[1][2] = ( -967./12500. ) + ( 0 );
  integrandPDETrue[1][3] = ( -76494583./80000000. ) + ( -825614363./14026335000. );
  integrandPDETrue[2][0] = ( -967./3125. ) + ( 0 );
  integrandPDETrue[2][1] = ( -967./12500. ) + ( 0 );
  integrandPDETrue[2][2] = ( -365319./312500. ) + ( 11./625. );
  integrandPDETrue[2][3] = ( -76494583./62500000. ) + ( -451819883./87664593750. );

  integrandAuxTrue[0][0] = ( -887./1000. );
  integrandAuxTrue[0][1] = ( -137./500. );
  integrandAuxTrue[0][2] = ( -2./5. );
  integrandAuxTrue[0][3] = ( -1093./1000. );
  integrandAuxTrue[1][0] = ( 967./1000. );
  integrandAuxTrue[1][1] = ( 1./4. );
  integrandAuxTrue[1][2] = ( 8./25. );
  integrandAuxTrue[1][3] = ( 107./100. );
  integrandAuxTrue[2][0] = ( 0 );
  integrandAuxTrue[2][1] = ( 0 );
  integrandAuxTrue[2][2] = ( 0 );
  integrandAuxTrue[2][3] = ( 0 );

  integrandAuyTrue[0][0] = ( -897./1000. );
  integrandAuyTrue[0][1] = ( -17./100. );
  integrandAuyTrue[0][2] = ( -1./5. );
  integrandAuyTrue[0][3] = ( -507./500. );
  integrandAuyTrue[1][0] = ( 0 );
  integrandAuyTrue[1][1] = ( 0 );
  integrandAuyTrue[1][2] = ( 0 );
  integrandAuyTrue[1][3] = ( 0 );
  integrandAuyTrue[2][0] = ( 967./1000. );
  integrandAuyTrue[2][1] = ( 1./4. );
  integrandAuyTrue[2][2] = ( 8./25. );
  integrandAuyTrue[2][3] = ( 107./100. );



//  integrandAuyTrue[2] = 0;

  for (int i=0; i< fcnPDE.nDOF(); i++)
      for (int j=0; j< fcnPDE.nEqn(); j++)
      {
        SANS_CHECK_CLOSE( integrandPDETrue[i][j], integrandPDE[i][j], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandAuxTrue[i][j], integrandAUX[i][0][j], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandAuyTrue[i][j], integrandAUX[i][1][j], small_tol, close_tol );
      }

////////
  sRef = 1;  tRef = 0;
  fcnPDE( {sRef, tRef}, integrandPDE, 3 );
  fcnAUX( {sRef, tRef}, integrandAUX, 3 );

  //PDE residual integrands: (advective) + (viscous)
  integrandPDETrue[0][0] = ( 1133./2000. ) + ( 0 );
  integrandPDETrue[0][1] = ( 242059./200000. ) + ( -2./375. );
  integrandPDETrue[0][2] = ( 7883./6250. ) + ( -47./3750. );
  integrandPDETrue[0][3] = ( 84919549./40000000. ) + ( -6446341./1491890625. );
  integrandPDETrue[1][0] = ( -2369./10000. ) + ( 0 );
  integrandPDETrue[1][1] = ( -1134487./1000000. ) + ( -11./7500. );
  integrandPDETrue[1][2] = ( -2369./31250. ) + ( 17./2500. );
  integrandPDETrue[1][3] = ( -177559057./200000000. ) + ( -50718899./2983781250. );
  integrandPDETrue[2][0] = ( -206./625. ) + ( 0 );
  integrandPDETrue[2][1] = ( -2369./31250. ) + ( 17./2500. );
  integrandPDETrue[2][2] = ( -18523./15625. ) + ( 43./7500. );
  integrandPDETrue[2][3] = ( -7719959./6250000. ) + ( 63611581./2983781250. );

  integrandAuxTrue[0][0] = ( -103./100. );
  integrandAuxTrue[0][1] = ( -23./100. );
  integrandAuxTrue[0][2] = ( -8./25. );
  integrandAuxTrue[0][3] = ( -27./25. );
  integrandAuxTrue[1][0] = ( 109./100. );
  integrandAuxTrue[1][1] = ( 61./250. );
  integrandAuxTrue[1][2] = ( 19./50. );
  integrandAuxTrue[1][3] = ( 1103./1000. );
  integrandAuxTrue[2][0] = ( 0 );
  integrandAuxTrue[2][1] = ( 0 );
  integrandAuxTrue[2][2] = ( 0 );
  integrandAuxTrue[2][3] = ( 0 );

  integrandAuyTrue[0][0] = ( -103./100. );
  integrandAuyTrue[0][1] = ( -23./100. );
  integrandAuyTrue[0][2] = ( -8./25. );
  integrandAuyTrue[0][3] = ( -27./25. );
  integrandAuyTrue[1][0] = ( -1./50. );
  integrandAuyTrue[1][1] = ( 1./125. );
  integrandAuyTrue[1][2] = ( 1./20. );
  integrandAuyTrue[1][3] = ( 17./1000. );
  integrandAuyTrue[2][0] = ( 103./100. );
  integrandAuyTrue[2][1] = ( 23./100. );
  integrandAuyTrue[2][2] = ( 8./25. );
  integrandAuyTrue[2][3] = ( 27./25. );



  for (int i=0; i< fcnPDE.nDOF(); i++)
    for (int j=0; j< fcnPDE.nEqn(); j++)
    {
      SANS_CHECK_CLOSE( integrandPDETrue[i][j], integrandPDE[i][j], small_tol, close_tol );
      SANS_CHECK_CLOSE( integrandAuxTrue[i][j], integrandAUX[i][0][j], small_tol, close_tol );
      SANS_CHECK_CLOSE( integrandAuyTrue[i][j], integrandAUX[i][1][j], small_tol, close_tol );
    }

  sRef = 0;  tRef = 1;
  fcnPDE( {sRef, tRef}, integrandPDE, 3 );
  fcnAUX( {sRef, tRef}, integrandAUX, 3 );

  //PDE residual integrands: (advective) + (viscous)
  integrandPDETrue[0][0] = ( 627./1000. ) + ( 0 );
  integrandPDETrue[0][1] = ( 118167./100000. ) + ( 131./30000. );
  integrandPDETrue[0][2] = ( 31893./25000. ) + ( -17./6000. );
  integrandPDETrue[0][3] = ( 42984099./20000000. ) + ( -22918639./363000000. );
  integrandPDETrue[1][0] = ( -231./1000. ) + ( 0 );
  integrandPDETrue[1][1] = ( -109851./100000. ) + ( -49./15000. );
  integrandPDETrue[1][2] = ( -2079./25000. ) + ( -11./10000. );
  integrandPDETrue[1][3] = ( -15836247./20000000. ) + ( 4769119./136125000. );
  integrandPDETrue[2][0] = ( -99./250. ) + ( 0 );
  integrandPDETrue[2][1] = ( -2079./25000. ) + ( -11./10000. );
  integrandPDETrue[2][2] = ( -14907./12500. ) + ( 59./15000. );
  integrandPDETrue[2][3] = ( -6786963./5000000. ) + ( 6120593./217800000. );

  integrandAuxTrue[0][0] = ( -11./10. );
  integrandAuxTrue[0][1] = ( -21./100. );
  integrandAuxTrue[0][2] = ( -9./25. );
  integrandAuxTrue[0][3] = ( -21./20. );
  integrandAuxTrue[1][0] = ( 11./10. );
  integrandAuxTrue[1][1] = ( 21./100. );
  integrandAuxTrue[1][2] = ( 9./25. );
  integrandAuxTrue[1][3] = ( 21./20. );
  integrandAuxTrue[2][0] = ( -1./20. );
  integrandAuxTrue[2][1] = ( -13./1000. );
  integrandAuxTrue[2][2] = ( -33./1000. );
  integrandAuxTrue[2][3] = ( 17./500. );

  integrandAuyTrue[0][0] = ( -11./10. );
  integrandAuyTrue[0][1] = ( -21./100. );
  integrandAuyTrue[0][2] = ( -9./25. );
  integrandAuyTrue[0][3] = ( -21./20. );
  integrandAuyTrue[1][0] = ( 0 );
  integrandAuyTrue[1][1] = ( 0 );
  integrandAuyTrue[1][2] = ( 0 );
  integrandAuyTrue[1][3] = ( 0 );
  integrandAuyTrue[2][0] = ( 28./25. );
  integrandAuyTrue[2][1] = ( 29./125. );
  integrandAuyTrue[2][2] = ( 383./1000. );
  integrandAuyTrue[2][3] = ( 113./100. );



  for (int i=0; i< fcnPDE.nDOF(); i++)
    for (int j=0; j< fcnPDE.nEqn(); j++)
    {
      SANS_CHECK_CLOSE( integrandPDETrue[i][j], integrandPDE[i][j], small_tol, close_tol );
      SANS_CHECK_CLOSE( integrandAuxTrue[i][j], integrandAUX[i][0][j], small_tol, close_tol );
      SANS_CHECK_CLOSE( integrandAuyTrue[i][j], integrandAUX[i][1][j], small_tol, close_tol );
    }

  sRef = 1./3.;  tRef = 1./3.;
  fcnPDE( {sRef, tRef}, integrandPDE, 3 );
  fcnAUX( {sRef, tRef}, integrandAUX, 3 );
//
  //PDE residual integrands: (advective) + (viscous)
  integrandPDETrue[0][0] = ( 523393./900000. ) + ( 0 );
  integrandPDETrue[0][1] = ( 108038039./90000000. ) + ( 307./90000. );
  integrandPDETrue[0][2] = ( 3403393./2700000. ) + ( -989./90000. );
  integrandPDETrue[0][3] = ( 348429804073./162000000000. ) + ( -1626614908253./258968043000000. );
  integrandPDETrue[1][0] = ( -71231./300000. ) + ( 0 );
  integrandPDETrue[1][1] = ( -11212771./10000000. ) + ( -239./45000. );
  integrandPDETrue[1][2] = ( -71231./900000. ) + ( 19./10000. );
  integrandPDETrue[1][3] = ( -47419440791./54000000000. ) + ( -425049084623./43161340500000. );
  integrandPDETrue[2][0] = ( -3097./9000. ) + ( 0 );
  integrandPDETrue[2][1] = ( -71231./900000. ) + ( 19./10000. );
  integrandPDETrue[2][2] = ( -31897./27000. ) + ( 409./45000. );
  integrandPDETrue[2][3] = ( -2061714817./1620000000. ) + ( 4176909415991./258968043000000. );

  integrandAuxTrue[0][0] = ( -3067./3000. );
  integrandAuxTrue[0][1] = ( -2093./9000. );
  integrandAuxTrue[0][2] = ( -3053./9000. );
  integrandAuxTrue[0][3] = ( -4783./4500. );
  integrandAuxTrue[1][0] = ( 3127./3000. );
  integrandAuxTrue[1][1] = ( 2047./9000. );
  integrandAuxTrue[1][2] = ( 2947./9000. );
  integrandAuxTrue[1][3] = ( 4817./4500. );
  integrandAuxTrue[2][0] = ( 1./100. );
  integrandAuxTrue[2][1] = ( -23./9000. );
  integrandAuxTrue[2][2] = ( -53./9000. );
  integrandAuxTrue[2][3] = ( 17./4500. );

  integrandAuyTrue[0][0] = ( -9221./9000. );
  integrandAuyTrue[0][1] = ( -49./225. );
  integrandAuyTrue[0][2] = ( -2807./9000. );
  integrandAuyTrue[0][3] = ( -3149./3000. );
  integrandAuyTrue[1][0] = ( 7./900. );
  integrandAuyTrue[1][1] = ( 11./900. );
  integrandAuyTrue[1][2] = ( 193./9000. );
  integrandAuyTrue[1][3] = ( 17./1000. );
  integrandAuyTrue[2][0] = ( 9361./9000. );
  integrandAuyTrue[2][1] = ( 109./450. );
  integrandAuyTrue[2][2] = ( 3193./9000. );
  integrandAuyTrue[2][3] = ( 3251./3000. );

  for (int i=0; i< fcnPDE.nDOF(); i++)
    for (int j=0; j< fcnPDE.nEqn(); j++)
    {
      SANS_CHECK_CLOSE( integrandPDETrue[i][j], integrandPDE[i][j], small_tol, close_tol );
      SANS_CHECK_CLOSE( integrandAuxTrue[i][j], integrandAUX[i][0][j], small_tol, close_tol );
      SANS_CHECK_CLOSE( integrandAuyTrue[i][j], integrandAUX[i][1][j], small_tol, close_tol );
    }

//
//
//  // test the element integral of the functor
//
  int quadratureorder = -1;
  int nIntegrand = afldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Triangle, BasisWeightedPDEClass::IntegrandType> integralPDE(quadratureorder, nIntegrand);
  GalerkinWeightedIntegral<TopoD2, Triangle, BasisWeightedAUXClass::IntegrandType> integralAUX(quadratureorder, nIntegrand);


  Real rsdPDETrue[3][4], rsdAuxTrue[3][4], rsdAuyTrue[3][4];
  BasisWeightedPDEClass::IntegrandType rsdPDEElem[3];
  BasisWeightedAUXClass::IntegrandType rsdAUXElem[3];

  // cell integration for canonical element
  integralPDE( fcnPDE, xfldElem, rsdPDEElem, nIntegrand );
  integralAUX( fcnAUX, xfldElem, rsdAUXElem, nIntegrand );
//

  //PDE residual: (advection) + (diffusion)
  rsdPDETrue[0][0] = ( 348931./1200000. ) + ( 0 );
  rsdPDETrue[0][1] = ( 180044713./300000000. ) + ( 307./180000. );
  rsdPDETrue[0][2] = ( 189100171./300000000. ) + ( -989./180000. );
  rsdPDETrue[0][3] = ( 387142357567./360000000000. ) + ( -0.00249983511875611 );
  rsdPDETrue[1][0] = ( -47443./400000. ) + ( 0 );
  rsdPDETrue[1][1] = ( -84093299./150000000. ) + ( -239./90000. );
  rsdPDETrue[1][2] = ( -790541./20000000. ) + ( 19./20000. );
  rsdPDETrue[1][3] = ( -52695175427./120000000000. ) + ( -0.00539847735443355 );
  rsdPDETrue[2][0] = ( -103301./600000. ) + ( 0 );
  rsdPDETrue[2][1] = ( -790541./20000000. ) + ( 19./20000. );
  rsdPDETrue[2][2] = ( -22155257./37500000. ) + ( 409./90000. );
  rsdPDETrue[2][3] = ( -114528415643./180000000000. ) + ( 0.00789831247304454 );

  rsdAuxTrue[0][0] = ( -6109./12000. );
  rsdAuxTrue[0][1] = ( -2807./24000. );
  rsdAuxTrue[0][2] = ( -4133./24000. );
  rsdAuxTrue[0][3] = ( -4263./8000. );
  rsdAuxTrue[1][0] = ( 6269./12000. );
  rsdAuxTrue[1][1] = ( 917./8000. );
  rsdAuxTrue[1][2] = ( 4007./24000. );
  rsdAuxTrue[1][3] = ( 12857./24000. );
  rsdAuxTrue[2][0] = ( 1./600. );
  rsdAuxTrue[2][1] = ( -3./2000. );
  rsdAuxTrue[2][2] = ( -43./12000. );
  rsdAuxTrue[2][3] = ( 17./6000. );

  rsdAuyTrue[0][0] = ( -1531./3000. );
  rsdAuyTrue[0][1] = ( -257./2400. );
  rsdAuyTrue[0][2] = ( -1229./8000. );
  rsdAuyTrue[0][3] = ( -4197./8000. );
  rsdAuyTrue[1][0] = ( 1./480. );
  rsdAuyTrue[1][1] = ( 59./12000. );
  rsdAuyTrue[1][2] = ( 81./8000. );
  rsdAuyTrue[1][3] = ( 17./2400. );
  rsdAuyTrue[2][0] = ( 6239./12000. );
  rsdAuyTrue[2][1] = ( 241./2000. );
  rsdAuyTrue[2][2] = ( 527./3000. );
  rsdAuyTrue[2][3] = ( 13033./24000. );

  const Real small_tol_rsd = 1e-11;
  const Real close_tol_rsd = 1e-10;

  for (int i=0; i< fcnPDE.nDOF(); i++)
    for (int j=0; j< fcnPDE.nEqn(); j++)
    {
      SANS_CHECK_CLOSE( rsdPDETrue[i][j], rsdPDEElem[i][j], small_tol_rsd, close_tol_rsd );
      SANS_CHECK_CLOSE( rsdAuxTrue[i][j], rsdAUXElem[i][0][j], small_tol_rsd, close_tol_rsd );
      SANS_CHECK_CLOSE( rsdAuyTrue[i][j], rsdAUXElem[i][1][j], small_tol_rsd, close_tol_rsd );
    }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
