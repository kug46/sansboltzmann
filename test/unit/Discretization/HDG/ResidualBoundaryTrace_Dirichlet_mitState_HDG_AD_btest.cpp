// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ResidualBoundary_HDG_Triangle_AD_btest
// testing of boundary trace-integral residual functions for HDG
// with Advection-Diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_Trace.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "Discretization/HDG/IntegrandBoundaryTrace_Flux_mitState_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_HDG.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ResidualBoundaryTrace_Dirichlet_mitState_HDG_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Line_BCTypeDirichlet_mitState_X1Q1LG1_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCTypeDirichlet_mitState<AdvectiveFlux1D_Uniform,ViscousFlux1D_Uniform> BCTypeDirichlet_mitState1D;

  typedef BCAdvectionDiffusion<PhysD1,BCTypeDirichlet_mitState1D> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> BoundaryIntegrandClass;

  // PDE

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // BC
  Real uB = 12./5.;
  BCClass bc(pde, uB);

  // grid: P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( qfld.nDOF(), 2 );

  // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 4;

  // auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( afld.nDOF(), 2 );

  // auxiliary variable data
  afld.DOF(0) = { 2};
  afld.DOF(1) = { 7};

  // trace variable
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( afld.nDOF(), 2 );

  qIfld.DOF(0) =  0; //Left domain boundary
  qIfld.DOF(1) =  8; //Right domain boundary

  // quadrature rule
  int quadratureOrder[2] = {0,0};

  ArrayQ rsdPDETrue[2];
  ArrayQ rsdINTTrue[2];

  //PDE residuals: (advective) + (viscous) + (stabilization)
  rsdPDETrue[0] = ( 0 ) + ( 0 ) + ( 0 ); // Basis function 1
  rsdPDETrue[1] = ( 22/5. ) + ( -14861/1000. ) + ( -2123/250. ); // Basis function 2

//  rsdAUXxTrue[0] = ( 0 ); // Basis function 1
//  rsdAUXxTrue[1] = ( -12/5. ); // Basis function 2

  rsdINTTrue[0] = ( 0 ); // Basis function 1 left boundary
  rsdINTTrue[1] = ( 15488/625. ); // Basis function 1 right boundary

  // PDE/BC global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());
  SLA::SparseVector<ArrayQ> rsdIntGlobal(qIfld.nDOF());

  // integrand

  const std::vector<int> BoundaryGroups = {1};
  DiscretizationHDG<PDEClass> disc( pde, Local, Gradient );

  // integrands
  BoundaryIntegrandClass fcnbc( pde, bc, BoundaryGroups, disc );

  // base interface

  rsdPDEGlobal = 0;
  rsdIntGlobal = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_HDG(fcnbc, rsdPDEGlobal, rsdIntGlobal),
                                                              xfld, (qfld, afld), qIfld, quadratureOrder, 2 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDEGlobal[1], small_tol, close_tol );

//  SANS_CHECK_CLOSE( rsdAuxTrue[0], rsdAuGlobal[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( rsdAuxTrue[1], rsdAuGlobal[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdINTTrue[0], rsdIntGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdINTTrue[1], rsdIntGlobal[1], small_tol, close_tol );
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_BCTypeDirichlet_mitState_2D_Triangle_X1Q1LG1_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCTypeDirichlet_mitState<AdvectiveFlux2D_Uniform,ViscousFlux2D_Uniform> BCTypeDirichlet_mitState2D;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeDirichlet_mitState2D> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> BoundaryIntegrandClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // BC
  Real uB = 12./5.;
  BCClass bc(pde, uB);

  // grid: P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( 3, qfld.nDOF() );

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  // auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( 3, afld.nDOF() );

  // triangle auxiliary variable data (left)
  afld.DOF(0) = { 2, -3};
  afld.DOF(1) = { 7,  8};
  afld.DOF(2) = {-1, -5};

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( 6, qIfld.nDOF() );

  // Lagrange multiplier DOF data
  qIfld.DOF(0) =  0;
  qIfld.DOF(1) =  0;
  qIfld.DOF(2) =  8;
  qIfld.DOF(3) = -1;
  qIfld.DOF(4) =  0;
  qIfld.DOF(5) =  0;

  // quadrature rule
  int quadratureOrder[3] = {2,2,2};

  Real rsdPDETrue[3];
  Real rsdINTTrue[2];

  // PDE residuals (left): (boundary flux) + (Lagrange)
  rsdPDETrue[0] = ( 0 ) + ( 0 ) + ( 0 ); // Basis function 1
  rsdPDETrue[1] = ( 13/6. ) + ( -6877/750. ) + ( -559/75. ); // Basis function 2
  rsdPDETrue[2] = ( 143/60. ) + ( -143/75. ) + ( 559/75. ); // Basis function 3

//  rsdAUXxTrue[0] = ( 0 ); // Basis function 1
//  rsdAUXyTrue[0] = ( 0 ); // Basis function 1
//  rsdAUXxTrue[1] = ( -6/5. ); // Basis function 2
//  rsdAUXyTrue[1] = ( -6/5. ); // Basis function 2
//  rsdAUXxTrue[2] = ( -6/5. ); // Basis function 3
//  rsdAUXyTrue[2] = ( -6/5. ); // Basis function 3

  rsdINTTrue[0] = ( 151229/7500. ); // Basis function 1
  rsdINTTrue[1] = ( -77441/7500. ); // Basis function 2

  // PDE/BC global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());
  SLA::SparseVector<ArrayQ> rsdIntGlobal(qIfld.nDOF());

  // integrand
  const std::vector<int> BoundaryGroups = {1};
  DiscretizationHDG<PDEClass> disc( pde, Local, Gradient );

  // integrands
  BoundaryIntegrandClass fcnbc( pde, bc, BoundaryGroups, disc );
  // base interface

  rsdPDEGlobal = 0;
  rsdIntGlobal = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_HDG(fcnbc,
                                                                                        rsdPDEGlobal, rsdIntGlobal),
                                                              xfld, (qfld, afld), qIfld, quadratureOrder, 3 );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDEGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[2], rsdPDEGlobal[2], small_tol, close_tol );

//  SANS_CHECK_CLOSE( rsdAuxTrue[0], rsdAuGlobal[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( rsdAuyTrue[0], rsdAuGlobal[1], small_tol, close_tol );
//
//  SANS_CHECK_CLOSE( rsdAuxTrue[1], rsdAuGlobal[2], small_tol, close_tol );
//  SANS_CHECK_CLOSE( rsdAuyTrue[1], rsdAuGlobal[3], small_tol, close_tol );
//
//  SANS_CHECK_CLOSE( rsdAuxTrue[2], rsdAuGlobal[4], small_tol, close_tol );
//  SANS_CHECK_CLOSE( rsdAuyTrue[2], rsdAuGlobal[5], small_tol, close_tol );

  SANS_CHECK_CLOSE(             0, rsdIntGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE(             0, rsdIntGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdINTTrue[0], rsdIntGlobal[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdINTTrue[1], rsdIntGlobal[3], small_tol, close_tol );
  SANS_CHECK_CLOSE(             0, rsdIntGlobal[4], small_tol, close_tol );
  SANS_CHECK_CLOSE(             0, rsdIntGlobal[5], small_tol, close_tol );

}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
