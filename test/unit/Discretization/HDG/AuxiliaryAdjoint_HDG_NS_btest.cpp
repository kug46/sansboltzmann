// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AuxiliaryAdjoint_HDG_NS_btest
// testing computing the auxiliary adjoint of HDG with NS
// Constructs the complete PDE, AUX and INT system and compares with statically condensed solves

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/SurrealS.h"

#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"

#include "Discretization/HDG/IntegrandCell_HDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_Flux_mitState_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_OutputWeightRsd_HDG.h"

#include "Discretization/HDG/SetFieldCell_HDG_AuxiliaryVariable.h"
#include "Discretization/HDG/SetFieldCell_HDG_AuxiliaryAdjoint.h"
#include "Discretization/HDG/SetFieldBoundaryTrace_HDG_AuxiliaryVariable.h"
#include "Discretization/HDG/SetFieldBoundaryTrace_HDG_AuxiliaryAdjoint.h"
#include "Discretization/HDG/ResidualCell_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_HDG.h"
#include "Discretization/HDG/JacobianCell_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_HDG_AuxiliaryVariable.h"
#include "Discretization/HDG/JacobianFunctionalBoundaryTrace_HDG_sansLG.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/QuadratureOrder.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Field/XField_CellToTrace.h"
#include "Field/tools/for_each_CellGroup.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

#include "JacobianTest_HDG.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;
using namespace SANS::testspace;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

namespace // no-name namespace keep things private to this file
{
template<class SurrealClass, class IntegrandCell, class IntegrandITrace,
         class IntegrandBTrace1, class IntegrandBTrace2,
         class JacAUXClass,
         class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
void computeAuxiliaryVariables(const IntegrandCell& fcnCell, const IntegrandITrace& fcnITrace,
                               const IntegrandBTrace1& fcnBTrace1, const IntegrandBTrace2& fcnBTrace2,
                               const FieldDataInvMassMatrix_Cell& mmfld, JacAUXClass& jacAUX_a_bcell,
                               const XField_CellToTrace<PhysDim, TopoDim>& connectivity,
                               const XField<PhysDim, TopoDim>& xfld, const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                               const Field<PhysDim, TopoDim, VectorArrayQ>& afld, const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                               const QuadratureOrder& quadOrder)
{
  jacAUX_a_bcell = 0.0;

  FieldDataVectorD_BoundaryCell<VectorArrayQ> rsdAUX_bcell(qfld);
  rsdAUX_bcell = 0.0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
      SetFieldBoundaryTrace_HDG_AuxiliaryVariable( fcnBTrace1, rsdAUX_bcell, jacAUX_a_bcell ),
      xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

  IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
      SetFieldBoundaryTrace_HDG_AuxiliaryVariable( fcnBTrace2, rsdAUX_bcell, jacAUX_a_bcell ),
      xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

  std::vector<int> cellgroupsAux(fcnCell.nCellGroups());
  for (std::size_t i = 0; i < fcnCell.nCellGroups(); i++)
    cellgroupsAux[i] = fcnCell.cellGroup(i);

  //Note: jacAUX_a_bcell gets completed and inverted inside SetFieldCell_HDG_AuxiliaryVariable
  IntegrateCellGroups<TopoDim>::integrate(
      SetFieldCell_HDG_AuxiliaryVariable( fcnCell, fcnITrace, mmfld, connectivity,
                                          xfld, qfld, afld, qIfld,
                                          quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                          rsdAUX_bcell, cellgroupsAux, true, jacAUX_a_bcell),
      xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );
}
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( AuxiliaryAdjoint_HDG_NS_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AuxiliaryAdjoint_HDG_NS_Triangle )
{
  const int D = PhysD2::D;

  typedef SurrealS<4> SurrealClass;

  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template MatrixQ<Real> MatrixQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef NDPDEClass::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef NDPDEClass::template TensorMatrixQ<Real> TensorMatrixQ;

  typedef BCNDConvertSpace<PhysD2, BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState,PDEClass>> NDBCWallClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCWallClass>, NDBCWallClass::Category> NDBCWallVecCat;

  typedef BCNDConvertSpace<PhysD2, BCEuler2D<BCTypeFullState_mitState,PDEClass>> NDBCCharClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCCharClass>, NDBCCharClass::Category> NDBCCharVecCat;

  typedef IntegrandCell_HDG<NDPDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<NDPDEClass> IntegrandITraceClass;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCWallVecCat, HDG> IntegrandBTrace1Class;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCCharVecCat, HDG> IntegrandBTrace2Class;

  // DRAG INTEGRAND
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;

  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, HDG> IntegrandOutputClass;

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.1;
  const Real Reynolds = 10;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
  const Real qRef = 1;                              // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);
  const Real cRef = qRef/Mach;                      // speed of sound
  const Real tRef = (cRef*cRef)/(gamma*R);          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum );

  // BC
  NDBCWallClass bcwall( pde );

  bool upwind = true;
  NDBCCharClass bcchar(pde, DensityVelocityPressure2D<Real>(rhoRef, uRef, vRef, pRef), upwind);

  // grid
  int ii = 2;
  int jj = 2;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  XField_CellToTrace<PhysD2, TopoD2> connectivity(xfld);

  // initial condition
  ArrayQ q0;
  pde.setDOFFrom( q0, DensityVelocityPressure2D<Real>(rhoRef, uRef, vRef, pRef) );

  // solution
  int order = 2;

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = q0;

  // Auxiliary variable
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
  afld = 0;

  // Interface variable
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);
  qIfld = q0;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order-1, BasisFunctionCategory_Legendre, {} );
  lgfld = 0;

  const int nDOFPDE = qfld.nDOF();
  const int nDOFAUX = D*afld.nDOF();
  const int nDOFINT = qIfld.nDOF();
  const int nDOFBC = lgfld.nDOF();

  // Compute the field of inverse mass matrices
  FieldDataInvMassMatrix_Cell mmfld(qfld);

  // quadrature rule
  QuadratureOrder quadOrder(xfld, 2*order);

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Local, Gradient );

  // integrands
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandITraceClass fcnITrace( pde, disc, {0,1,2} );
  IntegrandBTrace1Class fcnBTrace1( pde, bcwall, {0,2}, disc );
  IntegrandBTrace2Class fcnBTrace2( pde, bcchar, {1,3}, disc );

  NDOutputClass outputFcn(pde, 1., 0.);
  IntegrandOutputClass output_integrand( outputFcn, {0,2} );

  // linear system setup

  // complete jacobian nonzero pattern without static condensation of auxiliary variables
  //
  //        q   a  qI  lg
  //   PDE  X   X   X   X
  //   AUX  X   X   X   X
  //   IINT X   X   X   0
  //   BC   X   X   0   0

  DLA::MatrixD<DLA::MatrixD<MatrixQ>> jacT =
  {{ {nDOFPDE, nDOFPDE}, {nDOFPDE, nDOFAUX}, {nDOFPDE, nDOFINT}, {nDOFPDE, nDOFBC} },
   { {nDOFAUX, nDOFPDE}, {nDOFAUX, nDOFAUX}, {nDOFAUX, nDOFINT}, {nDOFAUX, nDOFBC} },
   { {nDOFINT, nDOFPDE}, {nDOFINT, nDOFAUX}, {nDOFINT, nDOFINT}, {nDOFINT, nDOFBC} },
   { {nDOFBC , nDOFPDE}, {nDOFBC , nDOFAUX}, {nDOFBC , nDOFINT}, {nDOFBC , nDOFBC} }};

  DLA::VectorD<DLA::VectorD<ArrayQ>> rhs = DLA::VectorD< DLA::DenseVectorSize >({ nDOFPDE, nDOFAUX, nDOFINT, nDOFBC });
  DLA::VectorD<DLA::VectorD<ArrayQ>> adj = DLA::VectorD< DLA::DenseVectorSize >({ nDOFPDE, nDOFAUX, nDOFINT, nDOFBC });

  rhs = 0;

  // Create right hand side for the adjoint
  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      JacobianFunctionalBoundaryTrace_HDG_Test<SurrealClass>( output_integrand, fcnBTrace1, rhs(0), rhs(1), rhs(2) ),
      xfld, (qfld, afld), qIfld,
      quadOrder.boundaryTraceOrders.data(),
      quadOrder.boundaryTraceOrders.size() );

  jacT = 0;

  // jacobian transpose
  auto jacTPDE_q  = Transpose(jacT)(0,0);
  auto jacTPDE_a  = Transpose(jacT)(0,1);
  auto jacTPDE_qI = Transpose(jacT)(0,2);
//auto jacTPDE_lg = Transpose(jacT)(0,3);

  auto jacTAUX_q  = Transpose(jacT)(1,0);
  auto jacTAUX_a  = Transpose(jacT)(1,1);
  auto jacTAUX_qI = Transpose(jacT)(1,2);
//auto jacTAUX_lg = Transpose(jacT)(1,3);

  auto jacTINT_q  = Transpose(jacT)(2,0);
  auto jacTINT_a  = Transpose(jacT)(2,1);
  auto jacTINT_qI = Transpose(jacT)(2,2);
//auto jacTINT_lg = Transpose(jacT)(2,3);

  auto jacTBC_q  = Transpose(jacT)(3,0);
  auto jacTBC_a  = Transpose(jacT)(3,1);
  auto jacTBC_qI = Transpose(jacT)(3,2);
//auto jacTBC_lg = Transpose(jacT)(3,3);


  IntegrateCellGroups<TopoD2>::integrate( JacobianCell_HDG_Test<SurrealClass>(fcnCell, jacTPDE_q, jacTPDE_a,
                                                                                       jacTAUX_q, jacTAUX_a),
                                          xfld, (qfld, afld),
                                          quadOrder.cellOrders.data(),
                                          quadOrder.cellOrders.size() );


  IntegrateInteriorTraceGroups_FieldTrace<TopoD2>::integrate(
      JacobianInteriorTrace_HDG_Test<SurrealClass>(fcnITrace, jacTPDE_q, jacTPDE_a, jacTPDE_qI,
                                                              jacTAUX_q,            jacTAUX_qI,
                                                              jacTINT_q, jacTINT_a, jacTINT_qI),
      xfld, (qfld, afld), qIfld,
      quadOrder.interiorTraceOrders.data(),
      quadOrder.interiorTraceOrders.size() );


  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      JacobianBoundaryTrace_HDG_Test<SurrealClass>( fcnBTrace1, jacTPDE_q, jacTPDE_a, jacTPDE_qI,
                                                                jacTAUX_q, jacTAUX_a, jacTAUX_qI,
                                                                jacTINT_q, jacTINT_a, jacTINT_qI),
      xfld, (qfld, afld), qIfld,
      quadOrder.boundaryTraceOrders.data(),
      quadOrder.boundaryTraceOrders.size() );

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      JacobianBoundaryTrace_HDG_Test<SurrealClass>( fcnBTrace2, jacTPDE_q, jacTPDE_a, jacTPDE_qI,
                                                                jacTAUX_q, jacTAUX_a, jacTAUX_qI,
                                                                jacTINT_q, jacTINT_a, jacTINT_qI),
      xfld, (qfld, afld), qIfld,
      quadOrder.boundaryTraceOrders.data(),
      quadOrder.boundaryTraceOrders.size() );

  // Compute the adjoint
  adj = DLA::InverseLU::Solve(jacT, rhs);

  // Adjoint fields
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, order, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> bfld(xfld, order, BasisFunctionCategory_Legendre);
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> wIfld(xfld, order, BasisFunctionCategory_Legendre);
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> mufld( xfld, order-1, BasisFunctionCategory_Legendre, {} );

  // Copy the solution from the linear algebra vector to the adjoint field variables
  BOOST_CHECK( nDOFPDE == wfld.nDOF() );
  BOOST_CHECK( nDOFPDE == adj[0].m() );
  for (int k = 0; k < nDOFPDE; k++)
    wfld.DOF(k) = adj[0][k];

  BOOST_CHECK( nDOFINT == wIfld.nDOF() );
  BOOST_CHECK( nDOFINT == adj[2].m() );
  for (int k = 0; k < nDOFINT; k++)
    wIfld.DOF(k) = adj[2][k];

  BOOST_CHECK( nDOFBC == mufld.nDOF() );
  BOOST_CHECK( nDOFBC == adj[3].m() );
  for (int k = 0; k < nDOFBC; k++)
    mufld.DOF(k) = adj[3][k];

  FieldDataMatrixD_BoundaryCell<TensorMatrixQ> invJacAUX_a_bcell(qfld);

  computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace1, fcnBTrace2,
                                          mmfld, invJacAUX_a_bcell, connectivity,
                                          xfld, qfld, afld, qIfld, quadOrder);

  //Compute RHS for static-condensed system
  FieldDataMatrixD_BoundaryCell<VectorMatrixQ> jacAUX_q_bcell(qfld);
  FieldDataMatrixD_BoundaryTrace<VectorMatrixQ> jacAUX_qI_btrace(qfld, qIfld);
  jacAUX_q_bcell = 0.0;
  jacAUX_qI_btrace = 0.0;

  std::vector<std::vector<std::vector<int>>> mapDOFGlobal_qI(xfld.nBoundaryTraceGroups());

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      JacobianBoundaryTrace_HDG_AuxiliaryVariable<SurrealClass>( fcnBTrace1, jacAUX_q_bcell, jacAUX_qI_btrace, mapDOFGlobal_qI ),
      xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      JacobianBoundaryTrace_HDG_AuxiliaryVariable<SurrealClass>( fcnBTrace2, jacAUX_q_bcell, jacAUX_qI_btrace, mapDOFGlobal_qI ),
      xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

  bfld = 0;

  //This will set the first component of the auxiliary adjoint "b"
  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      JacobianFunctionalBoundaryTrace_HDG_sansLG<SurrealClass>( output_integrand, fcnBTrace1, fcnCell, fcnITrace,
                                                                invJacAUX_a_bcell, jacAUX_q_bcell, jacAUX_qI_btrace,
                                                                mapDOFGlobal_qI, connectivity,
                                                                xfld, qfld, afld, qIfld,
                                                                quadOrder.cellOrders.data(), quadOrder.cellOrders.size(),
                                                                quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                                                rhs(0), rhs(2) ),
      xfld, (qfld, afld, bfld), qIfld,
      quadOrder.boundaryTraceOrders.data(),
      quadOrder.boundaryTraceOrders.size() );


  //Compute the remaining contributions to the auxiliary adjoint
  IntegrateCellGroups<TopoD2>::integrate(
      SetFieldCell_HDG_AuxiliaryAdjoint<SurrealClass>( fcnCell, fcnITrace,
                                                       mmfld, invJacAUX_a_bcell, connectivity,
                                                       xfld, qfld, afld, qIfld, wfld, bfld, wIfld,
                                                       quadOrder.interiorTraceOrders.data(),
                                                       quadOrder.interiorTraceOrders.size() ),
      xfld, (qfld, afld, wfld, bfld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      SetFieldBoundaryTrace_HDG_AuxiliaryAdjoint<SurrealClass>( fcnBTrace1, invJacAUX_a_bcell),
      xfld, (qfld, afld, wfld, bfld), (qIfld, wIfld),
      quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      SetFieldBoundaryTrace_HDG_AuxiliaryAdjoint<SurrealClass>( fcnBTrace2, invJacAUX_a_bcell),
      xfld, (qfld, afld, wfld, bfld), (qIfld, wIfld),
      quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

  const Real small_tol = 1e-10;
  const Real close_tol = 2e-9;

  for ( int i = 0; i < bfld.nDOF(); i++ )
    for (int d = 0; d < D; d++)
    {
      for (int m = 0; m < ArrayQ::M; m++)
      {
//      std::cout << "[" << i << "][" << d << "][" << m << "]: " << bfld.DOF(i)[d][m] << " == " << adj[1][D*i + d][m]<< std::endl;
      SANS_CHECK_CLOSE( bfld.DOF(i)[d][m], adj[1][D*i + d][m], small_tol, close_tol );
      }
    }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
