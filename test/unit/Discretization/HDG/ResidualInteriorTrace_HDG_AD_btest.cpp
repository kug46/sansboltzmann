// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ResidualInteriorTrace_HDG_AD_btest
// testing of 2-D line residual functions for HDG with Advection-Diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_InteriorTrace.h"

#include "Field/XField_CellToTrace.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"
#include "Discretization/HDG/ResidualCell_HDG.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/QuadratureOrder.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"

#include "IntegrandTest_HDG.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ResidualInteriorTrace_HDG_AD_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualInteriorTrace_1D_HDG_2Line_X1Q1R1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef testspace::IntegrandCell_HDG_None<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandTraceClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_UniformGrad source(1.0/4.0, 3.0/5.0);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid: 2 lines @ P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;

  BOOST_REQUIRE_EQUAL( 3, xfld.nDOF() );
  BOOST_REQUIRE_EQUAL( 2, xfld.nElem() );

  BOOST_REQUIRE_EQUAL( xfld.nInteriorTraceGroups(), 1 );

  XField_CellToTrace<PhysD1, TopoD1> connectivity(xfld);

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_REQUIRE_EQUAL(qfld.nDOF(), 4);

  // line solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 4;

  // line solution data (right)
  qfld.DOF(2) = 7;
  qfld.DOF(3) = 9;

  // auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_REQUIRE_EQUAL( afld.nDOF()*PhysD1::D, 4 );

  // line auxiliary variable (left)
  afld.DOF(0) = { 2};
  afld.DOF(1) = { 7};

  // line auxiliary variable (right)
  afld.DOF(2) = { 9};
  afld.DOF(3) = {-1};

  // interface solution: P1 (aka Q1)
  Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_REQUIRE_EQUAL(qIfld.nDOF(), 1);
  BOOST_REQUIRE_EQUAL( qIfld.nInteriorTraceGroups(), 1 );

  // node trace data
  qIfld.DOF(0) =  8;

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Local );

  // integrand
  IntegrandCellClass fcnCell({0});
  IntegrandTraceClass fcnTrace( pde, disc, {0} );

  // quadrature rule
  QuadratureOrder quadOrder(xfld, 2);

  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());
  SLA::SparseVector<ArrayQ> rsdINTGlobal(qIfld.nDOF());
  rsdPDEGlobal = 0;
  rsdINTGlobal = 0;

  //Compute interior trace residuals without solving for auxiliary variables
  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_HDG(fcnCell, fcnTrace, connectivity,
                                                           xfld, qfld, afld, qIfld,
                                                           quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                                           rsdPDEGlobal, rsdINTGlobal),
                                          xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

  Real rsdPDELTrue[2], rsdPDERTrue[2], rsdPDEITrue[1];

  //PDE residuals (left): (advective) + (viscous) + (stabilization)
  rsdPDELTrue[0] = ( 0 ) + ( 0 ) + ( 0 );   // Basis function 1
  rsdPDELTrue[1] = ( 22/5. ) + ( -14861/1000. ) + ( -2123/250. );   // Basis function 2

  //PDE residuals (right): (advective) + (viscous) + (stabilization)
  rsdPDERTrue[0] = ( -44/5. ) + ( 19107/1000. ) + ( -2123/1000. );   // Basis function 1
  rsdPDERTrue[1] = ( 0 ) + ( 0 ) + ( 0 );   // Basis function 2

  //INT residual integrands (left): (advective) + (viscous) + (stabilization)
  rsdPDEITrue[0] = ( 22/5. ) + ( -2123/500. ) + ( 2123/200. );  // Basis function 1

  const Real small_tol = 1e-13;
  const Real close_tol = 1.5e-12;

  SANS_CHECK_CLOSE( rsdPDELTrue[0], rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDELTrue[1], rsdPDEGlobal[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdPDERTrue[0], rsdPDEGlobal[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue[1], rsdPDEGlobal[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdPDEITrue[0], rsdINTGlobal[0], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualInteriorTrace_2D_HDG_2Triangle_X1Q1R1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef testspace::IntegrandCell_HDG_None<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandTraceClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_UniformGrad source(0.25, 0.6, -1.5);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid: 2 triangles @ P1 (aka X1)
  XField2D_2Triangle_X1_1Group xfld;

  XField_CellToTrace<PhysD2, TopoD2> connectivity(xfld);

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  // triangle solution data (right)
  qfld.DOF(3) = 7;
  qfld.DOF(4) = 2;
  qfld.DOF(5) = 9;

  // auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle auxiliary variable (left)
  afld.DOF(0) = { 2, -3};
  afld.DOF(1) = { 7,  8};
  afld.DOF(2) = {-1, -5};

  // triangle auxiliary variable (right)
  afld.DOF(3) = { 9,  6};
  afld.DOF(4) = {-1,  3};
  afld.DOF(5) = { 2, -3};

  // interface solution: P1 (aka Q1)
  Field_DG_InteriorTrace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // line trace data
  qIfld.DOF(0) =  8;
  qIfld.DOF(1) = -1;

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Local );

  // integrand
  IntegrandCellClass fcnCell({0});
  IntegrandTraceClass fcnTrace( pde, disc, {0} );

  // quadrature rule
  QuadratureOrder quadOrder(xfld, 2);

  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());
  SLA::SparseVector<ArrayQ> rsdINTGlobal(qIfld.nDOF());
  rsdPDEGlobal = 0;
  rsdINTGlobal = 0;

  //Compute interior trace residuals without solving for auxiliary variables
  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_HDG(fcnCell, fcnTrace, connectivity,
                                                           xfld, qfld, afld, qIfld,
                                                           quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                                           rsdPDEGlobal, rsdINTGlobal),
                                          xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

  Real rsdPDELTrue[3], rsdPDERTrue[3], rsdPDEITrue[2];

  //PDE residuals (left): (advective) + (viscous) + (stabilization)
  rsdPDELTrue[0] = ( 0 ) + ( 0 ) + ( 0 )  ;   // Basis function 1
  rsdPDELTrue[1] = ( 13/6. ) + ( -6877/750. ) + ( -559/75. )  ;   // Basis function 2
  rsdPDELTrue[2] = ( 143/60. ) + ( -143/75. ) + ( 559/75. )  ;   // Basis function 3

  //PDE residuals (right): (advective) + (viscous) + (lifting-operator))
  rsdPDERTrue[0] = ( 0 ) + ( 0 )+ ( 0 ) ;   // Basis function 1
  rsdPDERTrue[1] = ( -13/10. ) + ( 39/50. )+ ( 3913/375. ) ;   // Basis function 2
  rsdPDERTrue[2] = ( -13/4. ) + ( 169/250. )+ ( 559/75. ) ;   // Basis function 3

  //Interface residuals: (advective) + (viscous) + (stabilization)
  rsdPDEITrue[0] = ( 13/12. )+( 637/75. ) + ( 0 );   // Basis function 1
  rsdPDEITrue[1] = ( -13/12. )+( 169/150. ) + ( -2236/125. );   // Basis function 2

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  SANS_CHECK_CLOSE( rsdPDELTrue[0], rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDELTrue[1], rsdPDEGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDELTrue[2], rsdPDEGlobal[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdPDERTrue[0], rsdPDEGlobal[3], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue[1], rsdPDEGlobal[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue[2], rsdPDEGlobal[5], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdPDEITrue[0], rsdINTGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDEITrue[1], rsdINTGlobal[1], small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
