// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// isValidStateInteriorTrace_HDG_Euler_btest
// testing of trace physicality check for HDG with Euler

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/Tuple.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/PDEEuler2D.h"

#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/PDEEuler1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_InteriorTrace.h"

#include "Field/XField.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"
#include "Discretization/isValidState/isValidStateInteriorTrace_HDG.h"

#include "Discretization/IntegrateInteriorTraceGroups_FieldTrace.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( isValidStateInteriorTrace_HDG_Euler_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidStateInteriorTrace_2D_2Triangle_X1Q1_1Group )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef IntegrandInteriorTrace_HDG<NDPDEClass> IntegrandClass;
  typedef DiscretizationHDG<NDPDEClass> DiscretizationClass;

  // PDE
  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)

  GasModel gas(gamma, R);

  NDPDEClass pde(gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 4 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  Real rho, u, v, t;

  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;

  // set
  Real qDataPrim[4] = {rho, u, v, t};
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY", "Temperature"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );

  // grid: 2 triangles @ P1 (aka X1)
  XField2D_2Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle solution data (left)
  qfld.DOF(0) = q;
  qfld.DOF(1) = q;
  qfld.DOF(2) = q;

  // triangle solution data (right)
  qfld.DOF(3) = q;
  qfld.DOF(4) = q;
  qfld.DOF(5) = q;

  // auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle auxiliary variable (left)
  afld.DOF(0) = { 2, -3};
  afld.DOF(1) = { 7,  8};
  afld.DOF(2) = {-1, -5};

  // triangle auxiliary variable (right)
  afld.DOF(3) = { 9,  6};
  afld.DOF(4) = {-1,  3};
  afld.DOF(5) = { 2, -3};

  // interface solution: P1 (aka Q1)
  Field_DG_InteriorTrace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // line trace data
  qIfld.DOF(0) = q;
  qIfld.DOF(1) = q;
  // HDG discretization: a = gradq; no stabilization
  DiscretizationClass disc( pde, Nothing, Gradient );

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // quadrature rule (linear: basis grad is const, flux is linear)
  int quadratureOrder = 2;
  bool isValidState;

  isValidState = true;
  IntegrateInteriorTraceGroups_FieldTrace<TopoD2>::integrate( isValidStateInteriorTrace_HDG(fcnint, pde, isValidState),
                                                              xfld, (qfld, afld), qIfld, &quadratureOrder,
                                                              1);
  BOOST_CHECK_EQUAL( isValidState, true );

  // Now for a non-physical density
  rho = -1.137;
  qDataPrim[0] = rho;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );
  qIfld.DOF(0) = q;

  isValidState = true;
  IntegrateInteriorTraceGroups_FieldTrace<TopoD2>::integrate( isValidStateInteriorTrace_HDG(fcnint, pde, isValidState),
                                                              xfld, (qfld, afld), qIfld, &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( isValidState, false );

  // Now for a non-physical temperature
  rho = 1.137; t = -0.987;
  qDataPrim[0] = rho;
  qDataPrim[3] = t;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );
  qIfld.DOF(0) = q;

  isValidState = true;
  IntegrateInteriorTraceGroups_FieldTrace<TopoD2>::integrate( isValidStateInteriorTrace_HDG(fcnint, pde, isValidState),
                                                              xfld, (qfld, afld), qIfld, &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( isValidState, false );

  // Now for a non-physical density and temperature
  rho = -1.137; t = -0.987;
  qDataPrim[0] = rho;
  qDataPrim[3] = t;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );
  qIfld.DOF(0) = q;

  isValidState = true;
  IntegrateInteriorTraceGroups_FieldTrace<TopoD2>::integrate( isValidStateInteriorTrace_HDG(fcnint, pde, isValidState),
                                                              xfld, (qfld, afld), qIfld, &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( isValidState, false );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
