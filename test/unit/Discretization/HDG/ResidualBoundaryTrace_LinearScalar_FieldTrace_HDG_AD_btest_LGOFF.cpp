// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ResidualBoundary_HDG_Triangle_AD_btest
// testing of boundary trace-integral residual functions for HDG
// with Advection-Diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_Trace.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_mitLG_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_sansLG_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_FieldTrace_HDG.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ResidualBoundaryTrace_HDG_AD_NEW_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_mitLG_1D_1Line_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> BoundaryIntegrandClass;

  // PDE

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC

  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( qfld.nDOF(), 2 );

  // solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 4;

  // auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( afld.nDOF(), 2 );

  // auxiliary variable data (left)
  afld.DOF(0) = { 7};
  afld.DOF(1) = { 9};

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( afld.nDOF(), 2 );

  // Lagrange multiplier DOF data
  lgfld.DOF(0) =  0; //Left domain boundary
  lgfld.DOF(1) =  7; //Right domain boundary

  // quadrature rule
  int quadratureOrder[2] = {0,0};

  // PDE residuals (left): (boundary flux) + (Lagrange)
  Real rsd1PDE = (0)      + (0);
  Real rsd2PDE = (22./5.) + (-19107./1000.) + (92214./3125.);

  // Auxiliary residuals: (Lagrange)
  Real rsd1Aux = 0;
  Real rsd2Aux = -97885161./2500000.;

  // BC residuals
  Real rsd1BC = 0;
  Real rsd2BC = 19607./500.;

  // PDE/BC global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());
  SLA::SparseVector<ArrayQ> rsdAuGlobal(PhysD1::D*afld.nDOF());
  SLA::SparseVector<ArrayQ> rsdBCGlobal(lgfld.nDOF());

  // integrand

  const std::vector<int> BoundaryGroups = {1};
  DiscretizationHDG<PDEClass> disc( pde, Global, AugGradient );

  // integrands
  BoundaryIntegrandClass fcnbc( pde, bc, BoundaryGroups, disc );

  // base interface

  rsdPDEGlobal = 0;
  rsdAuGlobal = 0;
  rsdBCGlobal = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc, rsdPDEGlobal, rsdAuGlobal, rsdBCGlobal),
                                                      xfld, (qfld, afld), lgfld, quadratureOrder, 2 );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  SANS_CHECK_CLOSE( rsd1PDE, rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2PDE, rsdPDEGlobal[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsd1Aux, rsdAuGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2Aux, rsdAuGlobal[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsd1BC, rsdBCGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2BC, rsdBCGlobal[1], small_tol, close_tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_FieldTrace_sansLG_1D_1Line_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_sansLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> BoundaryIntegrandClass;

  // PDE

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC

  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( qfld.nDOF(), 2 );

  // solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 4;

  // auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( afld.nDOF(), 2 );

  // auxiliary variable data (left)
  afld.DOF(0) = { 2};
  afld.DOF(1) = { 7};

  // Lagrange multiplier
  Field_DG_Trace<PhysD1, TopoD1, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( qIfld.nDOF(), 2 );

  // Lagrange multiplier DOF data
  qIfld.DOF(0) =  0; //Left domain boundary
  qIfld.DOF(1) =  8; //Right domain boundary

  // quadrature rule
  int quadratureOrder[2] = {0,0};

  Real rsdPDETrue[2], rsdAuxTrue[2], rsdPDEITrue;

  //PDE residuals (left): (advective) + (viscous) + (bc)
  rsdPDETrue[0] = ( 0 ) +  ( 0 ) + ( 0 ); // Basis function 1
  rsdAuxTrue[0] = ( 0 ); // Basis function 1
  rsdPDETrue[1] = ( 22/5. ) +  ( -14861/1000. ) + ( 55583/16250. ); // Basis function 2
  rsdAuxTrue[1] = ( 55583/6500. ); // Basis function 2
  rsdPDEITrue = ( 2123/250. ); // Basis function 1

  // PDE/BC global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());
  SLA::SparseVector<ArrayQ> rsdAuGlobal(PhysD1::D*afld.nDOF());
  SLA::SparseVector<ArrayQ> rsdBCGlobal(qIfld.nDOF());

  // integrand
  const std::vector<int> BoundaryGroups = {1};
  DiscretizationHDG<PDEClass> disc( pde, Local, Gradient );

  // integrands
  BoundaryIntegrandClass fcnbc( pde, bc, BoundaryGroups, disc );

  // base interface
  rsdPDEGlobal = 0;
  rsdAuGlobal = 0;
  rsdBCGlobal = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc, rsdPDEGlobal, rsdAuGlobal, rsdBCGlobal),
                                                      xfld, (qfld, afld), qIfld, quadratureOrder, 2 );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDEGlobal[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdAuxTrue[0], rsdAuGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAuxTrue[1], rsdAuGlobal[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( 0, rsdBCGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDEITrue, rsdBCGlobal[1], small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_mitLG_2D_1Triangle_BCTypeLinearRobin_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> BoundaryIntegrandClass;


  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC

  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  // auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle auxiliary variable data (left)
  afld.DOF(0) = { 2, -3};
  afld.DOF(1) = { 7,  8};
  afld.DOF(2) = {-1, -5};

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // Lagrange multiplier DOF data
  lgfld.DOF(0) =  0;
  lgfld.DOF(1) =  0;
  lgfld.DOF(2) =  7;
  lgfld.DOF(3) = -9;
  lgfld.DOF(4) =  0;
  lgfld.DOF(5) =  0;

  // quadrature rule
  int quadratureOrder[3] = {2,2,2};

  // PDE residuals (left): (boundary flux) + (Lagrange)
  Real rsd1PDE = (0)           + (0);
  Real rsd2PDE = (-3329./500.) + (5329./2500. + 21043*sqrt(2)/9375.);
  Real rsd3PDE = (   49./100.) + (-2909./500. - 4043*sqrt(2)/1875.);

  // Auxiliary residuals: (Lagrange)
  Real rsd1Aux = 0;
  Real rsd2Aux = -223./250. - 2896101*sqrt(2)/625000.;
  Real rsd3Aux = 17171./1250. - 114399*sqrt(2)/125000.;
  Real rsd1Auy = 0;
  Real rsd2Auy = -13./25. - 168831*sqrt(2)/62500.;
  Real rsd3Auy = 1001./125. - 6669*sqrt(2)/12500.;

  // BC residuals
  Real rsd1BC = 4329./250. + sqrt(2)/6.;
  Real rsd2BC = 171./50. + sqrt(2)/3.;

  // PDE/BC global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());
  SLA::SparseVector<ArrayQ> rsdAuGlobal(2*afld.nDOF());
  SLA::SparseVector<ArrayQ> rsdBCGlobal(lgfld.nDOF());

  // integrand
  const std::vector<int> BoundaryGroups = {1};
  DiscretizationHDG<PDEClass> disc( pde, Global, AugGradient );

  // integrands
  BoundaryIntegrandClass fcnbc( pde, bc, BoundaryGroups, disc );
  // base interface

  rsdPDEGlobal = 0;
  rsdAuGlobal = 0;
  rsdBCGlobal = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc, rsdPDEGlobal, rsdAuGlobal, rsdBCGlobal),
                                             xfld, (qfld, afld), lgfld, quadratureOrder, 3 );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  SANS_CHECK_CLOSE( rsd1PDE, rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2PDE, rsdPDEGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd3PDE, rsdPDEGlobal[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsd1Aux, rsdAuGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd1Auy, rsdAuGlobal[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsd2Aux, rsdAuGlobal[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2Auy, rsdAuGlobal[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsd3Aux, rsdAuGlobal[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd3Auy, rsdAuGlobal[5], small_tol, close_tol );

  SANS_CHECK_CLOSE(      0, rsdBCGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE(      0, rsdBCGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd1BC, rsdBCGlobal[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2BC, rsdBCGlobal[3], small_tol, close_tol );
  SANS_CHECK_CLOSE(      0, rsdBCGlobal[4], small_tol, close_tol );
  SANS_CHECK_CLOSE(      0, rsdBCGlobal[5], small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_FieldTrace_sansLG_2D_1Triangle_BCTypeLinearRobin_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_sansLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> BoundaryIntegrandClass;


  // PDE

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC

  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  // auxiliary variable: P1 (aka R1)
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle auxiliary variable data (left)
  afld.DOF(0) = { 2, -3};
  afld.DOF(1) = { 7,  8};
  afld.DOF(2) = {-1, -5};

  // Lagrange multiplier
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // Lagrange multiplier DOF data
  qIfld.DOF(0) =  0;
  qIfld.DOF(1) =  0;
  qIfld.DOF(2) =  8;
  qIfld.DOF(3) = -1;
  qIfld.DOF(4) =  0;
  qIfld.DOF(5) =  0;

  // quadrature rule
  int quadratureOrder[3] = {2,2,2};

  Real rsdPDETrue[3], rsdAuxTrue[3], rsdAuyTrue[3];
  Real rsdPDEITrue[2];

  //PDE residuals (left):
  rsdPDETrue[0] = ( 0 ) + ( 0 ) + ( 0 ); // Basis function 1
  rsdPDETrue[1] = ( 13/6. ) + ( -4329/500. ) + ( (-1/65000.)*((pow(2,-1/2.))*((-30+(13)*(sqrt(2)))*(2500+(12987)*(sqrt(2))))) ); // Basis function 2
  rsdPDETrue[2] = ( 143/60. ) + ( -171/100. ) + ( (-1/13000.)*((pow(2,-1/2.))*((-30+(13)*(sqrt(2)))*(-100+(513)*(sqrt(2))))) ); // Basis function 3

  rsdAuxTrue[0] = ( 0 ); // Basis function 1
  rsdAuyTrue[0] = ( 0 ); // Basis function 1
  rsdAuxTrue[1] = ( (1/3250.)*((pow(2,-1/2.))*(2500+(12987)*(sqrt(2)))) ); // Basis function 2
  rsdAuyTrue[1] = ( (1/3250.)*((pow(2,-1/2.))*(2500+(12987)*(sqrt(2)))) ); // Basis function 2
  rsdAuxTrue[2] = ( (1/650.)*((pow(2,-1/2.))*(-100+(513)*(sqrt(2)))) ); // Basis function 3
  rsdAuyTrue[2] = ( (1/650.)*((pow(2,-1/2.))*(-100+(513)*(sqrt(2)))) ); // Basis function 3

  rsdPDEITrue[0] = ( 353/50. ); // Basis function 1
  rsdPDEITrue[1] = ( -353/50. ); // Basis function 2


  // PDE/BC global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());
  SLA::SparseVector<ArrayQ> rsdAuGlobal(2*afld.nDOF());
  SLA::SparseVector<ArrayQ> rsdBCGlobal(qIfld.nDOF());

  // integrand
  const std::vector<int> BoundaryGroups = {1};
  DiscretizationHDG<PDEClass> disc( pde, Local, Gradient );

  // integrands
  BoundaryIntegrandClass fcnbc( pde, bc, BoundaryGroups, disc );
  // base interface

  rsdPDEGlobal = 0;
  rsdAuGlobal = 0;
  rsdBCGlobal = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc, rsdPDEGlobal, rsdAuGlobal, rsdBCGlobal),
                                             xfld, (qfld, afld), qIfld, quadratureOrder, 3 );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDEGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[2], rsdPDEGlobal[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdAuxTrue[0], rsdAuGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAuyTrue[0], rsdAuGlobal[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdAuxTrue[1], rsdAuGlobal[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAuyTrue[1], rsdAuGlobal[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdAuxTrue[2], rsdAuGlobal[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAuyTrue[2], rsdAuGlobal[5], small_tol, close_tol );

  SANS_CHECK_CLOSE(      0, rsdBCGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE(      0, rsdBCGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDEITrue[0], rsdBCGlobal[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDEITrue[1], rsdBCGlobal[3], small_tol, close_tol );
  SANS_CHECK_CLOSE(      0, rsdBCGlobal[4], small_tol, close_tol );
  SANS_CHECK_CLOSE(      0, rsdBCGlobal[5], small_tol, close_tol );

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
