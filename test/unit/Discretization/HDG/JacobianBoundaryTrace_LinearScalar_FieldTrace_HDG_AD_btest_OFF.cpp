// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianBoundaryTrace_HDG_AD_btest
// testing of HDG boundary trace-integral jacobian: advection-diffusion

#include <boost/mpl/list.hpp>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_Trace.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_mitLG_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_sansLG_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_FieldTrace_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_FieldTrace_HDG.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"

#include "Surreal/SurrealS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct
namespace           // local definition
{

//----------------------------------------------------------------------------//
// jacobian dumps
template <int N>
void
dump( Real jac[][N], int m, int n )
{
  cout << "{";
  for (int i = 0; i < m; i++)
  {
    cout << "{";
    for (int j = 0; j < n; j++)
    {
      cout << jac[i][j];
      if (j < n-1) cout << ", ";
    }
    cout << "}";
    if (i < m-1) cout << ", ";
  }
  cout << "}" << std::endl;
}

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianBoundaryTrace_LinearScalar_mitLG_HDG_AD_test_suite )

typedef boost::mpl::list< SurrealS<1>, SurrealS<2> > Surreals;


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_mitLG_1D_1Line_X1Q0_Surreal1 )
{
  typedef SurrealS<1> SurrealClass;

  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> BoundaryIntegrandClass;

  Real u = 1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // BC

  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  // grid: single triangle, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P0 (aka Q0)
  int qorder = 0;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK( qfld.nDOF() == 1 );

  // line solution data (left)
  qfld.DOF(0) = 3;

  // auxiliary variable
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK( afld.nDOF()*PhysD1::D == 1 );

  // line auxiliary variable (left)
  afld.DOF(0) = { 2};

  // Trace field
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK( qIfld.nDOF() == 2 );

  qIfld.DOF(0) =  6;
  qIfld.DOF(1) =  7;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK( lgfld.nDOF() == 2 );

  lgfld.DOF(0) =  0;
  lgfld.DOF(1) =  5;

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder[2] = {0, 0};

  // integrand

  DiscretizationHDG<PDEClass> disc( pde, Global, AugGradient );
  const std::vector<int> BoundaryGroups = {1};
  BoundaryIntegrandClass fcnbc( pde, bc, BoundaryGroups, disc );

  // jacobian via FD w/ residual operator; assumes scalar PDE

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(1), rsdPDEGlobal1(1);
  SLA::SparseVector<ArrayQ> rsdAuGlobal0(1) , rsdAuGlobal1(1);
  SLA::SparseVector<ArrayQ> rsdIntGlobal0(2), rsdIntGlobal1(2);
  SLA::SparseVector<ArrayQ> rsdBCGlobal0(2) , rsdBCGlobal1(2);
  Real jacPDE_q[1][1] = {{0}}    , jacPDE_a[1][1] = {{0}}    , jacPDE_qI[1][2] = {{0,0}}      , jacPDE_lg[1][2] = {{0,0}};
  Real jacAu_q[1][1]  = {{0}}    , jacAu_a[1][1]  = {{0}}    , jacAu_qI[1][2]  = {{0,0}}      , jacAu_lg[1][2]  = {{0,0}};
  Real jacInt_q[1][1] = {{0}}    , jacInt_a[1][1] = {{0}}    , jacInt_qI[1][2] = {{0,0}}      , jacInt_lg[1][2] = {{0,0}};
  Real jacBC_q[2][1]  = {{0},{0}}, jacBC_a[2][1]  = {{0},{0}}, jacBC_qI[2][2]  = {{0,0},{0,0}}, jacBC_lg[2][2]  = {{0,0},{0,0}};

  rsdPDEGlobal0 = 0;
  rsdAuGlobal0 = 0;
  rsdIntGlobal0 = 0;
  rsdBCGlobal0 = 0;
  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                              rsdPDEGlobal0, rsdAuGlobal0, rsdIntGlobal0, rsdBCGlobal0),
                                                              xfld, (qfld, afld), (qIfld, lgfld), quadratureOrder, 2 );

  for (int i = 0; i < PhysD1::D*afld.nDOF(); i++)
    for (int j = 0; j < PhysD1::D*afld.nDOF(); j++)
      jacAu_a[i][j] = 0; //Suppress clang analyzer warning


  // wrt u
  for (int j = 0; j < qfld.nDOF(); j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdAuGlobal1 = 0;
    rsdIntGlobal1 = 0;
    rsdBCGlobal1 = 0;
    IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                rsdPDEGlobal1, rsdAuGlobal1, rsdIntGlobal1, rsdBCGlobal1),
                                                                xfld, (qfld, afld), (qIfld, lgfld), quadratureOrder, 2 );


    qfld.DOF(j) -= 1;

    for (int i = 0; i < qfld.nDOF(); i++)
    {
      jacPDE_q[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
      for (int d = 0; d < PhysD1::D; d++)
        jacAu_q[PhysD1::D*i+d][j]  = rsdAuGlobal1[PhysD1::D*i+d]  - rsdAuGlobal0[PhysD1::D*i+d];
    }

    for (int i = 0; i < lgfld.nDOF(); i++)
      jacBC_q[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
  }

  for (int j = 0; j < afld.nDOF(); j++)
    for (int d = 0; d < PhysD1::D; d++)
    {
      afld.DOF(j)[d] += 1;

      rsdPDEGlobal1 = 0;
      rsdAuGlobal1 = 0;
      rsdIntGlobal1 = 0;
      rsdBCGlobal1 = 0;
      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                  rsdPDEGlobal1, rsdAuGlobal1, rsdIntGlobal1, rsdBCGlobal1),
                                                                  xfld, (qfld, afld), (qIfld, lgfld), quadratureOrder, 2 );

      afld.DOF(j)[d] -= 1;

      for (int i = 0; i < qfld.nDOF(); i++)
      {
        jacPDE_a[i][PhysD1::D*j+d] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
        for (int d = 0; d < PhysD1::D; d++)
          jacAu_a[PhysD1::D*i+d][PhysD1::D*j+d]  = rsdAuGlobal1[PhysD1::D*i+d]  - rsdAuGlobal0[PhysD1::D*i+d];
      }

      for (int i = 0; i < lgfld.nDOF(); i++)
        jacBC_a[i][PhysD1::D*j+d] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
    }

  for (int j = 0; j < lgfld.nDOF(); j++)
  {
    lgfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdAuGlobal1 = 0;
    rsdIntGlobal1 = 0;
    rsdBCGlobal1 = 0;
    IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                rsdPDEGlobal1, rsdAuGlobal1, rsdIntGlobal1, rsdBCGlobal1),
                                                                xfld, (qfld, afld), (qIfld, lgfld), quadratureOrder, 2 );

    lgfld.DOF(j) -= 1;

    for (int i = 0; i < qfld.nDOF(); i++)
    {
      jacPDE_lg[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];

      for (int d = 0; d < PhysD1::D; d++)
        jacAu_lg[PhysD1::D*i+d][j]  = rsdAuGlobal1[PhysD1::D*i+d]  - rsdAuGlobal0[PhysD1::D*i+d];
    }

    for (int i = 0; i < lgfld.nDOF(); i++)
      jacBC_lg[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
  }

  #if 0
    std::cout << "jacPDE_q = " << std::endl; dump(jacPDE_q,1,1);
    std::cout << "jacPDE_a = " << std::endl; dump(jacPDE_a,1,1);
    std::cout << "jacPDE_lg = " << std::endl; dump(jacPDE_lg,1,2);
    std::cout << "jacAu_q = " << std::endl; dump(jacAu_q,1,1);
    std::cout << "jacAu_a = " << std::endl; dump(jacAu_a,1,1);
    std::cout << "jacAu_lg = " << std::endl; dump(jacAu_lg,1,2);
    std::cout << "jacBC_q = " << std::endl; dump(jacBC_q,2,1);
    std::cout << "jacBC_a = " << std::endl; dump(jacBC_a,2,1);
    std::cout << "jacBC_lg = " << std::endl; dump(jacBC_lg,2,2);
  #endif

  // jacobian via Surreal

  DLA::MatrixD<MatrixQ> mtxPDEGlob_q(1,1), mtxPDEGlob_a(1,1), mtxPDEGlob_qI(1,2), mtxPDEGlob_lg(1,2);
  DLA::MatrixD<MatrixQ> mtxAuGlob_q(1,1) , mtxAuGlob_a(1,1) , mtxAuGlob_qI(1,2) , mtxAuGlob_lg(1,2);
  DLA::MatrixD<MatrixQ> mtxIntGlob_q(1,1), mtxIntGlob_a(1,1), mtxIntGlob_qI(1,2), mtxIntGlob_lg(1,2);
  DLA::MatrixD<MatrixQ> mtxBCGlob_q(2,1) , mtxBCGlob_a(2,1) , mtxBCGlob_qI(2,2) , mtxBCGlob_lg(2,2);

  mtxPDEGlob_q = 0; mtxPDEGlob_a = 0; mtxPDEGlob_qI = 0; mtxPDEGlob_lg = 0;
  mtxAuGlob_q  = 0; mtxAuGlob_a  = 0; mtxAuGlob_qI  = 0; mtxAuGlob_lg  = 0;
  mtxIntGlob_q = 0; mtxIntGlob_a = 0; mtxIntGlob_qI = 0; mtxIntGlob_lg = 0;
  mtxBCGlob_q  = 0; mtxBCGlob_a  = 0; mtxBCGlob_qI  = 0; mtxBCGlob_lg  = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
      JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnbc,
                                                         mtxPDEGlob_q, mtxPDEGlob_a, mtxPDEGlob_qI, mtxPDEGlob_lg,
                                                         mtxAuGlob_q , mtxAuGlob_a , mtxAuGlob_qI , mtxAuGlob_lg,
                                                         mtxIntGlob_q, mtxIntGlob_a, mtxIntGlob_qI, mtxIntGlob_lg,
                                                         mtxBCGlob_q , mtxBCGlob_a , mtxBCGlob_qI , mtxBCGlob_lg),
      xfld, (qfld, afld), (qIfld, lgfld), quadratureOrder, 2);

  #if 0
    std::cout << "mtxPDEGlob_q " << std::endl; mtxPDEGlob_q.dump();
    std::cout << "mtxPDEGlob_a " << std::endl; mtxPDEGlob_a.dump();
    std::cout << "mtxPDEGlob_lg " << std::endl; mtxPDEGlob_lg.dump();
    std::cout << "mtxAuGlob_q " << std::endl; mtxAuGlob_q.dump();
    std::cout << "mtxAuGlob_a " << std::endl; mtxAuGlob_a.dump();
    std::cout << "mtxAuGlob_lg " << std::endl; mtxAuGlob_lg.dump();
    std::cout << "mtxBCGlob_q " << std::endl; mtxBCGlob_q.dump();
    std::cout << "mtxBCGlob_a " << std::endl; mtxBCGlob_a.dump();
    std::cout << "mtxBCGlob_lg " << std::endl; mtxBCGlob_lg.dump();
  #endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_q[i][j], mtxPDEGlob_q(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD1::D*afld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_q[i][j], mtxAuGlob_q(i,j), small_tol, close_tol );

  for (int i = 0; i < qIfld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacInt_q[i][j], mtxIntGlob_q(i,j), small_tol, close_tol );

  for (int i = 0; i < lgfld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_q[i][j], mtxBCGlob_q(i,j), small_tol, close_tol );


  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < PhysD1::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_a[i][j], mtxPDEGlob_a(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD1::D*afld.nDOF(); i++)
    for (int j = 0; j < PhysD1::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_a[i][j], mtxAuGlob_a(i,j), small_tol, close_tol );

  for (int i = 0; i < qIfld.nDOF(); i++)
    for (int j = 0; j < afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacInt_a[i][j], mtxIntGlob_a(i,j), small_tol, close_tol );

  for (int i = 0; i < lgfld.nDOF(); i++)
    for (int j = 0; j < PhysD1::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_a[i][j], mtxBCGlob_a(i,j), small_tol, close_tol );


  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < qIfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_qI[i][j], mtxPDEGlob_qI(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD1::D*afld.nDOF(); i++)
    for (int j = 0; j < qIfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_qI[i][j], mtxAuGlob_qI(i,j), small_tol, close_tol );

  for (int i = 0; i < qIfld.nDOF(); i++)
    for (int j = 0; j < lgfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacInt_qI[i][j], mtxIntGlob_qI(i,j), small_tol, close_tol );

  for (int i = 0; i < lgfld.nDOF(); i++)
    for (int j = 0; j < lgfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_qI[i][j], mtxBCGlob_qI(i,j), small_tol, close_tol );


  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < lgfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_lg[i][j], mtxPDEGlob_lg(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD1::D*afld.nDOF(); i++)
    for (int j = 0; j < lgfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_lg[i][j], mtxAuGlob_lg(i,j), small_tol, close_tol );

  for (int i = 0; i < qIfld.nDOF(); i++)
    for (int j = 0; j < lgfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacInt_lg[i][j], mtxIntGlob_lg(i,j), small_tol, close_tol );

  for (int i = 0; i < lgfld.nDOF(); i++)
    for (int j = 0; j < lgfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_lg[i][j], mtxBCGlob_lg(i,j), small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_FieldTrace_sansLG_1D_1Line_X1Q0_Surreal1 )
{
  typedef SurrealS<1> SurrealClass;

  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_sansLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> BoundaryIntegrandClass;

  Real u = 1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // BC

  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  // grid: single triangle, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P0 (aka Q0)
  int qorder = 0;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK( qfld.nDOF() == 1 );

  // line solution data (left)
  qfld.DOF(0) = 3;

  // auxiliary variable
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK( afld.nDOF()*PhysD1::D == 1 );

  // line auxiliary variable (left)
  afld.DOF(0) = { 2};

  // Lagrange multiplier
  Field_DG_Trace<PhysD1, TopoD1, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK( qIfld.nDOF() == 2 );

  qIfld.DOF(0) =  0;
  qIfld.DOF(1) =  5;

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder[2] = {0, 0};

  // integrand

  DiscretizationHDG<PDEClass> disc( pde, Global, AugGradient );
  const std::vector<int> BoundaryGroups = {1};
  BoundaryIntegrandClass fcnbc( pde, bc, BoundaryGroups, disc );

  // jacobian via FD w/ residual operator; assumes scalar PDE

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(1), rsdPDEGlobal1(1);
  SLA::SparseVector<ArrayQ> rsdAuGlobal0(1) , rsdAuGlobal1(1);
  SLA::SparseVector<ArrayQ> rsdBCGlobal0(2) , rsdBCGlobal1(2);
  Real jacPDE_q[1][1] = {{0}}    , jacPDE_a[1][1] = {{0}}    , jacPDE_lg[1][2] = {{0,0}};
  Real jacAu_q[1][1]  = {{0}}    , jacAu_a[1][1]  = {{0}}    , jacAu_lg[1][2]  = {{0,0}};
  Real jacBC_q[2][1]  = {{0},{0}}, jacBC_a[2][1]  = {{0},{0}}, jacBC_lg[2][2]  = {{0,0},{0,0}};

  rsdPDEGlobal0 = 0;
  rsdAuGlobal0 = 0;
  rsdBCGlobal0 = 0;
  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc, rsdPDEGlobal0, rsdAuGlobal0, rsdBCGlobal0),
                                                      xfld, (qfld, afld), qIfld, quadratureOrder, 2 );

  for (int i = 0; i < PhysD1::D*afld.nDOF(); i++)
    for (int j = 0; j < PhysD1::D*afld.nDOF(); j++)
      jacAu_a[i][j] = 0; //Suppress clang analyzer warning


  // wrt u
  for (int j = 0; j < qfld.nDOF(); j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdAuGlobal1 = 0;
    rsdBCGlobal1 = 0;
    IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                rsdPDEGlobal1, rsdAuGlobal1, rsdBCGlobal1),
                                                                xfld, (qfld, afld), qIfld, quadratureOrder, 2 );


    qfld.DOF(j) -= 1;

    for (int i = 0; i < qfld.nDOF(); i++)
    {
      jacPDE_q[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
      for (int d = 0; d < PhysD1::D; d++)
        jacAu_q[PhysD1::D*i+d][j]  = rsdAuGlobal1[PhysD1::D*i+d]  - rsdAuGlobal0[PhysD1::D*i+d];
    }

    for (int i = 0; i < qIfld.nDOF(); i++)
      jacBC_q[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
  }

  for (int j = 0; j < afld.nDOF(); j++)
    for (int d = 0; d < PhysD1::D; d++)
    {
      afld.DOF(j)[d] += 1;

      rsdPDEGlobal1 = 0;
      rsdAuGlobal1 = 0;
      rsdBCGlobal1 = 0;
      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                  rsdPDEGlobal1, rsdAuGlobal1, rsdBCGlobal1),
                                                                  xfld, (qfld, afld), qIfld, quadratureOrder, 2 );

      afld.DOF(j)[d] -= 1;

      for (int i = 0; i < qfld.nDOF(); i++)
      {
        jacPDE_a[i][PhysD1::D*j+d] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
        for (int d = 0; d < PhysD1::D; d++)
          jacAu_a[PhysD1::D*i+d][PhysD1::D*j+d]  = rsdAuGlobal1[PhysD1::D*i+d]  - rsdAuGlobal0[PhysD1::D*i+d];
      }

      for (int i = 0; i < qIfld.nDOF(); i++)
        jacBC_a[i][PhysD1::D*j+d] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
    }

  for (int j = 0; j < qIfld.nDOF(); j++)
  {
    qIfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdAuGlobal1 = 0;
    rsdBCGlobal1 = 0;
    IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                rsdPDEGlobal1, rsdAuGlobal1, rsdBCGlobal1),
                                                                xfld, (qfld, afld), qIfld, quadratureOrder, 2 );

    qIfld.DOF(j) -= 1;

    for (int i = 0; i < qfld.nDOF(); i++)
    {
      jacPDE_lg[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];

      for (int d = 0; d < PhysD1::D; d++)
        jacAu_lg[PhysD1::D*i+d][j]  = rsdAuGlobal1[PhysD1::D*i+d]  - rsdAuGlobal0[PhysD1::D*i+d];
    }

    for (int i = 0; i < qIfld.nDOF(); i++)
      jacBC_lg[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
  }

  #if 0
    std::cout << "jacPDE_q = " << std::endl; dump(jacPDE_q,1,1);
    std::cout << "jacPDE_a = " << std::endl; dump(jacPDE_a,1,1);
    std::cout << "jacPDE_lg = " << std::endl; dump(jacPDE_lg,1,2);
    std::cout << "jacAu_q = " << std::endl; dump(jacAu_q,1,1);
    std::cout << "jacAu_a = " << std::endl; dump(jacAu_a,1,1);
    std::cout << "jacAu_lg = " << std::endl; dump(jacAu_lg,1,2);
    std::cout << "jacBC_q = " << std::endl; dump(jacBC_q,2,1);
    std::cout << "jacBC_a = " << std::endl; dump(jacBC_a,2,1);
    std::cout << "jacBC_lg = " << std::endl; dump(jacBC_lg,2,2);
  #endif

  // jacobian via Surreal

  DLA::MatrixD<MatrixQ> mtxPDEGlob_q(1,1), mtxPDEGlob_a(1,1), mtxPDEGlob_lg(1,2);
  DLA::MatrixD<MatrixQ> mtxAuGlob_q(1,1) , mtxAuGlob_a(1,1) , mtxAuGlob_lg(1,2);
  DLA::MatrixD<MatrixQ> mtxBCGlob_q(2,1) , mtxBCGlob_a(2,1) , mtxBCGlob_lg(2,2);

  mtxPDEGlob_q = 0; mtxPDEGlob_a = 0; mtxPDEGlob_lg = 0;
  mtxAuGlob_q  = 0; mtxAuGlob_a  = 0; mtxAuGlob_lg  = 0;
  mtxBCGlob_q  = 0; mtxBCGlob_a  = 0; mtxBCGlob_lg  = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
      JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnbc, mtxPDEGlob_q, mtxPDEGlob_a, mtxPDEGlob_lg,
                                                        mtxAuGlob_q , mtxAuGlob_a , mtxAuGlob_lg ,
                                                        mtxBCGlob_q , mtxBCGlob_a , mtxBCGlob_lg),
      xfld, (qfld, afld), qIfld, quadratureOrder, 2);

  #if 0
    std::cout << "mtxPDEGlob_q " << std::endl; mtxPDEGlob_q.dump();
    std::cout << "mtxPDEGlob_a " << std::endl; mtxPDEGlob_a.dump();
    std::cout << "mtxPDEGlob_lg " << std::endl; mtxPDEGlob_lg.dump();
    std::cout << "mtxAuGlob_q " << std::endl; mtxAuGlob_q.dump();
    std::cout << "mtxAuGlob_a " << std::endl; mtxAuGlob_a.dump();
    std::cout << "mtxAuGlob_lg " << std::endl; mtxAuGlob_lg.dump();
    std::cout << "mtxBCGlob_q " << std::endl; mtxBCGlob_q.dump();
    std::cout << "mtxBCGlob_a " << std::endl; mtxBCGlob_a.dump();
    std::cout << "mtxBCGlob_lg " << std::endl; mtxBCGlob_lg.dump();
  #endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_q[i][j], mtxPDEGlob_q(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD1::D*afld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_q[i][j], mtxAuGlob_q(i,j), small_tol, close_tol );

  for (int i = 0; i < qIfld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_q[i][j], mtxBCGlob_q(i,j), small_tol, close_tol );


  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < PhysD1::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_a[i][j], mtxPDEGlob_a(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD1::D*afld.nDOF(); i++)
    for (int j = 0; j < PhysD1::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_a[i][j], mtxAuGlob_a(i,j), small_tol, close_tol );

  for (int i = 0; i < qIfld.nDOF(); i++)
    for (int j = 0; j < PhysD1::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_a[i][j], mtxBCGlob_a(i,j), small_tol, close_tol );


  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < qIfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_lg[i][j], mtxPDEGlob_lg(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD1::D*afld.nDOF(); i++)
    for (int j = 0; j < qIfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_lg[i][j], mtxAuGlob_lg(i,j), small_tol, close_tol );

  for (int i = 0; i < qIfld.nDOF(); i++)
    for (int j = 0; j < qIfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_lg[i][j], mtxBCGlob_lg(i,j), small_tol, close_tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( HDG_mitLG_1D_1Line_X1Q1_Surreal, SurrealClass, Surreals )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> BoundaryIntegrandClass;

  Real u = 1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // BC

  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  // grid: single triangle, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK( qfld.nDOF() == 2 );

  // line solution data (left)
  qfld.DOF(0) = 3;
  qfld.DOF(1) = 4;

  // auxiliary variable
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK( afld.nDOF()*PhysD1::D == 2 );

  // line auxiliary variable (left)
  afld.DOF(0) = { 2};
  afld.DOF(1) = { 7};

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK( lgfld.nDOF() == 2 );

  lgfld.DOF(0) =  0;
  lgfld.DOF(1) =  5;

  const int nDOFPDE = qfld.nDOF();
  const int nDOFAu  = afld.nDOF();
  const int nDOFBC  = lgfld.nDOF();

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder[2] = {0, 0};

  // integrand
  DiscretizationHDG<PDEClass> disc( pde, Global, AugGradient );
  const std::vector<int> BoundaryGroups = {1};
  BoundaryIntegrandClass fcnbc( pde, bc, BoundaryGroups, disc );

  // jacobian via FD w/ residual operator; assumes scalar PDE

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(2), rsdPDEGlobal1(2);
  SLA::SparseVector<ArrayQ> rsdAuGlobal0(2) , rsdAuGlobal1(2);
  SLA::SparseVector<ArrayQ> rsdBCGlobal0(2) , rsdBCGlobal1(2);
  Real jacPDE_q[2][2], jacPDE_a[2][2], jacPDE_lg[2][2];
  Real jacAu_q[2][2] , jacAu_a[2][2] , jacAu_lg[2][2];
  Real jacBC_q[2][2] , jacBC_a[2][2] , jacBC_lg[2][2];

  rsdPDEGlobal0 = 0;
  rsdAuGlobal0 = 0;
  rsdBCGlobal0 = 0;
  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc, rsdPDEGlobal0, rsdAuGlobal0, rsdBCGlobal0),
                                                      xfld, (qfld, afld), lgfld, quadratureOrder, 2 );

  // suppressing clang analyzer warnings
  for (int i = 0; i < 2; i++)
  {
    for (int j = 0; j < 2; j++)
    {
      jacPDE_q[i][j] = 0;
      jacPDE_a[i][j] = 0;
      jacPDE_lg[i][j] = 0;
      jacAu_q[i][j] = 0;
      jacAu_a[i][j] = 0;
      jacAu_lg[i][j] = 0;
      jacBC_q[i][j] = 0;
      jacBC_a[i][j] = 0;
      jacBC_lg[i][j] = 0;
    }
  }

  // wrt u
  for (int j = 0; j < qfld.nDOF(); j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdAuGlobal1 = 0;
    rsdBCGlobal1 = 0;
    IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                rsdPDEGlobal1, rsdAuGlobal1, rsdBCGlobal1),
                                                                xfld, (qfld, afld), lgfld, quadratureOrder, 2 );

    qfld.DOF(j) -= 1;

    for (int i = 0; i < qfld.nDOF(); i++)
    {
      jacPDE_q[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
      for (int d = 0; d < PhysD1::D; d++)
        jacAu_q[PhysD1::D*i+d][j]  = rsdAuGlobal1[PhysD1::D*i+d]  - rsdAuGlobal0[PhysD1::D*i+d];
    }

    for (int i = 0; i < lgfld.nDOF(); i++)
      jacBC_q[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
  }

  for (int j = 0; j < afld.nDOF(); j++)
    for (int d = 0; d < PhysD1::D; d++)
    {
      afld.DOF(j)[d] += 1;

      rsdPDEGlobal1 = 0;
      rsdAuGlobal1 = 0;
      rsdBCGlobal1 = 0;
      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                  rsdPDEGlobal1, rsdAuGlobal1, rsdBCGlobal1),
                                                                  xfld, (qfld, afld), lgfld, quadratureOrder, 2 );

      afld.DOF(j)[d] -= 1;

      for (int i = 0; i < qfld.nDOF(); i++)
      {
        jacPDE_a[i][PhysD1::D*j+d] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
        for (int d = 0; d < PhysD1::D; d++)
          jacAu_a[PhysD1::D*i+d][PhysD1::D*j+d]  = rsdAuGlobal1[PhysD1::D*i+d]  - rsdAuGlobal0[PhysD1::D*i+d];
      }

      for (int i = 0; i < lgfld.nDOF(); i++)
        jacBC_a[i][PhysD1::D*j+d] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
    }

  for (int j = 0; j < lgfld.nDOF(); j++)
  {
    lgfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdAuGlobal1 = 0;
    rsdBCGlobal1 = 0;
    IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                rsdPDEGlobal1, rsdAuGlobal1, rsdBCGlobal1),
                                                                xfld, (qfld, afld), lgfld, quadratureOrder, 2 );

    lgfld.DOF(j) -= 1;

    for (int i = 0; i < qfld.nDOF(); i++)
    {
      jacPDE_lg[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];

      for (int d = 0; d < PhysD1::D; d++)
        jacAu_lg[PhysD1::D*i+d][j]  = rsdAuGlobal1[PhysD1::D*i+d]  - rsdAuGlobal0[PhysD1::D*i+d];
    }

    for (int i = 0; i < lgfld.nDOF(); i++)
      jacBC_lg[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
  }

  #if 0
    std::cout << "jacPDE_q = " << std::endl; dump(jacPDE_q,1,1);
    std::cout << "jacPDE_a = " << std::endl; dump(jacPDE_a,1,2);
    std::cout << "jacPDE_lg = " << std::endl; dump(jacPDE_lg,1,3);
    std::cout << "jacAu_q = " << std::endl; dump(jacAu_q,2,1);
    std::cout << "jacAu_a = " << std::endl; dump(jacAu_a,2,2);
    std::cout << "jacAu_lg = " << std::endl; dump(jacAu_lg,2,3);
    std::cout << "jacBC_q = " << std::endl; dump(jacBC_q,3,1);
    std::cout << "jacBC_a = " << std::endl; dump(jacBC_a,3,2);
    std::cout << "jacBC_lg = " << std::endl; dump(jacBC_lg,3,3);
  #endif

  // jacobian via Surreal

  DLA::MatrixD<MatrixQ> mtxPDEGlob_q(nDOFPDE,nDOFPDE), mtxPDEGlob_a(nDOFPDE,nDOFAu), mtxPDEGlob_lg(nDOFPDE,nDOFBC);
  DLA::MatrixD<MatrixQ> mtxAuGlob_q(nDOFAu,nDOFPDE) , mtxAuGlob_a(nDOFAu,nDOFAu) , mtxAuGlob_lg(nDOFAu,nDOFBC);
  DLA::MatrixD<MatrixQ> mtxBCGlob_q(nDOFBC,nDOFPDE) , mtxBCGlob_a(nDOFBC,nDOFAu) , mtxBCGlob_lg(nDOFBC,nDOFBC);

  mtxPDEGlob_q = 0; mtxPDEGlob_a = 0; mtxPDEGlob_lg = 0;
  mtxAuGlob_q  = 0; mtxAuGlob_a  = 0; mtxAuGlob_lg  = 0;
  mtxBCGlob_q  = 0; mtxBCGlob_a  = 0; mtxBCGlob_lg  = 0;

  BOOST_CHECK( lgfld.nDOF() == 2 );

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
      JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnbc, mtxPDEGlob_q, mtxPDEGlob_a, mtxPDEGlob_lg,
                                                        mtxAuGlob_q , mtxAuGlob_a , mtxAuGlob_lg ,
                                                        mtxBCGlob_q , mtxBCGlob_a , mtxBCGlob_lg),
      xfld, (qfld, afld), lgfld, quadratureOrder, 2);

  #if 0
    std::cout << "mtxPDEGlob_q " << std::endl; mtxPDEGlob_q.dump();
    std::cout << "mtxPDEGlob_a " << std::endl; mtxPDEGlob_a.dump();
    std::cout << "mtxPDEGlob_lg " << std::endl; mtxPDEGlob_lg.dump();
    std::cout << "mtxAuGlob_q " << std::endl; mtxAuGlob_q.dump();
    std::cout << "mtxAuGlob_a " << std::endl; mtxAuGlob_a.dump();
    std::cout << "mtxAuGlob_lg " << std::endl; mtxAuGlob_lg.dump();
    std::cout << "mtxBCGlob_q " << std::endl; mtxBCGlob_q.dump();
    std::cout << "mtxBCGlob_a " << std::endl; mtxBCGlob_a.dump();
    std::cout << "mtxBCGlob_lg " << std::endl; mtxBCGlob_lg.dump();
  #endif

  const Real small_tol = 1e-12;
  const Real close_tol = 3e-12;

  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_q[i][j], mtxPDEGlob_q(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD1::D*afld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_q[i][j], mtxAuGlob_q(i,j), small_tol, close_tol );

  for (int i = 0; i < lgfld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_q[i][j], mtxBCGlob_q(i,j), small_tol, close_tol );


  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < PhysD1::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_a[i][j], mtxPDEGlob_a(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD1::D*afld.nDOF(); i++)
    for (int j = 0; j < PhysD1::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_a[i][j], mtxAuGlob_a(i,j), small_tol, close_tol );

  for (int i = 0; i < lgfld.nDOF(); i++)
    for (int j = 0; j < PhysD1::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_a[i][j], mtxBCGlob_a(i,j), small_tol, close_tol );


  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < lgfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_lg[i][j], mtxPDEGlob_lg(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD1::D*afld.nDOF(); i++)
    for (int j = 0; j < lgfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_lg[i][j], mtxAuGlob_lg(i,j), small_tol, close_tol );

  for (int i = 0; i < lgfld.nDOF(); i++)
    for (int j = 0; j < lgfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_lg[i][j], mtxBCGlob_lg(i,j), small_tol, close_tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( HDG_FieldTrace_sansLG_1D_1Line_X1Q1_Surreal, SurrealClass, Surreals )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_sansLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> BoundaryIntegrandClass;

  Real u = 1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // BC

  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  // grid: single triangle, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK( qfld.nDOF() == 2 );

  // line solution data (left)
  qfld.DOF(0) = 3;
  qfld.DOF(1) = 4;

  // auxiliary variable
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK( afld.nDOF()*PhysD1::D == 2 );

  // line auxiliary variable (left)
  afld.DOF(0) = { 2};
  afld.DOF(1) = { 7};

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK( qIfld.nDOF() == 2 );

  qIfld.DOF(0) =  0;
  qIfld.DOF(1) =  5;

  const int nDOFPDE = qfld.nDOF();
  const int nDOFAu  = afld.nDOF();
  const int nDOFBC  = qIfld.nDOF();

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder[2] = {0, 0};

  // integrand
  DiscretizationHDG<PDEClass> disc( pde, Global, AugGradient );
  const std::vector<int> BoundaryGroups = {1};
  BoundaryIntegrandClass fcnbc( pde, bc, BoundaryGroups, disc );

  // jacobian via FD w/ residual operator; assumes scalar PDE

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(2), rsdPDEGlobal1(2);
  SLA::SparseVector<ArrayQ> rsdAuGlobal0(2) , rsdAuGlobal1(2);
  SLA::SparseVector<ArrayQ> rsdBCGlobal0(2) , rsdBCGlobal1(2);
  Real jacPDE_q[2][2], jacPDE_a[2][2], jacPDE_lg[2][2];
  Real jacAu_q[2][2] , jacAu_a[2][2] , jacAu_lg[2][2];
  Real jacBC_q[2][2] , jacBC_a[2][2] , jacBC_lg[2][2];

  rsdPDEGlobal0 = 0;
  rsdAuGlobal0 = 0;
  rsdBCGlobal0 = 0;
  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc, rsdPDEGlobal0, rsdAuGlobal0, rsdBCGlobal0),
                                                      xfld, (qfld, afld), qIfld, quadratureOrder, 2 );

  // suppressing clang analyzer warnings
  for (int i = 0; i < 2; i++)
  {
    for (int j = 0; j < 2; j++)
    {
      jacPDE_q[i][j] = 0;
      jacPDE_a[i][j] = 0;
      jacPDE_lg[i][j] = 0;
      jacAu_q[i][j] = 0;
      jacAu_a[i][j] = 0;
      jacAu_lg[i][j] = 0;
      jacBC_q[i][j] = 0;
      jacBC_a[i][j] = 0;
      jacBC_lg[i][j] = 0;
    }
  }

  // wrt u
  for (int j = 0; j < qfld.nDOF(); j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdAuGlobal1 = 0;
    rsdBCGlobal1 = 0;
    IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                rsdPDEGlobal1, rsdAuGlobal1, rsdBCGlobal1),
                                                                xfld, (qfld, afld), qIfld, quadratureOrder, 2 );

    qfld.DOF(j) -= 1;

    for (int i = 0; i < qfld.nDOF(); i++)
    {
      jacPDE_q[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
      for (int d = 0; d < PhysD1::D; d++)
        jacAu_q[PhysD1::D*i+d][j]  = rsdAuGlobal1[PhysD1::D*i+d]  - rsdAuGlobal0[PhysD1::D*i+d];
    }

    for (int i = 0; i < qIfld.nDOF(); i++)
      jacBC_q[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
  }

  for (int j = 0; j < afld.nDOF(); j++)
    for (int d = 0; d < PhysD1::D; d++)
    {
      afld.DOF(j)[d] += 1;

      rsdPDEGlobal1 = 0;
      rsdAuGlobal1 = 0;
      rsdBCGlobal1 = 0;
      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                  rsdPDEGlobal1, rsdAuGlobal1, rsdBCGlobal1),
                                                                  xfld, (qfld, afld), qIfld, quadratureOrder, 2 );

      afld.DOF(j)[d] -= 1;

      for (int i = 0; i < qfld.nDOF(); i++)
      {
        jacPDE_a[i][PhysD1::D*j+d] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
        for (int d = 0; d < PhysD1::D; d++)
          jacAu_a[PhysD1::D*i+d][PhysD1::D*j+d]  = rsdAuGlobal1[PhysD1::D*i+d]  - rsdAuGlobal0[PhysD1::D*i+d];
      }

      for (int i = 0; i < qIfld.nDOF(); i++)
        jacBC_a[i][PhysD1::D*j+d] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
    }

  for (int j = 0; j < qIfld.nDOF(); j++)
  {
    qIfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdAuGlobal1 = 0;
    rsdBCGlobal1 = 0;
    IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                rsdPDEGlobal1, rsdAuGlobal1, rsdBCGlobal1),
                                                                xfld, (qfld, afld), qIfld, quadratureOrder, 2 );

    qIfld.DOF(j) -= 1;

    for (int i = 0; i < qfld.nDOF(); i++)
    {
      jacPDE_lg[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];

      for (int d = 0; d < PhysD1::D; d++)
        jacAu_lg[PhysD1::D*i+d][j]  = rsdAuGlobal1[PhysD1::D*i+d]  - rsdAuGlobal0[PhysD1::D*i+d];
    }

    for (int i = 0; i < qIfld.nDOF(); i++)
      jacBC_lg[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
  }

  #if 0
    std::cout << "jacPDE_q = " << std::endl; dump(jacPDE_q,1,1);
    std::cout << "jacPDE_a = " << std::endl; dump(jacPDE_a,1,2);
    std::cout << "jacPDE_lg = " << std::endl; dump(jacPDE_lg,1,3);
    std::cout << "jacAu_q = " << std::endl; dump(jacAu_q,2,1);
    std::cout << "jacAu_a = " << std::endl; dump(jacAu_a,2,2);
    std::cout << "jacAu_lg = " << std::endl; dump(jacAu_lg,2,3);
    std::cout << "jacBC_q = " << std::endl; dump(jacBC_q,3,1);
    std::cout << "jacBC_a = " << std::endl; dump(jacBC_a,3,2);
    std::cout << "jacBC_lg = " << std::endl; dump(jacBC_lg,3,3);
  #endif

  // jacobian via Surreal

  DLA::MatrixD<MatrixQ> mtxPDEGlob_q(nDOFPDE,nDOFPDE), mtxPDEGlob_a(nDOFPDE,nDOFAu), mtxPDEGlob_lg(nDOFPDE,nDOFBC);
  DLA::MatrixD<MatrixQ> mtxAuGlob_q(nDOFAu,nDOFPDE) , mtxAuGlob_a(nDOFAu,nDOFAu) , mtxAuGlob_lg(nDOFAu,nDOFBC);
  DLA::MatrixD<MatrixQ> mtxBCGlob_q(nDOFBC,nDOFPDE) , mtxBCGlob_a(nDOFBC,nDOFAu) , mtxBCGlob_lg(nDOFBC,nDOFBC);

  mtxPDEGlob_q = 0; mtxPDEGlob_a = 0; mtxPDEGlob_lg = 0;
  mtxAuGlob_q  = 0; mtxAuGlob_a  = 0; mtxAuGlob_lg  = 0;
  mtxBCGlob_q  = 0; mtxBCGlob_a  = 0; mtxBCGlob_lg  = 0;

  BOOST_CHECK( qIfld.nDOF() == 2 );

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
      JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnbc, mtxPDEGlob_q, mtxPDEGlob_a, mtxPDEGlob_lg,
                                                        mtxAuGlob_q , mtxAuGlob_a , mtxAuGlob_lg ,
                                                        mtxBCGlob_q , mtxBCGlob_a , mtxBCGlob_lg),
      xfld, (qfld, afld), qIfld, quadratureOrder, 2);

  #if 0
    std::cout << "mtxPDEGlob_q " << std::endl; mtxPDEGlob_q.dump();
    std::cout << "mtxPDEGlob_a " << std::endl; mtxPDEGlob_a.dump();
    std::cout << "mtxPDEGlob_lg " << std::endl; mtxPDEGlob_lg.dump();
    std::cout << "mtxAuGlob_q " << std::endl; mtxAuGlob_q.dump();
    std::cout << "mtxAuGlob_a " << std::endl; mtxAuGlob_a.dump();
    std::cout << "mtxAuGlob_lg " << std::endl; mtxAuGlob_lg.dump();
    std::cout << "mtxBCGlob_q " << std::endl; mtxBCGlob_q.dump();
    std::cout << "mtxBCGlob_a " << std::endl; mtxBCGlob_a.dump();
    std::cout << "mtxBCGlob_lg " << std::endl; mtxBCGlob_lg.dump();
  #endif

  const Real small_tol = 1e-12;
  const Real close_tol = 3e-12;

  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_q[i][j], mtxPDEGlob_q(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD1::D*afld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_q[i][j], mtxAuGlob_q(i,j), small_tol, close_tol );

  for (int i = 0; i < qIfld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_q[i][j], mtxBCGlob_q(i,j), small_tol, close_tol );


  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < PhysD1::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_a[i][j], mtxPDEGlob_a(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD1::D*afld.nDOF(); i++)
    for (int j = 0; j < PhysD1::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_a[i][j], mtxAuGlob_a(i,j), small_tol, close_tol );

  for (int i = 0; i < qIfld.nDOF(); i++)
    for (int j = 0; j < PhysD1::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_a[i][j], mtxBCGlob_a(i,j), small_tol, close_tol );


  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < qIfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_lg[i][j], mtxPDEGlob_lg(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD1::D*afld.nDOF(); i++)
    for (int j = 0; j < qIfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_lg[i][j], mtxAuGlob_lg(i,j), small_tol, close_tol );

  for (int i = 0; i < qIfld.nDOF(); i++)
    for (int j = 0; j < qIfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_lg[i][j], mtxBCGlob_lg(i,j), small_tol, close_tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_mitLG_2D_1Triangle_X1Q0_Surreal1 )
{
  typedef SurrealS<1> SurrealClass;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> BoundaryIntegrandClass;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // BC

  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P0 (aka Q0)
  int qorder = 0;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK( qfld.nDOF() == 1 );

  // triangle solution data (left)
  qfld.DOF(0) = 3;

  // auxiliary variable
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK( afld.nDOF()*PhysD2::D == 2 );

  // triangle auxiliary variable (left)
  afld.DOF(0) = { 2, -3};

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK( lgfld.nDOF() == 3 );

  lgfld.DOF(0) =  0;
  lgfld.DOF(1) =  5;
  lgfld.DOF(2) =  0;

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder[3] = {1, 1, 1};

  // integrand
  DiscretizationHDG<PDEClass> disc( pde, Global, AugGradient );
  const std::vector<int> BoundaryGroups = {1};
  BoundaryIntegrandClass fcnbc( pde, bc, BoundaryGroups, disc );

  // jacobian via FD w/ residual operator; assumes scalar PDE

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(1), rsdPDEGlobal1(1);
  SLA::SparseVector<ArrayQ> rsdAuGlobal0(2) , rsdAuGlobal1(2);
  SLA::SparseVector<ArrayQ> rsdBCGlobal0(3) , rsdBCGlobal1(3);
  Real jacPDE_q[1][1] = {{0}}        , jacPDE_a[1][2] = {{0,0}}            , jacPDE_lg[1][3] = {{0,0,0}};
  Real jacAu_q[2][1]  = {{0},{0}}    , jacAu_a[2][2]  = {{0,0},{0,0}}      , jacAu_lg[2][3]  = {{0,0,0},{0,0,0}};
  Real jacBC_q[3][1]  = {{0},{0},{0}}, jacBC_a[3][2]  = {{0,0},{0,0},{0,0}}, jacBC_lg[3][3]  = {{0,0,0},{0,0,0},{0,0,0}};

  rsdPDEGlobal0 = 0;
  rsdAuGlobal0 = 0;
  rsdBCGlobal0 = 0;
  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                              rsdPDEGlobal0, rsdAuGlobal0, rsdBCGlobal0),
                                                              xfld, (qfld, afld), lgfld, quadratureOrder, 3 );

  // wrt u
  for (int j = 0; j < qfld.nDOF(); j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdAuGlobal1 = 0;
    rsdBCGlobal1 = 0;
    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                rsdPDEGlobal1, rsdAuGlobal1, rsdBCGlobal1),
                                                                xfld, (qfld, afld), lgfld, quadratureOrder, 3 );

    qfld.DOF(j) -= 1;

    for (int i = 0; i < qfld.nDOF(); i++)
    {
      jacPDE_q[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
      for (int d = 0; d < PhysD2::D; d++)
        jacAu_q[PhysD2::D*i+d][j]  = rsdAuGlobal1[PhysD2::D*i+d]  - rsdAuGlobal0[PhysD2::D*i+d];
    }

    for (int i = 0; i < lgfld.nDOF(); i++)
      jacBC_q[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
  }

  for (int j = 0; j < afld.nDOF(); j++)
    for (int d = 0; d < PhysD2::D; d++)
    {
      afld.DOF(j)[d] += 1;

      rsdPDEGlobal1 = 0;
      rsdAuGlobal1 = 0;
      rsdBCGlobal1 = 0;
      IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                  rsdPDEGlobal1, rsdAuGlobal1, rsdBCGlobal1),
                                                                  xfld, (qfld, afld), lgfld, quadratureOrder, 3 );

      afld.DOF(j)[d] -= 1;

      for (int i = 0; i < qfld.nDOF(); i++)
      {
        jacPDE_a[i][PhysD2::D*j+d] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
        for (int d2 = 0; d2 < PhysD2::D; d2++)
          jacAu_a[PhysD2::D*i+d2][PhysD2::D*j+d]  = rsdAuGlobal1[PhysD2::D*i+d2]  - rsdAuGlobal0[PhysD2::D*i+d2];
      }

      for (int i = 0; i < lgfld.nDOF(); i++)
        jacBC_a[i][PhysD2::D*j+d] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
    }

  for (int j = 0; j < lgfld.nDOF(); j++)
  {
    lgfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdAuGlobal1 = 0;
    rsdBCGlobal1 = 0;
    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                rsdPDEGlobal1, rsdAuGlobal1, rsdBCGlobal1),
                                                                xfld, (qfld, afld), lgfld, quadratureOrder, 3 );

    lgfld.DOF(j) -= 1;

    for (int i = 0; i < qfld.nDOF(); i++)
    {
      jacPDE_lg[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];

      for (int d = 0; d < PhysD2::D; d++)
        jacAu_lg[PhysD2::D*i+d][j]  = rsdAuGlobal1[PhysD2::D*i+d]  - rsdAuGlobal0[PhysD2::D*i+d];
    }

    for (int i = 0; i < lgfld.nDOF(); i++)
      jacBC_lg[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
  }

  #if 0
    std::cout << "jacPDE_q = " << std::endl; dump(jacPDE_q,1,1);
    std::cout << "jacPDE_a = " << std::endl; dump(jacPDE_a,1,2);
    std::cout << "jacPDE_lg = " << std::endl; dump(jacPDE_lg,1,3);
    std::cout << "jacAu_q = " << std::endl; dump(jacAu_q,2,1);
    std::cout << "jacAu_a = " << std::endl; dump(jacAu_a,2,2);
    std::cout << "jacAu_lg = " << std::endl; dump(jacAu_lg,2,3);
    std::cout << "jacBC_q = " << std::endl; dump(jacBC_q,3,1);
    std::cout << "jacBC_a = " << std::endl; dump(jacBC_a,3,2);
    std::cout << "jacBC_lg = " << std::endl; dump(jacBC_lg,3,3);
  #endif

  // jacobian via Surreal

  DLA::MatrixD<MatrixQ> mtxPDEGlob_q(1,1), mtxPDEGlob_a(1,2), mtxPDEGlob_lg(1,3);
  DLA::MatrixD<MatrixQ> mtxAuGlob_q(2,1) , mtxAuGlob_a(2,2) , mtxAuGlob_lg(2,3);
  DLA::MatrixD<MatrixQ> mtxBCGlob_q(3,1) , mtxBCGlob_a(3,2) , mtxBCGlob_lg(3,3);

  mtxPDEGlob_q = 0; mtxPDEGlob_a = 0; mtxPDEGlob_lg = 0;
  mtxAuGlob_q  = 0; mtxAuGlob_a  = 0; mtxAuGlob_lg  = 0;
  mtxBCGlob_q  = 0; mtxBCGlob_a  = 0; mtxBCGlob_lg  = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnbc, mtxPDEGlob_q, mtxPDEGlob_a, mtxPDEGlob_lg,
                                                        mtxAuGlob_q , mtxAuGlob_a , mtxAuGlob_lg ,
                                                        mtxBCGlob_q , mtxBCGlob_a , mtxBCGlob_lg),
      xfld, (qfld, afld), lgfld, quadratureOrder, 3);

  #if 0
    std::cout << "mtxPDEGlob_q " << std::endl; mtxPDEGlob_q.dump();
    std::cout << "mtxPDEGlob_a " << std::endl; mtxPDEGlob_a.dump();
    std::cout << "mtxPDEGlob_lg " << std::endl; mtxPDEGlob_lg.dump();
    std::cout << "mtxAuGlob_q " << std::endl; mtxAuGlob_q.dump();
    std::cout << "mtxAuGlob_a " << std::endl; mtxAuGlob_a.dump();
    std::cout << "mtxAuGlob_lg " << std::endl; mtxAuGlob_lg.dump();
    std::cout << "mtxBCGlob_q " << std::endl; mtxBCGlob_q.dump();
    std::cout << "mtxBCGlob_a " << std::endl; mtxBCGlob_a.dump();
    std::cout << "mtxBCGlob_lg " << std::endl; mtxBCGlob_lg.dump();
  #endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_q[i][j], mtxPDEGlob_q(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD2::D*afld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_q[i][j], mtxAuGlob_q(i,j), small_tol, close_tol );

  for (int i = 0; i < lgfld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_q[i][j], mtxBCGlob_q(i,j), small_tol, close_tol );


  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < PhysD2::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_a[i][j], mtxPDEGlob_a(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD2::D*afld.nDOF(); i++)
    for (int j = 0; j < PhysD2::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_a[i][j], mtxAuGlob_a(i,j), small_tol, close_tol );

  for (int i = 0; i < lgfld.nDOF(); i++)
    for (int j = 0; j < PhysD2::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_a[i][j], mtxBCGlob_a(i,j), small_tol, close_tol );


  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < lgfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_lg[i][j], mtxPDEGlob_lg(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD2::D*afld.nDOF(); i++)
    for (int j = 0; j < lgfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_lg[i][j], mtxAuGlob_lg(i,j), small_tol, close_tol );

  for (int i = 0; i < lgfld.nDOF(); i++)
    for (int j = 0; j < lgfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_lg[i][j], mtxBCGlob_lg(i,j), small_tol, close_tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_FieldTrace_sansLG_2D_1Triangle_X1Q0_Surreal1 )
{
  typedef SurrealS<1> SurrealClass;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_sansLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> BoundaryIntegrandClass;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // BC

  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P0 (aka Q0)
  int qorder = 0;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK( qfld.nDOF() == 1 );

  // triangle solution data (left)
  qfld.DOF(0) = 3;

  // auxiliary variable
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK( afld.nDOF()*PhysD2::D == 2 );

  // triangle auxiliary variable (left)
  afld.DOF(0) = { 2, -3};

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK( qIfld.nDOF() == 3 );

  qIfld.DOF(0) =  0;
  qIfld.DOF(1) =  5;
  qIfld.DOF(2) =  0;

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder[3] = {1, 1, 1};

  // integrand
  DiscretizationHDG<PDEClass> disc( pde, Global, AugGradient );
  const std::vector<int> BoundaryGroups = {1};
  BoundaryIntegrandClass fcnbc( pde, bc, BoundaryGroups, disc );

  // jacobian via FD w/ residual operator; assumes scalar PDE

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(1), rsdPDEGlobal1(1);
  SLA::SparseVector<ArrayQ> rsdAuGlobal0(2) , rsdAuGlobal1(2);
  SLA::SparseVector<ArrayQ> rsdBCGlobal0(3) , rsdBCGlobal1(3);
  Real jacPDE_q[1][1] = {{0}}        , jacPDE_a[1][2] = {{0,0}}            , jacPDE_lg[1][3] = {{0,0,0}};
  Real jacAu_q[2][1]  = {{0},{0}}    , jacAu_a[2][2]  = {{0,0},{0,0}}      , jacAu_lg[2][3]  = {{0,0,0},{0,0,0}};
  Real jacBC_q[3][1]  = {{0},{0},{0}}, jacBC_a[3][2]  = {{0,0},{0,0},{0,0}}, jacBC_lg[3][3]  = {{0,0,0},{0,0,0},{0,0,0}};

  rsdPDEGlobal0 = 0;
  rsdAuGlobal0 = 0;
  rsdBCGlobal0 = 0;
  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                              rsdPDEGlobal0, rsdAuGlobal0, rsdBCGlobal0),
                                                              xfld, (qfld, afld), qIfld, quadratureOrder, 3 );

  // wrt u
  for (int j = 0; j < qfld.nDOF(); j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdAuGlobal1 = 0;
    rsdBCGlobal1 = 0;
    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                rsdPDEGlobal1, rsdAuGlobal1, rsdBCGlobal1),
                                                                xfld, (qfld, afld), qIfld, quadratureOrder, 3 );

    qfld.DOF(j) -= 1;

    for (int i = 0; i < qfld.nDOF(); i++)
    {
      jacPDE_q[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
      for (int d = 0; d < PhysD2::D; d++)
        jacAu_q[PhysD2::D*i+d][j]  = rsdAuGlobal1[PhysD2::D*i+d]  - rsdAuGlobal0[PhysD2::D*i+d];
    }

    for (int i = 0; i < qIfld.nDOF(); i++)
      jacBC_q[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
  }

  for (int j = 0; j < afld.nDOF(); j++)
    for (int d = 0; d < PhysD2::D; d++)
    {
      afld.DOF(j)[d] += 1;

      rsdPDEGlobal1 = 0;
      rsdAuGlobal1 = 0;
      rsdBCGlobal1 = 0;
      IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                  rsdPDEGlobal1, rsdAuGlobal1, rsdBCGlobal1),
                                                                  xfld, (qfld, afld), qIfld, quadratureOrder, 3 );

      afld.DOF(j)[d] -= 1;

      for (int i = 0; i < qfld.nDOF(); i++)
      {
        jacPDE_a[i][PhysD2::D*j+d] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
        for (int d2 = 0; d2 < PhysD2::D; d2++)
          jacAu_a[PhysD2::D*i+d2][PhysD2::D*j+d]  = rsdAuGlobal1[PhysD2::D*i+d2]  - rsdAuGlobal0[PhysD2::D*i+d2];
      }

      for (int i = 0; i < qIfld.nDOF(); i++)
        jacBC_a[i][PhysD2::D*j+d] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
    }

  for (int j = 0; j < qIfld.nDOF(); j++)
  {
    qIfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdAuGlobal1 = 0;
    rsdBCGlobal1 = 0;
    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                rsdPDEGlobal1, rsdAuGlobal1, rsdBCGlobal1),
                                                                xfld, (qfld, afld), qIfld, quadratureOrder, 3 );

    qIfld.DOF(j) -= 1;

    for (int i = 0; i < qfld.nDOF(); i++)
    {
      jacPDE_lg[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];

      for (int d = 0; d < PhysD2::D; d++)
        jacAu_lg[PhysD2::D*i+d][j]  = rsdAuGlobal1[PhysD2::D*i+d]  - rsdAuGlobal0[PhysD2::D*i+d];
    }

    for (int i = 0; i < qIfld.nDOF(); i++)
      jacBC_lg[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
  }

  #if 0
    std::cout << "jacPDE_q = " << std::endl; dump(jacPDE_q,1,1);
    std::cout << "jacPDE_a = " << std::endl; dump(jacPDE_a,1,2);
    std::cout << "jacPDE_lg = " << std::endl; dump(jacPDE_lg,1,3);
    std::cout << "jacAu_q = " << std::endl; dump(jacAu_q,2,1);
    std::cout << "jacAu_a = " << std::endl; dump(jacAu_a,2,2);
    std::cout << "jacAu_lg = " << std::endl; dump(jacAu_lg,2,3);
    std::cout << "jacBC_q = " << std::endl; dump(jacBC_q,3,1);
    std::cout << "jacBC_a = " << std::endl; dump(jacBC_a,3,2);
    std::cout << "jacBC_lg = " << std::endl; dump(jacBC_lg,3,3);
  #endif

  // jacobian via Surreal

  DLA::MatrixD<MatrixQ> mtxPDEGlob_q(1,1), mtxPDEGlob_a(1,2), mtxPDEGlob_lg(1,3);
  DLA::MatrixD<MatrixQ> mtxAuGlob_q(2,1) , mtxAuGlob_a(2,2) , mtxAuGlob_lg(2,3);
  DLA::MatrixD<MatrixQ> mtxBCGlob_q(3,1) , mtxBCGlob_a(3,2) , mtxBCGlob_lg(3,3);

  mtxPDEGlob_q = 0; mtxPDEGlob_a = 0; mtxPDEGlob_lg = 0;
  mtxAuGlob_q  = 0; mtxAuGlob_a  = 0; mtxAuGlob_lg  = 0;
  mtxBCGlob_q  = 0; mtxBCGlob_a  = 0; mtxBCGlob_lg  = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnbc, mtxPDEGlob_q, mtxPDEGlob_a, mtxPDEGlob_lg,
                                                        mtxAuGlob_q , mtxAuGlob_a , mtxAuGlob_lg ,
                                                        mtxBCGlob_q , mtxBCGlob_a , mtxBCGlob_lg),
      xfld, (qfld, afld), qIfld, quadratureOrder, 3);

  #if 0
    std::cout << "mtxPDEGlob_q " << std::endl; mtxPDEGlob_q.dump();
    std::cout << "mtxPDEGlob_a " << std::endl; mtxPDEGlob_a.dump();
    std::cout << "mtxPDEGlob_lg " << std::endl; mtxPDEGlob_lg.dump();
    std::cout << "mtxAuGlob_q " << std::endl; mtxAuGlob_q.dump();
    std::cout << "mtxAuGlob_a " << std::endl; mtxAuGlob_a.dump();
    std::cout << "mtxAuGlob_lg " << std::endl; mtxAuGlob_lg.dump();
    std::cout << "mtxBCGlob_q " << std::endl; mtxBCGlob_q.dump();
    std::cout << "mtxBCGlob_a " << std::endl; mtxBCGlob_a.dump();
    std::cout << "mtxBCGlob_lg " << std::endl; mtxBCGlob_lg.dump();
  #endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_q[i][j], mtxPDEGlob_q(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD2::D*afld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_q[i][j], mtxAuGlob_q(i,j), small_tol, close_tol );

  for (int i = 0; i < qIfld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_q[i][j], mtxBCGlob_q(i,j), small_tol, close_tol );


  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < PhysD2::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_a[i][j], mtxPDEGlob_a(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD2::D*afld.nDOF(); i++)
    for (int j = 0; j < PhysD2::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_a[i][j], mtxAuGlob_a(i,j), small_tol, close_tol );

  for (int i = 0; i < qIfld.nDOF(); i++)
    for (int j = 0; j < PhysD2::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_a[i][j], mtxBCGlob_a(i,j), small_tol, close_tol );


  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < qIfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_lg[i][j], mtxPDEGlob_lg(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD2::D*afld.nDOF(); i++)
    for (int j = 0; j < qIfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_lg[i][j], mtxAuGlob_lg(i,j), small_tol, close_tol );

  for (int i = 0; i < qIfld.nDOF(); i++)
    for (int j = 0; j < qIfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_lg[i][j], mtxBCGlob_lg(i,j), small_tol, close_tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( HDG_mitLG_2D_1Triangle_X1Q1_Surreal, SurrealClass, Surreals )
{

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> BoundaryIntegrandClass;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // BC

  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK( qfld.nDOF() == 3 );

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  // auxiliary variable
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK( afld.nDOF()*PhysD2::D == 6 );

  // triangle auxiliary variable (left)
  afld.DOF(0) = { 2, -3};
  afld.DOF(1) = { 7,  8};
  afld.DOF(2) = {-1, -5};

  // interface solution: P1 (aka Q1)
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK( lgfld.nDOF() == 6 );

  // line trace data
  lgfld.DOF(0) =  0;
  lgfld.DOF(1) =  0;
  lgfld.DOF(2) =  8;
  lgfld.DOF(3) = -1;
  lgfld.DOF(4) =  0;
  lgfld.DOF(5) =  0;

  const int nDOFPDE = qfld.nDOF();
  const int nDOFAu  = afld.nDOF()*PhysD2::D;
  const int nDOFBC  = lgfld.nDOF();

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder[3] = {2, 2, 2};

  // integrand
  DiscretizationHDG<PDEClass> disc( pde, Global, AugGradient );
  const std::vector<int> BoundaryGroups = {1};
  BoundaryIntegrandClass fcnbc( pde, bc, BoundaryGroups, disc );

  // jacobian via FD w/ residual operator; assumes scalar PDE

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(3), rsdPDEGlobal1(3);
  SLA::SparseVector<ArrayQ> rsdAuGlobal0(6) , rsdAuGlobal1(6);
  SLA::SparseVector<ArrayQ> rsdBCGlobal0(6) , rsdBCGlobal1(6);
  Real jacPDE_q[3][3] = {{0}}, jacPDE_a[3][6] = {{0}}, jacPDE_lg[3][6] = {{0}};
  Real jacAu_q[6][3] = {{0}}, jacAu_a[6][6] = {{0}}, jacAu_lg[6][6] = {{0}};
  Real jacBC_q[6][3] = {{0}}, jacBC_a[6][6] = {{0}}, jacBC_lg[6][6] = {{0}};



  rsdPDEGlobal0 = 0;
  rsdAuGlobal0 = 0;
  rsdBCGlobal0 = 0;
  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                              rsdPDEGlobal0, rsdAuGlobal0, rsdBCGlobal0),
                                                              xfld, (qfld, afld), lgfld, quadratureOrder, 3 );

  // wrt u
  for (int j = 0; j < qfld.nDOF(); j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdAuGlobal1 = 0;
    rsdBCGlobal1 = 0;
    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                rsdPDEGlobal1, rsdAuGlobal1, rsdBCGlobal1),
                                                                xfld, (qfld, afld), lgfld, quadratureOrder, 3 );

    qfld.DOF(j) -= 1;

    for (int i = 0; i < qfld.nDOF(); i++)
    {
      jacPDE_q[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
      for (int d = 0; d < PhysD2::D; d++)
        jacAu_q[PhysD2::D*i+d][j]  = rsdAuGlobal1[PhysD2::D*i+d]  - rsdAuGlobal0[PhysD2::D*i+d];
    }

    for (int i = 0; i < lgfld.nDOF(); i++)
      jacBC_q[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
  }

  for (int j = 0; j < afld.nDOF(); j++)
    for (int d = 0; d < PhysD2::D; d++)
    {
      afld.DOF(j)[d] += 1;

      rsdPDEGlobal1 = 0;
      rsdAuGlobal1 = 0;
      rsdBCGlobal1 = 0;
      IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                  rsdPDEGlobal1, rsdAuGlobal1, rsdBCGlobal1),
                                                                  xfld, (qfld, afld), lgfld, quadratureOrder, 3 );

      afld.DOF(j)[d] -= 1;

      for (int i = 0; i < qfld.nDOF(); i++)
      {
        jacPDE_a[i][PhysD2::D*j+d] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
        for (int d2 = 0; d2 < PhysD2::D; d2++)
          jacAu_a[PhysD2::D*i+d2][PhysD2::D*j+d]  = rsdAuGlobal1[PhysD2::D*i+d2]  - rsdAuGlobal0[PhysD2::D*i+d2];
      }

      for (int i = 0; i < lgfld.nDOF(); i++)
        jacBC_a[i][PhysD2::D*j+d] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
    }

  for (int j = 0; j < lgfld.nDOF(); j++)
  {
    lgfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdAuGlobal1 = 0;
    rsdBCGlobal1 = 0;
    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                rsdPDEGlobal1, rsdAuGlobal1, rsdBCGlobal1),
                                                                xfld, (qfld, afld), lgfld, quadratureOrder, 3 );

    lgfld.DOF(j) -= 1;

    for (int i = 0; i < qfld.nDOF(); i++)
    {
      jacPDE_lg[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];

      for (int d = 0; d < PhysD2::D; d++)
        jacAu_lg[PhysD2::D*i+d][j]  = rsdAuGlobal1[PhysD2::D*i+d]  - rsdAuGlobal0[PhysD2::D*i+d];
    }

    for (int i = 0; i < lgfld.nDOF(); i++)
      jacBC_lg[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
  }

  #if 0
    std::cout << "jacPDE_q = " << std::endl; dump(jacPDE_q,1,1);
    std::cout << "jacPDE_a = " << std::endl; dump(jacPDE_a,1,2);
    std::cout << "jacPDE_lg = " << std::endl; dump(jacPDE_lg,1,3);
    std::cout << "jacAu_q = " << std::endl; dump(jacAu_q,2,1);
    std::cout << "jacAu_a = " << std::endl; dump(jacAu_a,2,2);
    std::cout << "jacAu_lg = " << std::endl; dump(jacAu_lg,2,3);
    std::cout << "jacBC_q = " << std::endl; dump(jacBC_q,3,1);
    std::cout << "jacBC_a = " << std::endl; dump(jacBC_a,3,2);
    std::cout << "jacBC_lg = " << std::endl; dump(jacBC_lg,3,3);
  #endif

  // jacobian via Surreal

  DLA::MatrixD<MatrixQ> mtxPDEGlob_q(nDOFPDE,nDOFPDE), mtxPDEGlob_a(nDOFPDE,nDOFAu), mtxPDEGlob_lg(nDOFPDE,nDOFBC);
  DLA::MatrixD<MatrixQ> mtxAuGlob_q(nDOFAu,nDOFPDE) , mtxAuGlob_a(nDOFAu,nDOFAu) , mtxAuGlob_lg(nDOFAu,nDOFBC);
  DLA::MatrixD<MatrixQ> mtxBCGlob_q(nDOFBC,nDOFPDE) , mtxBCGlob_a(nDOFBC,nDOFAu) , mtxBCGlob_lg(nDOFBC,nDOFBC);

  mtxPDEGlob_q = 0; mtxPDEGlob_a = 0; mtxPDEGlob_lg = 0;
  mtxAuGlob_q  = 0; mtxAuGlob_a  = 0; mtxAuGlob_lg  = 0;
  mtxBCGlob_q  = 0; mtxBCGlob_a  = 0; mtxBCGlob_lg  = 0;

  BOOST_CHECK(1 == 1);

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnbc, mtxPDEGlob_q, mtxPDEGlob_a, mtxPDEGlob_lg,
                                                        mtxAuGlob_q , mtxAuGlob_a , mtxAuGlob_lg ,
                                                        mtxBCGlob_q , mtxBCGlob_a , mtxBCGlob_lg),
      xfld, (qfld, afld), lgfld, quadratureOrder, 3);

  #if 0
    std::cout << "mtxPDEGlob_q " << std::endl; mtxPDEGlob_q.dump();
    std::cout << "mtxPDEGlob_a " << std::endl; mtxPDEGlob_a.dump();
    std::cout << "mtxPDEGlob_lg " << std::endl; mtxPDEGlob_lg.dump();
    std::cout << "mtxAuGlob_q " << std::endl; mtxAuGlob_q.dump();
    std::cout << "mtxAuGlob_lg " << std::endl; mtxAuGlob_lg.dump();
    std::cout << "mtxBCGlob_q " << std::endl; mtxBCGlob_q.dump();
    std::cout << "mtxBCGlob_a " << std::endl; mtxBCGlob_a.dump();
    std::cout << "mtxBCGlob_lg " << std::endl; mtxBCGlob_lg.dump();
  #endif

  const Real small_tol = 1e-12;
  const Real close_tol = 2e-12;
  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_q[i][j], mtxPDEGlob_q(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD2::D*afld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_q[i][j], mtxAuGlob_q(i,j), small_tol, close_tol );

  for (int i = 0; i < lgfld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_q[i][j], mtxBCGlob_q(i,j), small_tol, close_tol );

  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < PhysD2::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_a[i][j], mtxPDEGlob_a(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD2::D*afld.nDOF(); i++)
    for (int j = 0; j < PhysD2::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_a[i][j], mtxAuGlob_a(i,j), small_tol, close_tol );

  for (int i = 0; i < lgfld.nDOF(); i++)
    for (int j = 0; j < PhysD2::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_a[i][j], mtxBCGlob_a(i,j), small_tol, close_tol );

  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < lgfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_lg[i][j], mtxPDEGlob_lg(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD2::D*afld.nDOF(); i++)
    for (int j = 0; j < lgfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_lg[i][j], mtxAuGlob_lg(i,j), small_tol, close_tol );

  for (int i = 0; i < lgfld.nDOF(); i++)
    for (int j = 0; j < lgfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_lg[i][j], mtxBCGlob_lg(i,j), small_tol, close_tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( HDG_FieldTrace_sansLG_2D_1Triangle_X1Q1_Surreal, SurrealClass, Surreals )
{

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_sansLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> BoundaryIntegrandClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // BC

  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK( qfld.nDOF() == 3 );

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  // auxiliary variable
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK( afld.nDOF()*PhysD2::D == 6 );

  // triangle auxiliary variable (left)
  afld.DOF(0) = { 2, -3};
  afld.DOF(1) = { 7,  8};
  afld.DOF(2) = {-1, -5};

  // interface solution: P1 (aka Q1)
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK( qIfld.nDOF() == 6 );

  // line trace data
  qIfld.DOF(0) =  0;
  qIfld.DOF(1) =  0;
  qIfld.DOF(2) =  8;
  qIfld.DOF(3) = -1;
  qIfld.DOF(4) =  0;
  qIfld.DOF(5) =  0;

  const int nDOFPDE = qfld.nDOF();
  const int nDOFAu  = afld.nDOF()*PhysD2::D;
  const int nDOFBC  = qIfld.nDOF();

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder[3] = {2, 2, 2};

  // integrand
  DiscretizationHDG<PDEClass> disc( pde, Global, AugGradient );
  const std::vector<int> BoundaryGroups = {1};
  BoundaryIntegrandClass fcnbc( pde, bc, BoundaryGroups, disc );

  // jacobian via FD w/ residual operator; assumes scalar PDE

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(3), rsdPDEGlobal1(3);
  SLA::SparseVector<ArrayQ> rsdAuGlobal0(6) , rsdAuGlobal1(6);
  SLA::SparseVector<ArrayQ> rsdBCGlobal0(6) , rsdBCGlobal1(6);
  Real jacPDE_q[3][3] = {{0}}, jacPDE_a[3][6] = {{0}}, jacPDE_lg[3][6] = {{0}};
  Real jacAu_q[6][3] = {{0}}, jacAu_a[6][6] = {{0}}, jacAu_lg[6][6] = {{0}};
  Real jacBC_q[6][3] = {{0}}, jacBC_a[6][6] = {{0}}, jacBC_lg[6][6] = {{0}};

  rsdPDEGlobal0 = 0;
  rsdAuGlobal0 = 0;
  rsdBCGlobal0 = 0;
  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                              rsdPDEGlobal0, rsdAuGlobal0, rsdBCGlobal0),
                                                              xfld, (qfld, afld), qIfld, quadratureOrder, 3 );

  // wrt q
  for (int j = 0; j < qfld.nDOF(); j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdAuGlobal1 = 0;
    rsdBCGlobal1 = 0;
    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                rsdPDEGlobal1, rsdAuGlobal1, rsdBCGlobal1),
                                                                xfld, (qfld, afld), qIfld, quadratureOrder, 3 );

    qfld.DOF(j) -= 1;

    for (int i = 0; i < qfld.nDOF(); i++)
    {
      jacPDE_q[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
      for (int d = 0; d < PhysD2::D; d++)
        jacAu_q[PhysD2::D*i+d][j]  = rsdAuGlobal1[PhysD2::D*i+d]  - rsdAuGlobal0[PhysD2::D*i+d];
    }

    for (int i = 0; i < qIfld.nDOF(); i++)
      jacBC_q[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
  }

  for (int j = 0; j < afld.nDOF(); j++)
    for (int d = 0; d < PhysD2::D; d++)
    {
      afld.DOF(j)[d] += 1;

      rsdPDEGlobal1 = 0;
      rsdAuGlobal1 = 0;
      rsdBCGlobal1 = 0;
      IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                  rsdPDEGlobal1, rsdAuGlobal1, rsdBCGlobal1),
                                                                  xfld, (qfld, afld), qIfld, quadratureOrder, 3 );

      afld.DOF(j)[d] -= 1;

      for (int i = 0; i < qfld.nDOF(); i++)
      {
        jacPDE_a[i][PhysD2::D*j+d] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
        for (int d2 = 0; d2 < PhysD2::D; d2++)
          jacAu_a[PhysD2::D*i+d2][PhysD2::D*j+d]  = rsdAuGlobal1[PhysD2::D*i+d2]  - rsdAuGlobal0[PhysD2::D*i+d2];
      }

      for (int i = 0; i < qIfld.nDOF(); i++)
        jacBC_a[i][PhysD2::D*j+d] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
    }

  for (int j = 0; j < qIfld.nDOF(); j++)
  {
    qIfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdAuGlobal1 = 0;
    rsdBCGlobal1 = 0;
    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnbc,
                                                                rsdPDEGlobal1, rsdAuGlobal1, rsdBCGlobal1),
                                                                xfld, (qfld, afld), qIfld, quadratureOrder, 3 );

    qIfld.DOF(j) -= 1;

    for (int i = 0; i < qfld.nDOF(); i++)
    {
      jacPDE_lg[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];

      for (int d = 0; d < PhysD2::D; d++)
        jacAu_lg[PhysD2::D*i+d][j]  = rsdAuGlobal1[PhysD2::D*i+d]  - rsdAuGlobal0[PhysD2::D*i+d];
    }

    for (int i = 0; i < qIfld.nDOF(); i++)
      jacBC_lg[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];
  }

  #if 0
    std::cout << "jacPDE_q = " << std::endl; dump(jacPDE_q,1,1);
    std::cout << "jacPDE_a = " << std::endl; dump(jacPDE_a,1,2);
    std::cout << "jacPDE_lg = " << std::endl; dump(jacPDE_lg,1,3);
    std::cout << "jacAu_q = " << std::endl; dump(jacAu_q,2,1);
    std::cout << "jacAu_a = " << std::endl; dump(jacAu_a,2,2);
    std::cout << "jacAu_lg = " << std::endl; dump(jacAu_lg,2,3);
    std::cout << "jacBC_q = " << std::endl; dump(jacBC_q,3,1);
    std::cout << "jacBC_a = " << std::endl; dump(jacBC_a,3,2);
    std::cout << "jacBC_lg = " << std::endl; dump(jacBC_lg,3,3);
  #endif

  // jacobian via Surreal

  DLA::MatrixD<MatrixQ> mtxPDEGlob_q(nDOFPDE,nDOFPDE), mtxPDEGlob_a(nDOFPDE,nDOFAu), mtxPDEGlob_lg(nDOFPDE,nDOFBC);
  DLA::MatrixD<MatrixQ> mtxAuGlob_q(nDOFAu,nDOFPDE) , mtxAuGlob_a(nDOFAu,nDOFAu) , mtxAuGlob_lg(nDOFAu,nDOFBC);
  DLA::MatrixD<MatrixQ> mtxBCGlob_q(nDOFBC,nDOFPDE) , mtxBCGlob_a(nDOFBC,nDOFAu) , mtxBCGlob_lg(nDOFBC,nDOFBC);

  mtxPDEGlob_q = 0; mtxPDEGlob_a = 0; mtxPDEGlob_lg = 0;
  mtxAuGlob_q  = 0; mtxAuGlob_a  = 0; mtxAuGlob_lg  = 0;
  mtxBCGlob_q  = 0; mtxBCGlob_a  = 0; mtxBCGlob_lg  = 0;

  BOOST_CHECK(1 == 1);

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnbc, mtxPDEGlob_q, mtxPDEGlob_a, mtxPDEGlob_lg,
                                                        mtxAuGlob_q , mtxAuGlob_a , mtxAuGlob_lg ,
                                                        mtxBCGlob_q , mtxBCGlob_a , mtxBCGlob_lg),
      xfld, (qfld, afld), qIfld, quadratureOrder, 3);

  #if 0
    std::cout << "mtxPDEGlob_q " << std::endl; mtxPDEGlob_q.dump();
    std::cout << "mtxPDEGlob_a " << std::endl; mtxPDEGlob_a.dump();
    std::cout << "mtxPDEGlob_lg " << std::endl; mtxPDEGlob_lg.dump();
    std::cout << "mtxAuGlob_q " << std::endl; mtxAuGlob_q.dump();
    std::cout << "mtxAuGlob_lg " << std::endl; mtxAuGlob_lg.dump();
    std::cout << "mtxBCGlob_q " << std::endl; mtxBCGlob_q.dump();
    std::cout << "mtxBCGlob_a " << std::endl; mtxBCGlob_a.dump();
    std::cout << "mtxBCGlob_lg " << std::endl; mtxBCGlob_lg.dump();
  #endif

  const Real small_tol = 1e-12;
  const Real close_tol = 2e-12;
  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_q[i][j], mtxPDEGlob_q(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD2::D*afld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_q[i][j], mtxAuGlob_q(i,j), small_tol, close_tol );

  for (int i = 0; i < qIfld.nDOF(); i++)
    for (int j = 0; j < qfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_q[i][j], mtxBCGlob_q(i,j), small_tol, close_tol );

  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < PhysD2::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_a[i][j], mtxPDEGlob_a(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD2::D*afld.nDOF(); i++)
    for (int j = 0; j < PhysD2::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_a[i][j], mtxAuGlob_a(i,j), small_tol, close_tol );

  for (int i = 0; i < qIfld.nDOF(); i++)
    for (int j = 0; j < PhysD2::D*afld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_a[i][j], mtxBCGlob_a(i,j), small_tol, close_tol );

  for (int i = 0; i < qfld.nDOF(); i++)
    for (int j = 0; j < qIfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacPDE_lg[i][j], mtxPDEGlob_lg(i,j), small_tol, close_tol );

  for (int i = 0; i < PhysD2::D*afld.nDOF(); i++)
    for (int j = 0; j < qIfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacAu_lg[i][j], mtxAuGlob_lg(i,j), small_tol, close_tol );

  for (int i = 0; i < qIfld.nDOF(); i++)
    for (int j = 0; j < qIfld.nDOF(); j++)
      SANS_CHECK_CLOSE( jacBC_lg[i][j], mtxBCGlob_lg(i,j), small_tol, close_tol );

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
