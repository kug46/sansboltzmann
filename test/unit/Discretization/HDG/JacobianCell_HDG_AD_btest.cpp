// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianCell_HDG_AD_btest
// testing of HDG cell-integral jacobian: advection-diffusion

#include <boost/mpl/list.hpp>

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_Trace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_Trace.h"

#include "Field/XField_CellToTrace.h"

#include "Discretization/HDG/IntegrandCell_HDG.h"

#include "Discretization/HDG/SetFieldCell_HDG_AuxiliaryVariable.h"
#include "Discretization/HDG/SetFieldBoundaryTrace_HDG_AuxiliaryVariable.h"
#include "Discretization/HDG/ResidualCell_HDG.h"
#include "Discretization/HDG/JacobianCell_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_HDG_AuxiliaryVariable.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/QuadratureOrder.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#include "IntegrandTest_HDG.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct
namespace           // local definition
{

//----------------------------------------------------------------------------//
// jacobian dumps
template <int N>
void
dump( Real jac[][N], int m, int n )
{
  cout << "{";
  for (int i = 0; i < m; i++)
  {
    cout << "{";
    for (int j = 0; j < n; j++)
    {
      cout << jac[i][j];
      if (j < n-1) cout << ", ";
    }
    cout << "}";
    if (i < m-1) cout << ", ";
  }
  cout << "}" << std::endl;
}

template<class SurrealClass, class IntegrandCell, class IntegrandITrace, class IntegrandBTrace,
         class JacAUXClass,
         class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
void computeAuxiliaryVariables(const IntegrandCell& fcnCell, const IntegrandITrace& fcnITrace, const IntegrandBTrace& fcnBTrace,
                               const FieldDataInvMassMatrix_Cell& mmfld, JacAUXClass& jacAUX_a_bcell,
                               const XField_CellToTrace<PhysDim, TopoDim>& connectivity,
                               const XField<PhysDim, TopoDim>& xfld, const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                               const Field<PhysDim, TopoDim, VectorArrayQ>& afld, const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                               const QuadratureOrder& quadOrder)
{
  jacAUX_a_bcell = 0.0;

  FieldDataVectorD_BoundaryCell<VectorArrayQ> rsdAUX_bcell(qfld);
  rsdAUX_bcell = 0.0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
      SetFieldBoundaryTrace_HDG_AuxiliaryVariable( fcnBTrace, rsdAUX_bcell, jacAUX_a_bcell ),
      xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

  std::vector<int> cellgroupsAux(fcnCell.nCellGroups());
  for (std::size_t i = 0; i < fcnCell.nCellGroups(); i++)
    cellgroupsAux[i] = fcnCell.cellGroup(i);

  //Note: jacAUX_a_bcell gets completed and inverted inside SetFieldCell_HDG_AuxiliaryVariable
  IntegrateCellGroups<TopoDim>::integrate(
      SetFieldCell_HDG_AuxiliaryVariable( fcnCell, fcnITrace, mmfld, connectivity,
                                          xfld, qfld, afld, qIfld,
                                          quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                          rsdAUX_bcell, cellgroupsAux, true, jacAUX_a_bcell),
      xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );
}

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianCell_HDG_Triangle_AD_test_suite )

typedef boost::mpl::list< SurrealS<1>, SurrealS<2> > Surreals;

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( HDG_1D_1Line_X1Q0_Surreal, SurrealClass, Surreals )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef PDEClass::template TensorMatrixQ<Real> TensorMatrixQ;

  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef testspace::IntegrandInteriorTrace_HDG_None<PDEClass> IntegrandITraceClass; //dummy integrand for testing
  typedef testspace::IntegrandBoundaryTrace_None<PDEClass> IntegrandBTraceClass; //dummy integrand for testing
  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  const int D = PhysD1::D;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_UniformGrad source(0.25, 0.6);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == D );
  BOOST_CHECK( pde.N == 1 );

  // HDG discretization
  DiscretizationClass disc( pde, Global );

  // integrand
  std::vector<int> cellgroups = {0};
  IntegrandCellClass fcnCell( pde, disc, cellgroups );
  IntegrandITraceClass fcnITrace( {0} );
  IntegrandBTraceClass fcnBTrace( {0, 1} );

  // grid: X1
  XField1D xfld(3);

  XField_CellToTrace<PhysD1, TopoD1> connectivity(xfld);

  // solution
  for ( int qorder = 0; qorder <= 3; qorder++ )
  {
    Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
    const int qDOF = qfld.nDOF();

    // solution data
    for (int i = 0; i < qDOF; i++)
      qfld.DOF(i) = sin(PI*i/((Real)qDOF));

    // auxiliary variable
    Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Legendre);
    const int aDOF = afld.nDOF();

    // auxiliary variable data
    for (int i = 0; i < aDOF; i++)
      afld.DOF(i) = cos(PI*i/((Real)aDOF));

    // interface solution
    Field_DG_Trace<PhysD1, TopoD1, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Legendre);
    const int qIDOF = qIfld.nDOF();

    for (int i = 0; i < qIDOF; i++)
      qIfld.DOF(i) = -sin(PI*i/((Real)qIDOF));

    // Compute the field of inverse mass matrices
    FieldDataInvMassMatrix_Cell mmfld(afld);

    // quadrature rule
    QuadratureOrder quadOrder(xfld, 2*qorder);

    // jacobian via FD w/ residual operator; assumes scalar PDE
    // topology-specific single group interface

    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
    SLA::SparseVector<ArrayQ> rsdINTGlobal(qIDOF);
    DLA::MatrixD<MatrixQ> jacPDE_q(qDOF,qDOF);

    jacPDE_q = 0;

    rsdPDEGlobal0 = 0;
    rsdINTGlobal = 0;

    FieldDataMatrixD_BoundaryCell<TensorMatrixQ> jacAUX_a_bcell(qfld);

    computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                            xfld, qfld, afld, qIfld, quadOrder);

    //Compute cell PDE residual at initial state
    IntegrateCellGroups<TopoD1>::integrate(
        ResidualCell_HDG( fcnCell, fcnITrace, connectivity,
                          xfld, qfld, afld, qIfld,
                          quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                         rsdPDEGlobal0, rsdINTGlobal ),
        xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

    //wrt q
    for (int j = 0; j < qDOF; j++)
    {
      qfld.DOF(j) += 1;

      rsdPDEGlobal1 = 0;
      rsdINTGlobal = 0;

      computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                              xfld, qfld, afld, qIfld, quadOrder);

      //Compute cell PDE residual at perturbed state
      IntegrateCellGroups<TopoD1>::integrate(
          ResidualCell_HDG( fcnCell, fcnITrace, connectivity,
                            xfld, qfld, afld, qIfld,
                            quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                            rsdPDEGlobal1, rsdINTGlobal ),
          xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

      qfld.DOF(j) -= 1;

      for (int i = 0; i < qDOF; i++)
        jacPDE_q(i,j) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    }

    DLA::MatrixD<MatrixQ> jacPDEGlob_q(qDOF,qDOF);
    DLA::MatrixD<MatrixQ> jacPDEGlob_qI(qDOF,qIDOF);
    DLA::MatrixD<MatrixQ> jacINTGlob_q(qIDOF,qDOF);
    DLA::MatrixD<MatrixQ> jacINTGlob_qI(qIDOF,qIDOF);
    jacPDEGlob_q = 0;
    jacPDEGlob_qI = 0;
    jacINTGlob_q = 0;
    jacINTGlob_qI = 0;

    computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                            xfld, qfld, afld, qIfld, quadOrder);

    FieldDataMatrixD_BoundaryCell<VectorMatrixQ> jacAUX_q_bcell(qfld);
    FieldDataMatrixD_BoundaryTrace<VectorMatrixQ> jacAUX_qI_btrace(qfld, qIfld);
    jacAUX_q_bcell = 0.0;
    jacAUX_qI_btrace = 0.0;

    std::vector<std::vector<std::vector<int>>> mapDOFGlobal_qI(xfld.nBoundaryTraceGroups());

    // Compute Jacobians via Surreals
    IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
        JacobianBoundaryTrace_HDG_AuxiliaryVariable<SurrealClass>( fcnBTrace, jacAUX_q_bcell, jacAUX_qI_btrace, mapDOFGlobal_qI ),
        xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

    IntegrateCellGroups<TopoD1>::integrate(
        JacobianCell_HDG<SurrealClass>( fcnCell, fcnITrace,
                                        mmfld, jacAUX_a_bcell, jacAUX_q_bcell, jacAUX_qI_btrace,
                                        mapDOFGlobal_qI, connectivity,
                                        xfld, qfld, afld, qIfld,
                                        quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                        cellgroups,
                                        jacPDEGlob_q, jacPDEGlob_qI,
                                        jacINTGlob_q, jacINTGlob_qI ),
         xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
        JacobianBoundaryTrace_HDG<SurrealClass>( fcnCell, fcnITrace, fcnBTrace,
                                                 jacAUX_a_bcell, jacAUX_q_bcell, jacAUX_qI_btrace,
                                                 mapDOFGlobal_qI, connectivity,
                                                 xfld, qfld, afld, qIfld,
                                                 quadOrder.cellOrders.data(), quadOrder.cellOrders.size(),
                                                 quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                                 jacPDEGlob_q, jacPDEGlob_qI,
                                                 jacINTGlob_q, jacINTGlob_qI),
        xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

    const Real small_tol = 1e-10;
    const Real close_tol = 5e-10;

    for (int i = 0; i < qDOF; i++)
    {
      for (int j = 0; j < qDOF; j++)
      {
        SANS_CHECK_CLOSE( jacPDE_q(i,j), jacPDEGlob_q(i,j), small_tol, close_tol );
      }
    }

  } // qorder loop
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( HDG_2D_1Triangle_X1Q0_Surreal, SurrealClass, Surreals )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef PDEClass::template TensorMatrixQ<Real> TensorMatrixQ;

  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef testspace::IntegrandInteriorTrace_HDG_None<PDEClass> IntegrandITraceClass; //dummy integrand for testing
  typedef testspace::IntegrandBoundaryTrace_None<PDEClass> IntegrandBTraceClass; //dummy integrand for testing
  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  const int D = PhysD2::D;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_UniformGrad source(0.25, 0.6, -1.5);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == D );
  BOOST_CHECK( pde.N == 1 );

  // grid
//  XField2D_2Triangle_X1_1Group xfld;
  XField2D_Box_Triangle_X1 xfld(2,2);
//  XField2D_Box_UnionJack_Triangle_X1 xfld(2,2);

  XField_CellToTrace<PhysD2, TopoD2> connectivity(xfld);

  // HDG discretization
  DiscretizationClass disc( pde, Global );

  // integrand
  std::vector<int> cellgroups = {0};
  IntegrandCellClass fcnCell( pde, disc, cellgroups );
  IntegrandITraceClass fcnITrace( {0,1,2} );
  IntegrandBTraceClass fcnBTrace( {0, 1, 2, 3} );

  // solution
  for ( int qorder = 0; qorder <= 3; qorder++ )
  {
    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
    const int qDOF = qfld.nDOF();

    // solution data
    for (int i = 0; i < qDOF; i++)
      qfld.DOF(i) = sin(PI*i/((Real)qDOF));

    // auxiliary variable
    Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Legendre);
    const int aDOF = afld.nDOF();

    // auxiliary variable data
    for (int i = 0; i < aDOF; i++)
      afld.DOF(i) = {cos(PI*i/((Real)aDOF)), sin(PI*i/((Real)aDOF))};

    // interface solution
    Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Legendre);
    const int qIDOF = qIfld.nDOF();

    for (int i = 0; i < qIDOF; i++)
      qIfld.DOF(i) = -sin(PI*i/((Real)qIDOF));

    // Compute the field of inverse mass matrices
    FieldDataInvMassMatrix_Cell mmfld(afld);

    // quadrature rule
    QuadratureOrder quadOrder(xfld, 2*qorder);

    // jacobian via FD w/ residual operator; assumes scalar PDE
    // topology-specific single group interface

    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
    SLA::SparseVector<ArrayQ> rsdINTGlobal(qIDOF);
    DLA::MatrixD<MatrixQ> jacPDE_q(qDOF,qDOF);

    jacPDE_q = 0;

    rsdPDEGlobal0 = 0;
    rsdINTGlobal = 0;

    FieldDataMatrixD_BoundaryCell<TensorMatrixQ> jacAUX_a_bcell(qfld);

    computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                            xfld, qfld, afld, qIfld, quadOrder);

    //Compute cell PDE residual at initial state
    IntegrateCellGroups<TopoD2>::integrate(
        ResidualCell_HDG( fcnCell, fcnITrace, connectivity,
                          xfld, qfld, afld, qIfld,
                          quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                          rsdPDEGlobal0, rsdINTGlobal ),
        xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

    //wrt q
    for (int j = 0; j < qDOF; j++)
    {
      qfld.DOF(j) += 1;

      rsdPDEGlobal1 = 0;
      rsdINTGlobal = 0;

      computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                              xfld, qfld, afld, qIfld, quadOrder);

      //Compute cell PDE residual at perturbed state
      IntegrateCellGroups<TopoD2>::integrate(
          ResidualCell_HDG( fcnCell, fcnITrace, connectivity,
                            xfld, qfld, afld, qIfld,
                            quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                            rsdPDEGlobal1, rsdINTGlobal ),
          xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

      qfld.DOF(j) -= 1;

      for (int i = 0; i < qDOF; i++)
        jacPDE_q(i,j) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    }

    DLA::MatrixD<MatrixQ> jacPDEGlob_q(qDOF,qDOF);
    DLA::MatrixD<MatrixQ> jacPDEGlob_qI(qDOF,qIDOF);
    DLA::MatrixD<MatrixQ> jacINTGlob_q(qIDOF,qDOF);
    DLA::MatrixD<MatrixQ> jacINTGlob_qI(qIDOF,qIDOF);
    jacPDEGlob_q = 0;
    jacPDEGlob_qI = 0;
    jacINTGlob_q = 0;
    jacINTGlob_qI = 0;

    computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                            xfld, qfld, afld, qIfld, quadOrder);

    FieldDataMatrixD_BoundaryCell<VectorMatrixQ> jacAUX_q_bcell(qfld);
    FieldDataMatrixD_BoundaryTrace<VectorMatrixQ> jacAUX_qI_btrace(qfld, qIfld);
    jacAUX_q_bcell = 0.0;
    jacAUX_qI_btrace = 0.0;

    std::vector<std::vector<std::vector<int>>> mapDOFGlobal_qI(xfld.nBoundaryTraceGroups());

    // Compute Jacobians via Surreals
    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
        JacobianBoundaryTrace_HDG_AuxiliaryVariable<SurrealClass>( fcnBTrace, jacAUX_q_bcell, jacAUX_qI_btrace, mapDOFGlobal_qI ),
        xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

    IntegrateCellGroups<TopoD2>::integrate(
        JacobianCell_HDG<SurrealClass>( fcnCell, fcnITrace,
                                        mmfld, jacAUX_a_bcell, jacAUX_q_bcell, jacAUX_qI_btrace,
                                        mapDOFGlobal_qI, connectivity,
                                        xfld, qfld, afld, qIfld,
                                        quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                        cellgroups,
                                        jacPDEGlob_q, jacPDEGlob_qI,
                                        jacINTGlob_q, jacINTGlob_qI ),
         xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
        JacobianBoundaryTrace_HDG<SurrealClass>( fcnCell, fcnITrace, fcnBTrace,
                                                 jacAUX_a_bcell, jacAUX_q_bcell, jacAUX_qI_btrace,
                                                 mapDOFGlobal_qI, connectivity,
                                                 xfld, qfld, afld, qIfld,
                                                 quadOrder.cellOrders.data(), quadOrder.cellOrders.size(),
                                                 quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                                 jacPDEGlob_q, jacPDEGlob_qI,
                                                 jacINTGlob_q, jacINTGlob_qI),
        xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

    const Real small_tol = 1e-10;
    const Real close_tol = 1e-9;

    for (int i = 0; i < qDOF; i++)
    {
      for (int j = 0; j < qDOF; j++)
      {
//        std::cout << "i = " << i << ", j = " << j << " : " << jacPDE_q(i,j) << ", " << jacPDEGlob_q(i,j) << std::endl;
        SANS_CHECK_CLOSE( jacPDE_q(i,j), jacPDEGlob_q(i,j), small_tol, close_tol );
      }
    }

  } //qorder loop
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( HDG_3D_2Tet_X1, SurrealClass, Surreals )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_UniformGrad > PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef PDEClass::template TensorMatrixQ<Real> TensorMatrixQ;

  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef testspace::IntegrandInteriorTrace_HDG_None<PDEClass> IntegrandITraceClass; //dummy integrand for testing
  typedef testspace::IntegrandBoundaryTrace_None<PDEClass> IntegrandBTraceClass; //dummy integrand for testing
  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  const int D = PhysD3::D;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.7;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123; Real kxy = 0.553; Real kxz = 0.760;
  Real kyx = 0.343; Real kyy = 1.007; Real kyz = 0.870;
  Real kzx = 0.690; Real kzy = 0.365; Real kzz = 1.460;

  ViscousFlux3D_Uniform visc(kxx, kxy, kxz,
                                 kyx, kyy, kyz,
                                 kzx, kzy, kzz);

  Real a0 = 0.4, ax = 0.7, ay = -0.3, az = 0.2;
  Source3D_UniformGrad source(a0, ax, ay, az);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == D );
  BOOST_CHECK( pde.N == 1 );

  // HDG discretization
  DiscretizationClass disc( pde, Global );

  // integrand
  std::vector<int> cellgroups = {0};
  IntegrandCellClass fcnCell( pde, disc, cellgroups );
  IntegrandITraceClass fcnITrace( {0} );
  IntegrandBTraceClass fcnBTrace( {0,1,2,3,4,5} );

  // grid: X1
  XField3D_2Tet_X1_1Group xfld;

  XField_CellToTrace<PhysD3, TopoD3> connectivity(xfld);

  std::vector<BasisFunctionCategory> basis_categories = {BasisFunctionCategory_Legendre,
                                                         BasisFunctionCategory_Hierarchical,
                                                         BasisFunctionCategory_Hierarchical};

  // solution
  for ( int qorder = 0; qorder <= 2; qorder++ )
  {
    Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, basis_categories[qorder]);
    const int qDOF = qfld.nDOF();

    // solution data
    for (int i = 0; i < qDOF; i++)
      qfld.DOF(i) = sin(PI*i/((Real)qDOF));

    // auxiliary variable
    Field_DG_Cell<PhysD3, TopoD3, VectorArrayQ> afld(xfld, qorder, basis_categories[qorder]);
    const int aDOF = afld.nDOF();

    // auxiliary variable data
    for (int i = 0; i < aDOF; i++)
      afld.DOF(i) = {cos(PI*i/((Real)aDOF)), sin(PI*i/((Real)aDOF)), -cos(PI*i/((Real)aDOF))};

    // interface solution
    Field_DG_Trace<PhysD3, TopoD3, ArrayQ> qIfld(xfld, qorder, basis_categories[qorder]);
    const int qIDOF = qIfld.nDOF();

    for (int i = 0; i < qIDOF; i++)
      qIfld.DOF(i) = -sin(PI*i/((Real)qIDOF));

    // Compute the field of inverse mass matrices
    FieldDataInvMassMatrix_Cell mmfld(afld);

    // quadrature rule
    QuadratureOrder quadOrder(xfld, 2*qorder);

    // jacobian via FD w/ residual operator; assumes scalar PDE
    // topology-specific single group interface

    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
    SLA::SparseVector<ArrayQ> rsdINTGlobal(qIDOF);
    DLA::MatrixD<MatrixQ> jacPDE_q(qDOF,qDOF);

    jacPDE_q = 0;

    rsdPDEGlobal0 = 0;
    rsdINTGlobal = 0;

    FieldDataMatrixD_BoundaryCell<TensorMatrixQ> jacAUX_a_bcell(qfld);

    computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                            xfld, qfld, afld, qIfld, quadOrder);

    //Compute cell PDE residual at initial state
    IntegrateCellGroups<TopoD3>::integrate(
        ResidualCell_HDG( fcnCell, fcnITrace, connectivity,
                          xfld, qfld, afld, qIfld,
                          quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                          rsdPDEGlobal0, rsdINTGlobal ),
        xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

    //wrt q
    for (int j = 0; j < qDOF; j++)
    {
      qfld.DOF(j) += 1;

      rsdPDEGlobal1 = 0;
      rsdINTGlobal = 0;

      computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                              xfld, qfld, afld, qIfld, quadOrder);

      //Compute cell PDE residual at perturbed state
      IntegrateCellGroups<TopoD3>::integrate(
          ResidualCell_HDG( fcnCell, fcnITrace, connectivity,
                            xfld, qfld, afld, qIfld,
                            quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                            rsdPDEGlobal1, rsdINTGlobal ),
          xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

      qfld.DOF(j) -= 1;

      for (int i = 0; i < qDOF; i++)
        jacPDE_q(i,j) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    }

    DLA::MatrixD<MatrixQ> jacPDEGlob_q(qDOF,qDOF);
    DLA::MatrixD<MatrixQ> jacPDEGlob_qI(qDOF,qIDOF);
    DLA::MatrixD<MatrixQ> jacINTGlob_q(qIDOF,qDOF);
    DLA::MatrixD<MatrixQ> jacINTGlob_qI(qIDOF,qIDOF);
    jacPDEGlob_q = 0;
    jacPDEGlob_qI = 0;
    jacINTGlob_q = 0;
    jacINTGlob_qI = 0;

    computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                            xfld, qfld, afld, qIfld, quadOrder);

    FieldDataMatrixD_BoundaryCell<VectorMatrixQ> jacAUX_q_bcell(qfld);
    FieldDataMatrixD_BoundaryTrace<VectorMatrixQ> jacAUX_qI_btrace(qfld, qIfld);
    jacAUX_q_bcell = 0.0;
    jacAUX_qI_btrace = 0.0;

    std::vector<std::vector<std::vector<int>>> mapDOFGlobal_qI(xfld.nBoundaryTraceGroups());

    // Compute Jacobians via Surreals
    IntegrateBoundaryTraceGroups_FieldTrace<TopoD3>::integrate(
        JacobianBoundaryTrace_HDG_AuxiliaryVariable<SurrealClass>( fcnBTrace, jacAUX_q_bcell, jacAUX_qI_btrace, mapDOFGlobal_qI ),
        xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

    IntegrateCellGroups<TopoD3>::integrate(
        JacobianCell_HDG<SurrealClass>( fcnCell, fcnITrace,
                                        mmfld, jacAUX_a_bcell, jacAUX_q_bcell, jacAUX_qI_btrace,
                                        mapDOFGlobal_qI, connectivity,
                                        xfld, qfld, afld, qIfld,
                                        quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                        cellgroups,
                                        jacPDEGlob_q, jacPDEGlob_qI,
                                        jacINTGlob_q, jacINTGlob_qI ),
         xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD3>::integrate(
        JacobianBoundaryTrace_HDG<SurrealClass>( fcnCell, fcnITrace, fcnBTrace,
                                                 jacAUX_a_bcell, jacAUX_q_bcell, jacAUX_qI_btrace,
                                                 mapDOFGlobal_qI, connectivity,
                                                 xfld, qfld, afld, qIfld,
                                                 quadOrder.cellOrders.data(), quadOrder.cellOrders.size(),
                                                 quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                                 jacPDEGlob_q, jacPDEGlob_qI,
                                                 jacINTGlob_q, jacINTGlob_qI),
        xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

    const Real small_tol = 1e-10;
    const Real close_tol = 1e-10;

    for (int i = 0; i < qDOF; i++)
    {
      for (int j = 0; j < qDOF; j++)
      {
//        std::cout << "i = " << i << ", j = " << j << " : " << jacPDE_q(i,j) << ", " << jacPDEGlob_q(i,j) << std::endl;
        SANS_CHECK_CLOSE( jacPDE_q(i,j), jacPDEGlob_q(i,j), small_tol, close_tol );
      }
    }

  } //qorder loop
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
