// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandBoundaryTrace_None_HDG_AD_btest
// testing of boundary integrands: HDG Advection-Diffusion on Triangles

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_None_HDG.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"

#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandBoundaryTrace_HDG_AD_btest_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Line_BCNone_X1Q1LG1_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCNone<PhysD1,AdvectionDiffusionTraits<PhysD1>::N> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementAFieldCell;
  typedef Element<ArrayQ,TopoD0,Node> ElementQFieldTrace;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> BasisWeightedClass;
  typedef IntegrandClass::BasisWeighted_AUX<Real,Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> BasisWeightedAUXClass;

  // PDE
  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3;
  Source1D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // BC
  BCClass bc;

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 0 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  ElementXFieldTrace xfldTrace(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xfldTrace.order() );
  BOOST_CHECK_EQUAL( 1, xfldTrace.nDOF() );

  // adjacent line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = {x1};
  xfldElem.DOF(1) = {x2};

  xfldTrace.DOF(0) = {x2};
  xfldTrace.normalSignL() = 1;

  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // triangle solution (left)
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 4;

  // auxilliary variable
  ElementAFieldCell afldElem(order, BasisFunctionCategory_Hierarchical);

  // line solution (left)
  afldElem.DOF(0) = 2;
  afldElem.DOF(1) = 7;

  // trace variable
  ElementQFieldTrace qIfldElem(0, BasisFunctionCategory_Legendre);

  qIfldElem.DOF(0) =  8;

  // integrand functor
  DiscretizationHDG<PDEClass> disc( pde, Global );
  const std::vector<int> BoundaryGroups = {1};
  IntegrandClass fcnbc( pde, bc, BoundaryGroups, disc );

  BasisWeightedClass fcn = fcnbc.integrand( xfldTrace, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, afldElem, qIfldElem );
  BasisWeightedAUXClass fcnAUX = fcnbc.integrand_AUX( xfldTrace, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, afldElem, qIfldElem );

  BOOST_CHECK_EQUAL( 2, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  BOOST_CHECK_EQUAL( 2, fcnAUX.nDOFCell() );
  BOOST_CHECK_EQUAL( 1, fcnAUX.nDOFTrace() );
  BOOST_CHECK( fcnAUX.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;
  ArrayQ integrandPDETrue[2];
  ArrayQ integrandAUXxTrue[2];
  ArrayQ integrandINTTrue[1];
  BasisWeightedClass::IntegrandCellType integrandPDE[2];
  BasisWeightedClass::IntegrandTraceType integrandINT[1];
  BasisWeightedAUXClass::IntegrandType integrandAUX[2];

  // Test at sRef={0}, s={1}
  sRef = {0};
  fcn( sRef, integrandPDE, 2, integrandINT, 1 );
  fcnAUX( sRef, integrandAUX, 2 );

  //=======================================================
  // Viscous terms zeroed out per request by Prof. Darmofal
  //=======================================================

  //PDE residual integrands: (advective) + (viscous) + (stabilization)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + ( 0 );  // Basis function 1
  integrandPDETrue[1] = ( 22/5. ) + ( 0 ) + ( 0 );  // Basis function 2

  //Aux residual integrands:
  integrandAUXxTrue[0] = ( 0 );  // Basis function 1
  integrandAUXxTrue[1] = ( -8 );  // Basis function 2

  //BC residual integrands:
  integrandINTTrue[0] = ( 22/5. );  // Basis function 1

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandAUXxTrue[0], integrandAUX[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[1], integrandAUX[1][0], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandINTTrue[0], integrandINT[0], small_tol, close_tol );

  // test the trace element integral of the functor

  int quadratureorder = 0;
  int nIntegrandCell = qfldElem.nDOF();
  int nIntegrandTrace = qIfldElem.nDOF();
  typedef BasisWeightedClass::IntegrandCellType IntegrandPDEType;
  typedef BasisWeightedClass::IntegrandTraceType IntegrandINTType;
  typedef BasisWeightedAUXClass::IntegrandType IntegrandAUXType;

  GalerkinWeightedIntegral<TopoD0, Node, IntegrandPDEType, IntegrandINTType> integral(quadratureorder, nIntegrandCell, nIntegrandTrace);
  GalerkinWeightedIntegral<TopoD0, Node, IntegrandAUXType> integralAUX(quadratureorder, nIntegrandCell);

  ArrayQ rsdPDETrue[2], rsdINTTrue[1], rsdAUXxTrue[2];

  IntegrandPDEType rsdPDE[2];
  IntegrandINTType rsdINT[1];
  IntegrandAUXType rsdAUX[2];

  // cell integration for canonical element
  integral( fcn, xfldTrace, rsdPDE, nIntegrandCell, rsdINT, nIntegrandTrace );
  integralAUX( fcnAUX, xfldTrace, rsdAUX, nIntegrandCell );

  //PDE residuals: (advective) + (viscous) + (stabilization)
  rsdPDETrue[0] = ( 0 ) + ( 0 ) + ( 0 ); // Basis function 1
  rsdPDETrue[1] = ( 22/5. ) + ( 0 ) + ( 0 ); // Basis function 2

  rsdAUXxTrue[0] = ( 0 ); // Basis function 1
  rsdAUXxTrue[1] = ( -8 ); // Basis function 2

  rsdINTTrue[0] = ( 22/5. ); // Basis function 1

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDE[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdAUXxTrue[0], rsdAUX[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXxTrue[1], rsdAUX[1][0], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdINTTrue[0], rsdINT[0], small_tol, close_tol );
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_Line_BCNone_X1Q1LG1_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCNone<PhysD1,AdvectionDiffusionTraits<PhysD1>::N> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementAFieldCell;
  typedef Element<ArrayQ,TopoD0,Node> ElementQFieldTrace;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> FieldWeightedClass;

  // PDE
  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3;
  Source1D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // BC
  BCClass bc;

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 0 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  ElementXFieldTrace xfldTrace(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xfldTrace.order() );
  BOOST_CHECK_EQUAL( 1, xfldTrace.nDOF() );

  // adjacent line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = {x1};
  xfldElem.DOF(1) = {x2};

  xfldTrace.DOF(0) = {x2};
  xfldTrace.normalSignL() = 1;

  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // triangle solution (left)
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 4;

  // auxilliary variable
  ElementAFieldCell afldElem(order, BasisFunctionCategory_Hierarchical);

  // line solution (left)
  afldElem.DOF(0) = 2;
  afldElem.DOF(1) = 7;

  // trace variable
  ElementQFieldTrace qIfldTrace(0, BasisFunctionCategory_Legendre);

  qIfldTrace.DOF(0) = 8;

  // weight
  ElementQFieldCell wfldElem(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElem.order() );
  BOOST_CHECK_EQUAL( 3, wfldElem.nDOF() );

  // triangle solution (left)
  wfldElem.DOF(0) = 3;
  wfldElem.DOF(1) = 4;
  wfldElem.DOF(2) = 5;

  // auxiliary variable
  ElementAFieldCell bfldElem(order+1, BasisFunctionCategory_Hierarchical);

  // line solution (left)
  bfldElem.DOF(0) = -5;
  bfldElem.DOF(1) =  3;
  bfldElem.DOF(2) =  2;

  // trace weight
  ElementQFieldTrace wIfldTrace(0, BasisFunctionCategory_Legendre);

  wIfldTrace.DOF(0) = -2;

  // integrand functor
  DiscretizationHDG<PDEClass> disc( pde, Global, Gradient );
  const std::vector<int> BoundaryGroups = {1};
  IntegrandClass fcnbc( pde, bc, BoundaryGroups, disc );

  FieldWeightedClass fcn = fcnbc.integrand( xfldTrace, CanonicalTraceToCell(0, 1),
                                            xfldElem,
                                            qfldElem, afldElem,
                                            wfldElem, bfldElem,
                                            qIfldTrace, wIfldTrace );

  BOOST_CHECK_EQUAL( 2, fcn.nDOFCell() );
  BOOST_CHECK_EQUAL( 1, fcn.nDOFTrace() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;
  Real integrandPDETrue, integrandAUXTrue, integrandINTTrue;
  FieldWeightedClass::IntegrandCellType integrandCell = 0;
  FieldWeightedClass::IntegrandTraceType integrandTrace = 0;

  // Test at sRef={0}, s={1}
  sRef = {0};
  fcn( sRef, integrandCell, integrandTrace );

  //=======================================================
  // Viscous terms zeroed out per request by Prof. Darmofal
  //=======================================================

  //PDE residual integrands: (advective) + (viscous) + (stabilization)
  integrandPDETrue = ( 88/5. ) + ( 0 ) + ( 0 );

  //Aux residual integrands: (stabilization)
  integrandAUXTrue = ( -24 );

  //BC residual integrands: (stabilization)
  integrandINTTrue = ( -44/5. );

  SANS_CHECK_CLOSE( integrandPDETrue, integrandCell.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXTrue, integrandCell.Au, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandINTTrue, integrandTrace, small_tol, close_tol );

  // test the trace element integral of the functor
  int quadratureorder = 0;
  typedef FieldWeightedClass::IntegrandCellType IntegrandCellType;
  typedef FieldWeightedClass::IntegrandTraceType IntegrandTraceType;
  ElementIntegral<TopoD0, Node, IntegrandCellType, IntegrandTraceType> integral(quadratureorder);

  ArrayQ rsdPDETrue, rsdAUXTrue, rsdINTTrue;

  IntegrandCellType rsdElem;
  IntegrandTraceType rsdTrace;
  // cell integration for canonical element
  integral( fcn, xfldTrace, rsdElem, rsdTrace );

  //PDE residuals: (advective) + (viscous) + (stabilization)
  rsdPDETrue = ( 88/5. ) + ( 0 ) + ( 0 );
  rsdAUXTrue = ( -24 );
  rsdINTTrue = ( -44/5. );

  SANS_CHECK_CLOSE( rsdPDETrue, rsdElem.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXTrue, rsdElem.Au, small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdINTTrue, rsdTrace, small_tol, close_tol );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_1D_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCNone<PhysD1,AdvectionDiffusionTraits<PhysD1>::N> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;

  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef Element<ArrayQ,TopoD0,Node> ElementQFieldTrace;
  typedef Element<DLA::VectorS<1,ArrayQ>,TopoD1,Line> ElementAFieldCell;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD0,Node,TopoD1,Line,ElementParam> BasisWeightedClass;
  typedef IntegrandClass::BasisWeighted_AUX<Real,Real,TopoD0,Node,TopoD1,Line,ElementParam> BasisWeightedAUXClass;
  typedef IntegrandClass::FieldWeighted<Real,Real,TopoD0,Node,TopoD1,Line,ElementParam> FieldWeightedClass;
  typedef DiscretizationHDG<PDEClass> DiscretizationClass;


  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3;
  Source1D_Uniform source(a);

  // PDE
  PDEClass pde( adv, visc, source );

  // BC
  BCClass bc;

  // grid
  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  // adjacent line grid
  Real x2, x3;

  x2 = 1;
  x3 = 2;

  xfldElemL.DOF(0) = x2;
  xfldElemL.DOF(1) = x3;

  xnode.DOF(0) = x2;
  xnode.normalSignL() = 1;

  for (int qorder = 2; qorder< 4; qorder++)
  {
    // solution
    ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementAFieldCell afldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementAFieldCell bfldElemL(qorder, BasisFunctionCategory_Hierarchical);

    ElementQFieldTrace qIfldTrace(0, BasisFunctionCategory_Legendre);
    ElementQFieldTrace wIfldTrace(0, BasisFunctionCategory_Legendre);

    BOOST_CHECK_EQUAL( qIfldTrace.nDOF(), wIfldTrace.nDOF() );
    for (int dof = 0; dof < qIfldTrace.nDOF(); dof++)
    {
      qIfldTrace.DOF(dof) = (dof+1)*pow(-1,dof);
      wIfldTrace.DOF(dof) = 0;
    }

    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), afldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), wfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), bfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qIfldTrace.nDOF(), wIfldTrace.nDOF() );

    // line solution
    for ( int dof = 0; dof < qfldElemL.nDOF(); dof ++ )
    {
      qfldElemL.DOF(dof) = (dof+1)*pow(-1,dof);
      wfldElemL.DOF(dof) = 0;

      afldElemL.DOF(dof) = (dof+2)*pow(-1,dof+1);
      bfldElemL.DOF(dof) = 0;
    }

    // HDG discretization
    DiscretizationClass disc( pde, Global, Gradient );

    // integrand
    const std::vector<int> BoundaryGroups = {1};

    IntegrandClass fcnbc( pde, bc, BoundaryGroups, disc );

    // integrand functor
    BasisWeightedClass fcnB = fcnbc.integrand( xnode,
                                               CanonicalTraceToCell(0,1),
                                               xfldElemL,
                                               qfldElemL, afldElemL,
                                               qIfldTrace );

    BasisWeightedAUXClass fcnAUX = fcnbc.integrand_AUX( xnode,
                                                        CanonicalTraceToCell(0,1),
                                                        xfldElemL,
                                                        qfldElemL, afldElemL,
                                                        qIfldTrace );
    // integrand functor
    FieldWeightedClass fcnW = fcnbc.integrand( xnode,
                                               CanonicalTraceToCell(0,1),
                                               xfldElemL,
                                               qfldElemL, afldElemL,
                                               wfldElemL, bfldElemL,
                                               qIfldTrace, wIfldTrace );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;

    const int nIntegrandL = qfldElemL.nDOF();
    const int nIntegrandTrace = qIfldTrace.nDOF();

    int quadratureorder = 0;
    GalerkinWeightedIntegral<TopoD0, Node, BasisWeightedClass::IntegrandCellType,
                                           BasisWeightedClass::IntegrandTraceType>
                                           integralB(quadratureorder, nIntegrandL, nIntegrandTrace);

    GalerkinWeightedIntegral<TopoD0, Node, BasisWeightedAUXClass::IntegrandType> integralAUX(quadratureorder, nIntegrandL);


    ElementIntegral<TopoD0, Node, FieldWeightedClass::IntegrandCellType,
                                  FieldWeightedClass::IntegrandTraceType> integralW(quadratureorder);
    std::vector<BasisWeightedClass::IntegrandCellType> rsdElemBL(nIntegrandL, 0);
    std::vector<BasisWeightedClass::IntegrandTraceType> rsdElemBT(nIntegrandTrace, 0);
    std::vector<BasisWeightedAUXClass::IntegrandType> rsdElemAUXL(nIntegrandL, 0);
    FieldWeightedClass::IntegrandCellType rsdElemWL=0;
    FieldWeightedClass::IntegrandTraceType rsdElemWT=0;

    // cell integration for canonical element
    integralB( fcnB, xnode, rsdElemBL.data(), nIntegrandL, rsdElemBT.data(), nIntegrandTrace );
    integralAUX( fcnAUX, xnode, rsdElemAUXL.data(), nIntegrandL );

    for (int i = 0; i < wfldElemL.nDOF(); i++ )
    {
      // set just one of the weights to one
      wfldElemL.DOF(i) = 1; bfldElemL.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemWL = 0; rsdElemWT = 0;
      integralW(fcnW, xnode, rsdElemWL, rsdElemWT );

      // test the two integrands are the same
      SANS_CHECK_CLOSE ( rsdElemWL.PDE, rsdElemBL[i], small_tol, close_tol );

      Real tmpSum = 0.0;
      for (int d = 0; d < PhysD1::D; d++) tmpSum += rsdElemAUXL[i][d];

      SANS_CHECK_CLOSE( rsdElemWL.Au, tmpSum, small_tol, close_tol );

      // reset to 0
      wfldElemL.DOF(i) = 0; bfldElemL.DOF(i) = 0;
    }

    for (int i = 0; i < wIfldTrace.nDOF(); i++ )
    {
      wIfldTrace.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemWL = 0; rsdElemWT = 0;
      integralW(fcnW, xnode, rsdElemWL, rsdElemWT );

      SANS_CHECK_CLOSE( rsdElemWT, rsdElemBT[i], small_tol, close_tol)
      wIfldTrace.DOF(i) = 0;
    }
  }
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_BCNone_2D_Triangle_X1Q1LG1_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCNone<PhysD2,AdvectionDiffusionTraits<PhysD2>::N> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementAFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandBoundaryClass;
  typedef IntegrandBoundaryClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> BasisWeightedClass;
  typedef IntegrandBoundaryClass::BasisWeighted_AUX<Real,Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> BasisWeightedAUXClass;

  // PDE
  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real a = 2.35;
  Source2D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // BC
  BCClass bc;

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 0 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // grid coordinates
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution
  order = 1;
  ElementQFieldCell qfldElemL(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemL.nDOF() );

  // triangle solution
  qfldElemL.DOF(0) = 1;
  qfldElemL.DOF(1) = 3;
  qfldElemL.DOF(2) = 4;

  // auxiliary variable
  ElementAFieldCell afldElemL(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, afldElemL.order() );
  BOOST_CHECK_EQUAL( 3, afldElemL.nDOF() );

  afldElemL.DOF(0) = { 2, -3};
  afldElemL.DOF(1) = { 7,  8};
  afldElemL.DOF(2) = {-1, -5};

  // Trace
  ElementQFieldTrace qIedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qIedge.order() );
  BOOST_CHECK_EQUAL( 2, qIedge.nDOF() );

  qIedge.DOF(0) =  8;
  qIedge.DOF(1) = -1;

  // integrand
  DiscretizationHDG<PDEClass> disc( pde, Global, Gradient );
  const std::vector<int> BoundaryGroups = {1};
  IntegrandBoundaryClass fcnbc( pde, bc, BoundaryGroups, disc );

  // integrand functor
  BasisWeightedClass fcn = fcnbc.integrand( xedge, CanonicalTraceToCell(0, 1), xfldElemL, qfldElemL, afldElemL, qIedge );
  BasisWeightedAUXClass fcnAUX = fcnbc.integrand_AUX( xedge, CanonicalTraceToCell(0, 1), xfldElemL, qfldElemL, afldElemL, qIedge );

  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  BOOST_CHECK_EQUAL( 3, fcnAUX.nDOFCell() );
  BOOST_CHECK_EQUAL( 2, fcnAUX.nDOFTrace() );
  BOOST_CHECK( fcnAUX.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  RefCoordTraceType sRef;
  Real integrandPDETrue[3];
  Real integrandAUXxTrue[3], integrandAUXyTrue[3];
  Real integrandINTTrue[2];
  BasisWeightedClass::IntegrandCellType integrand[3];
  BasisWeightedClass::IntegrandTraceType integrandINT[2];
  BasisWeightedAUXClass::IntegrandType integrandAUX[3];

  //=======================================================
  // Viscous terms zeroed out per request by Prof. Darmofal
  //=======================================================

  // Test at sRef={0}, s={1}
  sRef = {0};
  fcn( sRef, integrand, 3, integrandINT, 2 );
  fcnAUX( sRef, integrandAUX, 3 );

  //PDE residual integrands: (advective) + (diffusive) + (stabilization)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + ( 0 );  // Basis function 1
  integrandPDETrue[1] = ( (39/10.)*(pow(2,-1/2.)) ) + ( 0 ) + ( 0 );  // Basis function 2
  integrandPDETrue[2] = ( 0 ) + ( 0 ) + ( 0 );  // Basis function 3

  //Auxiliary integrands: (stabilization)
  integrandAUXxTrue[0] = ( 0 );  // Basis function 1
  integrandAUXyTrue[0] = ( 0 );  // Basis function 1
  integrandAUXxTrue[1] = ( (-4)*(sqrt(2)) );  // Basis function 2
  integrandAUXyTrue[1] = ( (-4)*(sqrt(2)) );  // Basis function 2
  integrandAUXxTrue[2] = ( 0 );  // Basis function 3
  integrandAUXyTrue[2] = ( 0 );  // Basis function 3

  //PDE Trace residual integrands: (stabilization)
  integrandINTTrue[0] = ( (13/2.)*(pow(2,-1/2.)) );  // Basis function 1
  integrandINTTrue[1] = ( 0 );  // Basis function 2

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandAUXxTrue[0], integrandAUX[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[0], integrandAUX[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[1], integrandAUX[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[1], integrandAUX[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[2], integrandAUX[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[2], integrandAUX[2][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandINTTrue[0], integrandINT[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandINTTrue[1], integrandINT[1], small_tol, close_tol );


  // Test at sRef={1}, s={0}
  sRef = {1};
  fcn( sRef, integrand, 3, integrandINT, 2 );
  fcnAUX( sRef, integrandAUX, 3 );

  //PDE residual integrands: (advective) + (diffusive) + (stabilization)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + ( 0 );  // Basis function 1
  integrandPDETrue[1] = ( 0 ) + ( 0 ) + ( 0 );  // Basis function 2
  integrandPDETrue[2] = ( (13/5.)*(sqrt(2)) ) + ( 0 ) + ( 0 );  // Basis function 3

  //Auxiliary integrands: (stabilization)
  integrandAUXxTrue[0] = ( 0 );  // Basis function 1
  integrandAUXyTrue[0] = ( 0 );  // Basis function 1
  integrandAUXxTrue[1] = ( 0 );  // Basis function 2
  integrandAUXyTrue[1] = ( 0 );  // Basis function 2
  integrandAUXxTrue[2] = ( pow(2,-1/2.) );  // Basis function 3
  integrandAUXyTrue[2] = ( pow(2,-1/2.) );  // Basis function 3

  //PDE Trace residual integrands: (stabilization)
  integrandINTTrue[0] = ( 0 );  // Basis function 1
  integrandINTTrue[1] = ( (-13/2.)*(pow(2,-1/2.)) );  // Basis function 2

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandAUXxTrue[0], integrandAUX[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[0], integrandAUX[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[1], integrandAUX[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[1], integrandAUX[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[2], integrandAUX[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[2], integrandAUX[2][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandINTTrue[0], integrandINT[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandINTTrue[1], integrandINT[1], small_tol, close_tol );


  // Test at sRef={1/2}, s={1/2}
  sRef = {1/2.};
  fcn( sRef, integrand, 3, integrandINT, 2 );
  fcnAUX( sRef, integrandAUX, 3 );

  //PDE residual integrands: (advective) + (diffusive) + (stabilization)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + ( 0 );  // Basis function 1
  integrandPDETrue[1] = ( (91/40.)*(pow(2,-1/2.)) ) + ( 0 ) + ( 0 );  // Basis function 2
  integrandPDETrue[2] = ( (91/40.)*(pow(2,-1/2.)) ) + ( 0 ) + ( 0 );  // Basis function 3

  //Auxiliary integrands: (stabilization)
  integrandAUXxTrue[0] = ( 0 );  // Basis function 1
  integrandAUXyTrue[0] = ( 0 );  // Basis function 1
  integrandAUXxTrue[1] = ( (-7/4.)*(pow(2,-1/2.)) );  // Basis function 2
  integrandAUXyTrue[1] = ( (-7/4.)*(pow(2,-1/2.)) );  // Basis function 2
  integrandAUXxTrue[2] = ( (-7/4.)*(pow(2,-1/2.)) );  // Basis function 3
  integrandAUXyTrue[2] = ( (-7/4.)*(pow(2,-1/2.)) );  // Basis function 3

  //PDE Trace residual integrands: (stabilization)
  integrandINTTrue[0] = ( 0 );  // Basis function 1
  integrandINTTrue[1] = ( 0 );  // Basis function 2

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandAUXxTrue[0], integrandAUX[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[0], integrandAUX[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[1], integrandAUX[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[1], integrandAUX[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[2], integrandAUX[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[2], integrandAUX[2][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandINTTrue[0], integrandINT[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandINTTrue[1], integrandINT[1], small_tol, close_tol );


  // test the element integral of the functor

  int quadratureorder = 2;
  int nIntegrandL = qfldElemL.nDOF();
  int nIntegrandI = qIedge.nDOF();
  typedef BasisWeightedClass::IntegrandCellType IntegrandCellType;
  typedef BasisWeightedClass::IntegrandTraceType IntegrandTraceType;
  typedef BasisWeightedAUXClass::IntegrandType IntegrandAUXType;
  GalerkinWeightedIntegral<TopoD1, Line, IntegrandCellType, IntegrandTraceType> integralBC(quadratureorder, nIntegrandL, nIntegrandI);
  GalerkinWeightedIntegral<TopoD1, Line, IntegrandAUXType> integralAUX(quadratureorder, nIntegrandL);

  IntegrandCellType rsdPDE[3];
  IntegrandTraceType rsdINT[2];
  IntegrandAUXType rsdAUX[3];

  // cell integration for canonical element
  integralBC( fcn, xedge, rsdPDE, nIntegrandL, rsdINT, nIntegrandI );
  integralAUX( fcnAUX, xedge, rsdAUX, nIntegrandL );

  Real rsdPDETrue[3], rsdINTTrue[2], rsdAUXxTrue[3], rsdAUXyTrue[3];

  //PDE residuals: (advective) + (diffusive) + (stabilization)
  rsdPDETrue[0] = ( 0 ) + ( 0 ) + ( 0 ); // Basis function 1
  rsdPDETrue[1] = ( 13/6. ) + ( 0 ) + ( 0 ); // Basis function 2
  rsdPDETrue[2] = ( 143/60. ) + ( 0 ) + ( 0 ); // Basis function 3

  rsdAUXxTrue[0] = ( 0 ); // Basis function 1
  rsdAUXyTrue[0] = ( 0 ); // Basis function 1
  rsdAUXxTrue[1] = ( -5/2. ); // Basis function 2
  rsdAUXyTrue[1] = ( -5/2. ); // Basis function 2
  rsdAUXxTrue[2] = ( -1 ); // Basis function 3
  rsdAUXyTrue[2] = ( -1 ); // Basis function 3

  rsdINTTrue[0] = ( 13/12. ); // Basis function 1
  rsdINTTrue[1] = ( -13/12. ); // Basis function 2

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[2], rsdPDE[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdAUXxTrue[0], rsdAUX[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXyTrue[0], rsdAUX[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXxTrue[1], rsdAUX[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXyTrue[1], rsdAUX[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXxTrue[2], rsdAUX[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXyTrue[2], rsdAUX[2][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdINTTrue[0], rsdINT[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdINTTrue[1], rsdINT[1], small_tol, close_tol );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_BCNone_2D_Triangle_X1Q1LG1_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCNone<PhysD2,AdvectionDiffusionTraits<PhysD2>::N> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementAFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldTrace;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandBoundaryClass;
  typedef IntegrandBoundaryClass::FieldWeighted<Real,Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> FieldWeightedClass;

  // PDE
  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Real a = 2.35;
  Source2D_Uniform source(a);

  // PDE
  PDEClass pde( adv, visc, source );

  // BC
  BCClass bc;

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 0 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // grid coordinates
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  // auxiliary variable
  ElementAFieldCell afldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, afldElem.order() );
  BOOST_CHECK_EQUAL( 3, afldElem.nDOF() );

  afldElem.DOF(0) = { 2, -3};
  afldElem.DOF(1) = { 7,  8};
  afldElem.DOF(2) = {-1, -5};

  // Trace
  ElementQFieldTrace qIfldTrace(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qIfldTrace.order() );
  BOOST_CHECK_EQUAL( 2, qIfldTrace.nDOF() );

  qIfldTrace.DOF(0) =  8;
  qIfldTrace.DOF(1) = -1;

  // weight
  ElementQFieldCell wfldElem(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElem.order() );
  BOOST_CHECK_EQUAL( 6, wfldElem.nDOF() );

  // triangle solution
  wfldElem.DOF(0) = -2;
  wfldElem.DOF(1) =  4;
  wfldElem.DOF(2) =  3;
  wfldElem.DOF(3) =  2;
  wfldElem.DOF(4) =  4;
  wfldElem.DOF(5) = -1;

  // auxiliary variable
  ElementAFieldCell bfldElem(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, bfldElem.order() );
  BOOST_CHECK_EQUAL( 6, bfldElem.nDOF() );

  bfldElem.DOF(0) = { 1,  5};
  bfldElem.DOF(1) = { 3,  6};
  bfldElem.DOF(2) = {-1,  7};
  bfldElem.DOF(3) = { 3,  1};
  bfldElem.DOF(4) = { 2,  2};
  bfldElem.DOF(5) = { 4,  3};

  // Trace
  ElementQFieldTrace wIfldTrace(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wIfldTrace.order() );
  BOOST_CHECK_EQUAL( 3, wIfldTrace.nDOF() );

  wIfldTrace.DOF(0) =  2;
  wIfldTrace.DOF(1) = -1;
  wIfldTrace.DOF(2) =  3;

  // integrand
  DiscretizationHDG<PDEClass> disc( pde, Global, Gradient );
  const std::vector<int> BoundaryGroups = {1};
  IntegrandBoundaryClass fcnbc( pde, bc, BoundaryGroups, disc );

  // integrand functor
  FieldWeightedClass fcn = fcnbc.integrand( xedge, CanonicalTraceToCell(0, 1),
                                            xfldElem,
                                            qfldElem, afldElem,
                                            wfldElem, bfldElem,
                                            qIfldTrace, wIfldTrace );

  BOOST_CHECK_EQUAL( 3, fcn.nDOFCell() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOFTrace() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;
  Real integrandPDETrue, integrandAUXTrue, integrandTraceTrue;
  FieldWeightedClass::IntegrandCellType integrandCell = 0;
  FieldWeightedClass::IntegrandTraceType integrandTrace = 0;

  // Test at sRef={0}, s={1}
  sRef = {0};
  fcn( sRef, integrandCell, integrandTrace );

  //PDE residual integrands: (advective) + (diffusive) + (stabilization)
  integrandPDETrue = ( (39/5.)*(sqrt(2)) ) + ( 0 ) + ( 0 );

  //Auxiliary integrands: (stabilization)
  integrandAUXTrue = ( (-36)*(sqrt(2)) );

  //PDE Trace residual integrands: (stabilization)
  integrandTraceTrue = ( (13)*(pow(2,-1/2.)) );

  SANS_CHECK_CLOSE( integrandPDETrue, integrandCell.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXTrue, integrandCell.Au, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTraceTrue, integrandTrace, small_tol, close_tol );


  // Test at sRef={1}, s={0}
  sRef = {1};
  fcn( sRef, integrandCell, integrandTrace );

  //PDE residual integrands: (advective) + (diffusive) + (stabilization)
  integrandPDETrue = ( (39/5.)*(sqrt(2)) ) + ( 0 ) + ( 0 );

  //Auxiliary integrands: (stabilization)
  integrandAUXTrue = ( (3)*(sqrt(2)) );

  //PDE Trace residual integrands: (stabilization)
  integrandTraceTrue = ( (13/2.)*(pow(2,-1/2.)) );

  SANS_CHECK_CLOSE( integrandPDETrue, integrandCell.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXTrue, integrandCell.Au, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTraceTrue, integrandTrace, small_tol, close_tol );


  // Test at sRef={1/2}, s={1/2}
  sRef = {1/2.};
  fcn( sRef, integrandCell, integrandTrace );

  //PDE residual integrands: (advective) + (diffusive) + (stabilization)
  integrandPDETrue = ( (1001/40.)*(pow(2,-1/2.)) ) + ( 0 ) + ( 0 );

  //Auxiliary integrands: (stabilization)
  integrandAUXTrue = ( (-161/4.)*(pow(2,-1/2.)) );

  //PDE Trace residual integrands: (stabilization)
  integrandTraceTrue = ( 0 );

  SANS_CHECK_CLOSE( integrandPDETrue, integrandCell.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXTrue, integrandCell.Au, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTraceTrue, integrandTrace, small_tol, close_tol );


  // Test at sRef={1/5}, s={4/5}
  sRef = {1/5.};
  fcn( sRef, integrandCell, integrandTrace );

  //PDE residual integrands: (advective) + (diffusive) + (stabilization)
  integrandPDETrue = ( (6604/625.)*(sqrt(2)) ) + ( 0 ) + ( 0 );

  //Auxiliary integrands: (stabilization)
  integrandAUXTrue = ( (-4247/125.)*(sqrt(2)) );

  //PDE Trace residual integrands: (stabilization)
  integrandTraceTrue = ( (3237/250.)*(pow(2,-1/2.)) );

  SANS_CHECK_CLOSE( integrandPDETrue, integrandCell.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXTrue, integrandCell.Au, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTraceTrue, integrandTrace, small_tol, close_tol );


  // test the element integral of the functor

  int quadratureorder = 5;
  typedef FieldWeightedClass::IntegrandCellType IntegrandCellType;
  typedef FieldWeightedClass::IntegrandTraceType IntegrandTraceType;

  ElementIntegral<TopoD1, Line, IntegrandCellType, IntegrandTraceType> integralBC(quadratureorder);

  IntegrandCellType rsdElem = 0;
  IntegrandTraceType rsdTrace = 0;

  // cell integration for canonical element
  integralBC( fcn, xedge, rsdElem, rsdTrace );

  Real rsdPDETrue, rsdAUXTrue, rsdTraceTrue;

  //PDE residuals: (advective) + (diffusive) + (stabilization)
  rsdPDETrue = ( 1313/60. ) + ( 0 ) + ( 0 );
  rsdAUXTrue = ( -227/6. );
  rsdTraceTrue = ( 13/4. );

  SANS_CHECK_CLOSE( rsdPDETrue, rsdElem.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXTrue, rsdElem.Au, small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTraceTrue, rsdTrace, small_tol, close_tol );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_2D_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCNone<PhysD2,AdvectionDiffusionTraits<PhysD2>::N> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldTrace;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementAFieldCell;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementParam> BasisWeightedClass;
  typedef IntegrandClass::BasisWeighted_AUX<Real,Real,TopoD1,Line,TopoD2,Triangle,ElementParam> BasisWeightedAUXClass;
  typedef IntegrandClass::FieldWeighted<Real,Real,TopoD1,Line,TopoD2,Triangle,ElementParam> FieldWeightedClass;
  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  // PDE
  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Real a = 2.35;
  Source2D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // BC
  BCClass bc;

  // grid
  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // grid coordinates
  Real x1, x2, x3, y1, y2, y3;

  x1 = -0.1;  y1 = -0.05;
  x2 =    1;  y2 =     0;
  x3 =  0.2;  y3 =     1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  for (int qorder = 2; qorder< 4; qorder++)
  {
    // solution
    ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementAFieldCell afldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementAFieldCell bfldElemL(qorder, BasisFunctionCategory_Hierarchical);

    ElementQFieldTrace qIfldElem(qorder, BasisFunctionCategory_Legendre);
    ElementQFieldTrace wIfldElem(qorder, BasisFunctionCategory_Legendre);

    BOOST_CHECK_EQUAL( qIfldElem.nDOF(), wIfldElem.nDOF() );
    for (int dof = 0; dof < qIfldElem.nDOF(); dof++)
    {
      qIfldElem.DOF(dof) = (dof+1)*pow(-1,dof);
      wIfldElem.DOF(dof) = 0;
    }

    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), afldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), wfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), bfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qIfldElem.nDOF(), wIfldElem.nDOF() );

    // line solution
    for ( int dof = 0; dof < qfldElemL.nDOF(); dof ++ )
    {
      qfldElemL.DOF(dof) = (dof+1)*pow(-1,dof);
      wfldElemL.DOF(dof) = 0;

      afldElemL.DOF(dof) = (dof+2)*pow(-1,dof+1);
      bfldElemL.DOF(dof) = 0;
    }

    // HDG discretization
    DiscretizationClass disc( pde, Global, Gradient );

    // integrand
    const std::vector<int> BoundaryGroups = {1};

    IntegrandClass fcnbc( pde, bc, BoundaryGroups, disc );

    // integrand functor
    BasisWeightedClass fcnB = fcnbc.integrand( xedge,
                                               CanonicalTraceToCell(0,1),
                                               xfldElemL,
                                               qfldElemL, afldElemL,
                                               qIfldElem );

    BasisWeightedAUXClass fcnAUX = fcnbc.integrand_AUX( xedge,
                                                        CanonicalTraceToCell(0,1),
                                                        xfldElemL,
                                                        qfldElemL, afldElemL,
                                                        qIfldElem );
    // integrand functor
    FieldWeightedClass fcnW = fcnbc.integrand( xedge,
                                               CanonicalTraceToCell(0,1),
                                               xfldElemL,
                                               qfldElemL, afldElemL,
                                               wfldElemL, bfldElemL,
                                               qIfldElem, wIfldElem );

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;
    const int nIntegrandL = qfldElemL.nDOF();
    const int nIntegrandTrace = qIfldElem.nDOF();

    int quadratureorder = 2*qorder;
    GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedClass::IntegrandCellType,
                                           BasisWeightedClass::IntegrandTraceType>
                                           integralB(quadratureorder, nIntegrandL, nIntegrandTrace);

    GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedAUXClass::IntegrandType>
                                           integralAUX(quadratureorder, nIntegrandL);

    ElementIntegral<TopoD1, Line, FieldWeightedClass::IntegrandCellType,
                                  FieldWeightedClass::IntegrandTraceType> integralW(quadratureorder);
    std::vector<BasisWeightedClass::IntegrandCellType> rsdElemBL(nIntegrandL, 0);
    std::vector<BasisWeightedClass::IntegrandTraceType> rsdElemBT(nIntegrandTrace, 0);
    std::vector<BasisWeightedAUXClass::IntegrandType> rsdElemAUX(nIntegrandL, 0);
    FieldWeightedClass::IntegrandCellType rsdElemWL=0;
    FieldWeightedClass::IntegrandTraceType rsdElemWT=0;

    // cell integration for canonical element
    integralB( fcnB, xedge, rsdElemBL.data(), nIntegrandL, rsdElemBT.data(), nIntegrandTrace );
    integralAUX( fcnAUX, xedge, rsdElemAUX.data(), nIntegrandL );

    for (int i = 0; i < wfldElemL.nDOF(); i++ )
    {
      // set just one of the weights to one
      wfldElemL.DOF(i) = 1; bfldElemL.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemWL = 0; rsdElemWT = 0;
      integralW(fcnW, xedge, rsdElemWL, rsdElemWT );

      // test the two integrands are the same
      SANS_CHECK_CLOSE ( rsdElemWL.PDE, rsdElemBL[i], small_tol, close_tol );

      Real tmpSum = 0.0;
      for (int d = 0; d < PhysD2::D; d++) tmpSum += rsdElemAUX[i][d];

      SANS_CHECK_CLOSE ( rsdElemWL.Au, tmpSum, small_tol, close_tol );

      // reset to 0
      wfldElemL.DOF(i) = 0; bfldElemL.DOF(i) = 0;
    }

    for (int i = 0; i < wIfldElem.nDOF(); i++ )
    {
      wIfldElem.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemWL = 0; rsdElemWT = 0;
      integralW(fcnW, xedge, rsdElemWL, rsdElemWT );

      SANS_CHECK_CLOSE( rsdElemWT, rsdElemBT[i], small_tol, close_tol)
      wIfldElem.DOF(i) = 0;
    }
  }
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
