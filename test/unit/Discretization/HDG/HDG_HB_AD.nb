(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     28935,        986]
NotebookOptionsPosition[     25364,        853]
NotebookOutlinePosition[     25716,        869]
CellTagsIndexPosition[     25673,        866]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"integrandt1phi1", " ", "=", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{
          RowBox[{"phi", 
           RowBox[{"(", " ", 
            RowBox[{"dudt2", " ", "+", " ", "dudt3"}], " ", ")"}]}], "/.", 
          " ", 
          RowBox[{"phi", " ", "->", " ", 
           RowBox[{"1", "-", "t"}]}]}], " ", "/.", " ", 
         RowBox[{"dudt2", " ", "->", " ", 
          RowBox[{"Pi", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{
              RowBox[{"(", 
               RowBox[{"-", "1"}], ")"}], "^", 
              RowBox[{"(", 
               RowBox[{"n", "-", "i"}], ")"}]}], " ", "/", " ", 
             RowBox[{"Sin", "[", " ", 
              RowBox[{"Pi", 
               RowBox[{
                RowBox[{"(", 
                 RowBox[{"n", "-", "i"}], ")"}], "/", "3"}]}], "]"}]}], ")"}],
            "u2"}]}]}], " ", "/.", " ", 
        RowBox[{"dudt3", " ", "\[Rule]", "  ", 
         RowBox[{"Pi", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{
             RowBox[{"(", 
              RowBox[{"-", "1"}], ")"}], "^", 
             RowBox[{"(", 
              RowBox[{"n", "-", "j"}], ")"}]}], " ", "/", " ", 
            RowBox[{"Sin", "[", " ", 
             RowBox[{"Pi", 
              RowBox[{
               RowBox[{"(", 
                RowBox[{"n", "-", "j"}], ")"}], "/", "3"}]}], "]"}]}], ")"}], 
          "u3"}]}]}], " ", "/.", " ", 
       RowBox[{"n", "\[Rule]", "1"}]}], " ", "/.", " ", 
      RowBox[{"i", " ", "\[Rule]", " ", "2"}]}], " ", "/.", " ", 
     RowBox[{"j", " ", "\[Rule]", " ", "3"}]}], "/.", " ", 
    RowBox[{"u2", " ", "\[Rule]", " ", "0"}]}], " ", "/.", " ", 
   RowBox[{"u3", " ", "\[Rule]", " ", 
    RowBox[{
     RowBox[{"2", "*", 
      RowBox[{"(", 
       RowBox[{"1", "-", "t"}], ")"}]}], "+", 
     RowBox[{"5", "t", " "}]}]}]}]}]], "Input",
 CellChangeTimes->{{3.6533273402650213`*^9, 3.6533277774644527`*^9}, {
  3.653327893209646*^9, 3.6533278934090023`*^9}, {3.653327947968401*^9, 
  3.653327955712002*^9}, {3.653328325950522*^9, 3.653328328332761*^9}, {
  3.653328834206196*^9, 3.653328836326017*^9}, {3.653328885702921*^9, 
  3.653328918139455*^9}, {3.653328980305835*^9, 3.653329019138105*^9}, {
  3.65332906780936*^9, 3.653329179212846*^9}, {3.653329263210389*^9, 
  3.653329266226083*^9}, {3.653329391064952*^9, 3.6533294054813213`*^9}, {
  3.6533294401889353`*^9, 3.653329449084937*^9}, {3.6533295335890207`*^9, 
  3.653329558540103*^9}}],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{"2", " ", "\[Pi]", " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "t"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"2", " ", 
       RowBox[{"(", 
        RowBox[{"1", "-", "t"}], ")"}]}], "+", 
      RowBox[{"5", " ", "t"}]}], ")"}]}], 
   SqrtBox["3"]]}]], "Output",
 CellChangeTimes->{{3.653329130160905*^9, 3.653329179503175*^9}, 
   3.6533292928711348`*^9, {3.6533293969078836`*^9, 3.653329405972589*^9}, {
   3.653329443403077*^9, 3.653329449370844*^9}, {3.653329536324781*^9, 
   3.6533295591381073`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt1phi2", " ", "=", " ", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{
          RowBox[{"phi", " ", 
           RowBox[{"(", 
            RowBox[{"dudt2", " ", "+", " ", "dudt3"}], ")"}]}], " ", "/.", 
          " ", 
          RowBox[{"phi", " ", "->", " ", "t"}]}], " ", "/.", " ", 
         RowBox[{"dudt2", " ", "->", " ", 
          RowBox[{
           RowBox[{"(", 
            RowBox[{"Pi", "/", "1"}], ")"}], " ", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{
              RowBox[{"(", 
               RowBox[{"-", "1"}], ")"}], "^", 
              RowBox[{"(", 
               RowBox[{"n", "-", "i"}], ")"}]}], " ", "/", " ", 
             RowBox[{"Sin", "[", " ", 
              RowBox[{"Pi", 
               RowBox[{
                RowBox[{"(", 
                 RowBox[{"n", "-", "i"}], ")"}], "/", "3"}]}], "]"}]}], ")"}],
            "u2"}]}]}], " ", "/.", "  ", 
        RowBox[{"dudt3", " ", "->", " ", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{"Pi", "/", "1"}], ")"}], 
          RowBox[{"(", 
           RowBox[{
            RowBox[{
             RowBox[{"(", 
              RowBox[{"-", "1"}], ")"}], "^", 
             RowBox[{"(", 
              RowBox[{"n", "-", "j"}], ")"}]}], " ", "/", " ", 
            RowBox[{"Sin", "[", " ", 
             RowBox[{"Pi", 
              RowBox[{
               RowBox[{"(", 
                RowBox[{"n", "-", "j"}], ")"}], "/", "3"}]}], "]"}]}], ")"}], 
          "u3"}]}]}], " ", "/.", " ", 
       RowBox[{"n", "\[Rule]", "1"}]}], " ", "/.", " ", 
      RowBox[{"i", " ", "\[Rule]", " ", "2"}]}], " ", "/.", " ", 
     RowBox[{"j", " ", "\[Rule]", " ", "3"}]}], "/.", " ", 
    RowBox[{"u2", " ", "\[Rule]", " ", "0"}]}], " ", "/.", " ", 
   RowBox[{"u3", " ", "\[Rule]", " ", 
    RowBox[{
     RowBox[{"2", 
      RowBox[{"(", 
       RowBox[{"1", "-", "t"}], ")"}]}], "+", 
     RowBox[{"5", "t", " "}]}]}]}]}]], "Input",
 CellChangeTimes->{{3.653328046916183*^9, 3.653328047732092*^9}, {
  3.653328946849113*^9, 3.653328983637454*^9}, {3.653329187117593*^9, 
  3.65332919626941*^9}, {3.653329299584365*^9, 3.653329299751992*^9}, {
  3.6533294742552643`*^9, 3.653329561932132*^9}}],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{"2", " ", "\[Pi]", " ", "t", " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"2", " ", 
       RowBox[{"(", 
        RowBox[{"1", "-", "t"}], ")"}]}], "+", 
      RowBox[{"5", " ", "t"}]}], ")"}]}], 
   SqrtBox["3"]]}]], "Output",
 CellChangeTimes->{
  3.653328048677018*^9, 3.653328950408009*^9, {3.6533291888599987`*^9, 
   3.65332919660592*^9}, 3.653329299991346*^9, {3.6533294755575314`*^9, 
   3.653329509864594*^9}, {3.653329540756638*^9, 3.6533295623960257`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt2phi1", " ", "=", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{
          RowBox[{"phi", 
           RowBox[{"(", " ", 
            RowBox[{"dudt1", " ", "+", " ", "dudt3"}], " ", ")"}]}], "/.", 
          " ", 
          RowBox[{"phi", " ", "->", " ", 
           RowBox[{"1", "-", "t"}]}]}], " ", "/.", " ", 
         RowBox[{"dudt1", " ", "->", " ", 
          RowBox[{"Pi", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{
              RowBox[{"(", 
               RowBox[{"-", "1"}], ")"}], "^", 
              RowBox[{"(", 
               RowBox[{"n", "-", "i"}], ")"}]}], " ", "/", " ", 
             RowBox[{"Sin", "[", " ", 
              RowBox[{"Pi", 
               RowBox[{
                RowBox[{"(", 
                 RowBox[{"n", "-", "i"}], ")"}], "/", "3"}]}], "]"}]}], ")"}],
            "u1"}]}]}], " ", "/.", " ", 
        RowBox[{"dudt3", " ", "\[Rule]", "  ", 
         RowBox[{"Pi", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{
             RowBox[{"(", 
              RowBox[{"-", "1"}], ")"}], "^", 
             RowBox[{"(", 
              RowBox[{"n", "-", "j"}], ")"}]}], " ", "/", " ", 
            RowBox[{"Sin", "[", " ", 
             RowBox[{"Pi", 
              RowBox[{
               RowBox[{"(", 
                RowBox[{"n", "-", "j"}], ")"}], "/", "3"}]}], "]"}]}], ")"}], 
          "u3"}]}]}], " ", "/.", " ", 
       RowBox[{"n", "\[Rule]", "2"}]}], " ", "/.", " ", 
      RowBox[{"i", " ", "\[Rule]", " ", "1"}]}], " ", "/.", " ", 
     RowBox[{"j", " ", "\[Rule]", " ", "3"}]}], "/.", " ", 
    RowBox[{"u1", " ", "\[Rule]", " ", 
     RowBox[{
      RowBox[{"1", "*", 
       RowBox[{"(", 
        RowBox[{"1", "-", "t"}], ")"}]}], " ", "+", " ", 
      RowBox[{"4", "t"}]}]}]}], " ", "/.", " ", 
   RowBox[{"u3", " ", "\[Rule]", " ", 
    RowBox[{
     RowBox[{"2", "*", 
      RowBox[{"(", 
       RowBox[{"1", "-", "t"}], ")"}]}], "+", 
     RowBox[{"5", "t", " "}]}]}]}]}]], "Input",
 CellChangeTimes->{{3.653329542299271*^9, 3.653329595691765*^9}, {
  3.6533296485675173`*^9, 3.653329648685626*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{"1", "-", "t"}], ")"}], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"-", 
     FractionBox[
      RowBox[{"2", " ", "\[Pi]", " ", 
       RowBox[{"(", 
        RowBox[{"1", "+", 
         RowBox[{"3", " ", "t"}]}], ")"}]}], 
      SqrtBox["3"]]}], "+", 
    FractionBox[
     RowBox[{"2", " ", "\[Pi]", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"2", " ", 
         RowBox[{"(", 
          RowBox[{"1", "-", "t"}], ")"}]}], "+", 
        RowBox[{"5", " ", "t"}]}], ")"}]}], 
     SqrtBox["3"]]}], ")"}]}]], "Output",
 CellChangeTimes->{{3.653329543979074*^9, 3.6533295665559196`*^9}, 
   3.653329598612558*^9, 3.653329657858039*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt2phi2", " ", "=", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{
          RowBox[{"phi", 
           RowBox[{"(", " ", 
            RowBox[{"dudt1", " ", "+", " ", "dudt3"}], " ", ")"}]}], "/.", 
          " ", 
          RowBox[{"phi", " ", "->", " ", "t"}]}], " ", "/.", " ", 
         RowBox[{"dudt1", " ", "->", " ", 
          RowBox[{"Pi", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{
              RowBox[{"(", 
               RowBox[{"-", "1"}], ")"}], "^", 
              RowBox[{"(", 
               RowBox[{"n", "-", "i"}], ")"}]}], " ", "/", " ", 
             RowBox[{"Sin", "[", " ", 
              RowBox[{"Pi", 
               RowBox[{
                RowBox[{"(", 
                 RowBox[{"n", "-", "i"}], ")"}], "/", "3"}]}], "]"}]}], ")"}],
            "u1"}]}]}], " ", "/.", " ", 
        RowBox[{"dudt3", " ", "\[Rule]", "  ", 
         RowBox[{"Pi", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{
             RowBox[{"(", 
              RowBox[{"-", "1"}], ")"}], "^", 
             RowBox[{"(", 
              RowBox[{"n", "-", "j"}], ")"}]}], " ", "/", " ", 
            RowBox[{"Sin", "[", " ", 
             RowBox[{"Pi", 
              RowBox[{
               RowBox[{"(", 
                RowBox[{"n", "-", "j"}], ")"}], "/", "3"}]}], "]"}]}], ")"}], 
          "u3"}]}]}], " ", "/.", " ", 
       RowBox[{"n", "\[Rule]", "2"}]}], " ", "/.", " ", 
      RowBox[{"i", " ", "\[Rule]", " ", "1"}]}], " ", "/.", " ", 
     RowBox[{"j", " ", "\[Rule]", " ", "3"}]}], "/.", " ", 
    RowBox[{"u1", " ", "\[Rule]", " ", 
     RowBox[{
      RowBox[{"1", "*", 
       RowBox[{"(", 
        RowBox[{"1", "-", "t"}], ")"}]}], " ", "+", " ", 
      RowBox[{"4", "t"}]}]}]}], " ", "/.", " ", 
   RowBox[{"u3", " ", "\[Rule]", " ", 
    RowBox[{
     RowBox[{"2", "*", 
      RowBox[{"(", 
       RowBox[{"1", "-", "t"}], ")"}]}], "+", 
     RowBox[{"5", "t", " "}]}]}]}]}]], "Input",
 CellChangeTimes->{{3.653329606276093*^9, 3.653329620540868*^9}, {
  3.6533296519994507`*^9, 3.653329652261731*^9}}],

Cell[BoxData[
 RowBox[{"t", " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"-", 
     FractionBox[
      RowBox[{"2", " ", "\[Pi]", " ", 
       RowBox[{"(", 
        RowBox[{"1", "+", 
         RowBox[{"3", " ", "t"}]}], ")"}]}], 
      SqrtBox["3"]]}], "+", 
    FractionBox[
     RowBox[{"2", " ", "\[Pi]", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"2", " ", 
         RowBox[{"(", 
          RowBox[{"1", "-", "t"}], ")"}]}], "+", 
        RowBox[{"5", " ", "t"}]}], ")"}]}], 
     SqrtBox["3"]]}], ")"}]}]], "Output",
 CellChangeTimes->{{3.653329606740033*^9, 3.653329623537133*^9}, 
   3.653329659895512*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt3phi1", " ", "=", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{
          RowBox[{"phi", 
           RowBox[{"(", " ", 
            RowBox[{"dudt1", " ", "+", " ", "dudt2"}], " ", ")"}]}], "/.", 
          " ", 
          RowBox[{"phi", " ", "->", " ", 
           RowBox[{"1", "-", "t"}]}]}], " ", "/.", " ", 
         RowBox[{"dudt1", " ", "->", " ", 
          RowBox[{"Pi", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{
              RowBox[{"(", 
               RowBox[{"-", "1"}], ")"}], "^", 
              RowBox[{"(", 
               RowBox[{"n", "-", "i"}], ")"}]}], " ", "/", " ", 
             RowBox[{"Sin", "[", " ", 
              RowBox[{"Pi", 
               RowBox[{
                RowBox[{"(", 
                 RowBox[{"n", "-", "i"}], ")"}], "/", "3"}]}], "]"}]}], ")"}],
            "u1"}]}]}], " ", "/.", " ", 
        RowBox[{"dudt2", " ", "\[Rule]", "  ", 
         RowBox[{"Pi", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{
             RowBox[{"(", 
              RowBox[{"-", "1"}], ")"}], "^", 
             RowBox[{"(", 
              RowBox[{"n", "-", "j"}], ")"}]}], " ", "/", " ", 
            RowBox[{"Sin", "[", " ", 
             RowBox[{"Pi", 
              RowBox[{
               RowBox[{"(", 
                RowBox[{"n", "-", "j"}], ")"}], "/", "3"}]}], "]"}]}], ")"}], 
          "u2"}]}]}], " ", "/.", " ", 
       RowBox[{"n", "\[Rule]", "3"}]}], " ", "/.", " ", 
      RowBox[{"i", " ", "\[Rule]", " ", "1"}]}], " ", "/.", " ", 
     RowBox[{"j", " ", "\[Rule]", " ", "2"}]}], "/.", " ", 
    RowBox[{"u1", " ", "\[Rule]", " ", 
     RowBox[{
      RowBox[{"1", "*", 
       RowBox[{"(", 
        RowBox[{"1", "-", "t"}], ")"}]}], " ", "+", " ", 
      RowBox[{"4", "t"}]}]}]}], " ", "/.", " ", 
   RowBox[{"u2", " ", "\[Rule]", " ", "0"}]}]}]], "Input",
 CellChangeTimes->{{3.653329617348414*^9, 3.653329680838468*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{"2", " ", "\[Pi]", " ", 
   RowBox[{"(", 
    RowBox[{"1", "-", "t"}], ")"}], " ", 
   RowBox[{"(", 
    RowBox[{"1", "+", 
     RowBox[{"3", " ", "t"}]}], ")"}]}], 
  SqrtBox["3"]]], "Output",
 CellChangeTimes->{3.6533296914659986`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt3phi2", " ", "=", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{
          RowBox[{"phi", 
           RowBox[{"(", " ", 
            RowBox[{"dudt1", " ", "+", " ", "dudt2"}], " ", ")"}]}], "/.", 
          " ", 
          RowBox[{"phi", " ", "->", " ", "t"}]}], " ", "/.", " ", 
         RowBox[{"dudt1", " ", "->", " ", 
          RowBox[{"Pi", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{
              RowBox[{"(", 
               RowBox[{"-", "1"}], ")"}], "^", 
              RowBox[{"(", 
               RowBox[{"n", "-", "i"}], ")"}]}], " ", "/", " ", 
             RowBox[{"Sin", "[", " ", 
              RowBox[{"Pi", 
               RowBox[{
                RowBox[{"(", 
                 RowBox[{"n", "-", "i"}], ")"}], "/", "3"}]}], "]"}]}], ")"}],
            "u1"}]}]}], " ", "/.", " ", 
        RowBox[{"dudt2", " ", "\[Rule]", "  ", 
         RowBox[{"Pi", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{
             RowBox[{"(", 
              RowBox[{"-", "1"}], ")"}], "^", 
             RowBox[{"(", 
              RowBox[{"n", "-", "j"}], ")"}]}], " ", "/", " ", 
            RowBox[{"Sin", "[", " ", 
             RowBox[{"Pi", 
              RowBox[{
               RowBox[{"(", 
                RowBox[{"n", "-", "j"}], ")"}], "/", "3"}]}], "]"}]}], ")"}], 
          "u2"}]}]}], " ", "/.", " ", 
       RowBox[{"n", "\[Rule]", "3"}]}], " ", "/.", " ", 
      RowBox[{"i", " ", "\[Rule]", " ", "1"}]}], " ", "/.", " ", 
     RowBox[{"j", " ", "\[Rule]", " ", "2"}]}], "/.", " ", 
    RowBox[{"u1", " ", "\[Rule]", " ", 
     RowBox[{
      RowBox[{"1", "*", 
       RowBox[{"(", 
        RowBox[{"1", "-", "t"}], ")"}]}], " ", "+", " ", 
      RowBox[{"4", "t"}]}]}]}], " ", "/.", " ", 
   RowBox[{"u2", " ", "\[Rule]", " ", "0"}]}]}]], "Input",
 CellChangeTimes->{{3.653329693495098*^9, 3.653329695183052*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{"2", " ", "\[Pi]", " ", "t", " ", 
   RowBox[{"(", 
    RowBox[{"1", "+", 
     RowBox[{"3", " ", "t"}]}], ")"}]}], 
  SqrtBox["3"]]], "Output",
 CellChangeTimes->{3.6533296956753607`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt1phi1", " ", "/.", " ", 
  RowBox[{"t", " ", "\[Rule]", " ", "0"}]}]], "Input",
 CellChangeTimes->{{3.6533296983206577`*^9, 3.653329712191074*^9}}],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{"4", " ", "\[Pi]"}], 
   SqrtBox["3"]]}]], "Output",
 CellChangeTimes->{3.653329712713097*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt1phi1", " ", "/.", 
  RowBox[{"t", "\[Rule]", " ", "1"}]}]], "Input",
 CellChangeTimes->{{3.653329715466139*^9, 3.6533297251354027`*^9}}],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{3.6533297255838633`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt1phi1", " ", "/.", 
  RowBox[{"t", "\[Rule]", 
   RowBox[{"1", "/", "2"}]}]}]], "Input",
 CellChangeTimes->{{3.653329727619875*^9, 3.653329735327633*^9}}],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{"7", " ", "\[Pi]"}], 
   RowBox[{"2", " ", 
    SqrtBox["3"]}]]}]], "Output",
 CellChangeTimes->{3.6533297356413507`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt1phi2", " ", "/.", 
  RowBox[{"t", "\[Rule]", "0"}]}]], "Input",
 CellChangeTimes->{{3.653329748680307*^9, 3.6533297501282673`*^9}}],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{3.6533297505400543`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt1phi2", " ", "/.", 
  RowBox[{"t", "\[Rule]", "1"}]}]], "Input",
 CellChangeTimes->{{3.653329752513874*^9, 3.6533297553603373`*^9}}],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{"10", " ", "\[Pi]"}], 
   SqrtBox["3"]]}]], "Output",
 CellChangeTimes->{3.653329755713924*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt1phi2", " ", "/.", 
  RowBox[{"t", "\[Rule]", 
   RowBox[{"1", "/", "2"}]}]}]], "Input",
 CellChangeTimes->{{3.653329760032626*^9, 3.6533297601366463`*^9}}],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{"7", " ", "\[Pi]"}], 
   RowBox[{"2", " ", 
    SqrtBox["3"]}]]}]], "Output",
 CellChangeTimes->{3.65332976044726*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt2phi1", " ", "/.", 
  RowBox[{"t", "\[Rule]", "0"}]}]], "Input",
 CellChangeTimes->{{3.6533297672409487`*^9, 3.653329768656872*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{"2", " ", "\[Pi]"}], 
  SqrtBox["3"]]], "Output",
 CellChangeTimes->{3.65332976906609*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt2phi1", " ", "/.", 
  RowBox[{"t", "\[Rule]", "1"}]}]], "Input",
 CellChangeTimes->{{3.65332977269693*^9, 3.653329780209105*^9}}],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{{3.65332977685515*^9, 3.653329780455061*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt2phi1", " ", "/.", 
  RowBox[{"t", "\[Rule]", 
   RowBox[{"1", "/", "2"}]}]}]], "Input",
 CellChangeTimes->{{3.653329800324938*^9, 3.653329802971222*^9}}],

Cell[BoxData[
 FractionBox["\[Pi]", 
  SqrtBox["3"]]], "Output",
 CellChangeTimes->{3.653329803278432*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt2phi2", " ", "/.", 
  RowBox[{"t", "\[Rule]", "0"}]}]], "Input",
 CellChangeTimes->{{3.653329821906551*^9, 3.6533298233705797`*^9}}],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{3.6533298263970737`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt2phi2", " ", "/.", 
  RowBox[{"t", "\[Rule]", "1"}]}]], "Input",
 CellChangeTimes->{{3.6533298304124203`*^9, 3.653329833722986*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{"2", " ", "\[Pi]"}], 
  SqrtBox["3"]]], "Output",
 CellChangeTimes->{3.653329834156139*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt2phi2", " ", "/.", 
  RowBox[{"t", "\[Rule]", 
   RowBox[{"1", "/", "2"}]}]}]], "Input",
 CellChangeTimes->{{3.6533298368207283`*^9, 3.6533298405092983`*^9}}],

Cell[BoxData[
 FractionBox["\[Pi]", 
  SqrtBox["3"]]], "Output",
 CellChangeTimes->{{3.6533298376139812`*^9, 3.653329840849543*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt3phi1", " ", "/.", 
  RowBox[{"t", "\[Rule]", "0"}]}]], "Input",
 CellChangeTimes->{{3.653329821906551*^9, 3.6533298233705797`*^9}, {
  3.6533298811971893`*^9, 3.653329882797161*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{"2", " ", "\[Pi]"}], 
  SqrtBox["3"]]], "Output",
 CellChangeTimes->{3.6533298263970737`*^9, 3.6533298831283083`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt3phi1", " ", "/.", 
  RowBox[{"t", "\[Rule]", "1"}]}]], "Input",
 CellChangeTimes->{{3.653329888670961*^9, 3.653329889509528*^9}}],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{3.653329889831284*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt3phi1", " ", "/.", 
  RowBox[{"t", "\[Rule]", 
   RowBox[{"1", "/", "2"}]}]}]], "Input",
 CellChangeTimes->{{3.653329891815136*^9, 3.6533298921655407`*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{"5", " ", "\[Pi]"}], 
  RowBox[{"2", " ", 
   SqrtBox["3"]}]]], "Output",
 CellChangeTimes->{3.653329892447626*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt3phi2", " ", "/.", 
  RowBox[{"t", "\[Rule]", "0"}]}]], "Input",
 CellChangeTimes->{{3.653329925406739*^9, 3.6533299255267477`*^9}}],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{3.653329927335475*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt3phi2", " ", "/.", 
  RowBox[{"t", "\[Rule]", "1"}]}]], "Input",
 CellChangeTimes->{{3.653329929846772*^9, 3.653329931710938*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{"8", " ", "\[Pi]"}], 
  SqrtBox["3"]]], "Output",
 CellChangeTimes->{3.653329931940483*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"integrandt3phi2", " ", "/.", 
  RowBox[{"t", "\[Rule]", 
   RowBox[{"1", "/", "2"}]}]}]], "Input",
 CellChangeTimes->{{3.653329944755516*^9, 3.653329947162217*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{"5", " ", "\[Pi]"}], 
  RowBox[{"2", " ", 
   SqrtBox["3"]}]]], "Output",
 CellChangeTimes->{3.6533299473857737`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Integrate", "[", 
  RowBox[{"integrandt1phi1", ",", 
   RowBox[{"{", 
    RowBox[{"t", ",", "0", ",", "1"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.6533300139428368`*^9, 3.6533300289329453`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", 
   SqrtBox["3"]}], " ", "\[Pi]"}]], "Output",
 CellChangeTimes->{3.653330029405216*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Integrate", "[", 
  RowBox[{"integrandt1phi2", ",", 
   RowBox[{"{", 
    RowBox[{"t", ",", "0", ",", "1"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.6533300507991323`*^9, 3.653330050949636*^9}}],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{"4", " ", "\[Pi]"}], 
   SqrtBox["3"]]}]], "Output",
 CellChangeTimes->{3.653330051197171*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Integrate", "[", 
  RowBox[{"integrandt2phi1", ",", 
   RowBox[{"{", 
    RowBox[{"t", ",", "0", ",", "1"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.653330084274186*^9, 3.653330084416832*^9}}],

Cell[BoxData[
 FractionBox["\[Pi]", 
  SqrtBox["3"]]], "Output",
 CellChangeTimes->{3.653330085826352*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Integrate", "[", 
  RowBox[{"integrandt2phi2", ",", 
   RowBox[{"{", 
    RowBox[{"t", ",", "0", ",", "1"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.6533300907865477`*^9, 3.653330092248802*^9}}],

Cell[BoxData[
 FractionBox["\[Pi]", 
  SqrtBox["3"]]], "Output",
 CellChangeTimes->{3.653330092523629*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Integrate", "[", 
  RowBox[{"integrandt3phi1", ",", 
   RowBox[{"{", 
    RowBox[{"t", ",", "0", ",", "1"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.653330113869672*^9, 3.6533301139884863`*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{"2", " ", "\[Pi]"}], 
  SqrtBox["3"]]], "Output",
 CellChangeTimes->{3.6533301144957027`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Integrate", "[", 
  RowBox[{"integrandt3phi2", ",", 
   RowBox[{"{", 
    RowBox[{"t", ",", "0", ",", "1"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.653330128030053*^9, 3.653330129988619*^9}}],

Cell[BoxData[
 RowBox[{
  SqrtBox["3"], " ", "\[Pi]"}]], "Output",
 CellChangeTimes->{3.6533301302508593`*^9}]
}, Open  ]]
},
WindowSize->{808, 651},
WindowMargins->{{4, Automatic}, {Automatic, 4}},
FrontEndVersion->"10.2 for Mac OS X x86 (32-bit, 64-bit Kernel) (July 29, \
2015)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 2546, 64, 80, "Input"],
Cell[3129, 88, 584, 16, 52, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3750, 109, 2295, 62, 97, "Input"],
Cell[6048, 173, 529, 14, 52, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6614, 192, 2198, 62, 80, "Input"],
Cell[8815, 256, 700, 23, 53, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9552, 284, 2166, 61, 80, "Input"],
Cell[11721, 347, 629, 21, 53, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12387, 373, 2015, 56, 80, "Input"],
Cell[14405, 431, 275, 9, 52, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[14717, 445, 1983, 55, 80, "Input"],
Cell[16703, 502, 226, 7, 52, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[16966, 514, 181, 3, 28, "Input"],
Cell[17150, 519, 149, 5, 52, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[17336, 529, 171, 3, 28, "Input"],
Cell[17510, 534, 72, 1, 28, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[17619, 540, 188, 4, 28, "Input"],
Cell[17810, 546, 176, 6, 51, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[18023, 557, 166, 3, 28, "Input"],
Cell[18192, 562, 72, 1, 28, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[18301, 568, 166, 3, 28, "Input"],
Cell[18470, 573, 150, 5, 52, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[18657, 583, 190, 4, 28, "Input"],
Cell[18850, 589, 173, 6, 51, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[19060, 600, 166, 3, 28, "Input"],
Cell[19229, 605, 128, 4, 52, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[19394, 614, 163, 3, 28, "Input"],
Cell[19560, 619, 93, 1, 28, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[19690, 625, 188, 4, 28, "Input"],
Cell[19881, 631, 106, 3, 49, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[20024, 639, 166, 3, 28, "Input"],
Cell[20193, 644, 72, 1, 28, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[20302, 650, 166, 3, 28, "Input"],
Cell[20471, 655, 129, 4, 52, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[20637, 664, 192, 4, 28, "Input"],
Cell[20832, 670, 132, 3, 49, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[21001, 678, 217, 4, 28, "Input"],
Cell[21221, 684, 155, 4, 52, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[21413, 693, 164, 3, 28, "Input"],
Cell[21580, 698, 70, 1, 28, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[21687, 704, 190, 4, 28, "Input"],
Cell[21880, 710, 153, 5, 51, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[22070, 720, 166, 3, 28, "Input"],
Cell[22239, 725, 70, 1, 28, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[22346, 731, 164, 3, 28, "Input"],
Cell[22513, 736, 129, 4, 52, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[22679, 745, 188, 4, 28, "Input"],
Cell[22870, 751, 155, 5, 51, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[23062, 761, 228, 5, 28, "Input"],
Cell[23293, 768, 127, 4, 33, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[23457, 777, 226, 5, 28, "Input"],
Cell[23686, 784, 149, 5, 52, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[23872, 794, 224, 5, 28, "Input"],
Cell[24099, 801, 106, 3, 49, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[24242, 809, 226, 5, 28, "Input"],
Cell[24471, 816, 106, 3, 49, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[24614, 824, 226, 5, 28, "Input"],
Cell[24843, 831, 131, 4, 52, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[25011, 840, 224, 5, 28, "Input"],
Cell[25238, 847, 110, 3, 33, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
