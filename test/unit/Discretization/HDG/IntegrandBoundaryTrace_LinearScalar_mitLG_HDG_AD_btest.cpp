// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandBoundaryTrace_LinearScalar_mitLG_HDG_AD_btest
// testing of boundary integrands: HDG Advection-Diffusion on Triangles

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_mitLG_HDG.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"

#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_None> PDEAdvectionDiffusion1D;
typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_None> PDEAdvectionDiffusion2D;
//typedef PDEAdvectionDiffusion<PhysD3,
//                              AdvectiveFlux3D_Uniform,
//                              ViscousFlux3D_Uniform,
//                              Source3D_None> PDEAdvectionDiffusion3D;

typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass1D;
typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass2D;
//typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass3D;

typedef BCNDConvertSpace<PhysD1, BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_mitLG> > BCClass1D;
typedef BCNDConvertSpace<PhysD2, BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_mitLG> > BCClass2D;
//typedef BCNDConvertSpace<PhysD3, BCAdvectionDiffusion<PhysD3,BCTypeLinearRobin_mitLG> > BCClass3D;

typedef NDVectorCategory<boost::mpl::vector1<BCClass1D>, BCClass1D::Category> NDBCVecCat1D;
typedef NDVectorCategory<boost::mpl::vector1<BCClass2D>, BCClass2D::Category> NDBCVecCat2D;
//typedef NDVectorCategory<boost::mpl::vector1<BCClass3D>, BCClass3D::Category> NDBCVecCat3D;

typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldLine;
typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldArea_Triangle;
typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldArea_Quad;
//typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldVolume_Tet;
//typedef ElementXField<PhysD3,TopoD3,Hex> ElementXFieldVolume_Hex;

// 1D
template class IntegrandBoundaryTrace<PDEClass1D, NDBCVecCat1D, HDG>
               ::BasisWeighted<Real, TopoD0, Node, TopoD1, Line, ElementXFieldLine>;
// 2D
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, HDG>
               ::BasisWeighted<Real, TopoD1, Line, TopoD2, Triangle, ElementXFieldArea_Triangle>;
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, HDG>
               ::BasisWeighted<Real, TopoD1, Line, TopoD2, Quad, ElementXFieldArea_Quad>;
// 3D
//template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
//               ::BasisWeighted<Real, TopoD2, Triangle, TopoD3, Tet, ElementXFieldVolume_Tet>;
//template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
//               ::BasisWeighted<Real, TopoD2, Quad, TopoD3, Hex, ElementXFieldVolume_Hex>;

// 1D
template class IntegrandBoundaryTrace<PDEClass1D, NDBCVecCat1D, HDG>
               ::FieldWeighted<Real, Real, TopoD0, Node, TopoD1, Line, ElementXFieldLine>;
// 2D
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, HDG>
               ::FieldWeighted<Real, Real, TopoD1, Line, TopoD2, Triangle, ElementXFieldArea_Triangle>;
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, HDG>
               ::FieldWeighted<Real, Real, TopoD1, Line, TopoD2, Quad, ElementXFieldArea_Quad>;
// 3D
//template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
//               ::FieldWeighted<Real, Real, TopoD2, Triangle, TopoD3, Tet, ElementXFieldVolume_Tet>;
//template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
//               ::FieldWeighted<Real, Real, TopoD2, Quad, TopoD3, Hex, ElementXFieldVolume_Hex>;
}

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandBoundaryTrace_LinearScalar_FieldTrace_mitLG_HDG_AD_btest_suite )

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Line_BCTypeLinearRobin_X1Q1LG1_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef Element<ArrayQ,TopoD0,Node> ElementQFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementAFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> BasisWeightedClass;
  typedef IntegrandClass::BasisWeighted_AUX<Real,Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> BasisWeightedAUXClass;

  // PDE
  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC
  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  ElementXFieldTrace xfldTrace(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xfldTrace.order() );
  BOOST_CHECK_EQUAL( 1, xfldTrace.nDOF() );

  // adjacent line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = {x1};
  xfldElem.DOF(1) = {x2};

  xfldTrace.DOF(0) = {x2};
  xfldTrace.normalSignL() = 1;

  // solution

  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // triangle solution (left)
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 4;

  // auxilliary variable
  ElementAFieldCell afldElem(order, BasisFunctionCategory_Hierarchical);

  // line solution (left)
  afldElem.DOF(0) = 2;
  afldElem.DOF(1) = 7;

  // interface solution
  ElementQFieldTrace qIfldElem(0, BasisFunctionCategory_Legendre);

  qIfldElem.DOF(0) = 8;

  // Lagrange multiplier
  ElementQFieldTrace lgfldElem(0, BasisFunctionCategory_Legendre);

  lgfldElem.DOF(0) = 7;

  // integrand functor (needs to be Gradient)
  DiscretizationHDG<PDEClass> disc( pde, Local, Gradient );
  const std::vector<int> BoundaryGroups = {1};
  IntegrandClass fcnbc( pde, bc, BoundaryGroups, disc );

  BasisWeightedClass fcn = fcnbc.integrand( xfldTrace, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, afldElem, qIfldElem, lgfldElem );
  BasisWeightedAUXClass fcnAUX = fcnbc.integrand_AUX( xfldTrace, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, afldElem, qIfldElem, lgfldElem );

  BOOST_CHECK_EQUAL( 2, fcn.nDOFCell() );
  BOOST_CHECK_EQUAL( 1, fcn.nDOFTrace() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  BOOST_CHECK_EQUAL( 2, fcnAUX.nDOFCell() );
  BOOST_CHECK_EQUAL( 1, fcnAUX.nDOFTrace() );
  BOOST_CHECK( fcnAUX.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;
  ArrayQ integrandPDETrue[2];
  ArrayQ integrandAUXxTrue[2];
  ArrayQ integrandINTTrue[1];
  ArrayQ integrandBCTrue[1];
  BasisWeightedClass::IntegrandCellType integrandPDE[2];
  BasisWeightedClass::IntegrandTraceType integrandINT[1];
  BasisWeightedAUXClass::IntegrandType integrandAUX[2];
  BasisWeightedClass::IntegrandTraceType integrandBC[1];

  // Test at sRef={0}, s={1}
  sRef = {0};
  fcn( sRef, integrandPDE, 2, integrandINT, 1, integrandBC, 1 );
  fcnAUX( sRef, integrandAUX, 2 );

  //PDE residual integrands: (advective) + (diffusive) + (bc)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + ( 0 ) ;  // Basis function 1
  integrandPDETrue[1] = ( 22/5. ) + ( -14861/1000. ) + ( 2563133/65000. ) ;  // Basis function 2

  //Auxiliary integrands:
  integrandAUXxTrue[0] = ( 0 );  // Basis function 1
  integrandAUXxTrue[1] = ( -145083/6500. );  // Basis function 2

  //PDE Trace residual integrands: (stabilization)
  integrandINTTrue[0] = ( 2123/250. );  // Basis function 1

  //BC residual integrands:
  integrandBCTrue[0] = ( 55583/1000. );  // Basis function 1

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandAUXxTrue[0], integrandAUX[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[1], integrandAUX[1][0], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandINTTrue[0], integrandINT[0], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBCTrue[0], integrandBC[0], small_tol, close_tol );

  // TODO: Need another version of GalerkinWeightedIntegral that handles 3 templates
  // test the trace element integral of the functor

  int quadratureorder = 0;
  int nIntegrand = qfldElem.nDOF();
  int nIntegrandI = qIfldElem.nDOF();
  int nIntegrandBC = lgfldElem.nDOF();
  typedef BasisWeightedClass::IntegrandCellType IntegrandPDEType;
  typedef BasisWeightedClass::IntegrandTraceType IntegrandINTType;
  typedef BasisWeightedClass::IntegrandTraceType IntegrandBCType;
  typedef BasisWeightedAUXClass::IntegrandType IntegrandAUXType;
  GalerkinWeightedIntegral<TopoD0, Node, IntegrandPDEType, IntegrandINTType, IntegrandBCType> integral(quadratureorder,
                                                                                                       nIntegrand, nIntegrandI, nIntegrandBC);
  GalerkinWeightedIntegral<TopoD0, Node, IntegrandAUXType> integralAUX(quadratureorder, nIntegrand);

  ArrayQ rsdPDETrue[2], rsdINTTrue[1], rsdBCTrue[1], rsdAUXxTrue[2];

  IntegrandPDEType rsdPDE[2];
  IntegrandINTType rsdINT[1];
  IntegrandINTType rsdBC[1];
  IntegrandAUXType rsdAUX[2];

  // cell integration for canonical element
  integral( fcn, xfldTrace, rsdPDE, nIntegrand, rsdINT, nIntegrandI, rsdBC, nIntegrandBC );
  integralAUX( fcnAUX, xfldTrace, rsdAUX, nIntegrand );

  //PDE residuals (left): (advective) + (viscous) + (bc)
  rsdPDETrue[0] = ( 0 ) +  ( 0 ) +  ( 0 ); // Basis function 1
  rsdPDETrue[1] = ( 22/5. ) +  ( -14861/1000. ) +  ( 2563133/65000. ); // Basis function 2

  rsdAUXxTrue[0] = ( 0 ); // Basis function 1
  rsdAUXxTrue[1] = ( -145083/6500. ); // Basis function 2

  rsdINTTrue[0] = ( 2123/250. ); // Basis function 1

  rsdBCTrue[0] = ( 55583/1000. ); // Basis function 1

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDE[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdAUXxTrue[0], rsdAUX[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXxTrue[1], rsdAUX[1][0], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdINTTrue[0], rsdINT[0], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdBCTrue[0], rsdBC[0], small_tol, close_tol );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_Line_BCTypeLinearRobin_X1Q1LG1_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef Element<ArrayQ,TopoD0,Node> ElementQFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementAFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> FieldWeightedClass;

  // PDE
  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // BC
  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  ElementXFieldTrace xfldTrace(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xfldTrace.order() );
  BOOST_CHECK_EQUAL( 1, xfldTrace.nDOF() );

  // adjacent line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = {x1};
  xfldElem.DOF(1) = {x2};

  xfldTrace.DOF(0) = {x2};
  xfldTrace.normalSignL() = 1;

  // solution

  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // triangle solution (left)
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 4;

  // auxilliary variable
  ElementAFieldCell afldElem(order, BasisFunctionCategory_Hierarchical);

  // line solution (left)
  afldElem.DOF(0) = 2;
  afldElem.DOF(1) = 7;

  // interface solution
  ElementQFieldTrace qIfldElem(0, BasisFunctionCategory_Legendre);

  qIfldElem.DOF(0) =  8;

  // Lagrange multiplier
  ElementQFieldTrace lgfldElem(0, BasisFunctionCategory_Legendre);

  lgfldElem.DOF(0) =  7;

  // weight
  ElementQFieldCell wfldElem(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElem.order() );
  BOOST_CHECK_EQUAL( 3, wfldElem.nDOF() );

  // triangle solution (left)
  wfldElem.DOF(0) = 3;
  wfldElem.DOF(1) = 4;
  wfldElem.DOF(2) = 5;

  // auxiliary variable
  ElementAFieldCell bfldElem(order+1, BasisFunctionCategory_Hierarchical);

  // line solution (left)
  bfldElem.DOF(0) = -5;
  bfldElem.DOF(1) =  3;
  bfldElem.DOF(2) =  2;

  // interface weight
  ElementQFieldTrace wIfldElem(0, BasisFunctionCategory_Legendre);

  wIfldElem.DOF(0) = -2;

  // Lagrange multiplier
  ElementQFieldTrace mufldElem(0, BasisFunctionCategory_Legendre);

  mufldElem.DOF(0) =  3;

  // integrand functor
  DiscretizationHDG<PDEClass> disc( pde, Local, Gradient );
  const std::vector<int> BoundaryGroups = {1};
  IntegrandClass fcnbc( pde, bc, BoundaryGroups, disc );

  FieldWeightedClass fcn = fcnbc.integrand( xfldTrace, CanonicalTraceToCell(0, 1),
                                            xfldElem,
                                            qfldElem, afldElem,
                                            wfldElem, bfldElem,
                                            qIfldElem, lgfldElem,
                                            wIfldElem, mufldElem );

  BOOST_CHECK_EQUAL( 2, fcn.nDOFCell() );
  BOOST_CHECK_EQUAL( 1, fcn.nDOFTrace() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;
  Real integrandPDETrue, integrandAUXTrue, integrandINTTrue, integrandBCTrue;
  FieldWeightedClass::IntegrandCellType integrand = 0;
  FieldWeightedClass::IntegrandTraceType integrandBC = 0, integrandINT = 0;

  // Test at sRef={0}, s={1}
  sRef = {0};
  fcn( sRef, integrand, integrandINT, integrandBC );

  //PDE residual integrands: (advective) + (diffusive) + (bc)
  integrandPDETrue = ( 88/5. ) + ( -14861/250. ) + ( 2563133/16250. ) ;  // Basis function

  //Auxiliary integrands:
  integrandAUXTrue = ( -435249/6500. );  // Basis function

  //PDE Trace residual integrands: (stabilization)
  integrandINTTrue = ( -2123/125. );  // Basis function

  //BC residual integrands:
  integrandBCTrue = ( 166749/1000. );  // Basis function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXTrue, integrand.Au, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandINTTrue, integrandINT, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCTrue, integrandBC, small_tol, close_tol );

  // test the trace element integral of the functor
  int quadratureorder = 0;
  typedef FieldWeightedClass::IntegrandCellType IntegrandCellType;
  ElementIntegral<TopoD0, Node, IntegrandCellType, Real, Real> integral(quadratureorder);

  ArrayQ rsdPDETrue, rsdAUXTrue, rsdINTTrue, rsdBCTrue;

  IntegrandCellType rsdCell;
  ArrayQ rsdINT, rsdBC;

  // cell integration for canonical element
  integral( fcn, xfldTrace, rsdCell, rsdINT, rsdBC );

  //PDE residuals (left): (advective) + (viscous) + (bc)
  rsdPDETrue = ( 88/5. ) +  ( -14861/250. ) +  ( 2563133/16250. ); // Basis function

  rsdAUXTrue = ( -435249/6500. ); // Basis function

  rsdINTTrue = ( -2123/125. ); // Basis function

  rsdBCTrue = ( 166749/1000. ); // Basis function

  SANS_CHECK_CLOSE( rsdPDETrue, rsdCell.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXTrue, rsdCell.Au, small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdINTTrue, rsdINT, small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue, rsdBC, small_tol, close_tol );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_1D_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;

  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef Element<ArrayQ,TopoD0,Node> ElementQFieldTrace;
  typedef Element<DLA::VectorS<1,ArrayQ>,TopoD1,Line> ElementAFieldCell;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD0,Node,TopoD1,Line,ElementParam> BasisWeightedClass;
  typedef IntegrandClass::BasisWeighted_AUX<Real,Real,TopoD0,Node,TopoD1,Line,ElementParam> BasisWeightedAUXClass;
  typedef IntegrandClass::FieldWeighted<Real,Real,TopoD0,Node,TopoD1,Line,ElementParam> FieldWeightedClass;
  typedef DiscretizationHDG<PDEClass> DiscretizationClass;


  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3;
  Source1D_Uniform source(a);

  // PDE
  PDEClass pde( adv, visc, source );

  // BC
  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  // grid
  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  // adjacent line grid
  Real x2, x3;

  x2 = 1;
  x3 = 2;

  xfldElemL.DOF(0) = x2;
  xfldElemL.DOF(1) = x3;

  xnode.DOF(0) = x2;
  xnode.normalSignL() = 1;

  for (int qorder = 2; qorder< 4; qorder++)
  {
    // solution
    ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementAFieldCell afldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementAFieldCell bfldElemL(qorder, BasisFunctionCategory_Hierarchical);

    ElementQFieldTrace qIfldTrace(0, BasisFunctionCategory_Legendre);
    ElementQFieldTrace wIfldTrace(0, BasisFunctionCategory_Legendre);

    // Lagrange multiplier
    ElementQFieldTrace lgfldTrace(0, BasisFunctionCategory_Legendre);
    ElementQFieldTrace mufldTrace(0, BasisFunctionCategory_Legendre);

    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), afldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), wfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), bfldElemL.nDOF() );

    // line solution
    for ( int dof = 0; dof < qfldElemL.nDOF(); dof ++ )
    {
      qfldElemL.DOF(dof) = (dof+1)*pow(-1,dof);
      wfldElemL.DOF(dof) = 0;

      afldElemL.DOF(dof) = (dof+2)*pow(-1,dof+1);
      bfldElemL.DOF(dof) = 0;
    }

    BOOST_CHECK_EQUAL( qIfldTrace.nDOF(), wIfldTrace.nDOF() );

    for (int dof = 0; dof < qIfldTrace.nDOF(); dof++)
    {
      qIfldTrace.DOF(dof) = (dof+1)*pow(-1,dof);
      wIfldTrace.DOF(dof) = 0;
    }

    BOOST_CHECK_EQUAL( lgfldTrace.nDOF(), mufldTrace.nDOF() );

    for (int dof = 0; dof < lgfldTrace.nDOF(); dof++)
    {
      lgfldTrace.DOF(dof) = (dof+3)*pow(-1,dof);
      mufldTrace.DOF(dof) = 0;
    }

    // HDG discretization
    DiscretizationClass disc( pde, Global, Gradient );

    // integrand
    const std::vector<int> BoundaryGroups = {1};

    IntegrandClass fcnbc( pde, bc, BoundaryGroups, disc );

    // integrand functor
    BasisWeightedClass fcnB = fcnbc.integrand( xnode,
                                               CanonicalTraceToCell(0,1),
                                               xfldElemL,
                                               qfldElemL, afldElemL,
                                               qIfldTrace, lgfldTrace );

    BasisWeightedAUXClass fcnAUX = fcnbc.integrand_AUX( xnode,
                                                        CanonicalTraceToCell(0,1),
                                                        xfldElemL,
                                                        qfldElemL, afldElemL,
                                                        qIfldTrace, lgfldTrace );
    // integrand functor
    FieldWeightedClass fcnW = fcnbc.integrand( xnode,
                                               CanonicalTraceToCell(0,1),
                                               xfldElemL,
                                               qfldElemL, afldElemL,
                                               wfldElemL, bfldElemL,
                                               qIfldTrace, lgfldTrace,
                                               wIfldTrace, mufldTrace );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;

    int nIntegrandL = qfldElemL.nDOF();
    int nIntegrandI = qIfldTrace.nDOF();
    int nIntegrandBC = lgfldTrace.nDOF();

    int quadratureorder = 0;
    GalerkinWeightedIntegral<TopoD0, Node, BasisWeightedClass::IntegrandCellType,
                                           BasisWeightedClass::IntegrandTraceType,
                                           BasisWeightedClass::IntegrandTraceType>
                                           integralB(quadratureorder, nIntegrandL, nIntegrandI, nIntegrandBC);

    GalerkinWeightedIntegral<TopoD0, Node, BasisWeightedAUXClass::IntegrandType> integralAUX(quadratureorder, nIntegrandL);

    ElementIntegral<TopoD0, Node, FieldWeightedClass::IntegrandCellType,
                                  FieldWeightedClass::IntegrandTraceType,
                                  FieldWeightedClass::IntegrandTraceType> integralW(quadratureorder);
    std::vector<BasisWeightedClass::IntegrandCellType> rsdElemBL(nIntegrandL, 0);
    std::vector<BasisWeightedClass::IntegrandTraceType> rsdElemBI(nIntegrandI, 0);
    std::vector<BasisWeightedClass::IntegrandTraceType> rsdElemBC(nIntegrandBC, 0);
    std::vector<BasisWeightedAUXClass::IntegrandType> rsdElemAUXL(nIntegrandL, 0);
    FieldWeightedClass::IntegrandCellType rsdElemWL=0;
    FieldWeightedClass::IntegrandTraceType rsdElemWI=0;
    FieldWeightedClass::IntegrandTraceType rsdElemWBC=0;

    // cell integration for canonical element
    integralB( fcnB, xnode,
               rsdElemBL.data(), rsdElemBL.size(),
               rsdElemBI.data(), rsdElemBI.size(),
               rsdElemBC.data(), rsdElemBC.size() );
    integralAUX( fcnAUX, xnode, rsdElemAUXL.data(), nIntegrandL );

    for (int i = 0; i < wfldElemL.nDOF(); i++ )
    {
      // set just one of the weights to one
      wfldElemL.DOF(i) = 1; bfldElemL.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemWL = 0; rsdElemWI = 0; rsdElemWBC = 0;
      integralW(fcnW, xnode, rsdElemWL, rsdElemWI, rsdElemWBC );

      // test the two integrands are the same
      SANS_CHECK_CLOSE ( rsdElemWL.PDE, rsdElemBL[i], small_tol, close_tol );

      Real tmpSum = 0.0;
      for (int d = 0; d < PhysD1::D; d++) tmpSum += rsdElemAUXL[i][d];

      SANS_CHECK_CLOSE( rsdElemWL.Au, tmpSum, small_tol, close_tol );

      // reset to 0
      wfldElemL.DOF(i) = 0; bfldElemL.DOF(i) = 0;
    }

    for (int i = 0; i < wIfldTrace.nDOF(); i++ )
    {
      wIfldTrace.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemWL = 0; rsdElemWI = 0; rsdElemWBC = 0;
      integralW(fcnW, xnode, rsdElemWL, rsdElemWI, rsdElemWBC );

      SANS_CHECK_CLOSE( rsdElemWI, rsdElemBI[i], small_tol, close_tol)
      wIfldTrace.DOF(i) = 0;
    }

    for (int i = 0; i < mufldTrace.nDOF(); i++ )
    {
      mufldTrace.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemWL = 0; rsdElemWI = 0; rsdElemWBC = 0;
      integralW(fcnW, xnode, rsdElemWL, rsdElemWI, rsdElemWBC );

      SANS_CHECK_CLOSE( rsdElemWBC, rsdElemBC[i], small_tol, close_tol)
      mufldTrace.DOF(i) = 0;
    }
  }
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_BCTypeLinearRobin_2D_Triangle_X1Q1LG1_test )
{
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementAFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandBoundaryClass;
  typedef IntegrandBoundaryClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> BasisWeightedClass;
  typedef IntegrandBoundaryClass::BasisWeighted_AUX<Real,Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> BasisWeightedAUXClass;

  // PDE
  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // BC
  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // grid coordinates
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution
  order = 1;
  ElementQFieldCell qfldElemL(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemL.nDOF() );

  // triangle solution
  qfldElemL.DOF(0) = 1;
  qfldElemL.DOF(1) = 3;
  qfldElemL.DOF(2) = 4;

  // auxiliary variable
  ElementAFieldCell afldElemL(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, afldElemL.order() );
  BOOST_CHECK_EQUAL( 3, afldElemL.nDOF() );

  afldElemL.DOF(0) = { 2, -3};
  afldElemL.DOF(1) = { 7,  8};
  afldElemL.DOF(2) = {-1, -5};

  // interface solution  order = 1;
  ElementQFieldTrace qIedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qIedge.order() );
  BOOST_CHECK_EQUAL( 2, qIedge.nDOF() );

  qIedge.DOF(0) =  8;
  qIedge.DOF(1) = -1;

  // Lagrange multiplier
  order = 1;
  ElementQFieldTrace lgedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, lgedge.order() );
  BOOST_CHECK_EQUAL( 2, lgedge.nDOF() );

  lgedge.DOF(0) =  7;
  lgedge.DOF(1) = -9;

  // integrand
  // Local length scale matches that in the Mathematica Notebook
  DiscretizationHDG<PDEClass> disc( pde, Local, Gradient );
  const std::vector<int> BoundaryGroups = {1};
  IntegrandBoundaryClass fcnbc( pde, bc, BoundaryGroups, disc );

  // integrand functor
  BasisWeightedClass fcn = fcnbc.integrand( xedge, CanonicalTraceToCell(0, 1), xfldElemL, qfldElemL, afldElemL, qIedge, lgedge );
  BasisWeightedAUXClass fcnAUX = fcnbc.integrand_AUX( xedge, CanonicalTraceToCell(0, 1), xfldElemL, qfldElemL, afldElemL, qIedge, lgedge );

  BOOST_CHECK_EQUAL( 3, fcn.nDOFCell() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOFTrace() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  BOOST_CHECK_EQUAL( 3, fcnAUX.nDOFCell() );
  BOOST_CHECK_EQUAL( 2, fcnAUX.nDOFTrace() );
  BOOST_CHECK( fcnAUX.needsEvaluation() == true );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  RefCoordTraceType sRef;
  Real integrandPDETrue[3];
  Real integrandAUXxTrue[3], integrandAUXyTrue[3];
  Real integrandBCTrue[2], integrandPDEITrue[2];
  BasisWeightedClass::IntegrandCellType integrand[3];
  BasisWeightedClass::IntegrandTraceType integrandBC[2], integrandPDEI[2];
  BasisWeightedAUXClass::IntegrandType integrandAUX[3];

  // Test at sRef={0}, s={1}
  sRef = {0};
  fcn( sRef, integrand, 3, integrandPDEI, 2, integrandBC, 2 );
  fcnAUX( sRef, integrandAUX, 3 );

  //PDE residual integrands: (advective) + (diffusive) + (bc)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + ( 0 );  // Basis function 1
  integrandPDETrue[1] = ( (39/10.)*(pow(2,-1/2.)) ) + ( (-2054/125.)*(sqrt(2)) ) + ( (7/32500.)*(93632+(70135)*(sqrt(2))) );  // Basis function 2
  integrandPDETrue[2] = ( 0 ) + ( 0 ) + ( 0 );  // Basis function 3

  //Auxiliary integrands:
  integrandAUXxTrue[0] = ( 0 );  // Basis function 1
  integrandAUXyTrue[0] = ( 0 );  // Basis function 1
  integrandAUXxTrue[1] = ( (-3/3250.)*(8216+(8375)*(sqrt(2))) );  // Basis function 2
  integrandAUXyTrue[1] = ( (-3/3250.)*(8216+(8375)*(sqrt(2))) );  // Basis function 2
  integrandAUXxTrue[2] = ( 0 );  // Basis function 3
  integrandAUXyTrue[2] = ( 0 );  // Basis function 3

  //PDE Trace residual integrands: (stabilization) + (bc)
  integrandPDEITrue[0] = ( (559/25.)*(sqrt(2)) );  // Basis function 1
  integrandPDEITrue[1] = ( 0 );  // Basis function 2

  //BC residual integrands:
  integrandBCTrue[0] = ( 11+(6162/125.)*(sqrt(2)) );  // Basis function 1
  integrandBCTrue[1] = ( 0 );  // Basis function 2

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandAUXxTrue[0], integrandAUX[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[0], integrandAUX[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[1], integrandAUX[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[1], integrandAUX[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[2], integrandAUX[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[2], integrandAUX[2][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDEITrue[0], integrandPDEI[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDEITrue[1], integrandPDEI[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBCTrue[0], integrandBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCTrue[1], integrandBC[1], small_tol, close_tol );


  // Test at sRef={1/2}, s={1/2}
  sRef = {1/2.};
  fcn( sRef, integrand, 3, integrandPDEI, 2, integrandBC, 2 );
  fcnAUX( sRef, integrandAUX, 3 );

  //PDE residual integrands: (advective) + (diffusive) + (bc)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + ( 0 );  // Basis function 1
  integrandPDETrue[1] = ( (91/40.)*(pow(2,-1/2.)) ) + ( (-2769/500.)*(pow(2,-1/2.)) )
                        + ( (1/130000.)*(-19018+(-118365)*(sqrt(2))) );  // Basis function 2
  integrandPDETrue[2] = ( (91/40.)*(pow(2,-1/2.)) ) + ( (-2769/500.)*(pow(2,-1/2.)) )
                        + ( (1/130000.)*(-19018+(-118365)*(sqrt(2))) );  // Basis function 3

  //Auxiliary integrands:
  integrandAUXxTrue[0] = ( 0 );  // Basis function 1
  integrandAUXyTrue[0] = ( 0 );  // Basis function 1
  integrandAUXxTrue[1] = ( -639/500.+(141/52.)*(pow(2,-1/2.)) );  // Basis function 2
  integrandAUXyTrue[1] = ( -639/500.+(141/52.)*(pow(2,-1/2.)) );  // Basis function 2
  integrandAUXxTrue[2] = ( -639/500.+(141/52.)*(pow(2,-1/2.)) );  // Basis function 3
  integrandAUXyTrue[2] = ( -639/500.+(141/52.)*(pow(2,-1/2.)) );  // Basis function 3

  //PDE Trace residual integrands: (stabilization) + (bc)
  integrandPDEITrue[0] = ( 0 );  // Basis function 1
  integrandPDEITrue[1] = ( 0 );  // Basis function 2

  //BC residual integrands:
  integrandBCTrue[0] = ( 1+(8307/500.)*(pow(2,-1/2.)) );  // Basis function 1
  integrandBCTrue[1] = ( 1+(8307/500.)*(pow(2,-1/2.)) );  // Basis function 2

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandAUXxTrue[0], integrandAUX[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[0], integrandAUX[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[1], integrandAUX[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[1], integrandAUX[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[2], integrandAUX[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[2], integrandAUX[2][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDEITrue[0], integrandPDEI[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDEITrue[1], integrandPDEI[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBCTrue[0], integrandBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCTrue[1], integrandBC[1], small_tol, close_tol );


  // Test at sRef={1}, s={0}
  sRef = {1};
  fcn( sRef, integrand, 3, integrandPDEI, 2, integrandBC, 2 );
  fcnAUX( sRef, integrandAUX, 3 );

  //PDE residual integrands: (advective) + (diffusive) + (bc)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + ( 0 );  // Basis function 1
  integrandPDETrue[1] = ( 0 ) + ( 0 ) + ( 0 );  // Basis function 2
  integrandPDETrue[2] = ( (13/5.)*(sqrt(2)) ) + ( (1339/125.)*(pow(2,-1/2.)) ) + ( (1/16250.)*(-337221+(-304655)*(sqrt(2))) );  // Basis function 3

  //Auxiliary integrands:
  integrandAUXxTrue[0] = ( 0 );  // Basis function 1
  integrandAUXyTrue[0] = ( 0 );  // Basis function 1
  integrandAUXxTrue[1] = ( 0 );  // Basis function 2
  integrandAUXyTrue[1] = ( 0 );  // Basis function 2
  integrandAUXxTrue[2] = ( 309/125.+(171/13.)*(sqrt(2)) );  // Basis function 3
  integrandAUXyTrue[2] = ( 309/125.+(171/13.)*(sqrt(2)) );  // Basis function 3

  //PDE Trace residual integrands: (stabilization) + (bc)
  integrandPDEITrue[0] = ( 0 );  // Basis function 1
  integrandPDEITrue[1] = ( (-559/25.)*(sqrt(2)) );  // Basis function 2

  //BC residual integrands:
  integrandBCTrue[0] = ( 0 );  // Basis function 1
  integrandBCTrue[1] = ( -7+(-4017/125.)*(pow(2,-1/2.)) );  // Basis function 2

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandAUXxTrue[0], integrandAUX[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[0], integrandAUX[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[1], integrandAUX[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[1], integrandAUX[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[2], integrandAUX[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[2], integrandAUX[2][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDEITrue[0], integrandPDEI[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDEITrue[1], integrandPDEI[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBCTrue[0], integrandBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCTrue[1], integrandBC[1], small_tol, close_tol );


  // Test at sRef={1/5}, s={4/5}
  sRef = {1/5.};
  fcn( sRef, integrand, 3, integrandPDEI, 2, integrandBC, 2 );
  fcnAUX( sRef, integrandAUX, 3 );

  //PDE residual integrands: (advective) + (diffusive) + (bc)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + ( 0 );  // Basis function 1
  integrandPDETrue[1] = ( (208/125.)*(sqrt(2)) ) + ( (-30186/3125.)*(sqrt(2)) )
                        + ( (2/203125.)*(973627+(677235)*(sqrt(2))) );  // Basis function 2
  integrandPDETrue[2] = ( (52/125.)*(sqrt(2)) ) + ( (-15093/3125.)*(pow(2,-1/2.)) )
                        + ( (1/406250.)*(973627+(677235)*(sqrt(2))) );  // Basis function 3

  //Auxiliary integrands:
  integrandAUXxTrue[0] = ( 0 );  // Basis function 1
  integrandAUXyTrue[0] = ( 0 );  // Basis function 1
  integrandAUXxTrue[1] = ( (-12/40625.)*(15093+(9625)*(sqrt(2))) );  // Basis function 2
  integrandAUXyTrue[1] = ( (-12/40625.)*(15093+(9625)*(sqrt(2))) );  // Basis function 2
  integrandAUXxTrue[2] = ( (-3/40625.)*(15093+(9625)*(sqrt(2))) );  // Basis function 3
  integrandAUXyTrue[2] = ( (-3/40625.)*(15093+(9625)*(sqrt(2))) );  // Basis function 3

  //PDE Trace residual integrands: (stabilization) + (bc)
  integrandPDEITrue[0] = ( (6708/625.)*(sqrt(2)) );  // Basis function 1
  integrandPDEITrue[1] = ( (1677/625.)*(sqrt(2)) );  // Basis function 2

  //BC residual integrands:
  integrandBCTrue[0] = ( (2/3125.)*(9250+(45279)*(sqrt(2))) );  // Basis function 1
  integrandBCTrue[1] = ( (1/6250.)*(9250+(45279)*(sqrt(2))) );  // Basis function 2

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandAUXxTrue[0], integrandAUX[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[0], integrandAUX[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[1], integrandAUX[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[1], integrandAUX[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxTrue[2], integrandAUX[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyTrue[2], integrandAUX[2][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDEITrue[0], integrandPDEI[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDEITrue[1], integrandPDEI[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBCTrue[0], integrandBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCTrue[1], integrandBC[1], small_tol, close_tol );


  // test the element integral of the functor

  int quadratureorder = 2;
  int nIntegrandL = qfldElemL.nDOF();
  int nIntegrandI = qIedge.nDOF();
  int nIntegrandBC = lgedge.nDOF();
  typedef BasisWeightedClass::IntegrandCellType IntegrandCellType;
  typedef BasisWeightedAUXClass::IntegrandType IntegrandAUXType;
  GalerkinWeightedIntegral<TopoD1, Line, IntegrandCellType, ArrayQ, ArrayQ> integralBC(quadratureorder, nIntegrandL, nIntegrandI, nIntegrandBC);
  GalerkinWeightedIntegral<TopoD1, Line, IntegrandAUXType> integralAUX(quadratureorder, nIntegrandL);

  IntegrandCellType rsdElem[3];
  ArrayQ rsdPDEI[2], rsdBC[2];
  IntegrandAUXType rsdAUX[3];

  // cell integration for canonical element
  integralBC( fcn, xedge, rsdElem, nIntegrandL, rsdPDEI, nIntegrandI, rsdBC, nIntegrandBC );
  integralAUX( fcnAUX, xedge, rsdAUX, nIntegrandL );

  Real rsdPDETrue[3], rsdPDEITrue[2], rsdAUXxTrue[3], rsdAUXyTrue[3], rsdBCTrue[2];

  //PDE residuals (left): (advective) + (viscous) + (bc)
  rsdPDETrue[0] = ( 0 ) + ( 0 ) + ( 0 ); // Basis function 1
  rsdPDETrue[1] = ( 13/6. ) + ( -6877/750. ) + ( (1/97500.)*((pow(2,-1/2.))*((40+(39)*(sqrt(2)))*(2500+(6877)*(sqrt(2))))) ); // Basis function 2
  rsdPDETrue[2] = ( 143/60. ) + ( -143/75. ) + ( (1/19500.)*((pow(2,-1/2.))*((40+(39)*(sqrt(2)))*(-4025+(286)*(sqrt(2))))) ); // Basis function 3

  rsdAUXxTrue[0] = ( 0 ); // Basis function 1
  rsdAUXyTrue[0] = ( 0 ); // Basis function 1
  rsdAUXxTrue[1] = ( (1/3250.)*(-2500+(-6877)*(sqrt(2))) ); // Basis function 2
  rsdAUXyTrue[1] = ( (1/3250.)*(-2500+(-6877)*(sqrt(2))) ); // Basis function 2
  rsdAUXxTrue[2] = ( (1/650.)*(4025+(-286)*(sqrt(2))) ); // Basis function 3
  rsdAUXyTrue[2] = ( (1/650.)*(4025+(-286)*(sqrt(2))) ); // Basis function 3

  rsdPDEITrue[0] = ( 559/75. ); // Basis function 1
  rsdPDEITrue[1] = ( -559/75. ); // Basis function 2

  rsdBCTrue[0] = ( (1/250.)*((pow(2,-1/2.))*(1250+(6877)*(sqrt(2)))) ); // Basis function 1
  rsdBCTrue[1] = ( (1/25.)*((pow(2,-1/2.))*(-25+(143)*(sqrt(2)))) ); // Basis function 2

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdElem[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdElem[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[2], rsdElem[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdAUXxTrue[0], rsdAUX[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXyTrue[0], rsdAUX[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXxTrue[1], rsdAUX[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXyTrue[1], rsdAUX[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXxTrue[2], rsdAUX[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXyTrue[2], rsdAUX[2][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdPDEITrue[0], rsdPDEI[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDEITrue[1], rsdPDEI[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdBCTrue[0], rsdBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue[1], rsdBC[1], small_tol, close_tol );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_BCTypeLinearRobin_2D_Triangle_X1Q1LG1_test )
{
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementAFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandBoundaryClass;
  typedef IntegrandBoundaryClass::FieldWeighted<Real,Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> FieldWeightedClass;

  // PDE
  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // BC
  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // grid coordinates
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  // auxiliary variable
  ElementAFieldCell afldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, afldElem.order() );
  BOOST_CHECK_EQUAL( 3, afldElem.nDOF() );

  afldElem.DOF(0) = { 2, -3};
  afldElem.DOF(1) = { 7,  8};
  afldElem.DOF(2) = {-1, -5};

  // interface solution
  ElementQFieldTrace qIedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qIedge.order() );
  BOOST_CHECK_EQUAL( 2, qIedge.nDOF() );

  qIedge.DOF(0) =  8;
  qIedge.DOF(1) = -1;

  // Lagrange multiplier
  ElementQFieldTrace lgedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, lgedge.order() );
  BOOST_CHECK_EQUAL( 2, lgedge.nDOF() );

  lgedge.DOF(0) =  7;
  lgedge.DOF(1) = -9;

  // weight
  ElementQFieldCell wfldElem(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElem.order() );
  BOOST_CHECK_EQUAL( 6, wfldElem.nDOF() );

  // triangle solution
  wfldElem.DOF(0) = -2;
  wfldElem.DOF(1) =  4;
  wfldElem.DOF(2) =  3;
  wfldElem.DOF(3) =  2;
  wfldElem.DOF(4) =  4;
  wfldElem.DOF(5) = -1;

  // auxiliary variable
  ElementAFieldCell bfldElem(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, bfldElem.order() );
  BOOST_CHECK_EQUAL( 6, bfldElem.nDOF() );

  bfldElem.DOF(0) = { 1,  5};
  bfldElem.DOF(1) = { 3,  6};
  bfldElem.DOF(2) = {-1,  7};
  bfldElem.DOF(3) = { 3,  1};
  bfldElem.DOF(4) = { 2,  2};
  bfldElem.DOF(5) = { 4,  3};

  // Lagrange multiplier
  ElementQFieldTrace wIedge(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wIedge.order() );
  BOOST_CHECK_EQUAL( 3, wIedge.nDOF() );

  wIedge.DOF(0) =  2;
  wIedge.DOF(1) = -1;
  wIedge.DOF(2) =  3;

  // Lagrange multiplier
  ElementQFieldTrace muedge(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, muedge.order() );
  BOOST_CHECK_EQUAL( 3, muedge.nDOF() );

  muedge.DOF(0) = -3;
  muedge.DOF(1) =  2;
  muedge.DOF(2) = -1;

  // integrand

  DiscretizationHDG<PDEClass> disc( pde, Local, Gradient );
  const std::vector<int> BoundaryGroups = {1};
  IntegrandBoundaryClass fcnbc( pde, bc, BoundaryGroups, disc );

  // integrand functor
  FieldWeightedClass fcn = fcnbc.integrand( xedge, CanonicalTraceToCell(0, 1),
                                            xfldElem,
                                            qfldElem, afldElem,
                                            wfldElem, bfldElem,
                                            qIedge, lgedge,
                                            wIedge, muedge );

  BOOST_CHECK_EQUAL( 3, fcn.nDOFCell() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOFTrace() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 2e-13;
  RefCoordTraceType sRef;
  Real integrandPDETrue, integrandAUXTrue;
  Real integrandBCTrue, integrandPDEITrue;
  FieldWeightedClass::IntegrandCellType integrand;
  FieldWeightedClass::IntegrandTraceType integrandBC, integrandPDEI;

  // Test at sRef={0}, s={1}
  sRef = {0};
  fcn( sRef, integrand, integrandPDEI, integrandBC );

  //PDE residual integrands: (advective) + (diffusive) + (bc)
  integrandPDETrue = ( (39/5.)*(sqrt(2)) ) + ( (-8216/125.)*(sqrt(2)) ) + ( (7/8125.)*(93632+(70135)*(sqrt(2))) );  // Weight function

  //Auxiliary integrands:
  integrandAUXTrue = ( (-27/3250.)*(8216+(8375)*(sqrt(2))) );  // Weight function

  //PDE Trace residual integrands: (stabilization) + (bc)
  integrandPDEITrue = ( (1118/25.)*(sqrt(2)) );  // Weight function

  //BC residual integrands:
  integrandBCTrue = ( (-3)*(11+(6162/125.)*(sqrt(2))) );  // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXTrue, integrand.Au, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDEITrue, integrandPDEI, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCTrue, integrandBC, small_tol, close_tol );


  // Test at sRef={1}, s={0}
  sRef = {1};
  fcn( sRef, integrand, integrandPDEI, integrandBC );

  //PDE residual integrands: (advective) + (diffusive) + (bc)
  integrandPDETrue = ( (39/5.)*(sqrt(2)) ) + ( (4017/125.)*(pow(2,-1/2.)) ) + ( (-3/16250.)*(337221+(304655)*(sqrt(2))) );  // Weight function

  //Auxiliary integrands:
  integrandAUXTrue = ( 1854/125.+(1026/13.)*(sqrt(2)) );  // Weight function

  //PDE Trace residual integrands: (stabilization) + (bc)
  integrandPDEITrue = ( (559/25.)*(sqrt(2)) );  // Weight function

  //BC residual integrands:
  integrandBCTrue = ( -14+(-4017/125.)*(sqrt(2)) );  // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXTrue, integrand.Au, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDEITrue, integrandPDEI, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCTrue, integrandBC, small_tol, close_tol );


  // Test at sRef={1/2}, s={1/2}
  sRef = {1/2.};
  fcn( sRef, integrand, integrandPDEI, integrandBC );

  //PDE residual integrands: (advective) + (diffusive) + (bc)
  integrandPDETrue = ( (1001/40.)*(pow(2,-1/2.)) ) + ( (-30459/500.)*(pow(2,-1/2.)) )
                     + ( (-11/130000.)*(19018+(118365)*(sqrt(2))) );  // Weight function

  //Auxiliary integrands:
  integrandAUXTrue = ( (69/13000.)*(-5538+(5875)*(sqrt(2))) );  // Weight function

  //PDE Trace residual integrands: (stabilization) + (bc)
  integrandPDEITrue = ( 0 );  // Weight function

  //BC residual integrands:
  integrandBCTrue = ( -3+(-24921/500.)*(pow(2,-1/2.)) );  // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXTrue, integrand.Au, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDEITrue, integrandPDEI, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCTrue, integrandBC, small_tol, close_tol );


  // Test at sRef={1/5}, s={4/5}
  sRef = {1/5.};
  fcn( sRef, integrand, integrandPDEI, integrandBC );

  //PDE residual integrands: (advective) + (diffusive) + (bc)
  integrandPDETrue = ( (6604/625.)*(sqrt(2)) ) + ( (-1916811/15625.)*(pow(2,-1/2.)) )
                     + ( (127/2031250.)*(973627+(677235)*(sqrt(2))) );  // Weight function

  //Auxiliary integrands:
  integrandAUXTrue = ( (-822/203125.)*(15093+(9625)*(sqrt(2))) );  // Weight function

  //PDE Trace residual integrands: (stabilization) + (bc)
  integrandPDEITrue = ( (139191/3125.)*(sqrt(2)) );  // Weight function

  //BC residual integrands:
  integrandBCTrue = ( (-33/15625.)*(9250+(45279)*(sqrt(2))) );  // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXTrue, integrand.Au, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDEITrue, integrandPDEI, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCTrue, integrandBC, small_tol, close_tol );


  // test the element integral of the functor

  int quadratureorder = 5;
  typedef FieldWeightedClass::IntegrandCellType IntegrandCellType;
  ElementIntegral<TopoD1, Line, IntegrandCellType, Real, Real> integralBC(quadratureorder);

  IntegrandCellType rsdElem = 0;
  Real rsdBC = 0, rsdPDEI = 0;

  // cell integration for canonical element
  integralBC( fcn, xedge, rsdElem, rsdPDEI, rsdBC );

  Real rsdPDETrue, rsdAUXTrue;
  Real rsdPDEITrue, rsdBCTrue;

  //PDE residuals (left): (advective) + (viscous) + (bc)
  rsdPDETrue = ( 1313/60. ) + ( -21437/375. )
               + ( (1/97500.)*((pow(2,-1/2.))*((40+(39)*(sqrt(2)))*(-73875+(42874)*(sqrt(2))))) ); // Weight function
  rsdAUXTrue = ( (1/13.)*((pow(2,-1/2.))*(-741+(581)*(sqrt(2)))) ); // Weight function
  rsdPDEITrue = ( 559/25. ); // Weight function
  rsdBCTrue = ( (-1/750.)*((pow(2,-1/2.))*(14750+(69927)*(sqrt(2)))) ); // Weight function

  SANS_CHECK_CLOSE( rsdPDETrue, rsdElem.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXTrue, rsdElem.Au, small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDEITrue, rsdPDEI, small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue, rsdBC, small_tol, close_tol );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_2D_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldTrace;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementAFieldCell;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementParam> BasisWeightedClass;
  typedef IntegrandClass::BasisWeighted_AUX<Real,Real,TopoD1,Line,TopoD2,Triangle,ElementParam> BasisWeightedAUXClass;
  typedef IntegrandClass::FieldWeighted<Real,Real,TopoD1,Line,TopoD2,Triangle,ElementParam> FieldWeightedClass;
  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  // PDE
  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real a = 2.35;
  Source2D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // BC
  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  // grid
  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // grid coordinates
  Real x1, x2, x3, y1, y2, y3;

  x1 = -0.1;  y1 = -0.05;
  x2 =    1;  y2 =     0;
  x3 =  0.2;  y3 =     1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  for (int qorder = 2; qorder< 4; qorder++)
  {
    // solution
    ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementAFieldCell afldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementAFieldCell bfldElemL(qorder, BasisFunctionCategory_Hierarchical);

    ElementQFieldTrace qIfldTrace(qorder, BasisFunctionCategory_Legendre);
    ElementQFieldTrace wIfldTrace(qorder, BasisFunctionCategory_Legendre);

    ElementQFieldTrace lgfldTrace(qorder, BasisFunctionCategory_Legendre);
    ElementQFieldTrace mufldTrace(qorder, BasisFunctionCategory_Legendre);

    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), afldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), wfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), bfldElemL.nDOF() );

    // line solution
    for ( int dof = 0; dof < qfldElemL.nDOF(); dof ++ )
    {
      qfldElemL.DOF(dof) = (dof+1)*pow(-1,dof);
      wfldElemL.DOF(dof) = 0;

      afldElemL.DOF(dof) = (dof+2)*pow(-1,dof+1);
      bfldElemL.DOF(dof) = 0;
    }

    BOOST_CHECK_EQUAL( qIfldTrace.nDOF(), wIfldTrace.nDOF() );
    for (int dof = 0; dof < qIfldTrace.nDOF(); dof++)
    {
      qIfldTrace.DOF(dof) = (dof+1)*pow(-1,dof);
      wIfldTrace.DOF(dof) = 0;
    }

    BOOST_CHECK_EQUAL( lgfldTrace.nDOF(), mufldTrace.nDOF() );
    for (int dof = 0; dof < lgfldTrace.nDOF(); dof++)
    {
      lgfldTrace.DOF(dof) = (dof+3)*pow(-1,dof);
      mufldTrace.DOF(dof) = 0;
    }

    // HDG discretization
    DiscretizationClass disc( pde, Global, Gradient );

    // integrand
    const std::vector<int> BoundaryGroups = {1};

    IntegrandClass fcnbc( pde, bc, BoundaryGroups, disc );

    // integrand functor
    BasisWeightedClass fcnB = fcnbc.integrand( xedge,
                                               CanonicalTraceToCell(0,1),
                                               xfldElemL,
                                               qfldElemL, afldElemL,
                                               qIfldTrace, lgfldTrace );

    BasisWeightedAUXClass fcnAUX = fcnbc.integrand_AUX( xedge,
                                                        CanonicalTraceToCell(0,1),
                                                        xfldElemL,
                                                        qfldElemL, afldElemL,
                                                        qIfldTrace, lgfldTrace );
    // integrand functor
    FieldWeightedClass fcnW = fcnbc.integrand( xedge,
                                               CanonicalTraceToCell(0,1),
                                               xfldElemL,
                                               qfldElemL, afldElemL,
                                               wfldElemL, bfldElemL,
                                               qIfldTrace, lgfldTrace,
                                               wIfldTrace, mufldTrace );

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;

    int nIntegrandL = qfldElemL.nDOF();
    int nIntegrandI = qIfldTrace.nDOF();
    int nIntegrandBC = lgfldTrace.nDOF();

    int quadratureorder = 2*qorder;
    GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedClass::IntegrandCellType,
                                           BasisWeightedClass::IntegrandTraceType,
                                           BasisWeightedClass::IntegrandTraceType>
                                           integralB(quadratureorder, nIntegrandL, nIntegrandI, nIntegrandBC);

    GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedAUXClass::IntegrandType>
                                           integralAUX(quadratureorder, nIntegrandL);

    ElementIntegral<TopoD1, Line, FieldWeightedClass::IntegrandCellType,
                                  FieldWeightedClass::IntegrandTraceType,
                                  FieldWeightedClass::IntegrandTraceType> integralW(quadratureorder);
    std::vector<BasisWeightedClass::IntegrandCellType> rsdElemBL(nIntegrandL, 0);
    std::vector<BasisWeightedClass::IntegrandTraceType> rsdElemBI(nIntegrandI, 0);
    std::vector<BasisWeightedClass::IntegrandTraceType> rsdElemBC(nIntegrandBC, 0);
    std::vector<BasisWeightedAUXClass::IntegrandType> rsdElemAUX(nIntegrandL, 0);
    FieldWeightedClass::IntegrandCellType rsdElemWL=0;
    FieldWeightedClass::IntegrandTraceType rsdElemWI=0;
    FieldWeightedClass::IntegrandTraceType rsdElemWBC=0;

    // cell integration for canonical element
    integralB( fcnB, xedge,
               rsdElemBL.data(), rsdElemBL.size(),
               rsdElemBI.data(), rsdElemBI.size(),
               rsdElemBC.data(), rsdElemBC.size() );

    integralAUX( fcnAUX, xedge, rsdElemAUX.data(), nIntegrandL );

    for (int i = 0; i < wfldElemL.nDOF(); i++ )
    {
      // set just one of the weights to one
      wfldElemL.DOF(i) = 1; bfldElemL.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemWL = 0; rsdElemWI = 0; rsdElemWBC = 0;
      integralW(fcnW, xedge, rsdElemWL, rsdElemWI, rsdElemWBC );

      // test the two integrands are the same
      SANS_CHECK_CLOSE ( rsdElemWL.PDE, rsdElemBL[i], small_tol, close_tol );

      Real tmpSum = 0.0;
      for (int d = 0; d < PhysD2::D; d++) tmpSum += rsdElemAUX[i][d];

      SANS_CHECK_CLOSE ( rsdElemWL.Au, tmpSum, small_tol, close_tol );

      // reset to 0
      wfldElemL.DOF(i) = 0; bfldElemL.DOF(i) = 0;
    }

    for (int i = 0; i < wIfldTrace.nDOF(); i++ )
    {
      wIfldTrace.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemWL = 0; rsdElemWI = 0; rsdElemWBC = 0;
      integralW(fcnW, xedge, rsdElemWL, rsdElemWI, rsdElemWBC );

      SANS_CHECK_CLOSE( rsdElemWI, rsdElemBI[i], small_tol, close_tol)
      wIfldTrace.DOF(i) = 0;
    }

    for (int i = 0; i < mufldTrace.nDOF(); i++ )
    {
      mufldTrace.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemWL = 0; rsdElemWI = 0; rsdElemWBC = 0;
      integralW(fcnW, xedge, rsdElemWL, rsdElemWI, rsdElemWBC );

      SANS_CHECK_CLOSE( rsdElemWBC, rsdElemBC[i], small_tol, close_tol)
      mufldTrace.DOF(i) = 0;
    }
  }
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
