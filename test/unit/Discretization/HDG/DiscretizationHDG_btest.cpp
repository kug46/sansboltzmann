// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// DiscretizationHDG_Triangle_btest
// testing of DiscretizationHDG class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "Discretization/HDG/DiscretizationHDG.h"

using namespace std;
using namespace SANS;

typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_None > PDEAdvectionDiffusion2D;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct

namespace SANS
{
template class DiscretizationHDG<PDEAdvectionDiffusion2D>;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( DiscretizationHDG_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef DiscretizationHDG<PDEClass> DiscretizationHDGClass;

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  Real nu = 1;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  DiscretizationHDGClass disc0( pde );
  BOOST_CHECK_EQUAL( Nothing, disc0.tauType() );

  DiscretizationHDGClass disc1( pde, Nothing );
  BOOST_CHECK_EQUAL( Nothing, disc1.tauType() );

  DiscretizationHDGClass disc2( pde, Global );
  BOOST_CHECK_EQUAL( Global, disc2.tauType() );

  DiscretizationHDGClass disc3( pde, Local );
  BOOST_CHECK_EQUAL( Local, disc3.tauType() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( evalStabilization_test )
{
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef DiscretizationHDG<PDEClass> DiscretizationHDGClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;
  typedef PDEClass::VectorX VectorX;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  Real nu = 1;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // HDG discretization options

  DiscretizationHDGClass disc0( pde );
  DiscretizationHDGClass disc1( pde, Global );
  DiscretizationHDGClass disc2( pde, Local );

  Real x, y, nx, ny;
  Real lengthscale;
  Real tauTrue;
  ArrayQ q;
  MatrixQ tau;

  x = 2;
  y = 5;
  nx = -0.8;
  ny =  0.6;
  lengthscale = 9.;
  q = 0;

  VectorX X = {x, y};

//  tauTrue = fabs(nx*u + ny*v); //advective stabilization taken care of through Roe scheme
  tauTrue = 0;
  disc0.evalStabilization( X, {nx, ny}, lengthscale, q, tau );
  SANS_CHECK_CLOSE( tauTrue, tau, small_tol, close_tol );

//  tauTrue = fabs(nx*u + ny*v) + nu;
  tauTrue = nu;
  disc1.evalStabilization( X, {nx, ny}, lengthscale, q, tau );
  SANS_CHECK_CLOSE( tauTrue, tau, small_tol, close_tol );

//  tauTrue = fabs(nx*u + ny*v) + nu/lengthscale;
  tauTrue = nu/lengthscale;
  disc2.evalStabilization( X, {nx, ny}, lengthscale, q, tau );
  SANS_CHECK_CLOSE( tauTrue, tau, small_tol, close_tol );
}


#if 0   // dump() not yet implemented
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Discretization/DiscretizationHDG_dump_pattern.txt", true );

  const bool verbose = true;

  DiscretizationHDG disc0(0);
  disc0.dump( 2, output );

  DiscretizationHDG disc1(0, 3, verbose);
  disc1.dump( 2, output );

  DiscretizationHDG disc2(-1, 3, verbose);
  disc2.dump( 2, output );

  DiscretizationHDG disc3(-2, 3, verbose);
  disc3.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
