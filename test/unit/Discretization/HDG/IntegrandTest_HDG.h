// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDTEST_HDG_H
#define INTEGRANDTEST_HDG_H

#include "tools/SANSnumerics.h"     // Real
#include "tools/call_with_derived.h"

#include "Discretization/HDG/JacobianCell_HDG_Element.h"
#include "Discretization/HDG/JacobianInteriorTrace_HDG_Element.h"

namespace SANS
{
namespace testspace
{

struct HDG {};

template <class PDE_>
class IntegrandCell_HDG_None : public IntegrandCellType< IntegrandCell_HDG_None<PDE_> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual arrays

  static const int D = PhysDim::D;     // Physical dimensions
  static const int N = 1;              // total PDE equations

  IntegrandCell_HDG_None( const std::vector<int>& CellGroups ) : cellGroups_(CellGroups) {}

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  template<class Tq, class Ta, class TopoDim, class Topology, class ElementParam>
  class BasisWeighted_PDE
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;

    typedef Element<ArrayQ<Tq>, TopoDim, Topology> ElementQFieldType;
    typedef Element<VectorArrayQ<Ta>, TopoDim, Topology> ElementAFieldType;

    typedef typename ElementXFieldType::RefCoordType RefCoordType;
    typedef typename ElementXFieldType::VectorX VectorX;

    typedef JacobianElemCell_HDG<PhysDim,MatrixQ<Real>> JacobianElemCellType;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    typedef typename promote_Surreal<Tq, Ta>::type Ts;
    typedef ArrayQ<Ts> IntegrandType;

    BasisWeighted_PDE( const ElementParam& paramfldElem,
                       const ElementQFieldType& qfldElem,
                       const ElementAFieldType& afldElem ) :
                         xfldElem_(get<-1>(paramfldElem)),  // XField must be the last parameter
                         qfldElem_(qfldElem), afldElem_(afldElem),
                         paramfldElem_(paramfldElem),
                         nDOF_( qfldElem_.nDOF() )
    {
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // Cell integrand
    template <class Ti>
    void operator()( const QuadPointType& sRef, ArrayQ<Ti> integrand[], int neqn ) const
    {
      for (int k = 0; k < neqn; k++)
        integrand[k] = 0.0;
    }

    // cell element Jacobian integrand
    void operator()( const Real dJ, const QuadPointType& sRef, JacobianElemCellType& mtxElem ) const
    {
      mtxElem = 0;
    }

  protected:
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementAFieldType& afldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
  };

  template<class Tq, class Ta, class TopoDim, class Topology>
  class BasisWeighted_AUX
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<ArrayQ<Tq>, TopoDim, Topology> ElementQFieldType;
    typedef Element<VectorArrayQ<Ta>, TopoDim, Topology> ElementAFieldType;

    typedef typename ElementXFieldType::RefCoordType RefCoordType;
    typedef typename ElementXFieldType::VectorX VectorX;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    typedef typename promote_Surreal<Tq, Ta>::type Ts;
    typedef VectorArrayQ<Ts> IntegrandType;

    BasisWeighted_AUX( const ElementXFieldType& xfldElem,
                       const ElementQFieldType& qfldElem,
                       const ElementAFieldType& afldElem ) :
                         xfldElem_(xfldElem),
                         qfldElem_(qfldElem), afldElem_(afldElem),
                         nDOF_( qfldElem_.nDOF() ),
                         phi_(qfldElem_.nDOF()),
                         gradphi_(qfldElem_.nDOF())
    {
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // Cell integrand
    void operator()( const QuadPointType& sRef, IntegrandType integrand[], int neqn ) const
    {
      VectorArrayQ<Ta> a; // auxiliary variable (gradient)

      // basis value, gradient
      qfldElem_.evalBasis( sRef, phi_.data(), phi_.size() );

      afldElem_.evalFromBasis( phi_.data(), phi_.size(), a );

      for (int k = 0; k < neqn; k++)
        integrand[k] = phi_[k]*a;
    }

  protected:
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementAFieldType& afldElem_;

    const int nDOF_;
    mutable std::vector<Real> phi_;
    mutable std::vector< DLA::VectorS<PhysDim::D, Real> > gradphi_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef Element<VectorArrayQ<T>, TopoDim, Topology> ElementAFieldType;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    typedef IntegrandHDG< Real, Real > IntegrandType;

    FieldWeighted(  const ElementParam& paramfldElem,
                    const ElementQFieldType& qfldElem,
                    const ElementAFieldType& afldElem,
                    const ElementQFieldType& wfldElem,
                    const ElementAFieldType& bfldElem) :
                    xfldElem_(get<-1>(paramfldElem)),  // XField must be the last parameter
                    qfldElem_(qfldElem), afldElem_(afldElem),
                    wfldElem_(wfldElem), bfldElem_(bfldElem),
                    paramfldElem_(paramfldElem),
                    nDOF_( qfldElem_.nDOF() ),
                    nwDOF_( wfldElem_.nDOF() )
    {
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
      SANS_ASSERT( wfldElem_.basis() == bfldElem_.basis() );
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // Cell integrand
    void operator()( const QuadPointType& sRef, IntegrandType& integrand ) const
    {
      integrand = 0.0;
    }

  protected:
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementAFieldType& afldElem_;
    const ElementQFieldType& wfldElem_;
    const ElementAFieldType& bfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    const int nwDOF_;
  };

  template<class Tq, class Ta, class TopoDim, class Topology, class ElementParam>
  BasisWeighted_PDE<Tq, Ta, TopoDim, Topology, ElementParam>
  integrand_PDE(const ElementParam& paramfldElem,
                const Element<ArrayQ<Tq>, TopoDim, Topology>& qfldElem,
                const Element<VectorArrayQ<Ta>, TopoDim, Topology>& afldElem) const
  {
    return BasisWeighted_PDE<Tq,Ta,TopoDim,Topology,ElementParam>( paramfldElem, qfldElem, afldElem );
  }

  template<class Tq, class Ta, class TopoDim, class Topology>
  BasisWeighted_AUX<Tq, Ta, TopoDim, Topology>
  integrand_AUX(const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
                const Element<ArrayQ<Tq>, TopoDim, Topology>& qfldElem,
                const Element<VectorArrayQ<Ta>, TopoDim, Topology>& afldElem) const
  {
    return BasisWeighted_AUX<Tq,Ta,TopoDim,Topology>( xfldElem, qfldElem, afldElem );
  }

  template<class T, class TopoDim, class Topology, class ElementParam>
  FieldWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
            const Element<VectorArrayQ<T>, TopoDim, Topology>& afldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& wfldElem,
            const Element<VectorArrayQ<T>, TopoDim, Topology>& bfldElem) const
  {
    return FieldWeighted<T,TopoDim,Topology,ElementParam>( paramfldElem,
                                                           qfldElem, afldElem,
                                                           wfldElem, bfldElem );
  }

protected:
  const std::vector<int> cellGroups_;
};


template<class PDE_>
class IntegrandInteriorTrace_HDG_None :
  public IntegrandInteriorTraceType< IntegrandInteriorTrace_HDG_None<PDE_> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  template <class T>
  using MatrixQ = T;    // solution/residual jacobians

  static const int D = PhysDim::D;          // Physical dimensions
  static const int N = 1;

  IntegrandInteriorTrace_HDG_None( const std::vector<int>& InteriorTraceGroups ) :
    interiorTraceGroups_(InteriorTraceGroups) {}

  std::size_t nInteriorTraceGroups() const { return interiorTraceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return interiorTraceGroups_[n]; }
  const std::vector<int>& interiorTraceGroups() const { return interiorTraceGroups_; }

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class ElementParamL >
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;

    typedef Element<      ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementTraceField;
    typedef Element<      ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<VectorArrayQ<T>, TopoDimCell, TopologyL> ElementAFieldL;

    typedef JacobianElemInteriorTrace_HDG<PhysDim, MatrixQ<Real>> JacobianElemInteriorTraceType;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef ArrayQ<T> IntegrandCellType;
    typedef ArrayQ<T> IntegrandTraceType;

    BasisWeighted( const ElementXFieldTrace& xfldElemTrace,
                   const ElementTraceField& qfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL,
                   const int normalSign,
                   const ElementParamL& paramfldElemL, // XField must be the last parameter
                   const ElementQFieldL& qfldElemL,
                   const ElementAFieldL& afldElemL ) :
                     xfldElemTrace_(xfldElemTrace), qfldElemTrace_(qfldElemTrace),
                     canonicalTraceL_(canonicalTraceL), normalSign_(normalSign),
                     xfldElemL_(get<-1>(paramfldElemL)),
                     qfldElemL_(qfldElemL), afldElemL_(afldElemL),
                     paramfldElemL_(paramfldElemL),
                     nDOFL_( qfldElemL_.nDOF() ),
                     nDOFI_( qfldElemTrace_.nDOF() )
    {
      BOOST_REQUIRE( qfldElemL_.basis() == afldElemL_.basis() );

      SANS_ASSERT( normalSign_ == 1 || normalSign_ == -1 );
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return N; }

    // total DOFs
    int nDOFLeft() const { return qfldElemL_.nDOF(); }
    int nDOFTrace() const { return qfldElemTrace_.nDOF(); }

    // element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandCellL[], const int neqnL,
                                                          ArrayQ<Ti> integrandIntI[] , const int neqnI ) const
    {
      for (int k = 0; k < neqnL; k++)
        integrandCellL[k] = 0.0;

      for (int k = 0; k < neqnI; k++)
        integrandIntI[k] = 0.0;
    }

    // trace element Jacobian integrand
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     JacobianElemInteriorTraceType& mtxElemL,
                     JacobianElemInteriorTraceType& mtxElemI ) const
    {
      mtxElemL = 0;
      mtxElemI = 0;
    }

  protected:
    const ElementXFieldTrace& xfldElemTrace_;
    const ElementTraceField& qfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const int normalSign_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementAFieldL& afldElemL_;
    const ElementParamL& paramfldElemL_;

    const int nDOFL_;
    const int nDOFI_;
  };

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class ElementParamL >
  class BasisWeighted_AUX
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementTraceField;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;

    typedef typename ElementXFieldTrace::RefCoordType RefCoordTraceType;
    typedef typename ElementXFieldL::RefCoordType RefCoordCellType;
    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef VectorArrayQ<T> IntegrandType;

    BasisWeighted_AUX( const ElementXFieldTrace& xfldElemTrace,
                       const ElementTraceField& qfldElemTrace,
                       const CanonicalTraceToCell& canonicalTraceL,
                       const int normalSign,
                       const ElementParamL& paramfldElemL, // XField must be the last parameter
                       const ElementQFieldL& qfldElemL ) :
                         xfldElemTrace_(xfldElemTrace), qfldElemTrace_(qfldElemTrace),
                         canonicalTraceL_(canonicalTraceL), normalSign_(normalSign),
                         xfldElemL_(get<-1>(paramfldElemL)),
                         qfldElemL_(qfldElemL),
                         paramfldElemL_(paramfldElemL),
                         nDOFL_( qfldElemL_.nDOF() ),
                         nDOFI_( qfldElemTrace_.nDOF() )
    {
      SANS_ASSERT( normalSign_ == 1 || normalSign_ == -1 );
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return qfldElemL_.nDOF(); }
    int nDOFTrace() const { return qfldElemTrace_.nDOF(); }

    // element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, VectorArrayQ<Ti> integrandL[], const int neqnL ) const
    {
      for (int k = 0; k < neqnL; k++)
        integrandL[k] = 0.0;
    }

  protected:
    const ElementXFieldTrace& xfldElemTrace_;
    const ElementTraceField& qfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const int normalSign_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementParamL& paramfldElemL_;

    const int nDOFL_;
    const int nDOFI_;
  };

  template< class T, class TopoDimTrace, class TopologyTrace,
                       class TopoDimCell,  class TopologyL,    class ElementParamL >
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;

    typedef Element<      ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementTraceField;
    typedef Element<      ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<VectorArrayQ<T>, TopoDimCell, TopologyL> ElementAFieldL;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef IntegrandHDG< Real, Real > IntegrandCellType;
    typedef Real IntegrandTraceType;

    FieldWeighted( const ElementXFieldTrace& xfldElemTrace,
                   const ElementTraceField& qIfldElemTrace,
                   const ElementTraceField& wIfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL,
                   const int normalSign,
                   const ElementParamL& paramfldElemL, // XField must be the last parameter
                   const ElementQFieldL& qfldElemL,
                   const ElementAFieldL& afldElemL,
                   const ElementQFieldL& wfldElemL,
                   const ElementAFieldL& bfldElemL) :
                   xfldElemTrace_(xfldElemTrace),
                   qIfldElemTrace_(qIfldElemTrace), wIfldElemTrace_(wIfldElemTrace),
                   canonicalTraceL_(canonicalTraceL), normalSign_(normalSign),
                   xfldElemL_(get<-1>(paramfldElemL)),
                   qfldElemL_(qfldElemL), afldElemL_(afldElemL),
                   wfldElemL_(wfldElemL), bfldElemL_(bfldElemL),
                   paramfldElemL_(paramfldElemL),
                   nDOFL_( qfldElemL_.nDOF() ),
                   nDOFI_( qIfldElemTrace_.nDOF() ),
                   nwDOFL_( wfldElemL_.nDOF() ),
                   nwDOFI_( wIfldElemTrace_.nDOF() )
    {
      SANS_ASSERT( qfldElemL_.basis() == afldElemL_.basis() );
      SANS_ASSERT( normalSign_ == 1 || normalSign_ == -1 );
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return qfldElemL_.nDOF(); }

    // element trace integrand
    void operator()( const QuadPointTraceType& sRefTrace, IntegrandCellType& integrandCellL,
                                                          IntegrandTraceType& integrandIntI ) const
    {
      integrandCellL = 0.0;
      integrandIntI = 0.0;
    }

  protected:
    const ElementXFieldTrace& xfldElemTrace_;
    const ElementTraceField& qIfldElemTrace_;
    const ElementTraceField& wIfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const int normalSign_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementAFieldL& afldElemL_;
    const ElementQFieldL& wfldElemL_;
    const ElementAFieldL& bfldElemL_;
    const ElementParamL& paramfldElemL_;

    const int nDOFL_;
    const int nDOFI_;
    const int nwDOFL_;
    const int nwDOFI_;
  };

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class ElementParamL >
  BasisWeighted<T, TopoDimTrace, TopologyTrace,
                   TopoDimCell,  TopologyL,     ElementParamL >
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
                const Element<ArrayQ<T>, TopoDimTrace, TopologyTrace>& qIfldElemTrace,
                const CanonicalTraceToCell& canonicalTraceL, const int normalSign,
                const ElementParamL& paramfldElemL,     // XField must be the last parameter
                const Element<ArrayQ<T>      , TopoDimCell, TopologyL>& qfldElemL,
                const Element<VectorArrayQ<T>, TopoDimCell, TopologyL>& afldElemL) const
  {
    return BasisWeighted<T, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  TopologyL,     ElementParamL>(xfldElemTrace, qIfldElemTrace,
                                                                        canonicalTraceL, normalSign,
                                                                        paramfldElemL, qfldElemL, afldElemL);
  }

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class ElementParamL >
  BasisWeighted_AUX<T, TopoDimTrace, TopologyTrace,
                       TopoDimCell,  TopologyL,     ElementParamL >
  integrand_AUX(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
                const Element<ArrayQ<T>, TopoDimTrace, TopologyTrace>& qIfldElemTrace,
                const CanonicalTraceToCell& canonicalTraceL, const int normalSign,
                const ElementParamL& paramfldElemL,     // XField must be the last parameter
                const Element<ArrayQ<T>, TopoDimCell, TopologyL>& qfldElemL) const
  {
    return BasisWeighted_AUX<T, TopoDimTrace, TopologyTrace,
                                TopoDimCell,  TopologyL,     ElementParamL>(xfldElemTrace, qIfldElemTrace,
                                                                            canonicalTraceL, normalSign,
                                                                            paramfldElemL, qfldElemL);
  }

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class ElementParamL >
  FieldWeighted<T, TopoDimTrace, TopologyTrace,
                   TopoDimCell,  TopologyL,     ElementParamL >
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const Element<ArrayQ<T>, TopoDimTrace, TopologyTrace>& qIfldElemTrace,
            const Element<ArrayQ<T>, TopoDimTrace, TopologyTrace>& wIfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const int normalSign,
            const ElementParamL& paramfldElemL,     // XField must be the last parameter
            const Element<ArrayQ<T>      , TopoDimCell, TopologyL>& qfldElemL,
            const Element<VectorArrayQ<T>, TopoDimCell, TopologyL>& afldElemL,
            const Element<ArrayQ<T>      , TopoDimCell, TopologyL>& wfldElemL,
            const Element<VectorArrayQ<T>, TopoDimCell, TopologyL>& bfldElemL) const
  {
    return FieldWeighted<T, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  TopologyL,     ElementParamL>(xfldElemTrace,
                                                                        qIfldElemTrace, wIfldElemTrace,
                                                                        canonicalTraceL, normalSign,
                                                                        paramfldElemL,
                                                                        qfldElemL, afldElemL,
                                                                        wfldElemL, bfldElemL);
  }

protected:
  const std::vector<int> interiorTraceGroups_;
};



template <class PDE_>
class IntegrandBoundaryTrace_None : public IntegrandBoundaryTraceType< IntegrandBoundaryTrace_None<PDE_> >
{
public:
  typedef PDE_ PDE;
  typedef BCCategory::None Category;
  typedef HDG DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  typedef typename DLA::VectorS<PhysDim::D, Real> VecReal;

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  IntegrandBoundaryTrace_None(const std::vector<int>& BoundaryGroups) :
    BoundaryGroups_(BoundaryGroups) {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class Topology, class ElementParam >
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology    > ElementXFieldCell;

    typedef Element<ArrayQ<T>, TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<VectorArrayQ<T>, TopoDimCell, Topology> ElementAFieldCell;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef ArrayQ<T> IntegrandCellType;
    typedef ArrayQ<T> IntegrandTraceType;

    BasisWeighted( const ElementXFieldTrace& xfldElemTrace,
                   const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldCell& qfldElem,
                   const ElementAFieldCell& afldElem,
                   const ElementQFieldTrace& qIfldTrace ) :
                     xfldElemTrace_(xfldElemTrace),
                     canonicalTrace_(canonicalTrace),
                     xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                     qfldElem_(qfldElem), afldElem_(afldElem),
                     qIfldTrace_(qIfldTrace),
                     paramfldElem_( paramfldElem ),
                     nDOFElem_(qfldElem_.nDOF()),
                     nDOFTrace_(qIfldTrace_.nDOF())

    {
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOFElem_; }

    // boundary element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandCell[], const int neqnCell,
                                                          ArrayQ<Ti> integrandTrace[], const int neqnTrace ) const
    {
      for (int k = 0; k < neqnCell; k++) integrandCell[k] = 0;
      for (int k = 0; k < neqnTrace; k++) integrandTrace[k] = 0;
    }


  protected:
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementAFieldCell& afldElem_;
    const ElementQFieldTrace& qIfldTrace_;
    const ElementParam& paramfldElem_;

    const int nDOFElem_;
    const int nDOFTrace_;
  };

  template<class Tq, class Ta, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell,  class Topology, class ElementParam>
  class BasisWeighted_AUX
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef Element<      ArrayQ<Tq>, TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<VectorArrayQ<Ta>, TopoDimCell, Topology> ElementAFieldCell;
    typedef Element<ArrayQ<Tq>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef typename promote_Surreal<Tq, Ta>::type Ts;

    typedef VectorArrayQ<Ts> IntegrandType;

    BasisWeighted_AUX( const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                       const ElementParam& paramfldElem,
                       const ElementQFieldCell& qfldElem,
                       const ElementAFieldCell& afldElem,
                       const ElementQFieldTrace& qIfldTrace ) :
                         xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                         xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                         qfldElem_(qfldElem),
                         afldElem_(afldElem),
                         qIfldTrace_(qIfldTrace),
                         paramfldElem_( paramfldElem ),
                         nDOFElem_(qfldElem_.nDOF()),
                         nDOFTrace_(qIfldTrace_.nDOF())
    {
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFCell() const { return nDOFElem_; }
    int nDOFTrace() const { return nDOFTrace_; }

    // boundary element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, VectorArrayQ<Ti> integrandL[], const int neqnL ) const
    {
      for (int k = 0; k < neqnL; k++) integrandL[k] = 0;
    }

  protected:
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementAFieldCell& afldElem_;
    const ElementQFieldTrace& qIfldTrace_;
    const ElementParam& paramfldElem_;

    const int nDOFElem_;
    const int nDOFTrace_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class Topology, class ElementParam >
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef Element<      ArrayQ<T>, TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<VectorArrayQ<T>, TopoDimCell, Topology> ElementAFieldCell;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef IntegrandHDG< T, T > IntegrandCellType;
    typedef T IntegrandTraceType;

    FieldWeighted( const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldCell& qfldElem,
                   const ElementAFieldCell& afldElem,
                   const ElementQFieldCell& wfldElem,
                   const ElementAFieldCell& bfldElem,
                   const ElementQFieldTrace& qIfldTrace,
                   const ElementQFieldTrace& wIfldTrace ) :
                     xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                     xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                     qfldElem_(qfldElem), afldElem_(afldElem),
                     wfldElem_(wfldElem), bfldElem_(bfldElem),
                     qIfldTrace_(qIfldTrace), wIfldTrace_(wIfldTrace),
                     paramfldElem_( paramfldElem ),
                     nDOFElem_(qfldElem_.nDOF()),
                     nDOFTrace_(qIfldTrace_.nDOF())
    {
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
      SANS_ASSERT( wfldElem_.basis() == bfldElem_.basis() );
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFCell() const { return nDOFElem_; }
    int nDOFTrace() const { return nDOFTrace_; }

    // boundary element trace integrand
    void operator()( const QuadPointTraceType& sRefTrace, IntegrandCellType& integrandCell,
                                                          IntegrandTraceType& integrandTrace ) const
    {
      integrandCell = 0;
      integrandTrace = 0;
    }

  protected:
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementAFieldCell& afldElem_;
    const ElementQFieldCell& wfldElem_;
    const ElementAFieldCell& bfldElem_;
    const ElementQFieldTrace& qIfldTrace_;
    const ElementQFieldTrace& wIfldTrace_;
    const ElementParam& paramfldElem_;

    const int nDOFElem_;
    const int nDOFTrace_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted<T, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<T>,       TopoDimCell , Topology>& qfldElem,
            const Element<VectorArrayQ<T>, TopoDimCell , Topology>& afldElem,
            const Element<ArrayQ<T>,       TopoDimTrace, TopologyTrace>& qIfldTrace ) const
  {
    return BasisWeighted<T, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  Topology, ElementParam>(xfldElemTrace, canonicalTrace,
                                                                  paramfldElem, qfldElem, afldElem, qIfldTrace);
  }

  template<class Tq, class Ta, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted_AUX<Tq, Ta, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand_AUX(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                const ElementParam& paramfldElem,
                const Element<ArrayQ<Tq>, TopoDimCell, Topology >& qfldElem,
                const Element<VectorArrayQ<Ta>, TopoDimCell, Topology >& afldElem,
                const Element<ArrayQ<Tq>, TopoDimTrace, TopologyTrace >& qIfldTrace) const
  {
    return BasisWeighted_AUX<Tq, Ta, TopoDimTrace, TopologyTrace,
                             TopoDimCell,  Topology, ElementParam>(xfldElemTrace, canonicalTrace,
                                                                   paramfldElem, qfldElem, afldElem, qIfldTrace );
  }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
  FieldWeighted<T, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<T>,       TopoDimCell, Topology >& qfldElem,
            const Element<VectorArrayQ<T>, TopoDimCell, Topology >& afldElem,
            const Element<ArrayQ<T>,       TopoDimCell, Topology >& wfldElem,
            const Element<VectorArrayQ<T>, TopoDimCell, Topology >& bfldElem,
            const Element<ArrayQ<T>,       TopoDimTrace, TopologyTrace>& qIfldTrace,
            const Element<ArrayQ<T>,       TopoDimTrace, TopologyTrace>& wIfldTrace ) const
  {
    return FieldWeighted<T, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  Topology, ElementParam>(xfldElemTrace,
                                                                  canonicalTrace,
                                                                  paramfldElem,
                                                                  qfldElem, afldElem,
                                                                  wfldElem, bfldElem,
                                                                  qIfldTrace, wIfldTrace);
  }

private:
  const std::vector<int> BoundaryGroups_;
};


} //testspace
}

#endif  // INTEGRANDTEST_HDG_H
