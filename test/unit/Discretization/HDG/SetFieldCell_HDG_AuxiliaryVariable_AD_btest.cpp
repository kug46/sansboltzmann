// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// SetField_HDG_AuxiliaryVariable_AD_btest
// testing of auxiliary variable calculations for HDG with Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_Trace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"

#include "Field/XField_CellToTrace.h"

#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"
#include "Field/FieldData/FieldDataVectorD_BoundaryCell.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/HDG/IntegrandCell_HDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_sansLG_HDG.h"
#include "Discretization/HDG/SetFieldCell_HDG_AuxiliaryVariable.h"
#include "Discretization/HDG/SetFieldBoundaryTrace_HDG_AuxiliaryVariable.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/QuadratureOrder.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#include "IntegrandTest_HDG.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

namespace           // local definition
{

template<class SurrealClass, class IntegrandCell, class IntegrandITrace, class IntegrandBTrace,
         class JacAUXClass,
         class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
void computeAuxiliaryVariables(const IntegrandCell& fcnCell, const IntegrandITrace& fcnITrace, const IntegrandBTrace& fcnBTrace,
                               const FieldDataInvMassMatrix_Cell& mmfld, JacAUXClass& jacAUX_a_bcell,
                               const XField_CellToTrace<PhysDim, TopoDim>& connectivity,
                               const XField<PhysDim, TopoDim>& xfld, const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                               const Field<PhysDim, TopoDim, VectorArrayQ>& afld, const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                               const QuadratureOrder& quadOrder)
{
  jacAUX_a_bcell = 0.0;

  FieldDataVectorD_BoundaryCell<VectorArrayQ> rsdAUX_bcell(qfld);
  rsdAUX_bcell = 0.0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
      SetFieldBoundaryTrace_HDG_AuxiliaryVariable( fcnBTrace, rsdAUX_bcell, jacAUX_a_bcell ),
      xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

  std::vector<int> cellgroupsAux(fcnCell.nCellGroups());
  for (std::size_t i = 0; i < fcnCell.nCellGroups(); i++)
    cellgroupsAux[i] = fcnCell.cellGroup(i);

  //Note: jacAUX_a_bcell gets completed and inverted inside SetFieldCell_HDG_AuxiliaryVariable
  IntegrateCellGroups<TopoDim>::integrate(
      SetFieldCell_HDG_AuxiliaryVariable( fcnCell, fcnITrace, mmfld, connectivity,
                                          xfld, qfld, afld, qIfld,
                                          quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                          rsdAUX_bcell, cellgroupsAux, true, jacAUX_a_bcell),
      xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );
}

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( SetField_HDG_AuxiliaryVariable_AD_btest_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_AuxiliaryVariable_2Line_X1Q1R1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef PDEClass::template TensorMatrixQ<Real> TensorMatrixQ;

  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandITraceClass;
  typedef testspace::IntegrandBoundaryTrace_None<PDEClass> IntegrandBTraceClass; //dummy integrand for testing

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_UniformGrad source(1.0/4.0, 3.0/5.0);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid
  XField1D_2Line_X1_1Group xfld;

  XField_CellToTrace<PhysD1, TopoD1> connectivity(xfld);

  // solution
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 4);

  // line solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 4;

  // line solution data (right)
  qfld.DOF(2) = 7;
  qfld.DOF(3) = 9;

  // auxiliary variable
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  afld = 1;

  // interface solution
  Field_DG_Trace<PhysD1, TopoD1, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // node trace data
  qIfld.DOF(0) = 8; //interior trace DOF
  qIfld.DOF(1) = 0; //boundary trace DOF should be zero, since the "true" values only account for interior trace
  qIfld.DOF(2) = 0; //boundary trace DOF should be zero, since the "true" values only account for interior trace

  // Compute the field of inverse mass matrices
  FieldDataInvMassMatrix_Cell mmfld(afld);

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Local );

  // integrand
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandITraceClass fcnITrace( pde, disc, {0} );
  IntegrandBTraceClass fcnBTrace( {0, 1} );

  // quadrature rule
  QuadratureOrder quadOrder(xfld, 2);

  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());
  SLA::SparseVector<ArrayQ> rsdINTGlobal(qIfld.nDOF());
  rsdPDEGlobal = 0;
  rsdINTGlobal = 0;

  FieldDataMatrixD_BoundaryCell<TensorMatrixQ> jacAUX_a_bcell(qfld);

  computeAuxiliaryVariables<SurrealS<1>>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                         xfld, qfld, afld, qIfld, quadOrder);

  typedef Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ>::FieldCellGroupType<Line> AFieldCellGroupType;
  typedef AFieldCellGroupType::ElementType<> ElementAFieldClass;

  const AFieldCellGroupType& afldCell = afld.getCellGroup<Line>(0);

  ElementAFieldClass aElemL( afldCell.basis() );
  ElementAFieldClass aElemR( afldCell.basis() );

  int elemL = 0, elemR = 1;

  afldCell.getElement(aElemL, elemL);
  afldCell.getElement(aElemR, elemR);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  // Auxiliary variables
  Real aLxTrue[2];
  Real aRxTrue[2];

  //Auxiliary variable (left)
  aLxTrue[0] = ( -1 );   // Basis function 1
  aLxTrue[1] = ( 17 );   // Basis function 2

  //Auxiliary variable(right)
  aRxTrue[0] = ( 16 );   // Basis function 1
  aRxTrue[1] = ( -32 );   // Basis function 2

  SANS_CHECK_CLOSE( aLxTrue[0], aElemL.DOF(0)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( aLxTrue[1], aElemL.DOF(1)[0], small_tol, close_tol );

  SANS_CHECK_CLOSE( aRxTrue[0], aElemR.DOF(0)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( aRxTrue[1], aElemR.DOF(1)[0], small_tol, close_tol );
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_AuxiliaryVariable_2Triangle_X1Q1R1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef PDEClass::template TensorMatrixQ<Real> TensorMatrixQ;

  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandITraceClass;
  typedef testspace::IntegrandBoundaryTrace_None<PDEClass> IntegrandBTraceClass; //dummy integrand for testing

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_UniformGrad source(0.25, 0.6, -1.5);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid
  XField2D_2Triangle_X1_1Group xfld;

  XField_CellToTrace<PhysD2, TopoD2> connectivity(xfld);

  // solution
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 6 );

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  // triangle solution data (right)
  qfld.DOF(3) = 7;
  qfld.DOF(4) = 2;
  qfld.DOF(5) = 9;

  // auxiliary variables
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( afld.nDOF(), 6 );

  //Initialize afld to some random nonzero values, the solve should still produce the right values.
  afld.DOF(0) = { 0.2, 1.0};
  afld.DOF(1) = {-2.0, 3.0};
  afld.DOF(2) = { 0.5, 0.0};

  afld.DOF(3) = {-0.8, 4.0};
  afld.DOF(4) = { 2.3,-3.0};
  afld.DOF(5) = {-0.6, 0.7};

  // interface solution
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // trace data
  qIfld.DOF(0) =  8; //interior trace DOF
  qIfld.DOF(1) = -1; //interior trace DOF

  for (int i = 2; i < 10; i++)
    qIfld.DOF(i) =  0; //boundary trace DOF should be zero, since the "true" values only account for interior trace

  // Compute the field of inverse mass matrices
  FieldDataInvMassMatrix_Cell mmfld(qfld);

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Local );

  // integrand
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandITraceClass fcnITrace( pde, disc, {0} );
  IntegrandBTraceClass fcnBTrace( {0} );

  // quadrature rule
  QuadratureOrder quadOrder(xfld, 2);

  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());
  SLA::SparseVector<ArrayQ> rsdINTGlobal(qIfld.nDOF());
  rsdPDEGlobal = 0;
  rsdINTGlobal = 0;

  FieldDataMatrixD_BoundaryCell<TensorMatrixQ> jacAUX_a_bcell(qfld);

  computeAuxiliaryVariables<SurrealS<1>>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                         xfld, qfld, afld, qIfld, quadOrder);

  typedef Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ>::FieldCellGroupType<Triangle> AFieldCellGroupType;
  typedef AFieldCellGroupType::ElementType<> ElementAFieldClass;

  const AFieldCellGroupType& afldCell = afld.getCellGroup<Triangle>(0);

  ElementAFieldClass aElemL( afldCell.basis() );
  ElementAFieldClass aElemR( afldCell.basis() );

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  int elemL = 0, elemR = 1;

  afldCell.getElement(aElemL, elemL);
  afldCell.getElement(aElemR, elemR);

  Real aLxTrue[3], aLyTrue[3];
  Real aRxTrue[3], aRyTrue[3];

  //Auxiliary variables (left)
  aLxTrue[0] = ( 11 ); // Basis function 1
  aLxTrue[1] = ( 7 ); // Basis function 2
  aLxTrue[2] = ( 3 ); // Basis function 3
  aLyTrue[0] = ( 11 ); // Basis function 1
  aLyTrue[1] = ( 39 ); // Basis function 2
  aLyTrue[2] = ( -29 ); // Basis function 3

  //Auxiliary variables(right)
  aRxTrue[0] = ( -51 );   // Basis function 1
  aRxTrue[1] = ( 69 );   // Basis function 2
  aRxTrue[2] = ( -39 );   // Basis function 3
  aRyTrue[0] = ( -51 );   // Basis function 1
  aRyTrue[1] = ( -3 );   // Basis function 2
  aRyTrue[2] = ( 33 );   // Basis function 3

  SANS_CHECK_CLOSE( aLxTrue[0], aElemL.DOF(0)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( aLxTrue[1], aElemL.DOF(1)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( aLxTrue[2], aElemL.DOF(2)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( aLyTrue[0], aElemL.DOF(0)[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( aLyTrue[1], aElemL.DOF(1)[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( aLyTrue[2], aElemL.DOF(2)[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( aRxTrue[0], aElemR.DOF(0)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( aRxTrue[1], aElemR.DOF(1)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( aRxTrue[2], aElemR.DOF(2)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( aRyTrue[0], aElemR.DOF(0)[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( aRyTrue[1], aElemR.DOF(1)[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( aRyTrue[2], aElemR.DOF(2)[1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_AuxiliaryVariable_Box_Triangle_X1Q1R1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef PDEClass::template TensorMatrixQ<Real> TensorMatrixQ;

  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandITraceClass;
  typedef testspace::IntegrandBoundaryTrace_None<PDEClass> IntegrandBTraceClass; //dummy integrand for testing

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_UniformGrad source(0.25, 0.6, -1.5);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid
  XField2D_Box_Triangle_X1 xfld(2,2);

  XField_CellToTrace<PhysD2, TopoD2> connectivity(xfld);

  // solution
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  const int qDOF = qfld.nDOF();

  // solution data
  for (int i = 0; i < qDOF; i++)
    qfld.DOF(i) = sin(PI*i/((Real)qDOF));

  // auxiliary variables
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld1(xfld, qorder, BasisFunctionCategory_Hierarchical);
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld2(xfld, qorder, BasisFunctionCategory_Hierarchical);
  const int aDOF = afld1.nDOF();

  //Initialize aflds to two different solutions
  for (int i = 0; i < aDOF; i++)
  {
    afld1.DOF(i) = {sin(PI*i/((Real)aDOF)), -cos(PI*i/((Real)aDOF))};
    afld2.DOF(i) = {cos(PI*i/((Real)aDOF)),  sin(PI*i/((Real)aDOF))};
  }

  // interface solution
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  const int qIDOF = qIfld.nDOF();

  // trace data
  for (int i = 0; i < qIDOF; i++)
    qIfld.DOF(i) = cos(PI*i/((Real)qIDOF));

  // Compute the field of inverse mass matrices
  FieldDataInvMassMatrix_Cell mmfld(qfld);

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Local );

  // integrand
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandITraceClass fcnITrace( pde, disc, {0,1,2} );
  IntegrandBTraceClass fcnBTrace( {0, 1, 2, 3} );

  // quadrature rule
  QuadratureOrder quadOrder(xfld, 2);

  FieldDataMatrixD_BoundaryCell<TensorMatrixQ> jacAUX_a_bcell(qfld);

  //Solve for afld1
  computeAuxiliaryVariables<SurrealS<1>>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                         xfld, qfld, afld1, qIfld, quadOrder);

  //Solve for afld2
  computeAuxiliaryVariables<SurrealS<1>>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                         xfld, qfld, afld2, qIfld, quadOrder);

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  //Check if both aflds have the same values after the solve
  for (int i = 0; i < aDOF; i++)
  {
    SANS_CHECK_CLOSE( afld1.DOF(i)[0], afld2.DOF(i)[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( afld1.DOF(i)[1], afld2.DOF(i)[1], small_tol, close_tol );
  }
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_AuxiliaryVariable_Box_Triangle_X1Q1_LinearScalar_sansLG )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef PDEClass::template TensorMatrixQ<Real> TensorMatrixQ;

  typedef BCNDConvertSpace<PhysD2, BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_sansLG>> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

//  typedef testspace::IntegrandCell_HDG_None<PDEClass> IntegrandCellClass; //dummy integrand for testing
//  typedef testspace::IntegrandInteriorTrace_HDG_None<PDEClass> IntegrandITraceClass; //dummy integrand for testing

  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandITraceClass;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandBTraceClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_UniformGrad source(0.25, 0.6, -1.5);

  PDEClass pde( adv, visc, source );

  // BC
  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  // grid
  XField2D_Box_Triangle_X1 xfld(2,2);

  XField_CellToTrace<PhysD2, TopoD2> connectivity(xfld);

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Local );

  // integrand
//  IntegrandCellClass fcnCell( {0} );
//  IntegrandITraceClass fcnITrace( {0,1,2} );

  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandITraceClass fcnITrace( pde, disc, {0,1,2} );
  IntegrandBTraceClass fcnBTrace( pde, bc, {0,1,2,3}, disc );

  // solution
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  const int qDOF = qfld.nDOF();

  // solution data
  for (int i = 0; i < qDOF; i++)
    qfld.DOF(i) = sin(PI*i/((Real)qDOF));

  // auxiliary variables
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld1(xfld, qorder, BasisFunctionCategory_Hierarchical);
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld2(xfld, qorder, BasisFunctionCategory_Hierarchical);
  const int aDOF = afld1.nDOF();

  //Initialize aflds to two different solutions
  for (int i = 0; i < aDOF; i++)
  {
    afld1.DOF(i) = {sin(PI*i/((Real)aDOF)), -cos(PI*i/((Real)aDOF))};
    afld2.DOF(i) = {cos(PI*i/((Real)aDOF)),  sin(PI*i/((Real)aDOF))};
  }

  // interface solution
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  const int qIDOF = qIfld.nDOF();

  // trace data
  for (int i = 0; i < qIDOF; i++)
    qIfld.DOF(i) = cos(PI*i/((Real)qIDOF));

  // Compute the field of inverse mass matrices
  FieldDataInvMassMatrix_Cell mmfld(qfld);

  // quadrature rule
  QuadratureOrder quadOrder(xfld, 2);

  FieldDataMatrixD_BoundaryCell<TensorMatrixQ> jacAUX_a_bcell(qfld);

  //Solve for afld1
  computeAuxiliaryVariables<SurrealS<1>>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                         xfld, qfld, afld1, qIfld, quadOrder);

  //Solve for afld2
  computeAuxiliaryVariables<SurrealS<1>>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                         xfld, qfld, afld2, qIfld, quadOrder);

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-11;

  //Check if both aflds have the same values after the solve
  for (int i = 0; i < aDOF; i++)
  {
    SANS_CHECK_CLOSE( afld1.DOF(i)[0], afld2.DOF(i)[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( afld1.DOF(i)[1], afld2.DOF(i)[1], small_tol, close_tol );
  }
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
