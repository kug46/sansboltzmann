// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
//#define DISPLAY_FOR_DEBUGGING
//#define ISTIMINGBlock4x4

#include <boost/test/unit_test.hpp>
#include <boost/mpl/vector_c.hpp>

#include "pyrite_fstream.h"
#include "SANS_btest.h"

#define ALGEBRAICEQUATIONSET_HDGADVECTIVE_INSTANTIATE
#include "Discretization/HDG/AlgebraicEquationSet_HDGAdvective_impl.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_Trace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/output_Tecplot.h"

#define SCALARVECTOR_INSTANTIATE
#include "LinearAlgebra/SparseLinAlg/ScalarVector_impl.h"

#include "LinearAlgebra/SparseLinAlg/ScalarMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/WritePlainVector.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "pde/IBL/PDEIBL2D_impl.h"
#include "pde/IBL/BCIBL2D.h"
#include "pde/IBL/SetSolnDofCell_IBL2D.h"
#include "pde/IBL/SetVelDofCell_IBL2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#ifdef ISTIMINGBlock4x4
#include "tools/timer.h"
#endif

#include "unit/UnitGrids/XField2D_Line_X1_1Group.h"

#include "unit/Discretization/jacobianPingTest_btest.h"

using namespace std;
using namespace SANS;
using namespace SANS::BLA;
using namespace SANS::DLA;
using namespace SANS::SLA;

//----------------------------------------------------------------------------//
// Utility stuff
//
namespace SANS
{
typedef PhysD2 PhysDim;
typedef TopoD1 TopoDim;

typedef AlgEqSetTraits_Sparse AESTraitsTag;

/////////////////////////////////////////////////////////////////////
// IBL
typedef VarTypeDANCt VarType;
typedef VarData2DDANCt VarDataType;

typedef PDEIBL<PhysDim,VarType> PDEClassIBL;
typedef PDENDConvertSpace<PhysDim, PDEClassIBL> NDPDEClassIBL;

typedef BCIBL2DVector_NoWake<VarType> BCVectorClass;

typedef NDPDEClassIBL::template ArrayQ<Real> ArrayQIBL;
typedef NDPDEClassIBL::template ArrayParam<Real> ArrayQauxv;
typedef NDPDEClassIBL::VectorX VectorX;
typedef NDPDEClassIBL::template MatrixQ<Real> MatrixQ;

typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQauxv>,
                                       XField<PhysDim, TopoDim> >::type TupleFieldIBLParamType;

typedef AlgebraicEquationSet_HDGAdvective<NDPDEClassIBL,BCNDConvertSpace,BCVectorClass,
                                          AESTraitsTag,HDGAdv_manifold,TupleFieldIBLParamType> AlgebraicEquationSetClass;

typedef typename AlgebraicEquationSetClass::SystemMatrix SystemMatrixClass;
typedef typename AlgebraicEquationSetClass::SystemNonZeroPattern SystemNonZeroPatternClass;
typedef typename AlgebraicEquationSetClass::SystemVector SystemVectorClass;
typedef typename AlgebraicEquationSetClass::BCParams BCParamsIBL;
} // namespace SANS

//############################################################################//
BOOST_AUTO_TEST_SUITE( AlgebraicEquationSet_HDGAdvective_Line_IBL_airfoilOnly )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AlgebraicEquationSet_HDGAdvective_Line_IBL_airfoilOnly_test )
{
#ifdef ISTIMINGBlock4x4
  timer totaltime; // start timing the whole unit test
#endif

  /////////////////////////////////////////////////////////////////////
  // ---------- Set problem parameters ---------- //
  const int order_soln = 1; // solution order
  const int order_param_grid = 1;  // order: parameter field and grid

  const Real p0 = 1.e+5; // [Pa] stagnation pressure
  const Real T0 = 300; // [K] stagnation temperature

  // ---------- Set up IBL ---------- //
  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelIBL gasModel(gasModelDict);

  const Real mue = 1.827e-5;
  typename PDEClassIBL::ViscosityModelType viscosityModel(mue);

  PyDict transitionModelDict_wall;
  transitionModelDict_wall[TransitionModelParams::params.ntinit] = 0.0;
  transitionModelDict_wall[TransitionModelParams::params.ntcrit] = 9.0;
  transitionModelDict_wall[TransitionModelParams::params.xtr] = 10.0;
  transitionModelDict_wall[TransitionModelParams::params.isTransitionActive] = false;
  transitionModelDict_wall[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.turbulentBL;
  TransitionModelParams::checkInputs(transitionModelDict_wall); // necessary for checking input parameter validity
  const typename PDEClassIBL::TransitionModelType transitionModel_wall(transitionModelDict_wall);

  typedef typename PDEClassIBL::ThicknessesCoefficientsType ThicknessesCoefficientsType;
  typedef typename ThicknessesCoefficientsType::ClassParamsType ThicknessesCoefficientsParamsType;

  PyDict tcVarDict;
  tcVarDict[ThicknessesCoefficientsParamsType::params.nameCDclosure]
            = ThicknessesCoefficientsParamsType::params.nameCDclosure.ibl3eqm;

  // PDE
  NDPDEClassIBL pdeIBLWall(gasModel, viscosityModel, transitionModel_wall, tcVarDict);

  // BC
  // Create a BC dictionaries
  PyDict BCOutArgs;
  BCOutArgs[BCParamsIBL::params.BC.BCType] = BCParamsIBL::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCOut"] = BCOutArgs;

  // No exceptions should be thrown
  BCParamsIBL::checkInputs(PyBCList);

  std::map<std::string, std::vector<int>> bcBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  bcBoundaryGroups["BCOut"] = {0,1};

  // ---------- Set up grid ---------- //
  std::vector<VectorX> coordinates_a = { {1.0,  0.3},
                                         {0.0,  0.2},
                                         {1.0, -0.5} };
  XField2D_Line_X1_1Group xfld(coordinates_a);

  const std::vector<int> cellGroup_a = {0};
  const std::vector<int> interiorTraceGroups_a = {0};
  const std::vector<int> boundaryTraceGroups_a = {0,1};

  // ---------- Initialize solution/parameter fields ---------- //
  // parameter field: velocity and gradients
  Field_DG_Cell<PhysDim, TopoDim, VectorX> velfld( xfld, order_param_grid, BasisFunctionCategory_Hierarchical );
  Field_DG_Cell<PhysDim, TopoDim, VectorX> velxXfld( xfld, order_param_grid, BasisFunctionCategory_Hierarchical );
  Field_DG_Cell<PhysDim, TopoDim, VectorX> velzXfld( xfld, order_param_grid, BasisFunctionCategory_Hierarchical );

  const std::vector<Real> dataVel_a = {-1, -0.4, 0.9};
  for_each_CellGroup<TopoDim>::apply( SetVelocityDofCell_IBL2D(dataVel_a, cellGroup_a), (velfld, velxXfld, velzXfld, xfld) );

#if 1
  // manually make sure that velocity magnitude are genuinely discontinuous. Otherwise finite differencing used in jacobian ping test
  // will be problematic in the presence of possibly discontinuous residual caused by max() function in DG numerical flux
  velfld.DOF(1) = 1.2*velfld.DOF(1);
#endif

  BOOST_REQUIRE_EQUAL( xfld.nElem(), velfld.nElem() );
  BOOST_REQUIRE_EQUAL( xfld.nElem(), velxXfld.nElem() );
  BOOST_REQUIRE_EQUAL( xfld.nElem(), velzXfld.nElem() );

  // parameter field
  Field_DG_Cell<PhysDim, TopoDim, ArrayQauxv> qauxvfld(xfld, order_param_grid, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( xfld.nElem(), qauxvfld.nElem() );

  const auto paramInterpret = pdeIBLWall.getParamInterpreter();
  for (int i = 0; i < qauxvfld.nDOF(); ++i)
  {
    const typename PDEClassIBL::ParamInterpType::TensorX<Real> gradq1
      = { {velxXfld.DOF(i)[0], velxXfld.DOF(i)[1]},
          {velzXfld.DOF(i)[0], velzXfld.DOF(i)[1]} };
    qauxvfld.DOF(i) = paramInterpret.setDOFFrom(velfld.DOF(i), gradq1, p0, T0);
  }

  // IBL solution field
  Field_DG_Cell<PhysDim, TopoDim, ArrayQIBL> qIBLfld( xfld, order_soln, BasisFunctionCategory_Hierarchical );
  Field_DG_Trace<PhysDim, TopoDim, ArrayQIBL> qTraceIBLfld( xfld, order_soln, BasisFunctionCategory_Hierarchical );

  const int nnode = (int) coordinates_a.size();
  const std::vector<ArrayQIBL> dataQinit_a(nnode, pdeIBLWall.setDOFFrom(VarDataType(0.1,  1.2, 0.4, 2.3, 6.0, 0.002)));
  for_each_CellGroup<TopoDim>::apply( SetSolnDofCell_IBL2D<VarType>(dataQinit_a, cellGroup_a, order_soln), (xfld, qIBLfld) );

  BOOST_REQUIRE_EQUAL( xfld.nElem(), qIBLfld.nElem() );

  BOOST_CHECK(nnode == qTraceIBLfld.nDOF());
  for (int j = 0; j < nnode-2; ++j)
    qTraceIBLfld.DOF(j) = dataQinit_a.at(j+1);

  qTraceIBLfld.DOF(nnode-2) = dataQinit_a.at(0);
  qTraceIBLfld.DOF(nnode-1) = dataQinit_a.at(nnode-1);


  // Lagrange multiplier: Hierarchical, C0 (also at corners)
  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQIBL>
  lgfld( xfld, order_soln, BasisFunctionCategory_Legendre, BCParamsIBL::getLGBoundaryGroups(PyBCList, bcBoundaryGroups) );
  lgfld = 0.0;

  /////////////////////////////////////////////////////////////////////
  // SET UP EQUATION SETS

  const Real tol_eqn = 1.e-12;

  // IBL
  TupleFieldIBLParamType tupleIBLfld = (qauxvfld, xfld);

  QuadratureOrder quadratureOrder(xfld, 4);
  std::vector<Real> tol_IBL(AlgebraicEquationSetClass::nEqnSet, tol_eqn);
  AlgebraicEquationSetClass iblEqnSet(tupleIBLfld, qIBLfld, qTraceIBLfld, lgfld, pdeIBLWall, quadratureOrder, ResidualNorm_L2,
                                      tol_IBL, cellGroup_a, interiorTraceGroups_a, PyBCList, bcBoundaryGroups);

  SystemVectorClass sln(iblEqnSet.vectorStateSize());
  SystemVectorClass rsd(iblEqnSet.vectorEqSize());
  rsd = 0.0;

  iblEqnSet.fillSystemVector(sln);

  // residual
  SystemVectorClass rsdinit(iblEqnSet.vectorEqSize());
  rsdinit = 0;
  iblEqnSet.residual(sln, rsdinit);

  // jacobian nonzero pattern
  SystemNonZeroPatternClass nz(iblEqnSet.matrixSize());
  iblEqnSet.jacobian(sln, nz);

  // jacobian
  SystemMatrixClass jac(nz);
  iblEqnSet.jacobian(sln, jac);

#if defined(DISPLAY_FOR_DEBUGGING)
  fstream fout( "tmp/jac_init.mtx", fstream::out );
  cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
#endif

  BOOST_REQUIRE(jac.m() == AlgebraicEquationSetClass::nEqnSet);
  BOOST_REQUIRE(jac.n() == AlgebraicEquationSetClass::nSolSet);

  const std::vector<Real> step_vec = {2e-3, 1e-3};
  const std::vector<Real>& rate_range = {1.8, 2.3};
  const Real nonzero_tol = 1.e-13;
  const Real small_tol = 3.5e-12;

  const ArrayQIBL qArrayQscale = {0.5, 1.0, 0.5, 2.0, 1.0, 0.001};

#if defined(DISPLAY_FOR_DEBUGGING)
  std::cout << std::endl << "========== Testing jac_block = jac ==========" << std::endl << std::endl;
  const bool verbose = true;
#else
  const bool verbose = false;
#endif

  SystemVectorClass rsdp(iblEqnSet.vectorEqSize());
  SystemVectorClass rsdm(iblEqnSet.vectorEqSize());

  detail_jacobianPingTest::jacobianPingTest::
  ping_block<MatrixQ>(jac, rsdp, rsdm, rsdp, rsdm, sln, sln, iblEqnSet,
                      step_vec, qArrayQscale, rate_range, verbose, nonzero_tol, small_tol);

  // dump out jacobian for inspection
#if defined(DISPLAY_FOR_DEBUGGING) && 0
  std::string filename = "tmp/jac_HDGAdv_uncoupledIBL.mtx";
  std::cout << "Dumping " << filename << std::endl;
  WriteMatrixMarketFile( jac, filename );
#endif

  ////////////////////////////////////////////////////////////////////////////////////////
  // Test solver
  ////////////////////////////////////////////////////////////////////////////////////////
  { // UMFPACK linear solver for debugging
#if defined(DISPLAY_FOR_DEBUGGING)
    std::cout << "Testing linear solver" << std::endl;
#endif

    PyDict UMFPACKDict;
    UMFPACKParam::checkInputs(UMFPACKDict);

    UMFPACK<SystemMatrixClass> linearSolver(UMFPACKDict, iblEqnSet);

    SystemVectorClass r0(iblEqnSet.vectorEqSize());
    r0 = 0.0;
    DLA::index(r0[0][0],0) = 1.0;

    SystemVectorClass dq(iblEqnSet.vectorStateSize());
    dq = 0.0;

    linearSolver.solve(r0, dq);

    SystemVectorClass b(iblEqnSet.vectorEqSize());
    b = linearSolver.A() * dq;

    const ScalarVector b_plain(b), r0_plain(r0);
    const Real tol_small = 3e-14, tol_close = 4.e-14;
    for (int j = 0; j < b_plain.m; ++j)
      SANS_CHECK_CLOSE(r0_plain.v[j], b_plain.v[j], tol_small, tol_close);
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
