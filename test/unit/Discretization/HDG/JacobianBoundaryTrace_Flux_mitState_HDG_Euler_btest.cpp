// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianBoundaryTrace_HDG_AD_btest
// testing of HDG boundary trace-integral jacobian: advection-diffusion

#include <boost/mpl/list.hpp>

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/BCEuler2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"

#include "Field/XField_CellToTrace.h"

#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_Flux_mitState_HDG.h"
#include "Discretization/HDG/SetFieldCell_HDG_AuxiliaryVariable.h"
#include "Discretization/HDG/SetFieldBoundaryTrace_HDG_AuxiliaryVariable.h"

#include "Discretization/HDG/ResidualCell_HDG.h"
#include "Discretization/HDG/JacobianCell_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_HDG_AuxiliaryVariable.h"
#include "Discretization/HDG/ResidualBoundaryTrace_HDG.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/QuadratureOrder.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#include "IntegrandTest_HDG.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct
namespace       // local definition
{

template<class SurrealClass, class IntegrandCell, class IntegrandITrace, class IntegrandBTrace,
         class JacAUXClass,
         class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
void computeAuxiliaryVariables(const IntegrandCell& fcnCell, const IntegrandITrace& fcnITrace, const IntegrandBTrace& fcnBTrace,
                               const FieldDataInvMassMatrix_Cell& mmfld, JacAUXClass& jacAUX_a_bcell,
                               const XField_CellToTrace<PhysDim, TopoDim>& connectivity,
                               const XField<PhysDim, TopoDim>& xfld, const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                               const Field<PhysDim, TopoDim, VectorArrayQ>& afld, const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                               const QuadratureOrder& quadOrder)
{
  jacAUX_a_bcell = 0.0;

  FieldDataVectorD_BoundaryCell<VectorArrayQ> rsdAUX_bcell(qfld);
  rsdAUX_bcell = 0.0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
      SetFieldBoundaryTrace_HDG_AuxiliaryVariable( fcnBTrace, rsdAUX_bcell, jacAUX_a_bcell ),
      xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

  std::vector<int> cellgroupsAux(fcnCell.nCellGroups());
  for (std::size_t i = 0; i < fcnCell.nCellGroups(); i++)
    cellgroupsAux[i] = fcnCell.cellGroup(i);

  //Note: jacAUX_a_bcell gets completed and inverted inside SetFieldCell_HDG_AuxiliaryVariable
  IntegrateCellGroups<TopoDim>::integrate(
      SetFieldCell_HDG_AuxiliaryVariable( fcnCell, fcnITrace, mmfld, connectivity,
                                          xfld, qfld, afld, qIfld,
                                          quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                          rsdAUX_bcell, cellgroupsAux, true, jacAUX_a_bcell),
      xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );
}

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianBoundaryTrace_Flux_mitState_HDG_Euler_test_suite )

typedef boost::mpl::list< SurrealS<4> > Surreals;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( HDG_2D_OutflowSubsonic_Pressure_mitState_Triangle, SurrealClass, Surreals )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template MatrixQ<Real> MatrixQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef NDPDEClass::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef NDPDEClass::template TensorMatrixQ<Real> TensorMatrixQ;

  typedef BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState, PDEClass> BCClass;
  typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;

  typedef testspace::IntegrandCell_HDG_None<NDPDEClass> IntegrandCellClass; //dummy integrand for testing
  typedef IntegrandInteriorTrace_HDG<NDPDEClass> IntegrandITraceClass;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, HDG> IntegrandBTraceClass;

  static const int D = PhysD2::D;
  static const int N = NDPDEClass::N;

  // q step size
  std::vector<Real> qstep0 = {5e-5, 5e-5, 5e-5, 5e-5}; // first step sizes
  std::vector<Real> qstep1 = {1e-5, 1e-5, 1e-5, 1e-5}; // second step sizes

  // a step size
  std::vector<Real> astep0 = {5e-5, 5e-5, 5e-5, 5e-5}; // first step sizes
  std::vector<Real> astep1 = {1e-5, 1e-5, 1e-5, 1e-5}; // second step sizes

  // qI step size
  std::vector<Real> qIstep0 = {5e-5, 5e-5, 5e-5, 5e-5}; // first step sizes
  std::vector<Real> qIstep1 = {1e-5, 1e-5, 1e-5, 1e-5}; // second step sizes

  // PDE
  const Real gamma = 1.4;
  const Real R = 1;   // J/(kg K)
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Raw);

  // Test conditions - M = 0.3, T0 = 300.1, P0 = 101325; alpha = 0.1 rad
  Real rho, u, v, t, p; // Taken from BCEuler2D_btest
  rho = 1.2;
  t = 1.1;
  p  = R*rho*t;

  Real M = 0.3;
  Real c =  sqrt(gamma*R*t); // sqrt ( gamma, R , t)
  Real alpha = 0.1;
  u = M*c*cos(alpha);
  v = M*c*sin(alpha);

  // BC
  NDBCClass bc(pde, p);

  std::vector<int> bcList = {1,2}; // top and right are 1,2 (x = 1) and (y = 1)

  // static tests
  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 3 );

  // grid: X1
  XField2D_Box_UnionJack_Triangle_X1 xfld(2, 2);

  XField_CellToTrace<PhysD2, TopoD2> connectivity(xfld);

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Local, Gradient );

  // integrand
  std::vector<int> cellgroups = {0};
  IntegrandCellClass fcnCell( cellgroups );
  IntegrandITraceClass fcnITrace( pde, disc, {0,1,2} );
  IntegrandBTraceClass fcnBTrace( pde, bc, bcList, disc );

  // solution: P1
  for (int qorder = 0; qorder <= 2; qorder++ )
  {
    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
    const int qDOF = qfld.nDOF();

    // solution data
    qfld = pde.setDOFFrom(DensityVelocityPressure2D<Real>( rho, u, v, t));

    // auxiliary variable:
    Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, qorder, BasisFunctionCategory_Legendre);

    // afld = 0;
    // auxiliary data
    for (int d = 0; d < D; d++)
      for (int n = 0; n < N; n++)
        for (int i = 0; i < qDOF; i++)
          afld.DOF(i)[d][n] = 0.1*sin(PI*i/((Real)qDOF));

    // trace variable
    Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, qorder, BasisFunctionCategory_Legendre);
    const int qIDOF = qIfld.nDOF();

    // interface data
    qIfld = pde.setDOFFrom(DensityVelocityPressure2D<Real>( rho, u, v, t));

    // Compute the field of inverse mass matrices
    FieldDataInvMassMatrix_Cell mmfld(qfld);

    // quadrature rule
    QuadratureOrder quadOrder(xfld, 2*qorder);

    // jacobian via FD w/ residual operator;
    // topology-specific single group interface

    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
    SLA::SparseVector<ArrayQ> rsdINTGlobal0(qIDOF), rsdINTGlobal1(qIDOF);

    // coarse step
    DLA::MatrixD<MatrixQ> jacPDE_q_step0(qDOF,qDOF) , jacPDE_qI_step0(qDOF,qIDOF);
    DLA::MatrixD<MatrixQ> jacINT_q_step0(qIDOF,qDOF), jacINT_qI_step0(qIDOF,qIDOF);
    // fine step
    DLA::MatrixD<MatrixQ> jacPDE_q_step1(qDOF,qDOF) , jacPDE_qI_step1(qDOF,qIDOF);
    DLA::MatrixD<MatrixQ> jacINT_q_step1(qIDOF,qDOF), jacINT_qI_step1(qIDOF,qIDOF);

    FieldDataMatrixD_BoundaryCell<TensorMatrixQ> jacAUX_a_bcell(qfld);

    // jacobian wrt q
    for (int j = 0; j < qDOF; j++)
    {
      for (int n = 0; n < N; n++ )
      {
        //---------------Ping with large step size-------------------
        // std::cout<< "qfld.DOF(" << j << ")[" << n << "] = " << qfld.DOF(j)[n];
        qfld.DOF(j)[n] += qstep0[n];
        // std::cout<< ", qfld.DOF(" << j << ")[" << n << "] = " << qfld.DOF(j)[n] << std::endl;

        // compute the perturbed BC residual
        rsdPDEGlobal1 = 0;
        rsdINTGlobal1 = 0;

        computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                                xfld, qfld, afld, qIfld, quadOrder);

        //Compute the residuals at state1
        IntegrateCellGroups<TopoD2>::integrate(
            ResidualCell_HDG( fcnCell, fcnITrace, connectivity,
                              xfld, qfld, afld, qIfld,
                              quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                             rsdPDEGlobal1, rsdINTGlobal1 ),
            xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

        IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_HDG(fcnBTrace, rsdPDEGlobal1, rsdINTGlobal1),
                                                                    xfld, (qfld, afld), qIfld,
                                                                    quadOrder.boundaryTraceOrders.data(),
                                                                    quadOrder.boundaryTraceOrders.size() );

        qfld.DOF(j)[n] -= 2*qstep0[n];

        rsdPDEGlobal0 = 0;
        rsdINTGlobal0 = 0;

        computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                                xfld, qfld, afld, qIfld, quadOrder);

        //Compute the residuals at state0
        IntegrateCellGroups<TopoD2>::integrate(
            ResidualCell_HDG( fcnCell, fcnITrace, connectivity,
                              xfld, qfld, afld, qIfld,
                              quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                             rsdPDEGlobal0, rsdINTGlobal0 ),
            xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

        IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_HDG(fcnBTrace, rsdPDEGlobal0, rsdINTGlobal0),
                                                                    xfld, (qfld, afld), qIfld,
                                                                    quadOrder.boundaryTraceOrders.data(),
                                                                    quadOrder.boundaryTraceOrders.size() );

        qfld.DOF(j)[n] += qstep0[n];

        for (int i = 0; i < qDOF; i++)
          for (int m = 0; m < N; m++)
            jacPDE_q_step0(i,j)(m,n) = (rsdPDEGlobal1[i][m] - rsdPDEGlobal0[i][m])/(2.0*qstep0[n]);
            // std::cout<< "rsdPDEGlobal1[" << i << "][" << m << "] = " << rsdPDEGlobal1[i][m] <<
            //           ", rsdPDEGlobal0[" << i << "][" << m << "] = " << rsdPDEGlobal0[i][m] <<
            //           ", qstep0[" << n << "] = " << qstep0[n] << std::endl;

        for (int i = 0; i < qIDOF; i++)
          for (int m = 0; m < N; m++)
            jacINT_q_step0(i,j)(m,n) = (rsdINTGlobal1[i][m] - rsdINTGlobal0[i][m])/(2.0*qstep0[n]);


        //---------------Ping with small step size-------------------
        qfld.DOF(j)[n] += qstep1[n];

        // compute the perturbed BC residual
        rsdPDEGlobal1 = 0;
        rsdINTGlobal1 = 0;

        computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                                xfld, qfld, afld, qIfld, quadOrder);
        //Compute the residuals at state1
        IntegrateCellGroups<TopoD2>::integrate(
            ResidualCell_HDG( fcnCell, fcnITrace, connectivity,
                              xfld, qfld, afld, qIfld,
                              quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                             rsdPDEGlobal1, rsdINTGlobal1 ),
            xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

        IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_HDG(fcnBTrace, rsdPDEGlobal1, rsdINTGlobal1),
                                                                    xfld, (qfld, afld), qIfld,
                                                                    quadOrder.boundaryTraceOrders.data(),
                                                                    quadOrder.boundaryTraceOrders.size() );

        qfld.DOF(j)[n] -= 2*qstep1[n];

        rsdPDEGlobal0 = 0;
        rsdINTGlobal0 = 0;

        computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                                xfld, qfld, afld, qIfld, quadOrder);

        //Compute the residuals at state0
        IntegrateCellGroups<TopoD2>::integrate(
            ResidualCell_HDG( fcnCell, fcnITrace, connectivity,
                              xfld, qfld, afld, qIfld,
                              quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                             rsdPDEGlobal0, rsdINTGlobal0 ),
            xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

        IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_HDG(fcnBTrace, rsdPDEGlobal0, rsdINTGlobal0),
                                                                    xfld, (qfld, afld), qIfld,
                                                                    quadOrder.boundaryTraceOrders.data(),
                                                                    quadOrder.boundaryTraceOrders.size() );

        qfld.DOF(j)[n] += qstep1[n];

        for (int i = 0; i < qDOF; i++)
          for (int m = 0; m < N; m++)
            jacPDE_q_step1(i,j)(m,n) = (rsdPDEGlobal1[i][m] - rsdPDEGlobal0[i][m])/(2.0*qstep1[n]);

        for (int i = 0; i < qIDOF; i++)
          for (int m = 0; m < N; m++)
            jacINT_q_step1(i,j)(m,n) = (rsdINTGlobal1[i][m] - rsdINTGlobal0[i][m])/(2.0*qstep1[n]);
      }
    }

    // jacobian wrt qI
    for (int n = 0; n < N; n++)
    {
      for (int j = 0; j < qIDOF; j++)
      {
        //---------------Ping with large step size-------------------
        qIfld.DOF(j)[n] += qIstep0[n];

        // compute the perturbed BC residual
        rsdPDEGlobal1 = 0;
        rsdINTGlobal1 = 0;

        computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                                xfld, qfld, afld, qIfld, quadOrder);

        //Compute the residuals at state1
        IntegrateCellGroups<TopoD2>::integrate(
            ResidualCell_HDG( fcnCell, fcnITrace, connectivity,
                              xfld, qfld, afld, qIfld,
                              quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                             rsdPDEGlobal1, rsdINTGlobal1 ),
            xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

        IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_HDG(fcnBTrace, rsdPDEGlobal1, rsdINTGlobal1),
                                                                    xfld, (qfld, afld), qIfld,
                                                                    quadOrder.boundaryTraceOrders.data(),
                                                                    quadOrder.boundaryTraceOrders.size() );

        qIfld.DOF(j)[n] -= 2*qIstep0[n];

        rsdPDEGlobal0 = 0;
        rsdINTGlobal0 = 0;

        computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                                xfld, qfld, afld, qIfld, quadOrder);

        //Compute the residuals at state0
        IntegrateCellGroups<TopoD2>::integrate(
            ResidualCell_HDG( fcnCell, fcnITrace, connectivity,
                              xfld, qfld, afld, qIfld,
                              quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                             rsdPDEGlobal0, rsdINTGlobal0 ),
            xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

        IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_HDG(fcnBTrace, rsdPDEGlobal0, rsdINTGlobal0),
                                                                    xfld, (qfld, afld), qIfld,
                                                                    quadOrder.boundaryTraceOrders.data(),
                                                                    quadOrder.boundaryTraceOrders.size() );

        qIfld.DOF(j)[n] += qIstep0[n];

        for (int i = 0; i < qDOF; i++)
          for (int m = 0; m < N; m++)
            jacPDE_qI_step0(i,j)(m,n) = (rsdPDEGlobal1[i][m] - rsdPDEGlobal0[i][m])/(2.0*qIstep0[n]);

        for (int i = 0; i < qIDOF; i++)
          for (int m = 0; m < N; m++)
            jacINT_qI_step0(i,j)(m,n) = (rsdINTGlobal1[i][m] - rsdINTGlobal0[i][m])/(2.0*qIstep0[n]);


        //---------------Ping with small step size-------------------
        qIfld.DOF(j)[n] += qIstep1[n];

        // compute the perturbed BC residual
        rsdPDEGlobal1 = 0;
        rsdINTGlobal1 = 0;

        computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                                xfld, qfld, afld, qIfld, quadOrder);

        //Compute the residuals at state1
        IntegrateCellGroups<TopoD2>::integrate(
            ResidualCell_HDG( fcnCell, fcnITrace, connectivity,
                              xfld, qfld, afld, qIfld,
                              quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                             rsdPDEGlobal1, rsdINTGlobal1 ),
            xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

        IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_HDG(fcnBTrace, rsdPDEGlobal1, rsdINTGlobal1),
                                                                    xfld, (qfld, afld), qIfld,
                                                                    quadOrder.boundaryTraceOrders.data(),
                                                                    quadOrder.boundaryTraceOrders.size() );

        qIfld.DOF(j)[n] -= 2*qIstep1[n];

        rsdPDEGlobal0 = 0;
        rsdINTGlobal0 = 0;

        computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                                xfld, qfld, afld, qIfld, quadOrder);

        //Compute the residuals at state0
        IntegrateCellGroups<TopoD2>::integrate(
            ResidualCell_HDG( fcnCell, fcnITrace, connectivity,
                              xfld, qfld, afld, qIfld,
                              quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                             rsdPDEGlobal0, rsdINTGlobal0 ),
            xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

        IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_HDG(fcnBTrace, rsdPDEGlobal0, rsdINTGlobal0),
                                                                    xfld, (qfld, afld), qIfld,
                                                                    quadOrder.boundaryTraceOrders.data(),
                                                                    quadOrder.boundaryTraceOrders.size() );

        qIfld.DOF(j)[n] += qIstep1[n];

        for (int i = 0; i < qDOF; i++)
          for (int m = 0; m < N; m++)
            jacPDE_qI_step1(i,j)(m,n) = (rsdPDEGlobal1[i][m] - rsdPDEGlobal0[i][m])/(2.0*qIstep1[n]);

        for (int i = 0; i < qIDOF; i++)
          for (int m = 0; m < N; m++)
            jacINT_qI_step1(i,j)(m,n) = (rsdINTGlobal1[i][m] - rsdINTGlobal0[i][m])/(2.0*qIstep1[n]);
      }
    }

    // jacobian via Surreal

    DLA::MatrixD<MatrixQ> jacPDEGlob_q(qDOF,qDOF);
    DLA::MatrixD<MatrixQ> jacPDEGlob_qI(qDOF,qIDOF);
    DLA::MatrixD<MatrixQ> jacINTGlob_q(qIDOF,qDOF);
    DLA::MatrixD<MatrixQ> jacINTGlob_qI(qIDOF,qIDOF);
    jacPDEGlob_q = 0;
    jacPDEGlob_qI = 0;
    jacINTGlob_q = 0;
    jacINTGlob_qI = 0;

    computeAuxiliaryVariables<SurrealClass>(fcnCell, fcnITrace, fcnBTrace, mmfld, jacAUX_a_bcell, connectivity,
                                            xfld, qfld, afld, qIfld, quadOrder);

    // Compute Jacobians via Surreals
    FieldDataMatrixD_BoundaryCell<VectorMatrixQ> jacAUX_q_bcell(qfld);
    FieldDataMatrixD_BoundaryTrace<VectorMatrixQ> jacAUX_qI_btrace(qfld, qIfld);
    jacAUX_q_bcell = 0.0;
    jacAUX_qI_btrace = 0.0;

    std::vector<std::vector<std::vector<int>>> mapDOFGlobal_qI(xfld.nBoundaryTraceGroups());

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
        JacobianBoundaryTrace_HDG_AuxiliaryVariable<SurrealClass>( fcnBTrace, jacAUX_q_bcell, jacAUX_qI_btrace, mapDOFGlobal_qI ),
        xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

    IntegrateCellGroups<TopoD2>::integrate(
        JacobianCell_HDG<SurrealClass>( fcnCell, fcnITrace,
                                        mmfld, jacAUX_a_bcell, jacAUX_q_bcell, jacAUX_qI_btrace,
                                        mapDOFGlobal_qI, connectivity,
                                        xfld, qfld, afld, qIfld,
                                        quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                        cellgroups,
                                        jacPDEGlob_q, jacPDEGlob_qI,
                                        jacINTGlob_q, jacINTGlob_qI ),
         xfld, (qfld, afld), quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
        JacobianBoundaryTrace_HDG<SurrealClass>( fcnCell, fcnITrace, fcnBTrace,
                                                 jacAUX_a_bcell, jacAUX_q_bcell, jacAUX_qI_btrace,
                                                 mapDOFGlobal_qI, connectivity,
                                                 xfld, qfld, afld, qIfld,
                                                 quadOrder.cellOrders.data(), quadOrder.cellOrders.size(),
                                                 quadOrder.interiorTraceOrders.data(), quadOrder.interiorTraceOrders.size(),
                                                 jacPDEGlob_q, jacPDEGlob_qI,
                                                 jacINTGlob_q, jacINTGlob_qI),
        xfld, (qfld, afld), qIfld, quadOrder.boundaryTraceOrders.data(), quadOrder.boundaryTraceOrders.size() );

    const Real small_tol = 5e-9;
    const Real close_tol = 1e-5;

    for (int i = 0; i <  qDOF; i++)
      for (int j = 0; j < qDOF; j++)
        for (int n = 0; n < N; n++)
          for (int m = 0; m < N; m++)
          {
            Real error0 = fabs(jacPDE_q_step0(i,j)(m,n) - jacPDEGlob_q(i,j)(m,n));
            Real error1 = fabs(jacPDE_q_step1(i,j)(m,n) - jacPDEGlob_q(i,j)(m,n));

//            std::cout << "PDE_q(" << i << ", " << j << ", " << m << ", " << n << ") : "
//                      << jacPDE_q_step1(i,j)(m,n) << ", " << jacPDEGlob_q(i,j)(m,n) << ", " << error1 << std::endl;

            if (error1 > small_tol)
            {
              Real rate = log(error1/error0) / log(qstep1[n]/qstep0[n]);
              BOOST_CHECK_MESSAGE( rate >= 1.8 && rate <= 2.5, "Rate check failed: rate = " << rate <<
                                   ", errors = [" << error0 << "," << error1 << "]" );
            }
            SANS_CHECK_CLOSE( jacPDE_q_step1(i,j)(m,n), jacPDEGlob_q(i,j)(m,n), small_tol, close_tol );
          }


    for (int i = 0; i <  qDOF; i++)
      for (int j = 0; j < qIDOF; j++)
        for (int n = 0; n < N; n++)
          for (int m = 0; m < N; m++)
          {
            Real error0 = fabs(jacPDE_qI_step0(i,j)(m,n) - jacPDEGlob_qI(i,j)(m,n));
            Real error1 = fabs(jacPDE_qI_step1(i,j)(m,n) - jacPDEGlob_qI(i,j)(m,n));
            if (error1 > small_tol)
            {
              Real rate = log(error1/error0) / log(qIstep1[n]/qIstep0[n]);
              BOOST_CHECK_MESSAGE( rate >= 1.8 && rate <= 2.5, "Rate check failed: rate = " << rate <<
                                   ", errors = [" << error0 << "," << error1 << "]" );
            }

            SANS_CHECK_CLOSE( jacPDE_qI_step1(i,j)(m,n), jacPDEGlob_qI(i,j)(m,n), small_tol, close_tol );
          }


    for (int i = 0; i <  qIDOF; i++)
      for (int j = 0; j < qDOF; j++)
        for (int n = 0; n < N; n++)
          for (int m = 0; m < N; m++)
          {
            Real error0 = fabs(jacINT_q_step0(i,j)(m,n) - jacINTGlob_q(i,j)(m,n));
            Real error1 = fabs(jacINT_q_step1(i,j)(m,n) - jacINTGlob_q(i,j)(m,n));
            if (error1 > small_tol)
            {
              Real rate = log(error1/error0) / log(qstep1[n]/qstep0[n]);
              BOOST_CHECK_MESSAGE( rate >= 1.8 && rate <= 2.5, "Rate check failed: rate = " << rate <<
                                   ", errors = [" << error0 << "," << error1 << "]" );
            }
            SANS_CHECK_CLOSE( jacINT_q_step1(i,j)(m,n), jacINTGlob_q(i,j)(m,n), small_tol, close_tol );
          }

    for (int i = 0; i <  qIDOF; i++)
      for (int j = 0; j < qIDOF; j++)
        for (int n = 0; n < N; n++)
          for (int m = 0; m < N; m++)
          {
            Real error0 = fabs(jacINT_qI_step0(i,j)(m,n) - jacINTGlob_qI(i,j)(m,n));
            Real error1 = fabs(jacINT_qI_step1(i,j)(m,n) - jacINTGlob_qI(i,j)(m,n));
            if (error1 > small_tol)
            {
              Real rate = log(error1/error0) / log(qIstep1[n]/qIstep0[n]);
              BOOST_CHECK_MESSAGE( rate >= 1.8 && rate <= 2.5, "Rate check failed: rate = " << rate <<
                                   ", errors = [" << error0 << "," << error1 << "]" );
            }
            SANS_CHECK_CLOSE( jacINT_qI_step1(i,j)(m,n), jacINTGlob_qI(i,j)(m,n), small_tol, close_tol );
          }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
