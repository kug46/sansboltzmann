// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
//#define DISPLAY_FOR_DEBUGGING

#include <boost/test/unit_test.hpp>
#include <boost/mpl/vector_c.hpp>

#include "SANS_btest.h"
#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/ForcingFunction1D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDGAdvective.h"

#include "Discretization/IntegrateCellGroups.h"

#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_Trace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/XField1D/XField1D.h"

#include "unit/Discretization/jacobianPingTest_btest.h"

using namespace SANS;

namespace SANS
{
typedef PhysD1 PhysDim;
typedef TopoD1 TopoDim;

typedef AdvectiveFlux1D_Uniform AdvectiveFluxClass;
typedef ViscousFlux1D_None ViscousFluxClass;
typedef Source1D_Uniform SourceClass;
typedef PDEAdvectionDiffusion<PhysDim,AdvectiveFluxClass,ViscousFluxClass,SourceClass> PDEClass;
typedef PDENDConvertSpace<PhysDim, PDEClass> NDPDEClass;

typedef ForcingFunction1D_MMS<PDEClass> ForcingClass;

typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
typedef NDPDEClass::template MatrixQ<Real> MatrixQ;

typedef ScalarFunction1D_Sine SolutionExactClass;
typedef SolnNDConvertSpace<PhysDim, SolutionExactClass> NDSolutionExactClass;

typedef BCTypeFunction_mitStateParam BCParamType_mitState;
typedef BCAdvectionDiffusionParams<PhysDim,BCParamType_mitState> BCParamsType_mitState;

typedef BCAdvectionDiffusion<PhysDim,BCTypeFunction_mitState<AdvectiveFluxClass,ViscousFluxClass>> BCClass;
typedef BCNDConvertSpace<PhysDim, BCClass> NDBCClass;
typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;

typedef BCAdvectionDiffusion1DVector<AdvectiveFluxClass,ViscousFluxClass> BCVector;

typedef AlgebraicEquationSet_HDGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
          AlgEqSetTraits_Sparse, HDGAdv, XField<PhysDim, TopoDim> > AlgebraicEquationSetClass;
typedef typename AlgebraicEquationSetClass::SystemMatrix SystemMatrixClass;
typedef typename AlgebraicEquationSetClass::SystemNonZeroPattern SystemNonZeroPatternClass;
typedef typename AlgebraicEquationSetClass::SystemVector SystemVectorClass;
typedef typename AlgebraicEquationSetClass::BCParams BCParams;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( AlgebraicEquationSet_HDGAdvective_Advection )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AlgebraicEquationSet_HDGAdvective_Advection_test )
{
  NDSolutionExactClass solnExact;

  // PDE
  AdvectiveFluxClass adv( 1.1 );
  ViscousFluxClass visc;
  SourceClass source(1.0);

  PyDict scalarFunctionDict;
  //TODO: have to manually make sure that the Function.Name is consistent with SolutionExactClass
  scalarFunctionDict[BCParamsType_mitState::params.Function.Name] =
      BCParamsType_mitState::params.Function.Sine;
  scalarFunctionDict[SolutionExactClass::ParamsType::params.a] = 3.0;

  PyDict MMS_solnDict;
  MMS_solnDict[BCParamsType_mitState::params.Function] = scalarFunctionDict;

  const auto MMS_soln = ScalarFunction1DParams::getFunction(MMS_solnDict);

  std::shared_ptr<ForcingClass> forcingptr( new ForcingClass(*MMS_soln));

  NDPDEClass pde( adv, visc, source, forcingptr ); // solution as argument means a forcing function is generated

  // BC
  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCParamsType_mitState::params.Function] = scalarFunctionDict;
  BCSoln[BCParamsType_mitState::params.SolutionBCType] =
         BCParamsType_mitState::params.SolutionBCType.Robin;
  BCSoln[BCParamsType_mitState::params.Upwind] = false;

  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCIn"] = BCSoln;
  PyBCList["BCOut"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCIn"] = {0};
  BCBoundaryGroups["BCOut"] = {1};

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  const std::vector<int> cellGroups = {0};
  const std::vector<int> interiorTraceGroups = {0};
  const std::vector<int> boundaryTraceGroups = {0,1};

  // grid:
  const int polyDegMax = 5;
  const int nelem = 3;

  for (int polyDeg = 0; polyDeg <= polyDegMax; ++polyDeg)
  {
#if defined(DISPLAY_FOR_DEBUGGING)
    std::cout << std::endl << "========== Solution polynomial degree p = " << polyDeg <<  " ==========" << std::endl << std::endl;
#endif

    XField1D xfld(nelem);

    // HDG solution fields
    Field_DG_Cell<PhysDim, TopoDim, ArrayQ> qfld(xfld, polyDeg, BasisFunctionCategory_Legendre); // cell solution
    Field_DG_Trace<PhysDim, TopoDim, ArrayQ> qIfld(xfld, polyDeg, BasisFunctionCategory_Legendre); // interior trace solution
    qfld = 0.0;
    qIfld = 0.0;

    // Lagrange multiplier
    const std::vector<int> LG_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
    Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> lgfld(xfld, polyDeg, BasisFunctionCategory_Legendre, LG_BGroup_list );

    // quadrature rule
    QuadratureOrder quadratureOrder(xfld, -1);

    const std::vector<Real> tol(AlgebraicEquationSetClass::nEqnSet, 1e-10);

    AlgebraicEquationSetClass AlgEqSet(xfld, qfld, qIfld, lgfld, pde, quadratureOrder, ResidualNorm_L2, tol,
                                       cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups );

    // residual vectors

    SystemVectorClass q(AlgEqSet.vectorStateSize());
    SystemVectorClass rsd(AlgEqSet.vectorEqSize());

    AlgEqSet.fillSystemVector(q);

    // dump jacobian
    SystemNonZeroPatternClass nz(AlgEqSet.matrixSize());
    AlgEqSet.jacobian(q,nz);
    SystemMatrixClass jac(nz);
    AlgEqSet.jacobian(q, jac);

#if defined(DISPLAY_FOR_DEBUGGING)
    std::cout << "writing initial jacobian tmp/jac_initial.mtx" << std::endl;
    WriteMatrixMarketFile( jac, "tmp/jac_initial.mtx" );
#endif

    BOOST_REQUIRE(jac.m() == AlgebraicEquationSetClass::nEqnSet);
    BOOST_REQUIRE(jac.n() == AlgebraicEquationSetClass::nSolSet);

    const std::vector<Real> step_vec = {2e-3, 1e-3};
    const std::vector<Real>& rate_range = {1.8, 2.3};
    const Real nonzero_tol = 1.e-15;
    const Real small_tol = 1.e-15;

    const ArrayQ qArrayQscale = {1.0};

#if defined(DISPLAY_FOR_DEBUGGING)
    std::cout << std::endl << "========== Testing jac_block = jac ==========" << std::endl << std::endl;
    const bool verbose = true;
#else
    const bool verbose = false;
#endif
    SystemVectorClass rsdp(AlgEqSet.vectorEqSize());
    SystemVectorClass rsdm(AlgEqSet.vectorEqSize());

    detail_jacobianPingTest::jacobianPingTest::
    ping_block<MatrixQ>(jac, rsdp, rsdm, rsdp, rsdm, q, q, AlgEqSet,
                        step_vec, qArrayQscale, rate_range, verbose, nonzero_tol, small_tol);

  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
