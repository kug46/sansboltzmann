// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandInteroirTrace_HDG_AD_btest
// testing of residual integrands for HDG: Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"

#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementIntegral.h"

#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_UniformGrad> PDEAdvectionDiffusion1D;
typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldLine;
template class IntegrandInteriorTrace_HDG<PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D>>::
               BasisWeighted<Real,TopoD0,Node,TopoD1,Line,ElementXFieldLine>;
template class IntegrandInteriorTrace_HDG<PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D>>::
               BasisWeighted_AUX<Real,TopoD0,Node,TopoD1,Line,ElementXFieldLine>;
template class IntegrandInteriorTrace_HDG<PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D>>::
               FieldWeighted<Real,TopoD0,Node,TopoD1,Line,ElementXFieldLine>;

typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_UniformGrad > PDEAdvectionDiffusion2D;
typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldTri;
template class IntegrandInteriorTrace_HDG<PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D>>::
               BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldTri>;
template class IntegrandInteriorTrace_HDG<PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D>>::
               BasisWeighted_AUX<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldTri>;
template class IntegrandInteriorTrace_HDG<PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D>>::
               FieldWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldTri>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandInteriorTrace_HDG_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_1D_Line_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;

  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef Element<ArrayQ,TopoD0,Node> ElementQFieldTrace;
  typedef Element<DLA::VectorS<1,ArrayQ>,TopoD1,Line> ElementAFieldCell;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD0,Node,TopoD1,Line,ElementParam> BasisWeightedClass;
  typedef IntegrandClass::BasisWeighted_AUX<Real,TopoD0,Node,TopoD1,Line,ElementParam> BasisWeightedAUXClass;
  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_UniformGrad source(1.0/4.0, 3.0/5.0);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemR.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3;

  x1 = 0;
  x2 = 1;
  x3 = 2;

  xfldElemL.DOF(0) = {x1};
  xfldElemL.DOF(1) = {x2};

  xfldElemR.DOF(0) = {x2};
  xfldElemR.DOF(1) = {x3};

  xnode.DOF(0) = {x2};
  xnode.normalSignL() =  1;

  // solution

  order = 1;
  ElementQFieldCell qfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell qfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, qfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, qfldElemR.nDOF() );

  // line solution (left)
  qfldElemL.DOF(0) = 1;
  qfldElemL.DOF(1) = 4;

  // line solution (right)
  qfldElemR.DOF(0) = 7;
  qfldElemR.DOF(1) = 9;

  // auxiliary variable

  ElementAFieldCell afldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementAFieldCell afldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, afldElemL.order() );
  BOOST_CHECK_EQUAL( 2, afldElemL.nDOF() );

  afldElemL.DOF(0) = { 2};
  afldElemL.DOF(1) = { 7};

  afldElemR.DOF(0) = { 9};
  afldElemR.DOF(1) = {-1};

  // interface solution

  ElementQFieldTrace qnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, qnode.order() );
  BOOST_CHECK_EQUAL( 1, qnode.nDOF() );

  qnode.DOF(0) = 8;

  // HDG discretization
  DiscretizationClass disc( pde, Local );

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  BasisWeightedClass fcnPDEL = fcnint.integrand( xnode, qnode, CanonicalTraceToCell(0,0), +1,
                                                 xfldElemL, qfldElemL, afldElemL );

  BasisWeightedClass fcnPDER = fcnint.integrand( xnode, qnode, CanonicalTraceToCell(1,0), -1,
                                                 xfldElemR, qfldElemR, afldElemR );

  BasisWeightedAUXClass fcnAUXL = fcnint.integrand_AUX( xnode, qnode, CanonicalTraceToCell(0,0), +1,
                                                        xfldElemL, qfldElemL );

  BasisWeightedAUXClass fcnAUXR = fcnint.integrand_AUX( xnode, qnode, CanonicalTraceToCell(1,0), -1,
                                                        xfldElemR, qfldElemR );

  BOOST_CHECK_EQUAL( 1, fcnPDEL.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcnPDEL.nDOFLeft() );
  BOOST_CHECK( fcnPDEL.needsEvaluation() == true );

  BOOST_CHECK_EQUAL( 1, fcnPDER.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcnPDER.nDOFLeft() );
  BOOST_CHECK( fcnPDER.needsEvaluation() == true );

  BOOST_CHECK_EQUAL( 1, fcnAUXL.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcnAUXL.nDOFLeft() );
  BOOST_CHECK( fcnAUXL.needsEvaluation() == true );

  BOOST_CHECK_EQUAL( 1, fcnAUXR.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcnAUXR.nDOFLeft() );
  BOOST_CHECK( fcnAUXR.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 2e-12;
  Real sRef;

  Real integrandPDELTrue[2], integrandPDERTrue[2], integrandPDEITrue[1];
  Real integrandAUXxLTrue[2], integrandAUXxRTrue[2];

  BasisWeightedClass::IntegrandCellType integrandPDEL[2], integrandPDER[2];
  BasisWeightedClass::IntegrandTraceType integrandPDEIL[1], integrandPDEIR[1], integrandPDEI[1];
  BasisWeightedAUXClass::IntegrandType integrandAUXL[2], integrandAUXR[2];

  sRef = 0;

  fcnPDEL( sRef, integrandPDEL, 2, integrandPDEIL, 1 );
  fcnPDER( sRef, integrandPDER, 2, integrandPDEIR, 1 );

  fcnAUXL( sRef, integrandAUXL, 2 );
  fcnAUXR( sRef, integrandAUXR, 2 );

  integrandPDEI[0] = integrandPDEIL[0] + integrandPDEIR[0];

  //PDE residual integrands (left): (advective) + (viscous) + (stabilization)
  integrandPDELTrue[0] = ( 0 ) + ( 0 )+ ( 0 );  // Basis function 1
  integrandPDELTrue[1] = ( 22/5. ) + ( -14861/1000. )+ ( -2123/250. );  // Basis function 2

  //PDE residual integrands (right): (advective) + (viscous) + (stabilization)
  integrandPDERTrue[0] = ( -44/5. ) + ( 19107/1000. )+ ( -2123/1000. );  // Basis function 1
  integrandPDERTrue[1] = ( 0 ) + ( 0 )+ ( 0 );  // Basis function 2

  //AUX residual integrands (left):
  integrandAUXxLTrue[0] = ( 0 );  // Basis function 1
  integrandAUXxLTrue[1] = ( -8 );  // Basis function 2

  //AUX residual integrands (right):
  integrandAUXxRTrue[0] = ( 8 );  // Basis function 1
  integrandAUXxRTrue[1] = ( 0 );  // Basis function 2

  //INT residual integrands: (advective) + (viscous) + (stabilization)
  integrandPDEITrue[0] = ( 22/5. ) + ( -2123/500. ) + ( 2123/200. );  // Basis function 1

  SANS_CHECK_CLOSE( integrandPDELTrue[0], integrandPDEL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDELTrue[1], integrandPDEL[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDERTrue[0], integrandPDER[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue[1], integrandPDER[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandAUXxLTrue[0], integrandAUXL[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxLTrue[1], integrandAUXL[1][0], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandAUXxRTrue[0], integrandAUXR[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxRTrue[1], integrandAUXR[1][0], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDEITrue[0], integrandPDEI[0], small_tol, close_tol );


  // test the element integral of the functor

  int quadratureorder = 0;
  int nIntegrandL = qfldElemL.nDOF();
  int nIntegrandR = qfldElemR.nDOF();
  int nIntegrandTrace = qnode.nDOF();

  typedef BasisWeightedClass::IntegrandCellType CellIntegrandType;
  typedef BasisWeightedClass::IntegrandTraceType TraceIntegrandType;
  typedef BasisWeightedAUXClass::IntegrandType AUXIntegrandType;

  GalerkinWeightedIntegral<TopoD0, Node, CellIntegrandType, TraceIntegrandType>
    integralPDEL(quadratureorder, nIntegrandL, nIntegrandTrace);

  GalerkinWeightedIntegral<TopoD0, Node, CellIntegrandType, TraceIntegrandType>
    integralPDER(quadratureorder, nIntegrandR, nIntegrandTrace);

  GalerkinWeightedIntegral<TopoD0, Node, AUXIntegrandType>
    integralAUXL(quadratureorder, nIntegrandL);

  GalerkinWeightedIntegral<TopoD0, Node, AUXIntegrandType>
    integralAUXR(quadratureorder, nIntegrandR);

  CellIntegrandType rsdPDEElemL[2] = {0,0}, rsdPDEElemR[2] = {0,0};
  TraceIntegrandType rsdPDEElemIL[1] = {0}, rsdPDEElemIR[1] = {0}, rsdPDEElemI[1] = {0};
  AUXIntegrandType rsdAUXElemL[2] = {0,0}, rsdAUXElemR[2] = {0,0};

  // cell integration for canonical element
  integralPDEL( fcnPDEL, xnode, rsdPDEElemL, nIntegrandL, rsdPDEElemIL, nIntegrandTrace );
  integralPDER( fcnPDER, xnode, rsdPDEElemR, nIntegrandR, rsdPDEElemIR, nIntegrandTrace );

  // cell integration for canonical element
  integralAUXL( fcnAUXL, xnode, rsdAUXElemL, nIntegrandL );
  integralAUXR( fcnAUXR, xnode, rsdAUXElemR, nIntegrandR );

  rsdPDEElemI[0] = rsdPDEElemIL[0] + rsdPDEElemIR[0];

  Real rsdPDELTrue[2], rsdPDERTrue[2], rsdPDEITrue[2];
  Real rsdAUXxLTrue[2], rsdAUXxRTrue[2];

  //PDE residuals (left): (advective) + (viscous) + (stabilization)
  rsdPDELTrue[0] = ( 0 ) + ( 0 ) + ( 0 );   // Basis function 1
  rsdPDELTrue[1] = ( 22/5. ) + ( -14861/1000. ) + ( -2123/250. );   // Basis function 2

  //PDE residuals (right): (advective) + (viscous) + (stabilization)
  rsdPDERTrue[0] = ( -44/5. ) + ( 19107/1000. ) + ( -2123/1000. );   // Basis function 1
  rsdPDERTrue[1] = ( 0 ) + ( 0 ) + ( 0 );   // Basis function 2

  //AUX residuals (left):
  rsdAUXxLTrue[0] = ( 0 );   // Basis function 1
  rsdAUXxLTrue[1] = ( -8 );   // Basis function 2

  //AUX residuals (right):
  rsdAUXxRTrue[0] = ( 8 );   // Basis function 1
  rsdAUXxRTrue[1] = ( 0 );   // Basis function 2

  //INT residual integrands (left): (advective) + (viscous) + (stabilization)
  rsdPDEITrue[0] = ( 22/5. ) + ( -2123/500. ) + ( 2123/200. );  // Basis function 1

  SANS_CHECK_CLOSE( rsdPDELTrue[0], rsdPDEElemL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDELTrue[1], rsdPDEElemL[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdPDERTrue[0], rsdPDEElemR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue[1], rsdPDEElemR[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdAUXxLTrue[0], rsdAUXElemL[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXxLTrue[1], rsdAUXElemL[1][0], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdAUXxRTrue[0], rsdAUXElemR[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXxRTrue[1], rsdAUXElemR[1][0], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdPDEITrue[0], rsdPDEElemI[0], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_1D_Line_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;

  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef Element<ArrayQ,TopoD0,Node> ElementQFieldTrace;
  typedef Element<DLA::VectorS<1,ArrayQ>,TopoD1,Line> ElementAFieldCell;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD0,Node,TopoD1,Line,ElementParam> FieldWeightedClass;
  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_UniformGrad source(1.0/4.0, 3.0/5.0);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemR.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3;

  x1 = 0;
  x2 = 1;
  x3 = 2;

  xfldElemL.DOF(0) = {x1};
  xfldElemL.DOF(1) = {x2};

  xfldElemR.DOF(0) = {x2};
  xfldElemR.DOF(1) = {x3};

  xnode.DOF(0) = {x2};
  xnode.normalSignL() = 1;

  // solution
  order = 1;
  ElementQFieldCell qfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell qfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, qfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, qfldElemR.nDOF() );

  // line solution (left)
  qfldElemL.DOF(0) = 1;
  qfldElemL.DOF(1) = 4;

  // line solution (right)
  qfldElemR.DOF(0) = 7;
  qfldElemR.DOF(1) = 9;

  // auxiliary variable
  ElementAFieldCell afldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementAFieldCell afldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, afldElemL.order() );
  BOOST_CHECK_EQUAL( 2, afldElemL.nDOF() );

  afldElemL.DOF(0) = { 2};
  afldElemL.DOF(1) = { 7};

  afldElemR.DOF(0) = { 9};
  afldElemR.DOF(1) = {-1};

  // interface solution
  ElementQFieldTrace qnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, qnode.order() );
  BOOST_CHECK_EQUAL( 1, qnode.nDOF() );

  qnode.DOF(0) =  8;

  // solution
  order = 1;
  ElementQFieldCell wfldElemL(order+1, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell wfldElemR(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, wfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 2, wfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, wfldElemR.nDOF() );

  // line solution (left)
  wfldElemL.DOF(0) = 3;
  wfldElemL.DOF(1) = 4;
  wfldElemL.DOF(2) = 5;

  // line solution (right)
  wfldElemR.DOF(0) =  5;
  wfldElemR.DOF(1) =  4;
  wfldElemR.DOF(2) = -1;

  // auxiliary variable
  ElementAFieldCell bfldElemL(order+1, BasisFunctionCategory_Hierarchical);
  ElementAFieldCell bfldElemR(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, bfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, bfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 2, bfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, bfldElemR.nDOF() );

  bfldElemL.DOF(0) = {-5};
  bfldElemL.DOF(1) = { 3};
  bfldElemL.DOF(2) = { 2};

  bfldElemR.DOF(0) = { 3};
  bfldElemR.DOF(1) = { 2};
  bfldElemR.DOF(2) = { 4};

  // interface solution
  ElementQFieldTrace wnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, wnode.order() );
  BOOST_CHECK_EQUAL( 1, wnode.nDOF() );

  wnode.DOF(0) = -2;

  // HDG discretization
  DiscretizationClass disc( pde, Local );

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  FieldWeightedClass fcnL = fcnint.integrand( xnode, qnode, wnode, CanonicalTraceToCell(0,0), +1,
                                              xfldElemL, qfldElemL, afldElemL, wfldElemL, bfldElemL );

  FieldWeightedClass fcnR = fcnint.integrand( xnode, qnode, wnode, CanonicalTraceToCell(1,0), -1,
                                              xfldElemR, qfldElemR, afldElemR, wfldElemR, bfldElemR );

  BOOST_CHECK_EQUAL( 1, fcnL.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcnL.nDOFLeft() );
  BOOST_CHECK( fcnL.needsEvaluation() == true );

  BOOST_CHECK_EQUAL( 1, fcnR.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcnR.nDOFLeft() );
  BOOST_CHECK( fcnR.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 2e-12;
  Real sRef;
  Real integrandPDELTrue, integrandPDERTrue, integrandPDEITrue;
  Real integrandAUXLTrue, integrandAUXRTrue;
  FieldWeightedClass::IntegrandCellType integrandL, integrandR;
  FieldWeightedClass::IntegrandTraceType integrandIL, integrandIR, integrandI;

  // Test at sRef={0}, s={1}
  sRef = {0};
  fcnL( {sRef}, integrandL, integrandIL );
  fcnR( {sRef}, integrandR, integrandIR );

  integrandI = integrandIL + integrandIR;

  //PDE residual integrands (left): (advective) + (viscous) + (stabilization)
  integrandPDELTrue = ( 88./5. ) + ( -14861./250. )+ ( -4246./125. );  // Weight function
  integrandPDERTrue = ( -44 ) + ( 19107./200. )+ ( -2123./200. );  // Weight function

  //AUX residual integrands:
  integrandAUXLTrue = ( -24 );  // Weight function
  integrandAUXRTrue = ( 24 );  // Weight function

  //INT residual integrands (left): (advective) + (viscous) + (stabilization)
  integrandPDEITrue = ( -44/5. ) + ( 2123/250. ) + ( -2123/100. );  // Basis function

  SANS_CHECK_CLOSE( integrandPDELTrue, integrandL.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue, integrandR.PDE, small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandAUXLTrue, integrandL.Au, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXRTrue, integrandR.Au, small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDEITrue, integrandI, small_tol, close_tol );

  // test the element integral of the functor

  int quadratureorder = 0;
  typedef FieldWeightedClass::IntegrandCellType CellIntegrandType;
  typedef FieldWeightedClass::IntegrandTraceType TraceIntegrandType;
  ElementIntegral<TopoD0, Node, CellIntegrandType, TraceIntegrandType> integralL(quadratureorder);
  ElementIntegral<TopoD0, Node, CellIntegrandType, TraceIntegrandType> integralR(quadratureorder);

  CellIntegrandType rsdElemL = 0;
  CellIntegrandType rsdElemR = 0;
  TraceIntegrandType rsdElemTraceL = 0, rsdElemTraceR = 0, rsdElemTrace = 0;

  Real rsdPDELTrue, rsdPDERTrue, rsdPDEITrue;
  Real rsdAUXLTrue, rsdAUXRTrue;

  // cell integration for canonical element
  integralL( fcnL, xnode, rsdElemL, rsdElemTraceL );
  integralR( fcnR, xnode, rsdElemR, rsdElemTraceR );

  rsdElemTrace = rsdElemTraceL + rsdElemTraceR;

  //PDE residuals (left): (advective) + (viscous) + (stabilization)
  rsdPDELTrue = ( 17.6 ) + ( -14861./250. ) + ( -4246./125. );   // Weight function
  rsdPDERTrue = ( -44. ) + ( 19107./200. ) + ( -2123./200. );   // Weight function

  //AUX residuals (left): (Auxiliary)
  rsdAUXLTrue = ( -24 );   // Weight function
  rsdAUXRTrue = ( 24 );   // Weight function

  //INT residual integrands (left): (advective) + (viscous) + (stabilization)
  rsdPDEITrue = ( -44/5. ) + ( 2123/250. ) + ( -2123/100. );  // Weight function

  SANS_CHECK_CLOSE( rsdPDELTrue, rsdElemL.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue, rsdElemR.PDE, small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdAUXLTrue, rsdElemL.Au, small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXRTrue, rsdElemR.Au, small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdPDEITrue, rsdElemTrace, small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_1D_Line_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;

  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef Element<ArrayQ,TopoD0,Node> ElementQFieldTrace;
  typedef Element<DLA::VectorS<1,ArrayQ>,TopoD1,Line> ElementAFieldCell;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD0,Node,TopoD1,Line,ElementParam> BasisWeightedClass;
  typedef IntegrandClass::BasisWeighted_AUX<Real,TopoD0,Node,TopoD1,Line,ElementParam> BasisWeightedAUXClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD0,Node,TopoD1,Line,ElementParam> FieldWeightedClass;
  typedef DiscretizationHDG<PDEClass> DiscretizationClass;


  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_UniformGrad source(1.0/4.0, 3.0/5.0);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemR.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent line grid
  Real x1, x2, x3;

  x1 = 0;
  x2 = 1;
  x3 = 2;

  xfldElemL.DOF(0) = x1;
  xfldElemL.DOF(1) = x2;

  xfldElemR.DOF(0) = x2;
  xfldElemR.DOF(1) = x3;

  xnode.DOF(0) = x2;
  xnode.normalSignL() = 1;

  for (int qorder = 2; qorder< 4; qorder++)
  {
    // solution
    ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell qfldElemR(qorder, BasisFunctionCategory_Hierarchical);
    ElementAFieldCell afldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementAFieldCell afldElemR(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElemR(qorder, BasisFunctionCategory_Hierarchical);
    ElementAFieldCell bfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementAFieldCell bfldElemR(qorder, BasisFunctionCategory_Hierarchical);

    ElementQFieldTrace qnode(0, BasisFunctionCategory_Legendre);
    ElementQFieldTrace wnode(0, BasisFunctionCategory_Legendre);

    BOOST_CHECK_EQUAL( qnode.nDOF(), wnode.nDOF() );
    for (int dof = 0; dof < qnode.nDOF(); dof++)
    {
      qnode.DOF(dof) = (dof+1)*pow(-1,dof);
      wnode.DOF(dof) = 0;
    }

    BOOST_CHECK_EQUAL( qorder,   qfldElemL.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   qfldElemR.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElemR.nDOF() );

    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), qfldElemR.nDOF() );

    BOOST_CHECK_EQUAL( qorder,   afldElemL.order() );
    BOOST_CHECK_EQUAL( qorder+1, afldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   afldElemR.order() );
    BOOST_CHECK_EQUAL( qorder+1, afldElemR.nDOF() );

    BOOST_CHECK_EQUAL( afldElemL.nDOF(), afldElemR.nDOF() );

    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), afldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), wfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), bfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), afldElemR.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), wfldElemR.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), bfldElemR.nDOF() );
    // line solution
    for ( int dof = 0; dof < qfldElemL.nDOF(); dof ++ )
    {
      qfldElemL.DOF(dof) = (dof+1)*pow(-1,dof);
      qfldElemR.DOF(dof) = (dof+1)*pow(-1,dof);
      wfldElemL.DOF(dof) = 0;
      wfldElemR.DOF(dof) = 0;

      afldElemL.DOF(dof) = (dof+2)*pow(-1,dof+1);
      afldElemR.DOF(dof) = (dof+2)*pow(-1,dof+1);
      bfldElemL.DOF(dof) = 0;
      bfldElemR.DOF(dof) = 0;
    }

    // HDG discretization
    DiscretizationClass disc( pde, Local );

    // integrand
    IntegrandClass fcnint( pde, disc, {0} );

    // integrand functor
    BasisWeightedClass fcnB_PDEL = fcnint.integrand( xnode, qnode,
                                                     CanonicalTraceToCell(0,0), +1,
                                                     xfldElemL,
                                                     qfldElemL, afldElemL );

    BasisWeightedClass fcnB_PDER = fcnint.integrand( xnode, qnode,
                                                     CanonicalTraceToCell(1,0), -1,
                                                     xfldElemR,
                                                     qfldElemR, afldElemR );

    BasisWeightedAUXClass fcnB_AUXL = fcnint.integrand_AUX( xnode, qnode,
                                                            CanonicalTraceToCell(0,0), +1,
                                                            xfldElemL,
                                                            qfldElemL );

    BasisWeightedAUXClass fcnB_AUXR = fcnint.integrand_AUX( xnode, qnode,
                                                            CanonicalTraceToCell(1,0), -1,
                                                            xfldElemR,
                                                            qfldElemR );

    // integrand functor
    FieldWeightedClass fcnWL = fcnint.integrand( xnode, qnode, wnode,
                                                 CanonicalTraceToCell(0,0), +1,
                                                 xfldElemL,
                                                 qfldElemL, afldElemL,
                                                 wfldElemL, bfldElemL );

    FieldWeightedClass fcnWR = fcnint.integrand( xnode, qnode, wnode,
                                                 CanonicalTraceToCell(1,0), -1,
                                                 xfldElemR,
                                                 qfldElemR, afldElemR,
                                                 wfldElemR, bfldElemR );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;
    const int nIntegrandL = qfldElemL.nDOF();
    const int nIntegrandR = qfldElemR.nDOF();
    const int nIntegrandTrace = qnode.nDOF();

    int quadratureorder = 0;
    GalerkinWeightedIntegral<TopoD0, Node, BasisWeightedClass::IntegrandCellType,
                                           BasisWeightedClass::IntegrandTraceType>
                                           integralB_PDEL(quadratureorder, nIntegrandL, nIntegrandTrace);

    GalerkinWeightedIntegral<TopoD0, Node, BasisWeightedClass::IntegrandCellType,
                                           BasisWeightedClass::IntegrandTraceType>
                                           integralB_PDER(quadratureorder, nIntegrandR, nIntegrandTrace);

    GalerkinWeightedIntegral<TopoD0, Node, BasisWeightedAUXClass::IntegrandType>
                                           integralB_AUXL(quadratureorder, nIntegrandL);

    GalerkinWeightedIntegral<TopoD0, Node, BasisWeightedAUXClass::IntegrandType>
                                           integralB_AUXR(quadratureorder, nIntegrandR);

    ElementIntegral<TopoD0, Node, FieldWeightedClass::IntegrandCellType,
                                  FieldWeightedClass::IntegrandTraceType> integralWL(quadratureorder);

    ElementIntegral<TopoD0, Node, FieldWeightedClass::IntegrandCellType,
                                  FieldWeightedClass::IntegrandTraceType> integralWR(quadratureorder);

    std::vector<BasisWeightedClass::IntegrandCellType> rsdPDEElemBL(nIntegrandL, 0);
    std::vector<BasisWeightedClass::IntegrandCellType> rsdPDEElemBR(nIntegrandR, 0);
    std::vector<BasisWeightedAUXClass::IntegrandType> rsdAUXElemBL(nIntegrandL, 0);
    std::vector<BasisWeightedAUXClass::IntegrandType> rsdAUXElemBR(nIntegrandR, 0);
    std::vector<BasisWeightedClass::IntegrandTraceType> rsdPDEElemBIL(nIntegrandTrace, 0), rsdPDEElemBIR(nIntegrandTrace, 0);
    FieldWeightedClass::IntegrandCellType rsdElemWL = 0;
    FieldWeightedClass::IntegrandCellType rsdElemWR = 0;
    FieldWeightedClass::IntegrandTraceType rsdElemWIL = 0, rsdElemWIR = 0;

    // cell integration for canonical element
    integralB_PDEL( fcnB_PDEL, xnode, rsdPDEElemBL.data(), nIntegrandL, rsdPDEElemBIL.data(), nIntegrandTrace );
    integralB_PDER( fcnB_PDER, xnode, rsdPDEElemBR.data(), nIntegrandR, rsdPDEElemBIR.data(), nIntegrandTrace );

    integralB_AUXL( fcnB_AUXL, xnode, rsdAUXElemBL.data(), nIntegrandL );
    integralB_AUXR( fcnB_AUXR, xnode, rsdAUXElemBR.data(), nIntegrandR );

    for (int i = 0; i < wfldElemL.nDOF(); i++ )
    {
      // set just one of the weights to one
      wfldElemL.DOF(i) = 1; bfldElemL.DOF(i) = 1;
      wfldElemR.DOF(i) = 1; bfldElemR.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemWL = 0; rsdElemWR = 0;
      rsdElemWIL = 0; rsdElemWIR = 0;
      integralWL(fcnWL, xnode, rsdElemWL, rsdElemWIL );
      integralWR(fcnWR, xnode, rsdElemWR, rsdElemWIR );

      // test the two integrands are the same
      SANS_CHECK_CLOSE ( rsdElemWL.PDE, rsdPDEElemBL[i], small_tol, close_tol );
      SANS_CHECK_CLOSE ( rsdElemWR.PDE, rsdPDEElemBR[i], small_tol, close_tol );

      Real tmpL = 0, tmpR = 0;
      for (int d = 0; d < PhysD1::D; d ++)
      {
        tmpL += rsdAUXElemBL[i][d];
        tmpR += rsdAUXElemBR[i][d];
      }
      SANS_CHECK_CLOSE( rsdElemWL.Au, tmpL, small_tol, close_tol );
      SANS_CHECK_CLOSE( rsdElemWR.Au, tmpR, small_tol, close_tol );

      // reset to 0
      wfldElemL.DOF(i) = 0; bfldElemL.DOF(i) = 0;
      wfldElemR.DOF(i) = 0; bfldElemR.DOF(i) = 0;
    }

    for (int i = 0; i < wnode.nDOF(); i++ )
    {
      wnode.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemWL = 0; rsdElemWR = 0;
      rsdElemWIL = 0; rsdElemWIR = 0;
      integralWL(fcnWL, xnode, rsdElemWR, rsdElemWIL );
      integralWR(fcnWR, xnode, rsdElemWR, rsdElemWIR );

      SANS_CHECK_CLOSE( rsdElemWIL, rsdPDEElemBIL[i], small_tol, close_tol);
      SANS_CHECK_CLOSE( rsdElemWIR, rsdPDEElemBIR[i], small_tol, close_tol);
      wnode.DOF(i) = 0;
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_2D_Triangle_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;

  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldTrace;
  typedef Element<DLA::VectorS<2,ArrayQ>,TopoD2,Triangle> ElementAFieldCell;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementParam> BasisWeightedClass;
  typedef IntegrandClass::BasisWeighted_AUX<Real,TopoD1,Line,TopoD2,Triangle,ElementParam> BasisWeightedAUXClass;

  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_UniformGrad source(0.25, 0.6, -1.5);

  PDEClass pde( adv, visc, source );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemR.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;
  x4 = 1;  y4 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xfldElemR.DOF(0) = {x4, y4};
  xfldElemR.DOF(1) = {x3, y3};
  xfldElemR.DOF(2) = {x2, y2};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution

  order = 1;
  ElementQFieldCell qfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell qfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemR.nDOF() );

  // triangle solution (left)
  qfldElemL.DOF(0) = 1;
  qfldElemL.DOF(1) = 3;
  qfldElemL.DOF(2) = 4;

  // triangle solution (right)
  qfldElemR.DOF(0) = 7;
  qfldElemR.DOF(1) = 2;
  qfldElemR.DOF(2) = 9;

  // auxiliary variable

  ElementAFieldCell afldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementAFieldCell afldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, afldElemL.order() );
  BOOST_CHECK_EQUAL( 3, afldElemL.nDOF() );

  afldElemL.DOF(0) = { 2, -3};
  afldElemL.DOF(1) = { 7,  8};
  afldElemL.DOF(2) = {-1, -5};

  afldElemR.DOF(0) = { 9,  6};
  afldElemR.DOF(1) = {-1,  3};
  afldElemR.DOF(2) = { 2, -3};

  // interface solution

  ElementQFieldTrace qedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qedge.order() );
  BOOST_CHECK_EQUAL( 2, qedge.nDOF() );

  qedge.DOF(0) =  8;
  qedge.DOF(1) = -1;

  // HDG discretization
  DiscretizationClass disc( pde, Local );

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  BasisWeightedClass fcnPDEL = fcnint.integrand( xedge, qedge, CanonicalTraceToCell(0,1), +1,
                                                        xfldElemL, qfldElemL, afldElemL );

  BasisWeightedClass fcnPDER = fcnint.integrand( xedge, qedge, CanonicalTraceToCell(0,-1), -1,
                                                        xfldElemR, qfldElemR, afldElemR );

  BasisWeightedAUXClass fcnAUXL = fcnint.integrand_AUX( xedge, qedge, CanonicalTraceToCell(0,1), +1,
                                                        xfldElemL, qfldElemL );

  BasisWeightedAUXClass fcnAUXR = fcnint.integrand_AUX( xedge, qedge, CanonicalTraceToCell(0,-1), -1,
                                                        xfldElemR, qfldElemR );

  BOOST_CHECK_EQUAL( 1, fcnPDEL.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcnPDEL.nDOFLeft() );
  BOOST_CHECK( fcnPDEL.needsEvaluation() == true );

  BOOST_CHECK_EQUAL( 1, fcnPDER.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcnPDER.nDOFLeft() );
  BOOST_CHECK( fcnPDER.needsEvaluation() == true );

  BOOST_CHECK_EQUAL( 1, fcnAUXL.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcnAUXL.nDOFLeft() );
  BOOST_CHECK( fcnAUXL.needsEvaluation() == true );

  BOOST_CHECK_EQUAL( 1, fcnAUXR.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcnAUXR.nDOFLeft() );
  BOOST_CHECK( fcnAUXR.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  Real sRef;

  Real integrandPDELTrue[3], integrandPDERTrue[3], integrandPDEITrue[2];
  Real integrandAUXxLTrue[3], integrandAUXyLTrue[3];
  Real integrandAUXxRTrue[3], integrandAUXyRTrue[3];

  BasisWeightedClass::IntegrandCellType integrandPDEL[3], integrandPDER[3];
  BasisWeightedClass::IntegrandTraceType integrandPDEIL[2], integrandPDEIR[2], integrandPDEI[2];
  BasisWeightedAUXClass::IntegrandType integrandAUXL[3], integrandAUXR[3];

  // Test at sRef={0}, {s,t}={1, 0}
  sRef = 0;
  fcnPDEL( sRef, integrandPDEL, 3, integrandPDEIL, 2 );
  fcnPDER( sRef, integrandPDER, 3, integrandPDEIR, 2 );

  fcnAUXL( sRef, integrandAUXL, 3 );
  fcnAUXR( sRef, integrandAUXR, 3 );

  integrandPDEI[0] = integrandPDEIL[0] + integrandPDEIR[0];
  integrandPDEI[1] = integrandPDEIL[1] + integrandPDEIR[1];

  //PDE residual integrands (left): (advective) + (viscous) + (stabilization)
  integrandPDELTrue[0] = ( 0 ) + ( 0 ) + ( 0 ) ;  // Basis function 1
  integrandPDELTrue[1] = ( (39/10.)*(pow(2,-1/2.)) ) + ( (-2054/125.)*(sqrt(2)) ) + ( (-559/25.)*(sqrt(2)) ) ;  // Basis function 2
  integrandPDELTrue[2] = ( 0 ) + ( 0 ) + ( 0 ) ;  // Basis function 3

  //PDE residual integrands (right): (advective) + (viscous) + (stabilization)
  integrandPDERTrue[0] = ( 0 ) + ( 0 ) + ( 0 )  ;  // Basis function 1
  integrandPDERTrue[1] = ( 0 ) + ( 0 ) + ( 0 )  ;  // Basis function 2
  integrandPDERTrue[2] = ( (-26/5.)*(sqrt(2)) ) + ( (143/125.)*(pow(2,-1/2.)) ) + ( (559/125.)*(sqrt(2)) )  ;  // Basis function 3

  SANS_CHECK_CLOSE( integrandPDELTrue[0], integrandPDEL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDELTrue[1], integrandPDEL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDELTrue[2], integrandPDEL[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDERTrue[0], integrandPDER[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue[1], integrandPDER[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue[2], integrandPDER[2], small_tol, close_tol );

  //AUX integrands (left):
  integrandAUXxLTrue[0] = ( 0 );  // Basis function 1
  integrandAUXxLTrue[1] = ( (-4)*(sqrt(2)) );  // Basis function 2
  integrandAUXxLTrue[2] = ( 0 );  // Basis function 3
  integrandAUXyLTrue[0] = ( 0 );  // Basis function 1
  integrandAUXyLTrue[1] = ( (-4)*(sqrt(2)) );  // Basis function 2
  integrandAUXyLTrue[2] = ( 0 );  // Basis function 3

  //AUX integrands (right):
  integrandAUXxRTrue[0] = ( 0 );  // Basis function 1
  integrandAUXxRTrue[1] = ( 0 );  // Basis function 2
  integrandAUXxRTrue[2] = ( (4)*(sqrt(2)) );  // Basis function 3
  integrandAUXyRTrue[0] = ( 0 );  // Basis function 1
  integrandAUXyRTrue[1] = ( 0 );  // Basis function 2
  integrandAUXyRTrue[2] = ( (4)*(sqrt(2)) );  // Basis function 3

  SANS_CHECK_CLOSE( integrandAUXxLTrue[0], integrandAUXL[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxLTrue[1], integrandAUXL[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxLTrue[2], integrandAUXL[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyLTrue[0], integrandAUXL[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyLTrue[1], integrandAUXL[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyLTrue[2], integrandAUXL[2][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandAUXxRTrue[0], integrandAUXR[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxRTrue[1], integrandAUXR[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxRTrue[2], integrandAUXR[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyRTrue[0], integrandAUXR[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyRTrue[1], integrandAUXR[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyRTrue[2], integrandAUXR[2][1], small_tol, close_tol );

  //Interface residual integrands: (advective) + (viscous) + (stabilization)
  integrandPDEITrue[0] = ( (13/2.)*(pow(2,-1/2.)) ) + ( (793/25.)*(pow(2,-1/2.)) ) + ( (2236/125.)*(sqrt(2)) );  // Basis function 1
  integrandPDEITrue[1] = ( 0 ) + ( 0 ) + ( 0 );  // Basis function 2

  SANS_CHECK_CLOSE( integrandPDEITrue[0], integrandPDEI[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDEITrue[1], integrandPDEI[1], small_tol, close_tol );


  // Test at sRef={1}, {s,t}={0, 1}
  sRef = 1;
  fcnPDEL( sRef, integrandPDEL, 3, integrandPDEIL, 2 );
  fcnPDER( sRef, integrandPDER, 3, integrandPDEIR, 2 );

  fcnAUXL( sRef, integrandAUXL, 3 );
  fcnAUXR( sRef, integrandAUXR, 3 );

  integrandPDEI[0] = integrandPDEIL[0] + integrandPDEIR[0];
  integrandPDEI[1] = integrandPDEIL[1] + integrandPDEIR[1];

  //PDE residual integrands (left): (advective) + (viscous) + (stabilization)
  integrandPDELTrue[0] = ( 0 ) + ( 0 ) + ( 0 ) ;  // Basis function 1
  integrandPDELTrue[1] = ( 0 ) + ( 0 ) + ( 0 ) ;  // Basis function 2
  integrandPDELTrue[2] = ( (13/5.)*(sqrt(2)) ) + ( (1339/125.)*(pow(2,-1/2.)) ) + ( (559/25.)*(sqrt(2)) ) ;  // Basis function 3

  //PDE residual integrands (right): (advective) + (viscous) + (stabilization)
  integrandPDERTrue[0] = ( 0 ) + ( 0 ) + ( 0 )  ;  // Basis function 1
  integrandPDERTrue[1] = ( (13/10.)*(pow(2,-1/2.)) ) + ( (221/125.)*(pow(2,-1/2.)) ) + ( (1677/125.)*(sqrt(2)) )  ;  // Basis function 2
  integrandPDERTrue[2] = ( 0 ) + ( 0 ) + ( 0 )  ;  // Basis function 3

  SANS_CHECK_CLOSE( integrandPDELTrue[0], integrandPDEL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDELTrue[1], integrandPDEL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDELTrue[2], integrandPDEL[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDERTrue[0], integrandPDER[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue[1], integrandPDER[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue[2], integrandPDER[2], small_tol, close_tol );

  //AUX integrands (left):
  integrandAUXxLTrue[0] = ( 0 );  // Basis function 1
  integrandAUXxLTrue[1] = ( 0 );  // Basis function 2
  integrandAUXxLTrue[2] = ( pow(2,-1/2.) );  // Basis function 3
  integrandAUXyLTrue[0] = ( 0 );  // Basis function 1
  integrandAUXyLTrue[1] = ( 0 );  // Basis function 2
  integrandAUXyLTrue[2] = ( pow(2,-1/2.) );  // Basis function 3

  //AUX integrands (right):
  integrandAUXxRTrue[0] = ( 0 );  // Basis function 1
  integrandAUXxRTrue[1] = ( (-1)*(pow(2,-1/2.)) );  // Basis function 2
  integrandAUXxRTrue[2] = ( 0 );  // Basis function 3
  integrandAUXyRTrue[0] = ( 0 );  // Basis function 1
  integrandAUXyRTrue[1] = ( (-1)*(pow(2,-1/2.)) );  // Basis function 2
  integrandAUXyRTrue[2] = ( 0 );  // Basis function 3

  SANS_CHECK_CLOSE( integrandAUXxLTrue[0], integrandAUXL[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxLTrue[1], integrandAUXL[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxLTrue[2], integrandAUXL[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyLTrue[0], integrandAUXL[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyLTrue[1], integrandAUXL[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyLTrue[2], integrandAUXL[2][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandAUXxRTrue[0], integrandAUXR[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxRTrue[1], integrandAUXR[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxRTrue[2], integrandAUXR[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyRTrue[0], integrandAUXR[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyRTrue[1], integrandAUXR[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyRTrue[2], integrandAUXR[2][1], small_tol, close_tol );

  //Interface residual integrands: (advective) + (viscous) + (stabilization)
  integrandPDEITrue[0] = ( 0 ) + ( 0 ) + ( 0 );  // Basis function 1
  integrandPDEITrue[1] = ( (-13/2.)*(pow(2,-1/2.)) ) + ( (-156/25.)*(sqrt(2)) ) + ( (-4472/125.)*(sqrt(2)) );  // Basis function 2

  SANS_CHECK_CLOSE( integrandPDEITrue[0], integrandPDEI[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDEITrue[1], integrandPDEI[1], small_tol, close_tol );


  // Test at sRef={1/2}, {s,t}={1/2, 1/2}
  sRef = 1/2.;
  fcnPDEL( sRef, integrandPDEL, 3, integrandPDEIL, 2 );
  fcnPDER( sRef, integrandPDER, 3, integrandPDEIR, 2 );

  fcnAUXL( sRef, integrandAUXL, 3 );
  fcnAUXR( sRef, integrandAUXR, 3 );

  integrandPDEI[0] = integrandPDEIL[0] + integrandPDEIR[0];
  integrandPDEI[1] = integrandPDEIL[1] + integrandPDEIR[1];

  //PDE residual integrands (left): (advective) + (viscous) + (stabilization)
  integrandPDELTrue[0] = ( 0 ) + ( 0 ) + ( 0 ) ;  // Basis function 1
  integrandPDELTrue[1] = ( (91/40.)*(pow(2,-1/2.)) ) + ( (-2769/500.)*(pow(2,-1/2.)) ) + ( 0 ) ;  // Basis function 2
  integrandPDELTrue[2] = ( (91/40.)*(pow(2,-1/2.)) ) + ( (-2769/500.)*(pow(2,-1/2.)) ) + ( 0 ) ;  // Basis function 3

  //PDE residual integrands (right): (advective) + (viscous) + (stabilization)
  integrandPDERTrue[0] = ( 0 ) + ( 0 ) + ( 0 )  ;  // Basis function 1
  integrandPDERTrue[1] = ( (-91/40.)*(pow(2,-1/2.)) ) + ( (91/125.)*(pow(2,-1/2.)) ) + ( (559/125.)*(sqrt(2)) )  ;  // Basis function 2
  integrandPDERTrue[2] = ( (-91/40.)*(pow(2,-1/2.)) ) + ( (91/125.)*(pow(2,-1/2.)) ) + ( (559/125.)*(sqrt(2)) )  ;  // Basis function 3

  SANS_CHECK_CLOSE( integrandPDELTrue[0], integrandPDEL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDELTrue[1], integrandPDEL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDELTrue[2], integrandPDEL[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDERTrue[0], integrandPDER[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue[1], integrandPDER[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue[2], integrandPDER[2], small_tol, close_tol );

  //AUX integrands (left):
  integrandAUXxLTrue[0] = ( 0 );  // Basis function 1
  integrandAUXxLTrue[1] = ( (-7/4.)*(pow(2,-1/2.)) );  // Basis function 2
  integrandAUXxLTrue[2] = ( (-7/4.)*(pow(2,-1/2.)) );  // Basis function 3
  integrandAUXyLTrue[0] = ( 0 );  // Basis function 1
  integrandAUXyLTrue[1] = ( (-7/4.)*(pow(2,-1/2.)) );  // Basis function 2
  integrandAUXyLTrue[2] = ( (-7/4.)*(pow(2,-1/2.)) );  // Basis function 3

  //AUX integrands (right):
  integrandAUXxRTrue[0] = ( 0 );  // Basis function 1
  integrandAUXxRTrue[1] = ( (7/4.)*(pow(2,-1/2.)) );  // Basis function 2
  integrandAUXxRTrue[2] = ( (7/4.)*(pow(2,-1/2.)) );  // Basis function 3
  integrandAUXyRTrue[0] = ( 0 );  // Basis function 1
  integrandAUXyRTrue[1] = ( (7/4.)*(pow(2,-1/2.)) );  // Basis function 2
  integrandAUXyRTrue[2] = ( (7/4.)*(pow(2,-1/2.)) );  // Basis function 3

  SANS_CHECK_CLOSE( integrandAUXxLTrue[0], integrandAUXL[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxLTrue[1], integrandAUXL[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxLTrue[2], integrandAUXL[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyLTrue[0], integrandAUXL[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyLTrue[1], integrandAUXL[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyLTrue[2], integrandAUXL[2][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandAUXxRTrue[0], integrandAUXR[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxRTrue[1], integrandAUXR[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXxRTrue[2], integrandAUXR[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyRTrue[0], integrandAUXR[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyRTrue[1], integrandAUXR[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXyRTrue[2], integrandAUXR[2][1], small_tol, close_tol );

  //Interface residual integrands: (advective) + (viscous) + (stabilization)
  integrandPDEITrue[0] = ( 0 ) + ( (481/100.)*(pow(2,-1/2.)) ) + ( (-559/125.)*(sqrt(2)) );  // Basis function 1
  integrandPDEITrue[1] = ( 0 ) + ( (481/100.)*(pow(2,-1/2.)) ) + ( (-559/125.)*(sqrt(2)) );  // Basis function 2

  SANS_CHECK_CLOSE( integrandPDEITrue[0], integrandPDEI[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDEITrue[1], integrandPDEI[1], small_tol, close_tol );

  // test the element integral of the functor

  int quadratureorder = 2;
  int nIntegrandL = qfldElemL.nDOF();
  int nIntegrandR = qfldElemR.nDOF();
  int nIntegrandTrace = qedge.nDOF();
  typedef BasisWeightedClass::IntegrandCellType CellIntegrandType;
  typedef BasisWeightedClass::IntegrandTraceType TraceIntegrandType;
  typedef BasisWeightedAUXClass::IntegrandType AUXIntegrandType;

  GalerkinWeightedIntegral<TopoD1, Line, CellIntegrandType, TraceIntegrandType>
    integralPDEL(quadratureorder, nIntegrandL, nIntegrandTrace);

  GalerkinWeightedIntegral<TopoD1, Line, CellIntegrandType, TraceIntegrandType>
    integralPDER(quadratureorder, nIntegrandR, nIntegrandTrace);

  GalerkinWeightedIntegral<TopoD1, Line, AUXIntegrandType> integralAUXL(quadratureorder, nIntegrandL);
  GalerkinWeightedIntegral<TopoD1, Line, AUXIntegrandType> integralAUXR(quadratureorder, nIntegrandR);


  CellIntegrandType rsdPDEElemL[3] = {0,0,0};
  CellIntegrandType rsdPDEElemR[3] = {0,0,0};
  TraceIntegrandType rsdPDEElemIL[2] = {0,0}, rsdPDEElemIR[2] = {0,0}, rsdPDEElemI[2] = {0,0};
  AUXIntegrandType rsdAUXElemL[3] = {0,0,0};
  AUXIntegrandType rsdAUXElemR[3] = {0,0,0};

  // cell integration for canonical element
  integralPDEL( fcnPDEL, xedge, rsdPDEElemL, nIntegrandL, rsdPDEElemIL, nIntegrandTrace );
  integralPDER( fcnPDER, xedge, rsdPDEElemR, nIntegrandR, rsdPDEElemIR, nIntegrandTrace );

  integralAUXL( fcnAUXL, xedge, rsdAUXElemL, nIntegrandL );
  integralAUXR( fcnAUXR, xedge, rsdAUXElemR, nIntegrandR );

  rsdPDEElemI[0] = rsdPDEElemIL[0] + rsdPDEElemIR[0];
  rsdPDEElemI[1] = rsdPDEElemIL[1] + rsdPDEElemIR[1];

  Real rsdPDELTrue[3], rsdPDERTrue[3], rsdPDEITrue[2];
  Real rsdAUXxLTrue[3], rsdAUXyLTrue[3];
  Real rsdAUXxRTrue[3], rsdAUXyRTrue[3];

  //PDE residuals (left): (advective) + (viscous) + (stabilization)
  rsdPDELTrue[0] = ( 0 ) + ( 0 ) + ( 0 )  ;   // Basis function 1
  rsdPDELTrue[1] = ( 13/6. ) + ( -6877/750. ) + ( -559/75. )  ;   // Basis function 2
  rsdPDELTrue[2] = ( 143/60. ) + ( -143/75. ) + ( 559/75. )  ;   // Basis function 3

  //PDE residuals (right): (advective) + (viscous) + (lifting-operator))
  rsdPDERTrue[0] = ( 0 ) + ( 0 )+ ( 0 ) ;   // Basis function 1
  rsdPDERTrue[1] = ( -13/10. ) + ( 39/50. )+ ( 3913/375. ) ;   // Basis function 2
  rsdPDERTrue[2] = ( -13/4. ) + ( 169/250. )+ ( 559/75. ) ;   // Basis function 3

  SANS_CHECK_CLOSE( rsdPDELTrue[0], rsdPDEElemL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDELTrue[1], rsdPDEElemL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDELTrue[2], rsdPDEElemL[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdPDERTrue[0], rsdPDEElemR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue[1], rsdPDEElemR[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue[2], rsdPDEElemR[2], small_tol, close_tol );

  //AUX residuals (left):
  rsdAUXxLTrue[0] = ( 0 );   // Basis function 1
  rsdAUXxLTrue[1] = ( -5/2. );   // Basis function 2
  rsdAUXxLTrue[2] = ( -1 );   // Basis function 3
  rsdAUXyLTrue[0] = ( 0 );   // Basis function 1
  rsdAUXyLTrue[1] = ( -5/2. );   // Basis function 2
  rsdAUXyLTrue[2] = ( -1 );   // Basis function 3

  //AUX residuals (right):
  rsdAUXxRTrue[0] = ( 0 );   // Basis function 1
  rsdAUXxRTrue[1] = ( 1 );   // Basis function 2
  rsdAUXxRTrue[2] = ( 5/2. );   // Basis function 3
  rsdAUXyRTrue[0] = ( 0 );   // Basis function 1
  rsdAUXyRTrue[1] = ( 1 );   // Basis function 2
  rsdAUXyRTrue[2] = ( 5/2. );   // Basis function 3

  SANS_CHECK_CLOSE( rsdAUXxLTrue[0], rsdAUXElemL[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXxLTrue[1], rsdAUXElemL[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXxLTrue[2], rsdAUXElemL[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXyLTrue[0], rsdAUXElemL[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXyLTrue[1], rsdAUXElemL[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXyLTrue[2], rsdAUXElemL[2][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdAUXxRTrue[0], rsdAUXElemR[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXxRTrue[1], rsdAUXElemR[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXxRTrue[2], rsdAUXElemR[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXyRTrue[0], rsdAUXElemR[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXyRTrue[1], rsdAUXElemR[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXyRTrue[2], rsdAUXElemR[2][1], small_tol, close_tol );

  //Interface residuals: (advective) + (viscous) + (stabilization)
  rsdPDEITrue[0] = ( 13/12. )+( 637/75. ) + ( 0 );   // Basis function 1
  rsdPDEITrue[1] = ( -13/12. )+( 169/150. ) + ( -2236/125. );   // Basis function 2

  SANS_CHECK_CLOSE( rsdPDEITrue[0], rsdPDEElemI[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDEITrue[1], rsdPDEElemI[1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_2D_Triangle_Triangle_Jacobian_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> MatrixQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;

  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldTrace;
  typedef Element<DLA::VectorS<2,ArrayQ>,TopoD2,Triangle> ElementAFieldCell;

  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementParam> BasisWeightedClass;

  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.478;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real a = 0.4, b = 0.7, c = -0.2;
  Source2D_UniformGrad source(a, b, c);

  PDEClass pde( adv, visc, source );

  // grid
  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;
  x4 = 1;  y4 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xfldElemR.DOF(0) = {x4, y4};
  xfldElemR.DOF(1) = {x3, y3};
  xfldElemR.DOF(2) = {x2, y2};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // HDG discretization
  DiscretizationClass disc( pde, Local );

  // integrand functor
  IntegrandClass fcnint( pde, disc, {0} );

  for (int qorder = 1; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    // solution
    ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);

    // auxiliary variable
    ElementAFieldCell afldElemL(qorder, BasisFunctionCategory_Hierarchical);

    // interface solution
    ElementQFieldTrace qedge(qorder, BasisFunctionCategory_Hierarchical);

    // line solution
    for (int dof = 0; dof < qfldElemL.nDOF();dof++)
    {
      qfldElemL.DOF(dof) = (dof+1)*pow(-1,dof);
      afldElemL.DOF(dof) = { (dof+2)*pow(-1,dof+1), (dof+3)*pow(-1,dof+1) };
    }

    for (int dof = 0; dof < qedge.nDOF();dof++)
      qedge.DOF(dof) = (dof+3)*pow(-1,dof);

    BasisWeightedClass fcnPDEL = fcnint.integrand( xedge, qedge, CanonicalTraceToCell(0,1), +1,
                                                          xfldElemL, qfldElemL, afldElemL );

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-11;

    RefCoordTraceType sRefTrace = Line::centerRef;

    const int nDOFL = qfldElemL.nDOF();
    const int nDOFI = qedge.nDOF();

    DLA::VectorD<ArrayQ> integrand0L(nDOFL), integrand1L(nDOFL);
    DLA::VectorD<ArrayQ> integrand0I(nDOFI), integrand1I(nDOFI);

    JacobianElemInteriorTraceSize sizeL(nDOFL, nDOFL, nDOFI);
    JacobianElemInteriorTraceSize sizeI(nDOFI, nDOFL, nDOFI);

    JacobianElemInteriorTrace_HDG<PhysD2,MatrixQ> mtxElemLTrue(sizeL);
    JacobianElemInteriorTrace_HDG<PhysD2,MatrixQ> mtxElemITrue(sizeI);

    JacobianElemInteriorTrace_HDG<PhysD2,MatrixQ> mtxElemL(sizeL);
    JacobianElemInteriorTrace_HDG<PhysD2,MatrixQ> mtxElemI(sizeI);

    // compute jacobians via finite differenec (exact for linear PDE)
    fcnPDEL(sRefTrace, &integrand0L[0], integrand0L.m(), &integrand0I[0], integrand0I.m());

    // wrt qL
    for (int i = 0; i < nDOFL; i++)
    {
      qfldElemL.DOF(i) += 1;
      fcnPDEL(sRefTrace, &integrand1L[0], integrand1L.m(), &integrand1I[0], integrand1I.m());
      qfldElemL.DOF(i) -= 1;

      for (int j = 0; j < nDOFL; j++)
        mtxElemLTrue.PDE_q(j,i) = integrand1L[j] - integrand0L[j];

      for (int j = 0; j < nDOFI; j++)
        mtxElemITrue.PDE_q(j,i) = integrand1I[j] - integrand0I[j];
    }

    // wrt aL
    for (int d = 0; d < PhysD2::D; d++)
      for (int i = 0; i < nDOFL; i++)
      {
        afldElemL.DOF(i)[d] += 1;
        fcnPDEL(sRefTrace, &integrand1L[0], integrand1L.m(), &integrand1I[0], integrand1I.m());
        afldElemL.DOF(i)[d] -= 1;

        for (int j = 0; j < nDOFL; j++)
          mtxElemLTrue.PDE_a(j,i)(0,d) = integrand1L[j] - integrand0L[j];

        for (int j = 0; j < nDOFI; j++)
          mtxElemITrue.PDE_a(j,i)(0,d) = integrand1I[j] - integrand0I[j];
      }

    // wrt qI
    for (int i = 0; i < nDOFI; i++)
    {
      qedge.DOF(i) += 1;
      fcnPDEL(sRefTrace, &integrand1L[0], integrand1L.m(), &integrand1I[0], integrand1I.m());
      qedge.DOF(i) -= 1;

      for (int j = 0; j < nDOFL; j++)
        mtxElemLTrue.PDE_qI(j,i) = integrand1L[j] - integrand0L[j];

      for (int j = 0; j < nDOFI; j++)
        mtxElemITrue.PDE_qI(j,i) = integrand1I[j] - integrand0I[j];
    }

    mtxElemL = 0;
    mtxElemI = 0;

    // accumulate the jacobian via Surreal
    fcnPDEL(1., sRefTrace, mtxElemL, mtxElemI);

    for (int i = 0; i < nDOFL; i++)
    {
      for (int j = 0; j < nDOFL; j++)
        SANS_CHECK_CLOSE( mtxElemLTrue.PDE_q(i,j), mtxElemL.PDE_q(i,j), small_tol, close_tol );

      for (int d = 0; d < PhysD2::D; d++)
        for (int j = 0; j < nDOFL; j++)
          SANS_CHECK_CLOSE( mtxElemLTrue.PDE_a(i,j)(0,d), mtxElemL.PDE_a(i,j)(0,d), small_tol, close_tol );

      for (int j = 0; j < nDOFI; j++)
        SANS_CHECK_CLOSE( mtxElemLTrue.PDE_qI(i,j), mtxElemL.PDE_qI(i,j), small_tol, close_tol );
    }

    for (int i = 0; i < nDOFI; i++)
    {
      for (int j = 0; j < nDOFL; j++)
        SANS_CHECK_CLOSE( mtxElemITrue.PDE_q(i,j), mtxElemI.PDE_q(i,j), small_tol, close_tol );

      for (int d = 0; d < PhysD2::D; d++)
        for (int j = 0; j < nDOFL; j++)
          SANS_CHECK_CLOSE( mtxElemITrue.PDE_a(i,j)(0,d), mtxElemI.PDE_a(i,j)(0,d), small_tol, close_tol );

      for (int j = 0; j < nDOFI; j++)
        SANS_CHECK_CLOSE( mtxElemITrue.PDE_qI(i,j), mtxElemI.PDE_qI(i,j), small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_2D_Triangle_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordType;

  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldTrace;
  typedef Element<DLA::VectorS<2,ArrayQ>,TopoD2,Triangle> ElementAFieldCell;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementParam> FieldWeightedClass;

  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_UniformGrad source(0.25, 0.6, -1.5);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid
  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemR.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;
  x4 = 1;  y4 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xfldElemR.DOF(0) = {x4, y4};
  xfldElemR.DOF(1) = {x3, y3};
  xfldElemR.DOF(2) = {x2, y2};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution
  order = 1;
  ElementQFieldCell qfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell qfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemR.nDOF() );

  // triangle solution (left)
  qfldElemL.DOF(0) = 1;
  qfldElemL.DOF(1) = 3;
  qfldElemL.DOF(2) = 4;

  // triangle solution (right)
  qfldElemR.DOF(0) = 7;
  qfldElemR.DOF(1) = 2;
  qfldElemR.DOF(2) = 9;

  // auxiliary variable
  ElementAFieldCell afldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementAFieldCell afldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, afldElemL.order() );
  BOOST_CHECK_EQUAL( 3, afldElemL.nDOF() );

  afldElemL.DOF(0) = { 2, -3};
  afldElemL.DOF(1) = { 7,  8};
  afldElemL.DOF(2) = {-1, -5};

  afldElemR.DOF(0) = { 9,  6};
  afldElemR.DOF(1) = {-1,  3};
  afldElemR.DOF(2) = { 2, -3};

  // interface solution
  ElementQFieldTrace qedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qedge.order() );
  BOOST_CHECK_EQUAL( 2, qedge.nDOF() );

  qedge.DOF(0) =  8;
  qedge.DOF(1) = -1;

  ElementQFieldCell wfldElemL(order+1, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell wfldElemR(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElemL.order() );
  BOOST_CHECK_EQUAL( 6, wfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 2, wfldElemR.order() );
  BOOST_CHECK_EQUAL( 6, wfldElemR.nDOF() );

  // triangle solution (left)
  wfldElemL.DOF(0) = -2;
  wfldElemL.DOF(1) =  4;
  wfldElemL.DOF(2) =  3;
  wfldElemL.DOF(3) =  2;
  wfldElemL.DOF(4) =  4;
  wfldElemL.DOF(5) = -1;

  // triangle solution (right)
  wfldElemR.DOF(0) =  7;
  wfldElemR.DOF(1) =  3;
  wfldElemR.DOF(2) =  2;
  wfldElemR.DOF(3) = -1;
  wfldElemR.DOF(4) =  5;
  wfldElemR.DOF(5) = -3;

  // auxiliary variable
  ElementAFieldCell bfldElemL(order+1, BasisFunctionCategory_Hierarchical);
  ElementAFieldCell bfldElemR(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, bfldElemL.order() );
  BOOST_CHECK_EQUAL( 6, bfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 2, bfldElemR.order() );
  BOOST_CHECK_EQUAL( 6, bfldElemR.nDOF() );

  bfldElemL.DOF(0) = { 1, 5};
  bfldElemL.DOF(1) = { 3, 6};
  bfldElemL.DOF(2) = {-1, 7};
  bfldElemL.DOF(3) = { 3, 1};
  bfldElemL.DOF(4) = { 2, 2};
  bfldElemL.DOF(5) = { 4, 3};

  bfldElemR.DOF(0) = { 5, 6};
  bfldElemR.DOF(1) = { 4, 5};
  bfldElemR.DOF(2) = { 3, 4};
  bfldElemR.DOF(3) = { 4, 3};
  bfldElemR.DOF(4) = { 3, 2};
  bfldElemR.DOF(5) = { 2, 1};

  // interface solution
  ElementQFieldTrace wedge(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wedge.order() );
  BOOST_CHECK_EQUAL( 3, wedge.nDOF() );

  wedge.DOF(0) =  2;
  wedge.DOF(1) = -1;
  wedge.DOF(2) =  3;


  // HDG discretization
  DiscretizationClass disc( pde, Local );

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  FieldWeightedClass fcnL = fcnint.integrand( xedge, qedge, wedge, CanonicalTraceToCell(0,1), +1,
                                              xfldElemL, qfldElemL, afldElemL, wfldElemL, bfldElemL );

  FieldWeightedClass fcnR = fcnint.integrand( xedge, qedge, wedge, CanonicalTraceToCell(0,-1), -1,
                                              xfldElemR, qfldElemR, afldElemR, wfldElemR, bfldElemR );

  BOOST_CHECK_EQUAL( 1, fcnL.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcnL.nDOFLeft() );
  BOOST_CHECK( fcnL.needsEvaluation() == true );

  BOOST_CHECK_EQUAL( 1, fcnR.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcnR.nDOFLeft() );
  BOOST_CHECK( fcnR.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordType sRef;
  FieldWeightedClass::IntegrandCellType integrandL, integrandR;
  FieldWeightedClass::IntegrandTraceType integrandIL, integrandIR, integrandI;
  Real integrandPDELTrue, integrandPDERTrue, integrandPDEITrue;
  Real integrandAUXLTrue, integrandAUXRTrue;

  // Test at sRef={0}, {s,t}={1, 0}
  sRef={0};
  fcnL( sRef, integrandL, integrandIL );
  fcnR( sRef, integrandR, integrandIR );

  integrandI = integrandIL + integrandIR;

  //PDE residual integrands (left): (advective) + (viscous) + (stabilization)
  integrandPDELTrue = ( (39/5.)*(sqrt(2)) ) + ( (-8216/125.)*(sqrt(2)) ) + ( (-2236/25.)*(sqrt(2)) ); // Weight function

  //PDE residual integrands (right): (advective) + (viscous) + (stabilization)
  integrandPDERTrue = ( (-52/5.)*(sqrt(2)) ) + ( (143/125.)*(sqrt(2)) ) + ( (1118/125.)*(sqrt(2)) ); // Weight function

  SANS_CHECK_CLOSE( integrandPDELTrue, integrandL.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue, integrandR.PDE, small_tol, close_tol );

  //AUX residual integrands: (Auxiliary)
  integrandAUXLTrue = ( (-36)*(sqrt(2)) );  // Weight function
  integrandAUXRTrue = ( (28)*(sqrt(2)) );  // Weight function

  SANS_CHECK_CLOSE( integrandAUXLTrue, integrandL.Au, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXRTrue, integrandR.Au, small_tol, close_tol );

  //Interface residual integrands: (advective) + (viscous) + (stabilization)
  integrandPDEITrue = ( (13)*(pow(2,-1/2.)) ) + ( (793/25.)*(sqrt(2)) ) + ( (4472/125.)*(sqrt(2)) );  // Weight function

  SANS_CHECK_CLOSE( integrandPDEITrue, integrandI, small_tol, close_tol );


  // Test at sRef={1}, {s,t}={0, 1}
  sRef={1};
  fcnL( sRef, integrandL, integrandIL );
  fcnR( sRef, integrandR, integrandIR );

  integrandI = integrandIL + integrandIR;

  //PDE residual integrands (left): (advective) + (viscous) + (stabilization)
  integrandPDELTrue = ( (39/5.)*(sqrt(2)) ) + ( (4017/125.)*(pow(2,-1/2.)) ) + ( (1677/25.)*(sqrt(2)) ); // Weight function

  //PDE residual integrands (right): (advective) + (viscous) + (stabilization)
  integrandPDERTrue = ( (39/10.)*(pow(2,-1/2.)) ) + ( (663/125.)*(pow(2,-1/2.)) ) + ( (5031/125.)*(sqrt(2)) ); // Weight function

  SANS_CHECK_CLOSE( integrandPDELTrue, integrandL.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue, integrandR.PDE, small_tol, close_tol );

  //AUX residual integrands: (Auxiliary)
  integrandAUXLTrue = ( (3)*(sqrt(2)) );  // Weight function
  integrandAUXRTrue = ( (-9)*(pow(2,-1/2.)) );  // Weight function

  SANS_CHECK_CLOSE( integrandAUXLTrue, integrandL.Au, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXRTrue, integrandR.Au, small_tol, close_tol );

  //Interface residual integrands: (advective) + (viscous) + (stabilization)
  integrandPDEITrue = ( (13/2.)*(pow(2,-1/2.)) ) + ( (156/25.)*(sqrt(2)) ) + ( (4472/125.)*(sqrt(2)) );  // Weight function

  SANS_CHECK_CLOSE( integrandPDEITrue, integrandI, small_tol, close_tol );


  // Test at sRef={1/2}, {s,t}={1/2, 1/2}
  sRef={1./2.};
  fcnL( sRef, integrandL, integrandIL );
  fcnR( sRef, integrandR, integrandIR );

  integrandI = integrandIL + integrandIR;

  //PDE residual integrands (left): (advective) + (viscous) + (stabilization)
  integrandPDELTrue = ( (1001/40.)*(pow(2,-1/2.)) ) + ( (-30459/500.)*(pow(2,-1/2.)) ) + ( 0 ); // Weight function

  //PDE residual integrands (right): (advective) + (viscous) + (stabilization)
  integrandPDERTrue = ( (-273/40.)*(pow(2,-1/2.)) ) + ( (273/125.)*(pow(2,-1/2.)) ) + ( (1677/125.)*(sqrt(2)) ); // Weight function

  SANS_CHECK_CLOSE( integrandPDELTrue, integrandL.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue, integrandR.PDE, small_tol, close_tol );

  //AUX residual integrands: (Auxiliary)
  integrandAUXLTrue = ( (-161/4.)*(pow(2,-1/2.)) );  // Weight function
  integrandAUXRTrue = ( (105/2.)*(pow(2,-1/2.)) );  // Weight function

  SANS_CHECK_CLOSE( integrandAUXLTrue, integrandL.Au, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXRTrue, integrandR.Au, small_tol, close_tol );

  //Interface residual integrands: (advective) + (viscous) + (stabilization)
  integrandPDEITrue = ( 0 ) + ( (3367/100.)*(pow(2,-1/2.)) ) + ( (-3913/125.)*(sqrt(2)) );  // Weight function

  SANS_CHECK_CLOSE( integrandPDEITrue, integrandI, small_tol, close_tol );


  // Test at sRef={1/5}, {s,t}={4/5, 1/5}
  sRef={1./5.};
  fcnL( sRef, integrandL, integrandIL );
  fcnR( sRef, integrandR, integrandIR );

  integrandI = integrandIL + integrandIR;

  //PDE residual integrands (left): (advective) + (viscous) + (stabilization)
  integrandPDELTrue = ( (6604/625.)*(sqrt(2)) ) + ( (-1916811/15625.)*(pow(2,-1/2.)) ) + ( (-212979/3125.)*(sqrt(2)) ); // Weight function

  //PDE residual integrands (right): (advective) + (viscous) + (stabilization)
  integrandPDERTrue = ( (-15717/1250.)*(pow(2,-1/2.)) ) + ( (30927/15625.)*(pow(2,-1/2.)) ) + ( (152607/15625.)*(sqrt(2)) ); // Weight function

  SANS_CHECK_CLOSE( integrandPDELTrue, integrandL.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue, integrandR.PDE, small_tol, close_tol );

  //AUX residual integrands: (Auxiliary)
  integrandAUXLTrue = ( (-4247/125.)*(sqrt(2)) );  // Weight function
  integrandAUXRTrue = ( (9207/125.)*(pow(2,-1/2.)) );  // Weight function

  SANS_CHECK_CLOSE( integrandAUXLTrue, integrandL.Au, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandAUXRTrue, integrandR.Au, small_tol, close_tol );

  //Interface residual integrands: (advective) + (viscous) + (stabilization)
  integrandPDEITrue = ( (3237/250.)*(pow(2,-1/2.)) ) + ( (23738/625.)*(sqrt(2)) ) + ( (371176/15625.)*(sqrt(2)) );  // Weight function

  SANS_CHECK_CLOSE( integrandPDEITrue, integrandI, small_tol, close_tol );

  // Test the Elemental Integral

  int quadratureorder = 3;
  typedef FieldWeightedClass::IntegrandCellType CellIntegrandType;
  typedef FieldWeightedClass::IntegrandTraceType TraceIntegrandType;
  ElementIntegral<TopoD1, Line, CellIntegrandType, TraceIntegrandType> integral(quadratureorder);

  CellIntegrandType rsdElemL = 0;
  CellIntegrandType rsdElemR = 0;
  TraceIntegrandType rsdElemTraceL = 0, rsdElemTraceR = 0, rsdElemTrace = 0;

  // cell integration for canonical element
  integral( fcnL, xedge, rsdElemL, rsdElemTraceL );
  integral( fcnR, xedge, rsdElemR, rsdElemTraceR );

  rsdElemTrace = rsdElemTraceL + rsdElemTraceR;

  Real rsdPDELTrue, rsdPDERTrue, rsdPDEITrue;
  Real rsdAUXLTrue, rsdAUXRTrue;

  //PDE residuals (left): (advective) + (viscous) + (stabilization)
  rsdPDELTrue = ( 1313/60. ) + ( -21437/375. ) + ( -559/75. ); // Weight function

  //PDE residuals (right): (advective) + (viscous) + (stabilization)
  rsdPDERTrue = ( -221/30. ) + ( 2041/750. )+ ( 12857/375. ); // Weight function

  SANS_CHECK_CLOSE( rsdPDELTrue, rsdElemL.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue, rsdElemR.PDE, small_tol, close_tol );

  //Aux residuals (left):
  rsdAUXLTrue = ( -227/6. );   // Basis function

  //AUX residuals (right):
  rsdAUXRTrue = ( 257/6. );   // Basis function

  SANS_CHECK_CLOSE( rsdAUXLTrue, rsdElemL.Au, small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdAUXRTrue, rsdElemR.Au, small_tol, close_tol );

  //INT residuals: (advective) + (viscous) + (stabilization)
  rsdPDEITrue = ( 13/4. )+( 351/10. ) + ( -2236/125. );   // Weight function

  SANS_CHECK_CLOSE( rsdPDEITrue, rsdElemTrace, small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_2D_Triangle_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;

  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldTrace;
  typedef Element<DLA::VectorS<2,ArrayQ>,TopoD2,Triangle> ElementAFieldCell;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementParam> BasisWeightedClass;
  typedef IntegrandClass::BasisWeighted_AUX<Real,TopoD1,Line,TopoD2,Triangle,ElementParam> BasisWeightedAUXClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementParam> FieldWeightedClass;
  typedef DiscretizationHDG<PDEClass> DiscretizationClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_UniformGrad source(0.25, 0.6, -1.5);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid
  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemR.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;
  x4 = 1;  y4 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xfldElemR.DOF(0) = {x4, y4};
  xfldElemR.DOF(1) = {x3, y3};
  xfldElemR.DOF(2) = {x2, y2};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  for (int qorder = 2; qorder< 4; qorder++)
  {
    // solution
    ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell qfldElemR(qorder, BasisFunctionCategory_Hierarchical);
    ElementAFieldCell afldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementAFieldCell afldElemR(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElemR(qorder, BasisFunctionCategory_Hierarchical);
    ElementAFieldCell bfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementAFieldCell bfldElemR(qorder, BasisFunctionCategory_Hierarchical);

    ElementQFieldTrace qedge(qorder, BasisFunctionCategory_Legendre);
    ElementQFieldTrace wedge(qorder, BasisFunctionCategory_Legendre);

    BOOST_CHECK_EQUAL( qorder,   qedge.order() );
    BOOST_CHECK_EQUAL( qorder+1, qedge.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   wedge.order() );
    BOOST_CHECK_EQUAL( qorder+1, wedge.nDOF() );

    BOOST_CHECK_EQUAL( qedge.nDOF(), wedge.nDOF() );
    for (int dof = 0; dof < qedge.nDOF(); dof++)
    {
      qedge.DOF(dof) = (dof+1)*pow(-1,dof);
      wedge.DOF(dof) = 0;
    }

    BOOST_CHECK_EQUAL( qorder,   qfldElemL.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   qfldElemR.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElemR.nDOF() );

    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), qfldElemR.nDOF() );

    BOOST_CHECK_EQUAL( qorder,   afldElemL.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, afldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   afldElemR.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, afldElemR.nDOF() );

    BOOST_CHECK_EQUAL( afldElemL.nDOF(), afldElemR.nDOF() );

    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), afldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), wfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), bfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), afldElemR.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), wfldElemR.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), bfldElemR.nDOF() );
    // line solution
    for ( int dof = 0; dof < qfldElemL.nDOF(); dof ++ )
    {
      qfldElemL.DOF(dof) = (dof+1)*pow(-1,dof);
      qfldElemR.DOF(dof) = (dof+1)*pow(-1,dof);
      wfldElemL.DOF(dof) = 0;
      wfldElemR.DOF(dof) = 0;

      afldElemL.DOF(dof) = (dof+2)*pow(-1,dof+1);
      afldElemR.DOF(dof) = (dof+2)*pow(-1,dof+1);
      bfldElemL.DOF(dof) = 0;
      bfldElemR.DOF(dof) = 0;
    }

    // HDG discretization
    DiscretizationClass disc( pde, Local );

    // integrand
    IntegrandClass fcnint( pde, disc, {0} );

    // integrand functor
    BasisWeightedClass fcnB_PDEL = fcnint.integrand( xedge, qedge,
                                                     CanonicalTraceToCell(0,0), +1,
                                                     xfldElemL,
                                                     qfldElemL, afldElemL );

    BasisWeightedClass fcnB_PDER = fcnint.integrand( xedge, qedge,
                                                     CanonicalTraceToCell(1,0), -1,
                                                     xfldElemR,
                                                     qfldElemR, afldElemR );

    BasisWeightedAUXClass fcnB_AUXL = fcnint.integrand_AUX( xedge, qedge,
                                                            CanonicalTraceToCell(0,0), +1,
                                                            xfldElemL,
                                                            qfldElemL );

    BasisWeightedAUXClass fcnB_AUXR = fcnint.integrand_AUX( xedge, qedge,
                                                            CanonicalTraceToCell(1,0), -1,
                                                            xfldElemR,
                                                            qfldElemR );

    // integrand functor
    FieldWeightedClass fcnWL = fcnint.integrand( xedge, qedge, wedge,
                                                 CanonicalTraceToCell(0,0), +1,
                                                 xfldElemL,
                                                 qfldElemL, afldElemL,
                                                 wfldElemL, bfldElemL );

    FieldWeightedClass fcnWR = fcnint.integrand( xedge, qedge, wedge,
                                                 CanonicalTraceToCell(1,0), -1,
                                                 xfldElemR,
                                                 qfldElemR, afldElemR,
                                                 wfldElemR, bfldElemR );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;
    const int nIntegrandL = qfldElemL.nDOF();
    const int nIntegrandR = qfldElemR.nDOF();
    const int nIntegrandTrace = qedge.nDOF();

    int quadratureorder = 0;
    GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedClass::IntegrandCellType,
                                           BasisWeightedClass::IntegrandTraceType>
                                           integralB_PDEL(quadratureorder, nIntegrandL, nIntegrandTrace);

    GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedClass::IntegrandCellType,
                                           BasisWeightedClass::IntegrandTraceType>
                                           integralB_PDER(quadratureorder, nIntegrandR, nIntegrandTrace);

    GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedAUXClass::IntegrandType>
                                           integralB_AUXL(quadratureorder, nIntegrandL);

    GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedAUXClass::IntegrandType>
                                           integralB_AUXR(quadratureorder, nIntegrandR);

    ElementIntegral<TopoD1, Line, FieldWeightedClass::IntegrandCellType,
                                  FieldWeightedClass::IntegrandTraceType> integralW(quadratureorder);

    std::vector<BasisWeightedClass::IntegrandCellType> rsdPDEElemBL(nIntegrandL, 0);
    std::vector<BasisWeightedClass::IntegrandCellType> rsdPDEElemBR(nIntegrandR, 0);
    std::vector<BasisWeightedClass::IntegrandTraceType> rsdPDEElemBIL(nIntegrandTrace, 0), rsdPDEElemBIR(nIntegrandTrace, 0);

    std::vector<BasisWeightedAUXClass::IntegrandType> rsdAUXElemBL(nIntegrandL, 0);
    std::vector<BasisWeightedAUXClass::IntegrandType> rsdAUXElemBR(nIntegrandR, 0);

    FieldWeightedClass::IntegrandCellType rsdElemWL = 0;
    FieldWeightedClass::IntegrandCellType rsdElemWR = 0;
    FieldWeightedClass::IntegrandTraceType rsdElemWIL = 0, rsdElemWIR = 0;

    // cell integration for canonical element
    integralB_PDEL( fcnB_PDEL, xedge, rsdPDEElemBL.data(), nIntegrandL, rsdPDEElemBIL.data(), nIntegrandTrace );
    integralB_PDER( fcnB_PDER, xedge, rsdPDEElemBR.data(), nIntegrandR, rsdPDEElemBIR.data(), nIntegrandTrace );

    integralB_AUXL( fcnB_AUXL, xedge, rsdAUXElemBL.data(), nIntegrandL );
    integralB_AUXR( fcnB_AUXR, xedge, rsdAUXElemBR.data(), nIntegrandR );

    for (int i = 0; i < wfldElemL.nDOF(); i++ )
    {
      // set just one of the weights to one
      wfldElemL.DOF(i) = 1; bfldElemL.DOF(i) = 1;
      wfldElemR.DOF(i) = 1; bfldElemR.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemWL = 0; rsdElemWR = 0;
      rsdElemWIL = 0; rsdElemWIR = 0;
      integralW(fcnWL, xedge, rsdElemWL, rsdElemWIL );
      integralW(fcnWR, xedge, rsdElemWR, rsdElemWIR );

      // test the two integrands are the same
      SANS_CHECK_CLOSE ( rsdElemWL.PDE, rsdPDEElemBL[i], small_tol, close_tol );
      SANS_CHECK_CLOSE ( rsdElemWR.PDE, rsdPDEElemBR[i], small_tol, close_tol );

      Real tmpL = 0, tmpR = 0;
      for (int d = 0; d < PhysD2::D; d ++)
      {
        tmpL += rsdAUXElemBL[i][d];
        tmpR += rsdAUXElemBR[i][d];
      }

      SANS_CHECK_CLOSE( rsdElemWL.Au, tmpL, small_tol, close_tol );
      SANS_CHECK_CLOSE( rsdElemWR.Au, tmpR, small_tol, close_tol );

      // reset to 0
      wfldElemL.DOF(i) = 0; bfldElemL.DOF(i) = 0;
      wfldElemR.DOF(i) = 0; bfldElemR.DOF(i) = 0;
    }

    for (int i = 0; i < wedge.nDOF(); i++ )
    {
      wedge.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemWL = 0; rsdElemWR = 0;
      rsdElemWIL = 0; rsdElemWIR = 0;
      integralW(fcnWL, xedge, rsdElemWL, rsdElemWIL );
      integralW(fcnWR, xedge, rsdElemWR, rsdElemWIR );

      SANS_CHECK_CLOSE( rsdElemWIL, rsdPDEElemBIL[i], small_tol, close_tol);
      SANS_CHECK_CLOSE( rsdElemWIR, rsdPDEElemBIR[i], small_tol, close_tol);
      wedge.DOF(i) = 0;
    }
  }
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
