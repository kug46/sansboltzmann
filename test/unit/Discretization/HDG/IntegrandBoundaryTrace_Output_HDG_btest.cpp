// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandBoundaryTrace_HDG_sansLG_Output_btest
// testing of boundary output integrand routines for HDG sansLG

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_OutputWeightRsd_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_Flux_mitState_HDG.h"

#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/OutputEuler2D.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandBoundaryTrace_HDG_sansLG_Output_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IntegrandTest_NS_Drag_P1_2D_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef NDPDEClass::PhysDim PhysDim;

  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;

  typedef DiscretizationHDG<NDPDEClass> DiscretizationClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDOutputClass::template ArrayJ<Real> ArrayJ;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
//  const Real tSutherland = 110;     // K
//  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // HDG discretization
  DiscretizationClass disc(pde, Local, Gradient);

  // static
  BOOST_CHECK( PhysDim::D == 2 );

  //Elements
  typedef ElementXField<PhysD2, TopoD2, Triangle> ElementXFieldCell;
  typedef ElementXField<PhysD2, TopoD1, Line> ElementXFieldTrace;
  typedef Element<ArrayQ, TopoD2, Triangle> ElementQFieldType;
  typedef Element<VectorArrayQ, TopoD2, Triangle> ElementAFieldType;
  typedef Element<ArrayQ, TopoD1, Line> ElementQFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordType;

  // WALL BC INTEGRAND
  typedef BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass> BCClass;
  typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, HDG> BCIntegrandBoundaryTrace;

  NDBCClass bc(pde);

  BCIntegrandBoundaryTrace bcintegrand( pde, bc, {1}, disc );

  // DRAG INTEGRAND
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, HDG> IntegrandOutputClass;

  NDOutputClass outputFcn(pde, 1., 0.);
  IntegrandOutputClass output_integrand( outputFcn, {1} );


  typedef IntegrandOutputClass::Functor<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell,BCIntegrandBoundaryTrace> FunctorClass;

  // grid

  int order = 1;
  ElementXFieldCell xfldElemCell(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemCell.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemCell.nDOF() );

  // triangle grid
  xfldElemCell.DOF(0) = {0, 0};
  xfldElemCell.DOF(1) = {1, 0};
  xfldElemCell.DOF(2) = {0, 1};

  ElementXFieldTrace xfldElemTrace(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemTrace.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemTrace.nDOF() );

  // trace grid
  xfldElemTrace.DOF(0) = {1, 0};
  xfldElemTrace.DOF(1) = {0, 1};

  CanonicalTraceToCell canonicalTrace(0,1);

  // solution: P1

  order = 1;
  ElementQFieldType qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = {1.0, 0.0, 0.0, 1.0};
  qfldElem.DOF(1) = {1.0, 0.0, 0.0, 1.0};
  qfldElem.DOF(2) = {1.0, 0.0, 0.0, 1.0};

  ElementAFieldType afldElem(order, BasisFunctionCategory_Hierarchical);
  afldElem.DOF(0) = 0.0;
  afldElem.DOF(1) = 0.0;
  afldElem.DOF(2) = 0.0;

  ElementQFieldTrace qIfldElemTrace( order, BasisFunctionCategory_Hierarchical );
  qIfldElemTrace.DOF(0) = {1.0, 0.0, 0.0, 1.0};
  qIfldElemTrace.DOF(1) = {1.0, 0.0, 0.0, 1.0};

  // //Weights
  // ElementQFieldType wfldElem( 0, BasisFunctionCategory_Legendre );
  // ElementAFieldType bfldElem( 0, BasisFunctionCategory_Legendre );
  // wfldElem.DOF(0) = 1;
  // bfldElem.DOF(0) = 0;

  FunctorClass fcn = output_integrand.integrand(bcintegrand,
                                                xfldElemTrace, canonicalTrace,
                                                xfldElemCell, qfldElem, afldElem,
                                                qIfldElemTrace );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordType sRef;
  ArrayJ integrandTrue;
  ArrayJ integrand;

  sRef = 0.5;
  integrandTrue = sqrt(2)/2;
  fcn( sRef, integrand );
  SANS_CHECK_CLOSE( integrandTrue, integrand, small_tol, close_tol );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
