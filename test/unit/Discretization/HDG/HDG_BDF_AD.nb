(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     29711,        946]
NotebookOptionsPosition[     26609,        829]
NotebookOutlinePosition[     26964,        845]
CellTagsIndexPosition[     26921,        842]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["1D", "Section",
 CellChangeTimes->{{3.633362885556506*^9, 3.633362890337681*^9}, {
   3.633362945492444*^9, 3.633362946018672*^9}, {3.641227999622908*^9, 
   3.6412280189368362`*^9}, {3.641644296383087*^9, 3.641644296726069*^9}, 
   3.641644387894855*^9}],

Cell[CellGroupData[{

Cell["BDF1 Temporal Cell integral:", "Subsection",
 CellChangeTimes->{
  3.641228090290807*^9, {3.6416443038392344`*^9, 3.641644308670457*^9}, {
   3.648559832613029*^9, 3.64855984791011*^9}, {3.6485605680952263`*^9, 
   3.6485605689017*^9}, {3.648563118882769*^9, 3.648563119018849*^9}}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.648560542484597*^9, 3.648560542490553*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{"phi", " ", "dudt"}], ")"}], "/.", 
        RowBox[{"{", 
         RowBox[{"dudt", "\[Rule]", 
          RowBox[{"(", 
           RowBox[{"du", "/", "dt"}], ")"}]}], "}"}]}], "/.", 
       RowBox[{"{", 
        RowBox[{"dt", "\[Rule]", "13"}], "}"}]}], "/.", 
      RowBox[{"{", 
       RowBox[{"du", " ", "\[Rule]", " ", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{
           RowBox[{"u0t1", " ", "-", " ", "u0t0"}], ",", " ", 
           RowBox[{"u1t1", "-", "u1t0"}]}], "}"}], ".", "phi"}]}], "}"}]}], "/.",
      "\[IndentingNewLine]", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"u0t1", "\[Rule]", "1"}], ",", 
       RowBox[{"u0t0", "\[Rule]", "0"}], ",", 
       RowBox[{"u1t1", " ", "\[Rule]", " ", "4"}], ",", " ", 
       RowBox[{"u1t0", "\[Rule]", "0"}]}], "}"}]}], " ", "/.", 
    RowBox[{"{", 
     RowBox[{"phi", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"1", "-", "x"}], ",", "x"}], "}"}]}], "}"}]}], "/.", 
   RowBox[{"{", 
    RowBox[{"x", "\[Rule]", "s"}], "}"}]}], "//", "Simplify"}]], "Input",
 CellChangeTimes->{{3.6484927471219254`*^9, 3.648492750481264*^9}, 
   3.648492816282699*^9, {3.648493121056246*^9, 3.648493134304059*^9}, {
   3.648493170332868*^9, 3.648493170612846*^9}, {3.648493319871538*^9, 
   3.648493370089692*^9}, {3.648495729059836*^9, 3.648495738869302*^9}, {
   3.648495782327435*^9, 3.648495798566021*^9}, {3.64856261271646*^9, 
   3.648562622474966*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    RowBox[{"-", 
     FractionBox["1", "13"]}], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "1"}], "+", "s"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", 
      RowBox[{"3", " ", "s"}]}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "13"], " ", "s", " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", 
      RowBox[{"3", " ", "s"}]}], ")"}]}]}], "}"}]], "Output",
 CellChangeTimes->{3.648863792970868*^9, 3.648863840739256*^9, 
  3.648863955270307*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"%", "/.", 
  RowBox[{"{", 
   RowBox[{"s", "\[Rule]", "0"}], "}"}], " "}]], "Input",
 CellChangeTimes->{{3.6488636912717323`*^9, 3.648863695118424*^9}, {
  3.648863802503969*^9, 3.648863814822551*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["1", "13"], ",", "0"}], "}"}]], "Output",
 CellChangeTimes->{
  3.648863695411978*^9, {3.6488637975574093`*^9, 3.648863802927992*^9}, {
   3.648863836417513*^9, 3.648863842287689*^9}, 3.648863956799109*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"%%", "/.", 
  RowBox[{"{", 
   RowBox[{"s", "\[Rule]", "1"}], "}"}], " "}]], "Input",
 CellChangeTimes->{{3.648863804709899*^9, 3.6488638436411037`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0", ",", 
   FractionBox["4", "13"]}], "}"}]], "Output",
 CellChangeTimes->{3.648492769967259*^9, 3.64849317766286*^9, 
  3.648493229854669*^9, 3.6484933758183517`*^9, 3.648495746035666*^9, 
  3.648495806163171*^9, 3.6484958578966627`*^9, 3.648563363488165*^9, 
  3.648863697914077*^9, 3.648863805389415*^9, 3.6488638440605097`*^9, 
  3.648863958160453*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"%%%", "/.", 
  RowBox[{"{", 
   RowBox[{"s", "\[Rule]", 
    RowBox[{"1", "/", "3"}]}], "}"}], " "}]], "Input",
 CellChangeTimes->{{3.648493209170795*^9, 3.648493218489334*^9}, {
   3.64856335334656*^9, 3.648563355867094*^9}, {3.6488638070460577`*^9, 
   3.648863807431994*^9}, 3.648863845592121*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["4", "39"], ",", 
   FractionBox["2", "39"]}], "}"}]], "Output",
 CellChangeTimes->{3.6488636995567904`*^9, 3.648863807757444*^9, 
  3.6488638459203978`*^9, 3.648863960486403*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Integrate", "[", 
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{
          RowBox[{
           RowBox[{"(", 
            RowBox[{"phi", " ", "dudt"}], ")"}], "/.", 
           RowBox[{"{", 
            RowBox[{"dudt", "\[Rule]", 
             RowBox[{"(", 
              RowBox[{"du", "/", "dt"}], ")"}]}], "}"}]}], "/.", 
          RowBox[{"{", 
           RowBox[{"dt", "\[Rule]", "13"}], "}"}]}], "/.", 
         RowBox[{"{", 
          RowBox[{"du", " ", "\[Rule]", " ", 
           RowBox[{
            RowBox[{"{", 
             RowBox[{
              RowBox[{"u0t1", " ", "-", " ", "u0t0"}], ",", " ", 
              RowBox[{"u1t1", "-", "u1t0"}]}], "}"}], ".", "phi"}]}], "}"}]}],
         "/.", "\[IndentingNewLine]", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"u0t1", "\[Rule]", "1"}], ",", 
          RowBox[{"u0t0", "\[Rule]", "0"}], ",", 
          RowBox[{"u1t1", " ", "\[Rule]", " ", "4"}], ",", " ", 
          RowBox[{"u1t0", "\[Rule]", "0"}]}], "}"}]}], " ", "/.", 
       RowBox[{"{", 
        RowBox[{"phi", "\[Rule]", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"1", "-", "x"}], ",", "x"}], "}"}]}], "}"}]}], "/.", 
      RowBox[{"{", 
       RowBox[{"x", "\[Rule]", "s"}], "}"}]}], "//", "Simplify"}], ",", 
    RowBox[{"{", 
     RowBox[{"s", ",", "0", ",", "1"}], "}"}]}], "]"}], " "}]], "Input",
 CellChangeTimes->{{3.648863634236376*^9, 3.6488637882876*^9}, 
   3.648863847528308*^9, {3.648863947611828*^9, 3.648863949186108*^9}, {
   3.648864024153096*^9, 3.6488640290990143`*^9}, {3.648864101910984*^9, 
   3.648864103205865*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["1", "13"], ",", 
   FractionBox["3", "26"]}], "}"}]], "Output",
 CellChangeTimes->{{3.648863680321903*^9, 3.6488637894376793`*^9}, 
   3.6488638481757603`*^9, {3.648863951520479*^9, 3.648863962094139*^9}, 
   3.648864033853731*^9, 3.648864103881319*^9}]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["BDF2 Temporal Cell integral:", "Subsection",
 CellChangeTimes->{
  3.641228090290807*^9, {3.6416443038392344`*^9, 3.641644308670457*^9}, {
   3.648559832613029*^9, 3.64855984791011*^9}, {3.6485605680952263`*^9, 
   3.6485605689017*^9}}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.648560542484597*^9, 3.648560542490553*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{"phi", " ", "dudt"}], ")"}], "/.", 
        RowBox[{"{", 
         RowBox[{"dudt", "\[Rule]", 
          RowBox[{"(", 
           RowBox[{"du", "/", "dt"}], ")"}]}], "}"}]}], "/.", 
       RowBox[{"{", 
        RowBox[{"dt", "\[Rule]", "13"}], "}"}]}], "/.", 
      RowBox[{"{", 
       RowBox[{"du", " ", "\[Rule]", " ", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{
           RowBox[{
            RowBox[{
             RowBox[{"3", "/", "2"}], "*", "u0t1"}], " ", "-", " ", 
            RowBox[{"2", "*", "u0t0"}], "+", 
            RowBox[{
             RowBox[{"1", "/", "2"}], "*", "u0tm1"}]}], ",", " ", 
           RowBox[{
            RowBox[{
             RowBox[{"3", "/", "2"}], "*", "u1t1"}], "-", 
            RowBox[{"2", "*", "u1t0"}], "+", 
            RowBox[{
             RowBox[{"1", "/", "2"}], "*", "u1tm1"}]}]}], "}"}], ".", 
         "phi"}]}], "}"}]}], "/.", "\[IndentingNewLine]", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"u0t1", "\[Rule]", "1"}], ",", 
       RowBox[{"u0t0", "\[Rule]", "0"}], ",", " ", 
       RowBox[{"u0tm1", " ", "\[Rule]", " ", 
        RowBox[{
         RowBox[{"-", "1"}], "/", "2"}]}], ",", " ", 
       RowBox[{"u1t1", " ", "\[Rule]", " ", "4"}], ",", " ", 
       RowBox[{"u1t0", "\[Rule]", "0"}], ",", " ", 
       RowBox[{"u1tm1", " ", "\[Rule]", " ", 
        RowBox[{"-", "2"}]}]}], "}"}]}], " ", "/.", 
    RowBox[{"{", 
     RowBox[{"phi", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"1", "-", "x"}], ",", "x"}], "}"}]}], "}"}]}], "/.", 
   RowBox[{"{", 
    RowBox[{"x", "\[Rule]", "s"}], "}"}]}], "//", "Simplify"}]], "Input",
 CellChangeTimes->{{3.6484927471219254`*^9, 3.648492750481264*^9}, 
   3.648492816282699*^9, {3.648493121056246*^9, 3.648493134304059*^9}, {
   3.648493170332868*^9, 3.648493170612846*^9}, {3.648493319871538*^9, 
   3.648493370089692*^9}, {3.648495729059836*^9, 3.648495738869302*^9}, {
   3.648495782327435*^9, 3.648495798566021*^9}, {3.64856261271646*^9, 
   3.648562622474966*^9}, {3.648563126828206*^9, 3.648563233431219*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    RowBox[{"-", 
     FractionBox["5", "52"]}], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "1"}], "+", "s"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", 
      RowBox[{"3", " ", "s"}]}], ")"}]}], ",", 
   RowBox[{
    FractionBox["5", "52"], " ", "s", " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", 
      RowBox[{"3", " ", "s"}]}], ")"}]}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.648495799106311*^9, 3.648495853714849*^9, 3.6485625852649403`*^9, 
   3.6485626804865723`*^9, {3.648563193402174*^9, 3.648563233875203*^9}, 
   3.648563303782827*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"%", "/.", 
  RowBox[{"{", 
   RowBox[{"s", "\[Rule]", "0"}], "}"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["5", "52"], ",", "0"}], "}"}]], "Output",
 CellChangeTimes->{
  3.6484927651094103`*^9, 3.648493173479352*^9, 3.648493228042697*^9, 
   3.648493372981517*^9, 3.648495743929657*^9, 3.648495804444537*^9, 
   3.648495855830179*^9, 3.648562587779858*^9, {3.6485631969301853`*^9, 
   3.648563236259796*^9}, 3.648563305840254*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"%%", "/.", 
  RowBox[{"{", 
   RowBox[{"s", "\[Rule]", "1"}], "}"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0", ",", 
   FractionBox["5", "13"]}], "}"}]], "Output",
 CellChangeTimes->{
  3.648492769967259*^9, 3.64849317766286*^9, 3.648493229854669*^9, 
   3.6484933758183517`*^9, 3.648495746035666*^9, 3.648495806163171*^9, 
   3.6484958578966627`*^9, {3.648563217818969*^9, 3.648563237686384*^9}, 
   3.648563307617263*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"%%%", "/.", 
  RowBox[{"{", 
   RowBox[{"s", "\[Rule]", 
    RowBox[{"1", "/", "3"}]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.648493209170795*^9, 3.648493218489334*^9}, {
  3.648563300073605*^9, 3.648563300209083*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["5", "39"], ",", 
   FractionBox["5", "78"]}], "}"}]], "Output",
 CellChangeTimes->{
  3.648492775111713*^9, 3.64849317938347*^9, {3.6484932096504917`*^9, 
   3.648493231507627*^9}, 3.648493377539394*^9, 3.64849574810317*^9, 
   3.648495807736868*^9, 3.648495860113492*^9, {3.6485632194826183`*^9, 
   3.648563239146995*^9}, {3.648563300409445*^9, 3.648563309769025*^9}}]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["2D", "Section",
 CellChangeTimes->{{3.633362885556506*^9, 3.633362890337681*^9}, {
   3.633362945492444*^9, 3.633362946018672*^9}, {3.641227999622908*^9, 
   3.6412280189368362`*^9}, {3.641644296383087*^9, 3.641644296726069*^9}, 
   3.641644387894855*^9, {3.648559863863584*^9, 3.648559864045999*^9}}],

Cell[CellGroupData[{

Cell["BDF1 Temporal Cell integral:", "Subsection",
 CellChangeTimes->{
  3.641228090290807*^9, {3.6416443038392344`*^9, 3.641644308670457*^9}, {
   3.648559832613029*^9, 3.64855984791011*^9}, {3.648563526472721*^9, 
   3.6485635274645147`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{"phi", " ", "dudt"}], ")"}], "/.", 
        RowBox[{"{", 
         RowBox[{"dudt", "\[Rule]", 
          RowBox[{"(", 
           RowBox[{"du", "/", "dt"}], ")"}]}], "}"}]}], "/.", 
       RowBox[{"{", 
        RowBox[{"dt", "\[Rule]", "13"}], "}"}]}], "/.", 
      RowBox[{"{", 
       RowBox[{"du", " ", "\[Rule]", " ", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{
           RowBox[{"(", 
            RowBox[{"u0t1", " ", "-", " ", "u0t0"}], ")"}], ",", " ", 
           RowBox[{"(", 
            RowBox[{"u1t1", "-", "u1t0"}], ")"}], ",", " ", 
           RowBox[{"(", 
            RowBox[{"u2t1", "-", "u2t0"}], ")"}]}], "}"}], ".", "phi"}]}], 
       "}"}]}], "/.", "\[IndentingNewLine]", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"u0t1", "\[Rule]", "1"}], ",", 
       RowBox[{"u0t0", "\[Rule]", "0"}], ",", 
       RowBox[{"u1t1", " ", "\[Rule]", " ", "3"}], ",", " ", 
       RowBox[{"u1t0", "\[Rule]", "0"}], ",", " ", 
       RowBox[{"u2t1", " ", "\[Rule]", " ", "4"}], ",", " ", 
       RowBox[{"u2t0", " ", "\[Rule]", " ", "0"}]}], "}"}]}], " ", "/.", 
    RowBox[{"{", 
     RowBox[{"phi", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"1", "-", "x", " ", "-", "y"}], ",", "x", ",", " ", "y"}], 
       "}"}]}], "}"}]}], "/.", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"x", "\[Rule]", "s"}], ",", " ", 
     RowBox[{"y", "\[Rule]", "t"}]}], "}"}]}], "//", "Simplify"}]], "Input",
 CellChangeTimes->{{3.648559908665593*^9, 3.6485599194081173`*^9}, {
  3.648559994437998*^9, 3.6485600752134447`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    RowBox[{"-", 
     FractionBox["1", "13"]}], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "1"}], "+", "s", "+", "t"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", 
      RowBox[{"2", " ", "s"}], "+", 
      RowBox[{"3", " ", "t"}]}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "13"], " ", "s", " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", 
      RowBox[{"2", " ", "s"}], "+", 
      RowBox[{"3", " ", "t"}]}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "13"], " ", "t", " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", 
      RowBox[{"2", " ", "s"}], "+", 
      RowBox[{"3", " ", "t"}]}], ")"}]}]}], "}"}]], "Output",
 CellChangeTimes->{3.648560079065206*^9, 3.648560157497291*^9, 
  3.64856020090382*^9, 3.6485602460153713`*^9, 3.648563430716968*^9, 
  3.648902017808674*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"%", "/.", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"s", "\[Rule]", "0"}], ",", " ", 
    RowBox[{"t", "\[Rule]", "0"}]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.6485600853432198`*^9, 3.648560091301793*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["1", "13"], ",", "0", ",", "0"}], "}"}]], "Output",
 CellChangeTimes->{3.648560093530541*^9, 3.648560159314061*^9, 
  3.648560202882536*^9, 3.648560248084647*^9, 3.648563432642424*^9, 
  3.648902019212454*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"%%", "/.", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"s", "\[Rule]", "1"}], ",", " ", 
    RowBox[{"t", "\[Rule]", "0"}]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.6485600962630568`*^9, 3.648560108935712*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0", ",", 
   FractionBox["3", "13"], ",", "0"}], "}"}]], "Output",
 CellChangeTimes->{3.648560204363698*^9, 3.648560249423355*^9, 
  3.648563434236846*^9, 3.648902020455468*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"%%%", "/.", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"s", "\[Rule]", "0"}], ",", " ", 
    RowBox[{"t", "\[Rule]", "1"}]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.648560171922164*^9, 3.648560208122158*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0", ",", "0", ",", 
   FractionBox["4", "13"]}], "}"}]], "Output",
 CellChangeTimes->{{3.648560195588793*^9, 3.6485602092155657`*^9}, 
   3.648560251870685*^9, 3.648563435683475*^9, 3.648902021888722*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"%%%%", "/.", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"s", "\[Rule]", 
     RowBox[{"1", "/", "3"}]}], ",", " ", 
    RowBox[{"t", "\[Rule]", 
     RowBox[{"2", "/", "3"}]}]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.648560220404464*^9, 3.648560242619069*^9}, {
  3.6485633949883432`*^9, 3.648563398868433*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0", ",", 
   FractionBox["11", "117"], ",", 
   FractionBox["22", "117"]}], "}"}]], "Output",
 CellChangeTimes->{{3.648902014476882*^9, 3.648902023201108*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Integrate", "[", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{"(", 
          RowBox[{"phi", " ", "dudt"}], ")"}], "/.", 
         RowBox[{"{", 
          RowBox[{"dudt", "\[Rule]", 
           RowBox[{"(", 
            RowBox[{"du", "/", "dt"}], ")"}]}], "}"}]}], "/.", 
        RowBox[{"{", 
         RowBox[{"dt", "\[Rule]", "13"}], "}"}]}], "/.", 
       RowBox[{"{", 
        RowBox[{"du", " ", "\[Rule]", " ", 
         RowBox[{
          RowBox[{"{", 
           RowBox[{
            RowBox[{"(", 
             RowBox[{"u0t1", " ", "-", " ", "u0t0"}], ")"}], ",", " ", 
            RowBox[{"(", 
             RowBox[{"u1t1", "-", "u1t0"}], ")"}], ",", " ", 
            RowBox[{"(", 
             RowBox[{"u2t1", "-", "u2t0"}], ")"}]}], "}"}], ".", "phi"}]}], 
        "}"}]}], "/.", "\[IndentingNewLine]", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"u0t1", "\[Rule]", "1"}], ",", 
        RowBox[{"u0t0", "\[Rule]", "0"}], ",", 
        RowBox[{"u1t1", " ", "\[Rule]", " ", "3"}], ",", " ", 
        RowBox[{"u1t0", "\[Rule]", "0"}], ",", " ", 
        RowBox[{"u2t1", " ", "\[Rule]", " ", "4"}], ",", " ", 
        RowBox[{"u2t0", " ", "\[Rule]", " ", "0"}]}], "}"}]}], " ", "/.", 
     RowBox[{"{", 
      RowBox[{"phi", "\[Rule]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"1", "-", "x", " ", "-", "y"}], ",", "x", ",", " ", "y"}], 
        "}"}]}], "}"}]}], "/.", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"x", "\[Rule]", "s"}], ",", " ", 
      RowBox[{"y", "\[Rule]", "t"}]}], "}"}]}], ",", " ", 
   RowBox[{"{", 
    RowBox[{"s", ",", "0", ",", "1"}], "}"}], ",", " ", 
   RowBox[{"{", 
    RowBox[{"t", ",", "0", ",", 
     RowBox[{"1", "-", "s"}]}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.648901912451983*^9, 3.648901993570269*^9}, {
  3.648902026853724*^9, 3.64890203062774*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["3", "104"], ",", 
   FractionBox["11", "312"], ",", 
   FractionBox["1", "26"]}], "}"}]], "Output",
 CellChangeTimes->{3.6489020376122723`*^9}]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["BDF2 Temporal Cell integral:", "Subsection",
 CellChangeTimes->{
  3.641228090290807*^9, {3.6416443038392344`*^9, 3.641644308670457*^9}, {
   3.648559832613029*^9, 3.64855984791011*^9}, {3.648563530024701*^9, 
   3.6485635308248568`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{"phi", " ", "dudt"}], ")"}], "/.", 
        RowBox[{"{", 
         RowBox[{"dudt", "\[Rule]", 
          RowBox[{"(", 
           RowBox[{"du", "/", "dt"}], ")"}]}], "}"}]}], "/.", 
       RowBox[{"{", 
        RowBox[{"dt", "\[Rule]", "13"}], "}"}]}], "/.", 
      RowBox[{"{", 
       RowBox[{"du", " ", "\[Rule]", " ", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{
           RowBox[{"(", 
            RowBox[{
             RowBox[{
              RowBox[{"3", "/", "2"}], "*", "u0t1"}], " ", "-", " ", 
             RowBox[{"2", "*", "u0t0"}], " ", "+", " ", 
             RowBox[{
              RowBox[{"1", "/", "2"}], "*", "u0tm1"}]}], ")"}], ",", " ", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{
              RowBox[{"3", "/", "2"}], "*", "u1t1"}], "-", 
             RowBox[{"2", "*", "u1t0"}], " ", "+", " ", 
             RowBox[{
              RowBox[{"1", "/", "2"}], "*", "u1tm1"}]}], ")"}], ",", " ", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{
              RowBox[{"3", "/", "2"}], "*", "u2t1"}], "-", 
             RowBox[{"2", "*", "u2t0"}], " ", "+", 
             RowBox[{
              RowBox[{"1", "/", "2"}], "*", "u2tm1"}]}], ")"}]}], "}"}], ".", 
         "phi"}]}], "}"}]}], "/.", "\[IndentingNewLine]", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"u0t1", "\[Rule]", "1"}], ",", 
       RowBox[{"u0t0", "\[Rule]", "0"}], ",", " ", 
       RowBox[{"u0tm1", " ", "\[Rule]", 
        RowBox[{"-", "2"}]}], ",", " ", 
       RowBox[{"u1t1", " ", "\[Rule]", " ", "3"}], ",", " ", 
       RowBox[{"u1t0", "\[Rule]", "0"}], ",", " ", 
       RowBox[{"u1tm1", " ", "\[Rule]", " ", 
        RowBox[{
         RowBox[{"-", "4"}], "/", "9"}]}], ",", " ", 
       RowBox[{"u2t1", " ", "\[Rule]", " ", "4"}], ",", " ", 
       RowBox[{"u2t0", " ", "\[Rule]", " ", "0"}], ",", " ", 
       RowBox[{"u2tm1", " ", "\[Rule]", " ", 
        RowBox[{"-", "5"}]}]}], "}"}]}], " ", "/.", 
    RowBox[{"{", 
     RowBox[{"phi", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"1", "-", "x", " ", "-", "y"}], ",", "x", ",", " ", "y"}], 
       "}"}]}], "}"}]}], "/.", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"x", "\[Rule]", "s"}], ",", " ", 
     RowBox[{"y", "\[Rule]", "t"}]}], "}"}]}], "//", "Simplify"}]], "Input",
 CellChangeTimes->{{3.648559908665593*^9, 3.6485599194081173`*^9}, {
   3.648559994437998*^9, 3.6485600752134447`*^9}, {3.648563540905429*^9, 
   3.6485636414212103`*^9}, {3.6485638043944883`*^9, 
   3.6485638332671423`*^9}, {3.648563889893812*^9, 3.6485638909013844`*^9}, 
   3.648563929736824*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    RowBox[{"-", 
     FractionBox["1", "234"]}], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "1"}], "+", "s", "+", "t"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{"24", "+", 
      RowBox[{"53", " ", "s"}], "+", 
      RowBox[{"39", " ", "t"}]}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "234"], " ", "s", " ", 
    RowBox[{"(", 
     RowBox[{"24", "+", 
      RowBox[{"53", " ", "s"}], "+", 
      RowBox[{"39", " ", "t"}]}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "234"], " ", "t", " ", 
    RowBox[{"(", 
     RowBox[{"24", "+", 
      RowBox[{"53", " ", "s"}], "+", 
      RowBox[{"39", " ", "t"}]}], ")"}]}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.648560079065206*^9, 3.648560157497291*^9, 3.64856020090382*^9, 
   3.6485602460153713`*^9, 3.648563430716968*^9, {3.648563634369526*^9, 
   3.64856364180624*^9}, {3.648563818902214*^9, 3.648563834892888*^9}, 
   3.648563892406555*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"%", "/.", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"s", "\[Rule]", "0"}], ",", " ", 
    RowBox[{"t", "\[Rule]", "0"}]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.6485600853432198`*^9, 3.648560091301793*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["4", "39"], ",", "0", ",", "0"}], "}"}]], "Output",
 CellChangeTimes->{
  3.648560093530541*^9, 3.648560159314061*^9, 3.648560202882536*^9, 
   3.648560248084647*^9, 3.648563432642424*^9, 3.6485636435806217`*^9, {
   3.6485638208741407`*^9, 3.648563839212449*^9}, 3.648563897535227*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"%%", "/.", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"s", "\[Rule]", "1"}], ",", " ", 
    RowBox[{"t", "\[Rule]", "0"}]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.6485600962630568`*^9, 3.648560108935712*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0", ",", 
   FractionBox["77", "234"], ",", "0"}], "}"}]], "Output",
 CellChangeTimes->{3.648560204363698*^9, 3.648560249423355*^9, 
  3.648563434236846*^9, 3.648563645098536*^9, 3.6485638231236477`*^9, 
  3.648563899446823*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"%%%", "/.", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"s", "\[Rule]", "0"}], ",", " ", 
    RowBox[{"t", "\[Rule]", "1"}]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.648560171922164*^9, 3.648560208122158*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0", ",", "0", ",", 
   FractionBox["7", "26"]}], "}"}]], "Output",
 CellChangeTimes->{{3.648560195588793*^9, 3.6485602092155657`*^9}, 
   3.648560251870685*^9, 3.648563435683475*^9, 3.6485636464982224`*^9, 
   3.6485638251027813`*^9, 3.648563901228112*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"%%%%", "/.", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"s", "\[Rule]", 
     RowBox[{"1", "/", "3"}]}], ",", " ", 
    RowBox[{"t", "\[Rule]", 
     RowBox[{"2", "/", "3"}]}]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.648560220404464*^9, 3.648560242619069*^9}, {
  3.6485633949883432`*^9, 3.648563398868433*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0", ",", 
   FractionBox["203", "2106"], ",", 
   FractionBox["203", "1053"]}], "}"}]], "Output",
 CellChangeTimes->{{3.648560226763322*^9, 3.648560253334046*^9}, 
   3.648563437187271*^9, 3.648563648602421*^9, 3.648563826949301*^9, 
   3.648563907951889*^9}]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["", "Section",
 CellChangeTimes->{{3.641644312920908*^9, 3.6416443218047047`*^9}, 
   3.6485598284556103`*^9, {3.64855987344639*^9, 3.648559895991066*^9}}],

Cell[BoxData["\[IndentingNewLine]"], "Input",
 CellChangeTimes->{{3.648492879181409*^9, 3.6484929048333797`*^9}, {
  3.648492970677807*^9, 3.648492998029477*^9}, {3.648493084990995*^9, 
  3.648493087253868*^9}, {3.648559859458447*^9, 3.648559889463036*^9}}]
}, Open  ]]
},
WindowSize->{927, 617},
WindowMargins->{{Automatic, 145}, {62, Automatic}},
FrontEndVersion->"10.2 for Mac OS X x86 (32-bit, 64-bit Kernel) (July 29, \
2015)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[579, 22, 261, 4, 64, "Section"],
Cell[CellGroupData[{
Cell[865, 30, 288, 4, 44, "Subsection"],
Cell[1156, 36, 92, 1, 28, "Input"],
Cell[CellGroupData[{
Cell[1273, 41, 1586, 42, 46, "Input"],
Cell[2862, 85, 528, 18, 48, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3427, 108, 225, 5, 28, "Input"],
Cell[3655, 115, 261, 6, 48, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3953, 126, 177, 4, 28, "Input"],
Cell[4133, 132, 397, 8, 48, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4567, 145, 324, 7, 28, "Input"],
Cell[4894, 154, 234, 6, 48, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5165, 165, 1685, 45, 63, "Input"],
Cell[6853, 212, 309, 7, 48, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[7211, 225, 242, 4, 44, "Subsection"],
Cell[7456, 231, 92, 1, 28, "Input"],
Cell[CellGroupData[{
Cell[7573, 236, 2207, 57, 80, "Input"],
Cell[9783, 295, 630, 20, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10450, 320, 103, 3, 28, "Input"],
Cell[10556, 325, 379, 8, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10972, 338, 104, 3, 28, "Input"],
Cell[11079, 343, 356, 8, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11472, 356, 245, 6, 28, "Input"],
Cell[11720, 364, 426, 9, 47, "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[12207, 380, 307, 4, 64, "Section"],
Cell[CellGroupData[{
Cell[12539, 388, 244, 4, 44, "Subsection"],
Cell[CellGroupData[{
Cell[12808, 396, 1699, 47, 63, "Input"],
Cell[14510, 445, 865, 27, 48, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15412, 477, 232, 6, 28, "Input"],
Cell[15647, 485, 264, 6, 48, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15948, 496, 233, 6, 28, "Input"],
Cell[16184, 504, 217, 5, 48, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[16438, 514, 232, 6, 28, "Input"],
Cell[16673, 522, 244, 5, 48, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[16954, 532, 336, 9, 28, "Input"],
Cell[17293, 543, 199, 5, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[17529, 553, 1930, 53, 80, InheritFromParent],
Cell[19462, 608, 199, 6, 48, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[19710, 620, 244, 4, 44, "Subsection"],
Cell[CellGroupData[{
Cell[19979, 628, 2756, 72, 97, "Input"],
Cell[22738, 702, 977, 29, 48, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[23752, 736, 232, 6, 28, "Input"],
Cell[23987, 744, 341, 7, 48, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[24365, 756, 233, 6, 28, "Input"],
Cell[24601, 764, 268, 6, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[24906, 775, 232, 6, 28, "Input"],
Cell[25141, 783, 296, 6, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[25474, 794, 336, 9, 28, "Input"],
Cell[25813, 805, 299, 7, 48, "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[26173, 819, 160, 2, 64, "Section"],
Cell[26336, 823, 257, 3, 46, "Input"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
