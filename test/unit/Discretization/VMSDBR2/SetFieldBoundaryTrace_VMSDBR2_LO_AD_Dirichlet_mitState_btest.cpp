// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// SetFieldBoundaryTrace_DGBR2_LO_AD_Dirichlet_mitState_btest
// testing of boundary trace lifting operator calculations for DG BR2 with Advection-Diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

//#include "Field/FieldLine_DG_Cell.h"
//#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_EG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_BoundaryTrace.h"

#include "Discretization/VMSD/Discretization_VMSD.h"
#include "Discretization/VMSDBR2/SetFieldBoundaryTrace_VMSD_LiftingOperator.h"
#include "Discretization/VMSDBR2/IntegrandBoundaryTrace_Flux_mitState_VMSD_BR2.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_BoundaryCell.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( SetFieldBoundaryTrace_VMSDBR2_LO_AD_Dirichlet_mitState_btest_suite )

////----------------------------------------------------------------------------//
//BOOST_AUTO_TEST_CASE( DGBR2_2Line_X1Q1R1 )
//{
//  typedef PDEAdvectionDiffusion<PhysD1,
//                                AdvectiveFlux1D_Uniform,
//                                ViscousFlux1D_Uniform,
//                                Source1D_None> PDEAdvectionDiffusion1D;
//  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
//  typedef PDEClass::template ArrayQ<Real> ArrayQ;
//  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
//
//  typedef BCTypeDirichlet_mitState<AdvectiveFlux1D_Uniform,ViscousFlux1D_Uniform> BCTypeDirichlet_mitState1D;
//
//  typedef BCNDConvertSpace<PhysD1, BCAdvectionDiffusion<PhysD1,BCTypeDirichlet_mitState1D> > BCClass;
//  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;
//  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;
//
//  // PDE
//
//  Real u = 1.1;
//  AdvectiveFlux1D_Uniform adv(u);
//
//  Real kxx = 2.123;
//  ViscousFlux1D_Uniform visc(kxx);
//
//  Source1D_None source;
//
//  PDEClass pde( adv, visc, source );
//
//  // BC
//  Real uB = 2.4;
//  BCClass bc(pde, uB);
//
//  // grid: P1 (aka X1)
//  XField1D_1Line_X1_1Group xfld;
//
//  // solution: P1 (aka Q1)
//  int qorder = 1;
//  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
//
//  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 2 );
//
//  // line solution data
//  qfld.DOF(0) = 2;
//  qfld.DOF(1) = 3;
//
//  // lifting-operator: P1 (aka R1)
//  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
//
//  // BR2 discretization
//  Real viscousEtaParameter = 2.0;
//  DiscretizationDGBR2 disc(0, viscousEtaParameter);
//
//  // Compute the field of inverse mass matrices
//  FieldDataInvMassMatrix_Cell mmfld(qfld);
//
//  // integrand applied to the right boundary
//  IntegrandClass fcn( pde, bc, {1}, disc );
//
//  // quadrature rule (no quadrature required: value at a node)
//  std::vector<int> quadratureOrder = {0,0};
//
//  // base interface
//  IntegrateBoundaryTraceGroups<TopoD1>::integrate(SetFieldBoundaryTrace_DGBR2_LiftingOperator(fcn, mmfld),
//                                                  xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());
//
//
//  typedef FieldLift<PhysD1, TopoD1, VectorArrayQ>::FieldCellGroupType<Line> RFieldCellGroupType;
//  typedef RFieldCellGroupType::ElementType<> ElementRFieldClass;
//
//  const RFieldCellGroupType& rfldCell = rfld.getCellGroup<Line>(0);
//
//  ElementRFieldClass rElemL( rfldCell.basis() );
//  ElementRFieldClass rElemR( rfldCell.basis() );
//
//  int elemL = 0;
//  int traceL = 0;
//
//  rfldCell.getElement(rElemL, elemL, traceL);
//
//  const Real small_tol = 1e-12;
//  const Real close_tol = 1e-12;
//
//  // Lifting operators
//  Real rLxTrue[2];
//
//  //Lifting Operator (left)
//  rLxTrue[0] = ( 6/5. );   // Basis function 1
//  rLxTrue[1] = ( -12/5. );   // Basis function 2
//
//  SANS_CHECK_CLOSE( rLxTrue[0], rElemL.DOF(0)[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( rLxTrue[1], rElemL.DOF(1)[0], small_tol, close_tol );
//}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VMSDBR2_2Triangle_X1Q1R1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCTypeDirichlet_mitState<AdvectiveFlux2D_Uniform,ViscousFlux2D_Uniform> BCTypeDirichlet_mitState2D;

  typedef BCNDConvertSpace<PhysD2, BCAdvectionDiffusion<PhysD2,BCTypeDirichlet_mitState2D> > BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, VMSDBR2> IntegrandClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // BC

  Real uB = 2.4;
  BCClass bc(pde, uB);

  // grid: P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical, EmbeddedCGField);


  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 3 );

  // triangle solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 0;
  qfld.DOF(2) = 4;

  // lifting-operator: P1 (aka R1)
//  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  FieldLift_DG_BoundaryTrace<PhysD2, TopoD2, VectorArrayQ> rbfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // BR2 discretization
//  Real viscousEtaParameter = 3.0;
  DiscretizationVMSD disc;

  // Compute the field of inverse mass matrices
  FieldDataInvMassMatrix_Cell mmfld(qfld);

  // integrand applied to the diagonal boundary
  IntegrandClass fcn( pde, bc, {1}, disc );

  // quadrature rule (quadratic: basis & flux both linear)
  std::vector<int> quadratureOrder = {2,2,2};

  IntegrateBoundaryTraceGroups_BoundaryCell<TopoD2>::integrate(SetFieldBoundaryTrace_VMSD_LiftingOperator(fcn, mmfld),
                                                  xfld, qfld, rbfld, quadratureOrder.data(), quadratureOrder.size());

  typedef Field<PhysD2, TopoD2, VectorArrayQ>::FieldCellGroupType<Triangle> RFieldCellGroupType;
  typedef RFieldCellGroupType::ElementType<> ElementRFieldClass;

  const RFieldCellGroupType& rfldCell = rbfld.getCellGroup<Triangle>(1);

  ElementRFieldClass rElemL( rfldCell.basis() );

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  int elem = 0;

  rfldCell.getElement(rElemL, elem);

  Real rLxTrue[3], rLyTrue[3];

  //Lifting Operator (left)
  rLxTrue[0] = ( -12/5. );   // Basis function 1
  rLxTrue[1] = ( 52/5. );   // Basis function 2
  rLxTrue[2] = ( -28/5. );   // Basis function 3
  rLyTrue[0] = ( -12/5. );   // Basis function 1
  rLyTrue[1] = ( 52/5. );   // Basis function 2
  rLyTrue[2] = ( -28/5. );   // Basis function 3

  SANS_CHECK_CLOSE( rLxTrue[0], rElemL.DOF(0)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rLxTrue[1], rElemL.DOF(1)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rLxTrue[2], rElemL.DOF(2)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rLyTrue[0], rElemL.DOF(0)[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rLyTrue[1], rElemL.DOF(1)[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rLyTrue[2], rElemL.DOF(2)[1], small_tol, close_tol );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
