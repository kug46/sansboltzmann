// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandInteriorTrace_DGBR2_AD_btest
// testing of 2-D element residual integrands for DG BR2: Advection-Diffusion on Triangles

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"

#include "Discretization/VMSDBR2/IntegrandTrace_VMSD_BR2.h"
#include "Discretization/VMSD/Discretization_VMSD.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_None> PDEAdvectionDiffusion1D;
typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass1D;
typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldLine;
template class IntegrandTrace_VMSD_BR2<PDEClass1D>::
              BasisWeighted<Real,Real,TopoD0,Node,TopoD1,Line,ElementXFieldLine>;
template class IntegrandTrace_VMSD_BR2<PDEClass1D>::
              BasisWeighted_LO<Real, Real, TopoD0,Node,TopoD1,Line,ElementXFieldLine>;
template class IntegrandTrace_VMSD_BR2<PDEClass1D>::
              FieldWeighted<Real,TopoD0,Node,TopoD1,Line,ElementXFieldLine>;

//typedef PDEAdvectionDiffusion<PhysD2,
//                              AdvectiveFlux2D_Uniform,
//                              ViscousFlux2D_Uniform,
//                              Source2D_None > PDEAdvectionDiffusion2D;
//typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass2D;
//typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldTri;
//template class IntegrandInteriorTrace_DGBR2<PDEClass2D>::
//              BasisWeighted_PDE<Real,Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementXFieldTri,ElementXFieldTri>;
//template class IntegrandInteriorTrace_DGBR2<PDEClass2D>::
//              BasisWeighted_LO<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementXFieldTri,ElementXFieldTri>;
//template class IntegrandInteriorTrace_DGBR2<PDEClass2D>::
//              FieldWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementXFieldTri,ElementXFieldTri>;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandTrace_VMSD_BR2_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_1D_Line_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementRFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef IntegrandTrace_VMSD_BR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO<Real, Real, TopoD0,Node,TopoD1,Line,ElementXFieldCell> BasisWeightedLOClass;


  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemR.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent line grid
  Real x1, x2, x3;

  x1 = 0;
  x2 = 1;
  x3 = 2;

  xfldElemL.DOF(0) = x1;
  xfldElemL.DOF(1) = x2;

  xfldElemR.DOF(0) = x2;
  xfldElemR.DOF(1) = x3;

  xnode.DOF(0) = x2;
  xnode.normalSignL() = 1;

  // solution

  order = 1;
  ElementQFieldCell qfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell qpfldElemL(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, qfldElemL.nDOF() );

  // line solution (left)
  qfldElemL.DOF(0) = 1.8;
  qfldElemL.DOF(1) = 2.7;

  // line solution (right)
  qpfldElemL.DOF(0) = 0.2;
  qpfldElemL.DOF(1) = 0.3;

  // lifting operators

  ElementRFieldCell rfldElemL(order, BasisFunctionCategory_Hierarchical);

  rfldElemL.DOF(0) =  3;  rfldElemL.DOF(1) = 2;

  // BR2 discretization

  // integrand
  DiscretizationVMSD disc;
  IntegrandClass fcnint( pde, disc, {0} );
  BOOST_CHECK( fcnint.N == 1);

//  Real normal = 1;

  // integrand functor
  BasisWeightedPDEClass fcnPDE = fcnint.integrand( xnode, CanonicalTraceToCell(0,0), xnode.normalSignL(),
                                                   xfldElemL, qfldElemL, qpfldElemL, rfldElemL );
  BasisWeightedLOClass fcnLO = fcnint.integrand_LO( xnode, CanonicalTraceToCell(0,0), xnode.normalSignL(),
                                                    xfldElemL, qfldElemL, qpfldElemL, rfldElemL );

  BOOST_CHECK_EQUAL( 1, fcnPDE.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcnPDE.nDOFLeft() );
  BOOST_CHECK( fcnPDE.needsEvaluation() == true );

  BOOST_CHECK_EQUAL( 1, fcnLO.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcnLO.nDOFLeft() );
  BOOST_CHECK( fcnLO.needsEvaluation() == true );

  const int nDOF = 2;
//  const Real small_tol = 1e-13;
//  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
//  Real integrandPDELTrue[nDOF],   integrandPDEpLTrue[nDOF];
//  Real integrandLiftxLTrue[nDOF];
  ArrayQ integrandPDEL[nDOF];
  ArrayQ integrandPDEpL[nDOF];
//  VectorArrayQ integrandLOL[nDOF];

  // Test at sRef={0}, s={1}
  sRef = {0};
  fcnPDE( sRef, integrandPDEL, 2, integrandPDEpL,2 );

  //PDE residual integrands (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
//  integrandPDELTrue[0] = ( 0 ) + ( 0 ) + ( -2123./500. ) + ( 0 );  // Basis function 1
//  integrandPDELTrue[1] = ( 33./10. ) + ( 2123./1000. ) + ( 2123./500. ) + ( -14861./1000. );  // Basis function 2
//
//  //PDE residual integrands (right): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
//  integrandPDEpLTrue[0] = ( -33./10. ) + ( -2123./1000. ) + ( -2123./500. ) + ( 14861./1000. );  // Basis function 1
//  integrandPDEpLTrue[1] = ( 0 ) + ( 0 ) + ( 2123./500. ) + ( 0 );  // Basis function 2
//
//  SANS_CHECK_CLOSE( integrandPDELTrue[0], integrandPDEL[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandPDELTrue[1], integrandPDEL[1], small_tol, close_tol );
//
//  SANS_CHECK_CLOSE( integrandPDELTrue[0], integrandPDEpL[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandPDELTrue[1], integrandPDEpL[1], small_tol, close_tol );
//
//  fcnLO( sRef, integrandLOL, 2 );
//
//  //LO residual integrands (left): (lifting-operator)
//  integrandLiftxLTrue[0] = ( 0 );  // Basis function 1
//  integrandLiftxLTrue[1] = ( 0.3 );  // Basis function 2
//
//  SANS_CHECK_CLOSE( integrandLiftxLTrue[0], integrandLOL[0][0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( integrandLiftxLTrue[1], integrandLOL[1][0], small_tol, close_tol );

////
//
//  // test the trace element integral of the functor
//
//  int quadratureorder = 0;
//  int nIntegrandL = qfldElemL.nDOF();
//  int nIntegrandR = qfldElemR.nDOF();
//  GalerkinWeightedIntegral<TopoD0, Node, ArrayQ,
//                                         ArrayQ> integralPDE(quadratureorder, nIntegrandL, nIntegrandR);
//  GalerkinWeightedIntegral<TopoD0, Node, BasisWeightedLOClass::IntegrandType,
//                                         BasisWeightedLOClass::IntegrandType> integralLO(quadratureorder, nIntegrandL, nIntegrandR);
//
//  ArrayQ rsdPDEElemL[nDOF] = {0,0};
//  ArrayQ rsdPDEElemR[nDOF] = {0,0};
//  BasisWeightedLOClass::IntegrandType rsdLOElemL[nDOF] = {0,0};
//  BasisWeightedLOClass::IntegrandType rsdLOElemR[nDOF] = {0,0};
//  Real rsdPDELTrue[nDOF],rsdPDERTrue[nDOF];
//  Real rsdLiftxLTrue[nDOF], rsdLiftxRTrue[nDOF];
//
//  // cell integration for canonical element
//  integralPDE( fcnPDE, xnode, rsdPDEElemL, nIntegrandL, rsdPDEElemR, nIntegrandR );
//
//  //PDE residuals (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
//  rsdPDELTrue[0] = ( 0 ) + ( 0 ) + ( -2123./500. ) + ( 0 ); // Basis function 1
//  rsdPDELTrue[1] = ( 33./10. ) + ( 2123./1000. ) + ( 2123./500. ) + ( -14861./1000. ); // Basis function 2
//
//  //PDE residuals (right): (advective) + (viscous) + (dual-consistent) + (lifting-operator))
//  rsdPDERTrue[0] = ( -33./10. ) + ( -2123./1000. ) + ( -2123./500. ) + ( 14861./1000. ); // Basis function 1
//  rsdPDERTrue[1] = ( 0 ) + ( 0 ) + ( 2123./500. ) + ( 0 ); // Basis function 2
//
//  SANS_CHECK_CLOSE( rsdPDELTrue[0], rsdPDEElemL[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( rsdPDELTrue[1], rsdPDEElemL[1], small_tol, close_tol );
//
//  SANS_CHECK_CLOSE( rsdPDERTrue[0], rsdPDEElemR[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( rsdPDERTrue[1], rsdPDEElemR[1], small_tol, close_tol );
//
//  // cell integration for canonical element
//  integralLO( fcnLO, xnode, rsdLOElemL, nIntegrandL, rsdLOElemR, nIntegrandR );
//
//  //LO residuals (left): (lifting-operator)
//  rsdLiftxLTrue[0] = ( 0 );   // Basis function 1
//  rsdLiftxLTrue[1] = ( -2 );   // Basis function 2
//
//  //LO residuals (right): (lifting-operator))
//  rsdLiftxRTrue[0] = ( -2 );   // Basis function 1
//  rsdLiftxRTrue[1] = ( 0 );   // Basis function 2
//
//  SANS_CHECK_CLOSE( rsdLiftxLTrue[0], rsdLOElemL[0][0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( rsdLiftxLTrue[1], rsdLOElemL[1][0], small_tol, close_tol );
//
//  SANS_CHECK_CLOSE( rsdLiftxRTrue[0], rsdLOElemR[0][0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( rsdLiftxRTrue[1], rsdLOElemR[1][0], small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_VMSD_1D_Line_Line_Jacobian_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> MatrixQ;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementRFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandTrace_VMSD_BR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,Real,TopoD0,Node,TopoD1,Line,ElementParam> BasisWeightedCoarseClass;
  typedef IntegrandClass::BasisWeighted_LO<Real,Real,TopoD0,Node,TopoD1,Line,ElementParam> BasisWeightedLOClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3, b = 1.4;
  Source1D_UniformGrad source(a, b);

  PDEClass pde( adv, visc, source );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemL.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent triangles grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElemL.DOF(0) = x1;
  xfldElemL.DOF(1) = x2;

  xnode.DOF(0) = x2;
  xnode.normalSignL() = 1;

  // integrand functor

  StabilizationNitsche stab(1, 2.5);

  for (int qorder = 1; qorder <= 4; qorder++)
  {

    ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell qpfldElemL(qorder+1, BasisFunctionCategory_Hierarchical);
    ElementRFieldCell rfldElemL(qorder+1, BasisFunctionCategory_Hierarchical);

    IntegrandClass fcnint( pde, stab, {0} );

    // line solution
    for (int dof = 0; dof < qfldElemL.nDOF();dof++)
    {
      qfldElemL.DOF(dof) = (dof+1)*pow(-1,dof);
    }

    for (int dof = 0; dof < qpfldElemL.nDOF();dof++)
    {
      qpfldElemL.DOF(dof) = (dof+2)*pow(-1,dof+1);
    }

    for (int dof = 0; dof < rfldElemL.nDOF();dof++)
      for (int d=0; d<PhysD1::D; d++)
      {
        rfldElemL.DOF(dof)[d] = d*(dof+2)*pow(-1,dof+1);
      }

    BasisWeightedCoarseClass fcnCoarse = fcnint.integrand( xnode, CanonicalTraceToCell(0,0), 1,
                                                           xfldElemL, qfldElemL, qpfldElemL, rfldElemL);

    const Real small_tol = 1e-11;
    const Real close_tol = 5e-11;

    RefCoordTraceType sRefTrace = 0;

    const int nDOFL = qfldElemL.nDOF();
    const int nDOFpL = qpfldElemL.nDOF();

    DLA::VectorD<ArrayQ> integrand0L(nDOFL), integrand1L(nDOFL);
    DLA::VectorD<ArrayQ> integrandp0L(nDOFpL), integrandp1L(nDOFpL);

    JacobianElemTraceSizeVMSD jacsize(nDOFL, nDOFpL);
    JacobianElemTrace_VMSD_PDE<PhysD1,MatrixQ> mtxElem(jacsize);
    JacobianElemTrace_VMSD_PDE<PhysD1,MatrixQ> mtxElemTrue(jacsize);

    mtxElem = 0; mtxElemTrue = 0;

    // compute jacobians via finite differenec (exact for linear PDE)
    fcnCoarse(sRefTrace, &integrand0L[0], integrand0L.m(), &integrandp0L[0], integrandp0L.m());

    // wrt qL
    for (int i = 0; i < nDOFL; i++)
    {
      qfldElemL.DOF(i) += 1;
      fcnCoarse(sRefTrace, &integrand1L[0], integrand1L.m(), &integrandp1L[0], integrandp1L.m());
      qfldElemL.DOF(i) -= 1;

      for (int j = 0; j < nDOFL; j++)
        mtxElemTrue.PDE_q(j,i) = integrand1L[j] - integrand0L[j];

      for (int j = 0; j < nDOFpL; j++)
        mtxElemTrue.PDEp_q(j,i) = integrandp1L[j] - integrandp0L[j];
    }

    // wrt qpL
    for (int i = 0; i < nDOFpL; i++)
    {
      qpfldElemL.DOF(i) += 1;
      fcnCoarse(sRefTrace, &integrand1L[0], integrand1L.m(), &integrandp1L[0], integrandp1L.m());
      qpfldElemL.DOF(i) -= 1;

      for (int j = 0; j < nDOFL; j++)
        mtxElemTrue.PDE_qp(j,i) = integrand1L[j] - integrand0L[j];

      for (int j = 0; j < nDOFpL; j++)
        mtxElemTrue.PDEp_qp(j,i) = integrandp1L[j] - integrandp0L[j];
    }

    //make up a dr_dqp

    for (int i = 0; i < nDOFpL; i++)
      for (int j = 0; j < nDOFpL; j++)
      {
        mtxElemTrue.r_qp(i,j) = (i+j);
        mtxElem.r_qp(i,j) = (i+j);
      }

    // wrt qpL
    JacobianElemCell_VMSDBR2<PhysD1,MatrixQ> mtxElemTmp(nDOFL, nDOFpL);
    mtxElemTmp = 0;
    for (int d=0; d < PhysD1::D; d++)
    {
      for (int i = 0; i < nDOFL; i++)
      {

        rfldElemL.DOF(i)[d] += 1;
        fcnCoarse(sRefTrace, &integrand1L[0], integrand1L.m(), &integrandp1L[0], integrandp1L.m());
        rfldElemL.DOF(i)[d] -= 1;

        //THIS IS REALLY PDE_R[d]
        for (int j = 0; j < nDOFL; j++)
          mtxElemTmp.PDE_LO(j,i)(0,d) = integrand1L[j] - integrand0L[j];

        //THIS IS REALLY PDEq_R[d]
        for (int j = 0; j < nDOFpL; j++)
          mtxElemTmp.PDEp_LO(j,i)(0,d) = integrandp1L[j] - integrandp0L[j];

      }
    }
    //chain rule on lifting operator
    mtxElemTrue.PDE_qp += mtxElemTmp.PDE_LO*mtxElemTrue.r_qp;
    mtxElemTrue.PDEp_qp += mtxElemTmp.PDEp_LO*mtxElemTrue.r_qp;

    // accumulate the jacobian via Surreal
    fcnCoarse(1., sRefTrace, mtxElem );

    for (int i = 0; i < nDOFL; i++)
      for (int j = 0; j < nDOFL; j++)
        SANS_CHECK_CLOSE( mtxElemTrue.PDE_q(i,j), mtxElem.PDE_q(i,j), small_tol, close_tol );

    for (int i = 0; i < nDOFL; i++)
      for (int j = 0; j < nDOFpL; j++)
        SANS_CHECK_CLOSE( mtxElemTrue.PDE_qp(i,j), mtxElem.PDE_qp(i,j), small_tol, close_tol );

    for (int i = 0; i < nDOFpL; i++)
      for (int j = 0; j < nDOFL; j++)
        SANS_CHECK_CLOSE( mtxElemTrue.PDEp_q(i,j), mtxElem.PDEp_q(i,j), small_tol, close_tol );

    for (int i = 0; i < nDOFpL; i++)
      for (int j = 0; j < nDOFpL; j++)
        SANS_CHECK_CLOSE( mtxElemTrue.PDEp_qp(i,j), mtxElem.PDEp_qp(i,j), small_tol, close_tol );


    //TEST LIFTING OPERATOR

    BasisWeightedLOClass fcnLO = fcnint.integrand_LO( xnode, CanonicalTraceToCell(0,0), 1,
                                                   xfldElemL, qfldElemL, qpfldElemL, rfldElemL);

    JacobianElemTrace_VMSD_LO<PhysD1> mtxLO(jacsize);
    JacobianElemTrace_VMSD_LO<PhysD1> mtxLOtrue(jacsize);

    mtxLO = 0; mtxLOtrue = 0;

    // wrt qpL
    DLA::VectorD<VectorArrayQ> integrandp0LO(nDOFpL), integrandp1LO(nDOFpL);
    integrandp0LO = 0;

    fcnLO(sRefTrace, &integrandp0LO[0], integrandp0LO.m());

    integrandp1LO = 0;
    for (int i = 0; i < nDOFpL; i++)
    {
      qpfldElemL.DOF(i) += 1;
      fcnLO(sRefTrace, &integrandp1LO[0], integrandp1LO.m());
      qpfldElemL.DOF(i) -= 1;

      for (int j = 0; j < nDOFL; j++)
        mtxLOtrue._qp(j,i) = integrandp1LO[j] - integrandp0LO[j];
    }

    // accumulate the jacobian via Surreal
    fcnLO(1., sRefTrace, mtxLO );

    for (int i = 0; i < nDOFL; i++)
      for (int j = 0; j < nDOFpL; j++)
        for (int d = 0; d < PhysD1::D; d++)
          SANS_CHECK_CLOSE( mtxLOtrue._qp(i,j)[d], mtxLO._qp(i,j)[d], small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_VMSD_2D_Triangle_Jacobian_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> MatrixQ;
  typedef DLA::VectorS<2,ArrayQ> VectorArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> CellXFieldClass;
  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementRFieldCell;
//  typedef ElementXFieldCell::RefCoordType RefCoordType;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandTrace_VMSD_BR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,Real,TopoD1,Line,TopoD2,Triangle,ElementParam> BasisWeightedCoarseClass;
  typedef IntegrandClass::BasisWeighted_LO<Real,Real,TopoD1,Line,TopoD2,Triangle,ElementParam> BasisWeightedLOClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Real s0 = 0.25, sx = 0.6, sy = -1.5;
  Source2D_UniformGrad source(s0, sx, sy);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid

  int order = 1;
  CellXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);
  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  DiscretizationVMSD stab;

  for (int qorder = 1; qorder <= 4; qorder++)
  {

    ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell qpfldElemL(qorder+1, BasisFunctionCategory_Hierarchical);
    ElementRFieldCell rfldElemL(qorder+1, BasisFunctionCategory_Hierarchical);

    IntegrandClass fcnint( pde, stab, {0} );

    // line solution
    for (int dof = 0; dof < qfldElemL.nDOF();dof++)
    {
      qfldElemL.DOF(dof) = (dof+1)*pow(-1,dof);
    }

    for (int dof = 0; dof < qpfldElemL.nDOF();dof++)
    {
      qpfldElemL.DOF(dof) = (dof+2)*pow(-1,dof+1);
    }

    for (int dof = 0; dof < rfldElemL.nDOF();dof++)
      for (int d=0; d < PhysD2::D; d++)
      {
        rfldElemL.DOF(dof)[d] = d*(dof+2)*pow(-1,dof+1);
      }

    BasisWeightedCoarseClass fcnCoarse = fcnint.integrand( xedge, CanonicalTraceToCell(0,1), 1,
                                                           xfldElem, qfldElemL, qpfldElemL, rfldElemL);

    const Real small_tol = 1e-11;
    const Real close_tol = 5e-11;

    RefCoordTraceType sRefTrace = 0;

    const int nDOFL = qfldElemL.nDOF();
    const int nDOFpL = qpfldElemL.nDOF();

    DLA::VectorD<ArrayQ> integrand0L(nDOFL), integrand1L(nDOFL);
    DLA::VectorD<ArrayQ> integrandp0L(nDOFpL), integrandp1L(nDOFpL);

    JacobianElemTraceSizeVMSD jacsize(nDOFL, nDOFpL);
    JacobianElemTrace_VMSD_PDE<PhysD2,MatrixQ> mtxElem(jacsize);
    JacobianElemTrace_VMSD_PDE<PhysD2,MatrixQ> mtxElemTrue(jacsize);

    mtxElem = 0; mtxElemTrue = 0;

    // compute jacobians via finite differenec (exact for linear PDE)
    fcnCoarse(sRefTrace, &integrand0L[0], integrand0L.m(), &integrandp0L[0], integrandp0L.m());

    // wrt qL
    for (int i = 0; i < nDOFL; i++)
    {
      qfldElemL.DOF(i) += 1;
      fcnCoarse(sRefTrace, &integrand1L[0], integrand1L.m(), &integrandp1L[0], integrandp1L.m());
      qfldElemL.DOF(i) -= 1;

      for (int j = 0; j < nDOFL; j++)
        mtxElemTrue.PDE_q(j,i) = integrand1L[j] - integrand0L[j];

      for (int j = 0; j < nDOFpL; j++)
        mtxElemTrue.PDEp_q(j,i) = integrandp1L[j] - integrandp0L[j];
    }

    // wrt qpL
    for (int i = 0; i < nDOFpL; i++)
    {
      qpfldElemL.DOF(i) += 1;
      fcnCoarse(sRefTrace, &integrand1L[0], integrand1L.m(), &integrandp1L[0], integrandp1L.m());
      qpfldElemL.DOF(i) -= 1;

      for (int j = 0; j < nDOFL; j++)
        mtxElemTrue.PDE_qp(j,i) = integrand1L[j] - integrand0L[j];

      for (int j = 0; j < nDOFpL; j++)
        mtxElemTrue.PDEp_qp(j,i) = integrandp1L[j] - integrandp0L[j];
    }

    //make up a dr_dqp

    for (int i = 0; i < nDOFpL; i++)
      for (int j = 0; j < nDOFpL; j++)
      {
        mtxElemTrue.r_qp(i,j) = (i+j);
        mtxElem.r_qp(i,j) = (i+j);
      }

    // wrt qpL
    JacobianElemCell_VMSDBR2<PhysD2,MatrixQ> mtxElemTmp(nDOFL, nDOFpL);
    mtxElemTmp = 0;
    for (int d=0; d < PhysD2::D; d++)
    {
      for (int i = 0; i < nDOFL; i++)
      {

        rfldElemL.DOF(i)[d] += 1;
        fcnCoarse(sRefTrace, &integrand1L[0], integrand1L.m(), &integrandp1L[0], integrandp1L.m());
        rfldElemL.DOF(i)[d] -= 1;

        //THIS IS REALLY PDE_R[d]
        for (int j = 0; j < nDOFL; j++)
          mtxElemTmp.PDE_LO(j,i)(0,d) = integrand1L[j] - integrand0L[j];

        //THIS IS REALLY PDEq_R[d]
        for (int j = 0; j < nDOFpL; j++)
          mtxElemTmp.PDEp_LO(j,i)(0,d) = integrandp1L[j] - integrandp0L[j];

      }
    }
    //chain rule on lifting operator
    mtxElemTrue.PDE_qp += mtxElemTmp.PDE_LO*mtxElemTrue.r_qp;
    mtxElemTrue.PDEp_qp += mtxElemTmp.PDEp_LO*mtxElemTrue.r_qp;

    // accumulate the jacobian via Surreal
    fcnCoarse(1., sRefTrace, mtxElem );

    for (int i = 0; i < nDOFL; i++)
      for (int j = 0; j < nDOFL; j++)
        SANS_CHECK_CLOSE( mtxElemTrue.PDE_q(i,j), mtxElem.PDE_q(i,j), small_tol, close_tol );

    for (int i = 0; i < nDOFL; i++)
      for (int j = 0; j < nDOFpL; j++)
        SANS_CHECK_CLOSE( mtxElemTrue.PDE_qp(i,j), mtxElem.PDE_qp(i,j), small_tol, close_tol );

    for (int i = 0; i < nDOFpL; i++)
      for (int j = 0; j < nDOFL; j++)
        SANS_CHECK_CLOSE( mtxElemTrue.PDEp_q(i,j), mtxElem.PDEp_q(i,j), small_tol, close_tol );

    for (int i = 0; i < nDOFpL; i++)
      for (int j = 0; j < nDOFpL; j++)
        SANS_CHECK_CLOSE( mtxElemTrue.PDEp_qp(i,j), mtxElem.PDEp_qp(i,j), small_tol, close_tol );


    //TEST LIFTING OPERATOR

    BasisWeightedLOClass fcnLO = fcnint.integrand_LO( xedge, CanonicalTraceToCell(0,1), 1,
                                                   xfldElem, qfldElemL, qpfldElemL, rfldElemL);

    JacobianElemTrace_VMSD_LO<PhysD2> mtxLO(jacsize);
    JacobianElemTrace_VMSD_LO<PhysD2> mtxLOtrue(jacsize);

    mtxLO = 0; mtxLOtrue = 0;

    // wrt qpL
    DLA::VectorD<VectorArrayQ> integrandp0LO(nDOFpL), integrandp1LO(nDOFpL);
    integrandp0LO = 0;

    fcnLO(sRefTrace, &integrandp0LO[0], integrandp0LO.m());

    integrandp1LO = 0;
    for (int i = 0; i < nDOFpL; i++)
    {
      qpfldElemL.DOF(i) += 1;
      fcnLO(sRefTrace, &integrandp1LO[0], integrandp1LO.m());
      qpfldElemL.DOF(i) -= 1;

      for (int j = 0; j < nDOFL; j++)
        mtxLOtrue._qp(j,i) = integrandp1LO[j] - integrandp0LO[j];
    }

    // accumulate the jacobian via Surreal
    fcnLO(1., sRefTrace, mtxLO );

    for (int i = 0; i < nDOFL; i++)
      for (int j = 0; j < nDOFpL; j++)
        for (int d = 0; d < PhysD2::D; d++)
          SANS_CHECK_CLOSE( mtxLOtrue._qp(i,j)[d], mtxLO._qp(i,j)[d], small_tol, close_tol );
  }
}

#if 0

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_1D_Line_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementRFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD0,Node,TopoD1,Line,Line,ElementParam,ElementParam> FieldWeightedClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemR.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent line grid
  Real x1, x2, x3;

  x1 = 0;
  x2 = 1;
  x3 = 2;

  xfldElemL.DOF(0) = x1;
  xfldElemL.DOF(1) = x2;

  xfldElemR.DOF(0) = x2;
  xfldElemR.DOF(1) = x3;

  xnode.DOF(0) = x2;
  xnode.normalSignL() = 1;

  // solution

  order = 1;
  ElementQFieldCell qfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell qfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, qfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, qfldElemR.nDOF() );

  // line solution (left)
  qfldElemL.DOF(0) = 2;
  qfldElemL.DOF(1) = 3;

  // line solution (right)
  qfldElemR.DOF(0) = 7;
  qfldElemR.DOF(1) = 4;

  // lifting operators

  ElementRFieldCell rfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementRFieldCell rfldElemR(order, BasisFunctionCategory_Hierarchical);

  rfldElemL.DOF(0) =  3;  rfldElemL.DOF(1) = 2;
  rfldElemR.DOF(0) =  5;  rfldElemR.DOF(1) = 4;

  // weighting
  ElementQFieldCell wfldElemL(order+1, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell wfldElemR(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, wfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 2, wfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, wfldElemR.nDOF() );

  // line solution (left)
  wfldElemL.DOF(0) = 3;
  wfldElemL.DOF(1) = 4;
  wfldElemL.DOF(2) = 5;

  // line solution (right)
  wfldElemR.DOF(0) = 5;
  wfldElemR.DOF(1) = 4;
  wfldElemR.DOF(2) =-1;

  // lifting operators

  ElementRFieldCell sfldElemL(order+1, BasisFunctionCategory_Hierarchical);
  ElementRFieldCell sfldElemR(order+1, BasisFunctionCategory_Hierarchical);

  sfldElemL.DOF(0) =  2;  sfldElemL.DOF(1) = 9;  sfldElemL.DOF(2) = 3;
  sfldElemR.DOF(0) =  3;  sfldElemR.DOF(1) = 2;  sfldElemR.DOF(2) = 4;

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  FieldWeightedClass fcn = fcnint.integrand( xnode, CanonicalTraceToCell(0,0), CanonicalTraceToCell(1,0),
                                             xfldElemL,
                                             qfldElemL, rfldElemL,
                                             wfldElemL, sfldElemL,
                                             xfldElemR,
                                             qfldElemR, rfldElemR,
                                             wfldElemR, sfldElemR );

  BOOST_CHECK( fcnint.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOFLeft() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOFRight() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandPDELTrue,   integrandPDERTrue;
  Real integrandLiftLTrue, integrandLiftRTrue;
  FieldWeightedClass::IntegrandType integrandL;
  FieldWeightedClass::IntegrandType integrandR;

  // Test at sRef={0}, s={1}
  sRef = {0};
  fcn( sRef, integrandL, integrandR );

  //PDE residual integrands (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  integrandPDELTrue = ( 66./5. ) + ( 2123./250. ) + ( -40337./500. ) + ( -14861./250. );  // Weight function
  integrandPDERTrue = ( -33./2. ) + ( -2123./200. ) + ( -2123./100. ) + ( 14861./200. );  // Weight function

  SANS_CHECK_CLOSE( integrandPDELTrue, integrandL.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue, integrandR.PDE, small_tol, close_tol );

  //LO residual integrands: (lifting)
  integrandLiftLTrue = ( -18 );   // Weight function
  integrandLiftRTrue = ( -6 );   // Weight function

  SANS_CHECK_CLOSE( integrandLiftLTrue, integrandL.Lift, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftRTrue, integrandR.Lift, small_tol, close_tol );

  // test the trace element integral of the functor

  int quadratureorder = 0;
  ElementIntegral<TopoD0, Node, FieldWeightedClass::IntegrandType,
                                FieldWeightedClass::IntegrandType> integral(quadratureorder);

  FieldWeightedClass::IntegrandType rsdElemL=0;
  FieldWeightedClass::IntegrandType rsdElemR=0;
  Real rsdPDELTrue,rsdPDERTrue;
  Real rsdLiftL,rsdLiftR;

  // cell integration for canonical element
  integral( fcn, xnode, rsdElemL, rsdElemR );

  //PDE residuals (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  rsdPDELTrue = ( 66./5. ) + ( 2123./250. ) + ( -40337./500. ) + ( -14861./250. ); // Weight function
  rsdPDERTrue = ( -33./2. ) + ( -2123./200. ) + ( -2123./100. ) + ( 14861./200. ); // Weight function

  SANS_CHECK_CLOSE( rsdPDELTrue, rsdElemL.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue, rsdElemR.PDE, small_tol, close_tol );

  //PDE residual: (lifting)
  rsdLiftL = ( -18 );   // Basis function
  rsdLiftR = ( -6 );   // Basis function

  SANS_CHECK_CLOSE( rsdLiftL, rsdElemL.Lift, small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftR, rsdElemR.Lift, small_tol, close_tol );

}
#endif

#if 1


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_1D_Line_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementRFieldCell;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandTrace_VMSD_BR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO<Real, Real, TopoD0,Node,TopoD1,Line,ElementXFieldCell> BasisWeightedLOClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD0,Node,TopoD1,Line,ElementParam> FieldWeightedClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3;
  Source1D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemR.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent line grid
  Real x1, x2, x3;

  x1 = 0;
  x2 = 1;
  x3 = 2;

  xfldElemL.DOF(0) = x1;
  xfldElemL.DOF(1) = x2;

  xfldElemR.DOF(0) = x2;
  xfldElemR.DOF(1) = x3;

  xnode.DOF(0) = x2;
  xnode.normalSignL() = 1;

  for (int qorder = 1; qorder< 4; qorder++)
  {
    // solution
    ElementQFieldCell efldElemL(0, BasisFunctionCategory_Legendre);
    ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell qpfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementRFieldCell rfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wpfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementRFieldCell sfldElemL(qorder, BasisFunctionCategory_Hierarchical);

    // line solution
    for ( int dof = 0; dof < qfldElemL.nDOF(); dof ++ )
    {
      qfldElemL.DOF(dof) = 1+(dof+1)*pow(-1,dof);
      qpfldElemL.DOF(dof) = 2+(dof+2)*pow(-1,dof+1);
      rfldElemL.DOF(dof) = 3+(dof+1)*pow(-1,dof);
      wfldElemL.DOF(dof) = 0;
      wpfldElemL.DOF(dof) = 0;
      sfldElemL.DOF(dof) = 0;
    }

    // BR2 discretization
    DiscretizationVMSD disc;

    // integrand
    IntegrandClass fcnint( pde, disc, {0} );

    // integrand functor
    BasisWeightedPDEClass fcnPDEB = fcnint.integrand( xnode, CanonicalTraceToCell(0,0),  1,
                                                          xfldElemL,
                                                          qfldElemL, qpfldElemL, rfldElemL);

    BasisWeightedLOClass fcnLOB = fcnint.integrand_LO( xnode, CanonicalTraceToCell(0,0),  1,
                                                       xfldElemL,
                                                       qfldElemL,
                                                       qpfldElemL, rfldElemL );

    // integrand functor
    FieldWeightedClass fcnW = fcnint.integrand( xnode, CanonicalTraceToCell(0,0),  1,
                                               xfldElemL,
                                               qfldElemL, qpfldElemL, rfldElemL,
                                               wfldElemL, wpfldElemL, sfldElemL, efldElemL );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;
    const int nIntegrandL = qfldElemL.nDOF();

    int quadratureorder = 0;
    GalerkinWeightedIntegral<TopoD0, Node, ArrayQ,
                                           ArrayQ> integralPDEB(quadratureorder, nIntegrandL, nIntegrandL);
    GalerkinWeightedIntegral<TopoD0, Node, VectorArrayQ> integralLOB(quadratureorder, nIntegrandL);

    GalerkinWeightedIntegral<TopoD0, Node, Real> integralW(quadratureorder,1);

    std::vector<ArrayQ> rsdPDEElemBL(nIntegrandL, 0);
    std::vector<ArrayQ> rsdPDEElemBP(nIntegrandL, 0);
    std::vector<VectorArrayQ> rsdLOElemBL(nIntegrandL, 0);
    DLA::VectorD<Real> estPDEElemL( 1 );

    // cell integration for canonical element
    integralPDEB( fcnPDEB, xnode, rsdPDEElemBL.data(), nIntegrandL, rsdPDEElemBP.data(), nIntegrandL );
    integralLOB ( fcnLOB,  xnode, rsdLOElemBL.data(),  nIntegrandL );

    estPDEElemL = 0;
    for (int i = 0; i < wfldElemL.nDOF(); i++ )
    {
      // set just one of the weights to one
      wfldElemL.DOF(i) = 1;
//      sfldElemL.DOF(i) = 1;
      //wpfldElemL.DOF(i) = 1;

      // cell integration for canonical element
      integralW(fcnW, xnode, estPDEElemL.data(), 1 );

      // test the two integrands are the same
      SANS_CHECK_CLOSE (rsdPDEElemBL[i], estPDEElemL[0], small_tol, close_tol );


      // reset to 0
      estPDEElemL = 0;
      wfldElemL.DOF(i) = 0;
    }

    for (int i = 0; i < wpfldElemL.nDOF(); i++ )
    {
      // set just one of the weights to one
      wpfldElemL.DOF(i) = 1;
//      sfldElemL.DOF(i) = 1;
      //wpfldElemL.DOF(i) = 1;

      // cell integration for canonical element
      integralW(fcnW, xnode, estPDEElemL.data(), 1 );

      // test the two integrands are the same
      SANS_CHECK_CLOSE (rsdPDEElemBP[i], estPDEElemL[0], small_tol, close_tol );


      // reset to 0
      estPDEElemL = 0;
      wpfldElemL.DOF(i) = 0;
    }

    for (int i = 0; i < sfldElemL.nDOF(); i++ )
    {
      // set just one of the weights to one
      sfldElemL.DOF(i)[0] = 1;

      // cell integration for canonical element
      integralW(fcnW, xnode, estPDEElemL.data(), 1 );

      // test the two integrands are the same
      SANS_CHECK_CLOSE (rsdLOElemBL[i][0], estPDEElemL[0], small_tol, close_tol );


      // reset to 0
      estPDEElemL = 0;
      sfldElemL.DOF(i)[0] = 0;
    }

  }



}

#endif
#if 0

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_2D_Triangle_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<DLA::VectorS<2,ArrayQ>,TopoD2,Triangle> ElementRFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;

  typedef IntegrandClass::BasisWeighted_PDE<Real,Real,TopoD1,Line,
                                            TopoD2,Triangle,Triangle,ElementXFieldCell,ElementXFieldCell> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO<Real, TopoD1, Line, TopoD2, Triangle, Triangle,ElementXFieldCell,ElementXFieldCell> BasisWeightedLOClass;


  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123, kxy = 0.553;
  Real kyx = 0.789, kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy,
                             kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemR.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;
  x4 = 1;  y4 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xfldElemR.DOF(0) = {x4, y4};
  xfldElemR.DOF(1) = {x3, y3};
  xfldElemR.DOF(2) = {x2, y2};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution

  order = 1;
  ElementQFieldCell qfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell qfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemR.nDOF() );

  // triangle solution (left)
  qfldElemL.DOF(0) = 1;
  qfldElemL.DOF(1) = 3;
  qfldElemL.DOF(2) = 4;

  // triangle solution (right)
  qfldElemR.DOF(0) = 7;
  qfldElemR.DOF(1) = 2;
  qfldElemR.DOF(2) = 9;

  // lifting operators

  ElementRFieldCell rfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementRFieldCell rfldElemR(order, BasisFunctionCategory_Hierarchical);

  rfldElemL.DOF(0) = { 2, -3};  rfldElemL.DOF(1) = { 7,  8};  rfldElemL.DOF(2) = {-1,  7};
  rfldElemR.DOF(0) = { 8, -2};  rfldElemR.DOF(1) = {-5,  7};  rfldElemR.DOF(2) = { 3,  9};

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  BasisWeightedPDEClass fcnPDE = fcnint.integrand_PDE( xedge, CanonicalTraceToCell(0, 1), CanonicalTraceToCell(0, -1),
                                                       xfldElemL, qfldElemL, rfldElemL,
                                                       xfldElemR, qfldElemR, rfldElemR );
  BasisWeightedLOClass fcnLO = fcnint.integrand_LO( xedge, CanonicalTraceToCell(0, 1), CanonicalTraceToCell(0, -1),
                                                    xfldElemL, qfldElemL,
                                                    xfldElemR, qfldElemR );


  BOOST_CHECK_EQUAL( 1, fcnPDE.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcnPDE.nDOFLeft() );
  BOOST_CHECK_EQUAL( 3, fcnPDE.nDOFRight() );
  BOOST_CHECK( fcnPDE.needsEvaluation() == true );

  const int nDOF = 3;
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandLiftxLTrue[nDOF], integrandLiftyLTrue[nDOF];
  Real integrandLiftxRTrue[nDOF], integrandLiftyRTrue[nDOF];
  Real integrandPDELTrue[nDOF];
  Real integrandPDERTrue[nDOF];
  ArrayQ integrandPDEL[nDOF];
  ArrayQ integrandPDER[nDOF];
  BasisWeightedLOClass::IntegrandType integrandLOL[nDOF];
  BasisWeightedLOClass::IntegrandType integrandLOR[nDOF];

  sRef = 0;
  fcnPDE( sRef, integrandPDEL, 3, integrandPDER, 3 );

  //PDE residual integrands (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  integrandPDELTrue[0] = ( 0 ) + ( 0 ) + ( -1677/125./sqrt(2) ) + ( 0 );  // Basis function 1
  integrandPDELTrue[1] = ( 39/10./sqrt(2) ) + ( -2743/250./sqrt(2) ) + ( 2007/250./sqrt(2) ) + ( -4173/25./sqrt(2) );  // Basis function 2
  integrandPDELTrue[2] = ( 0 ) + ( 0 ) + ( 1347/250./sqrt(2) ) + ( 0 );  // Basis function 3

  //PDE residual integrands (right): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  integrandPDERTrue[0] = ( 0 ) + ( 0 ) + ( 1677/125./sqrt(2) ) + ( 0 );  // Basis function 1
  integrandPDERTrue[1] = ( 0 ) + ( 0 ) + ( -2007/250./sqrt(2) ) + ( 0 );  // Basis function 2
  integrandPDERTrue[2] = ( -39/10./sqrt(2) ) + ( 2743/250./sqrt(2) ) + ( -1347/250./sqrt(2) ) + ( 4173/25./sqrt(2) );  // Basis function 3

  SANS_CHECK_CLOSE( integrandPDELTrue[0], integrandPDEL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDELTrue[1], integrandPDEL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDELTrue[2], integrandPDEL[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDERTrue[0], integrandPDER[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue[1], integrandPDER[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue[2], integrandPDER[2], small_tol, close_tol );

  fcnLO( sRef, integrandLOL, 3, integrandLOR, 3);

  //LO residual integrands (left): (lifting-operator)
  integrandLiftxLTrue[0] = ( 0 );  // Basis function 1
  integrandLiftyLTrue[0] = ( 0 );  // Basis function 1
  integrandLiftxLTrue[1] = ( -3/sqrt(2) );  // Basis function 2
  integrandLiftyLTrue[1] = ( -3/sqrt(2) );  // Basis function 2
  integrandLiftxLTrue[2] = ( 0 );  // Basis function 3
  integrandLiftyLTrue[2] = ( 0 );  // Basis function 3

  //LO residual integrands (right): (lifting-operator)
  integrandLiftxRTrue[0] = ( 0 );  // Basis function 1
  integrandLiftyRTrue[0] = ( 0 );  // Basis function 1
  integrandLiftxRTrue[1] = ( 0 );  // Basis function 2
  integrandLiftyRTrue[1] = ( 0 );  // Basis function 2
  integrandLiftxRTrue[2] = ( -3/sqrt(2) );  // Basis function 3
  integrandLiftyRTrue[2] = ( -3/sqrt(2) );  // Basis function 3

  SANS_CHECK_CLOSE( integrandLiftxLTrue[0], integrandLOL[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyLTrue[0], integrandLOL[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxLTrue[1], integrandLOL[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyLTrue[1], integrandLOL[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxLTrue[2], integrandLOL[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyLTrue[2], integrandLOL[2][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLiftxRTrue[0], integrandLOR[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyRTrue[0], integrandLOR[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxRTrue[1], integrandLOR[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyRTrue[1], integrandLOR[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxRTrue[2], integrandLOR[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyRTrue[2], integrandLOR[2][1], small_tol, close_tol );


  sRef = 1;
  fcnPDE( sRef, integrandPDEL, 3, integrandPDER, 3 );

  //PDE residual integrands (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  integrandPDELTrue[0] = ( 0 ) + ( 0 ) + ( 559/125./sqrt(2) ) + ( 0 );  // Basis function 1
  integrandPDELTrue[1] = ( 0 ) + ( 0 ) + ( -669/250./sqrt(2) ) + ( 0 );  // Basis function 2
  integrandPDELTrue[2] = ( (13/5.)*(sqrt(2)) ) + ( -2743/250./sqrt(2) ) + ( -449/250./sqrt(2) ) + ( (-819/125.)*(sqrt(2)) );  // Basis function 3

  //PDE residual integrands (right): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  integrandPDERTrue[0] = ( 0 ) + ( 0 ) + ( -559/125./sqrt(2) ) + ( 0 );  // Basis function 1
  integrandPDERTrue[1] = ( (-13/5.)*(sqrt(2)) ) + ( 2743/250./sqrt(2) ) + ( 669/250./sqrt(2) ) + ( (819/125.)*(sqrt(2)) );  // Basis function 2
  integrandPDERTrue[2] = ( 0 ) + ( 0 ) + ( 449/250./sqrt(2) ) + ( 0 );  // Basis function 3

  SANS_CHECK_CLOSE( integrandPDELTrue[0], integrandPDEL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDELTrue[1], integrandPDEL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDELTrue[2], integrandPDEL[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDERTrue[0], integrandPDER[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue[1], integrandPDER[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue[2], integrandPDER[2], small_tol, close_tol );

  fcnLO( sRef, integrandLOL, 3, integrandLOR, 3);

  //LO residual integrands (left): (lifting-operator)
  integrandLiftxLTrue[0] = ( 0 );  // Basis function 1
  integrandLiftyLTrue[0] = ( 0 );  // Basis function 1
  integrandLiftxLTrue[1] = ( 0 );  // Basis function 2
  integrandLiftyLTrue[1] = ( 0 );  // Basis function 2
  integrandLiftxLTrue[2] = ( 1/sqrt(2) );  // Basis function 3
  integrandLiftyLTrue[2] = ( 1/sqrt(2) );  // Basis function 3

  //LO residual integrands (right): (lifting-operator)
  integrandLiftxRTrue[0] = ( 0 );  // Basis function 1
  integrandLiftyRTrue[0] = ( 0 );  // Basis function 1
  integrandLiftxRTrue[1] = ( 1/sqrt(2) );  // Basis function 2
  integrandLiftyRTrue[1] = ( 1/sqrt(2) );  // Basis function 2
  integrandLiftxRTrue[2] = ( 0 );  // Basis function 3
  integrandLiftyRTrue[2] = ( 0 );  // Basis function 3

  SANS_CHECK_CLOSE( integrandLiftxLTrue[0], integrandLOL[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyLTrue[0], integrandLOL[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxLTrue[1], integrandLOL[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyLTrue[1], integrandLOL[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxLTrue[2], integrandLOL[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyLTrue[2], integrandLOL[2][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLiftxRTrue[0], integrandLOR[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyRTrue[0], integrandLOR[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxRTrue[1], integrandLOR[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyRTrue[1], integrandLOR[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxRTrue[2], integrandLOR[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyRTrue[2], integrandLOR[2][1], small_tol, close_tol );


  sRef = 0.5;
  fcnPDE( sRef, integrandPDEL, 3, integrandPDER, 3 );

  //PDE residual integrands (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  integrandPDELTrue[0] = ( 0 ) + ( 0 ) + ( -559/125./sqrt(2) ) + ( 0 );  // Basis function 1
  integrandPDELTrue[1] = ( 91/40./sqrt(2) ) + ( -2743/500./sqrt(2) ) + ( 669/250./sqrt(2) ) + ( -22503/500./sqrt(2) );  // Basis function 2
  integrandPDELTrue[2] = ( 91/40./sqrt(2) ) + ( -2743/500./sqrt(2) ) + ( 449/250./sqrt(2) ) + ( -22503/500./sqrt(2) );  // Basis function 3

  //PDE residual integrands (right): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  integrandPDERTrue[0] = ( 0 ) + ( 0 ) + ( 559/125./sqrt(2) ) + ( 0 );  // Basis function 1
  integrandPDERTrue[1] = ( -91/40./sqrt(2) ) + ( 2743/500./sqrt(2) ) + ( -669/250./sqrt(2) ) + ( 22503/500./sqrt(2) );  // Basis function 2
  integrandPDERTrue[2] = ( -91/40./sqrt(2) ) + ( 2743/500./sqrt(2) ) + ( -449/250./sqrt(2) ) + ( 22503/500./sqrt(2) );  // Basis function 3

  SANS_CHECK_CLOSE( integrandPDELTrue[0], integrandPDEL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDELTrue[1], integrandPDEL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDELTrue[2], integrandPDEL[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDERTrue[0], integrandPDER[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue[1], integrandPDER[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue[2], integrandPDER[2], small_tol, close_tol );

  fcnLO( sRef, integrandLOL, 3, integrandLOR, 3);

  //LO residual integrands (left): (lifting-operator)
  integrandLiftxLTrue[0] = ( 0 );  // Basis function 1
  integrandLiftyLTrue[0] = ( 0 );  // Basis function 1
  integrandLiftxLTrue[1] = ( -1/(2.*sqrt(2)) );  // Basis function 2
  integrandLiftyLTrue[1] = ( -1/(2.*sqrt(2)) );  // Basis function 2
  integrandLiftxLTrue[2] = ( -1/(2.*sqrt(2)) );  // Basis function 3
  integrandLiftyLTrue[2] = ( -1/(2.*sqrt(2)) );  // Basis function 3

  //LO residual integrands (right): (lifting-operator)
  integrandLiftxRTrue[0] = ( 0 );  // Basis function 1
  integrandLiftyRTrue[0] = ( 0 );  // Basis function 1
  integrandLiftxRTrue[1] = ( -1/(2.*sqrt(2)) );  // Basis function 2
  integrandLiftyRTrue[1] = ( -1/(2.*sqrt(2)) );  // Basis function 2
  integrandLiftxRTrue[2] = ( -1/(2.*sqrt(2)) );  // Basis function 3
  integrandLiftyRTrue[2] = ( -1/(2.*sqrt(2)) );  // Basis function 3

  SANS_CHECK_CLOSE( integrandLiftxLTrue[0], integrandLOL[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyLTrue[0], integrandLOL[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxLTrue[1], integrandLOL[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyLTrue[1], integrandLOL[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxLTrue[2], integrandLOL[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyLTrue[2], integrandLOL[2][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLiftxRTrue[0], integrandLOR[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyRTrue[0], integrandLOR[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxRTrue[1], integrandLOR[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyRTrue[1], integrandLOR[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxRTrue[2], integrandLOR[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyRTrue[2], integrandLOR[2][1], small_tol, close_tol );


  // test the trace element integral of the functor

  // quadrature rule: quadratic (basis & flux both linear)
  int quadratureorder = 2;

  int nIntegrandL = qfldElemL.nDOF();
  int nIntegrandR = qfldElemR.nDOF();
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ,
                                         ArrayQ> integralPDE(quadratureorder, nIntegrandL, nIntegrandR);
  GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedLOClass::IntegrandType,
                                         BasisWeightedLOClass::IntegrandType> integralLO(quadratureorder, nIntegrandL, nIntegrandR);

  ArrayQ rsdPDEElemL[nDOF] = {0,0,0};
  ArrayQ rsdPDEElemR[nDOF] = {0,0,0};
  BasisWeightedLOClass::IntegrandType rsdLOElemL[nDOF] = {0,0,0};
  BasisWeightedLOClass::IntegrandType rsdLOElemR[nDOF] = {0,0,0};
  Real rsdPDELTrue[nDOF],rsdPDERTrue[nDOF];
  Real rsdLiftxLTrue[nDOF], rsdLiftyLTrue[nDOF];
  Real rsdLiftxRTrue[nDOF], rsdLiftyRTrue[nDOF];

  // cell integration for canonical element
  integralPDE( fcnPDE, xedge, rsdPDEElemL, nIntegrandL, rsdPDEElemR, nIntegrandR );

  //PDE residuals (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  rsdPDELTrue[0] = ( 0 ) + ( 0 ) + ( -559/125. ) + ( 0 );   // Basis function 1
  rsdPDELTrue[1] = ( 13/6. ) + ( -2743/500. ) + ( 669/250. ) + ( -7228/125. );   // Basis function 2
  rsdPDELTrue[2] = ( 143/60. ) + ( -2743/500. ) + ( 449/250. ) + ( -8047/250. );   // Basis function 3

  //PDE residuals (right): (advective) + (viscous) + (dual-consistent) + (lifting-operator))
  rsdPDERTrue[0] = ( 0 ) + ( 0 ) + ( 559/125. ) + ( 0 );   // Basis function 1
  rsdPDERTrue[1] = ( -143/60. ) + ( 2743/500. ) + ( -669/250. ) + ( 8047/250. );   // Basis function 2
  rsdPDERTrue[2] = ( -13/6. ) + ( 2743/500. ) + ( -449/250. ) + ( 7228/125. );   // Basis function 3

  SANS_CHECK_CLOSE( rsdPDELTrue[0], rsdPDEElemL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDELTrue[1], rsdPDEElemL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDELTrue[2], rsdPDEElemL[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdPDERTrue[0], rsdPDEElemR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue[1], rsdPDEElemR[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue[2], rsdPDEElemR[2], small_tol, close_tol );

  integralLO( fcnLO, xedge, rsdLOElemL, nIntegrandL, rsdLOElemR, nIntegrandR );

  //LO residuals (left): (lifting-operator)
  rsdLiftxLTrue[0] = ( 0 ); // Basis function 1
  rsdLiftyLTrue[0] = ( 0 ); // Basis function 1
  rsdLiftxLTrue[1] = ( -5./6. ); // Basis function 2
  rsdLiftyLTrue[1] = ( -5./6. ); // Basis function 2
  rsdLiftxLTrue[2] = ( -1./6. ); // Basis function 3
  rsdLiftyLTrue[2] = ( -1./6. ); // Basis function 3

  //LO residuals (right): (lifting-operator))
  rsdLiftxRTrue[0] = ( 0 ); // Basis function 1
  rsdLiftyRTrue[0] = ( 0 ); // Basis function 1
  rsdLiftxRTrue[1] = ( -1./6. ); // Basis function 2
  rsdLiftyRTrue[1] = ( -1./6. ); // Basis function 2
  rsdLiftxRTrue[2] = ( -5./6. ); // Basis function 3
  rsdLiftyRTrue[2] = ( -5./6. ); // Basis function 3

  SANS_CHECK_CLOSE( rsdLiftxLTrue[0], rsdLOElemL[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftyLTrue[0], rsdLOElemL[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftxLTrue[1], rsdLOElemL[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftyLTrue[1], rsdLOElemL[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftxLTrue[2], rsdLOElemL[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftyLTrue[2], rsdLOElemL[2][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdLiftxRTrue[0], rsdLOElemR[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftyRTrue[0], rsdLOElemR[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftxRTrue[1], rsdLOElemR[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftyRTrue[1], rsdLOElemR[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftxRTrue[2], rsdLOElemR[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftyRTrue[2], rsdLOElemR[2][1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_2D_Triangle_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;

  typedef Element<ArrayQ,TopoD2,Triangle> CellQFieldClass;
  typedef Element<DLA::VectorS<2,ArrayQ>,TopoD2,Triangle> ElementRFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParam,ElementParam> FieldWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemR.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;
  x4 = 1;  y4 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xfldElemR.DOF(0) = {x4, y4};
  xfldElemR.DOF(1) = {x3, y3};
  xfldElemR.DOF(2) = {x2, y2};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution

  order = 1;
  CellQFieldClass qfldElemL(order, BasisFunctionCategory_Hierarchical);
  CellQFieldClass qfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemR.nDOF() );

  // triangle solution (left)
  qfldElemL.DOF(0) = 1;
  qfldElemL.DOF(1) = 3;
  qfldElemL.DOF(2) = 4;

  // triangle solution (right)
  qfldElemR.DOF(0) = 7;
  qfldElemR.DOF(1) = 2;
  qfldElemR.DOF(2) = 9;

  // lifting operators

  ElementRFieldCell rfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementRFieldCell rfldElemR(order, BasisFunctionCategory_Hierarchical);

  rfldElemL.DOF(0) = { 2, -3};  rfldElemL.DOF(1) = { 7,  8};  rfldElemL.DOF(2) = {-1,  7};
  rfldElemR.DOF(0) = { 8, -2};  rfldElemR.DOF(1) = {-5,  7};  rfldElemR.DOF(2) = { 3,  9};

  // weight
  CellQFieldClass wfldElemL(order+1, BasisFunctionCategory_Hierarchical);
  CellQFieldClass wfldElemR(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElemL.order() );
  BOOST_CHECK_EQUAL( 6, wfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 2, wfldElemR.order() );
  BOOST_CHECK_EQUAL( 6, wfldElemR.nDOF() );

  // triangle solution (left)
  wfldElemL.DOF(0) = -2;
  wfldElemL.DOF(1) =  4;
  wfldElemL.DOF(2) =  3;
  wfldElemL.DOF(3) =  2;
  wfldElemL.DOF(4) =  4;
  wfldElemL.DOF(5) = -1;

  // triangle solution (right)
  wfldElemR.DOF(0) =  7;
  wfldElemR.DOF(1) =  3;
  wfldElemR.DOF(2) =  2;
  wfldElemR.DOF(3) = -1;
  wfldElemR.DOF(4) =  5;
  wfldElemR.DOF(5) = -3;

  // lifting operators

  ElementRFieldCell sfldElemL(order+1, BasisFunctionCategory_Hierarchical);
  ElementRFieldCell sfldElemR(order+1, BasisFunctionCategory_Hierarchical);

  sfldElemL.DOF(0) = { 1,  5};  sfldElemL.DOF(1) = { 3,  6};  sfldElemL.DOF(2) = {-1,  7};
  sfldElemL.DOF(3) = { 3,  1};  sfldElemL.DOF(4) = { 2,  2};  sfldElemL.DOF(5) = { 4,  3};

  sfldElemR.DOF(0) = { 5,  6};  sfldElemR.DOF(1) = { 4,  5};  sfldElemR.DOF(2) = { 3,  4};
  sfldElemR.DOF(3) = { 4,  3};  sfldElemR.DOF(4) = { 3,  2};  sfldElemR.DOF(5) = { 2,  1};

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  FieldWeightedClass fcn = fcnint.integrand( xedge, CanonicalTraceToCell(0, 1), CanonicalTraceToCell(0, -1),
                                             xfldElemL,
                                             qfldElemL, rfldElemL,
                                             wfldElemL, sfldElemL,
                                             xfldElemR,
                                             qfldElemR, rfldElemR,
                                             wfldElemR, sfldElemR );

  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOFLeft() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOFRight() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandLiftLTrue, integrandLiftRTrue;
  Real integrandPDELTrue;
  Real integrandPDERTrue;
  FieldWeightedClass::IntegrandType integrandL;
  FieldWeightedClass::IntegrandType integrandR;

  // Test at sRef={0}, {s,t}={1, 0}
  sRef = {0};
  fcn( sRef, integrandL, integrandR );

  //PDE residual integrands (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  integrandPDELTrue = ( (39/5.)*(sqrt(2)) ) + ( (-2743/125.)*(sqrt(2)) )
                    + ( (42969/250.)*(pow(2,-1/2.)) ) + ( (-8346/25.)*(sqrt(2)) );  // Weight function
  integrandPDERTrue = ( (-39/5.)*(pow(2,-1/2.)) ) + ( (2743/125.)*(pow(2,-1/2.)) )
                    + ( (89871/250.)*(pow(2,-1/2.)) ) + ( (4173/25.)*(sqrt(2)) );  // Weight function

  SANS_CHECK_CLOSE( integrandPDELTrue, integrandL.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue, integrandR.PDE, small_tol, close_tol );

  //LO residual integrands (left): (lifting-operator)
  integrandLiftLTrue = ( (-27)*(pow(2,-1/2.)) );  // Weight function

  //LO residual integrands (right): (lifting-operator)
  integrandLiftRTrue = ( (-21)*(pow(2,-1/2.)) );  // Weight function

  SANS_CHECK_CLOSE( integrandLiftLTrue, integrandL.Lift, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftRTrue, integrandR.Lift, small_tol, close_tol );


  // Test at sRef={1}, {s,t}={0, 1}
  sRef = {1};
  fcn( sRef, integrandL, integrandR );

  //PDE residual integrands (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  integrandPDELTrue = ( (39/5.)*(sqrt(2)) ) + ( (-8229/250.)*(pow(2,-1/2.)) )
      + ( (6277/250.)*(pow(2,-1/2.)) ) + ( (-2457/125.)*(sqrt(2)) );  // Weight function
  integrandPDERTrue = ( (-39/5.)*(sqrt(2)) ) + ( (8229/250.)*(pow(2,-1/2.)) )
      + ( (6699/250.)*(pow(2,-1/2.)) ) + ( (2457/125.)*(sqrt(2)) );  // Weight function

  SANS_CHECK_CLOSE( integrandPDELTrue, integrandL.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue, integrandR.PDE, small_tol, close_tol );

  //LO residual integrands (left): (lifting-operator)
  integrandLiftLTrue = ( (3)*(sqrt(2)) );  // Weight function

  //LO residual integrands (right): (lifting-operator)
  integrandLiftRTrue = ( (9)*(pow(2,-1/2.)) );  // Weight function

  SANS_CHECK_CLOSE( integrandLiftLTrue, integrandL.Lift, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftRTrue, integrandR.Lift, small_tol, close_tol );

  // Test at sRef={1/2}, {s,t}={1/2, 1/2}
  sRef = {1/2.};
  fcn( sRef, integrandL, integrandR );

  //PDE residual integrands (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  integrandPDELTrue = ( (1001/40.)*(pow(2,-1/2.)) ) + ( (-30173/500.)*(pow(2,-1/2.)) )
      + ( (4023/250.)*(pow(2,-1/2.)) ) + ( (-247533/500.)*(pow(2,-1/2.)) );  // Weight function
  integrandPDERTrue = ( (-273/40.)*(pow(2,-1/2.)) ) + ( (8229/500.)*(pow(2,-1/2.)) )
      + ( (11629/250.)*(pow(2,-1/2.)) ) + ( (67509/500.)*(pow(2,-1/2.)) );  // Weight function

  SANS_CHECK_CLOSE( integrandPDELTrue, integrandL.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue, integrandR.PDE, small_tol, close_tol );

  //LO residual integrands (left): (lifting-operator)
  integrandLiftLTrue = ( (-23/2.)*(pow(2,-1/2.)) );  // Weight function

  //LO residual integrands (right): (lifting-operator)
  integrandLiftRTrue = ( (-15)*(pow(2,-1/2.)) );  // Weight function

  SANS_CHECK_CLOSE( integrandLiftLTrue, integrandL.Lift, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftRTrue, integrandR.Lift, small_tol, close_tol );

  // test the trace element integral of the functor

  // quadrature rule: quadratic (basis & flux both linear)
  int quadratureorder = 3;

  ElementIntegral<TopoD1, Line, FieldWeightedClass::IntegrandType,
                                FieldWeightedClass::IntegrandType> integral(quadratureorder);

  FieldWeightedClass::IntegrandType rsdElemL=0;
  FieldWeightedClass::IntegrandType rsdElemR=0;
  Real rsdPDELTrue,rsdPDERTrue;
  Real rsdLiftLTrue,rsdLiftRTrue;

  // cell integration for canonical element
  integral( fcn, xedge, rsdElemL, rsdElemR );

  //PDE residuals (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  rsdPDELTrue = ( 1313/60. ) + ( -79547/1500. ) + ( 32669/750. ) + ( -111969/250. );   // Basis function
  rsdPDERTrue = ( -169/20. ) + ( 30173/1500. ) + ( 71543/750. ) + ( 38051/250. );   // Basis function

  SANS_CHECK_CLOSE( rsdPDELTrue, rsdElemL.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue, rsdElemR.PDE, small_tol, close_tol );

  //LO residuals (left): (lifting-operator)
  rsdLiftLTrue = ( -67/6. ); // Weight function

  //LO residuals (right): (lifting-operator))
  rsdLiftRTrue = ( -12 ); // Weight function

  SANS_CHECK_CLOSE( rsdLiftLTrue, rsdElemL.Lift, small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftRTrue, rsdElemR.Lift, small_tol, close_tol );


}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_2D_Triangle_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform> PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementRFieldCell;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real, Real, TopoD1, Line,
                                            TopoD2, Triangle, Triangle,ElementXFieldCell,ElementXFieldCell> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO<Real, TopoD1, Line, TopoD2, Triangle, Triangle,ElementXFieldCell,ElementXFieldCell> BasisWeightedLOClass;

  typedef IntegrandClass::FieldWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParam,ElementParam> FieldWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123, kxy = 0.553;
  Real kyx = 0.789, kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy,
                             kyx, kyy);

  Real a = 2.3;
  Source2D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemR.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;
  x4 = 1;  y4 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xfldElemR.DOF(0) = {x4, y4};
  xfldElemR.DOF(1) = {x3, y3};
  xfldElemR.DOF(2) = {x2, y2};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  for (int qorder = 2; qorder< 4; qorder++)
  {
    // solution
    ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell qfldElemR(qorder, BasisFunctionCategory_Hierarchical);
    ElementRFieldCell rfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementRFieldCell rfldElemR(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElemR(qorder, BasisFunctionCategory_Hierarchical);
    ElementRFieldCell sfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementRFieldCell sfldElemR(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder,   qfldElemL.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   qfldElemR.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElemR.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   rfldElemL.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, rfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   rfldElemR.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, rfldElemR.nDOF() );

    BOOST_CHECK_EQUAL( rfldElemL.nDOF(), rfldElemR.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), qfldElemR.nDOF() );
    BOOST_CHECK_EQUAL( wfldElemL.nDOF(), wfldElemR.nDOF() );
    BOOST_CHECK_EQUAL( sfldElemL.nDOF(), sfldElemR.nDOF() );

    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), rfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), wfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), sfldElemL.nDOF() );
    // line solution
    for ( int dof = 0; dof < qfldElemL.nDOF(); dof ++ )
    {
      qfldElemL.DOF(dof) = (dof+1)*pow(-1,dof);
      qfldElemR.DOF(dof) = (dof+2)*pow(-1,dof+1);
      rfldElemL.DOF(dof) = (dof+1)*pow(-1,dof);
      rfldElemR.DOF(dof) = (dof+2)*pow(-1,dof+1);
      wfldElemL.DOF(dof) = 0;
      wfldElemR.DOF(dof) = 0;
      sfldElemL.DOF(dof) = 0;
      sfldElemR.DOF(dof) = 0;
    }

    // BR2 discretization
    Real viscousEtaParameter = 6;
    DiscretizationDGBR2 disc(0, viscousEtaParameter);

    // integrand
    IntegrandClass fcnint( pde, disc, {0} );

    // integrand functor

    BasisWeightedPDEClass fcnPDEB = fcnint.integrand_PDE( xedge, CanonicalTraceToCell(0, 1), CanonicalTraceToCell(0, -1),
                                                         xfldElemL, qfldElemL, rfldElemL,
                                                         xfldElemR, qfldElemR, rfldElemR );
    BasisWeightedLOClass fcnLOB = fcnint.integrand_LO( xedge, CanonicalTraceToCell(0, 1), CanonicalTraceToCell(0, -1),
                                                      xfldElemL, qfldElemL,
                                                      xfldElemR, qfldElemR );



    // integrand functor
    FieldWeightedClass fcnW = fcnint.integrand( xedge, CanonicalTraceToCell(0,1), CanonicalTraceToCell(0,-1),
                                                xfldElemL,
                                                qfldElemL, rfldElemL,
                                                wfldElemL, sfldElemL,
                                                xfldElemR,
                                                qfldElemR, rfldElemR,
                                                wfldElemR, sfldElemR );

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;
    const int nIntegrandL = qfldElemL.nDOF();
    const int nIntegrandR = qfldElemR.nDOF();

    int quadratureorder = -1;
    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ,
                                           ArrayQ> integralPDEB(quadratureorder, nIntegrandL, nIntegrandR);
    GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedLOClass::IntegrandType,
                                           BasisWeightedLOClass::IntegrandType> integralLOB(quadratureorder, nIntegrandL, nIntegrandR);

    ElementIntegral<TopoD1, Line, FieldWeightedClass::IntegrandType,
                                  FieldWeightedClass::IntegrandType> integralW(quadratureorder);
    std::vector<ArrayQ> rsdPDEElemBL(nIntegrandL, 0);
    std::vector<ArrayQ> rsdPDEElemBR(nIntegrandR, 0);
    std::vector<BasisWeightedLOClass::IntegrandType> rsdLOElemBL(nIntegrandL, 0);
    std::vector<BasisWeightedLOClass::IntegrandType> rsdLOElemBR(nIntegrandR, 0);
    FieldWeightedClass::IntegrandType rsdElemWL=0;
    FieldWeightedClass::IntegrandType rsdElemWR=0;

    // cell integration for canonical element
    integralPDEB( fcnPDEB, xedge, rsdPDEElemBL.data(), nIntegrandL, rsdPDEElemBR.data(), nIntegrandR );
    integralLOB ( fcnLOB,  xedge, rsdLOElemBL.data(),  nIntegrandL, rsdLOElemBR.data(),  nIntegrandR );

    for (int i = 0; i < wfldElemL.nDOF(); i++ )
    {
      // set just one of the weights to one
      wfldElemL.DOF(i) = 1; sfldElemL.DOF(i) = 1;
      wfldElemR.DOF(i) = 1; sfldElemR.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemWL = 0; rsdElemWR = 0;
      integralW(fcnW, xedge, rsdElemWL, rsdElemWR );

      // test the two integrands are the same
      SANS_CHECK_CLOSE ( rsdElemWL.PDE, rsdPDEElemBL[i], small_tol, close_tol );
      SANS_CHECK_CLOSE ( rsdElemWR.PDE, rsdPDEElemBR[i], small_tol, close_tol );

      Real tmpL = 0, tmpR = 0;
      for (int D = 0; D < PhysD2::D; D++ )
      {
        tmpL += rsdLOElemBL[i][D];
        tmpR += rsdLOElemBR[i][D];
      }
      SANS_CHECK_CLOSE( rsdElemWL.Lift, tmpL, small_tol, close_tol );
      SANS_CHECK_CLOSE( rsdElemWR.Lift, tmpR, small_tol, close_tol );

      // reset to 0
      wfldElemL.DOF(i) = 0; sfldElemL.DOF(i) = 0;
      wfldElemR.DOF(i) = 0; sfldElemR.DOF(i) = 0;
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_2D_Quad_Quad_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Quad> ElementQFieldCell;
  typedef Element<DLA::VectorS<2,ArrayQ>,TopoD2,Quad> ElementRFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;

  typedef IntegrandClass::BasisWeighted_PDE<Real,Real,TopoD1,Line,
                                            TopoD2,Quad,Quad,ElementXFieldCell,ElementXFieldCell> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO<Real, TopoD1, Line, TopoD2, Quad, Quad,ElementXFieldCell,ElementXFieldCell> BasisWeightedLOClass;


  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123, kxy = 0.553;
  Real kyx = 0.789, kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy,
                             kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 4, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 4, xfldElemR.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent Quads grid
  Real x0, x1, x2, x3, x4, x5, y0, y1, y2, y3, y4, y5;

  /*
    3-------2-------5
    |       |       |
    |  (0)  |  (1)  |
    |       |       |
    0 ------1-------4
  */

  x0 = 0;  y0 = 0;
  x1 = 1;  y1 = 0;
  x2 = 1;  y2 = 1;
  x3 = 0;  y3 = 1;
  x4 = 2;  y4 = 0;
  x5 = 2;  y5 = 1;

  xfldElemL.DOF(0) = {x0, y0};
  xfldElemL.DOF(1) = {x1, y1};
  xfldElemL.DOF(2) = {x2, y2};
  xfldElemL.DOF(3) = {x3, y3};

  xfldElemR.DOF(0) = {x1, y1};
  xfldElemR.DOF(1) = {x4, y4};
  xfldElemR.DOF(2) = {x5, y5};
  xfldElemR.DOF(3) = {x2, y2};

  xedge.DOF(0) = {x1, y1};
  xedge.DOF(1) = {x2, y2};

  // solution

  order = 1;
  ElementQFieldCell qfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell qfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 4, qfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldElemR.order() );
  BOOST_CHECK_EQUAL( 4, qfldElemR.nDOF() );

  // Quad solution (left)
  qfldElemL.DOF(0) = 1;
  qfldElemL.DOF(1) = 3;
  qfldElemL.DOF(2) = 6;
  qfldElemL.DOF(3) = 4;

  // Quad solution (right)
  qfldElemR.DOF(0) = 7;
  qfldElemR.DOF(1) = 2;
  qfldElemR.DOF(2) = 5;
  qfldElemR.DOF(3) = 9;

  // lifting operators

  ElementRFieldCell rfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementRFieldCell rfldElemR(order, BasisFunctionCategory_Hierarchical);

  rfldElemL.DOF(0) = { 2, -3};
  rfldElemL.DOF(1) = { 7,  8};
  rfldElemL.DOF(2) = { 4,  1};
  rfldElemL.DOF(3) = {-1,  7};

  rfldElemR.DOF(0) = { 8, -2};
  rfldElemR.DOF(1) = {-5,  7};
  rfldElemR.DOF(2) = { 1,  6};
  rfldElemR.DOF(3) = { 3,  9};

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  BasisWeightedPDEClass fcnPDE = fcnint.integrand_PDE( xedge, CanonicalTraceToCell(1, 1), CanonicalTraceToCell(3, -1),
                                                       xfldElemL, qfldElemL, rfldElemL,
                                                       xfldElemR, qfldElemR, rfldElemR );
  BasisWeightedLOClass fcnLO = fcnint.integrand_LO( xedge, CanonicalTraceToCell(1, 1), CanonicalTraceToCell(3, -1),
                                                    xfldElemL, qfldElemL,
                                                    xfldElemR, qfldElemR );


  BOOST_CHECK_EQUAL( 1, fcnPDE.nEqn() );
  BOOST_CHECK_EQUAL( 4, fcnPDE.nDOFLeft() );
  BOOST_CHECK_EQUAL( 4, fcnPDE.nDOFRight() );
  BOOST_CHECK( fcnPDE.needsEvaluation() == true );

  const int nDOF = 4;
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandLiftxLTrue[nDOF], integrandLiftyLTrue[nDOF];
  Real integrandLiftxRTrue[nDOF], integrandLiftyRTrue[nDOF];
  Real integrandPDELTrue[nDOF];
  Real integrandPDERTrue[nDOF];
  ArrayQ integrandPDEL[nDOF];
  ArrayQ integrandPDER[nDOF];
  BasisWeightedLOClass::IntegrandType integrandLOL[nDOF];
  BasisWeightedLOClass::IntegrandType integrandLOR[nDOF];

  // Test at sRef={0}, {s,t}={1, 0}
  sRef={0};
  fcnPDE( sRef, integrandPDEL, 4, integrandPDER, 4 );

  //PDE residual integrands (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  integrandPDELTrue[0] = ( 0 ) + ( 0 ) + ( -2123/500. ) + ( 0 );  // Basis function 1
  integrandPDELTrue[1] = ( 33/10. ) + ( 901/500. ) + ( 667/250. ) + ( -105489/1000. );  // Basis function 2
  integrandPDELTrue[2] = ( 0 ) + ( 0 ) + ( 789/500. ) + ( 0 );  // Basis function 3
  integrandPDELTrue[3] = ( 0 ) + ( 0 ) + ( 0 ) + ( 0 );  // Basis function 4

  //PDE residual integrands (right): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  integrandPDERTrue[0] = ( -33/10. ) + ( -901/500. ) + ( -728/125. ) + ( 105489/1000. );  // Basis function 1
  integrandPDERTrue[1] = ( 0 ) + ( 0 ) + ( 2123/500. ) + ( 0 );  // Basis function 2
  integrandPDERTrue[2] = ( 0 ) + ( 0 ) + ( 0 ) + ( 0 );  // Basis function 3
  integrandPDERTrue[3] = ( 0 ) + ( 0 ) + ( 789/500. ) + ( 0 );  // Basis function 4

  SANS_CHECK_CLOSE( integrandPDELTrue[0], integrandPDEL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDELTrue[1], integrandPDEL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDELTrue[2], integrandPDEL[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDELTrue[3], integrandPDEL[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDERTrue[0], integrandPDER[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue[1], integrandPDER[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue[2], integrandPDER[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue[3], integrandPDER[3], small_tol, close_tol );

  fcnLO( sRef, integrandLOL, 4, integrandLOR,4 );

  fcnLO( sRef, integrandLOL, 4, integrandLOR,4 );

  //LO residual integrands (left): (lifting-operator)
  integrandLiftxLTrue[0] = ( 0 );  // Basis function 1
  integrandLiftyLTrue[0] = ( 0 );  // Basis function 1
  integrandLiftxLTrue[1] = ( -2 );  // Basis function 2
  integrandLiftyLTrue[1] = ( 0 );  // Basis function 2
  integrandLiftxLTrue[2] = ( 0 );  // Basis function 3
  integrandLiftyLTrue[2] = ( 0 );  // Basis function 3
  integrandLiftxLTrue[3] = ( 0 );  // Basis function 4
  integrandLiftyLTrue[3] = ( 0 );  // Basis function 4

  //LO residual integrands (right): (lifting-operator)
  integrandLiftxRTrue[0] = ( -2 );  // Basis function 1
  integrandLiftyRTrue[0] = ( 0 );  // Basis function 1
  integrandLiftxRTrue[1] = ( 0 );  // Basis function 2
  integrandLiftyRTrue[1] = ( 0 );  // Basis function 2
  integrandLiftxRTrue[2] = ( 0 );  // Basis function 3
  integrandLiftyRTrue[2] = ( 0 );  // Basis function 3
  integrandLiftxRTrue[3] = ( 0 );  // Basis function 4
  integrandLiftyRTrue[3] = ( 0 );  // Basis function 4

  SANS_CHECK_CLOSE( integrandLiftxLTrue[0], integrandLOL[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyLTrue[0], integrandLOL[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxLTrue[1], integrandLOL[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyLTrue[1], integrandLOL[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxLTrue[2], integrandLOL[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyLTrue[2], integrandLOL[2][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxLTrue[3], integrandLOL[3][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyLTrue[3], integrandLOL[3][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLiftxRTrue[0], integrandLOR[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyRTrue[0], integrandLOR[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxRTrue[1], integrandLOR[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyRTrue[1], integrandLOR[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxRTrue[2], integrandLOR[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyRTrue[2], integrandLOR[2][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxRTrue[3], integrandLOR[3][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyRTrue[3], integrandLOR[3][1], small_tol, close_tol );


  // Test at sRef={1}, {s,t}={1, 1}
  sRef={1};
  fcnPDE( sRef, integrandPDEL, 4, integrandPDER, 4 );

  //PDE residual integrands (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  integrandPDELTrue[0] = ( 0 ) + ( 0 ) + ( 0 ) + ( 0 );  // Basis function 1
  integrandPDELTrue[1] = ( 0 ) + ( 0 ) + ( -2367/2000. ) + ( 0 );  // Basis function 2
  integrandPDELTrue[2] = ( 33/5. ) + ( 1481/2000. ) + ( 546/125. ) + ( -61173/1000. );  // Basis function 3
  integrandPDELTrue[3] = ( 0 ) + ( 0 ) + ( -6369/2000. ) + ( 0 );  // Basis function 4

  //PDE residual integrands (right): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  integrandPDERTrue[0] = ( 0 ) + ( 0 ) + ( -2367/2000. ) + ( 0 );  // Basis function 1
  integrandPDERTrue[1] = ( 0 ) + ( 0 ) + ( 0 ) + ( 0 );  // Basis function 2
  integrandPDERTrue[2] = ( 0 ) + ( 0 ) + ( 6369/2000. ) + ( 0 );  // Basis function 3
  integrandPDERTrue[3] = ( -33/5. ) + ( -1481/2000. ) + ( -2001/1000. ) + ( 61173/1000. );  // Basis function 4

  SANS_CHECK_CLOSE( integrandPDELTrue[0], integrandPDEL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDELTrue[1], integrandPDEL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDELTrue[2], integrandPDEL[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDELTrue[3], integrandPDEL[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDERTrue[0], integrandPDER[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue[1], integrandPDER[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue[2], integrandPDER[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue[3], integrandPDER[3], small_tol, close_tol );

  fcnLO( sRef, integrandLOL, 4, integrandLOR, 4);

  //LO residual integrands (left): (lifting-operator)
  integrandLiftxLTrue[0] = ( 0 );  // Basis function 1
  integrandLiftyLTrue[0] = ( 0 );  // Basis function 1
  integrandLiftxLTrue[1] = ( 0 );  // Basis function 2
  integrandLiftyLTrue[1] = ( 0 );  // Basis function 2
  integrandLiftxLTrue[2] = ( -3/2. );  // Basis function 3
  integrandLiftyLTrue[2] = ( 0 );  // Basis function 3
  integrandLiftxLTrue[3] = ( 0 );  // Basis function 4
  integrandLiftyLTrue[3] = ( 0 );  // Basis function 4

  //LO residual integrands (right): (lifting-operator)
  integrandLiftxRTrue[0] = ( 0 );  // Basis function 1
  integrandLiftyRTrue[0] = ( 0 );  // Basis function 1
  integrandLiftxRTrue[1] = ( 0 );  // Basis function 2
  integrandLiftyRTrue[1] = ( 0 );  // Basis function 2
  integrandLiftxRTrue[2] = ( 0 );  // Basis function 3
  integrandLiftyRTrue[2] = ( 0 );  // Basis function 3
  integrandLiftxRTrue[3] = ( -3/2. );  // Basis function 4
  integrandLiftyRTrue[3] = ( 0 );  // Basis function 4

  SANS_CHECK_CLOSE( integrandLiftxLTrue[0], integrandLOL[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyLTrue[0], integrandLOL[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxLTrue[1], integrandLOL[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyLTrue[1], integrandLOL[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxLTrue[2], integrandLOL[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyLTrue[2], integrandLOL[2][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLiftxRTrue[0], integrandLOR[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyRTrue[0], integrandLOR[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxRTrue[1], integrandLOR[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyRTrue[1], integrandLOR[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxRTrue[2], integrandLOR[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyRTrue[2], integrandLOR[2][1], small_tol, close_tol );


  // Test at sRef={1/2}, {s,t}={1, 1/2}
  sRef={1/2.};
  fcnPDE( sRef, integrandPDEL, 4, integrandPDER, 4 );

  //PDE residual integrands (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  integrandPDELTrue[0] = ( 0 ) + ( 0 ) + ( -14861/8000. ) + ( 0 );  // Basis function 1
  integrandPDELTrue[1] = ( 99/40. ) + ( 1017/1600. ) + ( 763/1600. ) + ( -83331/2000. );  // Basis function 2
  integrandPDELTrue[2] = ( 99/40. ) + ( 1017/1600. ) + ( 25907/8000. ) + ( -83331/2000. );  // Basis function 3
  integrandPDELTrue[3] = ( 0 ) + ( 0 ) + ( -14861/8000. ) + ( 0 );  // Basis function 4

  //PDE residual integrands (right): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  integrandPDERTrue[0] = ( -99/40. ) + ( -1017/1600. ) + ( -25907/8000. ) + ( 83331/2000. );  // Basis function 1
  integrandPDERTrue[1] = ( 0 ) + ( 0 ) + ( 14861/8000. ) + ( 0 );  // Basis function 2
  integrandPDERTrue[2] = ( 0 ) + ( 0 ) + ( 14861/8000. ) + ( 0 );  // Basis function 3
  integrandPDERTrue[3] = ( -99/40. ) + ( -1017/1600. ) + ( -763/1600. ) + ( 83331/2000. );  // Basis function 4

  SANS_CHECK_CLOSE( integrandPDELTrue[0], integrandPDEL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDELTrue[1], integrandPDEL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDELTrue[2], integrandPDEL[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDELTrue[3], integrandPDEL[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDERTrue[0], integrandPDER[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue[1], integrandPDER[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue[2], integrandPDER[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDERTrue[3], integrandPDER[3], small_tol, close_tol );

  fcnLO( sRef, integrandLOL, 4, integrandLOR, 4);

  //LO residual integrands (left): (lifting-operator)
  integrandLiftxLTrue[0] = ( 0 );  // Basis function 1
  integrandLiftyLTrue[0] = ( 0 );  // Basis function 1
  integrandLiftxLTrue[1] = ( -7/8. );  // Basis function 2
  integrandLiftyLTrue[1] = ( 0 );  // Basis function 2
  integrandLiftxLTrue[2] = ( -7/8. );  // Basis function 3
  integrandLiftyLTrue[2] = ( 0 );  // Basis function 3
  integrandLiftxLTrue[3] = ( 0 );  // Basis function 4
  integrandLiftyLTrue[3] = ( 0 );  // Basis function 4

  //LO residual integrands (right): (lifting-operator)
  integrandLiftxRTrue[0] = ( -7/8. );  // Basis function 1
  integrandLiftyRTrue[0] = ( 0 );  // Basis function 1
  integrandLiftxRTrue[1] = ( 0 );  // Basis function 2
  integrandLiftyRTrue[1] = ( 0 );  // Basis function 2
  integrandLiftxRTrue[2] = ( 0 );  // Basis function 3
  integrandLiftyRTrue[2] = ( 0 );  // Basis function 3
  integrandLiftxRTrue[3] = ( -7/8. );  // Basis function 4
  integrandLiftyRTrue[3] = ( 0 );  // Basis function 4

  SANS_CHECK_CLOSE( integrandLiftxLTrue[0], integrandLOL[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyLTrue[0], integrandLOL[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxLTrue[1], integrandLOL[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyLTrue[1], integrandLOL[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxLTrue[2], integrandLOL[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyLTrue[2], integrandLOL[2][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLiftxRTrue[0], integrandLOR[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyRTrue[0], integrandLOR[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxRTrue[1], integrandLOR[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyRTrue[1], integrandLOR[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxRTrue[2], integrandLOR[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyRTrue[2], integrandLOR[2][1], small_tol, close_tol );

  // test the trace element integral of the functor

  // quadrature rule: quadratic (basis & flux both linear)
  int quadratureorder = 2;

  int nIntegrandL = qfldElemL.nDOF();
  int nIntegrandR = qfldElemR.nDOF();
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ,
                                         ArrayQ> integralPDE(quadratureorder, nIntegrandL, nIntegrandR);
  GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedLOClass::IntegrandType,
                                         BasisWeightedLOClass::IntegrandType> integralLO(quadratureorder, nIntegrandL, nIntegrandR);

  ArrayQ rsdPDEElemL[nDOF] = {0,0,0,0};
  ArrayQ rsdPDEElemR[nDOF] = {0,0,0,0};
  BasisWeightedLOClass::IntegrandType rsdLOElemL[nDOF] = {0,0,0,0};
  BasisWeightedLOClass::IntegrandType rsdLOElemR[nDOF] = {0,0,0,0};
  Real rsdPDELTrue[nDOF],rsdPDERTrue[nDOF];
  Real rsdLiftxLTrue[nDOF], rsdLiftyLTrue[nDOF];
  Real rsdLiftxRTrue[nDOF], rsdLiftyRTrue[nDOF];

  // cell integration for canonical element
  integralPDE( fcnPDE, xedge, rsdPDEElemL, nIntegrandL, rsdPDEElemR, nIntegrandR );

  //PDE residuals (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  rsdPDELTrue[0] = ( 0 ) + ( 0 ) + ( -23353/12000. ) + ( 0 );   // Basis function 1
  rsdPDELTrue[1] = ( 11/5. ) + ( 8689/12000. ) + ( 212/375. ) + ( -90717/2000. );   // Basis function 2
  rsdPDELTrue[2] = ( 11/4. ) + ( 3283/6000. ) + ( 37799/12000. ) + ( -15189/400. );   // Basis function 3
  rsdPDELTrue[3] = ( 0 ) + ( 0 ) + ( -2123/1200. ) + ( 0 );   // Basis function 4

  //PDE residuals (right): (advective) + (viscous) + (dual-consistent) + (lifting-operator))
  rsdPDERTrue[0] = ( -11/5. ) + ( -8689/12000. ) + ( -19961/6000. ) + ( 90717/2000. );   // Basis function 1
  rsdPDERTrue[1] = ( 0 ) + ( 0 ) + ( 23353/12000. ) + ( 0 );   // Basis function 2
  rsdPDERTrue[2] = ( 0 ) + ( 0 ) + ( 2123/1200. ) + ( 0 );   // Basis function 3
  rsdPDERTrue[3] = ( -11/4. ) + ( -3283/6000. ) + ( -4661/12000. ) + ( 15189/400. );   // Basis function 4

  SANS_CHECK_CLOSE( rsdPDELTrue[0], rsdPDEElemL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDELTrue[1], rsdPDEElemL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDELTrue[2], rsdPDEElemL[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDELTrue[3], rsdPDEElemL[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdPDERTrue[0], rsdPDEElemR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue[1], rsdPDEElemR[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue[2], rsdPDEElemR[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue[3], rsdPDEElemR[3], small_tol, close_tol );

  integralLO( fcnLO, xedge, rsdLOElemL, nIntegrandL, rsdLOElemR, nIntegrandR );

  //LO residuals (left): (lifting-operator)
  rsdLiftxLTrue[0] = ( 0 ); // Basis function 1
  rsdLiftyLTrue[0] = ( 0 ); // Basis function 1
  rsdLiftxLTrue[1] = ( -11/12. ); // Basis function 2
  rsdLiftyLTrue[1] = ( 0 ); // Basis function 2
  rsdLiftxLTrue[2] = ( -5/6. ); // Basis function 3
  rsdLiftyLTrue[2] = ( 0 ); // Basis function 3
  rsdLiftxLTrue[3] = ( 0 ); // Basis function 4
  rsdLiftyLTrue[3] = ( 0 ); // Basis function 4

  //LO residuals (right): (lifting-operator))
  rsdLiftxRTrue[0] = ( -11/12. ); // Basis function 1
  rsdLiftyRTrue[0] = ( 0 ); // Basis function 1
  rsdLiftxRTrue[1] = ( 0 ); // Basis function 2
  rsdLiftyRTrue[1] = ( 0 ); // Basis function 2
  rsdLiftxRTrue[2] = ( 0 ); // Basis function 3
  rsdLiftyRTrue[2] = ( 0 ); // Basis function 3
  rsdLiftxRTrue[3] = ( -5/6. ); // Basis function 4
  rsdLiftyRTrue[3] = ( 0 ); // Basis function 4

  SANS_CHECK_CLOSE( rsdLiftxLTrue[0], rsdLOElemL[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftyLTrue[0], rsdLOElemL[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftxLTrue[1], rsdLOElemL[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftyLTrue[1], rsdLOElemL[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftxLTrue[2], rsdLOElemL[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftyLTrue[2], rsdLOElemL[2][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftxLTrue[3], rsdLOElemL[3][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftyLTrue[3], rsdLOElemL[3][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdLiftxRTrue[0], rsdLOElemR[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftyRTrue[0], rsdLOElemR[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftxRTrue[1], rsdLOElemR[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftyRTrue[1], rsdLOElemR[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftxRTrue[2], rsdLOElemR[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftyRTrue[2], rsdLOElemR[2][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftxRTrue[3], rsdLOElemR[3][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftyRTrue[3], rsdLOElemR[3][1], small_tol, close_tol );
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
