// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_DGBR2_Triangle_AD_btest
// testing of 2-D cell element residual integrands for DG BR2: Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"
#include "Field/Element/ElementLift.h"

#include "Discretization/VMSDBR2/IntegrandCell_VMSD_BR2.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_UniformGrad> PDEAdvectionDiffusion1D;
typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass1D;
typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldLine;

template class IntegrandCell_VMSD_BR2<PDEClass1D>::BasisWeighted<Real, Real, TopoD1, Line, ElementXFieldLine>;
template class IntegrandCell_VMSD_BR2<PDEClass1D>::BasisWeighted_LO<Real, TopoD1, Line>;
template class IntegrandCell_VMSD_BR2<PDEClass1D>::FieldWeighted<Real, TopoD1, Line, ElementXFieldLine>;

//typedef PDEAdvectionDiffusion<PhysD2,
//                              AdvectiveFlux2D_Uniform,
//                              ViscousFlux2D_Uniform,
//                              Source2D_UniformGrad > PDEAdvectionDiffusion2D;
//typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass2D;
//typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldTri;
//
//template class IntegrandCell_DGBR2<PDEClass2D>::BasisWeighted_PDE<Real, Real, TopoD2, Triangle, ElementXFieldTri>;
//template class IntegrandCell_DGBR2<PDEClass2D>::BasisWeighted_LO<Real, TopoD2, Triangle>;
//template class IntegrandCell_DGBR2<PDEClass2D>::FieldWeighted<Real, TopoD2, Triangle, ElementXFieldTri>;
//
//typedef PDEAdvectionDiffusion<PhysD3,
//                              AdvectiveFlux3D_Uniform,
//                              ViscousFlux3D_Uniform,
//                              Source3D_UniformGrad > PDEAdvectionDiffusion3D;
//typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass3D;
//typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldTet;
//
//template class IntegrandCell_DGBR2<PDEClass3D>::BasisWeighted_PDE<Real, Real, TopoD3, Tet, ElementXFieldTet>;
//template class IntegrandCell_DGBR2<PDEClass3D>::BasisWeighted_LO<Real, TopoD3, Tet>;
//template class IntegrandCell_DGBR2<PDEClass3D>::FieldWeighted<Real, TopoD3, Tet, ElementXFieldTet>;
}



//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_VMSD_BR2_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_1D_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef ElementLift<VectorArrayQ,TopoD1,Line> ElementrFieldCell;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementRFieldCell;
  typedef ElementXFieldCell::RefCoordType RefCoordType;

  typedef IntegrandCell_VMSD_BR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,Real,TopoD1,Line,ElementXFieldCell> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO <Real,TopoD1,Line> BasisWeightedLOClass;


  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real s0 = 0.25, sx = 0.6;
  Source1D_UniformGrad source(s0, sx);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell qpfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // line solution
  qfldElem.DOF(0) = 1.5;
  qfldElem.DOF(1) = 2.3;

  qpfldElem.DOF(0) = 0.5;
  qpfldElem.DOF(1) = 0.7;

  // lifting operators
  ElementrFieldCell rfldElems(order, BasisFunctionCategory_Hierarchical);

  rfldElems[0].DOF(0) = -1;  rfldElems[0].DOF(1) = 5;
  rfldElems[1].DOF(0) =  3;  rfldElems[1].DOF(1) = 2;

  // sum of lifting operators
  ElementRFieldCell RfldElem(order, BasisFunctionCategory_Hierarchical);

  RfldElem.vectorViewDOF() = rfldElems[0].vectorViewDOF() +
                             rfldElems[1].vectorViewDOF();

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // integrand functor
  BasisWeightedPDEClass fcnPDE = fcnint.integrand( xfldElem, qfldElem, qpfldElem, RfldElem );
  BasisWeightedLOClass  fcnLO  = fcnint.integrand_LO( xfldElem, rfldElems );

  //BOOST_CHECK( fcn.N == 1);
  BOOST_CHECK_EQUAL( 1, fcnPDE.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcnPDE.nDOF() );
  BOOST_CHECK( fcnPDE.needsEvaluation() == true );
  BOOST_CHECK_EQUAL( 1, fcnLO.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcnLO.nDOF() );
  BOOST_CHECK( fcnLO.needsEvaluation() == true );

  const int nDOF = 2;
  const int nNode = 2;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  RefCoordType sRef;
  Real integrandPDETrue[nDOF] = {0,0};
  ArrayQ integrandLiftxTrue[nDOF][nNode];
  ArrayQ integrandPDE[nDOF];
  ArrayQ integrandPDEp[nDOF];
  BasisWeightedLOClass::IntegrandType integrandLO[nDOF];

  // Test at {0}
  sRef = {0};
  fcnPDE( sRef, integrandPDE, 2, integrandPDEp, 2 );
  fcnLO( sRef, integrandLO, 2 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 11/5. ) + ( -2123/1000. ) + ( 23/10. ); // Basis function 1
  integrandPDETrue[1] = ( -11/5. ) + ( 2123/1000. ) + ( 0 ); // Basis function 2

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDEp[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDEp[1], small_tol, close_tol );

  //Lifting operator residual integrands: (lifting)
  integrandLiftxTrue[0][0] =  ( -1 );   // Basis function 1  Lifting Operator 1
  integrandLiftxTrue[1][0] =  ( 0 );   // Basis function 2  Lifting Operator 1
  integrandLiftxTrue[0][1] =  ( 3 );   // Basis function 1  Lifting Operator 2
  integrandLiftxTrue[1][1] =  ( 0 );   // Basis function 2  Lifting Operator 2

                                              // [DOF][trace][d]
  SANS_CHECK_CLOSE( integrandLiftxTrue[0][0], integrandLO[0][0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[1][0], integrandLO[1][0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[0][1], integrandLO[0][1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[1][1], integrandLO[1][1][0], small_tol, close_tol );


  // Test at {1}
  sRef = {1};
  fcnPDE( sRef, integrandPDE, 2, integrandPDEp, 2 );
  fcnLO( sRef, integrandLO, 2 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 33/10. ) + ( -2123/1000. ) + ( 0 ); // Basis function 1
  integrandPDETrue[1] = ( -33/10. ) + ( 2123/1000. ) + ( 111/20. ); // Basis function 2

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDEp[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDEp[1], small_tol, close_tol );

  //Lifting operator residual integrands: (lifting)
  integrandLiftxTrue[0][0] =  ( 0 );   // Basis function 1  Lifting Operator 1
  integrandLiftxTrue[1][0] =  ( 5 );   // Basis function 2  Lifting Operator 1
  integrandLiftxTrue[0][1] =  ( 0 );   // Basis function 1  Lifting Operator 2
  integrandLiftxTrue[1][1] =  ( 2 );   // Basis function 2  Lifting Operator 2

                                                // [DOF][trace][d]
  SANS_CHECK_CLOSE( integrandLiftxTrue[0][0], integrandLO[0][0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[1][0], integrandLO[1][0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[0][1], integrandLO[0][1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[1][1], integrandLO[1][1][0], small_tol, close_tol );


  // Test at {1/2}
  sRef = {1./2.};
  fcnPDE( sRef, integrandPDE, 2, integrandPDEp, 2 );
  fcnLO( sRef, integrandLO, 2 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 11/4. ) + ( -2123/1000. ) + ( 157/80. ); // Basis function 1
  integrandPDETrue[1] = ( -11/4. ) + ( 2123/1000. ) + ( 157/80. ); // Basis function 2

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDEp[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDEp[1], small_tol, close_tol );

  //Lifting operator residual integrands: (lifting)
  integrandLiftxTrue[0][0] =  ( 1 );   // Basis function 1  Lifting Operator 1
  integrandLiftxTrue[1][0] =  ( 1 );   // Basis function 2  Lifting Operator 1
  integrandLiftxTrue[0][1] =  ( 5./4. );   // Basis function 1  Lifting Operator 2
  integrandLiftxTrue[1][1] =  ( 5./4. );   // Basis function 2  Lifting Operator 2

                                                // [DOF][trace][d]
  SANS_CHECK_CLOSE( integrandLiftxTrue[0][0], integrandLO[0][0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[1][0], integrandLO[1][0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[0][1], integrandLO[0][1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[1][1], integrandLO[1][1][0], small_tol, close_tol );


  // test the element integral of the functor
  int quadratureorder = 2;
  int nIntegrand = nDOF;
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integralPDE(quadratureorder, nIntegrand, nIntegrand);
  GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedLOClass::IntegrandType> integralLO(quadratureorder, nIntegrand);

  ArrayQ rsdElemPDE[nDOF];
  ArrayQ rsdElemPDEp[nDOF];
  BasisWeightedLOClass::IntegrandType rsdElemLO[nDOF];

  // cell integration for canonical element
  integralPDE( fcnPDE, xfldElem, rsdElemPDE, nIntegrand, rsdElemPDEp, nIntegrand );
  integralLO( fcnLO, xfldElem, rsdElemLO, nIntegrand );
  Real rsdPDETrue[nDOF], rsdLiftxTrue[nDOF][nNode];

  //PDE residual: (advective) + (viscous) + (source)
  rsdPDETrue[0] = ( 11/4. ) + ( -2123/1000. ) + ( 203/120. );   // Basis function 1
  rsdPDETrue[1] = ( -11/4. ) + ( 2123/1000. ) + ( 67/30. );   // Basis function 2

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdElemPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdElemPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdElemPDEp[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdElemPDEp[1], small_tol, close_tol );

  //Lifting Operator residual:
  rsdLiftxTrue[0][0] = ( 1./2. ); // Basis function 1, Lifting Operator 1
  rsdLiftxTrue[1][0] = ( 3./2. ); // Basis function 2, Lifting Operator 1
  rsdLiftxTrue[0][1] = ( 4./3. ); // Basis function 1, Lifting Operator 2
  rsdLiftxTrue[1][1] = ( 7./6. ); // Basis function 2, Lifting Operator 2

                                        // [DOF][trace][d]
  SANS_CHECK_CLOSE( rsdLiftxTrue[0][0], rsdElemLO[0][0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftxTrue[1][0], rsdElemLO[1][0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftxTrue[0][1], rsdElemLO[0][1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftxTrue[1][1], rsdElemLO[1][1][0], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_1D_Line_P1P2_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;
  typedef ElementLift<VectorArrayQ,TopoD1,Line> ElementrFieldClass;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementRFieldClass;

  typedef ElementXFieldClass::RefCoordType RefCoordType;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_VMSD_BR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD1,Line,ElementParam> FieldWeightedClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real s0 = 0.25, sx = 0.6;
  Source1D_UniformGrad source(s0, sx);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  // solution
  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldClass qpfldElem(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldClass efldElem(0, BasisFunctionCategory_Legendre);

  efldElem.DOF(0) = 0;

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  qfldElem.DOF(0) = 1.5;
  qfldElem.DOF(1) = 2.3;

  qpfldElem.DOF(0) = 0.5;
  qpfldElem.DOF(1) = 0.7;

  // lifting operators
  ElementrFieldClass rfldElems(order, BasisFunctionCategory_Hierarchical);

  rfldElems[0].DOF(0) = -1;  rfldElems[0].DOF(1) = 5;
  rfldElems[1].DOF(0) =  3;  rfldElems[1].DOF(1) = 2;

  // sum of lifting operators
  ElementRFieldClass RfldElem(order, BasisFunctionCategory_Hierarchical);

  RfldElem.vectorViewDOF() = rfldElems[0].vectorViewDOF() +
                             rfldElems[1].vectorViewDOF();

  // weighting
  ElementQFieldClass wfldElem(order+1, BasisFunctionCategory_Hierarchical);
  ElementQFieldClass wpfldElem(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElem.order() );
  BOOST_CHECK_EQUAL( 3, wfldElem.nDOF() );

  wfldElem.DOF(0) = 2.9;
  wfldElem.DOF(1) = 4.3;
  wfldElem.DOF(2) = 4.8;

  wpfldElem.DOF(0) = 0.1;
  wpfldElem.DOF(1) = -0.3;
  wpfldElem.DOF(2) = 0.2;

  // lifting operator weighting
  ElementrFieldClass sfldElems(order+1, BasisFunctionCategory_Hierarchical);

  sfldElems[0].DOF(0) = -5;  sfldElems[0].DOF(1) =  3;  sfldElems[0].DOF(2) =  2;
  sfldElems[1].DOF(0) =  2;  sfldElems[1].DOF(1) =  9;  sfldElems[1].DOF(2) =  3;

  // BR2 discretization (not used)
//  Real viscousEtaParameter = 2;
//  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // integrand functor
  FieldWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem, qpfldElem, rfldElems,
                                                       wfldElem, wpfldElem, sfldElems, efldElem );



//  BOOST_CHECK( fcn.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  RefCoordType sRef;
  Real integrandPDETrue;
  Real integrand[1];
  integrand[0] = 0;

  // Test at {0}
  sRef = {0};
  fcn( sRef, integrand, 1 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue = ( -231/5. ) + ( 44583/1000. ) + ( 69/10. ); // Weight function

  integrandPDETrue += 5. + 6.;

  SANS_CHECK_CLOSE( integrandPDETrue, integrand[0], small_tol, close_tol );


  // Test at {1}
  sRef = {1};
  fcn( sRef, integrand, 1 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue = ( 627/10. ) + ( -40337/1000. ) + ( 111/5. ); // Weight function

  //Lifting Operator residual integrand: (lifting)
  integrandPDETrue +=  ( 15 );   // Lifting Operator
  integrandPDETrue +=  ( 18 );   // Lifting Operator

  SANS_CHECK_CLOSE( integrandPDETrue, integrand[0], small_tol, close_tol );

  // Test at {1/2}
  sRef = {1./2.};
  fcn( sRef, integrand, 1 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue = ( -11/4. ) + ( 2123/1000. ) + ( 2669/80. ); // Weight function

  //Lifting Operator residual integrand: (lifting)
  integrandPDETrue +=  ( 2 );   // Lifting Operator
  integrandPDETrue += ( 85./4. );   // Lifting Operator

  SANS_CHECK_CLOSE( integrandPDETrue, integrand[0],  small_tol, close_tol );

  // test the element integral of the functor
  int quadratureorder = 3;
  GalerkinWeightedIntegral<TopoD1, Line, Real> integral(quadratureorder, efldElem.nDOF() );

  std::vector<Real> rsdElem(efldElem.nDOF(), 0);

  // cell integration for canonical element
  integral( fcn, xfldElem, rsdElem.data(), rsdElem.size() );
  Real rsdPDETrue;

  //PDE residual: (advective) + (viscous) + (source)
  rsdPDETrue = ( 11/12. ) + ( 2123/1000. ) + ( 3251/120. ); // Weight function

  //Lifting Operator residual:
  rsdPDETrue += ( 14./3. ); // Lifting Operator
  rsdPDETrue += ( 109./6. ); // Lifting Operator

  SANS_CHECK_CLOSE( rsdPDETrue, rsdElem[0], small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VMSD_1D_Line_Jacobian_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementRFieldClass;
  typedef IntegrandCell_VMSD_BR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real, Real, TopoD1,Line, ElementXFieldClass > BasisWeightedCoarseClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;


  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 0.25, b = 0.6;
  Source1D_UniformGrad source(a, b);

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  IntegrandClass fcnint( pde, {0} );

  for (int qorder = 1; qorder <= 4; qorder++)
  {
    ElementQFieldClass qfldElem(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldClass qpfldElem(qorder+1, BasisFunctionCategory_Hierarchical);
    ElementRFieldClass rfldElem(qorder+1, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, qfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder+1, qpfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+2, qpfldElem.nDOF() );

    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    for (int dof = 0; dof < qpfldElem.nDOF();dof++)
      qpfldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    for (int dof = 0; dof < rfldElem.nDOF(); dof++)
      for (int d =0; d<PhysD1::D; d++)
        rfldElem.DOF(dof)[d] = d*(dof+1)*pow(-1,dof);

    BasisWeightedCoarseClass fcnCoarse = fcnint.integrand( xfldElem, qfldElem, qpfldElem, rfldElem );

    const Real small_tol = 1e-11;
    const Real close_tol = 5e-11;
    RefCoordType sRef = Line::centerRef;
    DLA::VectorD<ArrayQ> integrand0(qfldElem.nDOF()), integrand1(qfldElem.nDOF());
    DLA::VectorD<ArrayQ> integrandp0(qpfldElem.nDOF()), integrandp1(qpfldElem.nDOF());

    JacobianElemCell_VMSDBR2<PhysD1,MatrixQ> mtxElem(qfldElem.nDOF(), qpfldElem.nDOF());
    JacobianElemCell_VMSDBR2<PhysD1,MatrixQ> mtxElemTrue(qfldElem.nDOF(), qpfldElem.nDOF());

    // compute jacobians via finite differenec (exact for linear PDE)
    fcnCoarse(sRef, &integrand0[0], integrand0.m(), &integrandp0[0], integrandp0.m());

    for (int i = 0; i < qfldElem.nDOF(); i++)
    {
      qfldElem.DOF(i) += 1;
      fcnCoarse(sRef, &integrand1[0], integrand1.m(), &integrandp1[0], integrandp1.m());
      qfldElem.DOF(i) -= 1;

      for (int j = 0; j < qfldElem.nDOF(); j++)
        mtxElemTrue.PDE_q(j,i) = integrand1[j] - integrand0[j];

      for (int j = 0; j < qpfldElem.nDOF(); j++)
        mtxElemTrue.PDEp_q(j,i) = integrandp1[j] - integrandp0[j];
    }

    for (int i = 0; i < qpfldElem.nDOF(); i++)
    {
      qpfldElem.DOF(i) += 1;
      fcnCoarse(sRef, &integrand1[0], integrand1.m(), &integrandp1[0], integrandp1.m());
      qpfldElem.DOF(i) -= 1;

      for (int j = 0; j < qfldElem.nDOF(); j++)
        mtxElemTrue.PDE_qp(j,i) = integrand1[j] - integrand0[j];

      for (int j = 0; j < qpfldElem.nDOF(); j++)
        mtxElemTrue.PDEp_qp(j,i) = integrandp1[j] - integrandp0[j];
    }

    for (int i = 0; i < rfldElem.nDOF(); i++)
    {
      for (int d=0; d <PhysD1::D; d++)
      {
        rfldElem.DOF(i)[d] += 1;
        fcnCoarse(sRef, &integrand1[0], integrand1.m(), &integrandp1[0], integrandp1.m());
        rfldElem.DOF(i)[d] -= 1;

        for (int j = 0; j < qfldElem.nDOF(); j++)
          mtxElemTrue.PDE_LO(j,i)(0,d) = integrand1[j] - integrand0[j];

        for (int j = 0; j < qpfldElem.nDOF(); j++)
          mtxElemTrue.PDEp_LO(j,i)(0,d) = integrandp1[j] - integrandp0[j];
      }
    }


    mtxElem = 0;

    // accumulate the jacobian via Surreal
    fcnCoarse(1., sRef, mtxElem);

    for (int i = 0; i < qfldElem.nDOF(); i++)
      for (int j = 0; j < qfldElem.nDOF(); j++)
        SANS_CHECK_CLOSE( mtxElemTrue.PDE_q(i,j), mtxElem.PDE_q(i,j), small_tol, close_tol );

    for (int i = 0; i < qfldElem.nDOF(); i++)
      for (int j = 0; j < qpfldElem.nDOF(); j++)
        SANS_CHECK_CLOSE( mtxElemTrue.PDE_qp(i,j), mtxElem.PDE_qp(i,j), small_tol, close_tol );

    for (int i = 0; i < qpfldElem.nDOF(); i++)
      for (int j = 0; j < qfldElem.nDOF(); j++)
        SANS_CHECK_CLOSE( mtxElemTrue.PDEp_q(i,j), mtxElem.PDEp_q(i,j), small_tol, close_tol );

    for (int i = 0; i < qpfldElem.nDOF(); i++)
      for (int j = 0; j < qpfldElem.nDOF(); j++)
        SANS_CHECK_CLOSE( mtxElemTrue.PDEp_qp(i,j), mtxElem.PDEp_qp(i,j), small_tol, close_tol );

    for (int i = 0; i < qfldElem.nDOF(); i++)
      for (int j = 0; j < rfldElem.nDOF(); j++)
        for (int d=0; d <PhysD1::D; d++)
          SANS_CHECK_CLOSE( mtxElemTrue.PDE_LO(i,j)(0,d), mtxElem.PDE_LO(i,j)(0,d), small_tol, close_tol );

    for (int i = 0; i < qpfldElem.nDOF(); i++)
      for (int j = 0; j < rfldElem.nDOF(); j++)
        for (int d=0; d <PhysD1::D; d++)
          SANS_CHECK_CLOSE( mtxElemTrue.PDEp_LO(i,j)(0,d), mtxElem.PDEp_LO(i,j)(0,d), small_tol, close_tol );

  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_1D_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef ElementLift<VectorArrayQ,TopoD1,Line> ElementrFieldCell;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementRFieldCell;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandCell_VMSD_BR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,Real,TopoD1,Line,ElementXFieldCell> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO <Real,TopoD1,Line> BasisWeightedLOClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD1,Line,ElementParam> FieldWeightedClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real s0 = 0.25, sx = 0.6;
  Source1D_UniformGrad source(s0, sx);

  PDEClass pde( adv, visc , source );

  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  for (int qorder = 1; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldCell qfldElem( qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell qpfldElem(qorder, BasisFunctionCategory_Hierarchical);
    ElementrFieldCell rfldElems(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElem( qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wpfldElem(qorder, BasisFunctionCategory_Hierarchical);
    ElementrFieldCell sfldElems(qorder, BasisFunctionCategory_Hierarchical);

    ElementQFieldCell efldElem(0, BasisFunctionCategory_Legendre);

    BOOST_CHECK_EQUAL( qorder,   qfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   qpfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, qpfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   wfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, wfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   wpfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, wpfldElem.nDOF() );

    BOOST_REQUIRE_EQUAL(wfldElem.nDOF(), qfldElem.nDOF());
    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
    {
      qfldElem.DOF(dof)  = (dof+1)*pow(-1,dof);
      qpfldElem.DOF(dof) = (dof+2)*pow(-2,dof);
      wfldElem.DOF(dof) = 0;
      wpfldElem.DOF(dof) = 0;
      for (int trace = 0; trace < Line::NTrace; trace++ )
      {
        rfldElems[trace].DOF(dof) = (dof+1)*pow(-1,dof);
        sfldElems[trace].DOF(dof) = 0;
      }
    }

    // sum of lifting operators
    ElementRFieldCell RfldElem(qorder, BasisFunctionCategory_Hierarchical);

    RfldElem.vectorViewDOF() = rfldElems[0].vectorViewDOF() +
                               rfldElems[1].vectorViewDOF();

    IntegrandClass fcnint( pde, {0} );

    BasisWeightedPDEClass fcnPDE = fcnint.integrand( xfldElem, qfldElem, qpfldElem, RfldElem );
    BasisWeightedLOClass fcnLO = fcnint.integrand_LO( xfldElem, rfldElems );
    FieldWeightedClass fcnW = fcnint.integrand( xfldElem, qfldElem, qpfldElem, rfldElems,
                                                          wfldElem, wpfldElem, sfldElems, efldElem );

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;
    const int nIntegrand = qfldElem.nDOF();
    std::vector<ArrayQ> rsdElemW(efldElem.nDOF(),0);
    std::vector<ArrayQ> rsdElemPDE(nIntegrand,0);
    std::vector<ArrayQ> rsdElemPDEp(nIntegrand,0);
    std::vector<BasisWeightedLOClass::IntegrandType> rsdElemLO(nIntegrand,0);

    int quadratureorder = -1;
    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integralPDE(quadratureorder, nIntegrand, nIntegrand);
    GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedLOClass::IntegrandType> integralLO(quadratureorder, nIntegrand);

    // cell integration for canonical element
    integralPDE( fcnPDE, xfldElem, rsdElemPDE.data(), nIntegrand, rsdElemPDEp.data(), nIntegrand );
    integralLO( fcnLO, xfldElem, rsdElemLO.data(), nIntegrand );

    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integralW(quadratureorder, efldElem.nDOF());

    // check bar weights
    for (int i = 0; i < wfldElem.nDOF(); i++) // testing the wfld
    {
      // set just one of the weights to one
      wfldElem.DOF(i) = 1;
      for (int trace = 0; trace < Line::NTrace; trace ++ ) // wasteful, but need to loop here because it's a dot product inside
      {
        sfldElems[trace].DOF(i) = 1;

        // cell integration for canonical element
        rsdElemW[0] = 0;
        integralW( fcnW, xfldElem, rsdElemW.data(), rsdElemW.size() );

        Real tmp = 0;
        for (int D = 0; D < PhysD1::D; D++ )          // [DOF][trace][d]
          tmp += rsdElemLO[i][trace][D];

        // test the the two integrands are identical
        SANS_CHECK_CLOSE( rsdElemW[0], rsdElemPDE[i] + tmp, small_tol, close_tol );

        // reset to zero
        sfldElems[trace].DOF(i) = 0;
      }
      // reset to 0
      wfldElem.DOF(i) = 0;
    }

    // now check prime weights
    for (int i = 0; i < wpfldElem.nDOF(); i++) // testing the wfld
    {
      // set just one of the weights to one
      wpfldElem.DOF(i) = 1;
      for (int trace = 0; trace < Line::NTrace; trace ++ ) // wasteful, but need to loop here because it's a dot product inside
      {
        sfldElems[trace].DOF(i) = 1;

        // cell integration for canonical element
        rsdElemW[0] = 0;
        integralW( fcnW, xfldElem, rsdElemW.data(), rsdElemW.size() );

        Real tmp = 0;
        for (int D = 0; D < PhysD1::D; D++ )          // [DOF][trace][d]
          tmp += rsdElemLO[i][trace][D];

        // test the the two integrands are identical
        SANS_CHECK_CLOSE( rsdElemW[0], rsdElemPDE[i] + tmp, small_tol, close_tol );

        // reset to zero
        sfldElems[trace].DOF(i) = 0;
      }
      // reset to 0
      wpfldElem.DOF(i) = 0;
    }
  }
}
#if 0
#if 0
#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_2D_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<2,ArrayQ> VectorArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> CellXFieldClass;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef ElementLift<VectorArrayQ,TopoD2,Triangle> ElementrFieldCell;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementRFieldCell;
  typedef ElementXFieldCell::RefCoordType RefCoordType;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,Real,TopoD2,Triangle,ElementXFieldCell>BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO <Real,TopoD2,Triangle>BasisWeightedLOClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Real s0 = 0.25, sx = 0.6, sy = -1.5;
  Source2D_UniformGrad source(s0, sx, sy);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid

  int order = 1;
  CellXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  // solution

  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  // lifting operators

  ElementrFieldCell rfldElems(order, BasisFunctionCategory_Hierarchical);

  rfldElems[0].DOF(0) = { 2, -3};  rfldElems[0].DOF(1) = { 7,  8};  rfldElems[0].DOF(2) = {-1,  7};
  rfldElems[1].DOF(0) = { 9,  6};  rfldElems[1].DOF(1) = {-1,  3};  rfldElems[1].DOF(2) = { 2,  3};
  rfldElems[2].DOF(0) = {-2,  1};  rfldElems[2].DOF(1) = { 4, -4};  rfldElems[2].DOF(2) = {-9, -5};

  // sum of lifting operators
  ElementRFieldCell RfldElem(order, BasisFunctionCategory_Hierarchical);

  RfldElem.vectorViewDOF() = rfldElems[0].vectorViewDOF() +
                             rfldElems[1].vectorViewDOF() +
                             rfldElems[2].vectorViewDOF();

  // BR2 discretization (not used)
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  BasisWeightedPDEClass fcnPDE = fcnint.integrand_PDE( xfldElem, qfldElem, RfldElem );
  BasisWeightedLOClass fcnLO = fcnint.integrand_LO( xfldElem, rfldElems );

  BOOST_CHECK_EQUAL( 1, fcnPDE.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcnPDE.nDOF() );
  BOOST_CHECK( fcnPDE.needsEvaluation() == true );
  BOOST_CHECK_EQUAL( 1, fcnLO.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcnLO.nDOF() );
  BOOST_CHECK( fcnLO.needsEvaluation() == true );

  const int nDOF = 3;
  const int nEdge = 3;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  RefCoordType sRef;
  Real integrandPDETrue[nDOF] = {0,0,0};
  Real integrandLiftxTrue[nDOF][nEdge], integrandLiftyTrue[nDOF][nEdge];
  ArrayQ integrandPDE[nDOF];
  BasisWeightedLOClass::IntegrandType integrandLO[nDOF];

  // Test at {0, 0}
  sRef = {0, 0};
  fcnPDE( sRef, integrandPDE, 3 );
  fcnLO( sRef, integrandLO, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 13/10. ) + ( -1254/125. ) + ( -73/20. ); // Basis function 1
  integrandPDETrue[1] = ( -11/10. ) + ( 1181/200. ) + ( 0 ); // Basis function 2
  integrandPDETrue[2] = ( -1/5. ) + ( 4127/1000. ) + ( 0 ); // Basis function 3

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );

  //Lifting Operator residual integrands:
  integrandLiftxTrue[0][0] = ( 2 );   // Basis function 1 Lifting Operator 1
  integrandLiftxTrue[0][1] = ( 9 );   // Basis function 1 Lifting Operator 2
  integrandLiftxTrue[0][2] = ( -2 );   // Basis function 1 Lifting Operator 3
  integrandLiftyTrue[0][0] = ( -3 );   // Basis function 1 Lifting Operator 1
  integrandLiftyTrue[0][1] = ( 6 );   // Basis function 1 Lifting Operator 2
  integrandLiftyTrue[0][2] = ( 1 );   // Basis function 1 Lifting Operator 3

  integrandLiftxTrue[1][0] = ( 0 );   // Basis function 2 Lifting Operator 1
  integrandLiftxTrue[1][1] = ( 0 );   // Basis function 2 Lifting Operator 2
  integrandLiftxTrue[1][2] = ( 0 );   // Basis function 2 Lifting Operator 3
  integrandLiftyTrue[1][0] = ( 0 );   // Basis function 2 Lifting Operator 1
  integrandLiftyTrue[1][1] = ( 0 );   // Basis function 2 Lifting Operator 2
  integrandLiftyTrue[1][2] = ( 0 );   // Basis function 2 Lifting Operator 3

  integrandLiftxTrue[2][0] = ( 0 );   // Basis function 3 Lifting Operator 1
  integrandLiftxTrue[2][1] = ( 0 );   // Basis function 3 Lifting Operator 2
  integrandLiftxTrue[2][2] = ( 0 );   // Basis function 3 Lifting Operator 3
  integrandLiftyTrue[2][0] = ( 0 );   // Basis function 3 Lifting Operator 1
  integrandLiftyTrue[2][1] = ( 0 );   // Basis function 3 Lifting Operator 2
  integrandLiftyTrue[2][2] = ( 0 );   // Basis function 3 Lifting Operator 3

  for (int edge =0; edge <nEdge; edge++)
  {
      for (int k =0; k <nDOF; k++)
      {
        SANS_CHECK_CLOSE( integrandLiftxTrue[k][edge], integrandLO[k][edge][0], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandLiftyTrue[k][edge], integrandLO[k][edge][1], small_tol, close_tol );
      }
  }

  // Test at {1, 0}
  sRef = {1, 0};
  fcnPDE( sRef, integrandPDE, 3 );
  fcnLO( sRef, integrandLO, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 39/10. ) + ( -1254/125. ) + ( 0 ); // Basis function 1
  integrandPDETrue[1] = ( -33/10. ) + ( 1181/200. ) + ( -141/20. ); // Basis function 2
  integrandPDETrue[2] = ( -3/5. ) + ( 4127/1000. ) + ( 0 ); // Basis function 3

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );

  //Lifting Operator residual integrands:
  integrandLiftxTrue[0][0] = ( 0 );   // Basis function 1 Lifting Operator 1
  integrandLiftxTrue[0][1] = ( 0 );   // Basis function 1 Lifting Operator 2
  integrandLiftxTrue[0][2] = ( 0 );   // Basis function 1 Lifting Operator 3
  integrandLiftyTrue[0][0] = ( 0 );   // Basis function 1 Lifting Operator 1
  integrandLiftyTrue[0][1] = ( 0 );   // Basis function 1 Lifting Operator 2
  integrandLiftyTrue[0][2] = ( 0 );   // Basis function 1 Lifting Operator 3

  integrandLiftxTrue[1][0] = ( 7 );   // Basis function 2 Lifting Operator 1
  integrandLiftxTrue[1][1] = ( -1 );   // Basis function 2 Lifting Operator 2
  integrandLiftxTrue[1][2] = ( 4 );   // Basis function 2 Lifting Operator 3
  integrandLiftyTrue[1][0] = ( 8 );   // Basis function 2 Lifting Operator 1
  integrandLiftyTrue[1][1] = ( 3 );   // Basis function 2 Lifting Operator 2
  integrandLiftyTrue[1][2] = ( -4 );   // Basis function 2 Lifting Operator 3

  integrandLiftxTrue[2][0] = ( 0 );   // Basis function 3 Lifting Operator 1
  integrandLiftxTrue[2][1] = ( 0 );   // Basis function 3 Lifting Operator 2
  integrandLiftxTrue[2][2] = ( 0 );   // Basis function 3 Lifting Operator 3
  integrandLiftyTrue[2][0] = ( 0 );   // Basis function 3 Lifting Operator 1
  integrandLiftyTrue[2][1] = ( 0 );   // Basis function 3 Lifting Operator 2
  integrandLiftyTrue[2][2] = ( 0 );   // Basis function 3 Lifting Operator 3

  for (int edge =0; edge <nEdge; edge++)
  {
      for (int k =0; k <nDOF; k++)
      {
        SANS_CHECK_CLOSE( integrandLiftxTrue[k][edge], integrandLO[k][edge][0], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandLiftyTrue[k][edge], integrandLO[k][edge][1], small_tol, close_tol );
      }
  }

  // Test at {0, 1}
  sRef = {0, 1};
  fcnPDE( sRef, integrandPDE, 3 );
  fcnLO( sRef, integrandLO, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 26/5. ) + ( -1254/125. ) + ( 0 ); // Basis function 1
  integrandPDETrue[1] = ( -22/5. ) + ( 1181/200. ) + ( 0 ); // Basis function 2
  integrandPDETrue[2] = ( -4/5. ) + ( 4127/1000. ) + ( -73/5. ); // Basis function 3

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );

  //Lifting Operator residual integrands:
  integrandLiftxTrue[0][0] = ( 0 );   // Basis function 1 Lifting Operator 1
  integrandLiftxTrue[0][1] = ( 0 );   // Basis function 1 Lifting Operator 2
  integrandLiftxTrue[0][2] = ( 0 );   // Basis function 1 Lifting Operator 3
  integrandLiftyTrue[0][0] = ( 0 );   // Basis function 1 Lifting Operator 1
  integrandLiftyTrue[0][1] = ( 0 );   // Basis function 1 Lifting Operator 2
  integrandLiftyTrue[0][2] = ( 0 );   // Basis function 1 Lifting Operator 3

  integrandLiftxTrue[1][0] = ( 0 );   // Basis function 2 Lifting Operator 1
  integrandLiftxTrue[1][1] = ( 0 );   // Basis function 2 Lifting Operator 2
  integrandLiftxTrue[1][2] = ( 0 );   // Basis function 2 Lifting Operator 3
  integrandLiftyTrue[1][0] = ( 0 );   // Basis function 2 Lifting Operator 1
  integrandLiftyTrue[1][1] = ( 0 );   // Basis function 2 Lifting Operator 2
  integrandLiftyTrue[1][2] = ( 0 );   // Basis function 2 Lifting Operator 3

  integrandLiftxTrue[2][0] = ( -1 );   // Basis function 3 Lifting Operator 1
  integrandLiftxTrue[2][1] = ( 2 );   // Basis function 3 Lifting Operator 2
  integrandLiftxTrue[2][2] = ( -9 );   // Basis function 3 Lifting Operator 3
  integrandLiftyTrue[2][0] = ( 7 );   // Basis function 3 Lifting Operator 1
  integrandLiftyTrue[2][1] = ( 3 );   // Basis function 3 Lifting Operator 2
  integrandLiftyTrue[2][2] = ( -5 );   // Basis function 3 Lifting Operator 3

  for (int edge =0; edge <nEdge; edge++)
  {
      for (int k =0; k <nDOF; k++)
      {
        SANS_CHECK_CLOSE( integrandLiftxTrue[k][edge], integrandLO[k][edge][0], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandLiftyTrue[k][edge], integrandLO[k][edge][1], small_tol, close_tol );
      }
  }

  // Test at {1/3, 1/3}
  sRef = {1./3., 1./3.};
  fcnPDE( sRef, integrandPDE, 3 );
  fcnLO( sRef, integrandLO, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 52/15. ) + ( -1254/125. ) + ( -253/90. ); // Basis function 1
  integrandPDETrue[1] = ( -44/15. ) + ( 1181/200. ) + ( -253/90. ); // Basis function 2
  integrandPDETrue[2] = ( -8/15. ) + ( 4127/1000. ) + ( -253/90. ); // Basis function 3

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );

  //Lifting Operator residual integrands:
  integrandLiftxTrue[0][0] = ( 8./9. );   // Basis function 1 Lifting Operator 1
  integrandLiftxTrue[0][1] = ( 10./9. );   // Basis function 1 Lifting Operator 2
  integrandLiftxTrue[0][2] = ( -7./9. );   // Basis function 1 Lifting Operator 3
  integrandLiftyTrue[0][0] = ( 4./3. );   // Basis function 1 Lifting Operator 1
  integrandLiftyTrue[0][1] = ( 4./3. );   // Basis function 1 Lifting Operator 2
  integrandLiftyTrue[0][2] = ( -8./9. );   // Basis function 1 Lifting Operator 3

  integrandLiftxTrue[1][0] = ( 8./9. );   // Basis function 2 Lifting Operator 1
  integrandLiftxTrue[1][1] = ( 10./9. );   // Basis function 2 Lifting Operator 2
  integrandLiftxTrue[1][2] = ( -7./9. );   // Basis function 2 Lifting Operator 3
  integrandLiftyTrue[1][0] = ( 4./3. );   // Basis function 2 Lifting Operator 1
  integrandLiftyTrue[1][1] = ( 4./3. );   // Basis function 2 Lifting Operator 2
  integrandLiftyTrue[1][2] = ( -8./9. );   // Basis function 2 Lifting Operator 3

  integrandLiftxTrue[2][0] = ( 8./9. );   // Basis function 3 Lifting Operator 1
  integrandLiftxTrue[2][1] = ( 10./9. );   // Basis function 3 Lifting Operator 2
  integrandLiftxTrue[2][2] = ( -7./9. );   // Basis function 3 Lifting Operator 3
  integrandLiftyTrue[2][0] = ( 4./3. );   // Basis function 3 Lifting Operator 1
  integrandLiftyTrue[2][1] = ( 4./3. );   // Basis function 3 Lifting Operator 2
  integrandLiftyTrue[2][2] = ( -8./9. );   // Basis function 3 Lifting Operator 3

  for (int edge =0; edge <nEdge; edge++)
  {
      for (int k =0; k <nDOF; k++)
      {
        SANS_CHECK_CLOSE( integrandLiftxTrue[k][edge], integrandLO[k][edge][0], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandLiftyTrue[k][edge], integrandLO[k][edge][1], small_tol, close_tol );
      }
  }

  // test the element integral of the functor

  // quadrature rule (for linear solution: basis grad is const, flux is linear)
  int quadratureorder = 2; //(integral of phi*lifting operator requires a quadratic quadrature order)
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralPDE(quadratureorder, nIntegrand);
  GalerkinWeightedIntegral<TopoD2, Triangle, BasisWeightedLOClass::IntegrandType> integralLO(quadratureorder, nIntegrand);

  ArrayQ rsdElemPDE[nDOF] = {0,0,0};
  BasisWeightedLOClass::IntegrandType rsdElemLO[nDOF] = {0,0,0};

  Real rsdPDETrue[nDOF], rsdxLiftTrue[nEdge][nDOF], rsdyLiftTrue[nEdge][nDOF];

  // cell integration for canonical element
  integralPDE( fcnPDE, xfldElem, rsdElemPDE, nIntegrand );
  integralLO( fcnLO, xfldElem, rsdElemLO, nIntegrand );

  //PDE residual: (advection) + (diffusion) + (source)
  rsdPDETrue[0] = (26/15.) + (-627/125.) + (-193/160.); // Basis function 1
  rsdPDETrue[1] = (-22/15.) + (1181/400.) + (-647/480.); // Basis function 2
  rsdPDETrue[2] = (-4/15.) + (4127/2000.) + (-133/80.); // Basis function 3

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdElemPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdElemPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[2], rsdElemPDE[2], small_tol, close_tol );

  // Lifting Operator residual:
  rsdxLiftTrue[0][0] = (5./12.);   // Basis function 1
  rsdyLiftTrue[0][0] = (3./8.);   // Basis function 1
  rsdxLiftTrue[0][1] = (5./8.);   // Basis function 2
  rsdyLiftTrue[0][1] = (5./6.);   // Basis function 2
  rsdxLiftTrue[0][2] = (7./24.);   // Basis function 3
  rsdyLiftTrue[0][2] = (19./24.);   // Basis function 3
  rsdxLiftTrue[1][0] = (19./24.);   // Basis function 1
  rsdyLiftTrue[1][0] = (3./4.);   // Basis function 1
  rsdxLiftTrue[1][1] = (3./8.);   // Basis function 2
  rsdyLiftTrue[1][1] = (5./8.);   // Basis function 2
  rsdxLiftTrue[1][2] = (1./2.);   // Basis function 3
  rsdyLiftTrue[1][2] = (5./8.);   // Basis function 3
  rsdxLiftTrue[2][0] = (-3./8.);   // Basis function 1
  rsdyLiftTrue[2][0] = (-7./24.);   // Basis function 1
  rsdxLiftTrue[2][1] = (-1./8.);   // Basis function 2
  rsdyLiftTrue[2][1] = (-1./2.);   // Basis function 2
  rsdxLiftTrue[2][2] = (-2./3.);   // Basis function 3
  rsdyLiftTrue[2][2] = (-13./24.);   // Basis function 3

  for (int edge =0; edge <nEdge; edge++)
  {
      for (int k =0; k <nDOF; k++)
      {
        SANS_CHECK_CLOSE( rsdxLiftTrue[edge][k], rsdElemLO[k][edge][0], small_tol, close_tol );
        SANS_CHECK_CLOSE( rsdyLiftTrue[edge][k], rsdElemLO[k][edge][1], small_tol, close_tol );
      }
  }
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_2D_Triangle_P1P2_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;
  typedef ElementLift<VectorArrayQ,TopoD2,Triangle> ElementRFieldClass;

  typedef ElementXFieldClass::RefCoordType RefCoordType;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD2,Triangle,ElementParam> FieldWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real s0 = 0.25, sx = 0.6, sy = -1.5;
  Source2D_UniformGrad source(s0, sx, sy);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid
  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  // solution
  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  // lifting operators
  ElementRFieldClass rfldElems(order, BasisFunctionCategory_Hierarchical);

  rfldElems[0].DOF(0) = { 2, -3};  rfldElems[0].DOF(1) = { 7,  8};  rfldElems[0].DOF(2) = {-1,  7};
  rfldElems[1].DOF(0) = { 9,  6};  rfldElems[1].DOF(1) = {-1,  3};  rfldElems[1].DOF(2) = { 2,  3};
  rfldElems[2].DOF(0) = {-2,  1};  rfldElems[2].DOF(1) = { 4, -4};  rfldElems[2].DOF(2) = {-9, -5};

  // weighting
  ElementQFieldClass wfldElem(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElem.order() );
  BOOST_CHECK_EQUAL( 6, wfldElem.nDOF() );

  // triangle solution
  wfldElem.DOF(0) = -2;
  wfldElem.DOF(1) =  4;
  wfldElem.DOF(2) =  3;
  wfldElem.DOF(3) =  2;
  wfldElem.DOF(4) =  4;
  wfldElem.DOF(5) = -1;

  // Lifting Operator Weighting
  ElementRFieldClass sfldElems(order+1, BasisFunctionCategory_Hierarchical);

  sfldElems[0].DOF(0) = { 1,  5};  sfldElems[0].DOF(1) = { 3,  6};  sfldElems[0].DOF(2) = {-1,  7};
  sfldElems[0].DOF(3) = { 3,  1};  sfldElems[0].DOF(4) = { 2,  2};  sfldElems[0].DOF(5) = { 4,  3};

  sfldElems[1].DOF(0) = { 5,  6};  sfldElems[1].DOF(1) = { 4,  5};  sfldElems[1].DOF(2) = { 3,  4};
  sfldElems[1].DOF(3) = { 4,  3};  sfldElems[1].DOF(4) = { 3,  2};  sfldElems[1].DOF(5) = { 2,  1};

  sfldElems[2].DOF(0) = {-2, -1};  sfldElems[2].DOF(1) = {-3,  2};  sfldElems[2].DOF(2) = {-4, -3};
  sfldElems[2].DOF(3) = {-1, -3};  sfldElems[2].DOF(4) = {-2, -2};  sfldElems[2].DOF(5) = {-3, -1};

  // BR2 discretization (not used)
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  FieldWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem, rfldElems, wfldElem, sfldElems );

  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  const int nEdge = 3;
  RefCoordType sRef;
  Real integrandPDETrue, integrandLiftTrue[3];
  FieldWeightedClass::IntegrandType integrand;

  // Test at {0, 0}
  sRef = {0, 0};
  fcn( sRef, integrand );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue = ( -32/5. ) + ( 108389/1000. ) + ( 73/10. ); // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //Lifting Operator residual integrands:
  integrandLiftTrue[0] = ( -13 );   // Weight function
  integrandLiftTrue[1] = ( 81 );   // Weight function
  integrandLiftTrue[2] = ( 3 );   // Weight function

  for (int edge =0; edge <nEdge; edge++)
      SANS_CHECK_CLOSE( integrandLiftTrue[edge], integrand.Lift[edge], small_tol, close_tol );

  // Test at {1, 0}
  sRef = {1, 0};
  fcn( sRef, integrand );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue = ( -216/5. ) + ( 137233/1000. ) + ( -141/5. ); // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //Lifting Operator residual integrands:
  integrandLiftTrue[0] = ( 69 );   // Weight function
  integrandLiftTrue[1] = ( 11 );   // Weight function
  integrandLiftTrue[2] = ( -20 );   // Weight function

  for (int edge =0; edge <nEdge; edge++)
      SANS_CHECK_CLOSE( integrandLiftTrue[edge], integrand.Lift[edge], small_tol, close_tol );

  // Test at {0, 1}
  sRef = {0, 1};
  fcn( sRef, integrand );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue = ( 88/5. ) + ( -62399/1000. ) + ( -219/5. ); // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //Lifting Operator residual integrands:
  integrandLiftTrue[0] = ( 50 );   // Weight function
  integrandLiftTrue[1] = ( 18 );   // Weight function
  integrandLiftTrue[2] = ( 51 );   // Weight function

  for (int edge =0; edge <nEdge; edge++)
      SANS_CHECK_CLOSE( integrandLiftTrue[edge], integrand.Lift[edge], small_tol, close_tol );

  // Test at {1/3, 1/3}
  sRef = {1/3., 1/3.};
  fcn( sRef, integrand );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue = ( -656/45. ) + ( 183223/3000. ) + ( -1771/54. ); // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //Lifting Operator residual integrands:
  integrandLiftTrue[0] = ( 48 );   // Weight function
  integrandLiftTrue[1] = ( 172/3. );   // Weight function
  integrandLiftTrue[2] = ( 199/9. );   // Weight function

  for (int edge =0; edge <nEdge; edge++)
      SANS_CHECK_CLOSE( integrandLiftTrue[edge], integrand.Lift[edge], small_tol, close_tol );

  // test the element integral of the functor
  // quadrature rule (for linear solution: basis grad is const, flux is linear)
  int quadratureorder = 3;
  ElementIntegral<TopoD2, Triangle, FieldWeightedClass::IntegrandType> integral(quadratureorder);

  FieldWeightedClass::IntegrandType rsdElem = 0;

  // cell integration for canonical element
  integral( fcn, xfldElem, rsdElem );

  Real rsdPDETrue, rsdLiftTrue[3];

  //PDE residual: (advection) + (diffusion) + (source)
  rsdPDETrue = (-34/5.) + (183223/6000.) + (-4711/300.);

  SANS_CHECK_CLOSE( rsdPDETrue, rsdElem.PDE, small_tol, close_tol );

  // Lifting Operator residual:
  rsdLiftTrue[0] = (447/20.);   // Weight function
  rsdLiftTrue[1] = (103/4.);   // Weight function
  rsdLiftTrue[2] = (387/40.);   // Weight function

  for (int edge =0; edge <nEdge; edge++)
      SANS_CHECK_CLOSE( rsdLiftTrue[edge], rsdElem.Lift[edge], small_tol, close_tol );

}
#endif
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_2D_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;
  typedef ElementLift<VectorArrayQ,TopoD2,Triangle> ElementrFieldClass;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementRFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,Real,TopoD2,Triangle,ElementParam > BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO <Real,TopoD2,Triangle> BasisWeightedLOClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD2,Triangle,ElementParam > FieldWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Real s0 = 0.25, sx = 0.6, sy = -1.5;
  Source2D_UniformGrad source(s0, sx, sy);

  PDEClass pde( adv, visc , source );

    // static tests
    BOOST_CHECK( pde.D == 2 );
    BOOST_CHECK( pde.N == 1 );

    // flux components
    BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
    BOOST_CHECK( pde.hasFluxAdvective() == true );
    BOOST_CHECK( pde.hasFluxViscous() == true );
    BOOST_CHECK( pde.hasSource() == true );
    BOOST_CHECK( pde.hasForcingFunction() == false );

    BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

    // grid
    int order = 1;
    ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( 1, xfldElem.order() );
    BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

    // triangle grid
    Real x1, x2, x3, y1, y2, y3;

    x1 = 0;  y1 = 0;
    x2 = 1;  y2 = 0;
    x3 = 0;  y3 = 1;

    xfldElem.DOF(0) = {x1, y1};
    xfldElem.DOF(1) = {x2, y2};
    xfldElem.DOF(2) = {x3, y3};

  for (int qorder = 1; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldClass qfldElem( qorder, BasisFunctionCategory_Hierarchical);
    ElementrFieldClass rfldElems(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldClass wfldElem( qorder, BasisFunctionCategory_Hierarchical);
    ElementrFieldClass sfldElems(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder,   qfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   wfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, wfldElem.nDOF() );

    BOOST_REQUIRE_EQUAL(wfldElem.nDOF(), qfldElem.nDOF());
    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
    {
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);
      wfldElem.DOF(dof) = 0;
      for (int trace = 0; trace < Triangle::NTrace; trace++ )
      {
        rfldElems[trace].DOF(dof) = (dof+1)*pow(-1,dof);
        sfldElems[trace].DOF(dof) = 0;
      }
    }

    // sum of lifting operators
    ElementRFieldClass RfldElem(qorder, BasisFunctionCategory_Hierarchical);

    RfldElem.vectorViewDOF() = rfldElems[0].vectorViewDOF() +
                               rfldElems[1].vectorViewDOF() +
                               rfldElems[2].vectorViewDOF();

    // BR2 discretization (not used)
    Real viscousEtaParameter = 6;
    DiscretizationDGBR2 disc(0, viscousEtaParameter);

    IntegrandClass fcnint( pde, disc, {0} );

    BasisWeightedPDEClass fcnPDEB = fcnint.integrand_PDE( xfldElem, qfldElem, RfldElem );
    BasisWeightedLOClass fcnLOB = fcnint.integrand_LO( xfldElem, rfldElems );
    FieldWeightedClass fcnW = fcnint.integrand( xfldElem, qfldElem, rfldElems, wfldElem, sfldElems );

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;
    const int nIntegrand = qfldElem.nDOF();
    FieldWeightedClass::IntegrandType rsdElemW=0;
    std::vector<ArrayQ> rsdElemPDEB(nIntegrand,0); // trickery to get round the POD warning
    std::vector<BasisWeightedLOClass::IntegrandType> rsdElemLOB(nIntegrand,0); // trickery to get round the POD warning

    int quadratureorder = -1;
    GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralPDEB(quadratureorder, nIntegrand);
    GalerkinWeightedIntegral<TopoD2, Triangle, BasisWeightedLOClass::IntegrandType> integralLOB(quadratureorder, nIntegrand);

    // cell integration for canonical element
    integralPDEB( fcnPDEB, xfldElem, rsdElemPDEB.data(), nIntegrand );
    integralLOB( fcnLOB, xfldElem, rsdElemLOB.data(), nIntegrand );

    ElementIntegral<TopoD2, Triangle, FieldWeightedClass::IntegrandType> integralW(quadratureorder);

    for (int i = 0; i < wfldElem.nDOF(); i++) // testing the wfld
    {
      // set just one of the weights to one
      wfldElem.DOF(i) = 1;
      for (int trace = 0; trace < Triangle::NTrace; trace ++ ) // wasteful, but need to loop here because it's a dot product inside
      {
        sfldElems[trace].DOF(i) = 1;

        // cell integration for canonical element
        rsdElemW = 0;
        integralW( fcnW, xfldElem, rsdElemW );

        // test the the two integrands are identical
        SANS_CHECK_CLOSE( rsdElemW.PDE, rsdElemPDEB[i], small_tol, close_tol );

        Real tmp = 0;
        for (int D = 0; D < PhysD2::D; D++ )
          tmp += rsdElemLOB[i][trace][D];

        SANS_CHECK_CLOSE( rsdElemW.Lift[trace], tmp, small_tol, close_tol )

        // reset to zero
        sfldElems[trace].DOF(i) = 0;
      }
      // reset to 0
      wfldElem.DOF(i) = 0;
    }
  }

}
#endif



//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
