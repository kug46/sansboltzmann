// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AlgebraicEquationSet_Galerkin_Triangle_AD_btest
// testing AlgebraicEquationSet_Galerkin

//#define DISPLAY_FOR_DEBUGGING

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Surreal/SurrealS.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"

#include "pde/BCParameters.h"

#include "Discretization/VMSDBR2/AlgebraicEquationSet_VMSD_BR2.h"

#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_EG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_BoundaryTrace.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( AlgebraicEquationSet_VMSDBR2_Triangle_AD_test_suite )


template<class MatrixQ>
void checkDenseSparseEquality(DLA::MatrixD<MatrixQ>& djac, const SLA::SparseMatrix_CRS<MatrixQ>& sjac )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  const int *col_ind = sjac.get_col_ind();
  const int *row_ptr = sjac.get_row_ptr();

  for (int i = 0; i < sjac.m(); i++)
    for (int j = row_ptr[i]; j < row_ptr[i+1]; j++)
    {
      SANS_CHECK_CLOSE( djac(i,col_ind[j]), sjac[j], small_tol, close_tol );

      // Zero out the non-zero entry so the next loop can look for any non-zero values missed
      djac(i,col_ind[j]) = 0;
    }

  // Check that all non-zero values have been cleared
  for (int i = 0; i < djac.m(); i++)
    for (int j = 0; j < djac.n(); j++)
      BOOST_CHECK_EQUAL(djac(i,j), 0);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseSystem_VMSD )
{
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_SineSine> SolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
      AdvectiveFlux2D_Uniform,
      ViscousFlux2D_Uniform,
      Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef AlgebraicEquationSet_VMSD_BR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                        AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  SolutionExact solnExact;

  Real s1 = 1.0, s2 = -0.2, s3 = 0.473;
//  Real s1 = 0.0, s2 = 0, s3 = 0.0;
  Source2D_UniformGrad source(s1, s2, s3);

  typedef ForcingFunction2D_MMS<PDEAdvectionDiffusion2D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict SineSine;
  SineSine[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.SineSine;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function] = SineSine;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType] = "Dirichlet";
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict PyBCList;
  PyBCList["BCNameSolution"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNameSolution"] = {0, 1, 2, 3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);


  // grid: HierarchicalP1 (aka X1)

  int ii = 3;
  int jj = 2;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < xfld.nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  for (int order = 1; order <= 2; order++)
  {
    //std::cout << "order = " << order << std::endl;

    Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Lagrange, EmbeddedCGField);
    Field_EG_Cell<PhysD2, TopoD2, ArrayQ> qpfld(qfld, order, BasisFunctionCategory_Lagrange);
    FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Lagrange);
    FieldLift_DG_BoundaryTrace<PhysD2, TopoD2, VectorArrayQ> rbfld(xfld, order, BasisFunctionCategory_Lagrange);

    qfld = 0;
    qpfld = 0;
    rfld = 0;
    rbfld = 0;

    // Lagrange multiplier: Legendre P1

    Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                          BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

    lgfld = 0;

    QuadratureOrder quadratureOrder( xfld, 2*order + 2 );
    std::vector<Real> tol = {1e-12, 1e-12, 1e-12};

    {

      DiscretizationVMSD stab(VMSDp, false);
      PrimalEquationSetClass PrimalEqSet(xfld, qfld, qpfld, rfld,  rbfld, lgfld,
                                         pde, stab, quadratureOrder, ResidualNorm_L2, tol,
                                         {0}, interiorTraceGroups, PyBCList, BCBoundaryGroups );

      SystemVectorClass q(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize()), rsdnew(rsd.size());

      PrimalEqSet.fillSystemVector(q);

      rsd = 0;
      rsdnew = 0;

      PrimalEqSet.residual(q, rsd);

      // jacobian nonzero pattern

      SystemNonZeroPattern nz(PrimalEqSet.matrixSize());

      PrimalEqSet.jacobian(q, nz);

      // jacobian

      SystemMatrixClass jac(nz);
      jac = 0;

      PrimalEqSet.jacobian(q, jac);

      for ( int ii = 0; ii < jac.m(); ii++)
      {
        for ( int i = 0; i < jac(ii,0).m(); i++ )
        {
          std::vector<Real> AJac;
          std::vector<int> iAJac;

          for ( int jj = 0; jj < jac.n(); jj++)
          {
            if (jac(ii,jj).getNumNonZero() == 0) continue;
            for ( int k = 0; k < jac(ii,jj).rowNonZero(i); k++)
              if ( abs(jac(ii,jj).sparseRow(i,k)) > 1e-12)
              {
                // Save the non-zero value and column index
                AJac.push_back(jac(ii,jj).sparseRow(i,k));
                iAJac.push_back(jac(ii,jj).get_col_ind()[jac(ii,jj).get_row_ptr()[i] + k]);
              }
          }

  #ifdef DISPLAY_FOR_DEBUGGING
          std::cout << "-----------------" << std::endl;
          std::cout << "row = " << i << std::endl;
          for ( std::size_t k = 0; k < AJac.size(); k++)
            std::cout << AJac[k] << ", ";
          std::cout << std::endl;
          for ( std::size_t k = 0; k < iAJac.size(); k++)
            std::cout << iAJac[k] << ", ";
          std::cout << std::endl;
  #endif

          std::vector<Real> diffJac;
          std::vector<int> idiffJac;
          for ( int kk = 0; kk < q.m(); kk++)
          {
            for ( int k = 0; k < q[kk].m(); k++)
            {
              // Use finite difference to compute an exact Jacobian, which works for a Linear PDE
              rsdnew = 0;
              q[kk][k] += 1;

              PrimalEqSet.residual(q, rsdnew);

              q[kk][k] -= 1;

              Real diff = rsdnew[ii][i] - rsd[ii][i];

              // Save of only non-zero jacobian entries
              if ( abs(diff) > 1e-12 )
              {
                diffJac.push_back(diff);
                idiffJac.push_back(k);
              }
            }
          }
  #ifdef DISPLAY_FOR_DEBUGGING
          std::cout << "FD" << std::endl;
          for ( std::size_t k = 0; k < diffJac.size(); k++)
            std::cout << diffJac[k] << ", ";
          std::cout << std::endl;
          for ( std::size_t k = 0; k < diffJac.size(); k++)
            std::cout << idiffJac[k] << ", ";
          std::cout << std::endl;
          std::cout << "A-FD" << std::endl;
  #endif
          bool error = false;
          BOOST_REQUIRE_EQUAL(AJac.size(), diffJac.size());
          for ( std::size_t k = 0; k < diffJac.size(); k++)
          {
            if ( abs(AJac[k]-diffJac[k]) > 1e-12)
            {
              error = true;
              std::cout <<  ii << "\n";
              std::cout << "AJac[" << k << "]=" << AJac[k]
                                                        << " diffJac[" << k << "]=" << diffJac[k] << std::endl;
            }
          }
          BOOST_REQUIRE(!error);
        }
      }

      qfld = 0;
      qpfld = 0;
      rfld = 0;
      rbfld = 0;

      SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);
      SystemVectorClass dq(q.size());
      dq = 0;

      solver.solve(rsd, dq);


      SLA::UMFPACK<SystemMatrixClass> solverT(PrimalEqSet, SLA::TransposeSolve);
      SystemVectorClass dqT(q.size());
      dqT = 0;

      solverT.solve(rsd, dqT);

      qfld = 0;
      qpfld = 0;
      rfld = 0;
      rbfld = 0;

      DiscretizationVMSD stab2(VMSDp, true);
      PrimalEquationSetClass PrimalEqSet2(xfld, qfld, qpfld, rfld,  rbfld, lgfld,
                                         pde, stab2, quadratureOrder, ResidualNorm_L2, tol,
                                         {0}, interiorTraceGroups, PyBCList, BCBoundaryGroups );

      SystemVectorClass q2(PrimalEqSet2.vectorStateSize());
      SystemVectorClass rsd2(PrimalEqSet2.vectorEqSize());

      PrimalEqSet2.fillSystemVector(q2);

      rsd2 = 0;

      PrimalEqSet2.residual(q2, rsd2);
      SLA::UMFPACK<SystemMatrixClass> solver2(PrimalEqSet2);

      SystemVectorClass dq2(q2.size());
      dq2 = 0;
      solver2.solve(rsd2, dq2);

      const Real close_tol = 1e-10;
      const Real small_tol = 1e-10;

      for (int i = 0; i < dq2.m(); i++)
        for (int j = 0; j < dq2(i).m(); j++)
        {
  //        std::cout << "i: " << i << ", j: " << j << ", dq1: " << dq(i)[j] << ", dq2: " << dq2(i)[j] << "\n";
          SANS_CHECK_CLOSE( dq(i)[j], dq2(i)[j], small_tol, close_tol)
        }


      SLA::UMFPACK<SystemMatrixClass> solverT2(PrimalEqSet2, SLA::TransposeSolve);

      rsd2 = 0;
      SystemVectorClass dqT2(q2.size());
      dqT2 = 0;

      PrimalEqSet2.residual(q2, rsd2);
      solverT2.solve(rsd2, dqT2);

      for (int i = 0; i < dqT2.m(); i++)
        for (int j = 0; j < dqT2(i).m(); j++)
        {
  //        std::cout << "i: " << i << ", j: " << j << ", dq1: " << dq(i)[j] << ", dq2: " << dq2(i)[j] << "\n";
          SANS_CHECK_CLOSE( dqT(i)[j], dqT2(i)[j], small_tol, close_tol)
        }
    }
  }

}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseSystem_VMSDL )
{
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_SineSine> SolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
      AdvectiveFlux2D_Uniform,
      ViscousFlux2D_Uniform,
      Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef AlgebraicEquationSet_VMSD_BR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                        AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  SolutionExact solnExact;

  Real s1 = 1.0, s2 = -0.2, s3 = 0.473;
  Source2D_UniformGrad source(s1, s2, s3);

  typedef ForcingFunction2D_MMS<PDEAdvectionDiffusion2D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict SineSine;
  SineSine[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.SineSine;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function] = SineSine;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType] = "Dirichlet";
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict PyBCList;
  PyBCList["BCNameSolution"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNameSolution"] = {0, 1, 2, 3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);


  // grid: HierarchicalP1 (aka X1)

  int ii = 3;
  int jj = 2;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < xfld.nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  for (int order = 1; order <= 3; order++)
  {

    Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Lagrange, EmbeddedCGField);
    Field_EG_Cell<PhysD2, TopoD2, ArrayQ> qpfld(qfld, order-1, BasisFunctionCategory_Legendre);
    FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, order-1, BasisFunctionCategory_Legendre);
    FieldLift_DG_BoundaryTrace<PhysD2, TopoD2, VectorArrayQ> rbfld(xfld, order, BasisFunctionCategory_Lagrange);

    qfld = 0;
    qpfld = 0;
    rfld = 0;
    rbfld = 0;

    // Lagrange multiplier: Legendre P1

    Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                          BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

    lgfld = 0;

    QuadratureOrder quadratureOrder( xfld, 2*order + 2 );
    std::vector<Real> tol = {1e-12, 1e-12, 1e-12};

    {
      DiscretizationVMSD stab(VMSDpminus1, false);
      PrimalEquationSetClass PrimalEqSet(xfld, qfld, qpfld, rfld,  rbfld, lgfld,
                                         pde, stab, quadratureOrder, ResidualNorm_L2, tol,
                                         {0}, interiorTraceGroups, PyBCList, BCBoundaryGroups );

      SystemVectorClass q(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize()), rsdnew(rsd.size());

      PrimalEqSet.fillSystemVector(q);

      rsd = 0;
      rsdnew = 0;

      PrimalEqSet.residual(q, rsd);

      // jacobian nonzero pattern

      SystemNonZeroPattern nz(PrimalEqSet.matrixSize());

      PrimalEqSet.jacobian(q, nz);

      // jacobian

      SystemMatrixClass jac(nz);
      jac = 0;

      PrimalEqSet.jacobian(q, jac);

      for ( int ii = 0; ii < jac.m(); ii++)
      {
        for ( int i = 0; i < jac(ii,0).m(); i++ )
        {
          std::vector<Real> AJac;
          std::vector<int> iAJac;

          for ( int jj = 0; jj < jac.n(); jj++)
          {
            if (jac(ii,jj).getNumNonZero() == 0) continue;
            for ( int k = 0; k < jac(ii,jj).rowNonZero(i); k++)
              if ( abs(jac(ii,jj).sparseRow(i,k)) > 1e-12)
              {
                // Save the non-zero value and column index
                AJac.push_back(jac(ii,jj).sparseRow(i,k));
                iAJac.push_back(jac(ii,jj).get_col_ind()[jac(ii,jj).get_row_ptr()[i] + k]);
              }
          }

  #ifdef DISPLAY_FOR_DEBUGGING
          std::cout << "-----------------" << std::endl;
          std::cout << "row = " << i << std::endl;
          for ( std::size_t k = 0; k < AJac.size(); k++)
            std::cout << AJac[k] << ", ";
          std::cout << std::endl;
          for ( std::size_t k = 0; k < iAJac.size(); k++)
            std::cout << iAJac[k] << ", ";
          std::cout << std::endl;
  #endif

          std::vector<Real> diffJac;
          std::vector<int> idiffJac;
          for ( int kk = 0; kk < q.m(); kk++)
          {
            for ( int k = 0; k < q[kk].m(); k++)
            {
              // Use finite difference to compute an exact Jacobian, which works for a Linear PDE
              rsdnew = 0;
              q[kk][k] += 1;

              PrimalEqSet.residual(q, rsdnew);

              q[kk][k] -= 1;

              Real diff = rsdnew[ii][i] - rsd[ii][i];

              // Save of only non-zero jacobian entries
              if ( abs(diff) > 1e-12 )
              {
                diffJac.push_back(diff);
                idiffJac.push_back(k);
              }
            }
          }
  #ifdef DISPLAY_FOR_DEBUGGING
          std::cout << "FD" << std::endl;
          for ( std::size_t k = 0; k < diffJac.size(); k++)
            std::cout << diffJac[k] << ", ";
          std::cout << std::endl;
          for ( std::size_t k = 0; k < diffJac.size(); k++)
            std::cout << idiffJac[k] << ", ";
          std::cout << std::endl;
          std::cout << "A-FD" << std::endl;
  #endif
          bool error = false;
          BOOST_REQUIRE_EQUAL(AJac.size(), diffJac.size());
          for ( std::size_t k = 0; k < diffJac.size(); k++)
          {
            if ( abs(AJac[k]-diffJac[k]) > 1e-12)
            {
              error = true;
              std::cout <<  ii << "\n";
              std::cout << "AJac[" << k << "]=" << AJac[k]
                                                        << " diffJac[" << k << "]=" << diffJac[k] << std::endl;
            }
          }
          BOOST_REQUIRE(!error);
        }
      }


      qfld = 0;
      qpfld = 0;
      rfld = 0;
      rbfld = 0;

      SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);
      SystemVectorClass dq(q.size());
      dq = 0;

      solver.solve(rsd, dq);


      SLA::UMFPACK<SystemMatrixClass> solverT(PrimalEqSet, SLA::TransposeSolve);
      SystemVectorClass dqT(q.size());
      dqT = 0;

      solverT.solve(rsd, dqT);

      qfld = 0;
      qpfld = 0;
      rfld = 0;
      rbfld = 0;

      DiscretizationVMSD stab2(VMSDpminus1, true);
      PrimalEquationSetClass PrimalEqSet2(xfld, qfld, qpfld, rfld,  rbfld, lgfld,
                                         pde, stab2, quadratureOrder, ResidualNorm_L2, tol,
                                         {0}, interiorTraceGroups, PyBCList, BCBoundaryGroups );

      SystemVectorClass q2(PrimalEqSet2.vectorStateSize());
      SystemVectorClass rsd2(PrimalEqSet2.vectorEqSize());

      PrimalEqSet2.fillSystemVector(q2);

      rsd2 = 0;

      PrimalEqSet2.residual(q2, rsd2);
      SLA::UMFPACK<SystemMatrixClass> solver2(PrimalEqSet2);

      SystemVectorClass dq2(q2.size());
      dq2 = 0;
      solver2.solve(rsd2, dq2);

      const Real close_tol = 1e-7;
      const Real small_tol = 1e-10;

      for (int i = 0; i < dq2.m(); i++)
        for (int j = 0; j < dq2(i).m(); j++)
        {
  //        std::cout << "i: " << i << ", j: " << j << ", dq1: " << dq(i)[j] << ", dq2: " << dq2(i)[j] << "\n";
          SANS_CHECK_CLOSE( dq(i)[j], dq2(i)[j], small_tol, close_tol)
        }

      SLA::UMFPACK<SystemMatrixClass> solverT2(PrimalEqSet2, SLA::TransposeSolve);

      rsd2 = 0;
      SystemVectorClass dqT2(q2.size());
      dqT2 = 0;

      PrimalEqSet2.residual(q2, rsd2);
      solverT2.solve(rsd2, dqT2);

      for (int i = 0; i < dqT2.m(); i++)
        for (int j = 0; j < dqT2(i).m(); j++)
        {
  //        std::cout << "i: " << i << ", j: " << j << ", dq1: " << dq(i)[j] << ", dq2: " << dq2(i)[j] << "\n";
          SANS_CHECK_CLOSE( dqT(i)[j], dqT2(i)[j], small_tol, close_tol)
        }
    }
  }
}
#endif




//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
