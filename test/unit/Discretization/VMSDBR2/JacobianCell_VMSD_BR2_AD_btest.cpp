// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianInteriorTrace_DGBR2_AD_btest
// testing of interior trace jacobian: advection-diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"

#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_EG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_EG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

#include "Discretization/VMSD/Discretization_VMSD.h"
#include "Discretization/VMSDBR2/IntegrandTrace_VMSD_BR2.h"
#include "Discretization/VMSDBR2/SetLiftingOperator_VMSD_BR2.h"

#include "Discretization/VMSDBR2/ResidualCell_VMSD_BR2.h"
#include "Discretization/VMSDBR2/JacobianCell_VMSD_BR2.h"
#include "Discretization/VMSDBR2/JacobianCell_VMSD_BR2_Uncondensed.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegratePeriodicTraceGroups.h"

#include "Discretization/VMSDBR2/IntegrandCell_VMSD_BR2.h"
#include "Discretization/IntegrateCellGroups.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"
#include "unit/UnitGrids/XField3D_Box_Hex_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianCell_VMSDBR2_AD_test_suite )

//
////----------------------------------------------------------------------------//
//BOOST_AUTO_TEST_CASE( DGBR2_1D_Line_Line_X1_SourceGrad )
//{
//  typedef PDEAdvectionDiffusion<PhysD1,
//                                AdvectiveFlux1D_Uniform,
//                                ViscousFlux1D_Uniform,
//                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
//  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
//  typedef PDEClass::template ArrayQ<Real> ArrayQ;
//  typedef PDEClass::template MatrixQ<Real> MatrixQ;
//  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
////  typedef DLA::MatrixS<1,PhysD1::D,MatrixQ> RowMatrixQ;
//
//  typedef IntegrandTrace_VMSD_BR2<PDEClass> IntegrandClass;
//  typedef IntegrandCell_VMSD_BR2<PDEClass> IntegrandCellClass;
//
//  Real step = 1.0;
//
//  Real u = 1.1;
//  AdvectiveFlux1D_Uniform adv(u);
//
//  Real kxx = 2.123;
//  ViscousFlux1D_Uniform visc(kxx);
//
//  Real a = 2.1, b = 5.6; // With a gradient in the source
//  Source1D_UniformGrad source(a,b);
//
//  PDEClass pde( adv, visc, source );
//
//  // grid: X1
//  XField1D xfld(10,0,10);
//  XField_CellToTrace<PhysD1, TopoD1> xfldCellToTrace(xfld);
//
//  // BR2 discretization
////  Real viscousEtaParameter = 2;
//  DiscretizationVMSD disc;
//
//
//  std::vector<int> bTraceGroups = {0,1};
//  // integrand
//  IntegrandClass fcnI( pde, disc, {0} );
//  IntegrandClass fcnB( pde, disc, bTraceGroups );
//  IntegrandCellClass fcnCell( pde, {0} );
//
//
//  // sequence of solution orders
//  for (int qorder = 1; qorder <= 1; qorder++) //3
//  {
//    Field_CG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Lagrange);
//    Field_EG_Cell<PhysD1, TopoD1, ArrayQ> qpfld(qfld, qorder, BasisFunctionCategory_Lagrange);
//
//    const int qDOF = qfld.nDOF();
//    const int qDOFp = qpfld.nDOF();
//
//
//    // solution data
//    for (int i = 0; i < qDOF; i++)
//      qfld.DOF(i) = sin(PI*i/((Real)qDOF));
//
//    for (int i = 0; i < qDOFp; i++)
//      qpfld.DOF(i) = sin(PI*i/((Real)qDOFp));
//
//    // lifting-operator: P1
//    FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Lagrange);
//    rfld = 0;
//
//    // Compute the field of inverse mass matrices
//    FieldDataInvMassMatrix_Cell mmfld(qfld);
//
//    int quad = 2*qorder+2;
//    QuadratureOrder quadOrder(xfld, quad);
//
//    // jacobian via FD w/ residual operator; assumes scalar PDE
//
//    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal0_copy(qDOF), rsdPDEGlobal1(qDOF);
//    SLA::SparseVector<ArrayQ> rsdPDEGlobalp0(qDOFp), rsdPDEGlobalp0_copy(qDOFp), rsdPDEGlobalp1(qDOFp);
//
//    rsdPDEGlobal0 = 0; rsdPDEGlobalp0 = 0;
//    rsdPDEGlobal0_copy = 0; rsdPDEGlobalp0_copy = 0;
//
//    // Compute the lifting operator field
//    IntegrateInteriorTraceGroups<TopoD1>::integrate(
//        SetFieldInteriorTrace_VMSD_BR2_LiftingOperator(fcnI, mmfld),
//                                                     xfld, (qfld, qpfld, rfld),
//                                                     quadOrder.interiorTraceOrders.data(),
//                                                     quadOrder.interiorTraceOrders.size() );
//
//    IntegrateBoundaryTraceGroups<TopoD1>::integrate(
//        SetFieldInteriorTrace_onBoundary_VMSD_BR2_LiftingOperator(fcnB, mmfld, bTraceGroups),
//                                                     xfld, (qfld, qpfld, rfld),
//                                                     quadOrder.boundaryTraceOrders.data(),
//                                                     quadOrder.boundaryTraceOrders.size() );
//
//
//    IntegrateCellGroups<TopoD1>::integrate( ResidualCell_VMSD_BR2(fcnCell, fcnI, fcnB, xfldCellToTrace,
//                                                              xfld, qfld, qpfld, rfld, quadOrder,
//                                                              rsdPDEGlobal0, rsdPDEGlobalp0),
//                                             xfld, (qfld, qpfld, rfld),  quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );
//
//
//    // wrt q
//    DLA::MatrixD<Real> jacPDE_q(qDOF,qDOF);
//    DLA::MatrixD<Real> jacPDE_qp(qDOF,qDOFp);
//    DLA::MatrixD<Real> jacPDEp_q(qDOFp,qDOF);
//    DLA::MatrixD<Real> jacPDEp_qp(qDOFp,qDOFp);
//
//    jacPDE_q = 0;
//    jacPDE_qp = 0;
//    jacPDEp_q = 0;
//    jacPDEp_qp = 0;
//
//    for (int j = 0; j < qDOF; j++)
//    {
//      qfld.DOF(j) += step;
//
//      // Compute the lifting operator field with perturbed q field
//      IntegrateInteriorTraceGroups<TopoD1>::integrate(
//          SetFieldInteriorTrace_VMSD_BR2_LiftingOperator(fcnI, mmfld),
//                                                       xfld, (qfld, qpfld, rfld),
//                                                       quadOrder.interiorTraceOrders.data(),
//                                                       quadOrder.interiorTraceOrders.size() );
//
//      IntegrateBoundaryTraceGroups<TopoD1>::integrate(
//          SetFieldInteriorTrace_onBoundary_VMSD_BR2_LiftingOperator(fcnB, mmfld, bTraceGroups),
//                                                       xfld, (qfld, qpfld, rfld),
//                                                       quadOrder.boundaryTraceOrders.data(),
//                                                       quadOrder.boundaryTraceOrders.size() );
//
//      // Compute perturbed residual
//      rsdPDEGlobal1 = 0; rsdPDEGlobalp1 = 0;
//      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_VMSD_BR2(fcnCell, fcnI, fcnB, xfldCellToTrace,
//                                                                xfld, qfld, qpfld, rfld, quadOrder,
//                                                                rsdPDEGlobal1, rsdPDEGlobalp1),
//                                               xfld, (qfld, qpfld, rfld),  quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );
//
//      qfld.DOF(j) -= step;
//
//      for (int i = 0; i < qDOF; i++)
//        jacPDE_q(i,j) = (rsdPDEGlobal1[i] - rsdPDEGlobal0[i])/step;
//
//      for (int i = 0; i < qDOFp; i++)
//        jacPDEp_q(i,j) = (rsdPDEGlobalp1[i] - rsdPDEGlobalp0[i])/step;
//    }
//
//
//    for (int j = 0; j < qDOFp; j++)
//    {
//      qpfld.DOF(j) += step;
//
//      // Compute the lifting operator field with perturbed q field
//      IntegrateInteriorTraceGroups<TopoD1>::integrate(
//          SetFieldInteriorTrace_VMSD_BR2_LiftingOperator(fcnI, mmfld),
//                                                       xfld, (qfld, qpfld, rfld),
//                                                       quadOrder.interiorTraceOrders.data(),
//                                                       quadOrder.interiorTraceOrders.size() );
//
//      IntegrateBoundaryTraceGroups<TopoD1>::integrate(
//          SetFieldInteriorTrace_onBoundary_VMSD_BR2_LiftingOperator(fcnB, mmfld, bTraceGroups),
//                                                       xfld, (qfld, qpfld, rfld),
//                                                       quadOrder.boundaryTraceOrders.data(),
//                                                       quadOrder.boundaryTraceOrders.size() );
//
//      // Compute perturbed residual
//      rsdPDEGlobal1 = 0; rsdPDEGlobalp1 = 0;
//
//      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_VMSD_BR2(fcnCell, fcnI, fcnB, xfldCellToTrace,
//                                                                xfld, qfld, qpfld, rfld, quadOrder,
//                                                                rsdPDEGlobal1, rsdPDEGlobalp1),
//                                               xfld, (qfld, qpfld, rfld),  quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );
//
//      qpfld.DOF(j) -= step;
//
//      for (int i = 0; i < qDOF; i++)
//        jacPDE_qp(i,j) = (rsdPDEGlobal1[i] - rsdPDEGlobal0[i])/step;
//
//      for (int i = 0; i < qDOFp; i++)
//        jacPDEp_qp(i,j) = (rsdPDEGlobalp1[i] - rsdPDEGlobalp0[i])/step;
//    }
//
//    // Compute the lifting operator field with original qp field
//    IntegrateInteriorTraceGroups<TopoD1>::integrate(
//        SetFieldInteriorTrace_VMSD_BR2_LiftingOperator(fcnI, mmfld),
//                                                     xfld, (qfld, qpfld, rfld),
//                                                     quadOrder.interiorTraceOrders.data(),
//                                                     quadOrder.interiorTraceOrders.size() );
//
//    IntegrateBoundaryTraceGroups<TopoD1>::integrate(
//        SetFieldInteriorTrace_onBoundary_VMSD_BR2_LiftingOperator(fcnB, mmfld, bTraceGroups),
//                                                     xfld, (qfld, qpfld, rfld),
//                                                     quadOrder.boundaryTraceOrders.data(),
//                                                     quadOrder.boundaryTraceOrders.size() );
//
//    FieldDataMatrixD_Cell<MatrixQ> mtxCompleteResidual(qpfld, qfld);
//    FieldDataMatrixD_BoundaryCell<MatrixQ> boundJacPDE_qp(qfld, qpfld); //auxiliary jacobian for boundary cells
//
//    boundJacPDE_qp= 0;
//
//    DLA::MatrixD<MatrixQ> mtxPDEGlob_q(qDOF,qDOF);
//
//    mtxPDEGlob_q = 0;
//
//    //first test with zeros for initial residual
//    for (int i=0; i<qDOF; i++)
//      rsdPDEGlobal0_copy[i] = rsdPDEGlobal0[i];
//
//    for (int i=0; i<qDOFp; i++)
//      rsdPDEGlobalp0_copy[i] = rsdPDEGlobalp0[i];
//
//    typedef SurrealS<1> SurrealClass;
//    IntegrateCellGroups<TopoD1>::integrate(
//        JacobianCell_VMSD_BR2<SurrealClass>(fcnCell, fcnI, fcnB, xfldCellToTrace,
//                                            xfld, qfld, qpfld, rfld,
//                                            quadOrder, false,
//                                            rsdPDEGlobal0_copy, rsdPDEGlobalp0_copy, mtxPDEGlob_q,
//                                            mtxCompleteResidual, boundJacPDE_qp, mmfld),
//                                            xfld, (qfld, qpfld, rfld),
//                                            quadOrder.cellOrders.data(), quadOrder.cellOrders.size()  );
//
//    DLA::MatrixFactorized< DLA::MatrixDLUSolver, MatrixQ > Afactorized = DLA::InverseLU::Factorize(jacPDEp_qp);
//
//    DLA::MatrixD<Real> AinvB = Afactorized.backsolve(jacPDEp_q);
//    jacPDE_q -= jacPDE_qp*AinvB;
//
//    const Real small_tol = 1e-10;
//    const Real close_tol = 1e-10;
//
//    for (int i = 0; i < qDOF; i++)
//      for (int j = 0; j < qDOF; j++)
//        SANS_CHECK_CLOSE( jacPDE_q(i,j), mtxPDEGlob_q(i,j), small_tol, close_tol );
//
//    DLA::VectorD<Real> r1tmp(qDOFp);
//    DLA::VectorD<Real> r2tmp(qDOF);
//
//    r1tmp = 0;
//    r2tmp = 0;
//    for (int i = 0; i < qDOFp; i++)
//      r1tmp[i] = rsdPDEGlobalp0[i];
//
//    for (int i = 0; i < qDOF; i++)
//      r2tmp[i] = rsdPDEGlobal0[i];
//
//    DLA::VectorD<Real> r1 = Afactorized.backsolve(r1tmp);
//
//    for (int j = 0; j < qDOFp; j++)
//      SANS_CHECK_CLOSE( r1[j], rsdPDEGlobalp0_copy[j], small_tol, close_tol );
//
//    DLA::VectorD<Real> r2 = r2tmp - jacPDE_qp*r1;
//
//    for (int j = 0; j < qDOF; j++)
//      SANS_CHECK_CLOSE( r2[j], rsdPDEGlobal0_copy[j], small_tol, close_tol );
//
//  }
//}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( VMSD_2D_Box_UnionJack_Triangle_X1_SourceGrad )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad> PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
//  typedef DLA::MatrixS<1,PhysD2::D,MatrixQ> RowMatrixQ;

  typedef IntegrandTrace_VMSD_BR2<PDEClass> IntegrandClass;
  typedef IntegrandCell_VMSD_BR2<PDEClass> IntegrandCellClass;

  Real step = 1.0;

  Real u = 1;
  Real v = 0.2;
//  Real u = 0;
//  Real v = 0;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.348;
  Real kyy = 1.007;
//  Real kxx = 0;
//  Real kxy = 0;
//  Real kyx = 0;
//  Real kyy = 0;
  ViscousFlux2D_Uniform visc(kxx, kxy,
                             kyx, kyy);
//  ViscousFlux2D_None visc;
  Real a = 2.1, b = 5.6, c = -0.34; // With source gradient
//  Real a = 0, b = 0, c = 0; // With source gradient
  Source2D_UniformGrad source(a,b,c);
//  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // grid: X1
  XField2D_Box_UnionJack_Triangle_X1 xfld(4, 4);
  XField_CellToTrace<PhysD2, TopoD2> xfldCellToTrace(xfld);

  std::vector<int> iTraceGroups;
  for (int i=0; i<xfld.nInteriorTraceGroups(); i++)
    iTraceGroups.push_back(i);

  std::vector<int> bTraceGroups;
  for (int i=0; i<xfld.nBoundaryTraceGroups(); i++)
    bTraceGroups.push_back(i);

  // solution: P1
  for (int qorder = 1; qorder <= 2; qorder++)
  {
    Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Lagrange);
    Field_EG_Cell<PhysD2, TopoD2, ArrayQ> qpfld(qfld, qorder, BasisFunctionCategory_Lagrange);

    const int qDOF = qfld.nDOF();
    const int qDOFp = qpfld.nDOF();

    // solution data
    for (int i = 0; i < qDOF; i++)
      qfld.DOF(i) = sin(PI*i/((Real)qDOF));

    for (int i = 0; i < qDOFp; i++)
      qpfld.DOF(i) = sin(PI*i/((Real)qDOFp));

    // lifting-operator: P1
    FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Lagrange);
    rfld = 0;

    // Compute the field of inverse mass matrices
    FieldDataInvMassMatrix_Cell mmfld(qfld);

    // BR2 discretization
//    Real viscousEtaParameter = 6;
    DiscretizationVMSD disc;

    // integrand
    IntegrandClass fcnI( pde, disc, iTraceGroups );
    IntegrandClass fcnB( pde, disc, bTraceGroups );
    IntegrandCellClass fcnCell( pde, {0} );

    int quad = 2*qorder+2;
    QuadratureOrder quadOrder(xfld, quad);

    // jacobian via FD w/ residual operator; assumes scalar PDE

    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal0_copy(qDOF), rsdPDEGlobal1(qDOF);
    SLA::SparseVector<ArrayQ> rsdPDEGlobalp0(qDOFp), rsdPDEGlobalp0_copy(qDOFp), rsdPDEGlobalp1(qDOFp);

    rsdPDEGlobal0_copy = 0; rsdPDEGlobalp0_copy = 0;

//     Compute the lifting operator field
    IntegrateCellGroups<TopoD2>::integrate(
          SetLiftingOperator_VMSD_BR2(fcnCell, fcnI, fcnB, xfldCellToTrace,
                                      xfld, qfld, qpfld, rfld,
                                      quadOrder, mmfld),
                                      xfld, (qfld, qpfld, rfld),
          quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );


    rsdPDEGlobal0 = 0; rsdPDEGlobalp0 = 0;

    IntegrateCellGroups<TopoD2>::integrate( ResidualCell_VMSD_BR2(fcnCell, fcnI, fcnB, xfldCellToTrace,
                                                                  xfld, qfld, qpfld, rfld, quadOrder,
                                                                  rsdPDEGlobal0, rsdPDEGlobalp0),
                                            xfld, (qfld, qpfld, rfld),  quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );


    // wrt q
    DLA::MatrixD<Real> jacPDE_q(qDOF,qDOF);
    DLA::MatrixD<Real> jacPDE_qp(qDOF,qDOFp);
    DLA::MatrixD<Real> jacPDEp_q(qDOFp,qDOF);
    DLA::MatrixD<Real> jacPDEp_qp(qDOFp,qDOFp);

    jacPDE_q = 0;
    jacPDE_qp = 0;
    jacPDEp_q = 0;
    jacPDEp_qp = 0;

    for (int j = 0; j < qDOF; j++)
    {
      qfld.DOF(j) += step;

      // Compute the lifting operator field with perturbed q field
      IntegrateCellGroups<TopoD2>::integrate(
            SetLiftingOperator_VMSD_BR2(fcnCell, fcnI, fcnB, xfldCellToTrace,
                                                      xfld, qfld, qpfld, rfld,
                                                      quadOrder, mmfld),
                                                      xfld, (qfld, qpfld, rfld),
                                                      quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

      // Compute perturbed residual
      rsdPDEGlobal1 = 0; rsdPDEGlobalp1 = 0;
      IntegrateCellGroups<TopoD2>::integrate( ResidualCell_VMSD_BR2(fcnCell, fcnI, fcnB, xfldCellToTrace,
                                                                xfld, qfld, qpfld, rfld, quadOrder,
                                                                rsdPDEGlobal1, rsdPDEGlobalp1),
                                               xfld, (qfld, qpfld, rfld),  quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

      qfld.DOF(j) -= step;

      for (int i = 0; i < qDOF; i++)
        jacPDE_q(i,j) = (rsdPDEGlobal1[i] - rsdPDEGlobal0[i])/step;

      for (int i = 0; i < qDOFp; i++)
        jacPDEp_q(i,j) = (rsdPDEGlobalp1[i] - rsdPDEGlobalp0[i])/step;
    }


    for (int j = 0; j < qDOFp; j++)
    {
      qpfld.DOF(j) += step;

      // Compute the lifting operator field with perturbed q field
      IntegrateCellGroups<TopoD2>::integrate(
            SetLiftingOperator_VMSD_BR2(fcnCell, fcnI, fcnB, xfldCellToTrace,
                                                      xfld, qfld, qpfld, rfld,
                                                      quadOrder, mmfld),
                                                      xfld, (qfld, qpfld, rfld),
                                                      quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

      // Compute perturbed residual
      rsdPDEGlobal1 = 0; rsdPDEGlobalp1 = 0;

      IntegrateCellGroups<TopoD2>::integrate( ResidualCell_VMSD_BR2(fcnCell, fcnI, fcnB, xfldCellToTrace,
                                                                xfld, qfld, qpfld, rfld, quadOrder,
                                                                rsdPDEGlobal1, rsdPDEGlobalp1),
                                               xfld, (qfld, qpfld, rfld),  quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );

      qpfld.DOF(j) -= step;

      for (int i = 0; i < qDOF; i++)
        jacPDE_qp(i,j) = (rsdPDEGlobal1[i] - rsdPDEGlobal0[i])/step;

      for (int i = 0; i < qDOFp; i++)
        jacPDEp_qp(i,j) = (rsdPDEGlobalp1[i] - rsdPDEGlobalp0[i])/step;
    }

    // Compute the lifting operator field with perturbed q field
    IntegrateCellGroups<TopoD2>::integrate(
          SetLiftingOperator_VMSD_BR2(fcnCell, fcnI, fcnB, xfldCellToTrace,
                                                    xfld, qfld, qpfld, rfld,
                                                    quadOrder, mmfld),
                                                    xfld, (qfld, qpfld, rfld),
                                                    quadOrder.cellOrders.data(), quadOrder.cellOrders.size() );



    // NON-CONDENSED VERSION

    FieldDataMatrixD_BoundaryCell<MatrixQ> boundJacPDE_qp(qfld, qpfld); //auxiliary jacobian for boundary cells
    boundJacPDE_qp = 0;

    FieldDataMatrixD_BoundaryCell<MatrixQ> boundJacPDEp_q(qpfld, qfld); //auxiliary jacobian for boundary cells
    boundJacPDEp_q = 0;

    DLA::MatrixD<MatrixQ> mtxPDEGlob_q(qDOF,qDOF);
    DLA::MatrixD<MatrixQ> mtxPDEGlob_qp(qDOF,qDOFp);
    DLA::MatrixD<MatrixQ> mtxPDEpGlob_q(qDOFp,qDOF);
    DLA::MatrixD<MatrixQ> mtxPDEpGlob_qp(qDOFp,qDOFp);

    mtxPDEGlob_q = 0;
    mtxPDEGlob_qp = 0;
    mtxPDEpGlob_q = 0;
    mtxPDEpGlob_qp = 0;

    typedef SurrealS<1> SurrealClass;
    IntegrateCellGroups<TopoD2>::integrate(
        JacobianCell_VMSD_BR2_Uncondensed<SurrealClass>(fcnCell, fcnI, fcnB, xfldCellToTrace,
                                            xfld, qfld, qpfld, rfld,
                                            quadOrder,
                                            mtxPDEGlob_q, mtxPDEGlob_qp, mtxPDEpGlob_q,  mtxPDEpGlob_qp,
                                            boundJacPDE_qp, boundJacPDEp_q, mmfld),
                                            xfld, (qfld, qpfld, rfld),
                                            quadOrder.cellOrders.data(), quadOrder.cellOrders.size()  );


    const Real small_tol = 2e-8;
    const Real close_tol = 2e-8;
    for (int i = 0; i < qDOF; i++)
      for (int j = 0; j < qDOF; j++)
        SANS_CHECK_CLOSE( jacPDE_q(i,j), mtxPDEGlob_q(i,j), small_tol, close_tol );

    for (int i = 0; i < qDOF; i++)
      for (int j = 0; j < qDOFp; j++)
        SANS_CHECK_CLOSE( jacPDE_qp(i,j), mtxPDEGlob_qp(i,j), small_tol, close_tol );

    for (int i = 0; i < qDOFp; i++)
      for (int j = 0; j < qDOF; j++)
        SANS_CHECK_CLOSE( jacPDEp_q(i,j), mtxPDEpGlob_q(i,j), small_tol, close_tol );

    for (int i = 0; i < qDOFp; i++)
      for (int j = 0; j < qDOFp; j++)
        SANS_CHECK_CLOSE( jacPDEp_qp(i,j), mtxPDEpGlob_qp(i,j), small_tol, close_tol );

    //STATIC CONDENSED VERSION
    FieldDataMatrixD_Cell<MatrixQ> mtxCompleteResidual(qpfld, qfld);

    DLA::MatrixD<MatrixQ> mtxPDEGlob_q2(qDOF,qDOF);

    mtxPDEGlob_q2 = 0;

    FieldDataMatrixD_Cell<MatrixQ> dummyjac;

    //first test with zeros for initial residual
    for (int i=0; i<qDOF; i++)
      rsdPDEGlobal0_copy[i] = rsdPDEGlobal0[i];

    for (int i=0; i<qDOFp; i++)
      rsdPDEGlobalp0_copy[i] = rsdPDEGlobalp0[i];

    IntegrateCellGroups<TopoD2>::integrate(
        JacobianCell_VMSD_BR2<SurrealClass>(fcnCell, fcnI, fcnB, xfldCellToTrace,
                                            xfld, qfld, qpfld, rfld,
                                            quadOrder, false,
                                            rsdPDEGlobal0_copy, rsdPDEGlobalp0_copy, mtxPDEGlob_q2, dummyjac, dummyjac,
                                            mtxCompleteResidual, boundJacPDE_qp, boundJacPDEp_q, mmfld),
                                            xfld, (qfld, qpfld, rfld),
                                            quadOrder.cellOrders.data(), quadOrder.cellOrders.size()  );

    DLA::MatrixFactorized< DLA::MatrixDLUSolver, MatrixQ > Afactorized = DLA::InverseLU::Factorize(jacPDEp_qp);

    DLA::MatrixD<Real> AinvB = Afactorized.backsolve(jacPDEp_q);
    jacPDE_q -= jacPDE_qp*AinvB;

    for (int i = 0; i < qDOF; i++)
      for (int j = 0; j < qDOF; j++)
        SANS_CHECK_CLOSE( jacPDE_q(i,j), mtxPDEGlob_q2(i,j), small_tol, close_tol );

    DLA::VectorD<Real> r1tmp(qDOFp);
    DLA::VectorD<Real> r2tmp(qDOF);

    for (int i = 0; i < qDOFp; i++)
      r1tmp[i] = rsdPDEGlobalp0[i];

    for (int i = 0; i < qDOF; i++)
      r2tmp[i] = rsdPDEGlobal0[i];

    DLA::VectorD<Real> r1 = Afactorized.backsolve(r1tmp);

    for (int j = 0; j < qDOFp; j++)
      SANS_CHECK_CLOSE( r1[j], rsdPDEGlobalp0_copy[j], small_tol, close_tol );

    DLA::VectorD<Real> r2 = r2tmp - jacPDE_qp*r1;

    for (int j = 0; j < qDOF; j++)
    {
      SANS_CHECK_CLOSE( r2[j], rsdPDEGlobal0_copy[j], small_tol, close_tol );
    }
  }
}
//
////----------------------------------------------------------------------------//
//BOOST_AUTO_TEST_CASE( DGBR2_3D_Tet_Tet_X1_SourceUniform )
//{
//  typedef PDEAdvectionDiffusion<PhysD3,
//                                AdvectiveFlux3D_Uniform,
//                                ViscousFlux3D_Uniform,
//                                Source3D_Uniform > PDEAdvectionDiffusion3D;
//  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
//  typedef PDEClass::template ArrayQ<Real> ArrayQ;
//  typedef PDEClass::template MatrixQ<Real> MatrixQ;
//  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
//  typedef DLA::MatrixS<1,PhysD3::D,MatrixQ> RowMatrixQ;
//
//  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;
//
//  // global communicator
//  mpi::communicator world;
//
//  // split the communicator accross all processors
//  mpi::communicator comm = world.split(world.rank());
//
//  Real u = 1.1;
//  Real v = 0.2;
//  Real w = 0.7;
//  AdvectiveFlux3D_Uniform adv(u, v, w);
//
//  Real kxx = 2.123; Real kxy = 0.553; Real kxz = 0.760;
//  Real kyx = 0.343; Real kyy = 1.007; Real kyz = 0.870;
//  Real kzx = 0.690; Real kzy = 0.365; Real kzz = 1.460;
//
//  ViscousFlux3D_Uniform visc(kxx, kxy, kxz,
//                             kyx, kyy, kyz,
//                             kzx, kzy, kzz);
//
//  Real a = 2.1; // no source gradient
//  Source3D_Uniform source(a);
//
//  PDEClass pde( adv, visc, source );
//
//  // grid: X1
//  XField3D_Box_Tet_X1 xfld(comm, 2, 2, 2);
//
//  // solution: P1
//  for ( int qorder = 1; qorder <= 2; qorder++)
//  {
//    Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
//
//    const int qDOF = qfld.nDOF();
//
//    // solution data
//    for (int i = 0; i < qDOF; i++)
//      qfld.DOF(i) = sin(PI*i/((Real)qDOF));
//
//    // lifting operator: P1
//    FieldLift_DG_Cell<PhysD3, TopoD3, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
//
//    // Compute the field of inverse mass matrices
//    FieldDataInvMassMatrix_Cell mmfld(qfld);
//
//    // BR2 discretization
//    Real viscousEtaParameter = 6;
//    DiscretizationDGBR2 disc(0, viscousEtaParameter);
//
//    // integrand
//    IntegrandClass fcn( pde, disc, {0} );
//
//    // quadrature rule (2*P for basis and flux polynomials)
//    std::vector<int> quadratureOrder = {2*qorder};
//
//    // jacobian via FD w/ residual operator; assumes scalar PDE
//
//    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
//    DLA::MatrixD<Real> jacPDE_q(qDOF,qDOF);
//
//    // Compute the lifting operator field
//    IntegrateInteriorTraceGroups<TopoD3>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
//                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());
//
//    rsdPDEGlobal0 = 0;
//    IntegrateInteriorTraceGroups<TopoD3>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal0),
//                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());
//
//    //wrt q
//    for (int j = 0; j < qDOF; j++)
//    {
//      qfld.DOF(j) += 1;
//
//      // Compute the lifting operator field with perturbed q field
//      IntegrateInteriorTraceGroups<TopoD3>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
//                                                      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());
//
//      rsdPDEGlobal1 = 0;
//      IntegrateInteriorTraceGroups<TopoD3>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal1),
//                                                      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());
//
//      qfld.DOF(j) -= 1;
//
//      for (int i = 0; i < qDOF; i++)
//        jacPDE_q(i,j) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
//    }
//
//    // jacobian via Surreal
//
//    FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld); jacPDE_R = 0; // Lifitng operator jacobian from cell integral
//
//    DLA::MatrixD<MatrixQ> mtxPDEGlob_q(qDOF,qDOF);
//
//    mtxPDEGlob_q = 0;
//
//    IntegrateInteriorTraceGroups<TopoD3>::integrate(
//        JacobianInteriorTrace_DGBR2(fcn, mmfld, jacPDE_R, mtxPDEGlob_q),
//        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());
//
//    const Real small_tol = 5e-11;
//    const Real close_tol = 5e-11;
//
//    for (int i = 0; i < qDOF; i++)
//      for (int j = 0; j < qDOF; j++)
//        SANS_CHECK_CLOSE( jacPDE_q(i,j), mtxPDEGlob_q(i,j), small_tol, close_tol );
//  }
//}
//
////----------------------------------------------------------------------------//
//BOOST_AUTO_TEST_CASE( DGBR2_3D_Tet_Tet_X1_SourceGrad )
//{
//  typedef PDEAdvectionDiffusion<PhysD3,
//                                AdvectiveFlux3D_Uniform,
//                                ViscousFlux3D_Uniform,
//                                Source3D_UniformGrad > PDEAdvectionDiffusion3D;
//  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
//  typedef PDEClass::template ArrayQ<Real> ArrayQ;
//  typedef PDEClass::template MatrixQ<Real> MatrixQ;
//  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
//  typedef DLA::MatrixS<1,PhysD3::D,MatrixQ> RowMatrixQ;
//
//  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;
//  typedef IntegrandCell_DGBR2<PDEClass> IntegrandCellClass;
//
//  // global communicator
//  mpi::communicator world;
//
//  // split the communicator accross all processors
//  mpi::communicator comm = world.split(world.rank());
//
//  Real u = 1.1;
//  Real v = 0.2;
//  Real w = 0.7;
//  AdvectiveFlux3D_Uniform adv(u, v, w);
//
//  Real kxx = 2.123; Real kxy = 0.553; Real kxz = 0.760;
//  Real kyx = 0.343; Real kyy = 1.007; Real kyz = 0.870;
//  Real kzx = 0.690; Real kzy = 0.365; Real kzz = 1.460;
//
//  ViscousFlux3D_Uniform visc(kxx, kxy, kxz,
//                             kyx, kyy, kyz,
//                             kzx, kzy, kzz);
//
//  Real a = 2.1, b = 5.6, c = -0.34, d = 0.67; // With source gradient
//  Source3D_UniformGrad source(a,b,c,d);
//
//  PDEClass pde( adv, visc, source );
//
//  // grid: X1
//  XField3D_Box_Tet_X1 xfld(comm, 2, 2, 2);
//
//  // solution: P1
//  for ( int qorder = 1; qorder <= 2; qorder++)
//  {
//    Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
//
//    const int qDOF = qfld.nDOF();
//
//    // solution data
//    for (int i = 0; i < qDOF; i++)
//      qfld.DOF(i) = sin(PI*i/((Real)qDOF));
//
//    // lifting operator: P1
//    FieldLift_DG_Cell<PhysD3, TopoD3, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
//
//    // Compute the field of inverse mass matrices
//    FieldDataInvMassMatrix_Cell mmfld(qfld);
//
//    // BR2 discretization
//    Real viscousEtaParameter = 6;
//    DiscretizationDGBR2 disc(0, viscousEtaParameter);
//
//    // integrand
//    IntegrandClass fcn( pde, disc, {0} );
//    IntegrandCellClass fcnCell( pde, disc, {0} );
//
//    // quadrature rule (2*P for basis and flux polynomials)
//    std::vector<int> quadratureOrder = {2*qorder};
//
//    // jacobian via FD w/ residual operator; assumes scalar PDE
//
//    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
//    DLA::MatrixD<Real> jacPDE_q(qDOF,qDOF);
//
//    // Compute the lifting operator field
//    IntegrateInteriorTraceGroups<TopoD3>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
//                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());
//
//    rsdPDEGlobal0 = 0;
//    IntegrateCellGroups<TopoD3>::integrate( ResidualCell_DGBR2(fcnCell, rsdPDEGlobal0),
//                                            xfld, (qfld, rfld), quadratureOrder.data(), 1 );
//
//    IntegrateInteriorTraceGroups<TopoD3>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal0),
//                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());
//
//    //wrt q
//    for (int j = 0; j < qDOF; j++)
//    {
//      qfld.DOF(j) += 1;
//
//      // Compute the lifting operator field with perturbed q field
//      IntegrateInteriorTraceGroups<TopoD3>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
//                                                      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());
//
//      rsdPDEGlobal1 = 0;
//      IntegrateCellGroups<TopoD3>::integrate( ResidualCell_DGBR2(fcnCell, rsdPDEGlobal1),
//                                              xfld, (qfld, rfld), quadratureOrder.data(), 1 );
//
//      IntegrateInteriorTraceGroups<TopoD3>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal1),
//                                                      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());
//
//      qfld.DOF(j) -= 1;
//
//      for (int i = 0; i < qDOF; i++)
//        jacPDE_q(i,j) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
//    }
//
//    // jacobian via Surreal
//
//    FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld); jacPDE_R = 0; // Lifitng operator jacobian from cell integral
//
//    DLA::MatrixD<MatrixQ> mtxPDEGlob_q(qDOF,qDOF);
//
//    mtxPDEGlob_q = 0;
//    IntegrateCellGroups<TopoD3>::integrate( JacobianCell_DGBR2(fcnCell, mtxPDEGlob_q, jacPDE_R),
//                                            xfld, (qfld, rfld), quadratureOrder.data(), 1 );
//
//    IntegrateInteriorTraceGroups<TopoD3>::integrate(
//        JacobianInteriorTrace_DGBR2(fcn, mmfld, jacPDE_R, mtxPDEGlob_q),
//        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());
//
//    const Real small_tol = 1e-10;
//    const Real close_tol = 6e-10;
//
//    for (int i = 0; i < qDOF; i++)
//      for (int j = 0; j < qDOF; j++)
//        SANS_CHECK_CLOSE( jacPDE_q(i,j), mtxPDEGlob_q(i,j), small_tol, close_tol );
//  }
//}
//
//
////----------------------------------------------------------------------------//
//BOOST_AUTO_TEST_CASE( DGBR2_3D_Hex_Periodic_X1_SourceUniform )
//{
//  typedef PDEAdvectionDiffusion<PhysD3,
//                                AdvectiveFlux3D_Uniform,
//                                ViscousFlux3D_Uniform,
//                                Source3D_Uniform > PDEAdvectionDiffusion3D;
//  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
//  typedef PDEClass::template ArrayQ<Real> ArrayQ;
//  typedef PDEClass::template MatrixQ<Real> MatrixQ;
//  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
//  typedef DLA::MatrixS<1,PhysD3::D,MatrixQ> RowMatrixQ;
//
//  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;
//
//  // global communicator
//  mpi::communicator world;
//
//  // split the communicator accross all processors
//  mpi::communicator comm = world.split(world.rank());
//
//  Real u = 1.1;
//  Real v = 0.2;
//  Real w = 0.7;
//  AdvectiveFlux3D_Uniform adv(u, v, w);
//
//  Real kxx = 2.123; Real kxy = 0.553; Real kxz = 0.760;
//  Real kyx = 0.343; Real kyy = 1.007; Real kyz = 0.870;
//  Real kzx = 0.690; Real kzy = 0.365; Real kzz = 1.460;
//
//  ViscousFlux3D_Uniform visc(kxx, kxy, kxz,
//                             kyx, kyy, kyz,
//                             kzx, kzy, kzz);
//
//  Real a = 2.1; // no source gradient
//  Source3D_Uniform source(a);
//
//  PDEClass pde( adv, visc, source );
//
//  // grid: X1
//  XField3D_Box_Hex_X1 xfld(comm, 3, 3, 3, {{true, true, true}});
//
//  // solution: P1
//  for ( int qorder = 1; qorder < 2; qorder++)
//  {
//    Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
//
//    const int qDOF = qfld.nDOF();
//
//    // solution data
//    for (int i = 0; i < qDOF; i++)
//      qfld.DOF(i) = sin(PI*i/((Real)qDOF));
//
//    // lifting operator: P1
//    FieldLift_DG_Cell<PhysD3, TopoD3, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
//
//    // Compute the field of inverse mass matrices
//    FieldDataInvMassMatrix_Cell mmfld(qfld);
//
//    // BR2 discretization
//    Real viscousEtaParameter = Hex::NTrace;
//    DiscretizationDGBR2 disc(0, viscousEtaParameter);
//
//    // integrand
//    IntegrandClass fcn( pde, disc, {0} );
//
//    // quadrature rule (2*P for basis and flux polynomials)
//    std::vector<int> quadratureOrder = {2*qorder, 2*qorder, 2*qorder, 2*qorder, 2*qorder, 2*qorder};
//
//    // jacobian via FD w/ residual operator; assumes scalar PDE
//
//    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
//    DLA::MatrixD<Real> jacPDE_q(qDOF,qDOF);
//
//    // Compute the lifting operator field
//    IntegratePeriodicTraceGroups<TopoD3>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
//                                                    xfld, (qfld, rfld), quadratureOrder.data(),quadratureOrder.size());
//
//    rsdPDEGlobal0 = 0;
//    IntegratePeriodicTraceGroups<TopoD3>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal0),
//                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());
//
//    //wrt q
//    for (int j = 0; j < qDOF; j++)
//    {
//      qfld.DOF(j) += 1;
//
//      // Compute the lifting operator field with perturbed q field
//      IntegratePeriodicTraceGroups<TopoD3>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
//                                                      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());
//
//      rsdPDEGlobal1 = 0;
//      IntegratePeriodicTraceGroups<TopoD3>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal1),
//                                                      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());
//
//      qfld.DOF(j) -= 1;
//
//      for (int i = 0; i < qDOF; i++)
//        jacPDE_q(i,j) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
//    }
//
//    // jacobian via Surreal
//
//    FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld); jacPDE_R = 0; // Lifitng operator jacobian from cell integral
//
//    DLA::MatrixD<MatrixQ> mtxPDEGlob_q(qDOF,qDOF);
//
//    mtxPDEGlob_q = 0;
//
//    IntegratePeriodicTraceGroups<TopoD3>::integrate(
//        JacobianInteriorTrace_DGBR2(fcn, mmfld, jacPDE_R, mtxPDEGlob_q),
//        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());
//
//    const Real small_tol = 5e-11;
//    const Real close_tol = 5e-11;
//
//    for (int i = 0; i < qDOF; i++)
//      for (int j = 0; j < qDOF; j++)
//        SANS_CHECK_CLOSE( jacPDE_q(i,j), mtxPDEGlob_q(i,j), small_tol, close_tol );
//  }
//}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
