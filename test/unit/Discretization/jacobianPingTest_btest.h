// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef TEST_SYSTEM_BLOCKSOLVE_JACOBIANPINGTEST_BTEST_H_
#define TEST_SYSTEM_BLOCKSOLVE_JACOBIANPINGTEST_BTEST_H_

//#define ISTIMINGJacobianPingTest // turned on when refactoring this code for speed-up

#include <boost/test/unit_test.hpp>

#include <ostream>
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/MatrixSize.h"
#include "LinearAlgebra/DenseLinAlg/tools/MatrixS_or_T.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"

#ifdef ISTIMINGJacobianPingTest
#include "tools/timer.h"
#endif

namespace SANS
{

//===========================================================================//
namespace detail_jacobianPingTest
{

// Carry out ping tests on jacobian
struct jacobianPingTest
{
  template <class MatrixQ, class BlockMatrixType, class FullVectorType, class BlockEqnVector, class BlockVarVector, class FullAESType>
  static void ping_block(const BlockMatrixType& jac_block,
                         FullVectorType& rsdfullp, FullVectorType& rsdfullm, BlockEqnVector& rsdblockp, BlockEqnVector& rsdblockm,
                         FullVectorType& qfull, BlockVarVector& qblock, FullAESType& BlockAES,
                         const std::vector<Real>& step_vec, const typename DLA::VectorS_or_T<DLA::MatrixSize<MatrixQ>::N, Real>::type& qArrayQscale,
                         const std::vector<Real>& rate_range = {1.9,2.1}, // for 2nd-order center differencing
                         const bool verbose = false, const Real nonzero_tol = 1e-10, const Real small_tol = 1e-10,
                         const bool hasNonzeroComponent = true);
  //TODO: this default tolerances might not be very sound but is chosen so due to other existing ping tests
};

// carry out ping tests on a jacobian (e.g. 2x2 or 4x4 block system, or just ordinary system (aka 1x1 block system)
template <class MatrixQ, class BlockMatrixType, class FullVectorType, class BlockEqnVector, class BlockVarVector, class FullAESType>
void
jacobianPingTest::
ping_block(const BlockMatrixType& jac_block,
           FullVectorType& rsdfullp, FullVectorType& rsdfullm, BlockEqnVector& rsdblockp, BlockEqnVector& rsdblockm,
           FullVectorType& qfull, BlockVarVector& qblock, FullAESType& BlockAES,
           const std::vector<Real>& step_vec, const typename DLA::VectorS_or_T<DLA::MatrixSize<MatrixQ>::N, Real>::type& qArrayQscale,
           const std::vector<Real>& rate_range,
           const bool verbose, const Real nonzero_tol, const Real small_tol,
           const bool hasNonzeroComponent)
{
  // Carry out ping tests on a jacobian block jac_block by comparing it against derivatives computed from central differencing.
  // rsdfullp, rsdfullm: global residual R(Q) evaluated at R(Q+dQ) (plus) and R(Q-dQ) (minus) respectively. rsdfull can be of type
  //   e.g. BLA::VectorBlock_4< DLA::VectorD< SLA::SparseVector< ArrayQ > > > or e.g. DLA::VectorD< SLA::SparseVector< ArrayQ >.
  // rsdblockp, rsdblockm: references to sub-blocks (or the whole) of rsdfullp, rsdfullm respectively.
  // qfull: global variable Q
  // qblock: reference to sub-block (or the whole) of qfull.
  //
  // Note that jac_block := d(rsdblock)/d(qblock).
  //
  // Assume that (1) jac_block is of the type DLA::MatrixD< SLA::SparseMatrix_CRS< MatrixQ > > where MatrixQ is statically sized and is either
  // DLA::MatrixS<M,N,Real> or Real; (2) qblock is of the type DLA::VectorD< SLA::SparseVector< ArrayQ > > where ArrayQ is statically sized
  // and is either DLA::VectorS<M,Real> or Real.
  //
  // step_vec: size-2 vector storing finite difference step size
  // hasNonzeroComponent: Set it false if you know beforehand that this block should have no nonzero element; this speeds up the test.
  //                      By default, it should be true, aka assuming that the block has nonzero element(s).

  // Deduce sizes of MatrixQ
  const int M = DLA::MatrixSize<MatrixQ>::M;
  const int N = DLA::MatrixSize<MatrixQ>::N;

  // Sanity checks
  BOOST_CHECK( (std::is_same< DLA::MatrixD< SLA::SparseMatrix_CRS< MatrixQ > >, BlockMatrixType >::value) );
  BOOST_CHECK( (std::is_same< DLA::VectorD< SLA::SparseVector< typename DLA::VectorS_or_T<M,Real>::type > >, BlockEqnVector >::value) );
  BOOST_CHECK( (std::is_same< DLA::VectorD< SLA::SparseVector< typename DLA::VectorS_or_T<N,Real>::type > >, BlockVarVector >::value) );

  const int nstep = step_vec.size(); // number of finite-difference steps used for ping tests
  BOOST_REQUIRE(nstep==2); // we choose to use two step sizes to carry out ping tests

  // Loop over components (sub-blocks) of MatrixD
  for (int ii = 0; ii < jac_block.m(); ii++)
  {
    for (int jj = 0; jj < jac_block.n(); jj++)
    {
      if (verbose)
      {
        std::cout << std::endl << "-*-*-*-*-*-  DLA::MatrixD< SLA::SparseMatrix_CRS< MatrixQ > >(ii,jj): jac_block("<<ii<<","<<jj<<")"
                  << " (size = [" << jac_block(ii,jj).m() << "," << jac_block(ii,jj).n()
                  << "]; MatrixQ size = ["<<M<<","<<N<<"]) -*-*-*-*-*- " << std::endl << std::endl;
      }

      // Loop over each nonzero component (aka MatrixQ) of SparseMatrix_CRS
      for (int i = 0; i < jac_block(ii,jj).m(); i++)
      {
        std::vector<MatrixQ> Ajac;
        std::vector<int> iAjac;

        if (verbose)
          std::cout << "---- SLA::SparseMatrix_CRS< MatrixQ > Row i: " << i;

#ifdef ISTIMINGJacobianPingTest
        timer time_picknonzero; // start timing the whole unit test
#endif
        // ----------------------------------------------------------------------------------
        if (hasNonzeroComponent)
        {
          // Collect all the nonzero MatrixQ components in jac_block(ii,jj)
          if (jac_block(ii,jj).getNumNonZero() > 0)
          {
            int row_nonzeros = jac_block(ii,jj).rowNonZero(i);

            if (verbose)
              std::cout << ", number of allocated nonzero MatrixQ = " << row_nonzeros
                        << " (values of entries are still allowed to be zero)" << std::endl;

            // Pick out MatrixQ with nonzero entries
            for (int j = 0; j < row_nonzeros; j++) // MatrixQ column
            {
              bool anyNonZero = false;
              for (int m = 0; m < M && !anyNonZero; m++)
                for (int n = 0; n < N && !anyNonZero; n++)
                  if ( fabs( DLA::index(jac_block(ii,jj).sparseRow(i,j), m, n) ) > nonzero_tol) anyNonZero = true;

              if (anyNonZero)
              {
                // Save the non-zero MatrixQ and its column index in SLA::SparseMatrix_CRS< MatrixQ >
                Ajac.push_back(jac_block(ii,jj).sparseRow(i,j));
                iAjac.push_back( jac_block(ii,jj).get_col_ind()[ jac_block(ii,jj).get_row_ptr()[i] + j ] );
              }
            } // j loop
          } // if getNumNonZero
          else
          {
            if (verbose) std::cout << ", number of nonzero MatrixQ = 0" << " ----" << std::endl;
          }
        }
        else
        {
          // Caution: only if you already knew and explicitly specified that this block does not have any nonzero
          if (verbose)
          {
            std::cout << ", Don't even don't bother finite differencing since the current block is known/set to be zero!" << std::endl;
          }
          continue;
        }

#ifdef ISTIMINGJacobianPingTest
        std::cout << "Wall-clock time of picking out nonzeros : "
                  << time_picknonzero.elapsed() << " second(s)" << std::endl;

        timer time_jacobianFD; // start timing the whole unit test
#endif
        // ----------------------------------------------------------------------------------
        // Compute jacobian (jac_block) using central differencing
        //   Note that the result is exact (up to numerical precision) for a linear or quadratic residuals R(Q)
        //   but approximate for general nonlinear residual.
        std::vector<std::vector<MatrixQ> > fdjac(nstep);
        std::vector<std::vector<int> > ifdjac(nstep);

        for (int i_fd_step = 0; i_fd_step < nstep; i_fd_step++) // finite-difference step sizes
        {
          for (int j = 0; j < qblock[jj].m(); j++) // ArrayQ in qblock[jj]
          {
            MatrixQ fd = 0;

            for (int n = 0; n < N; n++) // entry in ArrayQ
            {
              rsdfullp = 0;
              // Note: scale the step size depending on pre-defined scale (problem-specific) to avoid excessively small/large FD step sizes
              const Real step_size = fabs(DLA::index(qArrayQscale, n))*step_vec[i_fd_step];

              DLA::index(qblock[jj][j],n) += step_size;
              BlockAES.residual(qfull, rsdfullp);

              rsdfullm = 0;
              DLA::index(qblock[jj][j],n) -= 2*step_size;
              BlockAES.residual(qfull, rsdfullm);

              DLA::index(qblock[jj][j],n) += step_size; // reset to input solution

              for (int m = 0; m < M; m++) // row in MatrixQ
                DLA::index(fd,m,n) = ( DLA::index(rsdblockp[ii][i],m) - DLA::index(rsdblockm[ii][i],m) )/(2*step_size);
            }

            // pick out MatrixQ with nonzero entries
            bool anyNonZero = false;
            for (int m = 0; m < M && !anyNonZero; m++)
              for (int n = 0; n < N && !anyNonZero; n++)
                if ( fabs(DLA::index(fd,m,n)) > nonzero_tol ) anyNonZero = true;

            if ( anyNonZero )
            {
              fdjac[i_fd_step].push_back(fd);
              ifdjac[i_fd_step].push_back(j);
            }
          } //j loop
        } //i_fd_step loop

#ifdef ISTIMINGJacobianPingTest
        std::cout << "Wall-clock time of computing jacobian by finite difference : "
                  << time_jacobianFD.elapsed() << " second(s)" << std::endl;

        timer time_jacobianCompare; // start timing the whole unit test
#endif
        // ----------------------------------------------------------------------------------
        // Compute input jac_block against the one computed by central differencing
        if (verbose)
          if ( Ajac.size() != fdjac[0].size() || Ajac.size() != fdjac[1].size())
          {
            std::cout << "Ajac = ";
            for (std::size_t j = 0; j < Ajac.size(); ++j)
              std::cout << Ajac[j] << ", ";
            std::cout << std::endl;

            std::cout << "fdjac[0] = ";
            for (std::size_t j = 0; j < fdjac[0].size(); ++j)
              std::cout << fdjac[0][j] << ", ";
            std::cout << std::endl;
          }

#if 0 // terminate tests as soon as check failure occurs
        BOOST_REQUIRE_EQUAL(Ajac.size(), fdjac[0].size());
        BOOST_REQUIRE_EQUAL(Ajac.size(), fdjac[1].size());
#else // just to keep the test going in the presence of check failures
        BOOST_CHECK_EQUAL(Ajac.size(), fdjac[0].size());
        BOOST_CHECK_EQUAL(Ajac.size(), fdjac[1].size());
#endif
        if ( Ajac.size()==fdjac[0].size() && Ajac.size()==fdjac[1].size() )
        {
          for (std::size_t j = 0; j < fdjac[0].size(); ++j)
          {
            // print out nonzero entries
            if (verbose)
            {
              std::cout << "----*---- nonzero j=" << j << ": " <<
                  "col(true,fd1,fd2) = (" << iAjac[j] << ", " << ifdjac[0][j] << ", " << ifdjac[1][j] << "), " <<
                  "val(true,fd1,fd2) = ( " << " ----*----" << std::endl;
              std::cout << Ajac[j] << " \\\\ jacobian being checked" << std::endl;
              std::cout << fdjac[0][j] << " \\\\ fd1" << std::endl;
              std::cout << fdjac[1][j] << " \\\\ fd2 )" << std::endl;
            }

            //Check if the col indices match up
            BOOST_CHECK_EQUAL(iAjac[j], ifdjac[0][j]);
            BOOST_CHECK_EQUAL(iAjac[j], ifdjac[1][j]);

            for (int m = 0; m < M; m++)
            {
              for (int n = 0; n < N; n++)
              {
                Real err_vec[2] = { fabs( DLA::index(Ajac[j],m,n) - DLA::index(fdjac[0][j],m,n) ),
                                    fabs( DLA::index(Ajac[j],m,n) - DLA::index(fdjac[1][j],m,n) )};

                if (verbose)
                {
                  std::cout << "----- i=" << i << ", j=" << j << ", col=" << ifdjac[0][j] << ": m=" << m << ", n=" << n << " - " <<
                      "errors = {" << err_vec[0] << "," << err_vec[1] << "}";
                }

                // Error in finite-difference jacobian is either zero as for linear or quadratic residuals,
                // or is nonzero for general nonlinear residual where we need to check error convergence rate
                if (err_vec[0] > small_tol && err_vec[1] > small_tol)
                {
                  Real rate = log(err_vec[1]/err_vec[0])/log(step_vec[1]/step_vec[0]);

                  if (verbose) std::cout << ", rate = " << rate << std::endl;

                  BOOST_CHECK_MESSAGE( (rate >= rate_range[0] && rate <= rate_range[1]) || (err_vec[1] < 1e-9),
                                       "Rate check failed at col = " << ifdjac[0][j] << ": m = " << m <<
                                       " n = " << n << ": rate = " << rate <<
                                       ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" );
                }
                else
                {
                  if (verbose) std::cout << std::endl;
                }
              }
            }
          }
        }
        else
          std::cout << "Error: sizes of Ajac, fdjac[0], and fdjac[1] are inconsistent!" << std::endl;


#ifdef ISTIMINGJacobianPingTest
        std::cout << "Wall-clock time of comparing jacobians : "
                  << time_jacobianCompare.elapsed() << " second(s)" << std::endl;
#endif

      } // i loop
    } // jj loop
  } // ii loop
}

} // namespace detail_jacobianPingTest

} // namespace SANS

#endif /* TEST_SYSTEM_BLOCKSOLVE_JACOBIANPINGTEST_BTEST_H_ */
