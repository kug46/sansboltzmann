// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Adjoint_LiftingOperator_DGBR2_Triangle_AD_btest
// testing computing the lifting operator adjoint of 2-D DG-BR2 with Advection-Diffusion on triangles
// Constructs the complete PDE and lifting operator system and compares with statically condensed solves

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include <fstream>
#include <string>

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/SurrealS.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "Discretization/DG/IntegrandCell_DGBR2.h"
#include "Discretization/DG/IntegrandInteriorTrace_DGBR2.h"

#include "Discretization/DG/ResidualCell_DGBR2.h"
#include "Discretization/DG/ResidualInteriorTrace_DGBR2.h"
#include "Discretization/DG/ResidualBoundaryTrace_DGBR2.h"

#include "Discretization/DG/JacobianCell_DGBR2_LiftingOperator.h"
#include "Discretization/DG/JacobianBoundaryTrace_DGBR2.h"

#include "Discretization/DG/SetFieldInteriorTrace_DGBR2_AdjointLO.h"
#include "Discretization/DG/SetFieldBoundaryTrace_DGBR2_AdjointLO.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "Discretization/DG/IntegrandBoundaryTrace_LinearScalar_sansLG_DGBR2.h"

#include "Field/Element/ElementProjection_L2.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Field/tools/for_each_CellGroup.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

namespace // no-name namespace keep things private to this file
{


template<class IntegrandCell, template<class> class Vector>
class ResidualCell_DGBR2_LiftingOperator_impl :
    public GroupIntegralCellType< ResidualCell_DGBR2_LiftingOperator_impl<IntegrandCell, Vector> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCell::template VectorArrayQ<Real> VectorArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  ResidualCell_DGBR2_LiftingOperator_impl( const IntegrandCell& fcn,
                                           Vector<ArrayQ>& rsdLOGlobal ) :
    fcn_(fcn), rsdLOGlobal_(rsdLOGlobal) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {
    const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld = get<1>(flds);

    BOOST_REQUIRE( rsdLOGlobal_.m()  == rfld.nDOF()*PhysDim::D );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobal,
      const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                     template FieldCellGroupType<Topology>& fldsCell,
      const int quadratureorder )
  {
    typedef typename XFieldType                                ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ          >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> RFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef ElementLift<VectorArrayQ, TopoDim, Topology> ElementRFieldClass;

    const int NTrace = Topology::NTrace;

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const RFieldCellGroupType& rfldCell = get<1>(fldsCell);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementRFieldClass rfldElems( rfldCell.basis() );

    // number of integrals evaluated
    const int nDOF = qfldElem.nDOF();
    const int nIntegrand = nDOF;

    // element integral
    typedef DLA::VectorS<NTrace, VectorArrayQ> IntegrandType;
    GalerkinWeightedIntegral<TopoDim, Topology, IntegrandType> integral(quadratureorder, nIntegrand);

    // just to make sure things are consistent
    BOOST_REQUIRE( xfldCell.nElem() == qfldCell.nElem() );
    BOOST_REQUIRE( xfldCell.nElem() == rfldCell.nElem() );

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal( nDOF, -1 );

    // residual array
    std::vector< IntegrandType > rsdLOElem( nIntegrand );

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      rfldCell.getElement( rfldElems, elem );

      for (int n = 0; n < nIntegrand; n++)
        for (int trace = 0; trace < NTrace; trace++)
          rsdLOElem[n][trace] = 0;

      // cell integration for canonical element
      integral( fcn_.integrand_LO(xfldElem, rfldElems), get<-1>(xfldElem), rsdLOElem.data(), nIntegrand );

      // scatter-add element integral to global
      int nGlobal;
      for (int trace = 0; trace < NTrace; trace++)
      {
        rfldCell.associativity( elem, trace ).getGlobalMapping( mapDOFGlobal.data(), nDOF );
        for (int n = 0; n < nDOF; n++)
        {
          for (int d = 0; d < PhysDim::D; d++)
          {
            nGlobal = PhysDim::D*mapDOFGlobal[n] + d;
            rsdLOGlobal_[nGlobal] += rsdLOElem[n][trace][d];
          }
        }
      }

    }
  }

protected:
  const IntegrandCell& fcn_;
  Vector<ArrayQ>& rsdLOGlobal_;
};


// Factory function

template<class IntegrandCell, template<class> class Vector, class ArrayQ>
ResidualCell_DGBR2_LiftingOperator_impl<IntegrandCell, Vector>
ResidualCell_DGBR2_LiftingOperator( const IntegrandCellType<IntegrandCell>& fcnPDE,
                                    Vector<ArrayQ>& rsdLOGlobal )
{
  static_assert( std::is_same< ArrayQ, typename IntegrandCell::template ArrayQ<Real> >::value, "These should be the same.");
  return ResidualCell_DGBR2_LiftingOperator_impl<IntegrandCell, Vector>(fcnPDE.cast(), rsdLOGlobal);
}

//===========================================================================//

template<class IntegrandInteriorTrace, template<class> class Vector>
class ResidualInteriorTrace_DGBR2_LiftingOperator_impl :
    public GroupIntegralInteriorTraceType< ResidualInteriorTrace_DGBR2_LiftingOperator_impl<IntegrandInteriorTrace, Vector> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandInteriorTrace::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename IntegrandInteriorTrace::PDE PDE;

  // Save off the boundary trace integrand and the residual vectors
  ResidualInteriorTrace_DGBR2_LiftingOperator_impl( const IntegrandInteriorTrace& fcn,
                                                    Vector<ArrayQ>& rsdLOGlobal ) :
     fcn_(fcn), rsdLOGlobal_(rsdLOGlobal) {}


  std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
  std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {
    const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld = get<1>(flds);

    BOOST_REQUIRE( rsdLOGlobal_.m()  == rfld.nDOF()*PhysDim::D );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                                               template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                                               template FieldCellGroupType<TopologyR>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

    //Right types
    typedef typename XFieldType::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
    typedef typename FieldLift<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyR> RFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const RFieldCellGroupTypeL& rfldCellL = get<1>(fldsCellL);

    const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);
    const RFieldCellGroupTypeR& rfldCellR = get<1>(fldsCellR);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // number of integrals evaluated per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();
    const int nIntegrandL = nDOFL;
    const int nIntegrandR = nDOFR;

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL( nDOFL );
    std::vector<int> mapDOFGlobalR( nDOFR );

    // PDE trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, VectorArrayQ, VectorArrayQ>
      integral(quadratureorder, nIntegrandL, nIntegrandR);

    // PDE cell residuals
    std::vector< VectorArrayQ > rsdLOElemL( nIntegrandL );
    std::vector< VectorArrayQ > rsdLOElemR( nIntegrandR );

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldCellR.getElement( xfldElemR, elemR );
      qfldCellR.getElement( qfldElemR, elemR );

      xfldTrace.getElement( xfldElemTrace, elem );

      for (int n = 0; n < nIntegrandL; n++) rsdLOElemL[n] = 0;
      for (int n = 0; n < nIntegrandR; n++) rsdLOElemR[n] = 0;

      // Compute the PDE residual given the lifting operators
      integral( fcn_.integrand_LO(xfldElemTrace,
                                  canonicalTraceL, canonicalTraceR,
                                  xfldElemL, qfldElemL,
                                  xfldElemR, qfldElemR),
                xfldElemTrace, rsdLOElemL.data(), nIntegrandL, rsdLOElemR.data(), nIntegrandR );

      // scatter-add element residuals to global
      rfldCellL.associativity( elemL, canonicalTraceL.trace ).getGlobalMapping( mapDOFGlobalL.data(), nDOFL );
      rfldCellR.associativity( elemR, canonicalTraceR.trace ).getGlobalMapping( mapDOFGlobalR.data(), nDOFR );

      int nGlobal;
      for (int n = 0; n < nDOFL; n++)
        for (int d = 0; d < PhysDim::D; d++)
        {
          nGlobal = PhysDim::D*mapDOFGlobalL[n] + d;
          rsdLOGlobal_[nGlobal] += rsdLOElemL[n][d];
        }

      for (int n = 0; n < nDOFR; n++)
        for (int d = 0; d < PhysDim::D; d++)
        {
          nGlobal = PhysDim::D*mapDOFGlobalR[n] + d;
          rsdLOGlobal_[nGlobal] += rsdLOElemR[n][d];
        }

    }
  }

protected:
  const IntegrandInteriorTrace& fcn_;
  Vector<ArrayQ>& rsdLOGlobal_;
};


// Factory function

template<class IntegrandInteriorTrace, template<class> class Vector, class ArrayQ>
ResidualInteriorTrace_DGBR2_LiftingOperator_impl<IntegrandInteriorTrace, Vector>
ResidualInteriorTrace_DGBR2_LiftingOperator( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                                             Vector<ArrayQ>& rsdLOGlobal )
{
  static_assert( std::is_same< ArrayQ, typename IntegrandInteriorTrace::template ArrayQ<Real> >::value, "These should be the same.");
  return ResidualInteriorTrace_DGBR2_LiftingOperator_impl<IntegrandInteriorTrace, Vector>(fcn.cast(), rsdLOGlobal);
}

//===========================================================================//

template<class IntegrandBoundaryTrace, template<class> class Vector>
class ResidualBoundaryTrace_DGBR2_LiftingOperator_impl :
    public GroupIntegralBoundaryTraceType< ResidualBoundaryTrace_DGBR2_LiftingOperator_impl<IntegrandBoundaryTrace, Vector> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTrace::template VectorArrayQ<Real> VectorArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  ResidualBoundaryTrace_DGBR2_LiftingOperator_impl( const IntegrandBoundaryTrace& fcn,
                                                    Vector<ArrayQ>& rsdLOGlobal ) :
    fcn_(fcn), rsdLOGlobal_(rsdLOGlobal) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {
    const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld = get<1>(flds);

    BOOST_REQUIRE( rsdLOGlobal_.m() == rfld.nDOF()*PhysDim::D );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                                        template FieldCellGroupType<TopologyL>& fldsCellL,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             int quadratureorder )
  {
    typedef typename XFieldType                               ::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>          ::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<> ElementRFieldClassL;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const RFieldCellGroupTypeL& rfldCellL = get<1>(fldsCellL);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementRFieldClassL rfldElemL( rfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // number of integrals evaluated per element
    int nIntegrandL = qfldElemL.nDOF();
    const int nDOFL = nIntegrandL;

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL( nIntegrandL );

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, VectorArrayQ> integral(quadratureorder, nIntegrandL);

    // element integrand/residuals
    std::vector<VectorArrayQ> rsdLOElemL( nIntegrandL );

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      rfldCellL.getElement( rfldElemL, elemL, canonicalTraceL.trace );

      xfldTrace.getElement( xfldElemTrace, elem );

      for (int n = 0; n < nIntegrandL; n++)
        rsdLOElemL[n] = 0;

      integral( fcn_.integrand_LO(xfldElemTrace, canonicalTraceL,
                                  xfldElemL, qfldElemL, rfldElemL),
                get<-1>(xfldElemTrace),
                rsdLOElemL.data(), nIntegrandL );

      // scatter-add element residuals to global
      rfldCellL.associativity( elemL, canonicalTraceL.trace ).getGlobalMapping( mapDOFGlobalL.data(), nDOFL );

      int nGlobal;
      for (int n = 0; n < nDOFL; n++)
        for (int d = 0; d < PhysDim::D; d++)
        {
          nGlobal = PhysDim::D*mapDOFGlobalL[n] + d;
          rsdLOGlobal_[nGlobal] += rsdLOElemL[n][d];
        }

    }
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  Vector<ArrayQ>& rsdLOGlobal_;
};

// Factory function

template<class IntegrandBoundaryTrace, template<class> class Vector, class ArrayQ>
ResidualBoundaryTrace_DGBR2_LiftingOperator_impl<IntegrandBoundaryTrace, Vector>
ResidualBoundaryTrace_DGBR2_LiftingOperator( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                             Vector<ArrayQ>& rsdLOGlobal)
{
  static_assert( std::is_same<ArrayQ, typename IntegrandBoundaryTrace::template ArrayQ<Real> >::value, "These should be the same.");
  return ResidualBoundaryTrace_DGBR2_LiftingOperator_impl<IntegrandBoundaryTrace, Vector>(fcn.cast(), rsdLOGlobal);
}

}


//############################################################################//
BOOST_AUTO_TEST_SUITE( Adjoint_LiftingOperator_DGBR2_Triangle_AD_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line )
{
  const int D = PhysD1::D;

  typedef SurrealS<1> SurrealClass;

  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,D,MatrixQ> RowMatrixQ;

  typedef ScalarFunction1D_Sine SolutionExact;

  typedef BCTypeFunctionLinearRobin_sansLG BCType;
  typedef BCNDConvertSpace<PhysD1, BCAdvectionDiffusion<PhysD1,BCType>> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandTraceClass;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandBCClass;

  // PDE

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv( u );

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 0.38, b = -0.67;
  Source1D_UniformGrad source(a, b);

  PDEClass pde( adv, visc, source );

  // BC
  std::shared_ptr<SolutionExact> solnExact( new SolutionExact );

  BCClass bc( solnExact, visc );

  // grid: HierarchicalP1 (aka X1)

  int ii = 3;

  XField1D xfld( ii, 0, ii );

  // solution: Legendre P1

  int order = 1;

  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0;

  const int nDOFPDE = qfld.nDOF();

#if 0
  cout << "btest: dumping qfld" << endl;  qfld.dump(2);
#endif

  // Lifting operators:

  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Legendre);
  rfld = 0;

  const int nDOFLO = D*rfld.nDOF();

  // Lagrange multiplier: Legendre P1

#if 1
  int orderBC = 0;
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld( xfld, orderBC, BasisFunctionCategory_Legendre, {} );
#elif 0
  order = 1;
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre, {} );
#else
  int orderBC = 1;
  std::vector<std::vector<int>> boundaryGroupSets = {{0},{1},{2},{3}};
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, orderBC, BasisFunctionCategory_Hierarchical, {} );
#endif

  const int nDOFBC = lgfld.nDOF();
  lgfld = 0;

#if 0
  cout << "btest: dumping lgfld" << endl;  lgfld.dump(2);
#endif

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrands
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandTraceClass fcnTrace( pde, disc, {0} );
  IntegrandBCClass fcnBC( pde, bc, {0,1}, disc );

  // quadrature rule
  int quadratureOrder[2] = {-1, -1};    // max

  // linear system setup

  // complete jacobian nonzero pattern without static condensation of lifting operator
  //
  //           r   q  lg
  //   LO      X   X   0
  //   PDE     X   X   X
  //   BC      0   X   0

  DLA::MatrixD<DLA::MatrixD<MatrixQ>> jacT =
      {{ { nDOFLO, nDOFLO}, { nDOFLO, nDOFPDE}, { nDOFLO, nDOFBC} },
       { {nDOFPDE, nDOFLO}, {nDOFPDE, nDOFPDE}, {nDOFPDE, nDOFBC} },
       { { nDOFBC, nDOFLO}, { nDOFBC, nDOFPDE}, { nDOFBC, nDOFBC} }};

  DLA::VectorD<DLA::VectorD<ArrayQ>> rhs = DLA::VectorD< DLA::DenseVectorSize >({ nDOFLO, nDOFPDE, nDOFBC });
  DLA::VectorD<DLA::VectorD<ArrayQ>> sln = DLA::VectorD< DLA::DenseVectorSize >({ nDOFLO, nDOFPDE, nDOFBC });

  // Create a dummy right hand side for the adjoint
  for (int i = 0; i < nDOFLO; i++)
    rhs[0][i] = 0; // must be zero

  for (int i = 0; i < nDOFPDE; i++)
    rhs[1][i] = cos(2*PI*i/nDOFPDE);

  for (int i = 0; i < nDOFBC; i++)
    rhs[2][i] = tan(2*PI*i/nDOFBC);

  jacT = 0;

  // jacobian transpose computed with finite difference

  auto jacTLO_r   = Transpose(jacT)(0,0);
  auto jacTLO_q   = Transpose(jacT)(0,1);
//auto jacTLO_lg  = Transpose(jacT)(0,2);

  auto jacTPDE_r  = Transpose(jacT)(1,0);
  auto jacTPDE_q  = Transpose(jacT)(1,1);
//auto jacTPDE_lg = Transpose(jacT)(1,2);

//auto jacTBC_r   = Transpose(jacT)(2,0);
//auto jacTBC_q   = Transpose(jacT)(2,1);
//auto jacTBC_lg  = Transpose(jacT)(2,2);

  DLA::VectorD<ArrayQ> rsdPDEGlobal0(nDOFPDE), rsdPDEGlobal1(nDOFPDE);
  DLA::VectorD<ArrayQ> rsdLOGlobal0(nDOFLO), rsdLOGlobal1(nDOFLO);

  rsdLOGlobal0 = 0;
  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2_LiftingOperator(fcnCell, rsdLOGlobal0),
                                          xfld, (qfld, rfld), quadratureOrder, xfld.nCellGroups() );

  IntegrateInteriorTraceGroups<TopoD1>::integrate(ResidualInteriorTrace_DGBR2_LiftingOperator(fcnTrace, rsdLOGlobal0),
                                                  xfld, (qfld, rfld), quadratureOrder, xfld.nInteriorTraceGroups());

  IntegrateBoundaryTraceGroups<TopoD1>::integrate( ResidualBoundaryTrace_DGBR2_LiftingOperator(fcnBC, rsdLOGlobal0),
                                                   xfld, (qfld, rfld), quadratureOrder, xfld.nBoundaryTraceGroups() );


  rsdPDEGlobal0 = 0;
  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnCell, rsdPDEGlobal0),
                                          xfld, (qfld, rfld), quadratureOrder, xfld.nCellGroups() );

  IntegrateInteriorTraceGroups<TopoD1>::integrate(ResidualInteriorTrace_DGBR2(fcnTrace, rsdPDEGlobal0),
                                                  xfld, (qfld, rfld), quadratureOrder, xfld.nInteriorTraceGroups());

  IntegrateBoundaryTraceGroups<TopoD1>::integrate( ResidualBoundaryTrace_DGBR2(fcnBC, rsdPDEGlobal0),
                                                   xfld, (qfld, rfld), quadratureOrder, xfld.nBoundaryTraceGroups() );


  //wrt q
  for (int j = 0; j < nDOFPDE; j++)
  {
    qfld.DOF(j) += 1;

    rsdLOGlobal1 = 0;
    IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2_LiftingOperator(fcnCell, rsdLOGlobal1),
                                            xfld, (qfld, rfld), quadratureOrder, xfld.nCellGroups() );

    IntegrateInteriorTraceGroups<TopoD1>::integrate(ResidualInteriorTrace_DGBR2_LiftingOperator(fcnTrace, rsdLOGlobal1),
                                                    xfld, (qfld, rfld), quadratureOrder, xfld.nInteriorTraceGroups());

    IntegrateBoundaryTraceGroups<TopoD1>::integrate( ResidualBoundaryTrace_DGBR2_LiftingOperator(fcnBC, rsdLOGlobal1),
                                                     xfld, (qfld, rfld), quadratureOrder, xfld.nBoundaryTraceGroups() );


    rsdPDEGlobal1 = 0;
    IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnCell, rsdPDEGlobal1),
                                            xfld, (qfld, rfld), quadratureOrder, xfld.nCellGroups() );

    IntegrateInteriorTraceGroups<TopoD1>::integrate(ResidualInteriorTrace_DGBR2(fcnTrace, rsdPDEGlobal1),
                                                    xfld, (qfld, rfld), quadratureOrder, xfld.nInteriorTraceGroups());

    IntegrateBoundaryTraceGroups<TopoD1>::integrate( ResidualBoundaryTrace_DGBR2(fcnBC, rsdPDEGlobal1),
                                                     xfld, (qfld, rfld), quadratureOrder, xfld.nBoundaryTraceGroups() );

    qfld.DOF(j) -= 1;

    for (int i = 0; i < nDOFLO; i++)
      jacTLO_q(i,j) = rsdLOGlobal1[i] - rsdLOGlobal0[i];

    for (int i = 0; i < nDOFPDE; i++)
      jacTPDE_q(i,j) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
  }

  //wrt r
  for (int j = 0; j < nDOFLO/D; j++)
  {
    for (int d = 0; d < D; d++)
    {
      rfld.DOF(j)[d] += 1;

      rsdLOGlobal1 = 0;
      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2_LiftingOperator(fcnCell, rsdLOGlobal1),
                                              xfld, (qfld, rfld), quadratureOrder, xfld.nCellGroups() );

      IntegrateInteriorTraceGroups<TopoD1>::integrate(ResidualInteriorTrace_DGBR2_LiftingOperator(fcnTrace, rsdLOGlobal1),
                                                      xfld, (qfld, rfld), quadratureOrder, xfld.nInteriorTraceGroups());

      IntegrateBoundaryTraceGroups<TopoD1>::integrate( ResidualBoundaryTrace_DGBR2_LiftingOperator(fcnBC, rsdLOGlobal1),
                                                       xfld, (qfld, rfld), quadratureOrder, xfld.nBoundaryTraceGroups() );


      rsdPDEGlobal1 = 0;
      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnCell, rsdPDEGlobal1),
                                              xfld, (qfld, rfld), quadratureOrder, xfld.nCellGroups() );

      IntegrateInteriorTraceGroups<TopoD1>::integrate(ResidualInteriorTrace_DGBR2(fcnTrace, rsdPDEGlobal1),
                                                      xfld, (qfld, rfld), quadratureOrder, xfld.nInteriorTraceGroups());

      IntegrateBoundaryTraceGroups<TopoD1>::integrate( ResidualBoundaryTrace_DGBR2(fcnBC, rsdPDEGlobal1),
                                                       xfld, (qfld, rfld), quadratureOrder, xfld.nBoundaryTraceGroups() );

      rfld.DOF(j)[d] -= 1;

      for (int i = 0; i < nDOFLO; i++)
        jacTLO_r(i,D*j+d) = rsdLOGlobal1[i] - rsdLOGlobal0[i];

      for (int i = 0; i < nDOFPDE; i++)
        jacTPDE_r(i,D*j+d) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    }
  }

#if 0
  SLA::WriteMatrixMarketFile(jacT(0,0), "tmp/jacT00.mtx");
  SLA::WriteMatrixMarketFile(jacT(0,1), "tmp/jacT01.mtx");
  SLA::WriteMatrixMarketFile(jacT(1,0), "tmp/jacT10.mtx");
  SLA::WriteMatrixMarketFile(jacT(1,1), "tmp/jacT11.mtx");

  std::cout << "rhs = {";
  for (int i = 0; i < nDOFLO; i++)
  {
    std::cout << rhs[0][i] << ", ";
  }
  for (int i = 0; i < nDOFPDE; i++)
  {
    std::cout << rhs[1][i];
    if ( i < nDOFPDE-1 )std::cout << ", ";
  }
  std::cout << "}" << std::endl;
#endif

  // Compute the adjoint
  sln = DLA::InverseLU::Solve(jacT, rhs);

  // Adjoint field via finite difference
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> wfldFD(xfld, order, BasisFunctionCategory_Legendre);
  wfldFD = 0;

  // Lifting operators adjoint via finite difference
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> sfldFD(xfld, order, BasisFunctionCategory_Legendre);
  sfldFD = 0;

  // Lagrange multiplier adjoint via finite difference

#if 1
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> mufldFD( xfld, orderBC, BasisFunctionCategory_Legendre, {} );
#elif 0
  order = 1;
  QField2D_DG_BoundaryEdge<PDEClass> mufldFD( xfld, order, BasisFunctionCategory_Legendre );
#else
  order = 1;
  std::vector<std::vector<int>> boundaryGroupSets = {{0},{1},{2},{3}};
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> mufldFD( xfld, order, BasisFunctionCategory_Hierarchical, {} );
#endif
  mufldFD = 0;

  // Set the lifting operator field
  for (int i = 0; i < nDOFLO/D; i++)
    for (int d = 0; d < D; d++)
      sfldFD.DOF(i)[d] = sln[0][D*i+d];

  for (int i = 0; i < nDOFPDE; i++)
  {
    wfldFD.DOF(i) = sln[1][i];
    //std::cout << " [" << i << "] " << wfldFD.DOF(i) << std::endl;
  }

  for (int i = 0; i < nDOFBC; i++)
    mufldFD.DOF(i) = sln[2][i];



/*
  std::fstream jacfile("tmp/jacobian.mtx", std::ios::out);
  WriteMatrixMarketFile( jac, jacfile );

  std::fstream jacTfile("tmp/jacobianT.mtx", std::ios::out);
  WriteMatrixMarketFile( jacT, jacTfile );
*/

  // Compute the lifting operator adjoint via static condensation

  const FieldDataInvMassMatrix_Cell mmfld(qfld); // Inverse mass matrix field

  FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld); // Lifting operator jacobian from cell integral

  // PDE jacobian wrt R
  IntegrateCellGroups<TopoD1>::integrate(
      JacobianCell_DGBR2_LiftingOperator(fcnCell, jacPDE_R),
      xfld, (qfld, rfld), quadratureOrder, xfld.nCellGroups() );

  // Lifting operators adjoint via static condensation
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> sfld(xfld, order, BasisFunctionCategory_Legendre);
  sfld = 0;

  IntegrateInteriorTraceGroups<TopoD1>::integrate(SetFieldInteriorTrace_DGBR2_AdjointLO(fcnTrace, mmfld, jacPDE_R),
                                                  xfld, (qfld, rfld, wfldFD, sfld), quadratureOrder, xfld.nInteriorTraceGroups());

  IntegrateBoundaryTraceGroups<TopoD1>::integrate(SetFieldBoundaryTrace_DGBR2_AdjointLO<SurrealClass>(fcnBC, jacPDE_R),
                                                  xfld, (qfld, rfld, wfldFD, sfld), quadratureOrder, xfld.nBoundaryTraceGroups());

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-10;

  for ( int i = 0; i < sfld.nDOF(); i++ )
  {
    for (int d = 0; d < D; d++)
    {
      //std::cout << "[" << i << "][" << d << "] " << sfldFD.DOF(i)[d] << " == " << sfld.DOF(i)[d] << std::endl;
      //Check that the numbers are the same
      SANS_CHECK_CLOSE( sfldFD.DOF(i)[d], sfld.DOF(i)[d], small_tol, close_tol );
    }
  }
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle )
{
  const int D = PhysD2::D;

  typedef SurrealS<1> SurrealClass;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,D,MatrixQ> RowMatrixQ;

  typedef ScalarFunction2D_SineSine SolutionExact;

  typedef BCTypeFunctionLinearRobin_sansLG BCType;
  typedef BCNDConvertSpace<PhysD2, BCAdvectionDiffusion<PhysD2,BCType>> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandTraceClass;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandBCClass;

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.348;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real a = 0.38, b = -0.67, c = -0.91;
  Source2D_UniformGrad source(a, b, c);

  PDEClass pde( adv, visc, source );

  // BC
  std::shared_ptr<SolutionExact> solnExact( new SolutionExact );

  BCClass bc( solnExact, visc );

  // grid: HierarchicalP1 (aka X1)

  int ii = 2;
  int jj = 2;

  XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj );

  // solution: Legendre P1

  int order = 1;

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0;

  const int nDOFPDE = qfld.nDOF();

#if 0
  cout << "btest: dumping qfld" << endl;  qfld.dump(2);
#endif

  // Lifting operators:

  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Legendre);
  rfld = 0;

  const int nDOFLO = D*rfld.nDOF();

  // Lagrange multiplier: Legendre P1

#if 1
  int orderBC = 0;
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, orderBC, BasisFunctionCategory_Legendre, {} );
#elif 0
  order = 1;
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre, {} );
#else
  int orderBC = 1;
  std::vector<std::vector<int>> boundaryGroupSets = {{0},{1},{2},{3}};
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, orderBC, BasisFunctionCategory_Hierarchical, {} );
#endif

  const int nDOFBC = lgfld.nDOF();
  lgfld = 0;

#if 0
  cout << "btest: dumping lgfld" << endl;  lgfld.dump(2);
#endif

  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrands
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandTraceClass fcnTrace( pde, disc, {0,1,2} );
  IntegrandBCClass fcnBC( pde, bc, {0,1,2,3}, disc );

  // quadrature rule
  int quadratureOrder[4] = {-1, -1, -1, -1};    // max

  // linear system setup

  // complete jacobian nonzero pattern without static condensation of lifting operator
  //
  //           r   q  lg
  //   LO      X   X   0
  //   PDE     X   X   X
  //   BC      0   X   0

  DLA::MatrixD<DLA::MatrixD<MatrixQ>> jacT =
      {{ { nDOFLO, nDOFLO}, { nDOFLO, nDOFPDE}, { nDOFLO, nDOFBC} },
       { {nDOFPDE, nDOFLO}, {nDOFPDE, nDOFPDE}, {nDOFPDE, nDOFBC} },
       { { nDOFBC, nDOFLO}, { nDOFBC, nDOFPDE}, { nDOFBC, nDOFBC} }};

  DLA::VectorD<DLA::VectorD<ArrayQ>> rhs = DLA::VectorD< DLA::DenseVectorSize >({ nDOFLO, nDOFPDE, nDOFBC });
  DLA::VectorD<DLA::VectorD<ArrayQ>> sln = DLA::VectorD< DLA::DenseVectorSize >({ nDOFLO, nDOFPDE, nDOFBC });

  // Create a dummy right hand side for the adjoint
  for (int i = 0; i < nDOFLO; i++)
    rhs[0][i] = 0; // must be zero

  for (int i = 0; i < nDOFPDE; i++)
    rhs[1][i] = cos(2*PI*i/nDOFPDE);

  for (int i = 0; i < nDOFBC; i++)
    rhs[2][i] = tan(2*PI*i/nDOFBC);


  jacT = 0;

  // jacobian transpose computed with finite difference

  auto jacTLO_r   = Transpose(jacT)(0,0);
  auto jacTLO_q   = Transpose(jacT)(0,1);
//auto jacTLO_lg  = Transpose(jacT)(0,2);

  auto jacTPDE_r  = Transpose(jacT)(1,0);
  auto jacTPDE_q  = Transpose(jacT)(1,1);
//auto jacTPDE_lg = Transpose(jacT)(1,2);

//auto jacTBC_r   = Transpose(jacT)(2,0);
//auto jacTBC_q   = Transpose(jacT)(2,1);
//auto jacTBC_lg  = Transpose(jacT)(2,2);

  DLA::VectorD<ArrayQ> rsdPDEGlobal0(nDOFPDE), rsdPDEGlobal1(nDOFPDE);
  DLA::VectorD<ArrayQ> rsdLOGlobal0(nDOFLO), rsdLOGlobal1(nDOFLO);

  rsdLOGlobal0 = 0;
  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_DGBR2_LiftingOperator(fcnCell, rsdLOGlobal0),
                                          xfld, (qfld, rfld), quadratureOrder, xfld.nCellGroups() );

  IntegrateInteriorTraceGroups<TopoD2>::integrate(ResidualInteriorTrace_DGBR2_LiftingOperator(fcnTrace, rsdLOGlobal0),
                                                  xfld, (qfld, rfld), quadratureOrder, xfld.nInteriorTraceGroups());

  IntegrateBoundaryTraceGroups<TopoD2>::integrate( ResidualBoundaryTrace_DGBR2_LiftingOperator(fcnBC, rsdLOGlobal0),
                                                   xfld, (qfld, rfld), quadratureOrder, xfld.nBoundaryTraceGroups() );


  rsdPDEGlobal0 = 0;
  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_DGBR2(fcnCell, rsdPDEGlobal0),
                                          xfld, (qfld, rfld), quadratureOrder, xfld.nCellGroups() );

  IntegrateInteriorTraceGroups<TopoD2>::integrate(ResidualInteriorTrace_DGBR2(fcnTrace, rsdPDEGlobal0),
                                                  xfld, (qfld, rfld), quadratureOrder, xfld.nInteriorTraceGroups());

  IntegrateBoundaryTraceGroups<TopoD2>::integrate( ResidualBoundaryTrace_DGBR2(fcnBC, rsdPDEGlobal0),
                                                   xfld, (qfld, rfld), quadratureOrder, xfld.nBoundaryTraceGroups() );


  //wrt q
  for (int j = 0; j < nDOFPDE; j++)
  {
    qfld.DOF(j) += 1;

    rsdLOGlobal1 = 0;
    IntegrateCellGroups<TopoD2>::integrate( ResidualCell_DGBR2_LiftingOperator(fcnCell, rsdLOGlobal1),
                                            xfld, (qfld, rfld), quadratureOrder, xfld.nCellGroups() );

    IntegrateInteriorTraceGroups<TopoD2>::integrate(ResidualInteriorTrace_DGBR2_LiftingOperator(fcnTrace, rsdLOGlobal1),
                                                    xfld, (qfld, rfld), quadratureOrder, xfld.nInteriorTraceGroups());

    IntegrateBoundaryTraceGroups<TopoD2>::integrate( ResidualBoundaryTrace_DGBR2_LiftingOperator(fcnBC, rsdLOGlobal1),
                                                     xfld, (qfld, rfld), quadratureOrder, xfld.nBoundaryTraceGroups() );


    rsdPDEGlobal1 = 0;
    IntegrateCellGroups<TopoD2>::integrate( ResidualCell_DGBR2(fcnCell, rsdPDEGlobal1),
                                            xfld, (qfld, rfld), quadratureOrder, xfld.nCellGroups() );

    IntegrateInteriorTraceGroups<TopoD2>::integrate(ResidualInteriorTrace_DGBR2(fcnTrace, rsdPDEGlobal1),
                                                    xfld, (qfld, rfld), quadratureOrder, xfld.nInteriorTraceGroups());

    IntegrateBoundaryTraceGroups<TopoD2>::integrate( ResidualBoundaryTrace_DGBR2(fcnBC, rsdPDEGlobal1),
                                                     xfld, (qfld, rfld), quadratureOrder, xfld.nBoundaryTraceGroups() );

    qfld.DOF(j) -= 1;

    for (int i = 0; i < nDOFLO; i++)
      jacTLO_q(i,j) = rsdLOGlobal1[i] - rsdLOGlobal0[i];

    for (int i = 0; i < nDOFPDE; i++)
      jacTPDE_q(i,j) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
  }

  //wrt r
  for (int j = 0; j < nDOFLO/D; j++)
  {
    for (int d = 0; d < D; d++)
    {
      rfld.DOF(j)[d] += 1;

      rsdLOGlobal1 = 0;
      IntegrateCellGroups<TopoD2>::integrate( ResidualCell_DGBR2_LiftingOperator(fcnCell, rsdLOGlobal1),
                                              xfld, (qfld, rfld), quadratureOrder, xfld.nCellGroups() );

      IntegrateInteriorTraceGroups<TopoD2>::integrate(ResidualInteriorTrace_DGBR2_LiftingOperator(fcnTrace, rsdLOGlobal1),
                                                      xfld, (qfld, rfld), quadratureOrder, xfld.nInteriorTraceGroups());

      IntegrateBoundaryTraceGroups<TopoD2>::integrate( ResidualBoundaryTrace_DGBR2_LiftingOperator(fcnBC, rsdLOGlobal1),
                                                       xfld, (qfld, rfld), quadratureOrder, xfld.nBoundaryTraceGroups() );


      rsdPDEGlobal1 = 0;
      IntegrateCellGroups<TopoD2>::integrate( ResidualCell_DGBR2(fcnCell, rsdPDEGlobal1),
                                              xfld, (qfld, rfld), quadratureOrder, xfld.nCellGroups() );

      IntegrateInteriorTraceGroups<TopoD2>::integrate(ResidualInteriorTrace_DGBR2(fcnTrace, rsdPDEGlobal1),
                                                      xfld, (qfld, rfld), quadratureOrder, xfld.nInteriorTraceGroups());

      IntegrateBoundaryTraceGroups<TopoD2>::integrate( ResidualBoundaryTrace_DGBR2(fcnBC, rsdPDEGlobal1),
                                                       xfld, (qfld, rfld), quadratureOrder, xfld.nBoundaryTraceGroups() );

      rfld.DOF(j)[d] -= 1;

      for (int i = 0; i < nDOFLO; i++)
        jacTLO_r(i,D*j+d) = rsdLOGlobal1[i] - rsdLOGlobal0[i];

      for (int i = 0; i < nDOFPDE; i++)
        jacTPDE_r(i,D*j+d) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    }
  }

#if 0
  SLA::WriteMatrixMarketFile(jacT(0,0), "tmp/jacT00.mtx");
  SLA::WriteMatrixMarketFile(jacT(0,1), "tmp/jacT01.mtx");
  SLA::WriteMatrixMarketFile(jacT(1,0), "tmp/jacT10.mtx");
  SLA::WriteMatrixMarketFile(jacT(1,1), "tmp/jacT11.mtx");
#endif

  // Compute the adjoint
  sln = DLA::InverseLU::Solve(jacT, rhs);

  // Adjoint field via finite difference
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfldFD(xfld, order, BasisFunctionCategory_Legendre);
  wfldFD = 0;

  // Lifting operators adjoint via finite difference
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> sfldFD(xfld, order, BasisFunctionCategory_Legendre);
  sfldFD = 0;

  // Lagrange multiplier adjoint via finite difference

#if 1
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> mufldFD( xfld, orderBC, BasisFunctionCategory_Legendre, {} );
#elif 0
  order = 1;
  QField2D_DG_BoundaryEdge<PDEClass> mufldFD( xfld, order, BasisFunctionCategory_Legendre );
#else
  order = 1;
  Field_CG_BoundaryTrace_Independent<PhysD2, TopoD2, ArrayQ> mufldFD( xfld, order, {} );
#endif
  mufldFD = 0;

  // Set the lifting operator field
  for (int i = 0; i < nDOFLO/D; i++)
    for (int d = 0; d < D; d++)
      sfldFD.DOF(i)[d] = sln[0][D*i+d];

  for (int i = 0; i < nDOFPDE; i++)
    wfldFD.DOF(i) = sln[1][i];

  for (int i = 0; i < nDOFBC; i++)
    mufldFD.DOF(i) = sln[2][i];

/*
  std::fstream jacfile("tmp/jacobian.mtx", std::ios::out);
  WriteMatrixMarketFile( jac, jacfile );

  std::fstream jacTfile("tmp/jacobianT.mtx", std::ios::out);
  WriteMatrixMarketFile( jacT, jacTfile );
*/

  // Compute the lifting operator adjoint via static condensation

  const FieldDataInvMassMatrix_Cell mmfld(qfld); // Inverse mass matrix field

  FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld); // Lifting operator jacobian from cell integral

  // PDE jacobian wrt R
  IntegrateCellGroups<TopoD2>::integrate(
      JacobianCell_DGBR2_LiftingOperator(fcnCell, jacPDE_R),
      xfld, (qfld, rfld), quadratureOrder, xfld.nCellGroups() );

  // Lifting operators adjoint via static condensation
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> sfld(xfld, order, BasisFunctionCategory_Legendre);
  sfld = 0;

  IntegrateInteriorTraceGroups<TopoD2>::integrate(SetFieldInteriorTrace_DGBR2_AdjointLO(fcnTrace, mmfld, jacPDE_R),
                                                  xfld, (qfld, rfld, wfldFD, sfld), quadratureOrder, xfld.nInteriorTraceGroups());

  IntegrateBoundaryTraceGroups<TopoD2>::integrate(SetFieldBoundaryTrace_DGBR2_AdjointLO<SurrealClass>(fcnBC, jacPDE_R),
                                                  xfld, (qfld, rfld, wfldFD, sfld), quadratureOrder, xfld.nBoundaryTraceGroups());

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-10;

  for ( int i = 0; i < sfld.nDOF(); i++ )
  {
    for (int d = 0; d < D; d++)
    {
      //std::cout << "[" << i << "][" << d << "] " << sfldFD.DOF(i)[d] << " == " << sfld.DOF(i)[d] << std::endl;
      //Check that the numbers are the same
      SANS_CHECK_CLOSE( sfldFD.DOF(i)[d], sfld.DOF(i)[d], small_tol, close_tol );
    }
  }
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
