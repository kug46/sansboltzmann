// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// SetFieldInteriorTrace_DGBR2_LiftingOperator_AD_btest
// testing of interior trace lifting operator calculations for DG BR2 with Advection-Diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"

#include "Discretization/DG/DiscretizationDGBR2.h"
#include "Discretization/DG/SetFieldInteriorTrace_DGBR2_LiftingOperator.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"

#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( SetFieldInteriorTrace_DGBR2_LiftingOperator_AD_btest_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_2Line_X1Q1R1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 4);

  // line solution data (left)
  qfld.DOF(0) = 2;
  qfld.DOF(1) = 3;

  // line solution data (right)
  qfld.DOF(2) = 7;
  qfld.DOF(3) = 4;

  // lifting-operator: P1 (aka R1)
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // Compute the field of inverse mass matrices
  FieldDataInvMassMatrix_Cell mmfld(qfld);

  // BR2 discretization
  Real viscousEtaParameter = 2.0;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcn( pde, disc, {0} );

  // quadrature rule (no quadrature required: value at a node)
  int quadratureOrder = 0;

  // base interface
  IntegrateInteriorTraceGroups<TopoD1>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                  xfld, (qfld, rfld), &quadratureOrder, 1);


  typedef FieldLift<PhysD1, TopoD1, VectorArrayQ>::FieldCellGroupType<Line> RFieldCellGroupType;
  typedef RFieldCellGroupType::ElementType<> ElementRFieldClass;

  const RFieldCellGroupType& rfldCell = rfld.getCellGroup<Line>(0);

  ElementRFieldClass rElemL( rfldCell.basis() );
  ElementRFieldClass rElemR( rfldCell.basis() );

  int elemL = 0, elemR = 1;
  int traceL = 0, traceR = 1;

  rfldCell.getElement(rElemL, elemL, traceL);
  rfldCell.getElement(rElemR, elemR, traceR);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  // Lifting operators
  Real rLxTrue[2];
  Real rRxTrue[2];

  //Lifting Operator (left)
  rLxTrue[0] = ( -4 );   // Basis function 1
  rLxTrue[1] = ( 8 );   // Basis function 2

  //Lifting Operator (right)
  rRxTrue[0] = ( 8 );   // Basis function 1
  rRxTrue[1] = ( -4 );   // Basis function 2

  SANS_CHECK_CLOSE( rLxTrue[0], rElemL.DOF(0)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rLxTrue[1], rElemL.DOF(1)[0], small_tol, close_tol );

  SANS_CHECK_CLOSE( rRxTrue[0], rElemR.DOF(0)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rRxTrue[1], rElemR.DOF(1)[0], small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_2Triangle_X1Q1R1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: P1 (aka X1)
  XField2D_2Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 6 );

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  // triangle solution data (right)
  qfld.DOF(3) = 7;
  qfld.DOF(4) = 2;
  qfld.DOF(5) = 9;

  // lifting-operator: P1 (aka R1)
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( rfld.nDOF(), 18 );

  // triangle auxiliary variable (left)
  rfld.DOF(0) = { 2, -3};
  rfld.DOF(1) = { 7,  8};
  rfld.DOF(2) = {-1,  7};
  rfld.DOF(3) = 0;
  rfld.DOF(4) = 0;
  rfld.DOF(5) = 0;
  rfld.DOF(6) = 0;
  rfld.DOF(7) = 0;
  rfld.DOF(8) = 0;

  // triangle auxiliary variable (right)
  rfld.DOF( 9) = { 8, -2};
  rfld.DOF(10) = {-5,  7};
  rfld.DOF(11) = { 3,  9};
  rfld.DOF(12) = 0;
  rfld.DOF(13) = 0;
  rfld.DOF(14) = 0;
  rfld.DOF(15) = 0;
  rfld.DOF(16) = 0;
  rfld.DOF(17) = 0;


  // Compute the field of inverse mass matrices
  FieldDataInvMassMatrix_Cell mmfld(qfld);

  // BR2 discretization
  Real viscousEtaParameter = 3.0;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcn( pde, disc, {0} );

  // quadrature rule (quadratic: basis & flux both linear)
  int quadratureOrder = 2;

  IntegrateInteriorTraceGroups<TopoD2>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                  xfld, (qfld, rfld), &quadratureOrder, 1);

  typedef FieldLift<PhysD2, TopoD2, VectorArrayQ>::FieldCellGroupType<Triangle> RFieldCellGroupType;
  typedef RFieldCellGroupType::ElementType<> ElementRFieldClass;

  const RFieldCellGroupType& rfldCell = rfld.getCellGroup<Triangle>(0);

  ElementRFieldClass rElemL( rfldCell.basis() );
  ElementRFieldClass rElemR( rfldCell.basis() );

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  int elemL = 0, elemR = 1;
  int traceL = 0, traceR = 0;

  rfldCell.getElement(rElemL, elemL, traceL);
  rfldCell.getElement(rElemR, elemR, traceR);

  Real rLxTrue[3], rLyTrue[3];
  Real rRxTrue[3], rRyTrue[3];

  //Lifting Operator (left)
  rLxTrue[0] = ( -6 );   // Basis function 1
  rLxTrue[1] = ( 14 );   // Basis function 2
  rLxTrue[2] = ( -2 );   // Basis function 3
  rLyTrue[0] = ( -6 );   // Basis function 1
  rLyTrue[1] = ( 14 );   // Basis function 2
  rLyTrue[2] = ( -2 );   // Basis function 3

  //Lifting Operator (right)
  rRxTrue[0] = ( -6 );   // Basis function 1
  rRxTrue[1] = ( -2 );   // Basis function 2
  rRxTrue[2] = ( 14 );   // Basis function 3
  rRyTrue[0] = ( -6 );   // Basis function 1
  rRyTrue[1] = ( -2 );   // Basis function 2
  rRyTrue[2] = ( 14 );   // Basis function 3

  SANS_CHECK_CLOSE( rLxTrue[0], rElemL.DOF(0)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rLxTrue[1], rElemL.DOF(1)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rLxTrue[2], rElemL.DOF(2)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rLyTrue[0], rElemL.DOF(0)[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rLyTrue[1], rElemL.DOF(1)[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rLyTrue[2], rElemL.DOF(2)[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rRxTrue[0], rElemR.DOF(0)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rRxTrue[1], rElemR.DOF(1)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rRxTrue[2], rElemR.DOF(2)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rRyTrue[0], rElemR.DOF(0)[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rRyTrue[1], rElemR.DOF(1)[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rRyTrue[2], rElemR.DOF(2)[1], small_tol, close_tol );


}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
