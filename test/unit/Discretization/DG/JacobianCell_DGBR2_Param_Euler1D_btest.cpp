// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianCell_DGBR2_Param_Euler1D_btest
// testing of 2-D line-integral jacobian: Euler1D

//#define DISPLAY_FOR_DEBUGGING

#include <boost/mpl/list.hpp>

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/Q1DPrimitiveSurrogate.h"
#include "pde/NS/Q1DConservative.h"
#include "pde/NS/Q1DEntropy.h"
#include "pde/NS/PDEEuler1D.h"
#include "pde/NS/PDEEulermitAVDiffusion1D.h"
#include "pde/NS/Fluids1D_Sensor.h"

#include "pde/Sensor/PDESensorParameter1D.h"
#include "pde/Sensor/Source1D_JumpSensor.h"
#include "pde/Sensor/BCSensorParameter1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/HField/HField_DG.h"

#include "Discretization/DG/IntegrandCell_DGBR2.h"
#include "Discretization/DG/JacobianCell_DGBR2_Param.h"
#include "Discretization/DG/ResidualCell_DGBR2.h"
#include "Discretization/IntegrateCellGroups.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "Surreal/SurrealS.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
class QTypeEntropy {};
}

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

#define SANS_CHECK_PING_ORDER( err_vec, delta_vec, small_tol ) \
    if (err_vec[1] > small_tol){ \
      Real rate = log(err_vec[1]/err_vec[0])/log(delta_vec[1]/delta_vec[0]); \
      BOOST_CHECK_MESSAGE( rate >= 1.9 && rate <= 4.0, "Rate check failed: rate = " << rate << \
                           ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" ); \
    }


//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianCell_Galerkin_Param_Euler1D_test_suite )

typedef boost::mpl::list< QTypePrimitiveRhoPressure//,
//                          QTypeConservative,
//                          QTypePrimitiveSurrogate,
//                          QTypeEntropy
                        > QTypes;
#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Galerkin2D_1Line_X1Q1Param1_Surreal_density , QType, QTypes )
{
  typedef PhysD1 PhysDim;
  typedef TopoD1 TopoDim;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef PDENDConvertSpace<PhysDim,PDEClass> NDPDEClass;
  typedef PDENDConvertSpace<PhysDim,AVPDEClass> NDAVPDEClass;
  typedef typename NDAVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDAVPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDAVPDEClass::template MatrixParam<Real> MatrixParam;

  typedef Fluids_Sensor<PhysD1, PDEClass> Sensor;
  typedef Source1D_JumpSensor<Sensor> Source_JumpSensor;
  typedef PDESensorParameter<PhysD1,
                             TraitsSizeEuler<PhysD1>,
                             Sensor_AdvectiveFlux1D_None,
                             Sensor_ViscousFlux1D_None,
                             Source_JumpSensor > SensorPDEClass;
  typedef PDENDConvertSpace<PhysD1, SensorPDEClass> SensorNDPDEClass;
  typedef typename SensorNDPDEClass::template ArrayQ<Real> SensorArrayQ;

  typedef SurrealS<AVPDEClass::Nparam> SurrealClass;

  typedef IntegrandCell_DGBR2<NDAVPDEClass> IntegrandClass;

  // ----------------------- SET UP FIELDS ----------------------- //
  const int qorder = 1;
  const int sorder = 0;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);
  NDAVPDEClass avpde(qorder, pde);

  // static tests
  BOOST_CHECK( avpde.D == 1 );
  BOOST_CHECK( avpde.N == 3 );
  BOOST_CHECK( avpde.Nparam == 1 );

  Sensor sensor(pde);
  Sensor_AdvectiveFlux1D_None sensor_adv;
  Sensor_ViscousFlux1D_None sensor_visc;
  Source_JumpSensor sensor_source(sensor);

  SensorNDPDEClass sensorpde(sensor_adv, sensor_visc, sensor_source);

  // grid: 2 lines @ P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;
  HField_DG<PhysDim,TopoDim> hfld(xfld);

  // solution: 2 lines @ P0 (aka Q0)
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  qfld.DOF(0) = pde.setDOFFrom( DensityVelocityPressure1D<Real>(0.5, 0, 0.5) );
  qfld.DOF(1) = pde.setDOFFrom( DensityVelocityPressure1D<Real>(0.6, 0, 0.5) );
  qfld.DOF(2) = pde.setDOFFrom( DensityVelocityPressure1D<Real>(0.7, 0, 0.5) );
  qfld.DOF(3) = pde.setDOFFrom( DensityVelocityPressure1D<Real>(0.8, 0, 0.5) );
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  rfld = 0.0;
  // Set up a sensor field
  Field_DG_Cell<PhysD1, TopoD1, SensorArrayQ> sensorfld(xfld, sorder, BasisFunctionCategory_Legendre);
  sensorfld = 0.0;

  // solution
  sensorfld.DOF(0) = 0.003;
  sensorfld.DOF(1) = 0.007;

  // ----------------------- INTEGRATE ----------------------- //
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);
  // integrand
  IntegrandClass fcnint( avpde, disc, {0} );

  const int nCellGroup = 1;

  const int quadratureOrder = -1;

  const int nDOF = 2 * (qorder + 1);
  const int paramDOF = 2 * (sorder + 1);

  const int nPDE = avpde.N;
  const int nArrayP = avpde.Nparam;

  const int m_jac = nPDE*nDOF;         // number of rows in jac
  const int n_jac = nArrayP*paramDOF;  // number of columns in jac

  // jacobian via FD w/ residual operator
  const int nk = 2;              // number of FD steps
  Real delta[nk] = {0.001, 0.0001}; // FD step sizes

  Real jac[m_jac][n_jac][nk];

  SLA::SparseVector<ArrayQ> rsdGlobalm1(nDOF), rsdGlobalp1(nDOF);

  for (int kk = 0; kk < nk; kk++)
  {
    for (int j = 0; j < paramDOF; j++)
    {
      for (int n = 0; n < nArrayP; n++)
      {
        // residual(param - d param)
        sensorfld.DOF(j) -= delta[kk];
        rsdGlobalm1 = 0;
        IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobalm1),
                                                (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, nCellGroup);

        // residual(param + d param)
        sensorfld.DOF(j) += 2.0*delta[kk];
        rsdGlobalp1 = 0;
        IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobalp1),
                                                (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, nCellGroup);

        BOOST_REQUIRE(rsdGlobalm1.m()*nPDE == m_jac);
        BOOST_REQUIRE(rsdGlobalp1.m()*nPDE == m_jac);

        const int slot_column = j*nArrayP + n;
        for (int i = 0; i < nDOF; i++)
        {
          for (int m = 0; m < nPDE; m++)
          {
            const int slot_row = i*nPDE + m;
            jac[slot_row][slot_column][kk] = (rsdGlobalp1[i][m] - rsdGlobalm1[i][m]) / (2.0*delta[kk]);
          }
        }

        // reset param
        sensorfld.DOF(j) -= delta[kk];
      }
    }
  }

  // jacobian via Surreal
  DLA::MatrixD<MatrixParam> mtxGlob(nDOF,paramDOF);
  mtxGlob = 0;
  const int iParam = 1; // parameter index in tuple
  IntegrateCellGroups<TopoD1>::integrate( JacobianCell_DGBR2_Param<SurrealClass, iParam>(fcnint, mtxGlob),
                                          (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, nCellGroup);
  // ping test
  Real err[nk];
  const Real small_tol = 1e-10;
  for (int i = 0; i < nDOF; i++)
  {
    for (int m = 0; m < nPDE; m++)
    {
      const int slot_row = i*nPDE + m;
      for (int j = 0; j < paramDOF; j++)
      {
        for (int n = 0; n < nArrayP; n++)
        {
          const int slot_column = j*nArrayP + n;
          for (int k = 0; k < nk; k++)
          {
            err[k] = (jac[slot_row][slot_column][k] - DLA::index(mtxGlob(i,j),m,n));
          }
          SANS_CHECK_PING_ORDER(err, delta, small_tol);
        }
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Galerkin2D_1Line_X1Q1Param1_Surreal_velocity , QType, QTypes )
{
  typedef PhysD1 PhysDim;
  typedef TopoD1 TopoDim;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef PDENDConvertSpace<PhysDim,PDEClass> NDPDEClass;
  typedef PDENDConvertSpace<PhysDim,AVPDEClass> NDAVPDEClass;
  typedef typename NDAVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDAVPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDAVPDEClass::template MatrixParam<Real> MatrixParam;

  typedef Fluids_Sensor<PhysD1, PDEClass> Sensor;
  typedef Source1D_JumpSensor<Sensor> Source_JumpSensor;
  typedef PDESensorParameter<PhysD1,
                             TraitsSizeEuler<PhysD1>,
                             Sensor_AdvectiveFlux1D_None,
                             Sensor_ViscousFlux1D_None,
                             Source_JumpSensor > SensorPDEClass;
  typedef PDENDConvertSpace<PhysD1, SensorPDEClass> SensorNDPDEClass;
  typedef typename SensorNDPDEClass::template ArrayQ<Real> SensorArrayQ;

  typedef SurrealS<AVPDEClass::Nparam> SurrealClass;

  typedef IntegrandCell_DGBR2<NDAVPDEClass> IntegrandClass;

  // ----------------------- SET UP FIELDS ----------------------- //
  const int qorder = 1;
  const int sorder = 0;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);
  NDAVPDEClass avpde(qorder, pde);

  // static tests
  BOOST_CHECK( avpde.D == 1 );
  BOOST_CHECK( avpde.N == 3 );
  BOOST_CHECK( avpde.Nparam == 1 );

  Sensor sensor(pde);
  Sensor_AdvectiveFlux1D_None sensor_adv;
  Sensor_ViscousFlux1D_None sensor_visc;
  Source_JumpSensor sensor_source(sensor);

  SensorNDPDEClass sensorpde(sensor_adv, sensor_visc, sensor_source);

  // grid: 2 lines @ P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;
  HField_DG<PhysDim,TopoDim> hfld(xfld);

  // solution: 2 lines @ P0 (aka Q0)
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  qfld.DOF(0) = pde.setDOFFrom( DensityVelocityPressure1D<Real>(0.5,  0.1, 0.5) );
  qfld.DOF(1) = pde.setDOFFrom( DensityVelocityPressure1D<Real>(0.5,  0.5, 0.5) );
  qfld.DOF(2) = pde.setDOFFrom( DensityVelocityPressure1D<Real>(0.5,    0, 0.5) );
  qfld.DOF(3) = pde.setDOFFrom( DensityVelocityPressure1D<Real>(0.5, -0.5, 0.5) );
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  rfld = 0.0;
  // Set up a sensor field
  Field_DG_Cell<PhysD1, TopoD1, SensorArrayQ> sensorfld(xfld, sorder, BasisFunctionCategory_Legendre);
  sensorfld = 0.0;

  // solution
  sensorfld.DOF(0) = 0.003;
  sensorfld.DOF(1) = 0.007;

  // ----------------------- INTEGRATE ----------------------- //
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);
  // integrand
  IntegrandClass fcnint( avpde, disc, {0} );

  const int nCellGroup = 1;

  const int quadratureOrder = -1;

  const int nDOF = 2 * (qorder + 1);
  const int paramDOF = 2 * (sorder + 1);

  const int nPDE = avpde.N;
  const int nArrayP = avpde.Nparam;

  const int m_jac = nPDE*nDOF;         // number of rows in jac
  const int n_jac = nArrayP*paramDOF;  // number of columns in jac

  // jacobian via FD w/ residual operator
  const int nk = 2;              // number of FD steps
  Real delta[nk] = {0.001, 0.0001}; // FD step sizes

  Real jac[m_jac][n_jac][nk];

  SLA::SparseVector<ArrayQ> rsdGlobalm1(nDOF), rsdGlobalp1(nDOF);

  for (int kk = 0; kk < nk; kk++)
  {
    for (int j = 0; j < paramDOF; j++)
    {
      for (int n = 0; n < nArrayP; n++)
      {
        // residual(param - d param)
        sensorfld.DOF(j) -= delta[kk];
        rsdGlobalm1 = 0;
        IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobalm1),
                                                (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, nCellGroup);

        // residual(param + d param)
        sensorfld.DOF(j) += 2.0*delta[kk];
        rsdGlobalp1 = 0;
        IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobalp1),
                                                (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, nCellGroup);

        BOOST_REQUIRE(rsdGlobalm1.m()*nPDE == m_jac);
        BOOST_REQUIRE(rsdGlobalp1.m()*nPDE == m_jac);

        const int slot_column = j*nArrayP + n;
        for (int i = 0; i < nDOF; i++)
        {
          for (int m = 0; m < nPDE; m++)
          {
            const int slot_row = i*nPDE + m;
            jac[slot_row][slot_column][kk] = (rsdGlobalp1[i][m] - rsdGlobalm1[i][m]) / (2.0*delta[kk]);
          }
        }

        // reset param
        sensorfld.DOF(j) -= delta[kk];
      }
    }
  }

  // jacobian via Surreal
  DLA::MatrixD<MatrixParam> mtxGlob(nDOF,paramDOF);
  mtxGlob = 0;
  const int iParam = 1; // parameter index in tuple
  IntegrateCellGroups<TopoD1>::integrate( JacobianCell_DGBR2_Param<SurrealClass, iParam>(fcnint, mtxGlob),
                                          (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, nCellGroup);
  // ping test
  Real err[nk];
  const Real small_tol = 1e-10;
  for (int i = 0; i < nDOF; i++)
  {
    for (int m = 0; m < nPDE; m++)
    {
      const int slot_row = i*nPDE + m;
      for (int j = 0; j < paramDOF; j++)
      {
        for (int n = 0; n < nArrayP; n++)
        {
          const int slot_column = j*nArrayP + n;
          for (int k = 0; k < nk; k++)
          {
            err[k] = (jac[slot_row][slot_column][k] - DLA::index(mtxGlob(i,j),m,n));
          }
          SANS_CHECK_PING_ORDER(err, delta, small_tol);
        }
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Galerkin2D_1Line_X1Q1Param1_Surreal_pressure , QType, QTypes )
{
  typedef PhysD1 PhysDim;
  typedef TopoD1 TopoDim;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef PDENDConvertSpace<PhysDim,PDEClass> NDPDEClass;
  typedef PDENDConvertSpace<PhysDim,AVPDEClass> NDAVPDEClass;
  typedef typename NDAVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDAVPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDAVPDEClass::template MatrixParam<Real> MatrixParam;

  typedef Fluids_Sensor<PhysD1, PDEClass> Sensor;
  typedef Source1D_JumpSensor<Sensor> Source_JumpSensor;
  typedef PDESensorParameter<PhysD1,
                             TraitsSizeEuler<PhysD1>,
                             Sensor_AdvectiveFlux1D_None,
                             Sensor_ViscousFlux1D_None,
                             Source_JumpSensor > SensorPDEClass;
  typedef PDENDConvertSpace<PhysD1, SensorPDEClass> SensorNDPDEClass;
  typedef typename SensorNDPDEClass::template ArrayQ<Real> SensorArrayQ;

  typedef SurrealS<AVPDEClass::Nparam> SurrealClass;

  typedef IntegrandCell_DGBR2<NDAVPDEClass> IntegrandClass;

  // ----------------------- SET UP FIELDS ----------------------- //
  const int qorder = 1;
  const int sorder = 0;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);
  NDAVPDEClass avpde(qorder, pde);

  // static tests
  BOOST_CHECK( avpde.D == 1 );
  BOOST_CHECK( avpde.N == 3 );
  BOOST_CHECK( avpde.Nparam == 1 );

  Sensor sensor(pde);
  Sensor_AdvectiveFlux1D_None sensor_adv;
  Sensor_ViscousFlux1D_None sensor_visc;
  Source_JumpSensor sensor_source(sensor);

  SensorNDPDEClass sensorpde(sensor_adv, sensor_visc, sensor_source);

  // grid: 2 lines @ P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;
  HField_DG<PhysDim,TopoDim> hfld(xfld);

  // solution: 2 lines @ P0 (aka Q0)
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  qfld.DOF(0) = pde.setDOFFrom( DensityVelocityPressure1D<Real>(0.5, 0, 0.5) );
  qfld.DOF(1) = pde.setDOFFrom( DensityVelocityPressure1D<Real>(0.5, 0, 0.8) );
  qfld.DOF(2) = pde.setDOFFrom( DensityVelocityPressure1D<Real>(0.5, 0, 0.4) );
  qfld.DOF(3) = pde.setDOFFrom( DensityVelocityPressure1D<Real>(0.5, 0, 0.1) );
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  rfld = 0.0;
  // Set up a sensor field
  Field_DG_Cell<PhysD1, TopoD1, SensorArrayQ> sensorfld(xfld, sorder, BasisFunctionCategory_Legendre);
  sensorfld = 0.0;

  // solution
  sensorfld.DOF(0) = 0.003;
  sensorfld.DOF(1) = 0.007;

  // ----------------------- INTEGRATE ----------------------- //
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);
  // integrand
  IntegrandClass fcnint( avpde, disc, {0} );

  const int nCellGroup = 1;

  const int quadratureOrder = -1;

  const int nDOF = 2 * (qorder + 1);
  const int paramDOF = 2 * (sorder + 1);

  const int nPDE = avpde.N;
  const int nArrayP = avpde.Nparam;

  const int m_jac = nPDE*nDOF;         // number of rows in jac
  const int n_jac = nArrayP*paramDOF;  // number of columns in jac

  // jacobian via FD w/ residual operator
  const int nk = 2;              // number of FD steps
  Real delta[nk] = {0.001, 0.0001}; // FD step sizes

  Real jac[m_jac][n_jac][nk];

  SLA::SparseVector<ArrayQ> rsdGlobalm1(nDOF), rsdGlobalp1(nDOF);

  for (int kk = 0; kk < nk; kk++)
  {
    for (int j = 0; j < paramDOF; j++)
    {
      for (int n = 0; n < nArrayP; n++)
      {
        // residual(param - d param)
        sensorfld.DOF(j) -= delta[kk];
        rsdGlobalm1 = 0;
        IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobalm1),
                                                (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, nCellGroup);

        // residual(param + d param)
        sensorfld.DOF(j) += 2.0*delta[kk];
        rsdGlobalp1 = 0;
        IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobalp1),
                                                (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, nCellGroup);

        BOOST_REQUIRE(rsdGlobalm1.m()*nPDE == m_jac);
        BOOST_REQUIRE(rsdGlobalp1.m()*nPDE == m_jac);

        const int slot_column = j*nArrayP + n;
        for (int i = 0; i < nDOF; i++)
        {
          for (int m = 0; m < nPDE; m++)
          {
            const int slot_row = i*nPDE + m;
            jac[slot_row][slot_column][kk] = (rsdGlobalp1[i][m] - rsdGlobalm1[i][m]) / (2.0*delta[kk]);
          }
        }

        // reset param
        sensorfld.DOF(j) -= delta[kk];
      }
    }
  }

  // jacobian via Surreal
  DLA::MatrixD<MatrixParam> mtxGlob(nDOF,paramDOF);
  mtxGlob = 0;
  const int iParam = 1; // parameter index in tuple
  IntegrateCellGroups<TopoD1>::integrate( JacobianCell_DGBR2_Param<SurrealClass, iParam>(fcnint, mtxGlob),
                                          (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, nCellGroup);
  // ping test
  Real err[nk];
  const Real small_tol = 1e-10;
  for (int i = 0; i < nDOF; i++)
  {
    for (int m = 0; m < nPDE; m++)
    {
      const int slot_row = i*nPDE + m;
      for (int j = 0; j < paramDOF; j++)
      {
        for (int n = 0; n < nArrayP; n++)
        {
          const int slot_column = j*nArrayP + n;
          for (int k = 0; k < nk; k++)
          {
            err[k] = (jac[slot_row][slot_column][k] - DLA::index(mtxGlob(i,j),m,n));
          }
          SANS_CHECK_PING_ORDER(err, delta, small_tol);
        }
      }
    }
  }
}
#endif
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Galerkin2D_1Line_X1Q1Param1_Surreal_Sod , QType, QTypes )
{
  typedef PhysD1 PhysDim;
  typedef TopoD1 TopoDim;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef PDENDConvertSpace<PhysDim,AVPDEClass> NDAVPDEClass;
  typedef typename NDAVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDAVPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDAVPDEClass::template MatrixParam<Real> MatrixParam;

  typedef Fluids_Sensor<PhysD1, PDEClass> Sensor;
  typedef Source1D_JumpSensor<Sensor> Source_JumpSensor;
  typedef PDESensorParameter<PhysD1,
                             TraitsSizeEuler<PhysD1>,
                             Sensor_AdvectiveFlux1D_None,
                             Sensor_ViscousFlux1D_None,
                             Source_JumpSensor > SensorPDEClass;
  typedef PDENDConvertSpace<PhysD1, SensorPDEClass> SensorNDPDEClass;
  typedef typename SensorNDPDEClass::template ArrayQ<Real> SensorArrayQ;

  typedef SurrealS<AVPDEClass::Nparam> SurrealClass;

  typedef IntegrandCell_DGBR2<NDAVPDEClass> IntegrandClass;

  // ----------------------- SET UP FIELDS ----------------------- //
  const int qorder = 1;
  const int sorder = 0;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  NDAVPDEClass avpde(qorder, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEClass::Euler_ResidInterp_Raw);

  // static tests
  BOOST_CHECK( avpde.D == 1 );
  BOOST_CHECK( avpde.N == 3 );
  BOOST_CHECK( avpde.Nparam == 1 );

  Sensor sensor(avpde);
  Sensor_AdvectiveFlux1D_None sensor_adv;
  Sensor_ViscousFlux1D_None sensor_visc;
  Source_JumpSensor sensor_source(qorder, sensor);

  SensorNDPDEClass sensorpde(sensor_adv, sensor_visc, sensor_source);

  // grid: 2 lines @ P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;
  HField_DG<PhysDim,TopoDim> hfld(xfld);

  // solution: 2 lines @ P0 (aka Q0)
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  qfld.DOF(0) = avpde.setDOFFrom( DensityVelocityPressure1D<Real>(1.0,   0, 1.0) );
  qfld.DOF(1) = avpde.setDOFFrom( DensityVelocityPressure1D<Real>(1.0,   0, 1.0) );
  qfld.DOF(2) = avpde.setDOFFrom( DensityVelocityPressure1D<Real>(0.125, 0, 0.1) );
  qfld.DOF(3) = avpde.setDOFFrom( DensityVelocityPressure1D<Real>(0.125, 0, 0.1) );
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  rfld = 0.0;
  // Set up a sensor field
  Field_DG_Cell<PhysD1, TopoD1, SensorArrayQ> sensorfld(xfld, sorder, BasisFunctionCategory_Legendre);
  sensorfld = 0.0;

  // solution
  sensorfld.DOF(0) = 0.003;
  sensorfld.DOF(1) = 0.007;

  // ----------------------- INTEGRATE ----------------------- //
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);
  // integrand
  IntegrandClass fcnint( avpde, disc, {0} );

  const int nCellGroup = 1;

  const int quadratureOrder = -1;

  const int nDOF = 2 * (qorder + 1);
  const int paramDOF = 2 * (sorder + 1);

  const int nPDE = avpde.N;
  const int nArrayP = avpde.Nparam;

  const int m_jac = nPDE*nDOF;         // number of rows in jac
  const int n_jac = nArrayP*paramDOF;  // number of columns in jac

  // jacobian via FD w/ residual operator
  const int nk = 2;              // number of FD steps
  Real delta[nk] = {0.001, 0.0001}; // FD step sizes

  Real jac[m_jac][n_jac][nk];

  SLA::SparseVector<ArrayQ> rsdGlobalm1(nDOF), rsdGlobalp1(nDOF);

  for (int kk = 0; kk < nk; kk++)
  {
    for (int j = 0; j < paramDOF; j++)
    {
      for (int n = 0; n < nArrayP; n++)
      {
        // residual(param - d param)
        sensorfld.DOF(j) -= delta[kk];
        rsdGlobalm1 = 0;
        IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobalm1),
                                                (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, nCellGroup);

        // residual(param + d param)
        sensorfld.DOF(j) += 2.0*delta[kk];
        rsdGlobalp1 = 0;
        IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobalp1),
                                                (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, nCellGroup);

        BOOST_REQUIRE(rsdGlobalm1.m()*nPDE == m_jac);
        BOOST_REQUIRE(rsdGlobalp1.m()*nPDE == m_jac);

        const int slot_column = j*nArrayP + n;
        for (int i = 0; i < nDOF; i++)
        {
          for (int m = 0; m < nPDE; m++)
          {
            const int slot_row = i*nPDE + m;
            jac[slot_row][slot_column][kk] = (rsdGlobalp1[i][m] - rsdGlobalm1[i][m]) / (2.0*delta[kk]);
          }
        }

        // reset param
        sensorfld.DOF(j) -= delta[kk];
      }
    }
  }

  // jacobian via Surreal
  DLA::MatrixD<MatrixParam> mtxGlob(nDOF,paramDOF);
  mtxGlob = 0;
  const int iParam = 1; // parameter index in tuple
  IntegrateCellGroups<TopoD1>::integrate( JacobianCell_DGBR2_Param<SurrealClass, iParam>(fcnint, mtxGlob),
                                          (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, nCellGroup);
  // ping test
  Real err[nk];
  const Real small_tol = 1e-10;
  for (int i = 0; i < nDOF; i++)
  {
    for (int m = 0; m < nPDE; m++)
    {
      const int slot_row = i*nPDE + m;
      for (int j = 0; j < paramDOF; j++)
      {
        for (int n = 0; n < nArrayP; n++)
        {
          const int slot_column = j*nArrayP + n;
          for (int k = 0; k < nk; k++)
          {
            err[k] = (jac[slot_row][slot_column][k] - DLA::index(mtxGlob(i,j),m,n));
          }
          SANS_CHECK_PING_ORDER(err, delta, small_tol);
#ifdef DISPLAY_FOR_DEBUGGING
          std::cout << "Jac(" << slot_row << "," << slot_column << ") = " << jac[slot_row][slot_column][1] << std::endl;
#endif
        }
      }
    }
  }
}
#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Galerkin2D_3Line_X1Q1Param1_Surreal_density, QType, QTypes )
{
  typedef PhysD1 PhysDim;
  typedef TopoD1 TopoDim;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef PDENDConvertSpace<PhysDim,PDEClass> NDPDEClass;
  typedef PDENDConvertSpace<PhysDim,AVPDEClass> NDAVPDEClass;
  typedef typename NDAVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDAVPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDAVPDEClass::template MatrixParam<Real> MatrixParam;

  typedef Fluids_Sensor<PhysD1, PDEClass> Sensor;
  typedef Source1D_JumpSensor<Sensor> Source_JumpSensor;
  typedef PDESensorParameter<PhysD1,
                             TraitsSizeEuler<PhysD1>,
                             Sensor_AdvectiveFlux1D_None,
                             Sensor_ViscousFlux1D_None,
                             Source_JumpSensor > SensorPDEClass;
  typedef PDENDConvertSpace<PhysD1, SensorPDEClass> SensorNDPDEClass;
  typedef typename SensorNDPDEClass::template ArrayQ<Real> SensorArrayQ;

  typedef SurrealS<AVPDEClass::Nparam> SurrealClass;

  typedef IntegrandCell_DGBR2<NDAVPDEClass> IntegrandClass;

  // ----------------------- SET UP FIELDS ----------------------- //
  const int qorder = 1;
  const int sorder = 0;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);
  NDAVPDEClass avpde(qorder, pde);

  // static tests
  BOOST_CHECK( avpde.D == 1 );
  BOOST_CHECK( avpde.N == 3 );
  BOOST_CHECK( avpde.Nparam == 1 );

  Sensor sensor(pde);
  Sensor_AdvectiveFlux1D_None sensor_adv;
  Sensor_ViscousFlux1D_None sensor_visc;
  Source_JumpSensor sensor_source(sensor);

  SensorNDPDEClass sensorpde(sensor_adv, sensor_visc, sensor_source);

  // grid: 2 lines @ P1 (aka X1)
  XField1D xfld( 3 );
  HField_DG<PhysDim,TopoDim> hfld(xfld);

  // solution: 2 lines @ P0 (aka Q0)
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
  qfld.DOF(0) = pde.setDOFFrom( DensityVelocityPressure1D<Real>(0.5, 0, 0.5) );
  qfld.DOF(1) = pde.setDOFFrom( DensityVelocityPressure1D<Real>(0.6, 0, 0.5) );
  qfld.DOF(2) = pde.setDOFFrom( DensityVelocityPressure1D<Real>(0.7, 0, 0.5) );
  qfld.DOF(3) = pde.setDOFFrom( DensityVelocityPressure1D<Real>(0.8, 0, 0.5) );
  qfld.DOF(4) = pde.setDOFFrom( DensityVelocityPressure1D<Real>(0.1, 0, 0.5) );
  qfld.DOF(5) = pde.setDOFFrom( DensityVelocityPressure1D<Real>(0.8, 0, 0.5) );
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Legendre);
  rfld = 0.0;
  // Set up a sensor field
  Field_DG_Cell<PhysD1, TopoD1, SensorArrayQ> sensorfld(xfld, sorder, BasisFunctionCategory_Legendre);
  sensorfld = 0.0;

  // solution
  sensorfld.DOF(0) = 0.003;
  sensorfld.DOF(1) = 0.007;
  sensorfld.DOF(2) = 0.004;


  // ----------------------- INTEGRATE ----------------------- //
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);
  // integrand
  IntegrandClass fcnint( avpde, disc, {0} );

  const int nCellGroup = 1;

  const int quadratureOrder = -1;

  const int nDOF = 3 * (qorder + 1);
  const int paramDOF = 3 * (sorder + 1);

  const int nPDE = avpde.N;
  const int nArrayP = avpde.Nparam;

  const int m_jac = nPDE*nDOF;         // number of rows in jac
  const int n_jac = nArrayP*paramDOF;  // number of columns in jac

  // jacobian via FD w/ residual operator
  const int nk = 2;              // number of FD steps
  Real delta[nk] = {0.001, 0.0001}; // FD step sizes

  Real jac[m_jac][n_jac][nk];

  SLA::SparseVector<ArrayQ> rsdGlobalm1(nDOF), rsdGlobalp1(nDOF);

  for (int kk = 0; kk < nk; kk++)
  {
    for (int j = 0; j < paramDOF; j++)
    {
      for (int n = 0; n < nArrayP; n++)
      {
        // residual(param - d param)
        sensorfld.DOF(j) -= delta[kk];
        rsdGlobalm1 = 0;
        IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobalm1),
                                                (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, nCellGroup);

        // residual(param + d param)
        sensorfld.DOF(j) += 2.0*delta[kk];
        rsdGlobalp1 = 0;
        IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobalp1),
                                                (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, nCellGroup);

        BOOST_REQUIRE(rsdGlobalm1.m()*nPDE == m_jac);
        BOOST_REQUIRE(rsdGlobalp1.m()*nPDE == m_jac);

        const int slot_column = j*nArrayP + n;
        for (int i = 0; i < nDOF; i++)
        {
          for (int m = 0; m < nPDE; m++)
          {
            const int slot_row = i*nPDE + m;
            jac[slot_row][slot_column][kk] = (rsdGlobalp1[i][m] - rsdGlobalm1[i][m]) / (2.0*delta[kk]);
          }
        }

        // reset param
        sensorfld.DOF(j) -= delta[kk];
      }
    }
  }

  // jacobian via Surreal
  DLA::MatrixD<MatrixParam> mtxGlob(nDOF,paramDOF);
  mtxGlob = 0;
  const int iParam = 1; // parameter index in tuple
  IntegrateCellGroups<TopoD1>::integrate( JacobianCell_DGBR2_Param<SurrealClass, iParam>(fcnint, mtxGlob),
                                          (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, nCellGroup);

  // ping test
  Real err[nk];
  const Real small_tol = 1e-10;
  for (int i = 0; i < nDOF; i++)
  {
    for (int m = 0; m < nPDE; m++)
    {
      const int slot_row = i*nPDE + m;
      for (int j = 0; j < paramDOF; j++)
      {
        for (int n = 0; n < nArrayP; n++)
        {
          const int slot_column = j*nArrayP + n;
          for (int k = 0; k < nk; k++)
          {
            err[k] = (jac[slot_row][slot_column][k] - DLA::index(mtxGlob(i,j),m,n));
          }
          SANS_CHECK_PING_ORDER(err, delta, small_tol);
        }
      }
    }
  }
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
