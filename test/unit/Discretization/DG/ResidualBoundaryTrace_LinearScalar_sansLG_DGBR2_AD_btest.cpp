// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ResidualBoundaryTrace_AD_btest
// testing of boundary integral residual functions with Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldVolume_DG_Cell.h"

#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "Discretization/DG/ResidualBoundaryTrace_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_LinearScalar_sansLG_DGBR2.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Quad_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Hex_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ResidualBoundaryTrace_LinearScalar_sansLG_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_1D_1Line_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_sansLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;

  // PDE

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // BC

  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  // grid: P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle solution data (left)
  qfld.DOF(0) = 2;
  qfld.DOF(1) = 3;

  // lifting-operator: P1
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( 4, rfld.nDOF() );

  // lifting operator data

  // BC group 1 is canonical trace 0, so only set first 2 DOF
  rfld.DOF(0) = -1; rfld.DOF(1) =  5;

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcn( pde, bc, {1}, disc );

  // quadrature rule
  int quadratureorder[2] = {0,0};

  // PDE global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(2);

  rsdPDEGlobal = 0;

  IntegrateBoundaryTraceGroups<TopoD1>::integrate( ResidualBoundaryTrace_DGBR2(fcn, rsdPDEGlobal),
                                                   xfld, (qfld, rfld), quadratureorder, 2 );

#if 0
  cout << "rsdPDEGlobal[0] = " << rsdPDEGlobal[0] << endl;
  cout << "rsdPDEGlobal[1] = " << rsdPDEGlobal[1] << endl;
  cout << "rsdPDEGlobal[2] = " << rsdPDEGlobal[2] << endl;
#endif

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  Real rsdPDETrue[2];

  //PDE residuals (left): (advective) + (viscous) + (BC) + (lifting-operator)
  rsdPDETrue[0] = ( 0 ) + ( 0 ) + ( 150858257./6500000. ) + ( 0 ); // Basis function 1
  rsdPDETrue[1] = ( 33./10. ) + ( -2123./1000. ) + ( -122434657./6500000. ) + ( -2123./100. ); // Basis function 2

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDEGlobal[1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_2D_1Triangle_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_sansLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC

  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // grid: P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL(3, qfld.nDOF());

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  // lifting operators
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL(9, rfld.nDOF());

  // BC group 1 is canonical trace 0, so only set first 3 DOF
  rfld.DOF(0) = { 2, -3};  rfld.DOF(1) = { 7,  8};  rfld.DOF(2) = {-1,  7};

  // integrand
  IntegrandClass fcn( pde, bc, {1}, disc );

  // quadrature rule
  int quadratureorder[3] = {2,2,2};

  Real rsdPDETrue[3];

  //PDE residuals (left): (advective) + (viscous) + (BC) + (lifting-operator)
  rsdPDETrue[0] = ( 0 ) + ( 0 ) + ( (1059/8125.)*(10+(963)*(sqrt(2))) ) + ( 0 );   // Basis function 1
  rsdPDETrue[1] = ( 13/6. ) + ( -627/125. ) +
                  ( (1/48750.)*((pow(2,-1/2.))*(-9925971+(2506070)*(sqrt(2)))) ) + ( -17667/250. );   // Basis function 2
  rsdPDETrue[2] = ( 143/60. ) + ( -627/125. ) +
                  ( (1/48750.)*((pow(2,-1/2.))*(-6022533+(1744390)*(sqrt(2)))) ) + ( -477/10. );   // Basis function 3

  // PDE/BC global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(3);

  // topology-specific single group interface

  rsdPDEGlobal = 0;

  IntegrateBoundaryTraceGroups<TopoD2>::integrate( ResidualBoundaryTrace_DGBR2(fcn, rsdPDEGlobal),
                                                      xfld, (qfld, rfld), quadratureorder, 3 );

#if 0
  cout << "rsdPDEGlobal[0] = " << rsdPDEGlobal[0] << endl;
  cout << "rsdPDEGlobal[1] = " << rsdPDEGlobal[1] << endl;
  cout << "rsdPDEGlobal[2] = " << rsdPDEGlobal[2] << endl;
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDEGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[2], rsdPDEGlobal[2], small_tol, close_tol );
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_2D_1Quad_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_sansLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC

  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField2D_1Quad_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // quad solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;
  qfld.DOF(3) = 6;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // Lagrange multiplier DOF data
  lgfld.DOF(0) =  7;
  lgfld.DOF(1) = -9;

  // integrand
  IntegrandClass fcn( pde, bc, {0} );

  // quadrature rule
  int quadratureorder[4] = {2,2,2,2};

  // PDE residuals (left): (advective) + (viscous) + (Lagrange)
  Real rsd1PDE = (-1./6.)  + (2879./1200.)  + (-42764651./15000000.);
  Real rsd2PDE = (-7./30.) + (10367./6000.) + (-60177283./15000000.);
  Real rsd3PDE = (0)       + (0)            + ( 79922569./15000000.);
  Real rsd4PDE = (0)       + (0)            + (-121847./3000000.);

  // BC residuals:
  Real rsd1BC = -1093./200.;
  Real rsd2BC = -3789./1000.;

  // PDE/BC global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(4);
  SLA::SparseVector<ArrayQ> rsdBCGlobal(8);

  rsdPDEGlobal = 0;
  rsdBCGlobal = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_sansLG_DGBR2(fcn, rsdPDEGlobal, rsdBCGlobal),
                                                      xfld, qfld, lgfld, quadratureorder, 4 );

#if 0
  cout << "rsdPDEGlobal[0] = " << rsdPDEGlobal[0] << endl;
  cout << "rsdPDEGlobal[1] = " << rsdPDEGlobal[1] << endl;
  cout << "rsdPDEGlobal[2] = " << rsdPDEGlobal[2] << endl;
  cout << "rsdPDEGlobal[3] = " << rsdPDEGlobal[3] << endl;
  cout << "rsdBCGlobal[0] = " << rsdBCGlobal[0] << endl;
  cout << "rsdBCGlobal[1] = " << rsdBCGlobal[1] << endl;
#endif

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( rsd1PDE, rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2PDE, rsdPDEGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd3PDE, rsdPDEGlobal[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd4PDE, rsdPDEGlobal[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsd1BC, rsdBCGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2BC, rsdBCGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE(      0, rsdBCGlobal[2], small_tol, close_tol );
  SANS_CHECK_CLOSE(      0, rsdBCGlobal[3], small_tol, close_tol );
  SANS_CHECK_CLOSE(      0, rsdBCGlobal[4], small_tol, close_tol );
  SANS_CHECK_CLOSE(      0, rsdBCGlobal[5], small_tol, close_tol );
  SANS_CHECK_CLOSE(      0, rsdBCGlobal[6], small_tol, close_tol );
  SANS_CHECK_CLOSE(      0, rsdBCGlobal[7], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_3D_1Tet_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion<PhysD3, BCTypeLinearRobin_sansLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD3, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Real kxz = 0.643;
  Real kyz = 0.765;
  Real kzz = 1.234;
  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                                  kxy, kyy, kyz,
                                  kxz, kyz, kzz );

  Source3D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC

  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField3D_1Tet_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( 4, qfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld.nElem() );

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;
  qfld.DOF(3) = 6;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // Lagrange multiplier DOF data
  lgfld.DOF(0) =  7;
  lgfld.DOF(1) = -9;
  lgfld.DOF(2) =  5;

  // integrand
  IntegrandClass fcn( pde, bc, {0} );

  // quadrature rule
  int quadratureorder[4] = {2,2,2,2};

  // PDE residuals (left): (advective) + (viscous) + (Lagrange)
  Real rsd1PDE = (0)       + (0)            + ( 15191./2500. - 37042563*sqrt(3)/2500000.);
  Real rsd2PDE = (6./5.)   + (-8941./2000.) + ( -5999./6000. + 35789779*sqrt(3)/5000000.);
  Real rsd3PDE = (51./40.) + (-8941./2000.) + (-27309./10000. + 2818291/(sqrt(3)*200000.));
  Real rsd4PDE = (57./40.) + (-8941./2000.) + (-29501./30000. + 43605083/(sqrt(3)*2500000.) );

  // BC residuals:
  Real rsd1BC = 8941/1000. + 1./(2.*sqrt(3));
  Real rsd2BC = 8941/1000. + 5./(8.*sqrt(3));
  Real rsd3BC = 8941/1000. + 7./(8.*sqrt(3));

  // PDE/BC global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(4);
  SLA::SparseVector<ArrayQ> rsdBCGlobal(4*3);

  // topology-specific single group interface

  rsdPDEGlobal = 0;
  rsdBCGlobal = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD3>::integrate( ResidualBoundaryTrace_sansLG_DGBR2(fcn, rsdPDEGlobal, rsdBCGlobal),
                                                      xfld, qfld, lgfld, quadratureorder, 4 );

#if 0
  cout << "rsdPDEGlobal[0] = " << rsdPDEGlobal[0] << endl;
  cout << "rsdPDEGlobal[1] = " << rsdPDEGlobal[1] << endl;
  cout << "rsdPDEGlobal[2] = " << rsdPDEGlobal[2] << endl;
  cout << "rsdPDEGlobal[3] = " << rsdPDEGlobal[3] << endl;
  cout << "rsdBCGlobal[0] = " << rsdBCGlobal[0] << endl;
  cout << "rsdBCGlobal[1] = " << rsdBCGlobal[1] << endl;
  cout << "rsdBCGlobal[2] = " << rsdBCGlobal[2] << endl;
#endif

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( rsd1PDE, rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2PDE, rsdPDEGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd3PDE, rsdPDEGlobal[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd4PDE, rsdPDEGlobal[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsd1BC, rsdBCGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2BC, rsdBCGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd3BC, rsdBCGlobal[2], small_tol, close_tol );
  SANS_CHECK_CLOSE(      0, rsdBCGlobal[3], small_tol, close_tol );
  SANS_CHECK_CLOSE(      0, rsdBCGlobal[4], small_tol, close_tol );
  SANS_CHECK_CLOSE(      0, rsdBCGlobal[5], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_3D_1Hex_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion<PhysD3,BCTypeLinearRobin_sansLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD3, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Real kxz = 0.643;
  Real kyz = 0.765;
  Real kzz = 1.234;
  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                                  kxy, kyy, kyz,
                                  kxz, kyz, kzz );

  Source3D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC

  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField3D_1Hex_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( 8, qfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld.nElem() );

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;
  qfld.DOF(3) = 6;

  qfld.DOF(4) = 8;
  qfld.DOF(5) = 2;
  qfld.DOF(6) = 9;
  qfld.DOF(7) = 7;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // Lagrange multiplier DOF data
  lgfld.DOF(0) =  7;
  lgfld.DOF(1) = -9;
  lgfld.DOF(2) =  5;
  lgfld.DOF(3) =  3;

  // integrand
  IntegrandClass fcn( pde, bc, {0} );

  // quadrature rule
  int quadratureorder[6] = {2,2,2,2,2,2};

  // PDE residuals (left): (advective) + (viscous) + (Lagrange)
  Real rsd1PDE = (-13./36.) + (931./480.)   + (-78006083./45000000.);
  Real rsd2PDE = (-7./18.)  + (5093./4000.) + (12392939./45000000.);
  Real rsd3PDE = (-35./72.) + (607./480.)   + (9337397./9000000.);
  Real rsd4PDE = (-37./72.) + (3647./2400.) + (-20634641./9000000.);

  Real rsd5PDE = (0)        + (0)           + (415241./1800000.);
  Real rsd6PDE = (0)        + (0)           + (-20461571./45000000.);
  Real rsd7PDE = (0)        + (0)           + (1000157./1800000.);
  Real rsd8PDE = (0)        + (0)           + (21435197./9000000.);


  // BC residuals:
  Real rsd1BC = -2813./720.;
  Real rsd2BC = -9941./3600.;
  Real rsd3BC = -1661./720.;
  Real rsd4BC = -45337./18000.;

  // PDE/BC global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(8);
  SLA::SparseVector<ArrayQ> rsdBCGlobal(6*4);

  // topology-specific single group interface

  rsdPDEGlobal = 0;
  rsdBCGlobal = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD3>::integrate( ResidualBoundaryTrace_sansLG_DGBR2(fcn, rsdPDEGlobal, rsdBCGlobal),
                                                      xfld, qfld, lgfld, quadratureorder, 6 );

#if 0
  cout << "rsdPDEGlobal[0] = " << rsdPDEGlobal[0] << endl;
  cout << "rsdPDEGlobal[1] = " << rsdPDEGlobal[1] << endl;
  cout << "rsdPDEGlobal[2] = " << rsdPDEGlobal[2] << endl;
  cout << "rsdPDEGlobal[3] = " << rsdPDEGlobal[3] << endl;
  cout << "rsdBCGlobal[0] = " << rsdBCGlobal[0] << endl;
  cout << "rsdBCGlobal[1] = " << rsdBCGlobal[1] << endl;
  cout << "rsdBCGlobal[2] = " << rsdBCGlobal[2] << endl;
#endif

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( rsd1PDE, rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2PDE, rsdPDEGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd3PDE, rsdPDEGlobal[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd4PDE, rsdPDEGlobal[3], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd5PDE, rsdPDEGlobal[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd6PDE, rsdPDEGlobal[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd7PDE, rsdPDEGlobal[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd8PDE, rsdPDEGlobal[7], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsd1BC, rsdBCGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2BC, rsdBCGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd3BC, rsdBCGlobal[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd4BC, rsdBCGlobal[3], small_tol, close_tol );
  for (int n = 4; n < 6*4; n++)
    SANS_CHECK_CLOSE(      0, rsdBCGlobal[n], small_tol, close_tol );

}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
