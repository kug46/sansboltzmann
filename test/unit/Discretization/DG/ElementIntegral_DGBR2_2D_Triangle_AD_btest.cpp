// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementIntegral_DGBR2_2D_Triangle_AD_btest
// testing of 2-D cell element residual integrands for DG BR2: Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementLift.h"

#include "Discretization/DG/IntegrandCell_DGBR2.h"
#include "Discretization/DG/IntegrandInteriorTrace_DGBR2.h"

using namespace std;
using namespace SANS;

typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_None > PDEAdvectionDiffusion2D;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass2D;
typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldTriangle;
template class IntegrandCell_DGBR2<PDEClass2D>::BasisWeighted_PDE<Real,Real,TopoD2,Triangle,ElementXFieldTriangle>;
template class IntegrandInteriorTrace_DGBR2<PDEClass2D>::
               BasisWeighted_PDE<Real,Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementXFieldTriangle,ElementXFieldTriangle>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementIntegral_DGBR2_2D_Triangle_AD_test_suite )

//----------------------------------------------------------------------------//
// residual check for complete interior element: P0 w/ const in (x,y)
//
BOOST_AUTO_TEST_CASE( P0const_test )
{
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldLineClass;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldAreaClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldAreaClass;
  typedef ElementLift<VectorArrayQ,TopoD2,Triangle> ElementrFieldAreaClass;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementRFieldAreaClass;

  typedef ElementXFieldAreaClass ElementParam;

  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandLineClass;
  typedef IntegrandLineClass::BasisWeighted_PDE<Real,Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParam,ElementParam> BasisWeightedLineClass;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandAreaClass;
  typedef IntegrandAreaClass::BasisWeighted_PDE<Real,Real,TopoD2,Triangle,ElementParam> BasisWeightedAreaClass;

  // grid

  int order = 1;
  ElementXFieldAreaClass xarea1(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea2(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea3(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea4(order, BasisFunctionCategory_Hierarchical);

  ElementXFieldLineClass xedge1(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldLineClass xedge2(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldLineClass xedge3(order, BasisFunctionCategory_Hierarchical);

  // grid coordinates
  Real x1, x2, x3, x4, x5, x6;
  Real y1, y2, y3, y4, y5, y6;

  x1 =  0;  y1 =  0;
  x2 =  1;  y2 =  0;
  x3 =  0;  y3 =  1;
  x4 =  1;  y4 =  1;
  x5 = -1;  y5 =  1;
  x6 =  1;  y6 = -1;

  xarea1.DOF(0) = {x1, y1};
  xarea1.DOF(1) = {x2, y2};
  xarea1.DOF(2) = {x3, y3};

  xarea2.DOF(0) = {x4, y4};
  xarea2.DOF(1) = {x3, y3};
  xarea2.DOF(2) = {x2, y2};

  xarea3.DOF(0) = {x5, y5};
  xarea3.DOF(1) = {x1, y1};
  xarea3.DOF(2) = {x3, y3};

  xarea4.DOF(0) = {x6, y6};
  xarea4.DOF(1) = {x2, y2};
  xarea4.DOF(2) = {x1, y1};

  xedge1.DOF(0) = {x2, y2};
  xedge1.DOF(1) = {x3, y3};

  xedge2.DOF(0) = {x3, y3};
  xedge2.DOF(1) = {x1, y1};

  xedge3.DOF(0) = {x1, y1};
  xedge3.DOF(1) = {x2, y2};

  CanonicalTraceToCell traceR(0,-1);

  // solution

  order = 0;
  ElementQFieldAreaClass qarea1(order, BasisFunctionCategory_Legendre);
  ElementQFieldAreaClass qarea2(order, BasisFunctionCategory_Legendre);
  ElementQFieldAreaClass qarea3(order, BasisFunctionCategory_Legendre);
  ElementQFieldAreaClass qarea4(order, BasisFunctionCategory_Legendre);

  // solution DOFs: const @ a
  Real a =  2.347;

  qarea1.DOF(0) = a;
  qarea2.DOF(0) = a;
  qarea3.DOF(0) = a;
  qarea4.DOF(0) = a;

  // lifting operators
  // NOTE: since solution is smooth, lifting operators are all zero; so we're
  // cheating and reusing a single field variable

  order = 0;
  ElementrFieldAreaClass rareas(order, BasisFunctionCategory_Legendre);
  for (int i = 0; i < 3; i++)
    rareas[i].DOF(0) = 0;

  ElementRFieldAreaClass rareasum(order, BasisFunctionCategory_Legendre);
  rareasum.vectorViewDOF() = 0;

  // BR2 discretization (not used)
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // area quadrature rule (linear: basis grad is const, flux is const)
  int quadratureorderArea = 1;

  // line quadrature rule (linear: basis is linear, flux is const)
  int quadratureorderLine = 1;

  {
    // PDE 1: advection
    //
    // residual = 0

    Real u = 1.731;
    Real v = 0.287;
    AdvectiveFlux2D_Uniform adv(u, v);

    Real kxx = 0;
    Real kxy = 0;
    Real kyy = 0;
    ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

    Source2D_None source;

    PDEClass pde( adv, visc, source );

    // integrand functors
    IntegrandAreaClass fcnareaint( pde, {0}, {0} );
    IntegrandLineClass fcnlineint( pde, disc, {0} );

    BasisWeightedAreaClass fcnarea1 = fcnareaint.integrand_PDE( xarea1, qarea1, rareasum );
    BasisWeightedLineClass fcnline1 = fcnlineint.integrand_PDE( xedge1, CanonicalTraceToCell(0,1), traceR,
                                                                xarea1, qarea1, rareas[0], xarea2, qarea2, rareas[0] );
    BasisWeightedLineClass fcnline2 = fcnlineint.integrand_PDE( xedge2, CanonicalTraceToCell(1,1), traceR,
                                                                xarea1, qarea1, rareas[0], xarea3, qarea3, rareas[0] );
    BasisWeightedLineClass fcnline3 = fcnlineint.integrand_PDE( xedge3, CanonicalTraceToCell(2,1), traceR,
                                                                xarea1, qarea1, rareas[0], xarea4, qarea4, rareas[0] );

    ArrayQ rsd[1];
    ArrayQ rsdL[1];
    ArrayQ rsdR[1];

    int nIntegrand = 1;
    int nIntegrandL = 1;
    int nIntegrandR = 1;
    GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralArea(quadratureorderArea, nIntegrand);
    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ,
                                           ArrayQ> integralLine(quadratureorderLine, nIntegrandL, nIntegrandR);

    integralArea( fcnarea1, xarea1, rsd, nIntegrand );
    integralLine( fcnline1, xedge1, rsdL, nIntegrandL, rsdR, nIntegrandR ); rsd[0] += rsdL[0];
    integralLine( fcnline2, xedge2, rsdL, nIntegrandL, rsdR, nIntegrandR ); rsd[0] += rsdL[0];
    integralLine( fcnline3, xedge3, rsdL, nIntegrandL, rsdR, nIntegrandR ); rsd[0] += rsdL[0];

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;
    SANS_CHECK_CLOSE( 0, rsd[0], small_tol, close_tol );
  }

  {
    // PDE 2: diffusion
    //
    // residual = 0

    Real u = 0;
    Real v = 0;
    AdvectiveFlux2D_Uniform adv(u, v);

    Real kxx = 2.123;
    Real kxy = 0.553;
    Real kyy = 1.007;
    ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

    Source2D_None source;

    PDEClass pde( adv, visc, source );

    // integrand functors
    IntegrandAreaClass fcnareaint( pde, {0}, {0} );
    IntegrandLineClass fcnlineint( pde, disc, {0} );

    BasisWeightedAreaClass fcnarea1 = fcnareaint.integrand_PDE( xarea1, qarea1, rareasum );
    BasisWeightedLineClass fcnline1 = fcnlineint.integrand_PDE( xedge1, CanonicalTraceToCell(0,1), traceR,
                                                                   xarea1, qarea1, rareas[0], xarea2, qarea2, rareas[0] );
    BasisWeightedLineClass fcnline2 = fcnlineint.integrand_PDE( xedge2, CanonicalTraceToCell(1,1), traceR,
                                                                   xarea1, qarea1, rareas[0], xarea3, qarea3, rareas[0] );
    BasisWeightedLineClass fcnline3 = fcnlineint.integrand_PDE( xedge3, CanonicalTraceToCell(2,1), traceR,
                                                                   xarea1, qarea1, rareas[0], xarea4, qarea4, rareas[0] );

    ArrayQ rsd[1];
    ArrayQ rsdL[1];
    ArrayQ rsdR[1];

    int nIntegrand = 1;
    int nIntegrandL = 1;
    int nIntegrandR = 1;
    GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralArea(quadratureorderArea, nIntegrand);
    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ,
                                           ArrayQ> integralLine(quadratureorderLine, nIntegrandL, nIntegrandR);

    integralArea( fcnarea1, xarea1, rsd, nIntegrand );
    integralLine( fcnline1, xedge1, rsdL, nIntegrandL, rsdR, nIntegrandR ); rsd[0] += rsdL[0];
    integralLine( fcnline2, xedge2, rsdL, nIntegrandL, rsdR, nIntegrandR ); rsd[0] += rsdL[0];
    integralLine( fcnline3, xedge3, rsdL, nIntegrandL, rsdR, nIntegrandR ); rsd[0] += rsdL[0];

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;
    SANS_CHECK_CLOSE( 0, rsd[0], small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
// residual check for complete interior element: P1 w/ linear in (x,y)
//
BOOST_AUTO_TEST_CASE( P1linear_test )
{
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD2::D, ArrayQ> VectorArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldLineClass;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldAreaClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldAreaClass;
  typedef ElementLift<VectorArrayQ,TopoD2,Triangle> ElementrFieldAreaClass;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementRFieldAreaClass;

  typedef ElementXFieldAreaClass ElementParam;

  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandLineClass;
  typedef IntegrandLineClass::BasisWeighted_PDE<Real,Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParam,ElementParam> BasisWeightedLineClass;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandAreaClass;
  typedef IntegrandAreaClass::BasisWeighted_PDE<Real,Real,TopoD2,Triangle,ElementParam> BasisWeightedAreaClass;

  // grid

  int order = 1;
  ElementXFieldAreaClass xarea1(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea2(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea3(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea4(order, BasisFunctionCategory_Hierarchical);

  ElementXFieldLineClass xedge1(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldLineClass xedge2(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldLineClass xedge3(order, BasisFunctionCategory_Hierarchical);

  // grid coordinates
  Real x1, x2, x3, x4, x5, x6;
  Real y1, y2, y3, y4, y5, y6;

  x1 =  0;  y1 =  0;
  x2 =  1;  y2 =  0;
  x3 =  0;  y3 =  1;
  x4 =  1;  y4 =  1;
  x5 = -1;  y5 =  1;
  x6 =  1;  y6 = -1;

  xarea1.DOF(0) = {x1, y1};
  xarea1.DOF(1) = {x2, y2};
  xarea1.DOF(2) = {x3, y3};

  xarea2.DOF(0) = {x4, y4};
  xarea2.DOF(1) = {x3, y3};
  xarea2.DOF(2) = {x2, y2};

  xarea3.DOF(0) = {x5, y5};
  xarea3.DOF(1) = {x1, y1};
  xarea3.DOF(2) = {x3, y3};

  xarea4.DOF(0) = {x6, y6};
  xarea4.DOF(1) = {x2, y2};
  xarea4.DOF(2) = {x1, y1};

  xedge1.DOF(0) = {x2, y2};
  xedge1.DOF(1) = {x3, y3};

  xedge2.DOF(0) = {x3, y3};
  xedge2.DOF(1) = {x1, y1};

  xedge3.DOF(0) = {x1, y1};
  xedge3.DOF(1) = {x2, y2};

  CanonicalTraceToCell traceR(0,-1);

  // solution

  order = 1;
  ElementQFieldAreaClass qarea1(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldAreaClass qarea2(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldAreaClass qarea3(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldAreaClass qarea4(order, BasisFunctionCategory_Hierarchical);

  // solution DOFs: linear @ a*x + b*y
  Real a =  2.347;
  Real b = -0.993;

  qarea1.DOF(0) = a*x1 + b*y1;
  qarea1.DOF(1) = a*x2 + b*y2;
  qarea1.DOF(2) = a*x3 + b*y3;

  qarea2.DOF(0) = a*x4 + b*y4;
  qarea2.DOF(1) = a*x3 + b*y3;
  qarea2.DOF(2) = a*x2 + b*y2;

  qarea3.DOF(0) = a*x5 + b*y5;
  qarea3.DOF(1) = a*x1 + b*y1;
  qarea3.DOF(2) = a*x3 + b*y3;

  qarea4.DOF(0) = a*x6 + b*y6;
  qarea4.DOF(1) = a*x2 + b*y2;
  qarea4.DOF(2) = a*x1 + b*y1;

  // lifting operators
  // NOTE: since solution is smooth, lifting operators are all zero; so we're
  // cheating and reusing a single field variable

  order = 1;
  ElementrFieldAreaClass rareas(order, BasisFunctionCategory_Hierarchical);
  for (int i = 0; i < 3; i++)
  {
    rareas[i].DOF(0) = 0;
    rareas[i].DOF(1) = 0;
    rareas[i].DOF(2) = 0;
  }

  ElementRFieldAreaClass rareasum(order, BasisFunctionCategory_Hierarchical);
  rareasum.vectorViewDOF() = 0;

  // BR2 discretization (not used)
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // area quadrature rule (linear: basis grad is const, flux is linear)
  int quadratureorderArea = 1;

  // line quadrature rule (quadratic: basis & flux both linear)
  int quadratureorderLine = 2;

  Real area1 = 0.5;

  {
    // PDE 1: advection
    //
    // residual = (1/3)*area*(u*a + v*b) * {1,1,1}

    Real u = 1.731;
    Real v = 0.287;
    AdvectiveFlux2D_Uniform adv(u, v);

    Real kxx = 0;
    Real kxy = 0;
    Real kyy = 0;
    ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

    Source2D_None source;

    PDEClass pde( adv, visc, source );

    // integrand functors
    IntegrandAreaClass fcnareaint( pde, {0}, {0} );
    IntegrandLineClass fcnlineint( pde, disc, {0} );

    BasisWeightedAreaClass fcnarea1 = fcnareaint.integrand_PDE( xarea1, qarea1, rareasum );
    BasisWeightedLineClass fcnline1 = fcnlineint.integrand_PDE( xedge1, CanonicalTraceToCell(0,1), traceR,
                                                                xarea1, qarea1, rareas[0], xarea2, qarea2, rareas[0] );
    BasisWeightedLineClass fcnline2 = fcnlineint.integrand_PDE( xedge2, CanonicalTraceToCell(1,1), traceR,
                                                                xarea1, qarea1, rareas[0], xarea3, qarea3, rareas[0] );
    BasisWeightedLineClass fcnline3 = fcnlineint.integrand_PDE( xedge3, CanonicalTraceToCell(2,1), traceR,
                                                                xarea1, qarea1, rareas[0], xarea4, qarea4, rareas[0] );

    ArrayQ rsd[3];
    ArrayQ rsdL[3];
    ArrayQ rsdR[3];

    int nIntegrand = 3;
    int nIntegrandL = 3;
    int nIntegrandR = 3;

    GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralArea(quadratureorderArea, nIntegrand);
    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ,
                                           ArrayQ> integralLine(quadratureorderLine, nIntegrandL, nIntegrandR);

    integralArea( fcnarea1, xarea1, rsd, nIntegrand );
    integralLine( fcnline1, xedge1, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
    integralLine( fcnline2, xedge2, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
    integralLine( fcnline3, xedge3, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;
    Real rsdTrue = (1./3.)*area1*(u*a + v*b);
    SANS_CHECK_CLOSE( rsdTrue, rsd[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsdTrue, rsd[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsdTrue, rsd[2], small_tol, close_tol );
  }

  {
    // PDE 2: diffusion
    //
    // residual = 0

    Real u = 0;
    Real v = 0;
    AdvectiveFlux2D_Uniform adv(u, v);

    Real kxx = 2.123;
    Real kxy = 0.553;
    Real kyy = 1.007;
    ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

    Source2D_None source;

    PDEClass pde( adv, visc, source );

    // integrand functors
    IntegrandAreaClass fcnareaint( pde, {0}, {0} );
    IntegrandLineClass fcnlineint( pde, disc, {0} );

    BasisWeightedAreaClass fcnarea1 = fcnareaint.integrand_PDE( xarea1, qarea1, rareasum );
    BasisWeightedLineClass fcnline1 = fcnlineint.integrand_PDE( xedge1, CanonicalTraceToCell(0,1), traceR,
                                                                xarea1, qarea1, rareas[0], xarea2, qarea2, rareas[0] );
    BasisWeightedLineClass fcnline2 = fcnlineint.integrand_PDE( xedge2, CanonicalTraceToCell(1,1), traceR,
                                                                xarea1, qarea1, rareas[0], xarea3, qarea3, rareas[0] );
    BasisWeightedLineClass fcnline3 = fcnlineint.integrand_PDE( xedge3, CanonicalTraceToCell(2,1), traceR,
                                                                xarea1, qarea1, rareas[0], xarea4, qarea4, rareas[0] );

    ArrayQ rsd[3];
    ArrayQ rsdL[3];
    ArrayQ rsdR[3];

    int nIntegrand = 3;
    int nIntegrandL = 3;
    int nIntegrandR = 3;

    GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralArea(quadratureorderArea, nIntegrand);
    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ,
                                           ArrayQ> integralLine(quadratureorderLine, nIntegrandL, nIntegrandR);

    integralArea( fcnarea1, xarea1, rsd, nIntegrand );
    integralLine( fcnline1, xedge1, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
    integralLine( fcnline2, xedge2, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
    integralLine( fcnline3, xedge3, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;
    SANS_CHECK_CLOSE( 0, rsd[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( 0, rsd[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( 0, rsd[2], small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
// residual check for complete interior element: P1 w/ quadratic in (x,y)
//
BOOST_AUTO_TEST_CASE( P1quadratic_test )
{
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD2::D, ArrayQ> VectorArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldLineClass;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldAreaClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldAreaClass;
  typedef ElementLift<VectorArrayQ,TopoD2,Triangle> ElementrFieldAreaClass;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementRFieldAreaClass;

  typedef ElementXFieldAreaClass ElementParam;

  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandLineClass;
  typedef IntegrandLineClass::BasisWeighted_PDE<Real,Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParam,ElementParam> BasisWeightedLineClass;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandAreaClass;
  typedef IntegrandAreaClass::BasisWeighted_PDE<Real,Real,TopoD2,Triangle,ElementParam> BasisWeightedAreaClass;

  // grid

  int order = 1;
  ElementXFieldAreaClass xarea1(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea2(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea3(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea4(order, BasisFunctionCategory_Hierarchical);

  ElementXFieldLineClass xedge1(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldLineClass xedge2(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldLineClass xedge3(order, BasisFunctionCategory_Hierarchical);

  // grid coordinates
  Real x1, x2, x3, x4, x5, x6;
  Real y1, y2, y3, y4, y5, y6;

  x1 =  0;  y1 =  0;
  x2 =  1;  y2 =  0;
  x3 =  0;  y3 =  1;
  x4 =  1;  y4 =  1;
  x5 = -1;  y5 =  1;
  x6 =  1;  y6 = -1;

  xarea1.DOF(0) = {x1, y1};
  xarea1.DOF(1) = {x2, y2};
  xarea1.DOF(2) = {x3, y3};

  xarea2.DOF(0) = {x4, y4};
  xarea2.DOF(1) = {x3, y3};
  xarea2.DOF(2) = {x2, y2};

  xarea3.DOF(0) = {x5, y5};
  xarea3.DOF(1) = {x1, y1};
  xarea3.DOF(2) = {x3, y3};

  xarea4.DOF(0) = {x6, y6};
  xarea4.DOF(1) = {x2, y2};
  xarea4.DOF(2) = {x1, y1};

  xedge1.DOF(0) = {x2, y2};
  xedge1.DOF(1) = {x3, y3};

  xedge2.DOF(0) = {x3, y3};
  xedge2.DOF(1) = {x1, y1};

  xedge3.DOF(0) = {x1, y1};
  xedge3.DOF(1) = {x2, y2};

  CanonicalTraceToCell traceR(0,-1);

  // solution

  order = 1;
  ElementQFieldAreaClass qarea1(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldAreaClass qarea2(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldAreaClass qarea3(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldAreaClass qarea4(order, BasisFunctionCategory_Hierarchical);

  // solution DOFs: quadratic @ a*x^2 + b*y^2
  Real a =  2.347;
  Real b = -0.993;

  qarea1.DOF(0) = a*x1*x1 + b*y1*y1;
  qarea1.DOF(1) = a*x2*x2 + b*y2*y2;
  qarea1.DOF(2) = a*x3*x3 + b*y3*y3;

  qarea2.DOF(0) = a*x4*x4 + b*y4*y4;
  qarea2.DOF(1) = a*x3*x3 + b*y3*y3;
  qarea2.DOF(2) = a*x2*x2 + b*y2*y2;

  qarea3.DOF(0) = a*x5*x5 + b*y5*y5;
  qarea3.DOF(1) = a*x1*x1 + b*y1*y1;
  qarea3.DOF(2) = a*x3*x3 + b*y3*y3;

  qarea4.DOF(0) = a*x6*x6 + b*y6*y6;
  qarea4.DOF(1) = a*x2*x2 + b*y2*y2;
  qarea4.DOF(2) = a*x1*x1 + b*y1*y1;

  // lifting operators
  // NOTE: since solution is smooth, lifting operators are all zero; so we're
  // cheating and reusing a single field variable

  order = 1;
  ElementrFieldAreaClass rareas(order, BasisFunctionCategory_Hierarchical);
  for (int i = 0; i < 3; i++)
  {
    rareas[i].DOF(0) = 0;
    rareas[i].DOF(1) = 0;
    rareas[i].DOF(2) = 0;
  }

  ElementRFieldAreaClass rareasum(order, BasisFunctionCategory_Hierarchical);
  rareasum.vectorViewDOF() = 0;

  // BR2 discretization (not used)
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // area quadrature rule (linear: basis grad is const, flux is linear)
  int quadratureorderArea = 1;

  // line quadrature rule (quadratic: basis & flux both linear)
  int quadratureorderLine = 2;

  Real area1 = 0.5;

  {
    // PDE 1: advection
    //
    // residual = (1/3)*area*(u*a + v*b) * {1,1,1}

    Real u = 1.731;
    Real v = 0.287;
    AdvectiveFlux2D_Uniform adv(u, v);

    Real kxx = 0;
    Real kxy = 0;
    Real kyy = 0;
    ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

    Source2D_None source;

    PDEClass pde( adv, visc, source );

    // integrand functors
    IntegrandAreaClass fcnareaint( pde, {0}, {0} );
    IntegrandLineClass fcnlineint( pde, disc, {0} );

    BasisWeightedAreaClass fcnarea1 = fcnareaint.integrand_PDE( xarea1, qarea1, rareasum );
    BasisWeightedLineClass fcnline1 = fcnlineint.integrand_PDE( xedge1, CanonicalTraceToCell(0,1), traceR,
                                                                xarea1, qarea1, rareas[0], xarea2, qarea2, rareas[0] );
    BasisWeightedLineClass fcnline2 = fcnlineint.integrand_PDE( xedge2, CanonicalTraceToCell(1,1), traceR,
                                                                xarea1, qarea1, rareas[0], xarea3, qarea3, rareas[0] );
    BasisWeightedLineClass fcnline3 = fcnlineint.integrand_PDE( xedge3, CanonicalTraceToCell(2,1), traceR,
                                                                xarea1, qarea1, rareas[0], xarea4, qarea4, rareas[0] );

    ArrayQ rsd[3];
    ArrayQ rsdL[3];
    ArrayQ rsdR[3];

    int nIntegrand = 3;
    int nIntegrandL = 3;
    int nIntegrandR = 3;

    GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralArea(quadratureorderArea, nIntegrand);
    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ,
                                           ArrayQ> integralLine(quadratureorderLine, nIntegrandL, nIntegrandR);

    integralArea( fcnarea1, xarea1, rsd, nIntegrand );
    integralLine( fcnline1, xedge1, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
    integralLine( fcnline2, xedge2, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
    integralLine( fcnline3, xedge3, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;
    Real rsdTrue = (1./3.)*area1*(u*a + v*b);
    SANS_CHECK_CLOSE( rsdTrue, rsd[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsdTrue, rsd[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsdTrue, rsd[2], small_tol, close_tol );
  }

  {
    // PDE 2: diffusion
    //
    // residual = -area*{ (a*kxx + b*kyy), b*kyy, a*kxx }

    Real u = 0;
    Real v = 0;
    AdvectiveFlux2D_Uniform adv(u, v);

    Real kxx = 2.123;
    Real kxy = 0.553;
    Real kyy = 1.007;
    ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

    Source2D_None source;

    PDEClass pde( adv, visc, source );

    // integrand functors
    IntegrandAreaClass fcnareaint( pde, {0}, {0} );
    IntegrandLineClass fcnlineint( pde, disc, {0} );

    BasisWeightedAreaClass fcnarea1 = fcnareaint.integrand_PDE( xarea1, qarea1, rareasum );
    BasisWeightedLineClass fcnline1 = fcnlineint.integrand_PDE( xedge1, CanonicalTraceToCell(0,1), traceR,
                                                                xarea1, qarea1, rareas[0], xarea2, qarea2, rareas[0] );
    BasisWeightedLineClass fcnline2 = fcnlineint.integrand_PDE( xedge2, CanonicalTraceToCell(1,1), traceR,
                                                                xarea1, qarea1, rareas[0], xarea3, qarea3, rareas[0] );
    BasisWeightedLineClass fcnline3 = fcnlineint.integrand_PDE( xedge3, CanonicalTraceToCell(2,1), traceR,
                                                                xarea1, qarea1, rareas[0], xarea4, qarea4, rareas[0] );

    ArrayQ rsd[3];
    ArrayQ rsdL[3];
    ArrayQ rsdR[3];

    int nIntegrand = 3;
    int nIntegrandL = 3;
    int nIntegrandR = 3;

    GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralArea(quadratureorderArea, nIntegrand);
    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ,
                                           ArrayQ> integralLine(quadratureorderLine, nIntegrandL, nIntegrandR);

    integralArea( fcnarea1, xarea1, rsd, nIntegrand );
    integralLine( fcnline1, xedge1, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
    integralLine( fcnline2, xedge2, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
    integralLine( fcnline3, xedge3, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;
    Real rsdTrue[3] = { -area1*(kxx*a + kyy*b), -area1*(kyy*b), -area1*(kxx*a) };
    SANS_CHECK_CLOSE( rsdTrue[0], rsd[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsdTrue[1], rsd[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsdTrue[2], rsd[2], small_tol, close_tol );
  }
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
