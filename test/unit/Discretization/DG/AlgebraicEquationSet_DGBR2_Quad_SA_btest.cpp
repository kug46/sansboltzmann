// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AlgebraicEquationSet_DGBR2_Quad_SA_btest
// testing AlgebraicEquationSet_DGBR2

//#define DISPLAY_FOR_DEBUGGING

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"


#include "tools/SANSnumerics.h"     // Real

#include "Surreal/SurrealS.h"

#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/QRANSSA2D.h"
#include "pde/NS/PDERANSSA2D.h"
#include "pde/NS/BCRANSSA2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/DistanceFunction/DistanceFunction.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Quad_X1.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( AlgebraicEquationSet_DGBR2_Quad_SA_test_suite )

typedef QTypePrimitiveRhoPressure QType;
typedef ViscosityModel_Const ViscosityModelType;
typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

typedef BCRANSSA2DVector< TraitsSizeRANSSA, TraitsModelRANSSAClass > BCVector;

typedef FieldTuple<Field<PhysD2, TopoD2, Real>, XField<PhysD2, TopoD2>, TupleClass<>> ParamFieldTupleType;

typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Dense, DGBR2, ParamFieldTupleType> PrimalEquationSetClass;
typedef PrimalEquationSetClass::BCParams BCParams;

typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
typedef NDPDEClass::template MatrixQ<Real> MatrixQ;
typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

// Size of the state vector
const int N = ArrayQ::M;

typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

void pingTest(const std::vector<ArrayQ>& step, const std::vector<Real>& rate_range,
              bool verbose, const Real nonzero_tol, const Real small_tol,
              PrimalEquationSetClass& PrimalEqSet, const NDPDEClass& pde, SystemMatrixClass& jac)
{
  SystemVectorClass q(PrimalEqSet.vectorStateSize());
  SystemVectorClass rsdp(q.size()), rsdm(q.size());

  PrimalEqSet.fillSystemVector(q);

  BOOST_REQUIRE( PrimalEqSet.isValidStateSystemVector(q) );

#if __clang_analyzer__
  return; // clang thinks we are accessing zero size memory below... sigh...
#endif

  // compute the jacobian with Surreals
  jac = 0;
  PrimalEqSet.jacobian(q, jac);

  for ( int ii = 0; ii < jac.m(); ii++) // sub-block row
  {
    for (int jj = 0; jj < jac.n(); jj++) // sub-block column
    {
      // Skip jacobians that are known to be zero
      if ( (ii == PrimalEquationSetClass::iBC && jj == PrimalEquationSetClass::ilg) ) continue;
      if ( jac(ii,jj).m() == 0 || jac(ii,jj).n() == 0) continue;

      if (verbose)
      {
        std::cout << std::endl << "Sub-block jac_block("<<ii<<","<<jj<<")" << ", size = [" << jac(ii,jj).m() << "," << jac(ii,jj).n() <<
            "] of MatrixQ of size = ["<<N<<","<<N<<"] -*-*-*-*-*- " << std::endl << std::endl;
      }

      for ( int i = 0; i < jac(ii,jj).m(); i++ )
      {
        if (verbose)
          std::cout << "- Row i: " << i;

        std::vector<MatrixQ> Ajac;
        std::vector<int> iAjac;

        for ( int j = 0; j < jac(ii,jj).n(); j++)
        {
          bool anyNonZero = false;
          for (int m = 0; m < N; m++)
            for (int n = 0; n < N; n++)
              if ( fabs( jac(ii,jj)(i,j)(m,n) ) > nonzero_tol) anyNonZero = true;

          if ( anyNonZero )
          {
            // Save the non-zero value and column index
            Ajac.push_back(jac(ii,jj)(i,j));
            iAjac.push_back(j);
          }
        }

        if (verbose)
          std::cout << ", # of number of nonzeros = " << Ajac.size() << std::endl;

        std::vector<MatrixQ> diffJac;
        std::vector<int> idiffJac;

        // Compute jacobian using finite difference: central differencing here
        std::vector<std::vector<MatrixQ>> diffjac(step.size());
        std::vector<std::vector<int>> idiffjac(step.size());

        MatrixQ diff = 0;
        for (std::size_t i_fd_step = 0; i_fd_step < step.size(); i_fd_step++) // finite-difference step sizes
        {
          for ( int j = 0; j < q[jj].m(); j++)
          {
            for (int n = 0; n < N; n++) // entry in ArrayQ
            {
              rsdp = 0;
              q[jj][j][n] += step[i_fd_step][n];
              PrimalEqSet.residual(q, rsdp);

              rsdm = 0;
              q[jj][j][n] -= 2*step[i_fd_step][n];
              PrimalEqSet.residual(q, rsdm);

              q[jj][j][n] += step[i_fd_step][n]; // reset to input solution

              for (int m = 0; m < N; m++) // row in MatrixQ
                DLA::index(diff,m,n) = ( rsdp[ii][i][m] - rsdm[ii][i][m] )/(2*step[i_fd_step][n]);
            }

            // pick out MatrixQ with nonzero entries
            bool anyNonZero = false;
            for (int m = 0; m < N; m++)
              for (int n = 0; n < N; n++)
                if ( fabs(DLA::index(diff,m,n)) > nonzero_tol ) anyNonZero = true;

            if ( anyNonZero )
            {
              diffjac[i_fd_step].push_back(diff);
              idiffjac[i_fd_step].push_back(j);
            }
          } //j loop
        } //i_fd_step loop


        if (verbose)
          if ( Ajac.size() != diffjac[0].size() || Ajac.size() != diffjac[1].size())
          {
            std::cout << "Ajac = ";
            for (std::size_t j = 0; j < Ajac.size(); ++j)
              std::cout << Ajac[j] << ", ";
            std::cout << std::endl;

            std::cout << "diffjac[0] = ";
            for (std::size_t j = 0; j < diffjac[0].size(); ++j)
              std::cout << diffjac[0][j] << ", ";
            std::cout << std::endl;

          }

        BOOST_REQUIRE_EQUAL(Ajac.size(), diffjac[0].size());
        BOOST_REQUIRE_EQUAL(Ajac.size(), diffjac[1].size());

        for (std::size_t j = 0; j < diffjac[0].size(); ++j)
        {
          // print out nonzero entries
          if (verbose)
          {
            std::cout << "--- nonzero j=" << j << ": " <<
                "col(true,diff1,diff2) = (" << iAjac[j] << ", " << idiffjac[0][j] << ", " << idiffjac[1][j] << "), " <<
                "values : " << std::endl <<
            "surreal = " << std::endl << Ajac[j] << std::endl <<
            "diff1   = " << std::endl << diffjac[0][j] << std::endl <<
            "diff2   = " << std::endl << diffjac[1][j] << std::endl;
          }

          //Check if the col indices match up
          BOOST_CHECK_EQUAL(iAjac[j], idiffjac[0][j]);
          BOOST_CHECK_EQUAL(iAjac[j], idiffjac[1][j]);

          for (int m = 0; m < N; m++)
          {
            for (int n = 0; n < N; n++)
            {
              Real err_vec[2] = { fabs( Ajac[j](m,n) - diffjac[0][j](m,n) ),
                                  fabs( Ajac[j](m,n) - diffjac[1][j](m,n) )};

              if (verbose)
              {
                std::cout << "----- i=" << i << ", j=" << j << ", col=" << idiffjac[0][j] << ": m=" << m << ", n=" << n << " - " <<
                    "errors = {" << err_vec[0] << "," << err_vec[1] << "}";
              }

              // Error in finite-difference jacobian is either zero as for linear or quadratic residuals,
              // or is nonzero for general nonlinear residual where we need to check error convergence rate
              if (err_vec[0] > small_tol && err_vec[1] > small_tol)
              {
                Real rate = log(err_vec[1]/err_vec[0])/log(step[1][n]/step[0][n]);

                if (verbose) std::cout << ", rate = " << rate << std::endl;

                BOOST_CHECK_MESSAGE( rate >= rate_range[0] && rate <= rate_range[1],
                                     "Rate check failed at col = " << idiffjac[0][j] << ": m = " << m <<
                                     " n = " << n << ": rate = " << rate <<
                                     ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" );
              }
              else
              {
                if (verbose) std::cout << std::endl;
              }
            }
          }
        }
      } // i loop
    } // jj loop
  } // ii loop
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DenseSystem )
{

  // PDE

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.2;
  const Real Reynolds = 1e4;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale

  const Real qRef = 1;                              // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);
  const Real cRef = qRef/Mach;                      // speed of sound
  const Real tRef = (cRef*cRef)/(gamma*R);          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity
  const Real chiRef = 3;
  const Real ntRef = chiRef*muRef/rhoRef;           // SA solution

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // BC
  const Real HRef = Cp*tRef + 0.5*(uRef*uRef + vRef*vRef);
  const Real sRef = log( pRef / pow(rhoRef,gamma) );
  const Real aSpec = atan(vRef/uRef);

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] = sRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = HRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.aSpec] = aSpec;
  BCIn["nt"] = ntRef; // TODO: not happy about this

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = pRef;

  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCNoSlip"] = BCNoSlip;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {XField2D_Box_Quad_X1::iTop};
  BCBoundaryGroups["BCNoSlip"] = {XField2D_Box_Quad_X1::iBottom};
  BCBoundaryGroups["BCOut"] = {XField2D_Box_Quad_X1::iRight};
  BCBoundaryGroups["BCIn"] = {XField2D_Box_Quad_X1::iLeft};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // grid: HierarchicalP1 (aka X1)

  int ii = 1;
  int jj = 1;

  XField2D_Box_Quad_X1 xfld( ii, jj );

  Field_CG_Cell<PhysD2, TopoD2, Real> distfld(xfld, 1, BasisFunctionCategory_Lagrange);

  ArrayQ q0;
  pde.setDOFFrom( q0, SAnt2D<DensityVelocityPressure2D<Real>>({rhoRef, uRef, vRef, pRef, ntRef}) );

  //Compute distance field
  DistanceFunction(distfld, BCBoundaryGroups.at("BCNoSlip"));

  // solution: P1 (aka Q1)

  int order = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = q0;

  // lifting operator

  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Legendre);
  rfld = 0;

  // Lagrange multiplier: Legendre P1

#if 0
  order = 0;
  QField2D_DG_BoundaryEdge<PDEClass> lgfld( xfld, order, BasisFunctionCategory_Legendre );
#elif 1
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre,
                                                        BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#else
  order = 1;
  QField2D_CG_BoundaryEdge_Independent<PDEClass> lgfld( xfld, order );
#endif

  lgfld = 0;

  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  ParamFieldTupleType paramfld = (distfld, xfld);

  QuadratureOrder quadratureOrder( xfld, 1 );
  std::vector<Real> tol = {1e-12, 1e-12};
  PrimalEquationSetClass PrimalEqSet(paramfld, qfld, rfld, lgfld, pde, disc,
                                     quadratureOrder, ResidualNorm_Default, tol,
                                     {0}, {}, PyBCList, BCBoundaryGroups);

  // jacobian nonzero pattern

  SystemNonZeroPattern nz(PrimalEqSet.matrixSize());

  PrimalEqSet.jacobian(nz);

  // jacobian

  SystemMatrixClass jac(nz);
  jac = 0;

  Real nonzero_tol = 1e-12;
  Real small_tol = 5e-9;

  std::vector<ArrayQ> step = { 1e-1, 1e-2 };
  // Use a smaller step size for the SA variable
  step[0][N-1] = 1e-5;
  step[1][N-1] = 1e-6;
#ifdef __INTEL_COMPILER
  std::vector<Real> rate_range = {1.65, 2.5};
#else
  std::vector<Real> rate_range = {1.8, 2.5};
#endif
  bool verbose = false;

  //-------------------------------------------------------------------------//
  // case 0: freestream
  qfld = pde.setDOFFrom( SAnt2D<DensityVelocityPressure2D<Real>>({rhoRef, uRef, vRef, pRef, ntRef}) );;

  // This avoids pinging about 0 == fabs(uy - vx)
  qfld.DOF(2)[1] += 0.01;
  qfld.DOF(2)[2] += 0.01;
#if 1
  verbose = false;
  pingTest(step, rate_range,
           verbose, nonzero_tol, small_tol,
           PrimalEqSet, pde, jac);
#endif

  Real rho = 1.225;
  Real u = 6.974;
  Real v = -3.231;
  Real t = 2.8815;

  Real rhox = 0.0421, rhoy = 0.0127;
  Real ux = 0.0521, uy = -0.0124;
  Real vx = -0.0312, vy = -0.0022;
  Real tx = 0.0715, ty = 0.0111;

  Real px = R*(rho*tx + rhox*t);
  Real py = R*(rho*ty + rhoy*t);

  Real nt, ntx, nty;
  Real chi, chix, chiy;
  Real dist;

  //-------------------------------------------------------------------------//
  // case 1: prod/wall dominant; S~ branch 1
  if (verbose) cout << "source: case1" << endl;

  chi  = 1.241;
  chix = 0; chiy = 0;
  dist = 60;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  ArrayQ qx = {rhox, ux, vx, px, ntx};
  ArrayQ qy = {rhoy, uy, vy, py, nty};

  qfld = pde.setDOFFrom( SAnt2D<DensityVelocityTemperature2D<Real>>({rho, u, v, t, nt}) );;

  qfld.DOF(1) = qx;
  qfld.DOF(2) = qy;

  {
    for (int i = 0; i < xfld.nDOF(); i++)
      xfld.DOF(i) *= dist;

    //Compute distance field
    DistanceFunction(distfld, BCBoundaryGroups.at("BCNoSlip"));

    // the modified grid means a new inverse mass matrix field needs to updated
    PrimalEquationSetClass PrimalEqSet(paramfld, qfld, rfld, lgfld, pde, disc,
                                       quadratureOrder, ResidualNorm_Default, tol,
                                       {0}, {}, PyBCList, BCBoundaryGroups);

    step = { 1e-3, 5e-4 };
    step[0][N-1] = 1e-5;
    step[1][N-1] = 1e-6;
    small_tol = 5e-8;
    verbose = false;
    pingTest(step, rate_range,
             verbose, nonzero_tol, small_tol,
             PrimalEqSet, pde, jac);
  }

  for (int i = 0; i < xfld.nDOF(); i++)
    xfld.DOF(i) /= dist;


  //-------------------------------------------------------------------------//
  // case 2: prod/wall dominant; S~ branch 2
  if (verbose) cout << "source: case2" << endl;

  chi  = 5.241;
  chix = 0; chiy = 0;
  dist = 40;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qx = {rhox, ux, vx, px, ntx};
  qy = {rhoy, uy, vy, py, nty};

  qfld = pde.setDOFFrom( SAnt2D<DensityVelocityTemperature2D<Real>>({rho, u, v, t, nt}) );;

  qfld.DOF(1) = qx;
  qfld.DOF(2) = qy;

  {
    for (int i = 0; i < xfld.nDOF(); i++)
      xfld.DOF(i) *= dist;

    //Compute distance field
    DistanceFunction(distfld, BCBoundaryGroups.at("BCNoSlip"));

    // the modified grid means a new inverse mass matrix field needs to updated
    PrimalEquationSetClass PrimalEqSet(paramfld, qfld, rfld, lgfld, pde, disc,
                                       quadratureOrder, ResidualNorm_Default, tol,
                                       {0}, {}, PyBCList, BCBoundaryGroups);

    step = { 1e-3, 5e-4 };
    step[0][N-1] = 1e-5;
    step[1][N-1] = 5e-6;
    small_tol = 5e-8;
    verbose = false;
    pingTest(step, rate_range,
             verbose, nonzero_tol, small_tol,
             PrimalEqSet, pde, jac);
  }

  for (int i = 0; i < xfld.nDOF(); i++)
    xfld.DOF(i) /= dist;

  //-------------------------------------------------------------------------//
  // case 5: negative SA; prod/wall dominant
  if (verbose) cout << "source: case3" << endl;

  chi  = -5.241;
  chix = 0; chiy = 0;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qx = {rhox, ux, vx, px, ntx};
  qy = {rhoy, uy, vy, py, nty};

  qfld = pde.setDOFFrom( SAnt2D<DensityVelocityTemperature2D<Real>>({rho, u, v, t, nt}) );;

  qfld.DOF(1) = qx;
  qfld.DOF(2) = qy;

  {
    for (int i = 0; i < xfld.nDOF(); i++)
      xfld.DOF(i) *= dist;

    //Compute distance field
    DistanceFunction(distfld, BCBoundaryGroups.at("BCNoSlip"));

    // the modified grid means a new inverse mass matrix field needs to updated
    PrimalEquationSetClass PrimalEqSet(paramfld, qfld, rfld, lgfld, pde, disc,
                                       quadratureOrder, ResidualNorm_Default, tol,
                                       {0}, {}, PyBCList, BCBoundaryGroups);

    step = { 1e-2, 5e-3 };
    step[0][N-1] = 1e-4;
    step[1][N-1] = 5e-5;
    small_tol = 5e-8;
    rate_range = {1.7, 4.5};
    verbose = false;
    pingTest(step, rate_range,
             verbose, nonzero_tol, small_tol,
             PrimalEqSet, pde, jac);
  }

  for (int i = 0; i < xfld.nDOF(); i++)
    xfld.DOF(i) /= dist;

#if 0
  //-------------------------------------------------------------------------//
  // case 6: negative SA; nonconservative diffusion dominant
  if (verbose) cout << "source: case3" << endl;

  chi  = -5.241;
  chix = 1.963*0; chiy = -0.781*0;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qx = {rhox, ux, vx, px, ntx};
  qy = {rhoy, uy, vy, py, nty};

  qfld = pde.setDOFFrom( SAnt2D<DensityVelocityTemperature2D<Real>>({rho, u, v, t, nt}) );;

  qfld.DOF(1) = qx*dist;
  qfld.DOF(2) = qy*dist;

  {
    for (int i = 0; i < xfld.nDOF(); i++)
      xfld.DOF(i) *= dist;

    //Compute distance field
    DistanceFunction(distfld, BCBoundaryGroups.at("BCNoSlip"));
  }

  step = { 1e-2, 5e-3 };
  step[0][N-1] = 1e-4;
  step[1][N-1] = 5e-5;
  small_tol = 5e-8;
  rate_range = {1.7, 4.0};
  verbose = true;
  pingTest(step, rate_range,
           verbose, nonzero_tol, small_tol,
           PrimalEqSet, pde, jac);

  for (int i = 0; i < xfld.nDOF(); i++)
    xfld.DOF(i) /= dist;
#endif
#if 0
  //-------------------------------------------------------------------------//
  // case 7: nt == 0
  if (verbose) cout << "source: case3" << endl;

  chi  = 0;
  chix = 1.963; chiy = -0.781;
  dist = 100;

  nt  = chi*(muRef/rho);
  ntx = chix*(muRef/rho);
  nty = chiy*(muRef/rho);

  qx = {rhox, ux, vx, px, ntx};
  qy = {rhoy, uy, vy, py, nty};

  qfld = pde.setDOFFrom( SAnt2D<DensityVelocityTemperature2D<Real>>({rho, u, v, t, nt}) );;

  qfld.DOF(1) = qx*dist;
  qfld.DOF(2) = qy*dist;

  {
    for (int i = 0; i < xfld.nDOF(); i++)
      xfld.DOF(i) *= dist;

    //Compute distance field
    DistanceFunction(distfld, BCBoundaryGroups.at("BCNoSlip"));
  }

  step = { 1e-2, 5e-3 };
  step[0][N-1] = 1e-4;
  step[1][N-1] = 5e-5;
  small_tol = 5e-8;
  rate_range = {1.7, 4.5};
  verbose = true;
  pingTest(step, rate_range,
           verbose, nonzero_tol, small_tol,
           PrimalEqSet, pde, jac);

  for (int i = 0; i < xfld.nDOF(); i++)
    xfld.DOF(i) /= dist;
#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
