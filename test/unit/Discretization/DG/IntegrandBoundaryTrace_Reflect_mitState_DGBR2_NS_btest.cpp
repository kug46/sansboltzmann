// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandBoundaryTrace_Reflect_mitState_DGBR2_NS_btest
// testing of boundary integrands: Navier-Stokes

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes3D.h"
#include "pde/NS/BCNavierStokes3D.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"
#include "Discretization/DG/IntegrandInteriorTrace_DGBR2.h"
#include "Discretization/DG/ResidualInteriorTrace_DGBR2.h"
#include "Discretization/DG/ResidualBoundaryTrace_DGBR2.h"
#include "Discretization/DG/SetFieldInteriorTrace_DGBR2_LiftingOperator.h"
#include "Discretization/DG/SetFieldBoundaryTrace_DGBR2_LiftingOperator.h"

#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{

}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandBoundaryTrace_Reflect_mitState_DGBR2_NS_btest_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Tet_X1Q1R1_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef NDPDEClass::VectorX VectorX;
  typedef DLA::MatrixS<PhysD3::D,PhysD3::D,Real> MatrixX;

  typedef ElementXField<PhysD3,TopoD2,Triangle> ElementXFieldTrace;

  typedef BCEuler3D< BCTypeReflect_mitState, PDEClass > BCClassRaw;
  typedef BCNDConvertSpace<PhysD3, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, DGBR2> IntegrandBCClass;
  typedef IntegrandInteriorTrace_DGBR2<NDPDEClass> IntegrandIntClass;

  // PDE

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  const Real tSuth = 110;          // K
  const Real tRef = 300;

  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde( gas, visc, tcond, Euler_ResidInterp_Raw );

  Real rho, u, v, w, t, p;

  rho = 1.225;                          // kg/m^3
  u = 69.784; v = -3.231; w = 45.836;   // m/s
  t = 288.15;                           // K
  p = gas.pressure(rho,t);

  // BC
  BCClass bc(pde);

  // grid
  XField3D_1Tet_X1_1Group xfld1Tet;
  XField3D_2Tet_X1_1Group xfld2Tet;

  Real theta = 20*PI/180;
  Real c = cos(theta);
  Real s = sin(theta);

  //Rotate the grids so the normal vector is more complicated
  MatrixX Rx = {{ 1,  0,  0},
                { 0,  c, -s},
                { 0,  s,  c} };
  MatrixX Ry = {{ c,  0,  s},
                { 0,  1,  0},
                {-s,  0,  c} };
  MatrixX Rz = {{ c,  s,  0},
                {-s,  c,  0},
                { 0,  0,  1} };

  MatrixX Rt = Rx*Ry*Rz;

  for (int i = 0; i < xfld1Tet.nDOF(); i++)
  {
    VectorX X = xfld1Tet.DOF(i);
    xfld1Tet.DOF(i) = Rt*X;
  }

  for (int i = 0; i < xfld2Tet.nDOF(); i++)
  {
    VectorX X = xfld2Tet.DOF(i);
    xfld2Tet.DOF(i) = Rt*X;
  }

  int order = 1;
  Field_DG_Cell<PhysD3,TopoD3,ArrayQ> qfld1Tet(xfld1Tet, order, BasisFunctionCategory_Hierarchical);
  Field_DG_Cell<PhysD3,TopoD3,ArrayQ> qfld2Tet(xfld2Tet, order, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( qfld1Tet.nDOF(), 4);
  BOOST_REQUIRE_EQUAL( qfld2Tet.nDOF(), 8);

  ElementXFieldTrace xfldElemTrace( order, BasisFunctionCategory_Hierarchical );
  xfld2Tet.getInteriorTraceGroup<Triangle>(0).getElement(xfldElemTrace, 0);

  FieldLift_DG_Cell<PhysD3,TopoD3,VectorArrayQ> rfld1Tet(xfld1Tet, order, BasisFunctionCategory_Hierarchical);
  FieldLift_DG_Cell<PhysD3,TopoD3,VectorArrayQ> rfld2Tet(xfld2Tet, order, BasisFunctionCategory_Hierarchical);

  rfld1Tet = 0;
  rfld2Tet = 0;

  // solution

  ArrayQ q0 = {rho, u, v, w, p};
  ArrayQ q1 = q0*1.2;
  ArrayQ q2 = q0*1.1;
  ArrayQ q3 = q0*1.3;
  ArrayQ qB0, qB1, qB2, qB3;

  VectorX X = 0;
  VectorX nL = 0;
  xfldElemTrace.unitNormal(Triangle::centerRef, nL);

  // compute the reflected states
  bc.state(X, nL, q0, qB0);
  bc.state(X, nL, q1, qB1);
  bc.state(X, nL, q2, qB2);
  bc.state(X, nL, q3, qB3);

  // tet solution cell 0
  qfld2Tet.DOF(0) = qfld1Tet.DOF(0) = q0;
  qfld2Tet.DOF(1) = qfld1Tet.DOF(1) = q1;
  qfld2Tet.DOF(2) = qfld1Tet.DOF(2) = q2;
  qfld2Tet.DOF(3) = qfld1Tet.DOF(3) = q3;

  // tet solution cell 1
  qfld2Tet.DOF(4) = qB1;
  qfld2Tet.DOF(5) = qB0;
  qfld2Tet.DOF(6) = qB2;
  qfld2Tet.DOF(7) = qB3;

  // BR2 discretization
  Real eta = 2*Tet::NFace;
  DiscretizationDGBR2 disc(0, eta);

  // PDE global residuals
  SLA::SparseVector<ArrayQ> rsdPDEInt(qfld2Tet.nDOF());
  SLA::SparseVector<ArrayQ> rsdPDEBC(qfld1Tet.nDOF());

  rsdPDEInt = 0;
  rsdPDEBC = 0;

  // BC integrand functor
  IntegrandBCClass fcnBC( pde, bc, {1}, disc );
  IntegrandIntClass fcnInt( pde, disc, {0});

  // quadrature rule
  int quadratureOrder[4] = {3, 3, 3, 3};

  // Compute the field of inverse mass matrices for the two tets
  FieldDataInvMassMatrix_Cell mmfld2Tet(qfld2Tet);

  // compute the lifting operator on the interior trace
  IntegrateInteriorTraceGroups<TopoD3>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcnInt, mmfld2Tet),
                                                  xfld2Tet, (qfld2Tet, rfld2Tet), quadratureOrder, 1);

  // compute the PDE residual on the interior trace
  IntegrateInteriorTraceGroups<TopoD3>::integrate(ResidualInteriorTrace_DGBR2(fcnInt, rsdPDEInt),
                                                  xfld2Tet, (qfld2Tet, rfld2Tet), quadratureOrder, 1);


  // Compute the field of inverse mass matrices for the sinle tet
  FieldDataInvMassMatrix_Cell mmfld1Tet(qfld1Tet);

  // compute the lifting operator on the reflected boundary
  IntegrateBoundaryTraceGroups<TopoD3>::integrate(SetFieldBoundaryTrace_DGBR2_LiftingOperator(fcnBC, mmfld1Tet),
                                                  xfld1Tet, (qfld1Tet, rfld1Tet), quadratureOrder, 4);

  // compute the residual on the reflected boundary
  IntegrateBoundaryTraceGroups<TopoD3>::integrate( ResidualBoundaryTrace_DGBR2(fcnBC, rsdPDEBC),
                                                   xfld1Tet, (qfld1Tet, rfld1Tet), quadratureOrder, 4 );

  // Lifting operators and residuals should now match between the interior trace and the reflected boundary trace


  const Real small_tol = 1e-10;
  const Real close_tol = 5e-5;

  for (int i = 0; i < rfld1Tet.nDOF(); i++)
  {
    for (int n = 0; n < NDPDEClass::N; n++)
    {
#if 0
      std::cout << i << ": 0 : " << rfld2Tet.DOF(i)[0] << std::endl;
      std::cout << i << ": 0 : " << rfld1Tet.DOF(i)[0] << std::endl;
      std::cout << i << ": 1 : " << rfld2Tet.DOF(i)[1] << std::endl;
      std::cout << i << ": 1 : " << rfld1Tet.DOF(i)[1] << std::endl;
      std::cout << i << ": 2 : " << rfld2Tet.DOF(i)[2] << std::endl;
      std::cout << i << ": 2 : " << rfld1Tet.DOF(i)[2] << std::endl;
#endif
      SANS_CHECK_CLOSE( rfld2Tet.DOF(i)[0][n], rfld1Tet.DOF(i)[0][n], small_tol, close_tol );
      SANS_CHECK_CLOSE( rfld2Tet.DOF(i)[1][n], rfld1Tet.DOF(i)[1][n], small_tol, close_tol );
      SANS_CHECK_CLOSE( rfld2Tet.DOF(i)[2][n], rfld1Tet.DOF(i)[2][n], small_tol, close_tol );
    }
  }


  for (int i = 0; i < rsdPDEBC.m(); i++)
  {
#if 0
    std::cout << i << ": " << rsdPDEInt[i] << std::endl;
    std::cout << i << ": " << rsdPDEBC[i] << std::endl;
#endif
    SANS_CHECK_CLOSE( rsdPDEInt[i][0], rsdPDEBC[i][0], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsdPDEInt[i][1], rsdPDEBC[i][1], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsdPDEInt[i][2], rsdPDEBC[i][2], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsdPDEInt[i][3], rsdPDEBC[i][3], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsdPDEInt[i][4], rsdPDEBC[i][4], small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_3D_Tet_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCEuler3D< BCTypeReflect_mitState, PDEClass > BCClassRaw;
  typedef BCNDConvertSpace<PhysD3, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD3,TopoD2,Triangle> ElementXFieldTrace;

  typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD3,Tet> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD3,Tet> ElementRFieldClass;

  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, DGBR2> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,TopoD2,Triangle,TopoD3,Tet,ElementXFieldCell> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO<Real,Real,TopoD2,Triangle,TopoD3,Tet,ElementXFieldCell> BasisWeightedLOClass;
  typedef IntegrandClass::FieldWeighted<Real,Real,TopoD2,Triangle,TopoD3,Tet,ElementXFieldCell> FieldWeightedClass;

  // PDE

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde( gas, visc, tcond, Euler_ResidInterp_Raw );

  Real rho, u, v, w, t;

  rho = 1.225;                          // kg/m^3
  u = 69.784; v = -3.231; w = 45.836;   // m/s
  t = 288.15;                           // K

  // BC
  BCClass bc(pde);

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 4, xfldElem.nDOF() );

  ElementXFieldTrace xtri(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xtri.order() );
  BOOST_CHECK_EQUAL( 3, xtri.nDOF() );

  // tet grid
  Real v1[3] = {0,0,0};
  Real v2[3] = {1,0,0};
  Real v3[3] = {0,1,0};
  Real v4[3] = {0,0,1};

  xfldElem.DOF(0) = {v1[0], v1[1], v1[2]};
  xfldElem.DOF(1) = {v2[0], v2[1], v2[2]};
  xfldElem.DOF(2) = {v3[0], v3[1], v3[2]};
  xfldElem.DOF(3) = {v4[0], v4[1], v4[2]};

  // trace 0
  xtri.DOF(0) = xfldElem.DOF(1);
  xtri.DOF(1) = xfldElem.DOF(2);
  xtri.DOF(2) = xfldElem.DOF(3);

  for ( int qorder = 2; qorder < 3; qorder++ )
  {
    //solution
    ElementQFieldCell qfldElem(qorder, BasisFunctionCategory_Hierarchical );
    ElementQFieldCell wfldElem(qorder, BasisFunctionCategory_Hierarchical );

    BOOST_CHECK_EQUAL( qorder,   qfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)*(qorder+3)/6, qfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   wfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)*(qorder+3)/6, wfldElem.nDOF() );

    BOOST_CHECK_EQUAL( qfldElem.nDOF(), wfldElem.nDOF() );

    // line solution
    for ( int dof = 0; dof < qfldElem.nDOF(); dof++ )
    {
      Real frac = (dof+1)/pow(2,dof);
      qfldElem.DOF(dof) = pde.setDOFFrom( DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );
      qfldElem.DOF(dof) *= frac;
      wfldElem.DOF(dof) = 0;
    }

    // lifting operators
    ElementRFieldClass rfldElem(qorder, BasisFunctionCategory_Hierarchical);
    ElementRFieldClass sfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder,   rfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)*(qorder+3)/6, rfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   sfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)*(qorder+3)/6, sfldElem.nDOF() );

    BOOST_CHECK_EQUAL( rfldElem.nDOF(), sfldElem.nDOF() );
    // lifting operator
    for ( int dof = 0; dof < rfldElem.nDOF(); dof++ )
    {
      rfldElem.DOF(dof) = (dof+1)*pow(-1,dof);
      sfldElem.DOF(dof) = 0;
    }
    // BR2 discretization
    Real viscousEtaParameter = 2*Tet::NFace;
    DiscretizationDGBR2 disc(0, viscousEtaParameter);

    // integrand functor
    IntegrandClass fcnint( pde, bc, {0}, disc );

    BasisWeightedPDEClass fcnPDEB = fcnint.integrand_PDE( xtri,
                                                          CanonicalTraceToCell(0, 1),
                                                          xfldElem,
                                                          qfldElem, rfldElem );
    BasisWeightedLOClass fcnLOB = fcnint.integrand_LO( xtri,
                                                       CanonicalTraceToCell(0, 1),
                                                       xfldElem,
                                                       qfldElem, rfldElem );
    FieldWeightedClass fcnW = fcnint.integrand( xtri,
                                                CanonicalTraceToCell(0, 1),
                                                xfldElem,
                                                qfldElem, rfldElem,
                                                wfldElem, sfldElem);

    const Real small_tol = 1e-13;
    const Real close_tol = 5e-6;
    const int nDOF = qfldElem.nDOF();
    int nIntegrand = qfldElem.nDOF();
    int quadratureorder = 4;

    GalerkinWeightedIntegral<TopoD2, Triangle, BasisWeightedPDEClass::IntegrandType> integralPDEB(quadratureorder, nIntegrand);
    GalerkinWeightedIntegral<TopoD2, Triangle, BasisWeightedLOClass::IntegrandType> integralLOB(quadratureorder, nIntegrand);
    ElementIntegral<TopoD2, Triangle, FieldWeightedClass::IntegrandType> integralW(quadratureorder);

    std::vector<BasisWeightedPDEClass::IntegrandType> rsdPDEElemB(nDOF);
    std::vector<BasisWeightedLOClass::IntegrandType> rsdLOElemB(nDOF);
    FieldWeightedClass::IntegrandType rsdElemW = 0;

    // cell integration for canonical element
    integralPDEB( fcnPDEB, xtri, rsdPDEElemB.data(), rsdPDEElemB.size() );
    integralLOB( fcnLOB, xtri, rsdLOElemB.data(), rsdLOElemB.size() );

    for ( int i = 0; i < wfldElem.nDOF(); i++ )
    {
      for (int n = 0; n < PDEClass::N; n++)
      {
        // set just one weight to one (i.e. mimic weighting with phi)
        wfldElem.DOF(i)[n] = 1;
        for (int d = 0; d < PhysD2::D; d++ )
          sfldElem.DOF(i)[d][n] = 1;

        // cell integration for canonical Element
        rsdElemW = 0;
        integralW( fcnW, xtri, rsdElemW );

        // test the two integrands are the same
        SANS_CHECK_CLOSE ( rsdElemW.PDE, rsdPDEElemB[i][n], small_tol, close_tol );

        Real tmp = 0;
        for (int d = 0; d < PhysD2::D; d++ )
          tmp += rsdLOElemB[i][d][n];

        SANS_CHECK_CLOSE( rsdElemW.Lift, tmp, small_tol, close_tol );

        // reset to 0
        wfldElem.DOF(i)[n] = 0;
        for (int d = 0; d < PhysD2::D; d++ )
          sfldElem.DOF(i)[d][n] = 0;
      }
    }
  }

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
