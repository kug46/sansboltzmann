// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AlgebraicEquationSet_DGAdvective_Triangle_AD_btest
// testing AlgebraicEquationSet_DG

//#define DISPLAY_FOR_DEBUGGING

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <string>
#include <fstream>

#include "tools/SANSnumerics.h"     // Real

#include "Surreal/SurrealS.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#ifdef SANS_PETSC
#include "LinearAlgebra/SparseLinAlg/PETSc/PETScSolver.h"
#endif

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( AlgebraicEquationSet_DGAdvective_Triangle_AD_test_suite )


template<class MatrixQ>
void checkDenseSparseEquality(DLA::MatrixD<MatrixQ>& djac, const SLA::SparseMatrix_CRS<MatrixQ>& sjac )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  if (sjac.getNumNonZero() > 0)
  {
    const int *col_ind = sjac.get_col_ind();
    const int *row_ptr = sjac.get_row_ptr();

    for (int i = 0; i < sjac.m(); i++)
      for (int j = row_ptr[i]; j < row_ptr[i+1]; j++)
      {
        SANS_CHECK_CLOSE( djac(i,col_ind[j]), sjac[j], small_tol, close_tol );

        // Zero out the non-zero entry so the next loop can look for any non-zero values missed
        djac(i,col_ind[j]) = 0;
      }
  }

  // Check that all non-zero values have been cleared
  for (int i = 0; i < djac.m(); i++)
    for (int j = 0; j < djac.n(); j++)
      BOOST_CHECK_EQUAL(djac(i,j), 0);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseSystem_test )
{
  typedef ScalarFunction2D_SineSine SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_None,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;
  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_None> BCVector;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;


  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  ViscousFlux2D_None visc;

  NDSolutionExact solnExact;

  Source2D_None source;

  typedef ForcingFunction2D_MMS<PDEAdvectionDiffusion2D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict SineSine;
  SineSine[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.SineSine;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = SineSine;

  PyDict PyBCList;
  PyBCList["BCNameSolution"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNameSolution"] = {0, 1, 2, 3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);


  // grid: HierarchicalP1 (aka X1)

  int ii = 1;
  int jj = 1;

  XField2D_Box_Triangle_X1 xfld( ii, jj );


  // solution: P1 (aka Q1)

  int order = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0;


  // Lagrange multiplier: Legendre P1

#if 0
  order = 0;
  QField2D_DG_BoundaryEdge<PDEClass> lgfld( xfld, order, BasisFunctionCategory_Legendre );
#elif 1
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre,
                                                        BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#else
  order = 1;
  QField2D_CG_BoundaryEdge_Independent<PDEClass> lgfld( xfld, order );
#endif

  lgfld = 0;

  QuadratureOrder quadratureOrder( xfld, 2*order+1 );
  std::vector<Real> tol = {1e-11, 1e-11};

  PrimalEquationSetClass PrimalEqSet(xfld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_Default, tol,
                                     {0}, {0}, PyBCList, BCBoundaryGroups);

  SystemVectorClass q(PrimalEqSet.vectorStateSize());
  SystemVectorClass rsd(PrimalEqSet.vectorEqSize()), rsdnew(rsd.size());

  PrimalEqSet.fillSystemVector(q);

  rsd = 0;
  rsdnew = 0;

  PrimalEqSet.residual(q, rsd);

  // jacobian nonzero pattern

  SystemNonZeroPattern nz(PrimalEqSet.matrixSize());

  PrimalEqSet.jacobian(q, nz);

  // jacobian

  SystemMatrixClass jac(nz);
  jac = 0;

  PrimalEqSet.jacobian(q, jac);

  for ( int ii = 0; ii < jac.m(); ii++)
  {
    for ( int i = 0; i < jac(ii,0).m(); i++ )
    {
      std::vector<Real> AJac;
      std::vector<int> iAJac;

      for ( int jj = 0; jj < jac.n(); jj++)
      {
        if (jac(ii,jj).getNumNonZero() == 0) continue;
        for ( int k = 0; k < jac(ii,jj).rowNonZero(i); k++)
          if ( abs(jac(ii,jj).sparseRow(i,k)) > 1e-12)
          {
            // Save the non-zero value and column index
            AJac.push_back(jac(ii,jj).sparseRow(i,k));
            iAJac.push_back(jac(ii,jj).get_col_ind()[jac(ii,jj).get_row_ptr()[i] + k]);
          }
      }

#ifdef DISPLAY_FOR_DEBUGGING
      std::cout << "-----------------" << std::endl;
      std::cout << "row = " << i << std::endl;
      for ( std::size_t k = 0; k < AJac.size(); k++)
        std::cout << AJac[k] << ", ";
      std::cout << std::endl;
      for ( std::size_t k = 0; k < iAJac.size(); k++)
        std::cout << iAJac[k] << ", ";
      std::cout << std::endl;
#endif

      std::vector<Real> diffJac;
      std::vector<int> idiffJac;
      for ( int kk = 0; kk < q.m(); kk++)
      {
        if (jac(ii,kk).getNumNonZero() == 0) continue;
        for ( int k = 0; k < q[kk].m(); k++)
        {
          // Use finite difference to compute an exact Jacobian, which works for a Linear PDE
          rsdnew = 0;
          q[kk][k] += 1;

          PrimalEqSet.residual(q, rsdnew);

          q[kk][k] -= 1;

          Real diff = rsdnew[ii][i] - rsd[ii][i];

          // Save of only non-zero jacobian entries
          if ( abs(diff) > 1e-12 )
          {
            diffJac.push_back(diff);
            idiffJac.push_back(k);
          }
        }
      }
#ifdef DISPLAY_FOR_DEBUGGING
      std::cout << "FD" << std::endl;
      for ( std::size_t k = 0; k < diffJac.size(); k++)
        std::cout << diffJac[k] << ", ";
      std::cout << std::endl;
      for ( std::size_t k = 0; k < diffJac.size(); k++)
        std::cout << idiffJac[k] << ", ";
      std::cout << std::endl;
      std::cout << "A-FD" << std::endl;
#endif
      bool error = false;
      BOOST_REQUIRE_EQUAL(AJac.size(), diffJac.size());
      for ( std::size_t k = 0; k < diffJac.size(); k++)
      {
        if ( abs(AJac[k]-diffJac[k]) > 1e-12)
        {
          error = true;
          std::cout << "AJac[" << k << "]=" << AJac[k]
                    << " diffJac[" << k << "]=" << diffJac[k] << std::endl;
        }
      }
      BOOST_REQUIRE(!error);
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DenseSystem_test )
{
  typedef ScalarFunction2D_SineSine SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_None,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;
  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_None> BCVector;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Dense, DGAdv, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  ViscousFlux2D_None visc;

  NDSolutionExact solnExact;

  Source2D_None source;

  typedef ForcingFunction2D_MMS<PDEAdvectionDiffusion2D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict SineSine;
  SineSine[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.SineSine;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = SineSine;

  PyDict PyBCList;
  PyBCList["BCNameSolution"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNameSolution"] = {0, 1, 2, 3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);


  // grid: HierarchicalP1 (aka X1)

  int ii = 1;
  int jj = 1;

  XField2D_Box_Triangle_X1 xfld( ii, jj );


  // solution: P1 (aka Q1)

  int order = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0;

  // Lagrange multiplier: Legendre P1

#if 0
  order = 0;
  QField2D_DG_BoundaryEdge<PDEClass> lgfld( xfld, order, BasisFunctionCategory_Legendre );
#elif 1
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre,
                                                        BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#else
  order = 1;
  QField2D_CG_BoundaryEdge_Independent<PDEClass> lgfld( xfld, order );
#endif

  lgfld = 0;

  QuadratureOrder quadratureOrder( xfld, 2*order+1 );
  std::vector<Real> tol = {1e-11, 1e-11};

  PrimalEquationSetClass PrimalEqSet(xfld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_Default, tol,
                                     {0}, {0}, PyBCList, BCBoundaryGroups);

  SystemVectorClass q(PrimalEqSet.vectorStateSize());
  SystemVectorClass rsd(PrimalEqSet.vectorEqSize()), rsdnew(rsd.size());

  PrimalEqSet.fillSystemVector(q);

  rsd = 0;
  rsdnew = 0;

  PrimalEqSet.residual(q, rsd);

  // jacobian nonzero pattern

  SystemNonZeroPattern nz(PrimalEqSet.matrixSize());

  PrimalEqSet.jacobian(q, nz);

  // jacobian

  SystemMatrixClass jac(nz);
  jac = 0;

  PrimalEqSet.jacobian(q, jac);

  for ( int ii = 0; ii < jac.m(); ii++)
  {
    for ( int i = 0; i < jac(ii,0).m(); i++ )
    {
      std::vector<Real> AJac;
      std::vector<int> iAJac;

      for ( int jj = 0; jj < jac.n(); jj++)
      {
        for ( int k = 0; k < jac(ii,jj).n(); k++)
          if ( abs(jac(ii,jj)(i,k)) > 1e-12)
          {
            // Save the non-zero value and column index
            AJac.push_back(jac(ii,jj)(i,k));
            iAJac.push_back(k);
          }
      }

#ifdef DISPLAY_FOR_DEBUGGING
      std::cout << "-----------------" << std::endl;
      std::cout << "row = " << i << std::endl;
      for ( std::size_t k = 0; k < AJac.size(); k++)
        std::cout << AJac[k] << ", ";
      std::cout << std::endl;
      for ( std::size_t k = 0; k < iAJac.size(); k++)
        std::cout << iAJac[k] << ", ";
      std::cout << std::endl;
#endif

      std::vector<Real> diffJac;
      std::vector<int> idiffJac;
      for ( int kk = 0; kk < q.m(); kk++)
      {
        for ( int k = 0; k < q[kk].m(); k++)
        {
          // Use finite difference to compute an exact Jacobian, which works for a Linear PDE
          rsdnew = 0;
          q[kk][k] += 1;

          PrimalEqSet.residual(q, rsdnew);

          q[kk][k] -= 1;

          Real diff = rsdnew[ii][i] - rsd[ii][i];

          // Save of only non-zero jacobian entries
          if ( abs(diff) > 1e-12 )
          {
            diffJac.push_back(diff);
            idiffJac.push_back(k);
          }
        }
      }
#ifdef DISPLAY_FOR_DEBUGGING
      std::cout << "FD" << std::endl;
      for ( std::size_t k = 0; k < diffJac.size(); k++)
        std::cout << diffJac[k] << ", ";
      std::cout << std::endl;
      for ( std::size_t k = 0; k < diffJac.size(); k++)
        std::cout << idiffJac[k] << ", ";
      std::cout << std::endl;
      std::cout << "A-FD" << std::endl;
#endif
      bool error = false;
      BOOST_REQUIRE_EQUAL(AJac.size(), diffJac.size());
      for ( std::size_t k = 0; k < diffJac.size(); k++)
      {
        if ( abs(AJac[k]-diffJac[k]) > 1e-12)
        {
          error = true;
          std::cout << "AJac[" << k << "]=" << AJac[k]
                    << " diffJac[" << k << "]=" << diffJac[k] << std::endl;
        }
      }
      BOOST_REQUIRE(!error);
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Sparse_Dense_Equality_test )
{
  typedef ScalarFunction2D_SineSine SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_None,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;
  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_None> BCVector;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2>> PrimalEquationSetSparse;
  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Dense, DGAdv, XField<PhysD2, TopoD2>> PrimalEquationSetDense;
  typedef PrimalEquationSetSparse::BCParams BCParams;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef PrimalEquationSetSparse::SystemMatrix SparseSystemMatrixClass;
  typedef PrimalEquationSetSparse::SystemVector SparseSystemVectorClass;
  typedef PrimalEquationSetSparse::SystemNonZeroPattern SparseSystemNonZeroPattern;

  typedef PrimalEquationSetDense::SystemMatrix DenseSystemMatrixClass;
  typedef PrimalEquationSetDense::SystemVector DenseSystemVectorClass;
  typedef PrimalEquationSetDense::SystemNonZeroPattern DenseSystemNonZeroPattern;

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  ViscousFlux2D_None visc;

  NDSolutionExact solnExact;

  Source2D_None source;

  typedef ForcingFunction2D_MMS<PDEAdvectionDiffusion2D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict SineSine;
  SineSine[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.SineSine;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = SineSine;

  PyDict PyBCList;
  PyBCList["BCNameSolution"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNameSolution"] = {0, 1, 2, 3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);


  // grid: HierarchicalP1 (aka X1)

  int ii = 1;
  int jj = 1;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  // triangle area
  Real triArea = 0.5;
  Real invJ = 1./triArea;
//  int nElem = xfld.nElem();

  // solution: P1 (aka Q1)

  int order = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0;

  int nDOFPDE = qfld.nDOF();

  // Lagrange multiplier: Legendre P1

#if 0
  order = 0;
  QField2D_DG_BoundaryEdge<PDEClass> lgfld( xfld, order, BasisFunctionCategory_Legendre );
#elif 1
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre,
                                                        BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#else
  order = 1;
  QField2D_CG_BoundaryEdge_Independent<PDEClass> lgfld( xfld, order );
#endif

  lgfld = 0;
  int nDOFBC = lgfld.nDOF();

  QuadratureOrder quadratureOrder( xfld, 2*order+1 );
  std::vector<Real> tol = {1e-11, 1e-11};

  PrimalEquationSetSparse PrimalEqSetSparse(xfld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_Default, tol,
                                            {0}, {0}, PyBCList, BCBoundaryGroups);
  PrimalEquationSetDense PrimalEqSetDense(xfld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_Default, tol,
                                          {0}, {0}, PyBCList, BCBoundaryGroups);

  SparseSystemVectorClass q_s(PrimalEqSetSparse.vectorStateSize());
  DenseSystemVectorClass q_d(PrimalEqSetDense.vectorStateSize());

  SparseSystemVectorClass rsd_s(PrimalEqSetSparse.vectorEqSize());
  DenseSystemVectorClass rsd_d(PrimalEqSetDense.vectorEqSize());

  PrimalEqSetSparse.fillSystemVector(q_s);
  PrimalEqSetDense.fillSystemVector(q_d);

  rsd_s = 0;
  rsd_d = 0;

  PrimalEqSetSparse.residual(q_s, rsd_s);
  PrimalEqSetDense.residual(q_d, rsd_d);

  BOOST_REQUIRE_EQUAL( q_s.m(), q_d.m() );
  BOOST_REQUIRE_EQUAL( rsd_s.m(), rsd_d.m() );


  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  for ( int ii = 0; ii < q_s.m(); ii++ )
    for ( int i = 0; i < q_s[ii].m(); i++ )
      SANS_CHECK_CLOSE( q_s[ii][i], q_d[ii][i], small_tol, close_tol );

  // jacobian nonzero pattern

  SparseSystemNonZeroPattern nz_s(PrimalEqSetSparse.matrixSize());
  DenseSystemNonZeroPattern nz_d(PrimalEqSetDense.matrixSize());

  PrimalEqSetSparse.jacobian(q_s, nz_s);
  PrimalEqSetDense.jacobian(q_d, nz_d);

  // jacobian

  SparseSystemMatrixClass jac_s(nz_s);
  DenseSystemMatrixClass jac_d(nz_d);
  jac_s = 0;
  jac_d = 0;

  PrimalEqSetSparse.jacobian(q_s, jac_s);
  PrimalEqSetDense.jacobian(q_d, jac_d);


  BOOST_REQUIRE_EQUAL( jac_s.m(), jac_d.m() );
  BOOST_REQUIRE_EQUAL( jac_s.n(), jac_d.n() );

  // jacobian nonzero pattern
  //
  //           q  lg
  //   PDE     X   X
  //   BC      X   0
  BOOST_TEST_CHECKPOINT( "PDE_q" );
  checkDenseSparseEquality(jac_d(0,0), jac_s(0,0));

  BOOST_TEST_CHECKPOINT( "PDE_lg" );
  checkDenseSparseEquality(jac_d(0,1), jac_s(0,1));


  BOOST_TEST_CHECKPOINT( "BC_q" );
  checkDenseSparseEquality(jac_d(1,0), jac_s(1,0));

  BOOST_TEST_CHECKPOINT( "BC_lg" );
  checkDenseSparseEquality(jac_d(1,1), jac_s(1,1));

  // test residual converged
  SparseSystemVectorClass rsd_s1(q_s.size());
  SparseSystemVectorClass rsd_s2(q_s.size());

  for (int i=0; i<nDOFPDE; i++)
  {
    rsd_s1[0][i] = 1e-15;
    rsd_s2[0][i] = 1e-15;
  }

  for (int i=0; i<nDOFBC; i++)
  {
    rsd_s1[1][i] = 1e-15;
    rsd_s2[1][i] = 1e-15;
  }
  std::vector<std::vector<Real>> rsdNorm = PrimalEqSetSparse.residualNorm(rsd_s1);

  BOOST_REQUIRE( PrimalEqSetSparse.convergedResidual(rsdNorm) == true );

  rsd_s1[0][1] = 1e-8;
  rsdNorm = PrimalEqSetSparse.residualNorm(rsd_s1);

  BOOST_REQUIRE( PrimalEqSetSparse.convergedResidual(rsdNorm) == false );

  // test residual decreased
  rsd_s1[PrimalEqSetSparse.iPDE] = 2;

  std::vector<std::vector<Real>> norm1( PrimalEqSetSparse.residualNorm(rsd_s1) );
  BOOST_CHECK_EQUAL(norm1[0][0], sqrt((qfld.nDOF()*2*2*invJ*invJ)/(qfld.nDOF())));

  rsd_s2[0][0] = 1e-5;
  rsd_s2[0][1] = 1e-5;
  rsd_s2[0][2] = 1e-5;
  rsd_s2[0][3] = 1e-5;
  rsd_s2[0][4] = 1e-5;
  rsd_s2[0][5] = 1e-5;
  rsdNorm = PrimalEqSetSparse.residualNorm(rsd_s2);

  BOOST_REQUIRE( PrimalEqSetSparse.decreasedResidual(norm1,rsdNorm) == true );

  rsd_s2[0][1] = 5;
  rsdNorm = PrimalEqSetSparse.residualNorm(rsd_s2);

  BOOST_REQUIRE( PrimalEqSetSparse.decreasedResidual(norm1,rsdNorm) == false );

  //test already converged
  rsd_s1[0][0] = 1e-14;
  rsd_s1[0][1] = 1e-14;
  rsd_s1[0][2] = 1e-14;
  rsd_s1[0][3] = 1e-14;
  rsd_s1[0][4] = 1e-14;
  rsd_s1[0][5] = 1e-14;

  rsd_s2[0][0] = 1e-13;
  rsd_s2[0][1] = 1e-13;
  rsd_s2[0][2] = 1e-13;
  rsd_s2[0][3] = 1e-13;
  rsd_s2[0][4] = 1e-13;
  rsd_s2[0][5] = 1e-13;
  rsdNorm = PrimalEqSetSparse.residualNorm(rsd_s2);

  BOOST_REQUIRE( PrimalEqSetSparse.decreasedResidual(norm1,rsdNorm) == true );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Serial_Parallel_Equivalency )
{
  typedef ScalarFunction2D_SineSine SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_None,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;
  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_None> BCVector;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Dense, DGAdv, XField<PhysD2, TopoD2>> PrimalEquationSetDense;
  typedef PrimalEquationSetDense::BCParams BCParams;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef PrimalEquationSetDense::SystemMatrix DenseSystemMatrixClass;
  typedef PrimalEquationSetDense::SystemVector DenseSystemVectorClass;
  typedef PrimalEquationSetDense::SystemNonZeroPattern DenseSystemNonZeroPattern;

  // global communicator
  mpi::communicator world;
  mpi::communicator comm_global = world.split(world.rank());

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  ViscousFlux2D_None visc;

  NDSolutionExact solnExact;

  Source2D_None source;

  typedef ForcingFunction2D_MMS<PDEAdvectionDiffusion2D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict SineSine;
  SineSine[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.SineSine;

//  PyDict BCSoln_mitLG;
//  BCSoln_mitLG[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
//  BCSoln_mitLG[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = SineSine;

  PyDict BCSoln_sansLG;
  BCSoln_sansLG[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln_sansLG[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = SineSine;

  PyDict PyBCList;
//  PyBCList["BCNameFunctionLinearRobin_mitLG"] = BCSoln_mitLG;
  PyBCList["BCNameFunctionLinearRobin_sansLG"] = BCSoln_sansLG;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
//  BCBoundaryGroups["BCNameFunctionLinearRobin_mitLG"]  = {XField2D_Box_Triangle_X1::iLeft,
//                                                          XField2D_Box_Triangle_X1::iBottom};
//  BCBoundaryGroups["BCNameFunctionLinearRobin_sansLG"] = {XField2D_Box_Triangle_X1::iRight,
//                                                          XField2D_Box_Triangle_X1::iTop};

  // TODO: Use above when Field_DG_BoundaryTrace has been parallelized
  BCBoundaryGroups["BCNameFunctionLinearRobin_sansLG"] = {XField2D_Box_Triangle_X1::iLeft,
                                                          XField2D_Box_Triangle_X1::iBottom,
                                                          XField2D_Box_Triangle_X1::iRight,
                                                          XField2D_Box_Triangle_X1::iTop};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  for (int comm_size = 1; comm_size <= world.size(); comm_size++)
  {
    int color = world.rank() < comm_size ? 0 : 1;
    mpi::communicator comm_local = world.split(color);

    if (color == 1) continue;

    // grid: HierarchicalP1 (aka X1)

    int ii = 3;
    int jj = 3;

    XField2D_Box_Triangle_Lagrange_X1 xfld_global( comm_global, ii, jj ); // complete system on all processors
    XField2D_Box_Triangle_Lagrange_X1 xfld_local ( comm_local , ii, jj ); // partitioned system

    // solution: P1 (aka Q1)
    int order = 1;
    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_global(xfld_global, order, BasisFunctionCategory_Legendre);
    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_local (xfld_local , order, BasisFunctionCategory_Legendre);
    qfld_global = 0;
    qfld_local  = 0;

    // Lagrange multiplier: Legendre P1
    std::vector<int> mitLG_bcgroups = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
    Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_global( xfld_global, order, BasisFunctionCategory_Legendre, mitLG_bcgroups );
    Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_local ( xfld_local , order, BasisFunctionCategory_Legendre, mitLG_bcgroups );
    lgfld_global = 0;
    lgfld_local  = 0;

    QuadratureOrder quadratureOrder( xfld_global, 2*order + 1 );
    std::vector<Real> tol = {1e-12, 1e-12};
    PrimalEquationSetDense PrimalEqSet_global(xfld_global, qfld_global, lgfld_global, pde, quadratureOrder,
                                               ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups);
    PrimalEquationSetDense PrimalEqSet_local (xfld_local, qfld_local, lgfld_local, pde, quadratureOrder,
                                              ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups);

    DenseSystemVectorClass q_global(PrimalEqSet_global.vectorStateSize());
    DenseSystemVectorClass q_local (PrimalEqSet_local.vectorStateSize());

    DenseSystemVectorClass rsd_global(PrimalEqSet_global.vectorEqSize());
    DenseSystemVectorClass rsd_local (PrimalEqSet_local.vectorEqSize());

    PrimalEqSet_global.fillSystemVector(q_global);
    PrimalEqSet_local.fillSystemVector(q_local);

    rsd_global = 0;
    rsd_local = 0;

    PrimalEqSet_global.residual(q_global, rsd_global);
    PrimalEqSet_local.residual(q_local, rsd_local);

    BOOST_REQUIRE_EQUAL( rsd_local[0].m(), qfld_local.nDOFpossessed() );

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;

    BOOST_REQUIRE_EQUAL(rsd_global.m(), 2);
    BOOST_REQUIRE_EQUAL(rsd_local.m(), 2);

    for ( int i = 0; i < rsd_local[0].m(); i++ )
      SANS_CHECK_CLOSE( rsd_global[0][qfld_local.local2nativeDOFmap(i)], rsd_local[0][i], small_tol, close_tol );

    for ( int i = 0; i < rsd_local[1].m(); i++ )
      SANS_CHECK_CLOSE( rsd_global[1][lgfld_local.local2nativeDOFmap(i)], rsd_local[1][i], small_tol, close_tol );

    // jacobian nonzero pattern

    DenseSystemNonZeroPattern nz_global(PrimalEqSet_global.matrixSize());
    DenseSystemNonZeroPattern nz_local(PrimalEqSet_local.matrixSize());

    PrimalEqSet_global.jacobian(q_global, nz_global);
    PrimalEqSet_local.jacobian(q_local, nz_local);

    // jacobian

    DenseSystemMatrixClass jac_global(nz_global);
    DenseSystemMatrixClass jac_local(nz_local);
    jac_global = 0;
    jac_local = 0;

    PrimalEqSet_global.jacobian(q_global, jac_global);
    PrimalEqSet_local.jacobian(q_local, jac_local);

    BOOST_REQUIRE_EQUAL(jac_global.m(), 2);
    BOOST_REQUIRE_EQUAL(jac_global.n(), 2);

    BOOST_REQUIRE_EQUAL(jac_local.m(), 2);
    BOOST_REQUIRE_EQUAL(jac_local.n(), 2);

#if __clang_analyzer__
    return; // clang thinks we are accessing zero size memory below... sigh...
#endif

    // jacobian nonzero pattern
    //
    //           q  lg
    //   PDE     X   X
    //   BC      X   0

    const DenseSystemMatrixClass::node_type& PDE_q_global = jac_global(0,0);
    const DenseSystemMatrixClass::node_type& PDE_q_local = jac_local(0,0);

    for ( int i = 0; i < PDE_q_local.m(); i++ )
      for ( int j = 0; j < PDE_q_local.n(); j++ )
      {
        int in = qfld_local.local2nativeDOFmap(i);
        int jn = qfld_local.local2nativeDOFmap(j);
        SANS_CHECK_CLOSE( PDE_q_global(in,jn), PDE_q_local(i,j), small_tol, close_tol );
      }

    const DenseSystemMatrixClass::node_type& PDE_lg_global = jac_global(0,1);
    const DenseSystemMatrixClass::node_type& PDE_lg_local = jac_local(0,1);

    for ( int i = 0; i < PDE_lg_local.m(); i++ )
      for ( int j = 0; j < PDE_lg_local.n(); j++ )
      {
        int in = qfld_local.local2nativeDOFmap(i);
        int jn = lgfld_local.local2nativeDOFmap(j);
        SANS_CHECK_CLOSE( PDE_lg_global(in,jn), PDE_lg_local(i,j), small_tol, close_tol );
      }


    const DenseSystemMatrixClass::node_type& BC_q_global = jac_global(1,0);
    const DenseSystemMatrixClass::node_type& BC_q_local = jac_local(1,0);

    for ( int i = 0; i < BC_q_local.m(); i++ )
      for ( int j = 0; j < BC_q_local.n(); j++ )
      {
        int in = lgfld_local.local2nativeDOFmap(i);
        int jn = qfld_local.local2nativeDOFmap(j);
        SANS_CHECK_CLOSE( BC_q_global(in,jn), BC_q_local(i,j), small_tol, close_tol );
      }

    const DenseSystemMatrixClass::node_type& BC_lg_global = jac_global(1,1);
    const DenseSystemMatrixClass::node_type& BC_lg_local = jac_local(1,1);

    for ( int i = 0; i < BC_lg_local.m(); i++ )
      for ( int j = 0; j < BC_lg_local.n(); j++ )
      {
        int in = lgfld_local.local2nativeDOFmap(i);
        int jn = lgfld_local.local2nativeDOFmap(j);
        SANS_CHECK_CLOSE( BC_lg_global(in,jn), BC_lg_local(i,j), small_tol, close_tol );
      }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Serial_Parallel_Periodic_Equivalency )
{
  typedef ScalarFunction2D_SineSine SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_None,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;
  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_None> BCVector;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Dense, DGAdv, XField<PhysD2, TopoD2>> PrimalEquationSetDense;
  typedef PrimalEquationSetDense::BCParams BCParams;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef PrimalEquationSetDense::SystemMatrix DenseSystemMatrixClass;
  typedef PrimalEquationSetDense::SystemVector DenseSystemVectorClass;
  typedef PrimalEquationSetDense::SystemNonZeroPattern DenseSystemNonZeroPattern;

  // global communicator
  mpi::communicator world;
  mpi::communicator comm_global = world.split(world.rank());

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  ViscousFlux2D_None visc;

  NDSolutionExact solnExact;

  Source2D_None source;

  typedef ForcingFunction2D_MMS<PDEAdvectionDiffusion2D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict SineSine;
  SineSine[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.SineSine;

  PyDict PyBCList;
  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);


  for (int comm_size = 1; comm_size <= world.size(); comm_size++)
  {
    int color = world.rank() < comm_size ? 0 : 1;
    mpi::communicator comm_local = world.split(color);

    if (color == 1) continue;

    // grid: HierarchicalP1 (aka X1)

    int ii = 3;
    int jj = 3;

    // construct grids with periodic boundaries
    XField2D_Box_Triangle_Lagrange_X1 xfld_global( comm_global, ii, jj, 0,1, 0,1, {{ true, true }} ); // complete system on all processors
    XField2D_Box_Triangle_Lagrange_X1 xfld_local ( comm_local , ii, jj, 0,1, 0,1, {{ true, true }} ); // partitioned system

    // solution: P1 (aka Q1)
    int order = 1;
    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_global(xfld_global, order, BasisFunctionCategory_Legendre);
    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_local (xfld_local , order, BasisFunctionCategory_Legendre);
    qfld_global = 0;
    qfld_local  = 0;

    // Lagrange multiplier: Legendre P1
    std::vector<int> mitLG_bcgroups = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
    Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_global( xfld_global, order, BasisFunctionCategory_Legendre, mitLG_bcgroups );
    Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_local ( xfld_local , order, BasisFunctionCategory_Legendre, mitLG_bcgroups );
    lgfld_global = 0;
    lgfld_local  = 0;

    QuadratureOrder quadratureOrder( xfld_global, 2*order + 1 );
    std::vector<Real> tol = {1e-12, 1e-12};
    PrimalEquationSetDense PrimalEqSet_global(xfld_global, qfld_global, lgfld_global, pde, quadratureOrder,
                                               ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups);
    PrimalEquationSetDense PrimalEqSet_local (xfld_local, qfld_local, lgfld_local, pde, quadratureOrder,
                                              ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups);

    DenseSystemVectorClass q_global(PrimalEqSet_global.vectorStateSize());
    DenseSystemVectorClass q_local (PrimalEqSet_local.vectorStateSize());

    DenseSystemVectorClass rsd_global(PrimalEqSet_global.vectorEqSize());
    DenseSystemVectorClass rsd_local (PrimalEqSet_local.vectorEqSize());

    PrimalEqSet_global.fillSystemVector(q_global);
    PrimalEqSet_local.fillSystemVector(q_local);

    rsd_global = 0;
    rsd_local = 0;

    PrimalEqSet_global.residual(q_global, rsd_global);
    PrimalEqSet_local.residual(q_local, rsd_local);

    BOOST_REQUIRE_EQUAL( rsd_local[0].m(), qfld_local.nDOFpossessed() );

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;

    BOOST_REQUIRE_EQUAL(rsd_global.m(), 2);
    BOOST_REQUIRE_EQUAL(rsd_local.m(), 2);

    for ( int i = 0; i < rsd_local[0].m(); i++ )
      SANS_CHECK_CLOSE( rsd_global[0][qfld_local.local2nativeDOFmap(i)], rsd_local[0][i], small_tol, close_tol );

    for ( int i = 0; i < rsd_local[1].m(); i++ )
      SANS_CHECK_CLOSE( rsd_global[1][lgfld_local.local2nativeDOFmap(i)], rsd_local[1][i], small_tol, close_tol );

    // jacobian nonzero pattern

    DenseSystemNonZeroPattern nz_global(PrimalEqSet_global.matrixSize());
    DenseSystemNonZeroPattern nz_local(PrimalEqSet_local.matrixSize());

    PrimalEqSet_global.jacobian(q_global, nz_global);
    PrimalEqSet_local.jacobian(q_local, nz_local);

    // jacobian

    DenseSystemMatrixClass jac_global(nz_global);
    DenseSystemMatrixClass jac_local(nz_local);
    jac_global = 0;
    jac_local = 0;

    PrimalEqSet_global.jacobian(q_global, jac_global);
    PrimalEqSet_local.jacobian(q_local, jac_local);

    BOOST_REQUIRE_EQUAL(jac_global.m(), 2);
    BOOST_REQUIRE_EQUAL(jac_global.n(), 2);

    BOOST_REQUIRE_EQUAL(jac_local.m(), 2);
    BOOST_REQUIRE_EQUAL(jac_local.n(), 2);

#if __clang_analyzer__
    return; // clang thinks we are accessing zero size memory below... sigh...
#endif

    // jacobian nonzero pattern
    //
    //           q  lg
    //   PDE     X   X
    //   BC      X   0

    const DenseSystemMatrixClass::node_type& PDE_q_global = jac_global(0,0);
    const DenseSystemMatrixClass::node_type& PDE_q_local = jac_local(0,0);

    for ( int i = 0; i < PDE_q_local.m(); i++ )
      for ( int j = 0; j < PDE_q_local.n(); j++ )
      {
        int in = qfld_local.local2nativeDOFmap(i);
        int jn = qfld_local.local2nativeDOFmap(j);
        SANS_CHECK_CLOSE( PDE_q_global(in,jn), PDE_q_local(i,j), small_tol, close_tol );
      }

    const DenseSystemMatrixClass::node_type& PDE_lg_global = jac_global(0,1);
    const DenseSystemMatrixClass::node_type& PDE_lg_local = jac_local(0,1);

    for ( int i = 0; i < PDE_lg_local.m(); i++ )
      for ( int j = 0; j < PDE_lg_local.n(); j++ )
      {
        int in = qfld_local.local2nativeDOFmap(i);
        int jn = lgfld_local.local2nativeDOFmap(j);
        SANS_CHECK_CLOSE( PDE_lg_global(in,jn), PDE_lg_local(i,j), small_tol, close_tol );
      }


    const DenseSystemMatrixClass::node_type& BC_q_global = jac_global(1,0);
    const DenseSystemMatrixClass::node_type& BC_q_local = jac_local(1,0);

    for ( int i = 0; i < BC_q_local.m(); i++ )
      for ( int j = 0; j < BC_q_local.n(); j++ )
      {
        int in = lgfld_local.local2nativeDOFmap(i);
        int jn = qfld_local.local2nativeDOFmap(j);
        SANS_CHECK_CLOSE( BC_q_global(in,jn), BC_q_local(i,j), small_tol, close_tol );
      }

    const DenseSystemMatrixClass::node_type& BC_lg_global = jac_global(1,1);
    const DenseSystemMatrixClass::node_type& BC_lg_local = jac_local(1,1);

    for ( int i = 0; i < BC_lg_local.m(); i++ )
      for ( int j = 0; j < BC_lg_local.n(); j++ )
      {
        int in = lgfld_local.local2nativeDOFmap(i);
        int jn = lgfld_local.local2nativeDOFmap(j);
        SANS_CHECK_CLOSE( BC_lg_global(in,jn), BC_lg_local(i,j), small_tol, close_tol );
      }
  }
}


#ifdef SANS_PETSC
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Serial_Parallel_Solve_Equivalency )
{
  typedef ScalarFunction2D_SineSine SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_None,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_None> BCVector;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2>> PrimalEquationSetSparse;
  typedef PrimalEquationSetSparse::BCParams BCParams;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef PrimalEquationSetSparse::SystemMatrix SparseSystemMatrixClass;
  typedef PrimalEquationSetSparse::SystemVector SparseSystemVectorClass;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-11;

  // global communicator
  mpi::communicator world;
  mpi::communicator comm_global = world.split(world.rank());

  // PDE
  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  ViscousFlux2D_None visc;

  NDSolutionExact solnExact;

  Source2D_None source;

  typedef ForcingFunction2D_MMS<PDEAdvectionDiffusion2D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  typedef BCTypeFunction_mitStateParam BCType;

  PyDict SineSine;
  SineSine[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.SineSine;

//  PyDict BCSoln;
//  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
//  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = SineSine;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = SineSine;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.SolutionBCType] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.SolutionBCType.Robin;

  PyDict PyBCList;
  PyBCList["BCNameFunction"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
//  BCBoundaryGroups["BCNameFunctionLinearRobin_mitLG"]  = {XField2D_Box_Triangle_X1::iLeft,
//                                                          XField2D_Box_Triangle_X1::iBottom};
//  BCBoundaryGroups["BCNameFunctionLinearRobin_sansLG"] = {XField2D_Box_Triangle_X1::iRight,
//                                                          XField2D_Box_Triangle_X1::iTop};

  // TODO: Use above when Field_DG_BoundaryTrace has been parallelized
  BCBoundaryGroups["BCNameFunction"] = {XField2D_Box_Triangle_X1::iLeft,
                                        XField2D_Box_Triangle_X1::iBottom,
                                        XField2D_Box_Triangle_X1::iRight,
                                        XField2D_Box_Triangle_X1::iTop};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  for (int comm_size = 1; comm_size <= world.size(); comm_size++)
  {
    int color = world.rank() < comm_size ? 0 : 1;
    mpi::communicator comm_local = world.split(color);

    if (color == 1) continue;

    // grid: HierarchicalP1 (aka X1)

    int ii = 3;
    int jj = 3;

    XField2D_Box_Triangle_Lagrange_X1 xfld_global( comm_global, ii, jj ); // complete system on all processors
    XField2D_Box_Triangle_Lagrange_X1 xfld_local ( comm_local , ii, jj ); // partitioned system

    // solution: P1 (aka Q1)
    int order = 1;
    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_global(xfld_global, order, BasisFunctionCategory_Legendre);
    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_local (xfld_local , order, BasisFunctionCategory_Legendre);
    qfld_global = 0;
    qfld_local  = 0;

    // Lagrange multiplier: Legendre P1
    std::vector<int> mitLG_bcgroups = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
    Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_global( xfld_global, order, BasisFunctionCategory_Legendre, mitLG_bcgroups );
    Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_local ( xfld_local , order, BasisFunctionCategory_Legendre, mitLG_bcgroups );
    lgfld_global = 0;
    lgfld_local  = 0;

    QuadratureOrder quadratureOrder( xfld_global, 2*order + 1 );
    std::vector<Real> tol = {1e-12, 1e-12};
    PrimalEquationSetSparse PrimalEqSet_global(xfld_global, qfld_global, lgfld_global, pde, quadratureOrder,
                                               ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups);
    PrimalEquationSetSparse PrimalEqSet_local (xfld_local, qfld_local, lgfld_local, pde, quadratureOrder,
                                              ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups);

    SparseSystemVectorClass q_global(PrimalEqSet_global.vectorStateSize());
    SparseSystemVectorClass q_local (PrimalEqSet_local.vectorStateSize());

    SparseSystemVectorClass rsd_global(PrimalEqSet_global.vectorEqSize());
    SparseSystemVectorClass rsd_local (PrimalEqSet_local.vectorEqSize());

    SparseSystemVectorClass dq_global(PrimalEqSet_global.vectorStateSize());
    SparseSystemVectorClass dq_local (PrimalEqSet_local.vectorStateSize());

    PrimalEqSet_global.fillSystemVector(q_global);
    PrimalEqSet_local.fillSystemVector(q_local);

    rsd_global = 0;
    rsd_local = 0;

    PrimalEqSet_global.residual(q_global, rsd_global);
    PrimalEqSet_local.residual(q_local, rsd_local);

    BOOST_REQUIRE_EQUAL(rsd_global.m(), 2);
    BOOST_REQUIRE_EQUAL(rsd_local.m(), 2);

    for ( int i = 0; i < rsd_local[0].m(); i++ )
      SANS_CHECK_CLOSE( rsd_global[0][qfld_local.local2nativeDOFmap(i)], rsd_local[0][i], small_tol, close_tol );

    for ( int i = 0; i < rsd_local[1].m(); i++ )
      SANS_CHECK_CLOSE( rsd_global[1][lgfld_local.local2nativeDOFmap(i)], rsd_local[1][i], small_tol, close_tol );

    PyDict PETScDict;
    PETScDict[SLA::PETScSolverParam::params.Verbose] = false;

    SLA::PETScSolver<SparseSystemMatrixClass> Solver_global(PETScDict, PrimalEqSet_global);
    SLA::PETScSolver<SparseSystemMatrixClass> Solver_local(PETScDict, PrimalEqSet_local);

    dq_global = 0;
    dq_local = 0;

    //Solve the linear system.
    Solver_global.solve(rsd_global, dq_global);
    Solver_local.solve(rsd_local, dq_local);

    BOOST_REQUIRE_EQUAL(dq_global.m(), 2);
    BOOST_REQUIRE_EQUAL(dq_local.m(), 2);

    for ( int i = 0; i < dq_local[0].m(); i++ )
      SANS_CHECK_CLOSE( dq_global[0][qfld_local.local2nativeDOFmap(i)], dq_local[0][i], small_tol, close_tol );

    for ( int i = 0; i < dq_local[1].m(); i++ )
      SANS_CHECK_CLOSE( dq_global[1][lgfld_local.local2nativeDOFmap(i)], dq_local[1][i], small_tol, close_tol );
  }
}
#endif // SANS_PETSC

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( daisy_chain_test )
{
  typedef ScalarFunction2D_SineSine SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_None,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;
  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_None> BCVector;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2>> PrimalEquationSetSparse;
  typedef PrimalEquationSetSparse::BCParams BCParams;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef PrimalEquationSetSparse::SystemMatrix SparseSystemMatrixClass;
  typedef PrimalEquationSetSparse::SystemVector SparseSystemVectorClass;
  typedef PrimalEquationSetSparse::SystemNonZeroPattern SparseSystemNonZeroPattern;

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  ViscousFlux2D_None visc;

  NDSolutionExact solnExact;

  Source2D_None source;

  typedef ForcingFunction2D_MMS<PDEAdvectionDiffusion2D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict SineSine;
  SineSine[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.SineSine;

  PyDict BCSoln_mitLG;
  BCSoln_mitLG[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln_mitLG[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = SineSine;

  PyDict BCSoln_sansLG;
  BCSoln_sansLG[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln_sansLG[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = SineSine;

  PyDict PyBCList;
  PyBCList["BCNameFunctionLinearRobin_mitLG"] = BCSoln_mitLG;
  PyBCList["BCNameFunctionLinearRobin_sansLG"] = BCSoln_sansLG;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNameFunctionLinearRobin_mitLG"] = {0, 1};
  BCBoundaryGroups["BCNameFunctionLinearRobin_sansLG"] = {2, 3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);


  // grid: HierarchicalP1 (aka X1)

  int ii = 1;
  int jj = 1;

  XField2D_Box_Triangle_X1 xfld( ii, jj );


  // solution: P1 (aka Q1)

  int order = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0;

  // Lagrange multiplier: Legendre P1

#if 0
  order = 0;
  QField2D_DG_BoundaryEdge<PDEClass> lgfld( xfld, order, BasisFunctionCategory_Legendre );
#elif 1
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre,
                                                        BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#else
  order = 1;
  QField2D_CG_BoundaryEdge_Independent<PDEClass> lgfld( xfld, order );
#endif

  lgfld = 0;

  QuadratureOrder quadratureOrder( xfld, 2*order+1 );
  std::vector<Real> tol = {1e-12, 1e-12};
  PrimalEquationSetSparse AES1(xfld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_Default, tol,
                               {0}, {0}, PyBCList, BCBoundaryGroups);
  std::shared_ptr<PrimalEquationSetSparse> AES2(
      new PrimalEquationSetSparse(xfld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_Default, tol,
                                  {0}, {0}, PyBCList, BCBoundaryGroups) );
  // Add the new AES
  AES1.addAlgebraicEquationSet(AES2);

  SparseSystemVectorClass q_s(AES1.vectorStateSize());
  SparseSystemVectorClass q_d(AES2->vectorStateSize());

  SparseSystemVectorClass rsd_s(AES1.vectorEqSize());
  SparseSystemVectorClass rsd_d(AES2->vectorEqSize());

  AES1.fillSystemVector(q_s);
  AES2->fillSystemVector(q_d);

  rsd_s = 0;
  rsd_d = 0;

  AES1.residual(q_s, rsd_s);
  AES2->residual(q_d, rsd_d);

  BOOST_REQUIRE_EQUAL( q_s.m(), q_d.m() );
  BOOST_REQUIRE_EQUAL( rsd_s.m(), rsd_d.m() );


  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  for ( int ii = 0; ii < q_s.m(); ii++ )
    for ( int i = 0; i < q_s[ii].m(); i++ )
      SANS_CHECK_CLOSE( rsd_s[ii][i], 2.0*rsd_d[ii][i], small_tol, close_tol );

  // jacobian nonzero pattern
  SparseSystemNonZeroPattern nz_s(AES1.matrixSize());
  SparseSystemNonZeroPattern nz_d(AES2->matrixSize());

  AES1.jacobian(q_s, nz_s);
  AES2->jacobian(q_d, nz_d);

  // jacobian
  SparseSystemMatrixClass jac_s(nz_s);
  SparseSystemMatrixClass jac_d(nz_d);
  jac_s = 0;
  jac_d = 0;

  AES1.jacobian(q_s, jac_s);
  AES2->jacobian(q_d, jac_d);

  BOOST_REQUIRE_EQUAL( jac_s.m(), jac_d.m() );
  BOOST_REQUIRE_EQUAL( jac_s.n(), jac_d.n() );

  for ( int ii = 0; ii < jac_s.m(); ii++ )
  {
    for ( int jj = 0; jj < jac_s.n(); jj++ )
    {
      BOOST_REQUIRE_EQUAL( jac_s(ii,jj).m(), jac_d(ii,jj).m() );
      BOOST_REQUIRE_EQUAL( jac_s(ii,jj).n(), jac_d(ii,jj).n() );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Residual_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_None,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;
  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_None> BCVector;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2>> PrimalEquationSet;
  typedef PrimalEquationSet::BCParams BCParams;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef PrimalEquationSet::SystemVector SystemVectorClass;

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  ViscousFlux2D_None visc;
  Source2D_None source;

  NDPDEClass pde( adv, visc, source );

  // BC
  PyDict SineSine;
  SineSine[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.SineSine;

  PyDict BCSoln_mitLG;
  BCSoln_mitLG[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln_mitLG[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = SineSine;

  PyDict BCSoln_sansLG;
  BCSoln_sansLG[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln_sansLG[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = SineSine;

  PyDict PyBCList;
  PyBCList["BCNameFunctionLinearRobin_mitLG"] = BCSoln_mitLG;
  PyBCList["BCNameFunctionLinearRobin_sansLG"] = BCSoln_sansLG;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNameFunctionLinearRobin_mitLG"] = {0, 1};
  BCBoundaryGroups["BCNameFunctionLinearRobin_sansLG"] = {2, 3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // grid
  int ii = 1;
  int jj = 1;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  // area of each triangle
  Real triArea = 0.5;
  Real invJ = 1./triArea;
//  int nElem = xfld.nElem();

  // solution: P1 (aka Q1)

  int order = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0;

  // Lagrange multiplier: Legendre P1
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre,
                                                        BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
  lgfld = 0;

  QuadratureOrder quadratureOrder( xfld, 2*order+1 );
  std::vector<Real> tol = {2e-12, 3e-12};

  PrimalEquationSet PrimalEqSet1(xfld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_Default, tol,
                                 {0}, {0}, PyBCList, BCBoundaryGroups);

  SystemVectorClass rsd(PrimalEqSet1.vectorEqSize());

  rsd[PrimalEqSet1.iPDE] = 2;
  rsd[PrimalEqSet1.iBC] = 3;

  std::vector<std::vector<Real>> rsdNrm1( PrimalEqSet1.residualNorm(rsd) );

  const Real close_tol = 1e-12;

  //Checking if the default residual norm type is equal to ResidualNorm_InvJac_DOF_Weighted
  BOOST_CHECK_CLOSE( sqrt((qfld.nDOF()*2*2*invJ*invJ)/(qfld.nDOF())), rsdNrm1[PrimalEqSet1.iPDE][0], close_tol );
  BOOST_CHECK_CLOSE( sqrt(lgfld.nDOF()*3*3), rsdNrm1[PrimalEqSet1.iBC][0], close_tol );

  //Try other residual norm types too
  PrimalEquationSet PrimalEqSet2(xfld, qfld, lgfld, pde, quadratureOrder,
                                 ResidualNorm_L2, tol, {0}, {0}, PyBCList, BCBoundaryGroups);

  PrimalEquationSet PrimalEqSet3(xfld, qfld, lgfld, pde, quadratureOrder,
                                 ResidualNorm_InvJac_RelWeighted, tol, {0}, {0}, PyBCList, BCBoundaryGroups);

  std::vector<std::vector<Real>> rsdNrm2( PrimalEqSet2.residualNorm(rsd) );
  std::vector<std::vector<Real>> rsdNrm3( PrimalEqSet3.residualNorm(rsd) );

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Discretization/AlgebraicEquationSet_DGAdvective_Triangle_AD_pattern.txt", true );

  PrimalEqSet1.printDecreaseResidualFailure(rsdNrm1, output);
  PrimalEqSet2.printDecreaseResidualFailure(rsdNrm2, output);
  PrimalEqSet3.printDecreaseResidualFailure(rsdNrm3, output);
  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
