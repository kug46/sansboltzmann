// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianBoundaryTrace_LinearScalar_sansLG_DGBR2_AD_btest
// testing of trace-integral jacobian: advection-diffusion

#include <boost/mpl/list.hpp>

#include <boost/test/unit_test.hpp>
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

#include "Discretization/DG/JacobianBoundaryTrace_DGBR2.h"
#include "Discretization/DG/ResidualBoundaryTrace_DGBR2.h"
#include "Discretization/DG/SetFieldBoundaryTrace_DGBR2_LiftingOperator.h"
#include "Discretization/DG/IntegrandBoundaryTrace_LinearScalar_sansLG_DGBR2.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianBoundaryTrace_LinearScalar_sansLG_DGBR2_AD_test_suite )

typedef boost::mpl::list< SurrealS<1>, SurrealS<2> > Surreals;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( DGBR2_1D_4Line, SurrealClass, Surreals )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef DLA::MatrixS<1,PhysD1::D,MatrixQ> RowMatrixQ;

  typedef BCNDConvertSpace<PhysD1, BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_sansLG>> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;

  // PDE

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // BC

  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  // grid: X1
  XField1D xfld(4);

  // solution: P1
  for (int qorder = 1; qorder <= 3; qorder++ )
  {
    Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

    const int qDOF = qfld.nDOF();

    // solution data
    for (int i = 0; i < qDOF; i++)
      qfld.DOF(i) = sin(PI*i/((Real)qDOF));

    // lifting-operator: P1
    FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Legendre);

    // BR2 discretization
    Real viscousEtaParameter = 2;
    DiscretizationDGBR2 disc(0, viscousEtaParameter);

    // Compute the field of inverse mass matrices
    FieldDataInvMassMatrix_Cell mmfld(qfld);

    // integrand
    IntegrandClass fcn( pde, bc, {0,1}, disc );

    // quadrature rule
    std::vector<int> quadratureOrder = {0,0};

    // jacobian via FD w/ residual operator; assumes scalar PDE
    // topology-specific single group interface

    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
    DLA::MatrixD<Real> jacPDE_q(qDOF,qDOF);

    rsdPDEGlobal0 = 0;

    // compute the lifting operator on the BC
    IntegrateBoundaryTraceGroups<TopoD1>::integrate( SetFieldBoundaryTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    // compute the BC residual
    IntegrateBoundaryTraceGroups<TopoD1>::integrate( ResidualBoundaryTrace_DGBR2(fcn, rsdPDEGlobal0),
                                                     xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size() );

    // jacobian wrt q
    for (int j = 0; j < qDOF; j++)
    {
      qfld.DOF(j) += 1;

      rsdPDEGlobal1 = 0;

      // compute the perturbed lifting operator on the BC
      IntegrateBoundaryTraceGroups<TopoD1>::integrate(SetFieldBoundaryTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

      // compute the perturbed BC residual
      IntegrateBoundaryTraceGroups<TopoD1>::integrate( ResidualBoundaryTrace_DGBR2(fcn, rsdPDEGlobal1),
                                                       xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size() );

      qfld.DOF(j) -= 1;

      for (int i = 0; i < qDOF; i++)
        jacPDE_q(i,j) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    }

#if 0
    cout << "FD: jacPDE_q  = " << endl;  jacPDE_q.dump(2);
#endif

    // jacobian via Surreal
    // topology-specific single group interface

    FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld); jacPDE_R = 0; // Lifting operator jacobian from cell integral

    DLA::MatrixD<MatrixQ> mtxGlobPDE_q(qDOF,qDOF);

    mtxGlobPDE_q = 0;

    IntegrateBoundaryTraceGroups<TopoD1>::integrate(
        JacobianBoundaryTrace_DGBR2<SurrealClass>(fcn, jacPDE_R, mtxGlobPDE_q),
        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size() );

#if 0
    cout << "Surreal: mtxGlobPDE_q  = " << endl;  mtxGlobPDE_q.dump(2);
#endif

    const Real small_tol = 5e-12;
    const Real close_tol = 5e-10;

    for (int i = 0; i < qDOF; i++)
      for (int j = 0; j < qDOF; j++)
        SANS_CHECK_CLOSE( jacPDE_q(i,j), mtxGlobPDE_q(i,j), small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( DGBR2_2D_Box_UnionJack_Triangle, SurrealClass, Surreals )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,PhysD2::D,MatrixQ> RowMatrixQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_sansLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.348;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // BC

  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  // grid: X1
  XField2D_Box_UnionJack_Triangle_X1 xfld(2, 2);

  // solution: P1
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  const int qDOF = qfld.nDOF();

  // solution data
  for (int i = 0; i < qDOF; i++)
    qfld.DOF(i) = sin(PI*i/((Real)qDOF));

  // lifting operator: P1
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // Compute the field of inverse mass matrices
  FieldDataInvMassMatrix_Cell mmfld(qfld);

  // integrand
  IntegrandClass fcn( pde, bc, {0,1,2,3}, disc );

  // quadrature rule (linear: basis is linear, solution is linear)
  std::vector<int> quadratureOrder = {2,2,2,2};

  // jacobian via FD w/ residual operator; assumes scalar PDE
  // topology-specific single group interface

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
  DLA::MatrixD<Real> jacPDE_q(qDOF,qDOF);

  rsdPDEGlobal0 = 0;

  // compute the lifting operator on the BC
  IntegrateBoundaryTraceGroups<TopoD2>::integrate( SetFieldBoundaryTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                  xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

  // compute the BC residual
  IntegrateBoundaryTraceGroups<TopoD2>::integrate( ResidualBoundaryTrace_DGBR2(fcn, rsdPDEGlobal0),
                                                   xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size() );

  // jacobian wrt q
  for (int j = 0; j < qDOF; j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;

    // compute the perturbed lifting operator on the BC
    IntegrateBoundaryTraceGroups<TopoD2>::integrate(SetFieldBoundaryTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    // compute the perturbed BC residual
    IntegrateBoundaryTraceGroups<TopoD2>::integrate( ResidualBoundaryTrace_DGBR2(fcn, rsdPDEGlobal1),
                                                     xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size() );

    qfld.DOF(j) -= 1;

    for (int i = 0; i < qDOF; i++)
      jacPDE_q(i,j) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
  }

#if 0
  cout << "FD: jacPDE_q  = ";  jacPDE_q.dump(2);  cout << endl;
#endif

  // jacobian via Surreal
  // topology-specific single group interface

  FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld); jacPDE_R = 0; // Lifting operator jacobian from cell integral

  DLA::MatrixD<MatrixQ> mtxGlobPDE_q(qDOF,qDOF);

  mtxGlobPDE_q = 0;

  IntegrateBoundaryTraceGroups<TopoD2>::integrate(
      JacobianBoundaryTrace_DGBR2<SurrealClass>(fcn, jacPDE_R, mtxGlobPDE_q),
      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size() );

#if 0
  cout << "Surreal: mtxGlobPDE_q  = " << endl;  mtxGlobPDE_q.dump(2);
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-11;

  for (int i = 0; i < qDOF; i++)
    for (int j = 0; j < qDOF; j++)
      SANS_CHECK_CLOSE( jacPDE_q(i,j), mtxGlobPDE_q(i,j), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( DGBR2_3D_Box_Tet, SurrealClass, Surreals )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None > PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,PhysD3::D,MatrixQ> RowMatrixQ;

  typedef BCAdvectionDiffusion<PhysD3,BCTypeLinearRobin_sansLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD3, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.7;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123; Real kxy = 0.553; Real kxz = 0.760;
  Real kyx = 0.343; Real kyy = 1.007; Real kyz = 0.870;
  Real kzx = 0.690; Real kzy = 0.365; Real kzz = 1.460;

  ViscousFlux3D_Uniform visc(kxx, kxy, kxz,
                                 kyx, kyy, kyz,
                                 kzx, kzy, kzz);

  Source3D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  // BC
  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  // grid: X1
  XField3D_Box_Tet_X1 xfld(comm, 1, 1, 1);

  // solution: P1
  int qorder = 1;
  Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  const int qDOF = qfld.nDOF();

  // solution data
  for (int i = 0; i < qDOF; i++)
    qfld.DOF(i) = sin(PI*i/((Real)qDOF));

  // lifting operator: P1
  FieldLift_DG_Cell<PhysD3, TopoD3, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // Compute the field of inverse mass matrices
  FieldDataInvMassMatrix_Cell mmfld(qfld);

  // integrand
  IntegrandClass fcn( pde, bc, {0,1,2,3,4,5}, disc );

  // quadrature rule (linear: basis is linear, solution is linear)
  std::vector<int> quadratureOrder = {2,2,2,2,2,2};

  // jacobian via FD w/ residual operator; assumes scalar PDE
  // topology-specific single group interface

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
  DLA::MatrixD<Real> jacPDE_q(qDOF,qDOF);

  rsdPDEGlobal0 = 0;

  // compute the lifting operator on the BC
  IntegrateBoundaryTraceGroups<TopoD3>::integrate( SetFieldBoundaryTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                  xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

  // compute the BC residual
  IntegrateBoundaryTraceGroups<TopoD3>::integrate( ResidualBoundaryTrace_DGBR2(fcn, rsdPDEGlobal0),
                                                      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size() );

  // jacobian wrt q
  for (int j = 0; j < qDOF; j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;

    // compute the perturbed lifting operator on the BC
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(SetFieldBoundaryTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    // compute the perturbed BC residual
    IntegrateBoundaryTraceGroups<TopoD3>::integrate( ResidualBoundaryTrace_DGBR2(fcn, rsdPDEGlobal1),
                                                        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size() );

    qfld.DOF(j) -= 1;

    for (int i = 0; i < qDOF; i++)
      jacPDE_q(i,j) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
  }

#if 0
  cout << "FD: jacPDE_q  = ";  jacPDE_q.dump(2);  cout << endl;
#endif

  // jacobian via Surreal
  // topology-specific single group interface

  FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld); jacPDE_R = 0; // Lifting operator jacobian from cell integral

  DLA::MatrixD<MatrixQ> mtxGlobPDE_q(qDOF,qDOF);

  mtxGlobPDE_q = 0;

  IntegrateBoundaryTraceGroups<TopoD3>::integrate(
      JacobianBoundaryTrace_DGBR2<SurrealClass>(fcn, jacPDE_R, mtxGlobPDE_q),
      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size() );

#if 0
  cout << "Surreal: mtxGlobPDE_q  = " << endl;  mtxGlobPDE_q.dump(2);
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 2e-9;

  for (int i = 0; i < qDOF; i++)
    for (int j = 0; j < qDOF; j++)
      SANS_CHECK_CLOSE( jacPDE_q(i,j), mtxGlobPDE_q(i,j), small_tol, close_tol );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
