// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianTranspose_DGBR2_Triangle_AD_btest
// testing computing the transposed Jacobian of 2-D DG-BR2 with Advection-Diffusion on triangles

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include <fstream>
#include <string>

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/SurrealS.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "Discretization/DG/IntegrandCell_DGBR2.h"
#include "Discretization/DG/IntegrandInteriorTrace_DGBR2.h"

#include "Discretization/DG/ResidualCell_DGBR2.h"
#include "Discretization/DG/ResidualInteriorTrace_DGBR2.h"
#include "Discretization/Galerkin/ResidualBoundaryTrace_mitLG_Galerkin.h"

#include "Discretization/DG/JacobianCell_DGBR2.h"
#include "Discretization/DG/JacobianInteriorTrace_DGBR2.h"
#include "Discretization/Galerkin/JacobianBoundaryTrace_mitLG_Galerkin.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"

#include "Field/Element/ElementProjection_L2.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianTranspose_DGBR2_Triangle_AD_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( X1Q1R1LG1 )
{
  const int D = PhysD2::D;

  typedef SurrealS<1> SurrealClass;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,D,MatrixQ> RowMatrixQ;

  typedef ScalarFunction2D_SineSine SolutionExact;

  typedef BCTypeFunctionLinearRobin_mitLG BCType;
  typedef BCNDConvertSpace<PhysD2, BCAdvectionDiffusion<PhysD2,BCType>> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandTraceClass;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandBCClass;

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.348;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real a = 0.38, b = -0.67, c = -0.91;
  Source2D_UniformGrad source(a, b, c);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // BC
  std::shared_ptr<SolutionExact> solnExact( new SolutionExact );

  BCClass bc( solnExact, visc );

  // grid: HierarchicalP1 (aka X1)

  int ii = 2;
  int jj = 2;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  // solution: Legendre P1

  int order = 1;

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0;

  const int nDOFPDE = qfld.nDOF();

  const FieldDataInvMassMatrix_Cell mmfld(qfld); // Inverse mass matrix field

#if 0
  cout << "btest: dumping qfld" << endl;  qfld.dump(2);
#endif

  // Lifting operators:

  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Legendre);
  rfld = 0;

  // Lagrange multiplier: Legendre P1

#if 1
  order = 1;
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical );
#elif 0
  order = 1;
  QField2D_DG_BoundaryEdge<PDEClass> lgfld( xfld, order, BasisFunctionCategory_Legendre );
#else
  order = 1;
  std::vector<std::vector<int>> boundaryGroupSets = {{0},{1},{2},{3}};
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(boundaryGroupSets, xfld, order, BasisFunctionCategory_Hierarchical );
#endif

  const int nDOFBC = lgfld.nDOF();
  lgfld = 0;

#if 0
  cout << "btest: dumping lgfld" << endl;  lgfld.dump(2);
#endif

  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrands
  IntegrandCellClass fcn( pde, disc, {0} );
  IntegrandTraceClass fcnTrace( pde, disc, {0} );
  IntegrandBCClass fcnBC( pde, bc, {0,1,2,3} );

  // quadrature rule
  int quadratureOrder[4] = {-1, -1, -1, -1};    // max
  int quadratureOrderMin[4] = {0, 0, 0, 0};     // min

  // linear system setup

  typedef SLA::SparseMatrix_CRS<MatrixQ>  SparseMatrixClass;
  typedef DLA::MatrixD<SparseMatrixClass> SystemMatrixClass;

  // jacobian nonzero pattern
  //
  //           q  lg
  //   PDE     X   X
  //   BC      X   0

  typedef SLA::SparseNonZeroPattern<MatrixQ> NonZeroPatternClass;

  DLA::MatrixD<NonZeroPatternClass> nz =
      {{ {nDOFPDE, nDOFPDE}, {nDOFPDE, nDOFBC} },
       { { nDOFBC, nDOFPDE}, { nDOFBC, nDOFBC} }};

  NonZeroPatternClass& nzPDE_q  = nz(0,0);
  NonZeroPatternClass& nzPDE_lg = nz(0,1);

  NonZeroPatternClass& nzBC_q   = nz(1,0);
  NonZeroPatternClass& nzBC_lg  = nz(1,1);

  FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld); // Lifting operator jacobian from cell integral

  IntegrateCellGroups<TopoD2>::integrate( JacobianCell_DGBR2(fcn, nzPDE_q, jacPDE_R),
                                          xfld, (qfld, rfld), quadratureOrderMin, xfld.nCellGroups() );

  IntegrateInteriorTraceGroups<TopoD2>::integrate(
      JacobianInteriorTrace_DGBR2(fcnTrace, mmfld, jacPDE_R, nzPDE_q),
      xfld, (qfld, rfld), quadratureOrderMin, xfld.nInteriorTraceGroups() );

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      JacobianBoundaryTrace_mitLG_Galerkin<SurrealClass>(fcnBC, nzPDE_q, nzPDE_lg, nzBC_q, nzBC_lg),
      xfld, qfld, lgfld, quadratureOrderMin, xfld.nBoundaryTraceGroups() );


  SystemMatrixClass jac(nz);

  SparseMatrixClass& jacPDE_q  = jac(0,0);
  SparseMatrixClass& jacPDE_lg = jac(0,1);

  SparseMatrixClass& jacBC_q   = jac(1,0);
  SparseMatrixClass& jacBC_lg  = jac(1,1);

  jac = 0;

  IntegrateCellGroups<TopoD2>::integrate( JacobianCell_DGBR2(fcn, jacPDE_q, jacPDE_R),
                                          xfld, (qfld, rfld), quadratureOrder, xfld.nCellGroups() );

  IntegrateInteriorTraceGroups<TopoD2>::integrate(
      JacobianInteriorTrace_DGBR2(fcnTrace, mmfld, jacPDE_R, jacPDE_q),
      xfld, (qfld, rfld), quadratureOrder, xfld.nInteriorTraceGroups() );

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      JacobianBoundaryTrace_mitLG_Galerkin<SurrealClass>(fcnBC, jacPDE_q, jacPDE_lg, jacBC_q, jacBC_lg),
      xfld, qfld, lgfld, quadratureOrder, xfld.nBoundaryTraceGroups() );

  // jacobian transpose

  //Create a transposed non-zero pattern as the lifting operators do not have a symmetric non-zero pattern
  DLA::MatrixD<NonZeroPatternClass> nzT(Transpose(nz));

  SystemMatrixClass jacT(nzT);

  auto jacTPDE_q  = Transpose(jacT)(0,0);
  auto jacTPDE_lg = Transpose(jacT)(0,1);

  auto jacTBC_q   = Transpose(jacT)(1,0);
  auto jacTBC_lg  = Transpose(jacT)(1,1);

  IntegrateCellGroups<TopoD2>::integrate( JacobianCell_DGBR2(fcn, jacTPDE_q, jacPDE_R),
                                          xfld, (qfld, rfld), quadratureOrder, xfld.nCellGroups() );

  IntegrateInteriorTraceGroups<TopoD2>::integrate(
      JacobianInteriorTrace_DGBR2(fcnTrace, mmfld, jacPDE_R, jacTPDE_q), // No need to transpose jacPDE_R
      xfld, (qfld, rfld), quadratureOrder, xfld.nInteriorTraceGroups() );

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      JacobianBoundaryTrace_mitLG_Galerkin<SurrealClass>(fcnBC, jacTPDE_q, jacTPDE_lg, jacTBC_q, jacTBC_lg),
      xfld, qfld, lgfld, quadratureOrder, xfld.nBoundaryTraceGroups() );

/*
  std::fstream jacfile("tmp/jacobian.mtx", std::ios::out);
  WriteMatrixMarketFile( jac, jacfile );

  std::fstream jacTfile("tmp/jacobianT.mtx", std::ios::out);
  WriteMatrixMarketFile( jacT, jacTfile );
*/

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-10;

  //Check that jacT is a transpose of jac
  for ( int i = 0; i < jac.m(); i++ )
    for ( int j = 0; j < jac.n(); j++ )
    {
      SparseMatrixClass& jacij = jac(i,j);
      SparseMatrixClass& jacTji = jacT(j,i);

      BOOST_REQUIRE_EQUAL(jacij.m(), jacTji.n());
      BOOST_REQUIRE_EQUAL(jacij.n(), jacTji.m());

      for ( int ib = 0; ib < jacij.m(); ib++ )
        for ( int jb = 0; jb < jacij.n(); jb++ )
        {
          //Only compare non-zero entries
          if ( !jacij.isNonZero(ib,jb) )
          {
            //Make sure the transpose is also a zero entry
            BOOST_CHECK( !jacTji.isNonZero(jb,ib) );
            continue;
          }
          //Check that the numbers are the same
          SANS_CHECK_CLOSE( jacij(ib,jb), jacTji(jb,ib), small_tol, close_tol );
        }
    }
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
