// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// SolutionData_DGBR2_btest
// testing of the SolutionData_DGBR2 class

#include <boost/test/unit_test.hpp>

#include "Discretization/DG/SolutionData_DGBR2.h"
#include "SANS_btest.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

using namespace std;


//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( SolutionData_DGBR2_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SolutionData_DGBR2_test )
{
  typedef AdvectiveFlux2D_Uniform AdvectiveFluxType;
  typedef ViscousFlux2D_Uniform ViscousFluxType;
  typedef Source2D_UniformGrad SourceType;

  typedef PDEAdvectionDiffusion<PhysD2, AdvectiveFluxType, ViscousFluxType, SourceType > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  int ii = 3;
  int jj = 3;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  int order = 1;

  // PDE
  Real a = 0.5;
  Real b = 1.0;
  Real nu = 0.0;
  AdvectiveFluxType adv( a, b );
  ViscousFluxType visc( nu, 0, 0, nu );
  SourceType source(0.0, 0.0, 0.0);

  // Field bundles need to be initialized with a disc now
  DiscretizationDGBR2 disc( 6.0);

  NDPDEClass pde( adv, visc, source );

  SolutionClass globalSol(xfld, pde, order, order+1,
                          BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical, {}, disc);

  BOOST_CHECK_EQUAL( &globalSol.pde, &pde);
  BOOST_CHECK_EQUAL( globalSol.primal.order, order);

  BOOST_CHECK_EQUAL( globalSol.primal.basis_cell, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( globalSol.primal.basis_trace, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( &globalSol.primal.qfld.getXField(), &xfld);
  BOOST_CHECK_EQUAL( &globalSol.primal.rfld.getXField(), &xfld);
  BOOST_CHECK_EQUAL( &globalSol.primal.lgfld.getXField(), &xfld);

  BOOST_CHECK_EQUAL( &globalSol.adjoint.qfld.getXField(), &xfld);
  BOOST_CHECK_EQUAL( &globalSol.adjoint.rfld.getXField(), &xfld);
  BOOST_CHECK_EQUAL( &globalSol.adjoint.lgfld.getXField(), &xfld);

  BOOST_CHECK_EQUAL( globalSol.primal.qfld.nDOF(), 2*ii*jj*3);
  BOOST_CHECK_EQUAL( globalSol.primal.rfld.nDOF(), 2*ii*jj*Triangle::NTrace*3);
  BOOST_CHECK_EQUAL( globalSol.primal.lgfld.nDOF(), 0);

  BOOST_CHECK_EQUAL( globalSol.adjoint.qfld.nDOF(), 2*ii*jj*6);
  BOOST_CHECK_EQUAL( globalSol.adjoint.rfld.nDOF(), 2*ii*jj*Triangle::NTrace*6);
  BOOST_CHECK_EQUAL( globalSol.adjoint.lgfld.nDOF(), 0);

  globalSol.setSolution(0.2);

  for (int i = 0; i < globalSol.primal.qfld.nDOF(); i++)
    SANS_CHECK_CLOSE( globalSol.primal.qfld.DOF(i), 0.2, small_tol, close_tol);

  SolutionClass globalSolFrom(xfld, pde, order, order+1,
                              BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical, {}, disc);

  globalSolFrom.setSolution(0.5);

  globalSol.setSolution(globalSolFrom);

  for (int i = 0; i < globalSol.primal.qfld.nDOF(); i++)
    SANS_CHECK_CLOSE( globalSol.primal.qfld.DOF(i), 0.5, small_tol, close_tol);

  //Create a lifted quantity field
  globalSol.createLiftedQuantityField(0, BasisFunctionCategory_Legendre);
  BOOST_CHECK_EQUAL( &globalSol.pliftedQuantityfld->getXField(), &xfld);
  BOOST_CHECK_EQUAL( globalSol.pliftedQuantityfld->nDOF(), 2*ii*jj);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
