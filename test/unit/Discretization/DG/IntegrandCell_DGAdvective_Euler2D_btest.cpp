// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_DGAdvective_Triangle_Euler2D_btest
// testing of 2-D cell element residual integrands for DG Advective: Euler

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementXFieldArea.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{

typedef QTypePrimitiveRhoPressure QType;
typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
template class IntegrandCell_Galerkin<NDPDEClass>::BasisWeighted<Real, TopoD2, Triangle, ElementXField<PhysD2, TopoD2, Triangle> >;

}



//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_DGAdvective_Euler2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass > NDPDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2, TopoD2, Triangle> CellXFieldClass;

  typedef ElementXField<PhysD2, TopoD2, Triangle> ElementXFieldClass;
  typedef Element<ArrayQ, TopoD2, Triangle> ElementQFieldClass;

  typedef IntegrandCell_Galerkin<NDPDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real, TopoD2, Triangle, ElementXField<PhysD2, TopoD2, Triangle> > BasisWeightedClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 4 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  CellXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  // solution

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0)(0) = 2.0;
  qfldElem.DOF(0)(1) = 6.0;
  qfldElem.DOF(0)(2) = 5.0;
  qfldElem.DOF(0)(3) = 23.0;

  qfldElem.DOF(1)(0) = 3.0;
  qfldElem.DOF(1)(1) = 2.0;
  qfldElem.DOF(1)(2) = 7.0;
  qfldElem.DOF(1)(3) = 42.0;

  qfldElem.DOF(2)(0) = 4.0;
  qfldElem.DOF(2)(1) = 3.0;
  qfldElem.DOF(2)(2) = 4.0;
  qfldElem.DOF(2)(3) = 87.0;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // integrand functor
  BasisWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem );

  BOOST_CHECK_EQUAL( 4, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordType sRef;
  Real integrandTrue[3][4] = {{0,0,0,0},{0,0,0,0},{0,0,0,0}};
  ArrayQ integrand[3] = {0,0,0};

  // Test at {0, 0}
  sRef = {0, 0};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective)
  // Basis function 1
  integrandTrue[0][0] = 22.0;
  integrandTrue[0][1] = 155.0;
  integrandTrue[0][2] = 133.0;
  integrandTrue[0][3] = 3113.0/2.0;

  // Basis function 2
  integrandTrue[1][0] = -12.0;
  integrandTrue[1][1] = -95.0;
  integrandTrue[1][2] = -60.0;
  integrandTrue[1][3] = -849.0;

  // Basis function 3
  integrandTrue[2][0] = -10.0;
  integrandTrue[2][1] = -60.0;
  integrandTrue[2][2] = -73.0;
  integrandTrue[2][3] = -1415.0/2.0;

  SANS_CHECK_CLOSE( integrandTrue[0][0], integrand[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[0][1], integrand[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[0][2], integrand[0][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[0][3], integrand[0][3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[1][0], integrand[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1][1], integrand[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1][2], integrand[1][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1][3], integrand[1][3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[2][0], integrand[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2][1], integrand[2][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2][2], integrand[2][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2][3], integrand[2][3], small_tol, close_tol );


// Test at {1, 0}
  //ArrayQ integrand2[3] = {0,0,0};
  sRef = {1, 0};
  fcn( sRef, integrand, 3 );
//
  //PDE residual integrands: (advective)
  // Basis function 1
  integrandTrue[0][0] = 27.0;
  integrandTrue[0][1] = 96.0;
  integrandTrue[0][2] = 231.0;
  integrandTrue[0][3] = 4077.0/2.0;

  // Basis function 2
  integrandTrue[1][0] = -6.0;
  integrandTrue[1][1] = -54.0;
  integrandTrue[1][2] = -42.0;
  integrandTrue[1][3] = -453.0;

  // Basis function 3
  integrandTrue[2][0] = -21.0;
  integrandTrue[2][1] = -42.0;
  integrandTrue[2][2] = -189.0;
  integrandTrue[2][3] = -3171.0/2.0;

  SANS_CHECK_CLOSE( integrandTrue[0][0], integrand[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[0][1], integrand[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[0][2], integrand[0][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[0][3], integrand[0][3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[1][0], integrand[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1][1], integrand[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1][2], integrand[1][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1][3], integrand[1][3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[2][0], integrand[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2][1], integrand[2][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2][2], integrand[2][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2][3], integrand[2][3], small_tol, close_tol );
//
//  // Test at {0, 1}

  sRef = {0, 1};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective)
  // Basis function 1
  integrandTrue[0][0] = 28.0;
  integrandTrue[0][1] = 171.0;
  integrandTrue[0][2] = 199.0;
  integrandTrue[0][3] = 4963.0/2.0;

  // Basis function 2
  integrandTrue[1][0] = -12.0;
  integrandTrue[1][1] = -123.0;
  integrandTrue[1][2] = -48.0;
  integrandTrue[1][3] = -2127.0/2.0;

  // Basis function 3
  integrandTrue[2][0] = -16.0;
  integrandTrue[2][1] = -48.0;
  integrandTrue[2][2] = -151.0;
  integrandTrue[2][3] = -1418;

  SANS_CHECK_CLOSE( integrandTrue[0][0], integrand[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[0][1], integrand[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[0][2], integrand[0][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[0][3], integrand[0][3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[1][0], integrand[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1][1], integrand[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1][2], integrand[1][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1][3], integrand[1][3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[2][0], integrand[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2][1], integrand[2][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2][2], integrand[2][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2][3], integrand[2][3], small_tol, close_tol );
//
//  // Test at {1/3, 1/3}

  sRef = {1./3., 1./3.};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective)
  // Basis function 1
  integrandTrue[0][0] = 27.0;
  integrandTrue[0][1] = 449.0/3.0;
  integrandTrue[0][2] = 584.0/3.0;
  integrandTrue[0][3] = 4323.0/2.0;

  // Basis function 2
  integrandTrue[1][0] = -11.0;
  integrandTrue[1][1] = -91.0;
  integrandTrue[1][2] = -176.0/3.0;
  integrandTrue[1][3] = -15851.0/18.0;

  // Basis function 3
  integrandTrue[2][0] = -16.0;
  integrandTrue[2][1] = -176.0/3.0;
  integrandTrue[2][2] = -136.0;
  integrandTrue[2][3] = -11528.0/9.0;

  SANS_CHECK_CLOSE( integrandTrue[0][0], integrand[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[0][1], integrand[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[0][2], integrand[0][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[0][3], integrand[0][3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[1][0], integrand[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1][1], integrand[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1][2], integrand[1][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1][3], integrand[1][3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[2][0], integrand[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2][1], integrand[2][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2][2], integrand[2][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2][3], integrand[2][3], small_tol, close_tol );

  // test the element integral of the functor

  // quadrature rule need higher quadrature for non-linear!
  int quadratureorder = 4;
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integral(quadratureorder, nIntegrand);

  ArrayQ rsdPDEElem[3] = {{0,0,0,0},{0,0,0,0},{0,0,0,0}};

  // cell integration for canonical element
  integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

  Real rsd[3][4];
  //PDE residual: (advection)
  rsd[0][0] = 40.0/3.0;
  rsd[0][1] = 1771.0/24.0;
  rsd[0][2] = 771.0/8.0;
  rsd[0][3] = 76603.0/72.0;
  rsd[1][0] = -43.0/8.0;
  rsd[1][1] = -547.0/12.0;
  rsd[1][2] = -677.0/24.0;
  rsd[1][3] = -7727.0/18.0;
  rsd[2][0] = -191.0/24.0;
  rsd[2][1] = -677.0/24.0;
  rsd[2][2] = -409.0/6.0;
  rsd[2][3] = -45695.0/72.0;

  SANS_CHECK_CLOSE( rsd[0][0], rsdPDEElem[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[0][1], rsdPDEElem[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[0][2], rsdPDEElem[0][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[0][3], rsdPDEElem[0][3], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[1][0], rsdPDEElem[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[1][1], rsdPDEElem[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[1][2], rsdPDEElem[1][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[1][3], rsdPDEElem[1][3], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[2][0], rsdPDEElem[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[2][1], rsdPDEElem[2][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[2][2], rsdPDEElem[2][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[2][3], rsdPDEElem[2][3], small_tol, close_tol );


}

#if 0 //TODO:
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Quad_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<2,ArrayQ> VectorArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Quad> CellXFieldClass;

  typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Quad> ElementQFieldClass;
  typedef Element<VectorArrayQ,TopoD2,Quad> ElementRFieldClass;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD2,Quad> BasisWeightedClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  CellXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 4, xfldElem.nDOF() );

  // Quad grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 1;  y3 = 1;
  x4 = 0;  y4 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};
  xfldElem.DOF(3) = {x4, y4};

  // solution

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 4, qfldElem.nDOF() );

  // Quad solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 6;
  qfldElem.DOF(3) = 4;

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  BasisWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem, rfldElems );

  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 4, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordType sRef;
  Real integrandTrue[4] = {0,0,0,0};
  ArrayQ integrand[4] = {0,0,0,0};

  // Test at {0, 0}
  sRef = {0, 0};
  fcn( sRef, integrand, 4 );

  //PDE residual integrands: (advective) + (viscous) + (lifting)
  integrandTrue[0] = ( 13./10. ) + ( -1254./125. ) + ( -8529./250. );   // Basis function 1
  integrandTrue[1] = ( -11./10. ) + ( 1181./200. ) + ( 6253./250. );   // Basis function 2
  integrandTrue[2] = ( 0 ) + ( 0 ) + ( 0 );   // Basis function 3
  integrandTrue[3] = ( -1./5. ) + ( 4127./1000. ) + ( 1138./125. );   // Basis function 4

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  // Test at {1, 0}
  sRef = {1, 0};
  fcn( sRef, integrand, 4 );

  //PDE residual integrands: (advective) + (viscous) + (lifting)
  integrandTrue[0] = ( 33./10. ) + ( -1181./200. ) + ( -2833./100. );   // Basis function 1
  integrandTrue[1] = ( -27./10. ) + ( 889./500. ) + ( 1648./125. );   // Basis function 2
  integrandTrue[2] = ( -3./5. ) + ( 4127./1000. ) + ( 7573./500. );   // Basis function 3
  integrandTrue[3] = ( 0 ) + ( 0 ) + ( 0 );   // Basis function 4

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  // Test at {1, 1}
  sRef = {1, 1};
  fcn( sRef, integrand, 4 );

  //PDE residual integrands: (advective) + (viscous) + (lifting)
  integrandTrue[0] = ( 0 ) + ( 0 ) + ( 0 );   // Basis function 1
  integrandTrue[1] = ( 6./5. ) + ( -4127./1000. ) + ( -397./100. );   // Basis function 2
  integrandTrue[2] = ( -39./5. ) + ( 1254./125. ) + ( 5631./250. );   // Basis function 3
  integrandTrue[3] = ( 33./5. ) + ( -1181./200. ) + ( -9277./500. );   // Basis function 4

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  // Test at {0, 1}
  sRef = {0, 1};
  fcn( sRef, integrand, 4 );

  //PDE residual integrands: (advective) + (viscous) + (lifting)
  integrandTrue[0] = ( 4./5. ) + ( -4127./1000. ) + ( -1717./500. );   // Basis function 1
  integrandTrue[1] = ( 0 ) + ( 0 ) + ( 0 );   // Basis function 2
  integrandTrue[2] = ( -22./5. ) + ( 1181./200. ) + ( -9973./500. );   // Basis function 3
  integrandTrue[3] = ( 18./5. ) + ( -889./500. ) + ( 1169./50. );   // Basis function 4

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  // Test at {1/2, 1/2}
  sRef = {1./2., 1./2.};
  fcn( sRef, integrand, 4 );

  //PDE residual integrands: (advective) + (viscous) + (lifting)
  integrandTrue[0] = ( 91./40. ) + ( -627./125. ) + ( -20901./2000. );   // Basis function 1
  integrandTrue[1] = ( -63./40. ) + ( 889./1000. ) + ( 2537./1000. );   // Basis function 2
  integrandTrue[2] = ( -91./40. ) + ( 627./125. ) + ( 20901./2000. );   // Basis function 3
  integrandTrue[3] = ( 63./40. ) + ( -889./1000. ) + ( -2537./1000. );   // Basis function 4

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );


  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );


  // test the element integral of the functor

  int quadratureorder = 2;
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Quad, ArrayQ> integral(quadratureorder, nIntegrand);

  ArrayQ rsdPDEElem[4] = {0,0,0,0};

  // cell integration for canonical element
  integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

  Real rsd[4];

  //PDE residual: (advection) + (diffusion) + (lifting)
  rsd[0] = (59./30.) + (-627./125.) + (-12457./1000.);   // Basis function 1
  rsd[1] = (-19./15.) + (889./1000.) + (9087./2000.);   // Basis function 2
  rsd[2] = (-31./12.) + (627./125.) + (2111./250.);   // Basis function 3
  rsd[3] = (113./60.) + (-889./1000.) + (-1061./2000.);   // Basis function 4

  SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[2], rsdPDEElem[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[3], rsdPDEElem[3], small_tol, close_tol );
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
