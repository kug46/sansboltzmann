// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ResidualInteriorTrace_DGBR2_AD_btest
// testing of trace residual functions for DG BR2 with Advection-Diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Discretization/DG/DiscretizationDGBR2.h"
#include "Discretization/DG/IntegrandInteriorTrace_DGBR2.h"
#include "Discretization/DG/ResidualInteriorTrace_DGBR2.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegratePeriodicTraceGroups.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Quad_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Quad_Lagrange_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ResidualInteriorTrace_DGBR2_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualInteriorTrace_DGBR2_2Line_X1Q1R1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 4);

  // line solution data (left)
  qfld.DOF(0) = 2;
  qfld.DOF(1) = 3;

  // line solution data (right)
  qfld.DOF(2) = 7;
  qfld.DOF(3) = 4;

  // lifting-operator: P1 (aka R1)
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( rfld.nDOF(), 8 );

  // lifting operators in left element
  rfld.DOF(0) = 3; //right edge
  rfld.DOF(1) = 2; //right edge
  rfld.DOF(2) = 0; //left edge
  rfld.DOF(3) = 0; //left edge

  // lifting operators in right element
  rfld.DOF(4) = 0; //right edge
  rfld.DOF(5) = 0; //right edge
  rfld.DOF(6) = 5; //left edge
  rfld.DOF(7) = 4; //left edge

  // BR2 discretization
  Real viscousEtaParameter = 2.0;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // quadrature rule (no quadrature required: value at a node)
  int quadratureOrder = 0;

  //PDE residuals (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  Real rsd1L = ( 0 ) + ( 0 ) + ( -2123./500. ) + ( 0 ); // Basis function 1
  Real rsd2L = ( 33./10. ) + ( 2123./1000. ) + ( 2123./500. ) + ( -14861./1000. ); // Basis function 2

  //PDE residuals (right): (advective) + (viscous) + (dual-consistent) + (lifting-operator))
  Real rsd1R = ( -33./10. ) + ( -2123./1000. ) + ( -2123./500. ) + ( 14861./1000. ); // Basis function 1
  Real rsd2R = ( 0 ) + ( 0 ) + ( 2123./500. ) + ( 0 ); // Basis function 2

  // PDE global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());

  rsdPDEGlobal = 0;

  // integrand
  IntegrandClass fcn( pde, disc, {0} );

  // base interface
  IntegrateInteriorTraceGroups<TopoD1>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal),
                                                  xfld, (qfld, rfld), &quadratureOrder, 1);

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( rsd1L, rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2L, rsdPDEGlobal[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsd1R, rsdPDEGlobal[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2R, rsdPDEGlobal[3], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualInteriorTrace_DGBR2_2Triangle_X1Q1R1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: P1 (aka X1)
  XField2D_2Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 6 );

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  // triangle solution data (right)
  qfld.DOF(3) = 7;
  qfld.DOF(4) = 2;
  qfld.DOF(5) = 9;

  // lifting-operator: P1 (aka R1)
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( rfld.nDOF(), 18 );

  // triangle auxiliary variable (left)
  rfld.DOF(0) = { 2, -3};
  rfld.DOF(1) = { 7,  8};
  rfld.DOF(2) = {-1,  7};
  rfld.DOF(3) = 0;
  rfld.DOF(4) = 0;
  rfld.DOF(5) = 0;
  rfld.DOF(6) = 0;
  rfld.DOF(7) = 0;
  rfld.DOF(8) = 0;

  // triangle auxiliary variable (right)
  rfld.DOF( 9) = { 8, -2};
  rfld.DOF(10) = {-5,  7};
  rfld.DOF(11) = { 3,  9};
  rfld.DOF(12) = 0;
  rfld.DOF(13) = 0;
  rfld.DOF(14) = 0;
  rfld.DOF(15) = 0;
  rfld.DOF(16) = 0;
  rfld.DOF(17) = 0;

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // quadrature rule (quadratic: basis & flux both linear)
  int quadratureOrder = 2;

  // PDE global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());

  Real rsdPDELTrue[3],rsdPDERTrue[3];

  rsdPDEGlobal = 0;

  // integrand
  IntegrandClass fcn( pde, disc, {0} );

  IntegrateInteriorTraceGroups<TopoD2>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal),
                                                  xfld, (qfld, rfld), &quadratureOrder, 1);

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  //PDE residuals (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  rsdPDELTrue[0] = ( 0 ) + ( 0 ) + ( -1059./250. ) + ( 0 );   // Basis function 1
  rsdPDELTrue[1] = ( 13./6. ) + ( -5073./1000. ) + ( 669./250. ) + ( -14043./250. );   // Basis function 2
  rsdPDELTrue[2] = ( 143./60. ) + ( -5073./1000. ) + ( 39./25. ) + ( -4053./125. );   // Basis function 3

  //PDE residuals (right): (advective) + (viscous) + (dual-consistent) + (lifting-operator))
  rsdPDERTrue[0] = ( 0 ) + ( 0 ) + ( 1059./250. ) + ( 0 );   // Basis function 1
  rsdPDERTrue[1] = ( -143./60. ) + ( 5073./1000. ) + ( -669./250. ) + ( 4053./125. );   // Basis function 2
  rsdPDERTrue[2] = ( -13./6. ) + ( 5073./1000. ) + ( -39./25. ) + ( 14043./250. );   // Basis function 3

  SANS_CHECK_CLOSE( rsdPDELTrue[0], rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDELTrue[1], rsdPDEGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDELTrue[2], rsdPDEGlobal[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdPDERTrue[0], rsdPDEGlobal[3], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue[1], rsdPDEGlobal[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue[2], rsdPDEGlobal[5], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualInteriorTrace_DGBR2_2Quad_AD )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;

  Real u = 1.1;
  Real v = 0.2;

  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123, kxy = 0.553;
  Real kyx = 0.789, kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy,
                             kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // grid: P1 (aka X1)
  XField2D_2Quad_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 8 );

  // Quad solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 6;
  qfld.DOF(3) = 4;

  // Quad solution data (right)
  qfld.DOF(4) = 7;
  qfld.DOF(5) = 2;
  qfld.DOF(6) = 5;
  qfld.DOF(7) = 9;

  // lifting-operator: P1 (aka R1)
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( rfld.nDOF(), 32 );

  // Quad auxiliary variable (left)
  rfld.DOF(0)  = 0;
  rfld.DOF(1)  = 0;
  rfld.DOF(2)  = 0;
  rfld.DOF(3)  = 0;
  rfld.DOF(4)  = { 2, -3};
  rfld.DOF(5)  = { 7,  8};
  rfld.DOF(6)  = { 4,  1};
  rfld.DOF(7)  = {-1,  7};
  rfld.DOF(8)  = 0;
  rfld.DOF(9)  = 0;
  rfld.DOF(10) = 0;
  rfld.DOF(11) = 0;
  rfld.DOF(12) = 0;
  rfld.DOF(13) = 0;
  rfld.DOF(14) = 0;
  rfld.DOF(15) = 0;

  // Quad auxiliary variable (right)
  rfld.DOF(16) = 0;
  rfld.DOF(17) = 0;
  rfld.DOF(18) = 0;
  rfld.DOF(19) = 0;
  rfld.DOF(20) = 0;
  rfld.DOF(21) = 0;
  rfld.DOF(22) = 0;
  rfld.DOF(23) = 0;
  rfld.DOF(24) = 0;
  rfld.DOF(25) = 0;
  rfld.DOF(26) = 0;
  rfld.DOF(27) = 0;
  rfld.DOF(28) = { 8, -2};
  rfld.DOF(29) = {-5,  7};
  rfld.DOF(30) = { 1,  6};
  rfld.DOF(31) = { 3,  9};

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // quadrature rule (quadratic: basis & flux both linear)
  int quadratureOrder[2] = {2};

  // PDE global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());

  Real rsdPDELTrue[4],rsdPDERTrue[4];

  rsdPDEGlobal = 0;

  // integrand
  IntegrandClass fcn( pde, disc, {0});

  IntegrateInteriorTraceGroups<TopoD2>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal),
                                                  xfld, (qfld, rfld), quadratureOrder, 1);

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  //PDE residuals (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  rsdPDELTrue[0] = ( 0 ) + ( 0 ) + ( -23353/12000. ) + ( 0 );   // Basis function 1
  rsdPDELTrue[1] = ( 11/5. ) + ( 8689/12000. ) + ( 212/375. ) + ( -90717/2000. );   // Basis function 2
  rsdPDELTrue[2] = ( 11/4. ) + ( 3283/6000. ) + ( 37799/12000. ) + ( -15189/400. );   // Basis function 3
  rsdPDELTrue[3] = ( 0 ) + ( 0 ) + ( -2123/1200. ) + ( 0 );   // Basis function 4

  //PDE residuals (right): (advective) + (viscous) + (dual-consistent) + (lifting-operator))
  rsdPDERTrue[0] = ( -11/5. ) + ( -8689/12000. ) + ( -19961/6000. ) + ( 90717/2000. );   // Basis function 1
  rsdPDERTrue[1] = ( 0 ) + ( 0 ) + ( 23353/12000. ) + ( 0 );   // Basis function 2
  rsdPDERTrue[2] = ( 0 ) + ( 0 ) + ( 2123/1200. ) + ( 0 );   // Basis function 3
  rsdPDERTrue[3] = ( -11/4. ) + ( -3283/6000. ) + ( -4661/12000. ) + ( 15189/400. );   // Basis function 4

  SANS_CHECK_CLOSE( rsdPDELTrue[0], rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDELTrue[1], rsdPDEGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDELTrue[2], rsdPDEGlobal[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDELTrue[3], rsdPDEGlobal[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdPDERTrue[0], rsdPDEGlobal[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue[1], rsdPDEGlobal[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue[2], rsdPDEGlobal[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue[3], rsdPDEGlobal[7], small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualInteriorTrace_DGBR2_2Quad_Periodic )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;

  // global communicator
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  Real u = 1.1;
  Real v = 0.2;

  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123, kxy = 0.553;
  Real kyx = 0.789, kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy,
                             kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // grid: P1 (aka X1)
  Real xmin = 0, xmax = 2;
  Real ymin = 0, ymax = 1;
  int ii = 2, jj = 1;
  XField2D_Box_Quad_Lagrange_X1 xfld(comm, ii, jj, xmin, xmax, ymin, ymax, {{true, false}});

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 8 );

  // Quad solution data (left)
  qfld.DOF(4) = 1;
  qfld.DOF(5) = 3;
  qfld.DOF(6) = 6;
  qfld.DOF(7) = 4;

  // Quad solution data (right)
  qfld.DOF(0) = 7;
  qfld.DOF(1) = 2;
  qfld.DOF(2) = 5;
  qfld.DOF(3) = 9;

  // lifting-operator: P1 (aka R1)
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( rfld.nDOF(), 32 );

  // Quad auxiliary variable (left)
  rfld.DOF(16) = 0;
  rfld.DOF(17) = 0;
  rfld.DOF(18) = 0;
  rfld.DOF(19) = 0;
  rfld.DOF(20) = { 2, -3};
  rfld.DOF(21) = { 7,  8};
  rfld.DOF(22) = { 4,  1};
  rfld.DOF(23) = {-1,  7};
  rfld.DOF(24) = 0;
  rfld.DOF(25) = 0;
  rfld.DOF(26) = 0;
  rfld.DOF(27) = 0;
  rfld.DOF(28) = 0;
  rfld.DOF(29) = 0;
  rfld.DOF(30) = 0;
  rfld.DOF(31) = 0;

  // Quad auxiliary variable (right)
  rfld.DOF(0)  = 0;
  rfld.DOF(1)  = 0;
  rfld.DOF(2)  = 0;
  rfld.DOF(3)  = 0;
  rfld.DOF(4)  = 0;
  rfld.DOF(5)  = 0;
  rfld.DOF(6)  = 0;
  rfld.DOF(7)  = 0;
  rfld.DOF(8)  = 0;
  rfld.DOF(9)  = 0;
  rfld.DOF(10) = 0;
  rfld.DOF(11) = 0;
  rfld.DOF(12) = { 8, -2};
  rfld.DOF(13) = {-5,  7};
  rfld.DOF(14) = { 1,  6};
  rfld.DOF(15) = { 3,  9};

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // quadrature rule (quadratic: basis & flux both linear)
  int quadratureOrder[4] = {2, 2, 2, 2};

  // PDE global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());

  Real rsdPDELTrue[4],rsdPDERTrue[4];

  rsdPDEGlobal = 0;

  // integrand
  IntegrandClass fcn( pde, disc, {}, {xfld.iRight});

  IntegratePeriodicTraceGroups<TopoD2>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal),
                                                  xfld, (qfld, rfld), quadratureOrder, 4);

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  //PDE residuals (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  rsdPDELTrue[0] = ( 0 ) + ( 0 ) + ( -23353/12000. ) + ( 0 );   // Basis function 1
  rsdPDELTrue[1] = ( 11/5. ) + ( 8689/12000. ) + ( 212/375. ) + ( -90717/2000. );   // Basis function 2
  rsdPDELTrue[2] = ( 11/4. ) + ( 3283/6000. ) + ( 37799/12000. ) + ( -15189/400. );   // Basis function 3
  rsdPDELTrue[3] = ( 0 ) + ( 0 ) + ( -2123/1200. ) + ( 0 );   // Basis function 4

  //PDE residuals (right): (advective) + (viscous) + (dual-consistent) + (lifting-operator))
  rsdPDERTrue[0] = ( -11/5. ) + ( -8689/12000. ) + ( -19961/6000. ) + ( 90717/2000. );   // Basis function 1
  rsdPDERTrue[1] = ( 0 ) + ( 0 ) + ( 23353/12000. ) + ( 0 );   // Basis function 2
  rsdPDERTrue[2] = ( 0 ) + ( 0 ) + ( 2123/1200. ) + ( 0 );   // Basis function 3
  rsdPDERTrue[3] = ( -11/4. ) + ( -3283/6000. ) + ( -4661/12000. ) + ( 15189/400. );   // Basis function 4

  SANS_CHECK_CLOSE( rsdPDELTrue[0], rsdPDEGlobal[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDELTrue[1], rsdPDEGlobal[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDELTrue[2], rsdPDEGlobal[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDELTrue[3], rsdPDEGlobal[7], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdPDERTrue[0], rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue[1], rsdPDEGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue[2], rsdPDEGlobal[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDERTrue[3], rsdPDEGlobal[3], small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
