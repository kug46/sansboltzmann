// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ResiDualDArea_DGAdvective_Triangle_Euler2D_btest
// testing of 2-D area residual functions for DGAdvective with Euler

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Discretization/DG/DiscretizationDGBR2.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/Galerkin/ResidualCell_Galerkin.h"

#include "Discretization/IntegrateCellGroups.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"


using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ResidualCell_DGAdvective_Euler2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualCell_DGAdvective_1Triangle_X1Q1 )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass > NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef IntegrandCell_Galerkin<NDPDEClass> IntegrandClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 4 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  int nDOF = 3;

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_REQUIRE_EQUAL( nDOF, qfld.nDOF() );

// solution data
  qfld.DOF(0) = { 2.0, 6.0, 5.0, 23.0 };
  qfld.DOF(1) = { 3.0, 2.0, 7.0, 42.0 };
  qfld.DOF(2) = { 4.0, 3.0, 4.0, 87.0 };

  // max quadrature for nonlinear problem
  int quadratureOrder = 4;

  Real rsd[3][4];
  //PDE residual: (Euler advection)
  rsd[0][0] = 40.0/3.0;
  rsd[0][1] = 1771.0/24.0;
  rsd[0][2] = 771.0/8.0;
  rsd[0][3] = 76603.0/72.0;
  rsd[1][0] = -43.0/8.0;
  rsd[1][1] = -547.0/12.0;
  rsd[1][2] = -677.0/24.0;
  rsd[1][3] = -7727.0/18.0;
  rsd[2][0] = -191.0/24.0;
  rsd[2][1] = -677.0/24.0;
  rsd[2][2] = -409.0/6.0;
  rsd[2][3] = -45695.0/72.0;

  const Real tol = 1e-13;
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());

  rsdPDEGlobal = 0;

  // integrand
  IntegrandClass fcn( pde, {0} );

  // base interface
  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin(fcn, rsdPDEGlobal),
                                          xfld, qfld, &quadratureOrder, 1 );

  BOOST_CHECK_CLOSE( rsd[0][0], rsdPDEGlobal[0][0], tol );
  BOOST_CHECK_CLOSE( rsd[0][1], rsdPDEGlobal[0][1], tol );
  BOOST_CHECK_CLOSE( rsd[0][2], rsdPDEGlobal[0][2], tol );
  BOOST_CHECK_CLOSE( rsd[0][3], rsdPDEGlobal[0][3], tol );
  BOOST_CHECK_CLOSE( rsd[1][0], rsdPDEGlobal[1][0], tol );
  BOOST_CHECK_CLOSE( rsd[1][1], rsdPDEGlobal[1][1], tol );
  BOOST_CHECK_CLOSE( rsd[1][2], rsdPDEGlobal[1][2], tol );
  BOOST_CHECK_CLOSE( rsd[1][3], rsdPDEGlobal[1][3], tol );
  BOOST_CHECK_CLOSE( rsd[2][0], rsdPDEGlobal[2][0], tol );
  BOOST_CHECK_CLOSE( rsd[2][1], rsdPDEGlobal[2][1], tol );
  BOOST_CHECK_CLOSE( rsd[2][2], rsdPDEGlobal[2][2], tol );
  BOOST_CHECK_CLOSE( rsd[2][3], rsdPDEGlobal[2][3], tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
