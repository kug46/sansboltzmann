// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianInteriorTrace_DGBR2_TwoPhase_btest
// testing of interior trace jacobian: two-phase flow

#include <boost/mpl/list.hpp>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/PorousMedia/Q1DPrimitive_pnSw.h"
#include "pde/PorousMedia/PDETwoPhase1D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/SolutionFunction_TwoPhase1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/Element/ElementSequence.h"

#include "Discretization/DG/DiscretizationDGBR2.h"
#include "Discretization/DG/IntegrandInteriorTrace_DGBR2.h"
#include "Discretization/DG/SetFieldInteriorTrace_DGBR2_LiftingOperator.h"
#include "Discretization/DG/ResidualInteriorTrace_DGBR2.h"
#include "Discretization/DG/JacobianInteriorTrace_DGBR2.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianInteriorTrace_DGBR2_TwoPhase_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_TwoPhase1D_ST_Box_Triangle_X1 )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef QTypePrimitive_pnSw QType;
  typedef Q1D<QType, CapillaryModel, TraitsSizeTwoPhase> QInterpreter;

  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, Real, CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef PDENDConvertSpaceTime<PhysD1, PDEClass> NDPDEClass;

  typedef SolutionFunction_TwoPhase1D_3ConstSaturation<QInterpreter, TraitsSizeTwoPhase> SolutionType;
  typedef SolnNDConvertSpaceTime<PhysD1,SolutionType> NDSolutionType;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template MatrixQ<Real> MatrixQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,PhysD2::D,MatrixQ> RowMatrixQ;

  typedef IntegrandInteriorTrace_DGBR2<NDPDEClass> IntegrandClass;

  const int D = PhysD2::D;
  const int N = 2;

  std::vector<Real> step0 = {5e+1, 1e-1}; //first step-sizes for pressure and saturation
  std::vector<Real> step1 = {5e+0, 1e-2}; //second step-sizes for pressure and saturation


  const Real L = 2000.0; //ft
  const Real T = 1000.0; //days
  const Real Ls = 10.0;  //ft - well size

  // Grid
  std::vector<Real> xvec = {0, 500, 995, 1000, 1005, 1500, 2000};
  std::vector<Real> yvec = {0, 500, 1000};
  XField2D_Box_Triangle_X1 xfld(xvec, yvec, true);

  // PDE
  const Real pref = 14.7;

  DensityModel rhow(62.4, 5.0e-6, pref);
  DensityModel rhon(52.1, 1.5e-5, pref);

  PorosityModel phi(0.3, 3.0e-6, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  CapillaryModel pc(5);
  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;

  // Set up source term PyDicts
  PyDict well_fixedpressure;
  well_fixedpressure[SourceTwoPhase1DType_FixedWellPressure_Param::params.pB] = 2350.0;
  well_fixedpressure[SourceTwoPhase1DParam::params.Source.SourceType] = SourceTwoPhase1DParam::params.Source.FixedPressure;

  PyDict source_well;
  source_well[SourceTwoPhase1DParam::params.Source] = well_fixedpressure;
  source_well[SourceTwoPhase1DParam::params.xmin] = 0.5*(L - Ls);
  source_well[SourceTwoPhase1DParam::params.xmax] = 0.5*(L + Ls);
  source_well[SourceTwoPhase1DParam::params.Tmin] = 0;
  source_well[SourceTwoPhase1DParam::params.Tmax] = T;
  source_well[SourceTwoPhase1DParam::params.smoothLx] = 5.0;
  source_well[SourceTwoPhase1DParam::params.smoothT] = 0.0;

  PyDict source_list;
  source_list["production_well"] = source_well;

  NDPDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc, source_list);

  // static tests
  BOOST_CHECK( pde.D == D );
  BOOST_CHECK( pde.N == N );

  //Initial solution
  PyDict initSoln;
//  initSoln[BCClassSolution::ParamsType::params.Function.Name] = BCClassSolution::ParamsType::params.Function.ThreeConstSaturation;
  initSoln[SolutionType::ParamsType::params.pn] = 2500.0;
  initSoln[SolutionType::ParamsType::params.Sw_left] = 1.0;
  initSoln[SolutionType::ParamsType::params.Sw_mid] = 0.1;
  initSoln[SolutionType::ParamsType::params.Sw_right] = 1.0;
  initSoln[SolutionType::ParamsType::params.xLeftMid] = 0.25*L;
  initSoln[SolutionType::ParamsType::params.xMidRight] = 0.75*L;
  NDSolutionType NDinitSolution(initSoln, pde.variableInterpreter());

  // solution: P1
  for (int qorder = 1; qorder <= 2; qorder++)
  {
    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

    const int qDOF = qfld.nDOF();

    // set initial solution
    for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(NDinitSolution, {0}), (xfld, qfld) );

    // lifting-operator: P1
    FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

    // Compute the field of inverse mass matrices
    FieldDataInvMassMatrix_Cell mmfld(qfld);

    // BR2 discretization
    Real viscousEtaParameter = 3;
    DiscretizationDGBR2 disc(0, viscousEtaParameter);

    // integrand
    IntegrandClass fcn( pde, disc, {0} );

    // quadrature rule (2*P for basis and flux polynomials)
    std::vector<int> quadratureOrder = {2*qorder, 2*qorder, 2*qorder};

    // jacobian via FD w/ residual operator; assumes scalar PDE

    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
    DLA::MatrixD<MatrixQ> jacPDE_q_step0(qDOF,qDOF);
    DLA::MatrixD<MatrixQ> jacPDE_q_step1(qDOF,qDOF);

    // Compute the lifting operator field
    IntegrateInteriorTraceGroups<TopoD2>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    rsdPDEGlobal0 = 0;
    IntegrateInteriorTraceGroups<TopoD2>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal0),
                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

//    Real rsdNorm = 0.0;
//    for (int i = 0; i < rsdPDEGlobal0.m(); i++)
//      for (int j = 0; j < N; j++)
//        rsdNorm += pow(rsdPDEGlobal0[i][j],2);
//
//    std::cout<< "Global L2:" << std::setprecision(10) << std::scientific << rsdNorm << std::endl;

    // wrt q
    for (int j = 0; j < qDOF; j++)
    {
      for (int n = 0; n < N; n++)
      {
        //---------------Ping with large step size-------------------

        qfld.DOF(j)[n] += step0[n];

        // Compute the lifting operator field with perturbed q field
        IntegrateInteriorTraceGroups<TopoD2>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

        // Compute perturbed residuals
        rsdPDEGlobal1 = 0;
        IntegrateInteriorTraceGroups<TopoD2>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal1),
                                                        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

        qfld.DOF(j)[n] -= 2*step0[n];

        // Compute the lifting operator field with perturbed q field
        IntegrateInteriorTraceGroups<TopoD2>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

        rsdPDEGlobal0 = 0;
        IntegrateInteriorTraceGroups<TopoD2>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal0),
                                                        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

        qfld.DOF(j)[n] += step0[n];

        for (int i = 0; i < qDOF; i++)
          for (int m = 0; m < N; m++)
            jacPDE_q_step0(i,j)(m,n) = (rsdPDEGlobal1[i][m] - rsdPDEGlobal0[i][m])/(2.0*step0[n]);

        //---------------Ping with small step size-------------------

        qfld.DOF(j)[n] += step1[n];

        // Compute the lifting operator field with perturbed q field
        IntegrateInteriorTraceGroups<TopoD2>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

        // Compute perturbed residuals
        rsdPDEGlobal1 = 0;
        IntegrateInteriorTraceGroups<TopoD2>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal1),
                                                        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

        qfld.DOF(j)[n] -= 2*step1[n];

        // Compute the lifting operator field with perturbed q field
        IntegrateInteriorTraceGroups<TopoD2>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

        rsdPDEGlobal0 = 0;
        IntegrateInteriorTraceGroups<TopoD2>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal0),
                                                        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

        qfld.DOF(j)[n] += step1[n];

        for (int i = 0; i < qDOF; i++)
          for (int m = 0; m < N; m++)
            jacPDE_q_step1(i,j)(m,n) = (rsdPDEGlobal1[i][m] - rsdPDEGlobal0[i][m])/(2.0*step1[n]);
      }
    }

    // jacobian via Surreal

    FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld); jacPDE_R = 0; // Lifting operator jacobian from cell integral

    DLA::MatrixD<MatrixQ> mtxPDEGlob_q(qDOF,qDOF);

    mtxPDEGlob_q = 0;
    IntegrateInteriorTraceGroups<TopoD2>::integrate(
        JacobianInteriorTrace_DGBR2(fcn, mmfld, jacPDE_R, mtxPDEGlob_q),
        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    const Real small_tol = 5e-10;
    const Real close_tol = 1e-6;

    for (int i = 0; i < qDOF; i++)
      for (int j = 0; j < qDOF; j++)
        for (int m = 0; m < N; m++ )
          for (int n = 0; n < N; n++ )
          {
            Real error0 = fabs(jacPDE_q_step0(i,j)(m,n) - mtxPDEGlob_q(i,j)(m,n));
            Real error1 = fabs(jacPDE_q_step1(i,j)(m,n) - mtxPDEGlob_q(i,j)(m,n));

            if (error1 > small_tol)
            {
              Real rate = log(error1/error0) / log(step1[n]/step0[n]);

              BOOST_CHECK_MESSAGE( rate >= 1.5 && rate <= 5.0, "Rate check failed: rate = " << rate <<
                                   ", errors = [" << error0 << "," << error1 << "]" );
            }

            SANS_CHECK_CLOSE( jacPDE_q_step1(i,j)(m,n), mtxPDEGlob_q(i,j)(m,n), small_tol, close_tol );
          }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
