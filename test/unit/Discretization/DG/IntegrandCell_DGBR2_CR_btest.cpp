// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_DGBR2_Triangle_CR_btest
// testing of 2-D cell element residual integrands for DG BR2: Cauchy-Riemann

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/CauchyRiemann/PDECauchyRiemann2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementLift.h"

#include "Discretization/DG/IntegrandCell_DGBR2.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDENDConvertSpace<PhysD2, PDECauchyRiemann2D> PDEClass2D;
typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldTriangle;
template class IntegrandCell_DGBR2<PDEClass2D>::BasisWeighted_PDE<Real,Real,TopoD2,Triangle,ElementXFieldTriangle>;
template class IntegrandCell_DGBR2<PDEClass2D>::BasisWeighted_LO<Real,TopoD2,Triangle>;
}



//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_DGBR2_CR_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_2D_Triangle_test )
{
  typedef PDENDConvertSpace<PhysD2, PDECauchyRiemann2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> CellXFieldClass;
  typedef CellXFieldClass::RefCoordType RefCoordType;

  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;
  typedef ElementLift<VectorArrayQ,TopoD2,Triangle> ElementrFieldClass;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementRFieldClass;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldTriangle;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,Real,TopoD2,Triangle,ElementXFieldTriangle> BasisWeightedPDEClass;


  PDEClass pde;

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 2 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == false );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  CellXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  // solution

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = {1, 2};
  qfldElem.DOF(1) = {3, 4};
  qfldElem.DOF(2) = {5, 6};

  // lifting operators

  ElementrFieldClass rfldElems(order, BasisFunctionCategory_Hierarchical);

  rfldElems[0].DOF(0) = { 2, -3};  rfldElems[0].DOF(1) = { 7,  8};  rfldElems[0].DOF(2) = {-1,  7};
  rfldElems[1].DOF(0) = { 9,  6};  rfldElems[1].DOF(1) = {-1,  3};  rfldElems[1].DOF(2) = { 2,  3};
  rfldElems[2].DOF(0) = {-2,  1};  rfldElems[2].DOF(1) = { 4, -4};  rfldElems[2].DOF(2) = {-9, -5};

  // sum of lifting operators
  ElementRFieldClass RfldElem(order, BasisFunctionCategory_Hierarchical);

  RfldElem.vectorViewDOF() = rfldElems[0].vectorViewDOF() +
                             rfldElems[1].vectorViewDOF() +
                             rfldElems[2].vectorViewDOF();

  // BR2 discretization (not used)
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  BasisWeightedPDEClass fcnPDE = fcnint.integrand_PDE( xfldElem, qfldElem, RfldElem );

  BOOST_CHECK_EQUAL( 2, fcnPDE.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcnPDE.nDOF() );
  BOOST_CHECK( fcnPDE.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordType sRef;
  ArrayQ integrandTrue[3];
  ArrayQ integrandPDE[3];

  sRef = {0, 0};
  fcnPDE( sRef, integrandPDE, 3 );

  integrandTrue[0] = { 3, -1};
  integrandTrue[1] = {-1,  2};
  integrandTrue[2] = {-2, -1};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][k], integrandPDE[0][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][k], integrandPDE[1][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][k], integrandPDE[2][k], small_tol, close_tol );
  }


  sRef = {1, 0};
  fcnPDE( sRef, integrandPDE, 3 );

  integrandTrue[0] = { 7, -1};
  integrandTrue[1] = {-3,  4};
  integrandTrue[2] = {-4, -3};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][k], integrandPDE[0][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][k], integrandPDE[1][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][k], integrandPDE[2][k], small_tol, close_tol );
  }


  sRef = {0, 1};
  fcnPDE( sRef, integrandPDE, 3 );

  integrandTrue[0] = { 11, -1};
  integrandTrue[1] = { -5,  6};
  integrandTrue[2] = { -6, -5};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][k], integrandPDE[0][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][k], integrandPDE[1][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][k], integrandPDE[2][k], small_tol, close_tol );
  }


  sRef = {1./3., 1./3.};
  fcnPDE( sRef, integrandPDE, 3 );

  integrandTrue[0] = { 7, -1};
  integrandTrue[1] = {-3,  4};
  integrandTrue[2] = {-4, -3};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][k], integrandPDE[0][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][k], integrandPDE[1][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][k], integrandPDE[2][k], small_tol, close_tol );
  }


  // test the element integral of the functor

  // quadrature rule (for linear solution: basis grad is const, flux is linear)
  int quadratureorder = 2; //(integral of phi*lifting operator requires a quadratic quadrature order)
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integral(quadratureorder, nIntegrand);

  ArrayQ rsdPDEElem[3] = {0,0,0};

  // cell integration for canonical element
  integral( fcnPDE, xfldElem, rsdPDEElem, nIntegrand );

  ArrayQ rsd1 = { 3.5, -0.5};
  ArrayQ rsd2 = {-1.5,  2.0};
  ArrayQ rsd3 = {-2.0, -1.5};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( rsd1[k], rsdPDEElem[0][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd2[k], rsdPDEElem[1][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd3[k], rsdPDEElem[2][k], small_tol, close_tol );
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
