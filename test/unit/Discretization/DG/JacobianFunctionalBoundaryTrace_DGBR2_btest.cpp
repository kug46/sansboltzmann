// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianFunctionalBoundaryTrace_DGBR2_btest
// testing of 2-D functional boundary - output integral jacobian

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/list.hpp>

#include "tools/SANSnumerics.h"     // Real

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "Surreal/SurrealS.h"

#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/DG/SetFieldBoundaryTrace_DGBR2_LiftingOperator.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"
#include "Discretization/DG/FunctionalBoundaryTrace_DGBR2.h"
#include "Discretization/DG/JacobianFunctionalBoundaryTrace_DGBR2.h"

#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

#define SANS_CHECK_PING_ORDER( err_vec, delta_vec, small_tol ) \
    if (fabs(err_vec[1]) > small_tol) \
    { \
      Real rate = log(err_vec[1]/err_vec[0])/log(delta_vec[1]/delta_vec[0]); \
      BOOST_CHECK_MESSAGE( rate >= 1.9 && rate <= 4.0, "Rate check failed: rate = " << rate << \
                           ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" ); \
    }

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianFunctionalBoundaryTrace_DGBR2_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_P1_Linear_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef SurrealS<4> SurrealClass;

  // BC INTEGRAND
  typedef BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass> BCClass;
  typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, DGBR2> IntegrandBCClass;

  // DRAG INTEGRAND
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDOutputClass::template ArrayJ<Real> ArrayJ;
  typedef typename NDOutputClass::template MatrixJ<Real> MatrixJ;

  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> IntegrandOutputClass;

  // pde class
  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)
  const Real muRef = 1.0;        // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  NDBCClass bc(pde);
  IntegrandBCClass fcnBC( pde, bc, {0,1,2}, disc );

  NDOutputClass outputFcn(pde, 1., 0.);
  IntegrandOutputClass output_integrand( outputFcn, {0,1,2} );


  //GRID: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;
//  XField2D_Box_UnionJack_Triangle_X1 xfld(2,2);

  int qorder = 2;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> sfld(xfld, qorder, BasisFunctionCategory_Hierarchical);


  // quadrature rule
  std::vector<int> quadratureOrder(xfld.nBoundaryTraceGroups(), -1);

  // solution: P2

  BOOST_REQUIRE( qfld.nDOF() == 6 );

  qfld.DOF(0) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(1.0, 0.5, 0.6, 1.0));
  qfld.DOF(1) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(1.1, 0.3, 0.4, 2.0));
  qfld.DOF(2) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(1.2, 0.1, 0.2, 3.0));

  qfld.DOF(3) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(0.01, 0.03, 0.06, 0.1));
  qfld.DOF(4) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(0.02, 0.02, 0.04, 0.2));
  qfld.DOF(5) = pde.setDOFFrom(DensityVelocityPressure2D<Real>(0.03, 0.05, 0.02, 0.4));

  // Compute the field of inverse mass matrices
  FieldDataInvMassMatrix_Cell mmfld(qfld);


  // Set the lifting operator on the BC given qfld
  IntegrateBoundaryTraceGroups<TopoD2>::integrate(SetFieldBoundaryTrace_DGBR2_LiftingOperator(fcnBC, mmfld),
                                                  xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

  // Via Surreals

  DLA::VectorD<MatrixJ> jacFunctional_q( qfld.nDOF() );
  jacFunctional_q = 0;

  IntegrateBoundaryTraceGroups<TopoD2>::integrate(
      JacobianFunctionalBoundaryTrace_DGBR2<SurrealClass>( output_integrand, fcnBC, jacFunctional_q ),
      xfld, (qfld, rfld, sfld), quadratureOrder.data(), quadratureOrder.size() );

  // Via Finite Difference

  ArrayJ functional0, functional1;

  const Real small_tol = 1e-10;
  const Real close_tol = 6.5e-9;

  const Real eps = 1e-2;

  Real delta[2] = {10*eps, eps};

  for (int i = 0; i < qfld.nDOF(); i++)
    for (int n = 0; n < ArrayQ::M; n++)
    {
      Real err_Functional_q[2];
      Real fdFunctional_q = 0;

      for (int j = 0; j < 2; j++)
      {
        // Positive pertubation
        qfld.DOF(i)[n] += delta[j];

        // Set the lifting operator on the BC given the perturbed qfld
        IntegrateBoundaryTraceGroups<TopoD2>::integrate(SetFieldBoundaryTrace_DGBR2_LiftingOperator(fcnBC, mmfld),
                                                        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

        functional1 = 0;
        IntegrateBoundaryTraceGroups<TopoD2>::integrate(
            FunctionalBoundaryTrace_DGBR2( output_integrand, fcnBC, functional1 ),
            xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size() );

        // Negative pertubation
        qfld.DOF(i)[n] -= 2*delta[j];

        // Set the lifting operator on the BC given the perturbed qfld
        IntegrateBoundaryTraceGroups<TopoD2>::integrate(SetFieldBoundaryTrace_DGBR2_LiftingOperator(fcnBC, mmfld),
                                                        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

        functional0 = 0;
        IntegrateBoundaryTraceGroups<TopoD2>::integrate(
            FunctionalBoundaryTrace_DGBR2( output_integrand, fcnBC, functional0 ),
            xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size() );

        // Restore the perturbation
        qfld.DOF(i)[n] += delta[j];

        // functional is a Real
        fdFunctional_q = (functional1 - functional0)/(2*delta[j]);
        err_Functional_q[j] = fabs(fdFunctional_q - jacFunctional_q[i][n]);
      }

      //std::cout << "[" << i << "][" << n << "] " << (functional1 - functional0)/(2*delta[1]) << " " << jacFunctional_q[i][n] << std::endl;

      SANS_CHECK_PING_ORDER( err_Functional_q, delta, small_tol )
      SANS_CHECK_CLOSE( fdFunctional_q, jacFunctional_q[i][n], small_tol, close_tol );
    }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_P1_Parallel_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef SurrealS<4> SurrealClass;

  // BC INTEGRAND
  typedef BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass> BCClass;
  typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, DGBR2> IntegrandBCClass;

  // DRAG INTEGRAND
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDOutputClass::template MatrixJ<Real> MatrixJ;

  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> IntegrandOutputClass;

  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  // pde class
  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)
  const Real muRef = 1.0;        // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  NDBCClass bc(pde);
  IntegrandBCClass fcnBC( pde, bc, {0,1,2,3}, disc );

  NDOutputClass outputFcn(pde, 0.25, 0.35);
  IntegrandOutputClass output_integrand( outputFcn, {0,1} );

  int ii = 5;
  int jj = 4;

  int qorder = 2;

  // grid parallel (partitioned across processors)
  XField2D_Box_Triangle_Lagrange_X1 xfld_parallel(world, ii, jj);
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_parallel(xfld_parallel, qorder, BasisFunctionCategory_Hierarchical);
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld_parallel(xfld_parallel, qorder, BasisFunctionCategory_Hierarchical);
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> sfld_parallel(xfld_parallel, qorder, BasisFunctionCategory_Hierarchical);

  // grid serial (complete grid on all processors)
  XField2D_Box_Triangle_Lagrange_X1 xfld_serial(comm, ii, jj);
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_serial(xfld_serial, qorder, BasisFunctionCategory_Hierarchical);
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld_serial(xfld_serial, qorder, BasisFunctionCategory_Hierarchical);
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> sfld_serial(xfld_serial, qorder, BasisFunctionCategory_Hierarchical);

  sfld_parallel = 0;
  sfld_serial = 0;

  // quadrature rule
  std::vector<int> quadratureOrder(xfld_serial.nBoundaryTraceGroups(), 3*qorder+1);

  // solution

  ArrayQ q = pde.setDOFFrom(DensityVelocityPressure2D<Real>(1.0, 0.5, 0.6, 1.0));
  qfld_parallel = q;
  qfld_serial = q;

  // Compute the field of inverse mass matrices
  FieldDataInvMassMatrix_Cell mmfld_parallel(qfld_parallel);
  FieldDataInvMassMatrix_Cell mmfld_serial(qfld_serial);


  // Set the lifting operator on the BC given qfld
  IntegrateBoundaryTraceGroups<TopoD2>::integrate(SetFieldBoundaryTrace_DGBR2_LiftingOperator(fcnBC, mmfld_parallel),
                                                  xfld_parallel, (qfld_parallel, rfld_parallel), quadratureOrder.data(), quadratureOrder.size());

  IntegrateBoundaryTraceGroups<TopoD2>::integrate(SetFieldBoundaryTrace_DGBR2_LiftingOperator(fcnBC, mmfld_serial),
                                                  xfld_serial, (qfld_serial, rfld_serial), quadratureOrder.data(), quadratureOrder.size());

  DLA::VectorD<MatrixJ> jacFunctional_q_parallel( qfld_parallel.nDOFpossessed() );
  jacFunctional_q_parallel = 0;

  DLA::VectorD<MatrixJ> jacFunctional_q_serial( qfld_serial.nDOFpossessed() );
  jacFunctional_q_serial = 0;

  // compute the parallel jacobian
  IntegrateBoundaryTraceGroups<TopoD2>::integrate(
      JacobianFunctionalBoundaryTrace_DGBR2<SurrealClass>( output_integrand, fcnBC, jacFunctional_q_parallel ),
      xfld_parallel, (qfld_parallel, rfld_parallel, sfld_parallel), quadratureOrder.data(), quadratureOrder.size() );

  // compute the serial jacobian
  IntegrateBoundaryTraceGroups<TopoD2>::integrate(
      JacobianFunctionalBoundaryTrace_DGBR2<SurrealClass>( output_integrand, fcnBC, jacFunctional_q_serial ),
      xfld_serial, (qfld_serial, rfld_serial, sfld_serial), quadratureOrder.data(), quadratureOrder.size() );


  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  for (int i = 0; i < jacFunctional_q_parallel.m(); i++)
  {
    // get the serial native index
    int is = qfld_parallel.local2nativeDOFmap(i);

    // the jacobians should match
    for (int n = 0; n < ArrayQ::M; n++)
      SANS_CHECK_CLOSE( jacFunctional_q_serial[is][n], jacFunctional_q_parallel[i][n], small_tol, close_tol );
  }

#if 0 // local2nativeDOFmap is does not yet exist for FieldLift_DG_Cell
  for (int i = 0; i < sfld_parallel.nDOFpossessed(); i++)
  {
    // get the serial native index
    int is = sfld_parallel.local2nativeDOFmap(i);

    // the sfld should match
    for (int d = 0; d < VectorArrayQ::M; d++)
      for (int n = 0; n < ArrayQ::M; n++)
        SANS_CHECK_CLOSE( sfld_serial.DOF(is)[d][n], sfld_parallel.DOF(i)[d][n], small_tol, close_tol );
  }
#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
