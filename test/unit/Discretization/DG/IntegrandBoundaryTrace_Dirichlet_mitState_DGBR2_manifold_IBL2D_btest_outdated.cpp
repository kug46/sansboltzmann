// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandBoundaryTrace_DGBR2_manifold_IBL2D_btest
// testing of element boundary trace residual integrands for DGBR2_manifold: IBL2D

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/Tuple.h"

#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2_manifold.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementLift.h"
#include "Field/Tuple/ElementTuple.h"

#include "pde/IBL/PDEIBL2D.h"
#include "pde/IBL/BCIBL2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEIBL<PhysD2,VarTypeDANCt> PDEClassIBL2D;
typedef PDENDConvertSpace<PhysD2,PDEClassIBL2D> NDPDEClassIBL2D;

typedef NDPDEClassIBL2D::template ArrayQ<Real> ArrayQ;
typedef PDEClassIBL2D::template ArrayParam<Real> ArrayParam;
typedef PDEClassIBL2D::template VectorX<Real> VectorX;

typedef BCIBL2D<BCTypeFullState,VarTypeDANCt> BCClassIBL2D;
typedef BCNDConvertSpace<PhysD2, BCClassIBL2D> NDBCClass2D;
typedef NDVectorCategory<boost::mpl::vector1<NDBCClass2D>, NDBCClass2D::Category> NDBCVecCat2D;

typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldClass;
typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;
typedef Element<ArrayParam,TopoD1,Line> ElementParamFieldClass;
typedef Element<VectorX,TopoD1,Line> ElementVFieldClass;

typedef MakeTuple<ElementTuple, ElementParamFieldClass,
                                ElementXFieldClass>::type ElementParamType;

template class IntegrandBoundaryTrace<NDPDEClassIBL2D, NDBCVecCat2D, DGBR2_manifold>::
BasisWeighted_PDE<Real, TopoD0, Node, TopoD1, Line, ElementParamType>;
template class IntegrandBoundaryTrace<NDPDEClassIBL2D, NDBCVecCat2D, DGBR2_manifold>::
BasisWeighted_LO<Real, Real, TopoD0, Node, TopoD1, Line, ElementParamType>;
}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandBoundaryTrace_Dirichlet_mitState_DGBR2_manifold_IBL2D_btest_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Line_X1Q1R1_IncompressibleLaminarWallProfile_test )
{
  // type definition
  typedef PhysD2 PhysDim;
  typedef VarData2DDANCt VarDataType;
  typedef VarTypeDANCt VarType;
  typedef VarInterpret2D<VarType> VarInterpType;
  typedef PDEIBL<PhysDim,VarType> PDEClass;
  typedef PDENDConvertSpace<PhysDim,PDEClass> NDPDEClass;
  /* default parameters
      const Real gam = 1.4;
      const Real R = 287.15;
      const Real mui = 1.827e-5;
      const Real Pr = 0.715;
   */
  NDPDEClass pde(VelocityProfile2D::LaminarBL);
  VarInterpType varInterpret;

  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::VectorS<PhysDim::D,VectorX> VectorVectorX;

  typedef BCIBL2D<BCTypeFullState,VarType> BCClassIBL2D;
  typedef BCNDConvertSpace<PhysDim, BCClassIBL2D > NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;
  typedef BCIBL2DParams<BCTypeFullState> BCParamsType;

  typedef ElementXField<PhysDim,TopoD0,Node> ElementXFieldTraceClass;
  typedef ElementXField<PhysDim,TopoD1,Line> ElementXFieldCellClass;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementRFieldClass;  // TODO: not ElementLift?

  typedef ElementXFieldTraceClass::RefCoordType RefCoordTraceType;

  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, DGBR2_manifold> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,TopoD0,Node,TopoD1,Line,ElementParamType> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO<Real,Real,TopoD0,Node,TopoD1,Line,ElementParamType> BasisWeightedLOClass;

  // BC

  PyDict bcArgs;
  bcArgs[NDBCClass::ParamsType::params.isFullState] = true;
  bcArgs[NDBCClass::ParamsType::params.delta] = 0.01;
  bcArgs[NDBCClass::ParamsType::params.A]  =  0.2;

  BCParamsType BCParamIBL2D;
  BCParamIBL2D.checkInputs(bcArgs);

  NDBCClass bc(pde,bcArgs);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 2 );
  BOOST_CHECK( bc.NBC == 2 );

  // ----------------------- ELEMENTAL FIELDS ----------------------- //
  const int order = 1;

  // Geometry (line grid)
  ElementXFieldCellClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  ElementXFieldTraceClass xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent line grid
  VectorX X1, X2;
  X1 = { 0.000, 0.000 };
  X2 = { 0.800, 0.600 };

  xfldElem.DOF(0) = X1;
  xfldElem.DOF(1) = X2;

  xnode.DOF(0) = X2;
  xnode.normalSignL() = 1;

  // Parameter: edge velocity field
  const Real qxi_L_0 = 4.000, qzi_L_0 = 2.000, p0i_L_0 = 1.100e+05, T0i_L_0 = 290.00;
  const Real qxi_L_1 = 3.000, qzi_L_1 = 4.000, p0i_L_1 = 1.000e+05, T0i_L_1 = 350.00;

  ElementQFieldClass vfldElem(order, BasisFunctionCategory_Hierarchical);

  vfldElem.DOF(0) = { qxi_L_0, qzi_L_0 };
  vfldElem.DOF(1) = { qxi_L_1, qzi_L_1 };

  // Parameter: edge velocity gradient fields
  VectorVectorX gradvL, gradvR;
  std::vector<VectorX> gradphiL(vfldElem.nDOF()), gradphiR(vfldElem.nDOF());

  xfldElem.evalBasisGradient( 0.0, xfldElem, gradphiL.data(), gradphiL.size() );
  xfldElem.evalBasisGradient( 1.0, xfldElem, gradphiR.data(), gradphiR.size() );

  vfldElem.evalFromBasis( gradphiL.data(), gradphiL.size(), gradvL );
  vfldElem.evalFromBasis( gradphiR.data(), gradphiR.size(), gradvR );

  // Parameter field
  ElementParamFieldClass paramElem(order, BasisFunctionCategory_Hierarchical);

  paramElem.DOF(0) = {qxi_L_0, qzi_L_0, gradvL[0][0], gradvL[1][0], gradvL[0][1], gradvL[1][1], p0i_L_0, T0i_L_0};
  paramElem.DOF(1) = {qxi_L_1, qzi_L_1, gradvR[0][0], gradvR[1][0], gradvR[0][1], gradvR[1][1], p0i_L_1, T0i_L_1};

  // Solution
  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // State vector at L element basis fcn 0
  const Real delta_0 = 0.1000;
  const Real A_0     = 3.0000;

  // State vector at L element basis fcn 1
  const Real delta_1 = 0.0800;
  const Real A_1     = 3.3000;

  // line solution (left)
  qfldElem.DOF(0) = pde.setDOFFrom( VarDataType(delta_0,A_0) );
  qfldElem.DOF(1) = pde.setDOFFrom( VarDataType(delta_1,A_1) );

  // lifting operators
  ElementRFieldClass rfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, rfldElem.order() );
  BOOST_CHECK_EQUAL( 2, rfldElem.nDOF() );

  rfldElem.DOF(0) = { 1.00, 2.00 };  // Basis fcn 1  { x, z }
  rfldElem.DOF(1) = { 3.00, 4.00 };  // Basis fcn 2  { x, z }

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand functor

  IntegrandClass fcnint( pde, bc, {0}, disc );

//  BOOST_CHECK( fcnint.needsFieldTrace() == false ); //TODO: ?

  //Explicitly create a paramater instance.
  //If it was created in the argument below, the temporary variable in the function argument would go out of scope and result in undefined behavior
  ElementParamType tupleElem = (paramElem, xfldElem);

  BasisWeightedPDEClass fcnPDE = fcnint.integrand_PDE( xnode, CanonicalTraceToCell(0, 1), tupleElem, qfldElem, rfldElem );
  BasisWeightedLOClass fcnLO = fcnint.integrand_LO( xnode, CanonicalTraceToCell(0, 1), tupleElem, qfldElem, rfldElem );

  BOOST_CHECK_EQUAL( 2, fcnPDE.nDOF() );
  BOOST_CHECK( fcnPDE.needsEvaluation() == true );

  BOOST_CHECK_EQUAL( 2, fcnLO.nDOF() );
  BOOST_CHECK( fcnLO.needsEvaluation() == true );

#if 0
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
#endif

  const int nBasis = 2;

  RefCoordTraceType sRef;
  ArrayQ integrandPDETrue[nBasis];
  VectorArrayQ integrandLiftTrue[nBasis];

  BasisWeightedPDEClass::IntegrandType integrandPDE[nBasis];
  BasisWeightedLOClass::IntegrandType integrandLO[nBasis];

  sRef = 0;
  fcnPDE( sRef, integrandPDE, nBasis );
  fcnLO( sRef, integrandLO, nBasis );

  //PDE residual integrands (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  integrandPDETrue[0] = 0; // Basis function 1
  integrandPDETrue[1] = 0;  // Basis function 2

  //LO residual integrands (left): (lifting-operator)
  integrandLiftTrue[0] = 0;  // Basis function 1
  integrandLiftTrue[1] = 0;  // Basis function 2

#if 0 // TODO: need to be refactored and completed
  for (int ibasis = 0; ibasis < nBasis; ibasis++)
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE( integrandPDETrue[ibasis][n], integrandPDE[ibasis][n], small_tol, close_tol );
    }
  }

  for ( int nbasis = 0; nbasis < nBasis; nbasis++ )
  {
    for ( int idim = 0; idim < pde.D; idim++ )
    {
      for ( int ieqn = 0; ieqn < pde.N; ieqn++ )
      {
        SANS_CHECK_CLOSE( integrandLiftTrue[nbasis][idim][ieqn], integrandLO[nbasis][idim][ieqn], small_tol, close_tol );
      }
    }
  }
#endif

  // test the trace element integral of the functor
  const int quadratureorder = 0;
  const int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD0, Node, BasisWeightedPDEClass::IntegrandType> integralPDE(quadratureorder, nIntegrand);
  GalerkinWeightedIntegral<TopoD0, Node, BasisWeightedLOClass::IntegrandType> integralLO(quadratureorder, nIntegrand);

  BasisWeightedPDEClass::IntegrandType rsdPDEElem[nBasis] = {0,0};
  BasisWeightedLOClass::IntegrandType rsdLOElem[nBasis] = {0,0};
  ArrayQ rsdPDETrue[nBasis];
  VectorArrayQ rsdLiftTrue[nBasis];

  // cell integration for canonical element
  integralPDE( fcnPDE, xnode, rsdPDEElem, nIntegrand );
  integralLO( fcnLO, xnode, rsdLOElem, nIntegrand );

  //PDE residuals (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  rsdPDETrue[0] = 0; // Basis function 1
  rsdPDETrue[1] = 0; // Basis function 2

  //LO residuals (left): (lifting-operator)
  rsdLiftTrue[0] = 0;   // Basis function 1
  rsdLiftTrue[1] = 0;   // Basis function 2

  // check
#if 0
  for (int ibasis = 0; ibasis < nBasis; ibasis++)
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE( rsdPDETrue[ibasis][n], rsdPDEElem[ibasis][n], small_tol, close_tol );
    }
  }

  for ( int nbasis = 0; nbasis < nBasis; nbasis++ )
  {
    for ( int idim = 0; idim < pde.D; idim++ )
    {
      for ( int ieqn = 0; ieqn < pde.N; ieqn++ )
      {
        SANS_CHECK_CLOSE( rsdLiftTrue[nbasis][idim][ieqn], rsdLOElem[nbasis][idim][ieqn], small_tol, close_tol );
      }
    }
  }
#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
