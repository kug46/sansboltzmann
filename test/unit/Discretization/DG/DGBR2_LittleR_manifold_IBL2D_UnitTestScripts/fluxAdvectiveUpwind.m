function [ f ] = fluxAdvectiveUpwind(e1L,varL,e1R,varR,Nrm,PDEparam)

%%
vardummy = varL;
varType = 'DAGVelcoityStagPT';
deltaL = varInterpret(varL,vardummy,vardummy,varType,'delta');
AL = varInterpret(varL,vardummy,vardummy,varType,'A');
q1L = varInterpret(varL,vardummy,vardummy,varType,'VelocityEdge');
p0L = varInterpret(varL,vardummy,vardummy,varType,'p0Edge');
T0L = varInterpret(varL,vardummy,vardummy,varType,'T0Edge');

qL = varInterpret(varL,vardummy,vardummy,varType,'SpeedEdge');
rhoL = varInterpret(varL,vardummy,vardummy,varType,'DensityEdge',PDEparam);

deltaR = varInterpret(varR,vardummy,vardummy,varType,'delta');
AR = varInterpret(varR,vardummy,vardummy,varType,'A');
q1R = varInterpret(varR,vardummy,vardummy,varType,'VelocityEdge');
p0R = varInterpret(varR,vardummy,vardummy,varType,'p0Edge');
T0R = varInterpret(varR,vardummy,vardummy,varType,'T0Edge');

qR = varInterpret(varR,vardummy,vardummy,varType,'SpeedEdge');
rhoR = varInterpret(varR,vardummy,vardummy,varType,'DensityEdge',PDEparam);

%% 
% left element
Re_d_L = rhoL * qL * deltaL / PDEparam.mui_;

secondaryVarL = ThicknessCoefficient(Re_d_L, deltaL, AL);
delta1sL = secondaryVarL.delta1s;
deltaqL = secondaryVarL.deltaq;
deltarhoL = secondaryVarL.deltarho;
theta11L = secondaryVarL.theta11;
theta1sL = secondaryVarL.theta1s;

mL = rhoL * deltarhoL;
ML = rhoL * delta1sL * q1L;
pL = ML - mL * q1L;
kL = rhoL * qL^2 * deltaqL;

PL = rhoL * theta11L * ( q1L * q1L' );
KL = rhoL * qL^2 * theta1sL * q1L;

PTdoteL = PL' * e1L;

% right element
Re_d_R = rhoR * qR * deltaR / PDEparam.mui_;

secondaryVarR = ThicknessCoefficient(Re_d_R, deltaR, AR);
delta1sR = secondaryVarR.delta1s;
deltaqR = secondaryVarR.deltaq;
deltarhoR = secondaryVarR.deltarho;
theta11R = secondaryVarR.theta11;
theta1sR = secondaryVarR.theta1s;

mR = rhoR * deltarhoR;
MR = rhoR * delta1sR * q1R;
pR = MR - mR * q1R;
kR = rhoR * qR^2 * deltaqR;

PR = rhoR * theta11R * ( q1R * q1R' );
KR = rhoR * qR^2 * theta1sR * q1R;

PTdoteR = PR' * e1R;

%%
uConsL = [pL'*e1L; kL; q1L(1); q1L(2); p0L; T0L];
uConsR = [pR'*e1R; kR; q1R(1); q1R(2); p0R; T0R];

f = zeros(6,1);

alpha = max( abs(qL), abs(qR) );

f(1) = 0.5 * (PTdoteL + PTdoteR)' * Nrm + 0.5 * alpha * ( uConsL(1) - uConsR(1));
f(2) = 0.5 * (KL + KR)' * Nrm + 0.5 * alpha * ( uConsL(2) - uConsR(2));

end
