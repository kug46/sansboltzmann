clear all; close all;

%% state vector
delta = 2.e-2;
A = -0.5;
qx = 4;  
qz = 3;  
p0 = 28.125;  
T0 = 4.6875;

var = [delta; A; qx; qz; p0; T0];
varx = 0 * var;
varz = 0 * var;

%% parameters
e1 = [0.6; 0.8]; % basis vector

if ( (norm(e1) - 1) > 100*eps )
    error('e1 is not normalized.')
else
end

param = struct;

param.gam_ = 2;
param.R_ = 2;
param.mui_ = 2E-5;
param.Pr_ = 0.715;

%%
varType = 'DAGVelcoityStagPT';
q1 = varInterpret(var,varx,varz,varType,'VelocityEdge');
p0 = varInterpret(var,varx,varz,varType,'p0Edge');
T0 = varInterpret(var,varx,varz,varType,'T0Edge');

q = varInterpret(var,varx,varz,varType,'SpeedEdge');
rho = varInterpret(var,varx,varz,varType,'DensityEdge',param);

%%
[ fx, fz ] = fluxAdvective(e1,var,param);

fprintf(sprintf('fxTrue = { %5.16e,\n', fx(1)));
for i = 2:5
    fprintf(sprintf('           %5.16e,\n', fx(i)));
end
fprintf(sprintf('           %5.16e };\n', fx(6)));

fprintf(sprintf('fzTrue = { %5.16e,\n', fz(1)));
for i = 2:5
    fprintf(sprintf('           %5.16e,\n', fz(i)));
end
fprintf(sprintf('           %5.16e };\n', fz(6)));
  