function [ integrand_source, integrand_fluxadvective ] ...
    = IntegrandCell(xi,Xxi,e1,e1x,e1z,var,varx,varz,param)

normal = param.normal;

% evaluate weighting function & gradient
Phi = basis(xi);
gradPhi = basisGrad(xi,Xxi,normal);

% evaluate advective flux terms
[ fx, fz ] = fluxAdvective(e1,var,param);

% evaluate source terms
sourceTerms = source(e1,e1x,e1z,var,varx,varz,param);

% evaluate integrand
integrand_source        = sourceTerms * Phi;
integrand_fluxadvective = -  [fx,fz] * gradPhi;

end