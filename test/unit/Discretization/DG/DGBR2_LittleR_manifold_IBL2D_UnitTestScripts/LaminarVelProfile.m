function [U,dUdetab] = LaminarVelProfile(etab,A)
% This function computes the viscous velocity profile at wall coordinates
% eta with parameter A.

Abar = scaleA(A);

f0 = 6*etab.^2 - 8*etab.^3 + 3*etab.^4;
df0detab = 6*2*etab  - 8*3*etab.^2 + 3*4*etab.^3;

f1 = etab - 3*etab.^2 + 3*etab.^3 - etab.^4;
df1detab = 1 - 3*2*etab + 3*3*etab.^2 - 4*etab.^3;

U = Abar * f1 + f0;
dUdetab = Abar * df1detab + df0detab;

end