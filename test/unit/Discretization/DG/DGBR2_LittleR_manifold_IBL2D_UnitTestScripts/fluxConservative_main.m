clear all; close all;

%% state vector
delta = 1.2;
A = 2.3;
qx = 4;  
qz = 3;  
p0 = 1.2E+5;  
T0 = 300;

var = [delta; A; qx; qz; p0; T0];
varx = 0 * var;
varz = 0 * var;

%% parameters
e1 = [0.6; 0.8]; % basis vector

param = struct;

param.gam_ = 1.4;
param.R_ = 287.15;
param.mui_ = 1.827E-5;
param.Pr_ = 0.715;

%%
varType = 'DAGVelcoityStagPT';
q1 = varInterpret(var,varx,varz,varType,'VelocityEdge');
p0 = varInterpret(var,varx,varz,varType,'p0Edge');
T0 = varInterpret(var,varx,varz,varType,'T0Edge');

q = varInterpret(var,varx,varz,varType,'SpeedEdge');
rho = varInterpret(var,varx,varz,varType,'DensityEdge',param);

%%
Re_d = rho * q * delta / param.mui_;

secondaryVar = ThicknessCoefficient(Re_d, delta, A);
delta1s = secondaryVar.delta1s;
deltaq = secondaryVar.deltaq;
deltarho = secondaryVar.deltarho;

m = rho * deltarho;
M = rho * delta1s * q1;
p = M - m * q1;
k = rho * q^2 * deltaq;

%%
uCons = [p'*e1; k; qx; qz; p0; T0];

fprintf(sprintf('uConsTrue = { %5.16e,\n', uCons(1)));
for i = 2:5
    fprintf(sprintf('              %5.16e,\n', uCons(i)));
end
fprintf(sprintf('              %5.16e };\n', uCons(6)));
  