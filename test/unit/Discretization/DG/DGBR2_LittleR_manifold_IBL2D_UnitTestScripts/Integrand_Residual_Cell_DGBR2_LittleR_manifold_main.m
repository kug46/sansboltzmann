clc; clear all; close all;

D = 2;      % physical dimension
N = 6;      % number of equations
nBasis = 2; % number of DOFs = basis fcns per element; linear element
nTrace = 2; % number of traces; line grids

%% parameters
param = struct;

% PDE
param.gam_ = 1.4;
param.R_ = 287.15;
param.mui_ = 1.827E-5;
param.Pr_ = 0.715;

% EIF parameters
qxi_L = 4;  qzi_L = 3;  p0i_L = 1.2E+5;  T0i_L = 300;
qxi_R = 8;  qzi_R = 5;  p0i_R = 1.25E+5; T0i_R = 320;

EIF_L = [ qxi_L; qzi_L; p0i_L; T0i_L ];
EIF_R = [ qxi_R; qzi_R; p0i_R; T0i_R ];

% elemental geometry
e1L = [ 0.8; 0.6 ];
e1R = e1L;

XL = [ 1.2;  2.2 ];
XR = XL + e1R;

param.normal = evalNormal(XL,XR);

%% lifting operators
rL = { 1*ones(N,1), 2*ones(N,1);
       4*ones(N,1), 3*ones(N,1) }; % basis fcn 1

rR = { 3*ones(N,1), 4*ones(N,1);
       2*ones(N,1), 1*ones(N,1) }; % basis fcn 2

%% state vectors & xz-derivatives
varL = [ 2E-2;   1.2; 3;   4;   1.3E+5;  290 ];
varR = [ 2.2E-2; 1.3; 5;   2;   1.4E+5;  350 ];

%% integrand
xi_array = [ 0; 0.6; 1 ];

for i = 1:length(xi_array)
    xi = xi_array(i);
    Xxi = evalFromDBasis(xi,XL,XR);
    
    EIF = evalFromBasis(xi,EIF_L,EIF_R);
    param.qxi = EIF(1);
    param.qzi = EIF(2);
    param.p0i = EIF(3);
    param.T0i = EIF(4);
        
    e1 = evalFromBasis(xi,e1L,e1R);
    e1X = evalFromBasisGrad(xi,e1L,e1R,Xxi,param.normal);
    e1x = e1X(:,1);  e1z = e1X(:,2);
    
    integrand_lift = zeros(nBasis,nTrace,D,N);
    Phi = basis(xi);
    
    Rlift = zeros(N,D);
    rlift = zeros(N,D);
    for idim = 1:D
        for itrace = 1:nTrace
            rlift(:,idim) = evalFromBasis(xi,rL{itrace,idim},rR{itrace,idim});
            Rlift(:,idim) = Rlift(:,idim) + rlift(:,idim);
            
            for ibasis = 1:nBasis
                integrand_lift(ibasis,itrace,idim,:) = Phi(ibasis) * rlift(:,idim);
            end
        end
    end
    
    var = evalFromBasis(xi,varL,varR);
    varX = evalFromBasisGrad(xi,varL,varR,Xxi,param.normal);
    varx_lifted = varX(:,1) + Rlift(:,1);  
    varz_lifted = varX(:,2) + Rlift(:,2);
    
    [ integrand_source, integrand_fluxadvective ] = ...
        IntegrandCell(xi,Xxi,e1,e1x,e1z,var,varx_lifted,varz_lifted,param);
    
    
    %% print output
    fprintf(sprintf('Integrand: xi = %1.2f\n',xi));
    for ibasis = 1:nBasis
        fprintf(sprintf('// Basis function %d\n',ibasis));
        fprintf(sprintf('eIntegrand = ( %5.16e ) + ( %5.16e );\n',integrand_source(1,ibasis), integrand_fluxadvective(1,ibasis)));
        fprintf(sprintf('starIntegrand = ( %5.16e ) + ( %5.16e );\n',integrand_source(2,ibasis), integrand_fluxadvective(2,ibasis)));
        fprintf(sprintf('qxIntegrand = ( %5.16e ) + ( %5.16e );\n',integrand_source(3,ibasis), integrand_fluxadvective(3,ibasis)));
        fprintf(sprintf('qzIntegrand = ( %5.16e ) + ( %5.16e );\n',integrand_source(4,ibasis), integrand_fluxadvective(4,ibasis)));
        fprintf(sprintf('p0Integrand = ( %5.16e ) + ( %5.16e );\n',integrand_source(5,ibasis), integrand_fluxadvective(5,ibasis)));
        fprintf(sprintf('T0Integrand = ( %5.16e ) + ( %5.16e );\n',integrand_source(6,ibasis), integrand_fluxadvective(6,ibasis)));
        fprintf(sprintf('integrandTrue[%d] = { eIntegrand, starIntegrand, qxIntegrand, qzIntegrand, p0Integrand, T0Integrand };\n',ibasis-1));
        fprintf('\n');
    end
    
    for ibasis = 1:nBasis
        for itrace = 1:nTrace
            fprintf(sprintf('integrandLiftTrue[%d][%d] = { %5.16e, %5.16e };  // Basis function %d  Lifting Operator %d  { x, z }\n',....
                ibasis-1,itrace-1, integrand_lift(ibasis,itrace,1,6), integrand_lift(ibasis,itrace,2,6), ibasis,itrace ));
        end
        fprintf('\n');
    end
    
end

%% residual (integrals)
% set up quadrature
Nq = 20; % number of Gauss quadrature points. Quadrature order = 2*Nq - 1
xiqLim = [0, 1]; % integration limits
[xiq,wq] = lgwt(xiqLim,Nq); % evaluate Gaussian quadrature weights and locations

% evaluate integrands and apply quadrature
residualCell_source = zeros(N,nBasis);
residualCell_fluxadvective = zeros(N,nBasis);
residualCell_lift = zeros(nBasis,nTrace,D);

for i = 1:Nq
    xi = xiq(i);
    Xxi = evalFromDBasis(xi,XL,XR);
    detJ = sqrt(Xxi'*Xxi);
        
    EIF = evalFromBasis(xi,EIF_L,EIF_R);
    param.qxi = EIF(1);
    param.qzi = EIF(2);
    param.p0i = EIF(3);
    param.T0i = EIF(4);

    e1 = evalFromBasis(xi,e1L,e1R);
    e1X = evalFromBasisGrad(xi,e1L,e1R,Xxi,param.normal);
    e1x = e1X(:,1);  e1z = e1X(:,2);
    
    integrand_lift = zeros(nBasis,nTrace,D,N);
    Phi = basis(xi);
    
    Rlift = zeros(N,D);
    rlift = zeros(N,D);
    for idim = 1:D
        for itrace = 1:nTrace
            rlift(:,idim) = evalFromBasis(xi,rL{itrace,idim},rR{itrace,idim});
            Rlift(:,idim) = Rlift(:,idim) + rlift(:,idim);
            
            for ibasis = 1:nBasis
                integrand_lift(ibasis,itrace,idim,:) = Phi(ibasis) * rlift(:,idim);
                
                residualCell_lift(ibasis,itrace,idim) = residualCell_lift(ibasis,itrace,idim) ...
                    + wq(i)*detJ * integrand_lift(ibasis,itrace,idim,6);
            end
        end
    end
    
    var = evalFromBasis(xi,varL,varR);
    varX = evalFromBasisGrad(xi,varL,varR,Xxi,param.normal);
    varx_lifted = varX(:,1) + Rlift(:,1);  
    varz_lifted = varX(:,2) + Rlift(:,2);
    
    [ integrand_source, integrand_fluxadvective ] ...
        = IntegrandCell(xi,Xxi,e1,e1x,e1z,var,varx_lifted,varz_lifted,param);

    residualCell_source = residualCell_source ...
        + wq(i)*detJ * integrand_source;
    residualCell_fluxadvective = residualCell_fluxadvective ...
        + wq(i)*detJ * integrand_fluxadvective;
end

%% print output
fprintf('Integrand integral\n');
for i = 1:nBasis
    fprintf(sprintf('// Basis function %d\n',i));
    fprintf(sprintf('eRes = ( %5.16e ) + ( %5.16e );\n',residualCell_source(1,i), residualCell_fluxadvective(1,i)));
    fprintf(sprintf('starRes = ( %5.16e ) + ( %5.16e );\n',residualCell_source(2,i), residualCell_fluxadvective(2,i)));
    fprintf(sprintf('qxRes = ( %5.16e ) + ( %5.16e );\n',residualCell_source(3,i), residualCell_fluxadvective(3,i)));
    fprintf(sprintf('qzRes = ( %5.16e ) + ( %5.16e );\n',residualCell_source(4,i), residualCell_fluxadvective(4,i)));
    fprintf(sprintf('p0Res = ( %5.16e ) + ( %5.16e );\n',residualCell_source(5,i), residualCell_fluxadvective(5,i)));
    fprintf(sprintf('T0Res = ( %5.16e ) + ( %5.16e );\n',residualCell_source(6,i), residualCell_fluxadvective(6,i)));
    fprintf(sprintf('rsdPDETrue[%d] = { eRes, starRes, qxRes, qzRes, p0Res, T0Res };\n',i-1));
    fprintf('\n');
end

for ibasis = 1:nBasis
    for itrace = 1:nTrace
        fprintf(sprintf('rsdLiftTrue[%d][%d] = { %5.16e, %5.16e };  // Basis function %d  Lifting Operator %d  { x, z }\n',....
            ibasis-1,itrace-1, residualCell_lift(ibasis,itrace,1), residualCell_lift(ibasis,itrace,2), ibasis,itrace ));
    end
    fprintf('\n');
end