clc; clear all; close all;

%% Constants
D = 2;      % physical dimension
N = 6;      % number of equations
nBasis = 2; % number of DOFs = basis fcns per element; linear element
nElem = 2;  % number of elements = 2, i.e. left and right
LRindx = { 'L', 'R' };

%% PDE parameters
param = struct;

param.gam_ = 1.4;
param.R_ = 287.15;
param.mui_ = 1.827E-5;
param.Pr_ = 0.715;

%% Line element grid
X1 = [ 0;   0   ];
X2 = [ 0.8; 0.6 ];
X3 = [ 1.6; 0   ];

% print
Xall = { X1; X2; X3 };

for i = 1 : (nElem+1)
    fprintf(sprintf('X%d = { %2.3f, %2.3f };\n',i,Xall{i}));
end
fprintf('\n');

%% EIF parameter fields
% EIF = [ qxi, qzi, p0i, T0i ];

EIF_L_1 = [ 4;  2; 1.2E+5; 300 ];
EIF_L_2 = [ 7; -1; 1.5E+5; 320 ];

EIF_R_1 = [ -1;  3; 1.4E+5; 340 ];
EIF_R_2 = [ -3; -2; 0.9E+5; 290 ];

% print
EIFall = { EIF_L_1, EIF_L_2; 
           EIF_R_1, EIF_R_2; };
for i = 1:nElem
    for j = 1:nBasis
        suffix = sprintf('%s_%d', LRindx{i}, j-1);        
        fprintf( sprintf('const Real qxi_%s = %2.3f, qzi_%s = %2.3f, p0i_%s = %2.3e, T0i_%s = %5.2f;\n', ...
                         suffix, EIFall{i,j}(1), suffix, EIFall{i,j}(2), suffix, EIFall{i,j}(3), suffix, EIFall{i,j}(4)) );
    end
end
fprintf('\n');

%% Solution fields
% var = [ delta; A; qx; qz; p0; T0 ];

varL_1 = [ 0.1 ;  3 ;   4;   2;   1.1E+5;  290 ];
varL_2 = [ 0.08;  3.3;  3;   4;   1.0E+5;  350 ];

varR_1 = [ 0.12;  2.8;  2;   1;   1.3E+5;  330 ];
varR_2 = [ 0.09;  2.4;  5;   8;   0.9E+5;  310 ];

% print
varall = { varL_1, varL_2; 
           varR_1, varR_2; };
for i = 1:nElem
    for j = 1:nBasis
        suffix = sprintf('%s_%d', LRindx{i}, j-1);
        fprintf( sprintf('// State vector at %s element basis fcn %d\n', LRindx{i}, j-1) );
        fprintf( sprintf('const Real delta_%s = %2.4f;\n', suffix, varall{i,j}(1)) );
        fprintf( sprintf('const Real A_%s     = %2.4f;\n', suffix, varall{i,j}(2)) );
        fprintf( sprintf('const Real qx_%s    = %2.4f;\n', suffix, varall{i,j}(3)) );
        fprintf( sprintf('const Real qz_%s    = %2.4f;\n', suffix, varall{i,j}(4)) );
        fprintf( sprintf('const Real p0_%s    = %2.4e;\n', suffix, varall{i,j}(5)) );
        fprintf( sprintf('const Real T0_%s    = %2.4f;\n', suffix, varall{i,j}(6)) );
        fprintf('\n');
    end
end
fprintf('\n');

%% lifting operators
% size(r) = [ nBasis ]

rL = [ [1; 2], [3; 4] ];
rR = [ [4; 3], [2; 1] ];

% print
rall = { rL; rR; };

for i = 1:nElem
    for j = 1:nBasis
        fprintf( sprintf('rfldElem%s.DOF(%d) = { %2.2f, %2.2f };  // Basis fcn %d  { x, z }\n', LRindx{i}, j-1, rall{i}(:,j), j) );
    end
    fprintf('\n');
end
fprintf('\n'); 
   
%% integrand
% sRef
xiL = 1; % sRefL
xiR = 0; % sRefR

XxiL = evalFromDBasis(xiL,X1,X2);
XxiR = evalFromDBasis(xiR,X2,X3);

% EIF
EIF_L = evalFromBasis(xiL,EIF_L_1,EIF_L_2);
EIF_R = evalFromBasis(xiL,EIF_R_1,EIF_R_2);

% unit element normal
normalL = evalNormal(X1,X2);
normalR = evalNormal(X2,X3);

% basis vector
e1L = XxiL / norm(XxiL);
e1R = XxiR / norm(XxiR);

e1XL = zeros(D,nBasis);
e1XR = zeros(D,nBasis); % for linear elements

% unit trace normal
Nrm = 0.5 * (e1L + e1R); % assuming e1L points out of left element and e1R points into right element
Nrm = Nrm / norm(Nrm);   % normalize
if (norm(Nrm)-1 < eps)
else
    error('Unit trace normal is not normalized.');
end

% solution evaluation
varL = evalFromBasis(xiL,varL_1,varL_2);

varR = evalFromBasis(xiR,varR_1,varR_2);

% integrand
integrand_interiortrace = zeros(nElem,nBasis,N);

[ integrand_interiortrace ]  ...
       = IntegrandInteriorTrace(xiL,e1L,varL, xiR,e1R,varR, Nrm,param);

integrand_lift = zeros(nElem,nBasis,D,N);
Phi = basis([xiL;xiR]);

dqn = zeros(N,D);
dqn = (varL - varR) * Nrm';

for ielem = 1:nElem
    for idim = 1:D
        for ibasis = 1:nBasis
            integrand_lift(ielem,ibasis,idim,:) = 0.5 * Phi(ielem,ibasis) * dqn(:,idim);
        end
    end
end

%% print output
fprintf(sprintf('Integrand interior trace:\n'));
for ielem = 1:nElem
    fprintf(sprintf('// %s element\n',LRindx{ielem}));
    for ibasis = 1:nBasis
        fprintf(sprintf('// Basis function %d\n',ibasis));
        fprintf(sprintf('eIntegrand    = %5.16e;\n',integrand_interiortrace(ielem,ibasis,1)) );
        fprintf(sprintf('starIntegrand = %5.16e;\n',integrand_interiortrace(ielem,ibasis,2)) );
        fprintf(sprintf('qxIntegrand   = %5.16e;\n',integrand_interiortrace(ielem,ibasis,3)) );
        fprintf(sprintf('qzIntegrand   = %5.16e;\n',integrand_interiortrace(ielem,ibasis,4)) );
        fprintf(sprintf('p0Integrand   = %5.16e;\n',integrand_interiortrace(ielem,ibasis,5)) );
        fprintf(sprintf('T0Integrand   = %5.16e;\n',integrand_interiortrace(ielem,ibasis,6)) );
        fprintf(sprintf('integrandTrue%s[%d] = { eIntegrand, starIntegrand, qxIntegrand, qzIntegrand, p0Integrand, T0Integrand };\n',LRindx{ielem},ibasis-1));
        fprintf('\n');
    end
end

for ielem = 1:nElem
    for ibasis = 1:nBasis
        for idim = 1:D
            
            if idim == 1
                dimstr = 'x-component';
            elseif idim == 2
                dimstr = 'z-component';
            else
                error('Invalid physical dimension')
            end
            
            fprintf(sprintf('integrandLiftTrue%s[%d][%d] = { %5.16e,\n', ...
                LRindx{ielem},ibasis-1,idim-1, integrand_lift(ielem,ibasis,idim,1) ));
            fprintf(sprintf('                             %5.16e,\n', ...
                                               integrand_lift(ielem,ibasis,idim,2) ));
            fprintf(sprintf('                             %5.16e,\n', ...
                                               integrand_lift(ielem,ibasis,idim,3) ));
            fprintf(sprintf('                             %5.16e,\n', ...
                                               integrand_lift(ielem,ibasis,idim,4) ));
            fprintf(sprintf('                             %5.16e,\n', ...
                                               integrand_lift(ielem,ibasis,idim,5) ));
            fprintf(sprintf('                             %5.16e };  // Basis function %d  %s\n', ...
                                               integrand_lift(ielem,ibasis,idim,6), ibasis,dimstr ));
            fprintf('\n');
        end
    end
    fprintf('\n');
end

    

% %% residual (integrals)
% % set up quadrature
% Nq = 20; % number of Gauss quadrature points. Quadrature order = 2*Nq - 1
% xiqLim = [0, 1]; % integration limits
% [xiq,wq] = lgwt(xiqLim,Nq); % evaluate Gaussian quadrature weights and locations
% 
% % evaluate integrands and apply quadrature
% residualCell_source = zeros(N,nBasis);
% residualCell_fluxadvective = zeros(N,nBasis);
% residualCell_lift = zeros(nBasis,nTrace,D);
% 
% for i = 1:Nq
%     xi = xiq(i);
%     Xxi = evalFromDBasis(xi,XL,XR);
%     detJ = sqrt(Xxi'*Xxi);
%         
%     EIF = evalFromBasis(xi,EIF_L,EIF_R);
%     param.qxi = EIF(1);
%     param.qzi = EIF(2);
%     param.p0i = EIF(3);
%     param.T0i = EIF(4);
% 
%     e1 = evalFromBasis(xi,e1L,e1R);
%     e1X = evalFromBasisGrad(xi,e1L,e1R,Xxi,param.normal);
%     e1x = e1X(:,1);  e1z = e1X(:,2);
%     
%     integrand_lift = zeros(nBasis,nTrace,D,N);
%     Phi = basis(xi);
%     
%     Rlift = zeros(N,D);
%     rlift = zeros(N,D);
%     for idim = 1:D
%         for itrace = 1:nTrace
%             rlift(:,idim) = evalFromBasis(xi,rL{itrace,idim},rR{itrace,idim});
%             Rlift(:,idim) = Rlift(:,idim) + rlift(:,idim);
%             
%             for ibasis = 1:nBasis
%                 integrand_lift(ibasis,itrace,idim,:) = Phi(ibasis) * rlift(:,idim);
%                 
%                 residualCell_lift(ibasis,itrace,idim) = residualCell_lift(ibasis,itrace,idim) ...
%                     + wq(i)*detJ * integrand_lift(ibasis,itrace,idim,6);
%             end
%         end
%     end
%     
%     var = evalFromBasis(xi,varL,varR);
%     varX = evalFromBasisGrad(xi,varL,varR,Xxi,param.normal);
%     varx_lifted = varX(:,1) + Rlift(:,1);  
%     varz_lifted = varX(:,2) + Rlift(:,2);
%     
%     [ integrand_source, integrand_fluxadvective ] ...
%         = IntegrandCell(xi,Xxi,e1,e1x,e1z,var,varx_lifted,varz_lifted,param);
% 
%     residualCell_source = residualCell_source ...
%         + wq(i)*detJ * integrand_source;
%     residualCell_fluxadvective = residualCell_fluxadvective ...
%         + wq(i)*detJ * integrand_fluxadvective;
% end
% 
% %% print output
% fprintf('Integrand integral\n');
% for i = 1:nBasis
%     fprintf(sprintf('// Basis function %d\n',i));
%     fprintf(sprintf('eRes = ( %5.16e ) + ( %5.16e );\n',residualCell_source(1,i), residualCell_fluxadvective(1,i)));
%     fprintf(sprintf('starRes = ( %5.16e ) + ( %5.16e );\n',residualCell_source(2,i), residualCell_fluxadvective(2,i)));
%     fprintf(sprintf('qxRes = ( %5.16e ) + ( %5.16e );\n',residualCell_source(3,i), residualCell_fluxadvective(3,i)));
%     fprintf(sprintf('qzRes = ( %5.16e ) + ( %5.16e );\n',residualCell_source(4,i), residualCell_fluxadvective(4,i)));
%     fprintf(sprintf('p0Res = ( %5.16e ) + ( %5.16e );\n',residualCell_source(5,i), residualCell_fluxadvective(5,i)));
%     fprintf(sprintf('T0Res = ( %5.16e ) + ( %5.16e );\n',residualCell_source(6,i), residualCell_fluxadvective(6,i)));
%     fprintf(sprintf('rsdPDETrue[%d] = { eRes, starRes, qxRes, qzRes, p0Res, T0Res };\n',i-1));
%     fprintf('\n');
% end
% 
% for ibasis = 1:nBasis
%     for itrace = 1:nTrace
%         fprintf(sprintf('rsdLiftTrue[%d][%d] = { %5.16e, %5.16e };  // Basis function %d  Lifting Operator %d  { x, z }\n',....
%             ibasis-1,itrace-1, residualCell_lift(ibasis,itrace,1), residualCell_lift(ibasis,itrace,2), ibasis,itrace ));
%     end
%     fprintf('\n');
% end