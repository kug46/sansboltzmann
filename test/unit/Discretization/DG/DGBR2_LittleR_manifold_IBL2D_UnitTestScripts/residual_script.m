clear all; close all;

N = 6; % number of equations per element

%% parameters
param = struct;

% PDE
param.gam_ = 1.4;
param.R_ = 287.15;
param.mui_ = 1.827E-5;
param.Pr_ = 0.715;

% EIF
qxi_L = 4;  qzi_L = 3;  p0i_L = 1.2E+5;  T0i_L = 300;
qxi_R = 8;  qzi_R = 5;  p0i_R = 1.25E+5;  T0i_R = 320;
EIF_L = [ qxi_L; qzi_L; p0i_L; T0i_L ];
EIF_R = [ qxi_R; qzi_R; p0i_R; T0i_R ];

% elemental geometry
e1L = [ 1; 0 ];  e1R = [ 0.8; 0.6 ];

XL = [ 1.2; 2.2 ];  XR = [ 1.4; 2.5 ];

param.normal = evalNormal(XL,XR);

%% state vectors & xz-derivatives
varL = [ 2E-2;   1.2; 3;   4;   1.3E+5;  290 ];
varR = [ 2.2E-2; 1.3; 5;   2;   1.4E+5;  350 ];

%% integrand
xi_array = [ 0; 0.6; 1 ];
for i = 1:length(xi_array)
    xi = xi_array(i);
    EIF = evalFromBasis(xi,EIF_L,EIF_R);
        param.qxi = EIF(1);
        param.qzi = EIF(2);
        param.p0i = EIF(3);
        param.T0i = EIF(4);
    Xxi = evalFromDBasis(xi,XL,XR);
    e1 = evalFromBasis(xi,e1L,e1R);
    e1X = evalFromBasisGrad(xi,e1L,e1R,Xxi,param.normal);
    e1x = e1X(:,1);  e1z = e1X(:,2);
    var = evalFromBasis(xi,varL,varR);
    varX = evalFromBasisGrad(xi,varL,varR,Xxi,param.normal);
    varx = varX(:,1);  varz = varX(:,2);
    [ integrand_source, integrand_fluxadvective ] = ...
        IntegrandCell(xi,Xxi,e1,e1x,e1z,var,varx,varz,param);
    
    %% print output
    fprintf(sprintf('Integrand: xi = %1.2f\n',xi));
    for j = 1:2
        fprintf(sprintf('// Basis function %d\n',j));
        fprintf(sprintf('eIntegrand = ( %5.16e ) + ( %5.16e );\n',integrand_source(1,j), integrand_fluxadvective(1,j)));
        fprintf(sprintf('starIntegrand = ( %5.16e ) + ( %5.16e );\n',integrand_source(2,j), integrand_fluxadvective(2,j)));
        fprintf(sprintf('qxIntegrand = ( %5.16e ) + ( %5.16e );\n',integrand_source(3,j), integrand_fluxadvective(3,j)));
        fprintf(sprintf('qzIntegrand = ( %5.16e ) + ( %5.16e );\n',integrand_source(4,j), integrand_fluxadvective(4,j)));
        fprintf(sprintf('p0Integrand = ( %5.16e ) + ( %5.16e );\n',integrand_source(5,j), integrand_fluxadvective(5,j)));
        fprintf(sprintf('T0Integrand = ( %5.16e ) + ( %5.16e );\n',integrand_source(6,j), integrand_fluxadvective(6,j)));
        fprintf(sprintf('integrandTrue[%d] = { eIntegrand, starIntegrand, qxIntegrand, qzIntegrand, p0Integrand, T0Integrand };\n',i-1));
        fprintf('\n');
    end
    
end

%% residual (integrals)
% set up quadrature
Nq = 39; % number of Gauss quadrature points. Quadrature order = 2*Nq - 1
xiqLim = [0, 1]; % integration limits
[xiq,wq] = lgwt(xiqLim,Nq); % evaluate Gaussian quadrature weights and locations

% evaluate integrands and apply quadrature
residualCell_source = zeros(N,2);
residualCell_fluxadvective = zeros(N,2);

for i = 1:Nq
    xi = xiq(i);
    EIF = evalFromBasis(xi,EIF_L,EIF_R);
        param.qxi = EIF(1);
        param.qzi = EIF(2);
        param.p0i = EIF(3);
        param.T0i = EIF(4);
    Xxi = evalFromDBasis(xi,XL,XR);
        detJ = sqrt(Xxi'*Xxi);
    e1 = evalFromBasis(xi,e1L,e1R);
    e1X = evalFromBasisGrad(xi,e1L,e1R,Xxi,param.normal);
    e1x = e1X(:,1);  e1z = e1X(:,2);
    var = evalFromBasis(xi,varL,varR);
    varX = evalFromBasisGrad(xi,varL,varR,Xxi,param.normal);
    varx = varX(:,1);  varz = varX(:,2);
    [ integrand_source, integrand_fluxadvective ] = IntegrandCell(xi,Xxi,e1,e1x,e1z,var,varx,varz,param);
    
    residualCell_source = residualCell_source ...
        + wq(i)*detJ * integrand_source;
    residualCell_fluxadvective = residualCell_fluxadvective ...
        + wq(i)*detJ * integrand_fluxadvective;
end

%% print output
fprintf('Residual\n');
for i = 1:2
    fprintf(sprintf('// Basis function %d\n',i));
    fprintf(sprintf('eRes = ( %5.16e ) + ( %5.16e );\n',residualCell_source(1,i), residualCell_fluxadvective(1,i)));
    fprintf(sprintf('starRes = ( %5.16e ) + ( %5.16e );\n',residualCell_source(2,i), residualCell_fluxadvective(2,i)));
    fprintf(sprintf('qxRes = ( %5.16e ) + ( %5.16e );\n',residualCell_source(3,i), residualCell_fluxadvective(3,i)));
    fprintf(sprintf('qzRes = ( %5.16e ) + ( %5.16e );\n',residualCell_source(4,i), residualCell_fluxadvective(4,i)));
    fprintf(sprintf('p0Res = ( %5.16e ) + ( %5.16e );\n',residualCell_source(5,i), residualCell_fluxadvective(5,i)));
    fprintf(sprintf('T0Res = ( %5.16e ) + ( %5.16e );\n',residualCell_source(6,i), residualCell_fluxadvective(6,i)));
    fprintf(sprintf('residualTrue[%d] = { eRes, starRes, qxRes, qzRes, p0Res, T0Res };\n',i-1));
    fprintf('\n');
end

