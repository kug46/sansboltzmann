function [ integrand_interiortrace ]  ...
       = IntegrandInteriorTrace(xiL,e1L,varL, xiR,e1R,varR, Nrm,PDEparam)
% weighting fcn
PhiL = basis(xiL);
PhiR = basis(xiR);

% constants
nElem = 2;
nBasis = 2;
N = length(varL);

% evaluate upwinded advective flux
f = fluxAdvectiveUpwind(e1L,varL,e1R,varR,Nrm,PDEparam);

% integrand
integrand_interiortrace = zeros(nElem,nBasis,N);

for ibasis = 1:nBasis
    integrand_interiortrace(1,ibasis,:) =  PhiL(ibasis) * f;
    integrand_interiortrace(2,ibasis,:) = -PhiR(ibasis) * f;
end

end