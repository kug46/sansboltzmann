function secondaryVar = ThicknessCoefficient(Re_d, delta, A)
% Compute secondary variables: thickness integrals and shear stress
% coefficients.

%% set up quadrature
Nq = 200; % number of Gauss quadrature points. Quadrature order = 2*Nq - 1
% Nq = 20 matches max quadrature order of 39 in SANS
etabqLim = [0, 1]; % integration limits on \etab
[etabq,wq] = lgwt(etabqLim,Nq); % evaluate Gaussian quadrature weights and locations

%% compute profiles
[U,dUdetab] = LaminarVelProfile(etabq,A); % viscous velocity streamwise component; evaluate at quadrature points
R = LaminarDensityProfile(etabq); % viscous density profile; evaluate at quadrature points
DetaDetabar = DetaDetab(etabq,A); % derivative: d\eta/d\etab

%% evaluate thicknesses [secondary variables]
delta1s = delta * (wq.*DetaDetabar)' * ( 1 - R.*U ); % delta_1^*;
theta11 = delta * (wq.*DetaDetabar)' * ( (1 - U) .* R .* U ); % theta_11
theta1s = delta * (wq.*DetaDetabar)' * ( (1 - U.^2) .* R .* U ); % theta_1^*
delta1p = delta * (wq.*DetaDetabar)' * ( 1 - U ); % delta_1^'
deltarho = delta * (wq.*DetaDetabar)' * ( 1 - R ); % delta_rho
deltaq = delta * (wq.*DetaDetabar)' * ( (1 - U.^2) .* R ); % delta_q

%% evaluate shear stress coefficients [secondary variables]
% skin friction coefficient
S0 = LaminarStressProfile(0,A,Re_d);
Cf1 = 2.0 * S0;

% dissipation coefficient
S = LaminarStressProfile(etabq,A,Re_d);
CD = wq' * ( S .* dUdetab );

%% assemble output
% initialize
secondaryVar = struct;
% thicknesses
secondaryVar.delta1s = delta1s;
secondaryVar.theta11 = theta11;
secondaryVar.theta1s = theta1s;
secondaryVar.delta1p = delta1p;
secondaryVar.deltarho = deltarho;
secondaryVar.deltaq = deltaq;
% shear stress coefficients
secondaryVar.Cf1 = Cf1;
secondaryVar.CD = CD;