function normal = evalNormal(XL,XR)

tangent = ( XR - XL ) / ( (XR-XL)' * (XR-XL) )^0.5;

RotateCountclock90 = [ 0, -1; 1, 0 ];

normal = RotateCountclock90 * tangent;

end
