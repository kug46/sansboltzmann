function [ fx, fz ] = fluxAdvective(e1,var,param)
% compute the advective flux terms

%% pde parameters
mui_ = param.mui_;

%% state vectors & derived variables
varType = 'DAGVelcoityStagPT';
delta = varInterpret(var,var,var,varType,'delta');
A = varInterpret(var,var,var,varType,'A');
q1 = varInterpret(var,var,var,varType,'VelocityEdge');

q = varInterpret(var,var,var,varType,'SpeedEdge');
rho = varInterpret(var,var,var,varType,'DensityEdge',param);
Re_d = rho * q * delta / mui_;

%% COMPUTE DEFECT INTEGRALS AND SURFACE GRADIENTS
% secondary variables
secondaryVar = ThicknessCoefficient(Re_d, delta, A);
theta11 = secondaryVar.theta11;
theta1s = secondaryVar.theta1s;

% defect thicknesses
P = rho * theta11 * ( q1 * q1' );
K = rho * q^2 * theta1s * q1;

PTdote = P' * e1;

%% assemble source terms
fx = zeros(size(var));
fz = zeros(size(var));

fx(1) = PTdote(1);
fz(1) = PTdote(2);

fx(2) = K(1);
fz(2) = K(2);
