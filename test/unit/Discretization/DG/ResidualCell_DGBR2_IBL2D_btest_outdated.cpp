// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// testing of DGBR2 cell residual functions for IBL2D

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Discretization/DG/ResidualCell_DGBR2.h"
#include "Discretization/DG/IntegrandCell_DGBR2_manifold.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/tools/for_each_CellGroup.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "pde/IBL/SetVelDofCell_IBL2D.h"
#include "pde/IBL/PDEIBL2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "tools/Tuple.h"

#include "unit/UnitGrids/XField2D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Line_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEIBL<PhysD2,VarTypeDANCt> PDEIBL2D;
typedef PDENDConvertSpace<PhysD2,PDEIBL2D> NDPDEClassIBL2D;
template class IntegrandCell_DGBR2_manifold<NDPDEClassIBL2D>;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( ResidualCell_DGBR2_IBL2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualCell_DGBR2_IBL2D_Line_X1Q1R1_IncompressibleLaminarWallProfile_test )
{
  // type definition
  typedef PhysD2 PhysDim;
  typedef VarData2DDANCt VarDataType;
  typedef VarTypeDANCt VarType;
  typedef PDEIBL<PhysDim,VarType> PDEClass;
  typedef PDENDConvertSpace<PhysDim,PDEClass> NDPDEClass;
  /* default parameters
      const Real gam = 1.4;
      const Real R = 287.15;
      const Real mui = 1.827e-5;
      const Real Pr = 0.715;
   */
  NDPDEClass pde(VelocityProfile2D::LaminarBL);

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef PDEClass::template ArrayParam<Real> ArrayParam;
  typedef NDPDEClass::VectorX VectorX;
  typedef PDEClass::template ArrayS<Real> ArrayS;

  typedef IntegrandCell_DGBR2_manifold<NDPDEClass> IntegrandClass;

  const int nBasis = 2;
  const int nTrace = 2;

  // ----------------------- FIELDS ----------------------- //
  // solution: single line, P1 (aka Q1)
  const int order = 1;

  // grid: single line, P1 (aka X1)
  VectorX X1, X2;
  Real x_L = 1.2, x_R = x_L + 0.8;
  Real z_L = 2.2, z_R = z_L + 0.6;

  X1 = { x_L, z_L };
  X2 = { x_R, z_R };

  XField2D_1Line_X1_1Group xfld( X1, X2 );

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // EIF edge velocity DG field
  Field_DG_Cell<PhysDim, TopoD1, VectorX> velfld(xfld, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( nBasis, velfld.nDOF() );
  BOOST_CHECK_EQUAL( 1,      velfld.nElem() );

  Real qxi_L = 3.,  qzi_L = 4.;
  Real qxi_R = 5.,  qzi_R = 2.;

  VectorX q1_L = { qxi_L, qzi_L };
  VectorX q1_R = { qxi_R, qzi_R };

  velfld.DOF(0) = q1_L;
  velfld.DOF(1) = q1_R;

  std::vector<Real> dataVel = {6, 2};

  Field_DG_Cell<PhysDim, TopoD1, VectorX> vxXfld(xfld, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( nBasis, vxXfld.nDOF() );
  BOOST_CHECK_EQUAL( 1,      vxXfld.nElem() );

  Field_DG_Cell<PhysDim, TopoD1, VectorX> vzXfld(xfld, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( nBasis, vzXfld.nDOF() );
  BOOST_CHECK_EQUAL( 1,      vzXfld.nElem() );

  for_each_CellGroup<TopoD1>::apply( SetVelocityDofCell_IBL2D(dataVel, {0}), (velfld, vxXfld, vzXfld, xfld) );

  // EIF stagnation state DG field
  Field_DG_Cell<PhysDim, TopoD1, ArrayS> stagfld(xfld, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( nBasis, stagfld.nDOF() );
  BOOST_CHECK_EQUAL( 1,      stagfld.nElem() );

  Real p0i_L = 1.30e+5,  T0i_L = 290.;
  Real p0i_R = 1.40e+5,  T0i_R = 350.;

  ArrayQ stag_L = { p0i_L, T0i_L };
  ArrayQ stag_R = { p0i_R, T0i_R };

  stagfld.DOF(0) = stag_L;
  stagfld.DOF(1) = stag_R;

  // parameter field
  Field_DG_Cell<PhysDim, TopoD1, ArrayParam> paramfld(xfld, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( nBasis, paramfld.nDOF() );
  BOOST_CHECK_EQUAL( 1,      paramfld.nElem() );

  paramfld.DOF(0) = {velfld.DOF(0)[0], velfld.DOF(0)[1],
                     vxXfld.DOF(0)[0], vxXfld.DOF(0)[1],
                     vzXfld.DOF(0)[0], vzXfld.DOF(0)[1],
                     stagfld.DOF(0)[0], stagfld.DOF(0)[1]};

  paramfld.DOF(1) = {velfld.DOF(1)[0], velfld.DOF(1)[1],
                     vxXfld.DOF(1)[0], vxXfld.DOF(1)[1],
                     vzXfld.DOF(1)[0], vzXfld.DOF(1)[1],
                     stagfld.DOF(1)[0], stagfld.DOF(1)[1]};

  // Solution variable DG field
  Field_DG_Cell<PhysDim, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( nBasis, qfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld.nElem() );

  // State vector solution at line cell first node {0}
  Real delta_L = 2e-2;
  Real A_L     = 1.2;

  // State vector solution at line cell second node {1}
  Real delta_R = 2.2e-2;
  Real A_R     = 1.3;

  // Line solution
  qfld.DOF(0) = pde.setDOFFrom( VarDataType(delta_L,A_L) );
  qfld.DOF(1) = pde.setDOFFrom( VarDataType(delta_R,A_R) );

  // lifting operator: single triangle, P1 (aka Q1)
  FieldLift_DG_Cell<PhysDim, TopoD1, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Hierarchical);

  rfld.DOF(0) = { 1, 2 };  rfld.DOF(1) = { 3, 4 }; // Trace 1  Basis Function  { x, z }
  rfld.DOF(2) = { 4, 3 };  rfld.DOF(3) = { 2, 1 }; // Trace 2  Basis Function  { x, z }

  BOOST_CHECK_EQUAL( nBasis*nTrace, rfld.nDOF() );
  BOOST_CHECK( rfld.D == pde.D);

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // ----------------------------- TEST RESIDUAL (INTEGRAL) ----------------------------- //
  const Real small_tol_res = 5e-13;
  const Real close_tol_res = 5e-13;

  // Test the element integral of the functor: quadrature order = 39
  int quadratureOrder = 39;

  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());

  rsdPDEGlobal = 0;

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // grid integration (just one element)
  const int nCellGroup = 1;

  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdPDEGlobal),
                                          (paramfld, xfld), (qfld, rfld), &quadratureOrder, nCellGroup );

  Real eRes;
  Real starRes;

  ArrayQ rsdPDETrue[nBasis];

  // PDE residuals: (source) + (advective flux)
  // Basis function 1
  eRes = ( -1.0160079240343214e-01 ) + ( 6.4774236872871135e-02 );
  starRes = ( -2.7242315945861288e-02 ) + ( 4.6313795440318911e-01 );
  rsdPDETrue[0] = { eRes, starRes };

  // Basis function 2
  eRes = ( -7.1776476686346499e-02 ) + ( -6.4774236872871135e-02 );
  starRes = ( -1.4066661472156960e-02 ) + ( -4.6313795440318911e-01 );
  rsdPDETrue[1] = { eRes, starRes };

  for ( int ibasis = 0; ibasis < pde.D; ibasis++ )
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE( rsdPDETrue[ibasis][n], rsdPDEGlobal[ibasis][n], small_tol_res, close_tol_res );
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
