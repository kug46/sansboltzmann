// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_DGBR2_Triangle_AD_btest
// testing of 2-D cell element residual integrands for DG BR2: Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"
#include "Field/Element/ElementLift.h"

#include "Discretization/DG/IntegrandCell_DGBR2.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_UniformGrad> PDEAdvectionDiffusion1D;
typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass1D;
typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldLine;

template class IntegrandCell_DGBR2<PDEClass1D>::BasisWeighted_PDE<Real, Real, TopoD1, Line, ElementXFieldLine>;
template class IntegrandCell_DGBR2<PDEClass1D>::BasisWeighted_LO<Real, TopoD1, Line>;
template class IntegrandCell_DGBR2<PDEClass1D>::FieldWeighted<Real, TopoD1, Line, ElementXFieldLine>;

typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_UniformGrad > PDEAdvectionDiffusion2D;
typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass2D;
typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldTri;

template class IntegrandCell_DGBR2<PDEClass2D>::BasisWeighted_PDE<Real, Real, TopoD2, Triangle, ElementXFieldTri>;
template class IntegrandCell_DGBR2<PDEClass2D>::BasisWeighted_LO<Real, TopoD2, Triangle>;
template class IntegrandCell_DGBR2<PDEClass2D>::FieldWeighted<Real, TopoD2, Triangle, ElementXFieldTri>;

typedef PDEAdvectionDiffusion<PhysD3,
                              AdvectiveFlux3D_Uniform,
                              ViscousFlux3D_Uniform,
                              Source3D_UniformGrad > PDEAdvectionDiffusion3D;
typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass3D;
typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldTet;

template class IntegrandCell_DGBR2<PDEClass3D>::BasisWeighted_PDE<Real, Real, TopoD3, Tet, ElementXFieldTet>;
template class IntegrandCell_DGBR2<PDEClass3D>::BasisWeighted_LO<Real, TopoD3, Tet>;
template class IntegrandCell_DGBR2<PDEClass3D>::FieldWeighted<Real, TopoD3, Tet, ElementXFieldTet>;
}



//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_DGBR2_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_1D_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef ElementLift<VectorArrayQ,TopoD1,Line> ElementrFieldCell;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementRFieldCell;
  typedef ElementXFieldCell::RefCoordType RefCoordType;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,Real,TopoD1,Line,ElementXFieldCell> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO <Real,TopoD1,Line> BasisWeightedLOClass;


  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real s0 = 0.25, sx = 0.6;
  Source1D_UniformGrad source(s0, sx);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

 BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // line solution
  qfldElem.DOF(0) = 2;
  qfldElem.DOF(1) = 3;

  // lifting operators
  ElementrFieldCell rfldElems(order, BasisFunctionCategory_Hierarchical);

  rfldElems[0].DOF(0) = -1;  rfldElems[0].DOF(1) = 5;
  rfldElems[1].DOF(0) =  3;  rfldElems[1].DOF(1) = 2;

  // sum of lifting operators
  ElementRFieldCell RfldElem(order, BasisFunctionCategory_Hierarchical);

  RfldElem.vectorViewDOF() = rfldElems[0].vectorViewDOF() +
                             rfldElems[1].vectorViewDOF();

  // BR2 discretization (not used)
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  BasisWeightedPDEClass fcnPDE = fcnint.integrand_PDE( xfldElem, qfldElem, RfldElem );
  BasisWeightedLOClass  fcnLO  = fcnint.integrand_LO( xfldElem, rfldElems );

  //BOOST_CHECK( fcn.N == 1);
  BOOST_CHECK_EQUAL( 1, fcnPDE.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcnPDE.nDOF() );
  BOOST_CHECK( fcnPDE.needsEvaluation() == true );
  BOOST_CHECK_EQUAL( 1, fcnLO.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcnLO.nDOF() );
  BOOST_CHECK( fcnLO.needsEvaluation() == true );

  const int nDOF = 2;
  const int nNode = 2;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordType sRef;
  Real integrandPDETrue[nDOF] = {0,0};
  ArrayQ integrandLiftxTrue[nDOF][nNode];
  ArrayQ integrandPDE[nDOF];
  BasisWeightedLOClass::IntegrandType integrandLO[nDOF];

  // Test at {0}
  sRef = {0};
  fcnPDE( sRef, integrandPDE, 2 );
  fcnLO( sRef, integrandLO, 2 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 11/5. ) + ( -2123/1000. ) + ( 23/10. ); // Basis function 1
  integrandPDETrue[1] = ( -11/5. ) + ( 2123/1000. ) + ( 0 ); // Basis function 2

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );

  //Lifting operator residual integrands: (lifting)
  integrandLiftxTrue[0][0] =  ( -1 );   // Basis function 1  Lifting Operator 1
  integrandLiftxTrue[1][0] =  ( 0 );   // Basis function 2  Lifting Operator 1
  integrandLiftxTrue[0][1] =  ( 3 );   // Basis function 1  Lifting Operator 2
  integrandLiftxTrue[1][1] =  ( 0 );   // Basis function 2  Lifting Operator 2

                                              // [DOF][trace][d]
  SANS_CHECK_CLOSE( integrandLiftxTrue[0][0], integrandLO[0][0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[1][0], integrandLO[1][0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[0][1], integrandLO[0][1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[1][1], integrandLO[1][1][0], small_tol, close_tol );


  // Test at {1}
  sRef = {1};
  fcnPDE( sRef, integrandPDE, 2 );
  fcnLO( sRef, integrandLO, 2 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 33/10. ) + ( -2123/1000. ) + ( 0 ); // Basis function 1
  integrandPDETrue[1] = ( -33/10. ) + ( 2123/1000. ) + ( 111/20. ); // Basis function 2

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );

  //Lifting operator residual integrands: (lifting)
  integrandLiftxTrue[0][0] =  ( 0 );   // Basis function 1  Lifting Operator 1
  integrandLiftxTrue[1][0] =  ( 5 );   // Basis function 2  Lifting Operator 1
  integrandLiftxTrue[0][1] =  ( 0 );   // Basis function 1  Lifting Operator 2
  integrandLiftxTrue[1][1] =  ( 2 );   // Basis function 2  Lifting Operator 2

                                                // [DOF][trace][d]
  SANS_CHECK_CLOSE( integrandLiftxTrue[0][0], integrandLO[0][0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[1][0], integrandLO[1][0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[0][1], integrandLO[0][1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[1][1], integrandLO[1][1][0], small_tol, close_tol );


  // Test at {1/2}
  sRef = {1./2.};
  fcnPDE( sRef, integrandPDE, 2 );
  fcnLO( sRef, integrandLO, 2 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 11/4. ) + ( -2123/1000. ) + ( 157/80. ); // Basis function 1
  integrandPDETrue[1] = ( -11/4. ) + ( 2123/1000. ) + ( 157/80. ); // Basis function 2

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );

  //Lifting operator residual integrands: (lifting)
  integrandLiftxTrue[0][0] =  ( 1 );   // Basis function 1  Lifting Operator 1
  integrandLiftxTrue[1][0] =  ( 1 );   // Basis function 2  Lifting Operator 1
  integrandLiftxTrue[0][1] =  ( 5./4. );   // Basis function 1  Lifting Operator 2
  integrandLiftxTrue[1][1] =  ( 5./4. );   // Basis function 2  Lifting Operator 2

                                                // [DOF][trace][d]
  SANS_CHECK_CLOSE( integrandLiftxTrue[0][0], integrandLO[0][0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[1][0], integrandLO[1][0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[0][1], integrandLO[0][1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[1][1], integrandLO[1][1][0], small_tol, close_tol );


  // test the element integral of the functor
  int quadratureorder = 2;
  int nIntegrand = nDOF;
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integralPDE(quadratureorder, nIntegrand);
  GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedLOClass::IntegrandType> integralLO(quadratureorder, nIntegrand);

  ArrayQ rsdElemPDE[nDOF];
  BasisWeightedLOClass::IntegrandType rsdElemLO[nDOF];

  // cell integration for canonical element
  integralPDE( fcnPDE, xfldElem, rsdElemPDE, nIntegrand );
  integralLO( fcnLO, xfldElem, rsdElemLO, nIntegrand );
  Real rsdPDETrue[nDOF], rsdLiftxTrue[nDOF][nNode];

  //PDE residual: (advective) + (viscous) + (source)
  rsdPDETrue[0] = ( 11/4. ) + ( -2123/1000. ) + ( 203/120. );   // Basis function 1
  rsdPDETrue[1] = ( -11/4. ) + ( 2123/1000. ) + ( 67/30. );   // Basis function 2

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdElemPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdElemPDE[1], small_tol, close_tol );

  //Lifting Operator residual:
  rsdLiftxTrue[0][0] = ( 1./2. ); // Basis function 1, Lifting Operator 1
  rsdLiftxTrue[1][0] = ( 3./2. ); // Basis function 2, Lifting Operator 1
  rsdLiftxTrue[0][1] = ( 4./3. ); // Basis function 1, Lifting Operator 2
  rsdLiftxTrue[1][1] = ( 7./6. ); // Basis function 2, Lifting Operator 2

                                        // [DOF][trace][d]
  SANS_CHECK_CLOSE( rsdLiftxTrue[0][0], rsdElemLO[0][0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftxTrue[1][0], rsdElemLO[1][0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftxTrue[0][1], rsdElemLO[0][1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftxTrue[1][1], rsdElemLO[1][1][0], small_tol, close_tol );
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_1D_Line_P1P2_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;
  typedef ElementLift<VectorArrayQ,TopoD1,Line> ElementrFieldClass;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementRFieldClass;

  typedef ElementXFieldClass::RefCoordType RefCoordType;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD1,Line,ElementParam> FieldWeightedClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real s0 = 0.25, sx = 0.6;
  Source1D_UniformGrad source(s0, sx);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  // solution
  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  qfldElem.DOF(0) = 2;
  qfldElem.DOF(1) = 3;

  // lifting operators
  ElementrFieldClass rfldElems(order, BasisFunctionCategory_Hierarchical);

  rfldElems[0].DOF(0) = -1;  rfldElems[0].DOF(1) = 5;
  rfldElems[1].DOF(0) =  3;  rfldElems[1].DOF(1) = 2;

  // sum of lifting operators
  ElementRFieldClass RfldElem(order, BasisFunctionCategory_Hierarchical);

  RfldElem.vectorViewDOF() = rfldElems[0].vectorViewDOF() +
                             rfldElems[1].vectorViewDOF();

  // weighting
  ElementQFieldClass wfldElem(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElem.order() );
  BOOST_CHECK_EQUAL( 3, wfldElem.nDOF() );

  wfldElem.DOF(0) = 3;
  wfldElem.DOF(1) = 4;
  wfldElem.DOF(2) = 5;

  // lifting operator weighting
  ElementrFieldClass sfldElems(order+1, BasisFunctionCategory_Hierarchical);

  sfldElems[0].DOF(0) = -5;  sfldElems[0].DOF(1) =  3;  sfldElems[0].DOF(2) =  2;
  sfldElems[1].DOF(0) =  2;  sfldElems[1].DOF(1) =  9;  sfldElems[1].DOF(2) =  3;

  // BR2 discretization (not used)
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  FieldWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem, rfldElems, wfldElem, sfldElems );

  //BOOST_CHECK( fcn.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  RefCoordType sRef;
  Real integrandPDETrue, integrandLiftTrue[2];
  FieldWeightedClass::IntegrandType integrand;

  // Test at {0}
  sRef = {0};
  fcn( sRef, integrand );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue = ( -231/5. ) + ( 44583/1000. ) + ( 69/10. ); // Weight function

  //Lifting Operator residual integrand: (lifting)
  integrandLiftTrue[0] =  ( 5 );   // Lifting Operator
  integrandLiftTrue[1] =  ( 6 );   // Lifting Operator

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftTrue[0], integrand.Lift[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftTrue[1], integrand.Lift[1], small_tol, close_tol );

  // Test at {1}
  sRef = {1};
  fcn( sRef, integrand );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue = ( 627/10. ) + ( -40337/1000. ) + ( 111/5. ); // Weight function

  //Lifting Operator residual integrand: (lifting)
  integrandLiftTrue[0] =  ( 15 );   // Lifting Operator
  integrandLiftTrue[1] =  ( 18 );   // Lifting Operator

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftTrue[0], integrand.Lift[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftTrue[1], integrand.Lift[1], small_tol, close_tol );

  // Test at {1/2}
  sRef = {1./2.};
  fcn( sRef, integrand );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue = ( -11/4. ) + ( 2123/1000. ) + ( 2669/80. ); // Weight function

  //Lifting Operator residual integrand: (lifting)
  integrandLiftTrue[0] =  ( 2 );   // Lifting Operator
  integrandLiftTrue[1] =  ( 85./4. );   // Lifting Operator

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftTrue[0], integrand.Lift[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftTrue[1], integrand.Lift[1], small_tol, close_tol );

  // test the element integral of the functor
  int quadratureorder = 3;
  ElementIntegral<TopoD1, Line, FieldWeightedClass::IntegrandType> integral(quadratureorder);

  FieldWeightedClass::IntegrandType rsdElem;

  // cell integration for canonical element
  integral( fcn, xfldElem, rsdElem );
  Real rsdPDETrue, rsdLO[2];

  //PDE residual: (advective) + (viscous) + (source)
  rsdPDETrue = ( 11/12. ) + ( 2123/1000. ) + ( 3251/120. ); // Weight function

  SANS_CHECK_CLOSE( rsdPDETrue, rsdElem.PDE, small_tol, close_tol );

  //Lifting Operator residual:
  rsdLO[0] = ( 14./3. ); // Lifting Operator
  rsdLO[1] = ( 109./6. ); // Lifting Operator

  SANS_CHECK_CLOSE( rsdLO[0], rsdElem.Lift[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLO[1], rsdElem.Lift[1], small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_1D_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;
  typedef ElementLift<VectorArrayQ,TopoD1,Line> ElementrFieldClass;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementRFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,Real,TopoD1,Line,ElementParam> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO <Real,TopoD1,Line> BasisWeightedLOClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD1,Line,ElementParam> FieldWeightedClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real s0 = 0.25, sx = 0.6;
  Source1D_UniformGrad source(s0, sx);

  PDEClass pde( adv, visc , source );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  for (int qorder = 2; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldClass qfldElem( qorder, BasisFunctionCategory_Hierarchical);
    ElementrFieldClass rfldElems(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldClass wfldElem( qorder, BasisFunctionCategory_Hierarchical);
    ElementrFieldClass sfldElems(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder,   qfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   wfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, wfldElem.nDOF() );

    BOOST_REQUIRE_EQUAL(wfldElem.nDOF(), qfldElem.nDOF());
    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
    {
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);
      wfldElem.DOF(dof) = 0;
      for (int trace = 0; trace < Line::NTrace; trace++ )
      {
        rfldElems[trace].DOF(dof) = (dof+1)*pow(-1,dof);
        sfldElems[trace].DOF(dof) = 0;
      }
    }

    // sum of lifting operators
    ElementRFieldClass RfldElem(qorder, BasisFunctionCategory_Hierarchical);

    RfldElem.vectorViewDOF() = rfldElems[0].vectorViewDOF() +
                               rfldElems[1].vectorViewDOF();

    // BR2 discretization (not used)
    Real viscousEtaParameter = 2;
    DiscretizationDGBR2 disc(0, viscousEtaParameter);

    IntegrandClass fcnint( pde, disc, {0} );

    BasisWeightedPDEClass fcnPDEB = fcnint.integrand_PDE( xfldElem, qfldElem, RfldElem );
    BasisWeightedLOClass fcnLOB = fcnint.integrand_LO( xfldElem, rfldElems );
    FieldWeightedClass fcnW = fcnint.integrand( xfldElem, qfldElem, rfldElems, wfldElem, sfldElems );

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;
    const int nIntegrand = qfldElem.nDOF();
    FieldWeightedClass::IntegrandType rsdElemW=0;
    std::vector<ArrayQ> rsdElemPDEB(nIntegrand,0); // trickery to get round the POD warning
    std::vector<BasisWeightedLOClass::IntegrandType> rsdElemLOB(nIntegrand,0); // trickery to get round the POD warning

    int quadratureorder = -1;
    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integralPDEB(quadratureorder, nIntegrand);
    GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedLOClass::IntegrandType> integralLOB(quadratureorder, nIntegrand);

    // cell integration for canonical element
    integralPDEB( fcnPDEB, xfldElem, rsdElemPDEB.data(), nIntegrand );
    integralLOB( fcnLOB, xfldElem, rsdElemLOB.data(), nIntegrand );

    ElementIntegral<TopoD1, Line, FieldWeightedClass::IntegrandType> integralW(quadratureorder);

    for (int i = 0; i < wfldElem.nDOF(); i++) // testing the wfld
    {
      // set just one of the weights to one
      wfldElem.DOF(i) = 1;
      for (int trace = 0; trace < Line::NTrace; trace ++ ) // wasteful, but need to loop here because it's a dot product inside
      {
        sfldElems[trace].DOF(i) = 1;

        // cell integration for canonical element
        rsdElemW = 0;
        integralW( fcnW, xfldElem, rsdElemW );

        // test the the two integrands are identical
        SANS_CHECK_CLOSE( rsdElemW.PDE, rsdElemPDEB[i], small_tol, close_tol );

        Real tmp = 0;
        for (int D = 0; D < PhysD1::D; D++ )          // [DOF][trace][d]
          tmp += rsdElemLOB[i][trace][D];

        SANS_CHECK_CLOSE( rsdElemW.Lift[trace], tmp, small_tol, close_tol )

        // reset to zero
        sfldElems[trace].DOF(i) = 0;
      }
      // reset to 0
      wfldElem.DOF(i) = 0;
    }
  }

}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_2D_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<2,ArrayQ> VectorArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> CellXFieldClass;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef ElementLift<VectorArrayQ,TopoD2,Triangle> ElementrFieldCell;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementRFieldCell;
  typedef ElementXFieldCell::RefCoordType RefCoordType;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,Real,TopoD2,Triangle,ElementXFieldCell>BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO <Real,TopoD2,Triangle>BasisWeightedLOClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Real s0 = 0.25, sx = 0.6, sy = -1.5;
  Source2D_UniformGrad source(s0, sx, sy);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid

  int order = 1;
  CellXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  // solution

  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  // lifting operators

  ElementrFieldCell rfldElems(order, BasisFunctionCategory_Hierarchical);

  rfldElems[0].DOF(0) = { 2, -3};  rfldElems[0].DOF(1) = { 7,  8};  rfldElems[0].DOF(2) = {-1,  7};
  rfldElems[1].DOF(0) = { 9,  6};  rfldElems[1].DOF(1) = {-1,  3};  rfldElems[1].DOF(2) = { 2,  3};
  rfldElems[2].DOF(0) = {-2,  1};  rfldElems[2].DOF(1) = { 4, -4};  rfldElems[2].DOF(2) = {-9, -5};

  // sum of lifting operators
  ElementRFieldCell RfldElem(order, BasisFunctionCategory_Hierarchical);

  RfldElem.vectorViewDOF() = rfldElems[0].vectorViewDOF() +
                             rfldElems[1].vectorViewDOF() +
                             rfldElems[2].vectorViewDOF();

  // BR2 discretization (not used)
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  BasisWeightedPDEClass fcnPDE = fcnint.integrand_PDE( xfldElem, qfldElem, RfldElem );
  BasisWeightedLOClass fcnLO = fcnint.integrand_LO( xfldElem, rfldElems );

  BOOST_CHECK_EQUAL( 1, fcnPDE.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcnPDE.nDOF() );
  BOOST_CHECK( fcnPDE.needsEvaluation() == true );
  BOOST_CHECK_EQUAL( 1, fcnLO.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcnLO.nDOF() );
  BOOST_CHECK( fcnLO.needsEvaluation() == true );

  const int nDOF = 3;
  const int nEdge = 3;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  RefCoordType sRef;
  Real integrandPDETrue[nDOF] = {0,0,0};
  Real integrandLiftxTrue[nDOF][nEdge], integrandLiftyTrue[nDOF][nEdge];
  ArrayQ integrandPDE[nDOF];
  BasisWeightedLOClass::IntegrandType integrandLO[nDOF];

  // Test at {0, 0}
  sRef = {0, 0};
  fcnPDE( sRef, integrandPDE, 3 );
  fcnLO( sRef, integrandLO, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 13/10. ) + ( -1254/125. ) + ( -73/20. ); // Basis function 1
  integrandPDETrue[1] = ( -11/10. ) + ( 1181/200. ) + ( 0 ); // Basis function 2
  integrandPDETrue[2] = ( -1/5. ) + ( 4127/1000. ) + ( 0 ); // Basis function 3

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );

  //Lifting Operator residual integrands:
  integrandLiftxTrue[0][0] = ( 2 );   // Basis function 1 Lifting Operator 1
  integrandLiftxTrue[0][1] = ( 9 );   // Basis function 1 Lifting Operator 2
  integrandLiftxTrue[0][2] = ( -2 );   // Basis function 1 Lifting Operator 3
  integrandLiftyTrue[0][0] = ( -3 );   // Basis function 1 Lifting Operator 1
  integrandLiftyTrue[0][1] = ( 6 );   // Basis function 1 Lifting Operator 2
  integrandLiftyTrue[0][2] = ( 1 );   // Basis function 1 Lifting Operator 3

  integrandLiftxTrue[1][0] = ( 0 );   // Basis function 2 Lifting Operator 1
  integrandLiftxTrue[1][1] = ( 0 );   // Basis function 2 Lifting Operator 2
  integrandLiftxTrue[1][2] = ( 0 );   // Basis function 2 Lifting Operator 3
  integrandLiftyTrue[1][0] = ( 0 );   // Basis function 2 Lifting Operator 1
  integrandLiftyTrue[1][1] = ( 0 );   // Basis function 2 Lifting Operator 2
  integrandLiftyTrue[1][2] = ( 0 );   // Basis function 2 Lifting Operator 3

  integrandLiftxTrue[2][0] = ( 0 );   // Basis function 3 Lifting Operator 1
  integrandLiftxTrue[2][1] = ( 0 );   // Basis function 3 Lifting Operator 2
  integrandLiftxTrue[2][2] = ( 0 );   // Basis function 3 Lifting Operator 3
  integrandLiftyTrue[2][0] = ( 0 );   // Basis function 3 Lifting Operator 1
  integrandLiftyTrue[2][1] = ( 0 );   // Basis function 3 Lifting Operator 2
  integrandLiftyTrue[2][2] = ( 0 );   // Basis function 3 Lifting Operator 3

  for (int edge =0; edge <nEdge; edge++)
  {
      for (int k =0; k <nDOF; k++)
      {
        SANS_CHECK_CLOSE( integrandLiftxTrue[k][edge], integrandLO[k][edge][0], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandLiftyTrue[k][edge], integrandLO[k][edge][1], small_tol, close_tol );
      }
  }

  // Test at {1, 0}
  sRef = {1, 0};
  fcnPDE( sRef, integrandPDE, 3 );
  fcnLO( sRef, integrandLO, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 39/10. ) + ( -1254/125. ) + ( 0 ); // Basis function 1
  integrandPDETrue[1] = ( -33/10. ) + ( 1181/200. ) + ( -141/20. ); // Basis function 2
  integrandPDETrue[2] = ( -3/5. ) + ( 4127/1000. ) + ( 0 ); // Basis function 3

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );

  //Lifting Operator residual integrands:
  integrandLiftxTrue[0][0] = ( 0 );   // Basis function 1 Lifting Operator 1
  integrandLiftxTrue[0][1] = ( 0 );   // Basis function 1 Lifting Operator 2
  integrandLiftxTrue[0][2] = ( 0 );   // Basis function 1 Lifting Operator 3
  integrandLiftyTrue[0][0] = ( 0 );   // Basis function 1 Lifting Operator 1
  integrandLiftyTrue[0][1] = ( 0 );   // Basis function 1 Lifting Operator 2
  integrandLiftyTrue[0][2] = ( 0 );   // Basis function 1 Lifting Operator 3

  integrandLiftxTrue[1][0] = ( 7 );   // Basis function 2 Lifting Operator 1
  integrandLiftxTrue[1][1] = ( -1 );   // Basis function 2 Lifting Operator 2
  integrandLiftxTrue[1][2] = ( 4 );   // Basis function 2 Lifting Operator 3
  integrandLiftyTrue[1][0] = ( 8 );   // Basis function 2 Lifting Operator 1
  integrandLiftyTrue[1][1] = ( 3 );   // Basis function 2 Lifting Operator 2
  integrandLiftyTrue[1][2] = ( -4 );   // Basis function 2 Lifting Operator 3

  integrandLiftxTrue[2][0] = ( 0 );   // Basis function 3 Lifting Operator 1
  integrandLiftxTrue[2][1] = ( 0 );   // Basis function 3 Lifting Operator 2
  integrandLiftxTrue[2][2] = ( 0 );   // Basis function 3 Lifting Operator 3
  integrandLiftyTrue[2][0] = ( 0 );   // Basis function 3 Lifting Operator 1
  integrandLiftyTrue[2][1] = ( 0 );   // Basis function 3 Lifting Operator 2
  integrandLiftyTrue[2][2] = ( 0 );   // Basis function 3 Lifting Operator 3

  for (int edge =0; edge <nEdge; edge++)
  {
      for (int k =0; k <nDOF; k++)
      {
        SANS_CHECK_CLOSE( integrandLiftxTrue[k][edge], integrandLO[k][edge][0], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandLiftyTrue[k][edge], integrandLO[k][edge][1], small_tol, close_tol );
      }
  }

  // Test at {0, 1}
  sRef = {0, 1};
  fcnPDE( sRef, integrandPDE, 3 );
  fcnLO( sRef, integrandLO, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 26/5. ) + ( -1254/125. ) + ( 0 ); // Basis function 1
  integrandPDETrue[1] = ( -22/5. ) + ( 1181/200. ) + ( 0 ); // Basis function 2
  integrandPDETrue[2] = ( -4/5. ) + ( 4127/1000. ) + ( -73/5. ); // Basis function 3

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );

  //Lifting Operator residual integrands:
  integrandLiftxTrue[0][0] = ( 0 );   // Basis function 1 Lifting Operator 1
  integrandLiftxTrue[0][1] = ( 0 );   // Basis function 1 Lifting Operator 2
  integrandLiftxTrue[0][2] = ( 0 );   // Basis function 1 Lifting Operator 3
  integrandLiftyTrue[0][0] = ( 0 );   // Basis function 1 Lifting Operator 1
  integrandLiftyTrue[0][1] = ( 0 );   // Basis function 1 Lifting Operator 2
  integrandLiftyTrue[0][2] = ( 0 );   // Basis function 1 Lifting Operator 3

  integrandLiftxTrue[1][0] = ( 0 );   // Basis function 2 Lifting Operator 1
  integrandLiftxTrue[1][1] = ( 0 );   // Basis function 2 Lifting Operator 2
  integrandLiftxTrue[1][2] = ( 0 );   // Basis function 2 Lifting Operator 3
  integrandLiftyTrue[1][0] = ( 0 );   // Basis function 2 Lifting Operator 1
  integrandLiftyTrue[1][1] = ( 0 );   // Basis function 2 Lifting Operator 2
  integrandLiftyTrue[1][2] = ( 0 );   // Basis function 2 Lifting Operator 3

  integrandLiftxTrue[2][0] = ( -1 );   // Basis function 3 Lifting Operator 1
  integrandLiftxTrue[2][1] = ( 2 );   // Basis function 3 Lifting Operator 2
  integrandLiftxTrue[2][2] = ( -9 );   // Basis function 3 Lifting Operator 3
  integrandLiftyTrue[2][0] = ( 7 );   // Basis function 3 Lifting Operator 1
  integrandLiftyTrue[2][1] = ( 3 );   // Basis function 3 Lifting Operator 2
  integrandLiftyTrue[2][2] = ( -5 );   // Basis function 3 Lifting Operator 3

  for (int edge =0; edge <nEdge; edge++)
  {
      for (int k =0; k <nDOF; k++)
      {
        SANS_CHECK_CLOSE( integrandLiftxTrue[k][edge], integrandLO[k][edge][0], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandLiftyTrue[k][edge], integrandLO[k][edge][1], small_tol, close_tol );
      }
  }

  // Test at {1/3, 1/3}
  sRef = {1./3., 1./3.};
  fcnPDE( sRef, integrandPDE, 3 );
  fcnLO( sRef, integrandLO, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 52/15. ) + ( -1254/125. ) + ( -253/90. ); // Basis function 1
  integrandPDETrue[1] = ( -44/15. ) + ( 1181/200. ) + ( -253/90. ); // Basis function 2
  integrandPDETrue[2] = ( -8/15. ) + ( 4127/1000. ) + ( -253/90. ); // Basis function 3

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );

  //Lifting Operator residual integrands:
  integrandLiftxTrue[0][0] = ( 8./9. );   // Basis function 1 Lifting Operator 1
  integrandLiftxTrue[0][1] = ( 10./9. );   // Basis function 1 Lifting Operator 2
  integrandLiftxTrue[0][2] = ( -7./9. );   // Basis function 1 Lifting Operator 3
  integrandLiftyTrue[0][0] = ( 4./3. );   // Basis function 1 Lifting Operator 1
  integrandLiftyTrue[0][1] = ( 4./3. );   // Basis function 1 Lifting Operator 2
  integrandLiftyTrue[0][2] = ( -8./9. );   // Basis function 1 Lifting Operator 3

  integrandLiftxTrue[1][0] = ( 8./9. );   // Basis function 2 Lifting Operator 1
  integrandLiftxTrue[1][1] = ( 10./9. );   // Basis function 2 Lifting Operator 2
  integrandLiftxTrue[1][2] = ( -7./9. );   // Basis function 2 Lifting Operator 3
  integrandLiftyTrue[1][0] = ( 4./3. );   // Basis function 2 Lifting Operator 1
  integrandLiftyTrue[1][1] = ( 4./3. );   // Basis function 2 Lifting Operator 2
  integrandLiftyTrue[1][2] = ( -8./9. );   // Basis function 2 Lifting Operator 3

  integrandLiftxTrue[2][0] = ( 8./9. );   // Basis function 3 Lifting Operator 1
  integrandLiftxTrue[2][1] = ( 10./9. );   // Basis function 3 Lifting Operator 2
  integrandLiftxTrue[2][2] = ( -7./9. );   // Basis function 3 Lifting Operator 3
  integrandLiftyTrue[2][0] = ( 4./3. );   // Basis function 3 Lifting Operator 1
  integrandLiftyTrue[2][1] = ( 4./3. );   // Basis function 3 Lifting Operator 2
  integrandLiftyTrue[2][2] = ( -8./9. );   // Basis function 3 Lifting Operator 3

  for (int edge =0; edge <nEdge; edge++)
  {
      for (int k =0; k <nDOF; k++)
      {
        SANS_CHECK_CLOSE( integrandLiftxTrue[k][edge], integrandLO[k][edge][0], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandLiftyTrue[k][edge], integrandLO[k][edge][1], small_tol, close_tol );
      }
  }

  // test the element integral of the functor

  // quadrature rule (for linear solution: basis grad is const, flux is linear)
  int quadratureorder = 2; //(integral of phi*lifting operator requires a quadratic quadrature order)
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralPDE(quadratureorder, nIntegrand);
  GalerkinWeightedIntegral<TopoD2, Triangle, BasisWeightedLOClass::IntegrandType> integralLO(quadratureorder, nIntegrand);

  ArrayQ rsdElemPDE[nDOF] = {0,0,0};
  BasisWeightedLOClass::IntegrandType rsdElemLO[nDOF] = {0,0,0};

  Real rsdPDETrue[nDOF], rsdxLiftTrue[nEdge][nDOF], rsdyLiftTrue[nEdge][nDOF];

  // cell integration for canonical element
  integralPDE( fcnPDE, xfldElem, rsdElemPDE, nIntegrand );
  integralLO( fcnLO, xfldElem, rsdElemLO, nIntegrand );

  //PDE residual: (advection) + (diffusion) + (source)
  rsdPDETrue[0] = (26/15.) + (-627/125.) + (-193/160.); // Basis function 1
  rsdPDETrue[1] = (-22/15.) + (1181/400.) + (-647/480.); // Basis function 2
  rsdPDETrue[2] = (-4/15.) + (4127/2000.) + (-133/80.); // Basis function 3

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdElemPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdElemPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[2], rsdElemPDE[2], small_tol, close_tol );

  // Lifting Operator residual:
  rsdxLiftTrue[0][0] = (5./12.);   // Basis function 1
  rsdyLiftTrue[0][0] = (3./8.);   // Basis function 1
  rsdxLiftTrue[0][1] = (5./8.);   // Basis function 2
  rsdyLiftTrue[0][1] = (5./6.);   // Basis function 2
  rsdxLiftTrue[0][2] = (7./24.);   // Basis function 3
  rsdyLiftTrue[0][2] = (19./24.);   // Basis function 3
  rsdxLiftTrue[1][0] = (19./24.);   // Basis function 1
  rsdyLiftTrue[1][0] = (3./4.);   // Basis function 1
  rsdxLiftTrue[1][1] = (3./8.);   // Basis function 2
  rsdyLiftTrue[1][1] = (5./8.);   // Basis function 2
  rsdxLiftTrue[1][2] = (1./2.);   // Basis function 3
  rsdyLiftTrue[1][2] = (5./8.);   // Basis function 3
  rsdxLiftTrue[2][0] = (-3./8.);   // Basis function 1
  rsdyLiftTrue[2][0] = (-7./24.);   // Basis function 1
  rsdxLiftTrue[2][1] = (-1./8.);   // Basis function 2
  rsdyLiftTrue[2][1] = (-1./2.);   // Basis function 2
  rsdxLiftTrue[2][2] = (-2./3.);   // Basis function 3
  rsdyLiftTrue[2][2] = (-13./24.);   // Basis function 3

  for (int edge =0; edge <nEdge; edge++)
  {
      for (int k =0; k <nDOF; k++)
      {
        SANS_CHECK_CLOSE( rsdxLiftTrue[edge][k], rsdElemLO[k][edge][0], small_tol, close_tol );
        SANS_CHECK_CLOSE( rsdyLiftTrue[edge][k], rsdElemLO[k][edge][1], small_tol, close_tol );
      }
  }
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_2D_Triangle_P1P2_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;
  typedef ElementLift<VectorArrayQ,TopoD2,Triangle> ElementRFieldClass;

  typedef ElementXFieldClass::RefCoordType RefCoordType;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD2,Triangle,ElementParam> FieldWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real s0 = 0.25, sx = 0.6, sy = -1.5;
  Source2D_UniformGrad source(s0, sx, sy);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid
  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  // solution
  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  // lifting operators
  ElementRFieldClass rfldElems(order, BasisFunctionCategory_Hierarchical);

  rfldElems[0].DOF(0) = { 2, -3};  rfldElems[0].DOF(1) = { 7,  8};  rfldElems[0].DOF(2) = {-1,  7};
  rfldElems[1].DOF(0) = { 9,  6};  rfldElems[1].DOF(1) = {-1,  3};  rfldElems[1].DOF(2) = { 2,  3};
  rfldElems[2].DOF(0) = {-2,  1};  rfldElems[2].DOF(1) = { 4, -4};  rfldElems[2].DOF(2) = {-9, -5};

  // weighting
  ElementQFieldClass wfldElem(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElem.order() );
  BOOST_CHECK_EQUAL( 6, wfldElem.nDOF() );

  // triangle solution
  wfldElem.DOF(0) = -2;
  wfldElem.DOF(1) =  4;
  wfldElem.DOF(2) =  3;
  wfldElem.DOF(3) =  2;
  wfldElem.DOF(4) =  4;
  wfldElem.DOF(5) = -1;

  // Lifting Operator Weighting
  ElementRFieldClass sfldElems(order+1, BasisFunctionCategory_Hierarchical);

  sfldElems[0].DOF(0) = { 1,  5};  sfldElems[0].DOF(1) = { 3,  6};  sfldElems[0].DOF(2) = {-1,  7};
  sfldElems[0].DOF(3) = { 3,  1};  sfldElems[0].DOF(4) = { 2,  2};  sfldElems[0].DOF(5) = { 4,  3};

  sfldElems[1].DOF(0) = { 5,  6};  sfldElems[1].DOF(1) = { 4,  5};  sfldElems[1].DOF(2) = { 3,  4};
  sfldElems[1].DOF(3) = { 4,  3};  sfldElems[1].DOF(4) = { 3,  2};  sfldElems[1].DOF(5) = { 2,  1};

  sfldElems[2].DOF(0) = {-2, -1};  sfldElems[2].DOF(1) = {-3,  2};  sfldElems[2].DOF(2) = {-4, -3};
  sfldElems[2].DOF(3) = {-1, -3};  sfldElems[2].DOF(4) = {-2, -2};  sfldElems[2].DOF(5) = {-3, -1};

  // BR2 discretization (not used)
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  FieldWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem, rfldElems, wfldElem, sfldElems );

  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  const int nEdge = 3;
  RefCoordType sRef;
  Real integrandPDETrue, integrandLiftTrue[3];
  FieldWeightedClass::IntegrandType integrand;

  // Test at {0, 0}
  sRef = {0, 0};
  fcn( sRef, integrand );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue = ( -32/5. ) + ( 108389/1000. ) + ( 73/10. ); // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //Lifting Operator residual integrands:
  integrandLiftTrue[0] = ( -13 );   // Weight function
  integrandLiftTrue[1] = ( 81 );   // Weight function
  integrandLiftTrue[2] = ( 3 );   // Weight function

  for (int edge =0; edge <nEdge; edge++)
      SANS_CHECK_CLOSE( integrandLiftTrue[edge], integrand.Lift[edge], small_tol, close_tol );

  // Test at {1, 0}
  sRef = {1, 0};
  fcn( sRef, integrand );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue = ( -216/5. ) + ( 137233/1000. ) + ( -141/5. ); // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //Lifting Operator residual integrands:
  integrandLiftTrue[0] = ( 69 );   // Weight function
  integrandLiftTrue[1] = ( 11 );   // Weight function
  integrandLiftTrue[2] = ( -20 );   // Weight function

  for (int edge =0; edge <nEdge; edge++)
      SANS_CHECK_CLOSE( integrandLiftTrue[edge], integrand.Lift[edge], small_tol, close_tol );

  // Test at {0, 1}
  sRef = {0, 1};
  fcn( sRef, integrand );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue = ( 88/5. ) + ( -62399/1000. ) + ( -219/5. ); // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //Lifting Operator residual integrands:
  integrandLiftTrue[0] = ( 50 );   // Weight function
  integrandLiftTrue[1] = ( 18 );   // Weight function
  integrandLiftTrue[2] = ( 51 );   // Weight function

  for (int edge =0; edge <nEdge; edge++)
      SANS_CHECK_CLOSE( integrandLiftTrue[edge], integrand.Lift[edge], small_tol, close_tol );

  // Test at {1/3, 1/3}
  sRef = {1/3., 1/3.};
  fcn( sRef, integrand );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue = ( -656/45. ) + ( 183223/3000. ) + ( -1771/54. ); // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //Lifting Operator residual integrands:
  integrandLiftTrue[0] = ( 48 );   // Weight function
  integrandLiftTrue[1] = ( 172/3. );   // Weight function
  integrandLiftTrue[2] = ( 199/9. );   // Weight function

  for (int edge =0; edge <nEdge; edge++)
      SANS_CHECK_CLOSE( integrandLiftTrue[edge], integrand.Lift[edge], small_tol, close_tol );

  // test the element integral of the functor
  // quadrature rule (for linear solution: basis grad is const, flux is linear)
  int quadratureorder = 3;
  ElementIntegral<TopoD2, Triangle, FieldWeightedClass::IntegrandType> integral(quadratureorder);

  FieldWeightedClass::IntegrandType rsdElem = 0;

  // cell integration for canonical element
  integral( fcn, xfldElem, rsdElem );

  Real rsdPDETrue, rsdLiftTrue[3];

  //PDE residual: (advection) + (diffusion) + (source)
  rsdPDETrue = (-34/5.) + (183223/6000.) + (-4711/300.);

  SANS_CHECK_CLOSE( rsdPDETrue, rsdElem.PDE, small_tol, close_tol );

  // Lifting Operator residual:
  rsdLiftTrue[0] = (447/20.);   // Weight function
  rsdLiftTrue[1] = (103/4.);   // Weight function
  rsdLiftTrue[2] = (387/40.);   // Weight function

  for (int edge =0; edge <nEdge; edge++)
      SANS_CHECK_CLOSE( rsdLiftTrue[edge], rsdElem.Lift[edge], small_tol, close_tol );

}


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_2D_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;
  typedef ElementLift<VectorArrayQ,TopoD2,Triangle> ElementrFieldClass;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementRFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,Real,TopoD2,Triangle,ElementParam > BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO <Real,TopoD2,Triangle> BasisWeightedLOClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD2,Triangle,ElementParam > FieldWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Real s0 = 0.25, sx = 0.6, sy = -1.5;
  Source2D_UniformGrad source(s0, sx, sy);

  PDEClass pde( adv, visc , source );

    // static tests
    BOOST_CHECK( pde.D == 2 );
    BOOST_CHECK( pde.N == 1 );

    // flux components
    BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
    BOOST_CHECK( pde.hasFluxAdvective() == true );
    BOOST_CHECK( pde.hasFluxViscous() == true );
    BOOST_CHECK( pde.hasSource() == true );
    BOOST_CHECK( pde.hasForcingFunction() == false );

    BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

    // grid
    int order = 1;
    ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( 1, xfldElem.order() );
    BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

    // triangle grid
    Real x1, x2, x3, y1, y2, y3;

    x1 = 0;  y1 = 0;
    x2 = 1;  y2 = 0;
    x3 = 0;  y3 = 1;

    xfldElem.DOF(0) = {x1, y1};
    xfldElem.DOF(1) = {x2, y2};
    xfldElem.DOF(2) = {x3, y3};

  for (int qorder = 1; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldClass qfldElem( qorder, BasisFunctionCategory_Hierarchical);
    ElementrFieldClass rfldElems(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldClass wfldElem( qorder, BasisFunctionCategory_Hierarchical);
    ElementrFieldClass sfldElems(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder,   qfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   wfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, wfldElem.nDOF() );

    BOOST_REQUIRE_EQUAL(wfldElem.nDOF(), qfldElem.nDOF());
    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
    {
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);
      wfldElem.DOF(dof) = 0;
      for (int trace = 0; trace < Triangle::NTrace; trace++ )
      {
        rfldElems[trace].DOF(dof) = (dof+1)*pow(-1,dof);
        sfldElems[trace].DOF(dof) = 0;
      }
    }

    // sum of lifting operators
    ElementRFieldClass RfldElem(qorder, BasisFunctionCategory_Hierarchical);

    RfldElem.vectorViewDOF() = rfldElems[0].vectorViewDOF() +
                               rfldElems[1].vectorViewDOF() +
                               rfldElems[2].vectorViewDOF();

    // BR2 discretization (not used)
    Real viscousEtaParameter = 6;
    DiscretizationDGBR2 disc(0, viscousEtaParameter);

    IntegrandClass fcnint( pde, disc, {0} );

    BasisWeightedPDEClass fcnPDEB = fcnint.integrand_PDE( xfldElem, qfldElem, RfldElem );
    BasisWeightedLOClass fcnLOB = fcnint.integrand_LO( xfldElem, rfldElems );
    FieldWeightedClass fcnW = fcnint.integrand( xfldElem, qfldElem, rfldElems, wfldElem, sfldElems );

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;
    const int nIntegrand = qfldElem.nDOF();
    FieldWeightedClass::IntegrandType rsdElemW=0;
    std::vector<ArrayQ> rsdElemPDEB(nIntegrand,0); // trickery to get round the POD warning
    std::vector<BasisWeightedLOClass::IntegrandType> rsdElemLOB(nIntegrand,0); // trickery to get round the POD warning

    int quadratureorder = -1;
    GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralPDEB(quadratureorder, nIntegrand);
    GalerkinWeightedIntegral<TopoD2, Triangle, BasisWeightedLOClass::IntegrandType> integralLOB(quadratureorder, nIntegrand);

    // cell integration for canonical element
    integralPDEB( fcnPDEB, xfldElem, rsdElemPDEB.data(), nIntegrand );
    integralLOB( fcnLOB, xfldElem, rsdElemLOB.data(), nIntegrand );

    ElementIntegral<TopoD2, Triangle, FieldWeightedClass::IntegrandType> integralW(quadratureorder);

    for (int i = 0; i < wfldElem.nDOF(); i++) // testing the wfld
    {
      // set just one of the weights to one
      wfldElem.DOF(i) = 1;
      for (int trace = 0; trace < Triangle::NTrace; trace ++ ) // wasteful, but need to loop here because it's a dot product inside
      {
        sfldElems[trace].DOF(i) = 1;

        // cell integration for canonical element
        rsdElemW = 0;
        integralW( fcnW, xfldElem, rsdElemW );

        // test the the two integrands are identical
        SANS_CHECK_CLOSE( rsdElemW.PDE, rsdElemPDEB[i], small_tol, close_tol );

        Real tmp = 0;
        for (int D = 0; D < PhysD2::D; D++ )
          tmp += rsdElemLOB[i][trace][D];

        SANS_CHECK_CLOSE( rsdElemW.Lift[trace], tmp, small_tol, close_tol )

        // reset to zero
        sfldElems[trace].DOF(i) = 0;
      }
      // reset to 0
      wfldElem.DOF(i) = 0;
    }
  }

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_3D_Tet_test )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_UniformGrad > PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEAdvectionDiffusion3D::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<3,ArrayQ> VectorArrayQ;

  typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD3,Tet> ElementQFieldCell;
  typedef ElementLift<VectorArrayQ,TopoD3,Tet> ElementrFieldCell;
  typedef Element<VectorArrayQ,TopoD3,Tet> ElementRFieldCell;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,Real,TopoD3,Tet,ElementXFieldCell>BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO<Real,TopoD3,Tet>BasisWeightedLOClass;
  typedef ElementXFieldCell::RefCoordType RefCoordType;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.7;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123; Real kxy = 0.553; Real kxz = 0.760;
  Real kyx = 0.553; Real kyy = 1.007; Real kyz = 0.365;
  Real kzx = 0.760; Real kzy = 0.365; Real kzz = 1.460;

  ViscousFlux3D_Uniform visc(kxx, kxy, kxz,
                                 kyx, kyy, kyz,
                                 kzx, kzy, kzz);

  Real s0 = 0.25, sx = 0.6, sy = -1.5, sz = 0.1;
  Source3D_UniformGrad source(s0, sx, sy, sz);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid

  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 4, xfldElem.nDOF() );

  // tet grid
  Real v1[3] = {0,0,0};
  Real v2[3] = {1,0,0};
  Real v3[3] = {0,1,0};
  Real v4[3] = {0,0,1};

  xfldElem.DOF(0) = {v1[0], v1[1], v1[2]};
  xfldElem.DOF(1) = {v2[0], v2[1], v2[2]};
  xfldElem.DOF(2) = {v3[0], v3[1], v3[2]};
  xfldElem.DOF(3) = {v4[0], v4[1], v4[2]};

  // solution

  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 4, qfldElem.nDOF() );

  // tet solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 6;
  qfldElem.DOF(3) = 4;


  // lifting operators

  ElementrFieldCell rfldElems(order, BasisFunctionCategory_Hierarchical);

  rfldElems[0].DOF(0) = { 2, -3, -7};  rfldElems[0].DOF(1) = { 7,  8,  5};  rfldElems[0].DOF(2) = { 4,  1,  2};  rfldElems[0].DOF(3) = {-1,  7,  1};
  rfldElems[1].DOF(0) = { 9,  6,  3};  rfldElems[1].DOF(1) = {-1,  3,  7};  rfldElems[1].DOF(2) = { 3, -1, -2};  rfldElems[1].DOF(3) = { 2,  3,  4};
  rfldElems[2].DOF(0) = {-2,  1,  5};  rfldElems[2].DOF(1) = { 4, -4, -3};  rfldElems[2].DOF(2) = {-1,  2,  1};  rfldElems[2].DOF(3) = {-9, -5, -8};
  rfldElems[3].DOF(0) = { 2, -1, -1};  rfldElems[3].DOF(1) = { 1,  2,  6};  rfldElems[3].DOF(2) = { 3, -3, -4};  rfldElems[3].DOF(3) = {-4,  5,  2};

  // sum of lifting operators
  ElementRFieldCell RfldElem(order, BasisFunctionCategory_Hierarchical);

  RfldElem.vectorViewDOF() = rfldElems[0].vectorViewDOF() +
                             rfldElems[1].vectorViewDOF() +
                             rfldElems[2].vectorViewDOF() +
                             rfldElems[3].vectorViewDOF();

  // BR2 discretization (not used)
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  BasisWeightedPDEClass fcnPDE = fcnint.integrand_PDE( xfldElem, qfldElem, RfldElem );
  BasisWeightedLOClass fcnLO = fcnint.integrand_LO( xfldElem, rfldElems );

  BOOST_CHECK_EQUAL( 1, fcnPDE.nEqn() );
  BOOST_CHECK_EQUAL( 4, fcnPDE.nDOF() );
  BOOST_CHECK( fcnPDE.needsEvaluation() == true );
  BOOST_CHECK_EQUAL( 1, fcnLO.nEqn() );
  BOOST_CHECK_EQUAL( 4, fcnLO.nDOF() );
  BOOST_CHECK( fcnLO.needsEvaluation() == true );

  const int nDOF = 4;
  const int nFace = 4;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  RefCoordType sRef;
  ArrayQ integrandLiftxTrue[nDOF][nFace], integrandLiftyTrue[nDOF][nFace], integrandLiftzTrue[nDOF][nFace];
  Real integrandPDETrue[nDOF] = {0,0,0,0};
  ArrayQ integrandPDE[nDOF];
  BasisWeightedLOClass::IntegrandType integrandLO[nDOF];

  // Test at {0, 0, 0}
  sRef = {0, 0, 0};
  fcnPDE( sRef, integrandPDE, 4 );
  fcnLO( sRef, integrandLO, 4 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 2 ) + ( -6063/250. ) + ( -73/20. ); // Basis function 1
  integrandPDETrue[1] = ( -11/10. ) + ( 9291/1000. ) + ( 0 ); // Basis function 2
  integrandPDETrue[2] = ( -1/5. ) + ( 1809/250. ) + ( 0 ); // Basis function 3
  integrandPDETrue[3] = ( -7/10. ) + ( 309/40. ) + ( 0 ); // Basis function 4

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDE[3], small_tol, close_tol );

  //Lifting Operator residual integrands:
  integrandLiftxTrue[0][0] = ( 2 );   // Basis function 1 Lifting Operator 1
  integrandLiftxTrue[1][0] = ( 0 );   // Basis function 2 Lifting Operator 1
  integrandLiftxTrue[2][0] = ( 0 );   // Basis function 3 Lifting Operator 1
  integrandLiftxTrue[3][0] = ( 0 );   // Basis function 4 Lifting Operator 1
  integrandLiftyTrue[0][0] = ( -3 );   // Basis function 1 Lifting Operator 1
  integrandLiftyTrue[1][0] = ( 0 );   // Basis function 2 Lifting Operator 1
  integrandLiftyTrue[2][0] = ( 0 );   // Basis function 3 Lifting Operator 1
  integrandLiftyTrue[3][0] = ( 0 );   // Basis function 4 Lifting Operator 1
  integrandLiftzTrue[0][0] = ( -7 );   // Basis function 1 Lifting Operator 1
  integrandLiftzTrue[1][0] = ( 0 );   // Basis function 2 Lifting Operator 1
  integrandLiftzTrue[2][0] = ( 0 );   // Basis function 3 Lifting Operator 1
  integrandLiftzTrue[3][0] = ( 0 );   // Basis function 4 Lifting Operator 1

  integrandLiftxTrue[0][1] = ( 9 );   // Basis function 1 Lifting Operator 2
  integrandLiftxTrue[1][1] = ( 0 );   // Basis function 2 Lifting Operator 2
  integrandLiftxTrue[2][1] = ( 0 );   // Basis function 3 Lifting Operator 2
  integrandLiftxTrue[3][1] = ( 0 );   // Basis function 4 Lifting Operator 2
  integrandLiftyTrue[0][1] = ( 6 );   // Basis function 1 Lifting Operator 2
  integrandLiftyTrue[1][1] = ( 0 );   // Basis function 2 Lifting Operator 2
  integrandLiftyTrue[2][1] = ( 0 );   // Basis function 3 Lifting Operator 2
  integrandLiftyTrue[3][1] = ( 0 );   // Basis function 4 Lifting Operator 2
  integrandLiftzTrue[0][1] = ( 3 );   // Basis function 1 Lifting Operator 2
  integrandLiftzTrue[1][1] = ( 0 );   // Basis function 2 Lifting Operator 2
  integrandLiftzTrue[2][1] = ( 0 );   // Basis function 3 Lifting Operator 2
  integrandLiftzTrue[3][1] = ( 0 );   // Basis function 4 Lifting Operator 2

  integrandLiftxTrue[0][2] = ( -2 );   // Basis function 1 Lifting Operator 3
  integrandLiftxTrue[1][2] = ( 0 );   // Basis function 2 Lifting Operator 3
  integrandLiftxTrue[2][2] = ( 0 );   // Basis function 3 Lifting Operator 3
  integrandLiftxTrue[3][2] = ( 0 );   // Basis function 4 Lifting Operator 3
  integrandLiftyTrue[0][2] = ( 1 );   // Basis function 1 Lifting Operator 3
  integrandLiftyTrue[1][2] = ( 0 );   // Basis function 2 Lifting Operator 3
  integrandLiftyTrue[2][2] = ( 0 );   // Basis function 3 Lifting Operator 3
  integrandLiftyTrue[3][2] = ( 0 );   // Basis function 4 Lifting Operator 3
  integrandLiftzTrue[0][2] = ( 5 );   // Basis function 1 Lifting Operator 3
  integrandLiftzTrue[1][2] = ( 0 );   // Basis function 2 Lifting Operator 3
  integrandLiftzTrue[2][2] = ( 0 );   // Basis function 3 Lifting Operator 3
  integrandLiftzTrue[3][2] = ( 0 );   // Basis function 4 Lifting Operator 3

  integrandLiftxTrue[0][3] = ( 2 );   // Basis function 1 Lifting Operator 4
  integrandLiftxTrue[1][3] = ( 0 );   // Basis function 2 Lifting Operator 4
  integrandLiftxTrue[2][3] = ( 0 );   // Basis function 3 Lifting Operator 4
  integrandLiftxTrue[3][3] = ( 0 );   // Basis function 4 Lifting Operator 4
  integrandLiftyTrue[0][3] = ( -1 );   // Basis function 1 Lifting Operator 4
  integrandLiftyTrue[1][3] = ( 0 );   // Basis function 2 Lifting Operator 4
  integrandLiftyTrue[2][3] = ( 0 );   // Basis function 3 Lifting Operator 4
  integrandLiftyTrue[3][3] = ( 0 );   // Basis function 4 Lifting Operator 4
  integrandLiftzTrue[0][3] = ( -1 );   // Basis function 1 Lifting Operator 4
  integrandLiftzTrue[1][3] = ( 0 );   // Basis function 2 Lifting Operator 4
  integrandLiftzTrue[2][3] = ( 0 );   // Basis function 3 Lifting Operator 4
  integrandLiftzTrue[3][3] = ( 0 );   // Basis function 4 Lifting Operator 4

  for (int face = 0; face < nFace; face++)
  {
      for (int k = 0; k < nDOF; k++)
      {                                                          // [DOF][trace][d]
        SANS_CHECK_CLOSE( integrandLiftxTrue[k][face], integrandLO[k][face][0], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandLiftyTrue[k][face], integrandLO[k][face][1], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandLiftzTrue[k][face], integrandLO[k][face][2], small_tol, close_tol );
      }
  }


  // Test at {1, 0, 0}
  sRef = {1, 0, 0};
  fcnPDE( sRef, integrandPDE, 4 );
  fcnLO( sRef, integrandLO, 4 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 6 ) + ( -6063/250. ) + ( 0 ); // Basis function 1
  integrandPDETrue[1] = ( -33/10. ) + ( 9291/1000. ) + ( -213/20. ); // Basis function 2
  integrandPDETrue[2] = ( -3/5. ) + ( 1809/250. ) + ( 0 ); // Basis function 3
  integrandPDETrue[3] = ( -21/10. ) + ( 309/40. ) + ( 0 ); // Basis function 4

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDE[3], small_tol, close_tol );

  //Lifting Operator residual integrands:
  integrandLiftxTrue[0][0] = ( 0 );   // Basis function 1 Lifting Operator 1
  integrandLiftxTrue[1][0] = ( 7 );   // Basis function 2 Lifting Operator 1
  integrandLiftxTrue[2][0] = ( 0 );   // Basis function 3 Lifting Operator 1
  integrandLiftxTrue[3][0] = ( 0 );   // Basis function 4 Lifting Operator 1
  integrandLiftyTrue[0][0] = ( 0 );   // Basis function 1 Lifting Operator 1
  integrandLiftyTrue[1][0] = ( 8 );   // Basis function 2 Lifting Operator 1
  integrandLiftyTrue[2][0] = ( 0 );   // Basis function 3 Lifting Operator 1
  integrandLiftyTrue[3][0] = ( 0 );   // Basis function 4 Lifting Operator 1
  integrandLiftzTrue[0][0] = ( 0 );   // Basis function 1 Lifting Operator 1
  integrandLiftzTrue[1][0] = ( 5 );   // Basis function 2 Lifting Operator 1
  integrandLiftzTrue[2][0] = ( 0 );   // Basis function 3 Lifting Operator 1
  integrandLiftzTrue[3][0] = ( 0 );   // Basis function 4 Lifting Operator 1

  integrandLiftxTrue[0][1] = ( 0 );   // Basis function 1 Lifting Operator 2
  integrandLiftxTrue[1][1] = ( -1 );   // Basis function 2 Lifting Operator 2
  integrandLiftxTrue[2][1] = ( 0 );   // Basis function 3 Lifting Operator 2
  integrandLiftxTrue[3][1] = ( 0 );   // Basis function 4 Lifting Operator 2
  integrandLiftyTrue[0][1] = ( 0 );   // Basis function 1 Lifting Operator 2
  integrandLiftyTrue[1][1] = ( 3 );   // Basis function 2 Lifting Operator 2
  integrandLiftyTrue[2][1] = ( 0 );   // Basis function 3 Lifting Operator 2
  integrandLiftyTrue[3][1] = ( 0 );   // Basis function 4 Lifting Operator 2
  integrandLiftzTrue[0][1] = ( 0 );   // Basis function 1 Lifting Operator 2
  integrandLiftzTrue[1][1] = ( 7 );   // Basis function 2 Lifting Operator 2
  integrandLiftzTrue[2][1] = ( 0 );   // Basis function 3 Lifting Operator 2
  integrandLiftzTrue[3][1] = ( 0 );   // Basis function 4 Lifting Operator 2

  integrandLiftxTrue[0][2] = ( 0 );   // Basis function 1 Lifting Operator 3
  integrandLiftxTrue[1][2] = ( 4 );   // Basis function 2 Lifting Operator 3
  integrandLiftxTrue[2][2] = ( 0 );   // Basis function 3 Lifting Operator 3
  integrandLiftxTrue[3][2] = ( 0 );   // Basis function 4 Lifting Operator 3
  integrandLiftyTrue[0][2] = ( 0 );   // Basis function 1 Lifting Operator 3
  integrandLiftyTrue[1][2] = ( -4 );   // Basis function 2 Lifting Operator 3
  integrandLiftyTrue[2][2] = ( 0 );   // Basis function 3 Lifting Operator 3
  integrandLiftyTrue[3][2] = ( 0 );   // Basis function 4 Lifting Operator 3
  integrandLiftzTrue[0][2] = ( 0 );   // Basis function 1 Lifting Operator 3
  integrandLiftzTrue[1][2] = ( -3 );   // Basis function 2 Lifting Operator 3
  integrandLiftzTrue[2][2] = ( 0 );   // Basis function 3 Lifting Operator 3
  integrandLiftzTrue[3][2] = ( 0 );   // Basis function 4 Lifting Operator 3

  integrandLiftxTrue[0][3] = ( 0 );   // Basis function 1 Lifting Operator 4
  integrandLiftxTrue[1][3] = ( 1 );   // Basis function 2 Lifting Operator 4
  integrandLiftxTrue[2][3] = ( 0 );   // Basis function 3 Lifting Operator 4
  integrandLiftxTrue[3][3] = ( 0 );   // Basis function 4 Lifting Operator 4
  integrandLiftyTrue[0][3] = ( 0 );   // Basis function 1 Lifting Operator 4
  integrandLiftyTrue[1][3] = ( 2 );   // Basis function 2 Lifting Operator 4
  integrandLiftyTrue[2][3] = ( 0 );   // Basis function 3 Lifting Operator 4
  integrandLiftyTrue[3][3] = ( 0 );   // Basis function 4 Lifting Operator 4
  integrandLiftzTrue[0][3] = ( 0 );   // Basis function 1 Lifting Operator 4
  integrandLiftzTrue[1][3] = ( 6 );   // Basis function 2 Lifting Operator 4
  integrandLiftzTrue[2][3] = ( 0 );   // Basis function 3 Lifting Operator 4
  integrandLiftzTrue[3][3] = ( 0 );   // Basis function 4 Lifting Operator 4

  for (int face = 0; face < nFace; face++)
  {
      for (int k = 0; k < nDOF; k++)
      {                                                          // [DOF][trace][d]
        SANS_CHECK_CLOSE( integrandLiftxTrue[k][face], integrandLO[k][face][0], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandLiftyTrue[k][face], integrandLO[k][face][1], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandLiftzTrue[k][face], integrandLO[k][face][2], small_tol, close_tol );
      }
  }


  // Test at {0, 1, 0}
  sRef = {0, 1, 0};
  fcnPDE( sRef, integrandPDE, 4 );
  fcnLO( sRef, integrandLO, 4 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 12 ) + ( -6063/250. ) + ( 0 ); // Basis function 1
  integrandPDETrue[1] = ( -33/5. ) + ( 9291/1000. ) + ( 0 ); // Basis function 2
  integrandPDETrue[2] = ( -6/5. ) + ( 1809/250. ) + ( 21/10. ); // Basis function 3
  integrandPDETrue[3] = ( -21/5. ) + ( 309/40. ) + ( 0 ); // Basis function 4

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDE[3], small_tol, close_tol );

  //Lifting Operator residual integrands:
  integrandLiftxTrue[0][0] = ( 0 );   // Basis function 1 Lifting Operator 1
  integrandLiftxTrue[1][0] = ( 0 );   // Basis function 2 Lifting Operator 1
  integrandLiftxTrue[2][0] = ( 4 );   // Basis function 3 Lifting Operator 1
  integrandLiftxTrue[3][0] = ( 0 );   // Basis function 4 Lifting Operator 1
  integrandLiftyTrue[0][0] = ( 0 );   // Basis function 1 Lifting Operator 1
  integrandLiftyTrue[1][0] = ( 0 );   // Basis function 2 Lifting Operator 1
  integrandLiftyTrue[2][0] = ( 1 );   // Basis function 3 Lifting Operator 1
  integrandLiftyTrue[3][0] = ( 0 );   // Basis function 4 Lifting Operator 1
  integrandLiftzTrue[0][0] = ( 0 );   // Basis function 1 Lifting Operator 1
  integrandLiftzTrue[1][0] = ( 0 );   // Basis function 2 Lifting Operator 1
  integrandLiftzTrue[2][0] = ( 2 );   // Basis function 3 Lifting Operator 1
  integrandLiftzTrue[3][0] = ( 0 );   // Basis function 4 Lifting Operator 1

  integrandLiftxTrue[0][1] = ( 0 );   // Basis function 1 Lifting Operator 2
  integrandLiftxTrue[1][1] = ( 0 );   // Basis function 2 Lifting Operator 2
  integrandLiftxTrue[2][1] = ( 3 );   // Basis function 3 Lifting Operator 2
  integrandLiftxTrue[3][1] = ( 0 );   // Basis function 4 Lifting Operator 2
  integrandLiftyTrue[0][1] = ( 0 );   // Basis function 1 Lifting Operator 2
  integrandLiftyTrue[1][1] = ( 0 );   // Basis function 2 Lifting Operator 2
  integrandLiftyTrue[2][1] = ( -1 );   // Basis function 3 Lifting Operator 2
  integrandLiftyTrue[3][1] = ( 0 );   // Basis function 4 Lifting Operator 2
  integrandLiftzTrue[0][1] = ( 0 );   // Basis function 1 Lifting Operator 2
  integrandLiftzTrue[1][1] = ( 0 );   // Basis function 2 Lifting Operator 2
  integrandLiftzTrue[2][1] = ( -2 );   // Basis function 3 Lifting Operator 2
  integrandLiftzTrue[3][1] = ( 0 );   // Basis function 4 Lifting Operator 2

  integrandLiftxTrue[0][2] = ( 0 );   // Basis function 1 Lifting Operator 3
  integrandLiftxTrue[1][2] = ( 0 );   // Basis function 2 Lifting Operator 3
  integrandLiftxTrue[2][2] = ( -1 );   // Basis function 3 Lifting Operator 3
  integrandLiftxTrue[3][2] = ( 0 );   // Basis function 4 Lifting Operator 3
  integrandLiftyTrue[0][2] = ( 0 );   // Basis function 1 Lifting Operator 3
  integrandLiftyTrue[1][2] = ( 0 );   // Basis function 2 Lifting Operator 3
  integrandLiftyTrue[2][2] = ( 2 );   // Basis function 3 Lifting Operator 3
  integrandLiftyTrue[3][2] = ( 0 );   // Basis function 4 Lifting Operator 3
  integrandLiftzTrue[0][2] = ( 0 );   // Basis function 1 Lifting Operator 3
  integrandLiftzTrue[1][2] = ( 0 );   // Basis function 2 Lifting Operator 3
  integrandLiftzTrue[2][2] = ( 1 );   // Basis function 3 Lifting Operator 3
  integrandLiftzTrue[3][2] = ( 0 );   // Basis function 4 Lifting Operator 3

  integrandLiftxTrue[0][3] = ( 0 );   // Basis function 1 Lifting Operator 4
  integrandLiftxTrue[1][3] = ( 0 );   // Basis function 2 Lifting Operator 4
  integrandLiftxTrue[2][3] = ( 3 );   // Basis function 3 Lifting Operator 4
  integrandLiftxTrue[3][3] = ( 0 );   // Basis function 4 Lifting Operator 4
  integrandLiftyTrue[0][3] = ( 0 );   // Basis function 1 Lifting Operator 4
  integrandLiftyTrue[1][3] = ( 0 );   // Basis function 2 Lifting Operator 4
  integrandLiftyTrue[2][3] = ( -3 );   // Basis function 3 Lifting Operator 4
  integrandLiftyTrue[3][3] = ( 0 );   // Basis function 4 Lifting Operator 4
  integrandLiftzTrue[0][3] = ( 0 );   // Basis function 1 Lifting Operator 4
  integrandLiftzTrue[1][3] = ( 0 );   // Basis function 2 Lifting Operator 4
  integrandLiftzTrue[2][3] = ( -4 );   // Basis function 3 Lifting Operator 4
  integrandLiftzTrue[3][3] = ( 0 );   // Basis function 4 Lifting Operator 4

  for (int face = 0; face < nFace; face++)
  {
      for (int k = 0; k < nDOF; k++)
      {                                                          // [DOF][trace][d]
        SANS_CHECK_CLOSE( integrandLiftxTrue[k][face], integrandLO[k][face][0], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandLiftyTrue[k][face], integrandLO[k][face][1], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandLiftzTrue[k][face], integrandLO[k][face][2], small_tol, close_tol );
      }
  }

  // Test at {0, 0, 1}
  sRef = {0, 0, 1};
  fcnPDE( sRef, integrandPDE, 4 );
  fcnLO( sRef, integrandLO, 4 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 8 ) + ( -6063/250. ) + ( 0 ); // Basis function 1
  integrandPDETrue[1] = ( -22/5. ) + ( 9291/1000. ) + ( 0 ); // Basis function 2
  integrandPDETrue[2] = ( -4/5. ) + ( 1809/250. ) + ( 0 ); // Basis function 3
  integrandPDETrue[3] = ( -14/5. ) + ( 309/40. ) + ( -273/10. ); // Basis function 4

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDE[3], small_tol, close_tol );

  //Lifting Operator residual integrands:
  integrandLiftxTrue[0][0] = ( 0 );   // Basis function 1 Lifting Operator 1
  integrandLiftxTrue[1][0] = ( 0 );   // Basis function 2 Lifting Operator 1
  integrandLiftxTrue[2][0] = ( 0 );   // Basis function 3 Lifting Operator 1
  integrandLiftxTrue[3][0] = ( -1 );   // Basis function 4 Lifting Operator 1
  integrandLiftyTrue[0][0] = ( 0 );   // Basis function 1 Lifting Operator 1
  integrandLiftyTrue[1][0] = ( 0 );   // Basis function 2 Lifting Operator 1
  integrandLiftyTrue[2][0] = ( 0 );   // Basis function 3 Lifting Operator 1
  integrandLiftyTrue[3][0] = ( 7 );   // Basis function 4 Lifting Operator 1
  integrandLiftzTrue[0][0] = ( 0 );   // Basis function 1 Lifting Operator 1
  integrandLiftzTrue[1][0] = ( 0 );   // Basis function 2 Lifting Operator 1
  integrandLiftzTrue[2][0] = ( 0 );   // Basis function 3 Lifting Operator 1
  integrandLiftzTrue[3][0] = ( 1 );   // Basis function 4 Lifting Operator 1

  integrandLiftxTrue[0][1] = ( 0 );   // Basis function 1 Lifting Operator 2
  integrandLiftxTrue[1][1] = ( 0 );   // Basis function 2 Lifting Operator 2
  integrandLiftxTrue[2][1] = ( 0 );   // Basis function 3 Lifting Operator 2
  integrandLiftxTrue[3][1] = ( 2 );   // Basis function 4 Lifting Operator 2
  integrandLiftyTrue[0][1] = ( 0 );   // Basis function 1 Lifting Operator 2
  integrandLiftyTrue[1][1] = ( 0 );   // Basis function 2 Lifting Operator 2
  integrandLiftyTrue[2][1] = ( 0 );   // Basis function 3 Lifting Operator 2
  integrandLiftyTrue[3][1] = ( 3 );   // Basis function 4 Lifting Operator 2
  integrandLiftzTrue[0][1] = ( 0 );   // Basis function 1 Lifting Operator 2
  integrandLiftzTrue[1][1] = ( 0 );   // Basis function 2 Lifting Operator 2
  integrandLiftzTrue[2][1] = ( 0 );   // Basis function 3 Lifting Operator 2
  integrandLiftzTrue[3][1] = ( 4 );   // Basis function 4 Lifting Operator 2

  integrandLiftxTrue[0][2] = ( 0 );   // Basis function 1 Lifting Operator 3
  integrandLiftxTrue[1][2] = ( 0 );   // Basis function 2 Lifting Operator 3
  integrandLiftxTrue[2][2] = ( 0 );   // Basis function 3 Lifting Operator 3
  integrandLiftxTrue[3][2] = ( -9 );   // Basis function 4 Lifting Operator 3
  integrandLiftyTrue[0][2] = ( 0 );   // Basis function 1 Lifting Operator 3
  integrandLiftyTrue[1][2] = ( 0 );   // Basis function 2 Lifting Operator 3
  integrandLiftyTrue[2][2] = ( 0 );   // Basis function 3 Lifting Operator 3
  integrandLiftyTrue[3][2] = ( -5 );   // Basis function 4 Lifting Operator 3
  integrandLiftzTrue[0][2] = ( 0 );   // Basis function 1 Lifting Operator 3
  integrandLiftzTrue[1][2] = ( 0 );   // Basis function 2 Lifting Operator 3
  integrandLiftzTrue[2][2] = ( 0 );   // Basis function 3 Lifting Operator 3
  integrandLiftzTrue[3][2] = ( -8 );   // Basis function 4 Lifting Operator 3

  integrandLiftxTrue[0][3] = ( 0 );   // Basis function 1 Lifting Operator 4
  integrandLiftxTrue[1][3] = ( 0 );   // Basis function 2 Lifting Operator 4
  integrandLiftxTrue[2][3] = ( 0 );   // Basis function 3 Lifting Operator 4
  integrandLiftxTrue[3][3] = ( -4 );   // Basis function 4 Lifting Operator 4
  integrandLiftyTrue[0][3] = ( 0 );   // Basis function 1 Lifting Operator 4
  integrandLiftyTrue[1][3] = ( 0 );   // Basis function 2 Lifting Operator 4
  integrandLiftyTrue[2][3] = ( 0 );   // Basis function 3 Lifting Operator 4
  integrandLiftyTrue[3][3] = ( 5 );   // Basis function 4 Lifting Operator 4
  integrandLiftzTrue[0][3] = ( 0 );   // Basis function 1 Lifting Operator 4
  integrandLiftzTrue[1][3] = ( 0 );   // Basis function 2 Lifting Operator 4
  integrandLiftzTrue[2][3] = ( 0 );   // Basis function 3 Lifting Operator 4
  integrandLiftzTrue[3][3] = ( 2 );   // Basis function 4 Lifting Operator 4

  for (int face = 0; face < nFace; face++)
  {
      for (int k = 0; k < nDOF; k++)
      {                                                          // [DOF][trace][d]
        SANS_CHECK_CLOSE( integrandLiftxTrue[k][face], integrandLO[k][face][0], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandLiftyTrue[k][face], integrandLO[k][face][1], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandLiftzTrue[k][face], integrandLO[k][face][2], small_tol, close_tol );
      }
  }

  // Test at {1/4, 1/4, 1/4}
  sRef = {1./4., 1./4., 1./4.};
  fcnPDE( sRef, integrandPDE, 4 );
  fcnLO( sRef, integrandLO, 4 );

  //PDE residual integrands: (advective) + (viscous) + (source)
  integrandPDETrue[0] = ( 7 ) + ( -6063/250. ) + ( -79/32. ); // Basis function 1
  integrandPDETrue[1] = ( -77/20. ) + ( 9291/1000. ) + ( -79/32. ); // Basis function 2
  integrandPDETrue[2] = ( -7/10. ) + ( 1809/250. ) + ( -79/32. ); // Basis function 3
  integrandPDETrue[3] = ( -49/20. ) + ( 309/40. ) + ( -79/32. ); // Basis function 4

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDE[3], small_tol, close_tol );

  //Lifting Operator residual integrands:
  integrandLiftxTrue[0][0] = ( 3./4. );   // Basis function 1 Lifting Operator 1
  integrandLiftxTrue[1][0] = ( 3./4. );   // Basis function 2 Lifting Operator 1
  integrandLiftxTrue[2][0] = ( 3./4. );   // Basis function 3 Lifting Operator 1
  integrandLiftxTrue[3][0] = ( 3./4. );   // Basis function 4 Lifting Operator 1
  integrandLiftyTrue[0][0] = ( 13./16. );   // Basis function 1 Lifting Operator 1
  integrandLiftyTrue[1][0] = ( 13./16. );   // Basis function 2 Lifting Operator 1
  integrandLiftyTrue[2][0] = ( 13./16. );   // Basis function 3 Lifting Operator 1
  integrandLiftyTrue[3][0] = ( 13./16. );   // Basis function 4 Lifting Operator 1
  integrandLiftzTrue[0][0] = ( 1./16. );   // Basis function 1 Lifting Operator 1
  integrandLiftzTrue[1][0] = ( 1./16. );   // Basis function 2 Lifting Operator 1
  integrandLiftzTrue[2][0] = ( 1./16. );   // Basis function 3 Lifting Operator 1
  integrandLiftzTrue[3][0] = ( 1./16. );   // Basis function 4 Lifting Operator 1

  integrandLiftxTrue[0][1] = ( 13./16. );   // Basis function 1 Lifting Operator 2
  integrandLiftxTrue[1][1] = ( 13./16. );   // Basis function 2 Lifting Operator 2
  integrandLiftxTrue[2][1] = ( 13./16. );   // Basis function 3 Lifting Operator 2
  integrandLiftxTrue[3][1] = ( 13./16. );   // Basis function 4 Lifting Operator 2
  integrandLiftyTrue[0][1] = ( 11./16. );   // Basis function 1 Lifting Operator 2
  integrandLiftyTrue[1][1] = ( 11./16. );   // Basis function 2 Lifting Operator 2
  integrandLiftyTrue[2][1] = ( 11./16. );   // Basis function 3 Lifting Operator 2
  integrandLiftyTrue[3][1] = ( 11./16. );   // Basis function 4 Lifting Operator 2
  integrandLiftzTrue[0][1] = ( 3./4. );   // Basis function 1 Lifting Operator 2
  integrandLiftzTrue[1][1] = ( 3./4. );   // Basis function 2 Lifting Operator 2
  integrandLiftzTrue[2][1] = ( 3./4. );   // Basis function 3 Lifting Operator 2
  integrandLiftzTrue[3][1] = ( 3./4. );   // Basis function 4 Lifting Operator 2

  integrandLiftxTrue[0][2] = ( -1./2. );   // Basis function 1 Lifting Operator 3
  integrandLiftxTrue[1][2] = ( -1./2. );   // Basis function 2 Lifting Operator 3
  integrandLiftxTrue[2][2] = ( -1./2. );   // Basis function 3 Lifting Operator 3
  integrandLiftxTrue[3][2] = ( -1./2. );   // Basis function 4 Lifting Operator 3
  integrandLiftyTrue[0][2] = ( -3./8. );   // Basis function 1 Lifting Operator 3
  integrandLiftyTrue[1][2] = ( -3./8. );   // Basis function 2 Lifting Operator 3
  integrandLiftyTrue[2][2] = ( -3./8. );   // Basis function 3 Lifting Operator 3
  integrandLiftyTrue[3][2] = ( -3./8. );   // Basis function 4 Lifting Operator 3
  integrandLiftzTrue[0][2] = ( -5./16. );   // Basis function 1 Lifting Operator 3
  integrandLiftzTrue[1][2] = ( -5./16. );   // Basis function 2 Lifting Operator 3
  integrandLiftzTrue[2][2] = ( -5./16. );   // Basis function 3 Lifting Operator 3
  integrandLiftzTrue[3][2] = ( -5./16. );   // Basis function 4 Lifting Operator 3

  integrandLiftxTrue[0][3] = ( 1./8. );   // Basis function 1 Lifting Operator 4
  integrandLiftxTrue[1][3] = ( 1./8. );   // Basis function 2 Lifting Operator 4
  integrandLiftxTrue[2][3] = ( 1./8. );   // Basis function 3 Lifting Operator 4
  integrandLiftxTrue[3][3] = ( 1./8. );   // Basis function 4 Lifting Operator 4
  integrandLiftyTrue[0][3] = ( 3./16. );   // Basis function 1 Lifting Operator 4
  integrandLiftyTrue[1][3] = ( 3./16. );   // Basis function 2 Lifting Operator 4
  integrandLiftyTrue[2][3] = ( 3./16. );   // Basis function 3 Lifting Operator 4
  integrandLiftyTrue[3][3] = ( 3./16. );   // Basis function 4 Lifting Operator 4
  integrandLiftzTrue[0][3] = ( 3./16. );   // Basis function 1 Lifting Operator 4
  integrandLiftzTrue[1][3] = ( 3./16. );   // Basis function 2 Lifting Operator 4
  integrandLiftzTrue[2][3] = ( 3./16. );   // Basis function 3 Lifting Operator 4
  integrandLiftzTrue[3][3] = ( 3./16. );   // Basis function 4 Lifting Operator 4

  for (int face = 0; face < nFace; face++)
  {
      for (int k = 0; k < nDOF; k++)
      {                                                          // [DOF][trace][d]
        SANS_CHECK_CLOSE( integrandLiftxTrue[k][face], integrandLO[k][face][0], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandLiftyTrue[k][face], integrandLO[k][face][1], small_tol, close_tol );
        SANS_CHECK_CLOSE( integrandLiftzTrue[k][face], integrandLO[k][face][2], small_tol, close_tol );
      }
  }

  // test the element integral of the functor

  // quadrature rule (for linear solution: basis grad is const, flux is linear)
  int quadratureorder = 2; //(integral of phi*lifting operator requires a quadratic quadrature order)
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD3, Tet, ArrayQ> integralPDE(quadratureorder, nIntegrand);
  GalerkinWeightedIntegral<TopoD3, Tet, BasisWeightedLOClass::IntegrandType> integralLO(quadratureorder, nIntegrand);

  ArrayQ rsdElemPDE[nDOF];
  BasisWeightedLOClass::IntegrandType rsdElemLO[nDOF];

  // cell integration for canonical element
  integralPDE( fcnPDE, xfldElem, rsdElemPDE, nIntegrand );
  integralLO( fcnLO, xfldElem, rsdElemLO, nIntegrand );

  Real rsdPDETrue[nDOF],rsdLiftxTrue[nDOF][nFace], rsdLiftyTrue[nDOF][nFace], rsdLiftzTrue[nDOF][nFace];

  //PDE residual: (advection) + (diffusion) + (source)
  rsdPDETrue[0] = (7/6.) + (-2021/500.) + (-863/2400.); // Basis function 1
  rsdPDETrue[1] = (-77/120.) + (3097/2000.) + (-1003/2400.); // Basis function 2
  rsdPDETrue[2] = (-7/60.) + (603/500.) + (-187/600.); // Basis function 3
  rsdPDETrue[3] = (-49/120.) + (103/80.) + (-167/300.); // Basis function 4

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdElemPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdElemPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[2], rsdElemPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[3], rsdElemPDE[3], small_tol, close_tol );

  // Lifting Operator residual:
  rsdLiftxTrue[0][0] = (7./60.); // Basis function 1 Lifting Operator 1
  rsdLiftyTrue[0][0] = (1./12.); // Basis function 1 Lifting Operator 1
  rsdLiftzTrue[0][0] = (-1./20.); // Basis function 1 Lifting Operator 1
  rsdLiftxTrue[1][0] = (19./120.); // Basis function 2 Lifting Operator 1
  rsdLiftyTrue[1][0] = (7./40.); // Basis function 2 Lifting Operator 1
  rsdLiftzTrue[1][0] = (1./20.); // Basis function 2 Lifting Operator 1
  rsdLiftxTrue[2][0] = (2./15.); // Basis function 3 Lifting Operator 1
  rsdLiftyTrue[2][0] = (7./60.); // Basis function 3 Lifting Operator 1
  rsdLiftzTrue[2][0] = (1./40.); // Basis function 3 Lifting Operator 1
  rsdLiftxTrue[3][0] = (11./120.); // Basis function 4 Lifting Operator 1
  rsdLiftyTrue[3][0] = (1./6.); // Basis function 4 Lifting Operator 1
  rsdLiftzTrue[3][0] = (1./60.); // Basis function 4 Lifting Operator 1

  rsdLiftxTrue[0][1] = (11./60.); // Basis function 1 Lifting Operator 2
  rsdLiftyTrue[0][1] = (17./120.); // Basis function 1 Lifting Operator 2
  rsdLiftzTrue[0][1] = (1./8.); // Basis function 1 Lifting Operator 2
  rsdLiftxTrue[1][1] = (1./10.); // Basis function 2 Lifting Operator 2
  rsdLiftyTrue[1][1] = (7./60.); // Basis function 2 Lifting Operator 2
  rsdLiftzTrue[1][1] = (19./120.); // Basis function 2 Lifting Operator 2
  rsdLiftxTrue[2][1] = (2./15.); // Basis function 3 Lifting Operator 2
  rsdLiftyTrue[2][1] = (1./12.); // Basis function 3 Lifting Operator 2
  rsdLiftzTrue[2][1] = (1./12.); // Basis function 3 Lifting Operator 2
  rsdLiftxTrue[3][1] = (1./8.); // Basis function 4 Lifting Operator 2
  rsdLiftyTrue[3][1] = (7./60.); // Basis function 4 Lifting Operator 2
  rsdLiftzTrue[3][1] = (2./15.); // Basis function 4 Lifting Operator 2

  rsdLiftxTrue[0][2] = (-1./12.); // Basis function 1 Lifting Operator 3
  rsdLiftyTrue[0][2] = (-1./24.); // Basis function 1 Lifting Operator 3
  rsdLiftzTrue[0][2] = (0); // Basis function 1 Lifting Operator 3
  rsdLiftxTrue[1][2] = (-1./30.); // Basis function 2 Lifting Operator 3
  rsdLiftyTrue[1][2] = (-1./12.); // Basis function 2 Lifting Operator 3
  rsdLiftzTrue[1][2] = (-1./15.); // Basis function 2 Lifting Operator 3
  rsdLiftxTrue[2][2] = (-3./40.); // Basis function 3 Lifting Operator 3
  rsdLiftyTrue[2][2] = (-1./30.); // Basis function 3 Lifting Operator 3
  rsdLiftzTrue[2][2] = (-1./30.); // Basis function 3 Lifting Operator 3
  rsdLiftxTrue[3][2] = (-17./120.); // Basis function 4 Lifting Operator 3
  rsdLiftyTrue[3][2] = (-11./120.); // Basis function 4 Lifting Operator 3
  rsdLiftzTrue[3][2] = (-13./120.); // Basis function 4 Lifting Operator 3

  rsdLiftxTrue[0][3] = (1./30.); // Basis function 1 Lifting Operator 4
  rsdLiftyTrue[0][3] = (1./60.); // Basis function 1 Lifting Operator 4
  rsdLiftzTrue[0][3] = (1./60.); // Basis function 1 Lifting Operator 4
  rsdLiftxTrue[1][3] = (1./40.); // Basis function 2 Lifting Operator 4
  rsdLiftyTrue[1][3] = (1./24.); // Basis function 2 Lifting Operator 4
  rsdLiftzTrue[1][3] = (3./40.); // Basis function 2 Lifting Operator 4
  rsdLiftxTrue[2][3] = (1./24.); // Basis function 3 Lifting Operator 4
  rsdLiftyTrue[2][3] = (0); // Basis function 3 Lifting Operator 4
  rsdLiftzTrue[2][3] = (-1./120.); // Basis function 3 Lifting Operator 4
  rsdLiftxTrue[3][3] = (-1./60.); // Basis function 4 Lifting Operator 4
  rsdLiftyTrue[3][3] = (1./15.); // Basis function 4 Lifting Operator 4
  rsdLiftzTrue[3][3] = (1./24.); // Basis function 4 Lifting Operator 4

  for (int face = 0; face < nFace; face++)
  {
      for (int k = 0; k < nDOF; k++)
      {
        SANS_CHECK_CLOSE( rsdLiftxTrue[k][face], rsdElemLO[k][face][0], small_tol, close_tol );
        SANS_CHECK_CLOSE( rsdLiftyTrue[k][face], rsdElemLO[k][face][1], small_tol, close_tol );
        SANS_CHECK_CLOSE( rsdLiftzTrue[k][face], rsdElemLO[k][face][2], small_tol, close_tol );
      }
  }
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_3D_Tet_test )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_UniformGrad > PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD3,Tet> ElementQFieldClass;
  typedef ElementLift<VectorArrayQ,TopoD3,Tet> ElementrFieldClass;
  typedef Element<VectorArrayQ,TopoD3,Tet> ElementRFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,Real,TopoD3,Tet,ElementParam> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO<Real,TopoD3,Tet> BasisWeightedLOClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD3,Tet,ElementParam> FieldWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.7;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123; Real kxy = 0.553; Real kxz = 0.760;
  Real kyx = 0.553; Real kyy = 1.007; Real kyz = 0.365;
  Real kzx = 0.760; Real kzy = 0.365; Real kzz = 1.460;

  ViscousFlux3D_Uniform visc(kxx, kxy, kxz,
                                 kyx, kyy, kyz,
                                 kzx, kzy, kzz);

  Real s0 = 0.25, sx = 0.6, sy = -1.5, sz = 0.1;
  Source3D_UniformGrad source(s0, sx, sy, sz);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 4, xfldElem.nDOF() );

  // tet grid
  Real v1[3] = {0,0,0};
  Real v2[3] = {1,0,0};
  Real v3[3] = {0,1,0};
  Real v4[3] = {0,0,1};

  xfldElem.DOF(0) = {v1[0], v1[1], v1[2]};
  xfldElem.DOF(1) = {v2[0], v2[1], v2[2]};
  xfldElem.DOF(2) = {v3[0], v3[1], v3[2]};
  xfldElem.DOF(3) = {v4[0], v4[1], v4[2]};

  // solution
  for (int qorder = 1; qorder < 3; qorder++) // Need higher polynomial basis in 3D
  {
    ElementQFieldClass qfldElem( qorder, BasisFunctionCategory_Hierarchical);
    ElementrFieldClass rfldElems(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldClass wfldElem( qorder, BasisFunctionCategory_Hierarchical);
    ElementrFieldClass sfldElems(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder,   qfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)*(qorder+3)/6, qfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   wfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)*(qorder+3)/6, wfldElem.nDOF() );

    BOOST_REQUIRE_EQUAL(wfldElem.nDOF(), qfldElem.nDOF());
    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
    {
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);
      wfldElem.DOF(dof) = 0;
      for (int trace = 0; trace < Tet::NTrace; trace++ )
      {
        rfldElems[trace].DOF(dof) = (dof+1)*pow(-1,dof);
        sfldElems[trace].DOF(dof) = 0;
      }
    }

    // sum of lifting operators
    ElementRFieldClass RfldElem(qorder, BasisFunctionCategory_Hierarchical);

    RfldElem.vectorViewDOF() = rfldElems[0].vectorViewDOF() +
                               rfldElems[1].vectorViewDOF() +
                               rfldElems[2].vectorViewDOF() +
                               rfldElems[3].vectorViewDOF();

    // BR2 discretization (not used)
    Real viscousEtaParameter = 6;
    DiscretizationDGBR2 disc(0, viscousEtaParameter);

    // integrand
    IntegrandClass fcnint( pde, disc, {0} );

    // integrand functor
    BasisWeightedPDEClass fcnPDEB = fcnint.integrand_PDE( xfldElem, qfldElem, RfldElem );
    BasisWeightedLOClass fcnLOB = fcnint.integrand_LO( xfldElem, rfldElems );
    FieldWeightedClass fcnW = fcnint.integrand( xfldElem, qfldElem, rfldElems, wfldElem, sfldElems );

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;
    const int nIntegrand = qfldElem.nDOF();

    FieldWeightedClass::IntegrandType rsdElemW=0;
    std::vector<ArrayQ> rsdElemPDEB(nIntegrand,0); // trickery to get round the POD warning
    std::vector<BasisWeightedLOClass::IntegrandType> rsdElemLOB(nIntegrand,0); // trickery to get round the POD warning

    int quadratureorder = -1;
    GalerkinWeightedIntegral<TopoD3, Tet, ArrayQ> integralPDEB(quadratureorder, nIntegrand);
    GalerkinWeightedIntegral<TopoD3, Tet, BasisWeightedLOClass::IntegrandType> integralLOB(quadratureorder, nIntegrand);

    // cell integration for canonical element
    integralPDEB( fcnPDEB, xfldElem, rsdElemPDEB.data(), nIntegrand );
    integralLOB( fcnLOB, xfldElem, rsdElemLOB.data(), nIntegrand );

    ElementIntegral<TopoD3, Tet, FieldWeightedClass::IntegrandType> integralW(quadratureorder);

    for (int i = 0; i < wfldElem.nDOF(); i++) // testing the wfld
    {
      // set just one of the weights to one
      wfldElem.DOF(i) = 1;
      for (int trace = 0; trace < Triangle::NTrace; trace ++ ) // wasteful, but need to loop here because it's a dot product inside
      {
        sfldElems[trace].DOF(i) = 1;

        // cell integration for canonical element
        rsdElemW = 0;
        integralW( fcnW, xfldElem, rsdElemW );

        // test the the two integrands are identical
        SANS_CHECK_CLOSE( rsdElemW.PDE, rsdElemPDEB[i], small_tol, close_tol );

        Real tmp =0;
        for (int D = 0; D < PhysD3::D; D++ )
          tmp += rsdElemLOB[i][trace][D];

        SANS_CHECK_CLOSE( rsdElemW.Lift[trace], tmp, small_tol, close_tol )

        // reset to zero
        sfldElems[trace].DOF(i) = 0;
      }
      // reset to 0
      wfldElem.DOF(i) = 0;
    }
  }

}
#endif


#if 0 // This test looks super old fashioned, someone should fix it up
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_2D_Quad_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<2,ArrayQ> VectorArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Quad> CellXFieldClass;

  typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Quad> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD2,Quad> ElementRFieldCell;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,Real,TopoD2,Quad,ElementXFieldCell>BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO<Real,TopoD2,Quad>BasisWeightedLOClass;
  typedef ElementXFieldCell::RefCoordType RefCoordType;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  CellXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 4, xfldElem.nDOF() );

  // Quad grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 1;  y3 = 1;
  x4 = 0;  y4 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};
  xfldElem.DOF(3) = {x4, y4};

  // solution

  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 4, qfldElem.nDOF() );

  // Quad solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 6;
  qfldElem.DOF(3) = 4;

  // lifting operators

  ElementRFieldCell rfldElem1(order, BasisFunctionCategory_Hierarchical);
  ElementRFieldCell rfldElem2(order, BasisFunctionCategory_Hierarchical);
  ElementRFieldCell rfldElem3(order, BasisFunctionCategory_Hierarchical);
  ElementRFieldCell rfldElem4(order, BasisFunctionCategory_Hierarchical);

  rfldElem1.DOF(0) = { 2, -3};  rfldElem1.DOF(1) = { 7,  8};  rfldElem1.DOF(2) = { 4,  1};  rfldElem1.DOF(3) = {-1,  7};
  rfldElem2.DOF(0) = { 9,  6};  rfldElem2.DOF(1) = {-1,  3};  rfldElem1.DOF(2) = { 3, -1};  rfldElem2.DOF(3) = { 2,  3};
  rfldElem3.DOF(0) = {-2,  1};  rfldElem3.DOF(1) = { 4, -4};  rfldElem1.DOF(2) = {-1,  2};  rfldElem3.DOF(3) = {-9, -5};
  rfldElem4.DOF(0) = { 2, -1};  rfldElem3.DOF(1) = { 1,  2};  rfldElem1.DOF(2) = { 3, -3};  rfldElem3.DOF(3) = {-4,  5};

  ElementRFieldCell rfldElems[4];
  rfldElems[0] = rfldElem1;
  rfldElems[1] = rfldElem2;
  rfldElems[2] = rfldElem3;
  rfldElems[3] = rfldElem4;

  // BR2 discretization (not used)
  Real viscousEtaParameter = 8;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcnint( pde, disc, {0} );

  // integrand functor
  BasisWeightedPDEClass fcnPDE = fcnint.integrand_PDE( xfldElem, qfldElem, rfldElems );
  BasisWeightedLOClass fcnLO = fcnint.integrand_LO( xfldElem, rfldElems );

  BOOST_CHECK_EQUAL( 1, fcnPDE.nEqn() );
  BOOST_CHECK_EQUAL( 4, fcnPDE.nDOF() );
  BOOST_CHECK( fcnPDE.needsEvaluation() == true );
  BOOST_CHECK_EQUAL( 1, fcnLO.nEqn() );
  BOOST_CHECK_EQUAL( 4, fcnLO.nDOF() );
  BOOST_CHECK( fcnLO.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordType sRef;
  Real integrandPDETrue[4] = {0,0,0,0};
  ArrayQ integrandPDE[4] = {0,0,0,0};
  ArrayQ integrandLO[4] = {0,0,0,0};

  // Test at {0, 0}
  sRef = {0, 0};
  fcnPDE( sRef, integrandPDE, 4 );

  //PDE residual integrands: (advective) + (viscous) + (lifting)
  integrandPDETrue[0] = ( 13./10. ) + ( -1254./125. ) + ( -8529./250. );   // Basis function 1
  integrandPDETrue[1] = ( -11./10. ) + ( 1181./200. ) + ( 6253./250. );   // Basis function 2
  integrandPDETrue[2] = ( 0 ) + ( 0 ) + ( 0 );   // Basis function 3
  integrandPDETrue[3] = ( -1./5. ) + ( 4127./1000. ) + ( 1138./125. );   // Basis function 4

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDE[3], small_tol, close_tol );

  // Test at {1, 0}
  sRef = {1, 0};
  fcnPDE( sRef, integrandPDE, 4 );

  //PDE residual integrands: (advective) + (viscous) + (lifting)
  integrandPDETrue[0] = ( 33./10. ) + ( -1181./200. ) + ( -2833./100. );   // Basis function 1
  integrandPDETrue[1] = ( -27./10. ) + ( 889./500. ) + ( 1648./125. );   // Basis function 2
  integrandPDETrue[2] = ( -3./5. ) + ( 4127./1000. ) + ( 7573./500. );   // Basis function 3
  integrandPDETrue[3] = ( 0 ) + ( 0 ) + ( 0 );   // Basis function 4

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDE[3], small_tol, close_tol );

  // Test at {1, 1}
  sRef = {1, 1};
  fcnPDE( sRef, integrandPDE, 4 );

  //PDE residual integrands: (advective) + (viscous) + (lifting)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + ( 0 );   // Basis function 1
  integrandPDETrue[1] = ( 6./5. ) + ( -4127./1000. ) + ( -397./100. );   // Basis function 2
  integrandPDETrue[2] = ( -39./5. ) + ( 1254./125. ) + ( 5631./250. );   // Basis function 3
  integrandPDETrue[3] = ( 33./5. ) + ( -1181./200. ) + ( -9277./500. );   // Basis function 4

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDE[3], small_tol, close_tol );

  // Test at {0, 1}
  sRef = {0, 1};
  fcnPDE( sRef, integrandPDE, 4 );

  //PDE residual integrands: (advective) + (viscous) + (lifting)
  integrandPDETrue[0] = ( 4./5. ) + ( -4127./1000. ) + ( -1717./500. );   // Basis function 1
  integrandPDETrue[1] = ( 0 ) + ( 0 ) + ( 0 );   // Basis function 2
  integrandPDETrue[2] = ( -22./5. ) + ( 1181./200. ) + ( -9973./500. );   // Basis function 3
  integrandPDETrue[3] = ( 18./5. ) + ( -889./500. ) + ( 1169./50. );   // Basis function 4

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDE[3], small_tol, close_tol );

  // Test at {1/2, 1/2}
  sRef = {1./2., 1./2.};
  fcnPDE( sRef, integrandPDE, 4 );

  //PDE residual integrands: (advective) + (viscous) + (lifting)
  integrandPDETrue[0] = ( 91./40. ) + ( -627./125. ) + ( -20901./2000. );   // Basis function 1
  integrandPDETrue[1] = ( -63./40. ) + ( 889./1000. ) + ( 2537./1000. );   // Basis function 2
  integrandPDETrue[2] = ( -91./40. ) + ( 627./125. ) + ( 20901./2000. );   // Basis function 3
  integrandPDETrue[3] = ( 63./40. ) + ( -889./1000. ) + ( -2537./1000. );   // Basis function 4

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDE[3], small_tol, close_tol );


  // test the element integral of the functor

  int quadratureorder = 2;
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Quad, ArrayQ> integralPDE(quadratureorder, nIntegrand);
  GalerkinWeightedIntegral<TopoD2, Quad, BasisWeightedLOClass::IntegrandType> integralLO(quadratureorder, nIntegrand);

  ArrayQ rsdElemPDE[4] = {0,0,0,0};
  BasisWeightedLOClass::IntegrandType rsdElemLO[4] = {0,0,0,0};

  // cell integration for canonical element
  integralPDE( fcnPDE, xfldElem, rsdElemPDE, nIntegrand );
  integralLO( fcnLO, xfldElem, rsdElemLO, nIntegrand );

  Real rsd[4];

  //PDE residual: (advection) + (diffusion) + (lifting)
  rsd[0] = (59./30.) + (-627./125.) + (-12457./1000.);   // Basis function 1
  rsd[1] = (-19./15.) + (889./1000.) + (9087./2000.);   // Basis function 2
  rsd[2] = (-31./12.) + (627./125.) + (2111./250.);   // Basis function 3
  rsd[3] = (113./60.) + (-889./1000.) + (-1061./2000.);   // Basis function 4

  SANS_CHECK_CLOSE( rsd[0], rsdElemPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[1], rsdElemPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[2], rsdElemPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[3], rsdElemPDE[3], small_tol, close_tol );


}
#endif
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
