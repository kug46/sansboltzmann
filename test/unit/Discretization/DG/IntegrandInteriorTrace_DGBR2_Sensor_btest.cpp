// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandInteriorTrace_DGBR2_Sensor_btest
// testing of 2-D element residual integrands for DG BR2: sensor PDE on Triangles

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion_ArtificialViscosity2D.h"
#include "pde/AdvectionDiffusion/AdvectionDiffusion_Sensor.h"
#include "pde/Sensor/Source2D_JumpSensor.h"
#include "pde/Sensor/PDESensorParameter2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Tuple/ElementTuple.h"

#include "Discretization/DG/IntegrandInteriorTrace_DGBR2.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{

typedef AdvectiveFlux2D_Uniform AdvectiveFluxType;
typedef ViscousFlux2D_Uniform ViscousFluxType;
typedef Source2D_UniformGrad SourceType;

typedef PDEAdvectionDiffusion_ArtificialViscosity<PhysD2, AdvectiveFluxType, ViscousFluxType, SourceType> PDEClass_Primal;
typedef PDENDConvertSpace<PhysD2, PDEClass_Primal> NDPDEClass_Primal;

typedef AdvectionDiffusion_Sensor Sensor;
typedef Source2D_JumpSensor<Sensor> Source_JumpSensor;

typedef Sensor_AdvectiveFlux2D_Uniform Sensor_Advection;
typedef Sensor_ViscousFlux2D_GenHScale Sensor_Diffusion;

typedef PDESensorParameter<PhysD2,
                           SensorParameterTraits<PhysD2>,
                           Sensor_Advection,
                           Sensor_Diffusion,
                           Source_JumpSensor > PDEClass_Sensor;
typedef PDENDConvertSpace<PhysD2, PDEClass_Sensor> NDPDEClass_Sensor;

typedef typename DLA::MatrixSymS<PhysD2::D,Real> HType;
typedef Element<HType,TopoD2,Triangle> ElementHFieldTri;
typedef Element<Real,TopoD2,Triangle> ElementQFieldTri;
typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldTri;
typedef MakeTuple<ElementTuple, ElementHFieldTri, ElementQFieldTri, ElementXFieldTri>::type ElementParamTri;

template class IntegrandInteriorTrace_DGBR2<NDPDEClass_Sensor>::
              BasisWeighted_PDE<Real,Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParamTri,ElementParamTri>;
template class IntegrandInteriorTrace_DGBR2<NDPDEClass_Sensor>::
              BasisWeighted_LO<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParamTri,ElementParamTri>;
template class IntegrandInteriorTrace_DGBR2<NDPDEClass_Sensor>::
              FieldWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParamTri,ElementParamTri>;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandInteriorTrace_DGBR2_Sensor_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_2D_Triangle_Triangle_test )
{
  typedef AdvectiveFlux2D_Uniform AdvectiveFluxType;
  typedef ViscousFlux2D_Uniform ViscousFluxType;
  typedef Source2D_UniformGrad SourceType;

  typedef PDEAdvectionDiffusion_ArtificialViscosity<PhysD2, AdvectiveFluxType, ViscousFluxType, SourceType> PDEClass_Primal;
  typedef PDENDConvertSpace<PhysD2, PDEClass_Primal> NDPDEClass_Primal;

  typedef AdvectionDiffusion_Sensor Sensor;
  typedef Source2D_JumpSensor<Sensor> Source_JumpSensor;

  typedef Sensor_AdvectiveFlux2D_Uniform Sensor_Advection;
  typedef Sensor_ViscousFlux2D_GenHScale Sensor_Diffusion;

  typedef PDESensorParameter<PhysD2,
                             SensorParameterTraits<PhysD2>,
                             Sensor_Advection,
                             Sensor_Diffusion,
                             Source_JumpSensor > PDEClass_Sensor;
  typedef PDENDConvertSpace<PhysD2, PDEClass_Sensor> NDPDEClass_Sensor;

  typedef NDPDEClass_Sensor::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass_Sensor::template VectorArrayQ<Real> VectorArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementRFieldCell;

  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;

  typedef typename DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef Element<HType,TopoD2,Triangle> ElementHFieldCell;

  typedef MakeTuple<ElementTuple, ElementHFieldCell, ElementQFieldCell, ElementXFieldCell>::type ElementParam;

  typedef IntegrandInteriorTrace_DGBR2<NDPDEClass_Sensor> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real, Real, TopoD1, Line, TopoD2, Triangle, Triangle,ElementParam,ElementParam> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO<Real, TopoD1, Line, TopoD2, Triangle, Triangle,ElementParam,ElementParam> BasisWeightedLOClass;

  typedef IntegrandClass::FieldWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParam,ElementParam> FieldWeightedClass;

  int order_primal = 1;

  // Primal PDE
  Real a = 0.75;
  Real b = 1.0;
  Real nu = 0.0;
  AdvectiveFluxType adv( a, b );
//  AdvectiveFluxType adv( 0, a, 0, a,
//                         b, b, b, 0, 0.0, 0.0);
  ViscousFluxType visc( nu, 0, 0, nu );
  SourceType source(0.0, 0.0, 0.0);

  //Create artificial viscosity PDE
  NDPDEClass_Primal pde_AD_AV(adv, visc, source, order_primal);


  //Sensor PDE
  Sensor sensor(pde_AD_AV);
  Sensor_Advection sensor_adv(0.0, 0.0);
  Sensor_Diffusion sensor_visc(0.0);

  Source_JumpSensor sensor_source(order_primal, sensor, 0.1);

  NDPDEClass_Sensor pde(sensor_adv, sensor_visc, sensor_source);


  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasSourceTrace() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemR.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;
  x4 = 1;  y4 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xfldElemR.DOF(0) = {x4, y4};
  xfldElemR.DOF(1) = {x3, y3};
  xfldElemR.DOF(2) = {x2, y2};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  ElementHFieldCell hfldElemL(1, BasisFunctionCategory_Hierarchical);
  ElementHFieldCell hfldElemR(1, BasisFunctionCategory_Hierarchical);

  hfldElemL.DOF(0) = {{1.0}, {0.2, 2.0}};
  hfldElemL.DOF(1) = {{1.3}, {0.0, 1.0}};
  hfldElemL.DOF(2) = {{0.5}, {0.6, 3.0}};

  hfldElemR.DOF(0) = {{1.0}, {0.2, 2.0}};
  hfldElemR.DOF(1) = {{1.3}, {0.0, 1.0}};
  hfldElemR.DOF(2) = {{0.5}, {0.6, 3.0}};

  for (int qorder = 2; qorder< 4; qorder++)
  {
    // solution
    ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell qfldElemR(qorder, BasisFunctionCategory_Hierarchical);
    ElementRFieldCell rfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementRFieldCell rfldElemR(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElemR(qorder, BasisFunctionCategory_Hierarchical);
    ElementRFieldCell sfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementRFieldCell sfldElemR(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder,   qfldElemL.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   qfldElemR.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElemR.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   rfldElemL.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, rfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   rfldElemR.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, rfldElemR.nDOF() );

    BOOST_CHECK_EQUAL( rfldElemL.nDOF(), rfldElemR.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), qfldElemR.nDOF() );
    BOOST_CHECK_EQUAL( wfldElemL.nDOF(), wfldElemR.nDOF() );
    BOOST_CHECK_EQUAL( sfldElemL.nDOF(), sfldElemR.nDOF() );

    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), rfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), wfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qfldElemL.nDOF(), sfldElemL.nDOF() );

    // line solution
    for ( int dof = 0; dof < qfldElemL.nDOF(); dof ++ )
    {
      qfldElemL.DOF(dof) = (dof+1)*pow(-1,dof);
      qfldElemR.DOF(dof) = (dof+2)*pow(-1,dof+1);
      rfldElemL.DOF(dof) = (dof+1)*pow(-1,dof);
      rfldElemR.DOF(dof) = (dof+2)*pow(-1,dof+1);
      wfldElemL.DOF(dof) = 0;
      wfldElemR.DOF(dof) = 0;
      sfldElemL.DOF(dof) = 0;
      sfldElemR.DOF(dof) = 0;
    }

    // parameter solution
    ElementQFieldCell qparamfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell qparamfldElemR(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qparamfldElemL.nDOF(), qparamfldElemR.nDOF() );

    for ( int dof = 0; dof < qparamfldElemL.nDOF(); dof ++ )
    {
      qparamfldElemL.DOF(dof) = 1.25;
      qparamfldElemR.DOF(dof) = 0.75;
    }

    // create tuple elements
    ElementParam paramfldElemL = (hfldElemL, qparamfldElemL, xfldElemL);
    ElementParam paramfldElemR = (hfldElemR, qparamfldElemR, xfldElemR);

    // BR2 discretization
    Real viscousEtaParameter = 6;
    DiscretizationDGBR2 disc(0, viscousEtaParameter);

    // integrand
    IntegrandClass fcnint( pde, disc, {0} );

    // integrand functor

    BasisWeightedPDEClass fcnPDEB = fcnint.integrand_PDE( xedge, CanonicalTraceToCell(0, 1), CanonicalTraceToCell(0, -1),
                                                          paramfldElemL, qfldElemL, rfldElemL,
                                                          paramfldElemR, qfldElemR, rfldElemR );
    BasisWeightedLOClass fcnLOB = fcnint.integrand_LO( xedge, CanonicalTraceToCell(0, 1), CanonicalTraceToCell(0, -1),
                                                       paramfldElemL, qfldElemL,
                                                       paramfldElemR, qfldElemR );



    // integrand functor
    FieldWeightedClass fcnW = fcnint.integrand( xedge, CanonicalTraceToCell(0,1), CanonicalTraceToCell(0,-1),
                                                paramfldElemL,
                                                qfldElemL, rfldElemL,
                                                wfldElemL, sfldElemL,
                                                paramfldElemR,
                                                qfldElemR, rfldElemR,
                                                wfldElemR, sfldElemR );

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;
    const int nIntegrandL = qfldElemL.nDOF();
    const int nIntegrandR = qfldElemR.nDOF();

    int quadratureorder = -1;
    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ,
                                           ArrayQ> integralPDEB(quadratureorder, nIntegrandL, nIntegrandR);
    GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedLOClass::IntegrandType,
                                           BasisWeightedLOClass::IntegrandType> integralLOB(quadratureorder, nIntegrandL, nIntegrandR);

    ElementIntegral<TopoD1, Line, FieldWeightedClass::IntegrandType,
                                  FieldWeightedClass::IntegrandType> integralW(quadratureorder);
    std::vector<ArrayQ> rsdPDEElemBL(nIntegrandL, 0);
    std::vector<ArrayQ> rsdPDEElemBR(nIntegrandR, 0);
    std::vector<BasisWeightedLOClass::IntegrandType> rsdLOElemBL(nIntegrandL, 0);
    std::vector<BasisWeightedLOClass::IntegrandType> rsdLOElemBR(nIntegrandR, 0);
    FieldWeightedClass::IntegrandType rsdElemWL=0;
    FieldWeightedClass::IntegrandType rsdElemWR=0;

    // cell integration for canonical element
    integralPDEB( fcnPDEB, xedge, rsdPDEElemBL.data(), nIntegrandL, rsdPDEElemBR.data(), nIntegrandR );
    integralLOB ( fcnLOB,  xedge, rsdLOElemBL.data(),  nIntegrandL, rsdLOElemBR.data(),  nIntegrandR );

    for (int i = 0; i < wfldElemL.nDOF(); i++ )
    {
      // set just one of the weights to one
      wfldElemL.DOF(i) = 1; sfldElemL.DOF(i) = 1;
      wfldElemR.DOF(i) = 1; sfldElemR.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemWL = 0; rsdElemWR = 0;
      integralW(fcnW, xedge, rsdElemWL, rsdElemWR );

      // test the two integrands are the same
      SANS_CHECK_CLOSE ( rsdElemWL.PDE, rsdPDEElemBL[i], small_tol, close_tol );
      SANS_CHECK_CLOSE ( rsdElemWR.PDE, rsdPDEElemBR[i], small_tol, close_tol );

      Real tmpL = 0, tmpR = 0;
      for (int D = 0; D < PhysD2::D; D++ )
      {
        tmpL += rsdLOElemBL[i][D];
        tmpR += rsdLOElemBR[i][D];
      }
      SANS_CHECK_CLOSE( rsdElemWL.Lift, tmpL, small_tol, close_tol );
      SANS_CHECK_CLOSE( rsdElemWR.Lift, tmpR, small_tol, close_tol );

      // reset to 0
      wfldElemL.DOF(i) = 0; sfldElemL.DOF(i) = 0;
      wfldElemR.DOF(i) = 0; sfldElemR.DOF(i) = 0;
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
