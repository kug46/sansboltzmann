// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ResidualBoundaryTrace_Dirichlet_mitState_AD_btest
// testing of boundary integral residual functions with Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldVolume_DG_Cell.h"

#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "Discretization/DG/ResidualBoundaryTrace_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Quad_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Hex_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ResidualBoundaryTrace_Dirichlet_mitState_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_1D_1Line_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCTypeDirichlet_mitState<AdvectiveFlux1D_Uniform,ViscousFlux1D_Uniform> BCTypeDirichlet_mitState1D;

  typedef BCAdvectionDiffusion<PhysD1,BCTypeDirichlet_mitState1D> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;

  // PDE

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // BC
  Real uB = 2.4;
  BCClass bc(pde, uB);

  // grid: P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // triangle solution data (left)
  qfld.DOF(0) = 2;
  qfld.DOF(1) = 3;

  // lifting-operator: P1
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( 4, rfld.nDOF() );

  // lifting operator data

  // BC group 1 is canonical trace 0, so only set first 2 DOF
  rfld.DOF(0) = -1; rfld.DOF(1) =  5;

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcn( pde, bc, {1}, disc );

  // quadrature rule
  int quadratureorder[2] = {0,0};

  // PDE global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(2);

  rsdPDEGlobal = 0;

  IntegrateBoundaryTraceGroups<TopoD1>::integrate( ResidualBoundaryTrace_DGBR2(fcn, rsdPDEGlobal),
                                                   xfld, (qfld, rfld), quadratureorder, 2 );

#if 0
  cout << "rsdPDEGlobal[0] = " << rsdPDEGlobal[0] << endl;
  cout << "rsdPDEGlobal[1] = " << rsdPDEGlobal[1] << endl;
  cout << "rsdPDEGlobal[2] = " << rsdPDEGlobal[2] << endl;
#endif

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  Real rsdPDETrue[2];

  //PDE residuals (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  rsdPDETrue[0] = ( 0 ) + ( 0 ) + ( 6369./5000. ) + ( 0 ); // Basis function 1
  rsdPDETrue[1] = ( 33./10. ) + ( -2123./1000. ) + ( -6369./5000. ) + ( -2123./100. ); // Basis function 2

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDEGlobal[1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_2D_1Triangle_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCTypeDirichlet_mitState<AdvectiveFlux2D_Uniform,ViscousFlux2D_Uniform> BCTypeDirichlet_mitState2D;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeDirichlet_mitState2D> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC
  Real uB = 2.4;
  BCClass bc(pde, uB);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // grid: P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL(3, qfld.nDOF());

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  // lifting operators
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL(9, rfld.nDOF());

  // BC group 1 is canonical trace 0, so only set first 3 DOF
  rfld.DOF(0) = { 2, -3};  rfld.DOF(1) = { 7,  8};  rfld.DOF(2) = {-1,  7};

  // integrand
  IntegrandClass fcn( pde, bc, {1}, disc );

  // quadrature rule
  int quadratureorder[3] = {2,2,2};

  Real rsdPDETrue[3];

  //PDE residuals (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  rsdPDETrue[0] = ( 0 ) + ( 0 ) + ( 11649./2500. ) + ( 0 );   // Basis function 1
  rsdPDETrue[1] = ( 13./6. ) + ( -627./125. ) + ( -7359./2500. ) + ( -17667./250. );   // Basis function 2
  rsdPDETrue[2] = ( 143./60. ) + ( -627./125. ) + ( -429./250. ) + ( -477./10. );   // Basis function 3

  // PDE/BC global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(3);

  // topology-specific single group interface

  rsdPDEGlobal = 0;

  IntegrateBoundaryTraceGroups<TopoD2>::integrate( ResidualBoundaryTrace_DGBR2(fcn, rsdPDEGlobal),
                                                      xfld, (qfld, rfld), quadratureorder, 3 );

#if 0
  cout << "rsdPDEGlobal[0] = " << rsdPDEGlobal[0] << endl;
  cout << "rsdPDEGlobal[1] = " << rsdPDEGlobal[1] << endl;
  cout << "rsdPDEGlobal[2] = " << rsdPDEGlobal[2] << endl;
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDEGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[2], rsdPDEGlobal[2], small_tol, close_tol );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
