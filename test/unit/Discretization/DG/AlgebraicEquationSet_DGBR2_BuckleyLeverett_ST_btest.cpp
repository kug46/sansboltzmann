// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AlgebraicEquationSet_DGBR2_BuckleyLeverett_ST_btest

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/iostreams/device/mapped_file.hpp>
using boost::iostreams::mapped_file_source;

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverett.h"
#include "pde/PorousMedia/PDEBuckleyLeverett1D.h"
#include "pde/PorousMedia/BCBuckleyLeverett1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#include "NonLinearSolver/NewtonSolver.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( AlgebraicEquationSet_DGBR2_BuckleyLeverett_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Linesearch_field_dump_test )
{
  typedef QTypePrimitive_Sw QType;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef TraitsModelBuckleyLeverett<QType, RelPermModel, CapillaryModel> TraitsModelClass;
  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;

  typedef PDENDConvertSpaceTime<PhysD1, PDEClass> NDPDEClass;

  typedef BCBuckleyLeverett1DVector<PDEClass> BCVector;
  typedef BCParameters<BCVector> BCParams;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpaceTime, BCVector,
                                           AlgEqSetTraits_Sparse, DGBR2, XField<PhysD2, TopoD2>> PrimalEquationSet;

  typedef PrimalEquationSet::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSet::SystemVector SystemVectorClass;

  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank() == 0);

  // only test with rank 0
  if (world.rank() > 0) return;

  // grid
  int ii = 5;
  int jj = 5;

  XField2D_Box_Triangle_Lagrange_X1 xfld( comm, ii, jj );
  int iB = xfld.iBottom;
  int iR = xfld.iRight;
  int iT = xfld.iTop;
  int iL = xfld.iLeft;

  // PDE
  RelPermModel kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  const Real pc_max = 5.0;
  CapillaryModel cap_model(pc_max);

  NDPDEClass pde(phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  const Real SwL = 0.9;
  const Real SwR = 0.1;

  // BCs

  // Create a BC dictionary
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict TimeIC;
  TimeIC[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC;
  TimeIC[BCBuckleyLeverett1DParams<BCTypeTimeIC>::params.qB] = SwR;

  PyDict BCDirichletL;
  BCDirichletL[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin;
  BCDirichletL[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.A] = 1;
  BCDirichletL[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.B] = 0;
  BCDirichletL[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.bcdata] = SwL;

  PyDict BCDirichletR;
  BCDirichletR[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin;
  BCDirichletR[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.A] = 1;
  BCDirichletR[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.B] = 0;
  BCDirichletR[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.bcdata] = SwR;

  PyDict PyBCList;
  PyBCList["DirichletR"] = BCDirichletR;
  PyBCList["DirichletL"] = BCDirichletL;
  PyBCList["None"] = BCNone;
  PyBCList["TimeIC"] = TimeIC;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["DirichletR"] = {iR};
  BCBoundaryGroups["DirichletL"] = {iL};
  BCBoundaryGroups["None"] = {iT};
  BCBoundaryGroups["TimeIC"] = {iB};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // solution
  int order = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0;

  // lifting operator
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Legendre);
  rfld = 0;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre, active_boundaries );
  lgfld = 0;

  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  QuadratureOrder quadratureOrder( xfld, -1 );
  std::vector<Real> tol = {1e-10, 1e-10};
  PrimalEquationSet PrimalEqSet(xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder, ResidualNorm_Default, tol,
                                {0}, {0}, PyBCList, BCBoundaryGroups);

  // Nonlinear solver dicts
  PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-13;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = true;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

  // Check inputs
  NewtonSolverParam::checkInputs(NewtonSolverDict);

  // Create the solver object
  NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

  // set initial condition from current solution in solution fields
  SystemVectorClass sln0(PrimalEqSet.vectorStateSize());
  PrimalEqSet.fillSystemVector(sln0);

  // nonlinear solve
  SystemVectorClass sln(PrimalEqSet.vectorStateSize());
  Solver.solve(sln0, sln);

  //Check contents of tecplot file
  std::string filename_current = "tmp/linesearchfld_iPDE_iter0.plt";
  std::string filename_stored = "IO/Discretization/AlgebraicEquationSet_DGBR2_BL_linesearchfld_iPDE_iter0.plt";

  mapped_file_source f1(filename_current);
  mapped_file_source f2(filename_stored);

  //Check if the contents of the new script and the example are equal
  BOOST_CHECK( f1.size() == f2.size() && std::equal(f1.data(), f1.data() + f1.size(), f2.data()) );

  //Delete the written files
  std::remove(filename_current.c_str());

  BOOST_CHECK_EQUAL( PrimalEqSet.nResidNorm(), 2 );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
