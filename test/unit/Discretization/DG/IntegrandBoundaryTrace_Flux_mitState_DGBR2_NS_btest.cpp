// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandBoundaryTrace_Flux_mitState_DGBR2_NS_btest
// testing of boundary integrands: Navier-Stokes

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{

}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandBoundaryTrace_Flux_mitState_DGBR2_NS_btest_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Triangle_X1Q1R1_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef NDPDEClass::VectorX VectorX;

  typedef BCNavierStokes2D< BCTypeWallNoSlipAdiabatic_mitState, PDEClass > BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementRFieldClass;
  typedef ElementXFieldCell::RefCoordType RefCoordCellType;

  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, DGBR2> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO<Real,Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> BasisWeightedLOClass;

  // PDE

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde( gas, visc, tcond, Euler_ResidInterp_Raw );

  Real rho, u, v, t;

  rho = 1.225;              // kg/m^3
  u = 69.784; v = -3.231;   // m/s
  t = 288.15;               // K

  // BC
  BCClass bc(pde);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 2 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // normal vector on the edge
  VectorX nL = {1./sqrt(2.), 1./sqrt(2.)};

  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution (left)
  qfldElem.DOF(0) = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rho*0.9, u*0.9, v*0.9, t*0.9) );
  qfldElem.DOF(1) = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rho*1.1, u*1.1, v*1.1, t*1.1) );
  qfldElem.DOF(2) = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rho*0.8, u*0.8, v*0.8, t*0.8) );

  // lifting operators
  ElementRFieldClass rfldElem(order, BasisFunctionCategory_Hierarchical);

  rfldElem.DOF(0) = { 2, -3};  rfldElem.DOF(1) = { 7,  8};  rfldElem.DOF(2) = {-1,  7};

  // BR2 discretization
  Real eta = 6;
  DiscretizationDGBR2 disc(0, eta);

  // integrand functor
  IntegrandClass fcnint( pde, bc, {0}, disc );

  CanonicalTraceToCell canonicalTrace(0, 1);

  BasisWeightedPDEClass fcnPDE = fcnint.integrand_PDE( xedge, canonicalTrace, xfldElem, qfldElem, rfldElem );
  BasisWeightedLOClass fcnLO = fcnint.integrand_LO( xedge, canonicalTrace, xfldElem, qfldElem, rfldElem );

  BOOST_CHECK_EQUAL( 3, fcnPDE.nDOF() );
  BOOST_CHECK( fcnPDE.needsEvaluation() == true );

  BOOST_CHECK_EQUAL( 3, fcnLO.nDOF() );
  BOOST_CHECK( fcnLO.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRefTrace;
  RefCoordCellType sRefCell;
  ArrayQ integrandPDETrue[3];
  BasisWeightedPDEClass::IntegrandType integrandPDE[3];
  BasisWeightedLOClass::IntegrandType integrandLO[3], integrandLOTrue[3];

  std::vector<Real> phi(qfldElem.nDOF());
  std::vector<VectorX> gradphi(qfldElem.nDOF());
  VectorX X = 0;
  ArrayQ qL, qB;
  ArrayQ Fn;
  VectorArrayQ gradqL, gradqliftedL, rlift, dqn, FB;

  //***********************//
  sRefTrace = 0;
  fcnPDE( sRefTrace, integrandPDE, 3 );
  fcnLO( sRefTrace, integrandLO, 3 );

  TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalTrace, sRefTrace, sRefCell );

  qfldElem.evalBasis(sRefCell, phi.data(), phi.size());
  xfldElem.evalBasisGradient( sRefCell, qfldElem, gradphi.data(), gradphi.size() );
  qfldElem.evalFromBasis( phi.data(), phi.size(), qL );
  qfldElem.evalFromBasis( gradphi.data(), gradphi.size(), gradqL );
  rfldElem.evalFromBasis( phi.data(), phi.size(), rlift );

  gradqliftedL = gradqL + eta*rlift;

  // BC state
  bc.state( X, nL, qL, qB );

  // normal flux
  Fn = 0;
  bc.fluxNormal( X, nL, qL, gradqliftedL, qB, Fn );

  // Jump in primitive variable [ nL*( qL - qR ) ]
  for (int d = 0; d < PhysD2::D; d++)
    dqn[d] = nL[d]*(qL - qB);

  FB = 0;
  pde.fluxViscous( X, qB, dqn, FB );

  //PDE residual integrands
  for (int k = 0; k < 3; k++)
  {
    //                    (bc flux)   (dual consistency)
    integrandPDETrue[k] = phi[k]*Fn + dot(gradphi[k],FB);

    // No energy flux
    BOOST_CHECK_EQUAL(0, integrandPDETrue[k][pde.iEngy]);
  }

  for (int n = 0; n < PDEClass::N; n++)
  {
    SANS_CHECK_CLOSE( integrandPDETrue[0][n], integrandPDE[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1][n], integrandPDE[1][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2][n], integrandPDE[2][n], small_tol, close_tol );
  }

  //LO residual integrands
  for (int k = 0; k < 3; k++)
    integrandLOTrue[k] = phi[k]*dqn;

  for (int k = 0; k < 3; k++)
    for (int n = 0; n < PDEClass::N; n++)
    {
      SANS_CHECK_CLOSE( integrandLOTrue[k][0][n], integrandLO[k][0][n], small_tol, close_tol );
      SANS_CHECK_CLOSE( integrandLOTrue[k][1][n], integrandLO[k][1][n], small_tol, close_tol );
    }

  //***********************//
  sRefTrace = 1;
  fcnPDE( sRefTrace, integrandPDE, 3 );
  fcnLO( sRefTrace, integrandLO, 3 );

  TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalTrace, sRefTrace, sRefCell );

  qfldElem.evalBasis(sRefCell, phi.data(), phi.size());
  xfldElem.evalBasisGradient( sRefCell, qfldElem, gradphi.data(), gradphi.size() );
  qfldElem.evalFromBasis( phi.data(), phi.size(), qL );
  qfldElem.evalFromBasis( gradphi.data(), gradphi.size(), gradqL );
  rfldElem.evalFromBasis( phi.data(), phi.size(), rlift );

  gradqliftedL = gradqL + eta*rlift;

  // BC state
  bc.state( X, nL, qL, qB );

  // normal flux
  Fn = 0;
  bc.fluxNormal( X, nL, qL, gradqliftedL, qB, Fn );

  // Jump in primitive variable [ nL*( qL - qR ) ]
  for (int d = 0; d < PhysD2::D; d++)
    dqn[d] = nL[d]*(qL - qB);

  FB = 0;
  pde.fluxViscous( X, qB, dqn, FB );

  //PDE residual integrands
  for (int k = 0; k < 3; k++)
  {
    //                    (bc flux)   (dual consistency)
    integrandPDETrue[k] = phi[k]*Fn + dot(gradphi[k],FB);

    // No energy flux
    BOOST_CHECK_SMALL( integrandPDETrue[k][pde.iEngy], small_tol );
  }

  for (int n = 0; n < PDEClass::N; n++)
  {
    SANS_CHECK_CLOSE( integrandPDETrue[0][n], integrandPDE[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1][n], integrandPDE[1][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2][n], integrandPDE[2][n], small_tol, close_tol );
  }

  //LO residual integrands
  for (int k = 0; k < 3; k++)
    integrandLOTrue[k] = phi[k]*dqn;

  for (int k = 0; k < 3; k++)
    for (int n = 0; n < PDEClass::N; n++)
    {
      SANS_CHECK_CLOSE( integrandLOTrue[k][0][n], integrandLO[k][0][n], small_tol, close_tol );
      SANS_CHECK_CLOSE( integrandLOTrue[k][1][n], integrandLO[k][1][n], small_tol, close_tol );
    }

  //***********************//
  sRefTrace = 0.5;
  fcnPDE( sRefTrace, integrandPDE, 3 );
  fcnLO( sRefTrace, integrandLO, 3 );

  TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalTrace, sRefTrace, sRefCell );

  qfldElem.evalBasis(sRefCell, phi.data(), phi.size());
  xfldElem.evalBasisGradient( sRefCell, qfldElem, gradphi.data(), gradphi.size() );
  qfldElem.evalFromBasis( phi.data(), phi.size(), qL );
  qfldElem.evalFromBasis( gradphi.data(), gradphi.size(), gradqL );
  rfldElem.evalFromBasis( phi.data(), phi.size(), rlift );

  gradqliftedL = gradqL + eta*rlift;

  // BC state
  bc.state( X, nL, qL, qB );

  // normal flux
  Fn = 0;
  bc.fluxNormal( X, nL, qL, gradqliftedL, qB, Fn );

  // Jump in primitive variable [ nL*( qL - qR ) ]
  for (int d = 0; d < PhysD2::D; d++)
    dqn[d] = nL[d]*(qL - qB);

  FB = 0;
  pde.fluxViscous( X, qB, dqn, FB );

  //PDE residual integrands
  for (int k = 0; k < 3; k++)
  {
    //                    (bc flux)   (dual consistency)
    integrandPDETrue[k] = phi[k]*Fn + dot(gradphi[k],FB);

    // No energy flux
    BOOST_CHECK_SMALL(integrandPDETrue[k][pde.iEngy], small_tol);
  }

  for (int n = 0; n < PDEClass::N; n++)
  {
    SANS_CHECK_CLOSE( integrandPDETrue[0][n], integrandPDE[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1][n], integrandPDE[1][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2][n], integrandPDE[2][n], small_tol, close_tol );
  }

  //LO residual integrands
  for (int k = 0; k < 3; k++)
    integrandLOTrue[k] = phi[k]*dqn;

  for (int k = 0; k < 3; k++)
    for (int n = 0; n < PDEClass::N; n++)
    {
      SANS_CHECK_CLOSE( integrandLOTrue[k][0][n], integrandLO[k][0][n], small_tol, close_tol );
      SANS_CHECK_CLOSE( integrandLOTrue[k][1][n], integrandLO[k][1][n], small_tol, close_tol );
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_Triangle_X1Q1R1W2S2_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef NDPDEClass::VectorX VectorX;

  typedef BCNavierStokes2D< BCTypeWallNoSlipAdiabatic_mitState, PDEClass > BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementRFieldClass;
  typedef ElementXFieldCell::RefCoordType RefCoordCellType;

  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, DGBR2> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,Real,TopoD1,Line,TopoD2,Triangle, ElementXFieldCell> FieldWeightedClass;

  // PDE

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde( gas, visc, tcond, Euler_ResidInterp_Raw );

  Real rho, u, v, t;

  rho = 1.225;              // kg/m^3
  u = 69.784; v = -3.231;   // m/s
  t = 288.15;               // K

  // BC
  BCClass bc(pde);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 2 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // normal vector on the edge
  VectorX nL = {1./sqrt(2.), 1./sqrt(2.)};

  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution (left)
  qfldElem.DOF(0) = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rho*0.9, u*0.9, v*0.9, t*0.9) );
  qfldElem.DOF(1) = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rho*1.1, u*1.1, v*1.1, t*1.1) );
  qfldElem.DOF(2) = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rho*0.8, u*0.8, v*0.8, t*0.8) );

  // lifting operators
  ElementRFieldClass rfldElem(order, BasisFunctionCategory_Hierarchical);

  rfldElem.DOF(0) = { 2, -3};  rfldElem.DOF(1) = { 7,  8};  rfldElem.DOF(2) = {-1,  7};

  // weight
  ElementQFieldCell wfldElem(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElem.order() );
  BOOST_CHECK_EQUAL( 6, wfldElem.nDOF() );

  // triangle solution (left)
  wfldElem.DOF(0) = {-2.0,  1.0,  3.2,  5.1};
  wfldElem.DOF(1) = { 4.0,  0.7,  2.1, -1.2};
  wfldElem.DOF(2) = { 1.0, -0.9,  1.5,  2.7};
  wfldElem.DOF(3) = {-1.0,  1.2, -4.1, -3.1};
  wfldElem.DOF(4) = { 2.5, -0.3,  3.6,  1.8};
  wfldElem.DOF(5) = { 5.0,  2.3,  1.1, -3.6};

  // lifting operators
  ElementRFieldClass sfldElem(order+1, BasisFunctionCategory_Hierarchical);

  sfldElem.DOF(0) = { 1,  5};  sfldElem.DOF(1) = { 3,  6};  sfldElem.DOF(2) = {-1,  7};
  sfldElem.DOF(3) = { 3,  1};  sfldElem.DOF(4) = { 2,  2};  sfldElem.DOF(5) = { 4,  3};

  // BR2 discretization
  Real eta = 6;
  DiscretizationDGBR2 disc(0, eta);

  // integrand functor
  IntegrandClass fcnint( pde, bc, {0}, disc );

  CanonicalTraceToCell canonicalTrace(0, 1);

  FieldWeightedClass fcn = fcnint.integrand( xedge,
                                             canonicalTrace,
                                             xfldElem,
                                             qfldElem, rfldElem,
                                             wfldElem, sfldElem);

  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );
  BOOST_CHECK( fcnint.needsFieldTrace() == false );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRefTrace;
  RefCoordCellType sRefCell;

  Real integrandPDETrue, integrandLiftTrue;
  FieldWeightedClass::IntegrandType integrand;
  VectorX X = 0;
  ArrayQ qL, qB, w;
  ArrayQ Fn;
  VectorArrayQ gradqL, gradqliftedL, rlift, dqn, FB, gradw, slift;

  //***********************//
  // Test at sRef={0}, {s,t}={1, 0}
  sRefTrace = {0};
  fcn( sRefTrace, integrand );

  TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalTrace, sRefTrace, sRefCell );

  qfldElem.eval( sRefCell, qL );
  xfldElem.evalGradient( sRefCell, qfldElem, gradqL );
  rfldElem.eval( sRefCell, rlift );

  wfldElem.eval( sRefCell, w );
  sfldElem.eval( sRefCell, slift );
  xfldElem.evalGradient( sRefCell, wfldElem, gradw );

  gradqliftedL = gradqL + eta*rlift;

  // BC state
  bc.state( X, nL, qL, qB );

  // normal flux
  Fn = 0;
  bc.fluxNormal( X, nL, qL, gradqliftedL, qB, Fn );

  // Jump in primitive variable [ nL*( qL - qR ) ]
  for (int d = 0; d < PhysD2::D; d++)
    dqn[d] = nL[d]*(qL - qB);

  FB = 0;
  pde.fluxViscous( X, qB, dqn, FB );

  //                    (bc flux)   (dual consistency)
  integrandPDETrue = dot(w,Fn);
  for (int d = 0; d < PhysD2::D; d++)
    integrandPDETrue += dot(gradw[d],FB[d]);

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //LO residual integrands
  integrandLiftTrue = dot(slift,dqn);

  SANS_CHECK_CLOSE( integrandLiftTrue, integrand.Lift, small_tol, close_tol );


  //***********************//
  // Test at sRef={1}, {s,t}={0, 1}
  sRefTrace = {1};
  fcn( sRefTrace, integrand );

  TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalTrace, sRefTrace, sRefCell );

  qfldElem.eval( sRefCell, qL );
  xfldElem.evalGradient( sRefCell, qfldElem, gradqL );
  rfldElem.eval( sRefCell, rlift );

  wfldElem.eval( sRefCell, w );
  sfldElem.eval( sRefCell, slift );
  xfldElem.evalGradient( sRefCell, wfldElem, gradw );

  gradqliftedL = gradqL + eta*rlift;

  // BC state
  bc.state( X, nL, qL, qB );

  // normal flux
  Fn = 0;
  bc.fluxNormal( X, nL, qL, gradqliftedL, qB, Fn );

  // Jump in primitive variable [ nL*( qL - qR ) ]
  for (int d = 0; d < PhysD2::D; d++)
    dqn[d] = nL[d]*(qL - qB);

  FB = 0;
  pde.fluxViscous( X, qB, dqn, FB );

  //                    (bc flux)   (dual consistency)
  integrandPDETrue = dot(w,Fn);
  for (int d = 0; d < PhysD2::D; d++)
    integrandPDETrue += dot(gradw[d],FB[d]);

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //LO residual integrands
  integrandLiftTrue = dot(slift,dqn);

  SANS_CHECK_CLOSE( integrandLiftTrue, integrand.Lift, small_tol, close_tol );


  //***********************//
  // Test at sRef={1/2}, {s,t}={1/2, 1/2}
  sRefTrace = {1/2.};
  fcn( sRefTrace, integrand );

  TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalTrace, sRefTrace, sRefCell );

  qfldElem.eval( sRefCell, qL );
  xfldElem.evalGradient( sRefCell, qfldElem, gradqL );
  rfldElem.eval( sRefCell, rlift );

  wfldElem.eval( sRefCell, w );
  sfldElem.eval( sRefCell, slift );
  xfldElem.evalGradient( sRefCell, wfldElem, gradw );

  gradqliftedL = gradqL + eta*rlift;

  // BC state
  bc.state( X, nL, qL, qB );

  // normal flux
  Fn = 0;
  bc.fluxNormal( X, nL, qL, gradqliftedL, qB, Fn );

  // Jump in primitive variable [ nL*( qL - qR ) ]
  for (int d = 0; d < PhysD2::D; d++)
    dqn[d] = nL[d]*(qL - qB);

  FB = 0;
  pde.fluxViscous( X, qB, dqn, FB );

  //                    (bc flux)   (dual consistency)
  integrandPDETrue = dot(w,Fn);
  for (int d = 0; d < PhysD2::D; d++)
    integrandPDETrue += dot(gradw[d],FB[d]);

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //LO residual integrands
  integrandLiftTrue = dot(slift,dqn);

  SANS_CHECK_CLOSE( integrandLiftTrue, integrand.Lift, small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_2D_Triangle_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCNavierStokes2D< BCTypeWallNoSlipAdiabatic_mitState, PDEClass > BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementRFieldClass;

  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, DGBR2> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO<Real,Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> BasisWeightedLOClass;
  typedef IntegrandClass::FieldWeighted<Real,Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> FieldWeightedClass;

  // PDE

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde( gas, visc, tcond, Euler_ResidInterp_Raw );

  Real rho, u, v, t;

  rho = 1.225;              // kg/m^3
  u = 69.784; v = -3.231;   // m/s
  t = 288.15;               // K

  // BC
  BCClass bc(pde);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 2 );


  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  for ( int qorder = 2; qorder < 3; qorder++ )
  {
    //solution
    ElementQFieldCell qfldElem(qorder, BasisFunctionCategory_Hierarchical );
    ElementQFieldCell wfldElem(qorder, BasisFunctionCategory_Hierarchical );

    BOOST_CHECK_EQUAL( qorder,   qfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   wfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, wfldElem.nDOF() );

    BOOST_CHECK_EQUAL( qfldElem.nDOF(), wfldElem.nDOF() );

    // line solution
    for ( int dof = 0; dof < qfldElem.nDOF(); dof++ )
    {
      Real frac = (dof+1)*pow(-1,dof);
      qfldElem.DOF(dof) = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rho, u, v, t) );
      qfldElem.DOF(dof) *= frac;
      wfldElem.DOF(dof) = 0;
    }

    // lifting operators
    ElementRFieldClass rfldElem(qorder, BasisFunctionCategory_Hierarchical);
    ElementRFieldClass sfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder,   rfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, rfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   sfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, sfldElem.nDOF() );

    BOOST_CHECK_EQUAL( rfldElem.nDOF(), sfldElem.nDOF() );
    // lifting operator
    for ( int dof = 0; dof < rfldElem.nDOF(); dof++ )
    {
      rfldElem.DOF(dof) = (dof+1)*pow(-1,dof);
      sfldElem.DOF(dof) = 0;
    }
    // BR2 discretization
    Real viscousEtaParameter = 6;
    DiscretizationDGBR2 disc(0, viscousEtaParameter);

    // integrand functor
    IntegrandClass fcnint( pde, bc, {0}, disc );

    BasisWeightedPDEClass fcnPDEB = fcnint.integrand_PDE( xedge,
                                                          CanonicalTraceToCell(0, 1),
                                                          xfldElem,
                                                          qfldElem, rfldElem );
    BasisWeightedLOClass fcnLOB = fcnint.integrand_LO( xedge,
                                                       CanonicalTraceToCell(0, 1),
                                                       xfldElem,
                                                       qfldElem, rfldElem );
    FieldWeightedClass fcnW = fcnint.integrand( xedge,
                                                CanonicalTraceToCell(0, 1),
                                                xfldElem,
                                                qfldElem, rfldElem,
                                                wfldElem, sfldElem);

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;
    const int nDOF = 6; // want to be qorder+1, but can't
    int nIntegrand = qfldElem.nDOF();
    int quadratureorder = 4;

    GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedPDEClass::IntegrandType> integralPDEB(quadratureorder, nIntegrand);
    GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedLOClass::IntegrandType> integralLOB(quadratureorder, nIntegrand);
    ElementIntegral<TopoD1, Line, FieldWeightedClass::IntegrandType> integralW(quadratureorder);

    BasisWeightedPDEClass::IntegrandType rsdPDEElemB[nDOF];
    BasisWeightedLOClass::IntegrandType rsdLOElemB[nDOF];
    FieldWeightedClass::IntegrandType rsdElemW = 0;

    // cell integration for canonical element
    integralPDEB( fcnPDEB, xedge, rsdPDEElemB, nIntegrand );
    integralLOB( fcnLOB, xedge, rsdLOElemB, nIntegrand );

    for ( int i = 0; i < wfldElem.nDOF(); i++ )
    {
      for (int n = 0; n < PDEClass::N; n++)
      {
        // set just one weight to one (i.e. mimic weighting with phi)
        wfldElem.DOF(i)[n] = 1;
        for (int d = 0; d < PhysD2::D; d++ )
          sfldElem.DOF(i)[d][n] = 1;

        // cell integration for canonical Element
        rsdElemW = 0;
        integralW( fcnW, xedge, rsdElemW );

        // test the two integrands are the same
        SANS_CHECK_CLOSE ( rsdElemW.PDE, rsdPDEElemB[i][n], small_tol, close_tol );

        Real tmp = 0;
        for (int d = 0; d < PhysD2::D; d++ )
          tmp += rsdLOElemB[i][d][n];

        SANS_CHECK_CLOSE( rsdElemW.Lift, tmp, small_tol, close_tol );

        // reset to 0
        wfldElem.DOF(i)[n] = 0;
        for (int d = 0; d < PhysD2::D; d++ )
          sfldElem.DOF(i)[d][n] = 0;
      }
    }
  }

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
