// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_DGBR2_Output_btest
// testing of Output Integrand routines

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/list.hpp>

#include "tools/SANSnumerics.h"     // Real

#include "Field/Element/ElementIntegral.h"
#include "Field/Element/ElementLine.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"
#include "Field/Element/ElementLift.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "pde/OutputCategory.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_DGBR2_Output_test_suite )

template<class PhysDim_>
struct DummyOutputFunctional : public OutputType< DummyOutputFunctional<PhysDim_> >
{
  typedef PhysDim_ PhysDim;

  template<class T>
  using ArrayQ = DLA::VectorS<2,T>;

  // Array of outputs
  template<class T>
  using ArrayJ = T;

  // Matrix required to represent the Jacobian of this output functional
  template<class T>
  using MatrixJ = DLA::VectorS<2,T>;

  template<class T>
  using MatrixQ = DLA::MatrixS<2,2,T>;

  explicit DummyOutputFunctional() {}

  bool needsSolutionGradient() const { return false; }

  template<class T>
  void operator()(const Real& x, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayJ<T>& output ) const
  {
    output = q[0]*q[1];
  }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output ) const
  {
    output = q[0]*q[1];
  }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz, ArrayJ<T>& output ) const
  {
    output = q[0]*q[1];
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Integral1D_Line_test )
{
  typedef DummyOutputFunctional<PhysD1> OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef ElementXField<PhysD1      , TopoD1, Line> ElementXFieldType;
  typedef Element      <ArrayQ      , TopoD1, Line> ElementQFieldType;
  typedef ElementLift  <VectorArrayQ, TopoD1, Line> ElementRFieldType;
  typedef typename ElementXFieldType::RefCoordType RefCoordType;

  typedef IntegrandCell_DGBR2_Output<NDOutputClass> IntegrandClass;
  typedef typename IntegrandClass::template Functor<Real,TopoD1,Line,ElementXFieldType> FunctorClass;
  typedef typename IntegrandClass::template ArrayJ<Real> ArrayJ;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  // single line grid
  int order = 1;
  ElementXFieldType xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  xfldElem.DOF(0) = 0.0;
  xfldElem.DOF(1) = 1.0;

  // solution
  // p0
  ElementQFieldType qfld0Elem(0, BasisFunctionCategory_Legendre);
  qfld0Elem.DOF(0) = {0.3, 0.5};

  ElementRFieldType rfld0Elems(0, BasisFunctionCategory_Legendre);
  rfld0Elems[0].DOF(0) = {{-1, 2}};
  rfld0Elems[1].DOF(0) = {{ 3, 0}};

  // p1
  ElementQFieldType qfld1Elem(1, BasisFunctionCategory_Hierarchical);
  qfld1Elem.DOF(0) = {0.3, 0.5};
  qfld1Elem.DOF(1) = {0.8, 0.2};

  ElementRFieldType rfld1Elems(1, BasisFunctionCategory_Hierarchical);
  rfld1Elems[0].DOF(0) = {{-1, 2}};  rfld1Elems[0].DOF(1) = {{4,  3}};
  rfld1Elems[1].DOF(0) = {{ 3, 0}};  rfld1Elems[1].DOF(1) = {{5, -2}};

  // p2
  ElementQFieldType qfld2Elem(2, BasisFunctionCategory_Hierarchical);
  qfld2Elem.DOF(0) = { 0.3, 0.5};
  qfld2Elem.DOF(1) = { 0.8,-0.2};
  qfld2Elem.DOF(2) = {-0.4, 0.7};

  ElementRFieldType rfld2Elems(2, BasisFunctionCategory_Hierarchical);
  rfld2Elems[0].DOF(0) = {{-1, 2}};  rfld2Elems[0].DOF(1) = {{4,  3}};  rfld2Elems[0].DOF(2) = {{0,  1}};
  rfld2Elems[1].DOF(0) = {{ 3, 0}};  rfld2Elems[1].DOF(1) = {{5, -2}};  rfld2Elems[1].DOF(2) = {{6, -2}};

  BOOST_CHECK_EQUAL( 0, qfld0Elem.order() );
  BOOST_CHECK_EQUAL( 1, qfld0Elem.nDOF() );

  BOOST_CHECK_EQUAL( 1, qfld1Elem.order() );
  BOOST_CHECK_EQUAL( 2, qfld1Elem.nDOF() );

  BOOST_CHECK_EQUAL( 2, qfld2Elem.order() );
  BOOST_CHECK_EQUAL( 3, qfld2Elem.nDOF() );

  NDOutputClass outputFcn;
  IntegrandClass output_integrand( outputFcn, {0} );

  ArrayJ output;
  int quadorder = 4;
  RefCoordType sRef;
  ArrayJ integrand;
  ArrayQ q;
  ElementIntegral<TopoD1, Line, ArrayJ> ElemIntegral(quadorder);

  FunctorClass fcnInt0 = output_integrand.integrand(xfldElem, qfld0Elem, rfld0Elems);

  sRef = 0.5;
  fcnInt0(sRef, integrand);
  SANS_CHECK_CLOSE( 0.3*0.5, integrand, small_tol, close_tol );

  ElemIntegral( fcnInt0, xfldElem, output );
  SANS_CHECK_CLOSE( 0.3*0.5, output, small_tol, close_tol );


  FunctorClass fcnInt1 = output_integrand.integrand(xfldElem, qfld1Elem, rfld1Elems);

  sRef = 0.2;
  fcnInt1(sRef, integrand);
  qfld1Elem.eval(sRef, q);
  SANS_CHECK_CLOSE( q[0]*q[1], integrand, small_tol, close_tol );

  sRef = 0.7;
  fcnInt1(sRef, integrand);
  qfld1Elem.eval(sRef, q);
  SANS_CHECK_CLOSE( q[0]*q[1], integrand, small_tol, close_tol );

  ElemIntegral( fcnInt1, xfldElem, output );
  SANS_CHECK_CLOSE( 0.18, output, small_tol, close_tol );


  FunctorClass fcnInt2 = output_integrand.integrand(xfldElem, qfld2Elem, rfld2Elems);

  sRef = 0.2;
  fcnInt2(sRef, integrand);
  qfld2Elem.eval(sRef, q);
  SANS_CHECK_CLOSE( q[0]*q[1], integrand, small_tol, close_tol );

  sRef = 0.7;
  fcnInt2(sRef, integrand);
  qfld2Elem.eval(sRef, q);
  SANS_CHECK_CLOSE( q[0]*q[1], integrand, small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Integral2D_Triangle_test )
{
  typedef DummyOutputFunctional<PhysD2> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef ElementXField<PhysD2      , TopoD2, Triangle> ElementXFieldType;
  typedef Element      <ArrayQ      , TopoD2, Triangle> ElementQFieldType;
  typedef ElementLift  <VectorArrayQ, TopoD2, Triangle> ElementRFieldType;
  typedef typename ElementXFieldType::RefCoordType RefCoordType;

  typedef IntegrandCell_DGBR2_Output<NDOutputClass> IntegrandClass;
  typedef typename IntegrandClass::template Functor<Real,TopoD2,Triangle,ElementXFieldType> FunctorClass;
  typedef typename IntegrandClass::template ArrayJ<Real> ArrayJ;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  // single triangle grid
  int order = 1;
  ElementXFieldType xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  xfldElem.DOF(0) = {0.0, 0.0};
  xfldElem.DOF(1) = {1.0, 0.0};
  xfldElem.DOF(2) = {0.0, 1.0};

  // solution
  // p0
  ElementQFieldType qfld0Elem(0, BasisFunctionCategory_Legendre);
  qfld0Elem.DOF(0) = {0.3, 0.5};

  ElementRFieldType rfld0Elems(0, BasisFunctionCategory_Legendre);
  rfld0Elems[0].DOF(0) = {{-1, 2},{0, 5}};
  rfld0Elems[1].DOF(0) = {{ 3, 0},{4, 8}};
  rfld0Elems[2].DOF(0) = {{ 7,-2},{3, 6}};

  // p1
  ElementQFieldType qfld1Elem(1, BasisFunctionCategory_Hierarchical);
  qfld1Elem.DOF(0) = { 0.3, 0.5};
  qfld1Elem.DOF(1) = { 0.8, 0.2};
  qfld1Elem.DOF(2) = {-0.4, 0.7};

  ElementRFieldType rfld1Elems(1, BasisFunctionCategory_Hierarchical);
  rfld1Elems[0].DOF(0) = {{-1, 2},{0, 5}};  rfld1Elems[0].DOF(1) = {{-6, 2},{1, 3}};  rfld1Elems[0].DOF(2) = {{-4, 7},{6, 8}};
  rfld1Elems[1].DOF(0) = {{ 3, 0},{4, 8}};  rfld1Elems[0].DOF(1) = {{ 8, 2},{3,-2}};  rfld1Elems[0].DOF(2) = {{ 5, 5},{2, 7}};
  rfld1Elems[2].DOF(0) = {{ 7,-2},{3, 6}};  rfld1Elems[0].DOF(1) = {{ 0, 2},{9, 4}};  rfld1Elems[0].DOF(2) = {{ 3, 8},{0, 3}};

  // p2
  ElementQFieldType qfld2Elem(2, BasisFunctionCategory_Hierarchical);
  qfld2Elem.DOF(0) = { 0.3, 0.5};
  qfld2Elem.DOF(1) = { 0.8,-0.2};
  qfld2Elem.DOF(2) = {-0.4, 0.7};
  qfld2Elem.DOF(3) = { 0.1,-0.8};
  qfld2Elem.DOF(4) = {-0.5, 0.5};
  qfld2Elem.DOF(5) = { 0.9, 0.0};

  ElementRFieldType rfld2Elems(2, BasisFunctionCategory_Hierarchical);
  for (int i=0; i < Triangle::NTrace; i++)
    for (int j=0; j < 6; j++)
      rfld2Elems[i].DOF(j) = 0;

  BOOST_CHECK_EQUAL( 0, qfld0Elem.order() );
  BOOST_CHECK_EQUAL( 1, qfld0Elem.nDOF() );

  BOOST_CHECK_EQUAL( 1, qfld1Elem.order() );
  BOOST_CHECK_EQUAL( 3, qfld1Elem.nDOF() );

  BOOST_CHECK_EQUAL( 2, qfld2Elem.order() );
  BOOST_CHECK_EQUAL( 6, qfld2Elem.nDOF() );

  NDOutputClass outputFcn;
  IntegrandClass output_integrand( outputFcn, {0} );

  ArrayJ output;
  int quadorder = 4;
  RefCoordType sRef;
  ArrayJ integrand;
  ArrayQ q;
  ElementIntegral<TopoD2, Triangle, ArrayJ> ElemIntegral(quadorder);

  FunctorClass fcnInt0 = output_integrand.integrand(xfldElem, qfld0Elem, rfld0Elems);

  sRef = {0.2,0.6};
  fcnInt0(sRef, integrand);
  SANS_CHECK_CLOSE( 0.3*0.5, integrand, small_tol, close_tol );

  ElemIntegral( fcnInt0, xfldElem, output );
  SANS_CHECK_CLOSE( 0.075, output, small_tol, close_tol );


  FunctorClass fcnInt1 = output_integrand.integrand(xfldElem, qfld1Elem, rfld1Elems);

  sRef = {0.2, 0.6};
  fcnInt1(sRef, integrand);
  qfld1Elem.eval(sRef, q);
  SANS_CHECK_CLOSE( q[0]*q[1], integrand, small_tol, close_tol );

  sRef = {0.7, 0.1};
  fcnInt1(sRef, integrand);
  qfld1Elem.eval(sRef, q);
  SANS_CHECK_CLOSE( q[0]*q[1], integrand, small_tol, close_tol );


  FunctorClass fcnInt2 = output_integrand.integrand(xfldElem, qfld2Elem, rfld2Elems);

  sRef = {0.2, 0.6};
  fcnInt2(sRef, integrand);
  qfld2Elem.eval(sRef, q);
  SANS_CHECK_CLOSE( q[0]*q[1], integrand, small_tol, close_tol );

  sRef = {0.7, 0.1};
  fcnInt2(sRef, integrand);
  qfld2Elem.eval(sRef, q);
  SANS_CHECK_CLOSE( q[0]*q[1], integrand, small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Integral3D_Tet_test )
{
  typedef DummyOutputFunctional<PhysD3> OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef ElementXField<PhysD3      , TopoD3, Tet> ElementXFieldType;
  typedef Element      <ArrayQ      , TopoD3, Tet> ElementQFieldType;
  typedef ElementLift  <VectorArrayQ, TopoD3, Tet> ElementRFieldType;
  typedef typename ElementXFieldType::RefCoordType RefCoordType;

  typedef IntegrandCell_DGBR2_Output<NDOutputClass> IntegrandClass;
  typedef typename IntegrandClass::template Functor<Real,TopoD3,Tet,ElementXFieldType> FunctorClass;
  typedef typename IntegrandClass::template ArrayJ<Real> ArrayJ;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  // single tet grid
  int order = 1;
  ElementXFieldType xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 4, xfldElem.nDOF() );

  xfldElem.DOF(0) = {0.0, 0.0, 0.0};
  xfldElem.DOF(1) = {1.0, 0.0, 0.0};
  xfldElem.DOF(2) = {0.0, 1.0, 0.0};
  xfldElem.DOF(3) = {0.0, 0.0, 1.0};

  // solution
  // p0
  ElementQFieldType qfld0Elem(0, BasisFunctionCategory_Legendre);
  qfld0Elem.DOF(0) = {0.3, 0.5};

  ElementRFieldType rfld0Elems(0, BasisFunctionCategory_Legendre);
  rfld0Elems[0].DOF(0) = {{-1, 2},{0, 5},{4, 3}};
  rfld0Elems[1].DOF(0) = {{ 3, 0},{4, 8},{6,-2}};
  rfld0Elems[2].DOF(0) = {{ 7,-2},{3, 6},{8, 9}};
  rfld0Elems[3].DOF(0) = {{-2, 8},{5, 1},{3, 5}};

  BOOST_CHECK_EQUAL( 0, qfld0Elem.order() );
  BOOST_CHECK_EQUAL( 1, qfld0Elem.nDOF() );

  NDOutputClass outputFcn;
  IntegrandClass output_integrand( outputFcn, {0} );

  ArrayJ output;
  int quadorder = 4;
  RefCoordType sRef;
  ArrayJ integrand;
  ArrayQ q;
  ElementIntegral<TopoD3, Tet, ArrayJ> ElemIntegral(quadorder);

  FunctorClass fcnInt0 = output_integrand.integrand(xfldElem, qfld0Elem, rfld0Elems);

  sRef = {0.2,0.4,0.3};
  fcnInt0(sRef, integrand);
  SANS_CHECK_CLOSE( 0.3*0.5, integrand, small_tol, close_tol );

  ElemIntegral( fcnInt0, xfldElem, output );
  SANS_CHECK_CLOSE( 0.025, output, small_tol, close_tol );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
