// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianCell_DGBR2_Param_Burgers_btest
// testing of 2-D line-integral jacobian: advection-diffusion

#include <boost/mpl/list.hpp>

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/smoothmath.h"

#include "pde/Burgers/PDEBurgers_ArtificialViscosity1D.h"
#include "pde/AdvectionDiffusion/AdvectionDiffusion_Sensor.h"
#include "pde/Burgers/BCmitAVSensorBurgers1D.h"
#include "pde/Burgers/BCBurgers1D.h"
#include "pde/Burgers/PDEBurgers1D.h"
#include "pde/Burgers/BurgersConservative1D.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "pde/Sensor/PDESensorParameter1D.h"
#include "pde/Sensor/Source1D_JumpSensor.h"
#include "pde/Sensor/BCSensorParameter1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/HField/HFieldLine_DG.h"

#include "Discretization/DG/DiscretizationDGBR2.h"
#include "Discretization/DG/IntegrandCell_DGBR2.h"
#include "Discretization/DG/JacobianCell_DGBR2_Param.h"
#include "Discretization/DG/ResidualCell_DGBR2.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

#define SANS_CHECK_PING_ORDER( err_vec, delta_vec, small_tol ) \
    if (err_vec[1] > small_tol){ \
      Real rate = log(err_vec[1]/err_vec[0])/log(delta_vec[1]/delta_vec[0]); \
      BOOST_CHECK_MESSAGE( rate >= 1.9 && rate <= 4.0, "Rate check failed: rate = " << rate << \
                           ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" ); \
    }


//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianCell_DGBR2_Param_Burgers_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Sensor_Galerkin1D_1Line_X1Q0_Surreal1 )
{
  typedef SurrealS<1> SurrealClass;

  typedef BurgersConservative1D QType;
  typedef PDEBurgers<PhysD1,
                     QType,
                     ViscousFlux1D_Uniform,
                     Source1D_None > PDEBurgers1D;
  typedef PDENDConvertSpace<PhysD1, PDEBurgers1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef AdvectionDiffusion_Sensor Sensor;
  typedef Source1D_JumpSensor<Sensor> Source_JumpSensor;
  typedef PDESensorParameter<PhysD1,
                             AdvectionDiffusionTraits<PhysD1>,
                             Sensor_AdvectiveFlux1D_None,
                             Sensor_ViscousFlux1D_None,
                             Source_JumpSensor > SensorPDEClass;
  typedef PDENDConvertSpace<PhysD1, SensorPDEClass> SensorNDPDEClass;
  typedef SensorNDPDEClass::template ArrayQ<Real> SensorArrayQ;
  typedef SensorNDPDEClass::template VectorArrayQ<Real> SensorVectorArrayQ;
  typedef SensorNDPDEClass::template MatrixQ<Real> SensorMatrixQ;

  typedef IntegrandCell_DGBR2<SensorNDPDEClass> IntegrandClass;
  int qorder = 0;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);
  Source1D_None source;
  PDEClass pde( visc, source );

  Sensor sensor(pde);
  Sensor_AdvectiveFlux1D_None sensor_adv;
  Sensor_ViscousFlux1D_None sensor_visc;
  Source_JumpSensor sensor_source(qorder, sensor);

  SensorNDPDEClass sensorpde(sensor_adv, sensor_visc, sensor_source);

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // grid: 2 lines @ P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;
  HField_DG<PhysD1,TopoD1> hfld(xfld);

  // solution: 2 lines @ P0 (aka Q0)
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
  // Set up a sensor field
  Field_DG_Cell<PhysD1, TopoD1, SensorArrayQ> sensorfld(xfld, 0, BasisFunctionCategory_Legendre);
  sensorfld = 0.0;
  FieldLift_DG_Cell<PhysD1, TopoD1, SensorVectorArrayQ> sensor_rfld(xfld, 0, BasisFunctionCategory_Legendre);
  sensor_rfld = 0.0;

  BOOST_CHECK( qfld.nDOF() == 2 );

  ArrayQ qDOF[2];

  // triangle solution data (left)
  qDOF[0] = qfld.DOF(0) = 3;

  // triangle solution data (right)
  qDOF[1] = qfld.DOF(1) = 7;

  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);
  // integrand
  IntegrandClass fcnint( sensorpde, disc, {0} );

  // quadrature rule (constant)
  int quadratureOrder = 0;

  // jacobian via FD w/ residual operator; assumes scalar PDE
  // topology-specific single group interface

  SLA::SparseVector<SensorArrayQ> rsdGlobal0(2), rsdGlobalm1(2), rsdGlobalp1(2);
  Real jac[2][2][2];
  Real delta[2] = {1e-1, 1e-2};

  rsdGlobal0 = 0;
  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobal0),
                                          (hfld, qfld, xfld), (sensorfld, sensor_rfld), &quadratureOrder, 1);

  for (int kk = 0; kk < 2; kk++)
  {
    for (int j = 0; j < 2; j++)
    {
      qfld.DOF(j) -= delta[kk];
      rsdGlobalm1 = 0;
      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobalm1),
                                              (hfld, qfld, xfld), (sensorfld, sensor_rfld), &quadratureOrder, 1);
      qfld.DOF(j) += 2.0*delta[kk];
      rsdGlobalp1 = 0;
      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobalp1),
                                              (hfld, qfld, xfld), (sensorfld, sensor_rfld), &quadratureOrder, 1);

      for (int i = 0; i < 2; i++)
        jac[i][j][kk] = (rsdGlobalp1[i] - rsdGlobalm1[i]) / (2.0*delta[kk]);

      for (int i = 0; i < 2; i++)
        qfld.DOF(i) = qDOF[i];
    }
  }

  // jacobian via Surreal
  // topology-specific single group interface
  DLA::MatrixD<SensorMatrixQ> mtxGlob(2,2);
  mtxGlob = 0;
  IntegrateCellGroups<TopoD1>::integrate( JacobianCell_DGBR2_Param<SurrealClass, 1>(fcnint, mtxGlob),
                                          (hfld, qfld, xfld), (sensorfld, sensor_rfld), &quadratureOrder, 1 );

  Real err[2];
  const Real small_tol = 1e-10;
  for (int i = 0; i < 2; i++)
  {
    for (int j = 0; j < 2; j++)
    {
      for (int k = 0; k < 2; k++)
      {
        err[k] = (jac[i][j][k] - mtxGlob(i,j));
      }
      SANS_CHECK_PING_ORDER(err, delta, small_tol);
    }
  }

}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Burgers_Galerkin1D_1Line_X1Q0_Surreal1 )
{
  typedef SurrealS<1> SurrealClass;

  typedef BurgersConservative1D QType;
  typedef PDEBurgers<PhysD1,
                     QType,
                     ViscousFlux1D_Uniform,
                     Source1D_None > PDEBurgers1D;
  typedef PDENDConvertSpace<PhysD1, PDEBurgers1D> PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef PDEBurgers_ArtificialViscosity<PhysD1,
                                         QType,
                                         ViscousFlux1D_Uniform,
                                         Source1D_None> ArtificialViscosity;
  typedef PDENDConvertSpace<PhysD1, ArtificialViscosity> NDArtificialViscosity;
  typedef NDArtificialViscosity::template ArrayQ<Real> ArrayQ;
  typedef NDArtificialViscosity::template VectorArrayQ<Real> VectorArrayQ;
  typedef NDArtificialViscosity::template MatrixQ<Real> MatrixQ;

  typedef AdvectionDiffusion_Sensor Sensor;
  typedef Source1D_JumpSensor<Sensor> Source_JumpSensor;
  typedef PDESensorParameter<PhysD1,
                             AdvectionDiffusionTraits<PhysD1>,
                             Sensor_AdvectiveFlux1D_None,
                             Sensor_ViscousFlux1D_None,
                             Source_JumpSensor > SensorPDEClass;
  typedef PDENDConvertSpace<PhysD1, SensorPDEClass> SensorNDPDEClass;
  typedef SensorNDPDEClass::template ArrayQ<Real> SensorArrayQ;

  typedef IntegrandCell_DGBR2<NDArtificialViscosity> IntegrandClass;

  int qorder = 0;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);
  Source1D_None source;
  NDPDEClass pde( visc, source );
  NDArtificialViscosity avpde(qorder, pde);

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // grid: 2 lines @ P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;
  HField_DG<PhysD1,TopoD1> hfld(xfld);

  // solution: 2 lines @ P0 (aka Q0)
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
  qfld = 1.0;
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Legendre);
  rfld = 0.0;
  // Set up a sensor field
  Field_DG_Cell<PhysD1, TopoD1, SensorArrayQ> sensorfld(xfld, 0, BasisFunctionCategory_Legendre);
  sensorfld = 0.0;

  BOOST_CHECK( qfld.nDOF() == 2 );

  ArrayQ sDOF[2];

  // triangle solution data (left)
  sDOF[0] = sensorfld.DOF(0) = 0.3;

  // triangle solution data (right)
  sDOF[1] = sensorfld.DOF(1) = 0.7;

  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);
  // integrand
  IntegrandClass fcnint( avpde, disc, {0} );

  // quadrature rule (constant)
  int quadratureOrder = 0;

  // jacobian via FD w/ residual operator; assumes scalar PDE
  // topology-specific single group interface

  SLA::SparseVector<ArrayQ> rsdGlobal0(2), rsdGlobalm1(2), rsdGlobalp1(2);
  Real jac[2][2][2];
  Real delta[2] = {1e-1, 1e-2};

  rsdGlobal0 = 0;
  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobal0),
                                          (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, 1);

  for (int kk = 0; kk < 2; kk++)
  {
    for (int j = 0; j < 2; j++)
    {
      sensorfld.DOF(j) -= delta[kk];
      rsdGlobalm1 = 0;
      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobalm1),
                                              (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, 1);
      sensorfld.DOF(j) += 2.0*delta[kk];
      rsdGlobalp1 = 0;
      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobalp1),
                                              (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, 1);

      for (int i = 0; i < 2; i++)
        jac[i][j][kk] = (rsdGlobalp1[i] - rsdGlobalm1[i]) / (2.0*delta[kk]);

      for (int i = 0; i < 2; i++)
        sensorfld.DOF(i) = sDOF[i];
    }
  }

  // jacobian via Surreal
  // topology-specific single group interface
  DLA::MatrixD<MatrixQ> mtxGlob(2,2);
  mtxGlob = 0;
  IntegrateCellGroups<TopoD1>::integrate( JacobianCell_DGBR2_Param<SurrealClass, 1>(fcnint, mtxGlob),
                                          (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, 1 );

  Real err[2];
  const Real small_tol = 1e-10;
  for (int i = 0; i < 2; i++)
  {
    for (int j = 0; j < 2; j++)
    {
      for (int k = 0; k < 2; k++)
      {
        err[k] = (jac[i][j][k] - mtxGlob(i,j));
      }
      SANS_CHECK_PING_ORDER(err, delta, small_tol);
    }
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Sensor_Galerkin1D_2Line_X1Q1_Surreal )
{
  typedef SurrealS<1> SurrealClass;

  typedef BurgersConservative1D QType;
  typedef PDEBurgers<PhysD1,
                     QType,
                     ViscousFlux1D_Uniform,
                     Source1D_None > PDEBurgers1D;
  typedef PDENDConvertSpace<PhysD1, PDEBurgers1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef AdvectionDiffusion_Sensor Sensor;
  typedef Source1D_JumpSensor<Sensor> Source_JumpSensor;
  typedef PDESensorParameter<PhysD1,
                             AdvectionDiffusionTraits<PhysD1>,
                             Sensor_AdvectiveFlux1D_None,
                             Sensor_ViscousFlux1D_None,
                             Source_JumpSensor > SensorPDEClass;
  typedef PDENDConvertSpace<PhysD1, SensorPDEClass> SensorNDPDEClass;
  typedef SensorNDPDEClass::template ArrayQ<Real> SensorArrayQ;
  typedef SensorNDPDEClass::template VectorArrayQ<Real> SensorVectorArrayQ;
  typedef SensorNDPDEClass::template MatrixQ<Real> SensorMatrixQ;

  typedef IntegrandCell_DGBR2<SensorNDPDEClass> IntegrandClass;
  int qorder = 1;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);
  Source1D_None source;
  PDEClass pde( visc, source );

  Sensor sensor(pde);
  Sensor_AdvectiveFlux1D_None sensor_adv;
  Sensor_ViscousFlux1D_None sensor_visc;
  Source_JumpSensor sensor_source(qorder, sensor);

  SensorNDPDEClass sensorpde(sensor_adv, sensor_visc, sensor_source);

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // grid: 2 lines @ P1 (aka X1)
  XField1D xfld( 2 );
  HField_DG<PhysD1,TopoD1> hfld(xfld);

  // solution: 2 lines @ P1 (aka Q1)
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
  // Set up a sensor field
  Field_DG_Cell<PhysD1, TopoD1, SensorArrayQ> sensorfld(xfld, 0, BasisFunctionCategory_Legendre);
  sensorfld = 0.0;
  FieldLift_DG_Cell<PhysD1, TopoD1, SensorVectorArrayQ> sensor_rfld(xfld, 0, BasisFunctionCategory_Legendre);
  sensor_rfld = 0.0;

  ArrayQ qDOF[4];

  // line solution (left)
  qDOF[0] = qfld.DOF(0) = 1;
  qDOF[1] = qfld.DOF(1) = 3;

  // line solution (right)
  qDOF[2] = qfld.DOF(2) = 4;
  qDOF[3] = qfld.DOF(3) = 5;

  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);
  // integrand
  IntegrandClass fcnint( sensorpde, disc, {0} );

  // quadrature rule (constant)
  int quadratureOrder = 0;

  // jacobian via FD w/ residual operator; assumes scalar PDE
  // topology-specific single group interface

  SLA::SparseVector<SensorArrayQ> rsdGlobal0(2), rsdGlobalm1(2), rsdGlobalp1(2);
  Real jac[2][4][2];
  Real delta[2] = {1e-1, 1e-2};

  rsdGlobal0 = 0;
  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobal0),
                                          (hfld, qfld, xfld), (sensorfld, sensor_rfld), &quadratureOrder, 1);

  for (int kk = 0; kk < 2; kk++)
  {
    for (int j = 0; j < 4; j++)
    {
      qfld.DOF(j) -= delta[kk];
      rsdGlobalm1 = 0;
      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobalm1),
                                              (hfld, qfld, xfld), (sensorfld, sensor_rfld), &quadratureOrder, 1);
      qfld.DOF(j) += 2.0*delta[kk];
      rsdGlobalp1 = 0;
      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobalp1),
                                              (hfld, qfld, xfld), (sensorfld, sensor_rfld), &quadratureOrder, 1);

      for (int i = 0; i < 2; i++)
        jac[i][j][kk] = (rsdGlobalp1[i] - rsdGlobalm1[i]) / (2.0*delta[kk]);

      for (int i = 0; i < 4; i++)
        qfld.DOF(i) = qDOF[i];
    }
#if 0
    cout << "FD: jacobian = {";
    for (int i = 0; i < 2; i++)
    {
      cout << "{";
      for (int j = 0; j < 4; j++)
      {
        cout << jac[i][j][kk];
        if (j < 3) cout << ", ";
      }
      cout << "}";
      if (i < 1) cout << ", ";
    }
    cout << "}" << endl;
#endif
  }

  // jacobian via Surreal
  // topology-specific single group interface

  DLA::MatrixD<SensorMatrixQ> mtxGlob(2,4);
  mtxGlob = 0;
  IntegrateCellGroups<TopoD1>::integrate( JacobianCell_DGBR2_Param<SurrealClass, 1>(fcnint, mtxGlob),
                                          (hfld, qfld, xfld), (sensorfld, sensor_rfld), &quadratureOrder, 1 );
#if 0
  cout << "Surreal: jacobian = " << endl;
  mtxGlob.dump(2);
#endif

  Real err[2];
  const Real small_tol = 1e-10;
  for (int i = 0; i < 2; i++)
  {
    for (int j = 0; j < 4; j++)
    {
      for (int k = 0; k < 2; k++)
      {
        err[k] = fabs(jac[i][j][k] - mtxGlob(i,j));
      }
      SANS_CHECK_PING_ORDER(err, delta, small_tol);
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Burgers_Galerkin1D_2Line_X1Q1_Surreal )
{
  typedef SurrealS<1> SurrealClass;

  typedef BurgersConservative1D QType;
  typedef PDEBurgers<PhysD1,
                     QType,
                     ViscousFlux1D_Uniform,
                     Source1D_None > PDEBurgers1D;
  typedef PDENDConvertSpace<PhysD1, PDEBurgers1D> PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef PDEBurgers_ArtificialViscosity<PhysD1,
                                         QType,
                                         ViscousFlux1D_Uniform,
                                         Source1D_None> ArtificialViscosity;
  typedef PDENDConvertSpace<PhysD1, ArtificialViscosity> NDArtificialViscosity;
  typedef NDArtificialViscosity::template ArrayQ<Real> ArrayQ;
  typedef NDArtificialViscosity::template VectorArrayQ<Real> VectorArrayQ;
  typedef NDArtificialViscosity::template MatrixQ<Real> MatrixQ;

  typedef AdvectionDiffusion_Sensor Sensor;
  typedef Source1D_JumpSensor<Sensor> Source_JumpSensor;
  typedef PDESensorParameter<PhysD1,
                             AdvectionDiffusionTraits<PhysD1>,
                             Sensor_AdvectiveFlux1D_None,
                             Sensor_ViscousFlux1D_None,
                             Source_JumpSensor > SensorPDEClass;
  typedef PDENDConvertSpace<PhysD1, SensorPDEClass> SensorNDPDEClass;
  typedef SensorNDPDEClass::template ArrayQ<Real> SensorArrayQ;

  typedef IntegrandCell_DGBR2<NDArtificialViscosity> IntegrandClass;

  int qorder = 1;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);
  Source1D_None source;
  NDPDEClass pde( visc, source );
  NDArtificialViscosity avpde(qorder, pde);

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // grid: 2 lines @ P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;
  HField_DG<PhysD1,TopoD1> hfld(xfld);

  // solution: 2 lines @ P0 (aka Q0)
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
  qfld = 1.0;
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Legendre);
  rfld = 0.0;
  // Set up a sensor field
  Field_DG_Cell<PhysD1, TopoD1, SensorArrayQ> sensorfld(xfld, 0, BasisFunctionCategory_Legendre);
  sensorfld = 0.0;

  BOOST_CHECK( qfld.nDOF() == 4 );

  ArrayQ sDOF[2];

  // triangle solution data (left)
  sDOF[0] = sensorfld.DOF(0) = 0.3;

  // triangle solution data (right)
  sDOF[1] = sensorfld.DOF(1) = 0.7;

  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);
  // integrand
  IntegrandClass fcnint( avpde, disc, {0} );

  // quadrature rule (constant)
  int quadratureOrder = 0;

  // jacobian via FD w/ residual operator; assumes scalar PDE
  // topology-specific single group interface

  SLA::SparseVector<ArrayQ> rsdGlobal0(4), rsdGlobalm1(4), rsdGlobalp1(4);
  Real jac[4][2][2];
  Real delta[2] = {1e-1, 1e-2};

  rsdGlobal0 = 0;
  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobal0),
                                          (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, 1);

  for (int kk = 0; kk < 2; kk++)
  {
    for (int j = 0; j < 2; j++)
    {
      sensorfld.DOF(j) -= delta[kk];
      rsdGlobalm1 = 0;
      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobalm1),
                                              (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, 1);
      sensorfld.DOF(j) += 2.0*delta[kk];
      rsdGlobalp1 = 0;
      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobalp1),
                                              (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, 1);

      for (int i = 0; i < 4; i++)
        jac[i][j][kk] = (rsdGlobalp1[i] - rsdGlobalm1[i]) / (2.0*delta[kk]);

      for (int i = 0; i < 2; i++)
        sensorfld.DOF(i) = sDOF[i];
    }
#if 0
    cout << "FD: jacobian = {";
    for (int i = 0; i < 2; i++)
    {
      cout << "{";
      for (int j = 0; j < 4; j++)
      {
        cout << jac[i][j][kk];
        if (j < 3) cout << ", ";
      }
      cout << "}";
      if (i < 1) cout << ", ";
    }
    cout << "}" << endl;
#endif
  }

  // jacobian via Surreal
  // topology-specific single group interface

  DLA::MatrixD<MatrixQ> mtxGlob(4,2);
  mtxGlob = 0;
  IntegrateCellGroups<TopoD1>::integrate( JacobianCell_DGBR2_Param<SurrealClass, 1>(fcnint, mtxGlob),
                                          (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, 1 );
#if 0
  cout << "Surreal: jacobian = " << endl;
  mtxGlob.dump(2);
#endif

  Real err[2];
  const Real small_tol = 1e-10;
  for (int i = 0; i < 4; i++)
  {
    for (int j = 0; j < 2; j++)
    {
      for (int k = 0; k < 2; k++)
      {
        err[k] = fabs(jac[i][j][k] - mtxGlob(i,j));
      }
      SANS_CHECK_PING_ORDER(err, delta, small_tol);
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Sensor_Galerkin1D_3Line_X1Q1_Surreal )
{
  typedef SurrealS<1> SurrealClass;

  typedef BurgersConservative1D QType;
  typedef PDEBurgers<PhysD1,
                     QType,
                     ViscousFlux1D_Uniform,
                     Source1D_None > PDEBurgers1D;
  typedef PDENDConvertSpace<PhysD1, PDEBurgers1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef AdvectionDiffusion_Sensor Sensor;
  typedef Source1D_JumpSensor<Sensor> Source_JumpSensor;
  typedef PDESensorParameter<PhysD1,
                             AdvectionDiffusionTraits<PhysD1>,
                             Sensor_AdvectiveFlux1D_None,
                             Sensor_ViscousFlux1D_None,
                             Source_JumpSensor > SensorPDEClass;
  typedef PDENDConvertSpace<PhysD1, SensorPDEClass> SensorNDPDEClass;
  typedef SensorNDPDEClass::template ArrayQ<Real> SensorArrayQ;
  typedef SensorNDPDEClass::template VectorArrayQ<Real> SensorVectorArrayQ;
  typedef SensorNDPDEClass::template MatrixQ<Real> SensorMatrixQ;

  typedef IntegrandCell_DGBR2<SensorNDPDEClass> IntegrandClass;
  int qorder = 1;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);
  Source1D_None source;
  PDEClass pde( visc, source );

  Sensor sensor(pde);
  Sensor_AdvectiveFlux1D_None sensor_adv;
  Sensor_ViscousFlux1D_None sensor_visc;
  Source_JumpSensor sensor_source(qorder, sensor);

  SensorNDPDEClass sensorpde(sensor_adv, sensor_visc, sensor_source);

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // grid: 2 lines @ P1 (aka X1)
  XField1D xfld( 3 );
  HField_DG<PhysD1,TopoD1> hfld(xfld);

  // solution: 2 lines @ P1 (aka Q1)
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
  // Set up a sensor field
  Field_DG_Cell<PhysD1, TopoD1, SensorArrayQ> sensorfld(xfld, 0, BasisFunctionCategory_Legendre);
  sensorfld = 0.0;
  FieldLift_DG_Cell<PhysD1, TopoD1, SensorVectorArrayQ> sensor_rfld(xfld, 0, BasisFunctionCategory_Legendre);
  sensor_rfld = 0.0;

  ArrayQ qDOF[6];

  // line solution (left)
  qDOF[0] = qfld.DOF(0) = 1;
  qDOF[1] = qfld.DOF(1) = 3;

  // line solution (middle)
  qDOF[2] = qfld.DOF(2) = 4;
  qDOF[3] = qfld.DOF(3) = 5;

  // line solution (middle)
  qDOF[4] = qfld.DOF(4) = 7;
  qDOF[5] = qfld.DOF(5) = 7;

  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);
  // integrand
  IntegrandClass fcnint( sensorpde, disc, {0} );

  // quadrature rule (constant)
  int quadratureOrder = 0;

  // jacobian via FD w/ residual operator; assumes scalar PDE
  // topology-specific single group interface

  SLA::SparseVector<SensorArrayQ> rsdGlobal0(3), rsdGlobalm1(3), rsdGlobalp1(3);
  Real jac[3][6][2];
  Real delta[2] = {1e-1, 1e-2};

  rsdGlobal0 = 0;
  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobal0),
                                          (hfld, qfld, xfld), (sensorfld, sensor_rfld), &quadratureOrder, 1);

  for (int kk = 0; kk < 2; kk++)
  {
    for (int j = 0; j < 6; j++)
    {
      qfld.DOF(j) -= delta[kk];
      rsdGlobalm1 = 0;
      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobalm1),
                                              (hfld, qfld, xfld), (sensorfld, sensor_rfld), &quadratureOrder, 1);
      qfld.DOF(j) += 2.0*delta[kk];
      rsdGlobalp1 = 0;
      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobalp1),
                                              (hfld, qfld, xfld), (sensorfld, sensor_rfld), &quadratureOrder, 1);

      for (int i = 0; i < 3; i++)
        jac[i][j][kk] = (rsdGlobalp1[i] - rsdGlobalm1[i]) / (2.0*delta[kk]);

      for (int i = 0; i < 6; i++)
        qfld.DOF(i) = qDOF[i];
    }
#if 0
    cout << "FD: jacobian = {";
    for (int i = 0; i < 2; i++)
    {
      cout << "{";
      for (int j = 0; j < 6; j++)
      {
        cout << jac[i][j][kk];
        if (j < 5) cout << ", ";
      }
      cout << "}";
      if (i < 1) cout << ", ";
    }
    cout << "}" << endl;
#endif
  }

  // jacobian via Surreal
  // topology-specific single group interface

  DLA::MatrixD<SensorMatrixQ> mtxGlob(3,6);
  mtxGlob = 0;
  IntegrateCellGroups<TopoD1>::integrate( JacobianCell_DGBR2_Param<SurrealClass, 1>(fcnint, mtxGlob),
                                          (hfld, qfld, xfld), (sensorfld, sensor_rfld), &quadratureOrder, 1 );
#if 0
  cout << "Surreal: jacobian = " << endl;
  mtxGlob.dump(2);
#endif

  Real err[2];
  const Real small_tol = 1e-10;
  for (int i = 0; i < 3; i++)
  {
    for (int j = 0; j < 6; j++)
    {
      for (int k = 0; k < 2; k++)
      {
        err[k] = fabs(jac[i][j][k] - mtxGlob(i,j));
      }
      SANS_CHECK_PING_ORDER(err, delta, small_tol);
    }
  }
}
#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Burgers_Galerkin1D_3Line_X1Q1_Surreal )
{
  typedef SurrealS<1> SurrealClass;

  typedef BurgersConservative1D QType;
  typedef PDEBurgers<PhysD1,
                     QType,
                     ViscousFlux1D_Uniform,
                     Source1D_None > PDEBurgers1D;
  typedef PDENDConvertSpace<PhysD1, PDEBurgers1D> PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef Burgers_ArtificialViscosity<PhysD1,
                                      QType,
                                      ViscousFlux1D_Uniform,
                                      Source1D_None> ArtificialViscosity;
  typedef PDENDConvertSpace<PhysD1, ArtificialViscosity> NDArtificialViscosity;
  typedef NDArtificialViscosity::template ArrayQ<Real> ArrayQ;
  typedef NDArtificialViscosity::template VectorArrayQ<Real> VectorArrayQ;
  typedef NDArtificialViscosity::template MatrixQ<Real> MatrixQ;

  typedef AdvectionDiffusion_Sensor Sensor;
  typedef Source1D_JumpSensor<Sensor> Source_JumpSensor;
  typedef PDESensorParameter<PhysD1,
                             AdvectionDiffusionTraits<PhysD1>,
                             Sensor_AdvectiveFlux1D_None,
                             Sensor_ViscousFlux1D_None,
                             Source_JumpSensor > SensorPDEClass;
  typedef PDENDConvertSpace<PhysD1, SensorPDEClass> SensorNDPDEClass;
  typedef SensorNDPDEClass::template ArrayQ<Real> SensorArrayQ;

  typedef IntegrandCell_DGBR2<NDArtificialViscosity> IntegrandClass;

  int qorder = 1;

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);
  Source1D_None source;
  NDPDEClass pde( visc, source );
  NDArtificialViscosity avpde(qorder, pde);

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // grid: 2 lines @ P1 (aka X1)
  XField1D xfld( 3 );
  HField_DG<PhysD1,TopoD1> hfld(xfld);

  // solution: 2 lines @ P0 (aka Q0)
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
  qfld = 1.0;
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Legendre);
  rfld = 0.0;
  // Set up a sensor field
  Field_DG_Cell<PhysD1, TopoD1, SensorArrayQ> sensorfld(xfld, 0, BasisFunctionCategory_Legendre);
  sensorfld = 0.0;

  ArrayQ sDOF[3];

  // solution data
  sDOF[0] = sensorfld.DOF(0) = 0.3;
  sDOF[1] = sensorfld.DOF(1) = 0.7;
  sDOF[2] = sensorfld.DOF(1) = 0.4;

  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);
  // integrand
  IntegrandClass fcnint( avpde, disc, {0} );

  // quadrature rule (constant)
  int quadratureOrder = 0;

  // jacobian via FD w/ residual operator; assumes scalar PDE
  // topology-specific single group interface

  SLA::SparseVector<ArrayQ> rsdGlobal0(6), rsdGlobalm1(6), rsdGlobalp1(6);
  Real jac[6][3][2];
  Real delta[2] = {1e-1, 1e-2};

  rsdGlobal0 = 0;
  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobal0),
                                          (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, 1);

  for (int kk = 0; kk < 2; kk++)
  {
    for (int j = 0; j < 3; j++)
    {
      qfld.DOF(j) -= delta[kk];
      rsdGlobalm1 = 0;
      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobalm1),
                                              (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, 1);
      sensorfld.DOF(j) += 2.0*delta[kk];
      rsdGlobalp1 = 0;
      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnint, rsdGlobalp1),
                                              (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, 1);

      for (int i = 0; i < 6; i++)
        jac[i][j][kk] = (rsdGlobalp1[i] - rsdGlobalm1[i]) / (2.0*delta[kk]);

      for (int i = 0; i < 3; i++)
        sensorfld.DOF(i) = sDOF[i];
    }
#if 0
    cout << "FD: jacobian = {";
    for (int i = 0; i < 2; i++)
    {
      cout << "{";
      for (int j = 0; j < 6; j++)
      {
        cout << jac[i][j][kk];
        if (j < 5) cout << ", ";
      }
      cout << "}";
      if (i < 1) cout << ", ";
    }
    cout << "}" << endl;
#endif
  }

  // jacobian via Surreal
  // topology-specific single group interface

  DLA::MatrixD<MatrixQ> mtxGlob(6,3);
  mtxGlob = 0;
  IntegrateCellGroups<TopoD1>::integrate( JacobianCell_DGBR2_Param<SurrealClass, 1>(fcnint, mtxGlob),
                                          (hfld, sensorfld, xfld), (qfld, rfld), &quadratureOrder, 1 );
#if 0
  cout << "Surreal: jacobian = " << endl;
  mtxGlob.dump(2);
#endif

  Real err[2];
  const Real small_tol = 1e-10;
  for (int i = 0; i < 3; i++)
  {
    for (int j = 0; j < 6; j++)
    {
      for (int k = 0; k < 2; k++)
      {
        err[k] = fabs(jac[i][j][k] - mtxGlob(i,j));
      }
      SANS_CHECK_PING_ORDER(err, delta, small_tol);
    }
  }
}
#endif
#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin2D_1Triangle_X1Q0_Surreal1 )
{
  typedef SurrealS<1> SurrealClass;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion2D::MatrixQ<Real> MatrixQ;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // grid: 2 triangles @ P1 (aka X1)
  XField2D_2Triangle_X1_1Group xfld;

  // solution: 2 triangles @ P0 (aka Q0)
  int qorder = 0;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK( qfld.nDOF() == 2 );

  ArrayQ qDOF[2];

  // triangle solution data (left)
  qDOF[0] = qfld.DOF(0) = 3;

  // triangle solution data (right)
  qDOF[1] = qfld.DOF(1) = 7;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // quadrature rule (linear: basis linear, flux const)
  int quadratureOrder = 1;

  // jacobian via FD w/ residual operator; assumes scalar PDE
  // topology-specific single group interface

  SLA::SparseVector<ArrayQ> rsdGlobal0(2), rsdGlobal1(2);
  Real jac[2][2];

  rsdGlobal0 = 0;
  IntegrateInteriorTraceGroups<TopoD2>::integrate( ResidualInteriorTrace_Galerkin(fcnint, rsdGlobal0),
                                                   xfld, qfld, &quadratureOrder, 1);

  for (int j = 0; j < 2; j++)
  {
    qfld.DOF(j) += 1;

    rsdGlobal1 = 0;
    IntegrateInteriorTraceGroups<TopoD2>::integrate( ResidualInteriorTrace_Galerkin(fcnint, rsdGlobal1),
                                                     xfld, qfld, &quadratureOrder, 1);

    for (int i = 0; i < 2; i++)
      jac[i][j] = rsdGlobal1[i] - rsdGlobal0[i];

    for (int i = 0; i < 2; i++)
      qfld.DOF(i) = qDOF[i];
  }

#if 0
  cout << "FD: jacobian = {";
  for (int i = 0; i < 2; i++)
  {
    cout << "{";
    for (int j = 0; j < 2; j++)
    {
      cout << jac[i][j];
      if (j < 1) cout << ", ";
    }
    cout << "}";
    if (i < 1) cout << ", ";
  }
  cout << "}" << endl;
#endif

  // jacobian via Surreal
  // topology-specific single group interface

  DLA::MatrixD<MatrixQ> mtxGlob(2,2);

  mtxGlob = 0;
  IntegrateInteriorTraceGroups<TopoD2>::integrate( JacobianInteriorTrace_Galerkin<SurrealClass>(fcnint, mtxGlob),
                                                   xfld, qfld, &quadratureOrder, 1);

#if 0
  cout << "Surreal: jacobian = " << endl;
  mtxGlob.dump(2);
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 2; j++)
       SANS_CHECK_CLOSE( jac[i][j], mtxGlob(i,j), small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Galerkin2D_2Triangle_X1Q1_Surreal, SurrealClass, Surreals )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion2D::MatrixQ<Real> MatrixQ;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // line basis
  const BasisFunctionLineBase* xbasisLine = BasisFunctionLineBase::HierarchicalP1;

  BOOST_CHECK_EQUAL( 1, xbasisLine->order() );
  BOOST_CHECK_EQUAL( 2, xbasisLine->nBasis() );

  // area basis
  const BasisFunctionAreaBase<Triangle>* xbasisArea = BasisFunctionAreaBase<Triangle>::HierarchicalP1;
  const BasisFunctionAreaBase<Triangle>* qbasisArea = BasisFunctionAreaBase<Triangle>::HierarchicalP1;

  BOOST_CHECK_EQUAL( 1, xbasisArea->order() );
  BOOST_CHECK_EQUAL( 3, xbasisArea->nBasis() );

  BOOST_CHECK_EQUAL( 1, qbasisArea->order() );
  BOOST_CHECK_EQUAL( 3, qbasisArea->nBasis() );

  // grid: 2 triangles @ P1 (aka X1)
  XField2D_2Triangle_X1_1Group xfld;

  // solution: 2 triangles @ P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  ArrayQ qDOF[6];

  // triangle solution (left)
  qDOF[0] = qfld.DOF(0) = 1;
  qDOF[1] = qfld.DOF(1) = 3;
  qDOF[2] = qfld.DOF(2) = 5;

  // triangle solution (right)
  qDOF[3] = qfld.DOF(3) = 3;
  qDOF[4] = qfld.DOF(4) = 4;
  qDOF[5] = qfld.DOF(5) = 8;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // quadrature rule (quadratic: basis linear, flux linear)
  int quadratureOrder = 2;

  // jacobian via FD w/ residual operator; assumes scalar PDE
  // topology-specific single group interface

  SLA::SparseVector<ArrayQ> rsdGlobal0(6), rsdGlobal1(6);
  Real jac[6][6];

  rsdGlobal0 = 0;
  IntegrateInteriorTraceGroups<TopoD2>::integrate( ResidualInteriorTrace_Galerkin(fcnint, rsdGlobal0),
                                                   xfld, qfld, &quadratureOrder, 1);

#if 0
  cout << "FD: rsdGlobal0 = {";
  for (int i = 0; i < 6; i++)
    cout << rsdGlobal0[i] << ", ";
  cout << "}" << endl;
#endif

  for (int j = 0; j < 6; j++)
  {
    qfld.DOF(j) += 1;

    rsdGlobal1 = 0;
    IntegrateInteriorTraceGroups<TopoD2>::integrate( ResidualInteriorTrace_Galerkin(fcnint, rsdGlobal1),
                                                     xfld, qfld, &quadratureOrder, 1);

    for (int i = 0; i < 6; i++)
      jac[i][j] = rsdGlobal1[i] - rsdGlobal0[i];

    for (int i = 0; i < 6; i++)
      qfld.DOF(i) = qDOF[i];
  }

#if 0
  cout << "FD: jacobian = {";
  for (int i = 0; i < 6; i++)
  {
    cout << "{";
    for (int j = 0; j < 6; j++)
    {
      cout << jac[i][j];
      if (j < 5) cout << ", ";
    }
    cout << "}";
    if (i < 5) cout << ", ";
  }
  cout << "}" << endl;
#endif

  // jacobian via Surreal
  // topology-specific single group interface

  DLA::MatrixD<MatrixQ> mtxGlob(6,6);

  mtxGlob = 0;
  IntegrateInteriorTraceGroups<TopoD2>::integrate( JacobianInteriorTrace_Galerkin<SurrealClass>(fcnint, mtxGlob),
                                                   xfld, qfld, &quadratureOrder, 1);

#if 0
  cout << "Surreal: jacobian = " << endl;
  mtxGlob.dump(2);
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int i = 0; i < 6; i++)
    for (int j = 0; j < 6; j++)
       SANS_CHECK_CLOSE( jac[i][j], mtxGlob(i,j), small_tol, close_tol );
}

#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
