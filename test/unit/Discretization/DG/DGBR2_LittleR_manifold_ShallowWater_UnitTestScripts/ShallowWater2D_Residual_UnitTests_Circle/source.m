function source = source(X,var,param)

r = norm(X);
theta = atan2(X(2),X(1));

%% pde parameters
g = param.g_;
q0 = param.q0_;

%% state vectors & derived variables
varType = 'HVelocity';
H = varInterpret(var,varType,'H');
vx = varInterpret(var,varType,'vx');
vy = varInterpret(var,varType,'vy');
 
%% COMPUTE
geoSeries = 1 + theta + theta^2 + theta^3 + theta^4 + theta^5;
dHdtheta = 1 + 2*theta + 3*theta^2 + 4*theta^3 + 5*theta^4;
b_x = -sin(theta) * ( 1 - q0^2/(g*geoSeries^3) ) * dHdtheta / r;
b_y =  cos(theta) * ( 1 - q0^2/(g*geoSeries^3) ) * dHdtheta / r;

v = sqrt(vx^2 + vy^2);

%% assemble source terms
source = zeros(size(var));

source(1) = 0;
source(2) = - g * H * b_x + v^2/r^2 * H * X(1) + 0.5*g*H^2*cos(theta)/r;
source(3) = - g * H * b_y + v^2/r^2 * H * X(2) + 0.5*g*H^2*sin(theta)/r;

