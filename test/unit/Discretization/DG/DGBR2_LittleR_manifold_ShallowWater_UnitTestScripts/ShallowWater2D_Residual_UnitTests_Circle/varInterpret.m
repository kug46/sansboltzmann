function output = varInterpret(var,...
                               varType,outputType,...
                               varargin)

if nargin > 3
    param = varargin{1};
end

if strcmp( varType, 'HVelocity' )
    
    switch outputType
        case 'H'
            output = var(1);
        case 'vx'
            output = var(2);
        case 'vy'
            output = var(3);
    end
    
end

end