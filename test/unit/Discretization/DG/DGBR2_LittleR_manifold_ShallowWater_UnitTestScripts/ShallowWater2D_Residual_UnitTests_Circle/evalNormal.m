function normal = evalNormal(XL,XR)
% compute the surface normal of the line segment from XL to XR

tangent = ( XR - XL ) / ( (XR-XL)' * (XR-XL) )^0.5;

RotateClock90 = [ 0, 1; -1, 0 ];

normal = RotateClock90 * tangent;

if (norm(normal) - 1) > 10*eps
    error('Norm of unit normal vector is not 1.')
end

end
