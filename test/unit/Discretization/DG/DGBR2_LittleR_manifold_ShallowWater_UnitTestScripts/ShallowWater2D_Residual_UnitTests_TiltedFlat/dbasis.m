function Phi_xi = dbasis(sq)
% evaluate xi-derivative of basis functions.

nn = 2; % two solution nodes per element
nq = length(sq);

if nn ~= 2
    error('Wrong number of solution nodes');
end

Phi_xi = zeros(nq, nn);

% Phi(:,1) = 1 - sq; % 1 - xi
Phi_xi(:,1) = -1 * ones(size(sq)); % xi

% Phi(:,2) = sq; % xi
Phi_xi(:,2) = 1 * ones(size(sq)); % xi
