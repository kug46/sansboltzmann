function gradPhi = basisGrad(xi,Xxi,normal)
% compute surface gradient of basis functions from their xi-derivatives.

T = [ Xxi'; normal' ]; % T * nablasphi = [ Phi_xi; 0 ]

nn = 2;

gradPhi = zeros(2,nn);

Phi_xi = dbasis(xi);

for i = 1:nn
    gradPhi(:,i) = T \ [ Phi_xi(1,i); 0 ];
end

end