function [ fx, fz ] = fluxAdvective(X,var,param)
% compute the advective flux terms

theta = atan2(X(2),X(1));

%% parameters
g = param.g_;

%% state vectors & derived variables
varType = 'HVelocity';
H = varInterpret(var,varType,'H');
vx = varInterpret(var,varType,'vx');
vy = varInterpret(var,varType,'vy');

%% assemble advective flux terms
fx = zeros(size(var));
fz = zeros(size(var));

fx(1) = H * vx;
fz(1) = H * vy;

fx(2) = H * vx * vx + 0.5*g*H^2;
fz(2) = H * vx * vy;

fx(3) = H * vy * vx;
fz(3) = H * vy * vy + 0.5*g*H^2;
