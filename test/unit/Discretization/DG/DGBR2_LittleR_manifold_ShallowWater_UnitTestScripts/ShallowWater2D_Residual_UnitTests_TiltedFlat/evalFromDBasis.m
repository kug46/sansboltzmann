function q = evalFromDBasis(xi,qL,qR)

if (size(qL,2) ~= 1) || (size(qR,2) ~= 1)
    error('varL or varL is not column vector.')
end

if (size(xi,1) ~= 1) || (size(xi,2) ~= 1)
    error('xi is not a scalar')
end

q = [qL, qR] * dbasis(xi)';