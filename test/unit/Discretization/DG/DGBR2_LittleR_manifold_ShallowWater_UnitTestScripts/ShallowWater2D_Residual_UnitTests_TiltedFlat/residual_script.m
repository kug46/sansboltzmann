clear all; close all;

N = 3; % number of equations per element

%% parameters
param = struct;

% PDE
param.g_ = 9.81;
param.q0_ = 18.5;
param.normal = [0; 0]; % placeholder; to be computed later

%% nodal variable data
% left node
xL = 0; yL = 0;

theta = atan2(yL,xL);

XL = [xL, yL]';
sL = norm(XL);
HL = 3.4;
vxL = 3.1;
vyL = -2.2;
varL = [ HL, vxL, vyL ]';

% right node
XR = [1, 0]';
sR = norm(XR);
HR = 9.7;
vxR = 19;
vyR = 1.2;
varR = [ HR, vxR, vyR ]';

param.normal = evalNormal(XL,XR);

%% integrand
% reference coordinates xi
xi_array = [ 0; 0.6; 1 ];

for i = 1:length(xi_array)
    xi = xi_array(i);
    X = evalFromBasis(xi,XL,XR);
    Xxi = evalFromDBasis(xi,XL,XR);
    
    var = evalFromBasis(xi,varL,varR);
    
    [ integrand_source, integrand_fluxadvective ] = ...
        IntegrandCell(xi,X,Xxi,var,param);
    
    %% print output
    fprintf(sprintf('Integrand: xi = %1.2f\n',xi));
    for j = 1:2
        fprintf(sprintf('// Basis function %d\n',j));
        fprintf(sprintf('HIntegrand = ( %5.16e ) + ( %5.16e );\n',integrand_source(1,j), integrand_fluxadvective(1,j)));
        fprintf(sprintf('vxIntegrand = ( %5.16e ) + ( %5.16e );\n',integrand_source(2,j), integrand_fluxadvective(2,j)));
        fprintf(sprintf('vyIntegrand = ( %5.16e ) + ( %5.16e );\n',integrand_source(3,j), integrand_fluxadvective(3,j)));
        fprintf(sprintf('integrandTrue[%d] = { HIntegrand, vxIntegrand, vyIntegrand };\n',j-1));
        fprintf('\n');
    end
    
end

%% residual (integrals)
% set up quadrature
Nq = 20; % number of Gauss quadrature points. Quadrature order = 2*Nq - 1
xiqLim = [0, 1]; % integration limits
[xiq,wq] = lgwt(xiqLim,Nq); % evaluate Gaussian quadrature weights and locations

% evaluate integrands and apply quadrature
residualCell_source = zeros(N,2);
residualCell_fluxadvective = zeros(N,2);

for i = 1:Nq
    xi = xiq(i);
    X = evalFromBasis(xi,XL,XR);
    Xxi = evalFromDBasis(xi,XL,XR);
    detJ = sqrt(Xxi'*Xxi);
   
    var = evalFromBasis(xi,varL,varR);
    
    [ integrand_source, integrand_fluxadvective ] = IntegrandCell(xi,X,Xxi,var,param);
    
    residualCell_source = residualCell_source ...
        + wq(i)*detJ * integrand_source;
    residualCell_fluxadvective = residualCell_fluxadvective ...
        + wq(i)*detJ * integrand_fluxadvective;
end

%% print output
fprintf('Residual\n');
for i = 1:2
    fprintf(sprintf('// Basis function %d\n',i));
    fprintf(sprintf('HRes = ( %5.16e ) + ( %5.16e );\n',residualCell_source(1,i), residualCell_fluxadvective(1,i)));
    fprintf(sprintf('vxRes = ( %5.16e ) + ( %5.16e );\n',residualCell_source(2,i), residualCell_fluxadvective(2,i)));
    fprintf(sprintf('vyRes = ( %5.16e ) + ( %5.16e );\n',residualCell_source(3,i), residualCell_fluxadvective(3,i)));
    fprintf(sprintf('residualTrue[%d] = { HRes, vxRes, vyRes };\n',i-1));
    fprintf('\n');
end

