function source = source(X,var,param)

s = norm(X);
theta = atan2(X(2),X(1));

%% pde parameters
g = param.g_;
q0 = param.q0_;

%% state vectors & derived variables
varType = 'HVelocity';
H = varInterpret(var,varType,'H');
 
%% COMPUTE
geoSeries = 1 + s + s^2 + s^3 + s^4 + s^5;
dHdx = 1 + 2*s + 3*s^2 + 4*s^3 + 5*s^4;
b_x = ( 1 - q0^2/(g*geoSeries^3) ) * dHdx * cos(theta);
b_y = ( 1 - q0^2/(g*geoSeries^3) ) * dHdx * sin(theta);

%% assemble source terms
source = zeros(size(var));

source(1) = 0;
source(2) = - g * H * b_x;
source(3) = - g * H * b_y;

