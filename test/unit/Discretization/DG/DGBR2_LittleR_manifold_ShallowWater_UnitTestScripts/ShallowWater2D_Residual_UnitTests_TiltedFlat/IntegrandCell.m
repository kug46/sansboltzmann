function [ integrand_source, integrand_fluxadvective ] ...
    = IntegrandCell(xi,X,Xxi,var,param)

normal = param.normal;

% evaluate weighting function & gradient
Phi = basis(xi);
gradPhi = basisGrad(xi,Xxi,normal);

% evaluate advective flux terms
[ fx, fz ] = fluxAdvective(X,var,param);

% evaluate source terms
sourceTerms = source(X,var,param);

% evaluate integrand
integrand_source        = sourceTerms * Phi;
integrand_fluxadvective = -  [fx,fz] * gradPhi;

end