clear all; close all;

N = 2; % number of equations per element

%% parameters
param = struct;

% PDE
param.g_ = 9.81;
param.q0_ = 18.5;
param.normal = [0; 0]; % placeholder; to be computed later

%% nodal variable data
% left node
xL = 0;
HL = 1 + xL + xL^2 + xL^3 + xL^4 + xL^5;
vxL = param.q0_/HL;
varL = [ HL, vxL ]';

% right node
xR = 1;
HR = 1 + xR + xR^2 + xR^3 + xR^4 + xR^5;
vxR = param.q0_/HR;
varR = [ HR, vxR ]';

%% integrand
% reference coordinates xi
xi_array = [ 0; 0.6; 1 ];

for i = 1:length(xi_array)
    xi = xi_array(i);
    X = evalFromBasis(xi,xL,xR);
    Xxi = evalFromDBasis(xi,xL,xR);
    
    var = evalFromBasis(xi,varL,varR);
    varX = evalFromBasisGrad(xi,varL,varR,Xxi);
    
    [ integrand_source, integrand_fluxadvective ] = ...
        IntegrandCell(xi,X,Xxi,var,varX,param);
    
    %% print output
    fprintf(sprintf('Integrand: xi = %1.2f\n',xi));
    for j = 1:2
        fprintf(sprintf('// Basis function %d\n',j));
        fprintf(sprintf('HIntegrand = ( %5.16e ) + ( %5.16e );\n',integrand_source(1,j), integrand_fluxadvective(1,j)));
        fprintf(sprintf('uIntegrand = ( %5.16e ) + ( %5.16e );\n',integrand_source(2,j), integrand_fluxadvective(2,j)));
        fprintf(sprintf('integrandTrue[%d] = { HIntegrand, uIntegrand };\n',j-1));
        fprintf('\n');
    end
    
end

%% residual (integrals)
% set up quadrature
Nq = 20; % number of Gauss quadrature points. Quadrature order = 2*Nq - 1
xiqLim = [0, 1]; % integration limits
[xiq,wq] = lgwt(xiqLim,Nq); % evaluate Gaussian quadrature weights and locations

% evaluate integrands and apply quadrature
residualCell_source = zeros(N,2);
residualCell_fluxadvective = zeros(N,2);

for i = 1:Nq
    xi = xiq(i);
    X = evalFromBasis(xi,xL,xR);
    Xxi = evalFromDBasis(xi,xL,xR);
    detJ = sqrt(Xxi'*Xxi);
    
    var = evalFromBasis(xi,varL,varR);
    varX = evalFromBasisGrad(xi,varL,varR,Xxi);
    
    [ integrand_source, integrand_fluxadvective ] ...
        = IntegrandCell(xi,X,Xxi,var,varX,param);

    residualCell_source = residualCell_source ...
        + wq(i)*detJ * integrand_source;
    residualCell_fluxadvective = residualCell_fluxadvective ...
        + wq(i)*detJ * integrand_fluxadvective;
end

%% print output
fprintf('Residual\n');
for i = 1:2
    fprintf(sprintf('// Basis function %d\n',i));
    fprintf(sprintf('HRes = ( %5.16e ) + ( %5.16e );\n',residualCell_source(1,i), residualCell_fluxadvective(1,i)));
    fprintf(sprintf('uRes = ( %5.16e ) + ( %5.16e );\n',residualCell_source(2,i), residualCell_fluxadvective(2,i)));
    fprintf(sprintf('residualTrue[%d] = { HRes, uRes };\n',i-1));
    fprintf('\n');
end

