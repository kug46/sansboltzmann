function [ integrand_source, integrand_fluxadvective ] ...
    = IntegrandCell(xi,X,Xxi,var,varX,param)

% evaluate weighting function & gradient
Phi = basis(xi);
gradPhi = basisGrad(xi,Xxi);

% evaluate advective flux terms
f = fluxAdvective(var,param);

% evaluate source terms
sourceTerms = source(X,var,varX,param);

% evaluate integrand
integrand_source        = sourceTerms * Phi;
integrand_fluxadvective = -  f * gradPhi;

end