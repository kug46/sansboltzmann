function gradPhi = basisGrad(xi,Xxi)
% compute surface gradient of basis functions from their xi-derivatives.

nn = 2;

gradPhi = zeros(1,nn);

Phi_xi = dbasis(xi);

for i = 1:nn
    gradPhi(i) = Phi_xi(i) / Xxi;
end

end