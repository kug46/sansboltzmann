function normal = evalNormal(XL,XR)
% compute the surface normal of the line segment from XL to XR

tangent = ( XR - XL ) / ( (XR-XL)' * (XR-XL) )^0.5;

RotateCountclock90 = [ 0, -1; 1, 0 ];

normal = RotateCountclock90 * tangent;

end
