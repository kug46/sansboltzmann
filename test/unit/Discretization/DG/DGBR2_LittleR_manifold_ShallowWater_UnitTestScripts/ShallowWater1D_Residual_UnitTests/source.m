function source = source(X,var,varX,param)

x = X(1);

%% pde parameters
g = param.g_;
q0 = param.q0_;

%% state vectors & derived variables
varType = 'HVelocity';
H = varInterpret(var,varType,'H');
H_x = varInterpret(varX(:,1),varType,'H_x');
 
%% COMPUTE
geoSeries = 1 + x + x^2 + x^3 + x^4 + x^5;
dHdx = 1 + 2*x + 3*x^2 + 4*x^3 + 5*x^4;
b_x = ( 1 - q0^2/(g*geoSeries^3) ) * dHdx;

%% assemble source terms
source = zeros(size(var));

source(1) = 0;

source(2) = - g * H * b_x;

% source(2) = g * H * (H_x - b_x);

