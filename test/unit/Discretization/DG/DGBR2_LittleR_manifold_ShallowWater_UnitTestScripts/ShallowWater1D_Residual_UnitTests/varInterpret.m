function output = varInterpret(var,...
                               varType,outputType,...
                               varargin)

if nargin > 3
    param = varargin{1};
end

if strcmp( varType, 'HVelocity' )
    
    switch outputType
        case 'H'
            output = var(1);
        case 'u'
            output = var(2);
        case 'H_x'
            output = var(1,1);
    end
    
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rhoe = getDensityEdge(var,varType,param)
% compute density at BL edge.
gam_ = param.gam_;
R_ = param.R_;

T0 = varInterpret(var,var,var,varType,'T0Edge');
p0 = varInterpret(var,var,var,varType,'p0Edge');
qe = varInterpret(var,var,var,varType,'SpeedEdge');

Te = T0 - ( gam_ - 1 )/( 2 * gam_ * R_ ) * qe^2;
Me = qe / sqrt( gam_ * R_ * Te );
pe = p0 * ( 1 + (gam_-1)/2 * Me^2 )^(-gam_);
rhoe = 2 * ( p0 - pe ) / qe^2;
end