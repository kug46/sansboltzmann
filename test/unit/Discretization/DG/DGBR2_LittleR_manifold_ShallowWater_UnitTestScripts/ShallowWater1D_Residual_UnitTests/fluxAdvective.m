function f = fluxAdvective(var,param)
% compute the advective flux terms

%% parameters
g = param.g_;

%% state vectors & derived variables
varType = 'HVelocity';
H = varInterpret(var,varType,'H');
u = varInterpret(var,varType,'u');

%% assemble advective flux terms
f = zeros(size(var));

f(1) = H * u;

f(2) = H * u * u + 0.5 * g * H^2;

% f(2) = H * u * u;

