// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandBoundaryTrace_LinearScalar_sansLG_DGBR2_AD_btest
// testing of boundary integrands: Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Discretization/DG/IntegrandBoundaryTrace_LinearScalar_sansLG_DGBR2.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_None> PDEAdvectionDiffusion1D;
typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_None> PDEAdvectionDiffusion2D;
//typedef PDEAdvectionDiffusion<PhysD3,
//                              AdvectiveFlux3D_Uniform,
//                              ViscousFlux3D_Uniform,
//                              Source3D_None> PDEAdvectionDiffusion3D;

typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass1D;
typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass2D;
//typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass3D;

typedef BCNDConvertSpace<PhysD1, BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_sansLG> > BCClass1D;
typedef BCNDConvertSpace<PhysD2, BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_sansLG> > BCClass2D;
//typedef BCNDConvertSpace<PhysD3, BCAdvectionDiffusion<PhysD3,BCTypeLinearRobin_sansLG> > BCClass3D;

typedef NDVectorCategory<boost::mpl::vector1<BCClass1D>, BCClass1D::Category> NDBCVecCat1D;
typedef NDVectorCategory<boost::mpl::vector1<BCClass2D>, BCClass2D::Category> NDBCVecCat2D;
//typedef NDVectorCategory<boost::mpl::vector1<BCClass3D>, BCClass3D::Category> NDBCVecCat3D;

typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldLine;
typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldArea_Triangle;
typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldArea_Quad;
//typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldVolume_Tet;
//typedef ElementXField<PhysD3,TopoD3,Hex> ElementXFieldVolume_Hex;

// 1D
template class IntegrandBoundaryTrace<PDEClass1D, NDBCVecCat1D, DGBR2>
               ::BasisWeighted_PDE<Real, TopoD0, Node, TopoD1, Line, ElementXFieldLine>;
// 2D
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, DGBR2>
               ::BasisWeighted_PDE<Real, TopoD1, Line, TopoD2, Triangle, ElementXFieldArea_Triangle>;
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, DGBR2>
               ::BasisWeighted_PDE<Real, TopoD1, Line, TopoD2, Quad, ElementXFieldArea_Quad>;
// 3D
//template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
//               ::BasisWeighted_PDE<Real, TopoD2, Triangle, TopoD3, Tet, ElementXFieldVolume_Tet>;
//template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
//               ::BasisWeighted_PDE<Real, TopoD2, Quad, TopoD3, Hex, ElementXFieldVolume_Hex>;


// 1D
template class IntegrandBoundaryTrace<PDEClass1D, NDBCVecCat1D, DGBR2>
               ::BasisWeighted_LO<Real, Real, TopoD0, Node, TopoD1, Line, ElementXFieldLine>;
// 2D
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, DGBR2>
               ::BasisWeighted_LO<Real, Real, TopoD1, Line, TopoD2, Triangle, ElementXFieldArea_Triangle>;
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, DGBR2>
               ::BasisWeighted_LO<Real, Real, TopoD1, Line, TopoD2, Quad, ElementXFieldArea_Quad>;
// 3D
//template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
//               ::BasisWeighted_LO<Real, Real, TopoD2, Triangle, TopoD3, Tet, ElementXFieldVolume_Tet>;
//template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
//               ::BasisWeighted_LO<Real, Real, TopoD2, Quad, TopoD3, Hex, ElementXFieldVolume_Hex>;

// 1D
template class IntegrandBoundaryTrace<PDEClass1D, NDBCVecCat1D, DGBR2>
               ::FieldWeighted<Real, Real, TopoD0, Node, TopoD1, Line, ElementXFieldLine>;
// 2D
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, DGBR2>
               ::FieldWeighted<Real, Real, TopoD1, Line, TopoD2, Triangle, ElementXFieldArea_Triangle>;
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, DGBR2>
               ::FieldWeighted<Real, Real, TopoD1, Line, TopoD2, Quad, ElementXFieldArea_Quad>;
// 3D
//template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
//               ::FieldWeighted<Real, Real, TopoD2, Triangle, TopoD3, Tet, ElementXFieldVolume_Tet>;
//template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
//               ::FieldWeighted<Real, Real, TopoD2, Quad, TopoD3, Hex, ElementXFieldVolume_Hex>;
}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandBoundaryTrace_LinearScalar_sansLG_DGBR2_AD_btest_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Line_LinearRobin_X1Q1R1_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD1::D,ArrayQ> VectorArrayQ;

  typedef BCNDConvertSpace<PhysD1, BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_sansLG> > BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementRFieldClass;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO<Real,Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> BasisWeightedLOClass;

  // PDE

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // BC
  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = {x1};
  xfldElem.DOF(1) = {x2};

  xnode.DOF(0) = {x2};
  xnode.normalSignL() = 1;

  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // line solution (left)
  qfldElem.DOF(0) = 2;
  qfldElem.DOF(1) = 3;

  // lifting operators
  ElementRFieldClass rfldElem(order, BasisFunctionCategory_Hierarchical);

  rfldElem.DOF(0) = -1;  rfldElem.DOF(1) = 5;

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand functor

  IntegrandClass fcnint( pde, bc, {0}, disc );
  BOOST_CHECK( fcnint.needsFieldTrace() == false );

  BasisWeightedPDEClass fcnPDE = fcnint.integrand_PDE( xnode, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, rfldElem );
  BasisWeightedLOClass fcnLO = fcnint.integrand_LO( xnode, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, rfldElem );

  BOOST_CHECK_EQUAL( 2, fcnPDE.nDOF() );
  BOOST_CHECK( fcnPDE.needsEvaluation() == true );

  BOOST_CHECK_EQUAL( 2, fcnLO.nDOF() );
  BOOST_CHECK( fcnLO.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;
  Real integrandPDETrue[2];
  Real integrandLiftxTrue[2];
  BasisWeightedPDEClass::IntegrandType integrandPDE[2];
  BasisWeightedLOClass::IntegrandType integrandLO[2];

  sRef = 0;
  fcnPDE( sRef, integrandPDE, 2 );
  fcnLO( sRef, integrandLO, 2 );

  //PDE residual integrands (left): (advective) + (viscous) + (BC) + (lifting-operator)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + ( 150858257./6500000. ) + ( 0 );  // Basis function 1
  integrandPDETrue[1] = ( 33./10. ) + ( -2123./1000. ) + ( -122434657./6500000. ) + ( -2123./100. );  // Basis function 2

  //LO residual integrands (left): (lifting-operator)
  integrandLiftxTrue[0] = ( 0 );  // Basis function 1
  integrandLiftxTrue[1] = ( 71059./6500. );  // Basis function 2

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLiftxTrue[0], integrandLO[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[1], integrandLO[1][0], small_tol, close_tol );

  // test the trace element integral of the functor
  int quadratureorder = 0;
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD0, Node, BasisWeightedPDEClass::IntegrandType> integralPDE(quadratureorder, nIntegrand);
  GalerkinWeightedIntegral<TopoD0, Node, BasisWeightedLOClass::IntegrandType> integralLO(quadratureorder, nIntegrand);

  BasisWeightedPDEClass::IntegrandType rsdPDEElem[2] = {0,0};
  BasisWeightedLOClass::IntegrandType rsdLOElem[2] = {0,0};
  Real rsdPDETrue[2];
  Real rsdLiftxTrue[2];

  // cell integration for canonical element
  integralPDE( fcnPDE, xnode, rsdPDEElem, nIntegrand );
  integralLO( fcnLO, xnode, rsdLOElem, nIntegrand );

  //PDE residuals (left): (advective) + (viscous) + (BC) + (lifting-operator)
  rsdPDETrue[0] = ( 0 ) + ( 0 ) + ( 150858257./6500000. ) + ( 0 ); // Basis function 1
  rsdPDETrue[1] = ( 33./10. ) + ( -2123./1000. ) + ( -122434657./6500000. ) + ( -2123./100. ); // Basis function 2

  //LO residuals (left): (lifting-operator)
  rsdLiftxTrue[0] = ( 0 );   // Basis function 1
  rsdLiftxTrue[1] = ( 71059./6500. );   // Basis function 2

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDEElem[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDEElem[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdLiftxTrue[0], rsdLOElem[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftxTrue[1], rsdLOElem[1][0], small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_Line_X1Q1R1W2S2_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD1::D,ArrayQ> VectorArrayQ;

  typedef BCNDConvertSpace<PhysD1, BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_sansLG> > BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementRFieldClass;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> FieldWeightedClass;

  // PDE

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // BC
  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = {x1};
  xfldElem.DOF(1) = {x2};

  xnode.DOF(0) = {x2};
  xnode.normalSignL() = 1;

  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // line solution (left)
  qfldElem.DOF(0) = 2;
  qfldElem.DOF(1) = 3;

  // lifting operators
  ElementRFieldClass rfldElem(order, BasisFunctionCategory_Hierarchical);

  rfldElem.DOF(0) = -1;  rfldElem.DOF(1) = 5;

  // weight
  ElementQFieldCell wfldElem(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElem.order() );
  BOOST_CHECK_EQUAL( 3, wfldElem.nDOF() );

  // line solution (left)
  wfldElem.DOF(0) = 3;
  wfldElem.DOF(1) = 4;
  wfldElem.DOF(2) = 5;

  // lifting operators
  ElementRFieldClass sfldElem(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, sfldElem.order() );
  BOOST_CHECK_EQUAL( 3, sfldElem.nDOF() );

  sfldElem.DOF(0) = -5;  sfldElem.DOF(1) = 3;  sfldElem.DOF(2) = 2;

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand functor
  IntegrandClass fcnint( pde, bc, {0}, disc );

  FieldWeightedClass fcn = fcnint.integrand( xnode,
                                             CanonicalTraceToCell(0, 1),
                                             xfldElem,
                                             qfldElem, rfldElem,
                                             wfldElem, sfldElem);

  BOOST_CHECK_EQUAL( 2, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );
  BOOST_CHECK( fcnint.needsFieldTrace() == false );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;
  FieldWeightedClass::IntegrandType integrand;
  Real integrandPDETrue, integrandLiftTrue;

  // Test at sRef={0}, s={1}
  sRef = {0};
  fcn( sRef, integrand );

  //PDE residual integrands (left): (advective) + (viscous) + (BC) + (lifting-operator)
  integrandPDETrue = ( 66./5. ) + ( -2123./250. ) + ( 2980001283./6500000. ) + ( -2123./25. );  // Basis function
  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //LO residual integrands (left): (lifting-operator)
  integrandLiftTrue = ( 213177./6500. );  // Weight Function
  SANS_CHECK_CLOSE( integrandLiftTrue, integrand.Lift, small_tol, close_tol );

  // test the trace element integral of the functor
  int quadratureorder = 0;
  ElementIntegral<TopoD0, Node, FieldWeightedClass::IntegrandType> integral(quadratureorder);

  FieldWeightedClass::IntegrandType rsdElem=0;
  Real rsdPDETrue, rsdLiftTrue;

  // cell integration for canonical element
  integral( fcn, xnode, rsdElem );

  //PDE residuals (left): (advective) + (viscous) + (BC) + (lifting-operator)
  rsdPDETrue = ( 66./5. ) + ( -2123./250. ) + ( 2980001283./6500000. ) + ( -2123./25. ); // Basis function
  SANS_CHECK_CLOSE( rsdPDETrue, rsdElem.PDE, small_tol, close_tol );

  //LO residuals (left): (lifting-operator)
  rsdLiftTrue = ( 213177./6500. );   // Basis function
  SANS_CHECK_CLOSE( rsdLiftTrue, rsdElem.Lift, small_tol, close_tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_1D_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD1::D,ArrayQ> VectorArrayQ;

  typedef BCNDConvertSpace<PhysD1, BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_sansLG> > BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementRFieldClass;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO<Real,Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> BasisWeightedLOClass;
  typedef IntegrandClass::FieldWeighted<Real,Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> FieldWeightedClass;
  // PDE

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3;
  Source1D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // BC
  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = {x1};
  xfldElem.DOF(1) = {x2};

  xnode.DOF(0) = {x2};
  xnode.normalSignL() = 1;

  for ( int qorder = 2; qorder < 3; qorder++ )
  {
    //solution
    ElementQFieldCell qfldElem(qorder, BasisFunctionCategory_Hierarchical );
    ElementQFieldCell wfldElem(qorder, BasisFunctionCategory_Hierarchical );

    BOOST_CHECK_EQUAL( qorder,   qfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   wfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, wfldElem.nDOF() );

    BOOST_CHECK_EQUAL( qfldElem.nDOF(), wfldElem.nDOF() );
    // line solution
    for ( int dof = 0; dof < qfldElem.nDOF(); dof++ )
    {
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);
      wfldElem.DOF(dof) = 0;
    }

    // lifting operators
    ElementRFieldClass rfldElem(qorder, BasisFunctionCategory_Hierarchical);
    ElementRFieldClass sfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder,   rfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, rfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   sfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, sfldElem.nDOF() );

    BOOST_CHECK_EQUAL( rfldElem.nDOF(), sfldElem.nDOF() );
    // lifting operator
    for ( int dof = 0; dof < rfldElem.nDOF(); dof++ )
    {
      rfldElem.DOF(dof) = (dof+1)*pow(-1,dof);
      sfldElem.DOF(dof) = 0;
    }
    // BR2 discretization
    Real viscousEtaParameter = 2;
    DiscretizationDGBR2 disc(0, viscousEtaParameter);

    // integrand functor
    IntegrandClass fcnint( pde, bc, {0}, disc );

    BasisWeightedPDEClass fcnPDEB = fcnint.integrand_PDE( xnode,
                                                          CanonicalTraceToCell(0, 1),
                                                          xfldElem,
                                                          qfldElem, rfldElem );
    BasisWeightedLOClass fcnLOB = fcnint.integrand_LO( xnode,
                                                       CanonicalTraceToCell(0, 1),
                                                       xfldElem,
                                                       qfldElem, rfldElem );
    FieldWeightedClass fcnW = fcnint.integrand( xnode,
                                                CanonicalTraceToCell(0, 1),
                                                xfldElem,
                                                qfldElem, rfldElem,
                                                wfldElem, sfldElem);

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;
    const int nDOF = 3; // want to be qorder+1, but can't
    int nIntegrand = qfldElem.nDOF();
    int quadratureorder = 0;

    GalerkinWeightedIntegral<TopoD0, Node, BasisWeightedPDEClass::IntegrandType> integralPDEB(quadratureorder, nIntegrand);
    GalerkinWeightedIntegral<TopoD0, Node, BasisWeightedLOClass::IntegrandType> integralLOB(quadratureorder, nIntegrand);
    ElementIntegral<TopoD0, Node, FieldWeightedClass::IntegrandType> integralW(quadratureorder);

    BasisWeightedPDEClass::IntegrandType rsdElemPDEB[nDOF] = {0,0};
    BasisWeightedLOClass::IntegrandType rsdElemLOB[nDOF] = {0,0};
    FieldWeightedClass::IntegrandType rsdElemW = 0;

    // cell integration for canonical element
    integralPDEB( fcnPDEB, xnode, rsdElemPDEB, nIntegrand );
    integralLOB( fcnLOB, xnode, rsdElemLOB, nIntegrand );

    for ( int i = 0; i < wfldElem.nDOF(); i++ )
    {
      // set just one of the elements to one
      wfldElem.DOF(i) = 1; sfldElem.DOF(i) = 1;

      // cell integration for canonical Element
      rsdElemW = 0;
      integralW( fcnW, xnode, rsdElemW );

      // test the two integrands are the same
      SANS_CHECK_CLOSE ( rsdElemW.PDE, rsdElemPDEB[i], small_tol, close_tol );

      Real tmp = 0;
      for (int D = 0; D < PhysD1::D; D++ )
        tmp += rsdElemLOB[i][D];

      SANS_CHECK_CLOSE( rsdElemW.Lift, tmp, small_tol, close_tol );

      // reset to 0
      wfldElem.DOF(i) = 0; sfldElem.DOF(i) = 0;
    }
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Triangle_LinearRobin_X1Q1R1_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD2::D,ArrayQ> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_sansLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementRFieldClass;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO<Real,Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> BasisWeightedLOClass;
  // PDE

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // BC
  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution (left)
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  // lifting operators
  ElementRFieldClass rfldElem(order, BasisFunctionCategory_Hierarchical);

  rfldElem.DOF(0) = { 2, -3};  rfldElem.DOF(1) = { 7,  8};  rfldElem.DOF(2) = {-1,  7};

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand functor
  IntegrandClass fcnint( pde, bc, {0}, disc );

  BasisWeightedPDEClass fcnPDE = fcnint.integrand_PDE( xedge, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, rfldElem );
  BasisWeightedLOClass fcnLO = fcnint.integrand_LO( xedge, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, rfldElem );

  BOOST_CHECK_EQUAL( 3, fcnPDE.nDOF() );
  BOOST_CHECK( fcnPDE.needsEvaluation() == true );
  BOOST_CHECK_EQUAL( 3, fcnLO.nDOF() );
  BOOST_CHECK( fcnLO.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;
  Real integrandPDETrue[3];
  Real integrandLiftxTrue[3];
  Real integrandLiftyTrue[3];
  BasisWeightedPDEClass::IntegrandType integrandPDE[3];
  BasisWeightedLOClass::IntegrandType integrandLO[3];

  sRef = 0;
  fcnPDE( sRef, integrandPDE, 3 );
  fcnLO( sRef, integrandLO, 3 );

  //PDE residual integrands (left): (advective) + (viscous) + (BC) + (lifting-operator)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + ( (1059*(73989+125*sqrt(2)))/406250. ) + ( 0 );  // Basis function 1
  integrandPDETrue[1] = ( 39/(10.*sqrt(2)) ) + ( (-627*sqrt(2))/125. ) +
                        ( (-73451316+27621625*sqrt(2))/406250. ) + ( -23409/(125.*sqrt(2)) );  // Basis function 2
  integrandPDETrue[2] = ( 0 ) + ( 0 ) + ( (-3*(73989+125*sqrt(2)))/3125. ) + ( 0 );  // Basis function 3

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );

  //LO residual integrands (left): (lifting-operator)
  integrandLiftxTrue[0] = ( 0 );  // Basis function 1
  integrandLiftyTrue[0] = ( 0 );  // Basis function 1
  integrandLiftxTrue[1] = ( 73989/1625.+(1/13.)*(sqrt(2)) );  // Basis function 2
  integrandLiftyTrue[1] = ( 73989/1625.+(1/13.)*(sqrt(2)) );  // Basis function 2
  integrandLiftxTrue[2] = ( 0 );  // Basis function 3
  integrandLiftyTrue[2] = ( 0 );  // Basis function 3

  SANS_CHECK_CLOSE( integrandLiftxTrue[0], integrandLO[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyTrue[0], integrandLO[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[1], integrandLO[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyTrue[1], integrandLO[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[2], integrandLO[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyTrue[2], integrandLO[2][1], small_tol, close_tol );


  sRef = 1;
  fcnPDE( sRef, integrandPDE, 3 );
  fcnLO( sRef, integrandLO, 3 );

  //PDE residual integrands (left): (advective) + (viscous) + (BC) + (lifting-operator)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + ( (3177/406250.)*((pow(2,-1/2.))*(250+(7437)*(sqrt(2)))) ) + ( 0 );  // Basis function 1
  integrandPDETrue[1] = ( 0 ) + ( 0 ) + ( (-2007/406250.)*((pow(2,-1/2.))*(250+(7437)*(sqrt(2)))) ) + ( 0 );  // Basis function 2
  integrandPDETrue[2] = ( (13/5.)*(sqrt(2)) ) + ( (-627/125.)*(sqrt(2)) ) +
                        ( (3/81250.)*(-1044741+(539900)*(sqrt(2))) ) + ( (-6183/125.)*(pow(2,-1/2.)) );  // Basis function 3

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );

  //LO residual integrands (left): (lifting-operator)
  integrandLiftxTrue[0] = ( 0 );  // Basis function 1
  integrandLiftyTrue[0] = ( 0 );  // Basis function 1
  integrandLiftxTrue[1] = ( 0 );  // Basis function 2
  integrandLiftyTrue[1] = ( 0 );  // Basis function 2
  integrandLiftxTrue[2] = ( (3/1625.)*(7437+(125)*(sqrt(2))) );  // Basis function 3
  integrandLiftyTrue[2] = ( (3/1625.)*(7437+(125)*(sqrt(2))) );  // Basis function 3

  SANS_CHECK_CLOSE( integrandLiftxTrue[0], integrandLO[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyTrue[0], integrandLO[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[1], integrandLO[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyTrue[1], integrandLO[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[2], integrandLO[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyTrue[2], integrandLO[2][1], small_tol, close_tol );


  sRef = 0.5;
  fcnPDE( sRef, integrandPDE, 3 );
  fcnLO( sRef, integrandLO, 3 );

  //PDE residual integrands (left): (advective) + (viscous) + (BC) + (lifting-operator)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + ( (1059/8125.)*((pow(2,-1/2.))*(10+(963)*(sqrt(2)))) ) + ( 0 );  // Basis function 1
  integrandPDETrue[1] = ( (91/40.)*(pow(2,-1/2.)) ) + ( (-627/125.)*(pow(2,-1/2.)) ) +
                        ( (1/16250.)*(-1597719+(352810)*(sqrt(2))) ) + ( (-3699/125.)*(sqrt(2)) );  // Basis function 2
  integrandPDETrue[2] = ( (91/40.)*(pow(2,-1/2.)) ) + ( (-627/125.)*(pow(2,-1/2.)) ) +
                        ( (1/3250.)*(-212073+(71120)*(sqrt(2))) ) + ( (-3699/125.)*(sqrt(2)) );  // Basis function 3

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );

  //LO residual integrands (left): (lifting-operator)
  integrandLiftxTrue[0] = ( 0 );  // Basis function 1
  integrandLiftyTrue[0] = ( 0 );  // Basis function 1
  integrandLiftxTrue[1] = ( (1/65.)*(963+(5)*(sqrt(2))) );  // Basis function 2
  integrandLiftyTrue[1] = ( (1/65.)*(963+(5)*(sqrt(2))) );  // Basis function 2
  integrandLiftxTrue[2] = ( (1/65.)*(963+(5)*(sqrt(2))) );  // Basis function 3
  integrandLiftyTrue[2] = ( (1/65.)*(963+(5)*(sqrt(2))) );  // Basis function 3

  SANS_CHECK_CLOSE( integrandLiftxTrue[0], integrandLO[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyTrue[0], integrandLO[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[1], integrandLO[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyTrue[1], integrandLO[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[2], integrandLO[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftyTrue[2], integrandLO[2][1], small_tol, close_tol );


  // test the trace element integral of the functor
  int quadratureorder = 2;
  int nIntegrandL = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedPDEClass::IntegrandType> integralPDE(quadratureorder, nIntegrandL);
  GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedLOClass::IntegrandType> integralLO(quadratureorder, nIntegrandL);

  BasisWeightedPDEClass::IntegrandType rsdElemPDE[3] = {0,0,0};
  BasisWeightedLOClass::IntegrandType rsdElemLO[3] = {0,0,0};

  Real rsdPDETrue[3];
  Real rsdLiftxTrue[3];
  Real rsdLiftyTrue[3];

  // cell integration for canonical element
  integralPDE( fcnPDE, xedge, rsdElemPDE, nIntegrandL );
  integralLO( fcnLO, xedge, rsdElemLO, nIntegrandL );

  //PDE residuals (left): (advective) + (viscous) + (BC) + (lifting-operator)
  rsdPDETrue[0] = ( 0 ) + ( 0 ) + ( (1059/8125.)*(10+(963)*(sqrt(2))) ) + ( 0 );   // Basis function 1
  rsdPDETrue[1] = ( 13/6. ) + ( -627/125. ) +
                  ( (1/48750.)*((pow(2,-1/2.))*(-9925971+(2506070)*(sqrt(2)))) ) + ( -17667/250. );   // Basis function 2
  rsdPDETrue[2] = ( 143/60. ) + ( -627/125. ) +
                  ( (1/48750.)*((pow(2,-1/2.))*(-6022533+(1744390)*(sqrt(2)))) ) + ( -477/10. );   // Basis function 3

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdElemPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdElemPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[2], rsdElemPDE[2], small_tol, close_tol );

  //LO residuals (left): (lifting-operator)
  rsdLiftxTrue[0] = ( 0 ); // Basis function 1
  rsdLiftyTrue[0] = ( 0 ); // Basis function 1
  rsdLiftxTrue[1] = ( (1/9750.)*(1250+(170289)*(sqrt(2))) ); // Basis function 2
  rsdLiftyTrue[1] = ( (1/9750.)*(1250+(170289)*(sqrt(2))) ); // Basis function 2
  rsdLiftxTrue[2] = ( (1/9750.)*(1750+(118611)*(sqrt(2))) ); // Basis function 3
  rsdLiftyTrue[2] = ( (1/9750.)*(1750+(118611)*(sqrt(2))) ); // Basis function 3

  SANS_CHECK_CLOSE( rsdLiftxTrue[0], rsdElemLO[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftyTrue[0], rsdElemLO[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftxTrue[1], rsdElemLO[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftyTrue[1], rsdElemLO[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftxTrue[2], rsdElemLO[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftyTrue[2], rsdElemLO[2][1], small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_Triangle_LinearRobin_X1Q1R1W2S2_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD2::D,ArrayQ> VectorArrayQ;

  typedef BCNDConvertSpace<PhysD2, BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_sansLG> > BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementRFieldClass;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,Real,TopoD1,Line,TopoD2,Triangle, ElementXFieldCell> FieldWeightedClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.789;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // BC
  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution (left)
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  // lifting operators
  ElementRFieldClass rfldElem(order, BasisFunctionCategory_Hierarchical);

  rfldElem.DOF(0) = { 2, -3};  rfldElem.DOF(1) = { 7,  8};  rfldElem.DOF(2) = {-1,  7};

  // weight
  ElementQFieldCell wfldElem(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElem.order() );
  BOOST_CHECK_EQUAL( 6, wfldElem.nDOF() );

  // triangle solution (left)
  wfldElem.DOF(0) = -2;
  wfldElem.DOF(1) =  4;
  wfldElem.DOF(2) =  3;
  wfldElem.DOF(3) =  2;
  wfldElem.DOF(4) =  4;
  wfldElem.DOF(5) = -1;

  // lifting operators
  ElementRFieldClass sfldElem(order+1, BasisFunctionCategory_Hierarchical);

  sfldElem.DOF(0) = { 1,  5};  sfldElem.DOF(1) = { 3,  6};  sfldElem.DOF(2) = {-1,  7};
  sfldElem.DOF(3) = { 3,  1};  sfldElem.DOF(4) = { 2,  2};  sfldElem.DOF(5) = { 4,  3};

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand functor
  IntegrandClass fcnint( pde, bc, {0}, disc );

  FieldWeightedClass fcn = fcnint.integrand( xedge,
                                       CanonicalTraceToCell(0, 1),
                                       xfldElem,
                                       qfldElem, rfldElem,
                                       wfldElem, sfldElem);

  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );
  BOOST_CHECK( fcnint.needsFieldTrace() == false );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;

  Real integrandPDETrue,integrandLiftTrue;
  FieldWeightedClass::IntegrandType integrand;

  // Test at sRef={0}, {s,t}={1, 0}
  sRef = {0};
  fcn( sRef, integrand );

  //PDE residual integrands (left): (advective) + (viscous) + (BC) + (lifting-operator)
  integrandPDETrue = ( (39/5.)*(sqrt(2)) ) + ( (-2626/125.)*(sqrt(2)) )
      + ( (1/406250.)*(-1216391109+(114871625)*(sqrt(2))) ) + ( (-49296/125.)*(sqrt(2)) );  // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //LO residual integrands (left): (lifting-operator)
  integrandLiftTrue = ( 53919/125.+(9/13.)*(sqrt(2)) );  // Weight function

  SANS_CHECK_CLOSE( integrandLiftTrue, integrand.Lift, small_tol, close_tol );

  // Test at sRef={1}, {s,t}={0, 1}
  sRef = {1};
  fcn( sRef, integrand );

  //PDE residual integrands (left): (advective) + (viscous) + (BC) + (lifting-operator)
  integrandPDETrue = ( (39/5.)*(sqrt(2)) ) + ( (-3939/125.)*(pow(2,-1/2.)) )
      + ( (3/406250.)*(39086588+(8896625)*(sqrt(2))) ) + ( (-9009/125.)*(sqrt(2)) );  // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //LO residual integrands (left): (lifting-operator)
  integrandLiftTrue = ( 10134/125.+(18/13.)*(sqrt(2)) );  // Weight function

  SANS_CHECK_CLOSE( integrandLiftTrue, integrand.Lift, small_tol, close_tol );

  // Test at sRef={1/2}, {s,t}={1/2, 1/2}
  sRef = {1/2.};
  fcn( sRef, integrand );

  //PDE residual integrands (left): (advective) + (viscous) + (BC) + (lifting-operator)
  integrandPDETrue = ( (1001/40.)*(pow(2,-1/2.)) ) + ( (-14443/250.)*(pow(2,-1/2.)) )
      + ( (1/81250.)*(-57805782+(20301475)*(sqrt(2))) ) + ( (-168597/250.)*(pow(2,-1/2.)) );  // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //LO residual integrands (left): (lifting-operator)
  integrandLiftTrue = ( 8832/25.+(23/13.)*(sqrt(2)) );  // Weight function

  SANS_CHECK_CLOSE( integrandLiftTrue, integrand.Lift, small_tol, close_tol );

  // Test at sRef={1/5}, {s,t}={4/5, 1/5}
  sRef = {1/5.};
  fcn( sRef, integrand );

  //PDE residual integrands (left): (advective) + (viscous) + (BC) + (lifting-operator)
  integrandPDETrue = ( (6604/625.)*(sqrt(2)) ) + ( (-166751/3125.)*(pow(2,-1/2.)) )
      + ( (1/2031250.)*(-3949844856+(624924295)*(sqrt(2))) ) + ( (-6641973/15625.)*(sqrt(2)) );  // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //LO residual integrands (left): (lifting-operator)
  integrandLiftTrue = ( (274/203125.)*(333489+(875)*(sqrt(2))) );  // Weight function

  SANS_CHECK_CLOSE( integrandLiftTrue, integrand.Lift, small_tol, close_tol );

  // test the trace element integral of the functor
  int quadratureorder = 3;
  ElementIntegral<TopoD1, Line, FieldWeightedClass::IntegrandType> integral(quadratureorder);

  FieldWeightedClass::IntegrandType rsdElem=0;

  Real rsdPDETrue,rsdLiftTrue;

  // cell integration for canonical element
  integral( fcn, xedge, rsdElem );

  //PDE residuals (left): (advective) + (viscous) + (BC) + (lifting-operator)
  rsdPDETrue = ( 1313/60. ) + ( -38077/750. )
      + ( (1/243750.)*((pow(2,-1/2.))*(-451049397+(109518200)*(sqrt(2)))) ) + ( -75634/125. );   // Weight function

  SANS_CHECK_CLOSE( rsdPDETrue, rsdElem.PDE, small_tol, close_tol );

  //LO residuals (left): (lifting-operator)
  rsdLiftTrue = ( (1/4875.)*((pow(2,-1/2.))*(3129009+(14875)*(sqrt(2)))) ); // Weight function

  SANS_CHECK_CLOSE( rsdLiftTrue, rsdElem.Lift, small_tol, close_tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_2D_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD2::D,ArrayQ> VectorArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_sansLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD2,Triangle> ElementRFieldClass;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO<Real,Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> BasisWeightedLOClass;
  typedef IntegrandClass::FieldWeighted<Real,Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> FieldWeightedClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Real a = 2.3;
  Source2D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // BC
  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  for ( int qorder = 2; qorder < 3; qorder++ )
  {
    //solution
    ElementQFieldCell qfldElem(qorder, BasisFunctionCategory_Hierarchical );
    ElementQFieldCell wfldElem(qorder, BasisFunctionCategory_Hierarchical );

    BOOST_CHECK_EQUAL( qorder,   qfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   wfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, wfldElem.nDOF() );

    BOOST_CHECK_EQUAL( qfldElem.nDOF(), wfldElem.nDOF() );
    // line solution
    for ( int dof = 0; dof < qfldElem.nDOF(); dof++ )
    {
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);
      wfldElem.DOF(dof) = 0;
    }

    // lifting operators
    ElementRFieldClass rfldElem(qorder, BasisFunctionCategory_Hierarchical);
    ElementRFieldClass sfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder,   rfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, rfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   sfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, sfldElem.nDOF() );

    BOOST_CHECK_EQUAL( rfldElem.nDOF(), sfldElem.nDOF() );
    // lifting operator
    for ( int dof = 0; dof < rfldElem.nDOF(); dof++ )
    {
      rfldElem.DOF(dof) = (dof+1)*pow(-1,dof);
      sfldElem.DOF(dof) = 0;
    }
    // BR2 discretization
    Real viscousEtaParameter = 6;
    DiscretizationDGBR2 disc(0, viscousEtaParameter);

    // integrand functor
    IntegrandClass fcnint( pde, bc, {0}, disc );

    BasisWeightedPDEClass fcnPDEB = fcnint.integrand_PDE( xedge,
                                                      CanonicalTraceToCell(0, 1),
                                                      xfldElem,
                                                      qfldElem, rfldElem );
    BasisWeightedLOClass fcnLOB = fcnint.integrand_LO( xedge,
                                                      CanonicalTraceToCell(0, 1),
                                                      xfldElem,
                                                      qfldElem, rfldElem );
    FieldWeightedClass fcnW = fcnint.integrand( xedge,
                                                CanonicalTraceToCell(0, 1),
                                                xfldElem,
                                                qfldElem, rfldElem,
                                                wfldElem, sfldElem);

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;
    const int nDOF = 6; // want to be qorder+1, but can't
    int nIntegrand = qfldElem.nDOF();
    int quadratureorder = 4;

    GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedPDEClass::IntegrandType> integralPDEB(quadratureorder, nIntegrand);
    GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedLOClass::IntegrandType> integralLOB(quadratureorder, nIntegrand);
    ElementIntegral<TopoD1, Line, FieldWeightedClass::IntegrandType> integralW(quadratureorder);

    BasisWeightedPDEClass::IntegrandType rsdElemPDEB[nDOF] = {0,0};
    BasisWeightedLOClass::IntegrandType rsdElemLOB[nDOF] = {0,0};
    FieldWeightedClass::IntegrandType rsdElemW = 0;

    // cell integration for canonical element
    integralPDEB( fcnPDEB, xedge, rsdElemPDEB, nIntegrand );
    integralLOB( fcnLOB, xedge, rsdElemLOB, nIntegrand );

    for ( int i = 0; i < wfldElem.nDOF(); i++ )
    {
      // set just one of the elements to one
      wfldElem.DOF(i) = 1; sfldElem.DOF(i) = 1;

      // cell integration for canonical Element
      rsdElemW = 0;
      integralW( fcnW, xedge, rsdElemW );

      // test the two integrands are the same
      SANS_CHECK_CLOSE ( rsdElemW.PDE, rsdElemPDEB[i], small_tol, close_tol );

      Real tmp = 0;
      for (int D = 0; D < PhysD2::D; D++ )
        tmp += rsdElemLOB[i][D];

      SANS_CHECK_CLOSE( rsdElemW.Lift, tmp, small_tol, close_tol );


      // reset to 0
      wfldElem.DOF(i) = 0; sfldElem.DOF(i) = 0;
    }
  }

}


#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Quad_X1Q1R1_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD2::D,ArrayQ> VectorArrayQ;

  typedef BCNone<PhysD2,AdvectionDiffusionTraits<PhysD2>::N> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Quad> BasisWeightedClass;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Quad> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD2,Quad> ElementRFieldClass;

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC
  BCClass bc;

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 0 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 4, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 1;  y3 = 1;
  x4 = 0;  y4 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};
  xfldElem.DOF(3) = {x4, y4};

  xedge.DOF(0) = {x1, y1};
  xedge.DOF(1) = {x2, y2};


  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 4, qfldElem.nDOF() );

  // quad solution (left)
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;
  qfldElem.DOF(3) = 6;

  // lifting operators
  ElementRFieldClass rfldElem(order, BasisFunctionCategory_Hierarchical);

  rfldElem.DOF(0) = { 2, -3};
  rfldElem.DOF(1) = { 7,  8};
  rfldElem.DOF(2) = {-1,  7};
  rfldElem.DOF(3) = { 5, -9};

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand functor
  IntegrandClass fcnint( pde, bc, {0}, disc );

  BasisWeightedClass fcn = fcnint.integrand( xedge, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, rfldElem );

  BOOST_CHECK_EQUAL( 4, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandPDETrue[4];
  Real integrandLOxTrue[4];
  Real integrandLOyTrue[4];
  BasisWeightedClass::IntegrandType integrandPDE[4];

  sRef = 0;
  fcn( sRef, integrandPDE, 4 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (-1./5.) + (6141./1000.);
  integrandPDETrue[1] = (0)      + (0)          ;
  integrandPDETrue[2] = (0)      + (0)          ;
  integrandPDETrue[3] = (0)      + (0)          ;

  // Lifting operator residuals
  integrandLOxTrue[0] = 0; integrandLOyTrue[0] = 0;
  integrandLOxTrue[1] = 0; integrandLOyTrue[1] = 0;
  integrandLOxTrue[2] = 0; integrandLOyTrue[2] = 0;
  integrandLOxTrue[3] = 0; integrandLOyTrue[3] = 0;

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDEPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDEPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDEPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDEPDE[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLOxTrue[0], integrandPDELO[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOxTrue[1], integrandPDELO[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOxTrue[2], integrandPDELO[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOxTrue[3], integrandPDELO[3][0], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLOyTrue[0], integrandPDELO[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOyTrue[1], integrandPDELO[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOyTrue[2], integrandPDELO[2][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOyTrue[3], integrandPDELO[3][1], small_tol, close_tol );


  sRef = 1;
  fcn( sRef, integrandPDE, 4 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (0)      + (0)          ;
  integrandPDETrue[1] = (-3./5.) + (2113./1000.);
  integrandPDETrue[2] = (0)      + (0)          ;
  integrandPDETrue[3] = (0)      + (0)          ;

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDEPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDEPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDEPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDEPDE[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLOxTrue[0], integrandPDELO[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOxTrue[1], integrandPDELO[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOxTrue[2], integrandPDELO[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOxTrue[3], integrandPDELO[3][0], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLOyTrue[0], integrandPDELO[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOyTrue[1], integrandPDELO[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOyTrue[2], integrandPDELO[2][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOyTrue[3], integrandPDELO[3][1], small_tol, close_tol );


  sRef = 1./2.;
  fcn( sRef, integrandPDE, 4 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (-1./5.) + (4127./2000.);
  integrandPDETrue[1] = (-1./5.) + (4127./2000.);
  integrandPDETrue[2] = (0)      + (0)          ;
  integrandPDETrue[3] = (0)      + (0)          ;

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDEPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDEPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDEPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDEPDE[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLOxTrue[0], integrandPDELO[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOxTrue[1], integrandPDELO[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOxTrue[2], integrandPDELO[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOxTrue[3], integrandPDELO[3][0], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLOyTrue[0], integrandPDELO[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOyTrue[1], integrandPDELO[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOyTrue[2], integrandPDELO[2][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOyTrue[3], integrandPDELO[3][1], small_tol, close_tol );

  // test the trace element integral of the functor
  int quadratureorder = 2;
  int nIntegrandL = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedClass::IntegrandType> integral(quadratureorder, nIntegrandL);

  BasisWeightedClass::IntegrandType rsdPDEElemL[4] = {0,0,0,0};

  Real rsdPDE[4];
  Real rsdLOx[4];
  Real rsdLOy[4];

  // cell integration for canonical element
  integral( fcn, xedge, rsdPDEElemL, nIntegrandL );

  // PDE residuals (left): (advective) + (viscous)
  rsdPDE[0] = (-1./6.)  + (2879./1200.) ;
  rsdPDE[1] = (-7./30.) + (10367./6000.);
  rsdPDE[2] = (0)       + (0)           ;
  rsdPDE[3] = (0)       + (0)           ;

  // LO residuals
  rsdLOx[0] = 0; rsdLOy[0] = 0;
  rsdLOx[1] = 0; rsdLOy[1] = 0;
  rsdLOx[2] = 0; rsdLOy[2] = 0;
  rsdLOx[3] = 0; rsdLOy[3] = 0;

  SANS_CHECK_CLOSE( rsdPDE[0], rsdPDEElemLPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDE[1], rsdPDEElemLPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDE[2], rsdPDEElemLPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDE[3], rsdPDEElemLPDE[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdLOx[0], rsdPDEElemLLO[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLOx[1], rsdPDEElemLLO[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLOx[2], rsdPDEElemLLO[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLOx[3], rsdPDEElemLLO[3][0], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdLOy[0], rsdPDEElemLLO[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLOy[1], rsdPDEElemLLO[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLOy[2], rsdPDEElemLLO[2][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLOy[3], rsdPDEElemLLO[3][1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Tet_X1Q1R1LG1_test )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEAdvectionDiffusion3D::ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD3::D,ArrayQ> VectorArrayQ;

  typedef BCNone<PhysD3,AdvectionDiffusionTraits<PhysD3>::N> BCClassRaw;
  typedef BCNDConvertSpace<PhysD3, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD2,Triangle,TopoD3,Tet> BasisWeightedClass;

  typedef ElementXField<PhysD3,TopoD2,Triangle> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD3,Tet> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD3,Tet> ElementRFieldClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Real kxz = 0.643;
  Real kyz = 0.765;
  Real kzz = 1.234;
  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                                  kxy, kyy, kyz,
                                  kxz, kyz, kzz );

  Source3D_None source;

  PDEClass pde( adv, visc, source );

  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC
  BCClass bc;

  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 0 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 4, xfldElem.nDOF() );

  ElementXFieldTrace xtri(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xtri.order() );
  BOOST_CHECK_EQUAL( 3, xtri.nDOF() );

  // tetrahdral grid element
  Real x1, x2, x3, x4, y1, y2, y3, y4, z1, z2, z3, z4;

  x1 = 0;  y1 = 0;  z1 = 0;
  x2 = 1;  y2 = 0;  z2 = 0;
  x3 = 0;  y3 = 1;  z3 = 0;
  x4 = 0;  y4 = 0;  z4 = 1;

  xfldElem.DOF(0) = {x1, y1, z1};
  xfldElem.DOF(1) = {x2, y2, z2};
  xfldElem.DOF(2) = {x3, y3, z3};
  xfldElem.DOF(3) = {x4, y4, z4};

  xtri.DOF(0) = {x2, y2, z2};
  xtri.DOF(1) = {x3, y3, z3};
  xtri.DOF(2) = {x4, y4, z4};


  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 4, qfldElem.nDOF() );

  // tet solution (left)
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;
  qfldElem.DOF(3) = 6;

  // lifting operators
  ElementRFieldClass rfldElem(order, BasisFunctionCategory_Hierarchical);

  rfldElem.DOF(0) = { 2, -3,  6};
  rfldElem.DOF(1) = { 7,  8, -1};
  rfldElem.DOF(2) = {-1,  7,  4};
  rfldElem.DOF(3) = { 5, -9,  3};

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand functor
  IntegrandClass fcnint( pde, bc, {0}, disc );

  BasisWeightedClass fcn = fcnint.integrand( xtri, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, rfldElem );

  BOOST_CHECK_EQUAL( 4, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandPDETrue[4];
  Real integrandLOxTrue[4];
  Real integrandLOyTrue[4];
  Real integrandLOzTrue[4];
  BasisWeightedClass::IntegrandType integrandPDE[4];

  sRef = {0, 0};
  fcn( sRef, integrandPDE, 4 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (0)             + (0)                   ;
  integrandPDETrue[1] = (9.*sqrt(3)/5.) + (-8941.*sqrt(3)/1000.);
  integrandPDETrue[2] = (0)             + (0)                   ;
  integrandPDETrue[3] = (0)             + (0)                   ;

  // Lifting operator residuals
  integrandLOxTrue[0] = 0; integrandLOyTrue[0] = 0; integrandLOzTrue[0] = 0;
  integrandLOxTrue[1] = 0; integrandLOyTrue[1] = 0; integrandLOzTrue[1] = 0;
  integrandLOxTrue[2] = 0; integrandLOyTrue[2] = 0; integrandLOzTrue[2] = 0;
  integrandLOxTrue[3] = 0; integrandLOyTrue[3] = 0; integrandLOzTrue[3] = 0;

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDEPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDEPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDEPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDEPDE[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLOxTrue[0], integrandPDELO[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOxTrue[1], integrandPDELO[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOxTrue[2], integrandPDELO[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOxTrue[3], integrandPDELO[3][0], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLOyTrue[0], integrandPDELO[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOyTrue[1], integrandPDELO[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOyTrue[2], integrandPDELO[2][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOyTrue[3], integrandPDELO[3][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLOzTrue[0], integrandPDELO[0][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOzTrue[1], integrandPDELO[1][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOzTrue[2], integrandPDELO[2][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOzTrue[3], integrandPDELO[3][2], small_tol, close_tol );


  sRef = {1,0};
  fcn( sRef, integrandPDE, 4 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (0)              + (0)                   ;
  integrandPDETrue[1] = (0)              + (0)                   ;
  integrandPDETrue[2] = (12.*sqrt(3)/5.) + (-8941.*sqrt(3)/1000.);
  integrandPDETrue[3] = (0)              + (0)                   ;

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDEPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDEPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDEPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDEPDE[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLOxTrue[0], integrandPDELO[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOxTrue[1], integrandPDELO[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOxTrue[2], integrandPDELO[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOxTrue[3], integrandPDELO[3][0], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLOyTrue[0], integrandPDELO[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOyTrue[1], integrandPDELO[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOyTrue[2], integrandPDELO[2][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOyTrue[3], integrandPDELO[3][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLOzTrue[0], integrandPDELO[0][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOzTrue[1], integrandPDELO[1][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOzTrue[2], integrandPDELO[2][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOzTrue[3], integrandPDELO[3][2], small_tol, close_tol );


  sRef = {0,1};
  fcn( sRef, integrandPDE, 4 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (0)               + (0)                   ;
  integrandPDETrue[1] = (0)               + (0)                   ;
  integrandPDETrue[2] = (0)               + (0)                   ;
  integrandPDETrue[3] = (18.*sqrt(3)/5.)  + (-8941.*sqrt(3)/1000.);

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDEPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDEPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDEPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDEPDE[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLOxTrue[0], integrandPDELO[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOxTrue[1], integrandPDELO[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOxTrue[2], integrandPDELO[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOxTrue[3], integrandPDELO[3][0], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLOyTrue[0], integrandPDELO[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOyTrue[1], integrandPDELO[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOyTrue[2], integrandPDELO[2][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOyTrue[3], integrandPDELO[3][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLOzTrue[0], integrandPDELO[0][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOzTrue[1], integrandPDELO[1][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOzTrue[2], integrandPDELO[2][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOzTrue[3], integrandPDELO[3][2], small_tol, close_tol );


  sRef = {1./3., 1./3.};
  fcn( sRef, integrandPDE, 4 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (0)                + (0)                     ;
  integrandPDETrue[1] = (13./(5.*sqrt(3))) + (-8941./(1000.*sqrt(3)));
  integrandPDETrue[2] = (13./(5.*sqrt(3))) + (-8941./(1000.*sqrt(3)));
  integrandPDETrue[3] = (13./(5.*sqrt(3))) + (-8941./(1000.*sqrt(3)));

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDEPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDEPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDEPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDEPDE[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLOxTrue[0], integrandPDELO[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOxTrue[1], integrandPDELO[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOxTrue[2], integrandPDELO[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOxTrue[3], integrandPDELO[3][0], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLOyTrue[0], integrandPDELO[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOyTrue[1], integrandPDELO[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOyTrue[2], integrandPDELO[2][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOyTrue[3], integrandPDELO[3][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLOzTrue[0], integrandPDELO[0][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOzTrue[1], integrandPDELO[1][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOzTrue[2], integrandPDELO[2][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLOzTrue[3], integrandPDELO[3][2], small_tol, close_tol );


  // test the trace element integral of the functor
  int quadratureorder = 2;
  int nIntegrandL = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Triangle, BasisWeightedClass::IntegrandType> integral(quadratureorder, nIntegrandL);

  BasisWeightedClass::IntegrandType rsdPDEElemL[4] = {0,0,0,0};

  Real rsdPDE[4];
  Real rsdLOx[4];
  Real rsdLOy[4];
  Real rsdLOz[4];

  // cell integration for canonical element
  integral( fcn, xtri, rsdPDEElemL, nIntegrandL );

  // PDE residuals (left): (advective) + (viscous)
  rsdPDE[0] = (0)       + (0)           ;
  rsdPDE[1] = (6./5.)   + (-8941./2000.);
  rsdPDE[2] = (51./40.) + (-8941./2000.);
  rsdPDE[3] = (57./40.) + (-8941./2000.);

  // LO residuals
  rsdLOx[0] = 0; rsdLOy[0] = 0; rsdLOz[0] = 0;
  rsdLOx[1] = 0; rsdLOy[1] = 0; rsdLOz[1] = 0;
  rsdLOx[2] = 0; rsdLOy[2] = 0; rsdLOz[2] = 0;
  rsdLOx[3] = 0; rsdLOy[3] = 0; rsdLOz[3] = 0;

  SANS_CHECK_CLOSE( rsdPDE[0], rsdPDEElemLPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDE[1], rsdPDEElemLPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDE[2], rsdPDEElemLPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDE[3], rsdPDEElemLPDE[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdLOx[0], rsdPDEElemLLO[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLOx[1], rsdPDEElemLLO[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLOx[2], rsdPDEElemLLO[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLOx[3], rsdPDEElemLLO[3][0], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdLOy[0], rsdPDEElemLLO[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLOy[1], rsdPDEElemLLO[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLOy[2], rsdPDEElemLLO[2][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLOy[3], rsdPDEElemLLO[3][1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdLOz[0], rsdPDEElemLLO[0][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLOz[1], rsdPDEElemLLO[1][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLOz[2], rsdPDEElemLLO[2][2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLOz[3], rsdPDEElemLLO[3][2], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Hex_X1Q1R1_test )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEAdvectionDiffusion3D::ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD3::D,ArrayQ> VectorArrayQ;

  typedef BCNone<PhysD3,AdvectionDiffusionTraits<PhysD3>::N> BCClassRaw;
  typedef BCNDConvertSpace<PhysD3, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD2,Quad,TopoD3,Hex> BasisWeightedClass;

  typedef ElementXField<PhysD3,TopoD2,Quad> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD3,TopoD3,Hex> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD3,Hex> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD3,Hex> ElementRFieldClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Real kxz = 0.643;
  Real kyz = 0.765;
  Real kzz = 1.234;
  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                                  kxy, kyy, kyz,
                                  kxz, kyz, kzz );

  Source3D_None source;

  PDEClass pde( adv, visc, source );

  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC
  BCClass bc;

  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 0 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 8, xfldElem.nDOF() );

  ElementXFieldTrace xquad(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xquad.order() );
  BOOST_CHECK_EQUAL( 4, xquad.nDOF() );

  // hexahedral grid element
  xfldElem.DOF(0) = {0, 0, 0};
  xfldElem.DOF(1) = {1, 0, 0};
  xfldElem.DOF(2) = {1, 1, 0};
  xfldElem.DOF(3) = {0, 1, 0};

  xfldElem.DOF(4) = {0, 0, 1};
  xfldElem.DOF(5) = {1, 0, 1};
  xfldElem.DOF(6) = {1, 1, 1};
  xfldElem.DOF(7) = {0, 1, 1};

  xquad.DOF(0) = {0, 0, 0};
  xquad.DOF(1) = {0, 1, 0};
  xquad.DOF(2) = {1, 1, 0};
  xquad.DOF(3) = {1, 0, 0};


  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 8, qfldElem.nDOF() );

  // hex solution (left)
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;
  qfldElem.DOF(3) = 6;

  qfldElem.DOF(4) = 8;
  qfldElem.DOF(5) = 2;
  qfldElem.DOF(6) = 9;
  qfldElem.DOF(7) = 7;

  // lifting operators
  ElementRFieldClass rfldElem(order, BasisFunctionCategory_Hierarchical);

  rfldElem.DOF(0) = { 2, -3,  6};
  rfldElem.DOF(1) = { 7,  8, -1};
  rfldElem.DOF(2) = {-1,  7,  4};
  rfldElem.DOF(3) = { 5, -9,  3};
  rfldElem.DOF(4) = {-6,  2,  1};
  rfldElem.DOF(5) = { 7,  0, -5};
  rfldElem.DOF(6) = {-3, -8,  2};
  rfldElem.DOF(7) = { 9,  4,  0};

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand functor
  IntegrandClass fcnint( pde, bc, {0}, disc );

  BasisWeightedClass fcn = fcnint.integrand( xquad, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, rfldElem );

  BOOST_CHECK_EQUAL( 8, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 2e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandPDETrue[8];
  Real integrandLOxTrue[8];
  Real integrandLOyTrue[8];
  Real integrandLOzTrue[8];
  BasisWeightedClass::IntegrandType integrandPDE[8];

  sRef = {0, 0};
  fcn( sRef, integrandPDE, 8 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (-1./2.) + (13749./1000.);
  integrandPDETrue[1] = (0)      + (0)           ;
  integrandPDETrue[2] = (0)      + (0)           ;
  integrandPDETrue[3] = (0)      + (0)           ;

  integrandPDETrue[4] = (0)      + (0)           ;
  integrandPDETrue[5] = (0)      + (0)           ;
  integrandPDETrue[6] = (0)      + (0)           ;
  integrandPDETrue[7] = (0)      + (0)           ;

  // Lifting operator residuals
  integrandLOxTrue[0] = 0; integrandLOyTrue[0] = 0; integrandLOzTrue[0] = 0;
  integrandLOxTrue[1] = 0; integrandLOyTrue[1] = 0; integrandLOzTrue[1] = 0;
  integrandLOxTrue[2] = 0; integrandLOyTrue[2] = 0; integrandLOzTrue[2] = 0;
  integrandLOxTrue[3] = 0; integrandLOyTrue[3] = 0; integrandLOzTrue[3] = 0;
  integrandLOxTrue[4] = 0; integrandLOyTrue[4] = 0; integrandLOzTrue[4] = 0;
  integrandLOxTrue[5] = 0; integrandLOyTrue[5] = 0; integrandLOzTrue[5] = 0;
  integrandLOxTrue[6] = 0; integrandLOyTrue[6] = 0; integrandLOzTrue[6] = 0;
  integrandLOxTrue[7] = 0; integrandLOyTrue[7] = 0; integrandLOzTrue[7] = 0;

  for (int k = 0; k < 8; k++)
  {
    SANS_CHECK_CLOSE( integrandPDETrue[k], integrandPDEPDE[k], small_tol, close_tol );

    SANS_CHECK_CLOSE( integrandLOxTrue[k], integrandPDELO[k][0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandLOyTrue[k], integrandPDELO[k][1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandLOzTrue[k], integrandPDELO[k][2], small_tol, close_tol );
  }


  sRef = {1, 0};
  fcn( sRef, integrandPDE, 8 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (0)  + (0)          ;
  integrandPDETrue[1] = (0)  + (0)          ;
  integrandPDETrue[2] = (0)  + (0)          ;
  integrandPDETrue[3] = (-3) + (3773./1000.);

  integrandPDETrue[4] = (0)  + (0);
  integrandPDETrue[5] = (0)  + (0);
  integrandPDETrue[6] = (0)  + (0);
  integrandPDETrue[7] = (0)  + (0);

  for (int k = 0; k < 8; k++)
  {
    SANS_CHECK_CLOSE( integrandPDETrue[k], integrandPDEPDE[k], small_tol, close_tol );

    SANS_CHECK_CLOSE( integrandLOxTrue[k], integrandPDELO[k][0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandLOyTrue[k], integrandPDELO[k][1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandLOzTrue[k], integrandPDELO[k][2], small_tol, close_tol );
  }


  sRef = {1, 1};
  fcn( sRef, integrandPDE, 8 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (0)  + (0)          ;
  integrandPDETrue[1] = (0)  + (0)          ;
  integrandPDETrue[2] = (-2) + (5649./1000.);
  integrandPDETrue[3] = (0)  + (0)          ;

  integrandPDETrue[4] = (0)  + (0)          ;
  integrandPDETrue[5] = (0)  + (0)          ;
  integrandPDETrue[6] = (0)  + (0)          ;
  integrandPDETrue[7] = (0)  + (0)          ;

  for (int k = 0; k < 8; k++)
  {
    SANS_CHECK_CLOSE( integrandPDETrue[k], integrandPDEPDE[k], small_tol, close_tol );

    SANS_CHECK_CLOSE( integrandLOxTrue[k], integrandPDELO[k][0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandLOyTrue[k], integrandPDELO[k][1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandLOzTrue[k], integrandPDELO[k][2], small_tol, close_tol );
  }


  sRef = {0, 1};
  fcn( sRef, integrandPDE, 8 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (0)      + (0)         ;
  integrandPDETrue[1] = (-3./2.) + (817./1000.);
  integrandPDETrue[2] = (0)      + (0)         ;
  integrandPDETrue[3] = (0)      + (0)         ;

  integrandPDETrue[4] = (0)  + (0);
  integrandPDETrue[5] = (0)  + (0);
  integrandPDETrue[6] = (0)  + (0);
  integrandPDETrue[7] = (0)  + (0);

  for (int k = 0; k < 8; k++)
  {
    SANS_CHECK_CLOSE( integrandPDETrue[k], integrandPDEPDE[k], small_tol, close_tol );

    SANS_CHECK_CLOSE( integrandLOxTrue[k], integrandPDELO[k][0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandLOyTrue[k], integrandPDELO[k][1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandLOzTrue[k], integrandPDELO[k][2], small_tol, close_tol );
  }


  sRef = {1./2., 1./2.};
  fcn( sRef, integrandPDE, 8 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (-7./16.) + (5997./4000.);
  integrandPDETrue[1] = (-7./16.) + (5997./4000.);
  integrandPDETrue[2] = (-7./16.) + (5997./4000.);
  integrandPDETrue[3] = (-7./16.) + (5997./4000.);

  integrandPDETrue[4] = (0)  + (0);
  integrandPDETrue[5] = (0)  + (0);
  integrandPDETrue[6] = (0)  + (0);
  integrandPDETrue[7] = (0)  + (0);

  for (int k = 0; k < 8; k++)
  {
    SANS_CHECK_CLOSE( integrandPDETrue[k], integrandPDEPDE[k], small_tol, close_tol );

    SANS_CHECK_CLOSE( integrandLOxTrue[k], integrandPDELO[k][0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandLOyTrue[k], integrandPDELO[k][1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandLOzTrue[k], integrandPDELO[k][2], small_tol, close_tol );
  }


  // test the trace element integral of the functor
  int quadratureorder = 2;
  int nIntegrandL = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Quad, BasisWeightedClass::IntegrandType> integral(quadratureorder, nIntegrandL);

  BasisWeightedClass::IntegrandType rsdPDEElemL[8] = {0,0,0,0, 0,0,0,0};

  Real rsdPDE[8];
  Real rsdLOx[8];
  Real rsdLOy[8];
  Real rsdLOz[8];

  // cell integration for canonical element
  integral( fcn, xquad, rsdPDEElemL, nIntegrandL );

  // PDE residuals (left): (advective) + (viscous)
  rsdPDE[0] = (-13./36.) + (931./480.)  ;
  rsdPDE[1] = (-7./18.)  + (5093./4000.);
  rsdPDE[2] = (-35./72.) + (607./480.)  ;
  rsdPDE[3] = (-37./72.) + (3647./2400.);

  rsdPDE[4] = (0)        + (0);
  rsdPDE[5] = (0)        + (0);
  rsdPDE[6] = (0)        + (0);
  rsdPDE[7] = (0)        + (0);

  // LO residuals
  rsdLOx[0] = 0; rsdLOy[0] = 0; rsdLOz[0] = 0;
  rsdLOx[1] = 0; rsdLOy[1] = 0; rsdLOz[1] = 0;
  rsdLOx[2] = 0; rsdLOy[2] = 0; rsdLOz[2] = 0;
  rsdLOx[3] = 0; rsdLOy[3] = 0; rsdLOz[3] = 0;
  rsdLOx[4] = 0; rsdLOy[4] = 0; rsdLOz[4] = 0;
  rsdLOx[5] = 0; rsdLOy[5] = 0; rsdLOz[5] = 0;
  rsdLOx[6] = 0; rsdLOy[6] = 0; rsdLOz[6] = 0;
  rsdLOx[7] = 0; rsdLOy[7] = 0; rsdLOz[7] = 0;

  for (int k = 0; k < 8; k++)
  {
    SANS_CHECK_CLOSE( rsdPDE[k], rsdPDEElemLPDE[k], small_tol, close_tol );

    SANS_CHECK_CLOSE( rsdLOx[k], rsdPDEElemLLO[k][0], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsdLOy[k], rsdPDEElemLLO[k][1], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsdLOz[k], rsdPDEElemLLO[k][2], small_tol, close_tol );
  }
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
