// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Adjoint_LiftingOperator_DGBR2_Triangle_AD_btest
// testing computing the lifting operator adjoint of 2-D DG-BR2 with Advection-Diffusion on triangles
// Constructs the complete PDE and lifting operator system and compares with statically condensed solves

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include <fstream>
#include <string>

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/SurrealS.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "Discretization/DG/IntegrandCell_DGBR2.h"
#include "Discretization/DG/IntegrandInteriorTrace_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"

#include "Discretization/DG/ResidualCell_DGBR2.h"
#include "Discretization/DG/ResidualInteriorTrace_DGBR2.h"
#include "Discretization/DG/ResidualBoundaryTrace_DGBR2.h"

#include "Discretization/DG/JacobianCell_DGBR2_LiftingOperator.h"
#include "Discretization/DG/JacobianBoundaryTrace_DGBR2.h"
#include "Discretization/DG/JacobianFunctionalBoundaryTrace_DGBR2.h"

#include "Discretization/DG/SetFieldInteriorTrace_DGBR2_AdjointLO.h"
#include "Discretization/DG/SetFieldBoundaryTrace_DGBR2_AdjointLO.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "Discretization/DG/IntegrandBoundaryTrace_LinearScalar_sansLG_DGBR2.h"

#include "Field/Element/ElementProjection_L2.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Field/tools/for_each_CellGroup.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

namespace // no-name namespace keep things private to this file
{


template<class Surreal, class IntegrandCell, class SparseMatrix>
class JacobianCell_DGBR2_Test_impl :
    public GroupIntegralCellType< JacobianCell_DGBR2_Test_impl<Surreal, IntegrandCell, SparseMatrix> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCell::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename IntegrandCell::template MatrixQ<Real> MatrixQ;

  typedef typename IntegrandCell::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename IntegrandCell::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  JacobianCell_DGBR2_Test_impl(const IntegrandCell& fcn,
                               SparseMatrix& mtxGlobalPDE_q,
                               SparseMatrix& mtxGlobalPDE_r,
                               SparseMatrix& mtxGlobalLO_q,
                               SparseMatrix& mtxGlobalLO_r ) :
    fcn_(fcn),  mtxGlobalPDE_q_(mtxGlobalPDE_q),  mtxGlobalPDE_r_(mtxGlobalPDE_r),
                mtxGlobalLO_q_(mtxGlobalLO_q),    mtxGlobalLO_r_(mtxGlobalLO_r) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>&     qfld = get<0>(flds);
    const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld = get<1>(flds);

    BOOST_REQUIRE( mtxGlobalPDE_q_.m() == qfld.nDOF() );
    BOOST_REQUIRE( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    BOOST_REQUIRE( mtxGlobalPDE_r_.m() == qfld.nDOF() );
    BOOST_REQUIRE( mtxGlobalPDE_r_.n() == rfld.nDOF()*PhysDim::D );

    BOOST_REQUIRE( mtxGlobalLO_q_.m() == rfld.nDOF()*PhysDim::D );
    BOOST_REQUIRE( mtxGlobalLO_q_.n() == qfld.nDOF() );

    BOOST_REQUIRE( mtxGlobalLO_r_.m() == rfld.nDOF()*PhysDim::D );
    BOOST_REQUIRE( mtxGlobalLO_r_.n() == rfld.nDOF()*PhysDim::D );
  }


//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobal,
      const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                     template FieldCellGroupType<Topology>& fldsCell,
      const int quadratureorder )
  {
    typedef typename XFieldType                              ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ        >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> RFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<>        ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<Surreal> ElementQFieldSurrealClass;
    typedef ElementLift<VectorArrayQSurreal, TopoDim, Topology> ElementrFieldSurrealClass;
    typedef Element<VectorArrayQSurreal, TopoDim, Topology> ElementRFieldSurrealClass;

    const int NTrace = Topology::NTrace;    // total traces in element

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const RFieldCellGroupType& rfldCell = get<1>(fldsCell);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldSurrealClass qfldElemSurreal( qfldCell.basis() );
    ElementrFieldSurrealClass rfldElemsSurreal( rfldCell.basis() );
    ElementRFieldSurrealClass RfldElemSurreal( rfldCell.basis() );

    // DOFs per element
    const int nDOF = qfldElemSurreal.nDOF();

    // variables/equations per DOF
    const int nEqn = IntegrandCell::N;

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_q(nDOF);
    std::vector<int> mapDOFGlobal_r(nDOF*PhysDim::D*NTrace);

    // element integral
    GalerkinWeightedIntegral<TopoDim, Topology, ArrayQSurreal > integralPDE(quadratureorder, nDOF);

    typedef DLA::VectorS<NTrace, VectorArrayQSurreal> IntegrandType;
    GalerkinWeightedIntegral<TopoDim, Topology, IntegrandType> integralLO(quadratureorder, nDOF);

    // element integrand/residual
    std::vector< ArrayQSurreal > rsdPDEElem( nDOF );
    std::vector< IntegrandType > rsdLOElem( nDOF );

    // element jacobian matrices
    MatrixElemClass mtxElemPDE_q(nDOF, nDOF);
    MatrixElemClass mtxElemLO_q(PhysDim::D*nDOF*NTrace, nDOF);

    MatrixElemClass mtxElemPDE_r(nDOF, PhysDim::D*nDOF*NTrace);
    MatrixElemClass mtxElemLO_r(PhysDim::D*nDOF*NTrace, PhysDim::D*nDOF*NTrace);

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero element Jacobians
      mtxElemPDE_q = 0;  mtxElemPDE_r = 0;
      mtxElemLO_q  = 0;  mtxElemLO_r  = 0;

      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElemSurreal, elem );
      rfldCell.getElement( rfldElemsSurreal, elem );

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*((PhysDim::D*NTrace+1)*nDOF); nchunk += nDeriv)
      {

        // associate derivative slots with solution & lifting-operator variables

        int slot, slotOffset;
        for (int j = 0; j < nDOF; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qfldElemSurreal.DOF(j),n).deriv(k) = 0;

            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemSurreal.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset = nEqn*nDOF;

        for (int trace = 0; trace < NTrace; trace++)
        {
          for (int j = 0; j < nDOF; j++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              for (int d = 0; d < PhysDim::D; d++)
              {
                for (int k = 0; k < nDeriv; k++)
                  DLA::index(rfldElemsSurreal[trace].DOF(j)[d],n).deriv(k) = 0;

                slot = slotOffset + PhysDim::D*(nEqn*(nDOF*trace + j) + n) + d;
                if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                  DLA::index(rfldElemsSurreal[trace].DOF(j)[d],n).deriv(slot - nchunk) = 1;
              }
            }
          }
        }

        RfldElemSurreal.vectorViewDOF() = 0;
        for (int trace = 0; trace < Topology::NTrace; trace++)
          RfldElemSurreal.vectorViewDOF() += rfldElemsSurreal[trace].vectorViewDOF();

        for (int n = 0; n < nDOF; n++)
          rsdPDEElem[n] = 0;

        // cell integration for canonical element
        integralPDE( fcn_.integrand_PDE(xfldElem, qfldElemSurreal, RfldElemSurreal),
                     get<-1>(xfldElem), rsdPDEElem.data(), nDOF );

        for (int n = 0; n < nDOF; n++)
          for (int trace = 0; trace < NTrace; trace++)
            rsdLOElem[n][trace] = 0;

        // cell integration for canonical element
        integralLO( fcn_.integrand_LO(xfldElem, rfldElemsSurreal),
                    get<-1>(xfldElem), rsdLOElem.data(), nDOF );

        // accumulate derivatives into element jacobians

        for (int j = 0; j < nDOF; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOF; i++)
              {
                for (int m = 0; m < nEqn; m++)
                {
                  DLA::index(mtxElemPDE_q(i,j),m,n) = DLA::index(rsdPDEElem[i],m).deriv(slot - nchunk);

                  for (int trace = 0; trace < NTrace; trace++)
                  {
                    for (int d = 0; d < PhysDim::D; d++)
                    {
                      const int ii = PhysDim::D*(trace*nDOF + i) + d;
                      DLA::index(mtxElemLO_q(ii,j),m,n) = DLA::index(rsdLOElem[i][trace][d],m).deriv(slot - nchunk);
                    }
                  }
                }
              }
            }
          }
        }
        slotOffset = nEqn*nDOF;

        for (int trace = 0; trace < NTrace; trace++)
        {
          for (int j = 0; j < nDOF; j++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              for (int d1 = 0; d1 < PhysDim::D; d1++)
              {
                slot = slotOffset + PhysDim::D*(nEqn*(nDOF*trace + j) + n) + d1;
                if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                {
                  for (int i = 0; i < nDOF; i++)
                  {
                    for (int m = 0; m < nEqn; m++)
                    {
                      const int ii = PhysDim::D*(trace*nDOF + i);
                      const int jj = PhysDim::D*(trace*nDOF + j) + d1;

                      DLA::index(mtxElemPDE_r(i,jj),m,n) = DLA::index(rsdPDEElem[i],m).deriv(slot - nchunk);

                      for (int d2 = 0; d2 < PhysDim::D; d2++)
                      {
                        DLA::index(mtxElemLO_r(ii+d2,jj),m,n) = DLA::index(rsdLOElem[i][trace][d2],m).deriv(slot - nchunk);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }   // nchunk

      // scatter-add element jacobian to global

      const int rDOF = nDOF*PhysDim::D*NTrace;

      for (int trace = 0; trace < NTrace; trace++)
      {
        rfldCell.associativity( elem, trace ).getGlobalMapping( mapDOFGlobal_q.data(), nDOF );
        for (int n = 0; n < nDOF; n++)
          for (int d = 0; d < PhysDim::D; d++)
            mapDOFGlobal_r[nDOF*PhysDim::D*trace + PhysDim::D*n + d] = PhysDim::D*mapDOFGlobal_q[n]+d;
      }

      // jacobian wrt q
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal_q.data(), nDOF );

      mtxGlobalPDE_q_.scatterAdd( mtxElemPDE_q, mapDOFGlobal_q.data(), nDOF );
      // mtxGlobalLO_q.scatterAdd( mtxElemLO_q , mapDOFGlobal_r, rDOF, mapDOFGlobal_q, nDOF );

      mtxGlobalPDE_r_.scatterAdd( mtxElemPDE_r, mapDOFGlobal_q.data(), nDOF, mapDOFGlobal_r.data(), rDOF );
       mtxGlobalLO_r_.scatterAdd( mtxElemLO_r , mapDOFGlobal_r.data(), rDOF );

    } // elem

  }

protected:
  const IntegrandCell& fcn_;
  SparseMatrix& mtxGlobalPDE_q_;
  SparseMatrix& mtxGlobalPDE_r_;
  SparseMatrix& mtxGlobalLO_q_;
  SparseMatrix& mtxGlobalLO_r_;
};


// Factory function

template<class Surreal, class IntegrandCell, class SparseMatrix>
JacobianCell_DGBR2_Test_impl<Surreal, IntegrandCell, SparseMatrix>
JacobianCell_DGBR2_Test( const IntegrandCellType<IntegrandCell>& fcnPDE,
                         SparseMatrix& mtxGlobalPDE_q,
                         SparseMatrix& mtxGlobalPDE_r,
                         SparseMatrix& mtxGlobalLO_q,
                         SparseMatrix& mtxGlobalLO_r )
{
  return JacobianCell_DGBR2_Test_impl<Surreal, IntegrandCell, SparseMatrix>(
      fcnPDE.cast(), mtxGlobalPDE_q, mtxGlobalPDE_r, mtxGlobalLO_q, mtxGlobalLO_r );
}

//===========================================================================//


template<class Surreal, class IntegrandInteriorTrace, class SparseMatrix>
class JacobianInteriorTrace_DGBR2_Test_impl :
    public GroupIntegralInteriorTraceType< JacobianInteriorTrace_DGBR2_Test_impl<Surreal, IntegrandInteriorTrace, SparseMatrix> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::PDE PDE;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  JacobianInteriorTrace_DGBR2_Test_impl(const IntegrandInteriorTrace& fcn,
                                        SparseMatrix& mtxGlobalPDE_q,
                                        SparseMatrix& mtxGlobalPDE_r,
                                        SparseMatrix& mtxGlobalLO_q,
                                        SparseMatrix& mtxGlobalLO_r ) :
    fcn_(fcn),
    mtxGlobalPDE_q_(mtxGlobalPDE_q), mtxGlobalPDE_r_(mtxGlobalPDE_r),
    mtxGlobalLO_q_(mtxGlobalLO_q), mtxGlobalLO_r_(mtxGlobalLO_r) {}

  std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
  std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>&           qfld = get<0>(flds);
    const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld = get<1>(flds);

    BOOST_REQUIRE( mtxGlobalPDE_q_.m() == qfld.nDOF() );
    BOOST_REQUIRE( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    BOOST_REQUIRE( mtxGlobalPDE_r_.m() == qfld.nDOF() );
    BOOST_REQUIRE( mtxGlobalPDE_r_.n() == rfld.nDOF()*PhysDim::D );

    BOOST_REQUIRE( mtxGlobalLO_q_.m() == rfld.nDOF()*PhysDim::D );
    BOOST_REQUIRE( mtxGlobalLO_q_.n() == qfld.nDOF() );

    BOOST_REQUIRE( mtxGlobalLO_r_.m() == rfld.nDOF()*PhysDim::D );
    BOOST_REQUIRE( mtxGlobalLO_r_.n() == rfld.nDOF()*PhysDim::D );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                     template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                     template FieldCellGroupType<TopologyR>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {

    // Left types
    typedef typename XFieldType                                ::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>           ::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<>        ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldSurrealClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<Surreal> ElementRFieldSurrealClassL;

    // Right types
    typedef typename XFieldType                                ::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>           ::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyR> RFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<>        ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<Surreal> ElementQFieldSurrealClassR;
    typedef typename RFieldCellGroupTypeR::template ElementType<Surreal> ElementRFieldSurrealClassR;

    // Trace types
    typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const int D = PhysDim::D;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const RFieldCellGroupTypeL& rfldCellL = get<1>(fldsCellL);

    const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);
    const RFieldCellGroupTypeR& rfldCellR = get<1>(fldsCellR);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldSurrealClassL qfldElemL( qfldCellL.basis() );
    ElementRFieldSurrealClassL rfldElemL( rfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldSurrealClassR qfldElemR( qfldCellR.basis() );
    ElementRFieldSurrealClassR rfldElemR( rfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // DOFs per element / number of integrals evaluated per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();
    const int nIntegrandL = nDOFL;
    const int nIntegrandR = nDOFR;

    const int rDOFL = D*nDOFL;
    const int rDOFR = D*nDOFR;

    // variables/equations per DOF
    const int nEqn = PDE::N;

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_qL(nDOFL);
    std::vector<int> mapDOFGlobal_rL(rDOFL);
    std::vector<int> mapDOFGlobal_qR(nDOFR);
    std::vector<int> mapDOFGlobal_rR(rDOFR);

    // Lifting operator trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, VectorArrayQSurreal, VectorArrayQSurreal>
      integralLO(quadratureorder, nIntegrandL, nIntegrandR);

    // lifting operator residuals
    DLA::VectorD< VectorArrayQSurreal > rsdLOElemL( nIntegrandL );
    DLA::VectorD< VectorArrayQSurreal > rsdLOElemR( nIntegrandR );

    // PDE trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQSurreal, ArrayQSurreal>
      integralPDE(quadratureorder, nIntegrandL, nIntegrandR);

    // PDE cell residuals
    std::vector< ArrayQSurreal > rsdPDEElemL( nIntegrandL );
    std::vector< ArrayQSurreal > rsdPDEElemR( nIntegrandR );

    // element Lifting operator equation jacobian matrices (Array length D of nDOFxnDOF matrices)
    MatrixElemClass mtxLOElemL_qL(rDOFL, nDOFL);
    MatrixElemClass mtxLOElemR_qL(rDOFR, nDOFL);

    MatrixElemClass mtxLOElemL_qR(rDOFL, nDOFR);
    MatrixElemClass mtxLOElemR_qR(rDOFR, nDOFR);

    // element PDE jacobian matrices wrt q
    MatrixElemClass mtxPDEElemL_qL(nDOFL, nDOFL);
    MatrixElemClass mtxPDEElemR_qL(nDOFR, nDOFL);

    MatrixElemClass mtxPDEElemL_qR(nDOFL, nDOFR);
    MatrixElemClass mtxPDEElemR_qR(nDOFR, nDOFR);

    // element PDE jacobian matrices wrt r (lifting operator)
    MatrixElemClass mtxPDEElemL_rL(nDOFL, rDOFL);
    MatrixElemClass mtxPDEElemR_rL(nDOFR, rDOFL);

    MatrixElemClass mtxPDEElemL_rR(nDOFL, rDOFR);
    MatrixElemClass mtxPDEElemR_rR(nDOFR, rDOFR);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero LO element Jacobians
      mtxLOElemL_qL = 0; mtxLOElemR_qL = 0;
      mtxLOElemL_qR = 0; mtxLOElemR_qR = 0;

      // zero PDE element Jacobians
      mtxPDEElemL_qL = 0; mtxPDEElemR_qL = 0;
      mtxPDEElemL_qR = 0; mtxPDEElemR_qR = 0;
      mtxPDEElemL_rL = 0; mtxPDEElemR_rL = 0;
      mtxPDEElemL_rR = 0; mtxPDEElemR_rR = 0;

      // left/right elements
      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      rfldCellL.getElement( rfldElemL, elemL, canonicalTraceL.trace );

      xfldCellR.getElement( xfldElemR, elemR );
      qfldCellR.getElement( qfldElemR, elemR );
      rfldCellR.getElement( rfldElemR, elemR, canonicalTraceR.trace );

      xfldTrace.getElement( xfldElemTrace, elem );

      // number of simultaneous derivatives per functor call
      const int nDeriv = Surreal::N;

      // loop over derivative chunks for derivatives wrt qL and qR
      for (int nchunk = 0; nchunk < nEqn*(nDOFL+nDOFR); nchunk += nDeriv)
      {

        // associate derivative slots with solution variables

        // wrt qL
        int slot, slotOffset = 0;
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemL.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nEqn*nDOFL;

        // wrt qR
        for (int j = 0; j < nDOFR; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemR.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nEqn*nDOFR;

        for (int n = 0; n < nIntegrandL; n++) rsdLOElemL[n] = 0;
        for (int n = 0; n < nIntegrandR; n++) rsdLOElemR[n] = 0;

        // Lifting operator trace integration for canonical element
        integralLO( fcn_.integrand_LO(xfldElemTrace, canonicalTraceL, canonicalTraceR,
                                      xfldElemL, qfldElemL,
                                      xfldElemR, qfldElemR),
                    xfldElemTrace, rsdLOElemL.data(), nIntegrandL, rsdLOElemR.data(), nIntegrandR);


        for (int n = 0; n < nIntegrandL; n++) rsdPDEElemL[n] = 0;
        for (int n = 0; n < nIntegrandR; n++) rsdPDEElemR[n] = 0;

        // PDE trace integration for canonical element
        integralPDE( fcn_.integrand_PDE(xfldElemTrace, canonicalTraceL, canonicalTraceR,
                                        xfldElemL, qfldElemL, rfldElemL,
                                        xfldElemR, qfldElemR, rfldElemR),
                     xfldElemTrace, rsdPDEElemL.data(), nIntegrandL, rsdPDEElemR.data(), nIntegrandR);

        // accumulate derivatives into element jacobians

        slotOffset = 0;
        // wrt qL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qfldElemL.DOF(j),n).deriv(slot - nchunk) = 0; // unset derivative

              //L_qL
              for (int i = 0; i < nDOFL; i++)
              {
                for (int m = 0; m < nEqn; m++)
                {
                  for (int d = 0; d < D; d++)
                    DLA::index(mtxLOElemL_qL(D*i+d,j),m,n) = DLA::index(rsdLOElemL[i][d],m).deriv(slot - nchunk);

                  DLA::index(mtxPDEElemL_qL(i,j),m,n) = DLA::index(rsdPDEElemL[i],m).deriv(slot - nchunk);
                }
              }

              //R_qL
              for (int i = 0; i < nDOFR; i++)
              {
                for (int m = 0; m < nEqn; m++)
                {
                  for (int d = 0; d < D; d++)
                    DLA::index(mtxLOElemR_qL(D*i+d,j),m,n) = DLA::index(rsdLOElemR[i][d],m).deriv(slot - nchunk);

                  DLA::index(mtxPDEElemR_qL(i,j),m,n) = DLA::index(rsdPDEElemR[i],m).deriv(slot - nchunk);
                }
              }

            }
          } //n loop
        } //j loop
        slotOffset += nEqn*nDOFL;

        // wrt qR
        for (int j = 0; j < nDOFR; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qfldElemR.DOF(j),n).deriv(slot - nchunk) = 0; // unset derivative

              //L_qR
              for (int i = 0; i < nDOFL; i++)
              {
                for (int m = 0; m < nEqn; m++)
                {
                  for (int d = 0; d < D; d++)
                    DLA::index(mtxLOElemL_qR(D*i+d,j),m,n) = DLA::index(rsdLOElemL[i][d],m).deriv(slot - nchunk);

                  DLA::index(mtxPDEElemL_qR(i,j),m,n) = DLA::index(rsdPDEElemL[i],m).deriv(slot - nchunk);
                }
              }

              //R_qR
              for (int i = 0; i < nDOFR; i++)
              {
                for (int m = 0; m < nEqn; m++)
                {
                  for (int d = 0; d < D; d++)
                    DLA::index(mtxLOElemR_qR(D*i+d,j),m,n) = DLA::index(rsdLOElemR[i][d],m).deriv(slot - nchunk);

                  DLA::index(mtxPDEElemR_qR(i,j),m,n) = DLA::index(rsdPDEElemR[i],m).deriv(slot - nchunk);
                }
              }

            }
          } //n loop
        } //j loop
        slotOffset += nEqn*nDOFR;

      }   // nchunk loop


      // loop over derivative chunks for the PDE
      for (int nchunk = 0; nchunk < nEqn*D*(nDOFL+nDOFR); nchunk += nDeriv)
      {

        // associate derivative slots with lifting operator variables

        int slot, slotOffset = 0;

        // wrt rL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*(D*j + d) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(rfldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 1;
            }
          }
        }
        slotOffset += nEqn*D*nDOFL;

        // wrt rR
        for (int j = 0; j < nDOFR; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*(D*j + d) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(rfldElemR.DOF(j)[d],n).deriv(slot - nchunk) = 1;
            }
          }
        }
        slotOffset += nEqn*D*nDOFR;

        for (int n = 0; n < nIntegrandL; n++) rsdPDEElemL[n] = 0;
        for (int n = 0; n < nIntegrandR; n++) rsdPDEElemR[n] = 0;

        // trace integration for canonical element
        integralPDE( fcn_.integrand_PDE(xfldElemTrace, canonicalTraceL, canonicalTraceR,
                                        xfldElemL, qfldElemL, rfldElemL,
                                        xfldElemR, qfldElemR, rfldElemR),
                     xfldElemTrace, rsdPDEElemL.data(), nIntegrandL, rsdPDEElemR.data(), nIntegrandR);

        // accumulate derivatives into element jacobians

        slotOffset = 0;
        // wrt rL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*(D*j + d) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                DLA::index(rfldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 0; //unset derivative

                //L_rL
                for (int i = 0; i < nDOFL; i++)
                {
                  for (int m = 0; m < nEqn; m++)
                  {
                    DLA::index(mtxPDEElemL_rL(i,D*j+d),m,n) = DLA::index(rsdPDEElemL[i],m).deriv(slot - nchunk);
                  }
                }

                //R_rL
                for (int i = 0; i < nDOFR; i++)
                {
                  for (int m = 0; m < nEqn; m++)
                  {
                    DLA::index(mtxPDEElemR_rL(i,D*j+d),m,n) = DLA::index(rsdPDEElemR[i],m).deriv(slot - nchunk);
                  }
                }

              }
            } //n loop
          } //d loop
        } //j loop
        slotOffset += nEqn*D*nDOFL;


        // wrt rR
        for (int j = 0; j < nDOFR; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*(D*j + d) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                DLA::index(rfldElemR.DOF(j)[d],n).deriv(slot - nchunk) = 0; //unset derivative

                //L_rR
                for (int i = 0; i < nDOFL; i++)
                {
                  for (int m = 0; m < nEqn; m++)
                  {
                    DLA::index(mtxPDEElemL_rR(i,D*j+d),m,n) = DLA::index(rsdPDEElemL[i],m).deriv(slot - nchunk);
                  }
                }

                //R_rR
                for (int i = 0; i < nDOFR; i++)
                {
                  for (int m = 0; m < nEqn; m++)
                  {
                    DLA::index(mtxPDEElemR_rR(i,D*j+d),m,n) = DLA::index(rsdPDEElemR[i],m).deriv(slot - nchunk);
                  }
                }

              }
            } //n loop
          } //d1 loop
        } //j loop
        slotOffset += nEqn*D*nDOFR;

      }   // nchunk loop

      // scatter-add element jacobian to global
      rfldCellL.associativity( elemL, canonicalTraceL.trace ).getGlobalMapping( mapDOFGlobal_qL.data(), nDOFL );
      rfldCellR.associativity( elemR, canonicalTraceR.trace ).getGlobalMapping( mapDOFGlobal_qR.data(), nDOFR );

      // global mapping for rL
      for (int n = 0; n < nDOFL; n++)
        for (int d = 0; d < D; d++)
          mapDOFGlobal_rL[D*n+d] = D*mapDOFGlobal_qL[n] + d;

      // global mapping for rR
      for (int n = 0; n < nDOFR; n++)
        for (int d = 0; d < D; d++)
          mapDOFGlobal_rR[D*n+d] = D*mapDOFGlobal_qR[n] + d;

      // global mapping for qL
      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobal_qL.data(), nDOFL );

      // global mapping for qR
      qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobal_qR.data(), nDOFR );

      // jacobian wrt qL
      mtxGlobalPDE_q_.scatterAdd( mtxPDEElemL_qL, mapDOFGlobal_qL.data(), nDOFL );
      mtxGlobalPDE_q_.scatterAdd( mtxPDEElemR_qL, mapDOFGlobal_qR.data(), nDOFR, mapDOFGlobal_qL.data(), nDOFL );
       mtxGlobalLO_q_.scatterAdd( mtxLOElemL_qL , mapDOFGlobal_rL.data(), rDOFL, mapDOFGlobal_qL.data(), nDOFL );
       mtxGlobalLO_q_.scatterAdd( mtxLOElemR_qL , mapDOFGlobal_rR.data(), rDOFR, mapDOFGlobal_qL.data(), nDOFL );

      // jacobian wrt qR
      mtxGlobalPDE_q_.scatterAdd( mtxPDEElemL_qR, mapDOFGlobal_qL.data(), nDOFL, mapDOFGlobal_qR.data(), nDOFR );
      mtxGlobalPDE_q_.scatterAdd( mtxPDEElemR_qR, mapDOFGlobal_qR.data(), nDOFR );
       mtxGlobalLO_q_.scatterAdd( mtxLOElemL_qR , mapDOFGlobal_rL.data(), rDOFL, mapDOFGlobal_qR.data(), nDOFR );
       mtxGlobalLO_q_.scatterAdd( mtxLOElemR_qR , mapDOFGlobal_rR.data(), rDOFR, mapDOFGlobal_qR.data(), nDOFR );

      // jacobian wrt rL
      mtxGlobalPDE_r_.scatterAdd( mtxPDEElemL_rL, mapDOFGlobal_qL.data(), nDOFL, mapDOFGlobal_rL.data(), rDOFL );
      mtxGlobalPDE_r_.scatterAdd( mtxPDEElemR_rL, mapDOFGlobal_qR.data(), nDOFR, mapDOFGlobal_rL.data(), rDOFL );

      // jacobian wrt rR
      mtxGlobalPDE_r_.scatterAdd( mtxPDEElemL_rR, mapDOFGlobal_qL.data(), nDOFL, mapDOFGlobal_rR.data(), rDOFR );
      mtxGlobalPDE_r_.scatterAdd( mtxPDEElemR_rR, mapDOFGlobal_qR.data(), nDOFR, mapDOFGlobal_rR.data(), rDOFR );

    } //loop over elements

  }

protected:
  const IntegrandInteriorTrace& fcn_;
  SparseMatrix& mtxGlobalPDE_q_;
  SparseMatrix& mtxGlobalPDE_r_;
  SparseMatrix& mtxGlobalLO_q_;
  SparseMatrix& mtxGlobalLO_r_;
};


// Factory function

template<class Surreal, class IntegrandInteriorTrace, class SparseMatrix>
JacobianInteriorTrace_DGBR2_Test_impl<Surreal, IntegrandInteriorTrace, SparseMatrix>
JacobianInteriorTrace_DGBR2_Test( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                                  SparseMatrix& mtxGlobalPDE_q,
                                  SparseMatrix& mtxGlobalPDE_r,
                                  SparseMatrix& mtxGlobalLO_q,
                                  SparseMatrix& mtxGlobalLO_r )
{
  return JacobianInteriorTrace_DGBR2_Test_impl<Surreal, IntegrandInteriorTrace, SparseMatrix>(
      fcn.cast(), mtxGlobalPDE_q, mtxGlobalPDE_r, mtxGlobalLO_q, mtxGlobalLO_r );
}

//===========================================================================//


template<class Surreal, class IntegrandBoundaryTrace, class SparseMatrix>
class JacobianBoundaryTrace_DGBR2_Test_impl :
    public GroupIntegralBoundaryTraceType< JacobianBoundaryTrace_DGBR2_Test_impl<Surreal, IntegrandBoundaryTrace, SparseMatrix> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::PDE PDE;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  JacobianBoundaryTrace_DGBR2_Test_impl(const IntegrandBoundaryTrace& fcn,
                                        SparseMatrix& mtxGlobalPDE_q,
                                        SparseMatrix& mtxGlobalPDE_r,
                                        SparseMatrix& mtxGlobalLO_q,
                                        SparseMatrix& mtxGlobalLO_r ) :
    fcn_(fcn),
    mtxGlobalPDE_q_(mtxGlobalPDE_q), mtxGlobalPDE_r_(mtxGlobalPDE_r),
    mtxGlobalLO_q_ (mtxGlobalLO_q) , mtxGlobalLO_r_(mtxGlobalLO_r) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);

    BOOST_REQUIRE( mtxGlobalPDE_q_.m() == qfld.nDOF() );
    BOOST_REQUIRE( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                                        template FieldCellGroupType<TopologyL>& fldsCellL,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<Surreal> ElementRFieldClassL;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    const int D = PhysDim::D;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const RFieldCellGroupTypeL& rfldCellL = get<1>(fldsCellL);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementRFieldClassL rfldElemL( rfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // variables/equations per DOF
    const int nEqn = IntegrandBoundaryTrace::N;

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_qL(nDOFL,-1);
    std::vector<int> mapDOFGlobal_rL(D*nDOFL,-1);

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQSurreal> integralPDE(quadratureorder, nDOFL);
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, VectorArrayQSurreal> integralLO(quadratureorder, nDOFL);

    // element integrand/residuals
    std::vector<ArrayQSurreal> rsdPDEElemL( nDOFL );
    std::vector<VectorArrayQSurreal> rsdLOElemL( nDOFL );

    // element jacobians
    MatrixElemClass mtxPDEElemL_qL(nDOFL, nDOFL);
    MatrixElemClass mtxPDEElemL_rL(nDOFL, D*nDOFL);

    MatrixElemClass mtxLOElemL_qL(D*nDOFL, nDOFL);
    MatrixElemClass mtxLOElemL_rL(D*nDOFL, D*nDOFL);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero element Jacobians
      mtxPDEElemL_qL = 0; mtxPDEElemL_rL = 0;
      mtxLOElemL_qL  = 0; mtxLOElemL_rL  = 0;

      // left element
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      rfldCellL.getElement( rfldElemL, elemL, canonicalTraceL.trace );

      xfldTrace.getElement( xfldElemTrace, elem );

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*((D+1)*nDOFL); nchunk += nDeriv)
      {

        // associate derivative slots with solution variables

        int slot, slotOffset = 0;

        //wrt qL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qfldElemL.DOF(j), n).deriv(k) = 0;

            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemL.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nEqn*nDOFL;

        // wrt rL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              for (int k = 0; k < nDeriv; k++)
                DLA::index(rfldElemL.DOF(j)[d],n).deriv(k) = 0;

              slot = slotOffset + nEqn*(D*j + d) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(rfldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 1;
            }
          }
        }

        // line integration for canonical element

        for (int n = 0; n < nDOFL; n++)
          rsdLOElemL[n] = 0;

        integralLO( fcn_.integrand_LO(xfldElemTrace, canonicalTraceL,
                                      xfldElemL, qfldElemL, rfldElemL),
                    get<-1>(xfldElemTrace),
                    rsdLOElemL.data(), nDOFL );

        for (int n = 0; n < nDOFL; n++)
          rsdPDEElemL[n] = 0;

        integralPDE(
            fcn_.integrand_PDE(xfldElemTrace, canonicalTraceL,
                           xfldElemL, qfldElemL, rfldElemL),
            get<-1>(xfldElemTrace),
            rsdPDEElemL.data(), nDOFL );

        // accumulate derivatives into element jacobian
        slotOffset = 0;

        // wrt qL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                {
                  DLA::index(mtxPDEElemL_qL(i,j), m,n) += DLA::index(rsdPDEElemL[i], m).deriv(slot - nchunk);

                  for (int d = 0; d < D; d++)
                    DLA::index(mtxLOElemL_qL(D*i+d,j),m,n) += DLA::index(rsdLOElemL[i][d],m).deriv(slot - nchunk);
                }
            }
          }
        }
        slotOffset += nEqn*nDOFL;

        // wrt rL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d1 = 0; d1 < D; d1++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*(D*j + d1) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                for (int i = 0; i < nDOFL; i++)
                  for (int m = 0; m < nEqn; m++)
                  {
                    DLA::index(mtxPDEElemL_rL(i,D*j+d1),m,n) += DLA::index(rsdPDEElemL[i],m).deriv(slot - nchunk);

                    for (int d0 = 0; d0 < D; d0++)
                      DLA::index(mtxLOElemL_rL(D*i*d0,D*j+d1),m,n) += DLA::index(rsdLOElemL[i][d0],m).deriv(slot - nchunk);
                  }
              }
            } //n loop
          } //d1 loop
        } //j loop

      }   // nchunk

      // scatter-add element jacobian to global

      rfldCellL.associativity( elemL, canonicalTraceL.trace ).getGlobalMapping( mapDOFGlobal_qL.data(), nDOFL );

      // global mapping for rL
      for (int n = 0; n < nDOFL; n++)
        for (int d = 0; d < PhysDim::D; d++)
          mapDOFGlobal_rL[PhysDim::D*n+d] = PhysDim::D*mapDOFGlobal_qL[n] + d;

      // global mapping for qL
      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobal_qL.data(), nDOFL );

      mtxGlobalPDE_q_.scatterAdd( mtxPDEElemL_qL, mapDOFGlobal_qL.data(), nDOFL);
      mtxGlobalPDE_r_.scatterAdd( mtxPDEElemL_rL, mapDOFGlobal_qL.data(), nDOFL, mapDOFGlobal_rL.data(), nDOFL*PhysDim::D );

      mtxGlobalLO_q_.scatterAdd( mtxLOElemL_qL, mapDOFGlobal_rL.data(), nDOFL*PhysDim::D , mapDOFGlobal_qL.data(), nDOFL );
      mtxGlobalLO_r_.scatterAdd( mtxLOElemL_rL, mapDOFGlobal_rL.data(), nDOFL*PhysDim::D );
    }
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  SparseMatrix& mtxGlobalPDE_q_;
  SparseMatrix& mtxGlobalPDE_r_;
  SparseMatrix& mtxGlobalLO_q_;
  SparseMatrix& mtxGlobalLO_r_;
};

// Factory function

template<class Surreal, class IntegrandBoundaryTrace, class SparseMatrix>
JacobianBoundaryTrace_DGBR2_Test_impl<Surreal, IntegrandBoundaryTrace, SparseMatrix>
JacobianBoundaryTrace_DGBR2_Test( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                  SparseMatrix& mtxGlobalPDE_q,
                                  SparseMatrix& mtxGlobalPDE_r,
                                  SparseMatrix& mtxGlobalLO_q,
                                  SparseMatrix& mtxGlobalLO_r )
{
  return JacobianBoundaryTrace_DGBR2_Test_impl<Surreal, IntegrandBoundaryTrace, SparseMatrix>(
      fcn.cast(),
      mtxGlobalPDE_q, mtxGlobalPDE_r,
      mtxGlobalLO_q , mtxGlobalLO_r );
}

//===========================================================================//


template<class Surreal, class FunctionalIntegrandBoundaryTrace, class BCIntegrandBoundaryTrace, template<class> class Vector>
class JacobianFunctionalBoundaryTrace_DGBR2_Test_impl :
    public GroupIntegralBoundaryTraceType<
       JacobianFunctionalBoundaryTrace_DGBR2_Test_impl<Surreal, FunctionalIntegrandBoundaryTrace, BCIntegrandBoundaryTrace, Vector> >
{
public:
  typedef typename FunctionalIntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename FunctionalIntegrandBoundaryTrace::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayJ<Real> ArrayJ;
  typedef typename FunctionalIntegrandBoundaryTrace::template MatrixJ<Real> MatrixJ;
  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayJ<Surreal> ArrayJSurreal;
  typedef DLA::VectorS<PhysDim::D, MatrixJ> VectorMatrixJ;

  typedef typename BCIntegrandBoundaryTrace::PDE PDE;
  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixLOElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixRElemClass;

  JacobianFunctionalBoundaryTrace_DGBR2_Test_impl( const FunctionalIntegrandBoundaryTrace& fcnJ,
                                                   const BCIntegrandBoundaryTrace& fcnBC,
                                                   Vector<MatrixJ>& jacFunctional_q,
                                                   Vector<MatrixJ>& jacFunctional_r) :
    fcnJ_(fcnJ),
    fcnBC_(fcnBC),
    jacFunctional_q_(jacFunctional_q),
    jacFunctional_r_(jacFunctional_r)
  {
    // Find all groups that are common between the BC and functional
    for (std::size_t i = 0; i < fcnJ_.nBoundaryGroups(); i++)
    {
      for (std::size_t j = 0; j < fcnBC_.nBoundaryGroups(); j++)
      {
        std::size_t iBoundaryGroupJ = fcnJ_.boundaryGroup(i);
        if (iBoundaryGroupJ == fcnBC_.boundaryGroup(j))
          boundaryTraceGroups_.push_back(iBoundaryGroupJ);
      }
    }
  }

  std::size_t nBoundaryGroups() const { return boundaryTraceGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return boundaryTraceGroups_[n]; }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,           // q
                                                   FieldLift<PhysDim,TopoDim,VectorArrayQ>  // r
                                       >::type & flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    const FieldLift<PhysDim,TopoDim,VectorArrayQ>& rfld = get<1>(flds);

    BOOST_REQUIRE_EQUAL(           qfld.nDOF(), jacFunctional_q_.m() );
    BOOST_REQUIRE_EQUAL(PhysDim::D*rfld.nDOF(), jacFunctional_r_.m() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType, class XFieldTraceGroupType>
  void
  integrate( const int cellGroupGloalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,           // q
                                                  FieldLift<PhysDim, TopoDim, VectorArrayQ>  // r
                                     >::type::template FieldCellGroupType<TopologyL>& fldsCellL,
             const int traceGroupGlobal,
             const XFieldTraceGroupType& xfldTrace,
             int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<>        ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<Surreal> ElementRFieldClassL;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const int D = PhysDim::D;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const RFieldCellGroupTypeL& rfldCellL = get<1>(fldsCellL);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementRFieldClassL rfldElemL( rfldCellL.basis() );

    ElementXFieldTraceClass  xfldElemTrace(  xfldTrace.basis() );

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    // total number of entries in ArrayQ
    const int nVar = DLA::VectorSize<ArrayQ>::M;

    // total number of outputs in the functional
    const int nJEqn = DLA::VectorSize<ArrayJ>::M;
    static_assert( nJEqn == 1 , "number of output functionals must be 1 for now");

    // DOFs per element
    int nDOFL = qfldElemL.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_qL(nDOFL, -1);
    std::vector<int> mapDOFGlobal_rL(nDOFL, -1);

    // element integrand/residuals
    std::vector<VectorArrayQSurreal> rsdLOElemL( nDOFL );

    // trace element integral
    ElementIntegral<TopoDimTrace, TopologyTrace, ArrayJSurreal> integralJ(quadratureorder);
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, VectorArrayQSurreal> integralLO(quadratureorder, nDOFL);

    MatrixRElemClass mtxTLOElemL_qL(nDOFL, nDOFL);
    DLA::MatrixD< DLA::MatrixS<D,D,Real> > mtxTLOElemL_rL(nDOFL, nDOFL);

    // computes the elemental mass matrix
    ElementalMassMatrix<TopoDim, TopologyL> massMtx(get<-1>(xfldElemL), qfldElemL);

    // element functional jacobian vector
    DLA::VectorD<MatrixJ> jacFunctionalElem_q(nDOFL);
    DLA::VectorD<VectorMatrixJ> jacFunctionalElem_r(nDOFL);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // reset the element jacobian
      jacFunctionalElem_q = 0;
      jacFunctionalElem_r = 0;
      mtxTLOElemL_qL = 0;
      mtxTLOElemL_rL = 0;

      // left element
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      rfldCellL.getElement( rfldElemL, elemL, canonicalTraceL.trace );

      xfldTrace.getElement(  xfldElemTrace, elem );

      // Compute the elemental mass matrix and then add the lifting operator term from the BC
      massMtx(get<-1>(xfldElemL), mtxTLOElemL_rL);

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nVar*((D+1)*nDOFL); nchunk += nDeriv)
      {

        // associate derivative slots with solution variables

        int slot = 0;
        int slotOffset = 0;
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemL.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nVar*nDOFL;

        // wrt rL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nVar; n++)
            {
              slot = slotOffset + nVar*D*j + nVar*d + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(rfldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 1;
            }
          }
        }

        // integration for canonical element
        for (int n = 0; n < nDOFL; n++)
          rsdLOElemL[n] = 0;

        integralLO( fcnBC_.integrand_LO(xfldElemTrace, canonicalTraceL,
                                        xfldElemL, qfldElemL, rfldElemL),
                    get<-1>(xfldElemTrace),
                    rsdLOElemL.data(), nDOFL );

        // trace integration for canonical element
        ArrayJSurreal functional = 0;
        integralJ( fcnJ_.integrand(fcnBC_,
                                   xfldElemTrace, canonicalTraceL,
                                   xfldElemL, qfldElemL, rfldElemL),
                   get<-1>(xfldElemTrace),
                   functional );

        // accumulate derivatives into element jacobian

        //wrt qL
        slotOffset = 0;
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qfldElemL.DOF(j), n).deriv(slot - nchunk) = 0; // Reset the derivative

              // Note that n,m are transposed intentionally
              // TODO: Test where m > 1
              //for (int m = 0; m < nJEqn; m++)
              //  DLA::index(jacFunctionalElem_q[j],n,m) += DLA::index(functional, m).deriv(slot - nchunk);
              DLA::index(jacFunctionalElem_q[j], n) = DLA::index(functional, n).deriv(slot - nchunk);
            }
          }
        }
//
        slotOffset += nVar*nDOFL;
        // wrt rL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d1 = 0; d1 < D; d1++)
          {
            for (int n = 0; n < nVar; n++)
            {
              slot = slotOffset + nVar*D*j + nVar*d1 + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                DLA::index(rfldElemL.DOF(j)[d1],n).deriv(slot - nchunk) = 0; // Reset the derivative

                DLA::index(jacFunctionalElem_r[j][d1],n) = DLA::index(functional, n).deriv(slot - nchunk);
              }
            } //n loop
          } //d1 loop
        } //j loop

      }   // nchunk

      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobal_qL.data(), nDOFL );

      // scatter-add element jacobian to global
      for (int i = 0; i < nDOFL; i++)
        jacFunctional_q_[mapDOFGlobal_qL[i]] += jacFunctionalElem_q[i];

      rfldCellL.associativity( elemL, canonicalTraceL.trace ).getGlobalMapping( mapDOFGlobal_rL.data(), nDOFL );

      for (int i = 0; i < nDOFL; i++)
        for (int d = 0; d < D; d++)
          jacFunctional_r_[D*mapDOFGlobal_rL[i]+d] += jacFunctionalElem_r[i][d];

    } // elem
  }

protected:
  const FunctionalIntegrandBoundaryTrace& fcnJ_;
  const BCIntegrandBoundaryTrace& fcnBC_;
  std::vector<int> boundaryTraceGroups_;
  Vector<MatrixJ>& jacFunctional_q_;
  Vector<MatrixJ>& jacFunctional_r_;
};

// Factory function

template<class Surreal, class FunctionalIntegrandBoundaryTrace, class BCIntegrandBoundaryTrace, template<class> class Vector, class MatrixJ>
JacobianFunctionalBoundaryTrace_DGBR2_Test_impl<Surreal, FunctionalIntegrandBoundaryTrace, BCIntegrandBoundaryTrace, Vector>
JacobianFunctionalBoundaryTrace_DGBR2_Test( const IntegrandBoundaryTraceType<FunctionalIntegrandBoundaryTrace>& fcnJ,
                                              const IntegrandBoundaryTraceType<BCIntegrandBoundaryTrace>& fcnBC,
                                              Vector< MatrixJ >& func_q, Vector< MatrixJ >& func_r)
{
  typedef typename Scalar<MatrixJ>::type T;
  static_assert( std::is_same<typename FunctionalIntegrandBoundaryTrace::template MatrixJ<T>, MatrixJ>::value, "These should be the same");
  return JacobianFunctionalBoundaryTrace_DGBR2_Test_impl<Surreal, FunctionalIntegrandBoundaryTrace, BCIntegrandBoundaryTrace, Vector>(
      fcnJ.cast(), fcnBC.cast(), func_q, func_r );
}


}


//############################################################################//
BOOST_AUTO_TEST_SUITE( Adjoint_LiftingOperator_DGBR2_Triangle_NS_test_suite )


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_NS )
{
  const int D = PhysD2::D;

  typedef SurrealS<4> SurrealClass;

  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template MatrixQ<Real> MatrixQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,D,MatrixQ> RowMatrixQ;

  typedef BCNDConvertSpace<PhysD2, BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState,PDEClass>> NDBCWallClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCWallClass>, NDBCWallClass::Category> NDBCWallVecCat;

  typedef BCNDConvertSpace<PhysD2, BCEuler2D<BCTypeFullState_mitState,PDEClass>> NDBCCharClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCCharClass>, NDBCCharClass::Category> NDBCCharVecCat;


  typedef IntegrandCell_DGBR2<NDPDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_DGBR2<NDPDEClass> IntegrandTraceClass;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCWallVecCat, DGBR2> IntegrandBCWallClass;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCCharVecCat, DGBR2> IntegrandBCCharClass;

  // DRAG INTEGRAND
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;

  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> IntegrandOutputClass;

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.1;
  const Real Reynolds = 10;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
  const Real qRef = 1;                              // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);
  const Real cRef = qRef/Mach;                      // speed of sound
  const Real tRef = (cRef*cRef)/(gamma*R);          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum );

  // BC

  NDBCWallClass bcwall( pde );

  bool upwind = true;
  NDBCCharClass bcchar(pde, DensityVelocityPressure2D<Real>(rhoRef, uRef, vRef, pRef), upwind);

  // grid: HierarchicalP1 (aka X1)

  int ii = 2;
  int jj = 2;

#if 1
  XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj );
  std::vector<int> interorTraceGroups = {0,1,2};
#else
  XField2D_Box_Triangle_X1 xfld( ii, jj );
  std::vector<int> interorTraceGroups = {0};
#endif

  // initial condition
  ArrayQ q0;
  pde.setDOFFrom( q0, DensityVelocityPressure2D<Real>(rhoRef, uRef, vRef, pRef) );

  // solution: Legendre P1

  int order = 1;

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = q0;

  const int nDOFPDE = qfld.nDOF();

#if 0
  cout << "btest: dumping qfld" << endl;  qfld.dump(2);
#endif

  // Lifting operators:

  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Legendre);
  rfld = 0;

  const int nDOFLO = D*rfld.nDOF();

  // Lagrange multiplier: Legendre P1

#if 1
  int orderBC = 0;
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, orderBC, BasisFunctionCategory_Legendre, {} );
#elif 0
  order = 1;
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre, {} );
#else
  int orderBC = 1;
  std::vector<std::vector<int>> boundaryGroupSets = {{0},{1},{2},{3}};
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( boundaryGroupSets, xfld, orderBC, BasisFunctionCategory_Legendre, {} );
#endif

  const int nDOFBC = lgfld.nDOF();
  lgfld = 0;

#if 0
  cout << "btest: dumping lgfld" << endl;  lgfld.dump(2);
#endif

  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrands
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandTraceClass fcnTrace( pde, disc, interorTraceGroups );
  IntegrandBCWallClass fcnBCwall( pde, bcwall, {0,2}, disc );
  IntegrandBCCharClass fcnBCchar( pde, bcchar, {1,3}, disc );

  NDOutputClass outputFcn(pde, 1., 0.);
  IntegrandOutputClass output_integrand( outputFcn, {0,2} );

  // quadrature rule
  int quadratureOrder[4] = {-1, -1, -1, -1};    // max

  // linear system setup

  // complete jacobian nonzero pattern without static condensation of lifting operator
  //
  //           r   q  lg
  //   LO      X   X   0
  //   PDE     X   X   X
  //   BC      0   X   0

  DLA::MatrixD<DLA::MatrixD<MatrixQ>> jacT =
      {{ { nDOFLO, nDOFLO}, { nDOFLO, nDOFPDE}, { nDOFLO, nDOFBC} },
       { {nDOFPDE, nDOFLO}, {nDOFPDE, nDOFPDE}, {nDOFPDE, nDOFBC} },
       { { nDOFBC, nDOFLO}, { nDOFBC, nDOFPDE}, { nDOFBC, nDOFBC} }};

  DLA::VectorD<DLA::VectorD<ArrayQ>> rhs = DLA::VectorD< DLA::DenseVectorSize >({ nDOFLO, nDOFPDE, nDOFBC });
  DLA::VectorD<DLA::VectorD<ArrayQ>> sln = DLA::VectorD< DLA::DenseVectorSize >({ nDOFLO, nDOFPDE, nDOFBC });

  rhs = 0;

  // Create right hand side for the adjoint
  IntegrateBoundaryTraceGroups<TopoD2>::integrate(
      JacobianFunctionalBoundaryTrace_DGBR2_Test<SurrealClass>( output_integrand, fcnBCwall, rhs(1), rhs(0) ),
      xfld, (qfld, rfld), quadratureOrder, xfld.nBoundaryTraceGroups() );

  BOOST_REQUIRE_EQUAL(0, nDOFBC);
  for (int i = 0; i < nDOFBC; i++)
    for (int m = 0; m < ArrayQ::M; m++)
      rhs[2][i][m] = 3*m + tan(2*PI*i/nDOFBC);

#if 0
  std::cout << "rhs = " << std::endl;
  for (int i = 0; i < nDOFLO; i++)
  {
    for (int m = 0; m < ArrayQ::M; m++)
      std::cout << std::setprecision( 16 ) << rhs[0][i][m] << ", ";
    std::cout << std::endl;
  }

  for (int i = 0; i < nDOFPDE; i++)
  {
    for (int m = 0; m < ArrayQ::M; m++)
      std::cout << std::setprecision( 16 ) << rhs[1][i][m] << ", ";
    std::cout << std::endl;
  }

  for (int i = 0; i < nDOFBC; i++)
  {
    for (int m = 0; m < ArrayQ::M; m++)
      std::cout << std::setprecision( 16 ) << rhs[2][i][m] << ", ";
    std::cout << std::endl;
  }
#endif

  jacT = 0;

  // jacobian transpose computed with finite difference

  auto jacTLO_r   = Transpose(jacT)(0,0);
  auto jacTLO_q   = Transpose(jacT)(0,1);
//auto jacTLO_lg  = Transpose(jacT)(0,2);

  auto jacTPDE_r  = Transpose(jacT)(1,0);
  auto jacTPDE_q  = Transpose(jacT)(1,1);
//auto jacTPDE_lg = Transpose(jacT)(1,2);

//auto jacTBC_r   = Transpose(jacT)(2,0);
//auto jacTBC_q   = Transpose(jacT)(2,1);
//auto jacTBC_lg  = Transpose(jacT)(2,2);

  IntegrateCellGroups<TopoD2>::integrate(
      JacobianCell_DGBR2_Test<SurrealClass>(fcnCell, jacTPDE_q, jacTPDE_r, jacTLO_q, jacTLO_r),
      xfld, (qfld, rfld), quadratureOrder, xfld.nCellGroups() );

  IntegrateInteriorTraceGroups<TopoD2>::integrate(
      JacobianInteriorTrace_DGBR2_Test<SurrealClass>(fcnTrace, jacTPDE_q, jacTPDE_r, jacTLO_q, jacTLO_r),
      xfld, (qfld, rfld), quadratureOrder, xfld.nInteriorTraceGroups());

  IntegrateBoundaryTraceGroups<TopoD2>::integrate(
      JacobianBoundaryTrace_DGBR2_Test<SurrealClass>(fcnBCwall, jacTPDE_q, jacTPDE_r, jacTLO_q, jacTLO_r),
      xfld, (qfld, rfld), quadratureOrder, xfld.nBoundaryTraceGroups() );

  IntegrateBoundaryTraceGroups<TopoD2>::integrate(
      JacobianBoundaryTrace_DGBR2_Test<SurrealClass>(fcnBCchar, jacTPDE_q, jacTPDE_r, jacTLO_q, jacTLO_r),
      xfld, (qfld, rfld), quadratureOrder, xfld.nBoundaryTraceGroups() );

#if 0
  SLA::WriteMatrixMarketFile(jacT(0,0), "tmp/jacT00.mtx");
  SLA::WriteMatrixMarketFile(jacT(0,1), "tmp/jacT01.mtx");
  SLA::WriteMatrixMarketFile(jacT(1,0), "tmp/jacT10.mtx");
  SLA::WriteMatrixMarketFile(jacT(1,1), "tmp/jacT11.mtx");
#endif

  // Compute the adjoint
  sln = DLA::InverseLU::Solve(jacT, rhs);

#if 0
  std::cout << "w = " << std::endl;
  for (int i = 0; i < nDOFPDE; i++)
  {
    for (int m = 0; m < ArrayQ::M; m++)
      std::cout << std::setprecision( 16 ) << sln[1][i][m] << ", ";
    std::cout << std::endl;
  }
#endif

  // Adjoint field via finite difference
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfldTest(xfld, order, BasisFunctionCategory_Legendre);
  wfldTest = 0;

  // Lifting operators adjoint via finite difference
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> sfldTest(xfld, order, BasisFunctionCategory_Legendre);
  sfldTest = 0;

  // Lagrange multiplier adjoint via finite difference

#if 1
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> mufldTest( xfld, orderBC, BasisFunctionCategory_Legendre, {} );
#elif 0
  order = 1;
  QField2D_DG_BoundaryEdge<PDEClass> mufldTest( xfld, order, BasisFunctionCategory_Legendre );
#else
  order = 1;
  std::vector<std::vector<int>> boundaryGroupSets = {{0},{1},{2},{3}};
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> mufldTest(boundaryGroupSets, xfld, order, BasisFunctionCategory_Hierarchical );
#endif
  mufldTest = 0;

  // Set the lifting operator field
  for (int i = 0; i < nDOFLO/D; i++)
    for (int d = 0; d < D; d++)
      sfldTest.DOF(i)[d] = sln[0][D*i+d];

  for (int i = 0; i < nDOFPDE; i++)
    wfldTest.DOF(i) = sln[1][i];

  for (int i = 0; i < nDOFBC; i++)
    mufldTest.DOF(i) = sln[2][i];

/*
  std::fstream jacfile("tmp/jacobian.mtx", std::ios::out);
  WriteMatrixMarketFile( jac, jacfile );

  std::fstream jacTfile("tmp/jacobianT.mtx", std::ios::out);
  WriteMatrixMarketFile( jacT, jacTfile );
*/

  // Compute the lifting operator adjoint via static condensation

  const FieldDataInvMassMatrix_Cell mmfld(qfld); // Inverse mass matrix field

  FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld); // Lifting operator jacobian from cell integral

  // PDE jacobian wrt R
  IntegrateCellGroups<TopoD2>::integrate(
      JacobianCell_DGBR2_LiftingOperator(fcnCell, jacPDE_R),
      xfld, (qfld, rfld), quadratureOrder, xfld.nCellGroups() );

  // Lifting operators adjoint via static condensation
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> sfld(xfld, order, BasisFunctionCategory_Legendre);
  sfld = 0;

  IntegrateBoundaryTraceGroups<TopoD2>::integrate(
      JacobianFunctionalBoundaryTrace_DGBR2<SurrealClass>( output_integrand, fcnBCwall, rhs[1] ),
      xfld, (qfld, rfld, sfld), quadratureOrder, xfld.nBoundaryTraceGroups() );

  IntegrateInteriorTraceGroups<TopoD2>::integrate(SetFieldInteriorTrace_DGBR2_AdjointLO(fcnTrace, mmfld, jacPDE_R),
                                                  xfld, (qfld, rfld, wfldTest, sfld), quadratureOrder, xfld.nInteriorTraceGroups());

  IntegrateBoundaryTraceGroups<TopoD2>::integrate(SetFieldBoundaryTrace_DGBR2_AdjointLO<SurrealClass>(fcnBCwall, jacPDE_R),
                                                  xfld, (qfld, rfld, wfldTest, sfld), quadratureOrder, xfld.nBoundaryTraceGroups());

  IntegrateBoundaryTraceGroups<TopoD2>::integrate(SetFieldBoundaryTrace_DGBR2_AdjointLO<SurrealClass>(fcnBCchar, jacPDE_R),
                                                  xfld, (qfld, rfld, wfldTest, sfld), quadratureOrder, xfld.nBoundaryTraceGroups());

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-10;

  for ( int i = 0; i < sfld.nDOF(); i++ )
  {
    for (int d = 0; d < D; d++)
    {
      for (int m = 0; m < ArrayQ::M; m++)
      {
        //std::cout << "[" << i << "][" << d << "][" << m << "] " << sfldTest.DOF(i)[d][m] << " == " << sfld.DOF(i)[d][m] << std::endl;
        //Check that the numbers are the same
        SANS_CHECK_CLOSE( sfldTest.DOF(i)[d][m], sfld.DOF(i)[d][m], small_tol, close_tol );
      }
    }
  }
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
