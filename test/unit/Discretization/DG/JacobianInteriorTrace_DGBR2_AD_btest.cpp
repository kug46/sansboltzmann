// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianInteriorTrace_DGBR2_AD_btest
// testing of interior trace jacobian: advection-diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

#include "Discretization/DG/DiscretizationDGBR2.h"
#include "Discretization/DG/IntegrandInteriorTrace_DGBR2.h"
#include "Discretization/DG/SetFieldInteriorTrace_DGBR2_LiftingOperator.h"
#include "Discretization/DG/ResidualInteriorTrace_DGBR2.h"
#include "Discretization/DG/JacobianInteriorTrace_DGBR2.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegratePeriodicTraceGroups.h"

#include "Discretization/DG/IntegrandCell_DGBR2.h"
#include "Discretization/DG/JacobianCell_DGBR2.h"
#include "Discretization/DG/ResidualCell_DGBR2.h"
#include "Discretization/IntegrateCellGroups.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"
#include "unit/UnitGrids/XField3D_Box_Hex_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianInteriorTrace_DGBR2_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_1D_Line_Line_X1_SourceUniform )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,PhysD1::D,MatrixQ> RowMatrixQ;

  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;

  Real step = 1.0;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.1; // No gradient in the source term
  Source1D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // grid: X1
  XField1D xfld(4,0,4);

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcn( pde, disc, {0} );

  // sequence of solution orders
  for (int qorder = 0; qorder <= 3; qorder++)
  {
    Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

    const int nDOF = qfld.nDOF();

    // solution data
    for (int i = 0; i < nDOF; i++)
      qfld.DOF(i) = sin(PI*i/((Real)nDOF));

    // lifting-operator: P1
    FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Legendre);

    // Compute the field of inverse mass matrices
    FieldDataInvMassMatrix_Cell mmfld(qfld);

    // quadrature rule (scalar value at node)
    std::vector<int> quadratureOrder = {0};

    // jacobian via FD w/ residual operator; assumes scalar PDE

    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(nDOF), rsdPDEGlobal1(nDOF);
    DLA::MatrixD<MatrixQ> jacPDE_q(nDOF,nDOF);

    // Compute the lifting operator field
    IntegrateInteriorTraceGroups<TopoD1>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    rsdPDEGlobal0 = 0;
    IntegrateInteriorTraceGroups<TopoD1>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal0),
                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    // wrt q
    for (int j = 0; j < nDOF; j++)
    {
      qfld.DOF(j) += step;

      // Compute the lifting operator field with perturbed q field
      IntegrateInteriorTraceGroups<TopoD1>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

      // Compute perturbed residual
      rsdPDEGlobal1 = 0;
      IntegrateInteriorTraceGroups<TopoD1>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal1),
                                                      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

      qfld.DOF(j) -= step;

      for (int i = 0; i < nDOF; i++)
        jacPDE_q(i,j) = (rsdPDEGlobal1[i] - rsdPDEGlobal0[i])/step;
    }

    // jacobian via Surreal

    FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld); jacPDE_R = 0; // Lifitng operator jacobian from cell integral

    DLA::MatrixD<MatrixQ> mtxPDEGlob_q(nDOF,nDOF);

    mtxPDEGlob_q = 0;

    IntegrateInteriorTraceGroups<TopoD1>::integrate(
        JacobianInteriorTrace_DGBR2(fcn, mmfld, jacPDE_R, mtxPDEGlob_q),
        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

  #if 0
    cout << "Surreal: jacobian = " << endl;
    mtxGlob.dump(2);
  #endif

    const Real small_tol = 1e-10;
    const Real close_tol = 1e-10;

  #if 0
    std::cout << "jacPDE_q = " << std::endl;
    std::cout << jacPDE_q << std::endl;
    std::cout << "mtxPDEGlob_q = " << std::endl;
    std::cout << mtxPDEGlob_q << std::endl;
  #endif

    for (int i = 0; i < nDOF; i++)
      for (int j = 0; j < nDOF; j++)
        SANS_CHECK_CLOSE( jacPDE_q(i,j), mtxPDEGlob_q(i,j), small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_1D_Line_Line_X1_SourceGrad )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,PhysD1::D,MatrixQ> RowMatrixQ;

  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;
  typedef IntegrandCell_DGBR2<PDEClass> IntegrandCellClass;

  Real step = 1.0;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.1, b = 5.6; // With a gradient in the source
  Source1D_UniformGrad source(a,b);

  PDEClass pde( adv, visc, source );

  // grid: X1
  XField1D xfld(4,0,4);

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcn( pde, disc, {0} );
  IntegrandCellClass fcnCell( pde, disc, {0} );

  // sequence of solution orders
  for (int qorder = 0; qorder <= 0; qorder++) //3
  {
    Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

    const int qDOF = qfld.nDOF();

    // solution data
    for (int i = 0; i < qDOF; i++)
      qfld.DOF(i) = sin(PI*i/((Real)qDOF));

    // lifting-operator: P1
    FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Legendre);

    // Compute the field of inverse mass matrices
    FieldDataInvMassMatrix_Cell mmfld(qfld);

    // quadrature rule (scalar value at node)
    std::vector<int> quadratureOrder = {0};

    // jacobian via FD w/ residual operator; assumes scalar PDE

    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
    DLA::MatrixD<Real> jacPDE_q(qDOF,qDOF);

    // Compute the lifting operator field
    IntegrateInteriorTraceGroups<TopoD1>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    rsdPDEGlobal0 = 0;

    IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnCell, rsdPDEGlobal0),
                                            xfld, (qfld, rfld), quadratureOrder.data(), 1 );


    IntegrateInteriorTraceGroups<TopoD1>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal0),
                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    // wrt q
    for (int j = 0; j < qDOF; j++)
    {
      qfld.DOF(j) += step;

      // Compute the lifting operator field with perturbed q field
      IntegrateInteriorTraceGroups<TopoD1>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

      // Compute perturbed residual
      rsdPDEGlobal1 = 0;
      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcnCell, rsdPDEGlobal1),
                                              xfld, (qfld, rfld), quadratureOrder.data(), 1 );


      IntegrateInteriorTraceGroups<TopoD1>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal1),
                                                      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

      qfld.DOF(j) -= step;

      for (int i = 0; i < qDOF; i++)
        jacPDE_q(i,j) = (rsdPDEGlobal1[i] - rsdPDEGlobal0[i])/step;
    }

    // jacobian via Surreal

    FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld); jacPDE_R = 0; // Lifitng operator jacobian from cell integral

    DLA::MatrixD<MatrixQ> mtxPDEGlob_q(qDOF,qDOF);
    mtxPDEGlob_q = 0;

    IntegrateCellGroups<TopoD1>::integrate( JacobianCell_DGBR2(fcnCell, mtxPDEGlob_q, jacPDE_R),
                                            xfld, (qfld, rfld), quadratureOrder.data(), 1 );

    IntegrateInteriorTraceGroups<TopoD1>::integrate(
        JacobianInteriorTrace_DGBR2(fcn, mmfld, jacPDE_R, mtxPDEGlob_q),
        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

  #if 0
    cout << "Surreal: jacobian = " << endl;
    mtxGlob.dump(2);
  #endif

    const Real small_tol = 1e-10;
    const Real close_tol = 1e-10;

  #if 0
    std::cout << "jacPDE_q = " << std::endl;
    std::cout << jacPDE_q << std::endl;
    std::cout << "mtxPDEGlob_q = " << std::endl;
    std::cout << mtxPDEGlob_q << std::endl;
  #endif

    for (int i = 0; i < qDOF; i++)
      for (int j = 0; j < qDOF; j++)
        SANS_CHECK_CLOSE( jacPDE_q(i,j), mtxPDEGlob_q(i,j), small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_2D_Box_UnionJack_Triangle_X1_SourceUniform )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform> PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,PhysD2::D,MatrixQ> RowMatrixQ;

  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;

  Real step = 1.0;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.348;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy,
                             kyx, kyy);

  Real a = 2.1; // No source gradient
  Source2D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // grid: X1
  XField2D_Box_UnionJack_Triangle_X1 xfld(2, 2);

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcn( pde, disc, {0} );

  // solution: P1
  for (int qorder = 1; qorder <= 1; qorder++)
  {
    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

    const int qDOF = qfld.nDOF();

    // solution data
    for (int i = 0; i < qDOF; i++)
      qfld.DOF(i) = sin(PI*i/((Real)qDOF));

    // lifting-operator
    FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

    // Compute the field of inverse mass matrices
    FieldDataInvMassMatrix_Cell mmfld(qfld);

    // quadrature rule (2*P for basis and flux polynomials)
    std::vector<int> quadratureOrder = {2*qorder, 2*qorder, 2*qorder};

    // jacobian via FD w/ residual operator; assumes scalar PDE

    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
    DLA::MatrixD<Real> jacPDE_q(qDOF,qDOF);

    // Compute the lifting operator field
    IntegrateInteriorTraceGroups<TopoD2>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    rsdPDEGlobal0 = 0;
    IntegrateInteriorTraceGroups<TopoD2>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal0),
                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    // wrt q
    for (int j = 0; j < qDOF; j++)
    {
      qfld.DOF(j) += step;

      // Compute the lifting operator field with perturbed q field
      IntegrateInteriorTraceGroups<TopoD2>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

      // Compute perturbed residual
      rsdPDEGlobal1 = 0;
      IntegrateInteriorTraceGroups<TopoD2>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal1),
                                                      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

      qfld.DOF(j) -= step;

      for (int i = 0; i < qDOF; i++)
        jacPDE_q(i,j) = (rsdPDEGlobal1[i] - rsdPDEGlobal0[i])/step;
    }

    // jacobian via Surreal

    FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld); jacPDE_R = 0; // Lifitng operator jacobian from cell integral

    DLA::MatrixD<MatrixQ> mtxPDEGlob_q(qDOF,qDOF);

    mtxPDEGlob_q = 0;
    IntegrateInteriorTraceGroups<TopoD2>::integrate(
        JacobianInteriorTrace_DGBR2(fcn, mmfld, jacPDE_R, mtxPDEGlob_q),
        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    const Real small_tol = 1e-10;
    const Real close_tol = 1e-10;

    for (int i = 0; i < qDOF; i++)
      for (int j = 0; j < qDOF; j++)
        SANS_CHECK_CLOSE( jacPDE_q(i,j), mtxPDEGlob_q(i,j), small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_2D_Box_UnionJack_Triangle_X1_SourceGrad )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad> PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,PhysD2::D,MatrixQ> RowMatrixQ;

  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;
  typedef IntegrandCell_DGBR2<PDEClass> IntegrandCellClass;

  Real step = 1.0;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.348;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy,
                             kyx, kyy);

  Real a = 2.1, b = 5.6, c = -0.34; // With source gradient
  Source2D_UniformGrad source(a,b,c);

  PDEClass pde( adv, visc, source );

  // grid: X1
  XField2D_Box_UnionJack_Triangle_X1 xfld(2, 2);

  // solution: P1
  for (int qorder = 1; qorder <= 3; qorder++)
  {
    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

    const int qDOF = qfld.nDOF();

    // solution data
    for (int i = 0; i < qDOF; i++)
      qfld.DOF(i) = sin(PI*i/((Real)qDOF));

    // lifting-operator: P1
    FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

    // Compute the field of inverse mass matrices
    FieldDataInvMassMatrix_Cell mmfld(qfld);

    // BR2 discretization
    Real viscousEtaParameter = 6;
    DiscretizationDGBR2 disc(0, viscousEtaParameter);

    // integrand
    IntegrandClass fcn( pde, disc, {0} );
    IntegrandCellClass fcnCell( pde, disc, {0} );

    // quadrature rule (2*P for basis and flux polynomials)
    std::vector<int> quadratureOrder = {2*qorder, 2*qorder, 2*qorder};

    // jacobian via FD w/ residual operator; assumes scalar PDE

    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
    DLA::MatrixD<Real> jacPDE_q(qDOF,qDOF);

    // Compute the lifting operator field
    IntegrateInteriorTraceGroups<TopoD2>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    rsdPDEGlobal0 = 0;
    IntegrateCellGroups<TopoD2>::integrate( ResidualCell_DGBR2(fcnCell, rsdPDEGlobal0),
                                            xfld, (qfld, rfld), quadratureOrder.data(), 1 );

    IntegrateInteriorTraceGroups<TopoD2>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal0),
                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    // wrt q
    for (int j = 0; j < qDOF; j++)
    {
      qfld.DOF(j) += step;

      // Compute the lifting operator field with perturbed q field
      IntegrateInteriorTraceGroups<TopoD2>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

      // Compute perturbed residual
      rsdPDEGlobal1 = 0;
      IntegrateCellGroups<TopoD2>::integrate( ResidualCell_DGBR2(fcnCell, rsdPDEGlobal1),
                                              xfld, (qfld, rfld), quadratureOrder.data(), 1 );

      IntegrateInteriorTraceGroups<TopoD2>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal1),
                                                      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

      qfld.DOF(j) -= step;

      for (int i = 0; i < qDOF; i++)
        jacPDE_q(i,j) = (rsdPDEGlobal1[i] - rsdPDEGlobal0[i])/step;
    }

    // jacobian via Surreal

    FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld); jacPDE_R = 0; // Lifitng operator jacobian from cell integral

    DLA::MatrixD<MatrixQ> mtxPDEGlob_q(qDOF,qDOF);
    mtxPDEGlob_q = 0;

    IntegrateCellGroups<TopoD2>::integrate( JacobianCell_DGBR2(fcnCell, mtxPDEGlob_q, jacPDE_R),
                                            xfld, (qfld, rfld), quadratureOrder.data(), 1 );

    IntegrateInteriorTraceGroups<TopoD2>::integrate(
        JacobianInteriorTrace_DGBR2(fcn, mmfld, jacPDE_R, mtxPDEGlob_q),
        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    const Real small_tol = 1e-10;
    const Real close_tol = 5e-10;

    for (int i = 0; i < qDOF; i++)
      for (int j = 0; j < qDOF; j++)
        SANS_CHECK_CLOSE( jacPDE_q(i,j), mtxPDEGlob_q(i,j), small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_3D_Tet_Tet_X1_SourceUniform )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_Uniform > PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,PhysD3::D,MatrixQ> RowMatrixQ;

  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.7;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123; Real kxy = 0.553; Real kxz = 0.760;
  Real kyx = 0.343; Real kyy = 1.007; Real kyz = 0.870;
  Real kzx = 0.690; Real kzy = 0.365; Real kzz = 1.460;

  ViscousFlux3D_Uniform visc(kxx, kxy, kxz,
                             kyx, kyy, kyz,
                             kzx, kzy, kzz);

  Real a = 2.1; // no source gradient
  Source3D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // grid: X1
  XField3D_Box_Tet_X1 xfld(comm, 2, 2, 2);

  // solution: P1
  for ( int qorder = 1; qorder <= 2; qorder++)
  {
    Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

    const int qDOF = qfld.nDOF();

    // solution data
    for (int i = 0; i < qDOF; i++)
      qfld.DOF(i) = sin(PI*i/((Real)qDOF));

    // lifting operator: P1
    FieldLift_DG_Cell<PhysD3, TopoD3, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

    // Compute the field of inverse mass matrices
    FieldDataInvMassMatrix_Cell mmfld(qfld);

    // BR2 discretization
    Real viscousEtaParameter = 6;
    DiscretizationDGBR2 disc(0, viscousEtaParameter);

    // integrand
    IntegrandClass fcn( pde, disc, {0} );

    // quadrature rule (2*P for basis and flux polynomials)
    std::vector<int> quadratureOrder = {2*qorder};

    // jacobian via FD w/ residual operator; assumes scalar PDE

    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
    DLA::MatrixD<Real> jacPDE_q(qDOF,qDOF);

    // Compute the lifting operator field
    IntegrateInteriorTraceGroups<TopoD3>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    rsdPDEGlobal0 = 0;
    IntegrateInteriorTraceGroups<TopoD3>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal0),
                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    //wrt q
    for (int j = 0; j < qDOF; j++)
    {
      qfld.DOF(j) += 1;

      // Compute the lifting operator field with perturbed q field
      IntegrateInteriorTraceGroups<TopoD3>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

      rsdPDEGlobal1 = 0;
      IntegrateInteriorTraceGroups<TopoD3>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal1),
                                                      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

      qfld.DOF(j) -= 1;

      for (int i = 0; i < qDOF; i++)
        jacPDE_q(i,j) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    }

    // jacobian via Surreal

    FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld); jacPDE_R = 0; // Lifitng operator jacobian from cell integral

    DLA::MatrixD<MatrixQ> mtxPDEGlob_q(qDOF,qDOF);

    mtxPDEGlob_q = 0;

    IntegrateInteriorTraceGroups<TopoD3>::integrate(
        JacobianInteriorTrace_DGBR2(fcn, mmfld, jacPDE_R, mtxPDEGlob_q),
        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    const Real small_tol = 5e-11;
    const Real close_tol = 5e-11;

    for (int i = 0; i < qDOF; i++)
      for (int j = 0; j < qDOF; j++)
        SANS_CHECK_CLOSE( jacPDE_q(i,j), mtxPDEGlob_q(i,j), small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_3D_Tet_Tet_X1_SourceGrad )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_UniformGrad > PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,PhysD3::D,MatrixQ> RowMatrixQ;

  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;
  typedef IntegrandCell_DGBR2<PDEClass> IntegrandCellClass;

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.7;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123; Real kxy = 0.553; Real kxz = 0.760;
  Real kyx = 0.343; Real kyy = 1.007; Real kyz = 0.870;
  Real kzx = 0.690; Real kzy = 0.365; Real kzz = 1.460;

  ViscousFlux3D_Uniform visc(kxx, kxy, kxz,
                             kyx, kyy, kyz,
                             kzx, kzy, kzz);

  Real a = 2.1, b = 5.6, c = -0.34, d = 0.67; // With source gradient
  Source3D_UniformGrad source(a,b,c,d);

  PDEClass pde( adv, visc, source );

  // grid: X1
  XField3D_Box_Tet_X1 xfld(comm, 2, 2, 2);

  // solution: P1
  for ( int qorder = 1; qorder <= 2; qorder++)
  {
    Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

    const int qDOF = qfld.nDOF();

    // solution data
    for (int i = 0; i < qDOF; i++)
      qfld.DOF(i) = sin(PI*i/((Real)qDOF));

    // lifting operator: P1
    FieldLift_DG_Cell<PhysD3, TopoD3, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

    // Compute the field of inverse mass matrices
    FieldDataInvMassMatrix_Cell mmfld(qfld);

    // BR2 discretization
    Real viscousEtaParameter = 6;
    DiscretizationDGBR2 disc(0, viscousEtaParameter);

    // integrand
    IntegrandClass fcn( pde, disc, {0} );
    IntegrandCellClass fcnCell( pde, disc, {0} );

    // quadrature rule (2*P for basis and flux polynomials)
    std::vector<int> quadratureOrder = {2*qorder};

    // jacobian via FD w/ residual operator; assumes scalar PDE

    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
    DLA::MatrixD<Real> jacPDE_q(qDOF,qDOF);

    // Compute the lifting operator field
    IntegrateInteriorTraceGroups<TopoD3>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    rsdPDEGlobal0 = 0;
    IntegrateCellGroups<TopoD3>::integrate( ResidualCell_DGBR2(fcnCell, rsdPDEGlobal0),
                                            xfld, (qfld, rfld), quadratureOrder.data(), 1 );

    IntegrateInteriorTraceGroups<TopoD3>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal0),
                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    //wrt q
    for (int j = 0; j < qDOF; j++)
    {
      qfld.DOF(j) += 1;

      // Compute the lifting operator field with perturbed q field
      IntegrateInteriorTraceGroups<TopoD3>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

      rsdPDEGlobal1 = 0;
      IntegrateCellGroups<TopoD3>::integrate( ResidualCell_DGBR2(fcnCell, rsdPDEGlobal1),
                                              xfld, (qfld, rfld), quadratureOrder.data(), 1 );

      IntegrateInteriorTraceGroups<TopoD3>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal1),
                                                      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

      qfld.DOF(j) -= 1;

      for (int i = 0; i < qDOF; i++)
        jacPDE_q(i,j) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    }

    // jacobian via Surreal

    FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld); jacPDE_R = 0; // Lifitng operator jacobian from cell integral

    DLA::MatrixD<MatrixQ> mtxPDEGlob_q(qDOF,qDOF);

    mtxPDEGlob_q = 0;
    IntegrateCellGroups<TopoD3>::integrate( JacobianCell_DGBR2(fcnCell, mtxPDEGlob_q, jacPDE_R),
                                            xfld, (qfld, rfld), quadratureOrder.data(), 1 );

    IntegrateInteriorTraceGroups<TopoD3>::integrate(
        JacobianInteriorTrace_DGBR2(fcn, mmfld, jacPDE_R, mtxPDEGlob_q),
        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    const Real small_tol = 1e-10;
    const Real close_tol = 6e-10;

    for (int i = 0; i < qDOF; i++)
      for (int j = 0; j < qDOF; j++)
        SANS_CHECK_CLOSE( jacPDE_q(i,j), mtxPDEGlob_q(i,j), small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_3D_Hex_Periodic_X1_SourceUniform )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_Uniform > PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,PhysD3::D,MatrixQ> RowMatrixQ;

  typedef IntegrandInteriorTrace_DGBR2<PDEClass> IntegrandClass;

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.7;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123; Real kxy = 0.553; Real kxz = 0.760;
  Real kyx = 0.343; Real kyy = 1.007; Real kyz = 0.870;
  Real kzx = 0.690; Real kzy = 0.365; Real kzz = 1.460;

  ViscousFlux3D_Uniform visc(kxx, kxy, kxz,
                             kyx, kyy, kyz,
                             kzx, kzy, kzz);

  Real a = 2.1; // no source gradient
  Source3D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // grid: X1
  XField3D_Box_Hex_X1 xfld(comm, 3, 3, 3, {{true, true, true}});

  // solution: P1
  for ( int qorder = 1; qorder < 2; qorder++)
  {
    Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

    const int qDOF = qfld.nDOF();

    // solution data
    for (int i = 0; i < qDOF; i++)
      qfld.DOF(i) = sin(PI*i/((Real)qDOF));

    // lifting operator: P1
    FieldLift_DG_Cell<PhysD3, TopoD3, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

    // Compute the field of inverse mass matrices
    FieldDataInvMassMatrix_Cell mmfld(qfld);

    // BR2 discretization
    Real viscousEtaParameter = Hex::NTrace;
    DiscretizationDGBR2 disc(0, viscousEtaParameter);

    // integrand
    IntegrandClass fcn( pde, disc, {0} );

    // quadrature rule (2*P for basis and flux polynomials)
    std::vector<int> quadratureOrder = {2*qorder, 2*qorder, 2*qorder, 2*qorder, 2*qorder, 2*qorder};

    // jacobian via FD w/ residual operator; assumes scalar PDE

    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
    DLA::MatrixD<Real> jacPDE_q(qDOF,qDOF);

    // Compute the lifting operator field
    IntegratePeriodicTraceGroups<TopoD3>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                    xfld, (qfld, rfld), quadratureOrder.data(),quadratureOrder.size());

    rsdPDEGlobal0 = 0;
    IntegratePeriodicTraceGroups<TopoD3>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal0),
                                                    xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    //wrt q
    for (int j = 0; j < qDOF; j++)
    {
      qfld.DOF(j) += 1;

      // Compute the lifting operator field with perturbed q field
      IntegratePeriodicTraceGroups<TopoD3>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
                                                      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

      rsdPDEGlobal1 = 0;
      IntegratePeriodicTraceGroups<TopoD3>::integrate(ResidualInteriorTrace_DGBR2(fcn, rsdPDEGlobal1),
                                                      xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

      qfld.DOF(j) -= 1;

      for (int i = 0; i < qDOF; i++)
        jacPDE_q(i,j) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    }

    // jacobian via Surreal

    FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld); jacPDE_R = 0; // Lifitng operator jacobian from cell integral

    DLA::MatrixD<MatrixQ> mtxPDEGlob_q(qDOF,qDOF);

    mtxPDEGlob_q = 0;

    IntegratePeriodicTraceGroups<TopoD3>::integrate(
        JacobianInteriorTrace_DGBR2(fcn, mmfld, jacPDE_R, mtxPDEGlob_q),
        xfld, (qfld, rfld), quadratureOrder.data(), quadratureOrder.size());

    const Real small_tol = 5e-11;
    const Real close_tol = 5e-11;

    for (int i = 0; i < qDOF; i++)
      for (int j = 0; j < qDOF; j++)
        SANS_CHECK_CLOSE( jacPDE_q(i,j), mtxPDEGlob_q(i,j), small_tol, close_tol );
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
