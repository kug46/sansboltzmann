// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// SolutionData_DGBR2_Block2_btest
// testing of the SolutionData_DGBR2_Block2 class

#include <boost/test/unit_test.hpp>

#include "Discretization/DG/SolutionData_DGBR2_Block2.h"
#include "SANS_btest.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion_ArtificialViscosity2D.h"

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Sensor.h"
#include "pde/Sensor/Source2D_JumpSensor.h"
#include "pde/Sensor/PDESensorParameter2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

using namespace std;


//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( SolutionData_DGBR2_Block2_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SolutionData_DGBR2_Block2_test )
{
  typedef AdvectiveFlux2D_Uniform AdvectiveFluxType;
  typedef ViscousFlux2D_Uniform ViscousFluxType;
  typedef Source2D_UniformGrad SourceType;

  typedef PDEAdvectionDiffusion_ArtificialViscosity<PhysD2, AdvectiveFluxType, ViscousFluxType, SourceType> PDEClass_Primal;
  typedef PDENDConvertSpace<PhysD2, PDEClass_Primal> NDPDEClass_Primal;

  typedef AdvectionDiffusion_Sensor Sensor;
  typedef Source2D_JumpSensor<Sensor> Source_JumpSensor;
  typedef Sensor_AdvectiveFlux2D_Uniform Sensor_Advection;
  typedef Sensor_ViscousFlux2D_GenHScale Sensor_Diffusion;

  typedef PDESensorParameter<PhysD2,
                             SensorParameterTraits<PhysD2>,
                             Sensor_Advection,
                             Sensor_Diffusion,
                             Source_JumpSensor > PDEClass_Sensor;
  typedef PDENDConvertSpace<PhysD2, PDEClass_Sensor> NDPDEClass_Sensor;

  typedef ParamType_ArtificialViscosity ParamBuilderType;

  typedef SolutionData_DGBR2_Block2<PhysD2, TopoD2, NDPDEClass_Primal, NDPDEClass_Sensor, ParamBuilderType> SolutionClass;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  int ii = 3;
  int jj = 3;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  int order_primal = 1;
  int order_sensor = 0;

  // Principal PDE
  Real a = 0.5;
  Real b = 1.0;
  Real nu = 0.0;
  AdvectiveFluxType adv( a, b );
  ViscousFluxType visc( nu, 0, 0, nu );
  SourceType source(0.0, 0.0, 0.0);

  // Field bundles need to be initialized with a disc now
  DiscretizationDGBR2 disc0( 6.0), disc1(3.0);

  //Create artificial viscosity PDE
  NDPDEClass_Primal pde_primal(adv, visc, source, order_primal);

  //Sensor PDE
  Sensor sensor(pde_primal);
  Sensor_Advection sensor_adv(0.0, 0.0);
  Sensor_Diffusion sensor_visc(1.0);
  Source_JumpSensor sensor_source(order_primal, sensor);

  NDPDEClass_Sensor pde_sensor(sensor_adv, sensor_visc, sensor_source);

  SolutionClass globalSol(xfld, pde_primal, pde_sensor,
                          order_primal, order_sensor, order_primal+1, order_sensor+1,
                          BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Legendre,
                          BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Legendre,
                          {}, {}, disc0, disc1);

  BOOST_CHECK_EQUAL( &globalSol.pde0, &pde_primal);
  BOOST_CHECK_EQUAL( &globalSol.pde1, &pde_sensor);

  BOOST_CHECK_EQUAL( globalSol.primal0.order, order_primal);
  BOOST_CHECK_EQUAL( globalSol.primal1.order, order_sensor);

  BOOST_CHECK_EQUAL( globalSol.primal0.basis_cell, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( globalSol.primal0.basis_trace, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( globalSol.primal1.basis_cell, BasisFunctionCategory_Legendre);
  BOOST_CHECK_EQUAL( globalSol.primal1.basis_trace, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( &globalSol.primal0.qfld.getXField(), &xfld);
  BOOST_CHECK_EQUAL( &globalSol.primal0.rfld.getXField(), &xfld);
  BOOST_CHECK_EQUAL( &globalSol.primal0.lgfld.getXField(), &xfld);

  BOOST_CHECK_EQUAL( &globalSol.primal1.qfld.getXField(), &xfld);
  BOOST_CHECK_EQUAL( &globalSol.primal1.rfld.getXField(), &xfld);
  BOOST_CHECK_EQUAL( &globalSol.primal1.lgfld.getXField(), &xfld);

  BOOST_CHECK_EQUAL( &globalSol.adjoint0.qfld.getXField(), &xfld);
  BOOST_CHECK_EQUAL( &globalSol.adjoint0.rfld.getXField(), &xfld);
  BOOST_CHECK_EQUAL( &globalSol.adjoint0.lgfld.getXField(), &xfld);

  BOOST_CHECK_EQUAL( &globalSol.adjoint1.qfld.getXField(), &xfld);
  BOOST_CHECK_EQUAL( &globalSol.adjoint1.rfld.getXField(), &xfld);
  BOOST_CHECK_EQUAL( &globalSol.adjoint1.lgfld.getXField(), &xfld);

  BOOST_CHECK_EQUAL( globalSol.primal0.qfld.nDOF(), 2*ii*jj*3);
  BOOST_CHECK_EQUAL( globalSol.primal0.rfld.nDOF(), 2*ii*jj*Triangle::NTrace*3);
  BOOST_CHECK_EQUAL( globalSol.primal0.lgfld.nDOF(), 0);

  BOOST_CHECK_EQUAL( globalSol.primal1.qfld.nDOF(), 2*ii*jj*1);
  BOOST_CHECK_EQUAL( globalSol.primal1.rfld.nDOF(), 2*ii*jj*Triangle::NTrace*1);
  BOOST_CHECK_EQUAL( globalSol.primal1.lgfld.nDOF(), 0);

  BOOST_CHECK_EQUAL( globalSol.adjoint0.qfld.nDOF(), 2*ii*jj*6);
  BOOST_CHECK_EQUAL( globalSol.adjoint0.rfld.nDOF(), 2*ii*jj*Triangle::NTrace*6);
  BOOST_CHECK_EQUAL( globalSol.adjoint0.lgfld.nDOF(), 0);

  BOOST_CHECK_EQUAL( globalSol.adjoint1.qfld.nDOF(), 2*ii*jj*3);
  BOOST_CHECK_EQUAL( globalSol.adjoint1.rfld.nDOF(), 2*ii*jj*Triangle::NTrace*3);
  BOOST_CHECK_EQUAL( globalSol.adjoint1.lgfld.nDOF(), 0);

  globalSol.setSolution(0.1, 0.2);

  for (int i = 0; i < globalSol.primal0.qfld.nDOF(); i++)
    SANS_CHECK_CLOSE( globalSol.primal0.qfld.DOF(i), 0.1, small_tol, close_tol);

  for (int i = 0; i < globalSol.primal1.qfld.nDOF(); i++)
    SANS_CHECK_CLOSE( globalSol.primal1.qfld.DOF(i), 0.2, small_tol, close_tol);


  SolutionClass globalSolFrom(xfld, pde_primal, pde_sensor,
                              order_primal, order_sensor, order_primal+1, order_sensor+1,
                              BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Legendre,
                              BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Legendre,
                              {}, {}, disc0, disc1);

  globalSolFrom.setSolution(0.3, 0.5);

  globalSol.setSolution(globalSolFrom);

  for (int i = 0; i < globalSol.primal0.qfld.nDOF(); i++)
    SANS_CHECK_CLOSE( globalSol.primal0.qfld.DOF(i), 0.3, small_tol, close_tol);

  for (int i = 0; i < globalSol.primal1.qfld.nDOF(); i++)
    SANS_CHECK_CLOSE( globalSol.primal1.qfld.DOF(i), 0.5, small_tol, close_tol);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
