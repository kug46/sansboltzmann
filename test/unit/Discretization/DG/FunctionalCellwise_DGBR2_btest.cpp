// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// FunctionalCellwise_DGBR2_btest
// testing of cellwise functionals

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldVolume_DG_Cell.h"

#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AnalyticFunction/ScalarFunction3D.h"

#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "pde/OutputCell_WeightedSolution.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"
#include "Discretization/DG/FunctionalCellwise_DGBR2.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Discretization/IntegrateCellGroups.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( FunctionalCellwise_DGBR2_test_suite )

template<class PhysDim_>
struct DummyPDE
{
  typedef PhysDim_ PhysDim;
  template<class T>
  using ArrayQ = T; //DLA::VectorS<1,T>;

  template<class T>
  using MatrixQ = T; //DLA::MatrixS<1,1,T>;
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_P0_test )
{
  typedef ScalarFunction1D_Const WeightFcn; // Change this to change weighting type
  typedef OutputCell_WeightedSolution<DummyPDE<PhysD1>,WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandCell_DGBR2_Output<NDOutputClass> IntegrandClass;

  Real a0 = 1.4;
  WeightFcn weightFcn(a0);

  // grid:
  XField1D xfld(3);

  BOOST_CHECK_EQUAL( 4, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 3, xfld.nElem() );

  // solution:
  int qorder = 0;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
  BOOST_CHECK_EQUAL( 3, qfld.nDOF() );

  // solution data
  qfld.DOF(0) = 1.0;
  qfld.DOF(1) = 2.0;
  qfld.DOF(2) = 3.0;

  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Legendre);

  for (int i = 0; i < rfld.nDOF(); i++)
    rfld.DOF(i) = {sin((Real)i/((Real)rfld.nDOF())*PI)};

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder = 1;

  // integrand
  NDOutputClass fcnOutput(weightFcn);
  IntegrandClass integrand( fcnOutput, {0} );

  ArrayQ total_integral;

  total_integral = 0;
  IntegrateCellGroups<TopoD1>::integrate(
      FunctionalCell_DGBR2( integrand, total_integral ), xfld, (qfld, rfld), &quadratureOrder, 1 );

  std::vector<std::vector<ArrayQ>> cell_integrals(xfld.nCellGroups());
  IntegrateCellGroups<TopoD1>::integrate(
      FunctionalCellwise_DGBR2( integrand, cell_integrals ), xfld, (qfld, rfld), &quadratureOrder, 1 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( a0*(1.0)/3.0, cell_integrals[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( a0*(2.0)/3.0, cell_integrals[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( a0*(3.0)/3.0, cell_integrals[0][2], small_tol, close_tol );

  Real sum = 0.0;
  for (int i = 0; i < 3; i++)
    sum += cell_integrals[0][i];

  SANS_CHECK_CLOSE( sum, total_integral, small_tol, close_tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_P1_test )
{
  typedef ScalarFunction1D_Const WeightFcn; // Change this to change weighting type
  typedef OutputCell_WeightedSolution<DummyPDE<PhysD1>,WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandCell_DGBR2_Output<NDOutputClass> IntegrandClass;

  Real a0 = 1.4;
  WeightFcn weightFcn(a0);

  // grid:
  XField1D xfld(3);

  BOOST_CHECK_EQUAL( 4, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 3, xfld.nElem() );

  // solution:
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( 6, qfld.nDOF() );

  // solution data
  qfld.DOF(0) = 1.0;
  qfld.DOF(1) = 1.5;
  qfld.DOF(2) = 2.0;
  qfld.DOF(3) = 2.5;
  qfld.DOF(4) = 3.0;
  qfld.DOF(5) = 3.5;

  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  for (int i = 0; i < rfld.nDOF(); i++)
    rfld.DOF(i) = {sin((Real)i/((Real)rfld.nDOF())*PI)};

  // quadrature rule (linear: basis is linear, solution is linear)
  int quadratureOrder = 1;

  // integrand
  NDOutputClass fcnOutput(weightFcn);
  IntegrandClass integrand( fcnOutput, {0} );

  ArrayQ total_integral;

  total_integral = 0;
  IntegrateCellGroups<TopoD1>::integrate(
      FunctionalCell_DGBR2( integrand, total_integral ), xfld, (qfld, rfld), &quadratureOrder, 1 );

  std::vector<std::vector<ArrayQ>> cell_integrals(xfld.nCellGroups());
  IntegrateCellGroups<TopoD1>::integrate(
      FunctionalCellwise_DGBR2( integrand, cell_integrals ), xfld, (qfld, rfld), &quadratureOrder, 1 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( a0*(1.0+1.5)/6.0, cell_integrals[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( a0*(2.0+2.5)/6.0, cell_integrals[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( a0*(3.0+3.5)/6.0, cell_integrals[0][2], small_tol, close_tol );

  Real sum = 0.0;
  for (int i = 0; i < 3; i++)
    sum += cell_integrals[0][i];

  SANS_CHECK_CLOSE( sum, total_integral, small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_P0_test )
{
  typedef ScalarFunction2D_Linear WeightFcn; // Change this to change weighting type
  typedef OutputCell_WeightedSolution<DummyPDE<PhysD2>,WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandCell_DGBR2_Output<NDOutputClass> IntegrandClass;

  Real a0 = 1.4, a1 = 0.0, a2 = 0.0;
  WeightFcn weightFcn(a0,a1,a2);

  // grid:
  XField2D_2Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 4, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 2, xfld.nElem() );

  // solution:
  int qorder = 0;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
  BOOST_CHECK_EQUAL( 2, qfld.nDOF() );

  // solution data
  qfld.DOF(0) = 1.5;
  qfld.DOF(1) = 2.5;

  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Legendre);

  for (int i = 0; i < rfld.nDOF(); i++)
    rfld.DOF(i) = {sin((Real)i/((Real)rfld.nDOF())*PI), cos((Real)i/((Real)rfld.nDOF())*PI)};

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder = 1;

  // integrand
  NDOutputClass fcnOutput(weightFcn);
  IntegrandClass integrand( fcnOutput, {0} );

  ArrayQ total_integral;

  total_integral = 0;
  IntegrateCellGroups<TopoD2>::integrate(
      FunctionalCell_DGBR2( integrand, total_integral ), xfld, (qfld, rfld), &quadratureOrder, 1 );

  std::vector<std::vector<ArrayQ>> cell_integrals(xfld.nCellGroups());
  IntegrateCellGroups<TopoD2>::integrate(
      FunctionalCellwise_DGBR2( integrand, cell_integrals ), xfld, (qfld, rfld), &quadratureOrder, 1 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( a0*(1.5)/2.0, cell_integrals[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( a0*(2.5)/2.0, cell_integrals[0][1], small_tol, close_tol );

  Real sum = 0.0;
  for (int i = 0; i < 2; i++)
    sum += cell_integrals[0][i];

  SANS_CHECK_CLOSE( sum, total_integral, small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_P1_test )
{
  typedef ScalarFunction2D_Linear WeightFcn; // Change this to change weighting type
  typedef OutputCell_WeightedSolution<DummyPDE<PhysD2>,WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandCell_DGBR2_Output<NDOutputClass> IntegrandClass;

  Real a0 = 1.4, a1 = 0.0, a2 = 0.0;
  WeightFcn weightFcn(a0,a1,a2);

  // grid:
  XField2D_2Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 4, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 2, xfld.nElem() );

  // solution:
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( 6, qfld.nDOF() );

  // solution data
  qfld.DOF(0) = 1.5;
  qfld.DOF(1) = 2.5;
  qfld.DOF(2) = 3.5;
  qfld.DOF(3) = 4.4;
  qfld.DOF(4) = 5.5;
  qfld.DOF(5) = 6.6;

  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  for (int i = 0; i < rfld.nDOF(); i++)
    rfld.DOF(i) = {sin((Real)i/((Real)rfld.nDOF())*PI), cos((Real)i/((Real)rfld.nDOF())*PI)};

  // quadrature rule (linear: basis is linear, solution is linear)
  int quadratureOrder = 1;

  // integrand
  NDOutputClass fcnOutput(weightFcn);
  IntegrandClass integrand( fcnOutput, {0} );

  ArrayQ total_integral;

  total_integral = 0;
  IntegrateCellGroups<TopoD2>::integrate(
      FunctionalCell_DGBR2( integrand, total_integral ), xfld, (qfld, rfld), &quadratureOrder, 1 );

  std::vector<std::vector<ArrayQ>> cell_integrals(xfld.nCellGroups());
  IntegrateCellGroups<TopoD2>::integrate(
      FunctionalCellwise_DGBR2( integrand, cell_integrals ), xfld, (qfld, rfld), &quadratureOrder, 1 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( a0*(1.5+2.5+3.5)/6.0, cell_integrals[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( a0*(4.4+5.5+6.6)/6.0, cell_integrals[0][1], small_tol, close_tol );

  Real sum = 0.0;
  for (int i = 0; i < 2; i++)
    sum += cell_integrals[0][i];

  SANS_CHECK_CLOSE( sum, total_integral, small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Tet_P0_test )
{
  typedef ScalarFunction3D_Linear WeightFcn; // Change this to change weighting type
  typedef OutputCell_WeightedSolution<DummyPDE<PhysD3>,WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandCell_DGBR2_Output<NDOutputClass> IntegrandClass;

  Real a0 = 1.4, a1 = 0.0, a2 = 0.0, a3 = 0.0;
  WeightFcn weightFcn(a0,a1,a2,a3);

  // grid:
  XField3D_2Tet_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 5, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 2, xfld.nElem() );

  // solution:
  int qorder = 0;
  Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
  BOOST_CHECK_EQUAL( 2, qfld.nDOF() );

  // solution data
  qfld.DOF(0) = 1.5;
  qfld.DOF(1) = 2.5;

  FieldLift_DG_Cell<PhysD3, TopoD3, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Legendre);

  for (int i = 0; i < rfld.nDOF(); i++)
    rfld.DOF(i) = { sin((Real)i/((Real)rfld.nDOF())*PI),
                    cos((Real)i/((Real)rfld.nDOF())*PI),
                   -sin((Real)i/((Real)rfld.nDOF())*PI)};

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder = 1;

  // integrand
  NDOutputClass fcnOutput(weightFcn);
  IntegrandClass integrand( fcnOutput, {0} );

  ArrayQ total_integral;

  total_integral = 0;
  IntegrateCellGroups<TopoD3>::integrate(
      FunctionalCell_DGBR2( integrand, total_integral ), xfld, (qfld, rfld), &quadratureOrder, 1 );

  std::vector<std::vector<ArrayQ>> cell_integrals(xfld.nCellGroups());
  IntegrateCellGroups<TopoD3>::integrate(
      FunctionalCellwise_DGBR2( integrand, cell_integrals ), xfld, (qfld, rfld), &quadratureOrder, 1 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( a0*(1.5)/6.0, cell_integrals[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( a0*(2.5)/6.0, cell_integrals[0][1], small_tol, close_tol );

  Real sum = 0.0;
  for (int i = 0; i < 2; i++)
    sum += cell_integrals[0][i];

  SANS_CHECK_CLOSE( sum, total_integral, small_tol, close_tol );
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Tet_P1_test )
{
  typedef ScalarFunction3D_Linear WeightFcn; // Change this to change weighting type
  typedef OutputCell_WeightedSolution<DummyPDE<PhysD3>,WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandCell_DGBR2_Output<NDOutputClass> IntegrandClass;

  Real a0 = 1.4, a1 = 0.0, a2 = 0.0, a3 = 0.0;
  WeightFcn weightFcn(a0,a1,a2,a3);

  // grid:
  XField3D_2Tet_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 5, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 2, xfld.nElem() );

  // solution:
  int qorder = 1;
  Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( 8, qfld.nDOF() );

  // solution data
  qfld.DOF(0) = 1.5;
  qfld.DOF(1) = 2.5;
  qfld.DOF(2) = 3.5;
  qfld.DOF(3) = 4.5;
  qfld.DOF(4) = 5.5;
  qfld.DOF(5) = 6.5;
  qfld.DOF(6) = 7.5;
  qfld.DOF(7) = 8.5;

  FieldLift_DG_Cell<PhysD3, TopoD3, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  for (int i = 0; i < rfld.nDOF(); i++)
    rfld.DOF(i) = { sin((Real)i/((Real)rfld.nDOF())*PI),
                    cos((Real)i/((Real)rfld.nDOF())*PI),
                   -sin((Real)i/((Real)rfld.nDOF())*PI)};

  // quadrature rule (linear: basis is linear, solution is linear)
  int quadratureOrder = 1;

  // integrand
  NDOutputClass fcnOutput(weightFcn);
  IntegrandClass integrand( fcnOutput, {0} );

  ArrayQ total_integral;

  total_integral = 0;
  IntegrateCellGroups<TopoD3>::integrate(
      FunctionalCell_DGBR2( integrand, total_integral ), xfld, (qfld, rfld), &quadratureOrder, 1 );

  std::vector<std::vector<ArrayQ>> cell_integrals(xfld.nCellGroups());
  IntegrateCellGroups<TopoD3>::integrate(
      FunctionalCellwise_DGBR2( integrand, cell_integrals ), xfld, (qfld, rfld), &quadratureOrder, 1 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( a0*(1.5+2.5+3.5+4.5)/24.0, cell_integrals[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( a0*(5.5+6.5+7.5+8.5)/24.0, cell_integrals[0][1], small_tol, close_tol );

  Real sum = 0.0;
  for (int i = 0; i < 2; i++)
    sum += cell_integrals[0][i];

  SANS_CHECK_CLOSE( sum, total_integral, small_tol, close_tol );
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
