// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandBoundaryTrace_DGBR2_BuckleyLeverett_btest
// testing of boundary integrands: Buckley-Leverett

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverett.h"
#include "pde/PorousMedia/PDEBuckleyLeverett1D.h"
#include "pde/PorousMedia/BCBuckleyLeverett1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{

typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel_PowerLaw, CapillaryModel_Linear> TraitsModelClass;
typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEBL;

typedef PDENDConvertSpace<PhysD1, PDEBL> PDEClass1D;

typedef BCNDConvertSpace<PhysD1, BCBuckleyLeverett1D<BCTypeDirichlet_mitState, PDEBL> > BCClass1D;
typedef NDVectorCategory<boost::mpl::vector1<BCClass1D>, BCClass1D::Category> NDBCVecCat1D;

typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldLine;

// 1D
template class IntegrandBoundaryTrace<PDEClass1D, NDBCVecCat1D, DGBR2>
               ::BasisWeighted_PDE<Real, TopoD0, Node, TopoD1, Line, ElementXFieldLine>;

template class IntegrandBoundaryTrace<PDEClass1D, NDBCVecCat1D, DGBR2>
               ::BasisWeighted_LO<Real, Real, TopoD0, Node, TopoD1, Line, ElementXFieldLine>;

template class IntegrandBoundaryTrace<PDEClass1D, NDBCVecCat1D, DGBR2>
               ::FieldWeighted<Real, Real, TopoD0, Node, TopoD1, Line, ElementXFieldLine>;
}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandBoundaryTrace_Dirichlet_mitState_DGBR2_BuckleyLeverett_btest_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Line_X1Q1R1_test )
{
  typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel_PowerLaw, CapillaryModel_Linear> TraitsModelClass;
  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEBL;
  typedef PDENDConvertSpace<PhysD1, PDEBL> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD1::D,ArrayQ> VectorArrayQ;

  typedef BCNDConvertSpace<PhysD1, BCBuckleyLeverett1D<BCTypeDirichlet_mitState, PDEBL> > BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementRFieldClass;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO<Real,Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> BasisWeightedLOClass;

  // PDE
  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  const Real pcmax = 5.0;
  CapillaryModel_Linear cap_model(pcmax);

  PDEBL pdeBL(phi, uT, kr_model, mu_w, mu_n, K, cap_model);
  PDEClass pde(phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC
  Real SwB = 0.25;
  BCClass bc(pde, SwB);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent line grid
  Real x0, x1;

  x0 = 0;
  x1 = 1;

  xfldElem.DOF(0) = {x0};
  xfldElem.DOF(1) = {x1};

  xnode.DOF(0) = {x1};
  xnode.normalSignL() = 1;

  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // line solution (left)
  Real Sw0 = 0.4, Sw1 = 0.35;
  qfldElem.DOF(0) = Sw0;
  qfldElem.DOF(1) = Sw1;

  // lifting operators
  ElementRFieldClass rfldElem(order, BasisFunctionCategory_Hierarchical);

  rfldElem.DOF(0) = -1;  rfldElem.DOF(1) = 5;

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand functor

  IntegrandClass fcnint( pde, bc, {0}, disc );
  BOOST_CHECK( fcnint.needsFieldTrace() == false );

  BasisWeightedPDEClass fcnPDE = fcnint.integrand_PDE( xnode, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, rfldElem );
  BasisWeightedLOClass fcnLO = fcnint.integrand_LO( xnode, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, rfldElem );

  BOOST_CHECK_EQUAL( 2, fcnPDE.nDOF() );
  BOOST_CHECK( fcnPDE.needsEvaluation() == true );

  BOOST_CHECK_EQUAL( 2, fcnLO.nDOF() );
  BOOST_CHECK( fcnLO.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;
  Real integrandPDETrue[2];
  Real integrandLiftxTrue[2];
  BasisWeightedPDEClass::IntegrandType integrandPDE[2];
  BasisWeightedLOClass::IntegrandType integrandLO[2];

  ArrayQ q = Sw1;
  ArrayQ qB = SwB;
  ArrayQ dq = q - qB;
  ArrayQ qx = (Sw1 - Sw0) / (x1 - x0);
  ArrayQ qx_lifted = qx + viscousEtaParameter*rfldElem.DOF(1);

  Real t = 0;
  Real nx = 1.0;
  ArrayQ fAdvTrue = 0;
  pdeBL.fluxAdvectiveUpwind(x1, t, q, qB, nx, fAdvTrue);

  ArrayQ fViscTrue = 0;
  pdeBL.fluxViscous(x1, t, qB, qx_lifted, fViscTrue);
  fViscTrue *= nx;

  ArrayQ fViscDualTrue = 0; //dual consistency term
  pdeBL.fluxViscous(x1, t, qB, dq*nx, fViscDualTrue);

  sRef = 0;
  fcnPDE( sRef, integrandPDE, 2 );
  fcnLO( sRef, integrandLO, 2 );

  //PDE residual integrands (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  integrandPDETrue[0] = -fViscDualTrue;  // Basis function 1
  integrandPDETrue[1] = fAdvTrue + fViscTrue + fViscDualTrue;  // Basis function 2

  //LO residual integrands (left): (lifting-operator)
  integrandLiftxTrue[0] = ( 0 );  // Basis function 1
  integrandLiftxTrue[1] = dq*nx;  // Basis function 2

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLiftxTrue[0], integrandLO[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLiftxTrue[1], integrandLO[1][0], small_tol, close_tol );

  // test the trace element integral of the functor
  int quadratureorder = 0;
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD0, Node, BasisWeightedPDEClass::IntegrandType> integralPDE(quadratureorder, nIntegrand);
  GalerkinWeightedIntegral<TopoD0, Node, BasisWeightedLOClass::IntegrandType> integralLO(quadratureorder, nIntegrand);

  BasisWeightedPDEClass::IntegrandType rsdPDEElem[2] = {0,0};
  BasisWeightedLOClass::IntegrandType rsdLOElem[2] = {0,0};
  Real rsdPDETrue[2];
  Real rsdLiftxTrue[2];

  // cell integration for canonical element
  integralPDE( fcnPDE, xnode, rsdPDEElem, nIntegrand );
  integralLO( fcnLO, xnode, rsdLOElem, nIntegrand );

  //PDE residuals (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  rsdPDETrue[0] = -fViscDualTrue; // Basis function 1
  rsdPDETrue[1] = fAdvTrue + fViscTrue + fViscDualTrue; // Basis function 2

  //LO residuals (left): (lifting-operator)
  rsdLiftxTrue[0] = ( 0 );   // Basis function 1
  rsdLiftxTrue[1] = dq*nx;   // Basis function 2

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDEElem[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDEElem[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdLiftxTrue[0], rsdLOElem[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLiftxTrue[1], rsdLOElem[1][0], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_Line_X1Q1R1W2S2_test )
{
  typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel_PowerLaw, CapillaryModel_Linear> TraitsModelClass;
  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEBL;
  typedef PDENDConvertSpace<PhysD1, PDEBL> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD1::D,ArrayQ> VectorArrayQ;

  typedef BCNDConvertSpace<PhysD1, BCBuckleyLeverett1D<BCTypeDirichlet_mitState, PDEBL> > BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementRFieldClass;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> FieldWeightedClass;

  // PDE
  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  const Real pcmax = 5.0;
  CapillaryModel_Linear cap_model(pcmax);

  PDEBL pdeBL(phi, uT, kr_model, mu_w, mu_n, K, cap_model);
  PDEClass pde(phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC
  Real SwB = 0.25;
  BCClass bc(pde, SwB);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent line grid
  Real x0, x1;

  x0 = 0;
  x1 = 1;

  xfldElem.DOF(0) = {x0};
  xfldElem.DOF(1) = {x1};

  xnode.DOF(0) = {x1};
  xnode.normalSignL() = 1;

  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // line solution (left)
  Real Sw0 = 0.4, Sw1 = 0.35;
  qfldElem.DOF(0) = Sw0;
  qfldElem.DOF(1) = Sw1;

  // lifting operators
  ElementRFieldClass rfldElem(order, BasisFunctionCategory_Hierarchical);

  rfldElem.DOF(0) = -1;  rfldElem.DOF(1) = 5;

  // weight
  ElementQFieldCell wfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, wfldElem.order() );
  BOOST_CHECK_EQUAL( 2, wfldElem.nDOF() );

  // line solution (left)
  Real w0 = 3, w1 = 4;
  wfldElem.DOF(0) = w0;
  wfldElem.DOF(1) = w1;

  // lifting operators
  ElementRFieldClass sfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, sfldElem.order() );
  BOOST_CHECK_EQUAL( 2, sfldElem.nDOF() );

  Real s0 = -5, s1 = 3;
  sfldElem.DOF(0) = s0;  sfldElem.DOF(1) = s1;

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand functor
  IntegrandClass fcnint( pde, bc, {0}, disc );

  FieldWeightedClass fcn = fcnint.integrand( xnode,
                                             CanonicalTraceToCell(0, 1),
                                             xfldElem,
                                             qfldElem, rfldElem,
                                             wfldElem, sfldElem);

  BOOST_CHECK_EQUAL( 2, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );
  BOOST_CHECK( fcnint.needsFieldTrace() == false );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;
  FieldWeightedClass::IntegrandType integrand;
  Real integrandPDETrue, integrandLiftTrue;

  ArrayQ q = Sw1;
  ArrayQ qB = SwB;
  ArrayQ dq = q - qB;
  ArrayQ qx = (Sw1 - Sw0) / (x1 - x0);
  ArrayQ qx_lifted = qx + viscousEtaParameter*rfldElem.DOF(1);

  Real t = 0;
  Real nx = 1.0;
  ArrayQ fAdvTrue = 0;
  pdeBL.fluxAdvectiveUpwind(x1, t, q, qB, nx, fAdvTrue);

  ArrayQ fViscTrue = 0;
  pdeBL.fluxViscous(x1, t, qB, qx_lifted, fViscTrue);
  fViscTrue *= nx;

  ArrayQ fViscDualTrue = 0; //dual consistency term
  pdeBL.fluxViscous(x1, t, qB, dq*nx, fViscDualTrue);

  // Test at sRef={0}, s={1}
  sRef = {0};
  fcn( sRef, integrand );

  //PDE residual integrands (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  integrandPDETrue = w0*(-fViscDualTrue) + w1*(fAdvTrue + fViscTrue + fViscDualTrue);  // Basis function
  SANS_CHECK_CLOSE( integrandPDETrue, integrand.PDE, small_tol, close_tol );

  //LO residual integrands (left): (lifting-operator)
  integrandLiftTrue = s0*0 + s1*dq*nx;  // Basis function

  SANS_CHECK_CLOSE( integrandLiftTrue, integrand.Lift, small_tol, close_tol );

  // test the trace element integral of the functor
  int quadratureorder = 0;
  ElementIntegral<TopoD0, Node, FieldWeightedClass::IntegrandType> integral(quadratureorder);

  FieldWeightedClass::IntegrandType rsdElem=0;
  Real rsdPDETrue;
  Real rsdLiftTrue;

  // cell integration for canonical element
  integral( fcn, xnode, rsdElem );

  //PDE residuals (left): (advective) + (viscous) + (dual-consistent) + (lifting-operator)
  rsdPDETrue = w0*(-fViscDualTrue) + w1*(fAdvTrue + fViscTrue + fViscDualTrue); // Basis function

  SANS_CHECK_CLOSE( rsdPDETrue, rsdElem.PDE, small_tol, close_tol );

  //LO residuals (left): (lifting-operator)
  rsdLiftTrue = s0*0 + s1*dq*nx;   // Basis function

  SANS_CHECK_CLOSE( rsdLiftTrue, rsdElem.Lift, small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_1D_Line_test )
{
  typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel_PowerLaw, CapillaryModel_Linear> TraitsModelClass;
  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEBL;
  typedef PDENDConvertSpace<PhysD1, PDEBL> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD1::D,ArrayQ> VectorArrayQ;

  typedef BCNDConvertSpace<PhysD1, BCBuckleyLeverett1D<BCTypeDirichlet_mitState, PDEBL> > BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementRFieldClass;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, DGBR2> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO<Real,Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> BasisWeightedLOClass;
  typedef IntegrandClass::FieldWeighted<Real,Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> FieldWeightedClass;

  // PDE
  RelPermModel_PowerLaw kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  const Real pcmax = 5.0;
  CapillaryModel_Linear cap_model(pcmax);

  PDEClass pde(phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC
  Real SwB = 0.25;
  BCClass bc(pde, SwB);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = {x1};
  xfldElem.DOF(1) = {x2};

  xnode.DOF(0) = {x2};
  xnode.normalSignL() = 1;

  for ( int qorder = 2; qorder < 3; qorder++ )
  {
    //solution
    ElementQFieldCell qfldElem(qorder, BasisFunctionCategory_Hierarchical );
    ElementQFieldCell wfldElem(qorder, BasisFunctionCategory_Hierarchical );

    BOOST_CHECK_EQUAL( qorder,   qfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   wfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, wfldElem.nDOF() );

    BOOST_CHECK_EQUAL( qfldElem.nDOF(), wfldElem.nDOF() );
    // line solution
    for ( int dof = 0; dof < qfldElem.nDOF(); dof++ )
    {
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);
      wfldElem.DOF(dof) = 0;
    }

    // lifting operators
    ElementRFieldClass rfldElem(qorder, BasisFunctionCategory_Hierarchical);
    ElementRFieldClass sfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder,   rfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, rfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   sfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, sfldElem.nDOF() );

    BOOST_CHECK_EQUAL( rfldElem.nDOF(), sfldElem.nDOF() );
    // lifting operator
    for ( int dof = 0; dof < rfldElem.nDOF(); dof++ )
    {
      rfldElem.DOF(dof) = (dof+1)*pow(-1,dof);
      sfldElem.DOF(dof) = 0;
    }
    // BR2 discretization
    Real viscousEtaParameter = 2;
    DiscretizationDGBR2 disc(0, viscousEtaParameter);

    // integrand functor
    IntegrandClass fcnint( pde, bc, {0}, disc );

    BasisWeightedPDEClass fcnPDEB = fcnint.integrand_PDE( xnode,
                                                          CanonicalTraceToCell(0, 1),
                                                          xfldElem,
                                                          qfldElem, rfldElem );
    BasisWeightedLOClass fcnLOB = fcnint.integrand_LO( xnode,
                                                       CanonicalTraceToCell(0, 1),
                                                       xfldElem,
                                                       qfldElem, rfldElem );
    FieldWeightedClass fcnW = fcnint.integrand( xnode,
                                                CanonicalTraceToCell(0, 1),
                                                xfldElem,
                                                qfldElem, rfldElem,
                                                wfldElem, sfldElem);

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;
    const int nDOF = 3; // want to be qorder+1, but can't
    int nIntegrand = qfldElem.nDOF();
    int quadratureorder = 0;

    GalerkinWeightedIntegral<TopoD0, Node, BasisWeightedPDEClass::IntegrandType> integralPDEB(quadratureorder, nIntegrand);
    GalerkinWeightedIntegral<TopoD0, Node, BasisWeightedLOClass::IntegrandType> integralLOB(quadratureorder, nIntegrand);
    ElementIntegral<TopoD0, Node, FieldWeightedClass::IntegrandType> integralW(quadratureorder);

    BasisWeightedPDEClass::IntegrandType rsdPDEElemB[nDOF] = {0,0};
    BasisWeightedLOClass::IntegrandType rsdLOElemB[nDOF] = {0,0};
    FieldWeightedClass::IntegrandType rsdElemW = 0;

    // cell integration for canonical element
    integralPDEB( fcnPDEB, xnode, rsdPDEElemB, nIntegrand );
    integralLOB( fcnLOB, xnode, rsdLOElemB, nIntegrand );

    for ( int i = 0; i < wfldElem.nDOF(); i++ )
    {
      // set just one of the elements to one
      wfldElem.DOF(i) = 1; sfldElem.DOF(i) = 1;

      // cell integration for canonical Element
      rsdElemW = 0;
      integralW( fcnW, xnode, rsdElemW );

      // test the two integrands are the same
      SANS_CHECK_CLOSE ( rsdElemW.PDE, rsdPDEElemB[i], small_tol, close_tol );

      Real tmp = 0;
      for (int d = 0; d < PhysD1::D; d++ )
        tmp += rsdLOElemB[i][d];

      SANS_CHECK_CLOSE( rsdElemW.Lift, tmp, small_tol, close_tol );

      // reset to 0
      wfldElem.DOF(i) = 0; sfldElem.DOF(i) = 0;
    }
  }
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
