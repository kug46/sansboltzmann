// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianCell_DGBR2_LiftedQuantity_TwoPhaseAV_btest
// testing of cell jacobian: two-phase flow with artificial viscosity

#include <boost/mpl/list.hpp>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/PorousMedia/Q2DPrimitive_pnSw.h"
#include "pde/PorousMedia/TraitsTwoPhaseArtificialViscosity.h"
#include "pde/PorousMedia/PDETwoPhase_ArtificialViscosity2D.h"
#include "pde/PorousMedia/BCTwoPhaseArtificialViscosity2D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/PermeabilityModel2D.h"
#include "pde/PorousMedia/CapillaryModel.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"
#include "Field/HField/GenHFieldArea_CG.h"

#include "Discretization/DG/IntegrandCell_DGBR2.h"
#include "Discretization/DG/ResidualCell_DGBR2.h"
#include "Discretization/DG/SetFieldInteriorTrace_DGBR2_LiftedQuantity.h"
#include "Discretization/DG/JacobianCell_DGBR2.h"
#include "Discretization/DG/JacobianInteriorTrace_DGBR2_LiftedQuantity.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/QuadratureOrder.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

//#include "Field/output_Tecplot.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianCell_DGBR2_LiftedQuantity_TwoPhaseAV_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_TwoPhase_ArtificialViscosity2D_Box_Triangle_X1 )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef PermeabilityModel2D_QuadBlock RockPermModel;
  typedef QTypePrimitive_pnSw QType;

  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel,
                              CapillaryModel> TraitsModelTwoPhaseClass;

  typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity,
                                            TraitsModelTwoPhaseClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeTwoPhaseArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeTwoPhaseArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_TwoPhase<PDEBaseClass> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template MatrixQ<Real> MatrixQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,PhysD2::D,MatrixQ> RowMatrixQ;

  typedef IntegrandCell_DGBR2<NDPDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_DGBR2<NDPDEClass> IntegrandTraceClass;

  const int D = PhysD2::D;
  const int N = 3;

  std::vector<Real> step0 = {5e+0, 1e-3, 1e-1}; //first step-sizes for pressure, saturation and nu
  std::vector<Real> step1 = {5e-1, 1e-4, 1e-2}; //second step-sizes for pressure, saturation and nu


  const Real Lx = 100.0; //ft
  const Real Ly = Lx;
  const Real T = 1000.0; //days

  // Grid
//  std::vector<Real> xvec = {0, 100, 200, 400, 600, 800, 900, 1000};
  std::vector<Real> xvec = {0, 20, 40, 60, 80, 100};
  std::vector<Real> yvec = xvec;
  XField2D_Box_Triangle_X1 xfld(xvec, yvec, true);

  //Compute generalized log H-tensor field
  GenHField_CG<PhysD2, TopoD2> Hfld(xfld);

  // PDE
  const Real pref = 14.7;

  DensityModel rhow(62.4, 5.0e-6, pref);
  DensityModel rhon(52.1, 1.5e-5, pref);

  PorosityModel phi(0.3, 3.0e-6, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  CapillaryModel pc(2);
  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real Kref = conversion*200; //mD

  DLA::MatrixS<2,2,Real> Kref_mat = {{Kref, 0.0}, {0.0, Kref}};

  RockPermModel K(Kref_mat);

  // Set up source term PyDicts

  Real R_bore = 1.0/6.0; //ft
  Real R_well = 5.0; //ft
  Real L_offset = 10.0; //ft

  Real pnIn = 3300.0; //psi
  Real SwIn = 1.0;
  Real pnOut = 2700.0; //psi

  Real pwInit = 3000;
  Real SwInit = 0.5;
  Real sensorInit = 0.3;

  const int nWellParam = 1;

  PyDict well_in;
  well_in[SourceTwoPhase2DType_FixedPressureInflow_Param::params.pB] = pnIn;
  well_in[SourceTwoPhase2DType_FixedPressureInflow_Param::params.Sw] = SwIn;
  well_in[SourceTwoPhase2DType_FixedPressureInflow_Param::params.Rwellbore] = R_bore;
  well_in[SourceTwoPhase2DType_FixedPressureInflow_Param::params.nParam] = nWellParam;
  well_in[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureInflow;

  PyDict well_out;
  well_out[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.pB] = pnOut;
  well_out[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.Rwellbore] = R_bore;
  well_out[SourceTwoPhase2DType_FixedPressureOutflow_Param::params.nParam] = nWellParam;
  well_out[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureOutflow;

  PyDict source_injection;
  source_injection[SourceTwoPhase2DParam::params.Source] = well_in;
  source_injection[SourceTwoPhase2DParam::params.xcentroid] = L_offset;
  source_injection[SourceTwoPhase2DParam::params.ycentroid] = L_offset;
  source_injection[SourceTwoPhase2DParam::params.R] = R_well;
  source_injection[SourceTwoPhase2DParam::params.Tmin] = 0;
  source_injection[SourceTwoPhase2DParam::params.Tmax] = T;
  source_injection[SourceTwoPhase2DParam::params.smoothLr] = 0.0*R_well;
  source_injection[SourceTwoPhase2DParam::params.smoothT] = 0.0;
  source_injection[SourceTwoPhase2DParam::params.visibleAngle] = 2.0*PI;

  PyDict source_production;
  source_production[SourceTwoPhase2DParam::params.Source] = well_out;
  source_production[SourceTwoPhase2DParam::params.xcentroid] = Lx - L_offset;
  source_production[SourceTwoPhase2DParam::params.ycentroid] = Ly - L_offset;
  source_production[SourceTwoPhase2DParam::params.R] = R_well;
  source_production[SourceTwoPhase2DParam::params.Tmin] = 0;
  source_production[SourceTwoPhase2DParam::params.Tmax] = T;
  source_production[SourceTwoPhase2DParam::params.smoothLr] = 0.0*R_well;
  source_production[SourceTwoPhase2DParam::params.smoothT] = 0.0;
  source_production[SourceTwoPhase2DParam::params.visibleAngle] = 2.0*PI;

  PyDict source_list;
  source_list["injection_well"] = source_injection;
  source_list["production_well"] = source_production;

  SourceTwoPhase2DListParam::checkInputs(source_list);

  // TwoPhase PDE with AV
  int qorder = 1;
  bool hasSpaceTimeDiffusion = false;
  PDEBaseClass pdeTwoPhaseAV(qorder, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc, source_list);

  SensorAdvectiveFlux sensor_adv(0.0, 0.0);
  SensorViscousFlux sensor_visc(3.0);
  SensorSource sensor_source(pdeTwoPhaseAV);

  // AV PDE with sensor equation
  bool isSteady = false;
  NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteady,
                 qorder, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc, source_list);

  // Initial solution
  ArrayQ qInit;
  AVVariable<PressureNonWet_SaturationWet,Real> qdata({pwInit, SwInit}, sensorInit);
  pde.setDOFFrom( qInit, qdata );

  // static tests
  BOOST_CHECK( pde.D == D );
  BOOST_CHECK( pde.N == N );

  // solution
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  qfld = qInit;
  const int qDOF = qfld.nDOF();

  //perturb solution a bit
  for (int i = 0; i < qDOF; i++)
  {
    qfld.DOF(i)[0] += 5.0*cos(PI*i/((Real)qDOF)); //pw
    qfld.DOF(i)[1] += 0.1*sin(PI*i/((Real)qDOF)); //Sw
    qfld.DOF(i)[2] -= 0.1*cos(PI*i/((Real)qDOF)); //nu
  }

  // lifting-operator: P1
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  const int rDOF = rfld.nDOF();

  // lifting operator data
  for (int i = 0; i < rDOF; i++)
  {
    rfld.DOF(i)[0][0] =  sin(PI*i/((Real)rDOF));
    rfld.DOF(i)[0][1] =  0.1*cos(PI*i/((Real)rDOF));
    rfld.DOF(i)[0][2] = -0.1*sin(PI*i/((Real)rDOF));

    rfld.DOF(i)[1][0] = -cos(PI*i/((Real)rDOF));
    rfld.DOF(i)[1][1] =  0.1*sin(PI*i/((Real)rDOF));
    rfld.DOF(i)[1][2] = -0.1*sin(PI*i/((Real)rDOF));
  }

  // lifted quantity field
  int sorder = 0;
  Field_DG_Cell<PhysD2, TopoD2, Real> sfld(xfld, sorder, BasisFunctionCategory_Legendre);
  sfld = 0.0;

  // Compute the field of inverse mass matrices for the lifted quantity field
  FieldDataInvMassMatrix_Cell mmfld(sfld);

  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandTraceClass fcnITrace( pde, disc, {0} );

  // quadrature rule (2*P for basis and flux polynomials)
  QuadratureOrder quadratureOrder(xfld, 2*qorder);

  // jacobian via FD w/ residual operator; assumes scalar PDE

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
  DLA::MatrixD<MatrixQ> jacPDE_q_step0(qDOF,qDOF);
  DLA::MatrixD<MatrixQ> jacPDE_q_step1(qDOF,qDOF);

  //Compute lifted quantity field
  sfld = 0.0;
  IntegrateInteriorTraceGroups<TopoD2>::integrate(SetFieldInteriorTrace_DGBR2_LiftedQuantity(fcnITrace, mmfld),
                                                  (Hfld, xfld), (qfld, sfld),
                                                  quadratureOrder.interiorTraceOrders.data(), quadratureOrder.interiorTraceOrders.size());


  rsdPDEGlobal0 = 0;
  IntegrateCellGroups<TopoD2>::integrate(ResidualCell_DGBR2(fcnCell, rsdPDEGlobal0),
                                         (Hfld, xfld), (qfld, rfld, sfld),
                                         quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size());


//  output_Tecplot(sfld, "tmp/sfld.plt");

  // wrt q
  for (int j = 0; j < qDOF; j++)
  {
    for (int n = 0; n < N; n++)
    {
      //---------------Ping with large step size-------------------

      qfld.DOF(j)[n] += step0[n];

      //Recompute lifted quantity field
      sfld = 0.0;
      IntegrateInteriorTraceGroups<TopoD2>::integrate(SetFieldInteriorTrace_DGBR2_LiftedQuantity(fcnITrace, mmfld),
                                                      (Hfld, xfld), (qfld, sfld),
                                                      quadratureOrder.interiorTraceOrders.data(), quadratureOrder.interiorTraceOrders.size());

      // Compute perturbed residuals
      rsdPDEGlobal1 = 0;
      IntegrateCellGroups<TopoD2>::integrate(ResidualCell_DGBR2(fcnCell, rsdPDEGlobal1),
                                             (Hfld, xfld), (qfld, rfld, sfld),
                                             quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size());

      qfld.DOF(j)[n] -= 2*step0[n];

      //Recompute lifted quantity field
      sfld = 0.0;
      IntegrateInteriorTraceGroups<TopoD2>::integrate(SetFieldInteriorTrace_DGBR2_LiftedQuantity(fcnITrace, mmfld),
                                                      (Hfld, xfld), (qfld, sfld),
                                                      quadratureOrder.interiorTraceOrders.data(), quadratureOrder.interiorTraceOrders.size());

      rsdPDEGlobal0 = 0;
      IntegrateCellGroups<TopoD2>::integrate(ResidualCell_DGBR2(fcnCell, rsdPDEGlobal0),
                                             (Hfld, xfld), (qfld, rfld, sfld),
                                             quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size());

      qfld.DOF(j)[n] += step0[n];

      for (int i = 0; i < qDOF; i++)
        for (int m = 0; m < N; m++)
          jacPDE_q_step0(i,j)(m,n) = (rsdPDEGlobal1[i][m] - rsdPDEGlobal0[i][m])/(2.0*step0[n]);

      //---------------Ping with small step size-------------------

      qfld.DOF(j)[n] += step1[n];

      //Recompute lifted quantity field
      sfld = 0.0;
      IntegrateInteriorTraceGroups<TopoD2>::integrate(SetFieldInteriorTrace_DGBR2_LiftedQuantity(fcnITrace, mmfld),
                                                      (Hfld, xfld), (qfld, sfld),
                                                      quadratureOrder.interiorTraceOrders.data(), quadratureOrder.interiorTraceOrders.size());

      // Compute perturbed residuals
      rsdPDEGlobal1 = 0;
      IntegrateCellGroups<TopoD2>::integrate(ResidualCell_DGBR2(fcnCell, rsdPDEGlobal1),
                                             (Hfld, xfld), (qfld, rfld, sfld),
                                             quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size());

      qfld.DOF(j)[n] -= 2*step1[n];

      //Recompute lifted quantity field
      sfld = 0.0;
      IntegrateInteriorTraceGroups<TopoD2>::integrate(SetFieldInteriorTrace_DGBR2_LiftedQuantity(fcnITrace, mmfld),
                                                      (Hfld, xfld), (qfld, sfld),
                                                      quadratureOrder.interiorTraceOrders.data(), quadratureOrder.interiorTraceOrders.size());

      rsdPDEGlobal0 = 0;
      IntegrateCellGroups<TopoD2>::integrate(ResidualCell_DGBR2(fcnCell, rsdPDEGlobal0),
                                             (Hfld, xfld), (qfld, rfld, sfld),
                                             quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size());

      qfld.DOF(j)[n] += step1[n];

      for (int i = 0; i < qDOF; i++)
        for (int m = 0; m < N; m++)
          jacPDE_q_step1(i,j)(m,n) = (rsdPDEGlobal1[i][m] - rsdPDEGlobal0[i][m])/(2.0*step1[n]);
    }
  }

  // jacobian via Surreal

  FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld); jacPDE_R = 0; // Lifting operator jacobian from cell integral

  typedef typename MatrixLiftedQuantity<MatrixQ>::type MatrixT;
  std::shared_ptr<FieldDataMatrixD_Cell<MatrixT>> pjacPDE_liftedQuantity; //PDE jacobian wrt lifted quantity

  pjacPDE_liftedQuantity = std::make_shared<FieldDataMatrixD_Cell<MatrixT>>(qfld, sfld);

  DLA::MatrixD<MatrixQ> mtxPDEGlob_q(qDOF,qDOF);

  mtxPDEGlob_q = 0;
  IntegrateCellGroups<TopoD2>::integrate( JacobianCell_DGBR2(fcnCell, mtxPDEGlob_q, jacPDE_R, pjacPDE_liftedQuantity),
                                          (Hfld, xfld), (qfld, rfld, sfld),
                                          quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size());

  IntegrateInteriorTraceGroups<TopoD2>::integrate(
      JacobianInteriorTrace_DGBR2_LiftedQuantity(fcnITrace, mmfld, *pjacPDE_liftedQuantity, mtxPDEGlob_q),
      (Hfld, xfld), (qfld, sfld),
      quadratureOrder.interiorTraceOrders.data(),
      quadratureOrder.interiorTraceOrders.size() );

  const Real small_tol = 1e-6;
//  const Real close_tol[3] = {1e-2, 1e-5, 1e-5};

  for (int i = 0; i < qDOF; i++)
    for (int j = 0; j < qDOF; j++)
      for (int m = 0; m < N; m++ )
        for (int n = 0; n < N; n++ )
        {
          Real error0 = fabs(jacPDE_q_step0(i,j)(m,n) - mtxPDEGlob_q(i,j)(m,n));
          Real error1 = fabs(jacPDE_q_step1(i,j)(m,n) - mtxPDEGlob_q(i,j)(m,n));

          if (error1 > small_tol)
          {
            Real rate = log(error1/error0) / log(step1[n]/step0[n]);
//            std::cout << "i:" << i << ", j:" << j << ", m:" << m << ", n:" << n
//                      << ", err: " << error0 << ", " << error1 << ", rate: " << rate << std::endl;
            BOOST_CHECK_MESSAGE( rate >= 1.9 && rate <= 5.0, "Rate check failed: rate = " << rate <<
                                 ", errors = [" << error0 << "," << error1 << "]" );
          }
//          SANS_CHECK_CLOSE( jacPDE_q_step1(i,j)(m,n), mtxPDEGlob_q(i,j)(m,n), small_tol, close_tol[n] );
        }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
