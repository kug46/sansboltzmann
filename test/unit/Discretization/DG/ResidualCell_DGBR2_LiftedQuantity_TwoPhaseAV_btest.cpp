// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ResidualCell_DGBR2_LiftedQuantity_TwoPhaseAV_btest
// testing of residual functions for DG BR2 with lifted quantities for source terms

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/PorousMedia/Q2DPrimitive_pnSw.h"
#include "pde/PorousMedia/TraitsTwoPhaseArtificialViscosity.h"
#include "pde/PorousMedia/PDETwoPhase_ArtificialViscosity2D.h"
#include "pde/PorousMedia/BCTwoPhaseArtificialViscosity2D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/PermeabilityModel2D.h"
#include "pde/PorousMedia/CapillaryModel.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"
#include "Field/HField/GenHFieldArea_CG.h"

#include "Discretization/DG/SetFieldInteriorTrace_DGBR2_LiftedQuantity.h"
#include "Discretization/DG/IntegrandCell_DGBR2.h"
#include "Discretization/DG/DiscretizationDGBR2.h"
#include "Discretization/DG/ResidualCell_DGBR2.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ResidualCell_DGBR2_LiftedQuantity_TwoPhaseAV_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_4Triangle_X1Q0S0 )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef PermeabilityModel2D_QuadBlock RockPermModel;
  typedef QTypePrimitive_pnSw QType;

  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel,
                              CapillaryModel> TraitsModelTwoPhaseClass;

  typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity,
                                            TraitsModelTwoPhaseClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeTwoPhaseArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeTwoPhaseArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_TwoPhase<PDEBaseClass> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandCell_DGBR2<NDPDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_DGBR2<NDPDEClass> IntegrandTraceClass;

  int N = 3;
  int nDOF = 4;
  int Ntrace = 3;

  // PDE
  const Real pref = 14.7;

  DensityModel rhow(62.4, 5.0e-6, pref);
  DensityModel rhon(52.1, 1.5e-5, pref);

  PorosityModel phi(0.3, 3.0e-6, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  const Real ft3_per_bbl = 5.61458;
  const Real conversion = (1.127e-3)*ft3_per_bbl; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real Kref = conversion*200; //mD

  DLA::MatrixS<2,2,Real> Kref_mat = {{Kref, 0.0}, {0.0, Kref}};

  RockPermModel K(Kref_mat);

  CapillaryModel pc(0.0);

  // TwoPhase PDE with AV
  int order = 1; //hard-coded to 1 so that the artificial viscosity is added instead of being just zero
  bool hasSpaceTimeDiffusion = false;
  PDEBaseClass pdeTwoPhaseAV(order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

//  Sensor sensor(pdeTwoPhaseAV);
  SensorAdvectiveFlux sensor_adv(0.0, 0.0);
  SensorViscousFlux sensor_visc(3.0);
  SensorSource sensor_source(pdeTwoPhaseAV);

  // AV PDE with sensor equation
  bool isSteady = false;
  NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteady,
                 order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == N );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasSourceTrace() == false );
  BOOST_CHECK( pde.hasSourceLiftedQuantity() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid: P1 (aka X1)
  XField2D_4Triangle_X1_1Group xfld;

  //Compute generalized log H-tensor field
  GenHField_CG<PhysD2, TopoD2> Hfld(xfld);

  // solution field
  int qorder = 0;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
  BOOST_REQUIRE_EQUAL( qfld.nDOF(), nDOF );

  //P0 solutions in each cell
  Real pn[4] = {2500.0, 2520.0, 2513.0, 2527.0};
  Real Sw[4] = {0.26, 0.30, 0.20, 0.47};
  Real nu[4] = {0.15, 0.04, 0.23, 0.36};

  // triangle solution data
  for (int i = 0; i < 4; i++)
    qfld.DOF(i) = {pn[i], Sw[i], nu[i]};

  // lifting operator
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Legendre);

  rfld.DOF(0) = { 2, -3};  rfld.DOF(1) = { 7,  8};  rfld.DOF(2) = {-1,  7};
  rfld.DOF(3) = { 9,  6};  rfld.DOF(4) = {-1,  3};  rfld.DOF(5) = { 2,  3};
  rfld.DOF(6) = {-2,  1};  rfld.DOF(7) = { 4, -4};  rfld.DOF(8) = {-9, -5};
  rfld.DOF(9) = { 3,  4};  rfld.DOF(10) = { 1, 2};  rfld.DOF(11) = {3,  6};

  BOOST_CHECK_EQUAL( nDOF*Ntrace, rfld.nDOF() );
  BOOST_CHECK( rfld.D == pde.D);

  // lifted quantity field
  Field_DG_Cell<PhysD2, TopoD2, Real> sfld(xfld, qorder, BasisFunctionCategory_Legendre);
  BOOST_REQUIRE_EQUAL( sfld.nDOF(), nDOF );

  // Compute the field of inverse mass matrices for the lifted quantity field
  FieldDataInvMassMatrix_Cell mmfld(sfld);

  // BR2 discretization
  Real viscousEtaParameter = 3.0;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandTraceClass fcnITrace( pde, disc, {0} );

  Real triArea = 0.5;
  Real eps = 1e-3;

  //Trace saturation jumps
  Real jump[3] = {Sw[0] - Sw[1], Sw[0] - Sw[2], Sw[0] - Sw[3]};

  //P0 lifted quantities in the triangles
  Real lifted_quantity[4];
  lifted_quantity[0] = (smoothabs0(jump[0], eps) + smoothabs0(jump[1], eps) + smoothabs0(jump[2], eps)) / (Real) Ntrace;
  lifted_quantity[1] = smoothabs0(jump[0], eps) / (Real) Ntrace;
  lifted_quantity[2] = smoothabs0(jump[1], eps) / (Real) Ntrace;
  lifted_quantity[3] = smoothabs0(jump[2], eps) / (Real) Ntrace;

  DLA::MatrixSymS<PhysD2::D,Real> H = Hfld.DOF(0); //Mesh is isotropic, so all DOFs have same H-tensor
  Real x = 0, y = 0, time = 0;
  VectorArrayQ gradq = 0; //P0 solution has zero gradients

  ArrayQ rsdPDETrue[4]; //residuals of each cell

  for (int cell = 0; cell < 4; cell++)
  {
    Real Sk = log10(lifted_quantity[cell] + 1e-16);
    Real Sk_min = -4;
    Real Sk_max = -1;
    Real xi = (Sk - Sk_min)/(Sk_max - Sk_min);

//    Real sensor = smoothActivation_tanh(xi);
    Real alpha = 10;
    Real sensor = smoothActivation_exp(xi, alpha);

    VectorArrayQ R = 0; //sum of lifting operators on each cell

    for (int trace = 0; trace < Ntrace; trace++)
      R += rfld.DOF(Ntrace*cell + trace);

    VectorArrayQ gradq_lifted = gradq + R;

    Real nu_max;
    pdeTwoPhaseAV.artViscMax(H, x, y, time, qfld.DOF(cell), gradq_lifted[0], gradq_lifted[1], nu_max);

    //PDE residual: (source)  (advective + diffusion residuals are zero since gradq = 0)
    rsdPDETrue[cell][0] = 0.0; //Basis function 1
    rsdPDETrue[cell][1] = 0.0; //Basis function 2
    rsdPDETrue[cell][2] = (nu[cell] - sensor*nu_max)*triArea; //Basis function 3
  }

  const Real small_tol = 1e-13;
  const Real close_tol = 5e-13;
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());

  // quadrature rule (quadratic: basis & flux both linear)
  int quadratureOrder = 2;

  sfld = 0.0;
  IntegrateInteriorTraceGroups<TopoD2>::integrate(SetFieldInteriorTrace_DGBR2_LiftedQuantity(fcnITrace, mmfld),
                                                  (Hfld, xfld), (qfld, sfld), &quadratureOrder, 1);

  rsdPDEGlobal = 0;

  // base interface
  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_DGBR2(fcnCell, rsdPDEGlobal),
                                          (Hfld, xfld), (qfld, rfld, sfld), &quadratureOrder, 1 );

  //Cell 0
  SANS_CHECK_CLOSE( rsdPDETrue[0][0], rsdPDEGlobal[0][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[0][1], rsdPDEGlobal[0][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[0][2], rsdPDEGlobal[0][2], small_tol, close_tol );

  //Cell 1
  SANS_CHECK_CLOSE( rsdPDETrue[1][0], rsdPDEGlobal[1][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1][1], rsdPDEGlobal[1][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1][2], rsdPDEGlobal[1][2], small_tol, close_tol );

  //Cell 2
  SANS_CHECK_CLOSE( rsdPDETrue[2][0], rsdPDEGlobal[2][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[2][1], rsdPDEGlobal[2][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[2][2], rsdPDEGlobal[2][2], small_tol, close_tol );

  //Cell 3
  SANS_CHECK_CLOSE( rsdPDETrue[3][0], rsdPDEGlobal[3][0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[3][1], rsdPDEGlobal[3][1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[3][2], rsdPDEGlobal[3][2], small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
