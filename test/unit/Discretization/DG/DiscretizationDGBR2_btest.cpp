// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// DiscretizationDGBR2_Triangle_btest
// testing of DiscretizationDGBR2 class w/ triangle

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#include "Discretization/DG/DiscretizationDGBR2.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( DiscretizationDGBR2_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Discretization/DiscretizationDGBR2_ctor_pattern.txt", true );

  const bool verbose = true;

  DiscretizationDGBR2 disc0(0);
  BOOST_CHECK_EQUAL( 0, disc0.viscousEtaType() );
  BOOST_CHECK_EQUAL( 0, disc0.viscousEtaParameter() );

  DiscretizationDGBR2 disc1(0, 1, verbose, output);
  BOOST_CHECK_EQUAL( 0, disc1.viscousEtaType() );
  BOOST_CHECK_EQUAL( 1, disc1.viscousEtaParameter() );

  DiscretizationDGBR2 disc2(-1, 2, verbose, output);
  BOOST_CHECK_EQUAL( -1, disc2.viscousEtaType() );
  BOOST_CHECK_EQUAL(  2, disc2.viscousEtaParameter() );

  DiscretizationDGBR2 disc3(-2, 3, verbose, output);
  BOOST_CHECK_EQUAL( -2, disc3.viscousEtaType() );
  BOOST_CHECK_EQUAL(  3, disc3.viscousEtaParameter() );

  BOOST_CHECK( output.match_pattern() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( viscousEta_Triangle_test )
{
  typedef ElementXField<PhysD2, TopoD1, Line> XFieldLineClass;
  typedef ElementXField<PhysD2,TopoD2, Triangle> XFieldAreaClass;

  // grid

  int order = 1;
  XFieldAreaClass xfldL(order, BasisFunctionCategory_Hierarchical);
  XFieldAreaClass xfldR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldL.order() );
  BOOST_CHECK_EQUAL( 3, xfldL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldR.order() );
  BOOST_CHECK_EQUAL( 3, xfldR.nDOF() );

  XFieldLineClass xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;
  x4 = 4;  y4 = 1;

  xfldL.DOF(0) = {x1, y1};
  xfldL.DOF(1) = {x2, y2};
  xfldL.DOF(2) = {x3, y3};

  xfldR.DOF(0) = {x4, y4};
  xfldR.DOF(1) = {x3, y3};
  xfldR.DOF(2) = {x2, y2};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // viscous eta calculation

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Real eta, etaTrue;

  DiscretizationDGBR2 disc0(0);
  BOOST_CHECK_EQUAL( 0, disc0.viscousEtaType() );
  BOOST_CHECK_EQUAL( 0, disc0.viscousEtaParameter() );

  etaTrue = 0;
  eta = disc0.viscousEta( xedge, xfldL, xfldR, 0, 0 );
  SANS_CHECK_CLOSE( etaTrue, eta, small_tol, close_tol );

  DiscretizationDGBR2 disc1(0, 1);
  BOOST_CHECK_EQUAL( 0, disc1.viscousEtaType() );
  BOOST_CHECK_EQUAL( 1, disc1.viscousEtaParameter() );

  etaTrue = 1;
  eta = disc1.viscousEta( xedge, xfldL, xfldR, 0, 0 );
  SANS_CHECK_CLOSE( etaTrue, eta, small_tol, close_tol );

  DiscretizationDGBR2 disc2(-1, 2);
  BOOST_CHECK_EQUAL( -1, disc2.viscousEtaType() );
  BOOST_CHECK_EQUAL(  2, disc2.viscousEtaParameter() );

  etaTrue = 2*3;
  eta = disc2.viscousEta( xedge, xfldL, xfldR, 0, 0 );
  SANS_CHECK_CLOSE( etaTrue, eta, small_tol, close_tol );

  DiscretizationDGBR2 disc3(-2, 3);
  BOOST_CHECK_EQUAL( -2, disc3.viscousEtaType() );
  BOOST_CHECK_EQUAL(  3, disc3.viscousEtaParameter() );

  Real hL = 1./sqrt(2);
  Real hR = 2*sqrt(2);
  etaTrue = 3*(3./(1 + 0.5*(hL/hR + hR/hL)));
  eta = disc3.viscousEta( xedge, xfldL, xfldR, 0, 0 );
  SANS_CHECK_CLOSE( etaTrue, eta, small_tol, close_tol );
}


#if 0   // dump() not yet implemented
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Discretization/DiscretizationDGBR2_dump_pattern.txt", true );

  const bool verbose = true;

  DiscretizationDGBR2 disc0(0);
  disc0.dump( 2, output );

  DiscretizationDGBR2 disc1(0, 3, verbose);
  disc1.dump( 2, output );

  DiscretizationDGBR2 disc2(-1, 3, verbose);
  disc2.dump( 2, output );

  DiscretizationDGBR2 disc3(-2, 3, verbose);
  disc3.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
