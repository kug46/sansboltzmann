// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ResidualCell_DGBR2_AD_btest
// testing of residual functions for DG BR2 with Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

#include "Discretization/DG/IntegrandCell_DGBR2.h"
#include "Discretization/DG/DiscretizationDGBR2.h"
#include "Discretization/DG/ResidualCell_DGBR2.h"

#include "Discretization/IntegrateCellGroups.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ResidualCell_DGBR2_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualCell_DGBR2_1Line_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandClass;

  const int nDOF = 2;
  const int Ntrace = 2;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real s0 = 0.25, sx = 0.6;
  Source1D_UniformGrad source(s0, sx);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid: single triangle, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single line, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

   // solution data
  qfld.DOF(0) = 2;
  qfld.DOF(1) = 3;

  BOOST_CHECK_EQUAL( nDOF, qfld.nDOF() );

  // lifting operator: single triangle, P1 (aka Q1)
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  rfld.DOF(0) = -1;  rfld.DOF(1) = 5;
  rfld.DOF(2) =  3;  rfld.DOF(3) = 2;

  BOOST_CHECK_EQUAL( nDOF*Ntrace, rfld.nDOF() );
  BOOST_CHECK( rfld.D == pde.D);

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // quadrature rule (linear: basis grad is const, flux is linear)
  int quadratureOrder = 2; //(integral of phi*lifting operator requires a quadratic quadrature order)

  Real rsdPDETrue[nDOF];

  //PDE residual: (advective) + (viscous) + (source)
  rsdPDETrue[0] = ( 11/4. ) + ( -2123/1000. ) + ( 203/120. );   // Basis function 1
  rsdPDETrue[1] = ( -11/4. ) + ( 2123/1000. ) + ( 67/30. );   // Basis function 2

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());

  rsdPDEGlobal = 0;

  // integrand
  IntegrandClass fcn( pde, disc, {0} );

  // base interface
  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcn, rsdPDEGlobal),
                                          xfld, (qfld, rfld), &quadratureOrder, 1 );

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDEGlobal[1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualCell_DGBR2_1Triangle_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandClass;

  const int nDOF = 3;
  const int Ntrace = 3;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Real s0 = 0.25, sx = 0.6, sy = -1.5;
  Source2D_UniformGrad source(s0, sx, sy);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

   // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  BOOST_CHECK_EQUAL( nDOF, qfld.nDOF() );

  // lifting operator: single triangle, P1 (aka Q1)
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  rfld.DOF(0) = { 2, -3};  rfld.DOF(1) = { 7,  8};  rfld.DOF(2) = {-1,  7};
  rfld.DOF(3) = { 9,  6};  rfld.DOF(4) = {-1,  3};  rfld.DOF(5) = { 2,  3};
  rfld.DOF(6) = {-2,  1};  rfld.DOF(7) = { 4, -4};  rfld.DOF(8) = {-9, -5};

  BOOST_CHECK_EQUAL( nDOF*Ntrace, rfld.nDOF() );
  BOOST_CHECK( rfld.D == pde.D);

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // quadrature rule (for linear solution: basis grad is const, flux is linear)
  int quadratureOrder = 2; //(integral of phi*lifting operator requires a quadratic quadrature order)

  Real rsdPDETrue[nDOF];

  //PDE residual: (advection) + (diffusion) + (source)
  rsdPDETrue[0] = (26/15.) + (-627/125.) + (-193/160.); // Basis function 1
  rsdPDETrue[1] = (-22/15.) + (1181/400.) + (-647/480.); // Basis function 2
  rsdPDETrue[2] = (-4/15.) + (4127/2000.) + (-133/80.); // Basis function 3

  const Real small_tol = 1e-13;
  const Real close_tol = 5e-13;
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());

  rsdPDEGlobal = 0;

  // integrand
  IntegrandClass fcnPDE( pde, disc, {0} );

  // base interface
  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_DGBR2(fcnPDE, rsdPDEGlobal),
                                          xfld, (qfld, rfld), &quadratureOrder, 1 );

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDEGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[2], rsdPDEGlobal[2], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualCell_DGBR2_1Tet_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_UniformGrad > PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandClass;

  const int nDOF = 4;
  const int Ntrace = 4;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.7;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123; Real kxy = 0.553; Real kxz = 0.760;
  Real kyx = 0.553; Real kyy = 1.007; Real kyz = 0.365;
  Real kzx = 0.760; Real kzy = 0.365; Real kzz = 1.460;

  ViscousFlux3D_Uniform visc(kxx, kxy, kxz,
                                 kyx, kyy, kyz,
                                 kzx, kzy, kzz);

  Real s0 = 0.25, sx = 0.6, sy = -1.5, sz = 0.1;
  Source3D_UniformGrad source(s0, sx, sy, sz);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid: single triangle, P1 (aka X1)
  XField3D_1Tet_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 4, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single tet, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

   // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 6;
  qfld.DOF(3) = 4;

  BOOST_CHECK_EQUAL( nDOF, qfld.nDOF() );

  // lifting operator: single tet, P1 (aka Q1)
  FieldLift_DG_Cell<PhysD3, TopoD3, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  rfld.DOF( 0) = { 2, -3, -7};  rfld.DOF( 1) = { 7,  8,  5};  rfld.DOF( 2) = { 4,  1,  2};  rfld.DOF( 3) = {-1,  7,  1};
  rfld.DOF( 4) = { 9,  6,  3};  rfld.DOF( 5) = {-1,  3,  7};  rfld.DOF( 6) = { 3, -1, -2};  rfld.DOF( 7) = { 2,  3,  4};
  rfld.DOF( 8) = {-2,  1,  5};  rfld.DOF( 9) = { 4, -4, -3};  rfld.DOF(10) = {-1,  2,  1};  rfld.DOF(11) = {-9, -5, -8};
  rfld.DOF(12) = { 2, -1, -1};  rfld.DOF(13) = { 1,  2,  6};  rfld.DOF(14) = { 3, -3, -4};  rfld.DOF(15) = {-4,  5,  2};


  BOOST_CHECK_EQUAL( nDOF*Ntrace, rfld.nDOF() );
  BOOST_CHECK( rfld.D == pde.D);

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // quadrature rule (for linear solution: basis grad is const, flux is linear)
  int quadratureOrder = 2; //(integral of phi*lifting operator requires a quadratic quadrature order)

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());

  rsdPDEGlobal = 0;

  // integrand
  IntegrandClass fcnPDE( pde, disc, {0} );

  // base interface
  IntegrateCellGroups<TopoD3>::integrate( ResidualCell_DGBR2(fcnPDE, rsdPDEGlobal),
                                          xfld, (qfld, rfld), &quadratureOrder, 1 );

  Real rsdPDETrue[nDOF];

  //PDE residual: (advection) + (diffusion) + (source)
  rsdPDETrue[0] = (7/6.) + (-2021/500.) + (-863/2400.); // Basis function 1
  rsdPDETrue[1] = (-77/120.) + (3097/2000.) + (-1003/2400.); // Basis function 2
  rsdPDETrue[2] = (-7/60.) + (603/500.) + (-187/600.); // Basis function 3
  rsdPDETrue[3] = (-49/120.) + (103/80.) + (-167/300.); // Basis function 4

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDEGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[2], rsdPDEGlobal[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[3], rsdPDEGlobal[3], small_tol, close_tol );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
