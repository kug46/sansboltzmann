// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// SetFieldInteriorTrace_DGBR2_LiftedQuantity_TwoPhaseAV_btest
// testing of interior trace lifted quantity calculations for DG BR2 with TwoPhase

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/PorousMedia/Q2DPrimitive_pnSw.h"
#include "pde/PorousMedia/TraitsTwoPhaseArtificialViscosity.h"
#include "pde/PorousMedia/PDETwoPhase_ArtificialViscosity2D.h"
#include "pde/PorousMedia/BCTwoPhaseArtificialViscosity2D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/PermeabilityModel2D.h"
#include "pde/PorousMedia/CapillaryModel.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"
#include "Field/HField/GenHFieldArea_CG.h"

#include "Discretization/DG/DiscretizationDGBR2.h"
#include "Discretization/DG/SetFieldInteriorTrace_DGBR2_LiftedQuantity.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"

#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( SetFieldInteriorTrace_DGBR2_LiftedQuantity_TwoPhaseAV_btest_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_4Triangle_X1Q0S0 )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef PermeabilityModel2D_QuadBlock RockPermModel;
  typedef QTypePrimitive_pnSw QType;

  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel,
                              CapillaryModel> TraitsModelTwoPhaseClass;

  typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity,
                                            TraitsModelTwoPhaseClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeTwoPhaseArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeTwoPhaseArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_TwoPhase<PDEBaseClass> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef IntegrandInteriorTrace_DGBR2<NDPDEClass> IntegrandClass;

  // PDE
  const Real pref = 14.7;

  DensityModel rhow(62.4, 5.0e-6, pref);
  DensityModel rhon(52.1, 1.5e-5, pref);

  PorosityModel phi(0.3, 3.0e-6, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  const Real ft3_per_bbl = 5.61458;
  const Real conversion = (1.127e-3)*ft3_per_bbl; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real Kref = conversion*200; //mD

  DLA::MatrixS<2,2,Real> Kref_mat = {{Kref, 0.0}, {0.0, Kref}};

  RockPermModel K(Kref_mat);

  CapillaryModel pc(0.0);

  // TwoPhase PDE with AV
  int order = 1;
  bool hasSpaceTimeDiffusion = false;
  PDEBaseClass pdeTwoPhaseAV(order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  SensorAdvectiveFlux sensor_adv(0.0, 0.0);
  SensorViscousFlux sensor_visc(3.0);
  SensorSource sensor_source(pdeTwoPhaseAV);

  // AV PDE with sensor equation
  bool isSteady = false;
  NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteady,
                 order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 3 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasSourceTrace() == false );
  BOOST_CHECK( pde.hasSourceLiftedQuantity() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid: P1 (aka X1)
  XField2D_4Triangle_X1_1Group xfld;

  //Compute generalized log H-tensor field
  GenHField_CG<PhysD2, TopoD2> Hfld(xfld);

  // solution field
  int qorder = 0;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 4 );

  Real Sw0 = 0.2;
  Real jump0 =  0.1;
  Real jump1 = -0.05;
  Real jump2 =  0.2;

  // triangle solution data
  qfld.DOF(0) = {2500, Sw0, 0.02};
  qfld.DOF(1) = {2510, Sw0 + jump0, 0.05};
  qfld.DOF(2) = {2520, Sw0 + jump1, 0.16};
  qfld.DOF(3) = {2520, Sw0 + jump2, 0.23};

  // lifted quantity field
  Field_DG_Cell<PhysD2, TopoD2, Real> sfld(xfld, qorder, BasisFunctionCategory_Legendre);
  BOOST_REQUIRE_EQUAL( sfld.nDOF(), 4 );

  // Compute the field of inverse mass matrices for the lifted quantity field
  FieldDataInvMassMatrix_Cell mmfld(sfld);

  // BR2 discretization
  Real viscousEtaParameter = 3.0;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcn( pde, disc, {0} );

  // quadrature rule (quadratic: basis & flux both linear)
  int quadratureOrder = 3;

  sfld = 0.0;
  IntegrateInteriorTraceGroups<TopoD2>::integrate(SetFieldInteriorTrace_DGBR2_LiftedQuantity(fcn, mmfld),
                                                  (Hfld, xfld), (qfld, sfld), &quadratureOrder, 1);

  Real Ntrace = 3.0;

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  Real eps = 1e-3;

  Real sTrue[4];
  sTrue[0] = (smoothabs0(jump0, eps) + smoothabs0(jump1, eps) + smoothabs0(jump2, eps)) / Ntrace;
  sTrue[1] = smoothabs0(jump0, eps) / Ntrace;
  sTrue[2] = smoothabs0(jump1, eps) / Ntrace;
  sTrue[3] = smoothabs0(jump2, eps) / Ntrace;

  SANS_CHECK_CLOSE( sTrue[0], sfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( sTrue[1], sfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( sTrue[2], sfld.DOF(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( sTrue[3], sfld.DOF(3), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_4Triangle_X1Q1S0 )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef PermeabilityModel2D_QuadBlock RockPermModel;
  typedef QTypePrimitive_pnSw QType;

  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel,
                              CapillaryModel> TraitsModelTwoPhaseClass;

  typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity,
                                            TraitsModelTwoPhaseClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeTwoPhaseArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeTwoPhaseArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_TwoPhase<PDEBaseClass> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef IntegrandInteriorTrace_DGBR2<NDPDEClass> IntegrandClass;

  // PDE
  const Real pref = 14.7;

  DensityModel rhow(62.4, 5.0e-6, pref);
  DensityModel rhon(52.1, 1.5e-5, pref);

  PorosityModel phi(0.3, 3.0e-6, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  const Real ft3_per_bbl = 5.61458;
  const Real conversion = (1.127e-3)*ft3_per_bbl; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real Kref = conversion*200; //mD

  DLA::MatrixS<2,2,Real> Kref_mat = {{Kref, 0.0}, {0.0, Kref}};

  RockPermModel K(Kref_mat);

  CapillaryModel pc(0.0);

  // TwoPhase PDE with AV
  int order = 1;
  bool hasSpaceTimeDiffusion = false;
  PDEBaseClass pdeTwoPhaseAV(order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  SensorAdvectiveFlux sensor_adv(0.0, 0.0);
  SensorViscousFlux sensor_visc(3.0);
  SensorSource sensor_source(pdeTwoPhaseAV);

  // AV PDE with sensor equation
  bool isSteady = false;
  NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteady,
                 order, hasSpaceTimeDiffusion, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 3 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasSourceTrace() == false );
  BOOST_CHECK( pde.hasSourceLiftedQuantity() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid: P1 (aka X1)
  XField2D_4Triangle_X1_1Group xfld;

  //Compute generalized log H-tensor field
  GenHField_CG<PhysD2, TopoD2> Hfld(xfld);

  // solution field
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 4*3 );

  //Saturation values at triangle nodes
  Real Sw_tri0[3] = {0.9, 0.7, 0.8}; //center triangle should have larger values than neighbors for the "true" results to be valid.
  Real Sw_tri1[3] = {0.5, 0.1, 0.4};
  Real Sw_tri2[3] = {0.0, 0.3, 0.2};
  Real Sw_tri3[3] = {0.3, 0.2, 0.6};

  // triangle solution data
  qfld.DOF(0) = {2500, Sw_tri0[0], 0.02};
  qfld.DOF(1) = {2510, Sw_tri0[1], 0.05};
  qfld.DOF(2) = {2520, Sw_tri0[2], 0.16};

  qfld.DOF(3) = {2530, Sw_tri1[0], 0.25};
  qfld.DOF(4) = {2580, Sw_tri1[1], 0.45};
  qfld.DOF(5) = {2510, Sw_tri1[2], 0.15};

  qfld.DOF(6) = {2540, Sw_tri2[0], 0.35};
  qfld.DOF(7) = {2570, Sw_tri2[1], 0.15};
  qfld.DOF(8) = {2585, Sw_tri2[2], 0.65};

  qfld.DOF( 9) = {2500, Sw_tri3[0], 0.35};
  qfld.DOF(10) = {2508, Sw_tri3[1], 0.15};
  qfld.DOF(11) = {2550, Sw_tri3[2], 0.65};

  // lifted quantity field
  int sorder = 0;
  Field_DG_Cell<PhysD2, TopoD2, Real> sfld(xfld, sorder, BasisFunctionCategory_Legendre);
  BOOST_REQUIRE_EQUAL( sfld.nDOF(), 4 );

  // Compute the field of inverse mass matrices for the lifted quantity field
  FieldDataInvMassMatrix_Cell mmfld(sfld);

  // BR2 discretization
  Real viscousEtaParameter = 3.0;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcn( pde, disc, {0} );

  // quadrature rule (quadratic: basis & flux both linear)
  int quadratureOrder = 3;

  sfld = 0.0;
  IntegrateInteriorTraceGroups<TopoD2>::integrate(SetFieldInteriorTrace_DGBR2_LiftedQuantity(fcn, mmfld),
                                                  (Hfld, xfld), (qfld, sfld), &quadratureOrder, 1);

  Real Ntrace = 3.0;

  Real eps = 1e-3;

  //our "true" answers are not fully accurate because of the inexact integration with the smoothabs0
  const Real small_tol = 1e-10;
  const Real close_tol = 1e-4;

  Real avg_jump0 = 0.5*(smoothabs0(Sw_tri0[1] - Sw_tri1[2], eps) + smoothabs0(Sw_tri0[2] - Sw_tri1[1], eps));
  Real avg_jump1 = 0.5*(smoothabs0(Sw_tri0[2] - Sw_tri2[0], eps) + smoothabs0(Sw_tri0[0] - Sw_tri2[2], eps));
  Real avg_jump2 = 0.5*(smoothabs0(Sw_tri0[0] - Sw_tri3[1], eps) + smoothabs0(Sw_tri0[1] - Sw_tri3[0], eps));

  Real sTrue[4];
  sTrue[0] = (avg_jump0 + avg_jump1 + avg_jump2) / Ntrace;
  sTrue[1] = avg_jump0 / Ntrace;
  sTrue[2] = avg_jump1 / Ntrace;
  sTrue[3] = avg_jump2 / Ntrace;

  SANS_CHECK_CLOSE( sTrue[0], sfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( sTrue[1], sfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( sTrue[2], sfld.DOF(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( sTrue[3], sfld.DOF(3), small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
