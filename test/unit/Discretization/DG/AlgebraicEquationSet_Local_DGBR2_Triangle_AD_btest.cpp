// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// LocalSolve_Split_DGBR2_Triangle_AD_btest
// testing of 2-D DG with Advection-Diffusion on triangles

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <fstream>
#include <string>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/AlgebraicEquationSet_Type.h" // Meta class for getting local EqSet from a global EqSet

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/AlgebraicEquationSet_Local_DG.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"

#include "Field/Local/Field_Local.h"
#include "Field/Local/FieldLift_Local.h"
#include "Field/Local/XField_LocalPatch.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/InverseLUP.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"


#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( AlgebraicEquationSet_Local_DGBR2_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Isotropic_DGBR2_AD_test )
{
  typedef ScalarFunction2D_DoubleBL SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef Field_DG_Cell<PhysD2, TopoD2, ArrayQ> QField2D_DG_Cell;
  typedef FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> RField2D_DG_Cell;
  typedef Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> LGField2D_DG_Trace;

  typedef AlgebraicEquationSet_Local_DG<NDPDEClass, BCNDConvertSpace, BCVector,
                                        DGBR2, XField<PhysD2, TopoD2>, AlgebraicEquationSet_DGBR2> AlgEqnSet_Local;
  typedef AlgEqnSet_Local::BCParams BCParams;

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef AlgEqnSet_Local::SystemMatrix SystemMatrixClass_Local;
  typedef AlgEqnSet_Local::SystemVector SystemVectorClass_Local;
  typedef AlgEqnSet_Local::SystemNonZeroPattern SystemNonZeroPattern_Local;

  int ii = 2;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 1;

  int main_group = 0;
  int main_cell = 0;
  ElementSplitType split_type = ElementSplitType::Edge;
  int split_edge_index = 0;

//  std::vector<int> global_itracegroup_list = {0,1,2};
  std::vector<int> global_btracegroup_list = {0,1,2,3};
  std::vector<int> local_itracegroup_list = {0,1};

  // PDE
  Real a = 1.0;
  Real b = 1.0;
  AdvectiveFlux2D_Uniform adv( a, b );

  Real nu = 0.273;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_UniformGrad source(0.3, 0.5, 0.1);

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.a] = a;
  solnArgs[NDSolutionExact::ParamsType::params.b] = b;
  solnArgs[NDSolutionExact::ParamsType::params.nu] = nu;

  NDSolutionExact solnExact(solnArgs);

  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact));

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict DoubleBL;
  DoubleBL[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.DoubleBL;
  DoubleBL[SolutionExact::ParamsType::params.a] = a;
  DoubleBL[SolutionExact::ParamsType::params.b] = b;
  DoubleBL[SolutionExact::ParamsType::params.nu] = nu;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = DoubleBL;

  PyDict PyBCList;
  PyBCList["BCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCName"] = global_btracegroup_list;

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  NDOutputClass fcnSqError(solnExact);
  OutputIntegrandClass errIntegrandGlobal(fcnSqError, {0});
  OutputIntegrandClass errIntegrandLocal(fcnSqError, {0,1});

  //Solution
  QField2D_DG_Cell qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0;

  //Lifting operators
  RField2D_DG_Cell rfld(xfld, order, BasisFunctionCategory_Legendre);
  rfld = 0;

  //Lagrange multipliers
  LGField2D_DG_Trace lgfld( xfld, order-1, BasisFunctionCategory_Legendre );
  lgfld = 0;

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  mpi::communicator comm_local = xfld.comm()->split(xfld.comm()->rank());

  //Extract the local mesh for the given element
  XField_LocalPatchConstructor<PhysD2,Triangle> xfld_constructor(comm_local, connectivity, main_group, main_cell, SpaceType::Discontinuous);

  // create an split patch
  XField_LocalPatch<PhysD2,Triangle> xfld_local( xfld_constructor, split_type, split_edge_index);

  Field_Local<QField2D_DG_Cell> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);
  FieldLift_Local<RField2D_DG_Cell> rfld_local(xfld_local, rfld, order, BasisFunctionCategory_Legendre);
  Field_Local<LGField2D_DG_Trace> lgfld_local(xfld_local, lgfld, order-1, BasisFunctionCategory_Legendre);

  int nMainBTraces = 0;
  std::vector<int> MainBTraceGroup_list;

  for (int i=0; i < lgfld_local.nBoundaryTraceGroups(); i++)
  {
    MainBTraceGroup_list.push_back(i);
    nMainBTraces += lgfld_local.getBoundaryTraceGroup<Line>(i).nElem();
  }

//  std::cout<<"nMainBTraces:"<<nMainBTraces<<std::endl;

  std::map<std::string, std::vector<int>> BCBoundaryGroups_local;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups_local["BCName"] = MainBTraceGroup_list;

  QuadratureOrder quadratureOrder_local( xfld_local, -1 );
  std::vector<Real> eqnsettol = {1e-12, 1e-12};

  std::vector<int> localCellGroups = {0};

  AlgEqnSet_Local PrimalEqSet_Local(xfld_local, qfld_local, rfld_local, lgfld_local,
                                    pde, disc, quadratureOrder_local, ResidualNorm_Default, eqnsettol,
                                    localCellGroups, local_itracegroup_list, PyBCList, BCBoundaryGroups_local);

  //------------------------------------------------------------------------------------------

  // Local residual
  SystemVectorClass_Local q_local(PrimalEqSet_Local.vectorStateSize());
  PrimalEqSet_Local.fillSystemVector(q_local);

  BOOST_CHECK_EQUAL( q_local.m(), 2 );
  BOOST_CHECK_EQUAL( q_local[AlgEqnSet_Local::iq].m(), 2*3 );
  BOOST_CHECK_EQUAL( q_local[AlgEqnSet_Local::ilg].m(), 2*1 );

  SystemVectorClass_Local rsd_local(PrimalEqSet_Local.vectorEqSize());
  rsd_local = 0;
  PrimalEqSet_Local.residual(q_local, rsd_local);
  std::vector<std::vector<Real>> rsdNorm = PrimalEqSet_Local.residualNorm(rsd_local);

#if 0 //Print local residual vector
  cout <<endl << "Solution residual:" <<endl;
  for (int k = 0; k < rsd_local[0].m(); k++) cout << k << "\t" << rsd_local[0][k] <<endl;

  cout <<endl << "Lagrange multiplier residual:" <<endl;
  for (int k = 0; k < rsd_local[1].m(); k++) cout << k << "\t" << rsd_local[1][k] << endl;
#endif

#ifdef __clang_analyzer__
  return; // clang analyzer complains about the matrix size below
#endif

  //Jacobian
  SystemNonZeroPattern_Local nz_local(PrimalEqSet_Local.matrixSize());
  SystemMatrixClass_Local jac_local(nz_local);

  BOOST_CHECK_EQUAL( jac_local.m(), 2 );
  BOOST_CHECK_EQUAL( jac_local.n(), 2 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::iq).m(), 2*3 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::iq).n(), 2*3 );

  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::ilg).m(), 2*3 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::ilg).n(), 2*1 );

  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::iq).m(), 2*1 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::iq).n(), 2*3 );

  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::ilg).m(), 2*1 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::ilg).n(), 2*1 );

  jac_local = 0;
  PrimalEqSet_Local.jacobian(jac_local);

  //Local solve - with extracted sub-system
  SystemVectorClass_Local sln_local(rsd_local.size());
  sln_local = DLA::InverseLUP::Solve(jac_local,rsd_local);

  //Update local solution fields
  q_local -= sln_local;
  PrimalEqSet_Local.setSolutionField(q_local);

  //Re-compute residual and check if it's zero
  rsd_local = 0;
  PrimalEqSet_Local.residual(rsd_local);
  rsdNorm = PrimalEqSet_Local.residualNorm(rsd_local);

  BOOST_REQUIRE( PrimalEqSet_Local.convergedResidual(rsdNorm) );

  //---------------------------------------------//
  // Testing Alternative FieldBundle Constructor //
  //---------------------------------------------//

  typedef typename AlgEqnSet_Local::FieldBundle_Local FieldBundle_Local;

  std::vector<int> active_BCGroup_list = AlgEqnSet_Local::BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  FieldBundle_DGBR2<PhysD2,TopoD2,ArrayQ> flds(xfld, order, BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_BCGroup_list );

  flds.qfld  = 0;
  flds.rfld  = 0;
  flds.lgfld = 0;

  std::vector<int> active_BCGroup_list_local = AlgEqnSet_Local::BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups_local);

  FieldBundle_Local flds_local( xfld_local, flds, order, {active_BCGroup_list_local} );

  AlgEqnSet_Local PrimalEqSet_Local2( xfld_local, flds_local, {}, pde, disc, quadratureOrder_local, ResidualNorm_Default, eqnsettol,
                                      localCellGroups, local_itracegroup_list, PyBCList, BCBoundaryGroups_local);

  //------------------------------------------------------------------------------------------

  // Local residual
  SystemVectorClass_Local q_local2(PrimalEqSet_Local2.vectorStateSize());
  PrimalEqSet_Local2.fillSystemVector(q_local2);

  BOOST_CHECK_EQUAL( q_local2.m(), 2 );
  BOOST_CHECK_EQUAL( q_local2[AlgEqnSet_Local::iq].m(), 2*3 );
  BOOST_CHECK_EQUAL( q_local2[AlgEqnSet_Local::ilg].m(), 2*1 );

  SystemVectorClass_Local rsd_local2(PrimalEqSet_Local2.vectorEqSize());
  rsd_local2 = 0;
  PrimalEqSet_Local2.residual(q_local2, rsd_local2);

#if 0 //Print local residual vector
  cout <<endl << "Solution residual:" <<endl;
  for (int k = 0; k < rsd_local2[0].m(); k++) cout << k << "\t" << rsd_local2[0][k] <<endl;

  cout <<endl << "Lagrange multiplier residual:" <<endl;
  for (int k = 0; k < rsd_local2[1].m(); k++) cout << k << "\t" << rsd_local2[1][k] << endl;
#endif

#ifdef __clang_analyzer__
  return; // clang analyzer complains about the matrix size below
#endif

  //Jacobian
  SystemNonZeroPattern_Local nz_local2(PrimalEqSet_Local2.matrixSize());
  SystemMatrixClass_Local jac_local2(nz_local2);

  BOOST_CHECK_EQUAL( jac_local2.m(), 2 );
  BOOST_CHECK_EQUAL( jac_local2.n(), 2 );
  BOOST_CHECK_EQUAL( jac_local2(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::iq).m(), 2*3 );
  BOOST_CHECK_EQUAL( jac_local2(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::iq).n(), 2*3 );

  BOOST_CHECK_EQUAL( jac_local2(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::ilg).m(), 2*3 );
  BOOST_CHECK_EQUAL( jac_local2(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::ilg).n(), 2*1 );

  BOOST_CHECK_EQUAL( jac_local2(AlgEqnSet_Local::iBC, AlgEqnSet_Local::iq).m(), 2*1 );
  BOOST_CHECK_EQUAL( jac_local2(AlgEqnSet_Local::iBC, AlgEqnSet_Local::iq).n(), 2*3 );

  BOOST_CHECK_EQUAL( jac_local2(AlgEqnSet_Local::iBC, AlgEqnSet_Local::ilg).m(), 2*1 );
  BOOST_CHECK_EQUAL( jac_local2(AlgEqnSet_Local::iBC, AlgEqnSet_Local::ilg).n(), 2*1 );

  jac_local2 = 0;
  PrimalEqSet_Local2.jacobian(jac_local2);

  //Local solve - with extracted sub-system
  SystemVectorClass_Local sln_local2(rsd_local2.size());
  sln_local2 = DLA::InverseLUP::Solve(jac_local2,rsd_local2);

  //Update local solution fields
  q_local2 -= sln_local2;
  PrimalEqSet_Local2.setSolutionField(q_local2);

  //Re-compute residual and check if it's zero
  rsd_local2 = 0;
  PrimalEqSet_Local2.residual(rsd_local2);
  rsdNorm = PrimalEqSet_Local2.residualNorm(rsd_local2);

  BOOST_REQUIRE( PrimalEqSet_Local2.convergedResidual(rsdNorm) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AlgebraicEquationSet_Local_DGBR2_from_Global_AD_test )
{
  typedef ScalarFunction2D_DoubleBL SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef Field_DG_Cell<PhysD2, TopoD2, ArrayQ> QField2D_DG_Cell;
  typedef FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> RField2D_DG_Cell;
  typedef Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> LGField2D_DG_Trace;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
              AlgEqSetTraits_Sparse, DGBR2, XField<PhysD2, TopoD2>> AlgEqnSet;
  typedef typename LocalEquationSet<AlgEqnSet>::type AlgEqnSet_Local;
  typedef AlgEqnSet_Local::BCParams BCParams;

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef AlgEqnSet_Local::SystemMatrix SystemMatrixClass_Local;
  typedef AlgEqnSet_Local::SystemVector SystemVectorClass_Local;
  typedef AlgEqnSet_Local::SystemNonZeroPattern SystemNonZeroPattern_Local;

  int ii = 2;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 1;

  int main_group = 0;
  int main_cell = 0;
  ElementSplitType split_type = ElementSplitType::Edge;
  int split_edge_index = 0;

//  std::vector<int> global_itracegroup_list = {0,1,2};
  std::vector<int> global_btracegroup_list = {0,1,2,3};
  std::vector<int> local_itracegroup_list = {0,1};

  // PDE
  Real a = 1.0;
  Real b = 1.0;
  AdvectiveFlux2D_Uniform adv( a, b );

  Real nu = 0.273;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_UniformGrad source(0.3, 0.5, 0.1);

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.a] = a;
  solnArgs[NDSolutionExact::ParamsType::params.b] = b;
  solnArgs[NDSolutionExact::ParamsType::params.nu] = nu;

  NDSolutionExact solnExact(solnArgs);

  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact));

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict DoubleBL;
  DoubleBL[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.DoubleBL;
  DoubleBL[SolutionExact::ParamsType::params.a] = a;
  DoubleBL[SolutionExact::ParamsType::params.b] = b;
  DoubleBL[SolutionExact::ParamsType::params.nu] = nu;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = DoubleBL;

  PyDict PyBCList;
  PyBCList["BCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCName"] = global_btracegroup_list;

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  NDOutputClass fcnSqError(solnExact);
  OutputIntegrandClass errIntegrandGlobal(fcnSqError, {0});
  OutputIntegrandClass errIntegrandLocal(fcnSqError, {0,1});

  //Solution
  QField2D_DG_Cell qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0;

  //Lifting operators
  RField2D_DG_Cell rfld(xfld, order, BasisFunctionCategory_Legendre);
  rfld = 0;

  //Lagrange multipliers
  LGField2D_DG_Trace lgfld( xfld, order-1, BasisFunctionCategory_Legendre );
  lgfld = 0;

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  mpi::communicator comm_local = xfld.comm()->split(xfld.comm()->rank());

  //Extract the local mesh for the given element
  XField_LocalPatchConstructor<PhysD2,Triangle> xfld_constructor(comm_local, connectivity, main_group, main_cell, SpaceType::Discontinuous);

  // create an split patch
  XField_LocalPatch<PhysD2,Triangle> xfld_local( xfld_constructor, split_type, split_edge_index);

  Field_Local<QField2D_DG_Cell> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);
  FieldLift_Local<RField2D_DG_Cell> rfld_local(xfld_local, rfld, order, BasisFunctionCategory_Legendre);
  Field_Local<LGField2D_DG_Trace> lgfld_local(xfld_local, lgfld, order-1, BasisFunctionCategory_Legendre);

  int nMainBTraces = 0;
  std::vector<int> MainBTraceGroup_list;

  for (int i=0; i < lgfld_local.nBoundaryTraceGroups(); i++)
  {
    MainBTraceGroup_list.push_back(i);
    nMainBTraces += lgfld_local.getBoundaryTraceGroup<Line>(i).nElem();
  }

//  std::cout<<"nMainBTraces:"<<nMainBTraces<<std::endl;

  std::map<std::string, std::vector<int>> BCBoundaryGroups_local;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups_local["BCName"] = MainBTraceGroup_list;

  QuadratureOrder quadratureOrder_local( xfld_local, -1 );
  std::vector<Real> eqnsettol = {1e-12, 1e-12};

  std::vector<int> localCellGroups = {0};

  AlgEqnSet_Local PrimalEqSet_Local(xfld_local, qfld_local, rfld_local, lgfld_local,
                                    pde, disc, quadratureOrder_local, ResidualNorm_Default, eqnsettol,
                                    localCellGroups, local_itracegroup_list, PyBCList, BCBoundaryGroups_local);

  //------------------------------------------------------------------------------------------

  // Local residual
  SystemVectorClass_Local q_local(PrimalEqSet_Local.vectorStateSize());
  PrimalEqSet_Local.fillSystemVector(q_local);

  BOOST_CHECK_EQUAL( q_local.m(), 2 );
  BOOST_CHECK_EQUAL( q_local[AlgEqnSet_Local::iq].m(), 2*3 );
  BOOST_CHECK_EQUAL( q_local[AlgEqnSet_Local::ilg].m(), 2*1 );

  SystemVectorClass_Local rsd_local(PrimalEqSet_Local.vectorEqSize());
  rsd_local = 0;
  PrimalEqSet_Local.residual(q_local, rsd_local);

#if 0 //Print local residual vector
  cout <<endl << "Solution residual:" <<endl;
  for (int k = 0; k < rsd_local[0].m(); k++) cout << k << "\t" << rsd_local[0][k] <<endl;

  cout <<endl << "Lagrange multiplier residual:" <<endl;
  for (int k = 0; k < rsd_local[1].m(); k++) cout << k << "\t" << rsd_local[1][k] << endl;
#endif

#ifdef __clang_analyzer__
  return; // clang analyzer complains about the matrix size below
#endif

  //Jacobian
  SystemNonZeroPattern_Local nz_local(PrimalEqSet_Local.matrixSize());
  SystemMatrixClass_Local jac_local(nz_local);

  BOOST_CHECK_EQUAL( jac_local.m(), 2 );
  BOOST_CHECK_EQUAL( jac_local.n(), 2 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::iq).m(), 2*3 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::iq).n(), 2*3 );

  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::ilg).m(), 2*3 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::ilg).n(), 2*1 );

  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::iq).m(), 2*1 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::iq).n(), 2*3 );

  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::ilg).m(), 2*1 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::ilg).n(), 2*1 );

  jac_local = 0;
  PrimalEqSet_Local.jacobian(jac_local);

  //Local solve - with extracted sub-system
  SystemVectorClass_Local sln_local(rsd_local.size());
  sln_local = DLA::InverseLUP::Solve(jac_local,rsd_local);

  //Update local solution fields
  q_local -= sln_local;
  PrimalEqSet_Local.setSolutionField(q_local);

  //Re-compute residual and check if it's zero
  rsd_local = 0;
  PrimalEqSet_Local.residual(rsd_local);
  std::vector<std::vector<Real>> rsdNorm = PrimalEqSet_Local.residualNorm(rsd_local);

  BOOST_REQUIRE( PrimalEqSet_Local.convergedResidual(rsdNorm) );


  //---------------------------------------------//
  // Testing Alternative FieldBundle Constructor //
  //---------------------------------------------//

  typedef typename AlgEqnSet_Local::FieldBundle_Local FieldBundle_Local;

  std::vector<int> active_BCGroup_list = AlgEqnSet_Local::BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  FieldBundle_DGBR2<PhysD2,TopoD2,ArrayQ> flds(xfld, order, BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_BCGroup_list );

  flds.qfld  = 0;
  flds.rfld  = 0;
  flds.lgfld = 0;

  std::vector<int> active_BCGroup_list_local = AlgEqnSet_Local::BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups_local);

  FieldBundle_Local flds_local( xfld_local, flds, order, {active_BCGroup_list_local} );

  AlgEqnSet_Local PrimalEqSet_Local2( xfld_local, flds_local, {}, pde, disc, quadratureOrder_local, ResidualNorm_Default, eqnsettol,
                                      localCellGroups, local_itracegroup_list, PyBCList, BCBoundaryGroups_local);

  //------------------------------------------------------------------------------------------

  // Local residual
  SystemVectorClass_Local q_local2(PrimalEqSet_Local2.vectorStateSize());
  PrimalEqSet_Local2.fillSystemVector(q_local2);

  BOOST_CHECK_EQUAL( q_local2.m(), 2 );
  BOOST_CHECK_EQUAL( q_local2[AlgEqnSet_Local::iq].m(), 2*3 );
  BOOST_CHECK_EQUAL( q_local2[AlgEqnSet_Local::ilg].m(), 2*1 );

  SystemVectorClass_Local rsd_local2(PrimalEqSet_Local2.vectorEqSize());
  rsd_local2 = 0;
  PrimalEqSet_Local2.residual(q_local2, rsd_local2);

#if 0 //Print local residual vector
  cout <<endl << "Solution residual:" <<endl;
  for (int k = 0; k < rsd_local2[0].m(); k++) cout << k << "\t" << rsd_local2[0][k] <<endl;

  cout <<endl << "Lagrange multiplier residual:" <<endl;
  for (int k = 0; k < rsd_local2[1].m(); k++) cout << k << "\t" << rsd_local2[1][k] << endl;
#endif

#ifdef __clang_analyzer__
  return; // clang analyzer complains about the matrix size below
#endif

  //Jacobian
  SystemNonZeroPattern_Local nz_local2(PrimalEqSet_Local2.matrixSize());
  SystemMatrixClass_Local jac_local2(nz_local2);

  BOOST_CHECK_EQUAL( jac_local2.m(), 2 );
  BOOST_CHECK_EQUAL( jac_local2.n(), 2 );
  BOOST_CHECK_EQUAL( jac_local2(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::iq).m(), 2*3 );
  BOOST_CHECK_EQUAL( jac_local2(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::iq).n(), 2*3 );

  BOOST_CHECK_EQUAL( jac_local2(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::ilg).m(), 2*3 );
  BOOST_CHECK_EQUAL( jac_local2(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::ilg).n(), 2*1 );

  BOOST_CHECK_EQUAL( jac_local2(AlgEqnSet_Local::iBC, AlgEqnSet_Local::iq).m(), 2*1 );
  BOOST_CHECK_EQUAL( jac_local2(AlgEqnSet_Local::iBC, AlgEqnSet_Local::iq).n(), 2*3 );

  BOOST_CHECK_EQUAL( jac_local2(AlgEqnSet_Local::iBC, AlgEqnSet_Local::ilg).m(), 2*1 );
  BOOST_CHECK_EQUAL( jac_local2(AlgEqnSet_Local::iBC, AlgEqnSet_Local::ilg).n(), 2*1 );

  jac_local2 = 0;
  PrimalEqSet_Local2.jacobian(jac_local2);

  //Local solve - with extracted sub-system
  SystemVectorClass_Local sln_local2(rsd_local2.size());
  sln_local2 = DLA::InverseLUP::Solve(jac_local2,rsd_local2);

  //Update local solution fields
  q_local2 -= sln_local2;
  PrimalEqSet_Local2.setSolutionField(q_local2);

  //Re-compute residual and check if it's zero
  rsd_local2 = 0;
  PrimalEqSet_Local2.residual(rsd_local2);
  rsdNorm = PrimalEqSet_Local2.residualNorm(rsd_local2);

  BOOST_REQUIRE( PrimalEqSet_Local2.convergedResidual(rsdNorm) );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
