// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// testing of DGBR2 cell-integral jacobian: advection-diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

#include "Field/FieldData/FieldDataMatrixD_CellLift.h"

#include "Discretization/DG/IntegrandCell_DGBR2.h"
#include "Discretization/DG/JacobianCell_DGBR2.h"
#include "Discretization/DG/ResidualCell_DGBR2.h"
#include "Discretization/IntegrateCellGroups.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianCell_DGBR2_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_1D_2Line_X1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,PhysD1::D,MatrixQ> RowMatrixQ;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 0.4, b = -0.3;
  Source1D_UniformGrad source(a, b);

  PDEClass pde( adv, visc, source );

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcn( pde, disc, {0} );

  // grid: X1
  XField1D xfld(2);

  // solution
  for ( int qorder = 0; qorder <= 3; qorder++ )
  {
    Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

    const int qDOF = qfld.nDOF();

    // solution data
    for (int i = 0; i < qDOF; i++)
      qfld.DOF(i) = sin(PI*i/((Real)qDOF));

    // lifting operator
    FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Legendre);

    const int rDOF = rfld.nDOF();

    // lifting operator data
    for (int i = 0; i < rDOF; i++)
      rfld.DOF(i) = cos(PI*i/((Real)rDOF));

    // quadrature rule
    int quadratureOrder = 6;

    // jacobian via FD w/ residual operator; assumes scalar PDE
    // topology-specific single group interface

    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
    DLA::MatrixD<Real> jacPDE_q(qDOF,qDOF);

    jacPDE_q = 0;

    rsdPDEGlobal0 = 0;
    IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcn, rsdPDEGlobal0),
                                            xfld, (qfld, rfld), &quadratureOrder, 1 );

    //wrt q
    for (int j = 0; j < qDOF; j++)
    {
      qfld.DOF(j) += 1;

      rsdPDEGlobal1 = 0;
      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_DGBR2(fcn, rsdPDEGlobal1),
                                              xfld, (qfld, rfld), &quadratureOrder, 1 );

      qfld.DOF(j) -= 1;

      for (int i = 0; i < qDOF; i++)
        jacPDE_q(i,j) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    }

  #if 0
    cout << "FD: jacobian = " << endl << jacPDE_q << endl;
  #endif

    FieldDataMatrixD_CellLift<RowMatrixQ> jacPDEGlob_R(rfld);
    jacPDEGlob_R = 0; // Lifting operator jacobian from cell integral

    // jacobian via Surreal
    DLA::MatrixD<MatrixQ> mtxPDEGlob_q(qDOF,qDOF);

    mtxPDEGlob_q = 0;
    IntegrateCellGroups<TopoD1>::integrate( JacobianCell_DGBR2(fcn, mtxPDEGlob_q, jacPDEGlob_R),
                                            xfld, (qfld, rfld), &quadratureOrder, 1 );

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-10;

    for (int i = 0; i < qDOF; i++)
      for (int j = 0; j < qDOF; j++)
        SANS_CHECK_CLOSE( jacPDE_q(i,j), mtxPDEGlob_q(i,j), small_tol, close_tol );
  } // qorder
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_2D_2Triangle_X1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,PhysD2::D,MatrixQ> RowMatrixQ;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.348;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real a = 0.4, b = 0.7, c = -0.2;
  Source2D_UniformGrad source(a, b, c);

  PDEClass pde( adv, visc, source );

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcn( pde, disc, {0} );

  // grid: X1
  XField2D_2Triangle_X1_1Group xfld;

  // solution
  for ( int qorder = 0; qorder <= 3; qorder++ )
  {
    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

    const int qDOF = qfld.nDOF();

    // solution data
    for (int i = 0; i < qDOF; i++)
      qfld.DOF(i) = sin(PI*i/((Real)qDOF));

    // lifting operator
    FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, qorder, BasisFunctionCategory_Legendre);

    const int rDOF = rfld.nDOF();

    // lifting operator data
    for (int i = 0; i < rDOF; i++)
      rfld.DOF(i) = {cos(PI*i/((Real)rDOF)), sin(PI*i/((Real)rDOF))};

    // quadrature rule
    int quadratureOrder = 6;

    // jacobian via FD w/ residual operator; assumes scalar PDE
    // topology-specific single group interface

    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
    DLA::MatrixD<Real> jacPDE_q(qDOF,qDOF);

    jacPDE_q = 0;

    rsdPDEGlobal0 = 0;
    IntegrateCellGroups<TopoD2>::integrate( ResidualCell_DGBR2(fcn, rsdPDEGlobal0),
                                            xfld, (qfld, rfld), &quadratureOrder, 1 );

    //wrt q
    for (int j = 0; j < qDOF; j++)
    {
      qfld.DOF(j) += 1;

      rsdPDEGlobal1 = 0;
      IntegrateCellGroups<TopoD2>::integrate( ResidualCell_DGBR2(fcn, rsdPDEGlobal1),
                                              xfld, (qfld, rfld), &quadratureOrder, 1 );

      qfld.DOF(j) -= 1;

      for (int i = 0; i < qDOF; i++)
        jacPDE_q(i,j) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    }

  #if 0
    cout << "FD: jacobian = " << endl << jacPDE_q << endl;
  #endif

    FieldDataMatrixD_CellLift<RowMatrixQ> jacPDEGlob_R(rfld);
    jacPDEGlob_R = 0; // Lifting operator jacobian from cell integral

    // jacobian via Surreal
    DLA::MatrixD<MatrixQ> mtxPDEGlob_q(qDOF,qDOF);

    mtxPDEGlob_q = 0;
    IntegrateCellGroups<TopoD2>::integrate( JacobianCell_DGBR2(fcn, mtxPDEGlob_q, jacPDEGlob_R),
                                            xfld, (qfld, rfld), &quadratureOrder, 1 );

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-10;

    for (int i = 0; i < qDOF; i++)
      for (int j = 0; j < qDOF; j++)
        SANS_CHECK_CLOSE( jacPDE_q(i,j), mtxPDEGlob_q(i,j), small_tol, close_tol );
  } //qorder loop
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGBR2_3D_2Tet_X1 )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_UniformGrad > PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef PDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,PhysD3::D,MatrixQ> RowMatrixQ;

  typedef IntegrandCell_DGBR2<PDEClass> IntegrandClass;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.7;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123; Real kxy = 0.553; Real kxz = 0.760;
  Real kyx = 0.343; Real kyy = 1.007; Real kyz = 0.870;
  Real kzx = 0.690; Real kzy = 0.365; Real kzz = 1.460;

  ViscousFlux3D_Uniform visc(kxx, kxy, kxz,
                                 kyx, kyy, kyz,
                                 kzx, kzy, kzz);

  Real a0 = 0.4, ax = 0.7, ay = -0.3, az = 0.2;
  Source3D_UniformGrad source(a0, ax, ay, az);

  PDEClass pde( adv, visc, source );

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand
  IntegrandClass fcn( pde, disc, {0} );

  // grid: X1
  XField3D_2Tet_X1_1Group xfld;

  std::vector<BasisFunctionCategory> basis_categories = {BasisFunctionCategory_Legendre,
                                                         BasisFunctionCategory_Hierarchical,
                                                         BasisFunctionCategory_Hierarchical};

  // solution
  for ( int qorder = 0; qorder <= 2; qorder++ )
  {
    Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, basis_categories[qorder]);

    const int qDOF = qfld.nDOF();

    // solution data
    for (int i = 0; i < qDOF; i++)
      qfld.DOF(i) = sin(PI*i/((Real)qDOF));

    // lifting operator
    FieldLift_DG_Cell<PhysD3, TopoD3, VectorArrayQ> rfld(xfld, qorder, basis_categories[qorder]);

    const int rDOF = rfld.nDOF();

    // lifting operator data
    for (int i = 0; i < rDOF; i++)
      rfld.DOF(i) = {cos(PI*i/((Real)rDOF)), sin(PI*i/((Real)rDOF)), -cos(PI*i/((Real)rDOF))};

    // quadrature rule
    int quadratureOrder = 4;

    // jacobian via FD w/ residual operator; assumes scalar PDE
    // topology-specific single group interface

    SLA::SparseVector<ArrayQ> rsdPDEGlobal0(qDOF), rsdPDEGlobal1(qDOF);
    DLA::MatrixD<Real> jacPDE_q(qDOF,qDOF);

    jacPDE_q = 0;

    rsdPDEGlobal0 = 0;
    IntegrateCellGroups<TopoD3>::integrate( ResidualCell_DGBR2(fcn, rsdPDEGlobal0),
                                            xfld, (qfld, rfld), &quadratureOrder, 1 );

    //wrt q
    for (int j = 0; j < qDOF; j++)
    {
      qfld.DOF(j) += 1;

      rsdPDEGlobal1 = 0;
      IntegrateCellGroups<TopoD3>::integrate( ResidualCell_DGBR2(fcn, rsdPDEGlobal1),
                                              xfld, (qfld, rfld), &quadratureOrder, 1 );

      qfld.DOF(j) -= 1;

      for (int i = 0; i < qDOF; i++)
        jacPDE_q(i,j) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    }

  #if 0
    cout << "FD: jacobian = " << endl << jacPDE_q << endl;
  #endif

    FieldDataMatrixD_CellLift<RowMatrixQ> jacPDEGlob_R(rfld);
    jacPDEGlob_R = 0; // Lifting operator jacobian from cell integral

    // jacobian via Surreal
    DLA::MatrixD<MatrixQ> mtxPDEGlob_q(qDOF,qDOF);

    mtxPDEGlob_q = 0;
    IntegrateCellGroups<TopoD3>::integrate( JacobianCell_DGBR2(fcn, mtxPDEGlob_q, jacPDEGlob_R),
                                            xfld, (qfld, rfld), &quadratureOrder, 1 );

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-10;

    for (int i = 0; i < qDOF; i++)
      for (int j = 0; j < qDOF; j++)
        SANS_CHECK_CLOSE( jacPDE_q(i,j), mtxPDEGlob_q(i,j), small_tol, close_tol );
  } //qorder loop
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
