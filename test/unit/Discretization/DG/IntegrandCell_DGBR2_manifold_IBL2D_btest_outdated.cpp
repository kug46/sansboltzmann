// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// testing of DGBR2_manifold cell integrands for IBL2D

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/Tuple.h"

#include "Discretization/DG/IntegrandCell_DGBR2_manifold.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementLift.h"
#include "Field/Tuple/ElementTuple.h"

#include "pde/IBL/PDEIBL2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

using namespace std;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEIBL<PhysD2,VarTypeDANCt> PDEClassIBL2D;
typedef PDENDConvertSpace<PhysD2,PDEClassIBL2D> NDPDEClassIBL2D;

typedef NDPDEClassIBL2D::template ArrayQ<Real> ArrayQ;
typedef PDEClassIBL2D::template ArrayParam<Real> ArrayParam;
typedef PDEClassIBL2D::template VectorX<Real> VectorX;

typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldClass;
typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;
typedef Element<ArrayParam,TopoD1,Line> ElementParamFieldClass;
typedef Element<VectorX,TopoD1,Line> ElementVFieldClass;

typedef MakeTuple<ElementTuple, ElementParamFieldClass,
                                ElementXFieldClass>::type ElementParamType;

template class IntegrandCell_DGBR2_manifold<NDPDEClassIBL2D>::BasisWeighted_PDE<Real,Real,TopoD1,Line,ElementParamType>;
template class IntegrandCell_DGBR2_manifold<NDPDEClassIBL2D>::BasisWeighted_LO<Real,TopoD1,Line,ElementXFieldClass>;
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_DGBR2_manifold_2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IntegrandCell_DGBR2_manifold_2D_Line_X1Q1R1_IncompressibleLaminarWallProfile_test )
{
  // type definition
  typedef PhysD2 PhysDim;
  typedef VarData2DDANCt VarDataType;
  typedef VarTypeDANCt VarType;
  typedef PDEIBL<PhysDim,VarType> PDEClass;
  typedef PDENDConvertSpace<PhysDim,PDEClass> NDPDEClass;
  /* default parameters
      const Real gam = 1.4;
      const Real R = 287.15;
      const Real mui = 1.827e-5;
      const Real Pr = 0.715;
   */
  NDPDEClass pde(VelocityProfile2D::LaminarBL);

  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::VectorS<PhysDim::D,VectorX> VectorVectorX;

  typedef ElementLift<VectorArrayQ,TopoD1,Line> ElementrFieldClass;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementRFieldClass;

  typedef IntegrandCell_DGBR2_manifold<NDPDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted_PDE<Real,Real,TopoD1,Line,ElementParamType> BasisWeightedPDEClass;
  typedef IntegrandClass::BasisWeighted_LO<Real,TopoD1,Line,ElementXFieldClass> BasisWeightedLOClass;

  // static tests: check dimensions
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 2 );

  // solution approximation order
  int order = 1;

  // ----------------------- ELEMENT PARAMETERS ----------------------- //
  // Geometry (line grid)
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  Real x_L = 1.2, x_R = x_L + 0.8;
  Real z_L = 2.2, z_R = z_L + 0.6;

  xfldElem.DOF(0) = { x_L, z_L };
  xfldElem.DOF(1) = { x_R, z_R };

  // Parameter: edge velocity field
  ElementQFieldClass vfldElem(order, BasisFunctionCategory_Hierarchical);

  const Real qxi_L = 3.,  qzi_L = 4.;
  const Real qxi_R = 5.,  qzi_R = 2.;

  ArrayQ q1_L = { qxi_L, qzi_L };
  ArrayQ q1_R = { qxi_R, qzi_R };

  vfldElem.DOF(0) = q1_L;
  vfldElem.DOF(1) = q1_R;

  // Parameter: edge velocity gradient fields
  VectorVectorX gradvL, gradvR;
  std::vector<VectorX> gradphiL(vfldElem.nDOF()), gradphiR(vfldElem.nDOF());

  xfldElem.evalBasisGradient( 0.0, xfldElem, gradphiL.data(), gradphiL.size() );
  xfldElem.evalBasisGradient( 1.0, xfldElem, gradphiR.data(), gradphiR.size() );

  vfldElem.evalFromBasis( gradphiL.data(), gradphiL.size(), gradvL );
  vfldElem.evalFromBasis( gradphiR.data(), gradphiR.size(), gradvR );

  // Parameter: stagnation state field
  const Real p0i_L = 1.30e+5,  T0i_L = 290.;
  const Real p0i_R = 1.40e+5,  T0i_R = 350.;

  // Parameter field
  ElementParamFieldClass paramElem(order, BasisFunctionCategory_Hierarchical);

  paramElem.DOF(0) = {qxi_L, qzi_L, gradvL[0][0], gradvL[1][0], gradvL[0][1], gradvL[1][1], p0i_L, T0i_L};
  paramElem.DOF(1) = {qxi_R, qzi_R, gradvR[0][0], gradvR[1][0], gradvR[0][1], gradvR[1][1], p0i_R, T0i_R};

  // Solution
  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // State vector solution at line cell left node
  Real delta_L = 2e-2;
  Real A_L     = 1.2;

  // State vector solution at line cell right node
  Real delta_R = 2.2e-2;
  Real A_R     = 1.3;

  // Line solution
  qfldElem.DOF(0) = pde.setDOFFrom( VarDataType(delta_L,A_L) );
  qfldElem.DOF(1) = pde.setDOFFrom( VarDataType(delta_R,A_R) );

  // lifting operators
  ElementrFieldClass rfldElems(order, BasisFunctionCategory_Hierarchical);

  rfldElems[0].DOF(0) = { 1, 2 };  // Trace 1  Basis Function 1  { x, z }
  rfldElems[0].DOF(1) = { 3, 4 };  // Trace 1  Basis Function 2  { x, z }

  rfldElems[1].DOF(0) = { 4, 3 };  // Trace 2  Basis Function 1  { x, z }
  rfldElems[1].DOF(1) = { 2, 1 };  // Trace 2  Basis Function 2  { x, z }

  // sum of lifting operators
  ElementRFieldClass RfldElem(order, BasisFunctionCategory_Hierarchical);

  RfldElem.vectorViewDOF() = rfldElems[0].vectorViewDOF() +
                             rfldElems[1].vectorViewDOF();

  // BR2 discretization (not used)
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // ----------------------------- TEST INTEGRAND ----------------------------- //
  const Real small_tol = 5e-13;
  const Real close_tol = 5e-13;

  IntegrandClass fcnint( pde, disc, {0} );

  //Explicitly create a paramater instance.
  //If it was created in the argument below, the temporary variable in the function argument would go out of scope and result in undefined behavior
  ElementParamType tupleElem = (paramElem, xfldElem);

  BasisWeightedPDEClass fcnPDE = fcnint.integrand_PDE( tupleElem, qfldElem, RfldElem );
  BasisWeightedLOClass  fcnLO  = fcnint.integrand_LO( xfldElem, rfldElems );

  BOOST_CHECK_EQUAL( 2, fcnPDE.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcnPDE.nDOF() );
  BOOST_CHECK( fcnPDE.needsEvaluation() == true );

  BOOST_CHECK_EQUAL( 2, fcnLO.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcnLO.nDOF() );
  BOOST_CHECK( fcnLO.needsEvaluation() == false );

  typedef typename ElementXFieldClass::BaseType::RefCoordType RefCoordType;

  RefCoordType sRef;

  Real eIntegrand;
  Real starIntegrand;

  const int nBasis = 2;  // number of basis functions
  const int nTrace = 2;

  ArrayQ integrandPDETrue[nBasis];
  VectorX integrandLiftTrue[nBasis][nTrace];
  BasisWeightedPDEClass::IntegrandType<Real> integrandPDE[nBasis];
  BasisWeightedLOClass::IntegrandType integrandLO[nBasis];

  // Test
  sRef = 0.;
  fcnPDE( sRef, integrandPDE, nBasis ); // IntegrandCell Functor operator()
  fcnLO( sRef, integrandLO, nBasis );

  // PDE residual integrands: (source) + (advective flux)
  // Basis function 1
  eIntegrand = ( 1.5410637956585847e-02 ) + ( 8.6834925249693717e-02 );
  starIntegrand = ( -6.1432717970541020e-02 ) + ( 6.9943484299232916e-01 );
  integrandPDETrue[0] = { eIntegrand, starIntegrand };

  // Basis function 2
  eIntegrand = ( 0.0000000000000000e+00 ) + ( -8.6834925249693717e-02 );
  starIntegrand = ( -0.0000000000000000e+00 ) + ( -6.9943484299232916e-01 );
  integrandPDETrue[1] = { eIntegrand, starIntegrand };

  for ( int ibasis = 0; ibasis < nBasis; ibasis++ )
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE( integrandPDETrue[ibasis][n], integrandPDE[ibasis][n], small_tol, close_tol );
    }
  }

  //Lifting operator residual integrands: (lifting)
  integrandLiftTrue[0][0] = { 1.0000000000000000e+00, 2.0000000000000000e+00 };  // Basis function 1  Lifting Operator 1  { x, z }
  integrandLiftTrue[0][1] = { 4.0000000000000000e+00, 3.0000000000000000e+00 };  // Basis function 1  Lifting Operator 2  { x, z }

  integrandLiftTrue[1][0] = { 0.0000000000000000e+00, 0.0000000000000000e+00 };  // Basis function 2  Lifting Operator 1  { x, z }
  integrandLiftTrue[1][1] = { 0.0000000000000000e+00, 0.0000000000000000e+00 };  // Basis function 2  Lifting Operator 2  { x, z }

  for ( int nbasis = 0; nbasis < nBasis; nbasis++ )
  {
    for ( int itrace = 0; itrace < nTrace; itrace++ )
    {
      for ( int idim = 0; idim < pde.D; idim++ )
      {
        for ( int ieqn = 0; ieqn < pde.N; ieqn++ )
        {
          SANS_CHECK_CLOSE( integrandLiftTrue[nbasis][itrace][idim], integrandLO[nbasis][itrace][idim][ieqn], small_tol, close_tol );
        }
      }
    }
  }

  // Test
  sRef = 0.6;
  fcnPDE( sRef, integrandPDE, nBasis ); // IntegrandCell Functor operator()
  fcnLO( sRef, integrandLO, nBasis );

  // PDE residual integrands: (source) + (advective flux)
  // Basis function 1
  eIntegrand = ( 6.2940253565260760e-03 ) + ( 9.4377985248203677e-02 );
  starIntegrand = ( -2.3745397884246271e-02 ) + ( 7.3872162780871742e-01 );
  integrandPDETrue[0] = { eIntegrand, starIntegrand };

  // Basis function 2
  eIntegrand = ( 9.4410380347891131e-03 ) + ( -9.4377985248203677e-02 );
  starIntegrand = ( -3.5618096826369405e-02 ) + ( -7.3872162780871742e-01 );
  integrandPDETrue[1] = { eIntegrand, starIntegrand };

  for ( int ibasis = 0; ibasis < nBasis; ibasis++ )
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE( integrandPDETrue[ibasis][n], integrandPDE[ibasis][n], small_tol, close_tol );
    }
  }

  //Lifting operator residual integrands: (lifting)
  integrandLiftTrue[0][0] = { 8.8000000000000012e-01, 1.2800000000000002e+00 };  // Basis function 1  Lifting Operator 1  { x, z }
  integrandLiftTrue[0][1] = { 1.1199999999999999e+00, 7.2000000000000020e-01 };  // Basis function 1  Lifting Operator 2  { x, z }

  integrandLiftTrue[1][0] = { 1.3200000000000001e+00, 1.9199999999999999e+00 };  // Basis function 2  Lifting Operator 1  { x, z }
  integrandLiftTrue[1][1] = { 1.6799999999999999e+00, 1.0800000000000001e+00 };  // Basis function 2  Lifting Operator 2  { x, z }

  for ( int nbasis = 0; nbasis < nBasis; nbasis++ )
  {
    for ( int itrace = 0; itrace < nTrace; itrace++ )
    {
      for ( int idim = 0; idim < pde.D; idim++ )
      {
        for ( int ieqn = 0; ieqn < pde.N; ieqn++ )
        {
          SANS_CHECK_CLOSE( integrandLiftTrue[nbasis][itrace][idim], integrandLO[nbasis][itrace][idim][ieqn], small_tol, close_tol );
        }
      }
    }
  }

  // Test
  sRef = 1;
  fcnPDE( sRef, integrandPDE, nBasis ); // IntegrandCell Functor operator()
  fcnLO( sRef, integrandLO, nBasis );

  // PDE residual integrands: (source) + (advective flux)
  // Basis function 1
  eIntegrand = ( 0.0000000000000000e+00 ) + ( 9.9905982556669951e-02 );
  starIntegrand = ( -0.0000000000000000e+00 ) + ( 8.6328331301762540e-01 );
  integrandPDETrue[0] = { eIntegrand, starIntegrand };

  // Basis function 2
  eIntegrand = ( 1.6019260953562833e-02 ) + ( -9.9905982556669951e-02 );
  starIntegrand = ( -6.5338342955398448e-02 ) + ( -8.6328331301762540e-01 );
  integrandPDETrue[1] = { eIntegrand, starIntegrand };

  for ( int ibasis = 0; ibasis < nBasis; ibasis++ )
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE( integrandPDETrue[ibasis][n], integrandPDE[ibasis][n], small_tol, close_tol );
    }
  }

  //Lifting operator residual integrands: (lifting)
  integrandLiftTrue[0][0] = { 0.0000000000000000e+00, 0.0000000000000000e+00 };  // Basis function 1  Lifting Operator 1  { x, z }
  integrandLiftTrue[0][1] = { 0.0000000000000000e+00, 0.0000000000000000e+00 };  // Basis function 1  Lifting Operator 2  { x, z }

  integrandLiftTrue[1][0] = { 3.0000000000000000e+00, 4.0000000000000000e+00 };  // Basis function 2  Lifting Operator 1  { x, z }
  integrandLiftTrue[1][1] = { 2.0000000000000000e+00, 1.0000000000000000e+00 };  // Basis function 2  Lifting Operator 2  { x, z }

  for ( int nbasis = 0; nbasis < nBasis; nbasis++ )
  {
    for ( int itrace = 0; itrace < nTrace; itrace++ )
    {
      for ( int idim = 0; idim < pde.D; idim++ )
      {
        for ( int ieqn = 0; ieqn < pde.N; ieqn++ )
        {
          SANS_CHECK_CLOSE( integrandLiftTrue[nbasis][itrace][idim], integrandLO[nbasis][itrace][idim][ieqn], small_tol, close_tol );
        }
      }
    }
  }

  // ----------------------------- TEST ELEMENTAL INTEGRAL (RESIDUAL) ----------------------------- //
  const Real small_tol_res = small_tol;
  const Real close_tol_res = close_tol;

  int nIntegrand = qfldElem.nDOF();

  // Test the element integral of the functor: quadrature order = 39
  int quadratureorder = 39;
  GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedPDEClass::IntegrandType<Real>> integralPDE(quadratureorder, nIntegrand);
  GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedLOClass::IntegrandType> integralLO(quadratureorder, nIntegrand);


  Real eRes;
  Real starRes;

  ArrayQ rsdPDETrue[nBasis];
  VectorX rsdLiftTrue[nBasis][nTrace];
  BasisWeightedPDEClass::IntegrandType<Real> rsdElemPDE[nBasis] = { 0, 0 };
  BasisWeightedLOClass::IntegrandType rsdElemLO[nBasis] = { 0, 0 };

  // cell integration for canonical element
  integralPDE( fcnPDE, xfldElem, rsdElemPDE, nIntegrand );
  integralLO( fcnLO, xfldElem, rsdElemLO, nIntegrand );

  // PDE residuals: (source) + (advective flux)
  // Basis function 1
  eRes = ( 7.7921476912574941e-03 ) + ( 9.3161624940993476e-02 );
  starRes = ( -2.9807898960344921e-02 ) + ( 7.4059965764333957e-01 );
  rsdPDETrue[0] = { eRes, starRes };

  // Basis function 2
  eRes = ( 7.8940289947055185e-03 ) + ( -9.3161624940993476e-02 );
  starRes = ( -3.0485550145167101e-02 ) + ( -7.4059965764333957e-01 );
  rsdPDETrue[1] = { eRes, starRes };

  for ( int ibasis = 0; ibasis < nBasis; ibasis++ )
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE( rsdPDETrue[ibasis][n], rsdElemPDE[ibasis][n], small_tol_res, close_tol_res );
    }
  }

  // Lifting Operator residual:
  rsdLiftTrue[0][0] = { 8.3333333333333337e-01, 1.3333333333333335e+00 };  // Basis function 1  Lifting Operator 1  { x, z }
  rsdLiftTrue[0][1] = { 1.6666666666666663e+00, 1.1666666666666665e+00 };  // Basis function 1  Lifting Operator 2  { x, z }

  rsdLiftTrue[1][0] = { 1.1666666666666665e+00, 1.6666666666666670e+00 };  // Basis function 2  Lifting Operator 1  { x, z }
  rsdLiftTrue[1][1] = { 1.3333333333333335e+00, 8.3333333333333337e-01 };  // Basis function 2  Lifting Operator 2  { x, z }

  for ( int itrace = 0; itrace < nTrace; itrace++ )
  {
    for ( int ibasis = 0; ibasis < nBasis; ibasis++ )
    {
      for ( int idim = 0; idim < pde.D; idim++ )
      {
        for ( int ieqn = 0; ieqn < pde.N; ieqn++ )
        {
          SANS_CHECK_CLOSE( rsdLiftTrue[ibasis][itrace][idim], rsdElemLO[ibasis][itrace][idim][ieqn], small_tol, close_tol );
        }
      }
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
