// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_Galerkin_Stabilized_AD_btest
// testing of cell element residual integrands for Galerkin: Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Stabilized.h"

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"

#include "Field/Element/GalerkinWeightedIntegral.h" // Basis Weighted
#include "Field/Element/ElementIntegral.h" // Field Weighted

#include "Field/Element/ElementLine.h"
#include "Field/Element/ElementArea.h"
#include "Field/Element/ElementVolume.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"

using namespace std;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_UniformGrad> PDEAdvectionDiffusion1D;
typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDE1DClassPlusTime;
template class IntegrandCell_Galerkin_Stabilized< PDE1DClassPlusTime >::BasisWeighted<Real, TopoD1, Line, ElementXField<PhysD1,TopoD1, Line> >;
template class IntegrandCell_Galerkin_Stabilized< PDE1DClassPlusTime >::FieldWeighted<Real, TopoD1, Line, ElementXField<PhysD1,TopoD1, Line> >;

typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_UniformGrad > PDEAdvectionDiffusion2D;
typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDE2DClassPlusTime;
template class IntegrandCell_Galerkin_Stabilized< PDE2DClassPlusTime >::
  BasisWeighted<Real, TopoD2, Triangle, ElementXField<PhysD2,TopoD2, Triangle> >;
template class IntegrandCell_Galerkin_Stabilized< PDE2DClassPlusTime >::
  FieldWeighted<Real, TopoD2, Triangle, ElementXField<PhysD2,TopoD2, Triangle> >;

//template class IntegrandCell_Galerkin< PDE2DClassPlusTime >::BasisWeighted<Real, TopoD2, Quad, ElementXField<PhysD2,TopoD2, Quad> >;
//
//typedef PDEAdvectionDiffusion<PhysD3,
//                              AdvectiveFlux3D_Uniform,
//                              ViscousFlux3D_Uniform,
//                              Source3D_None> PDEAdvectionDiffusion3D;
//typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDE3DClassPlusTime;
//template class IntegrandCell_Galerkin_Stabilized< PDE3DClassPlusTime >::BasisWeighted<Real, TopoD3, Tet, ElementXField<PhysD3,TopoD3, Tet> >;
//
//template class IntegrandCell_Galerkin_Stabilized< PDE3DClassPlusTime >::BasisWeighted<Real, TopoD3, Hex, ElementXField<PhysD3,TopoD3, Hex> >;
}

using namespace SANS;



//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_Galerkin_Stabilized_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_1D_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;
  typedef IntegrandCell_Galerkin_Stabilized<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line, ElementXField<PhysD1,TopoD1, Line> > BasisWeightedClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;


  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3, b = 1.4;
  Source1D_UniformGrad source(a, b);

  PDEClass pde( adv, visc, source );

  //TEST P1 for now
  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );
  // triangle solution
  qfldElem.DOF(0) = 2.;
  qfldElem.DOF(1) = 3.;

  StabilizationMatrix tau(StabilizationType::Adjoint, TauType::Glasby, order);

  // integrand
  IntegrandClass fcnint( pde, {0}, tau );

  BasisWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem );

  //BOOST_CHECK( fcn.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );


  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  RefCoordType sRef;
  Real integrandTrue[2];
  ArrayQ integrand[2];

  // Test at {0}
  sRef = {0};
  fcn( sRef, integrand, 2 );

  //PDE residual integrands:
  integrandTrue[0] = ( 11./5. ) + ( -2123./1000. ) + ( 6. ) + ( -3.896638463297507 );   // Basis function 1
  integrandTrue[1] = ( -11./5. ) + ( 2123./1000. ) + ( 0. ) + ( 2.0294991996341185 );   // Basis function 2

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );

  // Test at {1}
  sRef = {1};
  fcn( sRef, integrand, 2 );

  //PDE residual integrands:
  integrandTrue[0] = ( 33./10. ) + ( -2123./1000. ) + ( 0. ) + ( -2.6869426023324947 );   // Basis function 1
  integrandTrue[1] = ( -33./10. ) + ( 2123./1000. ) + ( 8.299999999999999 ) + ( 0.21495540818659975 );   // Basis function 2

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );

  // Test at {1/2}
  sRef = {1./2.};
  fcn( sRef, integrand, 2 );

  //PDE residual integrands:
  integrandTrue[0] = ( 11./4. ) + ( -2123./1000. ) + ( 3.575 ) + ( -3.443002515435628 );   // Basis function 1
  integrandTrue[1] = ( -11./4. ) + ( 2123./1000. ) + ( 3.575 ) + ( 1.2734392865309856 );   // Basis function 2

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );

  // Test at {2/7}
  sRef = {2./7.};
  fcn( sRef, integrand, 2 );

  //PDE residual integrands:
  integrandTrue[0] = ( 88./35. ) + ( -2123./1000. ) + ( 4.755102040816326 ) + ( -3.6744494276100554 );   // Basis function 1
  integrandTrue[1] = ( -88./35. ) + ( 2123./1000. ) + ( 1.9020408163265303 ) + ( 1.634496469523094 );   // Basis function 2

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );

  // test the element integral of the functor

  int quadratureorder = -1;
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integral(quadratureorder, nIntegrand);

  ArrayQ rsdPDEElem[2] = {0,0};
  Real rsd[2];

  // cell integration for canonical element
  integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

  //PDE residual:
  rsd[0] = ( 11./4. ) + ( -2123./1000. ) + ( 3.3833333333333337 )+ ( -3.392598521228756 );   // Basis function 1
  rsd[1] = ( -11./4. ) + ( 2123./1000. ) + ( 3.7666666666666666 )+ ( 1.2230352923241117 );   // Basis function 2

  SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_1D_Line_Jacobian_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion1D::template MatrixQ<Real> MatrixQ;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;
  typedef IntegrandCell_Galerkin_Stabilized<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line, ElementXField<PhysD1,TopoD1, Line> > BasisWeightedClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;


  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3, b = 1.4;
  Source1D_UniformGrad source(a, b);

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  for (int qorder = 1; qorder <= 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldClass qfldElem(qorder, BasisFunctionCategory_Hierarchical);

    const int nDOF = qfldElem.nDOF();

    // line solution
    for (int dof = 0; dof < nDOF;dof++)
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    StabilizationMatrix tau(StabilizationType::GLS, TauType::Glasby, qorder);

    // integrand
    IntegrandClass fcnint( pde, {0}, tau );

    BasisWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem );

    const Real small_tol = 1e-11;
    const Real close_tol = 5e-11;
    RefCoordType sRef = 0.5;
    DLA::VectorD<ArrayQ> integrand0(nDOF), integrand1(nDOF);
    DLA::MatrixD<MatrixQ> mtxPDEElemTrue(nDOF, nDOF);
    DLA::MatrixD<MatrixQ> mtxPDEElem(nDOF, nDOF);

    // compute jacobians via finite differenec (exact for linear PDE)
    fcn(sRef, &integrand0[0], integrand0.m());

    for (int i = 0; i < nDOF; i++)
    {
      qfldElem.DOF(i) += 1;
      fcn(sRef, integrand1.data(), integrand1.size());
      qfldElem.DOF(i) -= 1;

      for (int j = 0; j < nDOF; j++)
        mtxPDEElemTrue(j,i) = integrand1[j] - integrand0[j];
    }

    mtxPDEElem = 0;

    // accumulate the jacobian via Surreal
    fcn(1., sRef, mtxPDEElem);

    for (int i = 0; i < nDOF; i++)
      for (int j = 0; j < nDOF; j++)
        SANS_CHECK_CLOSE( mtxPDEElemTrue(i,j), mtxPDEElem(i,j), small_tol, close_tol );
  }
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_WeightVary_1D_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_Galerkin_Stabilized<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,ElementParam > BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD1,Line,ElementParam > FieldWeightedClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3, b = 1.4;
  Source1D_UniformGrad source(a, b);

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  Element<Real,TopoD1,Line> efldElem(0,BasisFunctionCategory_Legendre);

  const int nstab = (int)StabilizationType::nStabilizationType;
  StabilizationType stabs[nstab];

  for (int i = 0; i < nstab; i++ )
  {
    stabs[i] = (StabilizationType)i;

    if (i>2) stabs[i] = (StabilizationType)5;
  }

  const int ntau = (int)TauType::nTauType;
  TauType taus[ntau];
  for (int i = 0; i < ntau; i++ )
    taus[i] = (TauType)i;

  for (int qorder = 1; qorder <= 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldClass qfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, qfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElem.nDOF() );

    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    ElementQFieldClass wfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, wfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, wfldElem.nDOF() );

    // line weighting
    for (int dof = 0; dof < wfldElem.nDOF(); dof ++ )
      wfldElem.DOF(dof) = 0;

    BOOST_REQUIRE_EQUAL(wfldElem.nDOF(), qfldElem.nDOF());

    for (int istab = 0; istab < nstab; istab++)
      for (int itau = 0; itau < ntau; itau++)
      {

        // skip invalid combinations
        if (!StabilizationMatrix::checkStabTau(stabs[istab], taus[itau]) )
        {
          continue;
        }

        StabilizationMatrix tau(stabs[istab], taus[itau], qorder );
        StabilizationMatrix tau2( stabs[istab], taus[itau], qorder );

        IntegrandClass fcnint2( pde, {0}, tau2 ); // make sure basis weighted is using VMS stabilization
        IntegrandClass fcnint( pde, {0}, tau );

        BasisWeightedClass fcnB = fcnint2.integrand( xfldElem, qfldElem );
        FieldWeightedClass fcnW = fcnint.integrand( xfldElem, qfldElem, wfldElem, efldElem );

        const Real small_tol = 1e-11;
        const Real close_tol = 1e-11;
        Real rsdPDEElemW[1]={0};
        std::vector<ArrayQ> rsdPDEElemB(qfldElem.nDOF(), 0);

        int quadratureorder = 2*qorder;
        int nIntegrand = qfldElem.nDOF();
        GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integralB(quadratureorder, nIntegrand);

        // cell integration for canonical element
        integralB( fcnB, xfldElem, rsdPDEElemB.data(), nIntegrand );

        GalerkinWeightedIntegral<TopoD1, Line, Real> integralW(quadratureorder, efldElem.nDOF() );

        for (int i = 0; i < wfldElem.nDOF(); i++)
        {
          // set just one of the weights to one
          wfldElem.DOF(i) = 1;

          // cell integration for canonical element
          rsdPDEElemW[0] = 0;
          integralW( fcnW, xfldElem, rsdPDEElemW, 1 );

          // test the the two integrands are identical
          SANS_CHECK_CLOSE( rsdPDEElemW[0], rsdPDEElemB[i], small_tol, close_tol );

          // reset to zero
          wfldElem.DOF(i) = 0;
        }
      }
  }

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_EstimateVary_1D_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_Galerkin_Stabilized<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,ElementParam > BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD1,Line,ElementParam > FieldWeightedClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3, b = 1.4;
  Source1D_UniformGrad source(a, b);

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  ElementQFieldClass wfldElem(0,BasisFunctionCategory_Legendre);
  wfldElem.DOF(0) = 1;

  for (int qorder = 1; qorder <= 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldClass qfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, qfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElem.nDOF() );

    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    Element<Real,TopoD1,Line> efldElem(qorder, BasisFunctionCategory_Hierarchical);

    StabilizationMatrix tau(StabilizationType::Unstabilized, TauType::Glasby, qorder);
    IntegrandClass fcnint( pde, {0}, tau );

    BasisWeightedClass fcnB = fcnint.integrand( xfldElem, qfldElem );
    FieldWeightedClass fcnW = fcnint.integrand( xfldElem, qfldElem, wfldElem, efldElem );

    const Real small_tol = 1e-11;
    const Real close_tol = 1e-11;
    std::vector<Real> rsdPDEElemW(efldElem.nDOF(), 0);
    std::vector<ArrayQ> rsdPDEElemB(qfldElem.nDOF(), 0);

    int quadratureorder = 2*qorder;
    int nIntegrand = qfldElem.nDOF();
    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integralB(quadratureorder, nIntegrand);

    // cell integration for canonical element
    integralB( fcnB, xfldElem, rsdPDEElemB.data(), nIntegrand );

    GalerkinWeightedIntegral<TopoD1, Line, Real> integralW(quadratureorder, efldElem.nDOF() );

    integralW( fcnW, xfldElem, rsdPDEElemW.data(), rsdPDEElemW.size() );

    for (int i = 0; i < efldElem.nDOF(); i++)
      SANS_CHECK_CLOSE( rsdPDEElemW[i], rsdPDEElemB[i], small_tol, close_tol );

  }

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_2D_Triangle_test )
{
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;
  typedef IntegrandCell_Galerkin_Stabilized<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD2,Triangle, ElementXField<PhysD2,TopoD2, Triangle> > BasisWeightedClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.478;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real a = 0.35, b = 1.82, c = -2.49;
  Source2D_UniformGrad source(a, b, c);

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  StabilizationMatrix tau(StabilizationType::Adjoint, TauType::Glasby, order);

  // integrand
  IntegrandClass fcnint( pde, {0}, tau );

  BasisWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem );

  //BOOST_CHECK( fcn.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );


  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  RefCoordType sRef;
  Real integrandTrue[3];
  ArrayQ integrand[3];

  // Test at {0, 0}
  sRef = {0, 0};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source) + (stab)
  integrandTrue[0] = ( 13./10. ) + ( -4941./500. ) + ( -87./25. ) + ( 68./1045. );   // Basis function 1
  integrandTrue[1] = ( -11./10. ) + ( 1181./200. ) + ( 0 ) + ( -9928./51205. );   // Basis function 2
  integrandTrue[2] = ( -1./5. ) + ( 3977./1000. ) + ( 0 ) + ( 7786./51205. );   // Basis function 3


  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );

  // Test at {1, 0}
  sRef = {1, 0};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source) + (stab)
  integrandTrue[0] = ( 39./10. ) + ( -4941./500. ) + ( 0 ) + ( -9./7315. );   // Basis function 1
  integrandTrue[1] = ( -33./10. ) + ( 1181./200. ) + ( -139./50. ) + ( 257./51205. );   // Basis function 2
  integrandTrue[2] = ( -3./5. ) + ( 3977./1000. ) + ( 0 ) + ( -229./51205. );   // Basis function 3

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );

  // Test at {0, 1}
  sRef = {0, 1};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source) + (stab)
  integrandTrue[0] = ( 26./5. ) + ( -4941./500. ) + ( 0 ) + ( -333./14630. );   // Basis function 1
  integrandTrue[1] = ( -22./5. ) + ( 1181./200. ) + ( 0 ) + ( 5402./51205. );   // Basis function 2
  integrandTrue[2] = ( -4./5. ) + ( 3977./1000. ) + ( -243./100. ) + ( -444./4655. );   // Basis function 3

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );

  // Test at {1/3, 1/3}
  sRef = {1./3., 1./3.};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source) + (stab)
  integrandTrue[0] = ( 52./15. ) + ( -4941./500. ) + ( -869./900. ) + ( 464./65835. );   // Basis function 1
  integrandTrue[1] = ( -44./15. ) + ( 1181./200. ) + ( -869./900. ) + ( -24389./921690. );   // Basis function 2
  integrandTrue[2] = ( -8./15. ) + ( 3977./1000. ) + ( -869./900. ) + ( 551./24255. );   // Basis function 3

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );

  // test the element integral of the functor

  int quadratureorder = -1;
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integral(quadratureorder, nIntegrand);

  ArrayQ rsdPDEElem[3] = {0,0,0};
  Real rsd[3];
  // cell integration for canonical element
  integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

  //PDE residual integrands: (advective) + (viscous) + (source) + (stab)
  rsd[0] = (26./15.) + (-4941./1000.) + (-1217./2400.)+ (139./31920.);   // Basis function 1
  rsd[1] = (-22./15.) + (1181./400.) + (-1147./2400.)+ (-1733./129360.);   // Basis function 2
  rsd[2] = (-4./15.) + (3977./2000.) + (-139./300.)+ (6571./614460.);   // Basis function 3

  SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[2], rsdPDEElem[2], small_tol, close_tol );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_WeightVary_2D_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform> PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_Galerkin_Stabilized<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD2,Triangle,ElementParam > BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD2,Triangle,ElementParam > FieldWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.478;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real a = 2.3;
  Source2D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = -0.3;  y1 = -0.1;
  x2 =  1.2;  y2 =  0.0;
  x3 =  0.1;  y3 =  1.0;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  const int nstab = (int)StabilizationType::nStabilizationType;
  StabilizationType stabs[nstab];
  for (int i = 0; i < nstab; i++ )
  {
    stabs[i] = (StabilizationType)i;

    if (i>2) stabs[i] = (StabilizationType)5;

  }

  const int ntau = (int)TauType::nTauType;
  TauType taus[ntau];
  for (int i = 0; i < ntau; i++ )
    taus[i] = (TauType)i;

  Element<Real,TopoD2,Triangle> efldElem( 0, BasisFunctionCategory_Legendre );

  for (int qorder = 1; qorder <= 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldClass qfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, qfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElem.nDOF() );

    // solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    ElementQFieldClass wfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, wfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, wfldElem.nDOF() );

    // weighting
    wfldElem.vectorViewDOF() = 0;

    BOOST_REQUIRE_EQUAL(wfldElem.nDOF(), qfldElem.nDOF());

    for (int istab = 0; istab < nstab; istab++)
      for (int itau = 0; itau < ntau; itau++)
      {
        // skip invalid combinations
        // skip invalid combinations
        if (!StabilizationMatrix::checkStabTau(stabs[istab], taus[itau]) )
        {
          continue;
        }

        StabilizationMatrix tau(stabs[istab], taus[itau], qorder );
        StabilizationMatrix tau2(stabs[istab], taus[itau], qorder );

        IntegrandClass fcnint( pde, {0}, tau );
        IntegrandClass fcnint2( pde, {0}, tau2 );

        BasisWeightedClass fcnB = fcnint2.integrand( xfldElem, qfldElem );
        FieldWeightedClass fcnW = fcnint.integrand( xfldElem, qfldElem, wfldElem, efldElem );

        const Real small_tol = 1e-11;
        const Real close_tol = 1e-11;
        Real rsdPDEElemW[1]={0};
        std::vector<ArrayQ> rsdPDEElemB(qfldElem.nDOF(), 0);

        int quadratureorder = 2*qorder;
        int nIntegrand = qfldElem.nDOF();
        GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralB(quadratureorder, nIntegrand);

        // cell integration for canonical element
        integralB( fcnB, xfldElem, rsdPDEElemB.data(), nIntegrand );

        GalerkinWeightedIntegral<TopoD2, Triangle, Real> integralW(quadratureorder, efldElem.nDOF());

        for (int i = 0; i < wfldElem.nDOF(); i++)
        {
          // set just one of the weights to one
          wfldElem.DOF(i) = 1;

          // cell integration for canonical element
          rsdPDEElemW[0] = 0;
          integralW( fcnW, xfldElem, rsdPDEElemW, 1 );

          // test the the two integrands are identical
          SANS_CHECK_CLOSE( rsdPDEElemW[0], rsdPDEElemB[i], small_tol, close_tol );

          // reset to zero
          wfldElem.DOF(i) = 0;
        }
      }
  }

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_EstimateVary_2D_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform> PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_Galerkin_Stabilized<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD2,Triangle,ElementParam > BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD2,Triangle,ElementParam > FieldWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.478;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real a = 2.3;
  Source2D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = -0.3;  y1 = -0.1;
  x2 =  1.2;  y2 =  0.0;
  x3 =  0.1;  y3 =  1.0;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  Element<ArrayQ,TopoD2,Triangle> wfldElem( 0, BasisFunctionCategory_Legendre );
  wfldElem.DOF(0) = 1;

  for (int qorder = 1; qorder <= 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldClass qfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, qfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElem.nDOF() );

    // solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    ElementQFieldClass efldElem(qorder, BasisFunctionCategory_Hierarchical);

    StabilizationMatrix tau(StabilizationType::Unstabilized, TauType::Constant, qorder );

    IntegrandClass fcnint( pde, {0}, tau );

    BasisWeightedClass fcnB = fcnint.integrand( xfldElem, qfldElem );
    FieldWeightedClass fcnW = fcnint.integrand( xfldElem, qfldElem, wfldElem, efldElem );

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;
    std::vector<Real> rsdPDEElemW(efldElem.nDOF(), 0 );
    std::vector<ArrayQ> rsdPDEElemB(qfldElem.nDOF(), 0);

    int quadratureorder = 2*(qorder+1);
    int nIntegrand = qfldElem.nDOF();
    GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralB(quadratureorder, nIntegrand);

    // cell integration for canonical element
    integralB( fcnB, xfldElem, rsdPDEElemB.data(), nIntegrand );

    GalerkinWeightedIntegral<TopoD2, Triangle, Real> integralW(quadratureorder, efldElem.nDOF());

    integralW( fcnW, xfldElem, rsdPDEElemW.data(), rsdPDEElemW.size() );

    for (int i = 0; i < wfldElem.nDOF(); i++)
      SANS_CHECK_CLOSE( rsdPDEElemW[i], rsdPDEElemB[i], small_tol, close_tol );

  }

}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_FieldWeighted_2D_Triangle_test )
{
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;
  typedef IntegrandCell_Galerkin_Stabilized<PDEClass> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD2,Triangle, ElementXField<PhysD2,TopoD2, Triangle> > BasisWeightedClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  Real u = 11./10.;
  Real v = 2./10.;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2123./1000.;
  Real kxy = 553./1000.;
  Real kyx = 478./1000.;
  Real kyy = 1007./1000.;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real a = 35./100., b = 182./100., c = 249./100.;
  Source2D_UniformGrad source(a, b, c);

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  // weighting
  ElementQFieldClass wfldElem(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElem.order() );
  BOOST_CHECK_EQUAL( 6, wfldElem.nDOF() );

  // triangle solution
  wfldElem.DOF(0) = -2;
  wfldElem.DOF(1) =  4;
  wfldElem.DOF(2) =  3;
  wfldElem.DOF(3) =  2;
  wfldElem.DOF(4) =  4;
  wfldElem.DOF(5) = -1;

  // estimate
  ElementQFieldClass efldElem(1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, efldElem.order() );
  BOOST_CHECK_EQUAL( 3, efldElem.nDOF() );

  efldElem.DOF(0) = 0;
  efldElem.DOF(1) = 0;
  efldElem.DOF(2) = 0;

  StabilizationMatrix tau(StabilizationType::Adjoint, TauType::Constant, 1, 10.0 );

  // integrand
  IntegrandClass fcnint( pde, {0}, tau );

  BasisWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem, wfldElem, efldElem );

  //BOOST_CHECK( fcn.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );


  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  RefCoordType sRef;
  Real integrandTrue[3];
  ArrayQ integrand[3];

  // Test at {0, 0}
  sRef = {0, 0};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source) + (stabilization)
  integrandTrue[0] = ( -9 ) + ( 115091/1000. ) + ( -573/25. ) + ( 15566929/2500. );   // Basis function 1
//  integrandTrue[0] = ( -9 ) + ( 115091/1000. ) + ( -573/25. ) + ( 0. );   // Basis function 1
  integrandTrue[1] = ( 11/5. ) + ( -1181/100. ) + ( 0 ) + ( 0 );   // Basis function 2
  integrandTrue[2] = ( 2/5. ) + ( -3977/500. ) + ( 0 ) + ( 0 );   // Basis function 3

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );

  // Test at {1, 0}
  sRef = {1, 0};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source) + (stabilization)
  integrandTrue[0] = ( 78/5. ) + ( -4941/125. ) + ( 0 ) + ( 0 );   // Basis function 1
  integrandTrue[1] = ( -282/5. ) + ( 150279/1000. ) + ( 1216/25. ) + ( 5064521/625. );   // Basis function 2
//  integrandTrue[1] = ( -282/5. ) + ( 150279/1000. ) + ( 1216/25. ) + ( 0 );   // Basis function 2
  integrandTrue[2] = ( -12/5. ) + ( 3977/250. ) + ( 0 ) + ( 0 );   // Basis function 3

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );

  // Test at {0, 1}
  sRef = {0, 1};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source) + (stabilization)
  integrandTrue[0] = ( 78/5. ) + ( -14823/500. ) + ( 0 ) + ( 0 );   // Basis function 1
  integrandTrue[1] = ( -66/5. ) + ( 3543/200. ) + ( 0 ) + ( 0 );   // Basis function 2
//  integrandTrue[2] = ( 76/5. ) + ( -21813/500. ) + ( 3753/100. ) + ( 0 );   // Basis function 3
  integrandTrue[2] = ( 76/5. ) + ( -21813/500. ) + ( 3753/100. ) + ( -21374291/2500. );   // Basis function 3

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );

  // Test at {1/3, 1/3}
  sRef = {1/3., 1/3.};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source) + (stabilization)
  integrandTrue[0] = ( 388/45. ) + ( -179441/9000. ) + ( 25291/1620. ) + ( 132436673/202500. );   // Basis function 1
  integrandTrue[1] = ( -244/15. ) + ( 5182/125. ) + ( 25291/1620. ) + ( 132436673/202500. );   // Basis function 2
  integrandTrue[2] = ( -104/15. ) + ( 38203/1125. ) + ( 25291/1620. ) + ( 132436673/202500. );   // Basis function 3
//  integrandTrue[0] = ( 388/45. ) + ( -179441/9000. ) + ( 25291/1620. ) + ( 0 );   // Basis function 1
//  integrandTrue[1] = ( -244/15. ) + ( 5182/125. ) + ( 25291/1620. ) + ( 0 );   // Basis function 2
//  integrandTrue[2] = ( -104/15. ) + ( 38203/1125. ) + ( 25291/1620. ) + ( 0 );   // Basis function 3

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );

  // Test at {1/5, 2/3}
  sRef = {1/5., 2/3.};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source) + (stabilization)
  integrandTrue[0] = ( 120173/5625. ) + ( -1089937/22500. ) + ( 45346/5625. ) + ( -2909317/6750. );   // Basis function 1
  integrandTrue[1] = ( -106471/5625. ) + ( 6576113/225000. ) + ( 22673/1875. ) + ( -2909317/4500. );   // Basis function 2
  integrandTrue[2] = ( -29002/5625. ) + ( 1137293/56250. ) + ( 45346/1125. ) + ( -2909317/1350. );   // Basis function 3
//  integrandTrue[0] = ( 120173/5625. ) + ( -1089937/22500. ) + ( 45346/5625. ) + ( 0 );   // Basis function 1
//  integrandTrue[1] = ( -106471/5625. ) + ( 6576113/225000. ) + ( 22673/1875. ) + ( 0 );   // Basis function 2
//  integrandTrue[2] = ( -29002/5625. ) + ( 1137293/56250. ) + ( 45346/1125. ) + ( 0 );   // Basis function 3

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );

  // Test at {3/4, 1/7}
  sRef = {3/4., 1/7.};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source) + (stabilization)
  integrandTrue[0] = ( 323859/27440. ) + ( -177966/6125. ) + ( 5701023/1097600. ) + ( 4648330569/7840000. );   // Basis function 1
  integrandTrue[1] = ( -1007493/27440. ) + ( 18922473/196000. ) + ( 5701023/156800. ) + ( 4648330569/1120000. );   // Basis function 2
  integrandTrue[2] = ( -1927/280. ) + ( 5837583/196000. ) + ( 1900341/274400. ) + ( 1549443523/1960000. );   // Basis function 3
//  integrandTrue[0] = ( 323859/27440. ) + ( -177966/6125. ) + ( 5701023/1097600. ) + ( 0 );   // Basis function 1
//  integrandTrue[1] = ( -1007493/27440. ) + ( 18922473/196000. ) + ( 5701023/156800. ) + ( 0 );   // Basis function 2
//  integrandTrue[2] = ( -1927/280. ) + ( 5837583/196000. ) + ( 1900341/274400. ) + ( 0 );   // Basis function 3

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );

  // test the element integral of the functor

  int quadratureorder = 5;
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integral(quadratureorder, nIntegrand);

  ArrayQ rsdPDEElem[3] = {0,0,0};
  Real rsd[3];
  // cell integration for canonical element
  integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

  //PDE residual: (advection) + (diffusion) + (source) + (stabilization)
  rsd[0] = (2617/600.) + (-11127/2000.) + (84923/18000.) + (182126281/360000.);   // Basis function 1
  rsd[1] = (-1063/120.) + (66161/3000.) + (125681/18000.) + (209491591/360000.);   // Basis function 2
  rsd[2] = (-691/300.) + (1406/125.) + (153671/18000.) + (-20044597/180000.);   // Basis function 3
//  rsd[0] = (2617/600.) + (-11127/2000.) + (84923/18000.) + (0);   // Basis function 1
//  rsd[1] = (-1063/120.) + (66161/3000.) + (125681/18000.) + (0);   // Basis function 2
//  rsd[2] = (-691/300.) + (1406/125.) + (153671/18000.) + (0);   // Basis function 3

  SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, 10*close_tol );
  SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, 10*close_tol );
  SANS_CHECK_CLOSE( rsd[2], rsdPDEElem[2], small_tol, 10*close_tol );

}
#endif




#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_2D_Quad_test )
{
  typedef PDENDConvertSteady<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Quad> ElementQFieldClass;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD2,Quad, ElementXField<PhysD2,TopoD2, Quad> > BasisWeightedClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 4, xfldElem.nDOF() );

  // Quad grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 1;  y3 = 1;
  x4 = 0;  y4 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};
  xfldElem.DOF(3) = {x4, y4};

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 4, qfldElem.nDOF() );

  // Quad solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 6;
  qfldElem.DOF(3) = 4;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem );

  //BOOST_CHECK( fcn.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 4, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );


  const Real small_tol = 5e-13;
  const Real close_tol = 5e-13;
  RefCoordType sRef;
  Real integrandTrue[4];
  ArrayQ integrand[4];

  // Test at {0, 0}
  sRef = {0, 0};
  fcn( sRef, integrand, 4 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue[0] = ( 13./10. ) + ( -1254./125. );   // Basis function 1
  integrandTrue[1] = ( -11./10. ) + ( 1181./200. );   // Basis function 2
  integrandTrue[2] = ( 0 ) + ( 0 );   // Basis function 3
  integrandTrue[3] = ( -1./5. ) + ( 4127./1000. );   // Basis function 4

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  // Test at {1, 0}
  sRef = {1, 0};
  fcn( sRef, integrand, 4 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue[0] = ( 33./10. ) + ( -1181./200. );   // Basis function 1
  integrandTrue[1] = ( -27./10. ) + ( 889./500. );   // Basis function 2
  integrandTrue[2] = ( -3./5. ) + ( 4127./1000. );   // Basis function 3
  integrandTrue[3] = ( 0 ) + ( 0 );   // Basis function 4

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  // Test at {1, 1}
  sRef = {1, 1};
  fcn( sRef, integrand, 4 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue[0] = ( 0 ) + ( 0 );   // Basis function 1
  integrandTrue[1] = ( 6./5. ) + ( -4127./1000. );   // Basis function 2
  integrandTrue[2] = ( -39./5. ) + ( 1254./125. );   // Basis function 3
  integrandTrue[3] = ( 33./5. ) + ( -1181./200. );   // Basis function 4

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  // Test at {0, 1}
  sRef = {0, 1};
  fcn( sRef, integrand, 4 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue[0] = ( 4./5. ) + ( -4127./1000. );   // Basis function 1
  integrandTrue[1] = ( 0 ) + ( 0 );   // Basis function 2
  integrandTrue[2] = ( -22./5. ) + ( 1181./200. );   // Basis function 3
  integrandTrue[3] = ( 18./5. ) + ( -889./500. );   // Basis function 4

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  // Test at {1/2, 1/2}
  sRef = {1./2., 1./2.};
  fcn( sRef, integrand, 4 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue[0] = ( 91./40. ) + ( -627./125. );   // Basis function 1
  integrandTrue[1] = ( -63./40. ) + ( 889./1000. );   // Basis function 2
  integrandTrue[2] = ( -91./40. ) + ( 627./125. );   // Basis function 3
  integrandTrue[3] = ( 63./40. ) + ( -889./1000. );   // Basis function 4

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  // test the element integral of the functor

  int quadratureorder = 2;
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Quad, ArrayQ> integral(quadratureorder, nIntegrand);

  ArrayQ rsdPDEElem[4] = {0,0,0,0};

  // cell integration for canonical element
  integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

  Real rsd[4];

  //PDE residual: (advection) + (diffusion)
  rsd[0] = (59./30.) + (-627./125.);   // Basis function 1
  rsd[1] = (-19./15.) + (889./1000.);   // Basis function 2
  rsd[2] = (-31./12.) + (627./125.);   // Basis function 3
  rsd[3] = (113./60.) + (-889./1000.);   // Basis function 4

  SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[2], rsdPDEElem[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[3], rsdPDEElem[3], small_tol, close_tol );


}
#endif

//----------------------------------------------------------------------------//
#if 0 // Needs more basis functions on Quad
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_2D_Quad_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform> PDEAdvectionDiffusion2D;
  typedef PDENDConvertSteady<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Quad> ElementQFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD2,Quad,ElementParam > BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD2,Quad,ElementParam > FieldWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Real a = 2.3;
  Source2D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  for (int qorder = 2; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldClass qfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, qfldElem.order() );
//    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElem.nDOF() );

    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    ElementQFieldClass wfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, wfldElem.order() );
//    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, wfldElem.nDOF() );

    // line weighting
    for (int dof = 0; dof < wfldElem.nDOF(); dof ++ )
      wfldElem.DOF(dof) = 0;

    BOOST_REQUIRE_EQUAL(wfldElem.nDOF(), qfldElem.nDOF());

    IntegrandClass fcnint( pde, {0} );

    BasisWeightedClass fcnB = fcnint.integrand( xfldElem, qfldElem );
    FieldWeightedClass fcnW = fcnint.integrand( xfldElem, qfldElem, wfldElem );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;
    ArrayQ rsdPDEElemW=0;
    std::vector<ArrayQ> rsdPDEElemB(qfldElem.nDOF(), 0);

    int quadratureorder = 1;
    int nIntegrand = qfldElem.nDOF();
    GalerkinWeightedIntegral<TopoD2, Quad, ArrayQ> integralB(quadratureorder, nIntegrand);

    // cell integration for canonical element
    integralB( fcnB, xfldElem, rsdPDEElemB.data(), nIntegrand );

    ElementIntegral<TopoD2, Quad, ArrayQ> integralW(quadratureorder);

    for (int i = 0; i < wfldElem.nDOF(); i++)
    {
      // set just one of the weights to one
      wfldElem.DOF(i) = 1;

      // cell integration for canonical element
      rsdPDEElemW = 0;
      integralW( fcnW, xfldElem, rsdPDEElemW );

      // test the the two integrands are identical
      SANS_CHECK_CLOSE( rsdPDEElemW, rsdPDEElemB[i], small_tol, close_tol );

      // reset to zero
      wfldElem.DOF(i) = 0;
    }
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_3D_Tet_test )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEAdvectionDiffusion3D;
  typedef PDENDConvertSteady<PhysD3,  PDEAdvectionDiffusion3D > PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD3,Tet> ElementQFieldClass;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD3,Tet, ElementXField<PhysD3,TopoD3, Tet> > BasisWeightedClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Real kxz = 0.643;
  Real kyz = 0.765;
  Real kzz = 1.234;
  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                              kxy, kyy, kyz,
                              kxz, kyz, kzz );

  Source3D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 4, xfldElem.nDOF() );

  // tetrahdral grid element
  Real x1, x2, x3, x4, y1, y2, y3, y4, z1, z2, z3, z4;

  x1 = 0;  y1 = 0;  z1 = 0;
  x2 = 1;  y2 = 0;  z2 = 0;
  x3 = 0;  y3 = 1;  z3 = 0;
  x4 = 0;  y4 = 0;  z4 = 1;

  xfldElem.DOF(0) = {x1, y1, z1};
  xfldElem.DOF(1) = {x2, y2, z2};
  xfldElem.DOF(2) = {x3, y3, z3};
  xfldElem.DOF(3) = {x4, y4, z4};

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 4, qfldElem.nDOF() );

  // tet solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;
  qfldElem.DOF(3) = 6;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem );

  //BOOST_CHECK( fcn.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 4, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );


  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordType sRef;
  Real integrandTrue[4];
  ArrayQ integrand[4];

  sRef = {0, 0, 0};
  fcn( sRef, integrand, 4 );

  integrandTrue[0] = ( 9./5.)   + (-26823./1000.);    // (advective) + (viscous)
  integrandTrue[1] = (-11./10.) + ( 228./25.);
  integrandTrue[2] = (-1./5.)   + ( 994./125.);
  integrandTrue[3] = (-1./2.)   + ( 9751./1000.);

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );


  sRef = {1, 0, 0};
  fcn( sRef, integrand, 4 );

  integrandTrue[0] = ( 27./5.)  + (-26823./1000.);    // (advective) + (viscous)
  integrandTrue[1] = (-33./10.) + ( 228./25.);
  integrandTrue[2] = (-3./5.)   + ( 994./125.);
  integrandTrue[3] = (-3./2.)   + ( 9751./1000.);

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );


  sRef = {0, 1, 0};
  fcn( sRef, integrand, 4 );

  integrandTrue[0] = ( 36./5.) + (-26823./1000.);    // (advective) + (viscous)
  integrandTrue[1] = (-22./5.) + ( 228./25.);
  integrandTrue[2] = (-4./5.)  + ( 994./125.);
  integrandTrue[3] = (-2.)     + ( 9751./1000.);

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );


  sRef = {0, 0, 1};
  fcn( sRef, integrand, 4 );

  integrandTrue[0] = ( 54./5.) + (-26823./1000.);    // (advective) + (viscous)
  integrandTrue[1] = (-33./5.) + ( 228./25.);
  integrandTrue[2] = (-6./5.)  + ( 994./125.);
  integrandTrue[3] = (-3.)     + ( 9751./1000.);

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );


  sRef = {1./4., 1./4., 1./4.};
  fcn( sRef, integrand, 4 );

  integrandTrue[0] = ( 63./10.) + (-26823./1000.);    // (advective) + (viscous)
  integrandTrue[1] = (-77./20.) + ( 228./25.);
  integrandTrue[2] = (-7./10.)  + ( 994./125.);
  integrandTrue[3] = (-7./4.)   + ( 9751./1000.);

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );


  // test the element integral of the functor

  int quadratureorder = 1;
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD3, Tet, ArrayQ> integral(quadratureorder, nIntegrand);

  ArrayQ rsdPDEElem[4] = {0,0,0,0};

  // cell integration for canonical element
  integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

  Real rsd1 = ( 21./20.)  + (-8941./2000.);   // (advective) + (viscous)
  Real rsd2 = (-77./120.) + (38./25.);
  Real rsd3 = (-7./60.)   + (497./375.);
  Real rsd4 = (-7./24.)   + (9751./6000.);

  SANS_CHECK_CLOSE( rsd1, rsdPDEElem[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2, rsdPDEElem[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd3, rsdPDEElem[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd4, rsdPDEElem[3], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_3D_Hex_test )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEAdvectionDiffusion3D;
  typedef PDENDConvertSteady<PhysD3,  PDEAdvectionDiffusion3D > PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD3,TopoD3,Hex> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD3,Hex> ElementQFieldClass;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD3,Hex, ElementXField<PhysD3,TopoD3, Hex> > BasisWeightedClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Real kxz = 0.643;
  Real kyz = 0.765;
  Real kzz = 1.234;
  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                              kxy, kyy, kyz,
                              kxz, kyz, kzz );

  Source3D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 8, xfldElem.nDOF() );

  // hexahedral grid element

  xfldElem.DOF(0) = {0, 0, 0};
  xfldElem.DOF(1) = {1, 0, 0};
  xfldElem.DOF(2) = {1, 1, 0};
  xfldElem.DOF(3) = {0, 1, 0};

  xfldElem.DOF(4) = {0, 0, 1};
  xfldElem.DOF(5) = {1, 0, 1};
  xfldElem.DOF(6) = {1, 1, 1};
  xfldElem.DOF(7) = {0, 1, 1};

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 8, qfldElem.nDOF() );

  // hex solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;
  qfldElem.DOF(3) = 6;

  qfldElem.DOF(4) = 8;
  qfldElem.DOF(5) = 2;
  qfldElem.DOF(6) = 9;
  qfldElem.DOF(7) = 7;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem );

  //BOOST_CHECK( fcn.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 8, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );


  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;
  RefCoordType sRef;
  Real integrandTrue[8];
  ArrayQ integrand[8];

  sRef = {0, 0, 0};
  fcn( sRef, integrand, 8 );

  integrandTrue[0] = ( 9./5.)   + (-36757./1000.);    // (advective) + (viscous)
  integrandTrue[1] = (-11./10.) + ( 1439./125.);
  integrandTrue[2] = ( 0.)      + ( 0.);
  integrandTrue[3] = (-1./5.)   + ( 1437./125. );

  integrandTrue[4] = (-1./2.) + ( 13749./1000. );
  integrandTrue[5] = ( 0. )   + ( 0.);
  integrandTrue[6] = ( 0. )   + ( 0.);
  integrandTrue[7] = ( 0. )   + ( 0.);

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[4], integrand[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[5], integrand[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[6], integrand[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[7], integrand[7], small_tol, close_tol );

  sRef = {1, 0, 0};
  fcn( sRef, integrand, 8 );

  integrandTrue[0] = ( 33./10. ) + (-1039./250.);    // (advective) + (viscous)
  integrandTrue[1] = (-6./5.   ) + ( 1991./1000.);
  integrandTrue[2] = ( -3./5.  ) + ( 337./250.);
  integrandTrue[3] = ( 0.      ) + ( 0. );

  integrandTrue[4] = ( 0.    ) + ( 0. );
  integrandTrue[5] = (-3./2. ) + ( 817./1000. );
  integrandTrue[6] = ( 0.    ) + ( 0.);
  integrandTrue[7] = ( 0.    ) + ( 0.);

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[4], integrand[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[5], integrand[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[6], integrand[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[7], integrand[7], small_tol, close_tol );


  sRef = {1, 1, 0};
  fcn( sRef, integrand, 8 );

  integrandTrue[0] = ( 0.      ) + ( 0. );          // (advective) + (viscous)
  integrandTrue[1] = ( 4./5.   ) + (-1863./500.);
  integrandTrue[2] = ( -16./5. ) + (-2401./1000.);
  integrandTrue[3] = ( 22./5.  ) + ( 239./500. );

  integrandTrue[4] = ( 0. ) + ( 0. );
  integrandTrue[5] = ( 0. ) + ( 0. );
  integrandTrue[6] = (-2. ) + ( 5649./1000. );
  integrandTrue[7] = ( 0. ) + ( 0.);

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[4], integrand[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[5], integrand[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[6], integrand[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[7], integrand[7], small_tol, close_tol );


  sRef = {0, 1, 0};
  fcn( sRef, integrand, 8 );

  integrandTrue[0] = ( 6./5.   ) + (-2347./500.);    // (advective) + (viscous)
  integrandTrue[1] = ( 0.      ) + ( 0.);
  integrandTrue[2] = ( -33./5. ) + (-419./500.);
  integrandTrue[3] = ( 42./5.  ) + ( 1759./1000. );

  integrandTrue[4] = ( 0. ) + ( 0. );
  integrandTrue[5] = ( 0. ) + ( 0. );
  integrandTrue[6] = ( 0. ) + ( 0. );
  integrandTrue[7] = (-3. ) + ( 3773./1000. );

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[4], integrand[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[5], integrand[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[6], integrand[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[7], integrand[7], small_tol, close_tol );


  sRef = {0, 0, 1};
  fcn( sRef, integrand, 8 );

  integrandTrue[0] = ( 4. ) + (-803./200.);    // (advective) + (viscous)
  integrandTrue[1] = ( 0. ) + ( 0. );
  integrandTrue[2] = ( 0. ) + ( 0. );
  integrandTrue[3] = ( 0. ) + ( 0. );

  integrandTrue[4] = ( 32./5. ) + ( 471./40. );
  integrandTrue[5] = (-44./5. ) + (-879./100. );
  integrandTrue[6] = ( 0.     ) + ( 0. );
  integrandTrue[7] = (-8./5.  ) + ( 103./100. );

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[4], integrand[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[5], integrand[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[6], integrand[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[7], integrand[7], small_tol, close_tol );


  sRef = {1, 0, 1};
  fcn( sRef, integrand, 8 );

  integrandTrue[0] = ( 0. ) + ( 0. );    // (advective) + (viscous)
  integrandTrue[1] = ( 1. ) + (-263./1000. );
  integrandTrue[2] = ( 0. ) + ( 0. );
  integrandTrue[3] = ( 0. ) + ( 0. );

  integrandTrue[4] = ( 11./5. ) + ( 951./100. );
  integrandTrue[5] = (-14./5. ) + (-12213./1000. );
  integrandTrue[6] = (-2./5.  ) + ( 1483./500. );
  integrandTrue[7] = ( 0.     ) + ( 0. );

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[4], integrand[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[5], integrand[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[6], integrand[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[7], integrand[7], small_tol, close_tol );


  sRef = {1, 1, 1};
  fcn( sRef, integrand, 8 );

  integrandTrue[0] = ( 0.    ) + ( 0. );    // (advective) + (viscous)
  integrandTrue[1] = ( 0.    ) + ( 0. );
  integrandTrue[2] = ( 9./2. ) + ( -12811./1000. );
  integrandTrue[3] = ( 0.    ) + ( 0. );

  integrandTrue[4] = ( 0.      ) + ( 0. );
  integrandTrue[5] = ( 9./5.   ) + (-599./50. );
  integrandTrue[6] = (-81./5.  ) + ( 36123./1000. );
  integrandTrue[7] = ( 99./10. ) + ( -2833/250. );

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[4], integrand[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[5], integrand[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[6], integrand[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[7], integrand[7], small_tol, close_tol );


  sRef = {0, 1, 1};
  fcn( sRef, integrand, 8 );

  integrandTrue[0] = ( 0.    ) + ( 0. );    // (advective) + (viscous)
  integrandTrue[1] = ( 0.    ) + ( 0. );
  integrandTrue[2] = ( 0.    ) + ( 0. );
  integrandTrue[3] = ( 7./2. ) + (-351./200. );

  integrandTrue[4] = ( 7./5.   ) + ( -108./125. );
  integrandTrue[5] = ( 0.      ) + ( 0. );
  integrandTrue[6] = (-77./10. ) + ( 542./125. );
  integrandTrue[7] = ( 14./5.  ) + ( -1717./1000. );

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[4], integrand[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[5], integrand[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[6], integrand[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[7], integrand[7], small_tol, close_tol );


  sRef = {1./2., 1./2., 1./2.};
  fcn( sRef, integrand, 8 );

  integrandTrue[0] = ( 9/4.  ) + ( -5791./2000. );    // (advective) + (viscous)
  integrandTrue[1] = (-1./2. ) + ( -2163./1000. );
  integrandTrue[2] = (-1.    ) + ( 437./2000. );
  integrandTrue[3] = ( 7./4. ) + (-257./500. );

  integrandTrue[4] = ( 1.    ) + ( -437./2000. );
  integrandTrue[5] = (-7./4. ) + ( 257./500. );
  integrandTrue[6] = (-9./4. ) + ( 5791./2000. );
  integrandTrue[7] = ( 1./2. ) + ( 2163./1000. );

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[4], integrand[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[5], integrand[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[6], integrand[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[7], integrand[7], small_tol, close_tol );


  // test the element integral of the functor

  int quadratureorder = 2;
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD3, Hex, ArrayQ> integral(quadratureorder, nIntegrand);

  ArrayQ rsdPDEElem[8] = {0,0,0,0, 0,0,0,0};

  // cell integration for canonical element
  integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

  Real rsd1 = ( 461./240. ) + (-39247./12000.);   // (advective) + (viscous)
  Real rsd2 = (-83./240.  ) + (-851./480.);
  Real rsd3 = (-221./240. ) + (25./96.);
  Real rsd4 = ( 443./240. ) + (-6851./12000.);

  Real rsd5 = ( 257./240. ) + (2203/4000.);
  Real rsd6 = (-79./48.   ) + (-1081./4000.);
  Real rsd7 = (-207./80.  ) + (12991./4000.);
  Real rsd8 = ( 53./80.   ) + (7303./4000.);

  SANS_CHECK_CLOSE( rsd1, rsdPDEElem[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2, rsdPDEElem[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd3, rsdPDEElem[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd4, rsdPDEElem[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsd5, rsdPDEElem[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd6, rsdPDEElem[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd7, rsdPDEElem[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd8, rsdPDEElem[7], small_tol, close_tol );
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
